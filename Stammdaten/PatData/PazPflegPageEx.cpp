// PazPflegPageEx.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "PatData.h"
#include "PazPflegPageEx.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "PazPflegPage.h"
#include "paz_cab.h"
#include "pazpflegpageex.h"

// CPazPflegPageEx-Dialogfeld

IMPLEMENT_DYNAMIC(CPazPflegPageEx, CDbPropertyPage)
CPazPflegPageEx::CPazPflegPageEx()
	: CDbPropertyPage(CPazPflegPageEx::IDD)
{
	hBrush = NULL;
	hBrushStatic = NULL;

	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_KunBran2);
	HeadControls.Add (&m_Kun);
	HeadControls.Add (&m_A);
	A_kun_gx = NULL;
	A_bas = NULL;
	A_hndw = NULL;
	A_eig = NULL;
	A_eig_div = NULL;
	Pr_ausz_gx = NULL;
	Eti_typ = NULL;
	Kun = NULL;
	KunAdr = NULL;
	Mdn = NULL;
	MdnAdr = NULL;
	PazPflegPage = NULL;
	EtiAnz = 0.0;
	PaletteAnz = 0.0;
	LdPr = 0.0;
	ReadPr = TRUE;
	UsePazCab = FALSE;
	ModalChoice = TRUE;
}

CPazPflegPageEx::~CPazPflegPageEx()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
}

void CPazPflegPageEx::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LMDNEX, m_LMdn);
	DDX_Control(pDX, IDC_MDNEX, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAMEEX, m_MdnName);
	DDX_Control(pDX, IDC_LKUN_BRAN2EX, m_LKunBran2);
	DDX_Control(pDX, IDC_KUN_BRAN2EX, m_KunBran2);
	DDX_Control(pDX, IDC_LKUNEX, m_LKun);
	DDX_Control(pDX, IDC_KUNEX, m_Kun);
	DDX_Control(pDX, IDC_KUNNAMEEX, m_KunName);
	DDX_Control(pDX, IDC_HEADBORDEREX, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDEREX, m_DataBorder);
	DDX_Control(pDX, IDC_LAEX, m_LA);
	DDX_Control(pDX, IDC_AEX, m_A);
	DDX_Control(pDX, IDC_A_BZ1EX, m_ABz1);

	DDX_Control(pDX, IDC_ETIGROUP, m_EtiGroup);
	DDX_Control(pDX, IDC_LETIETIKETT, m_LEtiEtikett);
	DDX_Control(pDX, IDC_LETIEINH, m_LEtiEinh);
	DDX_Control(pDX, IDC_LETIME, m_LEtiMe);
	DDX_Control(pDX, IDC_LGEB_ETI, m_LGebEti);
	DDX_Control(pDX, IDC_ETIETIKETT, m_EtiEtikett);
	DDX_Control(pDX, IDC_ETIEINH, m_EtiEinh);
	DDX_Control(pDX, IDC_ETIME, m_EtiMe);
	DDX_Control(pDX, IDC_LPALETTE_ETI, m_LPaletteEti);
	DDX_Control(pDX, IDC_PALETTEETIKETT, m_PaletteEtikett);
	DDX_Control(pDX, IDC_PALETTEEINH, m_PaletteEinh);
	DDX_Control(pDX, IDC_PALETTEME, m_PaletteMe);
	DDX_Control(pDX, IDC_LPOS_ETI, m_LPosEtikett);
	DDX_Control(pDX, IDC_POSETI, m_PosEtikett);
	DDX_Control(pDX, IDC_LGEWPAR, m_LGewPar);
	DDX_Control(pDX, IDC_GEWPAR, m_GewPar);
	DDX_Control(pDX, IDC_LAUTOPAR, m_LAutoPar);
	DDX_Control(pDX, IDC_AUTOPAR, m_AutoPar);
	DDX_Control(pDX, IDC_LVPPAR, m_LVpPar);
	DDX_Control(pDX, IDC_VPPAR, m_VpPar);
	DDX_Control(pDX, IDC_LSONDER_ETI, m_LSonderEti);
	DDX_Control(pDX, IDC_SONDER_ETI, m_SonderEti);
	DDX_Control(pDX, IDC_LAUSZ_ART, m_LAuszArt);
	DDX_Control(pDX, IDC_AUSZ_ART, m_AuszArt);
	DDX_Control(pDX, IDC_LDEVISE, m_LDevise);
	DDX_Control(pDX, IDC_DEVISE, m_Devise);
	DDX_Control(pDX, IDC_DEVISE2, m_Devise2);
	DDX_Control(pDX, IDC_LPrRechKz, m_LPrRechKz);
	DDX_Control(pDX, IDC_PrRechKz, m_PrRechKz);
	DDX_Control(pDX, IDC_LEAN, m_LEan);
	DDX_Control(pDX, IDC_LCOST, m_LCost);
	DDX_Control(pDX, IDC_LETI_TYP, m_LEti_typ);
	DDX_Control(pDX, IDC_ETI_TYP_COMBO, m_Eti_typ);
	DDX_Control(pDX, IDC_EAN, m_Ean);
	DDX_Control(pDX, IDC_CAB, m_Cab);
	DDX_Control(pDX, IDC_LETI_SUM1, m_LEtiSum1);
	DDX_Control(pDX, IDC_ETI_SUM1_COMBO, m_EtiSum1);
	DDX_Control(pDX, IDC_EAN1, m_Ean1);
	DDX_Control(pDX, IDC_CAB1, m_Cab1);
	DDX_Control(pDX, IDC_LETI_SUM2, m_LEtiSum2);
	DDX_Control(pDX, IDC_ETI_SUM2_COMBO, m_EtiSum2);
	DDX_Control(pDX, IDC_EAN2, m_Ean2);
	DDX_Control(pDX, IDC_CAB2, m_Cab2);
	DDX_Control(pDX, IDC_LETI_SUM3, m_LEtiSum3);
	DDX_Control(pDX, IDC_ETI_SUM3_COMBO, m_EtiSum3);
	DDX_Control(pDX, IDC_EAN3, m_Ean3);
	DDX_Control(pDX, IDC_CAB3, m_Cab3);
	DDX_Control(pDX, IDC_LEAN_NVE, m_LEanNve);
	DDX_Control(pDX, IDC_LCOST_NVE, m_LCostNve);
	DDX_Control(pDX, IDC_LETI_NVE1, m_LEtiNve1);
	DDX_Control(pDX, IDC_ETI_NVE1, m_EtiNve1);
	DDX_Control(pDX, IDC_EAN_NVE1, m_EanNve1);
	DDX_Control(pDX, IDC_CAB_NVE1, m_CabNve1);
	DDX_Control(pDX, IDC_LETI_NVE2, m_LEtiNve2);
	DDX_Control(pDX, IDC_ETI_NVE2, m_EtiNve2);
	DDX_Control(pDX, IDC_EAN_NVE2, m_EanNve2);
	DDX_Control(pDX, IDC_CAB_NVE2, m_CabNve2);
	DDX_Control(pDX, IDC_VPBORDER, m_VpBorder);
	DDX_Control(pDX, IDC_LLDPR,    m_LLdPr);
	DDX_Control(pDX, IDC_LDPR,     m_LdPr);
}


BEGIN_MESSAGE_MAP(CPazPflegPageEx, CPropertyPage)
	ON_WM_CTLCOLOR ()
    ON_COMMAND (IDC_AKUNGXCHOICE, OnAKunGxChoice)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
END_MESSAGE_MAP()


// CPazPflegPageEx-Meldungshandler


void CPazPflegPageEx::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CPazPflegPageEx::OnInitDialog()
{
	CDialog::OnInitDialog();

	Cfg.SetProgName (_T("pazdata"));
	if (Cfg.GetCfgValue (_T("UseOdbc"), PROG_CFG::cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("ReadPr"), PROG_CFG::cfg_v) == TRUE)
    {
			ReadPr = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("UsePazCab"), PROG_CFG::cfg_v) == TRUE)
    {
			UsePazCab = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("ModalChoice"), PROG_CFG::cfg_v) == TRUE)
    {
			ModalChoice = _tstoi (PROG_CFG::cfg_v);
    }

	Cfg.CloseCfg ();

	PazProperty = (CPropertySheet *) GetParent ();
	View = (DbFormView *) PazProperty->GetParent ();
	Frame = (CFrameWnd *) View->GetParent ();

	A_kun_gx->opendbase (_T("bws"));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();

	CRect bRect (0, 0, 100, 20);;
	m_AKunGxChoice.Create (_T("Auswahl F8"),
		WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON | 
		BS_CENTER | BS_VCENTER | WS_TABSTOP, 
							bRect, this, IDC_AKUNGXCHOICE);

	m_LdPr.SetReadOnly ();
    m_LdPr.ModifyStyle (WS_TABSTOP, 0);
    Form.Add (new CUniFormField (&m_KunName,EDIT,   (char *)   &KunAdr->adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Mdn,EDIT,       (short *)  &Mdn->mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT,   (char *)   &MdnAdr->adr.adr_krz, VCHAR));
	Form.Add (new CUniFormField (&m_KunBran2,COMBOBOX, (char *) A_kun_gx->a_kun_gx.kun_bran2, VCHAR));
    Form.Add (new CFormField (&m_Kun,EDIT,       (long *)   &Kun->kun.kun, VLONG));
	Form.Add (new CFormField (&m_A,EDIT,         (double *) &A_bas->a_bas.a, VDOUBLE, 13, 0));
    Form.Add (new CUniFormField (&m_ABz1,EDIT,   (char *)   &A_bas->a_bas.a_bz1, VCHAR));

	Form.Add (new CFormField (&m_EtiEtikett, CHECKBOX,  (char *)   A_kun_gx->a_kun_gx.geb_eti, VCHAR));
	Form.Add (new CFormField (&m_EtiEinh,    COMBOBOX, (short *) &A_kun_gx->a_kun_gx.geb_fill, VSHORT));
//	Form.Add (new CFormField (&m_EtiMe,      EDIT,     (long *) &A_kun_gx->a_kun_gx.geb_anz, VLONG));
	Form.Add (new CFormField (&m_EtiMe,      EDIT,     (double *) &EtiAnz, VDOUBLE, 8, 3));

	Form.Add (new CFormField (&m_PaletteEtikett, CHECKBOX,  (char *)   A_kun_gx->a_kun_gx.pal_eti, VCHAR));
	Form.Add (new CFormField (&m_PaletteEinh,    COMBOBOX, (short *) &A_kun_gx->a_kun_gx.pal_fill, VSHORT));
//	Form.Add (new CFormField (&m_PaletteMe,      EDIT,     (long *) &A_kun_gx->a_kun_gx.pal_anz, VLONG));
	Form.Add (new CFormField (&m_PaletteMe,      EDIT,     (double *) &PaletteAnz, VDOUBLE, 8, 3));

	Form.Add (new CFormField (&m_LdPr,      EDIT,     (double *) &LdPr, VDOUBLE, 8, 3));

	Form.Add (new CFormField (&m_PosEtikett, CHECKBOX,  (char *)   A_kun_gx->a_kun_gx.pos_eti, VCHAR));
    Form.Add (new CFormField (&m_GewPar,EDIT,       (long *)   &A_kun_gx->a_kun_gx.gwpar, VLONG));
    Form.Add (new CFormField (&m_AutoPar,EDIT,       (long *)   &A_kun_gx->a_kun_gx.ampar, VLONG));
    Form.Add (new CFormField (&m_VpPar,EDIT,       (long *)   &A_kun_gx->a_kun_gx.vppar, VLONG));
    Form.Add (new CFormField (&m_SonderEti,EDIT,       (short *)   &A_kun_gx->a_kun_gx.sonder_eti, VSHORT));

	Form.Add (new CFormField (&m_AuszArt,COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.ausz_art, VSHORT));
	Form.Add (new CFormField (&m_Devise,COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.devise, VSHORT));
	Form.Add (new CFormField (&m_Devise2,COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.devise2, VSHORT));
	Form.Add (new CFormField (&m_PrRechKz, CHECKBOX,  (char *)   A_kun_gx->a_kun_gx.pr_rech_kz, VCHAR));
	Form.Add (new CFormField (&m_Eti_typ,COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.eti_typ, VSHORT));
	Form.Add (new CFormField (&m_Ean, EDIT,  (double *)   &A_kun_gx->a_kun_gx.ean, VDOUBLE, 13, 0));
	Form.Add (new CFormField (&m_Cab, COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.cab, VSHORT));

	Form.Add (new CFormField (&m_EtiSum1,COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.eti_sum1, VSHORT));
	Form.Add (new CFormField (&m_Ean1, EDIT,  (double *)   &A_kun_gx->a_kun_gx.ean1, VDOUBLE, 13, 0));
	Form.Add (new CFormField (&m_Cab1, COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.cab1, VSHORT));

	Form.Add (new CFormField (&m_EtiSum2,COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.eti_sum2, VSHORT));
	Form.Add (new CFormField (&m_Ean2, EDIT,  (double *)   &A_kun_gx->a_kun_gx.ean2, VDOUBLE, 13, 0));
	Form.Add (new CFormField (&m_Cab2, COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.cab2, VSHORT));

	Form.Add (new CFormField (&m_EtiSum3,COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.eti_sum3, VSHORT));
	Form.Add (new CFormField (&m_Ean3, EDIT,  (double *)   &A_kun_gx->a_kun_gx.ean3, VDOUBLE, 13, 0));
	Form.Add (new CFormField (&m_Cab3, COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.cab3, VSHORT));

//	Form.Add (new CFormField (&m_EtiNve1,EDIT,  (short *)   &A_kun_gx->a_kun_gx.eti_nve1, VSHORT));
	Form.Add (new CFormField (&m_EtiNve1,COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.eti_nve1, VSHORT));
	Form.Add (new CFormField (&m_EanNve1, EDIT,  (double *)   &A_kun_gx->a_kun_gx.ean_nve1, VDOUBLE, 13, 0));
	Form.Add (new CFormField (&m_CabNve1, COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.cab_nve1, VSHORT));

//	Form.Add (new CFormField (&m_EtiNve2,EDIT,  (short *)   &A_kun_gx->a_kun_gx.eti_nve2, VSHORT));
	Form.Add (new CFormField (&m_EtiNve2,COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.eti_nve2, VSHORT));
	Form.Add (new CFormField (&m_EanNve2, EDIT,  (double *)   &A_kun_gx->a_kun_gx.ean_nve2, VDOUBLE, 13, 0));
	Form.Add (new CFormField (&m_CabNve2, COMBOBOX,  (short *)   &A_kun_gx->a_kun_gx.cab_nve2, VSHORT));

	FillKunBran2 ();
	FillEinh (&m_EtiEinh);
	FillEinh (&m_PaletteEinh);
	FillAuszArt ();
	FillPTab (&m_Devise, _T("devise"));
	FillPTab (&m_Devise2, _T("devise"));
	FillPTab (&m_Eti_typ, _T("eti_typ"));
	FillPTab (&m_EtiSum1, _T("eti_typ"));
	FillPTab (&m_EtiSum2, _T("eti_typ"));
	FillPTab (&m_EtiSum3, _T("eti_typ"));
	FillPTab (&m_EtiNve1, _T("eti_typ"));
	FillPTab (&m_EtiNve2, _T("eti_typ"));

	TestEtiPTab (&m_Eti_typ);
	TestEtiPTab (&m_EtiSum1);
	TestEtiPTab (&m_EtiSum2);
	TestEtiPTab (&m_EtiSum3);
	TestEtiPTab (&m_EtiNve1);
	TestEtiPTab (&m_EtiNve2);

	if (!UsePazCab)
	{
		FillPTab (&m_Cab, _T("cab"));
		FillPTab (&m_Cab1, _T("cab"));
		FillPTab (&m_Cab2, _T("cab"));
		FillPTab (&m_Cab3, _T("cab"));
		FillPTab (&m_CabNve1, _T("cab"));
		FillPTab (&m_CabNve2, _T("cab"));
	}
	else
	{
		FillPazCab (&m_Cab, _T("cab"));
		FillPazCab (&m_Cab1, _T("cab"));
		FillPazCab (&m_Cab2, _T("cab"));
		FillPazCab (&m_Cab3, _T("cab"));
		FillPazCab (&m_CabNve1, _T("cab"));
		FillPazCab (&m_CabNve2, _T("cab"));
	}


    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 1, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	KunGrid.Create (this, 1, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	CtrlGrid.CreateChoiceButton (m_KunChoice, IDC_KUNCHOICE, this);
	CCtrlInfo *c_KunChoice = new CCtrlInfo (&m_KunChoice, 1, 0, 1, 1);
	KunGrid.Add (c_KunChoice);

	AGrid.Create (this, 1, 2);
    AGrid.SetBorder (0, 0);
    AGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 0, 0, 1, 1);
	AGrid.Add (c_A);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);

	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 6);
    c_HeadBorder->rightspace = 20; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);

	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 3, 1, 4, 1);
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LKunBran2 = new CCtrlInfo (&m_LKunBran2, 1, 2, 1, 1);
	CtrlGrid.Add (c_LKunBran2);
	CCtrlInfo *c_KunBran2 = new CCtrlInfo (&m_KunBran2, 2, 2, 1, 1);
	CtrlGrid.Add (c_KunBran2);

	CCtrlInfo *c_LKun = new CCtrlInfo (&m_LKun, 1, 3, 1, 1);
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid = new CCtrlInfo (&KunGrid, 2, 3, 1, 1);
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 3, 3, 4, 1);
	CtrlGrid.Add (c_KunName);

	CCtrlInfo *c_LA = new CCtrlInfo (&m_LA, 1, 4, 1, 1);
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid = new CCtrlInfo (&AGrid, 2, 4, 1, 1);
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_ABz1 = new CCtrlInfo (&m_ABz1, 3, 4, 4, 1);
	CtrlGrid.Add (c_ABz1);

	CCtrlInfo *c_AKunGxChoice = new CCtrlInfo (&m_AKunGxChoice, 1, 5, 1, 1);
	CtrlGrid.Add (c_AKunGxChoice);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 6, DOCKRIGHT, DOCKBOTTOM);
    c_DataBorder->rightspace = 20; 
	CtrlGrid.Add (c_DataBorder);

    EtiGrid.Create (this, 20, 20);
    EtiGrid.SetBorder (20, 20);
    EtiGrid.SetCellHeight (15);
    EtiGrid.SetFontCellHeight (this);
    EtiGrid.SetGridSpace (5, 8);

	CCtrlInfo *c_EtiGroup = new CCtrlInfo (&m_EtiGroup, 0, 1, 5, 5);
	EtiGrid.Add (c_EtiGroup);

	CCtrlInfo *c_LEtiEtikett = new CCtrlInfo (&m_LEtiEtikett, 2, 2, 1, 1);
	EtiGrid.Add (c_LEtiEtikett);
	CCtrlInfo *c_LEtiEinh = new CCtrlInfo (&m_LEtiEinh, 3, 2, 1, 1);
	EtiGrid.Add (c_LEtiEinh);
	CCtrlInfo *c_LEtiMe = new CCtrlInfo (&m_LEtiMe, 4, 2, 1, 1);
	EtiGrid.Add (c_LEtiMe);

	CCtrlInfo *c_LGebEti = new CCtrlInfo (&m_LGebEti, 1, 3, 1, 1);
	EtiGrid.Add (c_LGebEti);

	CCtrlInfo *c_EtiEtikett = new CCtrlInfo (&m_EtiEtikett, 2, 3, 1, 1);
	EtiGrid.Add (c_EtiEtikett);
	CCtrlInfo *c_EtiEinh = new CCtrlInfo (&m_EtiEinh, 3, 3, 1, 1);
	EtiGrid.Add (c_EtiEinh);
	CCtrlInfo *c_EtiMe = new CCtrlInfo (&m_EtiMe, 4, 3, 1, 1);
	EtiGrid.Add (c_EtiMe);

	CCtrlInfo *c_LPaletteEti = new CCtrlInfo (&m_LPaletteEti, 1, 4, 1, 1);
	EtiGrid.Add (c_LPaletteEti);

	CCtrlInfo *c_PaletteEtikett = new CCtrlInfo (&m_PaletteEtikett, 2, 4, 1, 1);
	EtiGrid.Add (c_PaletteEtikett);
	CCtrlInfo *c_PaletteEinh = new CCtrlInfo (&m_PaletteEinh, 3, 4, 1, 1);
	EtiGrid.Add (c_PaletteEinh);
	CCtrlInfo *c_PaletteMe = new CCtrlInfo (&m_PaletteMe, 4, 4, 1, 1);
	EtiGrid.Add (c_PaletteMe);

	CCtrlInfo *c_LPosEtikett = new CCtrlInfo (&m_LPosEtikett, 1, 5, 1, 1);
	EtiGrid.Add (c_LPosEtikett);

	CCtrlInfo *c_PosEtikett = new CCtrlInfo (&m_PosEtikett, 2, 5, 1, 1);
	EtiGrid.Add (c_PosEtikett);

	CCtrlInfo *c_EtiGrid = new CCtrlInfo (&EtiGrid, 1, 7, 3, 5);
	CtrlGrid.Add (c_EtiGrid);

	CCtrlInfo *c_VpBorder = new CCtrlInfo (&m_VpBorder, 4, 7, 3, 7);
	CtrlGrid.Add (c_VpBorder);

	CCtrlInfo *c_LGewPar = new CCtrlInfo (&m_LGewPar, 5, 8, 1, 1);
	CtrlGrid.Add (c_LGewPar);
	CCtrlInfo *c_GewPar = new CCtrlInfo (&m_GewPar, 6, 8, 1, 1);
	CtrlGrid.Add (c_GewPar);

	CCtrlInfo *c_LAutoPar = new CCtrlInfo (&m_LAutoPar, 5, 9, 1, 1);
	CtrlGrid.Add (c_LAutoPar);
	CCtrlInfo *c_AutoPar = new CCtrlInfo (&m_AutoPar, 6, 9, 1, 1);
	CtrlGrid.Add (c_AutoPar);

	CCtrlInfo *c_LVpPar = new CCtrlInfo (&m_LVpPar, 5, 10, 1, 1);
	CtrlGrid.Add (c_LVpPar);
	CCtrlInfo *c_VpPar = new CCtrlInfo (&m_VpPar, 6, 10, 1, 1);
	CtrlGrid.Add (c_VpPar);

	CCtrlInfo *c_LSonderEti = new CCtrlInfo (&m_LSonderEti, 5, 11, 1, 1);
	CtrlGrid.Add (c_LSonderEti);
	CCtrlInfo *c_SonderEti = new CCtrlInfo (&m_SonderEti, 6, 11, 1, 1);
	CtrlGrid.Add (c_SonderEti);


	CCtrlInfo *c_LAuszArt = new CCtrlInfo (&m_LAuszArt, 5, 12, 1, 1);
	CtrlGrid.Add (c_LAuszArt);
	CCtrlInfo *c_AuszArt = new CCtrlInfo (&m_AuszArt, 6, 12, 1, 1);
	CtrlGrid.Add (c_AuszArt);

	CCtrlInfo *c_LDevise = new CCtrlInfo (&m_LDevise, 1, 12, 1, 1);
	c_LDevise->SetCellPos (0, CtrlGrid.CellHeight / 2);
	CtrlGrid.Add (c_LDevise);

    DeviseGrid.Create (this, 1, 2);
    DeviseGrid.SetBorder (0, 0);
    DeviseGrid.SetCellHeight (15);
    DeviseGrid.SetFontCellHeight (this);
    DeviseGrid.SetGridSpace (5, 0);

	CCtrlInfo *c_Devise = new CCtrlInfo (&m_Devise, 0, 0, 1, 1);
	DeviseGrid.Add (c_Devise);
	CCtrlInfo *c_Devise2 = new CCtrlInfo (&m_Devise2, 1, 0, 1, 1);
	DeviseGrid.Add (c_Devise2);

	CCtrlInfo *c_DeviseGrid = new CCtrlInfo (&DeviseGrid, 2, 12, 1, 1);
	c_DeviseGrid->SetCellPos (0, CtrlGrid.CellHeight / 2);
	CtrlGrid.Add (c_DeviseGrid);

	if (ReadPr)
	{
		CCtrlInfo *c_LLdPr = new CCtrlInfo (&m_LLdPr, 1, 13, 1, 1);
		c_LLdPr->SetCellPos (0, CtrlGrid.CellHeight / 2);
		CtrlGrid.Add (c_LLdPr);
		CCtrlInfo *c_LdPr = new CCtrlInfo (&m_LdPr, 2, 13, 1, 1);
		c_LdPr->SetCellPos (0, CtrlGrid.CellHeight / 2);
		CtrlGrid.Add (c_LdPr);
	}
	else
	{
		m_LLdPr.ShowWindow (SW_HIDE);
		m_LdPr.ShowWindow (SW_HIDE);
	}

	CCtrlLine *c_SuLine = new CCtrlLine (this, HORIZONTAL, 100, 4, 1, 14, DOCKRIGHT, 1); 
    c_SuLine->rightspace = 23; 
	c_SuLine->SetCellPos (0, CtrlGrid.CellHeight);
	CtrlGrid.Add (c_SuLine);

    SuGrid.Create (this, 20, 20);
    SuGrid.SetBorder (0, 0);
    SuGrid.SetCellHeight (15);
    SuGrid.SetFontCellHeight (this);
    SuGrid.SetGridSpace (5, 8);

	CCtrlInfo *c_LPrRechKz = new CCtrlInfo (&m_LPrRechKz, 0, 0, 1, 1);
	SuGrid.Add (c_LPrRechKz);
	CCtrlInfo *c_PrRechKz = new CCtrlInfo (&m_PrRechKz, 1, 0, 1, 1);
	SuGrid.Add (c_PrRechKz);

	CCtrlInfo *c_LEan = new CCtrlInfo (&m_LEan, 2, 0, 1, 1);
	SuGrid.Add (c_LEan);
	CCtrlInfo *c_LCost = new CCtrlInfo (&m_LCost, 3, 0, 1, 1);
	SuGrid.Add (c_LCost);

	CCtrlInfo *c_LEti_typ = new CCtrlInfo (&m_LEti_typ, 0, 1, 1, 1);
	SuGrid.Add (c_LEti_typ);
	CCtrlInfo *c_Eti_typ = new CCtrlInfo (&m_Eti_typ, 1, 1, 1, 1);
	SuGrid.Add (c_Eti_typ);
	CCtrlInfo *c_Ean = new CCtrlInfo (&m_Ean, 2, 1, 1, 1);
	SuGrid.Add (c_Ean);
	CCtrlInfo *c_Cab = new CCtrlInfo (&m_Cab, 3, 1, 1, 1);
	SuGrid.Add (c_Cab);

	CCtrlInfo *c_LEtiSum1 = new CCtrlInfo (&m_LEtiSum1, 0, 2, 1, 1);
	SuGrid.Add (c_LEtiSum1);
	CCtrlInfo *c_EtiSum1 = new CCtrlInfo (&m_EtiSum1, 1, 2, 1, 1);
	SuGrid.Add (c_EtiSum1);
	CCtrlInfo *c_Ean1 = new CCtrlInfo (&m_Ean1, 2, 2, 1, 1);
	SuGrid.Add (c_Ean1);
	CCtrlInfo *c_Cab1 = new CCtrlInfo (&m_Cab1, 3, 2, 1, 1);
	SuGrid.Add (c_Cab1);

	CCtrlInfo *c_LEtiSum2 = new CCtrlInfo (&m_LEtiSum2, 0, 3, 1, 1);
	SuGrid.Add (c_LEtiSum2);
	CCtrlInfo *c_EtiSum2 = new CCtrlInfo (&m_EtiSum2, 1, 3, 1, 1);
	SuGrid.Add (c_EtiSum2);
	CCtrlInfo *c_Ean2 = new CCtrlInfo (&m_Ean2, 2, 3, 1, 1);
	SuGrid.Add (c_Ean2);
	CCtrlInfo *c_Cab2 = new CCtrlInfo (&m_Cab2, 3, 3, 1, 1);
	SuGrid.Add (c_Cab2);

	CCtrlInfo *c_LEtiSum3 = new CCtrlInfo (&m_LEtiSum3, 0, 4, 1, 1);
	SuGrid.Add (c_LEtiSum3);
	CCtrlInfo *c_EtiSum3 = new CCtrlInfo (&m_EtiSum3, 1, 4, 1, 1);
	SuGrid.Add (c_EtiSum3);
	CCtrlInfo *c_Ean3 = new CCtrlInfo (&m_Ean3, 2, 4, 1, 1);
	SuGrid.Add (c_Ean3);
	CCtrlInfo *c_Cab3 = new CCtrlInfo (&m_Cab3, 3, 4, 1, 1);
	SuGrid.Add (c_Cab3);

	CCtrlInfo *c_LEanNve = new CCtrlInfo (&m_LEanNve, 6, 0, 1, 1);
	SuGrid.Add (c_LEanNve);
	CCtrlInfo *c_LCostNve = new CCtrlInfo (&m_LCostNve, 7, 0, 1, 1);
	SuGrid.Add (c_LCostNve);

	CCtrlInfo *c_LEtiNve1 = new CCtrlInfo (&m_LEtiNve1, 4, 2, 1, 1);
	SuGrid.Add (c_LEtiNve1);
	CCtrlInfo *c_EtiNve1 = new CCtrlInfo (&m_EtiNve1, 5, 2, 1, 1);
	SuGrid.Add (c_EtiNve1);
	CCtrlInfo *c_EanNve1 = new CCtrlInfo (&m_EanNve1, 6, 2, 1, 1);
	SuGrid.Add (c_EanNve1);
	CCtrlInfo *c_CabNve1 = new CCtrlInfo (&m_CabNve1, 7, 2, 1, 1);
	SuGrid.Add (c_CabNve1);

	CCtrlInfo *c_LEtiNve2 = new CCtrlInfo (&m_LEtiNve2, 4, 3, 1, 1);
	SuGrid.Add (c_LEtiNve2);
	CCtrlInfo *c_EtiNve2 = new CCtrlInfo (&m_EtiNve2, 5, 3, 1, 1);
	SuGrid.Add (c_EtiNve2);
	CCtrlInfo *c_EanNve2 = new CCtrlInfo (&m_EanNve2, 6, 3, 1, 1);
	SuGrid.Add (c_EanNve2);
	CCtrlInfo *c_CabNve2 = new CCtrlInfo (&m_CabNve2, 7, 3, 1, 1);
	SuGrid.Add (c_CabNve2);


	CCtrlInfo *c_SuGrid = new CCtrlInfo (&SuGrid, 1, 15, 6, 1);
	CtrlGrid.Add (c_SuGrid);


	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
    EtiGrid.SetItemCharWidth (&m_EtiEinh, 20);
    EtiGrid.SetItemCharWidth (&m_PaletteEinh, 20);
	CtrlGrid.Display ();
    CtrlGrid.SetItemCharHeight (&m_HeadBorder, 6);
    CtrlGrid.SetItemCharWidth (&m_AKunGxChoice, 15);
    EtiGrid.SetItemCharHeight (&m_EtiGroup, 5);
    EtiGrid.SetItemCharWidth (&m_EtiGroup, 55);
    CtrlGrid.SetItemCharHeight (&m_VpBorder, 6);

/*
	m_Cab.SetDroppedWidth (200);
	m_Cab1.SetDroppedWidth (200);
	m_Cab2.SetDroppedWidth (200);
	m_Cab3.SetDroppedWidth (200);

	m_CabNve1.SetDroppedWidth (200);
	m_CabNve2.SetDroppedWidth (200);
*/


    EnableHeadControls (FALSE);
    Form.Show ();

	SetDropDownWidth (&m_Eti_typ);
	SetDropDownWidth (&m_EtiSum1);
	SetDropDownWidth (&m_EtiSum2);
	SetDropDownWidth (&m_EtiSum3);
	SetDropDownWidth (&m_EtiNve1);
	SetDropDownWidth (&m_EtiNve2);

//	m_Cab.ModifyStyle (0, CBS_NOINTEGRALHEIGHT);   
	SetDropDownWidth (&m_Cab);
	SetDropDownWidth (&m_Cab1);
	SetDropDownWidth (&m_Cab2);
	SetDropDownWidth (&m_Cab3);

	SetDropDownWidth (&m_CabNve1);
	SetDropDownWidth (&m_CabNve2);

	return TRUE;
}


BOOL CPazPflegPageEx::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				if (PazPflegPage != NULL)
				{
					PazPflegPage->StepBack ();
				}
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				if (PazPflegPage != NULL)
				{
					PazPflegPage->OnDelete ();
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				if (PazPflegPage != NULL)
				{
					PazPflegPage->write ();
				}
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (PazPflegPage != NULL)
				{
					if (((CPazPflegPage *) PazPflegPage)->Choice != NULL)
					{
						if (((CPazPflegPage *) PazPflegPage)->Choice->IsWindowVisible ())
						{
							((CPazPflegPage *) PazPflegPage)->Choice->ShowWindow (SW_HIDE);
						}
						else
						{
							((CPazPflegPage *) PazPflegPage)->Choice->ShowWindow (SW_SHOWNORMAL);
						}
					}
					else
					{
						((CPazPflegPage *) PazPflegPage)->OnAKunGxChoice ();
					}
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
                if (TestCabF9 ())
				{
					return TRUE;
				}
				break;

			}
			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}
			if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
			break;
		case WM_LBUTTONDOWN :
			if (TestComboBoxes (pMsg->hwnd, CPoint (pMsg->lParam))) return TRUE;
			break;
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

HBRUSH CPazPflegPageEx::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);

    if (hBrush == NULL)
	{
		  hBrush = CreateSolidBrush (Color);
		  hBrushStatic = CreateSolidBrush (Color);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			return hBrush;
	}
/*
	else if (pWnd->GetDlgCtrlID() == IDC_ARTDAT)
	{
            pDC->SetTextColor (CaptionColor);
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	else if (pWnd->GetDlgCtrlID() == IDC_AUSZDAT)
	{
            pDC->SetTextColor (CaptionColor);
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
*/
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CButton )))
	{
            pDC->SetBkColor (Color);
			pDC->SetBkMode (TRANSPARENT);
			return hBrushStatic;
	}
	return CPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CPazPflegPageEx::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CPazPflegPageEx::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

void CPazPflegPageEx::FillKunBran2 ()
{
	CFormField *f = Form.GetFormField (&m_KunBran2);
	if (f != NULL)
	{
		f->ComboValues.clear ();
//		f->ComboValues.push_back (new CString ("0")); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"kun_bran2\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
//		f->SetSel (0);
		f->Show ();
		f->Get ();
	}
}

void CPazPflegPageEx::OnAKunGxChoice ()
{
	if (PazPflegPage != NULL)
	{
		PazPflegPage->OnChoice ();
	}
}

void CPazPflegPageEx::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
//		Control->SetReadOnly (!enable);
        Control->EnableWindow (enable);
	}
	if (ModalChoice)
	{
		m_AKunGxChoice.EnableWindow (enable);
	}
}

void CPazPflegPageEx::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CPazPflegPageEx::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}


void CPazPflegPageEx::FillEinh (CWnd *Control)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"me_einh_fill\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
	}
}

void CPazPflegPageEx::FillAuszArt ()
{
	CFormField *f = Form.GetFormField (&m_AuszArt);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"ausz_art\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
	}
}

void CPazPflegPageEx::FillPTab (CWnd *Control, LPCTSTR PtItem)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		CString Query;
		Query.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			          _T("where ptitem = \"%s\" ")
					  _T("order by ptlfnr"), PtItem);
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (Query.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
	}
}

void CPazPflegPageEx::TestEtiPTab (CWnd *Control)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		if (f->ComboValues.size () > 0) return;
		for (int i = 0; i < 100; i ++)
		{
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%d Etikettentyp %d"), i + 1, i + 1);
		    f->ComboValues.push_back (ComboValue); 
		}
		f->FillComboBox ();
	}
}

void CPazPflegPageEx::SetDropDownWidth (CComboBox *combo)
{
	int len;
	CString Text;
    CSize s;
	TEXTMETRIC tm;

	len = 0;
	CDC *cDC = combo->GetDC ();
	CFont *pFont = combo->GetFont ();
    CFont* pOldFont = cDC->SelectObject(pFont);
	cDC->GetTextMetrics (&tm);
	for (int i = 0; i < combo->GetCount (); i ++)
	{
		combo->GetLBText (i, Text);
		s = cDC->GetTextExtent (Text);
		s.cx += GetSystemMetrics (SM_CXVSCROLL) + 
			2 * GetSystemMetrics (SM_CXBORDER) + tm.tmAveCharWidth;
		if (s.cx > len) len = s.cx;
	}
//	cDC->SelectObject(pOldFont);
    combo->ReleaseDC (cDC); 

	if (len > 300)
	{
		combo->SetDroppedWidth (400);
	}
	else
	{
		combo->SetDroppedWidth (len);
	}

	combo->SetHorizontalExtent (len);
}

void CPazPflegPageEx::FillPazCab (CWnd *Control, LPCTSTR PtItem)
{
	PAZ_CAB_CLASS PazCab;

	CFormField *f = Form.GetFormField (Control);
	if (f == NULL) return;
	PazCab.sqlout ((short *) &PazCab.paz_cab.cabnum, SQLSHORT, 0);
	int cursor = PazCab.sqlcursor (_T("select cabnum from paz_cab ")
		                           _T("where cabnum > -1 order by cabnum"));

	while (PazCab.sqlfetch (cursor) == 0)
	{
			PazCab.dbreadfirst ();
			LPSTR pos = (LPSTR) PazCab.paz_cab.cabinfo;
			CDbUniCode::DbToUniCode (PazCab.paz_cab.cabinfo, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), PazCab.paz_cab.cabnum,
				                              PazCab.paz_cab.cabinfo);
		    f->ComboValues.push_back (ComboValue); 
	}

	PazCab.sqlclose (cursor);
	f->FillComboBox ();
}

void CPazPflegPageEx::OnCopy ()
{
	((CPazPflegPage *) PazPflegPage)->OnEditCopy ();
}


void CPazPflegPageEx::OnPaste ()
{
	((CPazPflegPage *) PazPflegPage)->OnEditCopy ();
}

void CPazPflegPageEx::OnCopyEx ()
{
	((CPazPflegPage *) PazPflegPage)->Copy ();
}

void CPazPflegPageEx::OnPasteEx ()
{
	((CPazPflegPage *) PazPflegPage)->OnPaste ();
}

void CPazPflegPageEx::OnCbnDropdownCab()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	int idx = ChoicePazCab ();
	if (idx != -1)
	{
		m_Cab.SetCurSel (idx);
//		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
	}
}

int CPazPflegPageEx::ChoicePazCab ()
{
	CChoicePazCab Choice;
	Choice.NoSort = TRUE;
	Choice.NoSearch = TRUE;
    CString Text;
	Choice.IsModal = TRUE;
	Choice.CreateDlg ();
    Choice.SetDbClass (Pr_ausz_gx);
	Choice.DoModal();
    if (Choice.GetState ())
    {
		  int idx = Choice.SelectedItem;
		  return idx;
    }
	return -1;
}

BOOL CPazPflegPageEx::TestComboBoxes (HWND hWnd, CPoint mPoint)
{
	if (!UsePazCab) return FALSE;
	if (hWnd == m_Cab.m_hWnd)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_Cab.SetCurSel (idx);
		}
		m_Cab.SetFocus ();
		return TRUE;
	}
	else if (hWnd == m_Cab1.m_hWnd)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_Cab1.SetCurSel (idx);
		}
		m_Cab1.SetFocus ();
		return TRUE;
	}
	else if (hWnd == m_Cab2.m_hWnd)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_Cab2.SetCurSel (idx);
		}
		m_Cab2.SetFocus ();
		return TRUE;
	}
	else if (hWnd == m_Cab3.m_hWnd)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_Cab3.SetCurSel (idx);
		}
		m_Cab3.SetFocus ();
		return TRUE;
	}
	else if (hWnd == m_CabNve1.m_hWnd)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_CabNve1.SetCurSel (idx);
		}
		m_CabNve1.SetFocus ();
		return TRUE;
	}
	else if (hWnd == m_CabNve2.m_hWnd)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_CabNve2.SetCurSel (idx);
		}
		m_CabNve2.SetFocus ();
		return TRUE;
	}
	return FALSE;
}

BOOL CPazPflegPageEx::TestCabF9 ()
{
	if (!UsePazCab) return FALSE;
	if (GetFocus () == &m_Cab)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_Cab.SetCurSel (idx);
		}
		m_Cab.SetFocus ();
		return TRUE;
	}
	else if (GetFocus () == &m_Cab1)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_Cab1.SetCurSel (idx);
		}
		m_Cab1.SetFocus ();
		return TRUE;
	}
	else if (GetFocus () == &m_Cab2)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_Cab2.SetCurSel (idx);
		}
		m_Cab2.SetFocus ();
		return TRUE;
	}
	else if (GetFocus () == &m_Cab3)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_Cab3.SetCurSel (idx);
		}
		m_Cab3.SetFocus ();
		return TRUE;
	}
	else if (GetFocus () == &m_CabNve1)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_CabNve1.SetCurSel (idx);
		}
		m_CabNve1.SetFocus ();
		return TRUE;
	}
	else if (GetFocus () == &m_CabNve2)
	{
		int idx = ChoicePazCab ();
		if (idx != -1)
		{
			m_CabNve2.SetCurSel (idx);
		}
		m_CabNve2.SetFocus ();
		return TRUE;
	}
	return FALSE;
}
