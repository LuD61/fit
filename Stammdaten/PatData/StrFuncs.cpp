#include "StdAfx.h"
#include ".\strfuncs.h"

CStrFuncs::CStrFuncs(void)
{
}

CStrFuncs::~CStrFuncs(void)
{
}

double CStrFuncs::StrToDouble (LPTSTR string)
{
 double fl;
 double nk;
 double ziffer;
 double teiler;
 short minus;

 if (string == NULL) return (double) 0.0;
 fl = 0.0;
 nk = 0.0;
 teiler = 10;
 minus = 1;
 while (*string < 0X30)
 {
  if (*string == 0)
    return (0.0);
  if (*string == '-')
    break;
  if (*string == '+')
    break;
  if (*string == '.')
    break;
  if (*string == ',')
    break;
  string ++;
 }

 if (*string == '-')
 {
  minus = -1;
  string ++;
 }
 else if (*string == '+')
 {
  string ++;
 }

 while (*string)
 {
  if (*string == '.')
  {
   break;
  }
  if (*string == ',')
  {
   break;
  }
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = (fl * teiler) + ziffer;
  string ++;
 }

 if (*string == '.');
 else if (*string == ',');
 else
 {
  fl *= minus;
  return (fl);
 }
 string ++;
 while (*string)
 {
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  nk = (double) ziffer / teiler;
  fl += nk;
  teiler *= 10;
  string ++;
 }
// fl += nk;
 return (fl);
}

double CStrFuncs::StrToDouble (CString& Text)
{
	return StrToDouble (Text.GetBuffer (0));
}

void CStrFuncs::SysDate (CString& Date)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 Date.Format (_T("%02d.%02d.%04d"), ltime->tm_mday,
                                    ltime->tm_mon + 1,
                                    ltime->tm_year + 1900);
}

void CStrFuncs::ToClipboard (CString& Text)
{
	CEdit clip;
	clip.Create (WS_CHILD, CRect (0, 0, 80, 10), NULL, 10001);
	clip.SetWindowText (Text.GetBuffer ());
	clip.SetSel (0, -1);
	clip.Copy ();
	clip.DestroyWindow ();
}

void CStrFuncs::FromClipboard (CString& Text)
{
	CEdit clip;
	clip.Create (WS_CHILD, CRect (0, 0, 80, 10), NULL, 10001);
	clip.Paste ();
	clip.GetWindowText (Text);
	clip.DestroyWindow ();
}

void CStrFuncs::Trim (LPSTR pos)
{
    LPSTR p = &pos[strlen (pos) - 1];
	for (; *p <= ' ' && p >= pos; p -= 1) *p = 0;
}
