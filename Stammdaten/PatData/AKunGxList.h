#ifndef _AKUNGX_LIST_DEF
#define _AKUNGX_LIST_DEF
#pragma once

class CAKunGxList
{
public:
	short mdn;
	CString kun_bran2;
	long kun;
	double a;
	CString a_bz1;

	CAKunGxList(void);
	CAKunGxList(short, CString&, long, double, CString&);
	CAKunGxList(short, LPTSTR, long, double, LPTSTR);
	~CAKunGxList(void);
};
#endif
