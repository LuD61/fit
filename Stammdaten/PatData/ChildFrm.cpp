// ChildFrm.cpp : Implementierung der Klasse CChildFrame
//
#include "stdafx.h"
#include "PatData.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "PazTexte.h"
#include "Etipfleg.h"
#include "PazView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RECCHANGE                       WM_USER + 2000

CVector CChildFrame::TextWnd;
CVector CChildFrame::EtiWnd;
CVector CChildFrame::DataWnd;

// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	ON_WM_SIZE ()
	ON_WM_DESTROY ()
//	ON_COMMAND(ID_TEXT_CENT, OnTextCent)
//	ON_COMMAND(ID_TEXT_RIGHT, OnTextRight)
//	ON_COMMAND(ID_TEXT_LEFT, OnTextLeft)
	ON_COMMAND (RECCHANGE, OnRecChange)
	ON_COMMAND (ID_DLG1, OnDlg1)
END_MESSAGE_MAP()


// CChildFrame Erstellung/Zerst�rung

CChildFrame::CChildFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
	InitSize = FALSE;
	TextWnd.Init ();
	EtiWnd.Init ();
	DataWnd.Init ();
}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::Create(LPCTSTR lpszClassName,LPCTSTR lpszWindowName,
						DWORD dwStyle, RECT& rect,CMDIFrameWnd* pParentWnd,
						CCreateContext* pContext)
{
	return CMDIChildWnd::Create (lpszClassName, lpszWindowName, dwStyle,
						   rect, pParentWnd, pContext);
}



BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie die Fensterklasse oder die Stile hier, indem Sie CREATESTRUCT �ndern
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	if (cs.cy > 0)
	{
		cs.cy = 1000;
	}
	return TRUE;
}

BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	lpcs->cy = 500;
	BOOL ret = CMDIChildWnd::OnCreateClient(lpcs,   pContext);	

	CStringA ClassName = pContext->m_pNewViewClass->m_lpszClassName;
	if (ClassName == "CPazTexte")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->TxtWnd == NULL)
		{
			MainFrm->TxtWnd = this;
		}
		else
		{
			TextWnd.Add (this);
		}
//		SetTitle (_T("Zutatentexte"));
	}
	else if (ClassName == "CEtiPfleg")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->EtiWnd == NULL)
		{
			MainFrm->EtiWnd = this;
		}
		else
		{
			EtiWnd.Add (this);
		}
//		SetTitle (_T("Etiketten"));
	}
	else if (ClassName == "CPazView")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->DataWnd == NULL)
		{
			MainFrm->DataWnd = this;
		}
		else
		{
			DataWnd.Add (this);
		}
//		SetTitle (_T("Etiketten"));
	}
//	MDIMaximize ();
	return ret;
}


void CChildFrame::OnSize(UINT nType, int cx, int cy)
{
	static BOOL InitPatView;

	CMDIChildWnd::OnSize (nType, cx, cy);
	if (!InitSize)
	{
		    CRect pRect;
			GetParent ()->GetClientRect (pRect);
			CRect rect;
			GetWindowRect (&rect);
			GetParent ()->ScreenToClient (&rect);
			rect.bottom = pRect.bottom;
			rect.right += 55;
			rect.top = 0;
			InitSize = TRUE;
			MoveWindow (&rect, TRUE);
	}
}

// CChildFrame Diagnose

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// CChildFrame Meldungshandler

void CChildFrame::OnTextCent()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    DbFormView *view = (DbFormView *) GetActiveView ();
	view->TextCent ();
}

void CChildFrame::OnTextRight()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    DbFormView *view = (DbFormView *) GetActiveView ();
	view->TextRight ();
}

void CChildFrame::OnTextLeft()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    DbFormView *view = (DbFormView *) GetActiveView ();
	view->TextLeft ();
}

void CChildFrame::OnDestroy()
{
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	if (MainFrm->TxtWnd == this)
	{
		MainFrm->TxtWnd = NULL;
		if (TextWnd.GetCount () != 0)
		{
			MainFrm->TxtWnd = (CWnd *) TextWnd.Get (0);
			TextWnd.Drop (0);
		}
		return;
	}
	else if (MainFrm->EtiWnd == this)
	{
		MainFrm->EtiWnd = NULL;
		if (EtiWnd.GetCount () != 0)
		{
			MainFrm->EtiWnd = (CWnd *) EtiWnd.Get (0);
			EtiWnd.Drop (0);
		}
		return;
	}
	else if (MainFrm->DataWnd == this)
	{
		MainFrm->DataWnd = NULL;
		if (DataWnd.GetCount () != 0)
		{
			MainFrm->DataWnd = (CWnd *) DataWnd.Get (0);
			DataWnd.Drop (0);
		}
		return;
	}
	int i = -1;
	if ((i = TextWnd.Find (this)) != -1)
	{
		TextWnd.Drop (i);
	}
	else if ((i = EtiWnd.Find (this)) != -1)
	{
		EtiWnd.Drop (i);
	}
	else if ((i = DataWnd.Find (this)) != -1)
	{
		DataWnd.Drop (i);
	}
}

void CChildFrame::OnDlg1 ()
{
	int eins = 1;
}

void CChildFrame::OnRecChange()
{
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	if (MainFrm->TxtWnd != NULL)
	{
		CPazTexte *view = (CPazTexte *) ((CChildFrame *) MainFrm->TxtWnd)->GetActiveView ();
		if (view != NULL)
		{
			view->OnRecChange ();
		}
	}
}
