#ifndef _ETIPFLEG_DEF
#define _ETIPFLEG_DEF
#pragma once
#include "CtrlGrid.h"
#include "DbFormView.h"
#include "afxwin.h"
#include "ListDropTarget.h"
#include "eti_typ.h"
#include "FormTab.h"
#include "EtiShow.h"
#include "EtiView.h"
#include "ChoiceEti.h"
#include "NumEdit.h"
#include "afxcmn.h"


//#define IDC_ETICHOICE                   1055

// CEtiPfleg-Formularansicht

class CEtiPfleg : public DbFormView
{
	DECLARE_DYNCREATE(CEtiPfleg)

protected:
	CEtiPfleg();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CEtiPfleg();

public:
	enum { IDD = IDD_ETIPFLEG };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
    virtual BOOL PreTranslateMessage(MSG*);
    afx_msg void OnSize(UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	afx_msg void OnDrawItem (int, LPDRAWITEMSTRUCT lpDRAWITEMSTRUCT);
    afx_msg void OnDestroy ();

	DECLARE_MESSAGE_MAP()

	BOOL NewRecord;
public:
	CStatic m_LEtiNr;
	CStatic m_LEtiBz;
	CStatic m_LStdSize;
	CStatic m_LStdZe;
	CNumEdit m_EtiNr;
	CEdit m_EtiBz;
	CNumEdit m_StdSize;
	CNumEdit m_StdZe;
	CButton m_EtiChoice;
	CEtiShow m_EtiShow;
	CVector HeadControls;

	CFrameWnd m_EtiShowEx;
//	CEtiView m_EtiShowEx;
 
    HBRUSH WhiteBrush;

	CFormTab Form;

	ETI_TYP_CLASS Eti_typ;
	CListDropTarget dropTarget;
	CFont Font;
	CFont lFont;

	CCtrlGrid CtrlGrid;
	CCtrlGrid EtiGrid;
	CCtrlGrid EtiBrGrid;
	CCtrlGrid EtiHoeGrid;
	CCtrlGrid SgGrid;

	CChoiceEti *Choice;
	BOOL ModalChoice;

	void Register ();
	CStatic m_Border;
	CButton m_SgBorder;
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
    virtual BOOL OnKeyup ();
	virtual BOOL OnReturn ();
    virtual BOOL Read ();
	afx_msg void OnStnClickedLzeStd();
	CStatic m_LEtiBr;
	CEdit m_EtiHoe;
	CStatic m_LEtiHoe;
	CEdit m_EtiBr;
	CStatic m_LSg;
	CStatic m_LZe;
	CNumEdit m_Sg[20];
	CNumEdit m_Ze[20];

	short *SgTab[20];
	short *ZeTab[20];

    void OnEtiChoice(); 
    void OnEtiSelected ();
    void OnEtiCanceled ();
	void write ();
	afx_msg void OnFileSave();
	CSpinButtonCtrl m_EtiBrSpin;
	CSpinButtonCtrl m_EtiHoeSpin;
	afx_msg void OnDeltaposEtiBrSpin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposEtiHoeSpin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEtiBr();
	afx_msg void OnEnKillfocusEtiHoe();
	afx_msg void OnDelete();
    void EnableHeadControls (BOOL enable);
    void StepBack ();
	afx_msg void OnBack();
	afx_msg void OnEnChangeEtiBr();
	afx_msg void OnEnChangeEtiHoe();
    void EnableFields (BOOL b);
};
#endif

