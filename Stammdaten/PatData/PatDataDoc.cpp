// PatDataDoc.cpp : Implementierung der Klasse CPatDataDoc
//

#include "stdafx.h"
#include "PatData.h"

#include "PatDataDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPatDataDoc

IMPLEMENT_DYNCREATE(CPatDataDoc, CDocument)

BEGIN_MESSAGE_MAP(CPatDataDoc, CDocument)
END_MESSAGE_MAP()


// CPatDataDoc Erstellung/Zerst�rung

CPatDataDoc::CPatDataDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CPatDataDoc::~CPatDataDoc()
{
}

BOOL CPatDataDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CPatDataDoc Serialisierung

void CPatDataDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CPatDataDoc Diagnose

#ifdef _DEBUG
void CPatDataDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPatDataDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CPatDataDoc-Befehle
