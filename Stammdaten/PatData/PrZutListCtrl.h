#pragma once
#include <vector>
#include "editlistctrl.h"
#include "SgTab.h"

#define MAXLISTROWS 30

class CPrZutListCtrl :
	public CEditListCtrl
{
public:
	CSgTab *SgTab;
	CVector SgComboValues;
	BOOL AllMarked;
	int oldsel;
	std::vector<BOOL> vSelect;
	CPrZutListCtrl(void);
	~CPrZutListCtrl(void);
	void FillSgCombo (CSgTab *);
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	void SetSgFromRow (CString&, int);
    void CentTxt (CSgTab&);
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	virtual void ClearSelect ();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnMarkAll();
    int GetMaxChars (int, int);
};
