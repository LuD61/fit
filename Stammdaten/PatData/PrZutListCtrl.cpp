#include "StdAfx.h"
#include "PrZutListctrl.h"
#include "przutlistctrl.h"
#include "resource.h"

CPrZutListCtrl::CPrZutListCtrl(void)
{
	AllMarked = FALSE;
	SgTab = NULL;
}

CPrZutListCtrl::~CPrZutListCtrl(void)
{
//	SgComboValues.DestroyAll ();

	SgComboValues.FirstPosition ();
	CString *C;
	while ((C = (CString *) SgComboValues.GetNext ()) != NULL)
	{
		delete C;
	}
	SgComboValues.Init ();
}

int CPrZutListCtrl::GetMaxChars (int col, int row)
{
	if (SgTab == NULL) return 0;

	CString Sg = GetItemText (row, 2);
	int pos = 0;
	Sg = Sg.Tokenize (_T("/"), pos);
	short sg = (short) _wtoi (Sg.GetBuffer ());
	CSg* cSg = SgTab->Get (sg);
	if (cSg != NULL)
	{
		return cSg->ze;
	}
	return 0;
}

void CPrZutListCtrl::StartEnter (int col, int row)
{
	if (col < 2) col = 2;
	if (col == 2)
	{

		if (IsWindow (ListComboBox.m_hWnd))
		{
			StopEnter ();
		}
		CEditListCtrl::StartEnterCombo (col, row, &SgComboValues);
		oldsel = ListComboBox.GetCurSel ();
	}
	else if (col == 3)
	{
		int ze = GetMaxChars (col, row);
		LimitText = (UINT) ze;
		CEditListCtrl::StartEnter (col, row);
	}
}

void CPrZutListCtrl::SetSgFromRow (CString &Text, int row)
{
	for (int i = row; i < GetItemCount (); i ++)
	{
		FillList.SetItemText (Text.GetBuffer (0), i, 2);
	}
}



void CPrZutListCtrl::StopEnter ()
{

//	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		ListComboBox.GetLBText (idx, Text);
		FormatText (Text);
		if (idx != oldsel)
		{
//			SetSgFromRow (Text, EditRow);
		}
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListComboBox.DestroyWindow ();
	}

}

void CPrZutListCtrl::SetSel (CString& Text)
{
   Text.TrimRight ();
   int cpos = Text.GetLength ();
   /*
   if (EditCol == 2)
   {
		ListEdit.SetSel (0, -1);
		return;
   }
   */
   if (LimitText != -1)
   {
	ListEdit.SetSel (cpos, LimitText);
	ListEdit.ReplaceSel (_T(""));
   }
   ListEdit.SetSel (cpos, cpos);
}

void CPrZutListCtrl::FormatText (CString& Text)
{
/*
    if (EditCol == 2)
	{
		Text.Format ("%.2lf", StrToDouble (Text));
	}
    else if (EditCol == 4)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
    else if (EditCol == 5)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
*/
}

void CPrZutListCtrl::NextRow ()
{
	int count = GetItemCount ();
	SetEditText ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	}
	StopEnter ();
	EditRow ++;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	if (GetKeyState (VK_SHIFT) < 0)
	{
		BOOL &b = vSelect[EditRow - 1];
		b = TRUE;
		FillList.SetItemImage (EditRow - 1, 1);
	}
	else if (GetKeyState (VK_CONTROL) == 0)
	{
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
	}
}

void CPrZutListCtrl::PriorRow ()
{
	int count = GetItemCount ();
	if (EditRow <= 0)
	{
		return;
	}
	StopEnter ();
/*
	if (EditRow == count - 1)
	{
		CString PrZut = GetItemText (EditRow, 2);
		if (StrToDouble (PrZut) == 0.0)
		{
	        DeleteItem (EditRow);
		}
	}
*/
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}

void CPrZutListCtrl::OnReturn ()
{
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1)
	{
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 2;
	}
	else
	{
	    StopEnter ();
		EditCol ++;
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CPrZutListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CPrZutListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 2)
	{
		return;
	}
	StopEnter ();
	EditCol --;
    StartEnter (EditCol, EditRow);
}

BOOL CPrZutListCtrl::InsertRow ()
{
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CString Lfd;
	Lfd = GetItemText (EditRow, 1);
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (Lfd.GetBuffer (0), EditRow, 1);
	FillList.SetItemText (_T("0.00"), EditRow, 2);
	FillList.SetItemText (_T(""), EditRow, 3);
	int rowCount = GetItemCount ();
	for (int i = EditRow + 1; i < rowCount; i ++)
	{
		Lfd.Format (_T("%d"), i + 1);
	    FillList.SetItemText (Lfd.GetBuffer (0), i, 1);
	}
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CPrZutListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	int rowCount = GetItemCount ();
	CString Lfd;
	for (int i = EditRow + 1; i < rowCount; i ++)
	{
		Lfd.Format (_T("%d"), i);
	    FillList.SetItemText (Lfd.GetBuffer (0), i, 1);
	}
	return CEditListCtrl::DeleteRow ();
}

BOOL CPrZutListCtrl::AppendEmpty ()
{
	return FALSE;
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (rowCount >= MAXLISTROWS - 1)
	CString PrZut = GetItemText (rowCount - 1, 2);
	FillList.InsertItem (rowCount, -1);
	CString Lfd;
	Lfd.Format (_T("%hd"), rowCount + 1);
	FillList.SetItemText (Lfd.GetBuffer (0), rowCount, 1);
	FillList.SetItemText (_T("0.00"), rowCount, 2);
	FillList.SetItemText (_T(""), rowCount, 3);
	return TRUE;
}


BEGIN_MESSAGE_MAP(CPrZutListCtrl, CEditListCtrl)
//	ON_COMMAND(ID_MARK_ALL, OnMarkAll)
END_MESSAGE_MAP()

void CPrZutListCtrl::OnMarkAll()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	StopEnter ();
	for (int i = 0; i < GetItemCount (); i ++)
    {
			   SetItemState (i, LVIS_SELECTED, LVIS_SELECTED);
	}
}

void CPrZutListCtrl::HiLightItem (int Item)
{
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);

/*
		CRect rRect;
        CRect cRect;  
	    GetSubItemRect (Item, 0, LVIR_BOUNDS , rRect);
		GetClientRect (&cRect);
		CDC *cDC = GetDC ();
		cRect.top = rRect.top;
		cRect.bottom = rRect.bottom;
		cDC->FillSolidRect (&cRect, RGB (0,0,255));
		ReleaseDC (cDC);
*/
}

void CPrZutListCtrl::ClearSelect ()
{

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
}

void CPrZutListCtrl::RunItemClicked (int Item)
{
//		vSelect.push_back (FALSE);
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
}

void CPrZutListCtrl::RunCtrlItemClicked (int Item)
{
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
}

void CPrZutListCtrl::RunShiftItemClicked (int Item)
{
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
}

void CPrZutListCtrl::CentTxt (CSgTab& SgTab)
{
		BOOL SaveAll = TRUE;
		for (int i = 0; i < MAXLISTROWS && i < GetItemCount (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				SaveAll = FALSE;
				break;
			}
		}
}

void CPrZutListCtrl::FillSgCombo (CSgTab *SgTab)
{
	SgComboValues.FirstPosition ();
	CString *C;
	while ((C = (CString *) SgComboValues.GetNext ()) != NULL)
	{
		delete C;
	}
	SgComboValues.Init ();
	this->SgTab = SgTab;
	SgComboValues.Init ();
	SgTab->FirstPosition ();
	CSg *Sg;
	while ((Sg = SgTab->GetNext ()) != NULL)
	{
		CString *cSg = new CString ();
		cSg->Format (_T("%hd/%hd"), Sg->sg, Sg->ze);
		SgComboValues.Add (cSg);
	}
}
