#pragma once
#include "afxcmn.h"
#include "CtrlGrid.h"
#include "afxwin.h"
#include "DbFormView.h"
#include "DbPropertySheet.h"
#include "PazPflegPage.h"
#include "PazPflegPageEx.h"



// CPazView-Formularansicht

class CPazView : public DbFormView
{
	DECLARE_DYNCREATE(CPazView)

protected:
	CPazView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPazView();

public:
	enum { IDD = IDD_PAZVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    afx_msg void OnSize(UINT, int, int);

	DECLARE_MESSAGE_MAP()
public:
	CTabCtrl m_Tab;
	CCtrlGrid CtrlGrid;
	CCtrlGrid DataGrid;
	CCtrlGrid EtiGrid;
	CStatic m_LMdn;
	CEdit m_Mdn;
	CDbPropertySheet *m_PropertySheet;
	CPazPflegPage *m_PazPflegPage;
	CPazPflegPageEx *m_PazPflegPageEx;
	HBRUSH hBrush;
	HBRUSH staticBrush;
	void DeletePropertySheet ();
	afx_msg void OnLanguage();
	afx_msg void OnFileSave();
	afx_msg void OnDelete();
	afx_msg void OnBack();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnTabcopy();
	afx_msg void OnTabpaste();
	afx_msg void OnTextCent();
	afx_msg void OnTextLeft();
	afx_msg void OnTextRight();
};


