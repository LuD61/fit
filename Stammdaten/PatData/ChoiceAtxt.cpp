#include "stdafx.h"
#include "ChoiceATxt.h"
#include "DbUniCode.h"
#include "Atext.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceAtxt::Sort1 = -1;
int CChoiceAtxt::Sort2 = -1;
int CChoiceAtxt::Sort3 = -1;
int CChoiceAtxt::Sort4 = -1;
int CChoiceAtxt::Sort5 = -1;
// std::vector<CABasList *> CChoiceTxt::ABasList;

CChoiceAtxt::CChoiceAtxt(CWnd* pParent) 
        : CChoiceX(pParent)
{
}

CChoiceAtxt::~CChoiceAtxt() 
{
	DestroyList ();
}

void CChoiceAtxt::DestroyList() 
{
	for (std::vector<CATextList *>::iterator pabl = ATextList.begin (); pabl != ATextList.end (); ++pabl)
	{
		CATextList *abl = *pabl;
		delete abl;
	}
    ATextList.clear ();
}

void CChoiceAtxt::FillList () 
{
    long  sys;
    long txt_nr;
    TCHAR txt [sizeof (atexte.txt)];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl allgeime Texte"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Text-Nr"),       1, 100, LVCFMT_RIGHT);
    SetCol (_T("Gerätenummer"),  2, 100, LVCFMT_RIGHT);
    SetCol (_T("Text"),          3, 400);

	if (ATextList.size () == 0)
	{
		DbClass->sqlout ((long *) &sys,      SQLLONG, 0);
		DbClass->sqlout ((long *) &txt_nr,   SQLLONG, 0);
		DbClass->sqlout ((LPTSTR) txt,       SQLCHAR, sizeof (txt));
		int cursor = DbClass->sqlcursor (_T("select sys, txt_nr, txt ")
			                             _T("from atexte where txt_nr > 0"));

		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) txt;
			CDbUniCode::DbToUniCode (txt, pos);
			CATextList *abl = new CATextList (sys, txt_nr, txt);
			ATextList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CATextList *>::iterator pabl = ATextList.begin (); pabl != ATextList.end (); ++pabl)
	{
		CATextList *abl = *pabl;
		CString Sys;
		CString TxtNr;
		CString Txt;
		Sys.Format (_T("%ld"), abl->sys); 
		TxtNr.Format (_T("%ld"), abl->txt_nr); 
		Txt = abl->txt;
        int ret = InsertItem (i, -1);
        ret = SetItemText (TxtNr.GetBuffer (), i, 1);
        ret = SetItemText (Sys.GetBuffer (), i, 2);
        ret = SetItemText (Txt.GetBuffer (), i, 3);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceAtxt::RefreshList () 
{
    long  sys;
    long txt_nr;
    TCHAR txt [sizeof (atexte.txt)];

	DestroyList ();
	CListCtrl *listView = GetListView ();
	listView->DeleteAllItems ();

    DWORD Style = SetStyle (LVS_REPORT);

    int i = 0;

	if (ATextList.size () == 0)
	{
		DbClass->sqlout ((long *) &sys,      SQLLONG, 0);
		DbClass->sqlout ((long *) &txt_nr,   SQLLONG, 0);
		DbClass->sqlout ((LPTSTR) txt,       SQLCHAR, sizeof (txt));
		int cursor = DbClass->sqlcursor (_T("select sys, txt_nr, txt ")
			                             _T("from atexte where txt_nr > 0"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) txt;
			CDbUniCode::DbToUniCode (txt, pos);
			CATextList *abl = new CATextList (sys, txt_nr, txt);
			ATextList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CATextList *>::iterator pabl = ATextList.begin (); pabl != ATextList.end (); ++pabl)
	{
		CATextList *abl = *pabl;
		CString Sys;
		CString TxtNr;
		CString Txt;
		Sys.Format (_T("%ld"), abl->sys); 
		TxtNr.Format (_T("%ld"), abl->txt_nr); 
		Txt = abl->txt;
        int ret = InsertItem (i, -1);
        ret = SetItemText (TxtNr.GetBuffer (), i, 1);
        ret = SetItemText (Sys.GetBuffer (), i, 2);
        ret = SetItemText (Txt.GetBuffer (), i, 3);
        i ++;
    }

    Sort (listView);
    Sort (listView);
}

void CChoiceAtxt::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAtxt::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAtxt::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAtxt::SearchEtiTyp (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceAtxt::SearchTxt1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAtxt::SearchTxt2 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 4);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceAtxt::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             SearchEtiTyp (ListBox, EditText.GetBuffer (4));
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchTxt1 (ListBox, EditText.GetBuffer ());
             break;
        case 4 :
             EditText.MakeUpper ();
             SearchTxt2 (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceAtxt::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceAtxt::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {
	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   return (li2 - li1) * Sort2;
   }
   else if (SortRow == 2)
   {
	   long li1 = _tstoi (strItem1.GetBuffer ());
	   long li2 = _tstoi (strItem2.GetBuffer ());
	   return (li2 - li1) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   else if (SortRow == 4)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort5;
   }
   return 0;
}


void CChoiceAtxt::Sort (CListCtrl *ListBox)
{
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
        case 3:
              Sort4 *= -1;
              break;
        case 4:
              Sort5 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CATextList *abl = ATextList [i];
		   
		   abl->txt_nr     = _tstol (ListBox->GetItemText (i, 1));
		   abl->sys        = _tstol (ListBox->GetItemText (i, 2));
		   abl->txt        = ListBox->GetItemText (i, 3);
	}
}

void CChoiceAtxt::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = ATextList [idx];
}

BOOL CChoiceAtxt::Equals (void *abl, int idx)
{
	if (((CATextList *) abl)->txt_nr != ((CATextList *) ATextList [idx])->txt_nr)
	{
		return FALSE;
	}
	return TRUE;
}

CATextList *CChoiceAtxt::GetSelectedText ()
{
	CATextList *abl = (CATextList *) SelectedRow;
	return abl;
}

