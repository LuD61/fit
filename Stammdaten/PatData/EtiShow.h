#ifndef _ETI_SHOW_DEV
#define _ETI_SHOW_DEV
#pragma once
#include "afxwin.h"
#include "eti_typ.h"

class CEtiShow :
	public CStatic
{
protected :

public:
	ETI_TYP *eti_typ;
	COLORREF Background;
	COLORREF EtiColor;
	int LogRows;
	int LogColumns;
	int EanStartHeight;
	int EanHeight;
	CEtiShow(void);
	~CEtiShow(void);
	virtual void DrawItem (LPDRAWITEMSTRUCT lpDRAWITEMSTRUCT);
    void PrintShadow (CDC&, CRect&);
    void PrintRoundedShadow (CDC&, CRect);
	void CreateFont (CFont *, int , CString&);
	void CreateFont (CFont *, int , CString&, int);
    int GetpY (CRect& rect, int row);
    int GetpYBottom (CRect& rect, int row);
    int GetpX (CRect& rect, int row);
    int GetpXRight (CRect& rect, int row);
    void PrintEan (CDC& cDC, CRect& rect, LPTSTR Ean, int Row, int Column);
};
#endif
