// ChildFrm.h : Schnittstelle der Klasse CChildFrame
//


#pragma once
#include "vector.h"

class CChildFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CChildFrame)
public:
	CChildFrame();

// Attribute
public:

// Operationen
public:

// Überschreibungen
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual BOOL OnCreateClient(LPCREATESTRUCT, CCreateContext*);
// Implementierung
public:
	virtual ~CChildFrame();
    virtual BOOL Create(LPCTSTR lpszClassName,LPCTSTR lpszWindowName,
						DWORD dwStyle, RECT& rect,CMDIFrameWnd* pParentWnd,
						CCreateContext* pContext);
    afx_msg void OnSize(UINT, int, int);
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
public:
	static CVector TextWnd;
	static CVector EtiWnd;
	static CVector DataWnd;

	BOOL InitSize;
	afx_msg void OnTextCent();
	afx_msg void OnTextRight();
	afx_msg void OnTextLeft();
	afx_msg void OnDestroy();
    afx_msg void OnDlg1 ();
	void OnRecChange();
};
