#pragma once
#include "auto_nr.h"

class CGenNr :
	public AUTO_NR_CLASS
{
private:
	int cursor1;
	int cursor2;
	int DelCursor;
	void Prepare ();
public:
	long min;
	long max;
	CGenNr(void);
	CGenNr(short, short, LPTSTR);
	~CGenNr(void);
	long NextNr ();
	void FreeNr (long);
	BOOL Create ();
	void Close ();
};
