#ifndef _PAZPFLEGPAGE_DEF
#define _PAZPFLEGPAGE_DEF
#pragma once
#include "resource.h"
#include "DbPropertyPage.h"
#include "ListDropTarget.h"
#include "CtrlGrid.h"
#include "A_kun_gx.h"
#include "A_bas.h"
#include "A_hndw.h"
#include "A_eig.h"
#include "A_eig_div.h"
#include "Pr_a_gx.h"
#include "Eti_typ.h"
#include "Kun.h"
#include "Mdn.h"
#include "Adr.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "FormTab.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "ChoiceA.h"
#include "ChoiceEti.h"
#include "AKunGxChoice.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "DbFormView.h"
#include "CodeProperties.h"
#include "ChoiceTxt.h"
#include "ChoiceATxt.h"
#include "GenNr.h"
#include "PazPflegPageEx.h"
#include "mo_progcfg.h"

// CPazPflegPage-Dialogfeld

class CPazPflegPage : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CPazPflegPage)

public:
	CPazPflegPage();
	virtual ~CPazPflegPage();

// Dialogfelddaten
	enum { IDD = IDD_PAZPFLEGPAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	PROG_CFG Cfg;

	virtual BOOL OnInitDialog();
public:
	enum
	{
		HNDW = 1,
		EIG = 2,
		EIG_DIV = 3
	} A_TYP;

	BOOL TaraReadOnly;
	BOOL WithFreiText;
	BOOL Write160;
	BOOL ReadPr;
	BOOL Use_atexte;
	A_KUN_GX_CLASS A_kun_gx;
	A_BAS_CLASS A_bas;
	A_HNDW_CLASS A_hndw;
	A_EIG_CLASS A_eig;
	A_EIG_DIV_CLASS A_eig_div;
	PR_A_GX_CLASS Pr_ausz_gx;
	ETI_TYP_CLASS Eti_typ;
	KUN_CLASS Kun;
	ADR_CLASS KunAdr;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	PTABN_CLASS Ptabn;
	CGenNr *GenTextNr;
	CPazPflegPageEx *PazPflegPageEx;

	CFormTab Keys;
	CFormTab Form;
	CFormTab EtiForm;

	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid KunGrid;
	CCtrlGrid AGrid;
	CCtrlGrid EtiGrid;
	CCtrlGrid TxtNrGrid;
	CCtrlGrid KopfTextGrid;
	CCtrlGrid MhdTextGrid;
	CCtrlGrid FreiText1Grid;
	CCtrlGrid FreiText2Grid;
	CCtrlGrid FreiText3Grid;
	CCtrlGrid FreiText4Grid;
	CCtrlGrid FreiText5Grid;
	CCtrlGrid FreiText6Grid;
	CCtrlGrid FreiText7Grid;

	CListDropTarget dropTarget;

    CPropertySheet *PazProperty;
    DbFormView *View;
	CFrameWnd *Frame;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	CButton m_TxtNrChoice;
	CButton m_KopfTextChoice;
	CButton m_MhdTextChoice;
	CButton m_FreiText1Choice;
	CButton m_FreiText2Choice;
	CButton m_FreiText3Choice;
	CButton m_FreiText4Choice;
	CButton m_FreiText5Choice;
	CButton m_FreiText6Choice;
	CButton m_FreiText7Choice;
	void Register ();
	CEdit m_MdnName;
	CStatic m_LKunBran2;
	CComboBoxEx m_KunBran2;
	CStatic m_LKun;
	CNumEdit m_Kun;
	CButton m_KunChoice;
	CEdit m_KunName;
	CStatic m_HeadBorder;
	CStatic m_DataBorder;
	CStatic m_LA;
	CNumEdit m_A;
	CButton m_AChoice;
	CEdit m_ABz1;
    CVector HeadControls;

	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceKun *ChoiceKun;
	BOOL ModalChoiceKun;
	CChoiceA *ChoiceA;
	BOOL ModalChoiceA;
	CChoiceEti *ChoiceEti;
	BOOL ModalChoiceEti;
	CAKunGxChoice *Choice;
	BOOL ModalChoice;
	CChoiceTxt *ChoiceTxt;
	CChoiceAtxt *ChoiceATxt;
	BOOL ModalChoiceTxt;
	CCodeProperties Code;

    BOOL OnReturn ();
    BOOL OnKeyup ();
	BOOL Read ();
	void write ();
	void OnDelete ();
    void OnAchoice(); 
    void OnMdnchoice(); 
    void OnKunchoice(); 
    void OnTextchoice ();
    void OnKopfTextchoice ();
    void OnMhdTextchoice ();
    void FillKunBran2 ();
    void FillEtikett ();
    void FillMeEinhKun ();
	CStatic m_LSg;
	CStatic m_LPazBz;
	CComboBox m_Sg1;
	CTextEdit m_PazBz1;
	CComboBox m_Sg2;
	CTextEdit m_PazBz2;
	CStatic m_LEtiNr;
	CNumEdit m_EtiNr;
	CEdit m_EtiBz;
	CButton m_EtiChoice;
	CButton m_AKunGxChoice;

	int BranCursor;
	long BranKun;
	static HANDLE PriceLib;
    int (*preise_holen)(short dmdn, short dfil, 
		                        short dkun_fil, 
						        int dkun, double da, char *ddatum, 
						        short *sa, double *pr_ek, double *pr_vk);

	int (*SetPriceDbName) (char *);
	int (*SetPriceDb) (HENV, HDBC);

	static HANDLE Write160Lib;
    int (*dbild160)(LPSTR); 
    BOOL (*dOpenDbase)(LPSTR); 

	short *SgTab[20];
	short *ZeTab[20];

	long FirstEti;
	long text_nr;
	long text_nr2;
	int TextNrCursor;

    void OnEtiChoice ();
    void OnAKunGxSelected ();
    void OnAKunGxCanceled ();
    BOOL ReadEti ();
    BOOL ReadMdn ();
	afx_msg void OnEnKillfocusEtinr();
    void FillSgCombo ();
    void SetSgComboPos ();
    void GetFirstEti ();
    afx_msg void OnAKunGxChoice (); 
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	CStatic m_LAKun;
	CTextEdit m_AKun;
	CComboBox m_Sg3;
	CStatic m_LMeEinhKun;
	CComboBox m_MeEinhKun;
	CStatic m_LInh;
	CDecimalEdit m_Inh;
	CStatic m_LTara;
	CDecimalEdit m_Tara;
	CStatic m_LZutGew;
	CDecimalEdit m_ZutGew;
	CStatic m_LZutProz;
	CDecimalEdit m_ZutProz;
	CStatic m_LHbkZtr;
	CNumEdit m_HbkZtr;
    void DefaultMhd ();
	double DefaultTara ();
	CStatic m_TextNumbers;
	CStatic m_LPrZutTxt;
	CNumEdit m_PrZutTxt;
	CStatic m_LKopfTxt;
	CNumEdit m_KopfTxt;
	CStatic m_LMhdTxt;
	CNumEdit m_MhdTxt;
	CStatic m_LGenTxt;
	CNumEdit m_GenTxt;
	void CreateTextNr ();
	BOOL TextNrExist (long);
    void EnableHeadControls (BOOL enable);
    BOOL StepBack ();
	virtual void OnChoice ();
	afx_msg void OnEnKillfocusPrZutTxt();
	afx_msg void OnEnChangePrZutTxt();
	CEdit m_ABz2;
    virtual void OnCopy ();
    virtual void OnPaste ();
	BOOL Copy ();
	BOOL Paste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
    void EnableFields (BOOL b);
	afx_msg void OnTextCent();
	afx_msg void OnTextLeft();
	afx_msg void OnTextRight();
	afx_msg void OnKillFocus (CWnd *);
	afx_msg void OnEnKillfocusZutgew();
	afx_msg void OnEnKillfocusZutproz();
	CStatic m_LFreiText1;
	CNumEdit m_FreiText1;
	CNumEdit m_FreiText2;
	CNumEdit m_FreiText3;
	CNumEdit m_FreiText4;
	CNumEdit m_FreiText5;
	void *OnFreitextchoice ();
	void *OnFreitextchoiceAtexte ();
	void OnFreitext1choice ();
	void OnFreitext2choice ();
	void OnFreitext3choice ();
	void OnFreitext4choice ();
	void OnFreitext5choice ();
	void OnFreitext6choice ();
	void OnFreitext7choice ();
	void SetFreiTextMode ();
	CStatic m_LFreiText2;
	CNumEdit m_FreiText6;
	CNumEdit m_FreiText7;
	CStatic m_LFreiText3;
	CStatic m_LFreiText4;
	CStatic m_LFreiText5;
	CStatic m_LFreiText6;
	CStatic m_LFreiText7;

	short DefaultMdn;
	short DefaultEti;
	CComboBox m_etikett;
};
#endif