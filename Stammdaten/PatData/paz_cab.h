#ifndef _PAZ_CAB_DEF
#define _PAZ_CAB_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct PAZ_CAB {
   short          cabnum;
   TCHAR          cab[201];
   TCHAR          cabinfo[201];
};
extern struct PAZ_CAB paz_cab, paz_cab_null;

#line 8 "paz_cab.rh"

class PAZ_CAB_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PAZ_CAB paz_cab;  
               PAZ_CAB_CLASS () : DB_CLASS ()
               {
               }
};
#endif
