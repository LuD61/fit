#include "StdAfx.h"
#include "choiceeti.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceEti::Sort1 = -1;
int CChoiceEti::Sort2 = -1;
int CChoiceEti::Sort3 = -1;

CChoiceEti::CChoiceEti(CWnd* pParent)
        : CChoiceX(pParent)
{
}

CChoiceEti::~CChoiceEti(void)
{
	DestroyList ();
}

void CChoiceEti::DestroyList() 
{
	for (std::vector<CEtiList *>::iterator pabl = EtiList.begin (); pabl != EtiList.end (); ++pabl)
	{
		CEtiList *abl = *pabl;
		delete abl;
	}
    EtiList.clear ();
}

void CChoiceEti::FillList () 
{
    long eti;
	TCHAR eti_bz [200];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Etiketten"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Etikett"),      1, 150, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),  2, 250, LVCFMT_LEFT);

	if (EtiList.size () == 0)
	{
		DbClass->sqlout ((long *) &eti,  SQLLONG, 0);
		DbClass->sqlout ((LPTSTR)  eti_bz,     SQLCHAR, sizeof (eti_bz));
		int cursor = DbClass->sqlcursor (_T("select eti, eti_bz from eti_typ where eti > 0"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) eti_bz;
			CDbUniCode::DbToUniCode (eti_bz, pos);
			CEtiList *abl = new CEtiList (eti, eti_bz);
			EtiList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CEtiList *>::iterator pabl = EtiList.begin (); pabl != EtiList.end (); ++pabl)
	{
		CEtiList *abl = *pabl;
		CString EtiNr;
		EtiNr.Format (_T("%ld"), abl->eti); 
		CString Num;
		CString Bez;
		_tcscpy (eti_bz, abl->eti_bz.GetBuffer ());
		CString LText;
		LText.Format (_T("%ld %s"), abl->eti, 
									abl->eti_bz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = EtiNr;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (eti_bz, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort (listView);
}

void CChoiceEti::RefreshList () 
{
    long eti;
	TCHAR eti_bz [200];

	DestroyList ();
	CListCtrl *listView = GetListView ();
	listView->DeleteAllItems ();

    DWORD Style = SetStyle (LVS_REPORT);

    int i = 0;

	if (EtiList.size () == 0)
	{
		DbClass->sqlout ((long *) &eti,  SQLLONG, 0);
		DbClass->sqlout ((LPTSTR)  eti_bz,     SQLCHAR, sizeof (eti_bz));
		int cursor = DbClass->sqlcursor (_T("select eti, eti_bz from eti_typ where eti > 0"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) eti_bz;
			CDbUniCode::DbToUniCode (eti_bz, pos);
			CEtiList *abl = new CEtiList (eti, eti_bz);
			EtiList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CEtiList *>::iterator pabl = EtiList.begin (); pabl != EtiList.end (); ++pabl)
	{
		CEtiList *abl = *pabl;
		CString EtiNr;
		EtiNr.Format (_T("%ld"), abl->eti); 
		CString Num;
		CString Bez;
		_tcscpy (eti_bz, abl->eti_bz.GetBuffer ());
		CString LText;
		LText.Format (_T("%ld %s"), abl->eti, 
									abl->eti_bz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = EtiNr;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (eti_bz, i, 2);
        i ++;
    }

    Sort (listView);
    Sort (listView);
}


void CChoiceEti::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceEti::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceEti::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceEti::SearchBez (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceEti::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchBez (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceEti::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceEti::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {
	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   return (li2 - li1) * Sort2;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   return 0;
}


void CChoiceEti::Sort (CListCtrl *ListBox)
{
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CEtiList *abl = EtiList [i];
		   
		   abl->eti = _tstol (ListBox->GetItemText (i, 1));
		   abl->eti_bz = ListBox->GetItemText (i, 2);
	}
}

void CChoiceEti::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = EtiList [idx];
}

CEtiList *CChoiceEti::GetSelectedText ()
{
	CEtiList *abl = (CEtiList *) SelectedRow;
	return abl;
}

