#include "stdafx.h"
#include "ChoicePazCab.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoicePazCab::Sort1 = -1;
int CChoicePazCab::Sort2 = -1;
int CChoicePazCab::Sort3 = -1;
int CChoicePazCab::Sort4 = -1;
int CChoicePazCab::Sort5 = -1;
// std::vector<CABasList *> CChoicePazCab::ABasList;

CChoicePazCab::CChoicePazCab(CWnd* pParent) 
        : CChoiceX(pParent)
{
}

CChoicePazCab::~CChoicePazCab() 
{
	DestroyList ();
}

void CChoicePazCab::DestroyList() 
{
	for (std::vector<CPazCabList *>::iterator pabl = PazCabList.begin (); pabl != PazCabList.end (); ++pabl)
	{
		CPazCabList *abl = *pabl;
		delete abl;
	}
    PazCabList.clear ();
}

void CChoicePazCab::FillList () 
{
    short cabnum;
    TCHAR cab [402];
    TCHAR cabinfo [402];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl CAB"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("CAB-Nr "),   1, 150, LVCFMT_RIGHT);
    SetCol (_T("CabInfo"),  2, 1500);

	if (PazCabList.size () == 0)
	{
		DbClass->sqlout ((short *) &cabnum,   SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  cab,       SQLCHAR, 201);
		DbClass->sqlout ((LPTSTR)  cabinfo,   SQLCHAR, 201);
		int cursor = DbClass->sqlcursor (_T("select cabnum, cab, cabinfo from paz_cab where cabnum > -1 order by cabnum"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			cab[200] = 0;
			cabinfo[200] = 0;
			LPSTR pos = (LPSTR) cab;
			LPSTR p = &pos[strlen (pos) - 1];
			for (; *p <= ' ' && p >= pos; p -= 1) *p = 0;
			CDbUniCode::DbToUniCode (cab, pos);
			pos = (LPSTR) cabinfo;
			p = &pos[strlen (pos) - 1];
			for (; *p <= ' ' && p >= pos; p -= 1) *p = 0;
			CDbUniCode::DbToUniCode (cabinfo, pos);
			CPazCabList *abl = new CPazCabList (cabnum, cab, cabinfo);
			PazCabList.push_back (abl);
			i ++;
		}
		DbClass->sqlclose (cursor);
	}

    i = 0;
	for (std::vector<CPazCabList *>::iterator pabl = PazCabList.begin (); pabl != PazCabList.end (); ++pabl)
	{
		CPazCabList *abl = *pabl;
		CString CabNum;
		CabNum.Format (_T("%hd"), abl->cabnum); 
		CString Num;
		CString Bez;
		_tcscpy (cab, abl->cab.GetBuffer ());
		_tcscpy (cabinfo, abl->cabinfo.GetBuffer ());

		CString LText;
		LText.Format (_T("%hd %s"), abl->cabnum, 
									abl->cabinfo.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = CabNum;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (cabinfo, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
//    Sort (listView);
}

void CChoicePazCab::RefreshList () 
{
    short cabnum;
    TCHAR cab [402];
    TCHAR cabinfo [402];

	DestroyList ();
	CListCtrl *listView = GetListView ();
	listView->DeleteAllItems ();

    DWORD Style = SetStyle (LVS_REPORT);

    int i = 0;

	if (PazCabList.size () == 0)
	{
		DbClass->sqlout ((short *) &cabnum,   SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  cab,       SQLCHAR, sizeof (cab));
		DbClass->sqlout ((LPTSTR)  cabinfo,   SQLCHAR, sizeof (cabinfo));
		int cursor = DbClass->sqlcursor (_T("select cabnum, cab, cabinfo from paz_cab where cabnum > 0"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			cab[200] = 0;
			cabinfo[200] = 0;
			LPSTR pos = (LPSTR) cab;
			LPSTR p = &pos[strlen (pos) - 1];
			for (; *p <= ' ' && p >= pos; p -= 1) *p = 0;
			CDbUniCode::DbToUniCode (cab, pos);
			pos = (LPSTR) cabinfo;
		    p = &pos[strlen (pos) - 1];
			for (; *p <= ' ' && p >= pos; p -= 1) *p = 0;
			CDbUniCode::DbToUniCode (cabinfo, pos);
			CPazCabList *abl = new CPazCabList (cabnum, cab, cabinfo);
			PazCabList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CPazCabList *>::iterator pabl = PazCabList.begin (); pabl != PazCabList.end (); ++pabl)
	{
		CPazCabList *abl = *pabl;
		CString CabNum;
		CabNum.Format (_T("%hd"), abl->cabnum); 
		CString Num;
		CString Bez;
		_tcscpy (cab, abl->cab.GetBuffer ());
		_tcscpy (cabinfo, abl->cabinfo.GetBuffer ());

		CString LText;
		LText.Format (_T("%hd %s"), abl->cabnum, 
								   abl->cabinfo.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = CabNum;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (cabinfo, i, 2);
        i ++;
    }

    Sort (listView);
    Sort (listView);
}

void CChoicePazCab::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoicePazCab::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoicePazCab::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoicePazCab::SearchTxt1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoicePazCab::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchTxt1 (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoicePazCab::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoicePazCab::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {
	   long li1 = _tstoi (strItem1.GetBuffer ());
	   long li2 = _tstoi (strItem2.GetBuffer ());
	   return (li2 - li1) * Sort3;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}


void CChoicePazCab::Sort (CListCtrl *ListBox)
{
	if (NoSort)
	{
		return;
	}
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CPazCabList *abl = PazCabList [i];
		   
		   abl->cabnum = _tstoi (ListBox->GetItemText (i, 1));
		   abl->cabinfo = ListBox->GetItemText (i, 2);
		   abl->cab     = ListBox->GetItemText (i, 2);
	}
}

void CChoicePazCab::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = PazCabList [idx];
}

BOOL CChoicePazCab::Equals (void *abl, int idx)
{
	if (((CPazCabList *) abl)->cabnum != ((CPazCabList *) PazCabList [idx])->cabnum)
	{
		return FALSE;
	}
	return TRUE;
}

CPazCabList *CChoicePazCab::GetSelectedText ()
{
	CPazCabList *abl = (CPazCabList *) SelectedRow;
	return abl;
}

