// PatDataView.cpp : Implementierung der Klasse CPatDataView
//

#include "stdafx.h"
#include "PatData.h"

#include "PatDataDoc.h"
#include "PatDataView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPatDataView

IMPLEMENT_DYNCREATE(CPatDataView, CFormView)

BEGIN_MESSAGE_MAP(CPatDataView, CFormView)
	// Standarddruckbefehle
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
//	ON_EN_CHANGE(IDC_EDIT1, OnEnChangeEdit1)
END_MESSAGE_MAP()

// CPatDataView Erstellung/Zerst�rung

CPatDataView::CPatDataView()
	: CFormView(CPatDataView::IDD)
{
	// TODO: Hier Code zum Erstellen einf�gen

}

CPatDataView::~CPatDataView()
{
}

BOOL CPatDataView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CFormView::PreCreateWindow(cs);
}

// CPatDataView-Zeichnung
/*
void CPatDataView::OnDraw(CDC* pDC)
{
	CPatDataDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

}
*/

// CPatDataView drucken

BOOL CPatDataView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void CPatDataView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void CPatDataView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// CPatDataView Diagnose

#ifdef _DEBUG
void CPatDataView::AssertValid() const
{
	CFormView::AssertValid();
}

void CPatDataView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CPatDataDoc* CPatDataView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPatDataDoc)));
	return (CPatDataDoc*)m_pDocument;
}
#endif //_DEBUG


// CPatDataView Meldungshandler

void CPatDataView::OnEnChangeEdit1()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CFormView::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
}
