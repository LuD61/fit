#ifndef _CHOICEPAZCAB_DEF
#define _CHOICEPAZCAB_DEF

#include "ChoiceX.h"
#include "PazCabList.h"
#include <vector>

class CChoicePazCab : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
      
    public :
	    std::vector<CPazCabList *> PazCabList;
      	CChoicePazCab(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoicePazCab(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        virtual void RefreshList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchEtiTyp (CListCtrl *,  LPTSTR);
        void SearchTxt1 (CListCtrl *, LPTSTR);
        void SearchTxt2 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		virtual BOOL Equals (void *ListEntry, int idx);
		CPazCabList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
