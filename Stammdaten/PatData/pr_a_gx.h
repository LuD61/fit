#ifndef _PR_A_GX_DEF
#define _PR_A_GX_DEF

#include "dbclass.h"

struct PR_AUSZ_GX {
   long           text_nr;
   short          eti_typ;
   short          sg1;
   TCHAR          txt1[100];
   short          sg2;
   TCHAR          txt2[100];
   short          sg3;
   TCHAR          txt3[100];
   short          sg4;
   TCHAR          txt4[100];
   short          sg5;
   TCHAR          txt5[100];
   short          sg6;
   TCHAR          txt6[100];
   short          sg7;
   TCHAR          txt7[100];
   short          sg8;
   TCHAR          txt8[100];
   short          sg9;
   TCHAR          txt9[100];
   short          sg10;
   TCHAR          txt10[100];
   short          sg11;
   TCHAR          txt11[100];
   short          sg12;
   TCHAR          txt12[100];
   short          sg13;
   TCHAR          txt13[100];
   short          sg14;
   TCHAR          txt14[100];
   short          sg15;
   TCHAR          txt15[100];
   short          sg16;
   TCHAR          txt16[100];
   short          sg17;
   TCHAR          txt17[100];
   short          sg18;
   TCHAR          txt18[100];
   short          sg19;
   TCHAR          txt19[100];
   short          sg20;
   TCHAR          txt20[100];
};
extern struct PR_AUSZ_GX pr_ausz_gx, pr_ausz_gx_null;

#line 7 "pr_a_gx.rh"

class PR_A_GX_CLASS : public DB_CLASS 
{
       private :
               virtual void prepare ();

       public :
               PR_AUSZ_GX pr_ausz_gx;  
               PR_A_GX_CLASS (): DB_CLASS ()
               {
	       }
};
#endif