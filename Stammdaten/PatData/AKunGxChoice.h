#ifndef _CHOICEAKUNGX_DEF
#define _CHOICEAKUNGX_DEF

#include "ChoiceX.h"
#include "AKunGxList.h"
#include <vector>

class  CAKunGxChoice : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
      
    public :
		CString B;
		short m_Mdn;
		CString m_KunBran2; 
		long m_Kun;

	    std::vector<CAKunGxList *> AKunGxList;
      	CAKunGxChoice(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CAKunGxChoice(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        virtual void RefreshList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchKun (CListCtrl *,  LPTSTR);
        void SearchKunBran2 (CListCtrl *, LPTSTR);
        void SearchA (CListCtrl *, LPTSTR);
        void SearchABz1 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CAKunGxList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		CString& ShortToString (short);
		CString& LongToString (long);
		CString& DoubleToString (double, LPCTSTR);
};
#endif
