#pragma once
#include "vector.h"
#include "SgInfo.h"
#include "SgTab.h"

class CSgInfos :
	public CVector
{
public:
	CSgInfos(void);
	~CSgInfos(void);
	void Add (CSgInfo *);
    CSgInfo *GetNext ();
	CSgInfo *Get (int);
	int GetZe (int);
	void Clone (CSgTab&); 
	int FindSg (int sg);
};
