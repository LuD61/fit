#include "stdafx.h"
#include "ChoiceTxt.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceTxt::Sort1 = -1;
int CChoiceTxt::Sort2 = -1;
int CChoiceTxt::Sort3 = -1;
int CChoiceTxt::Sort4 = -1;
int CChoiceTxt::Sort5 = -1;
// std::vector<CABasList *> CChoiceTxt::ABasList;

CChoiceTxt::CChoiceTxt(CWnd* pParent) 
        : CChoiceX(pParent)
{
}

CChoiceTxt::~CChoiceTxt() 
{
	DestroyList ();
}

void CChoiceTxt::DestroyList() 
{
	for (std::vector<CPrAuszList *>::iterator pabl = PrAuszList.begin (); pabl != PrAuszList.end (); ++pabl)
	{
		CPrAuszList *abl = *pabl;
		delete abl;
	}
    PrAuszList.clear ();
}

void CChoiceTxt::FillList () 
{
    long  text_nr;
    short eti_typ;
    TCHAR txt1 [200];
    TCHAR txt2 [200];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Texte"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Text-Nr"),      1, 150, LVCFMT_RIGHT);
    SetCol (_T("Etikett"),      2, 100, LVCFMT_RIGHT);
    SetCol (_T("Textzeile 1"),  3, 250);
    SetCol (_T("Textzeile 2"),  4, 250);

	if (PrAuszList.size () == 0)
	{
		DbClass->sqlout ((long *)  &text_nr,   SQLLONG, 0);
		DbClass->sqlout ((short *) &eti_typ,   SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  txt1,     SQLCHAR, 100);
		DbClass->sqlout ((LPTSTR)  txt2,     SQLCHAR, 100);
		int cursor = DbClass->sqlcursor (_T("select text_nr, eti_typ, txt1, txt2 from pr_ausz_gx where text_nr > 0 order by text_nr, eti_typ"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			txt1[99] = 0;
			txt2[99] = 0;
			LPSTR pos = (LPSTR) txt1;
			LPSTR p = &pos[strlen (pos) - 1];
			for (; *p <= ' ' && p >= pos; p -= 1) *p = 0;
			CDbUniCode::DbToUniCode (txt1, pos);
			pos = (LPSTR) txt2;
			p = &pos[strlen (pos) - 1];
			for (; *p <= ' ' && p >= pos; p -= 1) *p = 0;
			CDbUniCode::DbToUniCode (txt2, pos);
			CPrAuszList *abl = new CPrAuszList (text_nr, eti_typ, txt1, txt2);
			PrAuszList.push_back (abl);
			i ++;
		}
		DbClass->sqlclose (cursor);
	}

    i = 0;
	for (std::vector<CPrAuszList *>::iterator pabl = PrAuszList.begin (); pabl != PrAuszList.end (); ++pabl)
	{
		CPrAuszList *abl = *pabl;
		CString TxtNr;
		TxtNr.Format (_T("%ld"), abl->txt_nr); 
		CString EtiTyp;
		EtiTyp.Format (_T("%hd"), abl->eti_typ); 
		CString Num;
		CString Bez;
		_tcscpy (txt1, abl->txt1.GetBuffer ());
		_tcscpy (txt2, abl->txt2.GetBuffer ());

		CString LText;
		LText.Format (_T("%ld %hd %s"), abl->txt_nr, 
										abl->eti_typ,
										abl->txt1.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = TxtNr;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (EtiTyp.GetBuffer (), i, 2);
        ret = SetItemText (txt1, i, 3);
        ret = SetItemText (txt2, i, 4);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
//    Sort (listView);
}

void CChoiceTxt::RefreshList () 
{
    long  text_nr;
    short eti_typ;
    TCHAR txt1 [200];
    TCHAR txt2 [200];

	DestroyList ();
	CListCtrl *listView = GetListView ();
	listView->DeleteAllItems ();

    DWORD Style = SetStyle (LVS_REPORT);

    int i = 0;

	if (PrAuszList.size () == 0)
	{
		DbClass->sqlout ((long *)  &text_nr,   SQLLONG, 0);
		DbClass->sqlout ((short *) &eti_typ,   SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  txt1,     SQLCHAR, sizeof (txt1));
		DbClass->sqlout ((LPTSTR)  txt2,     SQLCHAR, sizeof (txt2));
		int cursor = DbClass->sqlcursor (_T("select text_nr, eti_typ, txt1, txt2 from pr_ausz_gx where text_nr > 0"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			txt1[99] = 0;
			txt2[99] = 0;
			LPSTR pos = (LPSTR) txt1;
			LPSTR p = &pos[strlen (pos) - 1];
			for (; *p <= ' ' && p >= pos; p -= 1) *p = 0;
			CDbUniCode::DbToUniCode (txt1, pos);
			pos = (LPSTR) txt2;
		    p = &pos[strlen (pos) - 1];
			for (; *p <= ' ' && p >= pos; p -= 1) *p = 0;
			CDbUniCode::DbToUniCode (txt2, pos);
			CPrAuszList *abl = new CPrAuszList (text_nr, eti_typ, txt1, txt2);
			PrAuszList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CPrAuszList *>::iterator pabl = PrAuszList.begin (); pabl != PrAuszList.end (); ++pabl)
	{
		CPrAuszList *abl = *pabl;
		CString TxtNr;
		TxtNr.Format (_T("%ld"), abl->txt_nr); 
		CString EtiTyp;
		EtiTyp.Format (_T("%hd"), abl->eti_typ); 
		CString Num;
		CString Bez;
		_tcscpy (txt1, abl->txt1.GetBuffer ());
		_tcscpy (txt2, abl->txt2.GetBuffer ());

		CString LText;
		LText.Format (_T("%ld %hd %s"), abl->txt_nr, 
										abl->eti_typ,
										abl->txt1.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = TxtNr;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (EtiTyp.GetBuffer (), i, 2);
        ret = SetItemText (txt1, i, 3);
        ret = SetItemText (txt2, i, 4);
        i ++;
    }

    Sort (listView);
    Sort (listView);
}

void CChoiceTxt::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceTxt::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceTxt::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceTxt::SearchEtiTyp (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceTxt::SearchTxt1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceTxt::SearchTxt2 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 4);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceTxt::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             SearchEtiTyp (ListBox, EditText.GetBuffer (4));
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchTxt1 (ListBox, EditText.GetBuffer ());
             break;
        case 4 :
             EditText.MakeUpper ();
             SearchTxt2 (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceTxt::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceTxt::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {
	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   return (li2 - li1) * Sort2;
   }
   else if (SortRow == 2)
   {
	   long li1 = _tstoi (strItem1.GetBuffer ());
	   long li2 = _tstoi (strItem2.GetBuffer ());
	   return (li2 - li1) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   else if (SortRow == 4)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort5;
   }
   return 0;
}


void CChoiceTxt::Sort (CListCtrl *ListBox)
{
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
        case 3:
              Sort4 *= -1;
              break;
        case 4:
              Sort5 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CPrAuszList *abl = PrAuszList [i];
		   
		   abl->txt_nr = _tstol (ListBox->GetItemText (i, 1));
		   abl->eti_typ = _tstoi (ListBox->GetItemText (i, 2));
		   abl->txt1 = ListBox->GetItemText (i, 3);
		   abl->txt2 = ListBox->GetItemText (i, 4);
	}
}

void CChoiceTxt::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = PrAuszList [idx];
}

BOOL CChoiceTxt::Equals (void *abl, int idx)
{
	if (((CPrAuszList *) abl)->txt_nr != ((CPrAuszList *) PrAuszList [idx])->txt_nr)
	{
		return FALSE;
	}
	return TRUE;
}

CPrAuszList *CChoiceTxt::GetSelectedText ()
{
	CPrAuszList *abl = (CPrAuszList *) SelectedRow;
	return abl;
}

