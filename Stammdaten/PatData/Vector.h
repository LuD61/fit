#ifndef _VECTOR_DEF
#define _VECTOR_DEF

class CVector
{
    private :

       void **Arr;
       int anz;
       int pos;
    public :
       int GetAnz ()
       {
           return anz;
       }

       int GetCount ()
       {
           return anz;
       }

       CVector ();
       CVector (void **Object);
       ~CVector ();
       void Init (void);
       void SetObject (void **Object);
	   void **GetObject ();
       void DestroyAll ();
       void Add (void *);
       void SetPosition (int);
       void FirstPosition (void);
       void *Get (int);
       void *GetNext (int);
       void *GetNext (void);
       BOOL Drop (int);
       BOOL Drop (void *);
       BOOL Find (void *);

	   const CVector& operator= (CVector &);
};
#endif
