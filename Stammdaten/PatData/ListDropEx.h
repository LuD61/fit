#ifndef _CLISTDROPEX_DEF
#define _CLISTDROPEX_DEF
#include "afxole.h"
#include "ListDropTarget.h"
#include "DbFormView.h"

class CListDropEx : public CListDropTarget
{
private :
	  DbFormView *pWnd; 
   public :
   	  CListDropEx ();
	  void SetpWnd (DbFormView *pWnd);
	  virtual DROPEFFECT OnDropEx (CWnd* , COleDataObject*,  DROPEFFECT,  DROPEFFECT, CPoint);
};
#endif