#include "StdAfx.h"
#include "decimaledit.h"
#include "StrFuncs.h"

IMPLEMENT_DYNAMIC(CDecimalEdit, CNumEdit)

CDecimalEdit::CDecimalEdit(void)
{
	scale = 0;
}

CDecimalEdit::CDecimalEdit(int scale)
{
	this->scale = scale;
}

CDecimalEdit::~CDecimalEdit(void)
{
}

BEGIN_MESSAGE_MAP(CDecimalEdit, CNumEdit)
	ON_WM_KILLFOCUS ()
END_MESSAGE_MAP()

void CDecimalEdit::OnKillFocus (CWnd *newFocus)
{
	if (scale > 0)
	{
		CString Text;
		GetWindowText (Text);
		CString Format;
		Format.Format (_T("%c.%dlf"), '%', scale);
		Text.Format (Format.GetBuffer (), CStrFuncs::StrToDouble (Text));
		SetWindowText (Text);
	}
	CEdit::OnSetFocus (newFocus);
}

