// MainFrm.h : Schnittstelle der Klasse CMainFrame
//

#define ID_BUTTONS 6001

#pragma once
class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attribute
public:
	CWnd *TxtWnd;
	CWnd *DataWnd;
	CWnd *EtiWnd;

// Operationen
public:

// Überschreibungen
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementierung
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // Eingebundene Elemente der Steuerleiste
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CToolBar    m_wndToolBar2;
	CToolBar    m_InstBar;

// Generierte Funktionen für die Meldungstabellen
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext); 
	DECLARE_MESSAGE_MAP()
public:
	void AddToolbarButton (LPCTSTR Text);
};


