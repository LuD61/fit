#ifndef DB_PROPERTYSHEET_DEF
#define DB_PROPERTYSHEET_DEF
#pragma once
#include "afxdlgs.h"

class CDbPropertySheet :
	public CPropertySheet
{
protected :
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	BOOL Tabs;
	CDbPropertySheet(void);
	CDbPropertySheet(LPCTSTR);
	~CDbPropertySheet(void);
	virtual BOOL Read ();
	virtual BOOL Delete ();
	virtual BOOL Write ();
	virtual BOOL Print ();
	virtual BOOL EnablePrint (CCmdUI *);
	virtual BOOL TextCent ();
	virtual BOOL EnableTextCent (CCmdUI *);
	virtual BOOL TextLeft ();
	virtual BOOL EnableTextLeft (CCmdUI *);
	virtual BOOL TextRight ();
	virtual BOOL EnableTextRight (CCmdUI *);
	virtual void Show ();
	virtual void StepBack ();
	virtual void NextRecord ();
	virtual void PriorRecord ();
	virtual void FirstRecord ();
	virtual void LastRecord ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
	afx_msg void OnBreak();
    virtual void OnMarkAll ();
    virtual void OnUnMarkAll ();
};
#endif