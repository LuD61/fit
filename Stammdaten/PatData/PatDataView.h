// PatDataView.h : Schnittstelle der Klasse CPatDataView
//


#pragma once


class CPatDataView : public CFormView
{
protected: // Nur aus Serialisierung erstellen
	CPatDataView();
	DECLARE_DYNCREATE(CPatDataView)

// Attribute
public:
	enum { IDD = IDD_DIALOG1 };
	CPatDataDoc* GetDocument() const;

// Operationen
public:

// �berschreibungen
	public:
//	virtual void OnDraw(CDC* pDC);  // �berladen, um diese Ansicht darzustellen
virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~CPatDataView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit1();
};

#ifndef _DEBUG  // Debugversion in PatDataView.cpp
inline CPatDataDoc* CPatDataView::GetDocument() const
   { return reinterpret_cast<CPatDataDoc*>(m_pDocument); }
#endif

