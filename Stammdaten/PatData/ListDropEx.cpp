#include "stdafx.h"
#include "ListDropEx.h"

CListDropEx::CListDropEx () : CListDropTarget ()
{
	pWnd = NULL;
}

void CListDropEx::SetpWnd (DbFormView *pWnd)
{
	this->pWnd = pWnd;
}

DROPEFFECT CListDropEx::OnDropEx (CWnd* pWnd,  COleDataObject* pDataObject,
					                 	  DROPEFFECT dropDefault, DROPEFFECT dropList,
										  CPoint point)
{
	if (this->pWnd != NULL)
	{
		return this->pWnd->OnDrop (pWnd,pDataObject,
				       	      dropDefault, dropList,
						      point);
	}
	return DROPEFFECT_COPY;
}
