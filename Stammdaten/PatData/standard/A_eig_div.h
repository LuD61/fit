#ifndef _A_EIG_DIV_DEF
#define _A_EIG_DIV_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_EIG_DIV {
   double         a;
   long           a_krz;
   short          anz_reg_eti;
   short          anz_theke_eti;
   short          anz_waren_eti;
   short          delstatus;
   short          fil;
   double         gew_bto;
   double         inh;
   short          mdn;
   short          me_einh_ek;
   TCHAR          pr_ausz[2];
   TCHAR          pr_man[2];
   TCHAR          pr_ueb[2];
   TCHAR          reg_eti[2];
   TCHAR          rez[9];
   short          sg1;
   short          sg2;
   double         tara;
   TCHAR          theke_eti[2];
   TCHAR          verk_art[2];
   TCHAR          vpk_kz[2];
   TCHAR          waren_eti[2];
   TCHAR          mwst_ueb[2];
   DATE_STRUCT    verk_beg;
};
extern struct A_EIG_DIV a_eig_div, a_eig_div_null;

#line 8 "a_eig_div.rh"

class A_EIG_DIV_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_EIG_DIV a_eig_div;  
               A_EIG_DIV_CLASS () : DB_CLASS ()
               {
               }
};
#endif
