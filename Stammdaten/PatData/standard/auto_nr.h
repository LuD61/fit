#ifndef _AUTO_NR_DEF
#define _AUTO_NR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AUTO_NR {
   short          mdn;
   short          fil;
   TCHAR          nr_nam[11];
   long           nr_nr;
   short          satz_kng;
   long           max_wert;
   TCHAR          nr_char[11];
   long           nr_char_lng;
   TCHAR          fest_teil[11];
   TCHAR          nr_komb[21];
   short          delstatus;
};
extern struct AUTO_NR auto_nr, auto_nr_null;

#line 8 "auto_nr.rh"

class AUTO_NR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               AUTO_NR auto_nr;
               AUTO_NR_CLASS () : DB_CLASS ()
               {
               }
};
#endif
