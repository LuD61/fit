#pragma once
#include "afxdlgs.h"

class CDbPropertyPage :
	public CPropertyPage
{
protected:
	BOOL InData;
	DECLARE_DYNAMIC(CDbPropertyPage)
public:
	CDbPropertyPage(void);
	CDbPropertyPage(UINT);
	~CDbPropertyPage(void);
	virtual BOOL Read ();
	virtual BOOL Write ();
	virtual void write ();
	virtual void OnDelete ();
	virtual BOOL AfterWrite ();
	virtual BOOL Print ();
	virtual BOOL EnablePrint (CCmdUI *);
	virtual BOOL TextCent ();
	virtual BOOL EnableTextCent (CCmdUI *);
	virtual BOOL TextLeft ();
	virtual BOOL EnableTextLeft (CCmdUI *);
	virtual BOOL TextRight ();
	virtual BOOL EnableTextRight (CCmdUI *);
	virtual BOOL Delete ();
	virtual BOOL Show ();
	virtual BOOL StepBack ();
    virtual void NextRecord ();
    virtual void PriorRecord ();
    virtual void FirstRecord ();
    virtual void LastRecord ();
	virtual void OnCopy ();
	virtual void OnPaste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
    virtual void OnMarkAll ();
    virtual void OnUnMarkAll ();
	virtual void OnChoice (){}
    virtual DROPEFFECT OnDrop (CWnd* , COleDataObject*,  DROPEFFECT,  DROPEFFECT, CPoint);
};
