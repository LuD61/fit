// PazTexte.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "PatData.h"
#include "PatDataDoc.h"
#include "PazTexte.h"
#include "FillList.h"
#include "FormTab.h"
#include "SgTab.h"
#include "DbUniCode.h"
#include "BmpFunc.h"
#include "StrToken.h"

#define RECCHANGE                       WM_USER + 2000

// CPazTexte

IMPLEMENT_DYNCREATE(CPazTexte, DbFormView)

CPazTexte::CPazTexte()
	: DbFormView(CPazTexte::IDD)
{
	SgInfos.Init ();
	SgTab.Init ();
	Choice = NULL;
	ModalChoice = FALSE;
	ChoiceEti = NULL;
	ModalChoiceEti = TRUE;

    TxtTable[0] = Pr_ausz_gx.pr_ausz_gx.txt1; 
    TxtTable[1] = Pr_ausz_gx.pr_ausz_gx.txt2; 
    TxtTable[2] = Pr_ausz_gx.pr_ausz_gx.txt3; 
    TxtTable[3] = Pr_ausz_gx.pr_ausz_gx.txt4; 
    TxtTable[4] = Pr_ausz_gx.pr_ausz_gx.txt5; 
    TxtTable[5] = Pr_ausz_gx.pr_ausz_gx.txt6; 
    TxtTable[6] = Pr_ausz_gx.pr_ausz_gx.txt7; 
    TxtTable[7] = Pr_ausz_gx.pr_ausz_gx.txt8; 
    TxtTable[8] = Pr_ausz_gx.pr_ausz_gx.txt9; 
    TxtTable[9] = Pr_ausz_gx.pr_ausz_gx.txt10; 
    TxtTable[10] = Pr_ausz_gx.pr_ausz_gx.txt11; 
    TxtTable[11] = Pr_ausz_gx.pr_ausz_gx.txt12; 
    TxtTable[12] = Pr_ausz_gx.pr_ausz_gx.txt13; 
    TxtTable[13] = Pr_ausz_gx.pr_ausz_gx.txt14; 
    TxtTable[14] = Pr_ausz_gx.pr_ausz_gx.txt15; 
    TxtTable[15] = Pr_ausz_gx.pr_ausz_gx.txt16; 
    TxtTable[16] = Pr_ausz_gx.pr_ausz_gx.txt17; 
    TxtTable[17] = Pr_ausz_gx.pr_ausz_gx.txt18; 
    TxtTable[18] = Pr_ausz_gx.pr_ausz_gx.txt19; 
    TxtTable[19] = Pr_ausz_gx.pr_ausz_gx.txt20; 
    TxtTable[20] = NULL; 

    SgTable[0] = &Pr_ausz_gx.pr_ausz_gx.sg1; 
    SgTable[1] = &Pr_ausz_gx.pr_ausz_gx.sg2; 
    SgTable[2] = &Pr_ausz_gx.pr_ausz_gx.sg3; 
    SgTable[3] = &Pr_ausz_gx.pr_ausz_gx.sg4; 
    SgTable[4] = &Pr_ausz_gx.pr_ausz_gx.sg5; 
    SgTable[5] = &Pr_ausz_gx.pr_ausz_gx.sg6; 
    SgTable[6] = &Pr_ausz_gx.pr_ausz_gx.sg7; 
    SgTable[7] = &Pr_ausz_gx.pr_ausz_gx.sg8; 
    SgTable[8] = &Pr_ausz_gx.pr_ausz_gx.sg9; 
    SgTable[9] = &Pr_ausz_gx.pr_ausz_gx.sg10; 
    SgTable[10] = &Pr_ausz_gx.pr_ausz_gx.sg11; 
    SgTable[11] = &Pr_ausz_gx.pr_ausz_gx.sg12; 
    SgTable[12] = &Pr_ausz_gx.pr_ausz_gx.sg13; 
    SgTable[13] = &Pr_ausz_gx.pr_ausz_gx.sg14; 
    SgTable[14] = &Pr_ausz_gx.pr_ausz_gx.sg15; 
    SgTable[15] = &Pr_ausz_gx.pr_ausz_gx.sg16; 
    SgTable[16] = &Pr_ausz_gx.pr_ausz_gx.sg17; 
    SgTable[17] = &Pr_ausz_gx.pr_ausz_gx.sg18; 
    SgTable[18] = &Pr_ausz_gx.pr_ausz_gx.sg19; 
    SgTable[19] = &Pr_ausz_gx.pr_ausz_gx.sg20; 
    SgTable[20] = NULL; 
	HeadControls.Add (&m_TextNr);
	memcpy (&Pr_ausz_gx.pr_ausz_gx, 
		    &pr_ausz_gx, sizeof (PR_AUSZ_GX));

/*
	LPTSTR etc;

	etc = _tgetenv (_T("BWSETC"));
	if (etc != NULL)
	{
		CString CodeFile;
		CodeFile.Format (_T("%s\\CodeTable.cfg"), etc);
		Code.Load (CodeFile);
	}
*/
}

CPazTexte::~CPazTexte()
{
	Font.DeleteObject ();
	SgInfos.DestroyAll ();
	SgTab.DestroyAll ();
	A_bas.dbclose ();
	Pr_ausz_gx.dbclose ();
	EtiTyp.dbclose ();
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	memcpy (&pr_ausz_gx, &pr_ausz_gx_null, sizeof (PR_AUSZ_GX));
}

void CPazTexte::DoDataExchange(CDataExchange* pDX)
{
	DbFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BORDER, m_Border);
	DDX_Control(pDX, IDC_LTEXTNR, m_LTextNr);
	DDX_Control(pDX, IDC_TEXTNR, m_TextNr);
	DDX_Control(pDX, IDC_LETI, m_LEti);
	DDX_Control(pDX, IDC_ETI, m_Eti);
	DDX_Control(pDX, IDC_ETI_BZ, m_EtiBz);
	DDX_Control(pDX, IDC_LEFT, m_Left);
	DDX_Control(pDX, IDC_RIGHT, m_Right);
//	DDX_Control(pDX, IDC_ETI_BZ2, m_EtiBz2);
	DDX_Control(pDX, IDC_TEXTE, m_Texte);
}

BEGIN_MESSAGE_MAP(CPazTexte, DbFormView)
	ON_WM_SIZE ()
	ON_WM_ACTIVATE ()
	ON_BN_CLICKED(IDC_TEXTCHOICE , OnTextchoice)
	ON_BN_CLICKED(IDC_ETICHOICE , OnEtiChoice)
	ON_COMMAND (SELECTED, OnTextSelected)
	ON_COMMAND (CANCELED, OnTextCanceled)
	ON_COMMAND (RECCHANGE, OnRecChange)
	ON_COMMAND(ID_EDIT_COPY,OnCopy)
	ON_COMMAND(ID_EDIT_PASTE,OnPaste)
	ON_NOTIFY(LVN_BEGINRDRAG, IDC_TEXTE, OnLvnBegindrag)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_LANGUAGE, OnLanguage)
	ON_COMMAND(ID_TEXT_CENT, OnTextCent)
	ON_COMMAND(ID_TEXT_LEFT, OnTextLeft)
	ON_COMMAND(ID_TEXT_RIGHT, OnTextRight)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_COMMAND(ID_BACK, OnBack)
	ON_BN_CLICKED(IDC_LEFT, OnBnClickedLeft)
	ON_BN_CLICKED(IDC_RIGHT, OnBnClickedRight)
END_MESSAGE_MAP()


// CPazTexte-Diagnose

#ifdef _DEBUG
void CPazTexte::AssertValid() const
{
	CFormView::AssertValid();
}

void CPazTexte::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

BOOL CPazTexte::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return DbFormView::PreCreateWindow(cs);
}

void CPazTexte::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	Pr_ausz_gx.opendbase (_T("bws"));
	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();

//	memcpy (&Pr_ausz_gx.pr_ausz_gx, &pr_ausz_gx_null, sizeof (PR_AUSZ_GX));
	memcpy (&Pr_ausz_gx.pr_ausz_gx, &pr_ausz_gx, sizeof (PR_AUSZ_GX));
	Form.Add (new CFormField (&m_TextNr,EDIT,   (long *)  &Pr_ausz_gx.pr_ausz_gx.text_nr, VLONG));
	Form.Add (new CFormField (&m_Eti,EDIT,      (short *) &Pr_ausz_gx.pr_ausz_gx.eti_typ, VSHORT));

	TextGrid.Create (this, 1, 2);
    TextGrid.SetBorder (0, 0);
    TextGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_TextNr = new CCtrlInfo (&m_TextNr, 0, 0, 1, 1);
	TextGrid.Add (c_TextNr);
	CtrlGrid.CreateChoiceButton (m_TextChoice, IDC_TEXTCHOICE, this);
	CCtrlInfo *c_TextChoice = new CCtrlInfo (&m_TextChoice, 1, 0, 1, 1);
	TextGrid.Add (c_TextChoice);


	EtiGrid.Create (this, 1, 2);
    EtiGrid.SetBorder (0, 0);
    EtiGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Eti = new CCtrlInfo (&m_Eti, 0, 0, 1, 1);
	EtiGrid.Add (c_Eti);
	CtrlGrid.CreateChoiceButton (m_EtiChoice, IDC_ETICHOICE, this);
	CCtrlInfo *c_EtiChoice = new CCtrlInfo (&m_EtiChoice, 1, 0, 1, 1);
	EtiGrid.Add (c_EtiChoice);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	HBITMAP hBmp = CBmpFunc::LoadBitmap (AfxGetInstanceHandle(), IDB_LEFT, 
		                                                         IDB_LEFTMASK,
																 GetSysColor (COLOR_3DFACE));
	//	HBITMAP hBmp = LoadBitmap (AfxGetInstanceHandle (), MAKEINTRESOURCE (IDB_LEFT));
	m_Left.SetBitmap (hBmp);
//	hBmp = LoadBitmap (AfxGetInstanceHandle (), MAKEINTRESOURCE (IDB_RIGHT));
	hBmp = CBmpFunc::LoadBitmap (AfxGetInstanceHandle(), IDB_RIGHT, 
		                                                         IDB_RIGHTMASK,
																 GetSysColor (COLOR_3DFACE));
	m_Right.SetBitmap (hBmp);
    CCtrlInfo *c_Border = new CCtrlInfo (&m_Border, 0, 0, DOCKRIGHT, 4);
    c_Border->rightspace = 30; 
	CtrlGrid.Add (c_Border);
	CCtrlInfo *c_LTextNr = new CCtrlInfo (&m_LTextNr, 1, 1, 1, 1);
	CtrlGrid.Add (c_LTextNr);

	CCtrlInfo *c_TextGrid = new CCtrlInfo (&TextGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_TextGrid);

	CCtrlInfo *c_LEti = new CCtrlInfo (&m_LEti, 1, 2, 1, 1);
	CtrlGrid.Add (c_LEti);

	CCtrlInfo *c_EtiGrid = new CCtrlInfo (&EtiGrid, 2, 2, 2, 1);
	CtrlGrid.Add (c_EtiGrid);

	CCtrlInfo *c_EtiBz = new CCtrlInfo (&m_EtiBz, 4, 2, 1, 1);
	CtrlGrid.Add (c_EtiBz);

	CtrlGrid.Add (new CCtrlInfo (this, 80, 5, 1, 1, 1));
	CCtrlInfo *c_Left = new CCtrlInfo (&m_Left, 6, 1, 1, 3);
	CtrlGrid.Add (c_Left);
	CCtrlInfo *c_Right = new CCtrlInfo (&m_Right, 7, 1, 1, 3);
	CtrlGrid.Add (c_Right);

//	CCtrlInfo *c_EtiBz2 = new CCtrlInfo (&m_EtiBz2, 4, 3, 1, 1);
//	CtrlGrid.Add (c_EtiBz2);

	CCtrlInfo *c_Texte = new CCtrlInfo (&m_Texte, 0, 5, DOCKRIGHT, DOCKBOTTOM);
    c_Texte->rightspace = 30; 
	CtrlGrid.Add (c_Texte);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);


	ListCheck.Create (16, 16, ILC_COLOR8, 0, 4);
    CBitmap bm;
    if (bm.LoadBitmap(IDB_UNCHECKED))
	{
		ListCheck.Add(&bm, RGB (0,0,0));
	}
    CBitmap bm2;
    if (bm2.LoadBitmap(IDB_CHECKED))
	{
		ListCheck.Add(&bm2, RGB (0,0,0));
	}
	m_Texte.SetImageList (&ListCheck, LVSIL_SMALL);   

	m_Texte.SetFont (&lFont);

	FillList = m_Texte;
	FillList.SetStyle (LVS_REPORT);
	m_Texte.GridLines = TRUE;
	if (m_Texte.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
//	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Sel"), 0, 40);
	FillList.SetCol (_T("Zeile"), 1, 60, LVCFMT_RIGHT);
	FillList.SetCol (_T("SG/ZE"), 2, 60, LVCFMT_RIGHT);
	FillList.SetCol (_T("Text"), 3, 500, LVCFMT_LEFT);

	CtrlGrid.Display ();
	Form.Show ();
	m_EtiBz.SetReadOnly ();
	if (Pr_ausz_gx.pr_ausz_gx.text_nr != 0l)
	{
		Read ();
	}
    
	if (!ModalChoice)
	{
		OnTextchoice ();
		OnTextCanceled ();
		if (Pr_ausz_gx.pr_ausz_gx.text_nr != 0l)
		{
			CPrAuszList *p = new CPrAuszList (Pr_ausz_gx.pr_ausz_gx.text_nr, 1, _T(""), _T(""));
			Choice->SetListPos (p);
		}
	}
//	CtrlGrid.DockControlWidth (this, &m_EtiBz, &m_EtiBz2);
//	CtrlGrid.DockControlHeight (this, &m_EtiBz, &m_EtiBz2);
}


// CPazTexte-Meldungshandler


BOOL CPazTexte::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_Texte.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetFocus () != &m_Texte &&
					GetFocus ()->GetParent () != &m_Texte )
				{

					break;
			    }
				m_Texte.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F12)
			{
				write ();;
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					Choice->ShowWindow (SW_HIDE);
				}
				return TRUE;
			}

			else if (pMsg->wParam == VK_F9)
			{
				OnTextchoice ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F2)
			{
				OnBnClickedLeft();
			}
			else if (pMsg->wParam == VK_F3)
			{
				OnBnClickedRight();
			}
			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}

	}
    return CFormView::PreTranslateMessage(pMsg);
}

void CPazTexte::OnCopy ()
{
    CWnd *pWnd = GetFocus ();
	if (pWnd == &m_Texte.ListEdit ||
        pWnd == &m_Texte.ListComboBox ||
		pWnd == &m_Texte)
	{
		ListCopy ();
		return;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CPazTexte::OnPaste ()
{
    CWnd *pWnd = GetFocus ();
	if (pWnd == &m_Texte.ListEdit ||
        pWnd == &m_Texte.ListComboBox ||
		pWnd == &m_Texte)
	{
		ListPaste ();
		return;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}

void CPazTexte::ListPaste ()
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return;
    }

	CString Text;
    HGLOBAL hglbCopy;
    LPTSTR data;

	try
	{
		 hglbCopy =  ::GetClipboardData(CF_TEXT);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
		 data = (LPTSTR) GlobalLock ((HGLOBAL) hglbCopy);
         Text = data;
		 GlobalUnlock ((HGLOBAL) hglbCopy);
		 if (Text != "")
		 {
			m_Texte.StopEnter ();

/*
			TCHAR sep [] = {13, 10, 0};
			int pos = 0;
			for (int i = m_Texte.EditRow; i < MAXLISTROWS; i ++)
			{

				CString Row = Text.Tokenize (sep, pos);
				if (pos < 0) break;
				if (i >= m_Texte.GetItemCount ())
				{
					FillList.InsertItem (i, -1);
					CString RowNr;
					RowNr.Format (_T("%d"), i + 1);
				    FillList.SetItemText (RowNr.GetBuffer (), i, 1);
				}
*/
  		    CStrToken t;
		    t.SetSep (_T("\n"));
			t = Text;
			for (int i = m_Texte.EditRow, idx = 0; i < MAXLISTROWS; i ++, idx ++)
			{
				if (idx == t.GetAnzToken ()) break;
				CString Row = t.GetToken (idx);
				Row.TrimRight ();
				if (i >= m_Texte.GetItemCount ())
				{
					FillList.InsertItem (i, -1);
					CString RowNr;
					RowNr.Format (_T("%d"), i + 1);
				    FillList.SetItemText (RowNr.GetBuffer (), i, 1);
				}

				CString Sg;
				Sg = m_Texte.GetItemText (i, 2);
                *SgTable[i] = _wtoi (Sg.GetBuffer ());
				if (*SgTable[i] <= 0) 
				{
					*SgTable[i] = 1;
				}
				CSg* cSg = SgTab.Get (*SgTable[i]);
				if (cSg == NULL)
				{
					cSg = SgTab.Get (0);
				}
				if (cSg == NULL) continue;
				Sg.Format (_T("%hd/%hd"), cSg->sg, cSg->ze);
				FillList.SetItemText (Sg.GetBuffer (0), i, 2);

				FillList.SetItemText (_T(""), i, 3);
				FillList.SetItemText (Row.GetBuffer (60), i, 3);
			}
		    m_Texte.StartEnter (2, m_Texte.EditRow);
		}
	}
	catch (...) {};
    CloseClipboard();
}

void CPazTexte::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_Texte.ListEdit.m_hWnd))
	{
		m_Texte.StopEnter ();
		m_Texte.StartEnter (m_Texte.EditCol, m_Texte.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
		{
			BOOL& b = m_Texte.vSelect[i];
			if (b)
			{
				SaveAll = FALSE;
				break;
			}
		}
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
		for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
		{
			if (!SaveAll)
			{
				BOOL& b = m_Texte.vSelect[i];
				if (!b) continue;
			}
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = m_Texte.GetItemText (i, 3);
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

void CPazTexte::OnLvnBegindrag(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;

    COleDataSource dataSource;
    HGLOBAL hglbCopy;

    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard") );
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }
	try
	{
		BOOL SaveAll = TRUE;
		for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
		{
			BOOL& b = m_Texte.vSelect[i];
			if (b)
			{
				SaveAll = FALSE;
				break;
			}
		}
		
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
		for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
		{
			if (!SaveAll)
			{
				BOOL& b = m_Texte.vSelect[i];
				if (!b) continue;
			}

			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = m_Texte.GetItemText (i, 3);
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		STGMEDIUM stg;
		stg.tymed = TYMED_HGLOBAL;
		stg.hGlobal = hglbCopy;
		stg.pUnkForRelease = NULL;
		HANDLE cData = NULL;
//		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        dataSource.CacheGlobalData (CF_TEXT, hglbCopy);
	    DROPEFFECT effect = dataSource.DoDragDrop (DROPEFFECT_COPY,
		                                            NULL, NULL);
	}
	catch (...) {}
    CloseClipboard();
}

void CPazTexte::OnMarkAll()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	for (int i = 0; i < m_Texte.GetItemCount (); i ++)
    {
			   FillList.SetItemImage (i,1);
	}
	m_Texte.AllMarked = TRUE;
}

void CPazTexte::OnUnMarkAll()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	for (int i = 0; i < m_Texte.GetItemCount (); i ++)
    {
			   FillList.SetItemImage (i,0);
	}
	m_Texte.AllMarked = FALSE;
}


DROPEFFECT CPazTexte::OnDrop (CWnd* pWnd,  COleDataObject* pDataObject,
					                 	  DROPEFFECT dropDefault, DROPEFFECT dropList,
										  CPoint point)
{
	STGMEDIUM stg;
/*
	stg.tymed = TYMED_HGLOBAL;
	stg.hGlobal = hglbCopy;
	stg.pUnkForRelease = NULL;
*/
	ClientToScreen (&point);
	m_Texte.ScreenToClient (&point);
	if (point.y < 0)
	{
		return DROPEFFECT_COPY;
	}
	if (pDataObject->GetData (CF_TEXT, &stg))
	{

 	    if (stg.tymed == TYMED_HGLOBAL)
		{
			HGLOBAL hglbCopy = stg.hGlobal;
			LPTSTR data = (LPTSTR) GlobalLock ((HGLOBAL) hglbCopy);
			CString Text = data;
			GlobalUnlock ((HGLOBAL) hglbCopy);
			if (Text != "")
			{
				m_Texte.StopEnter ();
				TCHAR sep [] = {13, 10, 0};
				int pos = 0;
				for (int i = m_Texte.EditRow; i < MAXLISTROWS; i ++)
				{
					CString Row = Text.Tokenize (sep, pos);
					if (pos < 0) break;
					if (i >= m_Texte.GetItemCount ())
					{
						FillList.InsertItem (i, -1);
						CString RowNr;
						RowNr.Format (_T("%d"), i + 1);
						FillList.SetItemText (RowNr.GetBuffer (), i, 1);
					}
					CString cSg;
					cSg = m_Texte.GetItemText (i, 2);
					*SgTable[i] = _wtoi (cSg.GetBuffer ());
					if (*SgTable[i] == 0) *SgTable[i] = 1;
					cSg.Format (_T("%hd"), *SgTable[i]);
					FillList.SetItemText (cSg.GetBuffer (), i, 2);
					FillList.SetItemText (_T(""), i, 3);
					FillList.SetItemText (Row.GetBuffer (60), i, 3);
				}
				m_Texte.StartEnter (2, m_Texte.EditRow);
			}
		}
	}
	return DROPEFFECT_NONE;
}

BOOL CPazTexte::Show ()
{
	OnF9 ();
	return TRUE;
}

void CPazTexte::OnF9 ()
{
/*
	if (GetFocus () == &m_Ean)
	{
		OnAchoice ();
	}
*/
}

void CPazTexte::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize (nType, cx, cy);
	if (m_LTextNr.m_hWnd != NULL)
	{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
	}
}

void CPazTexte::MoveSize(int cx, int cy)
{
	if (m_LTextNr.m_hWnd != NULL)
	{
		CtrlGrid.Move (cx, cy);
	}
}

void CPazTexte::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CPazTexte::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Eti)
	{
		if (!Read ())
		{
			m_TextNr.SetFocus ();
			return FALSE;
		}
	}

	if (Control != &m_Texte &&
		Control->GetParent ()!= &m_Texte )
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
	return FALSE;
}

BOOL CPazTexte::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_Texte &&
		Control->GetParent ()!= &m_Texte )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
	else if (m_Texte.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
    return FALSE;
}

BOOL CPazTexte::Read ()
{
	CString TextNr;
	CString EtiNr;

	memcpy (&Pr_ausz_gx.pr_ausz_gx, &pr_ausz_gx_null, sizeof (pr_ausz_gx));
	memcpy (&pr_ausz_gx, &pr_ausz_gx_null, sizeof (pr_ausz_gx));
	m_TextNr.GetWindowText (TextNr);
	m_Eti.GetWindowText (EtiNr);
	Pr_ausz_gx.pr_ausz_gx.text_nr = _tstoi (TextNr.GetBuffer ());
	Pr_ausz_gx.pr_ausz_gx.eti_typ = _tstoi (EtiNr.GetBuffer ());
	if (!FillSg (Pr_ausz_gx.pr_ausz_gx.eti_typ))
	{
		return FALSE;
	}
	m_Texte.DeleteAllItems ();
	m_Texte.vSelect.clear ();
	int dsqlstatus = Pr_ausz_gx.dbreadfirst ();
//	if (dsqlstatus == 0)
	{
		memcpy (&pr_ausz_gx, &Pr_ausz_gx.pr_ausz_gx, sizeof (pr_ausz_gx));
		for (int i = 0; SgTable[i] != NULL; i ++)
		{
			FillList.InsertItem (i, 0);
			CString Row;
			Row.Format (_T("%d"), i + 1);
			FillList.SetItemText (Row.GetBuffer (), i, 1);
			CString Sg;
//			if (*SgTable[i] <= 0) *SgTable[i] = 1;
			CSg* cSg = SgTab.Get (*SgTable[i]);
			if (cSg != NULL)
			{
				Sg.Format (_T("%hd/%hd"), *SgTable[i], cSg->ze);
			}
			else
			{
				Sg.Format (_T("%hd/%hd"), *SgTable[i], 60);
			}
			FillList.SetItemText (Sg.GetBuffer (), i, 2);
			LPSTR p = (LPSTR) TxtTable[i];
			CDbUniCode::DbToUniCode (Code, TxtTable[i], p, sizeof (Pr_ausz_gx.pr_ausz_gx.txt1) - 1);
			FillList.SetItemText (TxtTable[i], i, 3);
			m_Texte.vSelect.push_back (FALSE);
		}
		EnableHeadControls (FALSE);
		return TRUE;
	}
	return FALSE;
}

BOOL CPazTexte::FillSg (long eti)
{
	EtiTyp.eti_typ.eti = eti;
	int dsqlstatus = EtiTyp.dbreadfirst ();
	if (dsqlstatus != 0)
	{
		MessageBox (_T ("Falsche Etikettennummer"), NULL, MB_OK);
		return FALSE;
	}

	memcpy (&eti_typ, &EtiTyp.eti_typ, sizeof (eti_typ));
	SgInfos.DestroyAll ();

//	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg01, EtiTyp.eti_typ.ze01));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg02, EtiTyp.eti_typ.ze02));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg03, EtiTyp.eti_typ.ze03));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg04, EtiTyp.eti_typ.ze04));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg05, EtiTyp.eti_typ.ze05));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg06, EtiTyp.eti_typ.ze06));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg07, EtiTyp.eti_typ.ze07));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg08, EtiTyp.eti_typ.ze08));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg09, EtiTyp.eti_typ.ze09));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg10, EtiTyp.eti_typ.ze10));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg11, EtiTyp.eti_typ.ze11));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg12, EtiTyp.eti_typ.ze12));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg13, EtiTyp.eti_typ.ze13));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg14, EtiTyp.eti_typ.ze14));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg15, EtiTyp.eti_typ.ze15));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg16, EtiTyp.eti_typ.ze16));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg17, EtiTyp.eti_typ.ze17));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg18, EtiTyp.eti_typ.ze18));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg19, EtiTyp.eti_typ.ze19));
	SgInfos.Add (new CSgInfo (EtiTyp.eti_typ.sg20, EtiTyp.eti_typ.ze20));
	SgInfos.Add (new CSgInfo (0, 0));

	SgInfos.Clone (SgTab);

	m_Texte.FillSgCombo (&SgTab);
	LPSTR p = (LPSTR) EtiTyp.eti_typ.eti_bz;
	DbToUniCode (EtiTyp.eti_typ.eti_bz, p);
	m_EtiBz.SetWindowText (EtiTyp.eti_typ.eti_bz);
	return TRUE;
}

void CPazTexte::DbToUniCode (LPTSTR tstr, LPSTR str)
{
	LPSTR aString;
	size_t len = strlen (str) + 2;
	aString = new char [len * 2];
	if (aString == NULL) return;
	memcpy (aString, (LPSTR) str, len);
	str = aString + strlen (aString);
	for (; (*str <= 0x20) && (str != aString); str -= 1);
	if (*str > 0x20) str += 1;
	*str = 0;
	len = strlen (aString) + 1;
	_tcscpy (tstr, _T(""));
    MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, aString, -1, tstr, (int) len);
	tstr[len] = 0;
	CString c = tstr;
	c.TrimRight ();
	_tcscpy (tstr, c.GetBuffer ());
	delete aString;
}

void CPazTexte::OnTextchoice ()
{
    CString Text;
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		Choice->SetListFocus ();
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceTxt (this);
	    Choice->IsModal = ModalChoice;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&Pr_ausz_gx);
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
		Choice->MoveWindow (&rect);
		Choice->SetListFocus ();
		return;
	}
    if (Choice->GetState ())
    {
/*
          Text = Choice->GetSelText ();
          int pos = 0;
		  CString TxtNr = Text.Tokenize (_T(" "), pos);
		  Pr_ausz_gx.pr_ausz_gx.text_nr = _tstol (TxtNr.GetBuffer (0));
*/
		  CPrAuszList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  Pr_ausz_gx.pr_ausz_gx.text_nr = abl->txt_nr;
		  Pr_ausz_gx.pr_ausz_gx.eti_typ = abl->eti_typ;
		  CString TextNr;
		  TextNr.Format (_T("%ld"), abl->txt_nr);
		  m_TextNr.SetWindowText (TextNr); 
		  CString EtiTyp;
		  EtiTyp.Format (_T("%hd"), abl->eti_typ);
		  m_Eti.SetWindowText (EtiTyp); 
		  m_Eti.SetSel (0, -1, TRUE);
		  m_Eti.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
//	delete Choice;
//	Choice = NULL;
}

void CPazTexte::OnTextSelected ()
{
	if (Choice == NULL) return;
    CPrAuszList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    Pr_ausz_gx.pr_ausz_gx.text_nr = abl->txt_nr;
    Pr_ausz_gx.pr_ausz_gx.eti_typ = abl->eti_typ;
    CString TextNr;
    TextNr.Format (_T("%ld"), abl->txt_nr);
    m_TextNr.SetWindowText (TextNr); 
    CString EtiTyp;
    EtiTyp.Format (_T("%hd"), abl->eti_typ);
    m_Eti.SetWindowText (EtiTyp); 
    m_Eti.SetSel (0, -1, TRUE);
    m_Eti.SetFocus ();
    PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
}

void CPazTexte::OnTextCanceled ()
{
//	if (Choice == NULL) return;
//	delete Choice;
//	Choice = NULL;
	Choice->ShowWindow (SW_HIDE);
}

void CPazTexte::write ()
{
	if (m_TextNr.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein Satz zum Schreiben selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}

	m_Texte.StopEnter ();
	for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
	{
		    CString Sg  = m_Texte.GetItemText (i, 2);
			int pos = 0;
			Sg = Sg.Tokenize (_T("/"), pos);
			*SgTable[i] = _tstoi (Sg.GetBuffer ());
			CString Row = m_Texte.GetItemText (i, 3);
			wmemset (TxtTable[i], 0, sizeof (TxtTable[i]));
			LPSTR p = (LPSTR) TxtTable[i];
			LPTSTR pt = Row.GetBuffer ();
			CDbUniCode::DbFromUniCode (Code, pt, p, sizeof (Pr_ausz_gx.pr_ausz_gx.txt1) / 2);
	}
	memcpy (&pr_ausz_gx, &Pr_ausz_gx.pr_ausz_gx, sizeof (pr_ausz_gx));
    Pr_ausz_gx.dbupdate ();
	if (Choice != NULL)
	{
		Choice->RefreshList ();
	}
	EnableHeadControls (TRUE);
	m_Texte.StopEnter ();
	m_Texte.DeleteAllItems ();
	m_Texte.vSelect.clear ();
	m_TextNr.SetFocus ();
}

void CPazTexte::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	write ();
}

void CPazTexte::OnLanguage()
{
	TCHAR CodePageName [256];
    LPTSTR etc;

	memset (CodePageName, 0, sizeof (CodePageName));
    OPENFILENAME fnstruct;
    static LPCTSTR lpstrFilter = 
            _T("Sprache\0*.*\0,\0");

    etc = _tgetenv (_T("BWSETC"));
	if (etc == NULL) return;
	CString InitialDir;
	InitialDir.Format (_T("%s\\CodePage"), etc);
    ZeroMemory (&fnstruct, sizeof (fnstruct));
    fnstruct.lStructSize = sizeof (fnstruct);
	fnstruct.hwndOwner   = this->m_hWnd;
    fnstruct.lpstrFile   = CodePageName;
    fnstruct.nMaxFile    = 255;
    fnstruct.lpstrFilter = lpstrFilter;
    fnstruct.lpstrInitialDir  = InitialDir.GetBuffer ();
	fnstruct.lpstrTitle = _T("Sprache ausw�hlen");
    BOOL ret = GetOpenFileName (&fnstruct);
	if (ret)
	{
		Code.DestroyAll ();
		CString CodeFile = CodePageName;
		Code.Load (CodeFile);
	}
}


BOOL CPazTexte::TextCent()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

   BOOL SaveAll = TRUE;
   m_Texte.StopEnter ();
   for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
   {
			BOOL& b = m_Texte.vSelect[i];
			if (b)
			{
				SaveAll = FALSE;
				break;
			}
   }


   for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
   {
			BOOL& b = m_Texte.vSelect[i];
			if (!SaveAll && !b) continue;

			CString Text = m_Texte.GetItemText (i, 3);
			Text.Trim ();
			CString cSg;
			if (m_Texte.EditRow == i && m_Texte.EditCol == 2 &&
				IsWindow (m_Texte.ListComboBox.m_hWnd))
			{
				int idx = m_Texte.ListComboBox.GetCurSel ();
				if (idx >= 0) 
				{
					m_Texte.ListComboBox.GetLBText (idx, cSg);
				}
			}
			else
			{
				cSg = m_Texte.GetItemText (i, 2);
			}
            *SgTable[i] = _tstoi (cSg.GetBuffer ());
			if (*SgTable[i] == 0) *SgTable[i] = 1;
			CSg *Sg = SgTab.Get (*SgTable[i]);
			if (Sg == NULL) continue;
			int ze = Sg->ze;
			int len = Text.GetLength ();
			int pos = max (0, (ze - len) / 2);
			CString NewText (' ', pos);
			NewText += Text;
			m_Texte.SetItemText (i, 3, NewText);
   }
   m_Texte.StartEnter (m_Texte.EditCol, m_Texte.EditRow);
   return TRUE; 
}

BOOL CPazTexte::TextLeft()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


   BOOL SaveAll = TRUE;
   m_Texte.StopEnter ();
   for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
   {
			BOOL& b = m_Texte.vSelect[i];
			if (b)
			{
				SaveAll = FALSE;
				break;
			}
   }


   for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
   {
			BOOL& b = m_Texte.vSelect[i];
			if (!SaveAll && !b) continue;

			CString Text = m_Texte.GetItemText (i, 3);
			Text.Trim ();
			CString cSg;
			if (m_Texte.EditRow == i && m_Texte.EditCol == 2 &&
				IsWindow (m_Texte.ListComboBox.m_hWnd))
			{
				int idx = m_Texte.ListComboBox.GetCurSel ();
				if (idx >= 0) 
				{
					m_Texte.ListComboBox.GetLBText (idx, cSg);
				}
			}
			else
			{
				cSg = m_Texte.GetItemText (i, 2);
			}
            *SgTable[i] = _tstoi (cSg.GetBuffer ());
			if (*SgTable[i] == 0) *SgTable[i] = 1;
			CSg *Sg = SgTab.Get (*SgTable[i]);
			if (Sg == NULL) continue;
			int ze = Sg->ze;
			int len = Text.GetLength ();
			int pos = 0;
			CString NewText (' ', pos);
			NewText += Text;
			m_Texte.SetItemText (i, 3, NewText);
   }
   m_Texte.StartEnter (m_Texte.EditCol, m_Texte.EditRow);
   return TRUE; 
}

BOOL CPazTexte::TextRight()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

   BOOL SaveAll = TRUE;
   m_Texte.StopEnter ();
   for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
   {
			BOOL& b = m_Texte.vSelect[i];
			if (b)
			{
				SaveAll = FALSE;
				break;
			}
   }
   for (int i = 0; i < MAXLISTROWS && i < m_Texte.GetItemCount (); i ++)
   {
			BOOL& b = m_Texte.vSelect[i];
			if (!SaveAll && !b) continue;

			CString Text = m_Texte.GetItemText (i, 3);
			Text.Trim ();
			CString cSg;
			if (m_Texte.EditRow == i && m_Texte.EditCol == 2 &&
				IsWindow (m_Texte.ListComboBox.m_hWnd))
			{
				int idx = m_Texte.ListComboBox.GetCurSel ();
				if (idx >= 0) 
				{
					m_Texte.ListComboBox.GetLBText (idx, cSg);
				}
			}
			else
			{
				cSg = m_Texte.GetItemText (i, 2);
			}
            *SgTable[i] = _tstoi (cSg.GetBuffer ());
			if (*SgTable[i] == 0) *SgTable[i] = 1;
			CSg *Sg = SgTab.Get (*SgTable[i]);
			if (Sg == NULL) continue;
			int ze = Sg->ze;
			int len = Text.GetLength ();
			TEXTMETRIC tm;
		    CFont *cFont = &Font;
		    if (cFont == NULL)
		    {
			  throw 1;
		    }
		    CDC *cDC = GetDC ();
		    cDC->SelectObject (cFont);
		    cDC->GetTextMetrics (&tm);
            CSize size = cDC->GetTextExtent (Text);
            CSize Xsize = cDC->GetTextExtent (_T("X"), 1);
 		    ReleaseDC (cDC);
//		    int pos = ze - size.cx /  Xsize.cx;
		    int pos = max (0, ze - len);
			CString NewText (' ', pos);
			NewText += Text;
			m_Texte.SetItemText (i, 3, NewText);
   }
   m_Texte.StartEnter (m_Texte.EditCol, m_Texte.EditRow);
   return TRUE; 
}

void CPazTexte::OnTextCent()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	TextCent ();
}

void CPazTexte::OnTextLeft()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	TextLeft ();
}

void CPazTexte::OnTextRight()
{
	TextRight ();
}

void CPazTexte::OnEtiChoice ()
{
    CString Text;
	if (ChoiceEti != NULL && !ModalChoiceEti)
	{
		ChoiceEti->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceEti == NULL)
	{
		ChoiceEti = new CChoiceEti (this);
	    ChoiceEti->IsModal = ModalChoiceEti;
		ChoiceEti->CreateDlg ();
	}

    ChoiceEti->SetDbClass (&Pr_ausz_gx);
	if (ModalChoiceEti)
	{
			ChoiceEti->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceEti->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceEti->MoveWindow (&rect);
		ChoiceEti->SetFocus ();
		return;
	}
    if (ChoiceEti->GetState ())
    {
		  CEtiList *abl = ChoiceEti->GetSelectedText (); 
		  if (abl == NULL) return;
		  Pr_ausz_gx.pr_ausz_gx.eti_typ = (short) abl->eti;
		  CString EtiNr;
		  EtiNr.Format (_T("%ld"), abl->eti);
		  m_Eti.SetWindowText (EtiNr); 
		  FillSg (Pr_ausz_gx.pr_ausz_gx.eti_typ);
		  m_Eti.SetFocus ();
    }
}


void CPazTexte::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


	if (m_TextNr.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein Satz zum Schreiben selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}

	if (MessageBox (_T("Zutaten l�schen"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		Pr_ausz_gx.dbdelete ();
		memcpy (&Pr_ausz_gx.pr_ausz_gx, &pr_ausz_gx_null, sizeof (PR_AUSZ_GX));
		m_Texte.DeleteAllItems ();
		EnableHeadControls (TRUE);
		Form.Show ();
		m_Texte.StopEnter ();
		m_Texte.DeleteAllItems ();
		m_Texte.vSelect.clear ();
		m_TextNr.SetFocus ();
		if (Choice != NULL)
		{
//			delete Choice;
//			Choice = NULL;
			Choice->RefreshList ();
		}
	}
}

void CPazTexte::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
//		Control->SetReadOnly (!enable);
        Control->EnableWindow (enable);
	}
}

void CPazTexte::StepBack ()
{
	if (m_TextNr.IsWindowEnabled ())
	{
				GetParent ()->DestroyWindow ();
	}
	else
	{
		EnableHeadControls (TRUE);
		m_Texte.StopEnter ();
		m_Texte.DeleteAllItems ();
		m_Texte.vSelect.clear ();
		m_TextNr.SetFocus ();
	}
}

void CPazTexte::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	StepBack ();
}

void CPazTexte::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	if (nState == WA_ACTIVE)
	{
		memcpy (&Pr_ausz_gx.pr_ausz_gx, 
			    &pr_ausz_gx, sizeof (PR_AUSZ_GX));
		if (Pr_ausz_gx.pr_ausz_gx.text_nr != 0l)
		{
			Read ();
		}
	}
}

void CPazTexte::OnRecChange()
{
	memcpy (&Pr_ausz_gx.pr_ausz_gx, 
		    &pr_ausz_gx, sizeof (PR_AUSZ_GX));
	if (Pr_ausz_gx.pr_ausz_gx.text_nr != 0l)
	{
		    Form.Show ();
			Read ();
		    if (!ModalChoice)
			{
				CPrAuszList *p = new CPrAuszList (Pr_ausz_gx.pr_ausz_gx.text_nr, 1, _T(""), _T(""));
				Choice->SetListPos (p);
			}
	}
}

void CPazTexte::OnBnClickedLeft()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (Choice == NULL) return;
	if (Choice->DecSel ())
	{
		Choice->SetListFocus ();
		OnTextSelected ();
	}
}

void CPazTexte::OnBnClickedRight()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (Choice == NULL) return;
	if (Choice->IncSel ())
	{
		Choice->SetListFocus ();
		OnTextSelected ();
	}
}
