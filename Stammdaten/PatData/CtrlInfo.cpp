#include "StdAfx.h"
#include "ctrlinfo.h"

IMPLEMENT_DYNAMIC(CCtrlInfo, CObject)
CCtrlInfo::CCtrlInfo(void)
{
	gridx = 0;
	gridy = 0;
	gridcx = 1;
	gridcy = 1;
	xplus = 0;
	yplus = 0;
	cxplus = 0;
	cyplus = 0;
	rightspace = 0;
	cWnd = NULL;
	Grid = NULL;
	width = 10;
	RightDockControl = NULL;
	pWidth = 0;
	Direction = HORIZONTAL;
}

CCtrlInfo::CCtrlInfo(CWnd *cWnd, int gridx, int gridy, 
					             int gridcx, int gridcy)
{
	this->cWnd = cWnd;
	xplus = 0;
	yplus = 0;
	cxplus = 0;
	cyplus = 0;
	SetPos (gridx, gridy, gridcx, gridcy);
	rightspace = 0;
	RightDockControl = NULL;
	Grid = NULL;
	SetWidth ();
	pWidth = 0;
	Direction = HORIZONTAL;
}

CCtrlInfo::CCtrlInfo(void *Grid, int gridx, int gridy, 
					             int gridcx, int gridcy)
{
	this->cWnd = NULL;
	xplus = 0;
	yplus = 0;
	cxplus = 0;
	cyplus = 0;
	SetPos (gridx, gridy, gridcx, gridcy);
	rightspace = 0;
	RightDockControl = NULL;
	this->Grid = Grid;
	SetWidth ();
	pWidth = 0;
	Direction = HORIZONTAL;
}

CCtrlInfo::CCtrlInfo(CWnd *pParent, int width, int gridx, int gridy, 
 		             int gridcx, int gridcy)
{
	Space.Create (NULL, WS_CHILD | WS_VISIBLE, CRect (0, 0, width, 12),
		         pParent);
	this->cWnd = &Space;
	xplus = 0;
	yplus = 0;
	cxplus = 0;
	cyplus = 0;
	SetPos (gridx, gridy, gridcx, gridcy);
	rightspace = 0;
	RightDockControl = NULL;
	Grid = NULL;
	SetWidth ();
	pWidth = 0;
	Direction = HORIZONTAL;
}


CCtrlInfo::~CCtrlInfo(void)
{
}

void CCtrlInfo::Create(CWnd *cWnd, int gridx, int gridy, 
					             int gridcx, int gridcy)
{
	this->cWnd = cWnd;
	SetPos (gridx, gridy, gridcx, gridcy);
}

void CCtrlInfo::SetPos(int gridx, int gridy, 
					   int gridcx, int gridcy)
{
	this->gridx = gridx;
	this->gridy = gridy;
	this->gridcx = gridcx;
	this->gridcy = gridcy;
}

void CCtrlInfo::SetCellPos (int xplus, int yplus)
{
	this->xplus = xplus;
	this->yplus = yplus;
}

void CCtrlInfo::SetCellPos (int xplus, int yplus, int cxplus, int cyplus)
{
	this->xplus = xplus;
	this->yplus = yplus;
	this->cxplus = cxplus;
	this->cyplus = cyplus;
}

void CCtrlInfo::SetWidth ()
{
	TEXTMETRIC tm;
	if (cWnd == NULL) return;

	CFont *font = cWnd->GetFont ();

	CDC *cDC = cWnd->GetDC ();
	cDC->SelectObject (font);
	CSize size = cDC->GetTextExtent (_T("X"), 1);
	cDC->GetTextMetrics (&tm);
	cWnd->ReleaseDC (cDC);

	FontSize = size;
	tmHeight = tm.tmHeight;
	tmWidth  = tm.tmAveCharWidth;
	CRect rect;
	cWnd->GetClientRect (&rect);
	width = rect.right / tm.tmAveCharWidth;
}

void CCtrlInfo::SetFont (CFont *font)
{
	TEXTMETRIC tm;
	if (cWnd == NULL) return;

	cWnd->SetFont (font);
	cWnd->SetFont (font);
	CDC *cDC = cWnd->GetDC ();
	cDC->SelectObject (font);
	CSize size = cDC->GetTextExtent (_T("X"), 1);
	cDC->GetTextMetrics (&tm);
	cWnd->ReleaseDC (cDC);

	double fWidth  = (double)  size.cx / FontSize.cx;  
	double fHeight = (double)  size.cy / FontSize.cy;  

	CRect rect;
	cWnd->GetClientRect (&rect);
	rect.right  = (int) (double) (fWidth * rect.right + 0.5);
	rect.top    = (int) (double) (fHeight * rect.top + 0.5);
	rect.bottom = (int) (double) (fHeight * rect.bottom + 0.5);
	cWnd->MoveWindow (&rect, FALSE);
}

