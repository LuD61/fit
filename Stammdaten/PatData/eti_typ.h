#ifndef _ETI_TYP_DEF
#define _ETI_TYP_DEF

#include "dbclass.h"

struct ETI_TYP {
   long           eti;
   TCHAR          eti_bz[37];
   short          max_zei_pos;
   short          sg01;
   short          ze01;
   short          sg02;
   short          ze02;
   short          sg03;
   short          ze03;
   short          sg04;
   short          ze04;
   short          sg05;
   short          ze05;
   short          sg06;
   short          ze06;
   short          sg07;
   short          ze07;
   short          sg08;
   short          ze08;
   short          sg09;
   short          ze09;
   short          sg10;
   short          ze10;
   short          sg11;
   short          ze11;
   short          sg12;
   short          ze12;
   short          sg13;
   short          ze13;
   short          sg14;
   short          ze14;
   short          sg15;
   short          ze15;
   short          sg16;
   short          ze16;
   short          sg17;
   short          ze17;
   short          sg18;
   short          ze18;
   short          sg19;
   short          ze19;
   short          sg20;
   short          ze20;
   short          sg_std;
   short          eti_br;
   short          eti_hoe;
};
extern struct ETI_TYP eti_typ, eti_typ_null;

#line 7 "eti_typ.rh"

class ETI_TYP_CLASS : public DB_CLASS 
{
       private :
               virtual void prepare ();

       public :
               ETI_TYP eti_typ;   
               ETI_TYP_CLASS (): DB_CLASS ()
               {
	       }
};
#endif