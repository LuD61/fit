#include "StdAfx.h"
#include "sginfos.h"

CSgInfos::CSgInfos(void)
{
}

CSgInfos::~CSgInfos(void)
{
}

void CSgInfos::Add (CSgInfo *si)
{

//	if (si->sg == 0 || si->ze == 0)
	if ((si->sg == 0) && (FindSg (0) != -1))
	{
		delete si;
		return;
	}
	CVector::Add (si);
}

CSgInfo *CSgInfos::GetNext ()
{
	return (CSgInfo *) CVector::GetNext ();
}

CSgInfo *CSgInfos::Get (int idx)
{
	return (CSgInfo *) CVector::Get (idx);
}

int CSgInfos::GetZe (int sg)
{
	CSgInfo *si;
	FirstPosition ();
	while ((si = GetNext ()) != NULL)
	{
		if (si->sg == sg)
		{
			return si->ze;
		}
	}
	return -1;
}

void CSgInfos::Clone (CSgTab& SgTab)
{
	SgTab.DestroyAll ();
	CSgInfo *si;
	FirstPosition ();
	while ((si = GetNext ()) != NULL)
	{
		SgTab.Add (new CSg ((short) si->sg, (short) si->ze));
	}
}

int CSgInfos::FindSg (int sg)
{
	int idx = 0;
	CSgInfo *si;
	FirstPosition ();
	while ((si = GetNext ()) != NULL)
	{
		if (si->sg == sg) return idx;
		idx ++;
	}
	return -1;
}
