// PazView.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "PatData.h"
#include "PazView.h"

#define CXPLUS 33
#define CYPLUS 45

// CPazView

IMPLEMENT_DYNCREATE(CPazView, DbFormView)

CPazView::CPazView()
	: DbFormView(CPazView::IDD)
{
	m_PropertySheet = NULL;
	m_PazPflegPage = NULL;
	hBrush = NULL;
	staticBrush = NULL;
}

CPazView::~CPazView()
{
		DeletePropertySheet ();
}

void CPazView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPazView, DbFormView)
	ON_WM_CTLCOLOR ()
//	ON_WM_SIZE ()
	ON_COMMAND(ID_LANGUAGE, OnLanguage)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_COMMAND(ID_BACK, OnBack)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_COMMAND(ID_TABCOPY, OnTabcopy)
	ON_COMMAND(ID_TABPASTE, OnTabpaste)
	ON_COMMAND(ID_TEXT_CENT, OnTextCent)
	ON_COMMAND(ID_TEXT_LEFT, OnTextLeft)
	ON_COMMAND(ID_TEXT_RIGHT, OnTextRight)
END_MESSAGE_MAP()


// CPazView-Diagnose

#ifdef _DEBUG
void CPazView::AssertValid() const
{
	CFormView::AssertValid();
}

void CPazView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG


// CPazView-Meldungshandler

void CPazView::OnInitialUpdate()
{
	DbFormView::OnInitialUpdate();

	m_PropertySheet = new CDbPropertySheet(_T("Preisauszeichnerdaten"));
    m_PropertySheet->m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
	m_PazPflegPage = new CPazPflegPage;
	m_PazPflegPageEx = new CPazPflegPageEx;
	m_PazPflegPage->PazPflegPageEx = m_PazPflegPageEx;
	m_PropertySheet->AddPage (m_PazPflegPage);
	m_PazPflegPageEx->PazPflegPage = m_PazPflegPage;
	m_PropertySheet->AddPage (m_PazPflegPageEx);
    m_PropertySheet->m_psh.hwndParent = m_hWnd;
    m_PropertySheet->m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
    m_PropertySheet->m_psh.pszCaption = _T("Simple");
    m_PropertySheet->m_psh.nStartPage = 0;

	m_PropertySheet->Create (this, WS_CHILD | WS_VISIBLE);
	CRect cRect;
    m_PropertySheet->GetClientRect (&cRect);
    m_PropertySheet->MoveWindow (0, 0,
		                        cRect.right, cRect.bottom);
	CSize Size = GetTotalSize ();
	if (Size.cy < (cRect.bottom) + 10)
	{
		Size.cy = cRect.bottom + 10;
	}
	if (Size.cx < (cRect.right) + 10)
	{
		Size.cx = cRect.right + 10;
	}
	SetScrollSizes (MM_TEXT, Size);
//	GetClientRect (&cRect);
	CRect pRect;
	CRect pcRect;
	CWnd *ChildFrame = GetParent ();
	CWnd *MainFrame = ChildFrame->GetParent ();
	ChildFrame->GetWindowRect (&pRect);
	MainFrame->ScreenToClient (&pRect);
	MainFrame->GetClientRect (&pcRect);
	BOOL Resize = FALSE;
	if (pcRect.right > pRect.left + cRect.right + CXPLUS)
	{
		pRect.right  = pRect.left + cRect.right + CXPLUS;
		Resize = TRUE;
	}
	if (pcRect.bottom > pRect.top + cRect.bottom + CYPLUS)
	{
		pRect.bottom = pRect.top + cRect.bottom + CYPLUS;
		Resize = TRUE;
	}
	if (Resize)
	{
		GetParent ()->MoveWindow (&pRect, TRUE);
	}
}

void CPazView::OnSize(UINT nType, int cx, int cy)
{
}

HBRUSH CPazView::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	if (hBrush == NULL)
	{
		hBrush = CreateSolidBrush (Color);
		staticBrush = CreateSolidBrush (Color);
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
 		    return staticBrush;
	}
	return DbFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CPazView::OnLanguage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	TCHAR CodePageName [256];
    LPTSTR etc;

	memset (CodePageName, 0, sizeof (CodePageName));
    OPENFILENAME fnstruct;
    static LPCTSTR lpstrFilter = 
            _T("Sprache\0*.*\0,\0");

    etc = _tgetenv (_T("BWSETC"));
	if (etc == NULL) return;
	CString InitialDir;
	InitialDir.Format (_T("%s\\CodePage"), etc);
    ZeroMemory (&fnstruct, sizeof (fnstruct));
    fnstruct.lStructSize = sizeof (fnstruct);
	fnstruct.hwndOwner   = this->m_hWnd;
    fnstruct.lpstrFile   = CodePageName;
    fnstruct.nMaxFile    = 255;
    fnstruct.lpstrFilter = lpstrFilter;
    fnstruct.lpstrInitialDir  = InitialDir.GetBuffer ();
	fnstruct.lpstrTitle = _T("Sprache ausw�hlen");
    BOOL ret = GetOpenFileName (&fnstruct);
	if (ret)
	{
		m_PazPflegPage->Code.DestroyAll ();
		CString CodeFile = CodePageName;
		m_PazPflegPage->Code.Load (CodeFile);
	}
}

void CPazView::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_PazPflegPage->write ();
	}
}

void CPazView::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_PazPflegPage->OnDelete ();
	}
}

void CPazView::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
			m_PazPflegPage->StepBack ();
	}
}

void CPazView::DeletePropertySheet ()
{
	try
	{
		if (m_PazPflegPage != NULL)
		{
			m_PropertySheet->RemovePage (m_PazPflegPage);
		}
		if (m_PazPflegPageEx != NULL)
		{
			m_PropertySheet->RemovePage (m_PazPflegPageEx);
		}

		if (m_PropertySheet != NULL)
		{
			delete m_PropertySheet;
			m_PropertySheet = NULL;
		}

		m_PazPflegPage->PazPflegPageEx = NULL;
		m_PazPflegPage->PazProperty = NULL;
		m_PazPflegPageEx->PazPflegPage = NULL;
		m_PazPflegPageEx->PazProperty = NULL;

		if (m_PazPflegPage != NULL)
		{
			delete m_PazPflegPage;
		}

		if (m_PazPflegPageEx != NULL)
		{
			delete m_PazPflegPageEx;
		}
	}
	catch (...) {}
}


void CPazView::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_PazPflegPage->OnCopy ();
	}
}

void CPazView::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_PazPflegPage->OnPaste ();
	}
}

void CPazView::OnTabcopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_PazPflegPage->OnCopyEx ();
	}
}

void CPazView::OnTabpaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_PazPflegPage->OnPasteEx ();
	}
}

void CPazView::OnTextCent()
{
	if (m_PropertySheet != NULL)
	{
		m_PazPflegPage->OnTextCent ();
	}
}

void CPazView::OnTextLeft()
{
	if (m_PropertySheet != NULL)
	{
		m_PazPflegPage->OnTextLeft ();
	}
}

void CPazView::OnTextRight()
{
	if (m_PropertySheet != NULL)
	{
		m_PazPflegPage->OnTextRight ();
	}
}
