#ifndef _PAZPFLEGPAGEEX_DEF
#define _PAZPFLEGPAGEEX_DEF
#pragma once
#include "resource.h"
#include "DbPropertyPage.h"
#include "ListDropTarget.h"
#include "CtrlGrid.h"
#include "CtrlLine.h"
#include "A_kun_gx.h"
#include "A_bas.h"
#include "A_hndw.h"
#include "A_eig.h"
#include "A_eig_div.h"
#include "Pr_a_gx.h"
#include "Eti_typ.h"
#include "Kun.h"
#include "Mdn.h"
#include "Adr.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "FormTab.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "ChoiceA.h"
#include "ChoiceEti.h"
#include "AKunGxChoice.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "DbFormView.h"
#include "CodeProperties.h"
#include "ChoiceTxt.h"
#include "ChoicePazCab.h"
#include "mo_progcfg.h"

// CPazPflegPage-Dialogfeld

class CPazPflegPageEx : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CPazPflegPageEx)

public:
	CPazPflegPageEx ();
	virtual ~CPazPflegPageEx();

// Dialogfelddaten
	enum { IDD = IDD_PAZPFLEGPAGEEX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	PROG_CFG Cfg;

	virtual BOOL OnInitDialog();
public:
	enum
	{
		HNDW = 1,
		EIG = 2,
		EIG_DIV = 3
	} A_TYP;

	A_KUN_GX_CLASS *A_kun_gx;
	A_BAS_CLASS *A_bas;
	A_HNDW_CLASS *A_hndw;
	A_EIG_CLASS *A_eig;
	A_EIG_DIV_CLASS *A_eig_div;
	PR_A_GX_CLASS *Pr_ausz_gx;
	ETI_TYP_CLASS *Eti_typ;
	KUN_CLASS *Kun;
	ADR_CLASS *KunAdr;
	MDN_CLASS *Mdn;
	ADR_CLASS *MdnAdr;
	PTABN_CLASS Ptabn;

	CFormTab Form;
	CFormTab EtiForm;

	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid KunGrid;
	CCtrlGrid AGrid;
	CCtrlGrid EtiGrid;
	CCtrlGrid DeviseGrid;
	CCtrlGrid SuGrid;

	CListDropTarget dropTarget;

    CPropertySheet *PazProperty;
	CDbPropertyPage *PazPflegPage;
    DbFormView *View;
	CFrameWnd *Frame;
	CButton m_AKunGxChoice;
	BOOL ModalChoice;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	CButton m_TxtNrChoice;
	CButton m_KopfTextChoice;
	CButton m_MhdTextChoice;
	void Register ();
	CEdit m_MdnName;
	CStatic m_LKunBran2;
	CComboBoxEx m_KunBran2;
	CStatic m_LKun;
	CNumEdit m_Kun;
	CButton m_KunChoice;
	CEdit m_KunName;
	CStatic m_HeadBorder;
	CStatic m_DataBorder;
	CStatic m_LA;
	CNumEdit m_A;
	CButton m_AChoice;
	CEdit m_ABz1;
    CVector HeadControls;

	CCodeProperties Code;
	BOOL ReadPr;
	BOOL UsePazCab;

    BOOL OnReturn ();
    BOOL OnKeyup ();

	short *SgTab[20];
	short *ZeTab[20];

	double EtiAnz;
	double PaletteAnz;
	double LdPr;

	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
    void EnableHeadControls (BOOL enable);
    void FillKunBran2 ();
    afx_msg void OnAKunGxChoice (); 
	CStatic m_EtiGroup;
	CStatic m_LEtiEtikett;
	CStatic m_LEtiEinh;
	CStatic m_LEtiMe;
	CStatic m_LGebEti;
	CButton m_EtiEtikett;
	CComboBox m_EtiEinh;
	CDecimalEdit m_EtiMe;
    void FillEinh (CWnd *);
	CStatic m_LPaletteEti;
	CButton m_PaletteEtikett;
	CComboBox m_PaletteEinh;
	CDecimalEdit m_PaletteMe;
	CStatic m_LPosEtikett;
	CButton m_PosEtikett;
	CStatic m_LGewPar;
	CNumEdit m_GewPar;
	CStatic m_LAutoPar;
	CNumEdit m_AutoPar;
	CStatic m_LVpPar;
	CNumEdit m_VpPar;
	CStatic m_LSonderEti;
	CNumEdit m_SonderEti;
	CStatic m_LAuszArt;
	CComboBox m_AuszArt;
	void FillAuszArt ();
	CStatic m_LDevise;
	CComboBox m_Devise;
	CComboBox m_Devise2;
    void FillPTab (CWnd *Control, LPCTSTR PtItem);
    void TestEtiPTab (CWnd *Control);
	CStatic m_LPrRechKz;
	CButton m_PrRechKz;
	CStatic m_LEan;
	CStatic m_LCost;
	CStatic m_LEti_typ;
	CComboBox m_Eti_typ;
	CNumEdit m_Ean;
	CComboBox m_Cab;
	CStatic m_LEtiSum1;
	CComboBox m_EtiSum1;
	CNumEdit m_Ean1;
	CComboBox m_Cab1;
	CStatic m_LEtiSum2;
	CComboBox m_EtiSum2;
	CNumEdit m_Ean2;
	CComboBox m_Cab2;
	CStatic m_LEtiSum3;
	CComboBox m_EtiSum3;
	CNumEdit m_Ean3;
	CComboBox m_Cab3;
	CStatic m_LEanNve;
	CStatic m_LCostNve;
	CStatic m_LEtiNve1;
//	CEdit m_EtiNve1;
	CComboBox m_EtiNve1;
	CNumEdit m_EanNve1;
	CComboBox m_CabNve1;
	CStatic m_LEtiNve2;
//	CEdit m_EtiNve2;
	CComboBox m_EtiNve2;
	CNumEdit m_EanNve2;
	CComboBox m_CabNve2;
	CStatic m_VpBorder;
	CStatic m_LLdPr;
	CDecimalEdit m_LdPr;
    void SetDropDownWidth (CComboBox *);
	void FillPazCab (CWnd *Control, LPCTSTR PtItem);
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
	afx_msg void OnCbnDropdownCab();
	int ChoicePazCab ();
	BOOL TestComboBoxes (HWND, CPoint);
	BOOL TestCabF9 ();
};
#endif