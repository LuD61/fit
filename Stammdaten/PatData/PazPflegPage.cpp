// PazPflegPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "PatData.h"
#include "PazPflegPage.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "pazpflegpage.h"
#include "token.h"
#include "pazpflegpage.h"
// #include "bild160.h"

#define RECCHANGE                       WM_USER + 2000

// CPazPflegPage-Dialogfeld

HANDLE CPazPflegPage::PriceLib = NULL;
HANDLE CPazPflegPage::Write160Lib = NULL;

IMPLEMENT_DYNAMIC(CPazPflegPage, CDbPropertyPage)
CPazPflegPage::CPazPflegPage()
	: CDbPropertyPage(CPazPflegPage::IDD)
{
	PazPflegPageEx = NULL;
	ReadPr = TRUE;
	Use_atexte = FALSE;
	hBrush = NULL;
	hBrushStatic = NULL;
	ChoiceA = NULL;
	ChoiceMdn = NULL;
	ChoiceKun = NULL;
	ChoiceEti = NULL;
	ChoiceTxt = NULL;
	ChoiceATxt = NULL;
	Choice = NULL;
	ModalChoiceA = TRUE;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	ModalChoiceEti = TRUE;
	ModalChoiceTxt = TRUE;
	ModalChoice = TRUE;
	WithFreiText = TRUE;
	text_nr = 0l;
	GenTextNr = new CGenNr (1, 0, _T("txt_nr"));
	TextNrCursor = -1;
	TaraReadOnly = FALSE;
	Write160 = TRUE;

	SgTab[0] = &Eti_typ.eti_typ.sg01;
	SgTab[1] = &Eti_typ.eti_typ.sg02;
	SgTab[2] = &Eti_typ.eti_typ.sg03;
	SgTab[3] = &Eti_typ.eti_typ.sg04;
	SgTab[4] = &Eti_typ.eti_typ.sg05;
	SgTab[5] = &Eti_typ.eti_typ.sg06;
	SgTab[6] = &Eti_typ.eti_typ.sg07;
	SgTab[7] = &Eti_typ.eti_typ.sg08;
	SgTab[8] = &Eti_typ.eti_typ.sg09;
	SgTab[9] = &Eti_typ.eti_typ.sg10;
	SgTab[10] = &Eti_typ.eti_typ.sg11;
	SgTab[11] = &Eti_typ.eti_typ.sg12;
	SgTab[12] = &Eti_typ.eti_typ.sg13;
	SgTab[13] = &Eti_typ.eti_typ.sg14;
	SgTab[14] = &Eti_typ.eti_typ.sg15;
	SgTab[15] = &Eti_typ.eti_typ.sg16;
	SgTab[16] = &Eti_typ.eti_typ.sg17;
	SgTab[17] = &Eti_typ.eti_typ.sg18;
	SgTab[18] = &Eti_typ.eti_typ.sg19;
	SgTab[19] = &Eti_typ.eti_typ.sg20;

	ZeTab[0] = &Eti_typ.eti_typ.ze01;
	ZeTab[1] = &Eti_typ.eti_typ.ze02;
	ZeTab[2] = &Eti_typ.eti_typ.ze03;
	ZeTab[3] = &Eti_typ.eti_typ.ze04;
	ZeTab[4] = &Eti_typ.eti_typ.ze05;
	ZeTab[5] = &Eti_typ.eti_typ.ze06;
	ZeTab[6] = &Eti_typ.eti_typ.ze07;
	ZeTab[7] = &Eti_typ.eti_typ.ze08;
	ZeTab[8] = &Eti_typ.eti_typ.ze09;
	ZeTab[9] = &Eti_typ.eti_typ.ze10;
	ZeTab[10] = &Eti_typ.eti_typ.ze11;
	ZeTab[11] = &Eti_typ.eti_typ.ze12;
	ZeTab[12] = &Eti_typ.eti_typ.ze13;
	ZeTab[13] = &Eti_typ.eti_typ.ze14;
	ZeTab[14] = &Eti_typ.eti_typ.ze15;
	ZeTab[15] = &Eti_typ.eti_typ.ze16;
	ZeTab[16] = &Eti_typ.eti_typ.ze17;
	ZeTab[17] = &Eti_typ.eti_typ.ze18;
	ZeTab[18] = &Eti_typ.eti_typ.ze19;
	ZeTab[19] = &Eti_typ.eti_typ.ze20;
	CUniFormField::Code = &Code;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_KunBran2);
	HeadControls.Add (&m_Kun);
	HeadControls.Add (&m_A);

/*
	if (ReadPr && PriceLib == NULL)
	{
		LPTSTR bws;
		bws = _tgetenv (_T("bws"));
		CString PreisDll;
		if (bws != NULL)
		{
			PreisDll.Format (_T("%s\\bin\\preise.dll"), bws);
		}
		else
		{
			PreisDll = _T("preise.dll");
		}
		PriceLib = LoadLibrary (PreisDll.GetBuffer ());
	}
    preise_holen = (int (*) (short, short, short, int ,double, 
		            LPSTR, short*, double *, double *)) 
					GetProcAddress ((HMODULE) PriceLib, "preise_holen");
	SetPriceDbName = (int (*) (LPSTR))
					GetProcAddress ((HMODULE) PriceLib, "SetPriceDbName");

	SetPriceDb = (int (*) (HENV, HDBC))
					GetProcAddress ((HMODULE) PriceLib, "SetPriceDb");
	if (SetPriceDbName != NULL)
	{
		(*SetPriceDbName) ("bws");
	}
*/
    dbild160 = NULL;
	dOpenDbase = NULL;
	preise_holen = NULL;
	BranCursor = -1;
	ReadPr = TRUE;
	DefaultMdn = 0;
	DefaultEti = 1;
}


CPazPflegPage::~CPazPflegPage()
{
	A_kun_gx.dbclose ();
	Kun.dbclose ();
	KunAdr.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	Pr_ausz_gx.dbclose ();
	Eti_typ.dbclose ();
	A_bas.dbclose ();
	A_hndw.dbclose ();
	A_eig.dbclose ();
	A_eig_div.dbclose ();
	Ptabn.dbclose ();

	if (TextNrCursor != -1)
	{
		A_kun_gx.sqlclose (TextNrCursor);
	}

	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
	}
	if (ChoiceA != NULL)
	{
		delete ChoiceA;
	}
	if (ChoiceKun != NULL)
	{
		delete ChoiceKun;
	}
	if (ChoiceEti != NULL)
	{
		delete ChoiceEti;
	}
	if (ChoiceTxt != NULL)
	{
		delete ChoiceTxt;
	}
	if (ChoiceATxt != NULL)
	{
		delete ChoiceATxt;
	}
	if (Choice != NULL)
	{
		delete Choice;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
	EtiForm.FirstPosition ();
	while ((f = (CFormField *) EtiForm.GetNext ()) != NULL)
	{
		delete f;
	}
	EtiForm.Init ();
}

void CPazPflegPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LKUN_BRAN2, m_LKunBran2);
	DDX_Control(pDX, IDC_KUN_BRAN2, m_KunBran2);
	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_KUNNAME, m_KunName);
	DDX_Control(pDX, IDC_HEADBORDER, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDER, m_DataBorder);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_A_BZ1, m_ABz1);
	DDX_Control(pDX, IDC_LSG, m_LSg);
	DDX_Control(pDX, IDC_LPAZBZ, m_LPazBz);
	DDX_Control(pDX, IDC_SG1, m_Sg1);
	DDX_Control(pDX, IDC_PAZBZ1, m_PazBz1);
	DDX_Control(pDX, IDC_SG2, m_Sg2);
	DDX_Control(pDX, IDC_PAZBZ2, m_PazBz2);
	DDX_Control(pDX, IDC_LETINR, m_LEtiNr);
	DDX_Control(pDX, IDC_ETINR, m_EtiNr);
	DDX_Control(pDX, IDC_ETI_BZ, m_EtiBz);
	DDX_Control(pDX, IDC_LA_KUN, m_LAKun);
	DDX_Control(pDX, IDC_A_KUN, m_AKun);
	DDX_Control(pDX, IDC_SG3, m_Sg3);
	DDX_Control(pDX, IDC_LME_EINH_KUN, m_LMeEinhKun);
	DDX_Control(pDX, IDC_ME_EINH_KUN, m_MeEinhKun);
	DDX_Control(pDX, IDC_LINH, m_LInh);
	DDX_Control(pDX, IDC_INH, m_Inh);
	DDX_Control(pDX, IDC_LTARA, m_LTara);
	DDX_Control(pDX, IDC_TARA, m_Tara);
	DDX_Control(pDX, IDC_LHBK_ZTR, m_LHbkZtr);
	DDX_Control(pDX, IDC_HBK_ZTR, m_HbkZtr);
	DDX_Control(pDX, IDC_TEXT_BORDER, m_TextNumbers);
	DDX_Control(pDX, IDC_LPR_ZUT_TXT, m_LPrZutTxt);
	DDX_Control(pDX, IDC_PR_ZUT_TXT, m_PrZutTxt);
	DDX_Control(pDX, IDC_LKOPF_TXT, m_LKopfTxt);
	DDX_Control(pDX, IDC_KOPF_TXT, m_KopfTxt);
	DDX_Control(pDX, IDC_LMHD_TXT, m_LMhdTxt);
	DDX_Control(pDX, IDC_MHD_TEXT, m_MhdTxt);
	DDX_Control(pDX, IDC_LGEN_TXT, m_LGenTxt);
	DDX_Control(pDX, IDC_GEN_TXT, m_GenTxt);
	DDX_Control(pDX, IDC_BZ2, m_ABz2);
	DDX_Control(pDX, IDC_LZUTGEW, m_LZutGew);
	DDX_Control(pDX, IDC_ZUTGEW, m_ZutGew);
	DDX_Control(pDX, IDC_LZUTPROZ, m_LZutProz);
	DDX_Control(pDX, IDC_ZUTPROZ, m_ZutProz);
	DDX_Control(pDX, IDC_LFREITEXT1, m_LFreiText1);
	DDX_Control(pDX, IDC_FREITEXT1, m_FreiText1);
	DDX_Control(pDX, IDC_FREITEXT2, m_FreiText2);
	DDX_Control(pDX, IDC_FREITEXT3, m_FreiText3);
	DDX_Control(pDX, IDC_FREITEXT4, m_FreiText4);
	DDX_Control(pDX, IDC_FREITEXT5, m_FreiText5);
	DDX_Control(pDX, IDC_LFREITEXT2, m_LFreiText2);
	DDX_Control(pDX, IDC_FREITEXT6, m_FreiText6);
	DDX_Control(pDX, IDC_FREITEXT7, m_FreiText7);
	DDX_Control(pDX, IDC_LFREITEXT3, m_LFreiText3);
	DDX_Control(pDX, IDC_LFREITEXT4, m_LFreiText4);
	DDX_Control(pDX, IDC_LFREITEXT5, m_LFreiText5);
	DDX_Control(pDX, IDC_LFREITEXT6, m_LFreiText6);
	DDX_Control(pDX, IDC_LFREITEXT7, m_LFreiText7);
	DDX_Control(pDX, IDC_ETIKETT, m_etikett);
}


BEGIN_MESSAGE_MAP(CPazPflegPage, CPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_KILLFOCUS ()
	ON_BN_CLICKED(IDC_ACHOICE ,   OnAchoice)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_BN_CLICKED(IDC_KUNCHOICE , OnKunchoice)
	ON_BN_CLICKED(IDC_ETICHOICE , OnEtiChoice)
	ON_BN_CLICKED(IDC_TEXTNR_CHOICE , OnTextchoice)
	ON_BN_CLICKED(IDC_KOPFTEXT_CHOICE , OnKopfTextchoice)
	ON_BN_CLICKED(IDC_MHDTEXT_CHOICE , OnMhdTextchoice)
	ON_BN_CLICKED(IDC_FREITEXT1_CHOICE , OnFreitext1choice)
	ON_BN_CLICKED(IDC_FREITEXT2_CHOICE , OnFreitext2choice)
	ON_BN_CLICKED(IDC_FREITEXT3_CHOICE , OnFreitext3choice)
	ON_BN_CLICKED(IDC_FREITEXT4_CHOICE , OnFreitext4choice)
	ON_BN_CLICKED(IDC_FREITEXT5_CHOICE , OnFreitext5choice)
	ON_BN_CLICKED(IDC_FREITEXT6_CHOICE , OnFreitext6choice)
	ON_BN_CLICKED(IDC_FREITEXT7_CHOICE , OnFreitext7choice)
//	ON_EN_KILLFOCUS(IDC_ETINR, OnEnKillfocusEtinr)
    ON_COMMAND (IDC_AKUNGXCHOICE, OnAKunGxChoice)
	ON_COMMAND (SELECTED, OnAKunGxSelected)
	ON_COMMAND (CANCELED, OnAKunGxCanceled)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
//	ON_EN_KILLFOCUS(IDC_PR_ZUT_TXT, OnEnKillfocusPrZutTxt)
	ON_EN_CHANGE(IDC_PR_ZUT_TXT, OnEnChangePrZutTxt)
	ON_COMMAND(ID_TEXT_CENT, OnTextCent)
	ON_COMMAND(ID_TEXT_LEFT, OnTextLeft)
	ON_COMMAND(ID_TEXT_RIGHT, OnTextRight)
	ON_EN_KILLFOCUS(IDC_ZUTGEW, OnEnKillfocusZutgew)
	ON_EN_KILLFOCUS(IDC_ZUTPROZ, OnEnKillfocusZutproz)
END_MESSAGE_MAP()


// CPazPflegPage-Meldungshandler


void CPazPflegPage::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CPazPflegPage::OnInitDialog()
{
	CDialog::OnInitDialog();

	Cfg.SetProgName (_T("pazdata"));
	if (Cfg.GetCfgValue (_T("UseOdbc"), PROG_CFG::cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("Use_atexte"), PROG_CFG::cfg_v) == TRUE)
    {
			Use_atexte = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("ReadPr"), PROG_CFG::cfg_v) == TRUE)
    {
			ReadPr = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("ModalChoice"), PROG_CFG::cfg_v) == TRUE)
    {
			ModalChoice = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("WithFreiText"), PROG_CFG::cfg_v) == TRUE)
    {
			WithFreiText = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("Write160"), PROG_CFG::cfg_v) == TRUE)
    {
			Write160 = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("DefaultMdn"), PROG_CFG::cfg_v) == TRUE)
    {
			DefaultMdn = _tstoi (PROG_CFG::cfg_v);
	}
    if (Cfg.GetCfgValue (_T("DefaultEti"), PROG_CFG::cfg_v) == TRUE)
    {
			DefaultEti = _tstoi (PROG_CFG::cfg_v);
	}
    if (Cfg.GetCfgValue (_T("TaraReadOnly"), PROG_CFG::cfg_v) == TRUE)
    {
			TaraReadOnly = _tstoi (PROG_CFG::cfg_v);
    }
	Cfg.CloseCfg ();


// DLL's laden

// Preise lesen

	LPTSTR bws;
	bws = _tgetenv (_T("bws"));
	if (ReadPr && PriceLib == NULL)
	{
		CString PreisDll;
		if (bws != NULL)
		{
			PreisDll.Format (_T("%s\\bin\\preise.dll"), bws);
		}
		else
		{
			PreisDll = _T("preise.dll");
		}
		PriceLib = LoadLibrary (PreisDll.GetBuffer ());
	}
    preise_holen = (int (*) (short, short, short, int ,double, 
		            LPSTR, short*, double *, double *)) 
					GetProcAddress ((HMODULE) PriceLib, "preise_holen");
	SetPriceDbName = (int (*) (LPSTR))
					GetProcAddress ((HMODULE) PriceLib, "SetPriceDbName");

	SetPriceDb = (int (*) (HENV, HDBC))
					GetProcAddress ((HMODULE) PriceLib, "SetPriceDb");
	if (SetPriceDbName != NULL)
	{
		(*SetPriceDbName) ("bws");
	}

// 160-iger schreiben

	if (Write160 && Write160Lib == NULL)
	{
		CString W160Dll;
		if (bws != NULL)
		{
			W160Dll.Format (_T("%s\\bin\\bild160dll.dll"), bws);
		}
		else
		{
			W160Dll = _T("bild160dll.dll");
		}
		Write160Lib = LoadLibrary (W160Dll.GetBuffer ());

		if (Write160Lib != NULL)
		{
			dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
			dOpenDbase = (BOOL (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "OpenDbase");
			if (dOpenDbase != NULL)
			{
				(*dOpenDbase) ("bws");
			}
		}
	}

// Ende DLL's laden


	if (PazPflegPageEx != NULL)
	{
		PazPflegPageEx->A_kun_gx = &A_kun_gx;
		PazPflegPageEx->A_bas = &A_bas;
		PazPflegPageEx->A_hndw = &A_hndw;
		PazPflegPageEx->A_eig = &A_eig;
		PazPflegPageEx->A_eig_div = &A_eig_div;
		PazPflegPageEx->Pr_ausz_gx = &Pr_ausz_gx;
		PazPflegPageEx->Eti_typ = &Eti_typ;
		PazPflegPageEx->Kun = &Kun;
		PazPflegPageEx->KunAdr = &KunAdr;
		PazPflegPageEx->Mdn = &Mdn;
		PazPflegPageEx->MdnAdr = &MdnAdr;
	}


	PazProperty = (CPropertySheet *) GetParent ();
	View = (DbFormView *) PazProperty->GetParent ();
	Frame = (CFrameWnd *) View->GetParent ();

	A_kun_gx.opendbase (_T("bws"));
	A_kun_gx.sqlout ((long *) &BranKun, SQLLONG, 0);
	A_kun_gx.sqlin ((long *) &A_kun_gx.a_kun_gx.mdn, SQLSHORT, 0);
	A_kun_gx.sqlin ((long *) &A_kun_gx.a_kun_gx.fil, SQLSHORT, 0);
	A_kun_gx.sqlin ((long *) A_kun_gx.a_kun_gx.kun_bran2, SQLCHAR, sizeof (A_kun_gx.a_kun_gx.kun_bran2));
	BranCursor = A_kun_gx.sqlcursor (_T("select kun from kun ")
								     _T("where mdn = ? ")
								     _T("and fil = ? ")
								     _T("and kun_bran2 = ?"));
	GetFirstEti ();
    memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
    memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
    memcpy (&Kun.kun, &kun_null, sizeof (KUN));
    memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
    memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
    memcpy (&A_kun_gx.a_kun_gx, &a_kun_gx_null, sizeof (A_KUN_GX));
    memcpy (&Eti_typ.eti_typ, &eti_typ_null, sizeof (ETI_TYP));
	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();

	Mdn.mdn.mdn = DefaultMdn;
	CRect bRect (0, 0, 100, 20);;
	m_AKunGxChoice.Create (_T("Auswahl F8"),
		WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON | 
		BS_CENTER | BS_VCENTER, 
							bRect, this, IDC_AKUNGXCHOICE);

    Keys.Add (new CUniFormField (&m_KunName,EDIT,   (char *)   &KunAdr.adr.adr_krz, VCHAR));
    Keys.Add (new CFormField (&m_Mdn,EDIT,       (short *)  &Mdn.mdn.mdn, VSHORT));
    Keys.Add (new CUniFormField (&m_MdnName,EDIT,   (char *)   &MdnAdr.adr.adr_krz, VCHAR));
	Keys.Add (new CUniFormField (&m_KunBran2,COMBOBOX, (char *) A_kun_gx.a_kun_gx.kun_bran2, VCHAR));
    Keys.Add (new CFormField (&m_Kun,EDIT,       (long *)   &Kun.kun.kun, VLONG));
	Keys.Add (new CFormField (&m_A,EDIT,         (double *) &A_bas.a_bas.a, VDOUBLE, 13, 0));

    Form.Add (new CUniFormField (&m_KunName,EDIT,   (char *)   &KunAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Mdn,EDIT,       (short *)  &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT,   (char *)   &MdnAdr.adr.adr_krz, VCHAR));
	Form.Add (new CUniFormField (&m_KunBran2,COMBOBOX, (char *) A_kun_gx.a_kun_gx.kun_bran2, VCHAR));
    Form.Add (new CFormField (&m_Kun,EDIT,       (long *)   &Kun.kun.kun, VLONG));
	Form.Add (new CFormField (&m_A,EDIT,         (double *) &A_bas.a_bas.a, VDOUBLE, 13, 0));
    Form.Add (new CUniFormField (&m_ABz1,EDIT,      (char *)   &A_bas.a_bas.a_bz1, VCHAR));
    Form.Add (new CUniFormField (&m_ABz2,EDIT,      (char *)   &A_bas.a_bas.a_bz2, VCHAR));
    Form.Add (new CUniFormField (&m_PazBz1,EDIT,  (char *)  A_kun_gx.a_kun_gx.a_bz3, VCHAR));

	if ( ! Use_atexte) //WAL-174
	{
		Form.Add (new CFormField (&m_Sg1,COMBOBOX, (short *) &A_kun_gx.a_kun_gx.sg1, VSHORT));
		Form.Add (new CFormField (&m_Sg2,COMBOBOX, (short *) &A_kun_gx.a_kun_gx.sg2, VSHORT));
		Form.Add (new CFormField (&m_Sg3,COMBOBOX, (short *) &A_kun_gx.a_kun_gx.sg3, VSHORT));
	}
    Form.Add (new CUniFormField (&m_PazBz2,EDIT,  (char *)  A_kun_gx.a_kun_gx.a_bz4, VCHAR));
    Form.Add (new CUniFormField (&m_AKun,EDIT,  (char *)  A_kun_gx.a_kun_gx.a_kun, VCHAR));
	Form.Add (new CFormField (&m_MeEinhKun,COMBOBOX, (short *) &A_kun_gx.a_kun_gx.me_einh_kun, VSHORT));
    Form.Add (new CFormField (&m_Inh,EDIT,  (double *)  &A_kun_gx.a_kun_gx.inh, VDOUBLE, 8, 3));
    Form.Add (new CFormField (&m_Tara,EDIT,  (double *)  &A_kun_gx.a_kun_gx.tara, VDOUBLE, 8, 3));
    Form.Add (new CFormField (&m_ZutGew,EDIT,  (double *)  &A_kun_gx.a_kun_gx.zut_gew, VDOUBLE, 8, 3));
    Form.Add (new CFormField (&m_ZutProz,EDIT,  (double *)  &A_kun_gx.a_kun_gx.zut_proz, VDOUBLE, 4, 1));
    Form.Add (new CFormField (&m_HbkZtr,EDIT,  (short *)  &A_kun_gx.a_kun_gx.hbk_ztr, VSHORT));

//    Form.Add (new CFormField (&m_PrZutTxt,EDIT,  (long *)  &A_kun_gx.a_kun_gx.text_nr, VLONG));
    Form.Add (new CFormField (&m_PrZutTxt,EDIT,  (long *)  &text_nr, VLONG));
	Form.Add (new CFormField (&m_KopfTxt,EDIT,  (long *)  &A_kun_gx.a_kun_gx.kopf_text, VLONG));
	Form.Add (new CFormField (&m_MhdTxt,EDIT,  (long *)  &A_kun_gx.a_kun_gx.mhd_text, VLONG));
	Form.Add (new CFormField (&m_FreiText1,EDIT,  (long *)  &A_kun_gx.a_kun_gx.freitext1, VLONG));
	Form.Add (new CFormField (&m_FreiText2,EDIT,  (long *)  &A_kun_gx.a_kun_gx.freitext2, VLONG));
	Form.Add (new CFormField (&m_FreiText3,EDIT,  (long *)  &A_kun_gx.a_kun_gx.freitext3, VLONG));
	Form.Add (new CFormField (&m_FreiText4,EDIT,  (long *)  &A_kun_gx.a_kun_gx.freitext4, VLONG));
	Form.Add (new CFormField (&m_FreiText5,EDIT,  (long *)  &A_kun_gx.a_kun_gx.freitext5, VLONG));
	Form.Add (new CFormField (&m_FreiText6,EDIT,  (long *)  &A_kun_gx.a_kun_gx.freitext6, VLONG));
	Form.Add (new CFormField (&m_FreiText7,EDIT,  (long *)  &A_kun_gx.a_kun_gx.freitext7, VLONG));

	Form.Add (new CFormField (&m_GenTxt,EDIT,  (long *)  &A_kun_gx.a_kun_gx.text_nr2, VLONG));

	if (Use_atexte) //WAL-174
	{
		m_EtiNr.ShowWindow (SW_HIDE);
		m_EtiBz.ShowWindow (SW_HIDE);
		m_LSg.ShowWindow (SW_HIDE);
		m_Sg1.ShowWindow (SW_HIDE);
		m_Sg2.ShowWindow (SW_HIDE);
		m_Sg3.ShowWindow (SW_HIDE);
		Form.Add (new CFormField (&m_etikett,COMBOBOX, (short *) &A_kun_gx.a_kun_gx.eti_typ, VSHORT));

	}
	else
	{
		m_etikett.ShowWindow (SW_HIDE);
		EtiForm.Add (new CFormField (&m_EtiNr,EDIT,   (long *)  &Eti_typ.eti_typ.eti, VLONG));
		EtiForm.Add (new CUniFormField (&m_EtiBz,EDIT,   (char *)  Eti_typ.eti_typ.eti_bz, VCHAR));
	}
	FillKunBran2 ();
	FillMeEinhKun ();
	FillEtikett ();  //WAL-174

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 1, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	KunGrid.Create (this, 1, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	CtrlGrid.CreateChoiceButton (m_KunChoice, IDC_KUNCHOICE, this);
	CCtrlInfo *c_KunChoice = new CCtrlInfo (&m_KunChoice, 1, 0, 1, 1);
	KunGrid.Add (c_KunChoice);

	AGrid.Create (this, 1, 2);
    AGrid.SetBorder (0, 0);
    AGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 0, 0, 1, 1);
	AGrid.Add (c_A);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);

  if  ( ! Use_atexte)//WAL-174
  {
	EtiGrid.Create (this, 1, 2);
    EtiGrid.SetBorder (0, 0);
    EtiGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_EtiNr = new CCtrlInfo (&m_EtiNr, 0, 0, 1, 1);
	EtiGrid.Add (c_EtiNr);
	CtrlGrid.CreateChoiceButton (m_EtiChoice, IDC_ETICHOICE, this);
	CCtrlInfo *c_EtiChoice = new CCtrlInfo (&m_EtiChoice, 1, 0, 1, 1);
	EtiGrid.Add (c_EtiChoice);
  }

	TxtNrGrid.Create (this, 1, 2);
    TxtNrGrid.SetBorder (0, 0);
    TxtNrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_PrZutTxt = new CCtrlInfo (&m_PrZutTxt, 0, 0, 1, 1);
	TxtNrGrid.Add (c_PrZutTxt);
	CtrlGrid.CreateChoiceButton (m_TxtNrChoice, IDC_TEXTNR_CHOICE, this);
	CCtrlInfo *c_TxtNrChoice = new CCtrlInfo (&m_TxtNrChoice, 1, 0, 1, 1);
	TxtNrGrid.Add (c_TxtNrChoice);

	KopfTextGrid.Create (this, 1, 2);
    KopfTextGrid.SetBorder (0, 0);
    KopfTextGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_KopfText = new CCtrlInfo (&m_KopfTxt, 0, 0, 1, 1);
	KopfTextGrid.Add (c_KopfText);
	CtrlGrid.CreateChoiceButton (m_KopfTextChoice, IDC_KOPFTEXT_CHOICE, this);
	CCtrlInfo *c_KopfTextChoice = new CCtrlInfo (&m_KopfTextChoice, 1, 0, 1, 1);
	KopfTextGrid.Add (c_KopfTextChoice);

	MhdTextGrid.Create (this, 1, 2);
    MhdTextGrid.SetBorder (0, 0);
    MhdTextGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_MhdText = new CCtrlInfo (&m_MhdTxt, 0, 0, 1, 1);
	MhdTextGrid.Add (c_MhdText);
	CtrlGrid.CreateChoiceButton (m_MhdTextChoice, IDC_MHDTEXT_CHOICE, this);
	CCtrlInfo *c_MhdTextChoice = new CCtrlInfo (&m_MhdTextChoice, 1, 0, 1, 1);
	MhdTextGrid.Add (c_MhdTextChoice);

	FreiText1Grid.Create (this, 1, 2);
    FreiText1Grid.SetBorder (0, 0);
    FreiText1Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_FreiText1 = new CCtrlInfo (&m_FreiText1, 0, 0, 1, 1);
	FreiText1Grid.Add (c_FreiText1);
	CtrlGrid.CreateChoiceButton (m_FreiText1Choice, IDC_FREITEXT1_CHOICE, this);
	CCtrlInfo *c_FreiText1Choice = new CCtrlInfo (&m_FreiText1Choice, 1, 0, 1, 1);
	FreiText1Grid.Add (c_FreiText1Choice);

	FreiText2Grid.Create (this, 1, 2);
    FreiText2Grid.SetBorder (0, 0);
    FreiText2Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_FreiText2 = new CCtrlInfo (&m_FreiText2, 0, 0, 1, 1);
	FreiText2Grid.Add (c_FreiText2);
	CtrlGrid.CreateChoiceButton (m_FreiText2Choice, IDC_FREITEXT2_CHOICE, this);
	CCtrlInfo *c_FreiText2Choice = new CCtrlInfo (&m_FreiText2Choice, 1, 0, 1, 1);
	FreiText2Grid.Add (c_FreiText2Choice);

	FreiText3Grid.Create (this, 1, 3);
    FreiText3Grid.SetBorder (0, 0);
    FreiText3Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_LFreiText3 = new CCtrlInfo (&m_LFreiText3, 0, 0, 1, 1);
	FreiText3Grid.Add (c_LFreiText3);
	CCtrlInfo *c_FreiText3 = new CCtrlInfo (&m_FreiText3, 1, 0, 1, 1);
	FreiText3Grid.Add (c_FreiText3);
	CtrlGrid.CreateChoiceButton (m_FreiText3Choice, IDC_FREITEXT3_CHOICE, this);
	CCtrlInfo *c_FreiText3Choice = new CCtrlInfo (&m_FreiText3Choice, 2, 0, 1, 1);
	FreiText3Grid.Add (c_FreiText3Choice);

	FreiText4Grid.Create (this, 1, 3);
    FreiText4Grid.SetBorder (0, 0);
    FreiText4Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_LFreiText4 = new CCtrlInfo (&m_LFreiText4, 0, 0, 1, 1);
	FreiText4Grid.Add (c_LFreiText4);
	CCtrlInfo *c_FreiText4 = new CCtrlInfo (&m_FreiText4, 1, 0, 1, 1);
	FreiText4Grid.Add (c_FreiText4);
	CtrlGrid.CreateChoiceButton (m_FreiText4Choice, IDC_FREITEXT4_CHOICE, this);
	CCtrlInfo *c_FreiText4Choice = new CCtrlInfo (&m_FreiText4Choice, 2, 0, 1, 1);
	FreiText4Grid.Add (c_FreiText4Choice);

	FreiText5Grid.Create (this, 1, 3);
    FreiText5Grid.SetBorder (0, 0);
    FreiText5Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_LFreiText5 = new CCtrlInfo (&m_LFreiText5, 0, 0, 1, 1);
	FreiText5Grid.Add (c_LFreiText5);
	CCtrlInfo *c_FreiText5 = new CCtrlInfo (&m_FreiText5, 1, 0, 1, 1);
	FreiText5Grid.Add (c_FreiText5);
	CtrlGrid.CreateChoiceButton (m_FreiText5Choice, IDC_FREITEXT5_CHOICE, this);
	CCtrlInfo *c_FreiText5Choice = new CCtrlInfo (&m_FreiText5Choice, 2, 0, 1, 1);
	FreiText5Grid.Add (c_FreiText5Choice);

	FreiText6Grid.Create (this, 1, 3);
    FreiText6Grid.SetBorder (0, 0);
    FreiText6Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_LFreiText6 = new CCtrlInfo (&m_LFreiText6, 0, 0, 1, 1);
	FreiText6Grid.Add (c_LFreiText6);
	CCtrlInfo *c_FreiText6 = new CCtrlInfo (&m_FreiText6, 1, 0, 1, 1);
	FreiText6Grid.Add (c_FreiText6);
	CtrlGrid.CreateChoiceButton (m_FreiText6Choice, IDC_FREITEXT6_CHOICE, this);
	CCtrlInfo *c_FreiText6Choice = new CCtrlInfo (&m_FreiText6Choice, 2, 0, 1, 1);
	FreiText6Grid.Add (c_FreiText6Choice);

	FreiText7Grid.Create (this, 1, 3);
    FreiText7Grid.SetBorder (0, 0);
    FreiText7Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_LFreiText7 = new CCtrlInfo (&m_LFreiText7, 0, 0, 1, 1);
	FreiText7Grid.Add (c_LFreiText7);
	CCtrlInfo *c_FreiText7 = new CCtrlInfo (&m_FreiText7, 1, 0, 1, 1);
	FreiText7Grid.Add (c_FreiText7);
	CtrlGrid.CreateChoiceButton (m_FreiText7Choice, IDC_FREITEXT7_CHOICE, this);
	CCtrlInfo *c_FreiText7Choice = new CCtrlInfo (&m_FreiText7Choice, 2, 0, 1, 1);
	FreiText7Grid.Add (c_FreiText7Choice);


	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 6);
    c_HeadBorder->rightspace = 20; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);

	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 3, 1, 4, 1);
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LKunBran2 = new CCtrlInfo (&m_LKunBran2, 1, 2, 1, 1);
	CtrlGrid.Add (c_LKunBran2);
	CCtrlInfo *c_KunBran2 = new CCtrlInfo (&m_KunBran2, 2, 2, 1, 1);
	CtrlGrid.Add (c_KunBran2);

	CCtrlInfo *c_LKun = new CCtrlInfo (&m_LKun, 1, 3, 1, 1);
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid = new CCtrlInfo (&KunGrid, 2, 3, 1, 1);
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 3, 3, 4, 1);
	CtrlGrid.Add (c_KunName);

	CCtrlInfo *c_LA = new CCtrlInfo (&m_LA, 1, 4, 1, 1);
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid = new CCtrlInfo (&AGrid, 2, 4, 1, 1);
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_ABz1 = new CCtrlInfo (&m_ABz1, 3, 4, 4, 1);
	CtrlGrid.Add (c_ABz1);
	CCtrlInfo *c_ABz2 = new CCtrlInfo (&m_ABz2, 3, 5, 4, 1);
	CtrlGrid.Add (c_ABz2);

	CCtrlInfo *c_AKunGxChoice = new CCtrlInfo (&m_AKunGxChoice, 1, 5, 1, 1);
	CtrlGrid.Add (c_AKunGxChoice);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 6, DOCKRIGHT, DOCKBOTTOM);
    c_DataBorder->rightspace = 20; 
	CtrlGrid.Add (c_DataBorder);

	CCtrlInfo *c_LEtiNr = new CCtrlInfo (&m_LEtiNr, 1, 7, 1, 1);
	CtrlGrid.Add (c_LEtiNr);

	if (Use_atexte)//WAL-174
	{
		CCtrlInfo *c_etikett = new CCtrlInfo (&m_etikett, 2, 7, 1, 1);
		CtrlGrid.Add (c_etikett);
	}
	else
	{
		CCtrlInfo *c_EtiGrid = new CCtrlInfo (&EtiGrid, 2, 7, 1, 1);
		CtrlGrid.Add (c_EtiGrid);
		CCtrlInfo *c_EtiBz = new CCtrlInfo (&m_EtiBz, 3, 7, 6, 1);
		CtrlGrid.Add (c_EtiBz);

		CCtrlInfo *c_LSg = new CCtrlInfo (&m_LSg, 1, 8, 1, 1);
		CtrlGrid.Add (c_LSg);
		CCtrlInfo *c_Sg1 = new CCtrlInfo (&m_Sg1, 1, 9, 1, 1);
		CtrlGrid.Add (c_Sg1);
		CCtrlInfo *c_Sg2 = new CCtrlInfo (&m_Sg2, 1, 10, 1, 1);
		CtrlGrid.Add (c_Sg2);
		CCtrlInfo *c_Sg3 = new CCtrlInfo (&m_Sg3, 1, 12, 1, 1);
		CtrlGrid.Add (c_Sg3);
	}


	CCtrlInfo *c_LPazBz = new CCtrlInfo (&m_LPazBz, 2, 8, 4, 1);
	CtrlGrid.Add (c_LPazBz);
	CCtrlInfo *c_PazBz1 = new CCtrlInfo (&m_PazBz1, 2, 9, DOCKRIGHT, 1);
    c_PazBz1->rightspace = 30; 
	CtrlGrid.Add (c_PazBz1);
	CCtrlInfo *c_PazBz2 = new CCtrlInfo (&m_PazBz2, 2, 10, DOCKRIGHT, 1);
    c_PazBz2->rightspace = 30; 
	CtrlGrid.Add (c_PazBz2);
	CCtrlInfo *c_LAKun = new CCtrlInfo (&m_LAKun, 2, 11, 1, 1);
	CtrlGrid.Add (c_LAKun);
	CCtrlInfo *c_AKun = new CCtrlInfo (&m_AKun, 2, 12, 1, 1);
	CtrlGrid.Add (c_AKun);
	CCtrlInfo *c_LMeEinhKun = new CCtrlInfo (&m_LMeEinhKun, 1, 13, 1, 1);
	CtrlGrid.Add (c_LMeEinhKun);
	CCtrlInfo *c_MeEinhKun = new CCtrlInfo (&m_MeEinhKun, 2, 13, 1, 1);
	CtrlGrid.Add (c_MeEinhKun);
	CCtrlInfo *c_LInh = new CCtrlInfo (&m_LInh, 1, 14, 1, 1);
	CtrlGrid.Add (c_LInh);
	CCtrlInfo *c_Inh = new CCtrlInfo (&m_Inh, 2, 14, 1, 1);
	CtrlGrid.Add (c_Inh);
	CCtrlInfo *c_LTara = new CCtrlInfo (&m_LTara, 1, 15, 1, 1);
	CtrlGrid.Add (c_LTara);
	CCtrlInfo *c_Tara = new CCtrlInfo (&m_Tara, 2, 15, 1, 1);
	CtrlGrid.Add (c_Tara);

	CCtrlInfo *c_LZutGew = new CCtrlInfo (&m_LZutGew, 1, 16, 1, 1);
	CtrlGrid.Add (c_LZutGew);
	CCtrlInfo *c_ZutGew = new CCtrlInfo (&m_ZutGew, 2, 16, 1, 1);
	CtrlGrid.Add (c_ZutGew);
	CCtrlInfo *c_LZutProz = new CCtrlInfo (&m_LZutProz, 1, 17, 1, 1);
	CtrlGrid.Add (c_LZutProz);
	CCtrlInfo *c_ZutProz = new CCtrlInfo (&m_ZutProz, 2, 17, 1, 1);
	CtrlGrid.Add (c_ZutProz);

	CCtrlInfo *c_LHbkZtr = new CCtrlInfo (&m_LHbkZtr, 1, 18, 1, 1);
	CtrlGrid.Add (c_LHbkZtr);
	CCtrlInfo *c_HbkZtr = new CCtrlInfo (&m_HbkZtr, 2, 18, 1, 1);
	CtrlGrid.Add (c_HbkZtr);

	CCtrlInfo *c_TxtSpace = new CCtrlInfo (this, 80, 3, 12, 1, 1);
	CtrlGrid.Add (c_TxtSpace);

	CCtrlInfo *c_TextNumbers = new CCtrlInfo (&m_TextNumbers, 4, 13, DOCKRIGHT, 6);
    c_TextNumbers->rightspace = 30; 
	CtrlGrid.Add (c_TextNumbers);

	CCtrlInfo *c_LKopfTxt = new CCtrlInfo (&m_LKopfTxt, 5, 14, 1, 1);
	CtrlGrid.Add (c_LKopfTxt);
	CCtrlInfo *c_KopfTextGrid = new CCtrlInfo (&KopfTextGrid, 6, 14, 1, 1);
	CtrlGrid.Add (c_KopfTextGrid);

	CCtrlInfo *c_LPrZutTxt = new CCtrlInfo (&m_LPrZutTxt, 5, 15, 1, 1);
	CtrlGrid.Add (c_LPrZutTxt);
	CCtrlInfo *c_TxtNrGrid = new CCtrlInfo (&TxtNrGrid, 6, 15, 1, 1);
	CtrlGrid.Add (c_TxtNrGrid);

	CCtrlInfo *c_LMhdTxt = new CCtrlInfo (&m_LMhdTxt, 5, 16, 1, 1);
	CtrlGrid.Add (c_LMhdTxt);
	CCtrlInfo *c_MhdTextGrid = new CCtrlInfo (&MhdTextGrid, 6, 16, 1, 1);
	CtrlGrid.Add (c_MhdTextGrid);

	CCtrlInfo *c_LFreiText1 = new CCtrlInfo (&m_LFreiText1, 5, 17, 1, 1);
	CtrlGrid.Add (c_LFreiText1);
	CCtrlInfo *c_FreiText1Grid = new CCtrlInfo (&FreiText1Grid, 6, 17, 1, 1);
	CtrlGrid.Add (c_FreiText1Grid);

	CCtrlInfo *c_LFreiText2 = new CCtrlInfo (&m_LFreiText2, 5, 18, 1, 1);
	CtrlGrid.Add (c_LFreiText2);
	CCtrlInfo *c_FreiText2Grid = new CCtrlInfo (&FreiText2Grid, 6, 18, 1, 1);
	CtrlGrid.Add (c_FreiText2Grid);

	CCtrlInfo *c_LGenTxt = new CCtrlInfo (&m_LGenTxt, 5, 18, 1, 1);
	CtrlGrid.Add (c_LGenTxt);
	CCtrlInfo *c_GenTxt = new CCtrlInfo (&m_GenTxt, 6, 18, 1, 1);
	CtrlGrid.Add (c_GenTxt);

	CCtrlInfo *c_FreiText3Grid = new CCtrlInfo (&FreiText3Grid, 7, 14, 1, 1);
	CtrlGrid.Add (c_FreiText3Grid);

	CCtrlInfo *c_FreiText4Grid = new CCtrlInfo (&FreiText4Grid, 7, 15, 1, 1);
	CtrlGrid.Add (c_FreiText4Grid);

	CCtrlInfo *c_FreiText5Grid = new CCtrlInfo (&FreiText5Grid, 7, 16, 1, 1);
	CtrlGrid.Add (c_FreiText5Grid);

	CCtrlInfo *c_FreiText6Grid = new CCtrlInfo (&FreiText6Grid, 7, 17, 1, 1);
	CtrlGrid.Add (c_FreiText6Grid);

	CCtrlInfo *c_FreiText7Grid = new CCtrlInfo (&FreiText7Grid, 7, 18, 1, 1);
	CtrlGrid.Add (c_FreiText7Grid);


	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	CtrlGrid.Display ();
    CtrlGrid.SetItemCharHeight (&m_HeadBorder, 6);
    CtrlGrid.SetItemCharHeight (&m_TextNumbers, 6);
    CtrlGrid.SetItemCharWidth (&m_AKunGxChoice, 15);
	SetFreiTextMode ();
    EnableFields (FALSE);
    Form.Show ();

	return TRUE;
}


BOOL CPazPflegPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnAKunGxChoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				else if (GetFocus () == &m_A)
				{
					OnAchoice ();
					return TRUE;
				}
				else if (GetFocus () == &m_KopfTxt)
				{
					OnKopfTextchoice ();
					return TRUE;
				}
				else if (GetFocus () == &m_PrZutTxt)
				{
					OnTextchoice ();
					return TRUE;
				}
				else if (GetFocus () == &m_MhdTxt)
				{
					OnMhdTextchoice ();
					return TRUE;
				}
				else if (GetFocus () == &m_FreiText1)
				{
					OnFreitext1choice ();
					return TRUE;
				}
				else if (GetFocus () == &m_FreiText2)
				{
					OnFreitext2choice ();
					return TRUE;
				}
				else if (GetFocus () == &m_FreiText3)
				{
					OnFreitext3choice ();
					return TRUE;
				}
				else if (GetFocus () == &m_FreiText4)
				{
					OnFreitext4choice ();
					return TRUE;
				}
				else if (GetFocus () == &m_FreiText5)
				{
					OnFreitext5choice ();
					return TRUE;
				}
				else if (GetFocus () == &m_FreiText6)
				{
					OnFreitext6choice ();
					return TRUE;
				}
				else if (GetFocus () == &m_FreiText7)
				{
					OnFreitext7choice ();
					return TRUE;
				}
				break;
			}

			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}
			if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

HBRUSH CPazPflegPage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);

    if (hBrush == NULL)
	{
		  hBrush = CreateSolidBrush (Color);
		  hBrushStatic = CreateSolidBrush (Color);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			return hBrush;
	}
/*
	else if (pWnd->GetDlgCtrlID() == IDC_ARTDAT)
	{
            pDC->SetTextColor (CaptionColor);
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	else if (pWnd->GetDlgCtrlID() == IDC_AUSZDAT)
	{
            pDC->SetTextColor (CaptionColor);
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
*/
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CButton )))
	{
            pDC->SetBkColor (Color);
			pDC->SetBkMode (TRANSPARENT);
			return hBrushStatic;
	}
	return CPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CPazPflegPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_A)
	{
		if (!Read ())
		{
			m_A.SetFocus ();
			return FALSE;
		}

	}

	if (Control == &m_EtiNr)
	{
		if (!ReadEti ())
		{
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CPazPflegPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

BOOL CPazPflegPage::ReadMdn ()
{
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Mdn.mdn.mdn == 0)
	{
		_tcscpy (MdnAdr.adr.adr_krz, (TCHAR *) "Zentrale");
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return TRUE;
	}
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}


BOOL CPazPflegPage::Read ()
{
	memcpy (&A_kun_gx.a_kun_gx, &a_kun_gx_null, sizeof (A_KUN_GX));
	Keys.Get ();
	if (A_bas.dbreadfirst () == 100)
	{
		MessageBox (_T("Artikel nicht angelegt"), NULL, MB_OK | MB_ICONERROR);
		m_A.SetFocus ();
        return FALSE;
	}
	A_kun_gx.a_kun_gx.mdn = Mdn.mdn.mdn;
	A_kun_gx.a_kun_gx.kun = Kun.kun.kun;
	A_kun_gx.a_kun_gx.a = A_bas.a_bas.a;
	A_kun_gx.a_kun_gx.tara = DefaultTara ();
	if (A_kun_gx.a_kun_gx.kun > 0)
	{
		_tcscpy (A_kun_gx.a_kun_gx.kun_bran2, _T("0"));
	}
	CString KunBran2;
	LPSTR pos = (LPSTR) A_kun_gx.a_kun_gx.kun_bran2;
	CDbUniCode::DbToUniCode (A_kun_gx.a_kun_gx.kun_bran2, pos);
	KunBran2 = A_kun_gx.a_kun_gx.kun_bran2;
	if (KunBran2.Trim () == "")
	{
		KunBran2 = _T("0");
		_tcscpy (A_kun_gx.a_kun_gx.kun_bran2, _T("0"));
	}

	LPSTR p = (LPSTR) A_kun_gx.a_kun_gx.kun_bran2;
	CString pt = A_kun_gx.a_kun_gx.kun_bran2;
	CDbUniCode::DbFromUniCode (pt.GetBuffer (), p, sizeof (A_kun_gx.a_kun_gx.kun_bran2) / 2);
    int dsqlstatus;
	if ((dsqlstatus = A_kun_gx.dbreadfirst ()) == 100)
	{
		memcpy (&A_kun_gx.a_kun_gx, &a_kun_gx_null, sizeof (A_KUN_GX));
		A_kun_gx.a_kun_gx.mdn = Mdn.mdn.mdn;
		A_kun_gx.a_kun_gx.kun = Kun.kun.kun;
		_tcscpy (A_kun_gx.a_kun_gx.kun_bran2, KunBran2.GetBuffer ());
		A_kun_gx.a_kun_gx.a = A_bas.a_bas.a;
		if (A_kun_gx.a_kun_gx.kun > 0)
		{
			_tcscpy (A_kun_gx.a_kun_gx.kun_bran2, _T("0"));
		}
		CDbUniCode::DbFromUniCode (pt.GetBuffer (), p, sizeof (A_kun_gx.a_kun_gx.kun_bran2) / 2);
		A_kun_gx.a_kun_gx.tara = DefaultTara ();
		_tcscpy (A_kun_gx.a_kun_gx.a_bz1, A_bas.a_bas.a_bz1);
		_tcscpy (A_kun_gx.a_kun_gx.a_bz2, A_bas.a_bas.a_bz2);
		_tcscpy (A_kun_gx.a_kun_gx.a_bz3, A_bas.a_bas.a_bz1);
		_tcscpy (A_kun_gx.a_kun_gx.a_bz4, A_bas.a_bas.a_bz2);
		MessageBox (_T("Neuer Satz"), MB_OK);
		Eti_typ.eti_typ.eti = FirstEti;
		DefaultMhd ();
	}
	text_nr = A_kun_gx.a_kun_gx.text_nr / 100;
	CString Eti;
	Eti.Format (_T("%ld"), A_kun_gx.a_kun_gx.text_nr);
	Eti = Eti.Right (2);
	memcpy (&Eti_typ.eti_typ, &eti_typ_null, sizeof (ETI_TYP));
	Eti_typ.eti_typ.eti = _tstoi (Eti.GetBuffer ());
	if (Eti_typ.eti_typ.eti == 0l)
	{
		Eti_typ.eti_typ.eti = FirstEti;
	}

	memcpy (&pr_ausz_gx, &pr_ausz_gx_null, sizeof (pr_ausz_gx));
	Pr_ausz_gx.pr_ausz_gx.text_nr = text_nr;
	Pr_ausz_gx.pr_ausz_gx.eti_typ = (short) Eti_typ.eti_typ.eti;
	if (Pr_ausz_gx.dbreadfirst () == 0)
	{
		memcpy (&pr_ausz_gx, 
			    &Pr_ausz_gx.pr_ausz_gx, 
				sizeof (pr_ausz_gx));
		Frame->SendMessage (WM_COMMAND, RECCHANGE, 0l);
	}

	if (Eti_typ.dbreadfirst () == 0)
	{
		FillSgCombo ();
		SetSgComboPos ();
	}
	if (A_kun_gx.a_kun_gx.text_nr2 == 0l)
	{
		CreateTextNr ();
	}

	short sa = 0;
	double vk_pr_i = 0.0;
	double ld_pr = 0.0;
	CString Date;
	CStrFuncs::SysDate (Date);
    char date[20];
	CDbUniCode::DbFromUniCode (Date.GetBuffer (), date, ((Date.GetLength () + 1) * 2));

//	if (dsqlstatus == 0)
	{
		if (preise_holen != NULL)
		{
			long kun = 0;
			short kun_fil = 1;

			if (A_kun_gx.a_kun_gx.kun != 0l)
			{
				kun = A_kun_gx.a_kun_gx.kun;
                kun_fil = 0;
			}
			else if (KunBran2 != "0")
			{
// Achtung !!! Diese Variante ist nut sinnvoll, wenn der Anwender sich an die
// Absprache h�lt, da� alle Kunden einer Branche denselben Preis haben.
				BranKun = 0l;
				kun_fil = 0;
				A_kun_gx.sqlopen (BranCursor);
				if (A_kun_gx.sqlfetch (BranCursor) == 0)
				{
					kun = BranKun;
				}
			}
			if (kun_fil == 1 || kun != 0l || BranKun != 0l)
			{
				(*preise_holen) (A_kun_gx.a_kun_gx.mdn,
					     A_kun_gx.a_kun_gx.fil,
						 kun_fil,
						 kun,
                         A_kun_gx.a_kun_gx.a,
                         date,
						 &sa,
						 &vk_pr_i,
						 &ld_pr);

			}
		}
	}

    EnableFields (TRUE);
	EtiForm.Show ();
	Form.Show ();
	if (PazPflegPageEx != NULL) 
	{
		PazPflegPageEx->EtiAnz = (double) A_kun_gx.a_kun_gx.geb_anz / 1000;
		PazPflegPageEx->PaletteAnz = (double) A_kun_gx.a_kun_gx.pal_anz / 1000;
		PazPflegPageEx->LdPr = ld_pr;
		PazPflegPageEx->Form.Show ();
	}
	if (A_kun_gx.a_kun_gx.zut_gew > 0.0)
	{
		m_ZutProz.SetWindowText (_T("0.0"));
		m_ZutProz.SetReadOnly (TRUE);
		m_ZutProz.ModifyStyle (WS_TABSTOP, 0);
	}
	else
	{
		m_ZutProz.SetReadOnly (FALSE);
		m_ZutProz.ModifyStyle (0, WS_TABSTOP);
	}
	return TRUE;
}

void CPazPflegPage::write ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein Satz zum Schreiben selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	Form.Get ();
	if (PazPflegPageEx != NULL)
	{
		PazPflegPageEx->Form.Get ();
		A_kun_gx.a_kun_gx.geb_anz = (long) (double) PazPflegPageEx->EtiAnz * 1000;
		A_kun_gx.a_kun_gx.pal_anz = (long) (double) PazPflegPageEx->PaletteAnz * 1000;
	}
	A_kun_gx.a_kun_gx.mdn =  Mdn.mdn.mdn; 
	A_kun_gx.a_kun_gx.kun  = Kun.kun.kun; 
	A_kun_gx.a_kun_gx.a   =  A_bas.a_bas.a; 
	EtiForm.Get ();

	A_kun_gx.a_kun_gx.text_nr = text_nr * 100 + Eti_typ.eti_typ.eti;
	CString KunBran2 = A_kun_gx.a_kun_gx.kun_bran2;
	KunBran2.TrimRight ();
	if (KunBran2 == "")
	{
			_tcscpy (A_kun_gx.a_kun_gx.kun_bran2, _T("0"));
	}
	A_kun_gx.dbupdate ();
	if (Choice != NULL)
	{
		Choice->RefreshList ();
	}
    EnableFields (FALSE);
	m_Mdn.SetFocus ();

	if (Write160 && dbild160 != NULL)
	{
		    CStringA Command;
			Command.Format ("bild160 B %.0lf -1 0 0 0",
			                 A_kun_gx.a_kun_gx.a); 

			(*dbild160) (Command.GetBuffer ());
	}

/*
	if (Write160)
	{
		    CStringA Command;
			Command.Format ("bild160 B %.0lf -1 0 0 0",
			                 A_kun_gx.a_kun_gx.a); 

			bild160 (Command.GetBuffer ());
	}
*/

	PazProperty->SetActivePage (0);	
}

void CPazPflegPage::OnAchoice ()
{
	Form.Get ();
	if (ChoiceA != NULL && !ModalChoiceA)
	{
		ChoiceA->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceA == NULL)
	{
		ChoiceA = new CChoiceA (this);
	    ChoiceA->IsModal = ModalChoiceA;
		ChoiceA->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceA->SetDbClass (&A_kun_gx);
	if (ModalChoiceA)
	{
			ChoiceA->DoModal();
//			delete ChoiceA;
//			ChoiceA = NULL;
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceA->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceA->MoveWindow (&rect);
		ChoiceA->SetFocus ();
		return;
	}
    if (ChoiceA->GetState ())
    {
		  CABasList *abl = ChoiceA->GetSelectedText (); 
		  if (abl == NULL) return;
		  a_kun_gx.a = abl->a;
          memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_A.SetSel (0, -1, TRUE);
		  m_A.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CPazPflegPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_kun_gx);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CPazPflegPage::OnKunchoice ()
{
	if (ChoiceKun != NULL && !ModalChoiceKun)
	{
		ChoiceKun->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceKun == NULL)
	{
		ChoiceKun = new CChoiceKun (this);
	    ChoiceKun->IsModal = ModalChoiceKun;
		ChoiceKun->CreateDlg ();
	}

    ChoiceKun->SetDbClass (&A_kun_gx);
	CString MdnNr;
	m_Mdn.GetWindowText (MdnNr);
	ChoiceKun->m_Mdn = _tstoi (MdnNr.GetBuffer ());
	if (ModalChoiceKun)
	{
			ChoiceKun->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceKun->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceKun->MoveWindow (&rect);
		ChoiceKun->SetFocus ();
		return;
	}
    if (ChoiceKun->GetState ())
    {
		  CKunList *abl = ChoiceKun->GetSelectedText (); 
		  if (abl == NULL) return;
		  a_kun_gx.mdn = abl->mdn;
		  a_kun_gx.kun = abl->kun;
		  memcpy (&Kun.kun, &kun_null, sizeof (KUN));
		  memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
		  Kun.kun.mdn = abl->mdn;
		  Kun.kun.kun = abl->kun;
		  if (Kun.dbreadfirst () == 0)
		  {
			  KunAdr.adr.adr = Kun.kun.adr1;
			  KunAdr.dbreadfirst ();
		  }
		  m_Kun.SetSel (0, -1, TRUE);
		  m_Kun.SetFocus ();
		  memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  Form.Show ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CPazPflegPage::FillKunBran2 ()
{
	CFormField *f = Form.GetFormField (&m_KunBran2);
	if (f != NULL)
	{
		f->ComboValues.clear ();
//		f->ComboValues.push_back (new CString ("0")); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"kun_bran2\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CPazPflegPage::FillEtikett () //WAL-174
{
	CFormField *f = Form.GetFormField (&m_etikett);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"etikett\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}


void CPazPflegPage::FillMeEinhKun ()
{
	CFormField *f = Form.GetFormField (&m_MeEinhKun);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"me_einh_kun\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CPazPflegPage::OnEtiChoice ()
{
    CString Text;
	if (ChoiceEti != NULL && !ModalChoiceEti)
	{
		ChoiceEti->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceEti == NULL)
	{
		ChoiceEti = new CChoiceEti (this);
	    ChoiceEti->IsModal = ModalChoiceEti;
		ChoiceEti->CreateDlg ();
	}

    ChoiceEti->SetDbClass (&A_kun_gx);
	if (ModalChoiceEti)
	{
			ChoiceEti->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceEti->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceEti->MoveWindow (&rect);
		ChoiceEti->SetFocus ();
		return;
	}
    if (ChoiceEti->GetState ())
    {
		  CEtiList *abl = ChoiceEti->GetSelectedText (); 
		  if (abl == NULL) return;
		  Eti_typ.eti_typ.eti = abl->eti;
		  CString EtiNr;
		  EtiNr.Format (_T("%ld"), abl->eti);
		  m_EtiNr.SetWindowText (EtiNr); 
		  _tcscpy (Eti_typ.eti_typ.eti_bz, abl->eti_bz.GetBuffer ());
		  m_EtiBz.SetWindowText (Eti_typ.eti_typ.eti_bz);
		  m_EtiNr.SetSel (0, -1, TRUE);
		  m_EtiNr.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

BOOL CPazPflegPage::ReadEti ()
{
	long eti = Eti_typ.eti_typ.eti;
	EtiForm.Get ();
	if (eti == Eti_typ.eti_typ.eti) return TRUE;
	if (Eti_typ.dbreadfirst () == 100)
	{
		  MessageBox (_T("Etikett nicht angelegt"), NULL,
						MB_OK | MB_ICONERROR);
		  m_EtiBz.SetWindowText (Eti_typ.eti_typ.eti_bz);
		  m_EtiNr.SetFocus ();
		  return FALSE;
	}
	EtiForm.Show ();
	FillSgCombo ();
	return TRUE;
}


void CPazPflegPage::OnEnKillfocusEtinr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ReadEti ();
}

void CPazPflegPage::FillSgCombo ()
{
	CFormField *f_Sg1 = Form.GetFormField (&m_Sg1);
	CFormField *f_Sg2 = Form.GetFormField (&m_Sg2);
	CFormField *f_Sg3 = Form.GetFormField (&m_Sg3);
	if (f_Sg1 != NULL && f_Sg2 != NULL)
	{
		f_Sg1->DestroyComboBox ();
		f_Sg2->DestroyComboBox ();
		f_Sg3->DestroyComboBox ();
		for (int i = 1; i < 20; i ++)
		{
			  if (*SgTab[i] == 0) continue;
              CString *Sg1 = new CString ();
			  Sg1->Format (_T("%hd/%hd"), *SgTab[i], *ZeTab[i]);
		      f_Sg1->ComboValues.push_back (Sg1); 
              CString *Sg2 = new CString ();
			  Sg2->Format (_T("%hd/%hd"), *SgTab[i], *ZeTab[i]);
		      f_Sg2->ComboValues.push_back (Sg2); 
              CString *Sg3 = new CString ();
			  Sg3->Format (_T("%hd/%hd"), *SgTab[i], *ZeTab[i]);
		      f_Sg3->ComboValues.push_back (Sg3); 
		}
		f_Sg1->FillComboBox ();
		m_Sg1.SetCurSel (0);
		f_Sg2->FillComboBox ();
		m_Sg2.SetCurSel (0);
		f_Sg3->FillComboBox ();
		m_Sg3.SetCurSel (0);
	}
}

void CPazPflegPage::SetSgComboPos ()
{
	for (int i = 1; i < 20; i ++)
	{
		if (A_kun_gx.a_kun_gx.sg1 == *SgTab[i])
		{
			CString Sg;
		    Sg.Format (_T("%hd/%hd"), *SgTab[i], *ZeTab[i]);
	        CFormField *f_Sg1 = Form.GetFormField (&m_Sg1);
			if (f_Sg1 != NULL)
			{
				f_Sg1->SetTextSelected (Sg);
			}
		}
	}
			
	for (int i = 1; i < 20; i ++)
	{
		if (A_kun_gx.a_kun_gx.sg2 == *SgTab[i])
		{
			CString Sg;
		    Sg.Format (_T("%hd/%hd"), *SgTab[i], *ZeTab[i]);
	        CFormField *f_Sg2 = Form.GetFormField (&m_Sg2);
			if (f_Sg2 != NULL)
			{
				f_Sg2->SetTextSelected (Sg);
			}
		}
	}
}

void CPazPflegPage::GetFirstEti ()
{
	FirstEti = DefaultEti;
	Eti_typ.eti_typ.eti = DefaultEti;
	if (Eti_typ.dbreadfirst () == 0)
	{
		FirstEti = DefaultEti;
		return;
	}

	FirstEti = 1;

	Eti_typ.sqlout ((long *) &FirstEti, SQLLONG, 0);
	int cursor = Eti_typ.sqlcursor (_T("select eti from eti_typ"));
	Eti_typ.sqlfetch (cursor);
	Eti_typ.sqlclose (cursor);
}

void CPazPflegPage::OnChoice ()
{
	OnAKunGxChoice ();
}

void CPazPflegPage::OnAKunGxChoice ()
{
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CAKunGxChoice (this);
	    Choice->IsModal = ModalChoice;
		Choice->CreateDlg ();
	}

	Form.Get ();
    Choice->SetDbClass (&A_kun_gx);
	CString MdnNr;
	m_Mdn.GetWindowText (MdnNr);
//	Choice->m_Mdn = _tstoi (MdnNr.GetBuffer ());
//	Choice->m_KunBran2 = A_kun_gx.a_kun_gx.kun_bran2;
	CString KunNr;
	m_Kun.GetWindowText (KunNr);
//	Choice->m_Kun = _tstol (KunNr.GetBuffer ());

	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
		Choice->MoveWindow (&rect);
		Choice->SetFocus ();
		return;
	}
    if (Choice->GetState ())
    {
		  CAKunGxList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  a_kun_gx.mdn = abl->mdn;
		  _tcscpy (A_kun_gx.a_kun_gx.kun_bran2, 
		  abl->kun_bran2.GetBuffer (sizeof (A_kun_gx.a_kun_gx.kun_bran2) - 1));
		  LPSTR p = (LPSTR) A_kun_gx.a_kun_gx.kun_bran2;
		  CString pt = A_kun_gx.a_kun_gx.kun_bran2;
		  CDbUniCode::DbFromUniCode (pt.GetBuffer (), p, sizeof (A_kun_gx.a_kun_gx.kun_bran2) / 2);
		  a_kun_gx.kun = abl->kun;
		  a_kun_gx.a   = abl->a;
		  memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  else
		  {
			  _tcscpy (MdnAdr.adr.adr_krz, _T(""));
		  }
		  memcpy (&Kun.kun, &kun_null, sizeof (KUN));
		  memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
		  Kun.kun.mdn = abl->mdn;
		  Kun.kun.kun = abl->kun;
		  if (Kun.dbreadfirst () == 0)
		  {
			  KunAdr.adr.adr = Kun.kun.adr1;
			  KunAdr.dbreadfirst ();
		  }
		  else
		  {
			  _tcscpy (KunAdr.adr.adr_krz, _T(""));
		  }
		  memcpy (&A_bas.a_bas, &a_bas, sizeof (A_BAS));
		  A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
			m_A.SetFocus ();
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
		  Form.Show ();
    }
}

void CPazPflegPage::OnAKunGxSelected ()
{
	if (Choice == NULL) return;
    CAKunGxList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    a_kun_gx.mdn = abl->mdn;
    _tcscpy (A_kun_gx.a_kun_gx.kun_bran2, 
    abl->kun_bran2.GetBuffer (sizeof (A_kun_gx.a_kun_gx.kun_bran2) - 1));
	LPSTR p = (LPSTR) A_kun_gx.a_kun_gx.kun_bran2;
	CString pt = A_kun_gx.a_kun_gx.kun_bran2;
	CDbUniCode::DbFromUniCode (pt.GetBuffer (), p, sizeof (A_kun_gx.a_kun_gx.kun_bran2) / 2);
    a_kun_gx.kun = abl->kun;
    a_kun_gx.a   = abl->a;
    memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
    memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
    Mdn.mdn.mdn = abl->mdn;
    if (Mdn.dbreadfirst () == 0)
    {
		  MdnAdr.adr.adr = Mdn.mdn.adr;
		  MdnAdr.dbreadfirst ();
    }
    else
    {
		  _tcscpy (MdnAdr.adr.adr_krz, _T(""));
    }
    memcpy (&Kun.kun, &kun_null, sizeof (KUN));
    memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
    Kun.kun.mdn = abl->mdn;
    Kun.kun.kun = abl->kun;
    if (Kun.dbreadfirst () == 0)
    {
		  KunAdr.adr.adr = Kun.kun.adr1;
		  KunAdr.dbreadfirst ();
    }
    else
    {
		  _tcscpy (KunAdr.adr.adr_krz, _T(""));
    }
    memcpy (&A_bas.a_bas, &a_bas, sizeof (A_BAS));
    A_bas.a_bas.a = abl->a;
    if (A_bas.dbreadfirst () == 0)
    {
		m_A.EnableWindow (TRUE);
		m_A.SetFocus ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
    Form.Show ();
    PazPflegPageEx->Form.Show ();
}

void CPazPflegPage::OnAKunGxCanceled ()
{
//	if (Choice == NULL) return;
//	delete Choice;
//	Choice = NULL;
	Choice->ShowWindow (SW_HIDE);
}

void CPazPflegPage::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CPazPflegPage::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}

double CPazPflegPage::DefaultTara ()
{
	switch (A_bas.a_bas.a_typ)
	{
		case HNDW :
			memcpy (&A_hndw.a_hndw, &a_hndw_null, sizeof (A_HNDW));
			A_hndw.a_hndw.a = A_bas.a_bas.a;
			A_hndw.dbreadfirst ();
			return A_hndw.a_hndw.tara;
		case EIG :
			memcpy (&A_eig.a_eig, &a_eig_null, sizeof (A_EIG));
			A_eig.a_eig.a = A_bas.a_bas.a;
			A_eig.dbreadfirst ();
			return A_eig.a_eig.tara;
		case EIG_DIV :
			memcpy (&A_eig_div.a_eig_div, &a_eig_div_null, sizeof (A_EIG_DIV));
			A_eig_div.a_eig_div.a = A_bas.a_bas.a;
			A_eig_div.dbreadfirst ();
			return A_eig_div.a_eig_div.tara;
	}
	return 0.0;
}
   

void CPazPflegPage::DefaultMhd ()
{

        A_kun_gx.a_kun_gx.hbk_ztr = 0;
        if (A_bas.a_bas.hbk_kz[0] == 'T' ||
            A_bas.a_bas.hbk_kz[0] <= ' ')
        {
            A_kun_gx.a_kun_gx.hbk_ztr = A_bas.a_bas.hbk_ztr;
        }
}

void CPazPflegPage::OnTextchoice ()
{
    CString Text;
	if (Use_atexte)
	{
		CATextList *abl = (CATextList *) OnFreitextchoiceAtexte ();
		if (abl != NULL)
		{
		  text_nr = abl->txt_nr;
			Form.Show ();
		}
		return;
	}
 	if (ChoiceTxt != NULL && !ModalChoiceTxt)
	{
		ChoiceTxt->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceTxt == NULL)
	{
		ChoiceTxt = new CChoiceTxt (this);
	    ChoiceTxt->IsModal = ModalChoiceTxt;
		ChoiceTxt->CreateDlg ();
	}

	ChoiceTxt->IsModal = ModalChoiceTxt;
    ChoiceTxt->SetDbClass (&Pr_ausz_gx);
	if (ModalChoiceTxt)
	{
			ChoiceTxt->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceTxt->MoveWindow (&rect);
		ChoiceTxt->SetFocus ();
		return;
	}
    if (ChoiceTxt->GetState ())
    {
		  CPrAuszList *abl = ChoiceTxt->GetSelectedText (); 
		  if (abl == NULL) return;
		  text_nr = abl->txt_nr;
		  Eti_typ.eti_typ.eti = abl->eti_typ;
		  CString TextNr;
		  Form.Show ();
		  EtiForm.Show ();
    }
}

void CPazPflegPage::OnKopfTextchoice ()
{
    CString Text;
	if (Use_atexte)
	{
		CATextList *abl = (CATextList *) OnFreitextchoiceAtexte ();
		if (abl != NULL)
		{
		  A_kun_gx.a_kun_gx.kopf_text = abl->txt_nr;
			Form.Show ();
		}
		return;
	}
	if (ChoiceTxt != NULL && !ModalChoiceTxt)
	{
		ChoiceTxt->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceTxt == NULL)
	{
		ChoiceTxt = new CChoiceTxt (this);
	    ChoiceTxt->IsModal = ModalChoiceTxt;
		ChoiceTxt->CreateDlg ();
	}

	ChoiceTxt->IsModal = ModalChoiceTxt;
    ChoiceTxt->SetDbClass (&Pr_ausz_gx);
	if (ModalChoiceTxt)
	{
			ChoiceTxt->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceTxt->MoveWindow (&rect);
		ChoiceTxt->SetFocus ();
		return;
	}
    if (ChoiceTxt->GetState ())
    {
		  CPrAuszList *abl = ChoiceTxt->GetSelectedText (); 
		  if (abl == NULL) return;
		  A_kun_gx.a_kun_gx.kopf_text = abl->txt_nr;
		  Form.Show ();
    }
}

void CPazPflegPage::OnMhdTextchoice ()
{
    CString Text;
	if (Use_atexte)
	{
		CATextList *abl = (CATextList *) OnFreitextchoiceAtexte ();
		if (abl != NULL)
		{
		  A_kun_gx.a_kun_gx.mhd_text = abl->txt_nr;
			Form.Show ();
		}
		return;
	}
	if (ChoiceTxt != NULL && !ModalChoiceTxt)
	{
		ChoiceTxt->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceTxt == NULL)
	{
		ChoiceTxt = new CChoiceTxt (this);
	    ChoiceTxt->IsModal = ModalChoiceTxt;
		ChoiceTxt->CreateDlg ();
	}

	ChoiceTxt->IsModal = ModalChoiceTxt;
    ChoiceTxt->SetDbClass (&Pr_ausz_gx);
	if (ModalChoiceTxt)
	{
			ChoiceTxt->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceTxt->MoveWindow (&rect);
		ChoiceTxt->SetFocus ();
		return;
	}
    if (ChoiceTxt->GetState ())
    {
		  CPrAuszList *abl = ChoiceTxt->GetSelectedText (); 
		  if (abl == NULL) return;
		  A_kun_gx.a_kun_gx.mhd_text = abl->txt_nr;
		  Form.Show ();
    }
}

void *CPazPflegPage::OnFreitextchoice ()
{
    CString Text;
	if (ChoiceTxt == NULL)
	{
		ChoiceTxt = new CChoiceTxt (this);
	    ChoiceTxt->IsModal = TRUE;
		ChoiceTxt->CreateDlg ();
	}

	ChoiceTxt->IsModal = TRUE;
    ChoiceTxt->SetDbClass (&Pr_ausz_gx);
	ChoiceTxt->DoModal();
    if (ChoiceTxt->GetState ())
    {
		  CPrAuszList *abl = ChoiceTxt->GetSelectedText (); 
		  return abl;
    }
	return NULL;
}
void *CPazPflegPage::OnFreitextchoiceAtexte ()
{
    CString Text;
	if (ChoiceATxt == NULL)
	{
		ChoiceATxt = new CChoiceAtxt (this);
	    ChoiceATxt->IsModal = TRUE;
		ChoiceATxt->CreateDlg ();
	}

	ChoiceATxt->IsModal = TRUE;
    ChoiceATxt->SetDbClass (&Pr_ausz_gx);
	ChoiceATxt->DoModal();
    if (ChoiceATxt->GetState ())
    {
		  CATextList *abl = ChoiceATxt->GetSelectedText (); 
		  return abl;
    }
	return NULL;
}

void CPazPflegPage::OnFreitext1choice ()
{
	if (Use_atexte == FALSE)
	{
		CPrAuszList *abl = (CPrAuszList *) OnFreitextchoice ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext1 = abl->txt_nr;
			Form.Show ();
		}
	}
	else
	{	
		CATextList *abl = (CATextList *) OnFreitextchoiceAtexte ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext1 = abl->txt_nr;
			Form.Show ();
		}
	}
}

void CPazPflegPage::OnFreitext2choice ()
{
	if (Use_atexte == FALSE)
	{
		CPrAuszList *abl = (CPrAuszList *) OnFreitextchoice ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext2 = abl->txt_nr;
			Form.Show ();
		}
	}
	else
	{	
		CATextList *abl = (CATextList *) OnFreitextchoiceAtexte ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext2 = abl->txt_nr;
			Form.Show ();
		}
	}
}

void CPazPflegPage::OnFreitext3choice ()
{
	if (Use_atexte == FALSE)
	{
		CPrAuszList *abl = (CPrAuszList *) OnFreitextchoice ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext3 = abl->txt_nr;
			Form.Show ();
		}
	}
	else
	{	
		CATextList *abl = (CATextList *) OnFreitextchoiceAtexte ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext3 = abl->txt_nr;
			Form.Show ();
		}
	}
}

void CPazPflegPage::OnFreitext4choice ()
{
	if (Use_atexte == FALSE)
	{
		CPrAuszList *abl = (CPrAuszList *) OnFreitextchoice ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext4 = abl->txt_nr;
			Form.Show ();
		}
	}
	else
	{	
		CATextList *abl = (CATextList *) OnFreitextchoiceAtexte ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext4 = abl->txt_nr;
			Form.Show ();
		}
	}
}

void CPazPflegPage::OnFreitext5choice ()
{
	if (Use_atexte == FALSE)
	{
		CPrAuszList *abl = (CPrAuszList *) OnFreitextchoice ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext5 = abl->txt_nr;
			Form.Show ();
		}
	}
	else
	{	
		CATextList *abl = (CATextList *) OnFreitextchoiceAtexte ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext5 = abl->txt_nr;
			Form.Show ();
		}
	}
}

void CPazPflegPage::OnFreitext6choice ()
{
	if (Use_atexte == FALSE)
	{
		CPrAuszList *abl = (CPrAuszList *) OnFreitextchoice ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext6 = abl->txt_nr;
			Form.Show ();
		}
	}
	else
	{	
		CATextList *abl = (CATextList *) OnFreitextchoiceAtexte ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext6 = abl->txt_nr;
			Form.Show ();
		}
	}
}

void CPazPflegPage::OnFreitext7choice ()
{
	if (Use_atexte == FALSE)
	{
		CPrAuszList *abl = (CPrAuszList *) OnFreitextchoice ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext7 = abl->txt_nr;
			Form.Show ();
		}
	}
	else
	{	
		CATextList *abl = (CATextList *) OnFreitextchoiceAtexte ();
		if (abl != NULL)
		{
			  A_kun_gx.a_kun_gx.freitext7 = abl->txt_nr;
			Form.Show ();
		}
	}
}

void CPazPflegPage::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


	if (m_Mdn.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein SDatz zum L�schen selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	if (MessageBox (_T("Preisauszeichnerdaten l�schen"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		A_kun_gx.beginwork ();
		A_kun_gx.dbdelete ();
		if (A_kun_gx.a_kun_gx.text_nr2 != 0l)
		{
			GenTextNr->FreeNr (A_kun_gx.a_kun_gx.text_nr2);
		}
		A_kun_gx.commitwork ();
		memcpy (&A_kun_gx.a_kun_gx, &a_kun_gx_null, sizeof (A_KUN_GX));
		memcpy (&Kun.kun, &kun_null, sizeof (KUN));
		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		EnableFields (FALSE);
		Form.Show ();
		m_Mdn.SetFocus ();
		if (Choice != NULL)
		{
			Choice->RefreshList ();
		}
		PazProperty->SetActivePage (0);
	}
}

void CPazPflegPage::CreateTextNr ()
{
	A_kun_gx.beginwork ();
	long text_nr = GenTextNr->NextNr ();
	if (text_nr == 0l)
	{
		A_kun_gx.commitwork ();
		return;
	}
	while (TextNrExist (text_nr))
	{
		text_nr = GenTextNr->NextNr ();
		if (text_nr == 0l)
		{
			A_kun_gx.commitwork ();
			return;
		}
	}
	A_kun_gx.a_kun_gx.text_nr2 = text_nr;
	A_kun_gx.commitwork ();
}

BOOL CPazPflegPage::TextNrExist (long text_nr2)
{
	if (TextNrCursor == -1)
	{
		A_kun_gx.sqlin ((short *) &A_kun_gx.a_kun_gx.mdn, SQLSHORT, 0);
		A_kun_gx.sqlin ((short *) &A_kun_gx.a_kun_gx.fil, SQLSHORT, 0);
		A_kun_gx.sqlin ((long *)  &A_kun_gx.a_kun_gx.text_nr2, SQLLONG, 0);
		TextNrCursor = A_kun_gx.sqlcursor (_T("select text_nr2 from a_kun_gx ")
			                _T("where mdn = ? ")
							_T("and fil = ? ")
							_T("and text_nr2 = ?"));
	}
	A_kun_gx.a_kun_gx.text_nr2 = text_nr2;
	A_kun_gx.sqlopen (TextNrCursor);
	if (A_kun_gx.sqlfetch (TextNrCursor) == 0)
	{
		return TRUE;
	}
	return FALSE;
}

void CPazPflegPage::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
//		Control->SetReadOnly (!enable);
        Control->EnableWindow (enable);
	}
	if (TaraReadOnly && enable)
	{
		m_Tara.SetReadOnly (TRUE);
	}
}

BOOL CPazPflegPage::StepBack ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
					Frame->DestroyWindow ();
					return FALSE;
	}
	else
	{
		EnableFields (FALSE);
		m_Mdn.SetFocus ();
		PazProperty->SetActivePage (0);	
	}
	return TRUE;
}

void CPazPflegPage::OnEnKillfocusPrZutTxt()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	Pr_ausz_gx.pr_ausz_gx.text_nr = text_nr;
	memcpy (&pr_ausz_gx,&Pr_ausz_gx.pr_ausz_gx, sizeof (pr_ausz_gx));
}

void CPazPflegPage::OnEnChangePrZutTxt()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDbPropertyPage::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
	CFormField *f = Form.GetFormField (&m_PrZutTxt);	
	if (f != NULL)
	{
		f->Get ();
		Pr_ausz_gx.pr_ausz_gx.text_nr = text_nr;
		memcpy (&pr_ausz_gx,&Pr_ausz_gx.pr_ausz_gx, sizeof (pr_ausz_gx));
	}
}

void CPazPflegPage::OnCopy ()
{
    OnEditCopy ();
}  



void CPazPflegPage::OnPaste ()
{
	OnEditPaste ();
}

void CPazPflegPage::OnCopyEx ()
{
	if (!m_A.IsWindowEnabled ())
	{
		Copy ();
		return;
	}
}

void CPazPflegPage::OnPasteEx ()
{
	if (!m_A.IsWindowEnabled () && Paste ())
	{
		A_kun_gx.CopyData (&a_kun_gx);
		Form.Show ();
		if (PazPflegPageEx != NULL && IsWindow (PazPflegPageEx->m_hWnd))
		{
			PazPflegPageEx->Form.Show ();
		}
		return;
	}
}

BOOL CPazPflegPage::Copy ()
{
	Form.Get ();
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return FALSE;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      int err = GetLastError ();
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return FALSE;  
    }

	try
	{
        UINT CF_A_KUN_GX = RegisterClipboardFormat (_T("a_kun_gx")); 
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, sizeof (A_KUN_GX)); 
        char *p = (char *) GlobalLock(hglbCopy);
        memcpy (p, &A_kun_gx.a_kun_gx, sizeof (A_kun_gx.a_kun_gx));
        GlobalUnlock(hglbCopy); 
		HANDLE cData;
		cData = ::SetClipboardData( CF_A_KUN_GX, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
    return TRUE;
}

BOOL CPazPflegPage::Paste ()
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return FALSE;
    }

    HGLOBAL hglbCopy;
    UINT CF_A_KUN_GX;
    _TCHAR name [256];
    CF_A_KUN_GX = EnumClipboardFormats (0);
	if (CF_A_KUN_GX == 0)
	{
		int err = GetLastError ();
		CloseClipboard();
		return FALSE;
	}

    int ret;
    while ((ret = GetClipboardFormatName (CF_A_KUN_GX, name, sizeof (name))) != 0)
    { 
         CString Name = name;
         if (Name == _T("a_kun_gx"))
         {
              break;
         }  
		 CF_A_KUN_GX = EnumClipboardFormats (CF_A_KUN_GX);
    }
    if (ret == 0)
    {
         CloseClipboard();
         return FALSE;
    }    

    try
    {	
  	     hglbCopy =  ::GetClipboardData(CF_A_KUN_GX);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
 	     LPSTR p = (LPSTR) GlobalLock ((HGLOBAL) hglbCopy);
         memcpy ( (LPSTR) &a_kun_gx, p, sizeof (a_kun_gx));
  	     GlobalUnlock ((HGLOBAL) hglbCopy);
    }
    catch (...) {};
    CloseClipboard();
    return TRUE; 
}

void CPazPflegPage::EnableFields (BOOL b)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (!b)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);
	if (ModalChoice)
	{
		m_AKunGxChoice.EnableWindow (!b);
	}
}

void CPazPflegPage::OnTextCent()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CString Text;
	m_PazBz1.GetWindowText (Text); 
	Text.Trim ();
	int idx = m_Sg1.GetCurSel ();
	CString cSg; 
	m_Sg1.GetLBText (idx,cSg);
    CToken t;
	t.SetSep (_T("/"));
	t = cSg;
	if (t.GetAnzToken () < 2) return;
	int ze = _tstoi (t.GetToken (1));
	int len = Text.GetLength ();
	int pos = max (0, (ze - len)/2);
    CString NewText (' ', pos);
	NewText += Text;
	m_PazBz1.SetWindowText (NewText); 

	m_PazBz2.GetWindowText (Text); 
	Text.Trim ();
	idx = m_Sg2.GetCurSel ();
	cSg; 
	m_Sg2.GetLBText (idx,cSg);
	t = cSg;
	if (t.GetAnzToken () < 2) return;
	ze = _tstoi (t.GetToken (1));
	len = Text.GetLength ();
	pos = max (0, (ze - len)/2);
    CString NewText2 (' ', pos);
	NewText2 += Text;
	m_PazBz2.SetWindowText (NewText2); 
}

void CPazPflegPage::OnTextLeft()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CString Text;
	m_PazBz1.GetWindowText (Text); 
	Text.Trim ();
	m_PazBz1.SetWindowText (Text); 

	m_PazBz2.GetWindowText (Text); 
	Text.Trim ();
	m_PazBz2.SetWindowText (Text); 
}

void CPazPflegPage::OnTextRight()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CString Text;
	m_PazBz1.GetWindowText (Text); 
	Text.Trim ();
	int idx = m_Sg1.GetCurSel ();
	CString cSg; 
	m_Sg1.GetLBText (idx,cSg);
    CToken t;
	t.SetSep (_T("/"));
	t = cSg;
	if (t.GetAnzToken () < 2) return;
	int ze = _tstoi (t.GetToken (1));
	int len = Text.GetLength ();
	int pos = max (0, ze - len);
    CString NewText (' ', pos);
	NewText += Text;
	m_PazBz1.SetWindowText (NewText); 

	m_PazBz2.GetWindowText (Text); 
	Text.Trim ();
	idx = m_Sg2.GetCurSel ();
	cSg; 
	m_Sg2.GetLBText (idx,cSg);
	t = cSg;
	if (t.GetAnzToken () < 2) return;
	ze = _tstoi (t.GetToken (1));
	len = Text.GetLength ();
	pos = max (0, ze - len);
    CString NewText2 (' ', pos);
	NewText2 += Text;
	m_PazBz2.SetWindowText (NewText2); 
}

void CPazPflegPage::OnKillFocus (CWnd *newFocus)
{
	CWnd *Control = GetFocus ();
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		if (f->Scale > 0)
		{
			f->Show ();
		}
	}
}

void CPazPflegPage::OnEnKillfocusZutgew()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CString Text;
	m_ZutGew.GetWindowText (Text);
	double zutgew = CStrFuncs::StrToDouble (Text);
	if (zutgew > 0.0)
	{
		m_ZutProz.SetWindowText (_T("0.0"));
		m_ZutProz.SetReadOnly (TRUE);
		m_ZutProz.ModifyStyle (WS_TABSTOP, 0);
		m_HbkZtr.SetFocus ();
	}
	else
	{
		m_ZutProz.SetReadOnly (FALSE);
		m_ZutProz.ModifyStyle (0, WS_TABSTOP);
		m_ZutProz.SetFocus ();
	}
}

void CPazPflegPage::OnEnKillfocusZutproz()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CPazPflegPage::SetFreiTextMode ()
{
	if (!WithFreiText) 
	{
	    m_LFreiText1.ShowWindow (SW_HIDE);
	    m_LFreiText2.ShowWindow (SW_HIDE);
	    m_FreiText1.ShowWindow (SW_HIDE);
	    m_FreiText2.ShowWindow (SW_HIDE);
	    m_FreiText3.ShowWindow (SW_HIDE);
	    m_FreiText4.ShowWindow (SW_HIDE);
	    m_FreiText5.ShowWindow (SW_HIDE);
	    m_FreiText6.ShowWindow (SW_HIDE);
	    m_FreiText7.ShowWindow (SW_HIDE);
		m_FreiText1Choice.ShowWindow (SW_HIDE);
		m_FreiText2Choice.ShowWindow (SW_HIDE);
		m_FreiText3Choice.ShowWindow (SW_HIDE);
		m_FreiText4Choice.ShowWindow (SW_HIDE);
		m_FreiText5Choice.ShowWindow (SW_HIDE);
		m_FreiText6Choice.ShowWindow (SW_HIDE);
		m_FreiText7Choice.ShowWindow (SW_HIDE);
		return;
	}

	m_LGenTxt.ShowWindow (SW_HIDE);
	m_GenTxt.ShowWindow (SW_HIDE);
}