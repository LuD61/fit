#include "StdAfx.h"
#include ".\strtoken.h"

CStrToken::CStrToken(void)
{
}

CStrToken::~CStrToken(void)
{
}

void CStrToken::GetTokens (LPTSTR Txt)
{
     AnzToken = 0;
     Buffer.TrimRight ();
     LPTSTR b = Buffer.GetBuffer (512);
     LPTSTR p = TStrTok (b, sep.GetBuffer (512));
     while (p != NULL)
     {
             AnzToken ++;
             p = TStrTok (NULL, sep.GetBuffer (512));
     }
     Tokens = new CString *[AnzToken];
     if (Tokens == NULL)
     {
             return;
     }

     Buffer = Txt;
     Buffer.TrimRight ();
     b = Buffer.GetBuffer (0);
     p = TStrTok (b, sep.GetBuffer (0));
     int i = 0;
     while (p != NULL)
     {
             Tokens[i] = new CString (p);
             p = TStrTok (NULL, sep.GetBuffer (512));
             i ++;
     }
     AktToken = 0;
}

char *CStrToken::StrTok (char *str, char *sep)
{
	static char *apos = NULL;
	char *p;
	int i, j;

	if (str != NULL) apos = str;
	if (str == NULL && apos == NULL) return NULL;

	p = apos;
	for (i = 0; apos[i] != 0; i ++)
	{
		if (apos[i] == sep[0])
		{
			int ipos = i + 1;
			for (j = 1; (sep[j] != 0) && (apos[ipos] != 0); j ++, ipos ++)
			{
				if (sep[j] != apos[ipos]) break;
			}
			if (sep[j] == 0)
			{
				apos[i] = 0;
				apos = &apos[ipos];
                return (p);
			}
		}
	}
	apos = NULL;
	return p;
}

wchar_t *CStrToken::WStrTok (wchar_t *str, wchar_t *sep)
{
	static wchar_t *apos = NULL;
	wchar_t *p;
	int i, j;

	if (str != NULL) apos = str;
	if (str == NULL && apos == NULL) return NULL;

	p = apos;
	for (i = 0; apos[i] != 0; i ++)
	{
		if (apos[i] == sep[0])
		{
			int ipos = i + 1;
			for (j = 1; (sep[j] != 0) && (apos[ipos] != 0); j ++, ipos ++)
			{
				if (sep[j] != apos[ipos]) break;
			}
			if (sep[j] == 0)
			{
				apos[i] = 0;
				apos = &apos[ipos];
                return (p);
			}
		}
	}
	apos = NULL;
	return p;
}

const CStrToken& CStrToken::operator=(LPTSTR Txt)
{
     if (Tokens != NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt);
     return *this;
}

const CStrToken& CStrToken::operator=(CString& Txt)
{
     if (Tokens != NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt.GetBuffer (512));
     return *this;
}
