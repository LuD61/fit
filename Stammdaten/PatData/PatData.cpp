// PatData.cpp : Definiert das Klassenverhalten f�r die Anwendung.
//

#include "stdafx.h"
#include "PatData.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "PatDataDoc.h"
#include "PatDataView.h"
#include "PazTexte.h"
#include "EtiPfleg.h"
#include "PazView.h"
#include "mo_progcfg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPatDataApp

BEGIN_MESSAGE_MAP(CPatDataApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// Dateibasierte Standarddokumentbefehle
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standarddruckbefehl "Seite einrichten"
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
	ON_COMMAND(ID_FILE_NEW_TEXTE, OnFileNewTexte)
	ON_COMMAND(ID_FILE_NEW_ETI, OnFileNewEti)
	ON_COMMAND(ID_PAZDATA, OnPazdata)
END_MESSAGE_MAP()


// CPatDataApp-Erstellung

CDocTemplate *TextInstance = NULL; 
CDocTemplate *EtiInstance = NULL; 
CDocTemplate *DataInstance = NULL; 

PROG_CFG Cfg (_T("Pazdata"));


CPatDataApp::CPatDataApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance positionieren
}


// Das einzige CPatDataApp-Objekt

CPatDataApp theApp;

// CPatDataApp Initialisierung

BOOL CPatDataApp::InitInstance()
{
	// InitCommonControls() ist f�r Windows XP erforderlich, wenn ein Anwendungsmanifest
	// die Verwendung von ComCtl32.dll Version 6 oder h�her zum Aktivieren
	// von visuellen Stilen angibt. Ansonsten treten beim Erstellen von Fenstern Fehler auf.
	InitCommonControls();
    LoadLibrary (_T("RICHED32.DLL"));

	CWinApp::InitInstance();

	// OLE-Bibliotheken initialisieren
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standardinitialisierung
	// Wenn Sie diese Features nicht verwenden und die Gr��e
	// der ausf�hrbaren Datei verringern m�chten, entfernen Sie
	// die nicht erforderlichen Initialisierungsroutinen.
	// �ndern Sie den Registrierungsschl�ssel unter dem Ihre Einstellungen gespeichert sind.
	// TODO: �ndern Sie diese Zeichenfolge entsprechend,
	// z.B. zum Namen Ihrer Firma oder Organisation.
	SetRegistryKey(_T("Vom lokalen Anwendungs-Assistenten generierte Anwendungen"));
	LoadStdProfileSettings(4);  // Standard INI-Dateioptionen laden (einschlie�lich MRU)
	// Dokumentvorlagen der Anwendung registrieren. Dokumentvorlagen
	//  dienen als Verbindung zwischen Dokumenten, Rahmenfenstern und Ansichten.
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_PatDataTYPE,
		RUNTIME_CLASS(CPatDataDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CPazTexte));
	AddDocTemplate(pDocTemplate);
	CMultiDocTemplate* pDocTemplate2;
	pDocTemplate2 = new CMultiDocTemplate(IDR_Etiketten,
		RUNTIME_CLASS(CPatDataDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CEtiPfleg));
	AddDocTemplate(pDocTemplate2);
	CMultiDocTemplate* pDocTemplate3;
	pDocTemplate3 = new CMultiDocTemplate(IDR_PazView,
		RUNTIME_CLASS(CPatDataDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CPazView));
	AddDocTemplate(pDocTemplate3);
	// Haupt-MDI-Rahmenfenster erstellen
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;
	// Rufen Sie DragAcceptFiles nur auf, wenn eine Suffix vorhanden ist.
	//  In einer MDI-Anwendung ist dies unmittelbar nach dem Festlegen von m_pMainWnd erforderlich
	// Befehlszeile parsen, um zu pr�fen auf Standardumgebungsbefehle DDE, Datei offen
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	// Verteilung der in der Befehlszeile angegebenen Befehle. Es wird FALSE zur�ckgegeben, wenn
	// die Anwendung mit /RegServer, /Register, /Unregserver oder /Unregister gestartet wurde.

//	if (!ProcessShellCommand(cmdInfo))
//		return FALSE;

	// Das Hauptfenster ist initialisiert und kann jetzt angezeigt und aktualisiert werden.
//	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->ShowWindow(SW_SHOWMAXIMIZED);
	pMainFrame->UpdateWindow();


	int Use_atexte = 0;
	if (Cfg.GetCfgValue (_T("Use_atexte"), PROG_CFG::cfg_v) == TRUE)
    {
			Use_atexte = _tstoi (PROG_CFG::cfg_v);
    }
	if (Use_atexte == 1) OnPazdata ();  //WAL-174

	return TRUE;
}



// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// Anwendungsbefehl zum Ausf�hren des Dialogfelds
void CPatDataApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// CPatDataApp Meldungshandler

void CPatDataApp::OnFileNewTexte()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	POSITION pos;
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		if (dt == TextInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->TxtWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->TxtWnd);
				return;
			}
		}

		TextInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
			dt->SetDefaultTitle (dc);
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}

void CPatDataApp::OnFileNewEti()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	POSITION pos;
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		if (dt == EtiInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->EtiWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->EtiWnd);
				return;
			}
		}

		EtiInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
			dt->SetDefaultTitle (dc);
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}


void CPatDataApp::OnPazdata()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	POSITION pos;
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		dt = GetNextDocTemplate (pos);
	}
	if (dt != NULL)
	{
		if (dt == DataInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->DataWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->DataWnd);
				return;
			}
		}

		DataInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
			dt->SetDefaultTitle (dc);
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}
