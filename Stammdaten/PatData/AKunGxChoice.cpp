#include "stdafx.h"
#include "AKunGxChoice.h"
#include "DbUniCode.h"
#include "StrFuncs.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CAKunGxChoice::Sort1 = -1;
int CAKunGxChoice::Sort2 = -1;
int CAKunGxChoice::Sort3 = -1;
int CAKunGxChoice::Sort4 = -1;
int CAKunGxChoice::Sort5 = -1;
int CAKunGxChoice::Sort6 = -1;

CAKunGxChoice::CAKunGxChoice(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_Mdn = 0;
	m_Kun = 0l;
	m_KunBran2 = "";
    SetDlgSize (376, 226);
}

CAKunGxChoice::~CAKunGxChoice() 
{
	DestroyList ();
}

void CAKunGxChoice::DestroyList() 
{
	for (std::vector<CAKunGxList *>::iterator pabl = AKunGxList.begin (); pabl != AKunGxList.end (); ++pabl)
	{
		CAKunGxList *abl = *pabl;
		delete abl;
	}
    AKunGxList.clear ();
}

void CAKunGxChoice::FillList () 
{
    short  mdn;
	long kun;
	TCHAR kun_bran2[4];
	double a;
	TCHAR a_bz1 [50];
	int cursor;

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Preisauszeichnerdaten"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Mandant"),        1, 80, LVCFMT_RIGHT);
    SetCol (_T("Branche"),        2, 80, LVCFMT_LEFT);
    SetCol (_T("Kunde"),          3, 80, LVCFMT_RIGHT);
    SetCol (_T("Artikel"),        4, 80, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),    5,200, LVCFMT_LEFT);

	if (AKunGxList.size () == 0)
	{
			CString Sql = _T("select a_kun_gx.mdn, a_kun_gx.kun_bran2,") 
						  _T("a_kun_gx.kun, a_kun_gx.a, a_bas.a_bz1 ")
				          _T("from a_kun_gx, a_bas where a_kun_gx.a > 0 ") 
						  _T("and a_bas.a = a_kun_gx.a ");
			if (m_Mdn > 0)
			{
				Sql += _T (" and a_kun_gx.mdn = ") + ShortToString (m_Mdn);
			}
			if (m_KunBran2 != "" && m_KunBran2 != "0")
			{
				Sql += _T(" and kun_bran2 = \"") + m_KunBran2 + _T("\"");
			}
			if (m_Kun > 0l)
			{
				Sql += _T (" and a_kun_gx.kun = ") + LongToString (m_Kun);
			}
			DbClass->sqlout ((short *)&mdn,      SQLSHORT, 0);
			DbClass->sqlout ((char *)&kun_bran2, SQLCHAR, sizeof (kun_bran2));
			DbClass->sqlout ((long *)&kun,      SQLLONG, 0);
			DbClass->sqlout ((double *)&a,      SQLDOUBLE, 0);
			DbClass->sqlout ((char *)a_bz1,    SQLCHAR, sizeof (a_bz1));
			cursor = DbClass->sqlcursor (Sql.GetBuffer ());
			while (DbClass->sqlfetch (cursor) == 0)
			{
				LPSTR pos = (LPSTR) kun_bran2;
				CDbUniCode::DbToUniCode (kun_bran2, pos);
				pos = (LPSTR) a_bz1;
				CDbUniCode::DbToUniCode (a_bz1, pos);
				CAKunGxList *abl = new CAKunGxList (mdn, kun_bran2, kun, a, a_bz1);
				AKunGxList.push_back (abl);
			}
			DbClass->sqlclose (cursor);
	}

	for (std::vector<CAKunGxList *>::iterator pabl = AKunGxList.begin (); pabl != AKunGxList.end (); ++pabl)
	{
		CAKunGxList *abl = *pabl;
		CString Mdn = ShortToString (abl->mdn);
		CString KunBran2 = abl->kun_bran2;
		CString Kun = LongToString (abl->kun);
		CString Art = DoubleToString (abl->a, _T("%.0lf"));
		CString ABz1 = abl->a_bz1;

		CString Num;
		CString LText;
		LText.Format (_T("%s %ld %.0lf"), abl->kun_bran2.GetBuffer (),
			                              abl->kun, 
									      abl->a);
		if (Style == LVS_REPORT)
		{
                Num = Mdn;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (KunBran2.GetBuffer (), i, 2);
        ret = SetItemText (Kun.GetBuffer (), i, 3);
        ret = SetItemText (Art.GetBuffer (), i, 4);
        ret = SetItemText (ABz1.GetBuffer (), i, 5);
        i ++;
    }

	SortRow = 4;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort (listView);
}

void CAKunGxChoice::RefreshList () 
{
    short  mdn;
	long kun;
	TCHAR kun_bran2[4];
	double a;
	TCHAR a_bz1 [50];
	int cursor;

	CListCtrl *listView = GetListView ();
	if (listView == NULL) return;
	if (!IsWindow (listView->m_hWnd)) return;
	DestroyList ();
	listView->DeleteAllItems ();

    DWORD Style = SetStyle (LVS_REPORT);

    int i = 0;

	if (AKunGxList.size () == 0)
	{
			CString Sql = _T("select a_kun_gx.mdn, a_kun_gx.kun_bran2,") 
						  _T("a_kun_gx.kun, a_kun_gx.a, a_bas.a_bz1 ")
				          _T("from a_kun_gx, a_bas where a_kun_gx.a > 0 ") 
						  _T("and a_bas.a = a_kun_gx.a ");
			if (m_Mdn > 0)
			{
				Sql += _T (" and a_kun_gx.mdn = ") + ShortToString (m_Mdn);
			}
			if (m_KunBran2 != "" && m_KunBran2 != "0")
			{
				Sql += _T(" and kun_bran2 = \"") + m_KunBran2 + _T("\"");
			}
			if (m_Kun > 0l)
			{
				Sql += _T (" and a_kun_gx.kun = ") + LongToString (m_Kun);
			}
			DbClass->sqlout ((short *)&mdn,      SQLSHORT, 0);
			DbClass->sqlout ((char *)&kun_bran2, SQLCHAR, sizeof (kun_bran2));
			DbClass->sqlout ((long *)&kun,      SQLLONG, 0);
			DbClass->sqlout ((double *)&a,      SQLDOUBLE, 0);
			DbClass->sqlout ((char *)&a_bz1,    SQLCHAR, sizeof (a_bz1));
			cursor = DbClass->sqlcursor (Sql.GetBuffer ());
			while (DbClass->sqlfetch (cursor) == 0)
			{
				LPSTR pos = (LPSTR) kun_bran2;
				CStrFuncs::Trim (pos);
				CDbUniCode::DbToUniCode (kun_bran2, pos);
				pos = (LPSTR) a_bz1;
				CStrFuncs::Trim (pos);
				CDbUniCode::DbToUniCode (a_bz1, pos);
				CAKunGxList *abl = new CAKunGxList (mdn, kun_bran2, kun, a, a_bz1);
				AKunGxList.push_back (abl);
			}
			DbClass->sqlclose (cursor);
	}

	for (std::vector<CAKunGxList *>::iterator pabl = AKunGxList.begin (); pabl != AKunGxList.end (); ++pabl)
	{
		CAKunGxList *abl = *pabl;
		CString Mdn = ShortToString (abl->mdn);
		CString KunBran2 = abl->kun_bran2;
		CString Kun = LongToString (abl->kun);
		CString Art = DoubleToString (abl->a, _T("%.0lf"));
		CString ABz1 = abl->a_bz1;

		CString Num;
		CString LText;
		LText.Format (_T("%s %ld %.0lf"), abl->kun_bran2.GetBuffer (),
			                              abl->kun, 
									      abl->a);
		if (Style == LVS_REPORT)
		{
                Num = Mdn;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (KunBran2.GetBuffer (), i, 2);
        ret = SetItemText (Kun.GetBuffer (), i, 3);
        ret = SetItemText (Art.GetBuffer (), i, 4);
        ret = SetItemText (ABz1.GetBuffer (), i, 5);
        i ++;
    }

    Sort (listView);
    Sort (listView);
}


void CAKunGxChoice::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CAKunGxChoice::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CAKunGxChoice::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CAKunGxChoice::SearchKunBran2 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CAKunGxChoice::SearchKun (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CAKunGxChoice::SearchA (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 4);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CAKunGxChoice::SearchABz1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 5);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CAKunGxChoice::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchKunBran2 (ListBox, EditText.GetBuffer (8));
             break;
        case 3 :
             SearchKun (ListBox, EditText.GetBuffer (8));
             break;
        case 4 :
             SearchA (ListBox, EditText.GetBuffer ());
             break;
        case 5 :
             SearchABz1 (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CAKunGxChoice::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CAKunGxChoice::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort4;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort4);
	   }
	   return 0;
   }
   else if (SortRow == 4)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort5;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort5);
	   }
	   return 0;
   }
   else if (SortRow == 5)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort6;
   }
   return 0;
}


void CAKunGxChoice::Sort (CListCtrl *ListBox)
{
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
        case 3:
              Sort4 *= -1;
              break;
        case 4:
              Sort5 *= -1;
              break;
        case 5:
              Sort6 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CAKunGxList *abl = AKunGxList [i];
		   
		   abl->mdn        = _tstoi (ListBox->GetItemText (i, 1));
		   abl->kun_bran2  = ListBox->GetItemText (i, 2);
		   abl->kun        = _tstol (ListBox->GetItemText (i, 3));
		   abl->a          = _tstof (ListBox->GetItemText (i, 4));
		   abl->a_bz1      = ListBox->GetItemText (i, 5);
	}
}

void CAKunGxChoice::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = AKunGxList [idx];
}

CAKunGxList *CAKunGxChoice::GetSelectedText ()
{
	CAKunGxList *abl = (CAKunGxList *) SelectedRow;
	return abl;
}

CString& CAKunGxChoice::ShortToString (short value)
{
	B.Format (_T("%hd"), value);
	return B;
}

CString& CAKunGxChoice::LongToString (long value)
{
	B.Format (_T("%ld"), value);
	return B;
}

CString& CAKunGxChoice::DoubleToString (double value, LPCTSTR format)
{
	B.Format (format, value);
	return B;
}
