// EtiPfleg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "PatData.h"
#include "EtiPfleg.h"
#include "EtiView.h"
#include "DbUniCode.h"
#include "UniFormField.h"

// CEtiPfleg

IMPLEMENT_DYNCREATE(CEtiPfleg, DbFormView)

CEtiPfleg::CEtiPfleg()
	: DbFormView(CEtiPfleg::IDD)
{
    WhiteBrush = (HBRUSH) GetStockObject (WHITE_BRUSH);
	ModalChoice = FALSE;
	Choice = NULL;
	memcpy (&Eti_typ.eti_typ, &eti_typ_null, sizeof (ETI_TYP));

	SgTab[0] = &Eti_typ.eti_typ.sg01;
	SgTab[1] = &Eti_typ.eti_typ.sg02;
	SgTab[2] = &Eti_typ.eti_typ.sg03;
	SgTab[3] = &Eti_typ.eti_typ.sg04;
	SgTab[4] = &Eti_typ.eti_typ.sg05;
	SgTab[5] = &Eti_typ.eti_typ.sg06;
	SgTab[6] = &Eti_typ.eti_typ.sg07;
	SgTab[7] = &Eti_typ.eti_typ.sg08;
	SgTab[8] = &Eti_typ.eti_typ.sg09;
	SgTab[9] = &Eti_typ.eti_typ.sg10;
	SgTab[10] = &Eti_typ.eti_typ.sg11;
	SgTab[11] = &Eti_typ.eti_typ.sg12;
	SgTab[12] = &Eti_typ.eti_typ.sg13;
	SgTab[13] = &Eti_typ.eti_typ.sg14;
	SgTab[14] = &Eti_typ.eti_typ.sg15;
	SgTab[15] = &Eti_typ.eti_typ.sg16;
	SgTab[16] = &Eti_typ.eti_typ.sg17;
	SgTab[17] = &Eti_typ.eti_typ.sg18;
	SgTab[18] = &Eti_typ.eti_typ.sg19;
	SgTab[19] = &Eti_typ.eti_typ.sg20;

	ZeTab[0] = &Eti_typ.eti_typ.ze01;
	ZeTab[1] = &Eti_typ.eti_typ.ze02;
	ZeTab[2] = &Eti_typ.eti_typ.ze03;
	ZeTab[3] = &Eti_typ.eti_typ.ze04;
	ZeTab[4] = &Eti_typ.eti_typ.ze05;
	ZeTab[5] = &Eti_typ.eti_typ.ze06;
	ZeTab[6] = &Eti_typ.eti_typ.ze07;
	ZeTab[7] = &Eti_typ.eti_typ.ze08;
	ZeTab[8] = &Eti_typ.eti_typ.ze09;
	ZeTab[9] = &Eti_typ.eti_typ.ze10;
	ZeTab[10] = &Eti_typ.eti_typ.ze11;
	ZeTab[11] = &Eti_typ.eti_typ.ze12;
	ZeTab[12] = &Eti_typ.eti_typ.ze13;
	ZeTab[13] = &Eti_typ.eti_typ.ze14;
	ZeTab[14] = &Eti_typ.eti_typ.ze15;
	ZeTab[15] = &Eti_typ.eti_typ.ze16;
	ZeTab[16] = &Eti_typ.eti_typ.ze17;
	ZeTab[17] = &Eti_typ.eti_typ.ze18;
	ZeTab[18] = &Eti_typ.eti_typ.ze19;
	ZeTab[19] = &Eti_typ.eti_typ.ze20;
	HeadControls.Add (&m_EtiNr);
}

CEtiPfleg::~CEtiPfleg()
{
	Eti_typ.dbclose ();
	Form.DestroyAll ();
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
}

void CEtiPfleg::DoDataExchange(CDataExchange* pDX)
{
	DbFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LETINR, m_LEtiNr);
	DDX_Control(pDX, IDC_LETI_BZ, m_LEtiBz);
	DDX_Control(pDX, IDC_LSTD_SIZE, m_LStdSize);
	DDX_Control(pDX, IDC_LZE_STD, m_LStdZe);
	DDX_Control(pDX, IDC_ETINR, m_EtiNr);
	DDX_Control(pDX, IDC_ETI_BZ, m_EtiBz);
	DDX_Control(pDX, IDC_STD_SIZE, m_StdSize);
	DDX_Control(pDX, IDC_STD_ZE, m_StdZe);
	DDX_Control(pDX, IDC_BORDER, m_Border);
	DDX_Control(pDX, IDC_LETI_BR, m_LEtiBr);
	DDX_Control(pDX, IDC_ETI_HOE, m_EtiHoe);
	DDX_Control(pDX, IDC_LETI_HOE, m_LEtiHoe);
	DDX_Control(pDX, IDC_ETI_BR, m_EtiBr);
	DDX_Control(pDX, IDC_ETI_BR_SPIN, m_EtiBrSpin);
	DDX_Control(pDX, IDC_ETI_HOE_SPIN, m_EtiHoeSpin);
}

BEGIN_MESSAGE_MAP(CEtiPfleg, DbFormView)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_WM_DESTROY ()
//	ON_WM_DRAWITEM ()
	ON_BN_CLICKED(IDC_ETICHOICE , OnEtiChoice)
	ON_COMMAND (SELECTED, OnEtiSelected)
	ON_COMMAND (CANCELED, OnEtiCanceled)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_STN_CLICKED(IDC_LZE_STD, OnStnClickedLzeStd)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_NOTIFY(UDN_DELTAPOS, IDC_ETI_BR_SPIN, OnDeltaposEtiBrSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_ETI_HOE_SPIN, OnDeltaposEtiHoeSpin)
	ON_EN_KILLFOCUS(IDC_ETI_BR, OnEnKillfocusEtiBr)
	ON_EN_KILLFOCUS(IDC_ETI_HOE, OnEnKillfocusEtiHoe)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_COMMAND(ID_BACK, OnBack)
	ON_EN_CHANGE(IDC_ETI_BR, OnEnChangeEtiBr)
	ON_EN_CHANGE(IDC_ETI_HOE, OnEnChangeEtiHoe)
END_MESSAGE_MAP()


// CEtiPfleg-Diagnose

#ifdef _DEBUG
void CEtiPfleg::AssertValid() const
{
	DbFormView::AssertValid();
}

void CEtiPfleg::Dump(CDumpContext& dc) const
{
	DbFormView::Dump(dc);
}
#endif //_DEBUG


// CEtiPfleg-Meldungshandler

void CEtiPfleg::OnInitialUpdate()
{
	DbFormView::OnInitialUpdate();

	Eti_typ.opendbase (_T("bws"));
	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();

	CRect rect (0, 0, 200, 100);
 
	m_EtiShow.eti_typ = &Eti_typ.eti_typ;
	m_EtiShow.Create (NULL,   WS_CHILD | 
		                      SS_OWNERDRAW |
		                      WS_VISIBLE | WS_BORDER,
		                      rect, this, IDC_ETI_SHOW);
  
	CRect cRect;
	m_StdSize.GetWindowRect (&cRect);
    cRect.right = cRect.right - cRect.left;
    cRect.bottom = cRect.bottom - cRect.top;
	cRect.left = 0;
	cRect.top = 0;
	CRect sRect;
	sRect = cRect;

    CDC *cDC = GetDC ();
	SetFont (&Font);
	CFont *oldFont = cDC->SelectObject (&Font);
	CString X = _T("X");
	CSize xSize = cDC->GetTextExtent (X);
	CString Text = _T("Schriftgr��e");
	CSize tSize = cDC->GetTextExtent (Text);
	sRect.right = tSize.cx + 40;
	m_LSg.Create (Text.GetBuffer (), WS_CHILD | WS_VISIBLE, sRect, this);
	Text = _T("Zeilen");
	tSize = cDC->GetTextExtent (Text);
	sRect.right = tSize.cx + 20;
	m_LZe.Create (Text.GetBuffer (), WS_CHILD | WS_VISIBLE, sRect, this);
	m_SgBorder.Create (_T(""), WS_CHILD | WS_VISIBLE | BS_GROUPBOX, cRect, this, 2500);
	m_SgBorder.ModifyStyleEx (0, WS_EX_CLIENTEDGE);

	cRect.right = 6 * xSize.cx;
	for (int i = 1; i < 20; i ++)
	{
		m_Sg[i].Create (WS_CHILD | WS_VISIBLE | ES_RIGHT | WS_TABSTOP, 
			            cRect, this,
			            IDC_SG + i);
		m_Sg[i].ModifyStyleEx (0, WS_EX_CLIENTEDGE);
		m_Ze[i].Create (WS_CHILD | WS_VISIBLE | ES_RIGHT | WS_TABSTOP, 
			            cRect, this,
			            IDC_ZE + i);
		m_Ze[i].ModifyStyleEx (0, WS_EX_CLIENTEDGE);
	}

    Form.Add (new CFormField (&m_EtiNr,EDIT,       (long *)  &Eti_typ.eti_typ.eti, VLONG));
    Form.Add (new CUniFormField (&m_EtiBz,EDIT,       (LPTSTR)  Eti_typ.eti_typ.eti_bz, VCHAR, sizeof (eti_typ.eti_bz), 0));
    Form.Add (new CFormField (&m_StdSize,EDIT,     (short *) &Eti_typ.eti_typ.sg_std, VSHORT));
    Form.Add (new CFormField (&m_StdZe,EDIT,      (short *) &Eti_typ.eti_typ.max_zei_pos, VSHORT));
    Form.Add (new CFormField (&m_EtiBr,EDIT,      (short *) &Eti_typ.eti_typ.eti_br, VSHORT));
    Form.Add (new CFormField (&m_EtiHoe,EDIT,      (short *) &Eti_typ.eti_typ.eti_hoe, VSHORT));
	for (int i = 1; i < 20; i ++)
	{
		Form.Add (new CFormField (&m_Sg[i],EDIT,      (short *) SgTab[i], VSHORT));
		Form.Add (new CFormField (&m_Ze[i],EDIT,      (short *) ZeTab[i], VSHORT));
	}

	m_EtiBrSpin.SetBuddy (&m_EtiBr);
	m_EtiBrSpin.SetRange (1, 200);

	m_EtiHoeSpin.SetBuddy (&m_EtiHoe);
	m_EtiHoeSpin.SetRange (1, 200);

	EtiGrid.Create (this, 1, 2);
    EtiGrid.SetBorder (0, 0);
    EtiGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_EtiNr = new CCtrlInfo (&m_EtiNr, 0, 0, 1, 1);
	EtiGrid.Add (c_EtiNr);
	CtrlGrid.CreateChoiceButton (m_EtiChoice, IDC_ETICHOICE, this);
	CCtrlInfo *c_EtiChoice = new CCtrlInfo (&m_EtiChoice, 1, 0, 1, 1);
	EtiGrid.Add (c_EtiChoice);

	EtiBrGrid.Create (this, 1, 2);
    EtiBrGrid.SetBorder (0, 0);
    EtiBrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_EtiBr = new CCtrlInfo (&m_EtiBr, 0, 0, 1, 1);
	EtiBrGrid.Add (c_EtiBr);
	CCtrlInfo *c_EtiBrSpin = new CCtrlInfo (&m_EtiBrSpin, 1, 0, 1, 1);
	EtiBrGrid.Add (c_EtiBrSpin);

	EtiHoeGrid.Create (this, 1, 2);
    EtiHoeGrid.SetBorder (0, 0);
    EtiHoeGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_EtiHoe = new CCtrlInfo (&m_EtiHoe, 0, 0, 1, 1);
	EtiHoeGrid.Add (c_EtiHoe);
	CCtrlInfo *c_EtiHoeSpin = new CCtrlInfo (&m_EtiHoeSpin, 1, 0, 1, 1);
	EtiHoeGrid.Add (c_EtiHoeSpin);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

    CCtrlInfo *c_Border = new CCtrlInfo (&m_Border, 0, 0, DOCKRIGHT, 5);
    c_Border->rightspace = 30; 
	CtrlGrid.Add (c_Border);

	CCtrlInfo *c_LEtiNr = new CCtrlInfo (&m_LEtiNr, 1, 1, 1, 1);
	CtrlGrid.Add (c_LEtiNr);

	CCtrlInfo *c_EtiGrid = new CCtrlInfo (&EtiGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_EtiGrid);

	CCtrlInfo *c_LEtiBz = new CCtrlInfo (&m_LEtiBz, 1, 2, 1, 1);
	CtrlGrid.Add (c_LEtiBz);
	CCtrlInfo *c_EtiBz = new CCtrlInfo (&m_EtiBz, 2, 2, 4, 1);
	CtrlGrid.Add (c_EtiBz);

	CCtrlInfo *c_LEtiBr = new CCtrlInfo (&m_LEtiBr, 1, 3, 1, 1);
	CtrlGrid.Add (c_LEtiBr);
	CCtrlInfo *c_EtiBrGrid = new CCtrlInfo (&EtiBrGrid, 2, 3, 1, 1);
	CtrlGrid.Add (c_EtiBrGrid);
 
	CCtrlInfo *c_hSpace = new CCtrlInfo (this, 20, 3, 3, 1, 1);
	CtrlGrid.Add (c_hSpace);
	 
	CCtrlInfo *c_LEtiHoe = new CCtrlInfo (&m_LEtiHoe, 4, 3, 1, 1);
	CtrlGrid.Add (c_LEtiHoe);
	CCtrlInfo *c_EtiHoeGrid = new CCtrlInfo (&EtiHoeGrid, 5, 3, 1, 1);
	CtrlGrid.Add (c_EtiHoeGrid);

	CCtrlInfo *c_LStdSize = new CCtrlInfo (&m_LStdSize, 1, 4, 1, 1);
	CtrlGrid.Add (c_LStdSize);
	CCtrlInfo *c_StdSize = new CCtrlInfo (&m_StdSize, 2, 4, 1, 1);
	CtrlGrid.Add (c_StdSize);

    CCtrlInfo *c_LStdZe = new CCtrlInfo (&m_LStdZe, 1, 5, 1, 1);
	CtrlGrid.Add (c_LStdZe);
	CCtrlInfo *c_StdZe = new CCtrlInfo (&m_StdZe, 2, 5, 1, 1);
	CtrlGrid.Add (c_StdZe);

	SgGrid.Create (this, 20, 4);
    SgGrid.SetBorder (0, 0);
    SgGrid.SetGridSpace (5, 3);
    SgGrid.SetCellHeight (15);
    SgGrid.SetFontCellHeight (this);

    CCtrlInfo *c_SgBorder = new CCtrlInfo (&m_SgBorder, 0, 7, 6, 21);
	CtrlGrid.Add (c_SgBorder);

    CCtrlInfo *c_Sg;
    CCtrlInfo *c_Ze;
	c_Sg = new CCtrlInfo (&m_LSg, 0, 0 , 1, 1);
    SgGrid.Add (c_Sg);
	c_Ze = new CCtrlInfo (&m_LZe, 1, 0 , 1, 1);
    SgGrid.Add (c_Ze);

    for (int i = 1; i < 20; i ++)
	{
		c_Sg = new CCtrlInfo (&m_Sg[i], 0, i , 1, 1);
	    SgGrid.Add (c_Sg);
		c_Ze = new CCtrlInfo (&m_Ze[i], 1, i , 1, 1);
	    SgGrid.Add (c_Ze);
	}


	CCtrlInfo *c_SgGrid = new CCtrlInfo (&SgGrid, 2, 8 , 4, 1);
    CtrlGrid.Add (c_SgGrid);

/*
    CCtrlInfo *c_Sg;
    CCtrlInfo *c_Ze;
	c_Sg = new CCtrlInfo (&m_LSg, 2, 7 , 1, 1);
    CtrlGrid.Add (c_Sg);
	c_Ze = new CCtrlInfo (&m_LZe, 3, 7 , 2, 1);
    CtrlGrid.Add (c_Ze);

    for (int i = 1; i < 20; i ++)
	{
		c_Sg = new CCtrlInfo (&m_Sg[i], 2, 7 + i , 1, 1);
	    CtrlGrid.Add (c_Sg);
		c_Ze = new CCtrlInfo (&m_Ze[i], 3, 7 + i , 2, 1);
	    CtrlGrid.Add (c_Ze);
	}
*/

	CCtrlInfo *c_sSpace = new CCtrlInfo (this, 100, 6, 10, 1, 1);
	CtrlGrid.Add (c_sSpace);
	CCtrlInfo *c_EtiShow = new CCtrlInfo (&m_EtiShow, 7, 10, DOCKRIGHT, DOCKBOTTOM);
    c_EtiShow->rightspace = 30; 
	CtrlGrid.Add (c_EtiShow);

/*
	SetFont (&Font);
    cDC = GetDC ();
	cDC->SelectObject (&Font);
*/
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();
	m_Sg[19].GetWindowRect (&cRect);
	CRect mRect;
	GetWindowRect (&mRect);
	CRect BorderRect;
	m_SgBorder.GetWindowRect (&BorderRect);
	BorderRect.bottom = cRect.bottom + 5;
	ScreenToClient (&BorderRect);
	CSize Size = GetTotalSize ();
/*
	if (mRect.bottom < cRect.bottom + 10)
	{
		Size.cy = mRect.bottom - mRect.top;
		SetScrollSizes (MM_TEXT, Size);
	}
*/
	ScreenToClient (&cRect);
	if (Size.cy < (cRect.bottom) + 10)
	{
		Size.cy = cRect.bottom + 10;
		SetScrollSizes (MM_TEXT, Size);
	}

	BorderRect.right = BorderRect.left + 400;
	m_SgBorder.MoveWindow (&BorderRect);
    EnableFields (FALSE);
    Form.Show ();
}

BOOL CEtiPfleg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
					break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F12)
			{
				write ();;
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{

				if (Choice != NULL)
				{
					Choice->ShowWindow (SW_HIDE);
				}

				return TRUE;
			}

			else if (pMsg->wParam == VK_F9)
			{
				OnEtiChoice ();
				return TRUE;
			}

/*
			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnEditCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnEditPaste ();
					   return TRUE;
				}
			}
*/

	}
    return CFormView::PreTranslateMessage(pMsg);
}

void CEtiPfleg::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CEtiPfleg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_EtiNr)
	{
		if (!Read ())
		{
			m_EtiNr.SetFocus ();
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
	return FALSE;
}

BOOL CEtiPfleg::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
    return FALSE;
}

BOOL CEtiPfleg::Read ()
{
	CString EtiNr;

	memcpy (&eti_typ, &eti_typ_null, sizeof (eti_typ));
	memcpy (&Eti_typ.eti_typ, &eti_typ_null, sizeof (eti_typ));
	m_EtiNr.GetWindowText (EtiNr);
	Eti_typ.eti_typ.eti = _tstoi (EtiNr.GetBuffer ());
	int dsqlstatus = Eti_typ.dbreadfirst ();
	if (dsqlstatus == 100)
	{
		memcpy (&Eti_typ.eti_typ, &eti_typ_null, sizeof (eti_typ));
		m_EtiNr.GetWindowText (EtiNr);
		Eti_typ.eti_typ.eti = _tstoi (EtiNr.GetBuffer ());
	}
    EnableFields (TRUE);
	memcpy (&eti_typ, &Eti_typ.eti_typ, sizeof (eti_typ));
	Form.Show ();
	m_EtiShow.Invalidate (TRUE);
	return TRUE;
}


void CEtiPfleg::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize (nType, cx, cy);
	if (m_LEtiNr.m_hWnd != NULL)
	{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
	}
}

void CEtiPfleg::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CEtiPfleg::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}

HBRUSH CEtiPfleg::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	HBRUSH hBrush = DbFormView::OnCtlColor (pDC, pWnd,nCtlColor);
	if (pWnd == &m_EtiShow)
	{
            pDC->SetBkColor (RGB (255,255,255));
			return WhiteBrush;
	}

	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
 //           pDC->SetBkColor (Color);
//			return hBrushStatic;
	}
	return hBrush;
}

void CEtiPfleg::OnDrawItem (int Control, LPDRAWITEMSTRUCT lpDRAWITEMSTRUCT)
{
	if (Control == IDC_ETI_SHOW)
	{
		int eins = 1;
	}
}

void CEtiPfleg::OnDestroy ()
{
//	m_EtiShowEx.DestroyWindow ();
}
void CEtiPfleg::OnStnClickedLzeStd()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CEtiPfleg::OnEtiChoice ()
{
    CString Text;
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceEti (this);
	    Choice->IsModal = ModalChoice;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&Eti_typ);
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
		Choice->MoveWindow (&rect);
		Choice->SetFocus ();
		return;
	}
    if (Choice->GetState ())
    {
/*
          Text = Choice->GetSelText ();
          int pos = 0;
		  CString TxtNr = Text.Tokenize (_T(" "), pos);
		  pr_ausz_gx.text_nr = _tstol (TxtNr.GetBuffer (0));
*/
		  CEtiList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  eti_typ.eti = abl->eti;
		  CString EtiNr;
		  EtiNr.Format (_T("%ld"), abl->eti);
		  m_EtiNr.SetWindowText (EtiNr); 
		  m_EtiNr.SetSel (0, -1, TRUE);
		  m_EtiNr.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
//	delete Choice;
//	Choice = NULL;
}

void CEtiPfleg::OnEtiSelected ()
{
	if (Choice == NULL) return;
    CEtiList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    eti_typ.eti = abl->eti;
    CString EtiNr;
    EtiNr.Format (_T("%ld"), abl->eti);
    EnableFields (FALSE);
    m_EtiNr.SetWindowText (EtiNr); 
    m_EtiNr.SetSel (0, -1, TRUE);
    m_EtiNr.SetFocus ();
    PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
}

void CEtiPfleg::OnEtiCanceled ()
{
//	if (Choice == NULL) return;
//	delete Choice;
//	Choice = NULL;
	Choice->ShowWindow (SW_HIDE);
}

void CEtiPfleg::write ()
{
	if (m_EtiNr.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein Satz zum Schreiben selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	Form.Get ();
	memcpy (&eti_typ, &Eti_typ.eti_typ, sizeof (eti_typ));
	LPSTR p = (LPSTR) Eti_typ.eti_typ.eti_bz;
	CString pt = Eti_typ.eti_typ.eti_bz;
	Eti_typ.dbupdate ();
    EnableFields (FALSE);
    m_EtiNr.SetSel (0, -1, TRUE);
    m_EtiNr.SetFocus ();
	if (Choice != NULL)
	{
		Choice->RefreshList ();
	}
}

void CEtiPfleg::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	write ();
}

void CEtiPfleg::OnDeltaposEtiBrSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;
	Form.Get ();
	eti_typ.eti_br += pNMUpDown->iDelta;
	m_EtiShow.Invalidate ();
}

void CEtiPfleg::OnDeltaposEtiHoeSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;
	Form.Get ();
	eti_typ.eti_hoe += pNMUpDown->iDelta;
	m_EtiShow.Invalidate ();
}

void CEtiPfleg::OnEnKillfocusEtiBr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CString Width;
	m_EtiBr.GetWindowText (Width);
	eti_typ.eti_br = _tstoi (Width.GetBuffer ());
	m_EtiShow.Invalidate ();
}

void CEtiPfleg::OnEnKillfocusEtiHoe()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CString Height;
	m_EtiHoe.GetWindowText (Height);
	eti_typ.eti_hoe = _tstoi (Height.GetBuffer ());
	m_EtiShow.Invalidate ();
}

void CEtiPfleg::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	if (m_EtiNr.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein Satz zum L�schen selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	if (MessageBox (_T("Etikett l�schen"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		Form.Get ();
		Eti_typ.dbdelete ();
		memcpy (&Eti_typ.eti_typ, &eti_typ_null, sizeof (ETI_TYP));
		EnableFields (FALSE);
		Form.Show ();
		m_EtiNr.SetSel (0, -1, TRUE);
		m_EtiNr.SetFocus ();
		if (Choice != NULL)
		{
//			delete Choice;
//			Choice = NULL;
			Choice->RefreshList ();
		}
	}
}

void CEtiPfleg::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
//		Control->SetReadOnly (!enable);
        Control->EnableWindow (enable);
	}
}

void CEtiPfleg::StepBack ()
{
	if (m_EtiNr.IsWindowEnabled ())
	{
				GetParent ()->DestroyWindow ();
	}
	else
	{
		EnableFields (FALSE);
		m_EtiNr.SetFocus ();
	}
}

void CEtiPfleg::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	StepBack ();
}

void CEtiPfleg::OnEnChangeEtiBr()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den DbFormView::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
	CString EtiBr;
	m_EtiBr.GetWindowText (EtiBr);
	Eti_typ.eti_typ.eti_br = _tstoi (EtiBr.GetBuffer ());
	m_EtiShow.Invalidate ();
}

void CEtiPfleg::OnEnChangeEtiHoe()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den DbFormView::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
	CString EtiHoe;
	m_EtiHoe.GetWindowText (EtiHoe);
	Eti_typ.eti_typ.eti_hoe = _tstoi (EtiHoe.GetBuffer ());
	m_EtiShow.Invalidate ();
}

void CEtiPfleg::EnableFields (BOOL b)
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);
}
