#include "StdAfx.h"
#include "etishow.h"

CEtiShow::CEtiShow(void)
{
	Background = RGB (255, 255, 255);
	EtiColor   = RGB (225, 225, 225);
	LogRows    = 24;
	LogColumns = 80;
	EanStartHeight = 15;
	eti_typ = NULL;
}

CEtiShow::~CEtiShow(void)
{
}

void CEtiShow::DrawItem (LPDRAWITEMSTRUCT lpDRAWITEMSTRUCT)
{
	int sSize = 2;
	CDC cDC;
	cDC.Attach (lpDRAWITEMSTRUCT->hDC);
	cDC.FillSolidRect (&lpDRAWITEMSTRUCT->rcItem, Background);
	if (eti_typ->eti_br > 0 &&
		eti_typ->eti_hoe > 0)
	{
		double hFactor0 = (double) cDC.GetDeviceCaps (HORZRES) /
			                      cDC.GetDeviceCaps (HORZSIZE);
		double vFactor0 = (double) cDC.GetDeviceCaps (VERTRES) /
			                      cDC.GetDeviceCaps (VERTSIZE);
		double hFactor = hFactor0;
		double vFactor = vFactor0;
		int cx = (int) ((double) eti_typ->eti_br * vFactor) * 2;
		int cy = (int) ((double) eti_typ->eti_hoe * hFactor) * 2;
		CRect rect;
		GetClientRect (&rect);
		int FontSize = 30;
		EanHeight = EanStartHeight;
		while ((rect.right < cx + 10) ||
			   (rect.bottom < cy + 10))
		{
			hFactor = 0.9;
			vFactor = 0.9;
			FontSize = (int) ((double) FontSize * 0.9);
			EanHeight = (int) ((double) FontSize * 0.9);
			cx = (int) ((double) cx * vFactor);
			cy = (int) ((double) cy * hFactor);
			if (cx <= 0) break;
			if (cy <= 0) break;
		}
		if (cx == 0 || cy == 0) return;
		int x = max (0, (rect.right - cx) / 2);
		int y = max (0, (rect.bottom - cy) / 2);
        cx += x;
		cy += y;

		rect.left = x;
		rect.top = y;
		rect.right = cx;
		rect.bottom = cy;

		PrintRoundedShadow (cDC, rect);

		CBrush Brush (EtiColor);
		CBrush *oldBrush = cDC.SelectObject (&Brush);

		CPen Pen;
		Pen.CreatePen (PS_SOLID, 1, RGB (0, 0, 0));
		CPen *oldPen = cDC.SelectObject (&Pen);

        cDC.RoundRect(rect, CPoint(17, 17));

//		PrintShadow (cDC, rect);
		cDC.SelectObject (oldPen);
		cDC.SelectObject (oldBrush);

		CString Text ("ABCDEFGHIJK");
		CFont *Font = new CFont;
//		Font->CreatePointFont (FontSize, _T("MS_SAN_SERIF"));
		if (FontSize < 5) FontSize = 5;;
        CreateFont (Font, FontSize, CString ("MS_SAN_SERIF"));
		CFont *oldFont = cDC.SelectObject (Font);
		CSize size = cDC.GetTextExtent (Text);
//		int regsize = (rect.bottom - rect.top) / 10;
//		int ty = rect.top + 2 * regsize;
		int ty = GetpY (rect, 6);
		while ((ty + size.cy) > rect.bottom)
		{
			FontSize -= 1;
			if (FontSize <= sSize) break;
		    cDC.SelectObject (oldFont);
			if (Font != NULL)
			{
				delete Font;
			}
			Font = new CFont;
//			Font->CreatePointFont (FontSize, _T("MS_SAN_SERIF"));
            CreateFont (Font, FontSize, CString ("MS_SAN_SERIF"));
			cDC.SelectObject (Font);
			CSize size = cDC.GetTextExtent (Text);
		}

		int tx = (rect.right - rect.left - size.cx) / 2;
		while (tx < 0)
		{
			FontSize -= 1;
			if (FontSize < sSize) break;
		    cDC.SelectObject (oldFont);
			if (Font != NULL)
			{
				delete Font;
			}
			Font = new CFont ();
			CreateFont (Font, FontSize, CString ("MS_SAN_SERIF"));
			cDC.SelectObject (Font);
			CSize size = cDC.GetTextExtent (Text);
		    tx = (rect.right - rect.left - size.cx) / 2;
		}
		tx += rect.left;
		cDC.SetBkColor (EtiColor);
		if (FontSize >= sSize)
		{
			cDC.TextOut (tx, ty, Text);
		}
		cDC.SelectObject (oldFont);
		if (Font != NULL)
		{
			delete Font;
		}

		Text = _T("abcdefghijk");
		Font = new CFont ();
		FontSize -= 5;
		if (FontSize < sSize) FontSize = sSize - 1;
//		ty = GetpY (rect, 8);
		ty += size.cy;
        CreateFont (Font, FontSize, CString ("MS_SAN_SERIF"), FW_NORMAL);
		oldFont = cDC.SelectObject (Font);
		size = cDC.GetTextExtent (Text);
		tx = max (0, (rect.right - rect.left - size.cx) / 2);
		tx += rect.left;
		if (FontSize >= sSize)
		{
			cDC.TextOut (tx, ty, Text);
		}

		Text = _T("1234567890 abcde");
		ty += size.cy;
		size = cDC.GetTextExtent (Text);
		tx = max (0, (rect.right - rect.left - size.cx) / 2);
		tx += rect.left;
		if (FontSize >= sSize)
		{
			cDC.TextOut (tx, ty, Text);
		}
      
		Text = _T ("12345,12 �");
		ty = GetpYBottom (rect, 6);
		FontSize += 10;
        while (TRUE)
		{
			cDC.SelectObject (oldFont);
			if (Font != NULL)
			{
			  delete Font;
			}
			Font = new CFont ();
			CreateFont (Font, FontSize, CString ("MS_SAN_SERIF"));
			oldFont = cDC.SelectObject (Font);
			size = cDC.GetTextExtent (Text);
			tx = rect.right - size.cx - 10;
			if (tx >= rect.left || FontSize == sSize - 1) break;
			cDC.SelectObject (oldFont);
			FontSize -= 1;
		}
		if (FontSize >= sSize) 
		{
			cDC.TextOut (tx, ty, Text);
		}
		cDC.SelectObject (oldFont);
		if (Font != NULL)
		{
			delete Font;
		}
		PrintEan (cDC, rect, _T("2812340005124"), ty, tx);
	}
}

void CEtiShow::PrintEan (CDC& cDC, CRect& rect, LPTSTR Ean, int Row, int Column)
{
	    int Width[] = {1,2,5,3,5,4,8,5,4,3};

		CPen lPen;
		lPen.CreatePen (PS_SOLID, 1, RGB (0, 0, 0));

		CPen *oldPen = cDC.SelectObject (&lPen);
		TCHAR *p = Ean;
		double fWidth = (double) EanHeight /EanStartHeight;
		int ySpace = (int) (double) fWidth * 10;
		ySpace = (ySpace < 2) ? 2 : ySpace;
		int space = (int) (double) fWidth * 3; 
		space = (space < 1) ? 1 : space;
		int ty = rect.bottom - EanHeight - ySpace;
		int tx = GetpX (rect, 10);
		for (; *p != 0; p += 1)
		{
			int pos = *p;
			pos &= 0x0F;
			if (pos > 9) continue;
			int width = (int) (double) ((double) Width[pos] * fWidth); 
			for (int i = 0; i < width; i ++)
			{
				cDC.MoveTo (tx + i, ty);
			    cDC.LineTo (tx + i, ty + EanHeight);
			}
			tx += width + space;
		}
		cDC.SelectObject (oldPen);
}



void CEtiShow::PrintShadow (CDC& cDC, CRect& rect)
{
	CRect hShadow;
	CRect vShadow;

	hShadow.left = rect.right;
	hShadow.top = rect.top + 1;
	hShadow.right = rect.right + 3;
	hShadow.bottom = rect.bottom + 3;

    cDC.FillSolidRect (&hShadow, RGB (0, 0,0));

	vShadow.left = rect.left + 1;
	vShadow.top = rect.bottom;
	vShadow.right = rect.right + 3;
	vShadow.bottom = rect.bottom + 3;

    cDC.FillSolidRect (&vShadow, RGB (0, 0,0));
}

void CEtiShow::PrintRoundedShadow (CDC& cDC, CRect rect)
{
		rect.left += 3;
		rect.top += 3;
		rect.right += 1;
		rect.bottom += 1;
		CPen Pen;

		Pen.CreatePen (PS_SOLID, 4, RGB (0, 0, 0));
		CPen *oldPen = cDC.SelectObject (&Pen);
        cDC.RoundRect(rect, CPoint(17, 17));
		cDC.SelectObject (oldPen);
}

void CEtiShow::CreateFont (CFont *Font, int FontSize, CString& FontName )
{
	BOOL ret = Font->CreateFont (FontSize, 
				            0, 
						    0, 
		 			        0, 
					        FW_BOLD, 
		                    FALSE, 
					        FALSE, 
					        0, 
					        ANSI_CHARSET,
					        OUT_DEFAULT_PRECIS,
					        CLIP_DEFAULT_PRECIS, 
					        DEFAULT_QUALITY,
					        DEFAULT_PITCH,
					        FontName);
}

void CEtiShow::CreateFont (CFont *Font, int FontSize, CString& FontName, int Attribute )
{
	BOOL ret = Font->CreateFont (FontSize, 
				            0, 
						    0, 
		 			        0, 
					        Attribute, 
		                    FALSE, 
					        FALSE, 
					        0, 
					        ANSI_CHARSET,
					        OUT_DEFAULT_PRECIS,
					        CLIP_DEFAULT_PRECIS, 
					        DEFAULT_QUALITY,
					        DEFAULT_PITCH,
					        FontName);
}

int CEtiShow::GetpY (CRect& rect, int row)
{
	int logsize = (rect.bottom - rect.top) / LogRows;
	return rect.top + row * logsize;
}

int CEtiShow::GetpYBottom (CRect& rect, int row)
{
	int logsize = (rect.bottom - rect.top) / LogRows;
	return rect.bottom - row * logsize;
}

int CEtiShow::GetpX (CRect& rect, int column)
{
	int logsize = (rect.bottom - rect.top) / LogColumns;
	return rect.left + column * logsize;
}

int CEtiShow::GetpXRight (CRect& rect, int column)
{
	int logsize = (rect.bottom - rect.top) / LogColumns;
	return rect.right - column * logsize;
}