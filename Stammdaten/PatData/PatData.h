// PatData.h : Hauptheaderdatei f�r die PatData-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // Hauptsymbole


// CPatDataApp:
// Siehe PatData.cpp f�r die Implementierung dieser Klasse
//

class CPatDataApp : public CWinApp
{
public:
	CPatDataApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnFileNewTexte();
	afx_msg void OnFileNewEti();
	afx_msg void OnPazdata();
};

extern CPatDataApp theApp;
