#include "stdafx.h"
#include "ChoiceA.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceA::Sort1 = -1;
int CChoiceA::Sort2 = -1;
int CChoiceA::Sort3 = -1;
int CChoiceA::Sort4 = -1;

CChoiceA::CChoiceA(CWnd* pParent) 
        : CChoiceX(pParent)
{
}

CChoiceA::~CChoiceA() 
{
	DestroyList ();
}

void CChoiceA::DestroyList() 
{
	for (std::vector<CABasList *>::iterator pabl = ABasList.begin (); pabl != ABasList.end (); ++pabl)
	{
		CABasList *abl = *pabl;
		delete abl;
	}
    ABasList.clear ();
}

void CChoiceA::FillList () 
{
    double  a;
    TCHAR a_bz1 [50];
    TCHAR a_bz2 [50];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Artikel"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Artikel"),        1, 150, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung 1"),  2, 250);
    SetCol (_T("Bezeichnung 2"),  3, 250);

	if (ABasList.size () == 0)
	{
		DbClass->sqlout ((double *)&a,        SQLDOUBLE, 0);
		DbClass->sqlout ((LPTSTR)  a_bz1,     SQLCHAR, sizeof (a_bz1));
		DbClass->sqlout ((LPTSTR)  a_bz2,     SQLCHAR, sizeof (a_bz2));
		int cursor = DbClass->sqlcursor (_T("select a, a_bz1, a_bz2 from a_bas where a > 0"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) a_bz1;
			CDbUniCode::DbToUniCode (a_bz1, pos);
			pos = (LPSTR) a_bz2;
			CDbUniCode::DbToUniCode (a_bz2, pos);
			CABasList *abl = new CABasList (a, a_bz1, a_bz2);
			ABasList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CABasList *>::iterator pabl = ABasList.begin (); pabl != ABasList.end (); ++pabl)
	{
		CABasList *abl = *pabl;
		CString Art;
		Art.Format (_T("%.0lf"), abl->a); 
		CString Num;
		CString Bez;
		_tcscpy (a_bz1, abl->a_bz1.GetBuffer ());
		_tcscpy (a_bz2, abl->a_bz2.GetBuffer ());

		CString LText;
		LText.Format (_T("%.0lf %s"), abl->a, 
									  abl->a_bz1.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Art;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (a_bz1, i, 2);
        ret = SetItemText (a_bz2, i, 3);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceA::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceA::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceA::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceA::SearchABz1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceA::SearchABz2 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceA::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchABz1 (ListBox, EditText.GetBuffer ());
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchABz2 (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceA::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceA::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}


void CChoiceA::Sort (CListCtrl *ListBox)
{
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
        case 3:
              Sort4 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CABasList *abl = ABasList [i];
		   
		   abl->a = _tstof (ListBox->GetItemText (i, 1));
		   abl->a_bz1 = ListBox->GetItemText (i, 2);
		   abl->a_bz2 = ListBox->GetItemText (i, 3);
	}
}

void CChoiceA::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = ABasList [idx];
}

CABasList *CChoiceA::GetSelectedText ()
{
	CABasList *abl = (CABasList *) SelectedRow;
	return abl;
}

