#ifndef _CHOICEETI_DEF
#define _CHOICEETI_DEF
#pragma once
#include "choicex.h"
#include "etilist.h"
#include <vector>

class CChoiceEti :
	public CChoiceX
{
public:
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
      
    public :
	    std::vector<CEtiList *> EtiList;
      	CChoiceEti(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceEti(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        virtual void RefreshList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchBez (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CEtiList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif