#include "StdAfx.h"
#include "PazCabList.h"

CPazCabList::CPazCabList(void)
{
	cabnum = 0;
	cab = _T("");
	cabinfo = _T("");
}

CPazCabList::CPazCabList(short cabnum,LPTSTR cab, LPTSTR cabinfo)
{
	this->cabnum  = cabnum;
	this->cab     = cab;
	this->cabinfo = cabinfo;
}

CPazCabList::~CPazCabList(void)
{
}
