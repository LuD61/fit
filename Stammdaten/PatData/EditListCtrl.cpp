#include "StdAfx.h"
#include "editlistctrl.h"

int CEditListCtrl::jrhstart = 70;
int CEditListCtrl::jrh1 = 1900;
int CEditListCtrl::jrh2 = 2000;
int CEditListCtrl::sjr = 1954;

BEGIN_MESSAGE_MAP(CEditListCtrl, CListCtrl)
	ON_WM_VSCROLL ()
	ON_WM_HSCROLL ()
	ON_WM_SETFOCUS ()
	ON_WM_KILLFOCUS ()
	ON_WM_LBUTTONDOWN ()
	ON_WM_MOUSEMOVE ()
END_MESSAGE_MAP()

CEditListCtrl::CEditListCtrl(void)
{
	EditRow = EditCol = 0;
	FillList = this;
	GridLines = FALSE;
	SetEditFocus = FALSE;
	dropTarget.Register (this);
	LimitText = -1;
}

CEditListCtrl::~CEditListCtrl(void)
{
}

void CEditListCtrl::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
}


void CEditListCtrl::OnSetFocus (CWnd *oldcWnd)
{

	if (oldcWnd == &ListEdit)
	{
		return;
	}
	else if (oldcWnd == &ListComboBox)
	{
		return;
	}
	StartEnter (2, 0);
}

void CEditListCtrl::OnKillFocus (CWnd *newcWnd)
{

	if (newcWnd == &ListEdit)
	{
		return;
	}
	else if (newcWnd == &ListComboBox)
	{
		return;
	}
	StopEnter ();
}


void CEditListCtrl::OnEnKillFocusEdit ()
{
}

void CEditListCtrl::StartEnterCombo (int col, int row, CVector *Values)
{
	if (IsWindow (ListComboBox.m_hWnd))
	{
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (ListEdit.m_hWnd))
	{
		ListEdit.DestroyWindow ();
	}
	EditRow = row;
	EditCol = col;
    CRect rect;
	GetSubItemRect (row, col, LVIR_BOUNDS , rect);
    HDITEM hdItem;
	rect.top -= 2;
	rect.bottom = rect.top + 140;
	ZeroMemory (&hdItem, sizeof (hdItem));
	hdItem.mask = HDI_FORMAT;
	GetHeaderCtrl ()->GetItem (col, &hdItem);
    if (hdItem.fmt & HDF_RIGHT)
	{
		ListComboBox.Create (WS_VISIBLE | CBS_DROPDOWNLIST |
			                 WS_CHILD | WS_VSCROLL, 
			                 rect, this, 1991);
	}
	else
	{
		ListComboBox.Create (WS_VISIBLE | CBS_DROPDOWNLIST |
			                 WS_CHILD | WS_VSCROLL, 
			                 rect, this, 1991);
	}
	ListComboBox.Clear ();
	CString *value;
	Values->FirstPosition ();
	while ((value = (CString *) Values->GetNext ()) != NULL)
	{
		ListComboBox.AddString (value->GetBuffer (0));
	}
	ListComboBox.SetFont (GetFont ());

    CString Text = GetItemText (row, col);
	int idx = ListComboBox.FindString (-1, Text.TrimRight ().GetBuffer (0));
	if (idx < 0) idx = 0;
	ListComboBox.SetCurSel (idx);

//	ListEdit.SetWindowText (Text);

	SetEditFocus = TRUE;
	ListComboBox.SetFocus ();
//	SetSel (Text);
	SetEditFocus = FALSE;
}

void CEditListCtrl::StartEnter (int col, int row)
{
/*
	if (IsWindow (ListComboBox.m_hWnd))
	{
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (ListEdit.m_hWnd))
	{
		ListEdit.DestroyWindow ();
	}
*/
	StopEnter ();
	EditRow = row;
	EditCol = col;
    CRect rect;
	GetSubItemRect (row, col, LVIR_BOUNDS , rect);
    HDITEM hdItem;
	rect.top -= 2;
	rect.bottom += 2;
	ZeroMemory (&hdItem, sizeof (hdItem));
	hdItem.mask = HDI_FORMAT;
	GetHeaderCtrl ()->GetItem (col, &hdItem);
    if (hdItem.fmt & HDF_RIGHT)
	{
		ListEdit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_RIGHT, 
			             rect, this, 1991);
	}
	else
	{
		ListEdit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_AUTOHSCROLL, 
			              rect, this, 1991);
	}
	ListEdit.SetFont (GetFont ());
    CString Text = GetItemText (row, col);
	ListEdit.SetReadOnly (FALSE);
	if (LimitText != -1)
	{
		Text = Text.Left (LimitText);
		ListEdit.SetWindowText (Text.GetBuffer ());
		ListEdit.SetLimitText (LimitText);
		if (LimitText == 0)
		{
			ListEdit.SetReadOnly ();
		}
	}
	else
	{
		ListEdit.SetWindowText (Text);
	}
	SetEditFocus = TRUE;
	ListEdit.SetFocus ();
	SetSel (Text);
	SetEditFocus = FALSE;
}

void CEditListCtrl::SetSel (CString& Text)
{
   Text.TrimRight ();
   int cpos = Text.GetLength ();
   if (LimitText != -1)
   {
	ListEdit.SetSel (cpos, LimitText);
	ListEdit.ReplaceSel (_T(""));
   }
   ListEdit.SetSel (cpos, cpos);
}

void CEditListCtrl::StopEnter ()
{
	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		ListComboBox.GetLBText (idx, Text);

		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListComboBox.DestroyWindow ();
	}
}

void CEditListCtrl::SetEditText ()
{
	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
	}
}

void CEditListCtrl::FormatText (CString& Text)
{
}

void CEditListCtrl::NextRow ()
{
	int count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditRow ++;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CEditListCtrl::PriorRow ()
{
	int count = GetItemCount ();
	if (EditRow <= 0)
	{
		return;
	}
	StopEnter ();
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CEditListCtrl::OnReturn ()
{
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1)
	{
		if (EditRow >= rowCount - 1)
		{
			return;
		}
		EditCol = 2;
		EditRow ++;
	}
	else
	{
		StopEnter ();
		EditCol ++;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CEditListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CEditListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 0)
	{
		return;
	}
	StopEnter ();
	EditCol --;
    StartEnter (EditCol, EditRow);
}

void CEditListCtrl::OnKeyDown(UINT nTCHAR, UINT nRepCnt, UINT nFlags)
{
	int diff = 0;
}

BOOL CEditListCtrl::OnKeyD (WPARAM vKey)
{
	int diff = 0;
    if (GetItemCount () == 0) return FALSE;
	if (!IsWindow (ListEdit.m_hWnd) &&
		!IsWindow (ListComboBox.m_hWnd)) 
	{
		StartEnter (1, 0);
		return TRUE;
	}
	switch (vKey)
	{
	case VK_DOWN :
		NextRow ();
		return TRUE;
	case VK_UP :
		PriorRow ();
		return TRUE;
	case VK_RETURN :
		OnReturn ();
		return TRUE;
	case VK_TAB :
		if (GetKeyState (VK_SHIFT) < 0)
		{
			PriorCol ();
		}
		else
		{
			NextCol ();
		}
		return TRUE;
	case VK_F6 :
		InsertRow ();
		return TRUE;
	case VK_F7 :
		DeleteRow ();
		return TRUE;
	case VK_NEXT :
		diff = EditRow - GetTopIndex ();
		StopEnter ();
		SendMessage (WM_KEYDOWN, vKey, 0l);
		EditRow = GetTopIndex () + diff;
		EditRow = (EditRow < GetItemCount ()) ? EditRow : 
		           GetItemCount () - 1;
        StartEnter (EditCol, EditRow);
		return TRUE;
	case VK_PRIOR :
		diff = EditRow - GetTopIndex ();
		StopEnter ();
		SendMessage (WM_KEYDOWN, vKey, 0l);
		EditRow = GetTopIndex () + diff;
		EditRow = (EditRow >= 0) ? EditRow : 
		           0;
        StartEnter (EditCol, EditRow);
		return TRUE;
	}
	return FALSE;
}

void CEditListCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int diff = 0;

    if (GetItemCount ()  == 0) return;
	switch (nSBCode)
	{
	case SB_LINEDOWN :
	case SB_PAGEDOWN :
		diff = EditRow - GetTopIndex ();
		StopEnter ();
		CListCtrl::OnVScroll (nSBCode, nPos, pScrollBar);
		EditRow = GetTopIndex () + diff;
		EditRow = (EditRow < GetItemCount ()) ? EditRow : 
		           GetItemCount () - 1;
        StartEnter (EditCol, EditRow);
		return;
	case SB_LINEUP :
	case SB_PAGEUP :
		diff = EditRow - GetTopIndex ();
		StopEnter ();
		CListCtrl::OnVScroll (nSBCode, nPos, pScrollBar);
		EditRow = GetTopIndex () + diff;
		EditRow = (EditRow >= 0) ? EditRow : 
		           0;
        StartEnter (EditCol, EditRow);
		return;
	case SB_THUMBPOSITION :
	case SB_THUMBTRACK :
		diff = EditRow - GetTopIndex ();
		StopEnter ();
		CListCtrl::OnVScroll (nSBCode, nPos, pScrollBar);
		EditRow = GetTopIndex () + diff;
		EditRow = (EditRow < GetItemCount ()) ? EditRow : 
		           GetItemCount () - 1;
        StartEnter (EditCol, EditRow);
		return;
	}
}

void CEditListCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{

    if (GetItemCount ()  == 0) return;
	StopEnter ();
	CListCtrl::OnHScroll (nSBCode, nPos, pScrollBar);
    StartEnter (EditCol, EditRow);
}

BOOL CEditListCtrl::OnLBuDown (CPoint& p)
{
	CRect rect;
	GetWindowRect (&rect);
	if (p.x < rect.left || p.x > rect.right) return FALSE;
	if (p.y < rect.top  || p.y > rect.bottom) return FALSE;

	ScreenToClient (&p);
	if (p.x < 0 || p.y < 0) 
	{
		StopEnter ();
		return FALSE;
	}
    LVHITTESTINFO lvhti;	
    lvhti.pt = p;	
   
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (rowCount == 0) return FALSE;

	SubItemHitTest(&lvhti);
   
    if (lvhti.flags & LVHT_ONITEMLABEL)
	{
		StartEnter (lvhti.iSubItem, lvhti.iItem);
		return TRUE;
	}
	return FALSE;
}

void CEditListCtrl::OnLButtonDown (UINT flags,CPoint p)
{
	CRect rect;
	GetWindowRect (&rect);
	if (p.x < 0 || p.y < 0) 
	{
		return;
	}
    LVHITTESTINFO lvhti;	
    lvhti.pt = p;	

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (rowCount == 0) return;

	SubItemHitTest(&lvhti);

    if ((flags & MK_SHIFT) != 0)
	{
		RunShiftItemClicked (lvhti.iItem);
		return;
	}
    if ((flags & MK_CONTROL) != 0)
	{
		RunCtrlItemClicked (lvhti.iItem);
		return;
	}

	if (lvhti.iSubItem == 0)
	{
		RunItemClicked (lvhti.iItem);
	}
   
    if (lvhti.flags & LVHT_ONITEMLABEL)
	{
		StartEnter (lvhti.iSubItem, lvhti.iItem);
		ClearSelect ();
		return;
	}
}

void CEditListCtrl::OnMouseMove (UINT flags,CPoint p)
{
	CListCtrl::OnMouseMove (flags, p);
	if ((flags & MK_LBUTTON) == 0)
	{
		return;
	}
	CRect rect;
	GetWindowRect (&rect);
	if (p.x < 0 || p.y < 0) 
	{
		return;
	}
    LVHITTESTINFO lvhti;	
    lvhti.pt = p;	

	SubItemHitTest(&lvhti);

	HiLightItem (lvhti.iItem);
}

BOOL CEditListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	DeleteItem (EditRow);
	if (GetItemCount () == 0)
	{
		AppendEmpty ();
		StartEnter (1, 0);
	}
	else if (EditRow == GetItemCount ())
	{
		PriorRow ();
	}
	else
	{
		StartEnter (EditCol, EditRow);
	}
	return TRUE;
}

BOOL CEditListCtrl::InsertRow ()
{
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	for (int i = 1; i < colCount; i ++)
	{
		FillList.SetItemText (_T(""), EditRow, i);
	}
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CEditListCtrl::AppendEmpty ()
{
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	FillList.InsertItem (rowCount, -1);
	for (int i = 1; i < colCount; i ++)
	{
		FillList.SetItemText (_T(""), rowCount, i);
	}
	return TRUE;
}

void CEditListCtrl::EnsureColVisible (int Col)
{
	int  cols = GetHeaderCtrl ()->GetItemCount ();
	if (Col >= cols) return;

    CRect rect;
	GetSubItemRect (EditRow, Col, LVIR_BOUNDS , rect);
	ClientToScreen (&rect);
    CRect wrect;
    GetWindowRect (&wrect);
	if (rect.left > wrect.left && rect.right < wrect.right)
	{
		return;
	}
	CRect r;
	if (rect.right >= wrect.right)
	{
		for (int i = 0; i < cols; i ++)
		{
			GetSubItemRect (EditRow, i, LVIR_BOUNDS , r);
			rect.right -= (r.right - r.left);
			if (rect.right < wrect.right) break;
		}
		Scroll (CSize (r.right, GetTopIndex ())); 
	}
	else if (rect.left <= wrect.left)
	{
		for (int i = 0; i < cols; i ++)
		{
			GetSubItemRect (EditRow, i, LVIR_BOUNDS , r);
			rect.left += (r.right - r.left);
			if (rect.left > wrect.left) break;
		}
		Scroll (CSize (r.left, GetTopIndex ())); 
	}
}
 

double CEditListCtrl::StrToDouble (LPTSTR string)
{
 double fl;
 double ziffer;
 double teiler;
 short minus;

 if (string == (LPTSTR) 0) return (double) 0.0;
 fl = 0;
 teiler = 10;
 minus = 1;
 while (*string < 0X30)
 {
  if (*string == 0)
    return (0.0);
  if (*string == '-')
    break;
  if (*string == '+')
    break;
  if (*string == '.')
    break;
  if (*string == ',')
    break;
  string ++;
 }

 if (*string == '-')
 {
  minus = -1;
  string ++;
 }
 else if (*string == '+')
 {
  string ++;
 }

 while (*string)
 {
  if (*string == '.')
  {
   break;
  }
  if (*string == ',')
  {
   break;
  }
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = (fl * teiler) + ziffer;
  string ++;
 }

 if (*string == '.')
   ;
 else if (*string == ',')
   ;
 else
 {
  fl *= minus;
  return (fl);
 }

 string ++;
 while (*string)
 {
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = fl + (ziffer / teiler);
  teiler *= 10;
  string ++;
 }
 fl *= minus;
 return (fl);
}

double CEditListCtrl::StrToDouble (CString& Text)
{
	return StrToDouble (Text.GetBuffer ());
}

void CEditListCtrl::DatFormat (CString &Date, const LPTSTR picture)
/**
Datumsfeld formatieren.
**/
{
      TCHAR tags [3];
      TCHAR mons [3];
      TCHAR jrs [3];
      short tag;
      short mon;
      short jr;
      int point;

	  Date.Trim ();
	  if (Date == "")
	  {
		         return;
	  }
	  if (Date.GetLength () < 6)
	  {
		         Date = "";
		         return;
	  }
	  if ((point = Date.Find (_T("."))) >= 0)
      {
                 tag = _tstoi (Date.Left (2));
                 point ++;
                 mon = _tstoi (Date.Mid (point, 2));
				 point = Date.Find (_T("."), point);
                 if (point < 0)
                 {
                              jr = jrh1;
                 }
                 else
                 {
                               point ++;
                               jr = _tstoi (Date.Mid (point));
                 }
      }
      else
      {
		         _tcscpy (tags, Date.Left (2).GetBuffer (0));
		         _tcscpy (mons, Date.Mid (2, 2).GetBuffer (0));
		         _tcscpy (jrs, Date.Mid (4).GetBuffer (0));
                 tag = _tstoi (tags);
                 mon = _tstoi (mons);
                 jr = _tstoi (jrs);
      }

      if (jr < 100)
      {
             if (jr < jrhstart)
             {
                  jr += jrh2;
             }
             else
             {
                  jr += jrh1;
             }
      }


      if (mon > 12 || mon < 1) 
      {
                 Date = "";
                 return;
      }

      if (tag < 1)
      {
		         Date = "";
                 return;
      }
                 
      if (IsMon31 (mon) && tag > 31) 
      {
		         Date = "";
                 return;
      }
      if (IsMon30 (mon) && tag > 30) 
      {
		         Date = "";
                 return;
      }

      if (IsMon29 (mon, jr) && tag > 29) 
      {
		         Date = "";
                 return;
      }

      if (IsMon28 (mon, jr) && tag > 28) 
      {
		         Date = "";
                 return;
      }

      if (jr < 100)
      {
             if (jr < jrhstart)
             {
                  jr += jrh2;
             }
             else
             {
                  jr += jrh1;
             }
      }

      if (memcmp (picture, "dd.mm.yyyy", 10) == 0)
      {
                Date.Format (_T("%02hd.%02hd.%04hd"),
                                  tag,mon,jr);
      }
      else if (memcmp (picture, _T("dd.mm.yy"), 8) == 0)
      {
                 Date.Format (_T("%02hd.%02hd.%02hd"),
                                tag,mon,jr % 100);
      }
                   
      else if (memcmp (picture, _T("ddmmyyyy"), 8) == 0)
      {
                 Date.Format (_T("%02hd%02hd%04hd"),
                                  tag,mon,jr);
      }
      else if (memcmp (picture, _T("ddmmyy"), 6) == 0)
      {
                 Date.Format (_T("%02hd%02hd%02hd"),
                                  tag,mon,jr % 100);
      }
      else
      {
		         Date = "";
      }
}

BOOL CEditListCtrl::IsMon31 (int mon)
/**
Monat im Datum pruefen.
**/
{
         static int mon31[] = {1,3,5,7,8,10,12};
         int i;

         for (i = 0; i < 7; i ++)
         {
                      if (mon == mon31[i]) return TRUE;
         }
         return FALSE;
}

BOOL CEditListCtrl::IsMon30 (int mon)
/**
Monat im Datum pruefen.
**/
{
         static int mon30[] = {4,6,9,11};
         int i;

         for (i = 0; i < 4; i ++)
         {
                      if (mon == mon30[i]) return TRUE;
         }
         return FALSE;
}

BOOL CEditListCtrl::IsMon29 (int mon, int jr)
/**
Monat im Datum pruefen.
**/
{
         static int sjr = 1954;
         int jrdiff; 

         if (mon != 2) return FALSE; 

         jrdiff = jr - sjr;
         if (jr % 4 == 0) return TRUE; 
         return FALSE;
}
 

BOOL CEditListCtrl::IsMon28 (int mon, int jr)
/**
Monat im Datum pruefen.
**/
{
         static int sjr = 1954;
         int jrdiff; 

         if (mon != 2) return FALSE; 

         jrdiff = jr - sjr;
         if (jr % 4) return TRUE; 
         return FALSE;
}
 
void CEditListCtrl::RunItemClicked (int Item)
{
}

void CEditListCtrl::RunCtrlItemClicked (int Item)
{
}
void CEditListCtrl::RunShiftItemClicked (int Item)
{
}
void CEditListCtrl::HiLightItem (int Item)
{
}

void CEditListCtrl::ClearSelect ()
{
}
