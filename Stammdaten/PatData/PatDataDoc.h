// PatDataDoc.h : Schnittstelle der Klasse CPatDataDoc
//


#pragma once

class CPatDataDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CPatDataDoc();
	DECLARE_DYNCREATE(CPatDataDoc)

// Attribute
public:

// Operationen
public:

// Überschreibungen
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CPatDataDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


