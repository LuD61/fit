#ifndef _ETIVIEW_DEF
#define _ETIVIEW_DEF
#pragma once
#include "afxwin.h"

class CEtiView :
	public CView
{
public:
	DECLARE_DYNCREATE(CEtiView)
	CEtiView(void);
	~CEtiView(void);

	virtual void OnDraw(CDC* pDC);  // �berladen zum Zeichnen dieser Ansicht

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};
#endif