#pragma once
#ifndef _PAZTEXTE_DEF
#define _PAZTEXTE_DEF
#include "CtrlGrid.h"
#include "PrZutListCtrl.h"
#include "ListDropTarget.h"
#include "ListDropEx.h"
#include "FillList.h"
#include "FormTab.h"
#include "SgTab.h"
#include "a_bas.h"
#include "pr_a_gx.h"
#include "eti_typ.h"
#include "SgInfos.h"
#include "ChoiceTxt.h"
#include "ChoiceEti.h"
#include "CodeProperties.h"
#include "NumEdit.h"
#include "FlatButton.h"

#include <vector>

#define IDC_TEXTCHOICE                   1050
// #define IDC_ETICHOICE					 1055

// CPazTexte-Formularansicht

class CPazTexte : public DbFormView
{
	DECLARE_DYNCREATE(CPazTexte)

protected:
	CPazTexte();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPazTexte();

public:
	enum { IDD = IDD_DIALOG1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	CCtrlGrid CtrlGrid;
	CCtrlGrid TextGrid;
	CCtrlGrid EtiGrid;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
    afx_msg void OnSize(UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG*);
public:
	CChoiceTxt *Choice;
	BOOL ModalChoice;
	CChoiceEti *ChoiceEti;
	BOOL ModalChoiceEti;
    void MoveSize(int, int);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	CListDropEx dropTarget;
	CFillList FillList;
	CFormTab Form;
    CImageList ListCheck;    // image list for icon view 
	std::vector<BOOL> vSelect;
	CCodeProperties Code;


	DECLARE_MESSAGE_MAP()

	TCHAR *TxtTable[21];
	short *SgTable[21];

	CSgTab SgTab;
	A_BAS_CLASS A_bas;
	PR_A_GX_CLASS Pr_ausz_gx;
	ETI_TYP_CLASS EtiTyp;
	CSgInfos SgInfos;
	CStatic m_Border;
	CStatic m_LTextNr;
	CStatic m_LEti;
	CNumEdit m_TextNr;
	CNumEdit m_Eti;
	CEdit m_EtiBz;
	CFlatButton m_Left;
	CFlatButton m_Right;
	CVector HeadControls;

    CPrZutListCtrl m_Texte;
	CFont Font;
	CFont lFont;
	CButton m_TextChoice;
	CButton m_EtiChoice;
	void Register ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    void ListPaste ();
    void ListCopy ();
    virtual DROPEFFECT OnDrop (CWnd* , COleDataObject*,  DROPEFFECT,  DROPEFFECT, CPoint);
    virtual BOOL OnKeyup ();
	virtual BOOL OnReturn ();
    virtual BOOL Read ();
/*
    virtual BOOL ReadEan ();
	virtual BOOL Write ();
	virtual BOOL TextCent ();
	virtual BOOL TextLeft ();
	virtual BOOL TextRight ();
*/
	virtual BOOL Show ();
	virtual void OnF9 ();
	afx_msg void OnLvnBegindrag(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMarkAll();
	afx_msg void OnUnMarkAll();
	afx_msg void OnUpdateMarkAll(CCmdUI *pCmdUI);
	BOOL FillSg (long);
//    void DbToUniCode (LPTSTR);
    void DbToUniCode (LPTSTR, LPSTR);
    void OnTextchoice(); 
    void OnTextSelected ();
    void OnTextCanceled ();
    void OnEtiChoice ();
	void write ();
	afx_msg void OnFileSave();
	afx_msg void OnLanguage();
	virtual BOOL TextCent ();
	virtual BOOL TextLeft ();
	virtual BOOL TextRight ();
	afx_msg void OnTextCent();
	afx_msg void OnTextLeft();
	afx_msg void OnTextRight();
	afx_msg void OnDelete();
    void EnableHeadControls (BOOL enable);
    void StepBack ();
	afx_msg void OnBack();
    afx_msg void OnActivate(UINT nState,  CWnd* pWndOther,  BOOL bMinimized );
public :
	void OnRecChange();
	afx_msg void OnBnClickedLeft();
	afx_msg void OnBnClickedRight();
};
#endif

