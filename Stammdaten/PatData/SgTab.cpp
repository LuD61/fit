#include "StdAfx.h"
#include ".\sgtab.h"

CSgTab::CSgTab(void)
{
}

CSgTab::~CSgTab(void)
{
}

CSg *CSgTab::GetNext ()
{
	return (CSg *) CVector::GetNext ();
}

CSg *CSgTab::Get (int i)
{
	return (CSg *) CVector::Get (i);
}

CSg *CSgTab::Get (short sg)
{
	CSg *cSg;
	FirstPosition ();
	while ((cSg = GetNext ()) != NULL)
	{
		if (*cSg == sg)
		{
			return (cSg);
		}
	}
	return NULL;
}
