// 16500Doc.h : Schnittstelle der Klasse CMyDoc
//


#pragma once

class CMyDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CMyDoc();
	DECLARE_DYNCREATE(CMyDoc)

// Attribute
public:

// Operationen
public:

// Überschreibungen
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CMyDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


