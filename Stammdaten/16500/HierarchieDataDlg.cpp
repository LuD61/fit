// HierarchieDataDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "HierarchieDataDlg.h"
#include "UniFormField.h"


// CHierarchieDataDlg-Dialogfeld

IMPLEMENT_DYNAMIC(CHierarchieDataDlg, CDialog)

CHierarchieDataDlg::CHierarchieDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHierarchieDataDlg::IDD, pParent)
{
	hBrush = NULL;
	hBrushStatic = NULL;
}

CHierarchieDataDlg::~CHierarchieDataDlg()
{
}

void CHierarchieDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_H1_GROUP, m_H1Group);
	DDX_Control(pDX, IDC_LHIERARCHIE1, m_LHierarchie1);
	DDX_Control(pDX, IDC_HIERARCHIE1, m_Hierarchie1);
	DDX_Control(pDX, IDC_LH1_BEZ, m_LH1Bez);
	DDX_Control(pDX, IDC_H1_BEZ, m_H1Bez);
	DDX_Control(pDX, IDC_LH1_BEZKRZ, m_LH1BezKrz);
	DDX_Control(pDX, IDC_H1_BEZKRZ, m_H1BezKrz);

	DDX_Control(pDX, IDC_H2_GROUP, m_H2Group);
	DDX_Control(pDX, IDC_LHIERARCHIE2, m_LHierarchie2);
	DDX_Control(pDX, IDC_HIERARCHIE2, m_Hierarchie2);
	DDX_Control(pDX, IDC_LH2_BEZ, m_LH2Bez);
	DDX_Control(pDX, IDC_H2_BEZ, m_H2Bez);
	DDX_Control(pDX, IDC_LH2_BEZKRZ, m_LH2BezKrz);
	DDX_Control(pDX, IDC_H2_BEZKRZ, m_H2BezKrz);

	DDX_Control(pDX, IDC_H3_GROUP, m_H3Group);
	DDX_Control(pDX, IDC_LHIERARCHIE3, m_LHierarchie3);
	DDX_Control(pDX, IDC_HIERARCHIE3, m_Hierarchie3);
	DDX_Control(pDX, IDC_LH3_BEZ, m_LH3Bez);
	DDX_Control(pDX, IDC_H3_BEZ, m_H3Bez);
	DDX_Control(pDX, IDC_LH3_BEZKRZ, m_LH3BezKrz);
	DDX_Control(pDX, IDC_H3_BEZKRZ, m_H3BezKrz);

	DDX_Control(pDX, IDC_H4_GROUP, m_H4Group);
	DDX_Control(pDX, IDC_LHIERARCHIE4, m_LHierarchie4);
	DDX_Control(pDX, IDC_HIERARCHIE4, m_Hierarchie4);
	DDX_Control(pDX, IDC_LH4_BEZ, m_LH4Bez);
	DDX_Control(pDX, IDC_H4_BEZ, m_H4Bez);
	DDX_Control(pDX, IDC_LH4_BEZKRZ, m_LH4BezKrz);
	DDX_Control(pDX, IDC_H4_BEZKRZ, m_H4BezKrz);
}


BEGIN_MESSAGE_MAP(CHierarchieDataDlg, CDialog)
	ON_WM_SIZE ()
//	ON_WM_CTLCOLOR ()
	ON_BN_CLICKED(IDC_HSAVE ,   OnSave)
	ON_BN_CLICKED(IDC_HDELETE , Delete)
	ON_EN_KILLFOCUS(IDC_HIERARCHIE1, OnEnKillFocusH1)
	ON_EN_KILLFOCUS(IDC_HIERARCHIE2, OnEnKillFocusH2)
	ON_EN_KILLFOCUS(IDC_HIERARCHIE3, OnEnKillFocusH3)
	ON_EN_KILLFOCUS(IDC_HIERARCHIE4, OnEnKillFocusH4)
END_MESSAGE_MAP()


// CHierarchieDataDlg-Meldungshandler

BOOL CHierarchieDataDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

    m_Save.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_HSAVE);
	m_Save.LoadBitmap (IDB_SAVE);
	m_Save.SetToolTip (_T("speichern"));

    m_Delete.Create (_T("l�schen"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_HDELETE);
	m_Delete.LoadBitmap (IDB_DELETE);
	m_Delete.LoadMask (IDB_DELETE_MASK);
	m_Delete.SetToolTip (_T("l�schen"));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	memcpy (&H1->h1, &h1_null, sizeof (struct H1));
	memcpy (&H2->h2, &h2_null, sizeof (struct H2));
	memcpy (&H3->h3, &h3_null, sizeof (struct H3));
	memcpy (&H4->h4, &h4_null, sizeof (struct H4));

/* --> 060912 boese boese 
	Form.Add (new CFormField    (&m_Hierarchie1,EDIT,        (long *)  &H1->h1.hirarchie1, VLONG));
	Form.Add (new CUniFormField (&m_H1Bez,      EDIT,        (char *)  &H1->h1.h1_bez, VCHAR));
	Form.Add (new CUniFormField (&m_H1BezKrz,   EDIT,        (char *)  &H1->h1.h1_bezkrz, VCHAR));

	Form.Add (new CFormField    (&m_Hierarchie2,EDIT,        (long *)  &H2->h2.hirarchie2, VLONG));
	Form.Add (new CUniFormField (&m_H2Bez,      EDIT,        (char *)  &H2->h2.h2_bez, VCHAR));
	Form.Add (new CUniFormField (&m_H2BezKrz,   EDIT,        (char *)  &H2->h2.h2_bezkrz, VCHAR));

	Form.Add (new CFormField    (&m_Hierarchie3,EDIT,        (long *)  &H3->h3.hirarchie3, VLONG));
	Form.Add (new CUniFormField (&m_H3Bez,      EDIT,        (char *)  &H3->h3.h3_bez, VCHAR));
	Form.Add (new CUniFormField (&m_H3BezKrz,   EDIT,        (char *)  &H3->h3.h3_bezkrz, VCHAR));

	Form.Add (new CFormField    (&m_Hierarchie4,EDIT,        (long *)  &H4->h4.hirarchie4, VLONG));
	Form.Add (new CUniFormField (&m_H4Bez,      EDIT,        (char *)  &H4->h4.h4_bez, VCHAR));
	Form.Add (new CUniFormField (&m_H4BezKrz,   EDIT,        (char *)  &H4->h4.h4_bezkrz, VCHAR));
< ---- */

	
	Form.Add (new CFormField    (&m_Hierarchie1,EDIT,        (long *)  &H1->h1.hirarchie1, VLONG));
	Form.Add (new CUniFormField (&m_H1Bez,      EDIT,        (char *)  H1->h1.h1_bez, VCHAR));
	Form.Add (new CUniFormField (&m_H1BezKrz,   EDIT,        (char *)  H1->h1.h1_bezkrz, VCHAR));

	Form.Add (new CFormField    (&m_Hierarchie2,EDIT,        (long *)  &H2->h2.hirarchie2, VLONG));
	Form.Add (new CUniFormField (&m_H2Bez,      EDIT,        (char *)  H2->h2.h2_bez, VCHAR));
	Form.Add (new CUniFormField (&m_H2BezKrz,   EDIT,        (char *)  H2->h2.h2_bezkrz, VCHAR));

	Form.Add (new CFormField    (&m_Hierarchie3,EDIT,        (long *)  &H3->h3.hirarchie3, VLONG));
	Form.Add (new CUniFormField (&m_H3Bez,      EDIT,        (char *)  H3->h3.h3_bez, VCHAR));
	Form.Add (new CUniFormField (&m_H3BezKrz,   EDIT,        (char *)  H3->h3.h3_bezkrz, VCHAR));

	Form.Add (new CFormField    (&m_Hierarchie4,EDIT,        (long *)  &H4->h4.hirarchie4, VLONG));
	Form.Add (new CUniFormField (&m_H4Bez,      EDIT,        (char *)  H4->h4.h4_bez, VCHAR));
	Form.Add (new CUniFormField (&m_H4BezKrz,   EDIT,        (char *)  H4->h4.h4_bezkrz, VCHAR));

	
	CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (10, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

//Grid Toolbar
	ToolGrid.Create (this, 2, 2);
    ToolGrid.SetBorder (0, 0);
    ToolGrid.SetGridSpace (0, 0);

	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 0, 1, 1);
	ToolGrid.Add (c_Save);

	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 1, 0, 1, 1);
	ToolGrid.Add (c_Delete);

	CCtrlInfo *c_ToolGrid = new CCtrlInfo (&ToolGrid, 1, 0, 1, 1);
	CtrlGrid.Add (c_ToolGrid);

// Grid f�r H1
    H1Grid.Create (this, 20, 20);
    H1Grid.SetBorder (5, 0);
    H1Grid.SetCellHeight (15);
    H1Grid.SetFontCellHeight (this);
    H1Grid.SetGridSpace (5, 8);

	CCtrlInfo *c_LHierarchie1 = new CCtrlInfo (&m_LHierarchie1, 0, 0, 1, 1);
	H1Grid.Add (c_LHierarchie1);
	CCtrlInfo *c_Hierarchie1 = new CCtrlInfo (&m_Hierarchie1, 1, 0, 1, 1);
	H1Grid.Add (c_Hierarchie1);

	CCtrlInfo *c_LH1Bez = new CCtrlInfo (&m_LH1Bez, 0, 1, 1, 1);
	H1Grid.Add (c_LH1Bez);
	CCtrlInfo *c_H1Bez = new CCtrlInfo (&m_H1Bez, 1, 1, 1, 1);	
	c_H1Bez->rightspace = 20;
	H1Grid.Add (c_H1Bez);

	CCtrlInfo *c_LH1BezKrz = new CCtrlInfo (&m_LH1BezKrz, 0, 2, 1, 1);
	H1Grid.Add (c_LH1BezKrz);
	CCtrlInfo *c_H1BezKrz = new CCtrlInfo (&m_H1BezKrz, 1, 2, 1, 1);
	H1Grid.Add (c_H1BezKrz);

// Grid f�r H2
    H2Grid.Create (this, 20, 20);
    H2Grid.SetBorder (5, 0);
    H2Grid.SetCellHeight (15);
    H2Grid.SetFontCellHeight (this);
    H2Grid.SetGridSpace (5, 8);

	CCtrlInfo *c_LHierarchie2 = new CCtrlInfo (&m_LHierarchie2, 0, 0, 1, 1);
	H2Grid.Add (c_LHierarchie2);
	CCtrlInfo *c_Hierarchie2 = new CCtrlInfo (&m_Hierarchie2, 1, 0, 1, 1);
	H2Grid.Add (c_Hierarchie2);

	CCtrlInfo *c_LH2Bez = new CCtrlInfo (&m_LH2Bez, 0, 1, 1, 1);
	H2Grid.Add (c_LH2Bez);
	CCtrlInfo *c_H2Bez = new CCtrlInfo (&m_H2Bez, 1, 1, 1, 1);	
	c_H2Bez->rightspace = 20;
	H2Grid.Add (c_H2Bez);

	CCtrlInfo *c_LH2BezKrz = new CCtrlInfo (&m_LH2BezKrz, 0, 2, 1, 1);
	H2Grid.Add (c_LH2BezKrz);
	CCtrlInfo *c_H2BezKrz = new CCtrlInfo (&m_H2BezKrz, 1, 2, 1, 1);
	H2Grid.Add (c_H2BezKrz);

// Grid f�r H3
    H3Grid.Create (this, 20, 20);
    H3Grid.SetBorder (5, 0);
    H3Grid.SetCellHeight (15);
    H3Grid.SetFontCellHeight (this);
    H3Grid.SetGridSpace (5, 8);

	CCtrlInfo *c_LHierarchie3 = new CCtrlInfo (&m_LHierarchie3, 0, 0, 1, 1);
	H3Grid.Add (c_LHierarchie3);
	CCtrlInfo *c_Hierarchie3 = new CCtrlInfo (&m_Hierarchie3, 1, 0, 1, 1);
	H3Grid.Add (c_Hierarchie3);

	CCtrlInfo *c_LH3Bez = new CCtrlInfo (&m_LH3Bez, 0, 1, 1, 1);
	H3Grid.Add (c_LH3Bez);
	CCtrlInfo *c_H3Bez = new CCtrlInfo (&m_H3Bez, 1, 1, 1, 1);	
	c_H3Bez->rightspace = 20;
	H3Grid.Add (c_H3Bez);

	CCtrlInfo *c_LH3BezKrz = new CCtrlInfo (&m_LH3BezKrz, 0, 2, 1, 1);
	H3Grid.Add (c_LH3BezKrz);
	CCtrlInfo *c_H3BezKrz = new CCtrlInfo (&m_H3BezKrz, 1, 2, 1, 1);
	H3Grid.Add (c_H3BezKrz);

// Grid f�r H4
    H4Grid.Create (this, 20, 20);
    H4Grid.SetBorder (5, 0);
    H4Grid.SetCellHeight (15);
    H4Grid.SetFontCellHeight (this);
    H4Grid.SetGridSpace (5, 8);

	CCtrlInfo *c_LHierarchie4 = new CCtrlInfo (&m_LHierarchie4, 0, 0, 1, 1);
	H4Grid.Add (c_LHierarchie4);
	CCtrlInfo *c_Hierarchie4 = new CCtrlInfo (&m_Hierarchie4, 1, 0, 1, 1);
	H4Grid.Add (c_Hierarchie4);

	CCtrlInfo *c_LH4Bez = new CCtrlInfo (&m_LH4Bez, 0, 1, 1, 1);
	H4Grid.Add (c_LH4Bez);
	CCtrlInfo *c_H4Bez = new CCtrlInfo (&m_H4Bez, 1, 1, 1, 1);	
	c_H4Bez->rightspace = 20;
	H4Grid.Add (c_H4Bez);

	CCtrlInfo *c_LH4BezKrz = new CCtrlInfo (&m_LH4BezKrz, 0, 2, 1, 1);
	H4Grid.Add (c_LH4BezKrz);
	CCtrlInfo *c_H4BezKrz = new CCtrlInfo (&m_H4BezKrz, 1, 2, 1, 1);
	H4Grid.Add (c_H4BezKrz);


// Rahmen f�r H1
	CCtrlInfo *c_H1Group = new CCtrlInfo (&m_H1Group, 0, 1, DOCKRIGHT, 4);
	c_H1Group->SetCellPos (0,6);
	CtrlGrid.Add (c_H1Group);

	CCtrlInfo *c_H1Grid = new CCtrlInfo (&H1Grid, 1, 2, 1, 1);
	c_H1Grid->SetCellPos (0,6);
	CtrlGrid.Add (c_H1Grid);

// Rahmen f�r H2
	CCtrlInfo *c_H2Group = new CCtrlInfo (&m_H2Group, 0, 6, DOCKRIGHT, 4);
	c_H2Group->SetCellPos (0,6);
	CtrlGrid.Add (c_H2Group);


	CCtrlInfo *c_H2Grid = new CCtrlInfo (&H2Grid, 1, 7, 1, 1);
	c_H2Grid->SetCellPos (0,6);
	CtrlGrid.Add (c_H2Grid);


// Rahmen f�r H3
	CCtrlInfo *c_H3Group = new CCtrlInfo (&m_H3Group, 0, 11, DOCKRIGHT, 4);
	c_H3Group->SetCellPos (0,6);
	CtrlGrid.Add (c_H3Group);

	CCtrlInfo *c_H3Grid = new CCtrlInfo (&H3Grid, 1, 12, 1, 1);
	c_H3Grid->SetCellPos (0,6);
	CtrlGrid.Add (c_H3Grid);

// Rahmen f�r H4
	CCtrlInfo *c_H4Group = new CCtrlInfo (&m_H4Group, 0, 16, DOCKRIGHT, 4);
	c_H4Group->SetCellPos (0,6);
	CtrlGrid.Add (c_H4Group);


	CCtrlInfo *c_H4Grid = new CCtrlInfo (&H4Grid, 1, 17, 1, 1);
	c_H4Grid->SetCellPos (0,6);
	CtrlGrid.Add (c_H4Grid);


	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	m_Save.Tooltip.SetFont (&Font);
	m_Save.Tooltip.SetSize ();
	m_Delete.Tooltip.SetFont (&Font);
	m_Delete.Tooltip.SetSize ();
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();

	return FALSE;
}

HBRUSH CHierarchieDataDlg::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CHierarchieDataDlg::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CHierarchieDataDlg::PreTranslateMessage(MSG* pMsg)
{
	return FALSE;
}

void CHierarchieDataDlg::Update ()
{
	Form.Show ();
}

void CHierarchieDataDlg::OnSave ()
{
	Form.Get ();
	H1->dbupdate ();

	if (H1->h1.hirarchie1 == 0 || 
		H2->h2.hirarchie2 != 0)
	{
		H2->h2.hirarchie1 = H1->h1.hirarchie1;
		H2->dbupdate ();
	}

	if (H1->h1.hirarchie1 == 0 && 
		H2->h2.hirarchie2 == 0 || 
		H3->h3.hirarchie3 != 0)
	{
		H3->h3.hirarchie1 = H2->h2.hirarchie1;
		H3->h3.hirarchie2 = H2->h2.hirarchie2;
		H3->dbupdate ();
	}

	if (H1->h1.hirarchie1 == 0 && 
		H2->h2.hirarchie2 == 0 && 
		H3->h3.hirarchie3 == 0 || 
		H4->h4.hirarchie4 != 0)
	{
		H4->h4.hirarchie1 = H3->h3.hirarchie1;
		H4->h4.hirarchie2 = H3->h3.hirarchie2;
		H4->h4.hirarchie3 = H3->h3.hirarchie3;
		H4->dbupdate ();
	}

	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update ();
	}
}

void CHierarchieDataDlg::Delete ()
{
	Form.Get ();
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->DeleteRow ();
	}
	Form.Show ();
}

void CHierarchieDataDlg::OnEnKillFocusH1 ()
{
	Form.Get ();
	_tcscpy (H1->h1.h1_bez,_T(""));
	_tcscpy (H1->h1.h1_bezkrz,_T(""));
	memcpy (&H2->h2, &h2_null, sizeof (struct H2));
	memcpy (&H3->h3, &h3_null, sizeof (struct H3));
	memcpy (&H4->h4, &h4_null, sizeof (struct H4));
	if (H1->dbreadfirst () != 0)
	{
		Form.Show ();
		return;
	}

	H2->h2.hirarchie1 = H1->h1.hirarchie1;
	H2->sqlopen (cursor_h21);
	if (H2->sqlfetch (cursor_h21) != 0)
	{
		H2->dbreadfirst ();
		Form.Show ();
		return;
	}
	H3->h3.hirarchie2 = H2->h2.hirarchie2;
	H3->sqlopen (cursor_h32);
	if (H3->sqlfetch (cursor_h32) != 0)
	{
		H3->dbreadfirst ();
		Form.Show ();
		return;
	}
	H4->h4.hirarchie3 = H3->h3.hirarchie3;
	H4->sqlopen (cursor_h43);
	H4->sqlfetch (cursor_h43);
	H4->dbreadfirst ();
	Form.Show ();
}

void CHierarchieDataDlg::OnEnKillFocusH2 ()
{
	Form.Get ();
	_tcscpy (H2->h2.h2_bez,_T(""));
	_tcscpy (H2->h2.h2_bezkrz,_T(""));
	memcpy (&H3->h3, &h3_null, sizeof (struct H3));
	memcpy (&H4->h4, &h4_null, sizeof (struct H4));
	if (H2->dbreadfirst () != 0)
	{
		Form.Show ();
		return;
	}
	H3->h3.hirarchie2 = H2->h2.hirarchie2;
	H3->sqlopen (cursor_h32);
	if (H3->sqlfetch (cursor_h32) != 0)
	{
		H3->dbreadfirst ();
		Form.Show ();
		return;
	}
	H4->h4.hirarchie3 = H3->h3.hirarchie3;
	H4->sqlopen (cursor_h43);
	H4->sqlfetch (cursor_h43);
	H4->dbreadfirst ();

	H1->h1.hirarchie1 = H2->h2.hirarchie1;
	H1->dbreadfirst ();
	Form.Show ();
}

void CHierarchieDataDlg::OnEnKillFocusH3 ()
{
	Form.Get ();
	_tcscpy (H3->h3.h3_bez,_T(""));
	_tcscpy (H3->h3.h3_bezkrz,_T(""));
	memcpy (&H4->h4, &h4_null, sizeof (struct H4));
	if (H3->dbreadfirst () != 0)
	{
		Form.Show ();
		return;
	}
	H4->h4.hirarchie3 = H3->h3.hirarchie3;
	H4->sqlopen (cursor_h43);
	H4->sqlfetch (cursor_h43);
	H4->dbreadfirst ();

	H2->h2.hirarchie2 = H3->h3.hirarchie2;
	if (H2->dbreadfirst () != 0)
	{
		Form.Show ();
		return;
	}
	H1->h1.hirarchie1 = H2->h2.hirarchie1;
	H1->dbreadfirst ();
	Form.Show ();
}

void CHierarchieDataDlg::OnEnKillFocusH4 ()
{
	Form.Get ();
	_tcscpy (H4->h4.h4_bez,_T(""));
	_tcscpy (H4->h4.h4_bezkrz,_T(""));
	if (H4->dbreadfirst () != 0)
	{
		Form.Show ();
		return;
	}
	H3->h3.hirarchie3 = H4->h4.hirarchie3;
	if (H3->dbreadfirst () != 0)
	{
		Form.Show ();
		return;
	}
	H2->h2.hirarchie2 = H3->h3.hirarchie2;
	if (H2->dbreadfirst () != 0)
	{
		Form.Show ();
		return;
	}
	H1->h1.hirarchie1 = H2->h2.hirarchie1;
	H1->dbreadfirst ();
	Form.Show ();
}
