#ifndef _RAB_STUFEK_DEF
#define _RAB_STUFEK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct RAB_STUFEK {
   short          mdn;
   short          fil;
   long           rab_nr_num;
   char           rab_nr[9];
   char           rab_bz_b[49];
   double         smt_rab;
   double         rech_rab;
   char           sa_rab[2];
   DATE_STRUCT    ae_dat;
   char           rab_typ[3];
   short          delstatus;
   char           rab_teil_smt[2];
   char           rab_me_einh[2];
   short          waehrung;
};
extern struct RAB_STUFEK rab_stufek, rab_stufek_null;

class RAB_STUFEK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               RAB_STUFEK rab_stufek;
               RAB_STUFEK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
