#pragma once
#include "afxwin.h"
#include "NumEdit.h"

class CDecimalEdit :
	public CNumEdit
{
	DECLARE_DYNAMIC(CDecimalEdit)
public:
	int scale;
	CDecimalEdit(void);
	CDecimalEdit(int scale);
	~CDecimalEdit(void);
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnKillFocus (CWnd *);
};
