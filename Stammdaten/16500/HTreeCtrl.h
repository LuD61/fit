#pragma once
#include "afxcmn.h"
#include "H1.h"
#include "H2.h"
#include "H3.h"
#include "H4.h"

class CHTreeCtrl :
	public CTreeCtrl
{
public:
	H1_CLASS *H1;
	H2_CLASS *H2;
	H3_CLASS *H3;
	H4_CLASS *H4;

	int H1Cursor;
	int H2Cursor;
	int H3Cursor;
	int H4Cursor;

	HTREEITEM HItem;
	CHTreeCtrl(void);
	~CHTreeCtrl(void);
	void Init ();
    HTREEITEM AddRootItem (LPSTR, int, int);
    HTREEITEM AddChildItem (HTREEITEM, LPSTR, int, int, int , DWORD);
    void SetTreeStyle (DWORD);
	BOOL Read ();
	BOOL ReadTree ();
	void ReadHTree (HTREEITEM ChildItem);
	void ReadH2Tree (HTREEITEM ChildItem);
	void ReadH3Tree (HTREEITEM ChildItem);
	void ReadH4Tree (HTREEITEM ChildItem);
};
