#include "StdAfx.h"
#include "HTreectrl.h"
#include <vector>

class HTreeEntry
{
public :
	int hierarchie;
    HTREEITEM Item;
	HTreeEntry (int hierarchie, HTREEITEM Item)
	{
		this->hierarchie = hierarchie;
		this->Item = Item;
	}
};

CHTreeCtrl::CHTreeCtrl(void)
{
	H1 = NULL;
	H2 = NULL;
	H3 = NULL;
	H4 = NULL;
	H1Cursor = -1;
	H2Cursor = -1;
	H3Cursor = -1;
	H4Cursor = -1;
}

CHTreeCtrl::~CHTreeCtrl(void)
{ 
	if (H1 != NULL && H1Cursor != -1)
	{
		H1->sqlclose (H1Cursor);
	}
	if (H2 != NULL && H2Cursor != -1)
	{
		H2->sqlclose (H2Cursor);
	}
	if (H3 != NULL && H3Cursor != -1)
	{
		H3->sqlclose (H3Cursor);
	}
	if (H4 != NULL && H4Cursor != -1)
	{
		H4->sqlclose (H4Cursor);
	}
}

void CHTreeCtrl::Init ()
{
	if (H1 == NULL || H2 == NULL || H3 == NULL || H4 == NULL)
	{
		return;
	}

	H1->sqlout ((long *) &H1->h1.hirarchie1, SQLLONG, 0); 
	H1->sqlout ((char *) &H1->h1.h1_bezkrz, SQLCHAR, sizeof (H1->h1.h1_bezkrz)); 
	H1Cursor = H1->sqlcursor (_T("select hirarchie1, h1_bezkrz from h1 order by 1"));

	H2->sqlin  ((long *) &H2->h2.hirarchie1, SQLLONG, 0); 
	H2->sqlout ((long *) &H2->h2.hirarchie2, SQLLONG, 0); 
	H2->sqlout ((char *) &H2->h2.h2_bezkrz, SQLCHAR, sizeof (H2->h2.h2_bezkrz)); 
	H2Cursor = H2->sqlcursor (_T("select hirarchie2, h2_bezkrz from h2 where hirarchie1 = ? order by 1"));

	H3->sqlin  ((long *) &H3->h3.hirarchie2, SQLLONG, 0); 
	H3->sqlout ((long *) &H3->h3.hirarchie3, SQLLONG, 0); 
	H3->sqlout ((char *) &H3->h3.h3_bezkrz, SQLCHAR, sizeof (H3->h3.h3_bezkrz)); 
	H3Cursor = H3->sqlcursor (_T("select hirarchie3, h3_bezkrz from h3 where hirarchie2 = ? order by 1"));

	H4->sqlin  ((long *) &H4->h4.hirarchie3, SQLLONG, 0); 
	H4->sqlout ((long *) &H4->h4.hirarchie4, SQLLONG, 0); 
	H4->sqlout ((char *) &H4->h4.h4_bezkrz, SQLCHAR, sizeof (H4->h4.h4_bezkrz)); 
	H4Cursor = H4->sqlcursor (_T("select hirarchie4, h4_bezkrz from h4 where hirarchie3 = ? order by 1"));
}

HTREEITEM CHTreeCtrl::AddRootItem (LPSTR Text, int idx, int Childs)
{
   TV_INSERTSTRUCT TvItem;

   TvItem.hParent     = TVI_ROOT;
   TvItem.hInsertAfter = TVI_LAST;

   TvItem.item.mask = TVIF_CHILDREN |  TVIF_CHILDREN | TVIF_HANDLE |
                      TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE |
                      TVIF_STATE | TVIF_TEXT;
   TvItem.item.hItem = NULL;
   TvItem.item.state      = TVIS_STATEIMAGEMASK | TVIS_EXPANDED; 
   TvItem.item.stateMask  = TVIS_STATEIMAGEMASK | TVIS_EXPANDED; 
   TvItem.item.pszText    = Text;
   TvItem.item.cchTextMax = (int) strlen (Text);      
   TvItem.item.iImage     = 0;          
   TvItem.item.iSelectedImage = 0;  
   TvItem.item.cChildren = Childs;       
   TvItem.item.lParam     = (LPARAM) idx;

   return InsertItem (&TvItem);
}

HTREEITEM CHTreeCtrl::AddChildItem (HTREEITEM hParent,  LPSTR Text, int idx, int Childs, 
                                   int iImage, DWORD state)
{
   TV_INSERTSTRUCT TvItem;

   TvItem.hParent     = hParent;
   TvItem.hInsertAfter = TVI_LAST;

   TvItem.item.mask = TVIF_CHILDREN | TVIF_HANDLE |
                      TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE |
                      TVIF_STATE | TVIF_TEXT;
   TvItem.item.hItem = NULL;
   TvItem.item.state      = state; 
   TvItem.item.stateMask  = state; 
   TvItem.item.pszText    = Text;
   TvItem.item.cchTextMax = (int) strlen (Text);      
   TvItem.item.iImage     = iImage;
   TvItem.item.iSelectedImage = iImage;  
   TvItem.item.cChildren = Childs;       
   TvItem.item.lParam     = (LPARAM) idx;

   return InsertItem (&TvItem);
}

void CHTreeCtrl::SetTreeStyle (DWORD style)
{
    DWORD oldStyle = GetWindowLong (m_hWnd, GWL_STYLE);

    SetWindowLong (m_hWnd, GWL_STYLE, oldStyle | style);
}

BOOL CHTreeCtrl::Read ()
{
	HTREEITEM Item = GetSelectedItem ();
	DeleteAllItems ();
    SetTreeStyle (TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS);

	ReadTree ();
	if (Item != NULL)
	{
		SelectItem (Item);
	}
    return TRUE;
}

BOOL CHTreeCtrl::ReadTree ()
{
	std::vector<HTreeEntry *> HTab;
	CString Text;
	H1->sqlopen (H1Cursor);
	while (H1->sqlfetch (H1Cursor) == 0)
	{
		Text.Format (_T("%ld %s"), H1->h1.hirarchie1, H1->h1.h1_bezkrz);
	    HTREEITEM Item = AddRootItem (Text.GetBuffer (), 0, 5);
		HTreeEntry *ete = new HTreeEntry (H1->h1.hirarchie1, Item);
		HTab.push_back (ete);
	}
	for (std::vector<HTreeEntry *>::iterator e = HTab.begin (); e != HTab.end (); ++e)
	{
		HTreeEntry *ete = *e;
		H2->h2.hirarchie1 = ete->hierarchie;
		ReadH2Tree (ete->Item);
		delete ete;
	}
	HTab.clear ();
	return TRUE;
}

void CHTreeCtrl::ReadH2Tree (HTREEITEM HItem)
{
	std::vector<HTreeEntry *> HTab;
	CString Text;
	H2->sqlopen (H2Cursor);
	while (H2->sqlfetch (H2Cursor) == 0)
	{
		Text.Format (_T("%ld %s"), H2->h2.hirarchie2, H2->h2.h2_bezkrz);
		HTREEITEM Item = AddChildItem (HItem,  Text.GetBuffer (), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED);
		HTreeEntry *ete = new HTreeEntry (H2->h2.hirarchie2, Item);
		HTab.push_back (ete);
	}
	for (std::vector<HTreeEntry *>::iterator e = HTab.begin (); e != HTab.end (); ++e)
	{
		HTreeEntry *ete = *e;
		H3->h3.hirarchie2 = ete->hierarchie;
		ReadH3Tree (ete->Item);
		delete ete;
	}
	HTab.clear ();
}

void CHTreeCtrl::ReadH3Tree (HTREEITEM HItem)
{
	std::vector<HTreeEntry *> HTab;
	CString Text;
	H3->sqlopen (H3Cursor);
	while (H3->sqlfetch (H3Cursor) == 0)
	{
		Text.Format (_T("%ld %s"), H3->h3.hirarchie3, H3->h3.h3_bezkrz);
		HTREEITEM Item = AddChildItem (HItem,  Text.GetBuffer (), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED);
		HTreeEntry *ete = new HTreeEntry (H3->h3.hirarchie3, Item);
		HTab.push_back (ete);
	}
	for (std::vector<HTreeEntry *>::iterator e = HTab.begin (); e != HTab.end (); ++e)
	{
		HTreeEntry *ete = *e;
		H4->h4.hirarchie3 = ete->hierarchie;
		ReadH4Tree (ete->Item);
		delete ete;
	}
	HTab.clear ();
}

void CHTreeCtrl::ReadH4Tree (HTREEITEM HItem)
{
	CString Text;
	H4->sqlopen (H4Cursor);
	while (H4->sqlfetch (H4Cursor) == 0)
	{
		Text.Format (_T("%ld %s"), H4->h4.hirarchie4, H4->h4.h4_bezkrz);
		HTREEITEM Item = AddChildItem (HItem,  Text.GetBuffer (), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED);
	}
}
