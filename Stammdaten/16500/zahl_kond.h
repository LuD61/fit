#ifndef _ZAHL_KOND_DEF
#define _ZAHL_KOND_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct ZAHL_KOND {
   double         skto1;
   double         skto2;
   double         skto3;
   char           txt[61];
   short          zahl_kond;
   short          ziel1;
   short          ziel2;
   short          ziel3;
   char           zuord[2];
   short          delstatus;
};
extern struct ZAHL_KOND zahl_kond, zahl_kond_null;

class ZAHL_KOND_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               ZAHL_KOND zahl_kond;  
               ZAHL_KOND_CLASS () : DB_CLASS ()
               {
               }
};
#endif
