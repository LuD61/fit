#include "stdafx.h"
#include "haus_bank.h"

struct HAUS_BANK haus_bank, haus_bank_null, haus_bank_def;

void HAUS_BANK_CLASS::prepare (void)
{
            TCHAR *sqltext;

 
            sqlin ((short *)   &haus_bank.mdn,  SQLSHORT, 0);           
	    sqlin ((long *)   &haus_bank.blz,  SQLLONG, 0);
            sqlin ((short *)   &haus_bank.bank,  SQLSHORT, 0);
    sqlout ((short *) &haus_bank.mdn,SQLSHORT,0);
    sqlout ((long *) &haus_bank.blz,SQLLONG,0);
    sqlout ((char *) haus_bank.kto,SQLCHAR,17);
    sqlout ((char *) haus_bank.bank_nam,SQLCHAR,37);
    sqlout ((char *) haus_bank.chk_einr,SQLCHAR,2);
    sqlout ((short *) &haus_bank.bank,SQLSHORT,0);
    sqlout ((short *) &haus_bank.delstatus,SQLSHORT,0);
            cursor = sqlcursor (_T("select haus_bank.mdn,  "
"haus_bank.blz,  haus_bank.kto,  haus_bank.bank_nam,  "
"haus_bank.chk_einr,  haus_bank.bank,  haus_bank.delstatus from haus_bank ")

					_T("where mdn = ? ")
					_T("and blz = ? ")                                  
					_T("and bank = ?"));
    sqlin ((short *) &haus_bank.mdn,SQLSHORT,0);
    sqlin ((long *) &haus_bank.blz,SQLLONG,0);
    sqlin ((char *) haus_bank.kto,SQLCHAR,17);
    sqlin ((char *) haus_bank.bank_nam,SQLCHAR,37);
    sqlin ((char *) haus_bank.chk_einr,SQLCHAR,2);
    sqlin ((short *) &haus_bank.bank,SQLSHORT,0);
    sqlin ((short *) &haus_bank.delstatus,SQLSHORT,0);
            sqltext = _T("update haus_bank set "
"haus_bank.mdn = ?,  haus_bank.blz = ?,  haus_bank.kto = ?,  "
"haus_bank.bank_nam = ?,  haus_bank.chk_einr = ?,  "
"haus_bank.bank = ?,  haus_bank.delstatus = ? ")

					_T("where mdn = ? ")
					_T("and blz = ? ")                                  
					_T("and bank = ?");
           
	    sqlin ((short *)   &haus_bank.mdn,  SQLSHORT, 0);           
	    sqlin ((long *)   &haus_bank.blz,  SQLLONG, 0);
            sqlin ((short *)   &haus_bank.bank,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

                                  _T("set delstatus = 0 where haus_bank = ? and delstatus = 0 for update");
            
	    sqlin ((short *)   &haus_bank.mdn,  SQLSHORT, 0);           
	    sqlin ((long *)   &haus_bank.blz,  SQLLONG, 0);
            sqlin ((short *)   &haus_bank.bank,  SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from haus_bank ")
					_T("where mdn = ? ")
					_T("and blz = ? ")                                  
					_T("and bank = ?"));
    sqlin ((short *) &haus_bank.mdn,SQLSHORT,0);
    sqlin ((long *) &haus_bank.blz,SQLLONG,0);
    sqlin ((char *) haus_bank.kto,SQLCHAR,17);
    sqlin ((char *) haus_bank.bank_nam,SQLCHAR,37);
    sqlin ((char *) haus_bank.chk_einr,SQLCHAR,2);
    sqlin ((short *) &haus_bank.bank,SQLSHORT,0);
    sqlin ((short *) &haus_bank.delstatus,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into haus_bank ("
"mdn,  blz,  kto,  bank_nam,  chk_einr,  bank,  delstatus) ")

                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?)"));

}
