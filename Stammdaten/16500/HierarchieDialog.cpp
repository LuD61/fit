// HierarchieDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "HierarchieDialog.h"


// CHierarchieDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CHierarchieDialog, CDialog)

CHierarchieDialog::CHierarchieDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CHierarchieDialog::IDD, pParent)
{

}

CHierarchieDialog::~CHierarchieDialog()
{
	H2.sqlclose (cursor_h21);
	H3.sqlclose (cursor_h31);
	H3.sqlclose (cursor_h32);
	H4.sqlclose (cursor_h41);
	H4.sqlclose (cursor_h42);
	H4.sqlclose (cursor_h43);
}

void CHierarchieDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CHierarchieDialog, CDialog)
	ON_WM_CTLCOLOR ()
	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CHierarchieDialog-Meldungshandler


BOOL CHierarchieDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	H2.sqlin ((long *) &H2.h2.hirarchie1, SQLLONG, 0);
	H2.sqlout ((long *) &H2.h2.hirarchie2, SQLLONG, 0);
	cursor_h21 = H2.sqlcursor (_T("select hirarchie2 from h2 where hirarchie1 = ?"));

	H3.sqlin ((long *) &H3.h3.hirarchie1, SQLLONG, 0);
	H3.sqlout ((long *) &H3.h3.hirarchie3, SQLLONG, 0);
	cursor_h31 = H3.sqlcursor (_T("select hirarchie3 from h3 where hirarchie1 = ?"));
	H4.sqlin ((long *) &H4.h4.hirarchie1, SQLLONG, 0);
	H4.sqlout ((long *) &H4.h4.hirarchie4, SQLLONG, 0);
	cursor_h41 = H4.sqlcursor (_T("select hirarchie4 from h4 where hirarchie1 = ?"));

	H3.sqlin ((long *) &H3.h3.hirarchie2, SQLLONG, 0);
	H3.sqlout ((long *) &H3.h3.hirarchie3, SQLLONG, 0);
	cursor_h32 = H3.sqlcursor (_T("select hirarchie3 from h3 where hirarchie2 = ?"));
	H4.sqlin ((long *) &H4.h4.hirarchie2, SQLLONG, 0);
	H4.sqlout ((long *) &H4.h4.hirarchie4, SQLLONG, 0);
	cursor_h42 = H4.sqlcursor (_T("select hirarchie4 from h4 where hirarchie2 = ?"));
	H4.sqlin ((long *) &H4.h4.hirarchie3, SQLLONG, 0);
	H4.sqlout ((long *) &H4.h4.hirarchie4, SQLLONG, 0);
	cursor_h43 = H4.sqlcursor (_T("select hirarchie4 from h4 where hirarchie3 = ?"));

	HierarchieTree.H1 = &H1;
	HierarchieTree.H2 = &H2;
	HierarchieTree.H3 = &H3;
	HierarchieTree.H4 = &H4;

	HierarchieTree.cursor_h21 = cursor_h21;
	HierarchieTree.cursor_h31 = cursor_h31;
	HierarchieTree.cursor_h41 = cursor_h41;
	HierarchieTree.cursor_h32 = cursor_h32;
	HierarchieTree.cursor_h42 = cursor_h42;
	HierarchieTree.cursor_h43 = cursor_h43;

	HierarchieDataDlg.H1 = &H1;
	HierarchieDataDlg.H2 = &H2;
	HierarchieDataDlg.H3 = &H3;
	HierarchieDataDlg.H4 = &H4;

	HierarchieDataDlg.cursor_h21 = cursor_h21;
	HierarchieDataDlg.cursor_h31 = cursor_h31;
	HierarchieDataDlg.cursor_h41 = cursor_h41;
	HierarchieDataDlg.cursor_h32 = cursor_h32;
	HierarchieDataDlg.cursor_h42 = cursor_h42;
	HierarchieDataDlg.cursor_h43 = cursor_h43;

	HierarchieTree.m_HierarchieTree.H1 = &H1;
	HierarchieTree.m_HierarchieTree.H2 = &H2;	
	HierarchieTree.m_HierarchieTree.H3 = &H3;
	HierarchieTree.m_HierarchieTree.H4 = &H4;
	HierarchieTree.Create (IDD_HIERARCHIE_TREE_DLG, this);
	HierarchieTree.ShowWindow (SW_SHOWNORMAL);
	HierarchieTree.UpdateWindow ();

	HierarchieDataDlg.Create (IDD_HIERARCHIE_DATA_DLG, this);
	HierarchieDataDlg.ShowWindow (SW_SHOWNORMAL);
	HierarchieDataDlg.UpdateWindow ();

	HierarchieTree.AddUpdateEvent (&HierarchieDataDlg);
	HierarchieDataDlg.AddUpdateEvent (&HierarchieTree);


	SplitPane.Create (this, 0, 0, &HierarchieTree, &HierarchieDataDlg, 30, CSplitPane::Horizontal);
	CRect rect;
	GetClientRect (&rect);
	SplitPane.SetLength (rect.bottom);
	int cy = rect.bottom;
	HierarchieTree.GetClientRect (&rect);
	HierarchieTree.MoveWindow (0, 0, rect.right, cy);

	return FALSE;
}

void CHierarchieDialog::OnSize (UINT nType, int cx, int cy)
{
	if (IsWindow (HierarchieTree.m_hWnd))
	{
		CRect rect;
		HierarchieTree.GetClientRect (&rect);
		HierarchieTree.MoveWindow (0, 0, rect.right, cy);
		SplitPane.SetLength (cy);

		HierarchieDataDlg.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
		rect.bottom = cy;
		HierarchieDataDlg.MoveWindow (&rect);
	}
}

HBRUSH CHierarchieDialog::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CHierarchieDialog::PreTranslateMessage(MSG* pMsg)
{

	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :

			if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					this->GetNextDlgTabItem (GetFocus ())->SetFocus ();
				}
				else
				{
					this->GetNextDlgTabItem (GetFocus (), 1)->SetFocus ();
				}
			}
			else if (pMsg->wParam == VK_RETURN)
			{
				this->GetNextDlgTabItem (GetFocus ())->SetFocus ();
			}
			else if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					Copy ();
				}
			}
			else if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					Paste ();
				}
			}
			if (pMsg->wParam == VK_F7)
			{
				HierarchieTree.DeleteRow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				HierarchieDataDlg.OnSave ();
				return TRUE;
			}
	}

	return FALSE;
}

void CHierarchieDialog::Copy ()
{
	CWnd *c = GetFocus ();
	if (c->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) c)->Copy ();
	}
}

void CHierarchieDialog::Paste ()
{
	CWnd *c = GetFocus ();
	if (c->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) c)->Paste ();
	}
}
