// KunPage4.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "KunPage4.h"
#include "DbPropertyPage.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "token.h"
#include "ptabn.h"
#include "gr_zuord.h"
#include "fil.h"
#include "rab_stufek.h"


// CKunPage4-Dialogfeld

IMPLEMENT_DYNAMIC(CKunPage4, CDbPropertyPage)

CKunPage4::CKunPage4()
	: CDbPropertyPage(CKunPage4::IDD)
{
	hBrush = NULL;
	hBrushStatic = NULL;
	Search = _T("");
	CUniFormField::Code = &Code;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Kun);
	flgKopfLs = FALSE;
	flgFussLs = FALSE;
	flgKopfRech = FALSE;
	flgFussRech = FALSE;


}

CKunPage4::~CKunPage4()
{
	
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
}

void CKunPage4::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_KUNNAME, m_KunName);
	DDX_Control(pDX, IDC_HEADBORDER, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDER, m_DataBorder);
	DDX_Control(pDX, IDC_DATABORDER2, m_DataBorder2);
	DDX_Control(pDX, IDC_DATABORDER3, m_DataBorder3);
	DDX_Control(pDX, IDC_LFORMLS, m_LFormLS);
	DDX_Control(pDX, IDC_FORMLS, m_FormLS);
	DDX_Control(pDX, IDC_LEERLS, m_LeerLS);
	DDX_Control(pDX, IDC_LLEERLS, m_LLeerLS);
	DDX_Control(pDX, IDC_SCHWUNDKZ, m_SchwundKZ);
	DDX_Control(pDX, IDC_LSCHWUND, m_LSchwund);
	DDX_Control(pDX, IDC_AUFLAGE1, m_Auflage1);
	DDX_Control(pDX, IDC_LAUFLAGE1, m_LAuflage1);
	DDX_Control(pDX, IDC_LFORMRECH, m_LFormRech);
	DDX_Control(pDX, IDC_FORMRECH, m_FormRech);
	DDX_Control(pDX, IDC_LRECHST, m_LRechSt);
	DDX_Control(pDX, IDC_RECHST, m_RechSt);
	DDX_Control(pDX, IDC_LRECHART, m_LRechArt);
	DDX_Control(pDX, IDC_RECHART, m_RechArt);
	DDX_Control(pDX, IDC_LEINZELAUSW, m_LEinzAusw);
	DDX_Control(pDX, IDC_EINZELAUSW, m_EinzAusw);
	DDX_Control(pDX, IDC_LAUFLAGE2, m_LAuflage2);
	DDX_Control(pDX, IDC_AUFLAGE2, m_Auflage2);
	DDX_Control(pDX, IDC_LPRAUSW, m_LPrAuswRech);
	DDX_Control(pDX, IDC_PRAUSW, m_PrAuswRech);
	DDX_Control(pDX, IDC_PRAUSW2, m_PrAuswLS);
	DDX_Control(pDX, IDC_LPRAUSW2, m_LPrAuswLS);
	DDX_Control(pDX, IDC_LKOPFLS, m_LKopfLS);
	DDX_Control(pDX, IDC_KOPFLS, m_KopfLS);
	DDX_Control(pDX, IDC_LFUSSLS, m_LFussLS);
	DDX_Control(pDX, IDC_FUSSLS, m_FussLS);
	DDX_Control(pDX, IDC_LKOPFRECH, m_LKopfRech);
	DDX_Control(pDX, IDC_KOPFRECH, m_KopfRech);
	DDX_Control(pDX, IDC_LFUSSRECH, m_LFussRech);
	DDX_Control(pDX, IDC_FUSSRECH, m_FussRech);
	DDX_Control(pDX, IDC_LFREITEXT, m_LFreitext);
	DDX_Control(pDX, IDC_FREITEXT, m_Freitext);
	DDX_Control(pDX, IDC_KOPFLSBEZ, m_KopfLSBez);
	DDX_Control(pDX, IDC_FUSSLSBEZ, m_FussLSBez);
	DDX_Control(pDX, IDC_KOPFRECHBEZ, m_KopfRechBez);
	DDX_Control(pDX, IDC_FUSSRECHBEZ, m_FussRechBez);
	DDX_Control(pDX, IDC_LKUN_BRAN, m_LKunBran);
	DDX_Control(pDX, IDC_KUN_BRAN, m_KunBran);
	DDX_Control(pDX, IDC_ETIKETTEN, m_Etiketten);
	DDX_Control(pDX, IDC_LETIKETTEN, m_LEtiketten);
	DDX_Control(pDX, IDC_ZOLLP, m_ZollP);
	DDX_Control(pDX, IDC_LZOLLP, m_LZollP);
	DDX_Control(pDX, IDC_LEMAILRECH, m_LEmailRech);
	DDX_Control(pDX, IDC_EMAILRECH, m_EMailRech);
	DDX_Control(pDX, IDC_LFREITEXT3, m_LFreitext3);
	DDX_Control(pDX, IDC_FREITEXT3, m_Freitext3);
	DDX_Control(pDX, IDC_LVERLADESCHEIN, m_LVerladeschein);
	DDX_Control(pDX, IDC_VERLADESCHEIN, m_Verladeschein);
}


BEGIN_MESSAGE_MAP(CKunPage4, CDbPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_KILLFOCUS ()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_BN_CLICKED(IDC_SCHWUNDKZ, &CKunPage4::OnBnClickedSchwundkz)
	ON_BN_CLICKED(IDC_EMAILRECH, &CKunPage4::OnBnClickedEMailRech)
	ON_EN_KILLFOCUS(IDC_KOPFLSBEZ, &CKunPage4::OnEnKillfocusKopflsbez)
	ON_EN_KILLFOCUS(IDC_FUSSLSBEZ, &CKunPage4::OnEnKillfocusFusslsbez)
	ON_EN_KILLFOCUS(IDC_KOPFRECHBEZ, &CKunPage4::OnEnKillfocusKopfrechbez)
	ON_EN_KILLFOCUS(IDC_FUSSRECHBEZ, &CKunPage4::OnEnKillfocusFussrechbez)
	ON_EN_CHANGE(IDC_KOPFLSBEZ, &CKunPage4::OnEnChangeKopflsbez)
	ON_EN_CHANGE(IDC_FUSSLSBEZ, &CKunPage4::OnEnChangeFusslsbez)
	ON_EN_CHANGE(IDC_KOPFRECHBEZ, &CKunPage4::OnEnChangeKopfrechbez)
	ON_EN_CHANGE(IDC_FUSSRECHBEZ, &CKunPage4::OnEnChangeFussrechbez)
	ON_BN_CLICKED(IDC_ZOLLP, &CKunPage4::OnBnClickedZollP)
END_MESSAGE_MAP()


// CKunPage4-Meldungshandler

void CKunPage4::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CKunPage4::OnInitDialog()
{
	CDialog::OnInitDialog();

	F5Pressed = FALSE;

	KunProperty = (CPropertySheet *) GetParent ();
	View = (DbFormView *) KunProperty->GetParent ();
	Frame = (CFrameWnd *) View->GetParent ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();

	CRect bRect (0, 0, 100, 20);;



	Form.Add (new CFormField (&m_Mdn,EDIT,        (short *)  &Mdn->mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *)   MdnAdr->adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Kun,EDIT,        (long *)   &Kun->kun.kun, VLONG));
	Form.Add (new CUniFormField (&m_KunName,EDIT, (char *)   Kun->kun.kun_krz1, VCHAR));
	Form.Add (new CFormField (&m_FormLS,COMBOBOX,    (char *)  Kun->kun.form_typ1, VCHAR));
	Form.Add (new CFormField (&m_LeerLS,COMBOBOX,    (short *)  &Kun->kun.kun_leer_kz, VSHORT));
	Form.Add (new CFormField (&m_SchwundKZ,CHECKBOX,    (char *)  Kun->kun.sw_rab, VCHAR));
	Form.Add (new CFormField (&m_Auflage1,EDIT,    (short *)  &Kun->kun.auflage1, VSHORT));
	Form.Add (new CFormField (&m_FormRech,COMBOBOX,    (char *)  Kun->kun.form_typ2, VCHAR));
	Form.Add (new CFormField (&m_RechSt,COMBOBOX,    (char *)  Kun->kun.rech_st, VCHAR));
	Form.Add (new CFormField (&m_RechArt,COMBOBOX,    (short *)  &Kun->kun.sam_rech, VSHORT));
	Form.Add (new CFormField (&m_EinzAusw,COMBOBOX,    (short *)  &Kun->kun.einz_ausw, VSHORT));
	Form.Add (new CFormField (&m_Auflage2,EDIT,    (short *)  &Kun->kun.auflage2, VSHORT));
	Form.Add (new CFormField (&m_PrAuswRech,COMBOBOX,    (short *)  &Kun->kun.pr_ausw_re, VSHORT));
	Form.Add (new CFormField (&m_ZollP,CHECKBOX,    (char *)  &Kun->kun.cmr, VLONG));
	Form.Add (new CFormField (&m_PrAuswLS,COMBOBOX,    (short *)  &Kun->kun.pr_ausw_ls, VSHORT));
	Form.Add (new CFormField (&m_KopfLS,EDIT,    (long *)  &Kun->kun.ls_kopf_txt, VLONG));
//	Form.Add (new CUniFormField (&m_KopfLSBez,EDIT,    (char *)  &Ls_txt->ls_txt.txt, VCHAR));
	Form.Add (new CFormField (&m_FussLS,EDIT,    (long *)  &Kun->kun.ls_fuss_txt, VLONG));
	Form.Add (new CFormField (&m_KopfRech,EDIT,    (long *)  &Kun->kun.rech_kopf_txt, VLONG));
	Form.Add (new CFormField (&m_FussRech,EDIT,    (long *)  &Kun->kun.rech_fuss_txt, VLONG));
	Form.Add (new CFormField (&m_Freitext,EDIT,    (char *)  Kun->kun.frei_txt2, VCHAR, sizeof (Kun->kun.frei_txt2) - 1));
	Form.Add (new CFormField (&m_Freitext3,EDIT,    (char *)  Kun->kun.frei_txt3, VCHAR, sizeof (Kun->kun.frei_txt3) - 1));
	Form.Add (new CFormField (&m_KunBran,COMBOBOX,    (char *) Kun->kun.kun_bran, VCHAR));	// 060912
	Form.Add (new CFormField (&m_Etiketten,COMBOBOX,    (short *)  &Kun->kun.etikett, VSHORT));
	Form.Add (new CFormField (&m_EMailRech,CHECKBOX,    (char *)  Kun->kun.email_rech, VCHAR));
	Form.Add (new CFormField (&m_Verladeschein,COMBOBOX,    (short *)  &Kun_erw->kun_erw.verladeschein, VSHORT));  //LOH-35

	Cfg.SetProgName (_T("16500")); 
	LPTSTR trenner = "$$";
	if (Cfg.GetCfgValue (_T("Freitext"), PROG_CFG::cfg_v, trenner) == TRUE)
    {
		    m_LFreitext.SetWindowTextA(PROG_CFG::cfg_v);
    }
	if (Cfg.GetCfgValue (_T("Freitext2"), PROG_CFG::cfg_v, trenner) == TRUE)
    {
		    m_LFreitext3.SetWindowTextA(PROG_CFG::cfg_v);
    }


	FillPtabCombo (&m_Verladeschein, _T("verladeschein"));
	
	FillPtabCombo (&m_FormLS, _T("form_typ1"));
	
	FillPtabCombo (&m_LeerLS, _T("kun_leer_kz"));

	FillPtabCombo (&m_Etiketten, _T("etikett"));

	strcpy (sys_par.sys_par_nam, "vbd_par");
	if (Sys_par->dbreadfirst () == 0)
	{
		if (!(sys_par.sys_par_wrt[0] == '1'))
		{
			FillPtabCombo3 (&m_FormRech, _T("form_typ2"));
		}
		else
		{
			FillPtabCombo (&m_FormRech, _T("form_typ2"));
		}

	}
	else
	{
			FillPtabCombo (&m_FormRech, _T("form_typ2"));
	}
	
	strcpy (sys_par.sys_par_nam, "zahl_ziel_par");
	if (Sys_par->dbreadfirst () == 0)
	{
		if (!(sys_par.sys_par_wrt[0] == '1'))
		{
			FillPtabCombo2 (&m_RechSt, _T("rech_st"));
		}
		else
		{
			FillPtabCombo (&m_RechSt, _T("rech_st"));
		}

	}
	FillPtabCombo (&m_RechArt, _T("sam_rech"));
	
	FillPtabCombo (&m_EinzAusw, _T("einz_ausw"));
	
	FillPtabCombo (&m_PrAuswLS, _T("pr_ausw_ls"));
	
	FillPtabCombo (&m_PrAuswRech, _T("pr_ausw_re"));

	FillPtabCombo (&m_KunBran, _T("kun_bran"));
	
	FillLSKopf();
	FillLSFuss();
	FillRechKopf();
	FillRechFuss();

	

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 1, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	

	KunGrid.Create (this, 1, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	

	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 4);
    c_HeadBorder->rightspace = 20; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 3, 1, 4, 1);
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LKun = new CCtrlInfo (&m_LKun, 1, 2, 1, 1);
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid = new CCtrlInfo (&KunGrid, 2, 2, 1, 1);
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 3, 2, 4, 1);
	CtrlGrid.Add (c_KunName);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 4, 3, DOCKBOTTOM);
	c_DataBorder->rightspace = 20;
	CtrlGrid.Add (c_DataBorder);

	CCtrlInfo *c_DataBorder2 = new CCtrlInfo (&m_DataBorder2, 3, 4, DOCKRIGHT, 10);
    c_DataBorder2->rightspace = 20;
	CtrlGrid.Add (c_DataBorder2);

	CCtrlInfo *c_DataBorder3 = new CCtrlInfo (&m_DataBorder3, 3, 14, DOCKRIGHT, 4);
    c_DataBorder3->rightspace = 24;
	CtrlGrid.Add (c_DataBorder3);

	CCtrlInfo *c_LKopfLS = new CCtrlInfo (&m_LKopfLS, 1, 5, 1, 1);
	CtrlGrid.Add (c_LKopfLS);
	CCtrlInfo *c_KopfLS = new CCtrlInfo (&m_KopfLS, 2, 5, 1, 1);
	CtrlGrid.Add (c_KopfLS);

	
	CCtrlInfo *c_KopfLSBez = new CCtrlInfo (&m_KopfLSBez, 2, 6, 1, 1);
	CtrlGrid.Add (c_KopfLSBez);

	CCtrlInfo *c_LFussLS = new CCtrlInfo (&m_LFussLS, 1, 9, 1, 1);
	CtrlGrid.Add (c_LFussLS);
	CCtrlInfo *c_FussLS = new CCtrlInfo (&m_FussLS, 2, 9, 1, 1);
	CtrlGrid.Add (c_FussLS);

	
	CCtrlInfo *c_FussLSBez = new CCtrlInfo (&m_FussLSBez, 2, 10, 1, 1);
	CtrlGrid.Add (c_FussLSBez);

	CCtrlInfo *c_LKopfRech = new CCtrlInfo (&m_LKopfRech, 1, 13, 1, 1);
	CtrlGrid.Add (c_LKopfRech);
	CCtrlInfo *c_KopfRech = new CCtrlInfo (&m_KopfRech, 2, 13, 1, 1);
	CtrlGrid.Add (c_KopfRech);

	
	CCtrlInfo *c_KopfRechBez = new CCtrlInfo (&m_KopfRechBez, 2, 14, 1, 1);
	CtrlGrid.Add (c_KopfRechBez);

	CCtrlInfo *c_LFussRech = new CCtrlInfo (&m_LFussRech, 1, 17, 1, 1);
	CtrlGrid.Add (c_LFussRech);
	CCtrlInfo *c_FussRech = new CCtrlInfo (&m_FussRech, 2, 17, 1, 1);
	CtrlGrid.Add (c_FussRech);

	
	CCtrlInfo *c_FussRechBez = new CCtrlInfo (&m_FussRechBez, 2, 18, 1, 1);
	CtrlGrid.Add (c_FussRechBez);

	CCtrlInfo *c_LFreitext = new CCtrlInfo (&m_LFreitext, 1, 21, 1, 1);
	CtrlGrid.Add (c_LFreitext);
	CCtrlInfo *c_Freitext = new CCtrlInfo (&m_Freitext, 2, 21, 1, 1);
	CtrlGrid.Add (c_Freitext);
	
	CCtrlInfo *c_LFreitext3 = new CCtrlInfo (&m_LFreitext3, 1, 24, 1, 1);
	CtrlGrid.Add (c_LFreitext3);
	CCtrlInfo *c_Freitext3 = new CCtrlInfo (&m_Freitext3, 2, 24, 1, 1);
	CtrlGrid.Add (c_Freitext3);
	
	
	CCtrlInfo *c_LFormLS = new CCtrlInfo (&m_LFormLS, 4, 5, 1, 1);
	CtrlGrid.Add (c_LFormLS);
	CCtrlInfo *c_FormLS = new CCtrlInfo (&m_FormLS, 5, 5, 1, 1);
	c_FormLS->rightspace = 40;
	CtrlGrid.Add (c_FormLS);

	
	CCtrlInfo *c_LLeerLS = new CCtrlInfo (&m_LLeerLS, 4, 7, 1, 1);
	CtrlGrid.Add (c_LLeerLS);
	CCtrlInfo *c_LeerLS = new CCtrlInfo (&m_LeerLS, 5, 7, 1, 1);
	c_LeerLS->rightspace = 40;
	CtrlGrid.Add (c_LeerLS);

	CCtrlInfo *c_LPrAuswLS = new CCtrlInfo (&m_LPrAuswLS, 4, 8, 1, 1);
	CtrlGrid.Add (c_LPrAuswLS);
	CCtrlInfo *c_PrAuswLS = new CCtrlInfo (&m_PrAuswLS, 5, 8, 1, 1);
	c_PrAuswLS->rightspace = 40;
	CtrlGrid.Add (c_PrAuswLS);

	
	CCtrlInfo *c_LAuflage1 = new CCtrlInfo (&m_LAuflage1, 4, 9, 1, 1);
	CtrlGrid.Add (c_LAuflage1);
	CCtrlInfo *c_Auflage1 = new CCtrlInfo (&m_Auflage1, 5, 9, 1, 1);
	c_Auflage1->rightspace = 40;
	CtrlGrid.Add (c_Auflage1);

	CCtrlInfo *c_LKunBran = new CCtrlInfo (&m_LKunBran, 4, 10, 1, 1);
	CtrlGrid.Add (c_LKunBran);
	CCtrlInfo *c_KunBran = new CCtrlInfo (&m_KunBran, 5, 10, 1, 1);
	c_KunBran->rightspace = 40;
	CtrlGrid.Add (c_KunBran);

	CCtrlInfo *c_LSchwund = new CCtrlInfo (&m_LSchwund, 4, 11, 1, 1);
	CtrlGrid.Add (c_LSchwund);
	CCtrlInfo *c_SchwundKZ = new CCtrlInfo (&m_SchwundKZ, 5, 11, 3, 1);
	CtrlGrid.Add (c_SchwundKZ);

	CCtrlInfo *c_LVerladeschein = new CCtrlInfo (&m_LVerladeschein, 4, 12, 1, 1);
	CtrlGrid.Add (c_LVerladeschein);
	CCtrlInfo *c_Verladeschein = new CCtrlInfo (&m_Verladeschein, 5, 12, 1, 1);
	c_Verladeschein->rightspace = 40;
	CtrlGrid.Add (c_Verladeschein);

	CCtrlInfo *c_LFormRech = new CCtrlInfo (&m_LFormRech, 4, 15, 1, 1);
	CtrlGrid.Add (c_LFormRech);
	CCtrlInfo *c_FormRech = new CCtrlInfo (&m_FormRech, 5, 15, 1, 1);
	c_FormRech->rightspace = 40;
	CtrlGrid.Add (c_FormRech);

	CCtrlInfo *c_LRechSt = new CCtrlInfo (&m_LRechSt, 4, 16, 1, 1);
	CtrlGrid.Add (c_LRechSt);
	CCtrlInfo *c_RechSt = new CCtrlInfo (&m_RechSt, 5, 16, 1, 1);
	c_RechSt->rightspace = 40;
	CtrlGrid.Add (c_RechSt);

	CCtrlInfo *c_LEmailRech = new CCtrlInfo (&m_LEmailRech, 4, 17, 1, 1);
	CtrlGrid.Add (c_LEmailRech);
	CCtrlInfo *c_EMailRech = new CCtrlInfo (&m_EMailRech, 5, 17, 1, 1);
	c_EMailRech->rightspace = 40;
	CtrlGrid.Add (c_EMailRech);

	CCtrlInfo *c_LRechArt = new CCtrlInfo (&m_LRechArt, 4, 18, 1, 1);
	CtrlGrid.Add (c_LRechArt);
	CCtrlInfo *c_RechArt = new CCtrlInfo (&m_RechArt, 5, 18, 1, 1);
	c_RechArt->rightspace = 40;
	CtrlGrid.Add (c_RechArt);

	CCtrlInfo *c_LEinzAusw = new CCtrlInfo (&m_LEinzAusw, 4, 19, 1, 1);
	CtrlGrid.Add (c_LEinzAusw);
	CCtrlInfo *c_EinzAusw = new CCtrlInfo (&m_EinzAusw, 5, 19, 1, 1);
	c_EinzAusw->rightspace = 40;
	CtrlGrid.Add (c_EinzAusw);

	
	CCtrlInfo *c_LPrAuswRech = new CCtrlInfo (&m_LPrAuswRech, 4, 20, 1, 1);
	CtrlGrid.Add (c_LPrAuswRech);
	CCtrlInfo *c_PrAuswRech = new CCtrlInfo (&m_PrAuswRech, 5, 20, 1, 1);
	c_PrAuswRech->rightspace = 40;
	CtrlGrid.Add (c_PrAuswRech);

	CCtrlInfo *c_LZollP = new CCtrlInfo (&m_LZollP, 4, 21, 1, 1);
	CtrlGrid.Add (c_LZollP);
	CCtrlInfo *c_ZollP = new CCtrlInfo (&m_ZollP, 5, 21, 3, 1);
	CtrlGrid.Add (c_ZollP);

	CCtrlInfo *c_LAuflage2 = new CCtrlInfo (&m_LAuflage2, 4, 22, 1, 1);
	CtrlGrid.Add (c_LAuflage2);
	CCtrlInfo *c_Auflage2 = new CCtrlInfo (&m_Auflage2, 5, 22, 1, 1);
	c_Auflage2->rightspace = 40;
	CtrlGrid.Add (c_Auflage2);

	CCtrlInfo *c_LEtiketten = new CCtrlInfo (&m_LEtiketten, 4, 24, 1, 1);
	CtrlGrid.Add (c_LEtiketten);
	CCtrlInfo *c_Etiketten = new CCtrlInfo (&m_Etiketten, 5, 24, 1, 1);
	c_Etiketten->rightspace = 40;
	CtrlGrid.Add (c_Etiketten);


	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	CtrlGrid.Display ();
    CtrlGrid.SetItemCharHeight (&m_HeadBorder, 3);
    EnableFields (TRUE);


	CString kz2 = Kun->kun.sw_rab;
	if (kz2 == _T("J"))
	{
		m_SchwundKZ.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_SchwundKZ.SetWindowTextA("   d e a k t i v i e r t");
	}

	if (Kun->kun.cmr == 1 )
	{
		m_ZollP.SetWindowTextA(" Zollpapiere erstellen");
	}
	else
	{
		m_ZollP.SetWindowTextA(" keine Zollpapiere");
	}

	CString email_rech = Kun->kun.email_rech;
	if (email_rech == _T("J"))
	{
		m_EMailRech.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_EMailRech.SetWindowTextA("   d e a k t i v i e r t");
	}


    Form.Show ();
	return TRUE;
}


BOOL CKunPage4::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return FALSE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				Update->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Form.Get();
				Update->Write();
				return TRUE;
			}

			

			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}
			if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

HBRUSH CKunPage4::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);

    if (hBrush == NULL)
	{
		  hBrush = CreateSolidBrush (Color);
		  hBrushStatic = CreateSolidBrush (Color);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			return hBrush;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CButton )))
	{
            pDC->SetBkColor (Color);
			pDC->SetBkMode (TRANSPARENT);
			return hBrushStatic;
	}
	return CPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CKunPage4::OnReturn ()
{
	CWnd *Control = GetFocus ();
	if (Control == &m_KopfLSBez) return FALSE;
	if (Control == &m_FussLSBez) return FALSE;
	if (Control == &m_KopfRechBez) return FALSE;
	if (Control == &m_FussRechBez) return FALSE;
	if (Control == &m_Freitext) return FALSE; //WAL-154
	if (Control == &m_Freitext3) return FALSE; //WAL-154


	if (Control == &m_KopfLS)
	{
		Form.Get();
		FillLSKopf();
		m_FussLS.SetFocus();
		Form.Show();
		return FALSE;
		
	}

	if (Control == &m_FussLS)
	{
		Form.Get();
		FillLSFuss();
		m_KopfRech.SetFocus();
		Form.Show();
		return FALSE;
		
	}

	if (Control == &m_KopfRech)
	{
		Form.Get();
		FillRechKopf();
		m_FussRech.SetFocus();
		Form.Show();
		return FALSE;
		
	}

	if (Control == &m_FussRech)
	{
		Form.Get();
		FillRechFuss();
		m_Freitext.SetFocus();
		Form.Show();
		return FALSE;
		
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CKunPage4::OnKeyup ()
{

	CWnd *Control = GetFocus ();
	if (Control == &m_KopfLSBez) return FALSE;
	if (Control == &m_FussLSBez) return FALSE;
	if (Control == &m_KopfRechBez) return FALSE;
	if (Control == &m_FussRechBez) return FALSE;

	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}


void CKunPage4::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CKunPage4::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}

void CKunPage4::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


	if (m_Mdn.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein SDatz zum L�schen selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	if (MessageBox (_T("Preisauszeichnerdaten l�schen"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		Kun->beginwork ();
		Kun->dbdelete ();
		Kun->commitwork ();
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		EnableFields (FALSE);
		Form.Show ();
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);
	}
}

void CKunPage4::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
//		Control->SetReadOnly (!enable);
        Control->EnableWindow (enable);
	}
}

BOOL CKunPage4::StepBack ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
					Frame->DestroyWindow ();
					return FALSE;
	}
	else
	{
		EnableFields (FALSE);
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);	
	}
	return TRUE;
}


void CKunPage4::OnCopy ()
{
    OnEditCopy ();
}  



void CKunPage4::OnPaste ()
{
	OnEditPaste ();
}

void CKunPage4::OnCopyEx ()
{

//	if (!m_A.IsWindowEnabled ())
	{
		Copy ();
		return;
	}
}

void CKunPage4::OnPasteEx ()
{

//	if (!m_A.IsWindowEnabled () && Paste ())
	{
		Kun->CopyData (&kun);
		Kun_erw->CopyData (&kun_erw);
		Form.Show ();
/*
		if (KunPageEx != NULL && IsWindow (KunPageEx->m_hWnd))
		{
			KunPageEx->Form.Show ();
		}
*/
		return;
	}
}

BOOL CKunPage4::Copy ()
{
	Form.Get ();
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return FALSE;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      int err = GetLastError ();
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return FALSE;  
    }

	try
	{
        UINT CF_A_KUN_GX = RegisterClipboardFormat (_T("kun")); 
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, sizeof (KUN)); 
        char *p = (char *) GlobalLock(hglbCopy);
        memcpy (p, &Kun->kun, sizeof (Kun->kun));
        GlobalUnlock(hglbCopy); 
		HANDLE cData;
		cData = ::SetClipboardData( CF_A_KUN_GX, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
    return TRUE;
}

BOOL CKunPage4::Paste ()
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return FALSE;
    }

    HGLOBAL hglbCopy;
    UINT CF_A_KUN_GX;
    _TCHAR name [256];
    CF_A_KUN_GX = EnumClipboardFormats (0);
	if (CF_A_KUN_GX == 0)
	{
		int err = GetLastError ();
		CloseClipboard();
		return FALSE;
	}

    int ret;
    while ((ret = GetClipboardFormatName (CF_A_KUN_GX, name, sizeof (name))) != 0)
    { 
         CString Name = name;
         if (Name == _T("a_kun_gx"))
         {
              break;
         }  
		 CF_A_KUN_GX = EnumClipboardFormats (CF_A_KUN_GX);
    }
    if (ret == 0)
    {
         CloseClipboard();
         return FALSE;
    }    

    try
    {	
  	     hglbCopy =  ::GetClipboardData(CF_A_KUN_GX);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
 	     LPSTR p = (LPSTR) GlobalLock ((HGLOBAL) hglbCopy);
         memcpy ( (LPSTR) &kun, p, sizeof (kun));
  	     GlobalUnlock ((HGLOBAL) hglbCopy);
    }
    catch (...) {};
    CloseClipboard();
    return TRUE; 
}

void CKunPage4::EnableFields (BOOL b)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (!b)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);

}


void CKunPage4::OnKillFocus (CWnd *newFocus)
{
	CWnd *Control = GetFocus ();
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		if (f->Scale > 0)
		{
			f->Show ();
		}
	}
}


void CKunPage4::FillPtabCombo (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem)); 
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert)); 
		Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0); 

		
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn->sqlcursor (Sql.GetBuffer ());
		while (Ptabn->sqlfetch (cursor) == 0)
		{
			Ptabn->dbreadfirst();

			LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn->ptabn.ptwert,
				                             Ptabn->ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}



void CKunPage4::OnBnClickedSchwundkz()
{
	CString kz2;
	Form.Get();
	
	kz2 = Kun->kun.sw_rab;
	if (kz2 == _T("J"))
	{
		m_SchwundKZ.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_SchwundKZ.SetWindowTextA("   d e a k t i v i e r t");
	}
}



void CKunPage4::OnBnClickedEMailRech()
{
	CString email_rech;
	Form.Get();
	
	email_rech = Kun->kun.email_rech;
	if (email_rech == _T("J"))
	{
		m_EMailRech.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_EMailRech.SetWindowTextA("   d e a k t i v i e r t");
	}
}


void CKunPage4::FillPtabCombo2 (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem)); 
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert)); 
		Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0); 

		
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" and ptwert <> 5 ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn->sqlcursor (Sql.GetBuffer ());
		while (Ptabn->sqlfetch (cursor) == 0)
		{
			Ptabn->dbreadfirst();

			LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn->ptabn.ptwert,
				                             Ptabn->ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}













void CKunPage4::FillPtabCombo3 (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem)); 
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert)); 
		Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0); 

		
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" and ptwert between 0 and 9 ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn->sqlcursor (Sql.GetBuffer ());
		while (Ptabn->sqlfetch (cursor) == 0)
		{
			Ptabn->dbreadfirst();

			LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn->ptabn.ptwert,
				                             Ptabn->ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();

		
	}
}













void CKunPage4::FillLSKopf ()
{
	m_KopfLSBez.SetWindowTextA("");
	CFormField *f = Form.GetFormField (&m_KopfLSBez);

	
		Ls_txt->sqlin ((long *) &Kun->kun.ls_kopf_txt , SQLLONG, 0);
		Ls_txt->sqlout ((long *) &Ls_txt->ls_txt.zei, SQLLONG, 0);
		Ls_txt->sqlout ((char *) Ls_txt->ls_txt.txt, SQLCHAR, 61);
				
		int cursor = Ls_txt->sqlcursor (_T("select zei, txt from ls_txt ")
			        _T("where ls_txt.nr = ? ")
				    _T("order by zei"));
		

		CString test;
		CString test2; 

		Ls_txt->sqlopen(cursor);
		
		while (Ls_txt->sqlfetch (cursor) == 0)
		{
			
			m_KopfLSBez.GetWindowTextA(test2);
			test = Ls_txt->ls_txt.txt;
			test.TrimRight();
			test2 += test + "\r\n";
			m_KopfLSBez.SetWindowTextA(test2);
		
		}
		Ls_txt->sqlclose (cursor);
}

void CKunPage4::OnEnKillfocusKopflsbez()
{
	CString CText;
	CString CText1;
	LPTSTR p;

	if (flgKopfLs == FALSE) return; 
	flgKopfLs = FALSE;
	if (MessageBox (_T("Text speichern ?"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{

		Form.Get();
		m_KopfLSBez.GetWindowTextA(CText);
		Ls_txt->sqlin ((long *) &Kun->kun.ls_kopf_txt , SQLLONG, 0);
		int dsqlstatus = 0;
		dsqlstatus = Ls_txt->sqlcomm (_T("delete from ls_txt ")
			        _T("where ls_txt.nr = ? "));

		if (dsqlstatus == 0 || dsqlstatus == 100)
		{
			int zei = 0;
			size_t len = 0;
			CToken t (CText, "\r\n");
			while ((p = t.NextToken ()) != NULL)
			{	
				zei ++;
				CString CText1 = p;
				len = strlen(CText1.GetBuffer());
			    Ls_txt->ls_txt.nr = Kun->kun.ls_kopf_txt ;
			    Ls_txt->ls_txt.zei = zei;
				if (len > 60)
				{
					len = 58; 
					strncpy(Ls_txt->ls_txt.txt,CText1.GetBuffer(),len);
					Ls_txt->ls_txt.txt[len+1] = 0;
				}
				else
				{
					strcpy(Ls_txt->ls_txt.txt,CText1.GetBuffer());
				}

				Ls_txt->sqlin ((long *) &Ls_txt->ls_txt.nr,SQLLONG,0);
				Ls_txt->sqlin ((long *) &Ls_txt->ls_txt.zei,SQLLONG,0);
				Ls_txt->sqlin ((char *) Ls_txt->ls_txt.txt,SQLCHAR,61);
				Ls_txt->sqlcomm  (_T("insert into ls_txt ("
								"nr,  zei,  txt) ")
                                      _T("values ")
                                      _T("(?,?,?)"));
			}
		}
	}
}



void CKunPage4::FillLSFuss ()
{
	m_FussLSBez.SetWindowTextA("");
	CFormField *f = Form.GetFormField (&m_FussLSBez);
	
		Ls_txt->sqlin ((long *) &Kun->kun.ls_fuss_txt , SQLLONG, 0);
		Ls_txt->sqlout ((long *) &Ls_txt->ls_txt.zei, SQLLONG, 0);
		Ls_txt->sqlout ((char *) Ls_txt->ls_txt.txt, SQLCHAR, 61);
				
		int cursor = Ls_txt->sqlcursor (_T("select zei, txt from ls_txt ")
			        _T("where ls_txt.nr = ? ")
				    _T("order by zei"));
		

		CString test;
		CString test2;

		int dsqlstatus = Ls_txt->sqlopen(cursor);
		dsqlstatus = Ls_txt->sqlfetch (cursor);
		while (dsqlstatus == 0)
		{
			m_FussLSBez.GetWindowTextA(test2);
			test = Ls_txt->ls_txt.txt;
			test.TrimRight();
			test2 += test + "\r\n";
			m_FussLSBez.SetWindowTextA(test2);
			dsqlstatus = Ls_txt->sqlfetch (cursor);		
		
		}
		Ls_txt->sqlclose (cursor);
}


void CKunPage4::FillRechKopf ()
{
	m_KopfRechBez.SetWindowTextA("");
	CFormField *f = Form.GetFormField (&m_KopfRechBez);
	
		Ls_txt->sqlin ((long *) &Kun->kun.rech_kopf_txt , SQLLONG, 0);
		Ls_txt->sqlout ((long *) &Ls_txt->ls_txt.zei, SQLLONG, 0);
		Ls_txt->sqlout ((char *) Ls_txt->ls_txt.txt, SQLCHAR, sizeof (Ls_txt->ls_txt.txt));
				
		int cursor = Ls_txt->sqlcursor (_T("select zei, txt from ls_txt ")
			        _T("where ls_txt.nr = ? ")
				    _T("order by zei"));
		

		CString test;
		CString test2;

		Ls_txt->sqlopen(cursor);
		
		while (Ls_txt->sqlfetch (cursor) == 0)
		{
			
			m_KopfRechBez.GetWindowTextA(test2);
			test = Ls_txt->ls_txt.txt;
			test.TrimRight();
			test2 += test + "\r\n";
			m_KopfRechBez.SetWindowTextA(test2);
		
		}
		Ls_txt->sqlclose (cursor);
}


void CKunPage4::FillRechFuss ()
{
	m_FussRechBez.SetWindowTextA("");
	CFormField *f = Form.GetFormField (&m_FussRechBez);
	
		Ls_txt->sqlin ((long *) &Kun->kun.rech_fuss_txt , SQLLONG, 0);
		Ls_txt->sqlout ((long *) &Ls_txt->ls_txt.zei, SQLLONG, 0);
		Ls_txt->sqlout ((char *) Ls_txt->ls_txt.txt, SQLCHAR, 61);
				
		int cursor = Ls_txt->sqlcursor (_T("select zei, txt from ls_txt ")
			        _T("where ls_txt.nr = ? ")
				    _T("order by zei"));
		

		CString test;
		CString test2;

		Ls_txt->sqlopen(cursor);
		
		while (Ls_txt->sqlfetch (cursor) == 0)
		{
			
			m_FussRechBez.GetWindowTextA(test2);
			test = Ls_txt->ls_txt.txt;
			test.TrimRight();
			test2 += test + "\r\n";
			m_FussRechBez.SetWindowTextA(test2);

		
		}
		Ls_txt->sqlclose (cursor);
}

BOOL CKunPage4::OnKillActive ()
{
	Form.Get ();
	if (F5Pressed)
	{
		return TRUE;
	}
	FillLSKopf();
	return TRUE;
}

BOOL CKunPage4::OnSetActive ()
{
    F5Pressed = FALSE;	
	Form.Show ();
	return TRUE;
}

void CKunPage4::UpdatePage ()
{
	Form.Show ();
}


void CKunPage4::OnEnKillfocusFusslsbez()
{
	CString CText;
	CString CText1;
	LPTSTR p;

	if (flgFussLs == FALSE) return; 
	flgFussLs = FALSE;
	if (MessageBox (_T("Text speichern ?"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{

		Form.Get();
		m_FussLSBez.GetWindowTextA(CText);
		Ls_txt->sqlin ((long *) &Kun->kun.ls_fuss_txt , SQLLONG, 0);
		int dsqlstatus = 0;
		dsqlstatus = Ls_txt->sqlcomm (_T("delete from ls_txt ")
			        _T("where ls_txt.nr = ? "));

		if (dsqlstatus == 0 || dsqlstatus == 100)
		{
			int zei = 0;
			size_t len = 0;
			CToken t (CText, "\r\n");
			while ((p = t.NextToken ()) != NULL)
			{	
				zei ++;
				CString CText1 = p;
				len = strlen(CText1.GetBuffer());
			    Ls_txt->ls_txt.nr = Kun->kun.ls_fuss_txt ;
			    Ls_txt->ls_txt.zei = zei;
				if (len > 60)
				{
					len = 58; 
					strncpy(Ls_txt->ls_txt.txt,CText1.GetBuffer(),len);
					Ls_txt->ls_txt.txt[len+1] = 0;
				}
				else
				{
					strcpy(Ls_txt->ls_txt.txt,CText1.GetBuffer());
				}

				Ls_txt->sqlin ((long *) &Ls_txt->ls_txt.nr,SQLLONG,0);
				Ls_txt->sqlin ((long *) &Ls_txt->ls_txt.zei,SQLLONG,0);
				Ls_txt->sqlin ((char *) Ls_txt->ls_txt.txt,SQLCHAR,61);
				Ls_txt->sqlcomm  (_T("insert into ls_txt ("
								"nr,  zei,  txt) ")
                                      _T("values ")
                                      _T("(?,?,?)"));
			}
		}
	}
}

void CKunPage4::OnEnKillfocusKopfrechbez()
{
	CString CText;
	CString CText1;
	LPTSTR p;
	if (flgKopfRech == FALSE) return; 
	flgKopfRech = FALSE;

	if (MessageBox (_T("Text speichern ?"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{

		Form.Get();
		m_KopfRechBez.GetWindowTextA(CText);
		Ls_txt->sqlin ((long *) &Kun->kun.rech_kopf_txt , SQLLONG, 0);
		int dsqlstatus = 0;
		dsqlstatus = Ls_txt->sqlcomm (_T("delete from ls_txt ")
			        _T("where ls_txt.nr = ? "));

		if (dsqlstatus == 0 || dsqlstatus == 100)
		{
			int zei = 0;
			size_t len = 0;
			CToken t (CText, "\r\n");
			while ((p = t.NextToken ()) != NULL)
			{	
				zei ++;
				CString CText1 = p;
				len = strlen(CText1.GetBuffer());
			    Ls_txt->ls_txt.nr = Kun->kun.rech_kopf_txt ;
			    Ls_txt->ls_txt.zei = zei;
				if (len > 60)
				{
					len = 58; 
					strncpy(Ls_txt->ls_txt.txt,CText1.GetBuffer(),len);
					Ls_txt->ls_txt.txt[len+1] = 0;
				}
				else
				{
					strcpy(Ls_txt->ls_txt.txt,CText1.GetBuffer());
				}

				Ls_txt->sqlin ((long *) &Ls_txt->ls_txt.nr,SQLLONG,0);
				Ls_txt->sqlin ((long *) &Ls_txt->ls_txt.zei,SQLLONG,0);
				Ls_txt->sqlin ((char *) Ls_txt->ls_txt.txt,SQLCHAR,61);
				Ls_txt->sqlcomm  (_T("insert into ls_txt ("
								"nr,  zei,  txt) ")
                                      _T("values ")
                                      _T("(?,?,?)"));
			}
		}
	}
}

void CKunPage4::OnEnKillfocusFussrechbez()
{
	CString CText;
	CString CText1;
	LPTSTR p;
	if (flgFussRech == FALSE) return; 
	flgFussRech = FALSE;

	if (MessageBox (_T("Text speichern ?"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		Form.Get();
		m_FussRechBez.GetWindowTextA(CText);
		Ls_txt->sqlin ((long *) &Kun->kun.rech_fuss_txt , SQLLONG, 0);
		int dsqlstatus = 0;
		dsqlstatus = Ls_txt->sqlcomm (_T("delete from ls_txt ")
			        _T("where ls_txt.nr = ? "));

		if (dsqlstatus == 0 || dsqlstatus == 100)
		{
			int zei = 0;
			size_t len = 0;
			CToken t (CText, "\r\n");
			while ((p = t.NextToken ()) != NULL)
			{	
				zei ++;
				CString CText1 = p;
				len = strlen(CText1.GetBuffer());
			    Ls_txt->ls_txt.nr = Kun->kun.rech_fuss_txt ;
			    Ls_txt->ls_txt.zei = zei;
				if (len > 60)
				{
					len = 58; 
					strncpy(Ls_txt->ls_txt.txt,CText1.GetBuffer(),len);
					Ls_txt->ls_txt.txt[len+1] = 0;
				}
				else
				{
					strcpy(Ls_txt->ls_txt.txt,CText1.GetBuffer());
				}

				Ls_txt->sqlin ((long *) &Ls_txt->ls_txt.nr,SQLLONG,0);
				Ls_txt->sqlin ((long *) &Ls_txt->ls_txt.zei,SQLLONG,0);
				Ls_txt->sqlin ((char *) Ls_txt->ls_txt.txt,SQLCHAR,61);
				Ls_txt->sqlcomm  (_T("insert into ls_txt ("
								"nr,  zei,  txt) ")
                                      _T("values ")
                                      _T("(?,?,?)"));
			}
		}
	}
}

void CKunPage4::OnEnChangeKopflsbez()
{
	flgKopfLs = TRUE;
}

void CKunPage4::OnEnChangeFusslsbez()
{
	flgFussLs = TRUE;
}

void CKunPage4::OnEnChangeKopfrechbez()
{
	flgKopfRech = TRUE;
}

void CKunPage4::OnEnChangeFussrechbez()
{
	flgFussRech = TRUE;
}

void CKunPage4::OnBnClickedZollP()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
		long lcmr;
	Form.Get();
	
	lcmr = Kun->kun.cmr;
	if (lcmr == 1)
	{
		m_ZollP.SetWindowTextA(" Zollpapiere erstellen");
	}
	else
	{
		m_ZollP.SetWindowTextA(" keine Zollpapiere");
	}
}




