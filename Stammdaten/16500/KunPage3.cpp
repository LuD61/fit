// KunPage3.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "KunPage3.h"
#include "DbPropertyPage.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "token.h"
#include "ptabn.h"
#include "gr_zuord.h"
#include "fil.h"
#include "rab_stufek.h"


// CKunPage3-Dialogfeld

IMPLEMENT_DYNAMIC(CKunPage3, CDbPropertyPage)

CKunPage3::CKunPage3()
	: CDbPropertyPage(CKunPage3::IDD)
{
	hBrush = NULL;
	hBrushStatic = NULL;
	ChoiceAdr = NULL;
	ModalChoiceAdr = TRUE;
	Search = _T("");

	CUniFormField::Code = &Code;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Kun);
	IncludeLand = TRUE;
}

CKunPage3::~CKunPage3()
{
	
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
}

void CKunPage3::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_KUNNAME, m_KunName);
	DDX_Control(pDX, IDC_HEADBORDER, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDER, m_DataBorder);
	DDX_Control(pDX, IDC_LANR, m_LAnr);
	DDX_Control(pDX, IDC_ANR, m_Anr);
	DDX_Control(pDX, IDC_LADR1, m_LAdr1);
	DDX_Control(pDX, IDC_ADR, m_Adr1);
	DDX_Control(pDX, IDC_LNAME1, m_LName1);
	DDX_Control(pDX, IDC_NAME, m_Name1);
	DDX_Control(pDX, IDC_NAME2, m_Name2);
	DDX_Control(pDX, IDC_LNAME2, m_LName2);
	DDX_Control(pDX, IDC_NAME3, m_Name3);
	DDX_Control(pDX, IDC_LNAME3, m_LName3);
	DDX_Control(pDX, IDC_STRASSE, m_Strasse);
	DDX_Control(pDX, IDC_LSTRASSE, m_LStrasse);
	DDX_Control(pDX, IDC_PLZ, m_Plz);
	DDX_Control(pDX, IDC_LPLZ, m_LPlz);
	DDX_Control(pDX, IDC_ORT, m_Ort);
	DDX_Control(pDX, IDC_LORT, m_LOrt);
	DDX_Control(pDX, IDC_LPOSTFACH, m_LPostfach);
	DDX_Control(pDX, IDC_POSTFACH, m_Postfach);
	DDX_Control(pDX, IDC_TELEFON, m_Telefon);
	DDX_Control(pDX, IDC_LTELEFON, m_LTelefon);
	DDX_Control(pDX, IDC_FAX, m_Fax);
	DDX_Control(pDX, IDC_POSTFACH2, m_plzPostfach);
	DDX_Control(pDX, IDC_EMAIL, m_EMAIL);
	DDX_Control(pDX, IDC_LEMAIL, m_LEmail);
	DDX_Control(pDX, IDC_PARTNER, m_Partner);
	DDX_Control(pDX, IDC_LPARTNER, m_LPartner);
	DDX_Control(pDX, IDC_STAAT, m_Staat);
	DDX_Control(pDX, IDC_LSTAAT, m_LStaat);
	DDX_Control(pDX, IDC_LAND, m_Land);
	Form.Add (new CFormField (&m_IncludeLand,CHECKBOX,    (short *)  &IncludeLand, VSHORT));
	DDX_Control(pDX, IDC_LLAND, m_LLand);
	DDX_Control(pDX, IDC_INCLUDE_LAND, m_IncludeLand);
	DDX_Control(pDX, IDC_LKUNKRZ1, m_LKunKrz1);
	DDX_Control(pDX, IDC_KUNKRZ1, m_KunKrz1);
	DDX_Control(pDX, IDC_DATABORDER2, m_DataBorder2);
	DDX_Control(pDX, IDC_Bonitaet, m_Bonitaet);
	DDX_Control(pDX, IDC_LBonitaet, m_LBonitaet);
	DDX_Control(pDX, IDC_DEBIKTO, m_DebiKto);
	DDX_Control(pDX, IDC_LDEBIKTO, m_LDebiKto);
	DDX_Control(pDX, IDC_MAHN, m_Mahn);
	DDX_Control(pDX, IDC_LMAHN, m_LMahn);
	DDX_Control(pDX, IDC_KREDVERS, m_KredVers);
	DDX_Control(pDX, IDC_LKREDVERS, m_LKredVers);
	DDX_Control(pDX, IDC_KREDLIM, m_KredLim);
	DDX_Control(pDX, IDC_LKREDLIM, m_LKredLim);
	DDX_Control(pDX, IDC_ZAHLART, m_ZahlArt);
	DDX_Control(pDX, IDC_LZAHLART, m_LZahlArt);
	DDX_Control(pDX, IDC_ZAHLZIEL, m_ZahlZiel);
	DDX_Control(pDX, IDC_LZAHLZIEL, m_LZahlZiel);
	DDX_Control(pDX, IDC_GUT, m_Gut);
	DDX_Control(pDX, IDC_LGUT, m_LGut);
	DDX_Control(pDX, IDC_NRBEIRECH, m_NrbeiRech);
	DDX_Control(pDX, IDC_LNRBEIRECH, m_LNrbeiRech);
	DDX_Control(pDX, IDC_RABSCHL, m_RabSchl);
	DDX_Control(pDX, IDC_LRABSCHL, m_LRabSchl);
	DDX_Control(pDX, IDC_LKONDKUN, m_LKondKun);
	DDX_Control(pDX, IDC_KONDKUN, m_KondKun);
	DDX_Control(pDX, IDC_RECHILN, m_RechILN);
	DDX_Control(pDX, IDC_LRECHILN, m_LRechLN);
	DDX_Control(pDX, IDC_PALBEW, m_Pal_bew);
	DDX_Control(pDX, IDC_TRAKOBER, m_Trakober);
	DDX_Control(pDX, IDC_LPfandVer, m_LPfandVer);
	DDX_Control(pDX, IDC_PfandVer, m_PfandVer);
	DDX_Control(pDX, IDC_PARTNER3, m_Partner3);
	DDX_Control(pDX, IDC_LPARTNER3, m_LPartner3);
	DDX_Control(pDX, IDC_LMODEM2, m_LModem2);
	DDX_Control(pDX, IDC_MODEM2, m_Modem2);
}


BEGIN_MESSAGE_MAP(CKunPage3, CPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_KILLFOCUS ()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_BN_CLICKED(IDC_ADR3CHOICE , OnAdr3Choice)
	ON_BN_CLICKED(IDC_PLZ3CHOICE , OnPlz3Choice)
	ON_COMMAND (SELECTED, OnAdrSelected)
	ON_COMMAND (CANCELED, OnAdrCanceled)
	ON_BN_CLICKED(IDC_Bonitaet, &CKunPage3::OnBnClickedBonitaet)
	ON_EN_CHANGE(IDC_KUNKRZ1, &CKunPage3::OnEnChangeKunkrz1)
	ON_BN_CLICKED(IDC_PALBEW, &CKunPage3::OnBnClickedPalbew)
	ON_BN_CLICKED(IDC_TRAKOBER, &CKunPage3::OnBnClickedTrakober)
END_MESSAGE_MAP()


// CKunPage2-Meldungshandler

void CKunPage3::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CKunPage3::OnInitDialog()
{
	CDialog::OnInitDialog();

	F5Pressed = FALSE;
	/*Cfg.SetProgName (_T("16500"));
	if (Cfg.GetCfgValue (_T("UseOdbc"), PROG_CFG::cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("ModalChoice"), PROG_CFG::cfg_v) == TRUE)
    {
			ModalChoice = _tstoi (PROG_CFG::cfg_v);
    }
	Cfg.CloseCfg ();
*/

	KunProperty = (CPropertySheet *) GetParent ();
	View = (DbFormView *) KunProperty->GetParent ();
	Frame = (CFrameWnd *) View->GetParent ();

	//Kun.opendbase (_T("bws"));
    //memcpy (&Mdn->mdn, &mdn_null, sizeof (MDN));
    //memcpy (&MdnAdr->adr, &adr_null, sizeof (ADR));
    //memcpy (&Kun->kun, &kun_null, sizeof (KUN));
    //memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();

	CRect bRect (0, 0, 100, 20);;

	

	Form.Add (new CFormField (&m_Mdn,EDIT,        (short *)  &Mdn->mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *)   MdnAdr->adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Kun,EDIT,        (long *)   &Kun->kun.kun, VLONG));
	Form.Add (new CUniFormField (&m_KunName,EDIT, (char *)   Kun->kun.kun_krz3, VCHAR));
	Form.Add (new CFormField (&m_Adr1,EDIT,        (long *)  &Kun->kun.adr3, VLONG));
	Form.Add (new CFormField (&m_Anr,COMBOBOX,    (short *)  &RechAdr->adr.anr, VSHORT));
	Form.Add (new CFormField (&m_Name1,EDIT,      (char *)  RechAdr->adr.adr_nam1, VCHAR));
	Form.Add (new CFormField (&m_Name2,EDIT,      (char *)  RechAdr->adr.adr_nam2, VCHAR));
	Form.Add (new CFormField (&m_Name3,EDIT,      (char *)  RechAdr->adr.adr_nam3, VCHAR));
	Form.Add (new CFormField (&m_Strasse,EDIT,      (char *)  RechAdr->adr.str, VCHAR));
	Form.Add (new CFormField (&m_Postfach,EDIT,      (char *)  RechAdr->adr.pf, VCHAR));
	Form.Add (new CFormField (&m_plzPostfach,EDIT,      (char *)  RechAdr->adr.plz_pf, VCHAR));
	Form.Add (new CFormField (&m_Plz,EDIT,      (char *)  RechAdr->adr.plz, VCHAR));
	Form.Add (new CFormField (&m_Ort,EDIT,      (char *)  RechAdr->adr.ort1, VCHAR));
	Form.Add (new CFormField (&m_Telefon,EDIT,      (char *)  RechAdr->adr.tel, VCHAR));
	Form.Add (new CFormField (&m_Fax,EDIT,      (char *)  RechAdr->adr.fax, VCHAR));
	Form.Add (new CFormField (&m_EMAIL,EDIT,      (char *)  RechAdr->adr.email, VCHAR));
	Form.Add (new CFormField (&m_Modem2,EDIT,      (char *)  RechAdr->adr.mobil, VCHAR));
	Form.Add (new CFormField (&m_Partner,EDIT,      (char *)  RechAdr->adr.partner, VCHAR));
	Form.Add (new CFormField (&m_Partner3,EDIT,      (char *)  RechAdr->adr.spartner, VCHAR));
	Form.Add (new CFormField (&m_Staat,COMBOBOX,    (short *)  &RechAdr->adr.staat, VSHORT));
	Form.Add (new CFormField (&m_Land,COMBOBOX,    (short *)  &RechAdr->adr.land, VSHORT));
	Form.Add (new CFormField (&m_KunKrz1,EDIT,      (char *)  Kun->kun.kun_krz3 , VCHAR));
	Form.Add (new CFormField (&m_Pal_bew,CHECKBOX,      (long *)  &Kun->kun.pal_bew , VLONG));
	Form.Add (new CFormField (&m_Trakober,CHECKBOX,      (long *)  &Kun->kun.trakober , VLONG));
	Form.Add (new CFormField (&m_Bonitaet,CHECKBOX,      (short *)  &Kun->kun.bonitaet , VSHORT));
	Form.Add (new CFormField (&m_DebiKto,EDIT,      (char *)  Kun->kun.deb_kto , VCHAR));
	Form.Add (new CFormField (&m_Mahn,COMBOBOX,      (short *)  &Kun->kun.kun_of_po , VSHORT));
	Form.Add (new CFormField (&m_KredVers,COMBOBOX,      (short *)  &Kun->kun.kred_vers , VSHORT));
	Form.Add (new CFormField (&m_KredLim,EDIT,      (double *)  &Kun->kun.kred_lim , VDOUBLE));
	Form.Add (new CFormField (&m_ZahlArt,COMBOBOX,      (short *)  &Kun->kun.zahl_art , VSHORT));
	Form.Add (new CFormField (&m_ZahlZiel,COMBOBOX,      (short *)  &Kun->kun.zahl_ziel , VSHORT));
	Form.Add (new CFormField (&m_NrbeiRech,EDIT,      (char *)  Kun->kun.nr_bei_rech , VCHAR));
	Form.Add (new CFormField (&m_Gut,COMBOBOX,      (short *)  &Kun->kun.gut , VSHORT));
	Form.Add (new CFormField (&m_RabSchl,COMBOBOX,      (short *)  Kun->kun.rab_schl , VCHAR));	// 060912
	Form.Add (new CFormField (&m_KondKun,EDIT,      (long *)  &Kun->kun.kond_kun , VLONG));
	Form.Add (new CFormField (&m_RechILN,EDIT,      (char *)  Kun->kun.rechiln , VCHAR));
	Form.Add (new CFormField (&m_PfandVer,CHECKBOX,    (short *)  &Kun->kun.pfand_ber, VSHORT));


	FillPtabCombo (&m_Anr, _T("anr"));
	FillPtabCombo (&m_Staat, _T("staat"));
	FillPtabCombo (&m_Land, _T("land"));
	FillPtabCombo (&m_Mahn, _T("kun_of_po"));
	FillPtabCombo (&m_KredVers, _T("kred_vers"));
	FillPtabCombo (&m_ZahlArt, _T("zahl_art"));
	FillZahlZiel ();
	FillPtabCombo (&m_Gut, _T("gut"));
	FillRabSchl();


	
	
	

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 1, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	

	KunGrid.Create (this, 1, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	

	Adr1Grid.Create (this, 1, 2);
    Adr1Grid.SetBorder (0, 0);
    Adr1Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_Adr1 = new CCtrlInfo (&m_Adr1, 0, 0, 1, 1);
	Adr1Grid.Add (c_Adr1);
	CtrlGrid.CreateChoiceButton (m_Adr1Choice, IDC_ADR3CHOICE, this);
	CCtrlInfo *c_Adr1Choice = new CCtrlInfo (&m_Adr1Choice, 1, 0, 1, 1);
	Adr1Grid.Add (c_Adr1Choice);

	Plz1Grid.Create (this, 1, 3);
    Plz1Grid.SetBorder (0, 0);
    Plz1Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_Plz = new CCtrlInfo (&m_Plz, 0, 0, 1, 1);
	Plz1Grid.Add (c_Plz);
	CtrlGrid.CreateChoiceButton (m_Plz3Choice, IDC_PLZ3CHOICE, this);
	CCtrlInfo *c_Plz3Choice = new CCtrlInfo (&m_Plz3Choice, 1, 0, 1, 1);
	Plz1Grid.Add (c_Plz3Choice);
	CCtrlInfo *c_IncludeLand = new CCtrlInfo (&m_IncludeLand, 2, 0, 1, 1);
	c_IncludeLand->SetCellPos (5, 0);
	Plz1Grid.Add (c_IncludeLand);
	
	TelFaxGrid.Create (this, 1, 2);
    TelFaxGrid.SetBorder (0, 0);
    TelFaxGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Telefon = new CCtrlInfo (&m_Telefon, 0, 0, 1, 1);
	TelFaxGrid.Add (c_Telefon);
	CCtrlInfo *c_Fax = new CCtrlInfo (&m_Fax, 1, 0, 1, 1);
	TelFaxGrid.Add (c_Fax);

	PLZPostfachGrid.Create (this, 1, 2);
    PLZPostfachGrid.SetBorder (0, 0);
    PLZPostfachGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Postfach = new CCtrlInfo (&m_Postfach, 0, 0, 1, 1);
	PLZPostfachGrid.Add (c_Postfach);
	CCtrlInfo *c_plzPostfach = new CCtrlInfo (&m_plzPostfach, 1, 0, 1, 1);
	PLZPostfachGrid.Add (c_plzPostfach);

	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 4);
    c_HeadBorder->rightspace = 20; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 3, 1, 4, 1);
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LKun = new CCtrlInfo (&m_LKun, 1, 2, 1, 1);
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid = new CCtrlInfo (&KunGrid, 2, 2, 1, 1);
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 3, 2, 4, 1);
	CtrlGrid.Add (c_KunName);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 4, 3, DOCKBOTTOM);
	CtrlGrid.Add (c_DataBorder);

	CCtrlInfo *c_DataBorder2 = new CCtrlInfo (&m_DataBorder2, 3, 4, DOCKRIGHT, DOCKBOTTOM);
    c_DataBorder2->rightspace = 20;
	CtrlGrid.Add (c_DataBorder2);


	CCtrlInfo *c_LAdr1 = new CCtrlInfo (&m_LAdr1, 1, 5, 1, 1);
	CtrlGrid.Add (c_LAdr1);
	CCtrlInfo *c_Adr1Grid = new CCtrlInfo (&Adr1Grid, 2, 5, 1, 1);
	CtrlGrid.Add (c_Adr1Grid);
	CCtrlInfo *c_LAnr = new CCtrlInfo (&m_LAnr, 1, 6, 1, 1);
	CtrlGrid.Add (c_LAnr);
	CCtrlInfo *c_Anr = new CCtrlInfo (&m_Anr, 2, 6, 1, 1);
	CtrlGrid.Add (c_Anr);
	CCtrlInfo *c_LName1 = new CCtrlInfo (&m_LName1, 1, 7, 1, 1);
	CtrlGrid.Add (c_LName1);
	CCtrlInfo *c_Name1 = new CCtrlInfo (&m_Name1, 2, 7, 1, 1);
	CtrlGrid.Add (c_Name1);

	CCtrlInfo *c_LName2 = new CCtrlInfo (&m_LName2, 1, 8, 1, 1);
	CtrlGrid.Add (c_LName2);
	CCtrlInfo *c_Name2 = new CCtrlInfo (&m_Name2, 2, 8, 1, 1);
	CtrlGrid.Add (c_Name2);

	CCtrlInfo *c_LName3 = new CCtrlInfo (&m_LName3, 1, 9, 1, 1);
	CtrlGrid.Add (c_LName3);
	CCtrlInfo *c_Name3 = new CCtrlInfo (&m_Name3, 2, 9, 1, 1);
	CtrlGrid.Add (c_Name3);

	CCtrlInfo *c_LStrasse = new CCtrlInfo (&m_LStrasse, 1, 10, 1, 1);
	CtrlGrid.Add (c_LStrasse);
	CCtrlInfo *c_Strasse = new CCtrlInfo (&m_Strasse, 2, 10, 1, 1);
	CtrlGrid.Add (c_Strasse);

	CCtrlInfo *c_LPlz = new CCtrlInfo (&m_LPlz, 1, 12, 1, 1);
	CtrlGrid.Add (c_LPlz);
	CCtrlInfo *c_Plz1Grid = new CCtrlInfo (&Plz1Grid, 2, 12, 1, 1);
	CtrlGrid.Add (c_Plz1Grid);
//	CCtrlInfo *c_Plz = new CCtrlInfo (&m_Plz, 2, 12, 1, 1);
//	CtrlGrid.Add (c_Plz);
	
	CCtrlInfo *c_LPostfach = new CCtrlInfo (&m_LPostfach, 1, 11, 1, 1);
	CtrlGrid.Add (c_LPostfach);
	CCtrlInfo *c_PLZPostfachGrid = new CCtrlInfo (&PLZPostfachGrid, 2, 11, 1, 1);
	CtrlGrid.Add (c_PLZPostfachGrid);

	CCtrlInfo *c_LOrt = new CCtrlInfo (&m_LOrt, 1, 13, 1, 1);
	CtrlGrid.Add (c_LOrt);
	CCtrlInfo *c_Ort = new CCtrlInfo (&m_Ort, 2, 13, 1, 1);
	CtrlGrid.Add (c_Ort);

	CCtrlInfo *c_LTelefon = new CCtrlInfo (&m_LTelefon, 1, 15, 1, 1);
	CtrlGrid.Add (c_LTelefon);
	CCtrlInfo *c_TelFaxGrid = new CCtrlInfo (&TelFaxGrid, 2, 15, 1, 1);
	CtrlGrid.Add (c_TelFaxGrid);
	
	CCtrlInfo *c_LEmail = new CCtrlInfo (&m_LEmail, 1, 16, 1, 1);
	CtrlGrid.Add (c_LEmail);
	CCtrlInfo *c_EMAIL = new CCtrlInfo (&m_EMAIL, 2, 16, 1, 1);
	CtrlGrid.Add (c_EMAIL);

	CCtrlInfo *c_LModem = new CCtrlInfo (&m_LModem2, 1, 17, 1, 1);
	CtrlGrid.Add (c_LModem);
	CCtrlInfo *c_Modem = new CCtrlInfo (&m_Modem2, 2, 17, 1, 1);
	CtrlGrid.Add (c_Modem);

	CCtrlInfo *c_LPartner = new CCtrlInfo (&m_LPartner, 1, 18, 1, 1);
	CtrlGrid.Add (c_LPartner);
	CCtrlInfo *c_Partner = new CCtrlInfo (&m_Partner, 2, 18, 1, 1);
	CtrlGrid.Add (c_Partner);

	CCtrlInfo *c_LPartner3 = new CCtrlInfo (&m_LPartner3, 1, 19, 1, 1);
	CtrlGrid.Add (c_LPartner3);
	CCtrlInfo *c_Partner3 = new CCtrlInfo (&m_Partner3, 2, 19, 1, 1);
	CtrlGrid.Add (c_Partner3);

	CCtrlInfo *c_LStaat = new CCtrlInfo (&m_LStaat, 1, 20, 1, 1);
	CtrlGrid.Add (c_LStaat);
	CCtrlInfo *c_Staat = new CCtrlInfo (&m_Staat, 2, 20, 1, 1);
	CtrlGrid.Add (c_Staat);

	CCtrlInfo *c_LLand = new CCtrlInfo (&m_LLand, 1, 21, 1, 1);
	CtrlGrid.Add (c_LLand);
	CCtrlInfo *c_Land = new CCtrlInfo (&m_Land, 2, 21, 1, 1);
	CtrlGrid.Add (c_Land);


	CCtrlInfo *c_LKunKrz1 = new CCtrlInfo (&m_LKunKrz1, 4, 5, 1, 1);
	CtrlGrid.Add (c_LKunKrz1);
	CCtrlInfo *c_KunKrz1 = new CCtrlInfo (&m_KunKrz1, 5, 5, 1, 1);
	c_KunKrz1->rightspace = 40;
	CtrlGrid.Add (c_KunKrz1);

	CCtrlInfo *c_Pal_bew = new CCtrlInfo (&m_Pal_bew, 4, 7, 1, 1);
	c_Pal_bew->rightspace = 40;
	CtrlGrid.Add (c_Pal_bew);

	CCtrlInfo *c_Trakober = new CCtrlInfo (&m_Trakober, 5, 7, 1, 1);
	c_Trakober->rightspace = 50;
	CtrlGrid.Add (c_Trakober);


	CCtrlInfo *c_LBonitaet = new CCtrlInfo (&m_LBonitaet, 4, 6, 1, 1);
	CtrlGrid.Add (c_LBonitaet);
	CCtrlInfo *c_Bonitaet = new CCtrlInfo (&m_Bonitaet, 5, 6, 1, 1);
	c_Bonitaet->rightspace = 40;
	CtrlGrid.Add (c_Bonitaet);

	CCtrlInfo *c_LMahn = new CCtrlInfo (&m_LMahn, 4, 8, 1, 1);
	CtrlGrid.Add (c_LMahn);
	CCtrlInfo *c_Mahn = new CCtrlInfo (&m_Mahn, 5, 8, 1, 1);
	c_Mahn->rightspace = 40;
	CtrlGrid.Add (c_Mahn);

	CCtrlInfo *c_LDebiKto = new CCtrlInfo (&m_LDebiKto, 4, 10, 1, 1);
	CtrlGrid.Add (c_LDebiKto);
	CCtrlInfo *c_DebiKto = new CCtrlInfo (&m_DebiKto, 5, 10, 1, 1);
	c_DebiKto->rightspace = 40;
	CtrlGrid.Add (c_DebiKto);

	CCtrlInfo *c_LKredVers = new CCtrlInfo (&m_LKredVers, 4, 11, 1, 1);
	CtrlGrid.Add (c_LKredVers);
	CCtrlInfo *c_KredVers = new CCtrlInfo (&m_KredVers, 5, 11, 1, 1);
	c_KredVers->rightspace = 40;
	CtrlGrid.Add (c_KredVers);
	
	CCtrlInfo *c_LKredLim = new CCtrlInfo (&m_LKredLim, 4, 12, 1, 1);
	CtrlGrid.Add (c_LKredLim);
	CCtrlInfo *c_KredLim = new CCtrlInfo (&m_KredLim, 5, 12, 1, 1);
	c_KredLim->rightspace = 40;
	CtrlGrid.Add (c_KredLim);

	CCtrlInfo *c_LZahlArt = new CCtrlInfo (&m_LZahlArt, 4, 14, 1, 1);
	CtrlGrid.Add (c_LZahlArt);
	CCtrlInfo *c_ZahlArt = new CCtrlInfo (&m_ZahlArt, 5, 14, 1, 1);
	c_ZahlArt->rightspace = 40;
	CtrlGrid.Add (c_ZahlArt);

	CCtrlInfo *c_LZahlZiel = new CCtrlInfo (&m_LZahlZiel, 4, 15, 1, 1);
	CtrlGrid.Add (c_LZahlZiel);
	CCtrlInfo *c_ZahlZiel = new CCtrlInfo (&m_ZahlZiel, 5, 15, 1, 1);
	c_ZahlZiel->rightspace = 40;
	CtrlGrid.Add (c_ZahlZiel);

	CCtrlInfo *c_LGut = new CCtrlInfo (&m_LGut, 4, 16, 1, 1);
	CtrlGrid.Add (c_LGut);
	CCtrlInfo *c_Gut = new CCtrlInfo (&m_Gut, 5, 16, 1, 1);
	c_Gut->rightspace = 40;
	CtrlGrid.Add (c_Gut);

	CCtrlInfo *c_LNrbeiRech = new CCtrlInfo (&m_LNrbeiRech, 4, 18, 1, 1);
	CtrlGrid.Add (c_LNrbeiRech);
	CCtrlInfo *c_NrbeiRech = new CCtrlInfo (&m_NrbeiRech, 5, 18, 1, 1);
	c_NrbeiRech->rightspace = 40;
	CtrlGrid.Add (c_NrbeiRech);

	CCtrlInfo *c_LKondKun = new CCtrlInfo (&m_LKondKun, 4, 19, 1, 1);
	CtrlGrid.Add (c_LKondKun);
	CCtrlInfo *c_KondKun = new CCtrlInfo (&m_KondKun, 5, 19, 1, 1);
	c_KondKun->rightspace = 40;
	CtrlGrid.Add (c_KondKun);

	CCtrlInfo *c_LRabSchl = new CCtrlInfo (&m_LRabSchl, 4, 20, 1, 1);
	CtrlGrid.Add (c_LRabSchl);
	CCtrlInfo *c_RabSchl = new CCtrlInfo (&m_RabSchl, 5, 20, 1, 1);
	c_RabSchl->rightspace = 40;
	CtrlGrid.Add (c_RabSchl);

	CCtrlInfo *c_LRechILN = new CCtrlInfo (&m_LRechLN, 4, 22, 1, 1);
	CtrlGrid.Add (c_LRechILN);
	CCtrlInfo *c_RechILN = new CCtrlInfo (&m_RechILN, 5, 22, 1, 1);
	c_RechILN->rightspace = 40;
	CtrlGrid.Add (c_RechILN);

	CCtrlInfo *c_LPfandVer = new CCtrlInfo (&m_LPfandVer, 4, 24, 1, 1);
	CtrlGrid.Add (c_LPfandVer);
	CCtrlInfo *c_PfandVer = new CCtrlInfo (&m_PfandVer, 5, 24, 1, 1);
	c_PfandVer->rightspace = 40;
	CtrlGrid.Add (c_PfandVer);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	CtrlGrid.Display ();
    CtrlGrid.SetItemCharHeight (&m_HeadBorder, 3);
    EnableFields (TRUE);

	short kz2 = Kun->kun.bonitaet;
	if (kz2 == 1)
	{
		m_Bonitaet.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_Bonitaet.SetWindowTextA("   d e a k t i v i e r t");
	}

    Form.Show ();

	return TRUE;
}


BOOL CKunPage3::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				Update->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Adr1)
				{
					OnAdr3Choice();
					return TRUE;
				}
				else if (GetFocus () == &m_Plz)
				{
					OnPlz3Choice();
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F12)
			{
				Form.Get();
				Update->Write();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (ChoiceAdr != NULL)
				{
					if (ChoiceAdr->IsWindowVisible ())
					{
						ChoiceAdr->ShowWindow (SW_HIDE);
					}
					else
					{
						ChoiceAdr->ShowWindow (SW_SHOWNORMAL);
					}
				}
				
			}
			else if (pMsg->wParam == VK_F9)
			{

			}

			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}
			if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

HBRUSH CKunPage3::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);

    if (hBrush == NULL)
	{
		  hBrush = CreateSolidBrush (Color);
		  hBrushStatic = CreateSolidBrush (Color);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			return hBrush;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CButton )))
	{
            pDC->SetBkColor (Color);
			pDC->SetBkMode (TRANSPARENT);
			return hBrushStatic;
	}
	return CPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CKunPage3::OnReturn ()
{
	CWnd *Control = GetFocus (); 

	if (Control == &m_Adr1)
	{
		Form.GetFormField (&m_Adr1)->Get ();
		
		OnAdrSelected();	
		m_Anr.SetFocus ();
		return FALSE;
		
	}

	if (Control == &m_Plz)
	{
		Form.GetFormField (&m_Plz)->Get ();
		Form.GetFormField (&m_Telefon)->Get ();
		PLZ_CLASS Plz = EnterPlz.GetPlzFromPlz (RechAdr->adr.plz);
		if (EnterPlz.Found ())
		{
			_tcscpy (RechAdr->adr.ort1, Plz.plz.ort);
			Form.GetFormField (&m_Ort)->Show ();
			if (_tcscmp (RechAdr->adr.tel, _T("0")) < 0)
			{
				_tcscpy (RechAdr->adr.tel, Plz.plz.vorwahl);
				Form.GetFormField (&m_Telefon)->Show ();
			}
			CFormField *fLand = Form.GetFormField (&m_Land);
			int idx = EnterPlz.GetComboIdx (Plz.plz.bundesland, fLand->ComboValues);
			if (idx != -1)
			{
				m_Land.SetCurSel (idx);
				fLand->Get ();
			}
		}
	}

	if (Control == &m_Ort)
	{
		Form.GetFormField (&m_Ort)->Get ();
		Form.GetFormField (&m_Telefon)->Get ();
		PLZ_CLASS Plz = EnterPlz.GetPlzFromOrt (RechAdr->adr.ort1);
		if (EnterPlz.Found ())
		{
			_tcscpy (RechAdr->adr.plz, Plz.plz.plz);
			Form.GetFormField (&m_Plz)->Show ();
			_tcscpy (RechAdr->adr.ort1, Plz.plz.ort);
			Form.GetFormField (&m_Ort)->Show ();
			if (_tcscmp (RechAdr->adr.tel, _T("0")) < 0)
			{
				_tcscpy (RechAdr->adr.tel, Plz.plz.vorwahl);
				Form.GetFormField (&m_Telefon)->Show ();
			}
			CFormField *fLand = Form.GetFormField (&m_Land);
			int idx = EnterPlz.GetComboIdx (Plz.plz.bundesland, fLand->ComboValues);
			if (idx != -1)
			{
				m_Land.SetCurSel (idx);
				fLand->Get ();
			}
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CKunPage3::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}



void CKunPage3::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CKunPage3::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}

void CKunPage3::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


	if (m_Mdn.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein SDatz zum L�schen selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	if (MessageBox (_T("Preisauszeichnerdaten l�schen"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		Kun->beginwork ();
		Kun->dbdelete ();
		Kun->commitwork ();
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		EnableFields (FALSE);
		Form.Show ();
		m_Mdn.SetFocus ();
		if (ChoiceAdr != NULL)
		{
			ChoiceAdr->RefreshList ();
		}
		KunProperty->SetActivePage (0);
	}
}

void CKunPage3::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
//		Control->SetReadOnly (!enable);
        Control->EnableWindow (enable);
	}
}

BOOL CKunPage3::StepBack ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
					Frame->DestroyWindow ();
					return FALSE;
	}
	else
	{
		EnableFields (FALSE);
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);	
	}
	return TRUE;
}


void CKunPage3::OnCopy ()
{
    OnEditCopy ();
}  



void CKunPage3::OnPaste ()
{
	OnEditPaste ();
}

void CKunPage3::OnCopyEx ()
{

//	if (!m_A.IsWindowEnabled ())
	{
		Copy ();
		return;
	}
}

void CKunPage3::OnPasteEx ()
{

//	if (!m_A.IsWindowEnabled () && Paste ())
	{
		Kun->CopyData (&kun);
		Kun_erw->CopyData (&kun_erw);
		Form.Show ();
/*
		if (KunPageEx != NULL && IsWindow (KunPageEx->m_hWnd))
		{
			KunPageEx->Form.Show ();
		}
*/
		return;
	}
}

BOOL CKunPage3::Copy ()
{
	Form.Get ();
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return FALSE;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      int err = GetLastError ();
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return FALSE;  
    }

	try
	{
        UINT CF_A_KUN_GX = RegisterClipboardFormat (_T("kun")); 
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, sizeof (KUN)); 
        char *p = (char *) GlobalLock(hglbCopy);
        memcpy (p, &Kun->kun, sizeof (Kun->kun));
        GlobalUnlock(hglbCopy); 
		HANDLE cData;
		cData = ::SetClipboardData( CF_A_KUN_GX, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
    return TRUE;
}

BOOL CKunPage3::Paste ()
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return FALSE;
    }

    HGLOBAL hglbCopy;
    UINT CF_A_KUN_GX;
    _TCHAR name [256];
    CF_A_KUN_GX = EnumClipboardFormats (0);
	if (CF_A_KUN_GX == 0)
	{
		int err = GetLastError ();
		CloseClipboard();
		return FALSE;
	}

    int ret;
    while ((ret = GetClipboardFormatName (CF_A_KUN_GX, name, sizeof (name))) != 0)
    { 
         CString Name = name;
         if (Name == _T("a_kun_gx"))
         {
              break;
         }  
		 CF_A_KUN_GX = EnumClipboardFormats (CF_A_KUN_GX);
    }
    if (ret == 0)
    {
         CloseClipboard();
         return FALSE;
    }    

    try
    {	
  	     hglbCopy =  ::GetClipboardData(CF_A_KUN_GX);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
 	     LPSTR p = (LPSTR) GlobalLock ((HGLOBAL) hglbCopy);
         memcpy ( (LPSTR) &kun, p, sizeof (kun));
  	     GlobalUnlock ((HGLOBAL) hglbCopy);
    }
    catch (...) {};
    CloseClipboard();
    return TRUE; 
}

void CKunPage3::EnableFields (BOOL b)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (!b)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);

}


void CKunPage3::OnKillFocus (CWnd *newFocus)
{
	CWnd *Control = GetFocus ();
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		if (f->Scale > 0)
		{
			f->Show ();
		}
	}
}


void CKunPage3::FillPtabCombo (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem)); 
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert)); 
		Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0); 

		
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn->sqlcursor (Sql.GetBuffer ());
		while (Ptabn->sqlfetch (cursor) == 0)
		{
			Ptabn->dbreadfirst();

			LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn->ptabn.ptwert,
				                             Ptabn->ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}




void CKunPage3::FillZahlZiel ()
{

	CFormField *f = Form.GetFormField (&m_ZahlZiel);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Zahl_kond->sqlout ((short *) &Zahl_kond->zahl_kond.zahl_kond, SQLSHORT, 0); 
		Zahl_kond->sqlout ((char *) Zahl_kond->zahl_kond.txt, SQLCHAR, sizeof (Zahl_kond->zahl_kond.txt)); 
		        int cursor = Ptabn->sqlcursor (_T("select zahl_kond, txt from zahl_kond ")
			                          _T("where zahl_kond >= 0 ")
									  _T("order by zahl_kond"));
		while (Zahl_kond->sqlfetch (cursor) == 0)
		{
			Zahl_kond->dbreadfirst();

			/*LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbezk, pos);
			*/
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), Zahl_kond->zahl_kond.zahl_kond,
												Zahl_kond->zahl_kond.txt);
		    f->ComboValues.push_back (ComboValue); 
		}
		Zahl_kond->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}









void CKunPage3::OnBnClickedBonitaet()
{
	short kz2;
	Form.Get();
	
	kz2 = Kun->kun.bonitaet;
	if (kz2 == 1)
	{
		m_Bonitaet.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_Bonitaet.SetWindowTextA("   d e a k t i v i e r t");
	}
}

void CKunPage3::FillRabSchl ()
{

	CFormField *f = Form.GetFormField (&m_RabSchl);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Rab_stufek->sqlout ((char *) Rab_stufek->rab_stufek.rab_nr , SQLCHAR, sizeof(Rab_stufek->rab_stufek.rab_nr)); 
		Rab_stufek->sqlout ((char *) Rab_stufek->rab_stufek.rab_bz_b, SQLCHAR, sizeof (Rab_stufek->rab_stufek.rab_bz_b)); 
		        int cursor = Ptabn->sqlcursor (_T("select rab_nr, rab_bz_b from rab_stufek ")
			                          _T("where rab_typ <> \"Z\" ")
									  _T("order by rab_nr"));
		
		CString *ComboValue = new CString ();
		ComboValue->Format (_T("%s %s"), "",
										 "kein Rabatt");
	    f->ComboValues.push_back (ComboValue); 

		while (Rab_stufek->sqlfetch (cursor) == 0)
		{
			Rab_stufek->dbreadfirst();


			/*LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbezk, pos);
			*/
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Rab_stufek->rab_stufek.rab_nr,
											 Rab_stufek->rab_stufek.rab_bz_b);
		    f->ComboValues.push_back (ComboValue); 
		}
		Rab_stufek->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}

void CKunPage3::OnAdr3Choice ()
{
	CString Adr3Nr;
	m_Adr1.GetWindowText (Adr3Nr);
    ChoiceStat = TRUE;
	if (ChoiceAdr != NULL && !ModalChoiceAdr)
	{
		ChoiceAdr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceAdr == NULL)
	{
		ChoiceAdr = new CChoiceAdr (this);
	    ChoiceAdr->IsModal = ModalChoiceAdr;
		ChoiceAdr->m_Adr = _tstol (Adr3Nr.GetBuffer ());
		ChoiceAdr->CreateDlg ();
	}

	ChoiceAdr->SetDbClass (RechAdr);
	ChoiceAdr->SearchText = Search;
	if (ModalChoiceAdr)
	{
			ChoiceAdr->DoModal();
	}
	
    if (ChoiceAdr->GetState ())
    {
		  CAdrList *abl = ChoiceAdr->GetSelectedText (); 
		  if (abl == NULL) return;
		  memcpy (&RechAdr->adr, &adr_null, sizeof (ADR));
		  Kun->kun.adr3 = abl->adr;
		  if (RechAdr->dbreadfirst () == 0)
		  {
			  RechAdr->adr.adr = Kun->kun.adr3;
			  if ( ! RechAdr->dbreadfirst ())	// if : 260812
				  strncpy (Kun->kun.rechiln, RechAdr->adr.iln,sizeof(Kun->kun.rechiln) -1);	// 260812
		  }
		  m_Adr1.SetSel (0, -1, TRUE);
		  m_Adr1.SetFocus ();
		  
		  Form.Show ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  ChoiceAdr = NULL;
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}

void CKunPage3::OnPlz3Choice ()
{
	PLZ_CLASS Plz;
	memcpy (&Plz.plz, &plz_null, sizeof (Plz.plz));
	Form.GetFormField (&m_Ort)->Get ();
	Form.GetFormField (&m_Telefon)->Get ();
	Form.GetFormField (&m_Land)->Get ();
	Form.GetFormField (&m_IncludeLand)->Get ();
	_tcscpy (Plz.plz.ort, RechAdr->adr.ort1);

	if (IncludeLand)
	{
		memcpy (&Ptabn->ptabn, &ptabn_null, sizeof (Ptabn->ptabn));
		_tcscpy (Ptabn->ptabn.ptitem, _T("land"));
		_stprintf (Ptabn->ptabn.ptwert, "%hd", RechAdr->adr.land);
		if (Ptabn->dbreadfirst () == 0)
		{
			_tcscpy (Plz.plz.bundesland, Ptabn->ptabn.ptbez);
		}
	}

	CPlzList *abl = EnterPlz.Choice (Plz);
	if (abl != NULL)
	{
		
		_tcscpy (RechAdr->adr.plz, abl->plz.GetBuffer ());
		Form.GetFormField (&m_Plz)->Show ();
		_tcscpy (RechAdr->adr.ort1, abl->ort.GetBuffer ());
		Form.GetFormField (&m_Ort)->Show ();
		if (_tcscmp (RechAdr->adr.tel, _T("0")) < 0)
		{
			_tcscpy (RechAdr->adr.tel, abl->vorwahl.GetBuffer ());
			Form.GetFormField (&m_Telefon)->Show ();
		}
		CFormField *fLand = Form.GetFormField (&m_Land);
		int idx = EnterPlz.GetComboIdx (abl->bundesland.GetBuffer (), 
			                            fLand->ComboValues);
		if (idx != -1)
		{
			m_Land.SetCurSel (idx);
			fLand->Get ();
		}
	}
}


void CKunPage3::OnAdrCanceled ()
{
	ChoiceAdr->ShowWindow (SW_HIDE);
}

BOOL CKunPage3::TestAdr3Type ()
{
	if (Kun->kun.adr3 == Kun->kun.adr1)
	{
		return TRUE;
	}
	/**  raus damit, gibt nur �rger LuD 10.01.2011
	RechAdr->adr.adr = Kun->kun.adr3;
	int dsqlstatus = RechAdr->dbreadfirst ();
	if (RechAdr->adr.adr_typ != KunAdrRechType)
	{
		AfxMessageBox (_T("Falscher Adresstyp"));
		m_Adr1.SetFocus ();
		return FALSE;
	}
	**/
	return TRUE;
}
void CKunPage3::OnAdrSelected ()
{
	OnAdrSelectedEx ();
}

BOOL CKunPage3::OnAdrSelectedEx ()
{
	CString AdrNr;
	
	if (ChoiceAdr == NULL) 
	{
		m_Adr1.GetWindowText (AdrNr);		
		memcpy (&RechAdr->adr, &adr_null, sizeof (ADR));
		
		long Adr1 = atol(AdrNr);
		Kun->kun.adr3 = Adr1;
	}
	else
	{
		CAdrList *abl = ChoiceAdr->GetSelectedText();
		
		if (abl == NULL) 
		{
			return FALSE;
		}
		else
		{
			memcpy (&RechAdr->adr, &adr_null, sizeof (ADR));
			Kun->kun.adr3 = abl->adr;

		}
	}
        
    
	RechAdr->adr.adr = Kun->kun.adr3;
	int dsqlstatus = RechAdr->dbreadfirst ();
	if (dsqlstatus == 100)
	{
		RechAdr->adr.adr_typ = KunAdrRechType;
// Grundwert holen LuD 10.01.2011
		RechAdr->adr.adr = -1;
		RechAdr->dbreadfirst ();
		RechAdr->adr.adr = Kun->kun.adr3;

	}

	if (Kun->kun.adr1 != Kun->kun.adr3)
	{
	/**  raus damit, gibt nur �rger LuD 10.01.2011
		if (RechAdr->adr.adr_typ != KunAdrRechType)
		{
			AfxMessageBox (_T("Falscher Adresstyp"));
			return FALSE;
		}
		**/
	}
    
	if (KunAdr->adr.adr == RechAdr->adr.adr || LiefAdr->adr.adr == RechAdr->adr.adr)
	{
		m_Anr.EnableWindow(FALSE);
		m_Name1.EnableWindow(FALSE);
		m_Name2.EnableWindow(FALSE);
		m_Name3.EnableWindow(FALSE);
		m_Strasse.EnableWindow(FALSE);
		m_Postfach.EnableWindow(FALSE);
		m_plzPostfach.EnableWindow(FALSE);
		m_Plz.EnableWindow(FALSE);
		m_Ort.EnableWindow(FALSE);
		m_Telefon.EnableWindow(FALSE);
		m_Fax.EnableWindow(FALSE);
		m_EMAIL.EnableWindow(FALSE);
		m_Partner.EnableWindow(FALSE);
		m_Staat.EnableWindow(FALSE);
		m_Land.EnableWindow(FALSE);
		Form.Show ();	
		m_KunKrz1.SetFocus();
	}
	else
	{
		m_Anr.EnableWindow(TRUE);
		m_Name1.EnableWindow(TRUE);
		m_Name2.EnableWindow(TRUE);
		m_Name3.EnableWindow(TRUE);
		m_Strasse.EnableWindow(TRUE);
		m_Postfach.EnableWindow(TRUE);
		m_plzPostfach.EnableWindow(TRUE);
		m_Plz.EnableWindow(TRUE);
		m_Ort.EnableWindow(TRUE);
		m_Telefon.EnableWindow(TRUE);
		m_Fax.EnableWindow(TRUE);
		m_EMAIL.EnableWindow(TRUE);
		m_Partner.EnableWindow(TRUE);
		m_Staat.EnableWindow(TRUE);
		m_Land.EnableWindow(TRUE);
		Form.Show ();
		m_Anr.SetFocus();
	}
	

	//Read ();
	
	if (ChoiceAdr != NULL)
	{
		if (ChoiceAdr->FocusBack)
		{
			ChoiceAdr->SetListFocus ();
		}
	}
	return TRUE;
}



BOOL CKunPage3::OnKillActive ()
{
	Form.Get ();
	if (F5Pressed)
	{
		return TRUE;
	}
	return TestAdr3Type ();
}

BOOL CKunPage3::OnSetActive ()
{
	F5Pressed = FALSE;
	if (KunAdr->adr.adr == RechAdr->adr.adr || LiefAdr->adr.adr == RechAdr->adr.adr)
	{
		m_Anr.EnableWindow(FALSE);
		m_Name1.EnableWindow(FALSE);
		m_Name2.EnableWindow(FALSE);
		m_Name3.EnableWindow(FALSE);
		m_Strasse.EnableWindow(FALSE);
		m_Postfach.EnableWindow(FALSE);
		m_plzPostfach.EnableWindow(FALSE);
		m_Plz.EnableWindow(FALSE);
		m_Ort.EnableWindow(FALSE);
		m_Telefon.EnableWindow(FALSE);
		m_Fax.EnableWindow(FALSE);
		m_EMAIL.EnableWindow(FALSE);
		m_Partner.EnableWindow(FALSE);
		m_Staat.EnableWindow(FALSE);
		m_Land.EnableWindow(FALSE);
	}
	else
	{				
		m_Anr.EnableWindow(TRUE);
		m_Name1.EnableWindow(TRUE);
		m_Name2.EnableWindow(TRUE);
		m_Name3.EnableWindow(TRUE);
		m_Strasse.EnableWindow(TRUE);
		m_Postfach.EnableWindow(TRUE);
		m_plzPostfach.EnableWindow(TRUE);
		m_Plz.EnableWindow(TRUE);
		m_Ort.EnableWindow(TRUE);
		m_Telefon.EnableWindow(TRUE);
		m_Fax.EnableWindow(TRUE);
		m_EMAIL.EnableWindow(TRUE);
		m_Partner.EnableWindow(TRUE);
		m_Staat.EnableWindow(TRUE);
		m_Land.EnableWindow(TRUE);
		Form.Show ();
		m_Anr.SetFocus();
	}
	return TRUE;

	
}

void CKunPage3::UpdatePage ()
{
	Form.Show ();
}





void CKunPage3::OnEnChangeKunkrz1()
{
	CString Text;
	m_KunKrz1.GetWindowText (Text);
	if (Text.GetLength() >= sizeof(Kun->kun.kun_krz3))
	{
		strcpy(Kun->kun.kun_krz3,"");
		strncpy(Kun->kun.kun_krz3,Text.GetBuffer(),sizeof(Kun->kun.kun_krz3)-1);
		Kun->kun.kun_krz3[sizeof(Kun->kun.kun_krz3)-1] = 0;
		Form.Show ();
		m_KunKrz1.SetSel (sizeof(Kun->kun.kun_krz3)-1, -1,FALSE);
	}
}

void CKunPage3::OnBnClickedPalbew()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CKunPage3::OnBnClickedTrakober()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}
