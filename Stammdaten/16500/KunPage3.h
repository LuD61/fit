#pragma once
#include "DbPropertyPage.h"
#include "resource.h"
#include "ListDropTarget.h"
#include "CtrlGrid.h"
#include "Kun.h"
#include "kun_erw.h"
#include "Mdn.h"
#include "Adr.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "FormTab.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "DbFormView.h"
#include "CodeProperties.h"
#include "mo_progcfg.h"
#include "sys_par.h"
#include "ptabn.h"
#include "iprgrstufk.h"
#include "i_kun_prk.h"
#include "kst.h"
#include "kun_tou.h"
#include "fil.h"
#include "gr_zuord.h"
#include "zahl_kond.h"
#include "rab_stufek.h"
#include "haus_bank.h"
#include "ls_txt.h"
#include "ChoiceAdr.h"
#include "ChoiceX.h"
#include "AdrList.h"
#include "EnterPlz.h"


#define IDC_ADR3CHOICE 4005
#define IDC_PLZ3CHOICE 4006

// CKunPage3-Dialogfeld

class CKunPage3 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CKunPage3)

public:
	CKunPage3();
	virtual ~CKunPage3();

// Dialogfelddaten
	enum { IDD = IDD_KUNPAGE3 };
	enum
	{
		KunAdrType = 20,
		KunAdrLiefType = 21,
		KunAdrRechType = 22,
		KunAdrDivType = 25
	};


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);


	DECLARE_MESSAGE_MAP()
	
	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	PROG_CFG Cfg;

	virtual BOOL OnInitDialog();



public:

	KUN_CLASS *Kun;
	KUN_ERW_CLASS *Kun_erw;
	ADR_CLASS *KunAdr;
	ADR_CLASS *RechAdr;
	ADR_CLASS *LiefAdr;
	MDN_CLASS *Mdn;
	ADR_CLASS *MdnAdr;
	PTABN_CLASS *Ptabn;
	IPRGRSTUFK_CLASS *Iprgrstufk;
	I_KUN_PRK_CLASS *I_kun_prk;
	KST_CLASS *Kst;
	SYS_PAR_CLASS *Sys_par;
	KUN_TOU_CLASS *Kun_tou;
	FIL_CLASS *Fil;
	GR_ZUORD_CLASS *Gr_zuord;
	ZAHL_KOND_CLASS *Zahl_kond;
	RAB_STUFEK_CLASS *Rab_stufek;
	HAUS_BANK_CLASS *Haus_bank;
	LS_TXT_CLASS *Ls_txt;
	
	CEnterPlz EnterPlz;
	CFormTab Form;

	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid KunGrid;
	CCtrlGrid Adr1Grid;
	CCtrlGrid TelFaxGrid;
	CCtrlGrid PLZPostfachGrid;
	CCtrlGrid BbnGrid;
	CCtrlGrid TourGrid;
	CCtrlGrid Tour2Grid;
	CCtrlGrid BonitaetGrid;
	CCtrlGrid Plz1Grid;
	CCtrlGrid Pal_bewGrid;
	CCtrlGrid TrakoberGrid;
		
	CListDropTarget dropTarget;

    CPropertySheet *KunProperty;
    DbFormView *View;
	CFrameWnd *Frame;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	
	void Register ();
	CEdit m_MdnName;
	CStatic m_LKun;
	CNumEdit m_Kun;
	
	CEdit m_KunName;
	CStatic m_HeadBorder;
	CStatic m_DataBorder;
	CStatic m_LAnr;
	CComboBox m_Anr;
	CStatic m_LAdr1;
	CNumEdit m_Adr1;
	CButton m_Adr1Choice;
	
    CVector HeadControls;
	BOOL ChoiceStat;
	CString Search;

	CChoiceAdr *ChoiceAdr;
	BOOL ModalChoiceAdr;
	
	CCodeProperties Code;
    BOOL OnReturn ();
    BOOL OnKeyup ();
    
	void FillZahlZiel ();
	void OnDelete ();
	void OnAdr3Choice();
	void OnPlz3Choice();
	void OnAdrCanceled();
	BOOL TestAdr3Type ();
	BOOL OnAdrSelectedEx();
	void OnAdrSelected();
	void FillRabSchl();
	void FillPtabCombo (CWnd *Control, LPTSTR Item);
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
    void EnableHeadControls (BOOL enable);
    BOOL StepBack ();
    virtual void OnCopy ();
    virtual void OnPaste ();
	BOOL Copy ();
	BOOL Paste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
    void EnableFields (BOOL b);
	void UpdatePage ();
	afx_msg void OnKillFocus (CWnd *);
	CStatic m_LName1;
	CTextEdit m_Name1;
public:
	CTextEdit m_Name2;
public:
	CStatic m_LName2;
public:
	CTextEdit m_Name3;
public:
	CStatic m_LName3;
public:
	CTextEdit m_Strasse;
public:
	CStatic m_LStrasse;
public:
	CNumEdit m_Plz;
	CButton m_Plz3Choice;
public:
	CStatic m_LPlz;
public:
	CTextEdit m_Ort;
public:
	CStatic m_LOrt;
public:
	CStatic m_LPostfach;
public:
	CNumEdit m_Postfach;
public:
	CNumEdit m_Telefon;
public:
	CStatic m_LTelefon;
public:
	CNumEdit m_Fax;
	public:
	CStatic m_LKunKrz1;
public:
	CTextEdit m_KunKrz1;
public:
	CNumEdit m_plzPostfach;
public:
	CNumEdit m_EMAIL;
public:
	CStatic m_LEmail;
public:
	CTextEdit m_Partner;
public:
	CStatic m_LPartner;
public:
	CComboBox m_Staat;
public:
	CStatic m_LStaat;
public:
	CComboBox m_Land;
	CButton m_IncludeLand;
	BOOL IncludeLand;
public:
	CStatic m_LLand;

public:
	CStatic m_DataBorder2;

public:
	CButton m_Bonitaet;
public:
	CStatic m_LBonitaet;
public:
	CNumEdit m_DebiKto;
public:
	CStatic m_LDebiKto;
public:
	CComboBox m_Mahn;
public:
	CStatic m_LMahn;
public:
	CComboBox m_KredVers;
public:
	CStatic m_LKredVers;
public:
	CNumEdit m_KredLim;
public:
	CStatic m_LKredLim;
public:
	CComboBox m_ZahlArt;
public:
	CStatic m_LZahlArt;
public:
	CComboBox m_ZahlZiel;
public:
	CStatic m_LZahlZiel;
public:
	CComboBox m_Gut;
public:
	CStatic m_LGut;
public:
	CNumEdit m_NrbeiRech;
public:
	CStatic m_LNrbeiRech;
public:
	CEdit m_BonitaetBez;
public:
	afx_msg void OnBnClickedBonitaet();
public:
	CComboBox m_RabSchl;
public:
	CComboBox m_ZuSchl;
public:
	CStatic m_LZuSchl;
public:
	CStatic m_LRabSchl;
public:
	CStatic m_LKondKun;
public:
	CNumEdit m_KondKun;

public:
	CNumEdit m_RechILN;
public:
	CStatic m_LRechLN;
	afx_msg void OnEnChangeKunkrz1();
	afx_msg void OnBnClickedPalbew();
	afx_msg void OnBnClickedTrakober();
	CButton m_Pal_bew;
	CButton m_Trakober;
	CStatic m_LPfandVer;
	CButton m_PfandVer;
	CEdit m_Partner3;
	CStatic m_LPartner3;
	CStatic m_LModem2;
	CEdit m_Modem2;
};




