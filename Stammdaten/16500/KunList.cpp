#include "StdAfx.h"
#include "KunList.h"

CKunList::CKunList(void)
{
	mdn = 0;
	kun = 0l;
	adr_krz = _T("");
	kun_bran2 = _T("");
}

CKunList::CKunList(short mdn, long kun, LPTSTR adr_krz, LPTSTR kun_bran2)
{
	this->mdn  = mdn;
	this->kun = kun;
	this->adr_krz = adr_krz;
	this->kun_bran2 = kun_bran2;
}

CKunList::~CKunList(void)
{
}
