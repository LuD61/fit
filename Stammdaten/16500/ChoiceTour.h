#ifndef _CHOICETOUR_DEF
#define _CHOICETOUR_DEF

#include "ChoiceX.h"
#include "TourList.h"
#include <vector>

class CChoiceTour : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
      
    public :
		long m_Adr;
	    std::vector<CTourList *> TourList;
      	CChoiceTour(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceTour(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchBez (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CTourList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
