#include "stdafx.h"
#include "kun_erw.h"

struct KUN_ERW kun_erw, kun_erw_null, kun_erw_def;

void KUN_ERW_CLASS::prepare (void)
{
            TCHAR *sqltext;

 

           	sqlin ((short *)   &kun_erw.mdn,  SQLSHORT, 0);
		sqlin ((short *)   &kun_erw.fil,  SQLSHORT, 0);
		sqlin ((long *)   &kun_erw.kun,  SQLLONG, 0);
    sqlout ((short *) &kun_erw.mdn,SQLSHORT,0);
    sqlout ((short *) &kun_erw.fil,SQLSHORT,0);
    sqlout ((long *) &kun_erw.kun,SQLLONG,0);
    sqlout ((long *) &kun_erw.m2laden,SQLLONG,0);
    sqlout ((short *) &kun_erw.werbung,SQLSHORT,0);
    sqlout ((short *) &kun_erw.hoehe_pal,SQLSHORT,0);
    sqlout ((short *) &kun_erw.kun_kl,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &kun_erw.mandseit,SQLDATE,0);
    sqlout ((TCHAR *) kun_erw.sambuch,SQLCHAR,2);
    sqlout ((short *) &kun_erw.best_profil,SQLSHORT,0);
    sqlout ((double *) &kun_erw.max_gew_dd,SQLDOUBLE,0);
    sqlout ((double *) &kun_erw.max_gew_eu,SQLDOUBLE,0);
    sqlout ((short *) &kun_erw.verladeschein,SQLSHORT,0);
            	cursor = sqlcursor (_T("select kun_erw.mdn,  ")
_T("kun_erw.fil,  kun_erw.kun,  kun_erw.m2laden,  kun_erw.werbung,  ")
_T("kun_erw.hoehe_pal,  kun_erw.kun_kl,  kun_erw.mandseit,  ")
_T("kun_erw.sambuch,  kun_erw.best_profil,  kun_erw.max_gew_dd,  ")
_T("kun_erw.max_gew_eu,  kun_erw.verladeschein from kun_erw ")

#line 16 "kun_erw.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ?"));

            	sqlin ((short *)   &kun_erw.mdn,  SQLSHORT, 0);
		sqlin ((short *)   &kun_erw.fil,  SQLSHORT, 0);
		sqlin ((long *)   &kun_erw.kun,  SQLLONG, 0);
            	test_upd_cursor = sqlcursor (_T("select kun from kun_erw ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ? for update"));



    sqlin ((short *) &kun_erw.mdn,SQLSHORT,0);
    sqlin ((short *) &kun_erw.fil,SQLSHORT,0);
    sqlin ((long *) &kun_erw.kun,SQLLONG,0);
    sqlin ((long *) &kun_erw.m2laden,SQLLONG,0);
    sqlin ((short *) &kun_erw.werbung,SQLSHORT,0);
    sqlin ((short *) &kun_erw.hoehe_pal,SQLSHORT,0);
    sqlin ((short *) &kun_erw.kun_kl,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &kun_erw.mandseit,SQLDATE,0);
    sqlin ((TCHAR *) kun_erw.sambuch,SQLCHAR,2);
    sqlin ((short *) &kun_erw.best_profil,SQLSHORT,0);
    sqlin ((double *) &kun_erw.max_gew_dd,SQLDOUBLE,0);
    sqlin ((double *) &kun_erw.max_gew_eu,SQLDOUBLE,0);
    sqlin ((short *) &kun_erw.verladeschein,SQLSHORT,0);
            	sqltext = _T("update kun_erw set ")
_T("kun_erw.mdn = ?,  kun_erw.fil = ?,  kun_erw.kun = ?,  ")
_T("kun_erw.m2laden = ?,  kun_erw.werbung = ?,  kun_erw.hoehe_pal = ?,  ")
_T("kun_erw.kun_kl = ?,  kun_erw.mandseit = ?,  kun_erw.sambuch = ?,  ")
_T("kun_erw.best_profil = ?,  kun_erw.max_gew_dd = ?,  ")
_T("kun_erw.max_gew_eu = ?,  kun_erw.verladeschein = ? ")

#line 31 "kun_erw.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ?");
		
		sqlin ((short *)   &kun_erw.mdn,  SQLSHORT, 0);
		sqlin ((short *)   &kun_erw.fil,  SQLSHORT, 0);
		sqlin ((long *)   &kun_erw.kun,  SQLLONG, 0);
           	upd_cursor = sqlcursor (sqltext);


         	sqlin ((short *)   &kun_erw.mdn,  SQLSHORT, 0);
		sqlin ((short *)   &kun_erw.fil,  SQLSHORT, 0);
		sqlin ((long *)   &kun_erw.kun,  SQLLONG, 0);            	
		del_cursor = sqlcursor (_T("delete from kun_erw ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ?"));
            	

    sqlin ((short *) &kun_erw.mdn,SQLSHORT,0);
    sqlin ((short *) &kun_erw.fil,SQLSHORT,0);
    sqlin ((long *) &kun_erw.kun,SQLLONG,0);
    sqlin ((long *) &kun_erw.m2laden,SQLLONG,0);
    sqlin ((short *) &kun_erw.werbung,SQLSHORT,0);
    sqlin ((short *) &kun_erw.hoehe_pal,SQLSHORT,0);
    sqlin ((short *) &kun_erw.kun_kl,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &kun_erw.mandseit,SQLDATE,0);
    sqlin ((TCHAR *) kun_erw.sambuch,SQLCHAR,2);
    sqlin ((short *) &kun_erw.best_profil,SQLSHORT,0);
    sqlin ((double *) &kun_erw.max_gew_dd,SQLDOUBLE,0);
    sqlin ((double *) &kun_erw.max_gew_eu,SQLDOUBLE,0);
    sqlin ((short *) &kun_erw.verladeschein,SQLSHORT,0);
		ins_cursor = sqlcursor (_T("insert into kun_erw (mdn,  fil,  kun,  ")
_T("m2laden,  werbung,  hoehe_pal,  kun_kl,  mandseit,  sambuch,  best_profil,  ")
_T("max_gew_dd,  max_gew_eu,  verladeschein) ")

#line 51 "kun_erw.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?)"));

#line 53 "kun_erw.rpp"
}
