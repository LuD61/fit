#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include "token.h"
#include "mo_progcfg.h"

static _TCHAR rec [1024];
static _TCHAR dm  [512];

_TCHAR PROG_CFG::cfg_v [512];

PROG_CFG::~PROG_CFG ()
{
	     CloseCfg (); 
}

BOOL PROG_CFG::OpenCfg (void)
/**
CFG-Datei oeffnen.
**/
{
         LPTSTR etc;

         etc = _tgetenv (_T("BWSETC"));
         if (etc == NULL)
         {
                     etc = _T("c:\\user\\fit\\etc");
         }
 
         _stprintf (dm, _T("%s\\%s.cfg"), etc, progname);

         progfp = _tfopen (dm, _T("r"));
         if (progfp == NULL) return FALSE;
         return TRUE;
}
           
void PROG_CFG::CloseCfg (void)
/**
CFG-Datei schliessen.
**/
{
         if (progfp != NULL)
         {
             fclose (progfp);
             progfp = NULL;
         }
}

BOOL PROG_CFG::GetCfgValue (LPTSTR progitem, LPTSTR progvalue)
/**
Wert fuer progitem holen.
**/
{
//         int anz;
         BOOL itOK; 
         int len;
		 CToken token;

         itOK = FALSE;
         if (progfp == NULL)
         {
                   if (OpenCfg () == FALSE) return FALSE;
         }

		 token.SetSep (_T(" "));
         progvalue[0] = (char) 0;
		 fseek (progfp, 0l, 0);
         while (_fgetts (rec, 1023, progfp))
         {
                   if (rec[0] < (char) 0x30) continue;
				   token = rec;
                   if (token.GetAnzToken () < 2) continue;
				   LPTSTR wort1 = token.GetToken (0);
                   len = (int) max (_tcslen (progitem), (int) _tcslen (wort1));

                   if (strupcmp (wort1, progitem, len) == 0)
                   {
					          LPTSTR wort2 = token.GetToken (1); 
                              _tcscpy (progvalue, wort2);
							  clipped (progvalue);
                              itOK = TRUE; 
                              break;
                  }
         }
         return itOK;
}

BOOL PROG_CFG::GetCfgValue (LPTSTR progitem, LPTSTR progvalue, LPTSTR csplit)
/**
Wert fuer progitem holen.
**/
{
//         int anz;
         BOOL itOK; 
         int len;
		 CToken token;

         itOK = FALSE;
         if (progfp == NULL)
         {
                   if (OpenCfg () == FALSE) return FALSE;
         }

		 token.SetSep (_T(csplit));
         progvalue[0] = (char) 0;
		 fseek (progfp, 0l, 0);
         while (_fgetts (rec, 1023, progfp))
         {
                   if (rec[0] < (char) 0x30) continue;
				   token = rec;
                   if (token.GetAnzToken () < 2) continue;
				   LPTSTR wort1 = token.GetToken (0);
                   len = (int) max (_tcslen (progitem), (int) _tcslen (wort1));

                   if (strupcmp (wort1, progitem, len) == 0)
                   {
					          LPTSTR wort2 = token.GetToken (1); 
                              _tcscpy (progvalue, wort2);
							  clipped (progvalue);
                              itOK = TRUE; 
                              break;
                  }
         }
         return itOK;
}


         
BOOL PROG_CFG::ReadCfgItem (LPTSTR itname, LPTSTR def, LPTSTR text,
                            LPTSTR *values, LPTSTR help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         LPTSTR s;
		 CToken token;

         if (progfp == NULL)
         {
                   if (OpenCfg () == FALSE) return FALSE;
         }

         while (s = _fgetts (rec, 1023, progfp))
         {
                   if (rec[0] >= (_TCHAR) 0x30) break;
         }

         if (s == NULL) return FALSE;

		 token.SetSep (_T(" "));
         token = rec;
		 anz = token.GetAnzToken ();
         if (anz < 3) return FALSE;
         LPTSTR wort1 = token.GetToken (0);
         LPTSTR wort2 = token.GetToken (1);
         LPTSTR wort3 = token.GetToken (2);

         itname = new _TCHAR [_tcslen (wort1) + 10];
         if (itname == NULL) return FALSE;
         _tcscpy (itname, wort1);
		 def = new _TCHAR [_tcslen (wort2) + 10];
         if (def == NULL) return FALSE;
         _tcscpy (def, wort2);
		 text = new _TCHAR [_tcslen (wort3) + 10];
         if (text == NULL) return FALSE;
         _tcscpy (text, wort3);
         return ReadCfgValues (values, help);
}
         
BOOL PROG_CFG::ReadCfgValues (LPTSTR *values, LPTSTR help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         int i;
         LPTSTR s;
         BOOL InValue;
		 CToken token;

         if (progfp == NULL)
         {
                   return FALSE;
         }

         i = 0;
         InValue = FALSE;
		 token.SetSep (_T(" "));
         while (s = _fgetts (rec, 1023, progfp))
         {
                   CString C = rec;
				   C.TrimRight ();
				   _tcscpy (rec, C.GetBuffer ());
                   if (rec[0] >= (_TCHAR) 0x30) return FALSE;
				   token = rec;
				   anz = token.GetAnzToken ();
                   if (anz == 0) continue;
				   LPTSTR wort1 = token.GetToken (0);
                   if (_tcscmp (wort1, _T("$VALUES")) == 0) 
                   {
                       InValue = TRUE;
                       continue;
                   }

                   if (_tcscmp (wort1, _T("$HELP"))   == 0) 
                   {
                        return ReadCfgHelp (help);
                   }
                   if (_tcscmp (wort1, _T("$END")) == 0) return TRUE;
                   if (InValue)
                   {
                       values[i] = new _TCHAR [_tcslen (rec) + 10];
                       if (values[i] == NULL)
                       {
                           return FALSE;
                       }
                       _tcscpy (values[i], rec);
                       i ++;
                   }
         }
         return FALSE;
}
         
                          
BOOL PROG_CFG::ReadCfgHelp (LPTSTR help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         int i;
         LPTSTR s;
         BOOL InHelp;
		 CToken token;

         if (progfp == NULL)
         {
                   return FALSE;
         }

         i = 0;
         InHelp = FALSE;
		 token.SetSep (_T(" "));
         while (s = _fgetts (rec, 1023, progfp))
         {
                   CString C = rec;
				   C.TrimRight ();
				   _tcscpy (rec, C.GetBuffer ());
                   if (rec[0] >= (_TCHAR) 0x30) return FALSE;
				   token = rec;
				   anz = token.GetAnzToken ();
                   if (anz == 0) continue;
				   LPTSTR wort1 = token.GetToken (0);
                   if (_tcscmp (wort1, _T("$HELP")) == 0) 
                   {
                       InHelp = TRUE;
                       continue;
                   }

                   if (_tcscmp (wort1, _T("$END"))    == 0) return TRUE;
                   if (InHelp)
                   {
					   help = new _TCHAR [_tcslen (rec) + 10];
                       if (help == NULL) return FALSE;
                       _tcscpy (help, rec);
                   }
         }
         return FALSE;
}
         
                               

BOOL PROG_CFG::GetGlobDefault (LPTSTR env, LPTSTR wert)
/**
Wert aus bws_default holen.
**/
{
        LPTSTR etc;
        int anz;
        _TCHAR buffer [512];
        FILE *fp;
	    CToken token;

        etc = _tgetenv (_T("BWSETC"));
        if (etc == NULL)
        {
                    etc = _T("C:\\USER\\FIT\\ETC");
        }

        _stprintf (buffer, _T("%s\\bws_defa"), etc);
        fp = _tfopen (buffer, _T("r"));
        if (fp == NULL) return FALSE;

		token.SetSep (_T(" "));
        while (_fgetts (buffer, 511, fp))
        {
                     CString C = buffer;
				     C.TrimRight ();
				     _tcscpy (buffer, C.GetBuffer ());
					 token = buffer;
					 anz = token.GetAnzToken ();

//                     anz = split (buffer);
                     if (anz < 2) continue;
					 LPTSTR wort1 = token.GetToken (0);
					 LPTSTR wort2 = token.GetToken (1);
					 CString Wort1 = wort1;
					 CString Env = env;
                     if (strupcmp (wort1, env, (int) _tcslen (env)) == 0)
                     {
                                 _tcscpy (wert, wort2);
                                 fclose (fp);
                                 return TRUE;
                     }
         }
         fclose (fp);
         return (FALSE);
}

BOOL PROG_CFG::GetGroupDefault (LPTSTR env, LPTSTR wert)
/**
Wert aus fitgroup.def holen.
**/
{
        LPTSTR etc;
        int anz;
        _TCHAR buffer [512];
        FILE *fp;
	    CToken token;

        etc = _tgetenv (_T("BWSETC"));
        if (etc == NULL)
        {
                    etc = _T("C:\\USER\\FIT\\ETC");
        }

        _stprintf (buffer, _T("%s\\fitgroup.def"), etc);
        fp = _tfopen (buffer, _T("r"));
        if (fp == NULL) return FALSE;

		token.SetSep (_T(" "));
        while (_fgetts (buffer, 511, fp))
        {
                     CString C = buffer;
				     C.TrimRight ();
				     _tcscpy (buffer, C.GetBuffer ());
					 token = buffer;
					 anz = token.GetAnzToken ();
                     if (anz < 2) continue;
					 LPTSTR wort1 = token.GetToken (0);
					 LPTSTR wort2 = token.GetToken (1);
					 CString Wort1 = wort1;
					 CString Env = env;
                     if (strupcmp (wort1, env, (int) _tcslen (env)) == 0)
                     {
                                 _tcscpy (wert, wort2);
                                 fclose (fp);
                                 return TRUE;
                     }
         }
         fclose (fp);
         return (FALSE);
}

void PROG_CFG::clipped (LPTSTR str)
{
	_TUCHAR *p = (_TUCHAR *) str;
	p += _tcslen (str);
	for (; (p >= (_TUCHAR *) str) && (*p < 0x21) ; p -= 1);
	*(p + 1) = 0;
}

void PROG_CFG::cr_weg (LPTSTR str)
{
	_TUCHAR *p = (_TUCHAR *) str;
	p += _tcslen (str);
	for (; (p >= (_TUCHAR *) str) && (*p != 13) && (*p != 10); 
		 p -= 1);
	*p = 0;
	if (p == (_TUCHAR *) str) return;
	*p --;
	for (; (p >= (_TUCHAR *) str) && (*p != 13) && (*p != 10); 
		 p -= 1);
	*p = 0;
}

int PROG_CFG::strupcmp (LPTSTR str1,LPTSTR str2, int len)
{
	CString Str1 = str1;
	CString Str2 = str2;
	Str2 = Str2.Left (len);
	return (Str1.CompareNoCase (Str2));
}
