#ifndef _HDATEN_DEF
#define _HDATEN_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct HDATEN {
   long           kun;
   short          mdn;
   double         rv_proz;
   double         zr_proz;
   long           plums_m01;
   long           plums_m02;
   long           plums_m03;
   long           plums_m04;
   long           plums_m05;
   long           plums_m06;
   long           plums_m07;
   long           plums_m08;
   long           plums_m09;
   long           plums_m10;
   long           plums_m11;
   long           plums_m12;
   short          jr;
};
extern struct HDATEN hdaten, hdaten_null;

#line 8 "hdaten.rh"

class HDATEN_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               HDATEN hdaten;  
               HDATEN_CLASS () : DB_CLASS ()
               {
               }
};
#endif
