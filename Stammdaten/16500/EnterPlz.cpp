#include "StdAfx.h"
#include "EnterPlz.h"

CEnterPlz::CEnterPlz(void)
{
	cursor_ort = -1;
	cursor_vorwahl = -1;
	ChoicePlz = NULL;
}

CEnterPlz::~CEnterPlz(void)
{
	if (cursor_ort != -1)
	{
		Plz.sqlclose (cursor_ort);
	}
	if (cursor_vorwahl != -1)
	{
		Plz.sqlclose (cursor_vorwahl);
	}
	if (ChoicePlz != NULL)
	{
		delete ChoicePlz;
	}
}

CPlzList* CEnterPlz::Choice (PLZ_CLASS& Plz)
{
	CString Query = _T("");
	CString Ort   = _T("");
	CString Bundesland = _T("");
	CString Vorwahl = _T("");
	CPlzList *abl = NULL;
	Ort = Plz.plz.ort;
	Bundesland = Plz.plz.bundesland;
	Vorwahl = Plz.plz.vorwahl;
	Ort.Trim ();
	Bundesland.Trim ();
	Vorwahl.Trim ();
	if (Ort != _T(""))
	{
		Query = "where upper (ort) matches upper (\"*" + Ort + "*\")";    //WAL-180   upper (
	}
	if (Bundesland != _T(""))
	{
		if (Query == _T(""))
		{
			Query = "where bundesland = \"" + Bundesland + "\"";
		}
		else
		{
			Query += " and bundesland = \"" + Bundesland + "\"";
		}
	}

	if (Vorwahl != _T(""))
	{
		if (Query == _T(""))
		{
			Query = "where vorwahl = \"" + Vorwahl + "\"";
		}
		else
		{
			Query += " and vorwahl = \"" + Vorwahl + "\"";
		}
	}
	if (ChoicePlz == NULL)
	{
		ChoicePlz = new CChoicePlz ();
		ChoicePlz->IsModal = TRUE;
		ChoicePlz->SetDlgSize (450, 300);
		ChoicePlz->SetBitmapButton (TRUE);
		ChoicePlz->CreateDlg ();
		ChoicePlz->SetQuery (Query.GetBuffer ());
		ChoicePlz->HideFilter = FALSE;
		LastQuery = Query;
	}
	else if (LastQuery != Query)
	{
		ChoicePlz->DestroyList ();
		ChoicePlz->SetQuery (Query.GetBuffer ());
		LastQuery = Query;
	}

    ChoicePlz->DoModal ();
    if (ChoicePlz->GetState ())
    {
		  abl = ChoicePlz->GetSelectedText (); 
	}
	return abl;
}

PLZ_CLASS& CEnterPlz::GetPlzFromPlz (LPTSTR plz)
{
	found = FALSE;
	memcpy (&Plz.plz, &plz_null, sizeof (Plz.plz));
	_tcscpy (Plz.plz.plz, plz);
    if (Plz.dbreadfirst () == 0)
	{
		found = TRUE;
	}
	return Plz;
}

PLZ_CLASS& CEnterPlz::GetPlzFromOrt (LPTSTR ort)
{
	if (cursor_ort == -1)
	{
		CString SqlString;
		Plz.sqlin  ((LPTSTR) Plz.plz.ort, SQLCHAR, sizeof (Plz.plz.ort));
		Plz.sqlout ((LPTSTR) Plz.plz.plz, SQLCHAR, sizeof (Plz.plz.plz));
		SqlString= _T("select plz from plz where upper (ort) matches upper (?)");  //WAL-180 upper
		cursor_ort = Plz.sqlcursor (SqlString.GetBuffer ());
	}
	found = FALSE;
	memcpy (&Plz.plz, &plz_null, sizeof (Plz.plz));
	CString Ort;
	Ort.Format (_T("*%s*"), ort);
	_tcscpy (Plz.plz.ort, Ort.GetBuffer ());
	Plz.sqlopen (cursor_ort);
	if (Plz.sqlfetch (cursor_ort) == 0)
	{
		found = TRUE;
		Plz.dbreadfirst ();
	}
	return Plz;
}

PLZ_CLASS& CEnterPlz::GetPlzFromVorwahl (LPTSTR vorwahl)
{
	if (cursor_vorwahl == -1)
	{
		CString SqlString;
		Plz.sqlin  ((LPTSTR) Plz.plz.vorwahl, SQLCHAR, sizeof (Plz.plz.vorwahl));
		Plz.sqlout ((LPTSTR) Plz.plz.plz, SQLCHAR, sizeof (Plz.plz.plz));
		SqlString= _T("select plz from plz where vorwahl = ?");
		cursor_vorwahl = Plz.sqlcursor (SqlString.GetBuffer ());
	}
	found = FALSE;
	memcpy (&Plz.plz, &plz_null, sizeof (Plz.plz));
	_tcscpy (Plz.plz.vorwahl, vorwahl);
	Plz.sqlopen (cursor_vorwahl);
	if (Plz.sqlfetch (cursor_vorwahl) == 0)
	{
		found = TRUE;
		Plz.dbreadfirst ();
	}
	return Plz;
}

int CEnterPlz::GetComboIdx (LPTSTR value, std::vector<CString *> Values)
{
	int idx = -1;

	CString Value = value;
	Value.Trim ();

	int i = 0;
	for (std::vector<CString *>::iterator it = Values.begin (); it != Values.end (); ++it)
	{
		CString *vValue = *it;
		if (vValue->Find (Value) != -1)
		{
			idx = i;
			break;
		}
		i ++;
	}
	return idx;
}
