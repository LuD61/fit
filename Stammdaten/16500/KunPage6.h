#pragma once

#include "DbPropertyPage.h"
#include "resource.h"
#include "ListDropTarget.h"
#include "CtrlGrid.h"
#include "Kun.h"
#include "kun_erw.h"
#include "Mdn.h"
#include "Adr.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "FormTab.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "DbFormView.h"
#include "CodeProperties.h"
#include "mo_progcfg.h"
#include "sys_par.h"
#include "ptabn.h"
#include "iprgrstufk.h"
#include "i_kun_prk.h"
#include "kst.h"
#include "kun_tou.h"
#include "fil.h"
#include "gr_zuord.h"
#include "zahl_kond.h"
#include "rab_stufek.h"
#include "haus_bank.h"
#include "ls_txt.h"
#include "zuschlag.h"



// CKunPage6-Dialogfeld

class CKunPage6 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CKunPage6)

public:
	CKunPage6();
	virtual ~CKunPage6();

// Dialogfelddaten
	enum { IDD = IDD_KUNPAGE6 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_MESSAGE_MAP()

	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	PROG_CFG Cfg;

	virtual BOOL OnInitDialog();



	
	public:

	KUN_CLASS *Kun;
	KUN_ERW_CLASS *Kun_erw;
	ADR_CLASS *KunAdr;
	MDN_CLASS *Mdn;
	ADR_CLASS *MdnAdr;
	PTABN_CLASS *Ptabn;
	IPRGRSTUFK_CLASS *Iprgrstufk;
	I_KUN_PRK_CLASS *I_kun_prk;
	KST_CLASS *Kst;
	SYS_PAR_CLASS *Sys_par;
	KUN_TOU_CLASS *Kun_tou;
	FIL_CLASS *Fil;
	GR_ZUORD_CLASS *Gr_zuord;
	ZAHL_KOND_CLASS *Zahl_kond;
	RAB_STUFEK_CLASS *Rab_stufek;
	HAUS_BANK_CLASS *Haus_bank;
	LS_TXT_CLASS *Ls_txt;
	ZUSCHLAG_CLASS *Zuschlag;


	
	CFormTab Form;

	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid KunGrid;

		
	CListDropTarget dropTarget;

    CPropertySheet *KunProperty;
    DbFormView *View;
	CFrameWnd *Frame;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	void Register ();
	CEdit m_MdnName;
	CStatic m_LKun;
	CNumEdit m_Kun;
	CButton m_KunChoice;
	CEdit m_KunName;
	CStatic m_HeadBorder;
	CStatic m_DataBorder;
	CStatic m_LAnr;
	CComboBox m_Anr;
	CStatic m_LAdr1;
	CNumEdit m_Adr1;
		
    CVector HeadControls;
	BOOL ChoiceStat;
	CString Search;

	CCodeProperties Code;
    BOOL OnReturn ();
    BOOL OnKeyup ();
	void OnDelete ();
	void FillPtabCombo (CWnd *Control, LPTSTR Item);
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
    void EnableHeadControls (BOOL enable);
    BOOL StepBack ();
    virtual void OnCopy ();
    virtual void OnPaste ();
	BOOL Copy ();
	BOOL Paste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
    void EnableFields (BOOL b);
	void UpdatePage ();
	afx_msg void OnKillFocus (CWnd *);



public:
	CStatic m_DataBorder2;
public:
	int m_KunPage6;
};

