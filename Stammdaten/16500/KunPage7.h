#pragma once

#include "DbPropertyPage.h"
#include "resource.h"
#include "ListDropTarget.h"
#include "CtrlGrid.h"
#include "Kun.h"
#include "kun_erw.h"
#include "Mdn.h"
#include "Adr.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "FormTab.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "DbFormView.h"
#include "CodeProperties.h"
#include "mo_progcfg.h"
#include "sys_par.h"
#include "ptabn.h"
#include "iprgrstufk.h"
#include "i_kun_prk.h"
#include "kst.h"
#include "kun_tou.h"
#include "fil.h"
#include "gr_zuord.h"
#include "zahl_kond.h"
#include "rab_stufek.h"
#include "haus_bank.h"
#include "ls_txt.h"
#include "Arrow.h"
#include "H1.h"
#include "H2.h"
#include "H3.h"
#include "H4.h"
#include "hdaten.h"
#include "StaticButton.h"
#include "afxdtctl.h"

#define IDC_H4CHOICE 5001 

// CKunPage7-Dialogfeld

class CKunPage7 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CKunPage7)

public:
	CKunPage7();
	virtual ~CKunPage7();

// Dialogfelddaten
	enum { IDD = IDD_KUNPAGE7 };
private :
	BOOL TestJrChange;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_MESSAGE_MAP()

	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	PROG_CFG Cfg;

	virtual BOOL OnInitDialog();


	public:

	KUN_CLASS *Kun;
	KUN_ERW_CLASS *Kun_erw;
	ADR_CLASS *KunAdr;
	ADR_CLASS *RechAdr;
	MDN_CLASS *Mdn;
	ADR_CLASS *MdnAdr;
	PTABN_CLASS *Ptabn;
	IPRGRSTUFK_CLASS *Iprgrstufk;
	I_KUN_PRK_CLASS *I_kun_prk;
	KST_CLASS *Kst;
	SYS_PAR_CLASS *Sys_par;
	KUN_TOU_CLASS *Kun_tou;
	FIL_CLASS *Fil;
	GR_ZUORD_CLASS *Gr_zuord;
	ZAHL_KOND_CLASS *Zahl_kond;
	RAB_STUFEK_CLASS *Rab_stufek;
	HAUS_BANK_CLASS *Haus_bank;
	LS_TXT_CLASS *Ls_txt;
	H1_CLASS H1;
	H2_CLASS H2;
	H3_CLASS H3;
	H4_CLASS H4;
	HDATEN_CLASS Hdaten;
	
	CFormTab Form;

	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid KunGrid;
	CCtrlGrid H4Grid;
	CCtrlGrid HiGrid;
	CCtrlGrid HdatenGrid;
		
	CListDropTarget dropTarget;

    CPropertySheet *KunProperty;
    DbFormView *View;
	CFrameWnd *Frame;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	void Register ();
	CEdit m_MdnName;
	CStatic m_LKun;
	CNumEdit m_Kun;
	CButton m_KunChoice;
	CEdit m_KunName;
	CStatic m_HeadBorder;
	CStatic m_DataBorder;
	CStatic m_LAnr;
	CComboBox m_Anr;
	CStatic m_LAdr1;
	CNumEdit m_Adr1;
	CButton m_KopfLSChoice;
	CButton m_FussLSChoice;
	CButton m_KopfRechChoice;
	CButton m_FussRechChoice;
	
    CVector HeadControls;
	BOOL ChoiceStat;
	CString Search;


	CCodeProperties Code;
    BOOL OnReturn ();
    BOOL OnKeyup ();
	void OnDelete ();
	void FillRabSchl();
	void FillHausbank();
	void FillPtabCombo (CWnd *Control, LPTSTR Item);
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
    void EnableHeadControls (BOOL enable);
    BOOL StepBack ();
    virtual void OnCopy ();
    virtual void OnPaste ();
	BOOL Copy ();
	BOOL Paste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
    void EnableFields (BOOL b);
	void UpdatePage ();
	afx_msg void OnKillFocus (CWnd *);



public:
	CStatic m_DataBorder2;


public:
	CStatic m_DataBorder3;


public:
	int m_KunPage7;
public:
	CStatic m_LBank;
public:
	CTextEdit m_Bank;
public:
	CStatic m_LBlz;
public:
	CNumEdit m_Blz;
	CStatic m_LKto;
	CNumEdit m_Kto;
	CEdit m_SWIFT;
	CStatic m_LSWIFT;


public:
	CStatic m_LHausbank;
	CComboBox m_Hausbank;
	CNumEdit m_Steuer;
	CNumEdit m_Ust;
	CStatic m_LSteuer;
	CStatic m_LUst;
	CStatic m_LEG;
	CComboBox m_EG;
	CComboBox m_GP;
	CStatic m_LGP;
	CNumEdit m_StatKun;
	CStatic m_LStatKun;
	CStatic m_HierarchieGroup;
	CArrow m_H4Arrow;

public:
	CStatic m_LH4;
	CEdit m_H4;
	CButton m_H4Choice;
	CStatic m_LH3;
	CEdit m_H3;
	CArrow m_H3Arrow;
public:
	CStatic m_LH2;
	CEdit m_H2;
	CArrow m_H2Arrow;
	CStatic m_LH1;
	CEdit m_H1;
	CEdit m_H4Bez;
	CEdit m_H3Bez;
	CEdit m_H2Bez;
	CEdit m_H1Bez;

	CStatic m_HdatenGroup;
	CStatic m_LJahr;
	CSpinButtonCtrl m_SpinJahr;
	CNumEdit m_Jahr;
	CStatic m_LRv;
	CNumEdit m_Rv;
	CStatic m_LZr;
	CNumEdit m_Zr;

	CStatic m_LTeuroMonat;
	CEdit m_LM01;
	CNumEdit m_PlumsM01;
	CEdit m_LM02;
	CNumEdit m_PlumsM02;
	CEdit m_LM03;
	CNumEdit m_PlumsM03;
	CEdit m_LM04;
	CNumEdit m_PlumsM04;
	CEdit m_LM05;
	CNumEdit m_PlumsM05;
	CEdit m_LM06;
	CNumEdit m_PlumsM06;

	CEdit m_LM07;
	CNumEdit m_PlumsM07;
	CEdit m_LM08;
	CNumEdit m_PlumsM08;
	CEdit m_LM09;
	CNumEdit m_PlumsM09;
	CEdit m_LM10;
	CNumEdit m_PlumsM10;
	CEdit m_LM11;
	CNumEdit m_PlumsM11;
	CEdit m_LM12;
	CNumEdit m_PlumsM12;
	CStaticButton m_HDialog;

	void OnH4Choice ();
public:
	virtual BOOL Read ();
	BOOL TestH4 ();
	BOOL TestFields ();
	void ReadHdaten ();
public:
	afx_msg void OnEnChangeJahr();
	afx_msg void OnHdialog ();
	CEdit m_IBAN;
	CStatic m_LIBAN;
	afx_msg void OnStnClickedLswift();
	CStatic m_LMandatRef;
	CEdit m_MandatRef;
	CStatic m_LAnzTageSepa;
	CNumEdit m_AnzTageSepa;
	CComboBox m_SepaArt;
	CStatic m_LSepaArt;
	CStatic m_LMandatSeit;
	CEdit m_MandatSeit;
	CDateTimeCtrl m_MandSeit;
	CStatic m_LSBuch;
	CButton m_SBuch;
	afx_msg void OnBnClickedSbuch();
};
