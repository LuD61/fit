#include "stdafx.h"
#include "zuschlag.h"

struct ZUSCHLAG zuschlag, zuschlag_null, zuschlag_def;

void ZUSCHLAG_CLASS::prepare (void)
{
            TCHAR *sqltext;

 
            sqlin ((short *)   &zuschlag.mdn,  SQLSHORT, 0);           
	    sqlin ((long *)   &zuschlag.kun,  SQLLONG, 0);
    sqlout ((short *) &zuschlag.mdn,SQLSHORT,0);
    sqlout ((long *) &zuschlag.kun,SQLLONG,0);
    sqlout ((double *) &zuschlag.grenz_stu1,SQLDOUBLE,0);
    sqlout ((double *) &zuschlag.grenz_stu2,SQLDOUBLE,0);
    sqlout ((double *) &zuschlag.grenz_stu3,SQLDOUBLE,0);
    sqlout ((double *) &zuschlag.grenz_stu4,SQLDOUBLE,0);
    sqlout ((double *) &zuschlag.proz_stu1,SQLDOUBLE,0);
    sqlout ((double *) &zuschlag.proz_stu2,SQLDOUBLE,0);
    sqlout ((double *) &zuschlag.proz_stu3,SQLDOUBLE,0);
    sqlout ((double *) &zuschlag.proz_stu4,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select zuschlag.mdn,  "
"zuschlag.kun,  zuschlag.grenz_stu1,  zuschlag.grenz_stu2,  "
"zuschlag.grenz_stu3,  zuschlag.grenz_stu4,  zuschlag.proz_stu1,  "
"zuschlag.proz_stu2,  zuschlag.proz_stu3,  zuschlag.proz_stu4 from zuschlag ")

#line 14 "zuschlag.rpp"
					_T("where mdn = ? ")
					_T("and kun = ? "));                                 

            
    sqlin ((short *) &zuschlag.mdn,SQLSHORT,0);
    sqlin ((long *) &zuschlag.kun,SQLLONG,0);
    sqlin ((double *) &zuschlag.grenz_stu1,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.grenz_stu2,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.grenz_stu3,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.grenz_stu4,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.proz_stu1,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.proz_stu2,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.proz_stu3,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.proz_stu4,SQLDOUBLE,0);
            sqltext = _T("update zuschlag set "
"zuschlag.mdn = ?,  zuschlag.kun = ?,  zuschlag.grenz_stu1 = ?,  "
"zuschlag.grenz_stu2 = ?,  zuschlag.grenz_stu3 = ?,  "
"zuschlag.grenz_stu4 = ?,  zuschlag.proz_stu1 = ?,  "
"zuschlag.proz_stu2 = ?,  zuschlag.proz_stu3 = ?,  "
"zuschlag.proz_stu4 = ? ")

#line 19 "zuschlag.rpp"
					_T("where mdn = ? ")
					_T("and kun = ? ");
           
	    sqlin ((short *)   &zuschlag.mdn,  SQLSHORT, 0);           
	    sqlin ((long *)   &zuschlag.kun,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);
            
	    sqlin ((short *)   &zuschlag.mdn,  SQLSHORT, 0);           
	    sqlin ((long *)   &zuschlag.kun,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from zuschlag ")
					_T("where mdn = ? ")
					_T("and kun = ? "));                                  

    sqlin ((short *) &zuschlag.mdn,SQLSHORT,0);
    sqlin ((long *) &zuschlag.kun,SQLLONG,0);
    sqlin ((double *) &zuschlag.grenz_stu1,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.grenz_stu2,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.grenz_stu3,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.grenz_stu4,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.proz_stu1,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.proz_stu2,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.proz_stu3,SQLDOUBLE,0);
    sqlin ((double *) &zuschlag.proz_stu4,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into zuschlag ("
"mdn,  kun,  grenz_stu1,  grenz_stu2,  grenz_stu3,  grenz_stu4,  proz_stu1,  "
"proz_stu2,  proz_stu3,  proz_stu4) ")

#line 33 "zuschlag.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,"
"?,?)"));

#line 35 "zuschlag.rpp"
}
