// KunPage2.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "KunPage2.h" 
#include "DbUniCode.h"
#include "UniFormField.h" 
#include "StrFuncs.h"
#include "token.h"
#include "ptabn.h"
#include "gr_zuord.h"
#include "fil.h"
#include "FrachtKosten.h"
#include "zuschlag.h"
#include "Process.h"
#include "ChoiceTour.h"


// CKunPage2-Dialogfeld

IMPLEMENT_DYNAMIC(CKunPage2, CDbPropertyPage)

CKunPage2::CKunPage2()
	: CDbPropertyPage(CKunPage2::IDD)
{
	hBrush = NULL;
	hBrushStatic = NULL;
	ChoiceAdr = NULL;
	ModalChoiceAdr = TRUE;
	Search = _T("");

	CUniFormField::Code = &Code;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Kun);
	IncludeLand = TRUE;
}

CKunPage2::~CKunPage2()
{
	//Kun.dbclose ();
	//KunAdr.dbclose ();
	//Mdn.dbclose ();
	//MdnAdr.dbclose ();
	//Ptabn.dbclose ();


	
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
}

void CKunPage2::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_KUNNAME, m_KunName);
	DDX_Control(pDX, IDC_HEADBORDER, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDER, m_DataBorder);
	DDX_Control(pDX, IDC_LANR, m_LAnr);
	DDX_Control(pDX, IDC_ANR, m_Anr);
	DDX_Control(pDX, IDC_LADR1, m_LAdr1);
	DDX_Control(pDX, IDC_ADR, m_Adr1);
	DDX_Control(pDX, IDC_KUN_LIEF, m_KunLief);
	DDX_Control(pDX, IDC_LNAME1, m_LName1);
	DDX_Control(pDX, IDC_NAME, m_Name1);
	DDX_Control(pDX, IDC_NAME2, m_Name2);
	DDX_Control(pDX, IDC_LNAME2, m_LName2);
	DDX_Control(pDX, IDC_NAME3, m_Name3);
	DDX_Control(pDX, IDC_LNAME3, m_LName3);
	DDX_Control(pDX, IDC_STRASSE, m_Strasse);
	DDX_Control(pDX, IDC_LSTRASSE, m_LStrasse);
	DDX_Control(pDX, IDC_PLZ, m_Plz);
	DDX_Control(pDX, IDC_LPLZ, m_LPlz);
	DDX_Control(pDX, IDC_ORT, m_Ort);
	DDX_Control(pDX, IDC_LORT, m_LOrt);
	DDX_Control(pDX, IDC_LPOSTFACH, m_LPostfach);
	DDX_Control(pDX, IDC_POSTFACH, m_Postfach);
	DDX_Control(pDX, IDC_TELEFON, m_Telefon);
	DDX_Control(pDX, IDC_LTELEFON, m_LTelefon);
	DDX_Control(pDX, IDC_FAX, m_Fax);
	DDX_Control(pDX, IDC_POSTFACH2, m_plzPostfach);
	DDX_Control(pDX, IDC_EMAIL, m_EMAIL);
	DDX_Control(pDX, IDC_LEMAIL, m_LEmail);
	DDX_Control(pDX, IDC_PARTNER, m_Partner);
	DDX_Control(pDX, IDC_LPARTNER, m_LPartner);
	DDX_Control(pDX, IDC_STAAT, m_Staat);
	DDX_Control(pDX, IDC_LSTAAT, m_LStaat);
	DDX_Control(pDX, IDC_LAND, m_Land);
	DDX_Control(pDX, IDC_INCLUDE_LAND, m_IncludeLand);
	DDX_Control(pDX, IDC_LLAND, m_LLand);
	DDX_Control(pDX, IDC_LKUNKRZ1, m_LKunKrz1);
	DDX_Control(pDX, IDC_KUNKRZ1, m_KunKrz1);
	DDX_Control(pDX, IDC_DATABORDER2, m_DataBorder2);
	DDX_Control(pDX, IDC_MODEM, m_Modem);
	DDX_Control(pDX, IDC_LMODEM, m_LModem);
	DDX_Control(pDX, IDC_LIEFSTOP, m_LiefStop);
	DDX_Control(pDX, IDC_LLIEFSTOP, m_LLiefStop);
	DDX_Control(pDX, IDC_ILN, m_Iln);
	DDX_Control(pDX, IDC_LILN, m_LIln);
	DDX_Control(pDX, IDC_ILN2, m_Iln2);
	DDX_Control(pDX, IDC_LILN2, m_LIln2);
	DDX_Control(pDX, IDC_LBB, m_LBbn);
	DDX_Control(pDX, IDC_BBN, m_Bbn);
	DDX_Control(pDX, IDC_BBS, m_Bbs);
	DDX_Control(pDX, IDC_LUMSATZ, m_LUmsatz);
	DDX_Control(pDX, IDC_UMSATZ, m_Umsatz);
	DDX_Control(pDX, IDC_LTOUR1, m_LTour1);
	DDX_Control(pDX, IDC_TOUR1, m_Tour1);
	DDX_Control(pDX, IDC_TOUR2, m_Tour2);
	DDX_Control(pDX, IDC_LTOUR2, m_LTour2);
	DDX_Control(pDX, IDC_TOUR1BEZ, m_Tour1Bez);
	DDX_Control(pDX, IDC_TOUR2BEZ, m_Tour2Bez);
	DDX_Control(pDX, IDC_VERSAND, m_Versand);
	DDX_Control(pDX, IDC_LVERSAND, m_LVersand);
	DDX_Control(pDX, IDC_MARKT, m_Markt);
	DDX_Control(pDX, IDC_LMARKT, m_LMarkt);
	DDX_Control(pDX, IDC_FRACHT, m_FrachtGew);
	DDX_Control(pDX, IDC_LFRACHT, m_LFrachtGew);
	DDX_Control(pDX, IDC_FILKZ, m_FilKZ);
	DDX_Control(pDX, IDC_LFILKZ, m_LFilKZ);
	DDX_Control(pDX, IDC_LFILGR, m_LFilGr);
	DDX_Control(pDX, IDC_FILGR, m_FilGr);
	DDX_Control(pDX, IDC_LFILNR, m_LFilNr);
	DDX_Control(pDX, IDC_FILNR, m_FilNr);
	DDX_Control(pDX, IDC_LSHOP_KZ, m_LShop_kz);
	DDX_Control(pDX, IDC_SHOP_KZ, m_Shop_kz);
	DDX_Control(pDX, IDC_SHOP_PASSW, m_Shop_passw);
	DDX_Control(pDX, IDC_LSHOP_PASSW, m_LShop_passw);
	DDX_Control(pDX, IDC_DATABORDER_INET, m_Databorder_inet);
	DDX_Control(pDX, IDC_LSHOP_HISTORY, m_LShop_History);
	DDX_Control(pDX, IDC_SHOP_HISTORY, m_Shop_History);
	DDX_Control(pDX, IDC_LSHOP_HISTORY, m_LShop_History);
	DDX_Control(pDX, IDC_LMINBEST, m_LMinBest);
	DDX_Control(pDX, IDC_MINBEST, m_MinBest);
	DDX_Control(pDX, IDC_PALH, m_PalH);
	DDX_Control(pDX, IDC_LPALH, m_LPalH);
	DDX_Control(pDX, IDC_PARTNER2, m_partner2);
	DDX_Control(pDX, IDC_LPARTNER2, m_LPartner2);
	DDX_Control(pDX, IDC_LPALGEWDD, m_LMaxGewDD);
	DDX_Control(pDX, IDC_LPALGEWEU, m_LMaxGewEU);
	DDX_Control(pDX, IDC_PALGEWDD, m_MaxGewDD);
	DDX_Control(pDX, IDC_PALGEWEU, m_MaxGewEU);
}


BEGIN_MESSAGE_MAP(CKunPage2, CDbPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_KILLFOCUS ()
	ON_BN_CLICKED(IDC_ADR2CHOICE , OnAdr2Choice)
	ON_BN_CLICKED(IDC_PLZ2CHOICE , OnPlz2Choice)
	ON_BN_CLICKED(IDC_TOURCHOICE1 , OnTourChoice1)
	ON_BN_CLICKED(IDC_TOURCHOICE2 , OnTourChoice2)
	ON_COMMAND (CANCELED, OnAdrCanceled)
	ON_COMMAND (SELECTED, OnAdrSelected)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_CBN_SELCHANGE(IDC_VERSAND, &CKunPage2::OnCbnSelchangeVersand)
	ON_CBN_SELCHANGE(IDC_FILGR, &CKunPage2::OnCbnSelchangeFilgr)
	ON_BN_CLICKED(IDC_FILKZ, &CKunPage2::OnBnClickedFilkz)
	ON_BN_CLICKED(IDC_LIEFSTOP, &CKunPage2::OnBnClickedLiefstop)
	ON_CBN_SELCHANGE(IDC_FRACHT, &CKunPage2::OnCbnSelchangeFracht)
	ON_COMMAND(IDC_KUN_LIEF, OnKunLief)
	ON_EN_CHANGE(IDC_KUNKRZ1, &CKunPage2::OnEnChangeKunkrz1)
	ON_BN_CLICKED(IDC_SHOP_KZ, &CKunPage2::OnBnClickedShopKz)
	ON_BN_CLICKED(IDC_SHOP_HISTORY, &CKunPage2::OnBnClickedShopHistory)
	ON_BN_CLICKED(IDC_MINBEST, &CKunPage2::OnBnClickedMinbest)
END_MESSAGE_MAP()


// CKunPage2-Meldungshandler

void CKunPage2::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CKunPage2::OnInitDialog()
{
	CDialog::OnInitDialog();
//	dlg.Construct (_T(""), this);

	F5Pressed = FALSE;
	Kun->sqlin ((long *)&tou, SQLLONG, 0); 
	Kun->sqlout ((char *)&tou_bz, SQLCHAR, sizeof (tou_bz)); 
	cursor_tour = Kun->sqlcursor (_T("select tou_bz from tou where tou = ?"));
	KunProperty = (CPropertySheet *) GetParent ();
	View = (DbFormView *) KunProperty->GetParent ();
	Frame = (CFrameWnd *) View->GetParent ();

	m_KunLief.nID = IDC_KUN_LIEF;
	m_KunLief.SetWindowText (_T("weitere Adressen"));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();

	CRect bRect (0, 0, 100, 20);;

	Form.Add (new CFormField (&m_Mdn,EDIT,        (short *)  &Mdn->mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *)   MdnAdr->adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Kun,EDIT,        (long *)   &Kun->kun.kun, VLONG));
	Form.Add (new CUniFormField (&m_KunName,EDIT, (char *)   Kun->kun.kun_krz2, VCHAR));
	Form.Add (new CFormField (&m_Adr1,EDIT,        (long *)  &Kun->kun.adr2, VLONG));
	Form.Add (new CFormField (&m_Anr,COMBOBOX,    (short *)  &LiefAdr->adr.anr, VSHORT));
	Form.Add (new CFormField (&m_Name1,EDIT,      (char *)  LiefAdr->adr.adr_nam1, VCHAR));
	Form.Add (new CFormField (&m_Name2,EDIT,      (char *)  LiefAdr->adr.adr_nam2, VCHAR));
	Form.Add (new CFormField (&m_Name3,EDIT,      (char *)  LiefAdr->adr.adr_nam3, VCHAR));
	Form.Add (new CFormField (&m_Strasse,EDIT,      (char *)  LiefAdr->adr.str, VCHAR));
	Form.Add (new CFormField (&m_Postfach,EDIT,      (char *)  LiefAdr->adr.pf, VCHAR));
	Form.Add (new CFormField (&m_plzPostfach,EDIT,      (char *)  LiefAdr->adr.plz_pf, VCHAR));
	Form.Add (new CFormField (&m_Plz,EDIT,      (char *)  LiefAdr->adr.plz, VCHAR));
	Form.Add (new CFormField (&m_Ort,EDIT,      (char *)  LiefAdr->adr.ort1, VCHAR));
	Form.Add (new CFormField (&m_Telefon,EDIT,      (char *)  LiefAdr->adr.tel, VCHAR));
	Form.Add (new CFormField (&m_Fax,EDIT,      (char *)  LiefAdr->adr.fax, VCHAR));
	Form.Add (new CFormField (&m_EMAIL,EDIT,      (char *)  LiefAdr->adr.email, VCHAR));
	Form.Add (new CFormField (&m_Partner,EDIT,      (char *)  LiefAdr->adr.partner, VCHAR));
	Form.Add (new CFormField (&m_partner2,EDIT,      (char *)  LiefAdr->adr.spartner, VCHAR));
	Form.Add (new CFormField (&m_Staat,COMBOBOX,    (short *)  &LiefAdr->adr.staat, VSHORT));
	Form.Add (new CFormField (&m_Land,COMBOBOX,    (short *)  &LiefAdr->adr.land, VSHORT));
	Form.Add (new CFormField (&m_IncludeLand,CHECKBOX,    (short *)  &IncludeLand, VSHORT));
	Form.Add (new CFormField (&m_KunKrz1,EDIT,      (char *)  Kun->kun.kun_krz2 , VCHAR));
	Form.Add (new CFormField (&m_Modem,EDIT,      (char *)  LiefAdr->adr.mobil , VCHAR));
	Form.Add (new CFormField (&m_LiefStop,CHECKBOX,      (short *)  &Kun->kun.kun_of_lf , VSHORT));
	Form.Add (new CFormField (&m_MinBest,CHECKBOX,      (short *)  &Kun->kun.min_best , VSHORT));
	Form.Add (new CFormField (&m_Iln,EDIT,      (char *)  Kun->kun.iln , VCHAR));
	Form.Add (new CFormField (&m_Iln2,EDIT,      (char *)  Kun->kun.plattform , VCHAR));
	Form.Add (new CFormField (&m_Bbn,EDIT,      (long *)  &Kun->kun.bbn , VLONG));
	Form.Add (new CFormField (&m_Bbs,EDIT,      (char *)  Kun->kun.bbs , VCHAR));
	Form.Add (new CFormField (&m_Umsatz,EDIT,      (double *)  &Kun->kun.jr_plan_ums , VDOUBLE, 13, 2));
	Form.Add (new CFormField (&m_Tour1,EDIT,      (long *)  &Kun_tou->kun_tou.tou , VLONG));
	Form.Add (new CFormField (&m_Tour2,EDIT,      (long *)  &Kun_tou->kun_tou.tou2 , VLONG));
	Form.Add (new CFormField (&m_Tour2Bez,EDIT,      (char *)  Kun_tou->kun_tou.tou_bz2 , VCHAR));
	Form.Add (new CFormField (&m_Tour1Bez,EDIT,      (char *)  Kun_tou->kun_tou.tou_bz , VCHAR));
	Form.Add (new CFormField (&m_Versand,COMBOBOX,      (char *)  Kun->kun.vers_art , VCHAR));
	Form.Add (new CFormField (&m_Markt,COMBOBOX,      (char *)  Kun->kun.be_log , VCHAR));
	Form.Add (new CFormField (&m_FrachtGew,COMBOBOX,      (short *)  &Kun->kun.lief_art , VSHORT));
	Form.Add (new CFormField (&m_FilKZ,CHECKBOX,      (char *)  Kun->kun.ueb_kz , VCHAR));
	Form.Add (new CFormField (&m_FilGr,COMBOBOX,      (short *)  &Kun->kun.sw_fil_gr , VSHORT));
	Form.Add (new CFormField (&m_FilNr,COMBOBOX,      (short *)  &Kun->kun.sw_fil , VSHORT));
	Form.Add (new CFormField (&m_Shop_kz,CHECKBOX,      (char *)  Kun->kun.shop_kz , VCHAR));	
	Form.Add (new CFormField (&m_Shop_passw,EDIT,      (char *)  Kun->kun.shop_passw , VCHAR));
	Form.Add (new CFormField (&m_Shop_History,CHECKBOX,      (char *)  Kun->kun.shop_history , VCHAR));
	Form.Add (new CFormField (&m_PalH,EDIT,      (short *)  &Kun_erw->kun_erw.hoehe_pal , VSHORT));
	Form.Add (new CFormField (&m_MaxGewDD,EDIT,      (double *)  &Kun_erw->kun_erw.max_gew_dd , VDOUBLE, 8, 3));
	Form.Add (new CFormField (&m_MaxGewEU,EDIT,      (double *)  &Kun_erw->kun_erw.max_gew_eu , VDOUBLE, 8, 3));

	FillPtabCombo (&m_Anr, _T("anr"));
	FillPtabCombo (&m_Staat, _T("staat"));
	FillPtabCombo (&m_Land, _T("land"));
	FillPtabCombo (&m_Versand, _T("vers_art"));
	FillPtabCombo (&m_Markt, _T("be_log"));
	FillPtabCombo (&m_FrachtGew, _T("lief_art"));
	FillGrZuord();
	FillFil ();
	
	CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 1, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	

	KunGrid.Create (this, 1, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	

	Adr1Grid.Create (this, 1, 4);
    Adr1Grid.SetBorder (0, 0);
    Adr1Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_Adr1 = new CCtrlInfo (&m_Adr1, 0, 0, 1, 1);
	Adr1Grid.Add (c_Adr1);
	CtrlGrid.CreateChoiceButton (m_Adr1Choice, IDC_ADR2CHOICE, this);
	CCtrlInfo *c_Adr1Choice = new CCtrlInfo (&m_Adr1Choice, 1, 0, 1, 1);
	Adr1Grid.Add (c_Adr1Choice);
	CCtrlInfo *c_KunLief = new CCtrlInfo (&m_KunLief, 3, 0, 1, 1);
	c_KunLief->SetCellPos (110, 0);
	Adr1Grid.Add (c_KunLief);

	Plz1Grid.Create (this, 1, 3);
    Plz1Grid.SetBorder (0, 0);
    Plz1Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_Plz = new CCtrlInfo (&m_Plz, 0, 0, 1, 1);
	Plz1Grid.Add (c_Plz);
	CtrlGrid.CreateChoiceButton (m_Plz2Choice, IDC_PLZ2CHOICE, this);
	CCtrlInfo *c_Plz2Choice = new CCtrlInfo (&m_Plz2Choice, 1, 0, 1, 1);
	Plz1Grid.Add (c_Plz2Choice);
	CCtrlInfo *c_IncludeLand = new CCtrlInfo (&m_IncludeLand, 2, 0, 1, 1);
	c_IncludeLand->SetCellPos (5, 0);
	Plz1Grid.Add (c_IncludeLand);
	
	TelFaxGrid.Create (this, 1, 2);
    TelFaxGrid.SetBorder (0, 0);
    TelFaxGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Telefon = new CCtrlInfo (&m_Telefon, 0, 0, 1, 1);
	TelFaxGrid.Add (c_Telefon);
	CCtrlInfo *c_Fax = new CCtrlInfo (&m_Fax, 1, 0, 1, 1);
	TelFaxGrid.Add (c_Fax);

	PLZPostfachGrid.Create (this, 1, 2);
    PLZPostfachGrid.SetBorder (0, 0);
    PLZPostfachGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Postfach = new CCtrlInfo (&m_Postfach, 0, 0, 1, 1);
	PLZPostfachGrid.Add (c_Postfach);
	CCtrlInfo *c_plzPostfach = new CCtrlInfo (&m_plzPostfach, 1, 0, 1, 1);
	PLZPostfachGrid.Add (c_plzPostfach);

	BbnGrid.Create (this, 1, 2);
    BbnGrid.SetBorder (0, 0);
    BbnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Bbn = new CCtrlInfo (&m_Bbn, 0, 0, 1, 1);
	BbnGrid.Add (c_Bbn);
	CCtrlInfo *c_Bbs = new CCtrlInfo (&m_Bbs, 1, 0, 1, 1);
	BbnGrid.Add (c_Bbs);

	TourGrid.Create (this, 1, 3);
    TourGrid.SetBorder (0, 0);
    TourGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Tour1 = new CCtrlInfo (&m_Tour1, 0, 0, 1, 1);
	TourGrid.Add (c_Tour1);
	CtrlGrid.CreateChoiceButton (m_TourChoice1, IDC_TOURCHOICE1, this);
	CCtrlInfo *c_TourChoice1 = new CCtrlInfo (&m_TourChoice1, 1, 0, 1, 1);
	TourGrid.Add (c_TourChoice1);
	CCtrlInfo *c_Tour1Bez = new CCtrlInfo (&m_Tour1Bez, 2, 0, 1, 1);
	c_Tour1Bez->SetCellPos (5, 0);
	TourGrid.Add (c_Tour1Bez);

	Tour2Grid.Create (this, 1, 3);
    Tour2Grid.SetBorder (0, 0);
    Tour2Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_Tour2 = new CCtrlInfo (&m_Tour2, 0, 0, 1, 1);
	Tour2Grid.Add (c_Tour2);
	CtrlGrid.CreateChoiceButton (m_TourChoice2, IDC_TOURCHOICE2, this);
	CCtrlInfo *c_TourChoice2 = new CCtrlInfo (&m_TourChoice2, 1, 0, 1, 1);
	Tour2Grid.Add (c_TourChoice2);
	CCtrlInfo *c_Tour2Bez = new CCtrlInfo (&m_Tour2Bez, 2, 0, 1, 1);
	c_Tour2Bez->SetCellPos (5, 0);
	Tour2Grid.Add (c_Tour2Bez);


	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 4);
    c_HeadBorder->rightspace = 20; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 3, 1, 4, 1);
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LKun = new CCtrlInfo (&m_LKun, 1, 2, 1, 1);
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid = new CCtrlInfo (&KunGrid, 2, 2, 1, 1);
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 3, 2, 4, 1);
	CtrlGrid.Add (c_KunName);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 4, 3, DOCKBOTTOM);
	CtrlGrid.Add (c_DataBorder);

	CCtrlInfo *c_DataBorder2 = new CCtrlInfo (&m_DataBorder2, 3, 4, DOCKRIGHT, DOCKBOTTOM);
    c_DataBorder2->rightspace = 20;
	CtrlGrid.Add (c_DataBorder2);


	CCtrlInfo *c_LAdr1 = new CCtrlInfo (&m_LAdr1, 1, 5, 1, 1);
	CtrlGrid.Add (c_LAdr1);
	CCtrlInfo *c_Adr1Grid = new CCtrlInfo (&Adr1Grid, 2, 5, 1, 1);
	CtrlGrid.Add (c_Adr1Grid);
	CCtrlInfo *c_LAnr = new CCtrlInfo (&m_LAnr, 1, 6, 1, 1);
	CtrlGrid.Add (c_LAnr);
	CCtrlInfo *c_Anr = new CCtrlInfo (&m_Anr, 2, 6, 1, 1);
	CtrlGrid.Add (c_Anr);
	CCtrlInfo *c_LName1 = new CCtrlInfo (&m_LName1, 1, 7, 1, 1);
	CtrlGrid.Add (c_LName1);
	CCtrlInfo *c_Name1 = new CCtrlInfo (&m_Name1, 2, 7, 1, 1);
	CtrlGrid.Add (c_Name1);

	CCtrlInfo *c_LName2 = new CCtrlInfo (&m_LName2, 1, 8, 1, 1);
	CtrlGrid.Add (c_LName2);
	CCtrlInfo *c_Name2 = new CCtrlInfo (&m_Name2, 2, 8, 1, 1);
	CtrlGrid.Add (c_Name2);

	CCtrlInfo *c_LName3 = new CCtrlInfo (&m_LName3, 1, 9, 1, 1);
	CtrlGrid.Add (c_LName3);
	CCtrlInfo *c_Name3 = new CCtrlInfo (&m_Name3, 2, 9, 1, 1);
	CtrlGrid.Add (c_Name3);

	CCtrlInfo *c_LStrasse = new CCtrlInfo (&m_LStrasse, 1, 10, 1, 1);
	CtrlGrid.Add (c_LStrasse);
	CCtrlInfo *c_Strasse = new CCtrlInfo (&m_Strasse, 2, 10, 1, 1);
	CtrlGrid.Add (c_Strasse);

	CCtrlInfo *c_LPlz = new CCtrlInfo (&m_LPlz, 1, 12, 1, 1);
	CtrlGrid.Add (c_LPlz);
	CCtrlInfo *c_Plz1Grid = new CCtrlInfo (&Plz1Grid, 2, 12, 1, 1);
	CtrlGrid.Add (c_Plz1Grid);
//	CCtrlInfo *c_Plz = new CCtrlInfo (&m_Plz, 2, 12, 1, 1);
//	CtrlGrid.Add (c_Plz);
	
	CCtrlInfo *c_LPostfach = new CCtrlInfo (&m_LPostfach, 1, 11, 1, 1);
	CtrlGrid.Add (c_LPostfach);
	CCtrlInfo *c_PLZPostfachGrid = new CCtrlInfo (&PLZPostfachGrid, 2, 11, 1, 1);
	CtrlGrid.Add (c_PLZPostfachGrid);

	CCtrlInfo *c_LOrt = new CCtrlInfo (&m_LOrt, 1, 13, 1, 1);
	CtrlGrid.Add (c_LOrt);
	CCtrlInfo *c_Ort = new CCtrlInfo (&m_Ort, 2, 13, 1, 1);
	CtrlGrid.Add (c_Ort);

	CCtrlInfo *c_LTelefon = new CCtrlInfo (&m_LTelefon, 1, 15, 1, 1);
	CtrlGrid.Add (c_LTelefon);
	CCtrlInfo *c_TelFaxGrid = new CCtrlInfo (&TelFaxGrid, 2, 15, 1, 1);
	CtrlGrid.Add (c_TelFaxGrid);
	
	CCtrlInfo *c_LEmail = new CCtrlInfo (&m_LEmail, 1, 16, 1, 1);
	CtrlGrid.Add (c_LEmail);
	CCtrlInfo *c_EMAIL = new CCtrlInfo (&m_EMAIL, 2, 16, 1, 1);
	CtrlGrid.Add (c_EMAIL);

	CCtrlInfo *c_LModem = new CCtrlInfo (&m_LModem, 1, 17, 1, 1);
	CtrlGrid.Add (c_LModem);
	CCtrlInfo *c_Modem = new CCtrlInfo (&m_Modem, 2, 17, 1, 1);
	CtrlGrid.Add (c_Modem);

	CCtrlInfo *c_LPartner = new CCtrlInfo (&m_LPartner, 1, 18, 1, 1);
	CtrlGrid.Add (c_LPartner);
	CCtrlInfo *c_Partner = new CCtrlInfo (&m_Partner, 2, 18, 1, 1);
	CtrlGrid.Add (c_Partner);

	CCtrlInfo *c_LPartner2 = new CCtrlInfo (&m_LPartner2, 1, 19, 1, 1);
	CtrlGrid.Add (c_LPartner2);
	CCtrlInfo *c_Partner2 = new CCtrlInfo (&m_partner2, 2, 19, 1, 1);
	CtrlGrid.Add (c_Partner2);

	CCtrlInfo *c_LStaat = new CCtrlInfo (&m_LStaat, 1, 20, 1, 1);
	CtrlGrid.Add (c_LStaat);
	CCtrlInfo *c_Staat = new CCtrlInfo (&m_Staat, 2, 20, 1, 1);
	CtrlGrid.Add (c_Staat);

	CCtrlInfo *c_LLand = new CCtrlInfo (&m_LLand, 1, 21, 1, 1);
	CtrlGrid.Add (c_LLand);
	CCtrlInfo *c_Land = new CCtrlInfo (&m_Land, 2, 21, 1, 1);
	CtrlGrid.Add (c_Land);


	CCtrlInfo *c_LKunKrz1 = new CCtrlInfo (&m_LKunKrz1, 4, 5, 1, 1);
	CtrlGrid.Add (c_LKunKrz1);
	CCtrlInfo *c_KunKrz1 = new CCtrlInfo (&m_KunKrz1, 5, 5, 1, 1);
	c_KunKrz1->rightspace = 40;
	CtrlGrid.Add (c_KunKrz1);

	CCtrlInfo *c_LMinBest = new CCtrlInfo (&m_LMinBest, 4, 6, 1, 1);
	CtrlGrid.Add (c_LMinBest);
	CCtrlInfo *c_MinBest = new CCtrlInfo (&m_MinBest, 5, 6, 1, 1);
	CtrlGrid.Add (c_MinBest);


	CCtrlInfo *c_LLiefStop = new CCtrlInfo (&m_LLiefStop, 4, 7, 1, 1);
	CtrlGrid.Add (c_LLiefStop);
	CCtrlInfo *c_LiefStop = new CCtrlInfo (&m_LiefStop, 5, 7, 1, 1);
	c_LiefStop->rightspace = 40;
	CtrlGrid.Add (c_LiefStop);

	CCtrlInfo *c_LIln = new CCtrlInfo (&m_LIln, 4, 8, 1, 1);
	CtrlGrid.Add (c_LIln);
	CCtrlInfo *c_Iln = new CCtrlInfo (&m_Iln, 5, 8, 1, 1);
	c_Iln->rightspace = 40;
	CtrlGrid.Add (c_Iln);

	CCtrlInfo *c_LIln2 = new CCtrlInfo (&m_LIln2, 4, 9, 1, 1);
	CtrlGrid.Add (c_LIln2);
	CCtrlInfo *c_Iln2 = new CCtrlInfo (&m_Iln2, 5, 9, 1, 1);
	c_Iln2->rightspace = 40;
	CtrlGrid.Add (c_Iln2);

	CCtrlInfo *c_LBbn = new CCtrlInfo (&m_LBbn, 4, 10, 1, 1);
	CtrlGrid.Add (c_LBbn);
	CCtrlInfo *c_BbnGrid = new CCtrlInfo (&BbnGrid, 5, 10, 1, 1);
	CtrlGrid.Add (c_BbnGrid);

	CCtrlInfo *c_LUmsatz = new CCtrlInfo (&m_LUmsatz, 4, 11, 1, 1);
	CtrlGrid.Add (c_LUmsatz);
	CCtrlInfo *c_Umsatz = new CCtrlInfo (&m_Umsatz, 5, 11, 1, 1);
	c_Umsatz->rightspace = 40;
	CtrlGrid.Add (c_Umsatz);

	CCtrlInfo *c_LTour1 = new CCtrlInfo (&m_LTour1, 4, 12, 1, 1);
	CtrlGrid.Add (c_LTour1);
	CCtrlInfo *c_TourGrid = new CCtrlInfo (&TourGrid, 5, 12, 1, 1);
	CtrlGrid.Add (c_TourGrid);

	CCtrlInfo *c_LTour2 = new CCtrlInfo (&m_LTour2, 4, 13, 1, 1);
	CtrlGrid.Add (c_LTour2);
	CCtrlInfo *c_Tour2Grid = new CCtrlInfo (&Tour2Grid, 5, 13, 1, 1);
	CtrlGrid.Add (c_Tour2Grid);

	CCtrlInfo *c_LVersand = new CCtrlInfo (&m_LVersand, 4, 14, 1, 1);
	CtrlGrid.Add (c_LVersand);
	CCtrlInfo *c_Versand = new CCtrlInfo (&m_Versand, 5, 14, 1, 1);
	c_Versand->rightspace = 40;
	CtrlGrid.Add (c_Versand);

	CCtrlInfo *c_LMarkt = new CCtrlInfo (&m_LMarkt, 4, 15, 1, 1);
	CtrlGrid.Add (c_LMarkt);
	CCtrlInfo *c_Markt = new CCtrlInfo (&m_Markt, 5, 15, 1, 1);
	c_Markt->rightspace = 40;
	CtrlGrid.Add (c_Markt);

	CCtrlInfo *c_LFrachtGew = new CCtrlInfo (&m_LFrachtGew, 4, 16, 1, 1);
	CtrlGrid.Add (c_LFrachtGew);
	CCtrlInfo *c_FrachtGew = new CCtrlInfo (&m_FrachtGew, 5, 16, 1, 1);
	c_FrachtGew->rightspace = 40;
	CtrlGrid.Add (c_FrachtGew);

	CCtrlInfo *c_LHoehePal = new CCtrlInfo (&m_LPalH, 4, 17, 1, 1);
	CtrlGrid.Add (c_LHoehePal);
	CCtrlInfo *c_PalH = new CCtrlInfo (&m_PalH, 5, 17, 1, 1);
	c_PalH->rightspace = 40;
	CtrlGrid.Add (c_PalH);

	CCtrlInfo *c_LMaxGewEU = new CCtrlInfo (&m_LMaxGewEU, 4, 18, 1, 1);
	CtrlGrid.Add (c_LMaxGewEU);
	CCtrlInfo *c_MaxGewEU = new CCtrlInfo (&m_MaxGewEU, 5, 18, 1, 1);
	c_MaxGewEU->rightspace = 40;
	CtrlGrid.Add (c_MaxGewEU);

	CCtrlInfo *c_LMaxGewDD = new CCtrlInfo (&m_LMaxGewDD, 4, 19, 1, 1);
	CtrlGrid.Add (c_LMaxGewDD);
	CCtrlInfo *c_MaxGewDD = new CCtrlInfo (&m_MaxGewDD, 5, 19, 1, 1);
	c_MaxGewEU->rightspace = 40;
	CtrlGrid.Add (c_MaxGewDD);

	CCtrlInfo *c_LFilKZ = new CCtrlInfo (&m_LFilKZ, 4, 20, 1, 1);
	CtrlGrid.Add (c_LFilKZ);
	CCtrlInfo *c_FilKZ = new CCtrlInfo (&m_FilKZ, 5, 20, 1, 1);
	c_FilKZ->rightspace = 40;
	CtrlGrid.Add (c_FilKZ);

	CCtrlInfo *c_LFilGr = new CCtrlInfo (&m_LFilGr, 4, 21, 1, 1);
	CtrlGrid.Add (c_LFilGr);
	CCtrlInfo *c_FilGr = new CCtrlInfo (&m_FilGr, 5, 21, 1, 1);
	c_FilGr->rightspace = 40;
	CtrlGrid.Add (c_FilGr);

	CCtrlInfo *c_LFilNr = new CCtrlInfo (&m_LFilNr, 4, 22, 1, 1);
	CtrlGrid.Add (c_LFilNr);
	CCtrlInfo *c_FilNr = new CCtrlInfo (&m_FilNr, 5, 22, 1, 1);
	c_FilNr->rightspace = 40;
	CtrlGrid.Add (c_FilNr);

	CCtrlInfo *c_DataBorder_inet = new CCtrlInfo (&m_Databorder_inet, 3, 23, DOCKRIGHT, DOCKBOTTOM);
    c_DataBorder_inet->rightspace = 20;
	CtrlGrid.Add (c_DataBorder_inet);

	CCtrlInfo *c_LShop_kz = new CCtrlInfo (&m_LShop_kz, 4, 24, 1, 1);
	CtrlGrid.Add (c_LShop_kz);
	CCtrlInfo *c_Shop_kz = new CCtrlInfo (&m_Shop_kz, 5, 24, 1, 1);
	c_Shop_kz->rightspace = 40;
	CtrlGrid.Add (c_Shop_kz);

	CCtrlInfo *c_LShop_History = new CCtrlInfo (&m_LShop_History, 4, 25, 1, 1);
	CtrlGrid.Add (c_LShop_History);
	CCtrlInfo *c_Shop_History = new CCtrlInfo (&m_Shop_History, 5, 25, 1, 1);
	c_Shop_History->rightspace = 40;
	CtrlGrid.Add (c_Shop_History);

	CCtrlInfo *c_LShop_passw = new CCtrlInfo (&m_LShop_passw, 4, 26, 1, 1);
	CtrlGrid.Add (c_LShop_passw);
	CCtrlInfo *c_Shop_passw = new CCtrlInfo (&m_Shop_passw, 5, 26, 1, 1);
	c_Shop_passw->rightspace = 40;
	CtrlGrid.Add (c_Shop_passw);


	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	CtrlGrid.Display ();
    CtrlGrid.SetItemCharHeight (&m_HeadBorder, 3);
    EnableFields (TRUE);

	CString kz2 = Kun->kun.shop_kz;
	if (kz2 == _T("J"))
	{
		m_Shop_kz.SetWindowTextA("   a k t i v i e r t");
		m_Shop_passw.EnableWindow(TRUE);
		m_Shop_History.EnableWindow(TRUE);
	}
	else
	{
		m_Shop_kz.SetWindowTextA("   d e a k t i v i e r t");
		m_Shop_passw.EnableWindow(FALSE);
		m_Shop_History.EnableWindow(FALSE);
	}


	kz2 = Kun->kun.shop_history;
	if (kz2 == _T("J"))
	{
		m_Shop_History.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_Shop_History.SetWindowTextA("   d e a k t i v i e r t");
	}


	kz2 = Kun->kun.ueb_kz;
	if (kz2 == _T("J"))
	{
		m_FilKZ.SetWindowTextA("   a k t i v i e r t");
		m_FilNr.EnableWindow(TRUE);
		m_FilGr.EnableWindow(TRUE);
	}
	else
	{
		m_FilKZ.SetWindowTextA("   d e a k t i v i e r t");
		m_FilNr.EnableWindow(FALSE);
		m_FilGr.EnableWindow(FALSE);
	}

	
	short kz3 = Kun->kun.kun_of_lf;
	if (kz3 == 1)
	{
		m_LiefStop.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_LiefStop.SetWindowTextA("   d e a k t i v i e r t");
	}

	short kz4 = Kun->kun.min_best;
	if (kz4 == 1)
	{
		m_MinBest.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_MinBest.SetWindowTextA("   d e a k t i v i e r t");
	}

	if (Kun->kun.adr1 == Kun->kun.adr2)
	{
		/*m_Name1.EnableWindow(FALSE);
		m_Name2.EnableWindow(FALSE);
		m_Name3.EnableWindow(FALSE);*/
	}

    Form.Show ();
	return TRUE;
}


BOOL CKunPage2::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				Update->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Adr1)
				{
					OnAdr2Choice();
					return TRUE;
				}
				else if (GetFocus () == &m_Plz)
				{
					OnPlz2Choice();
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F12)
			{
				
				Form.Get();
				if (!TestFields ())
				{
					return FALSE;
				}
				Update->Write();
				return TRUE;
			}


			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}
			if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

HBRUSH CKunPage2::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);

    if (hBrush == NULL)
	{
		  hBrush = CreateSolidBrush (Color);
		  hBrushStatic = CreateSolidBrush (Color);
		  m_KunLief.SetBkColor (DlgBkColor);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			return hBrush;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CButton )))
	{
            pDC->SetBkColor (Color);
			pDC->SetBkMode (TRANSPARENT);
			return hBrushStatic;
	}
	return CPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CKunPage2::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Adr1)
	{
		Form.GetFormField (&m_Adr1)->Get ();
		
		OnAdrSelectedEx();	
		m_Anr.SetFocus ();
		return FALSE;
		
	}

	if (Control == &m_Plz)
	{
		Form.GetFormField (&m_Plz)->Get ();
		Form.GetFormField (&m_Telefon)->Get ();
		PLZ_CLASS Plz = EnterPlz.GetPlzFromPlz (LiefAdr->adr.plz);
		if (EnterPlz.Found ())
		{
			_tcscpy (LiefAdr->adr.ort1, Plz.plz.ort);
			Form.GetFormField (&m_Ort)->Show ();
			if (_tcscmp (LiefAdr->adr.tel, _T("0")) < 0)
			{
				_tcscpy (LiefAdr->adr.tel, Plz.plz.vorwahl);
				Form.GetFormField (&m_Telefon)->Show ();
			}
			CFormField *fLand = Form.GetFormField (&m_Land);
			int idx = EnterPlz.GetComboIdx (Plz.plz.bundesland, fLand->ComboValues);
			if (idx != -1)
			{
				m_Land.SetCurSel (idx);
				fLand->Get ();
			}
		}
	}

	if (Control == &m_Ort)
	{
		Form.GetFormField (&m_Ort)->Get ();
		Form.GetFormField (&m_Telefon)->Get ();
		PLZ_CLASS Plz = EnterPlz.GetPlzFromOrt (LiefAdr->adr.ort1);
		if (EnterPlz.Found ())
		{
			_tcscpy (LiefAdr->adr.plz, Plz.plz.plz);
			Form.GetFormField (&m_Plz)->Show ();
			_tcscpy (LiefAdr->adr.ort1, Plz.plz.ort);
			Form.GetFormField (&m_Ort)->Show ();
			if (_tcscmp (LiefAdr->adr.tel, _T("0")) < 0)
			{
				_tcscpy (LiefAdr->adr.tel, Plz.plz.vorwahl);
				Form.GetFormField (&m_Telefon)->Show ();
			}
			CFormField *fLand = Form.GetFormField (&m_Land);
			int idx = EnterPlz.GetComboIdx (Plz.plz.bundesland, fLand->ComboValues);
			if (idx != -1)
			{
				m_Land.SetCurSel (idx);
				fLand->Get ();
			}
		}
	}


	if (Control == &m_Tour1)
	{
		if (!TestTour (1))
		{
			m_Tour1.SetFocus ();
			MessageBox (_T("Tour nicht gefunden"), _T(""), MB_OK | MB_ICONERROR);
			if ( ! minitour )
				return FALSE ;
		}
	}

	if (Control == &m_Tour2)
	{
		if (!TestTour (2))
		{
			m_Tour2.SetFocus ();
			MessageBox (_T("Tour nicht gefunden"), _T(""), MB_OK | MB_ICONERROR);
			if ( !minitour )	return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CKunPage2::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

void CKunPage2::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CKunPage2::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}

void CKunPage2::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


	if (m_Mdn.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein SDatz zum L�schen selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	if (MessageBox (_T("Preisauszeichnerdaten l�schen"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		Kun->beginwork ();
		Kun->dbdelete ();
		Kun->commitwork ();
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		EnableFields (FALSE);
		Form.Show ();
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);
	}
}

void CKunPage2::EnableHeadControls (BOOL enable)
{

	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
		Control->EnableWindow (enable);
	}
}

BOOL CKunPage2::StepBack ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
					Frame->DestroyWindow ();
					return FALSE;
	}
	else
	{
		EnableFields (FALSE);
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);	
	}
	return TRUE;
}


void CKunPage2::OnCopy ()
{
    OnEditCopy ();
}  



void CKunPage2::OnPaste ()
{
	OnEditPaste ();
}

void CKunPage2::OnCopyEx ()
{

//	if (!m_A.IsWindowEnabled ())
	{
		Copy ();
		return;
	}
}

void CKunPage2::OnPasteEx ()
{

//	if (!m_A.IsWindowEnabled () && Paste ())
	{
		Kun->CopyData (&kun);
		Kun_erw->CopyData (&kun_erw);
		Form.Show ();
/*
		if (KunPageEx != NULL && IsWindow (KunPageEx->m_hWnd))
		{
			KunPageEx->Form.Show ();
		}
*/
		return;
	}
}

BOOL CKunPage2::Copy ()
{
	Form.Get ();
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return FALSE;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      int err = GetLastError ();
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return FALSE;  
    }

	try
	{
        UINT CF_A_KUN_GX = RegisterClipboardFormat (_T("kun")); 
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, sizeof (KUN)); 
        char *p = (char *) GlobalLock(hglbCopy);
        memcpy (p, &Kun->kun, sizeof (Kun->kun));
        GlobalUnlock(hglbCopy); 
		HANDLE cData;
		cData = ::SetClipboardData( CF_A_KUN_GX, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
    return TRUE;
}

BOOL CKunPage2::Paste ()
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return FALSE;
    }

    HGLOBAL hglbCopy;
    UINT CF_A_KUN_GX;
    _TCHAR name [256];
    CF_A_KUN_GX = EnumClipboardFormats (0);
	if (CF_A_KUN_GX == 0)
	{
		int err = GetLastError ();
		CloseClipboard();
		return FALSE;
	}

    int ret;
    while ((ret = GetClipboardFormatName (CF_A_KUN_GX, name, sizeof (name))) != 0)
    { 
         CString Name = name;
         if (Name == _T("a_kun_gx"))
         {
              break;
         }  
		 CF_A_KUN_GX = EnumClipboardFormats (CF_A_KUN_GX);
    }
    if (ret == 0)
    {
         CloseClipboard();
         return FALSE;
    }    

    try
    {	
  	     hglbCopy =  ::GetClipboardData(CF_A_KUN_GX);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
 	     LPSTR p = (LPSTR) GlobalLock ((HGLOBAL) hglbCopy);
         memcpy ( (LPSTR) &kun, p, sizeof (kun));
  	     GlobalUnlock ((HGLOBAL) hglbCopy);
    }
    catch (...) {};
    CloseClipboard();
    return TRUE; 
}

void CKunPage2::EnableFields (BOOL b)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (!b)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);
}


void CKunPage2::OnKillFocus (CWnd *newFocus)
{
	CWnd *Control = GetFocus ();
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		if (f->Scale > 0)
		{
			f->Show ();
		}
	}

}


void CKunPage2::FillPtabCombo (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem)); 
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert)); 
		Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0); 

		
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn->sqlcursor (Sql.GetBuffer ());
		while (Ptabn->sqlfetch (cursor) == 0)
		{
			Ptabn->dbreadfirst();

			LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn->ptabn.ptwert,
				                             Ptabn->ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}




void CKunPage2::FillAnr ()
{

	CFormField *f = Form.GetFormField (&m_Anr);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem)); 
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert)); 
		Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn->sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"anr\" ")
									  _T("order by ptlfnr"));
		while (Ptabn->sqlfetch (cursor) == 0)
		{
			Ptabn->dbreadfirst();

			LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn->ptabn.ptwert,
				                             Ptabn->ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}


void CKunPage2::OnCbnSelchangeVersand()
{
	CString kz;

	Form.Get();

	kz = Kun->kun.vers_art;

	if (kz == _T("2"))
	{
		m_Markt.EnableWindow(TRUE);
	}
	else
	{
		m_Markt.EnableWindow(FALSE);
	}
		


}

void CKunPage2::FillGrZuord ()
{
	CFormField *f = Form.GetFormField (&m_FilGr);
	if (f != NULL)
	{
		f->ComboValues.clear ();
	
		Gr_zuord->sqlout ((short *) &Gr_zuord->gr_zuord.gr, SQLSHORT, 0); 
		Gr_zuord->sqlout ((char *) Gr_zuord->gr_zuord.gr_bz1  , SQLCHAR, sizeof (Gr_zuord->gr_zuord.gr_bz1)); 
		
        int cursor = Gr_zuord->sqlcursor (_T("select gr, gr_bz1 from gr_zuord where gr >= 0 ")
			                         	  _T("order by gr"));
		
		
		
		while (Gr_zuord->sqlfetch (cursor) == 0)
		{
			Gr_zuord->dbreadfirst ();

			//LPSTR pos = (LPSTR) Iprgrstufk.iprgrstufk.pr_gr_stuf;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			//pos = (LPSTR) Iprgrstufk.iprgrstufk.zus_bz;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), Gr_zuord->gr_zuord.gr,
											  Gr_zuord->gr_zuord.gr_bz1);
		    f->ComboValues.push_back (ComboValue); 
		}
		Gr_zuord->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}



void CKunPage2::FillFil ()
{
	CFormField *f = Form.GetFormField (&m_FilNr);
	char adr_nam1 [sizeof (KunAdr->adr.adr_nam1)];
	if (f != NULL)
	{
		f->ComboValues.clear ();
	
		Fil->sqlout ((short *) &Fil->fil.fil, SQLSHORT, 0); 
		Fil->sqlout ((char *) adr_nam1  , SQLCHAR, sizeof (KunAdr->adr.adr_nam1)); 
		Fil->sqlin  ((short *) &Mdn->mdn.mdn, SQLSHORT, 0);
		Fil->sqlin  ((short *) &Fil->fil.fil_gr, SQLSHORT, 0);
		
        int cursor = Fil->sqlcursor (_T("select fil.fil, adr.adr_nam1 from fil, adr ")
										_T("where fil.adr = adr.adr and fil.fil >= 0 and fil.mdn = ? ")
			                         	  _T("order by fil.fil"));
		
		
		CString *ComboValue = new CString ();
		ComboValue->Format (_T("%hd %s"),	0,
												"Keine Zuordnung");
	    f->ComboValues.push_back (ComboValue); 
		
		while (Fil->sqlfetch (cursor) == 0)
		{
			Fil->dbreadfirst ();
			//LPSTR pos = (LPSTR) Iprgrstufk.iprgrstufk.pr_gr_stuf;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			//pos = (LPSTR) Iprgrstufk.iprgrstufk.zus_bz;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"),	Fil->fil.fil,
												adr_nam1);
		    f->ComboValues.push_back (ComboValue); 
		}
		Fil->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}



void CKunPage2::OnCbnSelchangeFilgr()
{
	FillFil();

}

void CKunPage2::OnBnClickedFilkz()
{
	CString kz;
	
	Form.Get();

	kz = Kun->kun.ueb_kz;

	if (kz == _T("N"))
	{
		m_FilNr.EnableWindow(FALSE);
		m_FilGr.EnableWindow(FALSE);
		m_FilKZ.SetWindowTextA("   d e a k t i v i e r t");
	}
	else
	{
		m_FilNr.EnableWindow(TRUE);
		m_FilGr.EnableWindow(TRUE);
		m_FilKZ.SetWindowTextA("   a k t i v i e r t");
	}


	
}

void CKunPage2::OnBnClickedLiefstop()
{
	short kz3;
	Form.Get();

	kz3 = Kun->kun.kun_of_lf;
	if (kz3 == 1)
	{
		m_LiefStop.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_LiefStop.SetWindowTextA("   d e a k t i v i e r t");
	}
}


void CKunPage2::OnMoreFrachtKosten()
{	
	CFrachtKosten dlg;
	
	dlg.Zuschlag = Zuschlag;

	Zuschlag->zuschlag.mdn = Kun->kun.mdn;
	
	Zuschlag->zuschlag.kun = Kun->kun.kun;
	
	dlg.DoModal ();
}


void CKunPage2::OnCbnSelchangeFracht()
{
	OnMoreFrachtKosten();
}


void CKunPage2::OnAdr2Choice ()
{
	CString Adr2Nr;
	m_Adr1.GetWindowText (Adr2Nr);
    ChoiceStat = TRUE;
	if (ChoiceAdr != NULL && !ModalChoiceAdr)
	{
		ChoiceAdr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceAdr == NULL)
	{
		ChoiceAdr = new CChoiceAdr (this);
	    ChoiceAdr->IsModal = ModalChoiceAdr;
		ChoiceAdr->m_Adr = _tstol (Adr2Nr.GetBuffer ());
		ChoiceAdr->CreateDlg ();
	}

	ChoiceAdr->SetDbClass (LiefAdr);
	ChoiceAdr->SearchText = Search;
	if (ModalChoiceAdr)
	{
			ChoiceAdr->DoModal();
	}
	
    if (ChoiceAdr->GetState ())
    {
		  CAdrList *abl = ChoiceAdr->GetSelectedText (); 
		  if (abl == NULL) return;
		  memcpy (&LiefAdr->adr, &adr_null, sizeof (ADR));
		  Kun->kun.adr2 = abl->adr;
		  if (LiefAdr->dbreadfirst () == 0)
		  {
			  LiefAdr->adr.adr = Kun->kun.adr2;
			  if ( ! LiefAdr->dbreadfirst ())
			  	 strncpy (Kun->kun.iln, LiefAdr->adr.iln,sizeof(Kun->kun.iln) -1);	// 260812

		  }
		  m_Adr1.SetSel (0, -1, TRUE);
		  m_Adr1.SetFocus ();
//		  memcpy (&Mdn->mdn, &mdn_null, sizeof (MDN));
//		  memcpy (&LiefAdr->adr, &adr_null, sizeof (ADR));
		  
		  Form.Show ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  ChoiceAdr = NULL;
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}


void CKunPage2::OnPlz2Choice ()
{
	PLZ_CLASS Plz;
	memcpy (&Plz.plz, &plz_null, sizeof (Plz.plz));
	Form.GetFormField (&m_Ort)->Get ();
	Form.GetFormField (&m_Telefon)->Get ();
	Form.GetFormField (&m_Land)->Get ();
	Form.GetFormField (&m_IncludeLand)->Get ();
	_tcscpy (Plz.plz.ort, LiefAdr->adr.ort1);

	if (IncludeLand)
	{
		memcpy (&Ptabn->ptabn, &ptabn_null, sizeof (Ptabn->ptabn));
		_tcscpy (Ptabn->ptabn.ptitem, _T("land"));
		_stprintf (Ptabn->ptabn.ptwert, "%hd", LiefAdr->adr.land);
		if (Ptabn->dbreadfirst () == 0)
		{
			_tcscpy (Plz.plz.bundesland, Ptabn->ptabn.ptbez);
		}
	}

	CPlzList *abl = EnterPlz.Choice (Plz);
	if (abl != NULL)
	{
		
		_tcscpy (LiefAdr->adr.plz, abl->plz.GetBuffer ());
		Form.GetFormField (&m_Plz)->Show ();
		_tcscpy (LiefAdr->adr.ort1, abl->ort.GetBuffer ());
		Form.GetFormField (&m_Ort)->Show ();
		if (_tcscmp (LiefAdr->adr.tel, _T("0")) < 0)
		{
			_tcscpy (LiefAdr->adr.tel, abl->vorwahl.GetBuffer ());
			Form.GetFormField (&m_Telefon)->Show ();
		}
		CFormField *fLand = Form.GetFormField (&m_Land);
		int idx = EnterPlz.GetComboIdx (abl->bundesland.GetBuffer (), 
			                            fLand->ComboValues);
		if (idx != -1)
		{
			m_Land.SetCurSel (idx);
			fLand->Get ();
		}
	}
}

void CKunPage2::OnAdrCanceled ()
{
	ChoiceAdr->ShowWindow (SW_HIDE);
}

BOOL CKunPage2::TestAdr2Type ()
{
	if (Kun->kun.adr2 == Kun->kun.adr1)
	{
		return TRUE;
	}
	/**  raus damit, gibt nur �rger LuD 10.01.2011
	LiefAdr->adr.adr = Kun->kun.adr2;
	int dsqlstatus = LiefAdr->dbreadfirst ();
	if (LiefAdr->adr.adr_typ != KunAdrLiefType)
	{
		AfxMessageBox (_T("Falscher Adresstyp"));
		m_Adr1.SetFocus ();
		return FALSE;
	}
	**/
	return TRUE;
}
void CKunPage2::OnAdrSelected ()
{
	OnAdrSelectedEx ();
}

BOOL CKunPage2::OnAdrSelectedEx ()
{
	CString AdrNr;
	
	if (ChoiceAdr == NULL) 
	{
		m_Adr1.GetWindowText (AdrNr);		
		memcpy (&LiefAdr->adr, &adr_null, sizeof (ADR));
		
		long Adr1 = atol(AdrNr);
		Kun->kun.adr2 = Adr1;
	}
	else
	{
		CAdrList *abl = ChoiceAdr->GetSelectedText();
		
		if (abl == NULL) 
		{
			return FALSE;
		}
		else
		{
			memcpy (&LiefAdr->adr, &adr_null, sizeof (ADR));
			Kun->kun.adr2 = abl->adr;

		}
	}
        
    
	memcpy (&LiefAdr->adr, &adr_null, sizeof (ADR));
	LiefAdr->adr.adr = Kun->kun.adr2;
	LiefAdr->dbreadfirst ();
    
	int dsqlstatus = LiefAdr->dbreadfirst ();
	if (dsqlstatus == 100)
	{
		LiefAdr->adr.adr_typ = KunAdrLiefType;
// Grundwert holen LuD 10.01.2011
		LiefAdr->adr.adr = -1;
		LiefAdr->dbreadfirst ();
		LiefAdr->adr.adr = Kun->kun.adr2;

	}

	if (Kun->kun.adr1 != Kun->kun.adr2)
	{
	/**  raus damit, gibt nur �rger LuD 10.01.2011
		if (LiefAdr->adr.adr_typ != KunAdrLiefType)
		{
			AfxMessageBox (_T("Falscher Adresstyp"));
			return FALSE;
		}
		**/
	}

	if (KunAdr->adr.adr == LiefAdr->adr.adr)
	{
		m_Anr.EnableWindow(FALSE);
		m_Name1.EnableWindow(FALSE);
		m_Name2.EnableWindow(FALSE);
		m_Name3.EnableWindow(FALSE);
		m_Strasse.EnableWindow(FALSE);
		m_Postfach.EnableWindow(FALSE);
		m_plzPostfach.EnableWindow(FALSE);
		m_Plz.EnableWindow(FALSE);
		m_Ort.EnableWindow(FALSE);
		m_Telefon.EnableWindow(FALSE);
		m_Fax.EnableWindow(FALSE);
		m_EMAIL.EnableWindow(FALSE);
		m_Partner.EnableWindow(FALSE);
		m_Staat.EnableWindow(FALSE);
		m_Land.EnableWindow(FALSE);
		m_Modem.EnableWindow(FALSE);
		Form.Show ();	
		m_KunKrz1.SetFocus();
	}
	else
	{
		m_Anr.EnableWindow(TRUE);
		m_Name1.EnableWindow(TRUE);
		m_Name2.EnableWindow(TRUE);
		m_Name3.EnableWindow(TRUE);
		m_Strasse.EnableWindow(TRUE);
		m_Postfach.EnableWindow(TRUE);
		m_plzPostfach.EnableWindow(TRUE);
		m_Plz.EnableWindow(TRUE);
		m_Ort.EnableWindow(TRUE);
		m_Telefon.EnableWindow(TRUE);
		m_Fax.EnableWindow(TRUE);
		m_EMAIL.EnableWindow(TRUE);
		m_Partner.EnableWindow(TRUE);
		m_Staat.EnableWindow(TRUE);
		m_Land.EnableWindow(TRUE);
		m_Modem.EnableWindow(TRUE);
		Form.Show ();	
		m_Anr.SetFocus();
	}
    

	
	//Read ();
	
	
	if (ChoiceAdr != NULL)
	{
		if (ChoiceAdr->FocusBack)
		{
			ChoiceAdr->SetListFocus ();
		}
	}

	return TRUE;
}

BOOL CKunPage2::TestFields ()
{
	if (m_hWnd == NULL)
	{
		return TRUE;
	}
	try
	{

		if ( ! minitour )
		{
			if (!TestTour (1))
			{
				m_Tour1.SetFocus ();
				MessageBox (_T("Tour nicht gefunden"), _T(""), MB_OK | MB_ICONERROR);
				return FALSE;
			}
			if (!TestTour (2))
			{
				m_Tour2.SetFocus ();
				MessageBox (_T("Tour nicht gefunden"), _T(""), MB_OK | MB_ICONERROR);
				return FALSE;
			}
		}

		if (!TestAdr2Type ())
		{
			return FALSE;
		}
	}
	catch (...) {}
	return TRUE;
}

BOOL CKunPage2::OnKillActive ()
{
	Form.Get ();
	if (F5Pressed)
	{
		return TRUE;
	}
	return TestFields ();
}

BOOL CKunPage2::OnSetActive ()
{
	F5Pressed = FALSE;
	if (KunAdr->adr.adr == LiefAdr->adr.adr)
	{
		m_Anr.EnableWindow(FALSE);
		m_Name1.EnableWindow(FALSE);
		m_Name2.EnableWindow(FALSE);
		m_Name3.EnableWindow(FALSE);
		m_Strasse.EnableWindow(FALSE);
		m_Postfach.EnableWindow(FALSE);
		m_plzPostfach.EnableWindow(FALSE);
		m_Plz.EnableWindow(FALSE);
		m_Ort.EnableWindow(FALSE);
		m_Telefon.EnableWindow(FALSE);
		m_Fax.EnableWindow(FALSE);
		m_EMAIL.EnableWindow(FALSE);
		m_Partner.EnableWindow(FALSE);
		m_Staat.EnableWindow(FALSE);
		m_Land.EnableWindow(FALSE);
		m_Modem.EnableWindow(FALSE);
		Form.Show ();	
		m_KunKrz1.SetFocus();
	}
	else
	{				
		m_Anr.EnableWindow(TRUE);
		m_Name1.EnableWindow(TRUE);
		m_Name2.EnableWindow(TRUE);
		m_Name3.EnableWindow(TRUE);
		m_Strasse.EnableWindow(TRUE);
		m_Postfach.EnableWindow(TRUE);
		m_plzPostfach.EnableWindow(TRUE);
		m_Plz.EnableWindow(TRUE);
		m_Ort.EnableWindow(TRUE);
		m_Telefon.EnableWindow(TRUE);
		m_Fax.EnableWindow(TRUE);
		m_EMAIL.EnableWindow(TRUE);
		m_Partner.EnableWindow(TRUE);
		m_Staat.EnableWindow(TRUE);
		m_Land.EnableWindow(TRUE);
		m_Modem.EnableWindow(TRUE);
		Form.Show ();
		m_Anr.SetFocus();
	}
	return TRUE;
}

void CKunPage2::UpdatePage ()
{
	CString kz2 = Kun->kun.shop_kz;
	if (kz2 == _T("J"))
	{
		m_Shop_kz.SetWindowTextA("   a k t i v i e r t");
		m_Shop_passw.EnableWindow(TRUE);
	}
	else
	{
		m_Shop_kz.SetWindowTextA("   d e a k t i v i e r t");
		m_Shop_passw.EnableWindow(FALSE);
	}

	Form.Show ();
}

void CKunPage2::OnKunLief ()
{
	CProcess p;
	CString command;

	command.Format (_T("KunLief %hd 0 %ld"), Mdn->mdn.mdn, Kun->kun.kun);
	p.SetCommand (command);
	p.Start ();
}

void CKunPage2::OnTourChoice1 ()
{
	Form.Get ();
	CChoiceTour *Choice = new CChoiceTour (this);
    Choice->IsModal = TRUE;
	Choice->SearchText = "";
	Choice->CreateDlg ();

    Choice->SetDbClass (Kun);
	Choice->DoModal();

    if (Choice->GetState ())
    {
		  CTourList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  strcpy (tou_bz, "");
		  tou = abl->tou;
		  Kun->sqlopen (cursor_tour);
		  if (Kun->sqlfetch (cursor_tour) == 0)
		  {
			  Kun_tou->kun_tou.tou = tou;
			  strcpy (Kun_tou->kun_tou.tou_bz, tou_bz);;
			  Form.Show ();
		  }
	}
	delete Choice;
}

void CKunPage2::OnTourChoice2 ()
{
	Form.Get ();
	CChoiceTour *Choice = new CChoiceTour (this);
    Choice->IsModal = TRUE;
	Choice->SearchText = "";
	Choice->CreateDlg ();

    Choice->SetDbClass (Kun);
	Choice->DoModal();

    if (Choice->GetState ())
    {
		  CTourList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  strcpy (tou_bz, "");
		  tou = abl->tou;
		  Kun->sqlopen (cursor_tour);
		  if (Kun->sqlfetch (cursor_tour) == 0)
		  {
			  Kun_tou->kun_tou.tou2 = tou;
			  strcpy (Kun_tou->kun_tou.tou_bz2, tou_bz);;
			  Form.Show ();
		  }
	}
	delete Choice;
}

BOOL CKunPage2::TestTour (int pos)
{
	Form.Get ();
	if (pos == 1)
	{
		tou = Kun_tou->kun_tou.tou;
		strcpy (Kun_tou->kun_tou.tou_bz, "");
	}
	else if (pos == 2)
	{
		tou = Kun_tou->kun_tou.tou2;
		strcpy (Kun_tou->kun_tou.tou_bz2, "");
	}
	else
	{
		return TRUE;
	}

	if (tou == 0l)
	{
		Form.Show ();
		return TRUE;
	}

	Kun->sqlopen (cursor_tour);
	if (Kun->sqlfetch (cursor_tour) == 0)
	{
		if (pos == 1)
		{
			strcpy (Kun_tou->kun_tou.tou_bz, tou_bz);
		}
		else
		{
			strcpy (Kun_tou->kun_tou.tou_bz2, tou_bz);
		}
		Form.Show ();
		return TRUE;
	}
	return FALSE; 
}

void CKunPage2::OnEnChangeKunkrz1()
{
	CString Text;
	m_KunKrz1.GetWindowText (Text);
	if (Text.GetLength() >= sizeof(Kun->kun.kun_krz2))
	{
		strcpy(Kun->kun.kun_krz2,"");
		strncpy(Kun->kun.kun_krz2,Text.GetBuffer(),sizeof(Kun->kun.kun_krz2)-1);
		Kun->kun.kun_krz2[sizeof(Kun->kun.kun_krz2)-1] = 0;
		Form.Show ();
		m_KunKrz1.SetSel (sizeof(Kun->kun.kun_krz2)-1, -1,FALSE);
	}
}

void CKunPage2::OnBnClickedShopKz()
{
	CString kz;
	
	Form.Get();

	kz = Kun->kun.shop_kz;

	if (kz == _T("N"))
	{
		m_Shop_passw.EnableWindow(FALSE);
		m_Shop_History.EnableWindow(FALSE);
		m_Shop_kz.SetWindowTextA("   d e a k t i v i e r t");
	}
	else
	{
		m_Shop_passw.EnableWindow(TRUE);
		m_Shop_History.EnableWindow(TRUE);
		m_Shop_kz.SetWindowTextA("   a k t i v i e r t");
	}


	
}


void CKunPage2::OnBnClickedShopHistory()
{
	CString kz;
	
	Form.Get();

	kz = Kun->kun.shop_history;

	if (kz == _T("N"))
	{
		m_Shop_History.SetWindowTextA("   d e a k t i v i e r t");
	}
	else
	{
		m_Shop_History.SetWindowTextA("   a k t i v i e r t");
	}


	
}
/***
void CKunPage2::OnInfo()
{
	 CKunPage *Page = (CKunPage *) dlg.GetPage (0);
	 Page->OnInfo ();
 }
 ***/

void CKunPage2::OnBnClickedMinbest()
{
	short kz;
	Form.Get();

	kz = Kun->kun.min_best;
	if (kz == 1)
	{
		m_MinBest.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_MinBest.SetWindowTextA("   d e a k t i v i e r t");
	}
}
