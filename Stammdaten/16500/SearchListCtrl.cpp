#include "StdAfx.h"
#include "searchlistctrl.h"

//IMPLEMENT_DYNAMIC(CSearchListCtrl, CObject)
IMPLEMENT_DYNCREATE(CSearchListCtrl, CStatic)

CSearchListCtrl::CSearchListCtrl(void)
{
	idEdit   = 0;
	idButton = 0;
}

CSearchListCtrl::~CSearchListCtrl(void)
{
	DestroyWindow ();
}

void CSearchListCtrl::Create (RECT &rect, CWnd *pParent, UINT nIDEdit, UINT nIDButton, int Align)
{

    CRect ButtonRect;
	ButtonRect = rect;
    int ButtonWidth = 4 * (rect.bottom - rect.top);
	ButtonWidth /= 4;
	ButtonRect.left = ButtonRect.right - ButtonWidth;
	Button.Create (_T(".."),
		           BS_PUSHBUTTON | BS_CENTER | 
				   BS_TEXT | BS_BOTTOM | WS_VISIBLE | WS_CHILD,
		           ButtonRect, pParent, nIDButton);
	Button.ModifyStyle (WS_TABSTOP, 0);
	CRect EditRect;
	EditRect = rect;
	EditRect.right = ButtonRect.left - 1;
	if (Align == RIGHT)
	{
		Edit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_RIGHT, 
			             EditRect, pParent, nIDEdit);
	}
	else
	{
		Edit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_AUTOVSCROLL | ES_MULTILINE, 
			              EditRect, pParent, nIDEdit);
	}
}

BOOL CSearchListCtrl::DestroyWindow ()
{
	if (IsWindow (Edit.m_hWnd))
	{
		Edit.DestroyWindow ();
	}
	if (IsWindow (Button.m_hWnd))
	{
		Button.DestroyWindow ();
	}
	return TRUE;
}

