#pragma once

#include "DbPropertyPage.h"
#include "resource.h"
#include "ListDropTarget.h"
#include "CtrlGrid.h"
#include "Kun.h"
#include "kun_erw.h"
#include "Mdn.h"
#include "Adr.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "FormTab.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "DbFormView.h"
#include "CodeProperties.h"
#include "mo_progcfg.h"
#include "sys_par.h"
#include "ptabn.h"
#include "iprgrstufk.h"
#include "i_kun_prk.h"
#include "kst.h"
#include "kun_tou.h"
#include "fil.h"
#include "gr_zuord.h"
#include "zahl_kond.h"
#include "rab_stufek.h"
#include "haus_bank.h"
#include "ls_txt.h"
#include "ChoiceLstxt.h"
#include "LstxtList.h"





// CKunPage4-Dialogfeld

class CKunPage4 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CKunPage4)

public:
	CKunPage4();
	virtual ~CKunPage4();

// Dialogfelddaten
	enum { IDD = IDD_KUNPAGE4 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	PROG_CFG Cfg;

	virtual BOOL OnInitDialog();



public:

	KUN_CLASS *Kun;
	KUN_ERW_CLASS *Kun_erw;
	ADR_CLASS *KunAdr;
	MDN_CLASS *Mdn;
	ADR_CLASS *MdnAdr;
	PTABN_CLASS *Ptabn;
	IPRGRSTUFK_CLASS *Iprgrstufk;
	I_KUN_PRK_CLASS *I_kun_prk;
	KST_CLASS *Kst;
	SYS_PAR_CLASS *Sys_par;
	KUN_TOU_CLASS *Kun_tou;
	FIL_CLASS *Fil;
	GR_ZUORD_CLASS *Gr_zuord;
	ZAHL_KOND_CLASS *Zahl_kond;
	RAB_STUFEK_CLASS *Rab_stufek;
	HAUS_BANK_CLASS *Haus_bank;
	LS_TXT_CLASS *Ls_txt;

	BOOL flgKopfLs;
	BOOL flgFussLs;
	BOOL flgKopfRech;
	BOOL flgFussRech;




	CFormTab Form;

	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid KunGrid;
	CCtrlGrid SchwundGrid;
		
	CListDropTarget dropTarget;

    CPropertySheet *KunProperty;
    DbFormView *View;
	CFrameWnd *Frame;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	void Register ();
	CEdit m_MdnName;
	CStatic m_LKun;
	CNumEdit m_Kun;
	CButton m_KunChoice;
	CEdit m_KunName;
	CStatic m_HeadBorder;
	CStatic m_DataBorder;
	CStatic m_LAnr;
	CComboBox m_Anr;
	CStatic m_LAdr1;
	CNumEdit m_Adr1;

    CVector HeadControls;
	BOOL ChoiceStat;
	CString Search;

	CCodeProperties Code;
    BOOL OnReturn ();
    BOOL OnKeyup ();
    BOOL ReadMdn ();
	void FillLSKopf ();
	void FillLSFuss ();
	void FillRechFuss ();
	void FillRechKopf ();
	void FillZahlZiel ();
	void OnDelete ();
    void FillRabSchl();
	void FillPtabCombo (CWnd *Control, LPTSTR Item);
	void FillPtabCombo2 (CWnd *Control, LPTSTR Item);
	void FillPtabCombo3 (CWnd *Control, LPTSTR Item);
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
    void EnableHeadControls (BOOL enable);
    BOOL StepBack ();
	virtual void OnCopy ();
    virtual void OnPaste ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
	BOOL Copy ();
	BOOL Paste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
    void EnableFields (BOOL b);
	void UpdatePage ();
	afx_msg void OnKillFocus (CWnd *);
	CStatic m_LName1;
	CTextEdit m_Name1;
public:
	CTextEdit m_Name2;
public:
	CStatic m_LName2;
public:
	CTextEdit m_Name3;
public:
	CStatic m_LName3;
public:
	CTextEdit m_Strasse;
public:
	CStatic m_LStrasse;
public:
	CNumEdit m_Plz;
public:
	CStatic m_LPlz;
public:
	CTextEdit m_Ort;
public:
	CStatic m_LOrt;
public:
	CStatic m_LPostfach;
public:
	CNumEdit m_Postfach;
public:
	CNumEdit m_Telefon;
public:
	CStatic m_LTelefon;
public:
	CNumEdit m_Fax;
	public:
	CStatic m_LKunKrz1;
public:
	CTextEdit m_KunKrz1;
public:
	CNumEdit m_plzPostfach;
public:
	CNumEdit m_EMAIL;
public:
	CStatic m_LEmail;
public:
	CTextEdit m_Partner;
public:
	CStatic m_LPartner;
public:
	CComboBox m_Staat;
public:
	CStatic m_LStaat;
public:
	CComboBox m_Land;
public:
	CStatic m_LLand;

public:
	CStatic m_DataBorder2;


public:
	CStatic m_DataBorder3;
public:
	CStatic m_LFormLS;
public:
	CComboBox m_FormLS;
public:
	CComboBox m_LeerLS;
public:
	CStatic m_LLeerLS;
public:
	CEdit m_SchwundBez;
public:
	CButton m_SchwundKZ;
public:
	CStatic m_LSchwund;
public:
	afx_msg void OnBnClickedSchwundkz();
	afx_msg void OnBnClickedEMailRech();
	afx_msg void OnBnClickedZollP();
public:
	CNumEdit m_Auflage1;
public:
	CStatic m_LAuflage1;

public:
	CStatic m_LFormRech;
public:
	CComboBox m_FormRech;
public:
	CStatic m_LRechSt;
public:
	CComboBox m_RechSt;
public:
	CStatic m_LRechArt;
public:
	CComboBox m_RechArt;
public:
	CStatic m_LEinzAusw;
public:
	CComboBox m_EinzAusw;
public:
	CStatic m_LAuflage2;
public:
	CNumEdit m_Auflage2;

public:
	CStatic m_LPrAuswRech;
public:
	CComboBox m_PrAuswRech;
public:
	CComboBox m_PrAuswLS;
public:
	CStatic m_LPrAuswLS;
public:
	CStatic m_LKopfLS;
public:
	CNumEdit m_KopfLS;
public:
	CStatic m_LFussLS;
public:
	CNumEdit m_FussLS;
public:
	CStatic m_LKopfRech;
public:
	CNumEdit m_KopfRech;
public:
	CStatic m_LFussRech;
public:
	CNumEdit m_FussRech;
public:
	CStatic m_LFreitext;
public:
	CTextEdit m_Freitext;
public:
	CEdit m_KopfLSBez;
public:
	CEdit m_FussLSBez;
public:
	CEdit m_KopfRechBez;
public:
	CEdit m_FussRechBez;
public:
	CStatic m_LKunBran;
public:
	CComboBox m_KunBran;
	afx_msg void OnEnKillfocusKopflsbez();
	afx_msg void OnEnKillfocusFusslsbez();
	afx_msg void OnEnKillfocusKopfrechbez();
	afx_msg void OnEnKillfocusFussrechbez();
	afx_msg void OnEnChangeKopflsbez();
	afx_msg void OnEnChangeFusslsbez();
	afx_msg void OnEnChangeKopfrechbez();
	afx_msg void OnEnChangeFussrechbez();
	CComboBox m_Etiketten;
	CStatic m_LEtiketten;
	CButton m_ZollP;
	CStatic m_LZollP;
	CStatic m_LEmailRech;
	CButton m_EMailRech;
	CStatic m_LFreitext3;
	CTextEdit m_Freitext3;
	// Verladeschein
	CStatic m_LVerladeschein;
	CComboBox m_Verladeschein;
};
