#include "stdafx.h"
#include "ChoicePlz.h"
#include "DbUniCode.h"
#include "DbQuery.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoicePlz::Sort1 = -1;
int CChoicePlz::Sort2 = -1;
int CChoicePlz::Sort3 = -1;
int CChoicePlz::Sort4 = -1;
int CChoicePlz::Sort5 = -1;
int CChoicePlz::Sort6 = -1;

CChoicePlz::CChoicePlz(CWnd* pParent) 
        : CChoiceX(pParent)
{
      query = _T("");	
}

CChoicePlz::~CChoicePlz() 
{
	DestroyList ();
}

void CChoicePlz::DestroyList() 
{
	for (std::vector<CPlzList *>::iterator pabl = PlzList.begin (); pabl != PlzList.end (); ++pabl)
	{
		CPlzList *abl = *pabl;
		delete abl;
	}
    PlzList.clear ();
}

void CChoicePlz::FillList () 
{
    char  plz [21];
    TCHAR ort [37];
    TCHAR zusatz [37];
    TCHAR bundesland [37];
    TCHAR vorwahl [21];
    CString SqlString;

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Postleitzahlen"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Plz"),  1, 50, LVCFMT_RIGHT);
    SetCol (_T("Ort"),  2, 150);
    SetCol (_T("Zusatz"),  3, 150);
    SetCol (_T("Bundesland"),  4, 150);
    SetCol (_T("Vorwahl"),  5, 150);

	if (FirstRead && !HideFilter && query == _T(""))
	{
		FirstRead = FALSE;
		Load ();
		PostMessage (WM_COMMAND, IDC_FILTER, 0l);
		return;
	}
	if (PlzList.size () == 0)
	{
		DbClass->sqlout ((LPTSTR) plz,      SQLCHAR, sizeof (plz));
		DbClass->sqlout ((LPTSTR)  ort,   SQLCHAR, sizeof (ort));
		DbClass->sqlout ((LPTSTR)  zusatz,   SQLCHAR, sizeof (zusatz));
		DbClass->sqlout ((LPTSTR)  bundesland,   SQLCHAR, sizeof (bundesland));
		DbClass->sqlout ((LPTSTR)  vorwahl,   SQLCHAR, sizeof (vorwahl));
		SqlString = _T("select plz.plz, plz.ort, plz.zusatz, ")
                                                 _T("plz.bundesland, plz.vorwahl ")
			                             _T("from plz");
        if (query != "")
        {
             SqlString += _T(" ");
             SqlString += query; 
        }  

		if (QueryString != _T(""))
		{
			if (query == "")
			{
				SqlString += _T(" where ");
			}
			else
			{
				SqlString += _T(" and ");
			}
			SqlString += QueryString;
		}
                  
		int cursor = DbClass->sqlcursor (SqlString.GetBuffer ());
		while (DbClass->sqlfetch (cursor) == 0)
		{
			CPlzList *abl = new CPlzList ();
			LPSTR pos = (LPSTR) plz;
			CDbUniCode::DbToUniCode (plz, pos);
                        abl->plz = plz; 

			pos = (LPSTR) ort;
			CDbUniCode::DbToUniCode (ort, pos);
                        abl->ort = ort; 

			pos = (LPSTR) zusatz;
			CDbUniCode::DbToUniCode (zusatz, pos);
                        abl->zusatz = zusatz; 

			pos = (LPSTR) bundesland;
			CDbUniCode::DbToUniCode (bundesland, pos);
                        abl->bundesland = bundesland; 

			pos = (LPSTR) vorwahl;
			CDbUniCode::DbToUniCode (vorwahl, pos);
                        abl->vorwahl = vorwahl; 

			PlzList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CPlzList *>::iterator pabl = PlzList.begin (); pabl != PlzList.end (); ++pabl)
	{

		CPlzList *abl = *pabl;
        int ret = InsertItem (i, -1);
        ret = SetItemText (abl->plz.GetBuffer (), i, 1);
        ret = SetItemText (abl->ort.GetBuffer (), i, 2);
        ret = SetItemText (abl->zusatz.GetBuffer (), i, 3);
        ret = SetItemText (abl->bundesland.GetBuffer (), i, 4);
        ret = SetItemText (abl->vorwahl.GetBuffer (), i, 5);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort (listView);
}

/*
void CChoicePlz::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoicePlz::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoicePlz::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

	for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}
*/

void CChoicePlz::SearchCol (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, SortRow);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoicePlz::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    EditText.MakeUpper ();
    SearchCol (ListBox, EditText.GetBuffer ());
}



int CALLBACK CChoicePlz::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;

}


void CChoicePlz::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
        case 3:
              Sort4 *= -1;
              break;
        case 4:
              Sort5 *= -1;
              break;
        case 5:
              Sort6 *= -1;
              break;
    }

    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CPlzList *abl = PlzList [i];
		   
		   abl->plz         = ListBox->GetItemText (i, 1);
		   abl->ort         = ListBox->GetItemText (i, 2);
		   abl->zusatz      = ListBox->GetItemText (i, 3);
		   abl->bundesland  = ListBox->GetItemText (i, 4);
		   abl->vorwahl     = ListBox->GetItemText (i, 5);
	}
}

void CChoicePlz::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = PlzList [idx];
}

CPlzList *CChoicePlz::GetSelectedText ()
{
	return (CPlzList *) SelectedRow;
}


void CChoicePlz::OnFilter ()
{
	CDynDialog dlg;
	CDynDialogItem *Item;
	static int headercy = 200;

	dlg.DlgCaption = "Filter";
	dlg.UseFilter = UseFilter;

	if (vFilter.size () > 0)
	{
		for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
		{
			CDynDialogItem *i = *it;
			Item = new CDynDialogItem ();
			*Item = *i;
			if (Item->Name == _T("reset"))
			{
				if (UseFilter)
				{
					Item->Value = _T("aktiv");
				}
				else
				{
					Item->Value = _T("nicht aktiv");
				}
			}
			dlg.AddItem (Item);

		}
		dlg.Header.cy = headercy;	
	}
	else
	{
		int y = 10;
		Item = new CDynDialogItem ();
		Item->Name = _T("lplz");
		Item->CtrClass = "static";
		Item->Value = _T("PLZ");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1001;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("plz");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1002;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lort");
		Item->CtrClass = "static";
		Item->Value = _T("Ort");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1003;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("ort");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lbundesland");
		Item->CtrClass = "static";
		Item->Value = _T("Bundesland");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1005;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("bundesland");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1006;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lvorwahl");
		Item->CtrClass = "static";
		Item->Value = _T("Vorwahl");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1007;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("vorwahl");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1008;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		dlg.Header.cy = y + 30;
		headercy = dlg.Header.cy;

		Item = new CDynDialogItem ();
		Item->Name = _T("OK");
		Item->CtrClass = "button";
		Item->Value = _T("OK");
		Item->Template.x = 15;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDOK;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("cancel");
		Item->CtrClass = "button";
		Item->Value = _T("abbrechen");
		Item->Template.x = 70;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDCANCEL;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("reset");
		Item->CtrClass = "button";
		if (UseFilter)
		{
			Item->Value = _T("aktiv");
		}
		else
		{
			Item->Value = _T("nicht aktiv");
		}
		Item->Template.x = 125;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = ID_USE;
		dlg.AddItem (Item);
	}

	dlg.CreateModal ();
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK) return;
 
	UseFilter = dlg.UseFilter;
	int count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = dlg.Items.begin (); it != dlg.Items.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (count >= (int) vFilter.size ())
			{
				Item = new CDynDialogItem ();
				*Item = *i;
				vFilter.push_back (Item);
			}
			*vFilter[count] = *i;
			count ++;
	}
    CString Query;
    CDbQuery DbQuery;

	QueryString = _T("");
	if (!UseFilter)
	{
		FillList ();
		return;
	}
	count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (strcmp (i->CtrClass, "edit") != 0) continue; 
			if (i->Value.Trim () != _T(""))
			{
				if (i->Type == i->String || i->Type == i->Date)
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, TRUE);
				}
				else
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, FALSE);
				}
				if (count > 0)
				{
					QueryString += _T(" and ");
				}
                QueryString += Query;
				count ++;
			}
	}
	FillList ();
}
