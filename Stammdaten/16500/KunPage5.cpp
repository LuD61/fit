// KunPage5.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "KunPage5.h"
#include "DbPropertyPage.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "token.h"
#include "ptabn.h"
#include "gr_zuord.h"
#include "fil.h"
#include "rab_stufek.h"
#include "haus_bank.h"
#include "dta.h"




// CKunPage5-Dialogfeld

IMPLEMENT_DYNAMIC(CKunPage5, CDbPropertyPage)

CKunPage5::CKunPage5()
	: CDbPropertyPage(CKunPage5::IDD)
{
	hBrush = NULL;
	hBrushStatic = NULL;
	Search = _T("");

	CUniFormField::Code = &Code;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Kun);
}

CKunPage5::~CKunPage5()
{
	
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
}

void CKunPage5::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_KUNNAME, m_KunName);
	DDX_Control(pDX, IDC_HEADBORDER, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDER, m_DataBorder);
	DDX_Control(pDX, IDC_LDTA, m_LDta);
	DDX_Control(pDX, IDC_LEDITYP, m_LEdiTyp);
	DDX_Control(pDX, IDC_EDITYP, m_EdiTyp);
	DDX_Control(pDX, IDC_LE, m_LE);
	DDX_Control(pDX, IDC_LC, m_LC);
	DDX_Control(pDX, IDC_DATABORDER4, m_DataBorder4);
	DDX_Control(pDX, IDC_LDTABEZ, m_LDtaBez);
	DDX_Control(pDX, IDC_DTABEZ, m_DtaBez);
	DDX_Control(pDX, IDC_LDTAILN, m_LDtaILN);
	DDX_Control(pDX, IDC_DTAILN, m_DtaILN);
	DDX_Control(pDX, IDC_LDTAKZ, m_LDtaKZ);
	DDX_Control(pDX, IDC_DTAKZ, m_DtaKZ);
	DDX_Control(pDX, IDC_LDTATYP, m_LDtaTyp);
	DDX_Control(pDX, IDC_COMBO1, m_DtaTyp);
	DDX_Control(pDX, IDC_LKETTE, m_LKette);
	DDX_Control(pDX, IDC_KETTE, m_Kette);
	DDX_Control(pDX, IDC_LDTANR1, m_LDtaNr1);
	DDX_Control(pDX, IDC_DTANR1, m_DtaNr1);
	DDX_Control(pDX, IDC_LDTANR2, m_LDtaNr2);
	DDX_Control(pDX, IDC_DTANR2, m_DtaNr2);
	DDX_Control(pDX, IDC_LDTANR3, m_LDtaNr3);
	DDX_Control(pDX, IDC_DTANR3, m_DtaNr3);
	DDX_Control(pDX, IDC_LDTAVB1, m_LDtaVb1);
	DDX_Control(pDX, IDC_DTAVB1, m_DtaVb1);
	DDX_Control(pDX, IDC_LDTAVB2, m_LDtaVb2);
	DDX_Control(pDX, IDC_DTAVB2, m_DtaVb2);
	DDX_Control(pDX, IDC_LDTAVB3, m_LDtaVb3);
	DDX_Control(pDX, IDC_DTAVB3, m_DtaVb3);
	DDX_Control(pDX, IDC_DATABORDER5, m_DataBorder5);
	DDX_Control(pDX, IDC_DTANUMMER, m_DtaNummer);
}


BEGIN_MESSAGE_MAP(CKunPage5, CDbPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_KILLFOCUS ()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_CBN_SELCHANGE(IDC_DTANUMMER, &CKunPage5::OnCbnSelchangeDtanummer)
END_MESSAGE_MAP()


// CKunPage5-Meldungshandler


void CKunPage5::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CKunPage5::OnInitDialog()
{
	CDialog::OnInitDialog();

	F5Pressed = FALSE;
	KunProperty = (CPropertySheet *) GetParent ();
	View = (DbFormView *) KunProperty->GetParent ();
	Frame = (CFrameWnd *) View->GetParent ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();

	CRect bRect (0, 0, 100, 20);;

	Form.Add (new CFormField (&m_Mdn,EDIT,        (short *)  &Mdn->mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *)   MdnAdr->adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Kun,EDIT,        (long *)   &Kun->kun.kun, VLONG));
    Form.Add (new CUniFormField (&m_KunName,EDIT, (char *)   KunAdr->adr.adr_krz, VCHAR));
	Form.Add (new CUniFormField (&m_DtaNummer,COMBOBOX, (long *)   &Kun->kun.sedas_dta, VLONG));
	Form.Add (new CUniFormField (&m_EdiTyp,EDIT, (char *)   Kun->kun.edi_typ , VCHAR));
	Form.Add (new CUniFormField (&m_DtaBez,EDIT, (char *)   Dta->dta.kun_krz1, VCHAR));
	Form.Add (new CUniFormField (&m_DtaILN,EDIT, (char *)   Dta->dta.iln, VCHAR));
	Form.Add (new CUniFormField (&m_DtaKZ,EDIT, (short *)   &Dta->dta.test_kz, VSHORT));
	Form.Add (new CUniFormField (&m_DtaTyp,COMBOBOX, (short *)   &Dta->dta.typ, VSHORT));
	Form.Add (new CUniFormField (&m_Kette,COMBOBOX, (short *)   &Kun->kun.sedas_abr, VSHORT));
	Form.Add (new CUniFormField (&m_DtaNr1,EDIT, (char *)   Kun->kun.sedas_nr1, VCHAR));
	Form.Add (new CUniFormField (&m_DtaNr2,EDIT, (char *)   Kun->kun.sedas_nr2, VCHAR));
	Form.Add (new CUniFormField (&m_DtaNr3,EDIT, (char *)   Kun->kun.sedas_nr3, VCHAR));
	Form.Add (new CUniFormField (&m_DtaVb1,EDIT, (short *)   &Kun->kun.sedas_vb1, VSHORT));
	Form.Add (new CUniFormField (&m_DtaVb2,EDIT, (short *)   &Kun->kun.sedas_vb2, VSHORT));
	Form.Add (new CUniFormField (&m_DtaVb3,EDIT, (short *)   &Kun->kun.sedas_vb3, VSHORT));
	
	FillPtabCombo (&m_Kette, _T("sedas_abt"));	// 240812 : ins Feld kun.abr gehoeren die Werte von ptabn : "sedas_abt" auch nix ist erlaubt
	FillPtabCombo (&m_DtaTyp, _T("dta_typ"));
	FillDta();

	
	
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 1, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	

	KunGrid.Create (this, 1, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	

	
	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 4);
    c_HeadBorder->rightspace = 20; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 3, 1, 4, 1);
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LKun = new CCtrlInfo (&m_LKun, 1, 2, 1, 1);
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid = new CCtrlInfo (&KunGrid, 2, 2, 1, 1);
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 3, 2, 4, 1);
	CtrlGrid.Add (c_KunName);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 4, DOCKRIGHT, 5);
	c_DataBorder->rightspace = 20;
	CtrlGrid.Add (c_DataBorder);

	CCtrlInfo *c_DataBorder4 = new CCtrlInfo (&m_DataBorder4, 0, 9, 3, DOCKBOTTOM);
    c_DataBorder4->rightspace = 20;
	CtrlGrid.Add (c_DataBorder4);

	CCtrlInfo *c_DataBorder5 = new CCtrlInfo (&m_DataBorder5, 3, 9, DOCKRIGHT, DOCKBOTTOM);
    c_DataBorder5->rightspace = 20;
	CtrlGrid.Add (c_DataBorder5);

	CCtrlInfo *c_LEdiTyp = new CCtrlInfo (&m_LEdiTyp, 1, 6, 1, 1);
	CtrlGrid.Add (c_LEdiTyp);
	CCtrlInfo *c_EdiTyp = new CCtrlInfo (&m_EdiTyp, 2, 6, 1, 1);
	CtrlGrid.Add (c_EdiTyp);
	
	CCtrlInfo *c_LDta = new CCtrlInfo (&m_LDta, 1, 11, 1, 1);
	CtrlGrid.Add (c_LDta);
	CCtrlInfo *c_Dta = new CCtrlInfo (&m_DtaNummer, 2, 11, 1, 1);
	CtrlGrid.Add (c_Dta);
	
	CCtrlInfo *c_LKette = new CCtrlInfo (&m_LKette, 1, 13, 1, 1);
	CtrlGrid.Add (c_LKette);
	CCtrlInfo *c_Kette = new CCtrlInfo (&m_Kette, 2, 13, 1, 1);
	CtrlGrid.Add (c_Kette);

	CCtrlInfo *c_LDtaNr1 = new CCtrlInfo (&m_LDtaNr1, 1, 15, 1, 1);
	CtrlGrid.Add (c_LDtaNr1);
	CCtrlInfo *c_DtaNr1 = new CCtrlInfo (&m_DtaNr1, 2, 15, 1, 1);
	CtrlGrid.Add (c_DtaNr1);

	CCtrlInfo *c_LDtaNr2 = new CCtrlInfo (&m_LDtaNr2, 1, 16, 1, 1);
	CtrlGrid.Add (c_LDtaNr2);
	CCtrlInfo *c_DtaNr2 = new CCtrlInfo (&m_DtaNr2, 2, 16, 1, 1);
	CtrlGrid.Add (c_DtaNr2);

	CCtrlInfo *c_LDtaNr3 = new CCtrlInfo (&m_LDtaNr3, 1, 17, 1, 1);
	CtrlGrid.Add (c_LDtaNr3);
	CCtrlInfo *c_DtaNr3 = new CCtrlInfo (&m_DtaNr3, 2, 17, 1, 1);
	CtrlGrid.Add (c_DtaNr3);

	CCtrlInfo *c_LDtaVb1 = new CCtrlInfo (&m_LDtaVb1, 1, 19, 1, 1);
	CtrlGrid.Add (c_LDtaVb1);
	CCtrlInfo *c_DtaVb1 = new CCtrlInfo (&m_DtaVb1, 2, 19, 1, 1);
	CtrlGrid.Add (c_DtaVb1);

	CCtrlInfo *c_LDtaVb2 = new CCtrlInfo (&m_LDtaVb2, 1, 20, 1, 1);
	CtrlGrid.Add (c_LDtaVb2);
	CCtrlInfo *c_DtaVb2 = new CCtrlInfo (&m_DtaVb2, 2, 20, 1, 1);
	CtrlGrid.Add (c_DtaVb2);

	CCtrlInfo *c_LDtaVb3 = new CCtrlInfo (&m_LDtaVb3, 1, 21, 1, 1);
	CtrlGrid.Add (c_LDtaVb3);
	CCtrlInfo *c_DtaVb3 = new CCtrlInfo (&m_DtaVb3, 2, 21, 1, 1);
	CtrlGrid.Add (c_DtaVb3);


	CCtrlInfo *c_LDtaBez = new CCtrlInfo (&m_LDtaBez, 4, 11, 1, 1);
	CtrlGrid.Add (c_LDtaBez);
	CCtrlInfo *c_DtaBez = new CCtrlInfo (&m_DtaBez, 5, 11, 1, 1);
	c_DtaBez->rightspace = 40;
	CtrlGrid.Add (c_DtaBez);

	CCtrlInfo *c_LDtaILN = new CCtrlInfo (&m_LDtaILN, 4, 13, 1, 1);
	CtrlGrid.Add (c_LDtaILN);
	CCtrlInfo *c_DtaILN = new CCtrlInfo (&m_DtaILN, 5, 13, 1, 1);
	c_DtaILN->rightspace = 40;
	CtrlGrid.Add (c_DtaILN);

	CCtrlInfo *c_LDtaKZ = new CCtrlInfo (&m_LDtaKZ, 4, 15, 1, 1);
	CtrlGrid.Add (c_LDtaKZ);
	CCtrlInfo *c_DtaKZ = new CCtrlInfo (&m_DtaKZ, 5, 15, 1, 1);
	c_DtaKZ->rightspace = 40;
	CtrlGrid.Add (c_DtaKZ);

	CCtrlInfo *c_LDtaTyp = new CCtrlInfo (&m_LDtaTyp, 4, 17, 1, 1);
	CtrlGrid.Add (c_LDtaTyp);
	CCtrlInfo *c_DtaTyp = new CCtrlInfo (&m_DtaTyp, 5, 17, 1, 1);
	c_DtaTyp->rightspace = 40;
	CtrlGrid.Add (c_DtaTyp);
	
		
	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	CtrlGrid.Display ();
    CtrlGrid.SetItemCharHeight (&m_HeadBorder, 3);
    EnableFields (TRUE);

	m_DtaBez.EnableWindow(FALSE);
	m_DtaILN.EnableWindow(FALSE);
	m_DtaKZ.EnableWindow(FALSE);
	m_DtaTyp.EnableWindow(FALSE);

	FillDtaInfo();	
	
	Form.Show ();

	return TRUE;
}


BOOL CKunPage5::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				Update->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Form.Get();
				Update->Write();
				return TRUE;
			}

			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}
			if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

HBRUSH CKunPage5::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);

    if (hBrush == NULL)
	{
		  hBrush = CreateSolidBrush (Color);
		  hBrushStatic = CreateSolidBrush (Color);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			return hBrush;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CButton )))
	{
            pDC->SetBkColor (Color);
			pDC->SetBkMode (TRANSPARENT);
			return hBrushStatic;
	}
	return CPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CKunPage5::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CKunPage5::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}


void CKunPage5::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CKunPage5::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}

void CKunPage5::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


	if (m_Mdn.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein SDatz zum L�schen selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	if (MessageBox (_T("Preisauszeichnerdaten l�schen"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		Kun->beginwork ();
		Kun->dbdelete ();
		Kun->commitwork ();
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		EnableFields (FALSE);
		Form.Show ();
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);
	}
}

void CKunPage5::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
//		Control->SetReadOnly (!enable);
        Control->EnableWindow (enable);
	}
}

BOOL CKunPage5::StepBack ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
					Frame->DestroyWindow ();
					return FALSE;
	}
	else
	{
		EnableFields (FALSE);
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);	
	}
	return TRUE;
}


void CKunPage5::OnCopy ()
{
    OnEditCopy ();
}  



void CKunPage5::OnPaste ()
{
	OnEditPaste ();
}

void CKunPage5::OnCopyEx ()
{

//	if (!m_A.IsWindowEnabled ())
	{
		Copy ();
		return;
	}
}

void CKunPage5::OnPasteEx ()
{

//	if (!m_A.IsWindowEnabled () && Paste ())
	{
		Kun->CopyData (&kun);
		Kun_erw->CopyData (&kun_erw);
		Form.Show ();
/*
		if (KunPageEx != NULL && IsWindow (KunPageEx->m_hWnd))
		{
			KunPageEx->Form.Show ();
		}
*/
		return;
	}
}

BOOL CKunPage5::Copy ()
{
	Form.Get ();
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return FALSE;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      int err = GetLastError ();
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return FALSE;  
    }

	try
	{
        UINT CF_A_KUN_GX = RegisterClipboardFormat (_T("kun")); 
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, sizeof (KUN)); 
        char *p = (char *) GlobalLock(hglbCopy);
        memcpy (p, &Kun->kun, sizeof (Kun->kun));
        GlobalUnlock(hglbCopy); 
		HANDLE cData;
		cData = ::SetClipboardData( CF_A_KUN_GX, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
    return TRUE;
}

BOOL CKunPage5::Paste ()
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return FALSE;
    }

    HGLOBAL hglbCopy;
    UINT CF_A_KUN_GX;
    _TCHAR name [256];
    CF_A_KUN_GX = EnumClipboardFormats (0);
	if (CF_A_KUN_GX == 0)
	{
		int err = GetLastError ();
		CloseClipboard();
		return FALSE;
	}

    int ret;
    while ((ret = GetClipboardFormatName (CF_A_KUN_GX, name, sizeof (name))) != 0)
    { 
         CString Name = name;
         if (Name == _T("a_kun_gx"))
         {
              break;
         }  
		 CF_A_KUN_GX = EnumClipboardFormats (CF_A_KUN_GX);
    }
    if (ret == 0)
    {
         CloseClipboard();
         return FALSE;
    }    

    try
    {	
  	     hglbCopy =  ::GetClipboardData(CF_A_KUN_GX);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
 	     LPSTR p = (LPSTR) GlobalLock ((HGLOBAL) hglbCopy);
         memcpy ( (LPSTR) &kun, p, sizeof (kun));
  	     GlobalUnlock ((HGLOBAL) hglbCopy);
    }
    catch (...) {};
    CloseClipboard();
    return TRUE; 
}

void CKunPage5::EnableFields (BOOL b)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (!b)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);

}


void CKunPage5::OnKillFocus (CWnd *newFocus)
{
	CWnd *Control = GetFocus ();
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		if (f->Scale > 0)
		{
			f->Show ();
		}
	}
}


void CKunPage5::FillPtabCombo (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem)); 
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert)); 
		Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0); 

		
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn->sqlcursor (Sql.GetBuffer ());
		while (Ptabn->sqlfetch (cursor) == 0)
		{
			Ptabn->dbreadfirst();

			LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn->ptabn.ptwert,
				                             Ptabn->ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}










void CKunPage5::FillDta ()
{
	CFormField *f = Form.GetFormField (&m_DtaNummer);
	if (f != NULL)
	{
		f->ComboValues.clear ();

		Dta->sqlout ((long *) &Dta->dta.dta, SQLLONG, 0);
		Dta->sqlout ((char *) Dta->dta.kun_krz1, SQLCHAR, sizeof(Dta->dta.kun_krz1));
		
	    int cursor = Dta->sqlcursor (_T("select dta, kun_krz1 from dta ")
			                          _T("where dta > 0 ")
									  _T("order by dta"));
		
		
		
		while (Dta->sqlfetch (cursor) == 0)
		{
			Dta->dbreadfirst ();

			//LPSTR pos = (LPSTR) Iprgrstufk.iprgrstufk.pr_gr_stuf;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			//pos = (LPSTR) Iprgrstufk.iprgrstufk.zus_bz;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%ld %s"), Dta->dta.dta,
												Dta->dta.kun_krz1);
		    f->ComboValues.push_back (ComboValue); 
		}
		Dta->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}


void CKunPage5::FillDtaInfo ()
{

		
		Dta->sqlin ((long *) &Kun->kun.sedas_dta, SQLLONG, 0);
		Dta->sqlout ((char *) Dta->dta.kun_krz1, SQLCHAR, sizeof (Dta->dta.kun_krz1));
		Dta->sqlout ((char *) Dta->dta.iln, SQLCHAR, sizeof (Dta->dta.iln));
		Dta->sqlout ((short *) &Dta->dta.typ, SQLSHORT, 0);
		Dta->sqlout ((short *) &Dta->dta.test_kz, SQLSHORT, 0);
		

		int cursor2 = Dta->sqlcursor (_T("select kun_krz1, iln, typ, test_kz from dta ")
										_T("where dta.dta = ? "));
										
									
		Dta->sqlopen(cursor2);
		

		int sqlstatus = Dta->sqlfetch (cursor2);

		if (sqlstatus == 100)
		{
			strcpy_s(Dta->dta.kun_krz1,"Kein DTA gew�hlt");
			strcpy_s(Dta->dta.iln,"");
			
			Dta->dta.typ = 0;
			Dta->dta.test_kz= 0;
			
		}

		Form.Show();
				
		Dta->sqlclose (cursor2);
}





void CKunPage5::OnCbnSelchangeDtanummer()
{
	Form.Get();
	FillDtaInfo();
}

BOOL CKunPage5::OnKillActive ()
{
	Form.Get ();
	if (F5Pressed)
	{
		return TRUE;
	}
	return TRUE;
}

BOOL CKunPage5::OnSetActive ()
{
	F5Pressed = FALSE;
	Form.Show ();
	return TRUE;
}

void CKunPage5::UpdatePage ()
{
	Form.Show ();
}
