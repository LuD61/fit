#ifndef _GR_ZUORD_DEF
#define _GR_ZUORD_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct GR_ZUORD {
   short          gr;
   TCHAR          gr_bz1[25];
   TCHAR          gr_bz2[25];
   short          mdn;
   TCHAR          smt_kz[2];
};
extern struct GR_ZUORD gr_zuord, gr_zuord_null;

class GR_ZUORD_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               GR_ZUORD gr_zuord;
               GR_ZUORD_CLASS () : DB_CLASS ()
               {
               }
};
#endif
