#ifndef _H1_DEF
#define _H1_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct H1 {
   long           hirarchie1;
   TCHAR          h1_bez[37];
   TCHAR          h1_bezkrz[17];
};
extern struct H1 h1, h1_null;

#line 8 "H1.rh"

class H1_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               H1 h1;  
               H1_CLASS () : DB_CLASS ()
               {
               }
};
#endif
