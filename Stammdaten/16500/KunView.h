#pragma once
#include "afxcmn.h"
#include "CtrlGrid.h"
#include "afxwin.h"
#include "DbFormView.h"
#include "DbPropertySheet.h"
#include "KunPage.h"
#include "KunPage2.h"
#include "KunPage3.h"
#include "KunPage4.h"
#include "KunPage5.h"
#include "KunPage6.h"
#include "KunPage7.h"
#include "sys_par.h"
#include "FrachtKosten.h"
#include "haus_bank.h"
#include "dta.h"
#include "PageUpdate.h"





// CPazView-Formularansicht

class CKunView : public DbFormView,
						CPageUpdate
{
	DECLARE_DYNCREATE(CKunView)

protected:
	CKunView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CKunView();

public:
	enum { IDD = IDD_KUNVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    afx_msg void OnSize(UINT, int, int);
	

	DECLARE_MESSAGE_MAP()
public:
	int test;
	KUN_CLASS *Kun;
	KUN_ERW_CLASS *Kun_erw;
		
	CTabCtrl m_Tab;
	CCtrlGrid CtrlGrid;
	CCtrlGrid DataGrid;
	CStatic m_LMdn;
	CEdit m_Mdn;
	CDbPropertySheet *m_PropertySheet;
	CKunPage *m_KunPage;
	CKunPage2 *m_KunPage2;
	CKunPage3 *m_KunPage3;
	CKunPage4 *m_KunPage4;
	CKunPage5 *m_KunPage5;
	CKunPage6 *m_KunPage6;
	CKunPage7 *m_KunPage7;
	CFrachtKosten *m_FrachtKosten;
	HBRUSH hBrush;
	HBRUSH staticBrush;
	SYS_PAR_CLASS Sys_par;
	void DeletePropertySheet ();
	afx_msg void OnLanguage();
	afx_msg void OnFileSave();
	afx_msg void OnDelete();
	afx_msg void OnBack();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnTabcopy();
	afx_msg void OnTabpaste();
	afx_msg void OnTextCent();
	afx_msg void OnTextLeft();
	afx_msg void OnTextRight();

	virtual void Show ();
	virtual void Get ();
	virtual void Back ();
	virtual void Write ();
	virtual void Delete ();
	virtual void UpdateAll ();
	virtual void OnFilePrint();

};


