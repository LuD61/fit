#include "StdAfx.h"
#include "line.h"

CLine::CLine(void)
{
}

CLine::~CLine(void)
{
}

void CLine::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	HDC hDC = lpDrawItemStruct->hDC;
	CDC cDC;
	cDC.Attach (hDC);
	CPen blackPen (PS_SOLID, 1, RGB (120, 120, 120));
	CPen grayPen (PS_SOLID, 2, RGB (190, 190, 190));
	CPen whitePen (PS_SOLID, 1, RGB (255, 255, 255));
	CPen * oldPen = (CPen *) cDC.SelectObject (&blackPen);
	CRect rect;
	GetClientRect (&rect);
	cDC.MoveTo (0,0);
	cDC.LineTo (rect.right, 0);
	cDC.SelectObject (&grayPen);
	cDC.MoveTo (0,2);
//	cDC.LineTo (rect.right, 2);
	cDC.SelectObject (&whitePen);
	cDC.MoveTo (0,1);
	cDC.LineTo (rect.right, 1);
	cDC.SelectObject (oldPen);
}
