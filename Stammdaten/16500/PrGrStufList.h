#ifndef _PR_GR_STUF_LIST_DEF
#define _PR_GR_STUF_LIST_DEF
#pragma once

class CPrGrStufList
{
public:
	short mdn;
	long pr_gr_stuf;
	CString zus_bz;
	CPrGrStufList(void);
	CPrGrStufList(short, long, LPTSTR);
	~CPrGrStufList(void);
};
#endif
