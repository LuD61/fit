// KunPage7.cpp: Implementierungsdatei
//
#include "stdafx.h"
#include "16500.h"
#include "KunPage7.h"
#include "DbPropertyPage.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "token.h"
#include "ptabn.h"
#include "gr_zuord.h"
#include "fil.h"
#include "rab_stufek.h"
#include "haus_bank.h"
#include "ChoiceH.h"
#include "StrFuncs.h"
#include "HierarchieDialog.h"


// CKunPage7-Dialogfeld

IMPLEMENT_DYNAMIC(CKunPage7, CDbPropertyPage)

CKunPage7::CKunPage7()
	: CDbPropertyPage(CKunPage7::IDD)
{
	hBrush = NULL;
	hBrushStatic = NULL;
	Search = _T("");

	CUniFormField::Code = &Code;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Kun);
	TestJrChange = TRUE;
}

CKunPage7::~CKunPage7()
{

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
	H1.dbclose ();
	H2.dbclose ();
	H3.dbclose ();
	H4.dbclose ();
}

void CKunPage7::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_KUNNAME, m_KunName);
	DDX_Control(pDX, IDC_HEADBORDER, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDER, m_DataBorder);
	DDX_Control(pDX, IDC_DATABORDER2, m_DataBorder2);
	DDX_Control(pDX, IDC_LBANK, m_LBank);
	DDX_Control(pDX, IDC_BANK, m_Bank);
	DDX_Control(pDX, IDC_LBLZ, m_LBlz);
	DDX_Control(pDX, IDC_BLZ, m_Blz);
	DDX_Control(pDX, IDC_LKTO, m_LKto);
	DDX_Control(pDX, IDC_KTO, m_Kto);
	DDX_Control(pDX, IDC_SWIFT, m_SWIFT);
	DDX_Control(pDX, IDC_LSWIFT, m_LSWIFT);
	DDX_Control(pDX, IDC_IBAN, m_IBAN);
	DDX_Control(pDX, IDC_LIBAN, m_LIBAN);
	DDX_Control(pDX, IDC_LHAUSBANK, m_LHausbank);
	DDX_Control(pDX, IDC_HAUSBANK, m_Hausbank);
	DDX_Control(pDX, IDC_STEUER, m_Steuer);
	DDX_Control(pDX, IDC_UST, m_Ust);
	DDX_Control(pDX, IDC_LSteuer, m_LSteuer);
	DDX_Control(pDX, IDC_LUST, m_LUst);
	DDX_Control(pDX, IDC_LEG, m_LEG);
	DDX_Control(pDX, IDC_EG, m_EG);
	DDX_Control(pDX, IDC_GP, m_GP);
	DDX_Control(pDX, IDC_LGP, m_LGP);
	DDX_Control(pDX, IDC_STATKUN, m_StatKun);
	DDX_Control(pDX, IDC_LSTATKUN, m_LStatKun);
	DDX_Control(pDX, IDC_HIERARCHIE_GROUP, m_HierarchieGroup);
	DDX_Control(pDX, IDC_H4ARROW, m_H4Arrow);
	DDX_Control(pDX, IDC_H3ARROW, m_H3Arrow);
	DDX_Control(pDX, IDC_H2ARROW, m_H2Arrow);
	DDX_Control(pDX, IDC_LH4, m_LH4);
	DDX_Control(pDX, IDC_H4, m_H4);
	DDX_Control(pDX, IDC_LH3, m_LH3);
	DDX_Control(pDX, IDC_H3, m_H3);
	DDX_Control(pDX, IDC_LH2, m_LH2);
	DDX_Control(pDX, IDC_H2, m_H2);
	DDX_Control(pDX, IDC_LH1, m_LH1);
	DDX_Control(pDX, IDC_H1, m_H1);
	DDX_Control(pDX, IDC_H4_BEZ, m_H4Bez);
	DDX_Control(pDX, IDC_H3_BEZ, m_H3Bez);
	DDX_Control(pDX, IDC_H2_BEZ, m_H2Bez);
	DDX_Control(pDX, IDC_H1_BEZ, m_H1Bez);
	DDX_Control(pDX, IDC_HDATEN_GROUP, m_HdatenGroup);
	DDX_Control(pDX, IDC_LJAHR, m_LJahr);
	DDX_Control(pDX, IDC_JAHR, m_Jahr);
	DDX_Control(pDX, IDC_SPIN_JAHR, m_SpinJahr);
	DDX_Control(pDX, IDC_LRV, m_LRv);
	DDX_Control(pDX, IDC_RV, m_Rv);
	DDX_Control(pDX, IDC_LZR, m_LZr);
	DDX_Control(pDX, IDC_ZR, m_Zr);

	DDX_Control(pDX, IDC_LTEURO_MONAT, m_LTeuroMonat);
	DDX_Control(pDX, IDC_LM01, m_LM01);
	DDX_Control(pDX, IDC_LM02, m_LM02);
	DDX_Control(pDX, IDC_LM03, m_LM03);
	DDX_Control(pDX, IDC_LM04, m_LM04);
	DDX_Control(pDX, IDC_LM05, m_LM05);
	DDX_Control(pDX, IDC_LM06, m_LM06);
	DDX_Control(pDX, IDC_LM07, m_LM07);
	DDX_Control(pDX, IDC_LM08, m_LM08);
	DDX_Control(pDX, IDC_LM09, m_LM09);
	DDX_Control(pDX, IDC_LM10, m_LM10);
	DDX_Control(pDX, IDC_LM11, m_LM11);
	DDX_Control(pDX, IDC_LM12, m_LM12);

	DDX_Control(pDX, IDC_PLUMS_M01, m_PlumsM01);
	DDX_Control(pDX, IDC_PLUMS_M02, m_PlumsM02);
	DDX_Control(pDX, IDC_PLUMS_M03, m_PlumsM03);
	DDX_Control(pDX, IDC_PLUMS_M04, m_PlumsM04);
	DDX_Control(pDX, IDC_PLUMS_M05, m_PlumsM05);
	DDX_Control(pDX, IDC_PLUMS_M06, m_PlumsM06);
	DDX_Control(pDX, IDC_PLUMS_M07, m_PlumsM07);
	DDX_Control(pDX, IDC_PLUMS_M08, m_PlumsM08);
	DDX_Control(pDX, IDC_PLUMS_M09, m_PlumsM09);
	DDX_Control(pDX, IDC_PLUMS_M10, m_PlumsM10);
	DDX_Control(pDX, IDC_PLUMS_M11, m_PlumsM11);
	DDX_Control(pDX, IDC_PLUMS_M12, m_PlumsM12);
	DDX_Control(pDX, IDC_HDIALOG,   m_HDialog);
	DDX_Control(pDX, IDC_LMANDATREF, m_LMandatRef);
	DDX_Control(pDX, IDC_MANDATREF, m_MandatRef);
	DDX_Control(pDX, IDC_LTAGESEPA, m_LAnzTageSepa);
	DDX_Control(pDX, IDC_TAGESEPA, m_AnzTageSepa);
	DDX_Control(pDX, IDC_SEPAART, m_SepaArt);
	DDX_Control(pDX, IDC_LSEPAART, m_LSepaArt);
	DDX_Control(pDX, IDC_LMANDATSEIT, m_LMandatSeit);
	DDX_Control(pDX, IDC_MANDSEIT, m_MandSeit);
	DDX_Control(pDX, IDC_LSBUCH, m_LSBuch);
	DDX_Control(pDX, IDC_SBUCH, m_SBuch);
}


BEGIN_MESSAGE_MAP(CKunPage7, CDbPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_KILLFOCUS ()
	ON_BN_CLICKED(IDC_H4CHOICE , OnH4Choice)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_EN_CHANGE(IDC_JAHR, &CKunPage7::OnEnChangeJahr)
	ON_COMMAND(IDC_HDIALOG, OnHdialog)
	ON_STN_CLICKED(IDC_LSWIFT, &CKunPage7::OnStnClickedLswift)
	ON_BN_CLICKED(IDC_SBUCH, &CKunPage7::OnBnClickedSbuch)
END_MESSAGE_MAP()


// CKunPage7-Meldungshandler

void CKunPage7::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CKunPage7::OnInitDialog()
{
	CDialog::OnInitDialog();

	F5Pressed = FALSE;
	KunProperty = (CPropertySheet *) GetParent ();
	View = (DbFormView *) KunProperty->GetParent ();
	Frame = (CFrameWnd *) View->GetParent ();

	m_HDialog.nID = IDC_HDIALOG;
	m_HDialog.SetWindowText (_T("Hierarchien bearbeiten"));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();

	CRect bRect (0, 0, 100, 20);;

	Form.Add (new CFormField (&m_Mdn,EDIT,        (short *)  &Mdn->mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *)   MdnAdr->adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Kun,EDIT,        (long *)   &Kun->kun.kun, VLONG));
	Form.Add (new CUniFormField (&m_KunName,EDIT, (char *)   Kun->kun.kun_krz1, VCHAR));
	Form.Add (new CUniFormField (&m_Bank,EDIT, (char *)   Kun->kun.bank_kun, VCHAR));
	Form.Add (new CUniFormField (&m_Blz,EDIT, (long *)   &Kun->kun.blz, VLONG));
	Form.Add (new CUniFormField (&m_Kto,EDIT, (char *)   Kun->kun.kto_nr, VCHAR));
	Form.Add (new CUniFormField (&m_SWIFT,EDIT, (char *)   RechAdr->adr.swift, VCHAR));
	Form.Add (new CUniFormField (&m_IBAN,EDIT, (char *)   RechAdr->adr.iban, VCHAR));
	Form.Add (new CUniFormField (&m_MandatRef,EDIT, (char *)   Kun->kun.mandatref, VCHAR));
	Form.Add (new CFormField (&m_MandSeit, DATETIMEPICKER,  (DATE_STRUCT *)   &Kun_erw->kun_erw.mandseit , VDATE));
	Form.Add (new CUniFormField (&m_Hausbank,COMBOBOX, (short *)   &Kun->kun.hausbank, VSHORT));
	Form.Add (new CUniFormField (&m_Steuer,EDIT, (char *)   Kun->kun.ust_nummer, VCHAR));
	Form.Add (new CUniFormField (&m_Ust,EDIT, (char *)   Kun->kun.ust_id16, VCHAR));
	Form.Add (new CUniFormField (&m_EG,COMBOBOX, (short *)   &Kun->kun.eg_kz, VSHORT));
	Form.Add (new CUniFormField (&m_GP,COMBOBOX, (char *)   Kun->kun.gn_pkt_kz, VCHAR));
	Form.Add (new CUniFormField (&m_StatKun,EDIT, (long *)   &Kun->kun.stat_kun, VLONG));
	Form.Add (new CUniFormField (&m_SepaArt,COMBOBOX, (short *)   &Kun->kun.sepaart, VSHORT));
	Form.Add (new CUniFormField (&m_AnzTageSepa,EDIT, (short *)   &Kun->kun.tagesepa, VSHORT));
	Form.Add (new CUniFormField (&m_SBuch,CHECKBOX, (char *)   Kun_erw->kun_erw.sambuch, VCHAR));
	Form.Add (new CFormField (&m_H4,EDIT, (long *)   &H4.h4.hirarchie4, VLONG));
	Form.Add (new CFormField (&m_H3,EDIT, (long *)   &H3.h3.hirarchie3, VLONG));
	Form.Add (new CFormField (&m_H2,EDIT, (long *)   &H2.h2.hirarchie2, VLONG));
	Form.Add (new CFormField (&m_H1,EDIT, (long *)   &H1.h1.hirarchie1, VLONG));
/* ----> 060912 Boese, boese
	Form.Add (new CFormField (&m_H4Bez,EDIT, (char *) &H4.h4.h4_bez, VCHAR));
	Form.Add (new CFormField (&m_H3Bez,EDIT, (char *) &H3.h3.h3_bez, VCHAR));
	Form.Add (new CFormField (&m_H2Bez,EDIT, (char *) &H2.h2.h2_bez, VCHAR));
	Form.Add (new CFormField (&m_H1Bez,EDIT, (char *) &H1.h1.h1_bez, VCHAR));
< ------ */

	Form.Add (new CFormField (&m_H4Bez,EDIT, (char *) H4.h4.h4_bez, VCHAR));
	Form.Add (new CFormField (&m_H3Bez,EDIT, (char *) H3.h3.h3_bez, VCHAR));
	Form.Add (new CFormField (&m_H2Bez,EDIT, (char *) H2.h2.h2_bez, VCHAR));
	Form.Add (new CFormField (&m_H1Bez,EDIT, (char *) H1.h1.h1_bez, VCHAR));

	Form.Add (new CFormField (&m_Jahr, EDIT, (char *) &Hdaten.hdaten.jr, VSHORT));
	Form.Add (new CFormField (&m_Rv, EDIT, (char *)  &Hdaten.hdaten.rv_proz, VDOUBLE, 6, 2));
	Form.Add (new CFormField (&m_Zr, EDIT, (char *)  &Hdaten.hdaten.zr_proz, VDOUBLE, 6, 2));
	Form.Add (new CFormField (&m_PlumsM01, EDIT, (long *) &Hdaten.hdaten.plums_m01, VLONG));
	Form.Add (new CFormField (&m_PlumsM02, EDIT, (long *) &Hdaten.hdaten.plums_m02, VLONG));
	Form.Add (new CFormField (&m_PlumsM03, EDIT, (long *) &Hdaten.hdaten.plums_m03, VLONG));
	Form.Add (new CFormField (&m_PlumsM04, EDIT, (long *) &Hdaten.hdaten.plums_m04, VLONG));
	Form.Add (new CFormField (&m_PlumsM05, EDIT, (long *) &Hdaten.hdaten.plums_m05, VLONG));
	Form.Add (new CFormField (&m_PlumsM06, EDIT, (long *) &Hdaten.hdaten.plums_m06, VLONG));
	Form.Add (new CFormField (&m_PlumsM07, EDIT, (long *) &Hdaten.hdaten.plums_m07, VLONG));
	Form.Add (new CFormField (&m_PlumsM08, EDIT, (long *) &Hdaten.hdaten.plums_m08, VLONG));
	Form.Add (new CFormField (&m_PlumsM09, EDIT, (long *) &Hdaten.hdaten.plums_m09, VLONG));
	Form.Add (new CFormField (&m_PlumsM10, EDIT, (long *) &Hdaten.hdaten.plums_m10, VLONG));
	Form.Add (new CFormField (&m_PlumsM11, EDIT, (long *) &Hdaten.hdaten.plums_m11, VLONG));
	Form.Add (new CFormField (&m_PlumsM12, EDIT, (long *) &Hdaten.hdaten.plums_m12, VLONG));

    m_LM01.SetWindowText (_T("Jan"));
    m_LM02.SetWindowText (_T("Feb"));
    m_LM03.SetWindowText (_T("Mar"));
    m_LM04.SetWindowText (_T("Apr"));
    m_LM05.SetWindowText (_T("Mai"));
    m_LM06.SetWindowText (_T("Jun"));

    m_LM07.SetWindowText (_T("Jul"));
    m_LM08.SetWindowText (_T("Aug"));
    m_LM09.SetWindowText (_T("Sep"));
    m_LM10.SetWindowText (_T("Okt"));
    m_LM11.SetWindowText (_T("Nov"));
    m_LM12.SetWindowText (_T("Dez"));

	m_H4Arrow.TextColor = RGB (255,255,255);
	m_H3Arrow.TextColor = RGB (255,255,255);
	m_H2Arrow.TextColor = RGB (255,255,255);
//	m_H4Arrow.TextColor = RGB (255,255,0);
//	m_H3Arrow.TextColor = RGB (255,255,0);
//	m_H2Arrow.TextColor = RGB (255,255,0);

	FillHausbank();
	FillPtabCombo (&m_EG, _T("eg_kz"));
	FillPtabCombo (&m_GP, _T("gn_pkt_kz"));
	FillPtabCombo (&m_SepaArt, _T("sepaart"));

	CString kz2 = Kun_erw->kun_erw.sambuch;
	if (kz2 == _T("J"))
	{
		m_SBuch.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_SBuch.SetWindowTextA("   d e a k t i v i e r t");
	}

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 1, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	

	KunGrid.Create (this, 1, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);

	H4Grid.Create (this, 1, 2);
    H4Grid.SetBorder (0, 0);
    H4Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_H4 = new CCtrlInfo (&m_H4, 0, 0, 1, 1);
	H4Grid.Add (c_H4);
	CtrlGrid.CreateChoiceButton (m_H4Choice, IDC_H4CHOICE, this);
	CCtrlInfo *c_H4Choice = new CCtrlInfo (&m_H4Choice, 1, 0, 1, 1);
	H4Grid.Add (c_H4Choice);

	HiGrid.Create (this, 4, 20);
    HiGrid.SetBorder (0, 0);
    HiGrid.SetGridSpace (5, 8);
	CCtrlInfo *c_LH4 = new CCtrlInfo (&m_LH4, 0, 0, 1, 1);
	HiGrid.Add (c_LH4);
	CCtrlInfo *c_H4Grid = new CCtrlInfo (&H4Grid, 1, 0, 1, 1);
	HiGrid.Add (c_H4Grid);
	CCtrlInfo *c_H4Bez = new CCtrlInfo (&m_H4Bez, 2, 0, 1, 1);
	HiGrid.Add (c_H4Bez);
	CCtrlInfo *c_H4Arrow = new CCtrlInfo (&m_H4Arrow, 1, 1, 1, 1);
	c_H4Arrow->SetCellPos (5, 0);
	HiGrid.Add (c_H4Arrow);
	CCtrlInfo *c_LH3 = new CCtrlInfo (&m_LH3, 0, 1, 1, 1);
	c_LH3->SetCellPos (0, 10);
	HiGrid.Add (c_LH3);
	CCtrlInfo *c_H3 = new CCtrlInfo (&m_H3, 1, 1, 1, 1);
	c_H3->SetCellPos (35, 10);
	HiGrid.Add (c_H3);
	CCtrlInfo *c_H3Bez = new CCtrlInfo (&m_H3Bez, 2, 1, 1, 1);
	c_H3Bez->SetCellPos (0, 10);
	HiGrid.Add (c_H3Bez);
	CCtrlInfo *c_H3Arrow = new CCtrlInfo (&m_H3Arrow, 1, 2, 1, 1);
	c_H3Arrow->SetCellPos (40, 10);
	HiGrid.Add (c_H3Arrow);
	CCtrlInfo *c_LH2 = new CCtrlInfo (&m_LH2, 0, 2, 1, 1);
	c_LH2->SetCellPos (0, 20);
	HiGrid.Add (c_LH2);
	CCtrlInfo *c_H2 = new CCtrlInfo (&m_H2, 1, 2, 1, 1);
	c_H2->SetCellPos (70, 20);
	HiGrid.Add (c_H2);
	CCtrlInfo *c_H2Bez = new CCtrlInfo (&m_H2Bez, 2, 2, 1, 1);
	c_H2Bez->SetCellPos (0, 20);
	HiGrid.Add (c_H2Bez);
	CCtrlInfo *c_H2Arrow = new CCtrlInfo (&m_H2Arrow, 1, 3, 1, 1);
	c_H2Arrow->SetCellPos (75, 20);
	HiGrid.Add (c_H2Arrow);
	CCtrlInfo *c_LH1 = new CCtrlInfo (&m_LH1, 0, 3, 1, 1);
	c_LH1->SetCellPos (0, 30);
	HiGrid.Add (c_LH1);
	CCtrlInfo *c_H1 = new CCtrlInfo (&m_H1, 1, 3, 1, 1);
	c_H1->SetCellPos (105, 30);
	HiGrid.Add (c_H1);
	CCtrlInfo *c_H1Bez = new CCtrlInfo (&m_H1Bez, 2, 3, 1, 1);
	c_H1Bez->SetCellPos (0, 30);
	HiGrid.Add (c_H1Bez);

	HdatenGrid.Create (this, 10, 10);
    HdatenGrid.SetBorder (0, 0);
    HdatenGrid.SetGridSpace (3, 8);

	CCtrlInfo *c_LJahr = new CCtrlInfo (&m_LJahr, 0, 0, 2, 1);
	HdatenGrid.Add (c_LJahr);
	CCtrlInfo *c_Jahr = new CCtrlInfo (&m_Jahr, 2, 0, 2, 1);
	HdatenGrid.Add (c_Jahr);
	CCtrlInfo *c_LRv = new CCtrlInfo (&m_LRv, 0, 1, 2, 1);
	HdatenGrid.Add (c_LRv);
	CCtrlInfo *c_Rv = new CCtrlInfo (&m_Rv, 2, 1, 2, 1);
	HdatenGrid.Add (c_Rv);
	CCtrlInfo *c_LZr = new CCtrlInfo (&m_LZr, 0, 2, 2, 1);
	HdatenGrid.Add (c_LZr);
	CCtrlInfo *c_Zr = new CCtrlInfo (&m_Zr, 2, 2, 1, 1);
	HdatenGrid.Add (c_Zr);
	CCtrlInfo *c_LTeuroMonat = new CCtrlInfo (&m_LTeuroMonat, 0, 3, 2, 1);
	HdatenGrid.Add (c_LTeuroMonat);

	CCtrlInfo *c_LM01 = new CCtrlInfo (&m_LM01, 0, 4, 1, 1);
	HdatenGrid.Add (c_LM01);
	CCtrlInfo *c_LM02 = new CCtrlInfo (&m_LM02, 1, 4, 1, 1);
	HdatenGrid.Add (c_LM02);
	CCtrlInfo *c_LM03 = new CCtrlInfo (&m_LM03, 2, 4, 1, 1);
	HdatenGrid.Add (c_LM03);
	CCtrlInfo *c_LM04 = new CCtrlInfo (&m_LM04, 3, 4, 1, 1);
	HdatenGrid.Add (c_LM04);
	CCtrlInfo *c_LM05 = new CCtrlInfo (&m_LM05, 4, 4, 1, 1);
	HdatenGrid.Add (c_LM05);
	CCtrlInfo *c_LM06 = new CCtrlInfo (&m_LM06, 5, 4, 1, 1);
	HdatenGrid.Add (c_LM06);

	CCtrlInfo *c_PlumsM01 = new CCtrlInfo (&m_PlumsM01, 0, 5, 1, 1);
	HdatenGrid.Add (c_PlumsM01);
	CCtrlInfo *c_PlumsM02 = new CCtrlInfo (&m_PlumsM02, 1, 5, 1, 1);
	HdatenGrid.Add (c_PlumsM02);
	CCtrlInfo *c_PlumsM03 = new CCtrlInfo (&m_PlumsM03, 2, 5, 1, 1);
	HdatenGrid.Add (c_PlumsM03);
	CCtrlInfo *c_PlumsM04 = new CCtrlInfo (&m_PlumsM04, 3, 5, 1, 1);
	HdatenGrid.Add (c_PlumsM04);
	CCtrlInfo *c_PlumsM05 = new CCtrlInfo (&m_PlumsM05, 4, 5, 1, 1);
	HdatenGrid.Add (c_PlumsM05);
	CCtrlInfo *c_PlumsM06 = new CCtrlInfo (&m_PlumsM06, 5, 5, 1, 1);
	HdatenGrid.Add (c_PlumsM06);

	CCtrlInfo *c_LM07 = new CCtrlInfo (&m_LM07, 0, 6, 1, 1);
	HdatenGrid.Add (c_LM07);
	CCtrlInfo *c_LM08 = new CCtrlInfo (&m_LM08, 1, 6, 1, 1);
	HdatenGrid.Add (c_LM08);
	CCtrlInfo *c_LM09 = new CCtrlInfo (&m_LM09, 2, 6, 1, 1);
	HdatenGrid.Add (c_LM09);
	CCtrlInfo *c_LM10 = new CCtrlInfo (&m_LM10, 3, 6, 1, 1);
	HdatenGrid.Add (c_LM10);
	CCtrlInfo *c_LM11 = new CCtrlInfo (&m_LM11, 4, 6, 1, 1);
	HdatenGrid.Add (c_LM11);
	CCtrlInfo *c_LM12 = new CCtrlInfo (&m_LM12, 5, 6, 1, 1);
	HdatenGrid.Add (c_LM12);

	CCtrlInfo *c_PlumsM07 = new CCtrlInfo (&m_PlumsM07, 0, 7, 1, 1);
	HdatenGrid.Add (c_PlumsM07);
	CCtrlInfo *c_PlumsM08 = new CCtrlInfo (&m_PlumsM08, 1, 7, 1, 1);
	HdatenGrid.Add (c_PlumsM08);
	CCtrlInfo *c_PlumsM09 = new CCtrlInfo (&m_PlumsM09, 2, 7, 1, 1);
	HdatenGrid.Add (c_PlumsM09);
	CCtrlInfo *c_PlumsM10 = new CCtrlInfo (&m_PlumsM10, 3, 7, 1, 1);
	HdatenGrid.Add (c_PlumsM10);
	CCtrlInfo *c_PlumsM11 = new CCtrlInfo (&m_PlumsM11, 4, 7, 1, 1);
	HdatenGrid.Add (c_PlumsM11);
	CCtrlInfo *c_PlumsM12 = new CCtrlInfo (&m_PlumsM12, 5, 7, 1, 1);
	HdatenGrid.Add (c_PlumsM12);

	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 4);
    c_HeadBorder->rightspace = 20; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 3, 1, 4, 1);
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LKun = new CCtrlInfo (&m_LKun, 1, 2, 1, 1);
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid = new CCtrlInfo (&KunGrid, 2, 2, 1, 1);
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 3, 2, 4, 1);
	CtrlGrid.Add (c_KunName);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 4, 3, 10);
	c_DataBorder->rightspace = 20;
	CtrlGrid.Add (c_DataBorder);

	CCtrlInfo *c_DataBorder2 = new CCtrlInfo (&m_DataBorder2, 3, 4, DOCKRIGHT, 10);
    c_DataBorder2->rightspace = 20;
	CtrlGrid.Add (c_DataBorder2);

	CCtrlInfo *c_HdatenGroup = new CCtrlInfo (&m_HdatenGroup, 3, 13, DOCKRIGHT, DOCKBOTTOM);
    c_HdatenGroup->rightspace = 20;
	CtrlGrid.Add (c_HdatenGroup);

	CCtrlInfo *c_HdatenGrid = new CCtrlInfo (&HdatenGrid, 4, 14, 7, 10);
	CtrlGrid.Add (c_HdatenGrid);

	CCtrlInfo *c_LBank = new CCtrlInfo (&m_LBank, 1, 5, 1, 1);
	CtrlGrid.Add (c_LBank);
	CCtrlInfo *c_Bank = new CCtrlInfo (&m_Bank, 2, 5, 1, 1);
	CtrlGrid.Add (c_Bank);

	CCtrlInfo *c_LBlz = new CCtrlInfo (&m_LBlz, 1, 6, 1, 1);
	CtrlGrid.Add (c_LBlz);
	CCtrlInfo *c_Blz = new CCtrlInfo (&m_Blz, 2, 6, 1, 1);
	CtrlGrid.Add (c_Blz);

	CCtrlInfo *c_LKto = new CCtrlInfo (&m_LKto, 1, 7, 1, 1);
	CtrlGrid.Add (c_LKto);
	CCtrlInfo *c_Kto = new CCtrlInfo (&m_Kto, 2, 7, 1, 1);
	CtrlGrid.Add (c_Kto);

	CCtrlInfo *c_LSWIFT = new CCtrlInfo (&m_LSWIFT, 1, 8, 1, 1);
	CtrlGrid.Add (c_LSWIFT);
	CCtrlInfo *c_SWIFT = new CCtrlInfo (&m_SWIFT, 2, 8, 1, 1);
	CtrlGrid.Add (c_SWIFT);

	CCtrlInfo *c_LIBAN = new CCtrlInfo (&m_LIBAN, 1, 9, 1, 1);
	CtrlGrid.Add (c_LIBAN);
	CCtrlInfo *c_IBAN = new CCtrlInfo (&m_IBAN, 2, 9, 1, 1);
	CtrlGrid.Add (c_IBAN);

	CCtrlInfo *c_LHausbank = new CCtrlInfo (&m_LHausbank, 1, 10, 1, 1);
	CtrlGrid.Add (c_LHausbank);
	CCtrlInfo *c_Hausbank = new CCtrlInfo (&m_Hausbank, 2, 10, 1, 1);
	CtrlGrid.Add (c_Hausbank);

	CCtrlInfo *c_LMandatRef = new CCtrlInfo (&m_LMandatRef, 1, 11, 1, 1);
	CtrlGrid.Add (c_LMandatRef);
	CCtrlInfo *c_MandatRef = new CCtrlInfo (&m_MandatRef, 2, 11, 1, 1);
	CtrlGrid.Add (c_MandatRef);

	CCtrlInfo *c_LMandatSeit = new CCtrlInfo (&m_LMandatSeit, 1, 12, 1, 1);
	CtrlGrid.Add (c_LMandatSeit);
	CCtrlInfo *c_MandatSeit = new CCtrlInfo (&m_MandSeit, 2, 12, 1, 1);
	CtrlGrid.Add (c_MandatSeit);

	CCtrlInfo *c_LAnzTageSepa = new CCtrlInfo (&m_LAnzTageSepa, 1, 13, 1, 1);
	CtrlGrid.Add (c_LAnzTageSepa);
	CCtrlInfo *c_AnzTageSepa = new CCtrlInfo (&m_AnzTageSepa, 2, 13, 1, 1);
	CtrlGrid.Add (c_AnzTageSepa);

	CCtrlInfo *c_LSepaArt = new CCtrlInfo (&m_LSepaArt, 1, 14, 1, 1);
	CtrlGrid.Add (c_LSepaArt);
	CCtrlInfo *c_SepaArt = new CCtrlInfo (&m_SepaArt, 2, 14, 1, 1);
	CtrlGrid.Add (c_SepaArt);

	CCtrlInfo *c_LSBuch = new CCtrlInfo (&m_LSBuch, 1, 15, 1, 1);
	CtrlGrid.Add (c_LSBuch);
	CCtrlInfo *c_SBuch = new CCtrlInfo (&m_SBuch, 2, 15, 1, 1);
	CtrlGrid.Add (c_SBuch);

	CCtrlInfo *c_HierarchieGroup = new CCtrlInfo (&m_HierarchieGroup, 0, 16, 3, DOCKBOTTOM);
	c_HierarchieGroup->rightspace = 20;
	CtrlGrid.Add (c_HierarchieGroup);

	CCtrlInfo *c_HDialog = new CCtrlInfo (&m_HDialog, 1, 17, 3, 5);
	c_HDialog->SetCellPos (0, -8); 
	CtrlGrid.Add (c_HDialog);

	CCtrlInfo *c_HiGrid = new CCtrlInfo (&HiGrid, 1, 18, 3, 5);
	CtrlGrid.Add (c_HiGrid);

	CCtrlInfo *c_LSteuer = new CCtrlInfo (&m_LSteuer, 4, 6, 1, 1);
	CtrlGrid.Add (c_LSteuer);
	CCtrlInfo *c_Steuer = new CCtrlInfo (&m_Steuer, 5, 6, 1, 1);
	c_Steuer->rightspace = 40;
	CtrlGrid.Add (c_Steuer);

	CCtrlInfo *c_LUst = new CCtrlInfo (&m_LUst, 4, 7, 1, 1);
	CtrlGrid.Add (c_LUst);
	CCtrlInfo *c_Ust = new CCtrlInfo (&m_Ust, 5, 7, 1, 1);
	c_Ust->rightspace = 40;
	CtrlGrid.Add (c_Ust);

	CCtrlInfo *c_LEG = new CCtrlInfo (&m_LEG, 4, 9, 1, 1);
	CtrlGrid.Add (c_LEG);
	CCtrlInfo *c_EG = new CCtrlInfo (&m_EG, 5, 9, 1, 1);
	c_EG->rightspace = 40;
	CtrlGrid.Add (c_EG);

	CCtrlInfo *c_LGP = new CCtrlInfo (&m_LGP, 4, 10, 1, 1);
	CtrlGrid.Add (c_LGP);
	CCtrlInfo *c_GP = new CCtrlInfo (&m_GP, 5, 10, 1, 1);
	c_GP->rightspace = 40;
	CtrlGrid.Add (c_GP);

	CCtrlInfo *c_LStatKun = new CCtrlInfo (&m_LStatKun, 4, 11, 1, 1);
	CtrlGrid.Add (c_LStatKun);
	CCtrlInfo *c_StatKun = new CCtrlInfo (&m_StatKun, 5, 11, 1, 1);
	c_StatKun->rightspace = 40;
	CtrlGrid.Add (c_StatKun);


	
	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	CtrlGrid.Display ();
    CtrlGrid.SetItemCharHeight (&m_HeadBorder, 3);
    EnableFields (TRUE);



	strcpy (sys_par.sys_par_nam, "gn_pkt_par");
	if (Sys_par->dbreadfirst () == 0)
	{
		if (!(sys_par.sys_par_wrt[0] == '1'))
		{
			m_GP.EnableWindow(FALSE);
			m_LGP.EnableWindow(FALSE);
		}
	}

	CString Sysdate;
	CStrFuncs::SysDate (Sysdate);
	memcpy (&Hdaten.hdaten, &hdaten_null, sizeof (HDATEN));
	Hdaten.hdaten.mdn = Kun->kun.mdn; 
	Hdaten.hdaten.kun = Kun->kun.kun; 
	Hdaten.hdaten.jr  = _tstoi (&Sysdate.GetBuffer()[6]);
	Hdaten.dbreadfirst ();

    Form.Show ();
//	m_Bank.SetFocus();

	m_SpinJahr.SetRange32 (1900, 2100);
	m_SpinJahr.SetBuddy (&m_Jahr);

	return TRUE;

}


BOOL CKunPage7::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				Update->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Form.Get();
				Update->Write();
				return TRUE;
			}

			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}
			if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

HBRUSH CKunPage7::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);

    if (hBrush == NULL)
	{
		  hBrush = CreateSolidBrush (Color);
		  hBrushStatic = CreateSolidBrush (Color);
		  m_HDialog.SetBkColor (DlgBkColor);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			return hBrush;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CButton )))
	{
            pDC->SetBkColor (Color);
			pDC->SetBkMode (TRANSPARENT);
			return hBrushStatic;
	}
	return CPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CKunPage7::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_H4)
	{
		if (!TestH4 ())
		{
			m_H4.SetFocus ();
			MessageBox (_T("Hierarchie 4 nicht gefunden"), _T(""), MB_OK | MB_ICONERROR);
			return FALSE;
		}
	}
	else if (Control == &m_Jahr)
	{
		ReadHdaten ();
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CKunPage7::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}


void CKunPage7::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CKunPage7::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}

void CKunPage7::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


	if (m_Mdn.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein SDatz zum L�schen selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	if (MessageBox (_T("Preisauszeichnerdaten l�schen"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		Kun->beginwork ();
		Kun->dbdelete ();
		Kun->commitwork ();
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		EnableFields (FALSE);
		Form.Show ();
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);
	}
}

void CKunPage7::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
//		Control->SetReadOnly (!enable);
        Control->EnableWindow (enable);
	}
}

BOOL CKunPage7::StepBack ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
					Frame->DestroyWindow ();
					return FALSE;
	}
	else
	{
		F5Pressed = TRUE;
		EnableFields (FALSE);
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);	
	}
	return TRUE;
}


void CKunPage7::OnCopy ()
{
    OnEditCopy ();
}  



void CKunPage7::OnPaste ()
{
	OnEditPaste ();
}

void CKunPage7::OnCopyEx ()
{

//	if (!m_A.IsWindowEnabled ())
	{
		Copy ();
		return;
	}
}

void CKunPage7::OnPasteEx ()
{

//	if (!m_A.IsWindowEnabled () && Paste ())
	{
		Kun->CopyData (&kun);
		Kun_erw->CopyData (&kun_erw);
		Form.Show ();
/*
		if (KunPageEx != NULL && IsWindow (KunPageEx->m_hWnd))
		{
			KunPageEx->Form.Show ();
		}
*/
		return;
	}
}

BOOL CKunPage7::Copy ()
{
	Form.Get ();
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return FALSE;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      int err = GetLastError ();
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return FALSE;  
    }

	try
	{
        UINT CF_A_KUN_GX = RegisterClipboardFormat (_T("kun")); 
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, sizeof (KUN)); 
        char *p = (char *) GlobalLock(hglbCopy);
        memcpy (p, &Kun->kun, sizeof (Kun->kun));
        GlobalUnlock(hglbCopy); 
		HANDLE cData;
		cData = ::SetClipboardData( CF_A_KUN_GX, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
    return TRUE;
}

BOOL CKunPage7::Paste ()
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return FALSE;
    }

    HGLOBAL hglbCopy;
    UINT CF_A_KUN_GX;
    _TCHAR name [256];
    CF_A_KUN_GX = EnumClipboardFormats (0);
	if (CF_A_KUN_GX == 0)
	{
		int err = GetLastError ();
		CloseClipboard();
		return FALSE;
	}

    int ret;
    while ((ret = GetClipboardFormatName (CF_A_KUN_GX, name, sizeof (name))) != 0)
    { 
         CString Name = name;
         if (Name == _T("a_kun_gx"))
         {
              break;
         }  
		 CF_A_KUN_GX = EnumClipboardFormats (CF_A_KUN_GX);
    }
    if (ret == 0)
    {
         CloseClipboard();
         return FALSE;
    }    

    try
    {	
  	     hglbCopy =  ::GetClipboardData(CF_A_KUN_GX);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
 	     LPSTR p = (LPSTR) GlobalLock ((HGLOBAL) hglbCopy);
         memcpy ( (LPSTR) &kun, p, sizeof (kun));
  	     GlobalUnlock ((HGLOBAL) hglbCopy);
    }
    catch (...) {};
    CloseClipboard();
    return TRUE; 
}

void CKunPage7::EnableFields (BOOL b)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (!b)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);
}


void CKunPage7::OnKillFocus (CWnd *newFocus)
{
	CWnd *Control = GetFocus ();
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		if (f->Scale > 0)
		{
			f->Show ();
		}
	}
}


void CKunPage7::FillPtabCombo (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem)); 
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert)); 
		Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0); 

		
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn->sqlcursor (Sql.GetBuffer ());
		while (Ptabn->sqlfetch (cursor) == 0)
		{
			Ptabn->dbreadfirst();

			LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn->ptabn.ptwert,
				                             Ptabn->ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}

void CKunPage7::FillHausbank ()
{
	CFormField *f = Form.GetFormField (&m_Hausbank);
	if (f != NULL)
	{
		f->ComboValues.clear ();
	
		Haus_bank->sqlout ((short *) &Haus_bank->haus_bank.bank , SQLSHORT, 0); 
		Haus_bank->sqlout ((char *) Haus_bank->haus_bank.bank_nam  , SQLCHAR, sizeof (Haus_bank->haus_bank.bank_nam)); 
		
        int cursor = Haus_bank->sqlcursor (_T("select bank, bank_nam from haus_bank ")
			                          _T("where bank > 0 ")
									  _T("order by bank"));
		
		
		
		while (Haus_bank->sqlfetch (cursor) == 0)
		{
			Haus_bank->dbreadfirst ();

			//LPSTR pos = (LPSTR) Iprgrstufk.iprgrstufk.pr_gr_stuf;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			//pos = (LPSTR) Iprgrstufk.iprgrstufk.zus_bz;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), Haus_bank->haus_bank.bank,
												Haus_bank->haus_bank.bank_nam);
		    f->ComboValues.push_back (ComboValue); 
		}
		Haus_bank->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}

BOOL CKunPage7::OnKillActive ()
{
	Form.Get ();
	if (F5Pressed)
	{
		return TRUE;
	}
	return TestFields ();
}

BOOL CKunPage7::OnSetActive ()
{
	F5Pressed = FALSE;
	Form.Show ();
	return TRUE;
}

void CKunPage7::UpdatePage ()
{
	Form.Show ();
}

void CKunPage7::OnH4Choice ()
{
	Form.Get ();
	CChoiceH *Choice = new CChoiceH (this);
    Choice->IsModal = TRUE;
	Choice->SearchText = "";
	Choice->CreateDlg ();

    Choice->SetDbClass (Kun);
	Choice->DoModal();

    if (Choice->GetState ())
    {
		  CHList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  H4.h4.hirarchie4 = abl->H4;
		  Kun->kun.hirarchie4 = abl->H4;
		  if (H4.dbreadfirst () == 0)
		  {
				H3.h3.hirarchie3 = H4.h4.hirarchie3;
				Kun->kun.hirarchie3 = H4.h4.hirarchie3;
				H3.dbreadfirst ();
				H2.h2.hirarchie2 = H4.h4.hirarchie2;
				Kun->kun.hirarchie2 = H4.h4.hirarchie2;
				H2.dbreadfirst ();
				H1.h1.hirarchie1 = H4.h4.hirarchie1;
				Kun->kun.hirarchie1 = H4.h4.hirarchie1;
				H1.dbreadfirst ();
				Form.Show ();
		  }
	}
	delete Choice;
}

BOOL CKunPage7::Read ()
{
		  memcpy (&H4.h4, &h4_null, sizeof (struct H4));
		  memcpy (&H3.h3, &h3_null, sizeof (struct H3));
		  memcpy (&H2.h2, &h2_null, sizeof (struct H2));
		  memcpy (&H1.h1, &h1_null, sizeof (struct H1));

		  H4.h4.hirarchie4 = Kun->kun.hirarchie4;
		  if (H4.dbreadfirst () == 0)
		  {
				Kun->kun.hirarchie3 = H4.h4.hirarchie3;
				Kun->kun.hirarchie2 = H4.h4.hirarchie2;
				Kun->kun.hirarchie1 = H4.h4.hirarchie1;
				H3.h3.hirarchie3 = H4.h4.hirarchie3;
				H3.dbreadfirst ();
				H2.h2.hirarchie2 = H4.h4.hirarchie2;
				H2.dbreadfirst ();
				H1.h1.hirarchie1 = H4.h4.hirarchie1;
				H1.dbreadfirst ();
				Form.Show ();
				return TRUE;
		  }
		  return FALSE;
}

BOOL CKunPage7::TestH4 ()
{
		  Form.Get ();
		  memcpy (&H3.h3, &h3_null, sizeof (struct H3));
		  memcpy (&H2.h2, &h2_null, sizeof (struct H2));
		  memcpy (&H1.h1, &h1_null, sizeof (struct H1));

		  if (H4.dbreadfirst () == 0)
		  {
				Kun->kun.hirarchie4 = H4.h4.hirarchie4;
				Kun->kun.hirarchie3 = H4.h4.hirarchie3;
				Kun->kun.hirarchie2 = H4.h4.hirarchie2;
				Kun->kun.hirarchie1 = H4.h4.hirarchie1;
				H3.h3.hirarchie3 = H4.h4.hirarchie3;
				H3.dbreadfirst ();
				H2.h2.hirarchie2 = H4.h4.hirarchie2;
				H2.dbreadfirst ();
				H1.h1.hirarchie1 = H4.h4.hirarchie1;
				H1.dbreadfirst ();
//				Form.Show ();
				return TRUE;
		  }
		  return FALSE;
}

BOOL CKunPage7::TestFields ()
{
	if (m_hWnd == NULL)
	{
		return TRUE;
	}
	try
	{
		if (!TestH4 ())
		{
			m_H4.SetFocus ();
			MessageBox (_T("Hierarchie 4 nicht gefunden"), _T(""), MB_OK | MB_ICONERROR);
			return FALSE;
		}
		Form.Get ();
		Hdaten.dbupdate ();
	}
	catch (...) {}
	return TRUE;
}

void CKunPage7::ReadHdaten ()
{
	CString Jahr;
	CString Left;
	CString Right;
	m_Jahr.GetWindowText (Jahr); 
	Jahr.Trim ();
	int idx = Jahr.Find (".");
	if (idx != -1)
	{
		Jahr = Jahr.Left (idx) + Jahr.Right (Jahr.GetLength () - idx - 1);
	}


	memcpy (&Hdaten.hdaten, &hdaten_null, sizeof (HDATEN));
	Hdaten.hdaten.mdn = Kun->kun.mdn; 
	Hdaten.hdaten.kun = Kun->kun.kun; 
    Hdaten.hdaten.jr = _tstoi (Jahr.GetBuffer ());
	Hdaten.dbreadfirst ();
	Form.Show ();
}

void CKunPage7::OnEnChangeJahr()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDbPropertyPage::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.

	if (!TestJrChange)
	{
		return;
	}

	if (m_Jahr.m_hWnd == NULL)
	{
		return;
	}
	TestJrChange = FALSE;
	CString Jahr;
	CString Left;
	CString Right;
	m_Jahr.GetWindowText (Jahr); 
	Jahr.Trim ();
	int idx = Jahr.Find (".");
	if (idx != -1)
	{
		Jahr = Jahr.Left (idx) + Jahr.Right (Jahr.GetLength () - idx - 1);
	}
    int jr = _tstoi (Jahr.GetBuffer ());
	if (jr >= 1900 && jr <= 2100)
	{
		memcpy (&Hdaten.hdaten, &hdaten_null, sizeof (HDATEN));
		Hdaten.hdaten.mdn = Kun->kun.mdn; 
		Hdaten.hdaten.kun = Kun->kun.kun; 
		Hdaten.hdaten.jr = jr;
		Hdaten.dbreadfirst ();
		Form.Show ();
	}
	TestJrChange = TRUE;
}

void CKunPage7::OnHdialog ()
{
	CHierarchieDialog dlg;

	 INT_PTR ret = dlg.DoModal ();
}

void CKunPage7::OnStnClickedLswift()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CKunPage7::OnBnClickedSbuch()
{
	CString kz;
	
	Form.Get();

	kz = Kun_erw->kun_erw.sambuch;

	if (kz == _T("N"))
	{
		m_SBuch.SetWindowTextA("   d e a k t i v i e r t");
	}
	else
	{
		m_SBuch.SetWindowTextA("   a k t i v i e r t");
	}
}
