#pragma once


// CArrow

class CArrow : public CStatic
{
	DECLARE_DYNAMIC(CArrow)
protected:
	DWORD Style;
	DECLARE_MESSAGE_MAP()
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
	void Draw (CDC& cDC);

public:
	enum {
		Left = 1,
		Right = 2,
		Center = 3,
	};

	int Orientation;
	UINT nID;
	COLORREF BkColor;
	BOOL ColorSet;
	COLORREF TextColor;

	CArrow();
	virtual ~CArrow();
    virtual BOOL Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect,
                        CWnd* pParentWnd,   UINT nID = 0xffff);

};


