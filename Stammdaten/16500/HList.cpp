#include "StdAfx.h"
#include "HList.h"

CHList::CHList(void)
{
	H4 = 0l;
	H4bez = _T("");
	H3bez = _T("");
	H2bez = _T("");
	H1bez = _T("");
}

CHList::CHList(long H4, LPTSTR H4bez, LPTSTR H3bez, LPTSTR H2bez, LPTSTR H1bez)
{
	this->H4 = H4;
	this->H4bez = H4bez;
	this->H3bez = H3bez;
	this->H2bez = H2bez;
	this->H1bez = H1bez;
}

CHList::~CHList(void)
{
}
