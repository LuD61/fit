// 16500.h : Hauptheaderdatei f�r die 16500-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error 'stdafx.h' muss vor dieser Datei in PCH eingeschlossen werden.
#endif

#include "resource.h"       // Hauptsymbole


// CMyApp:
// Siehe 16500.cpp f�r die Implementierung dieser Klasse
//

class CMyApp : public CWinApp
{
public:
	CMyApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnFileNewKun();
};

extern CMyApp theApp;
extern int minitour ;
