#pragma once
#include "afxwin.h"
#include "ListEdit.h"

class CSearchListCtrl :
	public CStatic
//	public CObject
{
	DECLARE_DYNCREATE(CSearchListCtrl)
public:
	enum ALIGN
	{
		LEFT = 1,
		RIGHT = 2,
	};
	int idEdit;
	int idButton;

	CEdit Edit;
	CButton Button;
	CSearchListCtrl(void);
	~CSearchListCtrl(void);
	void Create (RECT &rect, CWnd *pParent, UINT nIDEdit, UINT nIDButton, int Align);
	virtual BOOL DestroyWindow ();
};
