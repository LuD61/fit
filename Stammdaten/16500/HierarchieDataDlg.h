#pragma once
#include "UpdateEvent.h"
#include "PageUpdate.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "H1.h"
#include "H2.h"
#include "H3.h"
#include "H4.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "StaticButton.h"
#include "afxwin.h"

#define IDC_HSAVE 3010
#define IDC_HDELETE 3011

// CHierarchieDataDlg-Dialogfeld

class CHierarchieDataDlg : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CHierarchieDataDlg)

public:
	CHierarchieDataDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CHierarchieDataDlg();

// Dialogfelddaten
	enum { IDD = IDD_HIERARCHIE_DATA_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor); 
	virtual void OnSize (UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()

private:
	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	CFont Font;
	CCtrlGrid CtrlGrid;
	CCtrlGrid H1Grid;
	CCtrlGrid H2Grid;
	CCtrlGrid H3Grid;
	CCtrlGrid H4Grid;
	CCtrlGrid ToolGrid;
	CFormTab Form;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	CStaticButton m_Save;
	CStaticButton m_Delete;

public:
	H1_CLASS *H1;
	H2_CLASS *H2;
	H3_CLASS *H3;
	H4_CLASS *H4;
	int cursor_h21;
	int cursor_h31;
	int cursor_h32;
	int cursor_h41;
	int cursor_h42;
	int cursor_h43;

	CStatic m_H1Group;
	CStatic m_LHierarchie1;
	CNumEdit m_Hierarchie1;
	CStatic m_LH1Bez;
	CTextEdit m_H1Bez;
	CStatic m_LH1BezKrz;
	CTextEdit m_H1BezKrz;

	CStatic m_H2Group;
	CStatic m_LHierarchie2;
	CNumEdit m_Hierarchie2;
	CStatic m_LH2Bez;
	CTextEdit m_H2Bez;
	CStatic m_LH2BezKrz;
	CTextEdit m_H2BezKrz;

	CStatic m_H3Group;
	CStatic m_LHierarchie3;
	CNumEdit m_Hierarchie3;
	CStatic m_LH3Bez;
	CTextEdit m_H3Bez;
	CStatic m_LH3BezKrz;
	CTextEdit m_H3BezKrz;

	CStatic m_H4Group;
	CStatic m_LHierarchie4;
	CNumEdit m_Hierarchie4;
	CStatic m_LH4Bez;
	CTextEdit m_H4Bez;
	CStatic m_LH4BezKrz;
	CTextEdit m_H4BezKrz;

	virtual void Update ();
	virtual void OnSave ();
	virtual void Delete ();
	void OnEnKillFocusH1 ();
	void OnEnKillFocusH2 ();
	void OnEnKillFocusH3 ();
	void OnEnKillFocusH4 ();
};

