#pragma once
#include "afxdlgs.h"
#include "PageUpdate.h"

class CDbPropertyPage :
	public CPropertyPage
{
protected:
	BOOL InData;
	DECLARE_DYNAMIC(CDbPropertyPage)
public:
	BOOL F5Pressed;
	COLORREF DlgBkColor;
	COLORREF ListBkColor;
	HBRUSH DlgBrush;
	BOOL FlatLayout;
	BOOL HideOK;
	CPageUpdate *Update;

	CString ArchiveName;

	CDbPropertyPage(void);
	CDbPropertyPage(UINT);
	~CDbPropertyPage(void);
	virtual BOOL Read ();
	virtual BOOL read ();
	virtual BOOL Write ();
	virtual void write ();
	virtual void OnDelete ();
	virtual void OnInsert ();
	virtual BOOL AfterWrite ();
	virtual BOOL Print ();
	virtual BOOL PrintAll ();
	virtual BOOL EnablePrint (CCmdUI *);
	virtual BOOL TextCent ();
	virtual BOOL EnableTextCent (CCmdUI *);
	virtual BOOL TextLeft ();
	virtual BOOL EnableTextLeft (CCmdUI *);
	virtual BOOL TextRight ();
	virtual BOOL EnableTextRight (CCmdUI *);
	virtual BOOL DeleteAll ();
	virtual BOOL Delete ();
	virtual BOOL Insert ();
	virtual BOOL Show ();
	virtual BOOL Get ();
	virtual BOOL StepBack ();
    virtual void NextRecord ();
    virtual void PriorRecord ();
    virtual void FirstRecord ();
    virtual void LastRecord ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnMarkAll ();
    virtual void OnUnMarkAll ();
	virtual void OnChoice (){}
    virtual DROPEFFECT OnDrop (CWnd* , COleDataObject*,  DROPEFFECT,  DROPEFFECT, CPoint);
	virtual void Save ();
	virtual void Load ();
	virtual void SetDefault ();
	virtual void UpdatePage ();
	virtual void GetPage ();
};
