#ifndef _CHOICEH_DEF
#define _CHOICEH_DEF

#include "ChoiceX.h"
#include "HList.h"
#include <vector>

class CChoiceH : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
protected:  
		virtual BOOL OnInitDialog(); 
    public :
		long m_H4;
	    std::vector<CHList *> HList;
      	CChoiceH(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceH(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchBez (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CHList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
