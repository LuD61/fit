// KunView.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "KunView.h"
#include "sys_par.h"
#include "kun.h"
#include "kun_erw.h"
#include "dta.h"
#include "Process.h"

#define CXPLUS 33
#define CYPLUS 45

// CKunView

IMPLEMENT_DYNCREATE(CKunView, DbFormView)

CKunView::CKunView()
	: DbFormView(CKunView::IDD)
{
	m_PropertySheet = NULL;
	m_KunPage = NULL;
	hBrush = NULL;
	staticBrush = NULL;
}

CKunView::~CKunView()
{
		DeletePropertySheet ();
}

void CKunView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CKunView, DbFormView)
	ON_WM_CTLCOLOR ()
//	ON_WM_SIZE ()
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_COMMAND(ID_BACK, OnBack)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)

/*
	ON_COMMAND(ID_LANGUAGE, OnLanguage)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)

	ON_COMMAND(ID_TABCOPY, OnTabcopy)
	ON_COMMAND(ID_TABPASTE, OnTabpaste)
	ON_COMMAND(ID_TEXT_CENT, OnTextCent)
	ON_COMMAND(ID_TEXT_LEFT, OnTextLeft)
	ON_COMMAND(ID_TEXT_RIGHT, OnTextRight)
*/
END_MESSAGE_MAP()


// CKunView-Diagnose

#ifdef _DEBUG
void CKunView::AssertValid() const
{
	CFormView::AssertValid();
}

void CKunView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
	
	
}
#endif //_DEBUG


// CKunView-Meldungshandler

void CKunView::OnInitialUpdate()
{
	DbFormView::OnInitialUpdate();

	m_PropertySheet = new CDbPropertySheet(_T("Preisauszeichnerdaten"));
    m_PropertySheet->m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
	m_KunPage = new CKunPage;
	m_PropertySheet->AddPage (m_KunPage);
	m_KunPage->Update = this;


	m_KunPage7 = new CKunPage7;
	m_PropertySheet->AddPage (m_KunPage7);

	m_KunPage7->Kun = &m_KunPage->Kun;
	m_KunPage7->Mdn = &m_KunPage->Mdn;
	m_KunPage7->KunAdr = &m_KunPage->KunAdr;
	m_KunPage7->MdnAdr = &m_KunPage->MdnAdr;
	m_KunPage7->RechAdr = &m_KunPage->RechAdr;
	m_KunPage7->Sys_par = &m_KunPage->Sys_par;
	m_KunPage7->Ptabn = &m_KunPage->Ptabn;
	m_KunPage7->Kun_tou = &m_KunPage->Kun_tou;
	m_KunPage7->Gr_zuord = &m_KunPage->Gr_zuord;
	m_KunPage7->Kun_erw = &m_KunPage->Kun_erw;
	m_KunPage7->Fil = &m_KunPage->Fil;
	m_KunPage7->Zahl_kond = &m_KunPage->Zahl_kond;
	m_KunPage7->Rab_stufek = &m_KunPage->Rab_stufek;
	m_KunPage7->Haus_bank = &m_KunPage->Haus_bank;
	m_KunPage7->Ls_txt = &m_KunPage->Ls_txt;
	m_KunPage7->Update = this;
	m_KunPage->Page7 = m_KunPage7;
	

	m_KunPage2 = new CKunPage2;
	m_PropertySheet->AddPage (m_KunPage2);
	
	m_KunPage2->Kun = &m_KunPage->Kun;
	m_KunPage2->Mdn = &m_KunPage->Mdn;
	m_KunPage2->KunAdr = &m_KunPage->KunAdr;
	m_KunPage2->LiefAdr = &m_KunPage->LiefAdr;
	m_KunPage2->Kun_erw = &m_KunPage->Kun_erw;
	m_KunPage2->MdnAdr = &m_KunPage->MdnAdr;
	m_KunPage2->Sys_par = &m_KunPage->Sys_par;
	m_KunPage2->Ptabn = &m_KunPage->Ptabn;
	m_KunPage2->Kun_tou = &m_KunPage->Kun_tou;
	m_KunPage2->Kun_erw = &m_KunPage->Kun_erw;
	m_KunPage2->Gr_zuord = &m_KunPage->Gr_zuord;
	m_KunPage2->Fil = &m_KunPage->Fil;
	m_KunPage2->Zahl_kond = &m_KunPage->Zahl_kond;
	m_KunPage2->Rab_stufek = &m_KunPage->Rab_stufek;
	m_KunPage2->Haus_bank = &m_KunPage->Haus_bank;
	m_KunPage2->Ls_txt = &m_KunPage->Ls_txt;
	m_KunPage2->Zuschlag = &m_KunPage->Zuschlag;
	m_KunPage2->Update = this;

	m_FrachtKosten = new CFrachtKosten;
	m_FrachtKosten->Zuschlag = &m_KunPage->Zuschlag;
			

	m_KunPage3 = new CKunPage3;
	m_PropertySheet->AddPage (m_KunPage3);

	m_KunPage3->Kun = &m_KunPage->Kun;
	m_KunPage3->Kun_erw = &m_KunPage->Kun_erw; //LOH-35
	m_KunPage3->Mdn = &m_KunPage->Mdn;
	m_KunPage3->KunAdr = &m_KunPage->KunAdr;
	m_KunPage3->RechAdr = &m_KunPage->RechAdr;
	m_KunPage3->LiefAdr = &m_KunPage->LiefAdr;
	m_KunPage3->MdnAdr = &m_KunPage->MdnAdr;
	m_KunPage3->Sys_par = &m_KunPage->Sys_par;
	m_KunPage3->Ptabn = &m_KunPage->Ptabn;
	m_KunPage3->Kun_tou = &m_KunPage->Kun_tou;
	m_KunPage3->Gr_zuord = &m_KunPage->Gr_zuord;
	m_KunPage3->Fil = &m_KunPage->Fil;
	m_KunPage3->Zahl_kond = &m_KunPage->Zahl_kond;
	m_KunPage3->Rab_stufek = &m_KunPage->Rab_stufek;
	m_KunPage3->Haus_bank = &m_KunPage->Haus_bank;
	m_KunPage3->Ls_txt = &m_KunPage->Ls_txt;
	m_KunPage3->Update = this;

	m_KunPage4 = new CKunPage4;
	m_PropertySheet->AddPage (m_KunPage4);

	m_KunPage4->Kun = &m_KunPage->Kun;
	m_KunPage4->Kun_erw = &m_KunPage->Kun_erw; //LOH-35
	m_KunPage4->Mdn = &m_KunPage->Mdn;
	m_KunPage4->KunAdr = &m_KunPage->KunAdr;
	m_KunPage4->MdnAdr = &m_KunPage->MdnAdr;
	m_KunPage4->Sys_par = &m_KunPage->Sys_par;
	m_KunPage4->Ptabn = &m_KunPage->Ptabn;
	m_KunPage4->Kun_tou = &m_KunPage->Kun_tou;
	m_KunPage4->Gr_zuord = &m_KunPage->Gr_zuord;
	m_KunPage4->Fil = &m_KunPage->Fil;
	m_KunPage4->Zahl_kond = &m_KunPage->Zahl_kond;
	m_KunPage4->Rab_stufek = &m_KunPage->Rab_stufek;
	m_KunPage4->Haus_bank = &m_KunPage->Haus_bank;
	m_KunPage4->Ls_txt = &m_KunPage->Ls_txt;
	m_KunPage4->Update = this;


	// Registerkarte Daten EDI ein- / ausschalten
	test = 0;
	Kun->opendbase(_T("bws"));
	strcpy (sys_par.sys_par_nam, "edi_par");
	if (Sys_par.dbreadfirst() == 0)
	{
		if (!(sys_par.sys_par_wrt[0] == '0'))
		{
			m_KunPage5 = new CKunPage5;
			m_PropertySheet->AddPage (m_KunPage5);
			test = 1;

			m_KunPage5->Kun = &m_KunPage->Kun;
			m_KunPage5->Kun_erw = &m_KunPage->Kun_erw; //LOH-35
			m_KunPage5->Mdn = &m_KunPage->Mdn;
			m_KunPage5->KunAdr = &m_KunPage->KunAdr;
			m_KunPage5->MdnAdr = &m_KunPage->MdnAdr;
			m_KunPage5->Sys_par = &m_KunPage->Sys_par;
			m_KunPage5->Ptabn = &m_KunPage->Ptabn;
			m_KunPage5->Kun_tou = &m_KunPage->Kun_tou;
			m_KunPage5->Gr_zuord = &m_KunPage->Gr_zuord;
			m_KunPage5->Fil = &m_KunPage->Fil;
			m_KunPage5->Zahl_kond = &m_KunPage->Zahl_kond;
			m_KunPage5->Rab_stufek = &m_KunPage->Rab_stufek;
			m_KunPage5->Haus_bank = &m_KunPage->Haus_bank;			
			m_KunPage5->Ls_txt = &m_KunPage->Ls_txt;
			m_KunPage5->Dta = &m_KunPage->Dta;
			m_KunPage5->Update = this;
		}
		
	}
	Kun->closedbase(_T("bws"));
		
	/*
	m_KunPage6 = new CKunPage6;
	m_PropertySheet->AddPage (m_KunPage6);

	m_KunPage6->Kun = &m_KunPage->Kun;
	m_KunPage6->Mdn = &m_KunPage->Mdn;
	m_KunPage6->KunAdr = &m_KunPage->KunAdr;
	m_KunPage6->MdnAdr = &m_KunPage->MdnAdr;
	m_KunPage6->Sys_par = &m_KunPage->Sys_par;
	m_KunPage6->Ptabn = &m_KunPage->Ptabn;
	m_KunPage6->Kun_tou = &m_KunPage->Kun_tou;
	m_KunPage6->Gr_zuord = &m_KunPage->Gr_zuord;
	m_KunPage6->Fil = &m_KunPage->Fil;
	m_KunPage6->Zahl_kond = &m_KunPage->Zahl_kond;
	m_KunPage6->Rab_stufek = &m_KunPage->Rab_stufek;
	m_KunPage6->Haus_bank = &m_KunPage->Haus_bank;
	m_KunPage6->Ls_txt = &m_KunPage->Ls_txt;
	m_KunPage6->Zuschlag = &m_KunPage->Zuschlag;
*/

	
    m_PropertySheet->m_psh.hwndParent = m_hWnd;
    m_PropertySheet->m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
    m_PropertySheet->m_psh.pszCaption = _T("Simple");
    m_PropertySheet->m_psh.nStartPage = 0;

	m_PropertySheet->Create (this, WS_CHILD | WS_VISIBLE);
	CRect cRect;
    m_PropertySheet->GetClientRect (&cRect);
    m_PropertySheet->MoveWindow (0, 0,
		                        cRect.right, cRect.bottom);
	CSize Size = GetTotalSize ();
	if (Size.cy < (cRect.bottom) + 10)
	{
		Size.cy = cRect.bottom + 10;
	}
	if (Size.cx < (cRect.right) + 10)
	{
		Size.cx = cRect.right + 10;
	}
	SetScrollSizes (MM_TEXT, Size);
//	GetClientRect (&cRect);
	CRect pRect;
	CRect pcRect;
	CWnd *ChildFrame = GetParent ();
	CWnd *MainFrame = ChildFrame->GetParent ();
	ChildFrame->GetWindowRect (&pRect);
	MainFrame->ScreenToClient (&pRect);
	MainFrame->GetClientRect (&pcRect);
	BOOL Resize = FALSE;
	if (pcRect.right > pRect.left + cRect.right + CXPLUS)
	{
		pRect.right  = pRect.left + cRect.right + CXPLUS;
		Resize = TRUE;
	}
	if (pcRect.bottom > pRect.top + cRect.bottom + CYPLUS)
	{
		pRect.bottom = pRect.top + cRect.bottom + CYPLUS;
		Resize = TRUE;
	}
	if (Resize)
	{
		GetParent ()->MoveWindow (&pRect, TRUE);
	}
}

void CKunView::OnSize(UINT nType, int cx, int cy)
{
}



HBRUSH CKunView::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	if (hBrush == NULL)
	{
		hBrush = CreateSolidBrush (Color);
		staticBrush = CreateSolidBrush (Color);
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
 		    return staticBrush;
	}
	return DbFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CKunView::OnLanguage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	TCHAR CodePageName [256];
    LPTSTR etc;

	memset (CodePageName, 0, sizeof (CodePageName));
    OPENFILENAME fnstruct;
    static LPCTSTR lpstrFilter = 
            _T("Sprache\0*.*\0,\0");

    etc = _tgetenv (_T("BWSETC"));
	if (etc == NULL) return;
	CString InitialDir;
	InitialDir.Format (_T("%s\\CodePage"), etc);
    ZeroMemory (&fnstruct, sizeof (fnstruct));
    fnstruct.lStructSize = sizeof (fnstruct);
	fnstruct.hwndOwner   = this->m_hWnd;
    fnstruct.lpstrFile   = CodePageName;
    fnstruct.nMaxFile    = 255;
    fnstruct.lpstrFilter = lpstrFilter;
    fnstruct.lpstrInitialDir  = InitialDir.GetBuffer ();
	fnstruct.lpstrTitle = _T("Sprache ausw�hlen");
    BOOL ret = GetOpenFileName (&fnstruct);
	if (ret)
	{
		m_KunPage->Code.DestroyAll ();
		CString CodeFile = CodePageName;
		m_KunPage->Code.Load (CodeFile);
	}
}

void CKunView::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_KunPage->Form.Get();
		m_KunPage2->Form.Get();
		m_KunPage3->Form.Get();
		m_KunPage4->Form.Get();

		if (!m_KunPage2->TestFields ())
		{
			return;
		}
		m_KunPage2->F5Pressed = TRUE;

		if (!m_KunPage3->TestAdr3Type ())
		{
			return;
		}

		if (test == 1)
		{
			m_KunPage5->Form.Get();
		}
		
		
		m_KunPage7->Form.Get();
		if (!m_KunPage7->TestFields ())
		{
			return;
		}
		m_KunPage7->F5Pressed = TRUE;
		m_KunPage->write ();
	}
}

void CKunView::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_KunPage->OnDelete ();
	}
}

void CKunView::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		CDbPropertyPage *Page = (CDbPropertyPage *) m_PropertySheet->GetActivePage ();
		if (Page != NULL)
		{
			Page->F5Pressed = TRUE;
		}
		m_KunPage->StepBack ();
	}
	else
	{
		exit(0);
	}

}

void CKunView::DeletePropertySheet ()
{
	try
	{
		if (m_KunPage != NULL)
		{
			m_PropertySheet->RemovePage (m_KunPage);
		}
/*
		if (m_KunPflegPageEx != NULL)
		{
			m_PropertySheet->RemovePage (m_KunPflegPageEx);
		}
*/

		if (m_PropertySheet != NULL)
		{
			delete m_PropertySheet;
			m_PropertySheet = NULL;
		}

//		m_KunPflegPage->KunPflegPageEx = NULL;
		m_KunPage->KunProperty = NULL;
//		m_KunPflegPageEx->KunPflegPage = NULL;
//		m_KunPflegPageEx->KunProperty = NULL;

		if (m_KunPage != NULL)
		{
			delete m_KunPage;
		}
/*
		if (m_KunPflegPageEx != NULL)
		{
			delete m_KunPflegPageEx;
		}
*/
	}
	catch (...) {}
}


void CKunView::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_KunPage->OnCopy ();
	}
}

void CKunView::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_KunPage->OnPaste ();
	}
}

void CKunView::OnTabcopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_KunPage->OnCopyEx ();
	}
}

void CKunView::OnTabpaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_PropertySheet != NULL)
	{
		m_KunPage->OnPasteEx ();
	}
}

/*
void CKunView::OnTextCent()
{
	if (m_PropertySheet != NULL)
	{
		m_KunPage->OnTextCent ();
	}
}

void CKunView::OnTextLeft()
{
	if (m_PropertySheet != NULL)
	{
		m_KunPage->OnTextLeft ();
	}
}

void CKunView::OnTextRight()
{
	if (m_PropertySheet != NULL)
	{
		m_KunPage->OnTextRight ();
	}
}
*/

void CKunView::Show ()
{
   for (int i = 1; i < m_PropertySheet->GetPageCount (); i ++)
   {
		CKunPage *Page = (CKunPage *) m_PropertySheet->GetPage (i);
		if (IsWindow (Page->m_hWnd))
		{
			Page->UpdatePage ();
		}
   }
}

void CKunView::Get ()
{
  
}

void CKunView::Back ()
{
	try
	{
		CDbPropertyPage *Page = (CDbPropertyPage *) m_PropertySheet->GetActivePage ();
		if (Page != NULL)
		{
			Page->F5Pressed = TRUE;
		}
		m_KunPage->StepBack();
	}
	catch (...) {}
}

void CKunView::Write ()
{
	if (!m_KunPage2->TestFields ())
	{
		return;
	}
	m_KunPage->write();
}

void CKunView::Delete ()
{
	m_KunPage->OnDelete();
}

void CKunView::UpdateAll ()
{
	
}

void CKunView::OnFilePrint()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CProcess print;
	CString Command = "dr70001 -name 16500";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
}
