#ifndef _ZUSCHLAG_DEF
#define _ZUSCHLAG_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct ZUSCHLAG {
   short          mdn;
   long           kun;
   double         grenz_stu1;
   double         grenz_stu2;
   double         grenz_stu3;
   double         grenz_stu4;
   double         proz_stu1;
   double         proz_stu2;
   double         proz_stu3;
   double         proz_stu4;
};
extern struct ZUSCHLAG zuschlag, zuschlag_null;

#line 8 "zuschlag.rh"

class ZUSCHLAG_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               ZUSCHLAG zuschlag;  
               ZUSCHLAG_CLASS () : DB_CLASS ()
               {
               }
};
#endif
