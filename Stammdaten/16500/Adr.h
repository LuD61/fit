#ifndef _ADR_DEF
#define _ADR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct ADR {
   long           adr;
   TCHAR          adr_krz[17];
   TCHAR          adr_nam1[37];
   TCHAR          adr_nam2[37];
   short          adr_typ;
   TCHAR          adr_verkt[17];
   short          anr;
   short          delstatus;
   TCHAR          fax[21];
   short          fil;
   DATE_STRUCT    geb_dat;
   short          land;
   short          mdn;
   TCHAR          merkm_1[3];
   TCHAR          merkm_2[3];
   TCHAR          merkm_3[3];
   TCHAR          merkm_4[3];
   TCHAR          merkm_5[3];
   TCHAR          modem[21];
   TCHAR          ort1[37];
   TCHAR          ort2[37];
   TCHAR          partner[37];
   TCHAR          pf[17];
   TCHAR          plz[9];
   short          staat;
   TCHAR          str[37];
   TCHAR          tel[21];
   TCHAR          telex[21];
   long           txt_nr;
   TCHAR          plz_postf[9];
   TCHAR          plz_pf[9];
   TCHAR          iln[33];
   TCHAR          email[37];
   TCHAR          swift[25];
   TCHAR          adr_nam3[37];
   TCHAR          mobil[21];
   TCHAR          iban[25];
   TCHAR          spartner[37];
};
extern struct ADR adr, adr_null;

#line 8 "adr.rh"

class ADR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
		int updSepa_cursor; 
       public :
               ADR adr;  
               ADR saveadr;  
               ADR_CLASS () : DB_CLASS (), updSepa_cursor (-1)
               {
               }
	int UpdateSepa (void);
};
#endif
