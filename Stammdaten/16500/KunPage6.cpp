// KunPage6.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "KunPage6.h"
#include "stdafx.h"
#include "16500.h"
#include "DbPropertyPage.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "token.h"
#include "ptabn.h"
#include "gr_zuord.h"
#include "fil.h"
#include "rab_stufek.h"
#include "haus_bank.h"


// CKunPage6-Dialogfeld

IMPLEMENT_DYNAMIC(CKunPage6, CDbPropertyPage)

CKunPage6::CKunPage6()
	: CDbPropertyPage(CKunPage6::IDD)
{
	hBrush = NULL;
	hBrushStatic = NULL;
	Search = _T("");

	CUniFormField::Code = &Code;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Kun);
}

CKunPage6::~CKunPage6()
{

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
}

void CKunPage6::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_KUNNAME, m_KunName);
	DDX_Control(pDX, IDC_HEADBORDER, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDER, m_DataBorder);
	DDX_Control(pDX, IDC_DATABORDER2, m_DataBorder2);
}


BEGIN_MESSAGE_MAP(CKunPage6, CDbPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_KILLFOCUS ()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
END_MESSAGE_MAP()


// CKunPage6-Meldungshandler


void CKunPage6::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CKunPage6::OnInitDialog()
{
	CDialog::OnInitDialog();

	F5Pressed = FALSE;
	KunProperty = (CPropertySheet *) GetParent ();
	View = (DbFormView *) KunProperty->GetParent ();
	Frame = (CFrameWnd *) View->GetParent ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();

	CRect bRect (0, 0, 100, 20);;

	Form.Add (new CFormField (&m_Mdn,EDIT,        (short *)  &Mdn->mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *)   MdnAdr->adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Kun,EDIT,        (long *)   &Kun->kun.kun, VLONG));
    Form.Add (new CUniFormField (&m_KunName,EDIT, (char *)   KunAdr->adr.adr_krz, VCHAR));
	
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 1, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	

	KunGrid.Create (this, 1, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	
	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 4);
    c_HeadBorder->rightspace = 20; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 3, 1, 4, 1);
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LKun = new CCtrlInfo (&m_LKun, 1, 2, 1, 1);
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid = new CCtrlInfo (&KunGrid, 2, 2, 1, 1);
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 3, 2, 4, 1);
	CtrlGrid.Add (c_KunName);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 4, 3, DOCKBOTTOM);
	c_DataBorder->rightspace = 20;
	CtrlGrid.Add (c_DataBorder);

	CCtrlInfo *c_DataBorder2 = new CCtrlInfo (&m_DataBorder2, 3, 4, DOCKRIGHT, DOCKBOTTOM);
    c_DataBorder2->rightspace = 20;
	CtrlGrid.Add (c_DataBorder2);

	
	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	CtrlGrid.Display ();
    CtrlGrid.SetItemCharHeight (&m_HeadBorder, 3);
    EnableFields (TRUE);
    Form.Show ();

	return TRUE;
}


BOOL CKunPage6::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Form.Get();
				Update->Write();
				return TRUE;
			}

			
			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}
			if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

HBRUSH CKunPage6::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);

    if (hBrush == NULL)
	{
		  hBrush = CreateSolidBrush (Color);
		  hBrushStatic = CreateSolidBrush (Color);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			return hBrush;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CButton )))
	{
            pDC->SetBkColor (Color);
			pDC->SetBkMode (TRANSPARENT);
			return hBrushStatic;
	}
	return CPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CKunPage6::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CKunPage6::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}


void CKunPage6::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CKunPage6::OnEditPaste()
{
	// TODO: 6F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}

void CKunPage6::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


	if (m_Mdn.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein SDatz zum L�schen selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	if (MessageBox (_T("Preisauszeichnerdaten l�schen"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		Kun->beginwork ();
		Kun->dbdelete ();
		Kun->commitwork ();
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		EnableFields (FALSE);
		Form.Show ();
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);
	}
}

void CKunPage6::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
//		Control->SetReadOnly (!enable);
        Control->EnableWindow (enable);
	}
}

BOOL CKunPage6::StepBack ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
					Frame->DestroyWindow ();
					return FALSE;
	}
	else
	{
		EnableFields (FALSE);
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);	
	}
	return TRUE;
}


void CKunPage6::OnCopy ()
{
    OnEditCopy ();
}  



void CKunPage6::OnPaste ()
{
	OnEditPaste ();
}

void CKunPage6::OnCopyEx ()
{

//	if (!m_A.IsWindowEnabled ())
	{
		Copy ();
		return;
	}
}

void CKunPage6::OnPasteEx ()
{

//	if (!m_A.IsWindowEnabled () && Paste ())
	{
		Kun->CopyData (&kun);
		Kun_erw->CopyData (&kun_erw);
		Form.Show ();
/*
		if (KunPageEx != NULL && IsWindow (KunPageEx->m_hWnd))
		{
			KunPageEx->Form.Show ();
		}
*/
		return;
	}
}

BOOL CKunPage6::Copy ()
{
	Form.Get ();
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return FALSE;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      int err = GetLastError ();
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return FALSE;  
    }

	try
	{
        UINT CF_A_KUN_GX = RegisterClipboardFormat (_T("kun")); 
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, sizeof (KUN)); 
        char *p = (char *) GlobalLock(hglbCopy);
        memcpy (p, &Kun->kun, sizeof (Kun->kun));
        GlobalUnlock(hglbCopy); 
		HANDLE cData;
		cData = ::SetClipboardData( CF_A_KUN_GX, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
    return TRUE;
}

BOOL CKunPage6::Paste ()
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return FALSE;
    }

    HGLOBAL hglbCopy;
    UINT CF_A_KUN_GX;
    _TCHAR name [256];
    CF_A_KUN_GX = EnumClipboardFormats (0);
	if (CF_A_KUN_GX == 0)
	{
		int err = GetLastError ();
		CloseClipboard();
		return FALSE;
	}

    int ret;
    while ((ret = GetClipboardFormatName (CF_A_KUN_GX, name, sizeof (name))) != 0)
    { 
         CString Name = name;
         if (Name == _T("a_kun_gx"))
         {
              break;
         }  
		 CF_A_KUN_GX = EnumClipboardFormats (CF_A_KUN_GX);
    }
    if (ret == 0)
    {
         CloseClipboard();
         return FALSE;
    }    

    try
    {	
  	     hglbCopy =  ::GetClipboardData(CF_A_KUN_GX);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
 	     LPSTR p = (LPSTR) GlobalLock ((HGLOBAL) hglbCopy);
         memcpy ( (LPSTR) &kun, p, sizeof (kun));
  	     GlobalUnlock ((HGLOBAL) hglbCopy);
    }
    catch (...) {};
    CloseClipboard();
    return TRUE; 
}

void CKunPage6::EnableFields (BOOL b)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (!b)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);
}


void CKunPage6::OnKillFocus (CWnd *newFocus)
{
	CWnd *Control = GetFocus ();
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		if (f->Scale > 0)
		{
			f->Show ();
		}
	}
}


void CKunPage6::FillPtabCombo (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem)); 
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert)); 
		Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0); 

		
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn->sqlcursor (Sql.GetBuffer ());
		while (Ptabn->sqlfetch (cursor) == 0)
		{
			Ptabn->dbreadfirst();

			LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn->ptabn.ptwert,
				                             Ptabn->ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn->sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}

BOOL CKunPage6::OnKillActive ()
{
	Form.Get ();
	if (F5Pressed)
	{
		return TRUE;
	}
	return TRUE;
}

BOOL CKunPage6::OnSetActive ()
{
	F5Pressed = FALSE;
	Form.Show ();
	return TRUE;
}

void CKunPage6::UpdatePage ()
{
	Form.Show ();
}
