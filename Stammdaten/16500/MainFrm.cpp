// MainFrm.cpp : Implementierung der Klasse CMainFrame
//

#include "stdafx.h"
#include "16500.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // Statusleistenanzeige
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame Erstellung/Zerst�rung

CMainFrame::CMainFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
	KunWnd = NULL;
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT | TBSTYLE_WRAPABLE, 
		WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Symbolleiste konnte nicht erstellt werden\n");
		return -1;      // Fehler bei Erstellung
	}

	if (!m_wndToolBar2.CreateEx(this, TBSTYLE_FLAT, 
		  WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar2.LoadToolBar(IDR_DLGMENU))
	{
		TRACE0("Symbolleiste konnte nicht erstellt werden\n");
		return -1;      
	}


	if (!m_InstBar.CreateEx(this, TBSTYLE_FLAT | TBSTYLE_WRAPABLE, 
		  WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC))
	{
		TRACE0("Symbolleiste konnte nicht erstellt werden\n");
		return -1;      // Fehler bei Erstellung
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Statusleiste konnte nicht erstellt werden\n");
		return -1;      // Fehler bei Erstellung
	}

	// TODO: L�schen Sie diese drei Zeilen, wenn Sie nicht m�chten, dass die Systemleiste andockbar ist
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);


    m_wndToolBar2.SetButtonStyle (0, TBBS_AUTOSIZE);   
    m_wndToolBar2.SetButtonStyle (1, TBBS_AUTOSIZE);   
    m_wndToolBar2.SetButtonStyle (2, TBBS_AUTOSIZE);   

	m_wndToolBar2.EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar2);
	VERIFY (m_wndToolBar2.SetButtonText (0, _T("Stammdaten")));
	VERIFY (m_wndToolBar2.SetButtonText (1, _T("        ")));
	VERIFY (m_wndToolBar2.SetButtonText (2, _T("        ")));


    CRect temp;
    m_wndToolBar2.GetItemRect(0,&temp);

    m_wndToolBar2.GetToolBarCtrl().SetButtonSize(CSize(temp.Width(),
      temp.Height()));

	m_InstBar.EnableDocking(CBRS_ALIGN_TOP);

	return 0;
}

void CMainFrame::AddToolbarButton (LPCTSTR Text)
{
	CToolBarCtrl& tc = m_InstBar.GetToolBarCtrl ();
	TCHAR *bText = new TCHAR [_tcslen (Text) + 1];
    _tcscpy (bText, Text);
	_tcscat (bText, _T(""));
	int idx = tc.AddStrings (bText);

	TBBUTTON *tb = new TBBUTTON;
	tb->iBitmap = -1;
	tb->idCommand = ID_BUTTONS + idx;
    tb->fsState = TBSTATE_ENABLED;
	tb->fsStyle = TBSTYLE_BUTTON;
	tb->dwData = 0xFFFF;
	tb->iString = idx;
	tc.AddButtons (1, tb);
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return TRUE;
}


// CMainFrame Diagnose

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
   CMenu* pMenu = NULL;
   if (m_hMenuDefault == NULL)
   {
      // default implementation for MFC V1 backward compatibility
      pMenu = GetMenu();
      ASSERT(pMenu != NULL);
      // This is attempting to guess which sub-menu is the Window menu.
      // The Windows user interface guidelines say that the right-most
      // menu on the menu bar should be Help and Window should be one
      // to the left of that.
      int iMenu = pMenu->GetMenuItemCount() - 2;

      // If this assertion fails, your menu bar does not follow the guidelines
      // so you will have to override this function and call CreateClient
      // appropriately or use the MFC V2 MDI functionality.
      ASSERT(iMenu >= 0);
      pMenu = pMenu->GetSubMenu(iMenu);
      ASSERT(pMenu != NULL);
   }

   return CreateClient(lpcs, pMenu);
}

// CMainFrame Meldungshandler


