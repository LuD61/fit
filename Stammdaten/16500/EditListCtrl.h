#pragma once
#include "afxcmn.h"
#include "FillList.h"
#include "ListEdit.h"
#include "ListComboBox.h"
#include "ListDropEx.h"
#include "Vector.h"

class CEditListCtrl :
	public CListCtrl
{

public:
	CEditListCtrl(void);
	~CEditListCtrl(void);
protected :
	DECLARE_MESSAGE_MAP()
	virtual void DrawItem (LPDRAWITEMSTRUCT);
public :
	CListDropEx dropTarget;

	UINT LimitText;
	BOOL GridLines;
	CFillList FillList;
	BOOL SetEditFocus;
	int EditRow;
	int EditCol;
	CListEdit ListEdit;
	CListComboBox ListComboBox;
	virtual void StartEnter (int, int);
	virtual void StartEnterCombo (int, int, CVector *);
	virtual void StopEnter ();
	virtual void SetEditText ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual BOOL OnKeyD (WPARAM);
	virtual BOOL OnLBuDown (CPoint&);
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	virtual void ClearSelect ();
    void EnsureColVisible (int);

    static int jrhstart;
    static int jrh1;
    static int jrh2;
    static int sjr;
    static double StrToDouble (LPTSTR);
    static double StrToDouble (CString&);
    static void DatFormat (CString &, LPTSTR);
    static BOOL IsMon31 (int);
    static BOOL IsMon30 (int);
    static BOOL IsMon29 (int, int);
    static BOOL IsMon28 (int, int);

	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar); 
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar); 
    afx_msg void OnKeyDown(UINT nTCHAR, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSetFocus (CWnd *);
	afx_msg void OnKillFocus (CWnd *);
	afx_msg void OnEnKillFocusEdit();
	afx_msg void OnLButtonDown (UINT, CPoint);
	afx_msg void OnMouseMove (UINT, CPoint);
};
