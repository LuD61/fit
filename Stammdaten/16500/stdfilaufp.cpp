#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "stdfilaufp.h"

struct STDFILAUFP stdfilaufp, stdfilaufp_null;

/*
create unique index "fit".i01stdfilaufp on "fit".stdfilaufp (a,lfd,wo_tag,
    fil,mdn);

*/

void STDFILAUFP_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &stdfilaufp.mdn, 1, 0);
            ins_quest ((char *)   &stdfilaufp.fil, 1, 0);
            ins_quest ((char *)   &stdfilaufp.a, 3, 0);
            ins_quest ((char *)   &stdfilaufp.wo_tag,  0, 3);
            ins_quest ((char *)   &stdfilaufp.lfd,  2, 0);
    out_quest ((char *) &stdfilaufp.mdn,1,0);
    out_quest ((char *) &stdfilaufp.fil,1,0);
    out_quest ((char *) stdfilaufp.wo_tag,0,3);
    out_quest ((char *) &stdfilaufp.lfd,2,0);
    out_quest ((char *) &stdfilaufp.a,3,0);
    out_quest ((char *) stdfilaufp.me_einh_bz,0,7);
    out_quest ((char *) &stdfilaufp.me_einh,1,0);
    out_quest ((char *) &stdfilaufp.me,3,0);
    out_quest ((char *) &stdfilaufp.posi,2,0);
            cursor = prepare_sql ("select stdfilaufp.mdn,  "
"stdfilaufp.fil,  stdfilaufp.wo_tag,  stdfilaufp.lfd,  stdfilaufp.a,  "
"stdfilaufp.me_einh_bz,  stdfilaufp.me_einh,  stdfilaufp.me,  "
"stdfilaufp.posi from stdfilaufp "

#line 30 "stdfilaufp.rpp"
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   a   = ? "
                                  "and   wo_tag = ? "
                                  "and lfd = ?");
    ins_quest ((char *) &stdfilaufp.mdn,1,0);
    ins_quest ((char *) &stdfilaufp.fil,1,0);
    ins_quest ((char *) stdfilaufp.wo_tag,0,3);
    ins_quest ((char *) &stdfilaufp.lfd,2,0);
    ins_quest ((char *) &stdfilaufp.a,3,0);
    ins_quest ((char *) stdfilaufp.me_einh_bz,0,7);
    ins_quest ((char *) &stdfilaufp.me_einh,1,0);
    ins_quest ((char *) &stdfilaufp.me,3,0);
    ins_quest ((char *) &stdfilaufp.posi,2,0);
            sqltext = "update stdfilaufp set "
"stdfilaufp.mdn = ?,  stdfilaufp.fil = ?,  stdfilaufp.wo_tag = ?,  "
"stdfilaufp.lfd = ?,  stdfilaufp.a = ?,  stdfilaufp.me_einh_bz = ?,  "
"stdfilaufp.me_einh = ?,  stdfilaufp.me = ?,  stdfilaufp.posi = ? "

#line 36 "stdfilaufp.rpp"
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   a   = ? "
                                  "and   wo_tag = ? "
                                  "and lfd = ?";
            ins_quest ((char *)   &stdfilaufp.mdn, 1, 0);
            ins_quest ((char *)   &stdfilaufp.fil, 1, 0);
            ins_quest ((char *)   &stdfilaufp.a, 3, 0);
            ins_quest ((char *)   &stdfilaufp.wo_tag,  0, 3);
            ins_quest ((char *)   &stdfilaufp.lfd,  2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)   &stdfilaufp.mdn, 1, 0);
            ins_quest ((char *)   &stdfilaufp.fil, 1, 0);
            ins_quest ((char *)   &stdfilaufp.a, 3, 0);
            ins_quest ((char *)   &stdfilaufp.wo_tag,  0, 3);
            ins_quest ((char *)   &stdfilaufp.lfd,  2, 0);
            test_upd_cursor = prepare_sql ("select a from stdfilaufp "
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   a   = ? "
                                  "and   wo_tag = ? "
                                  "and lfd = ? "
                                  "for update");

            ins_quest ((char *)   &stdfilaufp.mdn, 1, 0);
            ins_quest ((char *)   &stdfilaufp.fil, 1, 0);
            ins_quest ((char *)   &stdfilaufp.a, 3, 0);
            ins_quest ((char *)   &stdfilaufp.wo_tag,  0, 3);
            ins_quest ((char *)   &stdfilaufp.lfd,  2, 0);
            del_cursor = prepare_sql ("delete from stdfilaufp "
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   a   = ? "
                                  "and   wo_tag = ? "
                                  "and lfd = ?");
    ins_quest ((char *) &stdfilaufp.mdn,1,0);
    ins_quest ((char *) &stdfilaufp.fil,1,0);
    ins_quest ((char *) stdfilaufp.wo_tag,0,3);
    ins_quest ((char *) &stdfilaufp.lfd,2,0);
    ins_quest ((char *) &stdfilaufp.a,3,0);
    ins_quest ((char *) stdfilaufp.me_einh_bz,0,7);
    ins_quest ((char *) &stdfilaufp.me_einh,1,0);
    ins_quest ((char *) &stdfilaufp.me,3,0);
    ins_quest ((char *) &stdfilaufp.posi,2,0);
            ins_cursor = prepare_sql ("insert into stdfilaufp ("
"mdn,  fil,  wo_tag,  lfd,  a,  me_einh_bz,  me_einh,  me,  posi) "

#line 73 "stdfilaufp.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?)"); 

#line 75 "stdfilaufp.rpp"
}
