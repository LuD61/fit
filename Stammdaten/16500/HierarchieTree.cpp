// HierarchieTree.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "HierarchieTree.h"
#include "Token.h"


// CHierarchieTree-Dialogfeld

IMPLEMENT_DYNAMIC(CHierarchieTree, CDialog)

CHierarchieTree::CHierarchieTree(CWnd* pParent /*=NULL*/)
	: CDialog(CHierarchieTree::IDD, pParent)
{
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
}

CHierarchieTree::~CHierarchieTree()
{
}

void CHierarchieTree::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LHIERARCHIE_TREE, m_LHierarchieTree);
	DDX_Control(pDX, IDC_HIERARCHIE_TREE, m_HierarchieTree);
}


BEGIN_MESSAGE_MAP(CHierarchieTree, CDialog)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_NOTIFY(TVN_SELCHANGED, IDC_HIERARCHIE_TREE, OnTvnSelchangedEmbList)
END_MESSAGE_MAP()


// CHierarchieTree-Meldungshandler

BOOL CHierarchieTree::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}


    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0, 0, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 0);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LHierarchieTree = new CCtrlInfo (&m_LHierarchieTree, 1, 1, 1, 1);
	CtrlGrid.Add (c_LHierarchieTree);

	CCtrlInfo *c_HierarchieTree = new CCtrlInfo (&m_HierarchieTree, 0, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_HierarchieTree);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_HierarchieTree.SetFont (&Font);
	m_HierarchieTree.Init ();
	m_HierarchieTree.Read ();
	CtrlGrid.Display ();

	return FALSE;
}

HBRUSH CHierarchieTree::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CHierarchieTree::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CHierarchieTree::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :

/*
			if (pMsg->wParam == VK_F5)
			{
				return TRUE;
			}
*/

			if (pMsg->wParam == VK_F7)
			{
//				Delete ();
				return TRUE;
			}

/*
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}
*/

	}
	return FALSE;
}

int CHierarchieTree::GetRootPath (HTREEITEM Item, int p)
{
	HTREEITEM ht = m_HierarchieTree.GetParentItem (Item);
	if (ht == NULL)
	{
		return p;
	}
	return GetRootPath (ht, p + 1);
}


void CHierarchieTree::OnTvnSelchangedEmbList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;

	TVITEM Item = pNMTreeView->itemNew;

	HTREEITEM ht = m_HierarchieTree.GetSelectedItem ();
    int rootpath = GetRootPath (ht, 0);

	CString Text = m_HierarchieTree.GetItemText (ht);
	CToken t (Text, " ");
	int count = t.GetAnzToken ();
	if (count == 0) return;

	if (rootpath == 0)
	{
		H1->h1.hirarchie1 = _tstol (t.GetToken (0));
		memcpy (&H2->h2, &h2_null, sizeof (struct H2));
		memcpy (&H3->h3, &h3_null, sizeof (struct H3));
		memcpy (&H4->h4, &h4_null, sizeof (struct H4));
		H1->dbreadfirst ();
		H2->h2.hirarchie1 = H1->h1.hirarchie1;
		H2->sqlopen (cursor_h21);
		H2->sqlfetch (cursor_h21);
		H2->dbreadfirst ();
		H3->h3.hirarchie2 = H2->h2.hirarchie2;
		H3->sqlopen (cursor_h32);
		H3->sqlfetch (cursor_h32);
		H3->dbreadfirst ();
		H4->h4.hirarchie3 = H3->h3.hirarchie3;
		H4->sqlopen (cursor_h43);
		H4->sqlfetch (cursor_h43);
		H4->dbreadfirst ();
	}

	else if (rootpath == 1)
	{
		H2->h2.hirarchie2 = _tstol (t.GetToken (0));
		memcpy (&H3->h3, &h3_null, sizeof (struct H3));
		memcpy (&H4->h4, &h4_null, sizeof (struct H4));
		H2->dbreadfirst ();

		H3->h3.hirarchie2 = H2->h2.hirarchie2;
		H3->sqlopen (cursor_h32);
		H3->sqlfetch (cursor_h32);
		H3->dbreadfirst ();
		H4->h4.hirarchie3 = H3->h3.hirarchie3;
		H4->sqlopen (cursor_h43);
		H4->sqlfetch (cursor_h43);
		H4->dbreadfirst ();

		H1->h1.hirarchie1 = H2->h2.hirarchie1;
		H1->dbreadfirst ();
	}

	else if (rootpath == 2)
	{
		H3->h3.hirarchie3 = _tstol (t.GetToken (0));
		memcpy (&H4->h4, &h4_null, sizeof (struct H4));
		H3->dbreadfirst ();

		H4->h4.hirarchie3 = H3->h3.hirarchie3;
		H4->sqlopen (cursor_h43);
		H4->sqlfetch (cursor_h43);
		H4->dbreadfirst ();

		H2->h2.hirarchie2 = H3->h3.hirarchie2;
		H2->dbreadfirst ();
		H1->h1.hirarchie1 = H2->h2.hirarchie1;
		H1->dbreadfirst ();
	}

	else if (rootpath == 3)
	{
		H4->h4.hirarchie4 = _tstol (t.GetToken (0));
		H4->dbreadfirst ();

		H3->h3.hirarchie3 = H4->h4.hirarchie3;
		H3->dbreadfirst ();
		H2->h2.hirarchie2 = H3->h3.hirarchie2;
		H2->dbreadfirst ();
		H1->h1.hirarchie1 = H2->h2.hirarchie1;
		H1->dbreadfirst ();
	}

	for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
	{
		CUpdateEvent *event = *e;
		event->Update ();
	}
}

void CHierarchieTree::Update ()
{
	m_HierarchieTree.Read ();
}

void CHierarchieTree::DeleteRow ()
{
	HTREEITEM ht = m_HierarchieTree.GetSelectedItem ();
	CString Text = m_HierarchieTree.GetItemText (ht);
	CToken t (Text, " ");
	int count = t.GetAnzToken ();
	if (count == 0) return;

	INT_PTR ret = AfxMessageBox (_T("Hierarchiezweig l�schen ?"),MB_YESNO | MB_ICONQUESTION);
	if (ret != IDYES)
	{
		return;
	}
    int rootpath = GetRootPath (ht, 0);
	if (rootpath == 0)
	{
		H1->h1.hirarchie1 = _tstol (t.GetToken (0));
		memcpy (&H2->h2, &h2_null, sizeof (struct H2));
		memcpy (&H3->h3, &h3_null, sizeof (struct H3));
		memcpy (&H4->h4, &h4_null, sizeof (struct H4));
		H1->dbreadfirst ();
		H1->dbdelete ();
		DeleteH2 ();
		memcpy (&H1->h1, &h1_null, sizeof (struct H1));
	}
    if (rootpath == 1)
	{
		H2->h2.hirarchie2 = _tstol (t.GetToken (0));
		memcpy (&H3->h3, &h3_null, sizeof (struct H3));
		memcpy (&H4->h4, &h4_null, sizeof (struct H4));
		H2->dbreadfirst ();
		H2->dbdelete ();
		DeleteH3 ();
		memcpy (&H2->h2, &h2_null, sizeof (struct H2));
	}
    if (rootpath == 2)
	{
		H3->h3.hirarchie3 = _tstol (t.GetToken (0));
		memcpy (&H4->h4, &h4_null, sizeof (struct H4));
		H3->dbreadfirst ();
		H3->dbdelete ();
		DeleteH4 ();
		memcpy (&H3->h3, &h3_null, sizeof (struct H3));
	}
    if (rootpath == 2)
	{
		H3->h3.hirarchie3 = _tstol (t.GetToken (0));
		memcpy (&H4->h4, &h4_null, sizeof (struct H4));
		H3->dbreadfirst ();
		H3->dbdelete ();
		DeleteH4 ();
		memcpy (&H3->h3, &h3_null, sizeof (struct H3));
	}
    if (rootpath == 3)
	{
		H3->h3.hirarchie3 = _tstol (t.GetToken (0));
		H4->dbreadfirst ();
		H4->dbdelete ();
		memcpy (&H4->h4, &h4_null, sizeof (struct H4));
	}
	m_HierarchieTree.Read ();
}

void CHierarchieTree::DeleteH2 ()
{
	H2->h2.hirarchie1 = H1->h1.hirarchie1;
	H2->sqlopen (cursor_h21);
	while (H2->sqlfetch (cursor_h21) == 0)
	{
		H2->dbdelete ();
		DeleteH3 ();
	}
}

void CHierarchieTree::DeleteH3 ()
{
	H3->h3.hirarchie2 = H2->h2.hirarchie2;
	H3->sqlopen (cursor_h32);
	while (H3->sqlfetch (cursor_h32) == 0)
	{
		H3->dbdelete ();
		DeleteH4 ();
	}
}

void CHierarchieTree::DeleteH4 ()
{
	H4->h4.hirarchie3 = H3->h3.hirarchie3;
	H4->sqlopen (cursor_h43);
	while (H4->sqlfetch (cursor_h43) == 0)
	{
		H4->dbdelete ();
	}
}
