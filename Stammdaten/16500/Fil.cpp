#include "stdafx.h"
#include "fil.h"

struct FIL fil, fil_null;

void FIL_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &fil.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &fil.fil,  SQLSHORT, 0);
    sqlout ((TCHAR *) fil.abr_period,SQLCHAR,2);
    sqlout ((long *) &fil.adr,SQLLONG,0);
    sqlout ((long *) &fil.adr_lief,SQLLONG,0);
    sqlout ((short *) &fil.afl,SQLSHORT,0);
    sqlout ((TCHAR *) fil.auf_typ,SQLCHAR,2);
    sqlout ((TCHAR *) fil.best_kz,SQLCHAR,2);
    sqlout ((TCHAR *) fil.bli_kz,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &fil.dat_ero,SQLDATE,0);
    sqlout ((short *) &fil.daten_mnp,SQLSHORT,0);
    sqlout ((short *) &fil.delstatus,SQLSHORT,0);
    sqlout ((short *) &fil.fil,SQLSHORT,0);
    sqlout ((TCHAR *) fil.fil_kla,SQLCHAR,2);
    sqlout ((short *) &fil.fil_gr,SQLSHORT,0);
    sqlout ((double *) &fil.fl_lad,SQLDOUBLE,0);
    sqlout ((double *) &fil.fl_nto,SQLDOUBLE,0);
    sqlout ((double *) &fil.fl_vk_ges,SQLDOUBLE,0);
    sqlout ((short *) &fil.frm,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &fil.iakv,SQLDATE,0);
    sqlout ((TCHAR *) fil.inv_rht,SQLCHAR,2);
    sqlout ((long *) &fil.kun,SQLLONG,0);
    sqlout ((TCHAR *) fil.lief,SQLCHAR,17);
    sqlout ((TCHAR *) fil.lief_rht,SQLCHAR,2);
    sqlout ((long *) &fil.lief_s,SQLLONG,0);
    sqlout ((TCHAR *) fil.ls_abgr,SQLCHAR,2);
    sqlout ((TCHAR *) fil.ls_kz,SQLCHAR,2);
    sqlout ((short *) &fil.ls_sum,SQLSHORT,0);
    sqlout ((short *) &fil.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) fil.pers,SQLCHAR,13);
    sqlout ((short *) &fil.pers_anz,SQLSHORT,0);
    sqlout ((TCHAR *) fil.pos_kum,SQLCHAR,2);
    sqlout ((TCHAR *) fil.pr_ausw,SQLCHAR,2);
    sqlout ((TCHAR *) fil.pr_bel_entl,SQLCHAR,2);
    sqlout ((TCHAR *) fil.pr_fil_kz,SQLCHAR,2);
    sqlout ((long *) &fil.pr_lst,SQLLONG,0);
    sqlout ((TCHAR *) fil.pr_vk_kz,SQLCHAR,2);
    sqlout ((double *) &fil.reg_bed_theke_lng,SQLDOUBLE,0);
    sqlout ((double *) &fil.reg_kt_lng,SQLDOUBLE,0);
    sqlout ((double *) &fil.reg_kue_lng,SQLDOUBLE,0);
    sqlout ((double *) &fil.reg_lng,SQLDOUBLE,0);
    sqlout ((double *) &fil.reg_tks_lng,SQLDOUBLE,0);
    sqlout ((double *) &fil.reg_tkt_lng,SQLDOUBLE,0);
    sqlout ((TCHAR *) fil.ret_entl,SQLCHAR,2);
    sqlout ((TCHAR *) fil.smt_kz,SQLCHAR,2);
    sqlout ((short *) &fil.sonst_einh,SQLSHORT,0);
    sqlout ((short *) &fil.sprache,SQLSHORT,0);
    sqlout ((TCHAR *) fil.sw_kz,SQLCHAR,2);
    sqlout ((long *) &fil.tou,SQLLONG,0);
    sqlout ((TCHAR *) fil.umlgr,SQLCHAR,2);
    sqlout ((TCHAR *) fil.verk_st_kz,SQLCHAR,2);
    sqlout ((short *) &fil.vrs_typ,SQLSHORT,0);
    sqlout ((TCHAR *) fil.inv_akv,SQLCHAR,2);
    sqlout ((double *) &fil.planumsatz,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select fil.abr_period,  ")
_T("fil.adr,  fil.adr_lief,  fil.afl,  fil.auf_typ,  fil.best_kz,  fil.bli_kz,  ")
_T("fil.dat_ero,  fil.daten_mnp,  fil.delstatus,  fil.fil,  fil.fil_kla,  ")
_T("fil.fil_gr,  fil.fl_lad,  fil.fl_nto,  fil.fl_vk_ges,  fil.frm,  fil.iakv,  ")
_T("fil.inv_rht,  fil.kun,  fil.lief,  fil.lief_rht,  fil.lief_s,  fil.ls_abgr,  ")
_T("fil.ls_kz,  fil.ls_sum,  fil.mdn,  fil.pers,  fil.pers_anz,  fil.pos_kum,  ")
_T("fil.pr_ausw,  fil.pr_bel_entl,  fil.pr_fil_kz,  fil.pr_lst,  ")
_T("fil.pr_vk_kz,  fil.reg_bed_theke_lng,  fil.reg_kt_lng,  ")
_T("fil.reg_kue_lng,  fil.reg_lng,  fil.reg_tks_lng,  fil.reg_tkt_lng,  ")
_T("fil.ret_entl,  fil.smt_kz,  fil.sonst_einh,  fil.sprache,  fil.sw_kz,  ")
_T("fil.tou,  fil.umlgr,  fil.verk_st_kz,  fil.vrs_typ,  fil.inv_akv,  ")
_T("fil.planumsatz from fil ")

#line 13 "Fil.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ?"));
    sqlin ((TCHAR *) fil.abr_period,SQLCHAR,2);
    sqlin ((long *) &fil.adr,SQLLONG,0);
    sqlin ((long *) &fil.adr_lief,SQLLONG,0);
    sqlin ((short *) &fil.afl,SQLSHORT,0);
    sqlin ((TCHAR *) fil.auf_typ,SQLCHAR,2);
    sqlin ((TCHAR *) fil.best_kz,SQLCHAR,2);
    sqlin ((TCHAR *) fil.bli_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &fil.dat_ero,SQLDATE,0);
    sqlin ((short *) &fil.daten_mnp,SQLSHORT,0);
    sqlin ((short *) &fil.delstatus,SQLSHORT,0);
    sqlin ((short *) &fil.fil,SQLSHORT,0);
    sqlin ((TCHAR *) fil.fil_kla,SQLCHAR,2);
    sqlin ((short *) &fil.fil_gr,SQLSHORT,0);
    sqlin ((double *) &fil.fl_lad,SQLDOUBLE,0);
    sqlin ((double *) &fil.fl_nto,SQLDOUBLE,0);
    sqlin ((double *) &fil.fl_vk_ges,SQLDOUBLE,0);
    sqlin ((short *) &fil.frm,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &fil.iakv,SQLDATE,0);
    sqlin ((TCHAR *) fil.inv_rht,SQLCHAR,2);
    sqlin ((long *) &fil.kun,SQLLONG,0);
    sqlin ((TCHAR *) fil.lief,SQLCHAR,17);
    sqlin ((TCHAR *) fil.lief_rht,SQLCHAR,2);
    sqlin ((long *) &fil.lief_s,SQLLONG,0);
    sqlin ((TCHAR *) fil.ls_abgr,SQLCHAR,2);
    sqlin ((TCHAR *) fil.ls_kz,SQLCHAR,2);
    sqlin ((short *) &fil.ls_sum,SQLSHORT,0);
    sqlin ((short *) &fil.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) fil.pers,SQLCHAR,13);
    sqlin ((short *) &fil.pers_anz,SQLSHORT,0);
    sqlin ((TCHAR *) fil.pos_kum,SQLCHAR,2);
    sqlin ((TCHAR *) fil.pr_ausw,SQLCHAR,2);
    sqlin ((TCHAR *) fil.pr_bel_entl,SQLCHAR,2);
    sqlin ((TCHAR *) fil.pr_fil_kz,SQLCHAR,2);
    sqlin ((long *) &fil.pr_lst,SQLLONG,0);
    sqlin ((TCHAR *) fil.pr_vk_kz,SQLCHAR,2);
    sqlin ((double *) &fil.reg_bed_theke_lng,SQLDOUBLE,0);
    sqlin ((double *) &fil.reg_kt_lng,SQLDOUBLE,0);
    sqlin ((double *) &fil.reg_kue_lng,SQLDOUBLE,0);
    sqlin ((double *) &fil.reg_lng,SQLDOUBLE,0);
    sqlin ((double *) &fil.reg_tks_lng,SQLDOUBLE,0);
    sqlin ((double *) &fil.reg_tkt_lng,SQLDOUBLE,0);
    sqlin ((TCHAR *) fil.ret_entl,SQLCHAR,2);
    sqlin ((TCHAR *) fil.smt_kz,SQLCHAR,2);
    sqlin ((short *) &fil.sonst_einh,SQLSHORT,0);
    sqlin ((short *) &fil.sprache,SQLSHORT,0);
    sqlin ((TCHAR *) fil.sw_kz,SQLCHAR,2);
    sqlin ((long *) &fil.tou,SQLLONG,0);
    sqlin ((TCHAR *) fil.umlgr,SQLCHAR,2);
    sqlin ((TCHAR *) fil.verk_st_kz,SQLCHAR,2);
    sqlin ((short *) &fil.vrs_typ,SQLSHORT,0);
    sqlin ((TCHAR *) fil.inv_akv,SQLCHAR,2);
    sqlin ((double *) &fil.planumsatz,SQLDOUBLE,0);
            sqltext = _T("update fil set fil.abr_period = ?,  ")
_T("fil.adr = ?,  fil.adr_lief = ?,  fil.afl = ?,  fil.auf_typ = ?,  ")
_T("fil.best_kz = ?,  fil.bli_kz = ?,  fil.dat_ero = ?,  ")
_T("fil.daten_mnp = ?,  fil.delstatus = ?,  fil.fil = ?,  fil.fil_kla = ?,  ")
_T("fil.fil_gr = ?,  fil.fl_lad = ?,  fil.fl_nto = ?,  fil.fl_vk_ges = ?,  ")
_T("fil.frm = ?,  fil.iakv = ?,  fil.inv_rht = ?,  fil.kun = ?,  ")
_T("fil.lief = ?,  fil.lief_rht = ?,  fil.lief_s = ?,  fil.ls_abgr = ?,  ")
_T("fil.ls_kz = ?,  fil.ls_sum = ?,  fil.mdn = ?,  fil.pers = ?,  ")
_T("fil.pers_anz = ?,  fil.pos_kum = ?,  fil.pr_ausw = ?,  ")
_T("fil.pr_bel_entl = ?,  fil.pr_fil_kz = ?,  fil.pr_lst = ?,  ")
_T("fil.pr_vk_kz = ?,  fil.reg_bed_theke_lng = ?,  fil.reg_kt_lng = ?,  ")
_T("fil.reg_kue_lng = ?,  fil.reg_lng = ?,  fil.reg_tks_lng = ?,  ")
_T("fil.reg_tkt_lng = ?,  fil.ret_entl = ?,  fil.smt_kz = ?,  ")
_T("fil.sonst_einh = ?,  fil.sprache = ?,  fil.sw_kz = ?,  fil.tou = ?,  ")
_T("fil.umlgr = ?,  fil.verk_st_kz = ?,  fil.vrs_typ = ?,  ")
_T("fil.inv_akv = ?,  fil.planumsatz = ? ")

#line 16 "Fil.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ?");
            sqlin ((short *)   &fil.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &fil.fil,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &fil.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &fil.fil,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select fil from fil ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ?"));
            sqlin ((short *)   &fil.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &fil.fil,  SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from fil ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ?"));
    sqlin ((TCHAR *) fil.abr_period,SQLCHAR,2);
    sqlin ((long *) &fil.adr,SQLLONG,0);
    sqlin ((long *) &fil.adr_lief,SQLLONG,0);
    sqlin ((short *) &fil.afl,SQLSHORT,0);
    sqlin ((TCHAR *) fil.auf_typ,SQLCHAR,2);
    sqlin ((TCHAR *) fil.best_kz,SQLCHAR,2);
    sqlin ((TCHAR *) fil.bli_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &fil.dat_ero,SQLDATE,0);
    sqlin ((short *) &fil.daten_mnp,SQLSHORT,0);
    sqlin ((short *) &fil.delstatus,SQLSHORT,0);
    sqlin ((short *) &fil.fil,SQLSHORT,0);
    sqlin ((TCHAR *) fil.fil_kla,SQLCHAR,2);
    sqlin ((short *) &fil.fil_gr,SQLSHORT,0);
    sqlin ((double *) &fil.fl_lad,SQLDOUBLE,0);
    sqlin ((double *) &fil.fl_nto,SQLDOUBLE,0);
    sqlin ((double *) &fil.fl_vk_ges,SQLDOUBLE,0);
    sqlin ((short *) &fil.frm,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &fil.iakv,SQLDATE,0);
    sqlin ((TCHAR *) fil.inv_rht,SQLCHAR,2);
    sqlin ((long *) &fil.kun,SQLLONG,0);
    sqlin ((TCHAR *) fil.lief,SQLCHAR,17);
    sqlin ((TCHAR *) fil.lief_rht,SQLCHAR,2);
    sqlin ((long *) &fil.lief_s,SQLLONG,0);
    sqlin ((TCHAR *) fil.ls_abgr,SQLCHAR,2);
    sqlin ((TCHAR *) fil.ls_kz,SQLCHAR,2);
    sqlin ((short *) &fil.ls_sum,SQLSHORT,0);
    sqlin ((short *) &fil.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) fil.pers,SQLCHAR,13);
    sqlin ((short *) &fil.pers_anz,SQLSHORT,0);
    sqlin ((TCHAR *) fil.pos_kum,SQLCHAR,2);
    sqlin ((TCHAR *) fil.pr_ausw,SQLCHAR,2);
    sqlin ((TCHAR *) fil.pr_bel_entl,SQLCHAR,2);
    sqlin ((TCHAR *) fil.pr_fil_kz,SQLCHAR,2);
    sqlin ((long *) &fil.pr_lst,SQLLONG,0);
    sqlin ((TCHAR *) fil.pr_vk_kz,SQLCHAR,2);
    sqlin ((double *) &fil.reg_bed_theke_lng,SQLDOUBLE,0);
    sqlin ((double *) &fil.reg_kt_lng,SQLDOUBLE,0);
    sqlin ((double *) &fil.reg_kue_lng,SQLDOUBLE,0);
    sqlin ((double *) &fil.reg_lng,SQLDOUBLE,0);
    sqlin ((double *) &fil.reg_tks_lng,SQLDOUBLE,0);
    sqlin ((double *) &fil.reg_tkt_lng,SQLDOUBLE,0);
    sqlin ((TCHAR *) fil.ret_entl,SQLCHAR,2);
    sqlin ((TCHAR *) fil.smt_kz,SQLCHAR,2);
    sqlin ((short *) &fil.sonst_einh,SQLSHORT,0);
    sqlin ((short *) &fil.sprache,SQLSHORT,0);
    sqlin ((TCHAR *) fil.sw_kz,SQLCHAR,2);
    sqlin ((long *) &fil.tou,SQLLONG,0);
    sqlin ((TCHAR *) fil.umlgr,SQLCHAR,2);
    sqlin ((TCHAR *) fil.verk_st_kz,SQLCHAR,2);
    sqlin ((short *) &fil.vrs_typ,SQLSHORT,0);
    sqlin ((TCHAR *) fil.inv_akv,SQLCHAR,2);
    sqlin ((double *) &fil.planumsatz,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into fil (")
_T("abr_period,  adr,  adr_lief,  afl,  auf_typ,  best_kz,  bli_kz,  dat_ero,  daten_mnp,  ")
_T("delstatus,  fil,  fil_kla,  fil_gr,  fl_lad,  fl_nto,  fl_vk_ges,  frm,  iakv,  inv_rht,  ")
_T("kun,  lief,  lief_rht,  lief_s,  ls_abgr,  ls_kz,  ls_sum,  mdn,  pers,  pers_anz,  ")
_T("pos_kum,  pr_ausw,  pr_bel_entl,  pr_fil_kz,  pr_lst,  pr_vk_kz,  ")
_T("reg_bed_theke_lng,  reg_kt_lng,  reg_kue_lng,  reg_lng,  reg_tks_lng,  ")
_T("reg_tkt_lng,  ret_entl,  smt_kz,  sonst_einh,  sprache,  sw_kz,  tou,  umlgr,  ")
_T("verk_st_kz,  vrs_typ,  inv_akv,  planumsatz) ")

#line 33 "Fil.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 35 "Fil.rpp"
}
