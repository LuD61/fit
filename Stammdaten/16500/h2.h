#ifndef _H2_DEF
#define _H2_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct H2 {
   long           hirarchie1;
   long           hirarchie2;
   TCHAR          h2_bez[37];
   TCHAR          h2_bezkrz[17];
};
extern struct H2 h2, h2_null;

#line 8 "H2.rh"

class H2_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               H2 h2;  
               H2_CLASS () : DB_CLASS ()
               {
               }
};
#endif
