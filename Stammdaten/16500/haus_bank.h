#ifndef _HAUS_BANK_DEF
#define _HAUS_BANK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct HAUS_BANK {
   short          mdn;
   long           blz;
   char           kto[17];
   char           bank_nam[37];
   char           chk_einr[2];
   short          bank;
   short          delstatus;
};
extern struct HAUS_BANK haus_bank, haus_bank_null;


class HAUS_BANK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               HAUS_BANK haus_bank;  
               HAUS_BANK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
