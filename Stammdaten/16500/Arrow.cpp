// Arrow.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "Arrow.h"


// CArrow

IMPLEMENT_DYNAMIC(CArrow, CWnd)

CArrow::CArrow()
{
	nID = 0;
	TextColor = RGB (0, 0, 0);
	ColorSet = FALSE;
    BkColor = GetSysColor (COLOR_3DFACE);
	Orientation = Left;
}

CArrow::~CArrow()
{
}


BEGIN_MESSAGE_MAP(CArrow, CWnd)
END_MESSAGE_MAP()



// CArrow-Meldungshandler

BOOL CArrow::Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect,
                          CWnd* pParentWnd,   UINT nID)
{
	    this->nID = nID;
	    Style = dwStyle;
        dwStyle |= SS_OWNERDRAW;

		BOOL ret = CStatic::Create (lpszText, dwStyle, rect, pParentWnd, nID);
		return ret;
}

void CArrow::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC cDC;

	cDC.Attach (lpDrawItemStruct->hDC);
	Draw (cDC);
}

void CArrow::Draw (CDC& cDC)
{
	CPen *oldPen;
	CRect rect;

	GetClientRect (&rect);

	CPen Pen (PS_SOLID, 3, TextColor);
	oldPen = cDC.SelectObject (&Pen);
	int x = 0;
	if (Orientation == Center)
	{
		int x = rect.right / 2;
	}
	else if (Orientation == Right)
	{
		int x = rect.right - 1;
	}

	int y = 0;
	int cx = x;
	int cy = rect.bottom - 6;
	cDC.MoveTo (x,y);
	cDC.LineTo (cx, cy);
	cDC.MoveTo (cx,cy);
	if (Orientation == Right)
	{
		cx = 1;
		cDC.LineTo (cx, cy);
		cDC.LineTo (cx +5, cy - 5);
		cDC.MoveTo (cx,cy);
		cDC.LineTo (cx +5, cy + 5);
	}
	else
	{
		cx = rect.right;
		cDC.LineTo (cx, cy);
		cDC.LineTo (cx -5, cy - 5);
		cDC.MoveTo (cx,cy);
		cDC.LineTo (cx -5, cy + 5);
	}
	cDC.SelectObject (oldPen);
}

