#ifndef _KUN_TOU_DEF
#define _KUN_TOU_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct KUN_TOU {
   short          mdn;
   long           kun;
   long           tou;
   char           tou_bz[49];
   long           tou2;
   char           tou_bz2[49];
};
extern struct KUN_TOU kun_tou, kun_tou_null;

#line 8 "kun_tou.rh"

class KUN_TOU_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               KUN_TOU kun_tou;  
               KUN_TOU_CLASS () : DB_CLASS ()
               {
               }
};
#endif
