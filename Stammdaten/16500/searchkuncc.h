#ifndef _SEARCHKUNR_DEF
#define _SEARCHKUNR_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

struct SKUNCC
{
	  char kun [15];
	  char ls [15];
	  char adr_nam1 [38];
	  char plz [22];
	  char ort1 [38];
};

class SEARCHKUNR
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SKUNCC *skuntab;
           static struct SKUNCC skun;
           static int idx;
           static long kunanz;
           static CHQEX *Query;
           static int SearchField;
           static int sokun; 
           static int soadr_nam1; 
           static int soplz; 
           static int soort1; 
           static short mdn;
           static short ccmarkt;

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
		   int cx, cy;
        public :
           SEARCHKUNR ()
           {
			      cx = 80;
				  cy = 24;
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  mdn = 0;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHKUNR ()
           {
                  if (skuntab != NULL)
                  {
                      delete skuntab;
                      skuntab = NULL;
                  }
           }

		   void SetWsize (int cx, int cy)
		   {
			   this->cx = cx;
			   this->cy = cy;
		   }

           SKUNCC *GetSkun (void)
           {
               if (idx == -1) return NULL;
               return &skun;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           SKUNCC *GetNextSkun (void);
           SKUNCC *GetPriorSkun (void);
           SKUNCC *GetFirstSkun (void);
           SKUNCC *GetLastSkun (void);
           static void FillFormat (char *);
           static void FillVlines (char *);
           static void FillCaption (char *);
           static void FillRec (char *, int);
           static int sortkun (const void *, const void *);
           static void SortKun (HWND);
           static int sortadr_nam1 (const void *, const void *);
           static void SortAdr_nam1 (HWND);
           static int sortplz (const void *, const void *);
           static void SortPlz (HWND);
           static int sortort1 (const void *, const void *);
           static void SortOrt1 (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int ReadKun (char *);
           void SetParams (HINSTANCE, HWND);
           void SearchKun (short, short);
};  
#endif