#ifndef _CTRLINFO_DEF
#define _CTRLINFO_DEF
#pragma once
#include "Line.h"

#define HORIZONTAL 1
#define VERTICAL 2
#include "Line.h"
#include "mo_progitem.h"

class CCtrlInfo
{
	DECLARE_DYNAMIC(CCtrlInfo)
public:
		PROG_ITEM Item;
	static BOOL NewStyle;
	int gridx;
	int gridy;
	int gridcx;
	int gridcy;
	int xplus;
	int yplus;
	int cxplus;
	int cyplus;
	int rightspace;
	int width;
	CSize FontSize;
	int tmHeight;
	int tmWidth;
	CWnd *RightDockControl;
	CStatic Space;
	CLine Line;
	int pWidth;
	int Direction;

	CWnd *Parent;
	CWnd *BottomControl;
	int BottomSpace;

	CWnd *cWnd;
	void *Grid;
	CCtrlInfo(void);
	CCtrlInfo(CWnd *cWnd, int, int, int, int);
	CCtrlInfo(void *, int, int, int, int);
    CCtrlInfo(CWnd *, int, int, int,int, int);
	~CCtrlInfo(void);
	void Create(CWnd *cWnd, int, int, int, int);
	void SetPos (int, int, int, int);
	void SetWidth (int width);
	void SetWidth ();
	void SetFont (CFont *);
	void SetCellPos (int, int);
	void SetCellPos (int, int, int, int);
	void RepaintText ();
};
#endif