#ifndef _KUNPAGE_DEF
#define _KUNPAGE_DEF
#pragma once
#include "resource.h"
#include "DbPropertyPage.h"
#include "ListDropTarget.h"
#include "CtrlGrid.h"
#include "Kun.h"
#include "kun_erw.h"
#include "Mdn.h"
#include "Adr.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "FormTab.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "ChoiceAdr.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "DbFormView.h"
#include "CodeProperties.h"
#include "mo_progcfg.h"
#include "sys_par.h"
#include "ptabn.h"
#include "iprgrstufk.h"
#include "i_kun_prk.h"
#include "kst.h"
#include "kun_tou.h"
#include "fil.h"
#include "gr_zuord.h"
#include "zahl_kond.h"
#include "rab_stufek.h"
#include "haus_bank.h"
#include "FrachtKosten.h"
#include "zuschlag.h"
#include "ls_txt.h"
#include "dta.h"
#include "AdrList.h"
#include "SearchListCtrl.h"
#include "StaticButton.h"
#include "EnterPlz.h"


// CKunPage-Dialogfeld

#define IDC_MDNCHOICE 4001
#define IDC_KUNCHOICE 4002
#define IDC_ADR1CHOICE 4003
#define IDC_PRGRCHOICE 4004
#define IDC_PRLISTCHOICE 4005
#define IDC_PLZ1CHOICE 4006

class CKunPage : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CKunPage)

public:
	CKunPage();
	virtual ~CKunPage();

// Dialogfelddaten
	enum { IDD = IDD_KUNPAGE };

	enum
	{
		KunAdrType = 20,
		KunAdrLiefType = 21,
		KunAdrRechType = 22,
		KunAdrDivType = 25
	};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	PROG_CFG Cfg;

	virtual BOOL OnInitDialog();
	void FillAnr ();

public:

	int debitor_par;
	int max_kun_nr;
	int min_kun_nr;
	BOOL cfg_EmailPruefung;
	CDbPropertyPage *Page7;
	KUN_CLASS Kun;
	KUN_ERW_CLASS Kun_erw;
	ADR_CLASS KunAdr;
	ADR_CLASS LiefAdr;
	ADR_CLASS RechAdr;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	PTABN_CLASS Ptabn;
	IPRGRSTUFK_CLASS Iprgrstufk;
	I_KUN_PRK_CLASS I_kun_prk;
	KST_CLASS Kst;
	SYS_PAR_CLASS Sys_par;
	KUN_TOU_CLASS Kun_tou;
	FIL_CLASS Fil;
	GR_ZUORD_CLASS Gr_zuord;
	ZAHL_KOND_CLASS Zahl_kond;
	RAB_STUFEK_CLASS Rab_stufek;
	HAUS_BANK_CLASS Haus_bank;
	ZUSCHLAG_CLASS Zuschlag;
	LS_TXT_CLASS Ls_txt;
	DTA_CLASS Dta;
	
	CEnterPlz EnterPlz;
	CFormTab Form;

	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid KunGrid;
	CCtrlGrid Adr1Grid;
	CCtrlGrid TelFaxGrid;
	CCtrlGrid PLZPostfachGrid;
	CCtrlGrid VertreterGrid;
	CCtrlGrid InkassoGrid;
	CCtrlGrid UnterGruppeGrid;
	CCtrlGrid PrGrGrid;
	CCtrlGrid PrListGrid;
	CCtrlGrid Plz1Grid;
	
	CListDropTarget dropTarget;

    CPropertySheet *KunProperty;
    DbFormView *View;
	CFrameWnd *Frame;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	void Register ();
	CEdit m_MdnName;

	CStatic m_LKun;
	CNumEdit m_Kun;
	CStatic m_LKunNeu;
	CNumEdit m_KunNeu;
	CButton m_KunChoice;
	CEdit m_KunName;
	CStatic m_HeadBorder;
	CStatic m_DataBorder;
	CStatic m_LAnr;
	CComboBox m_Anr;
	CStatic m_LAdr1;
	CNumEdit m_Adr1;
	CButton m_Adr1Choice;
	
    void OnInfo ();
    static HANDLE InfoLib;
    BOOL (*_CallInfoEx) (HWND hMainWindow, RECT *rect,char *item, char *ItemValue,DWORD WinMode);
    CVector HeadControls;
	BOOL ChoiceStat;
	CString Search;

	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceAdr *ChoiceAdr;
	BOOL ModalChoiceAdr;
	CChoiceKun *Choice;
	BOOL ModalChoice;
	CCodeProperties Code;
    BOOL OnReturn ();
    BOOL OnKeyup ();
    BOOL ReadMdn ();
	BOOL Read ();
	BOOL RenameKun (long KunNeu);
	
	int HoleKunNrNeu ();
	void write ();
	void OnDelete ();
    void OnMdnchoice(); 
    void OnKunChoice();
	void OnAdr1Choice();
	void OnPlz1Choice();
	void FillPtabCombo (CWnd *Control, LPTSTR Item);
	void FillPrGr ();
	void FillPrList ();
	void OnKunSelected ();
    void OnKunCanceled ();
	BOOL TestAdr1Type ();
	BOOL OnAdrSelected ();
    void OnAdrCanceled ();
	void FillKosten ();
	void FillKunTyp();
	void OnPrGrchoice ();
	void OnKunPrchoice ();
	BOOL ReadPrGr ();
	BOOL PruefeEMail ();
	BOOL ReadKunPr ();
	void Lock();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnFilePrint();
	void OnEnterPartner();
    void EnableHeadControls (BOOL enable);
    BOOL StepBack ();
	virtual void OnChoice ();
    virtual void OnCopy ();
    virtual void OnPaste ();
	BOOL Copy ();
	BOOL Paste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
    void EnableFields (BOOL b);
	afx_msg void OnKillFocus (CWnd *);
	CStatic m_LName1;
	CTextEdit m_Name1;
public:
	CTextEdit m_Name2;
public:
	CStatic m_LName2;
public:
	CTextEdit m_Name3;
public:
	CStatic m_LName3;
public:
	CTextEdit m_Strasse;
public:
	CStatic m_LStrasse;
public:
	CNumEdit m_Plz;
	CButton m_Plz1Choice;
public:
	CStatic m_LPlz;
public:
	CTextEdit m_Ort;
public:
	CStatic m_LOrt;
public:
	CStatic m_LPostfach;
public:
	CNumEdit m_Postfach;
public:
	CNumEdit m_Telefon;
public:
	CNumEdit m_Mobil;
public:
	CStatic m_LMobil;
public:
	CStatic m_LTelefon;
public:
	CNumEdit m_Fax;
public:
	CNumEdit m_plzPostfach;
public:
	CNumEdit m_EMAIL;
public:
	CStatic m_LEmail;
public:
	CTextEdit m_Partner;
public:
	CStatic m_LPartner;
public:
	CStatic m_DataBorder2;
public:
	CComboBox m_Staat;
public:
	CStatic m_LStaat;
	
public:
	CComboBox m_Land;
	CButton m_IncludeLand;
	BOOL IncludeLand;
public:
	CStatic m_LLand;
public:
	CStatic m_LQmLaden;
public:
	CNumEdit m_QmLaden;
public:
	CComboBox m_Sprache;
public:
	CStatic m_LSprache;
public:
	CStatic m_LUnter1;
	CStatic m_LWerbung;
public:
	CStatic m_LUnter2;
public:
	CComboBox m_Unter2;
public:
	CComboBox m_Unter1;
	CComboBox m_Werbung;
public:
	CStatic m_LKunKrz1;
public:
	CTextEdit m_KunKrz1;
public:
	CButton m_LiefStop;
public:
	CStatic m_LLiefStop;
public:
	CComboBox m_KunBran;
public:
	CStatic m_LKunBran;
public:
	CStatic m_LKunTyp;
public:
	CComboBox m_KunTyp;
public:
	CButton m_FactKZ;
public:
	CStatic m_LFactKZ;
public:
	CNumEdit m_FactNR;
public:
	CStatic m_LFactNR;
public:
	CComboBox m_PreisHier;
public:
	CStatic m_LPreisHier;
public:
	CNumEdit m_PreisGruppen;
public:
	CStatic m_LPreisGruppen;
public:
	CStatic m_LPrGr;
public:
	CComboBox m_PrGr;
	CNumEdit m_PrGr1;
	CButton m_PrGrChoice;
	CEdit m_PrGrName;
public:
	CComboBox m_PrList;
	CNumEdit m_PrList1;
	CButton m_PrListChoice;
	CEdit m_PrListName;
public:
	CStatic m_LPrList;
public:
	afx_msg void OnCbnSelchangePrGr();
public:
	CComboBox m_Waehrung;
public:
	CStatic m_LWaehrung;
public:
	CNumEdit m_Vertr2;
public:
	CNumEdit m_Vertr1;
public:
	CStatic m_LVertr;
public:
	CNumEdit m_Inka2;
public:
	CNumEdit m_Inka1;
public:
	CStatic m_LInka;
public:
	CComboBox m_Kosten;
public:
	CStatic m_LKosten;
public:
	afx_msg void OnBnClickedFactkz();
public:
	CComboBox m_KunSchema;
public:
	CStatic m_LKunSchema;
	CStaticButton m_EnterPartner;
public:
	CEdit m_FactKZBez;
	bool KundeGesperrt;
	char ctext[60];
	afx_msg void OnEnChangeKunkrz1();
	CComboBox m_KunKl;
	CStatic m_LKunKl;
	CStatic m_LPartner4;
	CEdit m_Partner4;
	CComboBox m_Profil;
	CStatic m_LProfil;
};
#endif