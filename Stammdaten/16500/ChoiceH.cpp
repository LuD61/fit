#include "stdafx.h"
#include "ChoiceH.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceH::Sort1 = -1;
int CChoiceH::Sort2 = -1;
int CChoiceH::Sort3 = -1;
int CChoiceH::Sort4 = -1;
int CChoiceH::Sort5 = -1;
int CChoiceH::Sort6 = -1;

BOOL CChoiceH::OnInitDialog() 
{
	CChoiceX::OnInitDialog ();
	CRect rect;
	CRect wrect;
	GetClientRect (&rect);
	GetWindowRect (&wrect);
	ScreenToClient (&wrect);
	int scx = GetSystemMetrics (SM_CXSCREEN);
	int scy = GetSystemMetrics (SM_CYSCREEN);
	wrect.right += 400;
	rect.right += 400;
	wrect.left = (scx - rect.right) / 2;
	wrect.top = (scy - rect.bottom) / 2;
	wrect.bottom = wrect.top + rect.bottom + 5;
	MoveWindow (&wrect);
	return TRUE;
}

CChoiceH::CChoiceH(CWnd* pParent) 
        : CChoiceX(pParent)
{
}

CChoiceH::~CChoiceH() 
{
	DestroyList ();
}

void CChoiceH::DestroyList() 
{
	for (std::vector<CHList *>::iterator pabl = HList.begin (); pabl != HList.end (); ++pabl)
	{
		CHList *abl = *pabl;
		delete abl;
	}
    HList.clear ();
}

void CChoiceH::FillList () 
{
    long  hierarchie4;
    TCHAR h4_bez [37];
    TCHAR h3_bez [37];
    TCHAR h2_bez [37];
    TCHAR h1_bez [37];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Adressen"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("H4"),  1, 50, LVCFMT_RIGHT);
    SetCol (_T("Beschreibung H4"),  2, 150);
    SetCol (_T("Beschreibung H3"),  3, 150);
    SetCol (_T("Beschreibung H2"),  4, 150);
    SetCol (_T("Beschreibung H1"),  5, 150);

	if (HList.size () == 0)
	{
		DbClass->sqlout ((long *)&hierarchie4,      SQLLONG, 0);
		DbClass->sqlout ((LPTSTR)  h4_bez,   SQLCHAR, sizeof (h4_bez));
		DbClass->sqlout ((LPTSTR)  h3_bez,   SQLCHAR, sizeof (h3_bez));
		DbClass->sqlout ((LPTSTR)  h2_bez,   SQLCHAR, sizeof (h2_bez));
		DbClass->sqlout ((LPTSTR)  h1_bez,   SQLCHAR, sizeof (h1_bez));
		int cursor = DbClass->sqlcursor (_T("select hirarchie4, h4.h4_bez, h3.h3_bez, h2.h2_bez, h1.h1_bez ")
			                             _T("from h4, h3, h2, h1 where hirarchie4 >= 0 ")
                                                     _T("and h3.hirarchie3 = h4.hirarchie3 ")
                                                     _T("and h2.hirarchie2 = h4.hirarchie2 ")
                                                     _T("and h1.hirarchie1 = h4.hirarchie1"));
										 
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) h4_bez;
			CDbUniCode::DbToUniCode (h4_bez, pos);
			pos = (LPSTR) h3_bez;
			CDbUniCode::DbToUniCode (h3_bez, pos);
			pos = (LPSTR) h2_bez;
			CDbUniCode::DbToUniCode (h2_bez, pos);
			pos = (LPSTR) h1_bez;
			CDbUniCode::DbToUniCode (h1_bez, pos);

			CHList *abl = new CHList (hierarchie4, h4_bez, h3_bez, h2_bez, h1_bez);
			HList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CHList *>::iterator pabl = HList.begin (); pabl != HList.end (); ++pabl)
	{
		CHList *abl = *pabl;
		CString Hierarchie4;
		Hierarchie4.Format (_T("%ld"), abl->H4); 
		_tcscpy (h4_bez, abl->H4bez.GetBuffer ());
		_tcscpy (h3_bez, abl->H3bez.GetBuffer ());
		_tcscpy (h2_bez, abl->H2bez.GetBuffer ());
		_tcscpy (h1_bez, abl->H1bez.GetBuffer ());

		int ret = InsertItem (i, -1);
        ret = SetItemText (Hierarchie4.GetBuffer (), i, 1);
        ret = SetItemText (h4_bez, i, 2);
        ret = SetItemText (h3_bez, i, 3);
        ret = SetItemText (h2_bez, i, 4);
        ret = SetItemText (h1_bez, i, 5);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort (listView);
}


void CChoiceH::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceH::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceH::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

	for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceH::SearchBez (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceH::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
        case 2 :
        case 3 :
        case 4 :
        case 5 :
             EditText.MakeUpper ();
             SearchBez (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceH::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceH::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   else if (SortRow == 4)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort5;
   }
   else if (SortRow == 5)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort6;
   }
   return 0;
}


void CChoiceH::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
        case 3:
              Sort4 *= -1;
              break;
        case 4:
              Sort5 *= -1;
              break;
        case 5:
              Sort6 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CHList *abl = HList [i];
		   
		   abl->H4     = _tstoi (ListBox->GetItemText (i, 1));
		   abl->H4bez  = ListBox->GetItemText (i, 2);
		   abl->H3bez  = ListBox->GetItemText (i, 3);
		   abl->H2bez  = ListBox->GetItemText (i, 4);
		   abl->H1bez  = ListBox->GetItemText (i, 5);
	}
}

void CChoiceH::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = HList [idx];
}

CHList *CChoiceH::GetSelectedText ()
{
	CHList *abl = (CHList *) SelectedRow;
	return abl;
}

