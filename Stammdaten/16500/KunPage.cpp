// KunPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "KunPage.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "kunpage.h"
#include "token.h"
#include "sys_par.h"
#include "ptabn.h"
#include "iprgrstufk.h"
#include "kst.h"
#include "kun_tou.h"
#include "fil.h"
#include "gr_zuord.h"
#include "haus_bank.h"
#include "ls_txt.h"
#include "dta.h"
#include "ChoicePrGrStuf.h"
#include "ChoiceIKunPr.h"
#include "Process.h"


#define RECCHANGE                       WM_USER + 2000

// CKunPage-Dialogfeld

IMPLEMENT_DYNAMIC(CKunPage, CDbPropertyPage)
CKunPage::CKunPage()
	: CDbPropertyPage(CKunPage::IDD)
{
	hBrush = NULL;
	hBrushStatic = NULL;
	ChoiceMdn = NULL;
	Choice = NULL;
	ChoiceAdr = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceAdr = TRUE;
	ModalChoice = FALSE;
	Search = _T("");

	CUniFormField::Code = &Code;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Kun);
	IncludeLand = TRUE;
 	InfoLib = NULL;
   _CallInfoEx = NULL;
    debitor_par = 0;
	min_kun_nr = 1;
	max_kun_nr = 999999;
	cfg_EmailPruefung = TRUE;
}

CKunPage::~CKunPage()
{
	Kun.dbclose ();
	Kun_erw.dbclose ();
	KunAdr.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	Ptabn.dbclose ();
	Kst.dbclose();
	Kun_tou.dbclose();
	Ls_txt.dbclose();
	Dta.dbclose();
	Zuschlag.dbclose();
	

	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
	}
	if (Choice != NULL)
	{
		delete Choice;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
 	InfoLib = NULL;
   _CallInfoEx = NULL;
}

void CKunPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_LKUNNEU, m_LKunNeu);
	DDX_Control(pDX, IDC_KUNNEU, m_KunNeu);
	DDX_Control(pDX, IDC_KUNNAME, m_KunName);
	DDX_Control(pDX, IDC_HEADBORDER, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDER, m_DataBorder);
	DDX_Control(pDX, IDC_LANR, m_LAnr);
	DDX_Control(pDX, IDC_ANR, m_Anr);
	DDX_Control(pDX, IDC_LADR1, m_LAdr1);
	DDX_Control(pDX, IDC_ADR, m_Adr1);
	DDX_Control(pDX, IDC_LNAME1, m_LName1);
	DDX_Control(pDX, IDC_NAME, m_Name1);
	DDX_Control(pDX, IDC_NAME2, m_Name2);
	DDX_Control(pDX, IDC_LNAME2, m_LName2);
	DDX_Control(pDX, IDC_NAME3, m_Name3);
	DDX_Control(pDX, IDC_LNAME3, m_LName3);
	DDX_Control(pDX, IDC_STRASSE, m_Strasse);
	DDX_Control(pDX, IDC_LSTRASSE, m_LStrasse);
	DDX_Control(pDX, IDC_PLZ, m_Plz);
	DDX_Control(pDX, IDC_LPLZ, m_LPlz);
	DDX_Control(pDX, IDC_ORT, m_Ort);
	DDX_Control(pDX, IDC_LORT, m_LOrt);
	DDX_Control(pDX, IDC_LPOSTFACH, m_LPostfach);
	DDX_Control(pDX, IDC_POSTFACH, m_Postfach);
	DDX_Control(pDX, IDC_TELEFON, m_Telefon);
	DDX_Control(pDX, IDC_MOBIL, m_Mobil);
	DDX_Control(pDX, IDC_LTELEFON, m_LTelefon);
	DDX_Control(pDX, IDC_LMOBIL, m_LMobil);
	DDX_Control(pDX, IDC_FAX, m_Fax);
	DDX_Control(pDX, IDC_POSTFACH2, m_plzPostfach);
	DDX_Control(pDX, IDC_EMAIL, m_EMAIL);
	DDX_Control(pDX, IDC_LEMAIL, m_LEmail);
	DDX_Control(pDX, IDC_PARTNER, m_Partner);
	DDX_Control(pDX, IDC_LPARTNER, m_LPartner);
	DDX_Control(pDX, IDC_DATABORDER2, m_DataBorder2);
	DDX_Control(pDX, IDC_STAAT, m_Staat);
	DDX_Control(pDX, IDC_LSTAAT, m_LStaat);
	DDX_Control(pDX, IDC_LAND, m_Land);
	DDX_Control(pDX, IDC_INCLUDE_LAND, m_IncludeLand);
	DDX_Control(pDX, IDC_LLAND, m_LLand);
	DDX_Control(pDX, IDC_SPRACHE, m_Sprache);
	DDX_Control(pDX, IDC_LSPRACHE, m_LSprache);
	DDX_Control(pDX, IDC_LUNTER1, m_LUnter1);
	DDX_Control(pDX, IDC_UNTERGR2, m_Unter2);
	DDX_Control(pDX, IDC_UNTERGR1, m_Unter1);
	DDX_Control(pDX, IDC_LWERBUNG, m_LWerbung);
	DDX_Control(pDX, IDC_WERBUNG, m_Werbung);
	DDX_Control(pDX, IDC_LKUNKRZ1, m_LKunKrz1);
	DDX_Control(pDX, IDC_KUNKRZ1, m_KunKrz1);
	DDX_Control(pDX, IDC_KUNBRAN, m_KunBran);
	DDX_Control(pDX, IDC_LKUNBRAN, m_LKunBran);
	DDX_Control(pDX, IDC_LKUNTYP, m_LKunTyp);
	DDX_Control(pDX, IDC_KUNTYP, m_KunTyp);
	DDX_Control(pDX, IDC_FACTKZ, m_FactKZ);
	DDX_Control(pDX, IDC_LFACTKZ, m_LFactKZ);
	DDX_Control(pDX, IDC_FACTNR, m_FactNR);
	DDX_Control(pDX, IDC_LFACTNR, m_LFactNR);
	DDX_Control(pDX, IDC_PRHIER, m_PreisHier);
	DDX_Control(pDX, IDC_LPRHIER, m_LPreisHier);
	DDX_Control(pDX, IDC_LPRGR, m_LPrGr);
	DDX_Control(pDX, IDC_PRGR, m_PrGr);
	DDX_Control(pDX, IDC_PRGR1, m_PrGr1);
	DDX_Control(pDX, IDC_PRGR_NAME, m_PrGrName);
	DDX_Control(pDX, IDC_PRLIST, m_PrList);
	DDX_Control(pDX, IDC_LPRLIST, m_LPrList);
	DDX_Control(pDX, IDC_PRLIST1, m_PrList1);
	DDX_Control(pDX, IDC_PRLIST_NAME, m_PrListName);
	DDX_Control(pDX, IDC_WAEHRUNG, m_Waehrung);
	DDX_Control(pDX, IDC_LWAEHRUNG, m_LWaehrung);
	DDX_Control(pDX, IDC_VERTR2, m_Vertr2);
	DDX_Control(pDX, IDC_VERTR1, m_Vertr1);
	DDX_Control(pDX, IDC_LVERTR, m_LVertr);
	DDX_Control(pDX, IDC_INKA2, m_Inka2);
	DDX_Control(pDX, IDC_INKA1, m_Inka1);
	DDX_Control(pDX, IDC_LINKA, m_LInka);
	DDX_Control(pDX, IDC_KOSTEN, m_Kosten);
	DDX_Control(pDX, IDC_LKOSTEN, m_LKosten);
	DDX_Control(pDX, IDC_KUNSCHEMA, m_KunSchema);
	DDX_Control(pDX, IDC_LKUNSCHEMA, m_LKunSchema);
	DDX_Control(pDX, IDC_ENTER_PARTNER, m_EnterPartner);
	DDX_Control(pDX, IDC_LQMLADEN, m_LQmLaden);
	DDX_Control(pDX, IDC_QMLADEN, m_QmLaden);

	DDX_Control(pDX, IDC_KUNKL, m_KunKl);
	DDX_Control(pDX, IDC_LKUNKL, m_LKunKl);
	DDX_Control(pDX, IDC_LPARTNER4, m_LPartner4);
	DDX_Control(pDX, IDC_PARTNER4, m_Partner4);
	DDX_Control(pDX, IDC_PROFIL, m_Profil);
	DDX_Control(pDX, IDC_LPROFIL, m_LProfil);
}

BEGIN_MESSAGE_MAP(CKunPage, CPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_KILLFOCUS ()
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_BN_CLICKED(IDC_KUNCHOICE , OnKunChoice)
	ON_BN_CLICKED(IDC_ADR1CHOICE , OnAdr1Choice)
	ON_BN_CLICKED(IDC_PLZ1CHOICE , OnPlz1Choice)
	ON_BN_CLICKED(IDC_PRGRCHOICE , OnPrGrchoice)
	ON_BN_CLICKED(IDC_PRLISTCHOICE , OnKunPrchoice)
	ON_COMMAND (KUN_SELECTED, OnKunSelected)
	ON_COMMAND (KUN_CANCELED, OnKunCanceled)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_CBN_SELCHANGE(IDC_PRGR, &CKunPage::OnCbnSelchangePrGr)
	ON_BN_CLICKED(IDC_FACTKZ, &CKunPage::OnBnClickedFactkz)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(IDC_ENTER_PARTNER, OnEnterPartner)
	ON_EN_CHANGE(IDC_KUNKRZ1, &CKunPage::OnEnChangeKunkrz1)
END_MESSAGE_MAP()


// CKunPage-Meldungshandler
HANDLE CKunPage::InfoLib = NULL;


void CKunPage::Register ()
{
	dropTarget.Register (this);
	dropTarget.SetpWnd (this);
}

BOOL CKunPage::OnInitDialog()
{
	CDialog::OnInitDialog();
     InfoLib = NULL;
 	_CallInfoEx = NULL;
	m_EnterPartner.nID = IDC_ENTER_PARTNER;
	m_EnterPartner.SetWindowText (_T("Partner erfassen"));

	m_PrGr.ShowWindow (SW_HIDE);
	m_PrList.ShowWindow (SW_HIDE);
	Cfg.SetProgName (_T("16500"));
	if (Cfg.GetCfgValue (_T("UseOdbc"), PROG_CFG::cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("ModalChoice"), PROG_CFG::cfg_v) == TRUE)
    {
			ModalChoice = _tstoi (PROG_CFG::cfg_v);
    }

	minitour = 0 ;
    if (Cfg.GetCfgValue (_T("MiniTour"), PROG_CFG::cfg_v) == TRUE)
    {
			minitour = _tstoi (PROG_CFG::cfg_v);
    }

    if (Cfg.GetCfgValue (_T("min_kun_nr"), PROG_CFG::cfg_v) == TRUE)
    {
			min_kun_nr = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("max_kun_nr"), PROG_CFG::cfg_v) == TRUE)
    {
			max_kun_nr = _tstoi (PROG_CFG::cfg_v);
    }
    if (Cfg.GetCfgValue (_T("EmailPruefung"), PROG_CFG::cfg_v) == TRUE)
    {
			cfg_EmailPruefung = _tstoi (PROG_CFG::cfg_v);
    }
	Cfg.CloseCfg ();


	KunProperty = (CPropertySheet *) GetParent ();
	View = (DbFormView *) KunProperty->GetParent ();
	Frame = (CFrameWnd *) View->GetParent ();

	Kun.opendbase (_T("bws"));
    
	memcpy_s (&Mdn.mdn,sizeof (MDN), &mdn_null, sizeof (MDN));
    memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
    memcpy (&Kun.kun, &kun_null, sizeof (KUN));
    memcpy (&Kun_erw.kun_erw, &kun_erw_null, sizeof (KUN_ERW));
    memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
	memcpy (&LiefAdr.adr, &adr_null, sizeof(ADR));
	memcpy (&RechAdr.adr, &adr_null, sizeof(ADR));
	memcpy (&Zuschlag.zuschlag, &zuschlag_null, sizeof(ZUSCHLAG));
	memcpy (&Gr_zuord.gr_zuord, &gr_zuord_null, sizeof(GR_ZUORD));
	memcpy (&Kun_tou.kun_tou, &kun_tou_null, sizeof(KUN_TOU));
	memcpy (&Kst.kst, &kst_null, sizeof(KST));
	memcpy (&Ls_txt.ls_txt, &ls_txt_null, sizeof(LS_TXT));
	memcpy (&Dta.dta, &dta_null, sizeof(DTA));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}
	Register ();
	if (InfoLib == NULL) 
	{
		CString bws;
		BOOL ret = bws.GetEnvironmentVariable (_T("bws"));
		CString InfoDll;
		if (ret)
		{
			InfoDll.Format (_T("%s\\bin\\inflib.dll"), bws.GetBuffer ());
		}
		else
		{
			InfoDll = _T("inflib.dll.dll");
		}
		InfoLib = LoadLibrary (InfoDll.GetBuffer ());
		if (_CallInfoEx == NULL)
		{
			_CallInfoEx = (BOOL (*) (HWND, RECT *,char *, char *,DWORD))
					  GetProcAddress ((HMODULE) InfoLib, "_CallInfoEx");
		}
	}


	CRect bRect (0, 0, 100, 20);;

	Mdn.mdn.mdn = 1;

/* Mistzeug ------
	char test [6];
	strcpy(test,"abcdef");
	void *Vtest;
	Vtest     = (char *)   &test;

	CString Text = (LPTSTR)   Vtest;
	LPTSTR text;

	text = new TCHAR [(Text.GetLength () + 1) * 2];
	_tcscpy (text, Text.GetBuffer ());
< ---- */


	Form.Add (new CFormField (&m_Mdn,EDIT,        (short *)  &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *)   MdnAdr.adr.adr_krz, VCHAR));	// 060912
    Form.Add (new CFormField (&m_Kun,EDIT,        (long *)   &Kun.kun.kun, VLONG));
    Form.Add (new CFormField (&m_KunNeu,EDIT,        (long *)   &Kun.kun.stat_kun, VLONG));
	Form.Add (new CUniFormField (&m_KunName,EDIT, (char *)   Kun.kun.kun_krz1, VCHAR));		// 060912
	Form.Add (new CFormField (&m_Adr1,EDIT,        (long *)  &Kun.kun.adr1, VLONG));
	Form.Add (new CFormField (&m_Anr,COMBOBOX,    (short *)  &KunAdr.adr.anr, VSHORT));
	Form.Add (new CFormField (&m_Name1,EDIT,      (char *)  KunAdr.adr.adr_nam1, VCHAR));
	Form.Add (new CFormField (&m_Name2,EDIT,      (char *)  KunAdr.adr.adr_nam2, VCHAR));
	Form.Add (new CFormField (&m_Name3,EDIT,      (char *)  KunAdr.adr.adr_nam3, VCHAR));
	Form.Add (new CFormField (&m_Strasse,EDIT,      (char *)  &KunAdr.adr.str, VCHAR));
	Form.Add (new CFormField (&m_Postfach,EDIT,      (char *)  KunAdr.adr.pf, VCHAR));
	Form.Add (new CFormField (&m_plzPostfach,EDIT,      (char *)  KunAdr.adr.plz_pf, VCHAR));
	Form.Add (new CFormField (&m_Plz,EDIT,      (char *)  KunAdr.adr.plz, VCHAR));
	Form.Add (new CFormField (&m_Ort,EDIT,      (char *)  KunAdr.adr.ort1, VCHAR));
	Form.Add (new CFormField (&m_Telefon,EDIT,      (char *)  KunAdr.adr.tel, VCHAR));
	Form.Add (new CFormField (&m_Mobil,EDIT,      (char *)  KunAdr.adr.mobil, VCHAR));
	Form.Add (new CFormField (&m_Fax,EDIT,      (char *)  KunAdr.adr.fax, VCHAR));
	Form.Add (new CFormField (&m_EMAIL,EDIT,      (char *)  KunAdr.adr.email, VCHAR));
	Form.Add (new CFormField (&m_Partner,EDIT,      (char *)  KunAdr.adr.partner, VCHAR));
	Form.Add (new CFormField (&m_Partner4,EDIT,      (char *)  KunAdr.adr.spartner, VCHAR));
	Form.Add (new CFormField (&m_Staat,COMBOBOX,    (short *)  &KunAdr.adr.staat, VSHORT));
	Form.Add (new CFormField (&m_Land,COMBOBOX,    (short *)  &KunAdr.adr.land, VSHORT));
	Form.Add (new CFormField (&m_IncludeLand,CHECKBOX,    (short *)  &IncludeLand, VSHORT));
	Form.Add (new CFormField (&m_Sprache,COMBOBOX,    (short *)  &Kun.kun.sprache, VSHORT));
	Form.Add (new CFormField (&m_Unter1,COMBOBOX,        (short *)  &Kun.kun.kun_gr1 , VSHORT));
	Form.Add (new CFormField (&m_Unter2,COMBOBOX,        (short *)  &Kun.kun.kun_gr2 , VSHORT));
	Form.Add (new CFormField (&m_KunKrz1,EDIT,      (char *)  Kun.kun.kun_krz1 , VCHAR));
	Form.Add (new CFormField (&m_KunSchema,COMBOBOX,    (short *)  &Kun.kun.kun_schema , VSHORT));
	Form.Add (new CFormField (&m_KunBran,COMBOBOX,      (char *)  Kun.kun.kun_bran2 , VCHAR));	// 060912
	Form.Add (new CFormField (&m_KunTyp,COMBOBOX,       (short *)  &Kun.kun.kun_typ , VSHORT));
	Form.Add (new CFormField (&m_KunKl,COMBOBOX,       (short *)  &Kun_erw.kun_erw.kun_kl , VSHORT));
	Form.Add (new CFormField (&m_FactKZ,CHECKBOX,       (char *) Kun.kun.fak_kz, VCHAR));	// 060912
	Form.Add (new CFormField (&m_FactNR,EDIT,           (long *)   &Kun.kun.fak_nr, VLONG));
	Form.Add (new CFormField (&m_PreisHier,COMBOBOX,    (char *) Kun.kun.pr_hier, VCHAR));	// 060912

//	Form.Add (new CFormField (&m_PrGr,COMBOBOX,         (short *)  &Kun.kun.pr_stu , VSHORT));

	Form.Add (new CFormField (&m_PrGr1,EDIT,            (short *)  &Kun.kun.pr_stu , VSHORT));
	Form.Add (new CFormField (&m_PrGrName,EDIT,         (char *)  Iprgrstufk.iprgrstufk.zus_bz , VCHAR));	// 060912
	Form.Add (new CFormField (&m_PrList,COMBOBOX,       (long *)  &Kun.kun.pr_lst , VLONG));
	Form.Add (new CFormField (&m_PrList1,EDIT,          (long *)  &Kun.kun.pr_lst , VLONG));
	Form.Add (new CFormField (&m_PrListName,EDIT,       (char *)  I_kun_prk.i_kun_prk.zus_bz , VCHAR));	// 060912
	Form.Add (new CFormField (&m_Waehrung,COMBOBOX,     (short *)  &Kun.kun.waehrung , VSHORT));
	Form.Add (new CFormField (&m_Werbung,COMBOBOX,     (short *)  &Kun_erw.kun_erw.werbung , VSHORT)); //WAL-103
	Form.Add (new CFormField (&m_Vertr1,EDIT,        (long *)  &Kun.kun.vertr1 , VLONG));
	Form.Add (new CFormField (&m_Vertr2,EDIT,        (long *)  &Kun.kun.vertr2 , VLONG));
	Form.Add (new CFormField (&m_Inka1,EDIT,        (long *)  &Kun.kun.inka_nr , VLONG));
	Form.Add (new CFormField (&m_Inka2,EDIT,        (long *)  &Kun.kun.inka_nr2 , VLONG));
	Form.Add (new CFormField (&m_Kosten,COMBOBOX,        (long *)  &Kun.kun.kst , VLONG)); 
	Form.Add (new CFormField (&m_QmLaden,EDIT,        (long *)  &Kun_erw.kun_erw.m2laden , VLONG));
	Form.Add (new CFormField (&m_Profil,COMBOBOX,     (short *)  &Kun_erw.kun_erw.best_profil , VSHORT)); //WAL-103

	FillPtabCombo (&m_Anr, _T("anr"));
	FillPtabCombo (&m_Staat, _T("staat"));
	FillPtabCombo (&m_Land, _T("land"));
	FillPtabCombo (&m_Sprache, _T("sprache"));
	FillPtabCombo (&m_KunBran, _T("kun_bran2"));
	FillPtabCombo (&m_KunSchema, _T("kun_schema"));
	FillPtabCombo (&m_KunKl, _T("kun_kl"));
	FillPtabCombo (&m_Profil, _T("best_profil"));

	strcpy (sys_par.sys_par_nam, "debitor_par");
	if (Sys_par.dbreadfirst () == 0)
	{
		if ((sys_par.sys_par_wrt[0] == '1'))
		{
			debitor_par = 1;
		}
	}

	//F�llen des Kundentyps in Abh�ngigkeit von zahl_ziel_par
	strcpy (sys_par.sys_par_nam, "vbd_par");
	if (Sys_par.dbreadfirst () == 0)
	{
		if (!(sys_par.sys_par_wrt[0] == '1'))
		{
			FillKunTyp();
			
		}
		else
		{
			FillPtabCombo (&m_KunTyp, _T("kun_typ"));
		}

	}
	else
	{
			FillPtabCombo (&m_KunTyp, _T("kun_typ"));
	}

	
	FillPtabCombo (&m_PreisHier, _T("pr_hier"));
	FillPtabCombo (&m_Waehrung, _T("waehrung"));
	FillPtabCombo (&m_Werbung, _T("werbung")); 
	FillPtabCombo (&m_Unter1, _T("kun_gr"));
	FillPtabCombo (&m_Unter2, _T("kun_gr2"));
	FillPrGr();
	FillPrList();
	FillKosten();
	

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 1, 3);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1);
	c_MdnName->SetCellPos (5, 0);
	MdnGrid.Add (c_MdnName);

	KunGrid.Create (this, 5, 5);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	CtrlGrid.CreateChoiceButton (m_KunChoice, IDC_KUNCHOICE, this);
	CCtrlInfo *c_KunChoice = new CCtrlInfo (&m_KunChoice, 1, 0, 1, 1);
	KunGrid.Add (c_KunChoice);
	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 2, 0, 1, 1);
	c_KunName->SetCellPos (5, 0);
	KunGrid.Add (c_KunName);

	Adr1Grid.Create (this, 1, 3);
    Adr1Grid.SetBorder (0, 0);
    Adr1Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_Adr1 = new CCtrlInfo (&m_Adr1, 0, 0, 1, 1);
	Adr1Grid.Add (c_Adr1);
	CtrlGrid.CreateChoiceButton (m_Adr1Choice, IDC_ADR1CHOICE, this);
	CCtrlInfo *c_Adr1Choice = new CCtrlInfo (&m_Adr1Choice, 1, 0, 1, 1);
	Adr1Grid.Add (c_Adr1Choice);
	CCtrlInfo *c_EnterPartner = new CCtrlInfo (&m_EnterPartner, 2, 0, 1, 1);
	c_EnterPartner->SetCellPos (90, -5);
	Adr1Grid.Add (c_EnterPartner);

	Plz1Grid.Create (this, 1, 3);
    Plz1Grid.SetBorder (0, 0);
    Plz1Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_Plz = new CCtrlInfo (&m_Plz, 0, 0, 1, 1);
	Plz1Grid.Add (c_Plz);
	CtrlGrid.CreateChoiceButton (m_Plz1Choice, IDC_PLZ1CHOICE, this);
	CCtrlInfo *c_Plz1Choice = new CCtrlInfo (&m_Plz1Choice, 1, 0, 1, 1);
	Plz1Grid.Add (c_Plz1Choice);
	CCtrlInfo *c_IncludeLand = new CCtrlInfo (&m_IncludeLand, 2, 0, 1, 1);
	c_IncludeLand->SetCellPos (5, 0);
	Plz1Grid.Add (c_IncludeLand);

	TelFaxGrid.Create (this, 1, 2);
    TelFaxGrid.SetBorder (0, 0);
    TelFaxGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Telefon = new CCtrlInfo (&m_Telefon, 0, 0, 1, 1);
	TelFaxGrid.Add (c_Telefon);
	CCtrlInfo *c_Fax = new CCtrlInfo (&m_Fax, 1, 0, 1, 1);
	TelFaxGrid.Add (c_Fax);

	PLZPostfachGrid.Create (this, 1, 2);
    PLZPostfachGrid.SetBorder (0, 0);
    PLZPostfachGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Postfach = new CCtrlInfo (&m_Postfach, 0, 0, 1, 1);
	PLZPostfachGrid.Add (c_Postfach);
	CCtrlInfo *c_plzPostfach = new CCtrlInfo (&m_plzPostfach, 1, 0, 1, 1);
	PLZPostfachGrid.Add (c_plzPostfach);

	VertreterGrid.Create (this, 1, 2);
    VertreterGrid.SetBorder (0, 0);
    VertreterGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Vertr1 = new CCtrlInfo (&m_Vertr1, 0, 0, 1, 1);
	VertreterGrid.Add (c_Vertr1);
	CCtrlInfo *c_Vertr2 = new CCtrlInfo (&m_Vertr2, 1, 0, 1, 1);
	VertreterGrid.Add (c_Vertr2);

	InkassoGrid.Create (this, 1, 2);
    InkassoGrid.SetBorder (0, 0);
    InkassoGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Inka1 = new CCtrlInfo (&m_Inka1, 0, 0, 1, 1);
	InkassoGrid.Add (c_Inka1);
	CCtrlInfo *c_Inka2 = new CCtrlInfo (&m_Inka2, 1, 0, 1, 1);
	InkassoGrid.Add (c_Inka2);

	UnterGruppeGrid.Create (this, 1, 2);
    UnterGruppeGrid.SetBorder (0, 0);
    UnterGruppeGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Unter1 = new CCtrlInfo (&m_Unter1, 0, 0, 1, 1);
	UnterGruppeGrid.Add (c_Unter1);
	CCtrlInfo *c_Unter2 = new CCtrlInfo (&m_Unter2, 1, 0, 1, 1);
	UnterGruppeGrid.Add (c_Unter2);

	PrGrGrid.Create (this, 1, 3);
    PrGrGrid.SetBorder (0, 0);
    PrGrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_PrGr1 = new CCtrlInfo (&m_PrGr1, 0, 0, 1, 1);
	PrGrGrid.Add (c_PrGr1);
	CtrlGrid.CreateChoiceButton (m_PrGrChoice, IDC_PRGRCHOICE, this);
	CCtrlInfo *c_PrGrChoice = new CCtrlInfo (&m_PrGrChoice, 1, 0, 1, 1);
	PrGrGrid.Add (c_PrGrChoice);
	CCtrlInfo *c_PrGrName = new CCtrlInfo (&m_PrGrName, 2, 0, 1, 1);
	c_PrGrName->SetCellPos (5, 0);
	PrGrGrid.Add (c_PrGrName);

	
	PrListGrid.Create (this, 1, 3);
    PrListGrid.SetBorder (0, 0);
    PrListGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_PrList1 = new CCtrlInfo (&m_PrList1, 0, 0, 1, 1);
	PrListGrid.Add (c_PrList1);
	CtrlGrid.CreateChoiceButton (m_PrListChoice, IDC_PRLISTCHOICE, this);
	CCtrlInfo *c_PrListChoice = new CCtrlInfo (&m_PrListChoice, 1, 0, 1, 1);
	PrListGrid.Add (c_PrListChoice);
	CCtrlInfo *c_PrListName = new CCtrlInfo (&m_PrListName, 2, 0, 1, 1);
	c_PrListName->SetCellPos (5, 0);
	PrListGrid.Add (c_PrListName);


	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 4);
    c_HeadBorder->rightspace = 20; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);
//	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 3, 1, 4, 1);
//	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LKun = new CCtrlInfo (&m_LKun, 1, 2, 1, 1);
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid = new CCtrlInfo (&KunGrid, 2, 2, 1, 1);
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_LKunNeu = new CCtrlInfo (&m_LKunNeu, 4, 2, 1, 1);
	CtrlGrid.Add (c_LKunNeu);
	CCtrlInfo *c_KunNeu = new CCtrlInfo (&m_KunNeu, 5, 2, 1, 1);
	CtrlGrid.Add (c_KunNeu);
//	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 3, 2, 4, 1);
//	CtrlGrid.Add (c_KunName);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 4, 3, DOCKBOTTOM);
    CtrlGrid.Add (c_DataBorder);

	CCtrlInfo *c_DataBorder2 = new CCtrlInfo (&m_DataBorder2, 3, 4, DOCKRIGHT, DOCKBOTTOM);
    c_DataBorder2->rightspace = 20; 
	CtrlGrid.Add (c_DataBorder2);

	CCtrlInfo *c_LAdr1 = new CCtrlInfo (&m_LAdr1, 1, 5, 1, 1);
	CtrlGrid.Add (c_LAdr1);
	CCtrlInfo *c_Adr1Grid = new CCtrlInfo (&Adr1Grid, 2, 5, 1, 1);
	CtrlGrid.Add (c_Adr1Grid);
	CCtrlInfo *c_LAnr = new CCtrlInfo (&m_LAnr, 1, 6, 1, 1);
	CtrlGrid.Add (c_LAnr);
	CCtrlInfo *c_Anr = new CCtrlInfo (&m_Anr, 2, 6, 1, 1);
	CtrlGrid.Add (c_Anr);
	CCtrlInfo *c_LName1 = new CCtrlInfo (&m_LName1, 1, 7, 1, 1);
	CtrlGrid.Add (c_LName1);
	CCtrlInfo *c_Name1 = new CCtrlInfo (&m_Name1, 2, 7, 1, 1);
	CtrlGrid.Add (c_Name1);

	CCtrlInfo *c_LName2 = new CCtrlInfo (&m_LName2, 1, 8, 1, 1);
	CtrlGrid.Add (c_LName2);
	CCtrlInfo *c_Name2 = new CCtrlInfo (&m_Name2, 2, 8, 1, 1);
	CtrlGrid.Add (c_Name2);

	CCtrlInfo *c_LName3 = new CCtrlInfo (&m_LName3, 1, 9, 1, 1);
	CtrlGrid.Add (c_LName3);
	CCtrlInfo *c_Name3 = new CCtrlInfo (&m_Name3, 2, 9, 1, 1);
	CtrlGrid.Add (c_Name3);

	CCtrlInfo *c_LStrasse = new CCtrlInfo (&m_LStrasse, 1, 10, 1, 1);
	CtrlGrid.Add (c_LStrasse);
	CCtrlInfo *c_Strasse = new CCtrlInfo (&m_Strasse, 2, 10, 1, 1);
	CtrlGrid.Add (c_Strasse);

	CCtrlInfo *c_LPlz = new CCtrlInfo (&m_LPlz, 1, 12, 1, 1);
	CtrlGrid.Add (c_LPlz);
//	CCtrlInfo *c_Plz = new CCtrlInfo (&m_Plz, 2, 12, 1, 1);
//	CtrlGrid.Add (c_Plz);
	CCtrlInfo *c_Plz1Grid = new CCtrlInfo (&Plz1Grid, 2, 12, 1, 1);
	CtrlGrid.Add (c_Plz1Grid);
	
	CCtrlInfo *c_LPostfach = new CCtrlInfo (&m_LPostfach, 1, 11, 1, 1);
	CtrlGrid.Add (c_LPostfach);
	CCtrlInfo *c_PLZPostfachGrid = new CCtrlInfo (&PLZPostfachGrid, 2, 11, 1, 1);
	CtrlGrid.Add (c_PLZPostfachGrid);

	CCtrlInfo *c_LOrt = new CCtrlInfo (&m_LOrt, 1, 13, 1, 1);
	CtrlGrid.Add (c_LOrt);
	CCtrlInfo *c_Ort = new CCtrlInfo (&m_Ort, 2, 13, 1, 1);
	CtrlGrid.Add (c_Ort);

	CCtrlInfo *c_LTelefon = new CCtrlInfo (&m_LTelefon, 1, 15, 1, 1);
	CtrlGrid.Add (c_LTelefon);
	CCtrlInfo *c_TelFaxGrid = new CCtrlInfo (&TelFaxGrid, 2, 15, 1, 1);
	CtrlGrid.Add (c_TelFaxGrid);
	
	CCtrlInfo *c_LMobil = new CCtrlInfo (&m_LMobil, 1, 16, 1, 1);
	CtrlGrid.Add (c_LMobil);
	CCtrlInfo *c_Mobil = new CCtrlInfo (&m_Mobil, 2, 16, 1, 1);
	CtrlGrid.Add (c_Mobil);


	CCtrlInfo *c_LEmail = new CCtrlInfo (&m_LEmail, 1, 17, 1, 1);
	CtrlGrid.Add (c_LEmail);
	CCtrlInfo *c_EMAIL = new CCtrlInfo (&m_EMAIL, 2, 17, 1, 1);
	CtrlGrid.Add (c_EMAIL);

	CCtrlInfo *c_LPartner = new CCtrlInfo (&m_LPartner, 1, 18, 1, 1);
	CtrlGrid.Add (c_LPartner);
	CCtrlInfo *c_Partner = new CCtrlInfo (&m_Partner, 2, 18, 1, 1);
	CtrlGrid.Add (c_Partner);

	CCtrlInfo *c_LPartner4 = new CCtrlInfo (&m_LPartner4, 1, 19, 1, 1);
	CtrlGrid.Add (c_LPartner4);
	CCtrlInfo *c_Partner4 = new CCtrlInfo (&m_Partner4, 2, 19, 1, 1);
	CtrlGrid.Add (c_Partner4);

	CCtrlInfo *c_LStaat = new CCtrlInfo (&m_LStaat, 1, 20, 1, 1);
	CtrlGrid.Add (c_LStaat);
	CCtrlInfo *c_Staat = new CCtrlInfo (&m_Staat, 2, 20, 1, 1);
	CtrlGrid.Add (c_Staat);

	CCtrlInfo *c_LLand = new CCtrlInfo (&m_LLand, 1, 21, 1, 1);
	CtrlGrid.Add (c_LLand);
	CCtrlInfo *c_Land = new CCtrlInfo (&m_Land, 2, 21, 1, 1);
	CtrlGrid.Add (c_Land);

	CCtrlInfo *c_LQmLaden = new CCtrlInfo (&m_LQmLaden, 1, 23, 1, 1);
	CtrlGrid.Add (c_LQmLaden);
	CCtrlInfo *c_QmLaden = new CCtrlInfo (&m_QmLaden, 2, 23, 1, 1);
	CtrlGrid.Add (c_QmLaden);

	CCtrlInfo *c_LKunKrz1 = new CCtrlInfo (&m_LKunKrz1, 4, 5, 1, 1);
	CtrlGrid.Add (c_LKunKrz1);
	CCtrlInfo *c_KunKrz1 = new CCtrlInfo (&m_KunKrz1, 5, 5, 1, 1);
	c_KunKrz1->rightspace = 40;
	CtrlGrid.Add (c_KunKrz1);


	CCtrlInfo *c_LKosten = new CCtrlInfo (&m_LKosten, 4, 6, 1, 1);
	CtrlGrid.Add (c_LKosten);
	CCtrlInfo *c_Kosten = new CCtrlInfo (&m_Kosten, 5, 6, 1, 1);
	c_Kosten->rightspace = 40;
	CtrlGrid.Add (c_Kosten);


	CCtrlInfo *c_LSprache = new CCtrlInfo (&m_LSprache, 4, 7, 1, 1);
	CtrlGrid.Add (c_LSprache);
	CCtrlInfo *c_Sprache = new CCtrlInfo (&m_Sprache, 5, 7, 1, 1);
	c_Sprache->rightspace = 40;
	CtrlGrid.Add (c_Sprache);


	CCtrlInfo *c_LKunSchema = new CCtrlInfo (&m_LKunSchema, 4, 9, 1, 1);
	CtrlGrid.Add (c_LKunSchema);
	CCtrlInfo *c_KunSchema = new CCtrlInfo (&m_KunSchema, 5, 9, 1, 1);
	c_KunSchema->rightspace = 40;
	CtrlGrid.Add (c_KunSchema);

	CCtrlInfo *c_LKunBran = new CCtrlInfo (&m_LKunBran, 4, 10, 1, 1);
	CtrlGrid.Add (c_LKunBran);
	CCtrlInfo *c_KunBran = new CCtrlInfo (&m_KunBran, 5, 10, 1, 1);
	c_KunBran->rightspace = 40;
	CtrlGrid.Add (c_KunBran);

	CCtrlInfo *c_LKunTyp = new CCtrlInfo (&m_LKunTyp, 4, 11, 1, 1);
	CtrlGrid.Add (c_LKunTyp);
	CCtrlInfo *c_KunTyp = new CCtrlInfo (&m_KunTyp, 5, 11, 1, 1);
	c_KunTyp->rightspace = 40;
	CtrlGrid.Add (c_KunTyp);

	CCtrlInfo *c_LKunKl = new CCtrlInfo (&m_LKunKl, 4, 12, 1, 1);
	CtrlGrid.Add (c_LKunKl);
	CCtrlInfo *c_KunKl = new CCtrlInfo (&m_KunKl, 5, 12, 1, 1);
	c_KunKl->rightspace = 40;
	CtrlGrid.Add (c_KunKl);

	CCtrlInfo *c_LFactKZ = new CCtrlInfo (&m_LFactKZ, 4, 13, 1, 1);
	CtrlGrid.Add (c_LFactKZ);
	CCtrlInfo *c_FactKZ = new CCtrlInfo (&m_FactKZ, 5, 13, 1, 1);
	CtrlGrid.Add (c_FactKZ);

	CCtrlInfo *c_LFactNR = new CCtrlInfo (&m_LFactNR, 4, 14, 1, 1);
	CtrlGrid.Add (c_LFactNR);
	CCtrlInfo *c_FactNR = new CCtrlInfo (&m_FactNR, 5, 14, 1, 1);
	c_FactNR->rightspace = 40;
	CtrlGrid.Add (c_FactNR);

	CCtrlInfo *c_LPreisHier = new CCtrlInfo (&m_LPreisHier, 4, 15, 1, 1);
	CtrlGrid.Add (c_LPreisHier);
	CCtrlInfo *c_PreisHier = new CCtrlInfo (&m_PreisHier, 5, 15, 1, 1);
	c_PreisHier->rightspace = 40;
	CtrlGrid.Add (c_PreisHier);

	CCtrlInfo *c_LPrGr = new CCtrlInfo (&m_LPrGr, 4, 16, 1, 1);
	CtrlGrid.Add (c_LPrGr);
	CCtrlInfo *c_PrGr = new CCtrlInfo (&m_PrGr, 5, 16, 1, 1);
	c_PrGr->rightspace = 40;
	CtrlGrid.Add (c_PrGr);
	CCtrlInfo *c_PrGrGrid = new CCtrlInfo (&PrGrGrid, 5, 16, 1, 1);
	c_PrGrGrid->rightspace = 40;
	CtrlGrid.Add (c_PrGrGrid);

	CCtrlInfo *c_LPrList = new CCtrlInfo (&m_LPrList, 4, 17, 1, 1);
	CtrlGrid.Add (c_LPrList);
	CCtrlInfo *c_PrList = new CCtrlInfo (&m_PrList, 5, 17, 1, 1);
	c_PrList->rightspace = 40;
	CtrlGrid.Add (c_PrList);
	CCtrlInfo *c_PrListGrid = new CCtrlInfo (&PrListGrid, 5, 17, 1, 1);
	c_PrListGrid->rightspace = 40;
	CtrlGrid.Add (c_PrListGrid);

	CCtrlInfo *c_LWaehrung = new CCtrlInfo (&m_LWaehrung, 4, 18, 1, 1);
	CtrlGrid.Add (c_LWaehrung);
	CCtrlInfo *c_Waehrung = new CCtrlInfo (&m_Waehrung, 5, 18, 1, 1);
	c_Waehrung->rightspace = 40;
	CtrlGrid.Add (c_Waehrung);

	CCtrlInfo *c_LInka = new CCtrlInfo (&m_LInka, 4, 20, 1, 1);
	CtrlGrid.Add (c_LInka);
	CCtrlInfo *c_InkassoGrid = new CCtrlInfo (&InkassoGrid, 5, 20, 1, 1);
	CtrlGrid.Add (c_InkassoGrid);
	
	CCtrlInfo *c_LVertr = new CCtrlInfo (&m_LVertr, 4, 21, 1, 1);
	CtrlGrid.Add (c_LVertr);
	CCtrlInfo *c_VertreterGrid = new CCtrlInfo (&VertreterGrid, 5, 21, 1, 1);
	CtrlGrid.Add (c_VertreterGrid);

	CCtrlInfo *c_LUnter1 = new CCtrlInfo (&m_LUnter1, 4, 22, 1, 1);
	CtrlGrid.Add (c_LUnter1);
	CCtrlInfo *c_UnterGruppeGrid = new CCtrlInfo (&UnterGruppeGrid, 5, 22, 1, 1);
	CtrlGrid.Add (c_UnterGruppeGrid);

	CCtrlInfo *c_LWerbung = new CCtrlInfo (&m_LWerbung, 4, 23, 1, 1);
	CtrlGrid.Add (c_LWerbung);
	CCtrlInfo *c_Werbung = new CCtrlInfo (&m_Werbung, 5, 23, 1, 1);
	c_Werbung->rightspace = 40;
	CtrlGrid.Add (c_Werbung);

	CCtrlInfo *c_LProfil = new CCtrlInfo (&m_LProfil, 4, 24, 1, 1);
	CtrlGrid.Add (c_LProfil);
	CCtrlInfo *c_Profil = new CCtrlInfo (&m_Profil, 5, 24, 1, 1);
	c_Profil->rightspace = 40;
	CtrlGrid.Add (c_Profil);

	memcpy (&Kun_tou.kun_tou, &kun_tou_null, sizeof (KUN_TOU));
	memcpy (&Gr_zuord.gr_zuord, &gr_zuord_null, sizeof (GR_ZUORD));
	memcpy (&Zuschlag.zuschlag, &zuschlag_null, sizeof (ZUSCHLAG));
    memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
    memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk_null, sizeof (I_KUN_PRK));

	
	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	CtrlGrid.Display ();
    CtrlGrid.SetItemCharHeight (&m_HeadBorder, 3);
    CtrlGrid.SetItemCharWidth (&m_KunChoice, 15);
    EnableFields (FALSE);
	m_LKunNeu.ShowWindow (SW_HIDE);
	m_KunNeu.ShowWindow (SW_HIDE);

    Form.Show ();

	

	return TRUE;
}


BOOL CKunPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				break;
			}
			else if (pMsg->wParam == VK_F4)
			{
				OnInfo ();
			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
//				Form.Get();
				write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnKunChoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				else if (GetFocus () == &m_Kun)
				{
					OnKunChoice ();
					return TRUE;
				}
				else if (GetFocus () == &m_Adr1)
				{
					OnAdr1Choice();
					return TRUE;
				}
				else if (GetFocus () == &m_Plz)
				{
					OnPlz1Choice();
					return TRUE;
				}
				else if (GetFocus () == &m_PrGr1)
				{
					OnPrGrchoice();
					return TRUE;
				}
				else if (GetFocus () == &m_PrList1)
				{
					OnKunPrchoice();
					return TRUE;
				}
				break;
			}

			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}
			if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

HBRUSH CKunPage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);

    if (hBrush == NULL)
	{
		  hBrush = CreateSolidBrush (Color);
		  hBrushStatic = CreateSolidBrush (Color);
		  m_EnterPartner.SetBkColor (DlgBkColor);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			return hBrush;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
			return hBrushStatic;
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CButton )))
	{
            pDC->SetBkColor (Color);
			pDC->SetBkMode (TRANSPARENT);
			return hBrushStatic;
	}
	return CPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CKunPage::OnReturn ()
{
	long saveKun = 0;
	long KunNeu = 0;
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_Kun)
	{
		if (!Read ())
		{
			m_Kun.SetFocus ();
			return FALSE;
		}
		if (Kun.kun.kun < -1)
		{
			m_LKunNeu.ShowWindow (SW_SHOWNORMAL);
			m_KunNeu.ShowWindow (SW_SHOWNORMAL);
			m_KunNeu.SetReadOnly (FALSE);
			m_KunNeu.SetFocus ();
			saveKun = Kun.kun.kun;
	        KunNeu = HoleKunNrNeu();
			Kun.kun.kun = saveKun;
			Kun.dbreadfirst ();
			Kun.kun.stat_kun = KunNeu;
			Form.Show();

			return FALSE;
		}
	}
	if (Control == &m_KunNeu)
	{
		Form.Get ();
		if (!RenameKun (Kun.kun.stat_kun))
		{
			m_KunNeu.SetFocus ();
			return FALSE;
		}
		m_KunNeu.SetReadOnly (TRUE);
		Form.Show();
	}

	if (Control == &m_Adr1)
	{
		m_LKunNeu.ShowWindow (SW_HIDE);
		m_KunNeu.ShowWindow (SW_HIDE);
		Form.Show();
		if (OnAdrSelected())
		{
			m_Anr.SetFocus ();
		}
		else
		{
			m_Adr1.SetFocus ();
		}
		return FALSE;
		
	}

	if (Control == &m_Plz)
	{
		Form.GetFormField (&m_Plz)->Get ();
		Form.GetFormField (&m_Telefon)->Get ();
		PLZ_CLASS Plz = EnterPlz.GetPlzFromPlz (KunAdr.adr.plz);
		if (EnterPlz.Found ())
		{
			_tcscpy (KunAdr.adr.ort1, Plz.plz.ort);
			Form.GetFormField (&m_Ort)->Show ();
			if (_tcscmp (KunAdr.adr.tel, _T("0")) < 0)
			{
				_tcscpy (KunAdr.adr.tel, Plz.plz.vorwahl);
				Form.GetFormField (&m_Telefon)->Show ();
			}
			CFormField *fLand = Form.GetFormField (&m_Land);
			int idx = EnterPlz.GetComboIdx (Plz.plz.bundesland, fLand->ComboValues);
			if (idx != -1)
			{
				m_Land.SetCurSel (idx);
				fLand->Get ();
			}
		}
	}

	if (Control == &m_Ort)
	{
		Form.GetFormField (&m_Ort)->Get ();
		Form.GetFormField (&m_Telefon)->Get ();
		PLZ_CLASS Plz = EnterPlz.GetPlzFromOrt (KunAdr.adr.ort1);
		if (EnterPlz.Found ())
		{
			_tcscpy (KunAdr.adr.plz, Plz.plz.plz);
			Form.GetFormField (&m_Plz)->Show ();
			_tcscpy (KunAdr.adr.ort1, Plz.plz.ort);
			Form.GetFormField (&m_Ort)->Show ();
			if (_tcscmp (KunAdr.adr.tel, _T("0")) < 0)
			{
				_tcscpy (KunAdr.adr.tel, Plz.plz.vorwahl);
				Form.GetFormField (&m_Telefon)->Show ();
			}
			CFormField *fLand = Form.GetFormField (&m_Land);
			int idx = EnterPlz.GetComboIdx (Plz.plz.bundesland, fLand->ComboValues);
			if (idx != -1)
			{
				m_Land.SetCurSel (idx);
				fLand->Get ();
			}
		}
	}

	if (Control == &m_PrGr1)
	{
		
		if (!ReadPrGr ())
		{
			m_PrGr1.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_PrList1)
	{
		
		if (!ReadKunPr ())
		{
			m_PrList1.SetFocus ();
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CKunPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

BOOL CKunPage::ReadMdn ()
{
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
    short smdn = Mdn.mdn.mdn;
	Form.Get ();
	
	if (Mdn.mdn.mdn == 0)
	{
		return TRUE;
	}
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		if (smdn != Mdn.mdn.mdn && Choice != NULL)
		{
			delete Choice;
			Choice = NULL;
		}
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Mdn.mdn.mdn = smdn;
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}



BOOL CKunPage::RenameKun (long KunNeu)
{
	long saveKun = 0;
	long saveAdr = 0;
	int dsqlstatus = 0;
	saveKun = Kun.kun.kun;
	saveAdr = KunAdr.adr.adr;
	Kun.kun.kun = KunNeu;
	KunAdr.adr.adr = KunNeu;
	if (Kun.dbreadfirst() == 0)
	{
		Kun.kun.kun = saveKun;
		Kun.dbreadfirst ();
		MessageBox (_T("Diese Kundennummer ist schon vergeben ! "), NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}
	if (KunAdr.dbreadfirst() == 0)
	{
		KunAdr.adr.adr = saveAdr;
		KunAdr.dbreadfirst ();
		MessageBox (_T("Diese Adressnummer ist schon vergeben ! "), NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}

	Kun.sqlin ((long *) &Kun.kun.kun , SQLLONG, 0);
	Kun.sqlin ((long *) &Kun.kun.kun , SQLLONG, 0);
	Kun.sqlin ((long *) &Kun.kun.kun , SQLLONG, 0);
	Kun.sqlin ((long *) &Kun.kun.kun , SQLLONG, 0);
	Kun.sqlin ((short *) &Kun.kun.mdn , SQLSHORT, 0);
	Kun.sqlin ((short *) &Kun.kun.fil , SQLSHORT, 0);
	Kun.sqlin ((long *) &saveKun , SQLLONG, 0);
	Kun.sqlcomm (_T("update kun set kun = ?, adr1 = ?, adr2 = ?,adr3 = ? ")
			        _T("where mdn = ? and fil = ? and kun = ? "));
	dsqlstatus = Kun.dbreadfirst ();
	if (dsqlstatus != 0)
	{
		Kun.kun.kun = saveKun;
		Kun.dbreadfirst ();
		MessageBox (_T("�nderung der Kundennummer ist z.Zeit nicht m�glich ! "), NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}
	KunAdr.sqlin ((long *) &KunAdr.adr.adr , SQLLONG, 0);
	KunAdr.sqlin ((long *) &saveAdr , SQLLONG, 0);
	KunAdr.sqlcomm (_T("update adr set adr = ? ")
			        _T("where adr = ? "));
	dsqlstatus = KunAdr.dbreadfirst ();
	if (dsqlstatus != 0)
	{
		KunAdr.adr.adr = saveAdr;
		KunAdr.dbreadfirst ();
		MessageBox (_T("�nderung der Adressnummer ist z.Zeit nicht m�glich ! "), NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}
	Kun.kun.stat_kun = Kun.kun.kun;
	return TRUE;
}


BOOL CKunPage::Read ()
{
	CString kz;
	BOOL NewClient = FALSE;
	extern short sql_mode;
	

	if (ModalChoice)
	{
		CString cKun;
		m_Kun.GetWindowText (cKun);
		if (!CStrFuncs::IsDecimal (cKun))
		{
			Search = cKun;
			OnKunChoice ();
			Search = "";
			if (!ChoiceStat)
			{
				m_Kun.SetFocus ();
				m_Kun.SetSel (0, -1);
				return FALSE;
			}
		}
	}

	memcpy (&Kun.kun, &kun_null, sizeof (KUN));
	memcpy (&Kun_erw.kun_erw, &kun_erw_null, sizeof (KUN_ERW));
	Form.Get ();
	Kun.kun.mdn = Mdn.mdn.mdn;
	Kun_erw.kun_erw.mdn = Mdn.mdn.mdn;
	if (Kun.kun.kun == 0)
	{
	    Kun.kun.kun = HoleKunNrNeu();
		if (Kun.kun.kun == 0)
		{
			m_Kun.SetFocus ();
			m_Kun.SetSel (0, -1);
			return FALSE;
		}
	}

	if (Kun.dbreadfirst () == 100)
	{
		if (Kun.kun.kun < -1)
		{
			m_Kun.SetFocus ();
			m_Kun.SetSel (0, -1);
            return FALSE;
		}

		    NewClient = TRUE;
			memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
			memcpy (&LiefAdr.adr, &adr_null, sizeof(ADR));
			memcpy (&RechAdr.adr, &adr_null, sizeof(ADR));
			memcpy (&Zuschlag.zuschlag, &zuschlag_null, sizeof(ZUSCHLAG));
			memcpy (&Gr_zuord.gr_zuord, &gr_zuord_null, sizeof(GR_ZUORD));
			memcpy (&Kun_tou.kun_tou, &kun_tou_null, sizeof(KUN_TOU));
			memcpy (&Kst.kst, &kst_null, sizeof(KST));
			memcpy (&Ls_txt.ls_txt, &ls_txt_null, sizeof(LS_TXT));
			memcpy (&Dta.dta, &dta_null, sizeof(DTA));
			

// Grundwerte holen Lud 10.01.2011			
			int saveKun = Kun.kun.kun;
			Kun.kun.kun = -1;
			Kun.dbreadfirst();
			sprintf (Kun.kun.kun_krz1,"NEUANLAGE");
			Kun_erw.kun_erw.mdn = Kun.kun.mdn;
			Kun_erw.kun_erw.fil = Kun.kun.fil;
			Kun_erw.kun_erw.kun = -1;
			Kun_erw.dbreadfirst();
			Kun.kun.kun = saveKun;
			Kun.kun.stat_kun = Kun.kun.kun; //07112
			if (debitor_par == 1)
			{
				sprintf(Kun.kun.deb_kto,"%ld", Kun.kun.kun); //07112
			}


			KunAdr.adr.adr = -1;
			KunAdr.dbreadfirst();
			LiefAdr.adr.adr = -1;
			LiefAdr.dbreadfirst();
			RechAdr.adr.adr = -1;
			RechAdr.dbreadfirst();


			Kun.kun.adr1 = Kun.kun.kun;
			Kun.kun.adr2 = Kun.kun.kun;
			Kun.kun.adr3 = Kun.kun.kun;

			KunAdr.adr.adr = Kun.kun.kun;
			LiefAdr.adr.adr = Kun.kun.adr2;
			if ( ! LiefAdr.dbreadfirst())	// if : 260812
				strncpy (Kun.kun.iln, LiefAdr.adr.iln,sizeof(Kun.kun.iln) -1);	// 260812
			RechAdr.adr.adr = Kun.kun.adr3;
			if ( ! RechAdr.dbreadfirst())	// if : 260812
				strncpy (Kun.kun.rechiln, RechAdr.adr.iln,sizeof(Kun.kun.rechiln) -1);	// 260812


			Kun_tou.kun_tou.mdn = Kun.kun.mdn;
			Kun_tou.kun_tou.kun = Kun.kun.kun;
			Kun_tou.dbreadfirst();

			Zuschlag.zuschlag.kun = Kun.kun.kun;
			Zuschlag.zuschlag.mdn = Kun.kun.mdn;
			Zuschlag.dbreadfirst();			

	}
	
	else
	
	{ 
			memcpy (&Kun_erw.kun_erw, &kun_erw_null, sizeof (KUN_ERW));
			Kun_erw.kun_erw.mdn = Kun.kun.mdn;
			Kun_erw.kun_erw.fil = Kun.kun.fil;
			Kun_erw.kun_erw.kun = Kun.kun.kun;
			KunAdr.adr.adr = Kun.kun.adr1;
			Kun_erw.dbreadfirst ();     /* GK 10.12.2013 */
			KunAdr.dbreadfirst ();
			LiefAdr.adr.adr = Kun.kun.adr2;
			if ( ! LiefAdr.dbreadfirst())	// if : 260812
				strncpy (Kun.kun.iln, LiefAdr.adr.iln,sizeof(Kun.kun.iln) -1);	// 260812

			RechAdr.adr.adr = Kun.kun.adr3;
			if ( ! RechAdr.dbreadfirst())		// if : 260812
				strncpy (Kun.kun.rechiln, RechAdr.adr.iln,sizeof(Kun.kun.iln) -1);	// 260812

			Kun_tou.kun_tou.mdn = Kun.kun.mdn;
			Kun_tou.kun_tou.kun = Kun.kun.kun;
			if ( Kun.kun.stat_kun == 115 ) 
			{
				Kun.kun.stat_kun = Kun.kun.kun;
			}			
			Kun_tou.dbreadfirst();
			Zuschlag.zuschlag.kun = Kun.kun.kun;
			Zuschlag.zuschlag.mdn = Kun.kun.mdn;
			Zuschlag.dbreadfirst();			
			if (Page7 != NULL)
			{
				Page7->Read ();
			}
	}

	if (!NewClient)
	{
		Lock();
	
		if (KundeGesperrt)
		{
			MessageBox (_T("Der Kunde wird von einem anderen Benutzer bearbeitet"), NULL, 
						MB_OK | MB_ICONERROR);
			m_Kun.SetFocus ();
			m_Kun.SetSel (0, -1);
            return FALSE;
		}
	}
   
	EnableFields (TRUE);

   

	//Vertreter Felder ein / aus
	strcpy (sys_par.sys_par_nam, "vertr_abr_par");
	if (Sys_par.dbreadfirst () == 0)
	{
		if (!(sys_par.sys_par_wrt[0] == '1'))
		{
			
			m_Vertr1.EnableWindow(FALSE);
			m_Vertr2.EnableWindow(FALSE);
			m_LVertr.EnableWindow(FALSE);
		}
	}

	CString kz2 = Kun.kun.fak_kz;
	if (kz2 == _T("J"))
	{
		m_FactKZ.SetWindowTextA("   a k t i v i e r t");
		m_FactNR.EnableWindow(TRUE);
	}
	else if (kz2 == _T(""))
	{
		m_FactKZ.SetWindowTextA("   d e a k t i v i e r t");
		m_FactNR.EnableWindow(FALSE);
	}
	else
	{
		m_FactKZ.SetWindowTextA("   d e a k t i v i e r t");
		m_FactNR.EnableWindow(FALSE); 
	}

	m_Anr.SetFocus ();

	Form.Show ();
	ReadPrGr ();
	ReadKunPr ();
	Update->Show ();
	
	return TRUE;
}

void CKunPage::Lock()
{
	extern short sql_mode;
	short sql_s = sql_mode;
	sql_s = sql_mode;
	sql_mode = 1;
	Kun.beginwork();

	if (Kun.dblock () != 0)
	{
		KundeGesperrt = TRUE;
	}
	else
	{
		KundeGesperrt = FALSE;
	}
	sql_mode = sql_s;
}


void CKunPage::write ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein Satz zum Schreiben selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}

	
	/*if (KundeGesperrt) 
	{
			sprintf(ctext,"Kunde %.0f : Ist von einem anderen Benutzer gesperrt ",Kun.kun.kun);
			GetParentOwner ()->SetWindowText (_T(ctext));
	
			sprintf(ctext,"%s :  Speichern z.Zeit nicht m�glich",Kun.kun.kun_krz1);
			GetParent()->GetParent()->GetParent()-> SetWindowText (_T(ctext));
	}*/

	Form.Get ();
	if (!TestAdr1Type ())
	{
		return;
	}

	if	(Kun.kun.kun_seit.year < 1900)
	{ 
		CString Date;
		CStrFuncs::SysDate (Date);
		Kun.ToDbDate (Date, &Kun.kun.kun_seit);
	}
	
	if (KunAdr.adr.geb_dat.year < 1900)
	{
		KunAdr.adr.geb_dat.year = 1899;
		KunAdr.adr.geb_dat.month = 12;
		KunAdr.adr.geb_dat.day = 31;
	}
	
	if (LiefAdr.adr.geb_dat.year < 1900)
	{
		LiefAdr.adr.geb_dat.year = 1899;
		LiefAdr.adr.geb_dat.month = 12;
		LiefAdr.adr.geb_dat.day = 31;
	}

	if (RechAdr.adr.geb_dat.year < 1900)
	{
		RechAdr.adr.geb_dat.year = 1899;
		RechAdr.adr.geb_dat.month = 12;
		RechAdr.adr.geb_dat.day = 31;
	}


	if (!ReadPrGr ())
	{
		return;
	}

	if (!ReadKunPr ())
	{
		return;
	}

	if (cfg_EmailPruefung) 
	{
		if (!PruefeEMail ())
		{
			return;
		}
	}
// 260812 : erfassen in kun, zurueckschreiben in die Adressen 
// Problem : wenn adr2 == adr3 und nur auf einer Seite wurde editiert, was gilt dann ? 

		if ( Kun.kun.adr2 == Kun.kun.adr3 )
		{

			CString ariln ;
			CString aliln ;
			CString kriln ;
			CString kliln ;

			ariln = RechAdr.adr.iln ;
			aliln = LiefAdr.adr.iln ;
			kriln = Kun.kun.rechiln ;
			kliln = Kun.kun.iln ;

			if ( ! _tcscmp (ariln.Trim(), kriln.Trim()))
			{
				if (  ! _tcscmp (aliln.Trim(), kliln.Trim()))
				{
					;	//nix zu tun
				}
				else	// Rechadr mit Liefadr beaufschlagen 
				{
					strncpy (RechAdr.adr.iln, Kun.kun.iln,sizeof(Kun.kun.iln) -1);
					strncpy (LiefAdr.adr.iln, Kun.kun.iln,sizeof(Kun.kun.iln) -1);
					strncpy (Kun.kun.rechiln, Kun.kun.iln,sizeof(Kun.kun.iln) -1);
				}
			}
			else	// Liefadr mit Rechadr beaufschlagen 
			{
					strncpy (LiefAdr.adr.iln, Kun.kun.rechiln,sizeof(Kun.kun.iln) -1);
					strncpy (RechAdr.adr.iln, Kun.kun.rechiln,sizeof(Kun.kun.iln) -1);
					strncpy (Kun.kun.iln, Kun.kun.rechiln,sizeof(Kun.kun.iln) -1);

			}




		}
		else	// adr2 != adr3
		{
			strncpy (RechAdr.adr.iln, Kun.kun.rechiln,sizeof(Kun.kun.iln) -1);
			strncpy (LiefAdr.adr.iln, Kun.kun.iln,sizeof(Kun.kun.iln) -1);
		}
		if ( KunAdr.adr.adr == LiefAdr.adr.adr )
			strncpy (KunAdr.adr.iln, Kun.kun.iln,sizeof(Kun.kun.iln) -1);


	Kun.kun.tou = Kun_tou.kun_tou.tou;
	strncpy (KunAdr.adr.adr_krz, Kun.kun.kun_krz1,sizeof(KunAdr.adr.adr_krz) -1);
	KunAdr.adr.adr_krz[sizeof(KunAdr.adr.adr_krz) -1] = 0;
	if (KunAdr.adr.adr == LiefAdr.adr.adr)
	{
		strncpy (Kun.kun.kun_krz2, Kun.kun.kun_krz1,sizeof(Kun.kun.kun_krz2) -1);
		Kun.kun.kun_krz2[sizeof(Kun.kun.kun_krz2) -1] = 0;
	}
	if (KunAdr.adr.adr == RechAdr.adr.adr) 
	{
		strncpy (Kun.kun.kun_krz3, Kun.kun.kun_krz1,sizeof(Kun.kun.kun_krz3) -1);
		Kun.kun.kun_krz3[sizeof(Kun.kun.kun_krz3) -1] = 0;
	}

	if (KunAdr.adr.adr != 0) KunAdr.dbupdate();
	strcpy(Kun.kun.freifeld1,"1");  // gilt als "ueb_kz" , f�r testcsv.exe , damit sich bws_asc diesen Satz schnappt und freifeld1 wieder auf "" zur�cksetzt
	Kun_erw.kun_erw.mdn = Kun.kun.mdn;
	Kun_erw.kun_erw.fil = Kun.kun.fil;
	Kun_erw.kun_erw.kun = Kun.kun.kun;
	Kun.dbupdate ();

	if	(Kun_erw.kun_erw.mandseit.year < 1900)
	{ 
		CString Date;
		CStrFuncs::SysDate (Date);
		Kun_erw.ToDbDate (Date, &Kun_erw.kun_erw.mandseit);
	}


	Kun_erw.dbupdate ();
	Kun.commitwork ();

	if (KunAdr.adr.adr != LiefAdr.adr.adr)
	{
		strncpy (LiefAdr.adr.adr_krz, Kun.kun.kun_krz1,sizeof(LiefAdr.adr.adr_krz) -1);
		LiefAdr.adr.adr_krz[sizeof(LiefAdr.adr.adr_krz) -1] = 0;
		if (LiefAdr.adr.adr != 0) LiefAdr.dbupdate();
	}
	
	if (KunAdr.adr.adr != RechAdr.adr.adr)
	{
		if (LiefAdr.adr.adr != RechAdr.adr.adr)
		{
			strncpy (RechAdr.adr.adr_krz, Kun.kun.kun_krz1,sizeof(RechAdr.adr.adr_krz) -1);
			RechAdr.adr.adr_krz[sizeof(RechAdr.adr.adr_krz) -1] = 0;
			if (RechAdr.adr.adr != 0) RechAdr.dbupdate();
		} else RechAdr.UpdateSepa ();
	} else RechAdr.UpdateSepa ();
	
	Kun_tou.dbupdate();
	Gr_zuord.dbupdate();
	Zuschlag.dbupdate();
	Kun.commitwork ();

	if (Choice != NULL)
	{
		Choice->RefreshList ();
	}
    
	
	EnableFields (FALSE);
	m_Mdn.SetFocus ();
	KunProperty->SetActivePage (0);	
}



void CKunPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&Kun);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
		  Kun.kun.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CKunPage::OnKunChoice ()
{
	CString MdnNr;
	m_Mdn.GetWindowText (MdnNr);
    ChoiceStat = TRUE;
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceKun (this);
	    Choice->IsModal = ModalChoice;
		Choice->m_Mdn = _tstoi (MdnNr.GetBuffer ());
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->HideFilter = FALSE;
		Choice->HideOK    = HideOK;
		Choice->CreateDlg ();
	}

    Choice->SetDbClass (&Kun);
	Choice->SearchText = Search;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
		Choice->MoveWindow (&rect);
		Choice->SetListFocus ();
		return;
	}
    if (Choice->GetState ())
    {
		  CKunList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  memcpy (&Kun.kun, &kun_null, sizeof (KUN));
		  memcpy (&Kun_erw.kun_erw, &kun_erw_null, sizeof (KUN_ERW));
		  memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
		  Kun.kun.mdn = abl->mdn;
		  Kun.kun.kun = abl->kun;

		  if (Kun.dbreadfirst () == 0)
		  {
			  KunAdr.adr.adr = Kun.kun.adr1;
			  KunAdr.dbreadfirst ();
		  }

		  
		  
		  /*memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }*/
		  Form.Show ();
          m_Kun.SetSel (0, -1, TRUE);
		  m_Kun.SetFocus ();
		  PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}

void CKunPage::OnAdr1Choice ()
{
	CString AdrNr;
	m_Adr1.GetWindowText (AdrNr);
    
	ChoiceStat = TRUE;
	if (ChoiceAdr != NULL && !ModalChoiceAdr)
	{
		ChoiceAdr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceAdr == NULL)
	{
		ChoiceAdr = new CChoiceAdr (this);
	    ChoiceAdr->IsModal = ModalChoiceAdr;
		ChoiceAdr->m_Adr = _tstol (AdrNr.GetBuffer ());
		ChoiceAdr->CreateDlg ();
	}

    ChoiceAdr->SetDbClass (&KunAdr);
	ChoiceAdr->SearchText = Search;
	
	if (ModalChoiceAdr)
	{
			ChoiceAdr->DoModal();
	}
	
    if (ChoiceAdr->GetState ())
    {
		  CAdrList *abl = ChoiceAdr->GetSelectedText (); 
		  if (abl == NULL) return;
		  memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
		  Kun.kun.adr1 = abl->adr;
		  if (KunAdr.dbreadfirst () == 0)
		  {
			  KunAdr.adr.adr = Kun.kun.adr1;
			  KunAdr.dbreadfirst ();
		  }
			
		  Form.Show ();	
		  m_Adr1.SetSel (0, -1, TRUE);
		  m_Adr1.SetFocus ();
			  
	
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}

void CKunPage::OnPlz1Choice ()
{
	PLZ_CLASS Plz;
	memcpy (&Plz.plz, &plz_null, sizeof (Plz.plz));
	Form.GetFormField (&m_Ort)->Get ();
	Form.GetFormField (&m_Telefon)->Get ();
	Form.GetFormField (&m_Land)->Get ();
	Form.GetFormField (&m_IncludeLand)->Get ();
	_tcscpy (Plz.plz.ort, KunAdr.adr.ort1);
	if (IncludeLand)
	{
		memcpy (&Ptabn.ptabn, &ptabn_null, sizeof (Ptabn.ptabn));
		_tcscpy (Ptabn.ptabn.ptitem, _T("land"));
		_stprintf (Ptabn.ptabn.ptwert, "%hd", KunAdr.adr.land);
		if (Ptabn.dbreadfirst () == 0)
		{
			_tcscpy (Plz.plz.bundesland, Ptabn.ptabn.ptbez);
		}
	}
	CPlzList *abl = EnterPlz.Choice (Plz);
	if (abl != NULL)
	{
		
		_tcscpy (KunAdr.adr.plz, abl->plz.GetBuffer ());
		Form.GetFormField (&m_Plz)->Show ();
		_tcscpy (KunAdr.adr.ort1, abl->ort.GetBuffer ());
		Form.GetFormField (&m_Ort)->Show ();
		if (_tcscmp (KunAdr.adr.tel, _T("0")) < 0)
		{
			_tcscpy (KunAdr.adr.tel, abl->vorwahl.GetBuffer ());
			Form.GetFormField (&m_Telefon)->Show ();
		}
		CFormField *fLand = Form.GetFormField (&m_Land);
		int idx = EnterPlz.GetComboIdx (abl->bundesland.GetBuffer (), 
			                            fLand->ComboValues);
		if (idx != -1)
		{
			m_Land.SetCurSel (idx);
			fLand->Get ();
		}
	}
}

void CKunPage::OnChoice ()
{
	OnKunChoice ();
}

void CKunPage::OnKunSelected ()
{
	if (Choice == NULL) return;
    CKunList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    memcpy (&Kun.kun, &kun_null, sizeof (KUN));
    memcpy (&Kun_erw.kun_erw, &kun_erw_null, sizeof (KUN_ERW));
    memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
    Kun.kun.mdn = abl->mdn;
    Kun.kun.kun = abl->kun;
    memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
    memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
    Mdn.mdn.mdn = abl->mdn;
    if (Mdn.dbreadfirst () == 0)
    {
		  KunAdr.adr.adr = Mdn.mdn.adr;
		  KunAdr.dbreadfirst ();
    }
    else
    {
		  _tcscpy (KunAdr.adr.adr_krz, _T(""));
    }
    Form.Show ();
	Read ();
	if (Choice->FocusBack)
	{
		Choice->SetListFocus ();
	}
}

void CKunPage::OnKunCanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CKunPage::TestAdr1Type ()
{
	KunAdr.adr.adr = Kun.kun.adr1;
	memcpy (&KunAdr.saveadr,&KunAdr.adr, sizeof(KunAdr.adr));

	short savetyp = KunAdr.adr.adr_typ ;
	int dsqlstatus = KunAdr.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		savetyp = KunAdr.adr.adr_typ ;

		memcpy (&KunAdr.adr,&KunAdr.saveadr, sizeof(KunAdr.adr));
		
		
/**  raus damit, gibt nur �rger LuD 10.01.2011
	    if (KunAdr.adr.adr_typ != KunAdrType &&
			KunAdr.adr.adr_typ != KunAdrDivType)
		{
			AfxMessageBox (_T("Falscher Adresstyp"));
			m_Adr1.SetFocus ();
			return FALSE;
		}
		**/
	}
	else
	{
		KunAdr.adr.adr_typ = KunAdrType;
		savetyp = KunAdr.adr.adr_typ ;
	}

	KunAdr.adr.adr_typ = savetyp ;

	return TRUE;
}

BOOL CKunPage::OnAdrSelected ()
{
	CString AdrNr;
	
	if (ChoiceAdr == NULL) 
	{
		m_Adr1.GetWindowText (AdrNr);		
		memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
		
		long Adr1 = atol(AdrNr);
		Kun.kun.adr1 = Adr1;
	}
	else
	{
		CAdrList *abl = ChoiceAdr->GetSelectedText();
		
		if (abl == NULL) 
		{
			return FALSE;
		}
		else
		{
			memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
			Kun.kun.adr1 = abl->adr;

		}
	}
    
	memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
	KunAdr.adr.adr = Kun.kun.adr1;
	int dsqlstatus = KunAdr.dbreadfirst ();
	if (dsqlstatus == 100)
	{
		KunAdr.adr.adr_typ = KunAdrType;
// Grundwert holen LuD 10.01.2011
		KunAdr.adr.adr = -1;
		KunAdr.dbreadfirst ();
		KunAdr.adr.adr = Kun.kun.adr1;
		_tcscpy (KunAdr.adr.adr_krz, Kun.kun.kun_krz1);
	}
	/**  raus damit, gibt nur �rger LuD 10.01.2011
	if (KunAdr.adr.adr_typ != KunAdrType &&
		KunAdr.adr.adr_typ != KunAdrDivType)
	{
		AfxMessageBox (_T("Falscher Adresstyp"));
		return FALSE;
	}
	**/

	_tcscpy (Kun.kun.kun_krz1, KunAdr.adr.adr_krz);
    Form.Show ();
	m_Anr.SetFocus();
	
	//Read ();
		
	if (ChoiceAdr != NULL)
	{
		if (ChoiceAdr->FocusBack)
		{
			ChoiceAdr->SetListFocus ();
		}
	}
	return TRUE;
}


void CKunPage::OnAdrCanceled ()
{
	ChoiceAdr->ShowWindow (SW_HIDE);
}

void CKunPage::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Copy ();
	}
}

void CKunPage::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CWnd *pWnd = GetFocus ();
	if (pWnd->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) pWnd)->Paste ();
	}
}

void CKunPage::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.


	if (m_Mdn.IsWindowEnabled ())
	{
		MessageBox (_T("Es ist kein Satz zum L�schen selektiert"), NULL,
					MB_OK | MB_ICONERROR);
		return;
	}
	if (MessageBox (_T("Kundendaten l�schen"),NULL, MB_YESNO | MB_DEFBUTTON2) ==
		IDYES)
	{
		Kun.beginwork ();
		Kun.dbdelete ();
		Kun_erw.kun_erw.mdn = Kun.kun.mdn;
		Kun_erw.kun_erw.fil = Kun.kun.fil;
		Kun_erw.kun_erw.kun = Kun.kun.kun;
		Kun_erw.dbdelete ();
		Kun.commitwork ();
		memcpy (&Kun.kun, &kun_null, sizeof (KUN));
		memcpy (&Kun_erw.kun_erw, &kun_erw_null, sizeof (KUN_ERW));




		EnableFields (FALSE);
		Form.Show ();
		m_Mdn.SetFocus ();
		if (Choice != NULL)
		{
			Choice->RefreshList ();
		}
		KunProperty->SetActivePage (0);
	}
}

void CKunPage::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
//		Control->SetReadOnly (!enable);
        Control->EnableWindow (enable);
	}
}

BOOL CKunPage::StepBack ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
			Frame->DestroyWindow ();
			return FALSE;
					
	}
	
	if (!m_Mdn.IsWindowEnabled ())
	{
		EnableFields (FALSE);
		m_Mdn.SetFocus ();
		KunProperty->SetActivePage (0);	
		Kun.rollbackwork ();
		return TRUE;
	}
    return TRUE;	
}


void CKunPage::OnCopy ()
{
    OnEditCopy ();
}  



void CKunPage::OnPaste ()
{
	OnEditPaste ();
}

void CKunPage::OnCopyEx ()
{

//	if (!m_A.IsWindowEnabled ())
	{
		Copy ();
		return;
	}
}

void CKunPage::OnPasteEx ()
{

//	if (!m_A.IsWindowEnabled () && Paste ())
	{
		Kun.CopyData (&kun);
		Kun_erw.CopyData (&kun_erw);
		Form.Show ();
/*
		if (KunPageEx != NULL && IsWindow (KunPageEx->m_hWnd))
		{
			KunPageEx->Form.Show ();
		}
*/
		return;
	}
}

BOOL CKunPage::Copy ()
{
	Form.Get ();
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return FALSE;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      int err = GetLastError ();
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return FALSE;  
    }

	try
	{
        UINT CF_A_KUN_GX = RegisterClipboardFormat (_T("kun")); 
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, sizeof (KUN)); 
        char *p = (char *) GlobalLock(hglbCopy);
        memcpy (p, &Kun.kun, sizeof (Kun.kun));
        GlobalUnlock(hglbCopy); 
		HANDLE cData;
		cData = ::SetClipboardData( CF_A_KUN_GX, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
    return TRUE;
}

BOOL CKunPage::Paste ()
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return FALSE;
    }

    HGLOBAL hglbCopy;
    UINT CF_A_KUN_GX;
    _TCHAR name [256];
    CF_A_KUN_GX = EnumClipboardFormats (0);
	if (CF_A_KUN_GX == 0)
	{
		int err = GetLastError ();
		CloseClipboard();
		return FALSE;
	}

    int ret;
    while ((ret = GetClipboardFormatName (CF_A_KUN_GX, name, sizeof (name))) != 0)
    { 
         CString Name = name;
         if (Name == _T("a_kun_gx"))
         {
              break;
         }  
		 CF_A_KUN_GX = EnumClipboardFormats (CF_A_KUN_GX);
    }
    if (ret == 0)
    {
         CloseClipboard();
         return FALSE;
    }    

    try
    {	
  	     hglbCopy =  ::GetClipboardData(CF_A_KUN_GX);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
 	     LPSTR p = (LPSTR) GlobalLock ((HGLOBAL) hglbCopy);
         memcpy ( (LPSTR) &kun, p, sizeof (kun));
  	     GlobalUnlock ((HGLOBAL) hglbCopy);
    }
    catch (...) {};
    CloseClipboard();
    return TRUE; 
}

void CKunPage::EnableFields (BOOL b)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (!b)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);
	if (ModalChoice)
	{
		m_KunChoice.EnableWindow (!b);
	}
}


void CKunPage::OnKillFocus (CWnd *newFocus)
{
	CWnd *Control = GetFocus ();
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		if (f->Scale > 0)
		{
			f->Show ();
		}
	}
}


void CKunPage::FillPtabCombo (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 

		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();

		
	}
}




void CKunPage::FillAnr ()
{
	CFormField *f = Form.GetFormField (&m_Anr);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"anr\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}

void CKunPage::FillPrGr ()
{
	CFormField *f = Form.GetFormField (&m_PrGr);
	if (f != NULL)
	{
		f->ComboValues.clear ();
	
		Iprgrstufk.sqlout ((short *) &Iprgrstufk.iprgrstufk.pr_gr_stuf, SQLSHORT, 0); 
		Iprgrstufk.sqlout ((char *) Iprgrstufk.iprgrstufk.zus_bz  , SQLCHAR, sizeof (Iprgrstufk.iprgrstufk.zus_bz)); 
		
        int cursor = Iprgrstufk.sqlcursor (_T("select pr_gr_stuf, zus_bz from iprgrstufk ")
			                          _T("where pr_gr_stuf <> -1 ")
									  _T("order by pr_gr_stuf"));
		
		
		
		while (Iprgrstufk.sqlfetch (cursor) == 0)
		{
			Iprgrstufk.dbreadfirst ();

			//LPSTR pos = (LPSTR) Iprgrstufk.iprgrstufk.pr_gr_stuf;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			//pos = (LPSTR) Iprgrstufk.iprgrstufk.zus_bz;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), Iprgrstufk.iprgrstufk.pr_gr_stuf,
				                             Iprgrstufk.iprgrstufk.zus_bz);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}


void CKunPage::FillPrList ()
{
	CFormField *f = Form.GetFormField (&m_PrList);
	if (f != NULL)
	{
		f->ComboValues.clear ();
	
		I_kun_prk.sqlout ((long *) &I_kun_prk.i_kun_prk.kun_pr, SQLLONG, 0); 
		I_kun_prk.sqlout ((char *) I_kun_prk.i_kun_prk.zus_bz  , SQLCHAR, sizeof (I_kun_prk.i_kun_prk.zus_bz)); 
		
        int cursor = I_kun_prk.sqlcursor (_T("select kun_pr, zus_bz from i_kun_prk ")
			                          _T("where kun_pr <> -1 ")
									  _T("order by kun_pr"));
		
		
		
		while (I_kun_prk.sqlfetch (cursor) == 0)
		{
			I_kun_prk.dbreadfirst ();

			//LPSTR pos = (LPSTR) Iprgrstufk.iprgrstufk.pr_gr_stuf;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			//pos = (LPSTR) Iprgrstufk.iprgrstufk.zus_bz;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%ld %s"), I_kun_prk.i_kun_prk.kun_pr,
				                             I_kun_prk.i_kun_prk.zus_bz);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}



void CKunPage::FillKosten ()
{
	CFormField *f = Form.GetFormField (&m_Kosten);
	if (f != NULL)
	{
		f->ComboValues.clear ();
	
		Kst.sqlout ((long *) &Kst.kst.kst, SQLLONG, 0); 
		Kst.sqlout ((char *) Kst.kst.kst_bz1  , SQLCHAR, sizeof (Kst.kst.kst_bz1)); 
		
        int cursor = I_kun_prk.sqlcursor (_T("select kst, kst_bz1 from kst ")
			                          	  _T("order by kst"));
		
		
		
		while (I_kun_prk.sqlfetch (cursor) == 0)
		{
			Kst.dbreadfirst ();

			

			//LPSTR pos = (LPSTR) Iprgrstufk.iprgrstufk.pr_gr_stuf;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			//pos = (LPSTR) Iprgrstufk.iprgrstufk.zus_bz;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%ld %s"), Kst.kst.kst,
											 Kst.kst.kst_bz1);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}





void CKunPage::OnCbnSelchangePrGr()
{

	//FillRList nach Update der PrGRStuf	

}

void CKunPage::OnBnClickedFactkz()
{
	CString kz;
	Form.Get();
	kz = Kun.kun.fak_kz;

	if (kz == _T("J"))
	{
		m_FactNR.EnableWindow(TRUE);
		m_FactKZ.SetWindowTextA("   a k t i v i e r t");
	}
	else
	{
		m_FactNR.EnableWindow(FALSE);
		m_FactKZ.SetWindowTextA("   d e a k t i v i e r t");
	}
		
}



void CKunPage::FillKunTyp ()
{
	CFormField *f = Form.GetFormField (&m_KunTyp);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"kun_typ\" and ptbez not matches \"*Verband*\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			/*LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			*/
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}

BOOL CKunPage::OnKillActive ()
{
	Form.Get ();
	return TRUE;
}

BOOL CKunPage::OnSetActive ()
{
	Form.Show ();
	return TRUE;
}

void CKunPage::OnFilePrint()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
//	dlg.Print ();
	
	
}

BOOL CKunPage::PruefeEMail () //FS-319
{
	Form.Get ();
	int dkun;
	BOOL bWarnung = FALSE;
	CString Warnung , Kunde;
	char cadr_nam1 [sizeof (KunAdr.adr.adr_nam1) +1];
	CString email; 
	//email.Format (_T(KunAdr.adr.email));
	//email.MakeLower ();
	//strcpy (KunAdr.adr.email, 	email.GetBuffer ());
 
	if (strlen(KunAdr.adr.email) > 0)
	{
		Kun.sqlin ((char *) KunAdr.adr.email, SQLCHAR, sizeof (KunAdr.adr.adr_nam1)); 
		Kun.sqlin ((long *) &KunAdr.adr.adr, SQLLONG, 0); 
		Kun.sqlout ((long *) &dkun, SQLLONG, 0); 
		Kun.sqlout ((char *) cadr_nam1, SQLCHAR, sizeof (KunAdr.adr.adr_nam1)); 
		CString Sql;
        Sql.Format (_T("select kun,adr.adr_nam1 from kun,adr where kun.adr1 = adr.adr ")
			        _T("and LOWER (adr.email) = LOWER (?) and adr.adr <> ? ")); 

        int cursor = Kun.sqlcursor (Sql.GetBuffer ());
		Warnung.Format (_T("Die e-Mail Adresse wurde schon an folgende Kunden vergeben: \n"));
		while (Kun.sqlfetch (cursor) == 0)
		{
			Kunde.Format (_T("\n - %d %s  "), dkun, cadr_nam1);
			Warnung += Kunde;
			bWarnung = TRUE;
		}
		Kun.sqlclose (cursor);
	}
//	MessageBox (Warnung, "--Warnung -- ", MB_OK | MB_ICONWARNING);

	if (bWarnung == TRUE)
	{
		Kunde.Format (_T("\n\n Speichern daher nicht m�glich ! "), dkun, cadr_nam1);
		Warnung += Kunde;
//		INT_PTR ret = MessageBox (_T(Warnung),KunAdr.adr.email,MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);
	    AfxMessageBox( _T(Warnung));
	    return FALSE; 
		/**
		if (ret == IDYES)
		{
			return TRUE;
		} else return FALSE;
		**/
	}
	return TRUE;
}

BOOL CKunPage::ReadPrGr ()
{
	Form.Get ();
	if (Kun.kun.pr_stu == 0l)
	{
		strcpy (Iprgrstufk.iprgrstufk.zus_bz,"");
		Form.Show ();
		return TRUE;
	}

    Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
    Iprgrstufk.iprgrstufk.pr_gr_stuf = Kun.kun.pr_stu;
    if (Iprgrstufk.dbreadfirst () == 0)
	{
		Form.Show ();
		return TRUE;
	}
	MessageBox ("Preisgruppenstufe nicht gefunden", "Fehler", MB_OK | MB_ICONERROR);
	return FALSE;
}

BOOL CKunPage::ReadKunPr ()
{
	Form.Get ();
	if (Kun.kun.pr_lst == 0l)
	{
		strcpy (I_kun_prk.i_kun_prk.zus_bz,"");
		Form.Show ();
		return TRUE;
	}

    I_kun_prk.i_kun_prk.mdn = Mdn.mdn.mdn;
    I_kun_prk.i_kun_prk.kun_pr = Kun.kun.pr_lst;
    if (I_kun_prk.dbreadfirst () == 0)
	{
		Form.Show ();
		return TRUE;
	}
	MessageBox ("Kundenpreisliste nicht gefunden", "Fehler", MB_OK | MB_ICONERROR);
	return FALSE;
}

void CKunPage::OnPrGrchoice ()
{
	CChoicePrGrStuf *Choice = new CChoicePrGrStuf (this);
	 
	Form.Get ();
    Choice->IsModal = TRUE;
	Choice->m_Mdn = Mdn.mdn.mdn;
	Choice->SearchText = "";
	Choice->CreateDlg ();

    Choice->SetDbClass (&Kun);
	Choice->DoModal();
    if (Choice->GetState ())
    {
		  CPrGrStufList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
          Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
          Iprgrstufk.iprgrstufk.pr_gr_stuf = abl->pr_gr_stuf;
		  if (Iprgrstufk.dbreadfirst () == 0)
		  {
            Kun.kun.pr_stu = (short) Iprgrstufk.iprgrstufk.pr_gr_stuf;  
			Form.Show ();
//			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	delete Choice;
}

void CKunPage::OnKunPrchoice ()
{
	Form.Get ();
	CChoiceIKunPr *Choice = new CChoiceIKunPr (this);
    Choice->IsModal = TRUE;
	Choice->m_Mdn = Mdn.mdn.mdn;
	Choice->SearchText = "";
	Choice->CreateDlg ();

    Choice->SetDbClass (&Kun);
	Choice->DoModal();

    if (Choice->GetState ())
    {
		  CIKunPrList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk_null, sizeof (I_KUN_PRK));
          I_kun_prk.i_kun_prk.mdn = Mdn.mdn.mdn;
          I_kun_prk.i_kun_prk.kun_pr = abl->kun_pr;
		  if (I_kun_prk.dbreadfirst () == 0)
		  {
            Kun.kun.pr_lst = (short) I_kun_prk.i_kun_prk.kun_pr;  
			Form.Show ();
//			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
	}
	delete Choice;
}

void CKunPage::OnEnterPartner ()
{ 
	CProcess p;
	CString command;

	command.Format (_T("Partner %hd 0 %ld"), Mdn.mdn.mdn, Kun.kun.kun);
	p.SetCommand (command);
	p.Start ();
}

void CKunPage::OnEnChangeKunkrz1()
{
	CString Text;
	m_KunKrz1.GetWindowText (Text);
	if (Text.GetLength() >= sizeof(Kun.kun.kun_krz1))
	{
		strcpy(Kun.kun.kun_krz1,"");
		strncpy(Kun.kun.kun_krz1,Text.GetBuffer(),sizeof(Kun.kun.kun_krz1)-1);
		Kun.kun.kun_krz1[sizeof(Kun.kun.kun_krz1)-1] = 0;
//		Form.Show ();
		Form.GetFormField (&m_KunKrz1)->Show ();
		m_KunKrz1.SetSel (sizeof(Kun.kun.kun_krz1)-1, -1,FALSE);
	}
}
void CKunPage::OnInfo ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
		return;
	}

	if (_CallInfoEx == NULL)
	{
		return;
	}

	Form.Get ();
	// 15.12.2014  char Item[256] = {"vertrieb"};
	char Item[256] = {"kun"};   //15.12.2014   B�singer  ,   so ist es wie im 16500.rsr 
	char where [256];

//15.12.2014	sprintf (where, "where kun = %ld and kun_bran2 = %s and vertr1 = %ld", Kun.kun.kun,Kun.kun.kun_bran2,Kun.kun.vertr1);
	sprintf (where, "where kun = %ld ", Kun.kun.kun);

	BOOL ret = (*_CallInfoEx) (this->m_hWnd, NULL, Item, where, 0);

}


int CKunPage::HoleKunNrNeu ()
{
		Kun.sqlin ((long *) &min_kun_nr, SQLLONG, 0); 
		Kun.sqlin ((long *) &max_kun_nr, SQLLONG, 0); 
		Kun.sqlout ((long *) &Kun.kun.kun, SQLLONG, 0); 
		
        int cursor = Kun.sqlcursor (_T("select max(kun) from kun where kun between ? and ? "));
			
		int dsqlstatus = Kun.sqlfetch (cursor); 

		if (dsqlstatus == 0)
		{
			if (Kun.kun.kun == 0)
			{
				return min_kun_nr;
			}
			Kun.kun.kun ++;
			if (Kun.dbreadfirst () == 100)
			{
				return Kun.kun.kun;
			}
		}
		memcpy (&Kun.kun, &kun_null, sizeof (KUN));
		Form.Get ();
		Kun.kun.mdn = Mdn.mdn.mdn;
		return 0;
}


