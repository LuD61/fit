#pragma once
#include "SplitPane.h"
#include "HierarchieDataDlg.h"
#include "HierarchieTree.h"
#include "H1.h"
#include "H2.h"
#include "H3.h"
#include "H4.h"


// CHierarchieDialog-Dialogfeld

class CHierarchieDialog : public CDialog
{
	DECLARE_DYNAMIC(CHierarchieDialog)

public:
	CHierarchieDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CHierarchieDialog();

// Dialogfelddaten
	enum { IDD = IDD_HIERARCHIE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog ();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor); 
	virtual void OnSize (UINT nType, int cx, int cy);
	void Paste ();
	void Copy ();

private:
	H1_CLASS H1;
	H2_CLASS H2;
	H3_CLASS H3;
	H4_CLASS H4;
	int cursor_h21;
	int cursor_h31;
	int cursor_h32;
	int cursor_h41;
	int cursor_h42;
	int cursor_h43;

	CSplitPane SplitPane;
	CHierarchieDataDlg HierarchieDataDlg;
	CHierarchieTree HierarchieTree;
};
