/***************************************************************************/
/* Programmname :  Vector.CPP                                              */
/*-------------------------------------------------------------------------*/
/* Funktion :  Klasse CVector                                              */
/*-------------------------------------------------------------------------*/
/* Revision : 1.0    Datum : 07.06.04   erstellt von : W. Roth             */
/*                   allgemneine Klasse f�r Anzahl beliebige Anzahl von    */
/*                   von Pointern                                          */    
/*-------------------------------------------------------------------------*/
/* Aufruf :                                                                */
/* Funktionswert :                                                         */
/* Eingabeparameter :                                                      */
/*                                                                         */
/* Ausgabeparameter :                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/

#include "stdafx.h"
#include "Vector.h"

CVector::CVector ()
{
     Arr = NULL;
     anz = 0;
     pos = 0;
}

CVector::CVector (void **Object)
{
	  Arr = NULL;
      SetObject (Object);
}


CVector::~CVector ()
{
     if (Arr != NULL)
     {
         delete Arr;
     }
     Arr = NULL;
     anz = 0;
     pos = 0;
}

void CVector::Init (void)
{
     if (Arr != NULL)
     {
         delete Arr;
     }
     Arr = NULL;
     anz = 0;
     pos = 0;
}

const CVector& CVector::operator= (CVector& v)
{
	 Init ();

     v.FirstPosition ();
	 void *Object;
	 while ((Object = v.GetNext ()) != NULL)
	 {
		 Add (Object);
	 }
	 return *this;
}
     


void CVector::SetObject (void **Object)
{
     void **ArrS; 

     if (Arr != NULL)
     {
         delete Arr;
     }
     Arr = Object;
	 if (Arr == NULL) 
	 {
		 pos = anz = 0;
		 return;
	 }
     for (anz = 0; Arr[anz] != NULL; anz ++);
     ArrS = new void *[anz];
     for (int i = 0; i < anz; i ++)
     {
         ArrS[i] = Arr[i];
     }
     Arr = ArrS;
}

void **CVector::GetObject ()
{
	 return Arr;
}

void CVector::DestroyAll ()
{
     if (Arr == NULL)
     {
         return;
     }
     for (int i = 0; i < anz; i ++)
     {
         delete Arr[i];
     }
	 anz = 0;
	 delete Arr;
	 Arr = NULL;
}


void CVector::Add (void *Object)
{
    void **ArrS;
	int i;

    ArrS = new void * [anz + 1];
    for (i = 0; i < anz; i ++)
    {
        ArrS[i] = Arr[i];
    }

    void *Ch = Object;
    ArrS[i] = Ch;
    anz ++;
	if (Arr != NULL)
	{
       delete Arr;
	}
    Arr = ArrS;
}


BOOL CVector::Drop (int idx)
{
    void **ArrS;

    if (idx >= anz)
    {
        return FALSE;
    }

    ArrS = new void * [anz - 1];
	int j = 0;
    for (int i = 0, j = 0; i < anz; i ++)
    {
        if (i == idx) continue;
        ArrS[j] = Arr[i];
		j ++;
    }

    anz --;
    delete Arr;
    Arr = ArrS;
    return TRUE;
}

BOOL CVector::Drop (void *Object)
{

    for (int i = 0; i < anz; i ++)
    {
		if (Arr[i] == Object)
		{
			return Drop (i);
		}
	}
    return FALSE;
}

BOOL CVector::Find (void *Object)
{

    for (int i = 0; i < anz; i ++)
    {
		if (Arr[i] == Object)
		{
			return i;
		}
	}
    return -1;
}

void CVector::SetPosition (int pos)
{
    this->pos = pos;
}

void CVector::FirstPosition (void)
{
    SetPosition (0);
}

void *CVector::Get (int idx)
{
    if (idx >= anz)
    {
        return NULL;
    }
    return Arr [idx];
}

void *CVector::GetNext (int pos)
{
    this->pos = pos;
    return GetNext ();
}

void *CVector::GetNext (void)
{
    if (pos >= anz)
    {
        return NULL;
    }
    pos ++;

	
	return Arr [pos - 1];
}
