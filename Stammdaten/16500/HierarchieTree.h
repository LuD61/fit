#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "CtrlGrid.h"
#include "UpdateEvent.h"
#include "PageUpdate.h"
#include "H1.h"
#include "H2.h"
#include "H3.h"
#include "H4.h"
#include "HTreeCtrl.h"


// CHierarchieTree-Dialogfeld

class CHierarchieTree : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CHierarchieTree)

public:
	CHierarchieTree(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CHierarchieTree();

// Dialogfelddaten
	enum { IDD = IDD_HIERARCHIE_TREE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor); 
	virtual void OnSize (UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTvnSelchangedTree1(NMHDR *pNMHDR, LRESULT *pResult);
public:
	CStatic m_LHierarchieTree;
public:
	CPageUpdate *PageUpdate;
	CHTreeCtrl m_HierarchieTree;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	H1_CLASS *H1;
	H2_CLASS *H2;
	H3_CLASS *H3;
	H4_CLASS *H4;

	int cursor_h21;
	int cursor_h31;
	int cursor_h32;
	int cursor_h41;
	int cursor_h42;
	int cursor_h43;
	virtual void DeleteRow ();
	void DeleteH2 ();
	void DeleteH3 ();
	void DeleteH4 ();

private:
	CFont Font;
	CCtrlGrid CtrlGrid;
	afx_msg void OnTvnSelchangedEmbList(NMHDR *pNMHDR, LRESULT *pResult);
	int GetRootPath (HTREEITEM Item, int p);
	virtual void Update ();
};
