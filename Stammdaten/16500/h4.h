#ifndef _H4_DEF
#define _H4_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct H4 {
   long           hirarchie1;
   long           hirarchie2;
   long           hirarchie3;
   long           hirarchie4;
   TCHAR          h4_bez[37];
   TCHAR          h4_bezkrz[17];
};
extern struct H4 h4, h4_null;

#line 8 "H4.rh"

class H4_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               H4 h4;  
               H4_CLASS () : DB_CLASS ()
               {
               }
};
#endif
