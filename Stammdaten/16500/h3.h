#ifndef _H3_DEF
#define _H3_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct H3 {
   long           hirarchie1;
   long           hirarchie2;
   long           hirarchie3;
   TCHAR          h3_bez[37];
   TCHAR          h3_bezkrz[17];
};
extern struct H3 h3, h3_null;

#line 8 "H3.rh"

class H3_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               H3 h3;  
               H3_CLASS () : DB_CLASS ()
               {
               }
};
#endif
