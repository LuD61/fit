// FrachtKosten.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16500.h"
#include "KunPage.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "kunpage.h"
#include "token.h"
#include "sys_par.h"
#include "ptabn.h"
#include "iprgrstufk.h"
#include "kst.h"
#include "kun_tou.h"
#include "fil.h"
#include "gr_zuord.h"
#include "haus_bank.h"
#include "FrachtKosten.h"
#include "NumEdit.h"
#include "kun.h"
#include "kun_erw.h"
#include "KunPage2.h"
#include "zuschlag.h"
#include "NumEdit.h"




// CFrachtKosten-Dialogfeld

IMPLEMENT_DYNAMIC(CFrachtKosten, CDialog)

CFrachtKosten::CFrachtKosten(CWnd* pParent /*=NULL*/)
	: CDialog(CFrachtKosten::IDD, pParent)
{


}

CFrachtKosten::~CFrachtKosten()
{
	
}

void CFrachtKosten::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRENZ1, m_Grenz1);
	DDX_Control(pDX, IDC_GRENZ2, m_Grenz2);
	DDX_Control(pDX, IDC_GRENZ3, m_Grenz3);
	DDX_Control(pDX, IDC_GRENZ4, m_Grenz4);
	DDX_Control(pDX, IDC_PROZ1, m_Proz1);
	DDX_Control(pDX, IDC_PROZ2, m_Proz2);
	DDX_Control(pDX, IDC_PROZ3, m_Proz3);
	DDX_Control(pDX, IDC_PROZ4, m_Proz4);
	DDX_Control(pDX, IDC_GRENZ, m_LGrenz);
	DDX_Control(pDX, IDC_PROZ, m_LProz);
	DDX_Control(pDX, IDOK, m_BOK);
}


BEGIN_MESSAGE_MAP(CFrachtKosten, CDialog)
END_MESSAGE_MAP()


// CFrachtKosten-Meldungshandler


BOOL CFrachtKosten::OnInitDialog()
{
	CDialog::OnInitDialog();

		if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	Form.Add (new CFormField (&m_Grenz1,EDIT,        (double *)  &Zuschlag->zuschlag.grenz_stu1, VDOUBLE, 13, 3));
	Form.Add (new CFormField (&m_Grenz2,EDIT,        (double *)  &Zuschlag->zuschlag.grenz_stu2, VDOUBLE, 13, 3));
	Form.Add (new CFormField (&m_Grenz3,EDIT,        (double *)  &Zuschlag->zuschlag.grenz_stu3, VDOUBLE, 13, 3));
	Form.Add (new CFormField (&m_Grenz4,EDIT,        (double *)  &Zuschlag->zuschlag.grenz_stu4, VDOUBLE, 13, 3));
	Form.Add (new CFormField (&m_Proz1,EDIT,        (double *)  &Zuschlag->zuschlag.proz_stu1, VDOUBLE, 5, 2));
	Form.Add (new CFormField (&m_Proz2,EDIT,        (double *)  &Zuschlag->zuschlag.proz_stu2, VDOUBLE, 5, 2));
	Form.Add (new CFormField (&m_Proz3,EDIT,        (double *)  &Zuschlag->zuschlag.proz_stu3, VDOUBLE, 5, 2));
	Form.Add (new CFormField (&m_Proz4,EDIT,        (double *)  &Zuschlag->zuschlag.proz_stu4, VDOUBLE, 5, 2));


	CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);


	CCtrlInfo *c_LGrenz = new CCtrlInfo (&m_LGrenz, 1, 2, 1, 1);
	CtrlGrid.Add (c_LGrenz);
	CCtrlInfo *c_LProz = new CCtrlInfo (&m_LProz, 2, 2, 1, 1);
	CtrlGrid.Add (c_LProz);

	CCtrlInfo *c_Grenz1 = new CCtrlInfo (&m_Grenz1, 1, 4, 1, 1);
	CtrlGrid.Add (c_Grenz1);
	CCtrlInfo *c_Proz1 = new CCtrlInfo (&m_Proz1, 2, 4, 1, 1);
	CtrlGrid.Add (c_Proz1);

	CCtrlInfo *c_Grenz2 = new CCtrlInfo (&m_Grenz2, 1, 5, 1, 1);
	CtrlGrid.Add (c_Grenz2);
	CCtrlInfo *c_Proz2 = new CCtrlInfo (&m_Proz2, 2, 5, 1, 1);
	CtrlGrid.Add (c_Proz2);

	CCtrlInfo *c_Grenz3 = new CCtrlInfo (&m_Grenz3, 1, 6, 1, 1);
	CtrlGrid.Add (c_Grenz3);
	CCtrlInfo *c_Proz3 = new CCtrlInfo (&m_Proz3, 2, 6, 1, 1);
	CtrlGrid.Add (c_Proz3);

	CCtrlInfo *c_Grenz4 = new CCtrlInfo (&m_Grenz4, 1, 7, 1, 1);
	CtrlGrid.Add (c_Grenz4);
	CCtrlInfo *c_Proz4 = new CCtrlInfo (&m_Proz4, 2, 7, 1, 1);
	CtrlGrid.Add (c_Proz4);



	
	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	CtrlGrid.Display ();


	Form.Show ();

	return TRUE;

	m_Grenz1.SetFocus();

	
}


BOOL CFrachtKosten::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				
			}
			
	}
	return CDialog::PreTranslateMessage(pMsg);
}


BOOL CFrachtKosten::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_BOK)
	{
		OnOK();
		return TRUE;		
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}


void CFrachtKosten::OnOK ()
{
	Form.Get();
	CDialog::OnOK ();
}

