#include <stdio.h>
#include <stdlib.h>
#include "strfkt.h"
#include "scanean.h"


BOOL SCANEAN::ReadStandard (FILE *fp, char *buffer)
{
        int i = 0;

   
        while (fgets (buffer, 511, fp))
        {
            if (buffer[0] == '[')
            {
                break;
            }
            Token *token = new Token (buffer, " ");
            if (token == NULL)
            {
                continue;
            }
            int Tanz = token->GetAnzToken ();
            if (Tanz < 2)
            {
                continue;
            }
            if (Tanz == 2)
            {
                 Structean[i] = new STRUCTEAN (atoi (token->GetToken(0)), token->GetToken(1),
                                               4, 0, TRUE, FALSE); 
            }
            else if (Tanz == 3)
            {
                 Structean[i] = new STRUCTEAN (atoi (token->GetToken(0)), token->GetToken(1),
                                               atoi (token->GetToken(2)), 0, TRUE, FALSE);
            }
            else if (Tanz == 4)
            {
                 Structean[i] = new STRUCTEAN (atoi (token->GetToken(0)), token->GetToken(1),
                                               atoi (token->GetToken(2)),
                                               atoi (token->GetToken(3)), TRUE, FALSE);
            }
            else if (Tanz == 5)
            {
                 Structean[i] = new STRUCTEAN (atoi (token->GetToken(0)), token->GetToken(1),
                                               atoi (token->GetToken(2)),
                                               atoi (token->GetToken(3)), 
											   atoi (token->GetToken(4)), FALSE);
            }
            else
            {
                 Structean[i] = new STRUCTEAN (atoi (token->GetToken(0)), token->GetToken(1),
                                               atoi (token->GetToken(2)),
                                               atoi (token->GetToken(3)), 
											   atoi (token->GetToken(4)),
											   atoi (token->GetToken(5))
											   );
            }
            i ++;
        }
        Structean[i] = NULL;
        return FALSE;
}


BOOL SCANEAN::ReadEan128 (FILE *fp, char *buffer)
{
        int i = 0;

        return FALSE;
}

BOOL SCANEAN::Read (void)
{
        char buffer [512];
        char *etc;

        if (CfgName.GetLen () == 0)
        {
            return FALSE;
        }

        etc = getenv ("BWSETC");
        if (etc == NULL)
        {
            return FALSE;
        }

        sprintf (buffer, "%s\\%s", etc, CfgName.GetBuffer ());

        FILE *fp = fopen (buffer, "r");
        if (fp == NULL)
        {
            return FALSE;
        }

        while (fgets (buffer, 511, fp))
        {
            if (memcmp (buffer, "[Standard]", strlen ("[Standard]")) == 0)
            {
                if (ReadStandard (fp, buffer) == NULL)
                {
                    break;
                }
            }
            if (memcmp (buffer, "[EAN128]", strlen ("[EAN128]")) == 0)
            {
                if (ReadEan128 (fp, buffer) == NULL)
                {
                    break;
                }
            }
        }
        fclose (fp);
        return TRUE;
}

int SCANEAN::GetType (Text& Ean)
{
        Text EanCode = Ean.SubString (0, 2);
        short Code = atoi (EanCode.GetBuffer ());
        for (int i = 0; Structean[i] != NULL; i ++)
        {
            if (Code == Structean[i]->GetCode ())
            {
                break;
            }
        }
        if (Structean[i] == NULL)
        {
             return EANSTD;
        }
        Text Type = Structean[i]->GetType ();
        switch ((Type.GetBuffer ())[0])
        {
             case  'G' :
                 return EANGEW;
             case  'S' :
                 return EANSTK;
             case 'P' :
                 return EANPR;
             case 'L' :
                 return EANBON;
             default :
                 return EANSTD;
        }
}

STRUCTEAN *SCANEAN::GetStructean (Text& Ean)
{
        Ean.TrimLeft ();
		if (FixLen > 0 && Ean.GetLen () < FixLen)
		{
			return NULL;
		}
        Text EanCode = Ean.SubString (0, 2);
        short Code = atoi (EanCode.GetBuffer ());
        for (int i = 0; Structean[i] != NULL; i ++)
        {
            if (Code == Structean[i]->GetCode ())
            {
				if ((Structean[i]->GetType () == "G" ||
					 Structean[i]->GetType () == "S") &&
					Ean.GetLen () < 12)
				{
					return NULL;
				}
                return Structean[i];
            }
        }
        return NULL;
}

int SCANEAN::GetLen (Text& Ean)
{
        Text EanCode = Ean.SubString (0, 2);
        short Code = atoi (EanCode.GetBuffer ());
        for (int i = 0; Structean[i] != NULL; i ++)
        {
            if (Code == Structean[i]->GetCode ())
            {
                break;
            }
        }
        if (Structean[i] == NULL)
        {
             return 0;
        }
        return Structean[i]->GetLen ();
}

int SCANEAN::GetStartPos (Text& Ean)
{
        Text EanCode = Ean.SubString (0, 2);
        short Code = atoi (EanCode.GetBuffer ());
        for (int i = 0; Structean[i] != NULL; i ++)
        {
            if (Code == Structean[i]->GetCode ())
            {
                break;
            }
        }
        if (Structean[i] == NULL)
        {
             return 0;
        }
        return Structean[i]->GetAFromEan ();
}

BOOL SCANEAN::GetTestLen13 (Text& Ean)
{
        Text EanCode = Ean.SubString (0, 2);
        short Code = atoi (EanCode.GetBuffer ());
        for (int i = 0; Structean[i] != NULL; i ++)
        {
            if (Code == Structean[i]->GetCode ())
            {
                break;
            }
        }
        if (Structean[i] == NULL)
        {
             return 0;
        }
        return Structean[i]->GetPrFromEan ();
}

long SCANEAN::GetBonNr (Text& Ean)
{
	    if (FixLen > 0 && Ean.GetLen () < FixLen && GetTestLen13 (Ean))
		{
			return 0l;
		}
	    int Len   = GetLen (Ean);
        int Start = GetStartPos (Ean);
        Text BonNr = Ean.SubString (Start, Len);
		return atol (BonNr.GetBuffer ());
}


int SCANEAN::GetCode (Text& Ean)
{
        Text EanCode = Ean.SubString (0, 2);
        return atoi (EanCode.GetBuffer ());
}

char *SCANEAN::GetEanForDb (Text Ean)
{

        Text EanRest; 
        char format[10];

        int Len = GetLen (Ean);  
        if (Len == 0)
        {
            return Ean.GetBuffer ();
        }

        sprintf (format, "%c0%dd", '%', 10 - Len);
        EanDb = Ean.SubString (0, 2 + Len);
        EanRest.Format (format, 0);        
        EanDb + EanRest.GetBuffer ();;
        return EanDb.GetBuffer ();
}
        
char *SCANEAN::GetEanGew (Text Ean)
{

        EanGew = Ean.SubString (7, 5);
        return EanGew.GetBuffer ();
}


short SCANEAN::QuerSumme (short ziffer)
{
	     if (ziffer < 10)
		 {
			 return ziffer;
		 }

		 Text z;
		 z.Format ("%hd", ziffer);
		 short z1 = atoi (z.SubString (0, 1));
		 short z2 = atoi (z.SubString (1, 1));
		 return z1 + z2;
}

Text& SCANEAN::AddPlz (Text& art)
{
		int len = art.GetLen ();
        short summe = 0;
        for (int i = 0; i < len; i ++)
        {
                    short ziffer = art.GetBuffer () [i] - 0x30;          
                    if ((i % 2) == 0)
                    {
						     ziffer = QuerSumme (ziffer * 2);
                             summe += ziffer;
                    }
                    else
                    {
                             summe += ziffer;
                    }
         }
         short rest = summe % 10;
         short ziffer = (10 - rest) % 10; 
		 art.Format ("%s%hd",art.GetBuffer (), ziffer);
		 return art;
}

	    

