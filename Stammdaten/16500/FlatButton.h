#pragma once
#include "afxwin.h"
#include "atlimage.h"

class CFlatButton :
	public CButton
{
public:
	CImage image;
	CFlatButton(void);
	~CFlatButton(void);
	void SetBitmap (HBITMAP hBitmap);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct); 
};
