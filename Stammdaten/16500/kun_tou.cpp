#include "stdafx.h"
#include "kun_tou.h"

struct KUN_TOU kun_tou, kun_tou_null, kun_tou_def;

void KUN_TOU_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)  &kun_tou.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &kun_tou.kun,  SQLLONG, 0);
    sqlout ((short *) &kun_tou.mdn,SQLSHORT,0);
    sqlout ((long *) &kun_tou.kun,SQLLONG,0);
    sqlout ((long *) &kun_tou.tou,SQLLONG,0);
    sqlout ((char *) kun_tou.tou_bz,SQLCHAR,49);
    sqlout ((long *) &kun_tou.tou2,SQLLONG,0);
    sqlout ((char *) kun_tou.tou_bz2,SQLCHAR,49);
            cursor = sqlcursor (_T("select kun_tou.mdn,  "
"kun_tou.kun,  kun_tou.tou,  kun_tou.tou_bz,  kun_tou.tou2,  "
"kun_tou.tou_bz2 from kun_tou ")


                                  _T("where mdn = ? and kun = ?"));
    sqlin ((short *) &kun_tou.mdn,SQLSHORT,0);
    sqlin ((long *) &kun_tou.kun,SQLLONG,0);
    sqlin ((long *) &kun_tou.tou,SQLLONG,0);
    sqlin ((char *) kun_tou.tou_bz,SQLCHAR,49);
    sqlin ((long *) &kun_tou.tou2,SQLLONG,0);
    sqlin ((char *) kun_tou.tou_bz2,SQLCHAR,49);
            sqltext = _T("update kun_tou set kun_tou.mdn = ?,  "
"kun_tou.kun = ?,  kun_tou.tou = ?,  kun_tou.tou_bz = ?,  "
"kun_tou.tou2 = ?,  kun_tou.tou_bz2 = ? ")


                                  _T("where mdn = ? and kun = ?");
            sqlin ((short *)  &kun_tou.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &kun_tou.kun,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)  &kun_tou.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &kun_tou.kun,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select kun from kun_tou ")
                                  _T("where mdn = ? and kun = ?"));

            sqlin ((short *)  &kun_tou.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &kun_tou.kun,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from kun_tou ")
                                  _T("where mdn = ? and kun = ?"));
    sqlin ((short *) &kun_tou.mdn,SQLSHORT,0);
    sqlin ((long *) &kun_tou.kun,SQLLONG,0);
    sqlin ((long *) &kun_tou.tou,SQLLONG,0);
    sqlin ((char *) kun_tou.tou_bz,SQLCHAR,49);
    sqlin ((long *) &kun_tou.tou2,SQLLONG,0);
    sqlin ((char *) kun_tou.tou_bz2,SQLCHAR,49);
            ins_cursor = sqlcursor (_T("insert into kun_tou ("
"mdn,  kun,  tou,  tou_bz,  tou2,  tou_bz2) ")


                                      _T("values ")
                                      _T("(?,?,?,?,?,?)"));


}
