#pragma once
#include "DbPropertyPage.h"
#include "DbPropertySheet.h"
#include "resource.h"
#include "ListDropTarget.h"
#include "CtrlGrid.h"
#include "Kun.h"
#include "kun_erw.h"
#include "Mdn.h"
#include "Adr.h"
#include "afxwin.h"
#include "FormTab.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "ChoiceAdr.h"
#include "ChoiceX.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "DbFormView.h"
#include "CodeProperties.h"
#include "mo_progcfg.h"
#include "sys_par.h"
#include "ptabn.h"
#include "iprgrstufk.h"
#include "i_kun_prk.h"
#include "kst.h"
#include "kun_tou.h"
#include "fil.h"
#include "gr_zuord.h"
#include "zahl_kond.h"
#include "rab_stufek.h"
#include "haus_bank.h"
#include "ls_txt.h"
#include "zuschlag.h"
#include "AdrList.h"
#include "AdrList.h"
#include "StaticButton.h"
#include "EnterPlz.h"


// CKunPage2-Dialogfeld

#define IDC_ADR2CHOICE 4004
#define IDC_TOURCHOICE1 4005
#define IDC_TOURCHOICE2 4006
#define IDC_PLZ2CHOICE 4007

class CKunPage2 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CKunPage2)

public:
	CKunPage2();
	virtual ~CKunPage2();

// Dialogfelddaten
	enum { IDD = IDD_KUNPAGE2 };
	enum
	{
		KunAdrType = 20,
		KunAdrLiefType = 21,
		KunAdrRechType = 22,
		KunAdrDivType = 25
	};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	afx_msg void OnInfo();

    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	PROG_CFG Cfg;

	virtual BOOL OnInitDialog();
	void FillAnr ();



public:

	KUN_CLASS *Kun;
	KUN_ERW_CLASS *Kun_erw;
	ADR_CLASS *KunAdr;
	MDN_CLASS *Mdn;
	ADR_CLASS *MdnAdr;
	ADR_CLASS *LiefAdr;
	PTABN_CLASS *Ptabn;
	IPRGRSTUFK_CLASS *Iprgrstufk;
	I_KUN_PRK_CLASS *I_kun_prk;
	KST_CLASS *Kst;
	SYS_PAR_CLASS *Sys_par;
	KUN_TOU_CLASS *Kun_tou;
	FIL_CLASS *Fil;
	GR_ZUORD_CLASS *Gr_zuord;
	ZAHL_KOND_CLASS *Zahl_kond;
	RAB_STUFEK_CLASS *Rab_stufek;
	HAUS_BANK_CLASS *Haus_bank;
	LS_TXT_CLASS *Ls_txt;
	ZUSCHLAG_CLASS *Zuschlag;
	long tou;
	char tou_bz[49];
	int cursor_tour;

	CEnterPlz EnterPlz;
	CFormTab Form;

	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid KunGrid;
	CCtrlGrid Adr1Grid;
	CCtrlGrid TelFaxGrid;
	CCtrlGrid PLZPostfachGrid;
	CCtrlGrid BbnGrid;
	CCtrlGrid TourGrid;
	CCtrlGrid Tour2Grid;
	CCtrlGrid LiefStopGrid;
	CCtrlGrid FilKZGrid;
	CCtrlGrid Plz1Grid;
		
	CListDropTarget dropTarget;

    CPropertySheet *KunProperty;
    DbFormView *View;
	CFrameWnd *Frame;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	void Register ();
	CEdit m_MdnName;
	CStatic m_LKun;
	CNumEdit m_Kun;
	CButton m_KunChoice;
	CEdit m_KunName;
	CStatic m_HeadBorder;
	CStatic m_DataBorder;
	CStatic m_LAnr;
	CComboBox m_Anr;
	CStatic m_LAdr1;
	CNumEdit m_Adr1;
	CButton m_Adr1Choice;
	CStaticButton m_KunLief;
	
    CVector HeadControls;
	BOOL ChoiceStat;
	CString Search;

	CChoiceAdr *ChoiceAdr;
	BOOL ModalChoiceAdr;
	CCodeProperties Code;
    BOOL OnReturn ();
    BOOL OnKeyup ();
	void OnDelete ();
	void FillPtabCombo (CWnd *Control, LPTSTR Item);
	void FillGrZuord ();
	void FillFil ();
	void OnAdr2Choice();
	void OnPlz2Choice();
	void OnAdrCanceled();
	BOOL TestAdr2Type ();
	BOOL OnAdrSelectedEx();
	void OnAdrSelected();
	void OnKunLief();
	void OnTourChoice1();
	void OnTourChoice2();
	BOOL TestTour (int pos);
	BOOL TestFields ();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
    void EnableHeadControls (BOOL enable);
    BOOL StepBack ();
	virtual void OnCopy ();
    virtual void OnPaste ();
	BOOL Copy ();
	BOOL Paste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
    void EnableFields (BOOL b);
	void UpdatePage ();
	afx_msg void OnKillFocus (CWnd *);
	CStatic m_LName1;
	CTextEdit m_Name1;
public:
	CTextEdit m_Name2;
public:
	CStatic m_LName2;
public:
	CTextEdit m_Name3;
public:
	CStatic m_LName3;
public:
	CTextEdit m_Strasse;
public:
	CStatic m_LStrasse;
public:
	CNumEdit m_Plz;
	CButton m_Plz2Choice;
public:
	CStatic m_LPlz;
public:
	CTextEdit m_Ort;
public:
	CStatic m_LOrt;
public:
	CStatic m_LPostfach;
public:
	CNumEdit m_Postfach;
public:
	CNumEdit m_Telefon;
public:
	CStatic m_LTelefon;
public:
	CNumEdit m_Fax;
	public:
	CStatic m_LKunKrz1;
public:
	CTextEdit m_KunKrz1;
public:
	CNumEdit m_plzPostfach;
public:
	CNumEdit m_EMAIL;
public:
	CStatic m_LEmail;
public:
	CTextEdit m_Partner;
public:
	CStatic m_LPartner;
public:
	CComboBox m_Staat;
public:
	CStatic m_LStaat;
public:
	CComboBox m_Land;
	CButton m_IncludeLand;
	BOOL IncludeLand;
public:
	CStatic m_LLand;

public:
	CStatic m_DataBorder2;
public:
	CNumEdit m_Modem;
public:
	CStatic m_LModem;

public:
	CButton m_LiefStop;
public:
	CStatic m_LLiefStop;
public:
	CNumEdit m_Iln;
public:
	CStatic m_LIln;
public:
	CNumEdit m_Iln2;
public:
	CStatic m_LIln2;
public:
	CStatic m_LBbn;
public:
	CNumEdit m_Bbn;
public:
	CNumEdit m_Bbs;
public:
	CStatic m_LUmsatz;
public:
	CNumEdit m_Umsatz;
public:
	CStatic m_LTour1;
public:
	CNumEdit m_Tour1;
	CButton m_TourChoice1;
public:
	CNumEdit m_Tour2;
	CButton m_TourChoice2;
public:
	CStatic m_LTour2;
public:
	CTextEdit m_Tour1Bez;
public:
	CTextEdit m_Tour2Bez;
public:
	CComboBox m_Versand;
public:
	CStatic m_LVersand;
public:
	CComboBox m_Markt;
public:
	CStatic m_LMarkt;
public:
	afx_msg void OnCbnSelchangeVersand();
public:
	CComboBox m_PrAusw;
public:
	CStatic m_LPreisAusw;
public:
	CComboBox m_FrachtGew;
public:
	CStatic m_LFrachtGew;
public:
	CButton m_FilKZ;
public:
	CStatic m_LFilKZ;
public:
	CStatic m_LFilGr;
public:
	CComboBox m_FilGr;

public:
	CStatic m_LFilNr;
public:
	CComboBox m_FilNr;
public:
	afx_msg void OnCbnSelchangeFilgr();
public:
	afx_msg void OnBnClickedFilkz();
public:
	CEdit m_LiefStopBez;
public:
	afx_msg void OnBnClickedLiefstop();
public:
	CEdit m_FilKzBez;
void OnMoreFrachtKosten();
public:
	afx_msg void OnCbnSelchangeFracht();
	afx_msg void OnEnChangeKunkrz1();
	CStatic m_LShop_kz;
	CButton m_Shop_kz;
	CEdit m_Shop_passw;
	CStatic m_LShop_passw;
	afx_msg void OnBnClickedShopKz();
	afx_msg void OnBnClickedShopHistory();
	CStatic m_Databorder_inet;
public:
	CButton m_Shop_History;
public:
	CStatic m_LShop_History;
	CStatic m_LMinBest;
	CButton m_MinBest;
	afx_msg void OnBnClickedMinbest();
	CNumEdit m_PalH;
	CStatic m_LPalH;
	CEdit m_partner2;
	CStatic m_LPartner2;
	CStatic m_LMaxGewDD;
	CStatic m_LMaxGewEU;
	CNumEdit m_MaxGewDD;
	CNumEdit m_MaxGewEU;
};




