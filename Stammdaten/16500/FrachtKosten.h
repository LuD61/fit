#pragma once
#include "afxwin.h"
#include "KunPage.h"
#include "CtrlGrid.h"
#include "Kun.h"
#include "kun_erw.h"
#include "Mdn.h"
#include "Adr.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "FormTab.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "DbFormView.h"
#include "CodeProperties.h"
#include "mo_progcfg.h"
#include "sys_par.h"
#include "ptabn.h"
#include "iprgrstufk.h"
#include "i_kun_prk.h"
#include "kst.h"
#include "kun_tou.h"
#include "fil.h"
#include "gr_zuord.h"
#include "zahl_kond.h"
#include "rab_stufek.h"
#include "haus_bank.h"
#include "Zuschlag.h"
#include "NumEdit.h"
#include "KunPage2.h"



// CFrachtKosten-Dialogfeld

class CFrachtKosten : public CDialog
{
	DECLARE_DYNAMIC(CFrachtKosten)

public:
	CFrachtKosten(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CFrachtKosten();

// Dialogfelddaten
	enum { IDD = IDD_FRACHT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	PROG_CFG Cfg;


	virtual BOOL OnInitDialog();




public:

	ZUSCHLAG_CLASS *Zuschlag;

	CFormTab Form;

	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	DbFormView *View;
	CFrameWnd *Frame;

	CListDropTarget dropTarget;

	CVector HeadControls;
	BOOL ChoiceStat;
	CString Search;

	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceKun *Choice;
	BOOL ModalChoice;
	CCodeProperties Code;




	BOOL OnReturn ();
    BOOL OnKeyup ();
    BOOL ReadMdn ();
	BOOL Read ();
	afx_msg void OnKillFocus (CWnd *);

	void OnOK();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
    void EnableHeadControls (BOOL enable);
    BOOL StepBack ();
	BOOL Copy ();
	BOOL Paste ();
    void EnableFields (BOOL b);
	
	CNumEdit m_Grenz1;
public:
	CNumEdit m_Grenz2;
public:
	CNumEdit m_Grenz3;
public:
	CNumEdit m_Grenz4;
public:
	CNumEdit m_Proz1;
public:
	CNumEdit m_Proz2;
public:
	CNumEdit m_Proz3;
public:
	CNumEdit m_Proz4;
public:
	CStatic m_LGrenz;
public:
	CStatic m_LProz;
public:
	CButton m_BOK;
};
