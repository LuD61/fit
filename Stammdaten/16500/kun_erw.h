#ifndef _KUN_ERW_DEF
#define _KUN_ERW_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct KUN_ERW {
   short          mdn;
   short          fil;
   long           kun;
   long           m2laden;
   short          werbung;
   short          hoehe_pal;
   short          kun_kl;
   DATE_STRUCT    mandseit;
   TCHAR          sambuch[2];
   short          best_profil;
   double         max_gew_dd;
   double         max_gew_eu;
   short          verladeschein;
};
extern struct KUN_ERW kun_erw, kun_erw_null;

#line 8 "kun_erw.rh"

class KUN_ERW_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               KUN_ERW kun_erw;  
               KUN_ERW_CLASS () : DB_CLASS ()
               {
               }
};
#endif
