#include "stdafx.h"
#include "iprgrstufk.h"

struct IPRGRSTUFK iprgrstufk, iprgrstufk_null;

void IPRGRSTUFK_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &iprgrstufk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &iprgrstufk.pr_gr_stuf, SQLLONG, 0);
    sqlout ((short *) &iprgrstufk.mdn,SQLSHORT,0);
    sqlout ((long *) &iprgrstufk.pr_gr_stuf,SQLLONG,0);
    sqlout ((char *) iprgrstufk.zus_bz,SQLCHAR,25);
    sqlout ((DATE_STRUCT *) &iprgrstufk.gue_ab,SQLDATE,0);
    sqlout ((short *) &iprgrstufk.delstatus,SQLSHORT,0);
    sqlout ((short *) &iprgrstufk.waehrung,SQLSHORT,0);
            cursor = sqlcursor (_T("select iprgrstufk.mdn,  "
"iprgrstufk.pr_gr_stuf,  iprgrstufk.zus_bz,  iprgrstufk.gue_ab,  "
"iprgrstufk.delstatus,  iprgrstufk.waehrung from iprgrstufk ")

#line 13 "Iprgrstufk.rpp"
                                  _T("where mdn = ? ")
				  _T("and pr_gr_stuf = ?"));
    sqlin ((short *) &iprgrstufk.mdn,SQLSHORT,0);
    sqlin ((long *) &iprgrstufk.pr_gr_stuf,SQLLONG,0);
    sqlin ((char *) iprgrstufk.zus_bz,SQLCHAR,25);
    sqlin ((DATE_STRUCT *) &iprgrstufk.gue_ab,SQLDATE,0);
    sqlin ((short *) &iprgrstufk.delstatus,SQLSHORT,0);
    sqlin ((short *) &iprgrstufk.waehrung,SQLSHORT,0);
            sqltext = _T("update iprgrstufk set "
"iprgrstufk.mdn = ?,  iprgrstufk.pr_gr_stuf = ?,  "
"iprgrstufk.zus_bz = ?,  iprgrstufk.gue_ab = ?,  "
"iprgrstufk.delstatus = ?,  iprgrstufk.waehrung = ? ")

#line 16 "Iprgrstufk.rpp"
                                  _T("where mdn = ? ")
				  _T("and pr_gr_stuf = ?");
            sqlin ((short *)   &iprgrstufk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &iprgrstufk.pr_gr_stuf, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &iprgrstufk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &iprgrstufk.pr_gr_stuf, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select pr_gr_stuf from iprgrstufk ")
                                  _T("where mdn = ? ")
				  _T("and pr_gr_stuf = ?"));
            sqlin ((short *)   &iprgrstufk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &iprgrstufk.pr_gr_stuf, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from iprgrstufk ")
                                  _T("where mdn = ? ")
				  _T("and pr_gr_stuf = ?"));
    sqlin ((short *) &iprgrstufk.mdn,SQLSHORT,0);
    sqlin ((long *) &iprgrstufk.pr_gr_stuf,SQLLONG,0);
    sqlin ((char *) iprgrstufk.zus_bz,SQLCHAR,25);
    sqlin ((DATE_STRUCT *) &iprgrstufk.gue_ab,SQLDATE,0);
    sqlin ((short *) &iprgrstufk.delstatus,SQLSHORT,0);
    sqlin ((short *) &iprgrstufk.waehrung,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into iprgrstufk ("
"mdn,  pr_gr_stuf,  zus_bz,  gue_ab,  delstatus,  waehrung) ")

#line 33 "Iprgrstufk.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?)")); 

#line 35 "Iprgrstufk.rpp"
}
