// ChildFrm.cpp : Implementierung der Klasse CChildFrame
//
#include "stdafx.h"
#include "16500.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "KunView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RECCHANGE                       WM_USER + 2000

CVector CChildFrame::KunWnd;

// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	ON_WM_SIZE ()
	ON_WM_DESTROY ()
	ON_WM_PAINT ()
//	ON_COMMAND(ID_TEXT_CENT, OnTextCent)
//	ON_COMMAND(ID_TEXT_RIGHT, OnTextRight)
//	ON_COMMAND(ID_TEXT_LEFT, OnTextLeft)
	ON_COMMAND (RECCHANGE, OnRecChange)
//	ON_COMMAND (ID_DLG1, OnDlg1)
END_MESSAGE_MAP()


// CChildFrame Erstellung/Zerst�rung

CChildFrame::CChildFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
	InitSize = FALSE;
	KunWnd.Init ();
}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::Create(LPCTSTR lpszClassName,LPCTSTR lpszWindowName,
						DWORD dwStyle, RECT& rect,CMDIFrameWnd* pParentWnd,
						CCreateContext* pContext)
{
	return CMDIChildWnd::Create (lpszClassName, lpszWindowName, dwStyle,
						   rect, pParentWnd, pContext);
}



BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie die Fensterklasse oder die Stile hier, indem Sie CREATESTRUCT �ndern
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	if (cs.cy > 0)
	{
		cs.cy = 1000;
	}
	return TRUE;
}

BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	lpcs->cy = 500;
	BOOL ret = CMDIChildWnd::OnCreateClient(lpcs,   pContext);	

	CStringA ClassName = pContext->m_pNewViewClass->m_lpszClassName;
	if (ClassName == "CKunView")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->KunWnd == NULL)
		{
			MainFrm->KunWnd = this;
		}
		else
		{
			KunWnd.Add (this);
		}
	}
	MDIMaximize ();
	return ret;
}

void CChildFrame::OnSize(UINT nType, int cx, int cy)
{
	static BOOL InitPatView;

	CMDIChildWnd::OnSize (nType, cx, cy);
	if (!InitSize)
	{
		    CRect pRect;
			GetParent ()->GetClientRect (pRect);
			CRect rect;
			GetWindowRect (&rect);
			GetParent ()->ScreenToClient (&rect);
			rect.bottom = pRect.bottom;
			rect.right += 55;
			rect.top = 0;
			InitSize = TRUE;
			MoveWindow (&rect, TRUE);
	}
}

// CChildFrame Diagnose

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// CChildFrame Meldungshandler

void CChildFrame::OnTextCent()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    DbFormView *view = (DbFormView *) GetActiveView ();
	view->TextCent ();
}

void CChildFrame::OnTextRight()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    DbFormView *view = (DbFormView *) GetActiveView ();
	view->TextRight ();
}

void CChildFrame::OnTextLeft()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    DbFormView *view = (DbFormView *) GetActiveView ();
	view->TextLeft ();
}

void CChildFrame::OnDestroy()
{
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	if (MainFrm->KunWnd == this)
	{
		MainFrm->KunWnd = NULL;
		if (KunWnd.GetCount () != 0)
		{
			MainFrm->KunWnd = (CWnd *) KunWnd.Get (0);
			KunWnd.Drop (0);
		}
		return;
	}
	int i = -1;
	if ((i = KunWnd.Find (this)) != -1)
	{
		KunWnd.Drop (i);
	}
}

void CChildFrame::OnDlg1 ()
{
	int eins = 1;
}

void CChildFrame::OnRecChange()
{
/*
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	if (MainFrm->KunWnd != NULL)
	{
		CKunView *view = (CKunView *) ((CChildFrame *) MainFrm->KunWnd)->GetActiveView ();
		if (view != NULL)
		{
			view->OnRecChange ();
		}
	}
*/
}
