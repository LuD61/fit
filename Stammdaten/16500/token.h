/***************************************************************************/
/* Programmname :  Token.h                                                 */
/*-------------------------------------------------------------------------*/
/* Funktion :  Klasse CToken                                               */
/*-------------------------------------------------------------------------*/
/* Revision : 1.0    Datum : 07.06.04   erstellt von : W. Roth             */
/*                   allgemneine Klasse zum Zerlegen von Strings           */
/*                                                                         */    
/*-------------------------------------------------------------------------*/
/* Aufruf :                                                                */
/* Funktionswert :                                                         */
/* Eingabeparameter :                                                      */
/*                                                                         */
/* Ausgabeparameter :                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
#ifndef TOKEN_DEF
#define TOKEN_DEF
#include <string.h> 
#include <stdarg.h> 

class CToken
{
  private :
     CString Buffer;
     CString **Tokens;
     CString sep;
     int AnzToken;
     int AktToken;
  public :
     CToken ();
     CToken (LPTSTR, LPTSTR);
     CToken (CString&, LPTSTR);
     ~CToken ();
     const CToken& operator=(LPTSTR);
     const CToken& operator=(CString&);
     void GetTokens (LPTSTR);
     void SetSep (LPTSTR);
     LPTSTR NextToken (void);
     LPTSTR GetToken (int);
     int GetAnzToken (void);
};
#endif
