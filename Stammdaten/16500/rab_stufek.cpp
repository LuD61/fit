#include "stdafx.h"
#include "rab_stufek.h"

struct RAB_STUFEK rab_stufek, rab_stufek_null;

void RAB_STUFEK_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &rab_stufek.mdn, SQLSHORT, 0);
            sqlin ((short *)   &rab_stufek.fil, SQLSHORT, 0);
            sqlin ((char *)    rab_stufek.rab_nr, SQLCHAR, sizeof (rab_stufek.rab_nr));
            sqlin ((char *)    rab_stufek.rab_typ, SQLCHAR, sizeof (rab_stufek.rab_typ));
    sqlout ((short *) &rab_stufek.mdn,SQLSHORT,0);
    sqlout ((short *) &rab_stufek.fil,SQLSHORT,0);
    sqlout ((long *) &rab_stufek.rab_nr_num,SQLLONG,0);
    sqlout ((char *) rab_stufek.rab_nr,SQLCHAR,9);
    sqlout ((char *) rab_stufek.rab_bz_b,SQLCHAR,49);
    sqlout ((double *) &rab_stufek.smt_rab,SQLDOUBLE,0);
    sqlout ((double *) &rab_stufek.rech_rab,SQLDOUBLE,0);
    sqlout ((char *) rab_stufek.sa_rab,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &rab_stufek.ae_dat,SQLDATE,0);
    sqlout ((char *) rab_stufek.rab_typ,SQLCHAR,3);
    sqlout ((short *) &rab_stufek.delstatus,SQLSHORT,0);
    sqlout ((char *) rab_stufek.rab_teil_smt,SQLCHAR,2);
    sqlout ((char *) rab_stufek.rab_me_einh,SQLCHAR,2);
    sqlout ((short *) &rab_stufek.waehrung,SQLSHORT,0);
            cursor = sqlcursor (_T("select rab_stufek.mdn,  "
"rab_stufek.fil,  rab_stufek.rab_nr_num,  rab_stufek.rab_nr,  "
"rab_stufek.rab_bz_b,  rab_stufek.smt_rab,  rab_stufek.rech_rab,  "
"rab_stufek.sa_rab,  rab_stufek.ae_dat,  rab_stufek.rab_typ,  "
"rab_stufek.delstatus,  rab_stufek.rab_teil_smt,  "
"rab_stufek.rab_me_einh,  rab_stufek.waehrung from rab_stufek ")


				  _T("where mdn = ?")
				  _T("and fil = ?")
				  _T("and rab_nr = ?")
				  _T("and rab_typ = ?"));
    sqlin ((short *) &rab_stufek.mdn,SQLSHORT,0);
    sqlin ((short *) &rab_stufek.fil,SQLSHORT,0);
    sqlin ((long *) &rab_stufek.rab_nr_num,SQLLONG,0);
    sqlin ((char *) rab_stufek.rab_nr,SQLCHAR,9);
    sqlin ((char *) rab_stufek.rab_bz_b,SQLCHAR,49);
    sqlin ((double *) &rab_stufek.smt_rab,SQLDOUBLE,0);
    sqlin ((double *) &rab_stufek.rech_rab,SQLDOUBLE,0);
    sqlin ((char *) rab_stufek.sa_rab,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &rab_stufek.ae_dat,SQLDATE,0);
    sqlin ((char *) rab_stufek.rab_typ,SQLCHAR,3);
    sqlin ((short *) &rab_stufek.delstatus,SQLSHORT,0);
    sqlin ((char *) rab_stufek.rab_teil_smt,SQLCHAR,2);
    sqlin ((char *) rab_stufek.rab_me_einh,SQLCHAR,2);
    sqlin ((short *) &rab_stufek.waehrung,SQLSHORT,0);
            sqltext = _T("update rab_stufek set "
"rab_stufek.mdn = ?,  rab_stufek.fil = ?,  "
"rab_stufek.rab_nr_num = ?,  rab_stufek.rab_nr = ?,  "
"rab_stufek.rab_bz_b = ?,  rab_stufek.smt_rab = ?,  "
"rab_stufek.rech_rab = ?,  rab_stufek.sa_rab = ?,  "
"rab_stufek.ae_dat = ?,  rab_stufek.rab_typ = ?,  "
"rab_stufek.delstatus = ?,  rab_stufek.rab_teil_smt = ?,  "
"rab_stufek.rab_me_einh = ?,  rab_stufek.waehrung = ? ")


				  _T("where mdn = ?")
				  _T("and fil = ?")
				  _T("and rab_nr = ?")
				  _T("and rab_typ = ?");
            sqlin ((short *)   &rab_stufek.mdn, SQLSHORT, 0);
            sqlin ((short *)   &rab_stufek.fil, SQLSHORT, 0);
            sqlin ((char *)    rab_stufek.rab_nr, SQLCHAR, sizeof (rab_stufek.rab_nr));
            sqlin ((char *)    rab_stufek.rab_typ, SQLCHAR, sizeof (rab_stufek.rab_typ));
            upd_cursor = sqlcursor (sqltext);


            sqlin ((short *)   &rab_stufek.mdn, SQLSHORT, 0);
            sqlin ((short *)   &rab_stufek.fil, SQLSHORT, 0);
            sqlin ((char *)    rab_stufek.rab_nr, SQLCHAR, sizeof (rab_stufek.rab_nr));
            sqlin ((char *)    rab_stufek.rab_typ, SQLCHAR, sizeof (rab_stufek.rab_typ));
            test_upd_cursor = sqlcursor (_T("select rab_nr from rab_stufek ")
				  _T("where mdn = ?")
				  _T("and fil = ?")
				  _T("and rab_nr = ?")
				  _T("and rab_typ = ?"));

            sqlin ((short *)   &rab_stufek.mdn, SQLSHORT, 0);
            sqlin ((short *)   &rab_stufek.fil, SQLSHORT, 0);
            sqlin ((char *)    rab_stufek.rab_nr, SQLCHAR, sizeof (rab_stufek.rab_nr));
            sqlin ((char *)    rab_stufek.rab_typ, SQLCHAR, sizeof (rab_stufek.rab_typ));
            del_cursor = sqlcursor (_T("delete from rab_stufek ")
				  _T("where mdn = ?")
				  _T("and fil = ?")
				  _T("and rab_nr = ?")
				  _T("and rab_typ = ?"));
    sqlin ((short *) &rab_stufek.mdn,SQLSHORT,0);
    sqlin ((short *) &rab_stufek.fil,SQLSHORT,0);
    sqlin ((long *) &rab_stufek.rab_nr_num,SQLLONG,0);
    sqlin ((char *) rab_stufek.rab_nr,SQLCHAR,9);
    sqlin ((char *) rab_stufek.rab_bz_b,SQLCHAR,49);
    sqlin ((double *) &rab_stufek.smt_rab,SQLDOUBLE,0);
    sqlin ((double *) &rab_stufek.rech_rab,SQLDOUBLE,0);
    sqlin ((char *) rab_stufek.sa_rab,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &rab_stufek.ae_dat,SQLDATE,0);
    sqlin ((char *) rab_stufek.rab_typ,SQLCHAR,3);
    sqlin ((short *) &rab_stufek.delstatus,SQLSHORT,0);
    sqlin ((char *) rab_stufek.rab_teil_smt,SQLCHAR,2);
    sqlin ((char *) rab_stufek.rab_me_einh,SQLCHAR,2);
    sqlin ((short *) &rab_stufek.waehrung,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into rab_stufek ("
"mdn,  fil,  rab_nr_num,  rab_nr,  rab_bz_b,  smt_rab,  rech_rab,  sa_rab,  ae_dat,  "
"rab_typ,  delstatus,  rab_teil_smt,  rab_me_einh,  waehrung) ")


                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?)")); 


}

