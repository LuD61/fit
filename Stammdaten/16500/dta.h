#ifndef _DTA_DEF
#define _DTA_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct DTA {
   long           dta;
   char           kun_krz1[17];
   long           adr;
   char           iln[17];
   short          typ;
   short          test_kz;
   short          waehrung;
   char           dat_name[81];
   long           nummer;
   short          pri_seg;
   short          reli;
};
extern struct DTA dta, dta_null;


class DTA_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               DTA dta;  
               DTA_CLASS () : DB_CLASS ()
               {
               }
};
#endif
