#include "stdafx.h"
#include "ChoiceLstxt.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceLstxt::Sort1 = -1;
int CChoiceLstxt::Sort2 = -1;
int CChoiceLstxt::Sort3 = -1;

CChoiceLstxt::CChoiceLstxt(CWnd* pParent) 
        : CChoiceX(pParent)
{
}

CChoiceLstxt::~CChoiceLstxt() 
{
	DestroyList ();
}

void CChoiceLstxt::DestroyList() 
{
	for (std::vector<CLstxtList *>::iterator pabl = LstxtList.begin (); pabl != LstxtList.end (); ++pabl)
	{
		CLstxtList *abl = *pabl;
		delete abl;
	}
    LstxtList.clear ();
}

void CChoiceLstxt::FillList () 
{
    long  nr;
	TCHAR txt [60];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Texte"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
	SetCol (_T("Nr."),  1, 50, LVCFMT_RIGHT);
    SetCol (_T("Text"),  2, 250);

	if (LstxtList.size () == 0)
	{
		DbClass->sqlout ((long *)&nr,      SQLLONG, 0);
		DbClass->sqlout ((LPTSTR)  txt,   SQLCHAR, sizeof (txt));
		int cursor = DbClass->sqlcursor (_T("select ls_txt.nr, ls_txt.txt ")
			                             _T("from ls_txt where ls_txt.nr > 0 "));
										 
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) txt;
			CDbUniCode::DbToUniCode (txt, pos);
			CLstxtList *abl = new CLstxtList (nr, txt);
			LstxtList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CLstxtList *>::iterator pabl = LstxtList.begin (); pabl != LstxtList.end (); ++pabl)
	{
		CLstxtList *abl = *pabl;
		CString Txt;
		Txt.Format (_T("%ld"), abl->nr); 
		CString Num;
		CString Bez;
		_tcscpy (txt, abl->txt.GetBuffer ());

		CString LText;
		LText.Format (_T("%ld %s"), abl->nr, 
									abl->txt.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Txt;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (txt, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort (listView);
}


void CChoiceLstxt::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceLstxt::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceLstxt::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

	for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLstxt::SearchAdrKrz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLstxt::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchAdrKrz (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceLstxt::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceLstxt::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   return 0;
}


void CChoiceLstxt::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CLstxtList *abl = LstxtList [i];
		   
		   abl->nr     = _tstoi (ListBox->GetItemText (i, 1));
		   abl->txt	   = ListBox->GetItemText (i, 2);
	}
}

void CChoiceLstxt::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = LstxtList [idx];
}

CLstxtList *CChoiceLstxt::GetSelectedText ()
{
	CLstxtList *abl = (CLstxtList *) SelectedRow;
	return abl;
}

