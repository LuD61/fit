#include "stdafx.h"
#include "zahl_kond.h"

struct ZAHL_KOND zahl_kond, zahl_kond_null, zahl_kond_def;

void ZAHL_KOND_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &zahl_kond.zahl_kond,  SQLSHORT, 0);
    sqlout ((double *) &zahl_kond.skto1,SQLDOUBLE,0);
    sqlout ((double *) &zahl_kond.skto2,SQLDOUBLE,0);
    sqlout ((double *) &zahl_kond.skto3,SQLDOUBLE,0);
    sqlout ((char *) zahl_kond.txt,SQLCHAR,61);
    sqlout ((short *) &zahl_kond.zahl_kond,SQLSHORT,0);
    sqlout ((short *) &zahl_kond.ziel1,SQLSHORT,0);
    sqlout ((short *) &zahl_kond.ziel2,SQLSHORT,0);
    sqlout ((short *) &zahl_kond.ziel3,SQLSHORT,0);
    sqlout ((char *) zahl_kond.zuord,SQLCHAR,2);
    sqlout ((short *) &zahl_kond.delstatus,SQLSHORT,0);
            cursor = sqlcursor (_T("select zahl_kond.skto1,  "
"zahl_kond.skto2,  zahl_kond.skto3,  zahl_kond.txt,  "
"zahl_kond.zahl_kond,  zahl_kond.ziel1,  zahl_kond.ziel2,  "
"zahl_kond.ziel3,  zahl_kond.zuord,  zahl_kond.delstatus from zahl_kond ")


                                  _T("where zahl_kond = ?"));
    sqlin ((double *) &zahl_kond.skto1,SQLDOUBLE,0);
    sqlin ((double *) &zahl_kond.skto2,SQLDOUBLE,0);
    sqlin ((double *) &zahl_kond.skto3,SQLDOUBLE,0);
    sqlin ((char *) zahl_kond.txt,SQLCHAR,61);
    sqlin ((short *) &zahl_kond.zahl_kond,SQLSHORT,0);
    sqlin ((short *) &zahl_kond.ziel1,SQLSHORT,0);
    sqlin ((short *) &zahl_kond.ziel2,SQLSHORT,0);
    sqlin ((short *) &zahl_kond.ziel3,SQLSHORT,0);
    sqlin ((char *) zahl_kond.zuord,SQLCHAR,2);
    sqlin ((short *) &zahl_kond.delstatus,SQLSHORT,0);
            sqltext = _T("update zahl_kond set "
"zahl_kond.skto1 = ?,  zahl_kond.skto2 = ?,  zahl_kond.skto3 = ?,  "
"zahl_kond.txt = ?,  zahl_kond.zahl_kond = ?,  zahl_kond.ziel1 = ?,  "
"zahl_kond.ziel2 = ?,  zahl_kond.ziel3 = ?,  zahl_kond.zuord = ?,  "
"zahl_kond.delstatus = ? ")


                                  _T("where zahl_kond = ?");
            sqlin ((short *)   &zahl_kond.zahl_kond,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &zahl_kond.zahl_kond,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select zahl_kond from zahl_kond ")
                                  _T("where zahl_kond = ? for update"));
            
            sqlin ((short *)   &zahl_kond.zahl_kond,  SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from zahl_kond ")
                                  _T("where zahl_kond = ?"));
    sqlin ((double *) &zahl_kond.skto1,SQLDOUBLE,0);
    sqlin ((double *) &zahl_kond.skto2,SQLDOUBLE,0);
    sqlin ((double *) &zahl_kond.skto3,SQLDOUBLE,0);
    sqlin ((char *) zahl_kond.txt,SQLCHAR,61);
    sqlin ((short *) &zahl_kond.zahl_kond,SQLSHORT,0);
    sqlin ((short *) &zahl_kond.ziel1,SQLSHORT,0);
    sqlin ((short *) &zahl_kond.ziel2,SQLSHORT,0);
    sqlin ((short *) &zahl_kond.ziel3,SQLSHORT,0);
    sqlin ((char *) zahl_kond.zuord,SQLCHAR,2);
    sqlin ((short *) &zahl_kond.delstatus,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into zahl_kond ("
"skto1,  skto2,  skto3,  txt,  zahl_kond,  ziel1,  ziel2,  ziel3,  zuord,  delstatus) ")


                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,"
"?,?)"));


}
