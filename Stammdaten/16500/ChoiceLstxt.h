#ifndef _CHOICELSTXT_DEF
#define _CHOICELSTXT_DEF

#include "ChoiceX.h"
#include "LstxtList.h"
#include <vector>

class CChoiceLstxt : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
      
    public :
		long m_Lstxt;
	    std::vector<CLstxtList *> LstxtList;
      	CChoiceLstxt(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceLstxt(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchAdrKrz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CLstxtList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
