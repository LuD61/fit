#pragma once

#include "DbPropertyPage.h"
#include "resource.h"
#include "ListDropTarget.h"
#include "CtrlGrid.h"
#include "Kun.h"
#include "kun_erw.h"
#include "Mdn.h"
#include "Adr.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "FormTab.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "DbFormView.h"
#include "CodeProperties.h"
#include "mo_progcfg.h"
#include "sys_par.h"
#include "ptabn.h"
#include "iprgrstufk.h"
#include "i_kun_prk.h"
#include "kst.h"
#include "kun_tou.h"
#include "fil.h"
#include "gr_zuord.h"
#include "zahl_kond.h"
#include "rab_stufek.h"
#include "haus_bank.h"
#include "ls_txt.h"
#include "dta.h"




// CKunPage5-Dialogfeld

class CKunPage5 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CKunPage5)

public:
	CKunPage5();
	virtual ~CKunPage5();

// Dialogfelddaten
	enum { IDD = IDD_KUNPAGE5 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_MESSAGE_MAP()

	HBRUSH hBrush;
	HBRUSH hBrushStatic;
	PROG_CFG Cfg;

	virtual BOOL OnInitDialog();



	public:

	KUN_CLASS *Kun;
	KUN_ERW_CLASS *Kun_erw;
	ADR_CLASS *KunAdr;
	MDN_CLASS *Mdn;
	ADR_CLASS *MdnAdr;
	PTABN_CLASS *Ptabn;
	IPRGRSTUFK_CLASS *Iprgrstufk;
	I_KUN_PRK_CLASS *I_kun_prk;
	KST_CLASS *Kst;
	SYS_PAR_CLASS *Sys_par;
	KUN_TOU_CLASS *Kun_tou;
	FIL_CLASS *Fil;
	GR_ZUORD_CLASS *Gr_zuord;
	ZAHL_KOND_CLASS *Zahl_kond;
	RAB_STUFEK_CLASS *Rab_stufek;
	HAUS_BANK_CLASS *Haus_bank;
	LS_TXT_CLASS *Ls_txt;
	DTA_CLASS *Dta;


	
	CFormTab Form;

	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid KunGrid;

		
	CListDropTarget dropTarget;

    CPropertySheet *KunProperty;
    DbFormView *View;
	CFrameWnd *Frame;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	void Register ();
	CEdit m_MdnName;
	CStatic m_LKun;
	CNumEdit m_Kun;
	CButton m_KunChoice;
	CEdit m_KunName;
	CStatic m_HeadBorder;
	CStatic m_DataBorder;
	CStatic m_LAnr;
	CComboBox m_Anr;
	CStatic m_LAdr1;
	CNumEdit m_Adr1;
	CButton m_KopfLSChoice;
	CButton m_FussLSChoice;
	CButton m_KopfRechChoice;
	CButton m_FussRechChoice;
	
    CVector HeadControls;
	BOOL ChoiceStat;
	CString Search;

	CCodeProperties Code;
    BOOL OnReturn ();
    BOOL OnKeyup ();
    void FillLSKopf ();
	void FillZahlZiel ();
	void OnDelete ();
    void FillRabSchl();
	void FillHausbank();
	void FillDta();
	void FillDtaInfo();
	void FillPtabCombo (CWnd *Control, LPTSTR Item);
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
    void EnableHeadControls (BOOL enable);
    BOOL StepBack ();
	virtual void OnCopy ();
    virtual void OnPaste ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
	BOOL Copy ();
	BOOL Paste ();
    virtual void OnCopyEx ();
    virtual void OnPasteEx ();
    void EnableFields (BOOL b);
	void UpdatePage ();
	afx_msg void OnKillFocus (CWnd *);


public:
	CStatic m_DataBorder2;
public:
	CStatic m_DataBorder3;
/* ------>
public:
	int m_KunPage7;
public:
	CStatic m_LBank;
public:
	CTextEdit m_Bank;
public:
	CStatic m_LBlz;
public:
	CNumEdit m_Blz;
public:
	CStatic m_LKto;
public:
	CNumEdit m_Kto;

public:
	CNumEdit m_SWIFT;
public:
	CStatic m_LSWIFT;
public:
	CStatic m_LHausbank;
public:
	CComboBox m_Hausbank;
public:
	CNumEdit m_Steuer;
public:
	CNumEdit m_Ust;
public:
	CStatic m_LSteuer;
public:
	CStatic m_LUst;
public:
	CStatic m_LEG;
public:
	CComboBox m_EG;
public:
	CComboBox m_GP;
public:
	CStatic m_LGP;
public:
	CNumEdit m_StatKun;
public:
	CStatic m_LStatKun;

< --------- */



public:
	CStatic m_LDta;

public:
	CStatic m_LEdiTyp;
public:
	CNumEdit m_EdiTyp;
public:
	CStatic m_LE;
public:
	CStatic m_LC;
public:
	CStatic m_DataBorder4;
public:
	CStatic m_LDtaBez;
public:
	CEdit m_DtaBez;

public:
	CStatic m_LDtaILN;
public:
	CNumEdit m_DtaILN;

public:
	CStatic m_LDtaKZ;
public:
	CNumEdit m_DtaKZ;
public:
	CStatic m_LDtaTyp;
public:
	CComboBox m_DtaTyp;
public:
	CStatic m_LKette;
public:
	CComboBox m_Kette;
public:
	CStatic m_LDtaNr1;
public:
	CNumEdit m_DtaNr1;
public:
	CStatic m_LDtaNr2;
public:
	CNumEdit m_DtaNr2;
public:
	CStatic m_LDtaNr3;
public:
	CNumEdit m_DtaNr3;
public:
	CStatic m_LDtaVb1;

public:
	CNumEdit m_DtaVb1;
public:
	CStatic m_LDtaVb2;
public:
	CNumEdit m_DtaVb2;
public:
	CStatic m_LDtaVb3;
public:
	CNumEdit m_DtaVb3;
public:
	CStatic m_DataBorder5;

public:
	CComboBox m_DtaNummer;
public:
	afx_msg void OnCbnSelchangeDtanummer();
};
