#include "VFont.h"

VFont::VFont () : CVector ()
{
}

VFont::~VFont ()
{
}

void VFont::SetStdFont (HFONT Font)
{
	StdFont = Font;
}

BOOL VFont::Exist (RFont* rFont)
{
	RFont *f;
	FirstPosition ();
	while ((f = (RFont *) GetNext ()) != NULL)
	{
		if (*f == *rFont && f->GetFont () != NULL)
		{
			f->inc ();
			rFont->SetFont (f->GetFont ());
			return TRUE;
		}
	}
	return FALSE;
}

BOOL VFont::CanDestroy (HFONT Font)
{
	RFont *f;
	FirstPosition ();
	while ((f = (RFont *) GetNext ()) != NULL)
	{
		if (f->GetFont () == Font)
		{break;
		}
	}
	if (f == NULL)
	{
		return TRUE;
	}

	if (f->GetInstances () == 0)
	{
		return TRUE;
	}
	f->dec ();
	if (f->GetInstances () == 0)
	{
		Drop (f);
		return TRUE;
	}
	return FALSE;
}
