#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "strfkt.h"
#include "wmaskc.h"
#include "lbox.h"
#include "image.h"
#ifdef BIWAK
#include "conf_env.h"
#endif

#define MAXMEN 100

extern int mouseindialog (HWND, POINT *);
extern BOOL IsDlgClient (HWND, POINT *);
extern int MousetohWnd (HWND, POINT *);

LONG FAR PASCAL ListProcM (HWND,UINT,WPARAM,LPARAM);

struct MENCONTROL
{
       HWND hWnd;
       struct LMENUE *menue;
       int MenStart;
       BOOL vlines;
       BOOL hlines;
       int MenSelect;
       int listzab;
       BOOL ListFont;       
	   int title_mode;
	   int NoListTitle;
	   int sz;
	   int selcolor;
	   COLORREF SelColor;
	   COLORREF SelBkColor;
	   HWND lboxbar;
       char *listtab;
       char *liststruct;
       form *listform;
       form *listubform;
       form *listubscroll;
       int listanz;	
       BOOL  WithDblClck;
	   DWORD Id;
	   BOOL listenter;
	   int AktMenZeile;
	   double UbHeight;
	   double RowHeight;
       IMG **Images;
       void (*SortProc) (int, HWND);
};

static int MenStart  = 0;
static BOOL vlines   = FALSE;
static BOOL hlines   = FALSE;
static int MenSelect = 1;
static int listzab;
static BOOL ListFont;
static int title_mode;
static int NoListTitle;
static int sz;
static int selcolor = 1;
static COLORREF SelColor = WHITECOL;
static COLORREF SelBkColor = RGB ( 0, 0, 120);
static BOOL  WithDblClck = TRUE;
static DWORD Id;

static HWND lboxbar = NULL;
static HWND lbox = NULL;
static char *listtab;
static char *liststruct;
static form *listform;
static form *listubform;
static form *listubscroll = NULL;
static int listanz;	
static BOOL listenter = FALSE;
static int AktMenZeile = 0;
static double UbHeight = (double) 1.0;
static double RowHeight = (double) 1.0;
static char tchar = '\"';
static char HK;
static IMG **Images;
static HFONT LiFont;
static void (*SortProc) (int, HWND);
static BOOL SortEnable = FALSE;
static int SortRow = 0;
static int SortRowOld = -1;
static int SortDir = 1;

static BOOL TestRec = TRUE;

static TEXTMETRIC tm;

struct MENCONTROL **MenControl = NULL; 

static struct LMENUE **menuetab = NULL;
static int AktMen = -1;
static struct LMENUE *menue;

static int xchar, ychar, cxCaps;
static int cxClient, cyClient;
static int budown = 0;
static BOOL MousePressed = 0;
static int InTimer = 0;


void RegisterLboxM (HWND hWnd)
{
        static int registered = 0;
        WNDCLASS wc;
		HINSTANCE hMainInst;

        if (registered) return;

		registered = 1;
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hMainInst;
        wc.hIcon         =  0;

        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.lpszMenuName  =  "";
        wc.style         =  CS_DBLCLKS | CS_OWNDC;
        wc.lpfnWndProc   =  ListProcM;
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "lboxm";
        RegisterClass(&wc);

}

void EnableSort (BOOL b)
{
    SortEnable = b;
}

void SetSortRow (int Row)
{
    SortRow = Row;
}

void Settchar (char c)
{
	    tchar = c;
		HK    = c;
}

char Gettchar (void)
{
	     return tchar;
}

void SetListFontM (BOOL mode)
/**
ListFont setzen.
**/
{
            ListFont = mode;
            if (ListFont != 0) ListFont = 1;
}

void SetMenSelectM (BOOL mode)
{
	   MenSelect = max (0, min (1, mode));
}

void SetMenStartM (int me)
/**
**/
{
            MenStart = me;
}

void SetImage (IMG **img)
{
	        Images = img;
}

void SetListFontBefore (HFONT hFont)
{
            LiFont = hFont;
}

void SetSortProc (void (*sp) (int, HWND))
{
            SortProc = sp;
}

BOOL GetAktMen (HWND hWnd)
/**
Aktuelles Menue aus menuetab in menue uebertragn.
**/
{
      int i;

      if (MenControl == NULL) return FALSE;
      
      for (i = 0; i < AktMen; i ++)
      {
                 if (hWnd == MenControl [i]->hWnd) break;
      }
      if (i == AktMen) return FALSE;

      if (menue != MenControl [i]->menue)
      {
          SortDir = 1;
          SortRowOld = -1;
      }
      menue        = MenControl [i]->menue;
      MenStart     = MenControl [i]->MenStart;
      vlines       = MenControl [i]->vlines;
      hlines       = MenControl [i]->hlines;
      MenSelect    = MenControl [i]->MenSelect;
      listzab      = MenControl [i]->listzab;
      ListFont     = MenControl [i]->ListFont;
      title_mode   = MenControl [i]->title_mode;
      NoListTitle  = MenControl [i]->NoListTitle;
      sz           = MenControl [i]->sz;
      selcolor     = MenControl [i]->selcolor;
      SelColor     =  MenControl [i]->SelColor;
      SelBkColor   = MenControl [i]->SelBkColor;
      WithDblClck  = MenControl [i]->WithDblClck;
      Id           = MenControl [i]->Id;
      lboxbar      = MenControl [i]->lboxbar;
      lbox         = MenControl [i]->hWnd;
      listtab      = MenControl [i]->listtab;
      liststruct   = MenControl [i]->liststruct;
      listform     = MenControl [i]->listform;
      listubform   = MenControl [i]->listubform;
      listubscroll = MenControl [i]->listubscroll;
      listanz      = MenControl [i]->listanz;
      listenter    = MenControl [i]->listenter;
      AktMenZeile  = MenControl [i]->AktMenZeile;
      UbHeight     = MenControl [i]->UbHeight;
      RowHeight    = MenControl [i]->RowHeight;
	  HK           = menue->tchar;
	  Images       = MenControl [i]->Images;
	  SortProc     = MenControl [i]->SortProc;
      return TRUE;
}

BOOL SetAktMen (HWND hWnd)
/**
Aktuelles Menue aus menuetab in menue uebertragn.
**/
{
      int i;

      for (i = 0; i < AktMen; i ++)
      {
                 if (hWnd == MenControl [i]->hWnd) break;
      }
      if (i == AktMen) return FALSE;

      MenControl [i]->menue        = menue;
      MenControl [i]->MenStart     = MenStart;
      MenControl [i]->vlines       = vlines;
      MenControl [i]->hlines       = hlines;
      MenControl [i]->MenSelect    = MenSelect;
      MenControl [i]->listzab      = listzab;
      MenControl [i]->ListFont     = ListFont;
      MenControl [i]->title_mode   = title_mode;
      MenControl [i]->NoListTitle  = NoListTitle;
      MenControl [i]->sz           = sz;
      MenControl [i]->selcolor     = selcolor;
      MenControl [i]->SelColor     = SelColor;
      MenControl [i]->SelBkColor   = SelBkColor;
      MenControl [i]->WithDblClck  = WithDblClck;
      MenControl [i]->Id           = Id;
      MenControl [i]->lboxbar      = lboxbar;
      MenControl [i]->listtab      = listtab;
      MenControl [i]->liststruct   = liststruct;
      MenControl [i]->listform     = listform;
      MenControl [i]->listubform   = listubform;
      MenControl [i]->listubscroll = listubscroll;
      MenControl [i]->listanz      = listanz;
      MenControl [i]->listenter    = listenter;
      MenControl [i]->AktMenZeile  = AktMenZeile;
      MenControl [i]->UbHeight     = UbHeight;
      MenControl [i]->RowHeight    = RowHeight;
      MenControl [i]->Images       = Images;
      MenControl [i]->SortProc     = SortProc;
      return TRUE;
}

void initMenArr (LMENUE *men)
{
    int i;

    for (i = 0; i < 0x10000; i ++)
    {
        men->menArr[i] = NULL;
        men->menArr1[i] = NULL;
        men->menArr2[i] = NULL;
    }
}


static BOOL CreateMenueCtrl (HWND hWnd)
/**
Menue fuer ListBox generieren.
**/
{
      if (AktMen == MAXMEN) return FALSE;
      if (menuetab == NULL)
      {
                menuetab   = new struct LMENUE * [MAXMEN];
                MenControl = new struct MENCONTROL * [MAXMEN];
                AktMen = 0;
      }

      SortDir = 1;
      SortRowOld = -1;
      menuetab [AktMen]   = new LMENUE;
      initMenArr (menuetab [AktMen]);
      MenControl [AktMen] = new MENCONTROL;
      MenControl[AktMen]->hWnd = hWnd;
      MenControl[AktMen]->menue = menuetab[AktMen];
      MenControl[AktMen]->Images = Images;
      MenControl[AktMen]->SortProc = SortProc;
	  menue = MenControl [AktMen]->menue;
	  menue->menTAnz   = 0;
	  menue->menTitle[0] = NULL;
	  menue->menVlpos = NULL;
	  menue->menRowpos = NULL;
	  menue->menRowAttr = NULL;
	  menue->SortEnable = SortEnable;
	  menue->SortRow    = SortRow;
//      SortRowOld = SortRow;
	  menue->tchar = tchar;
      vlines      = FALSE;
      hlines      = FALSE;
//    MenSelect   = 1;
      listzab     = 1;
      ListFont    = 0;
      title_mode  = 0;
      NoListTitle = TRUE;
      sz          = 1;
      selcolor = 1;
      SelColor = WHITECOL;
      SelBkColor = RGB ( 0, 0, 120);
	  WithDblClck = TRUE;
	  lboxbar = NULL;
      listtab      = NULL;
      liststruct   = NULL;
      listform     = NULL;
      listubform   = NULL;
      listubscroll = NULL;
      listanz      = 0;
      listenter    = FALSE;
	  AktMenZeile  = 0;
	  UbHeight     = (double) 0.0;
	  RowHeight    = (double) 1.0;
      AktMen ++;
	  SetAktMen (hWnd);
      return TRUE;
}

static void DestroyMenueCtrl (HWND hWnd)
/**
Menue loeschen.
**/
{
      int i;

      for (i = 0; i < AktMen; i ++)
      {
                 if (hWnd == MenControl [i]->hWnd) break;
      }
      if (i == AktMen) return;

      delete menuetab [i]; 
      delete MenControl [i]; 

      AktMen --;
      for (; i < AktMen; i ++)
      {
             menuetab[i] = menuetab[i + 1];
             MenControl[i] = MenControl [ i + 1];  
      }
      menuetab[i]   = NULL;
      MenControl[i] = NULL;  
}

static int MinusHK (char *text, int pos)
/**
Anfuehrungszeichen abziehen.
**/
{
	   int i;
	   int ps;

	   ps = pos;
	   for (i = 0; i < ps; i ++)
	   {
		   if (text[i] == HK)
		   {
			   pos --;
		   }
	   }
	   if (pos < 0) pos = 0;
	   return pos;
}

static BOOL hkError (char *txt)
/**
Test das Zeichen '"' paarweise vorkommt.
**/
{
	    char *hk;
		char *txtend;
		int len;
		int i;

		len = strlen (txt);
		txtend = &txt[len];
		i = 0;
		while (hk = strchr (txt, HK))
		{
			i ++;
			txt = hk + 1;
			if (*txt == 0) break;
			if (txt > txtend) break;
		}
		if ((i % 2) == 0)
		{
			return FALSE;
		}
		return TRUE;
}

BOOL PrintImage (char *imgidx, int x, int y)
{
	    int i;
		int idx;

		if (Images == NULL) return FALSE;
		idx = atoi (imgidx) - 1;
		if (idx < 0) return FALSE;
		for (i = 0;Images[i]; i ++);
		if (i <= idx) return FALSE;
		Images[idx]->Print (lbox,x,y);
		return TRUE;
}

static int GetRowAttrAnz (void)
{
        int i;

        if (menue->menRowAttr == NULL) return 0;
        for (i = 0; menue->menRowAttr[i] != NULL; i ++);
        return i;
}

static void TextOut0 (HDC hdc, int x, int y, char * zeiletxt, int spalte)
/**
Text zerlegen und ausgeben.
**/
{
	   SIZE size;
       SIZE ssize;
	   int i, j, anz;
       int zanz;
	   char *p1;
	   char *p;
	   int pos;
	   int x1;
	   char *text;
	   char *txt;
       int RaAnz;

	   zanz = wsplit (zeiletxt, " ", HK);

	   txt = &zeiletxt[spalte];
	   text = new char [strlen (txt) + 2];
	   if (hkError (txt))
	   {
		   if (txt[0] != HK)
		   {
		           text[0] = HK;
		           strcpy (&text[1], &txt[1]);
		   }
		   else
		   {
		           text[0] = ' ';
		           strcpy (&text[1], &txt[1]);
		   }
	   }
	   else
	   {
		   strcpy (text, txt);
	   }
                                            
       GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 

	   anz = wsplit (text, " ", HK);
	   if (anz == 0) return;

       RaAnz = GetRowAttrAnz (); 
       zanz = zanz - anz;
       if (zanz < 0) zanz = 0;
	   p1 = text;
	   for (i = 0; i < anz; i ++)
	   {
		   p = strstr (p1, wort[i]);
		   if (p == NULL) continue;
		   pos = (int) (p - text);
		   p1 = p + strlen (wort[i]);
		   pos = MinusHK (zeiletxt, pos + spalte) - spalte;
		   x1 = x + pos * size.cx;

//           if (menue->menRowAttr && menue->menRowAttr[i + zanz])
           if (menue->menRowAttr && (i + zanz) < RaAnz)
           {

               if (menue->menRowAttr[i + zanz][0] == '%' &&
                   (strchr (&menue->menRowAttr[i + zanz][1], 'd') ||
                    strchr (&menue->menRowAttr[i + zanz][1], 'f')))
               {
                    for (j = 0; j < (int) strlen (wort[i]); j ++) x1 += size.cx;
                    GetTextExtentPoint32 (hdc, wort[i], strlen (wort [i]) , &ssize); 
                    x1 -= (ssize.cx);
                    if (x1 < 0) x1 = 0;
               }
           }

		   if (wort[i][0] == '#')
		   {
			   PrintImage (&wort[i][1], x1, y);
		   }
		   else
		   {
		       TextOut (hdc, x1, y, wort[i], strlen (wort[i]));
		   }
	   }
	   delete text;
}


static void TextOutTitle (HDC hdc, int x, int y, char * zeiletxt, int spalte)
/**
Text zerlegen und ausgeben.
**/
{
	   SIZE size;
	   int i, anz;
       int zanz;
	   char *p1;
	   char *p;
	   int pos;
	   int x1;
	   char *text;
	   char *txt;
       int RaAnz;

	   zanz = wsplit (zeiletxt, " ", HK);

	   txt = &zeiletxt[spalte];
	   text = new char [strlen (txt) + 2];
	   if (hkError (txt))
	   {
		   if (txt[0] != HK)
		   {
		           text[0] = HK;
		           strcpy (&text[1], &txt[1]);
		   }
		   else
		   {
		           text[0] = ' ';
		           strcpy (&text[1], &txt[1]);
		   }
	   }
	   else
	   {
		   strcpy (text, txt);
	   }
                                            
       GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 

	   anz = wsplit (text, " ", HK);
	   if (anz == 0) return;

       RaAnz = GetRowAttrAnz (); 
       zanz = zanz - anz;
       if (zanz < 0) zanz = 0;
	   p1 = text;
	   for (i = 0; i < anz; i ++)
	   {
		   p = strstr (p1, wort[i]);
		   if (p == NULL) continue;
		   pos = (int) (p - text);
		   p1 = p + strlen (wort[i]);
		   pos = MinusHK (zeiletxt, pos + spalte) - spalte;
		   x1 = x + pos * size.cx;

//           if (menue->menRowAttr && menue->menRowAttr[i + zanz])
/*
           if (menue->menRowAttr && (i + zanz) < RaAnz)
           {

               if (menue->menRowAttr[i + zanz][0] == '%' &&
                   (strchr (&menue->menRowAttr[i + zanz][1], 'd') ||
                    strchr (&menue->menRowAttr[i + zanz][1], 'f')))
               {
                    for (j = 0; j < (int) strlen (wort[i]); j ++) x1 += size.cx;
                    GetTextExtentPoint32 (hdc, wort[i], strlen (wort [i]) , &ssize); 
                    x1 -= (ssize.cx);
                    if (x1 < 0) x1 = 0;
               }
           }
*/

		   if (wort[i][0] == '#')
		   {
			   PrintImage (&wort[i][1], x1, y);
		   }
		   else
		   {
		       TextOut (hdc, x1, y, wort[i], strlen (wort[i]));
		   }
	   }
	   delete text;
}

void RecColorTitleM (HDC hdc, COLORREF bcolor, int xchar, int ychar)
/**
Eine Zeile mit Hintergrundfarbe.
**/
{
         HBRUSH hBrush;
         HBRUSH oldBrush;

         if (title_mode == BUTTON) return;
		 if (NoListTitle == TRUE) return; 

         hBrush = CreateSolidBrush (bcolor);
         oldBrush = SelectObject (hdc, hBrush);
         SelectObject (hdc, GetStockObject (NULL_PEN));
         if (menue->menTAnz == 0)
         {

                  Rectangle (hdc,0, 0, xchar * (menue->menWspalten + 1),
                                       (int) (double) ((double) ychar * UbHeight + 1));
		 }
         else
         {
                  Rectangle (hdc,0, 0, xchar * (menue->menWspalten + 1),
                                       (int) (double) ((double) ychar * menue->menTAnz
									   * UbHeight + 1));
         }
         DeleteObject (SelectObject (hdc, oldBrush));
}


void RecColorM (HDC hdc, COLORREF bcolor, int xchar, int ychar)
/**
Eine Zeile mit Hintergrundfarbe.
**/
{
         int y;
         HBRUSH hBrush;
         HBRUSH oldBrush;
/*
		 HBRUSH RectBrush;
		 RECT rect;
*/


         hBrush = CreateSolidBrush (bcolor);

         oldBrush = SelectObject (hdc, hBrush);
         SelectObject (hdc, GetStockObject (NULL_PEN));
         y = (int) (double) (((double) (menue->menSelect - menue->menZeile) * listzab * RowHeight
			                  + sz * UbHeight) * ychar);
         Rectangle (hdc,0, y, xchar * (menue->menWspalten + 1), y + ychar + 1);

         DeleteObject (SelectObject (hdc, oldBrush));
}

void PrintVLineM (HDC hdc, int y)
/**
Vertikale Linie Zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x;
        TEXTMETRIC tm;
		SIZE size;

        if (vlines	== 0) return;
        if (menue->menVlpos == NULL) return;

        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
		tm.tmAveCharWidth = size.cx;

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i < menue->menWspalten + 1; i ++)
        {
               if (i + menue->menSpalte >=
                   (int) strlen (menue->menVlpos)) break;

               if (menue->menVlpos[i + menue->menSpalte] > ' ')
               {
//                    x = i * tm.tmAveCharWidth;
                    x = i * size.cx;
                    MoveToEx (hdc, x, y, NULL);
                    LineTo (hdc, x, y + (tm.tmHeight * listzab) - hlines + 0);
               }
        }

        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}
        
void PrintSLinesM (HDC hdc)
/**
Horitontale Line fuer Scrollbereich zeichnen.
**/
{
        HPEN hPenB;
        HPEN hPenW;
        HPEN hPenL;
        HPEN oldPen;

        hPenB = CreatePen (PS_SOLID, 0, BLACKCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
        hPenL = CreatePen (PS_SOLID, 0, LTGRAYCOL);

        oldPen = SelectObject (hdc, hPenB);
        MoveToEx (hdc, menue->srect.left, menue->srect.top, NULL);
        LineTo (hdc, menue->srect.left, menue->srect.bottom);

        SelectObject (hdc, hPenW);
        MoveToEx (hdc, 0, menue->srect.top, NULL);
        LineTo (hdc, menue->srect.right, menue->srect.top);

        SelectObject (hdc, hPenL);
        MoveToEx (hdc, 0, menue->srect.top + 1, NULL);
        LineTo (hdc, menue->srect.right, menue->srect.top + 1);
        SelectObject (hdc, oldPen);
        DeleteObject (hPenB);
        DeleteObject (hPenW);
        DeleteObject (hPenL);
}

void PrintHLineM (HDC hdc, int y)
/**
Horitontale Line Zeichnen.
**/
{
        HPEN hPen;
        HPEN oldPen;
        int ublines;
        int testzeile;
        TEXTMETRIC tm;
		SIZE size;

        if (hlines != 1) return;
        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);

        if (menue->menTAnz > 1)
        {
                   ublines = menue->menTAnz - MenStart + 1;
        }
        else
        {
                   ublines = 1;
        }
        GetTextMetrics (hdc, &tm);
		tm.tmAveCharWidth = size.cx;
        testzeile = (y / tm.tmHeight) - menue->menTAnz;
        if (testzeile % ublines) return;
        
        oldPen = SelectObject (hdc, hPen);
        y += (int) (double) ((double) (tm.tmHeight * listzab * RowHeight) - 2);
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, (menue->menWspalten + 1) * size.cx, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}

void PrintVLineBlackM (HDC hdc)
/**
Vertikale Line Zeichnen.
**/
{
        HPEN hPen;
        HPEN oldPen;
        int i;
        int x;
        int y;
        int Height;
		SIZE size;

//        if (vlines == 0) return;
        if (menue->menVlpos == NULL) return;

        if (menue->menTAnz > 1)
        {
                   Height = (int) (double) ((double) 
					        (menue->menTAnz - MenStart) * tm.tmHeight * UbHeight);
                   y = MenStart;
                   y = (int) (double) ((double) y * tm.tmHeight * UbHeight);
        }
        else
        {
                   Height = (int) (double) ((double) tm.tmHeight * UbHeight);
                   y = 0;
        }

        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i < menue->menWspalten + 1; i ++)
        {
               if (i + menue->menSpalte >=
                   (int) strlen (menue->menVlpos)) break;

               if (menue->menVlpos[i + menue->menSpalte] > ' ')
               {
//                    x = i * tm.tmAveCharWidth;
                    x = i * size.cx;
                    MoveToEx (hdc, x, y, NULL);
                    LineTo (hdc, x, y + Height - 1);
               }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        hPen = CreatePen (PS_SOLID, 0, GRAYCOL);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i < menue->menWspalten; i ++)
        {
               if (i + menue->menSpalte >=
                   (int) strlen (menue->menVlpos)) break;

               if (menue->menVlpos[i + menue->menSpalte] > ' ')
               {
                    x = i * size.cx - 1;
                    MoveToEx (hdc, x, y, NULL);
                    LineTo (hdc, x, y + Height - 2);
               }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}
        
void PrintVLineWhiteM (HDC hdc)
/**
Verikale Line Zeichnen.
**/
{
        HPEN hPen;
        HPEN oldPen;
        int i;
        int x;
        int y;
        int Height;
		SIZE size;

//        if (vlines == 0) return;
        if (menue->menVlpos == NULL) return;

        if (menue->menTAnz > 1)
        {
                   Height = (int) (double) ((double) (menue->menTAnz - MenStart) * 
					                         tm.tmHeight * UbHeight);
                   y = MenStart;
                   y = (int) (double) ((double) tm.tmHeight * UbHeight);
        }
        else
        {
                   Height = (int) (double) ((double) tm.tmHeight * UbHeight);
                   y = 0;
        }

        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);
        if (menue->menSpalte == 0)
        {
                    x = 0;
                    MoveToEx (hdc, x, y, NULL);
                    LineTo (hdc, x, y + Height - 1);
        }
        for (i = 0; i < menue->menWspalten + 1; i ++)
        {
               if (i + menue->menSpalte >=
                   (int) strlen (menue->menVlpos)) break;

               if (menue->menVlpos[i + menue->menSpalte] > ' ')
               {
//                    x = i * tm.tmAveCharWidth + 1;
                    x = i * size.cx + 1;
                    MoveToEx (hdc, x, y, NULL);
                    LineTo (hdc, x, y + Height - 1);
               }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}
        
        
void PrintHLineTitleOM (HDC hdc)
/**
Horizontale Line Zeichnen.
**/
{
        HPEN hPen;
        HPEN oldPen;
        int y;

		if (menue->menVlpos && strlen (menue->menVlpos));
        else if (hlines == 0 && vlines == 0) return;

        if (menue->menTAnz < 2) return;

        hPen = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        y = (int) (double) ((double) MenStart * tm.tmHeight * UbHeight);
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, (menue->menWspalten + 1) * tm.tmAveCharWidth, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}
        
void PrintHLineTitleM (HDC hdc)
/**
Horizontale Line Zeichnen.
**/
{
        HPEN hPen;
        HPEN oldPen;
        int y;

		if (menue->menVlpos && strlen (menue->menVlpos));
        else if (hlines == 0 && vlines == 0) return;

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);

        oldPen = SelectObject (hdc, hPen);
        if (menue->menTAnz)
        {
                    y = (int) (double) ((double) menue->menTAnz * tm.tmHeight * UbHeight - 1);
        }
        else
        {
                    y = (int) (double) ((double) tm.tmHeight * UbHeight - 1);
        }
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, (menue->menWspalten + 1) * tm.tmAveCharWidth, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
		y --;
        hPen = CreatePen (PS_SOLID, 0, GRAYCOL);

        oldPen = SelectObject (hdc, hPen);
        MoveToEx (hdc, 1, y, NULL);
        LineTo (hdc, (menue->menWspalten + 1) * tm.tmAveCharWidth, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}
        

void ShowTitleM (HDC hdc, COLORREF color, COLORREF bcolor)
/**
Ueberschrift anzeigen.
**/
{
         char *text;
         int y;
         int xchar;
         int ychar;
         TEXTMETRIC tm;
		 SIZE size;
         int i;

         if (title_mode == BUTTON) return;
		 if (NoListTitle == TRUE) return; 

         GetTextMetrics (hdc, &tm);
         GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
         tm.tmAveCharWidth = size.cx;
         xchar = tm.tmAveCharWidth;
         ychar = tm.tmHeight;

         if (menue->menTitle[0] == NULL) return;

         SetBkMode (hdc, OPAQUE);
         SetTextColor (hdc,color);
         SetBkColor (hdc,bcolor);
         if (menue->menhFont)
         {
                     SelectObject (hdc, menue->menhFont);
                     GetTextMetrics (hdc, &tm);
                     GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
		             tm.tmAveCharWidth = size.cx;
         }
         y = 0;
         if (menue->menTAnz == 0 && menue->menTitle[0])
         {
                    if (menue->menSpalte >=
                        (int) strlen (menue->menTitle[0])) return;
//                    text = &menue->menTitle [0][menue->menSpalte];
                    text = &menue->menTitle [0][0];
                    TextOutTitle (hdc, 0, y, text, menue->menSpalte);
                    y = (int) (double) ((double) ychar * UbHeight);
         }
         for (i = 0; i < menue->menTAnz; i ++)
         {
                    if (menue->menSpalte <
                        (int) strlen (menue->menTitle[i]))
                    {
//                               text = &menue->menTitle [i][menue->menSpalte];
                               text = &menue->menTitle [i][0];
                               TextOutTitle (hdc, 0, y, text, menue->menSpalte);
                    }
                    y = (int) (double) ((double) y + ychar * UbHeight);
         }
         PrintHLineTitleM (hdc);
         PrintHLineTitleOM (hdc);
         PrintVLineWhiteM (hdc);
         PrintVLineBlackM (hdc);
         return;
}

void SetUpdRegM (RECT *rec, int idx)
/**
Update-Region fuer eine Zeile setzen.
**/
{
        rec->left = 0;
        rec->right = (menue->menWspalten + 1) * tm.tmAveCharWidth;
        rec->top = (int) (double) ((double) (idx * RowHeight) + (sz * UbHeight)
			                                 * tm.tmHeight);
        rec->bottom = (int) (double) ((double) rec->top + tm.tmHeight * RowHeight);
}

int InUpdRegM (int x, int y, int cx, int cy, PAINTSTRUCT *ps)
/**
Feststellen, ob der Bereich neu gezeichnet werden muss.
**/
{
         int py, px, pcy, pcx;

/* Update-Region ermitteln.                              */

          
         if (TestRec == FALSE) return TRUE;

         cx += x;
         cy += y;

//         py = ps->rcPaint.top / tm.tmHeight - 1;
         py = ps->rcPaint.top - 1;
         px = ps->rcPaint.left / tm.tmAveCharWidth - 1;
//         pcy = ps->rcPaint.bottom / tm.tmHeight + 1;
         pcy = ps->rcPaint.bottom + 1;
         pcx = ps->rcPaint.right / tm.tmAveCharWidth + 1;
         if (py < 0) py = 0;
         if (px < 0) px = 0;

         if (cy < py) return (0);
         if (y > pcy) return (0);
         return (1);
}

void ShowRowM (HWND hWnd, COLORREF color, COLORREF bcolor)
/**
Menuezeilen anzeigen.
**/
{
         char *text;
         int arrpos;
         int y;
         int xchar;
         int ychar;
         HDC hdc;
         TEXTMETRIC tm;
		 SIZE size;
         HFONT oldfont;

 	     if (!MenSelect) return; 
         if (menue->menSelect < menue->menZeile) return;

         hdc = GetDC (hWnd);

         if (menue->menhFont)
         {
                   oldfont = SelectObject (hdc, menue->menhFont);
         }
         GetTextMetrics (hdc, &tm);
         GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
         tm.tmAveCharWidth = size.cx;
         xchar = tm.tmAveCharWidth;
         ychar = tm.tmHeight;

         RecColorM (hdc, bcolor, xchar, ychar);

         SetBkMode (hdc, OPAQUE);
         SetTextColor (hdc,color);
         SetBkColor (hdc,bcolor);
         arrpos = menue->menSelect;
         y =  (int) (double) ((double) ((menue->menSelect - menue->menZeile) * listzab * 
			        RowHeight 
			        + sz * UbHeight) 
			        * ychar);
//         text = &menue->menArr[arrpos][menue->menSpalte];
         text = &menue->menArr[arrpos][0];
         TextOut0 (hdc, 0, y, text,menue->menSpalte);
         SetTextColor (hdc,BLACKCOL);
         SetBkColor (hdc,WHITECOL);
         if (listzab > 1)
         {
                   if (menue->menSpalte <
                       (int) strlen (menue->menArr1[arrpos]))
                   {
//                         text = &menue->menArr1[arrpos][menue->menSpalte];
                         text = &menue->menArr1[arrpos][0];
                         TextOut0 (hdc, 0, y + ychar, text, menue->menSpalte);
                   }
         }
         if (listzab > 2)
         {
                   if (menue->menSpalte <
                       (int) strlen (menue->menArr2[arrpos]))
                   {
//                         text = &menue->menArr2[arrpos][menue->menSpalte];
                         text = &menue->menArr2[arrpos][0];
                         TextOut0 (hdc, 0, y + 2 * ychar, text, menue->menSpalte);
                   }
         }
         PrintHLineM (hdc,y); 
         PrintVLineM (hdc,y); 
         ReleaseDC (hWnd, hdc);
         if (menue->menhFont)
         {
//                   DeleteObject (SelectObject (hdc, oldfont));
         }
	     SendMessage (GetParent (hWnd), WM_COMMAND,
                       MAKELONG (Id, LBN_SELCHANGE),	
		              (LPARAM) hWnd); 
         return;
}


int GetColumn (int xm)
/**
Spalte aus Ueberschrift holen.
**/
{
        int i;
        int x;
        HDC hdc;
        SIZE size;
        int column;

        hdc = GetDC (lbox);
        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
        ReleaseDC (lbox, hdc);
        column = 0;
        for (i = 0; i < menue->menWspalten + 1; i ++)
        {
/*
               if (i + menue->menSpalte >=
                   (int) strlen (menue->menVlpos)) break;
*/
               if (menue->menVlpos[i] == 0) break; 

               if (menue->menVlpos[i] > ' ')
               {
                    x = (i  - menue->menSpalte) * size.cx;
                    if (xm <= x)
                    {
                        break;
                    }
                    column ++;
               }
        }
        return column;
}

static int LongDiff (char *pos1, char *pos2, int len)
{
       char numpos1[80];
       char numpos2[80];

       sprintf (numpos1, "%ld", atol (pos1));
       sprintf (numpos2, "%ld", atol (pos2));
       numpos1[len] = 0;
       numpos2[len] = 0;
/*
       memcpy (numpos1, pos1, len);
       numpos1[len] = 0;
       memcpy (numpos2, pos2, len);
       numpos2[len] = 0;
*/
       if (atol (numpos1) > atol (numpos2))
       {
           return 1;
       }
       if (atol (numpos1) < atol (numpos2))
       {
           return -1;
       }
       return 0;
}

static int DoubleDiff (char *pos1, char *pos2, int len)
{
       char numpos1[80];
       char numpos2[80];

       memcpy (numpos1, pos1, len);
       numpos1[len] = 0;
       memcpy (numpos2, pos2, len);
       numpos2[len] = 0;
       if (ratod (numpos1) > ratod (numpos2))
       {
           return 1;
       }
       if (ratod (numpos1) < ratod (numpos2))
       {
           return -1;
       }
       return 0;
}

char *GetRowPos0 (char *elem, char *pos)
{
       int anz;

       if (menue->menRowpos == NULL) return pos;

       anz = wsplit (menue->menRowpos, " ");

       if (anz <= SortRow) 
       {
           return pos; 
       }
       return &elem [atoi (wort [SortRow])];
}

char *GetRowPos (char *elem, char *pos)
{
       int i;
       char *vl;
       char *vl1;

       if (menue->menRowpos)
       {
                 return GetRowPos0 (elem, pos);
       }
       if (menue->menVlpos == NULL) return pos;
       vl = menue->menVlpos;
       for (i = 0; i < SortRow; i ++)
       {
              vl1 = vl + 1;
              if (*(vl + 1) == 0)
              {
                  vl = vl1;
                  break;
              }
              vl = strchr (vl + 1, '1');
              if (vl == NULL) 
              {
                  vl = vl1;
                  break;
              }
       }
       i = (int) (vl - menue->menVlpos); 
       return &elem[i];
}

int GetRowLen0 (char *elem, int len)
{
       char *len1;
       int anz;

       if (menue->menRowpos == NULL) return len;

       anz = wsplit (menue->menRowpos, " ");

       if (anz <= SortRow) return len;
       len1 = strchr (wort[SortRow], ';');
       if (len1 == NULL) return len;

       len1 += 1;
       return atoi (len1);
}


int GetRowLen (char *elem, int len)
{
       int i;
       char *vl;
       char *vl1;

       if (menue->menRowpos)
       {
                 return GetRowLen0 (elem, len);
       }

       
       if (menue->menVlpos == NULL) return len;

       vl = menue->menVlpos;
       for (i = 0; i <= SortRow; i ++)
       {
              vl1 = vl; 
              if (*(vl + 1) == 0) 
              {
                  i = (int) (vl1 - menue->menVlpos);
                  return (int) strlen (&elem[i]);
              }
              vl = strchr (vl + 1, '1');
              if (vl == NULL)
              {
                  i = (int) (vl1 - menue->menVlpos);
                  return (int) strlen (&elem[i]);
              }
       }
       i = (int) (vl - vl1); 
       return i;
}


void SearchList (HWND hWnd, char *buffer)
{
        int i;
        int anz;
        char *pos1;
        int len1;
        char *elem = NULL;
        char *p;
        char tstr [2] = " ";

        tstr[0] = tchar;
        if (GetAktMen (hWnd) == FALSE) return;

        for (i = 0; i < menue->menAnz; i ++)
        {

          if (menue->menRowpos == NULL)
          {
            anz = wsplit (menue->menArr[i], " ");
            if (anz <= SortRow) continue;

            if (strlen (buffer) > strlen (wort[SortRow])) continue;

            p = strtok (wort[SortRow], tstr);
            if (p == NULL) continue;
 
            strcpy (wort [SortRow], p);
            if (menue->menRowAttr != NULL && menue->menRowAttr[SortRow][0] == '%' &&
                   strchr (&menue->menRowAttr[SortRow][1], 'd'))
            {
                   if (LongDiff (buffer, wort[SortRow], strlen (buffer)) == 0) break;
            }
            else if (menue->menRowAttr[SortRow][0] == '%' &&
                    strchr (&menue->menRowAttr[SortRow][1], 'f'))
            {
                   if (DoubleDiff (buffer, wort[SortRow], strlen (buffer)) == 0) break;
            }
            else 
            {
                if (strupcmp (buffer, wort[SortRow], strlen (buffer)) == 0) break;
            }
          }
          else
          {
            pos1 = GetRowPos (menue->menArr[i], NULL);
            if (pos1 == NULL)
            {
                      continue;
            }

            len1 = GetRowLen (menue->menArr[i], -1);
            if (len1 == -1)
            {
                      continue;
            }
            if (len1 > 255) len1 = 255;

            elem = new char [len1 + 1];
            memcpy (elem, pos1, len1);
            elem[len1] = 0;
            if (strlen (buffer) > (unsigned int) len1) continue;

            if (menue->menRowAttr[SortRow][0] == '%' &&
                   strchr (&menue->menRowAttr[SortRow][1], 'd'))
            {
                   if (LongDiff (buffer, elem, strlen (buffer)) == 0) break;
            }
            else if (menue->menRowAttr[SortRow][0] == '%' &&
                    strchr (&menue->menRowAttr[SortRow][1], 'f'))
            {
                   if (DoubleDiff (buffer, elem, strlen (buffer)) == 0) break;
            }
            else 
            {
                if (strupcmp (buffer, elem, strlen (buffer)) == 0) break;
            }
          }
        }

        if (elem) delete elem;
        if (i == menue->menAnz) return;
        SendMessage (hWnd, LB_SETCURSEL, (WPARAM) i, (LPARAM) 0l);
}

int SortList0 (const void *el1, const void *el2)
{
//       int anz;
       int len1;
       int len2;
       char **elem1;
       char **elem2;
       char * pos1;
       char * pos2;
       char posd1 [512];
       char posd2 [512];
       
       memset (posd1, ' ', 511);
       memset (posd2, ' ', 511);
       elem1 = (char **) el1; 
       elem2 = (char **) el2; 
/*
       anz = wsplit (*elem1, " ");
       if (anz <= SortRow)
       {
           return -1 * SortDir;
       }
       len1 = strlen (wort[SortRow]);
       len1 = GetRowLen (*elem1, len1);
       pos1 = strstr (*elem1, wort [SortRow]);
       pos1 = GetRowPos (*elem1, pos1);
       if (pos1 == NULL)
       {
           return -1 * SortDir;
       }
*/

       pos1 = GetRowPos (*elem1, NULL);
       if (pos1 == NULL)
       {
           return -1 * SortDir;
       }

       len1 = GetRowLen (*elem1, -1);
       if (len1 == -1)
       {
           return -1 * SortDir;
       }

/*
       anz = wsplit (*elem2, " ");
       if (anz <= SortRow)
       {
           return 1 * SortDir;
       }

       len2 = strlen (wort[SortRow]);
       len2 = GetRowLen (*elem2, len2);
       pos2 = strstr (*elem2, wort [SortRow]);
       pos2= GetRowPos (*elem2,pos2);
       if (pos2 == NULL)
       {
           return 1 * SortDir;
       }
*/

       pos2 = GetRowPos (*elem2, NULL);
       if (pos2 == NULL)
       {
           return 1 * SortDir;
       }

       len2 = GetRowLen (*elem2, -1);
       if (len2 == -1)
       {
           return 1 * SortDir;
       }
       memcpy (posd1, pos1, len1);
       memcpy (posd2, pos2, len2);
       if (len2 > len1) len1 = len2;
       posd1[len1] = 0;
       posd2[len1] = 0;
       if (SortRow < GetRowAttrAnz ())
       {
               if (menue->menRowAttr[SortRow][0] == '%' &&
                   strchr (&menue->menRowAttr[SortRow][1], 'd'))
               {
                   return LongDiff (posd1, posd2, len1) * SortDir;
               }
               else if (menue->menRowAttr[SortRow][0] == '%' &&
                    strchr (&menue->menRowAttr[SortRow][1], 'f'))
               {
                   return DoubleDiff (posd1, posd2, len1) * SortDir;
               }
       }

       return memcmp (posd1, posd2, len1) * SortDir;
}


void SortList (void)
{
       if (SortRow == SortRowOld)
       {
           SortDir *= -1;
       }
       else
       {
           SortDir = 1;
           SortRowOld = SortRow;
       }
       qsort (menue->menArr, menue->menAnz, sizeof (char *),
				   SortList0);
       SendMessage (GetParent (lbox), WM_COMMAND,
                                           MAKELONG (Id, LBN_SORT),	
			                               (LPARAM) lbox); 
}


void TestSort (int x, int y)
/*
Maus auf Ueberschriftspalte.
*/
{
         int column;

         if (menue->menRowpos == NULL &&
             menue->menVlpos == NULL)
         {
             return;
         }

         column = GetColumn (x);
         if (SortProc)
         {
             (*SortProc) (column, lbox);
         }
         else if (menue->SortEnable)
         {
             SortRow = column;
             SortList ();
             InvalidateRect (lbox, NULL, TRUE);
             UpdateWindow (lbox);
         }
}

BOOL SelectRowM (HWND hWnd, WPARAM wParam, LPARAM lParam)
/**
Zeile selectieren.
**/
{
       int zeile;
       int x,y;
       int xchar, ychar;
       int newselect;
       HDC hdc;
       TEXTMETRIC tm;
       HFONT oldfont;
	   SIZE size;

	   if (!MenSelect) return FALSE; 
        selcolor = 1;

        hdc = GetDC (hWnd);

        if (menue->menhFont)
        {
                   oldfont = SelectObject (hdc, menue->menhFont);
        }
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
        tm.tmAveCharWidth = size.cx;
        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight;
        if (menue->menhFont)
        {
//                   DeleteObject (SelectObject (hdc, oldfont));
        }
        ReleaseDC (hWnd, hdc);

        x = LOWORD (lParam);
        y = HIWORD (lParam);

		if (y < (int) (double) ((double) (sz * UbHeight * ychar))) 
        {
//            TestSort (x, y);
            return FALSE;
        }
        y -= (int) (double) ((double) (sz * UbHeight) * ychar);
        zeile = (int) (double) ((double) y / (ychar * RowHeight));
        zeile /= listzab;

        if (zeile < 0)
        {
                   return TRUE;
        }

        if (menue->menSelect - menue->menZeile == zeile)
        {
                   return TRUE;
        }

        newselect = zeile + menue->menZeile;
        if (newselect >= 0 && newselect < menue->menAnz)
        {
                    ShowRowM (hWnd, BLACKCOL, WHITECOL);
                    menue->menSelect = newselect;
                    ShowRowM (hWnd, SelColor, SelBkColor);
        }
/*
        if (zeile * listzab > menue->menWzeilen  - sz - listzab &&
            menue->menZeile * listzab  < menue->menAnz  * listzab
                                        - (menue->menWzeilen - sz))
        {
                    menue->menZeile ++;
                    SetScrollPos (hWnd, SB_VERT, menue->menZeile * listzab,
                                  TRUE);
                    InvalidateRect (hWnd, NULL, TRUE);
                    UpdateWindow (hWnd);
        }
*/
        else if (zeile < 0 && menue->menZeile > 0)
        {
                    menue->menZeile --;
                    SetScrollPos (hWnd, SB_VERT, menue->menZeile * listzab, 
                                  TRUE);
                    InvalidateRect (hWnd, NULL, TRUE);
                    UpdateWindow (hWnd);
        }
        return TRUE;
}


static void ShowMenueM (HWND hWnd, BOOL modus)
/**
Menuezeilen anzeigen.
**/
{
         static int aktzeile = 0;
         char *text;
         int arrpos;
         int i;
         HDC hdc;
         PAINTSTRUCT ps;
         int y;
         int xchar;
         int ychar;
         LMENUE menuesave;
		 SIZE size;

         if (GetAktMen (hWnd) == FALSE) return;

         memcpy (&menuesave, menue, sizeof (menue));

		 if (modus == 0)
		 {
                   hdc = BeginPaint (hWnd, &ps);
		 }
		 else
		 {
			       hdc = GetDC (hWnd);
		 }

         if (menue->menhFont)
         {
               SelectObject (hdc, menue->menhFont);
               GetTextMetrics (hdc, &tm);
               GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
		       tm.tmAveCharWidth = size.cx;
               xchar = tm.tmAveCharWidth;
               ychar = tm.tmHeight;
         }

         if (InUpdRegM (0, 0, menue->menWspalten, 1, &ps));
         {
               RecColorTitleM (hdc, GetSysColor (COLOR_3DFACE), xchar, ychar);
               ShowTitleM (hdc, BLACKCOL, GetSysColor (COLOR_3DFACE));
         }

         SetBkMode (hdc, OPAQUE);
         arrpos = menue->menZeile;
         for (i = 0; i < menue->menWzeilen; i ++)
         {
                  if (i >= menue->menAnz) break;  

                  y =  (int) (double) ((double) (i * listzab * RowHeight + sz * UbHeight)
					                             * ychar);
                  if (i == menue->menAnz - menue->menZeile) break;
                  if (i + menue->menZeile == menue->menSelect &&
                      selcolor && MenSelect)
                  {
                        RecColorM (hdc, SelBkColor, xchar, ychar);
                        SetTextColor (hdc,SelColor);
                        SetBkColor (hdc,SelBkColor);
                  }
                  else
                  {
                        SetTextColor (hdc,BLACKCOL);
                        SetBkColor (hdc,WHITECOL);
                  }

                  if (menue->menSpalte < (int) strlen (menue->menArr [arrpos]))
                  {
                        text = &menue->menArr[arrpos][0];
                        TextOut0 (hdc, 0, y, text, menue->menSpalte);
                        SetTextColor (hdc,BLACKCOL);
                        SetBkColor (hdc,WHITECOL);
                        if (listzab > 1)
                        {
                               if (menue->menSpalte <
                                   (int) strlen (menue->menArr1[arrpos]))
                               {
                                   text =
                                   &menue->menArr1[arrpos][0];
                                   TextOut0 (hdc, 0, y
                                        + ychar, text, menue->menSpalte);
                               }
                        }
                        if (listzab > 2)
                        {
                               if (menue->menSpalte <
                                   (int) strlen (menue->menArr2[arrpos]))
                               {
                                   text =
                                   &menue->menArr2[arrpos][0];
                                   TextOut0 (hdc, 0, y
                                        + 2 * ychar, text, menue->menSpalte);
                               }
                        }
                  }
                  if (menue->srect.bottom)
                  {
                        PrintSLinesM (hdc);
                  }
                  PrintHLineM (hdc,y); 
                  PrintVLineM (hdc,y); 
                  arrpos ++;
         }
		 if (hlines == 0)
		 {
                  for (;i < menue->menWzeilen - sz; i ++)
				  { 
                       y = (i * listzab + sz);
                       y *= ychar;
                       PrintVLineM (hdc,y); 
                  }
         }
		 if (modus == 0)
		 {
                  EndPaint (hWnd, &ps);
		 }
		 else
		 {
			       ReleaseDC (hWnd, hdc);
		 }
         memcpy (menue, &menuesave, sizeof (menue));
         return;
}

void ScrollBuTitleM (void)
/**
Buttonueberschrift scrollen.
**/
{
	    int i;
		form *savecurrent;


		if (listubscroll == NULL) return; 
		savecurrent = current_form;
	    CloseControls (listubscroll);
		listubscroll->fieldanz = 
		listubform->fieldanz - menue->menFpos;
	    memcpy (listubscroll->mask,
			&listubform->mask[menue->menFpos],
			listubscroll->fieldanz * sizeof field);
        listubscroll->mask[0].pos[1] = 0;
		for (i = 1; i < listubscroll->fieldanz; i ++)
		{

                 listubscroll->mask[i].pos[1] = 
                    listubscroll->mask[i - 1].pos[1] + 
                    listubscroll->mask[i - 1].length;
		}			
		display_form (lbox, listubscroll, 0, 0);
		current_form = savecurrent;
}

static int GetFirstRowM (form *frm)
/**
1. Zeilenposition fuer Form ermitteln.
**/
{
           int zeile;
           int i;

           zeile = 999;
           for (i = 0; i < frm->fieldanz; i ++)
           {
                  if (zeile > frm->mask[i].pos[0])
                  {
                         zeile = frm->mask[i].pos[0];
                  }
           }
           return zeile;
}


static int MaxListPosM (form *frm)
/**
Hoechste Listenposition fuer Form ermitteln.
**/
{
           int max;
           int spalten;
           int first;
           int i;

           spalten = max = 0;
           first = GetFirstRowM (frm);
           for (i = 0; i < frm->fieldanz; i ++)
           {
                  if (frm->mask[i].pos[0] > first) continue; 
                  if (spalten < frm->mask[i].pos[1] +
                                frm->mask[i].length)
                  {
                         spalten = frm->mask[i].pos[1] +
                                   frm->mask[i].length;
                         max = i;
                  }
           }
           return max;
}


static int GetFeldSpalteM (int sign)
/**
Spalte fuer Feld ermitteln.
**/
{
           int spalte;
           int first;

           first = GetFirstRowM (listform);
           if (menue->menFpos >= MaxListPosM (listform))
           {
                        menue->menFpos = MaxListPosM (listform);
           }
           else if (menue->menFpos < 0)
           {
                        menue->menFpos = 0;
           }
           if (listubform->mask[menue->menFpos].pos[0] != first)
           {
                        menue->menFpos += sign;
                        return (GetFeldSpalteM (sign));
           }

           if (menue->menFpos == 0)
           {
                     spalte = 0;
           }
           else
           {
                    spalte = listubform->mask[menue->menFpos].pos[1];
           }
           return spalte;
}

static int SetFeldSpalteM (void)
/**
Spalte auf Feld setzen.
**/
{
           int i;
           int spalte;
           int pos;
           int first;

           if (menue->menSpalte > listform->fieldanz - 1)
           {
               return listubform->mask[listform->fieldanz - 1].pos[1];
           }
           return listubform->mask[menue->menSpalte].pos[1];


           first = GetFirstRowM (listform);

           spalte = menue->menSpalte;
           pos = spalte;

           for (i = 0; i < listubform->fieldanz - 1; i ++)
           {
                     if (listubform->mask[i].pos[0] > first) continue;
                     if (pos >= listubform->mask[i].pos[1] &&
                         pos < listubform->mask[i + 1].pos[1])
                     {
                                 break;
                     }
           }
           menue->menFpos = i;
           if (menue->menFpos >= MaxListPosM (listform))
           {
                        menue->menFpos = MaxListPosM (listform);
           }
           if (menue->menFpos == 0)
           {
                     spalte = 0;
           }
           else
           {
                     spalte = listubform->mask[menue->menFpos].pos[1];
           }
           return spalte;
}

        
static int InsertMenueZeileM (WPARAM wParam, LPARAM lParam)
/**
Zeile in Menue einfuegen.
**/
{
        char **menArr;
        int idx;

        switch (AktMenZeile)
        {
                case 0 :
                          menArr = menue->menArr;
                          break;
                case 1 :
                          menArr = menue->menArr1;
                          break;
                case 2 :
                          menArr = menue->menArr2;
                          break;
        }
//        clipped ((char *) lParam);
        if ((int) wParam == -1 && AktMenZeile == 0)
        {
                menue->menArr[menue->menAnz] =
                         (char *)
                               GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
                               strcpy (menue->menArr[menue->menAnz],
                                       (char *) lParam);
                               idx = menue->menAnz;
                               menue->menAnz ++;
         }
         else        
         {
                 if (menArr[(int) wParam])
                 {
                           GlobalFree ((char *) menArr[(int) wParam]);
                 }
                 menArr[(int) wParam] =
                           (char *)
                                 GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
                                 strcpy ((char *) menArr[(int) wParam],
                                       (char *) lParam);
                 if ((int) wParam >= menue->menAnz)
                 {
                                  menue->menAnz = wParam + 1;
                 }
                 idx = (int) wParam;
          }
          return (idx);
}


static void SetScrollSize (void)
{
	    SCROLLINFO scinfo;

        scinfo.cbSize = sizeof (SCROLLINFO);
        scinfo.fMask  = SIF_PAGE | SIF_RANGE;
        scinfo.nPage  = 1;
        scinfo.nMin   = 0;
        scinfo.nMax   = menue->menAnz - (menue->menWzeilen - sz) / listzab,
        SetScrollInfo  (lbox, SB_VERT, &scinfo, TRUE);
}
                                       

BOOL ScrollListInsert (int idx)
{
          int i;
          
          for (i = menue->menAnz; i > idx; i --)
          {

              InsertMenueZeileM ((WPARAM) i, (LPARAM) menue->menArr[i - 1]);
              if (menue->menArr1[i - 1] != NULL)
              {
                        AktMenZeile = 1;
                        InsertMenueZeileM ((WPARAM) i, (LPARAM) menue->menArr1[i - 1]);
              }
              if (menue->menArr2[i - 1] != NULL)
              {
                        AktMenZeile = 2;
                        InsertMenueZeileM ((WPARAM) i, (LPARAM) menue->menArr2[i - 1]);
              }
              AktMenZeile = 0;
          }
          return TRUE;
}

BOOL ScrollListDelete (int idx)
{
          int i;
          

          if (menue->menAnz <= 0) return FALSE;

          for (i = idx; i < menue->menAnz - 1; i ++)
          {
              if (menue->menArr[i + 1] != NULL);
              {
                        InsertMenueZeileM ((WPARAM) i, (LPARAM) menue->menArr[i + 1]);
              }
              if (menue->menArr1[i + 1] != NULL)
              {
                        AktMenZeile = 1;
                        InsertMenueZeileM ((WPARAM) i, (LPARAM) menue->menArr1[i + 1]);
              }
              if (menue->menArr2[i + 1] != NULL)
              {
                        AktMenZeile = 2;
                        InsertMenueZeileM ((WPARAM) i, (LPARAM) menue->menArr2[i + 1]);
              }
              AktMenZeile = 0;
          }
          if (menue->menArr[i])
          {
              GlobalFree (menue->menArr[i]);
              menue->menArr [i] = NULL;
          }
          if (menue->menArr1[i])
          {
               GlobalFree (menue->menArr1[i]);
               menue->menArr1 [i] = NULL;
          }
          if (menue->menArr2[i])
          {
               GlobalFree (menue->menArr2[i]);
               menue->menArr2 [i] = NULL;
          }
          menue->menAnz --;
          if (menue->menSelect > menue->menAnz - 1)
          {
              menue->menSelect --;
          }
          SetScrollSize ();
          return TRUE;
}


BOOL ScrollList (HWND hWnd, int idx, int mode)
{
		 if (GetAktMen (hWnd) == FALSE) return 0;

         if (mode == SCINSERT)
         {
             return ScrollListInsert (idx);
         }
         if (mode == SCDELETE)
         {
             return ScrollListDelete (idx);
         }
         return FALSE;
}


static int OnCreate( HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
        CREATESTRUCT *cr;
		SIZE size;

        hdc = GetDC (hWnd);
        if (LiFont)
        { 
                  SelectObject (hdc, LiFont);
        }
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
		tm.tmAveCharWidth = size.cx;
        cr = (CREATESTRUCT *) lParam;
		Id = (DWORD) cr->hMenu;
        CreateMenueCtrl (hWnd);
        if (LiFont)
        { 
                  menue->menhFont = LiFont;
                  LiFont = NULL;
        }
        menue->menWzeilen  = (int) (double) ((double) cr->cy / (tm.tmHeight * RowHeight) - 1);
        menue->menWspalten = cr->cx / tm.tmAveCharWidth;
        menue->menZeile = 0;
        menue->menSpalte = 0;
        menue->menFpos = 0;
        menue->menSelect = -1;
        menue->menAnz = 0;
        menue->menSAnz = 0;
        menue->menhFont = 0;
		menue->rect.left = 0;
		menue->rect.top  = 0;
		menue->rect.right  = cr->cx;
		menue->rect.bottom = cr->cy;

        xchar = tm.tmAveCharWidth;
//        ychar = tm.tmHeight + tm.tmExternalLeading;
        ychar = tm.tmHeight;
        cxCaps = (tm.tmPitchAndFamily &1 ? 3 : 2) * xchar / 2;
        ReleaseDC (hWnd, hdc);
		return 0;
}

static int OnMove (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
		RECT rect;
		int cx, cy;
		SIZE size;

		if (GetAktMen (hWnd) == FALSE) return 0;

        GetClientRect (hWnd, &rect);
        hdc = GetDC (hWnd);
        if (menue->menhFont)
        {
                SelectObject (hdc, menue->menhFont);
        }
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
		tm.tmAveCharWidth = size.cx;

        ReleaseDC (lbox, hdc);

		if (tm.tmHeight == 0) return 0;;

        cx = (rect.right);
		cy = (rect.bottom);
        menue->menWzeilen  = cy / tm.tmHeight;
        menue->menWspalten = cx / tm.tmAveCharWidth;
		InvalidateRect (hWnd, &menue->rect, TRUE); 
        menue->rect.bottom = cy;
        menue->rect.right = cx;
		return 0;
}

   
static int OnSize( HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
 	    SCROLLINFO scinfo;
		SIZE size;

		if (GetAktMen (hWnd) == FALSE) return 0;
        hdc = GetDC (hWnd);
        if (menue->menhFont)
        {
                SelectObject (hdc, menue->menhFont);
        }
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
		tm.tmAveCharWidth = size.cx;
        ReleaseDC (hWnd, hdc);
        cyClient = HIWORD (lParam);
        cxClient = LOWORD (lParam);
        cxCaps = (tm.tmPitchAndFamily & 1 ? 3 : 2) * xchar / 2;
        menue->menWzeilen  = (int) (double) ((double) HIWORD (lParam) / 
					  (tm.tmHeight * RowHeight));
        menue->menWspalten = LOWORD (lParam) / tm.tmAveCharWidth;

        scinfo.cbSize = sizeof (SCROLLINFO);
        scinfo.fMask  = SIF_PAGE | SIF_RANGE;
        scinfo.nPage  = 1;
        scinfo.nMin   = 0;
		scinfo.nMax   = menue->menAnz - 
                                  (menue->menWzeilen - sz) / listzab,
        SetScrollInfo  (hWnd, SB_VERT, &scinfo, TRUE);

        scinfo.cbSize = sizeof (SCROLLINFO);
        scinfo.fMask  = SIF_PAGE | SIF_RANGE;
        scinfo.nPage  = menue->menWspalten + 1;
        scinfo.nMin   = 0;
        scinfo.nMax   = menue->menSAnz; 
        SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);

        if (menue->menSAnz)
        {
                 SetScrollPos (hWnd, SB_VERT, menue->menZeile, TRUE);
                 SetScrollPos (hWnd, SB_HORZ, menue->menSpalte, TRUE);
		}
 	    return OnMove (hWnd, msg, wParam, lParam);
}
static int OnVScroll( HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        int diff;
        RECT srect;

		if (GetAktMen (hWnd) == FALSE) return 0;
        lboxbar = (HWND) lParam;
        memcpy (&srect, &menue->rect, sizeof (RECT));
        switch (LOWORD (wParam))
        {
                case SB_LINEUP :
                     if (menue->menZeile > 0)
                     {
                            menue->menZeile -= 1;
				            srect.top = (int) (double) ((double) menue->menTAnz * UbHeight * 
				                         tm.tmHeight);
                            ScrollWindow (hWnd, 0, (int) (double) ((double) tm.tmHeight * RowHeight),
                                 &srect, &srect);
                      }
                      break;
                case SB_LINEDOWN :
                      if (menue->menZeile < menue->menAnz 
                                    -  (menue->menWzeilen - 1))
                      {
						    GetClientRect (hWnd, &srect);
                            menue->menZeile += 1;
 				            srect.top = (int) (double) ((double) menue->menTAnz * UbHeight * 
						                         tm.tmHeight);
                            ScrollWindow (hWnd, 0, (int) (double) ((double) -tm.tmHeight * RowHeight),
                                                   &srect, &srect);
                      }
                      break;
                case SB_PAGEUP :
                      SendMessage (hWnd, WM_KEYDOWN,
                                         VK_PRIOR, 0l);
                       return 0;
                case SB_PAGEDOWN :
                       SendMessage (hWnd, WM_KEYDOWN,
                                                    VK_NEXT, 0l);
                       return 0;
                case SB_THUMBPOSITION :
			            srect.top = (int) (double) ((double) menue->menTAnz * UbHeight * 
				                         tm.tmHeight);
                        diff = menue->menZeile - HIWORD (wParam);
                        menue->menZeile = HIWORD (wParam);

                        menue->menZeile = max (0, min (menue->menZeile,
                                          menue->menAnz
                                         - (menue->menWzeilen - 1) / listzab));
                         if (menue->menZeile
                                         == GetScrollPos (hWnd, SB_VERT))
                         {
                              diff = 0;
                         }

                         if (diff < 0)
                         {
                              ScrollWindow (hWnd, 0, (int) (double) 
								                   ((double) diff * tm.tmHeight * RowHeight),
                                                   &srect, &srect);
                                         srect.top = 
                                                   srect.bottom + (diff - 3) * tm.tmHeight;
                         }
                         else if (diff > 0)
                         {
                              ScrollWindow (lbox, 0, (int) (double)
								                   ((double) diff * tm.tmHeight * RowHeight),
                                                   &srect, &srect);
                         }
                         break;

                case SB_THUMBTRACK :
			             srect.top = (int) (double) ((double) menue->menTAnz * UbHeight * 
				                         tm.tmHeight);
                         diff = menue->menZeile - HIWORD (wParam);
                         menue->menZeile = HIWORD (wParam);

                         menue->menZeile = max (0, min (menue->menZeile,
                                          menue->menAnz
                                         - (menue->menWzeilen - 1) / listzab));
                         if (menue->menZeile
                                         == GetScrollPos (hWnd, SB_VERT))
                         {
                                     diff = 0;
                         }

                         if (diff < 0)
                         {
                                         ScrollWindow (lbox, 0, (int) (double)
								                   ((double) diff * tm.tmHeight * RowHeight),
                                                   &srect, &srect);
                         }
                         else if (diff > 0)
                         {
                                         ScrollWindow (lbox, 0, (int) (double)
								                   ((double) diff * tm.tmHeight * RowHeight),
                                                   &srect, &srect);
                         }
                         break;

                case SB_ENDSCROLL :
                default :
                         return DefWindowProc(hWnd, msg, wParam, lParam);

          }
          menue->menZeile = max (0, min (menue->menZeile,
                                         menue->menAnz
                                         - (menue->menWzeilen - 1) / listzab));
          if (menue->menZeile
                         != GetScrollPos (hWnd, SB_VERT))
          {
                          SetScrollPos (hWnd, SB_VERT, menue->menZeile,
                                        TRUE);
						  TestRec = FALSE;
                          UpdateWindow (hWnd);
						  TestRec = TRUE;
          }
          return 0;			
}

static int OnHScroll( HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
	    RECT srect; 
		int scsize;

		if (GetAktMen (hWnd) == FALSE) return 0;
        memcpy (&srect, &menue->rect, sizeof (RECT));
        switch (LOWORD (wParam))
        {
                case SB_LINEUP :
                      if (menue->menSpalte <= 0) return 0;
                      menue->menSpalte -= 1;
                      menue->menFpos --;
                      ScrollWindow (hWnd, tm.tmAveCharWidth, 0,
                                 &srect, &srect);
                      if (listenter)
                      {
                           menue->menSpalte =
                                   GetFeldSpalteM (-1);
                      }
                      InvalidateRect (lbox, NULL, TRUE);
                      break;
                case SB_LINEDOWN :
                      if (menue->menSpalte >= menue->menSAnz - menue->menWspalten) return 0;
                      menue->menSpalte += 1;
                      menue->menFpos ++;
                      ScrollWindow (hWnd, -tm.tmAveCharWidth, 0,
                                 &srect, &srect);
                      if (listenter)
                      {
                           menue->menSpalte =
                                   GetFeldSpalteM (1);
                      }
                      break;
                case SB_PAGEUP :
                      menue->menSpalte -= menue->menWspalten;
					  scsize = menue->menWspalten;
					  if (menue->menSpalte < 0)
					  {
						  scsize += menue->menSpalte;
					  }
                      ScrollWindow (hWnd, scsize * tm.tmAveCharWidth, 0,
                                 &srect, &srect);
                      InvalidateRect (hWnd, NULL, TRUE);
                      break;
                case SB_PAGEDOWN :
                      menue->menSpalte += menue->menWspalten;
					  scsize = menue->menWspalten;
					  if (menue->menSpalte > menue->menSAnz - menue->menWspalten)
					  {
						  scsize -= menue->menSpalte > menue->menSAnz - menue->menWspalten;
                          menue->menSpalte -= (menue->menWspalten - scsize);
					  }
					  
                      ScrollWindow (hWnd, -(scsize * tm.tmAveCharWidth), 0,
                                 &srect, &srect);
                      InvalidateRect (hWnd, NULL, TRUE);
                      break;
                case SB_THUMBPOSITION :
                case SB_THUMBTRACK :
					  scsize = menue->menSpalte -  HIWORD (wParam);
                      menue->menSpalte =   HIWORD (wParam);

                      ScrollWindow (hWnd, scsize * tm.tmAveCharWidth, 0,
                                 &srect, &srect);
                      InvalidateRect (hWnd, NULL, TRUE);
                       break;
                case SB_ENDSCROLL :
                default :
                       return DefWindowProc(hWnd, msg,
                                                     wParam, lParam);
       }
       if (listenter == 0)
       {
                menue->menSpalte = max (0, min (menue->menSpalte,
                                       menue->menSAnz - menue->menWspalten));
       }
       if (listenter == 0 && 
                       menue->menSpalte != GetScrollPos (hWnd, SB_HORZ))
       {
                  SetScrollPos (hWnd, SB_HORZ, menue->menSpalte, TRUE);  
                  if (title_mode == BUTTON)
                  {
				           ScrollBuTitleM ();
                  }
//                  InvalidateRect (hWnd, NULL, TRUE);
				  TestRec = FALSE;
                  UpdateWindow (hWnd);
			      TestRec = TRUE;
       }
       else if (listenter && 
                        menue->menFpos != GetScrollPos (hWnd, SB_HORZ))
       {
                  SetScrollPos (hWnd, SB_HORZ, menue->menFpos, TRUE);  
  			      if (title_mode == BUTTON)
                  {
				       ScrollBuTitleM ();
                  }
                  InvalidateRect (hWnd, NULL, TRUE);
	  }
      return 0;
}

static int OnKeydown( HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        RECT srect;
		int scsize;

		if (GetAktMen (hWnd) == FALSE) return 0;
		if (hWnd != lbox) return 0;
        memcpy (&srect, &menue->rect, sizeof (RECT));
        switch (wParam)
        {
             case VK_DOWN :
                 if (MenSelect == FALSE)
				 {
                       if (menue->menZeile < menue->menAnz 
                                     -  (menue->menWzeilen - 1))
                       {
                                menue->menZeile ++;
                                SetScrollPos (hWnd, SB_VERT,
                                            menue->menZeile, TRUE);
					            srect.top = (int) (double) ((double) menue->menTAnz * UbHeight * 
						                         tm.tmHeight);
                                ScrollWindow (lbox, 0, (int) (double) ((double) -tm.tmHeight * RowHeight),
                                                    &srect, &srect);
//                                InvalidateRect (hWnd, &srect, TRUE);
				                TestRec = FALSE;
								UpdateWindow (hWnd);
				                TestRec = TRUE;
                       }
                       break;
				 }
                 if (menue->menSelect < menue->menAnz - 1)
                 {
                      ShowRowM (hWnd, BLACKCOL, WHITECOL);
                               menue->menSelect ++;
				      if (selcolor)
                      {
                             ShowRowM (hWnd, SelColor, SelBkColor);
					  }
                 }
                 if ((menue->menSelect - menue->menZeile) * listzab >=
                                          menue->menWzeilen - sz)
                 {
                      menue->menZeile ++;
                      SetScrollPos (hWnd, SB_VERT,
                                    menue->menZeile, 
                                    TRUE);
					  srect.top = (int) (double) ((double) menue->menTAnz * UbHeight * 
						                  tm.tmHeight);
                      InvalidateRect (hWnd, &srect, TRUE);
                 }
  	             SendMessage (GetParent (hWnd), WM_COMMAND,
                                         MAKELONG (Id, LBN_SELCHANGE),	
			                             (LPARAM) hWnd); 
                 return 0;
            case VK_UP :
                 if (MenSelect == FALSE)
				 {
                      if (menue->menZeile > 0)
                      {
                              menue->menZeile --;
                              SetScrollPos (hWnd, SB_VERT,
                                            menue->menZeile, TRUE);
					          srect.top = (int) (double) ((double) menue->menTAnz * UbHeight * 
						                  tm.tmHeight);
                              ScrollWindow (lbox, 0, (int) (double) ((double) tm.tmHeight * RowHeight),
                                                    &srect, &srect);
//                              InvalidateRect (hWnd, &srect, TRUE);
			                  TestRec = FALSE;
							  UpdateWindow (hWnd);
				              TestRec = TRUE;
                      }
                      break;
				 }
                 if (menue->menSelect > 0)
                 {
                      ShowRowM (hWnd, BLACKCOL, WHITECOL);
                      menue->menSelect --;
 				      if (selcolor)
                      {
                              ShowRowM (hWnd, SelColor, SelBkColor);
					  }
                 }
                 if (menue->menSelect < menue->menZeile)
                 {
                      menue->menZeile --;
                      SetScrollPos (hWnd, SB_VERT,
                                             menue->menZeile, TRUE);
					  srect.top = (int) (double) ((double) menue->menTAnz * UbHeight * 
						                  tm.tmHeight);
                      InvalidateRect (hWnd, &srect, TRUE);
                 }
			     SendMessage (GetParent (hWnd), WM_COMMAND,
                                         MAKELONG (Id, LBN_SELCHANGE),	
			                             (LPARAM) hWnd); 
                 return 0;
           case VK_RIGHT :
                if (menue->menSpalte <  5 + menue->menSAnz -
                           menue->menWspalten ||
                            listenter)
                {
                       menue->menSpalte ++;
                       menue->menFpos ++;
                       if (listenter)
                       {
                           menue->menSpalte =
                                              GetFeldSpalteM (1);
                           SetScrollPos (hWnd, SB_HORZ,
                                                    menue->menFpos, TRUE);
                        }
                        else
                        {
                           SetScrollPos (hWnd, SB_HORZ,
                                         menue->menSpalte, TRUE);
                        }
                        if (title_mode == BUTTON)
						{
							             ScrollBuTitleM ();
						}
                        InvalidateRect (hWnd, NULL, TRUE);
				}
                return 0;
           case VK_LEFT :
                if (menue->menSpalte > 0)
                {
                        menue->menSpalte --;
                        menue->menFpos --;
                        if (listenter)
                        {
                               menue->menSpalte =
                                              GetFeldSpalteM (-1);
                               SetScrollPos (hWnd, SB_HORZ,
                                              menue->menFpos, 
                                              TRUE);
                        }
                        else
                        {
                               SetScrollPos (hWnd, SB_HORZ,
                                             menue->menSpalte, 
                                             TRUE);
                        }
 				        if (title_mode == BUTTON)
						{
				              ScrollBuTitleM ();
						}
                        InvalidateRect (hWnd, NULL, TRUE);
                 }
                 return 0;
            case VK_NEXT :
                 if (menue->menSelect - menue->menZeile <
                             (menue->menWzeilen - 1) / listzab - sz)
                 {
                        ShowRowM (hWnd, BLACKCOL, WHITECOL);
                        menue->menSelect = menue->menZeile +
                                                 (menue->menWzeilen
                                                  - 1) / listzab - sz;
                        if (menue->menSelect > menue->menAnz - 1)
                        {
                                 menue->menSelect = menue->menAnz - 1;
                        }
                        ShowRowM (hWnd, SelColor, SelBkColor);
				 }
                 else
                 {
					   scsize = menue->menZeile;
                       menue->menZeile += (menue->menWzeilen / listzab
                                              - sz);

                       menue->menZeile = min (menue->menZeile,
                                          sz + menue->menAnz
                                          - menue->menWzeilen / listzab);
					   scsize = scsize - menue->menZeile;
                       menue->menSelect = menue->menZeile;
				       SendMessage (GetParent (hWnd), WM_COMMAND,
                                           MAKELONG (Id, LBN_SELCHANGE),	
			                               (LPARAM) hWnd); 
                       SetScrollPos (hWnd, SB_VERT,
                                                    menue->menZeile * listzab, 
                                                    TRUE);
                       InvalidateRect (hWnd, &menue->rect, TRUE);
                  }
                  return 0;
             case VK_PRIOR :
                  if (menue->menSelect > menue->menZeile)
                  {
                       ShowRowM (hWnd, BLACKCOL, WHITECOL);
                       menue->menSelect = menue->menZeile;
                       ShowRowM (hWnd, SelColor, SelBkColor);
                  }
                  else if (menue->menZeile > 0)
                  {
					    scsize = menue->menZeile;
                        menue->menZeile -= (menue->menWzeilen / listzab
                                                 - sz);
                        menue->menZeile = max (0, menue->menZeile);
					    scsize = scsize - menue->menZeile;
                        menue->menSelect = menue->menZeile +
                                                (menue->menWzeilen
                                                  - 1) / listzab - sz;
			            SendMessage (GetParent (hWnd), WM_COMMAND,
                                           MAKELONG (Id, LBN_SELCHANGE),	
			                               (LPARAM) hWnd); 
                        SetScrollPos (hWnd, SB_VERT,
                                                    menue->menZeile * listzab, 
                                                    TRUE);
                        InvalidateRect (hWnd, &menue->rect, TRUE);
  						return 0;
             case VK_RETURN :
				        SendMessage (GetParent (hWnd), WM_COMMAND, 
                                     MAKELONG (Id, VK_RETURN), (LPARAM) hWnd);
						return 0;
                   }
                   return 0;
      }
      return 0;
}

static int OnButtondown( HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;

		if (GetAktMen (hWnd) == FALSE) return 0;

        GetCursorPos (&mousepos);

        if (!mouseindialog (hWnd, &mousepos)) return TRUE;
        if (IsDlgClient (hWnd, &mousepos))
        {
			       SetFocus (hWnd);
                   SelectRowM (hWnd, wParam, lParam);
                   SetTimer (hWnd, 1, 200, 0);  
                   InTimer = 1;
                   MousePressed = TRUE; 
                   return 0;
        }
		return 0;
}

static int OnButtonup( HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
        int x,y;
        int xchar, ychar;
        TEXTMETRIC tm;

		if (GetAktMen (hWnd) == FALSE) return 0;

        KillTimer (hWnd, 1); 
        MousePressed = FALSE;
        InTimer = 0;

        hdc = GetDC (hWnd);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (hWnd, hdc);
        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight;
        x = LOWORD (lParam);
        y = HIWORD (lParam);
		if (y < (int) (double) ((double) (sz * UbHeight * ychar))) 
        {
            TestSort (x, y);
        }
		return 0;
}


static int OnTimer( HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
        int diff;

		if (GetAktMen (hWnd) == FALSE) return 0;

        GetCursorPos (&mousepos);

        if (IsDlgClient (hWnd, &mousepos)) return 0;


        if (GetKeyState (VK_LBUTTON) >= 0)
        {
                    KillTimer (hWnd, 1); 
                    MousePressed = 0;
                    InTimer  = 0;
        }

        diff = MousetohWnd (hWnd, &mousepos);
        if (diff == 1)
        { 
                    if (InTimer == 2)
                    {
                            KillTimer (hWnd, 1); 
                            SetTimer (hWnd, 1, 200, 0);  
                            InTimer = 1;
                    }
                    SendMessage (hWnd, WM_KEYDOWN,
                                                  VK_DOWN, lParam);
         }
         else if (diff == 2)
         { 
                    if (InTimer == 1)
                    {
                             KillTimer (hWnd, 1); 
                             SetTimer (hWnd, 1, 50, 0);  
                             InTimer = 2;
                    }
                    SendMessage (hWnd, WM_KEYDOWN,
                                                  VK_DOWN, lParam);
          }
          else if (diff == -1)
          { 
                    if (InTimer == 2)
                    {
                             KillTimer (hWnd, 1); 
                             SetTimer (hWnd, 1, 200, 0);  
                             InTimer = 1;
                    }
                    SendMessage (hWnd, WM_KEYDOWN,
                                                  VK_UP, lParam);
          }
          else if (diff == -2)
          { 
                    if (InTimer == 1)
                    {
                              KillTimer (hWnd, 1); 
                              SetTimer (hWnd, 1, 50, 0);  
                              InTimer = 2;
                     }
                     SendMessage (hWnd, WM_KEYDOWN,
                                                  VK_UP, lParam);
           }
           return 0;
}

static int OnLButtonDblClk (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;

		if (GetAktMen (hWnd) == FALSE) return 0;

        GetCursorPos (&mousepos);

        if (!mouseindialog (hWnd, &mousepos)) return TRUE;
        if (IsDlgClient (hWnd, &mousepos))
        {
                if (SelectRowM (hWnd, wParam, lParam))
				{
                   if (WithDblClck)
				   {
                     SendMessage (GetParent (hWnd), WM_COMMAND, 
                                       MAKELONG (Id, LBN_DBLCLK), (LPARAM) hWnd);
				   }
				}
                return 0;
         }
		 return 0; 
}

static int OnMouseMove (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;

		if (GetAktMen (hWnd) == FALSE) return 0;

        GetCursorPos (&mousepos);

        if (!mouseindialog (hWnd, &mousepos)) return TRUE;

        if (wParam & MK_LBUTTON)
        {
                SelectRowM (hWnd, wParam, lParam);
                return TRUE;
        }
		return 0;
}

static int OnVPos (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
		if (GetAktMen (hWnd) == FALSE) return 0;

        clipped ((char *) lParam);
        if (strlen ((char *) lParam) == 0)
        {
			      menue->menVlpos = NULL;
                  return 0;
        }
        menue->menVlpos = (char *)    GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
        strcpy (menue->menVlpos, (char *) lParam);
		vlines = hlines = 0;
		if ((int) wParam == 1)
		{
		        vlines = TRUE;
		}
		else if ((int) wParam == 2)
		{
		        hlines = TRUE;
		}
		else if ((int) wParam == 3)
		{
		        hlines = TRUE;
		        vlines = TRUE;
		}
		SetAktMen (hWnd);
        return 0;
}

static int OnRPos (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
		if (GetAktMen (hWnd) == FALSE) return 0;

        clipped ((char *) lParam);
        if (strlen ((char *) lParam) == 0)
        {
			      menue->menRowpos = NULL;
                  return 0;
        }
        menue->menRowpos = (char *) GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
        strcpy (menue->menRowpos, (char *) lParam);
		SetAktMen (hWnd);
        return 0;
}

static int OnRowAttr (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        int anz;
        int i;
        
		if (GetAktMen (hWnd) == FALSE) return 0;

        clipped ((char *) lParam);
        if (strlen ((char *) lParam) == 0)
        {
			      menue->menRowAttr = NULL;
                  return 0;
        }

        anz = wsplit ((char *) lParam, " ");
        menue->menRowAttr = new char * [anz + 2];
        if (menue->menRowAttr == NULL)
        {
            return 0;
        }

        for (i = 0; i < anz; i ++)
        {
            menue->menRowAttr [i] = new char [256];
            if (menue->menRowAttr[i] == NULL)
            {
                    return 0;
            }
            strcpy (menue->menRowAttr [i], wort[i]);
        }
        menue->menRowAttr[i] = NULL;
		SetAktMen (hWnd);
        return 0;
}

static int OnRowSize (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
		if (GetAktMen (hWnd) == FALSE) return 0;

        RowHeight = (double) ((double) lParam / 100);
		SetAktMen (hWnd);
        return 0;
}

static int OnCaptSize (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
		if (GetAktMen (hWnd) == FALSE) return 0;

		if (NoListTitle) return 0;
        UbHeight = (double) ((double) lParam / 100);
		SetAktMen (hWnd);
        return 0;
}


static int OnTitle (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
		if (GetAktMen (hWnd) == FALSE) return 0;


        clipped ((char *) lParam);
        if (strlen ((char *) lParam) == 0)
        {
                                     return 0;
        }
        menue->menTitle[menue->menTAnz] = (char *)
                                    GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
        strcpy (menue->menTitle[menue->menTAnz],
                                      (char *) lParam);
        menue->menTAnz ++;
        sz = menue->menTAnz;
        menue->trect.top     = (int) (double) ((double) menue->menTAnz * tm.tmHeight * UbHeight);
        menue->trect.bottom = (int) (double) ((double) menue->menTAnz * tm.tmHeight  * UbHeight);
		NoListTitle = FALSE;
 	    if (UbHeight == 0.0)
		{
			 UbHeight = 1.0;
		}
		SetAktMen (hWnd);
        InvalidateRect (hWnd, NULL, TRUE);
        return 0;
}


static void ListScrollInfo (HWND hWnd)
{
        int i;
        int len;
        int Slen;
 	    SCROLLINFO scinfo;

        Slen = 0;
        for (i = 0; i < menue->menAnz; i ++)
        {
            len = strlen (menue->menArr[i]);
            if (len > Slen)
            {
                Slen = len;
            }
            if (listzab > 1)
            {
                    len = strlen (menue->menArr1[i]);
                    if (len > Slen)
                    {
                          Slen = len;
                    }
            }
            if (listzab > 2)
            {
                    len = strlen (menue->menArr2[i]);
                    if (len > Slen)
                    {
                          Slen = len;
                    }
            }
        }
        Slen ++;
        scinfo.cbSize = sizeof (SCROLLINFO);
        scinfo.fMask  = SIF_PAGE | SIF_RANGE;
        scinfo.nPage  = 1;
        scinfo.nMin   = 0;
        scinfo.nMax   = menue->menAnz - (menue->menWzeilen - sz) / listzab,
        SetScrollInfo  (hWnd, SB_VERT, &scinfo, TRUE);
        if (Slen > menue->menSAnz)
        {
                  menue->menSAnz = Slen; 

                  if (listform && listenter)
                  {
                            scinfo.cbSize = sizeof (SCROLLINFO);
                            scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                            scinfo.nPage  = 1;
           	                scinfo.nMin   = 0;
                            scinfo.nMax   = listform->fieldanz - 0; 
                            SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                  }
                  else if (listenter == 0)
                  {
                            scinfo.cbSize = sizeof (SCROLLINFO);
                            scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                            scinfo.nPage  = menue->menWspalten + 1;
          	                scinfo.nMin   = 0;
                            scinfo.nMax   = menue->menSAnz; 
                            SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                  }


         }
}


static int OnAddList (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        int idx;

		if (GetAktMen (hWnd) == FALSE) return 0;

        if (lParam == NULL)
        {
             ListScrollInfo (hWnd);
             return 0;
        }

        idx = InsertMenueZeileM (wParam, lParam);
        return 0;
}


int DoInsertList (HWND hWnd, WPARAM wParam,LPARAM lParam)
{
        int idx;
        int Slen;

		if (GetAktMen (hWnd) == FALSE) return 0;

                                      
        idx = InsertMenueZeileM (wParam, lParam);
        if (Slen > menue->menSAnz)
        {
                  menue->menSAnz = Slen; 

        }
 	    return 0;
}


int DoLastList (HWND hWnd, WPARAM wParam,LPARAM lParam)
{
        int Slen;
		RECT rect;
 	    SCROLLINFO scinfo;

		if (GetAktMen (hWnd) == FALSE) return 0;

                                      
        scinfo.cbSize = sizeof (SCROLLINFO);
        scinfo.fMask  = SIF_PAGE | SIF_RANGE;
        scinfo.nPage  = menue->menWzeilen;
        scinfo.nMin   = 0;
        scinfo.nMax   = menue->menAnz;
        SetScrollInfo  (hWnd, SB_VERT, &scinfo, TRUE);
        Slen = (int) strlen ((char *) lParam) + 22 * cxCaps / xchar ;
        Slen = (int) strlen ((char *) lParam) + 1;
        if (Slen > menue->menSAnz)
        {
                  menue->menSAnz = Slen; 

                  if (listform && listenter)
                  {
                            scinfo.cbSize = sizeof (SCROLLINFO);
                            scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                            scinfo.nPage  = 1;
           	                scinfo.nMin   = 0;
                            scinfo.nMax   = listform->fieldanz - 0; 
                            SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                  }
                  else if (listenter == 0)
                  {
                            scinfo.cbSize = sizeof (SCROLLINFO);
                            scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                            scinfo.nPage  = menue->menWspalten + 1;
          	                scinfo.nMin   = 0;
                            scinfo.nMax   = menue->menSAnz; 
                            SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                  }


         }

         if (menue->menAnz * listzab < menue->menZeile +  menue->menWzeilen)
         {
                   SetUpdRegM (&rect, menue->menAnz - 1); 
                   InvalidateRect (hWnd, &menue->rect, TRUE);
         }
         else if ((int) wParam != -1)
         {
                   SetUpdRegM (&rect, menue->menAnz - 1); 
                   InvalidateRect (hWnd, &menue->rect, TRUE);
         }

		 return 0;
}

static int OnInsertList (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        int idx;
        int Slen;
		RECT rect;
 	    SCROLLINFO scinfo;

		if (GetAktMen (hWnd) == FALSE) return 0;

                                      
        idx = InsertMenueZeileM (wParam, lParam);
        scinfo.cbSize = sizeof (SCROLLINFO);
        scinfo.fMask  = SIF_PAGE | SIF_RANGE;
//        scinfo.nPage  = 1;
        scinfo.nPage  = menue->menWzeilen;
        scinfo.nMin   = 0;
//        scinfo.nMax   = menue->menAnz - (menue->menWzeilen - sz) / listzab;
        scinfo.nMax   = menue->menAnz;
        SetScrollInfo  (hWnd, SB_VERT, &scinfo, TRUE);
        Slen = (int) strlen ((char *) lParam) + 22 * cxCaps / xchar ;
        Slen = (int) strlen ((char *) lParam) + 1;
        if (Slen > menue->menSAnz)
        {
                  menue->menSAnz = Slen; 

                  if (listform && listenter)
                  {
                            scinfo.cbSize = sizeof (SCROLLINFO);
                            scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                            scinfo.nPage  = 1;
           	                scinfo.nMin   = 0;
                            scinfo.nMax   = listform->fieldanz - 0; 
                            SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                  }
                  else if (listenter == 0)
                  {
                            scinfo.cbSize = sizeof (SCROLLINFO);
                            scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                            scinfo.nPage  = menue->menWspalten + 1;
          	                scinfo.nMin   = 0;
                            scinfo.nMax   = menue->menSAnz; 
                            SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                  }


         }

         if (menue->menAnz * listzab < menue->menZeile +  menue->menWzeilen)
         {
                   SetUpdRegM (&rect, idx); 
                   InvalidateRect (hWnd, &menue->rect, TRUE);
         }
         else if ((int) wParam != -1)
         {
                   SetUpdRegM (&rect, idx); 
                   InvalidateRect (hWnd, &menue->rect, TRUE);
         }

		 return 0;
}

static int OnGetCursel (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{

		if (GetAktMen (hWnd) == FALSE) return 0;
        if (menue->menAnz == 0) return -1;
        return (menue->menSelect);
}


int GetLboxText (HWND hWnd, int idx, char * text)
{

		if (GetAktMen (hWnd) == FALSE) return 0;
		strcpy (text, menue->menArr[idx]);
        return (0);
}

static int OnGetText (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
	    int idx; 

		if (GetAktMen (hWnd) == FALSE) return 0;
        idx = (int) wParam;
		strcpy ((char *) lParam, menue->menArr[idx]);
        return (0);
}

static int OnGetCount (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
		if (GetAktMen (hWnd) == FALSE) return 0;
        return menue->menAnz;
}

static int OnResetContent (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
	    int i; 

		if (GetAktMen (hWnd) == FALSE) return 0;

        for (i = 0; i < menue->menAnz; i ++)
        {
                    if (menue->menArr[i])
                    {
                            GlobalFree (menue->menArr[i]);
                            menue->menArr [i] = NULL;
                    }
                    if (menue->menArr1[i])
                    {
                            GlobalFree (menue->menArr1[i]);
                            menue->menArr1 [i] = NULL;
                    }
                    if (menue->menArr2[i])
                    {
                             GlobalFree (menue->menArr2[i]);
                             menue->menArr2 [i] = NULL;
                    }
        }
		menue->menAnz = 0;
		InvalidateRect (hWnd, &menue->rect, TRUE);
		UpdateWindow (hWnd);
        return (0);
}

static int OnSetFocus (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
//        SendMessage (GetParent (hWnd), WM_COMMAND, 
//                                       MAKELONG (Id, WM_SETFOCUS), (LPARAM) hWnd);

		if (GetAktMen (hWnd) == FALSE) return 0;

        SendMessage (GetParent (hWnd), WM_COMMAND, 
                                       MAKELONG (Id, LBN_SETFOCUS), (LPARAM) hWnd);

        return (0);
}

static int OnKillFocus (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
//        SendMessage (GetParent (hWnd), WM_COMMAND, 
//                                       MAKELONG (Id, WM_KILLFOCUS), (LPARAM) hWnd);
        SendMessage (GetParent (hWnd), WM_COMMAND, 
                                       MAKELONG (Id, LBN_KILLFOCUS), (LPARAM) hWnd);
        return (0);
}

static int OnSetCursel (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
	    

		if (GetAktMen (hWnd) == FALSE) return 0;

        menue->menSelect = (int) wParam;
        menue->menZeile  = (int) wParam;
		if (menue->menZeile > (menue->menAnz - menue->menWzeilen))
		{
			menue->menZeile = max (0, menue->menAnz - menue->menWzeilen + 1);
		}
        SetScrollPos (hWnd, SB_VERT, menue->menZeile * listzab, TRUE);
        InvalidateRect (hWnd, &menue->rect, TRUE);
        UpdateWindow (hWnd);
		ShowMenueM (hWnd, 0);
        return 0;
}

static int OnSort (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
	    

		if (GetAktMen (hWnd) == FALSE) return 0;

        SetSortRow ((int) wParam);
        SortList ();

		ShowMenueM (hWnd, 0);
        return 0;
}

static int OnSetFont (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
 	    SCROLLINFO scinfo;
		SIZE size;

		if (GetAktMen (hWnd) == FALSE) return 0;

        menue->menhFont = (HFONT) wParam;
        hdc = GetDC (hWnd);
        SelectObject (hdc, menue->menhFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
		tm.tmAveCharWidth = size.cx;
        ReleaseDC (hWnd, hdc);
        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight + tm.tmExternalLeading;
        cxCaps = (tm.tmPitchAndFamily & 1 ? 3 : 2) * xchar / 2;
//        menue->menWzeilen  = (int) (double) ((double )cyClient / (tm.tmHeight * RowHeight) + 1);
        menue->menWzeilen  = (int) (double) ((double )cyClient / (tm.tmHeight * RowHeight));
        menue->menWspalten = cxClient / tm.tmAveCharWidth;
        if (menue->menAnz * listzab > menue->menWzeilen)
        {
                scinfo.cbSize = sizeof (SCROLLINFO);
                scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                scinfo.nPage  = 1;
                scinfo.nMin   = 0;
                scinfo.nMax   = menue->menAnz - (menue->menWzeilen - 0) / listzab,
                SetScrollInfo  (hWnd, SB_VERT, &scinfo, TRUE);
        }
        else if (menue->menSAnz > menue->menWspalten)
        {
                SetScrollRange (hWnd, SB_HORZ, 0,
                                     5 + menue->menSAnz * listzab 
                                    - menue->menWspalten, TRUE);

                scinfo.cbSize = sizeof (SCROLLINFO);
                scinfo.fMask  = SIF_PAGE;
                if (listform && listenter)
				{
					scinfo.nPage  = 1;
				}
				else if (listenter == 0)
				{
                    scinfo.nPage  = menue->menWspalten + 1;
				}
                SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
         }
         SetScrollPos (hWnd, SB_VERT, menue->menZeile * listzab, TRUE);
         SetScrollPos (hWnd, SB_HORZ, menue->menSpalte * listzab, TRUE);
         return 0;
}

static int OnDestroy (HWND hWnd,UINT msg,  WPARAM wParam,LPARAM lParam)
{
	    int i;

        if (GetAktMen (hWnd) == FALSE) return 0;

        lboxbar = NULL;
        for (i = 0; i < menue->menAnz; i ++)
        {
                    if (menue->menArr[i])
                    {
                            GlobalFree (menue->menArr[i]);
                            menue->menArr [i] = NULL;
                    }
                    if (menue->menArr1[i])
                    {
                            GlobalFree (menue->menArr1[i]);
                            menue->menArr1 [i] = NULL;
                    }
                    if (menue->menArr2[i])
                    {
                             GlobalFree (menue->menArr2[i]);
                             menue->menArr2 [i] = NULL;
                    }
          }
          if (menue->menTitle[0])
          {
                    GlobalFree (menue->menTitle[0]);
		            menue->menTitle[0] = NULL;
          }
          if (menue->menVlpos)
          {
                    GlobalFree (menue->menVlpos);
  				    menue->menVlpos = NULL;
          }

          if (menue->menRowpos)
          {
                    GlobalFree (menue->menRowpos);
  				    menue->menRowpos = NULL;
          }

          if (menue->menRowAttr)
          {
                    for ( i = 0; menue->menRowAttr[i] != NULL; i ++)
                    {
                        delete menue->menRowAttr[i];
                    }
                    delete menue->menRowAttr;
  				    menue->menRowAttr = NULL;
          }

          if (menue->menhFont)
          {
                     DeleteObject (menue->menhFont);
								  menue->menhFont = NULL;
          }
		  DestroyMenueCtrl (hWnd);
		  MenSelect = TRUE;
		  return 0;
}
		 
		 
LONG FAR PASCAL ListProcM (HWND hWnd,UINT msg,
						   WPARAM wParam,LPARAM lParam)
{
	    HWND OldFocus;

        switch(msg)
        {
              case WM_CREATE :
				    return OnCreate (hWnd, msg, wParam, lParam);
              case WM_SIZE :
				    return OnSize (hWnd, msg, wParam, lParam);
              case WM_VSCROLL :
				    return OnVScroll (hWnd, msg, wParam, lParam);
              case WM_HSCROLL :
				    return OnHScroll (hWnd, msg, wParam, lParam);
              case WM_PAINT :
                    ShowMenueM (hWnd, 0);
                    return TRUE;
              case WM_MOVE :
				    return OnMove (hWnd, msg, wParam, lParam);
              case WM_KEYDOWN :
				    return OnKeydown (hWnd, msg, wParam, lParam);
              case WM_LBUTTONDOWN :
				    return OnButtondown (hWnd, msg, wParam, lParam);
              case WM_LBUTTONUP :
				    return OnButtonup (hWnd, msg, wParam, lParam);
               case WM_TIMER :
				    return OnTimer (hWnd, msg, wParam, lParam);
              case WM_LBUTTONDBLCLK :
				    return OnLButtonDblClk (hWnd, msg, wParam, lParam);
              case WM_MOUSEMOVE :
				    return OnMouseMove (hWnd, msg, wParam, lParam);
              case LB_VPOS :
				    return OnVPos (hWnd, msg, wParam, lParam);
              case LB_RPOS :
				    return OnRPos (hWnd, msg, wParam, lParam);
              case LB_ROWATTR :
				    return OnRowAttr (hWnd, msg, wParam, lParam);
              case LB_ROWSIZE :
				    return OnRowSize (hWnd, msg, wParam, lParam);
              case LB_CAPTSIZE :
				    return OnCaptSize (hWnd, msg, wParam, lParam);
              case LB_TITLE :
				    return OnTitle (hWnd, msg, wParam, lParam);
              case LB_ADDSTRING :
				    return OnAddList (hWnd, msg, wParam, lParam);
              case LB_INSERTSTRING :
				    return OnInsertList (hWnd, msg, wParam, lParam);
              case LB_GETCURSEL :
                  return OnGetCursel (hWnd, msg, wParam, lParam);
              case LB_GETTEXT :
 				    return OnGetText (hWnd, msg, wParam, lParam);
              case LB_GETCOUNT :
 				    return OnGetCount (hWnd, msg, wParam, lParam);
              case LB_SETCURSEL :
 				    return OnSetCursel (hWnd, msg, wParam, lParam);
              case LB_RESETCONTENT :
 				    return OnResetContent (hWnd, msg, wParam, lParam);
              case LB_SORT :
 				    return OnSort (hWnd, msg, wParam, lParam);
              case WM_SETFONT :
 				    return OnSetFont (hWnd, msg, wParam, lParam);
              case WM_SETFOCUS :
				    return OnSetFocus (hWnd, msg, wParam, lParam); 
				    OldFocus = SetFocus (hWnd);
                    break;
              case WM_KILLFOCUS :
				    return OnKillFocus (hWnd, msg, wParam, lParam); 
			  case WM_DESTROY :
 				    return OnDestroy (hWnd, msg, wParam, lParam);
		}
        return DefWindowProc(hWnd, msg, wParam, lParam);
}
