#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbfunc.h"
#include "a_bas.h"
#include "ls.h"
#include "mo_meld.h"
#include "mo_qa.h"

LS_CLASS ls_class;

static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
        return 0;
}


int QueryClass::querya (HWND hWnd)
/**
Query ueber Textnummer.
**/
{

        HANDLE hMainInst;

        static char aval[41];
        static char a_bz1val[41];
        static char hwgval[41];
        static char wgval[41];
        static char agval[41];
        static char a_typval[41];

        static ITEM iaval ("a", 
                           aval, 
                             "Artikel-Nr.......:", 
                           0);

        static ITEM ia_bz1val ("a_bz1", 
                                a_bz1val, 
                             "Bezeichnung......:", 
                                   0);
        static ITEM ihwgval ("hwg", 
                             hwgval, 
                             "Hauptwarengruppe.:", 
                             0);

        static ITEM iwgval ("wg", 
                            wgval, 
                             "Warengruppe......:", 

                            0);
        static ITEM iagval ("ag", 
                            agval, 
                             "Artikelgruppe....:", 
                             0);


        static ITEM ia_typval ("a_typ", 
                                a_typval, 
                             "Artikel-Typ......:", 
                                   0);

        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);

        static field _qtxtform[] = {
           &iaval,      40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ia_bz1val,  40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ihwgval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &iwgval,     40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iagval,     40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ia_typval,  40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 8,15, 0, "", BUTTON, 0, 0,KEY12,
           &iCA,        15, 0, 8,32, 0, "", BUTTON, 0, 0,KEY5,
		};

        static form qtxtform = {8, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"a", "a_bz1", "hwg", "wg", "ag",
                                 "a_typ",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (11, 62, 9, 10, hMainInst,
                               "Suchkriterien f�r Artikel");
        enter_form (query, &qtxtform, 0, 0);

        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEY10) 
        {
                  Preparea_basQuery (&qtxtform, qnamen);

                  Showa_basQuery (hWnd, 0);
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}

int QueryClass::queryauf (HWND hWnd)
/**
Query ueber Textnummer.
**/
{

        HANDLE hMainInst;

        static char aufval[41];
        static char kunfilval[41];
        static char kunval[41];
        static char ldatval[41];
        static char statval[41];

        static ITEM iaufval ("auf", 
                             aufval, 
                             "Auftrag-Nr.......:", 
                             0);

        static ITEM ikunfilval ("kun_fil", 
                                 kunfilval, 
                                "Kunde/Filiale....:", 
                                 0);

        static ITEM ikunval    ("kun", 
                                 kunval, 
                                "Kunde............:", 
                                 0);

        static ITEM ildatval ("lieferdat", 
                             ldatval, 
                             "Lieferdatum......:", 
                             0);

        static ITEM istatval("auf_stat", 
                             statval, 
                             "Status...........:",
                             0);

        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);

        static field _qtxtform[] = {
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 7,15, 0, "", BUTTON, 0, 0,KEY12,
           &iCA,        15, 0, 7,32, 0, "", BUTTON, 0, 0,KEY5,
		};

        static form qtxtform = {7, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"auf", "kun_fil", "kun", 
                                 "lieferdat", "auf_stat",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (10, 62, 9, 10, hMainInst,
                               "Suchkriterien f�r Auftr�ge");
        enter_form (query, &qtxtform, 0, 0);

        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                  if (ls_class.PrepareAufQuery (&qtxtform, qnamen) == 0)
                  {
                             ls_class.ShowBuAufQuery (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}
