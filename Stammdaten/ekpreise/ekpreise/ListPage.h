#pragma once
#include "afxcmn.h"
#include "CtrlGrid.h"
#include "CmpPrListCtrl.h"
#include "FillList.h"
#include "AprUpdateEvent.h"
#include "a_bas.h"
#include "prdk_k.h"
#include "mdn.h"
#include "fil.h"
#include "gr_zuord.h"
#include "Adr.h"
#include "Price.h"
#include "EkPreise.h"
#include "PrProt.h"
#include "Vector.h"
#include "PrProt.h"
#include "mo_progcfg.h"
#include "Etikett.h"
#include "Sys_par.h"
#include "a_kalkpreis.h"
#include "a_kalk_mat.h"
#include "a_kalk_eig.h"
#include "a_pr.h"
#include "ipr.h"
#include "ipr2.h"
#include "Calculate.h"
#include "Ptabn.h"

#define ART_OFF_MUEFLI 10000

// CListPage-Dialogfeld

class CListPage : public CDialog, public CAprUpdateEvent,
				  CListChangeHandler	
{
	DECLARE_DYNAMIC(CListPage)

public:
	CListPage(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CListPage();

// Dialogfelddaten
	enum { IDD = IDD_LIST_PAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
	virtual void OnSize (UINT, int, int);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
	PROG_CFG Cfg;
	CEtikett Etikett;
	short EkFilGr;
	short cfg_sql_mode;
	double pr_ek_a_mo_we;
	double pr_ek_a_kalk;
	double pr_sk_a_kalk;

	BOOL Write160;
	static HANDLE Write160Lib;
    int (*dbild160)(LPSTR); 
    BOOL (*dOpenDbase)(LPSTR); 

    CImageList image; 
	int CellHeight;
	CString ArtSelect;

	CString PersName;
	CPrice Ek_pr;
	A_BAS_CLASS A_bas;
	PRDK_K_CLASS Prdk_k;
	MDN_CLASS Mdn;
	MDN_CLASS MdnUser;
	ADR_CLASS MdnAdr;
	A_KALKPREIS_CLASS A_Kalkpreis;
	A_KALK_EIG_CLASS A_Kalk_eig;
	A_KALK_MAT_CLASS A_Kalk_mat;
	A_KALKHNDW_CLASS A_Kalkhndw;
	APR_CLASS A_Pr;
//	IPR_CLASS IPrGh1;
//	IPR2_CLASS IPrGh2;
	CPriceIpr Pr_ipr;
	FIL_CLASS Fil;
	GR_ZUORD_CLASS Gr_zuord;
	ADR_CLASS FilAdr;
	SYS_PAR_CLASS Sys_par;
	PTABN_CLASS Ptabn;
	CCalculate Calculate;

	CVector DbRows;
	CVector ListRows;
	int AprCursor;
	int MdnGrCursor;
	int MdnCursor;
	int FilCursor;
	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CFillList FillList;
	CCmpPrListCtrl m_List;
	CPrProt PrProt;

	int Pos_PrEk;
	int Pos_BearbKost;
	int Pos_Kalk_Hk;
	int	Pos_Aufs_Sk;
	int	Pos_Kalk_Sk;
	int	Pos_Aufs_Fil_Ek;
	int	Pos_Kalk_Fil_Ek;
	int	Pos_Fil_Ek;
	int	Pos_Aufs_Fil_Vk;
	int	Pos_Kalk_Fil_Vk;
	int	Pos_Fil_Vk;

	int	Pos_Aufs_Gh1;
	int	Pos_KalkVk_Gh1;
	int	Pos_Gh1;
	int	Pos_Sp_Gh1;
	int	Pos_Aufs_Gh2;
	int	Pos_KalkVk_Gh2;
	int	Pos_Gh2;
	int	Pos_Sp_Gh2;
	int	PosA2;



    BOOL InList (EK_PR_CLASS& Ek_pr);
    void ReadCfg ();
	void StopEnter ();
	void DestroyRows(CVector &Rows);
	BOOL TestWrite ();
	virtual BOOL Write ();
	virtual BOOL DeleteAll ();
    BOOL TestDecValues (EK_PR *ek_pr);
	BOOL IsChanged (CEkPreise *pEk_pr, CEkPreise *old_ek_pr);
	void DeleteDbRows ();
    void FillMdnGrCombo ();
    void FillMdnCombo ();
    void FillFilCombo ();


// Methoden von CUpdateEvent
	virtual void Read (CString& Select, CString& CmpSelect );
//	virtual void Read (CString& Select, short mdn_gr, short mdn, short fil_gr, short fil);
	virtual void SetMdn (short mdn);
	virtual void SetPrVkVisible (BOOL visible);
	virtual void Schreiben (void);
	virtual void DruckSetzen (void);
	virtual void AktSetzen (void);

//	ListChangeHandler

	virtual void RowChanged (int NewRow);
	virtual void ColChanged (int Row, int Col);
	void HoleSpaltenwert (int Row, int Col,char* Modus);
	void HoleSpaltenwerte (int Row, char *Modus);
};
