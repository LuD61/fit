// AprManDoc.h : Schnittstelle der Klasse CAprManDoc
//


#pragma once

class CAprManDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CAprManDoc();
	DECLARE_DYNCREATE(CAprManDoc)

// Attribute
public:

// Operationen
public:

// Überschreibungen
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CAprManDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


