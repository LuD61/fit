#ifndef _LST_DEF
#define _LST_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LST {
	TCHAR AlleArtikel[2]; 
	TCHAR AEdit[2]; 
	TCHAR EkEdit[2];
	TCHAR HkEdit[2];
	TCHAR HkKostEdit[2];
	TCHAR SkAufsEdit[2];
	TCHAR SkEdit[2];
	TCHAR FilEkAufsEdit[2];
	TCHAR FilEkKalkEdit[2];	
	TCHAR FilEkEdit[2];
	TCHAR FilVkAufsEdit[2];
	TCHAR FilVkKalkEdit[2];
	TCHAR FilVkEdit[2];
	TCHAR Gh1AufsEdit[2];
	TCHAR Gh1KalkVkEdit[2];
	TCHAR Gh1Edit[2];
	TCHAR Gh1SpEdit[2];
	TCHAR Gh2AufsEdit[2];
	TCHAR Gh2KalkVkEdit[2];
	TCHAR Gh2Edit[2];		
	TCHAR Gh2SpEdit[2];
};
extern struct LST lst, lst_null;

class LST_CLASS : public DB_CLASS 
{
       private :
       public :
               LST lst;  
               LST_CLASS () : DB_CLASS ()
               {
               }
};
#endif
