#include "stdafx.h"
#include "ek_pr.h"

struct EK_PR ek_pr, ek_pr_null;

void EK_PR_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &ek_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ek_pr.a, SQLDOUBLE, 0);
    sqlout ((double *) &ek_pr.a,SQLDOUBLE,0);
    sqlout ((short *) &ek_pr.fil,SQLSHORT,0);
    sqlout ((short *) &ek_pr.mdn,SQLSHORT,0);
    sqlout ((short *) &ek_pr.akt,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &ek_pr.bearb,SQLDATE,0);
    sqlout ((double *) &ek_pr.pr_ek,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.hk_kost,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.hk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.sk_aufs,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.sk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_ek_aufs,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_ek_kalk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_ek,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_vk_aufs,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_vk_kalk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_vk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh1_aufs,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh1_kalk_vk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh1,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh1_sp,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh2_aufs,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh2_kalk_vk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh2,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh2_sp,SQLDOUBLE,0);
    sqlout ((TCHAR *) ek_pr.modif,SQLCHAR,2);
    sqlout ((short *) &ek_pr.druck,SQLSHORT,0);
            cursor = sqlcursor (_T("select ek_pr.a,  ek_pr.fil,  ")
_T("ek_pr.mdn,  ek_pr.akt,  ek_pr.bearb,  ek_pr.pr_ek,  ek_pr.hk_kost,  ")
_T("ek_pr.hk,  ek_pr.sk_aufs,  ek_pr.sk,  ek_pr.fil_ek_aufs,  ")
_T("ek_pr.fil_ek_kalk,  ek_pr.fil_ek,  ek_pr.fil_vk_aufs,  ")
_T("ek_pr.fil_vk_kalk,  ek_pr.fil_vk,  ek_pr.gh1_aufs,  ek_pr.gh1_kalk_vk,  ")
_T("ek_pr.gh1,  ek_pr.gh1_sp,  ek_pr.gh2_aufs,  ek_pr.gh2_kalk_vk,  ")
_T("ek_pr.gh2,  ek_pr.gh2_sp,  ek_pr.modif,  ek_pr.druck from ek_pr ")

#line 13 "ek_pr.rpp"
                                  _T("where mdn = ? ")
				  _T("and a = ?"));
    sqlin ((double *) &ek_pr.a,SQLDOUBLE,0);
    sqlin ((short *) &ek_pr.fil,SQLSHORT,0);
    sqlin ((short *) &ek_pr.mdn,SQLSHORT,0);
    sqlin ((short *) &ek_pr.akt,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &ek_pr.bearb,SQLDATE,0);
    sqlin ((double *) &ek_pr.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.hk_kost,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.hk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.sk_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.sk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_ek_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_ek_kalk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_ek,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_vk_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_vk_kalk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1_kalk_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1_sp,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2_kalk_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2_sp,SQLDOUBLE,0);
    sqlin ((TCHAR *) ek_pr.modif,SQLCHAR,2);
    sqlin ((short *) &ek_pr.druck,SQLSHORT,0);
            sqltext = _T("update ek_pr set ek_pr.a = ?,  ")
_T("ek_pr.fil = ?,  ek_pr.mdn = ?,  ek_pr.akt = ?,  ek_pr.bearb = ?,  ")
_T("ek_pr.pr_ek = ?,  ek_pr.hk_kost = ?,  ek_pr.hk = ?,  ")
_T("ek_pr.sk_aufs = ?,  ek_pr.sk = ?,  ek_pr.fil_ek_aufs = ?,  ")
_T("ek_pr.fil_ek_kalk = ?,  ek_pr.fil_ek = ?,  ek_pr.fil_vk_aufs = ?,  ")
_T("ek_pr.fil_vk_kalk = ?,  ek_pr.fil_vk = ?,  ek_pr.gh1_aufs = ?,  ")
_T("ek_pr.gh1_kalk_vk = ?,  ek_pr.gh1 = ?,  ek_pr.gh1_sp = ?,  ")
_T("ek_pr.gh2_aufs = ?,  ek_pr.gh2_kalk_vk = ?,  ek_pr.gh2 = ?,  ")
_T("ek_pr.gh2_sp = ?,  ek_pr.modif = ?,  ek_pr.druck = ? ")

#line 16 "ek_pr.rpp"
                                  _T("where mdn = ? ")
				  _T("and a = ?");
            sqlin ((short *)   &ek_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ek_pr.a, SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &ek_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ek_pr.a, SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from ek_pr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?"));
            sqlin ((short *)   &ek_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ek_pr.a, SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from ek_pr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?"));
    sqlin ((double *) &ek_pr.a,SQLDOUBLE,0);
    sqlin ((short *) &ek_pr.fil,SQLSHORT,0);
    sqlin ((short *) &ek_pr.mdn,SQLSHORT,0);
    sqlin ((short *) &ek_pr.akt,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &ek_pr.bearb,SQLDATE,0);
    sqlin ((double *) &ek_pr.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.hk_kost,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.hk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.sk_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.sk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_ek_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_ek_kalk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_ek,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_vk_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_vk_kalk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1_kalk_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1_sp,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2_kalk_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2_sp,SQLDOUBLE,0);
    sqlin ((TCHAR *) ek_pr.modif,SQLCHAR,2);
    sqlin ((short *) &ek_pr.druck,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into ek_pr (a,  ")
_T("fil,  mdn,  akt,  bearb,  pr_ek,  hk_kost,  hk,  sk_aufs,  sk,  fil_ek_aufs,  fil_ek_kalk,  ")
_T("fil_ek,  fil_vk_aufs,  fil_vk_kalk,  fil_vk,  gh1_aufs,  gh1_kalk_vk,  gh1,  ")
_T("gh1_sp,  gh2_aufs,  gh2_kalk_vk,  gh2,  gh2_sp,  modif,  druck) ")

#line 33 "ek_pr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 35 "ek_pr.rpp"
}

BOOL EK_PR_CLASS::operator== (EK_PR& ek_pr)
{
            if (this->ek_pr.mdn    != ek_pr.mdn) return FALSE;  
            if (this->ek_pr.a      != ek_pr.a) return FALSE;  
            return TRUE;
} 

int  EK_PR_CLASS::dbupdate_e (void)
{
	strcpy ( ek_pr.modif,  "");
	ek_pr.akt = 0;
	ek_pr.druck = 1;

	return 	dbupdate ();
}
