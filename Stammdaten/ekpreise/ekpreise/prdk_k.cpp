#include "stdafx.h"
#include "prdk_k.h"

struct PRDK_K prdk_k, prdk_k_null, prdk_k_def;

void PRDK_K_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)    &prdk_k.mdn, SQLSHORT, 0);
            sqlin ((double *)   &prdk_k.a,   SQLDOUBLE, 0);
    sqlout ((short *) &prdk_k.mdn,SQLSHORT,0);
    sqlout ((double *) &prdk_k.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) prdk_k.rez,SQLCHAR,9);
    sqlout ((TCHAR *) prdk_k.rez_bz,SQLCHAR,73);
    sqlout ((TCHAR *) prdk_k.kalk_stat,SQLCHAR,2);
    sqlout ((TCHAR *) prdk_k.grbrt,SQLCHAR,2);
    sqlout ((TCHAR *) prdk_k.leits,SQLCHAR,10);
    sqlout ((double *) &prdk_k.chg_gew,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.varb_gew,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.zut_gew,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.huel_gew,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.tr_sw,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.is_befe_abs,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.is_befe_rel,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.is_be_rel,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.is_fett,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.is_fett_tol,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.is_fett_fe,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.is_f_h2o,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.varb_mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.varb_hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.varb_hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.zut_mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.zut_hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.zut_hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.huel_mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.huel_hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.huel_hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.rez_hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.rez_hk_vollk,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &prdk_k.dat,SQLDATE,0);
    sqlout ((double *) &prdk_k.sw_kalk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.bto_gew,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.nto_gew,SQLDOUBLE,0);
    sqlout ((TCHAR *) prdk_k.bearb_weg_p,SQLCHAR,5);
    sqlout ((double *) &prdk_k.bto_mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.bto_hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.bto_hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.nto_mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.nto_hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.nto_hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.a_hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.a_hk_vollk,SQLDOUBLE,0);
    sqlout ((short *) &prdk_k.delstatus,SQLSHORT,0);
    sqlout ((double *) &prdk_k.rez_mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.nto_berech_gew,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.bearb_weg,SQLDOUBLE,0);
    sqlout ((TCHAR *) prdk_k.bru,SQLCHAR,2);
    sqlout ((double *) &prdk_k.sw,SQLDOUBLE,0);
    sqlout ((short *) &prdk_k.variante,SQLSHORT,0);
    sqlout ((TCHAR *) prdk_k.variante_bz,SQLCHAR,25);
    sqlout ((short *) &prdk_k.akv,SQLSHORT,0);
    sqlout ((TCHAR *) prdk_k.prodphase,SQLCHAR,21);
    sqlout ((long *) &prdk_k.masch_nr1,SQLLONG,0);
    sqlout ((long *) &prdk_k.masch_nr2,SQLLONG,0);
    sqlout ((long *) &prdk_k.masch_nr3,SQLLONG,0);
    sqlout ((long *) &prdk_k.masch_nr4,SQLLONG,0);
    sqlout ((long *) &prdk_k.masch_nr5,SQLLONG,0);
    sqlout ((long *) &prdk_k.masch_nr6,SQLLONG,0);
    sqlout ((long *) &prdk_k.masch_nr7,SQLLONG,0);
    sqlout ((long *) &prdk_k.masch_nr8,SQLLONG,0);
    sqlout ((long *) &prdk_k.masch_nr9,SQLLONG,0);
    sqlout ((long *) &prdk_k.masch_nr10,SQLLONG,0);
    sqlout ((double *) &prdk_k.huel_wrt,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.vpk_wrt,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.huel_hk_wrt,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.vpk_hk_wrt,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.kost_gew,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.kost_mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.kost_hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.kost_hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.vpk_gew,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.vpk_mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.vpk_hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.vpk_hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.nvpk_mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.nvpk_hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.nvpk_hk_vollk,SQLDOUBLE,0);
    sqlout ((short *) &prdk_k.chg_anz_bzg,SQLSHORT,0);
    sqlout ((double *) &prdk_k.kalk_me,SQLDOUBLE,0);
    sqlout ((short *) &prdk_k.chg_anz_kalk,SQLSHORT,0);
    sqlout ((double *) &prdk_k.pers_wrt_bzg,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.pers_wrt_kalk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.kutter_gew,SQLDOUBLE,0);
    sqlout ((short *) &prdk_k.chargierung,SQLSHORT,0);
    sqlout ((double *) &prdk_k.a_sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.a_sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.a_filek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.a_filek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.a_filvk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.a_filvk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.sk_wrt,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.filek_wrt,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.filvk_wrt,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.bearb_weg_sk,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.bearb_weg_fek,SQLDOUBLE,0);
    sqlout ((double *) &prdk_k.bearb_weg_fvk,SQLDOUBLE,0);
    sqlout ((long *) &prdk_k.manr,SQLLONG,0);
    sqlout ((short *) &prdk_k.masch1_zeit,SQLSHORT,0);
    sqlout ((short *) &prdk_k.masch2_zeit,SQLSHORT,0);
    sqlout ((short *) &prdk_k.masch3_zeit,SQLSHORT,0);
    sqlout ((short *) &prdk_k.masch4_zeit,SQLSHORT,0);
    sqlout ((short *) &prdk_k.masch5_zeit,SQLSHORT,0);
    sqlout ((short *) &prdk_k.masch6_zeit,SQLSHORT,0);
    sqlout ((short *) &prdk_k.masch7_zeit,SQLSHORT,0);
    sqlout ((short *) &prdk_k.masch8_zeit,SQLSHORT,0);
    sqlout ((short *) &prdk_k.masch9_zeit,SQLSHORT,0);
    sqlout ((short *) &prdk_k.masch10_zeit,SQLSHORT,0);
    sqlout ((short *) &prdk_k.vorlaufzeit,SQLSHORT,0);
    sqlout ((double *) &prdk_k.varb_gew_nto,SQLDOUBLE,0);
    sqlout ((short *) &prdk_k.rework,SQLSHORT,0);
    sqlout ((long *) &prdk_k.anz_pers,SQLLONG,0);
    sqlout ((short *) &prdk_k.prod_abt,SQLSHORT,0);
            cursor = sqlcursor (_T("select prdk_k.mdn,  ")
_T("prdk_k.a,  prdk_k.rez,  prdk_k.rez_bz,  prdk_k.kalk_stat,  prdk_k.grbrt,  ")
_T("prdk_k.leits,  prdk_k.chg_gew,  prdk_k.varb_gew,  prdk_k.zut_gew,  ")
_T("prdk_k.huel_gew,  prdk_k.tr_sw,  prdk_k.is_befe_abs,  ")
_T("prdk_k.is_befe_rel,  prdk_k.is_be_rel,  prdk_k.is_fett,  ")
_T("prdk_k.is_fett_tol,  prdk_k.is_fett_fe,  prdk_k.is_f_h2o,  ")
_T("prdk_k.varb_mat_o_b,  prdk_k.varb_hk_teilk,  prdk_k.varb_hk_vollk,  ")
_T("prdk_k.zut_mat_o_b,  prdk_k.zut_hk_teilk,  prdk_k.zut_hk_vollk,  ")
_T("prdk_k.huel_mat_o_b,  prdk_k.huel_hk_teilk,  prdk_k.huel_hk_vollk,  ")
_T("prdk_k.rez_hk_teilk,  prdk_k.rez_hk_vollk,  prdk_k.dat,  ")
_T("prdk_k.sw_kalk,  prdk_k.bto_gew,  prdk_k.nto_gew,  prdk_k.bearb_weg_p,  ")
_T("prdk_k.bto_mat_o_b,  prdk_k.bto_hk_teilk,  prdk_k.bto_hk_vollk,  ")
_T("prdk_k.nto_mat_o_b,  prdk_k.nto_hk_teilk,  prdk_k.nto_hk_vollk,  ")
_T("prdk_k.a_hk_teilk,  prdk_k.a_hk_vollk,  prdk_k.delstatus,  ")
_T("prdk_k.rez_mat_o_b,  prdk_k.nto_berech_gew,  prdk_k.bearb_weg,  ")
_T("prdk_k.bru,  prdk_k.sw,  prdk_k.variante,  prdk_k.variante_bz,  ")
_T("prdk_k.akv,  prdk_k.prodphase,  prdk_k.masch_nr1,  prdk_k.masch_nr2,  ")
_T("prdk_k.masch_nr3,  prdk_k.masch_nr4,  prdk_k.masch_nr5,  ")
_T("prdk_k.masch_nr6,  prdk_k.masch_nr7,  prdk_k.masch_nr8,  ")
_T("prdk_k.masch_nr9,  prdk_k.masch_nr10,  prdk_k.huel_wrt,  ")
_T("prdk_k.vpk_wrt,  prdk_k.huel_hk_wrt,  prdk_k.vpk_hk_wrt,  ")
_T("prdk_k.kost_gew,  prdk_k.kost_mat_o_b,  prdk_k.kost_hk_teilk,  ")
_T("prdk_k.kost_hk_vollk,  prdk_k.vpk_gew,  prdk_k.vpk_mat_o_b,  ")
_T("prdk_k.vpk_hk_teilk,  prdk_k.vpk_hk_vollk,  prdk_k.nvpk_mat_o_b,  ")
_T("prdk_k.nvpk_hk_teilk,  prdk_k.nvpk_hk_vollk,  prdk_k.chg_anz_bzg,  ")
_T("prdk_k.kalk_me,  prdk_k.chg_anz_kalk,  prdk_k.pers_wrt_bzg,  ")
_T("prdk_k.pers_wrt_kalk,  prdk_k.kutter_gew,  prdk_k.chargierung,  ")
_T("prdk_k.a_sk_teilk,  prdk_k.a_sk_vollk,  prdk_k.a_filek_teilk,  ")
_T("prdk_k.a_filek_vollk,  prdk_k.a_filvk_teilk,  prdk_k.a_filvk_vollk,  ")
_T("prdk_k.sk_wrt,  prdk_k.filek_wrt,  prdk_k.filvk_wrt,  ")
_T("prdk_k.bearb_weg_sk,  prdk_k.bearb_weg_fek,  prdk_k.bearb_weg_fvk,  ")
_T("prdk_k.manr,  prdk_k.masch1_zeit,  prdk_k.masch2_zeit,  ")
_T("prdk_k.masch3_zeit,  prdk_k.masch4_zeit,  prdk_k.masch5_zeit,  ")
_T("prdk_k.masch6_zeit,  prdk_k.masch7_zeit,  prdk_k.masch8_zeit,  ")
_T("prdk_k.masch9_zeit,  prdk_k.masch10_zeit,  prdk_k.vorlaufzeit,  ")
_T("prdk_k.varb_gew_nto,  prdk_k.rework,  prdk_k.anz_pers,  ")
_T("prdk_k.prod_abt from prdk_k ")

#line 13 "prdk_k.rpp"
                                  _T("where mdn = ? ")
                                  _T("and a = ? and akv = 1"));
    sqlin ((short *) &prdk_k.mdn,SQLSHORT,0);
    sqlin ((double *) &prdk_k.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) prdk_k.rez,SQLCHAR,9);
    sqlin ((TCHAR *) prdk_k.rez_bz,SQLCHAR,73);
    sqlin ((TCHAR *) prdk_k.kalk_stat,SQLCHAR,2);
    sqlin ((TCHAR *) prdk_k.grbrt,SQLCHAR,2);
    sqlin ((TCHAR *) prdk_k.leits,SQLCHAR,10);
    sqlin ((double *) &prdk_k.chg_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.varb_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.zut_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.huel_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.tr_sw,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_befe_abs,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_befe_rel,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_be_rel,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_fett,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_fett_tol,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_fett_fe,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_f_h2o,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.varb_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.varb_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.varb_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.zut_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.zut_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.zut_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.huel_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.huel_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.huel_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.rez_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.rez_hk_vollk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &prdk_k.dat,SQLDATE,0);
    sqlin ((double *) &prdk_k.sw_kalk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bto_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nto_gew,SQLDOUBLE,0);
    sqlin ((TCHAR *) prdk_k.bearb_weg_p,SQLCHAR,5);
    sqlin ((double *) &prdk_k.bto_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bto_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bto_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nto_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nto_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nto_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_hk_vollk,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.delstatus,SQLSHORT,0);
    sqlin ((double *) &prdk_k.rez_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nto_berech_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bearb_weg,SQLDOUBLE,0);
    sqlin ((TCHAR *) prdk_k.bru,SQLCHAR,2);
    sqlin ((double *) &prdk_k.sw,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.variante,SQLSHORT,0);
    sqlin ((TCHAR *) prdk_k.variante_bz,SQLCHAR,25);
    sqlin ((short *) &prdk_k.akv,SQLSHORT,0);
    sqlin ((TCHAR *) prdk_k.prodphase,SQLCHAR,21);
    sqlin ((long *) &prdk_k.masch_nr1,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr2,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr3,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr4,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr5,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr6,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr7,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr8,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr9,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr10,SQLLONG,0);
    sqlin ((double *) &prdk_k.huel_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.huel_hk_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_hk_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.kost_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.kost_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.kost_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.kost_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nvpk_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nvpk_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nvpk_hk_vollk,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.chg_anz_bzg,SQLSHORT,0);
    sqlin ((double *) &prdk_k.kalk_me,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.chg_anz_kalk,SQLSHORT,0);
    sqlin ((double *) &prdk_k.pers_wrt_bzg,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.pers_wrt_kalk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.kutter_gew,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.chargierung,SQLSHORT,0);
    sqlin ((double *) &prdk_k.a_sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_filek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_filek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_filvk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_filvk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.sk_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.filek_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.filvk_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bearb_weg_sk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bearb_weg_fek,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bearb_weg_fvk,SQLDOUBLE,0);
    sqlin ((long *) &prdk_k.manr,SQLLONG,0);
    sqlin ((short *) &prdk_k.masch1_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch2_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch3_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch4_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch5_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch6_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch7_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch8_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch9_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch10_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.vorlaufzeit,SQLSHORT,0);
    sqlin ((double *) &prdk_k.varb_gew_nto,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.rework,SQLSHORT,0);
    sqlin ((long *) &prdk_k.anz_pers,SQLLONG,0);
    sqlin ((short *) &prdk_k.prod_abt,SQLSHORT,0);
            sqltext = _T("update prdk_k set prdk_k.mdn = ?,  ")
_T("prdk_k.a = ?,  prdk_k.rez = ?,  prdk_k.rez_bz = ?,  ")
_T("prdk_k.kalk_stat = ?,  prdk_k.grbrt = ?,  prdk_k.leits = ?,  ")
_T("prdk_k.chg_gew = ?,  prdk_k.varb_gew = ?,  prdk_k.zut_gew = ?,  ")
_T("prdk_k.huel_gew = ?,  prdk_k.tr_sw = ?,  prdk_k.is_befe_abs = ?,  ")
_T("prdk_k.is_befe_rel = ?,  prdk_k.is_be_rel = ?,  prdk_k.is_fett = ?,  ")
_T("prdk_k.is_fett_tol = ?,  prdk_k.is_fett_fe = ?,  ")
_T("prdk_k.is_f_h2o = ?,  prdk_k.varb_mat_o_b = ?,  ")
_T("prdk_k.varb_hk_teilk = ?,  prdk_k.varb_hk_vollk = ?,  ")
_T("prdk_k.zut_mat_o_b = ?,  prdk_k.zut_hk_teilk = ?,  ")
_T("prdk_k.zut_hk_vollk = ?,  prdk_k.huel_mat_o_b = ?,  ")
_T("prdk_k.huel_hk_teilk = ?,  prdk_k.huel_hk_vollk = ?,  ")
_T("prdk_k.rez_hk_teilk = ?,  prdk_k.rez_hk_vollk = ?,  prdk_k.dat = ?,  ")
_T("prdk_k.sw_kalk = ?,  prdk_k.bto_gew = ?,  prdk_k.nto_gew = ?,  ")
_T("prdk_k.bearb_weg_p = ?,  prdk_k.bto_mat_o_b = ?,  ")
_T("prdk_k.bto_hk_teilk = ?,  prdk_k.bto_hk_vollk = ?,  ")
_T("prdk_k.nto_mat_o_b = ?,  prdk_k.nto_hk_teilk = ?,  ")
_T("prdk_k.nto_hk_vollk = ?,  prdk_k.a_hk_teilk = ?,  ")
_T("prdk_k.a_hk_vollk = ?,  prdk_k.delstatus = ?,  ")
_T("prdk_k.rez_mat_o_b = ?,  prdk_k.nto_berech_gew = ?,  ")
_T("prdk_k.bearb_weg = ?,  prdk_k.bru = ?,  prdk_k.sw = ?,  ")
_T("prdk_k.variante = ?,  prdk_k.variante_bz = ?,  prdk_k.akv = ?,  ")
_T("prdk_k.prodphase = ?,  prdk_k.masch_nr1 = ?,  prdk_k.masch_nr2 = ?,  ")
_T("prdk_k.masch_nr3 = ?,  prdk_k.masch_nr4 = ?,  prdk_k.masch_nr5 = ?,  ")
_T("prdk_k.masch_nr6 = ?,  prdk_k.masch_nr7 = ?,  prdk_k.masch_nr8 = ?,  ")
_T("prdk_k.masch_nr9 = ?,  prdk_k.masch_nr10 = ?,  prdk_k.huel_wrt = ?,  ")
_T("prdk_k.vpk_wrt = ?,  prdk_k.huel_hk_wrt = ?,  ")
_T("prdk_k.vpk_hk_wrt = ?,  prdk_k.kost_gew = ?,  ")
_T("prdk_k.kost_mat_o_b = ?,  prdk_k.kost_hk_teilk = ?,  ")
_T("prdk_k.kost_hk_vollk = ?,  prdk_k.vpk_gew = ?,  ")
_T("prdk_k.vpk_mat_o_b = ?,  prdk_k.vpk_hk_teilk = ?,  ")
_T("prdk_k.vpk_hk_vollk = ?,  prdk_k.nvpk_mat_o_b = ?,  ")
_T("prdk_k.nvpk_hk_teilk = ?,  prdk_k.nvpk_hk_vollk = ?,  ")
_T("prdk_k.chg_anz_bzg = ?,  prdk_k.kalk_me = ?,  ")
_T("prdk_k.chg_anz_kalk = ?,  prdk_k.pers_wrt_bzg = ?,  ")
_T("prdk_k.pers_wrt_kalk = ?,  prdk_k.kutter_gew = ?,  ")
_T("prdk_k.chargierung = ?,  prdk_k.a_sk_teilk = ?,  ")
_T("prdk_k.a_sk_vollk = ?,  prdk_k.a_filek_teilk = ?,  ")
_T("prdk_k.a_filek_vollk = ?,  prdk_k.a_filvk_teilk = ?,  ")
_T("prdk_k.a_filvk_vollk = ?,  prdk_k.sk_wrt = ?,  ")
_T("prdk_k.filek_wrt = ?,  prdk_k.filvk_wrt = ?,  ")
_T("prdk_k.bearb_weg_sk = ?,  prdk_k.bearb_weg_fek = ?,  ")
_T("prdk_k.bearb_weg_fvk = ?,  prdk_k.manr = ?,  ")
_T("prdk_k.masch1_zeit = ?,  prdk_k.masch2_zeit = ?,  ")
_T("prdk_k.masch3_zeit = ?,  prdk_k.masch4_zeit = ?,  ")
_T("prdk_k.masch5_zeit = ?,  prdk_k.masch6_zeit = ?,  ")
_T("prdk_k.masch7_zeit = ?,  prdk_k.masch8_zeit = ?,  ")
_T("prdk_k.masch9_zeit = ?,  prdk_k.masch10_zeit = ?,  ")
_T("prdk_k.vorlaufzeit = ?,  prdk_k.varb_gew_nto = ?,  ")
_T("prdk_k.rework = ?,  prdk_k.anz_pers = ?,  prdk_k.prod_abt = ? ")

#line 16 "prdk_k.rpp"
                                  _T("where mdn = ? ")
                                  _T("and a = ? and akv = 1");
            sqlin ((short *)    &prdk_k.mdn, SQLSHORT, 0);
            sqlin ((double *)   &prdk_k.a,   SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)    &prdk_k.mdn, SQLSHORT, 0);
            sqlin ((double *)   &prdk_k.a,   SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from prdk_k ")
                                  _T("where mdn = ? ")
                                  _T("and a = ? and akv = 1"));
            sqlin ((short *)    &prdk_k.mdn, SQLSHORT, 0);
            sqlin ((double *)   &prdk_k.a,   SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update prdk_k set delstatus = 0 ")
                                  _T("where mdn = ? ")
                                  _T("and a = ? and akv = 1"));
            sqlin ((short *)    &prdk_k.mdn, SQLSHORT, 0);
            sqlin ((double *)   &prdk_k.a,   SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from prdk_k ")
                                  _T("where mdn = ? ")
                                  _T("and a = ? and akv = 1"));
    sqlin ((short *) &prdk_k.mdn,SQLSHORT,0);
    sqlin ((double *) &prdk_k.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) prdk_k.rez,SQLCHAR,9);
    sqlin ((TCHAR *) prdk_k.rez_bz,SQLCHAR,73);
    sqlin ((TCHAR *) prdk_k.kalk_stat,SQLCHAR,2);
    sqlin ((TCHAR *) prdk_k.grbrt,SQLCHAR,2);
    sqlin ((TCHAR *) prdk_k.leits,SQLCHAR,10);
    sqlin ((double *) &prdk_k.chg_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.varb_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.zut_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.huel_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.tr_sw,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_befe_abs,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_befe_rel,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_be_rel,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_fett,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_fett_tol,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_fett_fe,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.is_f_h2o,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.varb_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.varb_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.varb_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.zut_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.zut_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.zut_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.huel_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.huel_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.huel_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.rez_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.rez_hk_vollk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &prdk_k.dat,SQLDATE,0);
    sqlin ((double *) &prdk_k.sw_kalk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bto_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nto_gew,SQLDOUBLE,0);
    sqlin ((TCHAR *) prdk_k.bearb_weg_p,SQLCHAR,5);
    sqlin ((double *) &prdk_k.bto_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bto_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bto_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nto_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nto_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nto_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_hk_vollk,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.delstatus,SQLSHORT,0);
    sqlin ((double *) &prdk_k.rez_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nto_berech_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bearb_weg,SQLDOUBLE,0);
    sqlin ((TCHAR *) prdk_k.bru,SQLCHAR,2);
    sqlin ((double *) &prdk_k.sw,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.variante,SQLSHORT,0);
    sqlin ((TCHAR *) prdk_k.variante_bz,SQLCHAR,25);
    sqlin ((short *) &prdk_k.akv,SQLSHORT,0);
    sqlin ((TCHAR *) prdk_k.prodphase,SQLCHAR,21);
    sqlin ((long *) &prdk_k.masch_nr1,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr2,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr3,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr4,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr5,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr6,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr7,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr8,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr9,SQLLONG,0);
    sqlin ((long *) &prdk_k.masch_nr10,SQLLONG,0);
    sqlin ((double *) &prdk_k.huel_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.huel_hk_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_hk_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.kost_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.kost_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.kost_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.kost_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_gew,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.vpk_hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nvpk_mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nvpk_hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.nvpk_hk_vollk,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.chg_anz_bzg,SQLSHORT,0);
    sqlin ((double *) &prdk_k.kalk_me,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.chg_anz_kalk,SQLSHORT,0);
    sqlin ((double *) &prdk_k.pers_wrt_bzg,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.pers_wrt_kalk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.kutter_gew,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.chargierung,SQLSHORT,0);
    sqlin ((double *) &prdk_k.a_sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_filek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_filek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_filvk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.a_filvk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.sk_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.filek_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.filvk_wrt,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bearb_weg_sk,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bearb_weg_fek,SQLDOUBLE,0);
    sqlin ((double *) &prdk_k.bearb_weg_fvk,SQLDOUBLE,0);
    sqlin ((long *) &prdk_k.manr,SQLLONG,0);
    sqlin ((short *) &prdk_k.masch1_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch2_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch3_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch4_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch5_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch6_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch7_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch8_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch9_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.masch10_zeit,SQLSHORT,0);
    sqlin ((short *) &prdk_k.vorlaufzeit,SQLSHORT,0);
    sqlin ((double *) &prdk_k.varb_gew_nto,SQLDOUBLE,0);
    sqlin ((short *) &prdk_k.rework,SQLSHORT,0);
    sqlin ((long *) &prdk_k.anz_pers,SQLLONG,0);
    sqlin ((short *) &prdk_k.prod_abt,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into prdk_k (")
_T("mdn,  a,  rez,  rez_bz,  kalk_stat,  grbrt,  leits,  chg_gew,  varb_gew,  zut_gew,  ")
_T("huel_gew,  tr_sw,  is_befe_abs,  is_befe_rel,  is_be_rel,  is_fett,  ")
_T("is_fett_tol,  is_fett_fe,  is_f_h2o,  varb_mat_o_b,  varb_hk_teilk,  ")
_T("varb_hk_vollk,  zut_mat_o_b,  zut_hk_teilk,  zut_hk_vollk,  huel_mat_o_b,  ")
_T("huel_hk_teilk,  huel_hk_vollk,  rez_hk_teilk,  rez_hk_vollk,  dat,  sw_kalk,  ")
_T("bto_gew,  nto_gew,  bearb_weg_p,  bto_mat_o_b,  bto_hk_teilk,  bto_hk_vollk,  ")
_T("nto_mat_o_b,  nto_hk_teilk,  nto_hk_vollk,  a_hk_teilk,  a_hk_vollk,  ")
_T("delstatus,  rez_mat_o_b,  nto_berech_gew,  bearb_weg,  bru,  sw,  variante,  ")
_T("variante_bz,  akv,  prodphase,  masch_nr1,  masch_nr2,  masch_nr3,  masch_nr4,  ")
_T("masch_nr5,  masch_nr6,  masch_nr7,  masch_nr8,  masch_nr9,  masch_nr10,  ")
_T("huel_wrt,  vpk_wrt,  huel_hk_wrt,  vpk_hk_wrt,  kost_gew,  kost_mat_o_b,  ")
_T("kost_hk_teilk,  kost_hk_vollk,  vpk_gew,  vpk_mat_o_b,  vpk_hk_teilk,  ")
_T("vpk_hk_vollk,  nvpk_mat_o_b,  nvpk_hk_teilk,  nvpk_hk_vollk,  ")
_T("chg_anz_bzg,  kalk_me,  chg_anz_kalk,  pers_wrt_bzg,  pers_wrt_kalk,  ")
_T("kutter_gew,  chargierung,  a_sk_teilk,  a_sk_vollk,  a_filek_teilk,  ")
_T("a_filek_vollk,  a_filvk_teilk,  a_filvk_vollk,  sk_wrt,  filek_wrt,  ")
_T("filvk_wrt,  bearb_weg_sk,  bearb_weg_fek,  bearb_weg_fvk,  manr,  ")
_T("masch1_zeit,  masch2_zeit,  masch3_zeit,  masch4_zeit,  masch5_zeit,  ")
_T("masch6_zeit,  masch7_zeit,  masch8_zeit,  masch9_zeit,  masch10_zeit,  ")
_T("vorlaufzeit,  varb_gew_nto,  rework,  anz_pers,  prod_abt) ")

#line 38 "prdk_k.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 40 "prdk_k.rpp"
}
