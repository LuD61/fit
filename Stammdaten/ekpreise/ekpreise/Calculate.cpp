#include "StdAfx.h"
#include "Calculate.h"
#include "StrFuncs.h"


CCalculate::CCalculate(void)
{
	value1 = 0.0;
	value2 = 0.0;
	percent = 0.0;
	MarktSpPar = ReadSysPar;
	mwst = 19.0;
	a_a_kalkpreis = 0.0;
	fil_a_kalkpreis = 0;
	mdn_a_kalkpreis = 0;
	a_a_kalk = 0.0;
	a_a_bas = 0.0;
	ag = 0;
	fil_a_kalk = 0;
	mdn_a_kalk = 0;
	SkText = "";
	FilEkText = "";
	FilVkText = "";
}

CCalculate::~CCalculate(void)
{
}
void CCalculate::Init (void)
{
	value1 = 0.0;
	value2 = 0.0;
	percent = 0.0;
	MarktSpPar = ReadSysPar;
	mwst = 19.0;
	a_a_kalkpreis = 0.0;
	fil_a_kalkpreis = 0;
	mdn_a_kalkpreis = 0;
	a_a_kalk = 0.0;
	a_a_bas = 0.0;
	ag = 0;
	fil_a_kalk = 0;
	mdn_a_kalk = 0;
	SkText = "";
	FilEkText = "";
	FilVkText = "";
}


void CCalculate::SetValues (double value1, double value2)
{
	this->value1 = value1;
	this->value2 = value2;
}

void CCalculate::SetMwst (double mwst)
{
	this->mwst = mwst;
}

double CCalculate::GetPercent ()
{
	return percent;
}

double CCalculate::execute (double value1, double value2,double a)
{
	this->value1 = value1;
	this->value2 = value2;
	if (a != this->a_a_bas)
	{
		this->a_a_bas = a;
		memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas->a_bas.a = a;
		A_bas->dbreadfirst ();
		 _tcscpy (Ptabn.ptabn.ptitem, _T("mwst"));
		 _stprintf (Ptabn.ptabn.ptwert, _T("%d"), A_bas->a_bas.mwst);
		 if (Ptabn.dbreadfirst () == 0)
		 {
			 double mwst = CStrFuncs::StrToDouble (Ptabn.ptabn.ptwer1) * 100;
			 SetMwst (mwst);
		 }
	}

	return execute ();
}

double CCalculate::execute ()
{
	if (MarktSpPar == ReadSysPar)
	{
		_tcscpy (sys_par.sys_par_nam, "markt_sp_par");
		int dsqlstatus = Sys_par.dbreadfirst ();
		if (dsqlstatus == 0)
		{
			MarktSpPar = _tstoi (sys_par.sys_par_wrt);
		}
		else
		{
			MarktSpPar = AufschlagBrutto;
		}
	}

	if (value1 == 0.0 || value2 == 0.0)
	{
		return 0.0;
	}
	if (MarktSpPar == AufschlagBrutto)
	{
		double v1 = value1;
		double v2 = value2;
		double mwstp = (100 - mwst) / 100;
		v2 *= mwstp;
		percent = v2/v1 * 100 - 100;
	}

	else if (MarktSpPar == AufschlagNetto)
	{
		double v1 = value1;
		double v2 = value2;
		percent = v2/v1 * 100 - 100;
	}

	else if (MarktSpPar == AbschlagBrutto)
	{
		double v1 = value1;
		double v2 = value2;
		double mwstp = (100 - mwst) / 100;
		v2 *= mwstp;
		percent = 100 - v1/v2*100;
	}

	else if (MarktSpPar == AbschlagNetto)
	{
		double v1 = value1;
		double v2 = value2;
		percent = 100 - v1/v2*100;
	}

	return percent;
}

BOOL  CCalculate::isaktiveRezeptur (void)
{
	if (Prdk_k->prdk_k.akv == 1) return TRUE;
	return FALSE;
}

BOOL  CCalculate::isEigenprodukt (double a)
{
	if (a != A_bas->a_bas.a)
	{
		A_bas->a_bas.a = a;
		A_bas->dbreadfirst ();
	}
	if (A_bas->a_bas.a_typ == 2) return TRUE;
//	if (A_bas.a_bas.a_typ == 5) return FALSE;
//	if (A_bas.a_bas.a_typ == 6) return FALSE;
//	if (A_bas.a_bas.a_typ == 9) return FALSE;
//	if (A_bas.a_bas.a_typ == 14) return FALSE;

	if (A_bas->a_bas.a_typ2 == 2) return TRUE;
	return FALSE;
}
BOOL  CCalculate::isHandelsware (double a)
{
	if (a != A_bas->a_bas.a)
	{
		A_bas->a_bas.a = a;
		A_bas->dbreadfirst ();
	}
	if (A_bas->a_bas.a_typ == 1) return TRUE;
	if (A_bas->a_bas.a_typ == 8) return TRUE;
//	if (A_bas.a_bas.a_typ == 2) return FALSE;
	if (A_bas->a_bas.a_typ2 == 1) return TRUE;
	if (A_bas->a_bas.a_typ2 == 8) return TRUE;
	return FALSE;
}
BOOL  CCalculate::isMaterial (double a)
{
	if (a != A_bas->a_bas.a)
	{
		A_bas->a_bas.a = a;
		A_bas->dbreadfirst ();
	}
	if (A_bas->a_bas.a_typ == 5) return TRUE;
	if (A_bas->a_bas.a_typ == 6) return TRUE;
	if (A_bas->a_bas.a_typ == 9) return TRUE;
	if (A_bas->a_bas.a_typ == 8) return TRUE;
//	if (A_bas.a_bas.a_typ == 2) return FALSE;

	if (A_bas->a_bas.a_typ2 == 5) return TRUE;
	if (A_bas->a_bas.a_typ2 == 6) return TRUE;
	if (A_bas->a_bas.a_typ2 == 8) return TRUE;
	if (A_bas->a_bas.a_typ2 == 9) return TRUE;
	return FALSE;
}

void  CCalculate::LeseA_bas (void)
{
	A_bas->a_bas.a = this->a_a_bas;
	A_bas->dbreadfirst ();
}

void CCalculate::LeseA_kalkpreis (void)
{
	memcpy (&A_kalkpreis.a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
	A_kalkpreis.a_kalkpreis.mdn = this->mdn_a_kalkpreis;
	A_kalkpreis.a_kalkpreis.fil = this->fil_a_kalkpreis;
	A_kalkpreis.a_kalkpreis.a   = a_a_kalkpreis;
	while (A_kalkpreis.dbreadfirst () != 0)
	{
		if (A_kalkpreis.a_kalkpreis.fil > 0)
		{
			A_kalkpreis.a_kalkpreis.fil = 0;
		}
		else if (A_kalkpreis.a_kalkpreis.mdn > 0)
		{
			A_kalkpreis.a_kalkpreis.mdn = 0;
		}
		else
		{
			break;
		}
	}
}
void CCalculate::LeseA_kalk_eig (void)
{
	memcpy (&A_kalk_eig.a_kalk_eig, &a_kalk_eig_null, sizeof (A_KALK_EIG));
	A_kalk_eig.a_kalk_eig.mdn = this->mdn_a_kalk;
	A_kalk_eig.a_kalk_eig.fil = this->fil_a_kalk;
	A_kalk_eig.a_kalk_eig.a   = this->a_a_kalk;
	while (A_kalk_eig.dbreadfirst () != 0)
	{
		if (A_kalk_eig.a_kalk_eig.fil > 0)
		{
			A_kalk_eig.a_kalk_eig.fil = 0;
		}
		else if (A_kalk_eig.a_kalk_eig.mdn > 0)
		{
			A_kalk_eig.a_kalk_eig.mdn = 0;
		}
		else
		{
			break;
		}
	}
}
void CCalculate::LeseA_kalk_mat (void)
{
	memcpy (&A_kalk_mat.a_kalk_mat, &a_kalk_mat_null, sizeof (A_KALK_MAT));
	A_kalk_mat.a_kalk_mat.mdn = this->mdn_a_kalk;
	A_kalk_mat.a_kalk_mat.fil = this->fil_a_kalk;
	A_kalk_mat.a_kalk_mat.a   = this->a_a_kalk;
	while (A_kalk_mat.dbreadfirst () != 0)
	{
		if (A_kalk_mat.a_kalk_mat.fil > 0)
		{
			A_kalk_mat.a_kalk_mat.fil = 0;
		}
		else if (A_kalk_mat.a_kalk_mat.mdn > 0)
		{
			A_kalk_mat.a_kalk_mat.mdn = 0;
		}
		else
		{
			break;
		}
	}
}

void CCalculate::LeseA_kalkhndw (void)
{
	memcpy (&A_kalkhndw.a_kalkhndw, &a_kalkhndw_null, sizeof (A_KALKHNDW));
	A_kalkhndw.a_kalkhndw.mdn = this->mdn_a_kalk;
	A_kalkhndw.a_kalkhndw.fil = this->fil_a_kalk;
	A_kalkhndw.a_kalkhndw.a   = this->a_a_kalk;
	while (A_kalkhndw.dbreadfirst () != 0)
	{
		if (A_kalkhndw.a_kalkhndw.fil > 0)
		{
			A_kalkhndw.a_kalkhndw.fil = 0;
		}
		else if (A_kalkhndw.a_kalkhndw.mdn > 0)
		{
			A_kalkhndw.a_kalkhndw.mdn = 0;
		}
		else
		{
			break;
		}
	}
}


void  CCalculate::WriteBearbKost (short mdn, short fil, double a, double kost)
{
	int dsqlstatus;
		CString Date;
		CStrFuncs::SysDate (Date);

	if (a != this->a_a_bas)
	{
		this->a_a_bas = a;
		memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas->a_bas.a = a;
		A_bas->dbreadfirst ();
	}
	this->mdn_a_kalk = mdn;
	this->fil_a_kalk = fil;
	this->a_a_kalk = a;
	if (isEigenprodukt(a))
	{
		LeseA_kalk_eig() ;
		A_kalk_eig.a_kalk_eig.mdn = this->mdn_a_kalk;
		A_kalk_eig.a_kalk_eig.fil = this->fil_a_kalk;
		A_kalk_eig.a_kalk_eig.kost = kost;
		A_kalk_eig.ToDbDate (Date, &A_kalk_eig.a_kalk_eig.dat);
		A_kalk_eig.ToDbDate (Date, &A_kalk_eig.a_kalk_eig.aend_dat);
		dsqlstatus = A_kalk_eig.dbupdate ();
	}
	if (isMaterial(a)) 
	{
		LeseA_kalk_mat() ;
		A_kalk_mat.a_kalk_mat.mdn = this->mdn_a_kalk;
		A_kalk_mat.a_kalk_mat.fil = this->fil_a_kalk;
		A_kalk_mat.a_kalk_mat.kost = kost;
		A_kalk_mat.ToDbDate (Date, &A_kalk_mat.a_kalk_mat.dat);
		dsqlstatus = A_kalk_mat.dbupdate ();
	}


		this->mdn_a_kalkpreis = mdn;
		this->fil_a_kalkpreis = fil;
		this->a_a_kalkpreis = a;
	LeseA_kalkpreis() ;
	A_kalkpreis.a_kalkpreis.mdn = this->mdn_a_kalk;
	A_kalkpreis.a_kalkpreis.fil = this->fil_a_kalk;
	A_kalkpreis.a_kalkpreis.kost = kost;
	dsqlstatus = A_kalkpreis.dbupdate ();

	return ;
}

double CCalculate::GetMatoB (short mdn, short fil, double a)
{
	//Hier: holen _Ek 
	if (mdn != this->mdn_a_kalkpreis || fil != this->fil_a_kalkpreis || a != this->a_a_kalkpreis) 
	{
		this->mdn_a_kalkpreis = mdn;
		this->fil_a_kalkpreis = fil;
		this->a_a_kalkpreis = a;
		LeseA_kalkpreis();
	}
	return A_kalkpreis.a_kalkpreis.mat_o_b;
}

double  CCalculate::GetBearbKost (short mdn, short fil, double a)
{
	//Hier: holen _HkKost 
	if (a != this->a_a_bas)
	{
		this->a_a_bas = a;
		memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas->a_bas.a = a;
		A_bas->dbreadfirst ();
	}
	if (mdn != this->mdn_a_kalk || fil != this->fil_a_kalk || a != this->a_a_kalk) 
	{
		this->mdn_a_kalk = mdn;
		this->fil_a_kalk = fil;
		this->a_a_kalk = a;
		if (isEigenprodukt(a)) LeseA_kalk_eig() ;
		if (isMaterial(a)) LeseA_kalk_mat() ;
		if (isHandelsware(a)) LeseA_kalkhndw() ;
	}
	if (isEigenprodukt(a)) return A_kalk_eig.a_kalk_eig.kost;
	if (isMaterial(a)) return A_kalk_mat.a_kalk_mat.kost;
	return 0.0;
}

double CCalculate::GetHk (short mdn, short fil, double a)
{
	//Hier: holen _Hk 
	if (mdn != this->mdn_a_kalkpreis || fil != this->fil_a_kalkpreis || a != this->a_a_kalkpreis) 
	{
		this->mdn_a_kalkpreis = mdn;
		this->fil_a_kalkpreis = fil;
		this->a_a_kalkpreis = a;
		LeseA_kalkpreis();
	}
	return A_kalkpreis.a_kalkpreis.hk_vollk;
}

double CCalculate::GetSk (short mdn, short fil, double a)
{
	//Hier: holen _Sk 
	if (mdn != this->mdn_a_kalkpreis || fil != this->fil_a_kalkpreis || a != this->a_a_kalkpreis) 
	{
		this->mdn_a_kalkpreis = mdn;
		this->fil_a_kalkpreis = fil;
		this->a_a_kalkpreis = a;
		LeseA_kalkpreis();
	}
	return A_kalkpreis.a_kalkpreis.sk_vollk;
}
double CCalculate::GetSkAufs (short mdn, short fil, double a)
{
	//Hier: holen _SkAufs 
	SkText = "";
	if (a != this->a_a_bas)
	{
		this->a_a_bas = a;
		memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas->a_bas.a = a;
		A_bas->dbreadfirst ();
	}
	if (mdn != this->mdn_a_kalk || fil != this->fil_a_kalk || a != this->a_a_kalk) 
	{
		this->mdn_a_kalk = mdn;
		this->fil_a_kalk = fil;
		this->a_a_kalk = a;
		if (isEigenprodukt(a)) LeseA_kalk_eig() ;
		if (isMaterial(a)) LeseA_kalk_mat() ;
		if (isHandelsware(a)) LeseA_kalkhndw() ;
	}
	if (isEigenprodukt(a) && A_kalk_eig.a_kalk_eig.sp_vk > 0.0001) 
	{
		SkText = " %  (a)";
		return A_kalk_eig.a_kalk_eig.sp_vk;
	}
//	if (isMaterial() && A_kalk_mat.a_kalk_mat.sp) return A_kalk_mat.a_kalk_mat.kost;

	if (A_bas->a_bas.ag != this->ag)
	{
		this->ag = A_bas->a_bas.ag;
		memcpy (&Ag.ag, &ag_null, sizeof (AG));
		Ag.ag.ag = A_bas->a_bas.ag;
		Ag.dbreadfirst ();

		memcpy (&Wg.wg, &wg_null, sizeof (WG));
		Wg.wg.wg = A_bas->a_bas.wg;
		Wg.dbreadfirst ();

		memcpy (&Hwg.hwg, &hwg_null, sizeof (HWG));
		Hwg.hwg.hwg = A_bas->a_bas.hwg;
		Hwg.dbreadfirst ();
	}
	if (Ag.ag.sp_sk > 0.0001)
	{
		SkText = " % (ag)";
		return Ag.ag.sp_sk;
	}
	if (Wg.wg.sp_sk > 0.0001)
	{
		SkText = " % (wg)";
		return Wg.wg.sp_sk;
	}
	if (Hwg.hwg.sp_sk > 0.0001)
	{
		SkText = " %(hwg)";
		return Hwg.hwg.sp_sk;
	}
	return 0.0;
}
CString CCalculate::GetSkText (void)
{
	return SkText;
}
double CCalculate::GetFilEkAufs (short mdn, short fil, double a)
{
	//Hier: holen _FilEkAufs 
	FilEkText = "";
	if (a != this->a_a_bas)
	{
		this->a_a_bas = a;
		memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas->a_bas.a = a;
		A_bas->dbreadfirst ();
	}
	if (mdn != this->mdn_a_kalk || fil != this->fil_a_kalk || a != this->a_a_kalk) 
	{
		this->mdn_a_kalk = mdn;
		this->fil_a_kalk = fil;
		this->a_a_kalk = a;
		if (isEigenprodukt(a)) LeseA_kalk_eig() ;
		if (isMaterial(a)) LeseA_kalk_mat() ;
		if (isHandelsware(a)) LeseA_kalkhndw() ;
	}
	if (isEigenprodukt(a) && A_kalk_eig.a_kalk_eig.sp_fil > 0.0001) 
	{
		FilEkText = " %  (a)";
		return A_kalk_eig.a_kalk_eig.sp_fil;
	}

	if (A_bas->a_bas.ag != this->ag)
	{
		this->ag = A_bas->a_bas.ag;
		memcpy (&Ag.ag, &ag_null, sizeof (AG));
		Ag.ag.ag = A_bas->a_bas.ag;
		Ag.dbreadfirst ();

		memcpy (&Wg.wg, &wg_null, sizeof (WG));
		Wg.wg.wg = A_bas->a_bas.wg;
		Wg.dbreadfirst ();

		memcpy (&Hwg.hwg, &hwg_null, sizeof (HWG));
		Hwg.hwg.hwg = A_bas->a_bas.hwg;
		Hwg.dbreadfirst ();
	}
	if (Ag.ag.sp_fil > 0.0001)
	{
		FilEkText = " % (ag)";
		return Ag.ag.sp_fil;
	}
	if (Wg.wg.sp_fil > 0.0001)
	{
		FilEkText = " % (wg)";
		return Wg.wg.sp_fil;
	}
	if (Hwg.hwg.sp_fil > 0.0001)
	{
		FilEkText = " %(hwg)";
		return Hwg.hwg.sp_fil;
	}
	return 0.0;
}
CString CCalculate::GetFilEkText (void)
{
	return FilEkText;
}

double CCalculate::GetFilVkAufs (short mdn, short fil, double a)
{
	//Hier: holen _FilVkAufs 
	FilVkText = "";
	if (a != this->a_a_bas)
	{
		this->a_a_bas = a;
		memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas->a_bas.a = a;
		A_bas->dbreadfirst ();
	}
	if (mdn != this->mdn_a_kalk || fil != this->fil_a_kalk || a != this->a_a_kalk) 
	{
		this->mdn_a_kalk = mdn;
		this->fil_a_kalk = fil;
		this->a_a_kalk = a;
		if (isEigenprodukt(a)) LeseA_kalk_eig() ;
		if (isMaterial(a)) LeseA_kalk_mat() ;
		if (isHandelsware(a)) LeseA_kalkhndw() ;
	}
	if (isEigenprodukt(a) && A_kalk_eig.a_kalk_eig.sp_vk > 0.0001) 
	{
		FilVkText = " %  (a)";
		return A_kalk_eig.a_kalk_eig.sp_vk;
	}

	if (A_bas->a_bas.ag != this->ag)
	{
		this->ag = A_bas->a_bas.ag;
		memcpy (&Ag.ag, &ag_null, sizeof (AG));
		Ag.ag.ag = A_bas->a_bas.ag;
		Ag.dbreadfirst ();

		memcpy (&Wg.wg, &wg_null, sizeof (WG));
		Wg.wg.wg = A_bas->a_bas.wg;
		Wg.dbreadfirst ();

		memcpy (&Hwg.hwg, &hwg_null, sizeof (HWG));
		Hwg.hwg.hwg = A_bas->a_bas.hwg;
		Hwg.dbreadfirst ();
	}
	if (Ag.ag.sp_vk > 0.0001)
	{
		FilVkText = " % (ag)";
		return Ag.ag.sp_vk;
	}
	if (Wg.wg.sp_vk > 0.0001)
	{
		FilVkText = " % (wg)";
		return Wg.wg.sp_vk;
	}
	if (Hwg.hwg.sp_vk > 0.0001)
	{
		FilVkText = " %(hwg)";
		return Hwg.hwg.sp_vk;
	}
	return 0.0;
}
CString CCalculate::GetFilVkText (void)
{
	return FilVkText;
}

double CCalculate::GetFilEkKalk (short mdn, short fil, double a)
{
	//Hier: holen _FilEkKalk 
	if (mdn != this->mdn_a_kalkpreis || fil != this->fil_a_kalkpreis || a != this->a_a_kalkpreis) 
	{
		this->mdn_a_kalkpreis = mdn;
		this->fil_a_kalkpreis = fil;
		this->a_a_kalkpreis = a;
		LeseA_kalkpreis();
	}
	return A_kalkpreis.a_kalkpreis.fil_ek_vollk;
}
double CCalculate::GetFilVkKalk (short mdn, short fil, double a)
{
	//Hier: holen _FilVkAufs 
	if (mdn != this->mdn_a_kalkpreis || fil != this->fil_a_kalkpreis || a != this->a_a_kalkpreis) 
	{
		this->mdn_a_kalkpreis = mdn;
		this->fil_a_kalkpreis = fil;
		this->a_a_kalkpreis = a;
		LeseA_kalkpreis();
	}
	return A_kalkpreis.a_kalkpreis.fil_vk_vollk;
}

double CCalculate::GetHkVollk (short mdn, short fil, double a)
{
	//Hier: holen _Hk 
	if (mdn != this->mdn_a_kalkpreis || fil != this->fil_a_kalkpreis || a != this->a_a_kalkpreis) 
	{
		this->mdn_a_kalkpreis = mdn;
		this->fil_a_kalkpreis = fil;
		this->a_a_kalkpreis = a;
		LeseA_kalkpreis();
	}
	return A_kalkpreis.a_kalkpreis.hk_vollk;
}

double CCalculate::GetFilEk (short mdn, short fil, double a)
{
	//Hier: holen _FilEk 
	if (mdn != Pr_a_pr.a_pr.mdn || fil != Pr_a_pr.a_pr.fil || a != Pr_a_pr.a_pr.a) 
	{
		Pr_a_pr.a_pr.mdn = mdn;
		Pr_a_pr.a_pr.fil = fil;
		Pr_a_pr.a_pr.mdn_gr = 0;
		Pr_a_pr.a_pr.fil_gr = 0;
		Pr_a_pr.a_pr.a = a;
		if (Pr_a_pr.dbfetchpreisA_pr() != 0)
		{
			Pr_a_pr.a_pr.pr_ek_euro = 0.0;
			Pr_a_pr.a_pr.pr_vk_euro = 0.0;
		}
	}
	return Pr_a_pr.a_pr.pr_ek_euro;
}
double CCalculate::GetFilVk (short mdn, short fil, double a)
{
	//Hier: holen _FilEk 
	if (mdn != Pr_a_pr.a_pr.mdn || fil != Pr_a_pr.a_pr.fil || a != Pr_a_pr.a_pr.a) 
	{
		Pr_a_pr.a_pr.mdn = mdn;
		Pr_a_pr.a_pr.fil = fil;
		Pr_a_pr.a_pr.mdn_gr = 0;
		Pr_a_pr.a_pr.fil_gr = 0;
		Pr_a_pr.a_pr.a = a;
		if (Pr_a_pr.dbfetchpreisA_pr() != 0)
		{
			Pr_a_pr.a_pr.pr_ek_euro = 0.0;
			Pr_a_pr.a_pr.pr_vk_euro = 0.0;
		}
	}
	return Pr_a_pr.a_pr.pr_vk_euro; 
}
double CCalculate::GetVk (short mdn, double a, int pr_gr_stuf)
{
	//Hier: holen _Gh1 und _Gh2 je nach pr_gr_stuf 
	if (mdn != Pr_ipr.ipr.mdn || a != Pr_ipr.ipr.a || pr_gr_stuf != Pr_ipr.ipr.pr_gr_stuf ) 
	{
		Pr_ipr.ipr.mdn = mdn;
		Pr_ipr.ipr.kun_pr = 0;
		Pr_ipr.ipr.kun = 0;
		Pr_ipr.ipr.pr_gr_stuf = pr_gr_stuf;
		Pr_ipr.ipr.a = a;
		if (Pr_ipr.dbfetchpreisIpr() != 0)
		{
			Pr_ipr.ipr.vk_pr_eu = 0.0;
			Pr_ipr.ipr.add_ek_proz = 0.0;
		}
	}
	return Pr_ipr.ipr.vk_pr_eu;
}
double CCalculate::GetAddEkProz (short mdn, double a, int pr_gr_stuf)
{
	if (mdn != Pr_ipr.ipr.mdn || a != Pr_ipr.ipr.a || pr_gr_stuf != Pr_ipr.ipr.pr_gr_stuf ) 
	{
		Pr_ipr.ipr.mdn = mdn;
		Pr_ipr.ipr.kun_pr = 0;
		Pr_ipr.ipr.kun = 0;
		Pr_ipr.ipr.pr_gr_stuf = pr_gr_stuf;
		Pr_ipr.ipr.a = a;
		if (Pr_ipr.dbfetchpreisIpr() != 0)
		{
			Pr_ipr.ipr.vk_pr_eu = 0.0;
			Pr_ipr.ipr.add_ek_proz = 0.0;
		}
	}
	return Pr_ipr.ipr.add_ek_proz;
}
double CCalculate::GetVkSpanne (short mdn, double a, int pr_gr_stuf, double sk)
{
	if (mdn != Pr_ipr.ipr.mdn || a != Pr_ipr.ipr.a || pr_gr_stuf != Pr_ipr.ipr.pr_gr_stuf ) 
	{
		Pr_ipr.ipr.mdn = mdn;
		Pr_ipr.ipr.kun_pr = 0;
		Pr_ipr.ipr.kun = 0;
		Pr_ipr.ipr.pr_gr_stuf = pr_gr_stuf;
		Pr_ipr.ipr.a = a;
		if (Pr_ipr.dbfetchpreisIpr() != 0)
		{
			Pr_ipr.ipr.vk_pr_eu = 0.0;
			Pr_ipr.ipr.add_ek_proz = 0.0;
			return 0.0;
		}
	}
	return execute(sk,Pr_ipr.ipr.vk_pr_eu,a);
}
double CCalculate::GetKalkVk (short mdn, double a, int pr_gr_stuf, double sk)
{
	if (mdn != Pr_ipr.ipr.mdn || a != Pr_ipr.ipr.a || pr_gr_stuf != Pr_ipr.ipr.pr_gr_stuf ) 
	{
		Pr_ipr.ipr.mdn = mdn;
		Pr_ipr.ipr.kun_pr = 0;
		Pr_ipr.ipr.kun = 0;
		Pr_ipr.ipr.pr_gr_stuf = pr_gr_stuf;
		Pr_ipr.ipr.a = a;
		if (Pr_ipr.dbfetchpreisIpr() != 0)
		{
			Pr_ipr.ipr.vk_pr_eu = 0.0;
			Pr_ipr.ipr.add_ek_proz = 0.0;
			return 0.0;
		}
	}
	return (sk + (sk* Pr_ipr.ipr.add_ek_proz / 100) + Pr_ipr.ipr.add_ek_abs);
}

int CCalculate::UpdateIpr(short mdn, long PrGrStuf,double a, double pr)
{
	char mess [80];
	short dmdn;
	int dsqlstatus = 99;
	sprintf (mess, "Mandant %hd PrGrStuf %ld a= %.0lf, pr= %.0lf", mdn,PrGrStuf,a,pr);
//	MessageBox (NULL,_T(mess), NULL, MB_OK | MB_ICONERROR); 
	DbClass->sqlout  ((short *) &dmdn, SQLSHORT, 0);
	DbClass->sqlin  ((short *) &mdn, SQLSHORT, 0);
	DbClass->sqlin  ((long *) &PrGrStuf, SQLLONG, 0);
	DbClass->sqlin  ((double *) &a, SQLDOUBLE, 0);
	dsqlstatus = DbClass->sqlcomm (_T("select mdn from ipr where mdn = ? and pr_gr_stuf = ? and a = ? and kun_pr = 0 and kun = 0"));
	    sprintf (mess, "dsqlstatus = %ld", dsqlstatus);
//		MessageBox (NULL,_T(mess), NULL, MB_OK | MB_ICONERROR); 

    if (dsqlstatus == 100) // 2. Versuch,  bei Klingler ar hier immer sqlstatus 100, irgenwo ist der Stack von sqlin durcheinander geraten
	{
		DbClass->sqlout  ((short *) &dmdn, SQLSHORT, 0);
		DbClass->sqlin  ((short *) &mdn, SQLSHORT, 0);
		DbClass->sqlin  ((long *) &PrGrStuf, SQLLONG, 0);
		DbClass->sqlin  ((double *) &a, SQLDOUBLE, 0);
		dsqlstatus = DbClass->sqlcomm (_T("select mdn from ipr where mdn = ? and pr_gr_stuf = ? and a = ? and kun_pr = 0 and kun = 0"));
		    sprintf (mess, "2. Versuch dsqlstatus = %ld", dsqlstatus);
//			MessageBox (NULL,_T(mess), NULL, MB_OK | MB_ICONERROR); 
	}

	if (dsqlstatus == 0)
	{
//		MessageBox (NULL,_T("sqlstatus = 0"), NULL, MB_OK | MB_ICONERROR); 
		DbClass->sqlin  ((double *) &pr, SQLDOUBLE, 0);
		DbClass->sqlin  ((double *) &pr, SQLDOUBLE, 0);
		DbClass->sqlin  ((short *) &mdn, SQLSHORT, 0);
		DbClass->sqlin  ((long *) &PrGrStuf, SQLLONG, 0);
		DbClass->sqlin  ((double *) &a, SQLDOUBLE, 0);
	    return DbClass->sqlcomm (_T("update ipr set vk_pr_i = ? , vk_pr_eu = ? where mdn = ? and pr_gr_stuf = ? and a = ? and kun_pr = 0 and kun = 0"));
	}
	else
	{
		ipr.mdn = mdn;
		ipr.pr_gr_stuf = PrGrStuf;
		ipr.kun_pr = 0;
		ipr.a = a;
		ipr.vk_pr_i = pr;
		ipr.ld_pr = 0;
		ipr.aktion_nr = 0;
		ipr.waehrung = 1;
		ipr.kun = 0;
		ipr.a_grund = a;
		ipr.vk_pr_eu = pr;
		ipr.ld_pr_eu = 0;
		ipr.add_ek_proz = 0;
		ipr.add_ek_abs = 0;
		
	    DbClass->sqlin ((short *) &ipr.mdn,SQLSHORT,0);
	    DbClass->sqlin ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
	    DbClass->sqlin ((long *) &ipr.kun_pr,SQLLONG,0);
	    DbClass->sqlin ((double *) &ipr.a,SQLDOUBLE,0);
	    DbClass->sqlin ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
	    DbClass->sqlin ((double *) &ipr.ld_pr,SQLDOUBLE,0);
	    DbClass->sqlin ((short *) &ipr.aktion_nr,SQLSHORT,0);
	    DbClass->sqlin ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
	    DbClass->sqlin ((TCHAR *) ipr.modif,SQLCHAR,2);
	    DbClass->sqlin ((short *) &ipr.waehrung,SQLSHORT,0);
	    DbClass->sqlin ((long *) &ipr.kun,SQLLONG,0);
	    DbClass->sqlin ((double *) &ipr.a_grund,SQLDOUBLE,0);
	    DbClass->sqlin ((TCHAR *) ipr.kond_art,SQLCHAR,5);
	    DbClass->sqlin ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
	    DbClass->sqlin ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
	    DbClass->sqlin ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
	    DbClass->sqlin ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
            return DbClass->sqlcomm  (_T("insert into ipr (mdn,  ")
_T("pr_gr_stuf,  kun_pr,  a,  vk_pr_i,  ld_pr,  aktion_nr,  a_akt_kz,  modif,  waehrung,  ")
_T("kun,  a_grund,  kond_art,  vk_pr_eu,  ld_pr_eu,  add_ek_proz,  add_ek_abs) ")

                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?)")); 


	}


}

