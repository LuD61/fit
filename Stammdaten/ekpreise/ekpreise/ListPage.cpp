// ListPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "AprMan.h"
#include "ListPage.h"
#include "Util.h"
#include "StrFuncs.h"
#include "PriceIpr.h"


// CListPage-Dialogfeld

HANDLE CListPage::Write160Lib = NULL;

IMPLEMENT_DYNAMIC(CListPage, CDialog)

CListPage::CListPage(CWnd* pParent /*=NULL*/)
	: CDialog(CListPage::IDD, pParent)
{
	CString ProgNam;
	CUtil::GetProgNam(ProgNam);
//	Cfg.SetProgName( _T("EkPreise"));
	Cfg.SetProgName( _T(ProgNam.GetBuffer()));
	CellHeight = 0;
	m_List.ArtSelect = "";
    dbild160 = NULL;
	dOpenDbase = NULL;
	Write160 = TRUE;
	MdnGrCursor = -1;
	MdnCursor = -1;
	FilCursor = -1;
	EkFilGr = 99;
	cfg_sql_mode = 1;
	pr_ek_a_mo_we = 0.0;
	pr_ek_a_kalk = 0.0;
	pr_sk_a_kalk = 0.0;
}

CListPage::~CListPage()
{
	DestroyRows (DbRows);
	DestroyRows (ListRows);
}

void CListPage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_List);
}


BEGIN_MESSAGE_MAP(CListPage, CDialog)
	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CListPage-Meldungshandler

BOOL CListPage::OnInitDialog ()
{
	CDialog::OnInitDialog();

	CUtil::GetPersName (PersName);
	PrProt.Construct (PersName, CString ("ekpreise"), &Ek_pr);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	ReadCfg ();


	Etikett.ErfKz = _T("P");
	Etikett.A_bas = &A_bas;
	Calculate.A_bas = &A_bas;
	Calculate.Prdk_k = &Prdk_k;

	strcpy (sys_par.sys_par_nam, "reg_eti_par");
	if (Sys_par.dbreadfirst () == 0)
	{
		Etikett.Mode = _tstoi (sys_par.sys_par_wrt);
	}

	if (Write160 && Write160Lib == NULL)
	{
		CString bws;
		BOOL ret = bws.GetEnvironmentVariable (_T("bws"));
		CString W160Dll;
		if (ret)
		{
			W160Dll.Format (_T("%s\\bin\\bild160dll.dll"), bws.GetBuffer ());
		}
		else
		{
			W160Dll = _T("bild160dll.dll");
		}
		Write160Lib = LoadLibrary (W160Dll.GetBuffer ());
		if (Write160Lib != NULL && dbild160 == NULL)
		{
			dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
			dOpenDbase = (BOOL (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "OpenDbase");
			if (dOpenDbase != NULL)
			{
				(*dOpenDbase) ("bws");
			}
		}
	}
	else if (dbild160 == NULL)
	{
		dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
	}

	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_List.SetImageList (&image, LVSIL_SMALL);   
	}

	Gr_zuord.sqlout ((short *) &Gr_zuord.gr_zuord.gr, SQLSHORT, 0);
	Gr_zuord.sqlout ((char *) Gr_zuord.gr_zuord.gr_bz1, SQLCHAR, sizeof (Gr_zuord.gr_zuord.gr_bz1));
    MdnGrCursor = Gr_zuord.sqlcursor (_T("select gr, gr_bz1 from gr_zuord ")
 									  _T("where mdn = 0 ")
									  _T("and gr > 0 ")
								      _T("order by gr"));

	Mdn.sqlout ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);
	MdnAdr.sqlout ((char *) MdnAdr.adr.adr_krz, SQLCHAR, sizeof (MdnAdr.adr.adr_krz));
    MdnCursor = Mdn.sqlcursor (_T("select mdn.mdn, adr.adr_krz from mdn, adr ")
		                       _T("where mdn.mdn > 0 ")
							   _T("and adr.adr = mdn.adr ")
							   _T("order by mdn.mdn"));

	FillList = m_List;
	FillList.SetStyle (LVS_REPORT);
	if (m_List.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Artikel"), 1, 200, LVCFMT_LEFT);
	FillList.SetCol (_T("Mnd-Gr"), 2, 60, LVCFMT_LEFT);
	FillList.SetCol (_T("Mandant"), 3, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("Fil_Gr"), 4, 60, LVCFMT_LEFT);
	FillList.SetCol (_T("Filiale"), 5, 120, LVCFMT_LEFT);

	int POSFIL = 5 ; 

	int ipos;

    int iAusrichtung = LVCFMT_RIGHT;
	for (ipos = POSFIL+1; ipos < POSFIL+25; ipos ++)
	{
		if (m_List.Nachkomma (ipos) <= -1)  //-1 = Checkbox, -2 = Datum
		{
			iAusrichtung = LVCFMT_CENTER;
		}
		else if (m_List.Nachkomma (ipos) == 0) // char 
		{
			iAusrichtung = LVCFMT_LEFT;
		}
//NEUNEU L�nge auf 0 setzten und nur wenn len > 0 ist reinsetzen
		if (ipos == m_List.Pos_PrEk && m_List.EkLen) FillList.SetCol (_T(m_List.EkBez), ipos, m_List.EkLen, iAusrichtung);
		if (ipos == m_List.Pos_BearbKost && m_List.HkKostLen) FillList.SetCol (_T(m_List.HkKostBez), ipos, m_List.HkKostLen, iAusrichtung);
		if (ipos == m_List.Pos_Aufs_Sk && m_List.SkAufsLen) FillList.SetCol (_T(m_List.SkAufsBez), ipos, m_List.SkAufsLen, iAusrichtung);
		if (ipos == m_List.Pos_Kalk_Hk && m_List.HkLen) FillList.SetCol (_T(m_List.HkBez), ipos, m_List.HkLen, iAusrichtung);


		if (ipos == m_List.Pos_Kalk_Sk && m_List.SkLen) FillList.SetCol (_T(m_List.SkBez), ipos, m_List.SkLen, iAusrichtung);
		if (ipos == m_List.Pos_Aufs_Fil_Ek && m_List.FilEkAufsLen) FillList.SetCol (_T(m_List.FilEkAufsBez), ipos, m_List.FilEkAufsLen, iAusrichtung);
		if (ipos == m_List.Pos_Kalk_Fil_Ek && m_List.FilEkKalkLen) FillList.SetCol (_T(m_List.FilEkKalkBez), ipos, m_List.FilEkKalkLen, iAusrichtung); 
		if (ipos == m_List.Pos_Fil_Ek && m_List.FilEkLen) FillList.SetCol (_T(m_List.FilEkBez), ipos, m_List.FilEkLen, iAusrichtung); 
		if (ipos == m_List.Pos_Aufs_Fil_Vk && m_List.FilVkAufsLen) FillList.SetCol (_T(m_List.FilVkAufsBez), ipos, m_List.FilVkAufsLen, iAusrichtung);
		if (ipos == m_List.Pos_Kalk_Fil_Vk && m_List.FilVkKalkLen) FillList.SetCol (_T(m_List.FilVkKalkBez), ipos, m_List.FilVkKalkLen, iAusrichtung); 
		if (ipos == m_List.Pos_Fil_Vk && m_List.FilVkLen) FillList.SetCol (_T(m_List.FilVkBez), ipos, m_List.FilVkLen, iAusrichtung); 
		if (ipos == m_List.Pos_Aufs_Gh1 && m_List.Gh1AufsLen) FillList.SetCol (_T(m_List.Gh1AufsBez), ipos, m_List.Gh1AufsLen, iAusrichtung); 
		if (ipos == m_List.Pos_KalkVk_Gh1 && m_List.Gh1KalkVkLen) FillList.SetCol (_T(m_List.Gh1KalkVkBez), ipos, m_List.Gh1KalkVkLen, iAusrichtung); 
		if (ipos == m_List.Pos_Gh1 && m_List.Gh1Len) FillList.SetCol (_T(m_List.Gh1Bez), ipos, m_List.Gh1Len, iAusrichtung); 
		if (ipos == m_List.Pos_Sp_Gh1 && m_List.Gh1SpLen) FillList.SetCol (_T(m_List.Gh1SpBez), ipos, m_List.Gh1SpLen, iAusrichtung); 
		if (ipos == m_List.Pos_Aufs_Gh2 && m_List.Gh2AufsLen) FillList.SetCol (_T(m_List.Gh2AufsBez), ipos, m_List.Gh2AufsLen, iAusrichtung); 
		if (ipos == m_List.Pos_KalkVk_Gh2 && m_List.Gh2KalkVkLen) FillList.SetCol (_T(m_List.Gh2KalkVkBez), ipos, m_List.Gh2KalkVkLen, iAusrichtung); 
		if (ipos == m_List.Pos_Gh2 && m_List.Gh2Len) FillList.SetCol (_T(m_List.Gh2Bez), ipos, m_List.Gh2Len, iAusrichtung); 
		if (ipos == m_List.Pos_Sp_Gh2 && m_List.Gh2SpLen) FillList.SetCol (_T(m_List.Gh2SpBez), ipos, m_List.Gh2SpLen, iAusrichtung); 
		if (ipos == m_List.PosA2  && m_List.ALen) FillList.SetCol (_T("Artikel"), ipos, m_List.ALen, iAusrichtung);
		if (m_List.Nachkomma (ipos) == -1)  //Checkbox
		{
			m_List.ColType.Add (new CColType (ipos, m_List.CheckBox)); //NEUNEU Checkbox
		}

	}



	m_List.AddListChangeHandler (this);

// Hauptgrid
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (20, 8);

	CCtrlInfo *c_List     = new CCtrlInfo (&m_List, 0, 0, DOCKRIGHT, DOCKBOTTOM); 
	c_List->rightspace = 12;
	CtrlGrid.Add (c_List);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	FillMdnGrCombo ();
	FillMdnCombo ();
	m_List.Prepare ();

	CtrlGrid.Display ();

	return FALSE;
}

BOOL CListPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				m_List.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				m_List.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_F5)
			{
				StopEnter ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				m_List.DeleteRow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
				m_List.OnKey9 ();
				return TRUE;
			}
	}
    return CDialog::PreTranslateMessage(pMsg);
}

void CListPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

void CListPage::Read (CString& Select, CString& CmpSelect)
{
	CString Sql;
	StopEnter ();
	int dsqlstatus_ek_pr = 0 ;
	m_List.ArtSelect = Select;
	m_List.StopEnter ();
	m_List.DeleteAllItems ();
	m_List.vSelect.clear ();
	m_List.ListRows.Init ();
	DestroyRows (DbRows);
	int i = 0;
	m_List.EnableWindow ();
/*
	m_List.m_Mdn = Mdn.mdn.mdn;
	if (m_List.m_Mdn != 0)
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, 0);
		m_List.SetColumnWidth (m_List.PosMdn, 0);
	}
	else
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, 120);
		m_List.SetColumnWidth (m_List.PosMdn, 120);
	}
*/
// Hier: Sqlstatemant zum Einlesen der Daten Read()
	if (!strcmp (m_List.KalkulationsModus, "a_mo_we") == 0)
	{
	    Ek_pr.sqlout ((double *) &A_bas.a_bas.a, SQLDOUBLE, 0);  
		Sql = _T("select a_bas.a from a_bas ")
			  _T("where a_bas.a > 0 ");
	}
	if (strcmp (m_List.KalkulationsModus, "a_mo_we") == 0)
	{
		if (strcmp(m_List.Lst->lst.AlleArtikel,"N") == 0)
		{
		    Ek_pr.sqlin ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);  
		    Ek_pr.sqlin ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);  
			Ek_pr.sqlout ((double *) &A_bas.a_bas.a, SQLDOUBLE, 0);  
			Ek_pr.sqlout ((double *) &pr_ek_a_kalk, SQLDOUBLE, 0);  
			Ek_pr.sqlout ((double *) &pr_sk_a_kalk, SQLDOUBLE, 0);  
			Sql = _T("select a_bas.a,a_kalkpreis.hk_vollk,a_kalkpreis.sk_vollk from a_bas,ek_pr,outer a_kalkpreis ")
				_T("where a_bas.a > 0 and a_bas.a = ek_pr.a ")
				_T("and a_bas.a = a_kalkpreis.a and a_kalkpreis.mdn = ? and a_kalkpreis.fil = 0 and ")
				_T("ek_pr.mdn = ? and ek_pr.fil = 0 ");
		}
		else
		{
		    Ek_pr.sqlin ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);  
		    Ek_pr.sqlin ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);  
		    Ek_pr.sqlin ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);  
			Ek_pr.sqlout ((double *) &A_bas.a_bas.a, SQLDOUBLE, 0);  
			Ek_pr.sqlout ((double *) &pr_ek_a_mo_we, SQLDOUBLE, 0);  
			Ek_pr.sqlout ((double *) &pr_ek_a_kalk, SQLDOUBLE, 0);  
			Ek_pr.sqlout ((double *) &pr_sk_a_kalk, SQLDOUBLE, 0);  
			if (strcmp(m_List.Lst->lst.HkEdit,"N") == 0)  // wenn Kurznummer nicht angekreuzt ist, sollen nur die S�tze mit Kurznummer erscheinen
			{
				Sql = _T("select a_bas.a,  avg(eur_ek_mo / me_ek_mo), a_kalkpreis.hk_vollk,a_kalkpreis.sk_vollk from a_bas, ek_pr,outer a_mo_we, outer a_kalkpreis ")
				_T("where a_bas.a > 0 and a_bas.a = a_mo_we.a and a_mo_we.me_ek_mo > 0 ")
				_T("and a_bas.a = ek_pr.a and ek_pr.mdn = ? and ek_pr.fil = 0 ")
				_T("and a_bas.a = a_kalkpreis.a and a_kalkpreis.mdn = ? and a_kalkpreis.fil = 0 and ")
				_T("a_mo_we.mdn = ? and a_mo_we.fil = 0 ");
			}
			else  // mit outer ek_pr
			{
				Sql = _T("select a_bas.a,  avg(eur_ek_mo / me_ek_mo), a_kalkpreis.hk_vollk,a_kalkpreis.sk_vollk from a_bas,outer ek_pr,outer a_mo_we, outer a_kalkpreis ")
				_T("where a_bas.a > 0 and a_bas.a = a_mo_we.a and a_mo_we.me_ek_mo > 0 ")
				_T("and a_bas.a = ek_pr.a and ek_pr.mdn = ? and ek_pr.fil = 0 ")
				_T("and a_bas.a = a_kalkpreis.a and a_kalkpreis.mdn = ? and a_kalkpreis.fil = 0 and ")
				_T("a_mo_we.mdn = ? and a_mo_we.fil = 0 ");
			}
		}
	}
	Sql += _T( " ");
	if (CmpSelect.GetLength () > 0)
	{
		Sql += CmpSelect;
		Sql += _T( " ");
	}
	if (Select.GetLength () > 0)
	{
		Sql += Select;
		Sql += _T( " ");
	}
	if (strcmp (m_List.KalkulationsModus, "a_mo_we") == 0 && (strcmp(m_List.Lst->lst.AlleArtikel,"J") == 0)) 
	{
		Sql += _T("group by a_bas.a, a_kalkpreis.hk_vollk,a_kalkpreis.sk_vollk ");
	}

	Sql += _T("order by a_bas.a ");
    AprCursor = Ek_pr.sqlcursor (Sql.GetBuffer ());

	memcpy (&Ek_pr.ek_pr, &ek_pr_null, sizeof (EK_PR));
	Ek_pr.sqlopen (AprCursor);
	Calculate.Init();
	pr_ek_a_mo_we = 0.0;
	pr_ek_a_kalk = 0.0;
	while (Ek_pr.sqlfetch (AprCursor) == 0)
	{
		memcpy (&Ek_pr.ek_pr, &ek_pr_null, sizeof (EK_PR));
		Ek_pr.ek_pr.mdn = m_List.m_Mdn;
		Ek_pr.ek_pr.fil = 0;
		Ek_pr.ek_pr.a   = A_bas.a_bas.a;
 		dsqlstatus_ek_pr = Ek_pr.dbreadfirst ();

//		Ek_pr.ek_pr.pr_ek   = pr_ek_a_mo_we;

		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		Mdn.mdn.mdn = Ek_pr.ek_pr.mdn;

		FillList.InsertItem (i, 0);


		HoleSpaltenwerte(i,m_List.KalkulationsModus);

		CString PrEk;
		m_List.DoubleToString (Ek_pr.ek_pr.pr_ek, PrEk, m_List.Nachkomma(m_List.Pos_PrEk));

		CEkPreise *ek_pr = new CEkPreise (PrEk, Ek_pr.ek_pr);
		DbRows.Add (ek_pr);
		m_List.ListRows.Add (ek_pr);
		pr_ek_a_mo_we = 0.0;
		pr_ek_a_kalk = 0.0;

		i ++;
	}
	Ek_pr.sqlclose (AprCursor);
	Mdn.mdn.mdn = m_List.m_Mdn;
	m_List.SetFocus (); 
	m_List.FirstEnter ();
}


void CListPage::ReadCfg ()
{
    char cfg_v [256];
    char cfg_e [256];
    char cfg_b [256];
    char cfg_n [256];
    char cfg_p [256];
	//Defaults 
	m_List.EkPruef = 0;
	m_List.HkPruef = 0;
	m_List.ALen = 200;
	m_List.AEdit = 1;
	m_List.ALen2 = 0;

	m_List.EkLen = 100;
	m_List.EkEdit = 1;
	m_List.EkNachkomma = 2;
	strcpy (m_List.EkBez, "EK");

	m_List.HkKostLen = 0;
	m_List.HkKostEdit = 0;
	m_List.HkKostNachkomma = 2;
	strcpy (m_List.HkKostBez, "Bearb.kosten");

	m_List.HkLen = 0;
	m_List.HkEdit = 0;
	m_List.HkNachkomma = 2;
	strcpy (m_List.HkBez, "HK");

	m_List.SkAufsLen = 0;
	m_List.SkAufsEdit = 0;
	m_List.SkAufsNachkomma = 2;
	strcpy (m_List.SkAufsBez, "SK %");

	m_List.SkLen = 0;
	m_List.SkEdit = 0;
	m_List.SkNachkomma = 2;
	strcpy (m_List.SkBez, "SK");

	m_List.FilEkAufsLen = 0;
	m_List.FilEkAufsEdit = 0;
	m_List.FilEkAufsNachkomma = 2;
	strcpy (m_List.FilEkAufsBez, "Fil-Ek %");

	m_List.FilEkKalkLen = 0;
	m_List.FilEkKalkEdit = 0;
	m_List.FilEkKalkNachkomma = 2;
	strcpy (m_List.FilEkKalkBez, "kalk.Fil-Ek %");

	m_List.FilEkLen = 0;
	m_List.FilEkEdit = 0;
	m_List.FilEkNachkomma = 2;
	strcpy (m_List.FilEkBez, "Fil-Ek %");

	m_List.FilVkAufsLen = 0;
	m_List.FilVkAufsEdit = 0;
	m_List.FilVkAufsNachkomma = 2;
	strcpy (m_List.FilVkAufsBez, "Fil-Vk %");

	m_List.FilVkKalkLen = 0;
	m_List.FilVkKalkEdit = 0;
	m_List.FilVkKalkNachkomma = 2;
	strcpy (m_List.FilVkKalkBez, "kalk.Fil-Vk %");

	m_List.FilVkLen = 0;
	m_List.FilVkNachkomma = 2;
	m_List.FilVkEdit = 0;
	strcpy (m_List.FilVkBez, "Fil-Vk %");

	m_List.Gh1AufsLen = 0;
	m_List.Gh1AufsNachkomma = 2;
	m_List.Gh1AufsEdit = 0;
	strcpy (m_List.Gh1AufsBez, "GH1 %");

	m_List.Gh1KalkVkLen = 0;
	m_List.Gh1KalkVkNachkomma = 2;
	m_List.Gh1KalkVkEdit = 0;
	strcpy (m_List.Gh1KalkVkBez, "GH1.kalk.Vk");

	m_List.Gh1Len = 0;
	m_List.Gh1Nachkomma = 2;
	m_List.Gh1Edit = 0;
	strcpy (m_List.Gh1Bez, "GH1");

	m_List.Gh1SpLen = 0;
	m_List.Gh1SpNachkomma = 2;
	m_List.Gh1SpEdit = 0;
	strcpy (m_List.Gh1SpBez, "Spanne.GH1");

	m_List.Gh2AufsLen = 0;
	m_List.Gh2AufsNachkomma = 2;
	m_List.Gh2AufsEdit = 0;
	strcpy (m_List.Gh2AufsBez, "GH2 %");

	m_List.Gh2KalkVkLen = 0;
	m_List.Gh2KalkVkNachkomma = 2;
	m_List.Gh2KalkVkEdit = 0;
	strcpy (m_List.Gh2KalkVkBez, "GH2.kalk.Vk");

	m_List.Gh2Len = 0;
	m_List.Gh2Nachkomma = 2;
	m_List.Gh2Edit = 0;
	strcpy (m_List.Gh2Bez, "GH2");

	m_List.Gh2SpLen = 0;
	m_List.Gh2SpNachkomma = 2;
	m_List.Gh2SpEdit = 0;
	strcpy (m_List.Gh2SpBez, "Spanne.GH2");

	m_List.Gh1PrGrStuf = 2;
	m_List.Gh2PrGrStuf = 3;

	strcpy (m_List.KalkulationsModus, "a_kalkpreis");

//LuD Neu CFG
    if (Cfg.GetCfgValue ("KalkulationsModus", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE) 
	{
		strcpy (m_List.KalkulationsModus,cfg_v); 
	}

    if (Cfg.GetCfgValue ("sql_mode", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE) 
	{
		cfg_sql_mode = atoi(cfg_v);
	}

    if (Cfg.GetCfgValue ("Artikel", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE) 
	{
		m_List.ALen = atoi (cfg_v); 
		m_List.AEdit = atoi (cfg_e); 
	}
    if (Cfg.GetCfgValue ("_Ek", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.EkLen = atoi (cfg_v); 
		m_List.EkEdit = atoi (cfg_e); 
		m_List.EkNachkomma = atoi (cfg_n); 
		strcpy (m_List.EkBez, cfg_b);
		m_List.EkPruef = atoi (cfg_p); 

	}

    if (Cfg.GetCfgValue ("_HkKost", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.HkKostLen = atoi (cfg_v);
		m_List.HkKostEdit = atoi (cfg_e);
		m_List.HkKostNachkomma = atoi (cfg_n);
		strcpy (m_List.HkKostBez, cfg_b);
		m_List.HkPruef = atoi (cfg_p); 
	}

    if (Cfg.GetCfgValue ("_Hk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.HkLen = atoi (cfg_v);
		m_List.HkEdit = atoi (cfg_e);
		m_List.HkNachkomma = atoi (cfg_n);
		strcpy (m_List.HkBez, cfg_b);
		m_List.HkPruef = atoi (cfg_p); 
	}

    if (Cfg.GetCfgValue ("_SkAufs", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.SkAufsLen = atoi (cfg_v);
		m_List.SkAufsEdit = atoi (cfg_e);
		m_List.SkAufsNachkomma = atoi (cfg_n);
		strcpy (m_List.SkAufsBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Sk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.SkLen = atoi (cfg_v);
		m_List.SkEdit = atoi (cfg_e);
		m_List.SkNachkomma = atoi (cfg_n);
		strcpy (m_List.SkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilEkAufs", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilEkAufsLen = atoi (cfg_v);
		m_List.FilEkAufsEdit = atoi (cfg_e);
		m_List.FilEkAufsNachkomma = atoi (cfg_n);
		strcpy (m_List.FilEkAufsBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilEkKalk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilEkKalkLen = atoi (cfg_v);
		m_List.FilEkKalkEdit = atoi (cfg_e);
		m_List.FilEkKalkNachkomma = atoi (cfg_n);
		strcpy (m_List.FilEkKalkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilEk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilEkLen = atoi (cfg_v);
		m_List.FilEkEdit = atoi (cfg_e);
		m_List.FilEkNachkomma = atoi (cfg_n);
		strcpy (m_List.FilEkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilVkAufs", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilVkAufsLen = atoi (cfg_v);
		m_List.FilVkAufsEdit = atoi (cfg_e);
		m_List.FilVkAufsNachkomma = atoi (cfg_n);
		strcpy (m_List.FilVkAufsBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilVkKalk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilVkKalkLen = atoi (cfg_v);
		m_List.FilVkKalkEdit = atoi (cfg_e);
		m_List.FilVkKalkNachkomma = atoi (cfg_n);
		strcpy (m_List.FilVkKalkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilVk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilVkLen = atoi (cfg_v);
		m_List.FilVkEdit = atoi (cfg_e);
		m_List.FilVkNachkomma = atoi (cfg_n);
		 strcpy (m_List.FilVkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh1Aufs", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh1AufsLen = atoi (cfg_v);
		m_List.Gh1AufsEdit = atoi (cfg_e);
		m_List.Gh1AufsNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh1AufsBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh1KalkVk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh1KalkVkLen = atoi (cfg_v);
		m_List.Gh1KalkVkEdit = atoi (cfg_e);
		m_List.Gh1KalkVkNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh1KalkVkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh1", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh1Len = atoi (cfg_v);
		m_List.Gh1Edit = atoi (cfg_e);
		m_List.Gh1Nachkomma = atoi (cfg_n);
		strcpy (m_List.Gh1Bez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh1Sp", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh1SpEdit = atoi (cfg_e);
		m_List.Gh1SpLen = atoi (cfg_v);
		m_List.Gh1SpNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh1SpBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh2Aufs", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh2AufsLen = atoi (cfg_v);
		m_List.Gh2AufsEdit = atoi (cfg_e);
		m_List.Gh2AufsNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh2AufsBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh2KalkVk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		 m_List.Gh2KalkVkLen = atoi (cfg_v);
		 m_List.Gh2KalkVkEdit = atoi (cfg_e);
		 m_List.Gh2KalkVkNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh2KalkVkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh2", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh2Len = atoi (cfg_v);
		m_List.Gh2Edit = atoi (cfg_e);
		m_List.Gh2Nachkomma = atoi (cfg_n);
		strcpy (m_List.Gh2Bez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh2Sp", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh2SpLen = atoi (cfg_v);
		m_List.Gh2SpEdit = atoi (cfg_e);
		m_List.Gh2SpNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh2SpBez, cfg_b);
	}
    if (Cfg.GetCfgValue ("_A2", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE) 
	{
		m_List.ALen2 = atoi (cfg_v); 
	}


    if (Cfg.GetCfgValue ("Gh1PrGrStuf", cfg_v) == TRUE) m_List.Gh1PrGrStuf = atoi(cfg_v);
    if (Cfg.GetCfgValue ("Gh2PrGrStuf", cfg_v) == TRUE) m_List.Gh2PrGrStuf = atoi(cfg_v);


/***
    if (Cfg.GetCfgValue ("ALen", cfg_v) == TRUE) m_List.ALen = atoi (cfg_v); 
    if (Cfg.GetCfgValue ("EkLen", cfg_v) == TRUE) strcpy (m_List.EkBez, cfg_v);
    if (Cfg.GetCfgValue ("EkBez", cfg_v) == TRUE) strcpy (m_List.EkBez, cfg_v);

    if (Cfg.GetCfgValue ("HkKostLen", cfg_v) == TRUE) m_List.HkKostLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("HkKostBez", cfg_v) == TRUE) strcpy (m_List.HkKostBez, cfg_v);

    if (Cfg.GetCfgValue ("HkLen", cfg_v) == TRUE) m_List.HkLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("HkBez", cfg_v) == TRUE) strcpy (m_List.HkBez, cfg_v);

    if (Cfg.GetCfgValue ("SkAufsLen", cfg_v) == TRUE) m_List.SkAufsLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("SkAufsBez", cfg_v) == TRUE) strcpy (m_List.SkAufsBez, cfg_v);

    if (Cfg.GetCfgValue ("SkLen", cfg_v) == TRUE) m_List.SkLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("SkBez", cfg_v) == TRUE) strcpy (m_List.SkBez, cfg_v);

    if (Cfg.GetCfgValue ("FilEkAufsLen", cfg_v) == TRUE) m_List.FilEkAufsLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("FilEkAufsBez", cfg_v) == TRUE) strcpy (m_List.FilEkAufsBez, cfg_v);

    if (Cfg.GetCfgValue ("FilEkKalkLen", cfg_v) == TRUE) m_List.FilEkKalkLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("FilEkKalkBez", cfg_v) == TRUE) strcpy (m_List.FilEkKalkBez, cfg_v);

    if (Cfg.GetCfgValue ("FilEkLen", cfg_v) == TRUE) m_List.FilEkLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("FilEkBez", cfg_v) == TRUE) strcpy (m_List.FilEkBez, cfg_v);

    if (Cfg.GetCfgValue ("FilVkAufsLen", cfg_v) == TRUE) m_List.FilVkAufsLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("FilVkAufsBez", cfg_v) == TRUE) strcpy (m_List.FilVkAufsBez, cfg_v);

    if (Cfg.GetCfgValue ("FilVkKalkLen", cfg_v) == TRUE) m_List.FilVkKalkLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("FilVkKalkBez", cfg_v) == TRUE) strcpy (m_List.FilVkKalkBez, cfg_v);

    if (Cfg.GetCfgValue ("FilVkLen", cfg_v) == TRUE) m_List.FilVkLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("FilVkBez", cfg_v) == TRUE) strcpy (m_List.FilVkBez, cfg_v);

    if (Cfg.GetCfgValue ("Gh1AufsLen", cfg_v) == TRUE) m_List.Gh1AufsLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("Gh1AufsBez", cfg_v) == TRUE) strcpy (m_List.Gh1AufsBez, cfg_v);

    if (Cfg.GetCfgValue ("Gh1KalkVkLen", cfg_v) == TRUE) m_List.Gh1KalkVkLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("Gh1KalkVkBez", cfg_v) == TRUE) strcpy (m_List.Gh1KalkVkBez, cfg_v);

    if (Cfg.GetCfgValue ("Gh1Len", cfg_v) == TRUE) m_List.Gh1Len = atoi (cfg_v);
    if (Cfg.GetCfgValue ("Gh1Bez", cfg_v) == TRUE) strcpy (m_List.Gh1Bez, cfg_v);

    if (Cfg.GetCfgValue ("Gh1SpLen", cfg_v) == TRUE) m_List.Gh1SpLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("Gh1SpBez", cfg_v) == TRUE) strcpy (m_List.Gh1SpBez, cfg_v);

    if (Cfg.GetCfgValue ("Gh2AufsLen", cfg_v) == TRUE) m_List.Gh2AufsLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("Gh2AufsBez", cfg_v) == TRUE) strcpy (m_List.Gh2AufsBez, cfg_v);

    if (Cfg.GetCfgValue ("Gh2KalkVkLen", cfg_v) == TRUE) m_List.Gh2KalkVkLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("Gh2KalkVkBez", cfg_v) == TRUE) strcpy (m_List.Gh2KalkVkBez, cfg_v);

    if (Cfg.GetCfgValue ("Gh2Len", cfg_v) == TRUE) m_List.Gh2Len = atoi (cfg_v);
    if (Cfg.GetCfgValue ("Gh2Bez", cfg_v) == TRUE) strcpy (m_List.Gh2Bez, cfg_v);

    if (Cfg.GetCfgValue ("Gh2SpLen", cfg_v) == TRUE) m_List.Gh2SpLen = atoi (cfg_v);
    if (Cfg.GetCfgValue ("Gh2SpBez", cfg_v) == TRUE) strcpy (m_List.Gh2SpBez, cfg_v);

    if (Cfg.GetCfgValue ("Gh1PrGrStuf", cfg_v) == TRUE) m_List.Gh1PrGrStuf = atoi(cfg_v);
    if (Cfg.GetCfgValue ("Gh2PrGrStuf", cfg_v) == TRUE) m_List.Gh2PrGrStuf = atoi(cfg_v);
********/



    if (Cfg.GetCfgValue ("SpaltenMerken", cfg_v) == TRUE) 
	{
		if (atoi (cfg_v) == 1)
		{
			if (Cfg.GetSpaltenValue ("ALen", cfg_v) == TRUE) m_List.ALen = atoi (cfg_v); 
			if (Cfg.GetSpaltenValue ("EkLen", cfg_v) == TRUE) m_List.EkLen = atoi (cfg_v); 
			if (Cfg.GetSpaltenValue ("HkKostLen", cfg_v) == TRUE) m_List.HkKostLen = atoi (cfg_v); 
			if (Cfg.GetSpaltenValue ("HkLen", cfg_v) == TRUE) m_List.HkLen = atoi (cfg_v); 
			if (Cfg.GetSpaltenValue ("SkAufsLen", cfg_v) == TRUE) m_List.SkAufsLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("SkLen", cfg_v) == TRUE) m_List.SkLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("FilEkAufsLen", cfg_v) == TRUE) m_List.FilEkAufsLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("FilEkKalkLen", cfg_v) == TRUE) m_List.FilEkKalkLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("FilEkLen", cfg_v) == TRUE) m_List.FilEkLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("FilVkAufsLen", cfg_v) == TRUE) m_List.FilVkAufsLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("FilVkKalkLen", cfg_v) == TRUE) m_List.FilVkKalkLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("FilVkLen", cfg_v) == TRUE) m_List.FilVkLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("Gh1AufsLen", cfg_v) == TRUE) m_List.Gh1AufsLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("Gh1KalkVkLen", cfg_v) == TRUE) m_List.Gh1KalkVkLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("Gh1Len", cfg_v) == TRUE) m_List.Gh1Len = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("Gh1SpLen", cfg_v) == TRUE) m_List.Gh1SpLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("Gh2AufsLen", cfg_v) == TRUE) m_List.Gh2AufsLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("Gh2KalkVkLen", cfg_v) == TRUE) m_List.Gh2KalkVkLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("Gh2Len", cfg_v) == TRUE) m_List.Gh2Len = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("Gh2SpLen", cfg_v) == TRUE) m_List.Gh2SpLen = atoi (cfg_v);
			if (Cfg.GetSpaltenValue ("ALen2", cfg_v) == TRUE) m_List.ALen2 = atoi (cfg_v); 
		}
	}

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_List.MaxComboEntries = atoi (cfg_v);
    }
/*
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_List.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_List.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_List.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CListPage::StopEnter ()
{
		extern short sql_mode;

		m_List.StopEnter ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
		m_List.DeleteAllItems ();
		sql_mode = cfg_sql_mode;
		m_List.EnableWindow (FALSE);
}

void CListPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CEkPreise *ipr;
	while ((ipr = (CEkPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

void CListPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CEkPreise *ek_pr;
	while ((ek_pr = (CEkPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ek_pr.ek_pr, &ek_pr->ek_pr, sizeof (EK_PR));
		if (!InList (Ek_pr))
		{
			Ek_pr.dbdelete ();
//			PgrProt.Write (1);
		}
	}
}


BOOL CListPage::Write ()
{
	extern short sql_mode;
	short sql_s;

	CString cSysDate; 
	CStrFuncs::SysDate (cSysDate);  
	CString cSysTime; 
	CStrFuncs::SysTime (cSysTime);  

	m_List.ALen = m_List.GetColumnWidth (m_List.PosA);
	m_List.EkLen = m_List.GetColumnWidth (m_List.Pos_PrEk);
	m_List.HkKostLen = m_List.GetColumnWidth (m_List.Pos_BearbKost);
	m_List.HkLen = m_List.GetColumnWidth (m_List.Pos_Kalk_Hk);
	m_List.SkAufsLen = m_List.GetColumnWidth (m_List.Pos_Aufs_Sk);
	m_List.SkLen = m_List.GetColumnWidth (m_List.Pos_Kalk_Sk);
	m_List.FilEkAufsLen = m_List.GetColumnWidth (m_List.Pos_Aufs_Fil_Ek);
	m_List.FilEkKalkLen = m_List.GetColumnWidth (m_List.Pos_Kalk_Fil_Ek);
	m_List.FilEkLen = m_List.GetColumnWidth (m_List.Pos_Fil_Ek);
	m_List.FilVkAufsLen = m_List.GetColumnWidth (m_List.Pos_Aufs_Fil_Vk);
	m_List.FilVkKalkLen = m_List.GetColumnWidth (m_List.Pos_Kalk_Fil_Vk);
	m_List.FilVkLen = m_List.GetColumnWidth (m_List.Pos_Fil_Vk);
	m_List.Gh1AufsLen = m_List.GetColumnWidth (m_List.Pos_Aufs_Gh1);
	m_List.Gh1KalkVkLen = m_List.GetColumnWidth (m_List.Pos_KalkVk_Gh1);
	m_List.Gh1Len = m_List.GetColumnWidth (m_List.Pos_Gh1);
	m_List.Gh1SpLen = m_List.GetColumnWidth (m_List.Pos_Sp_Gh1);
	m_List.Gh2AufsLen = m_List.GetColumnWidth (m_List.Pos_Aufs_Gh2);
	m_List.Gh2KalkVkLen = m_List.GetColumnWidth (m_List.Pos_KalkVk_Gh2);
	m_List.Gh2Len = m_List.GetColumnWidth (m_List.Pos_Gh2);
	m_List.Gh2SpLen = m_List.GetColumnWidth (m_List.Pos_Sp_Gh2);
	m_List.ALen2 = m_List.GetColumnWidth (m_List.PosA2);


// LuD Neu Schreibe Spalten in tmp
	LPTSTR tmp = getenv ("TMPPATH");
	CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\ekpreise.spalten", tmp);
	}
	else
	{
		dName = "ekpreise.splalten";
	}
	if (m_List.HkLen > 0 || m_List.SkLen > 0 || m_List.FilEkLen > 0 || m_List.FilEkKalkLen > 0 || m_List.FilVkLen > 0)
	{
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{    // LuD Neu L�ngen in ekpreise.spalten schreiben
			fprintf (fp, "ALen %hd \n",m_List.ALen);
			fprintf (fp, "EkLen %hd \n",m_List.EkLen);
			fprintf (fp, "HkKostLen %hd \n",m_List.HkKostLen);
			fprintf (fp, "HkLen %hd \n",m_List.HkLen);
			fprintf (fp, "SkAufsLen %hd \n",m_List.SkAufsLen);
			fprintf (fp, "SkLen %hd \n",m_List.SkLen);
			fprintf (fp, "FilEkAufsLen %hd \n",m_List.FilEkAufsLen);
			fprintf (fp, "FilEkKalkLen %hd \n",m_List.FilEkKalkLen);
			fprintf (fp, "FilEkLen %hd \n",m_List.FilEkLen);
			fprintf (fp, "FilVkAufsLen %hd \n",m_List.FilVkAufsLen);
			fprintf (fp, "FilVkKalkLen %hd \n",m_List.FilVkKalkLen);
			fprintf (fp, "FilVkLen %hd \n",m_List.FilVkLen);
			fprintf (fp, "Gh1AufsLen %hd \n",m_List.Gh1AufsLen);
			fprintf (fp, "Gh1KalkVkLen %hd \n",m_List.Gh1KalkVkLen);
			fprintf (fp, "Gh1Len %hd \n",m_List.Gh1Len);
			fprintf (fp, "Gh1SpLen %hd \n",m_List.Gh1SpLen);
			fprintf (fp, "Gh2AufsLen %hd \n",m_List.Gh2AufsLen);
			fprintf (fp, "Gh2KalkVkLen %hd \n",m_List.Gh2KalkVkLen);
			fprintf (fp, "Gh2Len %hd \n",m_List.Gh2Len);
			fprintf (fp, "Gh2SpLen %hd \n",m_List.Gh2SpLen);
			fprintf (fp, "ALen2 %hd \n",m_List.ALen2);
			fclose (fp);
		}
	}


	if (!m_List.IsWindowEnabled ())
	{
		return FALSE;
	}

// Pr�fen auf doppelte Eintr�ge 

	m_List.StopEnter ();
	int count = m_List.GetItemCount ();
/*
	for (int i = 0; i < count; i ++)
	{
		if (!m_List.TestAprIndexM (i))
		{
			MessageBox (_T("Die Daten k�nnen nicht gespeichert werden!\n")
				        _T("Es sind mindestens 2 Eintr�ge auf gleicher Unternehmesebene vorhanden"),
						NULL,
						MB_OK | MB_ICONERROR);
			return FALSE;
		}
	}
*/

	sql_s = sql_mode;
	sql_mode = cfg_sql_mode;
	Ek_pr.beginwork ();
	m_List.StopEnter ();
	count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 EK_PR *ek_pr = new EK_PR;
		 memcpy (ek_pr, &ek_pr_null, sizeof (EK_PR));
         CString Text;
		 Text = m_List.GetItemText (i, m_List.PosA);
		 ek_pr->a = CStrFuncs::StrToDouble (Text);
		 Text = m_List.GetItemText (i, m_List.PosMdn);
		 ek_pr->mdn = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosFil);
		 ek_pr->fil = atoi (Text);
     
     	 CString PrEk =  m_List.GetItemText (i, m_List.Pos_PrEk);
		 ek_pr->pr_ek = CStrFuncs::StrToDouble (PrEk);

     	 CString HkKost =  m_List.GetItemText (i, m_List.Pos_BearbKost);
		 ek_pr->hk_kost = CStrFuncs::StrToDouble (HkKost);
     	 CString Hk =  m_List.GetItemText (i, m_List.Pos_Kalk_Hk);
		 ek_pr->hk = CStrFuncs::StrToDouble (Hk);
     	 CString SkAufs =  m_List.GetItemText (i, m_List.Pos_Aufs_Sk);
		 ek_pr->sk_aufs = CStrFuncs::StrToDouble (SkAufs);
     	 CString Sk =  m_List.GetItemText (i, m_List.Pos_Kalk_Sk);
		 ek_pr->sk = CStrFuncs::StrToDouble (Sk);
     	 CString FilEkAufs =  m_List.GetItemText (i, m_List.Pos_Aufs_Fil_Ek);
		 ek_pr->fil_ek_aufs = CStrFuncs::StrToDouble (FilEkAufs);
     	 CString FilEkKalk =  m_List.GetItemText (i, m_List.Pos_Kalk_Fil_Ek);
		 ek_pr->fil_ek_kalk = CStrFuncs::StrToDouble (FilEkKalk);
     	 CString FilEk =  m_List.GetItemText (i, m_List.Pos_Fil_Ek);
		 ek_pr->fil_ek = CStrFuncs::StrToDouble (FilEk);
     	 CString FilVkAufs =  m_List.GetItemText (i, m_List.Pos_Aufs_Fil_Vk);
		 ek_pr->fil_vk_aufs = CStrFuncs::StrToDouble (FilVkAufs);
     	 CString FilVkKalk =  m_List.GetItemText (i, m_List.Pos_Kalk_Fil_Vk);
		 ek_pr->fil_vk_kalk = CStrFuncs::StrToDouble (FilVkKalk);
     	 CString FilVk =  m_List.GetItemText (i, m_List.Pos_Fil_Vk);
		 ek_pr->fil_vk = CStrFuncs::StrToDouble (FilVk);
     	 CString Gh1Aufs =  m_List.GetItemText (i, m_List.Pos_Aufs_Gh1);
		 ek_pr->gh1_aufs = CStrFuncs::StrToDouble (Gh1Aufs);
     	 CString Gh1KalkVk =  m_List.GetItemText (i, m_List.Pos_KalkVk_Gh1);
		 ek_pr->gh1_kalk_vk = CStrFuncs::StrToDouble (Gh1KalkVk);
     	 CString Gh1 =  m_List.GetItemText (i, m_List.Pos_Gh1);
		 ek_pr->gh1 = CStrFuncs::StrToDouble (Gh1);
     	 CString Gh1Sp =  m_List.GetItemText (i, m_List.Pos_Sp_Gh1);
		 ek_pr->gh1_sp = CStrFuncs::StrToDouble (Gh1Sp);
     	 CString Gh2Aufs =  m_List.GetItemText (i, m_List.Pos_Aufs_Gh2);
		 ek_pr->gh2_aufs = CStrFuncs::StrToDouble (Gh2Aufs);
     	 CString Gh2KalkVk =  m_List.GetItemText (i, m_List.Pos_KalkVk_Gh2);
		 ek_pr->gh2_kalk_vk = CStrFuncs::StrToDouble (Gh2KalkVk);
     	 CString Gh2 =  m_List.GetItemText (i, m_List.Pos_Gh2);
		 ek_pr->gh2 = CStrFuncs::StrToDouble (Gh2);
     	 CString Gh2Sp =  m_List.GetItemText (i, m_List.Pos_Sp_Gh2);
		 ek_pr->gh2_sp = CStrFuncs::StrToDouble (Gh2Sp);




		 if (!TestDecValues (ek_pr)) 
		 {
			 MessageBox (_T("Maximalwert f�r Preisfeld �berschritten"));
			 return FALSE;
		 }

		 CEkPreise *pr = new CEkPreise (PrEk, *ek_pr);
//		 if (ek_pr->pr_ek_euro != 0.0 || 
//			 ek_pr->pr_vk_euro != 0.0)
		 {
				ListRows.Add (pr);
		 }
		 delete ek_pr;
	}

	if (!strcmp (m_List.KalkulationsModus, "a_mo_we") == 0)	DeleteDbRows ();

	Etikett.Clear ();
	ListRows.FirstPosition ();
	CEkPreise *ek_pr;
	while ((ek_pr = (CEkPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&Ek_pr.ek_pr, &ek_pr->ek_pr, sizeof (EK_PR));
		CString Date;
		CStrFuncs::SysDate (Date);
//040510		Ek_pr.ek_pr.akt = -1;
		Ek_pr.ek_pr.mdn = Mdn.mdn.mdn;
		ek_pr->ek_pr.mdn = Mdn.mdn.mdn;
//	    Ek_pr.dbreadfirst ();

//		..  ZuTun
//		..
//		Ek_pr.ek_pr.pr_vk_euro = ek_pr->ek_pr.pr_vk_euro;
//		Ek_pr.ek_pr.pr_vk = ek_pr->ek_pr.pr_vk;
		CEkPreise old_ek_pr;
//		EK_PR AprWrite;
//		memcpy (&AprWrite, &Ek_pr.ek_pr, sizeof (EK_PR));
//hier: Schreiben der Daten
		BOOL FlgUpda_kalkpreis = FALSE;
		if  (strcmp(m_List.Lst->lst.EkEdit,"J") == 0 || 
				strcmp(m_List.Lst->lst.HkKostEdit,"J") == 0 || 
				strcmp(m_List.Lst->lst.HkEdit,"J") == 0 ||
				strcmp(m_List.Lst->lst.SkEdit,"J") == 0 
				)
		{
				memcpy (&A_Kalkpreis.a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS)); 
				A_Kalkpreis.a_kalkpreis.mdn = Ek_pr.ek_pr.mdn;
				A_Kalkpreis.a_kalkpreis.fil = Ek_pr.ek_pr.fil;
				A_Kalkpreis.a_kalkpreis.a = Ek_pr.ek_pr.a;
				A_Kalkpreis.dbreadfirst(); 
		}
		BOOL FlgUpda_kalk_mat = FALSE;
		if  (strcmp(m_List.Lst->lst.EkEdit,"J") == 0 || 
				strcmp(m_List.Lst->lst.HkKostEdit,"J") == 0 || 
				strcmp(m_List.Lst->lst.HkEdit,"J") == 0 
				)
		{
				memcpy (&A_Kalk_mat.a_kalk_mat, &a_kalk_mat_null, sizeof (A_KALK_MAT)); 
				A_Kalk_mat.a_kalk_mat.mdn = Ek_pr.ek_pr.mdn;
				A_Kalk_mat.a_kalk_mat.fil = Ek_pr.ek_pr.fil;
				A_Kalk_mat.a_kalk_mat.a = Ek_pr.ek_pr.a;
				A_Kalk_mat.dbreadfirst(); 
		}
		BOOL FlgUpda_kalk_eig = FALSE;
		if  (strcmp(m_List.Lst->lst.EkEdit,"J") == 0 || 
				strcmp(m_List.Lst->lst.HkKostEdit,"J") == 0 || 
				strcmp(m_List.Lst->lst.HkEdit,"J") == 0 ||
				strcmp(m_List.Lst->lst.SkEdit,"J") == 0 
				)
		{
				memcpy (&A_Kalk_eig.a_kalk_eig, &a_kalk_eig_null, sizeof (A_KALK_EIG)); 
				A_Kalk_eig.a_kalk_eig.mdn = Ek_pr.ek_pr.mdn;
				A_Kalk_eig.a_kalk_eig.fil = Ek_pr.ek_pr.fil;
				A_Kalk_eig.a_kalk_eig.a = Ek_pr.ek_pr.a;
				A_Kalk_eig.ToDbDate (cSysDate  , &A_Kalk_eig.a_kalk_eig.dat);
				A_Kalk_eig.dbreadfirst(); 
		}
		BOOL FlgUpda_kalkhndw = FALSE;
		if  (strcmp(m_List.Lst->lst.EkEdit,"J") == 0 || 
				strcmp(m_List.Lst->lst.SkEdit,"J") == 0 
				)
		{
				memcpy (&A_Kalkhndw.a_kalkhndw, &a_kalkhndw_null, sizeof (A_KALKHNDW)); 
				A_Kalkhndw.a_kalkhndw.mdn = Ek_pr.ek_pr.mdn;
				A_Kalkhndw.a_kalkhndw.fil = Ek_pr.ek_pr.fil;
				A_Kalkhndw.a_kalkhndw.a = Ek_pr.ek_pr.a;
				A_Kalkhndw.ToDbDate (cSysDate  , &A_Kalkhndw.a_kalkhndw.dat);
				A_Kalkhndw.dbreadfirst(); 
		}
		BOOL FlgUpda_pr = FALSE;
		if  (strcmp(m_List.Lst->lst.FilEkEdit,"J") == 0  || strcmp(m_List.Lst->lst.FilVkEdit,"J") == 0 ) 
		{ 
				memcpy (&A_Pr.a_pr, &a_pr_null, sizeof (APR_CLASS)); 
				A_Pr.a_pr.mdn = Ek_pr.ek_pr.mdn;
				A_Pr.a_pr.fil = Ek_pr.ek_pr.fil;
				A_Pr.a_pr.mdn_gr = 0;
				A_Pr.a_pr.fil_gr = 0;
				A_Pr.a_pr.fil = Ek_pr.ek_pr.fil;
				A_Pr.a_pr.a = Ek_pr.ek_pr.a;
				A_Pr.a_pr.key_typ_dec13 = Ek_pr.ek_pr.a;
				A_Pr.dbreadfirst(); 
		}
		BOOL FlgUpdiprGh2 = FALSE;
		/*****
		if  (strcmp(m_List.Lst->lst.Gh2Edit,"J") == 0) 
		{ 
				memcpy (&IPrGh1.ipr, &ipr_null, sizeof (IPR_CLASS)); 
				IPrGh1.ipr.mdn = Ek_pr.ek_pr.mdn;
				IPrGh1.ipr.pr_gr_stuf = m_List.Gh2PrGrStuf;
				IPrGh1.ipr.kun_pr = 0;
				IPrGh1.ipr.kun = 0;
				IPrGh1.ipr.a = Ek_pr.ek_pr.a;
				IPrGh1.dbreadfirst(); 
				memcpy (&IPrGh2.ipr2, &IPrGh1.ipr, sizeof (IPR2_CLASS)); 
		}
		********/
		BOOL FlgUpdiprGh1 = FALSE;
		/*********
		if  (strcmp(m_List.Lst->lst.Gh1Edit,"J") == 0) 
		{ 
				memcpy (&IPrGh1.ipr, &ipr_null, sizeof (IPR_CLASS)); 
				IPrGh1.ipr.mdn = Ek_pr.ek_pr.mdn;
				IPrGh1.ipr.pr_gr_stuf = m_List.Gh1PrGrStuf;
				IPrGh1.ipr.kun_pr = 0;
				IPrGh1.ipr.kun = 0;
				IPrGh1.ipr.a = Ek_pr.ek_pr.a;
				IPrGh1.dbreadfirst(); 
		}
		*******/
		if (IsChanged (ek_pr, &old_ek_pr))
		{
			Ek_pr.ToDbDate (Date, &Ek_pr.ek_pr.bearb);

// hier: schreiben _EK
			if  (strcmp(m_List.Lst->lst.EkEdit,"J") == 0) 
			{
				Ek_pr.ek_pr.pr_ek = ek_pr->ek_pr.pr_ek;
				if  (strcmp(m_List.Lst->lst.HkEdit,"N") == 0) Ek_pr.ek_pr.hk = ek_pr->ek_pr.pr_ek + ek_pr->ek_pr.hk_kost;
				if  (strcmp(m_List.Lst->lst.SkEdit,"N") == 0 ) Ek_pr.ek_pr.sk = ek_pr->ek_pr.pr_ek + ek_pr->ek_pr.hk_kost * ( 1 + (ek_pr->ek_pr.sk_aufs / 100)); 
				if (A_Kalkpreis.a_kalkpreis.mat_o_b != Ek_pr.ek_pr.pr_ek)
				{
					A_Kalkpreis.a_kalkpreis.mat_o_b = Ek_pr.ek_pr.pr_ek;
					if  (strcmp(m_List.Lst->lst.HkEdit,"N") == 0) A_Kalkpreis.a_kalkpreis.hk_vollk = Ek_pr.ek_pr.pr_ek + Ek_pr.ek_pr.hk_kost;
					if  (strcmp(m_List.Lst->lst.SkEdit,"N") == 0) A_Kalkpreis.a_kalkpreis.sk_vollk = Ek_pr.ek_pr.sk ;
					FlgUpda_kalkpreis = TRUE;
				}
				if (A_Kalk_mat.a_kalk_mat.mat_o_b != Ek_pr.ek_pr.pr_ek && Calculate.isMaterial(Ek_pr.ek_pr.a))
				{
					A_Kalk_mat.a_kalk_mat.mat_o_b = Ek_pr.ek_pr.pr_ek;
					if  (strcmp(m_List.Lst->lst.HkEdit,"N") == 0) A_Kalk_mat.a_kalk_mat.hk_vollk = Ek_pr.ek_pr.pr_ek + Ek_pr.ek_pr.hk_kost;
					FlgUpda_kalk_mat = TRUE;
				}
				if (A_Kalk_eig.a_kalk_eig.mat_o_b != Ek_pr.ek_pr.pr_ek && Calculate.isEigenprodukt(Ek_pr.ek_pr.a))
				{
					A_Kalk_eig.a_kalk_eig.mat_o_b = Ek_pr.ek_pr.pr_ek;
					if  (strcmp(m_List.Lst->lst.HkEdit,"N") == 0) A_Kalk_eig.a_kalk_eig.hk_vollk = Ek_pr.ek_pr.pr_ek + Ek_pr.ek_pr.hk_kost;
					if  (strcmp(m_List.Lst->lst.SkEdit,"N") == 0) A_Kalk_eig.a_kalk_eig.sk_vollk = Ek_pr.ek_pr.sk ;
					FlgUpda_kalk_eig = TRUE;
				}
				if (A_Kalkhndw.a_kalkhndw.pr_ek1 != Ek_pr.ek_pr.pr_ek && Calculate.isHandelsware(Ek_pr.ek_pr.a))
				{
					A_Kalkhndw.a_kalkhndw.pr_ek1 = Ek_pr.ek_pr.pr_ek;
					if  (strcmp(m_List.Lst->lst.SkEdit,"N") == 0) A_Kalkhndw.a_kalkhndw.sk_vollk = Ek_pr.ek_pr.sk ;
					FlgUpda_kalkhndw = TRUE;
				}
			}
// hier: schreiben _HK_KOST
			if  (strcmp(m_List.Lst->lst.HkKostEdit,"J") == 0) 
			{
				Ek_pr.ek_pr.hk_kost = ek_pr->ek_pr.hk_kost;
				if (A_Kalkpreis.a_kalkpreis.kost != Ek_pr.ek_pr.hk_kost)
				{
					A_Kalkpreis.a_kalkpreis.kost = Ek_pr.ek_pr.hk_kost;
					FlgUpda_kalkpreis = TRUE;
				}
				if (A_Kalk_mat.a_kalk_mat.kost != Ek_pr.ek_pr.pr_ek && Calculate.isMaterial(Ek_pr.ek_pr.a))
				{
					A_Kalk_mat.a_kalk_mat.kost = Ek_pr.ek_pr.hk_kost;
					FlgUpda_kalk_mat = TRUE;
				}
				if (A_Kalk_eig.a_kalk_eig.kost != Ek_pr.ek_pr.hk_kost && Calculate.isEigenprodukt(Ek_pr.ek_pr.a))
				{
					A_Kalk_eig.a_kalk_eig.kost = Ek_pr.ek_pr.hk_kost;
					FlgUpda_kalk_eig = TRUE;
				}
	//			Calculate.WriteBearbKost (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a, Ek_pr.ek_pr.hk_kost );
			}
// hier: schreiben _HK

			if  (strcmp(m_List.Lst->lst.HkEdit,"J") == 0)  
			{
				Ek_pr.ek_pr.hk = ek_pr->ek_pr.hk;
				if (A_Kalkpreis.a_kalkpreis.hk_vollk != Ek_pr.ek_pr.hk)
				{
					A_Kalkpreis.a_kalkpreis.hk_vollk = Ek_pr.ek_pr.hk;
					FlgUpda_kalkpreis = TRUE;
				}
				if (A_Kalk_mat.a_kalk_mat.hk_vollk != Ek_pr.ek_pr.hk && Calculate.isMaterial(Ek_pr.ek_pr.a))
				{
					A_Kalk_mat.a_kalk_mat.hk_vollk = Ek_pr.ek_pr.hk;
					FlgUpda_kalk_mat = TRUE;
				}
				if (A_Kalk_eig.a_kalk_eig.hk_vollk != Ek_pr.ek_pr.hk && Calculate.isEigenprodukt(Ek_pr.ek_pr.a))
				{
					A_Kalk_eig.a_kalk_eig.hk_vollk = Ek_pr.ek_pr.hk;
					FlgUpda_kalk_eig = TRUE;
				}
			}
// hier: schreiben _SkAufs
			if  (strcmp(m_List.Lst->lst.SkAufsEdit,"J") == 0) 
			{
				Ek_pr.ek_pr.sk_aufs = ek_pr->ek_pr.sk_aufs;
			}
// hier: schreiben _Sk       
			if  (strcmp(m_List.Lst->lst.SkEdit,"J") == 0) 
			{
				if (A_Kalkpreis.a_kalkpreis.sk_vollk != Ek_pr.ek_pr.sk)
				{
					A_Kalkpreis.a_kalkpreis.sk_vollk = Ek_pr.ek_pr.sk;
					FlgUpda_kalkpreis = TRUE;
				}
				if (A_Kalkhndw.a_kalkhndw.sk_vollk != Ek_pr.ek_pr.sk && Calculate.isMaterial(Ek_pr.ek_pr.a))
				{
					A_Kalkhndw.a_kalkhndw.sk_vollk = Ek_pr.ek_pr.sk;
					FlgUpda_kalkhndw = TRUE;
				}
				if (A_Kalk_eig.a_kalk_eig.hk_vollk != Ek_pr.ek_pr.sk && Calculate.isEigenprodukt(Ek_pr.ek_pr.a))
				{
					A_Kalk_eig.a_kalk_eig.hk_vollk = Ek_pr.ek_pr.sk;
					FlgUpda_kalk_eig = TRUE;
				}
			}
// hier: schreiben _FilEkAufs
// hier: schreiben _FilEkKalk
// hier: schreiben _FilEk    
			if  (strcmp(m_List.Lst->lst.FilEkEdit,"J") == 0) 
			{
				if (A_Pr.a_pr.pr_ek_euro != Ek_pr.ek_pr.fil_ek)
				{
					A_Pr.a_pr.pr_ek_euro = Ek_pr.ek_pr.fil_ek;
					A_Pr.a_pr.pr_ek = Ek_pr.ek_pr.fil_ek;
					FlgUpda_pr = TRUE;
				}
			}
// hier: schreiben _FilVkAufs
// hier: schreiben _FilVkKalk
// hier: schreiben _FilVk    
			if  (strcmp(m_List.Lst->lst.FilVkEdit,"J") == 0) 
			{
				if (A_Pr.a_pr.pr_vk_euro != Ek_pr.ek_pr.fil_vk)
				{
					A_Pr.a_pr.pr_vk_euro = Ek_pr.ek_pr.fil_vk;
					A_Pr.a_pr.pr_vk = Ek_pr.ek_pr.fil_vk;
					FlgUpda_pr = TRUE;
				}
			}
// hier: schreiben _Gh1Aufs  
// hier: schreiben _Gh1KalkVk
// hier: schreiben _Gh1      
			if  (strcmp(m_List.Lst->lst.Gh1Edit,"J") == 0)  
			{
				/**
				memcpy (&IPrGh1.ipr, &ipr_null, sizeof (IPR_CLASS)); 
				IPrGh1.ipr.mdn = Ek_pr.ek_pr.mdn;
				IPrGh1.ipr.pr_gr_stuf = m_List.Gh1PrGrStuf;
				IPrGh1.ipr.kun_pr = 0;
				IPrGh1.ipr.kun = 0;
				IPrGh1.ipr.a = Ek_pr.ek_pr.a;
				IPrGh1.dbreadfirst(); 
				*****/
				Pr_ipr.ipr.mdn = Ek_pr.ek_pr.mdn;
				Pr_ipr.ipr.kun_pr = 0;
				Pr_ipr.ipr.kun = 0;
				Pr_ipr.ipr.pr_gr_stuf = m_List.Gh1PrGrStuf;
				Pr_ipr.ipr.a = Ek_pr.ek_pr.a;
				Calculate.UpdateIpr(Ek_pr.ek_pr.mdn, m_List.Gh1PrGrStuf,Ek_pr.ek_pr.a,Ek_pr.ek_pr.gh1);
				FlgUpdiprGh1 = TRUE;
				/***
				if (Pr_ipr.dbfetchpreisIpr() != 0)
				{
					Ek_pr.ek_pr.gh1 = ek_pr->ek_pr.gh1;
					if (Pr_ipr.ipr.vk_pr_eu != Ek_pr.ek_pr.gh1)
					{
						Pr_ipr.ipr.vk_pr_eu = Ek_pr.ek_pr.gh1;
						Pr_ipr.ipr.vk_pr_i = Ek_pr.ek_pr.gh1;
						Pr_ipr.dbupdate();
						FlgUpdiprGh1 = TRUE;
					}
**/
					/**
					if (IPrGh1.ipr.vk_pr_eu != Ek_pr.ek_pr.gh1)
					{
						IPrGh1.ipr.vk_pr_eu = Ek_pr.ek_pr.gh1;
						IPrGh1.ipr.vk_pr_i = Ek_pr.ek_pr.gh1;
						IPrGh1.dbupdate();
						FlgUpdiprGh1 = TRUE;
					}
				}
					**/
			}
// hier: schreiben _Gh1Sp    
// hier: schreiben _Gh2Aufs  
// hier: schreiben _Gh2KalkVk
// hier: schreiben _Gh2      
			if  (strcmp(m_List.Lst->lst.Gh2Edit,"J") == 0)  
			{
				/*
				memcpy (&IPrGh1.ipr, &ipr_null, sizeof (IPR_CLASS)); 
				IPrGh1.ipr.mdn = Ek_pr.ek_pr.mdn;
				IPrGh1.ipr.pr_gr_stuf = m_List.Gh2PrGrStuf;
				IPrGh1.ipr.kun_pr = 0;
				IPrGh1.ipr.kun = 0;
				IPrGh1.ipr.a = Ek_pr.ek_pr.a;
				IPrGh1.dbreadfirst(); 
				Ek_pr.ek_pr.gh2 = ek_pr->ek_pr.gh2;
				if (IPrGh1.ipr.vk_pr_eu != Ek_pr.ek_pr.gh2)
				{
					IPrGh1.ipr.vk_pr_eu = Ek_pr.ek_pr.gh2;
					IPrGh1.ipr.vk_pr_i = Ek_pr.ek_pr.gh2;
					IPrGh1.dbupdate();

					FlgUpdiprGh2 = TRUE;
				}
				*/
				Pr_ipr.ipr.mdn = Ek_pr.ek_pr.mdn;
				Pr_ipr.ipr.kun_pr = 0;
				Pr_ipr.ipr.kun = 0;
				Pr_ipr.ipr.pr_gr_stuf = m_List.Gh2PrGrStuf;
				Pr_ipr.ipr.a = Ek_pr.ek_pr.a;
				Calculate.UpdateIpr(Ek_pr.ek_pr.mdn, m_List.Gh2PrGrStuf,Ek_pr.ek_pr.a,Ek_pr.ek_pr.gh2);
				FlgUpdiprGh1 = TRUE;
				/*
				if (Pr_ipr.dbfetchpreisIpr() != 0)
				{
					Ek_pr.ek_pr.gh1 = ek_pr->ek_pr.gh1;
					if (Pr_ipr.ipr.vk_pr_eu != Ek_pr.ek_pr.gh1)
					{
						Pr_ipr.ipr.vk_pr_eu = Ek_pr.ek_pr.gh1;
						Pr_ipr.ipr.vk_pr_i = Ek_pr.ek_pr.gh1;
						Pr_ipr.dbupdate();
						FlgUpdiprGh1 = TRUE;
					}
				}
				*/

			}
// hier: schreiben _Gh2Sp    


//hier : schreiben ek_pr
			if  (strcmp(m_List.Lst->lst.EkEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.HkKostEdit,"J") == 0 ||
					strcmp(m_List.Lst->lst.HkEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.SkAufsEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.FilEkAufsEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.FilEkEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.FilVkAufsEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.FilVkEdit,"J") == 0 || 

					strcmp(m_List.Lst->lst.Gh1AufsEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.Gh1KalkVkEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.Gh1Edit,"J") == 0 || 
					strcmp(m_List.Lst->lst.Gh1SpEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.Gh2AufsEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.Gh2KalkVkEdit,"J") == 0 || 
					strcmp(m_List.Lst->lst.Gh2Edit,"J") == 0 || 
					strcmp(m_List.Lst->lst.Gh2SpEdit,"J") == 0  
					)
			{
 				Ek_pr.dbupdate_e (); 
			}
			if  (FlgUpda_kalkpreis == TRUE)
			{
				strcpy(A_Kalkpreis.a_kalkpreis.programm, "ekpreise");
				strcpy(A_Kalkpreis.a_kalkpreis.version, "08.09.2012");
				A_Kalkpreis.ToDbDate (cSysDate  , &A_Kalkpreis.a_kalkpreis.dat);
				sprintf ( A_Kalkpreis.a_kalkpreis.zeit , "%s" , cSysTime.GetBuffer() ) ;
				A_Kalkpreis.dbupdate();
			}
			if  (FlgUpda_kalk_mat == TRUE)
			{
				A_Kalk_mat.ToDbDate (cSysDate  , &A_Kalk_mat.a_kalk_mat.dat);
				A_Kalk_mat.dbupdate();
			}
			if  (FlgUpda_kalk_eig == TRUE)
			{
				A_Kalk_eig.ToDbDate (cSysDate  , &A_Kalk_eig.a_kalk_eig.aend_dat);
				A_Kalk_eig.dbupdate();
			}
			if  (FlgUpda_kalkhndw == TRUE)
			{
				A_Kalkhndw.ToDbDate (cSysDate  , &A_Kalkhndw.a_kalkhndw.aend_dat);
				A_Kalkhndw.dbupdate();
			}
			if  (FlgUpda_pr == TRUE)
			{
				A_Pr.ToDbDate (cSysDate  , &A_Pr.a_pr.bearb);
				A_Pr.dbupdate();
				if (Write160 && dbild160 != NULL)
				{
					CStringA Command;
					Command.Format ("bild160 B %.0lf %hd %hd %hd %hd",
					             A_Pr.a_pr.a,
								 A_Pr.a_pr.mdn_gr,
								 A_Pr.a_pr.mdn,
								 A_Pr.a_pr.fil_gr,
								 A_Pr.a_pr.fil); 

					(*dbild160) (Command.GetBuffer ());
				}

			}
			if  (FlgUpdiprGh2 == TRUE)
			{
				FlgUpdiprGh2 = FALSE;
			}
			if  (FlgUpdiprGh1 == TRUE)
			{
				FlgUpdiprGh1 = FALSE;
			}
		} // if isChanged
	} // while



//	Etikett.TestPrint ();
	Ek_pr.commitwork ();
	sql_mode = sql_s;
	StopEnter ();
	return TRUE;
}

BOOL CListPage::TestWrite ()
{
	if (!m_List.IsWindowEnabled ())
	{
		return FALSE;
	}

// Pr�fen auf doppelte Eintr�ge 

	m_List.GetEnter ();
	int count = m_List.GetItemCount ();
	count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 EK_PR *ek_pr = new EK_PR;
		 memcpy (ek_pr, &ek_pr_null, sizeof (EK_PR));
         CString Text;
		 Text = m_List.GetItemText (i, m_List.PosA);
		 ek_pr->a = CStrFuncs::StrToDouble (Text);
		 Text = m_List.GetItemText (i, m_List.PosMdn);
		 ek_pr->mdn = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosFil);
		 ek_pr->fil = atoi (Text);
     
     	 CString PrEk =  m_List.GetItemText (i, m_List.Pos_PrEk);
		 ek_pr->pr_ek = CStrFuncs::StrToDouble (PrEk);
     	 CString HkKost =  m_List.GetItemText (i, m_List.Pos_BearbKost);
		 ek_pr->hk_kost = CStrFuncs::StrToDouble (HkKost);
		 //ZuTun
//		 CString PrVk =  m_List.GetItemText (i, m_List.PosPrVk);
//		 ek_pr->pr_vk_euro = CStrFuncs::StrToDouble (PrVk);
//		 ek_pr->pr_vk = ek_pr->pr_vk_euro;

		 CEkPreise *pr = new CEkPreise (PrEk, *ek_pr);
		 if (ek_pr->pr_ek != 0.0)
		 {
				ListRows.Add (pr);
		 }
		 delete ek_pr;
	}

//	DeleteDbRows ();

	ListRows.FirstPosition ();
	CEkPreise *ek_pr;
	while ((ek_pr = (CEkPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&Ek_pr.ek_pr, &ek_pr->ek_pr, sizeof (EK_PR));
		CString Date;
		CStrFuncs::SysDate (Date);
		Ek_pr.ToDbDate (Date, &Ek_pr.ek_pr.bearb);
//040510 		Ek_pr.ek_pr.akt = -1;
		Ek_pr.dbreadfirst ();
		Ek_pr.ek_pr.pr_ek = ek_pr->ek_pr.pr_ek;
		Ek_pr.ek_pr.hk_kost = ek_pr->ek_pr.hk_kost;
//		..
//		.. ZuTun


//		Ek_pr.ek_pr.pr_vk_euro = ek_pr->ek_pr.pr_vk_euro;
//		Ek_pr.ek_pr.pr_vk = ek_pr->ek_pr.pr_vk;
		CEkPreise old_ek_pr;
		if (IsChanged (ek_pr, &old_ek_pr))
		{
			return TRUE;
		}
	}
	return FALSE;
}


BOOL CListPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (!m_List.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Alle Eintr�ge l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = cfg_sql_mode;
	Ek_pr.beginwork ();
	m_List.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	m_List.DeleteAllItems ();

	Ek_pr.commitwork ();
	sql_mode = sql_s;
	StopEnter ();
	return TRUE;
}

BOOL CListPage::IsChanged (CEkPreise *pEk_pr, CEkPreise *old_ek_pr)
{

	CStrFuncs Str;
	EK_PR Ek_prWrite;
	memcpy (&Ek_prWrite, &pEk_pr->ek_pr, sizeof (EK_PR));
  if (Str.IsDoubleEqual (Ek_prWrite.hk, 0.0) &&
		Str.IsDoubleEqual (Ek_prWrite.pr_ek, 0.0) &&
		Str.IsDoubleEqual (Ek_prWrite.hk, 0.0) &&
		Str.IsDoubleEqual (Ek_prWrite.sk, 0.0) &&
		Str.IsDoubleEqual (Ek_prWrite.fil_ek, 0.0) &&
		Str.IsDoubleEqual (Ek_prWrite.fil_vk, 0.0) &&
		Str.IsDoubleEqual (Ek_prWrite.hk_kost, 0.0) &&
		Str.IsDoubleEqual (Ek_prWrite.gh1, 0.0) &&
		Str.IsDoubleEqual (Ek_prWrite.gh2, 0.0) &&
		Str.IsDoubleEqual (Ek_prWrite.sk_aufs, 0.0) 
		  )  return FALSE;

    if (Ek_pr.dbreadfirst () == 100) 
	{
		if (!strcmp (m_List.KalkulationsModus, "a_mo_we") == 0)
		{
			if (Str.IsDoubleEqual (Ek_pr.ek_pr.pr_ek, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.pr_ek, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.hk, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.sk, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.fil_ek, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.fil_vk, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.hk_kost, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.sk_aufs, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.gh1, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.gh2, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.sk, 0.0)  )  return FALSE;
		}
		else
		{
		  if (Str.IsDoubleEqual (Ek_pr.ek_pr.hk, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.pr_ek, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.hk_kost, 0.0) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.sk_aufs, 0.0) 
				  )  return FALSE;
		}
		return TRUE;
	}

		if (!strcmp (m_List.KalkulationsModus, "a_mo_we") == 0)
		{
		   if (Str.IsDoubleEqual (Ek_pr.ek_pr.a, Ek_prWrite.a) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.pr_ek, Ek_prWrite.pr_ek) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.hk, Ek_prWrite.hk) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.sk, Ek_prWrite.sk) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.fil_ek, Ek_prWrite.fil_ek) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.fil_vk, Ek_prWrite.fil_vk) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.hk_kost, Ek_prWrite.hk_kost) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.gh1, Ek_prWrite.gh1) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.gh2, Ek_prWrite.gh2) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.sk_aufs, Ek_prWrite.sk_aufs) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.sk, Ek_prWrite.sk)  )  return FALSE;
		}
		else
		{
		   if (Str.IsDoubleEqual (Ek_pr.ek_pr.a, Ek_prWrite.a) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.pr_ek, Ek_prWrite.pr_ek) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.hk, Ek_prWrite.hk) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.hk_kost, Ek_prWrite.hk_kost) &&
				Str.IsDoubleEqual (Ek_pr.ek_pr.sk_aufs, Ek_prWrite.sk_aufs) 
				  )  return FALSE;
		}

	memcpy (&old_ek_pr->ek_pr,  &Ek_pr.ek_pr, sizeof (EK_PR));
//	memcpy (&Ek_pr.ek_pr, &pEk_pr->ek_pr, sizeof (EK_PR));
  return TRUE;

	/****190210  muss anders laufen, da schon durch das Einlesen �nderungen vorgenommen werden 
		(anderer EK durch einlesen aus a_mo_we ...) 
	CStrFuncs Str;
	DbRows.FirstPosition ();
	CEkPreise *ek_pr;
	while ((ek_pr = (CEkPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ek_pr.ek_pr, &pEk_pr->ek_pr, sizeof (EK_PR));
//				if (Ek_pr.ek_pr.hk > 0.0 //Kurznummer
//					|| Ek_pr.ek_pr.hk_kost > 0.0 //B1 Aufschlag
//					|| Ek_pr.ek_pr.sk_aufs > 0.0) //GH Aufschlag
		
		if (Str.IsDoubleEqual (Ek_pr.ek_pr.a, ek_pr->ek_pr.a) &&
			Str.IsDoubleEqual (Ek_pr.ek_pr.hk, ek_pr->ek_pr.hk) &&
			Str.IsDoubleEqual (Ek_pr.ek_pr.hk_kost, ek_pr->ek_pr.hk_kost) &&
			Str.IsDoubleEqual (Ek_pr.ek_pr.sk_aufs, ek_pr->ek_pr.sk_aufs) &&
			Str.IsDoubleEqual (Ek_pr.ek_pr.sk_aufs, ek_pr->ek_pr.sk_aufs)  )  break;
	} 	
	if (ek_pr == NULL)
	{
		old_ek_pr->ek_pr.pr_ek = 0.0;
		return TRUE;
	}
	memcpy (&old_ek_pr->ek_pr,  &ek_pr->ek_pr, sizeof (EK_PR));
//	if (pEk_pr->cEk != ek_pr->cEk) return TRUE;
//	if (pEk_pr->cVk != ek_pr->cVk) return TRUE;
	return FALSE;
*****************/
}

BOOL CListPage::InList (EK_PR_CLASS& Ek_pr)
{
	ListRows.FirstPosition ();
	CEkPreise *ek_pr;
	while ((ek_pr = (CEkPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Ek_pr == ek_pr->ek_pr) return TRUE;
	}
    return FALSE;
}

BOOL CListPage::TestDecValues (EK_PR *ek_pr)
{
	if (ek_pr->pr_ek > 9999.9999 ||
		ek_pr->pr_ek < -9999.9999)
	{
		return FALSE;
	}
// .. ZuTun
// ..
	return TRUE;
}

void CListPage::SetMdn (short mdn)
{
	m_List.m_Mdn = mdn;
	Mdn.mdn.mdn = mdn;
	MdnUser.mdn.mdn = mdn;
//	if (m_List.m_Mdn != 0)
//	{
		m_List.SetColumnWidth (m_List.PosMdnGr, 0);
		m_List.SetColumnWidth (m_List.PosMdn, 0);
		m_List.SetColumnWidth (m_List.PosFilGr, 0);
		m_List.SetColumnWidth (m_List.PosFil, 0);
//	}
//	else
//	{
//		m_List.SetColumnWidth (m_List.PosMdnGr, 60);
//		m_List.SetColumnWidth (m_List.PosMdn, 120);
//		m_List.SetColumnWidth (m_List.PosFilGr, 60);
//		m_List.SetColumnWidth (m_List.PosFil, 120);
//	}
}


void CListPage::Schreiben (void)
{
	Write();

}
void CListPage::AktSetzen (void)
{
//	Write();
	CString Sql;
	CString Text;
	double a = 0.0;
	int UpdCursor;
   Ek_pr.sqlcomm (_T("update ek_pr set akt = -1 where akt = 0"));

    Ek_pr.sqlin ((double *) &a, SQLDOUBLE, 0);  
    Ek_pr.sqlin ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);  
	Sql = _T("update ek_pr set akt = 0 where a = ? and mdn = ? and fil = 0 ");
	UpdCursor = Ek_pr.sqlcursor (Sql.GetBuffer ());

	m_List.StopEnter ();
	int count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 Text = m_List.GetItemText (i, m_List.PosA);
		 a = CStrFuncs::StrToDouble (Text);
		Ek_pr.sqlexecute (UpdCursor);
	}
	Ek_pr.sqlclose (UpdCursor);
}
void CListPage::DruckSetzen (void)
{
//	Write();
	CString Sql;
	CString Text;
	double a = 0.0;
	int UpdCursor;
   Ek_pr.sqlcomm (_T("update ek_pr set druck = 0 where druck = 1"));

    Ek_pr.sqlin ((double *) &a, SQLDOUBLE, 0);  
    Ek_pr.sqlin ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);  
	Sql = _T("update ek_pr set druck = 1 where a = ? and mdn = ? and fil = 0 ");
	UpdCursor = Ek_pr.sqlcursor (Sql.GetBuffer ());

	m_List.StopEnter ();
	int count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 Text = m_List.GetItemText (i, m_List.PosA);
		 a = CStrFuncs::StrToDouble (Text);
		Ek_pr.sqlexecute (UpdCursor);
	}
	Ek_pr.sqlclose (UpdCursor);
}

void CListPage::SetPrVkVisible (BOOL visible)
{
	m_List.SetColumnWidth (m_List.PosA, m_List.ALen);
	m_List.SetColumnWidth (m_List.Pos_PrEk, m_List.EkLen);
	m_List.SetColumnWidth (m_List.Pos_BearbKost, m_List.HkKostLen);
	m_List.SetColumnWidth (m_List.Pos_Kalk_Hk, m_List.HkLen);

	m_List.SetColumnWidth (m_List.Pos_Aufs_Sk, m_List.SkAufsLen);
	m_List.SetColumnWidth (m_List.Pos_Kalk_Sk, m_List.SkLen);
	m_List.SetColumnWidth (m_List.Pos_Aufs_Fil_Ek, m_List.FilEkAufsLen);
	m_List.SetColumnWidth (m_List.Pos_Kalk_Fil_Ek, m_List.FilEkKalkLen);
	m_List.SetColumnWidth (m_List.Pos_Fil_Ek, m_List.FilEkLen);
	m_List.SetColumnWidth (m_List.Pos_Aufs_Fil_Vk, m_List.FilVkAufsLen);
	m_List.SetColumnWidth (m_List.Pos_Kalk_Fil_Vk, m_List.FilVkKalkLen);
	m_List.SetColumnWidth (m_List.Pos_Fil_Vk, m_List.FilVkLen);
	m_List.SetColumnWidth (m_List.Pos_Aufs_Gh1, m_List.Gh1AufsLen);
	m_List.SetColumnWidth (m_List.Pos_KalkVk_Gh1, m_List.Gh1KalkVkLen);
	m_List.SetColumnWidth (m_List.Pos_Gh1, m_List.Gh1Len);
	m_List.SetColumnWidth (m_List.Pos_Sp_Gh1, m_List.Gh1SpLen);
	m_List.SetColumnWidth (m_List.Pos_Aufs_Gh2, m_List.Gh2AufsLen);
	m_List.SetColumnWidth (m_List.Pos_KalkVk_Gh2, m_List.Gh2KalkVkLen);
	m_List.SetColumnWidth (m_List.Pos_Gh2, m_List.Gh2Len);
	m_List.SetColumnWidth (m_List.Pos_Sp_Gh2, m_List.Gh2SpLen);
	m_List.SetColumnWidth (m_List.PosA2, m_List.ALen2);


	if (visible) //LuD Neu L�ngen setzen
	{
/*
		m_List.SetColumnWidth (m_List.PosPrVk, 100);
		m_List.SetColumnWidth (m_List.PosPrEkKalk, 100);
		m_List.SetColumnWidth (m_List.PosPrVkKalk, 100);
		m_List.SetColumnWidth (m_List.PosSk, 100);
		m_List.SetColumnWidth (m_List.PosSp, 100);
		***/
	}
	else
	{
		m_List.SetColumnWidth (m_List.Pos_BearbKost, 0);
		m_List.SetColumnWidth (m_List.Pos_Kalk_Hk, 0);
		m_List.SetColumnWidth (m_List.Pos_Aufs_Sk, 0);
		m_List.SetColumnWidth (m_List.Pos_Kalk_Sk, 0);
		m_List.SetColumnWidth (m_List.Pos_Aufs_Fil_Ek, 0);
		m_List.SetColumnWidth (m_List.Pos_Kalk_Fil_Ek, 0);
		m_List.SetColumnWidth (m_List.Pos_Fil_Ek, 0);
		m_List.SetColumnWidth (m_List.Pos_Aufs_Fil_Vk, 0);
		m_List.SetColumnWidth (m_List.Pos_Kalk_Fil_Vk, 0);
		m_List.SetColumnWidth (m_List.Pos_Fil_Vk, 0);
		m_List.SetColumnWidth (m_List.Pos_Aufs_Gh1, 0);
		m_List.SetColumnWidth (m_List.Pos_KalkVk_Gh1, 0);
		m_List.SetColumnWidth (m_List.Pos_Gh1, 0);
		m_List.SetColumnWidth (m_List.Pos_Sp_Gh1, 0);
		m_List.SetColumnWidth (m_List.Pos_Aufs_Gh2, 0);
		m_List.SetColumnWidth (m_List.Pos_KalkVk_Gh2, 0);
		m_List.SetColumnWidth (m_List.Pos_Gh2, 0);
		m_List.SetColumnWidth (m_List.Pos_Sp_Gh2, 0);
		m_List.SetColumnWidth (m_List.PosA2, 0);




//		m_List.SetColumnWidth (m_List.PosPrVk, 0);
//		m_List.SetColumnWidth (m_List.PosPrEkKalk, 0);
//		m_List.SetColumnWidth (m_List.PosPrVkKalk, 0);
//		m_List.SetColumnWidth (m_List.PosSk, 0);
//		m_List.SetColumnWidth (m_List.PosSp, 0);
	}
}

void CListPage::FillMdnGrCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0"));
	Values.Add (Value);
    Mdn.sqlopen (MdnGrCursor);
	while (Mdn.sqlfetch (MdnGrCursor) == 0)
	{
		Gr_zuord.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr,
			                          Gr_zuord.gr_zuord.gr_bz1);
		Values.Add (Value);
	}
	m_List.FillMdnGrCombo (Values);
}


void CListPage::FillMdnCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Zentrale"));
	Values.Add (Value);
    Mdn.sqlopen (MdnCursor);
	while (Mdn.sqlfetch (MdnCursor) == 0)
	{
		Mdn.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Mdn.mdn.mdn,
			                          MdnAdr.adr.adr_krz);
		Values.Add (Value);
	}
	m_List.FillMdnCombo (Values);
}


void CListPage::FillFilCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Mandant"));
	Values.Add (Value);
    Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		Fil.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Fil.fil.fil,
			                          FilAdr.adr.adr_krz);
		Values.Add (Value);
	}
	m_List.FillFilCombo (Values);
}

void CListPage::RowChanged (int NewRow)
{
}

void CListPage::ColChanged (int Row, int Col)
{
	CString cWert = _T("");
	if (Col == m_List.Pos_PrEk && strcmp (m_List.KalkulationsModus, "a_mo_we") == 0)
	{
		if (m_List.RowDeleted == TRUE)
		{
			m_List.ListRows.Drop(Row);
			m_List.RowDeleted = FALSE;
		}
		CEkPreise *ek_pr = (CEkPreise *) m_List.ListRows.Get(Row);
		if (ek_pr != NULL)
		{
			memcpy (&Ek_pr.ek_pr, &ek_pr->ek_pr, sizeof (EK_PR));
		}


		if (m_List.NoColChanged == FALSE) 
		{
			cWert =  m_List.GetItemText (Row, Col);

			if (strcmp (m_List.SchreibModus, "GH") == 0 )
			{
				 Ek_pr.ek_pr.gh1 = CStrFuncs::StrToDouble (cWert);
				 if (ek_pr != NULL)
				{
					ek_pr->ek_pr.gh1 = Ek_pr.ek_pr.gh1;
					m_List.ListRows.Replace(Row,ek_pr);
				}
			}
			else
			{
				 Ek_pr.ek_pr.pr_ek = CStrFuncs::StrToDouble (cWert);
				 if (ek_pr != NULL)
				{
					ek_pr->ek_pr.pr_ek = Ek_pr.ek_pr.pr_ek;
					m_List.ListRows.Replace(Row,ek_pr);
				}
			}
		}
		else
		{   // alter Wert zur�cksetzen
			m_List.DoubleToString (Ek_pr.ek_pr.pr_ek, cWert,m_List.Nachkomma(Col));
			FillList.SetItemText (cWert.GetBuffer (), Row, Col);
		}
	    HoleSpaltenwert(Row,m_List.Pos_Kalk_Sk,m_List.KalkulationsModus);
	    HoleSpaltenwert(Row,m_List.Pos_Fil_Ek,m_List.KalkulationsModus);
	}

	if (Col == m_List.Pos_Gh1 && strcmp (m_List.KalkulationsModus, "a_mo_we") == 0) // Ek bei GH
	{
		if (m_List.RowDeleted == TRUE)
		{
			m_List.ListRows.Drop(Row);
			m_List.RowDeleted = FALSE;
		}
		CEkPreise *ek_pr = (CEkPreise *) m_List.ListRows.Get(Row);
		if (ek_pr != NULL)
		{
			memcpy (&Ek_pr.ek_pr, &ek_pr->ek_pr, sizeof (EK_PR));
		}


		if (m_List.NoColChanged == FALSE) 
		{
			cWert =  m_List.GetItemText (Row, Col);
			 Ek_pr.ek_pr.gh1 = CStrFuncs::StrToDouble (cWert);
			if (ek_pr != NULL)
			{
//				memcpy (&ek_pr->ek_pr, &Ek_pr.ek_pr, sizeof (EK_PR));
				ek_pr->ek_pr.gh1 = Ek_pr.ek_pr.gh1;
				m_List.ListRows.Replace(Row,ek_pr);
			}
		}
		else
		{   // alter Wert zur�cksetzen
			m_List.DoubleToString (Ek_pr.ek_pr.gh1, cWert,m_List.Nachkomma(Col));
			FillList.SetItemText (cWert.GetBuffer (), Row, Col);
		}
	    HoleSpaltenwert(Row,m_List.Pos_Fil_Ek,m_List.KalkulationsModus);
	}







	if (Col == m_List.Pos_BearbKost)
	{
		if (m_List.RowDeleted == TRUE)
		{
			m_List.ListRows.Drop(Row);
			m_List.RowDeleted = FALSE;
		}
		CEkPreise *ek_pr = (CEkPreise *) m_List.ListRows.Get(Row);

		if (ek_pr != NULL)
		{
			memcpy (&Ek_pr.ek_pr, &ek_pr->ek_pr, sizeof (EK_PR));
		}

		if (m_List.NoColChanged == FALSE) 
		{
			cWert =  m_List.GetItemText (Row, Col);
			Ek_pr.ek_pr.hk_kost = CStrFuncs::StrToDouble (cWert);
			if (ek_pr != NULL)
			{
//				memcpy (&ek_pr->ek_pr, &Ek_pr.ek_pr, sizeof (EK_PR));
				ek_pr->ek_pr.hk_kost = Ek_pr.ek_pr.hk_kost;
				m_List.ListRows.Replace(Row,ek_pr);
			}
		}
		else
		{   // alter Wert zur�cksetzen
			m_List.DoubleToString (Ek_pr.ek_pr.hk_kost, cWert,m_List.Nachkomma(Col));
			FillList.SetItemText (cWert.GetBuffer (), Row, Col);
		}
	    HoleSpaltenwert(Row,m_List.Pos_Kalk_Sk,m_List.KalkulationsModus);
	}
	if (Col == m_List.Pos_Aufs_Sk)
	{
		if (m_List.RowDeleted == TRUE)
		{
			m_List.ListRows.Drop(Row);
			m_List.RowDeleted = FALSE;
		}
		CEkPreise *ek_pr = (CEkPreise *) m_List.ListRows.Get(Row);
		if (ek_pr != NULL)
		{
			memcpy (&Ek_pr.ek_pr, &ek_pr->ek_pr, sizeof (EK_PR));
		}

		if (m_List.NoColChanged == FALSE) 
		{
			cWert =  m_List.GetItemText (Row, Col);
			 Ek_pr.ek_pr.sk_aufs = CStrFuncs::StrToDouble (cWert);
			if (ek_pr != NULL)
			{
//				memcpy (&ek_pr->ek_pr, &Ek_pr.ek_pr, sizeof (EK_PR));
				ek_pr->ek_pr.sk_aufs = Ek_pr.ek_pr.sk_aufs;
				m_List.ListRows.Replace(Row,ek_pr);
			}
		}
		else
		{   // alter Wert zur�cksetzen
			m_List.DoubleToString (Ek_pr.ek_pr.sk_aufs, cWert,m_List.Nachkomma(Col));
			FillList.SetItemText (cWert.GetBuffer (), Row, Col);
		}
	    HoleSpaltenwert(Row,m_List.Pos_Fil_Ek,m_List.KalkulationsModus);
	}
	if (Col == m_List.Pos_Kalk_Hk && strcmp (m_List.KalkulationsModus, "a_mo_we") == 0  )
	{
		if (m_List.RowDeleted == TRUE)
		{
			m_List.ListRows.Drop(Row);
			m_List.RowDeleted = FALSE;
		}
		CEkPreise *ek_pr = (CEkPreise *) m_List.ListRows.Get(Row);
		if (ek_pr != NULL)
		{
			memcpy (&Ek_pr.ek_pr, &ek_pr->ek_pr, sizeof (EK_PR));
		}


		if (m_List.NoColChanged == FALSE) 
		{
			cWert =  m_List.GetItemText (Row, Col);
			 Ek_pr.ek_pr.hk = CStrFuncs::StrToDouble (cWert);
			if (ek_pr != NULL)
			{
//				memcpy (&ek_pr->ek_pr, &Ek_pr.ek_pr, sizeof (EK_PR));
				ek_pr->ek_pr.hk = Ek_pr.ek_pr.hk;
				m_List.ListRows.Replace(Row,ek_pr);
			}
		}
		else
		{   // alter Wert zur�cksetzen
			m_List.DoubleToString (Ek_pr.ek_pr.hk, cWert,m_List.Nachkomma(Col));
			FillList.SetItemText (cWert.GetBuffer (), Row, Col);
		}
	    HoleSpaltenwert(Row,m_List.Pos_Gh2,m_List.KalkulationsModus);
	}
}

void CListPage::HoleSpaltenwerte (int Row, char *Modus)
{
		HoleSpaltenwert(Row,m_List.PosA,Modus);
		HoleSpaltenwert(Row,m_List.Pos_PrEk,Modus);
		HoleSpaltenwert(Row,m_List.Pos_BearbKost,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Kalk_Hk,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Aufs_Sk,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Kalk_Sk,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Aufs_Fil_Ek,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Kalk_Fil_Ek,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Fil_Ek,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Aufs_Fil_Vk,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Kalk_Fil_Vk,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Fil_Vk,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Aufs_Gh1,Modus);
		HoleSpaltenwert(Row,m_List.Pos_KalkVk_Gh1,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Gh1,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Sp_Gh1,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Aufs_Gh2,Modus);
		HoleSpaltenwert(Row,m_List.Pos_KalkVk_Gh2,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Gh2,Modus);
		HoleSpaltenwert(Row,m_List.Pos_Sp_Gh2,Modus);
		HoleSpaltenwert(Row,m_List.PosA2,Modus);
}

void CListPage::HoleSpaltenwert (int Row, int Col,char *Modus)
{
//Hier: Spalten mit Daten f�llen (HoleSpaltenwert ())
	CString CWert;
	if (m_List.GetColumnWidth (Col) == 0) return;

	if (Col == m_List.PosA)
	{
		A_bas.a_bas.a = Ek_pr.ek_pr.a;
		A_bas.dbreadfirst ();

		CWert.Format (_T("%-10.0lf%s"), A_bas.a_bas.a, A_bas.a_bas.a_bz1);
		if (Calculate.isEigenprodukt(A_bas.a_bas.a))
		{
			Prdk_k.prdk_k.mdn = Ek_pr.ek_pr.mdn;
			Prdk_k.prdk_k.a = Ek_pr.ek_pr.a;
			if (Prdk_k.dbreadfirst () != 0) //Suche nach aktiver Rezeptur
			{
				 memcpy (&Prdk_k.prdk_k, &prdk_k_null, sizeof (PRDK_K));
			}
		}
	}

	if (Col == m_List.PosA2)
	{
		CString cA2;
		CWert.Format (_T("%s"), A_bas.a_bas.a_bz1);
	}


	//========================================================================
	//========= Modus  "a_kalkpreis"   ======================================
	//========================================================================

		if (Col == m_List.Pos_PrEk && !strcmp (Modus, "a_mo_we") == 0)
		{
			if (strcmp (Modus, "fil_ek") == 0)
			{
				Ek_pr.ek_pr.pr_ek = Calculate.GetFilEk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			}
			else
			{
				Ek_pr.ek_pr.pr_ek = Calculate.GetMatoB (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			}
			m_List.DoubleToString (Ek_pr.ek_pr.pr_ek, CWert, m_List.Nachkomma(Col));
			if (Calculate.isaktiveRezeptur() && strcmp(m_List.Lst->lst.EkEdit,"J") == 0)  
			{ 
				if (m_List.EkPruef == 1) //cfg-Einstellung : Rezepturen sollen gepr�ft werden
				{
					CWert = "_" + CWert; //testtest
				}
			}
		}


		if (Col == m_List.Pos_BearbKost && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.HkKost = Calculate.GetBearbKost (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			m_List.DoubleToString (m_List.HkKost, CWert,m_List.Nachkomma(Col));
			if (Calculate.isaktiveRezeptur() && strcmp(m_List.Lst->lst.HkKostEdit,"J") == 0)   
				if (m_List.EkPruef == 1) //cfg-Einstellung : Rezepturen sollen gepr�ft werden
				{
					CWert = "_" + CWert; //testtest
				}
		}

		if (Col == m_List.Pos_Kalk_Hk && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Hk = Calculate.GetHk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			m_List.DoubleToString (m_List.Hk, CWert,m_List.Nachkomma(Col));
			if (Calculate.isaktiveRezeptur() && strcmp(m_List.Lst->lst.HkEdit,"J") == 0)   
				if (m_List.EkPruef == 1) //cfg-Einstellung : Rezepturen sollen gepr�ft werden
				{
					CWert = "_" + CWert; //testtest
				}
		}
		if (Col == m_List.Pos_Aufs_Sk && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.SkAufs = Calculate.GetSkAufs (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			m_List.DoubleToString (m_List.SkAufs, CWert,m_List.Nachkomma(Col));
			CWert += Calculate.GetSkText();
		}
		if (Col == m_List.Pos_Kalk_Sk && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Sk = Calculate.GetSk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			m_List.DoubleToString (m_List.Sk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Aufs_Fil_Ek && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilEkAufs = Calculate.GetFilEkAufs (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			m_List.DoubleToString (m_List.FilEkAufs, CWert,m_List.Nachkomma(Col));
			CWert += Calculate.GetFilEkText();
		}
		if (Col == m_List.Pos_Kalk_Fil_Ek && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilEkKalk = Calculate.GetFilEkKalk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			m_List.DoubleToString (m_List.FilEkKalk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Fil_Ek && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilEk = Calculate.GetFilEk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			m_List.DoubleToString (m_List.FilEk, CWert,m_List.Nachkomma(Col));
		}

		if (Col == m_List.Pos_Aufs_Fil_Vk && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilVkAufs = Calculate.GetFilVkAufs (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			m_List.DoubleToString (m_List.FilVkAufs, CWert,m_List.Nachkomma(Col));
			CWert += Calculate.GetFilVkText();
		}
		if (Col == m_List.Pos_Kalk_Fil_Vk && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilVkKalk = Calculate.GetFilVkKalk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			m_List.DoubleToString (m_List.FilVkKalk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Fil_Vk && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilVk = Calculate.GetFilVk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.fil, Ek_pr.ek_pr.a);
			m_List.DoubleToString (m_List.FilVk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Aufs_Gh1 && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh1Aufs = Calculate.GetAddEkProz (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.a,m_List.Gh1PrGrStuf);
			m_List.DoubleToString (m_List.Gh1Aufs, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_KalkVk_Gh1 && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh1KalkVk = Calculate.GetKalkVk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.a,m_List.Gh1PrGrStuf,m_List.Sk);
			m_List.DoubleToString (m_List.Gh1KalkVk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Gh1 && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh1 = Calculate.GetVk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.a,m_List.Gh1PrGrStuf  );
			m_List.DoubleToString (m_List.Gh1, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Sp_Gh1 && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh1Sp = Calculate.GetVkSpanne (Ek_pr.ek_pr.mdn,  Ek_pr.ek_pr.a,m_List.Gh1PrGrStuf,m_List.Sk );
			m_List.DoubleToString (m_List.Gh1Sp, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Aufs_Gh2 && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh2Aufs = Calculate.GetAddEkProz (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.a,m_List.Gh2PrGrStuf);
			m_List.DoubleToString (m_List.Gh2Aufs, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_KalkVk_Gh2 && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh2KalkVk = Calculate.GetKalkVk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.a,m_List.Gh2PrGrStuf,m_List.Sk);
			m_List.DoubleToString (m_List.Gh2KalkVk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Gh2 && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh2 = Calculate.GetVk (Ek_pr.ek_pr.mdn, Ek_pr.ek_pr.a,m_List.Gh2PrGrStuf  );
			m_List.DoubleToString (m_List.Gh2, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Sp_Gh2 && !strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh2Sp = Calculate.GetVkSpanne (Ek_pr.ek_pr.mdn,  Ek_pr.ek_pr.a,m_List.Gh2PrGrStuf,m_List.Sk );
			m_List.DoubleToString (m_List.Gh2Sp, CWert,m_List.Nachkomma(Col));
		}




	//========================================================================
	//========= Modus     "a_mo_we"     ======================================
	//========================================================================

		if (Col == m_List.Pos_PrEk && strcmp (Modus, "a_mo_we") == 0)
		{
			if (pr_ek_a_mo_we == 0.0)
			{
				Ek_pr.ek_pr.pr_ek   = pr_ek_a_kalk;
			}
			else
			{
				Ek_pr.ek_pr.pr_ek   = pr_ek_a_mo_we;
			}
			if (strcmp (m_List.SchreibModus, "GH") == 0 )
			{
				if (Ek_pr.ek_pr.gh1 > 0.0)
				{
					Ek_pr.ek_pr.pr_ek   = Ek_pr.ek_pr.gh1;
				}
			}
			m_List.DoubleToString (Ek_pr.ek_pr.pr_ek, CWert, m_List.Nachkomma(Col));
			if (Calculate.isaktiveRezeptur() && strcmp(m_List.Lst->lst.EkEdit,"J") == 0)  
			{ 
				if (m_List.EkPruef == 1) //cfg-Einstellung : Rezepturen sollen gepr�ft werden
				{
//zuTun					CWert = "_" + CWert; //testtest
				}
			}
			else if (pr_ek_a_mo_we > 0.0001 )
			{ 
//zuTun				CWert = "_" + CWert; //testtest
			}
		}


		if (Col == m_List.Pos_BearbKost && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.HkKost = Ek_pr.ek_pr.hk_kost;
			m_List.DoubleToString (m_List.HkKost, CWert,m_List.Nachkomma(Col));
			if (Calculate.isaktiveRezeptur() && strcmp(m_List.Lst->lst.HkKostEdit,"J") == 0)   
				if (m_List.EkPruef == 1) //cfg-Einstellung : Rezepturen sollen gepr�ft werden
				{
					CWert = "_" + CWert; //testtest
				}
		}

		if (Col == m_List.Pos_Kalk_Hk && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Hk = Ek_pr.ek_pr.hk;
			m_List.DoubleToString (m_List.Hk, CWert,m_List.Nachkomma(Col));
			if (Calculate.isaktiveRezeptur() && strcmp(m_List.Lst->lst.HkEdit,"J") == 0)   
				if (m_List.EkPruef == 1) //cfg-Einstellung : Rezepturen sollen gepr�ft werden
				{
					CWert = "_" + CWert; //testtest
				}
		}
		if (Col == m_List.Pos_Aufs_Sk && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.SkAufs = Ek_pr.ek_pr.sk_aufs;
			m_List.DoubleToString (m_List.SkAufs, CWert,m_List.Nachkomma(Col));
			CWert += Calculate.GetSkText();
		}
		if (Col == m_List.Pos_Kalk_Sk && strcmp (Modus, "a_mo_we") == 0)
		{
			(Ek_pr.ek_pr.hk_kost == 0.0) ? m_List.Sk = 0.0 : m_List.Sk = Ek_pr.ek_pr.pr_ek + Ek_pr.ek_pr.hk_kost; 
			m_List.DoubleToString (m_List.Sk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Aufs_Fil_Ek && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilEkAufs = Ek_pr.ek_pr.fil_ek_aufs;
			m_List.DoubleToString (m_List.FilEkAufs, CWert,m_List.Nachkomma(Col));
			CWert += Calculate.GetFilEkText();
		}
		if (Col == m_List.Pos_Kalk_Fil_Ek && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilEkKalk = Ek_pr.ek_pr.fil_ek_kalk;
			m_List.DoubleToString (m_List.FilEkKalk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Fil_Ek && strcmp (Modus, "a_mo_we") == 0)
		{
			(Ek_pr.ek_pr.sk_aufs == 0.0) ? m_List.FilEk = 0.0 : m_List.FilEk = Ek_pr.ek_pr.gh1 + Ek_pr.ek_pr.sk_aufs; 
			m_List.DoubleToString (m_List.FilEk, CWert,m_List.Nachkomma(Col));
		}

		if (Col == m_List.Pos_Aufs_Fil_Vk && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilVkAufs = Ek_pr.ek_pr.fil_vk_aufs;
			m_List.DoubleToString (m_List.FilVkAufs, CWert,m_List.Nachkomma(Col));
			CWert += Calculate.GetFilVkText();
		}
		if (Col == m_List.Pos_Kalk_Fil_Vk && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilVkKalk = Ek_pr.ek_pr.fil_vk_kalk;
			m_List.DoubleToString (m_List.FilVkKalk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Fil_Vk && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.FilVk = Ek_pr.ek_pr.fil_vk;
			m_List.DoubleToString (m_List.FilVk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Aufs_Gh1 && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh1Aufs = Ek_pr.ek_pr.gh1_aufs;
			m_List.DoubleToString (m_List.Gh1Aufs, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_KalkVk_Gh1 && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh1KalkVk = Ek_pr.ek_pr.gh1_kalk_vk;
			m_List.DoubleToString (m_List.Gh1KalkVk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Gh1 && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh1 = Ek_pr.ek_pr.gh1;
			m_List.DoubleToString (m_List.Gh1, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Sp_Gh1 && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh1Sp = Ek_pr.ek_pr.gh1_sp;
			m_List.DoubleToString (m_List.Gh1Sp, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Aufs_Gh2 && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh2Aufs = Ek_pr.ek_pr.gh2_aufs;
			m_List.DoubleToString (m_List.Gh2Aufs, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_KalkVk_Gh2 && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh2KalkVk = Ek_pr.ek_pr.gh2_kalk_vk;
			m_List.DoubleToString (m_List.Gh2KalkVk, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Gh2 && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh2 = Ek_pr.ek_pr.hk + ART_OFF_MUEFLI;  //Artikeloffset bei M�fli
			if (Ek_pr.ek_pr.hk == (double) 0) m_List.Gh2 = (double) 0;
			m_List.DoubleToString (m_List.Gh2, CWert,m_List.Nachkomma(Col));
		}
		if (Col == m_List.Pos_Sp_Gh2 && strcmp (Modus, "a_mo_we") == 0)
		{
			m_List.Gh2Sp = Ek_pr.ek_pr.gh2_sp;
			m_List.DoubleToString (m_List.Gh2Sp, CWert,m_List.Nachkomma(Col));
		}

	FillList.SetItemText (CWert.GetBuffer (), Row, Col);
}




