//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AprMan.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_AprManTYPE                  129
#define IDB_UNCHECK                     130
#define IDS_STRING130                   130
#define IDB_BITMAP2                     131
#define IDB_CHECK                       131
#define IDD_COMPANY_PR_PAGE             132
#define IDB_OPEN                        133
#define IDB_BITMAP1                     134
#define IDB_DELCHECK                    134
#define ID_NEW_PR                       135
#define IDR_NEWPR                       135
#define IDD_GROUNDPRICE                 135
#define IDR_ARTAKT                      136
#define IDB_PRINT                       136
#define IDB_PRINT_MASK                  137
#define IDB_OPEN_MASK                   138
#define ID_AKT_PR_GR                    139
#define IDD_DATDIAL                     139
#define IDI_HARROWUP                    140
#define IDI_HARROWNO                    141
#define IDI_HARROWDOWN                  142
#define IDD_GRUND_PREIS                 143
#define IDR_DLGMENU                     145
#define IDD_IPR1                        200
#define IDD_APR1                        200
#define IDC_EDIT2                       1001
#define IDC_MDN_ADR_KRZ                 1001
#define IDC_LMDN_GR                     1003
#define IDC_MDN_GR                      1004
#define IDC_CMP_GROUP                   1005
#define IDC_GR_GROUP                    1006
#define IDC_LTEIL_SMT                   1007
#define IDC_LA_TYP                      1008
#define IDC_LJAHR                       1009
#define IDC_LARTIKEL                    1010
#define IDC_SORTIMENT_GROUP             1011
#define IDC_LMonat                      1012
#define IDC_EK_EDIT                     1015
#define IDC_LPE                         1015
#define IDC_LMDN_USER                   1016
#define IDC_LEKD                        1016
#define IDC_MDN_USER                    1017
#define IDC_LEK                         1017
#define IDC_LPRICE1                     1018
#define IDC_LSK                         1018
#define IDC_Hk_Edit                     1018
#define IDC_LPRICE2                     1019
#define IDC_HK_KOST_EDIT                1019
#define IDC_LPRICE3                     1020
#define IDC_PE                          1020
#define IDC_SK_AUFS_EDIT                1020
#define IDC_GROUNDPRICE1                1021
#define IDC_EKD                         1021
#define IDC_SK_EDIT                     1021
#define IDC_GROUNDPRICE2                1022
#define IDC_EK                          1022
#define IDC_EK_EDIT2                    1022
#define IDC_ALLE_ARTIKEL                1022
#define IDC_GROUNDPRICE3                1023
#define IDC_SK                          1023
#define IDC_FIL_EK_AUFS_EDIT            1023
#define IDC_LPRICE_GROUP                1024
#define IDC_SP_VK                       1024
#define IDC_FIL_EK_KALK_EDIT            1024
#define IDC_GROUND_PRICE                1025
#define IDC_FIL_EK_EDIT                 1025
#define IDC_LLIST_TYP                   1026
#define IDC_COMBO1                      1027
#define IDC_LIST_TYP                    1027
#define IDC_EDIT1                       1028
#define IDC_GH1_AUFS_EDIT               1028
#define Aktivierungsdatum               1029
#define IDC_GH1_AUFS_EDIT2              1029
#define IDC_GH1_KALK_VK_EDIT            1032
#define IDC_GH1_EDIT                    1034
#define IDC_GH1_SP_EDIT                 1035
#define IDC_GH2_AUFS_EDIT               1036
#define IDC_GH2_KALK_VK_EDIT            1037
#define IDC_GH2_EDIT                    1038
#define IDC_GH2_SP_EDIT                 1039
#define IDC_FIL_VK_AUFS_EDIT            1040
#define IDC_FIL_VK_KALK_EDIT            1041
#define IDC_FIL_VK_EDIT                 1042
#define IDC_LA                          4000
#define IDC_A                           4001
#define IDC_A_BZ1                       4002
#define IDC_LA_BZ1                      4003
#define IDC_LIST                        4004
#define IDC_CANCEL                      4005
#define IDC_SAVE                        4006
#define IDC_DELETE                      4007
#define IDC_LMDN                        4008
#define IDC_MDN                         4009
#define IDC_MDN_NAME                    4010
#define IDC_LFIL_GR                     4010
#define IDC_LFIL                        4011
#define IDC_A_BZ2                       4012
#define IDC_FIL_GR                      4012
#define IDC_FIL                         4013
#define IDC_INSERT                      4014
#define IDC_LHWG                        4014
#define IDC_HWG                         4015
#define IDC_LWG                         4016
#define IDC_WG                          4017
#define IDC_LAG                         4018
#define IDC_AG                          4019
#define IDC_TEIL_SMT                    4020
#define IDC_A_TYP                       4021
#define IDC_ARTIKEL                     4022
#define IDC_TEIL_SMT2                   4023
#define IDC_JAHR                        4023
#define IDC_JAHR2                       4024
#define IDC_MONAT                       4024
#define IDD_PR_ART_PAGE                 5102
#define IDD_ART_PR_PAGE                 5102
#define IDD_CMP_LIST_PAGE               5103
#define IDD_LIST_PAGE                   5104
#define IDD_ART_PR_PAGE1                5105
#define IDD_ART_AKT_PAGE                5105
#define ID_BACK                         32771
#define ID_DELETE                       32772
#define ID_INSERT                       32773
#define ID_STD_PR                       32775
#define ID_DELETEALL                    32776
#define ID_PRINT_ALL                    32777
#define ID_ARTAKT                       32781
#define ID_ANSICHT_KURZELISTE           32783
#define IDD_STATIC1                     32783
#define ID_COMPACT_MODE                 32784
#define ID_FENSTER_UNTEREINANDER        32785
#define ID_GRUND_PREIS                  32786
#define ID_Menu                         32787

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
