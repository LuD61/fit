#include "StdAfx.h"
#include ".\dbpropertysheet.h"
#include "DbPropertyPage.h"
#include "resource.h"

IMPLEMENT_DYNAMIC(CDbPropertySheet, CPropertySheet)

CDbPropertySheet::CDbPropertySheet(void)
{
	Tabs = TRUE;
}

CDbPropertySheet::CDbPropertySheet(LPCTSTR Title) :
                  CPropertySheet (Title) 
{
	Tabs = TRUE;
}

CDbPropertySheet::~CDbPropertySheet(void)
{
}


BEGIN_MESSAGE_MAP(CDbPropertySheet, CPropertySheet)
//	ON_COMMAND(ID_BREAK, OnBreak)
END_MESSAGE_MAP()


BOOL CDbPropertySheet::OnInitDialog() 
{
	BOOL bResult = CPropertySheet::OnInitDialog();
	if (!Tabs)
	{
/*
		DWORD ids []= {IDOK, IDCANCEL, ID_APPLY_NOW, IDHELP};
		DWORD ids []= {ID_APPLY_NOW, IDHELP};

		int s = sizeof (ids) / sizeof (DWORD);
		for (int i = 0; i < s; i ++)
		{
			CWnd *cWnd = GetDlgItem (ids[i]);
			if (cWnd != NULL)
			{
				cWnd->ShowWindow (SW_HIDE);
			}
		}
*/
		CTabCtrl *tab = GetTabControl ();
		tab->ShowWindow (FALSE);
	}

	return bResult;
}

BOOL CDbPropertySheet::Read ()
{
	return TRUE;
}

BOOL CDbPropertySheet::Print ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->Print ();
}

BOOL CDbPropertySheet::PrintAll ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->PrintAll ();
}

BOOL CDbPropertySheet::EnablePrint (CCmdUI *pCmdUI)
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->EnablePrint (pCmdUI);
}

BOOL CDbPropertySheet::TextCent ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->TextCent ();
}

BOOL CDbPropertySheet::EnableTextCent (CCmdUI *pCmdUI)
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->EnableTextCent (pCmdUI);
}

BOOL CDbPropertySheet::TextLeft ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->TextLeft ();
}

BOOL CDbPropertySheet::EnableTextLeft (CCmdUI *pCmdUI)
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->EnableTextLeft (pCmdUI);
}

BOOL CDbPropertySheet::TextRight ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->TextRight ();
}

BOOL CDbPropertySheet::EnableTextRight (CCmdUI *pCmdUI)
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->EnableTextRight (pCmdUI);
}

BOOL CDbPropertySheet::DeleteAll ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->DeleteAll ();
}

BOOL CDbPropertySheet::Insert ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->Insert ();
}

BOOL CDbPropertySheet::Delete ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	return Page->Delete ();
}

BOOL CDbPropertySheet::Write ()
{
	int count = GetPageCount ();

	for (int i = 0; i < count; i ++)
	{
		CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (i);
		if (IsWindow (Page->m_hWnd))
		{
			Page->Write ();
		}
	}
	for (int i = 0; i < count; i ++)
	{
		CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (i);
		if (IsWindow (Page->m_hWnd))
		{
			Page->AfterWrite ();
		}
	}
	return TRUE;
}

void CDbPropertySheet::Show ()
{

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	Page->Show ();
}

void CDbPropertySheet::StepBack ()
{

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	Page->StepBack ();
}

void CDbPropertySheet::NextRecord ()
{
	int count = GetPageCount ();

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	Page->NextRecord ();

/*
	for (int i = 0; i < count; i ++)
	{
		CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (i);
		Page->NextRecord ();
	}
*/
}

void CDbPropertySheet::PriorRecord ()
{
	int count = GetPageCount ();

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	Page->PriorRecord ();

/*
	for (int i = 0; i < count; i ++)
	{
		CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (i);
		Page->PriorRecord ();
	}
*/
}

void CDbPropertySheet::FirstRecord ()
{
	int count = GetPageCount ();

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	Page->FirstRecord ();

/*
	for (int i = 0; i < count; i ++)
	{
		CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (i);
		Page->FirstRecord ();
	}
*/
}

void CDbPropertySheet::LastRecord ()
{
	int count = GetPageCount ();

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	Page->LastRecord ();

/*
	for (int i = 0; i < count; i ++)
	{
		CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (i);
		Page->LastRecord ();
	}
*/
}

void CDbPropertySheet::OnBreak ()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

//    GetParent ()->SendMessage (WM_COMMAND, ID_BREAK, 0);
}

void CDbPropertySheet::OnCopy ()
{
	int count = GetPageCount ();

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	Page->OnCopy ();
}

void CDbPropertySheet::OnPaste ()
{
	int count = GetPageCount ();

	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	Page->OnPaste ();
}

void CDbPropertySheet::OnMarkAll ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	Page->OnMarkAll ();
}

void CDbPropertySheet::OnUnMarkAll ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	Page->OnUnMarkAll ();
}
