#include "stdafx.h"
#include "ChoiceA.h"
#include "DbUniCode.h"
#include "Process.h"
#include "StrFuncs.h"
#include "Token.h"
#include "DynDialog.h"
#include "DbQuery.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceA::Sort1 = -1;
int CChoiceA::Sort2 = -1;
int CChoiceA::Sort3 = -1;
int CChoiceA::Sort4 = -1;
int CChoiceA::Sort5 = -1;
int CChoiceA::Sort6 = -1;
int CChoiceA::Sort7 = -1;
int CChoiceA::Sort8 = -1;
int CChoiceA::Sort9 = -1;
int CChoiceA::Sort10 = -1;
int CChoiceA::Sort11 = -1;

CChoiceA::CChoiceA(CWnd* pParent) 
        : CChoiceX(pParent)
{
	ptcursor = -1;
	wgcursor = -1;
	hwgcursor = -1;
	Where = "";
	Types = "";
	Bean.ArchiveName = _T("AList.prp");
}

CChoiceA::~CChoiceA() 
{
	DestroyList ();
	if (ptcursor != -1) DbClass->sqlclose (ptcursor);
	if (wgcursor != -1) DbClass->sqlclose (wgcursor);
	if (hwgcursor != -1) DbClass->sqlclose (hwgcursor);
	ptcursor = -1;
	wgcursor = -1;
	hwgcursor = -1;
}

void CChoiceA::DestroyList() 
{
	for (std::vector<CABasList *>::iterator pabl = ABasList.begin (); pabl != ABasList.end (); ++pabl)
	{
		CABasList *abl = *pabl;
		delete abl;
	}
    ABasList.clear ();
}

void CChoiceA::FillList () 
{
    double  a;
    TCHAR a_bz1 [50];
    TCHAR a_bz2 [50];
    TCHAR a_bz3 [50];
    short a_typ;
    short a_typ2;
    long ag;
    short wg;
    short hwg;
    short teil_smt;
    short me_einh;
	TCHAR ptbez [37];
	extern short sql_mode;
	short sql_s;

	sql_s = sql_mode;
	sql_mode = 2;
	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Artikel"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Artikel"),           1, 150, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung 1"),     2, 250);
    SetCol (_T("Bezeichnung 2"),     3, 250);
    SetCol (_T("Artikeltyp"),        4, 150);
    SetCol (_T("Artikelgruppe"),     5, 150);
    SetCol (_T("Warengruppe"),       6, 150);
    SetCol (_T("Hautpwarengruppe"),  7, 150);
    SetCol (_T("Teilsortiment"),     8, 150);
    SetCol (_T("Mengeneinheit"),     9, 150);
    SetCol (_T("Matchcode"),        10, 150);
	SortRow = 2;

	if (FirstRead && !HideFilter)
	{
		FirstRead = FALSE;
		Load ();
		if (!IsModal)
		{
			PostMessage (WM_COMMAND, IDC_FILTER, 0l);
		}
//		return;
	}
	if (ABasList.size () == 0)
	{
		DbClass->sqlout ((double *)&a,        SQLDOUBLE, 0);
		DbClass->sqlout ((LPTSTR)  a_bz1,     SQLCHAR, sizeof (a_bz1));
		DbClass->sqlout ((LPTSTR)  a_bz2,     SQLCHAR, sizeof (a_bz2));
		DbClass->sqlout ((short *) &a_typ,    SQLSHORT, 0);
		DbClass->sqlout ((short *) &a_typ2,    SQLSHORT, 0);
		DbClass->sqlout ((long *)  &ag,       SQLLONG, 0);
		DbClass->sqlout ((short *) &wg,       SQLSHORT, 0);
 	    DbClass->sqlout ((short *) &hwg,      SQLSHORT, 0);
 	    DbClass->sqlout ((short *) &teil_smt, SQLSHORT, 0);
 	    DbClass->sqlout ((short *) &me_einh,  SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  a_bz3,     SQLCHAR, sizeof (a_bz3));
		CString Sql  = (_T("select a_bas.a, a_bas.a_bz1, a_bas.a_bz2, a_bas.a_typ, a_bas.a_typ2, a_bas.ag, a_bas.wg, a_bas.hwg, a_bas.teil_smt, a_bas.me_einh, a_bas.a_bz3 "
						   "from a_bas, outer ek_pr where a_bas.a = ek_pr.a and a_bas.a > 0 and a_bas.delstatus = 0"));
		Sql += " ";
		Sql += Where;
		if (QueryString != _T(""))
		{
			Sql += _T(" and ");
			Sql += QueryString;
		}
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe f�r den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}
/*
		int cursor = DbClass->sqlcursor (_T("select a, a_bz1, a_bz2, a_typ, ag, wg, hwg, teil_smt, me_einh "
											"from a_bas where a > 0"));
*/
		memset (a_bz3, 0, sizeof (a_bz3));
		DbClass->sqlopen (cursor);
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) a_bz1;
			CDbUniCode::DbToUniCode (a_bz1, pos);
			pos = (LPSTR) a_bz2;
			CDbUniCode::DbToUniCode (a_bz2, pos);
			pos = (LPSTR) a_bz3;
			CDbUniCode::DbToUniCode (a_bz3, pos);

			if (TestType (a_typ, a_typ2) == 2)
			{
				a_typ = a_typ2;
			}
			CString Ptwert;
			Ptwert.Format (_T("%hd"), a_typ);
            GetPtBez (_T("a_typ"), Ptwert.GetBuffer (), ptbez);
			CString A_typ;
			A_typ.Trim ();
//			A_typ.Format (_T("%hd %s"), a_typ, ptbez);
			A_typ.Format (_T("%s"), ptbez);

            memcpy (&Ag.ag, &ag_null, sizeof (AG));
            Ag.ag.ag = ag;
			Ag.dbreadfirst ();
			pos = (LPSTR) Ag.ag.ag_bz1;
			CDbUniCode::DbToUniCode (Ag.ag.ag_bz1, pos);
			CString Ags;
			Ags.Format (_T("%s"), Ag.ag.ag_bz1);
			Ags.Trim ();

			CString Wgs;
            this->wg = wg;
            GetWgBez ();
			Wgs = wg_bz1;


			CString Hwgs;
            this->hwg = hwg;
            GetHwgBez ();
			Hwgs = hwg_bz1;


			Ptwert.Format (_T("%hd"), teil_smt);
            GetPtBez (_T("teil_smt"), Ptwert.GetBuffer (), ptbez);
			CString Teil_smt;
			Teil_smt.Format (_T("%s"), ptbez);
			Teil_smt.Trim ();

			Ptwert.Format (_T("%hd"), me_einh);
            GetPtBez (_T("me_einh"), Ptwert.GetBuffer (), ptbez);
			CString Me_einh;
			Me_einh.Format (_T("%s"), ptbez);
			Me_einh.Trim ();

			CABasList *abl = new CABasList (a, a_bz1, a_bz2, A_typ.GetBuffer (),
			                                Ags.GetBuffer (),
			                                Wgs.GetBuffer (),
			                                Hwgs.GetBuffer (),
			                                Teil_smt.GetBuffer (),
											Me_einh.GetBuffer (),
											a_bz3);
			ABasList.push_back (abl);
			memset (a_bz3, 0, sizeof (a_bz3));
		}
		DbClass->sqlclose (cursor);
		Load ();
	}
	sql_mode = sql_s;

	for (std::vector<CABasList *>::iterator pabl = ABasList.begin (); pabl != ABasList.end (); ++pabl)
	{
		CABasList *abl = *pabl;
		CString Art;
		Art.Format (_T("%.0lf"), abl->a); 
		CString Num;
		CString Bez;
		_tcscpy (a_bz1, abl->a_bz1.GetBuffer ());
		_tcscpy (a_bz2, abl->a_bz2.GetBuffer ());
		_tcscpy (a_bz3, abl->a_bz3.GetBuffer ());

		CString LText;
		LText.Format (_T("%.0lf %s"), abl->a, 
									  abl->a_bz1.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Art;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (a_bz1, i, 2);
        ret = SetItemText (a_bz2, i, 3);
        ret = SetItemText (abl->a_typ.GetBuffer (), i, 4);
        ret = SetItemText (abl->ag.GetBuffer (), i, 5);
        ret = SetItemText (abl->wg.GetBuffer (), i, 6);
        ret = SetItemText (abl->hwg.GetBuffer (), i, 7);
        ret = SetItemText (abl->teil_smt.GetBuffer (), i, 8);
        ret = SetItemText (abl->me_einh.GetBuffer (), i, 9);
        ret = SetItemText (a_bz3, i, 10);
        i ++;
    }

    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort7 = -1;
    Sort8 = -1;
    Sort9 = -1;
    Sort10 = -1;
    Sort11 = -1;
    Sort (listView);
}


void CChoiceA::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceA::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceA::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceA::SearchABz1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceA::SearchABz2 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceA::SearchCol (CListCtrl *ListBox, LPTSTR Search, int col)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, col);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceA::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
	if ((EditText.Find (_T("*")) != -1) || EditText.Find (_T("?")) != -1)
	{
		CChoiceX::SearchMatch (EditText);
		return;
	}

    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchABz1 (ListBox, EditText.GetBuffer ());
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchABz2 (ListBox, EditText.GetBuffer ());
             break;
        default :
             EditText.MakeUpper ();
             SearchCol (ListBox, EditText.GetBuffer (), SortRow);
             break;
    }
}

int CChoiceA::GetPtBez (LPSTR ptitem, LPTSTR ptwert, LPTSTR ptbez)
{
	if (ptcursor == -1)
	{
		DbClass->sqlout ((LPTSTR) this->ptbez,  SQLCHAR, sizeof (this->ptbez));
		DbClass->sqlout ((LPTSTR) this->ptbezk, SQLCHAR, sizeof (this->ptbezk));
		DbClass->sqlin ((LPTSTR) this->ptitem, SQLCHAR, sizeof (this->ptitem));
		DbClass->sqlin  ((LPTSTR) this->ptwert, SQLCHAR, sizeof (this->ptwert));
	    ptcursor = DbClass->sqlcursor (_T("select ptbez,ptbezk from ptabn where ptitem = ? ")
							           _T("and ptwert = ?"));
	}
	_tcscpy (this->ptbez, _T(""));
	_tcscpy (this->ptbezk, _T(""));
	_tcscpy (this->ptitem, ptitem);
	_tcscpy (this->ptwert, ptwert);
	DbClass->sqlopen (ptcursor);
	int dsqlstatus = DbClass->sqlfetch (ptcursor);

	_tcscpy (ptbez, this->ptbez);
	LPSTR pos = (LPSTR) ptbez;
    CDbUniCode::DbToUniCode (ptbez, pos);
	return dsqlstatus;
}


int CChoiceA::GetWgBez ()
{
	if (wgcursor == -1)
	{
		DbClass->sqlout ((LPTSTR) this->wg_bz1, SQLCHAR, sizeof (this->wg_bz1));
		DbClass->sqlin  ((short *) &this->wg, SQLSHORT, 0);
	    wgcursor = DbClass->sqlcursor (_T("select wg_bz1 from wg where wg = ? "));
	}
	_tcscpy (this->wg_bz1, _T(""));
	DbClass->sqlopen (wgcursor);
	int dsqlstatus = DbClass->sqlfetch (wgcursor);

	LPSTR pos = (LPSTR) wg_bz1;
    CDbUniCode::DbToUniCode (wg_bz1, pos);
	return dsqlstatus;
}

int CChoiceA::GetHwgBez ()
{
	if (hwgcursor == -1)
	{
		DbClass->sqlout ((LPTSTR) this->hwg_bz1, SQLCHAR, sizeof (this->wg_bz1));
		DbClass->sqlin  ((short *) &this->hwg, SQLSHORT, 0);
	    hwgcursor = DbClass->sqlcursor (_T("select hwg_bz1 from hwg where hwg = ? "));
	}
	_tcscpy (this->hwg_bz1, _T(""));
	DbClass->sqlopen (hwgcursor);
	int dsqlstatus = DbClass->sqlfetch (hwgcursor);

	LPSTR pos = (LPSTR) hwg_bz1;
    CDbUniCode::DbToUniCode (hwg_bz1, pos);
	return dsqlstatus;
}

int CALLBACK CChoiceA::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   else if (SortRow == 4)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort5;
   }
   else if (SortRow == 5)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort6;
   }
   else if (SortRow == 6)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort7;
   }
   else if (SortRow == 7)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort8;
   }
   else if (SortRow == 8)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort9;
   }
   else if (SortRow == 9)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort10;
   }
   else if (SortRow == 10)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort11;
   }
   return 0;
}

void CChoiceA::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
        case 4:
              Sort5 *= -1;
			  if (Sort5 < 0) SortPos = 1;
              break;
        case 5:
              Sort6 *= -1;
			  if (Sort6 < 0) SortPos = 1;
              break;
        case 6:
              Sort7 *= -1;
			  if (Sort7 < 0) SortPos = 1;
              break;
        case 7:
              Sort8 *= -1;
			  if (Sort8 < 0) SortPos = 1;
              break;
        case 8:
              Sort9 *= -1;
			  if (Sort9 < 0) SortPos = 1;
              break;
        case 9:
              Sort10 *= -1;
			  if (Sort10 < 0) SortPos = 1;
              break;
        case 10:
              Sort11 *= -1;
			  if (Sort11 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CABasList *abl = ABasList [i];
		   
		   abl->a        = _tstof (ListBox->GetItemText (i, 1));
		   abl->a_bz1    = ListBox->GetItemText (i, 2);
		   abl->a_bz2    = ListBox->GetItemText (i, 3);
		   abl->a_typ    = ListBox->GetItemText (i, 4);
		   abl->ag       = ListBox->GetItemText (i, 5);
		   abl->wg       = ListBox->GetItemText (i, 6);
		   abl->hwg      = ListBox->GetItemText (i, 7);
		   abl->teil_smt = ListBox->GetItemText (i, 8);
		   abl->me_einh  = ListBox->GetItemText (i, 9);
		   abl->a_bz3    = ListBox->GetItemText (i, 10);
	}


	for (int i = 1; i <= 9; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);

}

void CChoiceA::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = ABasList [idx];
}

CABasList *CChoiceA::GetSelectedText ()
{
	CABasList *abl = (CABasList *) SelectedRow;
	return abl;
}

void CChoiceA::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (ABasList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

void CChoiceA::SetDefault ()
{
	CChoiceX::SetDefault ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    ListBox->SetColumnWidth (0, 0);
    ListBox->SetColumnWidth (1, 150);
//	SetColFmt (1, LVCFMT_RIGHT);
    ListBox->SetColumnWidth (2, 250);
    ListBox->SetColumnWidth (3, 250);
    ListBox->SetColumnWidth (4, 150);
    ListBox->SetColumnWidth (5, 150);
    ListBox->SetColumnWidth (6, 150);
    ListBox->SetColumnWidth (7, 150);
    ListBox->SetColumnWidth (8, 150);
    ListBox->SetColumnWidth (9, 150);
    ListBox->SetColumnWidth (10,250);
}

void CChoiceA::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cA = m_List.GetItemText (idx, 1);  
		double a = CStrFuncs::StrToDouble (cA.GetBuffer ());
		Message.Format (_T("ArtikelNr=%.0lf"), a);
		ToClipboard (Message);
	}
	p.SetCommand (_T("12100"));
	HANDLE Pid = p.Start ();
}

int CChoiceA::TestType (short a_typ, short a_typ2)
{
	if (Types == "") return 1;

	CToken t (Types, ",");
	LPTSTR p;
	while ((p = t.NextToken ()) != NULL)
	{
		int type = _tstoi (p);
		if (a_typ == (short) type)
		{
			return 1;
		}
		else if (a_typ2 == (short) type)
		{
			return 2;
		}
	}
	return 0;
}

void CChoiceA::OnFilter ()
{
	CDynDialog dlg;
	CDynDialogItem *Item;

	dlg.DlgCaption = "Filter";
	dlg.UseFilter = UseFilter;

	if (vFilter.size () > 0)
	{
		for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
		{
			CDynDialogItem *i = *it;
			Item = new CDynDialogItem ();
			*Item = *i;
			if (Item->Name == _T("reset"))
			{
				if (UseFilter)
				{
					Item->Value = _T("aktiv");
				}
				else
				{
					Item->Value = _T("nicht aktiv");
				}
			}
			dlg.AddItem (Item);

		}
	}
	else
	{
		int y = 10;
		Item = new CDynDialogItem ();
		Item->Name = _T("la");
		Item->CtrClass = "static";
		Item->Value = _T("Artikelnummer");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1001;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("a");
		Item->Type = Item->Decimal;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1002;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("la_bz1");
		Item->CtrClass = "static";
		Item->Value = _T("Bezeichnung 1");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1003;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("a_bz1");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		y += dlg.RowSpace;


		Item = new CDynDialogItem ();
		Item->Name = _T("la_bz2");
		Item->CtrClass = "static";
		Item->Value = _T("Bezeichnung 2");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("a_bz2");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1005;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("la_typ");
		Item->CtrClass = "static";
		Item->Value = _T("Artikeltyp");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1006;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("a_typ");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1007;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lag");
		Item->CtrClass = "static";
		Item->Value = _T("Artikelgruppe");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1008;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("ag");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1009;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lwg");
		Item->Type = Item->Short;
		Item->CtrClass = "static";
		Item->Value = _T("Warengruppe");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1010;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("wg");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1011;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lhwg");
		Item->CtrClass = "static";
		Item->Value = _T("HWG");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1012;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("hwg");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1013;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lteil_smt");
		Item->CtrClass = "static";
		Item->Value = _T("Teilsortortimet");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1014;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("teil_smt");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1015;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lme_einh");
		Item->CtrClass = "static";
		Item->Value = _T("Mengeneinheit");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1016;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("me_einh");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1017;
		dlg.AddItem (Item);
	
		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("la_bz3");
		Item->CtrClass = "static";
		Item->Value = _T("Matchcode");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1018;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("a_bz3");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1019;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		dlg.Header.cy = y + 30;

		Item = new CDynDialogItem ();
		Item->Name = _T("OK");
		Item->CtrClass = "button";
		Item->Value = _T("OK");
		Item->Template.x = 15;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDOK;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("cancel");
		Item->CtrClass = "button";
		Item->Value = _T("abbrechen");
		Item->Template.x = 70;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDCANCEL;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("reset");
		Item->CtrClass = "button";
		if (UseFilter)
		{
			Item->Value = _T("aktiv");
		}
		else
		{
			Item->Value = _T("nicht aktiv");
		}
		Item->Template.x = 125;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = ID_USE;
		dlg.AddItem (Item);
	}

	dlg.CreateModal ();
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK) return;
 
	UseFilter = dlg.UseFilter;
	int count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = dlg.Items.begin (); it != dlg.Items.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (count >= (int) vFilter.size ())
			{
				Item = new CDynDialogItem ();
				*Item = *i;
				vFilter.push_back (Item);
			}
			*vFilter[count] = *i;
			count ++;
	}
    CString Query;
    CDbQuery DbQuery;

	QueryString = _T("");
	if (!UseFilter)
	{
		FillList ();
		return;
	}
	count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (strcmp (i->CtrClass, "edit") != 0) continue; 
			if (i->Value.Trim () != _T(""))
			{
				if (i->Type == i->String || i->Type == i->Date)
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, TRUE);
				}
				else
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, FALSE);
				}
				if (count > 0)
				{
					QueryString += _T(" and ");
				}
                QueryString += Query;
				count ++;
			}
	}
	FillList ();
}
