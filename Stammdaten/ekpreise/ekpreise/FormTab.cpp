#include "StdAfx.h"
#include "formtab.h"


CFormTab *fTab;
CFormTab *CFormTab::v;

CFormTab::CFormTab(void)
{
	v = this;
	fTab = this;
}

CFormTab::~CFormTab(void)
{
}

void CFormTab::Show(void)
{
	FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) GetNext ()) != NULL)
	{
		int p = pos;
		f->Show ();
		pos = p;
	}
}

void CFormTab::Get (void)
{
	FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) GetNext ()) != NULL)
	{
		f->Get ();
	}
}

CFormField *CFormTab::GetFormField (CWnd *ctrl)
{
	FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) GetNext ()) != NULL)
	{
		if (*f == ctrl) return f;
	}
	return NULL;
}

