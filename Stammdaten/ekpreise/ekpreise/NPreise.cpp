#include "StdAfx.h"
#include "npreise.h"

CNPreise::CNPreise(void)
{
	cEkN = _T("0.0");
	cVkN = _T("0.0");
	memcpy (&akt_krz, &akt_krz_null, sizeof (AKT_KRZ));
}

CNPreise::CNPreise(CString& cEkN, CString& cVkN, AKT_KRZ& akt_krz)
{
	this->cEkN = cEkN.Trim ();
	this->cVkN = cVkN.Trim ();
	memcpy (&this->akt_krz, &akt_krz, sizeof (AKT_KRZ));
}

CNPreise::~CNPreise(void)
{
}
