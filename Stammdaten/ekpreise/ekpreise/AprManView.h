// AprManView.h : Schnittstelle der Klasse CAprManView
//


#pragma once


class CAprManView : public CView
{
protected: // Nur aus Serialisierung erstellen
	CAprManView();
	DECLARE_DYNCREATE(CAprManView)

// Attribute
public:
	CAprManDoc* GetDocument() const;

// Operationen
public:

// �berschreibungen
	public:
	virtual void OnDraw(CDC* pDC);  // �berladen, um diese Ansicht darzustellen
virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~CAprManView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // Debugversion in AprManView.cpp
inline CAprManDoc* CAprManView::GetDocument() const
   { return reinterpret_cast<CAprManDoc*>(m_pDocument); }
#endif

