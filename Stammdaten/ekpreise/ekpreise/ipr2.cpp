#include "stdafx.h"
#include "ipr2.h"

struct IPR2 ipr2, ipr2_null;


void IPR2_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &ipr2.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ipr2.a, SQLDOUBLE, 0);
            sqlin ((long *)    &ipr2.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &ipr2.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &ipr2.kun, SQLLONG, 0);
    sqlout ((short *) &ipr2.mdn,SQLSHORT,0);
    sqlout ((long *) &ipr2.pr_gr_stuf,SQLLONG,0);
    sqlout ((long *) &ipr2.kun_pr,SQLLONG,0);
    sqlout ((double *) &ipr2.a,SQLDOUBLE,0);
    sqlout ((double *) &ipr2.vk_pr_i,SQLDOUBLE,0);
    sqlout ((double *) &ipr2.ld_pr,SQLDOUBLE,0);
    sqlout ((short *) &ipr2.aktion_nr,SQLSHORT,0);
    sqlout ((TCHAR *) ipr2.a_akt_kz,SQLCHAR,2);
    sqlout ((TCHAR *) ipr2.modif,SQLCHAR,2);
    sqlout ((short *) &ipr2.waehrung,SQLSHORT,0);
    sqlout ((long *) &ipr2.kun,SQLLONG,0);
    sqlout ((double *) &ipr2.a_grund,SQLDOUBLE,0);
    sqlout ((TCHAR *) ipr2.kond_art,SQLCHAR,5);
    sqlout ((double *) &ipr2.vk_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &ipr2.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &ipr2.add_ek_proz,SQLDOUBLE,0);
    sqlout ((double *) &ipr2.add_ek_abs,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select ipr.mdn,  ")
_T("ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.ld_pr,  ")
_T("ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  ipr.waehrung,  ipr.kun,  ")
_T("ipr.a_grund,  ipr.kond_art,  ipr.vk_pr_eu,  ipr.ld_pr_eu,  ")
_T("ipr.add_ek_proz,  ipr.add_ek_abs from ipr ")

#line 16 "ipr.rpp"
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?"));
    sqlin ((short *) &ipr2.mdn,SQLSHORT,0);
    sqlin ((long *) &ipr2.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &ipr2.kun_pr,SQLLONG,0);
    sqlin ((double *) &ipr2.a,SQLDOUBLE,0);
    sqlin ((double *) &ipr2.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &ipr2.ld_pr,SQLDOUBLE,0);
    sqlin ((short *) &ipr2.aktion_nr,SQLSHORT,0);
    sqlin ((TCHAR *) ipr2.a_akt_kz,SQLCHAR,2);
    sqlin ((TCHAR *) ipr2.modif,SQLCHAR,2);
    sqlin ((short *) &ipr2.waehrung,SQLSHORT,0);
    sqlin ((long *) &ipr2.kun,SQLLONG,0);
    sqlin ((double *) &ipr2.a_grund,SQLDOUBLE,0);
    sqlin ((TCHAR *) ipr2.kond_art,SQLCHAR,5);
    sqlin ((double *) &ipr2.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &ipr2.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &ipr2.add_ek_proz,SQLDOUBLE,0);
    sqlin ((double *) &ipr2.add_ek_abs,SQLDOUBLE,0);
            sqltext = _T("update ipr set ipr.mdn = ?,  ")
_T("ipr.pr_gr_stuf = ?,  ipr.kun_pr = ?,  ipr.a = ?,  ipr.vk_pr_i = ?,  ")
_T("ipr.ld_pr = ?,  ipr.aktion_nr = ?,  ipr.a_akt_kz = ?,  ipr.modif = ?,  ")
_T("ipr.waehrung = ?,  ipr.kun = ?,  ipr.a_grund = ?,  ipr.kond_art = ?,  ")
_T("ipr.vk_pr_eu = ?,  ipr.ld_pr_eu = ?,  ipr.add_ek_proz = ?,  ")
_T("ipr.add_ek_abs = ? ")

#line 22 "ipr.rpp"
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?");
            sqlin ((short *)   &ipr2.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ipr2.a, SQLDOUBLE, 0);
            sqlin ((long *)    &ipr2.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &ipr2.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &ipr2.kun, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &ipr2.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ipr2.a, SQLDOUBLE, 0);
            sqlin ((long *)    &ipr2.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &ipr2.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &ipr2.kun, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select a from ipr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?"));
            sqlin ((short *)   &ipr2.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ipr2.a, SQLDOUBLE, 0);
            sqlin ((long *)    &ipr2.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &ipr2.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &ipr2.kun, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from ipr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?"));
    sqlin ((short *) &ipr2.mdn,SQLSHORT,0);
    sqlin ((long *) &ipr2.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &ipr2.kun_pr,SQLLONG,0);
    sqlin ((double *) &ipr2.a,SQLDOUBLE,0);
    sqlin ((double *) &ipr2.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &ipr2.ld_pr,SQLDOUBLE,0);
    sqlin ((short *) &ipr2.aktion_nr,SQLSHORT,0);
    sqlin ((TCHAR *) ipr2.a_akt_kz,SQLCHAR,2);
    sqlin ((TCHAR *) ipr2.modif,SQLCHAR,2);
    sqlin ((short *) &ipr2.waehrung,SQLSHORT,0);
    sqlin ((long *) &ipr2.kun,SQLLONG,0);
    sqlin ((double *) &ipr2.a_grund,SQLDOUBLE,0);
    sqlin ((TCHAR *) ipr2.kond_art,SQLCHAR,5);
    sqlin ((double *) &ipr2.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &ipr2.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &ipr2.add_ek_proz,SQLDOUBLE,0);
    sqlin ((double *) &ipr2.add_ek_abs,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into ipr (mdn,  ")
_T("pr_gr_stuf,  kun_pr,  a,  vk_pr_i,  ld_pr,  aktion_nr,  a_akt_kz,  modif,  waehrung,  ")
_T("kun,  a_grund,  kond_art,  vk_pr_eu,  ld_pr_eu,  add_ek_proz,  add_ek_abs) ")

#line 57 "ipr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?)")); 

#line 59 "ipr.rpp"
}

BOOL IPR2_CLASS::operator== (IPR2& ipr2)
{
            if (this->ipr2.pr_gr_stuf != ipr2.pr_gr_stuf) return FALSE;  
            if (this->ipr2.kun_pr     != ipr2.kun_pr) return FALSE;  
            if (this->ipr2.kun        != ipr2.kun) return FALSE;  
            if (this->ipr2.a          != ipr2.a) return FALSE;  
            return TRUE;
} 
