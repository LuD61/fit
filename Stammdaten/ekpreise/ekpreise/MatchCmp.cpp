#include "StdAfx.h"
#include ".\matchcmp.h"

CMatchCmp::CMatchCmp(void)
{
	MatchCode = NULL;
}

CMatchCmp::~CMatchCmp(void)
{
}

BOOL CMatchCmp::Compare (CString& txt)
{
	if (MatchCode == NULL) return TRUE;

	txt.MakeUpper ();
	MatchCode->MakeUpper ();
    TCHAR *p1 = MatchCode->GetBuffer ();
    TCHAR *p2 = txt.GetBuffer ();
    return matchcomp (p1, p2);
}

BOOL CMatchCmp::matchcomp (LPTSTR m, LPTSTR s)
{
		  if (*m == 0 && *s == 0)
		  {
			  return TRUE;
		  }

		  if (*m == 0)
		  {
			  return FALSE;
		  }

		  if (*s == 0)
		  {
			  return FALSE;
		  }

		  if (*m == _T('*'))
		  {
		      while (*m == _T('*') && *m != 0) m += 1;
		      if (*m == 0)
			  {
			         return TRUE;
			  }

			  int mlen;
			  for (mlen = 0; (m[mlen] != 0) && (m[mlen] != _T('*')); mlen ++);
		      while (*s != 0)
			  {
			        if (*m == _T('?'))
					{
				          m += 1;
				          continue;
					}
			        if (*m == *s)
					{
						if (memcmp (m, s, mlen) == 0)
						{
				           break;
						}
					}
			        s += 1;
			  }
		      if (*s == 0)
			  {
			       return FALSE;
			  }
		  }
		  else
		  {
			  if (*m != *s && *m != _T('?'))
			  {
				  return FALSE;
			  }
		  }

		  for (int i = 1; ; i ++)
		  {
			  if (m[i] == 0)
			  {
				  if (s[i] == 0)
				  {
				        return TRUE;
				  }
				  else
				  {
				        return FALSE;
				  }
			  }

			  if (m[i] == _T('*'))
			  {
			      return matchcomp (&m[i], &s[i]);
			  }

			  if (s[i] == 0)
			  {
				  return FALSE;
			  }

			  if (m[i] == _T('?'))
			  {
				  continue;
			  }
			  if (m[i] == s[i])
			  {
				  continue;
			  }
			  return matchcomp (&m[i], &s[i]);
		  }
		  return TRUE;
}
