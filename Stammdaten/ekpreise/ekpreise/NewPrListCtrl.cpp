#include "StdAfx.h"
#include "NewPrListCtrl.h"
#include "StrFuncs.h"
#include "resource.h"

CNewPrListCtrl::CNewPrListCtrl(void)
{
	MdnCombo.Init ();
	FilCombo.Init ();
	ChoiceMdnGr = NULL;
	ChoiceMdn = NULL;
	ChoiceFilGr = NULL;
	ChoiceFil = NULL;
	ModalChoiceMdnGr = TRUE;
	ModalChoiceMdn = TRUE;
	ModalChoiceFilGr = TRUE;
	ModalChoiceFil = TRUE;
	PosMdnGr    = POSMDNGR;
    PosMdn      = POSMDN;
	PosFilGr    = POSFILGR;
    PosFil      = POSFIL;
    PosDate     = POSDATE;
	PosPrEk     = POSPREK;
	PosPrVk     = POSPRVK;
	PosPrEkN    = POSPREKN;
	PosPrVkN    = POSPRVKN;

	Position[0] = &PosMdnGr;
	Position[1] = &PosMdn;
	Position[2] = &PosFilGr;
	Position[3] = &PosFil;
	Position[4] = &PosPrEk;
	Position[5] = &PosPrVk;
	Position[6] = &PosPrEkN;
	Position[7] = &PosPrVkN;
	Position[8] = NULL;
	MaxComboEntries = 20;
	FilGrCursor = -1;
	FilCursor = -1;

	Aufschlag = LIST;
	Mode = STANDARD;
	m_Mdn = 0;
	ListRows.Init ();
	ListMode = Large;
}

CNewPrListCtrl::~CNewPrListCtrl(void)
{
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
	}
	if (ChoiceFil != NULL)
	{
		delete ChoiceFil;
	}

	CString *c;
    MdnGrCombo.FirstPosition ();
	while ((c = (CString *) MdnGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnGrCombo.Init ();
    MdnCombo.FirstPosition ();
	while ((c = (CString *) MdnCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnCombo.Init ();
    FilGrCombo.FirstPosition ();
	while ((c = (CString *) FilGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilGrCombo.Init ();
    FilCombo.FirstPosition ();
	while ((c = (CString *) FilCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilCombo.Init ();
}

BEGIN_MESSAGE_MAP(CNewPrListCtrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CNewPrListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (PosDate, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		if (m_Mdn == 0)
		{
			StartEnter (PosMdnGr, 0);
		}
		else
		{
			StartEnter (PosFilGr, 0);
		}
	}
}

void CNewPrListCtrl::StartEnter (int col, int row)
{
	BOOL RowChanged = FALSE;
//	int sRow;
	if (col == 0) col = 1;
	CString Value;

	if (col == PosPrEk) return; 
	if (col == PosPrVk) return; 
	GetEnter ();
	GetColValue (row, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (col == PosFilGr || col == PosFil) return;
	}

	if (EditCol == PosFil)   ReadFil ();
	if (EditCol == PosFilGr) ReadFilGr ();

/*
	if (row != EditRow)
	{
		sRow = EditRow;
		RowChanged = TRUE;
	}
*/

	if (col == PosMdnGr)
	{
		if (MaxComboEntries > 0 && MdnGrCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &MdnGrCombo);
			oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosMdn)
	{
		if (MaxComboEntries > 0 && MdnCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &MdnCombo);
			oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosFilGr)
	{
		FillFilGrCombo (row);
		if (MaxComboEntries > 0 && FilGrCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &FilGrCombo);
		    oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosFil)
	{
		FillFilCombo (row);
		if (MaxComboEntries > 0 && FilCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &FilCombo);
		    oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosDate)
	{
			if (IsWindow (ListDate.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterDate (col, row);
			CTime date = CTime::GetCurrentTime ();
//			ListDate.SetRange (&date, NULL);
	}
	else
	{
		CEditListCtrl::StartEnter (col, row);
	}
/*
	if (RowChanged)
	{
		EditRow = sRow;
		TestAprIndex ();
		EditRow = row;
	}
*/
}

void CNewPrListCtrl::ReadApr ()
{
	CString cPrEk;
	CString cPrVk;

	if (EditCol != PosMdnGr && EditCol != PosMdn &&
		EditCol != PosFilGr && EditCol != PosFil)
	{
		return; 
	}

	GetEnter ();
	CString cMdnGr = GetItemText (EditRow, PosMdnGr);
	CString cMdn   = GetItemText (EditRow, PosMdn);
	CString cFilGr = GetItemText (EditRow, PosFilGr);
	CString cFil   = GetItemText (EditRow, PosFil);

	A_pr.a_pr.mdn_gr = _tstoi (cMdnGr.GetBuffer ());
	A_pr.a_pr.mdn    = _tstoi (cMdn.GetBuffer ());
	A_pr.a_pr.fil_gr = _tstoi (cFilGr.GetBuffer ());
	A_pr.a_pr.fil    = _tstoi (cFil.GetBuffer ());

	if (A_pr.dbreadfirst () == 0)
	{
		cPrEk.Format (_T("%.2lf"), A_pr.a_pr.pr_ek_euro);
		cPrVk.Format (_T("%.2lf"), A_pr.a_pr.pr_vk_euro);
		SetItemText (EditRow, PosPrEk, cPrEk.GetBuffer ());
		SetItemText (EditRow, PosPrVk, cPrVk.GetBuffer ());
	}
	else
	{
		SetItemText (EditRow, PosPrEk, _T("0,00"));
		SetItemText (EditRow, PosPrVk, _T("0,00"));
	}

	Akt_krz.akt_krz.a      = A_pr.a_pr.a;
	Akt_krz.akt_krz.mdn_gr = A_pr.a_pr.mdn_gr;
	Akt_krz.akt_krz.mdn    = A_pr.a_pr.mdn;
	Akt_krz.akt_krz.fil_gr = A_pr.a_pr.fil_gr;
	Akt_krz.akt_krz.fil    = A_pr.a_pr.fil;
	CString Date = GetItemText (EditRow, PosDate);
	Akt_krz.ToDbDate (Date, &Akt_krz.akt_krz.lad_akt_von);
	strcpy ((LPSTR) Akt_krz.akt_krz.lad_akv_sa, "9");
	if (Akt_krz.dbreadfirst () == 0)
	{
		cPrEk.Format (_T("%.2lf"), Akt_krz.akt_krz.pr_ek_sa_euro);
		cPrVk.Format (_T("%.2lf"), Akt_krz.akt_krz.pr_vk_sa_euro);
	}
	SetItemText (EditRow, PosPrEkN, cPrEk.GetBuffer ());
	SetItemText (EditRow, PosPrVkN, cPrVk.GetBuffer ());
}

void CNewPrListCtrl::StopEnter ()
{
	CString Text;

	Text = GetItemText (EditRow, PosMdn);
	short OldMdn = _tstoi (Text.GetBuffer ());
//	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	else if (IsWindow (ListDate.m_hWnd))
	{
		ListDate.GetWindowText (Text);
//		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListDate.DestroyWindow ();
	}
	short NewMdn = _tstoi (Text.GetBuffer ());
	if ((EditCol == PosMdn) && (NewMdn == 0 || NewMdn != OldMdn))
	{
		FillList.SetItemText (_T("0"), EditRow, PosFilGr);
		FillList.SetItemText (_T("0 Mandant"), EditRow, PosFil);
	}
}

void CNewPrListCtrl::SetSel (CString& Text)
{

   if (EditCol == PosPrEkN ||
	   EditCol == PosPrVkN ||
	   EditCol == PosDate)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CNewPrListCtrl::FormatText (CString& Text)
{
    if (EditCol == PosPrEk)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
    else if (EditCol == PosPrVk)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
}

void CNewPrListCtrl::NextRow ()
{
/*
	StopEnter ();
	StartEnter (EditCol, EditRow);
*/
	if (!TestDate ()) return;
	GetEnter ();
	CString PrEk = GetItemText (EditRow, PosPrEk);
	CString PrVk = GetItemText (EditRow, PosPrVk);
	CString PrEkN = GetItemText (EditRow, PosPrEkN);
	CString PrVkN = GetItemText (EditRow, PosPrVkN);
	if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0) && 
			(StrToDouble (PrEkN) == 0.0) &&
			(StrToDouble (PrVkN) == 0.0))
	{
		return;
	}

	if ( !TestDate ())
	{
		return;
	}

	int count = GetItemCount ();
	if (EditCol == PosFil)
	{
		ReadFil ();
	}	
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}	
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}

	SetEditText ();
	TestAprIndex ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		if (m_Mdn == 0)
		{
			EditCol = PosMdnGr;
		}
		else
		{
			EditCol = PosFilGr;
		}
	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFil)
		{
			EditRow --;
		}
		if (EditCol == PosFilGr)
		{
			EditRow --;
		}
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CNewPrListCtrl::PriorRow ()
{
	if (!TestDate ()) return;
	BOOL Test = TRUE;
	if (EditRow <= 0)
	{
			return;
	}

/*
	StopEnter ();
	StartEnter (EditCol, EditRow);
*/
	GetEnter ();
	int count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		CString PrEk = GetItemText (EditRow, PosPrEk);
		CString PrVk = GetItemText (EditRow, PosPrVk);
		if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
		{
	        DeleteItem (EditRow);
			Test = FALSE;
		}
	}
//	else
	{
		if (EditRow <= 0)
		{
			return;
		}
		if (EditCol == PosFil)
		{
			ReadFil ();
		}
		else if (EditCol == PosFilGr)
		{
			ReadFilGr ();
		}
		else if (EditCol == PosMdn)
		{
			ReadMdn ();
		}
		else if (EditCol == PosMdnGr)
		{
			ReadMdnGr ();
		}
		if (Test)
		{
			TestAprIndex ();
		}
	}
	StopEnter ();
	EditRow --;
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFil)
		{
			EditRow ++;
		}
		if (EditCol == PosFilGr)
		{
			EditRow ++;
		}
	}
 
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CNewPrListCtrl::LastCol ()
{
	if (EditCol < PosPrVkN) return FALSE;
	return TRUE;
}

void CNewPrListCtrl::OnReturn ()
{

	if (!TestDate ()) return;
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{
		CString PrEk = GetItemText (EditRow, PosPrEk);
		CString PrVk = GetItemText (EditRow, PosPrVk);
		CString PrEkN = GetItemText (EditRow, PosPrEkN);
		CString PrVkN = GetItemText (EditRow, PosPrVkN);
		if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0) && 
			(StrToDouble (PrEkN) == 0.0) &&
			(StrToDouble (PrVkN) == 0.0))
		{
			EditCol --;
			return;
		}
	}
	if (EditCol == PosFil)
	{
		ReadFil ();
	}
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}
	if (LastCol ())
	{
		TestAprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;

		if (EditRow == rowCount)
		{
			if (m_Mdn == 0)
			{
				EditCol = PosMdnGr;
			}
			else
			{
				EditCol = PosFilGr;
			}
		}
		else
		{
			EditCol = PosPrEk;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
/*
		if (EditCol == PosKunName)
		{
			EditCol ++;
		}
*/
	}
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
	if (m_Mdn == 0)
	{
		if (EditCol == PosFilGr)
		{
			EditCol ++;
		}
		if (EditCol == PosFil)
		{
			EditCol ++;
		}
	}
	if (EditCol == PosPrEk)
	{
		EditCol ++;
	}
	if (EditCol == PosPrVk)
	{
		EditCol ++;
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CNewPrListCtrl::NextCol ()
{
	if (!TestDate ()) return;
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	if (EditCol == PosFil)
	{
		ReadFil ();
	}
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}
	StopEnter ();
	EditCol ++;
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFilGr)
		{
			EditCol ++;
		}
		if (EditCol == PosFil)
		{
			EditCol ++;
		}
	}
	if (EditCol == PosPrEk)
	{
		EditCol ++;
	}
	if (EditCol == PosPrVk)
	{
		EditCol ++;
	}
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CNewPrListCtrl::PriorCol ()
{
	if (!TestDate ()) return;
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	if (EditCol == PosFil)
	{
		ReadFil ();
	}
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}
	StopEnter ();
	EditCol --;
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFil)
		{
			EditCol --;
		}
		if (EditCol == PosFilGr)
		{
			EditCol --;
		}
	}
	if (EditCol == PosPrVk)
	{
		EditCol --;
	}
	if (EditCol == PosPrEk)
	{
		EditCol --;
	}
    StartEnter (EditCol, EditRow);
}

BOOL CNewPrListCtrl::InsertRow ()
{
	CString PrEk = GetItemText (EditRow, PosPrEk);
	CString PrVk = GetItemText (EditRow, PosPrVk);
	if ((GetItemCount () > 0) && 
		(StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
	{
		return FALSE;
	}
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CString cMdnGr = _T("0 ");
	CString cMdn   = _T("0 ");
	if (m_Mdn != 0)
	{
		Mdn.mdn.mdn = m_Mdn;
		Mdn.dbreadfirst ();
		cMdnGr.Format (_T("%hd "), Mdn.mdn.mdn_gr);
		cMdn.Format (_T("%hd "), Mdn.mdn.mdn);
	}
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (cMdnGr.GetBuffer (), EditRow, PosMdnGr);
	FillList.SetItemText (cMdn.GetBuffer (), EditRow, PosMdn);
	FillList.SetItemText (_T("0"), EditRow, PosFilGr);
	FillList.SetItemText (_T("0"), EditRow, PosFil);
	FillList.SetItemText (_T("0,00 "), EditRow, PosPrEk);
	FillList.SetItemText (_T("0,00 "), EditRow, PosPrVk);
	FillList.SetItemText (_T("0,00 "), EditRow, PosPrEkN);
	FillList.SetItemText (_T("0,00 "), EditRow, PosPrVkN);
	if (m_Mdn == 0)
	{
			EditCol = PosMdnGr;
	}
	else
	{
			EditCol = PosFilGr;
	}
	StartEnter (EditCol, EditRow);
	return TRUE;
}

BOOL CNewPrListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	return CEditListCtrl::DeleteRow ();
}

BOOL CNewPrListCtrl::AppendEmpty ()
{

	int rowCount = GetItemCount ();
	if (rowCount > 0)
	{
		CString PrEk = GetItemText (EditRow, PosPrEk);
		CString PrVk = GetItemText (EditRow, PosPrVk);
		if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
		{
			return FALSE;
		}
	}
	CString cMdnGr = _T("0 ");
	CString cMdn   = _T("0 ");
	if (m_Mdn != 0)
	{
		Mdn.mdn.mdn = m_Mdn;
		Mdn.dbreadfirst ();
		cMdnGr.Format (_T("%hd "), Mdn.mdn.mdn_gr);
		cMdn.Format (_T("%hd "), Mdn.mdn.mdn);
	}
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText (cMdnGr.GetBuffer (), rowCount, PosMdnGr);
	FillList.SetItemText (cMdn.GetBuffer (), rowCount, PosMdn);
	FillList.SetItemText (_T("0"),  rowCount, PosFilGr);
	FillList.SetItemText (_T("0"),   rowCount, PosFil);
	FillList.SetItemText (_T("0,00 "), rowCount, PosPrEk);
	FillList.SetItemText (_T("0,00 "), rowCount, PosPrVk);
	FillList.SetItemText (_T("0,00 "), rowCount, PosPrEkN);
	FillList.SetItemText (_T("0,00 "), rowCount, PosPrVkN);
	CString cDate;
	CStrFuncs::SysDate (cDate);
	FillList.SetItemText (cDate.GetBuffer (), rowCount, PosDate);
	rowCount = GetItemCount ();
	return TRUE;
}

void CNewPrListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CNewPrListCtrl::FillMdnGrCombo (CVector& Values)
{
	CString *c;
    MdnGrCombo.FirstPosition ();
	while ((c = (CString *) MdnGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnGrCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MdnGrCombo.Add (c);
	}
}


void CNewPrListCtrl::FillMdnCombo (CVector& Values)
{
	CString *c;
    MdnCombo.FirstPosition ();
	while ((c = (CString *) MdnCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MdnCombo.Add (c);
	}
}

void CNewPrListCtrl::FillFilGrCombo (CVector& Values)
{
	CString *c;
    FilGrCombo.FirstPosition ();
	while ((c = (CString *) FilGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilGrCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		FilGrCombo.Add (c);
	}
}

void CNewPrListCtrl::FillFilCombo (CVector& Values)
{
	CString *c;
    FilCombo.FirstPosition ();
	while ((c = (CString *) FilCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		FilCombo.Add (c);
	}
}

void CNewPrListCtrl::RunItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
*/
}

void CNewPrListCtrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CNewPrListCtrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CNewPrListCtrl::OnChoice ()
{
	if (EditCol == PosFil)
	{
		OnFilChoice (CString (_T("")));
	}
	if (EditCol == PosFilGr)
	{
		OnFilGrChoice (CString (_T("")));
	}
	else if (EditCol == PosMdn)
	{
		OnMdnChoice (CString (_T("")));
	}
	else if (EditCol == PosMdnGr)
	{
		OnMdnGrChoice (CString (_T("")));
	}
}

void CNewPrListCtrl::OnFilChoice (CString& Search)
{
	FilChoiceStat = TRUE;
	CString V;
	GetColValue (EditRow, PosMdn, V);
	Mdn.mdn.mdn = _tstoi (V);
	if (ChoiceFil != NULL && !ModalChoiceFil)
	{
		ChoiceFil->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceFil == NULL)
	{
		ChoiceFil = new CChoiceFil (this);
	    ChoiceFil->IsModal = ModalChoiceFil;
		ChoiceFil->m_Mdn = Mdn.mdn.mdn;
		ChoiceFil->CreateDlg ();
	}

    ChoiceFil->SetDbClass (&Fil);
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	ChoiceFil->m_Mdn = _tstoi (Value.GetBuffer ());
	ChoiceFil->SearchText = Search;
	if (ModalChoiceFil)
	{
			ChoiceFil->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceFil->MoveWindow (&rect);
		ChoiceFil->SetFocus ();
		return;
	}
    if (ChoiceFil->GetState ())
    {
		  CFilList *abl = ChoiceFil->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  FilChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		  memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		  Fil.fil.mdn = Mdn.mdn.mdn;
		  Fil.fil.fil = abl->fil;
		  if (Fil.dbreadfirst () == 0)
		  {
			  FilAdr.adr.adr = Fil.fil.adr;
			  FilAdr.dbreadfirst ();
		  }
		  CString Text;
		  Text.Format (_T("%hd"), Fil.fil.fil);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
		  CString KunName;
//		  KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
//		  FillList.SetItemText (KunName.GetBuffer (), EditRow, PosKunName);
    }
	else
	{
		 FilChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}

void CNewPrListCtrl::OnFilGrChoice (CString& Search)
{
	CString V;
	GetColValue (EditRow, PosMdn, V);
	Mdn.mdn.mdn = _tstoi (V);
	FilGrChoiceStat = TRUE;
	if (ChoiceFilGr != NULL && !ModalChoiceFilGr)
	{
		ChoiceFilGr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceFilGr == NULL)
	{
		ChoiceFilGr = new CChoiceGrZuord (this);
	    ChoiceFilGr->IsModal = ModalChoiceFilGr;
	    ChoiceFilGr->m_Mdn = Mdn.mdn.mdn;
		ChoiceFilGr->CreateDlg ();
	}

    ChoiceFilGr->SetDbClass (&Fil);
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	ChoiceFilGr->m_Mdn = _tstoi (Value.GetBuffer ());
	ChoiceFilGr->SearchText = Search;
	if (ModalChoiceFilGr)
	{
			ChoiceFilGr->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceFil->MoveWindow (&rect);
		ChoiceFil->SetFocus ();
		return;
	}
    if (ChoiceFilGr->GetState ())
    {
		  CGrZuordList *abl = ChoiceFilGr->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  FilGrChoiceStat = FALSE;
			  return;
		  }
		  CString Text;
		  Text.Format (_T("%hd"), abl->gr);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
		 FilGrChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}


void CNewPrListCtrl::OnMdnChoice (CString& Search)
{
	MdnChoiceStat = TRUE;
	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

    ChoiceMdn->SetDbClass (&Fil);
//	ChoiceFil->m_Mdn = m_Mdn;
	ChoiceMdn->SearchText = Search;
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  MdnChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  CString Text;
		  Text.Format (_T("%hd"), Mdn.mdn.mdn);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
//		  CString KunName;
//		  KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
//		  FillList.SetItemText (KunName.GetBuffer (), EditRow, PosKunName);
    }
	else
	{
		 MdnChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}

void CNewPrListCtrl::OnMdnGrChoice (CString& Search)
{
	MdnGrChoiceStat = TRUE;
	if (ChoiceMdnGr != NULL && !ModalChoiceMdnGr)
	{
		ChoiceMdnGr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdnGr == NULL)
	{
		ChoiceMdnGr = new CChoiceGrZuord (this);
	    ChoiceMdnGr->IsModal = ModalChoiceMdnGr;
		ChoiceMdnGr->CreateDlg ();
	}

    ChoiceMdnGr->SetDbClass (&Mdn);
	ChoiceMdnGr->SearchText = Search;
	if (ModalChoiceMdnGr)
	{
			ChoiceMdnGr->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdnGr->MoveWindow (&rect);
		ChoiceMdnGr->SetFocus ();
		return;
	}
    if (ChoiceMdnGr->GetState ())
    {
		  CGrZuordList *abl = ChoiceMdnGr->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  MdnChoiceStat = FALSE;
			  return;
		  }
		  CString Text;
		  Text.Format (_T("%hd"), abl->gr);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
		 MdnGrChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}

void CNewPrListCtrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}

void CNewPrListCtrl::ReadMdnGr ()
{
	if (EditCol != PosMdnGr) return;
    memcpy (&Gr_zuord.gr_zuord, &gr_zuord_null, sizeof (GR_ZUORD));
    CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnMdnGrChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!MdnGrChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Gr_zuord.gr_zuord.mdn = 0;
	Gr_zuord.gr_zuord.gr = atoi (Text);
	if (Gr_zuord.gr_zuord.gr != 0)
	{
		if (Gr_zuord.dbreadfirst () != 0 && Gr_zuord.gr_zuord.gr != 0l)
		{
			MessageBox (_T("Mandantengruppe nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr, Gr_zuord.gr_zuord.gr_bz1);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
	else
	{
		Text = _T("0");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text = _T("0");
        SetItemText (EditRow, PosMdnGr, Text.GetBuffer ());
	}
	ReadApr ();
}

void CNewPrListCtrl::ReadMdn ()
{
	if (EditCol != PosMdn) return;
    memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
    CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnMdnChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!MdnChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Mdn.mdn.mdn = atoi (Text);
	if (Mdn.mdn.mdn != 0)
	{
		if (Mdn.dbreadfirst () == 0)
		{
			MdnAdr.adr.adr = Mdn.mdn.adr;
			MdnAdr.dbreadfirst ();
		}
		else if (Mdn.mdn.mdn != 0l)
		{
			MessageBox (_T("Mandant nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Mdn.mdn.mdn, MdnAdr.adr.adr_krz);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text.Format (_T("%hd"), Mdn.mdn.mdn_gr);
        SetItemText (EditRow, PosMdnGr, Text.GetBuffer ());
	}
	else
	{
		Text = _T("0  Zentrale");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
	ReadApr ();
}

void CNewPrListCtrl::ReadFilGr ()
{
	if (EditCol != PosFilGr) return;
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	if (_tstoi (Value) == 0) return;

    memcpy (&Gr_zuord.gr_zuord, &gr_zuord_null, sizeof (GR_ZUORD));
    CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnFilGrChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!FilGrChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Gr_zuord.gr_zuord.mdn = _tstoi (Value);
	Gr_zuord.gr_zuord.gr = atoi (Text);
	if (Gr_zuord.gr_zuord.gr != 0)
	{
		if (Gr_zuord.dbreadfirst () != 0 && Gr_zuord.gr_zuord.gr != 0l)
		{
			MessageBox (_T("Mandantengruppe nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr, Gr_zuord.gr_zuord.gr_bz1);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
	else
	{
		Text = _T("0");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text = _T("0");
        SetItemText (EditRow, PosFilGr, Text.GetBuffer ());
	}
	ReadApr ();
}


void CNewPrListCtrl::ReadFil ()
{
	if (EditCol != PosFil) return;
    memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
	CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnMdnChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!FilChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Fil.fil.fil = atoi (Text);
	Text = GetItemText (EditRow, PosMdn);
    Fil.fil.mdn = atoi (Text);
//	if (Fil.fil.mdn == 0l) return;

    CString FilName;
	if (Fil.fil.fil != 0)
	{
		if (Fil.dbreadfirst () == 0)
		{
		  FilAdr.adr.adr = Fil.fil.adr;
		  FilAdr.dbreadfirst ();
		}
		else if (Fil.fil.fil != 0l)
		{
			MessageBox (_T("Filiale nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Fil.fil.fil, FilAdr.adr.adr_krz);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text.Format (_T("%hd"), Fil.fil.fil_gr);
        SetItemText (EditRow, PosFilGr, Text.GetBuffer ());
	}
	else 
	{
		Text = _T("0  Mandant");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
	ReadApr ();
}

void CNewPrListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	if (col == PosMdn)
	{
		Text = cText.Tokenize (" ", pos);
	}
	else if (col == PosFil)
	{
		Text = cText.Tokenize (" ", pos);
	}
	else
	{
		Text = cText.Trim ();
	}
}

void CNewPrListCtrl::TestAprIndex ()
{
	int Items = GetItemCount ();
	if (Items <= 1) return;
	CString Value;
	GetColValue (EditRow, PosMdnGr, Value);
	short sMdnGr = _tstoi (Value.GetBuffer ());
	GetColValue (EditRow, PosMdn, Value);
	short sMdn = _tstoi (Value.GetBuffer ());
	GetColValue (EditRow, PosFilGr, Value);
	short sFilGr = _tstoi (Value.GetBuffer ());
	GetColValue (EditRow, PosFil, Value);
	short sFil = _tstoi (Value.GetBuffer ());
	GetColValue (EditRow, PosDate, Value);
	CString sDate = Value;
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosMdnGr, Value);
		short lMdnGr = _tstoi (Value.GetBuffer ());
		GetColValue (i, PosMdn, Value);
		short lMdn = _tstoi (Value.GetBuffer ());
		GetColValue (i, PosFilGr, Value);
		short lFilGr = _tstoi (Value.GetBuffer ());
		GetColValue (i, PosFil, Value);
		short lFil = _tstoi (Value.GetBuffer ());
		GetColValue (i, PosDate, Value);
		CString lDate = Value;
		if (lFil == sFil && lFilGr == sFilGr &&
			lMdn == sMdn && lMdnGr == sMdnGr &&
			lDate == sDate)
		{
			INT_PTR ret = MessageBox (_T("Ein Eintrag auf dieser Unternehmenseben existiert bereits")
				        _T("Eintrag l�schen ?"),
						NULL,
						MB_YESNO | MB_ICONQUESTION); 
            if (ret != IDYES) return;
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
			return;
		}

	}
}

BOOL CNewPrListCtrl::TestAprIndexM (int EditRow)
{
	int Items = GetItemCount ();
	if (Items <= 1) return TRUE;

	GetEnter ();
	CString PrEk;
	GetColValue (EditRow, PosPrEk, PrEk);
	CString PrVk;
	GetColValue (EditRow, PosPrVk, PrVk);
	double pr_ek = CStrFuncs::StrToDouble (PrEk);
	double pr_vk = CStrFuncs::StrToDouble (PrVk);
	if (pr_ek == 0.0 && pr_vk == 0.0) return TRUE;

	CString Value;
	GetColValue (EditRow, PosMdnGr, Value);
	short sMdnGr = _tstoi (Value.GetBuffer ());
	GetColValue (EditRow, PosMdn, Value);
	short sMdn = _tstoi (Value.GetBuffer ());
	GetColValue (EditRow, PosFilGr, Value);
	short sFilGr = _tstoi (Value.GetBuffer ());
	GetColValue (EditRow, PosFil, Value);
	short sFil = _tstoi (Value.GetBuffer ());
	GetColValue (EditRow, PosDate, Value);
	CString sDate = Value;
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;

		GetColValue (i, PosPrEk, PrEk);
		GetColValue (i, PosPrVk, PrVk);
		double pr_ek = CStrFuncs::StrToDouble (PrEk);
		double pr_vk = CStrFuncs::StrToDouble (PrVk);
		if (pr_ek == 0.0 && pr_vk == 0.0) continue;

		GetColValue (i, PosMdnGr, Value);
		short lMdnGr = _tstoi (Value.GetBuffer ());
		GetColValue (i, PosMdn, Value);
		short lMdn = _tstoi (Value.GetBuffer ());
		GetColValue (i, PosFilGr, Value);
		short lFilGr = _tstoi (Value.GetBuffer ());
		GetColValue (i, PosFil, Value);
		short lFil = _tstoi (Value.GetBuffer ());
		GetColValue (i, PosDate, Value);
		CString lDate = Value;
		if (lFil == sFil && lFilGr == sFilGr &&
			lMdn == sMdn && lMdnGr == sMdnGr &&
			lDate == sDate)
		{
			return FALSE;
		}

	}
	return TRUE;
}

void CNewPrListCtrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}

void CNewPrListCtrl::ReadFilGrCombo (int mdn)
{

	Gr_zuord.gr_zuord.mdn = mdn;
	if (Gr_zuord.gr_zuord.mdn == 0) return;

	CVector Values;
	Values.Init ();
	CString *V = new CString ();
	V->Format (_T("0"));
	Values.Add (V);
    Gr_zuord.sqlopen (FilGrCursor);
	while (Gr_zuord.sqlfetch (FilGrCursor) == 0)
	{
		Gr_zuord.dbreadfirst ();
		V = new CString ();
		V->Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr,
		                          Gr_zuord.gr_zuord.gr_bz1);
		Values.Add (V);
	}
	FillFilGrCombo (Values);
}

void CNewPrListCtrl::FillFilGrCombo (int row)
{

	CString Value;
	GetColValue (row, PosMdn, Value);
	ReadFilGrCombo (_tstoi (Value));
/*
	Gr_zuord.gr_zuord.mdn = _tstoi (Value);
	if (Gr_zuord.gr_zuord.mdn == 0) return;

	CVector Values;
	Values.Init ();
	CString *V = new CString ();
	V->Format (_T("0"));
	Values.Add (V);
    Gr_zuord.sqlopen (FilGrCursor);
	while (Gr_zuord.sqlfetch (FilGrCursor) == 0)
	{
		Gr_zuord.dbreadfirst ();
		V = new CString ();
		V->Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr,
		                          Gr_zuord.gr_zuord.gr_bz1);
		Values.Add (V);
	}
	FillFilGrCombo (Values);
*/
}

void CNewPrListCtrl::ReadFilCombo (int mdn)
{
	Fil.fil.mdn = mdn;
	if (Fil.fil.mdn == 0) return;
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Mandant"));
	Values.Add (Value);
    Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		Fil.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Fil.fil.fil,
			                          FilAdr.adr.adr_krz);
		Values.Add (Value);
	}
	FillFilCombo (Values);
}


void CNewPrListCtrl::FillFilCombo (int row)
{
	CString V;
	GetColValue (row, PosMdn, V);
	ReadFilCombo (_tstoi (V));
/*
	Fil.fil.mdn = _tstoi (V);
	if (Fil.fil.mdn == 0) return;
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Mandant"));
	Values.Add (Value);
    Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		Fil.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Fil.fil.fil,
			                          FilAdr.adr.adr_krz);
		Values.Add (Value);
	}
	FillFilCombo (Values);
*/
}


void CNewPrListCtrl::Prepare ()
{
	Gr_zuord.sqlin  ((short *) &Gr_zuord.gr_zuord.mdn, SQLSHORT, 0);
	Gr_zuord.sqlout ((short *) &Gr_zuord.gr_zuord.gr, SQLSHORT, 0);
	Gr_zuord.sqlout ((char *) Gr_zuord.gr_zuord.gr_bz1, SQLCHAR, sizeof (Gr_zuord.gr_zuord.gr_bz1));
    FilGrCursor = Gr_zuord.sqlcursor (_T("select gr, gr_bz1 from gr_zuord ")
 									  _T("where mdn = ? ")
									  _T("and gr > 0 ")
								      _T("order by gr"));

	Fil.sqlin ((short *) &Fil.fil.mdn, SQLSHORT, 0);
	Fil.sqlout ((short *) &Fil.fil.fil, SQLSHORT, 0);
	FilAdr.sqlout ((char *) &FilAdr.adr.adr_krz, SQLCHAR, sizeof (FilAdr.adr.adr_krz));
    FilCursor = Fil.sqlcursor (_T("select fil.fil, adr.adr_krz from fil, adr ")
		                       _T("where fil.mdn = ? ")
							   _T("and fil.fil > 0 ")
							   _T("and adr.adr = fil.adr ")
							   _T("order by fil.fil"));

}

BOOL CNewPrListCtrl::TestDate ()
{
	if (EditCol != PosDate)
	{
		return TRUE;
	}
	if (!IsWindow (ListDate.m_hWnd))
	{
		return TRUE;
	}

	CTime Date;

	ListDate.GetTime (Date);
	__time64_t date = Date.GetTime () / (24 * 3600);
	int dYear  = Date.GetYear ();
	int dMonth = Date.GetMonth ();
	int dDay   = Date.GetDay ();

	CString cSysDate; 
	CStrFuncs::SysDate (cSysDate);  
 	int day = _tstoi (cSysDate.Left (2).GetBuffer ());
	int month = _tstoi (cSysDate.Mid (3, 2).GetBuffer ());
	int year = _tstoi (cSysDate.Right (4).GetBuffer ());
	CTime SysDate (year, month, day, 0, 0, 0);
   
//	CTime SysDate = CTime::GetCurrentTime ();

	__time64_t sysdate = SysDate.GetTime () / (24 * 3600);
	int sYear  = SysDate.GetYear ();
	int sMonth = SysDate.GetMonth ();
	int sDay   = SysDate.GetDay ();
       
	if (date < sysdate)
	{
		AfxMessageBox (_T("Das Datum ist kleiner als das Tagesdatum"),MB_OK | MB_ICONERROR, 0);
		return FALSE;
	}
	return TRUE;
}

BOOL CNewPrListCtrl::TestDate (int Row)
{

	CString cDate;
	GetColValue (Row, PosDate, cDate);
 	int day = _tstoi (cDate.Left (2).GetBuffer ());
	int month = _tstoi (cDate.Mid (3, 2).GetBuffer ());
	int year = _tstoi (cDate.Right (4).GetBuffer ());
	CTime Date (year, month, day, 0, 0, 0);
	__time64_t date = Date.GetTime () / (24 * 3600);

	CString cSysDate; 
	CStrFuncs::SysDate (cSysDate);  
 	day = _tstoi (cSysDate.Left (2).GetBuffer ());
	month = _tstoi (cSysDate.Mid (3, 2).GetBuffer ());
	year = _tstoi (cSysDate.Right (4).GetBuffer ());
	CTime SysDate (year, month, day, 0, 0, 0);
   
//	CTime SysDate = CTime::GetCurrentTime ();

	__time64_t sysdate = SysDate.GetTime () / (24 * 3600);
	int sYear  = SysDate.GetYear ();
	int sMonth = SysDate.GetMonth ();
	int sDay   = SysDate.GetDay ();
       
	if (date < sysdate)
	{
		AfxMessageBox (_T("Das Datum ist kleiner als das Tagesdatum"),MB_OK | MB_ICONERROR, 0);
		return FALSE;
	}
	return TRUE;
}

void CNewPrListCtrl::SetListMode (int ListMode)
{
	if (ListMode != Compact && ListMode != Large)
	{
		return;
	}
	this->ListMode = ListMode;
	switch (ListMode)
	{
		case Large :
			CompanyWidth = LargeWidth;
			break;
		case Compact :
			CompanyWidth = CompactWidth;
			break;
	}
}