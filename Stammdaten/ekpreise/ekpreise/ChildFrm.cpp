// ChildFrm.cpp : Implementierung der Klasse CChildFrame
//
#include "stdafx.h"
#include "AprMan.h"
#include "Apr1.h"
#include "MainFrm.h"

#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RECCHANGE                       WM_USER + 2000

// CChildFrame

CVector CChildFrame::StdPrWnd;
CVector CChildFrame::NewPrWnd;
CVector CChildFrame::ArtAktWnd;

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	ON_WM_SIZE ()
	ON_WM_DESTROY ()
	ON_COMMAND (RECCHANGE, OnRecChange)
END_MESSAGE_MAP()


// CChildFrame Erstellung/Zerst�rung


CChildFrame::CChildFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
	InitSize = FALSE;
}

CChildFrame::~CChildFrame()
{
}


BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie die Fensterklasse oder die Stile hier, indem Sie CREATESTRUCT �ndern
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}

BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	lpcs->cy = 500;
	BOOL ret = CMDIChildWnd::OnCreateClient(lpcs,   pContext);	

	CStringA ClassName = pContext->m_pNewViewClass->m_lpszClassName;
	if (ClassName == "CApr1")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->StdPrWnd == NULL)
		{
			MainFrm->StdPrWnd = this;
		}
		else
		{
			StdPrWnd.Add (this);
		}
	}
	else if (ClassName == "CApr2")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->NewPrWnd == NULL)
		{
			MainFrm->NewPrWnd = this;
		}
		else
		{
			NewPrWnd.Add (this);
		}
	}
	else if (ClassName == "CApr3")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->ArtAktWnd == NULL)
		{
			MainFrm->ArtAktWnd = this;
		}
		else
		{
			ArtAktWnd.Add (this);
		}
	}
	MDIMaximize ();
	
	return ret;
}



// CChildFrame Diagnose

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// CChildFrame Meldungshandler

void CChildFrame::OnSize(UINT nType, int cx, int cy)
{

	CMDIChildWnd::OnSize (nType, cx, cy);
	if (!InitSize)
	{
		    CRect pRect;
			GetParent ()->GetClientRect (pRect);
			CRect rect;
			GetWindowRect (&rect);
			GetParent ()->ScreenToClient (&rect);
			rect.bottom = pRect.bottom;
			rect.right += 55;
			rect.top = 0;
			InitSize = TRUE;
			MoveWindow (&rect, TRUE);
	}
}

void CChildFrame::OnDestroy()
{

	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	if (MainFrm->StdPrWnd == this)
	{
		MainFrm->StdPrWnd = NULL;
		if (StdPrWnd.GetCount () != 0)
		{
			MainFrm->StdPrWnd = (CWnd *) StdPrWnd.Get (0);
			StdPrWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}

	else if (MainFrm->NewPrWnd == this)
	{
		MainFrm->NewPrWnd = NULL;
		if (NewPrWnd.GetCount () != 0)
		{
			MainFrm->NewPrWnd = (CWnd *) NewPrWnd.Get (0);
			NewPrWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	else if (MainFrm->ArtAktWnd == this)
	{
		MainFrm->ArtAktWnd = NULL;
		if (ArtAktWnd.GetCount () != 0)
		{
			MainFrm->ArtAktWnd = (CWnd *) ArtAktWnd.Get (0);
			ArtAktWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}

	int i = -1;
	if ((i = StdPrWnd.Find (this)) != -1)
	{
		StdPrWnd.Drop (i);
		InitSize = FALSE;
	}
	else if ((i = NewPrWnd.Find (this)) != -1)
	{
		NewPrWnd.Drop (i);
		InitSize = FALSE;
	}
	else if ((i = ArtAktWnd.Find (this)) != -1)
	{
		ArtAktWnd.Drop (i);
		InitSize = FALSE;
	}
}

void CChildFrame::OnRecChange()
{
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	if (MainFrm->StdPrWnd != NULL)
	{
		CApr1 *view = (CApr1 *) ((CChildFrame *) MainFrm->StdPrWnd)->GetActiveView ();
		if (view != NULL)
		{
			view->OnRecChange ();
		}
	}
}
