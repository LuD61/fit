#ifndef _A_KALK_MAT_DEF
#define _A_KALK_MAT_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_KALK_MAT {
   double         a;
   short          bearb_sk;
   short          delstatus;
   short          fil;
   double         hk_teilk;
   double         hk_vollk;
   double         kost;
   double         mat_o_b;
   short          mdn;
   double         sp_hk;
   DATE_STRUCT    dat;
};
extern struct A_KALK_MAT a_kalk_mat, a_kalk_mat_null;

#line 8 "a_kalk_mat.rh"

class A_KALK_MAT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_KALK_MAT a_kalk_mat;  
               A_KALK_MAT_CLASS () : DB_CLASS ()
               {
               }
};
#endif
