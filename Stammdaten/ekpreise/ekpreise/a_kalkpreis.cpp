#include "stdafx.h"
#include "a_kalkpreis.h"

struct A_KALKPREIS a_kalkpreis, a_kalkpreis_null, a_kalkpreis_def;

void A_KALKPREIS_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)    &a_kalkpreis.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalkpreis.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalkpreis.a,   SQLDOUBLE, 0);
    sqlout ((long *) &a_kalkpreis.mdn,SQLLONG,0);
    sqlout ((short *) &a_kalkpreis.fil,SQLSHORT,0);
    sqlout ((double *) &a_kalkpreis.a,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkpreis.mat_o_b,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkpreis.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkpreis.hk_teilk,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &a_kalkpreis.dat,SQLDATE,0);
    sqlout ((TCHAR *) a_kalkpreis.zeit,SQLCHAR,6);
    sqlout ((double *) &a_kalkpreis.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkpreis.fil_ek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkpreis.fil_vk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkpreis.sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkpreis.fil_ek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkpreis.fil_vk_teilk,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kalkpreis.programm,SQLCHAR,13);
    sqlout ((TCHAR *) a_kalkpreis.version,SQLCHAR,13);
    sqlout ((double *) &a_kalkpreis.kost,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select a_kalkpreis.mdn,  ")
_T("a_kalkpreis.fil,  a_kalkpreis.a,  a_kalkpreis.mat_o_b,  ")
_T("a_kalkpreis.hk_vollk,  a_kalkpreis.hk_teilk,  a_kalkpreis.dat,  ")
_T("a_kalkpreis.zeit,  a_kalkpreis.sk_vollk,  a_kalkpreis.fil_ek_vollk,  ")
_T("a_kalkpreis.fil_vk_vollk,  a_kalkpreis.sk_teilk,  ")
_T("a_kalkpreis.fil_ek_teilk,  a_kalkpreis.fil_vk_teilk,  ")
_T("a_kalkpreis.programm,  a_kalkpreis.version,  a_kalkpreis.kost from a_kalkpreis ")

#line 14 "a_kalkpreis.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
    sqlin ((long *) &a_kalkpreis.mdn,SQLLONG,0);
    sqlin ((short *) &a_kalkpreis.fil,SQLSHORT,0);
    sqlin ((double *) &a_kalkpreis.a,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.hk_teilk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_kalkpreis.dat,SQLDATE,0);
    sqlin ((TCHAR *) a_kalkpreis.zeit,SQLCHAR,6);
    sqlin ((double *) &a_kalkpreis.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.fil_ek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.fil_vk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.fil_ek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.fil_vk_teilk,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kalkpreis.programm,SQLCHAR,13);
    sqlin ((TCHAR *) a_kalkpreis.version,SQLCHAR,13);
    sqlin ((double *) &a_kalkpreis.kost,SQLDOUBLE,0);
            sqltext = _T("update a_kalkpreis set ")
_T("a_kalkpreis.mdn = ?,  a_kalkpreis.fil = ?,  a_kalkpreis.a = ?,  ")
_T("a_kalkpreis.mat_o_b = ?,  a_kalkpreis.hk_vollk = ?,  ")
_T("a_kalkpreis.hk_teilk = ?,  a_kalkpreis.dat = ?,  ")
_T("a_kalkpreis.zeit = ?,  a_kalkpreis.sk_vollk = ?,  ")
_T("a_kalkpreis.fil_ek_vollk = ?,  a_kalkpreis.fil_vk_vollk = ?,  ")
_T("a_kalkpreis.sk_teilk = ?,  a_kalkpreis.fil_ek_teilk = ?,  ")
_T("a_kalkpreis.fil_vk_teilk = ?,  a_kalkpreis.programm = ?,  ")
_T("a_kalkpreis.version = ?,  a_kalkpreis.kost = ? ")

#line 18 "a_kalkpreis.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?");
            sqlin ((short *)    &a_kalkpreis.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalkpreis.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalkpreis.a,   SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)    &a_kalkpreis.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalkpreis.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalkpreis.a,   SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_kalkpreis ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
            sqlin ((short *)    &a_kalkpreis.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalkpreis.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalkpreis.a,   SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_kalkpreis set a = a ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
            sqlin ((short *)    &a_kalkpreis.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalkpreis.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalkpreis.a,   SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_kalkpreis ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
    sqlin ((long *) &a_kalkpreis.mdn,SQLLONG,0);
    sqlin ((short *) &a_kalkpreis.fil,SQLSHORT,0);
    sqlin ((double *) &a_kalkpreis.a,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.mat_o_b,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.hk_teilk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_kalkpreis.dat,SQLDATE,0);
    sqlin ((TCHAR *) a_kalkpreis.zeit,SQLCHAR,6);
    sqlin ((double *) &a_kalkpreis.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.fil_ek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.fil_vk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.fil_ek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkpreis.fil_vk_teilk,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kalkpreis.programm,SQLCHAR,13);
    sqlin ((TCHAR *) a_kalkpreis.version,SQLCHAR,13);
    sqlin ((double *) &a_kalkpreis.kost,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into a_kalkpreis (")
_T("mdn,  fil,  a,  mat_o_b,  hk_vollk,  hk_teilk,  dat,  zeit,  sk_vollk,  fil_ek_vollk,  ")
_T("fil_vk_vollk,  sk_teilk,  fil_ek_teilk,  fil_vk_teilk,  programm,  version,  ")
_T("kost) ")

#line 48 "a_kalkpreis.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?)"));

#line 50 "a_kalkpreis.rpp"
}
