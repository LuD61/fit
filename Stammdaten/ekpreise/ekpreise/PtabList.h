#ifndef _PTAB_LIST_DEF
#define _PTAB_LIST_DEF
#pragma once

class CPtabList
{
public:
	CString ptwert;
	CString ptbez;
	CString ptbezk;
	CString ptwer1;
	CString ptwer2;
	CPtabList(void);
	CPtabList(LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR);
	~CPtabList(void);
};
#endif
