#ifndef _SYS_BEN_DEF
#define _SYS_BEN_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct SYS_BEN {
   short          berecht;
   TCHAR          dir_spr_vor[2];
   TCHAR          dir_spr_zur[2];
   short          fil;
   short          mdn;
   TCHAR          pers[13];
   TCHAR          pers_nam[9];
   TCHAR          pers_passwd[13];
   long           zahl;
   TCHAR          ben_txt[33];
};
extern struct SYS_BEN sys_ben, sys_ben_null;

#line 8 "Sys_ben.rh"

class SYS_BEN_CLASS : public DB_CLASS 
{
       private :
               int cursorpers_nam; 
               void prepare (void);
       public :
               SYS_BEN sys_ben;
               SYS_BEN_CLASS () : DB_CLASS ()
               {
			cursorpers_nam = -1;
               }
               int dbreadfirstpers_nam ();
               int dbreadpers_nam ();
};
#endif
