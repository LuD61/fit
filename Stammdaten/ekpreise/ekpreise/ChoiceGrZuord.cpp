#include "stdafx.h"
#include "ChoiceGrZuord.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceGrZuord::Sort1 = -1;
int CChoiceGrZuord::Sort2 = -1;
int CChoiceGrZuord::Sort3 = -1;

CChoiceGrZuord::CChoiceGrZuord(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_Mdn = 0;
}

CChoiceGrZuord::~CChoiceGrZuord() 
{
	DestroyList ();
}

void CChoiceGrZuord::DestroyList() 
{
	for (std::vector<CGrZuordList *>::iterator pabl = GrZuordList.begin (); pabl != GrZuordList.end (); ++pabl)
	{
		CGrZuordList *abl = *pabl;
		delete abl;
	}
    GrZuordList.clear ();
}

void CChoiceGrZuord::FillList () 
{
    short  gr;
    TCHAR gr_bz1 [25];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Gruppen"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Gruppe"),  1, 50, LVCFMT_RIGHT);
    SetCol (_T("Name"),  2, 250);

	if (GrZuordList.size () == 0)
	{
		DbClass->sqlin ((short *)&m_Mdn,      SQLSHORT, 0);
		DbClass->sqlout ((short *)&gr,      SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  gr_bz1,   SQLCHAR, sizeof (gr_bz1));
		int cursor = DbClass->sqlcursor (_T("select gr, gr_bz1 ")
			                             _T("from gr_zuord ")
										 _T("where mdn = ? ")
										 _T("and gr > 0 order by gr"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) gr_bz1;
			CDbUniCode::DbToUniCode (gr_bz1, pos);
			CGrZuordList *abl = new CGrZuordList (gr, gr_bz1);
			GrZuordList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CGrZuordList *>::iterator pabl = GrZuordList.begin (); pabl != GrZuordList.end (); ++pabl)
	{
		CGrZuordList *abl = *pabl;
		CString Gr;
		Gr.Format (_T("%hd"), abl->gr); 
		CString Num;
		CString Bez;
		_tcscpy (gr_bz1, abl->gr_bz1.GetBuffer ());

		CString LText;
		LText.Format (_T("%hd %s"), abl->gr, 
									abl->gr_bz1.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Gr;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (gr_bz1, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort (listView);
}


void CChoiceGrZuord::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceGrZuord::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceGrZuord::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceGrZuord::SearchGrBz1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceGrZuord::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchGrBz1 (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceGrZuord::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceGrZuord::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   return 0;
}


void CChoiceGrZuord::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CGrZuordList *abl = GrZuordList [i];
		   
		   abl->gr     = _tstoi (ListBox->GetItemText (i, 1));
		   abl->gr_bz1 = ListBox->GetItemText (i, 2);
	}
}

void CChoiceGrZuord::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = GrZuordList [idx];
}

CGrZuordList *CChoiceGrZuord::GetSelectedText ()
{
	CGrZuordList *abl = (CGrZuordList *) SelectedRow;
	return abl;
}

