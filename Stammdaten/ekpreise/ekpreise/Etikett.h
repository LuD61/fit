#pragma once
#include "Reg_eti.h"
#include "mo_auto.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "a_eig.h"
#include "Mdn.h"
#include "Fil.h"
#include "Smt.h"
//#include "Ptabn.h"
#include "sys_par.h"
#include "DllPreise.h"
#include "A_pr.h"
#include "vector"

class CEtikett
{
protected:
    REG_ETI reg_eti_pr; 
	CString Sysdate;
	int MdnCursor;
	int FilCursor;
	int InsertPrCursor;
	void Prepare ();
	short StartMdn;
	short StartFil;
	BOOL TestCompany;
	double vk_gr; 
	short vk_kz;

public:
	enum
	{
		No = 0,
		HndStk = 1,
		Gew = 2,
		HndPack = 3,
	};

    enum
	{
		Hndw = 1,
		Eig = 2,
	};

	enum
	{
		Off = 0,
		Save = 1,
		AskForPrint = 2,
		Print = 3,
	};

	enum
	{
		Auto = 0,
		Pr100g = 1,
		PrKg = 2,
	};

    double GewAuto;

	SYS_PAR_CLASS Sys_par;
//	PTABN_CLASS Ptabn;
	REG_ETI_CLASS Reg_eti;
	AUTO_CLASS AutoNr;
	A_BAS_CLASS *A_bas;
	A_HNDW_CLASS *A_hndw;
	A_EIG_CLASS *A_eig;
	BOOL DeleteA_hndw;
	BOOL DeleteA_eig;
	std::vector<REG_ETI> EtiTab;

	MDN_CLASS Mdn;
	FIL_CLASS Fil;
	APR_CLASS A_pr;
	CSmt Smt;
	CDllPreise DllPreise;
	int Mode;
	int Type;

	CString ErfKz;
	short EtiType;
	CEtikett(void);
	~CEtikett(void);
    BOOL Write ();
    BOOL WritePr (short mdn, short fil, double pr_vk);
    void WriteMdn ();
    void WriteFil ();
	void Clear ();
	void TestPrint ();
	void RunPrint ();
	void RunPrintWithPr (CString& Date);
	void RePrint (CString& Where, CString& Date); 
	void RunPrintFromStm (CString& Where, CString& Date);
    void StmWriteMdn ();
    void StmWriteFil ();
	void SortEtiTab ();
	static int _cdecl Compare (const void * elem1, const void *elem2);
};
