#ifndef _A_IV_PR_DEF
#define _A_IV_PR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct IV_PR {
   short          mdn;
   long           pr_gr_stuf;
   long           kun_pr;
   double         a;
   double         vk_pr_i;
   double         ld_pr;
   DATE_STRUCT    gue_ab;
   short          waehrung;
   double         vk_pr_eu;
   double         ld_pr_eu;
   long	          kun;
};
extern struct IV_PR iv_pr, iv_pr_null;

class IV_PR_CLASS : public DB_CLASS 
{
//       private :
//               void prepare (void);
       public :
               void prepare (void);
               IV_PR iv_pr;
               IV_PR_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (IV_PR&);  
};
#endif
