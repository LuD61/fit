// AprMan.h : Hauptheaderdatei f�r die AprMan-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error 'stdafx.h' muss vor dieser Datei in PCH eingeschlossen werden.
#endif

#include "resource.h"       // Hauptsymbole


// CAprManApp:
// Siehe AprMan.cpp f�r die Implementierung dieser Klasse
//

class CAprManApp : public CWinApp
{
public:
	CAprManApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnStdPr();
	afx_msg void OnNewPr();
	afx_msg void OnArtAkt();
};

extern CAprManApp theApp;
