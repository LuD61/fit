#include "stdafx.h"
#include "ek_prpr.h"

struct EK_PRPR ek_prpr, ek_prpr_null;

void EK_PRPR_CLASS::prepare (void)
{
/* ----> Lesen nicht notwendig , Auswertung sp�ter per Liste ?! 
	TCHAR *sqltext;

       sqlin ((short *)   &ek_pr.mdn, SQLSHORT, 0);
       sqlin ((double *)  &ek_pr.a, SQLDOUBLE, 0);
    sqlout ((double *) &ek_pr.a,SQLDOUBLE,0);
    sqlout ((short *) &ek_pr.fil,SQLSHORT,0);
    sqlout ((short *) &ek_pr.mdn,SQLSHORT,0);
    sqlout ((short *) &ek_pr.akt,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &ek_pr.bearb,SQLDATE,0);
    sqlout ((double *) &ek_pr.pr_ek,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.hk_kost,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.hk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.sk_aufs,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.sk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_ek_aufs,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_ek_kalk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_ek,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_vk_aufs,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_vk_kalk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.fil_vk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh1_aufs,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh1_kalk_vk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh1,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh1_sp,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh2_aufs,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh2_kalk_vk,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh2,SQLDOUBLE,0);
    sqlout ((double *) &ek_pr.gh2_sp,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select ek_pr.a,  ek_pr.fil,  ")
_T("ek_pr.mdn,  ek_pr.akt,  ek_pr.bearb,  ek_pr.pr_ek,  ek_pr.hk_kost,  ")
_T("ek_pr.hk,  ek_pr.sk_aufs,  ek_pr.sk,  ek_pr.fil_ek_aufs,  ")
_T("ek_pr.fil_ek_kalk,  ek_pr.fil_ek,  ek_pr.fil_vk_aufs,  ")
_T("ek_pr.fil_vk_kalk,  ek_pr.fil_vk,  ek_pr.gh1_aufs,  ek_pr.gh1_kalk_vk,  ")
_T("ek_pr.gh1,  ek_pr.gh1_sp,  ek_pr.gh2_aufs,  ek_pr.gh2_kalk_vk,  ")
_T("ek_pr.gh2,  ek_pr.gh2_sp from ek_pr ")

                                  _T("where mdn = ? ")
				  _T("and a = ?"));
		
< ------- */

/* -----> update nicht notwendig, immer INSERT ins Protokoll

	sqlin ((double *) &ek_prpr.a,SQLDOUBLE,0);
    sqlin ((short *) &ek_pr.fil,SQLSHORT,0);
    sqlin ((short *) &ek_pr.mdn,SQLSHORT,0);
    sqlin ((short *) &ek_pr.akt,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &ek_pr.bearb,SQLDATE,0);
    sqlin ((double *) &ek_pr.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.hk_kost,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.hk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.sk_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.sk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_ek_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_ek_kalk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_ek,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_vk_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_vk_kalk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.fil_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1_kalk_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh1_sp,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2_kalk_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2,SQLDOUBLE,0);
    sqlin ((double *) &ek_pr.gh2_sp,SQLDOUBLE,0);
            sqltext = _T("update ek_pr set ek_pr.a = ?,  ")
_T("ek_pr.fil = ?,  ek_pr.mdn = ?,  ek_pr.akt = ?,  ek_pr.bearb = ?,  ")
_T("ek_pr.pr_ek = ?,  ek_pr.hk_kost = ?,  ek_pr.hk = ?,  ")
_T("ek_pr.sk_aufs = ?,  ek_pr.sk = ?,  ek_pr.fil_ek_aufs = ?,  ")
_T("ek_pr.fil_ek_kalk = ?,  ek_pr.fil_ek = ?,  ek_pr.fil_vk_aufs = ?,  ")
_T("ek_pr.fil_vk_kalk = ?,  ek_pr.fil_vk = ?,  ek_pr.gh1_aufs = ?,  ")
_T("ek_pr.gh1_kalk_vk = ?,  ek_pr.gh1 = ?,  ek_pr.gh1_sp = ?,  ")
_T("ek_pr.gh2_aufs = ?,  ek_pr.gh2_kalk_vk = ?,  ek_pr.gh2 = ?,  ")
_T("ek_pr.gh2_sp = ? ")

                                  _T("where mdn = ? ")
				  _T("and a = ?");
            sqlin ((short *)   &ek_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ek_pr.a, SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

< ------- */

/* -----> testupd_nicht notwendig 

            sqlin ((short *)   &ek_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ek_pr.a, SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from ek_pr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?"));
            sqlin ((short *)   &ek_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ek_pr.a, SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from ek_pr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?"));
< ------ */
				  
	sqlin ((double *) &ek_prpr.a,SQLDOUBLE,0);
    sqlin ((short *) &ek_prpr.fil,SQLSHORT,0);
    sqlin ((short *) &ek_prpr.mdn,SQLSHORT,0);
    sqlin ((short *) &ek_prpr.akt,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &ek_prpr.bearb,SQLDATE,0);

	sqlin ((double *) &ek_prpr.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.hk_kost,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.hk,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.sk_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.sk,SQLDOUBLE,0);

	sqlin ((double *) &ek_prpr.fil_ek_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.fil_ek_kalk,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.fil_ek,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.fil_vk_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.fil_vk_kalk,SQLDOUBLE,0);

	sqlin ((double *) &ek_prpr.fil_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.gh1_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.gh1_kalk_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.gh1,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.gh1_sp,SQLDOUBLE,0);

	sqlin ((double *) &ek_prpr.gh2_aufs,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.gh2_kalk_vk,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.gh2,SQLDOUBLE,0);
    sqlin ((double *) &ek_prpr.gh2_sp,SQLDOUBLE,0);

    sqlin ((DATE_STRUCT *) &ek_prpr.akv, SQLDATE,0);
    sqlin ((char *) ek_prpr.zeit,SQLCHAR,9) ;
    sqlin ((char *) ek_prpr.pers_nam,SQLCHAR,17) ;
    sqlin ((DATE_STRUCT *) &ek_prpr.gue_ab,SQLDATE,0) ;

   ins_cursor = sqlcursor (_T("insert into ek_prpr ( " )
_T( " a, fil, mdn, akt, bearb ")
_T( ",pr_ek, hk_kost, hk, sk_aufs, sk ")
_T( ",fil_ek_aufs, fil_ek_kalk, fil_ek, fil_vk_aufs,  fil_vk_kalk ")
_T( ",fil_vk, gh1_aufs,  gh1_kalk_vk,  gh1, gh1_sp ")
_T( ",gh2_aufs, gh2_kalk_vk, gh2, gh2_sp ") 
_T( ",akv, zeit, pers_nam, gue_ab ")
_T(" ) values ( ")
_T(" ?,?, ?,?, ?,?, ?,?, ?,? ")
_T(",?,?, ?,?, ?,?, ?,?, ?,? ")
_T(",?,?, ?,?, ?,?, ?,? )" ) ); 

}

BOOL EK_PRPR_CLASS::operator== (EK_PRPR& ek_prpr)
{
            if (this->ek_prpr.mdn    != ek_prpr.mdn) return FALSE;  
            if (this->ek_prpr.a      != ek_prpr.a) return FALSE;  
            return TRUE;
} 
