#include "Debug.h"
#include "stdafx.h"

#define MAXDEBUG 20


CDebug::CDebug ()
{
	LogName = "Log-dbRange";
	ProgName = "";
	DebugFile = "";
	dbMode =FALSE;
}

CDebug::CDebug (Text& LogName, Text& ProgName)
{
	this->LogName = LogName;
	this->ProgName = ProgName;
	DebugFile = "";
	dbMode =FALSE;
	CreateDebugFile ();
}

void CDebug::CreateDebugFile ()
{
	     FILE *fp;
		 char NrName [512];
		 char buffer [80];
		 char *etc;
		 char *m;
		 int fnr = 1;

		 if (DebugFile != "") return;


		 m = getenv ("testmode");
		 if (m != NULL)
		 {
			 dbMode = max (atoi (m), 1);
		 }
		 if (!dbMode) return;
			 
		 etc = getenv ("BWSETC");
		 if (etc == NULL)
		 {
			 return;
		 }

		 sprintf (NrName, "%s\\sel_dbgnr.txt", etc);
		 fp = fopen (NrName, "r");
		 if (fp != NULL)
		 {
			 fgets (buffer, 79, fp);
			 fnr = atoi (buffer);
			 fnr = (fnr == 0) ? 1 : fnr;
			 fclose (fp);
		 }
 	     DebugFile.Format ("c:\\temp\\%s%d", LogName.GetBuffer (), fnr);
         fdebug = fopen (DebugFile.GetBuffer (), "w");
		 fclose (fdebug);
		 fnr ++;
		 if (fnr > MAXDEBUG) fnr = 1;
		 fp = fopen (NrName, "w");
		 fprintf (fp, "%d", fnr);
		 fclose (fp);
}

void CDebug::Output (LPSTR text)
{
	CreateDebugFile ();

    if (!dbMode) return;
	fdebug = fopen (DebugFile.GetBuffer (), "a");
	if (fdebug != NULL)
	{
		fprintf (fdebug, "%s", text);
		fclose (fdebug);
	}
}

void CDebug::Output (Text& text)
{
	Output (text.GetBuffer ());
}
