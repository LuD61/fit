#include "StdAfx.h"
#include "ListDateTimeCtrl.h"
#include "EditListCtrl.h"

CListDateTimeCtrl::CListDateTimeCtrl(void)
{
}

CListDateTimeCtrl::~CListDateTimeCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CListDateTimeCtrl, CDateTimeCtrl)
	ON_WM_KEYDOWN ()
END_MESSAGE_MAP()

void CListDateTimeCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	int diff = 0;
	switch (nChar)
	{
	case VK_RETURN :

// Achtung !! Wenn die Funktionalität von Pfeil nach unten und Pfeil nach oben
//            innerhalb der DateTimeControl gehandelt werden sollen,
//            müssen case VK_DOWN und case VK_UP auf Bemerkung gesetzt werden.
	case VK_DOWN :
	case VK_UP :
	case VK_TAB :
	case VK_F6 :
	case VK_F7 :
	case VK_NEXT :
	case VK_PRIOR :
		((CEditListCtrl *)GetParent ())->OnKeyD (nChar);
		return;
	}
CDateTimeCtrl::OnKeyDown (nChar, nRepCnt, nFlags);
}
