02.05.2010 : 
alter table ek_pr add modif char(1);
alter table ek_pr add druck smallint;

alter table a_kalkpreis add kost (decimal(12,4);


{Neue Struktur seit 09.02.2010 }

create table ek_pr
  (
    a decimal(13,0),
    fil smallint,
    mdn smallint,
    akt smallint,
    bearb date,
    pr_ek decimal,
    hk_kost decimal,
    hk decimal,
    sk_aufs decimal,
    sk decimal,
    fil_ek_aufs decimal,
    fil_ek_kalk decimal,
    fil_ek decimal,
    fil_vk_aufs decimal,
    fil_vk_kalk decimal,
    fil_vk decimal,
    gh1_aufs decimal,
    gh1_kalk_vk decimal,
    gh1 decimal,
    gh1_sp decimal,
    gh2_aufs decimal,
    gh2_kalk_vk decimal,
    gh2 decimal,
    gh2_sp decimal,
    modif char(1),
    druck smallint
  ) in fit_dat extent size 32 next size 256 lock mode row;

create unique index i01ek_pr on ek_pr (a, mdn,fil) in fit_dat ;

create table ek_prpr
  (
    a decimal(13,0),
    fil smallint,
    mdn smallint,
    akt smallint,
    bearb date,
    pr_ek decimal,
    hk_kost decimal,
    hk decimal,
    sk_aufs decimal,
    sk decimal,
    fil_ek_aufs decimal,
    fil_ek_kalk decimal,
    fil_ek decimal,
    fil_vk_aufs decimal,
    fil_vk_kalk decimal,
    fil_vk decimal,
    gh1_aufs decimal,
    gh1_kalk_vk decimal,
    gh1 decimal,
    gh1_sp decimal,
    gh2_aufs decimal,
    gh2_kalk_vk decimal,
    gh2 decimal,
    gh2_sp decimal,
    akv date,
    zeit char(8),
    pers_nam char(16),
    gue_ab date
    
  ) in fit_dat extent size 32 next size 256 lock mode row;

create unique index i01ek_prpr on ek_prpr (a, mdn,fil,akv,zeit) in fit_dat ;

