#ifndef _COLBUTTONDEF
#define _COLBUTTONDEF

class CColButton : public CButton
{
  protected :
     BOOL Active;
     BOOL Pressed;
     BOOL InButton;
     int Id;
  public :
    CColButton ();
    void SetId (int);
    int GetId (void);
    void SetActive (BOOL);
    BOOL GetActive (void);
    void UpBorder ();
    void NoBorder ();
    void DownBorder ();
    BOOL IsInButton (CPoint&);

  protected :
 

  //{{AFX_MSG(CColButton)
	    afx_msg virtual void OnMouseMove(UINT nFlags, CPoint point);
	    afx_msg virtual void OnLButtonDown(UINT nFlags, CPoint point);
	    afx_msg virtual void OnLButtonUp(UINT nFlags, CPoint point);
 	    afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif