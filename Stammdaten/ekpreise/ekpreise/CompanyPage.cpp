// CompanyPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#ifdef ARTIKEL
#include "Artikel.h"
#else
#include "AprMan.h"
#endif
#include "CompanyPage.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "ChoicePtab.h"
#include "Util.h"
#include "Process.h"
#include "Price.h"
#include "dbRange.h"
#include "StrFuncs.h"
#include "datdial.h"

DB_RANGE dbRange;

// CCompanyPage-Dialogfeld

IMPLEMENT_DYNAMIC(CCompanyPage, CDbPropertyPage)
CCompanyPage::CCompanyPage()
	: CDbPropertyPage(CCompanyPage::IDD)
{
	Frame = NULL;
	ChoiceHWG = NULL;
	ChoiceWG = NULL;
	ChoiceAG = NULL;
	ChoiceA = NULL;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceHWG = TRUE;
	ModalChoiceWG = TRUE;
	ModalChoiceAG = TRUE;
	ModalChoiceA = TRUE;
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
	CString ProgNam;
	CUtil::GetProgNam(ProgNam);
//	Cfg.SetProgName( _T("EkPreise"));
	Cfg.SetProgName( _T(ProgNam.GetBuffer()));

	MdnGrCursor = -1;
	MdnCursor = -1;

	EkLesCursor = -1 ;

	FilGrCursor = -1;
	FilCursor = -1;
	ListType = Preise;
	EkFilGr = 99;
	mitPrintButton = 1;
	RangeOk = TRUE;
	flgListeAktiv = FALSE;
}

CCompanyPage::~CCompanyPage()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}

	if (MdnGrCursor != -1)
	{
		Ek_pr.sqlclose (MdnGrCursor);
		MdnGrCursor = -1;
	}

	if (MdnCursor != -1)
	{
		Ek_pr.sqlclose (MdnCursor);
		MdnCursor = -1;
	}

    if (FilGrCursor != -1)
	{
		Ek_pr.sqlclose (FilGrCursor);
		FilGrCursor = -1;
	}
	if (FilCursor != -1)
	{
		Ek_pr.sqlclose (FilCursor);
		FilCursor = -1;
	}

	if (FilCursor != -1)
	{
		Ek_pr.sqlclose (EkLesCursor);
	}
}

void CCompanyPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN_GR, m_LMdnGr);
	DDX_Control(pDX, IDC_MDN_GR, m_MdnGr);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_LFIL_GR, m_LFilGr);
	DDX_Control(pDX, IDC_FIL_GR, m_FilGr);
	DDX_Control(pDX, IDC_LFIL, m_LFil);
	DDX_Control(pDX, IDC_FIL, m_Fil);
	DDX_Control(pDX, IDC_LLIST_TYP, m_LListTyp);
	DDX_Control(pDX, IDC_LIST_TYP, m_ListTyp);

	DDX_Control(pDX, IDC_LJAHR, m_LJahr);
	DDX_Control(pDX, IDC_JAHR, m_Jahr);
	DDX_Control(pDX, IDC_LMonat, m_LMonat);
	DDX_Control(pDX, IDC_MONAT, m_Monat);


	DDX_Control(pDX, IDC_LHWG, m_LHwg);
	DDX_Control(pDX, IDC_HWG, m_Hwg);
	DDX_Control(pDX, IDC_LWG, m_LWg);
	DDX_Control(pDX, IDC_WG, m_Wg);
	DDX_Control(pDX, IDC_LAG, m_LAg);
	DDX_Control(pDX, IDC_AG, m_Ag);
	DDX_Control(pDX, IDC_LTEIL_SMT, m_LTeilSmt);
	DDX_Control(pDX, IDC_TEIL_SMT, m_TeilSmt);
	DDX_Control(pDX, IDC_LA_TYP, m_LATyp);
	DDX_Control(pDX, IDC_A_TYP, m_ATyp);
	DDX_Control(pDX, IDC_CMP_GROUP, m_CmpGroup);
	DDX_Control(pDX, IDC_GR_GROUP, m_GrGroup);
	DDX_Control(pDX, IDC_SORTIMENT_GROUP, m_SmtGroup);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_EK_EDIT, m_EkEdit);
	DDX_Control(pDX, IDC_LMDN_USER, m_LMdnUser);
	DDX_Control(pDX, IDC_MDN_USER, m_MdnUser);
	DDX_Control(pDX, IDC_MDN_ADR_KRZ, m_MdnAdrKrz);
	DDX_Control(pDX, IDC_ARTIKEL, m_Artikel);
	DDX_Control(pDX, IDC_LARTIKEL, m_LArtikel);
	DDX_Control(pDX, IDC_Hk_Edit, m_HkEdit);
	DDX_Control(pDX, IDC_HK_KOST_EDIT, m_HkKostEdit);
	DDX_Control(pDX, IDC_SK_AUFS_EDIT, m_SkAufsEdit);
	DDX_Control(pDX, IDC_SK_EDIT, m_SkEdit);
	DDX_Control(pDX, IDC_ALLE_ARTIKEL, m_AlleArtikel);
	DDX_Control(pDX, IDC_FIL_EK_AUFS_EDIT, m_FilEkAufsEdit);
	DDX_Control(pDX, IDC_FIL_EK_KALK_EDIT, m_FilEkKalkEdit);
	DDX_Control(pDX, IDC_FIL_EK_EDIT, m_FilEkEdit);
	DDX_Control(pDX, IDC_GH1_AUFS_EDIT, m_Gh1AufsEdit);
	DDX_Control(pDX, IDC_GH1_KALK_VK_EDIT, m_Gh1KalkVkEdit);
	DDX_Control(pDX, IDC_GH1_EDIT, m_Gh1Edit);
	DDX_Control(pDX, IDC_GH1_SP_EDIT, m_Gh1SpEdit);
	DDX_Control(pDX, IDC_GH2_AUFS_EDIT, m_Gh2AufsEdit);
	DDX_Control(pDX, IDC_GH2_KALK_VK_EDIT, m_Gh2KalkVkEdit);
	DDX_Control(pDX, IDC_GH2_EDIT, m_Gh2Edit);
	DDX_Control(pDX, IDC_GH2_SP_EDIT, m_Gh2SpEdit);
	DDX_Control(pDX, IDC_FIL_VK_AUFS_EDIT, m_FilVkAufsEdit);
	DDX_Control(pDX, IDC_FIL_VK_KALK_EDIT, m_FilVkKalkEdit);
	DDX_Control(pDX, IDC_FIL_VK_EDIT, m_FilVkEdit);
}


BEGIN_MESSAGE_MAP(CCompanyPage, CDbPropertyPage)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_CBN_SELCHANGE(IDC_MDN, OnCbnSelchangeMdn)
	ON_BN_CLICKED(IDC_MDNUSERCHOICE , OnMdnchoice)
	ON_BN_CLICKED(IDC_HWGCHOICE , OnHwgChoice)
	ON_BN_CLICKED(IDC_WGCHOICE , OnWgChoice)
	ON_BN_CLICKED(IDC_AGCHOICE , OnAgChoice)
	ON_BN_CLICKED(IDC_TEILSMTCHOICE , OnTeilSmtChoice)
	ON_BN_CLICKED(IDC_ATYPCHOICE , OnATypChoice)
	ON_BN_CLICKED(IDC_ARTIKELCHOICE , OnArtikelChoice)
	ON_BN_CLICKED(IDC_PROPEN, OnVK_F6)
	ON_BN_CLICKED(IDC_PRPRINT, OnPrint)
	ON_EN_CHANGE(IDC_MDN_USER, OnEnChangeMdnUser)
	ON_CBN_SELCHANGE(IDC_FIL, OnCbnSelchangeFil)
	ON_BN_CLICKED(IDC_PRACTIVATE, OnActivate)
	ON_EN_KILLFOCUS(IDC_A_TYP, &CCompanyPage::OnEnKillfocusATyp)
	ON_EN_KILLFOCUS(IDC_ARTIKEL, &CCompanyPage::OnEnKillfocusArtikel)
	ON_EN_KILLFOCUS(IDC_TEIL_SMT, &CCompanyPage::OnEnKillfocusTeilSmt)
	ON_EN_KILLFOCUS(IDC_AG, &CCompanyPage::OnEnKillfocusAg)
	ON_EN_KILLFOCUS(IDC_WG, &CCompanyPage::OnEnKillfocusWg)
	ON_EN_KILLFOCUS(IDC_HWG, &CCompanyPage::OnEnKillfocusHwg)
	ON_EN_SETFOCUS(IDC_JAHR, &CCompanyPage::OnEnSetfocusJahr)
	ON_EN_SETFOCUS(IDC_MONAT, &CCompanyPage::OnEnSetfocusMonat)
	ON_EN_SETFOCUS(IDC_HWG, &CCompanyPage::OnEnSetfocusHwg)
	ON_EN_SETFOCUS(IDC_WG, &CCompanyPage::OnEnSetfocusWg)
	ON_EN_SETFOCUS(IDC_AG, &CCompanyPage::OnEnSetfocusAg)
	ON_EN_SETFOCUS(IDC_ARTIKEL, &CCompanyPage::OnEnSetfocusArtikel)
	ON_EN_SETFOCUS(IDC_TEIL_SMT, &CCompanyPage::OnEnSetfocusTeilSmt)
	ON_EN_SETFOCUS(IDC_A_TYP, &CCompanyPage::OnEnSetfocusATyp)
	ON_EN_KILLFOCUS(IDC_JAHR, &CCompanyPage::OnEnKillfocusJahr)
	ON_EN_KILLFOCUS(IDC_MONAT, &CCompanyPage::OnEnKillfocusMonat)
	ON_BN_CLICKED(IDC_EK_EDIT, &CCompanyPage::OnBnClickedEkEdit)
	ON_BN_CLICKED(IDC_Hk_Edit, &CCompanyPage::OnBnClickedHkEdit)
	ON_BN_CLICKED(IDC_HK_KOST_EDIT, &CCompanyPage::OnBnClickedHkKostEdit)
	ON_BN_CLICKED(IDC_SK_AUFS_EDIT, &CCompanyPage::OnBnClickedSkAufsEdit)
	ON_BN_CLICKED(IDC_SK_EDIT, &CCompanyPage::OnBnClickedSkEdit)
	ON_BN_CLICKED(IDC_ALLE_ARTIKEL, &CCompanyPage::OnBnClickedAlleArtikel)
	ON_EN_KILLFOCUS(IDC_MDN_USER, &CCompanyPage::OnEnKillfocusMdnUser)
	ON_BN_CLICKED(IDC_FIL_EK_AUFS_EDIT, &CCompanyPage::OnBnClickedFilEkAufsEdit)
	ON_BN_CLICKED(IDC_FIL_EK_KALK_EDIT, &CCompanyPage::OnBnClickedFilEkKalkEdit)
	ON_BN_CLICKED(IDC_FIL_EK_EDIT, &CCompanyPage::OnBnClickedFilEkEdit)
	ON_BN_CLICKED(IDC_GH1_AUFS_EDIT, &CCompanyPage::OnBnClickedGh1AufsEdit)
	ON_BN_CLICKED(IDC_GH1_KALK_VK_EDIT, &CCompanyPage::OnBnClickedGh1KalkVkEdit)
	ON_BN_CLICKED(IDC_GH1_EDIT, &CCompanyPage::OnBnClickedGh1Edit)
	ON_BN_CLICKED(IDC_GH1_SP_EDIT, &CCompanyPage::OnBnClickedGh1SpEdit)
	ON_BN_CLICKED(IDC_GH2_AUFS_EDIT, &CCompanyPage::OnBnClickedGh2AufsEdit)
	ON_BN_CLICKED(IDC_GH2_KALK_VK_EDIT, &CCompanyPage::OnBnClickedGh2KalkVkEdit)
	ON_BN_CLICKED(IDC_GH2_EDIT, &CCompanyPage::OnBnClickedGh2Edit)
	ON_BN_CLICKED(IDC_GH2_SP_EDIT, &CCompanyPage::OnBnClickedGh2SpEdit)
	ON_BN_CLICKED(IDC_FIL_VK_AUFS_EDIT, &CCompanyPage::OnBnClickedFilVkAufsEdit)
	ON_BN_CLICKED(IDC_FIL_VK_KALK_EDIT, &CCompanyPage::OnBnClickedFilVkKalkEdit)
	ON_BN_CLICKED(IDC_FIL_VK_EDIT, &CCompanyPage::OnBnClickedFilVkEdit)
END_MESSAGE_MAP()


// CCompanyPage-Meldungshandler

BOOL CCompanyPage::OnInitDialog ()
{
	CPropertyPage::OnInitDialog();

	A_bas.opendbase (_T("bws"));

	CUtil::GetPersName (PersName);
	CUtil::GetProgNam (ProgNam);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
//		lFont.CreatePointFont (85, _T("Courier"));
		lFont.CreatePointFont (95, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
//		lFont.CreatePointFont (95, _T("Courier New"));
		lFont.CreatePointFont (120, _T("Dlg"));
	}

	ReadCfg ();

    m_Open.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 60, 48),
		                          this, IDC_PROPEN);
	m_Open.LoadBitmap (IDB_OPEN);
	m_Open.LoadMask (IDB_OPEN_MASK);
	m_Open.SetToolTip (_T("Preise einlesen    F6"));
	m_Open.Tooltip.WindowOrientation = m_Open.Tooltip.Center;

		m_Print.Create (_T("Drucken"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 60, 48),
		                          this, IDC_PRPRINT);
		m_Print.LoadBitmap (IDB_PRINT);
		m_Print.LoadMask (IDB_PRINT_MASK);
		m_Print.SetToolTip (_T("Preise drucken"));
		m_Print.Tooltip.WindowOrientation = m_Print.Tooltip.Center;

	if (!mitPrintButton)
	{
		m_Print.ShowWindow(SW_HIDE);
	}


    m_Activate.Create (_T("EK-Preise aktivieren"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 180, 20),
		                          this, IDC_PRACTIVATE);
	m_Activate.Orientation = m_Activate.Left;
	m_Activate.SetToolTip (_T("�bernahme der EK-Preise in die EK-Tabelle"));
	m_Activate.Tooltip.WindowOrientation = m_Print.Tooltip.Center;

	PrDiff = 1;

	A_pr.sqlout ((short *)&gr,      SQLSHORT, 0);
	A_pr.sqlout ((LPTSTR)  gr_bz1,   SQLCHAR, sizeof (gr_bz1));
	MdnGrCursor = A_pr.sqlcursor (_T("select gr, gr_bz1 ")
			                           _T("from gr_zuord ")
									   _T("where mdn = 0 ")
									   _T("and gr > 0 order by gr"));

	A_pr.sqlout ((short *)&mdn,      SQLSHORT, 0);
	A_pr.sqlout ((LPTSTR) adr_krz,   SQLCHAR, sizeof (adr_krz));
	MdnCursor = A_pr.sqlcursor (_T("select mdn.mdn, adr.adr_krz ")
			                       _T("from mdn, adr ")
								   _T("where mdn.mdn > 0 ")
								   _T("and adr.adr = mdn.adr ")
								   _T("order by mdn.mdn"));

	A_pr.sqlin ((short *)&Mdn,      SQLSHORT, 0);
	A_pr.sqlout ((short *)&gr,      SQLSHORT, 0);
	A_pr.sqlout ((LPTSTR)  gr_bz1,   SQLCHAR, sizeof (gr_bz1));
	FilGrCursor = A_pr.sqlcursor (_T("select gr, gr_bz1 ")
		                           _T("from gr_zuord ")
								   _T("where mdn = ? ")
								   _T("and gr > 0 order by gr"));

	A_pr.sqlin ((short *)&Mdn,      SQLSHORT, 0);
	A_pr.sqlout ((short *)&fil,      SQLSHORT, 0);
	A_pr.sqlout ((LPTSTR) adr_krz,   SQLCHAR, sizeof (adr_krz));
	FilCursor = A_pr.sqlcursor (_T("select fil.fil, adr.adr_krz ")
		                           _T("from fil, adr ")
								   _T("where fil.fil > 0 ")
								   _T("and adr.adr = fil.adr ")
								   _T("and fil.mdn = ? ")
								   _T("order by fil.fil"));

	Hwg     = _T("");
	Wg      = _T("");
	Ag      = _T("");
	TeilSmt = _T("");
	ATyp    = _T("");
	Artikel = _T("");
	Jahr    = _T("");
	Monat   = _T("");

	memcpy (&MdnAdrUser.adr, &adr_null, sizeof (ADR));
	memcpy (&MdnUser.mdn, &mdn_null, sizeof (MDN));



    Form.Add (new CFormField (&m_MdnUser,EDIT,     (short *) &MdnUser.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnAdrKrz,EDIT,(char *) MdnAdrUser.adr.adr_krz, VCHAR));
	Form.Add (new CFormField (&m_MdnGr,COMBOBOX, (short *) &MdnGr, VSHORT));
	Form.Add (new CFormField (&m_Mdn,COMBOBOX,   (short *) &Mdn, VSHORT));
	Form.Add (new CFormField (&m_FilGr,COMBOBOX, (short *) &FilGr, VSHORT));
	Form.Add (new CFormField (&m_Fil,COMBOBOX,   (short *) &Fil, VSHORT));
	Form.Add (new CFormField (&m_ListTyp,COMBOBOX,   (short *) &ListType, VSHORT));

	Form.Add (new CFormField (&m_Jahr,EDIT,       (CString *) &Jahr, VSTRING));
	Form.Add (new CFormField (&m_Monat,EDIT,       (CString *) &Monat, VSTRING));

	Form.Add (new CFormField (&m_Hwg,EDIT,       (CString *) &Hwg, VSTRING));
	Form.Add (new CFormField (&m_Wg,EDIT,        (CString *) &Wg, VSTRING));
	Form.Add (new CFormField (&m_Ag,EDIT,        (CString *) &Ag, VSTRING));
	Form.Add (new CFormField (&m_TeilSmt,EDIT,   (CString *) &TeilSmt, VSTRING));
	Form.Add (new CFormField (&m_ATyp,EDIT,      (CString *) &ATyp, VSTRING));
	Form.Add (new CFormField (&m_Artikel,EDIT,   (CString *) &Artikel, VSTRING));

	Form.Add (new CFormField (&m_AlleArtikel,CHECKBOX,    (char *) &Lst.lst.AlleArtikel, VCHAR));
	Form.Add (new CFormField (&m_EkEdit,CHECKBOX,    (char *) &Lst.lst.EkEdit, VCHAR));
	Form.Add (new CFormField (&m_HkEdit,CHECKBOX,    (char *) &Lst.lst.HkEdit, VCHAR));
	Form.Add (new CFormField (&m_HkKostEdit,CHECKBOX,    (char *) &Lst.lst.HkKostEdit, VCHAR));
	Form.Add (new CFormField (&m_SkAufsEdit,CHECKBOX,    (char *) &Lst.lst.SkAufsEdit, VCHAR));
	Form.Add (new CFormField (&m_SkEdit,CHECKBOX,    (char *) &Lst.lst.SkEdit, VCHAR));
	Form.Add (new CFormField (&m_FilEkAufsEdit,CHECKBOX,    (char *) &Lst.lst.FilEkAufsEdit, VCHAR));
	Form.Add (new CFormField (&m_FilEkKalkEdit,CHECKBOX,    (char *) &Lst.lst.FilEkKalkEdit, VCHAR));
	Form.Add (new CFormField (&m_FilEkEdit,CHECKBOX,    (char *) &Lst.lst.FilEkEdit, VCHAR));
	Form.Add (new CFormField (&m_FilVkAufsEdit,CHECKBOX,    (char *) &Lst.lst.FilVkAufsEdit, VCHAR));
	Form.Add (new CFormField (&m_FilVkKalkEdit,CHECKBOX,    (char *) &Lst.lst.FilVkKalkEdit, VCHAR));
	Form.Add (new CFormField (&m_FilVkEdit,CHECKBOX,    (char *) &Lst.lst.FilVkEdit, VCHAR));

	Form.Add (new CFormField (&m_Gh1AufsEdit,CHECKBOX,    (char *) &Lst.lst.Gh1AufsEdit, VCHAR));
	Form.Add (new CFormField (&m_Gh1KalkVkEdit,CHECKBOX,    (char *) &Lst.lst.Gh1KalkVkEdit, VCHAR));
	Form.Add (new CFormField (&m_Gh1Edit,CHECKBOX,    (char *) &Lst.lst.Gh1Edit, VCHAR));
	Form.Add (new CFormField (&m_Gh1SpEdit,CHECKBOX,    (char *) &Lst.lst.Gh1SpEdit, VCHAR));
	Form.Add (new CFormField (&m_Gh2AufsEdit,CHECKBOX,    (char *) &Lst.lst.Gh2AufsEdit, VCHAR));
	Form.Add (new CFormField (&m_Gh2KalkVkEdit,CHECKBOX,    (char *) &Lst.lst.Gh2KalkVkEdit, VCHAR));
	Form.Add (new CFormField (&m_Gh2Edit,CHECKBOX,    (char *) &Lst.lst.Gh2Edit, VCHAR));
	Form.Add (new CFormField (&m_Gh2SpEdit,CHECKBOX,    (char *) &Lst.lst.Gh2SpEdit, VCHAR));

//	m_EkEdit.EnableWindow (FALSE);
//	m_EkEdit.ShowWindow (SW_HIDE);
	m_EkEdit.SetWindowText (_T(m_List.EkBez));
	m_HkEdit.SetWindowText (_T(m_List.HkBez));
	m_HkKostEdit.SetWindowText (_T(m_List.HkKostBez));
	m_SkAufsEdit.SetWindowText (_T(m_List.SkAufsBez));
	m_SkEdit.SetWindowText (_T(m_List.SkBez));
	m_FilEkAufsEdit.SetWindowText (_T(m_List.FilEkAufsBez));
	m_FilEkKalkEdit.SetWindowText (_T(m_List.FilEkKalkBez));
	m_FilEkEdit.SetWindowText (_T(m_List.FilEkBez));
	m_FilVkAufsEdit.SetWindowText (_T(m_List.FilVkAufsBez));
	m_FilVkKalkEdit.SetWindowText (_T(m_List.FilVkKalkBez));
	m_FilVkEdit.SetWindowText (_T(m_List.FilVkBez));

	m_Gh1AufsEdit.SetWindowText (_T(m_List.Gh1AufsBez));
	m_Gh1KalkVkEdit.SetWindowText (_T(m_List.Gh1KalkVkBez));
	m_Gh1Edit.SetWindowText (_T(m_List.Gh1Bez));
	m_Gh1SpEdit.SetWindowText (_T(m_List.Gh1SpBez));
	m_Gh2AufsEdit.SetWindowText (_T(m_List.Gh2AufsBez));
	m_Gh2KalkVkEdit.SetWindowText (_T(m_List.Gh2KalkVkBez));
	m_Gh2Edit.SetWindowText (_T(m_List.Gh2Bez));
	m_Gh2SpEdit.SetWindowText (_T(m_List.Gh2SpBez));

	if (!strcmp (m_List.KalkulationsModus, "a_mo_we") == 0)
	{
		m_AlleArtikel.ShowWindow (SW_HIDE);
	}
	if (strcmp (m_List.SchreibModus, "GH") == 0) 
	{
		m_AlleArtikel.ShowWindow (SW_HIDE);
	}
	if (m_List.EkEdit < 2) m_EkEdit.ShowWindow (SW_HIDE);
	if (m_List.HkEdit < 2) m_HkEdit.ShowWindow (SW_HIDE);
	if (m_List.HkKostEdit < 2) m_HkKostEdit.ShowWindow (SW_HIDE);
	if (m_List.SkAufsEdit < 2) m_SkAufsEdit.ShowWindow (SW_HIDE);
	if (m_List.SkEdit < 2) m_SkEdit.ShowWindow (SW_HIDE);
	if (m_List.FilEkAufsEdit < 2) m_FilEkAufsEdit.ShowWindow (SW_HIDE);
	if (m_List.FilEkKalkEdit < 2) m_FilEkKalkEdit.ShowWindow (SW_HIDE);
	if (m_List.FilEkEdit < 2) m_FilEkEdit.ShowWindow (SW_HIDE);
	if (m_List.FilVkAufsEdit < 2) m_FilVkAufsEdit.ShowWindow (SW_HIDE);
	if (m_List.FilVkKalkEdit < 2) m_FilVkKalkEdit.ShowWindow (SW_HIDE);
	if (m_List.FilVkEdit < 2) m_FilVkEdit.ShowWindow (SW_HIDE);

	if (m_List.Gh1AufsEdit < 2) m_Gh1AufsEdit.ShowWindow (SW_HIDE);
	if (m_List.Gh1KalkVkEdit < 2) m_Gh1KalkVkEdit.ShowWindow (SW_HIDE);
	if (m_List.Gh1Edit < 2) m_Gh1Edit.ShowWindow (SW_HIDE);
	if (m_List.Gh1SpEdit < 2) m_Gh1SpEdit.ShowWindow (SW_HIDE);
	if (m_List.Gh2AufsEdit < 2) m_Gh2AufsEdit.ShowWindow (SW_HIDE);
	if (m_List.Gh2KalkVkEdit < 2) m_Gh2KalkVkEdit.ShowWindow (SW_HIDE);
	if (m_List.Gh2Edit < 2) m_Gh2Edit.ShowWindow (SW_HIDE);
	if (m_List.Gh2SpEdit < 2) m_Gh2SpEdit.ShowWindow (SW_HIDE);

	strcpy(Lst.lst.EkEdit,"N");
	strcpy(Lst.lst.HkEdit,"N");
	strcpy(Lst.lst.HkKostEdit,"N");
	strcpy(Lst.lst.SkAufsEdit,"N");
	strcpy(Lst.lst.SkEdit,"N");
	strcpy(Lst.lst.FilEkAufsEdit,"N");
	strcpy(Lst.lst.FilEkKalkEdit,"N");
	strcpy(Lst.lst.FilEkEdit,"N");
	strcpy(Lst.lst.FilVkAufsEdit,"N");
	strcpy(Lst.lst.FilVkKalkEdit,"N");
	strcpy(Lst.lst.FilVkEdit,"N");

	strcpy(Lst.lst.Gh1AufsEdit,"N");
	strcpy(Lst.lst.Gh1KalkVkEdit,"N");
	strcpy(Lst.lst.Gh1Edit,"N");
	strcpy(Lst.lst.Gh1SpEdit,"N");
	strcpy(Lst.lst.Gh2AufsEdit,"N");
	strcpy(Lst.lst.Gh2KalkVkEdit,"N");
	strcpy(Lst.lst.Gh2Edit,"N");
	strcpy(Lst.lst.Gh2SpEdit,"N");

	if (m_List.EkEdit > 0) strcpy(Lst.lst.EkEdit,"J");
	if (m_List.HkEdit > 0) strcpy(Lst.lst.HkEdit,"J");
	if (m_List.HkKostEdit > 0) strcpy(Lst.lst.HkKostEdit,"J");
	if (m_List.SkAufsEdit > 0) strcpy(Lst.lst.SkAufsEdit,"J");
	if (m_List.SkEdit > 0) strcpy(Lst.lst.SkEdit,"J");
	if (m_List.FilEkAufsEdit > 0) strcpy(Lst.lst.FilEkAufsEdit,"J");
	if (m_List.FilEkKalkEdit > 0) strcpy(Lst.lst.FilEkKalkEdit,"J");
	if (m_List.FilEkEdit > 0) strcpy(Lst.lst.FilEkEdit,"J");
	if (m_List.FilVkAufsEdit > 0) strcpy(Lst.lst.FilVkAufsEdit,"J");
	if (m_List.FilVkKalkEdit > 0) strcpy(Lst.lst.FilVkKalkEdit,"J");
	if (m_List.FilVkEdit > 0) strcpy(Lst.lst.FilVkEdit,"J");

	if (m_List.Gh1AufsEdit > 0) strcpy(Lst.lst.Gh1AufsEdit,"J");
	if (m_List.Gh1KalkVkEdit > 0) strcpy(Lst.lst.Gh1KalkVkEdit,"J");
	if (m_List.Gh1Edit > 0) strcpy(Lst.lst.Gh1Edit,"J");
	if (m_List.Gh1SpEdit > 0) strcpy(Lst.lst.Gh1SpEdit,"J");
	if (m_List.Gh2AufsEdit > 0) strcpy(Lst.lst.Gh2AufsEdit,"J");
	if (m_List.Gh2KalkVkEdit > 0) strcpy(Lst.lst.Gh2KalkVkEdit,"J");
	if (m_List.Gh2Edit > 0) strcpy(Lst.lst.Gh2Edit,"J");
	if (m_List.Gh2SpEdit > 0) strcpy(Lst.lst.Gh2SpEdit,"J");

	if (m_List.EkEdit == 3) strcpy(Lst.lst.EkEdit,"N");
	if (m_List.HkEdit == 3) strcpy(Lst.lst.HkEdit,"N");
	if (m_List.HkKostEdit == 3) strcpy(Lst.lst.HkKostEdit,"N");
	if (m_List.SkAufsEdit == 3) strcpy(Lst.lst.SkAufsEdit,"N");
	if (m_List.SkEdit == 3) strcpy(Lst.lst.SkEdit,"N");
	if (m_List.FilEkAufsEdit == 3) strcpy(Lst.lst.FilEkAufsEdit,"N");
	if (m_List.FilEkKalkEdit == 3) strcpy(Lst.lst.FilEkKalkEdit,"N");
	if (m_List.FilEkEdit == 3) strcpy(Lst.lst.FilEkEdit,"N");
	if (m_List.FilVkAufsEdit == 3) strcpy(Lst.lst.FilVkAufsEdit,"N");
	if (m_List.FilVkKalkEdit == 3) strcpy(Lst.lst.FilVkKalkEdit,"N");
	if (m_List.FilVkEdit == 3) strcpy(Lst.lst.FilVkEdit,"N");

	if (m_List.Gh1AufsEdit == 3) strcpy(Lst.lst.Gh1AufsEdit,"N");
	if (m_List.Gh1KalkVkEdit == 3) strcpy(Lst.lst.Gh1KalkVkEdit,"N");
	if (m_List.Gh1Edit == 3) strcpy(Lst.lst.Gh1Edit,"N");
	if (m_List.Gh1SpEdit == 3) strcpy(Lst.lst.Gh1SpEdit,"N");
	if (m_List.Gh2AufsEdit == 3) strcpy(Lst.lst.Gh2AufsEdit,"N");
	if (m_List.Gh2KalkVkEdit == 3) strcpy(Lst.lst.Gh2KalkVkEdit,"N");
	if (m_List.Gh2Edit == 3) strcpy(Lst.lst.Gh2Edit,"N");
	if (m_List.Gh2SpEdit == 3) strcpy(Lst.lst.Gh2SpEdit,"N");

	FillMdnGrCombo ();
	FillMdnCombo ();
	FillListTypCombo ();
	m_FilGr.EnableWindow (FALSE);
	m_Fil.EnableWindow (FALSE);


// Grid f�r Benutzer

    UserGrid.Create (this, 20, 20);
    UserGrid.SetBorder (12, 20);
    UserGrid.SetCellHeight (15);
    UserGrid.SetFontCellHeight (this);
    UserGrid.SetGridSpace (5, 8);

	MdnUserGrid.Create (this, 1, 2);
    MdnUserGrid.SetBorder (0, 0);
    MdnUserGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_MdnUser = new CCtrlInfo (&m_MdnUser, 0, 0, 1, 1);
	MdnUserGrid.Add (c_MdnUser);
	CtrlGrid.CreateChoiceButton (m_MdnUserChoice, IDC_MDNUSERCHOICE, this);
	CCtrlInfo *c_MdnUserChoice = new CCtrlInfo (&m_MdnUserChoice, 1, 0, 1, 1);
	MdnUserGrid.Add (c_MdnUserChoice);

	CCtrlInfo *c_LMdnUser     = new CCtrlInfo (&m_LMdnUser, 0, 0, 1, 1); 
	UserGrid.Add (c_LMdnUser);
	CCtrlInfo *c_MdnUserGrid     = new CCtrlInfo (&MdnUserGrid, 1, 0, 1, 1); 
	UserGrid.Add (c_MdnUserGrid);
	CCtrlInfo *c_MdnAdrKrz    = new CCtrlInfo (&m_MdnAdrKrz, 2, 0, 1, 1); 
	UserGrid.Add (c_MdnAdrKrz);


// Grid f�r HWG mit Auswahl
	HwgGrid.Create (this, 1, 2);
    HwgGrid.SetBorder (0, 0);
    HwgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_HwgGrid = new CCtrlInfo (&m_Hwg, 0, 0, 1, 1);
	HwgGrid.Add (c_HwgGrid);
	CtrlGrid.CreateChoiceButton (m_HwgChoice, IDC_HWGCHOICE, this);
	CCtrlInfo *c_HwgChoice = new CCtrlInfo (&m_HwgChoice, 1, 0, 1, 1);
	HwgGrid.Add (c_HwgChoice);

// Grid f�r WG mit Auswahl
	WgGrid.Create (this, 1, 2);
    WgGrid.SetBorder (0, 0);
    WgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_WgGrid = new CCtrlInfo (&m_Wg, 0, 0, 1, 1);
	WgGrid.Add (c_WgGrid);
	CtrlGrid.CreateChoiceButton (m_WgChoice, IDC_WGCHOICE, this);
	CCtrlInfo *c_WgChoice = new CCtrlInfo (&m_WgChoice, 1, 0, 1, 1);
	WgGrid.Add (c_WgChoice);

// Grid f�r AG mit Auswahl
	AgGrid.Create (this, 1, 2);
    AgGrid.SetBorder (0, 0);
    AgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_AgGrid = new CCtrlInfo (&m_Ag, 0, 0, 1, 1);
	AgGrid.Add (c_AgGrid);
	CtrlGrid.CreateChoiceButton (m_AgChoice, IDC_AGCHOICE, this);
	CCtrlInfo *c_AgChoice = new CCtrlInfo (&m_AgChoice, 1, 0, 1, 1);
	AgGrid.Add (c_AgChoice);

// Grid f�r Teilsortiment mit Auswahl
	TeilSmtGrid.Create (this, 1, 2);
    TeilSmtGrid.SetBorder (0, 0);
    TeilSmtGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_TeilSmtGrid = new CCtrlInfo (&m_TeilSmt, 0, 0, 1, 1);
	TeilSmtGrid.Add (c_TeilSmtGrid);
	CtrlGrid.CreateChoiceButton (m_TeilSmtChoice, IDC_TEILSMTCHOICE, this);
	CCtrlInfo *c_TeilSmtChoice = new CCtrlInfo (&m_TeilSmtChoice, 1, 0, 1, 1);
	TeilSmtGrid.Add (c_TeilSmtChoice);

// Grid f�r Artikeltyp mit Auswahl
	ATypGrid.Create (this, 1, 2);
    ATypGrid.SetBorder (0, 0);
    ATypGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_ATypGrid = new CCtrlInfo (&m_ATyp, 0, 0, 1, 1);
	ATypGrid.Add (c_ATypGrid);
	CtrlGrid.CreateChoiceButton (m_ATypChoice, IDC_ATYPCHOICE, this);
	CCtrlInfo *c_ATypChoice = new CCtrlInfo (&m_ATypChoice, 1, 0, 1, 1);
	ATypGrid.Add (c_ATypChoice);

// Grid f�r Artikel mit Auswahl
	ArtikelGrid.Create (this, 1, 2);
    ArtikelGrid.SetBorder (0, 0);
    ArtikelGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_ArtikelGrid = new CCtrlInfo (&m_Artikel, 0, 0, 1, 1);
	ArtikelGrid.Add (c_ArtikelGrid);
	CtrlGrid.CreateChoiceButton (m_ArtikelChoice, IDC_ARTIKELCHOICE, this);
	CCtrlInfo *c_ArtikelChoice = new CCtrlInfo (&m_ArtikelChoice, 1, 0, 1, 1);
	ArtikelGrid.Add (c_ArtikelChoice);

// Hauptgrid
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (20, 8);

    CompanyGrid.Create (this, 20, 20);
    CompanyGrid.SetBorder (12, 20);
    CompanyGrid.SetCellHeight (15);
    CompanyGrid.SetFontCellHeight (this);
    CompanyGrid.SetGridSpace (5, 8);

	CCtrlInfo *c_CompanyGroup     = new CCtrlInfo (&m_CmpGroup, 0, 0, 5, 5); 
	CompanyGrid.Add (c_CompanyGroup);

	CCtrlInfo *c_LMdnGr     = new CCtrlInfo (&m_LMdnGr, 1, 1, 1, 1); 
	CompanyGrid.Add (c_LMdnGr);
	CCtrlInfo *c_MdnGr     = new CCtrlInfo (&m_MdnGr, 2, 1, 1, 1); 
	CompanyGrid.Add (c_MdnGr);
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 1, 2, 1, 1); 
	CompanyGrid.Add (c_LMdn);
	CCtrlInfo *c_Mdn     = new CCtrlInfo (&m_Mdn, 2, 2, 1, 1); 
	CompanyGrid.Add (c_Mdn);

	CCtrlInfo *c_LFilGr     = new CCtrlInfo (&m_LFilGr, 1, 3, 1, 1); 
	CompanyGrid.Add (c_LFilGr);
	CCtrlInfo *c_FilGr     = new CCtrlInfo (&m_FilGr, 2, 3, 1, 1); 
	CompanyGrid.Add (c_FilGr);
	CCtrlInfo *c_LFil     = new CCtrlInfo (&m_LFil, 1, 4, 1, 1); 
	CompanyGrid.Add (c_LFil);
	CCtrlInfo *c_Fil     = new CCtrlInfo (&m_Fil, 2, 4, 1, 1); 
	CompanyGrid.Add (c_Fil);

	CCtrlInfo *c_LListTyp     = new CCtrlInfo (&m_LListTyp, 1, 5, 1, 1); 
	CompanyGrid.Add (c_LListTyp);
	CCtrlInfo *c_ListTyp     = new CCtrlInfo (&m_ListTyp, 2, 5, 1, 1); 
	CompanyGrid.Add (c_ListTyp);

    GrGrid.Create (this, 20, 20);
    GrGrid.SetBorder (12, 20);
    GrGrid.SetCellHeight (15);
    GrGrid.SetFontCellHeight (this);
    GrGrid.SetGridSpace (5, 8);

	CCtrlInfo *c_GrGroup     = new CCtrlInfo (&m_GrGroup, 0, 0, 5, 5); 
	GrGrid.Add (c_GrGroup);

	CCtrlInfo *c_LHwg     = new CCtrlInfo (&m_LHwg, 1, 1, 1, 1); 
	GrGrid.Add (c_LHwg);
//	CCtrlInfo *c_Hwg     = new CCtrlInfo (&m_Hwg, 2, 1, 1, 1); 
	CCtrlInfo *c_Hwg     = new CCtrlInfo (&HwgGrid, 2, 1, 1, 1); 
	GrGrid.Add (c_Hwg);
	CCtrlInfo *c_LWg     = new CCtrlInfo (&m_LWg, 1, 2, 1, 1); 
	GrGrid.Add (c_LWg);
//	CCtrlInfo *c_Wg     = new CCtrlInfo (&m_Wg, 2, 2, 1, 1); 
	CCtrlInfo *c_Wg     = new CCtrlInfo (&WgGrid, 2, 2, 1, 1); 
	GrGrid.Add (c_Wg);
	CCtrlInfo *c_LAg     = new CCtrlInfo (&m_LAg, 1, 3, 1, 1); 
	GrGrid.Add (c_LAg);
//	CCtrlInfo *c_Ag     = new CCtrlInfo (&m_Ag, 2, 3, 1, 1); 
	CCtrlInfo *c_Ag     = new CCtrlInfo (&AgGrid, 2, 3, 1, 1); 
	GrGrid.Add (c_Ag);

	CCtrlInfo *c_LTeilSmt     = new CCtrlInfo (&m_LTeilSmt, 1, 4, 1, 1); 
	GrGrid.Add (c_LTeilSmt);
	CCtrlInfo *c_TeilSmt     = new CCtrlInfo (&TeilSmtGrid, 2, 4, 1, 1); 
	GrGrid.Add (c_TeilSmt);
	CCtrlInfo *c_LATyp     = new CCtrlInfo (&m_LATyp, 1, 5, 1, 1); 
	GrGrid.Add (c_LATyp);
	CCtrlInfo *c_ATyp     = new CCtrlInfo (&ATypGrid, 2, 5, 1, 1); 
	GrGrid.Add (c_ATyp);
	CCtrlInfo *c_LArtikel     = new CCtrlInfo (&m_LArtikel, 1, 6, 1, 1); 
	GrGrid.Add (c_LArtikel);
	CCtrlInfo *c_Artikel     = new CCtrlInfo (&ArtikelGrid, 2, 6, 1, 1); 
	GrGrid.Add (c_Artikel);

//	m_SmtGroup.ShowWindow (SW_HIDE);

    SmtGrid.Create (this, 20, 20);
    SmtGrid.SetBorder (12, 20);
    SmtGrid.SetCellHeight (15);
    SmtGrid.SetFontCellHeight (this);
    SmtGrid.SetGridSpace (5, 8);


	CCtrlInfo *c_SmtGroup     = new CCtrlInfo (&m_SmtGroup, 0, 0, 5, 5); 
	SmtGrid.Add (c_SmtGroup);

	CCtrlInfo *c_LJahr     = new CCtrlInfo (&m_LJahr, 1, 1, 1, 1); 
	SmtGrid.Add (c_LJahr);

	CCtrlInfo *c_Jahr = new CCtrlInfo (&m_Jahr, 2, 1, 3, 3);
	SmtGrid.Add (c_Jahr);

	CCtrlInfo *c_LMonat     = new CCtrlInfo (&m_LMonat, 1, 2, 1, 1); 
	SmtGrid.Add (c_LMonat);

	CCtrlInfo *c_Monat = new CCtrlInfo (&m_Monat, 2, 2, 3, 3);
	SmtGrid.Add (c_Monat);



	CCtrlInfo *c_UserGrid     = new CCtrlInfo (&UserGrid, 0, 0, 1, 1); 
	CtrlGrid.Add (c_UserGrid);
	CCtrlInfo *c_CompanyGrid     = new CCtrlInfo (&CompanyGrid, 0, 1, 1, 1); 
	CtrlGrid.Add (c_CompanyGrid);
	CompanyGrid.SetVisible (FALSE);
	CCtrlInfo *c_GrGrid     = new CCtrlInfo (&GrGrid, 0, 1, 1, 1); 
	CtrlGrid.Add (c_GrGrid);


//	CCtrlInfo *c_AlleArtikel     = new CCtrlInfo (&m_AlleArtikel, 5, 0, 3, 1); 
//	CtrlGrid.Add (c_AlleArtikel);
	int dz = 0;
	if (m_List.EkEdit > 1)
	{
		CCtrlInfo *c_EkEdit     = new CCtrlInfo (&m_EkEdit, 5, dz, 3, 1); 
		CtrlGrid.Add (c_EkEdit);
		dz++;
	}

	if (m_List.HkEdit > 1) 
	{
		CCtrlInfo *c_HkEdit     = new CCtrlInfo (&m_HkEdit, 5, dz, 3, 1); 
		CtrlGrid.Add (c_HkEdit);
		dz++;
	}

	if (m_List.HkKostEdit > 1)
	{
		CCtrlInfo *c_HkKostEdit     = new CCtrlInfo (&m_HkKostEdit, 5, dz, 3, 1); 
		CtrlGrid.Add (c_HkKostEdit);
		dz++;
	}

	if (m_List.SkAufsEdit > 1)
	{
		CCtrlInfo *c_SkAufsEdit     = new CCtrlInfo (&m_SkAufsEdit, 5, dz, 3, 1); 
		CtrlGrid.Add (c_SkAufsEdit);
		dz++;
	}

	if (m_List.SkEdit > 1)
	{
		CCtrlInfo *c_SkEdit     = new CCtrlInfo (&m_SkEdit, 5, dz, 3, 1); 
		CtrlGrid.Add (c_SkEdit);
		dz++;
	}

	if (m_List.FilEkAufsEdit > 1)
	{
		CCtrlInfo *c_FilEkAufsEdit     = new CCtrlInfo (&m_FilEkAufsEdit, 5, dz, 3, 1); 
		CtrlGrid.Add (c_FilEkAufsEdit);
		dz++;
	}
	if (m_List.FilEkKalkEdit > 1)
	{
		CCtrlInfo *c_FilEkKalkEdit     = new CCtrlInfo (&m_FilEkKalkEdit, 5, dz, 3, 1); 
		CtrlGrid.Add (c_FilEkKalkEdit);
		dz++;
	}
	if (m_List.FilEkEdit > 1)
	{
		CCtrlInfo *c_FilEkEdit     = new CCtrlInfo (&m_FilEkEdit, 5, dz, 3, 1); 
		CtrlGrid.Add (c_FilEkEdit);
		dz++;
	}
	short dx = 0;
	if (m_List.FilVkAufsEdit > 1)
	{
		if (dz >= 8) 
		{
			dx = 3;
			dz = 0;
		}
		CCtrlInfo *c_FilVkAufsEdit     = new CCtrlInfo (&m_FilVkAufsEdit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_FilVkAufsEdit);
		dz++;
	}
	if (m_List.FilVkKalkEdit > 1)
	{
		if (dz >= 8) 
		{
			dx = 3;
			dz = 0;
		}
		CCtrlInfo *c_FilVkKalkEdit     = new CCtrlInfo (&m_FilVkKalkEdit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_FilVkKalkEdit);
		dz++;
	}
	if (m_List.FilVkEdit > 1)
	{
		if (dz >= 8) 
		{
			dx = 3;
			dz = 0;
		}
		CCtrlInfo *c_FilVkEdit     = new CCtrlInfo (&m_FilVkEdit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_FilVkEdit);
		dz++;
	}

	if (m_List.Gh1AufsEdit > 1)
	{
		if (dz >= 8) 
		{
			dx = 3;
			dz = 0;
		}
		CCtrlInfo *c_Gh1AufsEdit     = new CCtrlInfo (&m_Gh1AufsEdit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_Gh1AufsEdit);
		dz++;
	}
	if (m_List.Gh1KalkVkEdit > 1)
	{
		if (dz >= 8) 
		{
			dx = 3;
			dz = 0;
		}
		CCtrlInfo *c_Gh1KalkVkEdit     = new CCtrlInfo (&m_Gh1KalkVkEdit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_Gh1KalkVkEdit);
		dz++;
	}
	if (m_List.Gh1Edit > 1)
	{
		if (dz >= 8) 
		{
			dx = 3;
			dz = 0;
		}
		CCtrlInfo *c_Gh1Edit     = new CCtrlInfo (&m_Gh1Edit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_Gh1Edit);
		dz++;
	}
	if (m_List.Gh1SpEdit > 1)
	{
		if (dz >= 8) 
		{
			dx = 3;
			dz = 0;
		}
		CCtrlInfo *c_Gh1SpEdit     = new CCtrlInfo (&m_Gh1SpEdit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_Gh1SpEdit);
		dz++;
	}

	if (m_List.Gh2AufsEdit > 1)
	{
		if (dz >= 8) 
		{
			dx = 3;
			dz = 0;
		}
		CCtrlInfo *c_Gh2AufsEdit     = new CCtrlInfo (&m_Gh2AufsEdit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_Gh2AufsEdit);
		dz++;
	}
	if (m_List.Gh2KalkVkEdit > 1)
	{
		if (dz >= 8) 
		{
			if (dx == 0) dx = 3;
			dx += 3;
			dz = 0;
		}
		CCtrlInfo *c_Gh2KalkVkEdit     = new CCtrlInfo (&m_Gh2KalkVkEdit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_Gh2KalkVkEdit);
		dz++;
	}
	if (m_List.Gh2Edit > 1)
	{
		if (dz >= 8) 
		{
			if (dx == 0) dx = 3;
			dx += 3;
			dz = 0;
		}
		CCtrlInfo *c_Gh2Edit     = new CCtrlInfo (&m_Gh2Edit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_Gh2Edit);
		dz++;
	}
	if (m_List.Gh2SpEdit > 1)
	{
		if (dz >= 8) 
		{
			if (dx == 0) dx = 3;
			dx += 3;
			dz = 0;
		}
		CCtrlInfo *c_Gh2SpEdit     = new CCtrlInfo (&m_Gh2SpEdit, 5+dx, dz, 3, 1); 
		CtrlGrid.Add (c_Gh2SpEdit);
		dz++;
	}


	CCtrlInfo *c_Activate     = new CCtrlInfo (&m_Activate, 2, 0, 3, 1); 
	CtrlGrid.Add (c_Activate);

	CCtrlInfo *c_Open     = new CCtrlInfo (&m_Open, 2, 1, 1, 1); 
	CtrlGrid.Add (c_Open);
		CCtrlInfo *c_Print     = new CCtrlInfo (&m_Print, 3, 1, 1, 1); 
		c_Print->SetCellPos (-18, 0);
		CtrlGrid.Add (c_Print);


	CCtrlInfo *c_AlleArtikel     = new CCtrlInfo (&m_AlleArtikel, 1, 3, 99, 99); 
	CtrlGrid.Add (c_AlleArtikel);


	CCtrlInfo *c_SmtGrid     = new CCtrlInfo (&SmtGrid, 1, 4, 99, 99); 
	CtrlGrid.Add (c_SmtGrid);

	if (strcmp (m_List.SchreibModus, "GH") == 0 )
	{
		strcpy(Lst.lst.AlleArtikel,"N");
	}
	else if (strcmp (m_List.SchreibModus, "a_kalkpreis") == 0 )
	{
		strcpy(Lst.lst.AlleArtikel,"N");
	}
	else
	{
		strcpy(Lst.lst.AlleArtikel,"J");
	}
	if (strcmp(Lst.lst.AlleArtikel,"J") == 0)
	{
		SmtGrid.SetVisible (TRUE);
	}
	else
	{
		SmtGrid.SetVisible (FALSE);
	}

	if (!strcmp (m_List.KalkulationsModus, "a_mo_we") == 0)
	{
//		SmtGrid.SetVisible (FALSE);
	    m_AlleArtikel.ShowWindow (SW_HIDE);
	}


/*
	CCtrlInfo *c_List     = new CCtrlInfo (&m_List, 0, 9, DOCKRIGHT, DOCKBOTTOM); 
	CtrlGrid.Add (c_List);
*/
	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Open.Tooltip.SetFont (&Font);
	m_Open.Tooltip.SetSize ();
		m_Print.Tooltip.SetFont (&Font);
		m_Print.Tooltip.SetSize ();
	m_Activate.Tooltip.SetFont (&Font);
	m_Activate.Tooltip.SetSize ();
	m_CmpGroup.SetFont (&lFont);
	m_GrGroup.SetFont (&lFont);

	if (PersName.Trim () != "")
	{
		_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
		if (Sys_ben.dbreadfirstpers_nam () == 0)
		{
			if (Sys_ben.sys_ben.mdn != 0)
			{
				MdnUser.mdn.mdn = Sys_ben.sys_ben.mdn;
				m_MdnUser.SetReadOnly ();
				m_MdnUser.ModifyStyle (WS_TABSTOP, 0);
			}
		}
	}

	Form.Show ();
	CtrlGrid.Display ();
//	MdnUser.mdn.mdn = 1;
//	Mdn = 1;


	CString cSysDate; 
	CStrFuncs::SysDate (cSysDate);  
 	int day = _tstoi (cSysDate.Left (2).GetBuffer ());
	int month = _tstoi (cSysDate.Mid (3, 2).GetBuffer ());
	int year = _tstoi (cSysDate.Right (4).GetBuffer ());
	CTime SysDate (year, month, day, 0, 0, 0);

	__time64_t sysdate = SysDate.GetTime () / (24 * 3600);
	int sYear  = SysDate.GetYear ();
	int sMonth = SysDate.GetMonth ();
	char cjr[5];
	char cmo[3];
	sprintf(cjr,"%d",sYear);
	sprintf(cmo,"%d",sMonth);
	Monat = cmo;
	Jahr = cjr;

	FilGr = EkFilGr;
	Form.Show ();
	ReadMdn ();

	return FALSE;
}

void CCompanyPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

HBRUSH CCompanyPage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			m_Open.SetBkColor(DlgBkColor);
				m_Print.SetBkColor(DlgBkColor);
			m_Activate.SetBkColor(DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (pWnd == &m_CmpGroup)
	{
            pDC->SetTextColor (RGB (0, 0, 255));
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (pWnd == &m_GrGroup)
	{
            pDC->SetTextColor (RGB (0, 0, 255));
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CCompanyPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_TAB) //160210 VK_TAB
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F6)
			{
				OnOpen ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
//				Write ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_MdnUser)
				{
					OnMdnchoice ();
					return TRUE;
				}
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CCompanyPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_MdnUser)
	{
		if (!ReadMdn ())
		{
			m_MdnUser.SetFocus ();
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
	return FALSE;
}

BOOL CCompanyPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
	return FALSE;
}


BOOL CCompanyPage::ReadMdn ()
{
	memcpy (&MdnUser.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdrUser.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (MdnUser.mdn.mdn == 0)
	{
		strcpy (MdnAdrUser.adr.adr_krz, "Zentrale");
		Form.Show ();
		m_MdnUser.SetFocus ();
		m_MdnUser.SetSel (0, -1);
		return TRUE;
	}
	if (MdnUser.dbreadfirst () == 0)
	{
		MdnAdrUser.adr.adr = MdnUser.mdn.adr;
		int dsqlstatus = MdnAdrUser.dbreadfirst ();
		Form.Show ();
		m_MdnUser.SetFocus ();
		m_MdnUser.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),MdnUser.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&MdnUser.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdrUser.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_MdnUser.SetFocus ();
		m_MdnUser.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}


void CCompanyPage::FillMdnGrCombo ()
{
	CFormField *f = Form.GetFormField (&m_MdnGr);
	if (f == NULL) return;
	A_pr.sqlopen (MdnGrCursor);
    f->ComboValues.push_back (new CString (_T("0"))); 
	while (A_pr.sqlfetch (MdnGrCursor) == 0)
	{
			CDbUniCode::DbToUniCode (gr_bz1, (LPSTR) gr_bz1);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), gr,
				                              gr_bz1);
		    f->ComboValues.push_back (ComboValue); 
	}
	A_pr.sqlclose (MdnGrCursor);
	f->FillComboBox ();
	f->SetSel (0);
	f->Get ();
}

void CCompanyPage::FillMdnCombo ()
{
	CFormField *f = Form.GetFormField (&m_Mdn);
	if (f == NULL) return;
	A_pr.sqlopen (MdnCursor);
    f->ComboValues.push_back (new CString (_T("0 Zentrale"))); 
	while (A_pr.sqlfetch (MdnCursor) == 0)
	{
			CDbUniCode::DbToUniCode (adr_krz, (LPSTR) adr_krz);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), mdn,
				                              adr_krz);
		    f->ComboValues.push_back (ComboValue); 
	}
	A_pr.sqlclose (MdnCursor);
	f->FillComboBox ();
	f->SetSel (0);
	f->Get ();
}

void CCompanyPage::FillFilGrCombo ()
{


	CFormField *f = Form.GetFormField (&m_FilGr);
	if (f == NULL) return;
	f->ComboValues.clear ();
	f->Get ();
	f->ComboValues.push_back (new CString (_T("0"))); 
	if (Mdn > 0)
	{
		m_FilGr.EnableWindow (TRUE);
		A_pr.sqlopen (FilGrCursor);
		while (A_pr.sqlfetch (FilGrCursor) == 0)
		{
			CDbUniCode::DbToUniCode (gr_bz1, (LPSTR) gr_bz1);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), gr,
				                              gr_bz1);
		    f->ComboValues.push_back (ComboValue); 
		}
	}
	else
	{
		m_FilGr.EnableWindow (FALSE);
	}
	f->FillComboBox ();
	FilGr = EkFilGr;
	f->Show ();
//	f->Get ();
}

void CCompanyPage::FillFilCombo ()
{
	CFormField *f = Form.GetFormField (&m_Fil);
	if (f == NULL) return;
	f->ComboValues.clear ();
	f->Get ();
	f->ComboValues.push_back (new CString (_T("0 Mandant"))); 
	if (Mdn > 0)
	{
		m_Fil.EnableWindow (TRUE);
		A_pr.sqlopen (FilCursor);
		while (A_pr.sqlfetch (FilCursor) == 0)
		{
			CDbUniCode::DbToUniCode (adr_krz, (LPSTR) adr_krz);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), fil,
				                              adr_krz);
		    f->ComboValues.push_back (ComboValue); 
		}
	}
	else
	{
		m_Fil.EnableWindow (FALSE);
	}
	f->FillComboBox ();
	f->SetSel (0);
	f->Get ();
}

void CCompanyPage::FillListTypCombo ()
{
	CFormField *f = Form.GetFormField (&m_ListTyp);
	if (f == NULL) return;
	f->ComboValues.clear ();
	CString *ComboValue = new CString (_T("0 Preise"));
    f->ComboValues.push_back (ComboValue); 
	ComboValue = new CString (_T("1 Aktionen"));
    f->ComboValues.push_back (ComboValue); 
	ComboValue = new CString (_T("2 Preise und Aktionen"));
    f->ComboValues.push_back (ComboValue); 
	f->FillComboBox ();
	f->SetSel (0);
	f->Get ();
}

void CCompanyPage::FillHwgCombo ()
{
}

void CCompanyPage::FillWgCombo ()
{
}

void CCompanyPage::FillAgCombo ()
{
}

void CCompanyPage::FillTeilSmtCombo ()
{
}

void CCompanyPage::FillATypCombo ()
{
}


void CCompanyPage::OnCbnSelchangeMdn()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	FillFilGrCombo ();
	FillFilCombo ();
	Form.Get ();
	if (Mdn != 0)
	{
		memcpy (&cMdn.mdn, &mdn_null, sizeof (MDN));
		cMdn.mdn.mdn = Mdn;
        cMdn.dbreadfirst ();
        MdnGr = cMdn.mdn.mdn_gr;
		Form.Show ();
	}
}

void CCompanyPage::OnCbnSelchangeFil()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	if (Fil != 0)
	{
		memcpy (&cFil.fil, &fil_null, sizeof (FIL));
		cFil.fil.mdn = Mdn;
		cFil.fil.fil = Fil;
        cFil.dbreadfirst ();
        FilGr = cFil.fil.fil_gr;
		Form.GetFormField (&m_FilGr)->Show ();
	}
}

void CCompanyPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&MdnUser.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdrUser.adr, &adr_null, sizeof (ADR));
		  MdnUser.mdn.mdn = abl->mdn;
		  if (MdnUser.dbreadfirst () == 0)
		  {
			  MdnAdrUser.adr.adr = MdnUser.mdn.adr;
			  MdnAdrUser.dbreadfirst ();
		  }
		  if (MdnUser.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_MdnUser.SetSel (0, -1, TRUE);
		  m_MdnUser.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}


void CCompanyPage::OnHwgChoice ()
{
	Form.Get ();
	if (ChoiceHWG != NULL && !ModalChoiceHWG)
	{
		ChoiceHWG->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceHWG == NULL)
	{
		ChoiceHWG = new CChoiceHWG (this);
	    ChoiceHWG->IsModal = ModalChoiceHWG;
		ChoiceHWG->IdArrDown = IDI_HARROWDOWN;
		ChoiceHWG->IdArrUp   = IDI_HARROWUP;
		ChoiceHWG->IdArrNo   = IDI_HARROWNO;
	    ChoiceHWG->HideEnter = FALSE;
		ChoiceHWG->CreateDlg ();
		ChoiceHWG->SingleSelection = FALSE;
	}

    ChoiceHWG->SetDbClass (&A_bas);

	ChoiceHWG->DoModal();

    if (ChoiceHWG->GetState ())
    {
		  CHWGList *abl; 

		  CString Text = "";
		  if (ChoiceHWG->SelectList.size () > 1)
		  {
				for (std::vector<CHWGList *>::iterator pabl = ChoiceHWG->SelectList.begin (); pabl != ChoiceHWG->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cHwg;
					cHwg.Format (_T("%hd"), abl->hwg);
					Text += cHwg;
				}
	   	  }
		  else
		  {
			  abl = ChoiceHWG->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%hd"), abl->hwg);
		  }
		  m_Hwg.SetWindowText (Text);
    }
}

void CCompanyPage::OnWgChoice ()
{
	Form.Get ();
	if (ChoiceWG != NULL && !ModalChoiceWG)
	{
		ChoiceWG->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceWG == NULL)
	{
		ChoiceWG = new CChoiceWG (this);
	    ChoiceWG->IsModal = ModalChoiceAG;
		ChoiceWG->IdArrDown = IDI_HARROWDOWN;
		ChoiceWG->IdArrUp   = IDI_HARROWUP;
		ChoiceWG->IdArrNo   = IDI_HARROWNO;
	    ChoiceWG->HideEnter = FALSE;
		ChoiceWG->CreateDlg ();
		ChoiceWG->SingleSelection = FALSE;
	}

    ChoiceWG->SetDbClass (&A_bas);

	ChoiceWG->DoModal();

    if (ChoiceWG->GetState ())
    {
		  CWGList *abl; 

		  CString Text = "";
		  if (ChoiceWG->SelectList.size () > 1)
		  {
				for (std::vector<CWGList *>::iterator pabl = ChoiceWG->SelectList.begin (); pabl != ChoiceWG->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cWg;
					cWg.Format (_T("%hd"), abl->wg);
					Text += cWg;
				}
	   	  }
		  else
		  {
			  abl = ChoiceWG->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%hd"), abl->wg);
		  }
		  m_Wg.SetWindowText (Text);
    }
}

void CCompanyPage::OnAgChoice ()
{
	Form.Get ();
	if (ChoiceAG != NULL && !ModalChoiceAG)
	{
		ChoiceAG->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceAG == NULL)
	{
		ChoiceAG = new CChoiceAG (this);
	    ChoiceAG->IsModal = ModalChoiceAG;
		ChoiceAG->IdArrDown = IDI_HARROWDOWN;
		ChoiceAG->IdArrUp   = IDI_HARROWUP;
		ChoiceAG->IdArrNo   = IDI_HARROWNO;
	    ChoiceAG->HideEnter = FALSE;
		ChoiceAG->CreateDlg ();
		ChoiceAG->SingleSelection = FALSE;
	}

    ChoiceAG->SetDbClass (&A_bas);

	ChoiceAG->DoModal();

    if (ChoiceAG->GetState ())
    {
		  CAGList *abl; 

		  CString Text = "";
		  if (ChoiceAG->SelectList.size () > 1)
		  {
				for (std::vector<CAGList *>::iterator pabl = ChoiceAG->SelectList.begin (); pabl != ChoiceAG->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cAg;
					cAg.Format (_T("%ld"), abl->ag);
					Text += cAg;
				}
	   	  }
		  else
		  {
			  abl = ChoiceAG->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%hd"), abl->ag);
		  }
		  m_Ag.SetWindowText (Text);
    }
}
void CCompanyPage::OnArtikelChoice ()
{
	Form.Get ();
	if (ChoiceA != NULL && !ModalChoiceA)
	{
		ChoiceA->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceA == NULL)
	{
		ChoiceA = new CChoiceA (this);
	    ChoiceA->IsModal = ModalChoiceA;
		ChoiceA->IdArrDown = IDI_HARROWDOWN;
		ChoiceA->IdArrUp   = IDI_HARROWUP;
		ChoiceA->IdArrNo   = IDI_HARROWNO;
	    ChoiceA->HideEnter = FALSE;
		ChoiceA->CreateDlg ();
		ChoiceA->SingleSelection = FALSE;
	}

    ChoiceA->SetDbClass (&A_bas);

	ChoiceA->DoModal();

    if (ChoiceA->GetState ())
    {
		  CABasList *abl; 

		  CString Text = "";
		  if (ChoiceA->SelectList.size () > 1)
		  {
				for (std::vector<CABasList *>::iterator pabl = ChoiceA->SelectList.begin (); pabl != ChoiceA->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cA;
					cA.Format (_T("%ld"), abl->a);
					Text += cA;
				}
	   	  }
		  else
		  {
			  abl = ChoiceA->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%.0fl"), abl->a);
		  }
		  m_Artikel.SetWindowText (Text);
    }
}

void CCompanyPage::OnTeilSmtChoice ()
{
	Form.Get ();
	CChoicePtab *ChoicePtab = new CChoicePtab (this);
    ChoicePtab->IsModal = TRUE;
	ChoicePtab->IdArrDown = IDI_HARROWDOWN;
	ChoicePtab->IdArrUp   = IDI_HARROWUP;
	ChoicePtab->IdArrNo   = IDI_HARROWNO;
    ChoicePtab->HideEnter = FALSE;
	ChoicePtab->CreateDlg ();
	ChoicePtab->SingleSelection = FALSE;
    ChoicePtab->Ptitem = _T("teil_smt");
    ChoicePtab->Caption = _T("Auswahl Teilsortiment");

    ChoicePtab->SetDbClass (&A_bas);

	ChoicePtab->DoModal();

    if (ChoicePtab->GetState ())
    {
		  CPtabList *abl; 

		  CString Text = "";
		  if (ChoicePtab->SelectList.size () > 1)
		  {
				for (std::vector<CPtabList *>::iterator pabl = ChoicePtab->SelectList.begin (); pabl != ChoicePtab->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					abl->ptwert.Trim ();
					Text += abl->ptwert;
				}
	   	  }
		  else
		  {
			  abl = ChoicePtab->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text = abl->ptwert.Trim ();
		  }
		  m_TeilSmt.SetWindowText (Text);
    }
}

void CCompanyPage::OnATypChoice ()
{
	Form.Get ();
	CChoicePtab *ChoicePtab = new CChoicePtab (this);
    ChoicePtab->IsModal = TRUE;
	ChoicePtab->IdArrDown = IDI_HARROWDOWN;
	ChoicePtab->IdArrUp   = IDI_HARROWUP;
	ChoicePtab->IdArrNo   = IDI_HARROWNO;
    ChoicePtab->HideEnter = FALSE;
	ChoicePtab->CreateDlg ();
	ChoicePtab->SingleSelection = FALSE;
    ChoicePtab->Ptitem = _T("a_typ");
    ChoicePtab->Caption = _T("Auswahl Artikeltyp");

    ChoicePtab->SetDbClass (&A_bas);

	ChoicePtab->DoModal();

    if (ChoicePtab->GetState ())
    {
		  CPtabList *abl; 

		  CString Text = "";
		  if (ChoicePtab->SelectList.size () > 1)
		  {
				for (std::vector<CPtabList *>::iterator pabl = ChoicePtab->SelectList.begin (); pabl != ChoicePtab->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					abl->ptwert.Trim ();
					Text += abl->ptwert;
				}
	   	  }
		  else
		  {
			  abl = ChoicePtab->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text = abl->ptwert.Trim ();
		  }
		  m_ATyp.SetWindowText (Text);
    }
}

void CCompanyPage::OnVK_F6 ()
{

	if (flgListeAktiv == TRUE)
	{
		CAprUpdateEvent *UpdateEvent = UpdateTab[0];
		UpdateEvent->Schreiben ();
	}

	PostMessage (WM_KEYDOWN, VK_F6, 0l);
}


void CCompanyPage::OnOpen ()
{

	flgListeAktiv = TRUE;
	CString Line = GetCommandLine ();

	CString CmpSelect (_T(""));
	CString Select (_T(""));
	CString SelectPart;
	Form.Get ();

	if (strcmp (m_List.KalkulationsModus, "a_mo_we") == 0)
	{
		if (strcmp(Lst.lst.AlleArtikel,"J") == 0)
		{
			if (strcmp(Lst.lst.HkEdit,"N") == 0)  // wenn Kurznummer nicht angekreuzt ist, sollen nur die S�tze mit Kurznummer erscheinen
			{
				Select += " and ek_pr.hk <> 0 "; 
			}
			if (Jahr.Trim ().GetLength () > 0)
			{
				dbRange.RangeIn ("a_mo_we.jr",Jahr.GetBuffer (), SQLLONG, 0); 
				SelectPart.Format (_T(dbRange.SetRange(" and <a_mo_we.jr> ")));
				Select += SelectPart;
			}
			if (Monat.Trim ().GetLength () > 0)
			{
				dbRange.RangeIn ("a_mo_we.mo",Monat.GetBuffer (), SQLLONG, 0); 
				SelectPart.Format (_T(dbRange.SetRange(" and <a_mo_we.mo> ")));
				Select += SelectPart;
			}
		}
		if (strcmp(Lst.lst.AlleArtikel,"N") == 0)
		{
		 	Select += " and ek_pr.fil == 0 "; //nur damit im select ein "ek_pr" vorkommt
			if (strcmp(Lst.lst.HkEdit,"J") != 0)// wenn Kurznummer nicht angekreuzt ist, sollen nur die S�tze mit Kurznummer erscheinen
			{
				Select += " and ek_pr.hk <> 0 "; 
			}
/* 270510 erst mal raus 
			if (strcmp(Lst.lst.EkEdit,"J") == 0)
			{
				Select += " and ek_pr.pr_ek <> 0 "; 
			}
			if (strcmp(Lst.lst.HkKostEdit,"J") == 0)
			{
				Select += " and ek_pr.hk_kost <> 0 "; 
			}
			if (strcmp(Lst.lst.SkAufsEdit,"J") == 0)
			{
				Select += " and ek_pr.sk_aufs <> 0 "; 
			}
			if (strcmp(Lst.lst.SkEdit,"J") == 0)
			{
				Select += " and ek_pr.sk <> 0 "; 
			}
			if (strcmp(Lst.lst.FilEkAufsEdit,"J") == 0)
			{
				Select += " and ek_pr.fil_ek_aufs <> 0 "; 
			}
			if (strcmp(Lst.lst.FilEkKalkEdit,"J") == 0)
			{
				Select += " and ek_pr.fil_ek_kalk <> 0 "; 
			}
			if (strcmp(Lst.lst.FilEkEdit,"J") == 0)
			{
				Select += " and ek_pr.fil_ek <> 0 "; 
			}
			if (strcmp(Lst.lst.FilVkAufsEdit,"J") == 0)
			{
				Select += " and ek_pr.fil_vk_aufs <> 0 "; 
			}
			if (strcmp(Lst.lst.FilVkKalkEdit,"J") == 0)
			{
				Select += " and ek_pr.fil_vk_kalk <> 0 "; 
			}
			if (strcmp(Lst.lst.FilVkEdit,"J") == 0)
			{
				Select += " and ek_pr.fil_vk <> 0 "; 
			}

			if (strcmp(Lst.lst.Gh1AufsEdit,"J") == 0)
			{
				Select += " and ek_pr.gh1_aufs <> 0 "; 
			}
			if (strcmp(Lst.lst.Gh1KalkVkEdit,"J") == 0)
			{
				Select += " and ek_pr.gh1_kalk_vk <> 0 "; 
			}
			if (strcmp(Lst.lst.Gh1Edit,"J") == 0)
			{
				Select += " and ek_pr.gh1 <> 0 "; 
			}
			if (strcmp(Lst.lst.Gh1SpEdit,"J") == 0)
			{
				Select += " and ek_pr.gh1_sp <> 0 "; 
			}
			if (strcmp(Lst.lst.Gh2AufsEdit,"J") == 0)
			{
				Select += " and ek_pr.gh2_aufs <> 0 "; 
			}
			if (strcmp(Lst.lst.Gh2KalkVkEdit,"J") == 0)
			{
				Select += " and ek_pr.gh2_kalk_vk <> 0 "; 
			}
			if (strcmp(Lst.lst.Gh2Edit,"J") == 0)
			{
				Select += " and ek_pr.gh2 <> 0 "; 
			}
			if (strcmp(Lst.lst.Gh2SpEdit,"J") == 0)
			{
				Select += " and ek_pr.gh2_sp <> 0 "; 
			}
270510 ********/
		}

	}
	if (Hwg.Trim ().GetLength () > 0)
	{
		dbRange.RangeIn ("a_bas.hwg",Hwg.GetBuffer (), SQLLONG, 0); 
//		SelectPart.Format (_T(" and a_bas.hwg in (%s)"), Hwg.GetBuffer ());
		SelectPart.Format (_T(dbRange.SetRange(" and <a_bas.hwg> ")));
		Select += SelectPart;
	}
	if (Wg.Trim ().GetLength () > 0)
	{
		dbRange.RangeIn ("a_bas.wg",Wg.GetBuffer (), SQLLONG, 0); 
//		SelectPart.Format (_T(" and a_bas.wg in (%s)"), Wg.GetBuffer ());
		SelectPart.Format (_T(dbRange.SetRange(" and <a_bas.wg> ")));
		Select += SelectPart;
	}
	if (Ag.Trim ().GetLength () > 0)
	{
		dbRange.RangeIn ("a_bas.ag",Ag.GetBuffer (), SQLLONG, 0); 
//		SelectPart.Format (_T(" and a_bas.ag in (%s)"), Ag.GetBuffer ());
		SelectPart.Format (_T(dbRange.SetRange(" and <a_bas.ag> ")));
		Select += SelectPart;
	}
	if (TeilSmt.Trim ().GetLength () > 0)
	{
		dbRange.RangeIn ("a_bas.teil_smt",TeilSmt.GetBuffer (), SQLLONG, 0); 
//		SelectPart.Format (_T(" and a_bas.teil_smt in (%s)"), TeilSmt.GetBuffer ());
		SelectPart.Format (_T(dbRange.SetRange(" and <a_bas.teil_smt> ")));
		Select += SelectPart;
	}
	if (ATyp.Trim ().GetLength () > 0)
	{
		dbRange.RangeIn ("a_bas.a_typ",ATyp.GetBuffer (), SQLLONG, 0); 
//		SelectPart.Format (_T(" and a_bas.a_typ in (%s)"), ATyp.GetBuffer ());
		SelectPart.Format (_T(dbRange.SetRange(" and <a_bas.a_typ> ")));
		Select += SelectPart;
	}
	if (Artikel.Trim ().GetLength () > 0)
	{
		dbRange.RangeIn ("a_bas.a",Artikel.GetBuffer (), SQLLONG, 0); 
		SelectPart.Format (_T(dbRange.SetRange(" and <a_bas.a> ")));
		Select += SelectPart;
	}
	CAprUpdateEvent *UpdateEvent = UpdateTab[0];
	/***
	if (PrDiff == 0)
	{
		UpdateEvent->SetMdn (Mdn);
		UpdateEvent->Read (Select, MdnGr, Mdn, FilGr, Fil);
	}
	else
	{
	*********/
		/**************** nicht n�tig 
		if (MdnGr != 0)
		{
			SelectPart.Format (_T(" and ek_pr.mdn_gr = %hd"), MdnGr);
			CmpSelect += SelectPart;
		}
		if (Mdn != 0)
		{
			SelectPart.Format (_T(" and ek_pr.mdn = %hd"), Mdn);
			CmpSelect += SelectPart;
		}
		if (FilGr != 0)
		{
			SelectPart.Format (_T(" and ek_pr.fil_gr = %hd"), FilGr);
			CmpSelect += SelectPart;
		}
		if (Fil != 0)
		{
			SelectPart.Format (_T(" and ek_pr.fil = %hd"), Fil);
			CmpSelect += SelectPart;
		}
		*************/

		UpdateEvent->SetMdn (Mdn);
//140908 LuD N Spalten aus-einschalten 		UpdateEvent->SetPrVkVisible (FilGr != EkFilGr);
		UpdateEvent->SetPrVkVisible (m_List.PrVkVisible);
		UpdateEvent->Read (Select, CmpSelect);
//	}
}

void CCompanyPage::OnEnChangeMdnUser()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den __super::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
	if (MdnUser.mdn.mdn != 0)
	{

		Mdn = MdnUser.mdn.mdn;
		CFormField *control = (CFormField *) Form.GetObject () [3];
		control->Show ();
		m_MdnGr.EnableWindow (FALSE);
		m_Mdn.EnableWindow (FALSE);
		m_FilGr.EnableWindow ();
		m_Fil.EnableWindow ();
		FillFilGrCombo ();
		FillFilCombo ();
	}
	else
	{
		m_MdnGr.EnableWindow ();
		m_Mdn.EnableWindow ();
	}
}




int  CCompanyPage::iv_prschreiben ( char * aktdatum , int ityp )
{



	CString cSysDate; 
	CStrFuncs::SysDate (cSysDate);  
	
	CString cAktdatum ;
	cAktdatum = _T(aktdatum) ;

	CString cSysTime; 
	CStrFuncs::SysTime (cSysTime);  


	if (EkLesCursor <  0 )
	{

    Ek_pr.sqlin ((short *)   &Ek_pr.ek_pr.mdn, SQLSHORT, 0);
//       sqlin ((double *)  &ek_pr.a, SQLDOUBLE, 0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.a,SQLDOUBLE,0);
    Ek_pr.sqlout ((short *) &Ek_pr.ek_pr.fil,SQLSHORT,0);
    Ek_pr.sqlout ((short *) &Ek_pr.ek_pr.mdn,SQLSHORT,0);
    Ek_pr.sqlout ((short *) &Ek_pr.ek_pr.akt,SQLSHORT,0);
    Ek_pr.sqlout ((DATE_STRUCT *) &Ek_pr.ek_pr.bearb,SQLDATE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.pr_ek,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.hk_kost,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.hk,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.sk_aufs,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.sk,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.fil_ek_aufs,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.fil_ek_kalk,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.fil_ek,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.fil_vk_aufs,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.fil_vk_kalk,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.fil_vk,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.gh1_aufs,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.gh1_kalk_vk,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.gh1,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.gh1_sp,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.gh2_aufs,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.gh2_kalk_vk,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.gh2,SQLDOUBLE,0);
    Ek_pr.sqlout ((double *) &Ek_pr.ek_pr.gh2_sp,SQLDOUBLE,0);


	EkLesCursor = Ek_pr.sqlcursor (_T("select ek_pr.a,  ek_pr.fil,  ")
_T("ek_pr.mdn,  ek_pr.akt,  ek_pr.bearb,  ek_pr.pr_ek,  ek_pr.hk_kost,  ")
_T("ek_pr.hk,  ek_pr.sk_aufs,  ek_pr.sk,  ek_pr.fil_ek_aufs,  ")
_T("ek_pr.fil_ek_kalk,  ek_pr.fil_ek,  ek_pr.fil_vk_aufs,  ")
_T("ek_pr.fil_vk_kalk,  ek_pr.fil_vk,  ek_pr.gh1_aufs,  ek_pr.gh1_kalk_vk,  ")
_T("ek_pr.gh1,  ek_pr.gh1_sp,  ek_pr.gh2_aufs,  ek_pr.gh2_kalk_vk,  ")
_T("ek_pr.gh2,  ek_pr.gh2_sp from ek_pr ")
       _T("where mdn = ?  and akt = 0 order by a " ) ) ;

	}

	if ( Ek_prpr.ins_cursor < 0 )
			Ek_prpr.prepare() ;

	if ( Iv_pr.ins_cursor < 0 )
			Iv_pr.prepare() ;


	Ek_pr.ek_pr.mdn = MdnUser.mdn.mdn ;



	memcpy (&Iv_pr.iv_pr, &iv_pr_null, sizeof (IV_PR));
		
	Iv_pr.iv_pr.waehrung = 2 ;
	Iv_pr.iv_pr.kun = 0 ;
	Iv_pr.iv_pr.ld_pr = 0.0 ;	// z.Z haben ohnehin alle ipr-Eintraege in diesen Listen 0-Laden-Preis drin 
	Iv_pr.iv_pr.ld_pr_eu = 0.0 ;
	Iv_pr.ToDbDate (cAktdatum , &Iv_pr.iv_pr.gue_ab);

	int retcode = 0 ;

	if ( ityp == 0 )	// B1-Preise fuer Muefli 
	{

		Ek_prpr.beginwork () ;
		Ek_pr.sqlopen (EkLesCursor) ;

		while (Ek_pr.sqlfetch (EkLesCursor) == 0)
		{
			// der kurze Artikel steht im Feld hk, der lange in gh2 , der Preis dazu  : pr_ek + hk_kost = sk  
			// dahin sollen die Preise :
			// kurze UND lange Nummern : mdn3 + preisliste 1320 
			// lange Nummern           : mdn4 , prgr 4 


			if ( ( Ek_pr.ek_pr.hk < 0.01 ) && ( Ek_pr.ek_pr.gh2 < 0.01 ) )
			{
				continue ;	// Nix schreiben
			}
			if ( Ek_pr.ek_pr.sk < 0.01 )
			{
				continue ;	// Nix schreiben
			}

			retcode = 1 ;

			Ek_pr.ek_pr.fil_ek = -1.0 ;	// Marker : B1-Preise

			memcpy (&Ek_prpr.ek_prpr, &ek_prpr_null, sizeof (EK_PRPR));

			Ek_prpr.ek_prpr.mdn = Ek_pr.ek_pr.mdn ;
			Ek_prpr.ek_prpr.fil = Ek_pr.ek_pr.fil ;

			Ek_prpr.ek_prpr.akt   = Ek_pr.ek_pr.akt ;
			Ek_prpr.ek_prpr.bearb = Ek_pr.ek_pr.bearb ;

			Ek_prpr.ek_prpr.a = Ek_pr.ek_pr.a ;
			Ek_prpr.ek_prpr.hk = Ek_pr.ek_pr.hk ;
			Ek_prpr.ek_prpr.gh2 = Ek_pr.ek_pr.gh2 ;

			Ek_prpr.ek_prpr.hk_kost = Ek_pr.ek_pr.hk_kost ;
			Ek_prpr.ek_prpr.sk_aufs = Ek_pr.ek_pr.sk_aufs ;

			Ek_prpr.ek_prpr.pr_ek = Ek_pr.ek_pr.pr_ek ;
			Ek_prpr.ek_prpr.fil_ek = Ek_pr.ek_pr.fil_ek ;
			Ek_prpr.ek_prpr.sk = Ek_pr.ek_pr.sk ;

			Ek_prpr.ToDbDate (cSysDate  , &Ek_prpr.ek_prpr.akv);
			Ek_prpr.ToDbDate (cAktdatum , &Ek_prpr.ek_prpr.gue_ab);

			sprintf ( Ek_prpr.ek_prpr.zeit , "%s" , cSysTime.GetBuffer() ) ;
			sprintf ( Ek_prpr.ek_prpr.pers_nam , "%s" , PersName.GetBuffer() );
			
			int insretcode = Ek_prpr.sqlexecute ( Ek_prpr.ins_cursor ) ;


			Iv_pr.iv_pr.a = Ek_pr.ek_pr.hk ;
			Iv_pr.iv_pr.vk_pr_i  = Ek_pr.ek_pr.sk ;
			Iv_pr.iv_pr.vk_pr_eu  = Ek_pr.ek_pr.sk ;
			if ( Iv_pr.iv_pr.a > 0.01 && ! insretcode )
			{	
				Iv_pr.iv_pr.mdn = 3 ;
				Iv_pr.iv_pr.pr_gr_stuf = 0 ;
				Iv_pr.iv_pr.kun_pr = 1320 ;

				insretcode = Iv_pr.sqlexecute ( Iv_pr.del_cursor ) ;
				insretcode = Iv_pr.sqlexecute ( Iv_pr.ins_cursor ) ;
			}

			Iv_pr.iv_pr.a = Ek_pr.ek_pr.gh2 ;
			if ( Iv_pr.iv_pr.a > 0.01 && ! insretcode )
			{

				Iv_pr.iv_pr.mdn = 3 ;
				Iv_pr.iv_pr.pr_gr_stuf = 0 ;
				Iv_pr.iv_pr.kun_pr = 1320 ;
				insretcode = Iv_pr.sqlexecute ( Iv_pr.del_cursor ) ;
				insretcode = Iv_pr.sqlexecute ( Iv_pr.ins_cursor ) ;
			}
			if ( Iv_pr.iv_pr.a > 0.01 && ! insretcode )
			{
				Iv_pr.iv_pr.mdn = 4 ;
				Iv_pr.iv_pr.pr_gr_stuf = 4 ;
				Iv_pr.iv_pr.kun_pr = 0 ;
				insretcode = Iv_pr.sqlexecute ( Iv_pr.del_cursor ) ;
				insretcode = Iv_pr.sqlexecute ( Iv_pr.ins_cursor ) ;
			}

			if ( insretcode )
			{
				retcode = 0 ;
				break ;
			}

		}
		if ( retcode == 0 )
			Ek_prpr.rollbackwork () ;
		else
			Ek_prpr.commitwork () ;

// Mist - dropped den ganzen Cursor ->		Ek_pr.sqlclose (EkLesCursor);
	}

	if ( ityp == 1 )	// GH-Preise f�r Muefli
	{
		// der kurze Artikel steht im Feld hk, der lange  steht im Feld gh2,
		//der Preis dazu : pr_ek + sk_aufs = fil_ek 
		// dahin sollen die Preise :
		// kurze Nummern : mdn1 + prgr 0 
		// lange Nummern : mdn4 , prgr 0 

		Ek_prpr.beginwork () ;

		Ek_pr.sqlopen (EkLesCursor);

		while (Ek_pr.sqlfetch (EkLesCursor) == 0)
		{

			if ( ( Ek_pr.ek_pr.hk < 0.01 ) && ( Ek_pr.ek_pr.gh2 < 0.01 ) )
			{
				continue ;	// Nix schreiben
			}
			if ( Ek_pr.ek_pr.fil_ek < 0.01 )
			{
				continue ;	// Nix schreiben
			}

			retcode = 1 ;
// Fast wie oben Anfang .......

			Ek_pr.ek_pr.sk = -1.0 ;	// Marker : GH-Preise

			memcpy (&Ek_prpr.ek_prpr, &ek_prpr_null, sizeof (EK_PRPR));

			Ek_prpr.ek_prpr.mdn = Ek_pr.ek_pr.mdn ;
			Ek_prpr.ek_prpr.fil = Ek_pr.ek_pr.fil ;

			Ek_prpr.ek_prpr.akt   = Ek_pr.ek_pr.akt ;
			Ek_prpr.ek_prpr.bearb = Ek_pr.ek_pr.bearb ;

			Ek_prpr.ek_prpr.a = Ek_pr.ek_pr.a ;
			Ek_prpr.ek_prpr.hk = Ek_pr.ek_pr.hk ;
			Ek_prpr.ek_prpr.gh2 = Ek_pr.ek_pr.gh2 ;

			Ek_prpr.ek_prpr.hk_kost = Ek_pr.ek_pr.hk_kost ;
			Ek_prpr.ek_prpr.sk_aufs = Ek_pr.ek_pr.sk_aufs ;

			Ek_prpr.ek_prpr.pr_ek = Ek_pr.ek_pr.pr_ek ;
			Ek_prpr.ek_prpr.fil_ek = Ek_pr.ek_pr.fil_ek ;
			Ek_prpr.ek_prpr.sk = Ek_pr.ek_pr.sk ;

			Ek_prpr.ToDbDate (cSysDate  , &Ek_prpr.ek_prpr.akv);
			Ek_prpr.ToDbDate (cAktdatum , &Ek_prpr.ek_prpr.gue_ab);

			sprintf ( Ek_prpr.ek_prpr.zeit , "%s" , cSysTime.GetBuffer() ) ;
			sprintf ( Ek_prpr.ek_prpr.pers_nam , "%s" , PersName.GetBuffer() );
			
			int insretcode = Ek_prpr.sqlexecute ( Ek_prpr.ins_cursor ) ;

			Iv_pr.iv_pr.a = Ek_pr.ek_pr.hk ;
			Iv_pr.iv_pr.vk_pr_i  = Ek_pr.ek_pr.fil_ek ;
			Iv_pr.iv_pr.vk_pr_eu  = Ek_pr.ek_pr.fil_ek ;

			if ( Iv_pr.iv_pr.a > 0.01 && ! insretcode )
			{	
				Iv_pr.iv_pr.mdn = 1 ;
				Iv_pr.iv_pr.pr_gr_stuf = 0 ;
				Iv_pr.iv_pr.kun_pr = 0 ;

				insretcode = Iv_pr.sqlexecute ( Iv_pr.del_cursor ) ;
				insretcode = Iv_pr.sqlexecute ( Iv_pr.ins_cursor ) ;
			}

			Iv_pr.iv_pr.a = Ek_pr.ek_pr.gh2 ;
			if ( Iv_pr.iv_pr.a > 0.01 && ! insretcode )
			{

				Iv_pr.iv_pr.mdn = 4 ;
				Iv_pr.iv_pr.pr_gr_stuf = 0 ;
				Iv_pr.iv_pr.kun_pr = 0 ;
				insretcode = Iv_pr.sqlexecute ( Iv_pr.del_cursor ) ;
				insretcode = Iv_pr.sqlexecute ( Iv_pr.ins_cursor ) ;
			}

			if ( insretcode )
			{
				retcode = 0 ;
				break ;
			}

		}

// Fast wie oben Ende .......

		if ( retcode == 0 )
			Ek_prpr.rollbackwork () ;
		else
			Ek_prpr.commitwork () ;
		Ek_prpr.commitwork () ;
// Mist -> droopped den ganzen Cursor 		Ek_pr.sqlclose (EkLesCursor);

	}

	return retcode ;
}

int CCompanyPage::preiseschreiben ( char * typ , char * edatstring,int tag, int monat, int jahr )
{
	int ityp = -1 ;
	if ( typ[0] == 'h' ) ityp = 0 ;
	if ( typ[0] == 'g' ) ityp = 1 ;


	CdatDial datdial;

	datdial.puttyp(ityp) ;
	datdial.putdatum( edatstring ,tag,monat,jahr) ;
	datdial.DoModal();
	int retcode = datdial.getretcode() ;
	if ( retcode )
	{
		datdial.getdatum( edatstring ) ;

		retcode = iv_prschreiben( edatstring, ityp ) ;
		return retcode ;
	}

	return 0 ;	
}

void offdatum ( char * input , int offset, int *ptag , int *pmonat, int *pjahr  ) 
{


  char hilfe5 [5] ;
  int tag,monat,jahr ;

  hilfe5[0] = input[0] ;
	hilfe5[1] = input[1] ;
	hilfe5[2] = '\0' ;
	tag = atoi ( hilfe5 ) ;

	hilfe5[0] = input[3] ;
	hilfe5[1] = input[4] ;
	hilfe5[2] = '\0' ;
	monat = atoi ( hilfe5 ) ;

	hilfe5[0] = input[6] ;
	hilfe5[1] = input[7] ;
	hilfe5[2] = input[8] ;
	hilfe5[3] = input[9] ;
	hilfe5[4] = '\0' ;
	jahr = atoi ( hilfe5 ) ;

	*ptag = tag ;
	*pmonat = monat ;
	*pjahr = jahr ;


	if ( offset == 0 )
	      return ;

	if ( offset < 0 )
	{
		while ( offset < 0 )
		{
			tag -- ;
			offset ++ ;
			if ( tag < 1 )
			{
				monat -- ;
				if ( monat < 1 )
				{
					jahr -- ;
					monat = 12 ;
					tag = 31 ;
					continue ;	// Dezember extra gehandelt
				}

				switch ( monat )
				{
					case  4 :
					case  6 :
					case  9 :
					case 11 :
					tag = 30 ;
					break ;
				case 2 :
					if ( ! ( jahr % 4) )
						 tag = 29 ;
					else
						tag = 28 ;
					break ;
			default :
					tag = 31 ;
					break ;
				}
			}
		}

		offset = 0 ;	// Notbremse
	}

	while ( offset >  0 )
	{
		tag ++ ;
		offset -- ;
		if ( tag < 29 ) continue ;
	
		switch ( monat )
		{
			case  4 :
			case  6 :
			case  9 :
			case 11 :
				if ( tag  > 30 )
				{
					monat ++ ;
					tag = 1 ; ;
				}
				break ;
			case 2 :
				if ( tag > 28 )	// IM Jahr 2000 geht dieser Check schief .....
				{
					if ( ! ( jahr % 4) )
					{
						if ( tag > 29 )
						{
							monat ++ ;
							tag = 1  ;
						}
					}
					else
					{
						monat ++ ;
						tag = 1  ;
					};
				}
				break ;
			default :
				if ( tag > 31 )
				{
					monat ++ ;
					tag = 1 ;
					if ( monat > 12 )
					{
						jahr ++ ;
						monat = 1 ;
					}
				}
		}
	}

	*ptag = tag ;
	*pmonat = monat ;
	*pjahr = jahr ;

	sprintf ( input, "%02d.%02d.%04d", tag,monat,jahr ) ;
}

void CCompanyPage::OnActivate ()
{
	CProcess activate;
	CString Command;
	CString bws;
	char cmdn[4];

	sprintf (cmdn,"%hd",Mdn);
	CString mandant = cmdn;
	bws.GetEnvironmentVariable (_T("BWS"));
	if (bws == "")
	{
		AfxMessageBox (_T("Die Preise k�nnen nicht aktiviert werden"));
		return;
	}

		if (flgListeAktiv == FALSE) return;
	CAprUpdateEvent *UpdateEvent = UpdateTab[0];
	UpdateEvent->AktSetzen ();
	UpdateEvent->Schreiben ();

	if (strcmp (m_List.KalkulationsModus, "a_mo_we") == 0)
	{

		CString cDatum ;
		CStrFuncs::SysDate( cDatum ) ;
		char datstring[22] ;
		char *S= cDatum.GetBuffer(20) ; 
		sprintf ( datstring,"%s",cDatum.GetBuffer()) ;
		int tag ,monat,jahr ;
		offdatum ( datstring , 1 , &tag, &monat, &jahr ) ;


		int ergebnis = 0 ;
		if (strcmp(Lst.lst.HkKostEdit,"J") == 0) 
		{

			if ( MessageBox(_T("B1-Preise aktivieren ??"), " ", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 ) == IDYES )
			{
				ergebnis = preiseschreiben("hk",datstring,tag,monat,jahr) ;
				if ( ! ergebnis )
				{
					AfxMessageBox (_T("B1-Preise aktivieren war nicht erfolgreich "));
					return;
				}
				else
				{
					AfxMessageBox (_T("B1-Preise wurden zur Aktivierung eingetragen "));
				};
			}
			else
					AfxMessageBox (_T("B1-Preise aktivieren wurden nicht aktiviert "));

		}

		if (strcmp(Lst.lst.SkAufsEdit ,"J") == 0) 
		{

			if ( MessageBox(_T("GH-Preise aktivieren ?? "), " ", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 ) == IDYES )
			{
				ergebnis = preiseschreiben("gh",datstring,tag,monat,jahr) ;
				if ( ! ergebnis )
				{
					AfxMessageBox (_T("GH-Preise aktivieren war nicht erfolgreich "));
					return;
				} 
				else
				{
					AfxMessageBox (_T("GH-Preise wurden zur Aktivierung eingetragen "));
				};
			}
			else
					AfxMessageBox (_T("GH-Preise wurden nicht aktiviert "));

		}

		return ;
	}

// else : bisheriger Ablauf 




	Command.Format (_T("%s\\BIN\\ekpreise_batch\\ToAKalkPreis.bat"), bws);
	activate.SetCommand (Command);
	HANDLE pid = activate.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		AfxMessageBox (_T("Die Preise k�nnen nicht aktiviert werden"));
		return;
	}
	if (activate.WaitForEnd () != 0)
	{
		AfxMessageBox (_T("Fehler beim Aktivieren der EK-Preise"));
	}
	else
	{
		// Filialpreiskalkulation : Aufruf 26630 <mdn> <fil_gr> <fil> <g�ltig> <dpr_akt_fil_ek> ek
		Command.Format (_T(m_List.Kalkulationsaufruf), mandant);
//		Command.Format (_T("rswrun 26630 batch %s 0 0 J ek"), mandant);
		activate.SetCommand (Command);
		HANDLE pid = activate.Start (SW_SHOWNORMAL);
		if (pid == NULL)
		{
			AfxMessageBox (_T("Die Kalkulation konnte nicht gestartet werden"));
			return;
		}
		if (activate.WaitForEnd () != 0)
		{
			AfxMessageBox (_T("Fehler bei der Kalkulation der EK-Preise"));
		}
		else
		{
//			AfxMessageBox (_T("EK-Preise wurdern kalkuliert und aktiviert"));
			OnOpen ();

		}
	}
}

void CCompanyPage::OnPrint ()
{
	Print ();
	return ;
}

BOOL CCompanyPage::Print ()
{

	CProcess print;
	CString Command;

	CString CmpSelect (_T(""));
	CString Select (_T(""));
	CString SelectPart;
	Form.Get ();

	if (flgListeAktiv == TRUE)
	{
		CAprUpdateEvent *UpdateEvent = UpdateTab[0];
		UpdateEvent->DruckSetzen ();
		UpdateEvent->Schreiben ();
	}


	/****
	OnCbnSelchangeFil();
	if (Hwg.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.hwg in (%s)"), Hwg.GetBuffer ());
		Select += SelectPart;
	}
	if (Wg.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.wg in (%s)"), Wg.GetBuffer ());
		Select += SelectPart;
	}
	if (Ag.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.ag in (%s)"), Ag.GetBuffer ());
		Select += SelectPart;
	}
	if (TeilSmt.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.teil_smt in (%s)"), TeilSmt.GetBuffer ());
		Select += SelectPart;
	}
	if (ATyp.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.a_typ in (%s)"), ATyp.GetBuffer ());
		Select += SelectPart;
	}
	CAprUpdateEvent *UpdateEvent = UpdateTab[0];
	if (PrDiff == 0)
	{
		if (!Print (Select, MdnGr, Mdn, FilGr, Fil))
		{
			MessageBox (_T("Liste kann nicht gedruckt werden"));
			return FALSE;
		}
	}
	else
	{
		if (MdnGr != 0)
		{
			SelectPart.Format (_T(" and ek_pr.mdn_gr = %hd"), MdnGr);
			CmpSelect += SelectPart;
		}
		if (Mdn != 0)
		{
			SelectPart.Format (_T(" and ek_pr.mdn = %hd"), Mdn);
			CmpSelect += SelectPart;
		}
		if (FilGr != 0)
		{
			SelectPart.Format (_T(" and ek_pr.fil_gr = %hd"), FilGr);
			CmpSelect += SelectPart;
		}
		if (Fil != 0)
		{
			SelectPart.Format (_T(" and ek_pr.fil = %hd"), Fil);
			CmpSelect += SelectPart;
		}
		if (!Print (Select, CmpSelect))
		{
			MessageBox (_T("Liste kann nicht gedruckt werden"));
			return FALSE;
		}
	}
	***************/

	LPTSTR tmp = getenv ("TMPPATH");
	CString dName;
	FILE *fp;

	switch (ListType)
	{
	case Preise:
		if (tmp != NULL)
		{
//			dName.Format ("%s\\ek_pr.llf", tmp);
			dName.Format ("%s\\%s.llf", tmp,ProgNam.GetBuffer());
		}
		else
		{
//			dName = "ek_pr.llf";
			dName.Format ("%s.llf", ProgNam.GetBuffer());
		}
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
//			fprintf (fp, "NAME ek_pr\n");
			fprintf (fp, "NAME %s\n",ProgNam.GetBuffer());
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "druck 1 1\n");
			fclose (fp);
//			Command.Format ("dr70001 -name ek_pr -datei %s", dName.GetBuffer ());
			Command.Format ("dr70001 -name %s -datei %s", ProgNam.GetBuffer(),dName.GetBuffer ());
		}
		break;
	case Aktionen:
		if (tmp != NULL)
		{
			dName.Format ("%s\\1120ca.llf", tmp);
		}
		else
		{
			dName = "1120ca.llf";
		}
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			short hwg       = _tstoi (Hwg.GetBuffer ());
			short wg        = _tstoi (Wg.GetBuffer ());
			long  ag        = _tstol (Ag.GetBuffer ());
			short teil_smt  = _tstoi (TeilSmt.GetBuffer ());
			short a_typ     = _tstoi (ATyp.GetBuffer ());
			fprintf (fp, "NAME 1120ca\n");
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "cmdn %hd %hd\n",Mdn, Mdn);
			fprintf (fp, "cfil_gr %hd %hd\n",FilGr, FilGr);
			fprintf (fp, "cfil %hd %hd\n",Fil, Fil);
			fprintf (fp, "hwg %hd %hd\n",hwg, hwg);
			fprintf (fp, "hwg %hd %hd\n",wg, wg);
			fprintf (fp, "wg %ld %ld\n",ag, ag);
			fprintf (fp, "ag %hd %hd\n",teil_smt, teil_smt);
			fclose (fp);
			Command.Format ("dr70001 -name 1120ca -datei %s", dName.GetBuffer ());
		}
		break;
	case PreiseAktionen:
		if (tmp != NULL)
		{
			dName.Format ("%s\\1120cpa.llf", tmp);
		}
		else
		{
			dName = "1120cpa.llf";
		}
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			short hwg       = _tstoi (Hwg.GetBuffer ());
			short wg        = _tstoi (Wg.GetBuffer ());
			long  ag        = _tstol (Ag.GetBuffer ());
			short teil_smt  = _tstoi (TeilSmt.GetBuffer ());
			short a_typ     = _tstoi (ATyp.GetBuffer ());
			fprintf (fp, "NAME 1120cpa\n");
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "cmdn %hd %hd\n",Mdn, Mdn);
			fprintf (fp, "cfil_gr %hd %hd\n",FilGr, FilGr);
			fprintf (fp, "cfil %hd %hd\n",Fil, Fil);
			fprintf (fp, "hwg %hd %hd\n",hwg, hwg);
			fprintf (fp, "wg %hd %hd\n",wg, wg);
			fprintf (fp, "ag %ld %ld\n",ag, ag);
			fprintf (fp, "teil_smt %hd %hd\n",teil_smt, teil_smt);
			fclose (fp);
			Command.Format ("dr70001 -name 1120cpa -datei %s", dName.GetBuffer ());
		}
		break;
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;

}

BOOL CCompanyPage::Print (CString& Select, CString& CmpSelect)
{
	/****************************
	CString Sql;
	int AprCursor;
	int sql_s;
	extern short sql_mode;

	sql_s = sql_mode;

	sql_mode = 1;

	int i = 0;
	A_pr_list.beginwork ();
	while (A_pr_list.sqlcomm ("delete from a_pr_list") < 0)
	{
		if ( i == 100)
		{
			return FALSE;
		}
		Sleep (50);
		i ++;
	}

	Sql = _T("select a_pr.a, a_pr.mdn_gr, a_pr.mdn, a_pr.fil_gr, a_pr.fil from a_pr, a_bas ")
		  _T("where a_pr.a > 0 and a_bas.a = a_pr.a");
	Sql += _T( " ");
	if (CmpSelect.GetLength () > 0)
	{
		Sql += CmpSelect;
		Sql += _T( " ");
	}
	if (Select.GetLength () > 0)
	{
		Sql += Select;
		Sql += _T( " ");
	}

	Sql += _T("order by a_pr.a, a_pr.mdn_gr, a_pr.mdn, a_pr.fil_gr, a_pr.fil");
    A_pr.sqlout ((double *) &A_pr.a_pr.a, SQLDOUBLE, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.mdn_gr, SQLSHORT, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.mdn, SQLSHORT, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.fil_gr, SQLSHORT, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.fil, SQLSHORT, 0);  
    AprCursor = A_pr.sqlcursor (Sql.GetBuffer ());

	sql_mode = sql_s;
	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	A_pr.sqlopen (AprCursor);
	while (A_pr.sqlfetch (AprCursor) == 0)
	{
		A_pr.dbreadfirst ();
		memcpy (&A_pr_list.a_pr_list, &a_pr_list_null, sizeof (A_PR_LIST));

		A_pr_list.a_pr_list.cmdn_gr  = MdnGr; 
		A_pr_list.a_pr_list.cmdn     = Mdn; 
		A_pr_list.a_pr_list.cfil_gr  = FilGr; 
		A_pr_list.a_pr_list.cfil     = Fil; 
		A_pr_list.a_pr_list.hwg      = _tstoi (Hwg.GetBuffer ()); 
		A_pr_list.a_pr_list.wg       = _tstoi (Wg.GetBuffer ()); 
		A_pr_list.a_pr_list.ag       = _tstol (Ag.GetBuffer ()); 
		A_pr_list.a_pr_list.teil_smt = _tstoi (TeilSmt.GetBuffer ()); 
		A_pr_list.a_pr_list.a_typ    = _tstoi (ATyp.GetBuffer ()); 

		A_pr_list.a_pr_list.a      = A_pr.a_pr.a; 
		A_pr_list.a_pr_list.mdn_gr = A_pr.a_pr.mdn_gr; 
		A_pr_list.a_pr_list.mdn    = A_pr.a_pr.mdn; 
		A_pr_list.a_pr_list.fil_gr = A_pr.a_pr.fil_gr; 
		A_pr_list.a_pr_list.fil    = A_pr.a_pr.fil; 
		A_pr_list.a_pr_list.pr_ek  = A_pr.a_pr.pr_ek_euro; 
		A_pr_list.a_pr_list.pr_vk  = A_pr.a_pr.pr_vk_euro; 
		if (ListType == Aktionen)
		{
			short sa = GetAktPrice (A_pr.a_pr.a, A_pr.a_pr.mdn_gr, A_pr.a_pr.mdn, A_pr.a_pr.fil_gr, A_pr.a_pr.fil);
			if (sa == 1 && A_pr_list.a_pr_list.lad_akt_von.day != 0)
			{
				A_pr_list.dbupdate ();
//				A_pr_list.dbinsert_nulls ();
			}
		}
		else if (ListType == PreiseAktionen)
		{
			short sa = GetAktPrice (A_pr.a_pr.a, A_pr.a_pr.mdn_gr, A_pr.a_pr.mdn, A_pr.a_pr.fil_gr, A_pr.a_pr.fil);
			if (sa == 1 && A_pr_list.a_pr_list.lad_akt_von.day != 0)
			{
				A_pr_list.dbupdate ();
			}
			else
			{
				A_pr_list.dbinsert_nulls ();
			}
		}
		else
		{
			A_pr_list.dbinsert_nulls ();
		}

	}
	A_pr.sqlclose (AprCursor);
	A_pr_list.commitwork ();
	sql_mode = sql_s;
	******************/
	return TRUE;
}

BOOL CCompanyPage::Print (CString& Select, short mdn_gr, short mdn, short fil_gr, short fil)
{
	/********************************
	CPrice Price;
	CString Sql;
	int AprCursor;

	int sql_s;
	extern short sql_mode;

	sql_s = sql_mode;

	sql_mode = 1;

	int i = 0;
	A_pr_list.beginwork ();
	while (A_pr_list.sqlcomm ("delete from a_pr_list") < 0)
	{
		if ( i == 100)
		{
			return FALSE;
		}
		Sleep (50);
		i ++;
	}

	Sql = _T("select distinct (a_pr.a) from a_pr, a_bas ")
		  _T("where a_pr.a > 0 and a_bas.a = a_pr.a ");
	Sql += _T( " ");
	if (Select.GetLength () > 0)
	{
		Sql += Select;
		Sql += _T( " ");
	}

	Sql += _T("order by a_pr.a");
    A_pr.sqlout ((double *) &A_pr.a_pr.a, SQLDOUBLE, 0);  
    AprCursor = A_pr.sqlcursor (Sql.GetBuffer ());

	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	A_pr.sqlopen (AprCursor);
	while (A_pr.sqlfetch (AprCursor) == 0)
	{
		Price.a_pr.a      = A_pr.a_pr.a;
		Price.a_pr.mdn_gr = mdn_gr;
		Price.a_pr.mdn    = mdn;
		Price.a_pr.fil_gr = fil_gr;
		Price.a_pr.fil    = fil;
		cFil.fil.mdn      = Price.a_pr.mdn;
		cFil.fil.fil      = Price.a_pr.fil;
		if (cFil.dbreadfirst () == 0)
		{
			Price.a_pr.fil_gr = cFil.fil.fil_gr;
		}
		if (Price.dbfetchpreis () != NULL) continue;
		memcpy (&A_pr_list.a_pr_list, &a_pr_list_null, sizeof (A_PR_LIST));
		A_pr_list.a_pr_list.cmdn_gr  = mdn_gr; 
		A_pr_list.a_pr_list.cmdn     = mdn; 
		A_pr_list.a_pr_list.cfil_gr  = fil_gr; 
		A_pr_list.a_pr_list.cfil     = fil; 
		A_pr_list.a_pr_list.hwg      = _tstoi (Hwg.GetBuffer ()); 
		A_pr_list.a_pr_list.wg       = _tstoi (Wg.GetBuffer ()); 
		A_pr_list.a_pr_list.ag       = _tstol (Ag.GetBuffer ()); 
		A_pr_list.a_pr_list.teil_smt = _tstoi (TeilSmt.GetBuffer ()); 
		A_pr_list.a_pr_list.a_typ    = _tstoi (ATyp.GetBuffer ()); 

		A_pr_list.a_pr_list.a       = Price.a_pr.a; 
		A_pr_list.a_pr_list.mdn_gr = Price.a_pr.mdn_gr; 
		A_pr_list.a_pr_list.mdn    = Price.a_pr.mdn; 
		A_pr_list.a_pr_list.fil_gr = Price.a_pr.fil_gr; 
		A_pr_list.a_pr_list.fil    = Price.a_pr.fil; 
		A_pr_list.a_pr_list.pr_ek  = Price.a_pr.pr_ek_euro;
		A_pr_list.a_pr_list.pr_vk  = Price.a_pr.pr_vk_euro; 
		if (ListType == Aktionen)
		{
			short sa = GetAktPrice (A_pr.a_pr.a, mdn_gr, mdn, fil_gr, fil);
			if (sa == 1 && A_pr_list.a_pr_list.lad_akt_von.day != 0)
			{
				A_pr_list.dbupdate ();
//				A_pr_list.dbinsert_nulls ();
			}
		}
		else if (ListType == PreiseAktionen)
		{
			short sa = GetAktPrice (A_pr.a_pr.a, mdn_gr, mdn, fil_gr, fil);
			if (sa == 1 && A_pr_list.a_pr_list.lad_akt_von.day != 0)
			{
				A_pr_list.dbupdate ();
			}
			else
			{
				A_pr_list.dbinsert_nulls ();
			}
		}
		else
		{
			A_pr_list.dbinsert_nulls ();
		}
	}
	A_pr.sqlclose (AprCursor);
	A_pr_list.commitwork ();
	*****************************/
	return TRUE;
}

/**********
short CCompanyPage::GetAktPrice (double a, short mdn_gr, short mdn, short fil_gr, short fil)
{
	short sa = 0;
	double pr_ek;
	double pr_vk;

	if (DllPreise.PriceLib != NULL && DllPreise.fetch_preis_tag != NULL)
	{
		CString Sysdate;
		DB_CLASS::Sysdate (Sysdate);
		sa = (*DllPreise.fetch_preis_tag) (mdn_gr, mdn, 
			                               fil_gr, fil,
										   a, Sysdate.GetBuffer (),
										   &pr_ek, &pr_vk);
		if (sa && (pr_ek != 0.0 ||pr_vk != 0.0))
		{
			A_pr_list.a_pr_list.pr_ek_sa = pr_ek;
			A_pr_list.a_pr_list.pr_vk_sa = pr_vk;
			strcpy (A_pr_list.a_pr_list.lad_akv_sa, "1");
			DllPreise.GetLadAktVon (&A_pr_list.a_pr_list.lad_akt_von);
			DllPreise.GetLadAktBis (&A_pr_list.a_pr_list.lad_akt_bis);
		}
		else
		{
			sa = 0;
		}
	}
	return sa;
}
****************/

void CCompanyPage::ReadCfg () 
{
    char cfg_v [256];
    char cfg_e [256];
    char cfg_b [256];
    char cfg_n [256];
    char cfg_p [256];



	//Defaults 
	m_List.EkPruef = 0;
	m_List.HkPruef = 0;

	strcpy(m_List.Kalkulationsaufruf,"");
	m_List.PrVkVisible = 0;
	strcpy (m_List.KalkulationsModus, "a_kalkpreis");

	m_List.ALen = 200;
	m_List.AEdit = 1;
	m_List.ALen2 = 0;

	m_List.EkLen = 100;
	m_List.EkEdit = 1;
	m_List.EkNachkomma = 2;
	strcpy (m_List.EkBez, "EK");

	m_List.HkKostLen = 0;
	m_List.HkKostEdit = 0;
	m_List.HkKostNachkomma = 2;
	strcpy (m_List.HkKostBez, "Bearb.kosten");

	m_List.HkLen = 0;
	m_List.HkEdit = 0;
	m_List.HkNachkomma = 2;
	strcpy (m_List.HkBez, "HK");

	m_List.SkAufsLen = 0;
	m_List.SkAufsEdit = 0;
	m_List.SkAufsNachkomma = 2;
	strcpy (m_List.SkAufsBez, "SK %");

	m_List.SkLen = 0;
	m_List.SkEdit = 0;
	m_List.SkNachkomma = 2;
	strcpy (m_List.SkBez, "SK");

	m_List.FilEkAufsLen = 0;
	m_List.FilEkAufsEdit = 0;
	m_List.FilEkAufsNachkomma = 2;
	strcpy (m_List.FilEkAufsBez, "Fil-Ek %");

	m_List.FilEkKalkLen = 0;
	m_List.FilEkKalkEdit = 0;
	m_List.FilEkKalkNachkomma = 2;
	strcpy (m_List.FilEkKalkBez, "kalk.Fil-Ek %");

	m_List.FilEkLen = 0;
	m_List.FilEkEdit = 0;
	m_List.FilEkNachkomma = 2;
	strcpy (m_List.FilEkBez, "Fil-Ek %");

	m_List.FilVkAufsLen = 0;
	m_List.FilVkAufsEdit = 0;
	m_List.FilVkAufsNachkomma = 2;
	strcpy (m_List.FilVkAufsBez, "Fil-Vk %");

	m_List.FilVkKalkLen = 0;
	m_List.FilVkKalkEdit = 0;
	m_List.FilVkKalkNachkomma = 2;
	strcpy (m_List.FilVkKalkBez, "kalk.Fil-Vk %");

	m_List.FilVkLen = 0;
	m_List.FilVkNachkomma = 2;
	m_List.FilVkEdit = 0;
	strcpy (m_List.FilVkBez, "Fil-Vk %");

	m_List.Gh1AufsLen = 0;
	m_List.Gh1AufsNachkomma = 2;
	m_List.Gh1AufsEdit = 0;
	strcpy (m_List.Gh1AufsBez, "GH1 %");

	m_List.Gh1KalkVkLen = 0;
	m_List.Gh1KalkVkNachkomma = 2;
	m_List.Gh1KalkVkEdit = 0;
	strcpy (m_List.Gh1KalkVkBez, "GH1.kalk.Vk");

	m_List.Gh1Len = 0;
	m_List.Gh1Nachkomma = 2;
	m_List.Gh1Edit = 0;
	strcpy (m_List.Gh1Bez, "GH1");

	m_List.Gh1SpLen = 0;
	m_List.Gh1SpNachkomma = 2;
	m_List.Gh1SpEdit = 0;
	strcpy (m_List.Gh1SpBez, "Spanne.GH1");

	m_List.Gh2AufsLen = 0;
	m_List.Gh2AufsNachkomma = 2;
	m_List.Gh2AufsEdit = 0;
	strcpy (m_List.Gh2AufsBez, "GH2 %");

	m_List.Gh2KalkVkLen = 0;
	m_List.Gh2KalkVkNachkomma = 2;
	m_List.Gh2KalkVkEdit = 0;
	strcpy (m_List.Gh2KalkVkBez, "GH2.kalk.Vk");

	m_List.Gh2Len = 0;
	m_List.Gh2Nachkomma = 2;
	m_List.Gh2Edit = 0;
	strcpy (m_List.Gh2Bez, "GH2");

	m_List.Gh2SpLen = 0;
	m_List.Gh2SpNachkomma = 2;
	m_List.Gh2SpEdit = 0;
	strcpy (m_List.Gh2SpBez, "Spanne.GH2");



    if (Cfg.GetCfgValue ("KalkulationsModus", cfg_v) == TRUE) 
	{
		strcpy (m_List.KalkulationsModus,cfg_v); 
	}
    if (Cfg.GetCfgValue ("Kalkulationsaufruf", cfg_v) == TRUE)
    {
			strcpy(m_List.Kalkulationsaufruf,cfg_v);
    }
    if (Cfg.GetCfgValue ("mitPrintButton", cfg_v) == TRUE)
    {
			mitPrintButton = atoi (cfg_v);
    }

    if (Cfg.GetCfgValue ("PrVkVisible", cfg_v) == TRUE)
    {
			m_List.PrVkVisible = atoi (cfg_v);
    }

    if (Cfg.GetCfgValue ("EkFilGr", cfg_v) == TRUE)
    {
			EkFilGr = atoi (cfg_v);
    }
	if (strlen(m_List.Kalkulationsaufruf) == 0)
	{
		strcpy (m_List.Kalkulationsaufruf, "rswrun 26630 batch %s 0 0 N 0 ek");
	}

    if (Cfg.GetCfgValue ("Artikel", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE) 
	{
		m_List.ALen = atoi (cfg_v); 
		m_List.AEdit = atoi (cfg_e); 
	}
    if (Cfg.GetCfgValue ("_Ek", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.EkLen = atoi (cfg_v); 
		m_List.EkEdit = atoi (cfg_e); 
		m_List.EkNachkomma = atoi (cfg_n); 
		strcpy (m_List.EkBez, cfg_b);
		m_List.EkPruef = atoi (cfg_p); 

	}

    if (Cfg.GetCfgValue ("_HkKost", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.HkKostLen = atoi (cfg_v);
		m_List.HkKostEdit = atoi (cfg_e);
		m_List.HkKostNachkomma = atoi (cfg_n);
		strcpy (m_List.HkKostBez, cfg_b);
		m_List.HkPruef = atoi (cfg_p); 
	}

    if (Cfg.GetCfgValue ("_Hk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.HkLen = atoi (cfg_v);
		m_List.HkEdit = atoi (cfg_e);
		m_List.HkNachkomma = atoi (cfg_n);
		strcpy (m_List.HkBez, cfg_b);
		m_List.HkPruef = atoi (cfg_p); 
	}

    if (Cfg.GetCfgValue ("_SkAufs", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.SkAufsLen = atoi (cfg_v);
		m_List.SkAufsEdit = atoi (cfg_e);
		m_List.SkAufsNachkomma = atoi (cfg_n);
		strcpy (m_List.SkAufsBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Sk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.SkLen = atoi (cfg_v);
		m_List.SkEdit = atoi (cfg_e);
		m_List.SkNachkomma = atoi (cfg_n);
		strcpy (m_List.SkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilEkAufs", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilEkAufsLen = atoi (cfg_v);
		m_List.FilEkAufsEdit = atoi (cfg_e);
		m_List.FilEkAufsNachkomma = atoi (cfg_n);
		strcpy (m_List.FilEkAufsBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilEkKalk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilEkKalkLen = atoi (cfg_v);
		m_List.FilEkKalkEdit = atoi (cfg_e);
		m_List.FilEkKalkNachkomma = atoi (cfg_n);
		strcpy (m_List.FilEkKalkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilEk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilEkLen = atoi (cfg_v);
		m_List.FilEkEdit = atoi (cfg_e);
		m_List.FilEkNachkomma = atoi (cfg_n);
		strcpy (m_List.FilEkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilVkAufs", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilVkAufsLen = atoi (cfg_v);
		m_List.FilVkAufsEdit = atoi (cfg_e);
		m_List.FilVkAufsNachkomma = atoi (cfg_n);
		strcpy (m_List.FilVkAufsBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilVkKalk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilVkKalkLen = atoi (cfg_v);
		m_List.FilVkKalkEdit = atoi (cfg_e);
		m_List.FilVkKalkNachkomma = atoi (cfg_n);
		strcpy (m_List.FilVkKalkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_FilVk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.FilVkLen = atoi (cfg_v);
		m_List.FilVkEdit = atoi (cfg_e);
		m_List.FilVkNachkomma = atoi (cfg_n);
		 strcpy (m_List.FilVkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh1Aufs", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh1AufsLen = atoi (cfg_v);
		m_List.Gh1AufsEdit = atoi (cfg_e);
		m_List.Gh1AufsNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh1AufsBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh1KalkVk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh1KalkVkLen = atoi (cfg_v);
		m_List.Gh1KalkVkEdit = atoi (cfg_e);
		m_List.Gh1KalkVkNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh1KalkVkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh1", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh1Len = atoi (cfg_v);
		m_List.Gh1Edit = atoi (cfg_e);
		m_List.Gh1Nachkomma = atoi (cfg_n);
		strcpy (m_List.Gh1Bez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh1Sp", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh1SpEdit = atoi (cfg_e);
		m_List.Gh1SpLen = atoi (cfg_v);
		m_List.Gh1SpNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh1SpBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh2Aufs", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh2AufsLen = atoi (cfg_v);
		m_List.Gh2AufsEdit = atoi (cfg_e);
		m_List.Gh2AufsNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh2AufsBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh2KalkVk", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		 m_List.Gh2KalkVkLen = atoi (cfg_v);
		 m_List.Gh2KalkVkEdit = atoi (cfg_e);
		 m_List.Gh2KalkVkNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh2KalkVkBez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh2", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh2Len = atoi (cfg_v);
		m_List.Gh2Edit = atoi (cfg_e);
		m_List.Gh2Nachkomma = atoi (cfg_n);
		strcpy (m_List.Gh2Bez, cfg_b);
	}

    if (Cfg.GetCfgValue ("_Gh2Sp", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE)
	{
		m_List.Gh2SpLen = atoi (cfg_v);
		m_List.Gh2SpEdit = atoi (cfg_e);
		m_List.Gh2SpNachkomma = atoi (cfg_n);
		strcpy (m_List.Gh2SpBez, cfg_b);
	}
    if (Cfg.GetCfgValue ("_A2", cfg_v,cfg_e,cfg_b,cfg_n,cfg_p) == TRUE) 
	{
		m_List.ALen2 = atoi (cfg_v); 
	}






}

void CCompanyPage::OnEnKillfocusATyp()
{
	Form.Get ();
	RangeOk = dbRange.PruefeRange (ATyp.GetBuffer ());
	if (RangeOk == FALSE)
	{
		CString Error;
		Error.Format (_T("Bitte die Eingabe �berpr�fen"),MdnUser.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
	    m_ATyp.SetFocus ();	}
}

void CCompanyPage::OnEnKillfocusArtikel()
{
	Form.Get ();
	RangeOk = dbRange.PruefeRange (Artikel.GetBuffer ());
	if (RangeOk == FALSE)
	{
		CString Error;
		Error.Format (_T("Bitte die Eingabe �berpr�fen"),MdnUser.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
	    m_Artikel.SetFocus ();	}
}

void CCompanyPage::OnEnKillfocusTeilSmt()
{
	Form.Get ();
	RangeOk = dbRange.PruefeRange (TeilSmt.GetBuffer ());
	if (RangeOk == FALSE)
	{
		CString Error;
		Error.Format (_T("Bitte die Eingabe �berpr�fen"),MdnUser.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
	    m_TeilSmt.SetFocus ();	}
}

void CCompanyPage::OnEnKillfocusAg()
{
	Form.Get ();
	RangeOk = dbRange.PruefeRange (Ag.GetBuffer ());
	if (RangeOk == FALSE)
	{
		CString Error;
		Error.Format (_T("Bitte die Eingabe �berpr�fen"),MdnUser.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
	    m_Ag.SetFocus ();	}
}

void CCompanyPage::OnEnKillfocusWg()
{
	Form.Get ();
	RangeOk = dbRange.PruefeRange (Wg.GetBuffer ());
	if (RangeOk == FALSE)
	{
		CString Error;
		Error.Format (_T("Bitte die Eingabe �berpr�fen"),MdnUser.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
	    m_Wg.SetFocus ();	}
}

void CCompanyPage::OnEnKillfocusHwg()
{
	Form.Get ();
	RangeOk = dbRange.PruefeRange (Hwg.GetBuffer ());
	if (RangeOk == FALSE)
	{
		CString Error;
		Error.Format (_T("Bitte die Eingabe �berpr�fen"),MdnUser.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
	    m_Hwg.SetFocus ();	}
}
void CCompanyPage::OnEnKillfocusJahr()
{
	Form.Get ();
	RangeOk = dbRange.PruefeRange (Jahr.GetBuffer ());
	if (RangeOk == FALSE)
	{
		CString Error;
		Error.Format (_T("Bitte die Eingabe �berpr�fen"),MdnUser.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
	    m_Jahr.SetFocus ();	}
}

void CCompanyPage::OnEnKillfocusMonat()
{
	Form.Get ();
	RangeOk = dbRange.PruefeRange (Monat.GetBuffer ());
	if (RangeOk == FALSE)
	{
		CString Error;
		Error.Format (_T("Bitte die Eingabe �berpr�fen"),MdnUser.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
	    m_Monat.SetFocus ();	}
}

void CCompanyPage::OnEnSetfocusJahr()
{
		m_Jahr.SetSel (0, -1);

}

void CCompanyPage::OnEnSetfocusMonat()
{
		m_Monat.SetSel (0, -1);
}

void CCompanyPage::OnEnSetfocusHwg()
{
		m_Hwg.SetSel (0, -1);
}

void CCompanyPage::OnEnSetfocusWg()
{
		m_Wg.SetSel (0, -1);
}

void CCompanyPage::OnEnSetfocusAg()
{
		m_Ag.SetSel (0, -1);
}

void CCompanyPage::OnEnSetfocusArtikel()
{
		m_Artikel.SetSel (0, -1);
}

void CCompanyPage::OnEnSetfocusTeilSmt()
{
		m_TeilSmt.SetSel (0, -1);
}

void CCompanyPage::OnEnSetfocusATyp()
{
		m_ATyp.SetSel (0, -1);
}


void CCompanyPage::OnBnClickedEkEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedHkEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedHkKostEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedSkAufsEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedSkEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedAlleArtikel()
{
	Form.Get();
	if (strcmp(Lst.lst.AlleArtikel,"J") == 0)
	{
		SmtGrid.SetVisible (TRUE);
		m_Jahr.SetFocus ();

	}
	else
	{
		SmtGrid.SetVisible (FALSE);
	}
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnEnKillfocusMdnUser()
{
	/********
		if (!ReadMdn ())
		{
			m_MdnUser.SetFocus ();
		}
		*******/

}

void CCompanyPage::OnBnClickedFilEkAufsEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedFilEkKalkEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedFilEkEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedGh1AufsEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedGh1KalkVkEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedGh1Edit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedGh1SpEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedGh2AufsEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}


void CCompanyPage::OnBnClickedGh2KalkVkEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedGh2Edit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedGh2SpEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedFilVkAufsEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedFilVkKalkEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}

void CCompanyPage::OnBnClickedFilVkEdit()
{
	if (flgListeAktiv == TRUE)
	{
		OnVK_F6();
	}
}
