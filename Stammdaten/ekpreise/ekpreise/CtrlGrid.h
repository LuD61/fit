#pragma once
#include "vector.h"
#include "CtrlInfo.h"

#define DOCKSIZE -1
#define DOCKLEFT -2
#define DOCKRIGHT -3
#define DOCKTOP -4
#define DOCKBOTTOM -5

class CCtrlGrid :
	public CVector
{
public:
	CWnd *Dlg;
	CRect *DlgSize;
	int rows;
	int columns;
	int RowSpace;
	int ColSpace;
	int xBorder;
	int yBorder;
	int CellHeight;
	int pcx;
    int pcy;
	CFont *Font;

	CVector ColStart;
	CCtrlGrid(void);
	~CCtrlGrid(void);
	void Create (CWnd *, int, int);
//	void Add (CCtrlInfo *);
	void Display ();
	int Calculate (int);
	void SetGridX (int, int, TEXTMETRIC*, int);
	void Move (int, int);
	void SetBorder (int, int);
	void SetGridSpace (int, int);
	void SetCellHeight (int);
	void SetFontCellHeight (CWnd *, CFont *);
	void SetFontCellHeight (CWnd *);
	void SetFontCellHeight (CWnd *, int);
	void SetParentMetrics (int , int);
    void TestRightDockControl ();
	void CreateChoiceButton (CButton&, int, CWnd*);
	void SetFont (CFont *);
	void GetGridRect (CRect *rect);
	void MoveGrid (CRect *rect);
	void MoveGridControls (int, int);
	void SetVisible (BOOL visible);

    void SetItemCharHeight (CWnd *Item, int Height);
    void SetItemLogHeight (CWnd *Item, int Height);
    void SetItemPixelHeight (CWnd *Item, int Height);

    void SetItemCharWidth (CWnd *Item, int Width);
    void SetItemLogWidth (CWnd *Item, int Width);
    void SetItemPixelWidth (CWnd *Item, int Width);

    static void DockControlWidth (CWnd *, CWnd *, CWnd *);
    static void DockControlHeight (CWnd *, CWnd *, CWnd *);
};
