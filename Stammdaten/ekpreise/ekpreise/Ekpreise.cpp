#include "StdAfx.h"
#include "ekpreise.h"

CEkPreise::CEkPreise(void)
{
	cEk = _T("0.0");
	cVk = _T("0.0");
	hk_kost = 0.0;
	sk_aufs = 0.0;
	memcpy (&ek_pr, &ek_pr_null, sizeof (EK_PR));
}

//CEkPreise::CEkPreise(CString& cEk, CString& cVk, EK_PR& ek_pr)
CEkPreise::CEkPreise(CString& cEk, EK_PR& ek_pr)
{
	this->cEk = cEk.Trim ();
	this->cVk = cVk.Trim ();
	hk_kost = ek_pr.hk_kost;
	sk_aufs = ek_pr.sk_aufs;
	memcpy (&this->ek_pr, &ek_pr, sizeof (EK_PR));
}

CEkPreise::~CEkPreise(void)
{
}
