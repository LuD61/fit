#include "StdAfx.h"
#include ".\dbquery.h"
#include "Token.h"

CDbQuery::CDbQuery(void)
{
}

CDbQuery::~CDbQuery(void)
{
}

CString& CDbQuery::ForColumn (CString& Value, CString& ColumnName, BOOL Quote)
{
	LPTSTR p;
	Query = _T("");
	CToken t;
	t.SetSep (_T(";"));
	t = Value;
	int i = 0;
	if (t.GetAnzToken () > 1)
	{
		Query += _T("(");
	}
	while ((p = t.NextToken ()) != NULL)
	{
		CString V = p;
		if (i > 0)
		{
			Query += _T(" or ");
		}
		if (Quote)
		{
            ForStringColumn (V, ColumnName);			
		}
		else
		{
            ForNumColumn (V, ColumnName);			
		}
		Query += Q;
		i ++;
	}
	if (t.GetAnzToken () > 1)
	{
		Query += _T(")");
	}
    return Query;
}

CString& CDbQuery::ForStringColumn (CString& Value, CString& ColumnName)
{
	Q = ColumnName;
	Value.Trim ();
	if (Value.GetBuffer ()[0] == _T('='))
	{
		Q += _T(" = ");
		Q += _T("\"");
		Q += Value.Mid (1);
		Q += _T("\"");
		return Q;
	}

	if (Value.GetBuffer ()[0] == _T('<'))
	{
		Q += _T(" < ");
		Q += _T("\"");
		Q += Value.Mid (1);
		Q += _T("\"");
		return Q;
	}
   
	if (Value.GetBuffer ()[0] == _T('>'))
	{
		Q += _T(" < ");
		Q += _T("\"");
		Q += Value.Mid (1);
		Q += _T("\"");
		return Q;
	}
	if ((Value.Find (_T('*'), 0) >= 0) ||
		(Value.Find (_T('?'), 0) >= 0))
	{
		Q += _T(" matches ");
		Q += _T("\"");
		Q += Value;
		Q += _T("\"");
	}
	else
	{
		Q += _T(" matches ");
		Q += _T("\"");
		Q += Value;
		Q += _T("*");
		Q += _T("\"");
	}
	return Q;
}

CString& CDbQuery::ForNumColumn (CString& Value, CString& ColumnName)
{
	Q = ColumnName;
	Value.Trim ();
	if (Value.GetBuffer ()[0] == _T('='))
	{
		Q += _T(" = ");
		Q += Value.Mid (1);
		return Q;
	}

	if (Value.GetBuffer ()[0] == _T('<'))
	{
		Q += _T(" < ");
		Q += Value.Mid (1);
		return Q;
	}
   
	if (Value.GetBuffer ()[0] == _T('>'))
	{
		Q += _T(" > ");
		Q += Value.Mid (1);
		return Q;
	}
	CToken t;
	t.SetSep (_T(":"));
	t = Value;
	if (t.GetAnzToken () > 1)
	{
		Q += _T(" between ");
		Q += t.GetToken (0);
		Q += _T(" and ");
		Q += t.GetToken (1);
		return Q;
	}

	t.SetSep (_T("-"));
	t = Value;
	if (t.GetAnzToken () > 1)
	{
		Q += _T(" between ");
		Q += t.GetToken (0);
		Q += _T(" and ");
		Q += t.GetToken (1);
		return Q;
	}

	Q += _T(" = ");
	Q += Value;
	return Q;
}
