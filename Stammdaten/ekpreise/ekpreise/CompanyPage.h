#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "DbPropertyPage.h"
#include "PrComboBox.h"
#include "CtrlGrid.h"
#include "CmpPrListCtrl.h"
#include "mo_progcfg.h"
#include "adr.h"
#include "Mdn.h"
#include "A_bas.h"
#include "A_pr.h"
#include "Ek_pr.h"
#include "A_pr_list.h"
#include "FormTab.h"
#include "ChoiceA.h"
#include "ChoiceAg.h"
#include "ChoiceWg.h"
#include "ChoiceHwg.h"
#include "NumEdit.h"
#include "StaticButton.h"
#include "AprUpdateEvent.h"
#include "ChoiceMdn.h"
#include "Sys_ben.h"
#include "Mdn.h"
#include "Fil.h"
#include "lst.h"
#include "ek_prpr.h"
#include "iv_pr.h"
#include "ListPage.h"


#include "DllPreise.h"

#define IDC_HWGCHOICE 11001
#define IDC_WGCHOICE  11002
#define IDC_AGCHOICE  11003
#define IDC_TEILSMTCHOICE  11004
#define IDC_ATYPCHOICE  11005

#define IDC_PROPEN 11006
#define IDC_MDNUSERCHOICE  11007
#define IDC_PRPRINT 11008
#define IDC_PRACTIVATE 11009
#define IDC_ARTIKELCHOICE  11010

// CCompanyPage-Dialogfeld

class CCompanyPage : public CDbPropertyPage, public CAprUpdateEvent
{
	DECLARE_DYNAMIC(CCompanyPage)

public:
	CCompanyPage();
	virtual ~CCompanyPage();

// Dialogfelddaten
	enum { IDD = IDD_COMPANY_PR_PAGE };

	enum LIST_TYP
	{
		Preise = 0,
		Aktionen = 1,
		PreiseAktionen = 2,
	};

	short ListType;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
	virtual void OnSize (UINT, int, int);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
	CWnd *Frame;
	PROG_CFG Cfg;

	A_BAS_CLASS A_bas;
	APR_CLASS A_pr;
	EK_PR_CLASS Ek_pr;
	EK_PRPR_CLASS Ek_prpr;
	IV_PR_CLASS Iv_pr;

	A_PR_LIST_CLASS A_pr_list;
	ADR_CLASS MdnAdrUser;
	MDN_CLASS MdnUser;
	LST_CLASS Lst;
	SYS_BEN_CLASS Sys_ben;
	MDN_CLASS cMdn;
	FIL_CLASS cFil;
	CDllPreise DllPreise;

	COLORREF DlgBkColor;
	HBRUSH DlgBrush;

	short MdnGr;
	short Mdn;
	short FilGr;
	short Fil;

	CString PersName;
	CString ProgNam;
	CString Hwg;
	CString Wg;
	CString Ag;
	CString Jahr;
	CString Monat;

	CString TeilSmt;
	CString ATyp;
	CString Artikel;

	int MdnGrCursor;
	int MdnCursor;
	int FilGrCursor;
	int FilCursor;
	int HwgCursor;
	int WgCursor;
	int AgCursor;
	int PtabCursor;


	int EkLesCursor ;

	short gr;
	short EkFilGr;
	short mitPrintButton;
	TCHAR gr_bz1 [25];

	short mdn;
	short fil;
	TCHAR adr_krz [17];
	short PrDiff;
	BOOL RangeOk;
	BOOL flgListeAktiv;

	CStatic m_LMdnGr;
	CPrComboBox m_MdnGr;
	CStatic m_LMdn;
	CPrComboBox m_Mdn;
	CStatic m_LFilGr;
	CPrComboBox m_FilGr;
	CStatic m_LFil;
	CPrComboBox m_Fil;
	CStatic m_LListTyp;
	CPrComboBox m_ListTyp;

	CStatic m_LJahr;
	CEdit m_Jahr;
	CStatic m_LMonat;
	CEdit m_Monat;

	CStatic m_LHwg;
	CEdit m_Hwg;
	CButton m_HwgChoice;
	CStatic m_LWg;
	CEdit m_Wg;
	CButton m_WgChoice;
	CStatic m_LAg;
	CEdit m_Ag;
	CButton m_AgChoice;

	CStatic m_LTeilSmt;
	CEdit m_TeilSmt;
	CButton m_TeilSmtChoice;
	CStatic m_LATyp;
	CEdit m_ATyp;
	CButton m_ATypChoice;
	CButton m_ArtikelChoice;

	CButton m_EkEdit;
	CStaticButton m_Open;
	CStaticButton m_Print;
	CStaticButton m_Activate;

	CStatic m_LMdnUser;
	CNumEdit m_MdnUser;
	CEdit m_MdnAdrKrz;
	CButton m_MdnUserChoice;

	CCtrlGrid UserGrid;
	CCtrlGrid MdnUserGrid;

	CCtrlGrid HwgGrid;
	CCtrlGrid WgGrid;
	CCtrlGrid AgGrid;
	CCtrlGrid TeilSmtGrid;
	CCtrlGrid ATypGrid;
	CCtrlGrid ArtikelGrid;

	CCtrlGrid CtrlGrid;
	CCtrlGrid CompanyGrid;
	CCtrlGrid GrGrid;
	CCtrlGrid SmtGrid;
	CStatic m_CmpGroup;
	CStatic m_GrGroup;
	CStatic m_SmtGroup;
	CFont Font;
	CFont lFont;
    CFormTab Form;

	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceHWG *ChoiceHWG;
	BOOL ModalChoiceHWG;
	CChoiceWG *ChoiceWG;
	BOOL ModalChoiceWG;
	CChoiceAG *ChoiceAG;
	BOOL ModalChoiceAG;
	CChoiceA *ChoiceA;
	BOOL ModalChoiceA;

	void FillMdnGrCombo ();
	void FillMdnCombo ();
	void FillFilGrCombo ();
	void FillFilCombo ();
	void FillListTypCombo ();

	void FillHwgCombo ();
	void FillWgCombo ();
	void FillAgCombo ();

	void FillTeilSmtCombo ();
	void FillATypCombo ();
	afx_msg void OnCbnSelchangeMdn();
	afx_msg void OnCbnSelchangeFil();
	afx_msg void OnHAgChoice ();
    void OnMdnchoice(); 
	void OnHwgChoice ();
	void OnWgChoice ();
	void OnAgChoice ();
	void OnArtikelChoice ();
	void OnTeilSmtChoice ();
	void OnATypChoice ();
	CFillList FillList;
	CCmpPrListCtrl m_List;
	void OnVK_F6 (void);
	afx_msg void OnOpen ();
	afx_msg void OnPrint ();
	afx_msg void OnActivate ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	BOOL ReadMdn ();
	afx_msg void OnEnChangeMdnUser();
	BOOL Print ();
	BOOL Print (CString& Select, CString& CmpSelect);
	BOOL Print (CString& Select, short mdn_gr, short mdn, short fil_gr, short fil);
	short GetAktPrice (double a, short mdn_gr, short mdn, short fil_gr, short fil);
	void ReadCfg ();
	int iv_prschreiben ( char * aktdatum , int ityp ) ;
    int preiseschreiben ( char * typ , char * edatstring,int tag, int monat, int jahr ) ;


public:
	afx_msg void OnEnKillfocusATyp();
public:
	CEdit m_Artikel;
public:
	afx_msg void OnEnKillfocusArtikel();
public:
	afx_msg void OnEnKillfocusTeilSmt();
public:
	afx_msg void OnEnKillfocusAg();
public:
	afx_msg void OnEnKillfocusWg();
public:
	afx_msg void OnEnKillfocusHwg();
public:
	CStatic m_LArtikel;
public:
	afx_msg void OnEnSetfocusJahr();
public:
	afx_msg void OnEnSetfocusMonat();
public:
	afx_msg void OnEnSetfocusHwg();
public:
	afx_msg void OnEnSetfocusWg();
public:
	afx_msg void OnEnSetfocusAg();
public:
	afx_msg void OnEnSetfocusArtikel();
public:
	afx_msg void OnEnSetfocusTeilSmt();
public:
	afx_msg void OnEnSetfocusATyp();
public:
	afx_msg void OnEnKillfocusJahr();
public:
	afx_msg void OnEnKillfocusMonat();
public:
	afx_msg void OnBnClickedEkEdit();
public:
	CButton m_HkEdit;
public:
	CButton m_HkKostEdit;
public:
	CButton m_SkAufsEdit;
public:
	CButton m_SkEdit;
public:
	afx_msg void OnBnClickedHkEdit();
public:
	afx_msg void OnBnClickedHkKostEdit();
public:
	afx_msg void OnBnClickedSkAufsEdit();
public:
	afx_msg void OnBnClickedSkEdit();
public:
	CButton m_AlleArtikel;
public:
	afx_msg void OnBnClickedAlleArtikel();
public:
	afx_msg void OnEnKillfocusMdnUser();
public:
	afx_msg void OnBnClickedFilEkAufsEdit();
public:
	CButton m_FilEkAufsEdit;
public:
	CButton m_FilEkKalkEdit;
public:
	afx_msg void OnBnClickedFilEkKalkEdit();
public:
	CButton m_FilEkEdit;
public:
	afx_msg void OnBnClickedFilEkEdit();
public:
	CButton m_Gh1AufsEdit;
public:
	afx_msg void OnBnClickedGh1AufsEdit();
public:
	CButton m_Gh1KalkVkEdit;
public:
	afx_msg void OnBnClickedGh1KalkVkEdit();
public:
	CButton m_Gh1Edit;
public:
	afx_msg void OnBnClickedGh1Edit();
public:
	CButton m_Gh1SpEdit;
public:
	afx_msg void OnBnClickedGh1SpEdit();
public:
	CButton m_Gh2AufsEdit;
public:
	afx_msg void OnBnClickedGh2AufsEdit();
public:
	CButton m_Gh2KalkVkEdit;
public:
	afx_msg void OnBnClickedGh2KalkVkEdit();
public:
	CButton m_Gh2Edit;
public:
	afx_msg void OnBnClickedGh2Edit();
public:
	CButton m_Gh2SpEdit;
public:
	afx_msg void OnBnClickedGh2SpEdit();
public:
	CButton m_FilVkAufsEdit;
public:
	CButton m_FilVkKalkEdit;
public:
	CButton m_FilVkEdit;
public:
	afx_msg void OnBnClickedFilVkAufsEdit();
public:
	afx_msg void OnBnClickedFilVkKalkEdit();
public:
	afx_msg void OnBnClickedFilVkEdit();
};
