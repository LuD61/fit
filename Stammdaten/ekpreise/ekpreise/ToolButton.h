#ifndef _TOOLBUTTENDEF
#define _TOOLBUTTENDEF

#include "Vector.h"
#include "ToolbarItem.h"

#define IDC_F5                          1026
#define IDC_F12                         1027
#define IDC_F7                          1028
#define IDC_PRINT2                      1029
#define IDC_SEPERATOR                   1031
#define IDC_LINE                        1034

class CToolButton
{
  protected :
      HBITMAP F5;
      HBITMAP F12;
      HBITMAP F7;
      HBITMAP BPrint;
      CVector StdToolbar;
      CVector *Toolbar;
      int startx;
      int tbx;
      int tby;

      int bitmapcx;
      int bitmapcy;

      int horzyup;
      int horzydown;
      int horzcx;
      int horzcy;

      int vertsepcx;
      int vertsepcy;

      int spacecx;
      int wspacecx;

  public :
      CToolButton ();
      ~CToolButton ();
      void SetSpace (int);
      void SetWSpace (int);
      void Create (CWnd *);
      void Create (CVector *);
      void Add (CToolbarItem *);
      BOOL DrawToolbar (int, HDC);
	  BOOL OnToolTipNotify(int,NMHDR *, LRESULT *);
      static CFont *SetFont (CDC *);
      int GetCX (CToolbarItem *);
};

#endif