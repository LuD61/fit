#pragma once
#include "formfield.h"
#include "CodeProperties.h"

class CUniFormField :
	public CFormField
{
public:
	static CCodeProperties *Code;
	static CCodeProperties NullCode;
	CUniFormField(void);
	CUniFormField(CWnd *, int, void *, int);
	CUniFormField(CWnd *, int, void *, int, int, int);
	~CUniFormField(void);

    virtual void Show ();
    virtual void Get ();
};
