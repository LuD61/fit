#ifndef _A_PREIS_DEF
#define _A_PREIS_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_PREIS {
   long           mdn;
   short          fil;
   double         a;
   short          prodabt;
   short          druckfolge;
   double         ek;
   double         plan_ek;
   double         kostenkg;
   double         kosteneinh;
   short          flg_bearb_weg;
   short          bearb_hk;
   short          bearb_p1;
   short          bearb_p2;
   short          bearb_p3;
   short          bearb_p4;
   short          bearb_p5;
   short          bearb_p6;
   short          bearb_vk;
   double         sp_sk;
   double         sp_fil;
   double         sp_vk;
   DATE_STRUCT    dat;
   TCHAR          zeit[6];
   TCHAR          pers_nam[9];
   long           inh_ek;
   double         bep;
   double         plan_bep;
   double         test_ek;
};
extern struct A_PREIS a_preis, a_preis_null;

#line 8 "a_preis.rh"

class A_PREIS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_PREIS a_preis;
               A_PREIS_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (A_PREIS&);  
};
#endif
