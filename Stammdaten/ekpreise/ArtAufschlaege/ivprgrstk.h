#ifndef _A_IVPRGRSTK_DEF
#define _A_IVPRGRSTK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct IVPRGRSTK {
   short          mdn;
   long           pr_gr_stuf;
   TCHAR          zus_bz[25];
   DATE_STRUCT    gue_ab;
   TCHAR          ueb_kz[2];
   short          delstatus;
   short          waehrung;
};
extern struct IVPRGRSTK ivprgrstk, ivprgrstk_null;

#line 8 "ivprgrstk.rh"

class IVPRGRSTK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               IVPRGRSTK ivprgrstk;
               IVPRGRSTK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
