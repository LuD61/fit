// AprManDoc.cpp : Implementierung der Klasse CAprManDoc
//

#include "stdafx.h"
#include "AprMan.h"

#include "AprManDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAprManDoc

IMPLEMENT_DYNCREATE(CAprManDoc, CDocument)

BEGIN_MESSAGE_MAP(CAprManDoc, CDocument)
END_MESSAGE_MAP()


// CAprManDoc Erstellung/Zerst�rung

CAprManDoc::CAprManDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CAprManDoc::~CAprManDoc()
{
}

BOOL CAprManDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CAprManDoc Serialisierung

void CAprManDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CAprManDoc Diagnose

#ifdef _DEBUG
void CAprManDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CAprManDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CAprManDoc-Befehle
