#ifndef _WG_DEF
#define _WG_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct WG {
   short          abt;
   short          bearb_fil;
   short          bearb_lad;
   short          bearb_sk;
   TCHAR          best_auto[2];
   TCHAR          bsd_kz[2];
   TCHAR          dec_kz[2];
   short          delstatus;
   TCHAR          durch_fil[2];
   TCHAR          durch_sk[2];
   TCHAR          durch_sw[2];
   TCHAR          durch_vk[2];
   long           erl_kto;
   TCHAR          hnd_gew[2];
   short          hwg;
   double         inv_ab_anp;
   double         inv_ab_letzt;
   double         inv_ab_stnd;
   TCHAR          kost_kz[3];
   short          me_einh;
   short          mwst;
   double         pers_rab;
   TCHAR          pfa[2];
   TCHAR          pr_ueb[2];
   TCHAR          reg_eti[2];
   short          sais1;
   short          sais2;
   short          sg1;
   short          sg2;
   TCHAR          smt[2];
   double         sp_fil;
   TCHAR          sp_kz[2];
   double         sp_sk;
   double         sp_vk;
   TCHAR          stk_lst_kz[2];
   double         sw;
   short          teil_smt;
   TCHAR          theke_eti[2];
   TCHAR          ums_verb[2];
   TCHAR          verk_art[2];
   DATE_STRUCT    verk_beg;
   DATE_STRUCT    verk_end;
   TCHAR          waren_eti[2];
   long           we_kto;
   short          wg;
   TCHAR          wg_bz1[25];
   TCHAR          wg_bz2[25];
   long           erl_kto_1;
   long           erl_kto_2;
   long           erl_kto_3;
   long           we_kto_1;
   long           we_kto_2;
   long           we_kto_3;
};
extern struct WG wg, wg_null;

#line 8 "wg.rh"

class WG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               WG wg;
               WG_CLASS () : DB_CLASS ()
               {
               }
};
#endif
