#include "stdafx.h"
#include "ag.h"

struct AG ag, ag_null;

void AG_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &ag.ag,  SQLLONG, 0);
    sqlout ((short *) &ag.abt,SQLSHORT,0);
    sqlout ((long *) &ag.ag,SQLLONG,0);
    sqlout ((TCHAR *) ag.ag_bz1,SQLCHAR,21);
    sqlout ((TCHAR *) ag.ag_bz2,SQLCHAR,21);
    sqlout ((short *) &ag.a_typ,SQLSHORT,0);
    sqlout ((short *) &ag.bearb_fil,SQLSHORT,0);
    sqlout ((short *) &ag.bearb_lad,SQLSHORT,0);
    sqlout ((short *) &ag.bearb_sk,SQLSHORT,0);
    sqlout ((TCHAR *) ag.best_auto,SQLCHAR,2);
    sqlout ((TCHAR *) ag.bsd_kz,SQLCHAR,2);
    sqlout ((short *) &ag.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) ag.durch_fil,SQLCHAR,2);
    sqlout ((TCHAR *) ag.durch_sk,SQLCHAR,2);
    sqlout ((TCHAR *) ag.durch_sw,SQLCHAR,2);
    sqlout ((TCHAR *) ag.durch_vk,SQLCHAR,2);
    sqlout ((long *) &ag.erl_kto,SQLLONG,0);
    sqlout ((short *) &ag.hbk,SQLSHORT,0);
    sqlout ((TCHAR *) ag.hbk_kz,SQLCHAR,2);
    sqlout ((short *) &ag.hbk_ztr,SQLSHORT,0);
    sqlout ((TCHAR *) ag.hnd_gew,SQLCHAR,2);
    sqlout ((TCHAR *) ag.kost_kz,SQLCHAR,3);
    sqlout ((short *) &ag.me_einh,SQLSHORT,0);
    sqlout ((short *) &ag.mwst,SQLSHORT,0);
    sqlout ((TCHAR *) ag.pfa,SQLCHAR,2);
    sqlout ((TCHAR *) ag.pr_ueb,SQLCHAR,2);
    sqlout ((TCHAR *) ag.reg_eti,SQLCHAR,2);
    sqlout ((short *) &ag.sais1,SQLSHORT,0);
    sqlout ((short *) &ag.sais2,SQLSHORT,0);
    sqlout ((short *) &ag.sg1,SQLSHORT,0);
    sqlout ((short *) &ag.sg2,SQLSHORT,0);
    sqlout ((TCHAR *) ag.smt,SQLCHAR,2);
    sqlout ((double *) &ag.sp_fil,SQLDOUBLE,0);
    sqlout ((TCHAR *) ag.sp_kz,SQLCHAR,2);
    sqlout ((double *) &ag.sp_sk,SQLDOUBLE,0);
    sqlout ((double *) &ag.sp_vk,SQLDOUBLE,0);
    sqlout ((TCHAR *) ag.stk_lst_kz,SQLCHAR,2);
    sqlout ((double *) &ag.sw,SQLDOUBLE,0);
    sqlout ((double *) &ag.tara,SQLDOUBLE,0);
    sqlout ((short *) &ag.teil_smt,SQLSHORT,0);
    sqlout ((TCHAR *) ag.theke_eti,SQLCHAR,2);
    sqlout ((TCHAR *) ag.verk_art,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &ag.verk_beg,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &ag.verk_end,SQLDATE,0);
    sqlout ((TCHAR *) ag.waren_eti,SQLCHAR,2);
    sqlout ((long *) &ag.we_kto,SQLLONG,0);
    sqlout ((short *) &ag.wg,SQLSHORT,0);
    sqlout ((double *) &ag.gn_pkt_gbr,SQLDOUBLE,0);
    sqlout ((TCHAR *) ag.a_krz_kz,SQLCHAR,2);
    sqlout ((TCHAR *) ag.tier_kz,SQLCHAR,3);
    sqlout ((long *) &ag.erl_kto_1,SQLLONG,0);
    sqlout ((long *) &ag.erl_kto_2,SQLLONG,0);
    sqlout ((long *) &ag.erl_kto_3,SQLLONG,0);
    sqlout ((long *) &ag.we_kto_1,SQLLONG,0);
    sqlout ((long *) &ag.we_kto_2,SQLLONG,0);
    sqlout ((long *) &ag.we_kto_3,SQLLONG,0);
            cursor = sqlcursor (_T("select ag.abt,  ag.ag,  ")
_T("ag.ag_bz1,  ag.ag_bz2,  ag.a_typ,  ag.bearb_fil,  ag.bearb_lad,  ")
_T("ag.bearb_sk,  ag.best_auto,  ag.bsd_kz,  ag.delstatus,  ag.durch_fil,  ")
_T("ag.durch_sk,  ag.durch_sw,  ag.durch_vk,  ag.erl_kto,  ag.hbk,  ag.hbk_kz,  ")
_T("ag.hbk_ztr,  ag.hnd_gew,  ag.kost_kz,  ag.me_einh,  ag.mwst,  ag.pfa,  ")
_T("ag.pr_ueb,  ag.reg_eti,  ag.sais1,  ag.sais2,  ag.sg1,  ag.sg2,  ag.smt,  ")
_T("ag.sp_fil,  ag.sp_kz,  ag.sp_sk,  ag.sp_vk,  ag.stk_lst_kz,  ag.sw,  ag.tara,  ")
_T("ag.teil_smt,  ag.theke_eti,  ag.verk_art,  ag.verk_beg,  ag.verk_end,  ")
_T("ag.waren_eti,  ag.we_kto,  ag.wg,  ag.gn_pkt_gbr,  ag.a_krz_kz,  ag.tier_kz,  ")
_T("ag.erl_kto_1,  ag.erl_kto_2,  ag.erl_kto_3,  ag.we_kto_1,  ag.we_kto_2,  ")
_T("ag.we_kto_3 from ag ")

#line 12 "ag.rpp"
                                  _T("where ag = ?"));
    sqlin ((short *) &ag.abt,SQLSHORT,0);
    sqlin ((long *) &ag.ag,SQLLONG,0);
    sqlin ((TCHAR *) ag.ag_bz1,SQLCHAR,21);
    sqlin ((TCHAR *) ag.ag_bz2,SQLCHAR,21);
    sqlin ((short *) &ag.a_typ,SQLSHORT,0);
    sqlin ((short *) &ag.bearb_fil,SQLSHORT,0);
    sqlin ((short *) &ag.bearb_lad,SQLSHORT,0);
    sqlin ((short *) &ag.bearb_sk,SQLSHORT,0);
    sqlin ((TCHAR *) ag.best_auto,SQLCHAR,2);
    sqlin ((TCHAR *) ag.bsd_kz,SQLCHAR,2);
    sqlin ((short *) &ag.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) ag.durch_fil,SQLCHAR,2);
    sqlin ((TCHAR *) ag.durch_sk,SQLCHAR,2);
    sqlin ((TCHAR *) ag.durch_sw,SQLCHAR,2);
    sqlin ((TCHAR *) ag.durch_vk,SQLCHAR,2);
    sqlin ((long *) &ag.erl_kto,SQLLONG,0);
    sqlin ((short *) &ag.hbk,SQLSHORT,0);
    sqlin ((TCHAR *) ag.hbk_kz,SQLCHAR,2);
    sqlin ((short *) &ag.hbk_ztr,SQLSHORT,0);
    sqlin ((TCHAR *) ag.hnd_gew,SQLCHAR,2);
    sqlin ((TCHAR *) ag.kost_kz,SQLCHAR,3);
    sqlin ((short *) &ag.me_einh,SQLSHORT,0);
    sqlin ((short *) &ag.mwst,SQLSHORT,0);
    sqlin ((TCHAR *) ag.pfa,SQLCHAR,2);
    sqlin ((TCHAR *) ag.pr_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) ag.reg_eti,SQLCHAR,2);
    sqlin ((short *) &ag.sais1,SQLSHORT,0);
    sqlin ((short *) &ag.sais2,SQLSHORT,0);
    sqlin ((short *) &ag.sg1,SQLSHORT,0);
    sqlin ((short *) &ag.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) ag.smt,SQLCHAR,2);
    sqlin ((double *) &ag.sp_fil,SQLDOUBLE,0);
    sqlin ((TCHAR *) ag.sp_kz,SQLCHAR,2);
    sqlin ((double *) &ag.sp_sk,SQLDOUBLE,0);
    sqlin ((double *) &ag.sp_vk,SQLDOUBLE,0);
    sqlin ((TCHAR *) ag.stk_lst_kz,SQLCHAR,2);
    sqlin ((double *) &ag.sw,SQLDOUBLE,0);
    sqlin ((double *) &ag.tara,SQLDOUBLE,0);
    sqlin ((short *) &ag.teil_smt,SQLSHORT,0);
    sqlin ((TCHAR *) ag.theke_eti,SQLCHAR,2);
    sqlin ((TCHAR *) ag.verk_art,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &ag.verk_beg,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &ag.verk_end,SQLDATE,0);
    sqlin ((TCHAR *) ag.waren_eti,SQLCHAR,2);
    sqlin ((long *) &ag.we_kto,SQLLONG,0);
    sqlin ((short *) &ag.wg,SQLSHORT,0);
    sqlin ((double *) &ag.gn_pkt_gbr,SQLDOUBLE,0);
    sqlin ((TCHAR *) ag.a_krz_kz,SQLCHAR,2);
    sqlin ((TCHAR *) ag.tier_kz,SQLCHAR,3);
    sqlin ((long *) &ag.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &ag.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &ag.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &ag.we_kto_1,SQLLONG,0);
    sqlin ((long *) &ag.we_kto_2,SQLLONG,0);
    sqlin ((long *) &ag.we_kto_3,SQLLONG,0);
            sqltext = _T("update ag set ag.abt = ?,  ag.ag = ?,  ")
_T("ag.ag_bz1 = ?,  ag.ag_bz2 = ?,  ag.a_typ = ?,  ag.bearb_fil = ?,  ")
_T("ag.bearb_lad = ?,  ag.bearb_sk = ?,  ag.best_auto = ?,  ag.bsd_kz = ?,  ")
_T("ag.delstatus = ?,  ag.durch_fil = ?,  ag.durch_sk = ?,  ")
_T("ag.durch_sw = ?,  ag.durch_vk = ?,  ag.erl_kto = ?,  ag.hbk = ?,  ")
_T("ag.hbk_kz = ?,  ag.hbk_ztr = ?,  ag.hnd_gew = ?,  ag.kost_kz = ?,  ")
_T("ag.me_einh = ?,  ag.mwst = ?,  ag.pfa = ?,  ag.pr_ueb = ?,  ")
_T("ag.reg_eti = ?,  ag.sais1 = ?,  ag.sais2 = ?,  ag.sg1 = ?,  ag.sg2 = ?,  ")
_T("ag.smt = ?,  ag.sp_fil = ?,  ag.sp_kz = ?,  ag.sp_sk = ?,  ag.sp_vk = ?,  ")
_T("ag.stk_lst_kz = ?,  ag.sw = ?,  ag.tara = ?,  ag.teil_smt = ?,  ")
_T("ag.theke_eti = ?,  ag.verk_art = ?,  ag.verk_beg = ?,  ")
_T("ag.verk_end = ?,  ag.waren_eti = ?,  ag.we_kto = ?,  ag.wg = ?,  ")
_T("ag.gn_pkt_gbr = ?,  ag.a_krz_kz = ?,  ag.tier_kz = ?,  ")
_T("ag.erl_kto_1 = ?,  ag.erl_kto_2 = ?,  ag.erl_kto_3 = ?,  ")
_T("ag.we_kto_1 = ?,  ag.we_kto_2 = ?,  ag.we_kto_3 = ? ")

#line 14 "ag.rpp"
                                  _T("where ag = ?");
            sqlin ((double *)   &ag.ag,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &ag.ag,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select ag from ag ")
                                  _T("where ag = ?"));
            sqlin ((double *)   &ag.ag,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from ag ")
                                  _T("where ag = ?"));
    sqlin ((short *) &ag.abt,SQLSHORT,0);
    sqlin ((long *) &ag.ag,SQLLONG,0);
    sqlin ((TCHAR *) ag.ag_bz1,SQLCHAR,21);
    sqlin ((TCHAR *) ag.ag_bz2,SQLCHAR,21);
    sqlin ((short *) &ag.a_typ,SQLSHORT,0);
    sqlin ((short *) &ag.bearb_fil,SQLSHORT,0);
    sqlin ((short *) &ag.bearb_lad,SQLSHORT,0);
    sqlin ((short *) &ag.bearb_sk,SQLSHORT,0);
    sqlin ((TCHAR *) ag.best_auto,SQLCHAR,2);
    sqlin ((TCHAR *) ag.bsd_kz,SQLCHAR,2);
    sqlin ((short *) &ag.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) ag.durch_fil,SQLCHAR,2);
    sqlin ((TCHAR *) ag.durch_sk,SQLCHAR,2);
    sqlin ((TCHAR *) ag.durch_sw,SQLCHAR,2);
    sqlin ((TCHAR *) ag.durch_vk,SQLCHAR,2);
    sqlin ((long *) &ag.erl_kto,SQLLONG,0);
    sqlin ((short *) &ag.hbk,SQLSHORT,0);
    sqlin ((TCHAR *) ag.hbk_kz,SQLCHAR,2);
    sqlin ((short *) &ag.hbk_ztr,SQLSHORT,0);
    sqlin ((TCHAR *) ag.hnd_gew,SQLCHAR,2);
    sqlin ((TCHAR *) ag.kost_kz,SQLCHAR,3);
    sqlin ((short *) &ag.me_einh,SQLSHORT,0);
    sqlin ((short *) &ag.mwst,SQLSHORT,0);
    sqlin ((TCHAR *) ag.pfa,SQLCHAR,2);
    sqlin ((TCHAR *) ag.pr_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) ag.reg_eti,SQLCHAR,2);
    sqlin ((short *) &ag.sais1,SQLSHORT,0);
    sqlin ((short *) &ag.sais2,SQLSHORT,0);
    sqlin ((short *) &ag.sg1,SQLSHORT,0);
    sqlin ((short *) &ag.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) ag.smt,SQLCHAR,2);
    sqlin ((double *) &ag.sp_fil,SQLDOUBLE,0);
    sqlin ((TCHAR *) ag.sp_kz,SQLCHAR,2);
    sqlin ((double *) &ag.sp_sk,SQLDOUBLE,0);
    sqlin ((double *) &ag.sp_vk,SQLDOUBLE,0);
    sqlin ((TCHAR *) ag.stk_lst_kz,SQLCHAR,2);
    sqlin ((double *) &ag.sw,SQLDOUBLE,0);
    sqlin ((double *) &ag.tara,SQLDOUBLE,0);
    sqlin ((short *) &ag.teil_smt,SQLSHORT,0);
    sqlin ((TCHAR *) ag.theke_eti,SQLCHAR,2);
    sqlin ((TCHAR *) ag.verk_art,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &ag.verk_beg,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &ag.verk_end,SQLDATE,0);
    sqlin ((TCHAR *) ag.waren_eti,SQLCHAR,2);
    sqlin ((long *) &ag.we_kto,SQLLONG,0);
    sqlin ((short *) &ag.wg,SQLSHORT,0);
    sqlin ((double *) &ag.gn_pkt_gbr,SQLDOUBLE,0);
    sqlin ((TCHAR *) ag.a_krz_kz,SQLCHAR,2);
    sqlin ((TCHAR *) ag.tier_kz,SQLCHAR,3);
    sqlin ((long *) &ag.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &ag.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &ag.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &ag.we_kto_1,SQLLONG,0);
    sqlin ((long *) &ag.we_kto_2,SQLLONG,0);
    sqlin ((long *) &ag.we_kto_3,SQLLONG,0);
            ins_cursor = sqlcursor (_T("insert into ag (abt,  ")
_T("ag,  ag_bz1,  ag_bz2,  a_typ,  bearb_fil,  bearb_lad,  bearb_sk,  best_auto,  bsd_kz,  ")
_T("delstatus,  durch_fil,  durch_sk,  durch_sw,  durch_vk,  erl_kto,  hbk,  hbk_kz,  ")
_T("hbk_ztr,  hnd_gew,  kost_kz,  me_einh,  mwst,  pfa,  pr_ueb,  reg_eti,  sais1,  sais2,  ")
_T("sg1,  sg2,  smt,  sp_fil,  sp_kz,  sp_sk,  sp_vk,  stk_lst_kz,  sw,  tara,  teil_smt,  ")
_T("theke_eti,  verk_art,  verk_beg,  verk_end,  waren_eti,  we_kto,  wg,  gn_pkt_gbr,  ")
_T("a_krz_kz,  tier_kz,  erl_kto_1,  erl_kto_2,  erl_kto_3,  we_kto_1,  we_kto_2,  ")
_T("we_kto_3) ")

#line 25 "ag.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 27 "ag.rpp"
}
