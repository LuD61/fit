#include "StdAfx.h"
#include "PtabList.h"

CPtabList::CPtabList(void)
{
	ptwert =  _T("");
	ptbez  = _T("");
	ptbezl1  = _T("");
	ptbezl2  = _T("");
	ptbezk = _T("");
	ptwer1 =  _T("");
	ptwer2 =  _T("");
}

CPtabList::CPtabList(LPSTR ptwert, LPTSTR ptbez,LPTSTR ptbezl1,LPTSTR ptbezl2, LPTSTR ptbezk, LPSTR ptwer1,LPSTR ptwer2)
{
	this->ptwert = ptwert;
	this->ptbez  = ptbez;
	this->ptbezl1  = ptbezl1;
	this->ptbezl2  = ptbezl2;
	this->ptbezk = ptbezk;
	this->ptwer1 = ptwer1;
	this->ptwer2 = ptwer2;
}

CPtabList::~CPtabList(void)
{
}
