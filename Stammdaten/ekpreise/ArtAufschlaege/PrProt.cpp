#include "StdAfx.h"
#include "prprot.h"
#include "StrFuncs.h"

CPrProt::CPrProt(void)
{
	PersName = "";
	Programm = "";
	Ek_pr = NULL;
}

CPrProt::CPrProt(CString& PersNam, CString& Programm, EK_PR_CLASS *Ek_pr)
{
	this->PersName = PersNam;
	this->Programm = Programm;
	this->Ek_pr = Ek_pr;
}

CPrProt::~CPrProt(void)
{
}

void CPrProt::Construct (CString& PersNam, CString& Programm, EK_PR_CLASS *Ek_pr)
{
	this->PersName = PersNam;
	this->Programm = Programm;
	this->Ek_pr = Ek_pr;
}

void CPrProt::Write (double pr_ek_alt, double pr_vk_alt)
{
	if (Ek_pr == NULL) return;

	memcpy (&Pr_prot.pr_prot, &pr_prot_null, sizeof  (PR_PROT));
	CString Date;
	CStrFuncs::SysDate (Date);
	Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.bearb);
	CString Time;
	CStrFuncs::SysTime (Time);

//	strcpy (Pr_prot.pr_prot.zeit, Time.GetBuffer (8));
	strcpy (Pr_prot.pr_prot.pers_nam, PersName.GetBuffer (8));
    Pr_prot.pr_prot.mdn = Ek_pr->ek_pr.mdn;
    Pr_prot.pr_prot.fil = Ek_pr->ek_pr.fil;
    Pr_prot.pr_prot.a = Ek_pr->ek_pr.a;
//    Pr_prot.pr_prot.pr_vk = Ek_pr->ek_pr.pr_vk;
//    Pr_prot.pr_prot.pr_vk_alt = pr_vk_alt;
//    Pr_prot.pr_prot.pr_vk_euro = Ek_pr->ek_pr.pr_vk_euro;
//    Pr_prot.pr_prot.pr_vk_alt_euro = pr_vk_alt;
    Date = "31.12.1899";
    Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.lad_akt_von);
    Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.lad_akt_bis);
    Pr_prot.dbinsert ();
}

