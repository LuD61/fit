#ifndef _A_BAS_DEF
#define _A_BAS_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_BAS {
   double         a;
   short          mdn;
   short          fil;
   TCHAR          a_bz1[25];
   TCHAR          a_bz2[25];
   double         a_gew;
   short          a_typ;
   short          a_typ2;
   short          abt;
   long           ag;
   TCHAR          best_auto[2];
   TCHAR          bsd_kz[2];
   TCHAR          cp_aufschl[2];
   short          delstatus;
   short          dr_folge;
   long           erl_kto;
   TCHAR          hbk_kz[2];
   short          hbk_ztr;
   TCHAR          hnd_gew[2];
   short          hwg;
   TCHAR          kost_kz[3];
   short          me_einh;
   TCHAR          modif[2];
   short          mwst;
   short          plak_div;
   TCHAR          stk_lst_kz[2];
   double         sw;
   short          teil_smt;
   long           we_kto;
   short          wg;
   short          zu_stoff;
   DATE_STRUCT    akv;
   DATE_STRUCT    bearb;
   TCHAR          pers_nam[9];
   double         prod_zeit;
   TCHAR          pers_rab_kz[2];
   double         gn_pkt_gbr;
   long           kost_st;
   TCHAR          sw_pr_kz[2];
   long           kost_tr;
   double         a_grund;
   long           kost_st2;
   long           we_kto2;
   long           charg_hand;
   long           intra_stat;
   TCHAR          qual_kng[5];
   TCHAR          a_bz3[25];
   short          lief_einh;
   double         inh_lief;
   long           erl_kto_1;
   long           erl_kto_2;
   long           erl_kto_3;
   long           we_kto_1;
   long           we_kto_2;
   long           we_kto_3;
   TCHAR          skto_f[2];
   double         sk_vollk;
   double         a_ersatz;
   short          a_ers_kz;
   short          me_einh_abverk;
   double         inh_abverk;
   TCHAR          hnd_gew_abverk[2];
   double         inh_ek;
   TCHAR          smt[2];
   double         a_leih;
   long           txt_nr1;
   long           txt_nr2;
   long           txt_nr3;
   long           txt_nr4;
   long           txt_nr5;
   long           txt_nr6;
   long           txt_nr7;
   long           txt_nr8;
   long           txt_nr9;
   long           txt_nr10;
   long           allgtxt_nr1;
   long           allgtxt_nr2;
   long           allgtxt_nr3;
   long           allgtxt_nr4;
   TCHAR          bild[129];
   short          vk_gr;
};
extern struct A_BAS a_bas, a_bas_null;

#line 8 "a_bas.rh"

class A_BAS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_BAS a_bas;  
               A_BAS_CLASS () : DB_CLASS ()
               {
               }
};
#endif
