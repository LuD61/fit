#ifndef _A_IPR_DEF
#define _A_IPR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct IPR {
   short          mdn;
   long           pr_gr_stuf;
   long           kun_pr;
   double         a;
   double         vk_pr_i;
   double         ld_pr;
   short          aktion_nr;
   TCHAR          a_akt_kz[2];
   TCHAR          modif[2];
   short          waehrung;
   long           kun;
   double         a_grund;
   TCHAR          kond_art[5];
   double         vk_pr_eu;
   double         ld_pr_eu;
   double         add_ek_proz;
   double         add_ek_abs;
};
extern struct IPR ipr, ipr_null;

class IPR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               IPR ipr;
               IPR_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (IPR&);  
};
#endif
