#ifndef _EK_APRPR_DEF
#define _EK_APRPR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct EK_PRPR {
   double         a;
   short          fil;
   short          mdn;
   short          akt;
   DATE_STRUCT    bearb;
   double         pr_ek;
   double         hk_kost;
   double         hk;
   double         sk_aufs;
   double         sk;
   double         fil_ek_aufs;
   double         fil_ek_kalk;
   double         fil_ek;
   double         fil_vk_aufs;
   double         fil_vk_kalk;
   double         fil_vk;
   double         gh1_aufs;
   double         gh1_kalk_vk;
   double         gh1;
   double         gh1_sp;
   double         gh2_aufs;
   double         gh2_kalk_vk;
   double         gh2;
   double         gh2_sp;
// Bis hierher identisch mit ek_pr 
   DATE_STRUCT    akv ;
   TCHAR           zeit[9] ;
   TCHAR           pers_nam[17] ;
   DATE_STRUCT    gue_ab ;
};
extern struct EK_PRPR ek_prpr, ek_prpr_null;

class EK_PRPR_CLASS : public DB_CLASS 
{
//       private :
//               void prepare (void);
       public :
               void prepare (void);
               EK_PRPR ek_prpr;
               EK_PRPR_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (EK_PRPR&);  
};

#endif
