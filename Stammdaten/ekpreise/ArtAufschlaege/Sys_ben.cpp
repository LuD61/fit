#include "stdafx.h"
#include "sys_ben.h"

struct SYS_BEN sys_ben, sys_ben_null;

void SYS_BEN_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &sys_ben.mdn, SQLSHORT, 0);
            sqlin ((short *)   &sys_ben.fil, SQLSHORT, 0);
            sqlin ((char *)    sys_ben.pers, SQLCHAR, sizeof (sys_ben.pers));
            sqlin ((char *)    sys_ben.pers_nam, SQLCHAR, sizeof (sys_ben.pers_nam));
    sqlout ((short *) &sys_ben.berecht,SQLSHORT,0);
    sqlout ((TCHAR *) sys_ben.dir_spr_vor,SQLCHAR,2);
    sqlout ((TCHAR *) sys_ben.dir_spr_zur,SQLCHAR,2);
    sqlout ((short *) &sys_ben.fil,SQLSHORT,0);
    sqlout ((short *) &sys_ben.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) sys_ben.pers,SQLCHAR,13);
    sqlout ((TCHAR *) sys_ben.pers_nam,SQLCHAR,9);
    sqlout ((TCHAR *) sys_ben.pers_passwd,SQLCHAR,13);
    sqlout ((long *) &sys_ben.zahl,SQLLONG,0);
    sqlout ((TCHAR *) sys_ben.ben_txt,SQLCHAR,33);
            cursor = sqlcursor (_T("select sys_ben.berecht,  ")
_T("sys_ben.dir_spr_vor,  sys_ben.dir_spr_zur,  sys_ben.fil,  sys_ben.mdn,  ")
_T("sys_ben.pers,  sys_ben.pers_nam,  sys_ben.pers_passwd,  sys_ben.zahl,  ")
_T("sys_ben.ben_txt from sys_ben ")

#line 15 "Sys_ben.rpp"
                                  _T("where mdn = ? ")
				  _T("and fil = ?")
				  _T("and pers = ?")
				  _T("and pers_nam = ?"));
    sqlin ((short *) &sys_ben.berecht,SQLSHORT,0);
    sqlin ((TCHAR *) sys_ben.dir_spr_vor,SQLCHAR,2);
    sqlin ((TCHAR *) sys_ben.dir_spr_zur,SQLCHAR,2);
    sqlin ((short *) &sys_ben.fil,SQLSHORT,0);
    sqlin ((short *) &sys_ben.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) sys_ben.pers,SQLCHAR,13);
    sqlin ((TCHAR *) sys_ben.pers_nam,SQLCHAR,9);
    sqlin ((TCHAR *) sys_ben.pers_passwd,SQLCHAR,13);
    sqlin ((long *) &sys_ben.zahl,SQLLONG,0);
    sqlin ((TCHAR *) sys_ben.ben_txt,SQLCHAR,33);
            sqltext = _T("update sys_ben set ")
_T("sys_ben.berecht = ?,  sys_ben.dir_spr_vor = ?,  ")
_T("sys_ben.dir_spr_zur = ?,  sys_ben.fil = ?,  sys_ben.mdn = ?,  ")
_T("sys_ben.pers = ?,  sys_ben.pers_nam = ?,  sys_ben.pers_passwd = ?,  ")
_T("sys_ben.zahl = ?,  sys_ben.ben_txt = ? ")

#line 20 "Sys_ben.rpp"
                                  _T("where mdn = ? ")
				  _T("and fil = ?")
				  _T("and pers = ?")
				  _T("and pers_nam = ?");
            sqlin ((short *)   &sys_ben.mdn, SQLSHORT, 0);
            sqlin ((short *)   &sys_ben.fil, SQLSHORT, 0);
            sqlin ((char *)    sys_ben.pers, SQLCHAR, sizeof (sys_ben.pers));
            sqlin ((char *)    sys_ben.pers_nam, SQLCHAR, sizeof (sys_ben.pers_nam));
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &sys_ben.mdn, SQLSHORT, 0);
            sqlin ((short *)   &sys_ben.fil, SQLSHORT, 0);
            sqlin ((char *)    sys_ben.pers, SQLCHAR, sizeof (sys_ben.pers));
            sqlin ((char *)    sys_ben.pers_nam, SQLCHAR, sizeof (sys_ben.pers_nam));
            test_upd_cursor = sqlcursor (_T("select pers from sys_ben ")
                                  _T("where mdn = ? ")
				  _T("and fil = ?")
				  _T("and pers = ?")
				  _T("and pers_nam = ?"));
            sqlin ((short *)   &sys_ben.mdn, SQLSHORT, 0);
            sqlin ((short *)   &sys_ben.fil, SQLSHORT, 0);
            sqlin ((char *)    sys_ben.pers, SQLCHAR, sizeof (sys_ben.pers));
            sqlin ((char *)    sys_ben.pers_nam, SQLCHAR, sizeof (sys_ben.pers_nam));
            del_cursor = sqlcursor (_T("delete from sys_ben ")
                                  _T("where mdn = ? ")
				  _T("and fil = ?")
				  _T("and pers = ?")
				  _T("and pers_nam = ?"));
    sqlin ((short *) &sys_ben.berecht,SQLSHORT,0);
    sqlin ((TCHAR *) sys_ben.dir_spr_vor,SQLCHAR,2);
    sqlin ((TCHAR *) sys_ben.dir_spr_zur,SQLCHAR,2);
    sqlin ((short *) &sys_ben.fil,SQLSHORT,0);
    sqlin ((short *) &sys_ben.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) sys_ben.pers,SQLCHAR,13);
    sqlin ((TCHAR *) sys_ben.pers_nam,SQLCHAR,9);
    sqlin ((TCHAR *) sys_ben.pers_passwd,SQLCHAR,13);
    sqlin ((long *) &sys_ben.zahl,SQLLONG,0);
    sqlin ((TCHAR *) sys_ben.ben_txt,SQLCHAR,33);
            ins_cursor = sqlcursor (_T("insert into sys_ben (")
_T("berecht,  dir_spr_vor,  dir_spr_zur,  fil,  mdn,  pers,  pers_nam,  pers_passwd,  ")
_T("zahl,  ben_txt) ")

#line 49 "Sys_ben.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?)")); 

#line 51 "Sys_ben.rpp"
            sqlin ((char *)    sys_ben.pers_nam, SQLCHAR, sizeof (sys_ben.pers_nam));
    sqlout ((short *) &sys_ben.berecht,SQLSHORT,0);
    sqlout ((TCHAR *) sys_ben.dir_spr_vor,SQLCHAR,2);
    sqlout ((TCHAR *) sys_ben.dir_spr_zur,SQLCHAR,2);
    sqlout ((short *) &sys_ben.fil,SQLSHORT,0);
    sqlout ((short *) &sys_ben.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) sys_ben.pers,SQLCHAR,13);
    sqlout ((TCHAR *) sys_ben.pers_nam,SQLCHAR,9);
    sqlout ((TCHAR *) sys_ben.pers_passwd,SQLCHAR,13);
    sqlout ((long *) &sys_ben.zahl,SQLLONG,0);
    sqlout ((TCHAR *) sys_ben.ben_txt,SQLCHAR,33);
            cursorpers_nam = sqlcursor (_T("select ")
_T("sys_ben.berecht,  sys_ben.dir_spr_vor,  sys_ben.dir_spr_zur,  ")
_T("sys_ben.fil,  sys_ben.mdn,  sys_ben.pers,  sys_ben.pers_nam,  ")
_T("sys_ben.pers_passwd,  sys_ben.zahl,  sys_ben.ben_txt from sys_ben ")

#line 53 "Sys_ben.rpp"
				  _T("where pers_nam = ?"));
}


int SYS_BEN_CLASS::dbreadfirstpers_nam ()
{
	    if (cursorpers_nam == -1)
            {
                    prepare ();
            }	
	    if (cursorpers_nam == -1) return -1;
            int dsqlstatus = sqlopen (cursorpers_nam);
            if (dsqlstatus == 0)
            {
                  dsqlstatus = sqlfetch (cursorpers_nam);
            }
            return dsqlstatus;
}

int SYS_BEN_CLASS::dbreadpers_nam ()
{
            return sqlfetch (cursorpers_nam);
}