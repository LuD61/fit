#ifndef _CHOICEAG_DEF
#define _CHOICEAG_DEF

#include "ChoiceX.h"
#include "AGList.h"
#include <vector>

class CChoiceAG : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;

    public :
	    std::vector<CAGList *> AGList;
	    std::vector<CAGList *> SelectList;
        CChoiceAG(CWnd* pParent = NULL);   // Standardkonstruktor
        ~CChoiceAG();
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchABz1 (CListCtrl *, LPTSTR);
        void SearchABz2 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CAGList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		void SaveSelection (CListCtrl *ListBox);
};
#endif
