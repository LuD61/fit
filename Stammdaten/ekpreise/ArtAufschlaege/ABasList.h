#ifndef _ABAS_LIST_DEF
#define _ABAS_LIST_DEF
#pragma once

class CABasList
{
public:
	double a;
	CString a_bz1;
	CString a_bz2;
	CString a_typ;
	CString ag;
	CString wg;
	CString hwg;
	CString teil_smt;
	CString me_einh;
	CString a_bz3;

	CABasList(void);
	CABasList(double, LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR);
	~CABasList(void);
};
#endif
