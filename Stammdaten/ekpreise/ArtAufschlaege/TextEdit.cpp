#include "StdAfx.h"
#include ".\textedit.h"

CTextEdit::CTextEdit(void)
{
}

CTextEdit::~CTextEdit(void)
{
}

BEGIN_MESSAGE_MAP(CTextEdit, CEdit)
	ON_WM_SETFOCUS ()
END_MESSAGE_MAP()

void CTextEdit::OnSetFocus (CWnd *oldFocus)
{
	CEdit::OnSetFocus (oldFocus);
	CString Text;
	GetWindowText (Text);
	PostMessage (EM_SETSEL, Text.GetLength (), Text.GetLength ());
}

