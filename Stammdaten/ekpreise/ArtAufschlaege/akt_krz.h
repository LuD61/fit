#ifndef _A_AKT_KRZ_DEF
#define _A_AKT_KRZ_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AKT_KRZ {
   double         a;
   DATE_STRUCT    bel_akt_bis;
   DATE_STRUCT    bel_akt_von;
   short          fil;
   short          fil_gr;
   DATE_STRUCT    lad_akt_bis;
   DATE_STRUCT    lad_akt_von;
   TCHAR          lad_akv_sa[2];
   TCHAR          lief_akv_sa[2];
   short          mdn;
   short          mdn_gr;
   double         pr_ek_sa;
   double         pr_vk_sa;
   TCHAR          zeit_bis[7];
   TCHAR          zeit_von[7];
   short          zeit_von_n;
   short          zeit_bis_n;
   double         pr_vk1_sa;
   double         pr_vk2_sa;
   double         pr_vk3_sa;
   TCHAR          dr_status[2];
   double         pr_ek_sa_euro;
   double         pr_vk_sa_euro;
};
extern struct AKT_KRZ akt_krz, akt_krz_null;

#line 8 "Akt_krz.rh"

class AKT_KRZ_CLASS : public DB_CLASS 
{
       private :
               int NewCursor;
       public :
               void prepare (void);
               AKT_KRZ akt_krz;
               AKT_KRZ_CLASS () : DB_CLASS ()
               {
		   NewCursor = -1;		
               }
               int dbreadfirst_new ();   
               int dbread_new ();   
               BOOL operator== (AKT_KRZ&);  
};
#endif
