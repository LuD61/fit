#pragma once
#include "sys_par.h"
#include "a_kalkpreis.h"
#include "a_bas.h"
#include "prdk_k.h"
#include "ag.h"
#include "wg.h"
#include "hwg.h"
#include "a_kalk_eig.h"
#include "a_kalk_mat.h"
#include "a_kalkhndw.h"
#include "PriceA_pr.h"
#include "PriceIpr.h"
#include "Ptabn.h"
#include "a_preis.h"

class CCalculate
{

private:
	SYS_PAR_CLASS Sys_par;
	PTABN_CLASS Ptabn;
	DB_CLASS *DbClass;
	A_KALKPREIS_CLASS A_kalkpreis;
	A_KALK_EIG_CLASS A_kalk_eig;
	A_KALK_MAT_CLASS A_kalk_mat;
	A_KALKHNDW_CLASS A_kalkhndw;
	A_PREIS_CLASS A_Preis;
	CPriceA_pr Pr_a_pr;
	CPriceIpr Pr_ipr;
	AG_CLASS Ag;
	WG_CLASS Wg;
	HWG_CLASS Hwg;
	CString SkText;
	CString FilEkText;
	CString FilVkText;
	double value1;
	double value2;
	double percent;
	int MarktSpPar;
	double mwst;
	double a_a_kalk;
	double a_a_bas;
	int ag;
	int fil_a_kalk;
	int mdn_a_kalk;
	double a_a_kalkpreis;
	int fil_a_kalkpreis;
	int mdn_a_kalkpreis;
public:
	enum
	{
		AufschlagBrutto = 1,
		AufschlagNetto  = 2,
		AbschlagBrutto = 3,
		AbschlagNetto  = 4,
		ReadSysPar     = 5
	};
	PRDK_K_CLASS *Prdk_k;
	A_BAS_CLASS *A_bas;
	CCalculate(void);
	~CCalculate(void);
	void SetValues (double value1, double value2);
	void SetMwst (double mwst);
	void Init (void);
	double GetPercent ();
	double execute ();
	double execute (double value1, double value2, double a);
	CString GetSkText (void);
	CString GetFilEkText (void);
	CString GetFilVkText (void);
	void LeseA_kalkpreis (void);
	void LeseA_kalk_eig (void);
	void LeseA_kalk_mat (void);
	void LeseA_kalkhndw (void);
	void LeseA_bas (void);
	BOOL isaktiveRezeptur (void);
	BOOL isEigenprodukt (double a);
	BOOL isHandelsware (double a);
	BOOL isMaterial (double a);
	void WriteBearbKost (short mdn, short fil, double a, double kost);
	double GetBearbKost (short mdn, short fil, double a);
	double GetHk (short mdn, short fil, double a);
	double GetSk (short mdn, short fil, double a);
	double GetSkAufs (short mdn, short fil, double a);
	double GetFilEkAufs (short mdn, short fil, double a);
	double GetFilVkAufs (short mdn, short fil, double a);
	double GetFilEkKalk (short mdn, short fil, double a);
	double GetFilVkKalk (short mdn, short fil, double a);
	double GetMatoB (short mdn, short fil, double a);
	double GetHkVollk (short mdn, short fil, double a);
	double GetFilEk (short mdn, short fil, double a);
	double GetFilVk (short mdn, short fil, double a);
	double GetA_gew (double a);
	double GetVkGrundartikel (short mdn, double a, int pr_gr_stuf, BOOL Grundartikel);
	double GetLDPRGrundartikel (short mdn, double a, int pr_gr_stuf, BOOL Grundartikel);
	double KalkAufschlaegeGH (short mdn, double a, int pr_gr_stuf, BOOL Grundartikel);
	double KalkAufschlaegeTOP (short mdn, double a, int pr_gr_stuf, BOOL Grundartikel);
	double KalkAufschlaegeLIMIT (short mdn, double a, int pr_gr_stuf, BOOL Grundartikel);
	double KalkAufschlagEmpfVk (short mdn, double a, int pr_gr_stuf, BOOL Grundartikel);
	double KalkAufschlaegeLDPR (short mdn, double a, int pr_gr_stuf, BOOL Grundartikel);
	double SchoenePreise (double preis); //WAL-127
	double ptabAufschlag (short aufschlag, double preis);
	double GetAufschlag (double a, short typ);
	double GetAddEkProz (short mdn, double a, int pr_gr_stuf);
	double GetVkSpanne (short mdn, double a, int pr_gr_stuf,double sk);
	double GetKalkVk (short mdn, double a, int pr_gr_stuf, double sk);
	int    UpdateIpr(short mdn, long PrGrStuf,double a, double pr);
	void clipped (char *str)
	{
		UCHAR *p = (UCHAR *) str;
		p += strlen (str);
		for (; (p >= (UCHAR *) str) && (*p < 0x21) ; p -= 1);
		*(p + 1) = 0;
	}

};
