#pragma once
#include "dbpropertysheet.h"

class CAPrSheet :
	public CDbPropertySheet
{
	DECLARE_DYNAMIC(CAPrSheet)
public:
	CAPrSheet(void);
	~CAPrSheet(void);
	virtual BOOL Write ();
//	virtual BOOL Print ();
};
