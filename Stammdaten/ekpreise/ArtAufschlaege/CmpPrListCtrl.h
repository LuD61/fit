#pragma once
#include <vector>
#include "editlistctrl.h"
#include "a_bas.h"
#include "mdn.h"
#include "fil.h"
#include "gr_zuord.h"
#include "Adr.h"
#include "a_pr.h"
#include "ChoiceA.h"
#include "ChoiceMdn.h"
#include "Choicefil.h"
#include "Choiceptab.h"
#include "ChoiceGrZuord.h"
#include "a_kalkpreis.h"
#include "lst.h"
#include "mo_progcfg.h"
#include "ek_pr.h"
#include "calculate.h"
#include "ptabn.h"


#define MAXLISTROWS 30

class CCmpPrListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	PROG_CFG Cfg;
	EK_PR_CLASS Ek_pr;

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
	};

	enum LISTPOS
	{
		POSA = 1,
		POSMDNGR = 2,
		POSMDN    = 3,
		POSFILGR = 4,
		POSFIL  = 5,
		POS_PREK     = 6,
	//LuD Neu
		POS_BEARBKOST = 7,
		POS_KALK_HK = 8,
		POS_AUFS_SK	= 9,
		POS_KALK_SK	= 10,
		POS_AUFS_FIL_EK = 11,
		POS_KALK_FIL_EK	= 12,
		POS_FIL_EK = 13,
		POS_AUFS_FIL_VK	= 14,
		POS_KALK_FIL_VK = 15,
		POS_FIL_VK	= 16,
		POS_AUFS_GH1 = 17,
		POS_KALKVK_GH1 = 18,
		POS_GH1	= 19,
		POS_SP_GH1 = 20,
		POS_AUFS_GH2 = 21,
		POS_KALKVK_GH2 = 22,
		POS_GH2 = 23,
		POS_SP_GH2 = 24,
		POSA2 = 25,






//		POSPRVK     = 7,
		POSPREKKALK     = 8,
		POSPRVKKALK     = 9,
		POSSK     = 10,
		POSSP     = 11,
	};

	int PosA;
	int PosMdnGr;
    int PosMdn;
	int PosFilGr;
	int PosFil;
	int Pos_PrEk;
	//LuD Neu
	int Pos_BearbKost;
	int Pos_Kalk_Hk;
	int	Pos_Aufs_Sk;
	int	Pos_Kalk_Sk;
	int	Pos_Aufs_Fil_Ek;
	int	Pos_Kalk_Fil_Ek;
	int	Pos_Fil_Ek;
	int	Pos_Aufs_Fil_Vk;
	int	Pos_Kalk_Fil_Vk;
	int	Pos_Fil_Vk;

	int	Pos_Aufs_Gh1;
	int	Pos_KalkVk_Gh1;
	int	Pos_Gh1;
	int	Pos_Sp_Gh1;
	int	Pos_Aufs_Gh2;
	int	Pos_KalkVk_Gh2;
	int	Pos_Gh2;
	int	Pos_Sp_Gh2;
	int	PosA2;
	int Neuanlage;
	BOOL NoColChanged;
	BOOL RowDeleted;
	char KalkulationsModus[32];
	char SchreibModus[32];







//	int PosPrVk;
//	int PosPrEkKalk;
//	int PosPrVkKalk;
//	int PosSk;
//	int PosSp;

    int *Position[25];

	short m_Mdn;
	int FilGrCursor;
	int FilCursor;
	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	char Kalkulationsaufruf[256];
	int  PrVkVisible;
	int oldsel;
	std::vector<BOOL> vSelect;
	CVector MdnGrCombo;
	CVector MdnCombo;
	CVector FilGrCombo;
	CVector FilCombo;
	CString ArtSelect;
	CChoiceA *ChoiceA;
	BOOL ModalChoiceA;
	BOOL AChoiceStat;
	CChoiceGrZuord *ChoiceMdnGr;
	BOOL ModalChoiceMdnGr;
	BOOL MdnGrChoiceStat;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	BOOL MdnChoiceStat;
	CChoicePtab *ChoicePtab;
	BOOL ModalChoicePtab;
	BOOL PtabChoiceStat;
	CChoiceGrZuord *ChoiceFilGr;
	BOOL ModalChoiceFilGr;
	BOOL FilGrChoiceStat;
	CChoiceFil *ChoiceFil;
	BOOL ModalChoiceFil;
	BOOL FilChoiceStat;
	CVector ListRows;

	A_BAS_CLASS A_bas;
	LST_CLASS *Lst;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	FIL_CLASS Fil;
	GR_ZUORD_CLASS Gr_zuord;
	ADR_CLASS FilAdr;
	A_KALKPREIS_CLASS A_kalkpreis;
	PTABN_CLASS Ptabn;
	CCalculate Calculate;

	//LuD Neu m_list. ...

	int ALen;
	int EkLen;
	int HkLen;
	int HkKostLen;
	int SkAufsLen;
	int SkLen;
	int FilEkAufsLen;
	int FilEkKalkLen;	
	int FilEkLen;
	int FilVkAufsLen;
	int FilVkKalkLen;
	int FilVkLen;
	int Gh1AufsLen;
	int Gh1KalkVkLen;
	int Gh1Len;
	int Gh1SpLen;
	int Gh2AufsLen;
	int Gh2KalkVkLen;
	int Gh2Len;		
	int Gh2SpLen;
	int ALen2;

	int AEdit;
	int EkEdit;
	int HkEdit;
	int HkKostEdit;
	int SkAufsEdit;
	int SkEdit;
	int FilEkAufsEdit;
	int FilEkKalkEdit;	
	int FilEkEdit;
	int FilVkAufsEdit;
	int FilVkKalkEdit;
	int FilVkEdit;
	int Gh1AufsEdit;
	int Gh1KalkVkEdit;
	int Gh1Edit;
	int Gh1SpEdit;
	int Gh2AufsEdit;
	int Gh2KalkVkEdit;
	int Gh2Edit;		
	int Gh2SpEdit;

	int EkNachkomma;
	int HkNachkomma;
	int HkKostNachkomma;
	int SkAufsNachkomma;
	int SkNachkomma;
	int FilEkAufsNachkomma;
	int FilEkKalkNachkomma;	
	int FilEkNachkomma;
	int FilVkAufsNachkomma;
	int FilVkKalkNachkomma;
	int FilVkNachkomma;
	int Gh1AufsNachkomma;
	int Gh1KalkVkNachkomma;
	int Gh1Nachkomma;
	int Gh1SpNachkomma;
	int Gh2AufsNachkomma;
	int Gh2KalkVkNachkomma;
	int Gh2Nachkomma;		
	int Gh2SpNachkomma;

	int EkPruef;
	int HkPruef;

	double SkAufs;
	double Sk;
	double FilEkAufs;
	double FilEkKalk;	
	double FilEk;
	double FilVkAufs;
	double FilVkKalk;
	double FilVk;
	double Gh1Aufs;
	double Gh1KalkVk;
	double Gh1;
	double Gh1Sp;
	double Gh2Aufs;
	double Gh2KalkVk;
	double Gh2;		
	double Gh2Sp;

	char SkAufsBez[32];
	char SkBez[32];
	char FilEkAufsBez[32];
	char FilEkKalkBez[32];	
	char FilEkBez[32];
	char FilVkAufsBez[32];
	char FilVkKalkBez[32];
	char FilVkBez[32];
	char Gh1AufsBez[32];
	char Gh1KalkVkBez[32];
	char Gh1Bez[32];
	char Gh1SpBez[32];
	char Gh2AufsBez[32];
	char Gh2KalkVkBez[32];
	char Gh2Bez[32];		
	char Gh2SpBez[32];




	double HkKost;
	double Hk;
	double sk_vollk;
	double spanne;
	double fil_ek_vollk;
	double fil_vk_vollk;
	char EkBez[32];
	char HkKostBez[32];
	char HkBez[32];
	int Gh1PrGrStuf;
	int Gh2PrGrStuf;

	CCmpPrListCtrl(void);
	~CCmpPrListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int, BOOL readonly = FALSE); //testtesttest
//	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	void FillMdnGrCombo (CVector&);
	void FillMdnCombo (CVector&);
	void FillFilGrCombo (CVector&);
	void FillFilCombo (CVector&);
	void OnChoice ();
	void OnAChoice (CString &);
	void OnMdnGrChoice (CString &);
	void OnPtabChoice (CString &);
	void OnMdnChoice (CString &);
	void OnFilGrChoice (CString &);
	void OnFilChoice (CString &);
	void OnKey9 ();
    void ReadA ();
    void ReadEk_pr ();
    void Ek_pr_to_ItemText (int Row);
    void ReadMdnGr ();
    void ReadMdn ();
    void ReadFilGr ();
    void ReadFil ();
    void FillFilGrCombo (int row);
    void FillFilCombo (int row);
    void ReadFilCombo (int mdn);
    void ReadFilGrCombo (int mdn);
    void GetColValue (int row, int col, CString& Text);
    BOOL TestAprIndex ();
    BOOL TestAprIndexM (int EditRow);
	void ScrollPositions (int pos);
	BOOL isDisplayonly (int Col, int Row);
	BOOL isDisplayonly (int Col);
	int Nachkomma (int Col);
	BOOL LastCol ();
	int GetFirstCol ();
	int GetLastCol ();
	void Prepare ();


};
