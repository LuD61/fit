#include "stdafx.h"
#include "iv_pr.h"

struct IV_PR iv_pr, iv_pr_null;

void IV_PR_CLASS::prepare (void)
{
 
            sqlin ((short *)   &iv_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &iv_pr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &iv_pr.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &iv_pr.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &iv_pr.kun, SQLLONG, 0);
// das gibt der Index nicht her :       sqlin ((DATE_STRUCT *)    &iv_pr.gue_ab, SQLDATE, 0);
            del_cursor = sqlcursor (_T("delete from iv_pr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ") );
// 				  _T("and kun = ?" ) ) ;
//				  _T(" and gue_ab = ?" ));

    sqlin ((short *) &iv_pr.mdn,SQLSHORT,0);

    sqlin ((long *) &iv_pr.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &iv_pr.kun_pr,SQLLONG,0);
    sqlin ((double *) &iv_pr.a,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.ld_pr,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &iv_pr.gue_ab,SQLDATE,0);
    sqlin ((short *) &iv_pr.waehrung,SQLSHORT,0);

	sqlin ((double *) &iv_pr.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((long *) &iv_pr.kun,SQLLONG,0);

	ins_cursor = sqlcursor (_T("insert into iv_pr (mdn,  ")
_T("pr_gr_stuf,  kun_pr,  a,  vk_pr_i,  ld_pr, gue_ab,  waehrung,  ")
_T(" vk_pr_eu,  ld_pr_eu, kun ) ")

_T("values ")
_T("(?,?,?,?,?,?,?,?,?,?,? )"));

}

BOOL IV_PR_CLASS::operator== (IV_PR& iv_pr)
{
            if (this->iv_pr.pr_gr_stuf != iv_pr.pr_gr_stuf) return FALSE;  
            if (this->iv_pr.kun_pr     != iv_pr.kun_pr) return FALSE;  
            if (this->iv_pr.kun        != iv_pr.kun) return FALSE;  
            if (this->iv_pr.a          != iv_pr.a) return FALSE;  
            return TRUE;
} 
