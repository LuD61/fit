#include "stdafx.h"
#include "a_preis.h"

struct A_PREIS a_preis, a_preis_null;

void A_PREIS_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &a_preis.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_preis.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_preis.a, SQLDOUBLE, 0);
    sqlout ((long *) &a_preis.mdn,SQLLONG,0);
    sqlout ((short *) &a_preis.fil,SQLSHORT,0);
    sqlout ((double *) &a_preis.a,SQLDOUBLE,0);
    sqlout ((short *) &a_preis.prodabt,SQLSHORT,0);
    sqlout ((short *) &a_preis.druckfolge,SQLSHORT,0);
    sqlout ((double *) &a_preis.ek,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.plan_ek,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.kostenkg,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.kosteneinh,SQLDOUBLE,0);
    sqlout ((short *) &a_preis.flg_bearb_weg,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_hk,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p1,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p2,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p3,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p4,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p5,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_p6,SQLSHORT,0);
    sqlout ((short *) &a_preis.bearb_vk,SQLSHORT,0);
    sqlout ((double *) &a_preis.sp_sk,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.sp_fil,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.sp_vk,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &a_preis.dat,SQLDATE,0);
    sqlout ((TCHAR *) a_preis.zeit,SQLCHAR,6);
    sqlout ((TCHAR *) a_preis.pers_nam,SQLCHAR,9);
    sqlout ((long *) &a_preis.inh_ek,SQLLONG,0);
    sqlout ((double *) &a_preis.bep,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.plan_bep,SQLDOUBLE,0);
    sqlout ((double *) &a_preis.test_ek,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select a_preis.mdn,  ")
_T("a_preis.fil,  a_preis.a,  a_preis.prodabt,  a_preis.druckfolge,  ")
_T("a_preis.ek,  a_preis.plan_ek,  a_preis.kostenkg,  a_preis.kosteneinh,  ")
_T("a_preis.flg_bearb_weg,  a_preis.bearb_hk,  a_preis.bearb_p1,  ")
_T("a_preis.bearb_p2,  a_preis.bearb_p3,  a_preis.bearb_p4,  ")
_T("a_preis.bearb_p5,  a_preis.bearb_p6,  a_preis.bearb_vk,  ")
_T("a_preis.sp_sk,  a_preis.sp_fil,  a_preis.sp_vk,  a_preis.dat,  ")
_T("a_preis.zeit,  a_preis.pers_nam,  a_preis.inh_ek,  a_preis.bep,  ")
_T("a_preis.plan_bep,  a_preis.test_ek from a_preis ")

#line 14 "a_preis.rpp"
                                  _T("where mdn = ? and fil = ? ")
				  _T("and a = ?"));
    sqlin ((long *) &a_preis.mdn,SQLLONG,0);
    sqlin ((short *) &a_preis.fil,SQLSHORT,0);
    sqlin ((double *) &a_preis.a,SQLDOUBLE,0);
    sqlin ((short *) &a_preis.prodabt,SQLSHORT,0);
    sqlin ((short *) &a_preis.druckfolge,SQLSHORT,0);
    sqlin ((double *) &a_preis.ek,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.plan_ek,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.kostenkg,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.kosteneinh,SQLDOUBLE,0);
    sqlin ((short *) &a_preis.flg_bearb_weg,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_hk,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p1,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p2,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p3,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p4,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p5,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p6,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_vk,SQLSHORT,0);
    sqlin ((double *) &a_preis.sp_sk,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.sp_fil,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.sp_vk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_preis.dat,SQLDATE,0);
    sqlin ((TCHAR *) a_preis.zeit,SQLCHAR,6);
    sqlin ((TCHAR *) a_preis.pers_nam,SQLCHAR,9);
    sqlin ((long *) &a_preis.inh_ek,SQLLONG,0);
    sqlin ((double *) &a_preis.bep,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.plan_bep,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.test_ek,SQLDOUBLE,0);
            sqltext = _T("update a_preis set a_preis.mdn = ?,  ")
_T("a_preis.fil = ?,  a_preis.a = ?,  a_preis.prodabt = ?,  ")
_T("a_preis.druckfolge = ?,  a_preis.ek = ?,  a_preis.plan_ek = ?,  ")
_T("a_preis.kostenkg = ?,  a_preis.kosteneinh = ?,  ")
_T("a_preis.flg_bearb_weg = ?,  a_preis.bearb_hk = ?,  ")
_T("a_preis.bearb_p1 = ?,  a_preis.bearb_p2 = ?,  a_preis.bearb_p3 = ?,  ")
_T("a_preis.bearb_p4 = ?,  a_preis.bearb_p5 = ?,  a_preis.bearb_p6 = ?,  ")
_T("a_preis.bearb_vk = ?,  a_preis.sp_sk = ?,  a_preis.sp_fil = ?,  ")
_T("a_preis.sp_vk = ?,  a_preis.dat = ?,  a_preis.zeit = ?,  ")
_T("a_preis.pers_nam = ?,  a_preis.inh_ek = ?,  a_preis.bep = ?,  ")
_T("a_preis.plan_bep = ?,  a_preis.test_ek = ? ")

#line 17 "a_preis.rpp"
                                  _T("where mdn = ? and fil = ? ")
				  _T("and a = ?");
            sqlin ((short *)   &a_preis.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_preis.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_preis.a, SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &a_preis.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_preis.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_preis.a, SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_preis ")
                                  _T("where mdn = ? and fil = ? ")
				  _T("and a = ?"));
            sqlin ((short *)   &a_preis.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_preis.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_preis.a, SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_preis ")
                                  _T("where mdn = ? and fil = ? ")
				  _T("and a = ?"));
    sqlin ((long *) &a_preis.mdn,SQLLONG,0);
    sqlin ((short *) &a_preis.fil,SQLSHORT,0);
    sqlin ((double *) &a_preis.a,SQLDOUBLE,0);
    sqlin ((short *) &a_preis.prodabt,SQLSHORT,0);
    sqlin ((short *) &a_preis.druckfolge,SQLSHORT,0);
    sqlin ((double *) &a_preis.ek,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.plan_ek,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.kostenkg,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.kosteneinh,SQLDOUBLE,0);
    sqlin ((short *) &a_preis.flg_bearb_weg,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_hk,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p1,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p2,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p3,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p4,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p5,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_p6,SQLSHORT,0);
    sqlin ((short *) &a_preis.bearb_vk,SQLSHORT,0);
    sqlin ((double *) &a_preis.sp_sk,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.sp_fil,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.sp_vk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_preis.dat,SQLDATE,0);
    sqlin ((TCHAR *) a_preis.zeit,SQLCHAR,6);
    sqlin ((TCHAR *) a_preis.pers_nam,SQLCHAR,9);
    sqlin ((long *) &a_preis.inh_ek,SQLLONG,0);
    sqlin ((double *) &a_preis.bep,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.plan_bep,SQLDOUBLE,0);
    sqlin ((double *) &a_preis.test_ek,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into a_preis (")
_T("mdn,  fil,  a,  prodabt,  druckfolge,  ek,  plan_ek,  kostenkg,  kosteneinh,  ")
_T("flg_bearb_weg,  bearb_hk,  bearb_p1,  bearb_p2,  bearb_p3,  bearb_p4,  ")
_T("bearb_p5,  bearb_p6,  bearb_vk,  sp_sk,  sp_fil,  sp_vk,  dat,  zeit,  pers_nam,  ")
_T("inh_ek,  bep,  plan_bep,  test_ek) ")

#line 37 "a_preis.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 39 "a_preis.rpp"
}

BOOL A_PREIS_CLASS::operator== (A_PREIS& a_preis)
{
            if (this->a_preis.mdn    != a_preis.mdn) return FALSE;  
            if (this->a_preis.fil    != a_preis.fil) return FALSE;  
            if (this->a_preis.a      != a_preis.a) return FALSE;  
            return TRUE;
} 

