#include "StdAfx.h"
#include "HWGList.h"

CHWGList::CHWGList(void)
{
	hwg = 0;
	hwg_bz1 = _T("");
	hwg_bz2 = _T("");
}

CHWGList::CHWGList(long hwg, LPTSTR hwg_bz1, LPTSTR hwg_bz2)
{
	this->hwg  = hwg;
	this->hwg_bz1 = hwg_bz1;
	this->hwg_bz2 = hwg_bz2;
}

CHWGList::~CHWGList(void)
{
}
