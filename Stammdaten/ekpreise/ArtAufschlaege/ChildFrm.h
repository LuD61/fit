// ChildFrm.h : Schnittstelle der Klasse CChildFrame
//
#include "vector.h"


#pragma once


class CChildFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CChildFrame)
public:
	CChildFrame();

// Attribute
public:

// Operationen
public:

// Überschreibungen
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual BOOL OnCreateClient(LPCREATESTRUCT, CCreateContext*);

// Implementierung
public:
	virtual ~CChildFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
    afx_msg void OnSize(UINT, int, int);

public:
	static CVector StdPrWnd;
	static CVector NewPrWnd;
	static CVector ArtAktWnd;
	BOOL InitSize;
	afx_msg void OnDestroy();
	void OnRecChange();

};
