// datDial.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "AprMan.h"
#include "datDial.h"


// CdatDial-Dialogfeld

IMPLEMENT_DYNAMIC(CdatDial, CPropertyPage)

CdatDial::CdatDial()
	: CPropertyPage(CdatDial::IDD)
	, v_edit1(_T(""))
{

}

CdatDial::~CdatDial()
{
}

void CdatDial::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit1);
	DDX_Text(pDX, IDC_EDIT1, v_edit1);
}


BEGIN_MESSAGE_MAP(CdatDial, CPropertyPage)
	ON_EN_KILLFOCUS(IDC_EDIT1, &CdatDial::OnEnKillfocusEdit1)
	ON_BN_CLICKED(IDOK, &CdatDial::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CdatDial::OnBnClickedCancel)
END_MESSAGE_MAP()


// CdatDial-Meldungshandler

static int dialtyp ;
static int retcode ;
static int tag,monat,jahr ;

void CdatDial::putdatum(char * edatum,int etag,int emonat,int ejahr)
{
	tag = etag ;
	monat = emonat ;
	jahr = ejahr ;

	v_edit1 = _T( edatum) ;
}

void CdatDial::puttyp(int etyp)
{

	dialtyp = etyp  ;
}


int CdatDial::getretcode(void)
{
	return retcode ;
}

char * CdatDial::getdatum(char * edatum)
{

	char *S= v_edit1.GetBuffer(512) ; 

	sprintf ( edatum,"%s",S);

	return edatum ;
}

BOOL CdatDial::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}

			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
	}
    return CDialog::PreTranslateMessage(pMsg);
}


BOOL CdatDial::OnInitDialog()
{


		retcode = 0 ;
		if ( dialtyp == 0 )
			SetWindowText(_T("B1-Preise"));
		if ( dialtyp == 1 )
			SetWindowText(_T("GH-Preise"));
		UpdateData(FALSE) ;
	return TRUE ;
}

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	int k = 0 ;	
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		if ( i < 6 ) k = -1 ;
		if ( k == 0 )
		{
			// die ersten beiden Zeichen MUESSEN ZAHLEN sein
			if ( bufi[0] > '9' || bufi[0] < '0' ||bufi[1] > '9' || bufi[1] < '0' )
				k = - 1 ;
		}
		if ( k == 0 )
		{
			if ( bufi[2] > '9' || bufi[2] < '0' )
				k = 2 ;		// Typ 8 oder typ 10 moeglich

		}
		if ( k == 0 )
		{
			// nur noch Typ 6 erlaubt 
			if (    bufi[3] > '9' || bufi[3] < '0'
				 || bufi[4] > '9' || bufi[4] < '0'
				 || bufi[5] > '9' || bufi[5] < '0' )
			{
				k = - 1 ;
			}
			else
			{	// Gueltiger Typ 6  ......
				k = 1 ;
				i = 6 ;
				bufi[6] = '\0' ;
			}
		}
		if ( k == 2 )
		{
			if ( i < 8 )
			{	
				k = -1 ;
			}
			else
			{	// 4.,5.,7.,8. Zeichen MUESSEN ZAHLEN sein, 6.Zeichen MUSS Nicht-Zahl sein
				if (   bufi[3] > '9' || bufi[3] < '0' 
					 ||bufi[4] > '9' || bufi[4] < '0'
				     ||bufi[6] > '9' || bufi[6] < '0'
				     ||bufi[7] > '9' || bufi[7] < '0'
				     || !( bufi[5] >'9' || bufi[5]< '0')
				    )
				k = - 1 ;
			}
		}
		if ( k == 2 )
		{
			if ( bufi[8] > '9' || bufi[8] < '0' )
			{	// gueltiger Typ 8 
				k = 1 ;
				i = 8 ;
				bufi[8] = '\0' ;
			}
		
		}

		if ( k == 2 )
		{
			if ( i < 10 )
			{	
				k = -1 ;
			}
			else
			{	// 9.,10. Zeichen MUESSEN ZAHLEN sein
				if (   bufi[8] > '9' || bufi[8] < '0' 
					 ||bufi[9] > '9' || bufi[9] < '0'
			       )
				{
					k = -1 ;
				}
				else
				{
					k = 1 ;
					i = 10 ;
					bufi[10] = '\0' ;
				}
			}
		}
		if ( k < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;	// ungueltige Eingabe -> Leerstring	
		}
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;	// Leerstring weil Mist eingepresst wurde
		}

// Zusatzcheck : gr�sser gleich Untergrenze


			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil1 < jahr )
			{
//				bufi[0] = '\0' ;
//				return TRUE ;	// Leerstring weil Jahr zu klein 
			}

			if ( hil1 == jahr )	// dann sind weite tests notwendig
			{
				hil5[0] = bufi[3] ;
				hil5[1] = bufi[4] ;
				hil5[2] = '\0' ;
				hil1    = atoi(hil5) ;
				if ( hil1 < monat )
				{
//					bufi[0] = '\0' ;
//					return TRUE ;	// Leerstring weil Monat zu klein 
				}
				if ( hil1 == monat )	// dann sind weite tests notwendig
				{
					hil5[0] = bufi[0] ;
					hil5[1] = bufi[1] ;
					hil5[2] = '\0' ;
					hil1    = atoi(hil5) ;
					if ( hil1 < tag )
					{
//						bufi[0] = '\0' ;
//						return TRUE ;	// Leerstring weil Monat zu klein 
					}
				}

			}






		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;			// Leerstring weil Mist eingepresst wurde
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;			// Leerstring weil Mist eingepresst wurde
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;		// Leerstring weil Mist eingepresst wurde
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;	// Leerstring weil Mist eingepresst wurde
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;		// Leerstring weil Mist eingepresst wurde
				};
			}
			break ;

		}

	}
	return j ;

}
static char bufh [550] ;

void CdatDial::OnEnKillfocusEdit1()
{
// Datums-Check 
	UpdateData(TRUE) ;
	int	i = m_edit1.GetLine(0,bufh,500) ;
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_edit1.Format("%s",_T(bufh));
		}
	}
	UpdateData (FALSE) ;
	return ;

}

void CdatDial::OnBnClickedOk()
{

	UpdateData (FALSE) ;

	int	i = m_edit1.GetLine(0,bufh,500) ;
	if (i < 10 )
	{
		MessageBox("Ung�ltiges Datum !", " ", MB_OK|MB_ICONSTOP);
		retcode = 0 ;

		CWnd *Control ;
		Control = &m_edit1 ;
		Control->SetFocus ();

		return ;
	}

	retcode = 1 ;
	CDialog::OnOK();
	return  ;
}

void CdatDial::OnBnClickedCancel()
{

	retcode = 0 ;
	CDialog::OnCancel();
}



BOOL CdatDial::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_edit1)
	{
		UpdateData(TRUE) ;
		int	i = m_edit1.GetLine(0,bufh,500) ;
		if (i)
		{
			if (datumcheck(bufh))
			{
				v_edit1.Format("%s",_T(bufh));
			}
		}
		UpdateData (FALSE) ;

		Control = GetNextDlgTabItem (Control, FALSE);
		if (Control != NULL)
		{
			Control->SetFocus ();
			return TRUE;
		}
		else
			return FALSE;

	}
	return FALSE ;
}
