// PrArtPreise.cpp : Implementierungsdatei
//

#include "stdafx.h"
#ifdef ARTIKEL
#include "Artikel.h"
#else
#include "AprMan.h"
#endif
#include "ArtPrPage.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Process.h"
#include "GrounPrice.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CArtPrPage Dialogfeld

IMPLEMENT_DYNAMIC(CArtPrPage, CDbPropertyPage)
HANDLE CArtPrPage::Write160Lib = NULL;

CArtPrPage::CArtPrPage(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage()
{
	Choice = NULL;
	ModalChoice = FALSE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	AprCursor = -1;
	MdnGrCursor = -1;
	MdnCursor = -1;
	FilCursor = -1;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_A);
	PosControls.Add (&m_List);

    HideButtons = FALSE;

	Frame = NULL;
	DbRows.Init ();
	ListRows.Init ();
	SearchA = _T("");
	Separator = _T(";");
	CellHeight = 0;
    dbild160 = NULL;
	dOpenDbase = NULL;
	Write160 = TRUE;
	Cfg.SetProgName( _T("-- --"));
	ArchiveName = _T("AprMan.prp");
	Load ();
}

CArtPrPage::CArtPrPage(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	Frame = NULL;
	AprCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
	ArchiveName = _T("AprMan.prp");
	Load ();
}

CArtPrPage::~CArtPrPage()
{
	Save ();
	Font.DeleteObject ();

	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	A_pr.dbclose ();
	MdnAdr.dbclose ();
	if (AprCursor != -1)
	{
		A_bas.sqlclose (AprCursor);
	}

	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);
}

void CArtPrPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_LA_BZ1, m_LA_bz1);
	DDX_Control(pDX, IDC_A_BZ1, m_A_bz1);
	DDX_Control(pDX, IDC_A_BZ2, m_A_bz2);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_GROUND_PRICE, m_GrounPrice);
}

BEGIN_MESSAGE_MAP(CArtPrPage, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_SIZE ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_ACHOICE ,  OnAchoice)
	ON_BN_CLICKED(IDC_CANCEL ,   OnCancel)
	ON_BN_CLICKED(IDC_SAVE ,     OnSave)
	ON_BN_CLICKED(IDC_DELETE ,   OnDelete)
	ON_BN_CLICKED(IDC_INSERT ,   OnInsert)
	ON_COMMAND (SELECTED, OnASelected)
	ON_COMMAND (CANCELED, OnACanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_COMMAND(IDC_GROUND_PRICE, OnGroundPrice)
END_MESSAGE_MAP()


// CArtPrPage Meldungshandler

BOOL CArtPrPage::OnInitDialog()
{
	extern short sql_mode;
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	A_bas.opendbase (_T("bws"));

    sql_mode = 1;
    A_bas.sqlcomm (_T("update a_pr set pr_ek_euro = 0 where pr_ek_euro is null"));
    A_bas.sqlcomm (_T("update a_pr set pr_vk_euro = 0 where pr_vk_euro is null"));
    sql_mode = 0;
	CUtil::GetPersName (PersName);
//	PrProt.Construct (PersName, CString ("11200"), &A_pr);
	GrundPreis.Create (IDD_GRUND_PREIS, this);
     
	ReadCfg ();

	Etikett.ErfKz = _T("P");
	Etikett.A_bas = &A_bas;
	strcpy (sys_par.sys_par_nam, "reg_eti_par");
	if (Sys_par.dbreadfirst () == 0)
	{
		Etikett.Mode = _tstoi (sys_par.sys_par_wrt);
	}

// 160-iger schreiben

	if (Write160 && Write160Lib == NULL)
	{
		CString bws;
		BOOL ret = bws.GetEnvironmentVariable (_T("bws"));
		CString W160Dll;
		if (ret)
		{
			W160Dll.Format (_T("%s\\bin\\bild160dll.dll"), bws.GetBuffer ());
		}
		else
		{
			W160Dll = _T("bild160dll.dll");
		}
		Write160Lib = LoadLibrary (W160Dll.GetBuffer ());
		if (Write160Lib != NULL && dbild160 == NULL)
		{
			dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
			dOpenDbase = (BOOL (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "OpenDbase");
			if (dOpenDbase != NULL)
			{
				(*dOpenDbase) ("bws");
			}
		}
	}
	else if (dbild160 == NULL)
	{
		dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
	}

	if (HideButtons)
	{
		ButtonControls.SetVisible (FALSE);
		RightListSpace = 15;
	}
	else
	{
		ButtonControls.SetVisible (TRUE);
		RightListSpace = 125;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	m_GrounPrice.nID = IDC_GROUND_PRICE;
	m_GrounPrice.SetWindowText (_T("Zusatzpreise"));
	m_GrounPrice.SetBkColor (GetSysColor (COLOR_3DFACE));

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_A,EDIT,       (double *) &A_bas.a_bas.a, VDOUBLE, 13, 0));
    Form.Add (new CUniFormField (&m_A_bz1,EDIT,   (char *) A_bas.a_bas.a_bz1, VCHAR));
    Form.Add (new CUniFormField (&m_A_bz2,EDIT,   (char *) A_bas.a_bas.a_bz2, VCHAR));

	A_pr.sqlin ((double *) &A_pr.a_pr.a,  SQLDOUBLE, 0);
	A_pr.sqlout ((short *)  &A_pr.a_pr.mdn_gr, SQLSHORT, 0);
	A_pr.sqlout ((short *)  &A_pr.a_pr.mdn, SQLSHORT, 0);
	A_pr.sqlout ((short *)  &A_pr.a_pr.fil_gr, SQLSHORT, 0);
	A_pr.sqlout ((short *)  &A_pr.a_pr.fil, SQLSHORT, 0);
	AprCursor = A_pr.sqlcursor (_T("select mdn_gr, mdn, fil_gr, fil from a_pr ")
		                       _T("where a = ? ")
		                       _T("order by mdn_gr, mdn, fil_gr, fil"));

	Gr_zuord.sqlout ((short *) &Gr_zuord.gr_zuord.gr, SQLSHORT, 0);
	Gr_zuord.sqlout ((char *) Gr_zuord.gr_zuord.gr_bz1, SQLCHAR, sizeof (Gr_zuord.gr_zuord.gr_bz1));
    MdnGrCursor = Gr_zuord.sqlcursor (_T("select gr, gr_bz1 from gr_zuord ")
 									  _T("where mdn = 0 ")
									  _T("and gr > 0 ")
								      _T("order by gr"));

	Mdn.sqlout ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);
	MdnAdr.sqlout ((char *) MdnAdr.adr.adr_krz, SQLCHAR, sizeof (MdnAdr.adr.adr_krz));
    MdnCursor = Mdn.sqlcursor (_T("select mdn.mdn, adr.adr_krz from mdn, adr ")
		                       _T("where mdn.mdn > 0 ")
							   _T("and adr.adr = mdn.adr ")
							   _T("order by mdn.mdn"));
/*
	Fil.sqlin ((short *) &Fil.fil.mdn, SQLSHORT, 0);
	Fil.sqlout ((short *) &Fil.fil.fil, SQLSHORT, 0);
	FilAdr.sqlout ((char *) &FilAdr.adr.adr_krz, SQLCHAR, sizeof (FilAdr.adr.adr_krz));
    FilCursor = Fil.sqlcursor (_T("select fil.fil, adr.adr_krz from fil, adr ")
		                       _T("where fil.mdn > ? ")
							   _T("and fil.fil > 0 ")
							   _T("and adr.adr = fil.adr ")
							   _T("order by fil.fil"));
*/

	m_List.Prepare ();

	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_List.SetImageList (&image, LVSIL_SMALL);   
	}

	FillList = m_List;
	FillList.SetStyle (LVS_REPORT);
	if (m_List.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	m_List.SetListMode (m_List.ListMode);
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Mandantengruppe"), 1, m_List.CompanyWidth, LVCFMT_LEFT);
	FillList.SetCol (_T("Mandant"), 2, m_List.CompanyWidth, LVCFMT_LEFT);
	FillList.SetCol (_T("Filialgruppe"), 3, m_List.CompanyWidth, LVCFMT_LEFT);
	FillList.SetCol (_T("Filiale"), 4, m_List.CompanyWidth, LVCFMT_LEFT);
	FillList.SetCol (_T("EK"), 5, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("VK"), 6, 100, LVCFMT_RIGHT);
	FillList.SetCol (_T("Sortiment"), 7, 100, LVCFMT_CENTER);
	FillList.SetCol (_T("Aktion"), 8, 100, LVCFMT_CENTER);
	FillList.SetCol (_T("Aktiv"), 9, 100, LVCFMT_CENTER);
	m_List.ColType.Add (new CColType (7, m_List.CheckBox));
	m_List.ColType.Add (new CColType (8, m_List.CheckBox));
	m_List.ColType.Add (new CColType (9, m_List.CheckBox));
	FillList.SetCol (_T(""), 10, 0);
	FillList.SetCol (_T(""), 11, 0);
	FillList.SetCol (_T(""), 12, 0);

	m_List.AddListChangeHandler (this);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	AGrid.Create (this, 1, 2);
    AGrid.SetBorder (0, 0);
    AGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 0, 0, 1, 1);
	AGrid.Add (c_A);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);

	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_GrundPreis = new CCtrlInfo (&GrundPreis, DOCKRIGHT, 1, 2, 1); 
	CtrlGrid.Add (c_GrundPreis);

	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_LA_bz1  = new CCtrlInfo (&m_LA_bz1, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LA_bz1);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 1, 2, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 1, 3, 3, 1); 
	CtrlGrid.Add (c_A_bz2);
	CCtrlInfo *c_GrounPrice  = new CCtrlInfo (&m_GrounPrice, DOCKRIGHT, 0, 1, 1); 
	CtrlGrid.Add (c_GrounPrice);
	CCtrlInfo *c_List  = new CCtrlInfo (&m_List, 0, 5, DOCKRIGHT, DOCKBOTTOM); 
//	c_List->rightspace = 125;
	c_List->rightspace = RightListSpace;
	CtrlGrid.Add (c_List);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	FillMdnGrCombo ();
	FillMdnCombo ();

	CtrlGrid.Display ();
	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	if (PersName.Trim () != "")
	{
		_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
		if (Sys_ben.dbreadfirstpers_nam () == 0)
		{
			if (Sys_ben.sys_ben.mdn != 0)
			{
				Mdn.mdn.mdn = Sys_ben.sys_ben.mdn;
				m_Mdn.SetReadOnly ();
				m_Mdn.ModifyStyle (WS_TABSTOP, 0);
			}
		}
	}
	Form.Show ();
	ReadMdn ();
	EnableHeadControls (TRUE);
	m_Mdn.SetFocus ();
	return FALSE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CArtPrPage::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CArtPrPage::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CArtPrPage::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CArtPrPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CArtPrPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_List.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_List &&
					GetFocus ()->GetParent () != &m_List )
				{

					break;
			    }
				m_List.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F11)
			{
				ShowGrundPreis ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnAchoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_A)
				{
					OnAchoice ();
					return TRUE;
				}
				m_List.OnKey9 ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CArtPrPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_A)
	{
		if (!Read ())
		{
			m_A.SetFocus ();
			return FALSE;
		}
	}

	if (Control != &m_List.ListEdit &&
		Control != &m_List.ListComboBox &&
		Control != &m_List.SearchListCtrl.Edit &&
		Control != &m_List.ListCheckBox &&
		Control != &m_List.ListDate)
{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CArtPrPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_List &&
		Control->GetParent ()!= &m_List )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_List.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}


BOOL CArtPrPage::ReadMdn ()
{
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Mdn.mdn.mdn == 0)
	{
		strcpy (MdnAdr.adr.adr_krz, "Zentrale");
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return TRUE;
	}
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}


BOOL CArtPrPage::read ()
{
	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}
	return Read ();

}

BOOL CArtPrPage::Read ()
{
	if (ModalChoice)
	{
		CString cA;
		m_A.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnAchoice ();
			SearchA = "";
			if (!AChoiceStat)
			{
				m_A.SetFocus ();
				m_A.SetSel (0, -1);
				return FALSE;
			}
		}
	}

	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	Form.Get ();
	if (A_bas.dbreadfirst () == 0)
	{
	    EnableHeadControls (FALSE);
		if (GrundPreis.IsWindowVisible ())
		{
			GrundPreis.Show (Mdn.mdn.mdn, 0, A_bas.a_bas.a, A_bas.a_bas.a_typ, A_bas.a_bas.me_einh);
		}
		ReadList ();
		Form.Show ();
		m_A.SetFocus ();
		m_A.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Artikel %.0lf nicht gefunden"),A_bas.a_bas.a);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);

		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		Form.Show ();
		m_A.SetFocus ();
		m_A.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CArtPrPage::ReadList ()
{
	m_List.StopEnter ();
	m_List.DeleteAllItems ();
	m_List.vSelect.clear ();
	m_List.ListRows.Init ();
	DestroyRows (DbRows);
	int i = 0;
	m_List.m_Mdn = Mdn.mdn.mdn;
	if (m_List.m_Mdn != 0)
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, 0);
		m_List.SetColumnWidth (m_List.PosMdn, 0);
	}
	else
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, m_List.CompanyWidth);
		m_List.SetColumnWidth (m_List.PosMdn, m_List.CompanyWidth);
	}
	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	A_pr.a_pr.a   = A_bas.a_bas.a;
	A_pr.sqlopen (AprCursor);
	while (A_pr.sqlfetch (AprCursor) == 0)
	{
		if (m_List.m_Mdn != 0 && A_pr.a_pr.mdn != m_List.m_Mdn)
		{
			continue;
		}
		A_pr.dbreadfirst ();
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		Mdn.mdn.mdn = A_pr.a_pr.mdn;
		if (Mdn.dbreadfirst () == 0)
		{
			MdnAdr.adr.adr = Mdn.mdn.adr;
			MdnAdr.dbreadfirst ();
		}
		Fil.fil.mdn = A_pr.a_pr.mdn;
		Fil.fil.fil = A_pr.a_pr.fil;
		if (Fil.dbreadfirst () == 0)
		{
			FilAdr.adr.adr = Fil.fil.adr;
			FilAdr.dbreadfirst ();
		}
		FillList.InsertItem (i, 0);
		CString cMdnGr;
		cMdnGr.Format (_T("%hd"), A_pr.a_pr.mdn_gr);
		FillList.SetItemText (cMdnGr.GetBuffer (), i, m_List.PosMdnGr);

		CString cMdn;
		if (A_pr.a_pr.mdn == 0)
		{
			if (m_List.ListMode == CArtPrListCtrl::Large)
			{
				cMdn.Format (_T("0  Zentrale"));
			}
			else
			{
				cMdn.Format (_T("0"));
			}
		}
		else
		{
			if (m_List.ListMode == CArtPrListCtrl::Large)
			{
				cMdn.Format (_T("%hd  %s"), A_pr.a_pr.mdn,
							MdnAdr.adr.adr_krz);
			}
			else
			{
				cMdn.Format (_T("%hd"), A_pr.a_pr.mdn);
			}
		}
		FillList.SetItemText (cMdn.GetBuffer (), i, m_List.PosMdn);

		CString cFilGr;
		cFilGr.Format (_T("%hd"), A_pr.a_pr.fil_gr);
		FillList.SetItemText (cFilGr.GetBuffer (), i, m_List.PosFilGr);

		CString cFil;
		if (A_pr.a_pr.fil == 0)
		{
			if (m_List.ListMode == CArtPrListCtrl::Large)
			{
				cFil.Format (_T("0  Mandant"));
			}
			else
			{
				cFil.Format (_T("0"));
			}
		}
		else
		{
			if (m_List.ListMode == CArtPrListCtrl::Large)
			{
				cFil.Format (_T("%hd  %s"), A_pr.a_pr.fil,
							FilAdr.adr.adr_krz);
			}
			else
			{
				cFil.Format (_T("%hd"), A_pr.a_pr.fil);
			}
		}
		FillList.SetItemText (cFil.GetBuffer (), i, m_List.PosFil);

		CString PrEk;
		if (DB_CLASS::IsDoublenull (A_pr.a_pr.pr_ek_euro))
		{
			A_pr.a_pr.pr_ek_euro = 0.0;
		}
		m_List.DoubleToString (A_pr.a_pr.pr_ek_euro, PrEk, 2);
		FillList.SetItemText (PrEk.GetBuffer (), i, m_List.PosPrEk);

		CString PrVk;
		if (DB_CLASS::IsDoublenull (A_pr.a_pr.pr_vk_euro))
		{
			A_pr.a_pr.pr_vk_euro = 0.0;
		}
		m_List.DoubleToString (A_pr.a_pr.pr_vk_euro, PrVk, 2);
		FillList.SetItemText (PrVk.GetBuffer (), i, m_List.PosPrVk);

		CString Smt = _T("X");
        if (A_pr.a_pr.pr_ek_euro == -1.0 && A_pr.a_pr.pr_vk_euro == 0.0)
		{
			Smt = _T("");
		}
		FillList.SetItemText (Smt.GetBuffer (), i, m_List.PosSmt);

		CString PrVk1;
		m_List.DoubleToString (A_pr.a_pr.pr_vk1, PrVk1, 2);
		FillList.SetItemText (PrVk1.GetBuffer (), i, m_List.PosPrVk1);
		CString PrVk2;
		m_List.DoubleToString (A_pr.a_pr.pr_vk2, PrVk2, 2);
		FillList.SetItemText (PrVk2.GetBuffer (), i, m_List.PosPrVk2);
		CString PrVk3;
		m_List.DoubleToString (A_pr.a_pr.pr_vk3, PrVk3, 2);
		FillList.SetItemText (PrVk3.GetBuffer (), i, m_List.PosPrVk3);

		CString Aktion = _T("X");
        if (A_pr.a_pr.akt == -1)
		{
			Aktion = _T("");
		}
		FillList.SetItemText (Aktion.GetBuffer (), i, m_List.PosAkt);

		CString Active = _T("X");
        if ((char) A_pr.a_pr.lad_akv[0] != '1')
		{
			Active = _T("");
		}

		FillList.SetItemText (Active.GetBuffer (), i, m_List.PosActive);
		CAPreise *a_pr = new CAPreise (PrEk, PrVk, A_pr.a_pr);
		DbRows.Add (a_pr);
		m_List.ListRows.Add (a_pr);
		i ++;
	}
	Mdn.mdn.mdn = m_List.m_Mdn;
	return TRUE;
}

BOOL CArtPrPage::IsChanged (CAPreise *pApr, CAPreise *old_a_pr)
{
	DbRows.FirstPosition ();
	CAPreise *a_pr;
	while ((a_pr = (CAPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&A_pr.a_pr, &pApr->a_pr, sizeof (A_PR));
		if (A_pr == a_pr->a_pr) break;
	} 	
	if (a_pr == NULL)
	{
		old_a_pr->a_pr.pr_ek_euro = 0.0;
		old_a_pr->a_pr.pr_vk_euro = 0.0;
		return TRUE;
	}
	memcpy (&old_a_pr->a_pr,  &a_pr->a_pr, sizeof (A_PR));
	if (pApr->cEk != a_pr->cEk) return TRUE;
	if (pApr->cVk != a_pr->cVk) return TRUE;
	if (pApr->a_pr.pr_vk1 != a_pr->a_pr.pr_vk1) return TRUE;
	if (pApr->a_pr.pr_vk2 != a_pr->a_pr.pr_vk2) return TRUE;
	if (pApr->a_pr.pr_vk3 != a_pr->a_pr.pr_vk3) return TRUE;
	return FALSE;
}

BOOL CArtPrPage::InList (APR_CLASS& A_pr)
{
	ListRows.FirstPosition ();
	CAPreise *a_pr;
	while ((a_pr = (CAPreise *) ListRows.GetNext ()) != NULL)
	{
		if (A_pr == a_pr->a_pr) return TRUE;
	}
    return FALSE;
}

void CArtPrPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CAPreise *a_pr;
	while ((a_pr = (CAPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&A_pr.a_pr, &a_pr->a_pr, sizeof (A_PR));
		if (!InList (A_pr))
		{
			A_pr.dbdelete ();
//			PgrProt.Write (1);
		}
	}
}

BOOL CArtPrPage::Write ()
{
	extern short sql_mode;
	short sql_s;
	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}

// Pr�fen auf doppelte Eintr�ge 

	m_List.StopEnter ();
//	m_List.TestSmt ();
	int count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		if (!m_List.TestAprIndexM (i))
		{
			MessageBox (_T("Die Daten k�nnen nicht gespeichert werden!\n")
				        _T("Es sind mindestens 2 Eintr�ge auf gleicher Unternehmesebene vorhanden"),
						NULL,
						MB_OK | MB_ICONERROR);
			return FALSE;
		}
	}

	sql_s = sql_mode;
	sql_mode = 1;
	A_pr.beginwork ();
	m_List.StopEnter ();
	count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 A_PR *a_pr = new A_PR;
		 memcpy (a_pr, &a_pr_null, sizeof (A_PR));
         CString Text;
		 Text = m_List.GetItemText (i, m_List.PosMdnGr);
		 a_pr->mdn_gr = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosMdn);
		 a_pr->mdn = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosFilGr);
		 a_pr->fil_gr = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosFil);
		 a_pr->fil = atoi (Text);
     
     	 CString PrEk =  m_List.GetItemText (i, m_List.PosPrEk);
		 a_pr->pr_ek_euro = CStrFuncs::StrToDouble (PrEk);
		 CString PrVk =  m_List.GetItemText (i, m_List.PosPrVk);
		 a_pr->pr_vk_euro = CStrFuncs::StrToDouble (PrVk);

		 CString cSmt = m_List.GetItemText (i, m_List.PosSmt);
		 if (a_pr->mdn_gr != 0 || a_pr->mdn != 0 ||
			 a_pr->fil_gr != 0 || a_pr->fil != 0)
		 {
			if (cSmt.Trim () == _T(""))
			{
			    a_pr->pr_ek_euro = -1.0;
				a_pr->pr_vk_euro = 0.0;
				PrEk = "-1,00";
				PrVk = "0,00";
			}
		 }
		 else
		 {
			 if ((a_pr->pr_ek_euro == -1.0) && (a_pr->pr_vk_euro == 0.0))
			 {
			    a_pr->pr_ek_euro = 0.0;
				PrEk = "0,00";
			 }
		 }

		 CString PrVk1 =  m_List.GetItemText (i, m_List.PosPrVk1);
		 a_pr->pr_vk1 = CStrFuncs::StrToDouble (PrVk1);
		 CString PrVk2 =  m_List.GetItemText (i, m_List.PosPrVk2);
		 a_pr->pr_vk2 = CStrFuncs::StrToDouble (PrVk2);
		 CString PrVk3 =  m_List.GetItemText (i, m_List.PosPrVk3);
		 a_pr->pr_vk3 = CStrFuncs::StrToDouble (PrVk3);

		 a_pr->pr_ek = a_pr->pr_ek_euro;
		 a_pr->pr_vk = a_pr->pr_vk_euro;
		 a_pr->a = A_bas.a_bas.a; 

		 if (!TestDecValues (a_pr)) 
		 {
			 MessageBox (_T("Maximalwert f�r Preisfeld �berschritten"));
			 return FALSE;
		 }

		 CAPreise *pr = new CAPreise (PrEk, PrVk, *a_pr);
		 if (a_pr->pr_ek_euro != 0.0 || 
			 a_pr->pr_vk_euro != 0.0 || a_pr->akt != -1)
		 {
				ListRows.Add (pr);
		 }
		 delete a_pr;
	}

	DeleteDbRows ();

	Etikett.Clear ();
	ListRows.FirstPosition ();
	CAPreise *a_pr;
	while ((a_pr = (CAPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&A_pr.a_pr, &a_pr->a_pr, sizeof (A_PR));
		CString Date;
		CStrFuncs::SysDate (Date);
		A_pr.ToDbDate (Date, &A_pr.a_pr.bearb);
		A_pr.a_pr.akt = -1;
		A_pr.dbreadfirst ();
		if ((A_pr.a_pr.akt == -1) && (a_pr->a_pr.pr_ek_euro == 0.0) &&
			(a_pr->a_pr.pr_vk_euro == 0.0))
		{
			continue;
		}
		A_pr.a_pr.key_typ_dec13 = A_pr.a_pr.a;
		A_pr.a_pr.key_typ_sint = A_pr.a_pr.mdn;
		A_pr.a_pr.pr_ek_euro = a_pr->a_pr.pr_ek_euro;
		A_pr.a_pr.pr_ek = a_pr->a_pr.pr_ek;
		A_pr.a_pr.pr_vk_euro = a_pr->a_pr.pr_vk_euro;
		A_pr.a_pr.pr_vk = a_pr->a_pr.pr_vk;
		A_PR AprWrite;
		memcpy (&AprWrite, &A_pr.a_pr, sizeof (A_PR));
		CAPreise old_a_pr;
		if (IsChanged (a_pr, &old_a_pr))
		{
			memcpy (&A_pr.a_pr, &AprWrite, sizeof (A_PR));
			A_pr.a_pr.pr_vk1 = a_pr->a_pr.pr_vk1;
			A_pr.a_pr.pr_vk2 = a_pr->a_pr.pr_vk2;
			A_pr.a_pr.pr_vk3 = a_pr->a_pr.pr_vk3;
			A_pr.dbupdate ();
			PrProt.Write (old_a_pr.a_pr.pr_ek_euro, old_a_pr.a_pr.pr_vk_euro);
			if (Write160 && dbild160 != NULL)
			{
				CStringA Command;
				if (A_pr.a_pr.pr_ek_euro == -1.0)
				{
					Command.Format ("bild160 L %.0lf %hd %hd %hd %hd",
					             A_bas.a_bas.a,
								 A_pr.a_pr.mdn_gr,
								 A_pr.a_pr.mdn,
								 A_pr.a_pr.fil_gr,
								 A_pr.a_pr.fil); 
				}
				else
				{
					Command.Format ("bild160 B %.0lf %hd %hd %hd %hd",
					             A_bas.a_bas.a,
								 A_pr.a_pr.mdn_gr,
								 A_pr.a_pr.mdn,
								 A_pr.a_pr.fil_gr,
								 A_pr.a_pr.fil); 
				}

				(*dbild160) (Command.GetBuffer ());
			}
			Etikett.WritePr (A_pr.a_pr.mdn, A_pr.a_pr.fil, A_pr.a_pr.pr_vk_euro);
		}
	}
	EnableHeadControls (TRUE);
	m_A.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	Etikett.TestPrint ();
	A_pr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

BOOL CArtPrPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Alle Eintr�ge l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	A_pr.beginwork ();
	m_List.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	m_List.DeleteAllItems ();

	EnableHeadControls (TRUE);
	m_A.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	A_pr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

void CArtPrPage::OnAchoice ()
{
    AChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceA (this);
	    Choice->IsModal = ModalChoice;
#ifndef ARTIKEL
	    Choice->HideEnter = FALSE;
#endif
	    Choice->HideFilter = FALSE;
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->SearchText = SearchA;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

/*
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		cy -= 100;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
*/
		Choice->MoveWindow (&rect);
		Choice->SetFocus ();

		return;
	}
    if (Choice->GetState ())
    {
		  CABasList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_A.EnableWindow (TRUE);
		  m_A.SetSel (0, -1, TRUE);
		  m_A.SetFocus ();
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
	 	  AChoiceStat = FALSE;	
	}
}

void CArtPrPage::OnASelected ()
{
	if (Choice == NULL) return;
    CABasList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    A_bas.a_bas.a = abl->a;
    if (A_bas.dbreadfirst () == 0)
    {
		m_A.EnableWindow (TRUE);
		m_A.SetFocus ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	if (CloseChoice)
	{
		OnACanceled (); 
	}
    Form.Show ();
	if (Choice->FocusBack)
	{
		Read ();
		m_List.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CArtPrPage::OnACanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CArtPrPage::StepBack ()
{
	if (m_A.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		m_List.StopEnter ();
		EnableHeadControls (TRUE);
		m_A.SetFocus ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
		m_List.DeleteAllItems ();
	}
	return TRUE;
}

void CArtPrPage::OnCancel ()
{
	StepBack ();
}

void CArtPrPage::OnSave ()
{
	Write ();
}

void CArtPrPage::FillMdnGrCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0"));
	Values.Add (Value);
    Mdn.sqlopen (MdnGrCursor);
	while (Mdn.sqlfetch (MdnGrCursor) == 0)
	{
		Gr_zuord.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr,
			                          Gr_zuord.gr_zuord.gr_bz1);
		Values.Add (Value);
	}
	m_List.FillMdnGrCombo (Values);
}

void CArtPrPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}


void CArtPrPage::FillMdnCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Zentrale"));
	Values.Add (Value);
    Mdn.sqlopen (MdnCursor);
	while (Mdn.sqlfetch (MdnCursor) == 0)
	{
		Mdn.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Mdn.mdn.mdn,
			                          MdnAdr.adr.adr_krz);
		Values.Add (Value);
	}
	m_List.FillMdnCombo (Values);
}


void CArtPrPage::FillFilCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Mandant"));
	Values.Add (Value);
    Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		Fil.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Fil.fil.fil,
			                          FilAdr.adr.adr_krz);
		Values.Add (Value);
	}
	m_List.FillFilCombo (Values);
}

void CArtPrPage::OnDelete ()
{
	CAPreise *a_pr = (CAPreise *) DbRows.Get (m_List.EditRow);
	if (a_pr != NULL && a_pr->a_pr.akt != -1)
	{
		MessageBox (_T("Die Zeile kann nicht gel�scht werden.\n")
			        _T("Eine Aktion ist aktiv"), _T("Fehler"), MB_OK | MB_ICONERROR);
		return;
	}
	m_List.DeleteRow ();
}

void CArtPrPage::OnInsert ()
{
	m_List.InsertRow ();
}

/*
BOOL CArtPrPage::Print ()
{
	CProcess print;
	CString Command = "70001 11112";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}
*/

BOOL CArtPrPage::Print ()
{

// Achtung !! Hier mu� noch der richtige Ablauf fp�r den Druck eingesetzt werden.

	CProcess print;
	CString Command;

	if (!m_A.IsWindowEnabled ())
	{
		Form.Get ();
		LPTSTR tmp = getenv ("TMPPATH");
		CString dName;
		FILE *fp;
		if (tmp != NULL)
		{
			dName.Format ("%s\\11200.llf", tmp);
		}
		else
		{
			dName = "11200.llf";
		}
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			fprintf (fp, "NAME 11200\n");
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
			fprintf (fp, "a %.0lf %.0lf\n", A_bas.a_bas.a,
			                        A_bas.a_bas.a);
			fclose (fp);
			Command.Format ("dr70001 -name 11200 -datei %s", dName.GetBuffer ());
		}
		else
		{
			Command = "dr70001 -name 11200";
		}
	}
	else
	{
		Command = "dr70001 -name 11200";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CArtPrPage::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_List.StartPauseEnter ();
}

void CArtPrPage::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_List.EndPauseEnter ();
}

void CArtPrPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Enable (enable);
	PosControls.Enable (!enable);
}

void CArtPrPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CAPreise *ipr;
	while ((ipr = (CAPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

void CArtPrPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_List.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_List.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_List.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_List.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CArtPrPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_List.ListEdit ||
        Control == &m_List.ListComboBox ||
		Control == &m_List.SearchListCtrl.Edit)	
	{
		ListCopy ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CArtPrPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CArtPrPage::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_List.ListEdit.m_hWnd) ||
		IsWindow (m_List.ListComboBox) ||
		IsWindow (m_List.SearchListCtrl.Edit))
	{
		m_List.StopEnter ();
		m_List.StartEnter (m_List.EditCol, m_List.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
//		for (int i = 0; i < MAXLISTROWS && i < m_List.GetItemCount (); i ++)
		for (int i = 0; i < m_List.GetItemCount (); i ++)
		{
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = "";
			int cols = m_List.GetHeaderCtrl ()->GetItemCount ();
			for (int j = 0; j < cols; j ++)
			{
				if (Row != "")
				{
					Row += Separator;
				}
				CString Field = m_List.GetItemText (i, j);
				Field.TrimRight ();
				Row += Field; 
			}
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

BOOL CArtPrPage::TestDecValues (A_PR *a_pr)
{
	if (a_pr->pr_ek_euro > 9999.9999 ||
		a_pr->pr_vk_euro < -9999.9999)
	{
		return FALSE;
	}

	if (a_pr->pr_ek_euro > 9999.99 ||
		a_pr->pr_vk_euro < -9999.99)
	{
		return FALSE;
	}
    
	return TRUE;
}

void CArtPrPage::OnGroundPrice ()
{
	CString PrVk1;
	CString PrVk2;
	CString PrVk3;
	CGrounPrice dlg;
	if (m_List.GetItemCount () == 0)
	{
		return;
	}
	int row = m_List.EditRow;
	if (row < 0)
	{
		return;
	}
	m_List.GetEnter ();
	CAPreise *a_pr = (CAPreise *) m_List.ListRows.Get(row);
	dlg.Price1 = CStrFuncs::StrToDouble (m_List.GetItemText (m_List.EditRow, m_List.PosPrVk1)); 
	dlg.Price2 = CStrFuncs::StrToDouble (m_List.GetItemText (m_List.EditRow, m_List.PosPrVk2)); 
	dlg.Price3 = CStrFuncs::StrToDouble (m_List.GetItemText (m_List.EditRow, m_List.PosPrVk3)); 
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		PrVk1.Format ("%.2lf", dlg.Price1); 
		PrVk2.Format ("%.2lf", dlg.Price2); 
		PrVk3.Format ("%.2lf", dlg.Price3); 
		m_List.SetItemText (m_List.EditRow, m_List.PosPrVk1, PrVk1.GetBuffer ());
		m_List.SetItemText (m_List.EditRow, m_List.PosPrVk2, PrVk2.GetBuffer ());
		m_List.SetItemText (m_List.EditRow, m_List.PosPrVk3, PrVk3.GetBuffer ());
	}
}

void CArtPrPage::SetListMode ()
{
	if (m_List.ListMode == CArtPrListCtrl::Compact)
	{
		m_List.SetListMode (CArtPrListCtrl::Large);
	}
	else
	{
		m_List.SetListMode (CArtPrListCtrl::Compact);
	}

	if (m_List.m_Mdn == 0)
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, m_List.CompanyWidth);
		m_List.SetColumnWidth (m_List.PosMdn, m_List.CompanyWidth);
	}
	m_List.SetColumnWidth (m_List.PosFilGr, m_List.CompanyWidth);
	m_List.SetColumnWidth (m_List.PosFil, m_List.CompanyWidth);
	m_List.Invalidate ();
	if (!m_A.IsWindowEnabled ())
	{
		ReadList ();
	}

	CView *parent = (CView *) GetParent ()->GetParent ();
	CDocument *doc = parent->GetDocument ();
	doc->UpdateAllViews (parent);
}

BOOL CArtPrPage::IsCompactMode ()
{
	if (m_List.ListMode == CArtPrListCtrl::Compact)
	{
		return TRUE;
	}
	return FALSE;
}


void CArtPrPage::Save ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	File.Open (Path.GetBuffer (), CFile::modeCreate | CFile::modeWrite);
	CArchive archive(&File, CArchive::store);

	try
	{
			archive.Write (&DlgBkColor, sizeof (DlgBkColor));
			archive.Write (&ListBkColor, sizeof (ListBkColor));
			archive.Write (&FlatLayout, sizeof (FlatLayout));
			archive.Write (&HideOK, sizeof (HideOK));
			archive.Write (&m_List.ListMode, sizeof (&m_List.ListMode)); 
	}
	catch (...) 
	{
		return;
	}


	archive.Close ();
	File.Close ();
}

void CArtPrPage::Load ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}

	if (!File.Open (Path.GetBuffer (), CFile::modeRead))
	{
		return;
	}
	CArchive archive(&File, CArchive::load);
	try
	{
			archive.Read (&DlgBkColor, sizeof (DlgBkColor));
			archive.Read (&ListBkColor, sizeof (ListBkColor));
			archive.Read (&FlatLayout, sizeof (FlatLayout));
			archive.Read (&HideOK, sizeof (HideOK));
			archive.Read (&m_List.ListMode, sizeof (&m_List.ListMode)); 
	}
	catch (...) 
	{
		return;
	}

	archive.Close ();
	File.Close ();
}

void CArtPrPage::OnRecChange()
{
	A_bas.a_bas.a = a_bas.a;
	EnableHeadControls (TRUE);
	m_A.SetFocus ();
	Form.Show ();
	Read ();
//	PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
}

void CArtPrPage::SetGrundPreis ()
{
	if (m_A.IsWindowEnabled ()) return;
	if (GrundPreis.IsWindowVisible ())
	{
		GrundPreis.Show (A_pr.a_pr.mdn, A_pr.a_pr.fil, A_bas.a_bas.a, A_bas.a_bas.a_typ, A_bas.a_bas.me_einh);
	}
}

void CArtPrPage::ShowGrundPreis ()
{
//	if (m_A.IsWindowEnabled ()) return;

	if (!GrundPreis.IsWindowVisible ())
	{
		GrundPreis.SetVisible ();
		GrundPreis.Show (Mdn.mdn.mdn, 0, A_bas.a_bas.a, A_bas.a_bas.a_typ, A_bas.a_bas.me_einh);
	}
	else
	{
		GrundPreis.SetVisible (FALSE);
	}
	CView *parent = (CView *) GetParent ()->GetParent ();
	CDocument *doc = parent->GetDocument ();
	doc->UpdateAllViews (parent);
}

void CArtPrPage::RowChanged (int NewRow)
{
    CString Text;
	Text = m_List.GetItemText (NewRow, m_List.PosMdn);
	A_pr.a_pr.mdn = _tstoi (Text);
	Text = m_List.GetItemText (NewRow, m_List.PosFil);
	A_pr.a_pr.fil = _tstoi (Text);
	SetGrundPreis ();
}

void CArtPrPage::ColChanged (int Row, int Col)
{
}

