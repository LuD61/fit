#ifndef _HWG_DEF
#define _WHG_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct HWG {
   short          hwg;
   TCHAR          hwg_bz1[25];
   TCHAR          hwg_bz2[25];
   TCHAR          sp_kz[2];
   long           we_kto;
   long           erl_kto;
   TCHAR          durch_vk[2];
   TCHAR          durch_sk[2];
   TCHAR          durch_fil[2];
   TCHAR          durch_sw[2];
   TCHAR          best_auto[2];
   double         sp_vk;
   double         sp_sk;
   double         sp_fil;
   double         sw;
   short          me_einh;
   short          mwst;
   short          teil_smt;
   short          bearb_lad;
   short          bearb_fil;
   short          bearb_sk;
   TCHAR          sam_ean[2];
   TCHAR          smt[2];
   TCHAR          kost_kz[3];
   double         pers_rab;
   DATE_STRUCT    akv;
   DATE_STRUCT    bearb;
   TCHAR          pers_nam[9];
   short          delstatus;
   long           erl_kto_1;
   long           erl_kto_2;
   long           erl_kto_3;
   long           we_kto_1;
   long           we_kto_2;
   long           we_kto_3;
};
extern struct HWG hwg, hwg_null;

#line 8 "hwg.rh"

class HWG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               HWG hwg;
               HWG_CLASS () : DB_CLASS ()
               {
               }
};
#endif
