#ifndef _PRDK_K_DEF
#define _PRDK_K_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct PRDK_K {
   short          mdn;
   double         a;
   TCHAR          rez[9];
   TCHAR          rez_bz[73];
   TCHAR          kalk_stat[2];
   TCHAR          grbrt[2];
   TCHAR          leits[10];
   double         chg_gew;
   double         varb_gew;
   double         zut_gew;
   double         huel_gew;
   double         tr_sw;
   double         is_befe_abs;
   double         is_befe_rel;
   double         is_be_rel;
   double         is_fett;
   double         is_fett_tol;
   double         is_fett_fe;
   double         is_f_h2o;
   double         varb_mat_o_b;
   double         varb_hk_teilk;
   double         varb_hk_vollk;
   double         zut_mat_o_b;
   double         zut_hk_teilk;
   double         zut_hk_vollk;
   double         huel_mat_o_b;
   double         huel_hk_teilk;
   double         huel_hk_vollk;
   double         rez_hk_teilk;
   double         rez_hk_vollk;
   DATE_STRUCT    dat;
   double         sw_kalk;
   double         bto_gew;
   double         nto_gew;
   TCHAR          bearb_weg_p[5];
   double         bto_mat_o_b;
   double         bto_hk_teilk;
   double         bto_hk_vollk;
   double         nto_mat_o_b;
   double         nto_hk_teilk;
   double         nto_hk_vollk;
   double         a_hk_teilk;
   double         a_hk_vollk;
   short          delstatus;
   double         rez_mat_o_b;
   double         nto_berech_gew;
   double         bearb_weg;
   TCHAR          bru[2];
   double         sw;
   short          variante;
   TCHAR          variante_bz[25];
   short          akv;
   TCHAR          prodphase[21];
   long           masch_nr1;
   long           masch_nr2;
   long           masch_nr3;
   long           masch_nr4;
   long           masch_nr5;
   long           masch_nr6;
   long           masch_nr7;
   long           masch_nr8;
   long           masch_nr9;
   long           masch_nr10;
   double         huel_wrt;
   double         vpk_wrt;
   double         huel_hk_wrt;
   double         vpk_hk_wrt;
   double         kost_gew;
   double         kost_mat_o_b;
   double         kost_hk_teilk;
   double         kost_hk_vollk;
   double         vpk_gew;
   double         vpk_mat_o_b;
   double         vpk_hk_teilk;
   double         vpk_hk_vollk;
   double         nvpk_mat_o_b;
   double         nvpk_hk_teilk;
   double         nvpk_hk_vollk;
   short          chg_anz_bzg;
   double         kalk_me;
   short          chg_anz_kalk;
   double         pers_wrt_bzg;
   double         pers_wrt_kalk;
   double         kutter_gew;
   short          chargierung;
   double         a_sk_teilk;
   double         a_sk_vollk;
   double         a_filek_teilk;
   double         a_filek_vollk;
   double         a_filvk_teilk;
   double         a_filvk_vollk;
   double         sk_wrt;
   double         filek_wrt;
   double         filvk_wrt;
   double         bearb_weg_sk;
   double         bearb_weg_fek;
   double         bearb_weg_fvk;
   long           manr;
   short          masch1_zeit;
   short          masch2_zeit;
   short          masch3_zeit;
   short          masch4_zeit;
   short          masch5_zeit;
   short          masch6_zeit;
   short          masch7_zeit;
   short          masch8_zeit;
   short          masch9_zeit;
   short          masch10_zeit;
   short          vorlaufzeit;
   double         varb_gew_nto;
   short          rework;
   long           anz_pers;
   short          prod_abt;
};
extern struct PRDK_K prdk_k, prdk_k_null;

#line 8 "prdk_k.rh"

class PRDK_K_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRDK_K prdk_k;  
               PRDK_K_CLASS () : DB_CLASS ()
               {
               }
};
#endif
