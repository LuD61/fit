#pragma once
#include "Pr_prot.h"
#include "ek_pr.h"

class CPrProt
{
public:
	CString PersName;
    CString Programm;

	EK_PR_CLASS *Ek_pr;
	PR_PROT_CLASS Pr_prot;

	CPrProt(void);
	CPrProt(CString& PersName, CString& Programm, EK_PR_CLASS *Ek_pr);
	~CPrProt(void);
	void Construct (CString& PersName, CString& Programm, EK_PR_CLASS *Ek_pr);
	void Write (double pr_ek_alt, double pr_vk_alt);
};
