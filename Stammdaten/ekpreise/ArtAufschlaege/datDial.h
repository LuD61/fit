#pragma once
#include "afxwin.h"


// CdatDial-Dialogfeld

class CdatDial : public CPropertyPage
{
	DECLARE_DYNAMIC(CdatDial)

public:
	CdatDial();
	virtual ~CdatDial();

// Dialogfelddaten
	enum { IDD = IDD_DATDIAL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnReturn (void);


	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edit1;
	CString v_edit1;
	afx_msg BOOL OnInitDialog();
	afx_msg void OnEnKillfocusEdit1();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

public:
	void putdatum(char *,int,int,int) ;
	void puttyp(int) ;
	int getretcode(void) ;
	char * getdatum(char *) ;
};
