#ifndef _REG_ETI_DEF
#define _REG_ETI_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct REG_ETI {
   double         a;
   short          anz;
   long           nr;
   short          reg;
   short          fil;
   short          mdn;
   DATE_STRUCT    dat;
   DATE_STRUCT    dat_akv;
   TCHAR          erf_kz[2];
   double         vk_pr;
};
extern struct REG_ETI reg_eti, reg_eti_null;

#line 8 "reg_eti.rh"

class REG_ETI_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               REG_ETI reg_eti;
               REG_ETI_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (REG_ETI&);  
};
#endif
