#include "stdafx.h"
#include "akt_krza.h"

void AKT_KRZA_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &akt_krz.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.mdn, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.fil, SQLSHORT, 0);
            sqlin ((double *)  &akt_krz.a, SQLDOUBLE, 0);
            sqlin ((DATE_STRUCT *)  &akt_krz.lad_akt_von, SQLDATE, 0);
    sqlout ((double *) &akt_krz.a,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &akt_krz.bel_akt_bis,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &akt_krz.bel_akt_von,SQLDATE,0);
    sqlout ((short *) &akt_krz.fil,SQLSHORT,0);
    sqlout ((short *) &akt_krz.fil_gr,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &akt_krz.lad_akt_bis,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &akt_krz.lad_akt_von,SQLDATE,0);
    sqlout ((TCHAR *) akt_krz.lad_akv_sa,SQLCHAR,2);
    sqlout ((TCHAR *) akt_krz.lief_akv_sa,SQLCHAR,2);
    sqlout ((short *) &akt_krz.mdn,SQLSHORT,0);
    sqlout ((short *) &akt_krz.mdn_gr,SQLSHORT,0);
    sqlout ((double *) &akt_krz.pr_ek_sa,SQLDOUBLE,0);
    sqlout ((double *) &akt_krz.pr_vk_sa,SQLDOUBLE,0);
    sqlout ((TCHAR *) akt_krz.zeit_bis,SQLCHAR,7);
    sqlout ((TCHAR *) akt_krz.zeit_von,SQLCHAR,7);
    sqlout ((short *) &akt_krz.zeit_von_n,SQLSHORT,0);
    sqlout ((short *) &akt_krz.zeit_bis_n,SQLSHORT,0);
    sqlout ((double *) &akt_krz.pr_vk1_sa,SQLDOUBLE,0);
    sqlout ((double *) &akt_krz.pr_vk2_sa,SQLDOUBLE,0);
    sqlout ((double *) &akt_krz.pr_vk3_sa,SQLDOUBLE,0);
    sqlout ((TCHAR *) akt_krz.dr_status,SQLCHAR,2);
    sqlout ((double *) &akt_krz.pr_ek_sa_euro,SQLDOUBLE,0);
    sqlout ((double *) &akt_krz.pr_vk_sa_euro,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select akt_krz.a,  ")
_T("akt_krz.bel_akt_bis,  akt_krz.bel_akt_von,  akt_krz.fil,  ")
_T("akt_krz.fil_gr,  akt_krz.lad_akt_bis,  akt_krz.lad_akt_von,  ")
_T("akt_krz.lad_akv_sa,  akt_krz.lief_akv_sa,  akt_krz.mdn,  ")
_T("akt_krz.mdn_gr,  akt_krz.pr_ek_sa,  akt_krz.pr_vk_sa,  ")
_T("akt_krz.zeit_bis,  akt_krz.zeit_von,  akt_krz.zeit_von_n,  ")
_T("akt_krz.zeit_bis_n,  akt_krz.pr_vk1_sa,  akt_krz.pr_vk2_sa,  ")
_T("akt_krz.pr_vk3_sa,  akt_krz.dr_status,  akt_krz.pr_ek_sa_euro,  ")
_T("akt_krz.pr_vk_sa_euro from akt_krz ")

#line 15 "akt_krza.rpp"
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ? ")
				  _T("and lad_akt_von = ? "));
    sqlin ((double *) &akt_krz.a,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &akt_krz.bel_akt_bis,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &akt_krz.bel_akt_von,SQLDATE,0);
    sqlin ((short *) &akt_krz.fil,SQLSHORT,0);
    sqlin ((short *) &akt_krz.fil_gr,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &akt_krz.lad_akt_bis,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &akt_krz.lad_akt_von,SQLDATE,0);
    sqlin ((TCHAR *) akt_krz.lad_akv_sa,SQLCHAR,2);
    sqlin ((TCHAR *) akt_krz.lief_akv_sa,SQLCHAR,2);
    sqlin ((short *) &akt_krz.mdn,SQLSHORT,0);
    sqlin ((short *) &akt_krz.mdn_gr,SQLSHORT,0);
    sqlin ((double *) &akt_krz.pr_ek_sa,SQLDOUBLE,0);
    sqlin ((double *) &akt_krz.pr_vk_sa,SQLDOUBLE,0);
    sqlin ((TCHAR *) akt_krz.zeit_bis,SQLCHAR,7);
    sqlin ((TCHAR *) akt_krz.zeit_von,SQLCHAR,7);
    sqlin ((short *) &akt_krz.zeit_von_n,SQLSHORT,0);
    sqlin ((short *) &akt_krz.zeit_bis_n,SQLSHORT,0);
    sqlin ((double *) &akt_krz.pr_vk1_sa,SQLDOUBLE,0);
    sqlin ((double *) &akt_krz.pr_vk2_sa,SQLDOUBLE,0);
    sqlin ((double *) &akt_krz.pr_vk3_sa,SQLDOUBLE,0);
    sqlin ((TCHAR *) akt_krz.dr_status,SQLCHAR,2);
    sqlin ((double *) &akt_krz.pr_ek_sa_euro,SQLDOUBLE,0);
    sqlin ((double *) &akt_krz.pr_vk_sa_euro,SQLDOUBLE,0);
            sqltext = _T("update akt_krz set akt_krz.a = ?,  ")
_T("akt_krz.bel_akt_bis = ?,  akt_krz.bel_akt_von = ?,  ")
_T("akt_krz.fil = ?,  akt_krz.fil_gr = ?,  akt_krz.lad_akt_bis = ?,  ")
_T("akt_krz.lad_akt_von = ?,  akt_krz.lad_akv_sa = ?,  ")
_T("akt_krz.lief_akv_sa = ?,  akt_krz.mdn = ?,  akt_krz.mdn_gr = ?,  ")
_T("akt_krz.pr_ek_sa = ?,  akt_krz.pr_vk_sa = ?,  akt_krz.zeit_bis = ?,  ")
_T("akt_krz.zeit_von = ?,  akt_krz.zeit_von_n = ?,  ")
_T("akt_krz.zeit_bis_n = ?,  akt_krz.pr_vk1_sa = ?,  ")
_T("akt_krz.pr_vk2_sa = ?,  akt_krz.pr_vk3_sa = ?,  ")
_T("akt_krz.dr_status = ?,  akt_krz.pr_ek_sa_euro = ?,  ")
_T("akt_krz.pr_vk_sa_euro = ? ")

#line 22 "akt_krza.rpp"
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ? ")
				  _T("and lad_akt_von = ? ");
            sqlin ((short *)   &akt_krz.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.mdn, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.fil, SQLSHORT, 0);
            sqlin ((double *)  &akt_krz.a, SQLDOUBLE, 0);
            sqlin ((DATE_STRUCT *)  &akt_krz.lad_akt_von, SQLDATE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &akt_krz.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.mdn, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.fil, SQLSHORT, 0);
            sqlin ((double *)  &akt_krz.a, SQLDOUBLE, 0);
            sqlin ((DATE_STRUCT *)  &akt_krz.lad_akt_von, SQLDATE, 0);
            test_upd_cursor = sqlcursor (_T("select a from akt_krz ")
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ? ")
				  _T("and lad_akt_von = ? "));
            sqlin ((short *)   &akt_krz.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.mdn, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.fil, SQLSHORT, 0);
            sqlin ((double *)  &akt_krz.a, SQLDOUBLE, 0);
            sqlin ((DATE_STRUCT *)  &akt_krz.lad_akt_von, SQLDATE, 0);
            del_cursor = sqlcursor (_T("delete from akt_krz ")
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ? ")
				  _T("and lad_akt_von = ? "));
    sqlin ((double *) &akt_krz.a,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &akt_krz.bel_akt_bis,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &akt_krz.bel_akt_von,SQLDATE,0);
    sqlin ((short *) &akt_krz.fil,SQLSHORT,0);
    sqlin ((short *) &akt_krz.fil_gr,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &akt_krz.lad_akt_bis,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &akt_krz.lad_akt_von,SQLDATE,0);
    sqlin ((TCHAR *) akt_krz.lad_akv_sa,SQLCHAR,2);
    sqlin ((TCHAR *) akt_krz.lief_akv_sa,SQLCHAR,2);
    sqlin ((short *) &akt_krz.mdn,SQLSHORT,0);
    sqlin ((short *) &akt_krz.mdn_gr,SQLSHORT,0);
    sqlin ((double *) &akt_krz.pr_ek_sa,SQLDOUBLE,0);
    sqlin ((double *) &akt_krz.pr_vk_sa,SQLDOUBLE,0);
    sqlin ((TCHAR *) akt_krz.zeit_bis,SQLCHAR,7);
    sqlin ((TCHAR *) akt_krz.zeit_von,SQLCHAR,7);
    sqlin ((short *) &akt_krz.zeit_von_n,SQLSHORT,0);
    sqlin ((short *) &akt_krz.zeit_bis_n,SQLSHORT,0);
    sqlin ((double *) &akt_krz.pr_vk1_sa,SQLDOUBLE,0);
    sqlin ((double *) &akt_krz.pr_vk2_sa,SQLDOUBLE,0);
    sqlin ((double *) &akt_krz.pr_vk3_sa,SQLDOUBLE,0);
    sqlin ((TCHAR *) akt_krz.dr_status,SQLCHAR,2);
    sqlin ((double *) &akt_krz.pr_ek_sa_euro,SQLDOUBLE,0);
    sqlin ((double *) &akt_krz.pr_vk_sa_euro,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into akt_krz (")
_T("a,  bel_akt_bis,  bel_akt_von,  fil,  fil_gr,  lad_akt_bis,  lad_akt_von,  ")
_T("lad_akv_sa,  lief_akv_sa,  mdn,  mdn_gr,  pr_ek_sa,  pr_vk_sa,  zeit_bis,  ")
_T("zeit_von,  zeit_von_n,  zeit_bis_n,  pr_vk1_sa,  pr_vk2_sa,  pr_vk3_sa,  ")
_T("dr_status,  pr_ek_sa_euro,  pr_vk_sa_euro) ")

#line 63 "akt_krza.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 65 "akt_krza.rpp"
            sqlin ((short *)   &akt_krz.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.mdn, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &akt_krz.fil, SQLSHORT, 0);
            sqlin ((double *)  &akt_krz.a, SQLDOUBLE, 0);
            sqlin ((DATE_STRUCT *)  &akt_krz.lad_akt_von, SQLDATE, 0);
    sqlout ((double *) &akt_krz.a,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &akt_krz.bel_akt_bis,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &akt_krz.bel_akt_von,SQLDATE,0);
    sqlout ((short *) &akt_krz.fil,SQLSHORT,0);
    sqlout ((short *) &akt_krz.fil_gr,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &akt_krz.lad_akt_bis,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &akt_krz.lad_akt_von,SQLDATE,0);
    sqlout ((TCHAR *) akt_krz.lad_akv_sa,SQLCHAR,2);
    sqlout ((TCHAR *) akt_krz.lief_akv_sa,SQLCHAR,2);
    sqlout ((short *) &akt_krz.mdn,SQLSHORT,0);
    sqlout ((short *) &akt_krz.mdn_gr,SQLSHORT,0);
    sqlout ((double *) &akt_krz.pr_ek_sa,SQLDOUBLE,0);
    sqlout ((double *) &akt_krz.pr_vk_sa,SQLDOUBLE,0);
    sqlout ((TCHAR *) akt_krz.zeit_bis,SQLCHAR,7);
    sqlout ((TCHAR *) akt_krz.zeit_von,SQLCHAR,7);
    sqlout ((short *) &akt_krz.zeit_von_n,SQLSHORT,0);
    sqlout ((short *) &akt_krz.zeit_bis_n,SQLSHORT,0);
    sqlout ((double *) &akt_krz.pr_vk1_sa,SQLDOUBLE,0);
    sqlout ((double *) &akt_krz.pr_vk2_sa,SQLDOUBLE,0);
    sqlout ((double *) &akt_krz.pr_vk3_sa,SQLDOUBLE,0);
    sqlout ((TCHAR *) akt_krz.dr_status,SQLCHAR,2);
    sqlout ((double *) &akt_krz.pr_ek_sa_euro,SQLDOUBLE,0);
    sqlout ((double *) &akt_krz.pr_vk_sa_euro,SQLDOUBLE,0);
            NewCursor = sqlcursor (_T("select akt_krz.a,  ")
_T("akt_krz.bel_akt_bis,  akt_krz.bel_akt_von,  akt_krz.fil,  ")
_T("akt_krz.fil_gr,  akt_krz.lad_akt_bis,  akt_krz.lad_akt_von,  ")
_T("akt_krz.lad_akv_sa,  akt_krz.lief_akv_sa,  akt_krz.mdn,  ")
_T("akt_krz.mdn_gr,  akt_krz.pr_ek_sa,  akt_krz.pr_vk_sa,  ")
_T("akt_krz.zeit_bis,  akt_krz.zeit_von,  akt_krz.zeit_von_n,  ")
_T("akt_krz.zeit_bis_n,  akt_krz.pr_vk1_sa,  akt_krz.pr_vk2_sa,  ")
_T("akt_krz.pr_vk3_sa,  akt_krz.dr_status,  akt_krz.pr_ek_sa_euro,  ")
_T("akt_krz.pr_vk_sa_euro from akt_krz ")

#line 72 "akt_krza.rpp"
                	          _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ? ")
				  _T("and lad_akt_von = ? ")
                                  _T("and lad_akv_sa = \"9\""));

}

int AKT_KRZA_CLASS::dbreadfirst_new ()
{
	    if (NewCursor == -1)
            {
		prepare ();
            }
	    if (NewCursor == -1) return -1;
            int dsqlstatus = sqlopen (NewCursor);
            if (dsqlstatus != 0) return dsqlstatus;
            return sqlfetch (NewCursor);    		
}

int AKT_KRZA_CLASS::dbread_new ()
{
            return sqlfetch (NewCursor);    		
}

BOOL AKT_KRZA_CLASS::operator== (AKT_KRZ& akt_krz)
{
            if (this->akt_krz.mdn_gr != akt_krz.mdn_gr) return FALSE;  
            if (this->akt_krz.mdn    != akt_krz.mdn) return FALSE;  
            if (this->akt_krz.fil_gr != akt_krz.fil_gr) return FALSE;  
            if (this->akt_krz.fil    != akt_krz.fil) return FALSE;  
            if (this->akt_krz.a      != akt_krz.a) return FALSE;  
            if (this->akt_krz.lad_akt_von.day 
                                     != akt_krz.lad_akt_von.day) return FALSE;
            if (this->akt_krz.lad_akt_von.month 
                                     != akt_krz.lad_akt_von.month) return FALSE;
            if (this->akt_krz.lad_akt_von.year 
                                     != akt_krz.lad_akt_von.year) return FALSE;
                                        
            return TRUE;
} 
