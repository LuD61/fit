#pragma once
#include "SplitPane.h"
#include "CompanyPage.h"
#include "ListPage.h"
#include "Lst.h"


// CCmpListPage-Dialogfeld

class CCmpListPage : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CCmpListPage)

public:
	CCmpListPage();
	virtual ~CCmpListPage();

// Dialogfelddaten
	enum { IDD = IDD_CMP_LIST_PAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
	virtual void OnSize (UINT, int, int);
    BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

public:
	LST_CLASS *Lst;
	CWnd *Frame;
	CSplitPane SplitPane;
	CCompanyPage Page1;
	CListPage Page2;
	virtual BOOL StepBack ();
    virtual void OnInsert ();
    virtual void OnDelete ();
	virtual BOOL Write ();
    virtual BOOL Print ();
};
