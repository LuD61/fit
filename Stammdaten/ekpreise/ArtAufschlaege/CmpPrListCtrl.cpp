#include "StdAfx.h"
#include "CmpPrListCtrl.h"
#include "StrFuncs.h"
#include "resource.h"
#include "Util.h"

CCmpPrListCtrl::CCmpPrListCtrl(void)
{
	MdnCombo.Init ();
	FilCombo.Init ();
	ArtSelect = "";
	ChoiceA	 = NULL;
	ChoiceMdnGr = NULL;
	ChoiceMdn = NULL;
	ChoicePtab = NULL;
	ChoiceFilGr = NULL;
	ChoiceFil = NULL;
	ModalChoiceA = TRUE;
	ModalChoiceMdnGr = TRUE;
	ModalChoiceMdn = TRUE;
	ModalChoicePtab = TRUE;
	ModalChoiceFilGr = TRUE;
	ModalChoiceFil = TRUE;
	PosA        = POSA;
	PosMdnGr    = POSMDNGR;
    PosMdn      = POSMDN;
	PosFilGr    = POSFILGR;
    PosFil      = POSFIL;
	Neuanlage = 1;
	NoColChanged = FALSE;
	CString ProgNam;
	CUtil::GetProgNam(ProgNam);
//	Cfg.SetProgName( _T("EkPreise"));
	Cfg.SetProgName( _T(ProgNam.GetBuffer()));



  //defaulteinstellungen
	//NEUNEU default alles auf 0
	/*** NEUNEU
	Pos_PrEk     = POS_PREK;

	Pos_BearbKost = POS_BEARBKOST;
	Pos_Kalk_Hk = POS_KALK_HK;

	Pos_Aufs_Sk = POS_AUFS_SK;
	Pos_Kalk_Sk = POS_KALK_SK;
	Pos_Aufs_Fil_Ek = POS_AUFS_FIL_EK;
	Pos_Kalk_Fil_Ek = POS_KALK_FIL_EK;
	Pos_Fil_Ek = POS_FIL_EK;
	Pos_Aufs_Fil_Vk = POS_AUFS_FIL_VK;
	Pos_Kalk_Fil_Vk = POS_KALK_FIL_VK;
	Pos_Fil_Vk = POS_FIL_VK;

	Pos_Aufs_Gh1 = POS_AUFS_GH1;
	Pos_KalkVk_Gh1 = POS_KALKVK_GH1;
	Pos_Gh1 = POS_GH1;
	Pos_Sp_Gh1 = POS_SP_GH1;
	Pos_Aufs_Gh2 = POS_AUFS_GH2;
	Pos_KalkVk_Gh2 = POS_KALKVK_GH2;
	Pos_Gh2 = POS_GH2;
	Pos_Sp_Gh2 = POS_SP_GH2;
	PosA2 = POSA2;
**********/
	//Reihenfolge aus ekpreise.cfg holen
	Pos_PrEk     = Cfg.GetCfgReihenfolge("_Ek",Pos_PrEk-POSFIL)+POSFIL ;

	Pos_BearbKost = Cfg.GetCfgReihenfolge("_HkKost",Pos_BearbKost-POSFIL)+POSFIL;
	Pos_Kalk_Hk = Cfg.GetCfgReihenfolge("_Hk",Pos_Kalk_Hk-POSFIL)+POSFIL;

	Pos_Aufs_Sk = Cfg.GetCfgReihenfolge("_SkAufs",Pos_Aufs_Sk-POSFIL)+POSFIL;
	Pos_Kalk_Sk = Cfg.GetCfgReihenfolge("_Sk",Pos_Kalk_Sk-POSFIL)+POSFIL;
	Pos_Aufs_Fil_Ek = Cfg.GetCfgReihenfolge("_FilEkAufs",Pos_Aufs_Fil_Ek-POSFIL)+POSFIL;
	Pos_Kalk_Fil_Ek = Cfg.GetCfgReihenfolge("_FilEkKalk",Pos_Kalk_Fil_Ek-POSFIL)+POSFIL;
	Pos_Fil_Ek = Cfg.GetCfgReihenfolge("_FilEk",Pos_Fil_Ek-POSFIL)+POSFIL;
	Pos_Aufs_Fil_Vk = Cfg.GetCfgReihenfolge("_FilVkAufs",Pos_Aufs_Fil_Vk-POSFIL)+POSFIL;
	Pos_Kalk_Fil_Vk = Cfg.GetCfgReihenfolge("_FilVkKalk",Pos_Kalk_Fil_Vk-POSFIL)+POSFIL;
	Pos_Fil_Vk = Cfg.GetCfgReihenfolge("_FilVk",Pos_Fil_Vk-POSFIL)+POSFIL;

	Pos_Aufs_Gh1 = Cfg.GetCfgReihenfolge("_Gh1Aufs",Pos_Aufs_Gh1-POSFIL)+POSFIL;
	Pos_KalkVk_Gh1 = Cfg.GetCfgReihenfolge("_Gh1KalkVk",Pos_KalkVk_Gh1-POSFIL)+POSFIL;
	Pos_Gh1 = Cfg.GetCfgReihenfolge("_Gh1",Pos_Gh1-POSFIL)+POSFIL;
	Pos_Sp_Gh1 = Cfg.GetCfgReihenfolge("_Gh1Sp",Pos_Sp_Gh1-POSFIL)+POSFIL;
	Pos_Aufs_Gh2 = Cfg.GetCfgReihenfolge("_Gh2Aufs",Pos_Aufs_Gh2-POSFIL)+POSFIL;
	Pos_KalkVk_Gh2 = Cfg.GetCfgReihenfolge("_Gh2KalkVk",Pos_KalkVk_Gh2-POSFIL)+POSFIL;
	Pos_Gh2 = Cfg.GetCfgReihenfolge("_Gh2",Pos_Gh2-POSFIL)+POSFIL;
	Pos_Sp_Gh2 = Cfg.GetCfgReihenfolge("_Gh2Sp",Pos_Sp_Gh2-POSFIL)+POSFIL;
	PosA2 = Cfg.GetCfgReihenfolge("_A2",PosA2-POSFIL)+POSFIL;
	char cfg_v [256];
    if (Cfg.GetCfgValue ("Neuanlage", cfg_v) == TRUE)
    {
			Neuanlage = atoi (cfg_v);
    }

    if (Cfg.GetCfgValue ("KalkulationsModus", cfg_v) == TRUE) 
	{
		strcpy (KalkulationsModus,cfg_v); 
	}

    if (Cfg.GetCfgValue ("SchreibModus", cfg_v) == TRUE) 
	{
		strcpy (SchreibModus,cfg_v); 
	}





//	PosPrVk     = POSPRVK;
//	PosPrEkKalk = POSPREKKALK;
//	PosPrVkKalk = POSPRVKKALK;
//	PosSk       = POSSK;
//	PosSp       = POSSP;

	Position[0] = &PosA;
	Position[1] = &PosMdnGr;
	Position[2] = &PosMdn;
	Position[3] = &PosFilGr;
	Position[4] = &PosFil;
	//LuD Neu
	Position[Pos_PrEk] = &Pos_PrEk;
	Position[Pos_BearbKost] = &Pos_BearbKost;
	Position[Pos_Kalk_Hk] = &Pos_Kalk_Hk;

	Position[Pos_Aufs_Sk] = &Pos_Aufs_Sk;
	Position[Pos_Kalk_Sk] = &Pos_Kalk_Sk;
	Position[Pos_Aufs_Fil_Ek] = &Pos_Aufs_Fil_Ek;
	Position[Pos_Kalk_Fil_Ek] = &Pos_Kalk_Fil_Ek;
	Position[Pos_Fil_Ek] = &Pos_Fil_Ek;
	Position[Pos_Aufs_Fil_Vk] = &Pos_Aufs_Fil_Vk;
	Position[Pos_Kalk_Fil_Vk] = &Pos_Kalk_Fil_Vk;
	Position[Pos_Fil_Vk] = &Pos_Fil_Vk;

	Position[Pos_Aufs_Gh1] = &Pos_Aufs_Gh1;
	Position[Pos_KalkVk_Gh1] = &Pos_KalkVk_Gh1;
	Position[Pos_Gh1] = &Pos_Gh1;
	Position[Pos_Sp_Gh1] = &Pos_Sp_Gh1;
	Position[Pos_Aufs_Gh2] = &Pos_Aufs_Gh2;
	Position[Pos_KalkVk_Gh2] = &Pos_KalkVk_Gh2;
	Position[Pos_Gh2] = &Pos_Gh2;
	Position[Pos_Sp_Gh2] = &Pos_Sp_Gh2;
	Position[PosA2] = &PosA2;


//	Position[6] = &PosPrVk;
//	Position[7] = &PosPrEkKalk;
//	Position[8] = &PosPrVkKalk;
//	Position[9] = &PosSk;
//	Position[10] = &PosSp;
//	Position[11] = NULL;
	MaxComboEntries = 20;
	FilGrCursor = -1;
	FilCursor = -1;

	Aufschlag = LIST;
	Mode = STANDARD;
	m_Mdn = 0;
	sk_vollk = 0.0;
	spanne = 0.0;
	ListRows.Init ();
	Lst = NULL;
}

CCmpPrListCtrl::~CCmpPrListCtrl(void)
{
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
	}
	if (ChoiceFil != NULL)
	{
		delete ChoiceFil;
	}

	CString *c;
    MdnCombo.FirstPosition ();
	while ((c = (CString *) MdnCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnCombo.Init ();
    FilCombo.FirstPosition ();
	while ((c = (CString *) FilCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilCombo.Init ();
}

BEGIN_MESSAGE_MAP(CCmpPrListCtrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CCmpPrListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		CString cA = GetItemText (EditRow, PosA);
		if (StrToDouble (cA) == 0.0 && Neuanlage == 1) 
		{
			StartEnter (PosA, 0); //TESTTEST
		}
		else
		{
			StartEnter (GetFirstCol(), 0);
		}
	}
	else
	{
		if (!AppendEmpty ()) return; 
		if (m_Mdn == 0)
		{
			StartEnter (PosA, 0);
		}
		else
		{
			StartEnter (PosA, 0);
		}
	}
}

//void CCmpPrListCtrl::StartEnter (int col, int row)
void CCmpPrListCtrl::StartEnter (int col, int row, BOOL readonly)  //testtesttest
{
	BOOL RowChanged = FALSE;
//	int sRow;
	if (col == 0) col = 1;
// LuDTest	if (col == PosPrEkKalk) return;
	//LuD Neu
//	if (col == Pos_BearbKost) return;
//	if (col == Pos_Kalk_Hk) return;

//	if (col == Pos_Aufs_Sk) return;
//	if (col == Pos_Kalk_Sk) return;
//	if (col == Pos_Aufs_Fil_Ek) return;
//	if (col == Pos_Kalk_Fil_Ek) return;
//	if (col == Pos_Fil_Ek) return;
//	if (col == Pos_Aufs_Fil_Vk) return;
//	if (col == Pos_Kalk_Fil_Vk) return;
//	if (col == Pos_Fil_Vk) return;
//	if (col == Pos_Aufs_Gh1) return;
//	if (col == Pos_KalkVk_Gh1) return;
//	if (col == Pos_Gh1) return;
//	if (col == Pos_Sp_Gh1) return;
//	if (col == Pos_Aufs_Gh2) return;
//	if (col == Pos_KalkVk_Gh2) return;
//	if (col == Pos_Gh2) return;
//	if (col == Pos_Sp_Gh2) return;
//	if (col == PosA2) return;



/*
	StopEnter ();
	StartEnter (EditCol, EditRow);
*/
	CString Value;
	GetEnter ();
	GetColValue (row, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (col == PosFilGr || col == PosFil) return;
	}

	if (EditCol == PosFil)   ReadFil ();
	if (EditCol == PosFilGr) ReadFilGr ();

	if (col == PosA && readonly == 1)   //testtesttest  readonly = 1 : einf�gemodus
	{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
	}
	if (col == Pos_Aufs_Gh1 ||
		col == Pos_KalkVk_Gh1 ||
		col == Pos_Gh1 ||
		col == Pos_Sp_Gh1 ||
		col == Pos_Aufs_Gh2 ||
		col == Pos_KalkVk_Gh2 ||
		col == Pos_Gh2 ||
		col == Pos_Aufs_Fil_Ek ||
		col == Pos_Kalk_Fil_Ek ||
		col == Pos_Fil_Ek  
		)   //WAL-85
	{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
	}

	else if (col == PosMdnGr)
	{
		if (MaxComboEntries > 0 && MdnGrCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &MdnGrCombo);
			oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosMdn)
	{
		if (MaxComboEntries > 0 && MdnCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &MdnCombo);
			oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosFilGr)
	{
		FillFilGrCombo (row);
		if (MaxComboEntries > 0 && FilGrCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &FilGrCombo);
		    oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosFil)
	{
		FillFilCombo (row);
		if (MaxComboEntries > 0 && FilCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &FilCombo);
		    oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else
	{
		CString cA = GetItemText (EditRow, PosA);
		if (StrToDouble (cA) == 0.0) 
		{
			CEditListCtrl::StartEnter (col, row,0);  //TESTTEST
		}
		else
		{
			CEditListCtrl::StartEnter (col, row,isDisplayonly(col,row));
		}
	}
}

void CCmpPrListCtrl::StopEnter ()
{
	CString Text;

	if (EditRow == -1) EditRow = 0;
	Text = GetItemText (EditRow, PosMdn);
	short OldMdn = _tstoi (Text.GetBuffer ());

	if (IsWindow (ListEdit.m_hWnd))
	{
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	else if (IsWindow (ListDate.m_hWnd))
	{
		ListDate.GetWindowText (Text);
//		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListDate.DestroyWindow ();
	}

	short NewMdn = _tstoi (Text.GetBuffer ());
	if ((EditCol == PosMdn) && (NewMdn == 0 || NewMdn != OldMdn))
	{
		FillList.SetItemText (_T("0"), EditRow, PosFilGr);
		FillList.SetItemText (_T("0 Mandant"), EditRow, PosFil);
	}
	PerformListChangeHandler (EditRow, EditCol);
// 010410 ?? DDDDDDDDDDDDD	UpdateWindow ();
}

void CCmpPrListCtrl::SetSel (CString& Text)
{

   if (!isDisplayonly(EditCol))
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CCmpPrListCtrl::FormatText (CString& Text)
{
	if (Nachkomma(EditCol) > 0 && !isDisplayonly(EditCol,EditRow)) 
	{
		DoubleToString (StrToDouble (Text), Text,Nachkomma(EditCol));
	}
	return;

    if (EditCol == Pos_PrEk)
	{
		DoubleToString (StrToDouble (Text), Text, 3);
	}
    else if (EditCol == Pos_BearbKost)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
}

void CCmpPrListCtrl::NextRow ()
{
/*
	StopEnter ();
	StartEnter (EditCol, EditRow);
*/
	CString cA;
	GetEnter ();
	CString PrEk = GetItemText (EditRow, Pos_PrEk);
//	CString PrVk = GetItemText (EditRow, PosPrVk);
/*
	if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
	{
		return;
	}
*/
	int count = GetItemCount ();
	if (EditCol == PosFil)
	{
		ReadFil ();
	}	
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}	
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}

	else if (EditCol == PosA)
	{
// 		ReadA ();
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
		{
			SearchListCtrl.Edit.GetWindowText (cA); 
		}
		else
		{
			GetColValue (EditRow, PosA, cA);
		}

		if (StrToDouble (cA) == 0.0) 
		{
			DeleteItem (EditRow);
			InvalidateRect (NULL);
			while (isDisplayonly(EditCol))
			{
				EditCol ++;
				if (LastCol()) break;
			}
			EnsureVisible (EditRow, FALSE);
			NoColChanged = TRUE;
			StartEnter (EditCol, EditRow);
			NoColChanged = FALSE;
			return;
		}

			while (isDisplayonly(EditCol))
			{
				EditCol ++;
				if (LastCol()) break;
			}
	}

	else if (EditCol == Pos_Aufs_Gh1 ||
		EditCol == Pos_KalkVk_Gh1 ||
		EditCol == Pos_Gh1 ||
		EditCol == Pos_Sp_Gh1 ||
		EditCol == Pos_Aufs_Gh2 ||
		EditCol == Pos_KalkVk_Gh2 ||
		EditCol == Pos_Gh2 ||
		EditCol == Pos_Aufs_Fil_Ek ||
		EditCol == Pos_Kalk_Fil_Ek ||
		EditCol == Pos_Fil_Ek  
		)   //WAL-85
	{
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
		{
			SearchListCtrl.Edit.GetWindowText (cA); 
		}
		else
		{
			GetColValue (EditRow, PosA, cA);
		}
			if (strlen(cA.GetBuffer()) > 0)
			{
				 _tcscpy (Ptabn.ptabn.ptitem, _T("aufschlaege"));
				_stprintf (Ptabn.ptabn.ptwert, _T("%s"), cA);
				if (Ptabn.dbreadfirst () == 100)
				{
					return;
				}
			}
	}

	SetEditText ();
	TestAprIndex ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		return ;  //LuD kein insert mehr !
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		if (m_Mdn == 0)
		{
			EditCol = PosMdnGr;
		}
		else
		{
//			EditCol = PosFilGr;  
			EditCol = PosA;  //LuD 140908
		}
	}
	else
	{
		if (EditRow == -1)
		{
			EditRow ++;
			StopEnter ();
		}
		else
		{
			StopEnter ();
			EditRow ++;
		}
	}
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFil)
		{
			EditRow --;
		}
		if (EditCol == PosFilGr)
		{
			EditRow --;
		}
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CCmpPrListCtrl::PriorRow ()
{
	BOOL Test = TRUE;
	CString cA;
	if (EditRow <= 0)
	{
			return;
	}

/*
	StopEnter ();
	StartEnter (EditCol, EditRow);
*/
	GetEnter ();
	int count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		CString PrEk = GetItemText (EditRow, Pos_PrEk);
//		CString PrVk = GetItemText (EditRow, PosPrVk);
/*
		if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
		{
	        DeleteItem (EditRow);
			Test = FALSE;
		}
*/
	}
//	else
	{
		if (EditRow <= 0)
		{
			return;
		}
		if (EditCol == PosFil)
		{
			ReadFil ();
		}
		else if (EditCol == PosFilGr)
		{
			ReadFilGr ();
		}
		else if (EditCol == PosMdn)
		{
			ReadMdn ();
		}
		else if (EditCol == PosMdnGr)
		{
			ReadMdnGr ();
		}
		else if (EditCol == Pos_Aufs_Gh1 ||
			EditCol == Pos_KalkVk_Gh1 ||
			EditCol == Pos_Gh1 ||
			EditCol == Pos_Sp_Gh1 ||
			EditCol == Pos_Aufs_Gh2 ||
			EditCol == Pos_KalkVk_Gh2 ||
			EditCol == Pos_Gh2 ||
			EditCol == Pos_Aufs_Fil_Ek ||
			EditCol == Pos_Kalk_Fil_Ek ||
			EditCol == Pos_Fil_Ek  
			)   //WAL-85
		{
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
		{
			SearchListCtrl.Edit.GetWindowText (cA); 
		}
		else
		{
			GetColValue (EditRow, PosA, cA);
		}
			if (strlen(cA.GetBuffer()) > 0)
			{
				 _tcscpy (Ptabn.ptabn.ptitem, _T("aufschlaege"));
				_stprintf (Ptabn.ptabn.ptwert, _T("%s"), cA);
				if (Ptabn.dbreadfirst () == 100)
				{
					return;
				}
			}
		}

		else if (EditCol == PosA)
		{
// 		ReadA ();
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
		{
			SearchListCtrl.Edit.GetWindowText (cA); 
		}
		else
		{
			GetColValue (EditRow, PosA, cA);
		}

		if (StrToDouble (cA) == 0.0) 
		{
			DeleteItem (EditRow);
			InvalidateRect (NULL);
			while (isDisplayonly(EditCol))
			{
				EditCol ++;
				if (LastCol()) break;
			}
			EnsureVisible (EditRow, FALSE);
			NoColChanged = TRUE;
			StartEnter (EditCol, EditRow);
			NoColChanged = FALSE;
			return;
		}

			while (isDisplayonly(EditCol))
			{
				EditCol ++;
				if (LastCol()) break;
			}
		}
		if (Test)
		{
			TestAprIndex ();
		}
	}
	StopEnter ();
	EditRow --;
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFil)
		{
			EditRow ++;
		}
		if (EditCol == PosFilGr)
		{
			EditRow ++;
		}
	}
 
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}

int CCmpPrListCtrl::Nachkomma (int Col)
{
	if (Col == Pos_PrEk) return EkNachkomma ;
	if (Col == Pos_BearbKost) return HkKostNachkomma;
	if (Col == Pos_Kalk_Hk) return HkNachkomma;
	if (Col == Pos_Aufs_Sk) return SkAufsNachkomma;
	if (Col == Pos_Kalk_Sk) return SkNachkomma;
	if (Col == Pos_Aufs_Fil_Ek) return FilEkAufsNachkomma;
	if (Col == Pos_Kalk_Fil_Ek) return FilEkKalkNachkomma;	
	if (Col == Pos_Fil_Ek) return FilEkNachkomma;
	if (Col == Pos_Aufs_Fil_Vk) return FilVkAufsNachkomma;
	if (Col == Pos_Kalk_Fil_Vk) return FilVkKalkNachkomma;
	if (Col == Pos_Fil_Vk) return FilVkNachkomma;
	if (Col == Pos_Aufs_Gh1) return Gh1AufsNachkomma;
	if (Col == Pos_KalkVk_Gh1) return Gh1KalkVkNachkomma;
	if (Col == Pos_Gh1) return Gh1Nachkomma;
	if (Col == Pos_Sp_Gh1) return Gh1SpNachkomma;
	if (Col == Pos_Aufs_Gh2) return Gh2AufsNachkomma;
	if (Col == Pos_KalkVk_Gh2) return Gh2KalkVkNachkomma;
	if (Col == Pos_Gh2) return Gh2Nachkomma;		
	if (Col == Pos_Sp_Gh2) return Gh2SpNachkomma;
	return 0;
}

BOOL CCmpPrListCtrl::isDisplayonly (int Col, int Row)
{
	CString Text;
	if (strcmp(Lst->lst.EkEdit,"J") == 0) EkEdit = 1; else EkEdit = 0;
	if (strcmp(Lst->lst.HkEdit,"J") == 0) HkEdit = 1; else HkEdit = 0;
	if (strcmp(Lst->lst.HkKostEdit,"J") == 0) HkKostEdit = 1; else HkKostEdit = 0;
	if (strcmp(Lst->lst.SkAufsEdit,"J") == 0) SkAufsEdit = 1; else SkAufsEdit = 0;
	if (strcmp(Lst->lst.SkEdit,"J") == 0) SkEdit = 1; else SkEdit = 0;
	if (strcmp(Lst->lst.FilEkAufsEdit,"J") == 0) FilEkAufsEdit = 1; else FilEkAufsEdit = 0;
	if (strcmp(Lst->lst.FilEkKalkEdit,"J") == 0) FilEkKalkEdit = 1; else FilEkKalkEdit = 0;
	if (strcmp(Lst->lst.FilEkEdit,"J") == 0) FilEkEdit = 1; else FilEkEdit = 0;
	if (strcmp(Lst->lst.FilVkAufsEdit,"J") == 0) FilVkAufsEdit = 1; else FilVkAufsEdit = 0;
	if (strcmp(Lst->lst.FilVkKalkEdit,"J") == 0) FilVkKalkEdit = 1; else FilVkKalkEdit = 0;
	if (strcmp(Lst->lst.FilVkEdit,"J") == 0) FilVkEdit = 1; else FilVkEdit = 0;

	if (strcmp(Lst->lst.Gh1AufsEdit,"J") == 0) Gh1AufsEdit = 1; else Gh1AufsEdit = 0;
	if (strcmp(Lst->lst.Gh1KalkVkEdit,"J") == 0) Gh1KalkVkEdit = 1; else Gh1KalkVkEdit = 0;
	if (strcmp(Lst->lst.Gh1Edit,"J") == 0) Gh1Edit = 1; else Gh1Edit = 0;
	if (strcmp(Lst->lst.Gh1SpEdit,"J") == 0) Gh1SpEdit = 1; else Gh1SpEdit = 0;
	if (strcmp(Lst->lst.Gh2AufsEdit,"J") == 0) Gh2AufsEdit = 1; else Gh2AufsEdit = 0;
	if (strcmp(Lst->lst.Gh2KalkVkEdit,"J") == 0) Gh2KalkVkEdit = 1; else Gh2KalkVkEdit = 0;
	if (strcmp(Lst->lst.Gh2Edit,"J") == 0) Gh2Edit = 1; else Gh2Edit = 0;
	if (strcmp(Lst->lst.Gh2SpEdit,"J") == 0) Gh2SpEdit = 1; else Gh2SpEdit = 0;




	if (Col == PosA) return !AEdit ;
	if (Col == Pos_PrEk) 
	{
		if (EkEdit == 1)
		{
			Text = GetItemText (Row, Col);
			if (strstr(Text.GetBuffer(),"_") > 0) EkEdit = 0;  //testtest
		}
		return !EkEdit ;
	}
	if (Col == Pos_BearbKost) 
	{
		if (HkKostEdit == 1)
		{
			Text = GetItemText (Row, Col);
			if (strstr(Text.GetBuffer(),"_") > 0) HkKostEdit = 0; //testtest
		}
		return !HkKostEdit ;
	}
	if (Col == Pos_Kalk_Hk)
	{
		if (HkEdit == 1)
		{
			Text = GetItemText (Row, Col);
			if (strstr(Text.GetBuffer(),"_") > 0) HkEdit = 0; //testtest
		}
		return !HkEdit;
	}
	if (Col == Pos_Aufs_Sk) return !SkAufsEdit;
	if (Col == Pos_Kalk_Sk) return !SkEdit;
	if (Col == Pos_Aufs_Fil_Ek) return !FilEkAufsEdit;
	if (Col == Pos_Kalk_Fil_Ek) return !FilEkKalkEdit;	
	if (Col == Pos_Fil_Ek) return !FilEkEdit;
	if (Col == Pos_Aufs_Fil_Vk) return !FilVkAufsEdit;
	if (Col == Pos_Kalk_Fil_Vk) return !FilVkKalkEdit;
	if (Col == Pos_Fil_Vk) return !FilVkEdit;
	if (Col == Pos_Aufs_Gh1) return !Gh1AufsEdit;
	if (Col == Pos_KalkVk_Gh1) return !Gh1KalkVkEdit;
	if (Col == Pos_Gh1) return !Gh1Edit;
	if (Col == Pos_Sp_Gh1) return !Gh1SpEdit;
	if (Col == Pos_Aufs_Gh2) return !Gh2AufsEdit;
	if (Col == Pos_KalkVk_Gh2) return !Gh2KalkVkEdit;
	if (Col == Pos_Gh2) return !Gh2Edit;		
	if (Col == Pos_Sp_Gh2) return !Gh2SpEdit;
	return TRUE;
}

BOOL CCmpPrListCtrl::isDisplayonly (int Col)
{
	CString Text;
	if (strcmp(Lst->lst.EkEdit,"J") == 0) EkEdit = 1; else EkEdit = 0;
	if (strcmp(Lst->lst.HkEdit,"J") == 0) HkEdit = 1; else HkEdit = 0;
	if (strcmp(Lst->lst.HkKostEdit,"J") == 0) HkKostEdit = 1; else HkKostEdit = 0;
	if (strcmp(Lst->lst.SkAufsEdit,"J") == 0) SkAufsEdit = 1; else SkAufsEdit = 0;
	if (strcmp(Lst->lst.SkEdit,"J") == 0) SkEdit = 1; else SkEdit = 0;
	if (strcmp(Lst->lst.FilEkAufsEdit,"J") == 0) FilEkAufsEdit = 1; else FilEkAufsEdit = 0;
	if (strcmp(Lst->lst.FilEkKalkEdit,"J") == 0) FilEkKalkEdit = 1; else FilEkKalkEdit = 0;
	if (strcmp(Lst->lst.FilEkEdit,"J") == 0) FilEkEdit = 1; else FilEkEdit = 0;
	if (strcmp(Lst->lst.FilVkAufsEdit,"J") == 0) FilVkAufsEdit = 1; else FilVkAufsEdit = 0;
	if (strcmp(Lst->lst.FilVkKalkEdit,"J") == 0) FilVkKalkEdit = 1; else FilVkKalkEdit = 0;
	if (strcmp(Lst->lst.FilVkEdit,"J") == 0) FilVkEdit = 1; else FilVkEdit = 0;

	if (strcmp(Lst->lst.Gh1AufsEdit,"J") == 0) Gh1AufsEdit = 1; else Gh1AufsEdit = 0;
	if (strcmp(Lst->lst.Gh1KalkVkEdit,"J") == 0) Gh1KalkVkEdit = 1; else Gh1KalkVkEdit = 0;
	if (strcmp(Lst->lst.Gh1Edit,"J") == 0) Gh1Edit = 1; else Gh1Edit = 0;
	if (strcmp(Lst->lst.Gh1SpEdit,"J") == 0) Gh1SpEdit = 1; else Gh1SpEdit = 0;
	if (strcmp(Lst->lst.Gh2AufsEdit,"J") == 0) Gh2AufsEdit = 1; else Gh2AufsEdit = 0;
	if (strcmp(Lst->lst.Gh2KalkVkEdit,"J") == 0) Gh2KalkVkEdit = 1; else Gh2KalkVkEdit = 0;
	if (strcmp(Lst->lst.Gh2Edit,"J") == 0) Gh2Edit = 1; else Gh2Edit = 0;
	if (strcmp(Lst->lst.Gh2SpEdit,"J") == 0) Gh2SpEdit = 1; else Gh2SpEdit = 0;




	if (Col == PosA) return !AEdit ;
	if (Col == Pos_PrEk) 
	{
		return !EkEdit ;
	}
	if (Col == Pos_BearbKost) 
	{
		return !HkKostEdit ;
	}
	if (Col == Pos_Kalk_Hk) return !HkEdit;
	if (Col == Pos_Aufs_Sk) return !SkAufsEdit;
	if (Col == Pos_Kalk_Sk) return !SkEdit;
	if (Col == Pos_Aufs_Fil_Ek) return !FilEkAufsEdit;
	if (Col == Pos_Kalk_Fil_Ek) return !FilEkKalkEdit;	
	if (Col == Pos_Fil_Ek) return !FilEkEdit;
	if (Col == Pos_Aufs_Fil_Vk) return !FilVkAufsEdit;
	if (Col == Pos_Kalk_Fil_Vk) return !FilVkKalkEdit;
	if (Col == Pos_Fil_Vk) return !FilVkEdit;
	if (Col == Pos_Aufs_Gh1) return !Gh1AufsEdit;
	if (Col == Pos_KalkVk_Gh1) return !Gh1KalkVkEdit;
	if (Col == Pos_Gh1) return !Gh1Edit;
	if (Col == Pos_Sp_Gh1) return !Gh1SpEdit;
	if (Col == Pos_Aufs_Gh2) return !Gh2AufsEdit;
	if (Col == Pos_KalkVk_Gh2) return !Gh2KalkVkEdit;
	if (Col == Pos_Gh2) return !Gh2Edit;		
	if (Col == Pos_Sp_Gh2) return !Gh2SpEdit;
	return TRUE;
}


int CCmpPrListCtrl::GetFirstCol ()
{
	int Col = 99;
	int Pos ;
	if (strcmp(Lst->lst.EkEdit,"J") == 0) EkEdit = 1; else EkEdit = 0;
	if (strcmp(Lst->lst.HkEdit,"J") == 0) HkEdit = 1; else HkEdit = 0;
	if (strcmp(Lst->lst.HkKostEdit,"J") == 0) HkKostEdit = 1; else HkKostEdit = 0;
	if (strcmp(Lst->lst.SkAufsEdit,"J") == 0) SkAufsEdit = 1; else SkAufsEdit = 0;
	if (strcmp(Lst->lst.SkEdit,"J") == 0) SkEdit = 1; else SkEdit = 0;
	if (strcmp(Lst->lst.FilEkAufsEdit,"J") == 0) FilEkAufsEdit = 1; else FilEkAufsEdit = 0;
	if (strcmp(Lst->lst.FilEkKalkEdit,"J") == 0) FilEkKalkEdit = 1; else FilEkKalkEdit = 0;
	if (strcmp(Lst->lst.FilEkEdit,"J") == 0) FilEkEdit = 1; else FilEkEdit = 0;
	if (strcmp(Lst->lst.FilVkAufsEdit,"J") == 0) FilVkAufsEdit = 1; else FilVkAufsEdit = 0;
	if (strcmp(Lst->lst.FilVkKalkEdit,"J") == 0) FilVkKalkEdit = 1; else FilVkKalkEdit = 0;
	if (strcmp(Lst->lst.FilVkEdit,"J") == 0) FilVkEdit = 1; else FilVkEdit = 0;


	if (strcmp(Lst->lst.Gh1AufsEdit,"J") == 0) Gh1AufsEdit = 1; else Gh1AufsEdit = 0;
	if (strcmp(Lst->lst.Gh1KalkVkEdit,"J") == 0) Gh1KalkVkEdit = 1; else Gh1KalkVkEdit = 0;
	if (strcmp(Lst->lst.Gh1Edit,"J") == 0) Gh1Edit = 1; else Gh1Edit = 0;
	if (strcmp(Lst->lst.Gh1SpEdit,"J") == 0) Gh1SpEdit = 1; else Gh1SpEdit = 0;
	if (strcmp(Lst->lst.Gh2AufsEdit,"J") == 0) Gh2AufsEdit = 1; else Gh2AufsEdit = 0;
	if (strcmp(Lst->lst.Gh2KalkVkEdit,"J") == 0) Gh2KalkVkEdit = 1; else Gh2KalkVkEdit = 0;
	if (strcmp(Lst->lst.Gh2Edit,"J") == 0) Gh2Edit = 1; else Gh2Edit = 0;
	if (strcmp(Lst->lst.Gh2SpEdit,"J") == 0) Gh2SpEdit = 1; else Gh2SpEdit = 0;



	if (Gh2SpEdit)
	{
		Pos = Pos_Sp_Gh2; (Pos < Col) ? Col = Pos : Col;
	}
	if (Gh2Edit)
	{
		Pos = Pos_Gh2; (Pos < Col) ? Col = Pos : Col;		
	}
	if (Gh2KalkVkEdit)
	{
		Pos = Pos_KalkVk_Gh2; (Pos < Col) ? Col = Pos : Col;
	}
	if (Gh2AufsEdit)
	{
		Pos = Pos_Aufs_Gh2; (Pos < Col) ? Col = Pos : Col;
	}
	if (Gh1SpEdit)
	{
		Pos = Pos_Sp_Gh1; (Pos < Col) ? Col = Pos : Col;
	}
	if (Gh1Edit)
	{
		Pos = Pos_Gh1; (Pos < Col) ? Col = Pos : Col;
	}
	if (Gh1KalkVkEdit)
	{
		Pos = Pos_KalkVk_Gh1; (Pos < Col) ? Col = Pos : Col;
	}
	if (Gh1AufsEdit)
	{
		Pos = Pos_Aufs_Gh1; (Pos < Col) ? Col = Pos : Col;
	}
	if (FilVkEdit)
	{
		Pos = Pos_Fil_Vk; (Pos < Col) ? Col = Pos : Col;
	}
	if (FilVkKalkEdit)
	{
		Pos = Pos_Kalk_Fil_Vk; (Pos < Col) ? Col = Pos : Col;
	}
	if (FilVkAufsEdit)
	{
		Pos = Pos_Aufs_Fil_Vk; (Pos < Col) ? Col = Pos : Col;
	}
	if (FilEkEdit)
	{
		Pos = Pos_Fil_Ek; (Pos < Col) ? Col = Pos : Col;
	}
	if (FilEkKalkEdit)
	{
		Pos = Pos_Kalk_Fil_Ek; (Pos < Col) ? Col = Pos : Col;	
	}
	if (FilEkAufsEdit)
	{
		Pos = Pos_Aufs_Fil_Ek; (Pos < Col) ? Col = Pos : Col;
	}
	if (SkEdit)
	{
		Pos = Pos_Kalk_Sk; (Pos < Col) ? Col = Pos : Col;
	}
	if (SkAufsEdit)
	{
		Pos = Pos_Aufs_Sk; (Pos < Col) ? Col = Pos : Col;
	}
	if (HkEdit)
	{
		Pos = Pos_Kalk_Hk; (Pos < Col) ? Col = Pos : Col;
	}
	if (HkKostEdit)
	{
		Pos = Pos_BearbKost; (Pos < Col) ? Col = Pos : Col;
	}
	if (EkEdit)
	{
		Pos = Pos_PrEk; (Pos < Col) ? Col = Pos : Col;
	}
return Col;
}

int CCmpPrListCtrl::GetLastCol ()
{
	int Col = 0;
	int Pos = 0;
	if (strcmp(Lst->lst.EkEdit,"J") == 0) EkEdit = 1; else EkEdit = 0;
	if (strcmp(Lst->lst.HkEdit,"J") == 0) HkEdit = 1; else HkEdit = 0;
	if (strcmp(Lst->lst.HkKostEdit,"J") == 0) HkKostEdit = 1; else HkKostEdit = 0;
	if (strcmp(Lst->lst.SkAufsEdit,"J") == 0) SkAufsEdit = 1; else SkAufsEdit = 0;
	if (strcmp(Lst->lst.SkEdit,"J") == 0) SkEdit = 1; else SkEdit = 0;
	if (strcmp(Lst->lst.FilEkAufsEdit,"J") == 0) FilEkAufsEdit = 1; else FilEkAufsEdit = 0;
	if (strcmp(Lst->lst.FilEkKalkEdit,"J") == 0) FilEkKalkEdit = 1; else FilEkKalkEdit = 0;
	if (strcmp(Lst->lst.FilEkEdit,"J") == 0) FilEkEdit = 1; else FilEkEdit = 0;
	if (strcmp(Lst->lst.FilVkAufsEdit,"J") == 0) FilVkAufsEdit = 1; else FilVkAufsEdit = 0;
	if (strcmp(Lst->lst.FilVkKalkEdit,"J") == 0) FilVkKalkEdit = 1; else FilVkKalkEdit = 0;
	if (strcmp(Lst->lst.FilVkEdit,"J") == 0) FilVkEdit = 1; else FilVkEdit = 0;


	if (strcmp(Lst->lst.Gh1AufsEdit,"J") == 0) Gh1AufsEdit = 1; else Gh1AufsEdit = 0;
	if (strcmp(Lst->lst.Gh1KalkVkEdit,"J") == 0) Gh1KalkVkEdit = 1; else Gh1KalkVkEdit = 0;
	if (strcmp(Lst->lst.Gh1Edit,"J") == 0) Gh1Edit = 1; else Gh1Edit = 0;
	if (strcmp(Lst->lst.Gh1SpEdit,"J") == 0) Gh1SpEdit = 1; else Gh1SpEdit = 0;
	if (strcmp(Lst->lst.Gh2AufsEdit,"J") == 0) Gh2AufsEdit = 1; else Gh2AufsEdit = 0;
	if (strcmp(Lst->lst.Gh2KalkVkEdit,"J") == 0) Gh2KalkVkEdit = 1; else Gh2KalkVkEdit = 0;
	if (strcmp(Lst->lst.Gh2Edit,"J") == 0) Gh2Edit = 1; else Gh2Edit = 0;
	if (strcmp(Lst->lst.Gh2SpEdit,"J") == 0) Gh2SpEdit = 1; else Gh2SpEdit = 0;



	if (EkEdit)
	{
		Pos = Pos_PrEk; (Pos > Col) ? Col = Pos : Col;
	}
	if (HkKostEdit)
	{
		Pos = Pos_BearbKost; (Pos > Col) ? Col = Pos : Col;
	}
	if (HkEdit)
	{
		Pos = Pos_Kalk_Hk; (Pos > Col) ? Col = Pos : Col;
	}
	if (SkAufsEdit)
	{
		Pos = Pos_Aufs_Sk; (Pos > Col) ? Col = Pos : Col;
	}
	if (SkEdit)
	{
		Pos = Pos_Kalk_Sk; (Pos > Col) ? Col = Pos : Col;
	}
	if (FilEkAufsEdit)
	{
		Pos = Pos_Aufs_Fil_Ek; (Pos > Col) ? Col = Pos : Col;
	}
	if (FilEkKalkEdit)
	{
		Pos = Pos_Kalk_Fil_Ek; (Pos > Col) ? Col = Pos : Col;	
	}
	if (FilEkEdit)
	{
		Pos = Pos_Fil_Ek; (Pos > Col) ? Col = Pos : Col;
	}
	if (FilVkAufsEdit)
	{
		Pos = Pos_Aufs_Fil_Vk; (Pos > Col) ? Col = Pos : Col;
	}
	if (FilVkKalkEdit)
	{
		Pos = Pos_Kalk_Fil_Vk; (Pos > Col) ? Col = Pos : Col;
	}
	if (FilVkEdit)
	{
		Pos = Pos_Fil_Vk; (Pos > Col) ? Col = Pos : Col;
	}
	if (Gh1AufsEdit)
	{
		Pos = Pos_Aufs_Gh1; (Pos > Col) ? Col = Pos : Col;
	}
	if (Gh1KalkVkEdit)
	{
		Pos = Pos_KalkVk_Gh1; (Pos > Col) ? Col = Pos : Col;
	}
	if (Gh1Edit)
	{
		Pos = Pos_Gh1; (Pos > Col) ? Col = Pos : Col;
	}
	if (Gh1SpEdit)
	{
		Pos = Pos_Sp_Gh1; (Pos > Col) ? Col = Pos : Col;
	}
	if (Gh2AufsEdit)
	{
		Pos = Pos_Aufs_Gh2; (Pos > Col) ? Col = Pos : Col;
	}
	if (Gh2KalkVkEdit)
	{
		Pos = Pos_KalkVk_Gh2; (Pos > Col) ? Col = Pos : Col;
	}
	if (Gh2Edit)
	{
		Pos = Pos_Gh2; (Pos > Col) ? Col = Pos : Col;		
	}
	if (Gh2SpEdit)
	{
		Pos = Pos_Sp_Gh2; (Pos > Col) ? Col = Pos : Col;
	}
return Col;
}

BOOL CCmpPrListCtrl::LastCol ()
{
//	int Col = PosPrVk;
//	int Col = Pos_PrEk;  //LuD  LastCol
	int Col = GetLastCol();  //LuDTest  LastCol
	if (EditCol == Col) return TRUE;
	if (GetColumnWidth (Col) == 0)
	{
		Col = GetFirstCol();
	}
	if (EditCol < Col) return FALSE;
	/*
	if (Mode == TERMIN && EditCol >= PosPrVk)
	{
		return TRUE;
	}
	*/

	return TRUE;
}

void CCmpPrListCtrl::OnReturn ()
{
	CString cA;

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol == PosFil)
	{
		ReadFil ();
	}
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}
	else if (EditCol == PosA)
	{
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
		{
			SearchListCtrl.Edit.GetWindowText (cA); 
		}
		else
		{
			GetColValue (EditRow, PosA, cA);
		}

		if (StrToDouble (cA) == 0.0) 
		{
			return;
		}
		ReadA ();
		if (!TestAprIndex ())
		{
			ReadEk_pr ();
		}
	}
	else if (EditCol == Pos_Aufs_Gh1 ||
		EditCol == Pos_KalkVk_Gh1 ||
		EditCol == Pos_Gh1 ||
		EditCol == Pos_Sp_Gh1 ||
		EditCol == Pos_Aufs_Gh2 ||
		EditCol == Pos_KalkVk_Gh2 ||
		EditCol == Pos_Gh2 ||
		EditCol == Pos_Aufs_Fil_Ek ||
		EditCol == Pos_Kalk_Fil_Ek ||
		EditCol == Pos_Fil_Ek  
		)   //WAL-85
	{
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
		{
			SearchListCtrl.Edit.GetWindowText (cA); 
		}
		else
		{
			GetColValue (EditRow, PosA, cA);
		}
			if (strlen(cA.GetBuffer()) > 0)
			{
				 _tcscpy (Ptabn.ptabn.ptitem, _T("aufschlaege"));
				_stprintf (Ptabn.ptabn.ptwert, _T("%s"), cA);
				if (Ptabn.dbreadfirst () == 100)
				{
					return;
				}
			}
	}

	if (LastCol ())
	{
//		TestAprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;

		if (EditRow == rowCount)
		{
			EditCol = PosA;
		}
		else
		{
//			EditCol = Pos_PrEk;
			EditCol = GetFirstCol();
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
	}
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
	if (m_Mdn == 0)
	{
		if (EditCol == PosFilGr)
		{
			EditCol ++;
		}
		if (EditCol == PosFil)
		{
			EditCol ++;
		}
	}
	if (this->m_Mdn != 0)
	{
		if (EditCol == PosMdnGr) EditCol ++;
		if (EditCol == PosMdn) EditCol ++;
		if (EditCol == PosFilGr)
		{
			EditCol ++;
		}
		if (EditCol == PosFil) 
		{
			EditCol ++;
		}
	}
//	if (EditCol == Pos_BearbKost) //testtest
	while (isDisplayonly(EditCol) && EditRow < rowCount)
	{
		EditCol ++;
		if (LastCol()) break;
	}

	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CCmpPrListCtrl::NextCol ()
{
	CString cA;
	int count = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}

	if (EditCol == PosFil)
	{
		ReadFil ();
	}
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}
	else if (EditCol == PosA)
	{
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
		{
			SearchListCtrl.Edit.GetWindowText (cA); 
		}
		if (StrToDouble (cA) == 0.0) 
		{
			return;
		}

//		ReadA ();
	}
	else if (EditCol == Pos_Aufs_Gh1 ||
		EditCol == Pos_KalkVk_Gh1 ||
		EditCol == Pos_Gh1 ||
		EditCol == Pos_Sp_Gh1 ||
		EditCol == Pos_Aufs_Gh2 ||
		EditCol == Pos_KalkVk_Gh2 ||
		EditCol == Pos_Gh2 ||
		EditCol == Pos_Aufs_Fil_Ek ||
		EditCol == Pos_Kalk_Fil_Ek ||
		EditCol == Pos_Fil_Ek  
		)   //WAL-85
	{
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
		{
			SearchListCtrl.Edit.GetWindowText (cA); 
		}
		else
		{
			GetColValue (EditRow, PosA, cA);
		}
			if (strlen(cA.GetBuffer()) > 0)
			{
				 _tcscpy (Ptabn.ptabn.ptitem, _T("aufschlaege"));
				_stprintf (Ptabn.ptabn.ptwert, _T("%s"), cA);
				if (Ptabn.dbreadfirst () == 100)
				{
					return;
				}
			}
	}

	if (LastCol ())
	{
		TestAprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;

		if (EditRow == rowCount)
		{
			EditCol = PosA;
		}
		else
		{
//			EditCol = Pos_PrEk;
			EditCol = GetFirstCol();
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
	}


	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFilGr)
		{
			EditCol ++;
		}
		if (EditCol == PosFil)
		{
			EditCol ++;
		}
	}
	if (this->m_Mdn != 0)
	{
		if (EditCol == PosMdnGr) EditCol ++;
		if (EditCol == PosMdn) EditCol ++;
		if (EditCol == PosFilGr)
		{
			EditCol ++;
		}
		if (EditCol == PosFil) 
		{
			EditCol ++;
		}
	}
	while (isDisplayonly(EditCol))
	{
		EditCol ++;
		if (LastCol()) break;
	}
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CCmpPrListCtrl::PriorCol ()
{
	CString cA;
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	if (EditCol == PosFil)
	{
		ReadFil ();
	}
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}
	else if (EditCol == PosA)
	{
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
		{
			SearchListCtrl.Edit.GetWindowText (cA); 
		}
		if (StrToDouble (cA) == 0.0) 
		{
			return;
		}
		ReadA ();
	}
	else if (EditCol == Pos_Aufs_Gh1 ||
		EditCol == Pos_KalkVk_Gh1 ||
		EditCol == Pos_Gh1 ||
		EditCol == Pos_Sp_Gh1 ||
		EditCol == Pos_Aufs_Gh2 ||
		EditCol == Pos_KalkVk_Gh2 ||
		EditCol == Pos_Gh2 ||
		EditCol == Pos_Aufs_Fil_Ek ||
		EditCol == Pos_Kalk_Fil_Ek ||
		EditCol == Pos_Fil_Ek  
		)   //WAL-85
	{
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
		{
			SearchListCtrl.Edit.GetWindowText (cA); 
		}
		else
		{
			GetColValue (EditRow, PosA, cA);
		}
			if (strlen(cA.GetBuffer()) > 0)
			{
				 _tcscpy (Ptabn.ptabn.ptitem, _T("aufschlaege"));
				_stprintf (Ptabn.ptabn.ptwert, _T("%s"), cA);
				if (Ptabn.dbreadfirst () == 100)
				{
					return;
				}
			}
	}

	StopEnter ();
	if (EditCol != GetFirstCol()) //160210
	{								
		EditCol --;
		while (isDisplayonly(EditCol)) //160210
		{
			EditCol --;
			if (EditCol == GetFirstCol()) break;
		}
	}
	else
	{
		(EditRow == 0) ? EditRow : EditRow --;
		EditCol = GetLastCol();
	}


	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFil)
		{
			EditCol --;
		}
		if (EditCol == PosFilGr)
		{
			EditCol --;
		}
	}
	if (this->m_Mdn != 0)
	{
		if (EditCol == PosMdn) EditCol --;
		if (EditCol == PosMdnGr) EditCol --;
		if (EditCol == PosFilGr)
		{
			EditCol --;
		}
		if (EditCol == PosFil)
		{
			EditCol --;
		}
	}
    StartEnter (EditCol, EditRow);
}

BOOL CCmpPrListCtrl::InsertRow ()
{
	if (!Neuanlage) return TRUE; //ZuTun erst mal insertRow deaktiviert
	CString PrEk = GetItemText (EditRow, Pos_PrEk);
//	CString PrVk = GetItemText (EditRow, PosPrVk);
/*
	if ((GetItemCount () > 0) && 
		(StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
	{
		return FALSE;
	}
*/
		CString cA;
		GetColValue (EditRow, PosA, cA);
		if (StrToDouble (cA) == 0.0) 
		{
			return TRUE;
		}


	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	cA	    = _T("0 ");
	CString cMdnGr = _T("0 ");
	CString cMdn   = _T("0 ");
	if (m_Mdn != 0)
	{
		Mdn.mdn.mdn = m_Mdn;
		Mdn.dbreadfirst ();
		cMdnGr.Format (_T("%hd "), Mdn.mdn.mdn_gr);
		cMdn.Format (_T("%hd "), Mdn.mdn.mdn);
	}
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (cA.GetBuffer (), EditRow, PosA);
	FillList.SetItemText (cMdnGr.GetBuffer (), EditRow, PosMdnGr);
	FillList.SetItemText (cMdn.GetBuffer (), EditRow, PosMdn);
	FillList.SetItemText (_T("0"), EditRow, PosFilGr);
	FillList.SetItemText (_T("0"), EditRow, PosFil);
	FillList.SetItemText (_T("0,00 "), EditRow, Pos_PrEk);
//	FillList.SetItemText (_T("0,00 "), EditRow, PosPrVk);
	EditCol = PosA;
	StartEnter (EditCol, EditRow,1); //testtesttest
	return TRUE;
}

BOOL CCmpPrListCtrl::DeleteRow ()
{
	BOOL dret;
	if (!IsWindow (m_hWnd)) return FALSE;
	RowDeleted = TRUE;

/***
	StopEnter ();
	DeleteItem (EditRow);
	if (GetItemCount () == 0)
	{
		AppendEmpty ();
		StartEnter (1, 0);
	}
	else if (EditRow == GetItemCount ())
	{
		PriorRow ();
	}
	else
	{
		StartEnter (EditCol, EditRow);
	}
	return TRUE;
**********/

	dret = CEditListCtrl::DeleteRow ();
	RowDeleted = FALSE;
	return dret;
}

BOOL CCmpPrListCtrl::AppendEmpty ()
{
	CString cA;
	if (!Neuanlage)  return FALSE;  //Kein Anf�gen (�ber cfg.. ZuTun
	int rowCount = GetItemCount ();
	if (rowCount > 0)
	{
		cA = GetItemText (EditRow, PosA);
		if (StrToDouble (cA) == 0.0) 
		{
			return FALSE;
		}
	}
	cA = _T("0 ");
	CString cMdnGr = _T("0 ");
	CString cMdn   = _T("0 ");
	if (m_Mdn != 0)
	{
		Mdn.mdn.mdn = m_Mdn;
		Mdn.dbreadfirst ();
		cMdnGr.Format (_T("%hd "), Mdn.mdn.mdn_gr);
		cMdn.Format (_T("%hd "), Mdn.mdn.mdn);
	}
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText (cA.GetBuffer (), rowCount, PosA);
	FillList.SetItemText (cMdnGr.GetBuffer (), rowCount, PosMdnGr);
	FillList.SetItemText (cMdn.GetBuffer (), rowCount, PosMdn);
	FillList.SetItemText (_T("0"),  rowCount, PosFilGr);
	FillList.SetItemText (_T("0"),   rowCount, PosFil);
	FillList.SetItemText (_T("0,00 "), rowCount, Pos_PrEk);
//	FillList.SetItemText (_T("0,00 "), rowCount, PosPrVk);
	rowCount = GetItemCount ();
	return TRUE;
}

void CCmpPrListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CCmpPrListCtrl::FillMdnGrCombo (CVector& Values)
{
	CString *c;
    MdnGrCombo.FirstPosition ();
	while ((c = (CString *) MdnGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnGrCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MdnGrCombo.Add (c);
	}
}


void CCmpPrListCtrl::FillMdnCombo (CVector& Values)
{
	CString *c;
    MdnCombo.FirstPosition ();
	while ((c = (CString *) MdnCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MdnCombo.Add (c);
	}
}

void CCmpPrListCtrl::FillFilGrCombo (CVector& Values)
{
	CString *c;
    FilGrCombo.FirstPosition ();
	while ((c = (CString *) FilGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilGrCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		FilGrCombo.Add (c);
	}
}

void CCmpPrListCtrl::FillFilCombo (CVector& Values)
{
	CString *c;
    FilCombo.FirstPosition ();
	while ((c = (CString *) FilCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		FilCombo.Add (c);
	}
}


void CCmpPrListCtrl::FillFilCombo (int row)
{
	CString V;
	GetColValue (row, PosMdn, V);
	ReadFilCombo (_tstoi (V));
}

void CCmpPrListCtrl::ReadFilCombo (int mdn)
{
	Fil.fil.mdn = mdn;
	if (Fil.fil.mdn == 0) return;
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Mandant"));
	Values.Add (Value);
    Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		Fil.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Fil.fil.fil,
			                          FilAdr.adr.adr_krz);
		Values.Add (Value);
	}
	FillFilCombo (Values);
}


void CCmpPrListCtrl::RunItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
*/
}

void CCmpPrListCtrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CCmpPrListCtrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CCmpPrListCtrl::OnChoice ()
{
	if (EditCol == PosFil)
	{
		OnFilChoice (CString (_T("")));
	}
	if (EditCol == PosFilGr)
	{
		OnFilGrChoice (CString (_T("")));
	}
	else if (EditCol == PosMdn)
	{
		OnMdnChoice (CString (_T("")));
	}
	else if (EditCol == PosMdnGr)
	{
		OnMdnGrChoice (CString (_T("")));
	}
	else if (EditCol == PosA)
	{
		OnAChoice (CString (_T("")));
	}
	else if (EditCol == Pos_Aufs_Gh1 ||
		EditCol == Pos_KalkVk_Gh1 ||
		EditCol == Pos_Gh1 ||
		EditCol == Pos_Sp_Gh1 ||
		EditCol == Pos_Aufs_Gh2 ||
		EditCol == Pos_KalkVk_Gh2 ||
		EditCol == Pos_Gh2 ||
		EditCol == Pos_Aufs_Fil_Ek ||
		EditCol == Pos_Kalk_Fil_Ek ||
		EditCol == Pos_Fil_Ek  
		)   //WAL-85
	{
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
//		OnPtabChoice (CString (_T("")));
		OnPtabChoice (Text);
	}
}

void CCmpPrListCtrl::OnFilChoice (CString& Search)
{
	FilChoiceStat = TRUE;
	CString V;
	GetColValue (EditRow, PosMdn, V);
	Mdn.mdn.mdn = _tstoi (V);
	if (ChoiceFil != NULL && !ModalChoiceFil)
	{
		ChoiceFil->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceFil == NULL)
	{
		ChoiceFil = new CChoiceFil (this);
	    ChoiceFil->IsModal = ModalChoiceFil;
		ChoiceFil->m_Mdn = Mdn.mdn.mdn;
		ChoiceFil->CreateDlg ();
	}

    ChoiceFil->SetDbClass (&Fil);
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	ChoiceFil->m_Mdn = _tstoi (Value.GetBuffer ());
	ChoiceFil->SearchText = Search;
	if (ModalChoiceFil)
	{
			ChoiceFil->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceFil->MoveWindow (&rect);
		ChoiceFil->SetFocus ();
		return;
	}
    if (ChoiceFil->GetState ())
    {
		  CFilList *abl = ChoiceFil->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  FilChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		  memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		  Fil.fil.mdn = Mdn.mdn.mdn;
		  Fil.fil.fil = abl->fil;
		  if (Fil.dbreadfirst () == 0)
		  {
			  FilAdr.adr.adr = Fil.fil.adr;
			  FilAdr.dbreadfirst ();
		  }
		  CString Text;
		  Text.Format (_T("%hd"), Fil.fil.fil);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
		  CString KunName;
//		  KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
//		  FillList.SetItemText (KunName.GetBuffer (), EditRow, PosKunName);
    }
	else
	{
		 FilChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}

void CCmpPrListCtrl::OnFilGrChoice (CString& Search)
{
	CString V;
	GetColValue (EditRow, PosMdn, V);
	Mdn.mdn.mdn = _tstoi (V);
	FilGrChoiceStat = TRUE;
	if (ChoiceFilGr != NULL && !ModalChoiceFilGr)
	{
		ChoiceFilGr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceFilGr == NULL)
	{
		ChoiceFilGr = new CChoiceGrZuord (this);
	    ChoiceFilGr->IsModal = ModalChoiceFilGr;
	    ChoiceFilGr->m_Mdn = Mdn.mdn.mdn;
		ChoiceFilGr->CreateDlg ();
	}

    ChoiceFilGr->SetDbClass (&Fil);
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	ChoiceFilGr->m_Mdn = _tstoi (Value.GetBuffer ());
	ChoiceFilGr->SearchText = Search;
	if (ModalChoiceFilGr)
	{
			ChoiceFilGr->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceFil->MoveWindow (&rect);
		ChoiceFil->SetFocus ();
		return;
	}
    if (ChoiceFilGr->GetState ())
    {
		  CGrZuordList *abl = ChoiceFilGr->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  FilGrChoiceStat = FALSE;
			  return;
		  }
		  CString Text;
		  Text.Format (_T("%hd"), abl->gr);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
		 FilGrChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}


void CCmpPrListCtrl::OnMdnChoice (CString& Search)
{
	MdnChoiceStat = TRUE;
	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

    ChoiceMdn->SetDbClass (&Fil);
//	ChoiceFil->m_Mdn = m_Mdn;
	ChoiceMdn->SearchText = Search;
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  MdnChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  CString Text;
		  Text.Format (_T("%hd"), Mdn.mdn.mdn);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
//		  CString KunName;
//		  KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
//		  FillList.SetItemText (KunName.GetBuffer (), EditRow, PosKunName);
    }
	else
	{
		 MdnChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}

void CCmpPrListCtrl::OnMdnGrChoice (CString& Search)
{
	MdnGrChoiceStat = TRUE;
	if (ChoiceMdnGr != NULL && !ModalChoiceMdnGr)
	{
		ChoiceMdnGr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdnGr == NULL)
	{
		ChoiceMdnGr = new CChoiceGrZuord (this);
	    ChoiceMdnGr->IsModal = ModalChoiceMdnGr;
		ChoiceMdnGr->CreateDlg ();
	}

    ChoiceMdnGr->SetDbClass (&Mdn);
	ChoiceMdnGr->SearchText = Search;
	if (ModalChoiceMdnGr)
	{
			ChoiceMdnGr->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdnGr->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdnGr->MoveWindow (&rect);
		ChoiceMdnGr->SetFocus ();
		return;
	}
    if (ChoiceMdnGr->GetState ())
    {
		  CGrZuordList *abl = ChoiceMdnGr->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  MdnGrChoiceStat = FALSE;
			  return;
		  }
		  CString Text;
		  Text.Format (_T("%hd"), abl->gr);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
		 MdnGrChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}


void CCmpPrListCtrl::OnPtabChoice (CString& Search)
{
	PtabChoiceStat = TRUE;
	if (ChoicePtab != NULL && !ModalChoicePtab)
	{
		ChoicePtab->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoicePtab == NULL)
	{
		ChoicePtab = new CChoicePtab (this);
	    ChoicePtab->IsModal = ModalChoicePtab;
		ChoicePtab->CreateDlg ();
	}

    ChoicePtab->SetDbClass (&A_bas);
	ChoicePtab->SearchText = Search;
    ChoicePtab->Ptitem = _T("aufschlaege");
    ChoicePtab->Caption = _T("Auswahl Aufschl�ge");
    ChoicePtab->bLongBez = TRUE;

	if (ModalChoicePtab)
	{
			ChoicePtab->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoicePtab->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoicePtab->MoveWindow (&rect);
		ChoicePtab->SetFocus ();
		return;
	}
    if (ChoicePtab->GetState ())
    {
		  CPtabList *abl = ChoicePtab->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  PtabChoiceStat = FALSE;
			  return;
		  }
		  CString Text;
		  Text.Format (_T("%s"), abl->ptwert);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
		 PtabChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}


void CCmpPrListCtrl::OnAChoice (CString& Search)
{
	AChoiceStat = TRUE;
	if (ChoiceA != NULL && !ModalChoiceA)
	{
		ChoiceA->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceA == NULL)
	{
		ChoiceA = new CChoiceA (this);
	    ChoiceA->IsModal = ModalChoiceA;
#ifndef ARTIKEL
	    ChoiceA->HideEnter = FALSE;
#endif
	    ChoiceA->HideFilter = FALSE;
		ChoiceA->IdArrDown = IDI_HARROWDOWN;
		ChoiceA->IdArrUp   = IDI_HARROWUP;
		ChoiceA->IdArrNo   = IDI_HARROWNO;
		ChoiceA->CreateDlg ();
	}

    ChoiceA->SetDbClass (&Mdn);
	ChoiceA->SearchText = Search;
	ChoiceA->Where = _T("");
	if (ArtSelect != _T(""))
	{
		ChoiceA->Where = ArtSelect;
	}
	if (ModalChoiceA)
	{
//			ChoiceA->FirstRead = TRUE;
			ChoiceA->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceA->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceA->MoveWindow (&rect);
		ChoiceA->SetFocus ();
		return;
	}
    if (ChoiceA->GetState ())
    {
		  CABasList *abl = ChoiceA->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  AChoiceStat = FALSE;
			  return;
		  }
		  CString Text;
		  Text.Format (_T("%.0lf"), abl->a);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
		 AChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}


void CCmpPrListCtrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}

void CCmpPrListCtrl::ReadA ()
{
	if (EditCol != PosA) return;
    memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
    CString Text;

	if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
	{
		SearchListCtrl.Edit.GetWindowText (Text); 
	}
	if (!CStrFuncs::IsDecimal (Text))
	{
			OnAChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%.0lf"), CStrFuncs::StrToDouble (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!AChoiceStat)
			{
				EditCol --;
				return;
			}
	}
	A_bas.a_bas.a = CStrFuncs::StrToDouble (Text);
	if (A_bas.a_bas.a == 0) 
	{
		EditCol --;
		return;
	}
	if (A_bas.dbreadfirst () != 0)
	{
		MessageBox (_T("Artikel nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return;
	}
	Text.Format (_T("%.0lf  %s"), A_bas.a_bas.a, A_bas.a_bas.a_bz1);
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
			SearchListCtrl.Edit.SetWindowText (Text);
	}
}

void CCmpPrListCtrl::ReadEk_pr ()
{
	if (EditCol != PosA) return;
    CString Text;

	if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
	{
		SearchListCtrl.Edit.GetWindowText (Text); 
	}
	if (!CStrFuncs::IsDecimal (Text))
	{
		return;
	}
	memcpy (&Ek_pr.ek_pr, &ek_pr_null, sizeof (EK_PR));
	Ek_pr.ek_pr.a = CStrFuncs::StrToDouble (Text);
	Ek_pr.ek_pr.mdn = Mdn.mdn.mdn;
	Ek_pr.ek_pr.fil = 0;
	if (Ek_pr.ek_pr.a == 0) 
	{
		return;
	}
	if (Ek_pr.dbreadfirst () != 0)
	{
		memcpy (&Ek_pr.ek_pr, &ek_pr_null, sizeof (EK_PR));
		Ek_pr.ek_pr.a = CStrFuncs::StrToDouble (Text);
		Ek_pr.ek_pr.mdn = Mdn.mdn.mdn;
		Ek_pr.ek_pr.fil = 0;
	}
	if (strcmp (KalkulationsModus, "a_mo_we") == 0)
	{
		Ek_pr.ek_pr.pr_ek = Calculate.GetHkVollk(Ek_pr.ek_pr.mdn,0,Ek_pr.ek_pr.a);
	}


	Ek_pr_to_ItemText(EditRow);

}
void CCmpPrListCtrl::Ek_pr_to_ItemText (int Row)
{
    CString cWert;

	DoubleToString (Ek_pr.ek_pr.pr_ek, cWert,Nachkomma(Pos_PrEk));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_PrEk);

	DoubleToString (Ek_pr.ek_pr.hk_kost, cWert,Nachkomma(Pos_BearbKost));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_BearbKost);

	//Pos_Kalk_Hk;
	DoubleToString (Ek_pr.ek_pr.hk, cWert,Nachkomma(Pos_Kalk_Hk));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Kalk_Hk);

	//Pos_Aufs_Sk;
	DoubleToString (Ek_pr.ek_pr.sk_aufs, cWert,Nachkomma(Pos_Aufs_Sk));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Aufs_Sk);
	//Pos_Kalk_Sk;
	DoubleToString (Ek_pr.ek_pr.sk, cWert,Nachkomma(Pos_Kalk_Sk));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Kalk_Sk);
	//Pos_Aufs_Fil_Ek;
	DoubleToString (Ek_pr.ek_pr.fil_ek_aufs, cWert,Nachkomma(Pos_Aufs_Fil_Ek));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Aufs_Fil_Ek);
	//Pos_Kalk_Fil_Ek;
	DoubleToString (Ek_pr.ek_pr.fil_ek_kalk, cWert,Nachkomma(Pos_Kalk_Fil_Ek));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Kalk_Fil_Ek);
	//Pos_Fil_Ek;
	DoubleToString (Ek_pr.ek_pr.fil_ek, cWert,Nachkomma(Pos_Fil_Ek));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Fil_Ek);
	//Pos_Aufs_Fil_Vk;
	DoubleToString (Ek_pr.ek_pr.fil_vk_aufs, cWert,Nachkomma(Pos_Aufs_Fil_Vk));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Aufs_Fil_Vk);
	//Pos_Kalk_Fil_Vk;
	DoubleToString (Ek_pr.ek_pr.fil_vk_kalk, cWert,Nachkomma(Pos_Kalk_Fil_Vk));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Kalk_Fil_Vk);
	//Pos_Fil_Vk;
	DoubleToString (Ek_pr.ek_pr.fil_vk, cWert,Nachkomma(Pos_Fil_Vk));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Fil_Vk);

	//Pos_Aufs_Gh1;
	DoubleToString (Ek_pr.ek_pr.gh1_aufs, cWert,Nachkomma(Pos_Aufs_Gh1));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Aufs_Gh1);
	//Pos_KalkVk_Gh1;
	DoubleToString (Ek_pr.ek_pr.gh1_kalk_vk, cWert,Nachkomma(Pos_KalkVk_Gh1));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_KalkVk_Gh1);
	//Pos_Gh1;
	DoubleToString (Ek_pr.ek_pr.gh1, cWert,Nachkomma(Pos_Gh1));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Gh1);
	//Pos_Sp_Gh1;
	DoubleToString (Ek_pr.ek_pr.gh1_sp, cWert,Nachkomma(Pos_Sp_Gh1));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Sp_Gh1);

	//Pos_Aufs_Gh2;
	DoubleToString (Ek_pr.ek_pr.gh2_aufs, cWert,Nachkomma(Pos_Aufs_Gh2));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Aufs_Gh2);
	//Pos_KalkVk_Gh2;
	DoubleToString (Ek_pr.ek_pr.gh2_kalk_vk, cWert,Nachkomma(Pos_KalkVk_Gh2));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_KalkVk_Gh2);
	//Pos_Gh2;
	DoubleToString (Ek_pr.ek_pr.gh2, cWert,Nachkomma(Pos_Gh2));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Gh2);
	//Pos_Sp_Gh2;
	DoubleToString (Ek_pr.ek_pr.gh2_sp, cWert,Nachkomma(Pos_Sp_Gh2));
	FillList.SetItemText (cWert.GetBuffer (), Row, Pos_Sp_Gh2);


}

void CCmpPrListCtrl::ReadMdnGr ()
{
	if (EditCol != PosMdnGr) return;
    memcpy (&Gr_zuord.gr_zuord, &gr_zuord_null, sizeof (GR_ZUORD));
    CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnMdnGrChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!MdnGrChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Gr_zuord.gr_zuord.mdn = 0;
	Gr_zuord.gr_zuord.gr = atoi (Text);
	if (Gr_zuord.gr_zuord.gr != 0)
	{
		if (Gr_zuord.dbreadfirst () != 0 && Gr_zuord.gr_zuord.gr != 0l)
		{
			MessageBox (_T("Mandantengruppe nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr, Gr_zuord.gr_zuord.gr_bz1);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
	else
	{
		Text = _T("0");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text = _T("0");
        SetItemText (EditRow, PosMdnGr, Text.GetBuffer ());
	}
}

void CCmpPrListCtrl::ReadMdn ()
{
	if (EditCol != PosMdn) return;
    memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
    CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnMdnChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!MdnChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Mdn.mdn.mdn = atoi (Text);
	if (Mdn.mdn.mdn != 0)
	{
		if (Mdn.dbreadfirst () == 0)
		{
			MdnAdr.adr.adr = Mdn.mdn.adr;
			MdnAdr.dbreadfirst ();
		}
		else if (Mdn.mdn.mdn != 0l)
		{
			MessageBox (_T("Mandant nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Mdn.mdn.mdn, MdnAdr.adr.adr_krz);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text.Format (_T("%hd"), Mdn.mdn.mdn_gr);
        SetItemText (EditRow, PosMdnGr, Text.GetBuffer ());
	}
	else
	{
		Text = _T("0  Zentrale");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
}

void CCmpPrListCtrl::ReadFilGr ()
{
	if (EditCol != PosFilGr) return;
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	if (_tstoi (Value) == 0) return;

    memcpy (&Gr_zuord.gr_zuord, &gr_zuord_null, sizeof (GR_ZUORD));
    CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnFilGrChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!FilGrChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Gr_zuord.gr_zuord.mdn = _tstoi (Value);
	Gr_zuord.gr_zuord.gr = atoi (Text);
	if (Gr_zuord.gr_zuord.gr != 0)
	{
		if (Gr_zuord.dbreadfirst () != 0 && Gr_zuord.gr_zuord.gr != 0l)
		{
			MessageBox (_T("Mandantengruppe nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr, Gr_zuord.gr_zuord.gr_bz1);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
	else
	{
		Text = _T("0");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text = _T("0");
        SetItemText (EditRow, PosFilGr, Text.GetBuffer ());
	}
}


void CCmpPrListCtrl::ReadFil ()
{
	if (EditCol != PosFil) return;
    memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
	CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnMdnChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!FilChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Fil.fil.fil = atoi (Text);
	Text = GetItemText (EditRow, PosMdn);
    Fil.fil.mdn = atoi (Text);
//	if (Fil.fil.mdn == 0l) return;

    CString FilName;
	if (Fil.fil.fil != 0)
	{
		if (Fil.dbreadfirst () == 0)
		{
		  FilAdr.adr.adr = Fil.fil.adr;
		  FilAdr.dbreadfirst ();
		}
		else if (Fil.fil.fil != 0l)
		{
			MessageBox (_T("Filiale nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Fil.fil.fil, FilAdr.adr.adr_krz);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text.Format (_T("%hd"), Fil.fil.fil_gr);
        SetItemText (EditRow, PosFilGr, Text.GetBuffer ());
	}
	else 
	{
		Text = _T("0  Mandant");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
}

void CCmpPrListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	if (col == PosMdn)
	{
		Text = cText.Tokenize (" ", pos);
	}
	else if (col == PosFil)
	{
		Text = cText.Tokenize (" ", pos);
	}
	else
	{
		Text = cText.Trim ();
	}
}

BOOL CCmpPrListCtrl::TestAprIndex ()
{
	int Items = GetItemCount ();
	if (Items <= 1) return FALSE;

	CString Value;
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))	
	{
		SearchListCtrl.Edit.GetWindowText (Value); 
	}
	else
	{
		GetColValue (EditRow, PosA, Value);
	}

	double sA = CStrFuncs::StrToDouble (Value); 
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosA, Value);
		double dA = CStrFuncs::StrToDouble (Value);
		if (dA == sA) 
		{
/***
			INT_PTR ret = MessageBox (_T("Dieser Artikel ist in der Liste schon vorhanden")
				        _T("Eintrag l�schen ?"),
						NULL,
						MB_YESNO | MB_ICONQUESTION); 
            if (ret != IDYES) 
			{
********/
				DeleteItem (EditRow);
				InvalidateRect (NULL);
				if ( i > EditRow) 
				{
					EditRow = i -1;
				}
				else
				{
					EditRow = i ;
				}
				return TRUE;
/***
			}
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
			return;
*****/
		}

	}
    return FALSE;
}

BOOL CCmpPrListCtrl::TestAprIndexM (int EditRow)
{
	int Items = GetItemCount ();
	if (Items <= 1) return TRUE;

	GetEnter ();
	CString PrEk;
	GetColValue (EditRow, Pos_PrEk, PrEk);
//	CString PrVk;
//	GetColValue (EditRow, PosPrVk, PrVk);
	double pr_ek = CStrFuncs::StrToDouble (PrEk);
//	double pr_vk = CStrFuncs::StrToDouble (PrVk);
	if (pr_ek == 0.0) return TRUE;

	CString Value;
	GetColValue (EditRow, PosA, Value);
	double sA = CStrFuncs::StrToDouble (Value);
	GetColValue (EditRow, PosMdnGr, Value);
	short sMdnGr = atoi (Value);
	GetColValue (EditRow, PosMdn, Value);
	short sMdn = atoi (Value);
	GetColValue (EditRow, PosFilGr, Value);
	short sFilGr = atoi (Value);
	GetColValue (EditRow, PosFil, Value);
	short sFil = atoi (Value);
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;

		GetColValue (i, Pos_PrEk, PrEk);
//		GetColValue (i, PosPrVk, PrVk);
		double pr_ek = CStrFuncs::StrToDouble (PrEk);
//		double pr_vk = CStrFuncs::StrToDouble (PrVk);
		if (pr_ek == 0.0) continue;

		GetColValue (i, PosA, Value);
		double dA = CStrFuncs::StrToDouble (Value);
		GetColValue (i, PosMdnGr, Value);
		short lMdnGr = atoi (Value);
		GetColValue (i, PosMdn, Value);
		short lMdn = atoi (Value);
		GetColValue (i, PosFilGr, Value);
		short lFilGr = atoi (Value);
		GetColValue (i, PosFil, Value);
		short lFil = atoi (Value);
		if (dA == sA && lFil == sFil && lFilGr == sFilGr &&
			lMdn == sMdn && lMdnGr == sMdnGr)
		{
			return FALSE;
		}

	}
	return TRUE;
}

void CCmpPrListCtrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}

void CCmpPrListCtrl::ReadFilGrCombo (int mdn)
{

	Gr_zuord.gr_zuord.mdn = mdn;
	if (Gr_zuord.gr_zuord.mdn == 0) return;

	CVector Values;
	Values.Init ();
	CString *V = new CString ();
	V->Format (_T("0"));
	Values.Add (V);
    Gr_zuord.sqlopen (FilGrCursor);
	while (Gr_zuord.sqlfetch (FilGrCursor) == 0)
	{
		Gr_zuord.dbreadfirst ();
		V = new CString ();
		V->Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr,
		                          Gr_zuord.gr_zuord.gr_bz1);
		Values.Add (V);
	}
	FillFilGrCombo (Values);
}

void CCmpPrListCtrl::FillFilGrCombo (int row)
{

	CString Value;
	GetColValue (row, PosMdn, Value);
	ReadFilGrCombo (_tstoi (Value));
}

void CCmpPrListCtrl::Prepare ()
{
	Gr_zuord.sqlin  ((short *) &Gr_zuord.gr_zuord.mdn, SQLSHORT, 0);
	Gr_zuord.sqlout ((short *) &Gr_zuord.gr_zuord.gr, SQLSHORT, 0);
	Gr_zuord.sqlout ((char *) Gr_zuord.gr_zuord.gr_bz1, SQLCHAR, sizeof (Gr_zuord.gr_zuord.gr_bz1));
    FilGrCursor = Gr_zuord.sqlcursor (_T("select gr, gr_bz1 from gr_zuord ")
 									  _T("where mdn = ? ")
									  _T("and gr > 0 ")
								      _T("order by gr"));

	Fil.sqlin ((short *) &Fil.fil.mdn, SQLSHORT, 0);
	Fil.sqlout ((short *) &Fil.fil.fil, SQLSHORT, 0);
	FilAdr.sqlout ((char *) &FilAdr.adr.adr_krz, SQLCHAR, sizeof (FilAdr.adr.adr_krz));
    FilCursor = Fil.sqlcursor (_T("select fil.fil, adr.adr_krz from fil, adr ")
		                       _T("where fil.mdn = ? ")
							   _T("and fil.fil > 0 ")
							   _T("and adr.adr = fil.adr ")
							   _T("order by fil.fil"));

}

