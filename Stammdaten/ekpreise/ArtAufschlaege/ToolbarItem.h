#ifndef _TOOLBAREX_DEF
#define _TOOLBAREX_DEF
#include "ColButton.h"

#define VERT 1
#define TBDOWN 2
#define TBUP 3
#define SPACE 4
#define TBTEXT 5

class CToolbarItem
{
    protected :
        CButton *Button;
        CString Text;
        CString TooltipText;
        int Id;
        HBITMAP Hb;
        int Seperator;
        CWnd *Parent;
        BOOL Active;
    public :
        CToolbarItem ();
        CToolbarItem (int ,HBITMAP, CWnd *);
        CToolbarItem (int, int, CWnd *);
        CToolbarItem (int, CString&, CWnd *);
        ~CToolbarItem ();
        void CreateButton (int, int, int, int);
        BOOL IsCtrl (int);
        BOOL IsTooltipCtrl (HWND, TOOLTIPTEXT *);
        void SetId (int);
        void SetHb (HBITMAP);
        void SetText (CString&);
        void SetSeperator (BOOL);
        void SetParent (CWnd *);
        void SetActive (BOOL);
        void SetTooltipText (char *);
        BOOL GetActive (void);
        int GetId (void);
        BOOL GetSeperator (void);
        CWnd *GetParent (void);
        HBITMAP GetHb (void);
        void GetHbSize (CPoint *);
        CString& GetText (void);
        void Display (HDC);
        void Display (HDC, int, int);
        void CentItem (HDC);
        void DrawSeperator (HDC);
        void DisplayText (HDC);
};
#endif