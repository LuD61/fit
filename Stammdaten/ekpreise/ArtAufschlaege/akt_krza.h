#ifndef _A_AKT_KRZA_DEF
#define _A_AKT_KRZA_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "akt_krz.h"


class AKT_KRZA_CLASS : public AKT_KRZ_CLASS 
{
       private :
               void prepare (void);
               int NewCursor;
       public :
               AKT_KRZ akt_krz;
               AKT_KRZA_CLASS () : AKT_KRZ_CLASS ()
               {
		   NewCursor = -1;		
               }
               int dbreadfirst_new ();   
               int dbread_new ();   
               BOOL operator== (AKT_KRZ&);  
};
#endif
