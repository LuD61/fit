#ifndef _DBRANGE_DEF
#define  _DBRANGE_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include <string>
using namespace std;


#ifndef MAXRANGEVARS
#define MAXRANGEVARS 0x1000
#endif


class RANGEVAR
{
       public :
           char feld[30];
           char *var;
           int typ;
           int len;
       RANGEVAR ()
       {
//           strcpy(feld,"");

           var = NULL;
           typ = 0;
           len = 0;
       }

       void SetFeld (char *var)
       {
           strcpy(this->feld,var);
       }

       char *GetFeld (void)
       {
           return feld;
       }
       void SetVar (char *var)
       {
           this->var = var;
       }

       char *GetVar (void)
       {
           return var;
       }

       void SetTyp (int typ)
       {
           this->typ = typ;
       }

       int GetTyp (void)
       {
           return typ;
       }

       void SetLen (int len)
       {
           this->len = len;
       }

       int GetLen (void)
       {
           return len;
       }
};
class SINGLERANGEVAR
{
       public :
           char var[256];
       SINGLERANGEVAR ()
       {
           strcpy(var,"");
       }

       void SetVar (char *var)
       {
           strcpy(this->var,var);
       }

       char *GetVar (void)
       {
           return var;
       }
};
class SINGLERANGEVAR_AND
{
       public :
           char var[256];
       SINGLERANGEVAR_AND ()
       {
           strcpy(var,"");
       }

       void SetVar (char *var)
       {
           strcpy(this->var,var);
       }

       char *GetVar (void)
       {
           return var;
       }
};

class DB_RANGE 
{
       RANGEVAR  InRange[MAXRANGEVARS];
       SINGLERANGEVAR  InsRange[MAXRANGEVARS];
       SINGLERANGEVAR_AND  InsRange_and[MAXRANGEVARS];
       int     InRangeAnz;
       int     InsRangeAnz;
       int     InsRange_andAnz;
       public :
			   void RangeIn (char *feldname, char *var, int typ, int len);
			   char* clipped (char *str);
               char* SetRange  (char * );
               BOOL PruefeRange  (char * );
			   char FindString[50];
			   char* Sql;
			   char* cRange;
			   char* cdest;
       
               DB_RANGE ()
               {
				   strcpy(FindString,"");
				   Sql = NULL;
				   cdest = NULL;
               }
               ~DB_RANGE ()
               {
			 	   if (Sql != NULL)   free (Sql);
				   if (cRange != NULL)   free (cRange);
				   if (cdest != NULL)   free (cdest);
               }
	   protected :
				char cReplace[500];
			   BOOL ReplaceRange (int i);
			   char* DB_RANGE::Replace(char* strBase, const std::string to_find, const std::string to_replace, BOOL all);
        
};

#endif