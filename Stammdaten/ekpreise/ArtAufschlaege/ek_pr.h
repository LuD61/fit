#ifndef _EK_APR_DEF
#define _EK_APR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct EK_PR {
   double         a;
   short          fil;
   short          mdn;
   short          akt;
   DATE_STRUCT    bearb;
   double         pr_ek;
   double         hk_kost;
   double         hk;
   double         sk_aufs;
   double         sk;
   double         fil_ek_aufs;
   double         fil_ek_kalk;
   double         fil_ek;
   double         fil_vk_aufs;
   double         fil_vk_kalk;
   double         fil_vk;
   double         gh1_aufs;
   double         gh1_kalk_vk;
   double         gh1;
   double         gh1_sp;
   double         gh2_aufs;
   double         gh2_kalk_vk;
   double         gh2;
   double         gh2_sp;
   TCHAR          modif[2];
   short          druck;
};
extern struct EK_PR ek_pr, ek_pr_null;

#line 8 "ek_pr.rh"

class EK_PR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               int dbupdate_e (void);
               EK_PR ek_pr;
               EK_PR_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (EK_PR&);  
};
#endif
