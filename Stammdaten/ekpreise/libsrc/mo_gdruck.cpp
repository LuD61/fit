/* ************************************************************************************** */
/*  Module zum Drucken ueber Windows GDI-Aufrufe                                          */
/*                                                                                        */
/*  erstellt am 16.11.1999                                                                */ 
/*                                                                                        */
/*  Autor  W.Roth                                                                         */
/*                                                                                        */
/* ************************************************************************************** */

#include <windows.h>
#include <stdio.h>
#include <process.h>
#ifdef OLDMASK
#include "wmask.h"
#else
#include "wmaskc.h"
#endif
#include "mo_meld.h"
#include "strfkt.h"
#include "mo_gdruck.h"
#include "mo_draw.h"
#include "Text.h"
#ifdef BIWAK
#include "conf_env.h"
#endif


#define TAB 9

extern HFONT EzCreateFontT (HDC, char *, int,
                            int, int, BOOL);
extern HBRUSH CreateSolidBrushT (COLORREF);
extern HBRUSH CreatePenT (int, int, COLORREF);
extern BOOL DeleteObjectT (HGDIOBJ); 


#define EzCreateFont EzCreateFontT
#define CreateSolidBrush CreateSolidBrushT
#define CreatePen CreatePenT
#define DeleteObject DeleteObjectT


static BMAP Bmap;
static BMAP *BmTab [100];
static char *BmName [100];
static int bmptr = 0;


LONG FAR PASCAL PrintProc(HWND,UINT,WPARAM,LPARAM);

int WindowOk = 0;
extern HANDLE  hMainInst;

static BOOL QuerDruck = FALSE;
static BOOL pBitmap   = FALSE;

static char DeviceName [512] = {""};
static PDEVMODE pDevModeOutput = NULL; 
static PRINTER_INFO_5 pinfo5 [100];
static char szAllPrinters [4096];
static char *PrinterTab [100];
static int printeranz;
static int printerpos;

static BOOL DefaultDevice = FALSE;
static BOOL PrinterSet = FALSE;
static HDC shdc = NULL;

static char *DevFile = NULL;
static BOOL OnlySelect = FALSE;
static BOOL OnlyGdi = FALSE;

void SetOnlyGdi (BOOL b)
{
    OnlyGdi = TRUE;
}

void SetDevFile (char *d)
{
    DevFile = d;
}

void SetSelectOnly (BOOL b)
{
    OnlySelect = b;
}

BOOL GetSelectOnly (void)
{
    return OnlySelect;
}

char FixPrinter [80];


static PRINTER_INFO_2 pinfo2 [30];
static PRINTER_INFO_2 pinfo2s [30];
static PRINTER_DEFAULTS pDefaults;
static char *pers_nam;

static TEXTMETRIC tm;
static HFONT hFont, oldfont;;
static int StdRowHeight;
static int StdRowWidth;

static int PosRowHeight;
static int PosRowWidth;

static int RowHeight;
static int AktRowHeight;
static int BmpRowHeight;
static int MaxRowHeight;
static int PageRows;
static int PageCols;
static int cxPage;
static int cyPage;
static  char *DocName = "PrintFile";
static  DEVMODE DevMode;
static  DEVMODE *pDevModeSelect = NULL;
static  DEVMODE *pDevMode = NULL;
static BOOL DefaultIsQuer = FALSE;
static char *DrkDeviceFile = NULL;

static DOCINFO di = {sizeof (DOCINFO), DocName, NULL};
static char text[] = {"Hello, Printer!"};


mfont printfont = {"Arial", 500, 0, 3, RGB (255, 0, 0),
                                       RGB (0, 255, 255),
                                            1,
                                            NULL};

mfont stdprintfont   = {"Courier New", 120, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

static mfont aktfont  = {"Courier New", 120, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
static mfont fettfont  = {"Courier New", 120, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};


mfont stdprintfont70 = {"Courier New", 120, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont71 = {"Courier New", 120, 100, 0, RGB (255, 0, 0),
                                      RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont72 = {"Courier New", 120, 90, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont73 = {"Courier New", 120, 80, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont74 = {"Courier New", 100, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont75 = {"Courier New", 100, 80, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont76 = {"Courier New", 100, 70, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont77 = {"Courier New", 100, 60, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

mfont stdprintfont78 = {"Courier New", 100, 50, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont79 = {"Courier New", 90, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont100 = {"Courier New", 90, 80, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

mfont stdprintfont101 = {"Courier New", 90, 70, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont102 = {"Courier New", 80, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont103 = {"Courier New", 80, 70, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

mfont stdprintfont104 = {"Courier New", 80, 60, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};


mfont stdprintfont80 = {"Courier New", 120, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont81 = {"Courier New", 100, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont82 = {"Courier New", 90, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont83 = {"Courier New", 80, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

mfont stdprintfont84 = {"Courier New", 70, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

mfont stdprintfont85 = {"Courier New", 60, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};


mfont stdprintfont90 = {"Courier New", 120, 150, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont91 = {"Courier New", 150, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont92 = {"Courier New", 160, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont93 = {"Courier New", 170, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

mfont stdprintfont94 = {"Courier New", 180, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont95 = {"Courier New", 200, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

static int SzTab[] = {70, 71, 72, 73, 74, 75,76, 77, 78, 79, 100, 101, 102, 103, 104,
					  90, 91, 92, 93, 94, 95, 0};

static int SzColTab[] = {70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 100, 101, 102, 103, 104,0};

static mfont *FontTab [] = {
	                        &stdprintfont70,
                            &stdprintfont71, 
                            &stdprintfont72, 
                            &stdprintfont73, 
                            &stdprintfont74, 
                            &stdprintfont75, 
                            &stdprintfont76, 
                            &stdprintfont77, 
                            &stdprintfont78, 
                            &stdprintfont79, 
                            &stdprintfont100, 
                            &stdprintfont101, 
                            &stdprintfont102, 
                            &stdprintfont103, 
                            &stdprintfont104, 
                            &stdprintfont90,
                            &stdprintfont91,
                            &stdprintfont92,
                            &stdprintfont93,
                            &stdprintfont94,
                            &stdprintfont95,
							NULL};
 

static mfont *FontColTab [] = {
	                        &stdprintfont70,
                            &stdprintfont71, 
                            &stdprintfont72, 
                            &stdprintfont73, 
                            &stdprintfont74, 
                            &stdprintfont75, 
                            &stdprintfont76, 
                            &stdprintfont77, 
                            &stdprintfont78, 
                            &stdprintfont79, 
                            &stdprintfont100, 
                            &stdprintfont101, 
                            &stdprintfont102, 
                            &stdprintfont103, 
                            &stdprintfont104, 
							NULL};

static char buffer [512];

mfont *rowprintfont = &stdprintfont90; 

int PageColAnz = 6;
static int PageColTab [6];

static int tabbl = 8;

static char Dname[80];

static BOOL breakprn = FALSE;

void SetPrnBreak (BOOL mode)
{
	     breakprn = mode;
}

void SetRowPrintFont (int font)
{
    int i;

    if (font == 0)
    {
        rowprintfont = &stdprintfont90;
    }

    for (i = 0; SzTab[i] != 0; i ++)
    {
        if (font == SzTab[i])
        {
              rowprintfont = FontTab[i];
              break;
        }
    }
}


BOOL CALLBACK AbortProc (HDC hdcPrn, int iCode)
/**
Abbruch fuer Druck;
**/
{
	      MSG msg;

          while (breakprn == FALSE && PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
		  {
			  TranslateMessage (&msg);
			  DispatchMessage (&msg);
		  }
		  return !breakprn;
}


void SetPrColor (COLORREF *color, char *cfg_v)
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};
	
	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return;
		}
	}


	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}

}


void SetPrFont (char *FontVal)
{
	char *p;
	int i;
	int fnr;
	char *Fname;


	p = strtok (FontVal, "\t ");
	if (p == NULL) return;
    fnr = atoi (p);

	for (i = 0; SzTab[i]; i ++)
	{
		if (SzTab[i] == fnr) break;
	}

	if (SzTab[i] == 0) return;

	p = strtok (NULL, "\t ");
	if (p && strcmp (p, " ") > 0)
	{
		Fname = new char [strlen (p) + 2];
		strcpy (Fname,p);
        FontTab[i]->FontName = Fname;
	}
	p = strtok (NULL, "\t ");
	if (p)
	{
        if (atoi (p) != -1) FontTab[i]->FontHeight = atoi (p);
	}
	p = strtok (NULL, "\t ");
	if (p)
	{
        if (atoi (p) != -1) FontTab[i]->FontWidth = atoi (p);
	}
	p = strtok (NULL, "\t ");
	if (p)
	{
        if (atoi (p) != -1) FontTab[i]->FontAttribute = atoi (p);
	}
	p = strtok (NULL, "\t ");
	if (p)
	{
        if (atoi (p) != -1) SetPrColor (&FontTab[i]->FontColor, p);
	}
	p = strtok (NULL, "\t ");
	if (p)
	{
        if (atoi (p) != -1) SetPrColor (&FontTab[i]->FontBkColor, p);
	}
}

void LoadFont (char *fname)
/**
Neuen Font laden.
**/
{
    FILE *fp;
	char buffer [512];

	fp = fopen (fname, "r");
	if (fp == NULL) return;
	
    while (fgets (buffer, 411, fp))
	{
		if (memcmp (buffer, "FONT", 4) == 0)
		{
                SetPrFont (&buffer[5]);
		}
	}
}
	

void SetpDevMode (DEVMODE *dm)
{
	pDevMode = dm;
}

void SetQuerDefault (BOOL b)
{
     DefaultIsQuer = b;
}

void SetDrkDeviceFile (char *dev)
{
     DrkDeviceFile = dev;
}



char *basname (char *dname)
/**
Basisname ermitteln.
**/
{
	int anz;
//	char pid[10];

	anz = wsplit (dname, "\\");
	if (anz < 1) return ("TEST");
	strcpy (Dname, wort [anz - 1]);
	anz = wsplit (Dname, "."); 
	if (anz < 1) return ("TEST");
	strcpy (Dname, wort [0]);
/*
	sprintf (pid, "%d", getpid ());
	strcat (Dname, pid);
*/
	return Dname;
}
	   

void ChangeTabs (unsigned char *buffer)
/**
Tabulatoren ersetzen.
**/
{
	unsigned char tabbuff [512];
	unsigned char *bf;
	unsigned char *tb;

     
	for (tb = tabbuff, bf = buffer; 
	      *bf; 
		  bf += 1, tb += 1)
	{
		if (*bf == TAB)
		{
			memset (tb, ' ', tabbl);
			tb += tabbl - 1;
		}
		else if (*bf < ' ')
		{
			*tb = ' ';
		}
		else
		{
			*tb = *bf;
		}
	}
	*tb = 0;
	strcpy ((char *) buffer, (char *) tabbuff);
}


void SetGdiPrinter (char *pr)
/**
Druckername fest setzen.
**/
{
	int i;
    GetAllPrinters ();
	for (i = 0; i < printeranz; i ++)
	{
		clipped (pr); 
		if (strcmp (pr, PrinterTab[i]) == 0)
		{
	               PrinterSet = TRUE;
				   strcpy (FixPrinter, pr);
				   break;
		}
	}
}

PDEVMODE SetQuerFormat (LPTSTR szDevice)
/**
Ausgabe auf Querformat setzen.
**/
{
		HANDLE hPrinter;
        DWORD dwNeeded;


		if (pDevModeOutput) free (pDevModeOutput);

	    if (OpenPrinter (szDevice, &hPrinter, NULL) == FALSE)
		{
		  return NULL;
		}
/*
	    if (GetPrinter (hPrinter, 2, (LPBYTE) pinfo2, sizeof (pinfo2), &dwNeeded) == FALSE)
		{
		  ClosePrinter (hPrinter);
		  return NULL;
		}
*/
        dwNeeded =  DocumentProperties(NULL, 
                                 hPrinter, 
                                 szDevice, 
                                 NULL, 
                                 NULL, 
                                 NULL); 
	    pDevModeOutput = (PDEVMODE) malloc (dwNeeded);
	    if (pDevModeOutput == NULL) return NULL;
        dwNeeded =  DocumentProperties(GetActiveWindow (), 
                                 hPrinter, 
                                 szDevice, 
                                 pDevModeOutput, 
                                 NULL, 
                                 DM_OUT_BUFFER); 

	    pDevModeOutput->dmFields = DM_ORIENTATION;
	    pDevModeOutput->dmOrientation = DMORIENT_LANDSCAPE;
        dwNeeded =  DocumentProperties(GetActiveWindow (), 
                                 hPrinter, 
                                 szDevice, 
                                 pDevModeOutput, 
                                 pDevModeOutput, 
                                 DM_OUT_BUFFER | DM_IN_BUFFER); 
	    ClosePrinter (hPrinter);
	    return pDevModeOutput;
}

PDEVMODE GetPrinterDefault (LPTSTR szDevice)
/**
Ausgabe auf Querformat setzen.
**/
{
		HANDLE hPrinter;
        DWORD dwNeeded;


		if (pDevModeOutput) free (pDevModeOutput);

	    if (OpenPrinter (szDevice, &hPrinter, NULL) == FALSE)
		{
		  return NULL;
		}
	    if (GetPrinter (hPrinter, 2, (LPBYTE) pinfo2, sizeof (pinfo2), &dwNeeded) == FALSE)
		{
		  ClosePrinter (hPrinter);
		  return NULL;
		}
        dwNeeded =  DocumentProperties(NULL, 
                                 hPrinter, 
                                 szDevice, 
                                 NULL, 
                                 NULL, 
                                 NULL); 
	    pDevModeOutput = (PDEVMODE) malloc (dwNeeded);
	    if (pDevModeOutput == NULL) return NULL;
        dwNeeded =  DocumentProperties(GetActiveWindow (), 
                                 hPrinter, 
                                 szDevice, 
                                 pDevModeOutput, 
                                 NULL, 
                                 DM_OUT_BUFFER); 
	    ClosePrinter (hPrinter);
		if (pDevModeOutput->dmPaperSize == DMPAPER_A4)
		{
			disp_mess ("A4-Format", 1);
		}
		if (pDevModeOutput->dmPaperSize == DMPAPER_A3)
		{
			disp_mess ("A3-Format", 1);
		}
		if (pDevModeOutput->dmPaperSize == DMPAPER_A5)
		{
			disp_mess ("A5-Format", 1);
		}
	    return pDevModeOutput;
}


BOOL PrinterInfo (char *szDevice)
/**
Informationen ueber Drucker holen.
**/
{
      DWORD dwNeeded;
      HANDLE hPrinter;


	  if (OpenPrinter (szDevice, &hPrinter, NULL) == FALSE)
	  {
		  return FALSE;
	  }
	  if (GetPrinter (hPrinter, 2, (LPBYTE) pinfo2, sizeof (pinfo2), &dwNeeded) == FALSE)
	  {
		  ClosePrinter (hPrinter);
		  return FALSE;
	  }

	  ClosePrinter (hPrinter);
	  return TRUE;
}

void GetEnumPrinters (void)
/**
Druckerinformationen holen.
**/
{
	  int i, j;
      DWORD dwNeeded, dwReturned;

// Alle Drucker  

/*
      EnumPrinters (PRINTER_ENUM_NETWORK, NULL, 1, (LPBYTE) pinfo1,
                        sizeof (pinfo1), &dwNeeded, &dwReturned);
*/

      if (EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 5, (LPBYTE) pinfo5,
                        sizeof (pinfo5), &dwNeeded, &dwReturned) == 0)
      {
		        return;
	  }
	  i = printeranz;
	  printeranz += dwReturned;
      for (j = 0; i < printeranz; i ++, j ++)
	  {
 		        PrinterTab[i] = pinfo5[j].pPrinterName;
	  }
      return;
}


HDC GetPrinterDC (char *szDevice)
/**
Druckerinformationen holen.
**/
{
      PRINTER_INFO_5 pinfo5 [20];
      DWORD dwNeeded, dwReturned;

      if (EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 5, (LPBYTE) pinfo5,
                        sizeof (pinfo5), &dwNeeded, &dwReturned))
      {
          for (int i = 0; pinfo5[i].pPrinterName != 0; i ++)
          {
		      if (strcmp (szDevice, pinfo5[i].pPrinterName) == 0)
              {
                 return CreateDC (NULL, pinfo5[i].pPrinterName, NULL, NULL);
              }
          }
      }
      if (EnumPrinters (PRINTER_ENUM_NAME, NULL, 5, (LPBYTE) pinfo5,
                        sizeof (pinfo5), &dwNeeded, &dwReturned))
      {
          for (int i = 0; pinfo5[i].pPrinterName != 0; i ++)
          {
  		      if (strcmp (szDevice, pinfo5[i].pPrinterName) == 0)
              {
                 return CreateDC (NULL, pinfo5[i].pPrinterName, NULL, NULL);
              }
          }
      }
      return NULL;
}

HDC GetPrinterDC (void)
/**
Druckerinformationen holen.
**/
{
      PRINTER_INFO_5 pinfo5 [20];
      DWORD dwNeeded, dwReturned;

      if (EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 5, (LPBYTE) pinfo5,
                        sizeof (pinfo5), &dwNeeded, &dwReturned))
      {
          return CreateDC (NULL, pinfo5[0].pPrinterName, NULL, NULL);
      }
      if (EnumPrinters (PRINTER_ENUM_NAME, NULL, 5, (LPBYTE) pinfo5,
                        sizeof (pinfo5), &dwNeeded, &dwReturned))
      {
          return CreateDC (NULL, pinfo5[0].pPrinterName, NULL, NULL);
      }
      return NULL;
}

HDC GetPrinterDCini (void)
/**
Printername aus WIN.INI holen.
**/
{
	char szPrinter [80];
	char *szDevice, *szDriver, *szOutput;

	GetProfileString ("windows", "device", ",,,", szPrinter, 80);

	if (NULL != (szDevice = strtok (szPrinter, ",")) &&
		NULL != (szDriver = strtok (NULL,      ",")) &&
		NULL != (szOutput = strtok (NULL,      ",")))
	{
		return CreateDC (szDriver, szDevice, szOutput, pDevMode);
	}
		return 0;
}

void SetPersName (char *pname)
/**
Pers_name setzen.
**/
{
	pers_nam = pname;
}

void SaveDruckerAkt (char *drucker)
/**
fit.akt und drucker.akt temporaer sichern und drucker in 
fit.akt und drucker.akt kopieren. Damit koennen Steuerzeichen fuer 7000f.rsr
festgelegt werden. Beispiel. Fax immer mit Steuerzeichen fuer GDI-Druck bestuecken.
**/
{
    char *frm;
    char source [512];
    char dest [512];

	frm = getenv ("FRMPATH");
	if (frm == NULL) return;

    sprintf (source, "%s\\fit.akt", frm);
    sprintf (dest,   "%s\\fit.sav", frm);
    CopyFile (source, dest, FALSE);

    sprintf (source, "%s\\drucker.akt", frm);
    sprintf (dest,   "%s\\drucker.sav", frm);
    CopyFile (source, dest, FALSE);

    sprintf (source, "%s\\%s", frm, drucker);
    sprintf (dest,   "%s\\fit.akt", frm);
    CopyFile (source, dest, FALSE);
    sprintf (dest,   "%s\\drucker.akt", frm);
    CopyFile (source, dest, FALSE);
}

void RestoreDruckerAkt (void)
/**
Druckerfiles wieder hestellen.
**/
{
    char *frm;
    char source [512];
    char dest [512];

	frm = getenv ("FRMPATH");
	if (frm == NULL) return;

    sprintf (dest, "%s\\fit.akt", frm);
    sprintf (source,   "%s\\fit.sav", frm);
    CopyFile (source, dest, FALSE);

    sprintf (dest, "%s\\drucker.akt", frm);
    sprintf (source,   "%s\\drucker.sav", frm);
    CopyFile (source, dest, FALSE);
}


void CopyGdi (int PageRows)
/**
gdi.cfg in drucker.akt ind pers_nam.ast kopieren.
Dabei aktuelle Seitenlaenge setzen.
**/
{

    char *frm;
    char buffer [256];
    FILE *in;
    FILE *drucker;
    FILE *pers;
    int i;
     
	frm = getenv ("FRMPATH");
	if (frm == NULL) return;

    sprintf (buffer, "%s\\gdi.cfg", frm);
    in = fopen (buffer, "r");
    if (in == NULL) return;

    sprintf (buffer, "%s\\drucker.akt", frm);
    drucker = fopen (buffer, "w");
    if (drucker == NULL) return;
    pers = NULL;
    if (pers_nam != NULL && strcmp (pers_nam, " ") > 0
        && strcmp (pers_nam, "drucker") != 0)
    {
             sprintf (buffer, "%s\\%s.akt", frm, pers_nam);
             pers = fopen (buffer, "w");
    }

    while (fgets (buffer, 256, in))
    {
        fputs (buffer, drucker);
        if (pers != NULL)
        {
                fputs (buffer, pers);
        }
		if (PageRows == 0 || PageCols == 0 || PageColAnz == 0)
		{
			continue;
		}
        for (i = 0; i < PageColAnz; i ++)
		{
            if (PageColTab [i] == 0)
			{
				continue;
			}
		}
        if (memcmp (buffer, ".3;", 3) == 0)
        {
            fgets (buffer, 256, in);
            fprintf (drucker, "%d\n", PageRows);
            if (pers != NULL)
            {
                  fprintf (pers, "%d\n", PageRows);
            }
        }
        else if (memcmp (buffer, ".4;", 3) == 0)
        {
            fgets (buffer, 256, in);
            fprintf (drucker, "%d\n", PageColTab [0]);
            if (pers != NULL)
            {
                  fprintf (pers, "%d\n", PageColTab [0]);
            }
        }
        else if (memcmp (buffer, ".5;", 3) == 0)
        {
            fgets (buffer, 256, in);
            fprintf (drucker, "%d\n", PageRows - 2);
            if (pers != NULL)
            {
                  fprintf (pers, "%d\n", PageRows - 2);
            }
        }
        else if (memcmp (buffer, ".6;", 3) == 0)
        {
            for (i = 0; i < PageColAnz; i ++)
            {
                  fgets (buffer, 256, in);
                  fprintf (drucker, "%d\n", PageColTab [i]);
                  if (pers != NULL)
                  {
                          fprintf (pers, "%d\n", PageColTab [i]);
                  }
            }
        }
    }
    fclose (in);
    fclose (drucker);
    if (pers != NULL)
    {
        fclose (pers);
    }
}

void SetPrinter (char *szDevice)
/**
Aktuellen Drucker setzen.
**/
{
	 char gdiname [512];
	 char *etc;
	 FILE *fp;

#ifndef BIWAK
	 etc = getenv ("FRMPATH");
#else
     etc = getenv ("ETC");
#endif
	 etc = getenv ("FRMPATH");
	 if (etc == NULL) return;

     if (pers_nam == NULL || pers_nam[0] <= ' ')
     {
                        pers_nam = "drucker";
     }
	 sprintf (gdiname, "%s\\%s.gdi", etc, pers_nam);
	 fp = fopen (gdiname, "w");
	 if (fp == NULL) return;
	 fprintf (fp, "%s\n", szDevice);
	 fclose (fp);
     if (DevFile != NULL)
     {
	          sprintf (gdiname, "%s\\%s.gdi", etc, DevFile);
	          fp = fopen (gdiname, "w");
	          if (fp == NULL) return;
	          fprintf (fp, "%s\n", szDevice);
	          fclose (fp);
     }

	 sprintf (gdiname, "%s\\drucker.gdi", etc);
	 fp = fopen (gdiname, "w");
	 if (fp == NULL) return;
	 fprintf (fp, "%s\n", szDevice);
	 fclose (fp);
}

void SetPrinterDevice (DEVMODE *DevMode)
/**
Aktuellen Drucker setzen.
**/
{
	 char gdiname [512];
	 char *etc;
	 FILE *fp;

#ifndef BIWAK
	 etc = getenv ("FRMPATH");
#else
     etc = getenv ("ETC");
#endif
	 if (etc == NULL) return;

     if (pers_nam == NULL || pers_nam[0] <= ' ')
     {
                        pers_nam = "drucker";
     }
	 sprintf (gdiname, "%s\\%s.dev", etc, pers_nam);
	 fp = fopen (gdiname, "w");
	 if (fp == NULL) return;
	 fwrite (DevMode, 1, sizeof (DEVMODE), fp);
     if (strlen (DeviceName) > 0)
     {
         fwrite (DeviceName, 1, strlen (DeviceName), fp);
     }
	 fclose (fp);

     if (DevFile != NULL)
     {
       	 sprintf (gdiname, "%s\\%s.dev", etc, DevFile);
	     fp = fopen (gdiname, "w");
	     if (fp == NULL) return;
	     fwrite (DevMode, 1, sizeof (DEVMODE), fp);
         if (strlen (DeviceName) > 0)
         {
             fwrite (DeviceName, 1, strlen (DeviceName), fp);
         }
	     fclose (fp);
     }


	 sprintf (gdiname, "%s\\drucker.dev", etc);
	 fp = fopen (gdiname, "w");
	 if (fp == NULL) return;
	 fwrite (DevMode, 1, sizeof (DEVMODE), fp);
     if (strlen (DeviceName) > 0)
     {
         fwrite (DeviceName, 1, strlen (DeviceName), fp);
     }
	 fclose (fp);
}


DEVMODE *GetDrkDeviceFile (char *devfile)
{
     static DEVMODE DevMode;
	 char gdiname [512];
	 char *etc;
	 FILE *fp;

     if (strchr (devfile, '\\') == 0)
     {
#ifndef BIWAK
	     etc = getenv ("FRMPATH");
#else
         etc = getenv ("ETC");
#endif
	     if (etc == NULL) return NULL;
	     sprintf (gdiname, "%s\\%s", etc, devfile);
     }
     else
     {
         strcpy (gdiname, devfile);
     }
	 fp = fopen (gdiname, "r");
	 if (fp == NULL) return NULL;
	 int len = sizeof (DEVMODE);
	 fread (&DevMode, 1, sizeof (DEVMODE), fp);
     int bytes = fread (DeviceName, 1, 511, fp);
     DeviceName[bytes] = 0;
	 fclose (fp);
     return &DevMode;
}


void WritePrinter (DEVMODE *DevMode)
/**
Ausgabe auf Querformat setzen.
**/
{
//	 SetPrinter ((char *) DevMode->dmDeviceName);
	 SetPrinter ((char *) DeviceName);
     CopyGdi (GetPageRows (DeviceName));
	 SetPrinterDevice (DevMode);
     SetpDevMode (DevMode);
     if (pDevModeSelect == NULL)
     {
         pDevModeSelect = new DEVMODE;
     }
     memcpy (pDevModeSelect, DevMode, sizeof (DEVMODE));
}

char *GetPrinterName (void)
{
     if (pDevMode == NULL) return NULL;
     if (strlen (DeviceName) > 0)
     {
            return DeviceName;
     }
     return (char *) pDevMode->dmDeviceName;
}

char *GetPrinterDevName (void)
{
     if (pDevModeSelect == NULL) return NULL;
     if (strlen (DeviceName) > 0)
     {
            return DeviceName;
     }
     return (char *) pDevModeSelect->dmDeviceName;
}


char *GetPrinter (char *szDevice)
/**
Aktuellen Drucker setzen.
**/
{
	 char gdiname [512];
	 char *etc;
	 FILE *fp;
     char *sz;

     sz = GetPrinterDevName ();
     if (sz) 
     {
         strcpy (szDevice, sz);
         return szDevice;
     }

#ifndef BIWAK
	     etc = getenv ("FRMPATH");
#else
         etc = getenv ("ETC");
#endif
	 if (etc == NULL) return NULL;

     if (pers_nam == NULL || pers_nam[0] <= ' ')
     {
                        pers_nam = "drucker";
     }
	 sprintf (gdiname, "%s\\%s.gdi", etc, pers_nam);
	 fp = fopen (gdiname, "r");
	 if (fp == NULL) return NULL;
	 if (fgets (szDevice, 80, fp) == NULL) 
	 {
 	      fclose (fp);
		  return NULL;
	 }
	 fclose (fp);
	 cr_weg (szDevice);
	 return szDevice;
}


HDC GetPrinterbyName (char *szDevice)
/**
Printername aus WIN.INI holen.
**/
{
	char szPrinter [80];
	char * szDriver, *szOutput;

	GetProfileString ("devices", szDevice, "", szPrinter, 80);

	szDriver = strtok (szPrinter, ",");
	szOutput = strtok (NULL,      ",");
    if (szDriver == NULL)
    {
             szDriver = "WINSPOOL";
    }
	if (QuerDruck && pDevMode == NULL)
	{
	         return CreateDC (szDriver, szDevice, szOutput, SetQuerFormat (szDevice));
	}
	else
	{
//	         return CreateDC (szDriver, szDevice, szOutput, NULL);
             if (QuerDruck)
             {
                       pDevMode->dmOrientation == DMORIENT_LANDSCAPE;                          

             }
	         return CreateDC (szDriver, szDevice, szOutput, pDevModeSelect);
	}
}

HDC GetPrinterbyNameSet (char *szDevice)
/**
Printername aus WIN.INI holen.
**/
{
	char szPrinter [80];
	char * szDriver, *szOutput;

	GetProfileString ("devices", szDevice, "", szPrinter, 80);

	szDriver = strtok (szPrinter, ",");
	szOutput = strtok (NULL,      ",");
	if (QuerDruck && pDevMode == NULL)
	{
	      return CreateDC (szDriver, szDevice, szOutput, SetQuerFormat (szDevice));
	}
	else
	{
//	      return CreateDC (szDriver, szDevice, szOutput, NULL); 
          if (QuerDruck)
          {
                       pDevMode->dmOrientation == DMORIENT_LANDSCAPE;                          

          }
          return CreateDC (szDriver, szDevice, szOutput, pDevMode);
	}
}


char *GetNextPrinter (void)
/**
Naechsten Druckernamen holen.
Neustart ueber GetAllPrinters oder wenn Ende erreicht.
**/
{
	 char *prptr;
	 
	 if (printerpos == printeranz)
	 {
		 printerpos = 0;
		 return NULL;
	 }
	 prptr = PrinterTab [printerpos];
	 printerpos ++;
	 return prptr;
}


void GetAllPrinters (void)
/**
Alle Printernamen aus WIN.INI holen.
**/
{
	char *prps;

	strcpy (szAllPrinters, "\0\0");
	GetProfileString ("devices", NULL, "", szAllPrinters, sizeof (szAllPrinters));
    prps = szAllPrinters;
	printeranz = 0;
	while (TRUE)
	{
		if (memcmp (prps, "\0\0", 2) == 0)
		{
			break;
		}
		PrinterTab[printeranz] = prps;
		printeranz ++;
		prps += strlen (prps) + 1;
	}
	printerpos = 0;
}


void PrintPage (HDC hdc)
/**
Eine Seite drucken.
**/
{
	HFONT hFont, oldfont;;
	TEXTMETRIC tm;


	Rectangle (hdc, 10, 10, cxPage - 20, cyPage - 20);

	SaveDC (hdc);

	SetMapMode (hdc, MM_ISOTROPIC);
	SetWindowExtEx (hdc, 1000, 1000, NULL);
	SetViewportExtEx (hdc, cxPage / 2, -cyPage / 2, NULL);
	SetViewportOrgEx (hdc, cxPage / 2, cyPage / 2, NULL);

	hFont = SetDeviceFont (hdc, &printfont, &tm);
	oldfont = SelectObject (hdc, hFont);
	
	SetTextAlign (hdc, TA_BASELINE | TA_CENTER);

    SetTextColor (hdc,BLUECOL);
    SetBkColor (hdc,YELLOWCOL);
	TextOut (hdc, 0,0, text, strlen (text));
    DeleteObject (SelectObject (hdc, oldfont)); 
	RestoreDC (hdc, -1);
}

/*
char *SetPrintFont (HDC hdc, char *bn)
/?
Steuerzeichen untersuchen und neuen Font setzen.
?/
{
    int i;
	
	bn += 1;

	for (i = 0; SzTab[i]; i++)
	{
		if (SzTab[i] == *bn)
		{
                 DeleteObject (SelectObject (hdc, oldfont));
	             hFont = SetDeviceFont (hdc, FontTab[i], &tm);
  	             AktRowHeight = (int) (double) ((double) tm.tmHeight * 1.0); 
				 MaxRowHeight= max (MaxRowHeight,AktRowHeight);
	             oldfont = SelectObject (hdc, hFont);
				 break;
		}
	}
	return (bn + 1);
}
*/

HFONT EzCreateFont (HDC hdc, mfont *Font)
{
        spezfont (Font);
        HFONT hFont = EzCreateFont (hdc,
                              "Courier New",
                              100,
                                0,
                                0,
                                TRUE);
        return hFont;
}

/*
Syntax fuer Bitmap '#BITMAP Dateiname [Streckfaktor] [Modus f�r Devicekontex]#'
                     Streckfaktor            0 Faktor zwischen Drucker - und Bilschirmaufl�sung
                                               wird berechnet.
                                           d.d beliebige Dezimalzahl. (z.B. 2.6)                       
                     Modus f�r Devicekontex  0 Bitmap einlesen in Druckerdevice
                                             1 Bitmap einlesen in Bildschirmdevice                                   
*/

void PrintPosBitmap (HDC hdc, char *bn, int x, int y)
{
		int i;
		char buffer [512];
        int anz; 
		POINT ptSize;
		HDC wHdc;
		double fkt = 1.0;
		int dmode = 0;
		int bmpt;

        for (i = 1; bn[i]; i ++)
        {
				 if (bn[i] == '#') break;
				 buffer[i - 1] = bn[i];
		}
		buffer[i - 1] = 0;
		anz = wsplit (buffer, " ");
		if (anz < 2) return;
		strcpy (buffer, wort[1]);
		if (anz > 2)
		{
                    fkt = atoi (wort[2]);
		}
		if (anz > 3)
        {
				    dmode = atoi (wort[3]);
        }
		for (i = 0; i < bmptr; i ++)
		{
				 if (strcmp (BmName[i], buffer) == 0) break;
		}
		if (i == bmptr)
		{
			     BmTab[bmptr] = new BMAP;
			     if (BmTab[bmptr] == NULL) return;
			     BmName[bmptr] = new char [strlen (buffer) + 2];
			     if (BmName[bmptr] == NULL)
				 {
				      delete BmTab[bmptr];
				      return;
				 }
				 bmpt = bmptr;
			     wHdc = GetDC (GetActiveWindow ());
			     if (dmode == 0)
				 {
			         if (BmTab[bmpt]->ReadBitmap (hdc, buffer) == NULL) return;
				 }
			     else
				 {
			         if (BmTab[bmpt]->ReadBitmap (wHdc, buffer) == NULL) return;
				 }
				 if (bmptr < 99) bmptr ++;
        }
		else
		{
				 strcpy (buffer, BmName[i]);
				 bmpt = i;
		}
		if (fkt == 0)
		{
			          BmTab[bmpt]->StrechBitmap (wHdc, hdc, x, y);
		}
		else if (fkt == 1)
		{
		          BmTab[bmpt]->DrawBitmap (hdc, x, y);
		}
		else
		{
			          BmTab[bmpt]->StrechBitmap (hdc, x, y, fkt, fkt);
		}

		ReleaseDC (GetActiveWindow (), wHdc);
		BmTab[bmpt]->BitmapSize (hdc, &ptSize);
}

char *PrintPosText (HDC hdc, char *bn, char *buffer)
/** Text positioniert mit Fontname, Schriftgr��e und Ausrichtung ausgeben.
**/
{
        int i;
        int x, y;
        int Len = 0;
        Text FontName;
        int FontSize;
        int FontAttribute;
        Text Direction;
        Text text;
        Token *token;
        SIZE size;
        mfont mFont  = {"Courier New", 120, 0, 1};

        for (i = 0; *bn != 0; bn += 1, i ++)
        {
            if ((*bn == '#') &&
                (memcmp (bn, "#BITMAP", 7) != 0))
            {
                break;
            }
            buffer[i] = *bn;
        }
        if (*bn != 0)
        {
            bn += 1;
        }

        buffer[i] = 0;

        token = new Token (buffer, ";");
        if (token->GetAnzToken () < 7)
        {
            delete token;
            return bn;
        }

        x = atoi (token->NextToken ()) / 8 * PosRowWidth;
        y = atoi (token->NextToken ()) / 8 * StdRowHeight;
        FontName = token->NextToken ();
        FontSize = atoi (token->NextToken ()) * 10 + 20;
        FontAttribute = atoi (token->NextToken ());
        Direction = token->NextToken ();
        if (Direction[0] == 'C')
        {
            Len = atoi (Direction.SubString (1, 255)) * PosRowWidth;
        }
        text = token->NextToken ();
        delete token;
        if (text.GetLen () == 0)
        {
            return bn;
        }

        if (FontName.GetLen () != 0 && 
            strcmp (FontName.GetBuffer (), "DEFAULT") != 0)
        {
            mFont.FontName = FontName.GetBuffer ();
        }
        if (FontSize != 0)
        {
            mFont.FontHeight = FontSize;
        }
 
        Text Bm = text.SubString (0, 7);
        if (Bm == Text ("#BITMAP"))
        {
            PrintPosBitmap (hdc, text.GetBuffer (), x, y);
        }
        else
        {
            mFont.FontAttribute = FontAttribute;
            HFONT hFont = EzCreateFont (hdc, &mFont);
            HFONT oldfont = SelectObject (hdc, hFont);
            SetBkMode (hdc, TRANSPARENT);
            if (Direction[0] == 'R')
            {
                   GetTextExtentPoint32 (hdc, text.GetBuffer (), text.GetLen (), &size); 
                   x -= size.cx;
            }  
            else if (Direction[0] == 'C' && Len > 0)
            {
                   Len = Len - x;
                   GetTextExtentPoint32 (hdc, text.GetBuffer (), text.GetLen (), &size); 
                   x += max (0,(Len - size.cx) / 2);
            }   
            TextOut (hdc, x, y, text.GetBuffer (), text.GetLen ());
            DeleteObject (SelectObject (hdc, oldfont));
        }
        return bn;
}

char *SetGrPart (HDC hdc, char *bn, int *x, int *y)
/**
Eingebaute Graphikteile bearbeiten.
**/
{
	    SIZE size;
		int len;
		int cx;
		int i;
		char buffer [512];
        int anz; 
		POINT ptSize;
		HDC wHdc;
		double fkt = 1.0;
		int dmode = 0;
		int bmpt;

        GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 
		if (memcmp (bn, "#LINE", 5) == 0)
		{
             len = atoi (&bn[5]);
			 if (len == 0) len = 80;
			 cx = len * size.cx;
             MoveToEx (hdc, *x, *y, NULL);
             LineTo (hdc, *x + cx, *y);
             *x += cx;
			 return (char *) bn + 8;
		}
		if (memcmp (bn, "#P", 2) == 0)
		{
             return PrintPosText (hdc, bn + 2, buffer); 
        }
             
		if (memcmp (bn, "#BITMAP", 7) == 0)
		{
             for (i = 1; bn[i]; i ++)
			 {
				 if (bn[i] == '#') break;
				 buffer[i] = bn[i];
			 }
			 if (bn[i] == 0) return bn + 7;
             bn += (i + 1); 
			 buffer[i] = 0;
			 anz = wsplit (buffer, " ");
			 if (anz < 2) return bn;
			 strcpy (buffer, wort[1]);
			 if (anz > 2)
			 {
                    fkt = atoi (wort[2]);
			 }
			 if (anz > 3)
			 {
				    dmode = atoi (wort[3]);
             }
			 for (i = 0; i < bmptr; i ++)
			 {
				 if (strcmp (BmName[i], buffer) == 0) break;
			 }
			 if (i == bmptr)
			 {
			     BmTab[bmptr] = new BMAP;
			     if (BmTab[bmptr] == NULL) return bn;
			     BmName[bmptr] = new char [strlen (buffer) + 2];
			     if (BmName[bmptr] == NULL)
				 {
				      delete BmTab[bmptr];
				      return bn;
				 }
				 bmpt = bmptr;
			     wHdc = GetDC (GetActiveWindow ());
			     if (dmode == 0)
				 {
			         if (BmTab[bmpt]->ReadBitmap (hdc, buffer) == NULL) return bn;
				 }
			     else
				 {
			         if (BmTab[bmpt]->ReadBitmap (wHdc, buffer) == NULL) return bn;
				 }
				 if (bmptr < 99) bmptr ++;
             }
			 else
			 {
				 strcpy (buffer, BmName[i]);
				 bmpt = i;
			 }

			 if (fkt == 0)
			 {
			          BmTab[bmpt]->StrechBitmap (wHdc, hdc, *x, *y);
			 }
			 else if (fkt == 1)
			 {
			          BmTab[bmpt]->DrawBitmap (hdc, *x, *y);
			 }
			 else
			 {
			          BmTab[bmpt]->StrechBitmap (hdc, *x, *y, fkt, fkt);
			 }

			 ReleaseDC (GetActiveWindow (), wHdc);
			 BmTab[bmpt]->BitmapSize (hdc, &ptSize);
			 *x += (int) (double) ((double) fkt * ptSize.x);
			 BmpRowHeight = (int) (double) ((double) fkt * ptSize.y);
			 return bn;
		}
		return bn;
}

char *SetPrintFont (HDC hdc, char *bn)
/**
Steuerzeichen untersuchen und neuen Font setzen.
**/
{
    int i;

	
	bn += 1;

    if (*bn == (char) 80)
    {
                 DeleteObject (SelectObject (hdc, oldfont));
	             hFont = SetDeviceFont (hdc, &fettfont, &tm);
	             oldfont = SelectObject (hdc, hFont);
                 return (bn + 1);
    }

    if (*bn == (char) 81)
    {
                 DeleteObject (SelectObject (hdc, oldfont));
	             hFont = SetDeviceFont (hdc, &aktfont, &tm);
	             oldfont = SelectObject (hdc, hFont);
                 return (bn + 1);
    }

	for (i = 0; SzTab[i]; i++)
	{
		if (SzTab[i] == *bn)
		{
                 memcpy (&aktfont, FontTab[i], sizeof (mfont));
                 memcpy (&fettfont, FontTab[i], sizeof (mfont));
                 fettfont.FontAttribute = 1;
                 DeleteObject (SelectObject (hdc, oldfont));
	             hFont = SetDeviceFont (hdc, FontTab[i], &tm);
	             oldfont = SelectObject (hdc, hFont);
				 break;
		}
	}
	return (bn + 1);
}



void  PrintTextGr (HDC hdc, int x, int y, char *buffer, int len)
{
	char *bs;
    char *bn;
    char *bstz;
    char *besc;
	char *be;
	char out [512];
    SIZE size;

	RowHeight = max (AktRowHeight, StdRowHeight);
	MaxRowHeight = RowHeight;

	BmpRowHeight = 0;
	be = buffer + len;
	bs = buffer;
	besc = strchr (bs, (char) 27);
	bstz = strchr (bs, '#');
	if (besc == NULL && bstz == NULL)
	{
  	    ChangeTabs ((unsigned char *) bs);
		TextOut (hdc, x, y, bs, strlen (bs));
		return;
	}

	if (bstz && besc)
	{
	     bn = min (bstz, besc);
	}
	else if (bstz)
	{
		 bn = bstz;
	}
	else if (besc)
	{
		 bn = besc;
	}
	else
	{
		 bn = 0;
	}
		 
	while (bs < be)
	{
		if (bs < bn)
		{
			memcpy (out, bs, (int) (bn - bs));
			out [(int) (bn - bs)]= 0; 
 	  	    ChangeTabs ((unsigned char *) out);
		    TextOut (hdc, x, y, out, strlen (out));
            GetTextExtentPoint (hdc, out, strlen (out), &size);
            x += size.cx;
// 			x += strlen (out) * StdRowWidth;
            
//			x += strlen (out) * tm.tmAveCharWidth;
		}
		if (bn)
		{
		    if (bn == besc)
			{
		          bs = SetPrintFont (hdc, bn);
			}
		    else if (bn == bstz)
			{
		          bs = SetGrPart (hdc, bn, &x, &y);
				  if (bn == bs) 
				  {
					  memcpy (out, bs, 1);
					  out[1] = 0;
		              TextOut (hdc, x, y, out, strlen (out));
                      GetTextExtentPoint (hdc, out, strlen (out), &size);
                      x += size.cx;
					  bs += 1;
				  }
			}
		}
		if (AktRowHeight > RowHeight) RowHeight = AktRowHeight;
	    besc = strchr (bs, (char) 27);
	    bstz = strchr (bs, '#');
	    if (besc == NULL && bstz == NULL)
		{
  	        ChangeTabs ((unsigned char *) bs);
		    TextOut (hdc, x, y, bs, strlen (bs));
//			if (AktRowHeight > RowHeight) RowHeight = AktRowHeight;
		    return;
		}
	    if (bstz && besc)
		{
	        bn = min (bstz, besc);
		}
	    else if (bstz)
		{
		    bn = bstz;
		}
	    else if (besc)
		{
		    bn = besc;
		}
	    else
		{
		    bn = 0;
		}
	}
//	if (AktRowHeight > RowHeight) RowHeight = AktRowHeight;
}


void  PrintText (HDC hdc, int x, int y, char *buffer, int len)
/**
Text schreiben, vorher auf Steuerzeichen pruefen..
**/ 
{
	char *bs;
    char *bn;
	char *be;
	char out [512];

	RowHeight = max (AktRowHeight, StdRowHeight);
	MaxRowHeight = RowHeight;

	be = buffer + len;
	bs = buffer;
	if ((bn = strchr (bs, (char) 27)) == NULL)
	{
  	    ChangeTabs ((unsigned char *) bs);
		TextOut (hdc, x, y, bs, strlen (bs));
		return;
	}
	while (bs < be)
	{
		if (bs < bn)
		{
			memcpy (out, bs, (int) (bn - bs));
			out [(int) (bn - bs)]= 0; 
 	  	    ChangeTabs ((unsigned char *) out);
		    TextOut (hdc, x, y, out, strlen (out));
			x += strlen (out) * tm.tmAveCharWidth;
		}
		bs = SetPrintFont (hdc, bn);
	    if ((bn = strchr (bs, (char) 27)) == NULL)
		{
  	        ChangeTabs ((unsigned char *) bs);
		    TextOut (hdc, x, y, bs, strlen (bs));
		    return;
		}
	}
}



void FormFeed (HDC hdc)
/**
Seite auf Drucker ausgeben.
**/
{
      EndPage (hdc);
}


void PrintBmPages (HDC hdc, char *dname) 
/**
Bitmap-Seiten drucken.
**/
{
	HBITMAP hBitmap;

	hBitmap = Bmap.ReadBitmap (hdc, dname);
	if (hBitmap == NULL)
	{
		pBitmap = FALSE;
		return;
	}
    if (StartPage (hdc) == 0) return;
	Bmap.DrawCompatibleBitmap (hdc, hBitmap, 0, 0);
    EndPage (hdc);
}	   


void SetPageColsTab (HDC hdc, int cxPage)
{
    int i;
    SIZE size;
    HFONT oldfont;

    for (i = 0; i < PageColAnz; i ++)
    {
 	       hFont = SetDeviceFont (hdc, FontTab [i], &tm);
           oldfont = SelectObject (hdc, hFont);
           GetTextExtentPoint32 (hdc, "X", 1, &size);
           PageColTab [i] = (int) (double) ((double) cxPage / size.cx);
           DeleteObject (hFont);
    }
    SelectObject (hdc, oldfont); 
}


void SetPageRows (int Rows)
/**
Seitenlaenge fuer gewaehlten Drucker setzen.
**/
{
     PageRows = Rows;
}

int GetPageRows (char *Printer)
/**
Seitenlaenge fuer gewaehlten Drucker holen.
**/
{
     HDC hdc;
     int RowHeight;
     int PageRows;
     int cxPage;
     int cyPage;
     PDEVMODE pDevMode;
     SIZE size;
     HFONT oldfont;

     if (Printer == NULL)
     {
               Printer = GetPrinterName ();
     }

     if (Printer == NULL)
     {
                return 0;
     }

     if (DefaultDevice)
     {
               hdc = GetPrinterbyNameSet (Printer);
               pDevMode = GetPrinterDefault (Printer);
 	           SetPrinterDevice (pDevMode);
     }
     else
     {
               hdc = GetPrinterbyName (Printer);
     }

     if (hdc == NULL) return 0;
     cxPage = GetDeviceCaps (hdc, HORZRES);
     cyPage = GetDeviceCaps (hdc, VERTRES);
	 hFont = SetDeviceFont (hdc, rowprintfont, &tm);
     oldfont = SelectObject (hdc, hFont);
     GetTextExtentPoint32 (hdc, "X", 1, &size);
	 RowHeight = (int) (double) ((double) tm.tmHeight * 1.0); 
     PageRows = (int) (double) ((double) cyPage / RowHeight);
     SelectObject (hdc, (HFONT) DeleteObject (hFont));

	 hFont = SetDeviceFont (hdc, &stdprintfont70, &tm);
     oldfont = SelectObject (hdc, hFont);
     GetTextExtentPoint32 (hdc, "X", 1, &size);
     SelectObject (hdc, oldfont);
     PageCols = (int) (double) ((double) cxPage / size.cx);
     SelectObject (hdc, (HFONT) DeleteObject (hFont));
     SetPageColsTab (hdc, cxPage);
     return PageRows;
}

int GetPageStdCols (char *Printer)
/**
Seitenlaenge fuer gewaehlten Drucker holen.
**/
{
     HDC hdc;
     int RowHeight;
     int PageRows;
     int cxPage;
     int cyPage;
     PDEVMODE pDevMode;
     SIZE size;
     HFONT oldfont;

     if (Printer == NULL)
     {
               Printer = GetPrinterName ();
     }

     if (Printer == NULL)
     {
                return 0;
     }

     if (DefaultDevice)
     {
               hdc = GetPrinterbyNameSet (Printer);
               pDevMode = GetPrinterDefault (Printer);
 	           SetPrinterDevice (pDevMode);
     }
     else
     {
               hdc = GetPrinterbyName (Printer);
     }

     if (hdc == NULL) return 0;
     cxPage = GetDeviceCaps (hdc, HORZRES);
     cyPage = GetDeviceCaps (hdc, VERTRES);
	 hFont = SetDeviceFont (hdc, rowprintfont, &tm);
     oldfont = SelectObject (hdc, hFont);
     GetTextExtentPoint32 (hdc, "X", 1, &size);
	 RowHeight = (int) (double) ((double) tm.tmHeight * 1.0); 
     PageRows = (int) (double) ((double) cyPage / RowHeight);
     SelectObject (hdc, (HFONT) DeleteObject (hFont));

	 hFont = SetDeviceFont (hdc, &stdprintfont70, &tm);
     oldfont = SelectObject (hdc, hFont);
     GetTextExtentPoint32 (hdc, "X", 1, &size);
     SelectObject (hdc, oldfont);
     PageCols = (int) (double) ((double) cxPage / size.cx);
     SelectObject (hdc, (HFONT) DeleteObject (hFont));
     SetPageColsTab (hdc, cxPage);
     return PageCols;
}


int GetRowWidthStz (int len)
{
     HDC hdc;
     int cxPage;
     int cyPage;
     PDEVMODE pDevMode;
     SIZE size;
     HFONT oldfont;
     int i;
     char *Printer;
     int MaxCols;
     int MaxPos;

     
     Printer = GetPrinterName ();
     if (Printer == NULL)
     {
                return 0;
     }

     if (DefaultDevice)
     {
               hdc = GetPrinterbyNameSet (Printer);
               pDevMode = GetPrinterDefault (Printer);
 	           SetPrinterDevice (pDevMode);
     }
     else
     {
               hdc = GetPrinterbyName (Printer);
     }
     if (hdc == NULL) return 0;
     cxPage = GetDeviceCaps (hdc, HORZRES);
     cyPage = GetDeviceCaps (hdc, VERTRES);

     MaxCols = 0;
     MaxPos = 0;
     for (i = 0; FontColTab[i] != NULL; i ++)
     {
	       hFont = SetDeviceFont (hdc, FontColTab[i], &tm);
           oldfont = SelectObject (hdc, hFont);
           GetTextExtentPoint32 (hdc, "X", 1, &size);
           SelectObject (hdc, oldfont);
           PageCols = (int) (double) ((double) cxPage / size.cx);
           SelectObject (hdc, (HFONT) DeleteObject (hFont));
           if (PageCols > len)
           {
               return SzColTab[i];
           }
           if (PageCols > MaxCols)
           {
               MaxCols = PageCols;
               MaxPos = i;
           }
     }
     return SzColTab[MaxPos];
}

/*
void PrintPages (HDC hdc, FILE *fp) 
{
	int Row;
	int	y;
	SIZE size;
	  
	hFont = SetDeviceFont (hdc, &stdprintfont, &tm);
	RowHeight = (int) (double) ((double) tm.tmHeight * 1.0); 
    StdRowHeight = AktRowHeight = MaxRowHeight = RowHeight;
    PageRows = (int) (double) ((double) cyPage / RowHeight) - 3;
	oldfont = SelectObject (hdc, hFont);
    GetTextExtentPoint32 (hdc, (char *) "X", 1 , &size); 

   
	Row = 1;
	y = MaxRowHeight;
    if (StartPage (hdc) == 0) return;
	while (fgets (buffer, 511, fp))
	{
		cr_weg (buffer); 
		if (Row == PageRows)
		{
			FormFeed (hdc);
			Row = 1;
		}

		if (strcmp (buffer, "#FF") == 0)
		{
			 FormFeed (hdc);
			 Row = 1;
	         y = MaxRowHeight;
			 continue;
		}

/?
		if (memcmp (buffer, "#LINE", 5) == 0)
		{
             len = atoi (&buffer[5]);
			 if (len == 0) len = 80;
			 cx = len * size.cx;
             MoveToEx (hdc, size.cx, y, NULL);
             LineTo (hdc, cx, y);
		}
		else
?/
		{
 	         PrintText (hdc, size.cx, y, buffer, strlen (buffer));
		}
		if (BmpRowHeight > MaxRowHeight)
		{
	          y += BmpRowHeight;
		}
		else
		{
	          y += MaxRowHeight;
		}
		Row ++;
	}
    EndPage (hdc);
	DeleteObject (SelectObject (hdc, oldfont));
}
*/	

void PrintPages (HDC hdc, FILE *fp) 
/**
Alle Seiten Drucken.
**/
{
	int Row;
    SIZE size;
	  
	hFont = SetDeviceFont (hdc, rowprintfont, &tm);
    SelectObject (hdc, hFont);
	RowHeight = (int) (double) ((double) tm.tmHeight * 1.0); 
    GetTextExtentPoint32 (hdc, "X", 1, &size);
    tm.tmAveCharWidth = size.cx;
    StdRowWidth = size.cx;
    StdRowHeight = AktRowHeight = MaxRowHeight = RowHeight;
    PageRows = (int) (double) ((double) cyPage / RowHeight);
	oldfont = SelectObject (hdc, hFont);

	hFont = SetDeviceFont (hdc, &stdprintfont, &tm);
    DeleteObject (SelectObject (hdc, hFont)); 
    GetTextMetrics (hdc, &tm);
    PosRowHeight = tm.tmHeight;
    GetTextExtentPoint32 (hdc, "X", 1, &size);
    PosRowWidth = size.cx;
   
	Row = 1;
    if (StartPage (hdc) <= 0) return;
	while (fgets (buffer, 511, fp))
	{
		cr_weg (buffer); 
        if (memcmp (buffer, "#S", 2) == 0)
        {
            char *p = strchr (&buffer[2], '#');
            if (p == NULL)
            {
                continue;
            }
            *p = 0;
            Row = atoi (&buffer[2]);
            if (*(p + 1) == 0)
            {
                continue;
            }
            p += 1;
            strcpy (buffer , p);
        }
		if (Row == PageRows - 2)
		{
			FormFeed (hdc);
			Row = 1;
		}

 	    PrintTextGr (hdc, StdRowWidth, Row * MaxRowHeight, buffer, strlen (buffer));
		Row ++;
	}
    EndPage (hdc);
	DeleteObject (SelectObject (hdc, oldfont));
}
	
	   
	   
int GdruckDC (char *dname)
/**
Die Datei dname �ber Window-Funktionen drucken.
**/
{
	  HDC hdc;
	  FILE *fp;

	  hdc = shdc;
	  DocName = basname (dname);
	  fp = fopen (dname, "r");
	  if (fp == NULL)
	  {
		  if (WindowOk)
		  {
		              print_mess (2, "%s kann nicht ge�ffnet werden", dname);
		  }
		  shdc = NULL;
		  return -1;
	  }

	  SetAbortProc (hdc, (ABORTPROC) AbortProc);
	  cxPage = GetDeviceCaps (hdc, HORZRES);
	  cyPage = GetDeviceCaps (hdc, VERTRES);

      di.lpszDocName = DocName;
	  if (StartDoc (hdc, &di) == 0) 
	  {
          pBitmap = FALSE;
		  return -1;
	  }

	  if (pBitmap)
	  {
		   fclose (fp);
	       PrintBmPages (hdc, dname);
	  }
	  else
	  {
	       PrintPages (hdc, fp);
	  }

	  EndDoc (hdc);
	  DeleteDC (hdc);
      pBitmap = FALSE;
	  return 0;
}


int Gdruck (char *dname)
/**
Die Datei dname �ber Window-Funktionen drucken.
**/
{
      HDC hdc;
	  FILE *fp;
	  char szDevice [80];

	  if (shdc)
	  {
		  int ret = GdruckDC (dname);
          return ret;
/*
          if (ret != -1)
          {
              return ret;
          }
*/
	  }

	  DocName = basname (dname);
	  fp = fopen (dname, "r");
	  if (fp == NULL)
	  {
		  if (WindowOk)
		  {
		              print_mess (2, "%s kann nicht ge�ffnet werden", dname);
		  }
          pBitmap = FALSE;
		  return -1;
	  }


	  if (PrinterSet)
	  {
	      hdc = GetPrinterbyName (FixPrinter);
		  if (hdc == NULL)
		  {
                hdc = GetPrinterDC (FixPrinter);
		  }
	  }
	  else if (GetPrinter (szDevice))
	  {

          hdc = GetPrinterbyName (szDevice);
		  if (hdc == NULL)
		  {
                hdc = GetPrinterDC (szDevice);
		  }
	  }
	  else
	  {
	      hdc = GetPrinterDCini ();
	  }

	  if (hdc == NULL) 
	  {
          pBitmap = FALSE;
		  return -1;
	  }

	  SetAbortProc (hdc, (ABORTPROC) AbortProc);

	  cxPage = GetDeviceCaps (hdc, HORZRES);
	  cyPage = GetDeviceCaps (hdc, VERTRES);

      di.lpszDocName = DocName;
	  if (StartDoc (hdc, &di) == 0) 
	  {
          pBitmap = FALSE;
		  return -1;
	  }

	  if (pBitmap)
	  {
		   fclose (fp);
	       PrintBmPages (hdc, dname);
	  }
	  else
	  {
	       PrintPages (hdc, fp);
	  }

	  EndDoc (hdc);
	  DeleteDC (hdc);
      pBitmap = FALSE;
	  return 0;
}


int PrintBitmap (char *dname)
/**
Bitmap drucken.
**/
{
	     pBitmap = TRUE;
		 return Gdruck (dname);
}
	     


BOOL SetPage (void)
/**
Seite Einrichten.
**/
{
 	   
         PAGESETUPDLG pgdlg;
		 DEVMODE *pDevMode;
		 DEVMODE *pDevModeOutput;


		 pgdlg.hDevMode    = NULL; 

         if (DefaultIsQuer)
         {
		          pgdlg.hDevMode = GlobalAlloc (LMEM_MOVEABLE, sizeof DEVMODE);
                  pDevModeOutput = (DEVMODE *) GlobalLock (pgdlg.hDevMode);

    	          pDevModeOutput->dmFields = DM_ORIENTATION;
 	              pDevModeOutput->dmOrientation = DMORIENT_LANDSCAPE;
                  GlobalUnlock (pgdlg.hDevMode); 
         }

		 SetpDevMode (NULL);
		 pgdlg.lStructSize = sizeof (PAGESETUPDLG);
		 pgdlg.hwndOwner   = NULL; 
		 pgdlg.hDevNames   = NULL; 
		 pgdlg.Flags       = NULL; 
		 pgdlg.ptPaperSize.x = 0;
		 pgdlg.ptPaperSize.y = 0;

		 pgdlg.rtMinMargin.left   = 0; 
		 pgdlg.rtMinMargin.top    = 0; 
		 pgdlg.rtMinMargin.right  = 0; 
		 pgdlg.rtMinMargin.bottom = 0; 

		 pgdlg.rtMargin.left   = 0; 
		 pgdlg.rtMargin.top    = 0; 
		 pgdlg.rtMargin.right  = 0; 
		 pgdlg.rtMargin.bottom = 0; 

		 pgdlg.hInstance   = hMainInst; 
		 pgdlg.lCustData   = NULL; 
		 pgdlg.lpfnPageSetupHook  
			               = NULL;
		 pgdlg.lpfnPagePaintHook  
			               = NULL;
		 pgdlg.lpPageSetupTemplateName 
			               = NULL;
		 pgdlg.hPageSetupTemplate
			               = NULL;
/*
		 if (PageSetupDlg(&pgdlg) == 0)
		 {
			 return FALSE;;
		 }
		 memcpy (&DevMode, (DEVMODE *) GlobalLock (pgdlg.hDevMode), 
			     sizeof (DEVMODE));
		 SetGdiPrinter ((char *) DevMode.dmDeviceName);
		 SetpDevMode (&DevMode);
 		 shdc = CreateDC ("WINSPOOL", (char *) DevMode.dmDeviceName, NULL, pDevMode);
*/
		 if (PageSetupDlg(&pgdlg))
         {
                    pDevMode           = (DEVMODE *) GlobalLock (pgdlg.hDevMode);
                    DEVNAMES *pDevNames = (DEVNAMES *) GlobalLock (pgdlg.hDevNames);

/*
                    if (pDevMode->dmOrientation == DMORIENT_LANDSCAPE)
                    {
                        QuerDruck = TRUE;
                    }
*/
                    if (DefaultIsQuer)
                    {
//                        QuerDruck = TRUE;
                          pDevMode->dmOrientation == DMORIENT_LANDSCAPE;                          
                    }
                    else
                    {
                        QuerDruck = FALSE;
                    }
                    memcpy (&DevMode, pDevMode, sizeof (DEVMODE));
                    WritePrinter (pDevMode);
         }
		 return TRUE;
}

BOOL SelectPrinter (void)
/**
Drucker selektieren.
**/
{
         PRINTDLG prdlg;
		 DEVMODE *pDevMode;
//		 DEVMODE *pDevModeOutput;
         char FileName [512]; 


         if (DrkDeviceFile != NULL)
         {
                  pDevMode = GetDrkDeviceFile (DrkDeviceFile);
                  if (pDevMode)
                  {
                    if (DefaultIsQuer)
                    {
                        QuerDruck = TRUE;
                    }
                    else if (pDevMode->dmOrientation == DMORIENT_LANDSCAPE)
                    {
                        QuerDruck = TRUE;
                    }
                    else
                    {
                         QuerDruck = FALSE;
                    }
                    memcpy (&DevMode, pDevMode, sizeof (DEVMODE));
            		SetpDevMode (&DevMode);
                    if (pDevModeSelect == NULL)
                    {
                         pDevModeSelect = new DEVMODE;
                    }
                    memcpy (pDevModeSelect, &DevMode, sizeof (DEVMODE));
                    return TRUE;
                  }
          }

          if (DevFile != NULL && OnlySelect == FALSE)
          {
                  sprintf (FileName, "%s.dev", DevFile);
                  pDevMode = GetDrkDeviceFile (FileName);
                  if (pDevMode)
                  {
                    if (pDevMode->dmOrientation == DMORIENT_LANDSCAPE)
                    {
                        QuerDruck = TRUE;
                    }
                    else
                    {
                        QuerDruck = FALSE;
                    }
                    memcpy (&DevMode, pDevMode, sizeof (DEVMODE));
            		SetpDevMode (&DevMode);
                    if (pDevModeSelect == NULL)
                    {
                         pDevModeSelect = new DEVMODE;
                    }
                    memcpy (pDevModeSelect, &DevMode, sizeof (DEVMODE));
                    return TRUE;
                  }
         }

           
		 prdlg.hDevMode    = NULL; 
/*
         if (DefaultIsQuer)
         {
		          prdlg.hDevMode = GlobalAlloc (LMEM_MOVEABLE, sizeof DEVMODE);
                  pDevModeOutput = (DEVMODE *) GlobalLock (prdlg.hDevMode);

 	              pDevModeOutput->dmOrientation = DMORIENT_LANDSCAPE;
                  GlobalUnlock (prdlg.hDevMode); 
         }
*/

         SetpDevMode (NULL);

		 shdc = NULL;
		 prdlg.lStructSize = sizeof (PRINTDLG);
		 prdlg.hwndOwner   = NULL; 
		 prdlg.hDevNames   = NULL; 
		 prdlg.hDC         = NULL; 
		 prdlg.Flags       = PD_RETURNDC; 
		 prdlg.nFromPage   = NULL;
		 prdlg.nToPage     = NULL;
		 prdlg.nMinPage    = NULL;
		 prdlg.nMaxPage    = NULL;
		 prdlg.nCopies    = 1;

		 prdlg.hInstance   = hMainInst; 
		 prdlg.lCustData   = NULL; 
		 prdlg.lpfnPrintHook  
			               = NULL;
		 prdlg.lpfnSetupHook 
			               = NULL;
		 prdlg.lpPrintTemplateName
			               = NULL;
		 prdlg.lpSetupTemplateName
			               = NULL;
		 prdlg.hPrintTemplate
			               = NULL;
		 prdlg.hSetupTemplate
			               = NULL;


         if (PrintDlg (&prdlg))
		 {
		            pDevMode = (DEVMODE *) GlobalLock (prdlg.hDevMode);
                    DEVNAMES *pDevNames = (DEVNAMES *) GlobalLock (prdlg.hDevNames);
                    char *prDevice = (char *) pDevNames;
                    prDevice += pDevNames->wDeviceOffset;
                    strcpy (DeviceName, prDevice);
/*
                    if (pDevMode->dmOrientation == DMORIENT_LANDSCAPE)
                    {
                        QuerDruck = TRUE;
                    }
*/
                    if (DefaultIsQuer)
                    {
                          pDevMode->dmOrientation = DMORIENT_LANDSCAPE;                          
//                        QuerDruck = TRUE;
                    }
                    else
                    {
                        QuerDruck = FALSE;
                    }
                    memcpy (&DevMode, pDevMode, sizeof (DEVMODE));
		            WritePrinter (pDevMode);
		 }
         else
         {
             GlobalUnlock (prdlg.hDevMode); 
             GlobalFree (prdlg.hDevMode); 
             return FALSE;
         }

		 SetpDevMode (&DevMode);
         GlobalUnlock (prdlg.hDevMode); 
         GlobalFree (prdlg.hDevMode); 
		 return TRUE;
}


int IsGdiPrint (void)
/**
Test, ob im Windows-GDI-Modus gedruckt wird.
**/
{

	      char *gdi;

          if (OnlyGdi)
          {
              return 1;
          }

		  gdi = getenv_default ("GDI_PRINT");
		  if (gdi)
		  {
			  return atoi (gdi);
		  }

		  return 1;
}

static void RegisterPrinter (void)
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered) return;

        registered = 1;
        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  PrintProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hMainInst;
        wc.hIcon         =  LoadIcon (hMainInst, "ROSIICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "PrintWindow";

        RegisterClass(&wc);

}

static char AktPrinter[80];

static ITEM iAktPrinter ("", AktPrinter, "", 0);

static field _fprinter[] =
{&iAktPrinter, 39, 0, 1, 1, 0, "", EDIT, 0, 0, 0};

static form fprinter = {1, 0, 0, _fprinter, 0, 0, 0, 0, NULL}; 
  

void ShowGdiPrinters (void)
/**
Drucker in Listbox anzeigen.
**/
{
	     HDC hdc; 
	     HFONT hFont, oldfont;
		 int x, y, cx, cy;
		 HWND lMainWindow, ListWindow;
		 int wsize;
		 int i;
		 int idx;
		 
         RegisterPrinter ();
		 
		 SetEnvFont ();
		 GetAllPrinters ();
         AktPrinter[0] = 0;
		 GetPrinter (AktPrinter);

		 wsize = printeranz;
		 if (wsize > 12) wsize = 12;

		 hFont = SetWindowFont (NULL);
		 hdc = GetDC (NULL);
		 oldfont = SelectObject (hdc, hFont);
		 GetTextMetrics (hdc, &tm);
		 DeleteObject (SelectObject (hdc, oldfont));
		 ReleaseDC (NULL, hdc);
		 cx = 42 * tm.tmAveCharWidth;
		 cy = (int) (double) ((double) (wsize + 5) * tm.tmHeight * 1.5);
		 x = 20 * tm.tmAveCharWidth;
		 y = (int) (double) ((double) (25 * tm.tmHeight * 1.5 - cy) / 2); 
		 y = max (0,y);

         lMainWindow  = CreateWindow ("PrintWindow",
                                      "Druckerwahl",
                                      WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_VISIBLE,
                                      x, y,
                                      cx, cy,
                                      NULL,
                                      NULL,
                                      hMainInst,
									  NULL);

//		  display_form (lMainWindow, &fprinter, 0, 0);
		  create_enter_form (lMainWindow, &fprinter, 0, 0);
		  SetBorder (WS_CHILD | WS_VISIBLE);
          ListWindow = OpenListWindowBu (lMainWindow, wsize,
                                         36, 2, 1, FALSE);

          for (i = 0; i < printeranz; i ++)
		  {
			  InsertListRow (PrinterTab [i]);
		  }
          idx = EnterQueryListBox ();
		  if (idx != -1)
		  {
			  SetPrinter (PrinterTab [idx]);
		  }
		  DestroyWindow (lMainWindow);
}



LONG FAR PASCAL PrintProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
        HDC hdc;

        switch(msg)
        {

              case WM_USER :
                      return 0;

              case WM_PAINT :
                      hdc = BeginPaint (hWnd, &ps);
                      EndPaint (hWnd, &ps);
                      break;
              case WM_DESTROY :
                      PostQuitMessage (0);
                      break;

              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}



