#ifndef _DFUE_PID_DEF
#define _DFUE_PID_DEF

extern struct DFUE_PID dfue_pid, dfue_pid_null;

class DFUE_PID_CLASS
{
       private :
            short cursor_mdm;
            short cursor_dfue_pid;
            short cursor_dfue_pid_tty;

            void out_quest_all (void);
            void prepare_mdm (void);       
            void prepare_dfue_pid (char *);       
            void prepare_dfue_pid_tty (void);       
       public:
           DFUE_PID_CLASS ()
           {
                    cursor_mdm      = -1;
                    cursor_dfue_pid = -1;
                    cursor_dfue_pid_tty = -1;
           }
           int lese_mdm (long);
           int lese_mdm (void);
           int lese_dfue_pid (char *);
           int lese_dfue_pid (void);
           int lese_dfue_pid_tty (void);

           void close_mdm (void);
           void close_dfue_pid (void);
           void close_dfue_pid_tty (void);
};

#endif