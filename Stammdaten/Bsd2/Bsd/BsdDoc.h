// BsdDoc.h : Schnittstelle der Klasse CBsdDoc
//


#pragma once


class CBsdDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CBsdDoc();
	DECLARE_DYNCREATE(CBsdDoc)

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CBsdDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


