// DataView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Bsd.h"
#include "DataView.h"


// CDataView-Dialogfeld

IMPLEMENT_DYNAMIC(CDataView, CDialog)

CDataView::CDataView(CWnd* pParent /*=NULL*/)
	: CDialog(CDataView::IDD, pParent)
{

}

CDataView::~CDataView()
{
}

void CDataView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDataView, CDialog)
//	ON_WM_SIZE ()
END_MESSAGE_MAP()

BOOL CDataView::OnInitDialog ()
{
	BOOL ret = CDialog::OnInitDialog ();
	dlg.Construct (_T(""), this);

	BsdData.Construct (IDD_BDS_DATA);
	dlg.AddPage (&BsdData);

	BsdAdd.Construct (IDD_BDS_ADD);
	dlg.AddPage (&BsdAdd);

    dlg.m_psh.pszCaption = _T("Simple");
    dlg.m_psh.nStartPage = 0;
    dlg.m_psh.dwSize = sizeof (dlg.m_psh);
	dlg.Create (this, WS_CHILD | WS_VISIBLE);
    BsdData.SetFocus (); 


	CRect cRect;
    dlg.GetClientRect (&cRect);
    dlg.MoveWindow (0, 0, cRect.right, cRect.bottom);
	return ret;
}

// CDataView-Meldungshandler

void  CDataView::OnSize (UINT nType, int cx, int cy)
{
	CRect rect;
	BsdData.GetWindowRect (&rect);
	ScreenToClient (&rect);
	rect.right = cx;
	rect.bottom = cy - 2;
	BsdData.MoveWindow (&rect);
}

void CDataView::OnUpdateFileSave(CCmdUI *pCmdUI)
{
	CDbPropertyPage *page = (CDbPropertyPage *) dlg.GetActivePage ();
	if (page == &BsdData)
	{
		if (BsdData.CanWrite)
		{
		   pCmdUI->Enable ();
		   return;
		}
	}
	else if (page == &BsdAdd)
	{
		if (BsdData.CanWrite)
		{
		   pCmdUI->Enable ();
		   return;
		}
	}
    pCmdUI->Enable (FALSE);
}

void CDataView::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CDbPropertyPage *page = (CDbPropertyPage *) dlg.GetActivePage ();
	if (page == &BsdData)
	{
		BsdData.Write ();
	}
	else if (page == &BsdAdd)
	{
		BsdAdd.Write ();
	}
}

void CDataView::OnSelected ()
{
	CDbPropertyPage *page = (CDbPropertyPage *) dlg.GetActivePage ();
/*
	if (page == &BsdData)
	{
		BsdData.OnSelected ();
	}
	else if (page == &BsdAdd)
	{
		BsdAdd.OnSelected ();
	}
*/
	if (IsWindow (BsdData.m_hWnd))
	{
		BsdData.OnSelected ();
	}
	if (IsWindow (BsdAdd.m_hWnd))
	{
		BsdAdd.OnSelected ();
	}
}

void CDataView::OnCanceled ()
{
	CDbPropertyPage *page = (CDbPropertyPage *) dlg.GetActivePage ();
	if (page == &BsdData)
	{
		BsdData.OnCanceled ();
	}
	else if (page == &BsdAdd)
	{
		BsdAdd.OnCanceled ();
	}
}


void CDataView::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CDbPropertyPage *page = (CDbPropertyPage *) dlg.GetActivePage ();
	if (page == &BsdData)
	{
		BsdData.OnEditCopy ();
	}
	else if (page == &BsdAdd)
	{
		BsdAdd.OnEditCopy ();
	}
}

void CDataView::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CDbPropertyPage *page = (CDbPropertyPage *) dlg.GetActivePage ();
	if (page == &BsdData)
	{
		BsdData.OnEditPaste ();
	}
	else if (page == &BsdAdd)
	{
		BsdAdd.OnEditPaste ();
	}
}


void CDataView::OnDlgBackgroun()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CDbPropertyPage *page = (CDbPropertyPage *) dlg.GetActivePage ();
	CColorDialog cdlg;
	cdlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	cdlg.m_cc.rgbResult = Color;
	if (cdlg.DoModal() == IDOK)
	{
		    COLORREF DlgBkColor = cdlg.GetColor();
			BsdData.CDlgBean::DlgBkColor = DlgBkColor;
			DeleteObject (BsdData.CDlgBean::DlgBrush);
			BsdData.CDlgBean::DlgBrush = NULL;
			if (IsWindow (BsdData.m_hWnd))
			{
				BsdData.Invalidate ();
			}
			BsdAdd.CDlgBean::DlgBkColor = DlgBkColor;
			DeleteObject (BsdAdd.CDlgBean::DlgBrush);
			BsdAdd.CDlgBean::DlgBrush = NULL;
			if (IsWindow (BsdAdd.m_hWnd))
			{
				BsdAdd.Invalidate ();
			}
	}
}

void CDataView::OnTextColor()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CDbPropertyPage *page = (CDbPropertyPage *) dlg.GetActivePage ();
	CColorDialog cdlg;
	cdlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	COLORREF Color = RGB (0, 0, 0);
	cdlg.m_cc.rgbResult = Color;
	if (cdlg.DoModal() == IDOK)
	{
			COLORREF TextColor = cdlg.GetColor();
			BsdData.TextColor = TextColor;
			if (IsWindow (BsdData.m_hWnd))
			{
				BsdData.Invalidate ();
			}
			BsdAdd.TextColor = TextColor;
			if (IsWindow (BsdAdd.m_hWnd))
			{
				BsdAdd.Invalidate ();
			}
	}
}

void CDataView::SetFocus()
{
	CDbPropertyPage *page = (CDbPropertyPage *) dlg.GetActivePage ();
	if (page != NULL)
	{
		page->SetFocus ();
	}
}
