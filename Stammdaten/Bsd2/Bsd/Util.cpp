#include "StdAfx.h"
#include "util.h"

CUtil::CUtil(void)
{
}

CUtil::~CUtil(void)
{
}

void CUtil::GetPersName (CString& PersName)
{
	int pos = 0;
	CString Progname;
	PersName = "";
	CString Line = GetCommandLine ();
	if (Line.GetBuffer ()[0] == _T('\"'))
	{
		int p1 = Line.Mid (1).Find (_T("\""));
		if (p1 == -1)
		{
			return;
		}
		if (Line.GetLength () < p1 + 3)
		{
			return;
		}
		Line = Line.Mid (p1 + 2);
		Line.Trim ();
		PersName = Line.Tokenize ( " ", pos);
		return;
	}

	Progname = Line.Tokenize ( " ", pos);
	if (pos > 0)
	{
		PersName = Line.Tokenize ( " ", pos);
	}
}
