//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Bsd.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_BSD_FORM                    101
#define IDR_MAINFRAME                   128
#define IDR_BsdTYPE                     129
#define IDD_BDS_ADD                     130
#define IDB_SAVE                        131
#define IDI_HARROWNO                    133
#define IDI_HARROWUP                    134
#define IDI_HARROWDOWN                  135
#define IDD_DATA_VIEW                   136
#define IDD_BDS_DATA                    137
#define IDB_NEW                         138
#define IDB_NEW_MASK                    139
#define IDD_NEW_BSDS                    140
#define IDB_F12MAKT                     146
#define IDB_F12MASKT                    146
#define IDB_F12T                        147
#define IDB_F5MASKT                     148
#define IDB_BITMAP1                     149
#define IDB_F5T                         149
#define IDC_LA                          1000
#define IDC_A                           1001
#define IDC_LMDN                        1002
#define IDC_MDN                         1003
#define IDC_LFIL                        1004
#define IDC_FIL                         1005
#define IDC_LCHARGENNR                  1006
#define IDC_CHARGENNR                   1007
#define IDC_LIDENT_NR                   1008
#define IDC_IDENT_NR                    1009
#define IDC_LBSD_UPDATE                 1010
#define IDC_BSD_UPDATE                  1011
#define IDC_BSD_RES_GRUND               1012
#define IDC_LBSD_GEW                    1013
#define IDC_LBSD_RES_GRUND              1014
#define IDC_LPERS                       1015
#define IDC_PERS                        1016
#define IDC_A_BZ1                       1017
#define IDC_A_BZ2                       1018
#define IDC_BSD_GEW                     1019
#define IDC_BSD_UPZEIT                  1020
#define IDC_LCHARGENNR_SEARCH           1021
#define IDC_LAGER_ORT_SEARCH            1022
#define IDC_LIDENT_NR_SEARCH            1023
#define IDC_IDENT_NR_SEARCH             1024
#define IDC_LLAGER_ORT_SEARCH           1025
#define IDC_CHARGENNR_SEARCH            1026
#define IDC_LLAGER_ORT                  1027
#define IDC_LAGER_ORT                   1028
#define IDC_INFO                        1029
#define IDC_LBSD_PLUS                   1031
#define IDC_BSD_PLUS                    1032
#define IDC_LBSD_MINUS                  1033
#define IDC_BSD_PLUS2                   1034
#define IDC_BSD_MINUS                   1034
#define IDC_BORDER                      1034
#define IDC_LBSD_GEW_AKT                1035
#define IDC_BSD_GEW_AKT                 1036
#define IDC_LBSD_GEW_KORR               1037
#define IDC_BSD_GEW_AKT2                1038
#define IDC_BSD_GEW_KORR                1038
#define ID_ANSICHT_HINTERGRUNDFARBEF32771 32771
#define ID_LISTBACKGROUND               32772
#define ID_ANSICHT_TEXTFARBEF32773      32773
#define ID_LIST_FOREGROUND              32774
#define ID_ANSICHT_HINTERGRUNDFARBEDIALOG 32775
#define ID_DLG_BACKGROUN                32776
#define ID_ANSICHT_TEXTFARBEF32777      32777
#define ID_TEXT_COLOR                   32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        150
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
