#include "StdAfx.h"
#include "artimages.h"
#include "DbUnicode.h"
#include "Token.h"

CArtImages::CArtImages(void)
{
	cursor = -1;
	FileName = "Images.fit";
	Path = "";
}

CArtImages::~CArtImages(void)
{
	if (cursor != -1)
	{
		A_bas.sqlclose (cursor);
		cursor = -1;
	}
}

BOOL CArtImages::Open ()
{
	CString ImageFiles;
	ImageFiles.GetEnvironmentVariable (_T("SERV160"));
	if (ImageFiles.GetLength () == 0)
	{
		ImageFiles = FileName;
	}
	else
	{
		ImageFiles += _T("\\");
		ImageFiles += FileName;
	}

	try
	{
		return File.Open (ImageFiles.GetBuffer (), CFile::modeCreate | 
					      CFile::modeWrite | CFile::shareDenyWrite);   
	}
	catch (...)
	{
		return FALSE;
	}
}

void CArtImages::Close ()
{
	if (File.m_hFile == CFile::hFileNull) return;
	File.Close ();
}

BOOL CArtImages::WriteFromPath ()
{
	if (Path == "") return FALSE;

	if (File.m_hFile == CFile::hFileNull) return FALSE;

	if (Path.Right (1) != _T("\\") && Path.Right (1) != _T("/"))
	{
		Path += _T("\\");
	}
	CString p = Path;
	p += _T("*.jpg");

	CString Record;
	HANDLE Dir = FindFirstFile(p.GetBuffer (), &fData);
	if (Dir == INVALID_HANDLE_VALUE || Dir == NULL)
	{
		return FALSE;
	}

	do
	{
		if ((fData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) continue;
		Record.Format (_T("%s%s"), Path.GetBuffer (), fData.cFileName);
		CString ServerPath;
		MakeServerPath (Record, ServerPath);
		CopyToServer (Record);
		Record.Format (_T("%s\r\n"), ServerPath.GetBuffer ());
		LPSTR im = new char [2 * (Record.GetLength () + 2)];
		CDbUniCode::DbFromUniCode (Record.GetBuffer (), im, Record.GetLength ());
    	File.Write (im, Record.GetLength ()); 
		delete im;
	}
	while (FindNextFile (Dir, &fData));
	FindClose (Dir);
	return TRUE;
}

BOOL CArtImages::WriteLinked ()
{
	char bild [129];

	if (File.m_hFile == CFile::hFileNull) return FALSE;
	if (cursor == -1)
	{
		A_bas.sqlout ((char *) bild, SQLCHAR, sizeof (bild));
		cursor = A_bas.sqlcursor (_T("select bild from a_bas where a > 0")
			                      _T("and bild > \"\" and bild is not null"));
	}

	if (cursor == -1) return FALSE;

	A_bas.sqlopen (cursor);
	while (A_bas.sqlfetch (cursor) == 0)
	{
		File.Write (bild, (int) strlen (bild));
		File.Write ("\r\n", 2);
	}
	return TRUE;
}

BOOL CArtImages::MakeServerPath (CString& Path, CString& ServerPath)
{
	ServerPath = "";
	if (!ServerPath.GetEnvironmentVariable (_T("serv160")))
	{
		return FALSE;
	}

	CToken t;
	t.SetSep (_T("\\/"));
	t = Path;
	int tcount = t.GetAnzToken ();
	CString FileName = t.GetToken (tcount - 1);

	LPTSTR p = ServerPath.GetBuffer ();
	int length = ServerPath.GetLength ();
	p += length - 1;
	if (*p == '\\' ||*p == '/')
	{
		p -= 1;
		length --;
	}
	for (;(*p != '\\') && (*p != '/') && length > 0; p -=1, length --);

	if (length > 0)
	{
		ServerPath = ServerPath.Left (length);
	}
	ServerPath += _T("bilder\\");
	ServerPath += FileName;
	return TRUE;
}

BOOL CArtImages::CopyToServer (CString& Path)
{
	MakeServerPath (Path, ServerPath);
	if (Path.CompareNoCase (ServerPath) == 0) return TRUE;
	return CopyFile (Path.GetBuffer (), ServerPath.GetBuffer (), FALSE);
}

