// BsdView.h : Schnittstelle der Klasse CBsdView
//


#pragma once

#include "SplitPane.h"
#include "DataView.h"
#include "BsdData.h"
#include "ChoiceBsds.h"
#include "DbPropertySheet.h"

class CBsdView : public CFormView
{
protected: // Nur aus Serialisierung erstellen
	CBsdView();
	DECLARE_DYNCREATE(CBsdView)

public:
	enum{ IDD = IDD_BSD_FORM };

// Attribute
public:
	CBsdDoc* GetDocument() const;

// Vorgänge
public:

// Überschreibungen
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung

public:
	CSplitPane SplitPane;
	CDataView *DataView;
	CBsdData *BsdData;
	CChoiceBsds *ChoiceBsds;
	BOOL DockedChoice;
	int Direction;
	CDbPropertySheet dlg;

// Implementierung
public:
	virtual ~CBsdView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
    virtual BOOL PreTranslateMessage(MSG* pMsg);

private:

public :
	void CreateView ();
	virtual void OnSize (UINT nType, int cx, int cy);
	virtual void OnSelected ();
	virtual void OnCanceled ();
	afx_msg void OnDestroy( );
	void OnFileSave();
	void OnUpdateFileSave(CCmdUI *pCmdUI);
public:
	afx_msg void OnEditCopy();
public:
	afx_msg void OnEditPaste();
public:
	afx_msg void OnListbackground();
public:
	afx_msg void OnListForeground();
public:
	afx_msg void OnDlgBackgroun();
public:
	afx_msg void OnTextColor();
};

#ifndef _DEBUG  // Debugversion in BsdView.cpp
inline CBsdDoc* CBsdView::GetDocument() const
   { return reinterpret_cast<CBsdDoc*>(m_pDocument); }

#endif

