#pragma once
#include "CtrlGrid.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "Bsds.h"
#include "A_bas.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "StaticButton.h"
#include "ChoiceBsds.h"
#include "ChoiceA.h"
#include "ImageCtrl.h"
#include "DlgBean.h"
#include "DbPropertyPage.h"

// CNewBsds-Dialogfeld
#define IDC_ACHOICE 3000

class CNewBsds : public CDialog
{
	DECLARE_DYNAMIC(CNewBsds)

public:
	CNewBsds(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CNewBsds();

// Dialogfelddaten
	enum { IDD = IDD_NEW_BSDS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
//    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

    CFormTab Form;
    A_BAS_CLASS A_bas;
    BSDS_CLASS Bsds;
    PTABN_CLASS Ptabn;
	CFont Font;
	CCtrlGrid CtrlGrid;
	CCtrlGrid HeadGrid;
	CCtrlGrid DataGrid;
	CCtrlGrid MdnFilGrid;
	CCtrlGrid ArtGrid;
	CChoiceA *Choice;
	BOOL ModalChoice;

public:
	CString PersName;

	CStatic m_Border;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CStatic m_LFil;
	CNumEdit m_Fil;
	CStatic m_LA;
	CNumEdit m_A;
	CButton m_AChoice;
	CTextEdit m_ABz1;
	CTextEdit m_ABz2;

	CStatic m_LLagerOrt;
	CTextEdit m_LagerOrt;
	CStatic m_LChargennr;
	CTextEdit m_Chargennr;
	CStatic m_LIdentNr;
	CTextEdit m_IdentNr;

	CStatic m_LBsdGew;
	CNumEdit m_BsdGew;

public:
	afx_msg void OnBnClickedOk();
	void OnAchoice ();
    void OnASelected ();
    void OnACanceled ();
	BOOL Write ();
public:
	afx_msg void OnEnKillfocusA();
public:
	CButton m_Save;
public:
	CButton m_Cancel;
};
