// BsdView.cpp : Implementierung der Klasse CBsdView
//

#include "stdafx.h"
#include "Bsd.h"

#include "BsdDoc.h"
#include "BsdView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define _CRTDBG_MAP_ALLOC
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;


// CBsdView

IMPLEMENT_DYNCREATE(CBsdView, CFormView)

BEGIN_MESSAGE_MAP(CBsdView, CFormView)
	ON_WM_SIZE ()
	ON_COMMAND (ID_FILE_SAVE, OnFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_COMMAND (SELECTED, OnSelected)
	ON_COMMAND (CANCELED, OnCanceled)
	ON_COMMAND(ID_EDIT_COPY, &CBsdView::OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, &CBsdView::OnEditPaste)
	ON_COMMAND(ID_LISTBACKGROUND, &CBsdView::OnListbackground)
	ON_COMMAND(ID_LIST_FOREGROUND, &CBsdView::OnListForeground)
	ON_COMMAND(ID_DLG_BACKGROUN, &CBsdView::OnDlgBackgroun)
	ON_COMMAND(ID_TEXT_COLOR, &CBsdView::OnTextColor)
END_MESSAGE_MAP()

// CBsdView-Erstellung/Zerst�rung

CBsdView::CBsdView()
	: CFormView(CBsdView::IDD)
{
	// TODO: Hier Code zur Konstruktion einf�gen
	DockedChoice = TRUE;
//	DockedChoice = FALSE;
	BsdData = NULL;
	DataView = NULL;
	ChoiceBsds = NULL;
	Direction = CSplitPane::Horizontal;
//	Direction = CSplitPane::Vertical;
}

CBsdView::~CBsdView()
{
	if (DataView != NULL)
	{
		delete DataView;
		DataView = NULL;
	}
	if (ChoiceBsds != NULL)
	{
		delete ChoiceBsds;
		ChoiceBsds = NULL;
	}
}

void CBsdView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL CBsdView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CFormView::PreCreateWindow(cs);
}

void CBsdView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	CreateView ();
}

void CBsdView::CreateView()
{
	if (DataView != NULL)
	{
		delete DataView;
		DataView = NULL;
	}

	if (ChoiceBsds != NULL)
	{
		delete ChoiceBsds;
		ChoiceBsds = NULL;
	}

//	dlg.Construct (_T(""), this);
/*
	Page1.Construct (IDD_BASISDATEN_PAGE);
	Page1.Frame = this;
	Page1.Update = this;
	Page1.HideButtons = TRUE;
	dlg.AddPage (&Page1);
    dlg.m_psh.pszCaption = _T("Simple");
    dlg.m_psh.nStartPage = 0;

/*
	BsdData = new CBsdData ();
	BsdData->Create (IDD_BDS_DATA, this);
	BsdData->Frame = this;
	BsdData->Update = this;
	BsdData->HideButtons = TRUE;
	dlg.AddPage (BsdData);
    dlg.m_psh.pszCaption = _T("Simple");
    dlg.m_psh.nStartPage = 0;
*/

	DataView = new CDataView ();
	DataView->Create (IDD_DATA_VIEW, this);
	DataView->ShowWindow (SW_SHOWNORMAL);
	DataView->UpdateWindow ();

	if (DockedChoice)
	{
		ChoiceBsds = new CChoiceBsds (this);
	    ChoiceBsds->IsModal = FALSE;
	    ChoiceBsds->Data = DataView;
		DataView->BsdData.Choice = ChoiceBsds;
		DataView->BsdAdd.Choice = ChoiceBsds;
//		ChoiceBsds->DlgBkColor = ListBkColor;
//		ChoiceBsds->Where = Where;
		ChoiceBsds->IdArrDown = IDI_HARROWDOWN;
		ChoiceBsds->IdArrUp   = IDI_HARROWUP;
		ChoiceBsds->IdArrNo   = IDI_HARROWNO;
		ChoiceBsds->HideOK    = TRUE;
		ChoiceBsds->HideCancel = TRUE;
		ChoiceBsds->HideFilter = FALSE;
//		ChoiceBsds->Types     = Types;
		ChoiceBsds->SetDlgEmbeddedStyle (0);
		ChoiceBsds->CreateDlg ();
		ChoiceBsds->ShowWindow (SW_SHOWNORMAL);
	    ChoiceBsds->UpdateWindow ();

//		SplitPane.Create (this, 0, 0, ChoiceBsds, BsdData, 40, Direction);
		SplitPane.Create (this, 0, 0, ChoiceBsds, DataView, 40, Direction);
	}
}

void CBsdView::OnSize (UINT nType, int cx, int cy)
{
	if (IsWindow (SplitPane.m_hWnd))
	{
		if (Direction == CSplitPane::Vertical)
		{
			CRect rect;
			ChoiceBsds->GetWindowRect (&rect);
			ScreenToClient (&rect);
			rect.right = cx;
			ChoiceBsds->MoveWindow (&rect);

			DataView->GetWindowRect (&rect);
			ScreenToClient (&rect);
			rect.right = cx;
			rect.bottom = cy - 2;
			DataView->MoveWindow (&rect);

			SplitPane.SetLength (cx);
		}
		else
		{
			CRect rect;
			ChoiceBsds->GetWindowRect (&rect);
			ScreenToClient (&rect);
			rect.bottom = cy;
			ChoiceBsds->MoveWindow (&rect);

			DataView->GetWindowRect (&rect);
			ScreenToClient (&rect);
			rect.bottom = cy;
			rect.right = cx - 2;
			DataView->MoveWindow (&rect);

			SplitPane.SetLength (cy);
		}
	}
}

BOOL CBsdView::PreTranslateMessage(MSG* pMsg)
{
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					if (GetFocus ()->GetParent () == BsdData)
					{
						ChoiceBsds->SetFocus ();
					}
					else if (GetFocus ()->GetParent () == ChoiceBsds)
					{
//						DataView->SetFocus ();
						DataView->dlg.GetActivePage ()->SetFocus ();
					}
					return TRUE;
				}
			}
			break;
	}
	return FALSE;
}

// CBsdView-Diagnose

#ifdef _DEBUG
void CBsdView::AssertValid() const
{
	CFormView::AssertValid();
}

void CBsdView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CBsdDoc* CBsdView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CBsdDoc)));
	return (CBsdDoc*)m_pDocument;
}
#endif //_DEBUG

void CBsdView::OnUpdateFileSave(CCmdUI *pCmdUI)
{
	if (DataView != NULL)
	{
		if (DataView->BsdData.CanWrite)
		{
		   pCmdUI->Enable ();
		   return;
		}
	}
    pCmdUI->Enable (FALSE);
}

void CBsdView::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (DataView != NULL)
	{
		DataView->OnFileSave ();
	}
}

void CBsdView::OnSelected ()
{
	if (DataView != NULL)
	{
		DataView->OnSelected ();
	}
}

void CBsdView::OnCanceled ()
{
	if (DataView != NULL)
	{
		DataView->OnCanceled ();
	}
}

// CBsdView-Meldungshandler

void CBsdView::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (DataView != NULL)
	{
		DataView->OnEditCopy ();
	}
}

void CBsdView::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (DataView != NULL)
	{
		DataView->OnEditPaste ();
	}
}

void CBsdView::OnListbackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	if (ChoiceBsds != NULL)
	{
		CColorDialog cdlg;
		cdlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
		COLORREF Color = GetSysColor (COLOR_3DFACE);
		cdlg.m_cc.rgbResult = Color;
		if (cdlg.DoModal() == IDOK)
		{
			Color = cdlg.GetColor();
			CListCtrl *ListView = ChoiceBsds->GetListView ();
			if (ListView != NULL)
			{
				ListView->SetBkColor (Color);
				ListView->SetTextBkColor (Color);
				ListView->Invalidate ();
			}
		}
	}
}

void CBsdView::OnListForeground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (ChoiceBsds != NULL)
	{
		CColorDialog cdlg;
		cdlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
		COLORREF Color = GetSysColor (COLOR_3DFACE);
		cdlg.m_cc.rgbResult = Color;
		if (cdlg.DoModal() == IDOK)
		{
			Color = cdlg.GetColor();
			CListCtrl *ListView = ChoiceBsds->GetListView ();
			if (ListView != NULL)
			{
				ListView->SetTextColor (Color);
				ListView->Invalidate ();
			}
		}
	}
}

void CBsdView::OnDlgBackgroun()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (DataView != NULL)
	{
		DataView->OnDlgBackgroun ();
	}
}

void CBsdView::OnTextColor()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (DataView != NULL)
	{
		DataView->OnTextColor ();
	}
}
