#include "stdafx.h"
#include "bsd_buch.h"

struct BSD_BUCH bsd_buch, bsd_buch_null;

void BSD_BUCH_CLASS::prepare (void)
/**
Cursor fuer bsd_buch vorbereiten.
**/
{
    sqlin ((long *) &bsd_buch.nr,SQLLONG,0);
    sqlin ((TCHAR *) bsd_buch.blg_typ,SQLCHAR,3);
    sqlin ((short *) &bsd_buch.mdn,SQLSHORT,0);
    sqlin ((short *) &bsd_buch.fil,SQLSHORT,0);
    sqlin ((short *) &bsd_buch.kun_fil,SQLSHORT,0);
    sqlin ((double *) &bsd_buch.a,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &bsd_buch.dat,SQLDATE,0);
    sqlin ((TCHAR *) bsd_buch.zeit,SQLCHAR,9);
    sqlin ((TCHAR *) bsd_buch.pers,SQLCHAR,13);
    sqlin ((TCHAR *) bsd_buch.bsd_lgr_ort,SQLCHAR,13);
    sqlin ((short *) &bsd_buch.qua_status,SQLSHORT,0);
    sqlin ((double *) &bsd_buch.me,SQLDOUBLE,0);
    sqlin ((double *) &bsd_buch.bsd_ek_vk,SQLDOUBLE,0);
    sqlin ((TCHAR *) bsd_buch.chargennr,SQLCHAR,31);
    sqlin ((TCHAR *) bsd_buch.ident_nr,SQLCHAR,21);
    sqlin ((TCHAR *) bsd_buch.herk_nachw,SQLCHAR,14);
    sqlin ((TCHAR *) bsd_buch.lief,SQLCHAR,17);
    sqlin ((long *) &bsd_buch.auf,SQLLONG,0);
    sqlin ((TCHAR *) bsd_buch.verfall,SQLCHAR,2);
    sqlin ((short *) &bsd_buch.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) bsd_buch.err_txt,SQLCHAR,17);
    sqlin ((double *) &bsd_buch.me2,SQLDOUBLE,0);
    sqlin ((short *) &bsd_buch.me_einh2,SQLSHORT,0);
        ins_cursor = sqlcursor (_T("insert into bsd_buch (nr,  ")
_T("blg_typ,  mdn,  fil,  kun_fil,  a,  dat,  zeit,  pers,  bsd_lgr_ort,  qua_status,  me,  ")
_T("bsd_ek_vk,  chargennr,  ident_nr,  herk_nachw,  lief,  auf,  verfall, ")
_T("delstatus,  err_txt,  me2,  me_einh2) ")

#line 12 "bsd_buch.rpp"
                                  _T("values ")
                                  _T("(?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 14 "bsd_buch.rpp"
}

int BSD_BUCH_CLASS::dbinsert (void)
/**
In bsd_buch einfuegen.
**/
{
        int dsqlstatus;
        if (ins_cursor == -1)
        {
                       prepare ();
        }
        dsqlstatus = sqlexecute (ins_cursor);
        return dsqlstatus;
}

void BSD_BUCH_CLASS::dbclose (void)
/**
Cursor schliessen.
**/
{
         if (ins_cursor == -1) return;
         sqlclose (ins_cursor);
         ins_cursor = -1;
}
       
           

