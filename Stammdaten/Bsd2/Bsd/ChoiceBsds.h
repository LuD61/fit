#ifndef _CHOICE_BSDS_DEF
#define _CHOICE_BSDS_DEF

#include "ChoiceX.h"
#include "BsdsList.h"
#include <vector>

class CChoiceBsds : public CChoiceX
{
    protected :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
        static int Sort7;
        static int Sort8;
        static int Sort9;
        static int Sort10;
        static int Sort11;
        static int Sort12;
        static int Sort13;
		int ptcursor;
		TCHAR ptitem [19];
		TCHAR ptwert [4];
		TCHAR ptbez [37];
		TCHAR ptbezk [17];
		int DlgHeight;
		CFont Font;
		BOOL FontIsSelected;

	protected:
	    virtual BOOL PreTranslateMessage(MSG* pMsg);

    public :
		CWnd *Data;
		CString Where;
		CString Types;
	    std::vector<CBsdsList *> BsdsList;
	    std::vector<CBsdsList *> SelectList;
      	CChoiceBsds(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceBsds(); 
        virtual void FillList (void);
        void SearchCol (CListCtrl *, LPTSTR, int);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
	    virtual void SaveSelection (CListCtrl *);
		CBsdsList *GetSelectedText ();
		void SetSelectedText (CBsdsList *);
        int GetPtBez (LPTSTR, LPTSTR, LPSTR);
		void DestroyList ();
		virtual void SetDefault ();
		virtual void OnEnter ();
	    virtual void OnFilter ();
		BOOL SearchRecord (short mdn, short fil, double a, LPTSTR bsd_lgr_ort, LPTSTR chargennr, LPTSTR ident_nr);
};
#endif
