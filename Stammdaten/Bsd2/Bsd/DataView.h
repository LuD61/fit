#pragma once
#include "BsdData.h"
#include "BsdAdd.h"
#include "ChoiceBsds.h"
#include "DbPropertySheet.h"

// CDataView-Dialogfeld

class CDataView : public CDialog
{
	DECLARE_DYNAMIC(CDataView)

public:
	CDataView(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDataView();

// Dialogfelddaten
	enum { IDD = IDD_DATA_VIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();

	DECLARE_MESSAGE_MAP()
public:
	CBsdData BsdData;
	CBsdAdd BsdAdd;
	CDbPropertySheet dlg;

	virtual void OnSize (UINT nType, int cx, int cy);
	virtual void OnSelected ();
	virtual void OnCanceled ();
	afx_msg void OnDestroy( );
	void OnFileSave();
	void OnUpdateFileSave(CCmdUI *pCmdUI);

public:
	afx_msg void OnEditCopy();
public:
	afx_msg void OnEditPaste();
public:
	afx_msg void OnListbackground();
public:
	afx_msg void OnListForeground();
public:
	afx_msg void OnDlgBackgroun();
public:
	afx_msg void OnTextColor();
	virtual void SetFocus ();

};
