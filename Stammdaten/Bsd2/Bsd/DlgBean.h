#pragma once

class CDlgBean
{
public:
	COLORREF TextColor;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	BOOL FlatLayout;
	CString ArchiveName;

	CDlgBean(void);
public:
	~CDlgBean(void);
	virtual void Save ();
	virtual void Load ();
};
