#include "StdAfx.h"
#include "DlgBean.h"

CDlgBean::CDlgBean(void)
{
	TextColor = RGB (0,0,0);
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
	FlatLayout = FALSE;
	ArchiveName = _T("");
}

CDlgBean::~CDlgBean(void)
{
	if (DlgBrush != NULL)
	{
		DeleteObject (DlgBrush);
		DlgBrush = 0;
	}
}

void CDlgBean::Save ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	File.Open (Path.GetBuffer (), CFile::modeCreate | CFile::modeWrite);
	CArchive archive(&File, CArchive::store);

	try
	{
			archive.Write (&DlgBkColor, sizeof (DlgBkColor));
			archive.Write (&FlatLayout, sizeof (FlatLayout));
			archive.Write (&TextColor, sizeof (TextColor));
	}
	catch (...) 
	{
		return;
	}


	archive.Close ();
	File.Close ();
}

void CDlgBean::Load ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}

	if (!File.Open (Path.GetBuffer (), CFile::modeRead))
	{
		return;
	}
	CArchive archive(&File, CArchive::load);
	try
	{
			archive.Read (&DlgBkColor, sizeof (DlgBkColor));
			archive.Read (&FlatLayout, sizeof (FlatLayout));
			archive.Read (&TextColor, sizeof (TextColor));
	}
	catch (...) 
	{
		return;
	}

	archive.Close ();
	File.Close ();

}

