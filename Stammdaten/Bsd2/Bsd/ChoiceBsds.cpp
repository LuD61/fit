#include "stdafx.h"
#include "ChoiceBsds.h"
#include "DbUniCode.h"
#include "Process.h"
#include "StrFuncs.h"
#include "Token.h"
#include "DynDialog.h"
#include "DbQuery.h"
#include "DbTime.h"
#include "Process.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define _CRTDBG_MAP_ALLOC
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceBsds::Sort1 = -1;
int CChoiceBsds::Sort2 = -1;
int CChoiceBsds::Sort3 = -1;
int CChoiceBsds::Sort4 = -1;
int CChoiceBsds::Sort5 = -1;
int CChoiceBsds::Sort6 = -1;
int CChoiceBsds::Sort7 = -1;
int CChoiceBsds::Sort8 = -1;
int CChoiceBsds::Sort9 = -1;
int CChoiceBsds::Sort10 = -1;
int CChoiceBsds::Sort11 = -1;
int CChoiceBsds::Sort12 = -1;
int CChoiceBsds::Sort13 = -1;

CChoiceBsds::CChoiceBsds(CWnd* pParent) 
        : CChoiceX(pParent)
{
	ptcursor = -1;
	Where = "";
	Types = "";
	FontIsSelected = FALSE;
	Bean.ArchiveName = _T("BsdsList.prp");
	Data = NULL;
}

CChoiceBsds::~CChoiceBsds() 
{
	DestroyList ();
	if (ptcursor != -1) DbClass->sqlclose (ptcursor);
	ptcursor = -1;
}

BOOL CChoiceBsds::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					if (Data != NULL)
					{
						Data->SetFocus ();
						return TRUE;
					}
				}
				break;
			}
	}
	return CChoiceX::PreTranslateMessage(pMsg);
}

void CChoiceBsds::DestroyList() 
{
	for (std::vector<CBsdsList *>::iterator pabl = BsdsList.begin (); pabl != BsdsList.end (); ++pabl)
	{
		CBsdsList *abl = *pabl;
		delete abl;
	}
    BsdsList.clear ();
}

void CChoiceBsds::FillList () 
{
	short mdn;
	short fil;
    double  a;
    TCHAR a_bz1 [50];
    TCHAR a_bz2 [50];
    TCHAR chargennr [50];
    TCHAR ident_nr [50];
    double bsd_gew;
    short bsd_red_grnd;
    DATE_STRUCT bsd_update;
    TCHAR pers [50];
    TCHAR bsd_lgr_ort [13];
	TCHAR ptbez [37];
	extern short sql_mode;
	short sql_s;

	sql_s = sql_mode;
	sql_mode = 2;
	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);
	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	if (!FontIsSelected)
	{
		if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
		{
			Font.CreatePointFont (85, _T("Dlg"));
		}
		else
		{
			Font.CreatePointFont (105, _T("Dlg"));
		}

		SetFont (&Font);

		for (int i = iSearchLabel; i < iDlgEnd; i ++)
		{
			CWnd * c = GetDlgItem (DlgItems[i]);
			if (c != NULL && IsWindow (c->m_hWnd))
			{
				c->SetFont (&Font);
			}
		}
		FontIsSelected = TRUE;
	}

	listView->SetFont (&Font);

    int i = 0;

    SetWindowText (_T("Bestansliste"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Mandant"),           1, 60, LVCFMT_RIGHT);
    SetCol (_T("Filiale"),           2, 60, LVCFMT_RIGHT);
    SetCol (_T("Artikel"),           3, 100, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung 1"),     4, 250);
    SetCol (_T("Bezeichnung 2"),     5, 250);
    SetCol (_T("Chargen-Nr"),        6, 250);
    SetCol (_T("Ident-Nr"),          7, 250);
    SetCol (_T("Bestand"),           8, 150);
    SetCol (_T("Änderungsgrund"),    9, 150);
    SetCol (_T("Änderungsdatum"),   10, 150);
    SetCol (_T("Bearbeiter"),       11, 150);
    SetCol (_T("Lagerort"),         12, 150);
	SortRow = 3;

	if (FirstRead && !HideFilter)
	{
		FirstRead = FALSE;
		Load ();
		PostMessage (WM_COMMAND, IDC_FILTER, 0l);
		return;
	}
	if (BsdsList.size () == 0)
	{
		DbClass->sqlout ((short *)&mdn,          SQLSHORT, 0);
		DbClass->sqlout ((short *)&fil,          SQLSHORT, 0);
		DbClass->sqlout ((double *)&a,           SQLDOUBLE, 0);
		DbClass->sqlout ((LPTSTR)  a_bz1,        SQLCHAR, sizeof (a_bz1));
		DbClass->sqlout ((LPTSTR)  a_bz2,        SQLCHAR, sizeof (a_bz2));
		DbClass->sqlout ((LPTSTR)  chargennr,    SQLCHAR, sizeof (chargennr));
		DbClass->sqlout ((LPTSTR)  ident_nr,     SQLCHAR, sizeof (ident_nr));
		DbClass->sqlout ((double *)&bsd_gew,     SQLDOUBLE, 0);
		DbClass->sqlout ((short *) &bsd_red_grnd, SQLSHORT, 0);
		DbClass->sqlout ((DATE_STRUCT *) &bsd_update, SQLDATE, 0);
		DbClass->sqlout ((LPTSTR)  pers,    SQLCHAR, sizeof (pers));
		DbClass->sqlout ((LPTSTR)  bsd_lgr_ort,  SQLCHAR, sizeof (bsd_lgr_ort));
		CString Sql  = (_T("select bsds.mdn, bsds.fil, bsds.a, a_bas.a_bz1, a_bas.a_bz2, bsds.chargennr, bsds.ident_nr, "
						   "bsds.bsd_gew, bsds.bsd_red_grnd, bsds.bsd_update, bsds.pers, bsds.bsd_lgr_ort "
						   "from bsds, a_bas where bsds.a = a_bas.a and bsds.delstatus = 0"));
		Sql += " ";
		Sql += Where;
		if (QueryString != _T(""))
		{
			Sql += _T(" and ");
			Sql += QueryString;
		}
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe für den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}
		memset (a_bz1, 0, sizeof (a_bz1));
		memset (a_bz2, 0, sizeof (a_bz2));
		memset (chargennr, 0, sizeof (chargennr));
		memset (ident_nr, 0, sizeof (ident_nr));
		memset (pers, 0, sizeof (pers));

		DbClass->sqlopen (cursor);
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) a_bz1;
			CDbUniCode::DbToUniCode (a_bz1, pos);
			pos = (LPSTR) a_bz2;
			CDbUniCode::DbToUniCode (a_bz2, pos);
			pos = (LPSTR) chargennr;
			CDbUniCode::DbToUniCode (chargennr, pos);
			pos = (LPSTR) ident_nr;
			CDbUniCode::DbToUniCode (ident_nr, pos);
			pos = (LPSTR) pers;
			CDbUniCode::DbToUniCode (pers, pos);
			pos = (LPSTR) bsd_lgr_ort;
			CDbUniCode::DbToUniCode (bsd_lgr_ort, pos);

			CString Ptwert;
			Ptwert.Format (_T("%hd"), bsd_red_grnd);
            GetPtBez (_T("bsd_red_grnd"), Ptwert.GetBuffer (), ptbez);
			CString BsdRedGrnd;
			BsdRedGrnd.Format (_T("%s"), ptbez);
			BsdRedGrnd.Trim ();
			CString BsdUpdate;
			DB_CLASS::FromDbDate (BsdUpdate, &bsd_update);

			CBsdsList *abl = 
				new CBsdsList(mdn,
							  fil,
							  a,
							  a_bz1,								                   
							  a_bz2,								                   
							  chargennr,								                   
							  ident_nr,								                   
							  bsd_gew,
							  BsdRedGrnd.GetBuffer (),
							  BsdUpdate.GetBuffer (),
							  pers, 
							  bsd_lgr_ort);
			BsdsList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
		Load ();
	}
	sql_mode = sql_s;

	for (std::vector<CBsdsList *>::iterator pabl = BsdsList.begin (); pabl != BsdsList.end (); ++pabl)
	{
		CBsdsList *abl = *pabl;
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString Fil;
		Fil.Format (_T("%hd"), abl->fil); 
		CString Art;
		Art.Format (_T("%.0lf"), abl->a); 
        int ret = InsertItem (i, -1);
        ret = SetItemText (Mdn.GetBuffer (), i, 1);
        ret = SetItemText (Fil.GetBuffer (), i, 2);
        ret = SetItemText (Art.GetBuffer (), i, 3);
        ret = SetItemText (abl->a_bz1.GetBuffer (), i, 4);
        ret = SetItemText (abl->a_bz2.GetBuffer (), i, 5);
        ret = SetItemText (abl->chargennr.GetBuffer (), i, 6);
        ret = SetItemText (abl->ident_nr.GetBuffer (), i, 7);
		CString BsdGew;
		BsdGew.Format (_T("%.3lf"), abl->bsd_gew);
        ret = SetItemText (BsdGew.GetBuffer (), i, 8);
        ret = SetItemText (abl->bsd_red_grnd.GetBuffer (), i, 9);
        ret = SetItemText (abl->bsd_update.GetBuffer (), i, 10);
        ret = SetItemText (abl->pers.GetBuffer (), i, 11);
        ret = SetItemText (abl->bsd_lgr_ort.GetBuffer (), i, 12);
        i ++;
    }

    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort7 = -1;
    Sort8 = -1;
    Sort9 = -1;
    Sort10 = -1;
    Sort11 = -1;
    Sort12 = -1;
    Sort13 = -1;
    Sort (listView);
}

BOOL CChoiceBsds::SearchRecord (short mdn, short fil, double a, LPTSTR bsd_lgr_ort, LPTSTR chargennr, LPTSTR ident_nr)
{
	int idx = 0;
	for (std::vector<CBsdsList *>::iterator pabl = BsdsList.begin (); pabl != BsdsList.end (); ++pabl)
	{
		CBsdsList *abl = *pabl;
		if (abl->mdn == mdn &&
			abl->fil  == fil &&
			abl->a   == a &&	
			abl->bsd_lgr_ort.TrimRight () == bsd_lgr_ort &&
			abl->chargennr.TrimRight () == chargennr &&
			abl->ident_nr.TrimRight () == ident_nr)
		{
			CListCtrl *listView = GetListView ();
			ScrolltoIdx (listView,idx);
			return TRUE;
		}
		idx ++;
	}
	return FALSE;
}

void CChoiceBsds::SearchCol (CListCtrl *ListBox, LPTSTR Search, int col)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, col);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceBsds::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
	if ((EditText.Find (_T("*")) != -1) || EditText.Find (_T("?")) != -1)
	{
		CChoiceX::SearchMatch (EditText);
		return;
	}

    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             EditText.MakeUpper ();
             SearchCol (ListBox, EditText.GetBuffer (8),SortRow);
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchCol (ListBox, EditText.GetBuffer (),SortRow);
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchCol (ListBox, EditText.GetBuffer (),SortRow);
             break;
        default :
             EditText.MakeUpper ();
             SearchCol (ListBox, EditText.GetBuffer (), SortRow);
             break;
    }
}

int CChoiceBsds::GetPtBez (LPSTR ptitem, LPTSTR ptwert, LPTSTR ptbez)
{
	if (ptcursor == -1)
	{
		DbClass->sqlout ((LPTSTR) this->ptbez,  SQLCHAR, sizeof (this->ptbez));
		DbClass->sqlout ((LPTSTR) this->ptbezk, SQLCHAR, sizeof (this->ptbezk));
		DbClass->sqlin ((LPTSTR) this->ptitem, SQLCHAR, sizeof (this->ptitem));
		DbClass->sqlin  ((LPTSTR) this->ptwert, SQLCHAR, sizeof (this->ptwert));
	    ptcursor = DbClass->sqlcursor (_T("select ptbez,ptbezk from ptabn where ptitem = ? ")
							           _T("and ptwert = ?"));
	}
	_tcscpy (this->ptbez, _T(""));
	_tcscpy (this->ptbezk, _T(""));
	_tcscpy (this->ptitem, ptitem);
	_tcscpy (this->ptwert, ptwert);
	DbClass->sqlopen (ptcursor);
	int dsqlstatus = DbClass->sqlfetch (ptcursor);

	_tcscpy (ptbez, this->ptbez);
	LPSTR pos = (LPSTR) ptbez;
    CDbUniCode::DbToUniCode (ptbez, pos);
	return dsqlstatus;
}

int CALLBACK CChoiceBsds::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {
// Mandant

	   int li1 = _tstoi (strItem1.GetBuffer ());
	   int li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
// Filiale

	   int li1 = _tstoi (strItem1.GetBuffer ());
	   int li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort3;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort3);
	   }
	   return 0;
   }

   else if (SortRow == 3)
   {
// Artikel

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort4;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort4);
	   }
	   return 0;
   }
   else if (SortRow == 4)
   {
// Artikelbezeichnung1
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort5;
   }
   else if (SortRow == 5)
   {
// Artikelbezeichnung2
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort6;
   }
   else if (SortRow == 6)
   {
// Chargennr
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort7;
   }
   else if (SortRow == 7)
   {
// Ident_nr
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort8;
   }
   else if (SortRow == 8)
   {
// bsd_gew
	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort9;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort9);
	   }
	   return 0;
   }
   else if (SortRow == 9)
   {
// bsd_res_grnd
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort10;
   }
   else if (SortRow == 10)
   {
// bsd_update
    DATE_STRUCT ld1;
	DATE_STRUCT ld2;
    DB_CLASS::ToDbDate (CString (strItem1.GetBuffer ()), &ld1);
    DB_CLASS::ToDbDate (CString (strItem2.GetBuffer ()), &ld2);
    DbTime dt1 (&ld1);
    DbTime dt2 (&ld2);
    time_t li1 = dt1.GetTime ();
    time_t li2 = dt2.GetTime ();
    if (li1 < li2)
    {
	   return Sort11;
    }
    else if (li1 > li2)
    {
	   return (-1 * Sort11);
    }
    return 0;
   }
   else if (SortRow == 11)
   {
// pers
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort12;
   }
   else if (SortRow == 12)
   {
// bsd_lgr_ort
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort13;
   }
   return 0;
}

void CChoiceBsds::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
        case 4:
              Sort5 *= -1;
			  if (Sort5 < 0) SortPos = 1;
              break;
        case 5:
              Sort6 *= -1;
			  if (Sort6 < 0) SortPos = 1;
              break;
        case 6:
              Sort7 *= -1;
			  if (Sort7 < 0) SortPos = 1;
              break;
        case 7:
              Sort8 *= -1;
			  if (Sort8 < 0) SortPos = 1;
              break;
        case 8:
              Sort9 *= -1;
			  if (Sort9 < 0) SortPos = 1;
              break;
        case 9:
              Sort10 *= -1;
			  if (Sort10 < 0) SortPos = 1;
              break;
        case 10:
              Sort11 *= -1;
			  if (Sort11 < 0) SortPos = 1;
              break;
        case 11:
              Sort12 *= -1;
			  if (Sort12 < 0) SortPos = 1;
              break;
        case 12:
              Sort13 *= -1;
			  if (Sort13 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CBsdsList *abl = BsdsList [i];
		   
		   abl->mdn          = _tstoi (ListBox->GetItemText (i, 1));
		   abl->fil          = _tstoi (ListBox->GetItemText (i, 2));
		   abl->a            = _tstof (ListBox->GetItemText (i, 3));
		   abl->a_bz1        = ListBox->GetItemText (i, 4);
		   abl->a_bz2        = ListBox->GetItemText (i, 5);
		   abl->chargennr    = ListBox->GetItemText (i, 6);
		   abl->ident_nr     = ListBox->GetItemText (i, 7);
		   abl->bsd_gew      = _tstof (ListBox->GetItemText (i, 8));
		   abl->bsd_red_grnd = ListBox->GetItemText (i, 9);
		   abl->bsd_update   = ListBox->GetItemText (i, 10);
		   abl->pers         = ListBox->GetItemText (i, 11);
		   abl->bsd_lgr_ort  = ListBox->GetItemText (i, 12);
	}

	for (int i = 1; i <= 12; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);

}

void CChoiceBsds::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = BsdsList [idx];
}

CBsdsList *CChoiceBsds::GetSelectedText ()
{
	CBsdsList *abl = (CBsdsList *) SelectedRow;
	return abl;
}

void CChoiceBsds::SetSelectedText (CBsdsList *abl)
{
	int ret;

    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int i   = ListBox->GetNextItem (-1, LVNI_SELECTED | LVNI_FOCUSED);
	CString Mdn;
	Mdn.Format (_T("%hd"), abl->mdn); 
	CString Fil;
	Fil.Format (_T("%hd"), abl->fil); 
	CString Art;
	Art.Format (_T("%.0lf"), abl->a); 
    ret = SetItemText (Mdn.GetBuffer (), i, 1);
    ret = SetItemText (Fil.GetBuffer (), i, 2);
    ret = SetItemText (Art.GetBuffer (), i, 3);
    ret = SetItemText (abl->a_bz1.GetBuffer (), i, 4);
    ret = SetItemText (abl->a_bz2.GetBuffer (), i, 5);
    ret = SetItemText (abl->chargennr.GetBuffer (), i, 6);
    ret = SetItemText (abl->ident_nr.GetBuffer (), i, 7);
	CString BsdGew;
	BsdGew.Format (_T("%.3lf"), abl->bsd_gew);
    ret = SetItemText (BsdGew.GetBuffer (), i, 8);
    ret = SetItemText (abl->bsd_red_grnd.GetBuffer (), i, 9);
    ret = SetItemText (abl->bsd_update.GetBuffer (), i, 10);
    ret = SetItemText (abl->pers.GetBuffer (), i, 11);
    ret = SetItemText (abl->bsd_lgr_ort.GetBuffer (), i, 12);
}

void CChoiceBsds::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (BsdsList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

void CChoiceBsds::SetDefault ()
{
	CChoiceX::SetDefault ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    ListBox->SetColumnWidth (0, 0);
    ListBox->SetColumnWidth (1, 150);
//	SetColFmt (1, LVCFMT_RIGHT);
    ListBox->SetColumnWidth (2, 250);
    ListBox->SetColumnWidth (3, 250);
    ListBox->SetColumnWidth (4, 150);
    ListBox->SetColumnWidth (5, 150);
    ListBox->SetColumnWidth (6, 150);
    ListBox->SetColumnWidth (7, 150);
    ListBox->SetColumnWidth (8, 150);
    ListBox->SetColumnWidth (9, 150);
    ListBox->SetColumnWidth (10,250);
    ListBox->SetColumnWidth (11,250);
    ListBox->SetColumnWidth (12,250);
}

void CChoiceBsds::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cA = m_List.GetItemText (idx, 1);  
		double a = CStrFuncs::StrToDouble (cA.GetBuffer ());
		Message.Format (_T("ArtikelNr=%.0lf"), a);
		ToClipboard (Message);
	}
	p.SetCommand (_T("12100"));
	HANDLE Pid = p.Start ();
}


void CChoiceBsds::OnFilter ()
{
	CDynDialog dlg;
	CDynDialogItem *Item;

	dlg.DlgCaption = "Filter";
	dlg.UseFilter = UseFilter;

	if (vFilter.size () > 0)
	{
		for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
		{
			CDynDialogItem *i = *it;
			Item = new CDynDialogItem ();
			*Item = *i;
			if (Item->Name == _T("reset"))
			{
				if (UseFilter)
				{
					Item->Value = _T("aktiv");
				}
				else
				{
					Item->Value = _T("nicht aktiv");
				}
			}
			dlg.AddItem (Item);
			dlg.Header.cy = DlgHeight;
		}
	}
	else
	{
		int y = 10;
		Item = new CDynDialogItem ();
		Item->Name = _T("lmdn");
		Item->CtrClass = "static";
		Item->Value = _T("Mandant");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1001;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("bsds.mdn");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1002;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lfil");
		Item->CtrClass = "static";
		Item->Value = _T("Filiale");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1003;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("bsds.fil");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("la");
		Item->CtrClass = "static";
		Item->Value = _T("Artikel");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1005;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("bsds.a");
		Item->Type = Item->Decimal;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1006;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("la_bz1");
		Item->CtrClass = "static";
		Item->Value = _T("Bezeichnung 1");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1007;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("a_bas.a_bz1");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1008;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lbsd_lgr_ort");
		Item->CtrClass = "static";
		Item->Value = _T("Lagerort");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1009;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("bsds.bsd_lgr_ort");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1010;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lchargennr");
		Item->CtrClass = "static";
		Item->Value = _T("Chargen-Nr");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1011;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("bsds.chargennr");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1012;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lident_nr");
		Item->CtrClass = "static";
		Item->Value = _T("Ident-Nr");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1013;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("bsds.ident_nr");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1014;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		dlg.Header.cy = y + 30;
		DlgHeight = dlg.Header.cy;

		Item = new CDynDialogItem ();
		Item->Name = _T("OK");
		Item->CtrClass = "button";
		Item->Value = _T("OK");
		Item->Template.x = 15;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDOK;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("cancel");
		Item->CtrClass = "button";
		Item->Value = _T("abbrechen");
		Item->Template.x = 70;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDCANCEL;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("reset");
		Item->CtrClass = "button";
		if (UseFilter)
		{
			Item->Value = _T("aktiv");
		}
		else
		{
			Item->Value = _T("nicht aktiv");
		}
		Item->Template.x = 125;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = ID_USE;
		dlg.AddItem (Item);
	}

	dlg.CreateModal ();
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK) return;
 
	UseFilter = dlg.UseFilter;
	int count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = dlg.Items.begin (); it != dlg.Items.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (count >= (int) vFilter.size ())
			{
				Item = new CDynDialogItem ();
				*Item = *i;
				vFilter.push_back (Item);
			}
			*vFilter[count] = *i;
			count ++;
	}
    CString Query;
    CDbQuery DbQuery;

	QueryString = _T("");
	if (!UseFilter)
	{
		FillList ();
		return;
	}
	count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (strcmp (i->CtrClass, "edit") != 0) continue; 
			if (i->Value.Trim () != _T(""))
			{
				if (i->Type == i->String || i->Type == i->Date)
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, TRUE);
				}
				else
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, FALSE);
				}
				if (count > 0)
				{
					QueryString += _T(" and ");
				}
                QueryString += Query;
				count ++;
			}
	}
	FillList ();
}
