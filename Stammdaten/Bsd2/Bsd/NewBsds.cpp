// NewBsds.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Bsd.h"
#include "NewBsds.h"
#include "UniFormfield.h"
#include "DbUniCode.h"
#include "StrFuncs.h"
#include "BMap.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define _CRTDBG_MAP_ALLOC
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;

// CNewBsds-Dialogfeld

IMPLEMENT_DYNAMIC(CNewBsds, CDialog)

CNewBsds::CNewBsds(CWnd* pParent /*=NULL*/)
	: CDialog(CNewBsds::IDD, pParent)
{
	Choice = NULL;
	ModalChoice = FALSE;
}

CNewBsds::~CNewBsds()
{

	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}

	Font.DeleteObject ();
	CFormField *f;
	Form.FirstPosition ();
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
}

void CNewBsds::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BORDER, m_Border);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_LFIL, m_LFil);
	DDX_Control(pDX, IDC_FIL, m_Fil);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_A_BZ1, m_ABz1);
	DDX_Control(pDX, IDC_A_BZ2, m_ABz2);
	DDX_Control(pDX, IDC_LLAGER_ORT, m_LLagerOrt);
	DDX_Control(pDX, IDC_LAGER_ORT, m_LagerOrt);
	DDX_Control(pDX, IDC_LCHARGENNR, m_LChargennr);
	DDX_Control(pDX, IDC_CHARGENNR, m_Chargennr);
	DDX_Control(pDX, IDC_LIDENT_NR, m_LIdentNr);
	DDX_Control(pDX, IDC_IDENT_NR, m_IdentNr);
	DDX_Control(pDX, IDC_LBSD_GEW, m_LBsdGew);
	DDX_Control(pDX, IDC_BSD_GEW, m_BsdGew);
	DDX_Control(pDX, IDOK, m_Save);
	DDX_Control(pDX, IDCANCEL, m_Cancel);
}

BEGIN_MESSAGE_MAP(CNewBsds, CDialog)
	ON_BN_CLICKED(IDOK, &CNewBsds::OnBnClickedOk)
	ON_BN_CLICKED(IDC_ACHOICE ,  OnAchoice)
	ON_EN_KILLFOCUS(IDC_A, &CNewBsds::OnEnKillfocusA)
	ON_COMMAND (SELECTED, OnASelected)
	ON_COMMAND (CANCELED, OnACanceled)
END_MESSAGE_MAP()

BOOL CNewBsds::OnInitDialog ()
{
	BOOL ret = CDialog::OnInitDialog ();

	_tcscpy (Bsds.bsd_lgr_ort, _T(""));
	_tcscpy (Bsds.chargennr, _T(""));
	_tcscpy (Bsds.ident_nr, _T(""));
	memcpy (&Bsds.bsds, &bsds_null, sizeof (BSDS));
	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

//	m_Cancel.SetWindowText (_T("Beenden"));
    HBITMAP HbF5 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, IDB_F5T, IDB_F5MASKT);
	m_Cancel.SetBitmap (HbF5);

    HBITMAP HbF12 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, IDB_F12T, IDB_F12MASKT);
	m_Save.SetBitmap (HbF12);

    Form.Add (new CFormField (&m_Mdn,EDIT,       (short *) &Bsds.bsds.mdn, VSHORT));
    Form.Add (new CFormField (&m_Fil,EDIT,       (short *) &Bsds.bsds.fil, VSHORT));
    Form.Add (new CFormField (&m_A,EDIT,         (double *) &Bsds.bsds.a, VDOUBLE, 13, 0));
    Form.Add (new CUniFormField (&m_ABz1,EDIT,   (char *) A_bas.a_bas.a_bz1, VCHAR));
    Form.Add (new CUniFormField (&m_ABz2,EDIT,   (char *) A_bas.a_bas.a_bz2, VCHAR));
	Form.Add (new CFormField (&m_LagerOrt,EDIT,  (char *) Bsds.bsd_lgr_ort, VCHAR));
    Form.Add (new CFormField (&m_Chargennr,EDIT, (char *) Bsds.chargennr, VCHAR));
    Form.Add (new CFormField (&m_IdentNr,EDIT,   (char *) Bsds.ident_nr, VCHAR));
	Form.Add (new CFormField (&m_BsdGew,EDIT,    (double *) &Bsds.bsds.bsd_gew, VDOUBLE, 12, 3));

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (10, 10);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    DataGrid.Create (this, 20, 20);
    DataGrid.SetBorder (10, 10);
    DataGrid.SetCellHeight (15);
    DataGrid.SetFontCellHeight (this, &Font);
    DataGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    MdnFilGrid.Create (this, 20, 20);
    MdnFilGrid.SetBorder (0, 0);
    MdnFilGrid.SetCellHeight (15);
    MdnFilGrid.SetFontCellHeight (this, &Font);
    MdnFilGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    ArtGrid.Create (this, 20, 20);
    ArtGrid.SetBorder (0, 0);
    ArtGrid.SetCellHeight (15);
    ArtGrid.SetFontCellHeight (this, &Font);
    ArtGrid.SetGridSpace (0, 8);  //Spaltenabstand und Zeilenabstand
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);


	CCtrlInfo *c_Border = new CCtrlInfo (&m_Border, 0, 0, 10, 10);
	CtrlGrid.Add (c_Border);

	CCtrlInfo *c_Data = new CCtrlInfo (&DataGrid, 3, 1, 10, 10);
	CtrlGrid.Add (c_Data);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1);
	MdnFilGrid.Add (c_LMdn);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 1, 0, 1, 1);
	MdnFilGrid.Add (c_Mdn);
	CCtrlInfo *c_LFil = new CCtrlInfo (&m_LFil, 2, 0, 1, 1);
	c_LFil->SetCellPos (50, 0, 0, 0);
	MdnFilGrid.Add (c_LFil);
	CCtrlInfo *c_Fil = new CCtrlInfo (&m_Fil, 4, 0, 1, 1);
	MdnFilGrid.Add (c_Fil);

	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnFilGrid, 1, 1, 5, 1);
	c_MdnGrid->SetCellPos (50, 0, 0, 0);
	DataGrid.Add (c_MdnGrid);

	CCtrlInfo *c_LA = new CCtrlInfo (&m_LA, 1, 3, 1, 1);
	DataGrid.Add (c_LA);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 1, 0, 1, 1);
	ArtGrid.Add (c_A);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 2, 0, 1, 1);
	ArtGrid.Add (c_AChoice);
	CCtrlInfo *c_ABz1 = new CCtrlInfo (&m_ABz1, 3, 0, 1, 1);
	c_ABz1->SetCellPos (10, 0);
	ArtGrid.Add (c_ABz1);
	CCtrlInfo *c_ABz2 = new CCtrlInfo (&m_ABz2, 3, 1, 1, 1);
	c_ABz2->SetCellPos (10, 0);

	ArtGrid.Add (c_ABz2);

	CCtrlInfo *c_ArtGrid = new CCtrlInfo (&ArtGrid, 2, 3, 5, 1);
	DataGrid.Add (c_ArtGrid);

	CCtrlInfo *c_LLagerOrt = new CCtrlInfo (&m_LLagerOrt, 1, 6, 1, 1);
	DataGrid.Add (c_LLagerOrt);
	CCtrlInfo *c_LagerOrt = new CCtrlInfo (&m_LagerOrt, 2, 6, 3, 1);
	DataGrid.Add (c_LagerOrt);

	CCtrlInfo *c_LChargennr = new CCtrlInfo (&m_LChargennr, 1, 7, 1, 1);
	DataGrid.Add (c_LChargennr);
	CCtrlInfo *c_Chargennr = new CCtrlInfo (&m_Chargennr, 2, 7, 3, 1);
	DataGrid.Add (c_Chargennr);

	CCtrlInfo *c_LIdentNr = new CCtrlInfo (&m_LIdentNr, 1, 8, 1, 1);
	DataGrid.Add (c_LIdentNr);
	CCtrlInfo *c_IdentNr = new CCtrlInfo (&m_IdentNr, 2, 8, 3, 1);
	DataGrid.Add (c_IdentNr);

	CCtrlInfo *c_LBsdGew = new CCtrlInfo (&m_LBsdGew, 1,10, 1, 1);
	DataGrid.Add (c_LBsdGew);
	CCtrlInfo *c_BsdGew = new CCtrlInfo (&m_BsdGew, 2, 10, 1, 1);
	DataGrid.Add (c_BsdGew);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	Form.Show ();
	CtrlGrid.Display ();

	return TRUE;
}

// CNewBsds-Meldungshandler


BOOL CNewBsds::PreTranslateMessage(MSG* pMsg)
{
//	CWnd *cWnd = NULL;
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :

			if (pMsg->wParam == VK_RETURN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CNewBsds::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (Write ())
	{
		OnOK();
	}
}

BOOL CNewBsds::Write ()
{
/*
	if (!CanWrite)
	{
		return FALSE;
	}
*/

	Form.Get ();
	if (Bsds.bsds.a == 0.0)
	{
		return FALSE;
	}
	if (Bsds.dbreadfirst () == 0)
	{
		AfxMessageBox (_T("Es existiert schon ein Eintrag f�r diese Artikelnummer und Chargennummer")); 
		return FALSE;
	}

	Form.Get ();
	_tcscpy (Bsds.bsds.bsd_lgr_ort, Bsds.bsd_lgr_ort);
	_tcscpy (Bsds.bsds.chargennr, Bsds.chargennr);
	_tcscpy (Bsds.bsds.ident_nr, Bsds.ident_nr);
	CString Date;
	CStrFuncs::SysDate (Date);
	Bsds.ToDbDate (Date, &Bsds.bsds.bsd_update);
	CString Time;
	CStrFuncs::SysTime (Time);
	_tcscpy (Bsds.bsds.bsd_upzeit, Time.GetBuffer ());
	_tcscpy (Bsds.bsds.pers, PersName.GetBuffer ());

	Bsds.dbupdate ();
	_tcscpy (Bsds.bsd_lgr_ort, Bsds.bsds.bsd_lgr_ort);
	_tcscpy (Bsds.chargennr, Bsds.bsds.chargennr);
	_tcscpy (Bsds.ident_nr, Bsds.bsds.ident_nr);
	Form.Show ();
	return TRUE;
}

void CNewBsds::OnEnKillfocusA()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	A_bas.a_bas.a = Bsds.bsds.a;
	if (A_bas.a_bas.a == 0.0)
	{
		return;
	}
	if (A_bas.dbreadfirst () != 0)
	{
		AfxMessageBox (_T("Artikel nicht gefunden"));
		m_A.SetFocus ();
		return;
	}
	Form.Show ();
}

void CNewBsds::OnAchoice ()
{
//    AChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceA (this);
		Choice->Bean.ArchiveName = _T("BsdAList.prp");

	    Choice->IsModal = ModalChoice;
		Choice->HideEnter = FALSE;
	    Choice->HideFilter = FALSE;
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->CreateDlg ();
	}

	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->SearchText = _T("");
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

		Choice->MoveWindow (&rect);
		Choice->SetFocus ();

		return;
	}
    if (Choice->GetState ())
    {
		  CABasList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_A.SetSel (0, -1, TRUE);
		  m_A.SetFocus ();
//		  PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CNewBsds::OnASelected ()
{
	if (Choice == NULL) return;
    CABasList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    Bsds.bsds.a = abl->a;
    A_bas.a_bas.a = abl->a;
    if (A_bas.dbreadfirst () == 0)
    {
//	    m_A.SetSel (0, -1, TRUE);
//		m_A.SetFocus ();
//		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
//	if (CloseChoice)
	{
//		OnACanceled (); 
	}
    Form.Show ();
	if (Choice->FocusBack)
	{
//		Read ();
//		m_List.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CNewBsds::OnACanceled ()
{
	Choice->ShowWindow (SW_HIDE);
	Form.Get ();
	CString BsdLgrOrt = Bsds.bsds.bsd_lgr_ort;
	CString Chargennr = Bsds.bsds.chargennr;
	CString Ident_nr = Bsds.bsds.ident_nr;
	if (Bsds.bsds.a == 0.0)
	{
	    m_A.SetSel (0, -1, TRUE);
		m_A.SetFocus ();
	}
	else if (BsdLgrOrt.Trim () == _T("")) 
	{
		m_LagerOrt.SetFocus ();
	}
	else if (Chargennr.Trim () == _T("")) 
	{
		m_Chargennr.SetFocus ();
	}
	else if (Ident_nr.Trim () == _T("")) 
	{
		m_IdentNr.SetFocus ();
	}
	else
	{
		m_BsdGew.SetFocus ();
	}
}


