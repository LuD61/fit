#include "stdafx.h"
#include "Bsd.h"
#include "BsdData.h"
#include "UniFormfield.h"
#include "DbUniCode.h"
#include "ChoiceBsds.h"
#include "StrFuncs.h"
#include "Util.h"
#include "CtrlLine.h"
#include "ArtImages.h"
#include "NewBsds.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define _CRTDBG_MAP_ALLOC
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;

// CBsdData-Dialogfeld

IMPLEMENT_DYNAMIC(CBsdData, CDbPropertyPage)

CBsdData::CBsdData(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage(CBsdData::IDD)
{
	Choice = NULL;
	CanWrite = FALSE;
	InRead = FALSE;
    ShowImage = FALSE; 
	CDlgBean::ArchiveName = _T("BsdDlg.prp");
	CDlgBean::Load ();
}

CBsdData::~CBsdData()
{
	CDlgBean::Save ();
	Font.DeleteObject ();
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
}

void CBsdData::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_LFIL, m_LFil);
	DDX_Control(pDX, IDC_FIL, m_Fil);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_A_BZ1, m_ABz1);
	DDX_Control(pDX, IDC_A_BZ2, m_ABz2);
	DDX_Control(pDX, IDC_LLAGER_ORT_SEARCH, m_LLagerOrtSearch);
	DDX_Control(pDX, IDC_LAGER_ORT_SEARCH, m_LagerOrtSearch);
	DDX_Control(pDX, IDC_LCHARGENNR_SEARCH, m_LChargennrSearch);
	DDX_Control(pDX, IDC_CHARGENNR_SEARCH, m_ChargennrSearch);
	DDX_Control(pDX, IDC_LIDENT_NR_SEARCH, m_LIdentNrSearch);
	DDX_Control(pDX, IDC_IDENT_NR_SEARCH, m_IdentNrSearch);
	DDX_Control(pDX, IDC_LLAGER_ORT, m_LLagerOrt);
	DDX_Control(pDX, IDC_LAGER_ORT, m_LagerOrt);
	DDX_Control(pDX, IDC_INFO, m_Info);
	DDX_Control(pDX, IDC_LCHARGENNR, m_LChargennr);
	DDX_Control(pDX, IDC_CHARGENNR, m_Chargennr);
	DDX_Control(pDX, IDC_LIDENT_NR, m_LIdentNr);
	DDX_Control(pDX, IDC_IDENT_NR, m_IdentNr);
	DDX_Control(pDX, IDC_LBSD_UPDATE, m_LBsdUpdate);
	DDX_Control(pDX, IDC_BSD_UPDATE, m_BsdUpdate);
	DDX_Control(pDX, IDC_BSD_UPZEIT, m_BsdZeit);
	DDX_Control(pDX, IDC_LBSD_RES_GRUND, m_LBsdResGrund);
	DDX_Control(pDX, IDC_BSD_RES_GRUND, m_BsdResGrund);
	DDX_Control(pDX, IDC_LBSD_GEW, m_LBsdGew);
	DDX_Control(pDX, IDC_BSD_GEW, m_BsdGew);
	DDX_Control(pDX, IDC_LPERS, m_LPers);
	DDX_Control(pDX, IDC_PERS, m_Pers);
}

BEGIN_MESSAGE_MAP(CBsdData, CDialog)
	ON_WM_CTLCOLOR ()
	ON_WM_RBUTTONDOWN ()
	ON_WM_RBUTTONDOWN ()
	ON_WM_LBUTTONDBLCLK( )
	ON_BN_CLICKED(IDC_SAVE, OnSave)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_EN_KILLFOCUS(IDC_BSD_GEW, &CBsdData::OnEnKillfocusBsdGew)
	ON_EN_KILLFOCUS(IDC_A, OnReadData)
	ON_EN_KILLFOCUS(IDC_LAGER_ORT_SEARCH, OnReadData)
	ON_EN_KILLFOCUS(IDC_CHARGENNR_SEARCH, OnReadData)
	ON_EN_KILLFOCUS(IDC_IDENT_NR_SEARCH, OnReadData)
	ON_COMMAND (SELECTED, OnSelected)
	ON_COMMAND (CANCELED, OnCanceled)
	ON_COMMAND(ID_LINK_IMAGE, OnLinkImage)
	ON_COMMAND(ID_UNLINK_IMAGE, OnUnlinkImage)
END_MESSAGE_MAP()

BOOL CBsdData::OnInitDialog ()
{
	BOOL ret = CDbPropertyPage::OnInitDialog ();
	SetWindowText (_T("Bestandserfassung"));
	m_ABz1.SetReadOnly (TRUE);
	m_ABz2.SetReadOnly (TRUE);
//	m_BsdUpdate.SetReadOnly (TRUE);
//	m_BsdZeit.SetReadOnly (TRUE);
	m_Pers.SetReadOnly (TRUE);
	m_ABz1.ModifyStyle (WS_TABSTOP, 0);
	m_ABz2.ModifyStyle (WS_TABSTOP, 0);
	m_BsdUpdate.ModifyStyle (WS_TABSTOP, 0);
	m_BsdZeit.ModifyStyle (WS_TABSTOP, 0);
	m_Pers.ModifyStyle (WS_TABSTOP, 0);

    m_ImageArticle.Create (_T("Bild"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, CRect (0, 0, 200, 150),
		                   this, IDC_ARTIMAGE);

    m_Save.Create (_T(""), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 32, 32),
		                          this, IDC_SAVE);
	m_Save.LoadBitmap (IDB_SAVE);
	m_Save.SetToolTip (_T("Satz speichern F12"));
	m_Save.Tooltip.WindowOrientation = CQuikInfo::Center;

    m_New.Create (_T(""), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 32, 32),
		                          this, IDC_NEW);
	m_New.LoadBitmap (IDB_NEW);
	m_New.LoadMask (IDB_NEW_MASK);
	m_New.SetToolTip (_T("Neuer Eintrag F6"));
	m_New.Tooltip.WindowOrientation = CQuikInfo::Center;

	CUtil::GetPersName (PersName);

	Bsds.opendbase (_T("bws"));
	_tcscpy (Bsds.bsd_lgr_ort, _T(""));
	_tcscpy (Bsds.chargennr, _T(""));
	_tcscpy (Bsds.ident_nr, _T(""));
	memcpy (&Bsds.bsds, &bsds_null, sizeof (BSDS));
	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

    Form.Add (new CFormField (&m_Mdn,EDIT,  (short *) &Bsds.bsds.mdn, VSHORT));
    Form.Add (new CFormField (&m_Fil,EDIT,  (short *) &Bsds.bsds.fil, VSHORT));
    Form.Add (new CFormField (&m_A,EDIT,       (double *) &Bsds.bsds.a, VDOUBLE, 13, 0));
    Form.Add (new CUniFormField (&m_ABz1,EDIT,   (char *) A_bas.a_bas.a_bz1, VCHAR));
    Form.Add (new CUniFormField (&m_ABz2,EDIT,   (char *) A_bas.a_bas.a_bz2, VCHAR));
	Form.Add (new CFormField (&m_LagerOrtSearch,EDIT,  (char *) Bsds.bsd_lgr_ort, VCHAR));
    Form.Add (new CFormField (&m_ChargennrSearch,EDIT,  (char *) Bsds.chargennr, VCHAR));
    Form.Add (new CFormField (&m_IdentNrSearch,EDIT,   (char *) Bsds.ident_nr, VCHAR));

	Form.Add (new CFormField (&m_LagerOrt,EDIT,  (char *) Bsds.bsds.bsd_lgr_ort, VCHAR));
    Form.Add (new CFormField (&m_Chargennr,EDIT,  (char *) Bsds.bsds.chargennr, VCHAR));
    Form.Add (new CFormField (&m_IdentNr,EDIT,   (char *) Bsds.bsds.ident_nr, VCHAR));
    Form.Add (new CFormField (&m_BsdUpdate,DATETIMEPICKER, (DATE_STRUCT *) &Bsds.bsds.bsd_update, VDATE));
    Form.Add (new CFormField (&m_BsdZeit,TIMEPICKER, (char *) Bsds.bsds.bsd_upzeit, VCHAR));
    Form.Add (new CFormField (&m_BsdResGrund,COMBOBOX, (short *) &Bsds.bsds.bsd_red_grnd, VSHORT));
    Form.Add (new CFormField (&m_Pers,EDIT, (char *) Bsds.bsds.pers, VCHAR));
	Form.Add (new CFormField (&m_BsdGew,EDIT,       (double *) &Bsds.bsds.bsd_gew, VDOUBLE, 12, 3));
	FillCombo (_T("bsd_red_grnd"), &m_BsdResGrund);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (10, 10);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    HeadGrid.Create (this, 20, 20);
    HeadGrid.SetBorder (10, 10);
    HeadGrid.SetCellHeight (15);
    HeadGrid.SetFontCellHeight (this, &Font);
    HeadGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    DataGrid.Create (this, 20, 20);
    DataGrid.SetBorder (10, 10);
    DataGrid.SetCellHeight (15);
    DataGrid.SetFontCellHeight (this, &Font);
    DataGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    MdnFilGrid.Create (this, 20, 20);
    MdnFilGrid.SetBorder (0, 0);
    MdnFilGrid.SetCellHeight (15);
    MdnFilGrid.SetFontCellHeight (this, &Font);
    MdnFilGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    ArtGrid.Create (this, 20, 20);
    ArtGrid.SetBorder (0, 0);
    ArtGrid.SetCellHeight (15);
    ArtGrid.SetFontCellHeight (this, &Font);
    ArtGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    DateTimeGrid.Create (this, 20, 20);
    DateTimeGrid.SetBorder (0, 0);
    DateTimeGrid.SetCellHeight (15);
    DateTimeGrid.SetFontCellHeight (this, &Font);
    DateTimeGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1);
	MdnFilGrid.Add (c_LMdn);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 1, 0, 1, 1);
	MdnFilGrid.Add (c_Mdn);
	CCtrlInfo *c_LFil = new CCtrlInfo (&m_LFil, 2, 0, 1, 1);
	c_LFil->SetCellPos (50, 0, 0, 0);
	MdnFilGrid.Add (c_LFil);
	CCtrlInfo *c_Fil = new CCtrlInfo (&m_Fil, 4, 0, 1, 1);
	MdnFilGrid.Add (c_Fil);

	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnFilGrid, 0, 1, 5, 1);
	c_MdnGrid->SetCellPos (50, 0, 0, 0);
	HeadGrid.Add (c_MdnGrid);

	CCtrlInfo *c_LA = new CCtrlInfo (&m_LA, 0, 3, 1, 1);
	HeadGrid.Add (c_LA);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 1, 0, 1, 1);
	ArtGrid.Add (c_A);
	CCtrlInfo *c_ABz1 = new CCtrlInfo (&m_ABz1, 2, 0, 1, 1);
	ArtGrid.Add (c_ABz1);
	CCtrlInfo *c_ABz2 = new CCtrlInfo (&m_ABz2, 2, 1, 1, 1);
	ArtGrid.Add (c_ABz2);

	CCtrlInfo *c_ArtGrid = new CCtrlInfo (&ArtGrid, 1, 3, 5, 1);
	HeadGrid.Add (c_ArtGrid);

    ControlGrid.Create (this, 1, 6);
    ControlGrid.SetBorder (10, 10);
    ControlGrid.SetCellHeight (15);
    ControlGrid.SetFontCellHeight (this, &Font);
    ControlGrid.SetGridSpace (2, 8);  //Spaltenabstand und Zeilenabstand


	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 0, 1, 1);
	ControlGrid.Add (c_Save);

	CCtrlInfo *c_New = new CCtrlInfo (&m_New, 1, 0, 1, 1);
	ControlGrid.Add (c_New);

	CCtrlInfo *c_Control = new CCtrlInfo (&ControlGrid, 5, 1, 5, 1);
	c_Control->SetCellPos (30, 0, 0, 0);
	HeadGrid.Add (c_Control);

	CCtrlInfo *c_LLagerOrtSearch = new CCtrlInfo (&m_LLagerOrtSearch, 0, 5, 1, 1);
	HeadGrid.Add (c_LLagerOrtSearch);
	CCtrlInfo *c_LagerOrtSearch = new CCtrlInfo (&m_LagerOrtSearch, 1, 5, 3, 1);
	HeadGrid.Add (c_LagerOrtSearch);

	CCtrlInfo *c_LChargennrSearch = new CCtrlInfo (&m_LChargennrSearch, 0, 6, 1, 1);
	HeadGrid.Add (c_LChargennrSearch);
	CCtrlInfo *c_ChargennrSearch = new CCtrlInfo (&m_ChargennrSearch, 1, 6, 3, 1);
	HeadGrid.Add (c_ChargennrSearch);

	CCtrlInfo *c_LIdentNrSearch = new CCtrlInfo (&m_LIdentNrSearch, 0, 7, 1, 1);
	HeadGrid.Add (c_LIdentNrSearch);
	CCtrlInfo *c_IdentNrSearch = new CCtrlInfo (&m_IdentNrSearch, 1, 7, 3, 1);
	HeadGrid.Add (c_IdentNrSearch);

	CCtrlInfo *c_HeadGrid = new CCtrlInfo (&HeadGrid, 0, 0, 6, 5);
	CtrlGrid.Add (c_HeadGrid);
	CCtrlLine *c_HeadLine = new CCtrlLine (this, 
		                               HORIZONTAL, 100, 2, 
									   0, 7, DOCKRIGHT, 1);
	c_HeadLine->SetCellPos (0, 5);
	c_HeadLine->rightspace = 25;
	CtrlGrid.Add (c_HeadLine);


	int yplus = 4;
	CCtrlInfo *c_Info = new CCtrlInfo (&m_Info, 0, 0, 4, 1);
	DataGrid.Add (c_Info);
	CCtrlInfo *c_LLagerOrt = new CCtrlInfo (&m_LLagerOrt, 0, 2, 1, 1);
	DataGrid.Add (c_LLagerOrt);
	CCtrlInfo *c_LagerOrt = new CCtrlInfo (&m_LagerOrt, 1, 2, 3, 1);
	DataGrid.Add (c_LagerOrt);

	CCtrlInfo *c_LChargennr = new CCtrlInfo (&m_LChargennr, 0, 3, 1, 1);
	DataGrid.Add (c_LChargennr);
	CCtrlInfo *c_Chargennr = new CCtrlInfo (&m_Chargennr, 1, 3, 3, 1);
	DataGrid.Add (c_Chargennr);

	CCtrlInfo *c_LIdentNr = new CCtrlInfo (&m_LIdentNr, 0, 4, 1, 1);
	DataGrid.Add (c_LIdentNr);
	CCtrlInfo *c_IdentNr = new CCtrlInfo (&m_IdentNr, 1, 4, 3, 1);
	DataGrid.Add (c_IdentNr);

	CCtrlInfo *c_LBsdGew = new CCtrlInfo (&m_LBsdGew, 0, 2 + yplus, 1, 1);
	DataGrid.Add (c_LBsdGew);
	CCtrlInfo *c_BsdGew = new CCtrlInfo (&m_BsdGew, 1, 2 + yplus, 1, 1);
	DataGrid.Add (c_BsdGew);

	CCtrlInfo *c_LBsdResGrund = new CCtrlInfo (&m_LBsdResGrund, 0, 3 + yplus, 1, 1);
	DataGrid.Add (c_LBsdResGrund);
	CCtrlInfo *c_BsdResGrund = new CCtrlInfo (&m_BsdResGrund, 1, 3 + yplus, 3, 1);
	DataGrid.Add (c_BsdResGrund);

	CCtrlInfo *c_LBsdUpdate = new CCtrlInfo (&m_LBsdUpdate, 0, 4 + yplus, 1, 1);
	DataGrid.Add (c_LBsdUpdate);

	CCtrlInfo *c_BsdUpdate = new CCtrlInfo (&m_BsdUpdate, 1, 0, 1, 1);
	DateTimeGrid.Add (c_BsdUpdate);
	CCtrlInfo *c_BsdZeit = new CCtrlInfo (&m_BsdZeit, 2, 0, 1, 1);
	DateTimeGrid.Add (c_BsdZeit);

	CCtrlInfo *c_DateTimeGrid = new CCtrlInfo (&DateTimeGrid, 1, 4 + yplus, 5, 1);
	DataGrid.Add (c_DateTimeGrid);

	CCtrlInfo *c_LPers = new CCtrlInfo (&m_LPers, 2, 2 + yplus, 1, 1);
	c_LPers->SetCellPos (5, 0);
	DataGrid.Add (c_LPers);
	CCtrlInfo *c_Pers = new CCtrlInfo (&m_Pers, 3, 2 + yplus, 1, 1);
	c_Pers->SetCellPos (-5, 0);
	DataGrid.Add (c_Pers);

	CCtrlInfo *c_DataGrid = new CCtrlInfo (&DataGrid, 0, 8, 5, 1);
	CtrlGrid.Add (c_DataGrid);

	CCtrlLine *c_Line = new CCtrlLine (this, 
		                               HORIZONTAL, 100, 2, 
									   0, 17, DOCKRIGHT, 1);
	c_Line->rightspace = 25;
	c_Line->SetCellPos (0, 5);
	CtrlGrid.Add (c_Line);

// ==== Bild =====
	CCtrlInfo *c_ImageArticle = new CCtrlInfo (&m_ImageArticle, 1, 18, 2, 7);
	CtrlGrid.Add (c_ImageArticle);



	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Save.Tooltip.SetFont (&Font);
	m_Save.Tooltip.SetSize ();
	m_New.Tooltip.SetFont (&Font);
	m_New.Tooltip.SetSize ();

	EnableFields (FALSE);

	Form.Show ();
	CtrlGrid.Display ();

	return TRUE;
}

void CBsdData::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

HBRUSH CBsdData::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && CDlgBean::DlgBkColor != NULL)
	{
		    if (CDlgBean::DlgBrush == NULL)
			{
				CDlgBean::DlgBrush = CreateSolidBrush (CDlgBean::DlgBkColor);
			}
			m_Save.SetBkColor (CDlgBean::DlgBkColor);
			m_New.SetBkColor (CDlgBean::DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return CDlgBean::DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && CDlgBean::DlgBkColor != NULL)
	{
			if (pWnd == &m_Info)
			{
				if (CanWrite)
				{
					pDC->SetTextColor (RGB (0, 128, 0));
				}
				else
				{
					pDC->SetTextColor (RGB (255, 0, 0));
				}

			}
			else
			{
				pDC->SetTextColor (TextColor);
			}
			pDC->SetBkColor (CDlgBean::DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return CDlgBean::DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CBsdData::PreTranslateMessage(MSG* pMsg)
{
//	CWnd *cWnd = NULL;
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
                if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					if (Choice != NULL)
					{
						Choice->SetFocus ();
						return TRUE;
					}
				}
/*
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					Control = GetNextDlgTabItem (Control, FALSE);
				}
				else
				{
					Control = GetNextDlgTabItem (Control, TRUE);
				}
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
*/
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				PostQuitMessage (0);
			}
			else if (pMsg->wParam == VK_F6)
			{
				OnNew ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
//				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CBsdData::OnReturn ()
{
	CWnd * Control = GetFocus ();
/*
	if (Control == &m_A || 
		Control == &m_Chargennr || 
		Control == &m_IdentNr)
	{
		Read ();
	}
*/
	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
	return TRUE;
}

void CBsdData::OnReadData ()
{
	Read ();
}

BOOL CBsdData::Read ()
{
	if (InRead)
	{
		return FALSE;
	}
	InRead = TRUE;
	Form.Get ();
	if (Choice != NULL && 
		!Choice->SearchRecord (Bsds.bsds.mdn,Bsds.bsds.fil,Bsds.bsds.a,
							   Bsds.bsd_lgr_ort,Bsds.chargennr,Bsds.ident_nr))
	{
		EnableFields (FALSE);
		m_Info.SetWindowTextA (_T("Satz nicht gefunden"));
		InRead = FALSE;
		return FALSE;
	}

	m_ImageArticle.Show (_T(""));
	A_bas.a_bas.a = Bsds.bsds.a;
	if (A_bas.dbreadfirst () == 0)
	{
		Form.Show ();
	}
	if (Bsds.dbreadfirst () == 0)
	{
		Form.Show ();
		EnableFields (TRUE);
		if (ShowImage)
		{
			m_ImageArticle.Show (A_bas.a_bas.bild);
			m_ImageArticle.Invalidate ();
		}
	}
	else
	{
		EnableFields (FALSE);
		m_Info.SetWindowTextA (_T("Satz nicht gefunden"));
	}
	m_Info.SetWindowTextA (_T("Satz kann ge�ndert werden"));
	InRead = FALSE;
	return FALSE;
}

void CBsdData::OnSave ()
{
	Write ();
}

void CBsdData::OnNew ()
{
	CNewBsds dlg;
	dlg.PersName = PersName;
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		if (Choice != NULL)
		{
			Choice->FillList ();
		}
	}
}

BOOL CBsdData::Write ()
{
	if (!CanWrite)
	{
		return FALSE;
	}
	Form.Get ();
	if (Bsds.dbreadfirst () != 0)
	{
		return FALSE;
	}
	Form.Get ();
	CString Date;
	CStrFuncs::SysDate (Date);
	Bsds.ToDbDate (Date, &Bsds.bsds.bsd_update);
	CString Time;
	CStrFuncs::SysTime (Time);
	_tcscpy (Bsds.bsds.bsd_upzeit, Time.GetBuffer ());
	_tcscpy (Bsds.bsds.pers, PersName.GetBuffer ());
//	Form.Show ();

	Bsds.dbupdate ();
	UpdateChoice ();
	_tcscpy (Bsds.bsd_lgr_ort, Bsds.bsds.bsd_lgr_ort);
	_tcscpy (Bsds.chargennr, Bsds.bsds.chargennr);
	_tcscpy (Bsds.ident_nr, Bsds.bsds.ident_nr);
	Form.Show ();
	return TRUE;
}

void CBsdData::FillCombo (LPTSTR Item, CWnd *control)
{
	CFormField *f = Form.GetFormField (control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		CString *ComboValue = new CString ();
		ComboValue->Format (_T("%s %s"), _T("0"),
			                             _T("kein Grund"));
	    f->ComboValues.push_back (ComboValue); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBsdData::OnEnKillfocusBsdGew()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Write ();
}

void CBsdData::OnSelected ()
{
	if (Choice == NULL) return;
    CBsdsList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
	Bsds.bsds.mdn = abl->mdn; 
	Bsds.bsds.fil = abl->fil; 
	Bsds.bsds.a   = abl->a; 
	_tcscpy (Bsds.bsd_lgr_ort, abl->bsd_lgr_ort.GetBuffer ()); 
	_tcscpy (Bsds.chargennr, abl->chargennr.GetBuffer ()); 
	_tcscpy (Bsds.ident_nr,  abl->ident_nr.GetBuffer ()); 
	Form.Show ();
	Read ();
}

void CBsdData::UpdateChoice ()
{
	TCHAR ptbez [37];
	if (Choice == NULL) return;
    CBsdsList *abl = Choice->GetSelectedText (); 
	abl->mdn     = Bsds.bsds.mdn;
	abl->fil     = Bsds.bsds.fil;
	abl->a       = Bsds.bsds.a;
	abl->bsd_lgr_ort = Bsds.bsds.bsd_lgr_ort;
	abl->chargennr = Bsds.bsds.chargennr;
	abl->ident_nr = Bsds.bsds.ident_nr;
	abl->bsd_gew = Bsds.bsds.bsd_gew;
	CString Ptwert;
	Ptwert.Format (_T("%hd"), Bsds.bsds.bsd_red_grnd);
    Choice->GetPtBez (_T("bsd_red_grnd"), Ptwert.GetBuffer (), ptbez);
	abl->bsd_red_grnd.Format (_T("%s"), ptbez);
	abl->bsd_red_grnd.Trim ();
	DB_CLASS::FromDbDate (abl->bsd_update, &Bsds.bsds.bsd_update);
	abl->pers = Bsds.bsds.pers;

	Choice->SetSelectedText (abl);
}

void CBsdData::OnCanceled ()
{
	PostQuitMessage (0);
}

void CBsdData::EnableFields (BOOL b)
{
//	m_Chargennr.EnableWindow (b);
//	m_IdentNr.EnableWindow (b);
//	CDateTimeCtrl m_BsdUpdate;
//	CDateTimeCtrl m_BsdZeit;
	m_BsdResGrund.EnableWindow (b);
	m_BsdGew.EnableWindow (b);
	m_Save.EnableWindow (b);
	CanWrite = b;
}

void CBsdData::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CWnd *control = GetFocus ();
	if (control->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) control)->Copy ();
	}
}

void CBsdData::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CWnd *control = GetFocus ();
	if (control->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) control)->Paste ();
	}
}

void CBsdData::DisplayImage ()
{
	m_ImageArticle.Show (_T(""));
	if (ShowImage)
	{
			m_ImageArticle.Show (A_bas.a_bas.bild);
	}
	m_ImageArticle.Invalidate ();
}

void CBsdData::ActionPerformed (int action)
{

	if (action == m_ImageArticle.Unlink)
	{
		    strcpy (A_bas.a_bas.bild, _T("")); 
	}
	else if (action == m_ImageArticle.Link)
	{
			CArtImages ArtImages;
            CString ServerPath;
			CString LocalPath = m_ImageArticle.GetImageFile ();
			if (ArtImages.MakeServerPath (LocalPath, ServerPath))
			{
			    strcpy (A_bas.a_bas.bild, ServerPath.GetBuffer ()); 
			}
			else
			{
				MessageBox (_T("Server wurde  nicht gefunden"), NULL, MB_OK | MB_ICONERROR);
			    strcpy (A_bas.a_bas.bild, LocalPath.GetBuffer ()); 
				return;
			}
			if (!ArtImages.CopyToServer (LocalPath))
			{
				MessageBox (_T("Bild kann nicht auf den Server kopiert werden"), NULL, MB_OK | MB_ICONERROR);
			}
	}
	else if (action == m_ImageArticle.ImageShow)
	{
			ShowImage = m_ImageArticle.ShowImage;
			CView *parent = (CView *) GetParent ()->GetParent ();
			CDocument *doc = parent->GetDocument ();
//			doc->UpdateAllViews (parent);
	}
	DisplayImage ();
}


void CBsdData::OnLinkImage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_ImageArticle.OnLinkImage ();
}

void CBsdData::OnUnlinkImage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_ImageArticle.OnUnlinkImage ();
}

void CBsdData::OnOpenImage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_ImageArticle.ImageFile = A_bas.a_bas.bild;
	m_ImageArticle.ImageFile.Trim ();
	m_ImageArticle.OnOpenImage ();
}

void CBsdData::OnOpenImageWith()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_ImageArticle.ImageFile = A_bas.a_bas.bild;
	m_ImageArticle.ImageFile.Trim ();
	m_ImageArticle.OnOpenImageWith ();
}

void CBsdData::OnRButtonDown( UINT flags, CPoint point)
{
//	if (!m_Mdn.IsWindowEnabled ())
	{
		m_ImageArticle.ShowImage = ShowImage;   
        m_ImageArticle.ImageListener = this;
		m_ImageArticle.ImageFile = A_bas.a_bas.bild;
		m_ImageArticle.ImageFile.Trim ();
		BOOL ret = m_ImageArticle.OnRButtonDown (flags, point);
	}
}

void CBsdData::OnLButtonDblClk( UINT flags, CPoint  point)
{
//	if (!m_Mdn.IsWindowEnabled ())
	{
		m_ImageArticle.ShowImage = ShowImage;   
        m_ImageArticle.ImageListener = this;
		m_ImageArticle.ImageFile = A_bas.a_bas.bild;
		m_ImageArticle.ImageFile.Trim ();
		BOOL ret = m_ImageArticle.OnLButtonDblClk (flags, point);
	}
}

