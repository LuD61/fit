#pragma once

#include "CtrlGrid.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "Bsds.h"
#include "A_bas.h"
#include "Ptabn.h"
#include "afxwin.h"
#include "StaticButton.h"
#include "ChoiceBsds.h"
#include "ImageCtrl.h"
#include "DlgBean.h"
#include "DbPropertyPage.h"

// CBsdData-Dialogfeld

#define IDC_SAVE 3010
#define IDC_NEW 3011
#define IDC_ARTIMAGE 3005

class CBsdData : public CDbPropertyPage,
				 public CDlgBean,
                 public CImageListener
{
	DECLARE_DYNAMIC(CBsdData)

public:
	CBsdData(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CBsdData();

// Dialogfelddaten
	enum { IDD = IDD_BDS_DATA};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);
//    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual void OnSize (UINT nType, int cx, int cy);
	virtual BOOL OnReturn ();

	DECLARE_MESSAGE_MAP()
    CFormTab Form;
    A_BAS_CLASS A_bas;
    BSDS_CLASS Bsds;
    PTABN_CLASS Ptabn;
	CFont Font;
	CCtrlGrid CtrlGrid;
	CCtrlGrid HeadGrid;
	CCtrlGrid DataGrid;
	CCtrlGrid MdnFilGrid;
	CCtrlGrid ArtGrid;
	CCtrlGrid DateTimeGrid;
	CCtrlGrid ControlGrid;
	BOOL InRead;

public:
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CStatic m_LFil;
	CNumEdit m_Fil;
	CStatic m_LA;
	CNumEdit m_A;
	CTextEdit m_ABz1;
	CTextEdit m_ABz2;

	CStatic m_LLagerOrtSearch;
	CTextEdit m_LagerOrtSearch;
	CStatic m_LChargennrSearch;
	CTextEdit m_ChargennrSearch;
	CStatic m_LIdentNrSearch;
	CTextEdit m_IdentNrSearch;

	CStatic m_Info;
	CStatic m_LLagerOrt;
	CTextEdit m_LagerOrt;
	CStatic m_LChargennr;
	CTextEdit m_Chargennr;
	CStatic m_LIdentNr;
	CTextEdit m_IdentNr;

	CStatic m_LBsdUpdate;
	CDateTimeCtrl m_BsdUpdate;
	CDateTimeCtrl m_BsdZeit;
	CStatic m_LBsdResGrund;
	CComboBox m_BsdResGrund;
	CStatic m_LBsdGew;
	CNumEdit m_BsdGew;
	CStatic m_LPers;
	CTextEdit m_Pers;
	CStaticButton m_Save;
	CStaticButton m_New;
	CImageCtrl m_ImageArticle;
	CChoiceBsds *Choice;
	BOOL CanWrite;
	CString PersName;
	BOOL ShowImage;

	BOOL Read ();
	virtual BOOL Write ();
	virtual void OnSave ();
	virtual void OnNew ();
	void FillCombo (LPTSTR Item, CWnd *control);
public:
	afx_msg void OnEnKillfocusBsdGew();
	afx_msg void OnReadData ();
	virtual void OnSelected ();
	virtual void OnCanceled ();
	void UpdateChoice ();
	void EnableFields (BOOL b);
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	void DisplayImage ();
    void ActionPerformed (int action);
    afx_msg void OnRButtonDown( UINT flags, CPoint point);
    afx_msg void OnLButtonDblClk( UINT flags, CPoint  point);
	afx_msg void OnLinkImage();
	afx_msg void OnUnlinkImage();
	afx_msg void OnOpenImage();
	afx_msg void OnOpenImageWith();
};
