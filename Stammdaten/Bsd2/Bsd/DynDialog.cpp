#include "StdAfx.h"
#include "DynDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define _CRTDBG_MAP_ALLOC
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;


CDynDialogItem& CDynDialogItem::operator= (CDynDialogItem& Item)
{
	Name = Item.Name;
	Type = Item.Type;
	CtrClass = Item.CtrClass;
	memcpy (&Template, &Item.Template, sizeof (Template));
	Value = Item.Value;
	return *this;
}

CDynDialog::CDynDialog(void)
{
	Header.style = WS_THICKFRAME | WS_POPUP | WS_VISIBLE | WS_CAPTION |  
				   DS_SETFONT | WS_SYSMENU | DS_CENTER;
	Header.dwExtendedStyle = NULL;
	Header.cdit = 0;
	Header.x = 0;
	Header.y = 0;
	Header.cx = 200;
	Header.cy = 200;
	Items.clear ();
	Parent = NULL;
	DlgMenu = NULL;
	DlgClass = NULL;
	DlgCaption = _T("");
	FontName = _T("MS SANS SERIF");
	FontSize = 10;
	DlgItempos = NULL;
	CharSize.cx = 8;
	CharSize.cy = 12;
	RowSpace = 15;
	UseFilter = TRUE;
    pDlgTemplate = NULL;
}

CDynDialog::~CDynDialog(void)
{
	for (std::vector<CDynDialogItem *>::iterator it = Items.begin (); 
		                                          it != Items.end (); ++it)
	{
		CDynDialogItem *item = *it;
		delete item;
	}
	Items.clear ();
	if (pDlgTemplate != NULL)
	{
		delete pDlgTemplate;
		pDlgTemplate = NULL;
	}
}

void CDynDialog::AddItem (CDynDialogItem *Item)
{
	Items.push_back (Item);
}

void CDynDialog::CreateItems (void)
{
	if (pDlgTemplate != NULL)
	{
		delete pDlgTemplate;
		pDlgTemplate = NULL;
	}
    DlgItempos = NULL;
    int size = 0x1000;
	int FirstRow;

    pDlgTemplate = new BYTE [size];
    if (pDlgTemplate == NULL)
    {
        return;

	}
	int ItemCount = (int) Items.size ();
	Header.cdit = ItemCount;
    BYTE *pos = (BYTE *) pDlgTemplate;
    memcpy (pDlgTemplate, &Header, sizeof (Header));
    pos += sizeof (Header);
    if (((size_t) pos % 2) != 0)
    {
        pos += 1;
    }

    pos = (BYTE *) AddHeader ((WORD *) pos, (LPSTR) &DlgMenu);
    pos = (BYTE *) AddHeader ((WORD *) pos, (LPSTR) &DlgClass);
    pos = (BYTE *) AddHeader ((WORD *) pos, (LPSTR) DlgCaption);
    pos = (BYTE *) AddFont ((WORD *) pos, FontSize, FontName);
    DlgItempos = pos;
	int i = 0;
	for (std::vector<CDynDialogItem *>::iterator it = Items.begin (); it != Items.end ();
		                                         ++it)
	{
		CDynDialogItem *item = *it;
//		item->Template.id = DLG_ID_OFFS + i;
		if (i == 0) FirstRow = item->Template.y;
		if (RowSpace != 0 && i > 0)
		{
			item->Template.y = FirstRow + i * RowSpace;
		}

		AddItem (&item->Template, item->CtrClass, item->Value);
		Header.cdit ++;
	}
}


WORD *CDynDialog::AddHeader (WORD *pos, LPSTR HeaderItem)
{
    WORD headerWord;
    LPWSTR NewItem;

    memcpy ((LPSTR) &headerWord, HeaderItem, sizeof (WORD));
    if (headerWord == NULL)
    {
            memcpy (pos, (BYTE *) &headerWord, sizeof (WORD));
            pos ++;
    }
    else if (headerWord == 0xFFFF)
    {
            memcpy (pos++, (BYTE *) &headerWord, sizeof (WORD));
            HeaderItem += sizeof (WORD);
            memcpy ((LPSTR) &headerWord, HeaderItem, sizeof (WORD));
            memcpy (pos++, (BYTE *) &headerWord, sizeof (WORD));
            pos ++;
    }
    else
    {
            int bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, HeaderItem, -1, NULL, 0);
            NewItem   = new WCHAR [bytes];
            bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, HeaderItem, -1, NewItem, bytes);
            int len = bytes * 2;
            memcpy (pos, (LPSTR) NewItem, len);
            delete NewItem;
            pos += bytes;
    }
    return pos;
}

WORD *CDynDialog::AddFont (WORD *pos, WORD Size, LPSTR Name)
{
    LPWSTR WideName;

    memcpy (pos, (BYTE *) &Size, sizeof (WORD));
    pos ++;
    int bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Name, -1, NULL, 0);
    WideName  = new WCHAR [bytes];
    bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Name, -1, WideName, bytes);
    int len = bytes * 2;
    memcpy (pos, (LPSTR) WideName, len);
    delete WideName;
    pos += bytes;
    return pos;
}

BOOL CDynDialog::AddItem (DLGITEMTEMPLATE *DlgItem, LPCSTR CtrClass, CString& Value)
{
    LPWSTR NewClass;

    while (((size_t) DlgItempos % 4) != 0)
    {
        DlgItempos += 1;
    }

    memcpy (DlgItempos, DlgItem, sizeof (DLGITEMTEMPLATE));
    DlgItempos += sizeof (DLGITEMTEMPLATE);

    if (((size_t) DlgItempos % 2) != 0)
    {
        DlgItempos += 1;
    }

    int bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, CtrClass, -1, NULL, 0);
    NewClass = new WCHAR [bytes];
    bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, CtrClass, -1, NewClass, bytes);
    int len = bytes * 2;
    memcpy ((LPSTR) DlgItempos, (LPSTR) NewClass, len);
    delete NewClass;
    DlgItempos += len;

#ifndef UNICODE
    bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Value.GetBuffer (), -1, NULL, 0);
    NewClass = new WCHAR [bytes];
    bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Value.GetBuffer (), -1, NewClass, bytes);
    len = bytes * 2;
    memcpy ((LPSTR) DlgItempos, (LPSTR) NewClass, len);
    delete NewClass;
    DlgItempos += len;
#else
	len = Value.GetLength * 2; 
    memcpy ((LPSTR) DlgItempos, (LPSTR) Value.GetBuffer (), len);
    DlgItempos += len;
#endif


    while (((size_t) DlgItempos % 2) != 0)
    {
        DlgItempos += 1;
    }
    WORD Creation = 0;
    memcpy (DlgItempos, (LPSTR) &Creation, sizeof (WORD));
    DlgItempos += sizeof (Creation);
    return TRUE;
}

BOOL CDynDialog::Create ()
{
	CreateItems ();
	return CreateIndirect (pDlgTemplate, Parent);
}

BOOL CDynDialog::CreateModal ()
{
	CreateItems ();
	return InitModalIndirect (pDlgTemplate, Parent);
}

void CDynDialog::CreateDlgTemplate (DLGITEMTEMPLATE *Template, int X, int Y, int Height, int Length, CSize CharSize)
{
	Template->x  = (short) (X * CharSize.cx);
	Template->y  = (short) (Y * CharSize.cy);
	Template->cx = (short) (Length * CharSize.cx);
	Template->cy = (short) (Height * CharSize.cy);
}


BEGIN_MESSAGE_MAP(CDynDialog, CDialog)
	//{{AFX_MSG_MAP(CChoiceX)
//	ON_WM_CTLCOLOR ()
//	ON_BN_CLICKED(IDOK , OnOK)
	ON_BN_CLICKED(2001 , OnReset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CDynDialog::OnReset ()
{
	CButton *m_Use = (CButton *) GetDlgItem (ID_USE);
	if (UseFilter)
	{
		UseFilter = FALSE;
		m_Use->SetWindowText (_T("nicht aktiv"));
	}
	else
	{
		UseFilter = TRUE;
		m_Use->SetWindowText (_T("aktiv"));
	}

}

void CDynDialog::OnOK ()
{

	CDynDialogItem *item;
	for (std::vector<CDynDialogItem *>::iterator it = Items.begin (); it != Items.end ();
		                                         ++it)
	{
		item = *it;
		if (strcmp (item->CtrClass, "edit") == 0)
		{
			CEdit *c = (CEdit *) GetDlgItem (item->Template.id);
			c->GetWindowText (item->Value);
		}
	}
	CDialog::OnOK ();
}

