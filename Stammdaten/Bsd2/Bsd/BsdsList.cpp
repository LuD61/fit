#include "StdAfx.h"
#include "BsdsList.h"

CBsdsList::CBsdsList(void)
{
        mdn = 0;
        fil = 0;
	a = 0.0;
	a_bz1 = _T("");
	a_bz2 = _T("");
	chargennr = _T("");
	ident_nr = _T("");
	bsd_gew = 0.0;
	bsd_red_grnd = _T("");
	bsd_update = _T("");
	pers = _T("");
	bsd_lgr_ort = _T("");
}

CBsdsList::CBsdsList(short mdn,
		  short fil,
                  double a,
                  LPTSTR a_bz1,								                   
                  LPTSTR a_bz2,								                   
                  LPTSTR chargennr,								                   
                  LPTSTR ident_nr,								                   
                  double bsd_gew,
                  LPTSTR bsd_red_grnd,
                  LPTSTR bsd_update,
                  LPTSTR pers,
				  LPTSTR bsd_lgr_ort) 
{
        this->mdn = mdn;
        this->fil = fil;
	this->a             = a;
	this->a_bz1         = a_bz1;
	this->a_bz2         = a_bz2;
	this->chargennr     = chargennr;
	this->ident_nr      = ident_nr;
	this->bsd_gew       = bsd_gew;
	this->bsd_red_grnd  = bsd_red_grnd;
	this->bsd_update    = bsd_update;
	this->pers          = pers;
	this->bsd_lgr_ort   = bsd_lgr_ort;
}

CBsdsList::~CBsdsList(void)
{
}
