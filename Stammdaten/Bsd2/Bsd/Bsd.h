// Bsd.h : Hauptheaderdatei f�r die Bsd-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"       // Hauptsymbole


// CBsdApp:
// Siehe Bsd.cpp f�r die Implementierung dieser Klasse
//

class CBsdApp : public CWinApp
{
public:
	CBsdApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();
    virtual int ExitInstance(); 

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CBsdApp theApp;