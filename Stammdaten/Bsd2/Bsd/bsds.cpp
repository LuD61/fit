#include "stdafx.h"
#include "bsds.h"

struct BSDS bsds, bsds_null;

void BSDS_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)    &bsds.mdn,   SQLSHORT, 0);
            sqlin ((short *)    &bsds.fil,  SQLSHORT, 0);
            sqlin ((double *)   &bsds.a,     SQLDOUBLE, 0);
            sqlin ((char *)     bsd_lgr_ort,  SQLCHAR, sizeof (bsd_lgr_ort));
            sqlin ((char *)     chargennr,  SQLCHAR, sizeof (chargennr));
            sqlin ((char *)     ident_nr,  SQLCHAR, sizeof (ident_nr));
    sqlout ((short *) &bsds.mdn,SQLSHORT,0);
    sqlout ((short *) &bsds.fil,SQLSHORT,0);
    sqlout ((double *) &bsds.a,SQLDOUBLE,0);
    sqlout ((long *) &bsds.bsd_kost_st,SQLLONG,0);
    sqlout ((TCHAR *) bsds.bsd_lgr_ort,SQLCHAR,13);
    sqlout ((long *) &bsds.lief_s,SQLLONG,0);
    sqlout ((long *) &bsds.lief_stk,SQLLONG,0);
    sqlout ((double *) &bsds.lief_gew,SQLDOUBLE,0);
    sqlout ((short *) &bsds.me_einh,SQLSHORT,0);
    sqlout ((double *) &bsds.ek_pr,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &bsds.lief_dat,SQLDATE,0);
    sqlout ((TCHAR *) bsds.lief_zeit_char,SQLCHAR,9);
    sqlout ((long *) &bsds.bsd_stk,SQLLONG,0);
    sqlout ((double *) &bsds.bsd_gew,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &bsds.verf_dat,SQLDATE,0);
    sqlout ((TCHAR *) bsds.qua_status,SQLCHAR,3);
    sqlout ((TCHAR *) bsds.inv_flag,SQLCHAR,2);
    sqlout ((long *) &bsds.inv_stk,SQLLONG,0);
    sqlout ((double *) &bsds.inv_gew,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &bsds.inv_dat,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &bsds.bsd_update,SQLDATE,0);
    sqlout ((TCHAR *) bsds.bsd_upzeit,SQLCHAR,9);
    sqlout ((short *) &bsds.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) bsds.lief,SQLCHAR,17);
    sqlout ((TCHAR *) bsds.lief_best,SQLCHAR,17);
    sqlout ((double *) &bsds.pr_vk,SQLDOUBLE,0);
    sqlout ((long *) &bsds.kun,SQLLONG,0);
    sqlout ((short *) &bsds.bsd_red_grnd,SQLSHORT,0);
    sqlout ((TCHAR *) bsds.pers,SQLCHAR,13);
    sqlout ((long *) &bsds.posi,SQLLONG,0);
    sqlout ((TCHAR *) bsds.chargennr,SQLCHAR,31);
    sqlout ((TCHAR *) bsds.ident_nr,SQLCHAR,21);
    sqlout ((TCHAR *) bsds.herk_nachw,SQLCHAR,14);
    sqlout ((double *) &bsds.wrt,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select bsds.mdn,  bsds.fil,  ")
_T("bsds.a,  bsds.bsd_kost_st,  bsds.bsd_lgr_ort,  bsds.lief_s,  ")
_T("bsds.lief_stk,  bsds.lief_gew,  bsds.me_einh,  bsds.ek_pr,  ")
_T("bsds.lief_dat,  bsds.lief_zeit_char,  bsds.bsd_stk,  bsds.bsd_gew,  ")
_T("bsds.verf_dat,  bsds.qua_status,  bsds.inv_flag,  bsds.inv_stk,  ")
_T("bsds.inv_gew,  bsds.inv_dat,  bsds.bsd_update,  bsds.bsd_upzeit,  ")
_T("bsds.delstatus,  bsds.lief,  bsds.lief_best,  bsds.pr_vk,  bsds.kun,  ")
_T("bsds.bsd_red_grnd,  bsds.pers,  bsds.posi,  bsds.chargennr,  ")
_T("bsds.ident_nr,  bsds.herk_nachw,  bsds.wrt from bsds ")

#line 17 "bsds.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   a = ? ")
                                  _T("and   bsd_lgr_ort = ? ")
                                  _T("and   chargennr = ? ")
                                  _T("and   ident_nr = ?"));
    sqlin ((short *) &bsds.mdn,SQLSHORT,0);
    sqlin ((short *) &bsds.fil,SQLSHORT,0);
    sqlin ((double *) &bsds.a,SQLDOUBLE,0);
    sqlin ((long *) &bsds.bsd_kost_st,SQLLONG,0);
    sqlin ((TCHAR *) bsds.bsd_lgr_ort,SQLCHAR,13);
    sqlin ((long *) &bsds.lief_s,SQLLONG,0);
    sqlin ((long *) &bsds.lief_stk,SQLLONG,0);
    sqlin ((double *) &bsds.lief_gew,SQLDOUBLE,0);
    sqlin ((short *) &bsds.me_einh,SQLSHORT,0);
    sqlin ((double *) &bsds.ek_pr,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &bsds.lief_dat,SQLDATE,0);
    sqlin ((TCHAR *) bsds.lief_zeit_char,SQLCHAR,9);
    sqlin ((long *) &bsds.bsd_stk,SQLLONG,0);
    sqlin ((double *) &bsds.bsd_gew,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &bsds.verf_dat,SQLDATE,0);
    sqlin ((TCHAR *) bsds.qua_status,SQLCHAR,3);
    sqlin ((TCHAR *) bsds.inv_flag,SQLCHAR,2);
    sqlin ((long *) &bsds.inv_stk,SQLLONG,0);
    sqlin ((double *) &bsds.inv_gew,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &bsds.inv_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &bsds.bsd_update,SQLDATE,0);
    sqlin ((TCHAR *) bsds.bsd_upzeit,SQLCHAR,9);
    sqlin ((short *) &bsds.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) bsds.lief,SQLCHAR,17);
    sqlin ((TCHAR *) bsds.lief_best,SQLCHAR,17);
    sqlin ((double *) &bsds.pr_vk,SQLDOUBLE,0);
    sqlin ((long *) &bsds.kun,SQLLONG,0);
    sqlin ((short *) &bsds.bsd_red_grnd,SQLSHORT,0);
    sqlin ((TCHAR *) bsds.pers,SQLCHAR,13);
    sqlin ((long *) &bsds.posi,SQLLONG,0);
    sqlin ((TCHAR *) bsds.chargennr,SQLCHAR,31);
    sqlin ((TCHAR *) bsds.ident_nr,SQLCHAR,21);
    sqlin ((TCHAR *) bsds.herk_nachw,SQLCHAR,14);
    sqlin ((double *) &bsds.wrt,SQLDOUBLE,0);
            sqltext = _T("update bsds set bsds.mdn = ?,  ")
_T("bsds.fil = ?,  bsds.a = ?,  bsds.bsd_kost_st = ?,  ")
_T("bsds.bsd_lgr_ort = ?,  bsds.lief_s = ?,  bsds.lief_stk = ?,  ")
_T("bsds.lief_gew = ?,  bsds.me_einh = ?,  bsds.ek_pr = ?,  ")
_T("bsds.lief_dat = ?,  bsds.lief_zeit_char = ?,  bsds.bsd_stk = ?,  ")
_T("bsds.bsd_gew = ?,  bsds.verf_dat = ?,  bsds.qua_status = ?,  ")
_T("bsds.inv_flag = ?,  bsds.inv_stk = ?,  bsds.inv_gew = ?,  ")
_T("bsds.inv_dat = ?,  bsds.bsd_update = ?,  bsds.bsd_upzeit = ?,  ")
_T("bsds.delstatus = ?,  bsds.lief = ?,  bsds.lief_best = ?,  ")
_T("bsds.pr_vk = ?,  bsds.kun = ?,  bsds.bsd_red_grnd = ?,  bsds.pers = ?,  ")
_T("bsds.posi = ?,  bsds.chargennr = ?,  bsds.ident_nr = ?,  ")
_T("bsds.herk_nachw = ?,  bsds.wrt = ? ")

#line 24 "bsds.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   a = ? ")
                                  _T("and   bsd_lgr_ort = ? ")
                                  _T("and   chargennr = ? ")
                                  _T("and   ident_nr = ?");
            sqlin ((short *)    &bsds.mdn,   SQLSHORT, 0);
            sqlin ((short *)    &bsds.fil,  SQLSHORT, 0);
            sqlin ((double *)   &bsds.a,     SQLDOUBLE, 0);
            sqlin ((char *)     bsd_lgr_ort,  SQLCHAR, sizeof (bsd_lgr_ort));
            sqlin ((char *)     chargennr,  SQLCHAR, sizeof (chargennr));
            sqlin ((char *)     ident_nr,  SQLCHAR, sizeof (ident_nr));
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)    &bsds.mdn,   SQLSHORT, 0);
            sqlin ((short *)    &bsds.fil,  SQLSHORT, 0);
            sqlin ((double *)   &bsds.a,     SQLDOUBLE, 0);
            sqlin ((char *)     bsd_lgr_ort,  SQLCHAR, sizeof (bsd_lgr_ort));
            sqlin ((char *)     chargennr,  SQLCHAR, sizeof (chargennr));
            sqlin ((char *)     ident_nr,  SQLCHAR, sizeof (ident_nr));
            test_upd_cursor = sqlcursor (_T("select a from bsds ")
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   a = ? ")
                                  _T("and   bsd_lgr_ort = ? ")
                                  _T("and   chargennr = ? ")
                                  _T("and   ident_nr = ?"));
            sqlin ((short *)    &bsds.mdn,   SQLSHORT, 0);
            sqlin ((short *)    &bsds.fil,  SQLSHORT, 0);
            sqlin ((double *)   &bsds.a,     SQLDOUBLE, 0);
            sqlin ((char *)     bsd_lgr_ort,  SQLCHAR, sizeof (bsd_lgr_ort));
            sqlin ((char *)     chargennr,  SQLCHAR, sizeof (chargennr));
            sqlin ((char *)     ident_nr,  SQLCHAR, sizeof (ident_nr));
            del_cursor = sqlcursor (_T("delete from bsds ")
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   a = ? ")
                                  _T("and   bsd_lgr_ort = ? ")
                                  _T("and   chargennr = ? ")
                                  _T("and   ident_nr = ?"));
    sqlin ((short *) &bsds.mdn,SQLSHORT,0);
    sqlin ((short *) &bsds.fil,SQLSHORT,0);
    sqlin ((double *) &bsds.a,SQLDOUBLE,0);
    sqlin ((long *) &bsds.bsd_kost_st,SQLLONG,0);
    sqlin ((TCHAR *) bsds.bsd_lgr_ort,SQLCHAR,13);
    sqlin ((long *) &bsds.lief_s,SQLLONG,0);
    sqlin ((long *) &bsds.lief_stk,SQLLONG,0);
    sqlin ((double *) &bsds.lief_gew,SQLDOUBLE,0);
    sqlin ((short *) &bsds.me_einh,SQLSHORT,0);
    sqlin ((double *) &bsds.ek_pr,SQLDOUBLE,0);
    sqlin ((TCHAR *) bsds.lief_zeit_char,SQLCHAR,9);
    sqlin ((long *) &bsds.bsd_stk,SQLLONG,0);
    sqlin ((double *) &bsds.bsd_gew,SQLDOUBLE,0);
    sqlin ((TCHAR *) bsds.qua_status,SQLCHAR,3);
    sqlin ((TCHAR *) bsds.inv_flag,SQLCHAR,2);
    sqlin ((long *) &bsds.inv_stk,SQLLONG,0);
    sqlin ((double *) &bsds.inv_gew,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &bsds.bsd_update,SQLDATE,0);
    sqlin ((TCHAR *) bsds.bsd_upzeit,SQLCHAR,9);
    sqlin ((short *) &bsds.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) bsds.lief,SQLCHAR,17);
    sqlin ((TCHAR *) bsds.lief_best,SQLCHAR,17);
    sqlin ((double *) &bsds.pr_vk,SQLDOUBLE,0);
    sqlin ((long *) &bsds.kun,SQLLONG,0);
    sqlin ((short *) &bsds.bsd_red_grnd,SQLSHORT,0);
    sqlin ((TCHAR *) bsds.pers,SQLCHAR,13);
    sqlin ((long *) &bsds.posi,SQLLONG,0);
    sqlin ((TCHAR *) bsds.chargennr,SQLCHAR,31);
    sqlin ((TCHAR *) bsds.ident_nr,SQLCHAR,21);
    sqlin ((TCHAR *) bsds.herk_nachw,SQLCHAR,14);
    sqlin ((double *) &bsds.wrt,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into bsds (")
_T("mdn,  fil,  a,  bsd_kost_st,  bsd_lgr_ort,  lief_s,  lief_stk,  lief_gew,  me_einh,  ")
_T("ek_pr, lief_zeit_char,  bsd_stk,  bsd_gew, qua_status,  ")
_T("inv_flag,  inv_stk,  inv_gew, bsd_update,  bsd_upzeit,  delstatus,  ")
_T("lief,  lief_best,  pr_vk,  kun,  bsd_red_grnd,  pers,  posi,  chargennr,  ident_nr,  ")
_T("herk_nachw,  wrt) ")

#line 65 "bsds.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 67 "bsds.rpp"
}
