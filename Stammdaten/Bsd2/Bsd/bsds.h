#ifndef _BSDS_DEF
#define _BSDS_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct BSDS {
   short          mdn;
   short          fil;
   double         a;
   long           bsd_kost_st;
   TCHAR          bsd_lgr_ort[13];
   long           lief_s;
   long           lief_stk;
   double         lief_gew;
   short          me_einh;
   double         ek_pr;
   DATE_STRUCT    lief_dat;
   TCHAR          lief_zeit_char[9];
   long           bsd_stk;
   double         bsd_gew;
   DATE_STRUCT    verf_dat;
   TCHAR          qua_status[3];
   TCHAR          inv_flag[2];
   long           inv_stk;
   double         inv_gew;
   DATE_STRUCT    inv_dat;
   DATE_STRUCT    bsd_update;
   TCHAR          bsd_upzeit[9];
   short          delstatus;
   TCHAR          lief[17];
   TCHAR          lief_best[17];
   double         pr_vk;
   long           kun;
   short          bsd_red_grnd;
   TCHAR          pers[13];
   long           posi;
   TCHAR          chargennr[31];
   TCHAR          ident_nr[21];
   TCHAR          herk_nachw[14];
   double         wrt;
};
extern struct BSDS bsds, bsds_null;

#line 8 "bsds.rh"

class BSDS_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
		       			
               BSDS bsds;
	       TCHAR bsd_lgr_ort[13];
	       TCHAR chargennr[31];
	       TCHAR ident_nr[21];

               BSDS_CLASS () : DB_CLASS ()
               {
               }
};
#endif
