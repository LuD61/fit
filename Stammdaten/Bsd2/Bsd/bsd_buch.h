#ifndef _BSD_BUCH_DEF
#define _BSD_BUCH_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct BSD_BUCH {
   long           nr;
   TCHAR          blg_typ[3];
   short          mdn;
   short          fil;
   short          kun_fil;
   double         a;
   DATE_STRUCT    dat;
   TCHAR          zeit[9];
   TCHAR          pers[13];
   TCHAR          bsd_lgr_ort[13];
   short          qua_status;
   double         me;
   double         bsd_ek_vk;
   TCHAR          chargennr[31];
   TCHAR          ident_nr[21];
   TCHAR          herk_nachw[14];
   TCHAR          lief[17];
   long           auf;
   TCHAR          verfall[2];
   DATE_STRUCT    verf_dat;
   short          delstatus;
   TCHAR          err_txt[17];
   DATE_STRUCT    lieferdat;
   double         me2;
   short          me_einh2;
};
extern struct BSD_BUCH bsd_buch, bsd_buch_null;

#line 8 "bsd_buch.rh"

extern struct BSD_BUCH bsd_buch, bsd_buch_null; 

class BSD_BUCH_CLASS : public DB_CLASS
{
       private :
             int ins_cursor;
             void prepare (void);
       public :
             BSD_BUCH bsd_buch;
             BSD_BUCH_CLASS () : ins_cursor (-1)
             {
             }

             int dbinsert (void);
             void dbclose (void);
}; 
#endif

           

