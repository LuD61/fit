// MainFrm.cpp : Implementierung der Klasse CMainFrame
//

#include "stdafx.h"
#include "Bsd.h"
#include "BsdDoc.h"
#include "BsdView.h"
#include "SplitPane.h"
#include "ChoiceBsds.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
//	ON_WM_SIZE ()
	ON_WM_CREATE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // Statusleistenanzeige
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame-Erstellung/Zerst�rung

CMainFrame::CMainFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Fehler beim Erstellen der Symbolleiste.\n");
		return -1;      // Fehler beim Erstellen
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Fehler beim Erstellen der Statusleiste.\n");
		return -1;      // Fehler beim Erstellen
	}

	// TODO: L�schen Sie diese drei Zeilen, wenn Sie nicht m�chten, dass die Systemleiste andockbar ist
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

//	SetTitle (_T("Bestandskorrektur"));

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return TRUE;
}

void CMainFrame::OnSize (UINT nType, int cx, int cy)
{
/*
	CBsdView *view = (CBsdView *) this->GetActiveView ();
	if (view != NULL)
	{
		if (IsWindow (view->SplitPane.m_hWnd))
		{
			if (view->Direction == CSplitPane::Vertical)
			{
				CRect rect;
				view->ChoiceBsds->GetWindowRect (&rect);
				ScreenToClient (&rect);
				rect.right = cx;
				view->ChoiceBsds->MoveWindow (&rect);

				view->BsdData->GetWindowRect (&rect);
				ScreenToClient (&rect);
				rect.right = cx;
				rect.bottom = cy - 2;
/				view->BsdData->MoveWindow (&rect);

				view->SplitPane.SetLength (cx);
			}
			else
			{
				CRect rect;
				view->ChoiceBsds->GetWindowRect (&rect);
				ScreenToClient (&rect);
				rect.bottom = cy;
				view->ChoiceBsds->MoveWindow (&rect);

				view->BsdData->GetWindowRect (&rect);
				ScreenToClient (&rect);
				rect.bottom = cy;
				rect.right = cx - 2;
/				view->BsdData->MoveWindow (&rect);

				view->SplitPane.SetLength (cy);
			}
		}
	}
*/
}


// CMainFrame-Diagnose

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame-Meldungshandler



