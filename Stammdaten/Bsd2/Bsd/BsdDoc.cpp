// BsdDoc.cpp : Implementierung der Klasse CBsdDoc
//

#include "stdafx.h"
#include "Bsd.h"

#include "BsdDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CBsdDoc

IMPLEMENT_DYNCREATE(CBsdDoc, CDocument)

BEGIN_MESSAGE_MAP(CBsdDoc, CDocument)
END_MESSAGE_MAP()


// CBsdDoc-Erstellung/Zerst�rung

CBsdDoc::CBsdDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CBsdDoc::~CBsdDoc()
{
}

BOOL CBsdDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CBsdDoc-Serialisierung

void CBsdDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CBsdDoc-Diagnose

#ifdef _DEBUG
void CBsdDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CBsdDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CBsdDoc-Befehle
