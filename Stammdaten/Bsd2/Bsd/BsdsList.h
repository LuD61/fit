#ifndef _BSDS_LIST_DEF
#define _BSDS_LIST_DEF
#pragma once

class CBsdsList
{
public:
	short mdn;
    short fil;
	double a;
	CString a_bz1;
	CString a_bz2;
	CString chargennr;
	CString ident_nr;
	double  bsd_gew;
	CString bsd_red_grnd;
	CString bsd_update;
	CString pers;
	CString bsd_lgr_ort;

	CBsdsList(void);
	CBsdsList(short mdn,
		  short fil,
                  double a,
                  LPTSTR a_bz1,								                   
                  LPTSTR a_bz2,								                   
                  LPTSTR chargennr,								                   
                  LPTSTR ident_nr,								                   
                  double bsd_gew,
                  LPTSTR bsd_red_grnd,
                  LPTSTR bsd_update,
                  LPTSTR pers,
				  LPTSTR bsd_lgr_ort
);
	~CBsdsList(void);
};
#endif
