#ifndef _CHOICEWG_DEF
#define _CHOICEWG_DEF

#include "ChoiceX.h"
#include "WGList.h"
#include <vector>

class CChoiceWG : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;

    public :
	    std::vector<CWGList *> WGList;
	    std::vector<CWGList *> SelectList;
        CChoiceWG(CWnd* pParent = NULL);   // Standardkonstruktor
        ~CChoiceWG();
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchABz1 (CListCtrl *, LPTSTR);
        void SearchABz2 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CWGList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		void SaveSelection (CListCtrl *ListBox);
};
#endif
