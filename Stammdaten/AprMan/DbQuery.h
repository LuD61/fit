#pragma once

class CDbQuery
{
protected:
	CString Q;
	CString Query;
public:
	CDbQuery(void);
	~CDbQuery(void);
	CString& ForStringColumn (CString& Value, CString& ColumnName);
	CString& ForNumColumn (CString& Value, CString& ColumnName);
	CString& ForColumn (CString& Value, CString& ColumnName, BOOL Quote);
};
