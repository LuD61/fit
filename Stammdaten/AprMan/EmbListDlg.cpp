// EmbListDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "EmbListDlg.h"
#include "Token.h"
#include "StrFuncs.h"


// CEmbListDlg-Dialogfeld

IMPLEMENT_DYNAMIC(CEmbListDlg, CDialog)
CEmbListDlg::CEmbListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEmbListDlg::IDD, pParent)
{
	dcs_a_par = -1;
	waa_a_par = -1;
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
}

CEmbListDlg::~CEmbListDlg()
{
}

void CEmbListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EMB_TEXT, m_LEmbText);
	DDX_Control(pDX, IDC_EMB_LIST, m_EmbList);
}


BEGIN_MESSAGE_MAP(CEmbListDlg, CDialog)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_NOTIFY(TVN_SELCHANGED, IDC_EMB_LIST, OnTvnSelchangedEmbList)
END_MESSAGE_MAP()


// CEmbListDlg-Meldungshandler

BOOL CEmbListDlg::OnInitDialog()
{
	A_bas   = &Basis->A_bas;
	A_hndw   = &Basis->A_hndw;
	A_ean   = &Basis->A_ean;
	A_emb   = &Basis->A_emb;
	A_krz   = &Basis->A_krz;
	Sys_par = &Basis->Sys_par;

	m_EmbList.A_bas = A_bas;
	m_EmbList.A_ean = A_ean;
	m_EmbList.A_emb = A_emb;

	CDialog::OnInitDialog();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		LFont.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		LFont.CreatePointFont (95, _T("Dlg"));
	}
	m_EmbList.Init ();
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0, 0, 0);
    CtrlGrid.SetCellHeight (15);
//    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 0);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LEmbText = new CCtrlInfo (&m_LEmbText, 1, 1, 1, 1);
	CtrlGrid.Add (c_LEmbText);

	CCtrlInfo *c_EmbList = new CCtrlInfo (&m_EmbList, 0, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_EmbList);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_EmbList.SetFont (&LFont);

	CtrlGrid.Display ();

	return FALSE;
}

HBRUSH CEmbListDlg::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CEmbListDlg::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CEmbListDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			if (pMsg->wParam == VK_F5)
			{
				PageUpdate->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}

/*
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
*/
	}
	return FALSE;
}


void CEmbListDlg::SetBasis (CBasisdatenPage *Basis)
{
	this->Basis = Basis;
}

void CEmbListDlg::Update ()
{
	m_EmbList.Read ();
}

void CEmbListDlg::Read ()
{
	m_EmbList.Read ();
}

void CEmbListDlg::OnTvnSelchangedEmbList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;

	TVITEM Item = pNMTreeView->itemNew;

	memcpy (&A_emb->a_emb, &a_emb_null, sizeof (A_EMB));
	HTREEITEM ht = m_EmbList.GetSelectedItem ();
	CString Text = m_EmbList.GetItemText (ht);
	CToken t (Text, " ");
	int count = t.GetAnzToken ();
	if (count == 0) return;
	if (count == 1)
	{
		A_emb->a_emb.unt_emb = CStrFuncs::StrToDouble (Text);
	}
	else
	{
		A_emb->a_emb.unt_emb = CStrFuncs::StrToDouble (t.GetToken (1));
		if (A_emb->a_emb.unt_emb == 0.0)
		{
			A_emb->a_emb.unt_emb = CStrFuncs::StrToDouble (t.GetToken (0));
		}
	}
	A_emb->dbreadfirst_unt_emb ();
	for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
	{
		CUpdateEvent *event = *e;
		event->Update ();
	}
}

BOOL CEmbListDlg::HasFocus ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_EmbList) return FALSE;
	return TRUE;
}

void CEmbListDlg::Delete ()
{
	if (!HasFocus ()) return;
	DeleteRow ();
}

void CEmbListDlg::DeleteRow ()
{

	HTREEITEM ht = m_EmbList.GetSelectedItem ();
	if (ht == NULL) return;
	if (m_EmbList.GetParentItem (ht) == NULL) return;

	CString Text = m_EmbList.GetItemText (ht);
	A_emb->a_emb.emb = CStrFuncs::StrToDouble (Text);
	A_emb->dbreadfirst_emb ();
	A_emb->dbdelete ();
	A_krz->a_krz.a_herk = A_emb->a_emb.emb;

	if (A_bas->a_bas.a_typ == Basis->Hndw || A_bas->a_bas.a_typ == Basis->Eig)
	{
		SetSysPar ();
		A_krz->dbreada_herkfirst ();
		A_krz->dbdelete ();
		if (_tcscmp (A_hndw->a_hndw.verk_art, _T("K")) == 0 &&
			dcs_a_par == 1)
		{
			Basis->AutoNr.nveinid (0, 0, "a_krz", A_krz->a_krz.a_krz);
		}
	}

	m_EmbList.DeleteItem (ht);

/*
	for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
	{
		CUpdateEvent *event = *e;
		event->Update ();
	}
*/
}

void CEmbListDlg::SetSysPar()
{
	if (dcs_a_par == -1)
	{
		strcpy ((LPSTR) sys_par.sys_par_nam, "dcs_a_par");
		if (Sys_par->dbreadfirst () == 0)
		{
			LPSTR p = (LPSTR) sys_par.sys_par_wrt;
			CDbUniCode::DbToUniCode (sys_par.sys_par_wrt, p);
			dcs_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}

	if (waa_a_par == -1)
	{
		strcpy ((LPSTR) sys_par.sys_par_nam, "waa_a_par");
		if (Sys_par->dbreadfirst () == 0)
		{
			LPSTR p = (LPSTR) sys_par.sys_par_wrt;
			CDbUniCode::DbToUniCode (sys_par.sys_par_wrt, p);
			waa_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}
}
