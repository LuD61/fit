#include "stdafx.h"
#include "a_pr_list.h"

struct A_PR_LIST a_pr_list, a_pr_list_null;

void A_PR_LIST_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &a_pr_list.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_pr_list.a, SQLDOUBLE, 0);
            sqlin ((double *)  &a_pr_list.lad_akt_von, SQLDATE, 0);
    sqlout ((short *) &a_pr_list.hwg,SQLSHORT,0);
    sqlout ((short *) &a_pr_list.wg,SQLSHORT,0);
    sqlout ((long *) &a_pr_list.ag,SQLLONG,0);
    sqlout ((short *) &a_pr_list.teil_smt,SQLSHORT,0);
    sqlout ((short *) &a_pr_list.a_typ,SQLSHORT,0);
    sqlout ((short *) &a_pr_list.cfil,SQLSHORT,0);
    sqlout ((short *) &a_pr_list.cfil_gr,SQLSHORT,0);
    sqlout ((short *) &a_pr_list.cmdn,SQLSHORT,0);
    sqlout ((short *) &a_pr_list.cmdn_gr,SQLSHORT,0);
    sqlout ((double *) &a_pr_list.a,SQLDOUBLE,0);
    sqlout ((short *) &a_pr_list.fil,SQLSHORT,0);
    sqlout ((short *) &a_pr_list.fil_gr,SQLSHORT,0);
    sqlout ((short *) &a_pr_list.mdn,SQLSHORT,0);
    sqlout ((short *) &a_pr_list.mdn_gr,SQLSHORT,0);
    sqlout ((double *) &a_pr_list.pr_ek,SQLDOUBLE,0);
    sqlout ((double *) &a_pr_list.pr_vk,SQLDOUBLE,0);
    sqlout ((double *) &a_pr_list.pr_ek_sa,SQLDOUBLE,0);
    sqlout ((double *) &a_pr_list.pr_vk_sa,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_pr_list.lad_akv_sa,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &a_pr_list.lad_akt_von,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &a_pr_list.lad_akt_bis,SQLDATE,0);
            cursor = sqlcursor (_T("select a_pr_list.hwg,  ")
_T("a_pr_list.wg,  a_pr_list.ag,  a_pr_list.teil_smt,  a_pr_list.a_typ,  ")
_T("a_pr_list.cfil,  a_pr_list.cfil_gr,  a_pr_list.cmdn,  ")
_T("a_pr_list.cmdn_gr,  a_pr_list.a,  a_pr_list.fil,  a_pr_list.fil_gr,  ")
_T("a_pr_list.mdn,  a_pr_list.mdn_gr,  a_pr_list.pr_ek,  a_pr_list.pr_vk,  ")
_T("a_pr_list.pr_ek_sa,  a_pr_list.pr_vk_sa,  a_pr_list.lad_akv_sa,  ")
_T("a_pr_list.lad_akt_von,  a_pr_list.lad_akt_bis from a_pr_list ")

#line 17 "a_pr_list.rpp"
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ? ")
				  _T("and lad_akt_von = ?"));
    sqlin ((short *) &a_pr_list.hwg,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.wg,SQLSHORT,0);
    sqlin ((long *) &a_pr_list.ag,SQLLONG,0);
    sqlin ((short *) &a_pr_list.teil_smt,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.a_typ,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.cfil,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.cfil_gr,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.cmdn,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.cmdn_gr,SQLSHORT,0);
    sqlin ((double *) &a_pr_list.a,SQLDOUBLE,0);
    sqlin ((short *) &a_pr_list.fil,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.fil_gr,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.mdn,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.mdn_gr,SQLSHORT,0);
    sqlin ((double *) &a_pr_list.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &a_pr_list.pr_vk,SQLDOUBLE,0);
    sqlin ((double *) &a_pr_list.pr_ek_sa,SQLDOUBLE,0);
    sqlin ((double *) &a_pr_list.pr_vk_sa,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_pr_list.lad_akv_sa,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_pr_list.lad_akt_von,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_pr_list.lad_akt_bis,SQLDATE,0);
            sqltext = _T("update a_pr_list set ")
_T("a_pr_list.hwg = ?,  a_pr_list.wg = ?,  a_pr_list.ag = ?,  ")
_T("a_pr_list.teil_smt = ?,  a_pr_list.a_typ = ?,  a_pr_list.cfil = ?,  ")
_T("a_pr_list.cfil_gr = ?,  a_pr_list.cmdn = ?,  a_pr_list.cmdn_gr = ?,  ")
_T("a_pr_list.a = ?,  a_pr_list.fil = ?,  a_pr_list.fil_gr = ?,  ")
_T("a_pr_list.mdn = ?,  a_pr_list.mdn_gr = ?,  a_pr_list.pr_ek = ?,  ")
_T("a_pr_list.pr_vk = ?,  a_pr_list.pr_ek_sa = ?,  ")
_T("a_pr_list.pr_vk_sa = ?,  a_pr_list.lad_akv_sa = ?,  ")
_T("a_pr_list.lad_akt_von = ?,  a_pr_list.lad_akt_bis = ? ")

#line 24 "a_pr_list.rpp"
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ? ")
				  _T("and lad_akt_von = ?");
            sqlin ((short *)   &a_pr_list.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_pr_list.a, SQLDOUBLE, 0);
            sqlin ((double *)  &a_pr_list.lad_akt_von, SQLDATE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &a_pr_list.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_pr_list.a, SQLDOUBLE, 0);
            sqlin ((double *)  &a_pr_list.lad_akt_von, SQLDATE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_pr_list ")
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ? ")
				  _T("and lad_akt_von = ?"));
            sqlin ((short *)   &a_pr_list.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr_list.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_pr_list.a, SQLDOUBLE, 0);
            sqlin ((double *)  &a_pr_list.lad_akt_von, SQLDATE, 0);
            del_cursor = sqlcursor (_T("delete from a_pr_list ")
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ? ")
				  _T("and lad_akt_von = ?"));
    sqlin ((short *) &a_pr_list.hwg,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.wg,SQLSHORT,0);
    sqlin ((long *) &a_pr_list.ag,SQLLONG,0);
    sqlin ((short *) &a_pr_list.teil_smt,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.a_typ,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.cfil,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.cfil_gr,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.cmdn,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.cmdn_gr,SQLSHORT,0);
    sqlin ((double *) &a_pr_list.a,SQLDOUBLE,0);
    sqlin ((short *) &a_pr_list.fil,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.fil_gr,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.mdn,SQLSHORT,0);
    sqlin ((short *) &a_pr_list.mdn_gr,SQLSHORT,0);
    sqlin ((double *) &a_pr_list.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &a_pr_list.pr_vk,SQLDOUBLE,0);
    sqlin ((double *) &a_pr_list.pr_ek_sa,SQLDOUBLE,0);
    sqlin ((double *) &a_pr_list.pr_vk_sa,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_pr_list.lad_akv_sa,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_pr_list.lad_akt_von,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_pr_list.lad_akt_bis,SQLDATE,0);
            ins_cursor = sqlcursor (_T("insert into a_pr_list (")
_T("hwg,  wg,  ag,  teil_smt,  a_typ,  cfil,  cfil_gr,  cmdn,  cmdn_gr,  a,  fil,  fil_gr,  mdn,  ")
_T("mdn_gr,  pr_ek,  pr_vk,  pr_ek_sa,  pr_vk_sa,  lad_akv_sa,  lad_akt_von,  ")
_T("lad_akt_bis) ")

#line 65 "a_pr_list.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 67 "a_pr_list.rpp"

    	    sqlin ((short *) &a_pr_list.hwg,SQLSHORT,0);
	    sqlin ((short *) &a_pr_list.wg,SQLSHORT,0);
	    sqlin ((long *) &a_pr_list.ag,SQLLONG,0);
	    sqlin ((short *) &a_pr_list.teil_smt,SQLSHORT,0);
	    sqlin ((short *) &a_pr_list.a_typ,SQLSHORT,0);
	    sqlin ((short *) &a_pr_list.cfil,SQLSHORT,0);
	    sqlin ((short *) &a_pr_list.cfil_gr,SQLSHORT,0);
	    sqlin ((short *) &a_pr_list.cmdn,SQLSHORT,0);
	    sqlin ((short *) &a_pr_list.cmdn_gr,SQLSHORT,0);
	    sqlin ((double *) &a_pr_list.a,SQLDOUBLE,0);
	    sqlin ((short *) &a_pr_list.fil,SQLSHORT,0);
	    sqlin ((short *) &a_pr_list.fil_gr,SQLSHORT,0);
	    sqlin ((short *) &a_pr_list.mdn,SQLSHORT,0);
	    sqlin ((short *) &a_pr_list.mdn_gr,SQLSHORT,0);
	    sqlin ((double *) &a_pr_list.pr_ek,SQLDOUBLE,0);
	    sqlin ((double *) &a_pr_list.pr_vk,SQLDOUBLE,0);
	    sqlin ((double *) &a_pr_list.pr_ek_sa,SQLDOUBLE,0);
	    sqlin ((double *) &a_pr_list.pr_vk_sa,SQLDOUBLE,0);
	    sqlin ((TCHAR *) a_pr_list.lad_akv_sa,SQLCHAR,2);
            ins_null_cursor = sqlcursor (_T("insert into a_pr_list (")
			_T("hwg,  wg,  ag,  teil_smt,  a_typ,  cfil,  cfil_gr,  cmdn,  cmdn_gr,  "
			   "a,  fil,  fil_gr,  mdn,  mdn_gr,  pr_ek,  pr_vk,  pr_ek_sa,  pr_vk_sa,  lad_akv_sa) ")
                        _T("values ")
                        _T("(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 
}


int A_PR_LIST_CLASS::dbinsert_nulls ()
{
	if (ins_null_cursor == -1)
	{
		prepare ();
        }
        return sqlexecute (ins_null_cursor);
}
