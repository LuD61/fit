// DialogTestDlg.h : Headerdatei
//

#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "CtrlGrid.h"
#include "ArtAktListCtrl.h"
#include "FillList.h"
#include "Sys_par.h"
#include "a_bas.h"
#include "gr_zuord.h"
#include "mdn.h"
#include "fil.h"
#include "adr.h"
#include "A_pr.h"
#include "Akt_krza.h"
#include "Sys_ben.h"
#include "ChoiceA.h"
#include "ChoiceMdn.h"
#include "FormTab.h"
#include "mo_progcfg.h"
#include "Controls.h"
#include "APreise.h"
#include "AktKrzPreise.h"
#include "PrProt.h"
#include "StaticButton.h"
#include "Ptabn.h"

#define IDC_ACHOICE 3000
#define IDC_MDNCHOICE 3001

// CArtAktPage Dialogfeld
class CArtAktPage : public CDbPropertyPage
{
// Konstruktion
protected:
	DECLARE_DYNAMIC(CArtAktPage)
public:
	CArtAktPage(CWnd* pParent = NULL);	// Standardkonstruktor
	CArtAktPage(UINT nIDTemplate);	// Standardkonstruktor

	~CArtAktPage ();

// Dialogfelddaten
//	enum { IDD = IDD_DIALOGTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public :
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	HBRUSH hBrush;
	HBRUSH staticBrush;
	BOOL ShowAbverk;
	BOOL OldMode;
	PROG_CFG Cfg;
	BOOL RemoveKun;
	int StartSize;

	BOOL Write160;
	static HANDLE Write160Lib;
    int (*dbild160)(LPSTR); 

    BOOL (*dOpenDbase)(LPSTR); 
	CControls HeadControls;
	CControls PosControls;
	CControls ButtonControls;
	BOOL HideButtons;
	int RightListSpace;
	CVector DbRows;
	CVector ListRows;
	int AktKrzCursor;
	int AprCursor;
	int NullCursor;
	int MdnGrCursor;
	int MdnCursor;
	int FilCursor;
	CWnd *Frame;
	CCtrlGrid CtrlGrid;
	CCtrlGrid AGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid EinhGrid;
	CButton m_MdnChoice;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CEdit m_MdnName;
	CButton m_AChoice;
	CStatic m_LA;
	CNumEdit m_A;
	CEdit m_LA_bz1;
	CEdit m_A_bz1;
	CEdit m_A_bz2;
	CStatic m_LMe_einh;
	CStatic m_LMe_einh_abverk;
	CStatic m_LInh;
	CStatic m_Me_einh;
//	CEdit m_Me_einh;
	CStatic m_Me_einh_abverk;
	CStatic m_Inh;
	CStaticButton m_Gueltig;
	CStaticButton m_BasisPr;
	CFillList FillList;
	CArtAktListCtrl m_List;
	CButton m_Cancel;
	CButton m_Save;
	CButton m_Delete;
	CButton m_Insert;
	CString m_MeEinh;
	CString m_MeEinhAbverk;
	CString m_Inhalt;
	int RighListSpace;
	CCtrlGrid ButtonGrid;
	GR_ZUORD_CLASS Gr_zuord;
	SYS_PAR_CLASS Sys_par;
	MDN_CLASS Mdn;
	FIL_CLASS Fil;
	ADR_CLASS MdnAdr;
	ADR_CLASS FilAdr;
	A_BAS_CLASS A_bas;
	APR_CLASS A_pr;
	AKT_KRZA_CLASS Akt_krz;
	SYS_BEN_CLASS Sys_ben;
	PTABN_CLASS Ptab;
	CPrProt PrProt;
	CFormTab Form;
	CChoiceA *Choice;
	BOOL ModalChoice;
	BOOL CloseChoice;
	BOOL AChoiceStat;
	CString SearchA;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CFont Font;
	CFont lFont;
	CString PersName;
	CString Separator;
    CImageList image; 
	int CellHeight;
	int scale;

	virtual void OnSize (UINT, int, int);
    void OnMdnchoice(); 
	void OnAchoice ();
    void OnASelected ();
    void OnACanceled ();
	virtual BOOL Read ();
	virtual BOOL read ();
	virtual BOOL Write ();
	virtual BOOL Print ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	virtual BOOL StepBack ();
	virtual void OnCancel ();
	virtual void OnSave ();
	BOOL ReadMdn ();
	BOOL ReadList ();
	BOOL ReadListOld ();
    void FillMdnGrCombo ();
    void FillMdnCombo ();
    void FillFilCombo ();
	virtual BOOL DeleteAll ();
	virtual void OnDelete ();
	virtual void OnInsert ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult);
    void EnableHeadControls (BOOL enable);
    void WriteRow (int row);
    void DestroyRows(CVector &Rows);
    void DestroyRowsEx(CVector &Rows);
    void DeleteDbRows ();
    BOOL IsChanged (CAktKrzPreise *pApr, CAktKrzPreise *old_a_pr);
    BOOL InList (AKT_KRZA_CLASS& Akt_krz);
	void ReadCfg ();
    void ListCopy ();
	BOOL TestDecValues (AKT_KRZ *akt_krz);
	virtual void Save ();
	virtual void Load ();
	void SetListMode ();
	BOOL IsCompactMode ();
	BOOL EntryExist (CString& MdnGr, CString& Mdn, CString& FilGr, CString& Fil, 
					 CString& AktVon, CString& AktBis, int startpos);
	afx_msg void OnGueltig();
	void ShowMeEinhAbverk (BOOL show); 
	void SetMeEinhAbverk (); 
	afx_msg void OnBasisPr();
};
