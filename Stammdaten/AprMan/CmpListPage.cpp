// CmpListPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "AprMan.h"
#include "CmpListPage.h"
#include "Process.h"


// CCmpListPage-Dialogfeld

IMPLEMENT_DYNAMIC(CCmpListPage, CDbPropertyPage)
CCmpListPage::CCmpListPage()
	: CDbPropertyPage(CCmpListPage::IDD)
{
}

CCmpListPage::~CCmpListPage()
{
}

void CCmpListPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCmpListPage, CPropertyPage)
	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CCmpListPage-Meldungshandler

BOOL CCmpListPage::OnInitDialog ()
{
	CPropertyPage::OnInitDialog();

	Page1.Create (IDD_COMPANY_PR_PAGE, this);
	Page1.ShowWindow (SW_SHOWNORMAL);
	Page1.UpdateWindow ();

	Page2.Create (IDD_LIST_PAGE, this);
	Page2.ShowWindow (SW_SHOWNORMAL);
	Page2.UpdateWindow ();

	Page1.AddUpdateEvent (&Page2);

	SplitPane.Create (this, 0, 0, &Page1, &Page2, 30, CSplitPane::Vertical);

	Page2.m_List.EnableWindow (FALSE);
	return FALSE;
}

void CCmpListPage::OnSize (UINT nType, int cx, int cy)
{
	if (IsWindow (SplitPane.m_hWnd))
	{
		CRect rect;
		Page1.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
        Page1.MoveWindow (&rect);

		Page2.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
		rect.bottom = cy - 2;
        Page2.MoveWindow (&rect);

		SplitPane.SetLength (cx);
	}
}

BOOL CCmpListPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CCmpListPage::Write ()
{
	if (!Page2.m_List.IsWindowEnabled ())
	{
		return FALSE;
	}

	return Page2.Write ();
}

BOOL CCmpListPage::StepBack ()
{
	if (!Page2.m_List.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}

	else
	{
		if (Page2.TestWrite ())
		{
			INT_PTR ret = MessageBox (_T("�nderungen speichern ?"), _T(""), MB_YESNO | MB_ICONQUESTION);
			if (ret == IDYES)
			{
				Page2.Write ();
				return TRUE;
			}
		}
		Page2.StopEnter ();
	}
	return TRUE;
}


void CCmpListPage::OnDelete ()
{
	Page2.m_List.DeleteRow ();
}

void CCmpListPage::OnInsert ()
{
	Page2.m_List.InsertRow ();
}

BOOL CCmpListPage::Print ()
{

// Achtung !! Hier mu� noch der richtige Ablauf fp�r den Druck eingesetzt werden.

	return Page1.Print ();

/*
	CProcess print;
    LPTSTR tmp = getenv ("TMPPATH");
	CString Command;
	Command = "dr70001 -name 11200";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
*/
}

