// PrNewPreise.cpp : Implementierungsdatei
//

#include "stdafx.h"
#ifdef ARTIKEL
#include "Artikel.h"
#else
#include "AprMan.h"
#endif
#include "NewPrPageDat.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Process.h"
#include "DbUniCode.h"
#include <vector>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CNewPrPageDat Dialogfeld

IMPLEMENT_DYNAMIC(CNewPrPageDat, CDbPropertyPage)
HANDLE CNewPrPageDat::Write160Lib = NULL;

class AprListAEntry
{
private:
	short mdn_gr;
	short mdn;
	short fil_gr;
	short fil;
	double Artikel;

public:
	AprListAEntry (double a,short mdn_gr, short mdn, short fil_gr, short fil)
	{
		this->mdn_gr = mdn_gr;
		this->mdn = mdn;
		this->fil_gr = fil_gr;
		this->fil = fil;
		this->Artikel = a;
	}

	int Compare (double a, short mdn_gr, short mdn, short fil_gr, short fil)
	{
		if (this->Artikel != a)
		{
			return (int) (a - this->Artikel);
		}
		if (this->mdn_gr != mdn_gr)
		{
			return mdn_gr - this->mdn_gr;
		}
		if (this->mdn != mdn)
		{
			return mdn - this->mdn;
		}
		if (this->fil_gr != fil_gr)
		{
			return fil_gr - this->fil_gr;
		}
		if (this->fil != fil)
		{
			return fil - this->fil;
		}
		return 0;
	}

	BOOL Exist (double a, short mdn_gr, short mdn, short fil_gr, short fil)
	{
		if ((this->Artikel == a) &&
		    (this->mdn_gr == mdn_gr) &&
		    (this->mdn == mdn) &&
		    (this->fil_gr == fil_gr) &&
			(this->fil == fil))
		{
			return TRUE;
		}
		return FALSE;
	}
};

static std::vector <AprListAEntry *> AprList;

CNewPrPageDat::CNewPrPageDat(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage()
{
	Choice = NULL;
	ModalChoice = FALSE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	AktKrzCursor = -1;
	AktKrzCursor2 = -1;
	AprCursor = -1;
	NullCursor = -1;
	MdnGrCursor = -1;
	MdnCursor = -1;
	FilCursor = -1;
	HeadControls.Add (&m_MdnGr);
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_FilGr);
	HeadControls.Add (&m_Filiale);
	HeadControls.Add (&m_GueAb);
	PosControls.Add (&m_List);
	PosControls.Add (&m_GueAb2);

    HideButtons = FALSE;

	Frame = NULL;
	DbRows.Init ();
	ListRows.Init ();
	SearchA = _T("");
	Separator = _T(";");
	CellHeight = 0;
    dbild160 = NULL;
	dOpenDbase = NULL;
	Write160 = TRUE;
	Cfg.SetProgName( _T("AprMan"));
	ArchiveName = _T("AprMan.prp");
	OldMode = FALSE;
	m_List.OldMode = OldMode;
	Load ();
}

CNewPrPageDat::CNewPrPageDat(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	Frame = NULL;
	AktKrzCursor = -1;
	AktKrzCursor2 = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
	NullCursor = -1;
	MdnGrCursor = -1;
	MdnCursor = -1;
	FilCursor = -1;
	ArchiveName = _T("AprMan.prp");
	m_List.OldMode = OldMode;
	Load ();
}

CNewPrPageDat::~CNewPrPageDat()
{
	Save ();
	Font.DeleteObject ();
	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	A_pr.dbclose ();
	MdnAdr.dbclose ();
	if (AktKrzCursor != -1)
	{
		A_bas.sqlclose (AktKrzCursor);
	}
	if (AktKrzCursor2 != -1)
	{
		A_bas.sqlclose (AktKrzCursor2);
	}
	if (AprCursor != -1)
	{
		A_bas.sqlclose (AprCursor);
	}
	if (NullCursor != -1)
	{
		A_bas.sqlclose (NullCursor);
	}
	if (MdnGrCursor != -1)
	{
		A_bas.sqlclose (MdnGrCursor);
	}
	if (MdnCursor != -1)
	{
		A_bas.sqlclose (MdnCursor);
	}
	if (FilCursor != -1)
	{
		A_bas.sqlclose (FilCursor);
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);
}

void CNewPrPageDat::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LMDNGR, m_LMdnGr);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_LFILGR, m_LFilGr);
	DDX_Control(pDX, IDC_LFILIALE, m_LFiliale);
	DDX_Control(pDX, IDC_MDNGR, m_MdnGr);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_FILGR, m_FilGr);
	DDX_Control(pDX, IDC_FILIALE, m_Filiale);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_LGUE_AB2, m_LGueAb2);
//	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_GUE_AB, m_GueAb);
	DDX_Control(pDX, IDC_GUE_AB2, m_GueAb2);
	DDX_Control(pDX, IDC_LIST, m_List);
//	DDX_Control(pDX, IDC_BASISPR, m_BasisPr);
}

BEGIN_MESSAGE_MAP(CNewPrPageDat, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_ACHOICE ,  OnAktKrzChoice)
	ON_BN_CLICKED(IDC_CANCEL ,   OnCancel)
	ON_BN_CLICKED(IDC_SAVE ,     OnSave)
	ON_BN_CLICKED(IDC_DELETE ,   OnDelete)
	ON_BN_CLICKED(IDC_INSERT ,   OnInsert)
	ON_COMMAND (SELECTED, OnAktKrzSelected)
	ON_COMMAND (CANCELED, OnAktKrzCanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_COMMAND(IDC_BASISPR, OnBasisPr)
	ON_NOTIFY(NM_KILLFOCUS, IDC_GUE_AB2, &CNewPrPageDat::OnNMKillfocusGueAb2)
END_MESSAGE_MAP()


// CNewPrPageDat Meldungshandler

BOOL CNewPrPageDat::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	A_bas.opendbase (_T("bws"));
	GueAb.day = 01;
	GueAb.month = 01;
	GueAb.year = 2014;
	dMdnGr = 0;
	dFilGr = 0;
	dFil = 0;

	strcpy (sys_par.sys_par_nam, "nachkpreis");
	if (Sys_par.dbreadfirst () == 0)
	{
		m_List.scale = _tstoi (sys_par.sys_par_wrt);
	}


	CUtil::GetPersName (PersName);
	PrProt.Construct (PersName, CString ("11200"), &A_pr, &Akt_krz, &Akt_krza);
     
	ReadCfg ();

// 160-iger schreiben

	if (Write160 && Write160Lib == NULL)
	{
		CString bws;
		BOOL ret = bws.GetEnvironmentVariable (_T("bws"));
		CString W160Dll;
		if (ret)
		{
			W160Dll.Format (_T("%s\\bin\\bild160dll.dll"), bws.GetBuffer ());
		}
		else
		{
			W160Dll = _T("bild160dll.dll");
		}
		Write160Lib = LoadLibrary (W160Dll.GetBuffer ());
		if (Write160Lib != NULL && dbild160 == NULL)
		{
			dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
			dOpenDbase = (BOOL (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "OpenDbase");
			if (dOpenDbase != NULL)
			{
				(*dOpenDbase) ("bws");
			}
		}
	}
	else if (dbild160 == NULL)
	{
		dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
	}

	if (HideButtons)
	{
		ButtonControls.SetVisible (FALSE);
		RightListSpace = 15;
	}
	else
	{
		ButtonControls.SetVisible (TRUE);
		RightListSpace = 125;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

/**
	m_BasisPr.nID = IDC_BASISPR;
	m_BasisPr.SetWindowText (_T("Basispreise"));
	m_BasisPr.SetBkColor (GetSysColor (COLOR_3DFACE));
	m_BasisPr.Orientation = m_BasisPr.Left;
	*/

    memcpy (&Akt_krz.akt_krz, &akt_krz_null, sizeof (AKT_KRZ));

    Form.Add (new CFormField (&m_MdnGr,EDIT,        (short *) &dMdnGr, VSHORT));
    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CFormField (&m_FilGr,EDIT,        (short *) &dFilGr, VSHORT));
    Form.Add (new CFormField (&m_Filiale,EDIT,        (short *) &dFil, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
//    Form.Add (new CFormField (&m_A,EDIT,       (double *) &A_bas.a_bas.a, VDOUBLE, 13, 0));
    Form.Add (new CFormField (&m_GueAb,DATETIMEPICKER,  (DATE_STRUCT *) &GueAb, VDATE));
    Form.Add (new CFormField (&m_GueAb2,DATETIMEPICKER,  (DATE_STRUCT *) &GueAb, VDATE));



	if (!OldMode)
	{
		A_pr.sqlin ((double *)  &A_pr.a_pr.a,  SQLDOUBLE, 0);
		A_pr.sqlout ((short *)  &A_pr.a_pr.mdn_gr, SQLSHORT, 0);
		A_pr.sqlout ((short *)  &A_pr.a_pr.mdn, SQLSHORT, 0);
		A_pr.sqlout ((short *)  &A_pr.a_pr.fil_gr, SQLSHORT, 0);
		A_pr.sqlout ((short *)  &A_pr.a_pr.fil, SQLSHORT, 0);
		AprCursor = A_pr.sqlcursor (_T("select mdn_gr, mdn, fil_gr, fil ") 
										  _T("from a_pr ")
										  _T("where a = ? ")
										  _T("order by mdn_gr, mdn, fil_gr, fil"));

		Akt_krz.sqlin ((DATE_STRUCT *)  &Akt_krz.akt_krz.lad_akt_von, SQLDATE, 0);
		Akt_krz.sqlin ((short *)  &Akt_krz.akt_krz.mdn_gr, SQLSHORT, 0);
		Akt_krz.sqlin ((short *)  &Akt_krz.akt_krz.mdn, SQLSHORT, 0);
		Akt_krz.sqlin ((short *)  &Akt_krz.akt_krz.fil_gr, SQLSHORT, 0);
		Akt_krz.sqlin ((short *)  &Akt_krz.akt_krz.fil, SQLSHORT, 0);
		Akt_krz.sqlout ((double *)  &Akt_krz.akt_krz.a,  SQLDOUBLE, 0);
		Akt_krz.sqlout ((short *)  &Akt_krz.akt_krz.mdn_gr, SQLSHORT, 0);
		Akt_krz.sqlout ((short *)  &Akt_krz.akt_krz.mdn, SQLSHORT, 0);
		Akt_krz.sqlout ((short *)  &Akt_krz.akt_krz.fil_gr, SQLSHORT, 0);
		Akt_krz.sqlout ((short *)  &Akt_krz.akt_krz.fil, SQLSHORT, 0);
		Akt_krz.sqlout ((DATE_STRUCT *)  &Akt_krz.akt_krz.lad_akt_von, SQLDATE, 0);
		Akt_krz.sqlout ((char *)   &Akt_krz.akt_krz.lad_akv_sa, SQLCHAR, sizeof (Akt_krz.akt_krz.lad_akv_sa));
	 	AktKrzCursor = Akt_krz.sqlcursor (_T("select a, mdn_gr, mdn, fil_gr, fil, lad_akt_von, lad_akv_sa from akt_krz ")
										  _T("where lad_akt_von = ? and mdn_gr = ? and mdn = ? ")
										  _T("and fil_gr = ? and fil = ? and lad_akv_sa = \"9\" ")
										  _T("order by mdn_gr, mdn, fil_gr, fil,lad_akt_von,a desc"));

		Akt_krz.sqlin ((double *)  &Akt_krz.akt_krz.a,  SQLDOUBLE, 0);
		Akt_krz.sqlout ((short *)  &Akt_krz.akt_krz.mdn_gr, SQLSHORT, 0);
		Akt_krz.sqlout ((short *)  &Akt_krz.akt_krz.mdn, SQLSHORT, 0);
		Akt_krz.sqlout ((short *)  &Akt_krz.akt_krz.fil_gr, SQLSHORT, 0);
		Akt_krz.sqlout ((short *)  &Akt_krz.akt_krz.fil, SQLSHORT, 0);
		Akt_krz.sqlout ((DATE_STRUCT *)  &Akt_krz.akt_krz.lad_akt_von, SQLDATE, 0);
		Akt_krz.sqlout ((char *)   &Akt_krz.akt_krz.lad_akv_sa, SQLCHAR, sizeof (Akt_krz.akt_krz.lad_akv_sa));
		AktKrzCursor2 = Akt_krz.sqlcursor (_T("select mdn_gr, mdn, fil_gr, fil, lad_akt_von, lad_akv_sa from akt_krz ")
										  _T("where a = ? and lad_akv_sa = \"9\" ")
										  _T("order by mdn_gr, mdn, fil_gr, fil, fil,lad_akt_von,a desc"));
	}

	Akt_krz.sqlin ((double *)  &A_pr.a_pr.a,  SQLDOUBLE, 0);
	Akt_krz.sqlin ((short *)  &A_pr.a_pr.mdn_gr, SQLSHORT, 0);
	Akt_krz.sqlin ((short *)  &A_pr.a_pr.mdn, SQLSHORT, 0);
	Akt_krz.sqlin ((short *)  &A_pr.a_pr.fil_gr, SQLSHORT, 0);
	Akt_krz.sqlin ((short *)  &A_pr.a_pr.fil, SQLSHORT, 0);
	NullCursor = Akt_krz.sqlcursor (_T("update akt_krz set lad_akt_bis = null, bel_akt_bis = null ")
								      _T("where a = ? ")
									  _T("and mdn_gr = ? ")
									  _T("and mdn = ? ")
									  _T("and fil_gr = ? ")
									  _T("and fil = ? ")
									  _T("and lad_akv_sa = \"9\" ")
									  _T("and lad_akt_bis = \"01.01.1900\" "));

	Gr_zuord.sqlout ((short *) &Gr_zuord.gr_zuord.gr, SQLSHORT, 0);
	Gr_zuord.sqlout ((char *) Gr_zuord.gr_zuord.gr_bz1, SQLCHAR, sizeof (Gr_zuord.gr_zuord.gr_bz1));
    MdnGrCursor = Gr_zuord.sqlcursor (_T("select gr, gr_bz1 from gr_zuord ")
 									  _T("where mdn = 0 ")
									  _T("and gr > 0 ")
								      _T("order by gr"));

	Mdn.sqlout ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);
	MdnAdr.sqlout ((char *) MdnAdr.adr.adr_krz, SQLCHAR, sizeof (MdnAdr.adr.adr_krz));
    MdnCursor = Mdn.sqlcursor (_T("select mdn.mdn, adr.adr_krz from mdn, adr ")
		                       _T("where mdn.mdn > 0 ")
							   _T("and adr.adr = mdn.adr ")
							   _T("order by mdn.mdn"));
/*
	Fil.sqlin ((short *) &Fil.fil.mdn, SQLSHORT, 0);
	Fil.sqlout ((short *) &Fil.fil.fil, SQLSHORT, 0);
	FilAdr.sqlout ((char *) &FilAdr.adr.adr_krz, SQLCHAR, sizeof (FilAdr.adr.adr_krz));
    FilCursor = Fil.sqlcursor (_T("select fil.fil, adr.adr_krz from fil, adr ")
		                       _T("where fil.mdn > ? ")
							   _T("and fil.fil > 0 ")
							   _T("and adr.adr = fil.adr ")
							   _T("order by fil.fil"));
*/

	m_List.Prepare ();

	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_List.SetImageList (&image, LVSIL_SMALL);   
	}

	/***
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	tab->ShowWindow (SW_HIDE);
	***/

	FillList = m_List;
	FillList.SetStyle (LVS_REPORT);
	if (m_List.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	m_List.SetListMode (m_List.ListMode);
	FillList.SetCol (_T(""), 0, 0);
//	FillList.SetCol (_T("MdnGr"), 1, 0, LVCFMT_LEFT);
//	FillList.SetCol (_T("Mandant"), 2, 0, LVCFMT_LEFT);
//	FillList.SetCol (_T("FilGr"), 3, 0, LVCFMT_LEFT);
//	FillList.SetCol (_T("Filiale"), 4, 0, LVCFMT_LEFT);
	FillList.SetCol (_T("Artikel"), 1,400, LVCFMT_LEFT);
	FillList.SetCol (_T("Datum"), 2, 100, LVCFMT_LEFT);
	FillList.SetCol (_T("EK"), 3, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("VK"), 4, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("EK Neu"), 5, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("VK Neu"), 6, 100, LVCFMT_RIGHT);
//	FillList.SetCol (_T("EK Basis"), 7, 0, LVCFMT_RIGHT);
//	FillList.SetCol (_T("VK Basis"), 8, 0, LVCFMT_RIGHT);

	m_List.AddDisplayOnly (m_List.PosPrEkAb);
	m_List.AddDisplayOnly (m_List.PosPrVkAb);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	AGrid.Create (this, 1, 2);
    AGrid.SetBorder (0, 0);
    AGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_A = new CCtrlInfo (&m_GueAb, 0, 0, 1, 1);
	AGrid.Add (c_A);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);


	CCtrlInfo *c_LMdnGr     = new CCtrlInfo (&m_LMdnGr, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdnGr);
	CCtrlInfo *c_MdnGr   = new CCtrlInfo (&m_MdnGr, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGr);

	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 1, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LFilGr     = new CCtrlInfo (&m_LFilGr, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LFilGr);
	CCtrlInfo *c_FilGr   = new CCtrlInfo (&m_FilGr, 1, 2, 1, 1); 
	CtrlGrid.Add (c_FilGr);

	CCtrlInfo *c_LFiliale     = new CCtrlInfo (&m_LFiliale, 0, 3, 1, 1); 
	CtrlGrid.Add (c_LFiliale);
	CCtrlInfo *c_Filiale   = new CCtrlInfo (&m_Filiale, 1, 3, 1, 1); 
	CtrlGrid.Add (c_Filiale);

	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 0, 4, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 1, 4, 1, 1); 
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_List  = new CCtrlInfo (&m_List, 0, 6, DOCKRIGHT, DOCKBOTTOM); 
//	c_List->rightspace = 125;

	CCtrlInfo *c_LGueAb2 = new CCtrlInfo(&m_LGueAb2, 4, 3, 1, 1);
	CtrlGrid.Add(c_LGueAb2);
	CCtrlInfo *c_GueAb2 = new CCtrlInfo(&m_GueAb2, 5, 3, 1, 1);
	CtrlGrid.Add(c_GueAb2);


	c_List->rightspace = RightListSpace;
	CtrlGrid.Add (c_List);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	FillMdnGrCombo ();
	FillMdnCombo ();

	CtrlGrid.Display ();
	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	if (PersName.Trim () != "")
	{
		_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
		if (Sys_ben.dbreadfirstpers_nam () == 0)
		{
			if (Sys_ben.sys_ben.mdn != 0)
			{
				Mdn.mdn.mdn = Sys_ben.sys_ben.mdn;
				m_Mdn.SetReadOnly ();
				m_Mdn.ModifyStyle (WS_TABSTOP, 0);
			}
		}
	}
	Form.Show ();
	ReadMdn ();
	EnableHeadControls (TRUE);
	m_Mdn.SetFocus ();
	return FALSE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

HBRUSH CNewPrPageDat::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	if (hBrush == NULL)
	{
		hBrush = CreateSolidBrush (Color);
		staticBrush = CreateSolidBrush (Color);
	}

	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
 		    return staticBrush;
	}
	return CPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CNewPrPageDat::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CNewPrPageDat::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CNewPrPageDat::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CNewPrPageDat::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CNewPrPageDat::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_List.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_List &&
					GetFocus ()->GetParent () != &m_List )
				{

					break;
			    }
				m_List.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnAktKrzChoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_GueAb)
				{
					OnAktKrzChoice ();
					return TRUE;
				}
				m_List.OnKey9 ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CNewPrPageDat::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_GueAb)
	{
		if (GueAb.day == 1 && GueAb.month == 1 && GueAb.year == 2014)
		{
			OnAktKrzChoice ();
			m_GueAb.SetFocus ();
			return FALSE;
		}

		if (!Read ())
		{
			m_GueAb.SetFocus ();
			return FALSE;
		}
	}

	if (Control != &m_List.ListEdit &&
		Control != &m_List.ListComboBox &&
		Control != &m_List.SearchListCtrl.Edit &&
		Control != &m_List.ListDate)
{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CNewPrPageDat::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_List &&
		Control->GetParent ()!= &m_List )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_List.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}


BOOL CNewPrPageDat::ReadMdn ()
{
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Mdn.mdn.mdn > 0) m_MdnGr.EnableWindow (FALSE);
	if (Mdn.mdn.mdn == 0) m_MdnGr.EnableWindow (TRUE);
	if (Mdn.mdn.mdn == 0)
	{
		strcpy (MdnAdr.adr.adr_krz, "Zentrale");
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return TRUE;
	}
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}


BOOL CNewPrPageDat::read ()
{
	if (m_GueAb.IsWindowEnabled ())
	{
		return FALSE;
	}
	return Read ();

}

BOOL CNewPrPageDat::Read ()
{
	if (ModalChoice)
	{
		CString cA;
		m_GueAb.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnAktKrzChoice ();
			SearchA = "";
			if (!AChoiceStat)
			{
				m_GueAb.SetFocus ();
				return FALSE;
			}
		}
	}

	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	Form.Get ();


	memcpy (&Akt_krz.akt_krz, &akt_krz_null, sizeof (AKT_KRZ));
	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	Akt_krz.akt_krz.mdn_gr = dMdnGr;
	Akt_krz.akt_krz.mdn = Mdn.mdn.mdn;
	Akt_krz.akt_krz.fil_gr = dFilGr;
	Akt_krz.akt_krz.fil = dFil;
	Akt_krz.akt_krz.lad_akt_von = GueAb;


	A_pr.sqlopen (AktKrzCursor);
	if (A_pr.sqlfetch (AktKrzCursor) == 0)
	{
	    EnableHeadControls (FALSE);
		ReadList ();
		Form.Show ();
		m_GueAb.SetFocus ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Keine Artikel zu diesem Datum gefunden"));
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);

		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		Form.Show ();
		m_GueAb.SetFocus ();
		return FALSE;
	}
	return FALSE;
}

void CNewPrPageDat::DestroyAprList ()
{
	for (std::vector<AprListAEntry *>::iterator pabl = AprList.begin (); pabl != AprList.end (); ++pabl)
	{
		AprListAEntry *entry = *pabl;
		delete entry;
	}
	AprList.clear ();
}

void CNewPrPageDat::InsertAktKrzEntry ()
{
	int i = 0;
	for (std::vector<AprListAEntry *>::iterator pabl = AprList.begin (); pabl != AprList.end (); ++pabl)
	{
		AprListAEntry *entry = *pabl;
		if (entry->Exist (Akt_krz.akt_krz.a,
		                  Akt_krz.akt_krz.mdn_gr,
			              Akt_krz.akt_krz.mdn, 
			              Akt_krz.akt_krz.fil_gr, 
			              Akt_krz.akt_krz.fil))
		{
			return;
		}

		if (entry->Compare (Akt_krz.akt_krz.a,
		                    Akt_krz.akt_krz.mdn_gr,
			                Akt_krz.akt_krz.mdn, 
			                Akt_krz.akt_krz.fil_gr, 
			                Akt_krz.akt_krz.fil) < 0)
		{
			break;

		}
		i ++;
	}
	FillList.InsertItem (i, 0);
	CString cA;
	cA.Format (_T("%.0lf"), Akt_krz.akt_krz.a);
	FillList.SetItemText (cA.GetBuffer (), i, m_List.PosA);


	CString cMdn;
	if (Akt_krz.akt_krz.mdn == 0)
	{
		if (m_List.ListMode == CNewPrListACtrl::Large)
		{
			cMdn.Format (_T("0  Zentrale"));
		}
		else
		{
			cMdn.Format (_T("0"));
		}
	}
	else
	{
		if (m_List.ListMode == CNewPrListACtrl::Large)
		{
			cMdn.Format (_T("%hd  %s"), Akt_krz.akt_krz.mdn,
						MdnAdr.adr.adr_krz);
		}
		else
		{
			cMdn.Format (_T("%hd"), Akt_krz.akt_krz.mdn);
		}
	}


	CString cFil;
	if (Akt_krz.akt_krz.fil == 0)
	{
		if (m_List.ListMode == CNewPrListACtrl::Large)
		{
			cFil.Format (_T("0  Mandant"));
		}
		else
		{
			cFil.Format (_T("0"));
		}
	}
	else
	{
		if (m_List.ListMode == CNewPrListACtrl::Large)
		{
			cFil.Format (_T("%hd  %s"), Akt_krz.akt_krz.fil,
						FilAdr.adr.adr_krz);
		}
		else
		{
			cFil.Format (_T("%hd"), Akt_krz.akt_krz.fil);
		}
	}

	CString cLadAktVon = _T("");
	Akt_krz.FromDbDate (cLadAktVon, &Akt_krz.akt_krz.lad_akt_von);
	FillList.SetItemText (cLadAktVon.GetBuffer (), i, m_List.PosDate);

	CString PrEk;
	m_List.DoubleToString (0.0, PrEk, 2);
	FillList.SetItemText (PrEk.GetBuffer (), i, m_List.PosPrEk);

	CString PrVk;
	m_List.DoubleToString (0.0, PrVk, 2);
	FillList.SetItemText (PrVk.GetBuffer (), i, m_List.PosPrVk);

	CString PrEkN;
	m_List.DoubleToString (Akt_krz.akt_krz.pr_ek_sa_euro, PrEkN, 2);
	FillList.SetItemText (PrEkN.GetBuffer (), i, m_List.PosPrEkN);

	CString PrVkN;
	m_List.DoubleToString (Akt_krz.akt_krz.pr_vk_sa_euro, PrVkN, 2);
	FillList.SetItemText (PrVkN.GetBuffer (), i, m_List.PosPrVkN);

	CAktKrzPreise *akt_krz = new CAktKrzPreise (PrEk, PrVk, Akt_krz.akt_krz);
	DbRows.Add (akt_krz);
}


BOOL CNewPrPageDat::ReadList ()
{
	int dsqlstatus;

	m_List.StopEnter ();
	m_List.DeleteAllItems ();
	m_List.vSelect.clear ();
	m_List.ListRows.Init ();
	m_List.EditRow = 0;
	m_List.EditCol = m_List.PosPrEkN;
	DestroyRows (DbRows);
	int i = 0;
	m_List.m_Mdn = Mdn.mdn.mdn;

	memcpy (&Akt_krz.akt_krz, &akt_krz_null, sizeof (AKT_KRZ));
	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	Akt_krz.akt_krz.mdn_gr = dMdnGr;
	Akt_krz.akt_krz.mdn = Mdn.mdn.mdn;
	Akt_krz.akt_krz.fil_gr = dFilGr;
	Akt_krz.akt_krz.fil = dFil;
	Akt_krz.akt_krz.lad_akt_von = GueAb;

	A_pr.sqlopen (AktKrzCursor);
	while (A_pr.sqlfetch (AktKrzCursor) == 0)
	{
		A_pr.a_pr.a = Akt_krz.akt_krz.a;
		A_bas.a_bas.a = Akt_krz.akt_krz.a;
		A_bas.dbreadfirst ();
		SetMeEinhAbverk ();
		A_pr.a_pr.mdn_gr = Akt_krz.akt_krz.mdn_gr;
		A_pr.a_pr.mdn = Akt_krz.akt_krz.mdn;
		A_pr.a_pr.fil_gr = Akt_krz.akt_krz.fil_gr;
		A_pr.a_pr.fil = Akt_krz.akt_krz.fil;
		dsqlstatus = A_pr.dbreadfirst ();
		if (dsqlstatus)
		{
			continue;
		}

		AprListAEntry *aprListAEntry = new AprListAEntry (A_pr.a_pr.a,
														A_pr.a_pr.mdn_gr,
													   A_pr.a_pr.mdn,
													   A_pr.a_pr.fil_gr,
													   A_pr.a_pr.fil);
		AprList.push_back (aprListAEntry);

		if (dsqlstatus == 0)
		{
			dsqlstatus = Akt_krz.dbreadfirst();
		}
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		Mdn.mdn.mdn = A_pr.a_pr.mdn;
		if (Mdn.dbreadfirst () == 0)
		{
			MdnAdr.adr.adr = Mdn.mdn.adr;
			MdnAdr.dbreadfirst ();
		}
		Fil.fil.mdn = A_pr.a_pr.mdn;
		Fil.fil.fil = A_pr.a_pr.fil;
		if (Fil.dbreadfirst () == 0)
		{
			FilAdr.adr.adr = Fil.fil.adr;
			FilAdr.dbreadfirst ();
		}
		FillList.InsertItem (i, 0);

		CString cA;
		cA.Format (_T("%.0lf %s %s"), A_bas.a_bas.a, CStrFuncs::clipped(A_bas.a_bas.a_bz1),CStrFuncs::clipped(A_bas.a_bas.a_bz2));
		FillList.SetItemText (cA.GetBuffer (), i, m_List.PosA);


		CString cLadAktVon = _T("");
		Akt_krz.FromDbDate (cLadAktVon, &Akt_krz.akt_krz.lad_akt_von);
		FillList.SetItemText (cLadAktVon.GetBuffer (), i, m_List.PosDate);

 		CString PrEk;
		m_List.DoubleToString (A_pr.a_pr.pr_ek_euro, PrEk, m_List.scale);
		FillList.SetItemText (PrEk.GetBuffer (), i, m_List.PosPrEk);

		CString PrVk;
		m_List.DoubleToString (A_pr.a_pr.pr_vk_euro, PrVk, 2);
		FillList.SetItemText (PrVk.GetBuffer (), i, m_List.PosPrVk);

		CString PrEkN;
		m_List.DoubleToString (Akt_krz.akt_krz.pr_ek_sa_euro, PrEkN, m_List.scale);
		FillList.SetItemText (PrEkN.GetBuffer (), i, m_List.PosPrEkN);

		CString PrVkN;
		m_List.DoubleToString (Akt_krz.akt_krz.pr_vk_sa_euro, PrVkN, 2);
		FillList.SetItemText (PrVkN.GetBuffer (), i, m_List.PosPrVkN);

		if (ShowAbverk)
		{
			if (A_bas.a_bas.inh_abverk == 0.0)
			{
				A_bas.a_bas.inh_abverk = 1.0;
			}
			double pr_ek = Akt_krz.akt_krz.pr_ek_sa_euro / A_bas.a_bas.inh_abverk;
			double pr_vk = Akt_krz.akt_krz.pr_vk_sa_euro / A_bas.a_bas.inh_abverk;
			m_List.DoubleToString (pr_ek, PrEk, m_List.scale);
			FillList.SetItemText (PrEk.GetBuffer (), i, m_List.PosPrEkAb);
			m_List.DoubleToString (pr_vk, PrVk, 2);
			FillList.SetItemText (PrVk.GetBuffer (), i, m_List.PosPrVkAb);
		}
		else
		{
			FillList.SetItemText (_T(""), i, m_List.PosPrEkAb);
			FillList.SetItemText (_T(""), i, m_List.PosPrVkAb);
		}

		CAktKrzPreise *akt_krz = new CAktKrzPreise (PrEk, PrVk, Akt_krz.akt_krz);
		DbRows.Add (akt_krz);
		m_List.ListRows.Add (akt_krz);
		/****
		i ++;
		while (Akt_krz.sqlfetch (AktKrzCursor) == 0)
		{
			dsqlstatus = Akt_krz.dbreadfirst();
			FillList.InsertItem (i, 0);

			CString Art;
			m_List.DoubleToString (Akt_krz.akt_krz.a, Art, 0);
			FillList.SetItemText (Art.GetBuffer (), i, m_List.PosA);

			CString cLadAktVon = _T("");
			Akt_krz.FromDbDate (cLadAktVon, &Akt_krz.akt_krz.lad_akt_von);
			FillList.SetItemText (cLadAktVon.GetBuffer (), i, m_List.PosDate);

 			CString PrEk;
			m_List.DoubleToString (A_pr.a_pr.pr_ek_euro, PrEk, m_List.scale);
			FillList.SetItemText (PrEk.GetBuffer (), i, m_List.PosPrEk);

			CString PrVk;
			m_List.DoubleToString (A_pr.a_pr.pr_vk_euro, PrVk, 2);
			FillList.SetItemText (PrVk.GetBuffer (), i, m_List.PosPrVk);

			CString PrEkN;
			m_List.DoubleToString (Akt_krz.akt_krz.pr_ek_sa_euro, PrEkN, m_List.scale);
			FillList.SetItemText (PrEkN.GetBuffer (), i, m_List.PosPrEkN);

			CString PrVkN;
			m_List.DoubleToString (Akt_krz.akt_krz.pr_vk_sa_euro, PrVkN, 2);
			FillList.SetItemText (PrVkN.GetBuffer (), i, m_List.PosPrVkN);

			CAktKrzPreise *akt_krz = new CAktKrzPreise (PrEk, PrVk, Akt_krz.akt_krz);
			DbRows.Add (akt_krz);
			m_List.ListRows.Add (akt_krz);
			i ++;
		}
		***/
	}

	/***
	Akt_krz.sqlopen (AktKrzCursor2);
	while (Akt_krz.sqlfetch (AktKrzCursor2) == 0)
	{
		if (m_List.m_Mdn != 0 && Akt_krz.akt_krz.mdn != m_List.m_Mdn)
		{
			continue;
		}

		dsqlstatus = Akt_krz.dbreadfirst ();
		InsertAktKrzEntry ();
	}
	****/
	DestroyAprList ();

	Mdn.mdn.mdn = m_List.m_Mdn;
	return TRUE;
}


BOOL CNewPrPageDat::IsChanged (CAktKrzPreise *pAktKrz, CAktKrzPreise *old_akt_krz)
{
	DbRows.FirstPosition ();
	CAktKrzPreise *akt_krz;
	while ((akt_krz = (CAktKrzPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Akt_krz.akt_krz, &pAktKrz->akt_krz, sizeof (AKT_KRZ));
		if (Akt_krz == akt_krz->akt_krz) break;
	} 	
	if (akt_krz == NULL)
	{
		old_akt_krz->akt_krz.pr_ek_sa_euro = 0.0;
		old_akt_krz->akt_krz.pr_vk_sa_euro = 0.0;
		return TRUE;
	}
	memcpy (&old_akt_krz->akt_krz,  &akt_krz->akt_krz, sizeof (AKT_KRZ));
	if (pAktKrz->cEk != akt_krz->cEk) return TRUE;
	if (pAktKrz->cVk != akt_krz->cVk) return TRUE;
	return FALSE;
}

BOOL CNewPrPageDat::InList (AKT_KRZ_CLASS& Akt_krz)
{
	ListRows.FirstPosition ();
	CAktKrzPreise *akt_krz;
	while ((akt_krz = (CAktKrzPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Akt_krz == akt_krz->akt_krz) return TRUE;
	}
    return FALSE;
}

void CNewPrPageDat::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CAktKrzPreise *akt_krz;
	while ((akt_krz = (CAktKrzPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Akt_krz.akt_krz, &akt_krz->akt_krz, sizeof (AKT_KRZ));
//		if (!InList (Akt_krz))  generell erst mal l�schen, damit es mit neuem Datum wieder beschrieben werden kann
//		{
			Akt_krz.dbdelete ();
//			PgrProt.Write (1);
//		}
	}
}

BOOL CNewPrPageDat::Write ()
{
	extern short sql_mode;
	short sql_s;
	if (m_GueAb.IsWindowEnabled ())
	{
		return FALSE;
	}

// Pr�fen auf doppelte Eintr�ge 

	m_List.StopEnter ();
	int count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		if (!m_List.TestAprIndexM (i))
		{
			MessageBox (_T("Die Daten k�nnen nicht gespeichert werden!\n")
				        _T("Es sind mindestens 2 Eintr�ge auf gleicher Unternehmesebene vorhanden"),
						NULL,
						MB_OK | MB_ICONERROR);
			return FALSE;
		}
		if (!m_List.TestDate (i))
		{
			return FALSE;
		}
	}

	sql_s = sql_mode;
//	sql_mode = 1;
	A_pr.beginwork ();
	m_List.StopEnter ();
	count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 AKT_KRZ *akt_krz = new AKT_KRZ;
		 memcpy (akt_krz, &akt_krz_null, sizeof (AKT_KRZ));
         CString Text;
		 Text = m_List.GetItemText (i, m_List.PosA);
		 akt_krz->a = atoi (Text);
			A_bas.a_bas.a =akt_krz->a;
			A_bas.dbreadfirst ();
			SetMeEinhAbverk ();
     
			akt_krz->mdn_gr = dMdnGr;
			akt_krz->mdn = Mdn.mdn.mdn;
			akt_krz->fil_gr = dFilGr;
			akt_krz->fil = dFilGr;

     	 CString LadAktVon =  m_List.GetItemText (i, m_List.PosDate);
		 DB_CLASS::ToDbDate (LadAktVon, &akt_krz->lad_akt_von);
     	 CString PrEk =  m_List.GetItemText (i, m_List.PosPrEkN);
		 akt_krz->pr_ek_sa_euro = CStrFuncs::StrToDouble (PrEk);
		 CString PrVk =  m_List.GetItemText (i, m_List.PosPrVkN);
		 akt_krz->pr_vk_sa_euro = CStrFuncs::StrToDouble (PrVk);

		 if (ShowAbverk)
		 {
     		CString PrEkBasis =  m_List.GetItemText (i, m_List.PosPrEkAb);
			akt_krz->pr_ek_sa = CStrFuncs::StrToDouble (PrEkBasis);
			CString PrVkBasis =  m_List.GetItemText (i, m_List.PosPrVkAb);
			akt_krz->pr_vk_sa = CStrFuncs::StrToDouble (PrVkBasis);
		 }
		 else
		 {
			akt_krz->pr_ek_sa = akt_krz->pr_ek_sa_euro;
			akt_krz->pr_vk_sa = akt_krz->pr_vk_sa_euro;
		 }

		 if (!TestDecValues (akt_krz)) 
		 {
			 MessageBox (_T("Maximalwert f�r Preisfeld �berschritten"));
			 return FALSE;
		 }

		 memcpy (&akt_krz->bel_akt_von, &akt_krz->lad_akt_von, sizeof (DATE_STRUCT));
		 memcpy (&akt_krz->bel_akt_bis, &akt_krz->lad_akt_bis, sizeof (DATE_STRUCT));
		 CAktKrzPreise *pr = new CAktKrzPreise (PrEk, PrVk, *akt_krz);
	//	 if (akt_krz->pr_ek_sa_euro != 0.0 || 
	//		 akt_krz->pr_vk_sa_euro != 0.0)
	//	 {
				ListRows.Add (pr);
	//	 }
		 delete akt_krz;
	}

	DeleteDbRows ();

	ListRows.FirstPosition ();
	CAktKrzPreise *akt_krz;
	while ((akt_krz = (CAktKrzPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&Akt_krz.akt_krz, &akt_krz->akt_krz, sizeof (AKT_KRZ));
//		CString Date;
//		CStrFuncs::SysDate (Date);
		strcpy ((LPSTR) Akt_krz.akt_krz.lad_akv_sa, "9");
		strcpy ((LPSTR) Akt_krz.akt_krz.lief_akv_sa, "9");

// Tabelle a_pr aktualisieren

		memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
		A_pr.a_pr.a = Akt_krz.akt_krz.a; 
		A_pr.a_pr.mdn_gr = Akt_krz.akt_krz.mdn_gr; 
		A_pr.a_pr.mdn    = Akt_krz.akt_krz.mdn; 
		A_pr.a_pr.fil_gr = Akt_krz.akt_krz.fil_gr; 
		A_pr.a_pr.fil    = Akt_krz.akt_krz.fil; 

		AKT_KRZ AktKrzWrite;
		memcpy (&AktKrzWrite, &Akt_krz.akt_krz, sizeof (AKT_KRZ));
		CAktKrzPreise old_akt_krz;
		if (IsChanged (akt_krz, &old_akt_krz) || TRUE )  // jetzt immer schreiben, da auch DeleteDbRows alles gel�scht hat !
		{
			memcpy (&Akt_krz.akt_krz, &AktKrzWrite, sizeof (AKT_KRZ));
			Akt_krz.akt_krz.lad_akt_von = GueAb;
			Akt_krz.dbupdate ();

			PrProt.Write ( 1 ,old_akt_krz.akt_krz.pr_ek_sa_euro, old_akt_krz.akt_krz.pr_vk_sa_euro);	// 070613

			Akt_krz.sqlexecute (NullCursor);
/*
			if (Write160 && dbild160 != NULL)
			{
				CStringA Command;
				Command.Format ("bild160 B %.0lf %hd %hd %hd %hd",
					             A_bas.a_bas.a,
								 A_pr.a_pr.mdn_gr,
								 A_pr.a_pr.mdn,
								 A_pr.a_pr.fil_gr,
								 A_pr.a_pr.fil); 

				(*dbild160) (Command.GetBuffer ());
			}
*/
		}
	}
	EnableHeadControls (TRUE);
	m_GueAb.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	A_pr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

BOOL CNewPrPageDat::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_GueAb.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Alle Eintr�ge l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	A_pr.beginwork ();
	m_List.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	m_List.DeleteAllItems ();

	EnableHeadControls (TRUE);
	m_GueAb.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	A_pr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

/***
void CNewPrPageDat::OnAchoice ()
{
    AChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceA (this);
	    Choice->IsModal = ModalChoice;
#ifndef ARTIKEL
	    Choice->HideEnter = FALSE;
#endif
	    Choice->HideFilter = FALSE;
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->SearchText = SearchA;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

		Choice->MoveWindow (&rect);
		Choice->SetFocus ();

		return;
	}
    if (Choice->GetState ())
    {
		  CABasList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_GueAb.EnableWindow (TRUE);
		  m_GueAb.SetFocus ();
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
	 	  AChoiceStat = FALSE;	
	}
}
*******/

void CNewPrPageDat::OnAktKrzChoice ()
{
    AChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
//		Choice->ShowWindow (SW_SHOWNORMAL);
//		return;
		//da die Auswahl jetzt durch eingabe eines anderen oder Neuen G�ltigkeitsdaums ver�nderbar ist, soll jedesmal neu Aufgebaut werden
		Choice = NULL;
	}
	if (Choice == NULL)
	{
		Choice = new CChoicePrTermin (this);
	    Choice->IsModal = ModalChoice;
	    Choice->m_Mdn = Mdn.mdn.mdn;
		Choice->CreateDlg ();
	}

    Choice->SetDbClass (&A_bas);
	//Choice->SearchText = Search;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{

		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 400;
		rect.bottom = scy - 50;
		Choice->MoveWindow (&rect);
		Choice->SetListFocus ();  

		return;
	}
    if (Choice->GetState ())
    {
		  CAktKrzList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
//          memcpy (&Akt_krz.akt_krz, &akt_krz_null, sizeof (AKTKRZ));
//          Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
//          Iprgrstufk.iprgrstufk.pr_gr_stuf = abl->pr_gr_stuf;
          dMdnGr = abl->mdn_gr;
          dFilGr = abl->fil_gr;
          dFil = abl->fil;
//		  if (Iprgrstufk.dbreadfirst () == 0)
//		  {
			Form.Show ();
			EnableHeadControls (TRUE);
			m_GueAb.SetFocus ();
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
//		  }
    }
	else
	{
	 	  AChoiceStat = FALSE;	
	}
}


void CNewPrPageDat::OnAktKrzSelected ()
{
	if (Choice == NULL) return;
    CAktKrzList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    dMdnGr = abl->mdn_gr;
    Mdn.mdn.mdn = abl->mdn;
    dFilGr = abl->fil_gr;
    dFil = abl->fil;
    GueAb = abl->lad_akt_von;


//    if (A_bas.dbreadfirst () == 0)
//    {
		m_GueAb.EnableWindow (TRUE);
		m_GueAb.SetFocus ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
//    }
	if (CloseChoice)
	{
		OnAktKrzCanceled (); 
	}
    Form.Show ();
	if (Choice->FocusBack)
	{
		Read ();
		m_List.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CNewPrPageDat::OnAktKrzCanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CNewPrPageDat::StepBack ()
{
	if (m_GueAb.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		m_List.StopEnter ();
		EnableHeadControls (TRUE);
		m_GueAb.SetFocus ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
		m_List.DeleteAllItems ();
	}
	return TRUE;
}

void CNewPrPageDat::OnCancel ()
{
	StepBack ();
}

void CNewPrPageDat::OnSave ()
{
	Write ();
}

void CNewPrPageDat::FillMdnGrCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0"));
	Values.Add (Value);
    Mdn.sqlopen (MdnGrCursor);
	while (Mdn.sqlfetch (MdnGrCursor) == 0)
	{
		Gr_zuord.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr,
			                          Gr_zuord.gr_zuord.gr_bz1);
		Values.Add (Value);
	}
	m_List.FillMdnGrCombo (Values);
}

void CNewPrPageDat::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}


void CNewPrPageDat::FillMdnCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Zentrale"));
	Values.Add (Value);
    Mdn.sqlopen (MdnCursor);
	while (Mdn.sqlfetch (MdnCursor) == 0)
	{
		Mdn.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Mdn.mdn.mdn,
			                          MdnAdr.adr.adr_krz);
		Values.Add (Value);
	}
	m_List.FillMdnCombo (Values);
}


void CNewPrPageDat::FillFilCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Mandant"));
	Values.Add (Value);
    Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		Fil.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Fil.fil.fil,
			                          FilAdr.adr.adr_krz);
		Values.Add (Value);
	}
	m_List.FillFilCombo (Values);
}

void CNewPrPageDat::OnDelete ()
{
	m_List.DeleteRow ();
}

void CNewPrPageDat::OnInsert ()
{
	m_List.InsertRow ();
}

/*
BOOL CNewPrPageDat::Print ()
{
	CProcess print;
	CString Command = "70001 11112";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}
*/

BOOL CNewPrPageDat::Print ()
{
	CProcess print;
	CString Command;

	if (!m_GueAb.IsWindowEnabled ())
	{
		Form.Get ();
		LPTSTR tmp = getenv ("TMPPATH");
		CString dName;
		FILE *fp;
		if (tmp != NULL)
		{
			dName.Format ("%s\\11200t.llf", tmp);
		}
		else
		{
			dName = "11200t.llf";
		}
		CString Command;
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			fprintf (fp, "NAME 11200t\n");
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
			fprintf (fp, "a %.0lf %.0lf\n", A_bas.a_bas.a,
										A_bas.a_bas.a);
			fclose (fp);
			Command.Format ("dr70001 -name 11200t -datei %s", dName.GetBuffer ());
		}
		else
		{
			Command = "dr70001 -name 11200t";
		}
	}
	else
	{
		Command = "dr70001 -name 11200t";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CNewPrPageDat::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_List.StartPauseEnter ();
}

void CNewPrPageDat::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_List.EndPauseEnter ();
}

void CNewPrPageDat::EnableHeadControls (BOOL enable)
{
	HeadControls.Enable (enable);
	PosControls.Enable (!enable);
}

void CNewPrPageDat::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CAPreise *ipr;
	while ((ipr = (CAPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

void CNewPrPageDat::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_List.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_List.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_List.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_List.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CNewPrPageDat::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_List.ListEdit ||
        Control == &m_List.ListComboBox ||
		Control == &m_List.SearchListCtrl.Edit)	
	{
		ListCopy ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CNewPrPageDat::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CNewPrPageDat::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_List.ListEdit.m_hWnd) ||
		IsWindow (m_List.ListComboBox) ||
		IsWindow (m_List.SearchListCtrl.Edit))
	{
		m_List.StopEnter ();
		m_List.StartEnter (m_List.EditCol, m_List.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
//		for (int i = 0; i < MAXLISTROWS && i < m_List.GetItemCount (); i ++)
		for (int i = 0; i < m_List.GetItemCount (); i ++)
		{
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = "";
			int cols = m_List.GetHeaderCtrl ()->GetItemCount ();
			for (int j = 0; j < cols; j ++)
			{
				if (Row != "")
				{
					Row += Separator;
				}
				CString Field = m_List.GetItemText (i, j);
				Field.TrimRight ();
				Row += Field; 
			}
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

BOOL CNewPrPageDat::TestDecValues (AKT_KRZ *akt_krz)
{
	if (akt_krz->pr_ek_sa_euro > 9999.9999 ||
		akt_krz->pr_vk_sa_euro < -9999.9999)
	{
		return FALSE;
	}

	if (akt_krz->pr_ek_sa_euro > 9999.99 ||
		akt_krz->pr_vk_sa_euro < -9999.99)
	{
		return FALSE;
	}
	if (akt_krz->lad_akt_bis.day == 0 &&
		akt_krz->lad_akt_bis.month == 0 &&  
		akt_krz->lad_akt_bis.year == 0)
	{
		akt_krz->lad_akt_bis.day = 01;
		akt_krz->lad_akt_bis.month = 01;
		akt_krz->lad_akt_bis.year = 1900;
	}

	return TRUE;
}

void CNewPrPageDat::OnBasisPr ()
{
	m_List.StopEnter ();
	if (m_List.GetEnterBasis ())
	{
		m_BasisPr.SetWindowText (_T("Basispreise"));
		m_List.SetEnterBasis (FALSE);
		m_List.RemoveDisplayOnly (m_List.PosPrEkN);
		m_List.RemoveDisplayOnly (m_List.PosPrVkN);
		m_List.AddDisplayOnly (m_List.PosPrEkAb);
		m_List.AddDisplayOnly (m_List.PosPrVkAb);
		if (m_List.GetItemCount () > 0)
		{
			if (m_List.EditCol == m_List.PosPrEkAb ||
				m_List.EditCol == m_List.PosPrVkAb)
			{
				m_List.EditCol = m_List.PosPrEkN;
			}
			m_List.StartEnter (m_List.EditCol, m_List.EditRow);
			m_List.PostMessage (WM_SETFOCUS, 0, 0);
		}
	}
	else
	{
		m_BasisPr.SetWindowText (_T("Abverkaufspreise"));
		m_List.SetEnterBasis (TRUE);
		m_List.RemoveDisplayOnly (m_List.PosPrEkAb);
		m_List.RemoveDisplayOnly (m_List.PosPrVkAb);
		m_List.AddDisplayOnly (m_List.PosPrEkN);
		m_List.AddDisplayOnly (m_List.PosPrVkN);
		if (m_List.GetItemCount () > 0)
		{
			if (m_List.EditCol == m_List.PosPrEkN ||
				m_List.EditCol == m_List.PosPrVkN)
			{
				m_List.EditCol = m_List.PosPrEkAb;
			}
			m_List.StartEnter (m_List.EditCol, m_List.EditRow);
			m_List.PostMessage (WM_SETFOCUS, 0, 0);
		}
	}
}


void CNewPrPageDat::SetListMode ()
{
	if (m_List.ListMode == CNewPrListACtrl::Compact)
	{
		m_List.SetListMode (CNewPrListACtrl::Large);
	}
	else
	{
		m_List.SetListMode (CNewPrListACtrl::Compact);
	}

	if (!m_GueAb.IsWindowEnabled ())
	{
		ReadList ();
	}

	CView *parent = (CView *) GetParent ()->GetParent ();
	CDocument *doc = parent->GetDocument ();
	doc->UpdateAllViews (parent);
}

BOOL CNewPrPageDat::IsCompactMode ()
{
	if (m_List.ListMode == CNewPrListACtrl::Compact)
	{
		return TRUE;
	}
	return FALSE;
}

void CNewPrPageDat::Save ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	File.Open (Path.GetBuffer (), CFile::modeCreate | CFile::modeWrite);
	CArchive archive(&File, CArchive::store);

	try
	{
			archive.Write (&DlgBkColor, sizeof (DlgBkColor));
			archive.Write (&ListBkColor, sizeof (ListBkColor));
			archive.Write (&FlatLayout, sizeof (FlatLayout));
			archive.Write (&HideOK, sizeof (HideOK));
			archive.Write (&m_List.ListMode, sizeof (&m_List.ListMode)); 
	}
	catch (...) 
	{
		return;
	}


	archive.Close ();
	File.Close ();
}

void CNewPrPageDat::Load ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}

	if (!File.Open (Path.GetBuffer (), CFile::modeRead))
	{
		return;
	}
	CArchive archive(&File, CArchive::load);
	try
	{
			archive.Read (&DlgBkColor, sizeof (DlgBkColor));
			archive.Read (&ListBkColor, sizeof (ListBkColor));
			archive.Read (&FlatLayout, sizeof (FlatLayout));
			archive.Read (&HideOK, sizeof (HideOK));
			archive.Read (&m_List.ListMode, sizeof (&m_List.ListMode)); 
	}
	catch (...) 
	{
		return;
	}

	archive.Close ();
	File.Close ();
}

void CNewPrPageDat::ShowMeEinhAbverk (BOOL show)
{
	/** 
	if (show)
	{
		m_LMe_einh_abverk.ShowWindow (SW_SHOWNORMAL);
		m_Me_einh_abverk.ShowWindow (SW_SHOWNORMAL);
		m_LInh.ShowWindow (SW_SHOWNORMAL);
		m_Inh.ShowWindow (SW_SHOWNORMAL);
		m_BasisPr.ShowWindow (SW_SHOWNORMAL);
	}
	else
	{
		m_LMe_einh_abverk.ShowWindow (SW_HIDE);
		m_Me_einh_abverk.ShowWindow (SW_HIDE);
		m_LInh.ShowWindow (SW_HIDE);
		m_Inh.ShowWindow (SW_HIDE);
		m_BasisPr.ShowWindow (SW_HIDE);
	}
	**/
}

void CNewPrPageDat::SetMeEinhAbverk ()
{
    CDbUniCode UniCode; 
	TCHAR tstr [sizeof (Ptab.ptabn.ptbez)];
	

	_tcscpy (Ptab.ptabn.ptitem, _T("me_einh"));
	_stprintf (Ptab.ptabn.ptwert, "%hd", A_bas.a_bas.me_einh); 
	_tcscpy (Ptab.ptabn.ptbez, _T(""));
	_tcscpy (Ptab.ptabn.ptbezk, _T(""));
	Ptab.dbreadfirst ();
	CDbUniCode::DbToUniCode (tstr, (LPSTR) Ptab.ptabn.ptbez);
	m_MeEinh = tstr;

	_stprintf (Ptab.ptabn.ptwert, "%hd", A_bas.a_bas.me_einh_abverk); 
	_tcscpy (Ptab.ptabn.ptbez, _T(""));
	_tcscpy (Ptab.ptabn.ptbezk, _T(""));
	Ptab.dbreadfirst ();
	CDbUniCode::DbToUniCode (tstr, (LPSTR) Ptab.ptabn.ptbez);
	m_MeEinhAbverk = tstr;
	m_Inhalt.Format (_T ("%.3lf %s"), A_bas.a_bas.inh_abverk, m_MeEinh.GetBuffer ());  

	if (A_bas.a_bas.me_einh == A_bas.a_bas.me_einh_abverk)
	{
		    ShowAbverk = FALSE;
			ShowMeEinhAbverk (FALSE);
			m_List.SetColumnWidth (m_List.PosPrEkAb, 0);
			m_List.SetColumnWidth (m_List.PosPrVkAb, 0);
			if (m_List.GetEnterBasis ())
			{
				m_BasisPr.SetWindowText (_T("Basispreise"));
				m_List.SetEnterBasis (FALSE);
				m_List.RemoveDisplayOnly (m_List.PosPrEkN);
				m_List.RemoveDisplayOnly (m_List.PosPrVkN);
				m_List.AddDisplayOnly (m_List.PosPrEkAb);
				m_List.AddDisplayOnly (m_List.PosPrVkAb);
				if (m_List.EditCol == m_List.PosPrEkAb ||
					m_List.EditCol == m_List.PosPrVkAb)
				{
					m_List.EditCol = m_List.PosPrEkN;
				}
			}
	}
	else
	{
		    ShowAbverk = TRUE;
			ShowMeEinhAbverk (TRUE);
			m_List.SetColumnWidth (m_List.PosPrEkAb, 80);
			m_List.SetColumnWidth (m_List.PosPrVkAb, 100);
	}
    m_List.SetShowAbverk (ShowAbverk);
}


void CNewPrPageDat::OnNMKillfocusGueAb2(NMHDR *pNMHDR, LRESULT *pResult)
{
	Form.Get ();
	Form.Show ();

	*pResult = 0;
}
