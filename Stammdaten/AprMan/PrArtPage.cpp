// PrArtPreise.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "AprMan.h"
#include "PrArtPage.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Process.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CPrArtPage Dialogfeld

CPrArtPage::CPrArtPage(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage()
{
	Choice = NULL;
	ModalChoice = FALSE;
	CloseChoice = FALSE;
	AprCursor = -1;
	MdnGrCursor = -1;
	MdnCursor = -1;
	FilCursor = -1;
	HeadControls.Add (&m_A);
	PosControls.Add (&m_List);

    HideButtons = FALSE;

	Frame = NULL;
	DbRows.Init ();
	ListRows.Init ();
	SearchA = _T("");
	Separator = _T(";");
	CellHeight = 0;
	Cfg.SetProgName( _T("AprMan"));
}

CPrArtPage::CPrArtPage(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	Frame = NULL;
	AprCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
}

CPrArtPage::~CPrArtPage()
{
	Font.DeleteObject ();
	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	A_pr.dbclose ();
	MdnAdr.dbclose ();
	if (AprCursor != -1)
	{
		A_bas.sqlclose (AprCursor);
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);
}

void CPrArtPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_LA_BZ1, m_LA_bz1);
	DDX_Control(pDX, IDC_A_BZ1, m_A_bz1);
	DDX_Control(pDX, IDC_A_BZ2, m_A_bz2);
	DDX_Control(pDX, IDC_LIST, m_List);
}

BEGIN_MESSAGE_MAP(CPrArtPage, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_SIZE ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_ACHOICE ,  OnAchoice)
	ON_BN_CLICKED(IDC_CANCEL ,   OnCancel)
	ON_BN_CLICKED(IDC_SAVE ,     OnSave)
	ON_BN_CLICKED(IDC_DELETE ,   OnDelete)
	ON_BN_CLICKED(IDC_INSERT ,   OnInsert)
	ON_COMMAND (SELECTED, OnASelected)
	ON_COMMAND (CANCELED, OnACanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
END_MESSAGE_MAP()


// CPrArtPage Meldungshandler

BOOL CPrArtPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	A_bas.opendbase (_T("bws"));

	CUtil::GetPersName (PersName);
	PrProt.Construct (PersName, CString ("11200"), &A_pr);
     
	ReadCfg ();

	if (HideButtons)
	{
		ButtonControls.SetVisible (FALSE);
		RightListSpace = 15;
	}
	else
	{
		ButtonControls.SetVisible (TRUE);
		RightListSpace = 125;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

    Form.Add (new CFormField (&m_A,EDIT,       (double *) &A_bas.a_bas.a, VDOUBLE, 13, 0));
    Form.Add (new CUniFormField (&m_A_bz1,EDIT,   (char *) A_bas.a_bas.a_bz1, VCHAR));
    Form.Add (new CUniFormField (&m_A_bz2,EDIT,   (char *) A_bas.a_bas.a_bz2, VCHAR));

	A_pr.sqlin ((double *) &A_pr.a_pr.a,  SQLDOUBLE, 0);
	A_pr.sqlout ((short *)  &A_pr.a_pr.mdn_gr, SQLSHORT, 0);
	A_pr.sqlout ((short *)  &A_pr.a_pr.mdn, SQLSHORT, 0);
	A_pr.sqlout ((short *)  &A_pr.a_pr.fil_gr, SQLSHORT, 0);
	A_pr.sqlout ((short *)  &A_pr.a_pr.fil, SQLSHORT, 0);
	AprCursor = A_pr.sqlcursor (_T("select mdn_gr, mdn, fil_gr, fil from a_pr ")
		                       _T("where a = ? ")
		                       _T("order by mdn_gr, mdn, fil_gr, fil"));

	Gr_zuord.sqlout ((short *) &Gr_zuord.gr_zuord.gr, SQLSHORT, 0);
	Gr_zuord.sqlout ((char *) Gr_zuord.gr_zuord.gr_bz1, SQLCHAR, sizeof (Gr_zuord.gr_zuord.gr_bz1));
    MdnGrCursor = Gr_zuord.sqlcursor (_T("select gr, gr_bz1 from gr_zuord ")
 									  _T("where mdn = 0 ")
									  _T("and gr > 0 ")
								      _T("order by gr"));

	Mdn.sqlout ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);
	MdnAdr.sqlout ((char *) MdnAdr.adr.adr_krz, SQLCHAR, sizeof (MdnAdr.adr.adr_krz));
    MdnCursor = Mdn.sqlcursor (_T("select mdn.mdn, adr.adr_krz from mdn, adr ")
		                       _T("where mdn.mdn > 0 ")
							   _T("and adr.adr = mdn.adr ")
							   _T("order by mdn.mdn"));
/*
	Fil.sqlin ((short *) &Fil.fil.mdn, SQLSHORT, 0);
	Fil.sqlout ((short *) &Fil.fil.fil, SQLSHORT, 0);
	FilAdr.sqlout ((char *) &FilAdr.adr.adr_krz, SQLCHAR, sizeof (FilAdr.adr.adr_krz));
    FilCursor = Fil.sqlcursor (_T("select fil.fil, adr.adr_krz from fil, adr ")
		                       _T("where fil.mdn > ? ")
							   _T("and fil.fil > 0 ")
							   _T("and adr.adr = fil.adr ")
							   _T("order by fil.fil"));
*/

	m_List.Prepare ();

	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_List.SetImageList (&image, LVSIL_SMALL);   
	}

	FillList = m_List;
	FillList.SetStyle (LVS_REPORT);
	if (m_List.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Mandantengruppe"), 1, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("Mandant"), 2, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("Filialgruppe"), 3, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("Filiale"), 4, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("EK"), 5, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("VK"), 6, 100, LVCFMT_RIGHT);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	AGrid.Create (this, 1, 2);
    AGrid.SetBorder (0, 0);
    AGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 0, 0, 1, 1);
	AGrid.Add (c_A);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);

	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_LA_bz1  = new CCtrlInfo (&m_LA_bz1, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LA_bz1);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 1, 1, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 1, 2, 3, 1); 
	CtrlGrid.Add (c_A_bz2);
	CCtrlInfo *c_List  = new CCtrlInfo (&m_List, 0, 3, DOCKRIGHT, DOCKBOTTOM); 
//	c_List->rightspace = 125;
	c_List->rightspace = RightListSpace;
	CtrlGrid.Add (c_List);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	FillMdnGrCombo ();
	FillMdnCombo ();

	CtrlGrid.Display ();
	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
	Form.Show ();
	EnableHeadControls (TRUE);
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}


void CPrArtPage::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CPrArtPage::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CPrArtPage::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CPrArtPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CPrArtPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_List.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_List &&
					GetFocus ()->GetParent () != &m_List )
				{

					break;
			    }
				m_List.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnAchoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_A)
				{
					OnAchoice ();
					return TRUE;
				}
				m_List.OnKey9 ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CPrArtPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_A)
	{
		if (!Read ())
		{
			m_A.SetFocus ();
			return FALSE;
		}
	}

	if (Control != &m_List.ListEdit &&
		Control != &m_List.ListComboBox &&
		Control != &m_List.SearchListCtrl.Edit)
{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CPrArtPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_List &&
		Control->GetParent ()!= &m_List )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_List.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}


BOOL CPrArtPage::read ()
{
	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}
	return Read ();

}

BOOL CPrArtPage::Read ()
{
	if (ModalChoice)
	{
		CString cA;
		m_A.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnAchoice ();
			SearchA = "";
			if (!AChoiceStat)
			{
				m_A.SetFocus ();
				m_A.SetSel (0, -1);
				return FALSE;
			}
		}
	}

	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	Form.Get ();
	if (A_bas.dbreadfirst () == 0)
	{
	    EnableHeadControls (FALSE);
		ReadList ();
		Form.Show ();
		m_A.SetFocus ();
		m_A.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Artikel %.0lf nicht gefunden"),A_bas.a_bas.a);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);

		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		Form.Show ();
		m_A.SetFocus ();
		m_A.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CPrArtPage::ReadList ()
{
	m_List.StopEnter ();
	m_List.DeleteAllItems ();
	m_List.vSelect.clear ();
	m_List.ListRows.Init ();
	int i = 0;
	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	A_pr.a_pr.a   = A_bas.a_bas.a;
	A_pr.sqlopen (AprCursor);
	while (A_pr.sqlfetch (AprCursor) == 0)
	{
		A_pr.dbreadfirst ();
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		Mdn.mdn.mdn = A_pr.a_pr.mdn;
		if (Mdn.dbreadfirst () == 0)
		{
			MdnAdr.adr.adr = Mdn.mdn.adr;
			MdnAdr.dbreadfirst ();
		}
		Fil.fil.mdn = A_pr.a_pr.mdn;
		Fil.fil.fil = A_pr.a_pr.fil;
		if (Fil.dbreadfirst () == 0)
		{
			FilAdr.adr.adr = Fil.fil.adr;
			FilAdr.dbreadfirst ();
		}
		FillList.InsertItem (i, 0);
		CString cMdnGr;
		cMdnGr.Format (_T("%hd"), A_pr.a_pr.mdn_gr);
		FillList.SetItemText (cMdnGr.GetBuffer (), i, m_List.PosMdnGr);

		CString cMdn;
		if (A_pr.a_pr.mdn == 0)
		{
			cMdn.Format (_T("0  Zentrale"));
		}
		else
		{
			cMdn.Format (_T("%hd  %s"), A_pr.a_pr.mdn,
							MdnAdr.adr.adr_krz);
		}
		FillList.SetItemText (cMdn.GetBuffer (), i, m_List.PosMdn);

		CString cFilGr;
		cFilGr.Format (_T("%hd"), A_pr.a_pr.fil_gr);
		FillList.SetItemText (cFilGr.GetBuffer (), i, m_List.PosFilGr);

		CString cFil;
		if (A_pr.a_pr.fil == 0)
		{
			cFil.Format (_T("0  Mandant"));
		}
		else
		{
			cFil.Format (_T("%hd  %s"), A_pr.a_pr.fil,
							FilAdr.adr.adr_krz);
		}
		FillList.SetItemText (cFil.GetBuffer (), i, m_List.PosFil);

		CString PrEk;
		m_List.DoubleToString (A_pr.a_pr.pr_ek_euro, PrEk, 2);
		FillList.SetItemText (PrEk.GetBuffer (), i, m_List.PosPrEk);

		CString PrVk;
		m_List.DoubleToString (A_pr.a_pr.pr_vk_euro, PrVk, 2);
		FillList.SetItemText (PrVk.GetBuffer (), i, m_List.PosPrVk);

		CPreise *a_pr = new CPreise (PrEk, PrVk, A_pr.a_pr);
		DbRows.Add (a_pr);
		m_List.ListRows.Add (a_pr);
		i ++;
	}
	return TRUE;
}

BOOL CPrArtPage::IsChanged (CPreise *pApr, CPreise *old_a_pr)
{
	DbRows.FirstPosition ();
	CPreise *a_pr;
	while ((a_pr = (CPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&A_pr.a_pr, &pApr->a_pr, sizeof (A_PR));
		if (A_pr == a_pr->a_pr) break;
	} 	
	if (a_pr == NULL)
	{
		old_a_pr->a_pr.pr_ek_euro = 0.0;
		old_a_pr->a_pr.pr_vk_euro = 0.0;
		return TRUE;
	}
	memcpy (&old_a_pr->a_pr,  &a_pr->a_pr, sizeof (A_PR));
	if (pApr->cEk != a_pr->cEk) return TRUE;
	if (pApr->cVk != a_pr->cVk) return TRUE;
	return FALSE;
}

BOOL CPrArtPage::InList (APR_CLASS& A_pr)
{
	ListRows.FirstPosition ();
	CPreise *a_pr;
	while ((a_pr = (CPreise *) ListRows.GetNext ()) != NULL)
	{
		if (A_pr == a_pr->a_pr) return TRUE;
	}
    return FALSE;
}

void CPrArtPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CPreise *a_pr;
	while ((a_pr = (CPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&A_pr.a_pr, &a_pr->a_pr, sizeof (A_PR));
		if (!InList (A_pr))
		{
			A_pr.dbdelete ();
//			PgrProt.Write (1);
		}
	}
}

BOOL CPrArtPage::Write ()
{
	extern short sql_mode;
	short sql_s;
	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}

// Pr�fen auf doppelte Eintr�ge 

	int count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		if (!m_List.TestAprIndexM (i))
		{
			MessageBox (_T("Die Daten k�nnen nicht gespeichert werden!\n")
				        _T("Es sind mindestens 2 Eintr�ge auf gleicher Unternehmesebene vorhanden"),
						NULL,
						MB_OK | MB_ICONERROR);
			return FALSE;
		}
	}

	sql_s = sql_mode;
	sql_mode = 1;
	A_pr.beginwork ();
	m_List.StopEnter ();
	count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 A_PR *a_pr = new A_PR;
		 memcpy (a_pr, &a_pr_null, sizeof (A_PR));
         CString Text;
		 Text = m_List.GetItemText (i, m_List.PosMdnGr);
		 a_pr->mdn_gr = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosMdn);
		 a_pr->mdn = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosFilGr);
		 a_pr->fil_gr = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosFil);
		 a_pr->fil = atoi (Text);
     
     	 CString PrEk =  m_List.GetItemText (i, m_List.PosPrEk);
		 a_pr->pr_ek_euro = CStrFuncs::StrToDouble (PrEk);
		 a_pr->pr_ek = a_pr->pr_ek_euro;
		 CString PrVk =  m_List.GetItemText (i, m_List.PosPrVk);
		 a_pr->pr_vk_euro = CStrFuncs::StrToDouble (PrVk);
		 a_pr->pr_vk = a_pr->pr_vk_euro;
		 a_pr->a = A_bas.a_bas.a; 

		 if (!TestDecValues (a_pr)) 
		 {
			 MessageBox (_T("Maximalwert f�r Preisfeld �berschritten"));
			 return FALSE;
		 }

		 CPreise *pr = new CPreise (PrEk, PrVk, *a_pr);
		 if (a_pr->pr_ek_euro != 0.0 || 
			 a_pr->pr_vk_euro != 0.0 || a_pr->akt != -1)
		 {
				ListRows.Add (pr);
		 }
		 delete a_pr;
	}

	DeleteDbRows ();

	ListRows.FirstPosition ();
	CPreise *a_pr;
	while ((a_pr = (CPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&A_pr.a_pr, &a_pr->a_pr, sizeof (A_PR));
		CString Date;
		CStrFuncs::SysDate (Date);
		A_pr.ToDbDate (Date, &A_pr.a_pr.bearb);
		A_pr.a_pr.akt = -1;
		A_pr.dbreadfirst ();
		A_pr.a_pr.key_typ_dec13 = A_pr.a_pr.a;
		A_pr.a_pr.key_typ_sint = A_pr.a_pr.mdn;
		A_pr.a_pr.pr_ek_euro = a_pr->a_pr.pr_ek_euro;
		A_pr.a_pr.pr_ek = a_pr->a_pr.pr_ek;
		A_pr.a_pr.pr_vk_euro = a_pr->a_pr.pr_vk_euro;
		A_pr.a_pr.pr_vk = a_pr->a_pr.pr_vk;
		A_pr.dbupdate ();
		CPreise old_a_pr;
		if (IsChanged (a_pr, &old_a_pr))
		{
			PrProt.Write (old_a_pr.a_pr.pr_ek_euro, old_a_pr.a_pr.pr_vk_euro);
		}
	}
	EnableHeadControls (TRUE);
	m_A.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	A_pr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

BOOL CPrArtPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Alle Eintr�ge l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	A_pr.beginwork ();
	m_List.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	m_List.DeleteAllItems ();

	EnableHeadControls (TRUE);
	m_A.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	A_pr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

void CPrArtPage::OnAchoice ()
{
    AChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceA (this);
	    Choice->IsModal = ModalChoice;
	    Choice->HideEnter = FALSE;
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->SearchText = SearchA;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

/*
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		cy -= 100;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
*/
		Choice->MoveWindow (&rect);
		Choice->SetFocus ();

		return;
	}
    if (Choice->GetState ())
    {
		  CABasList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_A.EnableWindow (TRUE);
		  m_A.SetSel (0, -1, TRUE);
		  m_A.SetFocus ();
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
	 	  AChoiceStat = FALSE;	
	}
}

void CPrArtPage::OnASelected ()
{
	if (Choice == NULL) return;
    CABasList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    A_bas.a_bas.a = abl->a;
    if (A_bas.dbreadfirst () == 0)
    {
		m_A.EnableWindow (TRUE);
		m_A.SetFocus ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	if (CloseChoice)
	{
		OnACanceled (); 
	}
    Form.Show ();
	if (Choice->FocusBack)
	{
		Read ();
		m_List.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CPrArtPage::OnACanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CPrArtPage::StepBack ()
{
	if (m_A.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		m_List.StopEnter ();
		EnableHeadControls (TRUE);
		m_A.SetFocus ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
		m_List.DeleteAllItems ();
	}
	return TRUE;
}

void CPrArtPage::OnCancel ()
{
	StepBack ();
}

void CPrArtPage::OnSave ()
{
	Write ();
}

void CPrArtPage::FillMdnGrCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0"));
	Values.Add (Value);
    Mdn.sqlopen (MdnGrCursor);
	while (Mdn.sqlfetch (MdnGrCursor) == 0)
	{
		Gr_zuord.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr,
			                          Gr_zuord.gr_zuord.gr_bz1);
		Values.Add (Value);
	}
	m_List.FillMdnGrCombo (Values);
}


void CPrArtPage::FillMdnCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Zentrale"));
	Values.Add (Value);
    Mdn.sqlopen (MdnCursor);
	while (Mdn.sqlfetch (MdnCursor) == 0)
	{
		Mdn.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Mdn.mdn.mdn,
			                          MdnAdr.adr.adr_krz);
		Values.Add (Value);
	}
	m_List.FillMdnCombo (Values);
}


void CPrArtPage::FillFilCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Mandant"));
	Values.Add (Value);
    Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		Fil.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Fil.fil.fil,
			                          FilAdr.adr.adr_krz);
		Values.Add (Value);
	}
	m_List.FillFilCombo (Values);
}

void CPrArtPage::OnDelete ()
{
	m_List.DeleteRow ();
}

void CPrArtPage::OnInsert ()
{
	m_List.InsertRow ();
}

/*
BOOL CPrArtPage::Print ()
{
	CProcess print;
	CString Command = "70001 11112";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}
*/

BOOL CPrArtPage::Print ()
{
	CProcess print;
	Form.Get ();
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11112.llf", tmp);
	}
	else
	{
		dName = "11112.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11112\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "a %.0lf %.0lf\n", A_bas.a_bas.a,
			                        A_bas.a_bas.a);
		fclose (fp);
		Command.Format ("dr70001 -name 11112 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11112";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CPrArtPage::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_List.StartPauseEnter ();
}

void CPrArtPage::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_List.EndPauseEnter ();
}

void CPrArtPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Enable (enable);
	PosControls.Enable (!enable);
}

void CPrArtPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

void CPrArtPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_List.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("RemoveKun", cfg_v) == TRUE)
    {
			RemoveKun = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_List.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_List.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_List.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("Aufschlag", cfg_v) == TRUE)
    {
			m_List.Aufschlag = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CPrArtPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_List.ListEdit ||
        Control == &m_List.ListComboBox ||
		Control == &m_List.SearchListCtrl.Edit)	
	{
		ListCopy ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CPrArtPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CPrArtPage::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_List.ListEdit.m_hWnd) ||
		IsWindow (m_List.ListComboBox) ||
		IsWindow (m_List.SearchListCtrl.Edit))
	{
		m_List.StopEnter ();
		m_List.StartEnter (m_List.EditCol, m_List.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
//		for (int i = 0; i < MAXLISTROWS && i < m_List.GetItemCount (); i ++)
		for (int i = 0; i < m_List.GetItemCount (); i ++)
		{
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = "";
			int cols = m_List.GetHeaderCtrl ()->GetItemCount ();
			for (int j = 0; j < cols; j ++)
			{
				if (Row != "")
				{
					Row += Separator;
				}
				CString Field = m_List.GetItemText (i, j);
				Field.TrimRight ();
				Row += Field; 
			}
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

BOOL CPrArtPage::TestDecValues (A_PR *a_pr)
{
	if (a_pr->pr_ek_euro > 9999.9999 ||
		a_pr->pr_vk_euro < -9999.9999)
	{
		return FALSE;
	}

	if (a_pr->pr_ek_euro > 9999.99 ||
		a_pr->pr_vk_euro < -9999.99)
	{
		return FALSE;
	}
    
	return TRUE;
}