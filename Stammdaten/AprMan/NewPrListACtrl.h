#pragma once
#include <vector>
#include "editlistctrl.h"
#include "mdn.h"
#include "fil.h"
#include "gr_zuord.h"
#include "Adr.h"
#include "a_pr.h"
#include "akt_krz.h"
#include "ChoiceMdn.h"
#include "Choicefil.h"
#include "ChoiceGrZuord.h"
#include "ChoiceA.h"
#include "A_bas.h"

#define MAXLISTROWS 30

class CNewPrListACtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
	BOOL RowDeleted;
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
	};

	enum LISTMODE
	{
		Large = 0,
		Compact = 1,
	};

	enum
	{
		LargeWidth = 120,
		CompactWidth = 40,
	};


	enum LISTPOS
	{
		POSA         = 1,
		POSDATE      = 2,
		POSPREK      = 3,
		POSPRVK      = 4,
		POSPREKN     = 5,
		POSPRVKN     = 6,
		POSPREKAB   =  7,
		POSPRVKAB   =  8,
	};

	BOOL ShowAbverk;
	void SetShowAbverk (BOOL ShowAbverk)
	{
		this->ShowAbverk = ShowAbverk;
	}

	BOOL GetShowAbverk ()
	{
		return ShowAbverk;
	}

	BOOL EnterBasis;
	void SetEnterBasis (BOOL EnterBasis)
	{
		this->EnterBasis = EnterBasis;
	}

	BOOL GetEnterBasis ()
	{
		return EnterBasis;
	}

	BOOL OldMode;
	int ListMode;
	int CompanyWidth;

	int PosA;
	int PosDate;
	int PosPrEk;
	int PosPrVk;
	int PosPrEkN;
	int PosPrVkN;
	int PosPrEkAb;
	int PosPrVkAb;
	int scale;

    int *Position[12];
	BOOL AChoiceStat;
	CChoiceA *ChoiceA;
	BOOL ModalChoiceA;
	CString ArtSelect;

	short m_Mdn;
	int FilGrCursor;
	int FilCursor;
	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	int oldsel;
	std::vector<BOOL> vSelect;
	CVector MdnGrCombo;
	CVector MdnCombo;
	CVector FilGrCombo;
	CVector FilCombo;
	CChoiceGrZuord *ChoiceMdnGr;
	BOOL ModalChoiceMdnGr;
	BOOL MdnGrChoiceStat;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	BOOL MdnChoiceStat;
	CChoiceGrZuord *ChoiceFilGr;
	BOOL ModalChoiceFilGr;
	BOOL FilGrChoiceStat;
	CChoiceFil *ChoiceFil;
	BOOL ModalChoiceFil;
	BOOL FilChoiceStat;
	CVector ListRows;

	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	FIL_CLASS Fil;
	GR_ZUORD_CLASS Gr_zuord;
	ADR_CLASS FilAdr;
	APR_CLASS A_pr;
	AKT_KRZ_CLASS Akt_krz;
	A_BAS_CLASS A_bas;
	CNewPrListACtrl(void);
	~CNewPrListACtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	void FillMdnGrCombo (CVector&);
	void FillMdnCombo (CVector&);
	void FillFilGrCombo (CVector&);
	void FillFilCombo (CVector&);
	void OnChoice ();
	void OnKey9 ();
    void ReadMdnGr ();
    void ReadMdn ();
    void ReadFilGr ();
    void ReadFil ();
    void ReadA ();
	void OnAChoice (CString &);
    void FillFilGrCombo (int row);
    void FillFilCombo (int row);
    void ReadFilCombo (int mdn);
    void ReadFilGrCombo (int mdn);
    void GetColValue (int row, int col, CString& Text);
    void TestAprIndex ();
    BOOL TestAprIndexM (int EditRow);
	void ScrollPositions (int pos);
	BOOL LastCol ();
	void Prepare ();
	BOOL TestDate ();
	BOOL TestDate (int Row);
    void ReadApr ();
	void SetListMode (int ListMode);
	void CalculateAbverk ();
};
