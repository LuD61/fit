// DialogTestDlg.h : Headerdatei
//

#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "CtrlGrid.h"
#include "ArtPrListCtrl.h"
#include "FillList.h"
#include "a_bas.h"
#include "gr_zuord.h"
#include "mdn.h"
#include "fil.h"
#include "adr.h"
#include "A_pr.h"
//#include "PGrProt.h"
#include "ChoiceA.h"
#include "ChoiceMdn.h"
#include "FormTab.h"
#include "mo_progcfg.h"
#include "Controls.h"
#include "Preise.h"
#include "PrProt.h"
#define IDC_ACHOICE 3000
#define IDC_MDNCHOICE 3001

// CPrArtPreise Dialogfeld
class CPrArtPage : public CDbPropertyPage
{
// Konstruktion
public:
	CPrArtPage(CWnd* pParent = NULL);	// Standardkonstruktor
	CPrArtPage(UINT nIDTemplate);	// Standardkonstruktor

	~CPrArtPage ();

// Dialogfelddaten
//	enum { IDD = IDD_DIALOGTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung
    virtual BOOL PreTranslateMessage(MSG* pMsg);


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public :
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	BOOL RemoveKun;
	int StartSize;

	CControls HeadControls;
	CControls PosControls;
	CControls ButtonControls;
	BOOL HideButtons;
	int RightListSpace;
	CVector DbRows;
	CVector ListRows;
	int AprCursor;
	int MdnGrCursor;
	int MdnCursor;
	int FilCursor;
	CWnd *Frame;
	CCtrlGrid CtrlGrid;
	CCtrlGrid AGrid;
	CNumEdit m_MdnName;
	CButton m_AChoice;
	CStatic m_LA;
	CNumEdit m_A;
	CEdit m_LA_bz1;
	CEdit m_A_bz1;
	CEdit m_A_bz2;
	CFillList FillList;
	CArtPrListCtrl m_List;
	CButton m_Cancel;
	CButton m_Save;
	CButton m_Delete;
	CButton m_Insert;
	int RighListSpace;
	CCtrlGrid ButtonGrid;
	GR_ZUORD_CLASS Gr_zuord;
	MDN_CLASS Mdn;
	FIL_CLASS Fil;
	ADR_CLASS MdnAdr;
	ADR_CLASS FilAdr;
	A_BAS_CLASS A_bas;
	APR_CLASS A_pr;
	CPrProt PrProt;
	CFormTab Form;
	CChoiceA *Choice;
	BOOL ModalChoice;
	BOOL CloseChoice;
	BOOL AChoiceStat;
	CString SearchA;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CFont Font;
	CFont lFont;
	CString PersName;
	CString Separator;
    CImageList image; 
	int CellHeight;

	virtual void OnSize (UINT, int, int);
	void OnAchoice ();
    void OnASelected ();
    void OnACanceled ();
	virtual BOOL Read ();
	virtual BOOL read ();
	virtual BOOL Write ();
	virtual BOOL Print ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	virtual BOOL StepBack ();
	virtual void OnCancel ();
	virtual void OnSave ();
	BOOL ReadList ();
    void FillMdnGrCombo ();
    void FillMdnCombo ();
    void FillFilCombo ();
	virtual BOOL DeleteAll ();
	virtual void OnDelete ();
	virtual void OnInsert ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult);
    void EnableHeadControls (BOOL enable);
    void WriteRow (int row);
    void DestroyRows(CVector &Rows);
    void DestroyRowsEx(CVector &Rows);
    void DeleteDbRows ();
    BOOL IsChanged (CPreise *pApr, CPreise *old_a_pr);
    BOOL InList (APR_CLASS& A_pr);
	void ReadCfg ();
    void ListCopy ();
	BOOL TestDecValues (A_PR *a_pr);
};
