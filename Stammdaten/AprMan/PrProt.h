#pragma once
#include "Pr_prot.h"
#include "a_pr.h"
#include "akt_krz.h"	// 070713
#include "akt_krza.h"	// 070713

class CPrProt
{
public:
	CString PersName;
    CString Programm;

	AKT_KRZ_CLASS *Akt_krz;
	AKT_KRZA_CLASS *Akt_krza;	
	APR_CLASS *A_pr;
	PR_PROT_CLASS Pr_prot;

	CPrProt(void);
	CPrProt(CString& PersName, CString& Programm, APR_CLASS *A_pr, AKT_KRZ_CLASS *Akt_krz,AKT_KRZA_CLASS *Akt_krza);
	~CPrProt(void);
	void Construct (CString& PersName, CString& Programm, APR_CLASS *A_pr, AKT_KRZ_CLASS *Akt_krz,AKT_KRZA_CLASS *Akt_krza);
	void Write (int typ, double pr_ek_alt, double pr_vk_alt);	// 070713
};
