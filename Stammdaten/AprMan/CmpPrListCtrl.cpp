#include "StdAfx.h"
#include "CmpPrListCtrl.h"
#include "StrFuncs.h"
#include "resource.h"

CCmpPrListCtrl::CCmpPrListCtrl(void)
{
	MdnCombo.Init ();
	FilCombo.Init ();
	ArtSelect = "";
	ChoiceA	 = NULL;
	ChoiceMdnGr = NULL;
	ChoiceMdn = NULL;
	ChoiceFilGr = NULL;
	ChoiceFil = NULL;
	ModalChoiceA = TRUE;
	ModalChoiceMdnGr = TRUE;
	ModalChoiceMdn = TRUE;
	ModalChoiceFilGr = TRUE;
	ModalChoiceFil = TRUE;
	PosA        = POSA;
	PosMdnGr    = POSMDNGR;
    PosMdn      = POSMDN;
	PosFilGr    = POSFILGR;
    PosFil      = POSFIL;
	PosPrEk     = POSPREK;
	PosPrVk     = POSPRVK;
	PosPrEkKalk = POSPREKKALK;
	PosPrVkKalk = POSPRVKKALK;
	PosSk       = POSSK;
	PosSp       = POSSP;

	Position[0] = &PosA;
	Position[1] = &PosMdnGr;
	Position[2] = &PosMdn;
	Position[3] = &PosFilGr;
	Position[4] = &PosFil;
	Position[5] = &PosPrEk;
	Position[6] = &PosPrVk;
	Position[7] = &PosPrEkKalk;
	Position[8] = &PosPrVkKalk;
	Position[9] = &PosSk;
	Position[10] = &PosSp;
	Position[11] = NULL;
	MaxComboEntries = 20;
	FilGrCursor = -1;
	FilCursor = -1;

	Aufschlag = LIST;
	Mode = STANDARD;
	m_Mdn = 0;
	sk_vollk = 0.0;
	spanne = 0.0;
	scale = 4;
	ListRows.Init ();
}

CCmpPrListCtrl::~CCmpPrListCtrl(void)
{
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
	}
	if (ChoiceFil != NULL)
	{
		delete ChoiceFil;
	}

	CString *c;
    MdnCombo.FirstPosition ();
	while ((c = (CString *) MdnCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnCombo.Init ();
    FilCombo.FirstPosition ();
	while ((c = (CString *) FilCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilCombo.Init ();
}

BEGIN_MESSAGE_MAP(CCmpPrListCtrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CCmpPrListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (PosPrEk, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		if (m_Mdn == 0)
		{
			StartEnter (PosA, 0);
		}
		else
		{
			StartEnter (PosFilGr, 0);
		}
	}
}

void CCmpPrListCtrl::StartEnter (int col, int row)
{
	BOOL RowChanged = FALSE;
//	int sRow;
	if (col == 0) col = 1;
	if (col == PosPrEkKalk) return;
	if (col == PosPrVkKalk) return;
	if (col == PosSk) return;
	if (col == PosSp) return;
/*
	StopEnter ();
	StartEnter (EditCol, EditRow);
*/
	CString Value;
	GetEnter ();
	GetColValue (row, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (col == PosFilGr || col == PosFil) return;
	}

	if (EditCol == PosFil)   ReadFil ();
	if (EditCol == PosFilGr) ReadFilGr ();

	if (col == PosA)
	{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
	}

	else if (col == PosMdnGr)
	{
		if (MaxComboEntries > 0 && MdnGrCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &MdnGrCombo);
			oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosMdn)
	{
		if (MaxComboEntries > 0 && MdnCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &MdnCombo);
			oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosFilGr)
	{
		FillFilGrCombo (row);
		if (MaxComboEntries > 0 && FilGrCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &FilGrCombo);
		    oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosFil)
	{
		FillFilCombo (row);
		if (MaxComboEntries > 0 && FilCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &FilCombo);
		    oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else
	{
		CEditListCtrl::StartEnter (col, row);
	}
}

void CCmpPrListCtrl::StopEnter ()
{
	CString Text;

	Text = GetItemText (EditRow, PosMdn);
	short OldMdn = _tstoi (Text.GetBuffer ());

	if (IsWindow (ListEdit.m_hWnd))
	{
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	short NewMdn = _tstoi (Text.GetBuffer ());
	if ((EditCol == PosMdn) && (NewMdn == 0 || NewMdn != OldMdn))
	{
		FillList.SetItemText (_T("0"), EditRow, PosFilGr);
		FillList.SetItemText (_T("0 Mandant"), EditRow, PosFil);
	}
	PerformListChangeHandler (EditRow, EditCol);
}

void CCmpPrListCtrl::SetSel (CString& Text)
{

   if (EditCol == PosPrEk ||
	   EditCol == PosPrVk)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CCmpPrListCtrl::FormatText (CString& Text)
{
    if (EditCol == PosPrEk)
	{
		DoubleToString (StrToDouble (Text), Text, scale);
	}
    else if (EditCol == PosPrVk)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
}

void CCmpPrListCtrl::NextRow ()
{
/*
	StopEnter ();
	StartEnter (EditCol, EditRow);
*/
	GetEnter ();
	CString PrEk = GetItemText (EditRow, PosPrEk);
	CString PrVk = GetItemText (EditRow, PosPrVk);
/*
	if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
	{
		return;
	}
*/
	int count = GetItemCount ();
	if (EditCol == PosFil)
	{
		ReadFil ();
	}	
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}	
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}

	else if (EditCol == PosA)
	{
		ReadA ();
	}

	SetEditText ();
	TestAprIndex ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		if (m_Mdn == 0)
		{
			EditCol = PosMdnGr;
		}
		else
		{
			EditCol = PosFilGr;
		}
	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFil)
		{
			EditRow --;
		}
		if (EditCol == PosFilGr)
		{
			EditRow --;
		}
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CCmpPrListCtrl::PriorRow ()
{
	BOOL Test = TRUE;
	if (EditRow <= 0)
	{
			return;
	}

/*
	StopEnter ();
	StartEnter (EditCol, EditRow);
*/
	GetEnter ();
	int count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		CString PrEk = GetItemText (EditRow, PosPrEk);
		CString PrVk = GetItemText (EditRow, PosPrVk);
/*
		if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
		{
	        DeleteItem (EditRow);
			Test = FALSE;
		}
*/
	}
//	else
	{
		if (EditRow <= 0)
		{
			return;
		}
		if (EditCol == PosFil)
		{
			ReadFil ();
		}
		else if (EditCol == PosFilGr)
		{
			ReadFilGr ();
		}
		else if (EditCol == PosMdn)
		{
			ReadMdn ();
		}
		else if (EditCol == PosMdnGr)
		{
			ReadMdnGr ();
		}
		else if (EditCol == PosA)
		{
			ReadA ();
		}
		if (Test)
		{
			TestAprIndex ();
		}
	}
	StopEnter ();
	EditRow --;
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFil)
		{
			EditRow ++;
		}
		if (EditCol == PosFilGr)
		{
			EditRow ++;
		}
	}
 
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CCmpPrListCtrl::LastCol ()
{
	int Col = PosPrVk;
	if (GetColumnWidth (PosPrVk) == 0)
	{
		Col = PosPrEk;
	}
	if (EditCol < Col) return FALSE;
	if (Mode == TERMIN && EditCol >= PosPrVk)
	{
		return TRUE;
	}

	return TRUE;
}

void CCmpPrListCtrl::OnReturn ()
{

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{
		CString PrEk = GetItemText (EditRow, PosPrEk);
		CString PrVk = GetItemText (EditRow, PosPrVk);
/*
		if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
		{
			EditCol --;
			return;
		}
*/
	}
	if (EditCol == PosFil)
	{
		ReadFil ();
	}
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}
	else if (EditCol == PosA)
	{
		ReadA ();
	}
	if (LastCol ())
	{
		TestAprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;

		if (EditRow == rowCount)
		{
			EditCol = PosA;
		}
		else
		{
			EditCol = PosPrEk;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
	}
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
	if (m_Mdn == 0)
	{
		if (EditCol == PosFilGr)
		{
			EditCol ++;
		}
		if (EditCol == PosFil)
		{
			EditCol ++;
		}
	}
	if (this->m_Mdn != 0)
	{
		if (EditCol == PosMdnGr) EditCol ++;
		if (EditCol == PosMdn) EditCol ++;
	}

	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CCmpPrListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	if (EditCol == PosFil)
	{
		ReadFil ();
	}
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}
	else if (EditCol == PosA)
	{
		ReadA ();
	}
	StopEnter ();
	EditCol ++;
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFilGr)
		{
			EditCol ++;
		}
		if (EditCol == PosFil)
		{
			EditCol ++;
		}
	}
	if (this->m_Mdn != 0)
	{
		if (EditCol == PosMdnGr) EditCol ++;
		if (EditCol == PosMdn) EditCol ++;
	}
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CCmpPrListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	if (EditCol == PosFil)
	{
		ReadFil ();
	}
	else if (EditCol == PosFilGr)
	{
		ReadFilGr ();
	}
	else if (EditCol == PosMdn)
	{
		ReadMdn ();
	}
	else if (EditCol == PosMdnGr)
	{
		ReadMdnGr ();
	}
	else if (EditCol == PosA)
	{
		ReadA ();
	}
	StopEnter ();
	EditCol --;
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	short m_Mdn = _tstoi (Value);
    if (m_Mdn == 0)
	{
		if (EditCol == PosFil)
		{
			EditCol --;
		}
		if (EditCol == PosFilGr)
		{
			EditCol --;
		}
	}
	if (this->m_Mdn != 0)
	{
		if (EditCol == PosMdn) EditCol --;
		if (EditCol == PosMdnGr) EditCol --;
	}
    StartEnter (EditCol, EditRow);
}

BOOL CCmpPrListCtrl::InsertRow ()
{
	CString PrEk = GetItemText (EditRow, PosPrEk);
	CString PrVk = GetItemText (EditRow, PosPrVk);
/*
	if ((GetItemCount () > 0) && 
		(StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
	{
		return FALSE;
	}
*/
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CString cA	    = _T("0 ");
	CString cMdnGr = _T("0 ");
	CString cMdn   = _T("0 ");
	if (m_Mdn != 0)
	{
		Mdn.mdn.mdn = m_Mdn;
		Mdn.dbreadfirst ();
		cMdnGr.Format (_T("%hd "), Mdn.mdn.mdn_gr);
		cMdn.Format (_T("%hd "), Mdn.mdn.mdn);
	}
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (cA.GetBuffer (), EditRow, PosA);
	FillList.SetItemText (cMdnGr.GetBuffer (), EditRow, PosMdnGr);
	FillList.SetItemText (cMdn.GetBuffer (), EditRow, PosMdn);
	FillList.SetItemText (_T("0"), EditRow, PosFilGr);
	FillList.SetItemText (_T("0"), EditRow, PosFil);
	CString EkFormat;
	EkFormat.Format (_T("%c.%dlf"), '%', scale);
	CString cPrEk;
	cPrEk.Format (EkFormat, 0.0);
	FillList.SetItemText (cPrEk.GetBuffer (), EditRow, PosPrEk);
	FillList.SetItemText (_T("0,00 "), EditRow, PosPrVk);
	EditCol = PosA;
	StartEnter (EditCol, EditRow);
	return TRUE;
}

BOOL CCmpPrListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	return CEditListCtrl::DeleteRow ();
}

BOOL CCmpPrListCtrl::AppendEmpty ()
{

	int rowCount = GetItemCount ();
	if (rowCount > 0)
	{
		CString PrEk = GetItemText (EditRow, PosPrEk);
		CString PrVk = GetItemText (EditRow, PosPrVk);
/*
		if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0))
		{
			return FALSE;
		}
*/
	}
	CString cA = _T("0 ");
	CString cMdnGr = _T("0 ");
	CString cMdn   = _T("0 ");
	if (m_Mdn != 0)
	{
		Mdn.mdn.mdn = m_Mdn;
		Mdn.dbreadfirst ();
		cMdnGr.Format (_T("%hd "), Mdn.mdn.mdn_gr);
		cMdn.Format (_T("%hd "), Mdn.mdn.mdn);
	}
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText (cA.GetBuffer (), rowCount, PosA);
	FillList.SetItemText (cMdnGr.GetBuffer (), rowCount, PosMdnGr);
	FillList.SetItemText (cMdn.GetBuffer (), rowCount, PosMdn);
	FillList.SetItemText (_T("0"),  rowCount, PosFilGr);
	FillList.SetItemText (_T("0"),   rowCount, PosFil);
	CString EkFormat;
	EkFormat.Format (_T("%c.%dlf"), '%', scale);
	CString cPrEk;
	cPrEk.Format (EkFormat, 0.0);
	FillList.SetItemText (cPrEk.GetBuffer (), rowCount, PosPrEk);
	FillList.SetItemText (_T("0,00 "), rowCount, PosPrVk);
	rowCount = GetItemCount ();
	return TRUE;
}

void CCmpPrListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CCmpPrListCtrl::FillMdnGrCombo (CVector& Values)
{
	CString *c;
    MdnGrCombo.FirstPosition ();
	while ((c = (CString *) MdnGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnGrCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MdnGrCombo.Add (c);
	}
}


void CCmpPrListCtrl::FillMdnCombo (CVector& Values)
{
	CString *c;
    MdnCombo.FirstPosition ();
	while ((c = (CString *) MdnCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MdnCombo.Add (c);
	}
}

void CCmpPrListCtrl::FillFilGrCombo (CVector& Values)
{
	CString *c;
    FilGrCombo.FirstPosition ();
	while ((c = (CString *) FilGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilGrCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		FilGrCombo.Add (c);
	}
}

void CCmpPrListCtrl::FillFilCombo (CVector& Values)
{
	CString *c;
    FilCombo.FirstPosition ();
	while ((c = (CString *) FilCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		FilCombo.Add (c);
	}
}


void CCmpPrListCtrl::FillFilCombo (int row)
{
	CString V;
	GetColValue (row, PosMdn, V);
	ReadFilCombo (_tstoi (V));
}

void CCmpPrListCtrl::ReadFilCombo (int mdn)
{
	Fil.fil.mdn = mdn;
	if (Fil.fil.mdn == 0) return;
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Mandant"));
	Values.Add (Value);
    Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		Fil.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Fil.fil.fil,
			                          FilAdr.adr.adr_krz);
		Values.Add (Value);
	}
	FillFilCombo (Values);
}


void CCmpPrListCtrl::RunItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
*/
}

void CCmpPrListCtrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CCmpPrListCtrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CCmpPrListCtrl::OnChoice ()
{
	if (EditCol == PosFil)
	{
		OnFilChoice (CString (_T("")));
	}
	if (EditCol == PosFilGr)
	{
		OnFilGrChoice (CString (_T("")));
	}
	else if (EditCol == PosMdn)
	{
		OnMdnChoice (CString (_T("")));
	}
	else if (EditCol == PosMdnGr)
	{
		OnMdnGrChoice (CString (_T("")));
	}
	else if (EditCol == PosA)
	{
		OnAChoice (CString (_T("")));
	}
}

void CCmpPrListCtrl::OnFilChoice (CString& Search)
{
	FilChoiceStat = TRUE;
	CString V;
	GetColValue (EditRow, PosMdn, V);
	Mdn.mdn.mdn = _tstoi (V);
	if (ChoiceFil != NULL && !ModalChoiceFil)
	{
		ChoiceFil->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceFil == NULL)
	{
		ChoiceFil = new CChoiceFil (this);
	    ChoiceFil->IsModal = ModalChoiceFil;
		ChoiceFil->m_Mdn = Mdn.mdn.mdn;
		ChoiceFil->CreateDlg ();
	}

    ChoiceFil->SetDbClass (&Fil);
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	ChoiceFil->m_Mdn = _tstoi (Value.GetBuffer ());
	ChoiceFil->SearchText = Search;
	if (ModalChoiceFil)
	{
			ChoiceFil->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceFil->MoveWindow (&rect);
		ChoiceFil->SetFocus ();
		return;
	}
    if (ChoiceFil->GetState ())
    {
		  CFilList *abl = ChoiceFil->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  FilChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		  memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		  Fil.fil.mdn = Mdn.mdn.mdn;
		  Fil.fil.fil = abl->fil;
		  if (Fil.dbreadfirst () == 0)
		  {
			  FilAdr.adr.adr = Fil.fil.adr;
			  FilAdr.dbreadfirst ();
		  }
		  CString Text;
		  Text.Format (_T("%hd"), Fil.fil.fil);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
		  CString KunName;
//		  KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
//		  FillList.SetItemText (KunName.GetBuffer (), EditRow, PosKunName);
    }
	else
	{
		 FilChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}

void CCmpPrListCtrl::OnFilGrChoice (CString& Search)
{
	CString V;
	GetColValue (EditRow, PosMdn, V);
	Mdn.mdn.mdn = _tstoi (V);
	FilGrChoiceStat = TRUE;
	if (ChoiceFilGr != NULL && !ModalChoiceFilGr)
	{
		ChoiceFilGr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceFilGr == NULL)
	{
		ChoiceFilGr = new CChoiceGrZuord (this);
	    ChoiceFilGr->IsModal = ModalChoiceFilGr;
	    ChoiceFilGr->m_Mdn = Mdn.mdn.mdn;
		ChoiceFilGr->CreateDlg ();
	}

    ChoiceFilGr->SetDbClass (&Fil);
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	ChoiceFilGr->m_Mdn = _tstoi (Value.GetBuffer ());
	ChoiceFilGr->SearchText = Search;
	if (ModalChoiceFilGr)
	{
			ChoiceFilGr->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceFil->MoveWindow (&rect);
		ChoiceFil->SetFocus ();
		return;
	}
    if (ChoiceFilGr->GetState ())
    {
		  CGrZuordList *abl = ChoiceFilGr->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  FilGrChoiceStat = FALSE;
			  return;
		  }
		  CString Text;
		  Text.Format (_T("%hd"), abl->gr);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
		 FilGrChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}


void CCmpPrListCtrl::OnMdnChoice (CString& Search)
{
	MdnChoiceStat = TRUE;
	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

    ChoiceMdn->SetDbClass (&Fil);
//	ChoiceFil->m_Mdn = m_Mdn;
	ChoiceMdn->SearchText = Search;
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  MdnChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  CString Text;
		  Text.Format (_T("%hd"), Mdn.mdn.mdn);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
//		  CString KunName;
//		  KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
//		  FillList.SetItemText (KunName.GetBuffer (), EditRow, PosKunName);
    }
	else
	{
		 MdnChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}

void CCmpPrListCtrl::OnMdnGrChoice (CString& Search)
{
	MdnGrChoiceStat = TRUE;
	if (ChoiceMdnGr != NULL && !ModalChoiceMdnGr)
	{
		ChoiceMdnGr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdnGr == NULL)
	{
		ChoiceMdnGr = new CChoiceGrZuord (this);
	    ChoiceMdnGr->IsModal = ModalChoiceMdnGr;
		ChoiceMdnGr->CreateDlg ();
	}

    ChoiceMdnGr->SetDbClass (&Mdn);
	ChoiceMdnGr->SearchText = Search;
	if (ModalChoiceMdnGr)
	{
			ChoiceMdnGr->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdnGr->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdnGr->MoveWindow (&rect);
		ChoiceMdnGr->SetFocus ();
		return;
	}
    if (ChoiceMdnGr->GetState ())
    {
		  CGrZuordList *abl = ChoiceMdnGr->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  MdnGrChoiceStat = FALSE;
			  return;
		  }
		  CString Text;
		  Text.Format (_T("%hd"), abl->gr);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
		 MdnGrChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}

void CCmpPrListCtrl::OnAChoice (CString& Search)
{
	AChoiceStat = TRUE;
	if (ChoiceA != NULL && !ModalChoiceA)
	{
		ChoiceA->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceA == NULL)
	{
		ChoiceA = new CChoiceA (this);
	    ChoiceA->IsModal = ModalChoiceA;
#ifndef ARTIKEL
	    ChoiceA->HideEnter = FALSE;
#endif
	    ChoiceA->HideFilter = FALSE;
		ChoiceA->IdArrDown = IDI_HARROWDOWN;
		ChoiceA->IdArrUp   = IDI_HARROWUP;
		ChoiceA->IdArrNo   = IDI_HARROWNO;
		ChoiceA->CreateDlg ();
	}

    ChoiceA->SetDbClass (&Mdn);
	ChoiceA->SearchText = Search;
	ChoiceA->Where = _T("");
	if (ArtSelect != _T(""))
	{
		ChoiceA->Where = ArtSelect;
	}
	if (ModalChoiceA)
	{
//			ChoiceA->FirstRead = TRUE;
			ChoiceA->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceA->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceA->MoveWindow (&rect);
		ChoiceA->SetFocus ();
		return;
	}
    if (ChoiceA->GetState ())
    {
		  CABasList *abl = ChoiceA->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  AChoiceStat = FALSE;
			  return;
		  }
		  CString Text;
		  Text.Format (_T("%.0lf"), abl->a);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
		 AChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}


void CCmpPrListCtrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}

void CCmpPrListCtrl::ReadA ()
{
	if (EditCol != PosA) return;
    memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
    CString Text;

	SearchListCtrl.Edit.GetWindowText (Text);
	if (!CStrFuncs::IsDecimal (Text))
	{
			OnAChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%.0lf"), CStrFuncs::StrToDouble (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!AChoiceStat)
			{
				EditCol --;
				return;
			}
	}
	A_bas.a_bas.a = CStrFuncs::StrToDouble (Text);
	if (A_bas.a_bas.a == 0) 
	{
		EditCol --;
		return;
	}
	if (A_bas.dbreadfirst () != 0)
	{
		MessageBox (_T("Artikel nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return;
	}
	Text.Format (_T("%.0lf  %s"), A_bas.a_bas.a, A_bas.a_bas.a_bz1);
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
			SearchListCtrl.Edit.SetWindowText (Text);
	}
}


void CCmpPrListCtrl::ReadMdnGr ()
{
	if (EditCol != PosMdnGr) return;
    memcpy (&Gr_zuord.gr_zuord, &gr_zuord_null, sizeof (GR_ZUORD));
    CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnMdnGrChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!MdnGrChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Gr_zuord.gr_zuord.mdn = 0;
	Gr_zuord.gr_zuord.gr = atoi (Text);
	if (Gr_zuord.gr_zuord.gr != 0)
	{
		if (Gr_zuord.dbreadfirst () != 0 && Gr_zuord.gr_zuord.gr != 0l)
		{
			MessageBox (_T("Mandantengruppe nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr, Gr_zuord.gr_zuord.gr_bz1);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
	else
	{
		Text = _T("0");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text = _T("0");
        SetItemText (EditRow, PosMdnGr, Text.GetBuffer ());
	}
}

void CCmpPrListCtrl::ReadMdn ()
{
	if (EditCol != PosMdn) return;
    memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
    CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnMdnChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!MdnChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Mdn.mdn.mdn = atoi (Text);
	if (Mdn.mdn.mdn != 0)
	{
		if (Mdn.dbreadfirst () == 0)
		{
			MdnAdr.adr.adr = Mdn.mdn.adr;
			MdnAdr.dbreadfirst ();
		}
		else if (Mdn.mdn.mdn != 0l)
		{
			MessageBox (_T("Mandant nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Mdn.mdn.mdn, MdnAdr.adr.adr_krz);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text.Format (_T("%hd"), Mdn.mdn.mdn_gr);
        SetItemText (EditRow, PosMdnGr, Text.GetBuffer ());
	}
	else
	{
		Text = _T("0  Zentrale");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
}

void CCmpPrListCtrl::ReadFilGr ()
{
	if (EditCol != PosFilGr) return;
	CString Value;
	GetColValue (EditRow, PosMdn, Value);
	if (_tstoi (Value) == 0) return;

    memcpy (&Gr_zuord.gr_zuord, &gr_zuord_null, sizeof (GR_ZUORD));
    CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnFilGrChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!FilGrChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Gr_zuord.gr_zuord.mdn = _tstoi (Value);
	Gr_zuord.gr_zuord.gr = atoi (Text);
	if (Gr_zuord.gr_zuord.gr != 0)
	{
		if (Gr_zuord.dbreadfirst () != 0 && Gr_zuord.gr_zuord.gr != 0l)
		{
			MessageBox (_T("Mandantengruppe nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr, Gr_zuord.gr_zuord.gr_bz1);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
	else
	{
		Text = _T("0");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text = _T("0");
        SetItemText (EditRow, PosFilGr, Text.GetBuffer ());
	}
}


void CCmpPrListCtrl::ReadFil ()
{
	if (EditCol != PosFil) return;
    memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
	CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		if (!IsWindow (ListComboBox.m_hWnd))
		{
			return;
		}
		int idx = ListComboBox.GetCurSel ();
		if (idx == -1) return;
		ListComboBox.GetLBText (idx, Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		int pos = 0;
		Text = Text.Tokenize (" ", pos);
	}
	else
	{
		SearchListCtrl.Edit.GetWindowText (Text);
		if (!CStrFuncs::IsDecimal (Text))
		{
			OnMdnChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%hd"), atoi (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!FilChoiceStat)
			{
				EditCol --;
				return;
			}
		}
	}
	Fil.fil.fil = atoi (Text);
	Text = GetItemText (EditRow, PosMdn);
    Fil.fil.mdn = atoi (Text);
//	if (Fil.fil.mdn == 0l) return;

    CString FilName;
	if (Fil.fil.fil != 0)
	{
		if (Fil.dbreadfirst () == 0)
		{
		  FilAdr.adr.adr = Fil.fil.adr;
		  FilAdr.dbreadfirst ();
		}
		else if (Fil.fil.fil != 0l)
		{
			MessageBox (_T("Filiale nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return;
		}
		Text.Format (_T("%hd  %s"), Fil.fil.fil, FilAdr.adr.adr_krz);
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
		Text.Format (_T("%hd"), Fil.fil.fil_gr);
        SetItemText (EditRow, PosFilGr, Text.GetBuffer ());
	}
	else 
	{
		Text = _T("0  Mandant");
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			SearchListCtrl.Edit.SetWindowText (Text);
		}
	}
}

void CCmpPrListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	if (col == PosMdn)
	{
		Text = cText.Tokenize (" ", pos);
	}
	else if (col == PosFil)
	{
		Text = cText.Tokenize (" ", pos);
	}
	else
	{
		Text = cText.Trim ();
	}
}

void CCmpPrListCtrl::TestAprIndex ()
{
	int Items = GetItemCount ();
	if (Items <= 1) return;

	CString Value;
	GetColValue (EditRow, PosA, Value);
	double sA = CStrFuncs::StrToDouble (Value);
	GetColValue (EditRow, PosMdnGr, Value);
	short sMdnGr = atoi (Value);
	GetColValue (EditRow, PosMdn, Value);
	short sMdn = atoi (Value);
	GetColValue (EditRow, PosFilGr, Value);
	short sFilGr = atoi (Value);
	GetColValue (EditRow, PosFil, Value);
	short sFil = atoi (Value);
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosA, Value);
		double dA = CStrFuncs::StrToDouble (Value);
		GetColValue (i, PosMdnGr, Value);
		short lMdnGr = atoi (Value);
		GetColValue (i, PosMdn, Value);
		short lMdn = atoi (Value);
		GetColValue (i, PosFilGr, Value);
		short lFilGr = atoi (Value);
		GetColValue (i, PosFil, Value);
		short lFil = atoi (Value);
		if (dA == sA && lFil == sFil && lFilGr == sFilGr &&
			lMdn == sMdn && lMdnGr == sMdnGr)
		{
			INT_PTR ret = MessageBox (_T("Ein Eintrag auf dieser Unternehmenseben existiert bereits")
				        _T("Eintrag l�schen ?"),
						NULL,
						MB_YESNO | MB_ICONQUESTION); 
            if (ret != IDYES) return;
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
			return;
		}

	}
}

BOOL CCmpPrListCtrl::TestAprIndexM (int EditRow)
{
	int Items = GetItemCount ();
	if (Items <= 1) return TRUE;

	GetEnter ();
	CString PrEk;
	GetColValue (EditRow, PosPrEk, PrEk);
	CString PrVk;
	GetColValue (EditRow, PosPrVk, PrVk);
	double pr_ek = CStrFuncs::StrToDouble (PrEk);
	double pr_vk = CStrFuncs::StrToDouble (PrVk);
	if (pr_ek == 0.0 && pr_vk == 0.0) return TRUE;

	CString Value;
	GetColValue (EditRow, PosA, Value);
	double sA = CStrFuncs::StrToDouble (Value);
	GetColValue (EditRow, PosMdnGr, Value);
	short sMdnGr = atoi (Value);
	GetColValue (EditRow, PosMdn, Value);
	short sMdn = atoi (Value);
	GetColValue (EditRow, PosFilGr, Value);
	short sFilGr = atoi (Value);
	GetColValue (EditRow, PosFil, Value);
	short sFil = atoi (Value);
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;

		GetColValue (i, PosPrEk, PrEk);
		GetColValue (i, PosPrVk, PrVk);
		double pr_ek = CStrFuncs::StrToDouble (PrEk);
		double pr_vk = CStrFuncs::StrToDouble (PrVk);
		if (pr_ek == 0.0 && pr_vk == 0.0) continue;

		GetColValue (i, PosA, Value);
		double dA = CStrFuncs::StrToDouble (Value);
		GetColValue (i, PosMdnGr, Value);
		short lMdnGr = atoi (Value);
		GetColValue (i, PosMdn, Value);
		short lMdn = atoi (Value);
		GetColValue (i, PosFilGr, Value);
		short lFilGr = atoi (Value);
		GetColValue (i, PosFil, Value);
		short lFil = atoi (Value);
		if (dA == sA && lFil == sFil && lFilGr == sFilGr &&
			lMdn == sMdn && lMdnGr == sMdnGr)
		{
			return FALSE;
		}

	}
	return TRUE;
}

void CCmpPrListCtrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}

void CCmpPrListCtrl::ReadFilGrCombo (int mdn)
{

	Gr_zuord.gr_zuord.mdn = mdn;
	if (Gr_zuord.gr_zuord.mdn == 0) return;

	CVector Values;
	Values.Init ();
	CString *V = new CString ();
	V->Format (_T("0"));
	Values.Add (V);
    Gr_zuord.sqlopen (FilGrCursor);
	while (Gr_zuord.sqlfetch (FilGrCursor) == 0)
	{
		Gr_zuord.dbreadfirst ();
		V = new CString ();
		V->Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr,
		                          Gr_zuord.gr_zuord.gr_bz1);
		Values.Add (V);
	}
	FillFilGrCombo (Values);
}

void CCmpPrListCtrl::FillFilGrCombo (int row)
{

	CString Value;
	GetColValue (row, PosMdn, Value);
	ReadFilGrCombo (_tstoi (Value));
}

void CCmpPrListCtrl::Prepare ()
{
	Gr_zuord.sqlin  ((short *) &Gr_zuord.gr_zuord.mdn, SQLSHORT, 0);
	Gr_zuord.sqlout ((short *) &Gr_zuord.gr_zuord.gr, SQLSHORT, 0);
	Gr_zuord.sqlout ((char *) Gr_zuord.gr_zuord.gr_bz1, SQLCHAR, sizeof (Gr_zuord.gr_zuord.gr_bz1));
    FilGrCursor = Gr_zuord.sqlcursor (_T("select gr, gr_bz1 from gr_zuord ")
 									  _T("where mdn = ? ")
									  _T("and gr > 0 ")
								      _T("order by gr"));

	Fil.sqlin ((short *) &Fil.fil.mdn, SQLSHORT, 0);
	Fil.sqlout ((short *) &Fil.fil.fil, SQLSHORT, 0);
	FilAdr.sqlout ((char *) &FilAdr.adr.adr_krz, SQLCHAR, sizeof (FilAdr.adr.adr_krz));
    FilCursor = Fil.sqlcursor (_T("select fil.fil, adr.adr_krz from fil, adr ")
		                       _T("where fil.mdn = ? ")
							   _T("and fil.fil > 0 ")
							   _T("and adr.adr = fil.adr ")
							   _T("order by fil.fil"));

}

