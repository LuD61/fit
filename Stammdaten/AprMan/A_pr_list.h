#ifndef _A_PR_LIST_DEF
#define _A_PR_LIST_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_PR_LIST {
   short          hwg;
   short          wg;
   long           ag;
   short          teil_smt;
   short          a_typ;
   short          cfil;
   short          cfil_gr;
   short          cmdn;
   short          cmdn_gr;
   double         a;
   short          fil;
   short          fil_gr;
   short          mdn;
   short          mdn_gr;
   double         pr_ek;
   double         pr_vk;
   double         pr_ek_sa;
   double         pr_vk_sa;
   TCHAR          lad_akv_sa[2];
   DATE_STRUCT    lad_akt_von;
   DATE_STRUCT    lad_akt_bis;
};
extern struct A_PR_LIST a_pr_list, a_pr_list_null;

#line 8 "a_pr_list.rh"

class A_PR_LIST_CLASS : public DB_CLASS 
{
       private :
               int ins_null_cursor;
               void prepare (void);
       public :
               A_PR_LIST a_pr_list;
               A_PR_LIST_CLASS () : DB_CLASS ()
               {
			ins_null_cursor = -1;		
               }
	       	
               ~A_PR_LIST_CLASS ()
	       {
			if (ins_null_cursor == -1)
                        {
			     sqlclose (ins_null_cursor);
                        }			 
	       }	
	       int dbinsert_nulls ();
};
#endif
