#include "StdAfx.h"
#include "NewPrListACtrl.h"
#include "StrFuncs.h"
#include "resource.h"

CNewPrListACtrl::CNewPrListACtrl(void)
{
	MdnCombo.Init ();
	FilCombo.Init ();
	ArtSelect = "";
	ChoiceMdnGr = NULL;
	ChoiceMdn = NULL;
	ChoiceFilGr = NULL;
	ChoiceFil = NULL;
	ChoiceA	 = NULL;
	ModalChoiceMdnGr = TRUE;
	ModalChoiceMdn = TRUE;
	ModalChoiceFilGr = TRUE;
	ModalChoiceFil = TRUE;
	ModalChoiceA = TRUE;
	PosA		= POSA;
    PosDate     = POSDATE;
	PosPrEk     = POSPREK;
	PosPrVk     = POSPRVK;
	PosPrEkN    = POSPREKN;
	PosPrVkN    = POSPRVKN;
	PosPrEkAb   = POSPREKAB;
	PosPrVkAb   = POSPRVKAB;

	Position[0] = &PosA;
//	Position[1] = &PosMdnGr;
//	Position[2] = &PosMdn;
//	Position[3] = &PosFilGr;
//	Position[4] = &PosFil;
	Position[1] = &PosPrEk;
	Position[2] = &PosPrVk;
	Position[3] = &PosPrEkN;
	Position[4] = &PosPrVkN;
	Position[5] = &PosPrEkAb;
	Position[6] = &PosPrVkAb;
	Position[7] = NULL;
	MaxComboEntries = 20;
	FilGrCursor = -1;
	FilCursor = -1;

	Aufschlag = LIST;
	Mode = STANDARD;
	m_Mdn = 0;
	ListRows.Init ();
	ListMode = Large;
	OldMode = TRUE;
	ShowAbverk = FALSE;
	EnterBasis = FALSE;
	scale = 4;
}

CNewPrListACtrl::~CNewPrListACtrl(void)
{
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
	}
	if (ChoiceFil != NULL)
	{
		delete ChoiceFil;
	}

	CString *c;
    MdnGrCombo.FirstPosition ();
	while ((c = (CString *) MdnGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnGrCombo.Init ();
    MdnCombo.FirstPosition ();
	while ((c = (CString *) MdnCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnCombo.Init ();
    FilGrCombo.FirstPosition ();
	while ((c = (CString *) FilGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilGrCombo.Init ();
    FilCombo.FirstPosition ();
	while ((c = (CString *) FilCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilCombo.Init ();
}

BEGIN_MESSAGE_MAP(CNewPrListACtrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CNewPrListACtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (EditCol, EditRow);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		if (m_Mdn == 0)
		{
			StartEnter (PosA, 0);
		}
		else
		{
			StartEnter (PosA, 0);
		}
	}
}

void CNewPrListACtrl::StartEnter (int col, int row)
{
	BOOL RowChanged = FALSE;
//	int sRow;
	if (col == 0) col = 1;
	CString Value;

	if (col == PosPrEk) return; 
	if (col == PosPrVk) return; 
	GetEnter ();

	if (IsDisplayOnly (col)) 
	{
		return;
	}

	if (col == PosA)
	{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
	}
	else if (col == PosDate)
	{
			if (IsWindow (ListDate.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterDate (col, row);
			CTime date = CTime::GetCurrentTime ();
	}
	else
	{
		CEditListCtrl::StartEnter (col, row);
	}
}

void CNewPrListACtrl::ReadApr ()
{
	CString cPrEk;
	CString cPrVk;
	CString cPrEkN;
	CString cPrVkN;

	if (EditCol != PosA) 
	{
		return; 
	}

	GetEnter ();
	CString cA = GetItemText (EditRow, PosA);

	A_pr.a_pr.mdn_gr = 0;   //ZuTun Wert aus Kopf holen
	A_pr.a_pr.mdn    = 1;   //ZuTun Wert aus Kopf holen
	A_pr.a_pr.fil_gr = 0;   //ZuTun Wert aus Kopf holen
	A_pr.a_pr.fil    = 0;   //ZuTun Wert aus Kopf holen
	A_pr.a_pr.a    = _tstoi (cA.GetBuffer ());

	if (A_pr.dbreadfirst () == 0)
	{
		cPrEk.Format (_T("%.2lf"), A_pr.a_pr.pr_ek_euro);
		cPrVk.Format (_T("%.2lf"), A_pr.a_pr.pr_vk_euro);
		SetItemText (EditRow, PosPrEk, cPrEk.GetBuffer ());
		SetItemText (EditRow, PosPrVk, cPrVk.GetBuffer ());
	}
	else
	{
		SetItemText (EditRow, PosPrEk, _T("0,00"));
		SetItemText (EditRow, PosPrVk, _T("0,00"));
	}

	Akt_krz.akt_krz.a      = A_pr.a_pr.a;
	Akt_krz.akt_krz.mdn_gr = A_pr.a_pr.mdn_gr;
	Akt_krz.akt_krz.mdn    = A_pr.a_pr.mdn;
	Akt_krz.akt_krz.fil_gr = A_pr.a_pr.fil_gr;
	Akt_krz.akt_krz.fil    = A_pr.a_pr.fil;
	CString Date = GetItemText (EditRow, PosDate);
	Akt_krz.ToDbDate (Date, &Akt_krz.akt_krz.lad_akt_von);
	strcpy ((LPSTR) Akt_krz.akt_krz.lad_akv_sa, "9");
	if (Akt_krz.dbreadfirst () == 0)
	{
		cPrEkN.Format (_T("%.2lf"), Akt_krz.akt_krz.pr_ek_sa_euro);
		cPrVkN.Format (_T("%.2lf"), Akt_krz.akt_krz.pr_vk_sa_euro);
	}
	else
	{
		cPrEkN.Format (_T("%.2lf"), 0.0);
		cPrVkN.Format (_T("%.2lf"), 0.0);
	}
//	SetItemText (EditRow, PosPrEkN, cPrEkN.GetBuffer ());
//	SetItemText (EditRow, PosPrVkN, cPrVkN.GetBuffer ());
}

void CNewPrListACtrl::StopEnter ()
{
	CString Text;

	Text = " test";
	short OldMdn = _tstoi (Text.GetBuffer ());
//	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	else if (IsWindow (ListDate.m_hWnd))
	{
		ListDate.GetWindowText (Text);
//		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListDate.DestroyWindow ();
	}
	CalculateAbverk ();
}

void CNewPrListACtrl::SetSel (CString& Text)
{

   if (EditCol == PosPrEkN ||
	   EditCol == PosPrVkN ||
	   EditCol == PosPrEkAb ||
	   EditCol == PosPrVkAb ||
	   EditCol == PosDate)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CNewPrListACtrl::FormatText (CString& Text)
{
    if (EditCol == PosPrEk)
	{
		DoubleToString (StrToDouble (Text), Text, scale);
	}
    else if (EditCol == PosPrVk)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
}

void CNewPrListACtrl::NextRow ()
{
/*
	StopEnter ();
	StartEnter (EditCol, EditRow);
*/
	if (!TestDate ()) return;
	RowDeleted = FALSE;
	GetEnter ();
	CString PrEk = GetItemText (EditRow, PosPrEk);
	CString PrVk = GetItemText (EditRow, PosPrVk);
	CString PrEkN = GetItemText (EditRow, PosPrEkN);
	CString PrVkN = GetItemText (EditRow, PosPrVkN);
	if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0) && 
			(StrToDouble (PrEkN) == 0.0) &&
			(StrToDouble (PrVkN) == 0.0))
	{
		if (OldMode)
		{
			return;
		}
	}

	if ( !TestDate ())
	{
		return;
	}

	int count = GetItemCount ();

	if (EditCol == PosA)
	{
		ReadA ();
	}

	SetEditText ();
    StopEnter ();
	TestAprIndex ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
		PrEkN = GetItemText (EditRow, PosPrEkN);
		PrVkN = GetItemText (EditRow, PosPrVkN);
		if ((StrToDouble (PrEkN) == 0.0) &&
			(StrToDouble (PrVkN) == 0.0))
		{
//			SetItemText (EditRow, PosDate, _T(""));
		}
		if (!RowDeleted)
		{
			EditRow ++;
		}
		if (m_Mdn == 0)
		{
			EditCol = PosA;
		}
		else
		{
			EditCol = PosA;
		}
	}
	else
	{
		PrEkN = GetItemText (EditRow, PosPrEkN);
		PrVkN = GetItemText (EditRow, PosPrVkN);
		if ((StrToDouble (PrEkN) == 0.0) &&
			(StrToDouble (PrVkN) == 0.0))
		{
//			SetItemText (EditRow, PosDate, _T(""));
		}
		if (!RowDeleted)
		{
			EditRow ++;
		}
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CNewPrListACtrl::PriorRow ()
{
	if (!TestDate ()) return;
	RowDeleted = FALSE;
	BOOL Test = TRUE;
	if (EditRow <= 0)
	{
			return;
	}

/*
	StopEnter ();
	StartEnter (EditCol, EditRow);
*/
	GetEnter ();
	int count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		CString PrEkN = GetItemText (EditRow, PosPrEkN);
		CString PrVkN = GetItemText (EditRow, PosPrVkN);
		if (OldMode)
		{
			if ((StrToDouble (PrEkN) == 0.0) &&
				(StrToDouble (PrVkN) == 0.0))
			{
				DeleteItem (EditRow);
				Test = FALSE;
			}
		}
	}
//	else
	{
		StopEnter ();
		if (EditRow <= 0)
		{
			return;
		}
		if (EditCol == PosA)
		{
			ReadA ();
		}
		if (Test)
		{
			TestAprIndex ();
		}
	}
	CString PrEkN = GetItemText (EditRow, PosPrEkN);
	CString PrVkN = GetItemText (EditRow, PosPrVkN);
	if ((StrToDouble (PrEkN) == 0.0) &&
		(StrToDouble (PrVkN) == 0.0))
	{
//		SetItemText (EditRow, PosDate, _T(""));
	}
	if (!RowDeleted)
	{
		EditRow --;
	}
 
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CNewPrListACtrl::LastCol ()
{
	if (EditCol < PosPrVkN) return FALSE;
	if (EnterBasis && (EditCol < PosPrVkAb)) return FALSE;
	return TRUE;
}

void CNewPrListACtrl::OnReturn ()
{

	if (!TestDate ()) return;
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{
		CString PrEk = GetItemText (EditRow, PosPrEk);
		CString PrVk = GetItemText (EditRow, PosPrVk);
		CString PrEkN = GetItemText (EditRow, PosPrEkN);
		CString PrVkN = GetItemText (EditRow, PosPrVkN);
		if ((StrToDouble (PrEk) == 0.0) &&
			(StrToDouble (PrVk) == 0.0) && 
			(StrToDouble (PrEkN) == 0.0) &&
			(StrToDouble (PrVkN) == 0.0))
		{
			EditCol --;
			return;
		}
	}
	if (LastCol ())
	{
	    StopEnter ();
		TestAprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
		CString PrEkN = GetItemText (EditRow, PosPrEkN);
		CString PrVkN = GetItemText (EditRow, PosPrVkN);
		if ((StrToDouble (PrEkN) == 0.0) &&
			(StrToDouble (PrVkN) == 0.0))
		{
//			SetItemText (EditRow, PosDate, _T(""));
		}
		if (!RowDeleted)
		{
			EditRow ++;
		}

		if (EditRow == rowCount)
		{
			if (m_Mdn == 0)
			{
				EditCol = PosA;
			}
			else
			{
				EditCol = PosA;
			}
		}
		else
		{
			EditCol = PosPrEkN;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
	}
	if (EditCol == PosPrEk)
	{
		EditCol ++;
	}
	if (EditCol == PosPrVk)
	{
		EditCol ++;
	}
	if (IsDisplayOnly (EditCol))
	{
		OnReturn ();
		return;
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CNewPrListACtrl::NextCol ()
{
	if (!TestDate ()) return;
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	if (EditCol == PosPrEk)
	{
		EditCol ++;
	}
	if (EditCol == PosPrVk)
	{
		EditCol ++;
	}
	if (IsDisplayOnly (EditCol))
	{
		NextCol ();
		return;
	}

	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CNewPrListACtrl::PriorCol ()
{
	if (!TestDate ()) return;
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	StopEnter ();
	EditCol --;
	if (EditCol == PosPrVk)
	{
		EditCol --;
	}
	if (EditCol == PosPrEk)
	{
		EditCol --;
	}
	if (IsDisplayOnly (EditCol))
	{
		PriorCol ();
		return;
	}
    StartEnter (EditCol, EditRow);
}

BOOL CNewPrListACtrl::InsertRow ()
{
	CString PrEkN = GetItemText (EditRow, PosPrEk);
	CString PrVkN = GetItemText (EditRow, PosPrVk);
	if (OldMode && (GetItemCount () > 0) && 
		(StrToDouble (PrEkN) == 0.0) &&
			(StrToDouble (PrVkN) == 0.0))
	{
		return FALSE;
	}
	StopEnter ();
	if ((StrToDouble (PrEkN) == 0.0) &&
		(StrToDouble (PrVkN) == 0.0))
	{
//		SetItemText (EditRow, PosDate, _T(""));
	}
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CString cA	    = _T("0 ");
	CString cMdnGr = _T("0 ");
	CString cMdn   = _T("0 ");
	if (m_Mdn != 0)
	{
		Mdn.mdn.mdn = m_Mdn;
		Mdn.dbreadfirst ();
		cMdnGr.Format (_T("%hd "), Mdn.mdn.mdn_gr);
		cMdn.Format (_T("%hd "), Mdn.mdn.mdn);
	}
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (cA.GetBuffer (), EditRow, PosA);
	CString EkFormat;
	EkFormat.Format (_T("%c.%dlf"), '%', scale);
	CString cPrEk;
	cPrEk.Format (EkFormat, 0.0);
	FillList.SetItemText (cPrEk.GetBuffer (), EditRow, PosPrEk);
	FillList.SetItemText (_T("0,00 "), EditRow, PosPrVk);
	FillList.SetItemText (cPrEk.GetBuffer (), EditRow, PosPrEkN);
	FillList.SetItemText (_T("0,00 "), EditRow, PosPrVkN);
	EditCol = PosA;
	StartEnter (EditCol, EditRow);
	return TRUE;
}

BOOL CNewPrListACtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	CString PrEk = GetItemText (EditRow, PosPrEk);
	CString PrVk = GetItemText (EditRow, PosPrVk);
	if (OldMode)
	{
		return CEditListCtrl::DeleteRow ();
	}
	else if ((StrToDouble (PrEk) == 0.0) && 
		(StrToDouble (PrVk) == 0.0))
	{
		return CEditListCtrl::DeleteRow ();
	}
	else
	{
		StopEnter ();
		SetItemText (EditRow, PosDate, _T(""));
		SetItemText (EditRow, PosPrEkN, _T("0,00"));
		SetItemText (EditRow, PosPrVkN, _T("0,00"));
		StartEnter (EditCol, EditRow);
	}
	return TRUE;
}

BOOL CNewPrListACtrl::AppendEmpty ()
{
	CString PrEkN;
	CString PrVkN;
	int rowCount = GetItemCount ();
	if (rowCount > 0)
	{
		PrEkN = GetItemText (EditRow, PosPrEkN);
		PrVkN = GetItemText (EditRow, PosPrVkN);
		if (OldMode && (StrToDouble (PrEkN) == 0.0) &&
					   (StrToDouble (PrVkN) == 0.0))
		{
			return FALSE;
		}
	}
	if ((StrToDouble (PrEkN) == 0.0) &&
		(StrToDouble (PrVkN) == 0.0))
	{
//		SetItemText (EditRow, PosDate, _T(""));
	}
	CString cA	    = _T("0 ");
	CString cMdnGr = _T("0 ");
	CString cMdn   = _T("0 ");
	if (m_Mdn != 0)
	{
		Mdn.mdn.mdn = m_Mdn;
		Mdn.dbreadfirst ();
		cMdnGr.Format (_T("%hd "), Mdn.mdn.mdn_gr);
		cMdn.Format (_T("%hd "), Mdn.mdn.mdn);
	}
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText (cA.GetBuffer (), rowCount, PosA);
	CString EkFormat;
	EkFormat.Format (_T("%c.%dlf"), '%', scale);
	CString cPrEk;
	cPrEk.Format (EkFormat, 0.0);
	FillList.SetItemText (cPrEk.GetBuffer (), rowCount, PosPrEk);
	FillList.SetItemText (_T("0,00 "), rowCount, PosPrVk);
	FillList.SetItemText (cPrEk.GetBuffer (), rowCount, PosPrEkN);
	FillList.SetItemText (_T("0,00 "), rowCount, PosPrVkN);
	CString cDate;
	CStrFuncs::SysDate (cDate);
	FillList.SetItemText (cDate.GetBuffer (), rowCount, PosDate);
	rowCount = GetItemCount ();
	return TRUE;
}

void CNewPrListACtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CNewPrListACtrl::FillMdnGrCombo (CVector& Values)
{
	CString *c;
    MdnGrCombo.FirstPosition ();
	while ((c = (CString *) MdnGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnGrCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MdnGrCombo.Add (c);
	}
}


void CNewPrListACtrl::FillMdnCombo (CVector& Values)
{
	CString *c;
    MdnCombo.FirstPosition ();
	while ((c = (CString *) MdnCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MdnCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MdnCombo.Add (c);
	}
}

void CNewPrListACtrl::FillFilGrCombo (CVector& Values)
{
	CString *c;
    FilGrCombo.FirstPosition ();
	while ((c = (CString *) FilGrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilGrCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		FilGrCombo.Add (c);
	}
}

void CNewPrListACtrl::FillFilCombo (CVector& Values)
{
	CString *c;
    FilCombo.FirstPosition ();
	while ((c = (CString *) FilCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	FilCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		FilCombo.Add (c);
	}
}

void CNewPrListACtrl::RunItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
*/
}

void CNewPrListACtrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CNewPrListACtrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CNewPrListACtrl::OnChoice ()
{
	if (EditCol == PosA)
	{
		OnAChoice (CString (_T("")));
	}

}


void CNewPrListACtrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}


void CNewPrListACtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	Text = cText.Trim ();
}

void CNewPrListACtrl::TestAprIndex ()
{
	RowDeleted = FALSE;
	int Items = GetItemCount ();
	if (Items <= 1) return;
	CString Value;
	GetColValue (EditRow, PosA, Value);
	double sA = CStrFuncs::StrToDouble (Value);

	Value =  GetItemText (EditRow, PosPrEkN);
	double sPrEkN = CStrFuncs::StrToDouble (Value);
	Value =  GetItemText (EditRow, PosPrVkN);
	double sPrVkN = CStrFuncs::StrToDouble (Value);
	GetColValue (EditRow, PosDate, Value);
	CString sDate = Value;
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosA, Value);
		double dA = CStrFuncs::StrToDouble (Value);
		GetColValue (i, PosDate, Value);
		CString lDate = Value;
		if (dA == sA && 
			lDate == sDate)
		{
			INT_PTR ret = MessageBox (_T("Ein Eintrag auf dieser Unternehmenseben existiert bereits")
				        _T("Eintrag l�schen ?"),
						NULL,
						MB_YESNO | MB_ICONQUESTION); 
            if (ret != IDYES) return;
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
			return;
		}
		GetColValue (i, PosPrEkN, Value);
		Value =  GetItemText (i, PosPrEkN);
		double lPrEkN = CStrFuncs::StrToDouble (Value);
		Value =  GetItemText (i, PosPrVkN);
		double lPrVkN = CStrFuncs::StrToDouble (Value);

		if (dA == sA) 
			
		{
            if (sPrEkN == 0.0 && sPrVkN == 0.0)
			{
				DeleteItem (EditRow);
				RowDeleted = TRUE;
				InvalidateRect (NULL);
				if (EditRow == GetItemCount ()) EditRow --;
				return;
			}
            else if (lPrEkN == 0.0 && lPrVkN == 0.0)
			{
				DeleteItem (i);
				RowDeleted = TRUE;
				InvalidateRect (NULL);
				if (i < EditRow) EditRow --;
				return;
			}
		}
	}
}

BOOL CNewPrListACtrl::TestAprIndexM (int EditRow)
{
	int Items = GetItemCount ();
	if (Items <= 1) return TRUE;

	GetEnter ();
	CString PrEk;
	GetColValue (EditRow, PosPrEkN, PrEk);
	CString PrVk;
	GetColValue (EditRow, PosPrVkN, PrVk);
	double pr_ek = CStrFuncs::StrToDouble (PrEk);
	double pr_vk = CStrFuncs::StrToDouble (PrVk);
	if (pr_ek == 0.0 && pr_vk == 0.0) return TRUE;

	CString Value;
	GetColValue (EditRow, PosA, Value);
	double sA = CStrFuncs::StrToDouble (Value);

	GetColValue (EditRow, PosDate, Value);
	CString sDate = Value;
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;

		GetColValue (i, PosPrEkN, PrEk);
		GetColValue (i, PosPrVkN, PrVk);
		double pr_ek = CStrFuncs::StrToDouble (PrEk);
		double pr_vk = CStrFuncs::StrToDouble (PrVk);
		if (pr_ek == 0.0 && pr_vk == 0.0) continue;

		GetColValue (i, PosA, Value);
		double dA = CStrFuncs::StrToDouble (Value);
		GetColValue (i, PosDate, Value);
		CString lDate = Value;
		if (dA == sA && 
			lDate == sDate)
		{
			return FALSE;
		}

	}
	return TRUE;
}

void CNewPrListACtrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}

void CNewPrListACtrl::ReadFilGrCombo (int mdn)
{

	Gr_zuord.gr_zuord.mdn = mdn;
	if (Gr_zuord.gr_zuord.mdn == 0) return;

	CVector Values;
	Values.Init ();
	CString *V = new CString ();
	V->Format (_T("0"));
	Values.Add (V);
    Gr_zuord.sqlopen (FilGrCursor);
	while (Gr_zuord.sqlfetch (FilGrCursor) == 0)
	{
		Gr_zuord.dbreadfirst ();
		V = new CString ();
		V->Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr,
		                          Gr_zuord.gr_zuord.gr_bz1);
		Values.Add (V);
	}
	FillFilGrCombo (Values);
}

void CNewPrListACtrl::FillFilGrCombo (int row)
{
}

void CNewPrListACtrl::ReadFilCombo (int mdn)
{
}


void CNewPrListACtrl::FillFilCombo (int row)
{
}


void CNewPrListACtrl::Prepare ()
{
	Gr_zuord.sqlin  ((short *) &Gr_zuord.gr_zuord.mdn, SQLSHORT, 0);
	Gr_zuord.sqlout ((short *) &Gr_zuord.gr_zuord.gr, SQLSHORT, 0);
	Gr_zuord.sqlout ((char *) Gr_zuord.gr_zuord.gr_bz1, SQLCHAR, sizeof (Gr_zuord.gr_zuord.gr_bz1));
    FilGrCursor = Gr_zuord.sqlcursor (_T("select gr, gr_bz1 from gr_zuord ")
 									  _T("where mdn = ? ")
									  _T("and gr > 0 ")
								      _T("order by gr"));

	Fil.sqlin ((short *) &Fil.fil.mdn, SQLSHORT, 0);
	Fil.sqlout ((short *) &Fil.fil.fil, SQLSHORT, 0);
	FilAdr.sqlout ((char *) &FilAdr.adr.adr_krz, SQLCHAR, sizeof (FilAdr.adr.adr_krz));
    FilCursor = Fil.sqlcursor (_T("select fil.fil, adr.adr_krz from fil, adr ")
		                       _T("where fil.mdn = ? ")
							   _T("and fil.fil > 0 ")
							   _T("and adr.adr = fil.adr ")
							   _T("order by fil.fil"));

}

BOOL CNewPrListACtrl::TestDate ()
{
	if (EditCol != PosDate)
	{
		return TRUE;
	}
	if (!IsWindow (ListDate.m_hWnd))
	{
		return TRUE;
	}

	CTime Date;

	ListDate.GetTime (Date);
	__time64_t date = Date.GetTime () / (24 * 3600);
	int dYear  = Date.GetYear ();
	int dMonth = Date.GetMonth ();
	int dDay   = Date.GetDay ();

	CString cSysDate; 
	CStrFuncs::SysDate (cSysDate);  
 	int day = _tstoi (cSysDate.Left (2).GetBuffer ());
	int month = _tstoi (cSysDate.Mid (3, 2).GetBuffer ());
	int year = _tstoi (cSysDate.Right (4).GetBuffer ());
	if (day == 0 || month == 0)
	{
		return TRUE;
	}
	CTime SysDate (year, month, day, 0, 0, 0);
   
//	CTime SysDate = CTime::GetCurrentTime ();

	__time64_t sysdate = SysDate.GetTime () / (24 * 3600);
	int sYear  = SysDate.GetYear ();
	int sMonth = SysDate.GetMonth ();
	int sDay   = SysDate.GetDay ();
       
	if (date < sysdate)
	{
		AfxMessageBox (_T("Das Datum ist kleiner als das Tagesdatum"),MB_OK | MB_ICONERROR, 0);
		return FALSE;
	}
	return TRUE;
}

BOOL CNewPrListACtrl::TestDate (int Row)
{

	CString cDate;
	GetColValue (Row, PosDate, cDate);
 	int day = _tstoi (cDate.Left (2).GetBuffer ());
	int month = _tstoi (cDate.Mid (3, 2).GetBuffer ());
	int year = _tstoi (cDate.Right (4).GetBuffer ());
	if (day == 0 || month == 0)
	{
		return TRUE;
	}
	CTime Date (year, month, day, 0, 0, 0);
	__time64_t date = Date.GetTime () / (24 * 3600);

	CString cSysDate; 
	CStrFuncs::SysDate (cSysDate);  
 	day = _tstoi (cSysDate.Left (2).GetBuffer ());
	month = _tstoi (cSysDate.Mid (3, 2).GetBuffer ());
	year = _tstoi (cSysDate.Right (4).GetBuffer ());
	if (day == 0 || month == 0)
	{
		return TRUE;
	}
	CTime SysDate (year, month, day, 0, 0, 0);
   
//	CTime SysDate = CTime::GetCurrentTime ();

	__time64_t sysdate = SysDate.GetTime () / (24 * 3600);
	int sYear  = SysDate.GetYear ();
	int sMonth = SysDate.GetMonth ();
	int sDay   = SysDate.GetDay ();
       
	if (date < sysdate)
	{
		AfxMessageBox (_T("Das Datum ist kleiner als das Tagesdatum"),MB_OK | MB_ICONERROR, 0);
		return FALSE;
	}
	return TRUE;
}

void CNewPrListACtrl::SetListMode (int ListMode)
{
	if (ListMode != Compact && ListMode != Large)
	{
		return;
	}
	this->ListMode = ListMode;
	switch (ListMode)
	{
		case Large :
			CompanyWidth = LargeWidth;
			break;
		case Compact :
			CompanyWidth = CompactWidth;
			break;
	}
}

void CNewPrListACtrl::CalculateAbverk ()
{
	if (!ShowAbverk) return;
	CString Value;
	if (EnterBasis)
	{
		GetColValue (EditRow, PosPrEkAb, Value);
		if (A_bas.a_bas.inh_abverk == 0.0)
		{
			A_bas.a_bas.inh_abverk = 1.0;
		}
		double pr_ek = CStrFuncs::StrToDouble (Value);
		pr_ek *= A_bas.a_bas.inh_abverk;
		Value.Format (_T("%.2lf"), pr_ek);
		FillList.SetItemText (Value.GetBuffer (0), EditRow, PosPrEkN);

		GetColValue (EditRow, PosPrVkAb, Value);
		double pr_vk = CStrFuncs::StrToDouble (Value);
		pr_vk *= A_bas.a_bas.inh_abverk;
		Value.Format (_T("%.2lf"), pr_vk);
		FillList.SetItemText (Value.GetBuffer (0), EditRow, PosPrVkN);
	}
	else
	{
		GetColValue (EditRow, PosPrEkN, Value);
		if (A_bas.a_bas.inh_abverk == 0.0)
		{
			A_bas.a_bas.inh_abverk = 1.0;
		}
		double pr_ek_abverk = CStrFuncs::StrToDouble (Value);
		pr_ek_abverk /= A_bas.a_bas.inh_abverk;
		Value.Format (_T("%.2lf"), pr_ek_abverk);
		FillList.SetItemText (Value.GetBuffer (0), EditRow, PosPrEkAb);

		GetColValue (EditRow, PosPrVkN, Value);
		double pr_vk_abverk = CStrFuncs::StrToDouble (Value);
		pr_vk_abverk /= A_bas.a_bas.inh_abverk;
		Value.Format (_T("%.2lf"), pr_vk_abverk);
		FillList.SetItemText (Value.GetBuffer (0), EditRow, PosPrVkAb);
	}
}
void CNewPrListACtrl::ReadA ()
{
	if (EditCol != PosA) return;
	if (! IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		return;
	}
    memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
    CString Text;

	SearchListCtrl.Edit.GetWindowText (Text);
	if (!CStrFuncs::IsDecimal (Text))
	{
			OnAChoice (Text);
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%.0lf"), CStrFuncs::StrToDouble (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!AChoiceStat)
			{
				EditCol --;
				return;
			}
	}
	A_bas.a_bas.a = CStrFuncs::StrToDouble (Text);
	if (A_bas.a_bas.a == 0) 
	{
		EditCol --;
		return;
	}
	if (A_bas.dbreadfirst () != 0)
	{
		MessageBox (_T("Artikel nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return;
	}
	Text.Format (_T("%.0lf  %s"), A_bas.a_bas.a, A_bas.a_bas.a_bz1);
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
			SearchListCtrl.Edit.SetWindowText (Text);
	}
}

void CNewPrListACtrl::OnAChoice (CString& Search)
{
	AChoiceStat = TRUE;
	if (ChoiceA != NULL && !ModalChoiceA)
	{
		ChoiceA->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceA == NULL)
	{
		ChoiceA = new CChoiceA (this);
	    ChoiceA->IsModal = ModalChoiceA;
#ifndef ARTIKEL
	    ChoiceA->HideEnter = FALSE;
#endif
	    ChoiceA->HideFilter = FALSE;
		ChoiceA->IdArrDown = IDI_HARROWDOWN;
		ChoiceA->IdArrUp   = IDI_HARROWUP;
		ChoiceA->IdArrNo   = IDI_HARROWNO;
		ChoiceA->CreateDlg ();
	}

    ChoiceA->SetDbClass (&Mdn);
	ChoiceA->SearchText = Search;
	ChoiceA->Where = _T("");
	if (ArtSelect != _T(""))
	{
		ChoiceA->Where = ArtSelect;
	}
	if (ModalChoiceA)
	{
//			ChoiceA->FirstRead = TRUE;
			ChoiceA->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceA->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceA->MoveWindow (&rect);
		ChoiceA->SetFocus ();
		return;
	}
    if (ChoiceA->GetState ())
    {
		  CABasList *abl = ChoiceA->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  AChoiceStat = FALSE;
			  return;
		  }
		  CString Text;
		  Text.Format (_T("%.0lf"), abl->a);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
		 AChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}
