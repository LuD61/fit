#ifndef _A_KALKPREIS_DEF
#define _A_KALKPREIS_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_KALKPREIS {
   long           mdn;
   short          fil;
   double         a;
   double         mat_o_b;
   double         hk_vollk;
   double         sk_vollk;
   double         fil_ek_vollk;
   double         fil_vk_vollk;
   double         hk_teilk;
   double         sk_teilk;
   double         fil_ek_teilk;
   double         fil_vk_teilk;
   DATE_STRUCT    dat;
   TCHAR          zeit[6];
   TCHAR          programm[13];
   TCHAR          version[13];
   double         kost;
};
extern struct A_KALKPREIS a_kalkpreis, a_kalkpreis_null;

#line 8 "a_kalkpreis.rh"

class A_KALKPREIS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_KALKPREIS a_kalkpreis;  
               A_KALKPREIS_CLASS () : DB_CLASS ()
               {
               }
};
#endif
