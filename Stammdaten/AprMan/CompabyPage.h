#pragma once
#include "afxwin.h"
#include "PrComboBox.h"


// CCompabyPage-Dialogfeld

class CCompabyPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CCompabyPage)

public:
	CCompabyPage();
	virtual ~CCompabyPage();

// Dialogfelddaten
	enum { IDD = IDD_COMPANY_PR_PAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	CStatic m_LMdnGr;
	CPrComboBox m_MdnGr;
	CStatic m_LMdn;
	CPrComboBox m_Mdn;
	CStatic m_LFilGr;
	CPrComboBox m_FilGr;
	CStatic m_LFil;
	CPrComboBox m_Fil;

	CStatic m_LHwg;
	CPrComboBox m_Hwg;
	CStatic m_LWg;
	CPrComboBox m_Wg;
	CStatic m_LAg;
	CPrComboBox m_Ag;

	CStatic m_LTeilSmt;
	CPrComboBox m_TeilSmt;
	CStatic m_LATyp;
	CPrComboBox m_ATyp;
};
