#pragma once
#include "DbFormView.h"
#include "APrSheet.h"
#include "ArtAktPage.h"
#include "CompanyPage.h"
#include "CmpListPage.h"
#include "mo_progcfg.h"



// CApr3-Formularansicht

class CApr3 : public DbFormView
{
	DECLARE_DYNCREATE(CApr3)

protected:
	CApr3();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CApr3();

public:
	enum { IDD = IDD_APR1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    afx_msg void OnSize(UINT, int, int);
	DECLARE_MESSAGE_MAP()
public:
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	int StartSize;
	int tabx;
	int taby;
	CAPrSheet dlg;
	CArtAktPage Page1;
	HBRUSH hBrush;
	HBRUSH staticBrush;
	void DeletePropertySheet ();
	afx_msg void OnLanguage();
	afx_msg void OnFileSave();
	afx_msg void OnBack();
	void ReadCfg ();

	afx_msg void OnDelete();
	afx_msg void OnInsert();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnDeleteall();
	afx_msg void OnFilePrint();
	afx_msg void OnPrintAll();
	afx_msg void OnUpdateInsert(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDelete(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFileSave(CCmdUI *pCmdUI);
public:
	afx_msg void OnCompactMode();
public:
	afx_msg void OnUpdateCompactMode(CCmdUI *pCmdUI);
};


