#include "StdAfx.h"
#include "apreise.h"

CAPreise::CAPreise(void)
{
	cEk = _T("0.0");
	cVk = _T("0.0");
	pr_vk1 = 0.0;
	pr_vk2 = 0.0;
	pr_vk3 = 0.0;
	memcpy (&a_pr, &a_pr_null, sizeof (A_PR));
}

CAPreise::CAPreise(CString& cEk, CString& cVk, A_PR& a_pr)
{
	this->cEk = cEk.Trim ();
	this->cVk = cVk.Trim ();
	pr_vk1 = a_pr.pr_vk1;
	pr_vk2 = a_pr.pr_vk2;
	pr_vk3 = a_pr.pr_vk3;
	memcpy (&this->a_pr, &a_pr, sizeof (A_PR));
}

CAPreise::~CAPreise(void)
{
}
