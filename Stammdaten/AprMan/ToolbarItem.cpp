#include "stdafx.h"
#include "ToolbarItem.h"
#include "Bmap.h"
#include "ToolButton.h"

CToolbarItem::CToolbarItem ()
{
    Id = 0;
    Hb = NULL;
    Seperator = NULL;
    Parent = NULL;
    Active = FALSE;
    Button = NULL;
}

CToolbarItem::CToolbarItem (int Id, HBITMAP Hb, CWnd *Parent)
{
    this->Id = Id;
    this->Hb = Hb;
    this->Seperator = NULL;
    this->Parent = Parent;
    Active = FALSE;
    Button = NULL;
}


CToolbarItem::CToolbarItem (int Id, BOOL Seperator, CWnd *Parent)
{
    this->Id = Id;
    this->Hb = NULL;
    this->Seperator = Seperator;
    this->Parent = Parent;
    Active = FALSE;
    Button = NULL;
}

CToolbarItem::CToolbarItem (int Id, CString& Text, CWnd *Parent)
{
    this->Id = Id;
    this->Hb = NULL;
    this->Seperator = TBTEXT;
    this->Text = Text;
    this->Parent = Parent;
    Active = FALSE;
    Button = NULL;
    TooltipText = "";
}

CToolbarItem::~CToolbarItem ()
{
    if (Button != NULL)
    {
        delete Button;
    }
}


BOOL CToolbarItem::IsCtrl (int Id)
{
    if (this->Id == Id)
    {
        return TRUE;
    }
    return FALSE;
}

BOOL CToolbarItem::IsTooltipCtrl (HWND hWnd, TOOLTIPTEXT *pTTT)
{
    if (Button != NULL)
    {
        if (Button->m_hWnd == hWnd)
        {
            pTTT->lpszText = TooltipText.GetBuffer (256);
            return TRUE;
        }
    }
    return FALSE;
}


void CToolbarItem::SetTooltipText (char *Text)
{
    TooltipText = Text;
}

void CToolbarItem::SetId (int Id)
{
    this->Id = Id;
}

void CToolbarItem::SetHb (HBITMAP Hb)
{
    this->Hb = Hb;
}

void CToolbarItem::SetText (CString &Text)
{
    this->Text = Text;
}

void CToolbarItem::SetSeperator (BOOL Seperator)
{
    this->Seperator = Seperator;
}

void CToolbarItem::SetParent (CWnd *Parent)
{
    this->Seperator = Seperator;
}

void CToolbarItem::SetActive (BOOL Active)
{
    this->Active = Active;
}

int CToolbarItem::GetId (void)
{
    return Id;
}

HBITMAP CToolbarItem::GetHb ()
{
    return Hb;
}

void CToolbarItem::GetHbSize (CPoint *p)
{
     BMAP bm;
     bm.BitmapSize ((HDC) NULL, Hb, p);
}

CString& CToolbarItem::GetText (void)
{
    return Text;
}

BOOL CToolbarItem::GetSeperator (void)
{
    return Seperator;
}

CWnd *CToolbarItem::GetParent (void)
{
    return Parent;
}

BOOL CToolbarItem::GetActive (void)
{
    return Active;
}

void CToolbarItem::Display (HDC hDC)
{
     if (Hb != NULL)
     {
           BMAP bm;
           bm.DrawBitmap (hDC, Hb, 0, 0);
     }
     else if (Seperator != NULL)
     {
           DrawSeperator (hDC);
     }
}

void CToolbarItem::Display (HDC hDC, int x, int y)
{
     CRect Rect; 

     BMAP bm;
     bm.DrawBitmap (hDC, Hb, x, y);
     if (Button != NULL)
     {
         Active = ((CColButton *) Button)->GetActive ();
     }
     if (Active == FALSE)
     {
          return;
     }
     if (Parent->GetDlgItem (Id) != NULL)
     {
              Parent->GetDlgItem (Id)->GetClientRect (&Rect);
     }
     else
     {
              return;
     }
}

void CToolbarItem::DisplayText (HDC hDC)
{
    CRect Rect; 
    CDC *cDC = CDC::FromHandle (hDC);
    if (Button != NULL)
    {
        Button->GetClientRect (&Rect);
    }
    else if (Parent->GetDlgItem (Id) != NULL)
    {
        Parent->GetDlgItem (Id)->GetClientRect (&Rect);
    }
    else
    {
        return;
    }

    CFont *Font = CToolButton::SetFont (cDC);
    CFont *oldFont = cDC->SelectObject (Font);

    CSize Size = cDC->GetTextExtent (Text);
    int x = max (0, (Rect.right - Size.cx) / 2);
    int y = max (0, (Rect.bottom - Size.cy) / 2);

    cDC->SetBkMode (TRANSPARENT);
    cDC->SetTextColor (RGB (0,0,255));
    cDC->TextOut (x,y, Text);
    cDC->SelectObject (oldFont);
    delete Font;
}


void CToolbarItem::CentItem (HDC hDC)
{
    CRect Rect; 
    POINT Point;
    int x = 0;
    int y = 0;

    if (Seperator == TBTEXT)
    {
            DisplayText (hDC);
            return;
    }
           
    if (Hb == NULL && Seperator != NULL)
    {
           DrawSeperator (hDC);
           return;
    }

    if (Parent->GetDlgItem (Id) != NULL && Button == NULL)
    {
        Parent->GetDlgItem (Id)->GetClientRect (&Rect);
    }
    else if (Button != NULL)
    {
        Button->GetClientRect (&Rect); 
    }
    else
    {
        return;
    }


    BMAP bm;
    bm.BitmapSize (hDC, Hb, &Point);
    x = max (0, (Rect.right  - Point.x) / 2);
    y = max (0, (Rect.bottom - Point.y) / 2);

    Display (hDC, x, y);
}


void CToolbarItem::DrawSeperator (HDC hDC)
{
    CRect Rect; 
    CDC *cDC = CDC::FromHandle (hDC);
    CPen *Pen = new CPen (PS_SOLID, 1, RGB (120,120,120));
    if (Parent == NULL)
    {
        return;
    }
    if (Parent->GetDlgItem (Id) != NULL)
    {
        Parent->GetDlgItem (Id)->GetClientRect (&Rect);
    }
    else
    {
        return;
    }
    if (Seperator == VERT)
    {
        Rect.left = max (0, Rect.right - 2) / 2;
        CPen *oldPen = cDC->SelectObject (Pen);
        cDC->MoveTo (Rect.left, Rect.top);
        cDC->LineTo (Rect.left, Rect.bottom);
        cDC->SelectObject (oldPen);
        delete Pen;
        Pen = new CPen (PS_SOLID, 1, RGB (255,255,255));
        cDC->SelectObject (Pen);
        cDC->MoveTo (Rect.left + 1, Rect.top);
        cDC->LineTo (Rect.left + 1, Rect.bottom);
        cDC->SelectObject (oldPen);
        delete Pen;
    }
    else if (Seperator == TBDOWN ||
             Seperator == TBUP)
    {
        CPen *oldPen = cDC->SelectObject (Pen);
        cDC->MoveTo (Rect.left, Rect.top);
        cDC->LineTo (Rect.right, Rect.top);
        cDC->SelectObject (oldPen);
        delete Pen;
        Pen = new CPen (PS_SOLID, 1, RGB (255,255,255));
        cDC->SelectObject (Pen);
        cDC->MoveTo (Rect.left, Rect.top + 1);
        cDC->LineTo (Rect.right, Rect.top +1);
        cDC->SelectObject (oldPen);
        delete Pen;
    }
}

void CToolbarItem::CreateButton (int x, int y, int cx, int cy)
{
    if (Button != NULL)
    {
        delete Button;
    }

    if (Seperator == TBDOWN ||
        Seperator == TBUP ||
        Seperator == VERT)
    {
            Button = new CButton ();
    }
    else
    {
            Button = new CColButton ();
            ((CColButton *)Button)->SetId (Id);
    }

    DWORD Style = WS_CHILD | WS_VISIBLE | BS_OWNERDRAW; 
//    DWORD Style = WS_CHILD | WS_VISIBLE | BS_BITMAP; 
    CRect Rect;
    Rect.left   = x;
    Rect.top    = y;
    Rect.right  = x + cx;
    Rect.bottom = y + cy;
    int ret = Button->Create ("", Style, Rect, Parent, Id);
}
    





