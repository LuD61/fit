#include "stdafx.h"
#include "a_kalk_eig.h"

struct A_KALK_EIG a_kalk_eig, a_kalk_eig_null, a_kalk_eig_def;

void A_KALK_EIG_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)    &a_kalk_eig.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalk_eig.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalk_eig.a,   SQLDOUBLE, 0);
    sqlout ((double *) &a_kalk_eig.a,SQLDOUBLE,0);
    sqlout ((short *) &a_kalk_eig.bearb_fil,SQLSHORT,0);
    sqlout ((short *) &a_kalk_eig.bearb_lad,SQLSHORT,0);
    sqlout ((short *) &a_kalk_eig.bearb_sk,SQLSHORT,0);
    sqlout ((short *) &a_kalk_eig.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_kalk_eig.fil,SQLSHORT,0);
    sqlout ((double *) &a_kalk_eig.fil_ek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.fil_ek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.hk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.hk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.kost,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.lad_vk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.lad_vk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.mat_o_b,SQLDOUBLE,0);
    sqlout ((short *) &a_kalk_eig.mdn,SQLSHORT,0);
    sqlout ((double *) &a_kalk_eig.sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.sp_fil,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.sp_lad,SQLDOUBLE,0);
    sqlout ((double *) &a_kalk_eig.sp_vk,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &a_kalk_eig.dat,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &a_kalk_eig.aend_dat,SQLDATE,0);
            cursor = sqlcursor (_T("select a_kalk_eig.a,  ")
_T("a_kalk_eig.bearb_fil,  a_kalk_eig.bearb_lad,  a_kalk_eig.bearb_sk,  ")
_T("a_kalk_eig.delstatus,  a_kalk_eig.fil,  a_kalk_eig.fil_ek_teilk,  ")
_T("a_kalk_eig.fil_ek_vollk,  a_kalk_eig.hk_teilk,  ")
_T("a_kalk_eig.hk_vollk,  a_kalk_eig.kost,  a_kalk_eig.lad_vk_teilk,  ")
_T("a_kalk_eig.lad_vk_vollk,  a_kalk_eig.mat_o_b,  a_kalk_eig.mdn,  ")
_T("a_kalk_eig.sk_teilk,  a_kalk_eig.sk_vollk,  a_kalk_eig.sp_fil,  ")
_T("a_kalk_eig.sp_lad,  a_kalk_eig.sp_vk,  a_kalk_eig.dat,  ")
_T("a_kalk_eig.aend_dat from a_kalk_eig ")

#line 14 "a_kalk_eig.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
    sqlin ((double *) &a_kalk_eig.a,SQLDOUBLE,0);
    sqlin ((short *) &a_kalk_eig.bearb_fil,SQLSHORT,0);
    sqlin ((short *) &a_kalk_eig.bearb_lad,SQLSHORT,0);
    sqlin ((short *) &a_kalk_eig.bearb_sk,SQLSHORT,0);
    sqlin ((short *) &a_kalk_eig.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_kalk_eig.fil,SQLSHORT,0);
    sqlin ((double *) &a_kalk_eig.fil_ek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.fil_ek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.kost,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.lad_vk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.lad_vk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.mat_o_b,SQLDOUBLE,0);
    sqlin ((short *) &a_kalk_eig.mdn,SQLSHORT,0);
    sqlin ((double *) &a_kalk_eig.sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.sp_fil,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.sp_lad,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.sp_vk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_kalk_eig.dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_kalk_eig.aend_dat,SQLDATE,0);
            sqltext = _T("update a_kalk_eig set ")
_T("a_kalk_eig.a = ?,  a_kalk_eig.bearb_fil = ?,  ")
_T("a_kalk_eig.bearb_lad = ?,  a_kalk_eig.bearb_sk = ?,  ")
_T("a_kalk_eig.delstatus = ?,  a_kalk_eig.fil = ?,  ")
_T("a_kalk_eig.fil_ek_teilk = ?,  a_kalk_eig.fil_ek_vollk = ?,  ")
_T("a_kalk_eig.hk_teilk = ?,  a_kalk_eig.hk_vollk = ?,  ")
_T("a_kalk_eig.kost = ?,  a_kalk_eig.lad_vk_teilk = ?,  ")
_T("a_kalk_eig.lad_vk_vollk = ?,  a_kalk_eig.mat_o_b = ?,  ")
_T("a_kalk_eig.mdn = ?,  a_kalk_eig.sk_teilk = ?,  ")
_T("a_kalk_eig.sk_vollk = ?,  a_kalk_eig.sp_fil = ?,  ")
_T("a_kalk_eig.sp_lad = ?,  a_kalk_eig.sp_vk = ?,  a_kalk_eig.dat = ?,  ")
_T("a_kalk_eig.aend_dat = ? ")

#line 18 "a_kalk_eig.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?");
            sqlin ((short *)    &a_kalk_eig.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalk_eig.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalk_eig.a,   SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)    &a_kalk_eig.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalk_eig.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalk_eig.a,   SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_kalk_eig ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
            sqlin ((short *)    &a_kalk_eig.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalk_eig.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalk_eig.a,   SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_kalk_eig set delstatus = 0 ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
            sqlin ((short *)    &a_kalk_eig.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalk_eig.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalk_eig.a,   SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_kalk_eig ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
    sqlin ((double *) &a_kalk_eig.a,SQLDOUBLE,0);
    sqlin ((short *) &a_kalk_eig.bearb_fil,SQLSHORT,0);
    sqlin ((short *) &a_kalk_eig.bearb_lad,SQLSHORT,0);
    sqlin ((short *) &a_kalk_eig.bearb_sk,SQLSHORT,0);
    sqlin ((short *) &a_kalk_eig.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_kalk_eig.fil,SQLSHORT,0);
    sqlin ((double *) &a_kalk_eig.fil_ek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.fil_ek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.hk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.hk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.kost,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.lad_vk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.lad_vk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.mat_o_b,SQLDOUBLE,0);
    sqlin ((short *) &a_kalk_eig.mdn,SQLSHORT,0);
    sqlin ((double *) &a_kalk_eig.sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.sp_fil,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.sp_lad,SQLDOUBLE,0);
    sqlin ((double *) &a_kalk_eig.sp_vk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_kalk_eig.dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_kalk_eig.aend_dat,SQLDATE,0);
            ins_cursor = sqlcursor (_T("insert into a_kalk_eig (")
_T("a,  bearb_fil,  bearb_lad,  bearb_sk,  delstatus,  fil,  fil_ek_teilk,  ")
_T("fil_ek_vollk,  hk_teilk,  hk_vollk,  kost,  lad_vk_teilk,  lad_vk_vollk,  ")
_T("mat_o_b,  mdn,  sk_teilk,  sk_vollk,  sp_fil,  sp_lad,  sp_vk,  dat,  aend_dat) ")

#line 48 "a_kalk_eig.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 50 "a_kalk_eig.rpp"
}
