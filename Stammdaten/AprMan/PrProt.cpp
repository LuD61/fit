#include "StdAfx.h"
#include "prprot.h"
#include "StrFuncs.h"

CPrProt::CPrProt(void)
{
	PersName = "";
	Programm = "";
	A_pr = NULL;
	Akt_krz = NULL;
	Akt_krza = NULL;
}

CPrProt::CPrProt(CString& PersNam, CString& Programm, APR_CLASS *A_pr, AKT_KRZ_CLASS *Akt_krz,AKT_KRZA_CLASS *Akt_krza)
{
	this->PersName = PersNam;
	this->Programm = Programm;
	this->A_pr = A_pr;
	this->Akt_krz = Akt_krz;
	this->Akt_krza = Akt_krza;
}

CPrProt::~CPrProt(void)
{
}

void CPrProt::Construct (CString& PersNam, CString& Programm, APR_CLASS *A_pr , AKT_KRZ_CLASS *Akt_krz, AKT_KRZA_CLASS *Akt_krza  )
{
	this->PersName = PersNam;
	this->Programm = Programm;
	this->A_pr = A_pr;
	this->Akt_krz = Akt_krz;
	this->Akt_krza = Akt_krza;
}

void CPrProt::Write (int typ, double pr_ek_alt, double pr_vk_alt)
{
	if (A_pr == NULL ) return;

	memcpy (&Pr_prot.pr_prot, &pr_prot_null, sizeof  (PR_PROT));
	CString Date;
	CStrFuncs::SysDate (Date);
	Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.bearb);
	CString Time;
	CStrFuncs::SysTime (Time);

//	strcpy (Pr_prot.pr_prot.zeit, Time.GetBuffer (8));
	strcpy (Pr_prot.pr_prot.pers_nam, PersName.GetBuffer (8));
	if (typ == 0)
	{
		Pr_prot.pr_prot.mdn_gr = A_pr->a_pr.mdn_gr;
		Pr_prot.pr_prot.mdn = A_pr->a_pr.mdn;
		Pr_prot.pr_prot.fil_gr = A_pr->a_pr.fil_gr;
		Pr_prot.pr_prot.fil = A_pr->a_pr.fil;
		Pr_prot.pr_prot.a = A_pr->a_pr.a;
		Pr_prot.pr_prot.pr_vk = A_pr->a_pr.pr_vk;
		Pr_prot.pr_prot.pr_vk_euro = A_pr->a_pr.pr_vk;

		Pr_prot.pr_prot.pr_vk_sa = 0;
		Pr_prot.pr_prot.pr_vk_sa_euro = 0;

		Pr_prot.pr_prot.pr_vk_alt = pr_vk_alt;
		Pr_prot.pr_prot.pr_vk_alt_euro = pr_vk_alt;
		Date = "31.12.1899";
		Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.lad_akt_von);
		Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.lad_akt_bis);
	}
	else	// 1 => auf termin, 2 => Aktion  :neu ab 070613
	{
		if ( typ == 2 )
		{

			Pr_prot.pr_prot.mdn_gr = Akt_krza->akt_krz.mdn_gr;
			Pr_prot.pr_prot.mdn = Akt_krza->akt_krz.mdn;
			Pr_prot.pr_prot.fil_gr = Akt_krza->akt_krz.fil_gr;
			Pr_prot.pr_prot.fil = Akt_krza->akt_krz.fil;
			Pr_prot.pr_prot.a = Akt_krza->akt_krz.a;
			Pr_prot.pr_prot.pr_vk = 0 ;
			Pr_prot.pr_prot.pr_vk_euro = 0 ;
			Pr_prot.pr_prot.pr_vk_sa = Akt_krza->akt_krz.pr_vk_sa;
			Pr_prot.pr_prot.pr_vk_sa_euro = Akt_krza->akt_krz.pr_vk_sa;
			Pr_prot.pr_prot.pr_vk_alt = pr_vk_alt;
			Pr_prot.pr_prot.pr_vk_alt_euro = pr_vk_alt;


			memcpy( &Pr_prot.pr_prot.lad_akt_von , &Akt_krza->akt_krz.lad_akt_von , sizeof( DATE_STRUCT)) ;
			memcpy( &Pr_prot.pr_prot.lad_akt_bis , &Akt_krza->akt_krz.lad_akt_bis , sizeof( DATE_STRUCT)) ;
			Date = "31.12.1899";
			if ( Pr_prot.pr_prot.lad_akt_von.year < 1999 || Pr_prot.pr_prot.lad_akt_von.year > 2099 )
				Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.lad_akt_von);
			if ( Pr_prot.pr_prot.lad_akt_bis.year < 1999 || Pr_prot.pr_prot.lad_akt_bis.year > 2099 )
				Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.lad_akt_bis);

		}
		else		// Typ == 1 
		{
			Pr_prot.pr_prot.mdn_gr = Akt_krz->akt_krz.mdn_gr;
			Pr_prot.pr_prot.mdn = Akt_krz->akt_krz.mdn;
			Pr_prot.pr_prot.fil_gr = Akt_krz->akt_krz.fil_gr;
			Pr_prot.pr_prot.fil = Akt_krz->akt_krz.fil;
			Pr_prot.pr_prot.a = Akt_krz->akt_krz.a;
			Pr_prot.pr_prot.pr_vk = 0 ;
			Pr_prot.pr_prot.pr_vk_euro = 0 ;
			Pr_prot.pr_prot.pr_vk_sa = Akt_krz->akt_krz.pr_vk_sa;
			Pr_prot.pr_prot.pr_vk_sa_euro = Akt_krz->akt_krz.pr_vk_sa;
			Pr_prot.pr_prot.pr_vk_alt = pr_vk_alt;
			Pr_prot.pr_prot.pr_vk_alt_euro = pr_vk_alt;

			memcpy( &Pr_prot.pr_prot.lad_akt_von , &Akt_krz->akt_krz.lad_akt_von , sizeof( DATE_STRUCT)) ;
			memcpy( &Pr_prot.pr_prot.lad_akt_bis , &Akt_krz->akt_krz.lad_akt_bis , sizeof( DATE_STRUCT)) ;
			Date = "31.12.1899";
			if ( Pr_prot.pr_prot.lad_akt_von.year < 1999 || Pr_prot.pr_prot.lad_akt_von.year > 2099 )
				Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.lad_akt_von);
			if ( Pr_prot.pr_prot.lad_akt_bis.year < 1999 || Pr_prot.pr_prot.lad_akt_bis.year > 2099 )
				Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.lad_akt_bis);
		}
	}
    Pr_prot.dbinsert ();
}

