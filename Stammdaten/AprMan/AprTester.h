#pragma once
#include <odbcinst.h>
#include "akt_krz.h"
#include "a_pr.h"

class CAprTester
{
private:
	int cursor;
	int cursor_akt_exist;
	DB_CLASS DbClass;
public:
	short mdn_gr;
	short mdn;
	short fil_gr;
	short fil;
	double a;
	TCHAR test_lad_akv[2];
	int akt_status;


	short akt;
	TCHAR lad_akv[2];
	TCHAR lief_akv[2];

	void Prepare ();

	CAprTester(void);
	~CAprTester(void);
	void Clear ();
	void TestLadAkv (AKT_KRZ& akt_krz, A_PR& a_pr, TCHAR *test_lad_akv);
	void TestAkt (AKT_KRZ &akt_krz,  A_PR& a_pr);
	BOOL operator!=(AKT_KRZ &akt_krz);
};
