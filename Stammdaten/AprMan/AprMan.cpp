// AprMan.cpp : Definiert das Klassenverhalten f�r die Anwendung.
//

#include "stdafx.h"
#include "AprMan.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "AprManDoc.h"
#include "AprManView.h"
#include "Apr1.h"
#include "Apr2.h"
#include "Apr3.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAprManApp

BEGIN_MESSAGE_MAP(CAprManApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// Dateibasierte Standarddokumentbefehle
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standarddruckbefehl "Seite einrichten"
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
	ON_COMMAND(ID_STD_PR, OnStdPr)
	ON_COMMAND(ID_NEW_PR, OnNewPr)
	ON_COMMAND(ID_ARTAKT, OnArtAkt)
//	ON_COMMAND(ID_AKT_PR_GR, OnPrAki)
END_MESSAGE_MAP()


CDocTemplate *StdPrInstance = NULL; 
CDocTemplate *NewPrInstance = NULL; 
CDocTemplate *ArtAktInstance = NULL; 

// CAprManApp-Erstellung

CAprManApp::CAprManApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance positionieren
}


// Das einzige CAprManApp-Objekt

CAprManApp theApp;

// CAprManApp Initialisierung

BOOL CAprManApp::InitInstance()
{
	// InitCommonControls() ist f�r Windows XP erforderlich, wenn ein Anwendungsmanifest
	// die Verwendung von ComCtl32.dll Version 6 oder h�her zum Aktivieren
	// von visuellen Stilen angibt. Ansonsten treten beim Erstellen von Fenstern Fehler auf.
	InitCommonControls();

	CWinApp::InitInstance();

	// OLE-Bibliotheken initialisieren
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standardinitialisierung
	// Wenn Sie diese Features nicht verwenden und die Gr��e
	// der ausf�hrbaren Datei verringern m�chten, entfernen Sie
	// die nicht erforderlichen Initialisierungsroutinen.
	// �ndern Sie den Registrierungsschl�ssel unter dem Ihre Einstellungen gespeichert sind.
	// TODO: �ndern Sie diese Zeichenfolge entsprechend,
	// z.B. zum Namen Ihrer Firma oder Organisation.
	SetRegistryKey(_T("Vom lokalen Anwendungs-Assistenten generierte Anwendungen"));
	LoadStdProfileSettings(4);  // Standard INI-Dateioptionen laden (einschlie�lich MRU)
	// Dokumentvorlagen der Anwendung registrieren. Dokumentvorlagen
	//  dienen als Verbindung zwischen Dokumenten, Rahmenfenstern und Ansichten.
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_AprManTYPE,
		RUNTIME_CLASS(CAprManDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CApr1));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);
	pDocTemplate = new CMultiDocTemplate(IDR_NEWPR,
		RUNTIME_CLASS(CAprManDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CApr2));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);
	pDocTemplate = new CMultiDocTemplate(IDR_ARTAKT,
		RUNTIME_CLASS(CAprManDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CApr3));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);
	// Haupt-MDI-Rahmenfenster erstellen
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;
	// Rufen Sie DragAcceptFiles nur auf, wenn eine Suffix vorhanden ist.
	//  In einer MDI-Anwendung ist dies unmittelbar nach dem Festlegen von m_pMainWnd erforderlich
	// Befehlszeile parsen, um zu pr�fen auf Standardumgebungsbefehle DDE, Datei offen
	CCommandLineInfo cmdInfo;
//	ParseCommandLine(cmdInfo);
	// Verteilung der in der Befehlszeile angegebenen Befehle. Es wird FALSE zur�ckgegeben, wenn
	// die Anwendung mit /RegServer, /Register, /Unregserver oder /Unregister gestartet wurde.
//	if (!ProcessShellCommand(cmdInfo))
//		return FALSE;
	// Das Hauptfenster ist initialisiert und kann jetzt angezeigt und aktualisiert werden.
	pMainFrame->ShowWindow(SW_SHOWMAXIMIZED);
	pMainFrame->UpdateWindow();
	OnStdPr ();
	return TRUE;
}



// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

class CAboutDlgEx : public CDialog
{
public:
	CAboutDlgEx ();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlgEx::CAboutDlgEx() : CDialog(CAboutDlgEx::IDD)
{
}

void CAboutDlgEx::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlgEx, CDialog)
END_MESSAGE_MAP()

// Anwendungsbefehl zum Ausf�hren des Dialogfelds
void CAprManApp::OnAppAbout()
{
	CAboutDlgEx aboutDlg;
	aboutDlg.DoModal();
}


// CAprManApp Meldungshandler


void CAprManApp::OnStdPr()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	POSITION pos;
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		if (dt == StdPrInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->StdPrWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->StdPrWnd);
				MainFrame->StdPrWnd->SetFocus ();
				return;
			}
		}

		StdPrInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
//			dt->SetDefaultTitle (dc);
			dc->SetTitle (_T("Standardpreise"));
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}


void CAprManApp::OnNewPr()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	POSITION pos;
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		if (dt == NewPrInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->NewPrWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->NewPrWnd);
				return;
			}
		}

		NewPrInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
//			dt->SetDefaultTitle (dc);
			dc->SetTitle (_T("Preise auf Termin"));
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}

void CAprManApp::OnArtAkt()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	POSITION pos;
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		dt = GetNextDocTemplate (pos);
	}
	if (dt != NULL)
	{
		if (dt == ArtAktInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->ArtAktWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->ArtAktWnd);
				return;
			}
		}

		ArtAktInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
//			dt->SetDefaultTitle (dc);
			dc->SetTitle (_T("Artikelaktionen"));
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}
