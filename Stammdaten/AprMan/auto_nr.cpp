#include "stdafx.h"
#include "auto_nr.h"

struct AUTO_NR auto_nr, auto_nr_null;

void AUTO_NR_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *) &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *) &auto_nr.fil, SQLSHORT, 0);
            sqlin ((char *) &auto_nr.nr_nam, SQLCHAR, 12);
            sqlin ((long *) &auto_nr.nr_nr, SQLLONG, 0);
    sqlout ((short *) &auto_nr.mdn,SQLSHORT,0);
    sqlout ((short *) &auto_nr.fil,SQLSHORT,0);
    sqlout ((TCHAR *) auto_nr.nr_nam,SQLCHAR,11);
    sqlout ((long *) &auto_nr.nr_nr,SQLLONG,0);
    sqlout ((short *) &auto_nr.satz_kng,SQLSHORT,0);
    sqlout ((long *) &auto_nr.max_wert,SQLLONG,0);
    sqlout ((TCHAR *) auto_nr.nr_char,SQLCHAR,11);
    sqlout ((long *) &auto_nr.nr_char_lng,SQLLONG,0);
    sqlout ((TCHAR *) auto_nr.fest_teil,SQLCHAR,11);
    sqlout ((TCHAR *) auto_nr.nr_komb,SQLCHAR,21);
    sqlout ((short *) &auto_nr.delstatus,SQLSHORT,0);
            cursor = sqlcursor (_T("select auto_nr.mdn,  ")
_T("auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  ")
_T("auto_nr.max_wert,  auto_nr.nr_char,  auto_nr.nr_char_lng,  ")
_T("auto_nr.fest_teil,  auto_nr.nr_komb,  auto_nr.delstatus from auto_nr ")

#line 15 "auto_nr.rpp"
                                 _T("where mdn = ? ")
                                 _T("and   fil = ? ")
                                 _T("and   nr_nam = ? ")
                                 _T("and   nr_nr = ?"));

            sqlin ((short *) &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *) &auto_nr.fil, SQLSHORT, 0);
            sqlin ((char *) &auto_nr.nr_nam, SQLCHAR, 12);
    sqlout ((short *) &auto_nr.mdn,SQLSHORT,0);
    sqlout ((short *) &auto_nr.fil,SQLSHORT,0);
    sqlout ((TCHAR *) auto_nr.nr_nam,SQLCHAR,11);
    sqlout ((long *) &auto_nr.nr_nr,SQLLONG,0);
    sqlout ((short *) &auto_nr.satz_kng,SQLSHORT,0);
    sqlout ((long *) &auto_nr.max_wert,SQLLONG,0);
    sqlout ((TCHAR *) auto_nr.nr_char,SQLCHAR,11);
    sqlout ((long *) &auto_nr.nr_char_lng,SQLLONG,0);
    sqlout ((TCHAR *) auto_nr.fest_teil,SQLCHAR,11);
    sqlout ((TCHAR *) auto_nr.nr_komb,SQLCHAR,21);
    sqlout ((short *) &auto_nr.delstatus,SQLSHORT,0);
            cursor_1 = sqlcursor (_T("select auto_nr.mdn,  ")
_T("auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  ")
_T("auto_nr.max_wert,  auto_nr.nr_char,  auto_nr.nr_char_lng,  ")
_T("auto_nr.fest_teil,  auto_nr.nr_komb,  auto_nr.delstatus from auto_nr ")

#line 24 "auto_nr.rpp"
                                 _T("where mdn = ? ")
                                 _T("and   fil = ? ")
                                 _T("and   nr_nam = ? ")
                                 _T("and   satz_kng = 1 for update"));

            sqlin ((short *) &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *) &auto_nr.fil, SQLSHORT, 0);
            sqlin ((char *) &auto_nr.nr_nam, SQLCHAR, 12);
    sqlout ((short *) &auto_nr.mdn,SQLSHORT,0);
    sqlout ((short *) &auto_nr.fil,SQLSHORT,0);
    sqlout ((TCHAR *) auto_nr.nr_nam,SQLCHAR,11);
    sqlout ((long *) &auto_nr.nr_nr,SQLLONG,0);
    sqlout ((short *) &auto_nr.satz_kng,SQLSHORT,0);
    sqlout ((long *) &auto_nr.max_wert,SQLLONG,0);
    sqlout ((TCHAR *) auto_nr.nr_char,SQLCHAR,11);
    sqlout ((long *) &auto_nr.nr_char_lng,SQLLONG,0);
    sqlout ((TCHAR *) auto_nr.fest_teil,SQLCHAR,11);
    sqlout ((TCHAR *) auto_nr.nr_komb,SQLCHAR,21);
    sqlout ((short *) &auto_nr.delstatus,SQLSHORT,0);
            cursor_2 = sqlcursor (_T("select auto_nr.mdn,  ")
_T("auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  ")
_T("auto_nr.max_wert,  auto_nr.nr_char,  auto_nr.nr_char_lng,  ")
_T("auto_nr.fest_teil,  auto_nr.nr_komb,  auto_nr.delstatus from auto_nr ")

#line 33 "auto_nr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   nr_nam = ? ")
                                  _T("and   satz_kng = 2 for update"));

            sqlin ((short *) &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *) &auto_nr.fil, SQLSHORT, 0);
            sqlin ((char *) &auto_nr.nr_nam, SQLCHAR, 12);
    sqlout ((short *) &auto_nr.mdn,SQLSHORT,0);
    sqlout ((short *) &auto_nr.fil,SQLSHORT,0);
    sqlout ((TCHAR *) auto_nr.nr_nam,SQLCHAR,11);
    sqlout ((long *) &auto_nr.nr_nr,SQLLONG,0);
    sqlout ((short *) &auto_nr.satz_kng,SQLSHORT,0);
    sqlout ((long *) &auto_nr.max_wert,SQLLONG,0);
    sqlout ((TCHAR *) auto_nr.nr_char,SQLCHAR,11);
    sqlout ((long *) &auto_nr.nr_char_lng,SQLLONG,0);
    sqlout ((TCHAR *) auto_nr.fest_teil,SQLCHAR,11);
    sqlout ((TCHAR *) auto_nr.nr_komb,SQLCHAR,21);
    sqlout ((short *) &auto_nr.delstatus,SQLSHORT,0);
            cursor_0 = sqlcursor (_T("select auto_nr.mdn,  ")
_T("auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  ")
_T("auto_nr.max_wert,  auto_nr.nr_char,  auto_nr.nr_char_lng,  ")
_T("auto_nr.fest_teil,  auto_nr.nr_komb,  auto_nr.delstatus from auto_nr ")

#line 42 "auto_nr.rpp"
				  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   nr_nam = ?"));

    sqlin ((short *) &auto_nr.mdn,SQLSHORT,0);
    sqlin ((short *) &auto_nr.fil,SQLSHORT,0);
    sqlin ((TCHAR *) auto_nr.nr_nam,SQLCHAR,11);
    sqlin ((long *) &auto_nr.nr_nr,SQLLONG,0);
    sqlin ((short *) &auto_nr.satz_kng,SQLSHORT,0);
    sqlin ((long *) &auto_nr.max_wert,SQLLONG,0);
    sqlin ((TCHAR *) auto_nr.nr_char,SQLCHAR,11);
    sqlin ((long *) &auto_nr.nr_char_lng,SQLLONG,0);
    sqlin ((TCHAR *) auto_nr.fest_teil,SQLCHAR,11);
    sqlin ((TCHAR *) auto_nr.nr_komb,SQLCHAR,21);
    sqlin ((short *) &auto_nr.delstatus,SQLSHORT,0);
            sqltext = _T("update auto_nr set auto_nr.mdn = ?,  ")
_T("auto_nr.fil = ?,  auto_nr.nr_nam = ?,  auto_nr.nr_nr = ?,  ")
_T("auto_nr.satz_kng = ?,  auto_nr.max_wert = ?,  auto_nr.nr_char = ?,  ")
_T("auto_nr.nr_char_lng = ?,  auto_nr.fest_teil = ?,  ")
_T("auto_nr.nr_komb = ?,  auto_nr.delstatus = ? ")

#line 47 "auto_nr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   nr_nam = ? ")
                                  _T("and   nr_nr = ? ")
                                  _T("and   satz_kng  = ?");
            sqlin ((short *) &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *) &auto_nr.fil, SQLSHORT, 0);
            sqlin ((char *) &auto_nr.nr_nam, SQLCHAR, 12);
            sqlin ((long *) &auto_nr.nr_nr, SQLLONG, 0);
            sqlin ((short *) &auto_nr.satz_kng, SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *) &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *) &auto_nr.fil, SQLSHORT, 0);
            sqlin ((char *) &auto_nr.nr_nam, SQLCHAR, 12);
            sqlin ((long *) &auto_nr.nr_nr, SQLLONG, 0);
            sqlin ((short *) &auto_nr.satz_kng, SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select nr_nr from auto_nr ")
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   nr_nam = ? ")
                                  _T("and   nr_nr = ? ")
                                  _T("and   satz_kng  = ?"));


    sqlin ((short *) &auto_nr.mdn,SQLSHORT,0);
    sqlin ((short *) &auto_nr.fil,SQLSHORT,0);
    sqlin ((TCHAR *) auto_nr.nr_nam,SQLCHAR,11);
    sqlin ((long *) &auto_nr.nr_nr,SQLLONG,0);
    sqlin ((short *) &auto_nr.satz_kng,SQLSHORT,0);
    sqlin ((long *) &auto_nr.max_wert,SQLLONG,0);
    sqlin ((TCHAR *) auto_nr.nr_char,SQLCHAR,11);
    sqlin ((long *) &auto_nr.nr_char_lng,SQLLONG,0);
    sqlin ((TCHAR *) auto_nr.fest_teil,SQLCHAR,11);
    sqlin ((TCHAR *) auto_nr.nr_komb,SQLCHAR,21);
    sqlin ((short *) &auto_nr.delstatus,SQLSHORT,0);
            sqltext = _T("update auto_nr set auto_nr.mdn = ?,  ")
_T("auto_nr.fil = ?,  auto_nr.nr_nam = ?,  auto_nr.nr_nr = ?,  ")
_T("auto_nr.satz_kng = ?,  auto_nr.max_wert = ?,  auto_nr.nr_char = ?,  ")
_T("auto_nr.nr_char_lng = ?,  auto_nr.fest_teil = ?,  ")
_T("auto_nr.nr_komb = ?,  auto_nr.delstatus = ? ")

#line 73 "auto_nr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   nr_nam = ? ")
                                  _T("and   satz_kng  = 2");

            sqlin ((short *) &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *) &auto_nr.fil, SQLSHORT, 0);
            sqlin ((char *) &auto_nr.nr_nam, SQLCHAR, 12);
            upd_cursor_2 = sqlcursor (sqltext);

            sqlin ((short *) &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *) &auto_nr.fil, SQLSHORT, 0);
            sqlin ((char *) &auto_nr.nr_nam, SQLCHAR, 12);
            test_upd_cursor_2 = sqlcursor (_T("select nr_nr from auto_nr ")
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   nr_nam = ? ")
                                  _T("and   satz_kng  = 2"));

            sqlin ((short *) &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *) &auto_nr.fil, SQLSHORT, 0);
            sqlin ((char *) &auto_nr.nr_nam, SQLCHAR, 12);
            sqlin ((long *) &auto_nr.nr_nr, SQLLONG, 0);
            sqlin ((short *) &auto_nr.satz_kng, SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from auto_nr ")
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   nr_nam = ? ")
                                  _T("and   nr_nr = ? ")
                                  _T("and   satz_kng  = ?"));

            sqlin ((short *) &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *) &auto_nr.fil, SQLSHORT, 0);
            sqlin ((char *) &auto_nr.nr_nam, SQLCHAR, 12);
            cursor_del_nr = sqlcursor (_T("delete from auto_nr ")
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and   nr_nam = ?"));

    sqlin ((short *) &auto_nr.mdn,SQLSHORT,0);
    sqlin ((short *) &auto_nr.fil,SQLSHORT,0);
    sqlin ((TCHAR *) auto_nr.nr_nam,SQLCHAR,11);
    sqlin ((long *) &auto_nr.nr_nr,SQLLONG,0);
    sqlin ((short *) &auto_nr.satz_kng,SQLSHORT,0);
    sqlin ((long *) &auto_nr.max_wert,SQLLONG,0);
    sqlin ((TCHAR *) auto_nr.nr_char,SQLCHAR,11);
    sqlin ((long *) &auto_nr.nr_char_lng,SQLLONG,0);
    sqlin ((TCHAR *) auto_nr.fest_teil,SQLCHAR,11);
    sqlin ((TCHAR *) auto_nr.nr_komb,SQLCHAR,21);
    sqlin ((short *) &auto_nr.delstatus,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into auto_nr (")
_T("mdn,  fil,  nr_nam,  nr_nr,  satz_kng,  max_wert,  nr_char,  nr_char_lng,  fest_teil,  ")
_T("nr_komb,  delstatus) ")

#line 113 "auto_nr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?)")); 

#line 115 "auto_nr.rpp"
}
int AUTO_NR_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int AUTO_NR_CLASS::dbreadfirst_0 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (cursor_0 == -1)
         {  
                this->prepare ();
         }
         sqlopen (cursor_0);
         return sqlfetch (cursor_0);
}

int AUTO_NR_CLASS::dbread_0 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         return sqlfetch (cursor_0);
}


int AUTO_NR_CLASS::dbreadfirst_1 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (cursor_1 == -1)
         {  
                this->prepare ();
         }
         sqlopen (cursor_1);
         return sqlfetch (cursor_1);
}

int AUTO_NR_CLASS::dbread_1 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         return sqlfetch (cursor_1);
}

int AUTO_NR_CLASS::dbreadfirst_2 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (cursor_2 == -1)
         {  
                this->prepare ();
         }
         sqlopen (cursor_2);
         return sqlfetch (cursor_2);
}

int AUTO_NR_CLASS::dbread_2 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         return sqlfetch (cursor_2);
}

int AUTO_NR_CLASS::dbupdate_2 (void)
/**
Eintrag fuer nr_nam loeschen.
**/
{
         sqlopen (test_upd_cursor_2); 
         dsqlstatus = sqlfetch (test_upd_cursor_2); 
         if (dsqlstatus == 100)
         {
                   sqlexecute (ins_cursor);
         }  
         else if (dsqlstatus == 0)
         {
                   sqlexecute (upd_cursor_2);
         }  
         return 0;
}



int AUTO_NR_CLASS::dbdelete_nr_nam (void)
/**
Eintrag fuer nr_nam loeschen.
**/
{
         if (cursor_del_nr == -1)
         {  
                this->prepare ();
         }
         return sqlexecute (cursor_del_nr);
}