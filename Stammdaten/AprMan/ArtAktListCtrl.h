#pragma once
#include <vector>
#include "editlistctrl.h"
#include "mdn.h"
#include "fil.h"
#include "gr_zuord.h"
#include "Adr.h"
#include "a_pr.h"
#include "akt_krza.h"
#include "ChoiceMdn.h"
#include "Choicefil.h"
#include "ChoiceGrZuord.h"
#include "A_bas.h"

#define MAXLISTROWS 30

class CArtAktListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
	BOOL RowDeleted;
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
	};

	enum LISTMODE
	{
		Large = 0,
		Compact = 1,
	};

	enum
	{
		LargeWidth = 120,
		CompactWidth = 40,
	};

	enum LISTPOS
	{
		POSMDNGR     = 1,
		POSMDN       = 2,
		POSFILGR     = 3,
		POSFIL       = 4,
		POSAKTVON    = 5,
		POSAKTBIS    = 6,
		POSPREK      = 7,
		POSPRVK      = 8,
		POSPREKN     = 9,
		POSPRVKN     = 10,
		POSPREKAB   =  11,
		POSPRVKAB   =  12,
		POSAKT       = 13,
	};

	BOOL OldMode;
	int ListMode;
	int CompanyWidth;

	int PosMdnGr;
    int PosMdn;
	int PosFilGr;
	int PosFil;
	int PosAktVon;
	int PosAktBis;
	int PosPrEk;
	int PosPrVk;
	int PosPrEkN;
	int PosPrVkN;
	int PosPrEkAb;
	int PosPrVkAb;
	int PosAkt;
	int scale;

    int *Position[14];

	BOOL ShowAbverk;
	void SetShowAbverk (BOOL ShowAbverk)
	{
		this->ShowAbverk = ShowAbverk;
	}

	BOOL GetShowAbverk ()
	{
		return ShowAbverk;
	}

	BOOL EnterBasis;
	void SetEnterBasis (BOOL EnterBasis)
	{
		this->EnterBasis = EnterBasis;
	}

	BOOL GetEnterBasis ()
	{
		return EnterBasis;
	}

	short m_Mdn;
	int FilGrCursor;
	int FilCursor;
	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	int oldsel;
	std::vector<BOOL> vSelect;
	CVector MdnGrCombo;
	CVector MdnCombo;
	CVector FilGrCombo;
	CVector FilCombo;
	CChoiceGrZuord *ChoiceMdnGr;
	BOOL ModalChoiceMdnGr;
	BOOL MdnGrChoiceStat;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	BOOL MdnChoiceStat;
	CChoiceGrZuord *ChoiceFilGr;
	BOOL ModalChoiceFilGr;
	BOOL FilGrChoiceStat;
	CChoiceFil *ChoiceFil;
	BOOL ModalChoiceFil;
	BOOL FilChoiceStat;
	CVector ListRows;
	CTime AktDateFrom;
	CTime AktDateTo;

	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	FIL_CLASS Fil;
	GR_ZUORD_CLASS Gr_zuord;
	ADR_CLASS FilAdr;
	APR_CLASS A_pr;
	AKT_KRZA_CLASS Akt_krz;
	A_BAS_CLASS *A_bas;
	void SetABas (A_BAS_CLASS *A_bas)
	{
		this->A_bas = A_bas;
	}
	CArtAktListCtrl(void);
	~CArtAktListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	void FillMdnGrCombo (CVector&);
	void FillMdnCombo (CVector&);
	void FillFilGrCombo (CVector&);
	void FillFilCombo (CVector&);
	void OnChoice ();
	void OnMdnGrChoice (CString &);
	void OnMdnChoice (CString &);
	void OnFilGrChoice (CString &);
	void OnFilChoice (CString &);
	void OnKey9 ();
    void ReadMdnGr ();
    void ReadMdn ();
    void ReadFilGr ();
    void ReadFil ();
    void FillFilGrCombo (int row);
    void FillFilCombo (int row);
    void ReadFilCombo (int mdn);
    void ReadFilGrCombo (int mdn);
    void GetColValue (int row, int col, CString& Text);
    void TestAprIndex ();
    BOOL TestAprIndexM (int EditRow);
	void ScrollPositions (int pos);
	BOOL LastCol ();
	void Prepare ();
	BOOL TestAktVon ();
	BOOL TestAktVon (int Row);
	BOOL TestAktBis ();
	BOOL TestAktBis (int Row);
	BOOL TestLadAkv ();
    void ReadApr ();
	void SetListMode (int ListMode);
	void CalculateAbverk ();
};
