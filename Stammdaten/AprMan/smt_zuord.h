#ifndef _SMT_ZUORD_DEF
#define _SMT_ZUORD_DEF

#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct SMT_ZUORD {
   short          fil;
   short          hwg;
   short          mdn;
   short          verk_st;
   DATE_STRUCT    zuord_dat;
   short          fil_gr;
   short          wg;
   short          ag;
};
extern struct SMT_ZUORD smt_zuord, smt_zuord_null;

#line 9 "smt_zuord.rh"

class SMT_ZUORD_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SMT_ZUORD smt_zuord;  
               SMT_ZUORD_CLASS () : DB_CLASS ()
               {
               }
};
#endif
