// ListPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "AprMan.h"
#include "ListPage.h"
#include "Util.h"
#include "StrFuncs.h"


// CListPage-Dialogfeld

HANDLE CListPage::Write160Lib = NULL;

IMPLEMENT_DYNAMIC(CListPage, CDialog)

CListPage::CListPage(CWnd* pParent /*=NULL*/)
	: CDialog(CListPage::IDD, pParent)
{
	Cfg.SetProgName( _T("AprMan"));
	CellHeight = 0;
	m_List.ArtSelect = "";
    dbild160 = NULL;
	dOpenDbase = NULL;
	Write160 = TRUE;
	MdnGrCursor = -1;
	MdnCursor = -1;
	FilCursor = -1;
}

CListPage::~CListPage()
{
	DestroyRows (DbRows);
	DestroyRows (ListRows);
}

void CListPage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_List);
}


BEGIN_MESSAGE_MAP(CListPage, CDialog)
	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CListPage-Meldungshandler

BOOL CListPage::OnInitDialog ()
{
	CDialog::OnInitDialog();
	strcpy (sys_par.sys_par_nam, "nachkpreis");
	if (Sys_par.dbreadfirst () == 0)
	{
		m_List.scale = _tstoi (sys_par.sys_par_wrt);
	}

	CUtil::GetPersName (PersName);
	PrProt.Construct (PersName, CString ("11200"), &A_pr, &Akt_krz, &Akt_krza);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	ReadCfg ();


	Etikett.ErfKz = _T("P");
	Etikett.A_bas = &A_bas;
	strcpy (sys_par.sys_par_nam, "reg_eti_par");
	if (Sys_par.dbreadfirst () == 0)
	{
		Etikett.Mode = _tstoi (sys_par.sys_par_wrt);
	}

	if (Write160 && Write160Lib == NULL)
	{
		CString bws;
		BOOL ret = bws.GetEnvironmentVariable (_T("bws"));
		CString W160Dll;
		if (ret)
		{
			W160Dll.Format (_T("%s\\bin\\bild160dll.dll"), bws.GetBuffer ());
		}
		else
		{
			W160Dll = _T("bild160dll.dll");
		}
		Write160Lib = LoadLibrary (W160Dll.GetBuffer ());
		if (Write160Lib != NULL && dbild160 == NULL)
		{
			dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
			dOpenDbase = (BOOL (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "OpenDbase");
			if (dOpenDbase != NULL)
			{
				(*dOpenDbase) ("bws");
			}
		}
	}
	else if (dbild160 == NULL)
	{
		dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
	}

	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_List.SetImageList (&image, LVSIL_SMALL);   
	}

	Gr_zuord.sqlout ((short *) &Gr_zuord.gr_zuord.gr, SQLSHORT, 0);
	Gr_zuord.sqlout ((char *) Gr_zuord.gr_zuord.gr_bz1, SQLCHAR, sizeof (Gr_zuord.gr_zuord.gr_bz1));
    MdnGrCursor = Gr_zuord.sqlcursor (_T("select gr, gr_bz1 from gr_zuord ")
 									  _T("where mdn = 0 ")
									  _T("and gr > 0 ")
								      _T("order by gr"));

	Mdn.sqlout ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);
	MdnAdr.sqlout ((char *) MdnAdr.adr.adr_krz, SQLCHAR, sizeof (MdnAdr.adr.adr_krz));
    MdnCursor = Mdn.sqlcursor (_T("select mdn.mdn, adr.adr_krz from mdn, adr ")
		                       _T("where mdn.mdn > 0 ")
							   _T("and adr.adr = mdn.adr ")
							   _T("order by mdn.mdn"));

	FillList = m_List;
	FillList.SetStyle (LVS_REPORT);
	if (m_List.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Artikel"), 1, 200, LVCFMT_LEFT);
	FillList.SetCol (_T("Mnd-Gr"), 2, 60, LVCFMT_LEFT);
	FillList.SetCol (_T("Mandant"), 3, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("Fil_Gr"), 4, 60, LVCFMT_LEFT);
	FillList.SetCol (_T("Filiale"), 5, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("EK"), 6, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("VK"), 7, 100, LVCFMT_RIGHT);
	FillList.SetCol (_T("EK kalk."), 8, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("VK kalk."), 9, 100, LVCFMT_RIGHT);
	FillList.SetCol (_T("Selbstkosten"), 10, 100, LVCFMT_RIGHT);
	FillList.SetCol (_T("Spanne"), 11, 100, LVCFMT_RIGHT);

	m_List.AddListChangeHandler (this);

// Hauptgrid
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (20, 8);

	CCtrlInfo *c_List     = new CCtrlInfo (&m_List, 0, 0, DOCKRIGHT, DOCKBOTTOM); 
	c_List->rightspace = 12;
	CtrlGrid.Add (c_List);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	FillMdnGrCombo ();
	FillMdnCombo ();
	m_List.Prepare ();

	CtrlGrid.Display ();

	return FALSE;
}

BOOL CListPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				m_List.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				m_List.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_F5)
			{
				StopEnter ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				m_List.DeleteRow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
				m_List.OnKey9 ();
				return TRUE;
			}
	}
    return CDialog::PreTranslateMessage(pMsg);
}

void CListPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

void CListPage::Read (CString& Select, CString& CmpSelect)
{
	CString Sql;
	StopEnter ();
	m_List.ArtSelect = Select;
	m_List.StopEnter ();
	m_List.DeleteAllItems ();
	m_List.vSelect.clear ();
	m_List.ListRows.Init ();
	DestroyRows (DbRows);
	int i = 0;
	m_List.EnableWindow ();
/*
	m_List.m_Mdn = Mdn.mdn.mdn;
	if (m_List.m_Mdn != 0)
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, 0);
		m_List.SetColumnWidth (m_List.PosMdn, 0);
	}
	else
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, 120);
		m_List.SetColumnWidth (m_List.PosMdn, 120);
	}
*/

	Sql = _T("select a_pr.a, a_pr.mdn_gr, a_pr.mdn, a_pr.fil_gr, a_pr.fil from a_pr, a_bas ")
		  _T("where a_pr.a > 0 and a_bas.a = a_pr.a");
	Sql += _T( " ");
	if (CmpSelect.GetLength () > 0)
	{
		Sql += CmpSelect;
		Sql += _T( " ");
	}
	if (Select.GetLength () > 0)
	{
		Sql += Select;
		Sql += _T( " ");
	}

	Sql += _T("order by a_pr.a, a_pr.mdn_gr, a_pr.mdn, a_pr.fil_gr, a_pr.fil");
    A_pr.sqlout ((double *) &A_pr.a_pr.a, SQLDOUBLE, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.mdn_gr, SQLSHORT, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.mdn, SQLSHORT, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.fil_gr, SQLSHORT, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.fil, SQLSHORT, 0);  
    AprCursor = A_pr.sqlcursor (Sql.GetBuffer ());

	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	A_pr.a_pr.a   = A_bas.a_bas.a;
	A_pr.sqlopen (AprCursor);
	while (A_pr.sqlfetch (AprCursor) == 0)
	{
		if (m_List.m_Mdn != 0 && A_pr.a_pr.mdn != m_List.m_Mdn)
		{
			continue;
		}
		A_pr.dbreadfirst ();
		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		Mdn.mdn.mdn = A_pr.a_pr.mdn;
		if (Mdn.dbreadfirst () == 0)
		{
			MdnAdr.adr.adr = Mdn.mdn.adr;
			MdnAdr.dbreadfirst ();
		}
		Fil.fil.mdn = A_pr.a_pr.mdn;
		Fil.fil.fil = A_pr.a_pr.fil;
		if (Fil.dbreadfirst () == 0)
		{
			FilAdr.adr.adr = Fil.fil.adr;
			FilAdr.dbreadfirst ();
		}
		FillList.InsertItem (i, 0);
		A_bas.a_bas.a = A_pr.a_pr.a;
		A_bas.dbreadfirst ();
		CString cA;
		cA.Format (_T("%.0lf %s"), A_bas.a_bas.a, A_bas.a_bas.a_bz1);
		FillList.SetItemText (cA.GetBuffer (), i, m_List.PosA);
		CString cMdnGr;
		cMdnGr.Format (_T("%hd"), A_pr.a_pr.mdn_gr);
		FillList.SetItemText (cMdnGr.GetBuffer (), i, m_List.PosMdnGr);

		CString cMdn;
		if (A_pr.a_pr.mdn == 0)
		{
			cMdn.Format (_T("0  Zentrale"));
		}
		else
		{
			cMdn.Format (_T("%hd  %s"), A_pr.a_pr.mdn,
							MdnAdr.adr.adr_krz);
		}
		FillList.SetItemText (cMdn.GetBuffer (), i, m_List.PosMdn);

		CString cFilGr;
		cFilGr.Format (_T("%hd"), A_pr.a_pr.fil_gr);
		FillList.SetItemText (cFilGr.GetBuffer (), i, m_List.PosFilGr);

		CString cFil;
		if (A_pr.a_pr.fil == 0)
		{
			cFil.Format (_T("0  Mandant"));
		}
		else
		{
			cFil.Format (_T("%hd  %s"), A_pr.a_pr.fil,
							FilAdr.adr.adr_krz);
		}
		FillList.SetItemText (cFil.GetBuffer (), i, m_List.PosFil);

		CString PrEk;
		m_List.DoubleToString (A_pr.a_pr.pr_ek_euro, PrEk, m_List.scale);
		FillList.SetItemText (PrEk.GetBuffer (), i, m_List.PosPrEk);

		CString PrVk;
		m_List.DoubleToString (A_pr.a_pr.pr_vk_euro, PrVk, 2);
		FillList.SetItemText (PrVk.GetBuffer (), i, m_List.PosPrVk);

		if (m_List.GetColumnWidth (m_List.PosPrVk) != 0)
		{

			m_List.sk_vollk = Calculate.GetSk (A_pr.a_pr.mdn, A_pr.a_pr.fil, A_pr.a_pr.a);
			if (m_List.sk_vollk == (double) 0)  m_List.sk_vollk = A_pr.a_pr.pr_ek_euro; //SR-2


			m_List.fil_ek_vollk = Calculate.GetEk ();
			CString EkVollk;
			m_List.DoubleToString (m_List.fil_ek_vollk, EkVollk, 4);
			FillList.SetItemText (EkVollk.GetBuffer (), i, m_List.PosPrEkKalk);

			m_List.fil_vk_vollk = Calculate.GetVk ();
			CString VkVollk;
			m_List.DoubleToString (m_List.fil_vk_vollk, VkVollk, 4);
			FillList.SetItemText (VkVollk.GetBuffer (), i, m_List.PosPrVkKalk);

			CString Sk;
			m_List.DoubleToString (m_List.sk_vollk, Sk, 4);
			FillList.SetItemText (Sk.GetBuffer (), i, m_List.PosSk);

			CString Sp;
			m_List.DoubleToString (m_List.spanne, Sp, 2);
			FillList.SetItemText (Sp.GetBuffer (), i, m_List.PosSp);
			ColChanged (i, m_List.PosSk);
		}

		CAPreise *a_pr = new CAPreise (PrEk, PrVk, A_pr.a_pr);
		DbRows.Add (a_pr);
		m_List.ListRows.Add (a_pr);
		i ++;
	}
	A_pr.sqlclose (AprCursor);
	Mdn.mdn.mdn = m_List.m_Mdn;
	m_List.SetFocus ();
	m_List.FirstEnter ();
}

void CListPage::Read (CString& Select, short mdn_gr, short mdn, short fil_gr, short fil)
{
	CString Sql;
/*
	if (mdn_gr == 0 && mdn == 0)
	{
		Read (Select, CString (_T("")));
		return;
	}
*/

	StopEnter ();
	m_List.ArtSelect = Select;
	m_List.StopEnter ();
	m_List.DeleteAllItems ();
	m_List.vSelect.clear ();
	m_List.ListRows.Init ();
	int i = 0;
	m_List.EnableWindow ();
/*
	m_List.m_Mdn = Mdn.mdn.mdn;
	if (m_List.m_Mdn != 0)
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, 0);
		m_List.SetColumnWidth (m_List.PosMdn, 0);
	}
	else
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, 120);
		m_List.SetColumnWidth (m_List.PosMdn, 120);
	}
*/

	Sql = _T("select distinct (a_pr.a) from a_pr, a_bas ")
		  _T("where a_pr.a > 0 and a_bas.a = a_pr.a");
	Sql += _T( " ");
	if (Select.GetLength () > 0)
	{
		Sql += Select;
		Sql += _T( " ");
	}

	Sql += _T("order by a_pr.a");
    A_pr.sqlout ((double *) &A_pr.a_pr.a, SQLDOUBLE, 0);  
    AprCursor = A_pr.sqlcursor (Sql.GetBuffer ());

	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	A_pr.a_pr.a   = A_bas.a_bas.a;
	A_pr.sqlopen (AprCursor);
	while (A_pr.sqlfetch (AprCursor) == 0)
	{
		A_pr.a_pr.mdn_gr = mdn_gr;
		A_pr.a_pr.mdn    = mdn;
		A_pr.a_pr.fil_gr = fil_gr;
		A_pr.a_pr.fil    = fil;
		if (m_List.m_Mdn != 0 && A_pr.a_pr.mdn != m_List.m_Mdn)
		{
			continue;
		}
		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		Mdn.mdn.mdn = A_pr.a_pr.mdn;
		if (Mdn.dbreadfirst () == 0)
		{
			A_pr.a_pr.mdn_gr = Mdn.mdn.mdn_gr;
			MdnAdr.adr.adr = Mdn.mdn.adr;
			MdnAdr.dbreadfirst ();
		}
		Fil.fil.mdn = A_pr.a_pr.mdn;
		Fil.fil.fil = A_pr.a_pr.fil;
		if (Fil.dbreadfirst () == 0)
		{
			A_pr.a_pr.fil_gr = Fil.fil.fil_gr;
			FilAdr.adr.adr = Fil.fil.adr;
			FilAdr.dbreadfirst ();
		}
		if (A_pr.dbfetchpreis () != NULL) continue;
		FillList.InsertItem (i, 0);
		A_bas.a_bas.a = A_pr.a_pr.a;
		A_bas.dbreadfirst ();
		CString cA;
		cA.Format (_T("%.0lf %s"), A_bas.a_bas.a, A_bas.a_bas.a_bz1);
		FillList.SetItemText (cA.GetBuffer (), i, m_List.PosA);
		CString cMdnGr;
		cMdnGr.Format (_T("%hd"), A_pr.a_pr.mdn_gr);
		FillList.SetItemText (cMdnGr.GetBuffer (), i, m_List.PosMdnGr);

		CString cMdn;
		if (A_pr.a_pr.mdn == 0)
		{
			cMdn.Format (_T("0  Zentrale"));
		}
		else
		{
			cMdn.Format (_T("%hd  %s"), A_pr.a_pr.mdn,
							MdnAdr.adr.adr_krz);
		}
		FillList.SetItemText (cMdn.GetBuffer (), i, m_List.PosMdn);

		CString cFilGr;
		cFilGr.Format (_T("%hd"), A_pr.a_pr.fil_gr);
		FillList.SetItemText (cFilGr.GetBuffer (), i, m_List.PosFilGr);

		CString cFil;
		if (A_pr.a_pr.fil == 0)
		{
			cFil.Format (_T("0  Mandant"));
		}
		else
		{
			cFil.Format (_T("%hd  %s"), A_pr.a_pr.fil,
							FilAdr.adr.adr_krz);
		}
		FillList.SetItemText (cFil.GetBuffer (), i, m_List.PosFil);

		CString PrEk;
		m_List.DoubleToString (A_pr.a_pr.pr_ek_euro, PrEk, m_List.scale);
		FillList.SetItemText (PrEk.GetBuffer (), i, m_List.PosPrEk);

		CString PrVk;
		m_List.DoubleToString (A_pr.a_pr.pr_vk_euro, PrVk, 2);
		FillList.SetItemText (PrVk.GetBuffer (), i, m_List.PosPrVk);

		if (m_List.GetColumnWidth (m_List.PosPrVk) != 0)
		{
			m_List.sk_vollk = Calculate.GetSk (A_pr.a_pr.mdn, A_pr.a_pr.fil, A_pr.a_pr.a);
			if (m_List.sk_vollk == (double) 0)  m_List.sk_vollk = A_pr.a_pr.pr_ek_euro; //SR-2


			m_List.fil_ek_vollk = Calculate.GetEk ();
			CString EkVollk;
			m_List.DoubleToString (m_List.fil_ek_vollk, EkVollk, 4);
			FillList.SetItemText (EkVollk.GetBuffer (), i, m_List.PosPrEkKalk);

			m_List.fil_vk_vollk = Calculate.GetVk ();
			CString VkVollk;
			m_List.DoubleToString (m_List.fil_vk_vollk, VkVollk, 4);
			FillList.SetItemText (VkVollk.GetBuffer (), i, m_List.PosPrVkKalk);


			CString Sk;
			m_List.DoubleToString (m_List.sk_vollk, Sk, 4);
			FillList.SetItemText (Sk.GetBuffer (), i, m_List.PosSk);

			CString Sp;
			m_List.DoubleToString (m_List.spanne, Sp, 2);
			FillList.SetItemText (Sp.GetBuffer (), i, m_List.PosSp);
			ColChanged (i, m_List.PosSk);
		}

		CAPreise *a_pr = new CAPreise (PrEk, PrVk, A_pr.a_pr);
		DbRows.Add (a_pr);
		m_List.ListRows.Add (a_pr);
		i ++;
	}
	Mdn.mdn.mdn = m_List.m_Mdn;
	m_List.SetFocus ();
	m_List.FirstEnter ();
}

void CListPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_List.MaxComboEntries = atoi (cfg_v);
    }
/*
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_List.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_List.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_List.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CListPage::StopEnter ()
{
		m_List.StopEnter ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
		m_List.DeleteAllItems ();
		m_List.EnableWindow (FALSE);
}

void CListPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CAPreise *ipr;
	while ((ipr = (CAPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

void CListPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CAPreise *a_pr;
	while ((a_pr = (CAPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&A_pr.a_pr, &a_pr->a_pr, sizeof (A_PR));
		if (!InList (A_pr))
		{
			A_pr.dbdelete ();
//			PgrProt.Write (1);
		}
	}
}


BOOL CListPage::Write ()
{
	extern short sql_mode;
	short sql_s;
	if (!m_List.IsWindowEnabled ())
	{
		return FALSE;
	}

// Pr�fen auf doppelte Eintr�ge 

	m_List.StopEnter ();
	int count = m_List.GetItemCount ();
/*
	for (int i = 0; i < count; i ++)
	{
		if (!m_List.TestAprIndexM (i))
		{
			MessageBox (_T("Die Daten k�nnen nicht gespeichert werden!\n")
				        _T("Es sind mindestens 2 Eintr�ge auf gleicher Unternehmesebene vorhanden"),
						NULL,
						MB_OK | MB_ICONERROR);
			return FALSE;
		}
	}
*/

	sql_s = sql_mode;
	sql_mode = 1;
	A_pr.beginwork ();
	m_List.StopEnter ();
	count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 A_PR *a_pr = new A_PR;
		 memcpy (a_pr, &a_pr_null, sizeof (A_PR));
         CString Text;
		 Text = m_List.GetItemText (i, m_List.PosA);
		 a_pr->a = CStrFuncs::StrToDouble (Text);
		 Text = m_List.GetItemText (i, m_List.PosMdnGr);
		 a_pr->mdn_gr = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosMdn);
		 a_pr->mdn = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosFilGr);
		 a_pr->fil_gr = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosFil);
		 a_pr->fil = atoi (Text);
     
     	 CString PrEk =  m_List.GetItemText (i, m_List.PosPrEk);
		 a_pr->pr_ek_euro = CStrFuncs::StrToDouble (PrEk);
		 a_pr->pr_ek = a_pr->pr_ek_euro;
		 CString PrVk =  m_List.GetItemText (i, m_List.PosPrVk);
		 a_pr->pr_vk_euro = CStrFuncs::StrToDouble (PrVk);
		 a_pr->pr_vk = a_pr->pr_vk_euro;

		 if (!TestDecValues (a_pr)) 
		 {
			 MessageBox (_T("Maximalwert f�r Preisfeld �berschritten"));
			 return FALSE;
		 }

		 CAPreise *pr = new CAPreise (PrEk, PrVk, *a_pr);
//		 if (a_pr->pr_ek_euro != 0.0 || 
//			 a_pr->pr_vk_euro != 0.0)
		 {
				ListRows.Add (pr);
		 }
		 delete a_pr;
	}

	DeleteDbRows ();

	Etikett.Clear ();
	ListRows.FirstPosition ();
	CAPreise *a_pr;
	while ((a_pr = (CAPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&A_pr.a_pr, &a_pr->a_pr, sizeof (A_PR));
		CString Date;
		CStrFuncs::SysDate (Date);
		A_pr.ToDbDate (Date, &A_pr.a_pr.bearb);
		A_pr.a_pr.akt = -1;
		A_pr.dbreadfirst ();
		A_pr.a_pr.key_typ_dec13 = A_pr.a_pr.a;
		A_pr.a_pr.key_typ_sint = A_pr.a_pr.mdn;
		A_pr.a_pr.pr_ek_euro = a_pr->a_pr.pr_ek_euro;
		A_pr.a_pr.pr_ek = a_pr->a_pr.pr_ek;
		A_pr.a_pr.pr_vk_euro = a_pr->a_pr.pr_vk_euro;
		A_pr.a_pr.pr_vk = a_pr->a_pr.pr_vk;
		CAPreise old_a_pr;
		A_PR AprWrite;
		memcpy (&AprWrite, &A_pr.a_pr, sizeof (A_PR));
		if (IsChanged (a_pr, &old_a_pr))
		{
			memcpy (&A_pr.a_pr, &AprWrite, sizeof (A_PR));
			A_pr.dbupdate ();
			PrProt.Write (0 ,old_a_pr.a_pr.pr_ek_euro, old_a_pr.a_pr.pr_vk_euro);
			if (Write160 && dbild160 != NULL)
			{
				CStringA Command;
				Command.Format ("bild160 B %.0lf %hd %hd %hd %hd",
					             A_pr.a_pr.a,
								 A_pr.a_pr.mdn_gr,
								 A_pr.a_pr.mdn,
								 A_pr.a_pr.fil_gr,
								 A_pr.a_pr.fil); 

				(*dbild160) (Command.GetBuffer ());
			}
			A_bas.a_bas.a = A_pr.a_pr.a;
			A_bas.dbreadfirst ();
			Etikett.WritePr (A_pr.a_pr.mdn, A_pr.a_pr.fil, A_pr.a_pr.pr_vk_euro);
		}
	}
	Etikett.TestPrint ();
	A_pr.commitwork ();
	sql_mode = sql_s;
	StopEnter ();
	return TRUE;
}

BOOL CListPage::TestWrite ()
{
	if (!m_List.IsWindowEnabled ())
	{
		return FALSE;
	}

// Pr�fen auf doppelte Eintr�ge 

	m_List.GetEnter ();
	int count = m_List.GetItemCount ();
	count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 A_PR *a_pr = new A_PR;
		 memcpy (a_pr, &a_pr_null, sizeof (A_PR));
         CString Text;
		 Text = m_List.GetItemText (i, m_List.PosA);
		 a_pr->a = CStrFuncs::StrToDouble (Text);
		 Text = m_List.GetItemText (i, m_List.PosMdnGr);
		 a_pr->mdn_gr = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosMdn);
		 a_pr->mdn = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosFilGr);
		 a_pr->fil_gr = atoi (Text);
		 Text = m_List.GetItemText (i, m_List.PosFil);
		 a_pr->fil = atoi (Text);
     
     	 CString PrEk =  m_List.GetItemText (i, m_List.PosPrEk);
		 a_pr->pr_ek_euro = CStrFuncs::StrToDouble (PrEk);
		 a_pr->pr_ek = a_pr->pr_ek_euro;
		 CString PrVk =  m_List.GetItemText (i, m_List.PosPrVk);
		 a_pr->pr_vk_euro = CStrFuncs::StrToDouble (PrVk);
		 a_pr->pr_vk = a_pr->pr_vk_euro;

		 CAPreise *pr = new CAPreise (PrEk, PrVk, *a_pr);
		 if (a_pr->pr_ek_euro != 0.0 || 
			 a_pr->pr_vk_euro != 0.0)
		 {
				ListRows.Add (pr);
		 }
		 delete a_pr;
	}

//	DeleteDbRows ();

	ListRows.FirstPosition ();
	CAPreise *a_pr;
	while ((a_pr = (CAPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&A_pr.a_pr, &a_pr->a_pr, sizeof (A_PR));
		CString Date;
		CStrFuncs::SysDate (Date);
		A_pr.ToDbDate (Date, &A_pr.a_pr.bearb);
		A_pr.a_pr.akt = -1;
		A_pr.dbreadfirst ();
		A_pr.a_pr.key_typ_dec13 = A_pr.a_pr.a;
		A_pr.a_pr.key_typ_sint = A_pr.a_pr.mdn;
		A_pr.a_pr.pr_ek_euro = a_pr->a_pr.pr_ek_euro;
		A_pr.a_pr.pr_ek = a_pr->a_pr.pr_ek;
		A_pr.a_pr.pr_vk_euro = a_pr->a_pr.pr_vk_euro;
		A_pr.a_pr.pr_vk = a_pr->a_pr.pr_vk;
		CAPreise old_a_pr;
		if (IsChanged (a_pr, &old_a_pr))
		{
			return TRUE;
		}
	}
	return FALSE;
}


BOOL CListPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (!m_List.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Alle Eintr�ge l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	A_pr.beginwork ();
	m_List.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	m_List.DeleteAllItems ();

	A_pr.commitwork ();
	sql_mode = sql_s;
	StopEnter ();
	return TRUE;
}

BOOL CListPage::IsChanged (CAPreise *pApr, CAPreise *old_a_pr)
{
	DbRows.FirstPosition ();
	CAPreise *a_pr;
	while ((a_pr = (CAPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&A_pr.a_pr, &pApr->a_pr, sizeof (A_PR));
		if (A_pr == a_pr->a_pr) break;
	} 	
	if (a_pr == NULL)
	{
		old_a_pr->a_pr.pr_ek_euro = 0.0;
		old_a_pr->a_pr.pr_vk_euro = 0.0;
		return TRUE;
	}
	memcpy (&old_a_pr->a_pr,  &a_pr->a_pr, sizeof (A_PR));
	if (pApr->cEk != a_pr->cEk) return TRUE;
	if (pApr->cVk != a_pr->cVk) return TRUE;
	return FALSE;
}

BOOL CListPage::InList (APR_CLASS& A_pr)
{
	ListRows.FirstPosition ();
	CAPreise *a_pr;
	while ((a_pr = (CAPreise *) ListRows.GetNext ()) != NULL)
	{
		if (A_pr == a_pr->a_pr) return TRUE;
	}
    return FALSE;
}

BOOL CListPage::TestDecValues (A_PR *a_pr)
{
	if (a_pr->pr_ek_euro > 9999.9999 ||
		a_pr->pr_vk_euro < -9999.9999)
	{
		return FALSE;
	}

	if (a_pr->pr_ek_euro > 9999.99 ||
		a_pr->pr_vk_euro < -9999.99)
	{
		return FALSE;
	}
    
	return TRUE;
}

void CListPage::SetMdn (short mdn)
{
	m_List.m_Mdn = mdn;
	if (m_List.m_Mdn != 0)
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, 0);
		m_List.SetColumnWidth (m_List.PosMdn, 0);
	}
	else
	{
		m_List.SetColumnWidth (m_List.PosMdnGr, 60);
		m_List.SetColumnWidth (m_List.PosMdn, 120);
	}
}

void CListPage::SetPrVkVisible (BOOL visible)
{
	if (visible)
	{
		m_List.SetColumnWidth (m_List.PosPrVk, 100);
		m_List.SetColumnWidth (m_List.PosPrEkKalk, 100);
		m_List.SetColumnWidth (m_List.PosPrVkKalk, 100);
		m_List.SetColumnWidth (m_List.PosSk, 100);
		m_List.SetColumnWidth (m_List.PosSp, 100);
	}
	else
	{
		m_List.SetColumnWidth (m_List.PosPrVk, 0);
		m_List.SetColumnWidth (m_List.PosPrEkKalk, 0);
		m_List.SetColumnWidth (m_List.PosPrVkKalk, 0);
		m_List.SetColumnWidth (m_List.PosSk, 0);
		m_List.SetColumnWidth (m_List.PosSp, 0);
	}
}

void CListPage::FillMdnGrCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0"));
	Values.Add (Value);
    Mdn.sqlopen (MdnGrCursor);
	while (Mdn.sqlfetch (MdnGrCursor) == 0)
	{
		Gr_zuord.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Gr_zuord.gr_zuord.gr,
			                          Gr_zuord.gr_zuord.gr_bz1);
		Values.Add (Value);
	}
	m_List.FillMdnGrCombo (Values);
}


void CListPage::FillMdnCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Zentrale"));
	Values.Add (Value);
    Mdn.sqlopen (MdnCursor);
	while (Mdn.sqlfetch (MdnCursor) == 0)
	{
		Mdn.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Mdn.mdn.mdn,
			                          MdnAdr.adr.adr_krz);
		Values.Add (Value);
	}
	m_List.FillMdnCombo (Values);
}


void CListPage::FillFilCombo ()
{
	CVector Values;
	Values.Init ();
	CString *Value = new CString ();
	Value->Format (_T("0 Mandant"));
	Values.Add (Value);
    Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		Fil.dbreadfirst ();
		Value = new CString ();
		Value->Format (_T("%hd  %s"), Fil.fil.fil,
			                          FilAdr.adr.adr_krz);
		Values.Add (Value);
	}
	m_List.FillFilCombo (Values);
}

void CListPage::RowChanged (int NewRow)
{
}

void CListPage::ColChanged (int Row, int Col)
{
	if (Col == m_List.PosPrVk ||
		Col == m_List.PosSk)
	{
		 Calculate.SetMwst (19.0);
     	 CString A =  m_List.GetItemText (Row, m_List.PosA);
		 A_bas.a_bas.a = CStrFuncs::StrToDouble (A);
		 if (A_bas.dbreadfirst () == 0)
		 {
			 _tcscpy (Ptabn.ptabn.ptitem, _T("mwst"));
			 _stprintf (Ptabn.ptabn.ptwert, _T("%d"), A_bas.a_bas.mwst);
			 if (Ptabn.dbreadfirst () == 0)
			 {
				 double mwst = CStrFuncs::StrToDouble (Ptabn.ptabn.ptwer1) * 100;
				 Calculate.SetMwst (mwst);
			 }
		 }

     	 CString PrVk =  m_List.GetItemText (Row, m_List.PosPrVk);
		 double pr_vk = CStrFuncs::StrToDouble (PrVk);
		 CString Sk =  m_List.GetItemText (Row, m_List.PosSk);
		 double sk = CStrFuncs::StrToDouble (Sk);
		 double spanne = Calculate.execute (sk, pr_vk);
		 CString Sp;
		 Sp.Format (_T("%.2lf"), spanne);
	 	 FillList.SetItemText (Sp.GetBuffer (), Row, m_List.PosSp);
	}
}
