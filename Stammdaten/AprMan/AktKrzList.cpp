#include "StdAfx.h"
#include "AktKrzList.h"

CAktKrzList::CAktKrzList(void)
{
	mdn_gr = 0;
	mdn = 0;
	fil_gr = 0;
	fil = 0;
	anzahl = 0;
	zus_bz = _T("");
}

CAktKrzList::CAktKrzList(short mdn_gr, short mdn, short fil_gr, short fil, LPTSTR zus_bz)
{
	this->mdn_gr    = mdn_gr;
	this->mdn    = mdn;
	this->fil_gr    = fil_gr;
	this->fil    = fil;
	this->zus_bz = zus_bz;
}

CAktKrzList::CAktKrzList(short mdn_gr, short mdn, short fil_gr, short fil, LPTSTR zus_bz, DATE_STRUCT lad_akt_von, short anzahl)
{
	this->mdn_gr    = mdn_gr;
	this->mdn    = mdn;
	this->fil_gr    = fil_gr;
	this->fil    = fil;
	this->zus_bz = zus_bz;
	this->lad_akt_von = lad_akt_von;
	this->anzahl = anzahl;
}

CAktKrzList::~CAktKrzList(void)
{
}
