// CompanyPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#ifdef ARTIKEL
#include "Artikel.h"
#else
#include "AprMan.h"
#endif
#include "CompanyPage.h"
#include "DbUniCode.h"
#include "UniFormField.h"
#include "ChoicePtab.h"
#include "Util.h"
#include "Process.h"
#include "Price.h"


// CCompanyPage-Dialogfeld

IMPLEMENT_DYNAMIC(CCompanyPage, CDbPropertyPage)
CCompanyPage::CCompanyPage()
	: CDbPropertyPage(CCompanyPage::IDD)
{
	Frame = NULL;
	ChoiceHWG = NULL;
	ChoiceWG = NULL;
	ChoiceAG = NULL;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceHWG = TRUE;
	ModalChoiceWG = TRUE;
	ModalChoiceAG = TRUE;
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
	Cfg.SetProgName( _T("AprMan"));
	MdnGrCursor = -1;
	MdnCursor = -1;
	FilGrCursor = -1;
	FilCursor = -1;
	ListType = Preise;
	EkFilGr = 99;
}

CCompanyPage::~CCompanyPage()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}

	if (MdnGrCursor != -1)
	{
		A_pr.sqlclose (MdnGrCursor);
		MdnGrCursor = -1;
	}

	if (MdnCursor != -1)
	{
		A_pr.sqlclose (MdnCursor);
		MdnCursor = -1;
	}

    if (FilGrCursor != -1)
	{
		A_pr.sqlclose (FilGrCursor);
		FilGrCursor = -1;
	}
	if (FilCursor != -1)
	{
		A_pr.sqlclose (FilCursor);
		FilCursor = -1;
	}
}

void CCompanyPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN_GR, m_LMdnGr);
	DDX_Control(pDX, IDC_MDN_GR, m_MdnGr);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_LFIL_GR, m_LFilGr);
	DDX_Control(pDX, IDC_FIL_GR, m_FilGr);
	DDX_Control(pDX, IDC_LFIL, m_LFil);
	DDX_Control(pDX, IDC_FIL, m_Fil);
	DDX_Control(pDX, IDC_LLIST_TYP, m_LListTyp);
	DDX_Control(pDX, IDC_LIST_TYP, m_ListTyp);
	DDX_Control(pDX, IDC_LHWG, m_LHwg);
	DDX_Control(pDX, IDC_HWG, m_Hwg);
	DDX_Control(pDX, IDC_LWG, m_LWg);
	DDX_Control(pDX, IDC_WG, m_Wg);
	DDX_Control(pDX, IDC_LAG, m_LAg);
	DDX_Control(pDX, IDC_AG, m_Ag);
	DDX_Control(pDX, IDC_LTEIL_SMT, m_LTeilSmt);
	DDX_Control(pDX, IDC_TEIL_SMT, m_TeilSmt);
	DDX_Control(pDX, IDC_LA_TYP, m_LATyp);
	DDX_Control(pDX, IDC_A_TYP, m_ATyp);
	DDX_Control(pDX, IDC_CMP_GROUP, m_CmpGroup);
	DDX_Control(pDX, IDC_GR_GROUP, m_GrGroup);
	DDX_Control(pDX, IDC_SORTIMENT_GROUP, m_SmtGroup);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_PR_DIFF, m_PrDiff);
	DDX_Control(pDX, IDC_LMDN_USER, m_LMdnUser);
	DDX_Control(pDX, IDC_MDN_USER, m_MdnUser);
	DDX_Control(pDX, IDC_MDN_ADR_KRZ, m_MdnAdrKrz);
}


BEGIN_MESSAGE_MAP(CCompanyPage, CDbPropertyPage)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_CBN_SELCHANGE(IDC_MDN, OnCbnSelchangeMdn)
	ON_BN_CLICKED(IDC_MDNUSERCHOICE , OnMdnchoice)
	ON_BN_CLICKED(IDC_HWGCHOICE , OnHwgChoice)
	ON_BN_CLICKED(IDC_WGCHOICE , OnWgChoice)
	ON_BN_CLICKED(IDC_AGCHOICE , OnAgChoice)
	ON_BN_CLICKED(IDC_TEILSMTCHOICE , OnTeilSmtChoice)
	ON_BN_CLICKED(IDC_ATYPCHOICE , OnATypChoice)
	ON_BN_CLICKED(IDC_PROPEN, OnOpen)
	ON_BN_CLICKED(IDC_PRPRINT, OnPrint)
	ON_EN_CHANGE(IDC_MDN_USER, OnEnChangeMdnUser)
	ON_CBN_SELCHANGE(IDC_FIL, OnCbnSelchangeFil)
END_MESSAGE_MAP()


// CCompanyPage-Meldungshandler

BOOL CCompanyPage::OnInitDialog ()
{
	CPropertyPage::OnInitDialog();

	A_bas.opendbase (_T("bws"));
	strcpy (sys_par.sys_par_nam, "nachkpreis");
	if (Sys_par.dbreadfirst () == 0)
	{
		m_List.scale = _tstoi (sys_par.sys_par_wrt);
	}

	CUtil::GetPersName (PersName);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
//		lFont.CreatePointFont (85, _T("Courier"));
		lFont.CreatePointFont (95, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
//		lFont.CreatePointFont (95, _T("Courier New"));
		lFont.CreatePointFont (120, _T("Dlg"));
	}

	ReadCfg ();

    m_Open.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 60, 48),
		                          this, IDC_PROPEN);
	m_Open.LoadBitmap (IDB_OPEN);
	m_Open.LoadMask (IDB_OPEN_MASK);
	m_Open.SetToolTip (_T("Preise einlesen    F6"));
	m_Open.Tooltip.WindowOrientation = m_Open.Tooltip.Center;

    m_Print.Create (_T("Drucken"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 60, 48),
		                          this, IDC_PRPRINT);
	m_Print.LoadBitmap (IDB_PRINT);
	m_Print.LoadMask (IDB_PRINT_MASK);
	m_Print.SetToolTip (_T("Preise drucken"));
	m_Print.Tooltip.WindowOrientation = m_Print.Tooltip.Center;

	PrDiff = 1;

	A_pr.sqlout ((short *)&gr,      SQLSHORT, 0);
	A_pr.sqlout ((LPTSTR)  gr_bz1,   SQLCHAR, sizeof (gr_bz1));
	MdnGrCursor = A_pr.sqlcursor (_T("select gr, gr_bz1 ")
			                           _T("from gr_zuord ")
									   _T("where mdn = 0 ")
									   _T("and gr > 0 order by gr"));

	A_pr.sqlout ((short *)&mdn,      SQLSHORT, 0);
	A_pr.sqlout ((LPTSTR) adr_krz,   SQLCHAR, sizeof (adr_krz));
	MdnCursor = A_pr.sqlcursor (_T("select mdn.mdn, adr.adr_krz ")
			                       _T("from mdn, adr ")
								   _T("where mdn.mdn > 0 ")
								   _T("and adr.adr = mdn.adr ")
								   _T("order by mdn.mdn"));

	A_pr.sqlin ((short *)&Mdn,      SQLSHORT, 0);
	A_pr.sqlout ((short *)&gr,      SQLSHORT, 0);
	A_pr.sqlout ((LPTSTR)  gr_bz1,   SQLCHAR, sizeof (gr_bz1));
	FilGrCursor = A_pr.sqlcursor (_T("select gr, gr_bz1 ")
		                           _T("from gr_zuord ")
								   _T("where mdn = ? ")
								   _T("and gr > 0 order by gr"));

	A_pr.sqlin ((short *)&Mdn,      SQLSHORT, 0);
	A_pr.sqlout ((short *)&fil,      SQLSHORT, 0);
	A_pr.sqlout ((LPTSTR) adr_krz,   SQLCHAR, sizeof (adr_krz));
	FilCursor = A_pr.sqlcursor (_T("select fil.fil, adr.adr_krz ")
		                           _T("from fil, adr ")
								   _T("where fil.fil > 0 ")
								   _T("and adr.adr = fil.adr ")
								   _T("and fil.mdn = ? ")
								   _T("order by fil.fil"));

	Hwg     = _T("");
	Wg      = _T("");
	Ag      = _T("");
	TeilSmt = _T("");
	ATyp    = _T("");

	memcpy (&MdnAdrUser.adr, &adr_null, sizeof (ADR));
	memcpy (&MdnUser.mdn, &mdn_null, sizeof (MDN));

    Form.Add (new CFormField (&m_MdnUser,EDIT,     (short *) &MdnUser.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnAdrKrz,EDIT,(char *) MdnAdrUser.adr.adr_krz, VCHAR));
	Form.Add (new CFormField (&m_MdnGr,COMBOBOX, (short *) &MdnGr, VSHORT));
	Form.Add (new CFormField (&m_Mdn,COMBOBOX,   (short *) &Mdn, VSHORT));
	Form.Add (new CFormField (&m_FilGr,COMBOBOX, (short *) &FilGr, VSHORT));
	Form.Add (new CFormField (&m_Fil,COMBOBOX,   (short *) &Fil, VSHORT));
	Form.Add (new CFormField (&m_ListTyp,COMBOBOX,   (short *) &ListType, VSHORT));

	Form.Add (new CFormField (&m_Hwg,EDIT,       (CString *) &Hwg, VSTRING));
	Form.Add (new CFormField (&m_Wg,EDIT,        (CString *) &Wg, VSTRING));
	Form.Add (new CFormField (&m_Ag,EDIT,        (CString *) &Ag, VSTRING));
	Form.Add (new CFormField (&m_TeilSmt,EDIT,   (CString *) &TeilSmt, VSTRING));
	Form.Add (new CFormField (&m_ATyp,EDIT,      (CString *) &ATyp, VSTRING));

	Form.Add (new CFormField (&m_PrDiff,CHECKBOX,    (short *) &PrDiff, VSHORT));

	FillMdnGrCombo ();
	FillMdnCombo ();
	FillListTypCombo ();
	m_FilGr.EnableWindow (FALSE);
	m_Fil.EnableWindow (FALSE);


// Grid f�r Benutzer

    UserGrid.Create (this, 20, 20);
    UserGrid.SetBorder (12, 20);
    UserGrid.SetCellHeight (15);
    UserGrid.SetFontCellHeight (this);
    UserGrid.SetGridSpace (5, 8);

	MdnUserGrid.Create (this, 1, 2);
    MdnUserGrid.SetBorder (0, 0);
    MdnUserGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_MdnUser = new CCtrlInfo (&m_MdnUser, 0, 0, 1, 1);
	MdnUserGrid.Add (c_MdnUser);
	CtrlGrid.CreateChoiceButton (m_MdnUserChoice, IDC_MDNUSERCHOICE, this);
	CCtrlInfo *c_MdnUserChoice = new CCtrlInfo (&m_MdnUserChoice, 1, 0, 1, 1);
	MdnUserGrid.Add (c_MdnUserChoice);

	CCtrlInfo *c_LMdnUser     = new CCtrlInfo (&m_LMdnUser, 0, 0, 1, 1); 
	UserGrid.Add (c_LMdnUser);
	CCtrlInfo *c_MdnUserGrid     = new CCtrlInfo (&MdnUserGrid, 1, 0, 1, 1); 
	UserGrid.Add (c_MdnUserGrid);
	CCtrlInfo *c_MdnAdrKrz    = new CCtrlInfo (&m_MdnAdrKrz, 2, 0, 1, 1); 
	UserGrid.Add (c_MdnAdrKrz);

// Grid f�r HWG mit Auswahl
	HwgGrid.Create (this, 1, 2);
    HwgGrid.SetBorder (0, 0);
    HwgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_HwgGrid = new CCtrlInfo (&m_Hwg, 0, 0, 1, 1);
	HwgGrid.Add (c_HwgGrid);
	CtrlGrid.CreateChoiceButton (m_HwgChoice, IDC_HWGCHOICE, this);
	CCtrlInfo *c_HwgChoice = new CCtrlInfo (&m_HwgChoice, 1, 0, 1, 1);
	HwgGrid.Add (c_HwgChoice);

// Grid f�r WG mit Auswahl
	WgGrid.Create (this, 1, 2);
    WgGrid.SetBorder (0, 0);
    WgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_WgGrid = new CCtrlInfo (&m_Wg, 0, 0, 1, 1);
	WgGrid.Add (c_WgGrid);
	CtrlGrid.CreateChoiceButton (m_WgChoice, IDC_WGCHOICE, this);
	CCtrlInfo *c_WgChoice = new CCtrlInfo (&m_WgChoice, 1, 0, 1, 1);
	WgGrid.Add (c_WgChoice);

// Grid f�r AG mit Auswahl
	AgGrid.Create (this, 1, 2);
    AgGrid.SetBorder (0, 0);
    AgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_AgGrid = new CCtrlInfo (&m_Ag, 0, 0, 1, 1);
	AgGrid.Add (c_AgGrid);
	CtrlGrid.CreateChoiceButton (m_AgChoice, IDC_AGCHOICE, this);
	CCtrlInfo *c_AgChoice = new CCtrlInfo (&m_AgChoice, 1, 0, 1, 1);
	AgGrid.Add (c_AgChoice);

// Grid f�r Teilsortiment mit Auswahl
	TeilSmtGrid.Create (this, 1, 2);
    TeilSmtGrid.SetBorder (0, 0);
    TeilSmtGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_TeilSmtGrid = new CCtrlInfo (&m_TeilSmt, 0, 0, 1, 1);
	TeilSmtGrid.Add (c_TeilSmtGrid);
	CtrlGrid.CreateChoiceButton (m_TeilSmtChoice, IDC_TEILSMTCHOICE, this);
	CCtrlInfo *c_TeilSmtChoice = new CCtrlInfo (&m_TeilSmtChoice, 1, 0, 1, 1);
	TeilSmtGrid.Add (c_TeilSmtChoice);

// Grid f�r Artikeltyp mit Auswahl
	ATypGrid.Create (this, 1, 2);
    ATypGrid.SetBorder (0, 0);
    ATypGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_ATypGrid = new CCtrlInfo (&m_ATyp, 0, 0, 1, 1);
	ATypGrid.Add (c_ATypGrid);
	CtrlGrid.CreateChoiceButton (m_ATypChoice, IDC_ATYPCHOICE, this);
	CCtrlInfo *c_ATypChoice = new CCtrlInfo (&m_ATypChoice, 1, 0, 1, 1);
	ATypGrid.Add (c_ATypChoice);

// Hauptgrid
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (20, 8);

    CompanyGrid.Create (this, 20, 20);
    CompanyGrid.SetBorder (12, 20);
    CompanyGrid.SetCellHeight (15);
    CompanyGrid.SetFontCellHeight (this);
    CompanyGrid.SetGridSpace (5, 8);

	CCtrlInfo *c_CompanyGroup     = new CCtrlInfo (&m_CmpGroup, 0, 0, 5, 5); 
	CompanyGrid.Add (c_CompanyGroup);

	CCtrlInfo *c_LMdnGr     = new CCtrlInfo (&m_LMdnGr, 1, 1, 1, 1); 
	CompanyGrid.Add (c_LMdnGr);
	CCtrlInfo *c_MdnGr     = new CCtrlInfo (&m_MdnGr, 2, 1, 1, 1); 
	CompanyGrid.Add (c_MdnGr);
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 1, 2, 1, 1); 
	CompanyGrid.Add (c_LMdn);
	CCtrlInfo *c_Mdn     = new CCtrlInfo (&m_Mdn, 2, 2, 1, 1); 
	CompanyGrid.Add (c_Mdn);

	CCtrlInfo *c_LFilGr     = new CCtrlInfo (&m_LFilGr, 1, 3, 1, 1); 
	CompanyGrid.Add (c_LFilGr);
	CCtrlInfo *c_FilGr     = new CCtrlInfo (&m_FilGr, 2, 3, 1, 1); 
	CompanyGrid.Add (c_FilGr);
	CCtrlInfo *c_LFil     = new CCtrlInfo (&m_LFil, 1, 4, 1, 1); 
	CompanyGrid.Add (c_LFil);
	CCtrlInfo *c_Fil     = new CCtrlInfo (&m_Fil, 2, 4, 1, 1); 
	CompanyGrid.Add (c_Fil);

	CCtrlInfo *c_LListTyp     = new CCtrlInfo (&m_LListTyp, 1, 5, 1, 1); 
	CompanyGrid.Add (c_LListTyp);
	CCtrlInfo *c_ListTyp     = new CCtrlInfo (&m_ListTyp, 2, 5, 1, 1); 
	CompanyGrid.Add (c_ListTyp);

    GrGrid.Create (this, 20, 20);
    GrGrid.SetBorder (12, 20);
    GrGrid.SetCellHeight (15);
    GrGrid.SetFontCellHeight (this);
    GrGrid.SetGridSpace (5, 8);

	CCtrlInfo *c_GrGroup     = new CCtrlInfo (&m_GrGroup, 0, 0, 5, 5); 
	GrGrid.Add (c_GrGroup);

	CCtrlInfo *c_LHwg     = new CCtrlInfo (&m_LHwg, 1, 1, 1, 1); 
	GrGrid.Add (c_LHwg);
//	CCtrlInfo *c_Hwg     = new CCtrlInfo (&m_Hwg, 2, 1, 1, 1); 
	CCtrlInfo *c_Hwg     = new CCtrlInfo (&HwgGrid, 2, 1, 1, 1); 
	GrGrid.Add (c_Hwg);
	CCtrlInfo *c_LWg     = new CCtrlInfo (&m_LWg, 1, 2, 1, 1); 
	GrGrid.Add (c_LWg);
//	CCtrlInfo *c_Wg     = new CCtrlInfo (&m_Wg, 2, 2, 1, 1); 
	CCtrlInfo *c_Wg     = new CCtrlInfo (&WgGrid, 2, 2, 1, 1); 
	GrGrid.Add (c_Wg);
	CCtrlInfo *c_LAg     = new CCtrlInfo (&m_LAg, 1, 3, 1, 1); 
	GrGrid.Add (c_LAg);
//	CCtrlInfo *c_Ag     = new CCtrlInfo (&m_Ag, 2, 3, 1, 1); 
	CCtrlInfo *c_Ag     = new CCtrlInfo (&AgGrid, 2, 3, 1, 1); 
	GrGrid.Add (c_Ag);

	CCtrlInfo *c_LTeilSmt     = new CCtrlInfo (&m_LTeilSmt, 1, 4, 1, 1); 
	GrGrid.Add (c_LTeilSmt);
	CCtrlInfo *c_TeilSmt     = new CCtrlInfo (&TeilSmtGrid, 2, 4, 1, 1); 
	GrGrid.Add (c_TeilSmt);
	CCtrlInfo *c_LATyp     = new CCtrlInfo (&m_LATyp, 1, 5, 1, 1); 
	GrGrid.Add (c_LATyp);
	CCtrlInfo *c_ATyp     = new CCtrlInfo (&ATypGrid, 2, 5, 1, 1); 
	GrGrid.Add (c_ATyp);

	m_SmtGroup.ShowWindow (SW_HIDE);

/*
    SmtGrid.Create (this, 20, 20);
    SmtGrid.SetBorder (12, 20);
    SmtGrid.SetCellHeight (15);
    SmtGrid.SetFontCellHeight (this);
    SmtGrid.SetGridSpace (5, 8);


	CCtrlInfo *c_SmtGroup     = new CCtrlInfo (&m_SmtGroup, 0, 0, 5, 5); 
	SmtGrid.Add (c_SmtGroup);

	CCtrlInfo *c_LTeilSmt     = new CCtrlInfo (&m_LTeilSmt, 1, 1, 1, 1); 
	SmtGrid.Add (c_LTeilSmt);
	CCtrlInfo *c_TeilSmt     = new CCtrlInfo (&TeilSmtGrid, 2, 1, 1, 1); 
	SmtGrid.Add (c_TeilSmt);
	CCtrlInfo *c_LATyp     = new CCtrlInfo (&m_LATyp, 1, 2, 1, 1); 
	SmtGrid.Add (c_LATyp);
	CCtrlInfo *c_ATyp     = new CCtrlInfo (&ATypGrid, 2, 2, 1, 1); 
	SmtGrid.Add (c_ATyp);
*/

	CCtrlInfo *c_UserGrid     = new CCtrlInfo (&UserGrid, 0, 0, 1, 1); 
	CtrlGrid.Add (c_UserGrid);
	CCtrlInfo *c_CompanyGrid     = new CCtrlInfo (&CompanyGrid, 0, 1, 1, 1); 
	CtrlGrid.Add (c_CompanyGrid);
//	CCtrlInfo *c_GrGrid     = new CCtrlInfo (&GrGrid, 0, 9, 1, 1); 
	CCtrlInfo *c_GrGrid     = new CCtrlInfo (&GrGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_GrGrid);
	CCtrlInfo *c_PrDiff     = new CCtrlInfo (&m_PrDiff, 2, 2, 3, 1); 
	CtrlGrid.Add (c_PrDiff);
	CCtrlInfo *c_Open     = new CCtrlInfo (&m_Open, 2, 4, 1, 1); 
	CtrlGrid.Add (c_Open);
	CCtrlInfo *c_Print     = new CCtrlInfo (&m_Print, 3, 4, 1, 1); 
	c_Print->SetCellPos (-18, 0);
	CtrlGrid.Add (c_Print);
/*
	CCtrlInfo *c_SmtGrid     = new CCtrlInfo (&SmtGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_SmtGrid);

	CCtrlInfo *c_List     = new CCtrlInfo (&m_List, 0, 9, DOCKRIGHT, DOCKBOTTOM); 
	CtrlGrid.Add (c_List);
*/

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Open.Tooltip.SetFont (&Font);
	m_Open.Tooltip.SetSize ();
	m_Print.Tooltip.SetFont (&Font);
	m_Print.Tooltip.SetSize ();
	m_CmpGroup.SetFont (&lFont);
	m_GrGroup.SetFont (&lFont);

	if (PersName.Trim () != "")
	{
		_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
		if (Sys_ben.dbreadfirstpers_nam () == 0)
		{
			if (Sys_ben.sys_ben.mdn != 0)
			{
				MdnUser.mdn.mdn = Sys_ben.sys_ben.mdn;
				m_MdnUser.SetReadOnly ();
				m_MdnUser.ModifyStyle (WS_TABSTOP, 0);
			}
		}
	}

	Form.Show ();
	CtrlGrid.Display ();

	return FALSE;
}

void CCompanyPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

HBRUSH CCompanyPage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			m_Open.SetBkColor(DlgBkColor);
			m_Print.SetBkColor(DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (pWnd == &m_CmpGroup)
	{
            pDC->SetTextColor (RGB (0, 0, 255));
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (pWnd == &m_GrGroup)
	{
            pDC->SetTextColor (RGB (0, 0, 255));
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CCompanyPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F6)
			{
				OnOpen ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
//				Write ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_MdnUser)
				{
					OnMdnchoice ();
					return TRUE;
				}
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CCompanyPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_MdnUser)
	{
		if (!ReadMdn ())
		{
			m_MdnUser.SetFocus ();
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
	return FALSE;
}

BOOL CCompanyPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
	return FALSE;
}


BOOL CCompanyPage::ReadMdn ()
{
	memcpy (&MdnUser.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdrUser.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (MdnUser.mdn.mdn == 0)
	{
		strcpy (MdnAdrUser.adr.adr_krz, "Zentrale");
		Form.Show ();
		m_MdnUser.SetFocus ();
		m_MdnUser.SetSel (0, -1);
		return TRUE;
	}
	if (MdnUser.dbreadfirst () == 0)
	{
		MdnAdrUser.adr.adr = MdnUser.mdn.adr;
		int dsqlstatus = MdnAdrUser.dbreadfirst ();
		Form.Show ();
		m_MdnUser.SetFocus ();
		m_MdnUser.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),MdnUser.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&MdnUser.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdrUser.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_MdnUser.SetFocus ();
		m_MdnUser.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}


void CCompanyPage::FillMdnGrCombo ()
{
	CFormField *f = Form.GetFormField (&m_MdnGr);
	if (f == NULL) return;
	A_pr.sqlopen (MdnGrCursor);
    f->ComboValues.push_back (new CString (_T("0"))); 
	while (A_pr.sqlfetch (MdnGrCursor) == 0)
	{
			CDbUniCode::DbToUniCode (gr_bz1, (LPSTR) gr_bz1);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), gr,
				                              gr_bz1);
		    f->ComboValues.push_back (ComboValue); 
	}
	A_pr.sqlclose (MdnGrCursor);
	f->FillComboBox ();
	f->SetSel (0);
	f->Get ();
}

void CCompanyPage::FillMdnCombo ()
{
	CFormField *f = Form.GetFormField (&m_Mdn);
	if (f == NULL) return;
	A_pr.sqlopen (MdnCursor);
    f->ComboValues.push_back (new CString (_T("0 Zentrale"))); 
	while (A_pr.sqlfetch (MdnCursor) == 0)
	{
			CDbUniCode::DbToUniCode (adr_krz, (LPSTR) adr_krz);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), mdn,
				                              adr_krz);
		    f->ComboValues.push_back (ComboValue); 
	}
	A_pr.sqlclose (MdnCursor);
	f->FillComboBox ();
	f->SetSel (0);
	f->Get ();
}

void CCompanyPage::FillFilGrCombo ()
{


	CFormField *f = Form.GetFormField (&m_FilGr);
	if (f == NULL) return;
	f->ComboValues.clear ();
	Form.Get ();
	f->ComboValues.push_back (new CString (_T("0"))); 
	if (Mdn > 0)
	{
		m_FilGr.EnableWindow (TRUE);
		A_pr.sqlopen (FilGrCursor);
		while (A_pr.sqlfetch (FilGrCursor) == 0)
		{
			CDbUniCode::DbToUniCode (gr_bz1, (LPSTR) gr_bz1);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), gr,
				                              gr_bz1);
		    f->ComboValues.push_back (ComboValue); 
		}
	}
	else
	{
		m_FilGr.EnableWindow (FALSE);
	}
	f->FillComboBox ();
	f->SetSel (0);
	f->Get ();
}

void CCompanyPage::FillFilCombo ()
{
	CFormField *f = Form.GetFormField (&m_Fil);
	if (f == NULL) return;
	f->ComboValues.clear ();
	Form.Get ();
	f->ComboValues.push_back (new CString (_T("0 Mandant"))); 
	if (Mdn > 0)
	{
		m_Fil.EnableWindow (TRUE);
		A_pr.sqlopen (FilCursor);
		while (A_pr.sqlfetch (FilCursor) == 0)
		{
			CDbUniCode::DbToUniCode (adr_krz, (LPSTR) adr_krz);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), fil,
				                              adr_krz);
		    f->ComboValues.push_back (ComboValue); 
		}
	}
	else
	{
		m_Fil.EnableWindow (FALSE);
	}
	f->FillComboBox ();
	f->SetSel (0);
	f->Get ();
}

void CCompanyPage::FillListTypCombo ()
{
	CFormField *f = Form.GetFormField (&m_ListTyp);
	if (f == NULL) return;
	f->ComboValues.clear ();
	CString *ComboValue = new CString (_T("0 Preise"));
    f->ComboValues.push_back (ComboValue); 
	ComboValue = new CString (_T("1 Aktionen"));
    f->ComboValues.push_back (ComboValue); 
	ComboValue = new CString (_T("2 Preise und Aktionen"));
    f->ComboValues.push_back (ComboValue); 
	f->FillComboBox ();
	f->SetSel (0);
	f->Get ();
}

void CCompanyPage::FillHwgCombo ()
{
}

void CCompanyPage::FillWgCombo ()
{
}

void CCompanyPage::FillAgCombo ()
{
}

void CCompanyPage::FillTeilSmtCombo ()
{
}

void CCompanyPage::FillATypCombo ()
{
}


void CCompanyPage::OnCbnSelchangeMdn()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	FillFilGrCombo ();
	FillFilCombo ();
	Form.Get ();
	if (Mdn != 0)
	{
		memcpy (&cMdn.mdn, &mdn_null, sizeof (MDN));
		cMdn.mdn.mdn = Mdn;
        cMdn.dbreadfirst ();
        MdnGr = cMdn.mdn.mdn_gr;
		Form.Show ();
	}
}

void CCompanyPage::OnCbnSelchangeFil()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	if (Fil != 0)
	{
		memcpy (&cFil.fil, &fil_null, sizeof (FIL));
		cFil.fil.mdn = Mdn;
		cFil.fil.fil = Fil;
        cFil.dbreadfirst ();
        FilGr = cFil.fil.fil_gr;
		Form.GetFormField (&m_FilGr)->Show ();
	}
}

void CCompanyPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&MdnUser.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdrUser.adr, &adr_null, sizeof (ADR));
		  MdnUser.mdn.mdn = abl->mdn;
		  if (MdnUser.dbreadfirst () == 0)
		  {
			  MdnAdrUser.adr.adr = MdnUser.mdn.adr;
			  MdnAdrUser.dbreadfirst ();
		  }
		  if (MdnUser.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_MdnUser.SetSel (0, -1, TRUE);
		  m_MdnUser.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}


void CCompanyPage::OnHwgChoice ()
{
	Form.Get ();
	if (ChoiceHWG != NULL && !ModalChoiceHWG)
	{
		ChoiceHWG->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceHWG == NULL)
	{
		ChoiceHWG = new CChoiceHWG (this);
	    ChoiceHWG->IsModal = ModalChoiceHWG;
		ChoiceHWG->IdArrDown = IDI_HARROWDOWN;
		ChoiceHWG->IdArrUp   = IDI_HARROWUP;
		ChoiceHWG->IdArrNo   = IDI_HARROWNO;
	    ChoiceHWG->HideEnter = FALSE;
		ChoiceHWG->CreateDlg ();
		ChoiceHWG->SingleSelection = FALSE;
	}

    ChoiceHWG->SetDbClass (&A_bas);

	ChoiceHWG->DoModal();

    if (ChoiceHWG->GetState ())
    {
		  CHWGList *abl; 

		  CString Text = "";
		  if (ChoiceHWG->SelectList.size () > 1)
		  {
				for (std::vector<CHWGList *>::iterator pabl = ChoiceHWG->SelectList.begin (); pabl != ChoiceHWG->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cHwg;
					cHwg.Format (_T("%hd"), abl->hwg);
					Text += cHwg;
				}
	   	  }
		  else
		  {
			  abl = ChoiceHWG->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%hd"), abl->hwg);
		  }
		  m_Hwg.SetWindowText (Text);
    }
}

void CCompanyPage::OnWgChoice ()
{
	Form.Get ();
	if (ChoiceWG != NULL && !ModalChoiceWG)
	{
		ChoiceWG->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceWG == NULL)
	{
		ChoiceWG = new CChoiceWG (this);
	    ChoiceWG->IsModal = ModalChoiceAG;
		ChoiceWG->IdArrDown = IDI_HARROWDOWN;
		ChoiceWG->IdArrUp   = IDI_HARROWUP;
		ChoiceWG->IdArrNo   = IDI_HARROWNO;
	    ChoiceWG->HideEnter = FALSE;
		ChoiceWG->CreateDlg ();
		ChoiceWG->SingleSelection = FALSE;
	}

    ChoiceWG->SetDbClass (&A_bas);

	ChoiceWG->DoModal();

    if (ChoiceWG->GetState ())
    {
		  CWGList *abl; 

		  CString Text = "";
		  if (ChoiceWG->SelectList.size () > 1)
		  {
				for (std::vector<CWGList *>::iterator pabl = ChoiceWG->SelectList.begin (); pabl != ChoiceWG->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cWg;
					cWg.Format (_T("%hd"), abl->wg);
					Text += cWg;
				}
	   	  }
		  else
		  {
			  abl = ChoiceWG->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%hd"), abl->wg);
		  }
		  m_Wg.SetWindowText (Text);
    }
}

void CCompanyPage::OnAgChoice ()
{
	Form.Get ();
	if (ChoiceAG != NULL && !ModalChoiceAG)
	{
		ChoiceAG->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceAG == NULL)
	{
		ChoiceAG = new CChoiceAG (this);
	    ChoiceAG->IsModal = ModalChoiceAG;
		ChoiceAG->IdArrDown = IDI_HARROWDOWN;
		ChoiceAG->IdArrUp   = IDI_HARROWUP;
		ChoiceAG->IdArrNo   = IDI_HARROWNO;
	    ChoiceAG->HideEnter = FALSE;
		ChoiceAG->CreateDlg ();
		ChoiceAG->SingleSelection = FALSE;
	}

    ChoiceAG->SetDbClass (&A_bas);

	ChoiceAG->DoModal();

    if (ChoiceAG->GetState ())
    {
		  CAGList *abl; 

		  CString Text = "";
		  if (ChoiceAG->SelectList.size () > 1)
		  {
				for (std::vector<CAGList *>::iterator pabl = ChoiceAG->SelectList.begin (); pabl != ChoiceAG->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cAg;
					cAg.Format (_T("%ld"), abl->ag);
					Text += cAg;
				}
	   	  }
		  else
		  {
			  abl = ChoiceAG->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%hd"), abl->ag);
		  }
		  m_Ag.SetWindowText (Text);
    }
}


void CCompanyPage::OnTeilSmtChoice ()
{
	Form.Get ();
	CChoicePtab *ChoicePtab = new CChoicePtab (this);
    ChoicePtab->IsModal = TRUE;
	ChoicePtab->IdArrDown = IDI_HARROWDOWN;
	ChoicePtab->IdArrUp   = IDI_HARROWUP;
	ChoicePtab->IdArrNo   = IDI_HARROWNO;
    ChoicePtab->HideEnter = FALSE;
	ChoicePtab->CreateDlg ();
	ChoicePtab->SingleSelection = FALSE;
    ChoicePtab->Ptitem = _T("teil_smt");
    ChoicePtab->Caption = _T("Auswahl Teilsortiment");

    ChoicePtab->SetDbClass (&A_bas);

	ChoicePtab->DoModal();

    if (ChoicePtab->GetState ())
    {
		  CPtabList *abl; 

		  CString Text = "";
		  if (ChoicePtab->SelectList.size () > 1)
		  {
				for (std::vector<CPtabList *>::iterator pabl = ChoicePtab->SelectList.begin (); pabl != ChoicePtab->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					abl->ptwert.Trim ();
					Text += abl->ptwert;
				}
	   	  }
		  else
		  {
			  abl = ChoicePtab->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text = abl->ptwert.Trim ();
		  }
		  m_TeilSmt.SetWindowText (Text);
    }
}

void CCompanyPage::OnATypChoice ()
{
	Form.Get ();
	CChoicePtab *ChoicePtab = new CChoicePtab (this);
    ChoicePtab->IsModal = TRUE;
	ChoicePtab->IdArrDown = IDI_HARROWDOWN;
	ChoicePtab->IdArrUp   = IDI_HARROWUP;
	ChoicePtab->IdArrNo   = IDI_HARROWNO;
    ChoicePtab->HideEnter = FALSE;
	ChoicePtab->CreateDlg ();
	ChoicePtab->SingleSelection = FALSE;
    ChoicePtab->Ptitem = _T("a_typ");
    ChoicePtab->Caption = _T("Auswahl Artikeltyp");

    ChoicePtab->SetDbClass (&A_bas);

	ChoicePtab->DoModal();

    if (ChoicePtab->GetState ())
    {
		  CPtabList *abl; 

		  CString Text = "";
		  if (ChoicePtab->SelectList.size () > 1)
		  {
				for (std::vector<CPtabList *>::iterator pabl = ChoicePtab->SelectList.begin (); pabl != ChoicePtab->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					abl->ptwert.Trim ();
					Text += abl->ptwert;
				}
	   	  }
		  else
		  {
			  abl = ChoicePtab->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text = abl->ptwert.Trim ();
		  }
		  m_ATyp.SetWindowText (Text);
    }
}

void CCompanyPage::OnOpen ()
{
	CString CmpSelect (_T(""));
	CString Select (_T(""));
	CString SelectPart;
	Form.Get ();

	if (Hwg.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.hwg in (%s)"), Hwg.GetBuffer ());
		Select += SelectPart;
	}
	if (Wg.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.wg in (%s)"), Wg.GetBuffer ());
		Select += SelectPart;
	}
	if (Ag.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.ag in (%s)"), Ag.GetBuffer ());
		Select += SelectPart;
	}
	if (TeilSmt.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.teil_smt in (%s)"), TeilSmt.GetBuffer ());
		Select += SelectPart;
	}
	if (ATyp.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.a_typ in (%s)"), ATyp.GetBuffer ());
		Select += SelectPart;
	}
	CAprUpdateEvent *UpdateEvent = UpdateTab[0];
	if (PrDiff == 0)
	{
		UpdateEvent->SetMdn (Mdn);
		UpdateEvent->Read (Select, MdnGr, Mdn, FilGr, Fil);
	}
	else
	{
		if (MdnGr != 0)
		{
			SelectPart.Format (_T(" and a_pr.mdn_gr = %hd"), MdnGr);
			CmpSelect += SelectPart;
		}
		if (Mdn != 0)
		{
			SelectPart.Format (_T(" and a_pr.mdn = %hd"), Mdn);
			CmpSelect += SelectPart;
		}
		if (FilGr != 0)
		{
			SelectPart.Format (_T(" and a_pr.fil_gr = %hd"), FilGr);
			CmpSelect += SelectPart;
		}
		if (Fil != 0)
		{
			SelectPart.Format (_T(" and a_pr.fil = %hd"), Fil);
			CmpSelect += SelectPart;
		}

		UpdateEvent->SetMdn (Mdn);
		UpdateEvent->SetPrVkVisible (FilGr != EkFilGr);
		UpdateEvent->Read (Select, CmpSelect);
	}
}

void CCompanyPage::OnEnChangeMdnUser()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den __super::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
	if (MdnUser.mdn.mdn != 0)
	{

		Mdn = MdnUser.mdn.mdn;
		CFormField *control = (CFormField *) Form.GetObject () [3];
		control->Show ();
		m_MdnGr.EnableWindow (FALSE);
		m_Mdn.EnableWindow (FALSE);
		m_FilGr.EnableWindow ();
		m_Fil.EnableWindow ();
		FillFilGrCombo ();
		FillFilCombo ();
	}
	else
	{
		m_MdnGr.EnableWindow ();
		m_Mdn.EnableWindow ();
	}
}

void CCompanyPage::OnPrint ()
{
	Print ();
}

BOOL CCompanyPage::Print ()
{

	CProcess print;
	CString Command;

	CString CmpSelect (_T(""));
	CString Select (_T(""));
	CString SelectPart;
	Form.Get ();

	OnCbnSelchangeFil();
	if (Hwg.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.hwg in (%s)"), Hwg.GetBuffer ());
		Select += SelectPart;
	}
	if (Wg.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.wg in (%s)"), Wg.GetBuffer ());
		Select += SelectPart;
	}
	if (Ag.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.ag in (%s)"), Ag.GetBuffer ());
		Select += SelectPart;
	}
	if (TeilSmt.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.teil_smt in (%s)"), TeilSmt.GetBuffer ());
		Select += SelectPart;
	}
	if (ATyp.Trim ().GetLength () > 0)
	{
		SelectPart.Format (_T(" and a_bas.a_typ in (%s)"), ATyp.GetBuffer ());
		Select += SelectPart;
	}
	CAprUpdateEvent *UpdateEvent = UpdateTab[0];
	if (PrDiff == 0)
	{
		if (!Print (Select, MdnGr, Mdn, FilGr, Fil))
		{
			MessageBox (_T("Liste kann nicht gedruckt werden"));
			return FALSE;
		}
	}
	else
	{
		if (MdnGr != 0)
		{
			SelectPart.Format (_T(" and a_pr.mdn_gr = %hd"), MdnGr);
			CmpSelect += SelectPart;
		}
		if (Mdn != 0)
		{
			SelectPart.Format (_T(" and a_pr.mdn = %hd"), Mdn);
			CmpSelect += SelectPart;
		}
		if (FilGr != 0)
		{
			SelectPart.Format (_T(" and a_pr.fil_gr = %hd"), FilGr);
			CmpSelect += SelectPart;
		}
		if (Fil != 0)
		{
			SelectPart.Format (_T(" and a_pr.fil = %hd"), Fil);
			CmpSelect += SelectPart;
		}
		if (!Print (Select, CmpSelect))
		{
			MessageBox (_T("Liste kann nicht gedruckt werden"));
			return FALSE;
		}
	}

	LPTSTR tmp = getenv ("TMPPATH");
	CString dName;
	FILE *fp;

	switch (ListType)
	{
	case Preise:
		if (tmp != NULL)
		{
			dName.Format ("%s\\1120c.llf", tmp);
		}
		else
		{
			dName = "1120c.llf";
		}
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			short hwg       = _tstoi (Hwg.GetBuffer ());
			short wg        = _tstoi (Wg.GetBuffer ());
			long  ag        = _tstol (Ag.GetBuffer ());
			short teil_smt  = _tstoi (TeilSmt.GetBuffer ());
			short a_typ     = _tstoi (ATyp.GetBuffer ());
			fprintf (fp, "NAME 1120c\n");
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "cmdn %hd %hd\n",Mdn, Mdn);
			fprintf (fp, "cfil_gr %hd %hd\n",FilGr, FilGr);
			fprintf (fp, "cfil %hd %hd\n",Fil, Fil);
			fprintf (fp, "hwg %hd %hd\n",hwg, hwg);
			fprintf (fp, "wg %hd %hd\n",wg, wg);
			fprintf (fp, "ag %ld %ld\n",ag, ag);
			fprintf (fp, "teil_smt %hd %hd\n",teil_smt, teil_smt);
			fclose (fp);
			Command.Format ("dr70001 -name 1120c -datei %s", dName.GetBuffer ());
		}
		break;
	case Aktionen:
		if (tmp != NULL)
		{
			dName.Format ("%s\\1120ca.llf", tmp);
		}
		else
		{
			dName = "1120ca.llf";
		}
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			short hwg       = _tstoi (Hwg.GetBuffer ());
			short wg        = _tstoi (Wg.GetBuffer ());
			long  ag        = _tstol (Ag.GetBuffer ());
			short teil_smt  = _tstoi (TeilSmt.GetBuffer ());
			short a_typ     = _tstoi (ATyp.GetBuffer ());
			fprintf (fp, "NAME 1120ca\n");
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "cmdn %hd %hd\n",Mdn, Mdn);
			fprintf (fp, "cfil_gr %hd %hd\n",FilGr, FilGr);
			fprintf (fp, "cfil %hd %hd\n",Fil, Fil);
			fprintf (fp, "hwg %hd %hd\n",hwg, hwg);
			fprintf (fp, "hwg %hd %hd\n",wg, wg);
			fprintf (fp, "wg %ld %ld\n",ag, ag);
			fprintf (fp, "ag %hd %hd\n",teil_smt, teil_smt);
			fclose (fp);
			Command.Format ("dr70001 -name 1120ca -datei %s", dName.GetBuffer ());
		}
		break;
	case PreiseAktionen:
		if (tmp != NULL)
		{
			dName.Format ("%s\\1120cpa.llf", tmp);
		}
		else
		{
			dName = "1120cpa.llf";
		}
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			short hwg       = _tstoi (Hwg.GetBuffer ());
			short wg        = _tstoi (Wg.GetBuffer ());
			long  ag        = _tstol (Ag.GetBuffer ());
			short teil_smt  = _tstoi (TeilSmt.GetBuffer ());
			short a_typ     = _tstoi (ATyp.GetBuffer ());
			fprintf (fp, "NAME 1120cpa\n");
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "cmdn %hd %hd\n",Mdn, Mdn);
			fprintf (fp, "cfil_gr %hd %hd\n",FilGr, FilGr);
			fprintf (fp, "cfil %hd %hd\n",Fil, Fil);
			fprintf (fp, "hwg %hd %hd\n",hwg, hwg);
			fprintf (fp, "wg %hd %hd\n",wg, wg);
			fprintf (fp, "ag %ld %ld\n",ag, ag);
			fprintf (fp, "teil_smt %hd %hd\n",teil_smt, teil_smt);
			fclose (fp);
			Command.Format ("dr70001 -name 1120cpa -datei %s", dName.GetBuffer ());
		}
		break;
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;

}

BOOL CCompanyPage::Print (CString& Select, CString& CmpSelect)
{
	CString Sql;
	int AprCursor;
	int sql_s;
	extern short sql_mode;

	sql_s = sql_mode;

	sql_mode = 1;

	int i = 0;
	A_pr_list.beginwork ();
	while (A_pr_list.sqlcomm ("delete from a_pr_list") < 0)
	{
		if ( i == 100)
		{
			return FALSE;
		}
		Sleep (50);
		i ++;
	}

	Sql = _T("select a_pr.a, a_pr.mdn_gr, a_pr.mdn, a_pr.fil_gr, a_pr.fil from a_pr, a_bas ")
		  _T("where a_pr.a > 0 and a_bas.a = a_pr.a");
	Sql += _T( " ");
	if (CmpSelect.GetLength () > 0)
	{
		Sql += CmpSelect;
		Sql += _T( " ");
	}
	if (Select.GetLength () > 0)
	{
		Sql += Select;
		Sql += _T( " ");
	}

	Sql += _T("order by a_pr.a, a_pr.mdn_gr, a_pr.mdn, a_pr.fil_gr, a_pr.fil");
    A_pr.sqlout ((double *) &A_pr.a_pr.a, SQLDOUBLE, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.mdn_gr, SQLSHORT, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.mdn, SQLSHORT, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.fil_gr, SQLSHORT, 0);  
    A_pr.sqlout ((short *) &A_pr.a_pr.fil, SQLSHORT, 0);  
    AprCursor = A_pr.sqlcursor (Sql.GetBuffer ());

	sql_mode = sql_s;
	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	A_pr.sqlopen (AprCursor);
	while (A_pr.sqlfetch (AprCursor) == 0)
	{
		A_pr.dbreadfirst ();
		memcpy (&A_pr_list.a_pr_list, &a_pr_list_null, sizeof (A_PR_LIST));

		A_pr_list.a_pr_list.cmdn_gr  = MdnGr; 
		A_pr_list.a_pr_list.cmdn     = Mdn; 
		A_pr_list.a_pr_list.cfil_gr  = FilGr; 
		A_pr_list.a_pr_list.cfil     = Fil; 
		A_pr_list.a_pr_list.hwg      = _tstoi (Hwg.GetBuffer ()); 
		A_pr_list.a_pr_list.wg       = _tstoi (Wg.GetBuffer ()); 
		A_pr_list.a_pr_list.ag       = _tstol (Ag.GetBuffer ()); 
		A_pr_list.a_pr_list.teil_smt = _tstoi (TeilSmt.GetBuffer ()); 
		A_pr_list.a_pr_list.a_typ    = _tstoi (ATyp.GetBuffer ()); 

		A_pr_list.a_pr_list.a      = A_pr.a_pr.a; 
		A_pr_list.a_pr_list.mdn_gr = A_pr.a_pr.mdn_gr; 
		A_pr_list.a_pr_list.mdn    = A_pr.a_pr.mdn; 
		A_pr_list.a_pr_list.fil_gr = A_pr.a_pr.fil_gr; 
		A_pr_list.a_pr_list.fil    = A_pr.a_pr.fil; 
		A_pr_list.a_pr_list.pr_ek  = A_pr.a_pr.pr_ek_euro; 
		A_pr_list.a_pr_list.pr_vk  = A_pr.a_pr.pr_vk_euro; 
		if (ListType == Aktionen)
		{
			short sa = GetAktPrice (A_pr.a_pr.a, A_pr.a_pr.mdn_gr, A_pr.a_pr.mdn, A_pr.a_pr.fil_gr, A_pr.a_pr.fil);
			if (sa == 1 && A_pr_list.a_pr_list.lad_akt_von.day != 0)
			{
				A_pr_list.dbupdate ();
//				A_pr_list.dbinsert_nulls ();
			}
		}
		else if (ListType == PreiseAktionen)
		{
			short sa = GetAktPrice (A_pr.a_pr.a, A_pr.a_pr.mdn_gr, A_pr.a_pr.mdn, A_pr.a_pr.fil_gr, A_pr.a_pr.fil);
			if (sa == 1 && A_pr_list.a_pr_list.lad_akt_von.day != 0)
			{
				A_pr_list.dbupdate ();
			}
			else
			{
				A_pr_list.dbinsert_nulls ();
			}
		}
		else
		{
			A_pr_list.dbinsert_nulls ();
		}

	}
	A_pr.sqlclose (AprCursor);
	A_pr_list.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

BOOL CCompanyPage::Print (CString& Select, short mdn_gr, short mdn, short fil_gr, short fil)
{
	CPrice Price;
	CString Sql;
	int AprCursor;

	int sql_s;
	extern short sql_mode;

	sql_s = sql_mode;

	sql_mode = 1;

	int i = 0;
	A_pr_list.beginwork ();
	while (A_pr_list.sqlcomm ("delete from a_pr_list") < 0)
	{
		if ( i == 100)
		{
			return FALSE;
		}
		Sleep (50);
		i ++;
	}

	Sql = _T("select distinct (a_pr.a) from a_pr, a_bas ")
		  _T("where a_pr.a > 0 and a_bas.a = a_pr.a ");
	Sql += _T( " ");
	if (Select.GetLength () > 0)
	{
		Sql += Select;
		Sql += _T( " ");
	}

	Sql += _T("order by a_pr.a");
    A_pr.sqlout ((double *) &A_pr.a_pr.a, SQLDOUBLE, 0);  
    AprCursor = A_pr.sqlcursor (Sql.GetBuffer ());

	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	A_pr.sqlopen (AprCursor);
	while (A_pr.sqlfetch (AprCursor) == 0)
	{
		Price.a_pr.a      = A_pr.a_pr.a;
		Price.a_pr.mdn_gr = mdn_gr;
		Price.a_pr.mdn    = mdn;
		Price.a_pr.fil_gr = fil_gr;
		Price.a_pr.fil    = fil;
		cFil.fil.mdn      = Price.a_pr.mdn;
		cFil.fil.fil      = Price.a_pr.fil;
		if (cFil.dbreadfirst () == 0)
		{
			Price.a_pr.fil_gr = cFil.fil.fil_gr;
		}
		if (Price.dbfetchpreis () != NULL) continue;
		memcpy (&A_pr_list.a_pr_list, &a_pr_list_null, sizeof (A_PR_LIST));
		A_pr_list.a_pr_list.cmdn_gr  = mdn_gr; 
		A_pr_list.a_pr_list.cmdn     = mdn; 
		A_pr_list.a_pr_list.cfil_gr  = fil_gr; 
		A_pr_list.a_pr_list.cfil     = fil; 
		A_pr_list.a_pr_list.hwg      = _tstoi (Hwg.GetBuffer ()); 
		A_pr_list.a_pr_list.wg       = _tstoi (Wg.GetBuffer ()); 
		A_pr_list.a_pr_list.ag       = _tstol (Ag.GetBuffer ()); 
		A_pr_list.a_pr_list.teil_smt = _tstoi (TeilSmt.GetBuffer ()); 
		A_pr_list.a_pr_list.a_typ    = _tstoi (ATyp.GetBuffer ()); 

		A_pr_list.a_pr_list.a       = Price.a_pr.a; 
		A_pr_list.a_pr_list.mdn_gr = Price.a_pr.mdn_gr; 
		A_pr_list.a_pr_list.mdn    = Price.a_pr.mdn; 
		A_pr_list.a_pr_list.fil_gr = Price.a_pr.fil_gr; 
		A_pr_list.a_pr_list.fil    = Price.a_pr.fil; 
		A_pr_list.a_pr_list.pr_ek  = Price.a_pr.pr_ek_euro;
		A_pr_list.a_pr_list.pr_vk  = Price.a_pr.pr_vk_euro; 
		if (ListType == Aktionen)
		{
			short sa = GetAktPrice (A_pr.a_pr.a, mdn_gr, mdn, fil_gr, fil);
			if (sa == 1 && A_pr_list.a_pr_list.lad_akt_von.day != 0)
			{
				A_pr_list.dbupdate ();
//				A_pr_list.dbinsert_nulls ();
			}
		}
		else if (ListType == PreiseAktionen)
		{
			short sa = GetAktPrice (A_pr.a_pr.a, mdn_gr, mdn, fil_gr, fil);
			if (sa == 1 && A_pr_list.a_pr_list.lad_akt_von.day != 0)
			{
				A_pr_list.dbupdate ();
			}
			else
			{
				A_pr_list.dbinsert_nulls ();
			}
		}
		else
		{
			A_pr_list.dbinsert_nulls ();
		}
	}
	A_pr.sqlclose (AprCursor);
	A_pr_list.commitwork ();
	return TRUE;
}

short CCompanyPage::GetAktPrice (double a, short mdn_gr, short mdn, short fil_gr, short fil)
{
	short sa = 0;
	double pr_ek;
	double pr_vk;

	if (DllPreise.PriceLib != NULL && DllPreise.fetch_preis_tag != NULL)
	{
		CString Sysdate;
		DB_CLASS::Sysdate (Sysdate);
		sa = (*DllPreise.fetch_preis_tag) (mdn_gr, mdn, 
			                               fil_gr, fil,
										   a, Sysdate.GetBuffer (),
										   &pr_ek, &pr_vk);
		if (sa && (pr_ek != 0.0 ||pr_vk != 0.0))
		{
			A_pr_list.a_pr_list.pr_ek_sa = pr_ek;
			A_pr_list.a_pr_list.pr_vk_sa = pr_vk;
			strcpy (A_pr_list.a_pr_list.lad_akv_sa, "1");
			DllPreise.GetLadAktVon (&A_pr_list.a_pr_list.lad_akt_von);
			DllPreise.GetLadAktBis (&A_pr_list.a_pr_list.lad_akt_bis);
		}
		else
		{
			sa = 0;
		}
	}
	return sa;
}

void CCompanyPage::ReadCfg ()
{
    char cfg_v [256];

    if (Cfg.GetCfgValue ("EkFilGr", cfg_v) == TRUE)
    {
			EkFilGr = atoi (cfg_v);
    }
}
