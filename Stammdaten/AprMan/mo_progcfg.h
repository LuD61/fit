#ifndef _PROG_CFG
#define _PROG_CFG


class PROG_CFG 
         {
          private :
                FILE *progfp;
          public :
              char progname [80];
			  PROG_CFG () 
			  {
				  progfp = NULL;
				  strcpy (progname, "");
			  }

              PROG_CFG (char *progname) : progfp (NULL)
              {
                       if (progname)
                       {
                               strcpy (this->progname, progname);
                       }
              }
			  ~PROG_CFG ();
              void SetProgName (char *progname)
              {
                       if (progname)
                       {
                               strcpy (this->progname, progname);
                       }
              }
              BOOL OpenCfg (void);
              void CloseCfg (void);
              BOOL GetCfgValue (char *, char *);
              BOOL ReadCfgItem (char *, char *, char *, char **, char *);
              BOOL ReadCfgValues (char **, char *);
              BOOL ReadCfgHelp (char *);
              BOOL  GetGlobDefault (char *, char *);
              BOOL  GetGroupDefault (char *, char *);
			  static void clipped (char *);
			  static void cr_weg (char *);
			  static int strupcmp (char *, char *, int);
         };
#endif         
                          
                               

