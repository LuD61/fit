#include "StdAfx.h"
#include "codeproperty.h"

CCodeProperty::CCodeProperty(void)
{
	source = 0;
	target = 0;
}

CCodeProperty::CCodeProperty(LPTSTR input)
{
	CString Input = input;
	SetValues (Input);
}


CCodeProperty::CCodeProperty(CString& Input)
{
	SetValues (Input);
}

CCodeProperty::~CCodeProperty(void)
{
}

void CCodeProperty::SetValues (CString& Input)
{
	source = 0;
	target = 0;
	int pos = 0;
	CString Source = Input.Tokenize (_T("="), pos);
	if (Source == "") return;
	Source.Trim ();
	CString Target = Input.Tokenize (_T("="), pos);
	if (Target == "") return;
	Target.Trim ();
	if (Source.GetBuffer ()[0] == _T('\''))
	{
		LPSTR p = (LPSTR) Source.GetBuffer ();
#ifdef _UNICODE
		source = (UCHAR) p[2];
#else
		source = (UCHAR) p[1];
#endif
	}
	else
	{
		source = (short) _tstoi (Source.GetBuffer ());
	}
	if (Target.GetBuffer ()[0] == _T('\''))
	{
		LPSTR p = (LPSTR) Target.GetBuffer ();
#ifdef _UNICODE
		target = (UCHAR) p[2];
#else
		target = (UCHAR) p[1];
#endif
	}
	else
	{
		target = (short) _tstoi (Target.GetBuffer ());
	}
}

short CCodeProperty::GetSource (short target)
{
	if (this->target == target)
	{
		return source;
	}
	return 0;
}

short CCodeProperty::GetTarget (short source)
{
	if (this->source == source)
	{
		return target;
	}
	return 0;
}

