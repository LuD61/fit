#pragma once

class CListBean
{
public:
	CString ArchiveName;
	CListCtrl *ListCtrl;
	int SortRow;
	CListBean(void);
	CListBean(LPTSTR ArchiveName);
	~CListBean(void);
	void Save ();
	void Load ();
	void SetListCtrl (CListCtrl* ListCtrl);
    CListCtrl* GetListCtrl ();
	void SetSortRow (int SortRow);
    int GetSortRow ();
    void DropArchive ();
};
