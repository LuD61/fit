#pragma once

class CPageUpdate
{
public:
	CPageUpdate(void);
	~CPageUpdate(void);
	virtual void Show ();
	virtual void Get ();
	virtual void Back ();
	virtual void Write ();
	virtual void Delete ();
	virtual void UpdateAll ();
};
