#include "stdafx.h"
#include "ChoicePrTermin.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoicePrTermin::Sort1 = -1;
int CChoicePrTermin::Sort2 = -1;
int CChoicePrTermin::Sort3 = -1;
int CChoicePrTermin::Sort4 = -1;
int CChoicePrTermin::Sort5 = -1;

CChoicePrTermin::CChoicePrTermin(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_Mdn = 0;
	Rows = NULL;
}

CChoicePrTermin::~CChoicePrTermin() 
{
	DestroyList ();
}

void CChoicePrTermin::DestroyList() 
{
	for (std::vector<CAktKrzList *>::iterator pabl = AktKrzList.begin (); pabl != AktKrzList.end (); ++pabl)
	{
		CAktKrzList *abl = *pabl;
		delete abl;
	}
    AktKrzList.clear ();
}

void CChoicePrTermin::FillList () 
{
    short  mdn_gr;
    short  mdn;
    short  fil_gr;
    short  fil;
    TCHAR zus_bz [34];
	DATE_STRUCT lad_akt_von;
	short anzahl;

	int cursor;

	DestroyList ();
	CListCtrl *listView = GetListView ();
	listView->DeleteAllItems ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Preise auf Termin"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Mdn-Gr"), 1, 50, LVCFMT_RIGHT);
    SetCol (_T("Mandant"), 2, 50, LVCFMT_RIGHT);
    SetCol (_T("Fil-Gr"), 3, 50, LVCFMT_RIGHT);
    SetCol (_T("Filiale"), 4, 50, LVCFMT_RIGHT);
//    SetCol (_T("Bezeichnung"),  5, 1);
    SetCol (_T("G�ltig ab"),  5, 80);
    SetCol (_T("Anzahl"),  6, 80);

	if (AktKrzList.size () == 0 && Rows == NULL)
	{
		if (m_Mdn != 0)
		{
			DbClass->sqlout ((short *)&mdn_gr,     SQLSHORT, 0);
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((short *)&fil_gr,     SQLSHORT, 0);
			DbClass->sqlout ((short *)&fil,     SQLSHORT, 0);
			DbClass->sqlout ((LPTSTR)  zus_bz, SQLCHAR, sizeof (zus_bz));
			DbClass->sqlout((DATE_STRUCT *)&lad_akt_von, SQLDATE, 0);
			DbClass->sqlout((short *)&anzahl, SQLSHORT, 0);

			DbClass->sqlin ((LPTSTR)  &m_Mdn,   SQLSHORT, 0);
			cursor = DbClass->sqlcursor(_T("select akt_krz.mdn_gr, akt_krz.mdn,akt_krz.fil_gr, akt_krz.fil, akt_krz.zeit_von, akt_krz.lad_akt_von, count(akt_krz.a) ")
				_T("from akt_krz ")
				_T("where akt_krz.mdn = ? and akt_krz.lad_akv_sa = \"9\"  ")
				_T("group by 1, 2, 3, 4,5 ,6 ")
				_T("order by 1, 2, 4 ,5 ,6"));

		}
		else
		{
			DbClass->sqlout ((short *)&mdn_gr,     SQLSHORT, 0);
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((short *)&fil_gr,     SQLSHORT, 0);
			DbClass->sqlout ((short *)&fil,     SQLSHORT, 0);
			DbClass->sqlout ((LPTSTR)  zus_bz, SQLCHAR, sizeof (zus_bz));
			DbClass->sqlout((DATE_STRUCT *)&lad_akt_von, SQLDATE, 0);
			DbClass->sqlout((short *)&anzahl, SQLSHORT, 0);

			cursor = DbClass->sqlcursor(_T("select akt_krz.mdn_gr, akt_krz.mdn,akt_krz.fil_gr, akt_krz.fil, akt_krz.zeit_von, akt_krz.lad_akt_von, count(akt_krz.a) ")
				_T("from akt_krz ")
				_T("where akt_krz.mdn >= 0  and akt_krz.lad_akv_sa = \"9\"  ")
				_T("group by 1, 2, 3, 4,5 ,6 ")
				_T("order by 1, 2, 4 ,5 ,6"));

		}
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) zus_bz;
			CDbUniCode::DbToUniCode (zus_bz, pos);
			CAktKrzList *abl = new CAktKrzList (mdn_gr,mdn, fil_gr, fil, zus_bz,lad_akt_von,anzahl);
			AktKrzList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}
	/***
	else if (AktKrzList.size () == 0 && Rows != NULL)
	{
		mdn = (short) m_Mdn;
		Rows->FirstPosition ();
		CString *c;
		while ((c = (CString *) Rows->GetNext ()) != NULL)
		{
			int pos = 0;
			CString PrGrStuf = c->Tokenize (" ", pos);
			if (PrGrStuf != "")
			{
				pr_gr_stuf = atol (PrGrStuf.GetBuffer ());
                int len = PrGrStuf.GetLength ();
				CString ZusBz = PrGrStuf.Mid (len + 1, -1);
				ZusBz.Trim ();
				strcpy (zus_bz, ZusBz.GetBuffer ());
			    CAktKrzList *abl = new CAktKrzList (mdn, pr_gr_stuf, zus_bz);
			    AktKrzList.push_back (abl);
			}
		}

	}
	******/

	for (std::vector<CAktKrzList *>::iterator pabl = AktKrzList.begin (); pabl != AktKrzList.end (); ++pabl)
	{
		CAktKrzList *abl = *pabl;
		CString MdnGr;
		MdnGr.Format (_T("%hd"), abl->mdn_gr); 
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString FilGr;
		FilGr.Format (_T("%hd"), abl->fil_gr); 
		CString Fil;
		Fil.Format (_T("%hd"), abl->fil); 
		CString Num;
		CString CAnzahl;
		CString Bez;
		_tcscpy (zus_bz, abl->zus_bz.GetBuffer ());

		CAnzahl.Format (_T("%hd"), abl->anzahl); 

		CString LText;
		LText.Format (_T("%hd %hd %hd %hd"), abl->mdn_gr,
									abl->mdn,
									abl->fil_gr,
									abl->fil );
                Num = LText;
        int ret = InsertItem (i, -1);
        ret = SetItemText (MdnGr.GetBuffer (), i, 1);
        ret = SetItemText (Mdn.GetBuffer (), i, 2);
        ret = SetItemText (FilGr.GetBuffer (), i, 3);
        ret = SetItemText (Fil.GetBuffer (), i, 4);
//        ret = SetItemText (zus_bz, i, 2);
		CString Clad_akt_von;
		Clad_akt_von.Format(_T("%02hd %02hd %02hd"), abl->lad_akt_von.day, abl->lad_akt_von.month, abl->lad_akt_von.year);
		ret = SetItemText(Clad_akt_von.GetBuffer(), i, 5);
		ret = SetItemText (CAnzahl.GetBuffer(), i, 6);

        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
	Sort5 = -1;
	Sort(listView);
}


void CChoicePrTermin::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoicePrTermin::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoicePrTermin::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoicePrTermin::SearchZusBz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();
		CString cSearch = Search;
		cSearch.MakeUpper ();

        if (_tmemcmp (cSearch.GetBuffer (), iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoicePrTermin::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             SearchZusBz (ListBox, EditText.GetBuffer (8));
             break;
		case 3:
			SearchZusBz(ListBox, EditText.GetBuffer(8));
			break;
		case 4:
			SearchZusBz(ListBox, EditText.GetBuffer(8));
			break;
	}
}

int CChoicePrTermin::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoicePrTermin::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
    CString cItem1 = strItem1;
    CString cItem2 = strItem2;
	cItem1.MakeUpper ();
	cItem2.MakeUpper ();
//	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
	return (cItem2.CompareNoCase (cItem1)) * Sort3;
   }
   else if (SortRow == 3)
   {
	   CString cItem1 = strItem1;
	   CString cItem2 = strItem2;
	   cItem1.MakeUpper();
	   cItem2.MakeUpper();
	   return (cItem2.CompareNoCase(cItem1)) * Sort4;
   }
   else if (SortRow == 4)
   {
	   CString cItem1 = strItem1;
	   CString cItem2 = strItem2;
	   cItem1.MakeUpper();
	   cItem2.MakeUpper();
	   return (cItem2.CompareNoCase(cItem1)) * Sort5;
   }
   return 0;
}


void CChoicePrTermin::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
		case 3:
			Sort4 *= -1;
			break;
		case 4:
			Sort5 *= -1;
			break;
	}
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CAktKrzList *abl = AktKrzList [i];
		   
		   abl->mdn_gr     = _tstoi (ListBox->GetItemText (i, 1));
		   abl->mdn     = _tstoi (ListBox->GetItemText (i, 2));
		   abl->fil_gr     = _tstoi (ListBox->GetItemText (i, 3));
		   abl->fil     = _tstoi (ListBox->GetItemText (i, 4));
		   abl->zus_bz     = ListBox->GetItemText (i, 5);
//		   abl->lad_akt_von     = ListBox->GetItemText (i, 6);
	}
}

void CChoicePrTermin::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = AktKrzList [idx];
}

CAktKrzList *CChoicePrTermin::GetSelectedText ()
{
	CAktKrzList *abl = (CAktKrzList *) SelectedRow;
	return abl;
}

