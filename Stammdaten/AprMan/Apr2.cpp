// Apr2.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "AprMan.h"
#include "Apr2.h"
#include ".\apr2.h"


// CApr2

IMPLEMENT_DYNCREATE(CApr2, DbFormView)

CApr2::CApr2()
	: DbFormView(CApr2::IDD)
{
	tabx = 0;
	taby = 0;
	StartSize = START_NORMAL;
	Cfg.SetProgName( _T("AprMan"));
	ReadCfg ();
}

CApr2::~CApr2()
{
}

void CApr2::DoDataExchange(CDataExchange* pDX)
{
	DbFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CApr2, DbFormView)
	ON_WM_SIZE ()
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
//	ON_WM_DRAWITEM()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	// ON_COMMAND(ID_PRINT_ALL, OnPrintAll)
	ON_COMMAND(ID_BACK, OnBack)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_COMMAND(ID_INSERT, OnInsert)
	ON_UPDATE_COMMAND_UI(ID_INSERT, OnUpdateInsert)
	ON_UPDATE_COMMAND_UI(ID_DELETE, OnUpdateDelete)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_COMMAND(ID_COMPACT_MODE, &CApr2::OnCompactMode)
	ON_UPDATE_COMMAND_UI(ID_COMPACT_MODE, &CApr2::OnUpdateCompactMode)
END_MESSAGE_MAP()


// CApr2-Diagnose

#ifdef _DEBUG
void CApr2::AssertValid() const
{
	DbFormView::AssertValid();
}

void CApr2::Dump(CDumpContext& dc) const
{
	DbFormView::Dump(dc);
}
#endif //_DEBUG


// CApr2-Meldungshandler

void CApr2::OnInitialUpdate()
{
	DbFormView::OnInitialUpdate();


	dlg.Construct (_T(""), this);
	Page1.Construct (IDD_ART_PR_PAGE);
	Page1.Frame = this;
	Page1.HideButtons = TRUE;
	dlg.AddPage (&Page1);

	Page2.Construct (IDD_ART_NEWPR_DAT_PAGE);
	Page2.Frame = this;
	dlg.AddPage (&Page2);

    dlg.m_psh.dwSize = sizeof (dlg.m_psh);
	dlg.Create (this, WS_CHILD | WS_VISIBLE);
    Page1.SetFocus (); 

	CRect cRect;
	GetWindowRect (&cRect);
	GetParent ()->ScreenToClient (&cRect);
	MoveWindow (&cRect);

/****

    dlg.m_psh.pszCaption = _T("Simple");
    dlg.m_psh.nStartPage = 0;
    dlg.m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
    dlg.m_psh.dwSize = sizeof (dlg.m_psh);
	dlg.Create (this, WS_CHILD | WS_VISIBLE);
    Page1.SetFocus (); 

	CRect cRect;
	GetWindowRect (&cRect);
	GetParent ()->ScreenToClient (&cRect);
	MoveWindow (&cRect);
**/

	CSize Size = GetTotalSize ();
	if (Size.cy < (cRect.bottom) + 10)
	{
		Size.cy = cRect.bottom + 10;
	}
	if (Size.cx < (cRect.right) + 10)
	{
		Size.cx = cRect.right + 10;
	}
	SetScrollSizes (MM_TEXT, Size);
}

void CApr2::OnSize(UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (dlg.m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	dlg.MoveWindow (tabx, taby, cx, cy);
//	dlg.MoveWindow (0, 0, cx, cy);
	CTabCtrl *tab = dlg.GetTabControl ();
	Page1.Frame = this;
	Page1.GetWindowRect (&pRect);
	dlg.ScreenToClient (&pRect);

	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = cx - 10 - tabx;
	frame.bottom = cy - 10 - taby;
	tab->MoveWindow (&frame);
	frame.top += 20;
	frame.left += 2;
    frame.right -= 4;
	frame.bottom -= 4;

	int page = dlg.GetActiveIndex ();
	if (page == 0)
	{
		Page1.MoveWindow (&frame);
	}
	else if (page == 1)
	{
		Page2.MoveWindow (&frame);
	}

}

HBRUSH CApr2::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	if (hBrush == NULL)
	{
		hBrush = CreateSolidBrush (Color);
		staticBrush = CreateSolidBrush (Color);
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
 		    return staticBrush;
	}
	return DbFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CApr2::ReadCfg ()
{
    char cfg_v [256];

    if (Cfg.GetCfgValue ("StartSize", cfg_v) == TRUE)
    {
			StartSize = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CApr2::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     dlg.Write ();
}

void CApr2::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     dlg.StepBack ();
}

void CApr2::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.Delete ();
}

void CApr2::OnInsert()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.Insert ();
}

void CApr2::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnCopy ();
}

void CApr2::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnPaste ();
}

void CApr2::OnDeleteall()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.DeleteAll ();
}

void CApr2::OnFilePrint()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.Print ();
}

void CApr2::OnPrintAll()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.PrintAll ();
}

void CApr2::OnUpdateInsert(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
//   CArtPrPage *Page = (CArtPrPage *) dlg.GetPage (0);
   CDbPropertyPage *Page = (CArtPrPage *) dlg.GetActivePage ();
   if (Page == &Page1)
   {
		if (Page1.m_A.IsWindowEnabled ())
		{
		   pCmdUI->Enable (FALSE);
		}
		else
		{
		   pCmdUI->Enable ();
		}
   }
   if (Page == &Page2)
   {
		if (Page2.m_GueAb.IsWindowEnabled ())
		{
		   pCmdUI->Enable (FALSE);
		}
		else
		{
		   pCmdUI->Enable ();
		}
   }
}

void CApr2::OnUpdateDelete(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
//   CArtPrPage *Page = (CArtPrPage *) dlg.GetPage (0);
   CDbPropertyPage *Page = (CArtPrPage *) dlg.GetActivePage ();
   if (Page == &Page1)
   {
		if (Page1.m_A.IsWindowEnabled ())
		{
		   pCmdUI->Enable (FALSE);
		}
		else
		{
		   pCmdUI->Enable ();
		}
   }
   if (Page == &Page2)
   {
		if (Page2.m_GueAb.IsWindowEnabled ())
		{
		   pCmdUI->Enable (FALSE);
		}
		else
		{
		   pCmdUI->Enable ();
		}
   }

}

void CApr2::OnUpdateFileSave(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CDbPropertyPage *Page = (CArtPrPage *) dlg.GetActivePage ();
   if (Page == &Page1)
   {
		if (Page1.m_A.IsWindowEnabled ())
		{
		   pCmdUI->Enable (FALSE);
		}
		else
		{
		   pCmdUI->Enable ();
		}
   }
   if (Page == &Page2)
   {
		if (Page2.m_GueAb.IsWindowEnabled ())
		{
		   pCmdUI->Enable (FALSE);
		}
		else
		{
		   pCmdUI->Enable ();
		}
   }
}

void CApr2::OnCompactMode()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CDbPropertyPage *Page = (CDbPropertyPage *) dlg.GetActivePage ();
   if (Page->IsKindOf( RUNTIME_CLASS( CNewPrPage)))
   {
	   ((CNewPrPage *) Page)->SetListMode ();
   }
}

void CApr2::OnUpdateCompactMode(CCmdUI *pCmdUI)
{
	   // TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CDbPropertyPage *Page = (CDbPropertyPage *) dlg.GetActivePage ();
   if (Page->IsKindOf( RUNTIME_CLASS( CNewPrPage)))
   {
	   pCmdUI->Enable (TRUE);
	   if (((CNewPrPage *) Page)->IsCompactMode ())
	   {
		   pCmdUI->SetCheck (TRUE);
	   }
	   else
	   {
		   pCmdUI->SetCheck (FALSE);
	   }
   }
   else
   {
		   pCmdUI->Enable (FALSE);
   }
}



