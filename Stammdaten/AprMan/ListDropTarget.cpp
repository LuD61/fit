#include "stdafx.h"
#include "ListDropTarget.h"

CListDropTarget::CListDropTarget () : COleDropTarget ()
{
}

void CListDropTarget::SetpWnd (CWnd *pWnd)
{
	this->pWnd = pWnd;
}

DROPEFFECT CListDropTarget::OnDragEnter (CWnd* mWnd,  COleDataObject* pDataObject,
					                 	 DWORD dwKeyState,   CPoint point)
{

	return DROPEFFECT_COPY;
}

DROPEFFECT CListDropTarget::OnDragOver (CWnd* mWnd,  COleDataObject* pDataObject,
					                 	 DWORD dwKeyState,   CPoint point)
{
	return DROPEFFECT_COPY;
}


DROPEFFECT CListDropTarget::OnDropEx (CWnd* pWnd,  COleDataObject* pDataObject,
					                 	  DROPEFFECT dropDefault, DROPEFFECT dropList,
										  CPoint point)
{
	return DROPEFFECT_COPY;
}
