#include "StdAfx.h"
#include "aktkrzpreise.h"

CAktKrzPreise::CAktKrzPreise(void)
{
	cEk = _T("0.0");
	cVk = _T("0.0");
	memcpy (&akt_krz, &akt_krz_null, sizeof (AKT_KRZ));
}

CAktKrzPreise::CAktKrzPreise(CString& cEk, CString& cVk, AKT_KRZ& akt_krz)
{
	this->cEk = cEk.Trim ();
	this->cVk = cVk.Trim ();
	memcpy (&this->akt_krz, &akt_krz, sizeof (AKT_KRZ));
}

CAktKrzPreise::~CAktKrzPreise(void)
{
}
