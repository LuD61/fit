#include "StdAfx.h"
#include "preise.h"

CPreise::CPreise(void)
{
	cEk = _T("0.0");
	cVk = _T("0.0");
	memcpy (&a_pr, &a_pr_null, sizeof (A_PR));
}

CPreise::CPreise(CString& cEk, CString& cVk, A_PR& a_pr)
{
	this->cEk = cEk.Trim ();
	this->cVk = cVk.Trim ();
	memcpy (&this->a_pr, &a_pr, sizeof (A_PR));
}

CPreise::~CPreise(void)
{
}
