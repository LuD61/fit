#pragma once
#include "afxwin.h"
#include "a_kalkhndw.h"
#include "a_kalk_eig.h"
#include "ptabn.h"
#include "FormTab.h"


// CGrundPreis-Dialogfeld

class CGrundPreis : public CDialog
{
	DECLARE_DYNAMIC(CGrundPreis)

public:
	CGrundPreis(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CGrundPreis();

// Dialogfelddaten
	enum { IDD = IDD_GRUND_PREIS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();    // DDX/DDV-Unterstützung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()
public:

	enum
	{
		Hndw = 1,
		Eig =  2,
	};

	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	HBRUSH EditBrush;

	CString Pe;
	double Ekd;
	double Ek;
	double Sk;
	double SpVk;

	CStatic m_LPe;
	CEdit m_Pe;
	CStatic m_LEkd;
	CEdit m_Ekd;
	CStatic m_LEk;
	CEdit m_Ek;
	CStatic m_LSk;
	CEdit m_Sk;
	CEdit m_SpVk;
	A_KALKHNDW_CLASS A_kalkhndw;
	A_KALK_EIG_CLASS A_kalk_eig;
    PTABN_CLASS Ptabn;

	CFormTab Form;

	void Show (short mdn, short fil, double a, short a_typ, short me_einh);
	void SetVisible (BOOL visible = TRUE);
};
