#ifndef _CHOICEHWG_DEF
#define _CHOICEHWG_DEF

#include "ChoiceX.h"
#include "HWGList.h"
#include <vector>

class CChoiceHWG : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;

    public :
	    std::vector<CHWGList *> HWGList;
	    std::vector<CHWGList *> SelectList;
        CChoiceHWG(CWnd* pParent = NULL);   // Standardkonstruktor
        ~CChoiceHWG();
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchABz1 (CListCtrl *, LPTSTR);
        void SearchABz2 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CHWGList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		void SaveSelection (CListCtrl *ListBox);
};
#endif
