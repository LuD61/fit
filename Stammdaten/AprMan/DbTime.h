#ifdef WIN32
#include <windows.h>
#endif
#include <odbcinst.h>
#include <time.h>

class DayTime
{
     public :
		 long lTime;
		 CString Time;
		 DayTime (char *t);
		 DayTime (CString *t);
		 DayTime *Create (char *t);
		 DayTime *Create (CString *t);
		 long GetLongTime ();
		 CString *GetTextTime ();
};

class DbTime
{
     private :
		  CString tDate;
		  DATE_STRUCT ds;
          SYSTEMTIME st;
		  FILETIME ft;
		  ULARGE_INTEGER ul;
		  time_t ldat;
     public :
		  DbTime ();
	      DbTime (DATE_STRUCT *);
		  time_t GetTime ();
          int GetWeekDay ();
		  void SetTime (time_t);
		  void SetDateStruct ();
		  CString* GetStringTime ();
		  DATE_STRUCT *GetDateStruct (DATE_STRUCT *);
#ifdef WIN32
          DbTime& operator+ (int);
          DbTime& operator- (int);
		  BOOL operator== (DbTime&);
		  BOOL operator< (DbTime&);
		  BOOL operator> (DbTime&);
		  BOOL operator<= (DbTime&);
		  BOOL operator>= (DbTime&);
#endif
          DbTime* Add (int);
          DbTime* Sub (int);
		  BOOL Equals (DbTime&);
		  BOOL Less (DbTime&);
		  BOOL Greater (DbTime&);
		  BOOL LessEqual (DbTime&);
		  BOOL GreaterEqual (DbTime&);
};
