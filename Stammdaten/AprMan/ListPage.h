#pragma once
#include "afxcmn.h"
#include "CtrlGrid.h"
#include "CmpPrListCtrl.h"
#include "FillList.h"
#include "AprUpdateEvent.h"
#include "a_bas.h"
#include "mdn.h"
#include "fil.h"
#include "gr_zuord.h"
#include "Adr.h"
#include "Price.h"
#include "APreise.h"
#include "PrProt.h"
#include "Vector.h"
#include "PrProt.h"
#include "mo_progcfg.h"
#include "Etikett.h"
#include "Sys_par.h"
#include "Calculate.h"
#include "Ptabn.h"
#include "Sys_par.h"

// CListPage-Dialogfeld

class CListPage : public CDialog, public CAprUpdateEvent,
				  CListChangeHandler	
{
	DECLARE_DYNAMIC(CListPage)

public:
	CListPage(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CListPage();

// Dialogfelddaten
	enum { IDD = IDD_LIST_PAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
	virtual void OnSize (UINT, int, int);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
	PROG_CFG Cfg;
	CEtikett Etikett;

	BOOL Write160;
	static HANDLE Write160Lib;
    int (*dbild160)(LPSTR); 
    BOOL (*dOpenDbase)(LPSTR); 

    CImageList image; 
	int CellHeight;
	CString ArtSelect;

	CString PersName;
	CPrice A_pr;
	AKT_KRZ_CLASS Akt_krz;	// 070613
	AKT_KRZA_CLASS Akt_krza;	// 070613
	A_BAS_CLASS A_bas;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	FIL_CLASS Fil;
	GR_ZUORD_CLASS Gr_zuord;
	ADR_CLASS FilAdr;
	SYS_PAR_CLASS Sys_par;
	PTABN_CLASS Ptabn;
	CCalculate Calculate;

	CVector DbRows;
	CVector ListRows;
	int AprCursor;
	int MdnGrCursor;
	int MdnCursor;
	int FilCursor;
	CFont Font;
	CFont lFont;
	CCtrlGrid CtrlGrid;
	CFillList FillList;
	CCmpPrListCtrl m_List;
	CPrProt PrProt;

    BOOL InList (APR_CLASS& A_pr);
    void ReadCfg ();
	void StopEnter ();
	void DestroyRows(CVector &Rows);
	BOOL TestWrite ();
	virtual BOOL Write ();
	virtual BOOL DeleteAll ();
    BOOL TestDecValues (A_PR *a_pr);
	BOOL IsChanged (CAPreise *pApr, CAPreise *old_a_pr);
	void DeleteDbRows ();
    void FillMdnGrCombo ();
    void FillMdnCombo ();
    void FillFilCombo ();

// Methoden von CUpdateEvent
	virtual void Read (CString& Select, CString& CmpSelect );
	virtual void Read (CString& Select, short mdn_gr, short mdn, short fil_gr, short fil);
	virtual void SetMdn (short mdn);
	virtual void SetPrVkVisible (BOOL visible);

//	ListChangeHandler

	virtual void RowChanged (int NewRow);
	virtual void ColChanged (int Row, int Col);
};
