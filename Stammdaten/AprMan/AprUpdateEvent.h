#pragma once
#include "updateevent.h"

class CAprUpdateEvent :
	public CUpdateEvent
{
public:
	std::vector<CAprUpdateEvent *> UpdateTab;
	CAprUpdateEvent(void);
	~CAprUpdateEvent(void);
	void AddUpdateEvent (CAprUpdateEvent *UpdateEvent);
	virtual void Read (CString& Select, CString&CmpSelect) {}
	virtual void Read (CString& Select, short MdnGr, short Mdn, short FilGr, short Fil) {}
	virtual void SetMdn (short mdn) {}
	virtual void SetPrVkVisible (BOOL visible) {}
};
