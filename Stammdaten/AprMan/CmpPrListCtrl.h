#pragma once
#include <vector>
#include "editlistctrl.h"
#include "a_bas.h"
#include "mdn.h"
#include "fil.h"
#include "gr_zuord.h"
#include "Adr.h"
#include "a_pr.h"
#include "ChoiceA.h"
#include "ChoiceMdn.h"
#include "Choicefil.h"
#include "ChoiceGrZuord.h"
#include "a_kalkpreis.h"

#define MAXLISTROWS 30

class CCmpPrListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
	};

	enum LISTPOS
	{
		POSA = 1,
		POSMDNGR = 2,
		POSMDN    = 3,
		POSFILGR = 4,
		POSFIL  = 5,
		POSPREK     = 6,
		POSPRVK     = 7,
		POSPREKKALK     = 8,
		POSPRVKKALK     = 9,
		POSSK     = 10,
		POSSP     = 11,
	};

	int PosA;
	int PosMdnGr;
    int PosMdn;
	int PosFilGr;
	int PosFil;
	int PosPrEk;
	int PosPrVk;
	int PosPrEkKalk;
	int PosPrVkKalk;
	int PosSk;
	int PosSp;
	int scale;

    int *Position[13];

	short m_Mdn;
	int FilGrCursor;
	int FilCursor;
	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	int oldsel;
	std::vector<BOOL> vSelect;
	CVector MdnGrCombo;
	CVector MdnCombo;
	CVector FilGrCombo;
	CVector FilCombo;
	CString ArtSelect;
	CChoiceA *ChoiceA;
	BOOL ModalChoiceA;
	BOOL AChoiceStat;
	CChoiceGrZuord *ChoiceMdnGr;
	BOOL ModalChoiceMdnGr;
	BOOL MdnGrChoiceStat;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	BOOL MdnChoiceStat;
	CChoiceGrZuord *ChoiceFilGr;
	BOOL ModalChoiceFilGr;
	BOOL FilGrChoiceStat;
	CChoiceFil *ChoiceFil;
	BOOL ModalChoiceFil;
	BOOL FilChoiceStat;
	CVector ListRows;

	A_BAS_CLASS A_bas;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	FIL_CLASS Fil;
	GR_ZUORD_CLASS Gr_zuord;
	ADR_CLASS FilAdr;
	A_KALKPREIS_CLASS A_kalkpreis;
	double sk_vollk;
	double spanne;
	double fil_ek_vollk;
	double fil_vk_vollk;
	CCmpPrListCtrl(void);
	~CCmpPrListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	void FillMdnGrCombo (CVector&);
	void FillMdnCombo (CVector&);
	void FillFilGrCombo (CVector&);
	void FillFilCombo (CVector&);
	void OnChoice ();
	void OnAChoice (CString &);
	void OnMdnGrChoice (CString &);
	void OnMdnChoice (CString &);
	void OnFilGrChoice (CString &);
	void OnFilChoice (CString &);
	void OnKey9 ();
    void ReadA ();
    void ReadMdnGr ();
    void ReadMdn ();
    void ReadFilGr ();
    void ReadFil ();
    void FillFilGrCombo (int row);
    void FillFilCombo (int row);
    void ReadFilCombo (int mdn);
    void ReadFilGrCombo (int mdn);
    void GetColValue (int row, int col, CString& Text);
    void TestAprIndex ();
    BOOL TestAprIndexM (int EditRow);
	void ScrollPositions (int pos);
	BOOL LastCol ();
	void Prepare ();
};
