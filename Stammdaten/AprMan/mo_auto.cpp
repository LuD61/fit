#include "stdafx.h"
#include "mo_auto.h"

char* AUTO_CLASS::clipped (LPTSTR str)
/**
Blank am Ende eines Strings abschneiden.
**/
{
	      CString Str = str;
		  Str.TrimRight ();
          _tcscpy (str, Str.GetBuffer ());
          return (str);
}


BOOL AUTO_CLASS::GenAutoNr (short mdn, short fil, LPTSTR nr_nam)
{
       extern short sql_mode; 
	   int i;
	   static int MAXWAIT = 100;
       MSG msg;

	   Nr = 0l;
       AutoNrClass.beginwork ();
       sql_mode = 1;
	   i = 0;
       while (TRUE)
       {
                dsqlstatus = nvholid (mdn, fil, nr_nam);

				if (dsqlstatus == -1)
				{
					        AutoNrClass.dbdelete_nr_nam ();
							dsqlstatus = 100;
				} 		
					   
                if (dsqlstatus == 100)
                {
                           dsqlstatus = nvanmprf (mdn,
                                                  fil,
                                                  nr_nam,
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = nvholid (mdn,
                                                         fil,
                                                        nr_nam);
                           }
                }

                if (dsqlstatus == 0) 
				{
					Nr = auto_nr.nr_nr;
					return TRUE;
				}
                Sleep (50);
                if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
                {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                }

				i ++;
				if (i > MAXWAIT) break;
       }
       sql_mode = 0;
       AutoNrClass.commitwork ();
	   return FALSE;
}

int AUTO_CLASS::nvholidk2 (short mdn, short fil, char *nr_nam)
/**
Naechste freie Numnmer holen.
**/
{

             memcpy (&auto_nr, &auto_nr_null, sizeof (struct AUTO_NR));
             auto_nr.mdn = mdn;
             auto_nr.fil = fil;
             strcpy (auto_nr.nr_nam, nr_nam);
             clipped (auto_nr.nr_nam); 
             dsqlstatus = AutoNrClass.dbreadfirst_2 ();
		     if (dsqlstatus < 0) return dsqlstatus;

             if (dsqlstatus == 100)
             {
						  return dsqlstatus;
             }

             AutoNrClass.dbdelete ();
			 if (auto_nr.nr_nr >= auto_nr.max_wert) return -1;
             auto_nr.nr_nr += (long) 1;
             AutoNrClass.dbupdate_2 ();
             auto_nr.nr_nr -= (long) 1;
             return dsqlstatus;
}
           

int AUTO_CLASS::nvholid (short mdn, short fil, char *nr_nam)
/**
Naechste freie Numnmer holen.
**/
{

             memcpy (&auto_nr, &auto_nr_null, sizeof (struct AUTO_NR));
             auto_nr.mdn = mdn;
             auto_nr.fil = fil;
             strcpy (auto_nr.nr_nam, nr_nam);
             clipped (auto_nr.nr_nam); 
             dsqlstatus = AutoNrClass.dbreadfirst_1 ();
		     if (dsqlstatus < 0) return dsqlstatus;

             if (dsqlstatus == 100)
             {
                          dsqlstatus = AutoNrClass.dbreadfirst_2 ();
						  if (dsqlstatus) return dsqlstatus;
             }
             if (auto_nr.satz_kng == 1)
             {
                          AutoNrClass.dbdelete ();
             }
             else
             {
                          AutoNrClass.dbdelete ();
				          if (auto_nr.nr_nr >= auto_nr.max_wert) return -1;
                          auto_nr.nr_nr += (long) 1;
                          AutoNrClass.dbupdate_2 ();
                          auto_nr.nr_nr -= (long) 1;
             }
             return dsqlstatus;
}
            
int AUTO_CLASS::nvanmprf (short mdn, short fil, char *nr_nam, long nr_nr,
                          long max_wert, long nr_char_lng, char *fest_teil)
/**
Neue Nummer generieren.
**/
{
             auto_nr.mdn      = mdn;             
             auto_nr.fil      = fil;             
             strcpy (auto_nr.nr_nam, nr_nam);             
             dsqlstatus = AutoNrClass.dbdelete_nr_nam ();
             auto_nr.nr_nr    = nr_nr;             
             auto_nr.max_wert = max_wert;             
             auto_nr.nr_char_lng  = nr_char_lng;             
             sprintf (auto_nr.nr_char, "%ld", nr_char_lng);
             strcpy (auto_nr.fest_teil, fest_teil);
             strcpy (auto_nr.nr_komb, " ");
             auto_nr.delstatus = 0;
             auto_nr.satz_kng = 2;
             dsqlstatus = AutoNrClass.dbupdate ();
             return dsqlstatus;
}

int AUTO_CLASS::nveinid0 (short mdn, short fil, char *nr_nam, long nr_nr)
/**
Nummer freigeben.
**/
{
             auto_nr.mdn      = mdn;             
             auto_nr.fil      = fil;             
             strcpy (auto_nr.nr_nam, nr_nam);             
           
             dsqlstatus = AutoNrClass.dbreadfirst_0 ();
             auto_nr.nr_nr    = nr_nr;             
             auto_nr.satz_kng = 1;
             dsqlstatus = AutoNrClass.dbupdate ();
             return dsqlstatus;
}  

int AUTO_CLASS::nveinid (short mdn, short fil, char *nr_nam, long nr_nr)
/**
Nuumer freigeben.
**/
{
	         static int maxwait = 50;
			 int i;
             extern short sql_mode;
			 int dsqlstatus;

			 sql_mode = 1;
             dsqlstatus = nveinid0 (mdn, fil, nr_nam, nr_nr);
			 i = 0;
			 while (dsqlstatus != 0)
			 {
				 i ++;
				 if (i == maxwait) break;
				 Sleep (50);
                 dsqlstatus = nveinid0 (mdn, fil, nr_nam, nr_nr); 
			 }
			 if (dsqlstatus != 0)
			 {
				 CString Message;
				 Message.Format (_T("Nummer %ld konnte nicht freigegeben werden"), nr_nr);
				 AfxMessageBox (Message.GetBuffer (), MB_OK, 0);
			 }
			 sql_mode = 0;
			 return dsqlstatus;
}


           

