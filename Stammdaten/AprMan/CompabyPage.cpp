// CompabyPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#ifdef ARTIKEL
#include "Artikel.h"
#else
#include "AprMan.h"
#endif
#include "CompabyPage.h"


// CCompabyPage-Dialogfeld

IMPLEMENT_DYNAMIC(CCompabyPage, CPropertyPage)
CCompabyPage::CCompabyPage()
	: CPropertyPage(CCompabyPage::IDD)
{
}

CCompabyPage::~CCompabyPage()
{
}

void CCompabyPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN_GR, m_LMdnGr);
	DDX_Control(pDX, IDC_MDN_GR, m_MdnGr);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_LFIL_GR, m_LFilGr);
	DDX_Control(pDX, IDC_FIL_GR, m_FilGr);
	DDX_Control(pDX, IDC_LFIL, m_LFil);
	DDX_Control(pDX, IDC_FIL, m_Fil);
	DDX_Control(pDX, IDC_LHWG, m_LHwg);
	DDX_Control(pDX, IDC_HWG, m_Hwg);
	DDX_Control(pDX, IDC_LWG, m_LWg);
	DDX_Control(pDX, IDC_WG, m_Wg);
	DDX_Control(pDX, IDC_LAG, m_LAg);
	DDX_Control(pDX, IDC_AG, m_Ag);
	DDX_Control(pDX, IDC_LTEIL_SMT, m_LTeilSmt);
	DDX_Control(pDX, IDC_TEIL_SMT, m_TeilSmt);
	DDX_Control(pDX, IDC_LA_TYP, m_LATyp);
	DDX_Control(pDX, IDC_A_TYP, m_ATyp);
}


BEGIN_MESSAGE_MAP(CCompabyPage, CPropertyPage)
END_MESSAGE_MAP()


// CCompabyPage-Meldungshandler
