#ifndef _CHOICEPTAB_DEF
#define _CHOICEPTAB_DEF

#include "ChoiceX.h"
#include "PtabList.h"
#include <vector>

class CChoicePtab : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;

    public :
		CString Ptitem;
		CString Caption;
	    std::vector<CPtabList *> PtabList;
	    std::vector<CPtabList *> SelectList;
        CChoicePtab(CWnd* pParent = NULL);   // Standardkonstruktor
        ~CChoicePtab();
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchABz1 (CListCtrl *, LPTSTR);
        void SearchABz2 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CPtabList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		void SaveSelection (CListCtrl *ListBox);
};
#endif
