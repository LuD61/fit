#pragma once
#include "Smt_zuord.h"
#include "A_bas.h"
#include "A_pr.h"
#include "Mdn.h"
#include "Fil.h"

class CSmt
{
protected:
	SMT_ZUORD_CLASS Smt_zuord;
	MDN_CLASS Mdn;
	FIL_CLASS Fil;
	APR_CLASS A_pr;
	short MdnCursor;
	short FilCursor;
	short HwgCursor;
	short HwgCursor2;
	short WgCursor;
	short WgCursor2;
	short AgCursor;
	void Prepare ();
public:
	A_BAS_CLASS *A_bas;
	CSmt(void);
	~CSmt(void);
	BOOL TestSmt (short mdn, short fil);
	BOOL TestPrSmt (short mdn, short fil);
};
