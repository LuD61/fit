#ifndef _AKTKRZ_LIST_DEF
#define _AKTKRZ_LIST_DEF
#pragma once
#include <sqlext.h>

class CAktKrzList
{
public:
	short mdn_gr;
	short mdn;
	short fil_gr;
	short fil;
	short anzahl;
	CString zus_bz;
	DATE_STRUCT lad_akt_von; 
	CAktKrzList(void);
	CAktKrzList(short, short,short,short, LPTSTR);
	CAktKrzList(short, short,short,short, LPTSTR, DATE_STRUCT,short);
	~CAktKrzList(void);
};
#endif
