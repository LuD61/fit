#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "DbPropertyPage.h"
#include "PrComboBox.h"
#include "CtrlGrid.h"
#include "CmpPrListCtrl.h"
#include "mo_progcfg.h"
#include "adr.h"
#include "Mdn.h"
#include "A_bas.h"
#include "A_pr.h"
#include "A_pr_list.h"
#include "FormTab.h"
#include "ChoiceAg.h"
#include "ChoiceWg.h"
#include "ChoiceHwg.h"
#include "NumEdit.h"
#include "StaticButton.h"
#include "AprUpdateEvent.h"
#include "ChoiceMdn.h"
#include "Sys_ben.h"
#include "Mdn.h"
#include "Fil.h"
#include "Sys_par.h"
#include "DllPreise.h"

#define IDC_HWGCHOICE 11001
#define IDC_WGCHOICE  11002
#define IDC_AGCHOICE  11003
#define IDC_TEILSMTCHOICE  11004
#define IDC_ATYPCHOICE  11005

#define IDC_PROPEN 11006
#define IDC_MDNUSERCHOICE  11007
#define IDC_PRPRINT 11008

// CCompanyPage-Dialogfeld

class CCompanyPage : public CDbPropertyPage, public CAprUpdateEvent
{
	DECLARE_DYNAMIC(CCompanyPage)

public:
	CCompanyPage();
	virtual ~CCompanyPage();

// Dialogfelddaten
	enum { IDD = IDD_COMPANY_PR_PAGE };

	enum LIST_TYP
	{
		Preise = 0,
		Aktionen = 1,
		PreiseAktionen = 2,
	};

	short ListType;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
	virtual void OnSize (UINT, int, int);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor); 
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
	CWnd *Frame;
	PROG_CFG Cfg;

	A_BAS_CLASS A_bas;
	APR_CLASS A_pr;
	A_PR_LIST_CLASS A_pr_list;
	ADR_CLASS MdnAdrUser;
	MDN_CLASS MdnUser;
	SYS_BEN_CLASS Sys_ben;
	SYS_PAR_CLASS Sys_par;
	MDN_CLASS cMdn;
	FIL_CLASS cFil;
	CDllPreise DllPreise;

	COLORREF DlgBkColor;
	HBRUSH DlgBrush;

	short MdnGr;
	short Mdn;
	short FilGr;
	short Fil;

	CString PersName;
	CString Hwg;
	CString Wg;
	CString Ag;

	CString TeilSmt;
	CString ATyp;

	int MdnGrCursor;
	int MdnCursor;
	int FilGrCursor;
	int FilCursor;
	int HwgCursor;
	int WgCursor;
	int AgCursor;
	int PtabCursor;

	short gr;
	short EkFilGr;
	TCHAR gr_bz1 [25];

	short mdn;
	short fil;
	TCHAR adr_krz [17];
	short PrDiff;

	CStatic m_LMdnGr;
	CPrComboBox m_MdnGr;
	CStatic m_LMdn;
	CPrComboBox m_Mdn;
	CStatic m_LFilGr;
	CPrComboBox m_FilGr;
	CStatic m_LFil;
	CPrComboBox m_Fil;
	CStatic m_LListTyp;
	CPrComboBox m_ListTyp;

	CStatic m_LHwg;
	CEdit m_Hwg;
	CButton m_HwgChoice;
	CStatic m_LWg;
	CEdit m_Wg;
	CButton m_WgChoice;
	CStatic m_LAg;
	CEdit m_Ag;
	CButton m_AgChoice;

	CStatic m_LTeilSmt;
	CEdit m_TeilSmt;
	CButton m_TeilSmtChoice;
	CStatic m_LATyp;
	CEdit m_ATyp;
	CButton m_ATypChoice;

	CButton m_PrDiff;
	CStaticButton m_Open;
	CStaticButton m_Print;

	CStatic m_LMdnUser;
	CNumEdit m_MdnUser;
	CEdit m_MdnAdrKrz;
	CButton m_MdnUserChoice;

	CCtrlGrid UserGrid;
	CCtrlGrid MdnUserGrid;

	CCtrlGrid HwgGrid;
	CCtrlGrid WgGrid;
	CCtrlGrid AgGrid;
	CCtrlGrid TeilSmtGrid;
	CCtrlGrid ATypGrid;

	CCtrlGrid CtrlGrid;
	CCtrlGrid CompanyGrid;
	CCtrlGrid GrGrid;
	CCtrlGrid SmtGrid;
	CStatic m_CmpGroup;
	CStatic m_GrGroup;
	CStatic m_SmtGroup;
	CFont Font;
	CFont lFont;
    CFormTab Form;

	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceHWG *ChoiceHWG;
	BOOL ModalChoiceHWG;
	CChoiceWG *ChoiceWG;
	BOOL ModalChoiceWG;
	CChoiceAG *ChoiceAG;
	BOOL ModalChoiceAG;

	void FillMdnGrCombo ();
	void FillMdnCombo ();
	void FillFilGrCombo ();
	void FillFilCombo ();
	void FillListTypCombo ();

	void FillHwgCombo ();
	void FillWgCombo ();
	void FillAgCombo ();

	void FillTeilSmtCombo ();
	void FillATypCombo ();
	afx_msg void OnCbnSelchangeMdn();
	afx_msg void OnCbnSelchangeFil();
	afx_msg void OnHAgChoice ();
    void OnMdnchoice(); 
	void OnHwgChoice ();
	void OnWgChoice ();
	void OnAgChoice ();
	void OnTeilSmtChoice ();
	void OnATypChoice ();
	CFillList FillList;
	CCmpPrListCtrl m_List;
	afx_msg void OnOpen ();
	afx_msg void OnPrint ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	BOOL ReadMdn ();
	afx_msg void OnEnChangeMdnUser();
	BOOL Print ();
	BOOL Print (CString& Select, CString& CmpSelect);
	BOOL Print (CString& Select, short mdn_gr, short mdn, short fil_gr, short fil);
	short GetAktPrice (double a, short mdn_gr, short mdn, short fil_gr, short fil);
	void ReadCfg ();
};
