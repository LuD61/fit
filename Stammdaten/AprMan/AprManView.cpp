// AprManView.cpp : Implementierung der Klasse CAprManView
//

#include "stdafx.h"
#include "AprMan.h"

#include "AprManDoc.h"
#include "AprManView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAprManView

IMPLEMENT_DYNCREATE(CAprManView, CView)

BEGIN_MESSAGE_MAP(CAprManView, CView)
	// Standarddruckbefehle
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CAprManView Erstellung/Zerst�rung

CAprManView::CAprManView()
{
	// TODO: Hier Code zum Erstellen einf�gen

}

CAprManView::~CAprManView()
{
}

BOOL CAprManView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CView::PreCreateWindow(cs);
}

// CAprManView-Zeichnung

void CAprManView::OnDraw(CDC* /*pDC*/)
{
	CAprManDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: Code zum Zeichnen der systemeigenen Daten hinzuf�gen
}


// CAprManView drucken

BOOL CAprManView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void CAprManView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void CAprManView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// CAprManView Diagnose

#ifdef _DEBUG
void CAprManView::AssertValid() const
{
	CView::AssertValid();
}

void CAprManView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CAprManDoc* CAprManView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CAprManDoc)));
	return (CAprManDoc*)m_pDocument;
}
#endif //_DEBUG


// CAprManView Meldungshandler
