#ifndef _HWG_LIST_DEF
#define _HWG_LIST_DEF
#pragma once

class CHWGList
{
public:
	long hwg;
	CString hwg_bz1;
	CString hwg_bz2;
	CHWGList(void);
	CHWGList(long, LPTSTR, LPTSTR);
	~CHWGList(void);
};
#endif
