#include "StdAfx.h"
#include "AprTester.h"

CAprTester::CAprTester(void)
{
	Clear ();
	cursor = -1;
	cursor_akt_exist = -1;
	akt_status = 0;
}

CAprTester::~CAprTester(void)
{
	if (cursor != -1)
	{
		DbClass.sqlclose (cursor);
	}
	if (cursor_akt_exist != -1)
	{
		DbClass.sqlclose (cursor_akt_exist);
	}
}

void CAprTester::Prepare ()
{
	if (cursor == -1)
	{
		DbClass.sqlin ((short *)  &mdn_gr, SQLSHORT, 0);
		DbClass.sqlin ((short *)  &mdn,    SQLSHORT, 0);
		DbClass.sqlin ((short *)  &fil_gr, SQLSHORT, 0);
		DbClass.sqlin ((short *)  &fil,    SQLSHORT, 0);
		DbClass.sqlin ((double *) &a,      SQLDOUBLE, 0);
		DbClass.sqlin ((char *)   test_lad_akv, SQLCHAR, sizeof (test_lad_akv));

		DbClass.sqlout ((LPTSTR) lad_akv, SQLCHAR, sizeof (lad_akv));
		DbClass.sqlout ((LPTSTR) lief_akv, SQLCHAR, sizeof (lief_akv));
		cursor = DbClass.sqlcursor (_T("select lad_akv_sa, lief_akv_sa from akt_krz ")
									_T("where mdn_gr = ? ")
									_T("and mdn = ? ")
									_T("and fil_gr = ? ")
									_T("and fil = ? ")
									_T("and a = ? ")
									_T("and lad_akv_sa = ?"));
		DbClass.sqlin ((short *)  &mdn_gr, SQLSHORT, 0);
		DbClass.sqlin ((short *)  &mdn,    SQLSHORT, 0);
		DbClass.sqlin ((short *)  &fil_gr, SQLSHORT, 0);
		DbClass.sqlin ((short *)  &fil,    SQLSHORT, 0);
		DbClass.sqlin ((double *) &a,      SQLDOUBLE, 0);
		cursor_akt_exist = DbClass.sqlcursor (_T("select a from akt_krz ")
									_T("where mdn_gr = ? ")
									_T("and mdn = ? ")
									_T("and fil_gr = ? ")
									_T("and fil = ? ")
									_T("and a = ? ")
									_T("and lad_akv_sa < 2"));
	}
}

void CAprTester::Clear ()
{
	mdn_gr = 0;
	mdn = 0;
	fil_gr = 0;
	fil = 0;
	a = 0.0;
	strcpy (test_lad_akv, "0");
	strcpy (lad_akv,   "0");
	strcpy (lief_akv,  "0");
}

void CAprTester::TestLadAkv (AKT_KRZ& akt_krz, A_PR& a_pr, TCHAR *test_lad_akv)
{
	int dsqlstatus;
	Prepare ();
	if (*this != akt_krz)
	{
		Clear ();
		mdn_gr = akt_krz.mdn_gr;
		mdn    = akt_krz.mdn;
		fil_gr = akt_krz.fil_gr;
		fil    = akt_krz.fil;
		a      = akt_krz.a;
//		strcpy (lad_akv, akt_krz.lad_akv_sa);
//		strcpy (lief_akv, akt_krz.lief_akv_sa);
		strcpy (lad_akv, "0");
		strcpy (lief_akv, "0");
		strcpy (this->test_lad_akv, test_lad_akv);
		dsqlstatus = DbClass.sqlopen (cursor);
		if (dsqlstatus == 0)
		{
			dsqlstatus = DbClass.sqlfetch (cursor);
		}
	}
	strcpy (a_pr.lad_akv,  lad_akv); 
	strcpy (a_pr.lief_akv, lief_akv); 

	dsqlstatus = DbClass.sqlopen (cursor_akt_exist);
	if (dsqlstatus == 0)
	{
		dsqlstatus = DbClass.sqlfetch (cursor_akt_exist);
	}
	if (dsqlstatus == 100)
	{
		if (a_pr.akt <= 0)
		{
			a_pr.akt = -1;
		}
	}
	else
	{
		if (a_pr.akt <= 0)
		{
			a_pr.akt = 0;
		}
	}

}

void CAprTester::TestAkt (AKT_KRZ& akt_krz, A_PR& a_pr)
{
	int dsqlstatus;
	Prepare ();
	if (*this != akt_krz)
	{
		Clear ();
		mdn_gr = akt_krz.mdn_gr;
		mdn    = akt_krz.mdn;
		fil_gr = akt_krz.fil_gr;
		fil    = akt_krz.fil;
		a      = akt_krz.a;
		dsqlstatus = DbClass.sqlopen (cursor_akt_exist);
		if (dsqlstatus == 0)
		{
			dsqlstatus = DbClass.sqlfetch (cursor_akt_exist);
		}
		akt_status = dsqlstatus;
	}
	if (dsqlstatus == 100)
	{
		if (a_pr.akt <= 0)
		{
			a_pr.akt = -1;
		}
	}
	else
	{
		if (a_pr.akt <= 0)
		{
			a_pr.akt = 0;
		}
	}
}

BOOL CAprTester::operator!=(AKT_KRZ &akt_krz)
{
	BOOL ret = TRUE;
	if (mdn_gr != akt_krz.mdn_gr)
	{
		ret = FALSE;
	}
	else if (mdn != akt_krz.mdn)
	{
		ret = FALSE;
	}
	else if (fil_gr != akt_krz.fil_gr)
	{
		ret = FALSE;
	}
	else if (fil != akt_krz.fil)
	{
		ret = FALSE;
	}
	else if (a != akt_krz.a)
	{
		ret = FALSE;
	}
	return !ret;
}
