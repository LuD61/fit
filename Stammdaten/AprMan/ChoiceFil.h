#ifndef _CHOICEFIL_DEF
#define _CHOICEFIL_DEF

#include "ChoiceX.h"
#include "FilList.h"
#include <vector>

class CChoiceFil : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
      
    public :
		short m_Mdn;
	    std::vector<CFilList *> FilList;
      	CChoiceFil(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceFil(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchAdrKrz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CFilList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
