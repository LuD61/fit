#include "stdafx.h"
#include "DbClass.h"
#include "kun.h"

struct KUN kun,  kun_null;

struct TOU tou, tou_save, tou_null;

extern DB_CLASS dbClass;


static int anzzfelder ;

TOU_CLASS tou_class ;


int TOU_CLASS::lesetou (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}
int TOU_CLASS::opentou (void)
{

		if ( readcursor < 0 ) prepare ();

         return dbClass.sqlopen (readcursor);
}
void TOU_CLASS::prepare (void)
{

test_upd_cursor = 1;

	dbClass.sqlin ((long *) &tou.tou, SQLLONG, 0);


	dbClass.sqlout (( char *) tou.tou_bz,SQLCHAR, 49 ) ;
/* ->
	dbClass.sqlout (( char *)  tou.fz_kla,SQLCHAR,3) ;
	dbClass.sqlout (( char *)  tou.fz,SQLCHAR,13) ; 
	dbClass.sqlout (( char *)  tou.srt_zeit,SQLCHAR,6 ) ;
	dbClass.sqlout (( char *)  tou.dau,SQLCHAR,6 ) ;
	dbClass.sqlout (( long *) &tou.lng,SQLLONG,0) ;
	dbClass.sqlout (( char *)  tou.fah_1[13];
	dbClass.sqlout (( char *)  tou.fah_2[13];
	dbClass.sqlout (( short *)&tou.delstatus;
	dbClass.sqlout (( long *) &tou.lgr;
	dbClass.sqlout (( long *) &tou.leitw_typ;
	dbClass.sqlout (( long *) &tou.htou;
	dbClass.sqlout (( long *) &tou.adr;
	dbClass.sqlout (( short *)&tou.eigentour;
	dbClass.sqlout (( long *) &tou.lgkonto;

< ----- */

	readcursor = dbClass.sqlcursor ("select "

	" tou_bz  "

	" from tou where tou = ?  " ) ;
	
 }



int KUN_CLASS::dbcount (void)
/**
Tabelle kun lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}

KUN_CLASS kun_class ;

int KUN_CLASS::lesekun (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int KUN_CLASS::openallkun (void)
{
	itvon = 1L ;
	itbis = 99999999L ;

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}


int KUN_CLASS::openkun (void)
{

		itvon = kun.kun ;
		itbis = kun.kun ;
		if ( readcursor < 0 ) prepare ();

         return dbClass.sqlopen (readcursor);
}

/* ---->

void KUN_CLASS::prepare (void)
{

	dbClass.sqlin ((long *) &kun.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = dbClass.sqlcursor ("select count(*) from kun "
										"where kun.kun = ? and kun.mdn = ? ");
										

//	dbClass.sqlin ((long *) &kun.kun, SQLLONG, 0);
	dbClass.sqlin ((long *) &itvon, SQLLONG, 0);
	dbClass.sqlin ((long *) &itbis, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);


dbClass.sqlout ((short *) &kun.mdn, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.fil, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.kun, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.adr1, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.adr2, SQLLONG, 0);  
dbClass.sqlout ((long *) &kun.adr3, SQLLONG, 0); 
dbClass.sqlout ((char *) kun.kun_seit, SQLCHAR, 11);
dbClass.sqlout ((long *) &kun.txt_nr1, SQLLONG, 0);
dbClass.sqlout ((char *) kun.frei_txt1, SQLCHAR, 65);

dbClass.sqlout ((char *) kun.kun_krz1, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.kun_bran, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.kun_krz2, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.kun_krz3, SQLCHAR, 17);
dbClass.sqlout ((short *) &kun.kun_typ, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.bbn, SQLLONG, 0);
dbClass.sqlout ((short *) &kun.pr_stu, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.pr_lst, SQLLONG, 0);
dbClass.sqlout ((char *) kun.vereinb, SQLCHAR, 6);
dbClass.sqlout ((long *) &kun.inka_nr, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.vertr1, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.vertr2, SQLLONG, 0);
dbClass.sqlout ((char *) kun.statk_period, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.a_period, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.sprache, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.txt_nr2, SQLLONG, 0);
dbClass.sqlout ((char *) kun.frei_txt2, SQLCHAR, 65);
dbClass.sqlout ((char *) kun.freifeld1, SQLCHAR, 9);
dbClass.sqlout ((char *) kun.freifeld2, SQLCHAR, 9);
dbClass.sqlout ((long *) &kun.tou, SQLLONG, 0);
dbClass.sqlout ((char *) kun.vers_art, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.lief_art, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.fra_ko_ber, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.rue_schei, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.form_typ1, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.auflage1, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.freifeld3, SQLCHAR, 9);
dbClass.sqlout ((char *) kun.freifeld4, SQLCHAR, 9);
dbClass.sqlout ((short *) &kun.zahl_art, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.zahl_ziel, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.form_typ2, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.auflage2, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.txt_nr3, SQLLONG, 0);
dbClass.sqlout ((char *) kun.frei_txt3, SQLCHAR, 65);
dbClass.sqlout ((char *) kun.nr_bei_rech, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.rech_st, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.sam_rech, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.einz_ausw, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.gut, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.rab_schl, SQLCHAR, 9);

dbClass.sqlout ((double *) &kun.bonus1, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.bonus2, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.tdm_grenz1, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.tdm_grenz2, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.jr_plan_ums, SQLDOUBLE, 0);
dbClass.sqlout ((char *) kun.deb_kto, SQLCHAR, 9);
dbClass.sqlout ((double *) &kun.kred_lim, SQLDOUBLE, 0);
dbClass.sqlout ((short *) &kun.inka_zaehl, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.bank_kun, SQLCHAR, 37);
dbClass.sqlout ((long *) &kun.blz, SQLLONG, 0);
dbClass.sqlout ((char *) kun.kto_nr, SQLCHAR, 17);
dbClass.sqlout ((short *) &kun.hausbank, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_of_po, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_of_lf, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_of_best, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.delstatus, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.kun_bran2, SQLCHAR, 3);
dbClass.sqlout ((long *) &kun.rech_fuss_txt, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.ls_fuss_txt, SQLLONG, 0);
dbClass.sqlout ((char *) kun.ust_id, SQLCHAR, 12);
dbClass.sqlout ((long *) &kun.rech_kopf_txt, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.ls_kopf_txt, SQLLONG, 0);
dbClass.sqlout ((char *) kun.gn_pkt_kz, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.sw_rab, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.bbs, SQLCHAR, 9);
dbClass.sqlout ((long *) &kun.inka_nr2, SQLLONG, 0);
dbClass.sqlout ((short *) &kun.sw_fil_gr, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sw_fil, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.ueb_kz, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.modif, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.kun_leer_kz, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.ust_id16, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.iln, SQLCHAR, 17);
dbClass.sqlout ((short *) &kun.waehrung, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.pr_ausw, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.pr_hier, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.pr_ausw_ls, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.pr_ausw_re, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_gr1, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_gr2, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.eg_kz, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.bonitaet, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kred_vers, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.kst, SQLLONG, 0);
dbClass.sqlout ((char *) kun.edi_typ, SQLCHAR, 2);
dbClass.sqlout ((long *) &kun.sedas_dta, SQLLONG, 0);
dbClass.sqlout ((char *) kun.sedas_kz, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.sedas_umf, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_abr, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_gesch, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_satz, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_med, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.sedas_nam, SQLCHAR, 11);
dbClass.sqlout ((short *) &kun.sedas_abk1, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_abk2, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_abk3, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.sedas_nr1, SQLCHAR, 9);
dbClass.sqlout ((char *) kun.sedas_nr2, SQLCHAR, 9);
dbClass.sqlout ((char *) kun.sedas_nr3, SQLCHAR, 9);
dbClass.sqlout ((short *) &kun.sedas_vb1, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_vb2, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_vb3, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.sedas_iln, SQLCHAR, 17);
dbClass.sqlout ((long *) &kun.kond_kun, SQLLONG, 0);
dbClass.sqlout ((short *) &kun.kun_schema, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.plattform, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.be_log, SQLCHAR, 4);
dbClass.sqlout ((long *) &kun.stat_kun, SQLLONG, 0);
dbClass.sqlout ((char *) kun.ust_nummer, SQLCHAR, 25);
dbClass.sqlout ((char *) kun.fak_kz, SQLCHAR, 2);
dbClass.sqlout ((long *) &kun.fak_nr, SQLLONG, 0);

	
	readcursor = dbClass.sqlcursor ("select "

	" mdn, fil, kun, adr1, adr2, adr3, kun_seit, txt_nr1, frei_txt1, "
	" kun_krz1, kun_bran, kun_krz2, kun_krz3, kun_typ, bbn, pr_stu, "
	" pr_lst, vereinb, inka_nr, vertr1, vertr2, statk_period, a_period, "
	" sprache, txt_nr2, frei_txt2, freifeld1, freifeld2, tou, vers_art, "
	" lief_art, fra_ko_ber, rue_schei, form_typ1, auflage1, freifeld3, "
	" freifeld4, zahl_art, zahl_ziel, form_typ2, auflage2, txt_nr3, "
	" frei_txt3, nr_bei_rech, rech_st, sam_rech, einz_ausw, gut, rab_schl, "
	" bonus1, bonus2, tdm_grenz1, tdm_grenz2, jr_plan_ums, deb_kto, "
	" kred_lim, inka_zaehl, bank_kun, blz, kto_nr, hausbank, kun_of_po, "
	" kun_of_lf, kun_of_best, delstatus, kun_bran2, rech_fuss_txt, "
	" ls_fuss_txt, ust_id, rech_kopf_txt, ls_kopf_txt, gn_pkt_kz, sw_rab, "
	" bbs, inka_nr2, sw_fil_gr, sw_fil, ueb_kz, modif, kun_leer_kz,"
	" ust_id16, iln, waehrung, pr_ausw, pr_hier, pr_ausw_ls, "
	" pr_ausw_re, kun_gr1, kun_gr2, eg_kz, bonitaet, kred_vers, kst, "
	" edi_typ, sedas_dta, sedas_kz, sedas_umf, sedas_abr, sedas_gesch,"
	" sedas_satz, sedas_med, sedas_nam, sedas_abk1, sedas_abk2, sedas_abk3, "
	" sedas_nr1, sedas_nr2, sedas_nr3, sedas_vb1, sedas_vb2, sedas_vb3, "
	" sedas_iln, kond_kun, kun_schema, plattform, be_log, stat_kun, "
	" ust_nummer, fak_kz, fak_nr "

	" from kun where kun between ? and ? and mdn = ? and delstatus = 0 " ) ;
 	
  }
< ---- */
void KUN_CLASS::prepare (void)
{

	dbClass.sqlin ((long *) &kun.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = dbClass.sqlcursor ("select count(*) from kun "
										"where kun.kun = ? and kun.mdn = ? ");
										

	dbClass.sqlin ((long *) &itvon, SQLLONG, 0);
	dbClass.sqlin ((long *) &itbis, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);


dbClass.sqlout ((long *) &kun.kun, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.adr1, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.adr2, SQLLONG, 0);  
dbClass.sqlout ((long *) &kun.adr3, SQLLONG, 0); 

dbClass.sqlout ((char *) kun.kun_krz1, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.kun_krz2, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.kun_krz3, SQLCHAR, 17);
dbClass.sqlout ((long *) &kun.tou, SQLLONG, 0);

dbClass.sqlout ((short *) &kun.kun_gr1, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_gr2, SQLSHORT, 0);
	
	readcursor = dbClass.sqlcursor ("select "

	" kun, adr1, adr2, adr3 "
	" , kun_krz1, kun_krz2, kun_krz3 "
	" , tou, kun_gr1, kun_gr2 "
	" from kun where kun between ? and ? and mdn = ? and delstatus = 0 " ) ;
 	
  }
