// 18550Dlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "List1Ctrl.h"


// CMy18550Dlg-Dialogfeld
class CMy18550Dlg : public CDialog
{
// Konstruktion
public:
	CMy18550Dlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_MY18550_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_mdnname;
public:
	CString v_mdnname;
public:
	CEdit m_mdnnr;
public:
	long v_mdnnr;
public:
	CList1Ctrl m_list1;
public:
	afx_msg void OnHdnBegintrackList1(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnHdnEndtrackList1(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedCancel();
public:
	afx_msg void OnEnKillfocusMdnnr();
public:
	afx_msg void OnEnKillfocusKundnr();
public:
 	afx_msg BOOL PreTranslateMessage(LPMSG) ;

	int branrekursion ;
	char kunbran2[5] ;
	int IMeEinhCursor ;

	void FillMeEinhCombo() ;
	void enablekunbran() ;
	void disablekunbran() ;
	void setzekunbran0() ;

	bool ReadMdn() ;
	virtual BOOL Read(void);
	virtual BOOL Write(void);
	virtual BOOL ReadList(void) ;

	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);
	virtual void DestroyRows(CVector &Rows);
	virtual BOOL InList (A_KUN_CLASS &a_kun_class) ;	// Input nur dummy ?!

	CVector DbRows;
	CVector ListRows;

	CFillList FillList ;

public:
	afx_msg void OnCbnKillfocusCombobran();
public:
	afx_msg void OnCbnSelchangeCombobran();
public:
	CComboBox m_combobran;
public:
	CString v_combobran;
public:
	afx_msg void OnBnClickedButtonkun();
public:
	CEdit m_kundnr;
public:
	long v_kundnr;
public:
	CEdit m_kundname;
public:
	CString v_kundname;
public:
	CButton m_buttonkun;
public:
	afx_msg void OnCbnSetfocusCombobran();
public:
	CButton m_buttkey6;
	CButton m_buttkey7;
	CButton m_buttkey8;
	CButton m_buttkey9;
	CButton m_buttkey10;
	CButton m_buttkey11;
	void keyset(BOOL) ;
public:
	afx_msg void OnEnSetfocusKundnr();
public:
	afx_msg void OnEnSetfocusMdnnr();
	afx_msg void OnBnClickedBdruck();
	afx_msg void OnBnClickedButtkey7();
	afx_msg void OnBnClickedButtkey6();
};
