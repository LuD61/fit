#include "stdafx.h"
#include "DbClass.h"
#include "a_bas.h"

extern DB_CLASS dbClass;

struct A_EAN a_ean,  a_ean_null;

struct A_BAS a_bas,  a_bas_null;

struct A_KUN a_kun,  a_kun_null;

struct B_KUN b_kun ;

static int anzzfelder ;

struct A_HNDW	 a_hndw,	a_hndw_null ;
struct A_EIG	 a_eig,		a_eig_null ;
struct A_EIG_DIV a_eig_div, a_eig_div_null ;


int A_HNDW_CLASS::lesea_hndw (void)
{
	int di = dbClass.sqlfetch (readcursor);
	return di;
}
int A_EIG_CLASS::lesea_eig (void)
{
	int di = dbClass.sqlfetch (readcursor);
	return di;
}
int A_EIG_DIV_CLASS::lesea_eig_div (void)
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int A_HNDW_CLASS::opena_hndw (void)
{
	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (readcursor);
}
int A_EIG_CLASS::opena_eig (void)
{
	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (readcursor);
}
int A_EIG_DIV_CLASS::opena_eig_div (void)
{
	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (readcursor);
}

void A_HNDW_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &a_hndw.a, SQLDOUBLE, 0);
 
	dbClass.sqlout ((short  *) &a_hndw.mdn ,  SQLSHORT  , 0 ) ;
	dbClass.sqlout ((short  *) &a_hndw.fil ,  SQLSHORT  , 0 ) ;
	dbClass.sqlout ((double *) &a_hndw.inh ,  SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((short  *) &a_hndw.me_einh_kun,SQLSHORT,0 ) ;

    readcursor = (short) dbClass.sqlcursor (
		"select mdn, fil, inh, me_einh_kun "
		" from a_hndw where a_hndw.a = ? ");
}

void A_EIG_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &a_eig.a, SQLDOUBLE, 0);
 
	dbClass.sqlout ((short  *) &a_eig.mdn ,  SQLSHORT  , 0 ) ;
	dbClass.sqlout ((short  *) &a_eig.fil ,  SQLSHORT  , 0 ) ;
	dbClass.sqlout ((double *) &a_eig.inh ,  SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((short  *) &a_eig.me_einh_ek,SQLSHORT,0 ) ;

    readcursor = (short) dbClass.sqlcursor (
		"select mdn, fil, inh, me_einh_ek "
		" from a_eig where a_eig.a = ? ");
}

void A_EIG_DIV_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &a_eig_div.a, SQLDOUBLE, 0);
 
	dbClass.sqlout ((short  *) &a_eig_div.mdn ,  SQLSHORT  , 0 ) ;
	dbClass.sqlout ((short  *) &a_eig_div.fil ,  SQLSHORT  , 0 ) ;
	dbClass.sqlout ((double *) &a_eig_div.inh ,  SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((short  *) &a_eig_div.me_einh_ek,SQLSHORT,0 ) ;

    readcursor = (short) dbClass.sqlcursor (
		"select mdn, fil, inh, me_einh_ek "
		" from a_eig_div where a_eig_div.a = ? ");
}


int A_EAN_CLASS::lesea_ean (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_EAN_CLASS::opena_ean (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (readcursor);
}

void A_EAN_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &a_ean.a, SQLDOUBLE, 0);
 

	dbClass.sqlout ((short *)  &a_ean.delstatus,SQLSHORT,0 ) ;
	dbClass.sqlout ((double *) &a_ean.ean, SQLDOUBLE,0 ) ;
	dbClass.sqlout ((char *)    a_ean.ean_bz, SQLCHAR, 25 ) ;
	dbClass.sqlout ((char *)    a_ean.h_ean_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout ((short *)  &a_ean.ean_vk_kz, SQLSHORT, 0 ) ;


    readcursor = (short) dbClass.sqlcursor (
		"select delstatus, ean, ean_bz, h_ean_kz, ean_vk_kz "
		" from a_ean where a_ean.a = ? ");
}

A_BAS_CLASS a_bas_class ;
int A_BAS_CLASS::dbcount (void)
/**
Tabelle A_BAS lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;
		 
}

int A_BAS_CLASS::lesea_bas (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_BAS_CLASS::opena_bas (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (readcursor);
}

void A_BAS_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &a_bas.a, SQLDOUBLE, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = (short)dbClass.sqlcursor ("select count(*) from a_bas "
										"where a_bas.a = ? ");
										
	test_upd_cursor = 1;


	dbClass.sqlin ((double *) &a_bas.a, SQLDOUBLE, 0);

	dbClass.sqlout(( double *)&a_bas.a, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.fil, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.a_bz1, SQLCHAR, 25 ) ;
	dbClass.sqlout(( char  *)  a_bas.a_bz2, SQLCHAR, 25 ) ;
	dbClass.sqlout(( double *)&a_bas.a_gew, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.a_typ, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.a_typ2, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.abt, SQLSHORT, 0 ) ;
	dbClass.sqlout(( long  *) &a_bas.ag, SQLLONG, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.best_auto, SQLCHAR, 2 ) ;
	dbClass.sqlout(( char  *)  a_bas.bsd_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout(( char  *)  a_bas.cp_aufschl, SQLCHAR, 2 ) ;
	dbClass.sqlout(( short  *)&a_bas.delstatus, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.dr_folge, SQLSHORT, 0 ) ;
	dbClass.sqlout(( long  *) &a_bas.erl_kto, SQLLONG, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.hbk_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout(( short  *)&a_bas.hbk_ztr, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.hnd_gew, SQLCHAR, 2 ) ;
	dbClass.sqlout(( short  *)&a_bas.hwg, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.kost_kz, SQLCHAR, 3 ) ;
	dbClass.sqlout(( short  *)&a_bas.me_einh, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.modif, SQLCHAR, 2 ) ;
	dbClass.sqlout(( short  *)&a_bas.mwst, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.plak_div, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.stk_lst_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout(( double *)&a_bas.sw, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( short  *) &a_bas.teil_smt, SQLSHORT, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.we_kto, SQLLONG, 0 ) ;
	dbClass.sqlout(( short  *) &a_bas.wg, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *) &a_bas.zu_stoff, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)   a_bas.akv, SQLCHAR, 11 ) ; 
	dbClass.sqlout(( char  *)   a_bas.bearb, SQLCHAR, 11 ) ;
	dbClass.sqlout(( char  *)   a_bas.pers_nam, SQLCHAR , 9 ) ;
	dbClass.sqlout(( double *) &a_bas.prod_zeit, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( char  *)   a_bas.pers_rab_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout(( double *) &a_bas.gn_pkt_gbr, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.kost_st, SQLLONG, 0 ) ;
	dbClass.sqlout(( char  *)   a_bas.sw_pr_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout(( long  *)  &a_bas.kost_tr, SQLLONG, 0 ) ;
	dbClass.sqlout(( double *) &a_bas.a_grund, SQLDOUBLE,0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.kost_st2, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.we_kto2, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.charg_hand, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.intra_stat, SQLLONG, 0 ) ;
	dbClass.sqlout(( char  *)   a_bas.qual_kng, SQLCHAR, 5 ) ; 
	dbClass.sqlout(( char  *)   a_bas.a_bz3, SQLCHAR, 25 ) ;
	dbClass.sqlout(( short  *) &a_bas.lief_einh, SQLSHORT, 0 ) ;
	dbClass.sqlout(( double *) &a_bas.inh_lief, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.erl_kto_1, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.erl_kto_2, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.erl_kto_3, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.we_kto_1, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.we_kto_2, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.we_kto_3, SQLLONG, 0 ) ;
	dbClass.sqlout(( char  *)   a_bas.skto_f, SQLCHAR, 2 ) ;
	dbClass.sqlout(( double *) &a_bas.sk_vollk, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( double *) &a_bas.a_ersatz, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( short  *) &a_bas.a_ers_kz, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *) &a_bas.me_einh_abverk, SQLSHORT, 0 ) ;
	dbClass.sqlout(( double *) &a_bas.inh_abverk, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.hnd_gew_abverk, SQLCHAR, 2 ) ;


	readcursor = (short) dbClass.sqlcursor ("select "
	" a ,mdn ,fil ,a_bz1 ,a_bz2 ,a_gew ,a_typ ,a_typ2 ,abt ,ag ,best_auto "
	" ,bsd_kz ,cp_aufschl ,delstatus ,dr_folge ,erl_kto ,hbk_kz ,hbk_ztr "
	" ,hnd_gew , hwg ,kost_kz ,me_einh ,modif , mwst ,plak_div ,stk_lst_kz "
	" ,sw ,teil_smt ,we_kto ,wg ,zu_stoff ,akv ,bearb ,pers_nam ,prod_zeit "
	" ,pers_rab_kz ,gn_pkt_gbr ,kost_st ,sw_pr_kz ,kost_tr ,a_grund ,kost_st2 "
	" ,we_kto2 ,charg_hand ,intra_stat ,qual_kng ,a_bz3 ,lief_einh ,inh_lief "
	" ,erl_kto_1 ,erl_kto_2 ,erl_kto_3 ,we_kto_1 ,we_kto_2 ,we_kto_3 ,skto_f "
	" ,sk_vollk ,a_ersatz ,a_ers_kz ,me_einh_abverk , inh_abverk ,hnd_gew_abverk "

	" from a_bas where a = ? " ) ;
	
	
}

A_KUN_CLASS a_kun_class ;

/* --->
int A_KUN_CLASS::deletea_kun (void)
{
	if ( readcursor < 0 ) prepare ();	
	int di = dbClass.sqlexecute (del_cursor);
	return di;
}
< --- */
int A_KUN_CLASS::deletea_kun (void)
{
	preparedel() ;	// immer wieder neu preparieren 	
	int di = dbClass.sqlexecute (del_cursor);
	return di;
}


int A_KUN_CLASS::inserta_kun (void)
{
	if ( readcursor < 0 ) prepare ();	
	int di = dbClass.sqlexecute (ins_cursor);
	return di;
}

int A_KUN_CLASS::upda_kun (void)
{
	if ( readcursor < 0 ) prepare ();	
	int di = dbClass.sqlexecute (upd_cursor);
	return di;
}


int A_KUN_CLASS::lesealla_kun (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_KUN_CLASS::lesea_kun (void)
{
      int di = dbClass.sqlfetch (test_upd_cursor);
	  return di;
}

int A_KUN_CLASS::openalla_kun (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (readcursor);
}
int A_KUN_CLASS::opena_kun (void)
{

		if ( test_upd_cursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (test_upd_cursor);
}

void A_KUN_CLASS::preparedel(void)
{
	char hilfehilfe[ 3300 ] ;	// 141009 : 2300->3300

	if ( del_cursor > -1 )
		dbClass.sqlclose ( del_cursor ) ;
	dbClass.sqlin (( short *)&a_kun.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin (( long *) &a_kun.kun, SQLLONG, 0 ) ;
	dbClass.sqlin (( char *)  a_kun.kun_bran2, SQLCHAR, 3 ) ;
	sprintf ( hilfehilfe ," delete from a_kun where mdn = ? and kun = ? and kun_bran2 = ? and fil = 0 "
		" and a not in (  %s ) and a between %1.0f and %1.0f " ,b_kun.inakun, b_kun.vonwert, b_kun.biswert ) ;
		// 141009 : viele Saetze korrekt handeln  

	del_cursor = (short) dbClass.sqlcursor ( hilfehilfe ) ;

/* --->
	del_cursor = (short) dbClass.sqlcursor ("delete "
	
	" from a_kun where mdn = ? and kun = ? and kun_bran2 = ? "
	" and a not in ( " 
	b_kun.inakun  
	" ) " ) ;
< ---- */
}

void A_KUN_CLASS::prepare (void)
{

// lese einen Artikel .....( test_upd_cursor )

dbClass.sqlin (( short *)&a_kun.mdn, SQLSHORT, 0 ) ;
dbClass.sqlin (( long *) &a_kun.kun, SQLLONG, 0 ) ;
dbClass.sqlin (( char *)  a_kun.kun_bran2, SQLCHAR, 3 ) ;
dbClass.sqlin (( double *) &a_kun.a, SQLDOUBLE, 0 ) ;
	
	dbClass.sqlout (( short *) &a_kun.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.fil, SQLSHORT, 0 ) ;
	dbClass.sqlout (( long *) &a_kun.kun, SQLLONG, 0 ) ;
	dbClass.sqlout (( double *) &a_kun.a, SQLDOUBLE, 0 ) ; 
	dbClass.sqlout (( char *) a_kun.a_kun, SQLCHAR, 14 );	// 131009 mea culpa
	dbClass.sqlout (( char *) a_kun.a_bz1, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlout (( short *) &a_kun.me_einh_kun, SQLSHORT, 0 ) ;
	dbClass.sqlout (( double *) &a_kun.inh, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( char *) a_kun.kun_bran2, SQLCHAR, 3 ) ;	// 131009 mea culpa
	dbClass.sqlout (( double *) &a_kun.tara, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( double *) &a_kun.ean, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( double *) &a_kun.ean_vk, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( char *) a_kun.a_bz2, SQLCHAR, 25 ) ;		// 131009 mea culpa
	dbClass.sqlout (( short *) &a_kun.hbk_ztr, SQLSHORT, 0 ) ; 
	dbClass.sqlout (( long *) &a_kun.kopf_text, SQLLONG, 0 ) ;
	dbClass.sqlout (( char *) a_kun.pr_rech_kz, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlout (( char *) a_kun.modif, SQLCHAR, 2 ) ;		// 131009 mea culpa
	dbClass.sqlout (( long *) &a_kun.text_nr, SQLLONG, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.devise, SQLSHORT, 0 ) ;
	dbClass.sqlout (( char *) a_kun.geb_eti, SQLCHAR, 2 ) ;		// 131009 mea culpa
	dbClass.sqlout (( char *) a_kun.geb_fill, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlout (( long *) &a_kun.geb_anz, SQLLONG, 0 ) ;
	dbClass.sqlout (( char *) a_kun.pal_eti, SQLCHAR, 2 ) ;		// 131009 mea culpa
	dbClass.sqlout (( char *) a_kun.pal_fill, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlout (( short *) &a_kun.pal_anz, SQLSHORT, 0 ) ; 
	dbClass.sqlout (( char *) a_kun.pos_eti, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlout (( short *) &a_kun.sg1, SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.sg2, SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.pos_fill, SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.ausz_art, SQLSHORT, 0 ) ;
	dbClass.sqlout (( long *) &a_kun.text_nr2, SQLLONG, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.cab, SQLSHORT, 0 ) ;
	dbClass.sqlout (( char *) a_kun.a_bz3, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlout (( char *) a_kun.a_bz4, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlout (( char *) a_kun.li_a, SQLCHAR, 14 ) ;  // 131009 mea culpa       
	dbClass.sqlout (( double *) &a_kun.geb_fakt, SQLDOUBLE, 0 ) ;


	test_upd_cursor = (short) dbClass.sqlcursor ("select "
	" mdn ,fil ,kun ,a ,a_kun ,a_bz1 ,me_einh_kun ,inh ,kun_bran2 ,tara "
	" ,ean ,ean_vk ,a_bz2 ,hbk_ztr ,kopf_text ,pr_rech_kz ,modif ,text_nr ,devise "
	" ,geb_eti ,geb_fill ,geb_anz ,pal_eti ,pal_fill ,pal_anz ,pos_eti ,sg1 "
	" ,sg2 ,pos_fill ,ausz_art ,text_nr2 ,cab ,a_bz3 ,a_bz4 ,li_a ,geb_fakt "
	
	" from a_kun where mdn = ? and kun = ? and kun_bran2 = ? and a = ? and fil = 0 " ) ;
// 141009 fil = 0 dazu 

/* ---> im  preparedel :  
	dbClass.sqlin (( short *)&a_kun.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin (( long *) &a_kun.kun, SQLLONG, 0 ) ;
	dbClass.sqlin (( char *)  a_kun.kun_bran2, SQLCHAR, 3 ) ;
	del_cursor = (short) dbClass.sqlcursor ("delete "
	
	" from a_kun where mdn = ? and kun = ? and kun_bran2 = ? " ) ;
< ---- */


	dbClass.sqlin (( short *)&a_kun.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin (( long *) &a_kun.kun, SQLLONG, 0 ) ;
	dbClass.sqlin (( char *)  a_kun.kun_bran2, SQLCHAR, 3 ) ;
	
	dbClass.sqlout (( char *) a_bas.a_bz1, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlout (( char *) a_bas.a_bz2, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlout (( char *) a_bas.a_bz3, SQLCHAR, 25 ) ;	// 131009 mea culpa

	dbClass.sqlout (( short *) &a_kun.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.fil, SQLSHORT, 0 ) ;
	dbClass.sqlout (( long *) &a_kun.kun, SQLLONG, 0 ) ;
	dbClass.sqlout (( double *) &a_kun.a, SQLDOUBLE, 0 ) ; 
	dbClass.sqlout (( char *)  a_kun.a_kun, SQLCHAR, 14 );	// 131009 mea culpa
	dbClass.sqlout (( char *)  a_kun.a_bz1, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlout (( short *) &a_kun.me_einh_kun, SQLSHORT, 0 ) ;
	dbClass.sqlout (( double *) &a_kun.inh, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( char *) a_kun.kun_bran2, SQLCHAR, 3 ) ;	// 131009 mea culpa
	dbClass.sqlout (( double *) &a_kun.tara, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( double *) &a_kun.ean, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( double *) &a_kun.ean_vk, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( char *)	a_kun.a_bz2, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlout (( short *) &a_kun.hbk_ztr, SQLSHORT, 0 ) ; 
	dbClass.sqlout (( long *) &a_kun.kopf_text, SQLLONG, 0 ) ;
	dbClass.sqlout (( char *) a_kun.pr_rech_kz, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlout (( char *) a_kun.modif, SQLCHAR, 2 ) ;		// 131009 mea culpa
	dbClass.sqlout (( long *) &a_kun.text_nr, SQLLONG, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.devise, SQLSHORT, 0 ) ;
	dbClass.sqlout (( char *) a_kun.geb_eti, SQLCHAR, 2 ) ;		// 131009 mea culpa	
	dbClass.sqlout (( char *) a_kun.geb_fill, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlout (( long *) &a_kun.geb_anz, SQLLONG, 0 ) ;
	dbClass.sqlout (( char *) a_kun.pal_eti, SQLCHAR, 2 ) ;		// 131009 mea culpa
	dbClass.sqlout (( char *) a_kun.pal_fill, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlout (( short *) &a_kun.pal_anz, SQLSHORT, 0 ) ; 
	dbClass.sqlout (( char *) a_kun.pos_eti, SQLCHAR, 2 ) ;
	dbClass.sqlout (( short *) &a_kun.sg1, SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.sg2, SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.pos_fill, SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.ausz_art, SQLSHORT, 0 ) ;
	dbClass.sqlout (( long *) &a_kun.text_nr2, SQLLONG, 0 ) ;
	dbClass.sqlout (( short *) &a_kun.cab, SQLSHORT, 0 ) ;
	dbClass.sqlout (( char *) a_kun.a_bz3, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlout (( char *) a_kun.a_bz4, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlout (( char *) a_kun.li_a, SQLCHAR, 14 ) ;   // 131009 mea culpa      
	dbClass.sqlout (( double *) &a_kun.geb_fakt, SQLDOUBLE, 0 ) ;

	readcursor = (short) dbClass.sqlcursor ("select "
	" a_bas.a_bz1, a_bas.a_bz2, a_bas.a_bz3 "
	" , a_kun.mdn ,a_kun.fil ,kun ,a_kun.a ,a_kun ,a_kun.a_bz1 ,me_einh_kun ,inh ,kun_bran2 ,tara "
	" ,ean ,ean_vk ,a_kun.a_bz2 ,a_kun.hbk_ztr ,kopf_text ,pr_rech_kz ,a_kun.modif ,text_nr ,devise "
	" ,geb_eti ,geb_fill ,geb_anz ,pal_eti ,pal_fill ,pal_anz ,pos_eti ,sg1 "
	" ,sg2 ,pos_fill ,a_kun.ausz_art ,text_nr2 ,cab ,a_kun.a_bz3 ,a_bz4 ,li_a ,geb_fakt "
	
	" from a_kun , a_bas where a_kun.mdn = ? and kun = ? and kun_bran2 = ? and a_kun.fil = 0 " 
	" and a_bas.a = a_kun.a "
	" order by a_kun.a " ) ;
	// 141009 fil = 0 dazu ... 300910 : fil-> a_kun.fil !!!

	dbClass.sqlin (( short *) &a_kun.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &a_kun.fil, SQLSHORT, 0 ) ;
	dbClass.sqlin (( long *)  &a_kun.kun, SQLLONG, 0 ) ;
	dbClass.sqlin (( double *)&a_kun.a, SQLDOUBLE, 0 ) ; 
	dbClass.sqlin (( char *)   a_kun.a_kun, SQLCHAR, 14 );	// 131009 mea culpa
	dbClass.sqlin (( char *)   a_kun.a_bz1, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlin (( short *) &a_kun.me_einh_kun, SQLSHORT, 0 ) ;
	dbClass.sqlin (( double *)&a_kun.inh, SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( char *)   a_kun.kun_bran2, SQLCHAR, 3 ) ;	// 131009 mea culpa
	dbClass.sqlin (( double *)&a_kun.tara, SQLDOUBLE, 0 ) ;

	dbClass.sqlin (( double *)&a_kun.ean, SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( double *)&a_kun.ean_vk, SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( char *)   a_kun.a_bz2, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlin (( short *) &a_kun.hbk_ztr, SQLSHORT, 0 ) ; 
	dbClass.sqlin (( long *)  &a_kun.kopf_text, SQLLONG, 0 ) ;
	dbClass.sqlin (( char *)   a_kun.pr_rech_kz, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlin (( char *)   a_kun.modif, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlin (( long *)  &a_kun.text_nr, SQLLONG, 0 ) ;
	dbClass.sqlin (( short *) &a_kun.devise, SQLSHORT, 0 ) ;

	dbClass.sqlin (( char *)   a_kun.geb_eti, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlin (( char *)   a_kun.geb_fill, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlin (( long *)  &a_kun.geb_anz, SQLLONG, 0 ) ;	
	dbClass.sqlin (( char *)   a_kun.pal_eti, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlin (( char *)   a_kun.pal_fill, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlin (( short *) &a_kun.pal_anz, SQLSHORT, 0 ) ; 
	dbClass.sqlin (( char *)   a_kun.pos_eti, SQLCHAR, 2 ) ;	// 131009 mea culpa
	dbClass.sqlin (( short *) &a_kun.sg1, SQLSHORT, 0 ) ;

	dbClass.sqlin (( short *) &a_kun.sg2, SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &a_kun.pos_fill, SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &a_kun.ausz_art, SQLSHORT, 0 ) ;
	dbClass.sqlin (( long *)  &a_kun.text_nr2, SQLLONG, 0 ) ;
	dbClass.sqlin (( short *) &a_kun.cab, SQLSHORT, 0 ) ;
	dbClass.sqlin (( char *)   a_kun.a_bz3, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlin (( char *)   a_kun.a_bz4, SQLCHAR, 25 ) ;	// 131009 mea culpa
	dbClass.sqlin (( char *)   a_kun.li_a, SQLCHAR, 14 ) ;	// 131009 mea culpa         
	dbClass.sqlin (( double *)&a_kun.geb_fakt, SQLDOUBLE, 0 ) ;

	ins_cursor = (short) dbClass.sqlcursor ("insert into a_kun  "
	" ( mdn ,fil ,kun ,a_kun.a ,a_kun ,a_bz1 ,me_einh_kun ,inh ,kun_bran2 ,tara "
	" ,ean ,ean_vk ,a_bz2 ,hbk_ztr ,kopf_text ,pr_rech_kz ,modif ,text_nr ,devise "
	" ,geb_eti ,geb_fill ,geb_anz ,pal_eti ,pal_fill ,pal_anz ,pos_eti ,sg1 "
	" ,sg2 ,pos_fill ,ausz_art ,text_nr2 ,cab ,a_bz3 ,a_bz4 ,li_a ,geb_fakt "
	" ) values ( "
	"    ? ,? ,? ,? ,? ,? ,? ,? ,? ,? "
	"   ,? ,? ,? ,? ,? ,? ,? ,? ,? "
	"   ,? ,? ,? ,? ,? ,? ,? ,? "
	"   ,? ,? ,? ,? ,? ,? ,? ,? ,? ) "

	) ;

// Updaten :
//		dbClass.sqlin (( short *) &a_kun.mdn, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( short *) &a_kun.fil, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( long *) &a_kun.kun, SQLLONG, 0 ) ;
//	dbClass.sqlin (( double *) &a_kun.a, SQLDOUBLE, 0 ) ; 

	dbClass.sqlin (( char *) a_kun.a_kun, SQLCHAR, 14 );
	dbClass.sqlin (( char *) a_kun.a_bz1, SQLCHAR, 25 ) ;
	dbClass.sqlin (( short *) &a_kun.me_einh_kun, SQLSHORT, 0 ) ;
	dbClass.sqlin (( double *) &a_kun.inh, SQLDOUBLE, 0 ) ;

//	dbClass.sqlin (( char *) &a_kun.kun_bran2, SQLCHAR, 3 ) ;
//	dbClass.sqlin (( double *) &a_kun.tara, SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( double *) &a_kun.ean, SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( double *) &a_kun.ean_vk, SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( char *) a_kun.a_bz2, SQLCHAR, 25 ) ;

//	dbClass.sqlin (( short *) &a_kun.hbk_ztr, SQLSHORT, 0 ) ; 
//	dbClass.sqlin (( long *) &a_kun.kopf_text, SQLLONG, 0 ) ;
//	dbClass.sqlin (( char *) &a_kun.pr_rech_kz, SQLCHAR, 2 ) ;
//	dbClass.sqlin (( char *) &a_kun.modif, SQLCHAR, 2 ) ;
//	dbClass.sqlin (( long *) &a_kun.text_nr, SQLLONG, 0 ) ;
//	dbClass.sqlin (( short *) &a_kun.devise, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( char *) &a_kun.geb_eti, SQLCHAR, 2 ) ;
//	dbClass.sqlin (( char *) &a_kun.geb_fill, SQLCHAR, 2 ) ;
//	dbClass.sqlin (( long *) &a_kun.geb_anz, SQLLONG, 0 ) ;
//	dbClass.sqlin (( char *) &a_kun.pal_eti, SQLCHAR, 2 ) ;
//	dbClass.sqlin (( char *) &a_kun.pal_fill, SQLCHAR, 2 ) ;
//	dbClass.sqlin (( short *) &a_kun.pal_anz, SQLSHORT, 0 ) ; 
//	dbClass.sqlin (( char *) &a_kun.pos_eti, SQLCHAR, 2 ) ;
//	dbClass.sqlin (( short *) &a_kun.sg1, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( short *) &a_kun.sg2, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( short *) &a_kun.pos_fill, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( short *) &a_kun.ausz_art, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( long *) &a_kun.text_nr2, SQLLONG, 0 ) ;
//	dbClass.sqlin (( short *) &a_kun.cab, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( char *) &a_kun.a_bz3, SQLCHAR, 25 ) ;
//	dbClass.sqlin (( char *) &a_kun.a_bz4, SQLCHAR, 25 ) ;
	dbClass.sqlin (( char *) &a_kun.li_a, SQLCHAR, 14 ) ;         
	dbClass.sqlin (( double *) &a_kun.geb_fakt, SQLDOUBLE, 0 ) ;


	dbClass.sqlin (( short *) &a_kun.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin (( long *) &a_kun.kun, SQLLONG, 0 ) ;
	dbClass.sqlin (( char *) a_kun.kun_bran2, SQLCHAR, 3 ) ;
	dbClass.sqlin (( double *) &a_kun.a, SQLDOUBLE, 0 ) ; 

	upd_cursor = (short) dbClass.sqlcursor ("update a_kun set "
//	" ( mdn ,fil ,kun ,a_kun.a ,a_kun ,a_bz1 ,me_einh_kun ,inh ,kun_bran2 ,tara "
	" a_kun = ? ,a_bz1 = ? ,me_einh_kun = ? ,inh = ? "
//	" ,ean ,ean_vk ,a_bz2 ,hbk_ztr ,kopf_text ,pr_rech_kz ,modif ,text_nr ,devise "
	" ,ean = ? ,ean_vk = ? ,a_bz2 = ? "
//	" ,geb_eti ,geb_fill ,geb_anz ,pal_eti ,pal_fill ,pal_anz ,pos_eti ,sg1 "
//	" ,sg2 ,pos_fill ,ausz_art ,text_nr2 ,cab ,a_bz3 ,a_bz4 ,li_a ,geb_fakt "
	" , li_a = ? ,geb_fakt = ?  "
	" where mdn = ? and kun = ? and kun_bran2 = ? and a = ? and fil = 0 " 
	// 141009 : fil = 0 dazu
	);

}

