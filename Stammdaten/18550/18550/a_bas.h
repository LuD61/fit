#ifndef _A_BAS_DEF
#define _A_BAS_DEF

// 110608 A

struct A_HNDW {
double a ;
short mdn ;
short fil ;
double inh ;
short me_einh_kun ;

// prinz. erweiterbare Basis-Struktur
};
struct A_EIG {
double a ;
short mdn ;
short fil ;
double inh ;
short me_einh_ek ;

// prinz. erweiterbare Basis-Struktur
};

struct A_EIG_DIV {
double a ;
short mdn ;
short fil ;
double inh ;
short me_einh_ek ;

// prinz. erweiterbare Basis-Struktur
};

extern struct A_HNDW a_hndw, a_hndw_null;

extern struct A_EIG a_eig, a_eig_null;

extern struct A_EIG_DIV a_eig_div, a_eig_div_null;


class A_HNDW_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_hndw (void);
               int opena_hndw (void);
               A_HNDW_CLASS () : DB_CLASS ()
               {
               }
};

class A_EIG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_eig (void);
               int opena_eig (void);
               A_EIG_CLASS () : DB_CLASS ()
               {
               }
};

class A_EIG_DIV_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_eig_div (void);
               int opena_eig_div (void);
               A_EIG_DIV_CLASS () : DB_CLASS ()
               {
               }
};

// 110608 E

struct A_EAN {
double a ;
short delstatus ;
double ean ;
char ean_bz[25] ;
char h_ean_kz[2] ;
short ean_vk_kz ;
};

extern struct A_EAN a_ean, a_ean_null;


class A_EAN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_ean (void);
               int opena_ean (void);
               A_EAN_CLASS () : DB_CLASS ()
               {
               }
};

struct A_BAS {
	double a ;
	short mdn ;
	short fil ;
	char a_bz1[25];
	char a_bz2[25] ;
	double a_gew ;
	short a_typ ;
	short a_typ2 ;
	short abt ;
	long ag ;
	char best_auto[2] ;
	char bsd_kz [2] ;
	char cp_aufschl[2] ;
	short delstatus ;
	short dr_folge ;
	long erl_kto ;
	char hbk_kz [2] ;
	short hbk_ztr ;
	char hnd_gew [2] ;
	short hwg ;
	char kost_kz [3] ;
	short me_einh ;
	char modif [2] ;
	short mwst ;
	short plak_div ;
	char stk_lst_kz [2] ;
	double sw ;
	short teil_smt ;
	long we_kto ;
	short wg ;
	short zu_stoff ;
	char akv [11]; 
	char bearb [11];
	char pers_nam [9] ;
	double prod_zeit ;
	char pers_rab_kz [2];
	double gn_pkt_gbr ;
	long kost_st ;
	char sw_pr_kz[2] ;
	long kost_tr ;
	double a_grund ;
	long kost_st2 ;
	long we_kto2 ;
	long charg_hand ;
	long intra_stat ;
	char qual_kng [5] ; 
	char a_bz3 [25] ;
	short lief_einh ;
	double inh_lief ;
	long erl_kto_1 ;
	long erl_kto_2 ;
	long erl_kto_3 ;
	long we_kto_1 ;
	long we_kto_2 ;
	long we_kto_3 ;
	char skto_f [2] ;
	double sk_vollk ;
	double a_ersatz ;
	short a_ers_kz ;
	short me_einh_abverk ;
	double inh_abverk ;
	char hnd_gew_abverk [2] ;
	
};
extern struct A_BAS a_bas, a_bas_null;

class A_BAS_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesea_bas (void);
               int opena_bas (void);
               A_BAS_CLASS () : DB_CLASS ()
               {
               }
};


struct A_KUN {

	short mdn ;
	short fil ;
	long kun ;
	double a ; 
	char a_kun[14] ;
	char a_bz1[25] ;
	short me_einh_kun ;
	double inh ;
	char kun_bran2[3] ;
	double tara ;
	double ean ;
	double ean_vk ;
	char a_bz2[25] ;
	short hbk_ztr ; 
	long kopf_text ;
	char pr_rech_kz[2];
	char modif[2] ;
	long text_nr ;
	short devise ;
	char geb_eti[2] ;
	char geb_fill[2] ;
	long geb_anz ;
	char pal_eti[2] ;
	char pal_fill[2] ;
	short pal_anz ; 
	char pos_eti[2] ;
	short sg1 ;
	short sg2 ;
	short pos_fill ;
	short ausz_art ;
	long text_nr2 ;
	short cab ;
	char a_bz3[25] ;
	char a_bz4[25] ;
	char li_a[14] ;         
	double geb_fakt ;

};
extern struct A_KUN a_kun, a_kun_null;

struct B_KUN {
char inakun [3099] ;	// 141009 2099->3099
double vonwert ;	// 141009
double biswert ;	// 141009
};

extern struct B_KUN b_kun ;


class A_KUN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
			   void preparedel(void);
       public :
//               int dbcount (void);
               int lesealla_kun (void);
               int openalla_kun (void);
               int lesea_kun (void);
               int opena_kun (void);
			   int deletea_kun (void) ;
			   int upda_kun(void) ;
			   int inserta_kun (void);
               A_KUN_CLASS () : DB_CLASS ()
               {
               }
};


#endif

