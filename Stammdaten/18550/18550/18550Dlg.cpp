// 18550Dlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "18550.h"
#include "18550Dlg.h"
#include "KunWahl.h"

#include "strfuncs.h"
#include "dbClass.h"
#include "mdn.h"
#include "adr.h"
#include "kun.h"
#include "a_bas.h"
#include "ptabn.h"

#include "FillList.h"

extern DB_CLASS dbClass ;
extern MDN_CLASS mdn_class ;
extern ADR_CLASS adr_class ;
extern KUN_CLASS kun_class ;
extern PTABN_CLASS ptabn_class ;
extern A_KUN_CLASS a_kun_class ;
// extern A_BAS_CLASS A_BAS_class ;

static char bufh[516] ;	// allgemeine Hilfsvariable
static CString bufx ;	// allgemeine Hilfsvariable 


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMy18550Dlg-Dialogfeld


CMy18550Dlg::CMy18550Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMy18550Dlg::IDD, pParent)
	, v_mdnname(_T(""))
	, v_mdnnr(0)
	, v_combobran(_T(""))
	, v_kundnr(0)
	, v_kundname(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy18550Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_MDNNR, m_mdnnr);
	DDX_Text(pDX, IDC_MDNNR, v_mdnnr);
	DDV_MinMaxLong(pDX, v_mdnnr, 0, 9999);
	DDX_Control(pDX, IDC_LIST1, m_list1);
	DDX_Control(pDX, IDC_COMBOBRAN, m_combobran);
	DDX_CBString(pDX, IDC_COMBOBRAN, v_combobran);
	DDX_Control(pDX, IDC_KUNDNR, m_kundnr);
	DDX_Text(pDX, IDC_KUNDNR, v_kundnr);
	DDV_MinMaxLong(pDX, v_kundnr, 0, 99999999);
	DDX_Control(pDX, IDC_KUNDNAME, m_kundname);
	DDX_Text(pDX, IDC_KUNDNAME, v_kundname);
	DDX_Control(pDX, IDC_BUTTONKUN, m_buttonkun);
	DDX_Control(pDX, IDC_BUTTKEY6, m_buttkey6);
	DDX_Control(pDX, IDC_BUTTKEY7, m_buttkey7);
	DDX_Control(pDX, IDC_BUTTKEY8, m_buttkey8);
	DDX_Control(pDX, IDC_BUTTKEY9, m_buttkey9);
	DDX_Control(pDX, IDC_BUTTKEY10, m_buttkey10);
	DDX_Control(pDX, IDC_BUTTKEY11, m_buttkey11);
}

BEGIN_MESSAGE_MAP(CMy18550Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_NOTIFY(HDN_BEGINTRACK, 0, &CMy18550Dlg::OnHdnBegintrackList1)
	ON_NOTIFY(HDN_ENDTRACK, 0, &CMy18550Dlg::OnHdnEndtrackList1)
	ON_BN_CLICKED(IDOK, &CMy18550Dlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMy18550Dlg::OnBnClickedCancel)
	ON_EN_KILLFOCUS(IDC_MDNNR, &CMy18550Dlg::OnEnKillfocusMdnnr)
	ON_EN_KILLFOCUS(IDC_KUNDNR, &CMy18550Dlg::OnEnKillfocusKundnr)
	ON_CBN_KILLFOCUS(IDC_COMBOBRAN, &CMy18550Dlg::OnCbnKillfocusCombobran)
	ON_CBN_SELCHANGE(IDC_COMBOBRAN, &CMy18550Dlg::OnCbnSelchangeCombobran)
	ON_BN_CLICKED(IDC_BUTTONKUN, &CMy18550Dlg::OnBnClickedButtonkun)
	ON_CBN_SETFOCUS(IDC_COMBOBRAN, &CMy18550Dlg::OnCbnSetfocusCombobran)
	ON_EN_SETFOCUS(IDC_KUNDNR, &CMy18550Dlg::OnEnSetfocusKundnr)
	ON_EN_SETFOCUS(IDC_MDNNR, &CMy18550Dlg::OnEnSetfocusMdnnr)
	ON_BN_CLICKED(IDC_BDRUCK, &CMy18550Dlg::OnBnClickedBdruck)
	ON_BN_CLICKED(IDC_BUTTKEY7, &CMy18550Dlg::OnBnClickedButtkey7)
	ON_BN_CLICKED(IDC_BUTTKEY6, &CMy18550Dlg::OnBnClickedButtkey6)
END_MESSAGE_MAP()



bool CMy18550Dlg::ReadMdn (void) 
{
	bool retcode = FALSE ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
			retcode = TRUE ;
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			retcode = FALSE ;
// hier nicht notwendig :	PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		retcode = FALSE ;
// hier nicht notwendig : 		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;
	return retcode ;
}

void CMy18550Dlg::FillMeEinhCombo ()
{
	CVector Values;
	Values.Init ();

	int i = ptabn_class.openallptabn ("me_einh") ;

//	Ptabn.sqlopen (IMeEinhCursor);
// 	while (Ptabn.sqlfetch (IMeEinhCursor) == 0)
	while ( ! ptabn_class.leseallptabn() )
	{
//		Ptabn.dbreadfirst ();
		CString *Value = new CString ();
		// ptabn.ptwert ist hier immer 3 Stellig + linksbuendig
		Value->Format ("%s  %s", ptabn.ptwert, ptabn.ptbezk);
		Values.Add (Value);
	}
	m_list1.FillMeEinhCombo (Values);
}


BOOL CMy18550Dlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				keyset(TRUE);
				m_list1.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_list1 &&
					GetFocus ()->GetParent () != &m_list1 )
				{

					break;
			    }
				keyset(TRUE);
				m_list1.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
// AAA OOOO					return FALSE ;
				}
				keyset(TRUE);
				m_list1.OnKeyD (VK_DOWN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				keyset(TRUE);
				m_list1.OnKeyD (VK_UP);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				OnCancel () ;
				return TRUE;
			}

 			else if (pMsg->wParam == VK_F6)
			{

//				OnInsert ();
//				return TRUE;
				CWnd *Control = GetFocus ();
				if (Control == &m_list1 ||
					Control->GetParent ()== &m_list1 )
				{
					m_list1.OnKeyD (VK_F6);
					return TRUE;
				}

			}

 			else if (pMsg->wParam == VK_F7)
			{
				CWnd *Control = GetFocus ();
				if (Control == &m_list1 ||
					Control->GetParent ()== &m_list1 )
				{
					m_list1.OnKeyD (VK_F7);
					return TRUE;
				}
//				OnDelete ();
//				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

/* ---->
			else if (pMsg->wParam == VK_F8)
			{
				** ---->
				if (Choice != NULL)
				{	if (Choice->IsWindowVisible ())
					{	Choice->ShowWindow (SW_HIDE);
					}
					else
					{	Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{	OnChoice ();
				}
				< ----- **
				OnChoice();	// alternative zur echten selektion 
			}
			else if (pMsg->wParam == VK_F9)
			{	if (GetFocus () == &m_mdn)
				{	OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_schlklknr)
				{	OnChoice ();
					return TRUE;
				}

				m_List.OnKey9 ();
				return TRUE;
			}
< ----- */
	}
//return CDbPropertyPage::PreTranslateMessage(pMsg);
//return CFormView::PreTranslateMessage(pMsg);
//	return FALSE ;
return CDialog::PreTranslateMessage(pMsg);
}


// CMy18550Dlg-Meldungshandler

BOOL CMy18550Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	branrekursion = 0 ;
	
	dbClass.opendbase (_T("bws"));


	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	// grundeinstellung
	m_combobran.EnableWindow (TRUE) ;
	m_combobran.ModifyStyle (0, WS_TABSTOP,0) ;
	m_kundnr.EnableWindow (TRUE) ;
	m_kundnr.ModifyStyle (0,WS_TABSTOP,0) ;
	m_buttonkun.EnableWindow (TRUE) ;
//	m_buttonkun.ModifyStyle (0,WS_TABSTOP,0) ;

	FillList = m_list1;
	FillList.SetStyle (LVS_REPORT);
	if (m_list1.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}

	/* --->
	Ptabn.sqlout ((long *)  &Ptabn.ptabn.ptlfnr, SQLLONG, 0);
	Ptabn.sqlout ((char *)  Ptabn.ptabn.ptwert, SQLCHAR, 4);
	Ptabn.sqlout ((char *)  Ptabn.ptabn.ptbezk, SQLCHAR, 9) ;

	IMeEinhCursor = Ptabn.sqlcursor ( "select ptlfnr"
							" , ptwert, ptbezk "
							" from ptabn "
							"where ptitem = 'me_einh'  order by ptlfnr " 
						) ;
< ------ */
	FillMeEinhCombo ();

//	m_buttkey6.EnableWindow (FALSE) ;
//	m_buttkey7.EnableWindow (FALSE) ;
//	m_buttkey8.EnableWindow (FALSE) ;
//	m_buttkey9.EnableWindow (FALSE) ;
//	m_buttkey10.EnableWindow(FALSE) ;
//	m_buttkey11.EnableWindow(FALSE) ;


	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Art-Nr."), 1, 50, LVCFMT_RIGHT);
	FillList.SetCol (_T("Bezeichnung"), 2, 180, LVCFMT_LEFT);
	FillList.SetCol (_T("|"), 3, 0, LVCFMT_LEFT);	// Platz sparen
	FillList.SetCol (_T("Kun.A-Nr."), 4, 65, LVCFMT_LEFT);
	FillList.SetCol (_T("Bez.1"), 5, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("Bez.2"), 6, 80, LVCFMT_LEFT);
	FillList.SetCol (_T("Best.ME"), 7, 60, LVCFMT_LEFT);
	FillList.SetCol (_T("Inh."), 8, 50, LVCFMT_RIGHT);
	FillList.SetCol (_T("Geb-Fakt"), 9, 60, LVCFMT_RIGHT);
	FillList.SetCol (_T("EAN"), 10, 80, LVCFMT_LEFT);
	FillList.SetCol (_T("EAN-VK"), 11, 80, LVCFMT_LEFT);
	FillList.SetCol (_T("Li-Art."), 12, 60, LVCFMT_RIGHT);

//	m_list1.ColType.Add (new CColType (5, m_list1.CheckBox)) ;

	mdn.mdn = 1 ;	// gibbet hoffentlich immer ?!
	v_mdnnr = 1 ;

	sprintf ( kunbran2, "0" ) ;
	kun.kun = 0 ;

	ReadMdn() ;


// Laden der Combo-Box kunbran2

	CString szptabn ;
	szptabn.Format("         " ) ;	// Leerstring ( d.h. KEINE Kundenbranche )
	((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->AddString(szptabn.GetBuffer(0));
	((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->SetCurSel(0);

	int sqlstat = ptabn_class.openallptabn ("kun_bran2");
	sqlstat = ptabn_class.leseallptabn();
	while(!sqlstat)
	{
		szptabn.Format("%s  %s",ptabn.ptwert, ptabn.ptbez);
		// hier haben wir jetzt die Namen und k�nnen sie in die ComboBox einf�gen

			((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->AddString(szptabn.GetBuffer(0));
			((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->SetCurSel(0);
	
		sqlstat = ptabn_class.leseallptabn () ;
	}

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CMy18550Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CMy18550Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CMy18550Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMy18550Dlg::OnHdnBegintrackList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	m_list1.StartPauseEnter ();

	*pResult = 0;
}

void CMy18550Dlg::OnHdnEndtrackList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	m_list1.EndPauseEnter ();

	*pResult = 0;
}

BOOL CMy18550Dlg::Read ()
{
	ReadList ();
	return TRUE;
}

BOOL CMy18550Dlg::ReadList ()
{

  m_list1.DeleteAllItems ();
  m_list1.vSelect.clear ();
  int i = 0;

  memcpy (&a_kun, &a_kun_null, sizeof (struct A_KUN));
  a_kun.mdn = mdn.mdn;
  sprintf ( a_kun.kun_bran2 ,"%s", kunbran2 ) ;
  a_kun.kun = kun.kun ;

  int sqlret = 100 ;

  a_kun_class.openalla_kun () ;
  sqlret =  a_kun_class.lesealla_kun() ;	
  if ( !sqlret )	// ls gefunden .....
  {
		CChoiceMeEinh HoleBezk ;


		while ( ! sqlret )
		{
	
		FillList.InsertItem (i, 0);

/* ---->
		CString pOSI;
		pOSI.Format (_T("%d"), i + 1 );
		FillList.SetItemText (pOSI.GetBuffer (), i, m_List.PosPosi);
		CString kOST_BEZ;
		kOST_BEZ = Schlaklkp.schlaklkp.kost_bez;
		FillList.SetItemText (kOST_BEZ.GetBuffer (), i, m_List.PosKost_bez);

		CString pTWERT ;
		TCHAR ptbezk [37] ; 
		pTWERT.Format (_T("%d"), Schlaklkp.schlaklkp.zubasis);
    	HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk);
	
		CString zUBASIS;
		zUBASIS.Format (_T("%d  %s"),Schlaklkp.schlaklkp.zubasis, ptbezk);
		FillList.SetItemText (zUBASIS.GetBuffer (), i, m_List.PosZubasis);

		CString zUSCHLAG;
		m_List.DoubleToString (Schlaklkp.schlaklkp.zuschlag, zUSCHLAG, 2);
		FillList.SetItemText (zUSCHLAG.GetBuffer (), i, m_List.PosZuschlag);

		double diposwert, diposwertkg ;
		switch ( Schlaklkp.schlaklkp.zubasis )
		{
//		case 0 :	// je kg
		case 1 :	// je Stck.
			diposwert = Schlaklkp.schlaklkp.zuschlag * Schlaklkk.schlaklkk.anzahl ;
			if ( Schlaklkk.schlaklkk.kaltgew != 0.0 )
			{
			    diposwertkg = (Schlaklkp.schlaklkp.zuschlag * 
					Schlaklkk.schlaklkk.anzahl) / Schlaklkk.schlaklkk.kaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;
		case 2 :	// pauschal
			diposwert = Schlaklkp.schlaklkp.zuschlag ;
			if ( Schlaklkk.schlaklkk.kaltgew != 0.0 )
			{
			    diposwertkg = Schlaklkp.schlaklkp.zuschlag / Schlaklkk.schlaklkk.kaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;

		default :	// auffang-Linie : je kg
		    diposwert = Schlaklkp.schlaklkp.zuschlag * Schlaklkk.schlaklkk.kaltgew ;
			diposwertkg = Schlaklkp.schlaklkp.zuschlag ;
			break ;
		}

		CString wERT;
		m_List.DoubleToString (diposwert, wERT, 2);
		Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
		FillList.SetItemText (wERT.GetBuffer (), i, m_List.PosWert);

		CString wERTKG;
		m_List.DoubleToString (diposwertkg, wERTKG, 4);
		Schlaklkp.schlaklkp.wertkg = CStrFuncs::StrToDouble (wERTKG);
		dizuabkg += Schlaklkp.schlaklkp.wertkg ;
		FillList.SetItemText (wERTKG.GetBuffer (), i, m_List.PosWertkg);
		sqlret = Schlaklkp.dbread() ;			
		i ++;
	}

	Schlaklkk.schlaklkk.zuabkg = dizuabkg;
< ---- */

		CString wERT;
		m_list1.DoubleToString (a_kun.a, wERT, 0);
		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosArtNr);

		CString pARTBEZ;
		pARTBEZ.Format( _T( "%s %s"), a_bas.a_bz1 , a_bas.a_bz2 ) ;
		FillList.SetItemText (pARTBEZ.GetBuffer (), i, m_list1.PosArtBez);

		CString pKANTE;
		pKANTE = _T("|") ;
		FillList.SetItemText (pKANTE.GetBuffer (), i, m_list1.PosKante);

		CString pA_KUN;
		pA_KUN = a_kun.a_kun ;
		FillList.SetItemText (pA_KUN.GetBuffer (), i, m_list1.PosA_kun);

		CString pA_BZ1;
		pA_BZ1 = a_kun.a_bz1 ;
		FillList.SetItemText (pA_BZ1.GetBuffer (), i, m_list1.PosA_bz1);

		CString pA_BZ2;
		pA_BZ2 = a_kun.a_bz2 ;
		FillList.SetItemText (pA_BZ2.GetBuffer (), i, m_list1.PosA_bz2);


		CString pTWERT ;
		TCHAR ptbezk [37] ; 
//		CString ptbezk ; 
		pTWERT.Format (_T("%d"), a_kun.me_einh_kun);
		HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk );
	
		CString zUBASIS;
// das Problem : Formatierung numerischer Wert mit 2 Folgeblanks 
		if ( a_kun.me_einh_kun > -1 && a_kun.me_einh_kun < 10 )
		{
			zUBASIS.Format (_T("%d    %s"),a_kun.me_einh_kun, ptbezk);
		}
		else
		{
			if ( a_kun.me_einh_kun > 9 && a_kun.me_einh_kun < 100 )
			{
				zUBASIS.Format (_T("%d   %s"),a_kun.me_einh_kun, ptbezk);
			}
			else	// Notbremse
				zUBASIS.Format (_T("%d   %s"),a_kun.me_einh_kun, ptbezk);
		}
		FillList.SetItemText (zUBASIS.GetBuffer (), i, m_list1.PosKun_me_einh);

		m_list1.DoubleToString (a_kun.inh, wERT, 3);
//		Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosKun_inh);

		m_list1.DoubleToString (a_kun.geb_fakt, wERT, 0);
		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosGeb_fakt);


		m_list1.DoubleToString (a_kun.ean, wERT, 0);
		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosEan);

		m_list1.DoubleToString (a_kun.ean_vk, wERT, 0);
		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosEan_VK);


		CString pLI_A;
		pLI_A = a_kun.li_a ;

		FillList.SetItemText (pLI_A.GetBuffer (), i, m_list1.PosLi_a);

/* --->
		CString pSTAT;
		pSTAT.Format (_T("%d"), lsk.ls_stat );
		FillList.SetItemText (pSTAT.GetBuffer (), i, m_list1.PosStat);


		CString pANZ;
		pANZ.Format (_T("%d"), fracht.anz );
		FillList.SetItemText (pANZ.GetBuffer (), i, m_list1.PosAnz);

		CString pAKTIV  = _T(" ")  ;
		if ( fracht.stat )
			pAKTIV = _T("X") ;
		FillList.SetItemText (pAKTIV.GetBuffer (), i, m_list1.PosAktiv);
< --- */

		sqlret =  a_kun_class.lesealla_kun() ;	
		i ++ ;
	}
  }
	return TRUE;
}

BOOL CMy18550Dlg::InList (A_KUN_CLASS & a_kun_class)
{
	
	m_buttkey6.EnableWindow (TRUE) ;
	m_buttkey7.EnableWindow (TRUE) ;
	m_buttkey8.EnableWindow (TRUE) ;
	m_buttkey9.EnableWindow (TRUE) ;
	m_buttkey10.EnableWindow(TRUE) ;
	m_buttkey11.EnableWindow(TRUE) ;

	ListRows.FirstPosition ();
/* --->
	CAkiPreise *aki_pr;
	while ((aki_pr = (CAkiPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Akiprgrstp.akiprgrstp.aki_nr == aki_pr->akiprgrstp.aki_nr &&
			Akiprgrstp.akiprgrstp.a == aki_pr->akiprgrstp.a) return TRUE;
	}
< ---- */

   return FALSE;
}
/* --->
void CpakdruDlg::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CSchlklkpos *pr;
	while ((pr = (CSchlklkpos *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Schlaklkp.schlaklkp, &pr->schlaklkp, sizeof (SCHLAKLKP));
		if (!InList (Schlaklkp))
		{
			Schlaklkp.dbdelete ();
		}
	}
}
< ----- */

BOOL CMy18550Dlg::Write ()
{
//	UpdateData(TRUE ) ; erzeugt Probeleme ?!

//	DbClass.beginwork ();

	m_list1.StopEnter ();

	int count = m_list1.GetItemCount ();

	int j = 0 ;
	int k =  0 ;
	char hilfepuffer[3099] ;	// 141009 2099 -> 3099 
	
// 141009 : gr�ssere Anzahl rows korrekt handeln , bei altem Ablauf droht sonst Datenverlust

	double startwert = 0.0 ;
	double endwert   = 0.0 ;
// Der Mechanismus funktioniert nur korrekt bei aufsteigend sortierter Liste
//	und wenn nicht gerade die letzte Position gel�scht wurde 


	int allesleer = 0 ;	// 020211
	int nureinerunde = 1 ;	// 240513

	int i = 0 ;
	while ( i < count )
	{

		j = 0 ;
		k =  0 ;
		hilfepuffer[0] = '\0' ;

		for ( ; i < count; i ++)
		{
			CString Text;
			Text = m_list1.GetItemText (i, m_list1.PosArtNr);
			double  hilfart = CStrFuncs::StrToDouble (Text);
			if ( hilfart > 0 )
			{
				if ( k > 0 )
				{
					sprintf ( hilfepuffer + k ,",%1.0f", hilfart ) ;
				}
				else	// erster Eintrag ohne komma 
				{
					sprintf ( hilfepuffer + k ,"%1.0f", hilfart ) ;
				}
				j++ ;
				k = (int) strlen ( hilfepuffer ) ;
				endwert = hilfart ;

			}
			else	// 020211 
			{
				// das folgende ist doppelmoppel aber ansonsten ok.
				if ( hilfart == 0.0 && count == 1 )
				allesleer = 1 ;
			}
				
			if ( k > 3080 )
			{	
				i ++ ;
				nureinerunde = 0 ;
				break ;	// Notbremse, falls mehr als mindestens 2080 /14 = 148 
									// oder entsprechend mehr bei Artikelnummern mit weniger als 13 stellen
			}
		}	// string aufbauen
		if ( j )
		{
			// redundante Saetze loeschen 
			sprintf ( b_kun.inakun,"%s",hilfepuffer ) ;
			a_kun.mdn = mdn.mdn ;
			a_kun.kun = kun.kun ;
			sprintf ( a_kun.kun_bran2 ,"%s", kunbran2 ) ;
			b_kun.vonwert = startwert ;
			b_kun.biswert = endwert ;
			if ( nureinerunde )	// 240513	- etwas besser als vorher
				b_kun.biswert = 99999999999.0 ;

			startwert = endwert + 1 ;
			a_kun_class.deletea_kun() ;
		}
		else
		{
			if ( allesleer )
			{
				allesleer = 0 ;
				sprintf ( b_kun.inakun,"-2" ) ;
				a_kun.mdn = mdn.mdn ;
				a_kun.kun = kun.kun ;
				sprintf ( a_kun.kun_bran2 ,"%s", kunbran2 ) ;
				b_kun.vonwert = 0 ;
				b_kun.biswert = 999999999999 ;
				startwert = endwert + 1 ;
				a_kun_class.deletea_kun() ;
			}

		}
	}	// bis alle Posten erledigt sind 

	count = m_list1.GetItemCount ();

	for (int i = 0; i < count; i ++)
	{

		CString Text;
		Text = m_list1.GetItemText (i, m_list1.PosArtNr);
		double  hilfart = CStrFuncs::StrToDouble (Text);
		if ( hilfart > 0 )
		{

			memcpy (&a_kun, &a_kun_null, sizeof (struct A_KUN));
			a_kun.mdn = mdn.mdn ;
			a_kun.kun = kun.kun ;
			sprintf ( a_kun.kun_bran2 ,"%s", kunbran2 ) ;
			a_kun.a = hilfart ;
			int j = a_kun_class.opena_kun() ;
			j = a_kun_class.lesea_kun() ;

			Text = m_list1.GetItemText (i, m_list1.PosA_bz1);
			sprintf ( a_kun.a_bz1 , "%s" , Text.Trim().Left(24).GetBuffer());

			Text = m_list1.GetItemText (i, m_list1.PosA_bz2);
			sprintf ( a_kun.a_bz2 , "%s" , Text.Trim().Left(24).GetBuffer());

			Text = m_list1.GetItemText (i, m_list1.PosA_kun);
			sprintf ( a_kun.a_kun , "%s" , Text.Trim().Left(16).GetBuffer());

			Text = m_list1.GetItemText (i, m_list1.PosKun_inh);
			a_kun.inh =CStrFuncs::StrToDouble (Text);
			if ( a_kun.inh < -9999 ) a_kun.inh = -9999 ;
			if ( a_kun.inh > 99999 ) a_kun.inh = 99999 ;

			Text = m_list1.GetItemText (i, m_list1.PosKun_me_einh);
			a_kun.me_einh_kun =atoi (Text.GetBuffer());

			Text = m_list1.GetItemText (i, m_list1.PosGeb_fakt);
			a_kun.geb_fakt =CStrFuncs::StrToDouble (Text);
			if ( a_kun.geb_fakt < -9999999 ) a_kun.geb_fakt = -9999999 ;
			if ( a_kun.geb_fakt > 99999999 ) a_kun.geb_fakt = 99999999 ;

			Text = m_list1.GetItemText (i, m_list1.PosEan);
			a_kun.ean =CStrFuncs::StrToDouble (Text);
			if ( a_kun.ean < -999999999999) a_kun.ean = -999999999999 ;
			if ( a_kun.ean > 9999999999999) a_kun.ean = 9999999999999 ;

			Text = m_list1.GetItemText (i, m_list1.PosEan_VK);
			a_kun.ean_vk =CStrFuncs::StrToDouble (Text);
			if ( a_kun.ean_vk < -999999999999) a_kun.ean_vk = -999999999999 ;
			if ( a_kun.ean_vk > 9999999999999) a_kun.ean_vk = 9999999999999 ;

			Text = m_list1.GetItemText (i, m_list1.PosLi_a);
			sprintf ( a_kun.li_a , "%s" , Text.Trim().Left(12).GetBuffer());

			if ( !j )
			{
				a_kun_class.upda_kun () ;
			}
			else
			{
				a_kun.a = hilfart ;
				a_kun.mdn = mdn.mdn ;
				a_kun.kun = kun.kun ;
				sprintf ( a_kun.kun_bran2 ,"%s", kunbran2 ) ;
				a_kun_class.inserta_kun ();
			}
		}
	}
			
//			memcpy ( &fracht.lief_term, &lsk.lieferdat, sizeof (  TIMESTAMP_STRUCT )) ;
//			memcpy ( &fracht.dat       , &lsk.lieferdat, sizeof ( TIMESTAMP_STRUCT )) ;

	if ( kun.kun > 0 )
		m_kundnr.SetFocus() ;
	else
		m_mdnnr.SetFocus() ;

	return TRUE;
}

void CMy18550Dlg::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	Rows.Init ();
}

DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}

BOOL CMy18550Dlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

/* ---> wird beim killfocusmdnnr organisiert , dort ist dann auch der input aktuell ...
	if (Control == &m_mdnnr)
	{	
		if (!ReadMdn ())
		{	m_mdnnr.SetFocus ();
			return FALSE;
		}
	}
< ---- */

	/* ----->
	if (Control == &m_schlklknr)
	{	if (!Read ())
		{	m_schlklknr.SetFocus ();
			return FALSE;
		}
	}
	< ------ */
	/* ----->
	if (Control == &m_ckalkart)
	{	kalkartfeldernachsetzen ();
		rechnenach (); 
	}
	< -------- */

	/* ---> 
	if (Control == &m_preisek ||
		Control == &m_anzahl ||
		Control == &m_lebendgew ||
		Control == &m_schlachtgewicht ||
		Control == &m_kaltgew ||
		Control == &m_ausbeute ||
		Control == &m_verlust )
	{
		rechnenach () ;
	}
	< ---- */

	if (Control != &m_list1 &&
		Control->GetParent ()!= &m_list1)
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CMy18550Dlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_list1 &&
		Control->GetParent ()!= &m_list1 )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_PrGrList.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}

void CMy18550Dlg::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Write () ;
	//	-> verlassen nur mit Abbrechen !! OnOK();
}

void CMy18550Dlg::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnCancel();
}

void CMy18550Dlg::OnEnKillfocusMdnnr()
{
	UpdateData (TRUE) ;

	int i = m_mdnnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 191110
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{


		v_mdnname.Format("              ");

		if ( mdn.mdn < 0 || mdn.mdn > 9999 )	// 030211
		{
			v_mdnnr = 1 ;
			UpdateData (FALSE) ;
		}
		else
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);

		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;
}

void CMy18550Dlg::keyset(BOOL schalter)
{
	schalter = TRUE ;	// immer anschalten, der Rest organsiert sich duch m_list1
	m_buttkey6.EnableWindow (schalter) ;
	m_buttkey7.EnableWindow (schalter) ;
	m_buttkey8.EnableWindow (schalter) ;
	m_buttkey9.EnableWindow (schalter) ;
	m_buttkey10.EnableWindow(schalter) ;
	m_buttkey11.EnableWindow(schalter) ;
}

void CMy18550Dlg::enablekunbran(void) 
{
	// Kundenbranche enablen, Kunde disablen 
		m_combobran.EnableWindow (TRUE) ;
		m_combobran.ModifyStyle (0, WS_TABSTOP,0) ;
		m_kundnr.EnableWindow (FALSE) ;
		m_kundnr.ModifyStyle (WS_TABSTOP,0,0) ;
		m_buttonkun.EnableWindow (FALSE) ;
//		m_buttonkun.ModifyStyle (WS_TABSTOP,0,0) ;
}

void CMy18550Dlg::disablekunbran(void) 
{

	// Kundenbranche disablen, Kunde enablen 
		m_combobran.EnableWindow (FALSE) ;
		m_combobran.ModifyStyle (WS_TABSTOP,0,0) ;
		m_kundnr.EnableWindow (TRUE) ;
		m_kundnr.ModifyStyle (0,WS_TABSTOP,0) ;
		m_buttonkun.EnableWindow (TRUE) ;
//		m_buttonkun.ModifyStyle (0,WS_TABSTOP,0) ;
}
void CMy18550Dlg::setzekunbran0(void) 
{

	sprintf ( bufh, "0" );
	int nCurSel = -1 ;
//	v_kundnr = 0 ;
//	kun.kun = 0 ;
//	v_kundname.Format("Kundenbranche") ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_combobran = bufx ;
		UpdateData(FALSE) ;
	}
	if ( ! branrekursion )
	{
		branrekursion = 1 ;
		CMy18550Dlg::OnCbnSelchangeCombobran();
		branrekursion = 0 ;
	}
}

void CMy18550Dlg::OnEnKillfocusKundnr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData (TRUE) ;
	kun.kun = v_kundnr ;
	kun.mdn = mdn.mdn ;
	if ( kun.kun == 0 )
	{
		enablekunbran() ;

		v_kundnr = kun.kun ;
		v_kundname.Format ( "Kundenbranche" ) ;
		Read() ;
		m_combobran.SetFocus() ;
	}
	else	// kun.kun < 0 wird in der Eingabe abgewiesen 
	{

		int i = kun_class.openkun();
		i = kun_class.lesekun();
		if ( i )
		{

			if ( v_kundnr < 0  || v_kundnr > 99999999 )	// 030211
			{
				v_kundnr = 0 ;
				UpdateData(FALSE) ;
			}
			else
				MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			PrevDlgCtrl();
		}
		else
		{
			v_kundnr = kun.kun ;
			v_kundname.Format ("%s", kun.kun_krz1 );
			setzekunbran0 () ;
			disablekunbran() ;
			Read () ;
		}
	}

	UpdateData(FALSE) ;

}

void CMy18550Dlg::OnCbnKillfocusCombobran()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE) ;
	sprintf ( bufh, "%s", v_combobran );
	int nCurSel = -1 ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_combobran = bufx ;
		UpdateData(FALSE) ;
		if ( ! branrekursion )
		{
			branrekursion = 1 ;
			CMy18550Dlg::OnCbnSelchangeCombobran();
			branrekursion = 0 ;
		}
		Read() ;
	}
	else
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		sprintf ( kunbran2 ,"-99" )	;	// unm�glicher wert, kun_bran2 ist nur 2 zeichen gross ....

		PrevDlgCtrl();
	}
	UpdateData(FALSE) ;
}

void CMy18550Dlg::OnCbnSelchangeCombobran()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	int nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->GetCurSel();
	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_combobran = bufx ;
		int i = (int) strlen ( bufh );
		int j,k ;
		j = k = 0 ;
		if ( i )
		{
//			int j, k ; 
			j = k = 0 ;
			for (j=k=0  ;j < i;j++ )
			{
				if ( bufh[j] == ' ') 
				{
					if ( k)
					{
						bufh[j] = '\0' ;
						break ;
					}
				}
				else
				{
					k ++ ;	// nicht-Space
				}
			}
		}
		bufh[2] = '\0' ;	// jedenfalls kappen
		sprintf ( kunbran2, "%s", bufh ) ;
		if ( ( k == 0)  || ( k == 1 && ! strncmp ( bufh,"0",1)))
		{
			sprintf ( kunbran2, "0" ) ;
			setzekunbran0() ;
			disablekunbran() ;
		}
		else
			enablekunbran() ;
	

	}
	else
	{
// das folgende wird bei killfocus besser abgearbeitet .....
		sprintf ( kunbran2 ,"-99" )	;	// unm�glicher wert, kun_bran2 ist nur 2 zeichen gross ....
//		sprintf ( kunbran2, "0" ) ;
//		setzekunbran0() ;
//		disablekunbran() ;
	}

	UpdateData(FALSE ) ;


}

void CMy18550Dlg::OnBnClickedButtonkun()
{

// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

CKunWahl DialKun ;
DialKun.DoModal() ;
// ladedabatovar () ;
if ( kun.kun > 0 )	// Kunde
{
	disablekunbran() ;
	v_kundnr = kun.kun ;
	v_kundname.Format ("%s", kun.kun_krz1 );
	setzekunbran0 () ;
	disablekunbran() ;
	Read () ;

}
else	// Kundenbranche aktivieren
{
	if ( kun.kun == 0 )
	{
		enablekunbran() ;
		v_kundnr = kun.kun ;
		v_kundname.Format ( "Kundenbranche" ) ;
		Read () ;
		m_combobran.SetFocus() ;
	}
}

UpdateData(FALSE) ;
}

void CMy18550Dlg::OnCbnSetfocusCombobran()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
//	UpdateData(TRUE);
	keyset(FALSE) ;
//	UpdateData(FALSE);
}

void CMy18550Dlg::OnEnSetfocusKundnr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
//	UpdateData(TRUE);
	keyset(FALSE) ;
//	UpdateData(FALSE);

}

void CMy18550Dlg::OnEnSetfocusMdnnr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
//	UpdateData(TRUE);
	keyset(FALSE);
//	UpdateData(FALSE);

}

void CMy18550Dlg::OnBnClickedBdruck()
{

// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Write () ;	// Ich nehme mal an, hier wollte einer vorher speichern ?!
// Starten der druckerei , 70001.exe verzweigt eigenst�ndig auf alt oder neu .......
	char s1[256] ;
	sprintf ( s1, "70001.exe 18550 " );
	int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
}

// 160709
void CMy18550Dlg::OnBnClickedButtkey7()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	// erst mal gar nix, weil ich ja nie weiss, ob ich in der Listenbearbeitung bin oder doch nicht......
}
// 160709
void CMy18550Dlg::OnBnClickedButtkey6()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	// erst mal gar nix, weil ich ja nie weiss, ob ich in der Listenbearbeitung bin oder doch nicht......
}
