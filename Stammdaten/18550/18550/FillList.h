#pragma once

class CFillList
{
protected :
	CListCtrl *listView;
public:
	CFillList(void);
	CFillList(CListCtrl *);
	~CFillList(void);
	CFillList& operator= (CListCtrl&);
	void SetListView (CListCtrl *);
    BOOL SetCol (LPTSTR, int, int, int);
    BOOL SetCol (LPTSTR, int, int);
    BOOL SetColAlign (int, int);
    int InsertItem (int, int);
    BOOL SetItemImage (int, int);
    BOOL SetItemImage (int, int, int);
    int GetItemImage (int);
    int GetItemImage (int, int);
    BOOL SetItemText (LPTSTR, int, int);
    CString GetItemText (int, int);
    DWORD SetStyle (DWORD);
    DWORD SetExtendedStyle (DWORD);
};
