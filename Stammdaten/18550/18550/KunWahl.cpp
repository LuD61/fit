// KunWahl.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "18550.h"
#include "KunWahl.h"

#include "DbClass.h"
#include "kun.h"
#include "mdn.h"

extern KUN_CLASS kun_class ;

static KUN kun_save ;


// CKunWahl-Dialogfeld

IMPLEMENT_DYNAMIC(CKunWahl, CDialog)

CKunWahl::CKunWahl(CWnd* pParent /*=NULL*/)
	: CDialog(CKunWahl::IDD, pParent)
	, v_kunlist(_T(""))
{

}

CKunWahl::~CKunWahl()
{
}


BOOL CKunWahl::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString szkun ;

	memcpy ( &kun_save , &kun, sizeof( KUN) ) ;	// sichern der alten Daten
	
	kun.mdn = mdn.mdn ;
	szkun.Format("       0  Kundenbranche");
	((CListBox *)GetDlgItem(IDC_KUNLIST))->AddString(szkun.GetBuffer(0));
	((CListBox *)GetDlgItem(IDC_KUNLIST))->SetCurSel(0);

	int sqlstat = kun_class.openallkun ();
	sqlstat = kun_class.lesekun();
	while(!sqlstat)
	{
	
			szkun.Format("%8.0d  %s",kun.kun,kun.kun_krz1);
		// hier haben wir jetzt die Kunden und k�nnen sie in die ListBox einf�gen

			((CListBox *)GetDlgItem(IDC_KUNLIST))->AddString(szkun.GetBuffer(0));
			((CListBox *)GetDlgItem(IDC_KUNLIST))->SetCurSel(0);
	
		sqlstat = kun_class.lesekun () ;
	}
	
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}




void CKunWahl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_KUNLIST, m_kunlist);
	DDX_LBString(pDX, IDC_KUNLIST, v_kunlist);
}


BEGIN_MESSAGE_MAP(CKunWahl, CDialog)
	ON_LBN_SELCHANGE(IDC_KUNLIST, &CKunWahl::OnLbnSelchangeKunlist)
	ON_LBN_KILLFOCUS(IDC_KUNLIST, &CKunWahl::OnLbnKillfocusKunlist)
	ON_BN_CLICKED(IDOK, &CKunWahl::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CKunWahl::OnBnClickedCancel)
END_MESSAGE_MAP()


// CKunWahl-Meldungshandler

void CKunWahl::OnLbnSelchangeKunlist()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CKunWahl::OnLbnKillfocusKunlist()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CKunWahl::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	
	char bufh[256] ;
	int i ;

	UpdateData(TRUE);

	int ok = TRUE ;

	bufh[0] = '\0' ;

	int nCurSel = ((CListBox *)GetDlgItem(IDC_KUNLIST))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
//		((CListBox *)GetDlgItem(IDC_LIST1))->GetLBText(nCurSel, bufx);
		((CListBox *)GetDlgItem(IDC_KUNLIST))->GetText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
			kun.kun = atol ( bufh ) ;
			kun.mdn = mdn.mdn ;
			if ( kun.kun == 0 )
			{
				v_kunlist.Format("       0  Kundenbranche",0);
				ok = TRUE ;
			}
			else
			{
				i = kun_class.openkun();
				i = kun_class.lesekun();
				if ( i )
				{
					v_kunlist.Format("                   ");
					ok = FALSE ;
				}
			}
		}
		else
		{
				v_kunlist.Format("                   ");
				ok = FALSE ;
				kun.kun = -1 ;
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_kunlist );
		i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%8.0d", hilfe );
		nCurSel=((CListBox *)GetDlgItem(IDC_KUNLIST))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CListBox *)GetDlgItem(IDC_KUNLIST))->SetCurSel(nCurSel) ;
//			((CListBox *)GetDlgItem(IDC_LIST1))->GetLBText(nCurSel, bufx);
			((CListBox *)GetDlgItem(IDC_KUNLIST))->GetText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 9 )
				{
					bufh[i] = '\0' ;
				}
			}
			else
			{
				bufh[9] = '\0' ;
			}
			kun.kun = atol ( bufh ) ;
			kun.mdn = mdn.mdn ;
			if ( kun.kun == 0 )
			{
				v_kunlist.Format("       0  Kundenbranche");
				ok = TRUE ;
			}
			else
			{
				i = kun_class.openkun();
				i = kun_class.lesekun();
				if ( i )
				{
					v_kunlist.Format("                   ");
					ok = FALSE ;
					kun.kun = -1 ;
				}
			}

		}
		else
		{
			v_kunlist.Format("                   ");
			ok = FALSE ;
			kun.kun = -1 ;
		}
	}
	UpdateData(FALSE);

	if ( ok == FALSE )
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
	}
	else
	{
		OnOK() ;
	}

}

void CKunWahl::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	memcpy ( &kun, &kun_save, sizeof ( KUN ) );

	OnCancel();
}
