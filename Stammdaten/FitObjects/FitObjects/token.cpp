/***************************************************************************/
/* Programmname :  Token.CPP                                               */
/*-------------------------------------------------------------------------*/
/* Funktion :  Klasse CToken                                               */
/*-------------------------------------------------------------------------*/
/* Revision : 1.0    Datum : 07.06.04   erstellt von : W. Roth             */
/*                   allgemneine Klasse zum Zerlegen von Strings           */
/*                                                                         */    
/*-------------------------------------------------------------------------*/
/* Aufruf :                                                                */
/* Funktionswert :                                                         */
/* Eingabeparameter :                                                      */
/*                                                                         */
/* Ausgabeparameter :                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/

#include "stdafx.h"
#include "Token.h"

#ifdef _DEBUG
// #define new DEBUG_NEW
#endif

CToken::CToken ()
{
     Tokens   = NULL;
     AnzToken = NULL;
     AktToken = NULL;
}

CToken::CToken (char *Txt, char *sep)
{
     Tokens = NULL;
     Buffer = Txt;
     Buffer.TrimRight ();
     this->sep = sep;
     GetTokens (Txt);
}

CToken::CToken (CString& Txt, char *sep)
{
     Tokens = NULL;
     Buffer = Txt;
     Buffer.TrimRight ();
     this->sep = sep;
     GetTokens (Txt.GetBuffer (512));

}


CToken::~CToken ()
{
     if (Tokens != NULL)
     {
         for (int i = 0; i < AnzToken; i ++)
         {
               delete Tokens[i];
         }
         delete Tokens;
     }
}

const CToken& CToken::operator=(char *Txt)
{
     if (Tokens != NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt);
     return *this;
}

const CToken& CToken::operator=(CString& Txt)
{
     if (Tokens != NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt.GetBuffer (512));
     return *this;
}

void CToken::Split (char *Txt)
{
     if (Tokens != NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt);
}

void CToken::Split (CString& Txt)
{
     if (Tokens != NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt.GetBuffer (512));
}

void CToken::GetTokens (char *Txt)
{
     AnzToken = 0;
     Buffer.TrimRight ();
     char *b = Buffer.GetBuffer (512);
     char *p = strtok (b, sep.GetBuffer (512));
     while (p != NULL)
     {
             AnzToken ++;
             p = strtok (NULL, sep.GetBuffer (512));
     }
     Tokens = new CString *[AnzToken];
     if (Tokens == NULL)
     {
             return;
     }

     Buffer = Txt;
     Buffer.TrimRight ();
     b = Buffer.GetBuffer (0);
     p = strtok (b, sep.GetBuffer (0));
     int i = 0;
     while (p != NULL)
     {
             Tokens[i] = new CString (p);
             p = strtok (NULL, sep.GetBuffer (512));
             i ++;
     }
     AktToken = 0;
}


void CToken::SetSep (char *sep)
{
     this->sep = sep;
}

char * CToken::NextToken (void)
{
     if (AktToken == AnzToken)
     {
          AktToken = 0;
          return NULL;
     }
     AktToken ++;
     return Tokens[AktToken - 1]->GetBuffer (512);
}

char * CToken::GetToken (int idx)
{
     if (idx < 0 || idx >= AnzToken)
     {
           return NULL;
     }

     return Tokens [idx]->GetBuffer (512);
}

int CToken::GetAnzToken (void)
{
     return AnzToken;
}

