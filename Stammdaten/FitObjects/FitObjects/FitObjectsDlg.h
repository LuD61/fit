#pragma once
#include <atlstr.h>

class CFitObjectsDlg
{
private:
    HWND m_hWnd;  
	CString m_Text;
	CString Line;
	int Lines;
	BOOL visible;
protected:
	static CFitObjectsDlg *instance;
	CFitObjectsDlg ();
	~CFitObjectsDlg ();
    void AboutHelp ();
    static INT_PTR CALLBACK DlgProc (HWND hwndDlg, UINT message, 
 				         WPARAM wParam, LPARAM lParam); 
public:
	CString& GetLine ()
	{
		return Line;
	}
	void SetVisible (BOOL visible=true)
	{
		this->visible = visible;
	}

	BOOL IsVisible ()
	{
		return visible;
	}
	BOOL Create ();
	void Show (BOOL b);
	void SetDlgPos (HWND dlg);
	void MoveControl (HWND dlg, int cx, int cy);
	void AddLine (CString& Line);
	static CFitObjectsDlg *GetInstance ();
	static void DestroyInstance ();
};
