#ifndef __COM_OBJECT_DEF
#define __COM_OBJECT_DEF

class CGuid
{
public:
	CString Name;
	BYTE ClsID [16];
	CGuid (BYTE *Guid, LPTSTR Name=_T("Guid"));
	void Save ();
};

class ComObject
{
public:
	CString Name;
	IUnknown *pUnknown;
	IDispatch *idp;
	CString App;
	CString TLib;
	BYTE AppID [16];
	BYTE LibID [16];
	ComObject::ComObject ();
	ComObject::ComObject (LPTSTR Name);
	IDispatch* CreateObject ();
	void Release ();
};
#endif

