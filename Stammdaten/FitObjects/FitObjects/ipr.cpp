#include "stdafx.h"
#include "ipr.h"

struct IPR ipr, ipr_null;

void IPR_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((char *) &ipr.mdn, 2, 0);
            sqlin ((char *) &ipr.a, 3, 0);
            sqlin ((char *) &ipr.pr_gr_stuf, 1, 0);
            sqlin ((char *) &ipr.kun_pr, 2, 0);
            sqlin ((char *) &ipr.kun, 2, 0);
    sqlout ((short *) &ipr.mdn,SQLSHORT,0);
    sqlout ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
    sqlout ((long *) &ipr.kun_pr,SQLLONG,0);
    sqlout ((double *) &ipr.a,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((short *) &ipr.aktion_nr,SQLSHORT,0);
    sqlout ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
    sqlout ((TCHAR *) ipr.modif,SQLCHAR,2);
    sqlout ((short *) &ipr.waehrung,SQLSHORT,0);
    sqlout ((long *) &ipr.kun,SQLLONG,0);
    sqlout ((double *) &ipr.a_grund,SQLDOUBLE,0);
    sqlout ((TCHAR *) ipr.kond_art,SQLCHAR,5);
    sqlout ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
    sqlout ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select ipr.mdn,  ")
_T("ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  ")
_T("ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  ")
_T("ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  ")
_T("ipr.add_ek_abs from ipr ")

#line 16 "ipr.rpp"
                                _T("where mdn = ? ")
                                  _T("and   a   = ? ")
                                  _T("and   pr_gr_stuf = ? ")
                                  _T("and kun_pr = ? ")
                                  _T("and kun = ?")); 



    sqlin ((short *) &ipr.mdn,SQLSHORT,0);
    sqlin ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &ipr.kun_pr,SQLLONG,0);
    sqlin ((double *) &ipr.a,SQLDOUBLE,0);
    sqlin ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &ipr.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((short *) &ipr.aktion_nr,SQLSHORT,0);
    sqlin ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
    sqlin ((TCHAR *) ipr.modif,SQLCHAR,2);
    sqlin ((short *) &ipr.waehrung,SQLSHORT,0);
    sqlin ((long *) &ipr.kun,SQLLONG,0);
    sqlin ((double *) &ipr.a_grund,SQLDOUBLE,0);
    sqlin ((TCHAR *) ipr.kond_art,SQLCHAR,5);
    sqlin ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
    sqlin ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
            sqltext = _T("update ipr set ipr.mdn = ?,  ")
_T("ipr.pr_gr_stuf = ?,  ipr.kun_pr = ?,  ipr.a = ?,  ipr.vk_pr_i = ?,  ")
_T("ipr.vk_pr_eu = ?,  ipr.ld_pr = ?,  ipr.ld_pr_eu = ?,  ")
_T("ipr.aktion_nr = ?,  ipr.a_akt_kz = ?,  ipr.modif = ?,  ")
_T("ipr.waehrung = ?,  ipr.kun = ?,  ipr.a_grund = ?,  ipr.kond_art = ?,  ")
_T("ipr.add_ek_proz = ?,  ipr.add_ek_abs = ? ")

#line 25 "ipr.rpp"
                               _T("where mdn = ? ")
                               _T("and   a = ? ")
                               _T("and   pr_gr_stuf = ? ")
                               _T("and   kun_pr = ? ")
                               _T("and   kun = ?");
  
            sqlin ((char *) &ipr.mdn, 2, 0);
            sqlin ((char *) &ipr.a, 3, 0);
            sqlin ((char *) &ipr.pr_gr_stuf, 1, 0);
            sqlin ((char *) &ipr.kun_pr,  2, 0);  
            sqlin ((char *) &ipr.kun,  2, 0);  

            upd_cursor = sqlcursor (sqltext);

    sqlin ((short *) &ipr.mdn,SQLSHORT,0);
    sqlin ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &ipr.kun_pr,SQLLONG,0);
    sqlin ((double *) &ipr.a,SQLDOUBLE,0);
    sqlin ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &ipr.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((short *) &ipr.aktion_nr,SQLSHORT,0);
    sqlin ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
    sqlin ((TCHAR *) ipr.modif,SQLCHAR,2);
    sqlin ((short *) &ipr.waehrung,SQLSHORT,0);
    sqlin ((long *) &ipr.kun,SQLLONG,0);
    sqlin ((double *) &ipr.a_grund,SQLDOUBLE,0);
    sqlin ((TCHAR *) ipr.kond_art,SQLCHAR,5);
    sqlin ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
    sqlin ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into ipr (mdn,  ")
_T("pr_gr_stuf,  kun_pr,  a,  vk_pr_i,  vk_pr_eu,  ld_pr,  ld_pr_eu,  aktion_nr,  ")
_T("a_akt_kz,  modif,  waehrung,  kun,  a_grund,  kond_art,  add_ek_proz,  add_ek_abs) ")

#line 40 "ipr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?)")); 

#line 42 "ipr.rpp"
            sqlin ((char *) &ipr.mdn, 2, 0);
            sqlin ((char *) &ipr.a, 3, 0);
            sqlin ((char *) &ipr.pr_gr_stuf, 1, 0);
            sqlin ((char *) &ipr.kun_pr,  2, 0);  
            sqlin ((char *) &ipr.kun,  2, 0);  
            test_upd_cursor = sqlcursor (_T("select a from ipr ")
                                  _T("where mdn = ? ")
                                  _T("and   a   = ? ")
                                  _T("and   pr_gr_stuf = ? ")
                                  _T("and kun_pr = ? ")
                                  _T("and kun = ?")); 
              
            sqlin ((char *) &ipr.mdn, 2, 0);
            sqlin ((char *) &ipr.a, 3, 0);
            sqlin ((char *) &ipr.pr_gr_stuf, 1, 0);
            sqlin ((char *) &ipr.kun_pr,  2, 0);  
            sqlin ((char *) &ipr.kun,  2, 0);  
            del_cursor = sqlcursor (_T("delete from ipr ")
                                  _T("where mdn = ? ")
                                  _T("and   a   = ? ")
                                  _T("and   pr_gr_stuf = ? ")
                                  _T("and kun_pr = ? ")
                                  _T("and kun = ?")); 
}

void IPR_CLASS::prepare_a (char *order)
{
            char sqltext[512];
            sqlin ((char *) &ipr.mdn, 2, 0);
            sqlin ((char *) &ipr.a, 3, 0);
            if (order)
            {
    sqlout ((short *) &ipr.mdn,SQLSHORT,0);
    sqlout ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
    sqlout ((long *) &ipr.kun_pr,SQLLONG,0);
    sqlout ((double *) &ipr.a,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((short *) &ipr.aktion_nr,SQLSHORT,0);
    sqlout ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
    sqlout ((TCHAR *) ipr.modif,SQLCHAR,2);
    sqlout ((short *) &ipr.waehrung,SQLSHORT,0);
    sqlout ((long *) &ipr.kun,SQLLONG,0);
    sqlout ((double *) &ipr.a_grund,SQLDOUBLE,0);
    sqlout ((TCHAR *) ipr.kond_art,SQLCHAR,5);
    sqlout ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
    sqlout ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
                        sprintf (sqltext, _T("select ipr.mdn,  ")
_T("ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  ")
_T("ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  ")
_T("ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  ")
_T("ipr.add_ek_abs from ipr ")

#line 75 "ipr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   a   = ? ")
                                  _T("%s"), order);
                        cursor_a = sqlcursor (sqltext);
            }
            else
            {
    sqlout ((short *) &ipr.mdn,SQLSHORT,0);
    sqlout ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
    sqlout ((long *) &ipr.kun_pr,SQLLONG,0);
    sqlout ((double *) &ipr.a,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((short *) &ipr.aktion_nr,SQLSHORT,0);
    sqlout ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
    sqlout ((TCHAR *) ipr.modif,SQLCHAR,2);
    sqlout ((short *) &ipr.waehrung,SQLSHORT,0);
    sqlout ((long *) &ipr.kun,SQLLONG,0);
    sqlout ((double *) &ipr.a_grund,SQLDOUBLE,0);
    sqlout ((TCHAR *) ipr.kond_art,SQLCHAR,5);
    sqlout ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
    sqlout ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
                        cursor_a = sqlcursor (_T("select ")
_T("ipr.mdn,  ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  ")
_T("ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  ")
_T("ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  ")
_T("ipr.add_ek_abs from ipr ")

#line 83 "ipr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   a   = ?"));
            }
            sqlin ((char *) &ipr.mdn, 2, 0);
            sqlin ((char *) &ipr.a, 3, 0);
            del_a_curs = sqlcursor (_T("delete from ipr ")
                                  _T("where mdn = ? ")
                                  _T("and   a   = ?"));
}

void IPR_CLASS::prepare_gr (char *order)
{
            char sqltext[512];
 
            sqlin ((char *) &ipr.mdn, 2, 0);
            sqlin ((char *) &ipr.pr_gr_stuf, 1, 0);
            sqlin ((char *) &ipr.kun_pr, 2, 0);
            if (order)
            {
                       
    sqlout ((short *) &ipr.mdn,SQLSHORT,0);
    sqlout ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
    sqlout ((long *) &ipr.kun_pr,SQLLONG,0);
    sqlout ((double *) &ipr.a,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((short *) &ipr.aktion_nr,SQLSHORT,0);
    sqlout ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
    sqlout ((TCHAR *) ipr.modif,SQLCHAR,2);
    sqlout ((short *) &ipr.waehrung,SQLSHORT,0);
    sqlout ((long *) &ipr.kun,SQLLONG,0);
    sqlout ((double *) &ipr.a_grund,SQLDOUBLE,0);
    sqlout ((TCHAR *) ipr.kond_art,SQLCHAR,5);
    sqlout ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
    sqlout ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
                        sprintf (sqltext, _T("select ipr.mdn,  ")
_T("ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  ")
_T("ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  ")
_T("ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  ")
_T("ipr.add_ek_abs from ipr ")

#line 104 "ipr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   pr_gr_stuf   = ? ")
                                  _T("and   kun_pr   = ? ")
                                  _T("%s"), order);
                        cursor_gr = sqlcursor (sqltext);
            }
            else
            {
    sqlout ((short *) &ipr.mdn,SQLSHORT,0);
    sqlout ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
    sqlout ((long *) &ipr.kun_pr,SQLLONG,0);
    sqlout ((double *) &ipr.a,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((short *) &ipr.aktion_nr,SQLSHORT,0);
    sqlout ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
    sqlout ((TCHAR *) ipr.modif,SQLCHAR,2);
    sqlout ((short *) &ipr.waehrung,SQLSHORT,0);
    sqlout ((long *) &ipr.kun,SQLLONG,0);
    sqlout ((double *) &ipr.a_grund,SQLDOUBLE,0);
    sqlout ((TCHAR *) ipr.kond_art,SQLCHAR,5);
    sqlout ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
    sqlout ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
                        cursor_gr = sqlcursor (_T("select ")
_T("ipr.mdn,  ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  ")
_T("ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  ")
_T("ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  ")
_T("ipr.add_ek_abs from ipr ")

#line 113 "ipr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   pr_gr_stuf   = ? ")
                                  _T("and   kun_pr   = ?"));
            }
            sqlin ((char *) &ipr.mdn, 2, 0);
            sqlin ((char *) &ipr.pr_gr_stuf, 1, 0);
            sqlin ((char *) &ipr.kun_pr, 2, 0);
            del_gr_curs = sqlcursor (_T("delete from ipr ")
                                  _T("where mdn = ? ")
                                  _T("and   pr_gr_stuf   = ? ")
                                  _T("and   kun_pr   = ?"));
}

int IPR_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int IPR_CLASS::dbreadfirst_a (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_a == -1)
         {
                this->prepare_a (order);
         }
         sqlopen (cursor_a);
         return sqlfetch (cursor_a);
}

int IPR_CLASS::dbread_a (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return sqlfetch (cursor_a);
}

int IPR_CLASS::dbreadfirst_gr (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_gr == -1)
         {
                this->prepare_gr (order);
         }
         sqlopen (cursor_gr);
         return sqlfetch (cursor_gr);
}

int IPR_CLASS::dbread_gr (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return sqlfetch (cursor_gr);
}

int IPR_CLASS::dbdelete_gr (void)
{
         if (del_gr_curs == -1)
         {
                this->prepare_a (NULL);
         }
         return sqlexecute (del_gr_curs);
}
       

int IPR_CLASS::dbdelete_a (void)
{
         if (del_a_curs == -1)
         {
                this->prepare_a (NULL);
         }
         return sqlexecute (del_a_curs);
}
       


int IPR_CLASS::dbclose_a (void)
{
        if (cursor_a != -1)
        { 
                 sqlclose (cursor_a);
                 cursor_a = -1;
        }
        return 0;
}


int IPR_CLASS::dbclose_gr (void)
{
        if (cursor_gr != -1)
        { 
                 sqlclose (cursor_gr);
                 cursor_gr = -1;
        }
        return 0;
}

