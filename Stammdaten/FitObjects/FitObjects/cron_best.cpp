#include "stdafx.h"
#include "cron_best.h"

struct CRON_BEST cron_best, cron_best_null, cron_best_def;

void CRON_BEST_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &cron_best.a,  SQLDOUBLE, 0);
            sqlin ((short *)    &cron_best.pr_kz,  SQLSHORT, 0);
    sqlout ((double *) &cron_best.a,SQLDOUBLE,0);
    sqlout ((short *) &cron_best.pr_kz,SQLSHORT,0);
    sqlout ((long *) &cron_best.bsd_stk,SQLLONG,0);
    sqlout ((double *) &cron_best.me,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select cron_best.a,  ")
_T("cron_best.pr_kz,  cron_best.bsd_stk,  cron_best.me from cron_best ")

#line 13 "cron_best.rpp"
                                  _T("where a = ? ")
                                  _T("and pr_kz = ?"));
            sqlin ((double *)   &cron_best.a,  SQLDOUBLE, 0);
            sqlout ((double *)   &cron_best.me,  SQLDOUBLE, 0);
            sqlout ((long *)   &cron_best.bsd_stk,  SQLLONG, 0);
            cursor_a = sqlcursor (_T("select sum (me), sum (bsd_stk) from cron_best ")
                                  _T("where a = ? "));
    sqlin ((double *) &cron_best.a,SQLDOUBLE,0);
    sqlin ((short *) &cron_best.pr_kz,SQLSHORT,0);
    sqlin ((long *) &cron_best.bsd_stk,SQLLONG,0);
    sqlin ((double *) &cron_best.me,SQLDOUBLE,0);
            sqltext = _T("update cron_best set ")
_T("cron_best.a = ?,  cron_best.pr_kz = ?,  cron_best.bsd_stk = ?,  ")
_T("cron_best.me = ? ")

#line 21 "cron_best.rpp"
                                  _T("where a = ? ")
                                  _T("and pr_kz = ?");
            sqlin ((double *)   &cron_best.a,  SQLDOUBLE, 0);
            sqlin ((short *)    &cron_best.pr_kz,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &cron_best.a,  SQLDOUBLE, 0);
            sqlin ((short *)    &cron_best.pr_kz,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select a from cron_best ")
                                  _T("where a = ? ")
                                  _T("and pr_kz = ?"));
            sqlin ((double *)   &cron_best.a,  SQLDOUBLE, 0);
            sqlin ((short *)    &cron_best.pr_kz,  SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from cron_best ")
                                  _T("where a = ?"));
    sqlin ((double *) &cron_best.a,SQLDOUBLE,0);
    sqlin ((short *) &cron_best.pr_kz,SQLSHORT,0);
    sqlin ((long *) &cron_best.bsd_stk,SQLLONG,0);
    sqlin ((double *) &cron_best.me,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into cron_best (")
_T("a,  pr_kz,  bsd_stk,  me) ")

#line 37 "cron_best.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?)"));

#line 39 "cron_best.rpp"
}

int CRON_BEST_CLASS::dbreadfirst_a ()
{
    int dsqlstatus = 100;
	if (cursor_a == -1)
        {
                prepare ();
        }
        if ((dsqlstatus = sqlopen (cursor_a)) == 0)
        {
                dsqlstatus = sqlfetch (cursor_a);
        }
        return dsqlstatus;
} 	

int CRON_BEST_CLASS::dbread_a ()
{
        return sqlfetch (cursor_a);
} 	
