#include "StdAfx.h"
#include <comdef.h>
#include <atlstr.h>
#include "ComObject.h"

CGuid::CGuid (BYTE *Guid, LPTSTR Name)
{
	BYTE hi, lo;
    int i = 0;
	int j = 0;
	unsigned long part1;
	unsigned short part2;
	unsigned short part3;
//	unsigned short part4;
	unsigned char partn;

	this->Name = Name;
    part1 = 0l;
	for (j = 0, i = 0; j <  8; i ++)
	{
		if (Guid[i] == '-' || Guid[i] == '{' ||
			Guid[i] == '}')
		{
			continue;
		}
		hi = Guid[i];
		if (hi > 0x39)
		{
			hi = hi + 10 - 0x41;
		}
		else 
		{
			hi -= 0x30;
		}
		if (j > 0)
		{
			part1 *= 16;
		}
		part1 +=  hi;
		i ++;
		j ++;
		lo = Guid[i];
		if (lo > 0x39)
		{
			lo = lo + 10 - 0x41;
		}
		else 
		{
			lo -= 0x30;
		}
		part1 *= 16;
		part1 += lo;
		j ++;
	}

	memcpy (ClsID, (char *) &part1, sizeof (long));

    part2 = 0l;
	for (j = 0; j <  4; i ++)
	{
		if (Guid[i] == '-' || Guid[i] == '{' ||
			Guid[i] == '}')
		{
			continue;
		}
		hi = Guid[i];
		if (hi > 0x39)
		{
			hi = hi + 10 - 0x41;
		}
		else 
		{
			hi -= 0x30;
		}
		if (j > 0)
		{
			part2 *= 16;
		}
		part2 +=  hi;
		i ++;
		j ++;
		lo = Guid[i];
		if (lo > 0x39)
		{
			lo = lo + 10 - 0x41;
		}
		else 
		{
			lo -= 0x30;
		}
		part2 *= 16;
		part2 += lo;
		j ++;
	}

	memcpy (&ClsID[sizeof (long)], (char *) &part2, sizeof (short));

    part3 = 0l;
	for (j = 0; j <  4; i ++)
	{
		if (Guid[i] == '-' || Guid[i] == '{' ||
			Guid[i] == '}')
		{
			continue;
		}
		hi = Guid[i];
		if (hi > 0x39)
		{
			hi = hi + 10 - 0x41;
		}
		else 
		{
			hi -= 0x30;
		}
		if (j > 0)
		{
			part3 *= 16;
		}
		part3 +=  hi;
		i ++;
		j ++;
		lo = Guid[i];
		if (lo > 0x39)
		{
			lo = lo + 10 - 0x41;
		}
		else 
		{
			lo -= 0x30;
		}
		part3 *= 16;
		part3 += lo;
		j ++;
	}

	memcpy (&ClsID[sizeof (long) + sizeof (short)], (char *) &part3, sizeof (short));

	partn = 0;
	for (j = 0; j <  16; i ++)
	{
		if (Guid[i] == '-' || Guid[i] == '{' ||
			Guid[i] == '}')
		{
			continue;
		}
		hi = Guid[i];
		if (hi > 0x39)
		{
			hi = hi + 10 - 0x41;
		}
		else 
		{
			hi -= 0x30;
		}
		partn =  hi;
		i ++;
		j ++;
		lo = Guid[i];
		if (lo > 0x39)
		{
			lo = lo + 10 - 0x41;
		}
		else 
		{
			lo -= 0x30;
		}
		partn *= 16;
		partn += lo;
		memcpy (&ClsID[sizeof (long) + 2 * sizeof (short) + j/2], (char *) &partn, sizeof (char));
		j ++;
	}
//	Save ();
}


void CGuid::Save ()
{
	FILE *fp = fopen (Name.GetBuffer (), "wb");
	if (fp != NULL)
	{
		fwrite (ClsID, 1, sizeof (ClsID), fp);
		CLSID Test = {0x8F04F7DC, 0x8C40, 0x11D3, 
		{0x9F, 0x5C, 0x00, 0x50, 0x04, 0x35, 0x77, 0xB8}};
		fwrite (&Test, 1, sizeof (CLSID), fp);
		fclose (fp);
	}
}


ComObject::ComObject ()
{
	Name = "";
	idp = NULL;
}

ComObject::ComObject (LPTSTR Name)
{
	this->Name = Name;
	idp = NULL;
}

IDispatch* ComObject::CreateObject ()
{
	if (Name == "") return NULL;
    HKEY hKey;

	CString Key = Name;
	Key += "\\CLSID";
    DWORD rc = RegOpenKeyEx( HKEY_CLASSES_ROOT,
                        Key.GetBuffer (),
                        0,
						KEY_QUERY_VALUE, 
                        &hKey);
	 if (rc != ERROR_SUCCESS)
	 {
		 return NULL;
	 }
 
     DWORD type;
     TCHAR name [256];
     DWORD cname = sizeof (name);
     BYTE value [512];
     DWORD cvalue = sizeof (value);

// ClsId f�r ComObject holen und konvertieren.
     HRESULT hr = RegEnumValue(hKey, 0, name, &cname, NULL, &type,
		              value, &cvalue);  

	 App = value;
	 CGuid Application ((BYTE *) App.GetBuffer (), _T("Application"));
	 memcpy (AppID, Application.ClsID, sizeof (AppID));

// ClsId f�r Typelib holen und konvertieren. Wird zur Zeit nicht benutzt.
	 CString TypeLib;
	 TypeLib.Format (_T("CLSID\\%s\\TypeLib"), App.GetBuffer ());

     rc = RegOpenKeyEx( HKEY_CLASSES_ROOT,
                       TypeLib.GetBuffer (),
                        0,
						KEY_QUERY_VALUE, 
                        &hKey);

	if (rc == ERROR_SUCCESS)
	{
		hr = RegEnumValue(hKey, 0, name, &cname, NULL, &type,
		              value, &cvalue);  
		TLib = value;
		CGuid Server ((BYTE *) TLib.GetBuffer (), _T("Server"));
		memcpy (LibID, Server.ClsID, sizeof (LibID));
	}
	else
	{
		 TLib = _T("NOLIB");
	}

	hr = CoCreateInstance ((IID &)  Application.ClsID, NULL,
	 					    CLSCTX_SERVER,
						    IID_IUnknown, (void **)&pUnknown); 

	hr = pUnknown->QueryInterface (IID_IDispatch, (void **) &idp);
	if (FAILED (hr))
	{
        return NULL;
	}

	pUnknown->Release ();
    return idp;
}

void ComObject::Release ()
{
	if (idp != NULL)
	{
		idp->Release ();
		idp = NULL;
	}
}
