#pragma once

class CFileHandler
{
private:
	CString m_FileName;
	long m_DiffTime;
public:
	void SetFileName (CString &FileName)
	{
		m_FileName = FileName;
	}

	void SetFileName (LPTSTR FileName)
	{
		m_FileName = FileName;
	}

	CString & GetFileName ()
	{
		return m_FileName;
	}

	void SetDiffTime (long DiffTime)
	{
		m_DiffTime = DiffTime;
	}

	CFileHandler(void);
	CFileHandler(CString& FileName);
	CFileHandler(LPTSTR FileName);
	~CFileHandler(void);
	BOOL IsNewFile ();
};
