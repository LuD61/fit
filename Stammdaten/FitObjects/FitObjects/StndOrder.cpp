// StndOrder.cpp: Implementierung von CStndOrder

#include "stdafx.h"
#include "StndOrder.h"
#include "FitObjectsDlg.h"
#include "StrFuncs.h"
#include "ClientHandler.h"

#define DLG CFitObjectsDlg::GetInstance ()

#define ROW DLG->GetLine () 

// #define LISTHANDLER CListHandler::GetInstance ()
#define LISTHANDLER ClientOrder->GetListHandler ()
#define STRTODOUBLE CStrFuncs::StrToDouble

// CStndOrder


STDMETHODIMP CStndOrder::put_SortMode(SHORT newVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	CClientHandler::GetInstance ()->SetSortMode ((CClientHandler::STD_SORT) newVal);
	ROW.Format ("Mdn = %hd", newVal);
	DLG->AddLine (ROW);
	return S_OK;
}

STDMETHODIMP CStndOrder::put_RdOptimize(SHORT newVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	CClientHandler::GetInstance ()->SetRdOptimize ((CClientHandler::STD_RDOPTIMIZE) newVal);
	ROW.Format ("Mdn = %hd", newVal);
	DLG->AddLine (ROW);
	return S_OK;
}

STDMETHODIMP CStndOrder::put_Mdn(SHORT newVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	ClientOrder->SetMdn (newVal);
	ROW.Format ("Mdn = %hd", newVal);
	DLG->AddLine (ROW);
	return S_OK;
}

STDMETHODIMP CStndOrder::put_Fil(SHORT newVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	ClientOrder->SetFil (newVal);
	ROW.Format ("Fil = %hd", newVal);
	DLG->AddLine (ROW);
	return S_OK;
}

STDMETHODIMP CStndOrder::put_KunFil(SHORT newVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	ClientOrder->SetKunFil (newVal);
	ROW.Format ("KunFil = %hd", newVal);
	DLG->AddLine (ROW);
	return S_OK;
}

STDMETHODIMP CStndOrder::put_Kun(LONG newVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	ClientOrder->SetKunNr (newVal);
	ROW.Format ("Kun = %ld", newVal);
	DLG->AddLine (ROW);
	return S_OK;
}

STDMETHODIMP CStndOrder::put_AktAuf(LONG newVal)
{
	// TODO: Add your implementation code here

	ClientOrder->SetAktAuf (newVal);
	return S_OK;
}

STDMETHODIMP CStndOrder::get_Name(BSTR* pVal)
{
	// TODO: Add your implementation code here

	*pVal = Name;
	return S_OK;
}

STDMETHODIMP CStndOrder::SetWindow(void)
{
	// TODO: Add your implementation code here

	DLG->Create();
	DLG->Show (true);
	DLG->AddLine (CString (_T("FitObjects gestartet")));

	return S_OK;
}

STDMETHODIMP CStndOrder::Load(short client)
{
	// TODO: Add your implementation code here

	DLG->AddLine (CString (_T("Laden des Standardauftrags gestartet")));
	CClientHandler *ClientHandler = CClientHandler::GetInstance ();
	ClientOrder = ClientHandler->GetClient ((int) client);
	if (ClientOrder != NULL)
	{
		sprintf (LINE, "Lade Daten f�r Client %hd", client);
		IDEBUG->Write ();
		ClientOrder->SetKeys ();
		sprintf (LINE, "Werte f�r Zugriff eingestellt");
		IDEBUG->Write ();
		ClientOrder->Load ();
		ClientOrder->WriteAufps ();
	}
	return S_OK;
}


STDMETHODIMP CStndOrder::GetFirstRecord(short client, IDispatch **pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	AUFPS **it;
	AUFPS *Aufps;

	CClientHandler *ClientHandler = CClientHandler::GetInstance ();
	ClientOrder = ClientHandler->GetClient ((int) client);
	if (ClientOrder != NULL)
	{
		sprintf (LINE, "1. Satz f�r Client %hd", client);
		IDEBUG->Write ();
		LISTHANDLER->GetStndAll ()->Start ();
		it = LISTHANDLER->GetStndAll ()->GetNext ();
		if (it != NULL)
		{
			Aufps = *it;
			comCAufps.Name = (_T("FitObjects.CAufps"));
			CAufps = (CCAufps *) comCAufps.CreateObject ();
			CAufps->SetEndOfRecord (FALSE);
			*pVal = CAufps;
			SetValues (CAufps, Aufps);
		}
		else
		{
			comCAufps.Name = (_T("FitObjects.CAufps"));
			CAufps = (CCAufps *) comCAufps.CreateObject ();
			CAufps->SetEndOfRecord (TRUE);
			*pVal = CAufps;
		}
	}
	return S_OK;
}

STDMETHODIMP CStndOrder::GetNextRecord( short client, IDispatch **pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	AUFPS **it;
	AUFPS *Aufps;


	CClientHandler *ClientHandler = CClientHandler::GetInstance ();
	ClientOrder = ClientHandler->GetClient ((int) client);
	if (ClientOrder != NULL)
	{
		sprintf (LINE, "n�chster Satz f�r Client %hd", client);
		IDEBUG->Write ();
		comCAufps.Release ();
		it = LISTHANDLER->GetStndAll ()->GetNext ();
		if (it != NULL)
		{
			Aufps = *it;
			com.Name = (_T("FitObjects.CAufps"));
			CAufps = (CCAufps *) com.CreateObject ();
			CAufps->SetEndOfRecord (FALSE);
			*pVal = CAufps;
			SetValues (CAufps, Aufps);
		}
		else
		{
			com.Name = (_T("FitObjects.CAufps"));
			CAufps = (CCAufps *) com.CreateObject ();
			CAufps->SetEndOfRecord (TRUE);
			*pVal = CAufps;
		}
	}
	return S_OK;
}

void CStndOrder::SetValues (CCAufps *CAufps, AUFPS *Aufps)
{
	ROW.Format ("Artikel %.0lf %s", STRTODOUBLE (Aufps->a), Aufps->a_bz1);
	DLG->AddLine (ROW);
	CAufps->SetPosi (atol (Aufps->posi));
	CAufps->SetA (STRTODOUBLE (Aufps->a));
	CAufps->SetABz1 ((char *) Aufps->a_bz1);
	CAufps->SetAufVkPr (STRTODOUBLE (Aufps->auf_vk_pr));
	CAufps->SetAufLadPr (STRTODOUBLE (Aufps->auf_lad_pr));
	CAufps->SetAufLadPr0 (STRTODOUBLE (Aufps->auf_lad_pr0));
	CAufps->SetMarge (STRTODOUBLE (Aufps->marge));
	CAufps->SetAufLadPrPrc (STRTODOUBLE (Aufps->auf_lad_pr_prc));
	CAufps->SetSaKzSint (atoi (Aufps->sa_kz_sint));
	CAufps->SetAGew (Aufps->a_gew);
	CAufps->SetDrFolge (Aufps->dr_folge);
	CAufps->SetDrFolge (Aufps->dr_folge);
	CAufps->SetMeEinh (atoi (Aufps->me_einh));
	CAufps->SetSmt (Aufps->smt);
	CAufps->SetBasisMeBz (Aufps->basis_me_bz);
	CAufps->SetMeEinhKun (atoi (Aufps->me_einh_kun));
	CAufps->SetMeEinhKun1 (atoi (Aufps->me_einh_kun1));
	CAufps->SetMeEinhKun2 (atoi (Aufps->me_einh_kun2));
	CAufps->SetMeEinhKun3 (atoi (Aufps->me_einh_kun3));
	CAufps->SetInh1 (STRTODOUBLE (Aufps->inh1));
	CAufps->SetInh2 (STRTODOUBLE (Aufps->inh2));
	CAufps->SetInh3 (STRTODOUBLE (Aufps->inh3));
	CAufps->SetMeBz (Aufps->me_bz);
	CAufps->SetLastMe (STRTODOUBLE (Aufps->last_me));
	CAufps->SetLastLDat (Aufps->last_ldat);
	CAufps->SetLastPrVk (STRTODOUBLE (Aufps->last_pr_vk));
	CAufps->SetBsd (STRTODOUBLE (Aufps->bsd));
	CAufps->SetBsd2 (STRTODOUBLE (Aufps->bsd2));
}

STDMETHODIMP CStndOrder::RegisterClient(SHORT* pVal)
{
	// TODO: Add your implementation code here


	CClientHandler *ClientHandler = CClientHandler::GetInstance ();
	if (!ClientHandler->Lock ())
	{
		*pVal = 0;
	}
	else
	{
		ClientOrder = new CClientOrder ();
		int client = ClientHandler->Add (ClientOrder);
		*pVal = client;
		sprintf (LINE, "Client %hd registriert", *pVal);
		IDEBUG->Write ();
	}
	return S_OK;
}

STDMETHODIMP CStndOrder::UnregisterClient(SHORT pVal)
{
	// TODO: Add your implementation code here

	CClientHandler *ClientHandler = CClientHandler::GetInstance ();
	ClientHandler->Drop (pVal);
    sprintf (LINE, "Client %hd Registrierung aufgel�st", pVal);
	IDEBUG->Write ();
	comCAufps.Release ();
	ClientHandler->Unlock ();
	return S_OK;
}

STDMETHODIMP CStndOrder::SetKeys(SHORT mdn, SHORT fil, SHORT kun_fil, LONG kun, LONG akt_auf, SHORT client)
{
	// TODO: Add your implementation code here

	CClientHandler *ClientHandler = CClientHandler::GetInstance ();
	CClientOrder *ClientOrder = ClientHandler->GetClient ((int) client);
	if (ClientOrder != NULL)
	{
		ClientOrder->SetMdn (mdn);
		ClientOrder->SetFil (fil);
		ClientOrder->SetKunFil (kun_fil);
		ClientOrder->SetKunNr (kun);
		ClientOrder->SetAktAuf (akt_auf);
	}
	return S_OK;
}
