#include "StdAfx.h"
#include "strfuncs.h"
#include "Token.h"
#include <math.h>
#include <time.h>

TCHAR *CStrFuncs::Separator = _T(";");
CString CStrFuncs::Format = _T("%.4lf");

CStrFuncs::CStrFuncs(void)
{
}

CStrFuncs::~CStrFuncs(void)
{
}

void CStrFuncs::Round (double d, CString& Value)
{
	Value.Format (Format.GetBuffer (), d);
	_tstof (Value.GetBuffer ());
}

void CStrFuncs::Round (double d, int scale, CString& Value)
{
	CString Format;
	Format.Format (_T("%c.%dlf"), '%', scale);
	Value.Format (Format.GetBuffer (), d);
}

double CStrFuncs::RoundToDouble (double d, int scale)
{
	double f = 0.5;
	CString Format;
	CString Value;
	Format.Format (_T("%c.%dlf"), '%', scale);
    f = f / pow (10, (double) scale);
	Value.Format (Format.GetBuffer (), d);
	return StrToDouble (Value);
}


BOOL CStrFuncs::IsDoubleEqual (double d1, double d2)
{
	CString D1;
	CString D2;

	Round (d1, D1);
	Round (d2, D2);
	D1.Trim ();
	D2.Trim ();
	if (D1 == D2)
	{
		return TRUE;
	}
	return FALSE;
}


double CStrFuncs::StrToDoubleA (LPSTR string)
{
 double fl;
 double nk;
 double ziffer;
 double teiler;
 short minus;

 if (string == NULL) return (double) 0.0;
 fl = 0.0;
 nk = 0.0;
 teiler = 10;
 minus = 1;
 while (*string < 0X30)
 {
  if (*string == 0)
    return (0.0);
  if (*string == '-')
    break;
  if (*string == '+')
    break;
  if (*string == '.')
    break;
  if (*string == ',')
    break;
  string ++;
 }

 if (*string == '-')
 {
  minus = -1;
  string ++;
 }
 else if (*string == '+')
 {
  string ++;
 }

 while (*string)
 {
  if (*string == '.')
  {
   break;
  }
  if (*string == ',')
  {
   break;
  }
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = (fl * teiler) + ziffer;
  string ++;
 }

 if (*string == '.');
 else if (*string == ',');
 else
 {
  fl *= minus;
  return (fl);
 }
 string ++;
 while (*string)
 {
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  nk = (double) ziffer / teiler;
  fl += nk;
  teiler *= 10;
  string ++;
 }
// fl += nk;
 return (fl *= minus);
}


double CStrFuncs::StrToDouble (LPTSTR string)
{
 double fl;
 double nk;
 double ziffer;
 double teiler;
 short minus;

 if (string == NULL) return (double) 0.0;
 fl = 0.0;
 nk = 0.0;
 teiler = 10;
 minus = 1;
 while (*string < 0X30)
 {
  if (*string == 0)
    return (0.0);
  if (*string == '-')
    break;
  if (*string == '+')
    break;
  if (*string == '.')
    break;
  if (*string == ',')
    break;
  string ++;
 }

 if (*string == '-')
 {
  minus = -1;
  string ++;
 }
 else if (*string == '+')
 {
  string ++;
 }

 while (*string)
 {
  if (*string == '.')
  {
   break;
  }
  if (*string == ',')
  {
   break;
  }
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = (fl * teiler) + ziffer;
  string ++;
 }

 if (*string == '.');
 else if (*string == ',');
 else
 {
  fl *= minus;
  return (fl);
 }
 string ++;
 while (*string)
 {
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  nk = (double) ziffer / teiler;
  fl += nk;
  teiler *= 10;
  string ++;
 }
// fl += nk;
 return (fl *= minus);
}

double CStrFuncs::StrToDouble (CString& Text)
{
	return StrToDouble (Text.GetBuffer (0));
}

/*
void CStrFuncs::ToClipboard (CString& Text)
{
	CEdit clip;
	clip.Create (WS_CHILD, CRect (0, 0, 80, 10), NULL, 10001);
	clip.SetWindowText (Text.GetBuffer ());
	clip.SetSel (0, -1);
	clip.Copy ();
	clip.DestroyWindow ();
}
*/

void CStrFuncs::ToClipboard (CString& Text)
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard(NULL))
    {
//      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
//      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	try
	{
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Text.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Text.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}


void CStrFuncs::FromClipboard (CString& Text)
{
/*
	CEdit clip;
	clip.Create (WS_CHILD, CRect (0, 0, 80, 10), NULL, 10001);
	clip.Paste ();
	clip.GetWindowText (Text);
	clip.DestroyWindow ();
*/
}

BOOL CStrFuncs::CanPaste ()
{
/*
	CString Text;
	CEdit clip;
	clip.Create (WS_CHILD, CRect (0, 0, 80, 10), NULL, 10001);
	clip.Paste ();
	clip.GetWindowText (Text);
	clip.DestroyWindow ();
	Text.Trim ();
	if (Text == _T(""))
	{
		return FALSE;
	}
*/
	return TRUE;
}


void CStrFuncs::Trim (LPSTR pos)
{
    LPSTR p = &pos[strlen (pos) - 1];
	for (; *p <= ' ' && p >= pos; p -= 1) *p = 0;
}

/*
int CStrFuncs::findstring0 (unsigned char *umltab, unsigned char *uwert, int *pos)
{
int i, j;
int len;
unsigned int zn;
unsigned int high;
unsigned int low;
unsigned int hundert;


for(umltab; *umltab <= ' '; umltab ++) if(*umltab == 0) break;
if(*umltab == 0) return(0);

if(memcmp (umltab, "0X",2) == 0 ||
   memcmp (umltab, "0x",2) == 0)
  {
  clipped ((char *) umltab);
  len = strlen ((char *) umltab);
  for(i = 2, j = 0; i < len; i += 4, j ++)
    {
    high = umltab[i];
    low  = umltab[i + 1];
    if(high > 0x39)
      {
      high -= (0x41 - 0xA);
      }
    else if(high >= 0x30)
      {
      high -= 0x30;
      }
    if(low > 0x39)
      {
      low -= (0x41 - 0xA);
      }
    else if(low >= 0x30)
      {
      low -= 0x30;
      }
    zn = (unsigned int) high * 16 + low;
    if(zn != uwert[j]) return(0);
    if(uwert[j] == 0) return(0);
    }
  if(i < len) return(0);
  *pos += j;
  }
else if(memcmp (umltab, "0D",2) == 0 ||
        memcmp (umltab, "0d",2) == 0)	
{
  clipped ((char *) umltab);
  len = strlen ((char *) umltab);
  for(i = 2, j = 0; i < len; i += 5, j ++)
    {
    hundert = umltab[i] - 0x30;
    high    = umltab[i + 1] - 0x30;
    low     = umltab[i + 2] - 0x30;
    zn = (unsigned int) hundert * 100 + high * 10 + low;
    if(zn != uwert[j]) return(0);
    if(uwert[j] == 0) return(0);
    }
  if(i != len) return(0);
  *pos += j;
  }
else
  {
  len = strlen ((char *) umltab);
  if(memcmp ((char *) umltab, uwert, len)) return(0);
  *pos += len;
  }
return(1);
}



int CStrFuncs::findstring (unsigned char *umltab, unsigned char *uwert, int *pos)
{
int i, anz;
int ps;

anz = split ((char *) umltab);
for(i = 1; i <= anz; i ++)
  {
  ps = findstring0 ((unsigned char *) wort[i], uwert, pos);
  if(ps) return(i);
  }
return(0);
}


int CStrFuncs::setstring (unsigned char * adr, unsigned char *umltab0, int *pos, int upos)
{
int i, j, anz;
int ulen;
char *p;
char *umltab;
unsigned int zn;
unsigned int high;
unsigned int low;
unsigned int hundert;


anz = split ((char *) umltab0);
if (anz == 0)
{
	umltab = umltab0;
}
else
{
	if (upos > anz) upos = anz;
	umltab = wort[upos]; 
}

i = *pos;
p = (char *) &adr[i];


ulen = strlen (umltab);
if(memcmp ((char *) umltab, "0X",2) == 0 ||
   memcmp ((char *) umltab, "0x",2) == 0)
  {
  clipped (umltab);
  ulen = strlen (umltab);
  for(i = 2, j = 0; i < ulen; i += 4, j ++)
    {
    high = umltab[i];
    low  = umltab[i + 1];
    if(high > 0x39)
      {
      high -= (0x41 - 0xA);
      }
    else if(high >= 0x30)
      {
      high -= 0x30;
      }
    if(low > 0x39)
      {
      low -= (0x41 - 0xA);
      }
    else if(low >= 0x30)
      {
      low -= 0x30;
      }
    zn = (unsigned int) high * 16 + low;
    p[j] = zn;
    }
  *pos += j;
  }
else if(memcmp (umltab, "0D",2) == 0 ||
		memcmp (umltab, "0d",2) == 0)
  {
  clipped (umltab);
  ulen = strlen (umltab);
  for(i = 2, j = 0; i < ulen; i += 5, j ++)
    {
    hundert = umltab[i] - 0x30;
    high    = umltab[i + 1] - 0x30;
    low     = umltab[i + 2] - 0x30;
    zn = (unsigned int) hundert * 100 + high * 10 + low;
    p[j] = zn;
    }
  *pos += j;
  }
else
  {
  memcpy ((char *) p, (char *) umltab, ulen);
  *pos += ulen;
  }
return(1);
}
*/

void CStrFuncs::FillString (LPTSTR m_Source, LPTSTR m_Target)
{
	int pos = 0;
	LPTSTR Source = NULL;
	int len = (int) _tcslen (m_Source);
	Source = new TCHAR [len + 1];
	int i = 0, j = 0;
	for (i = 0, j = 0; i < len; j++)
	{
		if ((memcmp (&m_Source[i], _T("0X"), 2 * sizeof (TCHAR)) == 0)||
			(memcmp (&m_Source[i], _T("0x"), 2 * sizeof (TCHAR)) == 0))		
		{
			unsigned int high   = m_Source[i + 2];
			unsigned int low    = m_Source[i + 3];
		    if(high > 0x39)
			{
				high -= (0x41 - 0xA);
			}
			else if(high >= 0x30)
			{
				high -= 0x30;
			}
			if(low > 0x39)
			{
				low -= (0x41 - 0xA);
			}
			else if(low >= 0x30)
			{
				low -= 0x30;
			}
			unsigned int zn = high * 16 + low;
			Source[j] = (TCHAR) zn;
			i += 4;
		}
		else if ((memcmp (&m_Source[i], _T("0D"), 2 * sizeof (TCHAR)) == 0)||
				 (memcmp (&m_Source[i], _T("0d"), 2 * sizeof (TCHAR)) == 0))		
		{
			unsigned int hundert = m_Source[i + 2] - 0x30;
			unsigned int high    = m_Source[i + 3] - 0x30;
			unsigned int low     = m_Source[i + 4] - 0x30;
			unsigned int zn = hundert * 100 + high * 10 + low;
			Source[j] = (TCHAR) zn;
			i += 5;
		}
		else
		{
			Source[j] = m_Source[i];
			i ++;
		}
	}
	Source[j] = _T('\0');
	_tcscpy (m_Target, Source);
	delete Source;
}


void CStrFuncs::ChangePart (LPTSTR String, LPTSTR m_Source, LPTSTR m_Target, int len)
{
	LPTSTR NewString = new TCHAR [len];
	if (NewString == NULL) return;

	int tlen = (int) _tcslen (m_Target);
	CToken t;
	t.SetSep (Separator);
	t = m_Source;
	int tcount = t.GetAnzToken ();
	int i = 0, j = 0;
	for (i = 0, j = 0; j < len;)
	{
		if (i >= (int) _tcslen (String)) break;
		BOOL Changed = FALSE;
		for (int p = 0; p < tcount; p ++)
		{
			LPTSTR s = t.GetToken (p);
			FillString (s, s);
			if (String[i] == s[0])
			{
				if (memcmp (&String[i], s, _tcslen (s) * sizeof (TCHAR)) == 0)
				{
					if (tlen > 0)
					{
						memcpy (&NewString[j], m_Target, tlen * sizeof (TCHAR));
						j += tlen;
					}
					i += (int) _tcslen (s);
					Changed = TRUE;
					break;
				}
			}
		}
		if (!Changed)
		{
			NewString[j] = String[i];
			j ++;
			i ++;
		}
		if (j >= len) break;
	}
	NewString[j] = _T('\0');
	_tcscpy (String, NewString);
	delete NewString;
}

void CStrFuncs::ChangeString (LPTSTR String, LPTSTR m_Source, LPTSTR m_Target, int len)
{
	len /= sizeof (TCHAR);
	LPTSTR Source = new TCHAR [_tcslen (m_Source) + 1];
	LPTSTR Target = new TCHAR [_tcslen (m_Target) + 1];

	_tcscpy (Source, m_Source);
	FillString (m_Target, Target);

    ChangePart (String, Source, Target, len);
	
	if (Source != NULL) delete Source;
	if (Target != NULL) delete Target;
}

void CStrFuncs::FromClipboardMByte (CString& Text)
{
    if (!OpenClipboard(NULL))
    {
//      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return;
    }

    HGLOBAL hglbCopy;
    LPSTR data;

	try
	{
		 hglbCopy =  ::GetClipboardData(CF_TEXT);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
		 data = (LPSTR) GlobalLock ((HGLOBAL) hglbCopy);
         Text = data;
		 GlobalUnlock ((HGLOBAL) hglbCopy);
	}
	catch (...) {};
    CloseClipboard();
}

void CStrFuncs::FromClipboardUni (CString& Text)
{
    if (!OpenClipboard(NULL))
    {
//      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return;
    }

    HGLOBAL hglbCopy;
    LPTSTR data;

	try
	{
		 hglbCopy =  ::GetClipboardData(CF_UNICODETEXT);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
		 data = (LPTSTR) GlobalLock ((HGLOBAL) hglbCopy);
         Text = data;
		 GlobalUnlock ((HGLOBAL) hglbCopy);
	}
	catch (...) {};
    CloseClipboard();
}

void CStrFuncs::FromClipboard (CString& Text, UINT format)
{
	if (format == CF_UNICODETEXT)
	{
		FromClipboardUni (Text); 
	}
	else if (format == CF_TEXT)
	{
		FromClipboardMByte (Text); 
	}
}

void CStrFuncs::ToClipboardMByte (CString& Text)
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard(NULL) )
    {
//      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
//      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	try
	{
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Text.GetLength () + 1) * 2 ); 
        LPSTR p = (LPSTR) GlobalLock(hglbCopy);
#ifdef UNICODE
		wchar_t *wp = Text.GetBuffer ();
	    MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, p, -1, wp, 0);
#else
        strcpy (p, Text.GetBuffer ());
#endif
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

void CStrFuncs::ToClipboardUni (CString& Text)
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard(NULL) )
    {
//      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
//      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	try
	{
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Text.GetLength () + 1) * 2 ); 
        wchar_t * p = (wchar_t *) GlobalLock(hglbCopy);
#ifdef UNICODE
        wcscpy (p, Text.GetBuffer ());
#else
		WideCharToMultiByte(CP_ACP, WC_COMPOSITECHECK , p, 
							-1,  Text.GetBuffer (),  Text.GetLength (),  NULL,  NULL);
#endif
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_UNICODETEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

void CStrFuncs::ToClipboard (CString& Text, UINT format)
{
	if (format == CF_UNICODETEXT)
	{
		ToClipboardUni (Text); 
	}
	else if (format == CF_TEXT)
	{
		ToClipboardMByte (Text); 
	}
}

void CStrFuncs::SysDate (CString& Date)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 Date.Format (_T("%02d.%02d.%04d"), ltime->tm_mday,
                                    ltime->tm_mon + 1,
                                    ltime->tm_year + 1900);
}

void CStrFuncs::SysTime (CString& Time)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 Time.Format (_T("%02d:%02d:%02d"), ltime->tm_hour,
									ltime->tm_min,
									ltime->tm_sec);
}

BOOL CStrFuncs::IsDecimal (CString& Text)
{
	Text.Trim ();
	TCHAR *p = Text.GetBuffer ();
	for (; *p != 0; p += 1)
	{
		if (*p == ' ') break;
		if (*p > (UCHAR) 64) return FALSE;
	}
	return TRUE;
}

void CStrFuncs::PointToKomma (CString& Value)
{
		int pos = Value.Find ('.');
		if (pos != -1)
		{
			Value.GetBuffer ()[pos] = ',';
		}
}

void CStrFuncs::DoubleToStr (CString& Value, double d, LPTSTR format)
{
	Value.Format (format, d);
	PointToKomma (Value);
}

