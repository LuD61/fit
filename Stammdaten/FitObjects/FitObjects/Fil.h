#ifndef _FIL_DEF
#define _FIL_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct FIL {
   TCHAR          abr_period[2];
   long           adr;
   long           adr_lief;
   short          afl;
   TCHAR          auf_typ[2];
   TCHAR          best_kz[2];
   TCHAR          bli_kz[2];
   DATE_STRUCT    dat_ero;
   short          daten_mnp;
   short          delstatus;
   short          fil;
   TCHAR          fil_kla[2];
   short          fil_gr;
   double         fl_lad;
   double         fl_nto;
   double         fl_vk_ges;
   short          frm;
   DATE_STRUCT    iakv;
   TCHAR          inv_rht[2];
   long           kun;
   TCHAR          lief[17];
   TCHAR          lief_rht[2];
   long           lief_s;
   TCHAR          ls_abgr[2];
   TCHAR          ls_kz[2];
   short          ls_sum;
   short          mdn;
   TCHAR          pers[13];
   short          pers_anz;
   TCHAR          pos_kum[2];
   TCHAR          pr_ausw[2];
   TCHAR          pr_bel_entl[2];
   TCHAR          pr_fil_kz[2];
   long           pr_lst;
   TCHAR          pr_vk_kz[2];
   double         reg_bed_theke_lng;
   double         reg_kt_lng;
   double         reg_kue_lng;
   double         reg_lng;
   double         reg_tks_lng;
   double         reg_tkt_lng;
   TCHAR          ret_entl[2];
   TCHAR          smt_kz[2];
   short          sonst_einh;
   short          sprache;
   TCHAR          sw_kz[2];
   long           tou;
   TCHAR          umlgr[2];
   TCHAR          verk_st_kz[2];
   short          vrs_typ;
   TCHAR          inv_akv[2];
   double         planumsatz;
};
extern struct FIL fil, fil_null;

#line 8 "Fil.rh"

class FIL_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               FIL fil;  
               FIL_CLASS () : DB_CLASS ()
               {
               }
};
#endif
