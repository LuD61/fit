========================================================================
    AKTIVE VORLAGENBIBLIOTHEK: FitObjects-Projekt�bersicht
========================================================================

Der Anwendungs-Assistent hat dieses FitObjects-Projekt als Ausgangspunkt
zum Schreiben der ausf�hrbaren Datei (EXE) erstellt.


Die Datei enth�lt eine Zusammenfassung des Inhalts der Dateien f�r
das Projekt.

FitObjects.vcproj
    Dies ist die Hauptprojektdatei f�r VC++-Projekte, die mit dem Anwendungs-Assistenten generiert werden. 
    Sie enth�lt Informationen zu der Version von Visual�C++, mit der die Datei generiert wurde, 
    sowie Informationen zu Plattformen, Konfigurationen und Projektfeatures, die mit dem
    Anwendungs-Assistenten ausgew�hlt wurden.

FitObjects.idl
    Diese Datei enth�lt die IDL-Definitionen der Typbibliothek, die Schnittstellen
    und Co-Klassen, die im Projekt definiert sind.
    Diese Datei wird vom MIDL-Compiler verarbeitet, um Folgendes zu generieren:
        C++-Schnittstellendefinitionen und GUID-Deklarationen (FitObjects.h)
        GUID-Definitionen                                (FitObjects_i.c)
        Eine Typbibliothek                                   (FitObjects.tlb)
        Marshallingcode                                 (FitObjects_p.c and dlldata.c)

FitObjects.h
    Diese Datei enth�lt die C++-Schnittstellendefinitionen und GUID-Deklarationen der
    in .idl definierten Elemente. Sie wird von MIDL w�hrend der Kompilierung erneut generiert.

FitObjects.cpp
    Diese Datei enth�lt die Objekttabelle und die Implementierung von WinMain.

FitObjects.rc
    Hierbei handelt es sich um eine Auflistung aller Ressourcen von Microsoft�Windows, die
    vom Programm verwendet werden.


/////////////////////////////////////////////////////////////////////////////
Weitere Standarddateien:

StdAfx.h, StdAfx.cpp
    Mit diesen Dateien werden vorkompilierte Headerdateien (PCH)
    mit der Bezeichnung FitObjects.pch und eine vorkompilierte Typdatei mit der Bezeichnung StdAfx.obj erstellt.

Resource.h
    Dies ist die Standardheaderdatei, die neue Ressourcen-IDs definiert.

/////////////////////////////////////////////////////////////////////////////
Proxy/Stub-DLL-Projekt und Moduldefinitionsdatei:

FitObjectsps.vcproj
    Dies ist die Projektdatei zum Erstellen einer Proxy/Stub-DLL.
	Die IDL-Datei im Hauptprojekt muss mindestens eine Schnittstelle enthalten. Die IDL-Datei muss 
	vor dem Erstellen der Proxy/Stub-DLL kompiliert werden.	In diesem Prozess werden
	dlldata.c, FitObjects_i.c und FitObjects_p.c generiert, die erforderlich sind, um
	die Proxy/Stub-DLL zu generieren.

FitObjectsps.def
    Die Moduldefinitionsdatei enth�lt die Linkerinformationen zu den Exporten, 
    die f�r den Proxy/Stub erforderlich sind.

/////////////////////////////////////////////////////////////////////////////