#include "StdAfx.h"
#include "ClientKey.h"

CClientKey::CClientKey(void)
{
	key = 0;
	ClientOrder = NULL;
}

CClientKey::CClientKey(int key, CClientOrder *ClientOrder)
{
	this->key = key;
	this->ClientOrder = ClientOrder;
}

CClientKey::~CClientKey(void)
{
	if (ClientOrder != NULL)
	{
		delete ClientOrder;
	}
}
