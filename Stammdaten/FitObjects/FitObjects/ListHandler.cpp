// ListHandler.cpp: Implementierung der Klasse CListHandler.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include <string>
#include "StrFuncs.h"
#include "ListHandler.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////


int CListHandler::CIdxCollection::GetIdxAll (int IdxValue)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxValue == IdxValue)
		{
			return Item->IdxAll;
		}
	}
	return -1;
}

int CListHandler::CIdxCollection::GetIdxValue (int IdxAll)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxAll == IdxAll)
		{
			return Item->IdxValue;
		}
	}
	return -1;
}


CListHandler::CIdxItem *CListHandler::CIdxCollection::GetItemByAll (int IdxAll)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxAll == IdxAll)
		{
			return Item;
		}
	}
	return NULL;
}

CListHandler::CIdxItem *CListHandler::CIdxCollection::GetItemByValue (int IdxValue)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxValue == IdxValue)
		{
			return Item;
		}
	}
	return NULL;
}


CListHandler *CListHandler::instance = NULL;

CListHandler *CListHandler::GetInstance ()
{
	if (instance == NULL)
	{
		instance = new CListHandler ();
	}
	return instance;
}

void CListHandler::DestroyInstance ()
{
	if (instance != NULL)
	{
		delete instance;
	}
}

CListHandler::CListHandler()
{
	ActiveMode = AllEntries;
	LoadComplete = FALSE;
	enableActivate = FALSE;
	activated = FALSE;
}

CListHandler::~CListHandler()
{
	Init ();
}

void CListHandler::Init ()
{
	StndAll.DestroyElements ();
	StndAll.Destroy ();
	StndValue.DestroyElements ();
	StndValue.Destroy ();
	IdxCollection.DestroyElements ();
	IdxCollection.Destroy ();
	LoadComplete = FALSE;
	enableActivate = FALSE;
	activated = FALSE;
}

void CListHandler::InitAufMe ()
{
	AUFPS **it;
	AUFPS *Item;

	StndAll.Start ();
	while ((it = StndAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			strcpy (Item->auf_me, "0.000");
		}
	}
}


int  CListHandler::Activate (AUFPS aufparr[MAXPOS], int aufpanz, int *idx)
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int i = 0;
	ListCount = StndAll.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	for (i = 0; i < aufpanz; i ++) 
	{
		SetStndAllA (&aufparr[i]);
	}
	ListCount = StndAll.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = StndAll.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
		}
	}
    activated = TRUE;
	return ListCount;
}

void CListHandler::SetStndAllA (AUFPS *Aufps)
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;

	StndAll.Start ();
	while ((it = StndAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (CStrFuncs::StrToDouble (Item->a) == CStrFuncs::StrToDouble (Aufps->a))
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				SetStndValueA (Aufps, posi, idx);
				break;
			}
		}
		idx ++;
	}
}

void CListHandler::SetStndValueA (AUFPS *Aufps, int posi, int idxAll)
{
	AUFPS **it;
	AUFPS *Item;
	int idxValue;

	BOOL found = FALSE;
	if (CStrFuncs::StrToDouble (Aufps->lief_me) != 0.0)
	{
		StndValue.Start ();
		while ((it = StndValue.GetNext ()) != NULL)
		{
			Item = *it;
			if (Item != NULL)
			{
				if (CStrFuncs::StrToDouble (Item->a) == CStrFuncs::StrToDouble (Aufps->a))
				{
					memcpy (Item, Aufps, sizeof (AUFPS));
					sprintf (Item->posi, "%d", posi);
					found = TRUE;
					break;
				}
			}
		}
		if (!found)
		{
			Item = new AUFPS;
			memcpy (Item, Aufps, sizeof (AUFPS));
			sprintf (Item->posi, "%d", posi);
			idxValue = StndValue.anz;
			StndValue.Add (Item);
			CIdxItem *item = new CIdxItem (idxAll, idxValue);
			IdxCollection.Add (item);
		}
	}
}

int CListHandler::GetAllPosiA (AUFPS *Aufps)
{
	AUFPS **it;
	AUFPS *Item;
	int posi;

	StndAll.Start ();
	while ((it = StndAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (CStrFuncs::StrToDouble (Item->a) == CStrFuncs::StrToDouble (Aufps->a))
			{
				posi = atoi (Item->posi);
				return posi;
			}
		}
	}
	return 10;
}

void CListHandler::Update (int idx, AUFPS *aufps)
{
	AUFPS **it;
	AUFPS *Aufps;
	int idxAll;
	int idxValue;
 
	if (!activated)
	{
		return;
	}
	if (ActiveMode == AllEntries)
	{
		idxAll = idx;
		it = StndAll.Get (idxAll);
		if (it != NULL)
		{
			Aufps = *it;
			memcpy (Aufps, aufps, sizeof (AUFPS));
		}
		it = NULL;
		idxValue = IdxCollection.GetIdxValue (idxAll);
		if (idxValue != -1)
		{
			it = StndValue.Get (idxValue);
		}
		if (it != NULL)
		{
			Aufps = *it;
            if (CStrFuncs::StrToDouble (aufps->auf_me) != 0.0)
			{
				memcpy (Aufps, aufps, sizeof (AUFPS));
			}
			else
			{
				StndValue.Drop (idxValue);
				delete Aufps;
				CIdxItem *item = IdxCollection.GetItemByAll (idxAll);
				if (item != NULL)
				{
					IdxCollection.Drop (item);
					delete item;
				}
			}
		}
		else
		{
            if (CStrFuncs::StrToDouble (aufps->auf_me) != 0.0)
			{
				Aufps = new AUFPS;
				memcpy (Aufps, aufps, sizeof (AUFPS));
				idxValue = StndValue.anz;
				StndValue.Add (Aufps);
				CIdxItem *item = new CIdxItem (idxAll, idxValue);
				IdxCollection.Add (item);
			}

		}
	}
	else
	{
		idxValue = idx;
		it = StndValue.Get (idxValue);
		if (it != NULL)
		{
			Aufps = *it;
			memcpy (Aufps, aufps, sizeof (AUFPS));
		}
		it = NULL;
		idxAll = IdxCollection.GetIdxAll (idxValue);
		if (idxAll != -1)
		{
			it = StndAll.Get (idxAll);
		}
		if (it != NULL)
		{
			Aufps = *it;
			memcpy (Aufps, aufps, sizeof (AUFPS));
		}
	}
} 

int CListHandler::Switch (AUFPS aufparr[MAXPOS], int *idx)
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	if (ActiveMode == AllEntries)
	{
		ListCount = StndValue.anz;
		if (ListCount == 0)
		{
			return StndAll.anz;
		}
		for (int i = 0; i < ListCount; i ++) 
		{
			it = StndValue.Get (i);
			if (it != NULL)
			{
				Aufps = *it;
				memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
			}
		}
		*idx = IdxCollection.GetIdxValue (*idx);
		ActiveMode = ValueEntries;
	}
	else
	{
		ListCount = StndAll.anz;
		for (int i = 0; i < ListCount; i ++) 
		{
			it = StndAll.Get (i);
			if (it != NULL)
			{
				Aufps = *it;
				memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
			}
		}
		*idx = IdxCollection.GetIdxAll (*idx);
		ActiveMode = AllEntries;
	}
	return ListCount;
}

double CListHandler::GetSmtPart ()
{
	double part = 0.0;
	if (StndAll.anz > 0.0 && StndValue.anz > 0.0)
	{
		part = StndValue.anz * 100 / StndAll.anz;
	}
	return part;
}