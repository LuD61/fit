#include "StdAfx.h"
#include "Datatables.h"

#ifdef _DEBUG
//#define new DEBUG_NEW
#endif


CDatatables *CDatatables::Instance = NULL;

CDatatables::CDatatables(void)
{
	Mdn.opendbase (_T("bws"));
}

CDatatables *CDatatables::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CDatatables ();
	}
	return Instance;
}

void CDatatables ::DeleteInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
	}
}
