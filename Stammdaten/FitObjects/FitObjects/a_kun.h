#ifndef _A_KUN_DEF
#define _A_KUN_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_KUN {
   short          mdn;
   short          fil;
   long           kun;
   double         a;
   TCHAR          a_kun[13];
   TCHAR          a_bz1[25];
   short          me_einh_kun;
   double         inh;
   TCHAR          kun_bran2[3];
   double         tara;
   double         ean;
   TCHAR          a_bz2[25];
   short          hbk_ztr;
   long           kopf_text;
   TCHAR          pr_rech_kz[2];
   TCHAR          modif[2];
   short          pos_fill;
   short          ausz_art;
   long           text_nr2;
   short          cab;
   TCHAR          a_bz3[25];
   TCHAR          a_bz4[25];
   long           text_nr;
   short          devise;
   TCHAR          geb_eti[2];
   TCHAR          geb_fill[2];
   long           geb_anz;
   TCHAR          pal_eti[2];
   TCHAR          pal_fill[2];
   short          pal_anz;
   TCHAR          pos_eti[2];
   short          sg1;
   short          sg2;
   TCHAR          li_a[13];
   double         geb_fakt;
   double         ean_vk;
};
extern struct A_KUN a_kun, a_kun_null;

#line 8 "a_kun.rh"

class A_KUN_CLASS : public DB_CLASS 
{
       private :
               int cursor_kun;
               int cursor_bra;
               void prepare (void);
       public :
               A_KUN a_kun;  
               A_KUN_CLASS () : DB_CLASS ()
               {
		    cursor_kun = -1;	
		    cursor_bra = -1;	
               }
               int dbreadfirst_kun ();
               int dbread_kun ();
               int dbreadfirst_bra ();
               int dbread_bra ();
};
#endif
