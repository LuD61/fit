// Debug.cpp: Implementierung der Klasse CDebug.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Debug.h"
#include "StrFuncs.h"

#define SYSDATE CStrFuncs::SysDate (Date);
#define SYSTIME CStrFuncs::SysTime (Time);

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CDebug *CDebug::Instance = NULL;

CDebug::CDebug()
{
	fp = NULL;
	strcpy (Name, "C:\\temp\\51100.dbg");
}

CDebug::~CDebug()
{
	if (fp != NULL)
	{
		Close ();
	}

}

CDebug *CDebug::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CDebug ();
	}
	return Instance;
}


void CDebug::Open ()
{
	fp = fopen (Name, "w");
}

void CDebug::Write ()
{
	if (fp == NULL)
	{
		Open ();
	}
	if (fp != NULL)
	{
		SYSDATE
		SYSTIME
		fprintf (fp, "%s %s | %s\n", Date.GetBuffer (), Time.GetBuffer (), Line);
		fflush (fp);
	}
}

void CDebug::Close ()
{
	if (fp != NULL)
	{
		fclose (fp);
		fp = NULL;
	}
}
