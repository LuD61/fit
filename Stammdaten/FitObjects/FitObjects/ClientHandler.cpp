#include "StdAfx.h"
#include "ClientHandler.h"

CClientHandler *CClientHandler::instance = NULL;

CClientHandler *CClientHandler::GetInstance ()
{
	if (instance == NULL)
	{
		instance = new CClientHandler ();
	}
	return instance;
}

void CClientHandler::DestroyInstance ()
{
	if (instance != NULL)
	{
		delete instance;
	}
}

CClientHandler::CClientHandler(void)
{
	IsRunning = FALSE;
	client = 1;
	SortMode = PosA;
	RdOptimize = No;
}

CClientHandler::~CClientHandler(void)
{
	ClientKeys.DestroyElements ();
	ClientKeys.Destroy ();
}

BOOL CClientHandler::Lock ()
{
	if (IsRunning)
	{
		return FALSE;
	}
	IsRunning = TRUE;
	return TRUE;
}

void CClientHandler::Unlock ()
{
	IsRunning = FALSE;
}

int CClientHandler::GetFreeClient ()
{
	BOOL exist = TRUE;
	CClientKey **it;
	CClientKey *ClientKey;
	client = 1;
	while (exist)
	{
		exist = FALSE;
		ClientKeys.Start ();
		while ((it = ClientKeys.GetNext ()) != NULL)
		{
			ClientKey = *it;
			if (ClientKey != NULL)
			{
				if (ClientKey->GetKey () == client)
				{
					exist = TRUE;
					client ++;
					break;
				}
			}
		}
	}
	return client;
}

int CClientHandler::Add (CClientOrder *ClientOrder)
{
	int ret = client;
	CClientKey *ClientKey = new CClientKey (client, ClientOrder);
	ClientKeys.Add (ClientKey);
	client = GetFreeClient ();
	return ret;
}

void CClientHandler::Drop (int client)
{
	CClientKey **it;
	CClientKey *ClientKey;
	int i = 0;
	ClientKeys.Start ();
	while ((it = ClientKeys.GetNext ()) != NULL)
	{
		ClientKey = *it;
		if (ClientKey != NULL)
		{
			if (ClientKey->GetKey () == client)
			{
				delete ClientKey;
				ClientKeys.Drop (i);
				break;
			}
		}
		i ++;
	}
}

CClientOrder *CClientHandler::GetClient (int client)
{
	CClientKey **it;
	CClientKey *ClientKey;
	ClientKeys.Start ();
	while ((it = ClientKeys.GetNext ()) != NULL)
	{
		ClientKey = *it;
		if (ClientKey != NULL)
		{
			if (ClientKey->GetKey () == client)
			{
				return ClientKey->GetClientOrder ();
			}
		}
	}
	return NULL;
}
