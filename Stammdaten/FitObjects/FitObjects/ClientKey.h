#pragma once
#include "clientorder.h"

class CClientKey
{
private:
	int key;
	CClientOrder *ClientOrder;
public:
	void SetKey (int key)
	{
		this->key = key;
	}

	int GetKey ()
	{
		return key;
	}
	void SetClientOrder (CClientOrder *ClientOrder)
	{
		this->ClientOrder = ClientOrder;
	}
	CClientOrder *GetClientOrder ()
	{
		return ClientOrder;
	}
	CClientKey(void);
	CClientKey(int key, CClientOrder *ClientOrder);
	~CClientKey(void);
};
