#include "StdAfx.h"
#include "FitObjectsDlg.h"
#include "resource.h"

using namespace std;

WORD SW_MODE = SW_SHOWNORMAL;

class CAboutDialog
{
public:
	int DoModal ();
    static INT_PTR CALLBACK DlgProc (HWND hwndDlg, UINT message, 
				     WPARAM wParam, LPARAM lParam); 
};

int CAboutDialog::DoModal ()
{
	HINSTANCE hInstance = (HINSTANCE) 0x00400000;
	DialogBox (hInstance, MAKEINTRESOURCE (IDD_ABOUT), NULL, DlgProc);
	return TRUE;
}

INT_PTR CALLBACK CAboutDialog::DlgProc (HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{ 

    switch (message) 
    {
        case WM_INITDIALOG:
			SetWindowText (hwndDlg, _T("Info �ber FitObjects"));
			break;
			return TRUE;
        case WM_COMMAND: 
            switch (LOWORD(wParam)) 
			{
			case IDOK :
                EndDialog(hwndDlg, wParam); 
                return TRUE; 
			}
			break;
        case WM_SYSCOMMAND: 
            switch (wParam)
			{
			case SC_CLOSE:
                PostQuitMessage (0); 
                return TRUE; 
			}
			break;
	}
	return FALSE;
}


#define FitDlg ((CFitObjectsDlg *)CCFitObjectsDlg::dlg)

CFitObjectsDlg *CFitObjectsDlg::instance = NULL;

CFitObjectsDlg *CFitObjectsDlg::GetInstance ()
{
	if (instance == NULL)
	{
		instance = new CFitObjectsDlg ();
	}
	return instance;
}

void CFitObjectsDlg::DestroyInstance ()
{
	if (instance != NULL)
	{
		delete instance;
	}
}

CFitObjectsDlg::CFitObjectsDlg ()
{
	m_hWnd = NULL;
	m_Text = "";
	Lines = 0;
	visible = FALSE;
}


CFitObjectsDlg::~CFitObjectsDlg(void)
{
}

BOOL CFitObjectsDlg::Create ()
{
	if (m_hWnd != NULL)
	{
		return TRUE;
	}
	HINSTANCE hInstance = (HINSTANCE) 0x00400000;
	m_hWnd = CreateDialog(hInstance,
						     MAKEINTRESOURCE (IDD_FORMVIEW),
							 NULL,
							 &DlgProc);
	if (m_hWnd == NULL)
	{
		return FALSE;
	}
	SetWindowPos (m_hWnd, HWND_TOPMOST, 0, 0, 0, 0,SWP_NOMOVE | SWP_NOSIZE); 
 	return TRUE;
}

void CFitObjectsDlg::Show (BOOL b)
{
	if (b)
	{
		ShowWindow (m_hWnd, SW_SHOWNORMAL);
		visible = TRUE;
	}
	else
	{
		ShowWindow (m_hWnd, SW_HIDE);
		visible = FALSE;
	}
	UpdateWindow (m_hWnd);
}

void CFitObjectsDlg::SetDlgPos (HWND hWnd)
{
	int scx = GetSystemMetrics (SM_CXFULLSCREEN);
	int scy = GetSystemMetrics (SM_CYFULLSCREEN);
	RECT rect;
	GetWindowRect (hWnd, &rect);
	int cx = rect.right - rect.left;
	int cy = rect.bottom - rect.top;
	rect.right  = scx;
	rect.bottom = scy;
	int x  = scx  - cx - 100;
	int y  = scy - cy;
	MoveWindow (hWnd, x, y, cx, cy, FALSE);
}

void CFitObjectsDlg::MoveControl (HWND hWnd, int cx, int cy)
{
	HWND m_Edit = GetDlgItem (hWnd, IDC_EDIT);
	if (m_Edit == NULL)
	{
		return;
	}

	RECT rect;
	GetWindowRect (m_Edit, &rect);
	MoveWindow (m_Edit, 10, 10, cx - 20, cy - 20, TRUE);
}

void CFitObjectsDlg::AddLine (CString& Line)
{
	HWND m_Edit = GetDlgItem (m_hWnd, IDC_EDIT);
	if (m_Text.GetLength () > 0)
	{
		m_Text += "\r\n";
	}
	m_Text += Line;
	Lines ++;
	SetWindowText (m_Edit, m_Text.GetBuffer ());
	int pos = m_Text.GetLength ();
	SendMessage (m_Edit, EM_LINESCROLL, 0, Lines);
	SendMessage (m_Edit, EM_SETSEL, -1, 0l);
}

INT_PTR CALLBACK CFitObjectsDlg::DlgProc (HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{ 
	int cx;
	int cy;

    switch (message) 
    {
        case WM_INITDIALOG:
			SetWindowText (hwndDlg, _T("FitProjects"));
			instance->SetDlgPos (hwndDlg);
//			PostMessage (hwndDlg, WM_USER, 0, 0l);
			break;
		case WM_SIZE:
			cx = LOWORD (lParam);;
			cy = HIWORD (lParam);
			instance->MoveControl (hwndDlg, cx, cy);
            break;
		case WM_SETFOCUS:
			return TRUE;
        case WM_USER: 
            if (instance != NULL)
			{
			}
			return TRUE;
        case WM_COMMAND: 
            switch (LOWORD(wParam)) 
			{
			case IDOK :
                EndDialog(hwndDlg, wParam); 
                return TRUE; 
			}
			break;
        case WM_SYSCOMMAND: 
            switch (wParam)
			{
			case SC_CLOSE:
                PostQuitMessage (0); 
                return TRUE; 
			}
			break;
	}
	return FALSE;
}
