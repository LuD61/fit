// CAufps.h: Deklaration von CCAufps

#pragma once
#include <comutil.h>
#include "resource.h"       // Hauptsymbole
#include "ComObject.h"



#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Singlethread-COM-Objekte werden auf der Windows CE-Plattform nicht vollständig unterstützt. Windows Mobile-Plattformen bieten beispielsweise keine vollständige DCOM-Unterstützung. Definieren Sie _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA, um ATL zu zwingen, die Erstellung von Singlethread-COM-Objekten zu unterstützen und die Verwendung eigener Singlethread-COM-Objektimplementierungen zu erlauben. Das Threadmodell in der RGS-Datei wurde auf 'Free' festgelegt, da dies das einzige Threadmodell ist, das auf Windows CE-Plattformen ohne DCOM unterstützt wird."
#endif


// ICAufps
[
	object,
	uuid("6DFBDF7C-65A0-442E-A9DA-BA9CA23CA613"),
	dual,	helpstring("ICAufps-Schnittstelle"),
	pointer_default(unique)
]
__interface ICAufps : IDispatch
{
	[propget, id(1), helpstring("Eigenschaft A")] HRESULT A([out, retval] DOUBLE* pVal);
	[propget, id(2), helpstring("Eigenschaft ABz1")] HRESULT ABz1([out, retval] BSTR* pVal);
	[propget, id(3), helpstring("Eigenschaft Posi")] HRESULT Posi([out, retval] LONG* pVal);
	[propget, id(4), helpstring("Eigenschaft AufVkPr")] HRESULT AufVkPr([out, retval] DOUBLE* pVal);
	[propget, id(5), helpstring("Eigenschaft AufLadPr")] HRESULT AufLadPr([out, retval] DOUBLE* pVal);
	[propget, id(6), helpstring("Eigenschaft AufLadPr0")] HRESULT AufLadPr0([out, retval] DOUBLE* pVal);
	[propget, id(7), helpstring("Eigenschaft SaKzSInt")] HRESULT SaKzSInt([out, retval] SHORT* pVal);
	[propget, id(8), helpstring("Eigenschaft AGew")] HRESULT AGew([out, retval] DOUBLE* pVal);
	[propget, id(9), helpstring("Eigenschaft DrFolge")] HRESULT DrFolge([out, retval] SHORT* pVal);
	[propget, id(10), helpstring("Eigenschaft MeEinh")] HRESULT MeEinh([out, retval] SHORT* pVal);
	[propget, id(11), helpstring("Eigenschaft BasisMeBz")] HRESULT BasisMeBz([out, retval] BSTR* pVal);
	[propget, id(12), helpstring("Eigenschaft MeEinhKun")] HRESULT MeEinhKun([out, retval] SHORT* pVal);
	[propget, id(13), helpstring("Eigenschaft MeEinhKun1")] HRESULT MeEinhKun1([out, retval] SHORT* pVal);
	[propget, id(14), helpstring("Eigenschaft MeEinhKun2")] HRESULT MeEinhKun2([out, retval] SHORT* pVal);
	[propget, id(15), helpstring("Eigenschaft MeEinhKun3")] HRESULT MeEinhKun3([out, retval] SCODE* pVal);
	[propget, id(16), helpstring("Eigenschaft Inh1")] HRESULT Inh1([out, retval] DOUBLE* pVal);
	[propget, id(17), helpstring("Eigenschaft Inh2")] HRESULT Inh2([out, retval] DOUBLE* pVal);
	[propget, id(18), helpstring("Eigenschaft Inh3")] HRESULT Inh3([out, retval] DOUBLE* pVal);
	[propget, id(19), helpstring("Eigenschaft MeBz")] HRESULT MeBz([out, retval] BSTR* pVal);
	[propget, id(20), helpstring("Eigenschaft LastMe")] HRESULT LastMe([out, retval] DOUBLE* pVal);
	[propget, id(21), helpstring("Eigenschaft LastLDat")] HRESULT LastLDat([out, retval] BSTR* pVal);
	[propget, id(22), helpstring("Eigenschaft LastPrVk")] HRESULT LastPrVk([out, retval] DOUBLE* pVal);
	[propget, id(23), helpstring("Eigenschaft EndOfRecords")] HRESULT EndOfRecords([out, retval] VARIANT_BOOL* pVal);
	[propget, id(24), helpstring("Eigenschaft Marge")] HRESULT Marge([out, retval] DOUBLE* pVal);
	[propget, id(25), helpstring("Eigenschaft AufLadPrPrc")] HRESULT AufLadPrPrc([out, retval] DOUBLE* pVal);
	[propget, id(26), helpstring("Eigenschaft Bsd")] HRESULT Bsd([out, retval] DOUBLE* pVal);
	[propget, id(27), helpstring("Eigenschaft Bsd2")] HRESULT Bsd2([out, retval] DOUBLE* pVal);
	[propget, id(28), helpstring("Eigenschaft Smt")] HRESULT Smt([out, retval] BSTR* pVal);
};



// CCAufps

[
	coclass,
	default(ICAufps),
	threading(apartment),
	vi_progid("FitObjects.CAufps"),
	progid("FitObjects.CAufps.1"),
	version(1.0),
	uuid("C0560202-540E-47B6-934F-DE8D0E200E18"),
	helpstring("CAufps Class")
]

class ATL_NO_VTABLE CCAufps :
	public ICAufps
{
private:
	long posi;
	double a;
	_bstr_t a_bz1;
	double auf_vk_pr;
	double auf_lad_pr;
	double auf_lad_pr0;
	double auf_lad_pr_prc;
	double marge;
	short sa_kz_sint;
	double a_gew;
	short dr_folge;
	double bsd;
	double bsd2;

public:

	void SetPosi (long posi)
	{
		this->posi = posi;
	}
	void SetA (double a)
	{
		this->a = a;
	}

	void SetABz1 (char * a_bz1)
	{
		wchar_t *a_bz1_w;
		size_t ssource = strlen (a_bz1) + 1;
		size_t starget = ssource * 2;
		size_t converted = 0;
		a_bz1_w = new wchar_t [starget];
		mbstowcs_s(&converted, a_bz1_w, ssource, a_bz1, _TRUNCATE);
		this->a_bz1 = a_bz1_w;
		delete a_bz1_w;
	}

	void SetAufVkPr (double auf_vk_pr)
	{
		this->auf_vk_pr = auf_vk_pr;
	}

	void SetAufLadPr (double auf_lad_pr)
	{
		this->auf_lad_pr = auf_lad_pr;
	}

	void SetAufLadPr0 (double auf_lad_pr0)
	{
		this->auf_lad_pr0 = auf_lad_pr0;
	}

	void SetAufLadPrPrc (double auf_lad_pr_prc)
	{
		this->auf_lad_pr_prc = auf_lad_pr_prc;
	}

	void SetMarge (double marge)
	{
		this->marge = marge;
	}

	void SetSaKzSint (short sa_kz_sint)
	{
		this->sa_kz_sint = sa_kz_sint;
	}

	void SetAGew (double a_gew)
	{
		this->a_gew = a_gew;
	}

	void SetBsd (double bsd)
	{
		this->bsd = bsd;
	}

	void SetBsd2 (double bsd2)
	{
		this->bsd2 = bsd2;
	}

	void SetDrFolge (short dr_folge)
	{
		this->dr_folge = dr_folge;
	}

private:
	short me_einh;
public:
	void SetMeEinh (short me_einh)
	{
		this->me_einh = me_einh;
	}
private:
	_bstr_t smt;
public:
	void SetSmt (LPTSTR smt)
	{
		wchar_t *smt_w;
		size_t ssource = strlen (smt) + 1;
		size_t starget = ssource * 2;
		size_t converted = 0;
		smt_w = new wchar_t [starget];
		mbstowcs_s(&converted, smt_w, ssource, smt, _TRUNCATE);
		this->smt = smt_w;
		delete smt_w;
	}

private:
	_bstr_t basis_me_bz;
public:
	void SetBasisMeBz (char *basis_me_bz)
	{
		wchar_t *basis_me_bz_w;
		size_t ssource = strlen (basis_me_bz) + 1;
		size_t starget = ssource * 2;
		size_t converted = 0;
		basis_me_bz_w = new wchar_t [starget];
		mbstowcs_s(&converted, basis_me_bz_w, ssource, basis_me_bz, _TRUNCATE);
		this->basis_me_bz = basis_me_bz_w;
		delete basis_me_bz_w;
	}
private:
	short me_einh_kun;
public:
	void SetMeEinhKun (short me_einh_kun)
	{
		this->me_einh_kun = me_einh_kun;
	}

private:
	short me_einh_kun1;
public:
	void SetMeEinhKun1 (short me_einh_kun1)
	{
		this->me_einh_kun1 = me_einh_kun1;
	}

private:
	short me_einh_kun2;
public:
	void SetMeEinhKun2 (short me_einh_kun2)
	{
		this->me_einh_kun2 = me_einh_kun2;
	}

private:
	short me_einh_kun3;
public:
	void SetMeEinhKun3 (short me_einh_kun3)
	{
		this->me_einh_kun3= me_einh_kun3;
	}

private:
	double inh1;
public:
	void SetInh1 (double inh1)
	{
		this->inh1 = inh1;
	}

private:
	double inh2;
public:
	void SetInh2 (double inh2)
	{
		this->inh2 = inh2;
	}

private:
	double inh3;
public:
	void SetInh3 (double inh3)
	{
		this->inh3 = inh3;
	}

private:
	_bstr_t me_bz;
public:
	void SetMeBz (char *me_bz)
	{
		wchar_t *me_bz_w;
		size_t ssource = strlen (me_bz) + 1;
		size_t starget = ssource * 2;
		size_t converted = 0;
		me_bz_w = new wchar_t [starget];
		mbstowcs_s(&converted, me_bz_w, ssource, me_bz, _TRUNCATE);
		this->me_bz = me_bz_w;
		delete me_bz_w;
	}

private:
	   double last_me;
public:
	   void SetLastMe (double last_me)
	   {
		   this->last_me = last_me;
	   }
private:
	   _bstr_t last_ldat;
public:
	   void SetLastLDat (char *last_ldat)
	   {
			wchar_t *last_ldat_w;
			size_t ssource = strlen (last_ldat) + 1;
			size_t starget = ssource * 2;
			size_t converted = 0;
			last_ldat_w = new wchar_t [starget];
			mbstowcs_s(&converted, last_ldat_w, ssource, last_ldat, _TRUNCATE);
			this->last_ldat = last_ldat_w;
			delete last_ldat_w;
	   }
private:
	   double last_pr_vk;
public:
	   void SetLastPrVk (double last_pr_vk)
	   {
		   this->last_pr_vk = last_pr_vk;
	   }
private:
	   BOOL end_of_records;
public:
	   void SetEndOfRecord (BOOL end_of_records)
	   {
		   this->end_of_records = end_of_records;
	   }

public:
	CCAufps()
	{
	}

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	STDMETHOD(get_A)(DOUBLE* pVal);
public:
	STDMETHOD(get_ABz1)(BSTR* pVal);
public:
	STDMETHOD(get_Posi)(LONG* pVal);
public:
	STDMETHOD(get_AufVkPr)(DOUBLE* pVal);
public:
	STDMETHOD(get_AufLadPr)(DOUBLE* pVal);
public:
	STDMETHOD(get_AufLadPr0)(DOUBLE* pVal);
public:
	STDMETHOD(get_AufLadPrPrc)(DOUBLE* pVal);
public:
	STDMETHOD(get_Marge)(DOUBLE* pVal);
public:
	STDMETHOD(get_SaKzSInt)(SHORT* pVal);
public:
	STDMETHOD(get_AGew)(DOUBLE* pVal);
public:
	STDMETHOD(get_DrFolge)(SHORT* pVal);
public:
	STDMETHOD(get_MeEinh)(SHORT* pVal);
public:
	STDMETHOD(get_Smt)(BSTR* pVal);
public:
	STDMETHOD(get_BasisMeBz)(BSTR* pVal);
public:
	STDMETHOD(get_MeEinhKun)(SHORT* pVal);
public:
	STDMETHOD(get_MeEinhKun1)(SHORT* pVal);
public:
	STDMETHOD(get_MeEinhKun2)(SHORT* pVal);
public:
	STDMETHOD(get_MeEinhKun3)(SCODE* pVal);
public:
	STDMETHOD(get_Inh1)(DOUBLE* pVal);
public:
	STDMETHOD(get_Inh2)(DOUBLE* pVal);
public:
	STDMETHOD(get_Inh3)(DOUBLE* pVal);
public:
	STDMETHOD(get_MeBz)(BSTR* pVal);
public:
	STDMETHOD(get_LastMe)(DOUBLE* pVal);
public:
	STDMETHOD(get_LastLDat)(BSTR* pVal);
public:
	STDMETHOD(get_LastPrVk)(DOUBLE* pVal);
public:
	STDMETHOD(get_EndOfRecords)(VARIANT_BOOL* pVal);
public:
	STDMETHOD(get_Bsd)(DOUBLE* pVal);
public:
	STDMETHOD(get_Bsd2)(DOUBLE* pVal);
};

