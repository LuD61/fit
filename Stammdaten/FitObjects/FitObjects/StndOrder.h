// StndOrder.h: Deklaration von CStndOrder

#pragma once
#include <comutil.h>
#include "resource.h"       // Hauptsymbole
#include "ClientOrder.h"
#include "ListHandler.h"
#include "debug.h"
#include "CAufps.h"
#include "ComObject.h"
#include "DataCollection.h"
#include "Mutex.h"

#define IDEBUG CDebug::GetInstance ()
#define LINE CDebug::GetInstance ()->GetLine ()

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Singlethread-COM-Objekte werden auf der Windows CE-Plattform nicht vollständig unterstützt. Windows Mobile-Plattformen bieten beispielsweise keine vollständige DCOM-Unterstützung. Definieren Sie _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA, um ATL zu zwingen, die Erstellung von Singlethread-COM-Objekten zu unterstützen und die Verwendung eigener Singlethread-COM-Objektimplementierungen zu erlauben. Das Threadmodell in der RGS-Datei wurde auf 'Free' festgelegt, da dies das einzige Threadmodell ist, das auf Windows CE-Plattformen ohne DCOM unterstützt wird."
#endif


// IStndOrder
[
	object,
	uuid("39B03882-167E-4B31-A372-9F250C4E3B77"),
	dual,	helpstring("IStndOrder-Schnittstelle"),
	pointer_default(unique)
]
__interface IStndOrder : IDispatch
{
	[propput, id(1), helpstring("Eigenschaft Mdn")] HRESULT Mdn([in] SHORT newVal);
	[propput, id(2), helpstring("Eigenschaft Fil")] HRESULT Fil([in] SHORT newVal);
	[propput, id(3), helpstring("Eigenschaft KunFil")] HRESULT KunFil([in] SHORT newVal);
	[propput, id(4), helpstring("Eigenschaft Kun")] HRESULT Kun([in] LONG newVal);
	[propput, id(5), helpstring("property AktAuf")] HRESULT AktAuf([in] LONG newVal);
	[propget, id(6), helpstring("property name")] HRESULT Name([out, retval] BSTR* pVal);
	[id(7), helpstring("method SetWindow")] HRESULT SetWindow(void);
	[id(8), helpstring("method Load")] HRESULT Load([in] SHORT client);
	[id(9), helpstring("Methode GetFirstRecord")] HRESULT GetFirstRecord([in] SHORT client, [out, retval] IDispatch **pVal);
	[id(10), helpstring("Methode GetNextRecord")] HRESULT GetNextRecord([in] SHORT client, [out, retval] IDispatch **pVal);
	[id(11), helpstring("method RegisterClient")] HRESULT RegisterClient([out, retval] SHORT* pVal);
	[id(12), helpstring("method UnregisterClient")] HRESULT UnregisterClient([in] SHORT newVal);
	[id(13), helpstring("method SetKeys")] HRESULT SetKeys([in] SHORT mdn, [in] SHORT fil, [in] SHORT kun_fil, [in] LONG kun, [in] LONG akt_auf, [in] SHORT client);
	[propput, id(14), helpstring("Eigenschaft SortMode")] HRESULT SortMode([in] SHORT newVal);
	[propput, id(15), helpstring("Eigenschaft RdOptimize")] HRESULT RdOptimize([in] SHORT newVal);
};


// CStndOrder

[
	coclass,
	default(IStndOrder),
	threading(apartment),
	vi_progid("FitObjects.StndOrder"),
	progid("FitObjects.StndOrder.1"),
	version(1.0),
	uuid("8E7B5B9C-8AD6-4798-BB2D-3593DD411802"),
	helpstring("StndOrder Class")
]
class ATL_NO_VTABLE CStndOrder :
	public IStndOrder
{
private:
	CClientOrder *ClientOrder;
	CDataCollection<CClientOrder *> ClientTab;
//	BSTR Name;
	_bstr_t Name;
	ComObject com;
	ComObject comCAufps;
	CCAufps *CAufps;
	CMutex *Mutex;

public:
	CStndOrder()
	{
		Mutex = NULL;
		Name = L"StndOrder";
		IDEBUG->SetName ("C:\\temp\\StndOrder.log");
		strcpy (LINE, "StndOrder gestartet");
		IDEBUG->Write ();
	}

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

	void SetValues (CCAufps *CAufps, AUFPS *Aufps);

public:

public:
	STDMETHOD(put_SortMode)(SHORT newVal);
public:
	STDMETHOD(put_RdOptimize)(SHORT newVal);
public:
	STDMETHOD(put_Mdn)(SHORT newVal);
public:
	STDMETHOD(put_Fil)(SHORT newVal);
public:
	STDMETHOD(put_KunFil)(SHORT newVal);
public:
	STDMETHOD(put_Kun)(LONG newVal);
public:
	STDMETHOD(put_AktAuf)(LONG newVal);
public:
	STDMETHOD(get_Name)(BSTR* pVal);
public:
	STDMETHOD(SetWindow)(void);
public:
	STDMETHOD(Load)(short client);
public:
	STDMETHOD(GetFirstRecord)(short client, IDispatch **pVal);
public:
	STDMETHOD(GetNextRecord)(short client, IDispatch **pVal );
public:
	STDMETHOD(RegisterClient)(SHORT *pVal);
public:
	STDMETHOD(UnregisterClient)(SHORT pVal);
public:
	STDMETHOD(SetKeys)(SHORT mdn, SHORT fil, SHORT kun_fil, LONG kun, LONG akt_auf, SHORT client);
};

