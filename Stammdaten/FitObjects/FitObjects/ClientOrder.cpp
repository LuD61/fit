// ClientOrder.cpp: Implementierung der Klasse CClientOrder.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <fstream>
#include "ClientOrder.h"
#include "StrFuncs.h"
#include "A_bas.h"
#include "FitObjectsDlg.h"
#include "ClientHandler.h"
#include "FileHandler.h"
#include "debug.h"

#define DLG CFitObjectsDlg::GetInstance ()

#define LINE DLG->GetLine () 

#define IDEBUG CDebug::GetInstance ()
#define DBLINE CDebug::GetInstance ()->GetLine ()

#define SORT_MODE CClientHandler::GetInstance ()->GetSortMode ()
#define RD_OPTIMIZE CClientHandler::GetInstance ()->GetRdOptimize ()

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

using namespace std;

extern short sql_mode;

CClientOrder::CClientOrder()
{
	ListHandler = NULL;
	StndAll = NULL;
	mdn = 0;
	fil = 0;
	kun_fil = 0;
	kun_nr = 0l;
	cursor = -1;
    LastFromAufKun = FALSE;
	inh = 0.0;
	DbIsOpen = false;
}

CClientOrder::~CClientOrder()
{
	if (ListHandler != NULL)
	{
		delete ListHandler;
		ListHandler = NULL;
	}
}

void CClientOrder::SetKeys ()
{
	stnd_aufp.mdn = mdn;
	stnd_aufp.fil = fil;
	stnd_aufp.kun_fil = kun_fil;
	stnd_aufp.kun = kun_nr;
}

void CClientOrder::SetKeys (short mdn, short fil, short kun_fil, long kun)
{
	stnd_aufp.mdn = mdn;
	stnd_aufp.fil = fil;
	stnd_aufp.kun_fil = kun_fil;
	stnd_aufp.kun = kun;
}

void CClientOrder::Load ()
{
	BOOL MustLoad = FALSE;

	if (!DbIsOpen)
	{
		sprintf (DBLINE, "�ffnen der Datenbank");
		IDEBUG->Write ();
		StndAufp.opendbase ("bws");
		sprintf (DBLINE, "Datenbank ge�ffnet");
		IDEBUG->Write ();
		sql_mode = 1;
		DbIsOpen = true;
	}
	ListHandler = new CListHandler ();
	StndAll = ListHandler->GetStndAll ();
	ListHandler->Init ();
	MustLoad = TRUE;
	mdn = stnd_aufp.mdn;
	fil = stnd_aufp.fil;
	kun_fil = stnd_aufp.kun_fil;
	kun_nr = stnd_aufp.kun;
	sprintf (DBLINE, "Lesecursor vorbereiten");
	IDEBUG->Write ();
	Prepare ();
	sprintf (DBLINE, "Lesecursor = %d", cursor);
	IDEBUG->Write ();
	Read ();
}

void CClientOrder::Prepare ()
{
	StndAufp.sqlin ((short *) &mdn, 1, 0);
	StndAufp.sqlin ((short *) &fil, 1, 0);
	StndAufp.sqlin ((long *) &kun_nr, 2, 0);
	StndAufp.sqlin ((short *) &kun_fil, 1, 0);

	StndAufp.sqlout ((short *) &stnd_aufp.mdn, 1, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.fil, 1, 0);
	StndAufp.sqlout ((long *) &stnd_aufp.kun, 2, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.kun_fil, 1, 0);
	StndAufp.sqlout ((long *) &stnd_aufp.posi, 2, 0);
	StndAufp.sqlout ((double *) &stnd_aufp.a, 3, 0);
	StndAufp.sqlout ((short *) &ag, 1, 0);
	StndAufp.sqlout ((short *) &wg, 1, 0);
	StndAufp.sqlout ((char *)  &a_bz1, 0, 25);

	if (SORT_MODE == CClientHandler::PosA)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 5,6");  
	}
	else if (SORT_MODE == CClientHandler::AgA)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 7,6");  
	}
	else if (SORT_MODE == CClientHandler::WgA)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 8,6");  
	}
	else if (SORT_MODE == CClientHandler::AgBz)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 7,9");  
	}
	else if (SORT_MODE == CClientHandler::WgBz)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 8,9");  
	}
	LINE.Format ("Cursor : %ld", cursor);
	DLG->AddLine (LINE);
}


int CClientOrder::Read ()
{
	int i = 0;
	int dsqlstatus = -1;
	int posi;
	AUFPS *Aufps;

	sprintf (DBLINE, "Lesen der Daten");
	IDEBUG->Write ();
	KunBran.Format (_T("kun%ld"), kun_nr);
	LINE.Format ("Werte vor open : %hd %hd %ld %hd", mdn, fil, kun_nr, kun_fil);
	DLG->AddLine (LINE);

    if ((dsqlstatus = StndAufp.sqlopen (cursor)) != 0)
	{
		LINE.Format ("Fehlen beim �ffnen");
		DLG->AddLine (LINE);
		return dsqlstatus;
	}

	if (ReadAufps ())
	{
		StndAufp.sqlclose (cursor); 
		return 0;
	}
    dsqlstatus = StndAufp.sqlfetch (cursor);
    LINE.Format ("SQLSTATUS nach 1. Lesen %d", dsqlstatus);
	DLG->AddLine (LINE);
	sprintf (DBLINE, "SQLSTATUS nach 1. Lesen %d", dsqlstatus);
	IDEBUG->Write ();

	if (dsqlstatus == 100)
	{
			if (stnd_aufp.kun_fil != 0) return dsqlstatus;


			sprintf (DBLINE, "Lesen der Branche");
			IDEBUG->Write ();
            StndAufp.sqlin  ((short *) &mdn, 1, 0);
            StndAufp.sqlin  ((short *) &fil, 1, 0);  
            StndAufp.sqlin  ((long *) &kun_nr, 2, 0);
            StndAufp.sqlout ((char *) kun_bran2, 0, sizeof (kun_bran2));
			int bran_cursor = StndAufp.sqlcursor ("select kun_bran2 from kun "
					              "where mdn = ? "
								  "and fil = ? "
								  "and kun = ?");
			dsqlstatus = StndAufp.sqlfetch (bran_cursor);
			StndAufp.sqlclose (bran_cursor);
			if (dsqlstatus == 100) return dsqlstatus;

			sprintf (DBLINE, "Branche = %s", kun_bran2);
			IDEBUG->Write ();
			KunBran.Format (_T("bran%s"), kun_bran2);
			KunBran.Trim ();
			if (ReadAufps ())
			{
				StndAufp.sqlclose (cursor); 
				return 0;
			}
            kun_nr = atol (kun_bran2);        
            kun_fil = 2;
            dsqlstatus = StndAufp.sqlopen (cursor); 
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
            kun_nr  = stnd_aufp.kun;        
            kun_fil = stnd_aufp.kun_fil;
            dsqlstatus = StndAufp.sqlfetch (cursor); 
	}

	posi = 10;
    while (dsqlstatus == 0)
    {
             stnd_aufp.kun     = kun_nr;        
             stnd_aufp.kun_fil = kun_fil;
			 Aufps = new AUFPS;   
			 memset (Aufps, 0, sizeof (AUFPS));
             StndAufp.dbreadfirsta (); 
			 sprintf (DBLINE, "Artikel %.0lf", stnd_aufp.a);
			 IDEBUG->Write ();
             sprintf (Aufps->a, "%13.0lf", stnd_aufp.a); 
			 A_bas.a_bas.a = stnd_aufp.a;
             dsqlstatus = A_bas.dbreadfirst ();
             if (dsqlstatus == 0)
             {
                         strcpy (Aufps->a_bz1, A_bas.a_bas.a_bz1); 
             }
             else
             {
                         strcpy (Aufps->a_bz1, " "); 
             }
			 sprintf (Aufps->posi, "%d", posi);
			 sprintf (DBLINE, "Belegen der Zeilenwerte");
			 IDEBUG->Write ();
			 FillRow (Aufps);
			 sprintf (DBLINE, "Zeilenwerte belegt");
			 IDEBUG->Write ();
			 StndAll->Add (Aufps);
			 sprintf (DBLINE, "Zeile in StndAll gespeichert");
			 IDEBUG->Write ();
			 LINE.Format ("%s %s %s", Aufps->a, Aufps->a_bz1, Aufps->auf_vk_pr);
			 DLG->AddLine (LINE);
             dsqlstatus = StndAufp.sqlfetch (cursor); 
             i ++;
			 posi += 10;
    }
    StndAufp.sqlclose (cursor); 
	sprintf (DBLINE, "Lesen beendet");
	IDEBUG->Write ();
	cursor = -1;
	return 0;
}

void CClientOrder::FillRow (AUFPS *Aufps)
{
   int dsqlstatus;
   char wert[sizeof (ptabn.ptwert)];
   short sa;
   double pr_ek;
   double pr_vk;
   if (DllPreise.PriceLib != NULL && 
	DllPreise.preise_holen != NULL)
   {
			  dsqlstatus = (DllPreise.preise_holen) (stnd_aufp.mdn, 
			                           0,
				                       stnd_aufp.kun_fil,
					                   stnd_aufp.kun,
						               stnd_aufp.a,
							           (LPSTR) lieferdat.c_str (),
								       &sa,
									   &pr_ek,
									   &pr_vk);
			  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
   }
/*
   else
   {
				dsqlstatus = WaPreis.preise_holen (stnd_aufp.mdn,
                                      0,
                                      stnd_aufp.kun_fil,
                                      stnd_aufp.kun,
                                      stnd_aufp.a,
                                      (LPSTR) lieferdat.c_str (),
                                      &sa,
                                      &pr_ek,
                                      &pr_vk);
   }
*/
//   pr_ek = 0.0; //testtest
//   pr_vk = 0.0; //testtest
   if (pr_ek != 0.0)
   {
	  sprintf (Aufps->auf_vk_pr, "%lf",  pr_ek);
	  sprintf (Aufps->sa_kz_sint, "%1d", sa);
   }
   if (pr_vk != 0.0)
   {
      sprintf (Aufps->auf_lad_pr,"%lf", pr_vk);
	  sprintf (Aufps->sa_kz_sint, "%1d", sa);
   }
   CalcLdPrPrc (Aufps, pr_ek, pr_vk);
//   strcpy (Aufps->kond_art, WaPreis.GetKondArt ());
//   strcpy (Aufps->kond_art0, WaPreis.GetKondArt ());
//   sprintf (Aufps->a_grund, "%4.0lf", WaPreis.GetAGrund ());
   FillKondArt (Aufps);
   Einh.SetAufEinh (1);
   sprintf (Aufps->aufp_txt, "%ld", 0l);
   GetLastMe (Aufps);
   memset (Aufps->bsd, 0, sizeof (Aufps->bsd));
   memcpy (&CronBest.cron_best, &cron_best_null, sizeof (CRON_BEST));
/*
   CronBest.cron_best.a = stnd_aufp.a;
   dsqlstatus = CronBest.dbreadfirst_a ();
   if (dsqlstatus == 0)
   {
	   sprintf (Aufps->bsd, "%.3lf", CronBest.cron_best.me);
   }
*/

   CronBest.cron_best.a = stnd_aufp.a;
   CronBest.cron_best.pr_kz = (int) BsdPrice;
   strcpy (Aufps->bsd, "0.000");
   dsqlstatus = CronBest.dbreadfirst ();
   if (dsqlstatus == 0)
   {
	   sprintf (Aufps->bsd, "%.3lf", CronBest.cron_best.me);
   }

   CronBest.cron_best.pr_kz = (int) BsdNoPrice;
   strcpy (Aufps->bsd2, "0.000");
   dsqlstatus = CronBest.dbreadfirst ();
   if (dsqlstatus == 0)
   {
	   sprintf (Aufps->bsd2, "%.3lf", CronBest.cron_best.me);
   }

   Aufps->a_gew = A_bas.a_bas.a_gew;
   Aufps->dr_folge = A_bas.a_bas.dr_folge;
   sprintf (wert, "%hd", A_bas.a_bas.me_einh);
   sprintf (Aufps->me_einh, "%hd", A_bas.a_bas.me_einh);
   strcpy (Aufps->smt, A_bas.a_bas.smt);
   strcpy (Ptab.ptabn.ptitem, "me_einh");
   strcpy (Ptab.ptabn.ptwert, wert);
   dsqlstatus = Ptab.dbreadfirst ();
   strcpy (Aufps->basis_me_bz, Ptab.ptabn.ptbezk);

   kumebest.mdn = mdn;
   kumebest.fil = fil;
   kumebest.kun = kun_nr;
   strcpy (kumebest.kun_bran2, kun.kun_bran2);
   kumebest.a = CStrFuncs::StrToDouble (Aufps->a);
   ReadMeEinh (Aufps);
}

void CClientOrder::FillKondArt (AUFPS *Aufps)
{
	   int dsqlstatus;

       memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));

       strcpy (ptabn.ptitem,"sap_kond_art");
	   strcpy (ptabn.ptwer1, Aufps->kond_art);

	   StndAufp.sqlin ((char *) ptabn.ptitem, 0, 19);
	   StndAufp.sqlin ((char *) ptabn.ptwer1, 0, 9);
	   StndAufp.sqlout ((char *) ptabn.ptwer2, 0, 9);
	   dsqlstatus = StndAufp.sqlcomm ("select ptwer2 from ptabn "
		                             "where ptitem = ? "
									 "and ptwer1 = ?");
       strcpy (Aufps->kond_art0, ptabn.ptwer1);
       strcpy (Aufps->kond_art,  ptabn.ptwer2);
}

void CClientOrder::GetLastMeAuf (AUFPS *Aufps)
/**
Letzte Bestellmenge des Kunden holen.
**/
{
	   double a; 
	   static int cursork = -1;
	   static int cursorp = -1;
	   static long auf;
	   static double auf_me;
	   static double pr_vk;
	   static double auf_me_ges;
	   static DATE_STRUCT ldat_struct;
	   static char ldat [12];
	   CString LDat;

	   a = CStrFuncs::StrToDouble (Aufps->a);
	   auf_me = 0.0;
	   pr_vk = 0.0;
	   auf_me_ges = 0.0;
	   memset (ldat, 0, sizeof (ldat));
	   if (cursork == -1)
	   {
		   DbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
		   DbClass.sqlin ((short *) &fil, SQLSHORT, 0);
		   DbClass.sqlin ((long *)  &kun_nr, SQLLONG, 0);
		   DbClass.sqlin ((long *)  &akt_auf, SQLLONG, 0);
		   DbClass.sqlout ((long *)  &auf, SQLLONG, 0);
		   DbClass.sqlout ((DATE_STRUCT *)  &ldat_struct, SQLDATE, 0);
		   cursork = DbClass.sqlcursor ("select auf, lieferdat from aufk where mdn = ? "
															  "and fil = ? "
															  "and kun = ? "
															  "and auf != ? "
															  "order by lieferdat desc, "
															  "auf desc");

		   DbClass.sqlin ((short *) &mdn, 1, 0);
		   DbClass.sqlin ((short *) &fil, 1, 0);
		   DbClass.sqlin ((long *)  &auf, 2, 0);
		   DbClass.sqlin ((double *)  &a, 3, 0);
		   DbClass.sqlout ((double *) &auf_me, 3, 0);
		   DbClass.sqlout ((double *) &pr_vk, 3, 0);
		   cursorp = DbClass.sqlcursor ("select auf_me, auf_vk_euro from aufp "
										"where mdn = ? "
										"and fil = ? "
										"and auf = ? "
										"and a = ?");
	   }
	   auf_me_ges = (double) 0.0;

	   if (DbClass.sqlopen (cursork) != 0)
	   {
		   return;
	   }
       while (DbClass.sqlfetch (cursork) == 0)
	   {
		   DbClass.FromDbDate (LDat, (DATE_STRUCT *) &ldat_struct);
		   strcpy (ldat, LDat.GetBuffer ());
		   if (DbClass.sqlopen (cursorp) != 0) break;
		   while (DbClass.sqlfetch (cursorp) == 0)
		   {
			   auf_me_ges += auf_me;
		   }
		   if (auf_me_ges != (double) 0.0) break;
	   }
	   sprintf (Aufps->last_me, "%.3lf", auf_me_ges);
	   sprintf (Aufps->last_ldat, "%s"  , ldat);
	   sprintf (Aufps->last_pr_vk,"%.3lf"  , pr_vk);
}


void CClientOrder::GetLastMe (AUFPS *Aufps)
/**
Letzte Bestellmenge des Kunden holen.
**/
{

	   double a; 
	   long auf;
	   double auf_me_ges;
	   double auf_vk_pr;


	   if (!LastFromAufKun)
	   {
			GetLastMeAuf (Aufps);
			return;
	   }


	   a = CStrFuncs::StrToDouble (Aufps->a);

	   auf_me_ges = 0;
	   strcpy (Aufps->last_ldat, "");
	   aufkun.auf_me = (double) 0.0;
	   aufkun.mdn     = mdn;
	   aufkun.fil     = fil;
	   aufkun.kun     = kun_nr;
	   aufkun.a       = a;
	   aufkun.kun_fil = kun_fil;

	   AufKun.dbreadlast ();

	   auf = aufkun.auf;
	   auf_me_ges = aufkun.auf_me;
	   auf_vk_pr = aufkun.auf_vk_pr;
	   CString LastDat;
	   AufKun.FromDbDate (LastDat, &aufkun.lieferdat);
	   strcpy (Aufps->last_ldat, LastDat.GetBuffer ());
	   while (AufKun.dbreadnextlast () == 0)
	   {
		   if (auf != aufkun.auf) break;
		   auf_me_ges += aufkun.auf_me;
		   auf_vk_pr = aufkun.auf_vk_pr;
	   }
	   if (auf_me_ges == (double) 0.0)
	   {
		   GetLastMeAuf (Aufps);
	   }
	   else
	   {
           sprintf (Aufps->last_me,     "%.3lf", auf_me_ges);
           sprintf (Aufps->last_pr_vk, "%.4lf", auf_vk_pr);
	   }
}

void CClientOrder::ReadMeEinh (AUFPS *Aufps)
/**
Mengeneinheiten holen.
**/
{
        int dsqlstatus;
        char ptwert [5];

        KEINHEIT keinheit;

		memset (&keinheit, 0, sizeof (KEINHEIT));
        Einh.GetKunEinh (mdn, fil, kun_nr,
                         A_bas.a_bas.a, &keinheit);
        strcpy (Aufps->basis_me_bz, keinheit.me_einh_bas_bez);
        strcpy (Aufps->me_bz, keinheit.me_einh_kun_bez);
        sprintf (Aufps->me_einh_kun, "%hd", keinheit.me_einh_kun);
        sprintf (Aufps->me_einh,     "%hd", keinheit.me_einh_bas);
        inh = keinheit.inh;
/*
        sprintf (Aufps->me_einh_kun1, "%hd", keinheit.me_einh1);
        sprintf (Aufps->me_einh_kun2, "%hd", keinheit.me_einh2);
        sprintf (Aufps->me_einh_kun3, "%hd", keinheit.me_einh3);

        sprintf (Aufps->inh1, "%.3lf", keinheit.inh1);
        sprintf (Aufps->inh2, "%.3lf", keinheit.inh2);
        sprintf (Aufps->inh3, "%.3lf", keinheit.inh3);
*/

        sprintf (Aufps->me_einh_kun1, "%hd", 0);
        sprintf (Aufps->me_einh_kun2, "%hd", 0);
        sprintf (Aufps->me_einh_kun3, "%hd", 0);

        sprintf (Aufps->inh1, "%.3lf", 0.0);
        sprintf (Aufps->inh2, "%.3lf", 0.0);
        sprintf (Aufps->inh3, "%.3lf", 0.0);

        return;


        switch (A_bas.a_bas.a_typ)
        {
             case enumHndw :
					Hndw.a_hndw.a = A_bas.a_bas.a;
					dsqlstatus = Hndw.dbreadfirst ();
                     break;
             case enumEig :
					Eig.a_eig.a = A_bas.a_bas.a;
					dsqlstatus = Eig.dbreadfirst ();
                    Hndw.a_hndw.me_einh_kun = Eig.a_eig.me_einh_ek;
                     break;
/*
             case enumEigDiv :
                     dsqlstatus = Hndw.lese_a_eig_div (A_bas.a_bas.a);
                     a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                     break;
*/
             case enumDienst :
             case enumLeih :
                     Hndw.a_hndw.me_einh_kun = A_bas.a_bas.me_einh;
                     break;
             default :
                     Hndw.a_hndw.me_einh_kun = A_bas.a_bas.me_einh;
                     break;
        }

		Aufps->a_typ = A_bas.a_bas.a_typ;

        sprintf (ptwert, "%hd", A_bas.a_bas.me_einh);
		strcpy (Ptab.ptabn.ptitem, "me_einh");
		strcpy (Ptab.ptabn.ptwert, ptwert);
        if (Ptab.dbreadfirst () == 0)
        {
            strcpy (Aufps->basis_me_bz, Ptab.ptabn.ptbezk);
        }

        if (dsqlstatus) return;

		if (a_hndw.me_einh_kun != 0)
		{
			sprintf (ptwert, "%hd", a_hndw.me_einh_kun);
			strcpy (Ptab.ptabn.ptwert, ptwert);
			if (Ptab.dbreadfirst () == 0)
			{
				strcpy (Aufps->me_bz, ptabn.ptbezk);
			}
		}
}


void CClientOrder::CalcLdPrPrc (AUFPS *aufps, double vk_pr, double ld_pr)
{
	    double auf_lad_pr_prc = 0.0;
	    if (vk_pr != 0.0 && ld_pr != 0.0)
		{
			auf_lad_pr_prc = 100 - (vk_pr * 100 / ld_pr);	
		}
		sprintf (aufps->auf_lad_pr_prc,"%lf", auf_lad_pr_prc);
		GrpPrice.SetAKey (CStrFuncs::StrToDouble (aufps->a));
		GrpPrice.CalculateLdPr (vk_pr, aufps->marge, sizeof (aufps->marge), aufps->auf_lad_pr0, sizeof (aufps->auf_lad_pr0));
}

void CClientOrder::WriteAufps ()
{
	string msOutFileName;
	string msLine;
	fstream mfspOut;
	AUFPS **it;
	AUFPS *Aufps;
	CString Etc;
	CString FileName;

	Etc.GetEnvironmentVariable (_T("BWSETC"));
	FileName.Format (_T("%s\\stnd"), Etc.GetBuffer ());
	CreateDirectory (FileName.GetBuffer (), NULL);
	FileName += _T("\\");
	FileName += KunBran;
	msOutFileName = FileName.GetBuffer ();
	mfspOut.open (msOutFileName.c_str (), ios_base::out | ios_base::binary);
	if (!mfspOut.is_open ())
	{
		 DLG->AddLine (CString ("Fehler beim Datei�ffnen"));
		return;
	}
    DLG->AddLine (CString ("Datei �ffnen OK"));
    StndAll->Start ();
	while ((it = StndAll->GetNext ()) != NULL)
	{
		Aufps = *it;
		mfspOut.write ((const char *) Aufps, sizeof (AUFPS));
	}
	mfspOut.close ();
}

BOOL CClientOrder::ReadAufps ()
{
	BOOL ret = FALSE;
	string msInFileName;
	string msLine;
	fstream mfspIn;
	AUFPS *Aufps;
	CString Etc;
	CString FileName;

	Etc.GetEnvironmentVariable (_T("BWSETC"));
	FileName.Format (_T("%s\\stnd"), Etc.GetBuffer ());
	CreateDirectory (FileName.GetBuffer (), NULL);
	FileName += _T("\\");
	FileName += KunBran;
    CFileHandler *FileHandler = new CFileHandler (FileName); 

	if (RD_OPTIMIZE == CClientHandler::FromFile || FileHandler->IsNewFile ())
	{
		msInFileName = FileName.GetBuffer ();
		sprintf (DBLINE, "Lesen der Daten");
		IDEBUG->Write ();
		sprintf (DBLINE, "�ffnen der Datei %s", msInFileName.c_str ());
		IDEBUG->Write ();
		mfspIn.open (msInFileName.c_str (), ios_base::in | ios_base::binary);
		if (mfspIn.is_open ())
		{
			DLG->AddLine (CString ("Datei �ffnen zum Lesen OK"));
			sprintf (DBLINE, "Datei �ffnen zum Lesen OK");
			IDEBUG->Write ();
			StndAll->DestroyElements ();
			StndAll->Destroy ();
			Aufps = new AUFPS;
			mfspIn.read ((char *) Aufps, sizeof (AUFPS));
			while (!mfspIn.eof ())
			{
				StndAll->Add (Aufps);
				Aufps = new AUFPS;
				mfspIn.read ((char *) Aufps, sizeof (AUFPS));
			}
			delete Aufps;
			mfspIn.close ();
			ret = TRUE;
		}
	}
	return ret;
}
