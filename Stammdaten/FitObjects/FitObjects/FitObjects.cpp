// FitObjects.cpp : Implementierung von WinMain

#include "stdafx.h"
#include "resource.h"
#include "FitObjects.h"
#include "FitObjectsDlg.h"

#define DLG CFitObjectsDlg::GetInstance ()

[
	module(exe, uuid = "{AA4187D7-4329-412D-B092-6F700906B4C4}", 
				helpstring = "FitObjects 1.0-Typbibliothek", 
				name = "FitObjectsLib")
];


//class CFitObjectsModule : public CAtlExeModuleT< CFitObjectsModule >
//class CFitObjectsModule
//{
//public :
//	DECLARE_LIBID(LIBID_FitObjectsLib)
//	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_FITOBJECTS, "{61C19D35-4C43-41CC-BFF9-DF16BA0D3577}")

/*
	HRESULT PreMessageLoop(int nShowCmd)
	{
		DLG->Create();
		DLG->Show (true);
		DLG->AddLine (CString (_T("FitObjects gestartet")));
		HRESULT hr = CAtlExeModuleT<CFitObjectsModule>::PreMessageLoop(nShowCmd);
		if (FAILED(hr))
		{
			DLG->AddLine (CString (_T("Fehler beim Start")));
			return hr;
		}
		DLG->AddLine (CString (_T("Start OK")));
		return S_OK;
	}
*/
//};

//CFitObjectsModule _AtlModule;



//
#if 0
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, 
                                LPTSTR /*lpCmdLine*/, int nShowCmd)
{
    return _AtlModule.WinMain(nShowCmd);
}
#endif

