#include "StdAfx.h"
#include "FileHandler.h"
#include "DbTime.h"
#include <time.h>

CFileHandler::CFileHandler(void)
{
	m_DiffTime = 60;
}

CFileHandler::~CFileHandler(void)
{
}

CFileHandler::CFileHandler(CString& FileName)
{
	m_FileName = FileName;
	m_DiffTime = 60;
}
CFileHandler::CFileHandler(LPTSTR FileName)
{
	m_FileName = FileName;
	m_DiffTime = 60;
}
BOOL CFileHandler::IsNewFile ()
{
	BOOL ret = FALSE;
	WIN32_FIND_DATA fileData;;
	FILETIME fTime;
	FILETIME sfTime;
	ULARGE_INTEGER ufTime;
	ULARGE_INTEGER usfTime;
	long diff;


	HANDLE file = FindFirstFile (m_FileName.GetBuffer (), &fileData);
	if (file != INVALID_HANDLE_VALUE)
	{
		fTime = fileData.ftLastWriteTime;
		GetSystemTimeAsFileTime (&sfTime);
		memcpy (&ufTime, &fTime, sizeof (ULARGE_INTEGER));
		memcpy (&usfTime, &sfTime, sizeof (ULARGE_INTEGER));
		diff = (long) (usfTime.QuadPart - ufTime.QuadPart) / 10000000;
		if (diff > 0 && diff < m_DiffTime)
		{
			ret = TRUE;
		}
	}
	return ret;
}
