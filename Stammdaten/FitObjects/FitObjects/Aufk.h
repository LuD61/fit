#ifndef _AUFK_DEF
#define _AUFK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AUFK {
   long           mdn;
   short          fil;
   long           ang;
   long           auf;
   long           adr;
   short          kun_fil;
   long           kun;
   DATE_STRUCT    lieferdat;
   TCHAR          lieferzeit[6];
   TCHAR          hinweis[49];
   short          auf_stat;
   TCHAR          kun_krz1[17];
   TCHAR          feld_bz1[20];
   TCHAR          feld_bz2[12];
   TCHAR          feld_bz3[8];
   short          delstatus;
   double         zeit_dec;
   long           kopf_txt;
   long           fuss_txt;
   long           vertr;
   TCHAR          auf_ext[17];
   long           tou;
   TCHAR          pers_nam[9];
   DATE_STRUCT    komm_dat;
   DATE_STRUCT    best_dat;
   short          waehrung;
   short          auf_art;
   short          gruppe;
   short          ccmarkt;
   short          fak_typ;
   long           tou_nr;
   TCHAR          ueb_kz[2];
   DATE_STRUCT    fix_dat;
   TCHAR          komm_name[13];
   DATE_STRUCT    akv;
   TCHAR          akv_zeit[6];
   DATE_STRUCT    bearb;
   TCHAR          bearb_zeit[6];
   short          psteuer_kz;
};
extern struct AUFK aufk, aufk_null;

#line 8 "Aufk.rh"

class AUFK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               AUFK aufk;  
               AUFK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
