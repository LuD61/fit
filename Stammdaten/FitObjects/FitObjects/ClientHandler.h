#pragma once
#include "DataCollection.h"
#include "ClientKey.h"

class CClientHandler
{
public:
	enum STD_SORT
	{
		PosA = 0,
        AgA = 1,
		WgA = 2,
		AgBz = 3,
		WgBz = 4,
	};
	enum STD_RDOPTIMIZE
	{
		No = 0,
        FromFile = 1,
	};
private:
	static CClientHandler *instance;
	int client;
	BOOL IsRunning;
	STD_SORT SortMode;
	STD_RDOPTIMIZE RdOptimize;
	CDataCollection<CClientKey *> ClientKeys;
protected:
	CClientHandler(void);
	~CClientHandler(void);
public:
	void SetSortMode (STD_SORT SortMode)
	{
		this->SortMode = SortMode;
	}

	STD_SORT GetSortMode ()
	{
		return SortMode;
	}

	void SetRdOptimize (STD_RDOPTIMIZE RdOptimize)
	{
		this->RdOptimize = RdOptimize;
	}

	STD_RDOPTIMIZE GetRdOptimize ()
	{
		return RdOptimize;
	}

	static CClientHandler *GetInstance ();
	static void DestroyInstance ();
	BOOL Lock ();
	void Unlock ();
	int Add (CClientOrder *ClientOrder);
	void Drop (int client);
	int GetFreeClient ();
	CClientOrder*GetClient (int client);
};
