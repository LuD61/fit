#pragma once
#include "A_bas.h"
#include "Aufk.h"
#include "Aufp.h"
#include "Mdn.h"
#include "Fil.h"
#include "Kun.h"
#include "A_hndw.h"
#include "A_eig.h"
#include "A_kun.h"
#include "Kumebest.h"
#include "Ptabn.h"

class CDatatables
{
public:
	A_BAS_CLASS A_bas;
	AUFK_CLASS Aufk;
	AUFP_CLASS Aufp;
	MDN_CLASS Mdn;
	FIL_CLASS Fil;
	KUN_CLASS Kun;
	A_HNDW_CLASS A_hndw;
	A_EIG_CLASS A_eig;
	A_KUN_CLASS A_kun;
	PTABN_CLASS Ptabn;
	KMB_CLASS Kmb;
	static CDatatables *Instance;
	CDatatables(void);
public:
	A_BAS_CLASS *GetABas ()
	{
		return &A_bas;
	}

	AUFK_CLASS *GetAufk ()
	{
		return &Aufk;
	}

	MDN_CLASS *GetMdn ()
	{
		return &Mdn;
	}
	FIL_CLASS *GetFil ()
	{
		return &Fil;
	}

	KUN_CLASS *GetKun ()
	{
		return &Kun;
	}

	KMB_CLASS *GetKmb ()
	{
		return &Kmb;
	}


	A_KUN_CLASS *GetAKun ()
	{
		return &A_kun;
	}

	A_HNDW_CLASS *GetAHndw ()
	{
		return &A_hndw;
	}

	A_EIG_CLASS *GetAEig ()
	{
		return &A_eig;
	}

	PTABN_CLASS *GetPtabn ()
	{
		return &Ptabn;
	}

	AUFP_CLASS *GetAufp ()
	{
		return &Aufp;
	}

    static CDatatables *GetInstance ();
    static void DeleteInstance ();
};
