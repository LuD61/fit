#include "stdafx.h"
#include "stnd_auf.h"

struct STND_AUFK stnd_aufk, stnd_aufk_null;
struct STND_AUFP stnd_aufp, stnd_aufp_null;

static char *sqltext;


void SAUFK_CLASS::prepare (void)
{
         sqlin ((char *) &stnd_aufk.mdn, 1, 0);
         sqlin ((char *) &stnd_aufk.fil, 1, 0);  
         sqlin ((char *) &stnd_aufk.kun, 2, 0);
         sqlin ((char *) &stnd_aufk.kun_fil, 1, 0);

    sqlout ((short *) &stnd_aufk.mdn,SQLSHORT,0);
    sqlout ((short *) &stnd_aufk.fil,SQLSHORT,0);
    sqlout ((short *) &stnd_aufk.kun_fil,SQLSHORT,0);
    sqlout ((long *) &stnd_aufk.kun,SQLLONG,0);
    sqlout ((TCHAR *) stnd_aufk.kun_krz1,SQLCHAR,17);
    sqlout ((TCHAR *) stnd_aufk.stnd_auf_kz,SQLCHAR,2);
    sqlout ((short *) &stnd_aufk.delstatus,SQLSHORT,0);
         cursor = sqlcursor (_T("select stnd_aufk.mdn,  ")
_T("stnd_aufk.fil,  stnd_aufk.kun_fil,  stnd_aufk.kun,  ")
_T("stnd_aufk.kun_krz1,  stnd_aufk.stnd_auf_kz,  stnd_aufk.delstatus from stnd_aufk ")

#line 18 "stnd_auf.rpp"
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_fil   = ? "));

    sqlin ((short *) &stnd_aufk.mdn,SQLSHORT,0);
    sqlin ((short *) &stnd_aufk.fil,SQLSHORT,0);
    sqlin ((short *) &stnd_aufk.kun_fil,SQLSHORT,0);
    sqlin ((long *) &stnd_aufk.kun,SQLLONG,0);
    sqlin ((TCHAR *) stnd_aufk.kun_krz1,SQLCHAR,17);
    sqlin ((TCHAR *) stnd_aufk.stnd_auf_kz,SQLCHAR,2);
    sqlin ((short *) &stnd_aufk.delstatus,SQLSHORT,0);
         sqltext = _T("update stnd_aufk set ")
_T("stnd_aufk.mdn = ?,  stnd_aufk.fil = ?,  stnd_aufk.kun_fil = ?,  ")
_T("stnd_aufk.kun = ?,  stnd_aufk.kun_krz1 = ?,  ")
_T("stnd_aufk.stnd_auf_kz = ?,  stnd_aufk.delstatus = ? ")

#line 24 "stnd_auf.rpp"
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_fil = ? ");
  
         sqlin ((char *) &stnd_aufk.mdn, 1, 0);
         sqlin ((char *) &stnd_aufk.fil, 1, 0);  
         sqlin ((char *) &stnd_aufk.kun, 2, 0);
         sqlin ((char *) &stnd_aufk.kun_fil, 1, 0);

         upd_cursor = sqlcursor (sqltext);

    sqlin ((short *) &stnd_aufk.mdn,SQLSHORT,0);
    sqlin ((short *) &stnd_aufk.fil,SQLSHORT,0);
    sqlin ((short *) &stnd_aufk.kun_fil,SQLSHORT,0);
    sqlin ((long *) &stnd_aufk.kun,SQLLONG,0);
    sqlin ((TCHAR *) stnd_aufk.kun_krz1,SQLCHAR,17);
    sqlin ((TCHAR *) stnd_aufk.stnd_auf_kz,SQLCHAR,2);
    sqlin ((short *) &stnd_aufk.delstatus,SQLSHORT,0);
         ins_cursor = sqlcursor (_T("insert into stnd_aufk (")
_T("mdn,  fil,  kun_fil,  kun,  kun_krz1,  stnd_auf_kz,  delstatus) ")

#line 37 "stnd_auf.rpp"
                                   _T("values ")
                                   _T("(?,?,?,?,?,?,?)"));

#line 39 "stnd_auf.rpp"

         sqlin ((char *) &stnd_aufk.mdn, 1, 0);
         sqlin ((char *) &stnd_aufk.fil, 1, 0);  
         sqlin ((char *) &stnd_aufk.kun, 2, 0);
         sqlin ((char *) &stnd_aufk.kun_fil, 1, 0);
         del_cursor = sqlcursor (_T("delete from stnd_aufk ")
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_fil = ? "));

         sqlin ((char *) &stnd_aufk.mdn, 1, 0);
         sqlin ((char *) &stnd_aufk.fil, 1, 0);  
         sqlin ((char *) &stnd_aufk.kun, 2, 0);
         sqlin ((char *) &stnd_aufk.kun_fil, 1, 0);
         test_upd_cursor = sqlcursor (_T("select kun from stnd_aufk ")
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_fil = ?"));
}

int SAUFK_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	     int dsqlstatus;
		 char kun_bran2[5];
         if (cursor == -1)
         {
                this->prepare ();
         }
         dsqlstatus =  this->DB_CLASS::dbreadfirst ();
		 if (dsqlstatus == 100)
		 {
			    if (stnd_aufk.kun_fil != 0) return dsqlstatus;

                sqlin  ((short *) &stnd_aufk.mdn, 1, 0);
                sqlin  ((short *) &stnd_aufk.fil, 1, 0);  
                sqlin  ((long *) &stnd_aufk.kun, 2, 0);
                sqlout ((char *) kun_bran2, 0, 4);
				dsqlstatus = sqlcomm (_T("select kun_bran2 from kun ")
					                  _T("where mdn = ? ")
									  _T("and fil = ? ")
									  _T("and kun = ?"));
				if (dsqlstatus == 100) return dsqlstatus;
				stnd_aufk.kun = atol (kun_bran2);
                dsqlstatus =  this->DB_CLASS::dbreadfirst ();
		 }
		 return dsqlstatus;
}

 
void SAUFP_CLASS::prepare (void)
{
         sqlin ((char *) &stnd_aufp.mdn, 1, 0);
         sqlin ((char *) &stnd_aufp.fil, 1, 0);  
         sqlin ((char *) &stnd_aufp.kun, 2, 0);
         sqlin ((char *) &stnd_aufp.kun_fil, 1, 0);

    sqlout ((short *) &stnd_aufp.mdn,SQLSHORT,0);
    sqlout ((short *) &stnd_aufp.fil,SQLSHORT,0);
    sqlout ((short *) &stnd_aufp.kun_fil,SQLSHORT,0);
    sqlout ((long *) &stnd_aufp.kun,SQLLONG,0);
    sqlout ((double *) &stnd_aufp.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) stnd_aufp.me_einh_bz,SQLCHAR,6);
    sqlout ((DATE_STRUCT *) &stnd_aufp.dat,SQLDATE,0);
    sqlout ((double *) &stnd_aufp.pr_vk,SQLDOUBLE,0);
    sqlout ((double *) &stnd_aufp.me,SQLDOUBLE,0);
    sqlout ((long *) &stnd_aufp.posi,SQLLONG,0);
         cursor = sqlcursor (_T("select stnd_aufp.mdn,  ")
_T("stnd_aufp.fil,  stnd_aufp.kun_fil,  stnd_aufp.kun,  stnd_aufp.a,  ")
_T("stnd_aufp.me_einh_bz,  stnd_aufp.dat,  stnd_aufp.pr_vk,  stnd_aufp.me,  ")
_T("stnd_aufp.posi from stnd_aufp ")

#line 101 "stnd_auf.rpp"
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_fil   = ? "));

    sqlin ((short *) &stnd_aufp.mdn,SQLSHORT,0);
    sqlin ((short *) &stnd_aufp.fil,SQLSHORT,0);
    sqlin ((short *) &stnd_aufp.kun_fil,SQLSHORT,0);
    sqlin ((long *) &stnd_aufp.kun,SQLLONG,0);
    sqlin ((double *) &stnd_aufp.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) stnd_aufp.me_einh_bz,SQLCHAR,6);
    sqlin ((DATE_STRUCT *) &stnd_aufp.dat,SQLDATE,0);
    sqlin ((double *) &stnd_aufp.pr_vk,SQLDOUBLE,0);
    sqlin ((double *) &stnd_aufp.me,SQLDOUBLE,0);
    sqlin ((long *) &stnd_aufp.posi,SQLLONG,0);
         sqltext = _T("update stnd_aufp set ")
_T("stnd_aufp.mdn = ?,  stnd_aufp.fil = ?,  stnd_aufp.kun_fil = ?,  ")
_T("stnd_aufp.kun = ?,  stnd_aufp.a = ?,  stnd_aufp.me_einh_bz = ?,  ")
_T("stnd_aufp.dat = ?,  stnd_aufp.pr_vk = ?,  stnd_aufp.me = ?,  ")
_T("stnd_aufp.posi = ? ")

#line 107 "stnd_auf.rpp"
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_fil = ? ")
                               _T("and   posi = ? ")
                               _T("and   a = ? ");
  
         sqlin ((char *) &stnd_aufp.mdn, 1, 0);
         sqlin ((char *) &stnd_aufp.fil, 1, 0);  
         sqlin ((char *) &stnd_aufp.kun, 2, 0);
         sqlin ((char *) &stnd_aufp.kun_fil, 1, 0);
         sqlin ((char *) &stnd_aufp.posi, 2, 0);
         sqlin ((char *) &stnd_aufp.a, 3, 0);

         upd_cursor = sqlcursor (sqltext);

    sqlin ((short *) &stnd_aufp.mdn,SQLSHORT,0);
    sqlin ((short *) &stnd_aufp.fil,SQLSHORT,0);
    sqlin ((short *) &stnd_aufp.kun_fil,SQLSHORT,0);
    sqlin ((long *) &stnd_aufp.kun,SQLLONG,0);
    sqlin ((double *) &stnd_aufp.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) stnd_aufp.me_einh_bz,SQLCHAR,6);
    sqlin ((DATE_STRUCT *) &stnd_aufp.dat,SQLDATE,0);
    sqlin ((double *) &stnd_aufp.pr_vk,SQLDOUBLE,0);
    sqlin ((double *) &stnd_aufp.me,SQLDOUBLE,0);
    sqlin ((long *) &stnd_aufp.posi,SQLLONG,0);
         ins_cursor = sqlcursor (_T("insert into stnd_aufp (")
_T("mdn,  fil,  kun_fil,  kun,  a,  me_einh_bz,  dat,  pr_vk,  me,  posi) ")

#line 124 "stnd_auf.rpp"
                                   _T("values ")
                                   _T("(?,?,?,?,?,?,?,?,?,?)"));

#line 126 "stnd_auf.rpp"

         sqlin ((char *) &stnd_aufp.mdn, 1, 0);
         sqlin ((char *) &stnd_aufp.fil, 1, 0);  
         sqlin ((char *) &stnd_aufp.kun, 2, 0);
         sqlin ((char *) &stnd_aufp.kun_fil, 1, 0);
         sqlin ((char *) &stnd_aufp.posi, 2, 0);
         sqlin ((char *) &stnd_aufp.a, 3, 0);
         del_cursor = sqlcursor (_T("delete from stnd_aufp ")
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_fil = ? ")
                               _T("and   posi = ? ")
                               _T("and   a = ? "));

         sqlin ((char *) &stnd_aufp.mdn, 1, 0);
         sqlin ((char *) &stnd_aufp.fil, 1, 0);  
         sqlin ((char *) &stnd_aufp.kun, 2, 0);
         sqlin ((char *) &stnd_aufp.kun_fil, 1, 0);
         sqlin ((char *) &stnd_aufp.posi, 2, 0);
         sqlin ((char *) &stnd_aufp.a, 3, 0);
         test_upd_cursor = sqlcursor (_T("select a from stnd_aufp ")
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_fil = ? ")
                               _T("and   posi = ? ")
                               _T("and   a = ? "));
}

void SAUFP_CLASS::preparea (void)
{
         sqlin ((char *) &stnd_aufp.mdn, 1, 0);
         sqlin ((char *) &stnd_aufp.fil, 1, 0);  
         sqlin ((char *) &stnd_aufp.kun, 2, 0);
         sqlin ((char *) &stnd_aufp.kun_fil, 1, 0);
         sqlin ((char *) &stnd_aufp.a, 3, 0);
         sqlin ((char *) &stnd_aufp.posi, 2, 0);
  

    sqlout ((short *) &stnd_aufp.mdn,SQLSHORT,0);
    sqlout ((short *) &stnd_aufp.fil,SQLSHORT,0);
    sqlout ((short *) &stnd_aufp.kun_fil,SQLSHORT,0);
    sqlout ((long *) &stnd_aufp.kun,SQLLONG,0);
    sqlout ((double *) &stnd_aufp.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) stnd_aufp.me_einh_bz,SQLCHAR,6);
    sqlout ((DATE_STRUCT *) &stnd_aufp.dat,SQLDATE,0);
    sqlout ((double *) &stnd_aufp.pr_vk,SQLDOUBLE,0);
    sqlout ((double *) &stnd_aufp.me,SQLDOUBLE,0);
    sqlout ((long *) &stnd_aufp.posi,SQLLONG,0);
         cursora = sqlcursor (_T("select stnd_aufp.mdn,  ")
_T("stnd_aufp.fil,  stnd_aufp.kun_fil,  stnd_aufp.kun,  stnd_aufp.a,  ")
_T("stnd_aufp.me_einh_bz,  stnd_aufp.dat,  stnd_aufp.pr_vk,  stnd_aufp.me,  ")
_T("stnd_aufp.posi from stnd_aufp ")

#line 167 "stnd_auf.rpp"
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_fil   = ? ")
                               _T("and   a = ? ")
                               _T("and posi = ?"));
         sqlin ((char *) &stnd_aufp.mdn, 1, 0);
         sqlin ((char *) &stnd_aufp.fil, 1, 0);  
         sqlin ((char *) &stnd_aufp.kun, 2, 0);
         sqlin ((char *) &stnd_aufp.kun_fil, 1, 0);
         sqlin ((char *) &stnd_aufp.a, 3, 0);
  

    sqlout ((short *) &stnd_aufp.mdn,SQLSHORT,0);
    sqlout ((short *) &stnd_aufp.fil,SQLSHORT,0);
    sqlout ((short *) &stnd_aufp.kun_fil,SQLSHORT,0);
    sqlout ((long *) &stnd_aufp.kun,SQLLONG,0);
    sqlout ((double *) &stnd_aufp.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) stnd_aufp.me_einh_bz,SQLCHAR,6);
    sqlout ((DATE_STRUCT *) &stnd_aufp.dat,SQLDATE,0);
    sqlout ((double *) &stnd_aufp.pr_vk,SQLDOUBLE,0);
    sqlout ((double *) &stnd_aufp.me,SQLDOUBLE,0);
    sqlout ((long *) &stnd_aufp.posi,SQLLONG,0);
         cursora0 = sqlcursor (_T("select stnd_aufp.mdn,  ")
_T("stnd_aufp.fil,  stnd_aufp.kun_fil,  stnd_aufp.kun,  stnd_aufp.a,  ")
_T("stnd_aufp.me_einh_bz,  stnd_aufp.dat,  stnd_aufp.pr_vk,  stnd_aufp.me,  ")
_T("stnd_aufp.posi from stnd_aufp ")

#line 181 "stnd_auf.rpp"
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_fil   = ? ")
                               _T("and   a = ?"));
}


int SAUFP_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	     int dsqlstatus;
		 char kun_bran2[5];
         if (cursor == -1)
         {
                this->prepare ();
         }
         dsqlstatus =  this->DB_CLASS::dbreadfirst ();
		 if (dsqlstatus == 100)
		 {
			    if (stnd_aufk.kun_fil != 0) return dsqlstatus;
                sqlin  ((short *) &stnd_aufp.mdn, 1, 0);
                sqlin  ((short *) &stnd_aufp.fil, 1, 0);  
                sqlin  ((long *) &stnd_aufp.kun, 2, 0);
                sqlout ((char *) kun_bran2, 0, 4);
				dsqlstatus = sqlcomm (_T("select kun_bran2 from kun ")
					                  _T("where mdn = ? ")
									  _T("and fil = ? ")
									  _T("and kun = ?"));
				if (dsqlstatus == 100) return dsqlstatus;
				stnd_aufp.kun = atol (kun_bran2);
				stnd_aufp.kun_fil = 2;
                dsqlstatus =  this->DB_CLASS::dbreadfirst ();
		 }
		 return dsqlstatus;
}


int SAUFP_CLASS::dbreadfirsta (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         int dsqlstatus;

         if (cursora == -1)
         {
                this->prepare ();
                this->preparea ();
         }
         dsqlstatus = sqlopen (cursora);
         dsqlstatus = sqlfetch (cursora);
	   return dsqlstatus;
}


int SAUFP_CLASS::dbreada (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	   int dsqlstatus;

         dsqlstatus = sqlfetch (cursora);
	   return dsqlstatus;
}

int SAUFP_CLASS::dbreadfirsta0 (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         int dsqlstatus;

         if (cursora0 == -1)
         {
                this->prepare ();
                this->preparea ();
         }
         dsqlstatus = sqlopen (cursora0);
         dsqlstatus = sqlfetch (cursora0);
	   return dsqlstatus;
}


int SAUFP_CLASS::dbreada0 (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	   int dsqlstatus;

       dsqlstatus = sqlfetch (cursora0);
	   return dsqlstatus;
}

int SAUFP_CLASS::dbclosea (void)
{
         if (cursora > -1)
         {
                  sqlclose (cursora);
                  cursora = -1;
         }
         if (cursora0 > -1)
         {
                  sqlclose (cursora0);
                  cursora0 = -1;
         }
         return 0;
}

