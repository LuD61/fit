#include "stdafx.h"
#include "aufp.h"

struct AUFP aufp, aufp_null;

void AUFP_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &aufp.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &aufp.fil,  SQLSHORT, 0);
            sqlin ((long *)   &aufp.auf,   SQLLONG, 0);
            sqlin ((long *)   &aufp.posi,   SQLLONG, 0);
            sqlin ((double *) &aufp.a,   SQLDOUBLE, 0);
    sqlout ((short *) &aufp.mdn,SQLSHORT,0);
    sqlout ((short *) &aufp.fil,SQLSHORT,0);
    sqlout ((long *) &aufp.auf,SQLLONG,0);
    sqlout ((long *) &aufp.posi,SQLLONG,0);
    sqlout ((long *) &aufp.aufp_txt,SQLLONG,0);
    sqlout ((double *) &aufp.a,SQLDOUBLE,0);
    sqlout ((double *) &aufp.auf_me,SQLDOUBLE,0);
    sqlout ((TCHAR *) aufp.auf_me_bz,SQLCHAR,7);
    sqlout ((double *) &aufp.lief_me,SQLDOUBLE,0);
    sqlout ((TCHAR *) aufp.lief_me_bz,SQLCHAR,7);
    sqlout ((double *) &aufp.auf_vk_pr,SQLDOUBLE,0);
    sqlout ((double *) &aufp.auf_lad_pr,SQLDOUBLE,0);
    sqlout ((short *) &aufp.delstatus,SQLSHORT,0);
    sqlout ((short *) &aufp.sa_kz_sint,SQLSHORT,0);
    sqlout ((double *) &aufp.prov_satz,SQLDOUBLE,0);
    sqlout ((long *) &aufp.ksys,SQLLONG,0);
    sqlout ((long *) &aufp.pid,SQLLONG,0);
    sqlout ((long *) &aufp.auf_klst,SQLLONG,0);
    sqlout ((short *) &aufp.teil_smt,SQLSHORT,0);
    sqlout ((short *) &aufp.dr_folge,SQLSHORT,0);
    sqlout ((double *) &aufp.inh,SQLDOUBLE,0);
    sqlout ((double *) &aufp.auf_vk_euro,SQLDOUBLE,0);
    sqlout ((double *) &aufp.auf_vk_fremd,SQLDOUBLE,0);
    sqlout ((double *) &aufp.auf_lad_euro,SQLDOUBLE,0);
    sqlout ((double *) &aufp.auf_lad_fremd,SQLDOUBLE,0);
    sqlout ((double *) &aufp.rab_satz,SQLDOUBLE,0);
    sqlout ((short *) &aufp.me_einh_kun,SQLSHORT,0);
    sqlout ((short *) &aufp.me_einh,SQLSHORT,0);
    sqlout ((short *) &aufp.me_einh_kun1,SQLSHORT,0);
    sqlout ((double *) &aufp.auf_me1,SQLDOUBLE,0);
    sqlout ((double *) &aufp.inh1,SQLDOUBLE,0);
    sqlout ((short *) &aufp.me_einh_kun2,SQLSHORT,0);
    sqlout ((double *) &aufp.auf_me2,SQLDOUBLE,0);
    sqlout ((double *) &aufp.inh2,SQLDOUBLE,0);
    sqlout ((short *) &aufp.me_einh_kun3,SQLSHORT,0);
    sqlout ((double *) &aufp.auf_me3,SQLDOUBLE,0);
    sqlout ((double *) &aufp.inh3,SQLDOUBLE,0);
    sqlout ((long *) &aufp.gruppe,SQLLONG,0);
    sqlout ((TCHAR *) aufp.kond_art,SQLCHAR,5);
    sqlout ((double *) &aufp.a_grund,SQLDOUBLE,0);
    sqlout ((short *) &aufp.ls_pos_kz,SQLSHORT,0);
    sqlout ((long *) &aufp.posi_ext,SQLLONG,0);
    sqlout ((long *) &aufp.kun,SQLLONG,0);
    sqlout ((double *) &aufp.a_ers,SQLDOUBLE,0);
    sqlout ((TCHAR *) aufp.ls_charge,SQLCHAR,31);
    sqlout ((short *) &aufp.aufschlag,SQLSHORT,0);
    sqlout ((double *) &aufp.aufschlag_wert,SQLDOUBLE,0);
    sqlout ((short *) &aufp.pos_txt_kz,SQLSHORT,0);
            cursor = sqlcursor (_T("select aufp.mdn,  aufp.fil,  ")
_T("aufp.auf,  aufp.posi,  aufp.aufp_txt,  aufp.a,  aufp.auf_me,  ")
_T("aufp.auf_me_bz,  aufp.lief_me,  aufp.lief_me_bz,  aufp.auf_vk_pr,  ")
_T("aufp.auf_lad_pr,  aufp.delstatus,  aufp.sa_kz_sint,  aufp.prov_satz,  ")
_T("aufp.ksys,  aufp.pid,  aufp.auf_klst,  aufp.teil_smt,  aufp.dr_folge,  ")
_T("aufp.inh,  aufp.auf_vk_euro,  aufp.auf_vk_fremd,  aufp.auf_lad_euro,  ")
_T("aufp.auf_lad_fremd,  aufp.rab_satz,  aufp.me_einh_kun,  aufp.me_einh,  ")
_T("aufp.me_einh_kun1,  aufp.auf_me1,  aufp.inh1,  aufp.me_einh_kun2,  ")
_T("aufp.auf_me2,  aufp.inh2,  aufp.me_einh_kun3,  aufp.auf_me3,  aufp.inh3,  ")
_T("aufp.gruppe,  aufp.kond_art,  aufp.a_grund,  aufp.ls_pos_kz,  ")
_T("aufp.posi_ext,  aufp.kun,  aufp.a_ers,  aufp.ls_charge,  aufp.aufschlag,  ")
_T("aufp.aufschlag_wert,  aufp.pos_txt_kz from aufp ")

#line 16 "Aufp.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and auf = ? ")
                                  _T("and posi = ? ")
                                  _T("and a = ? ")
				);
    sqlin ((short *) &aufp.mdn,SQLSHORT,0);
    sqlin ((short *) &aufp.fil,SQLSHORT,0);
    sqlin ((long *) &aufp.auf,SQLLONG,0);
    sqlin ((long *) &aufp.posi,SQLLONG,0);
    sqlin ((long *) &aufp.aufp_txt,SQLLONG,0);
    sqlin ((double *) &aufp.a,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_me,SQLDOUBLE,0);
    sqlin ((TCHAR *) aufp.auf_me_bz,SQLCHAR,7);
    sqlin ((double *) &aufp.lief_me,SQLDOUBLE,0);
    sqlin ((TCHAR *) aufp.lief_me_bz,SQLCHAR,7);
    sqlin ((double *) &aufp.auf_vk_pr,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_lad_pr,SQLDOUBLE,0);
    sqlin ((short *) &aufp.delstatus,SQLSHORT,0);
    sqlin ((short *) &aufp.sa_kz_sint,SQLSHORT,0);
    sqlin ((double *) &aufp.prov_satz,SQLDOUBLE,0);
    sqlin ((long *) &aufp.ksys,SQLLONG,0);
    sqlin ((long *) &aufp.pid,SQLLONG,0);
    sqlin ((long *) &aufp.auf_klst,SQLLONG,0);
    sqlin ((short *) &aufp.teil_smt,SQLSHORT,0);
    sqlin ((short *) &aufp.dr_folge,SQLSHORT,0);
    sqlin ((double *) &aufp.inh,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_vk_euro,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_vk_fremd,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_lad_euro,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_lad_fremd,SQLDOUBLE,0);
    sqlin ((double *) &aufp.rab_satz,SQLDOUBLE,0);
    sqlin ((short *) &aufp.me_einh_kun,SQLSHORT,0);
    sqlin ((short *) &aufp.me_einh,SQLSHORT,0);
    sqlin ((short *) &aufp.me_einh_kun1,SQLSHORT,0);
    sqlin ((double *) &aufp.auf_me1,SQLDOUBLE,0);
    sqlin ((double *) &aufp.inh1,SQLDOUBLE,0);
    sqlin ((short *) &aufp.me_einh_kun2,SQLSHORT,0);
    sqlin ((double *) &aufp.auf_me2,SQLDOUBLE,0);
    sqlin ((double *) &aufp.inh2,SQLDOUBLE,0);
    sqlin ((short *) &aufp.me_einh_kun3,SQLSHORT,0);
    sqlin ((double *) &aufp.auf_me3,SQLDOUBLE,0);
    sqlin ((double *) &aufp.inh3,SQLDOUBLE,0);
    sqlin ((long *) &aufp.gruppe,SQLLONG,0);
    sqlin ((TCHAR *) aufp.kond_art,SQLCHAR,5);
    sqlin ((double *) &aufp.a_grund,SQLDOUBLE,0);
    sqlin ((short *) &aufp.ls_pos_kz,SQLSHORT,0);
    sqlin ((long *) &aufp.posi_ext,SQLLONG,0);
    sqlin ((long *) &aufp.kun,SQLLONG,0);
    sqlin ((double *) &aufp.a_ers,SQLDOUBLE,0);
    sqlin ((TCHAR *) aufp.ls_charge,SQLCHAR,31);
    sqlin ((short *) &aufp.aufschlag,SQLSHORT,0);
    sqlin ((double *) &aufp.aufschlag_wert,SQLDOUBLE,0);
    sqlin ((short *) &aufp.pos_txt_kz,SQLSHORT,0);
            sqltext = _T("update aufp set aufp.mdn = ?,  ")
_T("aufp.fil = ?,  aufp.auf = ?,  aufp.posi = ?,  aufp.aufp_txt = ?,  ")
_T("aufp.a = ?,  aufp.auf_me = ?,  aufp.auf_me_bz = ?,  aufp.lief_me = ?,  ")
_T("aufp.lief_me_bz = ?,  aufp.auf_vk_pr = ?,  aufp.auf_lad_pr = ?,  ")
_T("aufp.delstatus = ?,  aufp.sa_kz_sint = ?,  aufp.prov_satz = ?,  ")
_T("aufp.ksys = ?,  aufp.pid = ?,  aufp.auf_klst = ?,  aufp.teil_smt = ?,  ")
_T("aufp.dr_folge = ?,  aufp.inh = ?,  aufp.auf_vk_euro = ?,  ")
_T("aufp.auf_vk_fremd = ?,  aufp.auf_lad_euro = ?,  ")
_T("aufp.auf_lad_fremd = ?,  aufp.rab_satz = ?,  aufp.me_einh_kun = ?,  ")
_T("aufp.me_einh = ?,  aufp.me_einh_kun1 = ?,  aufp.auf_me1 = ?,  ")
_T("aufp.inh1 = ?,  aufp.me_einh_kun2 = ?,  aufp.auf_me2 = ?,  ")
_T("aufp.inh2 = ?,  aufp.me_einh_kun3 = ?,  aufp.auf_me3 = ?,  ")
_T("aufp.inh3 = ?,  aufp.gruppe = ?,  aufp.kond_art = ?,  ")
_T("aufp.a_grund = ?,  aufp.ls_pos_kz = ?,  aufp.posi_ext = ?,  ")
_T("aufp.kun = ?,  aufp.a_ers = ?,  aufp.ls_charge = ?,  ")
_T("aufp.aufschlag = ?,  aufp.aufschlag_wert = ?,  aufp.pos_txt_kz = ? ")

#line 23 "Aufp.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and auf = ? ")
                                  _T("and posi = ? ")
                                  _T("and a = ? ");
            sqlin ((short *)   &aufp.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &aufp.fil,  SQLSHORT, 0);
            sqlin ((long *)   &aufp.auf,   SQLLONG, 0);
            sqlin ((long *)   &aufp.posi,   SQLLONG, 0);
            sqlin ((double *) &aufp.a,   SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &aufp.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &aufp.fil,  SQLSHORT, 0);
            sqlin ((long *)   &aufp.auf,   SQLLONG, 0);
            sqlin ((long *)   &aufp.posi,   SQLLONG, 0);
            sqlin ((double *) &aufp.a,   SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select auf from aufp ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and auf = ? ")
                                  _T("and posi = ? ")
                                  _T("and a = ? ")
				);
            sqlin ((short *)   &aufp.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &aufp.fil,  SQLSHORT, 0);
            sqlin ((long *)   &aufp.auf,   SQLLONG, 0);
            sqlin ((long *)   &aufp.posi,   SQLLONG, 0);
            sqlin ((double *) &aufp.a,   SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update aufp set delstatus = -1 ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and auf = ? ")
                                  _T("and posi = ? ")
                                  _T("and a = ? ")
				);
            sqlin ((short *)   &aufp.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &aufp.fil,  SQLSHORT, 0);
            sqlin ((long *)   &aufp.auf,   SQLLONG, 0);
            sqlin ((long *)   &aufp.posi,   SQLLONG, 0);
            sqlin ((double *) &aufp.a,   SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from aufp ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and auf = ? ")
                                  _T("and posi = ? ")
                                  _T("and a = ? ")
				);
    sqlin ((short *) &aufp.mdn,SQLSHORT,0);
    sqlin ((short *) &aufp.fil,SQLSHORT,0);
    sqlin ((long *) &aufp.auf,SQLLONG,0);
    sqlin ((long *) &aufp.posi,SQLLONG,0);
    sqlin ((long *) &aufp.aufp_txt,SQLLONG,0);
    sqlin ((double *) &aufp.a,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_me,SQLDOUBLE,0);
    sqlin ((TCHAR *) aufp.auf_me_bz,SQLCHAR,7);
    sqlin ((double *) &aufp.lief_me,SQLDOUBLE,0);
    sqlin ((TCHAR *) aufp.lief_me_bz,SQLCHAR,7);
    sqlin ((double *) &aufp.auf_vk_pr,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_lad_pr,SQLDOUBLE,0);
    sqlin ((short *) &aufp.delstatus,SQLSHORT,0);
    sqlin ((short *) &aufp.sa_kz_sint,SQLSHORT,0);
    sqlin ((double *) &aufp.prov_satz,SQLDOUBLE,0);
    sqlin ((long *) &aufp.ksys,SQLLONG,0);
    sqlin ((long *) &aufp.pid,SQLLONG,0);
    sqlin ((long *) &aufp.auf_klst,SQLLONG,0);
    sqlin ((short *) &aufp.teil_smt,SQLSHORT,0);
    sqlin ((short *) &aufp.dr_folge,SQLSHORT,0);
    sqlin ((double *) &aufp.inh,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_vk_euro,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_vk_fremd,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_lad_euro,SQLDOUBLE,0);
    sqlin ((double *) &aufp.auf_lad_fremd,SQLDOUBLE,0);
    sqlin ((double *) &aufp.rab_satz,SQLDOUBLE,0);
    sqlin ((short *) &aufp.me_einh_kun,SQLSHORT,0);
    sqlin ((short *) &aufp.me_einh,SQLSHORT,0);
    sqlin ((short *) &aufp.me_einh_kun1,SQLSHORT,0);
    sqlin ((double *) &aufp.auf_me1,SQLDOUBLE,0);
    sqlin ((double *) &aufp.inh1,SQLDOUBLE,0);
    sqlin ((short *) &aufp.me_einh_kun2,SQLSHORT,0);
    sqlin ((double *) &aufp.auf_me2,SQLDOUBLE,0);
    sqlin ((double *) &aufp.inh2,SQLDOUBLE,0);
    sqlin ((short *) &aufp.me_einh_kun3,SQLSHORT,0);
    sqlin ((double *) &aufp.auf_me3,SQLDOUBLE,0);
    sqlin ((double *) &aufp.inh3,SQLDOUBLE,0);
    sqlin ((long *) &aufp.gruppe,SQLLONG,0);
    sqlin ((TCHAR *) aufp.kond_art,SQLCHAR,5);
    sqlin ((double *) &aufp.a_grund,SQLDOUBLE,0);
    sqlin ((short *) &aufp.ls_pos_kz,SQLSHORT,0);
    sqlin ((long *) &aufp.posi_ext,SQLLONG,0);
    sqlin ((long *) &aufp.kun,SQLLONG,0);
    sqlin ((double *) &aufp.a_ers,SQLDOUBLE,0);
    sqlin ((TCHAR *) aufp.ls_charge,SQLCHAR,31);
    sqlin ((short *) &aufp.aufschlag,SQLSHORT,0);
    sqlin ((double *) &aufp.aufschlag_wert,SQLDOUBLE,0);
    sqlin ((short *) &aufp.pos_txt_kz,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into aufp (")
_T("mdn,  fil,  auf,  posi,  aufp_txt,  a,  auf_me,  auf_me_bz,  lief_me,  lief_me_bz,  ")
_T("auf_vk_pr,  auf_lad_pr,  delstatus,  sa_kz_sint,  prov_satz,  ksys,  pid,  ")
_T("auf_klst,  teil_smt,  dr_folge,  inh,  auf_vk_euro,  auf_vk_fremd,  ")
_T("auf_lad_euro,  auf_lad_fremd,  rab_satz,  me_einh_kun,  me_einh,  ")
_T("me_einh_kun1,  auf_me1,  inh1,  me_einh_kun2,  auf_me2,  inh2,  me_einh_kun3,  ")
_T("auf_me3,  inh3,  gruppe,  kond_art,  a_grund,  ls_pos_kz,  posi_ext,  kun,  a_ers,  ")
_T("ls_charge,  aufschlag,  aufschlag_wert,  pos_txt_kz) ")

#line 72 "Aufp.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?)"));

#line 74 "Aufp.rpp"
}
