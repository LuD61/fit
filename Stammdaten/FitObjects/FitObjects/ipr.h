#ifndef _IPR_DEF
#define _IPR_DEF

#include "dbclass.h"

struct IPR {
   short          mdn;
   long           pr_gr_stuf;
   long           kun_pr;
   double         a;
   double         vk_pr_i;
   double         vk_pr_eu;
   double         ld_pr;
   double         ld_pr_eu;
   short          aktion_nr;
   TCHAR          a_akt_kz[2];
   TCHAR          modif[2];
   short          waehrung;
   long           kun;
   double         a_grund;
   TCHAR          kond_art[5];
   double         add_ek_proz;
   double         add_ek_abs;
};
extern struct IPR ipr, ipr_null;

#line 7 "ipr.rh"

class IPR_CLASS : public DB_CLASS 
{
       private :
               int cursor_a;
               int cursor_gr;
               int del_a_curs;
               int del_gr_curs; 
               void prepare (void);
               void prepare_a (char *);
               void prepare_gr (char *);
       public :
               IPR_CLASS () : DB_CLASS (),
                                  cursor_a (-1) ,
                                  cursor_gr (-1) ,
                                  del_a_curs (-1) ,
                                  del_gr_curs (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_a (char *);
               int dbread_a (void);
               int dbreadfirst_gr (char *);
               int dbread_gr (void);
               int dbdelete_a (void);
               int dbdelete_gr (void);
               int dbclose_a (void);
               int dbclose_gr (void);
};
#endif
