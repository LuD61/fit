// CAufps.cpp: Implementierung von CCAufps

#include "stdafx.h"
#include "CAufps.h"


// CCAufps

STDMETHODIMP CCAufps::get_A(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = a;
	return S_OK;
}

STDMETHODIMP CCAufps::get_ABz1(BSTR* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = a_bz1;
	return S_OK;
}

STDMETHODIMP CCAufps::get_Posi(LONG* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = posi;
	return S_OK;
}

STDMETHODIMP CCAufps::get_AufVkPr(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = auf_vk_pr;
	return S_OK;
}

STDMETHODIMP CCAufps::get_AufLadPr(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = auf_lad_pr;
	return S_OK;
}

STDMETHODIMP CCAufps::get_AufLadPr0(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = auf_lad_pr0;
	return S_OK;
}

STDMETHODIMP CCAufps::get_AufLadPrPrc(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = auf_lad_pr_prc;
	return S_OK;
}

STDMETHODIMP CCAufps::get_Marge(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = marge;
	return S_OK;
}

STDMETHODIMP CCAufps::get_SaKzSInt(SHORT* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = sa_kz_sint;
	return S_OK;
}

STDMETHODIMP CCAufps::get_AGew(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = a_gew;
	return S_OK;
}

STDMETHODIMP CCAufps::get_DrFolge(SHORT* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = dr_folge;
	return S_OK;
}


STDMETHODIMP CCAufps::get_MeEinh(SHORT* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = me_einh;
	return S_OK;
}

STDMETHODIMP CCAufps::get_Smt(BSTR* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = smt;
	return S_OK;
}


STDMETHODIMP CCAufps::get_BasisMeBz(BSTR* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = basis_me_bz;
	return S_OK;
}

STDMETHODIMP CCAufps::get_MeEinhKun(SHORT* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = me_einh_kun;
	return S_OK;
}

STDMETHODIMP CCAufps::get_MeEinhKun1(SHORT* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = me_einh_kun1;
	return S_OK;
}

STDMETHODIMP CCAufps::get_MeEinhKun2(SHORT* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = me_einh_kun2;
	return S_OK;
}


STDMETHODIMP CCAufps::get_MeEinhKun3(SCODE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = me_einh_kun3;
	return S_OK;
}

STDMETHODIMP CCAufps::get_Inh1(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = inh1;
	return S_OK;
}


STDMETHODIMP CCAufps::get_Inh2(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = inh2;
	return S_OK;
}

STDMETHODIMP CCAufps::get_Inh3(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = inh3;
	return S_OK;
}

STDMETHODIMP CCAufps::get_MeBz(BSTR* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = me_bz;
	return S_OK;
}

STDMETHODIMP CCAufps::get_LastMe(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = last_me;
	return S_OK;
}

STDMETHODIMP CCAufps::get_LastLDat(BSTR* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = last_ldat;
	return S_OK;
}

STDMETHODIMP CCAufps::get_LastPrVk(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = last_pr_vk;
	return S_OK;
}


STDMETHODIMP CCAufps::get_EndOfRecords(VARIANT_BOOL* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = end_of_records;
	return S_OK;
}

STDMETHODIMP CCAufps::get_Bsd(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = bsd;
	return S_OK;
}

STDMETHODIMP CCAufps::get_Bsd2(DOUBLE* pVal)
{
	// TODO: F�gen Sie hier Ihren Implementierungscode ein.

	*pVal = bsd2;
	return S_OK;
}
