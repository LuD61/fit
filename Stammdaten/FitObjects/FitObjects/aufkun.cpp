#include "StdAfx.h"
#include "aufkun.h"

struct AUFKUN aufkun, aufkun_null;

void AUFKUN_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((char *)  &aufkun.mdn, 1, 0);
            sqlin ((char *)  &aufkun.fil, 1, 0);
            sqlin ((char *)  &aufkun.kun_fil, 1, 0);
            sqlin ((char *)   &aufkun.kun, 2, 0);
            sqlin ((char *) &aufkun.a, 3, 0);
            sqlin ((char *)  &aufkun.lfd, 2, 0);
    sqlout ((short *) &aufkun.mdn,SQLSHORT,0);
    sqlout ((short *) &aufkun.fil,SQLSHORT,0);
    sqlout ((long *) &aufkun.kun,SQLLONG,0);
    sqlout ((long *) &aufkun.auf,SQLLONG,0);
    sqlout ((double *) &aufkun.a,SQLDOUBLE,0);
    sqlout ((short *) &aufkun.kun_fil,SQLSHORT,0);
    sqlout ((long *) &aufkun.lfd,SQLLONG,0);
    sqlout ((DATE_STRUCT *) &aufkun.bearb,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &aufkun.lieferdat,SQLDATE,0);
    sqlout ((double *) &aufkun.auf_me,SQLDOUBLE,0);
    sqlout ((double *) &aufkun.lief_me,SQLDOUBLE,0);
    sqlout ((double *) &aufkun.auf_vk_pr,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select aufkun.mdn,  ")
_T("aufkun.fil,  aufkun.kun,  aufkun.auf,  aufkun.a,  aufkun.kun_fil,  ")
_T("aufkun.lfd,  aufkun.bearb,  aufkun.lieferdat,  aufkun.auf_me,  ")
_T("aufkun.lief_me,  aufkun.auf_vk_pr from aufkun ")

#line 17 "aufkun.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and kun_fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and a = ? ")
                                  _T("and lfd = ?"));

            sqlin ((char *) &aufkun.mdn, 1, 0);
            sqlin ((char *) &aufkun.fil, 1, 0);
            sqlin ((char *) &aufkun.kun_fil, 1, 0);
            sqlin ((char *) &aufkun.kun, 2, 0);
            sqlin ((char *) &aufkun.a, 3, 0);
 
    sqlout ((short *) &aufkun.mdn,SQLSHORT,0);
    sqlout ((short *) &aufkun.fil,SQLSHORT,0);
    sqlout ((long *) &aufkun.kun,SQLLONG,0);
    sqlout ((long *) &aufkun.auf,SQLLONG,0);
    sqlout ((double *) &aufkun.a,SQLDOUBLE,0);
    sqlout ((short *) &aufkun.kun_fil,SQLSHORT,0);
    sqlout ((long *) &aufkun.lfd,SQLLONG,0);
    sqlout ((DATE_STRUCT *) &aufkun.bearb,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &aufkun.lieferdat,SQLDATE,0);
    sqlout ((double *) &aufkun.auf_me,SQLDOUBLE,0);
    sqlout ((double *) &aufkun.lief_me,SQLDOUBLE,0);
    sqlout ((double *) &aufkun.auf_vk_pr,SQLDOUBLE,0);
            cursorlast = sqlcursor (_T("select aufkun.mdn,  ")
_T("aufkun.fil,  aufkun.kun,  aufkun.auf,  aufkun.a,  aufkun.kun_fil,  ")
_T("aufkun.lfd,  aufkun.bearb,  aufkun.lieferdat,  aufkun.auf_me,  ")
_T("aufkun.lief_me,  aufkun.auf_vk_pr from aufkun ")

#line 31 "aufkun.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and kun_fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and a = ? ")
                                  _T("order by bearb desc,lfd desc,auf"));

    sqlin ((short *) &aufkun.mdn,SQLSHORT,0);
    sqlin ((short *) &aufkun.fil,SQLSHORT,0);
    sqlin ((long *) &aufkun.kun,SQLLONG,0);
    sqlin ((long *) &aufkun.auf,SQLLONG,0);
    sqlin ((double *) &aufkun.a,SQLDOUBLE,0);
    sqlin ((short *) &aufkun.kun_fil,SQLSHORT,0);
    sqlin ((long *) &aufkun.lfd,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &aufkun.bearb,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &aufkun.lieferdat,SQLDATE,0);
    sqlin ((double *) &aufkun.auf_me,SQLDOUBLE,0);
    sqlin ((double *) &aufkun.lief_me,SQLDOUBLE,0);
    sqlin ((double *) &aufkun.auf_vk_pr,SQLDOUBLE,0);
            sqltext = _T("update aufkun set aufkun.mdn = ?,  ")
_T("aufkun.fil = ?,  aufkun.kun = ?,  aufkun.auf = ?,  aufkun.a = ?,  ")
_T("aufkun.kun_fil = ?,  aufkun.lfd = ?,  aufkun.bearb = ?,  ")
_T("aufkun.lieferdat = ?,  aufkun.auf_me = ?,  aufkun.lief_me = ?,  ")
_T("aufkun.auf_vk_pr = ? ")

#line 39 "aufkun.rpp"
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and kun_fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and a = ? ")
                                  _T("and lfd = ?");

            sqlin ((char *)  &aufkun.mdn, 1, 0);
            sqlin ((char *)  &aufkun.fil, 1, 0);
            sqlin ((char *)  &aufkun.kun_fil, 1, 0);
            sqlin ((char *)  &aufkun.kun, 2, 0);
            sqlin ((char *)  &aufkun.a, 3, 0);
            sqlin ((char *)  &aufkun.lfd, 2, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((char *)  &aufkun.mdn, 1, 0);
            sqlin ((char *)  &aufkun.fil, 1, 0);
            sqlin ((char *)  &aufkun.kun_fil, 1, 0);
            sqlin ((char *)  &aufkun.kun, 2, 0);
            sqlin ((char *)  &aufkun.a, 3, 0);

            test_upd_cursor = sqlcursor (_T("select a from aufkun ")
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and kun_fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and a = ? for update"));



            sqlin ((char *)  &aufkun.mdn, 1, 0);
            sqlin ((char *)  &aufkun.fil, 1, 0);
            sqlin ((char *)  &aufkun.kun_fil, 1, 0);
            sqlin ((char *)   &aufkun.kun, 2, 0);
            sqlin ((char *) &aufkun.a, 3, 0);
            sqlin ((char *)  &aufkun.lfd, 2, 0);
            del_cursor = sqlcursor (_T("delete from aufkun ")
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and kun_fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and a = ? ")
                                  _T("and lfd = ?"));

    sqlin ((short *) &aufkun.mdn,SQLSHORT,0);
    sqlin ((short *) &aufkun.fil,SQLSHORT,0);
    sqlin ((long *) &aufkun.kun,SQLLONG,0);
    sqlin ((long *) &aufkun.auf,SQLLONG,0);
    sqlin ((double *) &aufkun.a,SQLDOUBLE,0);
    sqlin ((short *) &aufkun.kun_fil,SQLSHORT,0);
    sqlin ((long *) &aufkun.lfd,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &aufkun.bearb,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &aufkun.lieferdat,SQLDATE,0);
    sqlin ((double *) &aufkun.auf_me,SQLDOUBLE,0);
    sqlin ((double *) &aufkun.lief_me,SQLDOUBLE,0);
    sqlin ((double *) &aufkun.auf_vk_pr,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into aufkun (")
_T("mdn,  fil,  kun,  auf,  a,  kun_fil,  lfd,  bearb,  lieferdat,  auf_me,  lief_me,  auf_vk_pr) ")

#line 84 "aufkun.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?)")); 

#line 86 "aufkun.rpp"
}
int AUFKUN_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int AUFKUN_CLASS::dbreadlast (void)
/**
Ersten Satz aus Tabelle lesen, absteigend sortiert nach bearb.
**/
{
         int dsqlstatus;

         if (cursorlast == -1)
         {
                this->prepare ();
         }
         dsqlstatus = sqlopen (cursorlast);
         if (dsqlstatus == 0)
         {
                 dsqlstatus = sqlfetch (cursorlast);
         }
         return dsqlstatus;
}

int AUFKUN_CLASS::dbreadnextlast (void)
/**
Naechsten Satz aus Tabelle lesen, absteigend sortiert nach bearb.
**/
{
         int dsqlstatus;

         dsqlstatus = sqlfetch (cursorlast);
         return dsqlstatus;
}
         
int AUFKUN_CLASS::dbupdatelast0 (long saveanz)
/**
Update in aufkun. Nur saveanz Eintraege pro Artikel und Kunde werden gespeichert.
**/
{
         int dsqlstatus;
         long anz; 
         long auf;

         if (cursorlast == -1)
         {
                this->prepare ();
         }
         anz = 0;
         sqlin ((short *) &aufkun.mdn, 1, 0); 
         sqlin ((short *) &aufkun.fil, 1, 0); 
         sqlin ((long *)  &aufkun.kun, 2, 0); 
         sqlin ((short *)  &aufkun.kun_fil, 1, 0); 
         sqlin ((double *)  &aufkun.a,   3, 0); 
         sqlout ((long *) &anz, 2, 0);
         dsqlstatus = sqlcomm (_T("select count (*) from aufkun ")
                               _T("where mdn = ? ")
                               _T("and fil   = ? ")
                               _T("and kun   = ? ")
                               _T("and kun_fil = ? ")
                               _T("and a     = ?"));
         if (dsqlstatus == 100 || anz < saveanz)
         {
                      aufkun.lfd = anz + 1;  
                      return sqlexecute (ins_cursor);
         }
         saveanz = anz; 
         auf = aufkun.auf;
         sqlin ((short *) &aufkun.mdn, 1, 0); 
         sqlin ((short *) &aufkun.fil, 1, 0); 
         sqlin ((long *)  &aufkun.kun, 2, 0); 
         sqlin ((short *)  &aufkun.kun_fil, 1, 0); 
         sqlin ((double *)  &aufkun.a,   3, 0); 
         sqlout ((long *) &aufkun.lfd, 2, 0);
         dsqlstatus = sqlcomm (_T("select lfd,bearb from aufkun ")
                               _T("where mdn = ? ")
                               _T("and fil   = ? ")
                               _T("and kun   = ? ")
                               _T("and kun_fil   = ? ")
                               _T("and a     = ? order by lfd"));
         anz ++;


         sqlin ((long *)   &anz, 2, 0);
         sqlin ((long *)   &auf, 2, 0);
         sqlin ((double *) &aufkun.auf_me, 3, 0);
         sqlin ((double *) &aufkun.lief_me, 3, 0);
         sqlin ((double *) &aufkun.auf_vk_pr, 3, 0);
         sqlin ((long *)   &aufkun.bearb, 2, 0);
         sqlin ((long *)   &aufkun.lieferdat, 2, 0);

         sqlin ((short *)  &aufkun.mdn, 1, 0);
         sqlin ((short *)  &aufkun.fil, 1, 0);
         sqlin ((short *)  &aufkun.kun_fil, 1, 0);
         sqlin ((long *)   &aufkun.kun, 2, 0);
         sqlin ((double *) &aufkun.a, 3, 0);
         sqlin ((long *)   &aufkun.lfd, 2, 0);

         sqlcomm (_T("update aufkun set lfd = ?,auf = ?,auf_me = ?,lief_me = ?,")
                                  _T("auf_vk_pr = ?,")
                                  _T("bearb = ?,lieferdat = ? ")  
                                  _T("where mdn = ? ")
                                  _T("and   fil = ? ")
                                  _T("and kun_fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and a = ? ")
                                  _T("and lfd = ?"));

         sqlin ((short *) &aufkun.mdn, 1, 0); 
         sqlin ((short *) &aufkun.fil, 1, 0); 
         sqlin ((long *)  &aufkun.kun, 2, 0); 
         sqlin ((short *)  &aufkun.kun_fil, 1, 0); 
         sqlin ((double *)  &aufkun.a,   3, 0); 
         sqlcomm (_T("update aufkun set lfd = lfd - 1 ")
                               _T("where mdn = ? ")
                               _T("and fil   = ? ")
                               _T("and kun   = ? ")
                               _T("and kun_fil   = ? ")
                               _T("and a     = ?"));
         return 0;
}

int AUFKUN_CLASS::dbupdatelast (long saveanz)
/**
Update in aufkun. Nur saveanz Eintraege pro Artikel und Kunde werden gespeichert.
**/
{
         int dsqlstatus;
		 extern short sql_mode;
		 short sqls;
		 int i = 0;

		 sqls = sql_mode;
		 sql_mode = 1;
		 dsqlstatus = dbupdatelast0 (saveanz);
		 while (dsqlstatus != 0)
		 {
			 i ++;
		     if (i >= 50) break;
			 Sleep (10);
		     dsqlstatus = dbupdatelast0 (saveanz);
		 }
		 sql_mode = sqls;
		 return dsqlstatus;
}
		 

void AUFKUN_CLASS::dbclose (void)          
/**
Alle Cursor schliessen.
**/
{
         if (cursorlast != -1)
         {
                 sqlclose (cursorlast);
                 cursorlast = -1;
         }
         this->DB_CLASS::dbclose ();  
}



