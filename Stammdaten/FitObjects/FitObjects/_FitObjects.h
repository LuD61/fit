

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Sun Jun 08 18:00:43 2014
 */
/* Compiler settings for _FitObjects.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef ___FitObjects_h__
#define ___FitObjects_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ICAufps_FWD_DEFINED__
#define __ICAufps_FWD_DEFINED__
typedef interface ICAufps ICAufps;
#endif 	/* __ICAufps_FWD_DEFINED__ */


#ifndef __IStndOrder_FWD_DEFINED__
#define __IStndOrder_FWD_DEFINED__
typedef interface IStndOrder IStndOrder;
#endif 	/* __IStndOrder_FWD_DEFINED__ */


#ifndef __CCAufps_FWD_DEFINED__
#define __CCAufps_FWD_DEFINED__

#ifdef __cplusplus
typedef class CCAufps CCAufps;
#else
typedef struct CCAufps CCAufps;
#endif /* __cplusplus */

#endif 	/* __CCAufps_FWD_DEFINED__ */


#ifndef __CStndOrder_FWD_DEFINED__
#define __CStndOrder_FWD_DEFINED__

#ifdef __cplusplus
typedef class CStndOrder CStndOrder;
#else
typedef struct CStndOrder CStndOrder;
#endif /* __cplusplus */

#endif 	/* __CStndOrder_FWD_DEFINED__ */


/* header files for imported files */
#include "docobj.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __ICAufps_INTERFACE_DEFINED__
#define __ICAufps_INTERFACE_DEFINED__

/* interface ICAufps */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ICAufps;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6DFBDF7C-65A0-442E-A9DA-BA9CA23CA613")
    ICAufps : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_A( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ABz1( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Posi( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AufVkPr( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AufLadPr( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AufLadPr0( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SaKzSInt( 
            /* [retval][out] */ SHORT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AGew( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DrFolge( 
            /* [retval][out] */ SHORT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MeEinh( 
            /* [retval][out] */ SHORT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BasisMeBz( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MeEinhKun( 
            /* [retval][out] */ SHORT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MeEinhKun1( 
            /* [retval][out] */ SHORT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MeEinhKun2( 
            /* [retval][out] */ SHORT *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MeEinhKun3( 
            /* [retval][out] */ SCODE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Inh1( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Inh2( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Inh3( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MeBz( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LastMe( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LastLDat( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LastPrVk( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EndOfRecords( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Marge( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AufLadPrPrc( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Bsd( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Bsd2( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Smt( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICAufpsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICAufps * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICAufps * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICAufps * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICAufps * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICAufps * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICAufps * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICAufps * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_A )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ABz1 )( 
            ICAufps * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Posi )( 
            ICAufps * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AufVkPr )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AufLadPr )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AufLadPr0 )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SaKzSInt )( 
            ICAufps * This,
            /* [retval][out] */ SHORT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AGew )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DrFolge )( 
            ICAufps * This,
            /* [retval][out] */ SHORT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MeEinh )( 
            ICAufps * This,
            /* [retval][out] */ SHORT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BasisMeBz )( 
            ICAufps * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MeEinhKun )( 
            ICAufps * This,
            /* [retval][out] */ SHORT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MeEinhKun1 )( 
            ICAufps * This,
            /* [retval][out] */ SHORT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MeEinhKun2 )( 
            ICAufps * This,
            /* [retval][out] */ SHORT *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MeEinhKun3 )( 
            ICAufps * This,
            /* [retval][out] */ SCODE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Inh1 )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Inh2 )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Inh3 )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MeBz )( 
            ICAufps * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LastMe )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LastLDat )( 
            ICAufps * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LastPrVk )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EndOfRecords )( 
            ICAufps * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Marge )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AufLadPrPrc )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Bsd )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Bsd2 )( 
            ICAufps * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Smt )( 
            ICAufps * This,
            /* [retval][out] */ BSTR *pVal);
        
        END_INTERFACE
    } ICAufpsVtbl;

    interface ICAufps
    {
        CONST_VTBL struct ICAufpsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICAufps_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICAufps_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICAufps_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICAufps_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICAufps_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICAufps_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICAufps_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICAufps_get_A(This,pVal)	\
    (This)->lpVtbl -> get_A(This,pVal)

#define ICAufps_get_ABz1(This,pVal)	\
    (This)->lpVtbl -> get_ABz1(This,pVal)

#define ICAufps_get_Posi(This,pVal)	\
    (This)->lpVtbl -> get_Posi(This,pVal)

#define ICAufps_get_AufVkPr(This,pVal)	\
    (This)->lpVtbl -> get_AufVkPr(This,pVal)

#define ICAufps_get_AufLadPr(This,pVal)	\
    (This)->lpVtbl -> get_AufLadPr(This,pVal)

#define ICAufps_get_AufLadPr0(This,pVal)	\
    (This)->lpVtbl -> get_AufLadPr0(This,pVal)

#define ICAufps_get_SaKzSInt(This,pVal)	\
    (This)->lpVtbl -> get_SaKzSInt(This,pVal)

#define ICAufps_get_AGew(This,pVal)	\
    (This)->lpVtbl -> get_AGew(This,pVal)

#define ICAufps_get_DrFolge(This,pVal)	\
    (This)->lpVtbl -> get_DrFolge(This,pVal)

#define ICAufps_get_MeEinh(This,pVal)	\
    (This)->lpVtbl -> get_MeEinh(This,pVal)

#define ICAufps_get_BasisMeBz(This,pVal)	\
    (This)->lpVtbl -> get_BasisMeBz(This,pVal)

#define ICAufps_get_MeEinhKun(This,pVal)	\
    (This)->lpVtbl -> get_MeEinhKun(This,pVal)

#define ICAufps_get_MeEinhKun1(This,pVal)	\
    (This)->lpVtbl -> get_MeEinhKun1(This,pVal)

#define ICAufps_get_MeEinhKun2(This,pVal)	\
    (This)->lpVtbl -> get_MeEinhKun2(This,pVal)

#define ICAufps_get_MeEinhKun3(This,pVal)	\
    (This)->lpVtbl -> get_MeEinhKun3(This,pVal)

#define ICAufps_get_Inh1(This,pVal)	\
    (This)->lpVtbl -> get_Inh1(This,pVal)

#define ICAufps_get_Inh2(This,pVal)	\
    (This)->lpVtbl -> get_Inh2(This,pVal)

#define ICAufps_get_Inh3(This,pVal)	\
    (This)->lpVtbl -> get_Inh3(This,pVal)

#define ICAufps_get_MeBz(This,pVal)	\
    (This)->lpVtbl -> get_MeBz(This,pVal)

#define ICAufps_get_LastMe(This,pVal)	\
    (This)->lpVtbl -> get_LastMe(This,pVal)

#define ICAufps_get_LastLDat(This,pVal)	\
    (This)->lpVtbl -> get_LastLDat(This,pVal)

#define ICAufps_get_LastPrVk(This,pVal)	\
    (This)->lpVtbl -> get_LastPrVk(This,pVal)

#define ICAufps_get_EndOfRecords(This,pVal)	\
    (This)->lpVtbl -> get_EndOfRecords(This,pVal)

#define ICAufps_get_Marge(This,pVal)	\
    (This)->lpVtbl -> get_Marge(This,pVal)

#define ICAufps_get_AufLadPrPrc(This,pVal)	\
    (This)->lpVtbl -> get_AufLadPrPrc(This,pVal)

#define ICAufps_get_Bsd(This,pVal)	\
    (This)->lpVtbl -> get_Bsd(This,pVal)

#define ICAufps_get_Bsd2(This,pVal)	\
    (This)->lpVtbl -> get_Bsd2(This,pVal)

#define ICAufps_get_Smt(This,pVal)	\
    (This)->lpVtbl -> get_Smt(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_A_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_A_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_ABz1_Proxy( 
    ICAufps * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICAufps_get_ABz1_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_Posi_Proxy( 
    ICAufps * This,
    /* [retval][out] */ LONG *pVal);


void __RPC_STUB ICAufps_get_Posi_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_AufVkPr_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_AufVkPr_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_AufLadPr_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_AufLadPr_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_AufLadPr0_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_AufLadPr0_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_SaKzSInt_Proxy( 
    ICAufps * This,
    /* [retval][out] */ SHORT *pVal);


void __RPC_STUB ICAufps_get_SaKzSInt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_AGew_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_AGew_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_DrFolge_Proxy( 
    ICAufps * This,
    /* [retval][out] */ SHORT *pVal);


void __RPC_STUB ICAufps_get_DrFolge_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_MeEinh_Proxy( 
    ICAufps * This,
    /* [retval][out] */ SHORT *pVal);


void __RPC_STUB ICAufps_get_MeEinh_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_BasisMeBz_Proxy( 
    ICAufps * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICAufps_get_BasisMeBz_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_MeEinhKun_Proxy( 
    ICAufps * This,
    /* [retval][out] */ SHORT *pVal);


void __RPC_STUB ICAufps_get_MeEinhKun_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_MeEinhKun1_Proxy( 
    ICAufps * This,
    /* [retval][out] */ SHORT *pVal);


void __RPC_STUB ICAufps_get_MeEinhKun1_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_MeEinhKun2_Proxy( 
    ICAufps * This,
    /* [retval][out] */ SHORT *pVal);


void __RPC_STUB ICAufps_get_MeEinhKun2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_MeEinhKun3_Proxy( 
    ICAufps * This,
    /* [retval][out] */ SCODE *pVal);


void __RPC_STUB ICAufps_get_MeEinhKun3_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_Inh1_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_Inh1_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_Inh2_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_Inh2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_Inh3_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_Inh3_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_MeBz_Proxy( 
    ICAufps * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICAufps_get_MeBz_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_LastMe_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_LastMe_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_LastLDat_Proxy( 
    ICAufps * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICAufps_get_LastLDat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_LastPrVk_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_LastPrVk_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_EndOfRecords_Proxy( 
    ICAufps * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ICAufps_get_EndOfRecords_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_Marge_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_Marge_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_AufLadPrPrc_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_AufLadPrPrc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_Bsd_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_Bsd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_Bsd2_Proxy( 
    ICAufps * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB ICAufps_get_Bsd2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ICAufps_get_Smt_Proxy( 
    ICAufps * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICAufps_get_Smt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICAufps_INTERFACE_DEFINED__ */


#ifndef __IStndOrder_INTERFACE_DEFINED__
#define __IStndOrder_INTERFACE_DEFINED__

/* interface IStndOrder */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IStndOrder;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("39B03882-167E-4B31-A372-9F250C4E3B77")
    IStndOrder : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Mdn( 
            /* [in] */ SHORT newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Fil( 
            /* [in] */ SHORT newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_KunFil( 
            /* [in] */ SHORT newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Kun( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AktAuf( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Name( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetWindow( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Load( 
            /* [in] */ SHORT client) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFirstRecord( 
            /* [in] */ SHORT client,
            /* [retval][out] */ IDispatch **pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextRecord( 
            /* [in] */ SHORT client,
            /* [retval][out] */ IDispatch **pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RegisterClient( 
            /* [retval][out] */ SHORT *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UnregisterClient( 
            /* [in] */ SHORT newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetKeys( 
            /* [in] */ SHORT mdn,
            /* [in] */ SHORT fil,
            /* [in] */ SHORT kun_fil,
            /* [in] */ LONG kun,
            /* [in] */ LONG akt_auf,
            /* [in] */ SHORT client) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SortMode( 
            /* [in] */ SHORT newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RdOptimize( 
            /* [in] */ SHORT newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IStndOrderVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IStndOrder * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IStndOrder * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IStndOrder * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IStndOrder * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IStndOrder * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IStndOrder * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IStndOrder * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Mdn )( 
            IStndOrder * This,
            /* [in] */ SHORT newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Fil )( 
            IStndOrder * This,
            /* [in] */ SHORT newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_KunFil )( 
            IStndOrder * This,
            /* [in] */ SHORT newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Kun )( 
            IStndOrder * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AktAuf )( 
            IStndOrder * This,
            /* [in] */ LONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Name )( 
            IStndOrder * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetWindow )( 
            IStndOrder * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Load )( 
            IStndOrder * This,
            /* [in] */ SHORT client);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFirstRecord )( 
            IStndOrder * This,
            /* [in] */ SHORT client,
            /* [retval][out] */ IDispatch **pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNextRecord )( 
            IStndOrder * This,
            /* [in] */ SHORT client,
            /* [retval][out] */ IDispatch **pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RegisterClient )( 
            IStndOrder * This,
            /* [retval][out] */ SHORT *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UnregisterClient )( 
            IStndOrder * This,
            /* [in] */ SHORT newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetKeys )( 
            IStndOrder * This,
            /* [in] */ SHORT mdn,
            /* [in] */ SHORT fil,
            /* [in] */ SHORT kun_fil,
            /* [in] */ LONG kun,
            /* [in] */ LONG akt_auf,
            /* [in] */ SHORT client);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SortMode )( 
            IStndOrder * This,
            /* [in] */ SHORT newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RdOptimize )( 
            IStndOrder * This,
            /* [in] */ SHORT newVal);
        
        END_INTERFACE
    } IStndOrderVtbl;

    interface IStndOrder
    {
        CONST_VTBL struct IStndOrderVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IStndOrder_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IStndOrder_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IStndOrder_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IStndOrder_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IStndOrder_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IStndOrder_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IStndOrder_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IStndOrder_put_Mdn(This,newVal)	\
    (This)->lpVtbl -> put_Mdn(This,newVal)

#define IStndOrder_put_Fil(This,newVal)	\
    (This)->lpVtbl -> put_Fil(This,newVal)

#define IStndOrder_put_KunFil(This,newVal)	\
    (This)->lpVtbl -> put_KunFil(This,newVal)

#define IStndOrder_put_Kun(This,newVal)	\
    (This)->lpVtbl -> put_Kun(This,newVal)

#define IStndOrder_put_AktAuf(This,newVal)	\
    (This)->lpVtbl -> put_AktAuf(This,newVal)

#define IStndOrder_get_Name(This,pVal)	\
    (This)->lpVtbl -> get_Name(This,pVal)

#define IStndOrder_SetWindow(This)	\
    (This)->lpVtbl -> SetWindow(This)

#define IStndOrder_Load(This,client)	\
    (This)->lpVtbl -> Load(This,client)

#define IStndOrder_GetFirstRecord(This,client,pVal)	\
    (This)->lpVtbl -> GetFirstRecord(This,client,pVal)

#define IStndOrder_GetNextRecord(This,client,pVal)	\
    (This)->lpVtbl -> GetNextRecord(This,client,pVal)

#define IStndOrder_RegisterClient(This,pVal)	\
    (This)->lpVtbl -> RegisterClient(This,pVal)

#define IStndOrder_UnregisterClient(This,newVal)	\
    (This)->lpVtbl -> UnregisterClient(This,newVal)

#define IStndOrder_SetKeys(This,mdn,fil,kun_fil,kun,akt_auf,client)	\
    (This)->lpVtbl -> SetKeys(This,mdn,fil,kun_fil,kun,akt_auf,client)

#define IStndOrder_put_SortMode(This,newVal)	\
    (This)->lpVtbl -> put_SortMode(This,newVal)

#define IStndOrder_put_RdOptimize(This,newVal)	\
    (This)->lpVtbl -> put_RdOptimize(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IStndOrder_put_Mdn_Proxy( 
    IStndOrder * This,
    /* [in] */ SHORT newVal);


void __RPC_STUB IStndOrder_put_Mdn_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IStndOrder_put_Fil_Proxy( 
    IStndOrder * This,
    /* [in] */ SHORT newVal);


void __RPC_STUB IStndOrder_put_Fil_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IStndOrder_put_KunFil_Proxy( 
    IStndOrder * This,
    /* [in] */ SHORT newVal);


void __RPC_STUB IStndOrder_put_KunFil_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IStndOrder_put_Kun_Proxy( 
    IStndOrder * This,
    /* [in] */ LONG newVal);


void __RPC_STUB IStndOrder_put_Kun_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IStndOrder_put_AktAuf_Proxy( 
    IStndOrder * This,
    /* [in] */ LONG newVal);


void __RPC_STUB IStndOrder_put_AktAuf_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IStndOrder_get_Name_Proxy( 
    IStndOrder * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IStndOrder_get_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IStndOrder_SetWindow_Proxy( 
    IStndOrder * This);


void __RPC_STUB IStndOrder_SetWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IStndOrder_Load_Proxy( 
    IStndOrder * This,
    /* [in] */ SHORT client);


void __RPC_STUB IStndOrder_Load_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IStndOrder_GetFirstRecord_Proxy( 
    IStndOrder * This,
    /* [in] */ SHORT client,
    /* [retval][out] */ IDispatch **pVal);


void __RPC_STUB IStndOrder_GetFirstRecord_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IStndOrder_GetNextRecord_Proxy( 
    IStndOrder * This,
    /* [in] */ SHORT client,
    /* [retval][out] */ IDispatch **pVal);


void __RPC_STUB IStndOrder_GetNextRecord_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IStndOrder_RegisterClient_Proxy( 
    IStndOrder * This,
    /* [retval][out] */ SHORT *pVal);


void __RPC_STUB IStndOrder_RegisterClient_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IStndOrder_UnregisterClient_Proxy( 
    IStndOrder * This,
    /* [in] */ SHORT newVal);


void __RPC_STUB IStndOrder_UnregisterClient_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IStndOrder_SetKeys_Proxy( 
    IStndOrder * This,
    /* [in] */ SHORT mdn,
    /* [in] */ SHORT fil,
    /* [in] */ SHORT kun_fil,
    /* [in] */ LONG kun,
    /* [in] */ LONG akt_auf,
    /* [in] */ SHORT client);


void __RPC_STUB IStndOrder_SetKeys_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IStndOrder_put_SortMode_Proxy( 
    IStndOrder * This,
    /* [in] */ SHORT newVal);


void __RPC_STUB IStndOrder_put_SortMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IStndOrder_put_RdOptimize_Proxy( 
    IStndOrder * This,
    /* [in] */ SHORT newVal);


void __RPC_STUB IStndOrder_put_RdOptimize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IStndOrder_INTERFACE_DEFINED__ */



#ifndef __FitObjectsLib_LIBRARY_DEFINED__
#define __FitObjectsLib_LIBRARY_DEFINED__

/* library FitObjectsLib */
/* [helpstring][uuid][version] */ 


EXTERN_C const IID LIBID_FitObjectsLib;

EXTERN_C const CLSID CLSID_CCAufps;

#ifdef __cplusplus

class DECLSPEC_UUID("C0560202-540E-47B6-934F-DE8D0E200E18")
CCAufps;
#endif

EXTERN_C const CLSID CLSID_CStndOrder;

#ifdef __cplusplus

class DECLSPEC_UUID("8E7B5B9C-8AD6-4798-BB2D-3593DD411802")
CStndOrder;
#endif
#endif /* __FitObjectsLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


