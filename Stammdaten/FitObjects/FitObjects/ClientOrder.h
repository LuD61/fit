// ClientOrder.h: Schnittstelle f�r die Klasse CClientOrder.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIENTORDER_H__211724AB_0CF5_4C96_B9F7_78B8C0CEEE2F__INCLUDED_)
#define AFX_CLIENTORDER_H__211724AB_0CF5_4C96_B9F7_78B8C0CEEE2F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "stnd_auf.h"
#include "ListHandler.h"
#include "DataCollection.h"
#include "Aufps.h"
#include "a_hndw.h"
#include "a_eig.h"
#include "mdn.h"
#include "fil.h"
#include "kun.h"
#include "aufkun.h"
/*
#include "aktion.h"
#include "akt_krz.h"
#include "a_pr.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
*/
#include "ptabn.h"
#include "mo_einh.h"
#include "kun.h"
#include "kumebest.h"
#include "DllPreise.h"
#include <string>
#include "GrpPrice.h"
#include "cron_best.h"

using namespace std;

class CClientOrder  
{
/*
public:
	enum STD_SORT
	{
		PosA = 0,
        AgA = 1,
		WgA = 2,
		AgBz = 3,
		WgBz = 4,
	};
*/
private:
	short mdn;
	short fil;
	short kun_fil;
	long kun_nr;
	long akt_auf;
    string lieferdat;
    BOOL LastFromAufKun;
	bool DbIsOpen;
	double inh;
	CString KunBran;

	int cursor;
	short ag;
	short wg;
	char a_bz1 [25];
	char kun_bran2 [4];
	SAUFP_CLASS StndAufp;
	DB_CLASS DbClass;
	EINH_CLASS Einh;
	PTABN_CLASS Ptab;
	A_BAS_CLASS A_bas;
	A_HNDW_CLASS Hndw;
	A_EIG_CLASS Eig;
	CRON_BEST_CLASS CronBest;
	AUFKUN_CLASS AufKun;
	CGrpPrice GrpPrice;
	CListHandler *ListHandler;
	CDataCollection<AUFPS *> *StndAll;
//	WA_PREISE WaPreis;
	CDllPreise DllPreise;
public:
	enum A_TYP
	{
		enumHndw = 1,
        enumEig = 2,
		enumEigDiv = 3,
		enumLeih = 12,
		enumDienst = 13,
	};

	enum PR_KZ
	{
		BsdNoPrice = 0,
		BsdPrice = 1,
	};

	CListHandler *GetListHandler ()
	{
		return ListHandler;
	}

	void SetMdn (short mdn)
	{
		this->mdn = mdn;
	}

	short GetMdn ()
	{
		return mdn;
	}

	void SetFil (short fil)
	{
		this->fil = fil;
	}

	short GetFil ()
	{
		return fil;
	}

	void SetKunFil (short kun_fil)
	{
		this->kun_fil = kun_fil;
	}

	short GetKunFil ()
	{
		return kun_fil;
	}

	void SetKunNr (long kun_nr)
	{
		this->kun_nr = kun_nr;
	}

	long GetKunNr ()
	{
		return kun_nr;
	}

	void SetAktAuf (long akt_auf)
	{
		this->akt_auf = akt_auf;
	}

	long GetAktAuf ()
	{
		return akt_auf;
	}

	void SetLastFromAufKun (BOOL LastFromAufKun)
	{
		this->LastFromAufKun = LastFromAufKun;

	}

	BOOL GetLastFromAufKun ()
	{
		return LastFromAufKun;
	}

	void SetLieferdat (LPSTR lieferdat)
	{
		this->lieferdat = lieferdat;
	}

	CClientOrder();
	virtual ~CClientOrder();
	void SetKeys ();
	void SetKeys (short mdn, short fil, short kun_fil, long kun);
	void Load ();
	BOOL ReadAufps ();
	void WriteAufps ();
	void Prepare ();
	int Read ();
	void FillRow (AUFPS *Aufps);
	void FillKondArt (AUFPS *Aufps);
	void GetLastMe (AUFPS *Aufps);
	void GetLastMeAuf (AUFPS *Aufps);
	void ReadMeEinh (AUFPS *Aufps);
	void CalcLdPrPrc (AUFPS *aufps, double vk_pr, double ld_pr);
};

#endif // !defined(AFX_CLIENTORDER_H__211724AB_0CF5_4C96_B9F7_78B8C0CEEE2F__INCLUDED_)
