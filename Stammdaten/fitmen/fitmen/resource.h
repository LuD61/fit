//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by FIT.RC
//
#define IDD_LCS_MESSEGE                 101
#define IDD_LICENCE                     103
#define IDB_FOLDER                      107
#define IDB_APP                         108
#define IDC_ROW1                        1000
#define IDC_ROW2                        1001
#define IDC_ROW3                        1002
#define IDC_ROW4                        1003
#define IDC_ROW5                        1004
#define IDC_ROW6                        1005
#define IDC_ROW7                        1006
#define IDC_ROW8                        1007
#define IDC_ROW9                        1008
#define IDC_ROW10                       1009
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1020
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
