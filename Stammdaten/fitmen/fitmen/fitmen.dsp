# Microsoft Developer Studio Project File - Name="fitmen" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=fitmen - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "fitmen.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "fitmen.mak" CFG="fitmen - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "fitmen - Win32 Release" (basierend auf  "Win32 (x86) Application")
!MESSAGE "fitmen - Win32 Debug" (basierend auf  "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "fitmen - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "d:\informix\incl\esql" /I "..\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib /nologo /subsystem:windows /machine:I386 /out:"d:\user\fit\bin\fit.exe" /libpath:"d:\informix\lib"

!ELSEIF  "$(CFG)" == "fitmen - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /I "..\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"d:\informix\lib"

!ENDIF 

# Begin Target

# Name "fitmen - Win32 Release"
# Name "fitmen - Win32 Debug"
# Begin Source File

SOURCE=.\arrd.ico
# End Source File
# Begin Source File

SOURCE=.\arrdown.ico
# End Source File
# Begin Source File

SOURCE=.\arrl.ico
# End Source File
# Begin Source File

SOURCE=.\arro.ico
# End Source File
# Begin Source File

SOURCE=.\arrr.ico
# End Source File
# Begin Source File

SOURCE=.\BkBitmap.cpp
# End Source File
# Begin Source File

SOURCE=.\BkBitmap.h
# End Source File
# Begin Source File

SOURCE=.\cmask.cpp
# End Source File
# Begin Source File

SOURCE=.\DATUM.C
# End Source File
# Begin Source File

SOURCE=.\dbclass.cpp
# End Source File
# Begin Source File

SOURCE=.\DBFUNC.CPP
# End Source File
# Begin Source File

SOURCE=.\res\exe.bmp
# End Source File
# Begin Source File

SOURCE=.\FIT.CPP
# End Source File
# Begin Source File

SOURCE=.\FIT.RC

!IF  "$(CFG)" == "fitmen - Win32 Release"

!ELSEIF  "$(CFG)" == "fitmen - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\fit0.ico
# End Source File
# Begin Source File

SOURCE=.\fit01.ico
# End Source File
# Begin Source File

SOURCE=.\fit02.ico
# End Source File
# Begin Source File

SOURCE=.\res\FOLDER.BMP
# End Source File
# Begin Source File

SOURCE=.\res\gear_1.bmp
# End Source File
# Begin Source File

SOURCE=.\handapst.ani
# End Source File
# Begin Source File

SOURCE=.\harrow.cur
# End Source File
# Begin Source File

SOURCE=.\LicenceDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\LicenceDialog.h
# End Source File
# Begin Source File

SOURCE=.\lizenz.cpp
# End Source File
# Begin Source File

SOURCE=.\mhs.ico
# End Source File
# Begin Source File

SOURCE=.\MO_CURSO.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_DRAW.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_EXPL.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_MELD.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_MENU.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_PASSW.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_progcfg.cpp
# End Source File
# Begin Source File

SOURCE=.\res\OpenFolder.bmp
# End Source File
# Begin Source File

SOURCE=.\pfeill.bmp
# End Source File
# Begin Source File

SOURCE=.\Stc.cpp
# End Source File
# Begin Source File

SOURCE=.\Stc.h
# End Source File
# Begin Source File

SOURCE=.\STDFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\STRFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\TextStyle.cpp
# End Source File
# Begin Source File

SOURCE=.\TextStyle.h
# End Source File
# Begin Source File

SOURCE=.\TransHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\TransHandler.h
# End Source File
# Begin Source File

SOURCE=.\WApplication.cpp
# End Source File
# Begin Source File

SOURCE=.\WApplication.h
# End Source File
# Begin Source File

SOURCE=.\WDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\WDialog.h
# End Source File
# Begin Source File

SOURCE=.\WMASK.CPP
# End Source File
# End Target
# End Project
