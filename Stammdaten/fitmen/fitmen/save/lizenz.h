#ifndef _LIZENZ_DEF
#define _LIZENZ_DEF
#include <windows.h>
#include "wmask.h"

class WFORM
{
          public :     
			   WFORM ()
			   {
			   }
			   void ProcessMessages (void);
               static void ShowDlg (HDC, form *);
               static CALLBACK LizenzProc(HWND,UINT, WPARAM,LPARAM);
};
	     
class MESSCLASS : public WFORM
{
          private :
			   HWND hMainWindow;
			   HANDLE hMainInst;
          public :
			  MESSCLASS () : WFORM ()
               {
               }
               HWND OpenWindow (HANDLE, HWND);
               void MessageText (HANDLE, HWND, char **);
               void DestroyText (void);
};

class FITL
{
          private :
               long ldiff;
               char datum [11];
               long ldat;
               char sysdat [11];
               long lsysdat;
			   MESSCLASS Msg;
          public :
			  FITL ()
               {
               }
               long Getldiff (void)
               {
                        return ldiff;
               }
               int TestLizenz (void);
               int TestDatum (char *);
               void LizenzMessage (HANDLE, HWND);
};
#endif
