#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <windows.h>
#include "mo_curso.h"
#include "strfkt.h"
#include "lizenz.h"
#include "wmask.h"

mfont TextFont = {
                 "Times New Roman", 
                 200, 
                 0, 
                 0, 
                 BLACKCOL, 
                 LTGRAYCOL,
                 0, 0};

static char *text[] = {"Ihre Lizenz ist abgelaufen.",
                       " ",
					   "Sofort nach dem Begleichen Ihrer Rechnung erhalten Sie ",
					   "von uns eine neue unbegrenzte Lizenz per Post oder �ber",
					   "Ferndiagnose.",
};

static int tlines = 5;

static form *TextForm;
static HWND TextWindow;

#define BYTES 6


void WFORM::ShowDlg (HDC hdc, form *frm)
/**
Dialogtext anzeigen.
**/
{
	      TEXTMETRIC tm;
          int i;
		  int x, y;
		  HFONT hFont, oldfont;;


          hFont = SetDeviceFont (hdc, frm->font, &tm);

		  oldfont = SelectObject (hdc, hFont);
		  for (i = 0; i < frm->fieldanz; i ++)
		  {
			      x = frm->mask[i].pos[1] * tm.tmAveCharWidth;
				  y = (int) (double) ((double) frm->mask[i].pos[0] * tm.tmHeight * 1.3);
                  SetBkMode (hdc, TRANSPARENT);
				  if (frm->font)
				  {
                            SetTextColor (hdc,frm->font->FontColor);
				  }
				  else
				  {
                            SetTextColor (hdc,BLACKCOL);
				  }
				  if (frm->mask[i].length == 0)
				  {
                            TextOut (hdc, x, y, frm->mask[i].feld, strlen (frm->mask[i].feld));
				  }
				  else
				  {
                            TextOut (hdc, x, y, frm->mask[i].feld, frm->mask[i].length);
				  }
		  }
		  DeleteObject (SelectObject (hdc, oldfont));
}

void WFORM::ProcessMessages (void)
{
	      MSG msg;

          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
				  break;
			  }
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
		  }
}

CALLBACK WFORM::LizenzProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					ShowDlg (hdc, TextForm);
                    EndPaint (hWnd, &ps);
					break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

HWND MESSCLASS::OpenWindow (HANDLE hInstance, HWND hMainWindow)
{
	      TEXTMETRIC tm;
		  int cx, cy, x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  int i;
		  HFONT hFont, oldfont;
		  HDC hdc;
		  SIZE size;
		  RECT rect;

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) LizenzProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "Lizenz";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }
	      
		  cx = 0;
		  hdc = GetDC (hMainWindow);
          hFont = SetDeviceFont (hdc, TextForm->font, &tm);
          oldfont = SelectObject (hdc, hFont); 
		  for (i = 0; i < tlines; i ++)
		  {
              GetTextExtentPoint32 (hdc, text[i], strlen (text[i]) , &size); 
			  if (size.cx > cx)
			  {
				  cx = size.cx;
			  }
		  }
          GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		  cx += 2 * size.cx;
		  DeleteObject (SelectObject (hdc, oldfont));
		  ReleaseDC (hMainWindow, hdc);
		  cy = (int) (double) ((double) tlines  * tm.tmHeight * 1.3);  

		  GetWindowRect (hMainWindow, &rect);
		  x = max (0, (rect.right - rect.left - cx) / 2) + rect.left;
		  y = max (0, (rect.bottom - rect.top - cy) / 2) + rect.top;

          return (CreateWindowEx (0, 
                                 "Lizenz",
                                 "",
                                 WS_VISIBLE | WS_DLGFRAME | WS_POPUP,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL));
}

void MESSCLASS::MessageText (HANDLE hInstance, HWND hWnd, char **text)
{
	      static form *frm;
		  int lines;
		  int i, j;
		  int start;

		  hMainWindow = hWnd;
		  hMainInst = hInstance;
		  frm = (form *) GlobalAlloc (GMEM_FIXED, sizeof (form));
		  if (frm == NULL) return;
		  
	      lines = 10;
		  if (tlines < lines)
		  {
			  lines = tlines;
		  }

		  frm->mask = (field *) GlobalAlloc (GMEM_FIXED, tlines * sizeof (field));
		  if (frm->mask == NULL)
		  {
			  GlobalFree (frm);
			  return;
		  }

		  frm->fieldanz = tlines;
		  frm->frmstart = 0;
		  frm->frmscroll = 0;
          frm->caption = "";
		  frm->before = NULL;
		  frm->after = NULL;
	      frm->cbfield = NULL;
		  frm->font = &TextFont;
		  for (i = 0; i < frm->fieldanz; i ++)
		  {
			  frm->mask[i].length = 0;
			  frm->mask[i].rows = 0;
			  frm->mask[i].pos[0] = i;
			  frm->mask[i].pos[1] = 1;
			  frm->mask[i].feldid = NULL;
			  frm->mask[i].picture = "";
			  frm->mask[i].attribut = DISPLAYONLY;
			  frm->mask[i].before = NULL;
			  frm->mask[i].after = NULL;
			  frm->mask[i].BuId = 0;
		  }
          start = 0;
		  for (i = start, j = 0; j < tlines; i ++, j ++)
		  {
			  frm->mask[j].feld = text[i];
		  }

		  TextForm = frm;
		  EnableWindows (hMainWindow, FALSE);
          MessageBeep (MB_ICONEXCLAMATION);
          TextWindow = OpenWindow (hInstance, hMainWindow);
		  ProcessMessages ();
}



void MESSCLASS::DestroyText (void)
{
 		  EnableWindows (hMainWindow, TRUE);
		  DestroyWindow (TextWindow);
		  GlobalFree (TextForm->mask);
		  GlobalFree (TextForm);
}

        
int FITL::TestDatum (char *buffer)
{
          short tag;
          short mon;
          short jr;
          char rot[4];
          long diff;

          memcpy (&tag, &buffer[0], sizeof (short)); 
          memcpy (&mon, &buffer[2], sizeof (short)); 
          memcpy (&jr,  &buffer[4], sizeof (short)); 
          memset (rot, 0, 4);
          rot[0] = (char) tag;  
          rot[1] = (char) mon;  
          rot[2] = (char) jr;  
          if (strcmp (rot, "rot") == 0) return 0;

          sprintf (datum, "%02hd.%02hd.%04hd", tag,mon,jr);
          sysdate (sysdat);
          ldat    = dasc_to_long (datum);
          lsysdat = dasc_to_long (sysdat);
          diff = lsysdat - ldat;
		  ldiff = diff;
          if (diff > 62) return 3;
          if (diff > 31) return 2;
          return 0;
}

int FITL::TestLizenz (void)
{
          char *serv160;
          FILE *fp;
          char buffer [512];
          int anz;
          int i;

          serv160 = getenv ("SERV160");
          if (serv160 == NULL) return 1;

          anz = wsplit (serv160, "\\");
          if (anz == 0) return 1;

          strcpy (buffer, wort[0]);
 
          for (i = 1; i < anz - 1; i ++)
          {
                  strcat (buffer, "\\");
                  strcat (buffer, wort[i]);
          }
          strcat (buffer, "\\");
          strcat (buffer, "etc\\fit.ini");
          fp = fopen (buffer, "rb");
          if (fp == NULL) return 1;

          if (fread (buffer, 1, BYTES, fp) < BYTES) return 1;
          fclose (fp);
          return (TestDatum (buffer));
}

void FITL::LizenzMessage (HANDLE hInstance, HWND hMainWindow)
{

		  Msg.MessageText (hInstance, hMainWindow, text);
		  Sleep (1000 * (ldiff - 31));
		  Msg.DestroyText ();
}



           
 

          

          
                    
          