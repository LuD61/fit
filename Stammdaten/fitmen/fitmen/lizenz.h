#ifndef _LIZENZ_DEF
#define _LIZENZ_DEF
#include <windows.h>
#include "wmask.h"
#include "cmask.h"
#include "DbClass.h"


class LC
{
public:
	   DB_CLASS DbClass;
	   int cursor;
	   long ldat;
	   short tag;
	   short mon;
	   short jr;
	   long ldat2;

	   LC ()
	   {
			   cursor = -1;
	   }

	   BOOL TestTable ();
	   void prepare ();
	   int dbreadfirst ();
	   void ToBuffer (char *buffer);
};

class FITL
{
          private :
               long ldiff;
               char datum [11];
               long ldat;
               char sysdat [11];
               long lsysdat;
			   MESSCLASS Msg;
			   LC lc;
          public :
			  FITL ()
               {
               }
               long Getldiff (void)
               {
                        return ldiff;
               }
               int TestLizenz (void);
               int TestDatum (char *);
               void LizenzMessage (HANDLE, HWND);
               void LizenzMessage (HANDLE, HWND, char *text[]);
               void LizenzMessage (HANDLE, HWND, char txt[]);
               void DisableLizenz (HANDLE, HWND);
               void Processes (HANDLE, HWND);
};
#endif
