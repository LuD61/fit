#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#ifndef CONSOLE
#include "wmaskc.h"
#include "mo_meld.h"
#include "dbclass.h"
#include "dbfunc.h"
#endif
#ifdef MO_CURSC
#include "mo_cursc.h"
#else
#include "mo_curso.h"
#endif
#include "strfkt.h"
#include "mdn.h"
#include "fil.h"

struct FIL _fil, _fil_null;
/* Variablen fuer Auswahl mit Buttonueberschrift    */

static DB_CLASS DbClass;
#include "itemc.h"
#define MAXSORT 30000
static long MaxSort;

static int dosort1 ();
static int dosort2 ();

struct SORT_AW
{
	     char sort1 [10];
		 char sort2 [25];
		 int  sortidx;
};

//static struct SORT_AW sort_aw, sort_awtab[MAXSORT];
static struct SORT_AW sort_aw, *sort_awtab;

static void SortMalloc (void) 
/**
Speicher fuer Listbereich zuordnen.
**/
{
	static BOOL SortOK = FALSE;
    long anz;

	if (SortOK) return;

	DbClass.sqlout ((long *) &anz, 2, 0);
	DbClass.sqlcomm ("select count (*) from fil");

	if (anz == 0l) return;

    sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   ((anz + 1) * sizeof (struct SORT_AW))); 
	while (sort_awtab == NULL)
	{
		if (anz == 0l) break;
		anz --;
        sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   (anz * sizeof (struct SORT_AW))); 
	}
	MaxSort = anz + 1;
	if (anz == 0l)
	{
		print_mess (2, "Es konnte kein Speicher fuer die Artikelauswahl zugewiesen werden");
		return;
	}
	SortOK = TRUE;
}


static int sort_awanz;
static int sort1 = -1;
static int sort2 = -1;
static int sort_idx = 0;

static ITEM isort1    ("", sort_aw.sort1,    "", 0);
static ITEM isort2    ("", sort_aw.sort2, "", 0);

static field _fsort_aw[] = {
&isort1,      6, 1, 0, 7, 0, "%4d", NORMAL, 0, 0, 0,
&isort2,     24, 1, 0,14, 0, "",    NORMAL, 0, 0, 0};

static form fsort_aw = {2, 0, 0, _fsort_aw, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "Filiale",  "", 0);
static ITEM isort2ub    ("", "Name",     "", 0);
static ITEM ifillub     ("", " ",             "", 0); 

static field _fsort_awub[] = {
&isort1ub,       13, 1, 0, 0, 0, "", BUTTON, 0, dosort1,    102,
&isort2ub,       25, 1, 0,13, 0, "", BUTTON, 0, dosort2,    103,
&ifillub,        80, 1, 0,38, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {3, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0, 13, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 38, 0, "", NORMAL, 0, 0, 0};

static form fsort_awl = {2, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};

static int dosort10 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return ((int) (atoi(el1->sort1) - atoi (el2->sort1)) * 
				                                  sort1);
}


int dosort1 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort10);
	sort1 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static int dosort20 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return (strcmp (el1->sort2,el2->sort2) * sort2);
}


int dosort2 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort20);
	sort2 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        sort_idx = idx;
        break_list ();
        return;
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

/* Ende Variablen fuer Auswahl mit Buttonueberschrift    */


static void FillUb  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%7s %-16s",
                          "Filiale", "Name");
}

static void FillValues  (char *buffer)
/**
Werte fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%7hd %-16.16s",
                          _fil.fil, _adr.adr_krz);
}


int FIL_CLASS::lese_fil (short mdn, short fil)
/**
Tabelle mdn fuer Mandant mdn lesen.
**/
{
         if (cursor_fil == -1)
         {
                ins_quest ((char *) &_fil.mdn, 1, 0);
                ins_quest ((char *) &_fil.fil, 1, 0);
                out_quest ((char *) &_fil.adr, 2, 0);
                out_quest ((char *) _adr.adr_krz, 0, 17);
                out_quest ((char *) &_fil.fil_gr, 1, 0);
                out_quest ((char *) _fil.smt_kz, 0, 2);
                out_quest ((char *) &_fil.tou, 2, 0);
                out_quest ((char *) &_fil.kun, 2, 0);
                out_quest ((char *) &_fil.verr_mdn, 2, 0);
                cursor_fil =
                    prepare_sql ("select fil.adr,adr.adr_krz, fil.fil_gr, smt_kz, tou, "
								 "kun, verr_mdn " 
                                 "from fil,adr "
                                 "where fil.mdn = ? "
                                 "and fil.fil = ? "
                                 "and fil.adr = adr.adr");
         }
         _fil.mdn = mdn;
         _fil.fil = fil;
         open_sql (cursor_fil);
         fetch_sql (cursor_fil);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int FIL_CLASS::lese_fil (void)
/**
Naechsten Satz aus Tabelle mdn lesen.
**/
{
         if (cursor_fil != -1) fetch_sql (cursor_fil);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int FIL_CLASS::lese_fil_gr (short mdn, short fil_gr)
/**
Tabelle fil fuer Filialgruppe mdn lesen.
**/
{
         if (cursor_fil_gr == -1)
         {
                ins_quest ((char *) &_fil.mdn, 1, 0);
                ins_quest ((char *) &_fil.fil_gr, 1, 0);
                out_quest ((char *) _adr.adr_krz, 0, 17);
                out_quest ((char *) &_fil.fil, 1, 0);
                cursor_fil_gr =
                    prepare_sql ("select adr.adr_krz, fil.fil "
                                 "from fil,adr "
                                 "where fil.mdn = ? "
                                 "and fil.fil_gr = ? "
                                 "and fil.adr = adr.adr");
         }
         _fil.mdn = mdn;
         _fil.fil_gr = fil_gr;
         open_sql (cursor_fil_gr);
         fetch_sql (cursor_fil_gr);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int FIL_CLASS::lese_fil_gr (void)
/**
Naechsten Satz aus Tabelle mdn lesen.
**/
{
         if (cursor_fil_gr != -1) fetch_sql (cursor_fil_gr);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}


void FIL_CLASS::close_fil (void)
/**
Cursor fil schliessen.
**/
{
          if (cursor_fil == -1) return;
          close_sql (cursor_fil);
          cursor_fil = -1;
}


void FIL_CLASS::close_fil_gr (void)
/**
Cursor fil_gr schliessen.
**/
{
          if (cursor_fil_gr == -1) return;
          close_sql (cursor_fil_gr);
          cursor_fil_gr = -1;
}

#ifndef CONSOLE

int FIL_CLASS::ShowAllFil (short mdn)
/**
Auswahl ueber Mandanten anzeigen.
**/
{
         int cursor_ausw;
         int i;

         ins_quest ((char *) &_fil.mdn, 1, 0);
         out_quest ((char *) &_fil.fil, 1, 0);
         out_quest (_adr.adr_krz, 0, 16);

         cursor_ausw = prepare_scroll ("select fil.fil, adr.adr_krz from "
                                       "fil,adr "
                                       "where fil.fil > 0 "
                                       "and fil.mdn = ? "
                                       "and adr.adr = fil.adr "
                                       "order by fil");
         if (sqlstatus)
         {
                     return (-1);
         }
         _fil.mdn = mdn;
         i = DbAuswahl (cursor_ausw, FillUb,
                                     FillValues);
         UpdateFkt ();
         SetFocus (current_form->mask[currentfield].feldid);

         SetCurrentField (currentfield);

         if (syskey == KEYESC || syskey == KEY5)
         {
                     close_sql (cursor_ausw);
                     return 0;
         }
         fetch_scroll (cursor_ausw, DBABSOLUTE, i + 1);
         close_sql (cursor_ausw);
         return 0;
}

int FIL_CLASS::ShowAllBu (HWND hWnd, int ws_flag, short mdn)
/**
Auswahl ueber Gruppen anzeigen.
**/
{
        int cursor_ausw;
		HWND eWindow;
	    form *savecurrent;

		SortMalloc ();
	    savecurrent = current_form;
        ins_quest ((char *) &_fil.mdn, 1, 0);
        out_quest ((char *) &_fil.fil, 1, 0);
        out_quest (_adr.adr_krz, 0, 16);

        cursor_ausw = prepare_scroll ("select fil.fil, adr.adr_krz from "
                                       "fil,adr "
                                       "where fil.fil > 0 "
                                       "and fil.mdn = ? "
                                       "and adr.adr = fil.adr "
                                       "order by fil");
        if (sqlstatus)
        {
                     return (-1);
        }

        _fil.mdn = mdn;
 		sort_awanz = 0;
		while (fetch_scroll (cursor_ausw, NEXT) == 0)
        {
			         sprintf (sort_awtab[sort_awanz].sort1, "%d",
						      _fil.fil);
					 strcpy (sort_awtab[sort_awanz].sort2, 
						     _adr.adr_krz);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					 if (sort_awanz == MAXSORT) break;
        }

		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
        eWindow = OpenListWindowEnF (10, 40, 8, 20, 0);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseControls (&fsort_awub); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
		 sort_idx = sort_awtab[sort_idx].sortidx;
         fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
         close_sql (cursor_ausw);
         return 0;
}


int FIL_CLASS::ShowAllBu (HWND hWnd, int ws_flag)
/**
Auswahl ueber Gruppen anzeigen.
**/
{
        int cursor_ausw;
		HWND eWindow;
	    form *savecurrent;

		SortMalloc ();
	    savecurrent = current_form;
        out_quest ((char *) &_fil.fil, 1, 0);
        out_quest (_adr.adr_krz, 0, 16);

        cursor_ausw = prepare_scroll ("select fil.fil, adr.adr_krz from "
                                       "fil,adr "
                                       "where fil.fil > 0 "
                                       "and adr.adr = fil.adr "
                                       "order by fil");
        if (sqlstatus)
        {
                     return (-1);
        }

 		sort_awanz = 0;
		while (fetch_scroll (cursor_ausw, NEXT) == 0)
        {
			         sprintf (sort_awtab[sort_awanz].sort1, "%d",
						      _fil.fil);
					 strcpy (sort_awtab[sort_awanz].sort2, 
						     _adr.adr_krz);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					 if (sort_awanz == MAXSORT) break;
        }

		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
        eWindow = OpenListWindowEnF (10, 40, 8, 20, 0);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseControls (&fsort_awub); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
		 sort_idx = sort_awtab[sort_idx].sortidx;
         fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
         close_sql (cursor_ausw);
         return 0;
}


int FIL_CLASS::ShowBuQuery (HWND hWnd, int ws_flag) 
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;

		SortMalloc ();
	    savecurrent = current_form;
 		sort_awanz = 0;
		while (fetch_scroll (cursor_ausw, NEXT) == 0)
        {
			         sprintf (sort_awtab[sort_awanz].sort1, "%hd",
						      _fil.fil);
				 strcpy (sort_awtab[sort_awanz].sort2, 
						     _adr.adr_krz);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					 if (sort_awanz == MAXSORT) break;
        }

		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
        eWindow = OpenListWindowEnF (10, 40, 8, 20, 0);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
		Setlistenter (1);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
		 sort_idx = sort_awtab[sort_idx].sortidx;
         fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
         close_sql (cursor_ausw);
         return 0;
}

int FIL_CLASS::PrepareQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 

         ReadQuery (qform, qnamen); 

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select fil.fil,adr.adr_krz "
							"from fil,adr "
                            "where fil.mdn = ? "
                            "and %s "
                            "and adr.adr = fil.adr "
							"order by fil.fil", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                                       "select fil.fil, adr.adr_krz from "
                                       "fil,adr "
                                       "where fil.fil > 0 "
                                       "and fil.mdn = ? "
                                       "and adr.adr = fil.adr "
                                       "order by fil.fil");
         }
         ins_quest ((char *) &_fil.mdn, 1, 0);
         out_quest ((char *) &_fil.fil, 1, 0);
         out_quest ((char *) _adr.adr_krz, 0, 17);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prepare_scroll (sqlstring);
         sql_mode = old_mode;
         if (sqlstatus < 0)
         {
                       return (sqlstatus);
         }
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}

#endif // CONSOLE
