#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbfunc.h"


static char *QueryTab;

void SetQueryTab (char *Tab)
/**
Aktuelle Query-Tabelle setzen.
**/
{
	       QueryTab = Tab;
}


char qstring[0x1000];

int (*DbAwSpez) (short,void (*) (char *), void (*) (char *)) = NULL;

void SetDbAwSpez (int
              (*AwSpez) (short, void (*) (char *), void (*) (char *)))
/**
DbAwSpez setzen.
**/
{
           DbAwSpez = AwSpez;
}

static BOOL IsDate (char *dat)
/**
Dateum pruefen.
**/
{
    char datum [12];

    char tag [3];
    char mon [3];
    char jr  [5];
    BOOL IsSchalt;


    if (!numeric (dat)) return FALSE;

    if (dasc_to_long (dat) <= 0l) return FALSE;

    dlong_to_asc (dasc_to_long (dat), datum);
    memcpy (tag, datum, 2);
    tag[2] = (char) 0;
    memcpy (mon, &datum[3], 2);
    mon[2] = (char) 0;
    strcpy (jr, &datum[6]);
    if (!numeric (tag)) return FALSE;
    if (!numeric (mon)) return FALSE;
    if (!numeric (jr)) return FALSE;


    if (atoi (mon) > 12) return FALSE; 
    if (atoi (mon) < 0) return FALSE; 
    if (atoi (tag) < 0) return FALSE; 

    if ((atoi (jr) % 4) == 0)
    {
        IsSchalt = TRUE;
    }
    else
    {
        IsSchalt = FALSE;
    }

    switch (atoi (mon))
    {
           case 1 :
           case 3 :
           case 5 :
           case 7 :
           case 8 :
           case 10 :
           case 12 :
                 if (atoi (tag) > 31) 
                 {
                      return FALSE; 
                 }
                 else
                 {
                      return TRUE; 
                 }
           case 4 :
           case 6 :
           case 9 :
           case 11:
                 if (atoi (tag) > 30)
                 {
                      return FALSE; 
                 }
                 else
                 {
                      return TRUE; 
                 }
           case 2 :
                 if (atoi (tag) > 29)
                 {
                      return FALSE; 
                 }
                 else if (IsSchalt == 0 && atoi (tag) > 28)
                 {
                      return FALSE; 
                 }
                 else
                 {
                      return TRUE; 
                 }
    }
    return TRUE;
}

static short col_cursor = -1;
static short col_cursort = -1;
static char aktfeld [20];
static char aktfeld0 [20];
static char akttab [20];
static int len;
static short typ;
static int quotOK = 0;
char *p;

int testvalue (char *colname, char *value)
/**
**/
{

           if (col_cursor == -1)
           {
                     ins_quest ((char *) aktfeld, 0, 20);
                     out_quest ((char *) &typ, 1, 0);
                     col_cursor = prepare_sql ("select coltype from "
                                               "syscolumns "
                                               "where colname = ?");
                     ins_quest ((char *) akttab, 0, 20);
                     ins_quest ((char *) aktfeld, 0, 20);
                     out_quest ((char *) &typ, 1, 0);
                     col_cursort = prepare_sql ("select coltype from "
                                               "syscolumns,systables "
                                               "where systables.tabname = ? "
											   "and syscolumns.colname = ? "
											   "and syscolumns.tabid = systables.tabid");
           }
           if (strlen (value) < 1) return FALSE;
           typ = 1;
           memset (aktfeld, ' ', 19);
           aktfeld [19] = (char) 0;
           p = strchr (colname, '.');
           if (p == NULL)
           {
                  memcpy (aktfeld, colname, strlen (colname));
                  open_sql (col_cursor);
                  int dsqlstatus = fetch_sql (col_cursor);
           }
           else
           {
			      len = (int) (p - colname);
			      memcpy (akttab, colname, len);
				  akttab [len] = (char) 0;
				  clipped (akttab);
                  memcpy (aktfeld, p + 1, strlen (p + 1));
                  open_sql (col_cursort);
                  fetch_sql (col_cursort);
           }
           if (sqlstatus) return FALSE;
           switch (typ)
           {
                case 0 :
                        return TRUE;
                case 1 :
                case 2 :
                case 6 :
                         if (strchr (value, '.')) return FALSE;  
                         if (strchr (value, ',')) return FALSE;  
                case 3 :
                case 4 :
                case 5 :
                case 8 :
                         if (numeric (value)) 
                         {
                                return TRUE;
                         }
                         else
                         {
                                  return FALSE;
                         } 
                case 7 :
                         if (IsDate (value))
                         {
                                  return 7;
                         }
                         else
                         {
                                  return FALSE;
                         }
           }
           return FALSE;
}

                   

void testquot (char *qfeld, char *colname)
/**
Typ der Feldes Testen. Wenn 0 (Character) oder 7 (date)
Anfuehrungszeichen setzen.
**/
{
 
           if (col_cursor == -1)
           {
                     ins_quest ((char *) aktfeld, 0, 20);
                     out_quest ((char *) &typ, 1, 0);
                     col_cursor = prepare_sql ("select coltype from "
                                               "syscolumns "
                                               "where colname = ?");
                     ins_quest ((char *) akttab, 0, 20);
                     ins_quest ((char *) aktfeld, 0, 20);
                     out_quest ((char *) &typ, 1, 0);
                     col_cursort = prepare_sql ("select coltype from "
                                               "syscolumns,systables "
                                               "where systables.tabname = ? "
											   "and syscolumns.colname = ? "
											   "and syscolumns.tabid = systables.tabid");
           }
                     
           memset (aktfeld, ' ', 19);
           aktfeld [19] = (char) 0;
           if (strcmp (colname, aktfeld0) == 0)
           {
                     if (quotOK == 0) return;
                     strcat (qfeld, "\"");
                     return;
           }
           strcpy (aktfeld0, colname);
           typ = 1;
           p = strchr (colname, '.');
           if (p == NULL)
           {
                  memcpy (aktfeld, colname, strlen (colname));
                  open_sql (col_cursor);
                  fetch_sql (col_cursor);
           }
           else
           {
			      len = (int) (p - colname);
			      memcpy (akttab, colname, len);
				  akttab [len] = (char) 0;
				  clipped (akttab);
                  memcpy (aktfeld, p + 1, strlen (p + 1));
                  open_sql (col_cursort);
                  fetch_sql (col_cursort);
           }
           if (sqlstatus) return;

           if (typ == 0 || typ == 7)
           {
                      quotOK = 1;
           }
           else
           {
                      quotOK = 0;
           }
           if (quotOK) strcat (qfeld, "\"");
}

                                 
void testmatch (char *qfeld, char *colname)
/**
Typ der Feldes Testen. Wenn 0 (Character) oder 7 (date)
Anfuehrungszeichen setzen.
**/
{
 
           if (col_cursor == -1)
           {
                     ins_quest ((char *) aktfeld, 0, 20);
                     out_quest ((char *) &typ, 1, 0);
                     col_cursor = prepare_sql ("select coltype from "
                                               "syscolumns "
                                               "where colname = ?");
                     ins_quest ((char *) akttab, 0, 20);
                     ins_quest ((char *) aktfeld, 0, 20);
                     out_quest ((char *) &typ, 1, 0);
                     col_cursort = prepare_sql ("select coltype from "
                                               "syscolumns,systables "
                                               "where systables.tabname = ? "
											   "and syscolumns.colname = ? "
											   "and syscolumns.tabid = systables.tabid");
           }
                     
           memset (aktfeld, ' ', 19);
           aktfeld [19] = (char) 0;
           if (strcmp (colname, aktfeld0) == 0)
           {
                     if (quotOK == 0) return;
                     strcat (qfeld, "*\"");
                     return;
           }
           strcpy (aktfeld0, colname);
           typ = 1;
           p = strchr (colname, '.');
           if (p == NULL)
           {
                  memcpy (aktfeld, colname, strlen (colname));
                  open_sql (col_cursor);
                  fetch_sql (col_cursor);
           }
           else
           {
			      len = (int) (p - colname);
			      memcpy (akttab, colname, len);
				  akttab [len] = (char) 0;
				  clipped (akttab);
                  memcpy (aktfeld, p + 1, strlen (p + 1));
                  open_sql (col_cursort);
                  fetch_sql (col_cursort);
           }
           if (sqlstatus) return;

           if (typ == 0 || typ == 7)
           {
//                      strcat (qfeld, "matches \"*");
                      strcat (qfeld, "matches \"");
                      quotOK = 1;
           }
           else
           {
                      strcat (qfeld, " = ");
                      quotOK = 0;
           }
}
                                 

void TestQueryField (char *qfeld, char *qname)
/**
Ein Query-Feld testen.
**/
{
         int anz;
         int i;
         char *pos;
         char *oldend;
		 int typ;

         if (qname == NULL) return;
		 strcpy (aktfeld0, "");
         oldend = &qstring [strlen (qstring)];
         clipped (qfeld);
         if (strlen (qfeld) == 0) return;

         if (memcmp (qfeld, " ", 1) <= 0) return;

         if (strlen (qstring))
         {
                    strcat (qstring, " and ");
         }
         strcat (qstring, qname);
         strcat (qstring, " ");
         if (pos = strchr (qfeld, '=')) 
         {
                    if (testvalue (qname, pos + 1) == FALSE)
                    {
                        *oldend = (char) 0;
                        return;
                    }
                    strcat (qstring, "= ");
                    testquot (qstring, qname);
                    strcat (qstring, pos + 1);
                    testquot (qstring, qname);
         }
         else if (pos = strchr (qfeld, '>')) 
         {
                    if (testvalue (qname, pos + 1) == FALSE)
                    {
                        *oldend = (char) 0;
                        return;
                    }
                    strcat (qstring, "> ");
                    testquot (qstring, qname);
                    strcat (qstring, pos + 1);
                    testquot (qstring, qname);
         }
         else if (pos = strchr (qfeld, '<')) 
         {
                    if (testvalue (qname, pos + 1) == FALSE)
                    {
                        *oldend = (char) 0;
                        return;
                    }
                    strcat (qstring, "< ");
                    testquot (qstring, qname);
                    strcat (qstring, pos + 1);
                    testquot (qstring, qname);
         }
         else if (strchr (qfeld, '*')) 
         {
                    strcat (qstring, "matches \"");
                    strcat (qstring, qfeld);
                    strcat (qstring, "\"");
         }
         else if (strchr (qfeld, ':'))
         {
                    anz = zsplit (qfeld, ':');
                    if (anz > 1)
                    {
                            strcat (qstring, "between ");
                            testquot (qstring, qname);
                            strcat (qstring, zwort[1]);
                            testquot (qstring, qname);
                            strcat (qstring, " and ");
                            testquot (qstring, qname);
                            strcat (qstring, zwort[2]);
                            testquot (qstring, qname);
                    }
                    else
                    {
                        *oldend = (char) 0;
                        return;
                    }
         }
         else if (strchr (qfeld, ','))
         {
                    anz = zsplit (qfeld, ',');
                    if (anz > 1)
                    {
                            strcat (qstring, "in (");
                            strcat (qstring, zwort[1]);
                            for (i = 2; i <= anz; i ++)
                            {
                                  strcat (qstring, ",");
                                  strcat (qstring, zwort[i]);
                            }
                            strcat (qstring, ")");
                    }
                    else
                    {
                        *oldend = (char) 0;
                        return;
                    }
         }
         else 
         {
                    typ = testvalue (qname, qfeld);
                    if (typ == FALSE)
                    {
                        *oldend = (char) 0;
                        return;
                    }
					if (typ == 7)
					{
                              strcat (qstring, " = ");
                              testquot (qstring, qname);
                              strcat (qstring, qfeld);
                              testquot (qstring, qname);
							  return;
					}
                    testmatch (qstring, qname);
                    strcat (qstring, qfeld);
                    testmatch (qstring, qname);
         }
}

int StatusQuery ()
/**
Query Eingabe testen.
**/
{
         if (strlen (qstring))
         {
                      return TRUE;
         }
         return FALSE;
}

#ifndef _CONSOLE
void ReadQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         int i;

         qstring[0] = (char) 0;

		 if (qform == NULL || qnamen == NULL) return;

         for (i = 0; i < qform->fieldanz; i ++)
         {
                   TestQueryField (qform->mask[i].item->GetFeldPtr (),
                                   qnamen[i]);
         }
}

int DbAuswahl (short cursor, void (*fillub) (char *),
               void (*fillvalues) (char *))
/**
Auswahl mit Scroll-Cursor azeigen.
**/
{
         int i;
         char buffer [256];

         if (DbAwSpez)
         {
                   return ((*DbAwSpez) (cursor, fillub, fillvalues));
         }
         if (fillvalues == NULL) return -1;

         SaveMenue ();
         InitMenue ();
         OpenListWindowBu (10, 40, 8, 20, 1);
         if (fillub != NULL)
         {
                      (*fillub) (buffer);
                      InsertListUb (buffer);
         }
         while (fetch_scroll (cursor, NEXT) == 0)
         {
                      (*fillvalues) (buffer);
                      InsertListRow (buffer);
         }
         i = EnterQueryListBox ();
         RestoreMenue ();
         SetLbox ();
         // RefreshLbox ();
         return i;
}

int DbAuswahlEx (short cursor, void (*fillub) (char *),
                 void (*fillvalues) (char *), 
				 form *VlForm)
/**
Auswahl mit Scroll-Cursor azeigen.
**/
{
         int i;
         char buffer [256];

         if (DbAwSpez)
         {
                   return ((*DbAwSpez) (cursor, fillub, fillvalues));
         }
         if (fillvalues == NULL) return -1;

         SaveMenue ();
         InitMenue ();
		 SetHLines (0);
		 SetVLines (1);
         OpenListWindowBu (10, 40, 8, 20, 1);
         if (fillub != NULL)
         {
                      (*fillub) (buffer);
                      InsertListUb (buffer);
         }
         if (VlForm != NULL)
         {
                      ElistVl (VlForm);
		              SetVLines (1);
         }
         while (fetch_scroll (cursor, NEXT) == 0)
         {
                      (*fillvalues) (buffer);
                      InsertListRow (buffer);
         }
         i = EnterQueryListBox ();
         RestoreMenue ();
         SetLbox ();
         // RefreshLbox ();
         return i;
}
#endif
