#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#ifndef CONSOLE
#include "wmaskc.h"
#include "mo_meld.h"
#endif
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "strfkt.h"


int DB_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         open_sql (cursor);
         fetch_sql (cursor);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int DB_CLASS::dbread (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         fetch_sql (cursor);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int DB_CLASS::dbupdate (void)
/**
Tabelle eti Updaten.
**/
{

         open_sql (test_upd_cursor);
         fetch_sql (test_upd_cursor);
         if (sqlstatus == 100)
         {
                   execute_curs (ins_cursor);
         }  
         else if (sqlstatus == 0)
         {
                   execute_curs (upd_cursor);
         }  
          
         return 0;
} 

int DB_CLASS::dblock (void)
/**
Tabelle eti Updaten.
**/
{

         open_sql (test_upd_cursor);
         fetch_sql (test_upd_cursor);
          
         return sqlstatus;
} 

int DB_CLASS::dbdelete (void)
/**
Tabelle eti lesen.
**/
{

         open_sql (test_upd_cursor);
         fetch_sql (test_upd_cursor);
         if (sqlstatus == 0)
         {
                      execute_curs (del_cursor);
         }
         return 0;
}

void DB_CLASS::dbclose (void)
/**
Cursor fuer eti schliessen.
**/
{
         if (cursor == -1) return;

         close_sql (cursor); 
         close_sql (upd_cursor); 
         close_sql (ins_cursor); 
         close_sql (del_cursor); 
         close_sql (test_upd_cursor);

         cursor = -1;
         upd_cursor = -1;
         ins_cursor = -1;
         del_cursor = -1;
         test_upd_cursor = -1;
         cursor_ausw = -1;
}

int DB_CLASS::dbmove (int mode)
/**
Scroll-Cursor lesen.
**/
{
         int scrollakt;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }

         scrollakt = scrollpos;
         switch (mode)
         {
             case FIRST :
                         scrollpos = 1;
                         break;
             case NEXT :     
                         scrollpos ++;
                         break;
             case PRIOR :
                         if (scrollpos > 1)
                         {
                                 scrollpos --;
                         }
                         break;
//             case LAST :      
             case CURRENT :
                         break;
             default :
                   return (-1);
         }
         fetch_scroll (cursor_ausw, mode);
         if (sqlstatus != 0)
         {
             scrollpos = scrollakt;
         }
         return (sqlstatus);
}

int DB_CLASS::dbmove (int mode, int pos)
/**
Scroll-Cursor lesen.
**/
{
         int scrollakt;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }
         scrollakt = scrollpos;
         switch (mode)
         {
             case DBABSOLUTE :      
             case RELATIVE :      
                    break;
             default :
                   return (-1);
         }
         fetch_scroll (cursor_ausw, mode, pos);
         if (sqlstatus != 0)
         {
             scrollpos = scrollakt;
         }
         return (sqlstatus);
}

int DB_CLASS::dbcanmove (int mode)
/**
Scroll-Cursor testen.
**/
{
         int status; 

         if (cursor_ausw == -1)
         {
                    return (-1);
         }

         switch (mode)
         {
             case FIRST :
                         scrollpos = 1;
                         break;
             case NEXT :     
                         scrollpos ++;
                         break;
             case PRIOR :
                         if (scrollpos > 1)
                         {
                                 scrollpos --;
                         }
                         break;
//             case LAST :      
             case CURRENT :
                         break;
             default :
                   return (-1);
         }
         fetch_scroll (cursor_ausw, mode);
         if (sqlstatus != 0)
         {
             status = FALSE;
         }
         else
         {
             status = TRUE;
         }
         dbmove (DBABSOLUTE, scrollpos);
         return (status);
}

int DB_CLASS::dbcanmove (int mode, int pos)
/**
Scroll-Cursor lesen.
**/
{
         int status;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }
         switch (mode)
         {
             case DBABSOLUTE :      
             case RELATIVE :      
                    break;
             default :
                   return (-1);
         }
         fetch_scroll (cursor_ausw, mode, pos);
         if (sqlstatus != 0)
         {
             status = FALSE;
         }
         else
         {
             status = TRUE;
         }
         dbmove (DBABSOLUTE, scrollpos);
         return (status);
}

int DB_CLASS::sqlin (void *vadr, int typ, int len)
/**
Eingabevariable fuer sqlcomm zuordnen.
**/
{
        return ins_quest ((char *) vadr, typ, len);
}


int DB_CLASS::sqlout (void *vadr, int typ, int len)
/**
Eingabevariable fuer sqlcomm zuordnen.
**/
{
        return out_quest ((char *) vadr, typ, len);
}

int DB_CLASS::sqlcursor (char *sql)
/**
Cursor vorbereiten.
**/
{
         return prepare_sql (sql);
}

int DB_CLASS::sqlopen (int cursor)
/**
Cursor vorbereiten.
**/
{
         return open_sql (cursor);
}

int DB_CLASS::sqlfetch (int cursor)
/**
Cursor vorbereiten.
**/
{
         return fetch_sql (cursor);
}

int DB_CLASS::sqlexecute (int cursor)
/**
Cursor vorbereiten.
**/
{
         return execute_curs (cursor);
}


int DB_CLASS::sqlclose (int cursor)
/**
Cursor schliessen.
**/
{
         return close_sql (cursor);
}
          

int DB_CLASS::sqlcomm (char *sql)
/**
SQL-Commando ausfuehren.
**/
{
         execute_sql (sql);
         return sqlstatus;
}
          

#ifndef CONSOLE
int DB_CLASS::ShowAllBu (HWND hWnd, int ws_flag,
                         int cursor_ausw, 
                         int (*endsort) (void),  
                         void (*IsAwClck) (int),
                         char *sort_awtab,
                         int sort_awanz,
                         char *sort_aw,
                         int size,
                         form *fsort_aw,
                         form *fsort_awl,
                         form *fsort_awub)  
/**
Auswahl ueber Gruppen anzeigen.
**/
{
	    form *savecurrent;
	    HWND eWindow;
		
		savecurrent = current_form;
	    save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
		SetHLines (0);
        eWindow = OpenListWindowEnF (10, 40, 8, 20, ws_flag);
        ElistVl (fsort_awl);
        ElistUb (fsort_awub);
		Setlistenter (1);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) sort_aw,
                   (int) size,
                   fsort_aw);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) sort_aw,
                             (int) size,
                             fsort_aw);
        current_form = savecurrent;
        restore_fkt (5); 
	    SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        set_fkt (NULL, 5);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
        }
        return 1;
}
#endif
