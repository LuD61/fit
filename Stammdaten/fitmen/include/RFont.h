#ifndef _RFONT_DEF
#define _RFONT_DEF
#include <windows.h>
#include "text.h"

class RFont
{
   private :
	   Text Name;
	   int Width;
	   int Height;
	   int Attributes;
	   HFONT Font;
	   int Instances;
   public :
	   RFont ();
	   RFont (Text, int, int, int, HFONT);
	   RFont (char *, int, int, int, HFONT);
	   ~RFont ();
       BOOL operator== (RFont&);
       RFont& operator=(RFont&);

	   void SetName (Text&);
	   Text& GetName ();
	   void SetWidth (int);
	   int GetWidth ();
	   void SetHeight (int);
	   int GetHeight ();
	   void SetAttributes (int);
	   int GetAttributes ();
	   void SetFont (HFONT);
	   HFONT GetFont ();
	   void inc ();
	   void dec ();
	   int GetInstances ();
};
#endif
