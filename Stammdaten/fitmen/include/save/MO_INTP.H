#ifndef _MO_INTP_DEF
#define _MO_INTP_DEF
/* ************************************************************************ */
/*       Definitionen und extern Variablen fuer Programme die das 
         Modul mo_intp benutzen.

         Autor : Wilhelm Roth
*/
/* ************************************************************************ */

typedef struct
       {   char  feldname [20];
           short feldlen;
           char  feldtyp [2];
           short nachkomma;
           short dimension;
       } FELDER;

typedef struct
       {   FELDER **felder;
           char   **buffer;
       } RECORD;

union FADR
      { unsigned char  *fchar;
        short *fshort;
        int   *fint;
        long  *flong;
        void  *fvoid;
        double *fdouble;
      };

extern FELDER *ziel;
extern FELDER *quelle;

extern int feld_anz;
extern int banz;
extern unsigned char *ausgabesatz;
extern int zlen;

char *feldadr (char *);
void mov (char *, char *, ...);
#endif