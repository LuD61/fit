#ifndef _HELP_DEF
#define _HELP_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"

#ifndef OK_CTL
#define OK_CTL 9603
#endif
#ifndef CANCEL_CTL
#define CANCEL_CTL 9604 
#endif

#define INITHELP 0
#define INITCOMM 1

class HELP : virtual public DLG 
{
          private :
              CFIELD **_fWork;
              CFORM *fWork;
              CFIELD **_fHelp;
              CFORM *fHelp;
              int cx, cy;
              char *head;
              char *itemdata;
              long hlen;
              long dlen;
              long offset;
              int items;
              char **Rows;
              HWND hMainWindow;
              HINSTANCE hInstance;
              char *HelpName;
              static COLORREF Background;
          public :
   	          HELP (char *, HINSTANCE, HWND);
   	          HELP (char *, HINSTANCE, HWND, BOOL);
   	          HELP (char *, char *, HINSTANCE, HWND);
   	          HELP (char *, char *, HINSTANCE, HWND, BOOL);
  	          ~HELP ();

 	          void Init (char *);
 	          void InitComment (char *);
              char *GetItpos (char *);
              BOOL ReadItem (char *,char *);
              char *GetItem (char *, char *);
              BOOL MakeCform (int); 
              BOOL OnKey (int);
              BOOL OnKey3 (void);
              BOOL OnKeyReturn (void);
//              BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
              BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
              BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
              BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
              BOOL OnLButtonDown (HWND, UINT,WPARAM,LPARAM);
              void SetWinBackground (COLORREF);
              static void SetBackground (COLORREF);
};
#endif
