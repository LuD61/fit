#ifndef DBSPEZOK
#define DBSPEZOK

class DBSPEZ_CLASS 
{
        private :
             int dsqlstatus;
     
        public :
            DBSPEZ_CLASS ()
            {
            }
            int TxtExist (double, long);
            long CreateTextNr (short, short,double);
            long GetNextTextNr (short, short,double);
            int  FreeTextNr (short, short,long);
};

#endif