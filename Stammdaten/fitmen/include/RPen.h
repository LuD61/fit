#ifndef _RPEN_DEF
#define _RPEN_DEF
#include <windows.h>

class RPen
{
   private :
	   int style;
       int width;
       COLORREF color;    
	   HPEN hPen;
	   int Instances;
   public :
	   RPen ();
	   RPen (int, int, COLORREF, HPEN);
	   ~RPen ();
           BOOL operator== (RPen&);
           RPen& operator=(RPen&);

	   void SetStyle (int);
	   int GetStyle ();
	   void SetWidth (int);
	   int GetWidth ();
	   void SetColor (COLORREF);
	   COLORREF GetColor ();
	   void SetPen (HPEN);
	   HPEN GetPen ();
	   void inc ();
	   void dec ();
	   int GetInstances ();
};
#endif
