#include "StdAfx.h"
#include "AkiKunLsList.h"

CAkiKunLsList::CAkiKunLsList(void)
{
	mdn = 0;
	aki_nr = 0;
	kun_pr = 0l;
	zus_bz = _T("");
}

CAkiKunLsList::CAkiKunLsList(short mdn, long aki_nr, long kun_pr, LPTSTR zus_bz)
{
	this->mdn    = mdn;
    this->aki_nr = aki_nr;
	this->kun_pr = kun_pr;
	this->zus_bz = zus_bz;
}

CAkiKunLsList::~CAkiKunLsList(void)
{
}
