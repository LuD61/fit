/***************************************************************************/
/* Programmname :  Log.CPP                                                 */
/*-------------------------------------------------------------------------*/
/* Funktion :  Klasse CStdLog                                               */
/*-------------------------------------------------------------------------*/
/* Revision : 1.0    Datum : 07.06.04   erstellt von : W. Roth             */
/*                   allgemneine Klasse zum Zerlegen von Strings           */
/*                                                                         */    
/*-------------------------------------------------------------------------*/
/* Aufruf :                                                                */
/* Funktionswert :                                                         */
/* Eingabeparameter :                                                      */
/*                                                                         */
/* Ausgabeparameter :                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
#include "StdAfx.h"
#include <share.h>
#include "log.h"

CString CStdLog::Record;

CStdLog::CStdLog(void)
{
	FileName = "";
	fp = NULL;
}

CStdLog::~CStdLog(void)
{
	Close ();
}

void CStdLog::Close ()
{
	if (fp != NULL)
	{
		fclose (fp);
		fp = NULL;
	}
}

void CStdLog::Delete ()
{
	if (FileName != "")
	{
		DeleteFile (FileName.GetBuffer ());
	}
}

void CStdLog::Write ()
{
	Write (Line);
}

void CStdLog::Write (CString& text)
{
char date [12];
char time [10];
CString FileText;

if (fp == NULL)
{
	fp = fopen (FileName.GetBuffer (), "w");
}
if (fp == NULL) return;

Sysdate (date);
Systime (time);

FileText.Format ("%s %s %s", date, time, text.GetBuffer ());
fprintf (fp, "%s\n", FileText.GetBuffer ()); 
fflush (fp);
}

void CStdLog::Println (CString *record)
{
	Write (*record);
}

void CStdLog::Sysdate (char *datum)
{
 SYSTEMTIME SysTime;
 GetSystemTime((LPSYSTEMTIME) &SysTime );
 sprintf (datum, "%02d.%02d.%04d", SysTime.wDay,
	                               SysTime.wMonth,
                                   SysTime.wYear);
}

void CStdLog::Systime (char *zeit)
{
 SYSTEMTIME SysTime;
 GetLocalTime((LPSYSTEMTIME) &SysTime );
 sprintf (zeit, "%02d:%02d:%02d", SysTime.wHour,
	                              SysTime.wMinute,
                                  SysTime.wSecond);
}

void CRingLog::Open ()
{
	  char buffer [256];
	  long fpos;
	  long i;

	  FileName.Trim ();
	  row = 0l;
	  if (fp == NULL)
	  {
			fp = _fsopen (FileName.GetBuffer (), "r+w", _SH_DENYNO);
			if (fp == NULL)
			{
				fp = fopen (FileName.GetBuffer (), "w");
			}
			if (fp != NULL)
			{
				fclose (fp);
				fp = _fsopen (FileName.GetBuffer (), "r+w", _SH_DENYNO);
			}
	   }
	   if (fp == NULL) 
	   {
		   int err = GetLastError ();
		   CString Error;
		   Error.Format ("Fehler %d beim �ffnen", err);
		   return;
	   }


	   fseek (fp, 0l, SEEK_SET);
	   if (fgets (buffer, 82, fp) == NULL)
	   {
		  row = (long) 1;
		  sprintf (buffer, "%ld", row);
          fprintf (fp, "%-78.78s\n", buffer);
          fpos = ftell (fp);
		  memset (buffer, '*', 78);
          fprintf (fp, "%-78.78s\n", buffer);
		  fflush (fp);
		  fseek (fp, fpos, SEEK_SET);
		  return;
	   }
	  row = atol (buffer);
	  i = 1;
	  while (fgets (buffer, 82, fp) != NULL)
	  {
		  i ++;
		  if (i == row)
		  {
			  break;
		  }
	  }
	  return;
}

void CRingLog::Write ()
{
	Write (Line);
}

void CRingLog::Write (CString& text)
{
	   long fpos = 0;
	   char date [12];
	   char time [10];
	   CString FileText;

	   if (fp == NULL)
	   {
		   Open ();
	   }
	   if (fp == NULL) return;

	   Sysdate (date);
	   Systime (time);
	   FileText.Format ("%s %s | %s", date, time, text.GetBuffer ());

	   if (row >= maxrows)
	   {
		   row = 2;
	   }
	   else
	   {
		   row ++;
		   fpos = ftell (fp);
	   }

	   fseek (fp, 0l, SEEK_SET);
	   CString Row;
	   Row.Format ("%ld", row);
	   fprintf (fp, "%-78.78s\n", Row.GetBuffer ());
	   fflush (fp);
       if (fpos > 0l)
	   {
		   fseek (fp, fpos, SEEK_SET);
	   }

	   CString Buffer;
	   Buffer.Format ("%-78.78s", FileText.GetBuffer ());
	   fprintf (fp, "%s\n", Buffer.GetBuffer ());
	   fflush (fp);
	   if (row < maxrows)
	   {
        char buffer[80];
		fpos = ftell (fp);
 		memset (buffer, '*', 78);
		fprintf (fp, "%-78.78s\n", buffer);
		fflush (fp);
		fseek (fp, fpos, SEEK_SET);
	   }
}

void CFifoLog::Write (CString& text)
/**
Text mit Datum, Uhrzeit, Letzter Satz ist auf der 1. Zeile
**/
{
char date [12];
char time [10];
CString FileText;


Sysdate (date);
Systime (time);

FileText.Format ("%s %s %s", date, time, text.GetBuffer ());
WriteFirstLine (FileText);
}

void CFifoLog::WriteFirstLine (CString& FileText)
{
FILE *in;
FILE *out; 
DWORD pid = 0;
int z;
char fname [256];
char buffer [256];

pid = GetCurrentProcessId ();
sprintf (fname, "touchwrite.%ld", pid);
CopyFile (FileName.GetBuffer (), fname, (int) FALSE);
out = fopen (FileName.GetBuffer (), "w");
if(out == NULL) return;
fprintf (out, "%s\n", FileText.GetBuffer ());
in = fopen (fname, "r");
if(in == NULL)
{
	fclose (out);
	return;
}
z = 0;
while( fgets (buffer, 255, in) != NULL )
{
  fputs (buffer, out);
  if(z >= MAXLINES) break;
  z ++;
}
fclose (in);
fclose (out);
DeleteFile ((LPCTSTR) fname);
}



