#include "stdafx.h"
#include "akiprgrstk.h"

struct AKIPRGRSTK akiprgrstk, akiprgrstk_null;

void AKIPRGRSTK_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &akiprgrstk.aki_nr, SQLLONG, 0);
    sqlout ((short *) &akiprgrstk.mdn,SQLSHORT,0);
    sqlout ((short *) &akiprgrstk.aki_nr,SQLLONG,0);
    sqlout ((TCHAR *) akiprgrstk.zus_bz,SQLCHAR,25);
    sqlout ((long *) &akiprgrstk.pr_gr_stuf,SQLLONG,0);
    sqlout ((DATE_STRUCT *) &akiprgrstk.aki_von,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &akiprgrstk.aki_bis,SQLDATE,0);
    sqlout ((short *) &akiprgrstk.delstatus,SQLSHORT,0);
    sqlout ((short *) &akiprgrstk.waehrung,SQLSHORT,0);
    sqlout ((TCHAR *) akiprgrstk.tv,SQLCHAR,2);
    sqlout ((TCHAR *) akiprgrstk.pr_gr_kopie,SQLCHAR,81);
            cursor = sqlcursor (_T("select akiprgrstk.mdn,  ")
_T("akiprgrstk.aki_nr,  akiprgrstk.zus_bz,  akiprgrstk.pr_gr_stuf,  ")
_T("akiprgrstk.aki_von,  akiprgrstk.aki_bis,  akiprgrstk.delstatus,  ")
_T("akiprgrstk.waehrung,  akiprgrstk.tv,  akiprgrstk.pr_gr_kopie from akiprgrstk ")

#line 12 "akiprgrstk.rpp"
                                  _T("where aki_nr = ? "));
    sqlin ((short *) &akiprgrstk.mdn,SQLSHORT,0);
    sqlin ((short *) &akiprgrstk.aki_nr,SQLLONG,0);
    sqlin ((TCHAR *) akiprgrstk.zus_bz,SQLCHAR,25);
    sqlin ((long *) &akiprgrstk.pr_gr_stuf,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &akiprgrstk.aki_von,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &akiprgrstk.aki_bis,SQLDATE,0);
    sqlin ((short *) &akiprgrstk.delstatus,SQLSHORT,0);
    sqlin ((short *) &akiprgrstk.waehrung,SQLSHORT,0);
    sqlin ((TCHAR *) akiprgrstk.tv,SQLCHAR,2);
    sqlin ((TCHAR *) akiprgrstk.pr_gr_kopie,SQLCHAR,81);
            sqltext = _T("update akiprgrstk set ")
_T("akiprgrstk.mdn = ?,  akiprgrstk.aki_nr = ?,  akiprgrstk.zus_bz = ?,  ")
_T("akiprgrstk.pr_gr_stuf = ?,  akiprgrstk.aki_von = ?,  ")
_T("akiprgrstk.aki_bis = ?,  akiprgrstk.delstatus = ?,  ")
_T("akiprgrstk.waehrung = ?,  akiprgrstk.tv = ?,  ")
_T("akiprgrstk.pr_gr_kopie = ? ")

#line 14 "akiprgrstk.rpp"
                                  _T("where aki_nr = ? ");
            sqlin ((short *)   &akiprgrstk.aki_nr, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &akiprgrstk.aki_nr, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select aki_nr from akiprgrstk ")
                                  _T("where aki_nr = ? for update"));
            sqlin ((short *)   &akiprgrstk.aki_nr, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from akiprgrstk ")
                                  _T("where aki_nr = ? "));
    sqlin ((short *) &akiprgrstk.mdn,SQLSHORT,0);
    sqlin ((short *) &akiprgrstk.aki_nr,SQLLONG,0);
    sqlin ((TCHAR *) akiprgrstk.zus_bz,SQLCHAR,25);
    sqlin ((long *) &akiprgrstk.pr_gr_stuf,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &akiprgrstk.aki_von,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &akiprgrstk.aki_bis,SQLDATE,0);
    sqlin ((short *) &akiprgrstk.delstatus,SQLSHORT,0);
    sqlin ((short *) &akiprgrstk.waehrung,SQLSHORT,0);
    sqlin ((TCHAR *) akiprgrstk.tv,SQLCHAR,2);
    sqlin ((TCHAR *) akiprgrstk.pr_gr_kopie,SQLCHAR,81);
            ins_cursor = sqlcursor (_T("insert into akiprgrstk (")
_T("mdn,  aki_nr,  zus_bz,  pr_gr_stuf,  aki_von,  aki_bis,  delstatus,  waehrung,  tv,  ")
_T("pr_gr_kopie) ")

#line 25 "akiprgrstk.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?)")); 

#line 27 "akiprgrstk.rpp"
}
