// FrameDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "IPrDialog.h"
#include "FrameDlg.h"


// CFrameDlg-Dialogfeld

IMPLEMENT_DYNAMIC(CFrameDlg, CDialog)
CFrameDlg::CFrameDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFrameDlg::IDD, pParent)
{
	tabx = 0;
	taby = 0;
	StartSize = START_NORMAL;
	Cfg.SetProgName( _T("IPrDialog"));
	ReadCfg ();
}

CFrameDlg::~CFrameDlg()
{
}

void CFrameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CFrameDlg, CDialog)
	ON_WM_SIZE ()
	ON_WM_DRAWITEM()
END_MESSAGE_MAP()


// CFrameDlg-Meldungshandler

BOOL CFrameDlg::OnInitDialog ()
{
	CDialog::OnInitDialog ();

//    ToolButton.Create (this);
//    ToolButton.Add (new CToolbarItem (IDC_SEPERATOR, VERT, this)); 

	dlg.Construct (_T(""), this);
	Page1.Construct (IDD_PR_ART_PAGE);
	Page1.Frame = this;
	dlg.AddPage (&Page1);
	Page2.Construct (IDD_PR_GR_PAGE);
	Page2.Frame = this;
	dlg.AddPage (&Page2);
	Page3.Construct (IDD_KUN_PR_PAGE);
	Page3.Frame = this;
	dlg.AddPage (&Page3);

    dlg.m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
    Page1.SetFocus (); 
	dlg.Create (this, WS_CHILD | WS_VISIBLE);

	if (StartSize == START_MAXIMIZED)
	{
		ShowWindow (SW_SHOWMAXIMIZED);
	}

	return FALSE;
}

void CFrameDlg::OnSize (UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	dlg.MoveWindow (tabx, taby, cx, cy);
//	dlg.MoveWindow (0, 0, cx, cy);
	CTabCtrl *tab = dlg.GetTabControl ();
	Page1.Frame = this;
	Page1.GetWindowRect (&pRect);
	dlg.ScreenToClient (&pRect);

	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = cx - 10 - tabx;
	frame.bottom = cy - 10 - taby;
	tab->MoveWindow (&frame);
	frame.top += 20;
	frame.left += 2;
    frame.right -= 4;
	frame.bottom -= 4;

	Page1.MoveWindow (&frame);
}

void CFrameDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen

    if (ToolButton.DrawToolbar (nIDCtl, lpDrawItemStruct->hDC)) 
    {
        return;
    }

	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

BOOL CFrameDlg::OnToolTipNotify(int nIDCtl,NMHDR *pNMHDR, LRESULT *pResult)
{
    if (ToolButton.OnToolTipNotify (nIDCtl, pNMHDR, pResult))
    {
        return TRUE;
    }
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *) pNMHDR;
    return FALSE;
}

void CFrameDlg::ReadCfg ()
{
    char cfg_v [256];

    if (Cfg.GetCfgValue ("StartSize", cfg_v) == TRUE)
    {
			StartSize = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}
