// PrArtPreise.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "IprMan.h"
#include "IvPrArtPage.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Process.h"
#include "UniFormField.h"
#include "DbUniCode.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CIvPrArtPage Dialogfeld

CIvPrArtPage::CIvPrArtPage(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage()
{
	Choice = NULL;
	ModalChoice = TRUE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	IprCursor = -1;
	IprGrStufkCursor = -1;
	IKunPrkCursor = -1;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_A);
	HeadControls.Add (&m_GueAb);
	PosControls.Add (&m_List);

	ButtonControls.Add (&m_Cancel);
	ButtonControls.Add (&m_Save);
	ButtonControls.Add (&m_Delete);
	ButtonControls.Add (&m_Insert);

    HideButtons = FALSE;

	Frame = NULL;
	IprCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	SearchA = _T("");
	Separator = _T(";");
	CellHeight = 0;
	Cfg.SetProgName( _T("IPrDialog"));
}

CIvPrArtPage::CIvPrArtPage(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	Frame = NULL;
	IprCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
}

CIvPrArtPage::~CIvPrArtPage()
{
	Font.DeleteObject ();
	Ivpr.rollbackwork ();
	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	Ivpr.dbclose ();
	Ivprgrstk.dbclose ();
	Iv_kun_prk.dbclose ();
	Iprgrstufk.dbclose ();
	Iv_kun_prk.dbclose ();
	Kun.dbclose ();
	KunAdr.dbclose ();
	if (IprCursor != -1)
	{
		A_bas.sqlclose (IprCursor);
	}
	if (IprGrStufkCursor != -1)
	{
		A_bas.sqlclose (IprGrStufkCursor);
	}
	if (IKunPrkCursor != -1)
	{
		A_bas.sqlclose (IKunPrkCursor);
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);
}

void CIvPrArtPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_LA_BZ1, m_LA_bz1);
	DDX_Control(pDX, IDC_A_BZ1, m_A_bz1);
	DDX_Control(pDX, IDC_A_BZ2, m_A_bz2);
	DDX_Control(pDX, IDC_LGUE_AB, m_LGueAb);
	DDX_Control(pDX, IDC_GUE_AB, m_GueAb);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_CANCEL, m_Cancel);
	DDX_Control(pDX, IDC_SAVE, m_Save);
	DDX_Control(pDX, IDC_DELETE, m_Delete);
	DDX_Control(pDX, IDC_INSERT, m_Insert);
}

BEGIN_MESSAGE_MAP(CIvPrArtPage, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_SIZE ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_ACHOICE ,  OnAchoice)
	ON_BN_CLICKED(IDC_CANCEL ,   OnCancel)
	ON_BN_CLICKED(IDC_SAVE ,     OnSave)
	ON_BN_CLICKED(IDC_DELETE ,   OnDelete)
	ON_BN_CLICKED(IDC_INSERT ,   OnInsert)
	ON_COMMAND (SELECTED, OnASelected)
	ON_COMMAND (CANCELED, OnACanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
END_MESSAGE_MAP()


// CIvPrArtPage Meldungshandler

BOOL CIvPrArtPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	A_bas.opendbase (_T("bws"));

	CUtil::GetPersName (PersName);
//	PgrProt.Construct (PersName, CString ("11112"), &Ivpr);
     
	ReadCfg ();

	if (HideButtons)
	{
		ButtonControls.SetVisible (FALSE);
		RightListSpace = 15;
	}
	else
	{
		ButtonControls.SetVisible (TRUE);
		RightListSpace = 125;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	m_Cancel.SetWindowText (_T("Beenden"));
    HBITMAP HbF5 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F5T", "F5MASKT");
	m_Cancel.SetBitmap (HbF5);

    HBITMAP HbF12 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F12T", "F12MASKT");
	m_Save.SetBitmap (HbF12);

    HBITMAP HbDel = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "DELT", "DELMASKT");
	m_Delete.SetBitmap (HbDel);

    HBITMAP HbInsert = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "INSERTT", "INSERTMASKT");
	m_Insert.SetBitmap (HbInsert);

    memcpy (&Ivprgrstk.ivprgrstk, &ivprgrstk_null, sizeof (IVPRGRSTK));
    memcpy (&Iv_kun_prk.iv_kun_prk, &iv_kun_prk_null, sizeof (IV_KUN_PRK));
    memcpy (&Ivpr.iv_pr, &iv_pr_null, sizeof (IV_PR));

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_A,EDIT,       (double *) &A_bas.a_bas.a, VDOUBLE, 13, 0));
    Form.Add (new CUniFormField (&m_A_bz1,EDIT,   (char *) A_bas.a_bas.a_bz1, VCHAR));
    Form.Add (new CUniFormField (&m_A_bz2,EDIT,   (char *) A_bas.a_bas.a_bz2, VCHAR));
    Form.Add (new CFormField (&m_GueAb,DATETIMEPICKER,  (DATE_STRUCT *) &Ivpr.iv_pr.gue_ab, VDATE));

	Ivpr.sqlin ((short *)  &Ivpr.iv_pr.mdn, SQLSHORT, 0);
	Ivpr.sqlin ((double *) &Ivpr.iv_pr.a,  SQLDOUBLE, 0);
	Ivpr.sqlin ((DATE_STRUCT *)  &Ivpr.iv_pr.gue_ab, SQLDATE, 0);
	Ivpr.sqlout ((long *)  &Ivpr.iv_pr.pr_gr_stuf, SQLLONG, 0);
	Ivpr.sqlout ((long *)  &Ivpr.iv_pr.kun_pr, SQLLONG, 0);
	Ivpr.sqlout ((long *)  &Ivpr.iv_pr.kun, SQLLONG, 0);
	IprCursor = Ivpr.sqlcursor (_T("select pr_gr_stuf, kun_pr, kun from iv_pr ")
		                       _T("where mdn = ? ")
		                       _T("and a = ? ")
		                       _T("and gue_ab = ? ")
		                       _T("order by pr_gr_stuf, kun_pr, kun"));
	Iprgrstufk.sqlin ((short *)  &Iprgrstufk.iprgrstufk.mdn, SQLSHORT, 0);
	Iprgrstufk.sqlout ((long *)  &Iprgrstufk.iprgrstufk.pr_gr_stuf, SQLLONG, 0);
	IprGrStufkCursor = Iprgrstufk.sqlcursor (_T("select pr_gr_stuf from iprgrstufk ")
		                                     _T("where mdn = ? ") 
											 _T("and pr_gr_stuf >= 0"));  
	I_kun_prk.sqlin ((short *)  &I_kun_prk.i_kun_prk.mdn, SQLSHORT, 0);
	I_kun_prk.sqlout ((long *)  &I_kun_prk.i_kun_prk.kun_pr, SQLLONG, 0);
	IKunPrkCursor = I_kun_prk.sqlcursor (_T("select kun_pr from i_kun_prk ")
		                                 _T("where mdn = ? ")
										 _T("and kun_pr >= 0"));  


	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_List.SetImageList (&image, LVSIL_SMALL);   
	}

	m_List.Mode = m_List.TERMIN;
	FillList = m_List;
	FillList.SetStyle (LVS_REPORT);
	if (m_List.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Preigruppenstufe"), 1, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("Kundenpreisliste"), 2, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("Kunde"), 3, 95, LVCFMT_RIGHT);
	FillList.SetCol (_T("Name"), 4, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("VK-Preis"), 5, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Laden-Preis"), 6, 100, LVCFMT_RIGHT);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	AGrid.Create (this, 1, 2);
    AGrid.SetBorder (0, 0);
    AGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 0, 0, 1, 1);
	AGrid.Add (c_A);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);

	ButtonGrid.Create (this, 5, 5);
    ButtonGrid.SetBorder (0, 0);
    ButtonGrid.SetCellHeight (20);
    ButtonGrid.SetGridSpace (0, 10);
	CCtrlInfo *c_Cancel = new CCtrlInfo (&m_Cancel, 0, 0, 1, 1);
	ButtonGrid.Add (c_Cancel);
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 1, 1, 1);
	ButtonGrid.Add (c_Save);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ButtonGrid.Add (c_Delete);
	CCtrlInfo *c_Insert = new CCtrlInfo (&m_Insert, 0, 3, 1, 1);
	ButtonGrid.Add (c_Insert);

	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_LA_bz1  = new CCtrlInfo (&m_LA_bz1, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LA_bz1);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 1, 2, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 1, 3, 3, 1); 
	CtrlGrid.Add (c_A_bz2);
	CCtrlInfo *c_LGueAb  = new CCtrlInfo (&m_LGueAb, 6, 3, 1, 1); 
	CtrlGrid.Add (c_LGueAb);
	CCtrlInfo *c_GueAb  = new CCtrlInfo (&m_GueAb, 7, 3, 1, 1); 
	CtrlGrid.Add (c_GueAb);
	CCtrlInfo *c_ButtonGrid = new CCtrlInfo (&ButtonGrid, DOCKRIGHT, 4, 1, 3); 
	CtrlGrid.Add (c_ButtonGrid);
	CCtrlInfo *c_List  = new CCtrlInfo (&m_List, 0, 4, DOCKRIGHT, DOCKBOTTOM); 
//	c_List->rightspace = 125;
	c_List->rightspace = RightListSpace;
	CtrlGrid.Add (c_List);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();
	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
	Form.Show ();
	ReadMdn ();
	if (RemoveKun)
	{
		m_List.DeleteColumn (m_List.PosKun);
		m_List.ScrollPositions (2);
		m_List.DeleteColumn (m_List.PosKunName);
		m_List.ScrollPositions (3);
	}
	EnableHeadControls (TRUE);
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}


void CIvPrArtPage::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CIvPrArtPage::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CIvPrArtPage::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CIvPrArtPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CIvPrArtPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_List.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_List &&
					GetFocus ()->GetParent () != &m_List )
				{

					break;
			    }
				m_List.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnAchoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_A)
				{
					OnAchoice ();
					return TRUE;
				}
				m_List.OnKey9 ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CIvPrArtPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

//	if (Control == &m_A)
	if (Control == &m_GueAb)
	{
		if (!Read ())
		{
			m_A.SetFocus ();
			return FALSE;
		}
	}

	if (Control != &m_List.ListEdit &&
		Control != &m_List.ListComboBox &&
		Control != &m_List.SearchListCtrl.Edit)
{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CIvPrArtPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_List &&
		Control->GetParent ()!= &m_List )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_List.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}


BOOL CIvPrArtPage::ReadMdn ()
{
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		FillPrGrStufCombo ();
		FillKunPrCombo ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CIvPrArtPage::read ()
{
	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}
	return Read ();

}

BOOL CIvPrArtPage::Read ()
{
	if (ModalChoice)
	{
		CString cA;
		m_A.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnAchoice ();
			SearchA = "";
			if (!AChoiceStat)
			{
				m_A.SetFocus ();
				m_A.SetSel (0, -1);
				return FALSE;
			}
		}
	}

	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	Form.Get ();
	if (A_bas.dbreadfirst () == 0)
	{
		if (ReadList ())
		{
			EnableHeadControls (FALSE);
			Form.Show ();
			m_A.SetFocus ();
			m_A.SetSel (0, -1);
			return TRUE;
		}
		else
		{
			m_A.SetFocus ();
			m_A.SetSel (0, -1);
			return FALSE;
		}
	}
	else
	{
		CString Error;
		Error.Format (_T("Artikel %.0lf nicht gefunden"),A_bas.a_bas.a);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);

		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		Form.Show ();
		m_A.SetFocus ();
		m_A.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CIvPrArtPage::LockList ()
{
	extern short sql_mode;
	BOOL ret = TRUE;
	Ivpr.beginwork ();
	short sqls = sql_mode;
	sql_mode = 1;

	Ivpr.sqlopen (IprCursor);
	while (Ivpr.sqlfetch (IprCursor) == 0)
	{
		int dsqlstatus = Ivpr.dbreadfirst ();
		if (dsqlstatus == 0)
		{
		      dsqlstatus = Ivpr.dbupdate ();
		}
		if (dsqlstatus < 0)
		{
			ret = FALSE;
			break;
		}
	}
	sql_mode = sqls;
	return ret;
}


BOOL CIvPrArtPage::ReadList ()
{
	m_List.StopEnter ();
	m_List.DeleteAllItems ();
	m_List.vSelect.clear ();
	DestroyRows (DbRows);
	int i = 0;
	memcpy (&Ivprgrstk.ivprgrstk.gue_ab, &Ivpr.iv_pr.gue_ab,  sizeof (DATE_STRUCT));
	memcpy (&Ivprgrstk.ivprgrstk.gue_ab, &Ivpr.iv_pr.gue_ab,  sizeof (DATE_STRUCT));
	memcpy (&Ivpr.iv_pr, &iv_pr_null, sizeof (IV_PR));
	Ivpr.iv_pr.mdn = Mdn.mdn.mdn;
	Ivpr.iv_pr.a   = A_bas.a_bas.a;
	memcpy (&Ivpr.iv_pr.gue_ab, &Ivprgrstk.ivprgrstk.gue_ab, sizeof (DATE_STRUCT));

	if (!LockList ())
	{
		MessageBox (_T("Der Artikel wird im Moment von einem anderen Benutzer bearbeitet"), _T(""),
			        MB_OK | MB_ICONERROR);
		return FALSE;
	}

	Ivpr.sqlopen (IprCursor);
	while (Ivpr.sqlfetch (IprCursor) == 0)
	{
		Ivpr.dbreadfirst ();
		memcpy (&Ivprgrstk.ivprgrstk, &ivprgrstk_null, sizeof (IVPRGRSTK));
		memcpy (&Iv_kun_prk.iv_kun_prk, &iv_kun_prk_null, sizeof (IV_KUN_PRK));
		memcpy (&Kun.kun, &kun_null, sizeof (KUN));
		memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
		Iprgrstufk.iprgrstufk.mdn = Ivpr.iv_pr.mdn;
		Iprgrstufk.iprgrstufk.pr_gr_stuf = Ivpr.iv_pr.pr_gr_stuf;
		Iprgrstufk.dbreadfirst ();
		I_kun_prk.i_kun_prk.mdn = Ivpr.iv_pr.mdn;
		I_kun_prk.i_kun_prk.kun_pr = Ivpr.iv_pr.kun_pr;
		I_kun_prk.dbreadfirst ();
		Kun.kun.mdn = Ivpr.iv_pr.mdn;
		Kun.kun.kun = Ivpr.iv_pr.kun;
		if (Kun.dbreadfirst () == 0)
		{
			KunAdr.adr.adr = Kun.kun.adr1;
			KunAdr.dbreadfirst ();
		}
		if (RemoveKun && Ivpr.iv_pr.kun > 0l) continue;
		FillList.InsertItem (i, 0);
		CString PrGrStuf;
		PrGrStuf.Format (_T("%ld  %s"), Ivpr.iv_pr.pr_gr_stuf,
							Iprgrstufk.iprgrstufk.zus_bz);
		FillList.SetItemText (PrGrStuf.GetBuffer (), i, m_List.PosPrGrStuf);
		CString KunPr;
		KunPr.Format (_T("%ld  %s"), Ivpr.iv_pr.kun_pr,
			I_kun_prk.i_kun_prk.zus_bz);
		FillList.SetItemText (KunPr.GetBuffer (), i, m_List.PosKunPr);
		CString Kun;
//		Kun.Format (_T("%ld  %s"), Ipr.ipr.kun, KunAdr.adr.adr_krz);
		if (!RemoveKun)
		{
			Kun.Format (_T("%ld"), Ivpr.iv_pr.kun);
			FillList.SetItemText (Kun.GetBuffer (), i, m_List.PosKun);
			CString KunName;
			KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
			FillList.SetItemText (KunName.GetBuffer (), i, m_List.PosKunName);
		}

		CString VkPr;
		m_List.DoubleToString (Ivpr.iv_pr.vk_pr_eu, VkPr, 4);
		FillList.SetItemText (VkPr.GetBuffer (), i, m_List.PosVkPr);
		CString LdPr;
		m_List.DoubleToString (Ivpr.iv_pr.ld_pr_eu, LdPr, 2);
		FillList.SetItemText (LdPr.GetBuffer (), i, m_List.PosLdPr);

		CVPreise *iv_pr = new CVPreise (VkPr, LdPr, Ivpr.iv_pr);
		DbRows.Add (iv_pr);
		i ++;
	}
	return TRUE;
}

BOOL CIvPrArtPage::IsChanged (CVPreise *pIpr)
{
	DbRows.FirstPosition ();
	CVPreise *iv_pr;
	while ((iv_pr = (CVPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ivpr.iv_pr, &iv_pr->iv_pr, sizeof (IV_PR));
		if (Ivpr == iv_pr->iv_pr) break;
	} 	
	if (iv_pr == NULL)
	{
		return TRUE;
	}
	if (pIpr->cEk != iv_pr->cEk) return TRUE;
	if (pIpr->cVk != iv_pr->cVk) return TRUE;
	return FALSE;
}

BOOL CIvPrArtPage::InList (IV_PR_CLASS& Iv_pr)
{
	ListRows.FirstPosition ();
	CVPreise *iv_pr;
	while ((iv_pr = (CVPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Ivpr == iv_pr->iv_pr) return TRUE;
	}
    return FALSE;
}

// 301111 A
int  CIvPrArtPage::testgrundpreis ( bool schongefragt ,  double ia ) 
{


	A_bas.a_bas.a = ia;
	A_bas.dbreadfirst ();

	if (sypgrundartpreis == 1 && A_bas.a_bas.me_einh != 2) return 0; //240412 Nur grundartpreis == 1 hat Einschr�nkung auf me_einh == 2, grundartpreis == 2 hat diese nicht
	if ( A_bas.a_bas.a_grund == A_bas.a_bas.a && A_bas.a_bas.a > 0 )
	{

		if ( schongefragt ) return 1 ;

		if (MessageBox (_T("�nderung auch f�r Artikelvarianten ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==	IDNO)
		{
			return -1;	// NEIN und nix machen
		}
		else
			return  1; // JA und was machen
	}
	return 0; // unentschieden
}
// 301111 E

void CIvPrArtPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CVPreise *iv_pr;
	while ((iv_pr = (CVPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ivpr.iv_pr, &iv_pr->iv_pr, sizeof (IV_PR));
		if (!InList (Ivpr))
		{
			Ivpr.dbdelete ();
			PgrProt.Write (1);
		}
	}
}

BOOL CIvPrArtPage::Write ()
{
	extern short sql_mode;
	short sql_s;

	sql_s = sql_mode;
	sql_mode = 1;
	Ivpr.beginwork ();
	m_List.StopEnter ();
	Form.Get ();
	int count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 IV_PR *iv_pr = new IV_PR;
		 memcpy (iv_pr, &iv_pr_null, sizeof (IV_PR));
         CString Text;
		 Text = m_List.GetItemText (i, m_List.PosPrGrStuf);
		 int start = 0;
		 CString Nr = Text.Tokenize (" ", start);
		 iv_pr->pr_gr_stuf = atol (Nr);
		 CString PrGrZusBez = Text.Tokenize (" ", start);
		 int idx = Text.Find (PrGrZusBez, 0);
		 if (idx != 0)
		 {
			 PrGrZusBez = Text.Mid (idx);
		 }
		 PrGrZusBez.Trim ();

		 Text = m_List.GetItemText (i, m_List.PosKunPr);
		 start = 0;
		 Nr = Text.Tokenize (" ", start);
		 iv_pr->kun_pr = atol (Nr);
		 CString KunPrZusBez = Text.Tokenize (" ", start);
		 idx = Text.Find (KunPrZusBez, 0);
		 if (idx != 0)
		 {
			 KunPrZusBez = Text.Mid (idx);
		 }
		 KunPrZusBez.Trim ();
		 if (m_List.PosKun != -1)
		 {
			Text = m_List.GetItemText (i, m_List.PosKun);
			iv_pr->kun = atol (Text);
		 }
		 if (iv_pr->kun > 0l)
		 {
			 Kun.kun.kun = iv_pr->kun;
			 if (Kun.dbreadfirst () == 0)
			 {
				 iv_pr->pr_gr_stuf = Kun.kun.pr_stu;
				 iv_pr->kun_pr = Kun.kun.pr_lst;
				 PrGrZusBez = "";
                 KunPrZusBez = "";
			 }
		 }
     
     	 CString VkPr =  m_List.GetItemText (i, m_List.PosVkPr);
		 iv_pr->vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
		 iv_pr->vk_pr_i = iv_pr->vk_pr_eu;
		 CString LdPr =  m_List.GetItemText (i, m_List.PosLdPr);
		 iv_pr->ld_pr_eu = CStrFuncs::StrToDouble (LdPr);
		 iv_pr->ld_pr = iv_pr->ld_pr_eu;
		 if (iv_pr->vk_pr_eu == 0.0 && iv_pr->ld_pr_eu == 0.0)
		 {
			 continue;
		 }
		 iv_pr->mdn = Mdn.mdn.mdn; 
		 iv_pr->a = A_bas.a_bas.a; 
		 memcpy (&iv_pr->gue_ab, &Ivpr.iv_pr.gue_ab, sizeof (DATE_STRUCT));
		 CVPreise *pr = new CVPreise (VkPr, LdPr, *iv_pr);
		 pr->Prgrzusbez = PrGrZusBez;
		 pr->Kunprzusbez = KunPrZusBez;
		 delete iv_pr;
		 ListRows.Add (pr);
	}

	DeleteDbRows ();

	ListRows.FirstPosition ();
	CVPreise *iv_pr;
	while ((iv_pr = (CVPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&Ivpr.iv_pr, &iv_pr->iv_pr, sizeof (IV_PR));
		Ivpr.dbupdate ();
		if (IsChanged (iv_pr))
		{

// 301111 A
			if ( 1 == 3 )		// evtl. sp�ter
			{
			int ierg = 0 ;
			if ( ischongefragt == FALSE )
			{
				ierg = testgrundpreis ( ischongefragt , Ivpr.iv_pr.a ) ;
				if ( !ierg )	// immer noch keine Entscheidung 
				{
				}
				else
				{
					if ( ierg == 1  ) 
					{
						ischongefragt = TRUE;	// JA und was machen
						igrundartpreis = TRUE;
					}
					if ( ierg == -1 )
					{	
						ischongefragt = TRUE;	// NEIN
						igrundartpreis = FALSE;
					}
				}
			}
			else	// Bereits entschieden ob ueberhaupt
			{
				if ( igrundartpreis == TRUE )
				{
					ierg = testgrundpreis (  ischongefragt , Ivpr.iv_pr.a );
				}
			}
			if ( ierg == 1 )
			{
				ierg = ierg ;				// Unterpos. schreiben
			}
			}	// evtl. sp�ter
// 301111 E

			PgrProt.Write ();
		}
		if (Ivpr.iv_pr.kun > 0l) continue;

		Ivprgrstk.ivprgrstk.mdn = Ivpr.iv_pr.mdn;
		Ivprgrstk.ivprgrstk.pr_gr_stuf = Ivpr.iv_pr.pr_gr_stuf;  
		/**310112 
		LPSTR p = (LPSTR) Ivprgrstk.ivprgrstk.zus_bz;
		strcpy (p, "");
		CDbUniCode::DbFromUniCode (CUniFormField::NullCode, 
			iv_pr->Prgrzusbez.GetBuffer (), 
			p, ((iv_pr->Prgrzusbez.GetLength () + 1) * 2));
			***/
		memcpy (&Ivprgrstk.ivprgrstk.gue_ab, &Ivpr.iv_pr.gue_ab, sizeof (DATE_STRUCT));
		Ivprgrstk.dbupdate ();

		Iv_kun_prk.iv_kun_prk.mdn = Ivpr.iv_pr.mdn;
		Iv_kun_prk.iv_kun_prk.kun_pr = Ivpr.iv_pr.kun_pr;  
		Iv_kun_prk.iv_kun_prk.pr_gr_stuf = Ivpr.iv_pr.pr_gr_stuf;  
		/**  310112 wozu soll das gut sein ?? f�hrt zum Absturz --> doch nicht
		p = (LPSTR) Iv_kun_prk.iv_kun_prk.zus_bz;
		strcpy (p, "");
		CDbUniCode::DbFromUniCode (CUniFormField::NullCode, 
			iv_pr->Kunprzusbez.GetBuffer (), 
			p, ((iv_pr->Kunprzusbez.GetLength () + 1) * 2));
			310112 ******************/
		memcpy (&Iv_kun_prk.iv_kun_prk.gue_ab, &Ivpr.iv_pr.gue_ab, sizeof (DATE_STRUCT));
		Iv_kun_prk.dbupdate ();   // 310112 Hier Absturz !!!!!!!!
	}
	EnableHeadControls (TRUE);
	m_A.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	Ivpr.commitwork ();
// 301111
	if ( sypgrundartpreis > 0)
		ischongefragt = FALSE ;
	else
	{
		igrundartpreis = FALSE;		//270612
		ischongefragt = TRUE ;
	}
	sql_mode = sql_s;
	return TRUE;
}

BOOL CIvPrArtPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Alle Eintr�ge l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	Ivpr.beginwork ();
	m_List.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	m_List.DeleteAllItems ();

	EnableHeadControls (TRUE);
	m_A.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	Ivpr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

void CIvPrArtPage::OnAchoice ()
{
    AChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceA (this);
	    Choice->IsModal = ModalChoice;
	    Choice->HideEnter = FALSE;
	    Choice->HideFilter = FALSE;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->SearchText = SearchA;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{

		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
/*
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		cy -= 100;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
*/
		Choice->MoveWindow (&rect);
//		Choice->SetFocus ();
		Choice->SetListFocus ();  //231111 

		return;
	}
    if (Choice->GetState ())
    {
		  CABasList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_A.EnableWindow (TRUE);
		  m_A.SetSel (0, -1, TRUE);
		  m_A.SetFocus ();
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
	 	  AChoiceStat = FALSE;	
	}
}

void CIvPrArtPage::OnASelected ()
{
	if (Choice == NULL) return;
    CABasList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    A_bas.a_bas.a = abl->a;
    if (A_bas.dbreadfirst () == 0)
    {
		m_GueAb.EnableWindow (TRUE);
		m_GueAb.SetFocus ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	if (CloseChoice)
	{
		OnACanceled (); 
	}
    Form.Show ();

	if (Choice->FocusBack)
	{
		Read ();
		m_List.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CIvPrArtPage::OnACanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CIvPrArtPage::StepBack ()
{
	if (m_A.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		m_List.StopEnter ();
		EnableHeadControls (TRUE);
		m_A.SetFocus ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
		m_List.DeleteAllItems ();
		Ivpr.rollbackwork ();
	}
	return TRUE;
}

void CIvPrArtPage::OnCancel ()
{
	StepBack ();
}

void CIvPrArtPage::OnSave ()
{
	Write ();
}

void CIvPrArtPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CIvPrArtPage::FillPrGrStufCombo ()
{
	CVector Values;
	Values.Init ();
	m_List.m_Mdn = Mdn.mdn.mdn;
	Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
    Iprgrstufk.sqlopen (IprGrStufkCursor);
	while (Iprgrstufk.sqlfetch (IprGrStufkCursor) == 0)
	{
		Iprgrstufk.dbreadfirst ();
		CString *Value = new CString ();
		Value->Format (_T("%ld  %s"), Iprgrstufk.iprgrstufk.pr_gr_stuf,
			                          Iprgrstufk.iprgrstufk.zus_bz);
		Values.Add (Value);
	}
	m_List.FillPrGrStufCombo (Values);
}

void CIvPrArtPage::FillKunPrCombo ()
{
	CVector Values;
	Values.Init ();
	I_kun_prk.i_kun_prk.mdn = Mdn.mdn.mdn;
    I_kun_prk.sqlopen (IKunPrkCursor);
	while (I_kun_prk.sqlfetch (IKunPrkCursor) == 0)
	{
		I_kun_prk.dbreadfirst ();
		CString *Value = new CString ();
		Value->Format (_T("%ld  %s"), I_kun_prk.i_kun_prk.kun_pr,
			                          I_kun_prk.i_kun_prk.zus_bz);
		Values.Add (Value);
	}
	m_List.FillKunPrCombo (Values);
}

void CIvPrArtPage::OnDelete ()
{
	m_List.DeleteRow ();
}

void CIvPrArtPage::OnInsert ()
{
	m_List.InsertRow ();
}

/*
BOOL CIvPrArtPage::Print ()
{
	CProcess print;
	CString Command = "70001 11112";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}
*/

BOOL CIvPrArtPage::Print ()
{
	CProcess print;
	Form.Get ();
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11112.llf", tmp);
	}
	else
	{
		dName = "11114.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11114\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "a %.0lf %.0lf\n", A_bas.a_bas.a,
			                        A_bas.a_bas.a);
		fclose (fp);
		Command.Format ("dr70001 -name 11114 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11114";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

BOOL CIvPrArtPage::PrintAll ()
{
	CProcess print;
	Form.Get ();
// Iprgrstufk.iprgrstufk.pr_gr_stuf
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11114.llf", tmp);
	}
	else
	{
		dName = "11114.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11114\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "MITRANGE 1\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
//		fprintf (fp, "pr_gr_stuf %ld %ld\n", (long) 0,
//			                                 (long) 99999999);
		fclose (fp);
		Command.Format ("dr70001 -name 11114 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11114";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}


void CIvPrArtPage::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_List.StartPauseEnter ();
}

void CIvPrArtPage::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_List.EndPauseEnter ();
}

void CIvPrArtPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Enable (enable);
	PosControls.Enable (!enable);
}

void CIvPrArtPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CVPreise *iv_pr;
	while ((iv_pr = (CVPreise *) Rows.GetNext ()) != NULL)
	{
		delete iv_pr;
	}
	Rows.Init ();
}

void CIvPrArtPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
// 301111 
	_tcscpy (sys_par.sys_par_nam, "grundartpreis");
  	int dsqlstatus = Sys_par.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		sypgrundartpreis = _tstoi (sys_par.sys_par_wrt);
	}
	else
	{
		sypgrundartpreis = 0;
	}

    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_List.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("RemoveKun", cfg_v) == TRUE)
    {
			RemoveKun = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_List.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_List.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_List.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CIvPrArtPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_List.ListEdit ||
        Control == &m_List.ListComboBox ||
		Control == &m_List.SearchListCtrl.Edit)	
	{
		ListCopy ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CIvPrArtPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CIvPrArtPage::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_List.ListEdit.m_hWnd) ||
		IsWindow (m_List.ListComboBox) ||
		IsWindow (m_List.SearchListCtrl.Edit))
	{
		m_List.StopEnter ();
		m_List.StartEnter (m_List.EditCol, m_List.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
//		for (int i = 0; i < MAXLISTROWS && i < m_List.GetItemCount (); i ++)
		for (int i = 0; i < m_List.GetItemCount (); i ++)
		{
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = "";
			int cols = m_List.GetHeaderCtrl ()->GetItemCount ();
			for (int j = 0; j < cols; j ++)
			{
				if (Row != "")
				{
					Row += Separator;
				}
				CString Field = m_List.GetItemText (i, j);
				Field.TrimRight ();
				Row += Field; 
			}
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}
