#include "stdafx.h"
#include "ChoiceMdn.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceMdn::Sort1 = -1;
int CChoiceMdn::Sort2 = -1;
int CChoiceMdn::Sort3 = -1;

CChoiceMdn::CChoiceMdn(CWnd* pParent) 
        : CChoiceX(pParent)
{
}

CChoiceMdn::~CChoiceMdn() 
{
	DestroyList ();
}

void CChoiceMdn::DestroyList() 
{
	for (std::vector<CMdnList *>::iterator pabl = MdnList.begin (); pabl != MdnList.end (); ++pabl)
	{
		CMdnList *abl = *pabl;
		delete abl;
	}
    MdnList.clear ();
}

void CChoiceMdn::FillList () 
{
    short  mdn;
    TCHAR adr_krz [34];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Mandanten"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Mandant"),  1, 50, LVCFMT_RIGHT);
    SetCol (_T("Name"),  2, 250);

	if (MdnList.size () == 0)
	{
		DbClass->sqlout ((short *)&mdn,      SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  adr_krz,   SQLCHAR, sizeof (adr_krz));
		int cursor = DbClass->sqlcursor (_T("select mdn.mdn, adr.adr_krz ")
			                             _T("from mdn, adr where mdn.mdn > 0 ")
										 _T("and adr.adr = mdn.adr"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) adr_krz;
			CDbUniCode::DbToUniCode (adr_krz, pos);
			CMdnList *abl = new CMdnList (mdn, adr_krz);
			MdnList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CMdnList *>::iterator pabl = MdnList.begin (); pabl != MdnList.end (); ++pabl)
	{
		CMdnList *abl = *pabl;
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString Num;
		CString Bez;
		_tcscpy (adr_krz, abl->adr_krz.GetBuffer ());

		CString LText;
		LText.Format (_T("%hd %s"), abl->mdn, 
									abl->adr_krz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Mdn;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (adr_krz, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort (listView);
}


void CChoiceMdn::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceMdn::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceMdn::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceMdn::SearchAdrKrz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceMdn::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchAdrKrz (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceMdn::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceMdn::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   return 0;
}


void CChoiceMdn::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);

    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CMdnList *abl = MdnList [i];
		   
		   abl->mdn     = _tstoi (ListBox->GetItemText (i, 1));
		   abl->adr_krz = ListBox->GetItemText (i, 2);
	}
}

void CChoiceMdn::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = MdnList [idx];
}

CMdnList *CChoiceMdn::GetSelectedText ()
{
	CMdnList *abl = (CMdnList *) SelectedRow;
	return abl;
}

