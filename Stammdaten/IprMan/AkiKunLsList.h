#ifndef _AKI_KUNLS_LIST_DEF
#define _AKI_KUNLS_LIST_DEF
#pragma once

class CAkiKunLsList
{
public:
	short mdn;
    long aki_nr;
	long kun_pr;
	CString zus_bz;
	CAkiKunLsList(void);
	CAkiKunLsList(short, long, long, LPTSTR);
	~CAkiKunLsList(void);
};
#endif
