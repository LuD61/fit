// IprManDoc.cpp : Implementierung der Klasse CIprManDoc
//

#include "stdafx.h"
#include "IprMan.h"

#include "IprManDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CIprManDoc

IMPLEMENT_DYNCREATE(CIprManDoc, CDocument)

BEGIN_MESSAGE_MAP(CIprManDoc, CDocument)
END_MESSAGE_MAP()


// CIprManDoc Erstellung/Zerst�rung

CIprManDoc::CIprManDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CIprManDoc::~CIprManDoc()
{
}

BOOL CIprManDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CIprManDoc Serialisierung

void CIprManDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CIprManDoc Diagnose

#ifdef _DEBUG
void CIprManDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CIprManDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CIprManDoc-Befehle
