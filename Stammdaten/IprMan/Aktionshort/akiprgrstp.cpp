#include "stdafx.h"
#include "akiprgrstp.h"

struct AKIPRGRSTP akiprgrstp, akiprgrstp_null;

void AKIPRGRSTP_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &akiprgrstp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)  &akiprgrstp.a,      SQLDOUBLE, 0);
//            sqlin ((double *)  &akiprgrstp.me_min, SQLDOUBLE, 0);
    sqlout ((short *) &akiprgrstp.mdn,SQLSHORT,0);
    sqlout ((short *) &akiprgrstp.aki_nr,SQLSHORT,0);
    sqlout ((double *) &akiprgrstp.a,SQLDOUBLE,0);
    sqlout ((double *) &akiprgrstp.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &akiprgrstp.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &akiprgrstp.aki_pr,SQLDOUBLE,0);
    sqlout ((double *) &akiprgrstp.aki_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &akiprgrstp.me_min,SQLDOUBLE,0);
    sqlout ((double *) &akiprgrstp.me_max,SQLDOUBLE,0);
    sqlout ((double *) &akiprgrstp.me,SQLDOUBLE,0);
    sqlout ((short *) &akiprgrstp.waehrung,SQLSHORT,0);
            cursor = sqlcursor (_T("select akiprgrstp.mdn,  ")
_T("akiprgrstp.aki_nr,  akiprgrstp.a,  akiprgrstp.ld_pr,  ")
_T("akiprgrstp.ld_pr_eu,  akiprgrstp.aki_pr,  akiprgrstp.aki_pr_eu,  ")
_T("akiprgrstp.me_min,  akiprgrstp.me_max,  akiprgrstp.me,  ")
_T("akiprgrstp.waehrung from akiprgrstp ")

#line 14 "akiprgrstp.rpp"
                                  _T("where aki_nr = ? ")
				  _T("and a = ? "));
//				  _T("and me_min = ?"));
    sqlin ((short *) &akiprgrstp.mdn,SQLSHORT,0);
    sqlin ((short *) &akiprgrstp.aki_nr,SQLSHORT,0);
    sqlin ((double *) &akiprgrstp.a,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.aki_pr,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.aki_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.me_min,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.me_max,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.me,SQLDOUBLE,0);
    sqlin ((short *) &akiprgrstp.waehrung,SQLSHORT,0);
            sqltext = _T("update akiprgrstp set ")
_T("akiprgrstp.mdn = ?,  akiprgrstp.aki_nr = ?,  akiprgrstp.a = ?,  ")
_T("akiprgrstp.ld_pr = ?,  akiprgrstp.ld_pr_eu = ?,  ")
_T("akiprgrstp.aki_pr = ?,  akiprgrstp.aki_pr_eu = ?,  ")
_T("akiprgrstp.me_min = ?,  akiprgrstp.me_max = ?,  akiprgrstp.me = ?,  ")
_T("akiprgrstp.waehrung = ? ")

#line 18 "akiprgrstp.rpp"
                                  _T("where aki_nr = ? ")
				  _T("and a = ? ");
//				  _T("and me_min = ?");
            sqlin ((short *)   &akiprgrstp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)  &akiprgrstp.a,      SQLDOUBLE, 0);
//            sqlin ((double *)  &akiprgrstp.me_min, SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &akiprgrstp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)  &akiprgrstp.a,      SQLDOUBLE, 0);
//            sqlin ((double *)  &akiprgrstp.me_min, SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select aki_nr from akiprgrstp ")
                                  _T("where aki_nr = ? ")
				  _T("and a = ? "));
//				  _T("and me_min = ?"));
            sqlin ((short *)   &akiprgrstp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)  &akiprgrstp.a,      SQLDOUBLE, 0);
//            sqlin ((double *)  &akiprgrstp.me_min, SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from akiprgrstp ")
                                  _T("where aki_nr = ? ")
				  _T("and a = ? "));
//				  _T("and me_min = ?"));
    sqlin ((short *) &akiprgrstp.mdn,SQLSHORT,0);
    sqlin ((short *) &akiprgrstp.aki_nr,SQLSHORT,0);
    sqlin ((double *) &akiprgrstp.a,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.aki_pr,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.aki_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.me_min,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.me_max,SQLDOUBLE,0);
    sqlin ((double *) &akiprgrstp.me,SQLDOUBLE,0);
    sqlin ((short *) &akiprgrstp.waehrung,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into akiprgrstp (")
_T("mdn,  aki_nr,  a,  ld_pr,  ld_pr_eu,  aki_pr,  aki_pr_eu,  me_min,  me_max,  me,  waehrung) ")

#line 41 "akiprgrstp.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?)")); 

#line 43 "akiprgrstp.rpp"
}
