// IprManView.cpp : Implementierung der Klasse CIprManView
//

#include "stdafx.h"
#include "IprMan.h"

#include "IprManDoc.h"
#include "IprManView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CIprManView

IMPLEMENT_DYNCREATE(CIprManView, CView)

BEGIN_MESSAGE_MAP(CIprManView, CView)
	// Standarddruckbefehle
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CIprManView Erstellung/Zerst�rung

CIprManView::CIprManView()
{
	// TODO: Hier Code zum Erstellen einf�gen

}

CIprManView::~CIprManView()
{
}

BOOL CIprManView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CView::PreCreateWindow(cs);
}

// CIprManView-Zeichnung

void CIprManView::OnDraw(CDC* /*pDC*/)
{
	CIprManDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: Code zum Zeichnen der systemeigenen Daten hinzuf�gen
}


// CIprManView drucken

BOOL CIprManView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void CIprManView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void CIprManView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// CIprManView Diagnose

#ifdef _DEBUG
void CIprManView::AssertValid() const
{
	CView::AssertValid();
}

void CIprManView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CIprManDoc* CIprManView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CIprManDoc)));
	return (CIprManDoc*)m_pDocument;
}
#endif //_DEBUG


// CIprManView Meldungshandler
