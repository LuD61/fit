// Ipr3.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "IprMan.h"
#include "Ipr3.h"


// CIpr3

IMPLEMENT_DYNCREATE(CIpr3, DbFormView)

CIpr3::CIpr3()
	: DbFormView(CIpr3::IDD)
{
	tabx = 0;
	taby = 0;
	StartSize = START_NORMAL;
	Cfg.SetProgName( _T("IPrDialog"));
	ReadCfg ();
}

CIpr3::~CIpr3()
{
}

void CIpr3::DoDataExchange(CDataExchange* pDX)
{
	DbFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CIpr3, DbFormView)
	ON_WM_SIZE ()
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_BACK, OnBack)
//	ON_WM_DRAWITEM()
ON_COMMAND(ID_DELETE, OnDelete)
ON_COMMAND(ID_INSERT, OnInsert)
ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
ON_COMMAND(ID_DELETEALL, OnDeleteall)
ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
ON_COMMAND(ID_PRINT_ALL, OnPrintAll)
END_MESSAGE_MAP()


// CIpr3-Diagnose

#ifdef _DEBUG
void CIpr3::AssertValid() const
{
	DbFormView::AssertValid();
}

void CIpr3::Dump(CDumpContext& dc) const
{
	DbFormView::Dump(dc);
}
#endif //_DEBUG


// CIpr3-Meldungshandler

void CIpr3::OnInitialUpdate()
{
	DbFormView::OnInitialUpdate();


	dlg.Construct (_T(""), this);
/*
	Page1.Construct (IDD_IVPR_ART_PAGE);
	Page1.Frame = this;
	Page1.HideButtons = TRUE;
	dlg.AddPage (&Page1);
*/
	Page1.Construct (IDD_AKI_GR_PAGE);
	Page1.Frame = this;
	Page1.HideButtons = TRUE;
	dlg.AddPage (&Page1);
	Page2.Construct (IDD_AKIKUN_PR_PAGE);
	Page2.Frame = this;
	Page2.HideButtons = TRUE;
	dlg.AddPage (&Page2);
	Page3.Construct (IDD_AKIKUN_PAGE);
	Page3.Frame = this;
	Page3.HideButtons = TRUE;
	dlg.AddPage (&Page3);

//    dlg.m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
    dlg.m_psh.dwSize = sizeof (dlg.m_psh);
	dlg.Create (this, WS_CHILD | WS_VISIBLE);
    Page1.SetFocus (); 
/*
	if (StartSize == START_MAXIMIZED)
	{
		ShowWindow (SW_SHOWMAXIMIZED);
	}
*/

	CRect cRect;
	GetWindowRect (&cRect);
	GetParent ()->ScreenToClient (&cRect);
	MoveWindow (&cRect);

//    dlg.GetClientRect (&cRect);
//    dlg.MoveWindow (0, 0, cRect.right, cRect.bottom);
	CSize Size = GetTotalSize ();
	if (Size.cy < (cRect.bottom) + 10)
	{
		Size.cy = cRect.bottom + 10;
	}
	if (Size.cx < (cRect.right) + 10)
	{
		Size.cx = cRect.right + 10;
	}
	SetScrollSizes (MM_TEXT, Size);
}

void CIpr3::OnSize(UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (dlg.m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	dlg.MoveWindow (tabx, taby, cx, cy);
//	dlg.MoveWindow (0, 0, cx, cy);
	CTabCtrl *tab = dlg.GetTabControl ();
	Page1.Frame = this;
	Page1.GetWindowRect (&pRect);
	dlg.ScreenToClient (&pRect);

	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = cx - 10 - tabx;
	frame.bottom = cy - 10 - taby;
	tab->MoveWindow (&frame);
	frame.top += 20;
	frame.left += 2;
    frame.right -= 4;
	frame.bottom -= 4;

	int page = dlg.GetActiveIndex ();
	if (page == 0)
	{
		Page1.MoveWindow (&frame);
	}
	else if (page == 1)
	{
		Page2.MoveWindow (&frame);
	}
	else if (page == 2)
	{
		Page3.MoveWindow (&frame);
	}
}

HBRUSH CIpr3::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	if (hBrush == NULL)
	{
		hBrush = CreateSolidBrush (Color);
		staticBrush = CreateSolidBrush (Color);
	}
	if (pWnd->IsKindOf( RUNTIME_CLASS( CStatic )))
	{
            pDC->SetBkColor (Color);
 		    return staticBrush;
	}
	return DbFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CIpr3::ReadCfg ()
{
    char cfg_v [256];

    if (Cfg.GetCfgValue ("StartSize", cfg_v) == TRUE)
    {
			StartSize = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CIpr3::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     dlg.Write ();
}

void CIpr3::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     dlg.StepBack ();
}

void CIpr3::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.Delete ();
}

void CIpr3::OnInsert()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.Insert ();
}

void CIpr3::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnCopy ();
}

void CIpr3::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnPaste ();
}

void CIpr3::OnDeleteall()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.DeleteAll ();
}

void CIpr3::OnFilePrint()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.Print ();
}

void CIpr3::OnPrintAll()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.PrintAll ();
}
