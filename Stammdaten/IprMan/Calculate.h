#pragma once
#include "sys_par.h"
#include "a_kalkpreis.h"

class CCalculate
{

private:
	SYS_PAR_CLASS Sys_par;
	A_KALKPREIS_CLASS A_kalkpreis;
	double value1;
	double value2;
	double percent;
	int MarktSpPar;
	double mwst;
public:
	enum
	{
		AufschlagBrutto = 1,
		AufschlagNetto  = 2,
		AbschlagBrutto = 3,
		AbschlagNetto  = 4,
		ReadSysPar     = 5
	};
	CCalculate(void);
	~CCalculate(void);
	void SetValues (double value1, double value2);
	void SetMwst (double mwst);
	double GetPercent ();
	double execute ();
	double execute (double value1, double value2);
	double GetSk (short mdn, short fil, double a);
	double GetEk ();
	double GetVk ();
};
