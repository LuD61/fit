#include "stdafx.h"
#include "akikunprp.h"

struct AKIKUNPRP akikunprp, akikunprp_null;

void AKIKUNPRP_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)    &akikunprp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)   &akikunprp.a,  SQLDOUBLE, 0);
    sqlout ((short *) &akikunprp.mdn,SQLSHORT,0);
    sqlout ((long *) &akikunprp.kun,SQLLONG,0);
    sqlout ((double *) &akikunprp.a,SQLDOUBLE,0);
    sqlout ((double *) &akikunprp.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &akikunprp.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &akikunprp.aki_pr,SQLDOUBLE,0);
    sqlout ((double *) &akikunprp.aki_pr_eu,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &akikunprp.aki_von,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &akikunprp.aki_bis,SQLDATE,0);
    sqlout ((short *) &akikunprp.waehrung,SQLSHORT,0);
    sqlout ((short *) &akikunprp.aki_nr,SQLSHORT,0);
    sqlout ((double *) &akikunprp.sa_rab,SQLDOUBLE,0);
    sqlout ((short *) &akikunprp.sa_kz,SQLSHORT,0);
            cursor = sqlcursor (_T("select akikunprp.mdn,  ")
_T("akikunprp.kun,  akikunprp.a,  akikunprp.ld_pr,  akikunprp.ld_pr_eu,  ")
_T("akikunprp.aki_pr,  akikunprp.aki_pr_eu,  akikunprp.aki_von,  ")
_T("akikunprp.aki_bis,  akikunprp.waehrung,  akikunprp.aki_nr,  ")
_T("akikunprp.sa_rab,  akikunprp.sa_kz from akikunprp ")

#line 13 "akikunprp.rpp"
                                  _T("where aki_nr = ? ")
			          _T("and a = ?"));
    sqlin ((short *) &akikunprp.mdn,SQLSHORT,0);
    sqlin ((long *) &akikunprp.kun,SQLLONG,0);
    sqlin ((double *) &akikunprp.a,SQLDOUBLE,0);
    sqlin ((double *) &akikunprp.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &akikunprp.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &akikunprp.aki_pr,SQLDOUBLE,0);
    sqlin ((double *) &akikunprp.aki_pr_eu,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &akikunprp.aki_von,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &akikunprp.aki_bis,SQLDATE,0);
    sqlin ((short *) &akikunprp.waehrung,SQLSHORT,0);
    sqlin ((short *) &akikunprp.aki_nr,SQLSHORT,0);
    sqlin ((double *) &akikunprp.sa_rab,SQLDOUBLE,0);
    sqlin ((short *) &akikunprp.sa_kz,SQLSHORT,0);
            sqltext = _T("update akikunprp set ")
_T("akikunprp.mdn = ?,  akikunprp.kun = ?,  akikunprp.a = ?,  ")
_T("akikunprp.ld_pr = ?,  akikunprp.ld_pr_eu = ?,  ")
_T("akikunprp.aki_pr = ?,  akikunprp.aki_pr_eu = ?,  ")
_T("akikunprp.aki_von = ?,  akikunprp.aki_bis = ?,  ")
_T("akikunprp.waehrung = ?,  akikunprp.aki_nr = ?,  ")
_T("akikunprp.sa_rab = ?,  akikunprp.sa_kz = ? ")

#line 16 "akikunprp.rpp"
                                  _T("where aki_nr = ? ")
			          _T("and a = ?");
            sqlin ((short *)    &akikunprp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)   &akikunprp.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)    &akikunprp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)   &akikunprp.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select aki_nr from akikunprp ")
                                  _T("where aki_nr = ? ")
			          _T("and a = ? for update"));
            sqlin ((short *)    &akikunprp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)   &akikunprp.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from akikunprp ")
                                  _T("where aki_nr = ? ")
			          _T("and a = ?"));
    sqlin ((short *) &akikunprp.mdn,SQLSHORT,0);
    sqlin ((long *) &akikunprp.kun,SQLLONG,0);
    sqlin ((double *) &akikunprp.a,SQLDOUBLE,0);
    sqlin ((double *) &akikunprp.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &akikunprp.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &akikunprp.aki_pr,SQLDOUBLE,0);
    sqlin ((double *) &akikunprp.aki_pr_eu,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &akikunprp.aki_von,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &akikunprp.aki_bis,SQLDATE,0);
    sqlin ((short *) &akikunprp.waehrung,SQLSHORT,0);
    sqlin ((short *) &akikunprp.aki_nr,SQLSHORT,0);
    sqlin ((double *) &akikunprp.sa_rab,SQLDOUBLE,0);
    sqlin ((short *) &akikunprp.sa_kz,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into akikunprp (")
_T("mdn,  kun,  a,  ld_pr,  ld_pr_eu,  aki_pr,  aki_pr_eu,  aki_von,  aki_bis,  waehrung,  ")
_T("aki_nr,  sa_rab,  sa_kz) ")

#line 33 "akikunprp.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?)")); 

#line 35 "akikunprp.rpp"
}
