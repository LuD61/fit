// ChildFrm.cpp : Implementierung der Klasse CChildFrame
//
#include "stdafx.h"
#include "IprMan.h"

#include "MainFrm.h"
#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CVector CChildFrame::StdPrWnd;
CVector CChildFrame::NewPrWnd;
CVector CChildFrame::AkiPrWnd;

// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	ON_WM_SIZE ()
	ON_WM_DESTROY ()
END_MESSAGE_MAP()


// CChildFrame Erstellung/Zerst�rung

CChildFrame::CChildFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
	InitSize = FALSE;
	StdPrWnd.Init ();
	NewPrWnd.Init ();
	AkiPrWnd.Init ();
}

CChildFrame::~CChildFrame()
{
}


BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie die Fensterklasse oder die Stile hier, indem Sie CREATESTRUCT �ndern
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}

BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	lpcs->cy = 500;
	BOOL ret = CMDIChildWnd::OnCreateClient(lpcs,   pContext);	

	CStringA ClassName = pContext->m_pNewViewClass->m_lpszClassName;
	if (ClassName == "CIpr1")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->StdPrWnd == NULL)
		{
			MainFrm->StdPrWnd = this;
		}
		else
		{
			StdPrWnd.Add (this);
		}
	}
	else if (ClassName == "CIpr2")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->NewPrWnd == NULL)
		{
			MainFrm->NewPrWnd = this;
		}
		else
		{
			NewPrWnd.Add (this);
		}
	}
	else if (ClassName == "CIpr3")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->NewPrWnd == NULL)
		{
			MainFrm->AkiPrWnd = this;
		}
		else
		{
			AkiPrWnd.Add (this);
		}
	}
	MDIMaximize ();
	
	return ret;
}


void CChildFrame::OnSize(UINT nType, int cx, int cy)
{

	CMDIChildWnd::OnSize (nType, cx, cy);
	if (!InitSize)
	{
		    CRect pRect;
			GetParent ()->GetClientRect (pRect);
			CRect rect;
			GetWindowRect (&rect);
			GetParent ()->ScreenToClient (&rect);
			rect.bottom = pRect.bottom;
			rect.right += 55;
			rect.top = 0;
			InitSize = TRUE;
			MoveWindow (&rect, TRUE);
	}
}

// CChildFrame Diagnose

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// CChildFrame Meldungshandler

void CChildFrame::OnDestroy()
{
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	if (MainFrm->StdPrWnd == this)
	{
		MainFrm->StdPrWnd = NULL;
		if (StdPrWnd.GetCount () != 0)
		{
			MainFrm->StdPrWnd = (CWnd *) StdPrWnd.Get (0);
			StdPrWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	else if (MainFrm->NewPrWnd == this)
	{
		MainFrm->NewPrWnd = NULL;
		if (NewPrWnd.GetCount () != 0)
		{
			MainFrm->NewPrWnd = (CWnd *) NewPrWnd.Get (0);
			NewPrWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	else if (MainFrm->AkiPrWnd == this)
	{
		MainFrm->AkiPrWnd = NULL;
		if (AkiPrWnd.GetCount () != 0)
		{
			MainFrm->AkiPrWnd = (CWnd *) AkiPrWnd.Get (0);
			AkiPrWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	int i = -1;
	if ((i = StdPrWnd.Find (this)) != -1)
	{
		StdPrWnd.Drop (i);
		InitSize = FALSE;
	}
	else if ((i = NewPrWnd.Find (this)) != -1)
	{
		NewPrWnd.Drop (i);
		InitSize = FALSE;
	}
	else if ((i = AkiPrWnd.Find (this)) != -1)
	{
		AkiPrWnd.Drop (i);
		InitSize = FALSE;
	}
}

