#pragma once
#include "DbFormView.h"
#include "KunPrSheet.h"
#include "IvPrArtPage.h"
#include "IvPrGrPage.h"
#include "IvPrKunPage.h"
#include "IvKunPage.h"
#include "KunPage.h"
#include "mo_progcfg.h"



// CIpr2-Formularansicht

class CIpr2 : public DbFormView
{
	DECLARE_DYNCREATE(CIpr2)

protected:
	CIpr2();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CIpr2();

public:
	enum { IDD = IDD_IPR2 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    afx_msg void OnSize(UINT, int, int);
	DECLARE_MESSAGE_MAP()
public:
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	int StartSize;
	int tabx;
	int taby;
	CKunPrSheet dlg;
	CIvPrArtPage Page1;
	CIvPrGrPage Page2;
	CIvPrKunPage Page3;
	CIvKunPage Page4;
	HBRUSH hBrush;
	HBRUSH staticBrush;
	void DeletePropertySheet ();
	afx_msg void OnLanguage();
	afx_msg void OnFileSave();
	afx_msg void OnBack();
	void ReadCfg ();

	afx_msg void OnDelete();
	afx_msg void OnInsert();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnDeleteall();
	afx_msg void OnFilePrint();
	afx_msg void OnPrintAll();
};


