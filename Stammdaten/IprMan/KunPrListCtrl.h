#pragma once
#include <vector>
#include "editlistctrl.h"
#include "A_bas.h"
#include "Ipr.h"
#include "ChoiceA.h"
#include "Vector.h"
#include "Preise.h"

#define MAXLISTROWS 30

class CKunPrListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:
	enum LISTPOS
	{
		POSA     = 1,
		POSABZ1  = 2,
		POSVKPR  = 3,
		POSLDPR  = 4,
		POSEKABS  = 6,
		POSEKPROZ = 7,
		POSSK     = 8,
		POSSP     = 9,
		POSRABKZ  = 8,
		POSRAB = 9,
		POSA_GRUND = 5,	// 301111 von 9 auf 5 am 170712
	};

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
		AKTION   = 2,
	};

	int PosA;
    int PosABz1;
	int PosVkPr;
	int PosLdPr;
	int PosEkAbs;
	int PosEkProz;
	int PosRabKz;
    int PosRab;
	int PosSk;
	int PosSp;
	int PosA_grund;	// 301111

    int *Position[12];	// 301111 11->12

	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	BOOL ActiveListRow;
	short m_Mdn;
	short mdn;
    long pr_gr_stuf;
    long kun_pr;
    long kun;
	int oldsel;
	std::vector<BOOL> vSelect;
	CChoiceA *ChoiceA;
	BOOL ModalChoiceA;
	BOOL AChoiceStat;
	CVector ListRows;

	A_BAS_CLASS A_bas;
	IPR_CLASS Ipr;
	double sk_vollk;
	double spanne;
	CKunPrListCtrl(void);
	~CKunPrListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	void OnAChoice (CString&);
	void OnChoice ();
	void OnKey9 ();
    void ReadABz1 ();
    void GetColValue (int row, int col, CString& Text);
    void TestIprIndex ();
	void ScrollPositions (int pos);
	void DestroyRows(CVector &Rows);
	void GrundArtikelpreise (CPreise *p ,int EditRow, int EditCol );	// 301111

	BOOL LastCol ();
};
