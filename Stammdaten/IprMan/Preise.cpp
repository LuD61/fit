#include "StdAfx.h"
#include "preise.h"

CPreise::CPreise(void)
{
	cEk = _T("0.0");
	cVk = _T("0.0");
	memcpy (&ipr, &ipr_null, sizeof (IPR));
}

CPreise::CPreise(CString& cEk, CString& cVk, IPR& ipr)
{
	this->cEk = cEk.Trim ();
	this->cVk = cVk.Trim ();
	memcpy (&this->ipr, &ipr, sizeof (IPR));
}

CPreise::~CPreise(void)
{
}
