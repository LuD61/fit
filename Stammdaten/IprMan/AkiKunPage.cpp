// AktPrGrPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "IprMan.h"
#include "AkiKunPage.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Decimal.h"
#include "Process.h"
#include "DbTime.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

// CAkiKunPage Dialogfeld

CAkiKunPage::CAkiKunPage(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage()
{
	Choice = NULL;
	ModalChoice = TRUE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ChoiceKun = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	IprCursor = -1;
	IprDelCursor = -1;
	IprGrStufkCursor = -1;
	IKunPrkCursor = -1;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_AkiNr);
	PosControls.Add (&m_ZusBez);
	PosControls.Add (&m_Kun);
	PosControls.Add (&m_KunKrz1);
	PosControls.Add (&m_AkiVon);
	PosControls.Add (&m_AkiBis);
	PosControls.Add (&m_KunPrList);

	ButtonControls.Add (&m_Cancel);
	ButtonControls.Add (&m_Save);
	ButtonControls.Add (&m_Delete);
	ButtonControls.Add (&m_Insert);

    	HideButtons = FALSE;

	Frame = NULL;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Search = _T("");
	Separator = _T(";");
	CellHeight = 0;
	Cfg.SetProgName( _T("IPrDialog"));
}

CAkiKunPage::CAkiKunPage(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	ChoiceMdn = NULL;
	ChoiceKun = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	Frame = NULL;
	IprCursor = -1;
	IprDelCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
}

CAkiKunPage::~CAkiKunPage()
{
	Font.DeleteObject ();
	Akikunprp.rollbackwork ();
	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	Akikunprp.dbclose ();
	Ipr.dbclose ();
	Akikunprk.dbclose ();
	Kun.dbclose ();
	KunAdr.dbclose ();
	if (IprCursor == -1)
	{
		Ipr.sqlclose (IprCursor);
	}
	if (IprDelCursor == -1)
	{
		Ipr.sqlclose (IprDelCursor);
	}
	if (IprGrStufkCursor == -1)
	{
		A_bas.sqlclose (IprGrStufkCursor);
	}
	if (IKunPrkCursor == -1)
	{
		A_bas.sqlclose (IKunPrkCursor);
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);

}

void CAkiKunPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LAKI_NR, m_LAkiNr);
	DDX_Control(pDX, IDC_AKI_NR, m_AkiNr);
	DDX_Control(pDX, IDC_LZUS_BEZ, m_LZusBez);
	DDX_Control(pDX, IDC_ZUS_BEZ, m_ZusBez);
	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_LKUN_KRZ1, m_LKunKrz1);
	DDX_Control(pDX, IDC_KUN_KRZ1, m_KunKrz1);
	DDX_Control(pDX, IDC_LAKI_VON, m_LAkiVon);
	DDX_Control(pDX, IDC_AKI_VON, m_AkiVon);
	DDX_Control(pDX, IDC_LAKI_BIS, m_LAkiBis);
	DDX_Control(pDX, IDC_AKI_BIS, m_AkiBis);
	DDX_Control(pDX, IDC_PRGR_LIST, m_KunPrList);

	DDX_Control(pDX, IDC_CANCEL, m_Cancel);
	DDX_Control(pDX, IDC_SAVE, m_Save);
	DDX_Control(pDX, IDC_DELETE, m_Delete);
	DDX_Control(pDX, IDC_INSERT, m_Insert);

}

BEGIN_MESSAGE_MAP(CAkiKunPage, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_SIZE ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_KUNCHOICE ,  OnKunchoice)
	ON_BN_CLICKED(IDC_AKINRCHOICE ,  OnChoice)
	ON_BN_CLICKED(IDC_CANCEL ,   OnCancel)
	ON_BN_CLICKED(IDC_SAVE ,     OnSave)
	ON_BN_CLICKED(IDC_DELETE ,   OnDelete)
	ON_BN_CLICKED(IDC_INSERT ,   OnInsert)
	ON_COMMAND (SELECTED, OnSelected)
	ON_COMMAND (CANCELED, OnCanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
END_MESSAGE_MAP()


// CAkiKunPage Meldungshandler

BOOL CAkiKunPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
//	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
//	ASSERT(IDM_ABOUTBOX < 0xF000);

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	A_bas.opendbase (_T("bws"));

	CUtil::GetPersName (PersName);
//	PgrProt.Construct (PersName, CString ("11121"), &Ivpr);

	ReadCfg ();

	if (HideButtons)
	{
		ButtonControls.SetVisible (FALSE);
		RightListSpace = 15;
	}
	else
	{
		ButtonControls.SetVisible (TRUE);
		RightListSpace = 125;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	m_KunKrz1.SetReadOnly ();
	m_KunKrz1.ModifyStyle (WS_TABSTOP, 0);

	m_Cancel.SetWindowText (_T("Beenden"));
    HBITMAP HbF5 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F5T", "F5MASKT");
	m_Cancel.SetBitmap (HbF5);

    HBITMAP HbF12 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F12T", "F12MASKT");
	m_Save.SetBitmap (HbF12);

    HBITMAP HbDel = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "DELT", "DELMASKT");
	m_Delete.SetBitmap (HbDel);

    HBITMAP HbInsert = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "INSERTT", "INSERTMASKT");
	m_Insert.SetBitmap (HbInsert);

    memcpy (&Akikunprk.akikunprk, &akikunprk_null, sizeof (AKIKUNPRK));
    memcpy (&Akikunprp.akikunprp, &akikunprp_null, sizeof (AKIKUNPRP));
	memcpy (&Kun.kun, &kun_null, sizeof (KUN));

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_AkiNr,EDIT,      (short *) &Akikunprk.akikunprk.aki_nr, VSHORT));
    Form.Add (new CFormField (&m_ZusBez,EDIT,     (char *) &Akikunprk.akikunprk.zus_bz, VCHAR));
    Form.Add (new CFormField (&m_AkiVon,DATETIMEPICKER,  (DATE_STRUCT *) &Akikunprk.akikunprk.aki_von, VDATE));
    Form.Add (new CFormField (&m_AkiBis,DATETIMEPICKER,  (DATE_STRUCT *) &Akikunprk.akikunprk.aki_bis, VDATE));
    Form.Add (new CFormField (&m_Kun,EDIT,        (long *) &Kun.kun.kun, VLONG));
    Form.Add (new CUniFormField (&m_KunKrz1,EDIT, (char *) Kun.kun.kun_krz1, VCHAR));
    
	Akikunprp.sqlin ((short *)  &Akikunprp.akikunprp.aki_nr, SQLSHORT, 0);
	Akikunprp.sqlout ((double *) &Akikunprp.akikunprp.a,  SQLDOUBLE, 0);
//	Akikunprp.sqlout ((double *) &Akikunprp.akikunprp.me_min,  SQLDOUBLE, 0);
	IprCursor = Akikunprp.sqlcursor (_T("select a from akikunprp ")
		                       _T("where aki_nr = ? "));
	Akikunprp.sqlin ((short *)  &Akikunprp.akikunprp.aki_nr, SQLSHORT, 0);
	IprDelCursor = Akikunprp.sqlcursor (_T("delete from akikunprp ")
		                 	     _T("where aki_nr = ? "));

	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_KunPrList.SetImageList (&image, LVSIL_SMALL);   
	}

	m_KunPrList.Mode = m_KunPrList.AKTION;
	FillList = m_KunPrList;
	FillList.SetStyle (LVS_REPORT);
	if (m_KunPrList.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Artikel"),            m_KunPrList.PosA, 130, LVCFMT_RIGHT);
	FillList.SetCol (_T("Bezeichnung"),        m_KunPrList.PosABz1, 250, LVCFMT_LEFT);
	FillList.SetCol (_T("VK-Preis Aktion"),    m_KunPrList.PosVkPr, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Laden-Preis Aktion"), m_KunPrList.PosLdPr, 100, LVCFMT_RIGHT);
	FillList.SetCol (_T("VK-Pr."),             m_KunPrList.PosVkPr + 3, 80, LVCFMT_RIGHT);	// 230812 2->3
	FillList.SetCol (_T("Laden-Pr."),          m_KunPrList.PosLdPr + 3, 100, LVCFMT_RIGHT);	// 230812 2->3
	FillList.SetCol (_T("Rabatt-Kz"),          m_KunPrList.PosRabKz,  80, LVCFMT_CENTER);
	FillList.SetCol (_T("Rabatt"),             m_KunPrList.PosRab, 100, LVCFMT_RIGHT);

	FillList.SetCol (_T(""),					m_KunPrList.PosA_grund, 0, LVCFMT_RIGHT);	// 230812

	m_KunPrList.ColType.Add (new CColType (m_KunPrList.PosRabKz, m_KunPrList.CheckBox));

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	AkiNrGrid.Create (this, 2, 2);
    AkiNrGrid.SetBorder (0, 0);
    AkiNrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_AkiNr = new CCtrlInfo (&m_AkiNr, 0, 0, 1, 1);
	AkiNrGrid.Add (c_AkiNr);
	CtrlGrid.CreateChoiceButton (m_AkiNrChoice, IDC_AKINRCHOICE, this);
	CCtrlInfo *c_AkiNrChoice = new CCtrlInfo (&m_AkiNrChoice, 1, 0, 1, 1);
	AkiNrGrid.Add (c_AkiNrChoice);

	KunGrid.Create (this, 1, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	CtrlGrid.CreateChoiceButton (m_KunChoice, IDC_KUNCHOICE, this);
	CCtrlInfo *c_KunChoice = new CCtrlInfo (&m_KunChoice, 1, 0, 1, 1);
	KunGrid.Add (c_KunChoice);

	ButtonGrid.Create (this, 5, 5);
    ButtonGrid.SetBorder (0, 0);
    ButtonGrid.SetCellHeight (20);
    ButtonGrid.SetGridSpace (0, 10);
	CCtrlInfo *c_Cancel = new CCtrlInfo (&m_Cancel, 0, 0, 1, 1);
	ButtonGrid.Add (c_Cancel);
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 1, 1, 1);
	ButtonGrid.Add (c_Save);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ButtonGrid.Add (c_Delete);
	CCtrlInfo *c_Insert = new CCtrlInfo (&m_Insert, 0, 3, 1, 1);
	ButtonGrid.Add (c_Insert);

	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LAkiNr     = new CCtrlInfo (&m_LAkiNr, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LAkiNr);
	CCtrlInfo *c_AkiNrGrid   = new CCtrlInfo (&AkiNrGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_AkiNrGrid);
	CCtrlInfo *c_LZusBez     = new CCtrlInfo (&m_LZusBez, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LZusBez);
	CCtrlInfo *c_ZusBez     = new CCtrlInfo (&m_ZusBez, 1, 2, 3, 1); 
	CtrlGrid.Add (c_ZusBez);

	CCtrlInfo *c_LKun     = new CCtrlInfo (&m_LKun, 0, 3, 1, 1); 
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid   = new CCtrlInfo (&KunGrid, 1, 3, 1, 1); 
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_LKunKrz1  = new CCtrlInfo (&m_LKunKrz1, 0, 4, 1, 1); 
	CtrlGrid.Add (c_LKunKrz1);
	CCtrlInfo *c_KunKrz1  = new CCtrlInfo (&m_KunKrz1, 1, 4, 3, 1); 
	CtrlGrid.Add (c_KunKrz1);

	CCtrlInfo *c_LAkiVon  = new CCtrlInfo (&m_LAkiVon, 0, 5, 1, 1); 
	CtrlGrid.Add (c_LAkiVon);
	CCtrlInfo *c_AkiVon  = new CCtrlInfo (&m_AkiVon, 1, 5, 1, 1); 
	CtrlGrid.Add (c_AkiVon);
	CCtrlInfo *c_LAkiBis  = new CCtrlInfo (&m_LAkiBis, 0, 6, 1, 1); 
	CtrlGrid.Add (c_LAkiBis);
	CCtrlInfo *c_AkiBis  = new CCtrlInfo (&m_AkiBis, 1, 6, 1, 1); 
	CtrlGrid.Add (c_AkiBis);

	CCtrlInfo *c_ButtonGrid = new CCtrlInfo (&ButtonGrid, DOCKRIGHT, 4, 1, 4); 
	CtrlGrid.Add (c_ButtonGrid);
	CCtrlInfo *c_PrList  = new CCtrlInfo (&m_KunPrList, 0, 8, DOCKRIGHT, DOCKBOTTOM); 
	c_PrList->rightspace = RightListSpace;
	CtrlGrid.Add (c_PrList);

	SetFont (&Font);
        CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
	Form.Show ();
	ReadMdn ();
	EnableHeadControls (TRUE);
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}


void CAkiKunPage::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CAkiKunPage::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CAkiKunPage::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CAkiKunPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CAkiKunPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_KunPrList.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_KunPrList &&
					GetFocus ()->GetParent () != &m_KunPrList )
				{

					break;
			    }
				m_KunPrList.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				m_KunPrList.OnKeyD (VK_DOWN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				m_KunPrList.OnKeyD (VK_UP);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnKunchoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_AkiNr)
				{
					OnChoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Kun)
				{
					OnKunchoice ();
					return TRUE;
				}
				m_KunPrList.OnKey9 ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CAkiKunPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_AkiNr)
	{
		if (!Read ())
		{
			m_AkiNr.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_Kun)
	{
		if (!ReadKun ())
		{
			m_Kun.SetFocus ();
			return FALSE;
		}
	}

	if (Control != &m_KunPrList &&
		Control->GetParent ()!= &m_KunPrList)
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CAkiKunPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_KunPrList &&
		Control->GetParent ()!= &m_KunPrList )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_KunPrList.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}


BOOL CAkiKunPage::ReadMdn ()
{
	int mdn;

	mdn = Mdn.mdn.mdn;
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Choice != NULL && mdn != Mdn.mdn.mdn)
	{
		delete Choice;
		Choice = NULL;
	}

	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		FillPrGrStufCombo ();
		FillKunPrCombo ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CAkiKunPage::Read ()
{
	if (ModalChoice)
	{
		CString cAkiNr;
		m_AkiNr.GetWindowText (cAkiNr);
		if (!CStrFuncs::IsDecimal (cAkiNr))
		{
			Search = cAkiNr;
//			OnAkiNrchoice ();
			Search = "";
			if (!ChoiceStat)
			{
				m_AkiNr.SetFocus ();
				m_AkiNr.SetSel (0, -1);
				return FALSE;
			}
		}
	}
	memcpy (&Akikunprk.akikunprk, &akikunprk_null, sizeof (AKIKUNPRK));
	Form.Get ();
	Akikunprk.akikunprk.mdn = Mdn.mdn.mdn;
	if (Akikunprk.akikunprk.aki_nr == 0)
	{
		MessageBox (_T("Aktionsnummer > 0 eingeben"), NULL, MB_OK | MB_ICONERROR);
		m_AkiNr.SetFocus ();
		return FALSE;
	}
	Akikunprp.beginwork ();
    if (Akikunprk.dblock () < 0)
	{
		MessageBox ("Die Aktion wird an einem anderen Arbeitsplatz bearbeitet");
		Akikunprp.commitwork ();
		return FALSE;
	}

    if (Akikunprk.dbreadfirst () == 0)
	{
		Kun.kun.kun = Akikunprk.akikunprk.kun;
		Form.Show (); 
		ReadKun ();
	}
	if (ReadList ())
	{
		EnableHeadControls (FALSE);
		return TRUE;
	}
	return FALSE;
}

BOOL CAkiKunPage::ReadKun ()
{
	memcpy (&Kun.kun, &kun_null, sizeof (KUN));
	Form.Get ();
	strcpy (Kun.kun.kun_krz1, ""); 
	Kun.kun.mdn = Mdn.mdn.mdn;
	if (Kun.dbreadfirst () == 0)
	{
		Akikunprk.akikunprk.kun      = Kun.kun.kun;
	}
	else
	{
		MessageBox (_T("Kunde nicht gefunden"), NULL, MB_OK | MB_ICONERROR);
		return FALSE;
	}

	Form.Show ();
	return TRUE;
}

BOOL CAkiKunPage::LockList ()
{
	extern short sql_mode;
	BOOL ret = TRUE;
	Akikunprp.beginwork ();
	short sqls = sql_mode;
	sql_mode = 1;

	Akikunprp.sqlopen (IprCursor);
	while (Akikunprp.sqlfetch (IprCursor) == 0)
	{
		int dsqlstatus = Akikunprp.dbreadfirst ();
		if (dsqlstatus == 0)
		{
		      dsqlstatus = Akikunprp.dbupdate ();
		}
		if (dsqlstatus < 0)
		{
			ret = FALSE;
			break;
		}
	}
	sql_mode = sqls;
	return ret;
}

BOOL CAkiKunPage::ReadList ()
{
	m_KunPrList.DeleteAllItems ();
	m_KunPrList.vSelect.clear ();
	int i = 0;
	memcpy (&Akikunprp.akikunprp, &akikunprp_null, sizeof (AKIKUNPRP));
	Akikunprp.akikunprp.mdn = Mdn.mdn.mdn;
	Akikunprp.akikunprp.aki_nr = Akikunprk.akikunprk.aki_nr;
	m_KunPrList.mdn = Mdn.mdn.mdn;
	m_KunPrList.pr_gr_stuf = Kun.kun.pr_stu;
	m_KunPrList.kun_pr = Kun.kun.pr_lst;
	m_KunPrList.kun = Kun.kun.kun; 

	if (!LockList ())
	{
		MessageBox (_T("Der Kunde wird im Moment von einem anderen Benutzer bearbeitet"), _T(""),
			        MB_OK | MB_ICONERROR);
		return FALSE;
	}

	DestroyRows (DbRows);
	DestroyRows (ListRows);
	Akikunprp.sqlopen (IprCursor);
	while (Akikunprp.sqlfetch (IprCursor) == 0)
	{
		Akikunprp.dbreadfirst ();
		memcpy (&Ipr.ipr, &ipr_null, sizeof (IPR));
		Ipr.ipr.mdn          = Mdn.mdn.mdn;
		Ipr.ipr.kun          = Akikunprk.akikunprk.kun;
		Ipr.ipr.kun_pr       = Kun.kun.pr_lst;
		Ipr.ipr.pr_gr_stuf   = Kun.kun.pr_stu;
		Ipr.ipr.a	     = Akikunprp.akikunprp.a;
		if (Ipr.dbreadfirst () == 100)
                {
			Ipr.ipr.kun_pr       = 0;
			Ipr.ipr.pr_gr_stuf   = 0;
		        Ipr.dbreadfirst ();
                }
		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas.a_bas.a = Akikunprp.akikunprp.a;
		A_bas.dbreadfirst ();
		Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 301111
		FillList.InsertItem (i, 0);
		CString A;
		A.Format (_T("%.0lf"), Akikunprp.akikunprp.a);
		FillList.SetItemText (A.GetBuffer (), i, m_KunPrList.PosA);
		CString ABz1;
		ABz1 = A_bas.a_bas.a_bz1;
		ABz1 += _T("   ");
		ABz1 += A_bas.a_bas.a_bz2;
		FillList.SetItemText (ABz1.GetBuffer (), i, m_KunPrList.PosABz1);
		CString VkPr;
		m_KunPrList.DoubleToString (Akikunprp.akikunprp.aki_pr_eu, VkPr, 4);
//		CDecimal *decVK = new CDecimal (VkPr);
//		delete decVK;
		Akikunprp.akikunprp.aki_pr_eu = CStrFuncs::StrToDouble (VkPr);
		FillList.SetItemText (VkPr.GetBuffer (), i, m_KunPrList.PosVkPr);
		CString LdPr;
		m_KunPrList.DoubleToString (Akikunprp.akikunprp.ld_pr_eu, LdPr, 2);
		FillList.SetItemText (LdPr.GetBuffer (), i, m_KunPrList.PosLdPr);

		m_KunPrList.DoubleToString (Ipr.ipr.vk_pr_eu, VkPr, 4);
		FillList.SetItemText (VkPr.GetBuffer (), i, m_KunPrList.PosVkPr + 2);
		m_KunPrList.DoubleToString (Ipr.ipr.ld_pr_eu, LdPr, 2);
		FillList.SetItemText (LdPr.GetBuffer (), i, m_KunPrList.PosLdPr + 2);

		if (Akikunprp.akikunprp.sa_kz)
		{
			FillList.SetItemText ("X", i, m_KunPrList.PosRabKz);
		}
		else
		{
			FillList.SetItemText (" ", i, m_KunPrList.PosRabKz);
		}

		CString Rab;
		m_KunPrList.DoubleToString (Akikunprp.akikunprp.sa_rab, Rab,2);
		FillList.SetItemText (Rab.GetBuffer (), i, m_KunPrList.PosRab);

		CAkiKunPr *aki_pr = new CAkiKunPr (VkPr, LdPr, Akikunprp.akikunprp);
		DbRows.Add (aki_pr);
		i ++;
	}
	return TRUE;
}

BOOL CAkiKunPage::IsChanged (CAkiKunPr *pIpr)
{
	DbRows.FirstPosition ();
	CAkiKunPr *aki_pr;
	while ((aki_pr = (CAkiKunPr *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Akikunprp.akikunprp, &pIpr->akikunprp, sizeof (AKIKUNPRP));
		if (Akikunprp.akikunprp.aki_nr == aki_pr->akikunprp.aki_nr &&
			Akikunprp.akikunprp.a == aki_pr->akikunprp.a) break;
	}
	if (aki_pr == NULL)
	{
		return TRUE;
	}
	if (pIpr->cEk != aki_pr->cEk) return TRUE;
	if (pIpr->cVk != aki_pr->cVk) return TRUE;
	return FALSE;
}

BOOL CAkiKunPage::InList (AKIKUNPRP_CLASS& Akikunprp)
{
	ListRows.FirstPosition ();
	CAkiKunPr *aki_pr;
	while ((aki_pr = (CAkiKunPr *) ListRows.GetNext ()) != NULL)
	{
		if (Akikunprp.akikunprp.aki_nr == aki_pr->akikunprp.aki_nr &&
			Akikunprp.akikunprp.a == aki_pr->akikunprp.a) return TRUE;
	}
    return FALSE;
}

// 301111 A
int  CAkiKunPage::testgrundpreis ( bool schongefragt ,  double ia ) 
{


	A_bas.a_bas.a = ia;
	A_bas.dbreadfirst ();

	if (sypgrundartpreis == 1 && A_bas.a_bas.me_einh != 2) return 0; //240412 Nur grundartpreis == 1 hat Einschr�nkung auf me_einh == 2, grundartpreis == 2 hat diese nicht
	if ( A_bas.a_bas.a_grund == A_bas.a_bas.a && A_bas.a_bas.a > 0 )
	{

		if ( schongefragt ) return 1 ;

		if (MessageBox (_T("�nderung auch f�r Artikelvarianten ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==	IDNO)
		{
			return -1;	// NEIN und nix machen
		}
		else
			return  1; // JA und was machen
	}
	return 0; // unentschieden
}
// 301111 E

void CAkiKunPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CAkiKunPr *aki_pr;
	while ((aki_pr = (CAkiKunPr *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Akikunprp.akikunprp, &aki_pr->akikunprp, sizeof (AKIKUNPRP));
		if (!InList (Akikunprp))
		{
			Akikunprp.dbdelete ();
//			PgrProt.Write (1);
		}
	}
}

BOOL CAkiKunPage::Write ()
{
	extern short sql_mode;
	short sql_s;

	if (!TerminOK ())
	{
		m_KunPrList.StopEnter ();
//		EnableHeadControls (TRUE);
		m_AkiVon.SetFocus ();
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
//	Akikunprp.beginwork ();
	m_KunPrList.StopEnter ();
	int count = m_KunPrList.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 AKIKUNPRP *akikunprp = new AKIKUNPRP;
		 memcpy (akikunprp, &akikunprp_null, sizeof (AKIKUNPRP));
         CString Text;
		 Text = m_KunPrList.GetItemText (i, m_KunPrList.PosA);
		 akikunprp->a = CStrFuncs::StrToDouble (Text);
     	 CString VkPr =  m_KunPrList.GetItemText (i, m_KunPrList.PosVkPr);
		 akikunprp->aki_pr_eu = CStrFuncs::StrToDouble (VkPr);
		 akikunprp->aki_pr = akikunprp->aki_pr_eu;
		 CString LdPr =  m_KunPrList.GetItemText (i, m_KunPrList.PosLdPr);
		 akikunprp->ld_pr_eu = CStrFuncs::StrToDouble (LdPr);
		 akikunprp->ld_pr = akikunprp->ld_pr_eu;

         CString RabKz = m_KunPrList.GetItemText (i, m_KunPrList.PosRabKz);
		 if (RabKz == _T("X"))
		 {
			 akikunprp->sa_kz = 1;
		 }
		 else
		 {
			 akikunprp->sa_kz = 0;
		 }
		 CString Rab =  m_KunPrList.GetItemText (i, m_KunPrList.PosRab);
		 akikunprp->sa_rab = CStrFuncs::StrToDouble (Rab);

		 akikunprp->mdn = Mdn.mdn.mdn; 
		 akikunprp->aki_nr = Akikunprk.akikunprk.aki_nr; 
		 CAkiKunPr *pr = new CAkiKunPr (VkPr, LdPr, *akikunprp);
		 if (akikunprp->aki_pr_eu != 0.0 || 
			 akikunprp->ld_pr_eu != 0.0)
		 {
				ListRows.Add (pr);
		 }
		 delete akikunprp;
	}

	Akikunprp.sqlexecute (IprDelCursor);
	DeleteDbRows ();

	ListRows.FirstPosition ();
	CAkiKunPr *aki_pr;
	while ((aki_pr = (CAkiKunPr *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&Akikunprp.akikunprp, &aki_pr->akikunprp, sizeof (AKIKUNPRP));
		Akikunprp.akikunprp.kun     = Akikunprk.akikunprk.kun;
		Akikunprp.akikunprp.aki_von = Akikunprk.akikunprk.aki_von;
		Akikunprp.akikunprp.aki_bis = Akikunprk.akikunprk.aki_bis;
		Akikunprp.dbupdate ();
/*
		if (IsChanged (iv_pr))
		{
			PgrProt.Write ();
		}
*/

// 301111 A
		if ( 1 == 3 )	// evtl. spaeter
		{
		if ( IsChanged (aki_pr))
		{
			int ierg = 0 ;
			if ( ischongefragt == FALSE )
			{
				ierg = testgrundpreis ( ischongefragt , Akikunprp.akikunprp.a ) ;
				if ( !ierg )	// immer noch keine Entscheidung 
				{
				}
				else
				{
					if ( ierg == 1  ) 
					{
						ischongefragt = TRUE;	// JA und was machen
						igrundartpreis = TRUE;
					}
					if ( ierg == -1 )
					{	
						ischongefragt = TRUE;	// NEIN
						igrundartpreis = FALSE;
					}
				}
			}
			else	// Bereits entschieden ob ueberhaupt
			{
				if ( igrundartpreis == TRUE )
				{
					ierg = testgrundpreis (  ischongefragt , Akikunprp.akikunprp.a);
				}
			}
			if ( ierg == 1 )
			{
				ierg = ierg ;				// Unterpos. schreiben
			}
		}
		}		// evtl. sp�ter
// 301111 E

	}
	// 301111
	if ( sypgrundartpreis > 0)
		ischongefragt = FALSE ;
	else
	{
		igrundartpreis = FALSE;		//270612
		ischongefragt = TRUE ;
	}

	EnableHeadControls (TRUE);
	m_AkiNr.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	Akikunprk.dbupdate ();
	Akikunprp.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

BOOL CAkiKunPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_AkiNr.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Aktion komplett l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
//	Akikunprp.beginwork ();
	m_KunPrList.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	m_KunPrList.DeleteAllItems ();
	Akikunprk.dbdelete ();

	EnableHeadControls (TRUE);
	m_AkiNr.SetFocus ();
	Akikunprp.commitwork ();
	sql_mode = sql_s;
	if (Choice != NULL)
	{
		Choice->FillList ();
	}
	return TRUE;
}


void CAkiKunPage::OnKunchoice ()
{
	Form.Get ();
	if (ChoiceKun != NULL && !ModalChoiceKun)
	{
		ChoiceKun->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceKun == NULL)
	{
		ChoiceKun = new CChoiceKun (this);
	    ChoiceKun->IsModal = ModalChoiceKun;
	    ChoiceKun->m_Mdn = Mdn.mdn.mdn;
		ChoiceKun->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceKun->SetDbClass (&A_bas);
	ChoiceKun->SearchText = Search;
	if (ModalChoiceKun)
	{
			ChoiceKun->DoModal();
	}
	else
	{

		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceKun->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
		ChoiceKun->MoveWindow (&rect);
		ChoiceKun->SetFocus ();

		return;
	}
    if (ChoiceKun->GetState ())
    {
		  CKunList *abl = ChoiceKun->GetSelectedText (); 
		  if (abl == NULL) return;
		  Kun.kun.mdn = Mdn.mdn.mdn;
          Kun.kun.kun = abl->kun;
		  if (Kun.dbreadfirst () == 0)
		  {
			Akikunprk.akikunprk.mdn = Mdn.mdn.mdn;
			Akikunprk.akikunprk.kun = abl->kun;
            Akikunprk.dbreadfirst ();
			Form.Show ();
			m_Kun.SetFocus ();
			if (Search == "")
			{
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			}
		  }
    }
}

void CAkiKunPage::OnChoice ()
{
       ChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceAkiKunPr (this);
	    Choice->IsModal = ModalChoice;
	    Choice->m_Mdn = Mdn.mdn.mdn;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->SearchText = Search;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{

		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
		Choice->MoveWindow (&rect);
//		Choice->SetFocus ();
		Choice->SetListFocus ();  //231111 


		return;
	}
    if (Choice->GetState ())
    {
		  CAkiKunPrList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  Akikunprk.akikunprk.mdn = Mdn.mdn.mdn;
#ifdef SHORTAKI
		  Akikunprk.akikunprk.aki_nr = (short) abl->aki_nr;
#else
		  Akikunprk.akikunprk.aki_nr = abl->aki_nr;
#endif
          Akikunprk.dbreadfirst ();
 		  Form.Show ();
		  EnableHeadControls (TRUE);
 		  m_AkiNr.SetFocus ();
		  if (Search == "")
		  {
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}

void CAkiKunPage::OnSelected ()
{
	if (Choice == NULL) return;
    CAkiKunPrList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    Akikunprk.akikunprk.mdn = Mdn.mdn.mdn;
#ifdef SHORTAKI
	Akikunprk.akikunprk.aki_nr = (short) abl->aki_nr;
#else
	 Akikunprk.akikunprk.aki_nr = abl->aki_nr;
#endif
    if (Akikunprk.dbreadfirst () == 0)
	{
		Kun.kun.mdn = Mdn.mdn.mdn;
		Kun.kun.kun = abl->kun;
        Kun.dbreadfirst ();
		m_AkiNr.EnableWindow (TRUE);
		m_AkiNr.SetFocus ();
		if (!Choice->FocusBack)
		{
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		}
    }
	if (CloseChoice)
	{
		OnCanceled (); 
	}
    Form.Show ();
	if (Choice->FocusBack)
	{
		DATE_STRUCT sDateVon;
		DATE_STRUCT sDateBis;
		memcpy (&sDateVon, &Akikunprk.akikunprk.aki_von,
				sizeof (DATE_STRUCT));
		memcpy (&sDateBis, &Akikunprk.akikunprk.aki_bis,
				sizeof (DATE_STRUCT));
		Read ();
		ReadList ();
		memcpy (&Akikunprk.akikunprk.aki_von,&sDateVon, 
				sizeof (DATE_STRUCT));
		memcpy (&Akikunprk.akikunprk.aki_bis,&sDateBis, 
				sizeof (DATE_STRUCT));
		Form.Show ();
		m_ZusBez.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CAkiKunPage::OnCanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CAkiKunPage::StepBack ()
{
	if (m_AkiNr.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		m_KunPrList.StopEnter ();
		EnableHeadControls (TRUE);
		m_AkiNr.SetFocus ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
		m_KunPrList.DeleteAllItems ();
		Akikunprp.rollbackwork ();
	}
	return TRUE;
}

void CAkiKunPage::OnCancel ()
{
	StepBack ();
}

void CAkiKunPage::OnSave ()
{
	Write ();
}

void CAkiKunPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
		  if (Choice != NULL && abl->mdn != Mdn.mdn.mdn)
	  	  {
			delete Choice;
			Choice = NULL;
		  }
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CAkiKunPage::FillPrGrStufCombo ()
{
}

void CAkiKunPage::FillKunPrCombo ()
{
}

void CAkiKunPage::OnDelete ()
{
	if (m_AkiNr.IsWindowEnabled ())
	{
		Form.Get ();


		CString Message;
		Message.Format (_T("Aktion %ld l�schen"), Akikunprk.akikunprk.aki_nr);

		int ret = MessageBox (Message, NULL, 
			MB_YESNO | MB_DEFBUTTON2 | MB_ICONQUESTION);
		if (ret == IDYES)
		{
			Akikunprk.dbdelete ();
			m_AkiNr.SetFocus ();
 			memcpy (&Akikunprk.akikunprk, &akikunprk_null, sizeof (AKIKUNPRK));
			Form.Show ();
			DestroyRows (DbRows);
			DestroyRows (ListRows);
			m_KunPrList.DeleteAllItems ();
			Akikunprp.sqlexecute (IprDelCursor);
		}
		m_Kun.SetFocus ();
		m_Kun.SetSel (0, -1);
		if (Choice != NULL)
		{
			Choice->FillList ();
		}
		return;
	}
	m_KunPrList.DeleteRow ();
}

void CAkiKunPage::OnInsert ()
{
	m_KunPrList.InsertRow ();
}

BOOL CAkiKunPage::Print ()
{
	CProcess print;
	Form.Get ();
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11132.llf", tmp);
	}
	else
	{
		dName = "11132.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11132\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "aki_nr %ld %ld\n", Akikunprk.akikunprk.aki_nr,
			                             Akikunprk.akikunprk.aki_nr);
		fclose (fp);
		Command.Format ("dr70001 -name 11132 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11132";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

BOOL CAkiKunPage::PrintAll ()
{
	CProcess print;
	Form.Get ();
// Iprgrstufk.iprgrstufk.pr_gr_stuf
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11132.llf", tmp);
	}
	else
	{
		dName = "11132.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11132\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "MITRANGE 1\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "aki_nr %ld %ld\n", (long) 0,
			                                 (long) 99999999);
		fclose (fp);
		Command.Format ("dr70001 -name 11132 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11132";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CAkiKunPage::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_KunPrList.StartPauseEnter ();
}

void CAkiKunPage::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_KunPrList.EndPauseEnter ();
}

void CAkiKunPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Enable (enable);
	PosControls.Enable (!enable);
}

void CAkiKunPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CAkiKunPr *aki_pr;
	while ((aki_pr = (CAkiKunPr *) Rows.GetNext ()) != NULL)
	{
		delete aki_pr;
	}
	Rows.Init ();
}

void CAkiKunPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
// 301111 
	_tcscpy (sys_par.sys_par_nam, "grundartpreis");
  	int dsqlstatus = Sys_par.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		sypgrundartpreis = _tstoi (sys_par.sys_par_wrt);
	}
	else
	{
		sypgrundartpreis = 0;
	}


    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_KunPrList.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("RemoveKun", cfg_v) == TRUE)
    {
			RemoveKun = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_KunPrList.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_KunPrList.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_KunPrList.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CAkiKunPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_KunPrList.ListEdit ||
        Control == &m_KunPrList.ListComboBox ||
		Control == &m_KunPrList.SearchListCtrl.Edit)	
	{
		ListCopy ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CAkiKunPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CAkiKunPage::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_KunPrList.ListEdit.m_hWnd) ||
		IsWindow (m_KunPrList.ListComboBox) ||
		IsWindow (m_KunPrList.SearchListCtrl.Edit))
	{
		m_KunPrList.StopEnter ();
		m_KunPrList.StartEnter (m_KunPrList.EditCol, m_KunPrList.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
		for (int i = 0; i < m_KunPrList.GetItemCount (); i ++)
		{
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = "";
			int cols = m_KunPrList.GetHeaderCtrl ()->GetItemCount ();
			for (int j = 0; j < cols; j ++)
			{
				if (Row != "")
				{
					Row += Separator;
				}
				CString Field = m_KunPrList.GetItemText (i, j);
				Field.TrimRight ();
				Row += Field; 
			}
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

BOOL CAkiKunPage::TerminOK ()
{
	DbTime SysDate;

	Form.Get ();

	DbTime AkiVon (&Akikunprk.akikunprk.aki_von);
	DbTime AkiBis (&Akikunprk.akikunprk.aki_bis);

/*
	if (AkiVon < SysDate)
	{
		MessageBox (_T("Das Startdatum ist kleiner als das Tagesdatum"),
			        NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}
*/
	if (AkiBis < AkiVon)
	{
		MessageBox (_T("Das Enddatum ist kleiner als das Startdatum"),
			        NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}
	if (AkiBis < SysDate)
	{
		MessageBox (_T("Das Enddatum ist kleiner als das Tagesdatum"),
			        NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}
	return TRUE;
}