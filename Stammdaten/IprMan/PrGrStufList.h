#ifndef _PR_GR_STUF_LIST_DEF
#define _PR_GR_STUF_LIST_DEF
#pragma once
#include <sqlext.h>

class CPrGrStufList
{
public:
	short mdn;
	short anzahl;
	long pr_gr_stuf;
	CString zus_bz;
	DATE_STRUCT gue_ab;
	CPrGrStufList(void);
	CPrGrStufList(short, long, LPTSTR);
	CPrGrStufList(short, long, LPTSTR, DATE_STRUCT,short);
	~CPrGrStufList(void);
};
#endif
