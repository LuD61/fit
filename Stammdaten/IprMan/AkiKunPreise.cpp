#include "StdAfx.h"
#include "AkiKunPreise.h"

CAkiKunPreise::CAkiKunPreise(void)
{
	cEk = _T("0.0");
	cVk = _T("0.0");
	memcpy (&akikunlsp, &akikunlsp_null, sizeof (AKIKUNLSP));
}

CAkiKunPreise::CAkiKunPreise(CString& cEk, CString& cVk, AKIKUNLSP& akikunlsp)
{
	this->cEk = cEk.Trim ();
	this->cVk = cVk.Trim ();
	memcpy (&this->akikunlsp, &akikunlsp, sizeof (AKIKUNLSP));
}

CAkiKunPreise::~CAkiKunPreise(void)
{
}
