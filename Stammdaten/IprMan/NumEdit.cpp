#include "StdAfx.h"
#include ".\numedit.h"

CNumEdit::CNumEdit(void)
{
}

CNumEdit::~CNumEdit(void)
{
}

BEGIN_MESSAGE_MAP(CNumEdit, CEdit)
	ON_WM_SETFOCUS ()
END_MESSAGE_MAP()

void CNumEdit::OnSetFocus (CWnd *oldFocus)
{
	CEdit::OnSetFocus (oldFocus);
	PostMessage (EM_SETSEL, 0, -1);
}

