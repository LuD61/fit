#ifndef _PRG_PROT_DEF
#define _PRG_PROT_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct PRG_PROT {
   long           propramm;
   DATE_STRUCT    bearb;
   char           zeit[9];
   char           pers_nam[9];
   short          mdn;
   long           pr_gr_stuf;
   long           kun;
   long           kun_pr;
   double         a;
   double         vk_pr_i;
   double         vk_pr_eu;
   double         ld_pr;
   double         ld_pr_eu;
   short          aktion_nr;
   DATE_STRUCT    aki_von;
   DATE_STRUCT    aki_bis;
   short          loesch;
   char           kond_art[5];
   char           tabelle[21];
};
extern struct PRG_PROT prg_prot, prg_prot_null;

#line 8 "Prg_prot.rh"

class PRG_PROT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRG_PROT prg_prot;
               PRG_PROT_CLASS () : DB_CLASS ()
               {
               }
	       virtual int dbupdate (); 
               BOOL operator== (PRG_PROT&);  
};
#endif
