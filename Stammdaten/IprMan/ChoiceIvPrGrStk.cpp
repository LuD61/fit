#include "stdafx.h"
#include "ChoiceIvPrGrStk.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceIvPrGrStk::Sort1 = -1;
int CChoiceIvPrGrStk::Sort2 = -1;
int CChoiceIvPrGrStk::Sort3 = -1;
int CChoiceIvPrGrStk::Sort4 = -1;
int CChoiceIvPrGrStk::Sort5 = -1;

CChoiceIvPrGrStk::CChoiceIvPrGrStk(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_Mdn = 0;
	Rows = NULL;
}

CChoiceIvPrGrStk::~CChoiceIvPrGrStk() 
{
	DestroyList ();
}

void CChoiceIvPrGrStk::DestroyList() 
{
	for (std::vector<CPrGrStufList *>::iterator pabl = PrGrStufList.begin (); pabl != PrGrStufList.end (); ++pabl)
	{
		CPrGrStufList *abl = *pabl;
		delete abl;
	}
    PrGrStufList.clear ();
}

void CChoiceIvPrGrStk::FillList () 
{
    short  mdn;
	long pr_gr_stuf;
    TCHAR zus_bz [34];
	DATE_STRUCT gue_ab;
	short anzahl;

	int cursor;

	DestroyList ();
	CListCtrl *listView = GetListView ();
	listView->DeleteAllItems ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Preisgruppenstufe"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Listennummer"), 1, 80, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),  2, 150);
    SetCol (_T("G�ltig ab"),  3, 80);
    SetCol (_T("Anzahl"),  4, 80);

	if (PrGrStufList.size () == 0 && Rows == NULL)
	{
		if (m_Mdn != 0)
		{
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((long *)&pr_gr_stuf,   SQLLONG, 0);
			DbClass->sqlout ((LPTSTR)  zus_bz, SQLCHAR, sizeof (zus_bz));
			DbClass->sqlout((DATE_STRUCT *)&gue_ab, SQLDATE, 0);
			DbClass->sqlout((short *)&anzahl, SQLSHORT, 0);

			DbClass->sqlin ((LPTSTR)  &m_Mdn,   SQLSHORT, 0);
			cursor = DbClass->sqlcursor(_T("select iprgrstufk.mdn, iprgrstufk.pr_gr_stuf, iprgrstufk.zus_bz, iv_pr.gue_ab, count(iv_pr.a) ")
				_T("from iprgrstufk, outer iv_pr ")
				_T("where iprgrstufk.mdn = ? and iprgrstufk.pr_gr_stuf >= 0 ")
				_T("and iv_pr.mdn = iprgrstufk.mdn and ")
				_T("iv_pr.kun_pr = 0 and iv_pr.kun = 0 ")
				_T("and iv_pr.pr_gr_stuf = iprgrstufk.pr_gr_stuf ")
				_T("group by 1, 2, 3, 4 ")
				_T("order by 1, 2, 4 "));

		}
		else
		{
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((long *)&pr_gr_stuf,      SQLLONG, 0);
			DbClass->sqlout ((LPTSTR)  zus_bz, SQLCHAR, sizeof (zus_bz));
			DbClass->sqlout((DATE_STRUCT *)&gue_ab, SQLDATE, 0);
			DbClass->sqlout((short *)&anzahl, SQLSHORT, 0);
			cursor = DbClass->sqlcursor(_T("select iprgrstufk.mdn, iprgrstufk.pr_gr_stuf, iprgrstufk.zus_bz, iv_pr.gue_ab, count(iv_pr.a) ")
				_T("from iprgrstufk, outer iv_pr ")
				_T("where iprgrstufk.pr_gr_stuf >= 0 ")
				_T("and iv_pr.mdn = iprgrstufk.mdn and ")
				_T("iv_pr.kun_pr = 0 and iv_pr.kun = 0 ")
				_T("and iv_pr.pr_gr_stuf = iprgrstufk.pr_gr_stuf ")
				_T("group by 1, 2, 3, 4 ")
				_T("order by 1, 2, 4 "));

		}
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) zus_bz;
			CDbUniCode::DbToUniCode (zus_bz, pos);
			CPrGrStufList *abl = new CPrGrStufList (mdn, pr_gr_stuf, zus_bz,gue_ab,anzahl);
			PrGrStufList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}
	else if (PrGrStufList.size () == 0 && Rows != NULL)
	{
		mdn = (short) m_Mdn;
		Rows->FirstPosition ();
		CString *c;
		while ((c = (CString *) Rows->GetNext ()) != NULL)
		{
			int pos = 0;
			CString PrGrStuf = c->Tokenize (" ", pos);
			if (PrGrStuf != "")
			{
				pr_gr_stuf = atol (PrGrStuf.GetBuffer ());
                int len = PrGrStuf.GetLength ();
				CString ZusBz = PrGrStuf.Mid (len + 1, -1);
				ZusBz.Trim ();
				strcpy (zus_bz, ZusBz.GetBuffer ());
			    CPrGrStufList *abl = new CPrGrStufList (mdn, pr_gr_stuf, zus_bz);
			    PrGrStufList.push_back (abl);
			}
		}

	}

	for (std::vector<CPrGrStufList *>::iterator pabl = PrGrStufList.begin (); pabl != PrGrStufList.end (); ++pabl)
	{
		CPrGrStufList *abl = *pabl;
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString PrGrStuf;
		PrGrStuf.Format (_T("%ld"), abl->pr_gr_stuf); 
		CString Num;
		CString CAnzahl;
		CString Bez;
		_tcscpy (zus_bz, abl->zus_bz.GetBuffer ());

		CAnzahl.Format (_T("%hd"), abl->anzahl); 

		CString LText;
		LText.Format (_T("%ld %s"), abl->pr_gr_stuf, 
									abl->zus_bz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = PrGrStuf;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (zus_bz, i, 2);
		CString Cgue_ab;
		Cgue_ab.Format(_T("%02hd.%02hd.%02hd"), abl->gue_ab.day, abl->gue_ab.month, abl->gue_ab.year);
		ret = SetItemText(Cgue_ab.GetBuffer(), i, 3);
		ret = SetItemText (CAnzahl.GetBuffer(), i, 4);

        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
	Sort5 = -1;
	Sort(listView);
}


void CChoiceIvPrGrStk::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceIvPrGrStk::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceIvPrGrStk::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceIvPrGrStk::SearchZusBz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();
		CString cSearch = Search;
		cSearch.MakeUpper ();

        if (_tmemcmp (cSearch.GetBuffer (), iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceIvPrGrStk::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             SearchZusBz (ListBox, EditText.GetBuffer (8));
             break;
		case 3:
			SearchZusBz(ListBox, EditText.GetBuffer(8));
			break;
		case 4:
			SearchZusBz(ListBox, EditText.GetBuffer(8));
			break;
	}
}

int CChoiceIvPrGrStk::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceIvPrGrStk::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
    CString cItem1 = strItem1;
    CString cItem2 = strItem2;
	cItem1.MakeUpper ();
	cItem2.MakeUpper ();
//	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
	return (cItem2.CompareNoCase (cItem1)) * Sort3;
   }
   else if (SortRow == 3)
   {
	   CString cItem1 = strItem1;
	   CString cItem2 = strItem2;
	   cItem1.MakeUpper();
	   cItem2.MakeUpper();
	   return (cItem2.CompareNoCase(cItem1)) * Sort4;
   }
   else if (SortRow == 4)
   {
	   CString cItem1 = strItem1;
	   CString cItem2 = strItem2;
	   cItem1.MakeUpper();
	   cItem2.MakeUpper();
	   return (cItem2.CompareNoCase(cItem1)) * Sort5;
   }
   return 0;
}


void CChoiceIvPrGrStk::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
		case 3:
			Sort4 *= -1;
			break;
		case 4:
			Sort5 *= -1;
			break;
	}
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CPrGrStufList *abl = PrGrStufList [i];
		   
		   abl->pr_gr_stuf     = _tstol (ListBox->GetItemText (i, 1));
		   abl->zus_bz     = ListBox->GetItemText (i, 2);
	}
}

void CChoiceIvPrGrStk::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = PrGrStufList [idx];
}

CPrGrStufList *CChoiceIvPrGrStk::GetSelectedText ()
{
	CPrGrStufList *abl = (CPrGrStufList *) SelectedRow;
	return abl;
}

