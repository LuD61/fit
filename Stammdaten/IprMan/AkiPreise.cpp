#include "StdAfx.h"
#include "AkiPreise.h"

CAkiPreise::CAkiPreise(void)
{
	cEk = _T("0.0");
	cVk = _T("0.0");
	memcpy (&akiprgrstp, &akiprgrstp_null, sizeof (AKIPRGRSTP));
}

CAkiPreise::CAkiPreise(CString& cEk, CString& cVk, AKIPRGRSTP& akiprgrstp)
{
	this->cEk = cEk.Trim ();
	this->cVk = cVk.Trim ();
	memcpy (&this->akiprgrstp, &akiprgrstp, sizeof (AKIPRGRSTP));
}

CAkiPreise::~CAkiPreise(void)
{
}
