// DialogTestDlg.h : Headerdatei
//

#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "KunPrListCtrl.h"
#include "PrArtPageConst.h"
#include "FillList.h"
#include "a_bas.h"
#include "mdn.h"
#include "adr.h"
#include "akikunprp.h"
#include "Ipr.h"
#include "PGrProt.h"
#include "Akikunprk.h"
#include "Kun.h"
#include "ChoiceAkiKunPr.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "FormTab.h"
#include "mo_progcfg.h"
#include "Controls.h"
#include "AkiKunPr.h"


#ifndef IDC_KUNCHOICE
#define IDC_KUNCHOICE 4000
#endif
#ifndef IDC_MDNCHOICE
#define IDC_MDNCHOICE 3001
#endif
#ifndef IDC_AKINRCHOICE
#define IDC_AKINRCHOICE 4001
#endif

// CPrArtPreise Dialogfeld
class CAkiKunPage : public CDbPropertyPage
{
// Konstruktion
public:
	CAkiKunPage(CWnd* pParent = NULL);	// Standardkonstruktor
	CAkiKunPage(UINT nIDTemplate);	// Standardkonstruktor

	~CAkiKunPage ();

// Dialogfelddaten
//	enum { IDD = IDD_DIALOGTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung
    virtual BOOL PreTranslateMessage(MSG* pMsg);


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public :
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	BOOL RemoveKun;
	int StartSize;

	CControls HeadControls;
	CControls PosControls;
	CControls ButtonControls;
	BOOL HideButtons;
	int RightListSpace;
	CVector DbRows;
	CVector ListRows;
	int IprCursor;
	int IprGrStufkCursor;
	int IKunPrkCursor;
	int IprDelCursor;
	CWnd *Frame;
	CCtrlGrid CtrlGrid;
	CCtrlGrid AkiNrGrid;
	CCtrlGrid KunGrid;
	CCtrlGrid MdnGrid;
	CButton m_MdnChoice;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CNumEdit m_MdnName;
	CStatic m_LAkiNr;
	CNumEdit m_AkiNr;
	CButton m_AkiNrChoice;
	CButton m_KunChoice;
	CStatic m_LZusBez;
	CEdit m_ZusBez;
	CStatic m_LKun;
	CNumEdit m_Kun;
	CStatic m_LKunKrz1;
	CTextEdit m_KunKrz1;
	CStatic m_LAkiVon;
	CDateTimeCtrl m_AkiVon;
	CStatic m_LAkiBis;
	CDateTimeCtrl m_AkiBis;
	CFillList FillList;
	CKunPrListCtrl m_KunPrList;
	CButton m_Cancel;
	CButton m_Save;
	CButton m_Delete;
	CButton m_Insert;
	int RighListSpace;
	CCtrlGrid ButtonGrid;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	A_BAS_CLASS A_bas;
	AKIKUNPRP_CLASS Akikunprp;
	IPR_CLASS Ipr;
	CPGrProt PgrProt;
	AKIKUNPRK_CLASS Akikunprk;
	PRG_PROT_CLASS Prg_prot;
	KUN_CLASS Kun;
	ADR_CLASS KunAdr;
	CFormTab Form;
	CChoiceAkiKunPr *Choice;
	BOOL ModalChoice;
	BOOL CloseChoice;
	BOOL ChoiceStat;
	CString Search;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceKun *ChoiceKun;
	BOOL ModalChoiceKun;
	CFont Font;
	CFont lFont;
	CString PersName;
	CString Separator;
    CImageList image; 
	int CellHeight;

	virtual void OnSize (UINT, int, int);
	void OnKunchoice ();
	void OnChoice ();
    void OnSelected ();
    void OnCanceled ();
    void OnMdnchoice(); 
	virtual BOOL Read ();
	virtual BOOL Write ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	virtual BOOL StepBack ();
	virtual void OnCancel ();
	virtual void OnSave ();
	BOOL ReadMdn ();
	BOOL ReadKun ();
	BOOL LockList ();
	BOOL ReadList ();
    void FillPrGrStufCombo ();
    void FillKunPrCombo ();
	virtual BOOL DeleteAll ();
	virtual BOOL Print ();
	virtual BOOL PrintAll ();
	virtual void OnDelete ();
	virtual void OnInsert ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult);
    void EnableHeadControls (BOOL enable);
    void WriteRow (int row);
    void DestroyRows(CVector &Rows);
    void DeleteDbRows ();
    BOOL IsChanged (CAkiKunPr* Akipr);
    BOOL InList (AKIKUNPRP_CLASS& Akikunprp);
	void ReadCfg ();
    void ListCopy ();
	BOOL TerminOK ();
	int  testgrundpreis ( bool schongefragt ,  double ia ) ;	// 301111 

};
