#ifndef _SYS_PAR_DEF
#define _SYS_PAR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct SYS_PAR {
   char           sys_par_nam[19];
   char           sys_par_wrt[2];
   char           sys_par_besch[33];
   long           zei;
   short          delstatus;
};
extern struct SYS_PAR sys_par, sys_par_null;


#line 8 "sys_par.rh"

class SYS_PAR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SYS_PAR_CLASS () : DB_CLASS ()
               {
               }
};
#endif
