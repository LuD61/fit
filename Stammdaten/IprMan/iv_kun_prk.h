#ifndef _IV_KUN_PRK_DEF
#define _IV_KUN_PRK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct IV_KUN_PRK {
   short          mdn;
   long           kun_pr;
   TCHAR          zus_bz[25];
   long           pr_gr_stuf;
   DATE_STRUCT    gue_ab;
   TCHAR          ueb_kz[2];
   short          delstatus;
   short          waehrung;
};
extern struct IV_KUN_PRK iv_kun_prk, iv_kun_prk_null;

#line 8 "iv_kun_prk.rh"

class IV_KUN_PRK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               IV_KUN_PRK iv_kun_prk;
               IV_KUN_PRK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
