#include "StdAfx.h"
#include "uniformfield.h"
#include "StrFuncs.h"
#include "DbUniCode.h"

CCodeProperties CUniFormField::NullCode;
CCodeProperties *CUniFormField::Code;

CUniFormField::CUniFormField(void)
{
	if (Code == NULL)
	{
		Code = &NullCode;
	}
}

CUniFormField::CUniFormField(CWnd *Control, int CtrlType, void *Vadr, int VType) :
	CFormField (Control, CtrlType, Vadr, VType)
{
	if (Code == NULL)
	{
		Code = &NullCode;
	}
}

CUniFormField::CUniFormField(CWnd *Control, int CtrlType, void *Vadr, int VType, int len, int Scale) :
	CFormField (Control, CtrlType, Vadr, VType, len, Scale)
{
	if (Code == NULL)
	{
		Code = &NullCode;
	}
}



CUniFormField::~CUniFormField(void)
{
}

void CUniFormField::Show ()
{
	CString Text;
	LPSTR pos;
	LPTSTR text;

	switch (VType)
	{
	case VCHAR :
		Text = (LPTSTR) Vadr;

		text = new TCHAR [(Text.GetLength () + 1) * 2];
		_tcscpy (text, Text.GetBuffer ());
		pos = (LPSTR) text;
	    CDbUniCode::DbToUniCode (*Code, text, pos, (Text.GetLength ()+ 1) * 2);
		Text = text;
		delete text;
		break;
	case VSHORT :
		Text.Format (_T("%hd"), *((short *) Vadr));
		break;
	case VLONG :
		Text.Format (_T("%ld"), *((long *) Vadr));
		break;
	case VSTRING :
		Text = *(CString *) Vadr;
		break;
	case VDOUBLE :
		CString frm;
		frm.Format (_T("%c.%dlf"), '%', Scale);
		Text.Format (frm.GetBuffer (0), *((double *) Vadr));
		break;
	}
	Text.TrimRight ();
	if (CtrlType == EDIT)
	{
		    ((CEdit *) Control)->SetWindowText (Text);
	}
	else if (CtrlType == COMBOBOX)
	{
			SetTextSelected (Text);
	}
	else if (CtrlType == CHECKBOX)
	{
		if ((Text.CollateNoCase (_T("J")) == 0) ||
			Text.Trim ().Compare (_T("1")) == 0)
		{
			((CButton *) Control)->SetCheck (BST_CHECKED);
		}
		else
		{
			((CButton *) Control)->SetCheck (BST_UNCHECKED);
		}
	}
}


void CUniFormField::Get ()
{
	CString Text;
	LPSTR p;
	LPTSTR text;

	if (CtrlType == EDIT)
	{
		    ((CEdit *) Control)->GetWindowText (Text);
	}
	else if (CtrlType == COMBOBOX)
	{
			GetSelectedText (Text);
			int iStart = 0;
			Text = Text.Tokenize (_T(" "), iStart);
	}
	else if (CtrlType == CHECKBOX)
	{
		int checked = ((CButton *) Control)->GetCheck ();
		if (checked == BST_CHECKED)
		{
			if (VType == VCHAR)
			{
				Text = "J";
			}
			else
			{
				Text = "1";
			}
		}
		else if (checked == BST_UNCHECKED)
		{
			if (VType == VCHAR)
			{
				Text = "N";
			}
			else
			{
				Text = "0";
			}
		}
	}
	switch (VType)
	{
		case VCHAR :
			text = new TCHAR [(Text.GetLength () + 1) * 2];
			_tcscpy (text, _T(""));
			p = (LPSTR) text;
			CDbUniCode::DbFromUniCode (*Code, Text.GetBuffer (), p, ((Text.GetLength () + 1) * 2));
			Text = text;
			_tcscpy ((LPTSTR) Vadr, Text.GetBuffer ());
			delete text;
			break;
		case VSHORT :
			*((short*) Vadr) = _tstoi (Text.GetBuffer(0));
			break;
		case VLONG :
			*((long*) Vadr) =  _tstol (Text.GetBuffer(0));
			break;
		case VDOUBLE :
			*((double *) Vadr) = CStrFuncs::StrToDouble (Text);
			break;
		case VSTRING :
			*(CString *)Vadr = Text;
			break;
	}
}
