// ChildFrm.h : Schnittstelle der Klasse CChildFrame
//


#pragma once
#include "vector.h"


class CChildFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CChildFrame)
public:
	CChildFrame();

// Attribute
public:

// Operationen
public:

// Überschreibungen
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual BOOL OnCreateClient(LPCREATESTRUCT, CCreateContext*);

// Implementierung
public:
	virtual ~CChildFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
    afx_msg void OnSize(UINT, int, int);

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
public:
	static CVector StdPrWnd;
	static CVector NewPrWnd;
	static CVector AkiPrWnd;
	BOOL InitSize;

	afx_msg void OnDestroy();

};

