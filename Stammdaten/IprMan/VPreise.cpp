#include "StdAfx.h"
#include "vpreise.h"

CVPreise::CVPreise(void)
{
	cEk = _T("0.0");
	cVk = _T("0.0");
	memcpy (&iv_pr, &iv_pr_null, sizeof (IV_PR));
}

CVPreise::CVPreise(CString& cEk, CString& cVk, IV_PR& iv_pr)
{
	this->cEk = cEk.Trim ();
	this->cVk = cVk.Trim ();
	memcpy (&this->iv_pr, &iv_pr, sizeof (IV_PR));
}

CVPreise::~CVPreise(void)
{
}
