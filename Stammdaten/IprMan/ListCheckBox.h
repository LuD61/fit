#pragma once
#include "afxwin.h"

class CListCheckBox :
	public CButton
{
public:
	CListCheckBox(void);
	~CListCheckBox(void);
	virtual BOOL Create (DWORD dwtyle, const RECT& rect, CWnd *pParentWnd, UINT nId);
protected :
	DECLARE_MESSAGE_MAP()
    afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};
