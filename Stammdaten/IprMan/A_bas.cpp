#include "stdafx.h"
#include "a_bas.h"

struct A_BAS a_bas, a_bas_null;

void A_BAS_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_bas.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_bas.a,SQLDOUBLE,0);
    sqlout ((short *) &a_bas.mdn,SQLSHORT,0);
    sqlout ((short *) &a_bas.fil,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.a_bz1,SQLCHAR,25);
    sqlout ((TCHAR *) a_bas.a_bz2,SQLCHAR,25);
    sqlout ((double *) &a_bas.a_gew,SQLDOUBLE,0);
    sqlout ((short *) &a_bas.a_typ,SQLSHORT,0);
    sqlout ((short *) &a_bas.a_typ2,SQLSHORT,0);
    sqlout ((short *) &a_bas.abt,SQLSHORT,0);
    sqlout ((long *) &a_bas.ag,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.best_auto,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.bsd_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.cp_aufschl,SQLCHAR,2);
    sqlout ((short *) &a_bas.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_bas.dr_folge,SQLSHORT,0);
    sqlout ((long *) &a_bas.erl_kto,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.hbk_kz,SQLCHAR,2);
    sqlout ((short *) &a_bas.hbk_ztr,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.hnd_gew,SQLCHAR,2);
    sqlout ((short *) &a_bas.hwg,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.kost_kz,SQLCHAR,3);
    sqlout ((short *) &a_bas.me_einh,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.modif,SQLCHAR,2);
    sqlout ((short *) &a_bas.mwst,SQLSHORT,0);
    sqlout ((short *) &a_bas.plak_div,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.stk_lst_kz,SQLCHAR,2);
    sqlout ((double *) &a_bas.sw,SQLDOUBLE,0);
    sqlout ((short *) &a_bas.teil_smt,SQLSHORT,0);
    sqlout ((long *) &a_bas.we_kto,SQLLONG,0);
    sqlout ((short *) &a_bas.wg,SQLSHORT,0);
    sqlout ((short *) &a_bas.zu_stoff,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &a_bas.akv,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &a_bas.bearb,SQLDATE,0);
    sqlout ((TCHAR *) a_bas.pers_nam,SQLCHAR,9);
    sqlout ((double *) &a_bas.prod_zeit,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.pers_rab_kz,SQLCHAR,2);
    sqlout ((double *) &a_bas.gn_pkt_gbr,SQLDOUBLE,0);
    sqlout ((long *) &a_bas.kost_st,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.sw_pr_kz,SQLCHAR,2);
    sqlout ((long *) &a_bas.kost_tr,SQLLONG,0);
    sqlout ((double *) &a_bas.a_grund,SQLDOUBLE,0);
    sqlout ((long *) &a_bas.kost_st2,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto2,SQLLONG,0);
    sqlout ((long *) &a_bas.charg_hand,SQLLONG,0);
    sqlout ((long *) &a_bas.intra_stat,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.qual_kng,SQLCHAR,5);
    sqlout ((TCHAR *) a_bas.a_bz3,SQLCHAR,25);
    sqlout ((short *) &a_bas.lief_einh,SQLSHORT,0);
    sqlout ((double *) &a_bas.inh_lief,SQLDOUBLE,0);
    sqlout ((long *) &a_bas.erl_kto_1,SQLLONG,0);
    sqlout ((long *) &a_bas.erl_kto_2,SQLLONG,0);
    sqlout ((long *) &a_bas.erl_kto_3,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto_1,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto_2,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto_3,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.skto_f,SQLCHAR,2);
    sqlout ((double *) &a_bas.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_bas.a_ersatz,SQLDOUBLE,0);
    sqlout ((short *) &a_bas.a_ers_kz,SQLSHORT,0);
    sqlout ((short *) &a_bas.me_einh_abverk,SQLSHORT,0);
    sqlout ((double *) &a_bas.inh_abverk,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.hnd_gew_abverk,SQLCHAR,2);
    sqlout ((double *) &a_bas.inh_ek,SQLDOUBLE,0);
    sqlout ((double *) &a_bas.a_leih,SQLDOUBLE,0);
    sqlout ((long *) &a_bas.txt_nr1,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr2,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr3,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr4,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr5,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr6,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr7,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr8,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr9,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr10,SQLLONG,0);
    sqlout ((long *) &a_bas.allgtxt_nr1,SQLLONG,0);
    sqlout ((long *) &a_bas.allgtxt_nr2,SQLLONG,0);
    sqlout ((long *) &a_bas.allgtxt_nr3,SQLLONG,0);
    sqlout ((long *) &a_bas.allgtxt_nr4,SQLLONG,0);
            cursor = sqlcursor (_T("select a_bas.a,  a_bas.mdn,  ")
_T("a_bas.fil,  a_bas.a_bz1,  a_bas.a_bz2,  a_bas.a_gew,  a_bas.a_typ,  ")
_T("a_bas.a_typ2,  a_bas.abt,  a_bas.ag,  a_bas.best_auto,  a_bas.bsd_kz,  ")
_T("a_bas.cp_aufschl,  a_bas.delstatus,  a_bas.dr_folge,  a_bas.erl_kto,  ")
_T("a_bas.hbk_kz,  a_bas.hbk_ztr,  a_bas.hnd_gew,  a_bas.hwg,  a_bas.kost_kz,  ")
_T("a_bas.me_einh,  a_bas.modif,  a_bas.mwst,  a_bas.plak_div,  ")
_T("a_bas.stk_lst_kz,  a_bas.sw,  a_bas.teil_smt,  a_bas.we_kto,  a_bas.wg,  ")
_T("a_bas.zu_stoff,  a_bas.akv,  a_bas.bearb,  a_bas.pers_nam,  ")
_T("a_bas.prod_zeit,  a_bas.pers_rab_kz,  a_bas.gn_pkt_gbr,  ")
_T("a_bas.kost_st,  a_bas.sw_pr_kz,  a_bas.kost_tr,  a_bas.a_grund,  ")
_T("a_bas.kost_st2,  a_bas.we_kto2,  a_bas.charg_hand,  a_bas.intra_stat,  ")
_T("a_bas.qual_kng,  a_bas.a_bz3,  a_bas.lief_einh,  a_bas.inh_lief,  ")
_T("a_bas.erl_kto_1,  a_bas.erl_kto_2,  a_bas.erl_kto_3,  a_bas.we_kto_1,  ")
_T("a_bas.we_kto_2,  a_bas.we_kto_3,  a_bas.skto_f,  a_bas.sk_vollk,  ")
_T("a_bas.a_ersatz,  a_bas.a_ers_kz,  a_bas.me_einh_abverk,  ")
_T("a_bas.inh_abverk,  a_bas.hnd_gew_abverk,  a_bas.inh_ek,  a_bas.a_leih,  ")
_T("a_bas.txt_nr1,  a_bas.txt_nr2,  a_bas.txt_nr3,  a_bas.txt_nr4,  ")
_T("a_bas.txt_nr5,  a_bas.txt_nr6,  a_bas.txt_nr7,  a_bas.txt_nr8,  ")
_T("a_bas.txt_nr9,  a_bas.txt_nr10,  a_bas.allgtxt_nr1,  ")
_T("a_bas.allgtxt_nr2,  a_bas.allgtxt_nr3,  a_bas.allgtxt_nr4 from a_bas ")

#line 12 "a_bas.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_bas.a,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.mdn,SQLSHORT,0);
    sqlin ((short *) &a_bas.fil,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.a_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) a_bas.a_bz2,SQLCHAR,25);
    sqlin ((double *) &a_bas.a_gew,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.a_typ,SQLSHORT,0);
    sqlin ((short *) &a_bas.a_typ2,SQLSHORT,0);
    sqlin ((short *) &a_bas.abt,SQLSHORT,0);
    sqlin ((long *) &a_bas.ag,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.best_auto,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.bsd_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.cp_aufschl,SQLCHAR,2);
    sqlin ((short *) &a_bas.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_bas.dr_folge,SQLSHORT,0);
    sqlin ((long *) &a_bas.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.hbk_kz,SQLCHAR,2);
    sqlin ((short *) &a_bas.hbk_ztr,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.hnd_gew,SQLCHAR,2);
    sqlin ((short *) &a_bas.hwg,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.kost_kz,SQLCHAR,3);
    sqlin ((short *) &a_bas.me_einh,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.modif,SQLCHAR,2);
    sqlin ((short *) &a_bas.mwst,SQLSHORT,0);
    sqlin ((short *) &a_bas.plak_div,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.stk_lst_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas.sw,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.teil_smt,SQLSHORT,0);
    sqlin ((long *) &a_bas.we_kto,SQLLONG,0);
    sqlin ((short *) &a_bas.wg,SQLSHORT,0);
    sqlin ((short *) &a_bas.zu_stoff,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &a_bas.akv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_bas.bearb,SQLDATE,0);
    sqlin ((TCHAR *) a_bas.pers_nam,SQLCHAR,9);
    sqlin ((double *) &a_bas.prod_zeit,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.pers_rab_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas.gn_pkt_gbr,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.kost_st,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.sw_pr_kz,SQLCHAR,2);
    sqlin ((long *) &a_bas.kost_tr,SQLLONG,0);
    sqlin ((double *) &a_bas.a_grund,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.kost_st2,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto2,SQLLONG,0);
    sqlin ((long *) &a_bas.charg_hand,SQLLONG,0);
    sqlin ((long *) &a_bas.intra_stat,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.qual_kng,SQLCHAR,5);
    sqlin ((TCHAR *) a_bas.a_bz3,SQLCHAR,25);
    sqlin ((short *) &a_bas.lief_einh,SQLSHORT,0);
    sqlin ((double *) &a_bas.inh_lief,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &a_bas.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &a_bas.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_1,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_2,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_3,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.skto_f,SQLCHAR,2);
    sqlin ((double *) &a_bas.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.a_ersatz,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.a_ers_kz,SQLSHORT,0);
    sqlin ((short *) &a_bas.me_einh_abverk,SQLSHORT,0);
    sqlin ((double *) &a_bas.inh_abverk,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.hnd_gew_abverk,SQLCHAR,2);
    sqlin ((double *) &a_bas.inh_ek,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.a_leih,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.txt_nr1,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr2,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr3,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr4,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr5,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr6,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr7,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr8,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr9,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr10,SQLLONG,0);
    sqlin ((long *) &a_bas.allgtxt_nr1,SQLLONG,0);
    sqlin ((long *) &a_bas.allgtxt_nr2,SQLLONG,0);
    sqlin ((long *) &a_bas.allgtxt_nr3,SQLLONG,0);
    sqlin ((long *) &a_bas.allgtxt_nr4,SQLLONG,0);
            sqltext = _T("update a_bas set a_bas.a = ?,  ")
_T("a_bas.mdn = ?,  a_bas.fil = ?,  a_bas.a_bz1 = ?,  a_bas.a_bz2 = ?,  ")
_T("a_bas.a_gew = ?,  a_bas.a_typ = ?,  a_bas.a_typ2 = ?,  a_bas.abt = ?,  ")
_T("a_bas.ag = ?,  a_bas.best_auto = ?,  a_bas.bsd_kz = ?,  ")
_T("a_bas.cp_aufschl = ?,  a_bas.delstatus = ?,  a_bas.dr_folge = ?,  ")
_T("a_bas.erl_kto = ?,  a_bas.hbk_kz = ?,  a_bas.hbk_ztr = ?,  ")
_T("a_bas.hnd_gew = ?,  a_bas.hwg = ?,  a_bas.kost_kz = ?,  ")
_T("a_bas.me_einh = ?,  a_bas.modif = ?,  a_bas.mwst = ?,  ")
_T("a_bas.plak_div = ?,  a_bas.stk_lst_kz = ?,  a_bas.sw = ?,  ")
_T("a_bas.teil_smt = ?,  a_bas.we_kto = ?,  a_bas.wg = ?,  ")
_T("a_bas.zu_stoff = ?,  a_bas.akv = ?,  a_bas.bearb = ?,  ")
_T("a_bas.pers_nam = ?,  a_bas.prod_zeit = ?,  a_bas.pers_rab_kz = ?,  ")
_T("a_bas.gn_pkt_gbr = ?,  a_bas.kost_st = ?,  a_bas.sw_pr_kz = ?,  ")
_T("a_bas.kost_tr = ?,  a_bas.a_grund = ?,  a_bas.kost_st2 = ?,  ")
_T("a_bas.we_kto2 = ?,  a_bas.charg_hand = ?,  a_bas.intra_stat = ?,  ")
_T("a_bas.qual_kng = ?,  a_bas.a_bz3 = ?,  a_bas.lief_einh = ?,  ")
_T("a_bas.inh_lief = ?,  a_bas.erl_kto_1 = ?,  a_bas.erl_kto_2 = ?,  ")
_T("a_bas.erl_kto_3 = ?,  a_bas.we_kto_1 = ?,  a_bas.we_kto_2 = ?,  ")
_T("a_bas.we_kto_3 = ?,  a_bas.skto_f = ?,  a_bas.sk_vollk = ?,  ")
_T("a_bas.a_ersatz = ?,  a_bas.a_ers_kz = ?,  a_bas.me_einh_abverk = ?,  ")
_T("a_bas.inh_abverk = ?,  a_bas.hnd_gew_abverk = ?,  a_bas.inh_ek = ?,  ")
_T("a_bas.a_leih = ?,  a_bas.txt_nr1 = ?,  a_bas.txt_nr2 = ?,  ")
_T("a_bas.txt_nr3 = ?,  a_bas.txt_nr4 = ?,  a_bas.txt_nr5 = ?,  ")
_T("a_bas.txt_nr6 = ?,  a_bas.txt_nr7 = ?,  a_bas.txt_nr8 = ?,  ")
_T("a_bas.txt_nr9 = ?,  a_bas.txt_nr10 = ?,  a_bas.allgtxt_nr1 = ?,  ")
_T("a_bas.allgtxt_nr2 = ?,  a_bas.allgtxt_nr3 = ?,  ")
_T("a_bas.allgtxt_nr4 = ? ")

#line 14 "a_bas.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_bas.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_bas.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_bas ")
                                  _T("where a = ?"));
            sqlin ((double *)   &a_bas.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_bas ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_bas.a,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.mdn,SQLSHORT,0);
    sqlin ((short *) &a_bas.fil,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.a_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) a_bas.a_bz2,SQLCHAR,25);
    sqlin ((double *) &a_bas.a_gew,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.a_typ,SQLSHORT,0);
    sqlin ((short *) &a_bas.a_typ2,SQLSHORT,0);
    sqlin ((short *) &a_bas.abt,SQLSHORT,0);
    sqlin ((long *) &a_bas.ag,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.best_auto,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.bsd_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.cp_aufschl,SQLCHAR,2);
    sqlin ((short *) &a_bas.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_bas.dr_folge,SQLSHORT,0);
    sqlin ((long *) &a_bas.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.hbk_kz,SQLCHAR,2);
    sqlin ((short *) &a_bas.hbk_ztr,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.hnd_gew,SQLCHAR,2);
    sqlin ((short *) &a_bas.hwg,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.kost_kz,SQLCHAR,3);
    sqlin ((short *) &a_bas.me_einh,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.modif,SQLCHAR,2);
    sqlin ((short *) &a_bas.mwst,SQLSHORT,0);
    sqlin ((short *) &a_bas.plak_div,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.stk_lst_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas.sw,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.teil_smt,SQLSHORT,0);
    sqlin ((long *) &a_bas.we_kto,SQLLONG,0);
    sqlin ((short *) &a_bas.wg,SQLSHORT,0);
    sqlin ((short *) &a_bas.zu_stoff,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &a_bas.akv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_bas.bearb,SQLDATE,0);
    sqlin ((TCHAR *) a_bas.pers_nam,SQLCHAR,9);
    sqlin ((double *) &a_bas.prod_zeit,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.pers_rab_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas.gn_pkt_gbr,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.kost_st,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.sw_pr_kz,SQLCHAR,2);
    sqlin ((long *) &a_bas.kost_tr,SQLLONG,0);
    sqlin ((double *) &a_bas.a_grund,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.kost_st2,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto2,SQLLONG,0);
    sqlin ((long *) &a_bas.charg_hand,SQLLONG,0);
    sqlin ((long *) &a_bas.intra_stat,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.qual_kng,SQLCHAR,5);
    sqlin ((TCHAR *) a_bas.a_bz3,SQLCHAR,25);
    sqlin ((short *) &a_bas.lief_einh,SQLSHORT,0);
    sqlin ((double *) &a_bas.inh_lief,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &a_bas.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &a_bas.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_1,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_2,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_3,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.skto_f,SQLCHAR,2);
    sqlin ((double *) &a_bas.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.a_ersatz,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.a_ers_kz,SQLSHORT,0);
    sqlin ((short *) &a_bas.me_einh_abverk,SQLSHORT,0);
    sqlin ((double *) &a_bas.inh_abverk,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.hnd_gew_abverk,SQLCHAR,2);
    sqlin ((double *) &a_bas.inh_ek,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.a_leih,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.txt_nr1,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr2,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr3,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr4,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr5,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr6,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr7,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr8,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr9,SQLLONG,0);
    sqlin ((long *) &a_bas.txt_nr10,SQLLONG,0);
    sqlin ((long *) &a_bas.allgtxt_nr1,SQLLONG,0);
    sqlin ((long *) &a_bas.allgtxt_nr2,SQLLONG,0);
    sqlin ((long *) &a_bas.allgtxt_nr3,SQLLONG,0);
    sqlin ((long *) &a_bas.allgtxt_nr4,SQLLONG,0);
            ins_cursor = sqlcursor (_T("insert into a_bas (a,  ")
_T("mdn,  fil,  a_bz1,  a_bz2,  a_gew,  a_typ,  a_typ2,  abt,  ag,  best_auto,  bsd_kz,  ")
_T("cp_aufschl,  delstatus,  dr_folge,  erl_kto,  hbk_kz,  hbk_ztr,  hnd_gew,  hwg,  ")
_T("kost_kz,  me_einh,  modif,  mwst,  plak_div,  stk_lst_kz,  sw,  teil_smt,  we_kto,  wg,  ")
_T("zu_stoff,  akv,  bearb,  pers_nam,  prod_zeit,  pers_rab_kz,  gn_pkt_gbr,  ")
_T("kost_st,  sw_pr_kz,  kost_tr,  a_grund,  kost_st2,  we_kto2,  charg_hand,  ")
_T("intra_stat,  qual_kng,  a_bz3,  lief_einh,  inh_lief,  erl_kto_1,  erl_kto_2,  ")
_T("erl_kto_3,  we_kto_1,  we_kto_2,  we_kto_3,  skto_f,  sk_vollk,  a_ersatz,  ")
_T("a_ers_kz,  me_einh_abverk,  inh_abverk,  hnd_gew_abverk,  inh_ek,  a_leih,  ")
_T("txt_nr1,  txt_nr2,  txt_nr3,  txt_nr4,  txt_nr5,  txt_nr6,  txt_nr7,  txt_nr8,  ")
_T("txt_nr9,  txt_nr10,  allgtxt_nr1,  allgtxt_nr2,  allgtxt_nr3,  allgtxt_nr4) ")

#line 25 "a_bas.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?)")); 

#line 27 "a_bas.rpp"
// 280612

    sqlin ((double *)   &a_bas.a_grund,  SQLDOUBLE, 0);
    sqlout ((double *) &a_bas.a,SQLDOUBLE,0);
    sqlout ((short *) &a_bas.mdn,SQLSHORT,0);
    sqlout ((short *) &a_bas.fil,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.a_bz1,SQLCHAR,25);
    sqlout ((TCHAR *) a_bas.a_bz2,SQLCHAR,25);
    sqlout ((double *) &a_bas.a_gew,SQLDOUBLE,0);
    sqlout ((short *) &a_bas.a_typ,SQLSHORT,0);
    sqlout ((short *) &a_bas.a_typ2,SQLSHORT,0);
    sqlout ((short *) &a_bas.abt,SQLSHORT,0);
    sqlout ((long *) &a_bas.ag,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.best_auto,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.bsd_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.cp_aufschl,SQLCHAR,2);
    sqlout ((short *) &a_bas.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_bas.dr_folge,SQLSHORT,0);
    sqlout ((long *) &a_bas.erl_kto,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.hbk_kz,SQLCHAR,2);
    sqlout ((short *) &a_bas.hbk_ztr,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.hnd_gew,SQLCHAR,2);
    sqlout ((short *) &a_bas.hwg,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.kost_kz,SQLCHAR,3);
    sqlout ((short *) &a_bas.me_einh,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.modif,SQLCHAR,2);
    sqlout ((short *) &a_bas.mwst,SQLSHORT,0);
    sqlout ((short *) &a_bas.plak_div,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.stk_lst_kz,SQLCHAR,2);
    sqlout ((double *) &a_bas.sw,SQLDOUBLE,0);
    sqlout ((short *) &a_bas.teil_smt,SQLSHORT,0);
    sqlout ((long *) &a_bas.we_kto,SQLLONG,0);
    sqlout ((short *) &a_bas.wg,SQLSHORT,0);
    sqlout ((short *) &a_bas.zu_stoff,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &a_bas.akv,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &a_bas.bearb,SQLDATE,0);
    sqlout ((TCHAR *) a_bas.pers_nam,SQLCHAR,9);
    sqlout ((double *) &a_bas.prod_zeit,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.pers_rab_kz,SQLCHAR,2);
    sqlout ((double *) &a_bas.gn_pkt_gbr,SQLDOUBLE,0);
    sqlout ((long *) &a_bas.kost_st,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.sw_pr_kz,SQLCHAR,2);
    sqlout ((long *) &a_bas.kost_tr,SQLLONG,0);
    sqlout ((double *) &a_bas.a_grund,SQLDOUBLE,0);
    sqlout ((long *) &a_bas.kost_st2,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto2,SQLLONG,0);
    sqlout ((long *) &a_bas.charg_hand,SQLLONG,0);
    sqlout ((long *) &a_bas.intra_stat,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.qual_kng,SQLCHAR,5);
    sqlout ((TCHAR *) a_bas.a_bz3,SQLCHAR,25);
    sqlout ((short *) &a_bas.lief_einh,SQLSHORT,0);
    sqlout ((double *) &a_bas.inh_lief,SQLDOUBLE,0);
    sqlout ((long *) &a_bas.erl_kto_1,SQLLONG,0);
    sqlout ((long *) &a_bas.erl_kto_2,SQLLONG,0);
    sqlout ((long *) &a_bas.erl_kto_3,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto_1,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto_2,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto_3,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.skto_f,SQLCHAR,2);
    sqlout ((double *) &a_bas.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_bas.a_ersatz,SQLDOUBLE,0);
    sqlout ((short *) &a_bas.a_ers_kz,SQLSHORT,0);
    sqlout ((short *) &a_bas.me_einh_abverk,SQLSHORT,0);
    sqlout ((double *) &a_bas.inh_abverk,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.hnd_gew_abverk,SQLCHAR,2);
    sqlout ((double *) &a_bas.inh_ek,SQLDOUBLE,0);
    sqlout ((double *) &a_bas.a_leih,SQLDOUBLE,0);
    sqlout ((long *) &a_bas.txt_nr1,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr2,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr3,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr4,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr5,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr6,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr7,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr8,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr9,SQLLONG,0);
    sqlout ((long *) &a_bas.txt_nr10,SQLLONG,0);
    sqlout ((long *) &a_bas.allgtxt_nr1,SQLLONG,0);
    sqlout ((long *) &a_bas.allgtxt_nr2,SQLLONG,0);
    sqlout ((long *) &a_bas.allgtxt_nr3,SQLLONG,0);
    sqlout ((long *) &a_bas.allgtxt_nr4,SQLLONG,0);
            test_lock_cursor = sqlcursor (_T("select a_bas.a,  a_bas.mdn,  ")
_T("a_bas.fil,  a_bas.a_bz1,  a_bas.a_bz2,  a_bas.a_gew,  a_bas.a_typ,  ")
_T("a_bas.a_typ2,  a_bas.abt,  a_bas.ag,  a_bas.best_auto,  a_bas.bsd_kz,  ")
_T("a_bas.cp_aufschl,  a_bas.delstatus,  a_bas.dr_folge,  a_bas.erl_kto,  ")
_T("a_bas.hbk_kz,  a_bas.hbk_ztr,  a_bas.hnd_gew,  a_bas.hwg,  a_bas.kost_kz,  ")
_T("a_bas.me_einh,  a_bas.modif,  a_bas.mwst,  a_bas.plak_div,  ")
_T("a_bas.stk_lst_kz,  a_bas.sw,  a_bas.teil_smt,  a_bas.we_kto,  a_bas.wg,  ")
_T("a_bas.zu_stoff,  a_bas.akv,  a_bas.bearb,  a_bas.pers_nam,  ")
_T("a_bas.prod_zeit,  a_bas.pers_rab_kz,  a_bas.gn_pkt_gbr,  ")
_T("a_bas.kost_st,  a_bas.sw_pr_kz,  a_bas.kost_tr,  a_bas.a_grund,  ")
_T("a_bas.kost_st2,  a_bas.we_kto2,  a_bas.charg_hand,  a_bas.intra_stat,  ")
_T("a_bas.qual_kng,  a_bas.a_bz3,  a_bas.lief_einh,  a_bas.inh_lief,  ")
_T("a_bas.erl_kto_1,  a_bas.erl_kto_2,  a_bas.erl_kto_3,  a_bas.we_kto_1,  ")
_T("a_bas.we_kto_2,  a_bas.we_kto_3,  a_bas.skto_f,  a_bas.sk_vollk,  ")
_T("a_bas.a_ersatz,  a_bas.a_ers_kz,  a_bas.me_einh_abverk,  ")
_T("a_bas.inh_abverk,  a_bas.hnd_gew_abverk,  a_bas.inh_ek,  a_bas.a_leih,  ")
_T("a_bas.txt_nr1,  a_bas.txt_nr2,  a_bas.txt_nr3,  a_bas.txt_nr4,  ")
_T("a_bas.txt_nr5,  a_bas.txt_nr6,  a_bas.txt_nr7,  a_bas.txt_nr8,  ")
_T("a_bas.txt_nr9,  a_bas.txt_nr10,  a_bas.allgtxt_nr1,  ")
_T("a_bas.allgtxt_nr2,  a_bas.allgtxt_nr3,  a_bas.allgtxt_nr4 from a_bas ")

                                  _T("where a_grund = ?"));
 
}
