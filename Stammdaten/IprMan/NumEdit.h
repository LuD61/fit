#pragma once
#include "afxwin.h"

class CNumEdit :
	public CEdit
{
public:
	CNumEdit(void);
	~CNumEdit(void);
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSetFocus (CWnd *);
};
