#include "StdAfx.h"
#include ".\dbpropertypage.h"

IMPLEMENT_DYNAMIC(CDbPropertyPage, CPropertyPage)

CDbPropertyPage::CDbPropertyPage(void)
{
}

CDbPropertyPage::CDbPropertyPage(UINT IDD)
	: CPropertyPage(IDD)
{
	InData = FALSE;
}

CDbPropertyPage::~CDbPropertyPage(void)
{
}

BOOL CDbPropertyPage::read ()
{
	return TRUE;
}

BOOL CDbPropertyPage::Read ()
{
	return TRUE;
}

BOOL CDbPropertyPage::Print ()
{
	return TRUE;
}

BOOL CDbPropertyPage::PrintAll ()
{
	return TRUE;
}

BOOL CDbPropertyPage::EnablePrint (CCmdUI *pCmdUI)
{
	return TRUE;
}

BOOL CDbPropertyPage::TextCent ()
{
	return TRUE;
}

BOOL CDbPropertyPage::EnableTextCent (CCmdUI *pCmdUI)
{
	return TRUE;
}

BOOL CDbPropertyPage::TextLeft ()
{
	return TRUE;
}

BOOL CDbPropertyPage::EnableTextLeft (CCmdUI *pCmdUI)
{
	return TRUE;
}
BOOL CDbPropertyPage::TextRight ()
{
	return TRUE;
}

BOOL CDbPropertyPage::EnableTextRight (CCmdUI *pCmdUI)
{
	return TRUE;
}

BOOL CDbPropertyPage::DeleteAll ()
{
	return TRUE;
}


BOOL CDbPropertyPage::Delete ()
{
	OnDelete ();
	return TRUE;
}

BOOL CDbPropertyPage::Insert ()
{
	OnInsert ();
	return TRUE;
}

BOOL CDbPropertyPage::Write ()
{
	return TRUE;
}

void CDbPropertyPage::write ()
{
}

void CDbPropertyPage::OnInsert ()
{
}

void CDbPropertyPage::OnDelete ()
{
}

BOOL CDbPropertyPage::AfterWrite ()
{
	return TRUE;
}

BOOL CDbPropertyPage::Show ()
{
	return TRUE;
}

BOOL CDbPropertyPage::StepBack ()
{
	return FALSE;
}

void CDbPropertyPage::NextRecord ()
{
}

void CDbPropertyPage::PriorRecord ()
{
}

void CDbPropertyPage::FirstRecord ()
{
}

void CDbPropertyPage::LastRecord ()
{
}

void CDbPropertyPage::OnCopy ()
{
}

void CDbPropertyPage::OnPaste ()
{
}

void CDbPropertyPage::OnMarkAll ()
{
}

void CDbPropertyPage::OnUnMarkAll ()
{
}

DROPEFFECT CDbPropertyPage::OnDrop (CWnd* pWnd,  COleDataObject* pDataObject,
					              	  DROPEFFECT dropDefault, DROPEFFECT dropList,
									  CPoint point)
{
	return DROPEFFECT_NONE;
}
