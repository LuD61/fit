#ifndef _AKI_GR_STUF_LIST_DEF
#define _AKI_GR_STUF_LIST_DEF
#pragma once
#include <sqlext.h>

class CAkiPrGrStufkList
{
public:
	short mdn;
    long aki_nr;
	long pr_gr_stuf;
	DATE_STRUCT aki_von;
	DATE_STRUCT aki_bis;

	CString zus_bz;
	CAkiPrGrStufkList(void);
	CAkiPrGrStufkList(short, long, long, LPTSTR,DATE_STRUCT,DATE_STRUCT);
	~CAkiPrGrStufkList(void);
};
#endif
