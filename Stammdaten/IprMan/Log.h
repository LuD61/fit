/***************************************************************************/
/* Programmname :  Log.h                                                   */
/*-------------------------------------------------------------------------*/
/* Funktion :  Klasse CToken                                               */
/*-------------------------------------------------------------------------*/
/* Revision : 1.0    Datum : 07.06.04   erstellt von : W. Roth             */
/*                   allgemneine Klasse zum Zerlegen von Strings           */
/*                                                                         */    
/*-------------------------------------------------------------------------*/
/* Aufruf :                                                                */
/* Funktionswert :                                                         */
/* Eingabeparameter :                                                      */
/*                                                                         */
/* Ausgabeparameter :                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
#pragma once
#ifndef _DLOG_DEF
#define _DLOG_DEF
#include <stdio.h>
#include <io.h>

#ifndef MAXLINES
#define MAXLINES 1000
#endif

class CStdLog
{
public:
	static CString Record;
	CString FileName;
	FILE *fp;
	CString Line;

	CStdLog(void);
	~CStdLog(void);

    virtual void Write (CString&);
	virtual void Write ();
	void Println (CString *record);
	virtual void Delete ();
	virtual void Open () {};
	virtual void Close ();
    void Sysdate (char *);
    void Systime (char *);
};

class CRingLog : public CStdLog
{
public:
	long maxrows;
	long row;
	CRingLog () : CStdLog () 
	{
		maxrows = 1000;
		row = 0;
	}
	virtual void Open ();
    virtual void Write (CString&);
	virtual void Write ();
};

class CFifoLog  : public CStdLog
{
public:
    virtual void Write (CString& text);
    void WriteFirstLine (CString& text);
};


#endif
