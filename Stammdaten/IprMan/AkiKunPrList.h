#ifndef _AKI_KUNPR_LIST_DEF
#define _AKI_KUNPR_LIST_DEF
#pragma once

class CAkiKunPrList
{
public:
	short mdn;
   	long aki_nr;
	long kun;
	CString zus_bz;
	CAkiKunPrList(void);
	CAkiKunPrList(short, long, long, LPTSTR);
	~CAkiKunPrList(void);
};
#endif
