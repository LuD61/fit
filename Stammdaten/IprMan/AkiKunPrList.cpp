#include "StdAfx.h"
#include "AkiKunPrList.h"

CAkiKunPrList::CAkiKunPrList(void)
{
	mdn = 0;
	aki_nr = 0;
	kun = 0l;
	zus_bz = _T("");
}

CAkiKunPrList::CAkiKunPrList(short mdn, long aki_nr, long kun, LPTSTR zus_bz)
{
	this->mdn      = mdn;
	this->aki_nr   = aki_nr;
	this->kun      = kun;
	this->zus_bz = zus_bz;
}

CAkiKunPrList::~CAkiKunPrList(void)
{
}
