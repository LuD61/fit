#pragma once
#include "afxwin.h"


// CAddEk-Dialogfeld

class CAddEk : public CDialog
{
	DECLARE_DYNAMIC(CAddEk)

public:
	CAddEk(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CAddEk();

// Dialogfelddaten
	enum { IDD = IDD_EKPROZ };
	CFont *Font;
	CWnd *Parent;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog ();

	DECLARE_MESSAGE_MAP()
public:
	CString EkProz;
	CString EkAbs;
	CEdit m_EkProz;
	CEdit m_EkAbs;
	virtual void OnOK ();
};
