// IprMan.h : Hauptheaderdatei f�r die IprMan-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // Hauptsymbole
#include "sys_par.h"		// 301111
#define SHORTAKI 1

// CIprManApp:
// Siehe IprMan.cpp f�r die Implementierung dieser Klasse
//

class CIprManApp : public CWinApp
{
public:
	CIprManApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnStdPr();
	afx_msg void OnNewPr();
	afx_msg void OnPrAki();
};

extern CIprManApp theApp;

extern SYS_PAR_CLASS Sys_par ;	// 301111
extern int sypgrundartpreis ;
extern bool ischongefragt ;
extern bool igrundartpreis ;