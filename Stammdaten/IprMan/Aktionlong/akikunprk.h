#ifndef _A_AKIKUNPRK_DEF
#define _A_AKIKUNPRK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AKIKUNPRK {
   short          mdn;
   long           kun;
   short          delstatus;
   short          waehrung;
   long           aki_nr;
   TCHAR          zus_bz[25];
   DATE_STRUCT    aki_von;
   DATE_STRUCT    aki_bis;
};
extern struct AKIKUNPRK akikunprk, akikunprk_null;

#line 8 "akikunprk.rh"

class AKIKUNPRK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               AKIKUNPRK akikunprk;
               AKIKUNPRK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
