#ifndef _A_AKIKUNPRP_DEF
#define _A_AKIKUNPRP_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AKIKUNPRP {
   short          mdn;
   long           kun;
   double         a;
   double         ld_pr;
   double         ld_pr_eu;
   double         aki_pr;
   double         aki_pr_eu;
   DATE_STRUCT    aki_von;
   DATE_STRUCT    aki_bis;
   short          waehrung;
   long           aki_nr;
};
extern struct AKIKUNPRP akikunprp, akikunprp_null;

#line 8 "akikunprp.rh"

class AKIKUNPRP_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               AKIKUNPRP akikunlsp;
               AKIKUNPRP_CLASS () : DB_CLASS ()
               {
               }
};
#endif
