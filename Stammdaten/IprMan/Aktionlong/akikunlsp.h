#ifndef _A_AKIKUNLSP_DEF
#define _A_AKIKUNLSP_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AKIKUNLSP {
   short          mdn;
   long           aki_nr;
   double         a;
   double         ld_pr;
   double         ld_pr_eu;
   double         aki_pr;
   double         aki_pr_eu;
   double         me_min;
   double         me_max;
   double         me;
   short          waehrung;
};
extern struct AKIKUNLSP akikunlsp, akikunlsp_null;

#line 8 "akikunlsp.rh"

class AKIKUNLSP_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               AKIKUNLSP akikunlsp;
               AKIKUNLSP_CLASS () : DB_CLASS ()
               {
               }
};
#endif
