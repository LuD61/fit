#ifndef _A_AKIPRGRSTP_DEF
#define _A_AKIPRGRSTP_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AKIPRGRSTP {
   short          mdn;
   long           aki_nr;
   double         a;
   double         ld_pr;
   double         ld_pr_eu;
   double         aki_pr;
   double         aki_pr_eu;
   double         me_min;
   double         me_max;
   double         me;
   short          waehrung;
};
extern struct AKIPRGRSTP akiprgrstp, akiprgrstp_null;

#line 8 "akiprgrstp.rh"

class AKIPRGRSTP_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               AKIPRGRSTP akiprgrstp;
               AKIPRGRSTP_CLASS () : DB_CLASS ()
               {
               }
};
#endif
