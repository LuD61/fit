#ifndef _A_AKIKUNLSK_DEF
#define _A_AKIKUNLSK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AKIKUNLSK {
   short          mdn;
   long           aki_nr;
   TCHAR          zus_bz[25];
   long           pr_gr_stuf;
   long           kun_pr;
   DATE_STRUCT    aki_von;
   DATE_STRUCT    aki_bis;
   short          delstatus;
   short          waehrung;
};
extern struct AKIKUNLSK akikunlsk, akikunlsk_null;

#line 8 "akikunlsk.rh"

class AKIKUNLSK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               AKIKUNLSK akikunlsk;
               AKIKUNLSK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
