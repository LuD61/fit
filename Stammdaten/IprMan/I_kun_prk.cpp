#include "stdafx.h"
#include "i_kun_prk.h"

struct I_KUN_PRK i_kun_prk, i_kun_prk_null;

void I_KUN_PRK_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &i_kun_prk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &i_kun_prk.kun_pr, SQLLONG, 0);
    sqlout ((short *) &i_kun_prk.mdn,SQLSHORT,0);
    sqlout ((long *) &i_kun_prk.kun_pr,SQLLONG,0);
    sqlout ((char *) i_kun_prk.zus_bz,SQLCHAR,25);
    sqlout ((long *) &i_kun_prk.pr_gr_stuf,SQLLONG,0);
    sqlout ((DATE_STRUCT *) &i_kun_prk.gue_ab,SQLDATE,0);
    sqlout ((short *) &i_kun_prk.delstatus,SQLSHORT,0);
    sqlout ((short *) &i_kun_prk.waehrung,SQLSHORT,0);
            cursor = sqlcursor (_T("select i_kun_prk.mdn,  "
"i_kun_prk.kun_pr,  i_kun_prk.zus_bz,  i_kun_prk.pr_gr_stuf,  "
"i_kun_prk.gue_ab,  i_kun_prk.delstatus,  i_kun_prk.waehrung from i_kun_prk ")

#line 13 "Ikun_prk.rpp"
                                  _T("where mdn = ? ")
				  _T("and kun_pr = ?"));
    sqlin ((short *) &i_kun_prk.mdn,SQLSHORT,0);
    sqlin ((long *) &i_kun_prk.kun_pr,SQLLONG,0);
    sqlin ((char *) i_kun_prk.zus_bz,SQLCHAR,25);
    sqlin ((long *) &i_kun_prk.pr_gr_stuf,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &i_kun_prk.gue_ab,SQLDATE,0);
    sqlin ((short *) &i_kun_prk.delstatus,SQLSHORT,0);
    sqlin ((short *) &i_kun_prk.waehrung,SQLSHORT,0);
            sqltext = _T("update i_kun_prk set "
"i_kun_prk.mdn = ?,  i_kun_prk.kun_pr = ?,  i_kun_prk.zus_bz = ?,  "
"i_kun_prk.pr_gr_stuf = ?,  i_kun_prk.gue_ab = ?,  "
"i_kun_prk.delstatus = ?,  i_kun_prk.waehrung = ? ")

#line 16 "Ikun_prk.rpp"
                                  _T("where mdn = ? ")
				  _T("and kun_pr = ?");
            sqlin ((short *)   &i_kun_prk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &i_kun_prk.kun_pr, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &i_kun_prk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &i_kun_prk.kun_pr, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select kun_pr from i_kun_prk ")
                                  _T("where mdn = ? ")
				  _T("and kun_pr = ?"));
            sqlin ((short *)   &i_kun_prk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &i_kun_prk.kun_pr, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from i_kun_prk ")
                                  _T("where mdn = ? ")
				  _T("and kun_pr = ?"));
    sqlin ((short *) &i_kun_prk.mdn,SQLSHORT,0);
    sqlin ((long *) &i_kun_prk.kun_pr,SQLLONG,0);
    sqlin ((char *) i_kun_prk.zus_bz,SQLCHAR,25);
    sqlin ((long *) &i_kun_prk.pr_gr_stuf,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &i_kun_prk.gue_ab,SQLDATE,0);
    sqlin ((short *) &i_kun_prk.delstatus,SQLSHORT,0);
    sqlin ((short *) &i_kun_prk.waehrung,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into i_kun_prk ("
"mdn,  kun_pr,  zus_bz,  pr_gr_stuf,  gue_ab,  delstatus,  waehrung) ")

#line 33 "Ikun_prk.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?)")); 

#line 35 "Ikun_prk.rpp"
}
