#ifndef _CHOICEAKIKUNLS_DEF
#define _CHOICEAKIKUNLS_DEF

#include "ChoiceX.h"
#include "AkiKunLsList.h"
#include "Vector.h"
#include <vector>

class CChoiceAkiKunLs : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
		long m_Mdn;
		CVector *Rows;
	    std::vector<CAkiKunLsList *> AkiKunLsList;
      	CChoiceAkiKunLs(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceAkiKunLs(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchKunPr (CListCtrl *,  LPTSTR);
        void SearchZusBz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CAkiKunLsList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
