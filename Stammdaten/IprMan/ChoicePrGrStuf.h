#ifndef _CHOICEPRGRSTUF_DEF
#define _CHOICEPRGRSTUF_DEF

#include "ChoiceX.h"
#include "PrGrStufList.h"
#include "Vector.h"
#include <vector>

class CChoicePrGrStuf : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
		static int Sort5;

    public :
		long m_Mdn;
		CVector *Rows;
	    std::vector<CPrGrStufList *> PrGrStufList;
      	CChoicePrGrStuf(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoicePrGrStuf(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchPrGrStuf (CListCtrl *,  LPTSTR);
        void SearchZusBz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CPrGrStufList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
