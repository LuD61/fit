#include "StdAfx.h"
#include "KunPrListCtrl.h"
#include "StrFuncs.h"
#include "resource.h"

extern int sypgrundartpreis ;	// 301111
extern bool igrundartpreis;
extern bool ischongefragt;
static bool mehrloeschen = FALSE ;	// 270612

CKunPrListCtrl::CKunPrListCtrl(void)
{
	ChoiceA = NULL;
	ModalChoiceA = TRUE;
	m_Mdn = 1;
	PosA     = POSA;
	PosABz1  = POSABZ1;
	PosVkPr  = POSVKPR;
	PosLdPr  = POSLDPR;
	PosEkAbs = POSEKABS;
	PosEkProz = POSEKPROZ;
	PosRabKz  = POSRABKZ;
	PosRab    = POSRAB;
	PosSk    = POSSK;
	PosSp    = POSSP;
	PosA_grund    = POSA_GRUND;	// 301111


	Position[0] = &PosA;
	Position[1] = &PosABz1;
	Position[2] = &PosVkPr;
	Position[3] = &PosLdPr;
	Position[4] = &PosA_grund;	// 301111  170712 : von 10 auf 4 , rest um 1 erhoeht
	Position[5] = &PosEkAbs;
	Position[6] = &PosEkProz;
	Position[7] = &PosRabKz;
	Position[8] = &PosRab;
	Position[9] = &PosSk;
	Position[10] = &PosSp;

	Position[11] = NULL;	// 301111 10->11
	MaxComboEntries = 20;
	mdn = 0;
	pr_gr_stuf = 0l;
	kun_pr = 0l;
	kun = 0l;
	ListRows.Init ();
	ActiveListRow = TRUE;
	Aufschlag = LIST;
	sk_vollk = 0.0;
	spanne = 0.0;
	Mode = STANDARD;

// 301111 
/*** das ist hier zu fr�h  300512
	if ( sypgrundartpreis > 0)
		ischongefragt = FALSE ;
	else
	{
		igrundartpreis = FALSE;
		ischongefragt = TRUE ;
	}
	****/


}

CKunPrListCtrl::~CKunPrListCtrl(void)
{
	if (ChoiceA != NULL)
	{
		delete ChoiceA;
	}
	DestroyRows (ListRows);
// 301111
	if ( sypgrundartpreis > 0)
		ischongefragt = FALSE ;
	else
	{
		igrundartpreis = FALSE;
		ischongefragt = TRUE ;
	}
}

BEGIN_MESSAGE_MAP(CKunPrListCtrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CKunPrListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (PosVkPr, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (PosA, 0);
	}
	PerformListChangeHandler ();
}

void CKunPrListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	if (col == PosABz1) return;
	if (col == PosA_grund) return;	// 180712

	if (Mode == TERMIN && col == PosVkPr + 2) return;
	if (Mode == TERMIN && col == PosLdPr + 2) return;
	if (col == PosA)
	{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
	}
	else if (col == PosRabKz)
	{
		CEditListCtrl::StartEnterCheckBox (col, row);
	}
	else
	{
		CEditListCtrl::StartEnter (col, row);
	}
}

void CKunPrListCtrl::StopEnter ()
{

//	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		int check = ListCheckBox.GetCheck ();
        if (check == BST_CHECKED)
		{
			FillList.SetItemText (_T("X"), EditRow, EditCol);
		}
		else
		{
			FillList.SetItemText (_T(" "), EditRow, EditCol);
		}
		ListCheckBox.DestroyWindow ();
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	CPreise *p = (CPreise *) ListRows.Get (EditRow);
	if (p != NULL)
	{
		int aktion =0 ;	// 301111
 		CString A =  GetItemText (EditRow, PosA);
		p->ipr.a = CStrFuncs::StrToDouble (A);
 		A =  GetItemText (EditRow, PosA_grund);	// 301111
		p->ipr.a_grund = CStrFuncs::StrToDouble (A);	// 301111

		CString VkPr =  GetItemText (EditRow, PosVkPr);
		if ( p->ipr.vk_pr_eu != CStrFuncs::StrToDouble (VkPr)) aktion = 1;	// 301111
		p->ipr.vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
		p->ipr.vk_pr_i = p->ipr.vk_pr_eu;
		CString LdPr =  GetItemText (EditRow, PosLdPr);
		if ( p->ipr.ld_pr_eu != CStrFuncs::StrToDouble (LdPr)) aktion = 1;	// 301111
		p->ipr.ld_pr_eu = CStrFuncs::StrToDouble (LdPr);
		p->ipr.ld_pr = p->ipr.ld_pr_eu;

// 301111 A
		if ( ( EditCol == PosVkPr || EditCol == PosLdPr) && aktion == 1 )
			GrundArtikelpreise ( p , EditRow, EditCol );
	}
	PerformListChangeHandler (EditRow, EditCol);
}

// 301111 
void CKunPrListCtrl::GrundArtikelpreise (CPreise *p ,int EditRow, int EditCol )
{
	if ( sypgrundartpreis < 1 )
		return ;	// Ablauf nicht aktiv

	if ( mehrloeschen == TRUE )
		return ;	// 270612 doppelmoppel


	if ( ischongefragt == TRUE )
		if ( igrundartpreis  == FALSE )
			return ;	// NEIN und nix machen

// bereits vorher entschieden ....300512
// 270612 : es sollte doch hier entschieden werden, weil vorher noch nichts entschieden ist ......

	if ( p->ipr.a != p->ipr.a_grund || p->ipr.a < 0.99 )
		return ;

	A_bas.a_bas.a = p->ipr.a;
	A_bas.dbreadfirst ();
	if ( A_bas.a_bas.me_einh != 2 && sypgrundartpreis == 1 ) //240412 bei sypgrundartpreis == 2 keine Einschr�nkung �ber me_einh = 2
		return ;
// 270612 : bis hierher raktiviert .......
	if ( ischongefragt == FALSE )
	{

			ischongefragt = TRUE ;	// wird hier sonst 2 mal durchlaufen 
			igrundartpreis   = FALSE ;	// NEIN und nix machen

		if (MessageBox ( _T("�nderung auch f�r Artikelvarianten ?"),NULL, MB_YESNO | MB_ICONQUESTION ) == IDNO)
		{
			ischongefragt = TRUE ;	// NEIN und nix machen
			igrundartpreis   = FALSE ;	// NEIN und nix machen
			return ;
		}
		else
		{
			ischongefragt = TRUE ;	// JA und was machen
			igrundartpreis   = TRUE ;	// JA und was machen
		}
	}
	if ( igrundartpreis == FALSE )
		return ;
// Ab hier geht es eigentlich erst los ....
	double savea ;

// Hier steht genau eine Kundenpreisliste,
// alle anderen Hierarchien werden zum Schluss per sql aktualisiert........

	savea = p->ipr.a;
	double va_grund;
	double va;
	CString VkPr = GetItemText (EditRow, PosVkPr);
	CString LdPr = GetItemText (EditRow, PosLdPr);

	int count = GetItemCount ();


	// 280612 : alle anhaengenden Artikel reinzwiebeln auf Kundenebene	100712 : bei "TIMM" fuer alle Tabseiten aktiv, jedoch nur f�er me_einh==2 
	if ( ( sypgrundartpreis == 2  && kun > 0 ) || sypgrundartpreis == 1 )
	{
		double	abgel_a[200];
		int abgel_ac = 0;
		int abgel_acp = 0;
		A_bas.a_bas.a_grund = savea ;
		int sqlstatintern = A_bas.dbreadfirstlock( ); 
		while ( abgel_ac < 200 && ! sqlstatintern )
		{
			if (A_bas.a_bas.a != savea )	// Grundartikel selber nicht mit murkeln
			{
				if ( sypgrundartpreis == 2 || (sypgrundartpreis == 1 && A_bas.a_bas.me_einh == 2) )	// 100712
				{
					abgel_a[abgel_ac] = A_bas.a_bas.a ;
					abgel_ac ++ ;
				}
			}
			sqlstatintern = A_bas.dbreadlock( ); 
			if ( sqlstatintern )
				break ;
		}
		
		for ( ; abgel_acp < abgel_ac ; abgel_acp ++ )
		{
			int i = 0 ;
			for ( ; i < count ; i++)
			{
				CString A = GetItemText(i, PosA );
				va = CStrFuncs::StrToDouble (A);
				if ( va == abgel_a[abgel_acp] )
					break ;
			}
			if ( i == count )	// es wurde nix gefunden 
			{
// Es folgt appe-satz 
//				int rowCount = GetItemCount ();
				if (count > 0)
				{
					CString VkPr =	GetItemText (EditRow, PosVkPr);
					CString LdPr =	GetItemText (EditRow, PosLdPr);
					CString EkAbs = GetItemText(EditRow, PosEkAbs);
					CString EkProz= GetItemText(EditRow, PosEkProz);

				/* ------>
					if (Mode == AKTION)
					{
						CString Rab = GetItemText (EditRow, PosRab);
						CString RabKz = GetItemText (EditRow, PosRabKz);
						if ((StrToDouble (VkPr) == 0.0) &&
							(StrToDouble (LdPr) == 0.0) && 
							(RabKz != "X" || StrToDouble (Rab) == 0.0))
						{
							return FALSE;
						}
					}
					else
					{
						if ((StrToDouble (VkPr) == 0.0) &&
							(StrToDouble (LdPr) == 0.0))
						{
							return FALSE;
						}

					}
< ----- */
					int colCount = GetHeaderCtrl ()->GetItemCount ();
					FillList.InsertItem (count, -1);

					CString A;
					A.Format (_T("%.0lf"),abgel_a[abgel_acp] );
					FillList.SetItemText (A.GetBuffer (), count, PosA);
					A_bas.a_bas.a = abgel_a[abgel_acp] ;	// 100712
					A_bas.dbreadfirst() ;

					A.Format (_T("%.0lf"),savea );
					FillList.SetItemText (A.GetBuffer (), count, PosA_grund);

					FillList.SetItemText (_T(A_bas.a_bas.a_bz1), count, PosABz1);	// das fuellen wir spaeter ...... oder lieber jetzt 100712
					FillList.SetItemText (VkPr.GetBuffer() , count, PosVkPr);
					FillList.SetItemText (LdPr.GetBuffer() , count, PosLdPr);
					FillList.SetItemText (EkAbs.GetBuffer(), count, PosEkAbs);
					FillList.SetItemText (EkProz.GetBuffer(), count, PosEkProz);

					IPR ipr;
					memcpy (&ipr, &ipr_null, sizeof (IPR));
					ipr.mdn = mdn;
					ipr.pr_gr_stuf = pr_gr_stuf;
					ipr.kun_pr = kun_pr;
					ipr.kun    = kun;
					CPreise *p = new CPreise (CString ("0,00"), CString ("0,00"), ipr);	
					if (ActiveListRow)
					{
						ListRows.Add (p);
					}
					count ++ ;
				}	// count > 0 
			}	// count < 1
		}	// while 

	}	// sypgrundartpreis == 2
	// 280612 : Essig-Automatik-Einfuegerei zuende 

	for ( int i = 0 ; i < count ; i++)
	{
		CString A_grund = GetItemText( i, PosA_grund );
		va_grund = CStrFuncs::StrToDouble (A_grund);
		if ( va_grund == savea )
		{
			CString A = GetItemText(i, PosA );
			va = CStrFuncs::StrToDouble (A);
			A_bas.a_bas.a = va; 
			A_bas.dbreadfirst ();
			if ( A_bas.a_bas.me_einh != 2 && sypgrundartpreis == 1 ) //240412 bei sypgrundartpreis == 2 keine Einschr�nkung �ber me_einh = 2
				continue ;
			FillList.SetItemText (VkPr.GetBuffer (0), i, PosVkPr);
			FillList.SetItemText (LdPr.GetBuffer (0), i, PosLdPr);
		}
	}
	A_bas.a_bas.a = savea;	// Wieder auf Original zeigen 
	A_bas.dbreadfirst ();
}


void CKunPrListCtrl::SetSel (CString& Text)
{

   if (EditCol == PosA || EditCol == PosVkPr ||
	   EditCol == PosLdPr || EditCol == PosEkAbs ||
	   EditCol == PosEkProz || EditCol == PosRab)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CKunPrListCtrl::FormatText (CString& Text)
{
    if (EditCol == PosVkPr)
	{
		DoubleToString (StrToDouble (Text), Text, 4);
	}
    else if (EditCol == PosLdPr)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
    else if (EditCol == PosEkAbs)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
    else if (EditCol == PosEkProz)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
    else if (Mode == AKTION && EditCol == PosRab)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
/*
    else if (EditCol == 4)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
    else if (EditCol == 5)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
*/
}

void CKunPrListCtrl::NextRow ()
{
	int count = GetItemCount ();
    GetEnter ();
	if (EditRow >= count - 1)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if (Mode == AKTION)
		{
			CString Rab = GetItemText (EditRow, PosRab);
			CString RabKz = GetItemText (EditRow, PosRabKz);
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0) && 
				(RabKz != "X" || StrToDouble (Rab) == 0.0))
			{
			return;
			}
		}
		else
		{
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0))
			{
			return;
			}
		}
	}
	if (EditCol == PosA)
	{
		ReadABz1 ();
	}
	SetEditText ();
	TestIprIndex ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;
	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	PerformListChangeHandler ();
}

void CKunPrListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
    GetEnter ();
	int count = GetItemCount ();
	if (EditRow == count - 1)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if ((StrToDouble (VkPr) == 0.0) &&
			(StrToDouble (LdPr) == 0.0))
		{
			ListRows.Drop (EditRow);
	        DeleteItem (EditRow);
		}
	}
	else
	{
		if (EditRow <= 0)
		{
			return;
		}
		if (IsWindow (SearchListCtrl.Edit.m_hWnd) && EditCol == PosA)
		{
			ReadABz1 ();
		}
		TestIprIndex ();
	}

	StopEnter ();
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
	PerformListChangeHandler ();
}

BOOL CKunPrListCtrl::LastCol ()
{
	if (EditCol < PosLdPr) return FALSE;
	if (Mode == TERMIN)
	{
		if (EditCol >= PosLdPr)
		{
			return TRUE;
		}
		return FALSE;
	}

	if (Mode == AKTION)
	{
		if (EditCol >= PosRab)
		{
			return TRUE;
		}
		return FALSE;
	}

	if (Aufschlag == LIST || Aufschlag == ALL)
	{
		if (EditCol < PosEkProz) return FALSE;
	}
	return TRUE;
}


void CKunPrListCtrl::OnReturn ()
{
    GetEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if (Mode == AKTION)
		{
			CString Rab = GetItemText (EditRow, PosRab);
			CString RabKz = GetItemText (EditRow, PosRabKz);
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0) && 
				(RabKz != "X" || StrToDouble (Rab) == 0.0))
			{
			return;
			}
		}
		else
		{
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0))
			{
			return;
			}
		}
	}
	if (EditCol == PosA)
	{
		ReadABz1 ();
	}
	if (LastCol ())
	{
		TestIprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
		if (EditRow == rowCount)
		{
			EditCol = PosA;
		}
		else
		{
			EditCol = PosVkPr;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
		if (EditCol == PosABz1 || EditCol == PosA_grund) // 180712 : posa_grund dazu
		{
			EditCol ++;
		}
		if (Mode == AKTION)
		{
			if (EditCol == PosLdPr + 1)
			{
				EditCol += 2;
			}
		}
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	PerformListChangeHandler ();
}

void CKunPrListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol == PosLdPr)
	{
		return;
	}
	if (EditCol == PosA)
	{
		ReadABz1 ();
	}
	StopEnter ();
	EditCol ++;
	if (EditCol == PosABz1 || EditCol == PosA_grund) // 180712 : a_grund dazu
	{
		EditCol ++;
	}
	if (Mode == AKTION)
	{
			if (EditCol == PosLdPr + 1)
			{
				EditCol += 2;
			}
	}
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CKunPrListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	if (EditCol == PosA)
	{
		ReadABz1 ();
	}
	StopEnter ();
	EditCol --;
	if (EditCol == PosABz1 || EditCol == PosA_grund) // 180712 : a_grund dazu
	{
		EditCol --;
	}
	if (Mode == AKTION)
	{
			if (EditCol == PosLdPr + 2)
			{
				EditCol -= 2;
			}
	}
    StartEnter (EditCol, EditRow);
}

BOOL CKunPrListCtrl::InsertRow ()
{
	CString VkPr = GetItemText (EditRow, PosVkPr);
	CString LdPr = GetItemText (EditRow, PosLdPr);
	if (Mode == AKTION)
	{
			CString Rab = GetItemText (EditRow, PosRab);
			CString RabKz = GetItemText (EditRow, PosRabKz);
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0) && 
				(RabKz != "X" || StrToDouble (Rab) == 0.0))
			{
				return FALSE;
			}
	}
	else
	{
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0))
			{
				return FALSE;
			}
	}
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T("0 "), EditRow, PosA);
	FillList.SetItemText (_T(""), EditRow, PosABz1);
	FillList.SetItemText (_T("0,00 "), EditRow, PosVkPr);
	FillList.SetItemText (_T("0,00 "), EditRow, PosLdPr);
	FillList.SetItemText (_T("0,00 "), EditRow, PosEkAbs);
	FillList.SetItemText (_T("0,00 "), EditRow, PosEkProz);

	IPR ipr;
	memcpy (&ipr, &ipr_null, sizeof (IPR));
	ipr.mdn = mdn;
	ipr.pr_gr_stuf = pr_gr_stuf;
	ipr.kun_pr = kun_pr;
	ipr.kun    = kun;
    CPreise *p = new CPreise (CString ("0,00"), CString ("0,00"),
							  ipr);	
	if (ActiveListRow)
	{
		ListRows.Insert (EditRow, p);
	}
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CKunPrListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;


// 270612 A
	mehrloeschen = FALSE ;
	double a_grund = 0.0 ;
	if  ( (
		     ((sypgrundartpreis == 2 ) && ( kun > 0 ))
	       || (sypgrundartpreis == 1  )					// 100712/110712  : Timm dazu 
		  )
		&& ( Mode == STANDARD )
		)
	{
		CString A = GetItemText (EditRow, PosA);
		CString A_grund = GetItemText (EditRow, PosA_grund);
		if ( StrToDouble (A) > 0.9 && ( StrToDouble (A)  == StrToDouble (A_grund)) )
		{

			if (sypgrundartpreis == 1  )					// 110712 A : Timm nur fuer me_einh = 2
			{
				double a_selbst = CStrFuncs::StrToDouble (A);
				A_bas.a_bas.a = a_selbst; 
				A_bas.dbreadfirst ();	// ich hoffe, das ist sqlstatus immer == 0 
				if ( A_bas.a_bas.me_einh != 2 )
					mehrloeschen = FALSE ;
				else	// 110712 : bisher gab es keine Unterscheidung
				{
					if (MessageBox (_T("Abgeleitete Artikel entfernen ?"), NULL, 
							 MB_YESNO | MB_ICONQUESTION) ==	IDYES )
					{
						a_grund = StrToDouble (A_grund );
						mehrloeschen = TRUE;
					}
				}

			}
			else	// 110712 : so war es bisher ...
			{
				if (MessageBox (_T("Abgeleitete Artikel entfernen ?"), NULL, 
						 MB_YESNO | MB_ICONQUESTION) ==	IDYES )
				{
					a_grund = StrToDouble (A_grund );
					mehrloeschen = TRUE;
				}
			}
		}
	}
	if ( !mehrloeschen )
	{	// das war bisher das Ende .....
		ListRows.Drop (EditRow);
		return CEditListCtrl::DeleteRow ();
	}
	else
	{
		int anz = ListRows.GetAnz() ;
		int ind = 0;
		while ( ind < anz )
		{
			CString A_grund = GetItemText (ind, PosA_grund);
			if ( a_grund == StrToDouble (A_grund))
			{

// 110712 A : me_einh beachten .....
				if ( sypgrundartpreis == 1 )
				{
					CString A_selbst = GetItemText (ind, PosA);
					double a_selbst = CStrFuncs::StrToDouble (A_selbst);
					A_bas.a_bas.a = a_selbst; 
					A_bas.dbreadfirst ();	// ich hoffe, das ist sqlstatus immer == 0 
					if ( A_bas.a_bas.me_einh != 2 )
					{
						ind ++ ;
					}
					else
					{
						ListRows.Drop (ind);
						EditRow = ind ;
						CEditListCtrl::DeleteRow ();
						anz -- ;
					}

				}
				else	// me_einh NICHT beachten ( like Essig )
// 110712 E : me_einh beachten .....
				{
					ListRows.Drop (ind);
					EditRow = ind ;
					CEditListCtrl::DeleteRow ();
					anz -- ;
				}
			}
			else
				ind ++ ;
		}
		mehrloeschen = FALSE ;
		return TRUE ;
	}
}

BOOL CKunPrListCtrl::AppendEmpty ()
{

	int rowCount = GetItemCount ();
	if (rowCount > 0)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if (Mode == AKTION)
		{
			CString Rab = GetItemText (EditRow, PosRab);
			CString RabKz = GetItemText (EditRow, PosRabKz);
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0) && 
				(RabKz != "X" || StrToDouble (Rab) == 0.0))
			{
				return FALSE;
			}
		}
		else
		{
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0))
			{
				return FALSE;
			}
		}
	}
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText (_T("0 "), rowCount, PosA);
	FillList.SetItemText (_T(""), rowCount, PosABz1);
	FillList.SetItemText (_T("0,00 "), rowCount, PosVkPr);
	FillList.SetItemText (_T("0,00 "), rowCount, PosLdPr);
	FillList.SetItemText (_T("0,00 "), rowCount, PosEkAbs);
	FillList.SetItemText (_T("0,00 "), rowCount, PosEkProz);
	IPR ipr;
	memcpy (&ipr, &ipr_null, sizeof (IPR));
	ipr.mdn = mdn;
	ipr.pr_gr_stuf = pr_gr_stuf;
	ipr.kun_pr = kun_pr;
	ipr.kun    = kun;
    CPreise *p = new CPreise (CString ("0,00"), CString ("0,00"),
							  ipr);	
	if (ActiveListRow)
	{
		ListRows.Add (p);
	}
	return TRUE;
}

void CKunPrListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CKunPrListCtrl::RunItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
*/
}

void CKunPrListCtrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CKunPrListCtrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CKunPrListCtrl::OnChoice ()
{
	if (EditCol == PosA)
	{
		OnAChoice (CString (_T("")));
	}
}

void CKunPrListCtrl::OnAChoice (CString& Search)
{
	AChoiceStat = TRUE;
	if (ChoiceA != NULL && !ModalChoiceA)
	{
		ChoiceA->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceA == NULL)
	{
		ChoiceA = new CChoiceA (this);
	    ChoiceA->IsModal = ModalChoiceA;
	    ChoiceA->HideEnter = FALSE;
	    ChoiceA->HideFilter = FALSE;
		ChoiceA->IdArrDown = IDI_HARROWDOWN;
		ChoiceA->IdArrUp   = IDI_HARROWUP;
		ChoiceA->IdArrNo   = IDI_HARROWNO;
		ChoiceA->CreateDlg ();
	}

    ChoiceA->SetDbClass (&A_bas);
	ChoiceA->SearchText = Search;
	if (ModalChoiceA)
	{
			ChoiceA->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceA->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceA->MoveWindow (&rect);
		ChoiceA->SetFocus ();
		return;
	}
    if (ChoiceA->GetState ())
    {
		  CABasList *abl = ChoiceA->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  AChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		  A_bas.a_bas.a = abl->a;
		  A_bas.dbreadfirst ();
		  CString Text;
		  Text.Format (_T("%.0lf"), A_bas.a_bas.a);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
		  CString ABz1;
		  ABz1.Format (_T("%s"), A_bas.a_bas.a_bz1);
		  FillList.SetItemText (ABz1.GetBuffer (), EditRow, PosABz1);
    }
	else
	{
		  AChoiceStat = FALSE;
		  SearchListCtrl.Edit.SetFocus ();
		  CString Text;
		  SearchListCtrl.Edit.GetWindowText (Text);
          SetSearchSel (Text);
	}
}


void CKunPrListCtrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}

void CKunPrListCtrl::ReadABz1 ()
{
	if (EditCol != PosA) return;
    memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
    CString Text;
	SearchListCtrl.Edit.GetWindowText (Text);
	if (!CStrFuncs::IsDecimal (Text))
	{
		OnAChoice (Text);
	    SearchListCtrl.Edit.GetWindowText (Text);
		Text.Format (_T("%.0lf"), CStrFuncs::StrToDouble (Text.GetBuffer ()));
	    SearchListCtrl.Edit.SetWindowText (Text);
		if (!AChoiceStat)
		{
			EditCol --;
			return;
		}
	}
	A_bas.a_bas.a = CStrFuncs::StrToDouble (Text);
	if (A_bas.dbreadfirst () != 0)
	{
		MessageBox (_T("Artikel nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return;
	}
    CString ABz1;
	ABz1.Format (_T("%s"), A_bas.a_bas.a_bz1);
	FillList.SetItemText (ABz1.GetBuffer (), EditRow, PosABz1);
	CString A_grund;													// 270612
	A_grund.Format (_T("%.0lf"), A_bas.a_bas.a_grund);					// 270612
	FillList.SetItemText (A_grund.GetBuffer (), EditRow, PosA_grund);	// 270612
	Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 170712


	if (Mode == STANDARD)
	{
		return;
	}
	memcpy (&Ipr.ipr, &ipr_null, sizeof (IPR));
	Ipr.ipr.mdn = mdn;
	Ipr.ipr.pr_gr_stuf = pr_gr_stuf;
	Ipr.ipr.kun_pr = kun_pr;
	Ipr.ipr.kun = kun;
	Ipr.ipr.a = A_bas.a_bas.a;
	Ipr.dbreadfirst ();
	Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 301111
//	CString A_grund;						// 301111
	A_grund.Format (_T("%.0lf"), A_bas.a_bas.a_grund);	// 301111
	FillList.SetItemText (A_grund.GetBuffer (), EditRow, PosA_grund);	// 301111
	CString VkPr;
	DoubleToString (Ipr.ipr.vk_pr_eu, VkPr, 4);
	FillList.SetItemText (VkPr.GetBuffer (), EditRow, PosVkPr + 2);
	CString LdPr;
	DoubleToString (Ipr.ipr.ld_pr_eu, LdPr, 2);
	FillList.SetItemText (LdPr.GetBuffer (), EditRow, PosLdPr + 2);
}
void CKunPrListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	Text = cText.Trim ();
}

void CKunPrListCtrl::TestIprIndex ()
{
	int Items = GetItemCount ();
	if (Items <= 1) return;
	CString Value;
	GetColValue (EditRow, PosA, Value);
	double rA = CStrFuncs::StrToDouble (Value);
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosA, Value);
 	    double lA = CStrFuncs::StrToDouble (Value);
		if (lA != rA) continue;
		ListRows.Drop (i);
 	    DeleteItem (i);
		InvalidateRect (NULL);
		if ( i < EditRow) EditRow --;
		return;
	}
}

void CKunPrListCtrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}

void CKunPrListCtrl::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

