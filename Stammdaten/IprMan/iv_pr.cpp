#include "stdafx.h"
#include "iv_pr.h"

struct IV_PR iv_pr, iv_pr_null;

void IV_PR_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &iv_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &iv_pr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &iv_pr.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &iv_pr.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &iv_pr.kun, SQLLONG, 0);
    sqlout ((short *) &iv_pr.mdn,SQLSHORT,0);
    sqlout ((long *) &iv_pr.pr_gr_stuf,SQLLONG,0);
    sqlout ((long *) &iv_pr.kun_pr,SQLLONG,0);
    sqlout ((double *) &iv_pr.a,SQLDOUBLE,0);
    sqlout ((double *) &iv_pr.vk_pr_i,SQLDOUBLE,0);
    sqlout ((double *) &iv_pr.vk_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &iv_pr.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &iv_pr.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &iv_pr.gue_ab,SQLDATE,0);
    sqlout ((short *) &iv_pr.waehrung,SQLSHORT,0);
    sqlout ((long *) &iv_pr.kun,SQLLONG,0);
            cursor = sqlcursor (_T("select iv_pr.mdn,  ")
_T("iv_pr.pr_gr_stuf,  iv_pr.kun_pr,  iv_pr.a,  iv_pr.vk_pr_i,  ")
_T("iv_pr.vk_pr_eu,  iv_pr.ld_pr,  iv_pr.ld_pr_eu,  iv_pr.gue_ab,  ")
_T("iv_pr.waehrung,  iv_pr.kun from iv_pr ")

#line 16 "iv_pr.rpp"
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?"));
    sqlin ((short *) &iv_pr.mdn,SQLSHORT,0);
    sqlin ((long *) &iv_pr.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &iv_pr.kun_pr,SQLLONG,0);
    sqlin ((double *) &iv_pr.a,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &iv_pr.gue_ab,SQLDATE,0);
    sqlin ((short *) &iv_pr.waehrung,SQLSHORT,0);
    sqlin ((long *) &iv_pr.kun,SQLLONG,0);
            sqltext = _T("update iv_pr set iv_pr.mdn = ?,  ")
_T("iv_pr.pr_gr_stuf = ?,  iv_pr.kun_pr = ?,  iv_pr.a = ?,  ")
_T("iv_pr.vk_pr_i = ?,  iv_pr.vk_pr_eu = ?,  iv_pr.ld_pr = ?,  ")
_T("iv_pr.ld_pr_eu = ?,  iv_pr.gue_ab = ?,  iv_pr.waehrung = ?,  ")
_T("iv_pr.kun = ? ")

#line 22 "iv_pr.rpp"
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?");
            sqlin ((short *)   &iv_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &iv_pr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &iv_pr.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &iv_pr.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &iv_pr.kun, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &iv_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &iv_pr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &iv_pr.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &iv_pr.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &iv_pr.kun, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select a from iv_pr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?"));
            sqlin ((short *)   &iv_pr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &iv_pr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &iv_pr.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &iv_pr.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &iv_pr.kun, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from iv_pr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?"));
    sqlin ((short *) &iv_pr.mdn,SQLSHORT,0);
    sqlin ((long *) &iv_pr.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &iv_pr.kun_pr,SQLLONG,0);
    sqlin ((double *) &iv_pr.a,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &iv_pr.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &iv_pr.gue_ab,SQLDATE,0);
    sqlin ((short *) &iv_pr.waehrung,SQLSHORT,0);
    sqlin ((long *) &iv_pr.kun,SQLLONG,0);
            ins_cursor = sqlcursor (_T("insert into iv_pr (")
_T("mdn,  pr_gr_stuf,  kun_pr,  a,  vk_pr_i,  vk_pr_eu,  ld_pr,  ld_pr_eu,  gue_ab,  ")
_T("waehrung,  kun) ")

#line 57 "iv_pr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?)")); 

#line 59 "iv_pr.rpp"
}

BOOL IV_PR_CLASS::operator== (IV_PR& iv_pr)
{
            if (this->iv_pr.pr_gr_stuf != iv_pr.pr_gr_stuf) return FALSE;  
            if (this->iv_pr.kun_pr     != iv_pr.kun_pr) return FALSE;  
            if (this->iv_pr.kun        != iv_pr.kun) return FALSE;  
            if (this->iv_pr.a          != iv_pr.a) return FALSE;  
            return TRUE;
} 
