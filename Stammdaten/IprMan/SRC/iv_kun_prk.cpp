#include "stdafx.h"
#include "iv_kun_prk.h"

struct IV_KUN_PRK iv_kun_prk, iv_kun_prk_null;

void IV_KUN_PRK_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &iv_kun_prk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &iv_kun_prk.kun_pr, SQLLONG, 0);
    sqlout ((short *) &iv_kun_prk.mdn,SQLSHORT,0);
    sqlout ((long *) &iv_kun_prk.kun_pr,SQLLONG,0);
    sqlout ((TCHAR *) iv_kun_prk.zus_bz,SQLCHAR,25);
    sqlout ((long *) &iv_kun_prk.pr_gr_stuf,SQLLONG,0);
    sqlout ((DATE_STRUCT *) &iv_kun_prk.gue_ab,SQLDATE,0);
    sqlout ((TCHAR *) iv_kun_prk.ueb_kz,SQLCHAR,2);
    sqlout ((short *) &iv_kun_prk.delstatus,SQLSHORT,0);
    sqlout ((short *) &iv_kun_prk.waehrung,SQLSHORT,0);
            cursor = sqlcursor (_T("select iv_kun_prk.mdn,  ")
_T("iv_kun_prk.kun_pr,  iv_kun_prk.zus_bz,  iv_kun_prk.pr_gr_stuf,  ")
_T("iv_kun_prk.gue_ab,  iv_kun_prk.ueb_kz,  iv_kun_prk.delstatus,  ")
_T("iv_kun_prk.waehrung from iv_kun_prk ")

#line 13 "iv_kun_prk.rpp"
                                  _T("where mdn = ? ")
				  _T("and kun_pr = ?"));
    sqlin ((short *) &iv_kun_prk.mdn,SQLSHORT,0);
    sqlin ((long *) &iv_kun_prk.kun_pr,SQLLONG,0);
    sqlin ((TCHAR *) iv_kun_prk.zus_bz,SQLCHAR,25);
    sqlin ((long *) &iv_kun_prk.pr_gr_stuf,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &iv_kun_prk.gue_ab,SQLDATE,0);
    sqlin ((TCHAR *) iv_kun_prk.ueb_kz,SQLCHAR,2);
    sqlin ((short *) &iv_kun_prk.delstatus,SQLSHORT,0);
    sqlin ((short *) &iv_kun_prk.waehrung,SQLSHORT,0);
            sqltext = _T("update iv_kun_prk set ")
_T("iv_kun_prk.mdn = ?,  iv_kun_prk.kun_pr = ?,  iv_kun_prk.zus_bz = ?,  ")
_T("iv_kun_prk.pr_gr_stuf = ?,  iv_kun_prk.gue_ab = ?,  ")
_T("iv_kun_prk.ueb_kz = ?,  iv_kun_prk.delstatus = ?,  ")
_T("iv_kun_prk.waehrung = ? ")

#line 16 "iv_kun_prk.rpp"
                                  _T("where mdn = ? ")
				  _T("and kun_pr = ?");
            sqlin ((short *)   &iv_kun_prk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &iv_kun_prk.kun_pr, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &iv_kun_prk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &iv_kun_prk.kun_pr, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select kun_pr from iv_kun_prk ")
                                  _T("where mdn = ? ")
				  _T("and kun_pr = ?"));
            sqlin ((short *)   &iv_kun_prk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &iv_kun_prk.kun_pr, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from iv_kun_prk ")
                                  _T("where mdn = ? ")
				  _T("and kun_pr = ?"));
    sqlin ((short *) &iv_kun_prk.mdn,SQLSHORT,0);
    sqlin ((long *) &iv_kun_prk.kun_pr,SQLLONG,0);
    sqlin ((TCHAR *) iv_kun_prk.zus_bz,SQLCHAR,25);
    sqlin ((long *) &iv_kun_prk.pr_gr_stuf,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &iv_kun_prk.gue_ab,SQLDATE,0);
    sqlin ((TCHAR *) iv_kun_prk.ueb_kz,SQLCHAR,2);
    sqlin ((short *) &iv_kun_prk.delstatus,SQLSHORT,0);
    sqlin ((short *) &iv_kun_prk.waehrung,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into iv_kun_prk (")
_T("mdn,  kun_pr,  zus_bz,  pr_gr_stuf,  gue_ab,  ueb_kz,  delstatus,  waehrung) ")

#line 33 "iv_kun_prk.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?)")); 

#line 35 "iv_kun_prk.rpp"
}
