// PrArtPreise.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "IprMan.h"
#include "PrArtPage.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Process.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CPrArtPage Dialogfeld

CPrArtPage::CPrArtPage(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage()
{
	Choice = NULL;
	ModalChoice = TRUE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	IprCursor = -1;
	IprGrStufkCursor = -1;
	IKunPrkCursor = -1;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_A);
	PosControls.Add (&m_List);

	ButtonControls.Add (&m_Cancel);
	ButtonControls.Add (&m_Save);
	ButtonControls.Add (&m_Delete);
	ButtonControls.Add (&m_Insert);

    HideButtons = FALSE;

	Frame = NULL;
	IprCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	SearchA = _T("");
	Separator = _T(";");
	CellHeight = 0;
	Cfg.SetProgName( _T("IPrDialog"));
}

CPrArtPage::CPrArtPage(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	Frame = NULL;
	IprCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
}

CPrArtPage::~CPrArtPage()
{
	Font.DeleteObject ();
	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	Ipr.dbclose ();
	IprGrStufk.dbclose ();
	Kun.dbclose ();
	KunAdr.dbclose ();
	if (IprCursor != -1)
	{
		A_bas.sqlclose (IprCursor);
	}
	if (IprGrStufkCursor != -1)
	{
		A_bas.sqlclose (IprGrStufkCursor);
	}
	if (IKunPrkCursor != -1)
	{
		A_bas.sqlclose (IKunPrkCursor);
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);
}

void CPrArtPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_LA_BZ1, m_LA_bz1);
	DDX_Control(pDX, IDC_A_BZ1, m_A_bz1);
	DDX_Control(pDX, IDC_A_BZ2, m_A_bz2);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_CANCEL, m_Cancel);
	DDX_Control(pDX, IDC_SAVE, m_Save);
	DDX_Control(pDX, IDC_DELETE, m_Delete);
	DDX_Control(pDX, IDC_INSERT, m_Insert);
}

BEGIN_MESSAGE_MAP(CPrArtPage, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_SIZE ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_ACHOICE ,  OnAchoice)
	ON_BN_CLICKED(IDC_CANCEL ,   OnCancel)
	ON_BN_CLICKED(IDC_SAVE ,     OnSave)
	ON_BN_CLICKED(IDC_DELETE ,   OnDelete)
	ON_BN_CLICKED(IDC_INSERT ,   OnInsert)
	ON_COMMAND (SELECTED, OnASelected)
	ON_COMMAND (CANCELED, OnACanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
END_MESSAGE_MAP()


// CPrArtPage Meldungshandler

BOOL CPrArtPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	A_bas.opendbase (_T("bws"));

	CUtil::GetPersName (PersName);
	PgrProt.Construct (PersName, CString ("11112"), &Ipr);
     
	ReadCfg ();

	if (HideButtons)
	{
		ButtonControls.SetVisible (FALSE);
		RightListSpace = 15;
	}
	else
	{
		ButtonControls.SetVisible (TRUE);
		RightListSpace = 125;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	m_Cancel.SetWindowText (_T("Beenden"));
    HBITMAP HbF5 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F5T", "F5MASKT");
	m_Cancel.SetBitmap (HbF5);

    HBITMAP HbF12 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F12T", "F12MASKT");
	m_Save.SetBitmap (HbF12);

    HBITMAP HbDel = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "DELT", "DELMASKT");
	m_Delete.SetBitmap (HbDel);

    HBITMAP HbInsert = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "INSERTT", "INSERTMASKT");
	m_Insert.SetBitmap (HbInsert);

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_A,EDIT,       (double *) &A_bas.a_bas.a, VDOUBLE, 13, 0));
    Form.Add (new CUniFormField (&m_A_bz1,EDIT,   (char *) A_bas.a_bas.a_bz1, VCHAR));
    Form.Add (new CUniFormField (&m_A_bz2,EDIT,   (char *) A_bas.a_bas.a_bz2, VCHAR));

	Ipr.sqlin ((short *)  &Ipr.ipr.mdn, SQLSHORT, 0);
	Ipr.sqlin ((double *) &Ipr.ipr.a,  SQLDOUBLE, 0);
	Ipr.sqlout ((long *)  &Ipr.ipr.pr_gr_stuf, SQLLONG, 0);
	Ipr.sqlout ((long *)  &Ipr.ipr.kun_pr, SQLLONG, 0);
	Ipr.sqlout ((long *)  &Ipr.ipr.kun, SQLLONG, 0);
	IprCursor = Ipr.sqlcursor (_T("select pr_gr_stuf, kun_pr, kun from ipr ")
		                       _T("where mdn = ? ")
		                       _T("and a = ? ")
		                       _T("order by pr_gr_stuf, kun_pr, kun"));
	IprGrStufk.sqlin ((short *)  &IprGrStufk.iprgrstufk.mdn, SQLSHORT, 0);
	IprGrStufk.sqlout ((long *)  &IprGrStufk.iprgrstufk.pr_gr_stuf, SQLLONG, 0);
	IprGrStufkCursor = IprGrStufk.sqlcursor (_T("select pr_gr_stuf from iprgrstufk ")
		                                     _T("where mdn = ? ") 
											 _T("and pr_gr_stuf >= 0"));  
	I_kun_prk.sqlin ((short *)  &I_kun_prk.i_kun_prk.mdn, SQLSHORT, 0);
	I_kun_prk.sqlout ((long *)  &I_kun_prk.i_kun_prk.kun_pr, SQLLONG, 0);
	IKunPrkCursor = I_kun_prk.sqlcursor (_T("select kun_pr from i_kun_prk ")
		                                 _T("where mdn = ? ")
										 _T("and kun_pr >= 0"));  


	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_List.SetImageList (&image, LVSIL_SMALL);   
	}

	FillList = m_List;
	FillList.SetStyle (LVS_REPORT);
	if (m_List.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Preigruppenstufe"), 1, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("Kundenpreisliste"), 2, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("Kunde"), 3, 95, LVCFMT_RIGHT);
	FillList.SetCol (_T("Name"), 4, 120, LVCFMT_LEFT);
	FillList.SetCol (_T("VK-Preis"), 5, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Laden-Preis"), 6, 100, LVCFMT_RIGHT);
	if (m_List.Aufschlag == m_List.LIST ||
		m_List.Aufschlag == m_List.ALL)
	{
		FillList.SetCol (_T("Aufschlag"),  m_List.PosEkAbs, 100, LVCFMT_RIGHT);
		FillList.SetCol (_T("Aufschlag %"),m_List.PosEkProz, 100, LVCFMT_RIGHT);
	}

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	AGrid.Create (this, 1, 2);
    AGrid.SetBorder (0, 0);
    AGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 0, 0, 1, 1);
	AGrid.Add (c_A);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);

	ButtonGrid.Create (this, 5, 5);
    ButtonGrid.SetBorder (0, 0);
    ButtonGrid.SetCellHeight (20);
    ButtonGrid.SetGridSpace (0, 10);
	CCtrlInfo *c_Cancel = new CCtrlInfo (&m_Cancel, 0, 0, 1, 1);
	ButtonGrid.Add (c_Cancel);
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 1, 1, 1);
	ButtonGrid.Add (c_Save);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ButtonGrid.Add (c_Delete);
	CCtrlInfo *c_Insert = new CCtrlInfo (&m_Insert, 0, 3, 1, 1);
	ButtonGrid.Add (c_Insert);

	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_LA_bz1  = new CCtrlInfo (&m_LA_bz1, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LA_bz1);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 1, 2, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 1, 3, 3, 1); 
	CtrlGrid.Add (c_A_bz2);
	CCtrlInfo *c_ButtonGrid = new CCtrlInfo (&ButtonGrid, DOCKRIGHT, 4, 1, 3); 
	CtrlGrid.Add (c_ButtonGrid);
	CCtrlInfo *c_List  = new CCtrlInfo (&m_List, 0, 4, DOCKRIGHT, DOCKBOTTOM); 
//	c_List->rightspace = 125;
	c_List->rightspace = RightListSpace;
	CtrlGrid.Add (c_List);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();
	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
	Form.Show ();
	ReadMdn ();
	if (RemoveKun)
	{
		m_List.DeleteColumn (m_List.PosKun);
		m_List.ScrollPositions (2);
		m_List.DeleteColumn (m_List.PosKunName);
		m_List.ScrollPositions (3);
	}
	EnableHeadControls (TRUE);
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}


void CPrArtPage::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CPrArtPage::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CPrArtPage::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CPrArtPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CPrArtPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_List.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_List &&
					GetFocus ()->GetParent () != &m_List )
				{

					break;
			    }
				m_List.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnAchoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_A)
				{
					OnAchoice ();
					return TRUE;
				}
				m_List.OnKey9 ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CPrArtPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_A)
	{
		if (!Read ())
		{
			m_A.SetFocus ();
			return FALSE;
		}
	}

	if (Control != &m_List.ListEdit &&
		Control != &m_List.ListComboBox &&
		Control != &m_List.SearchListCtrl.Edit)
{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CPrArtPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_List &&
		Control->GetParent ()!= &m_List )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_List.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}


BOOL CPrArtPage::ReadMdn ()
{
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		FillPrGrStufCombo ();
		FillKunPrCombo ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CPrArtPage::read ()
{
	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}
	return Read ();

}

BOOL CPrArtPage::Read ()
{
	if (ModalChoice)
	{
		CString cA;
		m_A.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnAchoice ();
			SearchA = "";
			if (!AChoiceStat)
			{
				m_A.SetFocus ();
				m_A.SetSel (0, -1);
				return FALSE;
			}
		}
	}

	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	Form.Get ();
	if (A_bas.dbreadfirst () == 0)
	{
	    EnableHeadControls (FALSE);
		ReadList ();
		Form.Show ();
		m_A.SetFocus ();
		m_A.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Artikel %.0lf nicht gefunden"),A_bas.a_bas.a);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);

		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		Form.Show ();
		m_A.SetFocus ();
		m_A.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CPrArtPage::ReadList ()
{
	m_List.StopEnter ();
	m_List.DeleteAllItems ();
	m_List.vSelect.clear ();
	m_List.ListRows.Init ();
	int i = 0;
	memcpy (&Ipr.ipr, &ipr_null, sizeof (IPR));
	Ipr.ipr.mdn = Mdn.mdn.mdn;
	Ipr.ipr.a   = A_bas.a_bas.a;
	Ipr.sqlopen (IprCursor);
	while (Ipr.sqlfetch (IprCursor) == 0)
	{
		Ipr.dbreadfirst ();
		memcpy (&IprGrStufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
		memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk_null, sizeof (I_KUN_PRK));
		memcpy (&Kun.kun, &kun_null, sizeof (KUN));
		memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
		IprGrStufk.iprgrstufk.mdn = Ipr.ipr.mdn;
		IprGrStufk.iprgrstufk.pr_gr_stuf = Ipr.ipr.pr_gr_stuf;
		IprGrStufk.dbreadfirst ();
		I_kun_prk.i_kun_prk.mdn = Ipr.ipr.mdn;
		I_kun_prk.i_kun_prk.kun_pr = Ipr.ipr.kun_pr;
		I_kun_prk.dbreadfirst ();
		Kun.kun.mdn = Ipr.ipr.mdn;
		Kun.kun.kun = Ipr.ipr.kun;
		if (Kun.dbreadfirst () == 0)
		{
			KunAdr.adr.adr = Kun.kun.adr1;
			KunAdr.dbreadfirst ();
		}
		if (RemoveKun && Ipr.ipr.kun > 0l) continue;
		FillList.InsertItem (i, 0);
		CString PrGrStuf;
		PrGrStuf.Format (_T("%ld  %s"), Ipr.ipr.pr_gr_stuf,
							IprGrStufk.iprgrstufk.zus_bz);
		FillList.SetItemText (PrGrStuf.GetBuffer (), i, m_List.PosPrGrStuf);
		CString KunPr;
		KunPr.Format (_T("%ld  %s"), Ipr.ipr.kun_pr,
			I_kun_prk.i_kun_prk.zus_bz);
		FillList.SetItemText (KunPr.GetBuffer (), i, m_List.PosKunPr);
		CString Kun;
//		Kun.Format (_T("%ld  %s"), Ipr.ipr.kun, KunAdr.adr.adr_krz);
		if (!RemoveKun)
		{
			Kun.Format (_T("%ld"), Ipr.ipr.kun);
			FillList.SetItemText (Kun.GetBuffer (), i, m_List.PosKun);
			CString KunName;
			KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
			FillList.SetItemText (KunName.GetBuffer (), i, m_List.PosKunName);
		}

		CString VkPr;
		m_List.DoubleToString (Ipr.ipr.vk_pr_eu, VkPr, 4);
		FillList.SetItemText (VkPr.GetBuffer (), i, m_List.PosVkPr);
		CString LdPr;
		m_List.DoubleToString (Ipr.ipr.ld_pr_eu, LdPr, 2);
		FillList.SetItemText (LdPr.GetBuffer (), i, m_List.PosLdPr);

		CString EkAbs;
		m_List.DoubleToString (Ipr.ipr.add_ek_abs, EkAbs, 2);
		FillList.SetItemText (EkAbs.GetBuffer (), i, m_List.PosEkAbs);
		CString EkProz;
		m_List.DoubleToString (Ipr.ipr.add_ek_proz, EkProz, 2);
		FillList.SetItemText (EkProz.GetBuffer (), i, m_List.PosEkProz);

		CPreise *ipr = new CPreise (VkPr, LdPr, Ipr.ipr);
		DbRows.Add (ipr);
		m_List.ListRows.Add (ipr);
		i ++;
	}
	return TRUE;
}

BOOL CPrArtPage::IsChanged (CPreise *pIpr)
{
	DbRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ipr.ipr, &pIpr->ipr, sizeof (IPR));
		if (Ipr == ipr->ipr) break;
	} 	
	if (ipr == NULL)
	{
		return TRUE;
	}
	if (pIpr->cEk != ipr->cEk) return TRUE;
	if (pIpr->cVk != ipr->cVk) return TRUE;
	return FALSE;
}

BOOL CPrArtPage::InList (IPR_CLASS& Ipr)
{
	ListRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Ipr == ipr->ipr) return TRUE;
	}
    return FALSE;
}

void CPrArtPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ipr.ipr, &ipr->ipr, sizeof (IPR));
		if (!InList (Ipr))
		{
			Ipr.dbdelete ();
			PgrProt.Write (1);
		}
	}
}

BOOL CPrArtPage::Write ()
{
	extern short sql_mode;
	short sql_s;
	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	Ipr.beginwork ();
	m_List.StopEnter ();
	int count = m_List.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 IPR *ipr = new IPR;
		 memcpy (ipr, &ipr_null, sizeof (IPR));
         CString Text;
		 Text = m_List.GetItemText (i, m_List.PosPrGrStuf);
		 ipr->pr_gr_stuf = atol (Text);
		 Text = m_List.GetItemText (i, m_List.PosKunPr);
		 ipr->kun_pr = atol (Text);
		 if (m_List.PosKun != -1)
		 {
			Text = m_List.GetItemText (i, m_List.PosKun);
			ipr->kun = atol (Text);
		 }
     
     	 CString VkPr =  m_List.GetItemText (i, m_List.PosVkPr);
		 ipr->vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
		 ipr->vk_pr_i = ipr->vk_pr_eu;
		 CString LdPr =  m_List.GetItemText (i, m_List.PosLdPr);
		 ipr->ld_pr_eu = CStrFuncs::StrToDouble (LdPr);
		 ipr->ld_pr = ipr->ld_pr_eu;
		 ipr->mdn = Mdn.mdn.mdn; 
		 ipr->a = A_bas.a_bas.a; 
		 if (m_List.Aufschlag == m_List.LIST ||
			 m_List.Aufschlag == m_List.ALL)
		 {
			CString EkAbs =  m_List.GetItemText (i, m_List.PosEkAbs);
			ipr->add_ek_abs = CStrFuncs::StrToDouble (EkAbs);
			CString EkProz =  m_List.GetItemText (i, m_List.PosEkProz);
			ipr->add_ek_proz = CStrFuncs::StrToDouble (EkProz);
		 }
		 else
		 {
			 CPreise *lpr = (CPreise *) m_List.ListRows.Get (i);
			 ipr->add_ek_abs = lpr->ipr.add_ek_abs;
			 ipr->add_ek_proz = lpr->ipr.add_ek_proz;
		 }

		 if (!TestDecValues (ipr)) 
		 {
			 MessageBox (_T("Maximalwert f�r Preisfeld �berschritten"));
			 return FALSE;
		 }

		 CPreise *pr = new CPreise (VkPr, LdPr, *ipr);
		 if (ipr->vk_pr_eu != 0.0 || 
			 ipr->ld_pr_eu != 0.0)
		 {
				ListRows.Add (pr);
		 }
		 delete ipr;
	}

	DeleteDbRows ();

	ListRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&Ipr.ipr, &ipr->ipr, sizeof (IPR));
		Ipr.dbupdate ();
		if (IsChanged (ipr))
		{
			PgrProt.Write ();
		}
	}
	EnableHeadControls (TRUE);
	m_A.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	Ipr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

BOOL CPrArtPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_A.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Alle Eintr�ge l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	Ipr.beginwork ();
	m_List.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	m_List.DeleteAllItems ();

	EnableHeadControls (TRUE);
	m_A.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	Ipr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

void CPrArtPage::OnAchoice ()
{
    AChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceA (this);
	    Choice->IsModal = ModalChoice;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->SearchText = SearchA;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

/*
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		cy -= 100;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
*/
		Choice->MoveWindow (&rect);
		Choice->SetFocus ();

		return;
	}
    if (Choice->GetState ())
    {
		  CABasList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_A.EnableWindow (TRUE);
		  m_A.SetSel (0, -1, TRUE);
		  m_A.SetFocus ();
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
	 	  AChoiceStat = FALSE;	
	}
}

void CPrArtPage::OnASelected ()
{
	if (Choice == NULL) return;
    CABasList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    A_bas.a_bas.a = abl->a;
    if (A_bas.dbreadfirst () == 0)
    {
		m_A.EnableWindow (TRUE);
		m_A.SetFocus ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	if (CloseChoice)
	{
		OnACanceled (); 
	}
    Form.Show ();
	if (Choice->FocusBack)
	{
		Read ();
		m_List.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CPrArtPage::OnACanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CPrArtPage::StepBack ()
{
	if (m_A.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		m_List.StopEnter ();
		EnableHeadControls (TRUE);
		m_A.SetFocus ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
		m_List.DeleteAllItems ();
	}
	return TRUE;
}

void CPrArtPage::OnCancel ()
{
	StepBack ();
}

void CPrArtPage::OnSave ()
{
	Write ();
}

void CPrArtPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CPrArtPage::FillPrGrStufCombo ()
{
	CVector Values;
	Values.Init ();
	m_List.m_Mdn = Mdn.mdn.mdn;
	IprGrStufk.iprgrstufk.mdn = Mdn.mdn.mdn;
    IprGrStufk.sqlopen (IprGrStufkCursor);
	while (IprGrStufk.sqlfetch (IprGrStufkCursor) == 0)
	{
		IprGrStufk.dbreadfirst ();
		CString *Value = new CString ();
		Value->Format (_T("%ld  %s"), IprGrStufk.iprgrstufk.pr_gr_stuf,
			                          IprGrStufk.iprgrstufk.zus_bz);
		Values.Add (Value);
	}
	m_List.FillPrGrStufCombo (Values);
}

void CPrArtPage::FillKunPrCombo ()
{
	CVector Values;
	Values.Init ();
	I_kun_prk.i_kun_prk.mdn = Mdn.mdn.mdn;
    I_kun_prk.sqlopen (IKunPrkCursor);
	while (I_kun_prk.sqlfetch (IKunPrkCursor) == 0)
	{
		I_kun_prk.dbreadfirst ();
		CString *Value = new CString ();
		Value->Format (_T("%ld  %s"), I_kun_prk.i_kun_prk.kun_pr,
			                          I_kun_prk.i_kun_prk.zus_bz);
		Values.Add (Value);
	}
	m_List.FillKunPrCombo (Values);
}

void CPrArtPage::OnDelete ()
{
	m_List.DeleteRow ();
}

void CPrArtPage::OnInsert ()
{
	m_List.InsertRow ();
}

/*
BOOL CPrArtPage::Print ()
{
	CProcess print;
	CString Command = "70001 11112";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}
*/

BOOL CPrArtPage::Print ()
{
	CProcess print;
	Form.Get ();
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11112.llf", tmp);
	}
	else
	{
		dName = "11112.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11112\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "a %.0lf %.0lf\n", A_bas.a_bas.a,
			                        A_bas.a_bas.a);
		fclose (fp);
		Command.Format ("dr70001 -name 11112 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11112";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CPrArtPage::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_List.StartPauseEnter ();
}

void CPrArtPage::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_List.EndPauseEnter ();
}

void CPrArtPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Enable (enable);
	PosControls.Enable (!enable);
}

void CPrArtPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

void CPrArtPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_List.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("RemoveKun", cfg_v) == TRUE)
    {
			RemoveKun = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_List.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_List.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_List.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("Aufschlag", cfg_v) == TRUE)
    {
			m_List.Aufschlag = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CPrArtPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_List.ListEdit ||
        Control == &m_List.ListComboBox ||
		Control == &m_List.SearchListCtrl.Edit)	
	{
		ListCopy ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CPrArtPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CPrArtPage::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_List.ListEdit.m_hWnd) ||
		IsWindow (m_List.ListComboBox) ||
		IsWindow (m_List.SearchListCtrl.Edit))
	{
		m_List.StopEnter ();
		m_List.StartEnter (m_List.EditCol, m_List.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
//		for (int i = 0; i < MAXLISTROWS && i < m_List.GetItemCount (); i ++)
		for (int i = 0; i < m_List.GetItemCount (); i ++)
		{
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = "";
			int cols = m_List.GetHeaderCtrl ()->GetItemCount ();
			for (int j = 0; j < cols; j ++)
			{
				if (Row != "")
				{
					Row += Separator;
				}
				CString Field = m_List.GetItemText (i, j);
				Field.TrimRight ();
				Row += Field; 
			}
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

BOOL CPrArtPage::TestDecValues (IPR *ipr)
{
	if (ipr->vk_pr_eu > 9999.9999 ||
		ipr->vk_pr_eu < -9999.9999)
	{
		return FALSE;
	}

	if (ipr->ld_pr_eu > 9999.99 ||
		ipr->ld_pr_eu < -9999.99)
	{
		return FALSE;
	}
    
	if (ipr->add_ek_proz > 9999.99 ||
		ipr->add_ek_proz < -9999.99)
	{
		return false;
	}

	if (ipr->add_ek_abs > 9999.99 ||
		ipr->add_ek_abs < -9999.99)
	{
		return false;
	}
	return TRUE;
}