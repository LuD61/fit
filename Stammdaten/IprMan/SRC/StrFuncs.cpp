#include "StdAfx.h"
#include "strfuncs.h"

CString CStrFuncs::Format = _T("%.4lf");

CStrFuncs::CStrFuncs(void)
{
}

CStrFuncs::~CStrFuncs(void)
{
}

void CStrFuncs::Round (double d, CString& Value)
{
	Value.Format (Format.GetBuffer (), d);
	_tstof (Value.GetBuffer ());
}

void CStrFuncs::Round (double d, int scale, CString& Value)
{
	CString Format;
	Format.Format (_T("%c.%dlf"), '%', scale);
	Value.Format (Format.GetBuffer (), d);
}

BOOL CStrFuncs::IsDoubleEqual (double d1, double d2)
{
	CString D1;
	CString D2;

	Round (d1, D1);
	Round (d2, D2);
	D1.Trim ();
	D2.Trim ();
	if (D1 == D2)
	{
		return TRUE;
	}
	return FALSE;
}

double CStrFuncs::StrToDouble (LPTSTR string)
{
 double fl;
 double nk;
 double ziffer;
 double teiler;
 short minus;

 if (string == NULL) return (double) 0.0;
 fl = 0.0;
 nk = 0.0;
 teiler = 10.0;
 minus = 1;
 while (*string < 0X30)
 {
  if (*string == 0)
    return (0.0);
  if (*string == '-')
    break;
  if (*string == '+')
    break;
  if (*string == '.')
    break;
  if (*string == ',')
    break;
  string ++;
 }

 if (*string == '-')
 {
  minus = -1;
  string ++;
 }
 else if (*string == '+')
 {
  string ++;
 }

 while (*string)
 {
  if (*string == '.')
  {
   break;
  }
  if (*string == ',')
  {
   break;
  }
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = (fl * teiler) + ziffer;
  string ++;
 }

 if (*string == '.');
 else if (*string == ',');
 else
 {
  fl *= minus;
  return (fl);
 }
 string ++;
 while (*string)
 {
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }

  ziffer = *string - 0X30;
  nk = (double) ((double) ziffer / teiler);
  fl = fl + nk;
  nk = 0.0;
  teiler *= 10.0;
  string ++;
 }
 fl += nk;
 return (fl);
}

double CStrFuncs::StrToDouble (CString& Text)
{
	return StrToDouble (Text.GetBuffer (0));
}

void CStrFuncs::SysDate (CString& Date)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 Date.Format (_T("%02d.%02d.%04d"), ltime->tm_mday,
                                    ltime->tm_mon + 1,
                                    ltime->tm_year + 1900);
}

void CStrFuncs::SysTime (CString& Time)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 Time.Format (_T("%02d:%02d:%02d"), ltime->tm_hour,
									ltime->tm_min,
									ltime->tm_sec);
}

void CStrFuncs::ToClipboard (CString& Text)
{
	CEdit clip;
	clip.Create (WS_CHILD, CRect (0, 0, 80, 10), NULL, 10001);
	clip.SetWindowText (Text.GetBuffer ());
	clip.SetSel (0, -1);
	clip.Copy ();
	clip.DestroyWindow ();
}

BOOL CStrFuncs::IsDecimal (CString& Text)
{
	Text.Trim ();
	TCHAR *p = Text.GetBuffer ();
	for (; *p != 0; p += 1)
	{
		if (*p == ' ') break;
		if (*p > (UCHAR) 64) return FALSE;
	}
	return TRUE;
}

void CStrFuncs::FromClipboard (CString& Text)
{
	CEdit clip;
	clip.Create (WS_CHILD, CRect (0, 0, 80, 10), NULL, 10001);
	clip.Paste ();
	clip.GetWindowText (Text);
	clip.DestroyWindow ();
}

void CStrFuncs::PointToKomma (CString& Value)
{
		int pos = Value.Find ('.');
		if (pos != -1)
		{
			Value.GetBuffer ()[pos] = ',';
		}
}

