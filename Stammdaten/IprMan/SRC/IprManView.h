// IprManView.h : Schnittstelle der Klasse CIprManView
//


#pragma once


class CIprManView : public CView
{
protected: // Nur aus Serialisierung erstellen
	CIprManView();
	DECLARE_DYNCREATE(CIprManView)

// Attribute
public:
	CIprManDoc* GetDocument() const;

// Operationen
public:

// �berschreibungen
	public:
	virtual void OnDraw(CDC* pDC);  // �berladen, um diese Ansicht darzustellen
virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~CIprManView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // Debugversion in IprManView.cpp
inline CIprManDoc* CIprManView::GetDocument() const
   { return reinterpret_cast<CIprManDoc*>(m_pDocument); }
#endif

