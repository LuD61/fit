#ifndef _I_KUN_PRK_DEF
#define _I_KUN_PRK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct I_KUN_PRK {
   short          mdn;
   long           kun_pr;
   char           zus_bz[25];
   long           pr_gr_stuf;
   DATE_STRUCT    gue_ab;
   short          delstatus;
   short          waehrung;
};
extern struct I_KUN_PRK i_kun_prk, i_kun_prk_null;

#line 8 "I_kun_prk.rh"

class I_KUN_PRK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               I_KUN_PRK i_kun_prk;
               I_KUN_PRK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
