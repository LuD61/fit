// AddEk.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "IprMan.h"
#include "AddEk.h"


// CAddEk-Dialogfeld

IMPLEMENT_DYNAMIC(CAddEk, CDialog)
CAddEk::CAddEk(CWnd* pParent /*=NULL*/)
	: CDialog(CAddEk::IDD, pParent)
{
	EkProz = "";
	EkAbs = "";
	Font = NULL;
	Parent = NULL;
}

CAddEk::~CAddEk()
{
}

void CAddEk::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EKPROZ, m_EkProz);
	DDX_Control(pDX, IDC_EKABS, m_EkAbs);
}


BEGIN_MESSAGE_MAP(CAddEk, CDialog)
END_MESSAGE_MAP()


// CAddEk-Meldungshandler

BOOL CAddEk::OnInitDialog ()
{
	CDialog::OnInitDialog ();
	if (Font != NULL)
	{
		SetFont (Font);
	}
	if (Parent != NULL)
	{
		CRect pRect;
		CRect rect;
		Parent->GetWindowRect (&pRect);
		GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		rect.top = pRect.top;
		rect.left = pRect.right - cx;
		rect.right = rect.left + cx;
		rect.bottom = rect.top + cy;
		MoveWindow (&rect);
	}
	m_EkProz.SetWindowText (EkProz);
	m_EkAbs.SetWindowText (EkAbs);
	return TRUE;
}

BOOL CAddEk::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
	}
    return CDialog::PreTranslateMessage(pMsg);
}

void CAddEk::OnOK ()
{
	m_EkProz.GetWindowText (EkProz);
	m_EkAbs.GetWindowText (EkAbs);
	CDialog::OnOK ();
}