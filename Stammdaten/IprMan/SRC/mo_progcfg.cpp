#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include "token.h"
#include "mo_progcfg.h"

static char rec [1024];
static char dm  [512];

PROG_CFG::~PROG_CFG ()
{
	     CloseCfg (); 
}

BOOL PROG_CFG::OpenCfg (void)
/**
CFG-Datei oeffnen.
**/
{
         char *etc;

         etc = getenv ("BWSETC");
         if (etc == NULL)
         {
                     etc = "c:\\user\\fit\\etc";
         }
 
         sprintf (dm, "%s\\%s.cfg", etc, progname);

         progfp = fopen (dm, "r");
         if (progfp == NULL) return FALSE;
         return TRUE;
}
           
void PROG_CFG::CloseCfg (void)
/**
CFG-Datei schliessen.
**/
{
         if (progfp)
         {
             fclose (progfp);
             progfp = NULL;
         }
}

BOOL PROG_CFG::GetCfgValue (char *progitem, char *progvalue)
/**
Wert fuer progitem holen.
**/
{
//         int anz;
         BOOL itOK; 
         int len;
		 CToken token;

         itOK = FALSE;
         if (progfp == NULL)
         {
                   if (OpenCfg () == FALSE) return FALSE;
         }

		 token.SetSep (" ");
         progvalue[0] = (char) 0;
		 fseek (progfp, 0l, 0);
         while (fgets (rec, 1023, progfp))
         {
                   if (rec[0] < (char) 0x30) continue;
				   token = rec;
                   if (token.GetAnzToken () < 2) continue;
				   char *wort1 = token.GetToken (0);
                   len = (int) max (strlen (progitem), (int) strlen (wort1));

                   if (strupcmp (wort1, progitem, len) == 0)
                   {
					          char *wort2 = token.GetToken (1); 
                              strcpy (progvalue, wort2);
							  clipped (progvalue);
                              itOK = TRUE; 
                              break;
                   }

         }
         return itOK;
}


         
BOOL PROG_CFG::ReadCfgItem (char *itname, char *def, char *text,
                            char **values, char *help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         char *s;
		 CToken token;

         if (progfp == NULL)
         {
                   if (OpenCfg () == FALSE) return FALSE;
         }

         while (s = fgets (rec, 1023, progfp))
         {
                   if (rec[0] >= (char) 0x30) break;
         }

         if (s == NULL) return FALSE;

		 token.SetSep (" ");
         token = rec;
		 anz = token.GetAnzToken ();
//         anz = wsplit (rec, " ");
         if (anz < 3) return FALSE;
         char *wort1 = token.GetToken (0);
         char *wort2 = token.GetToken (1);
         char *wort3 = token.GetToken (2);

         itname = (char *) malloc (strlen (wort1) + 10);
         if (itname == NULL) return FALSE;
         strcpy (itname, wort1);
         def = (char *) malloc (strlen (wort2) + 10);
         if (def == NULL) return FALSE;
         strcpy (def, wort2);
         text = (char *) malloc (strlen (wort3) + 10);
         if (text == NULL) return FALSE;
         strcpy (text, wort3);
         return ReadCfgValues (values, help);
}
         
BOOL PROG_CFG::ReadCfgValues (char **values, char *help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         int i;
         char *s;
         BOOL InValue;
		 CToken token;

         if (progfp == NULL)
         {
                   return FALSE;
         }

         i = 0;
         InValue = FALSE;
		 token.SetSep (" ");
         while (s = fgets (rec, 1023, progfp))
         {
                   cr_weg (rec);
                   if (rec[0] >= (char) 0x30) return FALSE;
				   token = rec;
				   anz = token.GetAnzToken ();
//                   anz = wsplit (rec, " ");
                   if (anz == 0) continue;
				   char *wort1 = token.GetToken (0);
                   if (strcmp (wort1, "$VALUES") == 0) 
                   {
                       InValue = TRUE;
                       continue;
                   }

                   if (strcmp (wort1, "$HELP")   == 0) 
                   {
                        return ReadCfgHelp (help);
                   }
                   if (strcmp (wort1, "$END")    == 0) return TRUE;
                   if (InValue)
                   {
                       values[i] = (char *) malloc (strlen (rec) + 10);
                       if (values[i] == NULL)
                       {
                           return FALSE;
                       }
                       strcpy (values[i], rec);
                       i ++;
                   }
         }
         return FALSE;
}
         
                          
BOOL PROG_CFG::ReadCfgHelp (char *help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         int i;
         char *s;
         BOOL InHelp;
		 CToken token;

         if (progfp == NULL)
         {
                   return FALSE;
         }

         i = 0;
         InHelp = FALSE;
		 token.SetSep (" ");
         while (s = fgets (rec, 1023, progfp))
         {
                   cr_weg (rec);
                   if (rec[0] >= (char) 0x30) return FALSE;
				   token = rec;
				   anz = token.GetAnzToken ();
//                   anz = wsplit (rec, " ");
                   if (anz == 0) continue;
				   char *wort1 = token.GetToken (0);
                   if (strcmp (wort1, "$HELP") == 0) 
                   {
                       InHelp = TRUE;
                       continue;
                   }

                   if (strcmp (wort1, "$END")    == 0) return TRUE;
                   if (InHelp)
                   {
                       help = (char *) malloc (strlen (rec) + 10);
                       if (help == NULL) return FALSE;
                       strcpy (help, rec);
                   }
         }
         return FALSE;
}
         
                               

BOOL PROG_CFG::GetGlobDefault (char *env, char *wert)
/**
Wert aus bws_default holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;
	    CToken token;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\bws_defa", etc);
        fp = fopen (buffer, "r");
        if (fp == NULL) return FALSE;

		token.SetSep (" ");
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
					 token = buffer;
					 anz = token.GetAnzToken ();

//                     anz = split (buffer);
                     if (anz < 2) continue;
					 char *wort1 = token.GetToken (0);
					 char *wort2 = token.GetToken (1);
                     if (strupcmp (wort1, env, (int) strlen (env)) == 0)
                     {
                                 strcpy (wert, wort2);
                                 fclose (fp);
                                 return TRUE;
                     }
         }
         fclose (fp);
         return (FALSE);
}

BOOL PROG_CFG::GetGroupDefault (char *env, char *wert)
/**
Wert aus fitgroup.def holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;
	    CToken token;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\fitgroup.def", etc);
        fp = fopen (buffer, "r");
        if (fp == NULL) return FALSE;

		token.SetSep (" ");
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
					 token = buffer;
					 anz = token.GetAnzToken ();
//                     anz = wsplit (buffer, " ");
                     if (anz < 2) continue;
					 char *wort1 = token.GetToken (0);
					 char *wort2 = token.GetToken (1);
                     if (strupcmp (wort1, env, (int) strlen (env)) == 0)
                     {
                                 strcpy (wert, wort2);
                                 fclose (fp);
                                 return TRUE;
                     }
         }
         fclose (fp);
         return (FALSE);
}

void PROG_CFG::clipped (char *str)
{
	UCHAR *p = (UCHAR *) str;
	p += strlen (str);
	for (; (p >= (UCHAR *) str) && (*p < 0x21) ; p -= 1);
	*(p + 1) = 0;
}

void PROG_CFG::cr_weg (char *str)
{
	UCHAR *p = (UCHAR *) str;
	p += strlen (str);
	for (; (p >= (UCHAR *) str) && (*p != 13) && (*p != 10); 
		 p -= 1);
	*p = 0;
	if (p == (UCHAR *) str) return;
	*p --;
	for (; (p >= (UCHAR *) str) && (*p != 13) && (*p != 10); 
		 p -= 1);
	*p = 0;
}

int PROG_CFG::strupcmp (char *str1, char *str2, int len)
{
	CString Str1 = str1;
	CString Str2 = str2;
	Str2 = Str2.Left (len);
	return (Str1.CompareNoCase (Str2));
}
