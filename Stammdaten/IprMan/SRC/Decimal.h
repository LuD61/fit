#pragma once

class CDecimal
{
public:
	CString sValue;
	double dValue;
	_int64 Scale;
	long Precision;
	int sCount;
	CString Format;
	CDecimal(void);
	CDecimal(double, int=4);
	CDecimal(CString&, int=4);
	~CDecimal(void);
    void StringToDec ();
	void ToScale (CString&);
};
