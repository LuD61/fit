// DialogTestDlg.h : Headerdatei
//

#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "CtrlGrid.h"
#include "PrArtListCtrl.h"
#include "PrArtPageConst.h"
#include "FillList.h"
#include "a_bas.h"
#include "mdn.h"
#include "adr.h"
#include "Ipr.h"
#include "PGrProt.h"
#include "Iprgrstufk.h"
#include "I_kun_prk.h"
#include "Kun.h"
#include "ChoiceA.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "FormTab.h"
#include "mo_progcfg.h"
#include "Controls.h"
#include "Preise.h"

#define IDC_ACHOICE 3000
#define IDC_MDNCHOICE 3001

// CPrArtPreise Dialogfeld
class CPrArtPage : public CDbPropertyPage
{
// Konstruktion
public:
	CPrArtPage(CWnd* pParent = NULL);	// Standardkonstruktor
	CPrArtPage(UINT nIDTemplate);	// Standardkonstruktor

	~CPrArtPage ();

// Dialogfelddaten
//	enum { IDD = IDD_DIALOGTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung
    virtual BOOL PreTranslateMessage(MSG* pMsg);


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public :
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	BOOL RemoveKun;
	int StartSize;

	CControls HeadControls;
	CControls PosControls;
	CControls ButtonControls;
	BOOL HideButtons;
	int RightListSpace;
	CVector DbRows;
	CVector ListRows;
	int IprCursor;
	int IprGrStufkCursor;
	int IKunPrkCursor;
	CWnd *Frame;
	CCtrlGrid CtrlGrid;
	CCtrlGrid AGrid;
	CCtrlGrid MdnGrid;
	CButton m_MdnChoice;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CNumEdit m_MdnName;
	CButton m_AChoice;
	CStatic m_LA;
	CNumEdit m_A;
	CEdit m_LA_bz1;
	CEdit m_A_bz1;
	CEdit m_A_bz2;
	CFillList FillList;
	CPrArtListCtrl m_List;
	CButton m_Cancel;
	CButton m_Save;
	CButton m_Delete;
	CButton m_Insert;
	int RighListSpace;
	CCtrlGrid ButtonGrid;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	A_BAS_CLASS A_bas;
	IPR_CLASS Ipr;
	CPGrProt PgrProt;
	IPRGRSTUFK_CLASS IprGrStufk;
	I_KUN_PRK_CLASS I_kun_prk;
	KUN_CLASS Kun;
	ADR_CLASS KunAdr;
	CFormTab Form;
	CChoiceA *Choice;
	BOOL ModalChoice;
	BOOL CloseChoice;
	BOOL AChoiceStat;
	CString SearchA;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceKun *ChoiceKun;
	BOOL ModalChoiceKun;
	CFont Font;
	CFont lFont;
	CString PersName;
	CString Separator;
    CImageList image; 
	int CellHeight;

	virtual void OnSize (UINT, int, int);
	void OnAchoice ();
    void OnASelected ();
    void OnACanceled ();
    void OnMdnchoice(); 
	virtual BOOL Read ();
	virtual BOOL read ();
	virtual BOOL Write ();
	virtual BOOL Print ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	virtual BOOL StepBack ();
	virtual void OnCancel ();
	virtual void OnSave ();
	BOOL ReadMdn ();
	BOOL ReadList ();
    void FillPrGrStufCombo ();
    void FillKunPrCombo ();
	virtual BOOL DeleteAll ();
	virtual void OnDelete ();
	virtual void OnInsert ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult);
    void EnableHeadControls (BOOL enable);
    void WriteRow (int row);
    void DestroyRows(CVector &Rows);
    void DestroyRowsEx(CVector &Rows);
    void DeleteDbRows ();
    BOOL IsChanged (CPreise *pIpr);
    BOOL InList (IPR_CLASS& Ipr);
	void ReadCfg ();
    void ListCopy ();
	BOOL TestDecValues (IPR *ipr);
};
