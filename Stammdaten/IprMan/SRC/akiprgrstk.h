#ifndef _A_AKIPRGRSTK_DEF
#define _A_AKIPRGRSTK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AKIPRGRSTK {
   short          mdn;
   short          aki_nr;
   TCHAR          zus_bz[25];
   long           pr_gr_stuf;
   DATE_STRUCT    aki_von;
   DATE_STRUCT    aki_bis;
   short          delstatus;
   short          waehrung;
};
extern struct AKIPRGRSTK akiprgrstk, akiprgrstk_null;

#line 8 "akiprgrstk.rh"

class AKIPRGRSTK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               AKIPRGRSTK akiprgrstk;
               AKIPRGRSTK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
