#include "StdAfx.h"
#include "AkiKunPr.h"

CAkiKunPr::CAkiKunPr(void)
{
	cEk = _T("0.0");
	cVk = _T("0.0");
	memcpy (&akikunprp, &akikunprp_null, sizeof (AKIKUNPRP));
}

CAkiKunPr::CAkiKunPr(CString& cEk, CString& cVk, AKIKUNPRP& akikunprp)
{
	this->cEk = cEk.Trim ();
	this->cVk = cVk.Trim ();
	memcpy (&this->akikunprp, &akikunprp, sizeof (AKIKUNPRP));
}

CAkiKunPr::~CAkiKunPr(void)
{
}
