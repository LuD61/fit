#include "stdafx.h"
#include "ipr.h"

struct IPR ipr, ipr_null;

void IPR_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &ipr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ipr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &ipr.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &ipr.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &ipr.kun, SQLLONG, 0);
    sqlout ((short *) &ipr.mdn,SQLSHORT,0);
    sqlout ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
    sqlout ((long *) &ipr.kun_pr,SQLLONG,0);
    sqlout ((double *) &ipr.a,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
    sqlout ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((short *) &ipr.aktion_nr,SQLSHORT,0);
    sqlout ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
    sqlout ((TCHAR *) ipr.modif,SQLCHAR,2);
    sqlout ((short *) &ipr.waehrung,SQLSHORT,0);
    sqlout ((long *) &ipr.kun,SQLLONG,0);
    sqlout ((double *) &ipr.a_grund,SQLDOUBLE,0);
    sqlout ((TCHAR *) ipr.kond_art,SQLCHAR,5);
    sqlout ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
    sqlout ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select ipr.mdn,  ")
_T("ipr.pr_gr_stuf,  ipr.kun_pr,  ipr.a,  ipr.vk_pr_i,  ipr.vk_pr_eu,  ")
_T("ipr.ld_pr,  ipr.ld_pr_eu,  ipr.aktion_nr,  ipr.a_akt_kz,  ipr.modif,  ")
_T("ipr.waehrung,  ipr.kun,  ipr.a_grund,  ipr.kond_art,  ipr.add_ek_proz,  ")
_T("ipr.add_ek_abs from ipr ")

#line 16 "ipr.rpp"
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?"));
    sqlin ((short *) &ipr.mdn,SQLSHORT,0);
    sqlin ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &ipr.kun_pr,SQLLONG,0);
    sqlin ((double *) &ipr.a,SQLDOUBLE,0);
    sqlin ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &ipr.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((short *) &ipr.aktion_nr,SQLSHORT,0);
    sqlin ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
    sqlin ((TCHAR *) ipr.modif,SQLCHAR,2);
    sqlin ((short *) &ipr.waehrung,SQLSHORT,0);
    sqlin ((long *) &ipr.kun,SQLLONG,0);
    sqlin ((double *) &ipr.a_grund,SQLDOUBLE,0);
    sqlin ((TCHAR *) ipr.kond_art,SQLCHAR,5);
    sqlin ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
    sqlin ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
            sqltext = _T("update ipr set ipr.mdn = ?,  ")
_T("ipr.pr_gr_stuf = ?,  ipr.kun_pr = ?,  ipr.a = ?,  ipr.vk_pr_i = ?,  ")
_T("ipr.vk_pr_eu = ?,  ipr.ld_pr = ?,  ipr.ld_pr_eu = ?,  ")
_T("ipr.aktion_nr = ?,  ipr.a_akt_kz = ?,  ipr.modif = ?,  ")
_T("ipr.waehrung = ?,  ipr.kun = ?,  ipr.a_grund = ?,  ipr.kond_art = ?,  ")
_T("ipr.add_ek_proz = ?,  ipr.add_ek_abs = ? ")

#line 22 "ipr.rpp"
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?");
            sqlin ((short *)   &ipr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ipr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &ipr.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &ipr.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &ipr.kun, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &ipr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ipr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &ipr.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &ipr.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &ipr.kun, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select a from ipr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?"));
            sqlin ((short *)   &ipr.mdn, SQLSHORT, 0);
            sqlin ((double *)  &ipr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &ipr.pr_gr_stuf, SQLLONG, 0);
            sqlin ((long *)    &ipr.kun_pr, SQLLONG, 0);
            sqlin ((long *)    &ipr.kun, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from ipr ")
                                  _T("where mdn = ? ")
				  _T("and a = ?")
				  _T("and pr_gr_stuf = ?")
				  _T("and kun_pr = ? ")
 				  _T("and kun = ?"));
    sqlin ((short *) &ipr.mdn,SQLSHORT,0);
    sqlin ((long *) &ipr.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &ipr.kun_pr,SQLLONG,0);
    sqlin ((double *) &ipr.a,SQLDOUBLE,0);
    sqlin ((double *) &ipr.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &ipr.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &ipr.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &ipr.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((short *) &ipr.aktion_nr,SQLSHORT,0);
    sqlin ((TCHAR *) ipr.a_akt_kz,SQLCHAR,2);
    sqlin ((TCHAR *) ipr.modif,SQLCHAR,2);
    sqlin ((short *) &ipr.waehrung,SQLSHORT,0);
    sqlin ((long *) &ipr.kun,SQLLONG,0);
    sqlin ((double *) &ipr.a_grund,SQLDOUBLE,0);
    sqlin ((TCHAR *) ipr.kond_art,SQLCHAR,5);
    sqlin ((double *) &ipr.add_ek_proz,SQLDOUBLE,0);
    sqlin ((double *) &ipr.add_ek_abs,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into ipr (mdn,  ")
_T("pr_gr_stuf,  kun_pr,  a,  vk_pr_i,  vk_pr_eu,  ld_pr,  ld_pr_eu,  aktion_nr,  ")
_T("a_akt_kz,  modif,  waehrung,  kun,  a_grund,  kond_art,  add_ek_proz,  add_ek_abs) ")

#line 57 "ipr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?)")); 

#line 59 "ipr.rpp"
}

BOOL IPR_CLASS::operator== (IPR& ipr)
{
            if (this->ipr.pr_gr_stuf != ipr.pr_gr_stuf) return FALSE;  
            if (this->ipr.kun_pr     != ipr.kun_pr) return FALSE;  
            if (this->ipr.kun        != ipr.kun) return FALSE;  
            if (this->ipr.a          != ipr.a) return FALSE;  
            return TRUE;
} 
