#include "stdafx.h"
#include "akikunlsp.h"

struct AKIKUNLSP akikunlsp, akikunlsp_null;

void AKIKUNLSP_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &akikunlsp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)   &akikunlsp.a,     SQLDOUBLE, 0);
    sqlout ((short *) &akikunlsp.mdn,SQLSHORT,0);
    sqlout ((short *) &akikunlsp.aki_nr,SQLSHORT,0);
    sqlout ((double *) &akikunlsp.a,SQLDOUBLE,0);
    sqlout ((double *) &akikunlsp.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &akikunlsp.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &akikunlsp.aki_pr,SQLDOUBLE,0);
    sqlout ((double *) &akikunlsp.aki_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &akikunlsp.me_min,SQLDOUBLE,0);
    sqlout ((double *) &akikunlsp.me_max,SQLDOUBLE,0);
    sqlout ((double *) &akikunlsp.me,SQLDOUBLE,0);
    sqlout ((short *) &akikunlsp.waehrung,SQLSHORT,0);
    sqlout ((double *) &akikunlsp.sa_rab,SQLDOUBLE,0);
    sqlout ((short *) &akikunlsp.sa_kz,SQLSHORT,0);
            cursor = sqlcursor (_T("select akikunlsp.mdn,  ")
_T("akikunlsp.aki_nr,  akikunlsp.a,  akikunlsp.ld_pr,  akikunlsp.ld_pr_eu,  ")
_T("akikunlsp.aki_pr,  akikunlsp.aki_pr_eu,  akikunlsp.me_min,  ")
_T("akikunlsp.me_max,  akikunlsp.me,  akikunlsp.waehrung,  ")
_T("akikunlsp.sa_rab,  akikunlsp.sa_kz from akikunlsp ")

#line 13 "akikunlsp.rpp"
                                  _T("where aki_nr = ? ")
			          _T("and a = ?"));
    sqlin ((short *) &akikunlsp.mdn,SQLSHORT,0);
    sqlin ((short *) &akikunlsp.aki_nr,SQLSHORT,0);
    sqlin ((double *) &akikunlsp.a,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.aki_pr,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.aki_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.me_min,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.me_max,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.me,SQLDOUBLE,0);
    sqlin ((short *) &akikunlsp.waehrung,SQLSHORT,0);
    sqlin ((double *) &akikunlsp.sa_rab,SQLDOUBLE,0);
    sqlin ((short *) &akikunlsp.sa_kz,SQLSHORT,0);
            sqltext = _T("update akikunlsp set ")
_T("akikunlsp.mdn = ?,  akikunlsp.aki_nr = ?,  akikunlsp.a = ?,  ")
_T("akikunlsp.ld_pr = ?,  akikunlsp.ld_pr_eu = ?,  ")
_T("akikunlsp.aki_pr = ?,  akikunlsp.aki_pr_eu = ?,  ")
_T("akikunlsp.me_min = ?,  akikunlsp.me_max = ?,  akikunlsp.me = ?,  ")
_T("akikunlsp.waehrung = ?,  akikunlsp.sa_rab = ?,  ")
_T("akikunlsp.sa_kz = ? ")

#line 16 "akikunlsp.rpp"
                                  _T("where aki_nr = ? ")
			          _T("and a = ?");
            sqlin ((short *)   &akikunlsp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)   &akikunlsp.a,     SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &akikunlsp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)   &akikunlsp.a,     SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select aki_nr from akikunlsp ")
                                  _T("where aki_nr = ? ")
			                       _T("and a = ?"));
            sqlin ((short *)   &akikunlsp.aki_nr, SQLSHORT, 0);
            sqlin ((double *)   &akikunlsp.a,     SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from akikunlsp ")
                                  _T("where aki_nr = ? ")
							      _T("and a = ?"));
    sqlin ((short *) &akikunlsp.mdn,SQLSHORT,0);
    sqlin ((short *) &akikunlsp.aki_nr,SQLSHORT,0);
    sqlin ((double *) &akikunlsp.a,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.aki_pr,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.aki_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.me_min,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.me_max,SQLDOUBLE,0);
    sqlin ((double *) &akikunlsp.me,SQLDOUBLE,0);
    sqlin ((short *) &akikunlsp.waehrung,SQLSHORT,0);
    sqlin ((double *) &akikunlsp.sa_rab,SQLDOUBLE,0);
    sqlin ((short *) &akikunlsp.sa_kz,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into akikunlsp (")
_T("mdn,  aki_nr,  a,  ld_pr,  ld_pr_eu,  aki_pr,  aki_pr_eu,  me_min,  me_max,  me,  waehrung,  ")
_T("sa_rab,  sa_kz) ")

#line 33 "akikunlsp.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?)")); 

#line 35 "akikunlsp.rpp"
}
