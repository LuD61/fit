#include "StdAfx.h"
#include "dbformview.h"

IMPLEMENT_DYNCREATE(DbFormView, CFormView)

DbFormView::DbFormView() : 
			CFormView ((UINT) 0)
{
}

DbFormView::DbFormView(UINT Id) : CFormView (Id)
{
}

DbFormView::~DbFormView(void)
{
}


DROPEFFECT DbFormView::OnDrop (CWnd* pWnd,  COleDataObject* pDataObject,
					                 	  DROPEFFECT dropDefault, DROPEFFECT dropList,
										  CPoint point)
{
	return DROPEFFECT_NONE;
}

void DbFormView::write ()
{
}

BOOL DbFormView::TextCent ()
{
	return TRUE;
}

BOOL DbFormView::TextLeft ()
{
	return TRUE;
}

BOOL DbFormView::TextRight ()
{
	return TRUE;
}
