#pragma once
#include <vector>
#include "editlistctrl.h"
#include "A_bas.h"
#include "Ipr.h"
#include "ChoiceA.h"
#include "Vector.h"
#include "Preise.h"

#define MAXLISTROWS 30

class CKunPrListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:
	enum LISTPOS
	{
		POSA     = 1,
		POSABZ1  = 2,
		POSVKPR  = 3,
		POSLDPR  = 4,
		POSEKABS  = 5,
		POSEKPROZ = 6,
		POSRABKZ  = 7,
		POSRAB = 8,
	};

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
		AKTION   = 2,
	};

	int PosA;
    int PosABz1;
	int PosVkPr;
	int PosLdPr;
	int PosEkAbs;
	int PosEkProz;
	int PosRabKz;
    int PosRab;

    int *Position[9];

	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	BOOL ActiveListRow;
	short m_Mdn;
	short mdn;
    long pr_gr_stuf;
    long kun_pr;
    long kun;
	int oldsel;
	std::vector<BOOL> vSelect;
	CChoiceA *ChoiceA;
	BOOL ModalChoiceA;
	BOOL AChoiceStat;
	CVector ListRows;

	A_BAS_CLASS A_bas;
	IPR_CLASS Ipr;
	CKunPrListCtrl(void);
	~CKunPrListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	void OnAChoice (CString&);
	void OnChoice ();
	void OnKey9 ();
    void ReadABz1 ();
    void GetColValue (int row, int col, CString& Text);
    void TestIprIndex ();
	void ScrollPositions (int pos);
	void DestroyRows(CVector &Rows);
	BOOL LastCol ();
};
