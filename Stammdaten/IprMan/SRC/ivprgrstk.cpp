#include "stdafx.h"
#include "ivprgrstk.h"

struct IVPRGRSTK ivprgrstk, ivprgrstk_null;

void IVPRGRSTK_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &ivprgrstk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &ivprgrstk.pr_gr_stuf, SQLLONG, 0);
    sqlout ((short *) &ivprgrstk.mdn,SQLSHORT,0);
    sqlout ((long *) &ivprgrstk.pr_gr_stuf,SQLLONG,0);
    sqlout ((TCHAR *) ivprgrstk.zus_bz,SQLCHAR,25);
    sqlout ((DATE_STRUCT *) &ivprgrstk.gue_ab,SQLDATE,0);
    sqlout ((TCHAR *) ivprgrstk.ueb_kz,SQLCHAR,2);
    sqlout ((short *) &ivprgrstk.delstatus,SQLSHORT,0);
    sqlout ((short *) &ivprgrstk.waehrung,SQLSHORT,0);
            cursor = sqlcursor (_T("select ivprgrstk.mdn,  ")
_T("ivprgrstk.pr_gr_stuf,  ivprgrstk.zus_bz,  ivprgrstk.gue_ab,  ")
_T("ivprgrstk.ueb_kz,  ivprgrstk.delstatus,  ivprgrstk.waehrung from ivprgrstk ")

#line 13 "ivprgrstk.rpp"
                                  _T("where mdn = ? ")
				  _T("and pr_gr_stuf = ?"));
    sqlin ((short *) &ivprgrstk.mdn,SQLSHORT,0);
    sqlin ((long *) &ivprgrstk.pr_gr_stuf,SQLLONG,0);
    sqlin ((TCHAR *) ivprgrstk.zus_bz,SQLCHAR,25);
    sqlin ((DATE_STRUCT *) &ivprgrstk.gue_ab,SQLDATE,0);
    sqlin ((TCHAR *) ivprgrstk.ueb_kz,SQLCHAR,2);
    sqlin ((short *) &ivprgrstk.delstatus,SQLSHORT,0);
    sqlin ((short *) &ivprgrstk.waehrung,SQLSHORT,0);
            sqltext = _T("update ivprgrstk set ")
_T("ivprgrstk.mdn = ?,  ivprgrstk.pr_gr_stuf = ?,  ")
_T("ivprgrstk.zus_bz = ?,  ivprgrstk.gue_ab = ?,  ivprgrstk.ueb_kz = ?,  ")
_T("ivprgrstk.delstatus = ?,  ivprgrstk.waehrung = ? ")

#line 16 "ivprgrstk.rpp"
                                  _T("where mdn = ? ")
				  _T("and pr_gr_stuf = ?");
            sqlin ((short *)   &ivprgrstk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &ivprgrstk.pr_gr_stuf, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &ivprgrstk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &ivprgrstk.pr_gr_stuf, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select pr_gr_stuf from ivprgrstk ")
                                  _T("where mdn = ? ")
				  _T("and pr_gr_stuf = ?"));
            sqlin ((short *)   &ivprgrstk.mdn, SQLSHORT, 0);
            sqlin ((long *)    &ivprgrstk.pr_gr_stuf, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from ivprgrstk ")
                                  _T("where mdn = ? ")
				  _T("and pr_gr_stuf = ?"));
    sqlin ((short *) &ivprgrstk.mdn,SQLSHORT,0);
    sqlin ((long *) &ivprgrstk.pr_gr_stuf,SQLLONG,0);
    sqlin ((TCHAR *) ivprgrstk.zus_bz,SQLCHAR,25);
    sqlin ((DATE_STRUCT *) &ivprgrstk.gue_ab,SQLDATE,0);
    sqlin ((TCHAR *) ivprgrstk.ueb_kz,SQLCHAR,2);
    sqlin ((short *) &ivprgrstk.delstatus,SQLSHORT,0);
    sqlin ((short *) &ivprgrstk.waehrung,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into ivprgrstk (")
_T("mdn,  pr_gr_stuf,  zus_bz,  gue_ab,  ueb_kz,  delstatus,  waehrung) ")

#line 33 "ivprgrstk.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?)")); 

#line 35 "ivprgrstk.rpp"
}
