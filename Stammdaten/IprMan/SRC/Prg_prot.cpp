#include "stdafx.h"
#include "prg_prot.h"

struct PRG_PROT prg_prot, prg_prot_null;

void PRG_PROT_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &prg_prot.mdn, SQLSHORT, 0);
            sqlin ((double *)  &prg_prot.a, SQLDOUBLE, 0);
    sqlout ((long *) &prg_prot.propramm,SQLLONG,0);
    sqlout ((DATE_STRUCT *) &prg_prot.bearb,SQLDATE,0);
    sqlout ((char *) prg_prot.zeit,SQLCHAR,9);
    sqlout ((char *) prg_prot.pers_nam,SQLCHAR,9);
    sqlout ((short *) &prg_prot.mdn,SQLSHORT,0);
    sqlout ((long *) &prg_prot.pr_gr_stuf,SQLLONG,0);
    sqlout ((long *) &prg_prot.kun,SQLLONG,0);
    sqlout ((long *) &prg_prot.kun_pr,SQLLONG,0);
    sqlout ((double *) &prg_prot.a,SQLDOUBLE,0);
    sqlout ((double *) &prg_prot.vk_pr_i,SQLDOUBLE,0);
    sqlout ((double *) &prg_prot.vk_pr_eu,SQLDOUBLE,0);
    sqlout ((double *) &prg_prot.ld_pr,SQLDOUBLE,0);
    sqlout ((double *) &prg_prot.ld_pr_eu,SQLDOUBLE,0);
    sqlout ((short *) &prg_prot.aktion_nr,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &prg_prot.aki_von,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &prg_prot.aki_bis,SQLDATE,0);
    sqlout ((short *) &prg_prot.loesch,SQLSHORT,0);
    sqlout ((char *) prg_prot.kond_art,SQLCHAR,5);
    sqlout ((char *) prg_prot.tabelle,SQLCHAR,21);
            cursor = sqlcursor (_T("select prg_prot.propramm,  "
"prg_prot.bearb,  prg_prot.zeit,  prg_prot.pers_nam,  prg_prot.mdn,  "
"prg_prot.pr_gr_stuf,  prg_prot.kun,  prg_prot.kun_pr,  prg_prot.a,  "
"prg_prot.vk_pr_i,  prg_prot.vk_pr_eu,  prg_prot.ld_pr,  "
"prg_prot.ld_pr_eu,  prg_prot.aktion_nr,  prg_prot.aki_von,  "
"prg_prot.aki_bis,  prg_prot.loesch,  prg_prot.kond_art,  "
"prg_prot.tabelle from prg_prot ")

#line 13 "Prg_prot.rpp"
                                  _T("where mdn = ? ")
				  _T("and a = ?"));
    sqlin ((long *) &prg_prot.propramm,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &prg_prot.bearb,SQLDATE,0);
    sqlin ((char *) prg_prot.zeit,SQLCHAR,9);
    sqlin ((char *) prg_prot.pers_nam,SQLCHAR,9);
    sqlin ((short *) &prg_prot.mdn,SQLSHORT,0);
    sqlin ((long *) &prg_prot.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &prg_prot.kun,SQLLONG,0);
    sqlin ((long *) &prg_prot.kun_pr,SQLLONG,0);
    sqlin ((double *) &prg_prot.a,SQLDOUBLE,0);
    sqlin ((double *) &prg_prot.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &prg_prot.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &prg_prot.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &prg_prot.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((short *) &prg_prot.aktion_nr,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &prg_prot.aki_von,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &prg_prot.aki_bis,SQLDATE,0);
    sqlin ((short *) &prg_prot.loesch,SQLSHORT,0);
    sqlin ((char *) prg_prot.kond_art,SQLCHAR,5);
    sqlin ((char *) prg_prot.tabelle,SQLCHAR,21);
            sqltext = _T("update prg_prot set "
"prg_prot.propramm = ?,  prg_prot.bearb = ?,  prg_prot.zeit = ?,  "
"prg_prot.pers_nam = ?,  prg_prot.mdn = ?,  prg_prot.pr_gr_stuf = ?,  "
"prg_prot.kun = ?,  prg_prot.kun_pr = ?,  prg_prot.a = ?,  "
"prg_prot.vk_pr_i = ?,  prg_prot.vk_pr_eu = ?,  prg_prot.ld_pr = ?,  "
"prg_prot.ld_pr_eu = ?,  prg_prot.aktion_nr = ?,  "
"prg_prot.aki_von = ?,  prg_prot.aki_bis = ?,  prg_prot.loesch = ?,  "
"prg_prot.kond_art = ?,  prg_prot.tabelle = ? ")

#line 16 "Prg_prot.rpp"
                                  _T("where mdn = ? ")
				  _T("and a = ?");
            sqlin ((short *)   &prg_prot.mdn, SQLSHORT, 0);
            sqlin ((double *)  &prg_prot.a, SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &prg_prot.mdn, SQLSHORT, 0);
            sqlin ((double *)  &prg_prot.a, SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from prg_prot ")
                                  _T("where mdn = ? ")
				  _T("and a = ?"));
            sqlin ((short *)   &prg_prot.mdn, SQLSHORT, 0);
            sqlin ((double *)  &prg_prot.a, SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from prg_prot ")
                                  _T("where mdn = ? ")
				  _T("and a = ?"));
    sqlin ((long *) &prg_prot.propramm,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &prg_prot.bearb,SQLDATE,0);
    sqlin ((char *) prg_prot.zeit,SQLCHAR,9);
    sqlin ((char *) prg_prot.pers_nam,SQLCHAR,9);
    sqlin ((short *) &prg_prot.mdn,SQLSHORT,0);
    sqlin ((long *) &prg_prot.pr_gr_stuf,SQLLONG,0);
    sqlin ((long *) &prg_prot.kun,SQLLONG,0);
    sqlin ((long *) &prg_prot.kun_pr,SQLLONG,0);
    sqlin ((double *) &prg_prot.a,SQLDOUBLE,0);
    sqlin ((double *) &prg_prot.vk_pr_i,SQLDOUBLE,0);
    sqlin ((double *) &prg_prot.vk_pr_eu,SQLDOUBLE,0);
    sqlin ((double *) &prg_prot.ld_pr,SQLDOUBLE,0);
    sqlin ((double *) &prg_prot.ld_pr_eu,SQLDOUBLE,0);
    sqlin ((short *) &prg_prot.aktion_nr,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &prg_prot.aki_von,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &prg_prot.aki_bis,SQLDATE,0);
    sqlin ((short *) &prg_prot.loesch,SQLSHORT,0);
    sqlin ((char *) prg_prot.kond_art,SQLCHAR,5);
    sqlin ((char *) prg_prot.tabelle,SQLCHAR,21);
            ins_cursor = sqlcursor (_T("insert into prg_prot ("
"propramm,  bearb,  zeit,  pers_nam,  mdn,  pr_gr_stuf,  kun,  kun_pr,  a,  vk_pr_i,  "
"vk_pr_eu,  ld_pr,  ld_pr_eu,  aktion_nr,  aki_von,  aki_bis,  loesch,  kond_art,  "
"tabelle) ")

#line 33 "Prg_prot.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?)")); 

#line 35 "Prg_prot.rpp"
}

int PRG_PROT_CLASS::dbupdate ()
{
            if (ins_cursor == -1)
            {
                   prepare ();
            }  
	    return sqlexecute (ins_cursor);
}

BOOL PRG_PROT_CLASS::operator== (PRG_PROT& prg_prot)
{
            if (this->prg_prot.pr_gr_stuf != prg_prot.pr_gr_stuf) return FALSE;  
            if (this->prg_prot.kun_pr     != prg_prot.kun_pr) return FALSE;  
            if (this->prg_prot.kun        != prg_prot.kun) return FALSE;  
            return TRUE;
} 
