#ifndef _BMAP_DEF
#define _BMAP_DEF

class BMAP
{
          public :
              HGLOBAL hglbDlg;
              BITMAPINFO FAR *bmap;
              HBITMAP hBitmap;
              HBITMAP hBitmapOrg;
              HBITMAP hBitmapZoom;
              CBitmap *cBitmap;
              CBitmap *cBitmapOrg;
              HWND bmphWnd;
              BMAP () : hBitmap (NULL), hBitmapZoom (NULL),
                        bmap (NULL)
              {
                  hglbDlg = NULL;
              }
         
			  void SetBitmap (HBITMAP hBitmap)
			  {
				  this-> hBitmap = hBitmap;
                  hBitmapOrg = hBitmap;
			  }

			  HBITMAP GetBitmap (void)
			  {
				  return hBitmap;
			  }

              CBitmap *ReadBitmap (HDC, char *);
              void DestroyBitmap (void);
              void DrawBitmap (CDC *, int, int);
              void DrawBitmap (CDC *, HBITMAP , int , int);
              void DrawBitmap (HDC, int, int);
              void DrawBitmap (HDC, HBITMAP , int , int);
              void BitmapSize (HDC, HBITMAP, POINT *);
              void BitmapSize (CDC *, HBITMAP, POINT *);
              static HBITMAP PrintBitmapMem (HBITMAP, HBITMAP, COLORREF);
              static HBITMAP LoadBitmap (HINSTANCE, char *, char *);
              static HBITMAP LoadBitmap (HINSTANCE, char *, char *, COLORREF);
};


#endif