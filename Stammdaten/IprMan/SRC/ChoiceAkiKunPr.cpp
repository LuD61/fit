#include "stdafx.h"
#include "ChoiceAkiKunPr.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceAkiKunPr::Sort1 = -1;
int CChoiceAkiKunPr::Sort2 = -1;
int CChoiceAkiKunPr::Sort3 = -1;
int CChoiceAkiKunPr::Sort4 = -1;

CChoiceAkiKunPr::CChoiceAkiKunPr(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_Mdn = 0;
	Rows = NULL;
}

CChoiceAkiKunPr::~CChoiceAkiKunPr() 
{
	DestroyList ();
}

void CChoiceAkiKunPr::DestroyList() 
{
	for (std::vector<CAkiKunPrList *>::iterator pabl = AkiKunPrList.begin (); pabl != AkiKunPrList.end (); ++pabl)
	{
		CAkiKunPrList *abl = *pabl;
		delete abl;
	}
    	AkiKunPrList.clear ();
}

void CChoiceAkiKunPr::FillList () 
{
    short  mdn = 1;
    long  aki_nr = 0;
	long kun = 0;
    TCHAR zus_bz [34];
	int cursor;

	DestroyList ();
	SelectedRow = NULL;
	CListCtrl *listView = GetListView ();
	listView->DeleteAllItems ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Kundenpreislisten-Aktionen"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Aktionsnummer"),      1, 80, LVCFMT_RIGHT);
    SetCol (_T("Kunde"),              2, 80, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),        3, 250);

	if (AkiKunPrList.size () == 0 && Rows == NULL)
	{
		if (m_Mdn != 0)
		{
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((long *)&aki_nr,     SQLLONG, 0);
			DbClass->sqlout ((long *)&kun,   SQLLONG, 0);
			DbClass->sqlout ((LPTSTR)  zus_bz, SQLCHAR, sizeof (zus_bz));
			DbClass->sqlin ((LPTSTR)  &m_Mdn,   SQLSHORT, 0);
			cursor = DbClass->sqlcursor (_T("select mdn, aki_nr, kun, zus_bz ")
				                             _T("from akikunprk where mdn = ? and kun >= 0 "));
		}
		else
		{
			DbClass->sqlout ((short *)&mdn,       SQLSHORT, 0);
			DbClass->sqlout ((long *)&aki_nr,     SQLLONG, 0);
			DbClass->sqlout ((long *)&kun,        SQLLONG, 0);
			DbClass->sqlout ((LPTSTR)  zus_bz, SQLCHAR, sizeof (zus_bz));
			cursor = DbClass->sqlcursor (_T("select mdn, aki_nr, kun, zus_bz ")
				                             _T("from akikunprk where kun >= 0 "));
		}
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) zus_bz;
			CDbUniCode::DbToUniCode (zus_bz, pos);
			CAkiKunPrList *abl = new CAkiKunPrList (mdn, aki_nr, kun, zus_bz);
			AkiKunPrList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}
	else if (AkiKunPrList.size () == 0 && Rows != NULL)
	{
		mdn = (short) m_Mdn;
		Rows->FirstPosition ();
		CString *c;
		while ((c = (CString *) Rows->GetNext ()) != NULL)
		{
			int pos = 0;
			CString Kun = c->Tokenize (" ", pos);
			if (Kun != "")
			{
				kun = atol (Kun.GetBuffer ());
                int len = Kun.GetLength ();
				CString ZusBz = Kun.Mid (len + 1, -1);
				ZusBz.Trim ();
				strcpy (zus_bz, ZusBz.GetBuffer ());
			    CAkiKunPrList *abl = new CAkiKunPrList (mdn, aki_nr,kun, zus_bz);
			    AkiKunPrList.push_back (abl);
			}
		}

	}

	for (std::vector<CAkiKunPrList *>::iterator pabl = AkiKunPrList.begin (); pabl != AkiKunPrList.end (); ++pabl)
	{
		CAkiKunPrList *abl = *pabl;
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString AkiNr;
		AkiNr.Format (_T("%ld"), abl->aki_nr); 
		CString Kun;
		Kun.Format (_T("%ld"), abl->kun); 
		CString Num;
		CString Bez;
		_tcscpy (zus_bz, abl->zus_bz.GetBuffer ());

		CString LText;
		LText.Format (_T("%ld %s"), abl->aki_nr, 
									abl->zus_bz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = AkiNr;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (Kun.GetBuffer (), i, 2);
        ret = SetItemText (zus_bz, i, 3);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceAkiKunPr::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAkiKunPr::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAkiKunPr::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAkiKunPr::SearchKunPr (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAkiKunPr::SearchZusBz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();
		CString cSearch = Search;
		cSearch.MakeUpper ();

        if (_tmemcmp (cSearch.GetBuffer (), iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAkiKunPr::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             SearchKunPr (ListBox, EditText.GetBuffer (8));
             break;
        case 3 :
             SearchZusBz (ListBox, EditText.GetBuffer (8));
             break;
    }
}

int CChoiceAkiKunPr::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceAkiKunPr::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort3;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort3);
	   }
	   return 0;
   }
   else if (SortRow == 3)
   {
    CString cItem1 = strItem1;
    CString cItem2 = strItem2;
	cItem1.MakeUpper ();
	cItem2.MakeUpper ();
//	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
	return (cItem2.CompareNoCase (cItem1)) * Sort4;
   }
   return 0;
}


void CChoiceAkiKunPr::Sort (CListCtrl *ListBox)
{
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
        case 3:
              Sort4 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CAkiKunPrList *abl = AkiKunPrList [i];
		   
		   abl->aki_nr     = (long) _tstol (ListBox->GetItemText (i, 1));
		   abl->kun        = _tstol (ListBox->GetItemText (i, 2));
		   abl->zus_bz     = ListBox->GetItemText (i, 3);
	}
}

void CChoiceAkiKunPr::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = AkiKunPrList [idx];
}

CAkiKunPrList *CChoiceAkiKunPr::GetSelectedText ()
{
	CAkiKunPrList *abl = (CAkiKunPrList *) SelectedRow;
	return abl;
}

