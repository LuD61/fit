#include "stdafx.h"
#include "bmap.h"


void BMAP::DestroyBitmap (void)
/**
Aktuelle Bitmap loeschen.
**/
{
       if (bmap)
       {
           GlobalFree (bmap);
           bmap = NULL;
           hglbDlg = NULL;
       }
}


CBitmap *BMAP::ReadBitmap (HDC hdc, char *bname)
/**
Graphic ausgeben.
**/
{
         FILE *bp;
         char far *rptr;
         DWORD bytes;
         unsigned rbytes;
         BITMAPFILEHEADER bmpheader;
         DWORD bmbitsoffs;
         const char far *bmbits;

         bp = fopen (bname, "rb");
         if (bp == (FILE *) 0)
         {
                     return NULL;
         }

         bytes = (DWORD) fread (&bmpheader, 1, sizeof (BITMAPFILEHEADER), bp);
         bmbitsoffs = bmpheader.bfOffBits - sizeof (BITMAPFILEHEADER);
         hglbDlg = GlobalAlloc (GMEM_FIXED, bmpheader.bfSize);
         if (hglbDlg == NULL)
         {
                     fclose (bp);
                     return NULL;
         }
         bmap = (BITMAPINFO FAR*) GlobalLock (hglbDlg);
         if (bmap == NULL)
         {
                     fclose (bp);
                     return NULL;
         }

         rptr = (char far *) bmap;
         rbytes = (unsigned) fread ((char *) rptr, 1, 0x1000, bp);
         while (rbytes == 0x1000)
         {
                 rptr += 0x1000;
                 rbytes = (unsigned) fread ((BYTE *) rptr, 1, 0x1000, bp);
         }

         bmbits = (const char far *) bmap + bmbitsoffs;
         hBitmap = CreateDIBitmap (hdc, (BITMAPINFOHEADER FAR *) bmap,
                                   CBM_INIT, bmbits, bmap, DIB_RGB_COLORS);
         fclose (bp);
         hBitmapOrg = hBitmap;
         cBitmap    = CBitmap::FromHandle (hBitmap);
         cBitmapOrg = CBitmap::FromHandle (hBitmap);
         return cBitmap;
}

void BMAP::DrawBitmap (HDC hdc, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void BMAP::DrawBitmap (CDC *pDC, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        HDC hdc = pDC->m_hDC;
        DrawBitmap (hdc, xStart, yStart);
}

void BMAP::DrawBitmap (HDC hdc, HBITMAP hBitmap, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
//        DWORD dwsize;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void BMAP::DrawBitmap (CDC *pDC, HBITMAP hBitmap, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        HDC hdc = pDC->m_hDC;
        DrawBitmap (hdc, hBitmap, xStart, yStart);
}

HBITMAP BMAP::PrintBitmapMem (HBITMAP hBitmapOrg, HBITMAP hMask, COLORREF BkColor)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
        HDC hdc;
		HBITMAP hBitmapMem;
		RECT rect;
		HBRUSH hBrush, oldbrush;

 
		hdc = GetDC (NULL);
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        SetViewportOrgEx (hdc, 0, 0, NULL);

		rect.left = 0;
		rect.top = 0;
		rect.right  = bm.bmWidth;
		rect.bottom = bm.bmHeight;
		hBrush = CreateSolidBrush (BkColor);

        hdcMemory  = CreateCompatibleDC(hdc);

        hBitmapMem = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);

        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapMem);
        oldbrush = (HBRUSH) SelectObject (hdcMemoryZ, hBrush);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                NULL, 0, 0, PATCOPY); 

        hbmOld = (HDC) SelectObject (hdcMemory, hMask);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCAND); 

        SelectObject (hdcMemory, hBitmapOrg);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCPAINT); 


        SelectObject (hdcMemory, hbmOld);

		DeleteObject (hBrush);
        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        ReleaseDC (NULL, hdc);

        DeleteObject (hBitmapOrg);
        DeleteObject (hMask);

        return hBitmapMem;
}

void BMAP::BitmapSize (HDC hdc, HBITMAP hBitmap, POINT *ptSize)
/**
Bitmap-Recheck holen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize->x = bm.bmWidth;
        ptSize->y = bm.bmHeight;
        DeleteDC (hdcMem); 
}

void BMAP::BitmapSize (CDC *pDC, HBITMAP hBitmap, POINT *ptSize)
/**
Bitmap-Recheck holen.
**/
{
        HDC hdc = pDC->m_hDC;
        BitmapSize (hdc, hBitmap, ptSize);
}
 

HBITMAP BMAP::LoadBitmap (HINSTANCE hInstance, char *BitmapName, char *MaskName)
{
	return PrintBitmapMem (::LoadBitmap (hInstance, BitmapName),
		                   ::LoadBitmap (hInstance, MaskName),
						   GetSysColor (COLOR_3DFACE));
}

HBITMAP BMAP::LoadBitmap (HINSTANCE hInstance, char *BitmapName, char *MaskName, COLORREF BkColor)
{
	return PrintBitmapMem (::LoadBitmap (hInstance, BitmapName),
		                   ::LoadBitmap (hInstance, MaskName),
						   BkColor);
}


