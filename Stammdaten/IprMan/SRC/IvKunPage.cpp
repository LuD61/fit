// PrArtPreise.cpp : Implementierungsdatei
//

#include "stdafx.h"
// #include "IPrDialog.h"
#include "IprMan.h"
#include "IvKunPage.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Decimal.h"
#include "Process.h"
#include "DbTime.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

// CIvKunPage Dialogfeld

CIvKunPage::CIvKunPage(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage()
{
	Choice = NULL;
	ModalChoice = TRUE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ChoiceKunPr = NULL;
	ChoicePrGrStuf = NULL;
	ModalChoiceMdn = TRUE;
    ModalChoicePrGrStuf = TRUE;
	IprCursor = -1;
	IprDelCursor = -1;
//	IprGrStufkCursor = -1;
	IKunPrkCursor = -1;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Kun);
	PosControls.Add (&m_KunPr);
	PosControls.Add (&m_KunZusBz);
	PosControls.Add (&m_PrGrStuf);
	PosControls.Add (&m_GueAb);
	PosControls.Add (&m_KunPrList);

	ButtonControls.Add (&m_Cancel);
	ButtonControls.Add (&m_Save);
	ButtonControls.Add (&m_Delete);
	ButtonControls.Add (&m_Insert);

    HideButtons = FALSE;

	Frame = NULL;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Search = _T("");
	Separator = _T(";");
	CellHeight = 0;
	Cfg.SetProgName( _T("IPrDialog"));
}

CIvKunPage::CIvKunPage(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ChoiceKunPr = NULL;
	ChoicePrGrStuf = NULL;
    ModalChoicePrGrStuf = TRUE;
	Frame = NULL;
	IprCursor = -1;
	IprDelCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
}

CIvKunPage::~CIvKunPage()
{

	Font.DeleteObject ();
	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	Ivpr.dbclose ();
	Iprgrstufk.dbclose ();
	Kun.dbclose ();
	KunAdr.dbclose ();
	if (IprCursor == -1)
	{
		Ivpr.sqlclose (IprCursor);
	}
	if (IprDelCursor == -1)
	{
		Ivpr.sqlclose (IprDelCursor);
	}
	if (IKunPrkCursor == -1)
	{
		A_bas.sqlclose (IKunPrkCursor);
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	if (ChoicePrGrStuf != NULL)
	{
		delete ChoicePrGrStuf;
		ChoicePrGrStuf = NULL;
	}
	if (ChoiceKunPr != NULL)
	{
		delete ChoiceKunPr;
		ChoiceKunPr = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);

}

void CIvKunPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_LKUN_KRZ1, m_LKunKrz1);
	DDX_Control(pDX, IDC_KUN_KRZ1, m_KunKrz1);
	DDX_Control(pDX, IDC_LKUN_PR, m_LKunPr);
	DDX_Control(pDX, IDC_KUN_PR, m_KunPr);
	DDX_Control(pDX, IDC_LKUN_ZUS_BZ, m_LKunZusBz);
	DDX_Control(pDX, IDC_KUN_ZUS_BZ, m_KunZusBz);

	DDX_Control(pDX, IDC_LPR_GR_STUF, m_LPrGrStuf);
	DDX_Control(pDX, IDC_PR_GR_STUF, m_PrGrStuf);
	DDX_Control(pDX, IDC_LZUS_BZ, m_LZusBz);
	DDX_Control(pDX, IDC_ZUS_BZ, m_ZusBz);
	DDX_Control(pDX, IDC_LGUE_AB, m_LGueAb);
	DDX_Control(pDX, IDC_GUE_AB, m_GueAb);


	DDX_Control(pDX, IDC_KUNPR_LIST, m_KunPrList);

	DDX_Control(pDX, IDC_CANCEL, m_Cancel);
	DDX_Control(pDX, IDC_SAVE, m_Save);
	DDX_Control(pDX, IDC_DELETE, m_Delete);
	DDX_Control(pDX, IDC_INSERT, m_Insert);

}

BEGIN_MESSAGE_MAP(CIvKunPage, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_SIZE ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_KUNCHOICE ,    OnKunchoice)
	ON_BN_CLICKED(IDC_KUNPRCHOICE ,  OnKunPrchoice)
	ON_BN_CLICKED(IDC_PRGRCHOICE ,  OnPrGrchoice)
	ON_BN_CLICKED(IDC_CANCEL ,   OnCancel)
	ON_BN_CLICKED(IDC_SAVE ,     OnSave)
	ON_BN_CLICKED(IDC_DELETE ,   OnDelete)
	ON_BN_CLICKED(IDC_INSERT ,   OnInsert)
	ON_COMMAND (SELECTED, OnKunSelected)
	ON_COMMAND (CANCELED, OnKunCanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
END_MESSAGE_MAP()


// CIvKunPage Meldungshandler

BOOL CIvKunPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
//	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
//	ASSERT(IDM_ABOUTBOX < 0xF000);

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	A_bas.opendbase (_T("bws"));

	CUtil::GetPersName (PersName);
//	PgrProt.Construct (PersName, CString ("11121"), &Ipr);

	ReadCfg ();

	if (HideButtons)
	{
		ButtonControls.SetVisible (FALSE);
		RightListSpace = 15;
	}
	else
	{
		ButtonControls.SetVisible (TRUE);
		RightListSpace = 125;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	m_Cancel.SetWindowText (_T("Beenden"));
    HBITMAP HbF5 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F5T", "F5MASKT");
	m_Cancel.SetBitmap (HbF5);

    HBITMAP HbF12 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F12T", "F12MASKT");
	m_Save.SetBitmap (HbF12);

    HBITMAP HbDel = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "DELT", "DELMASKT");
	m_Delete.SetBitmap (HbDel);

    HBITMAP HbInsert = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "INSERTT", "INSERTMASKT");
	m_Insert.SetBitmap (HbInsert);
    memcpy (&Ivpr.iv_pr, &iv_pr_null, sizeof (IV_PR));

    Form.Add (new CFormField (&m_Mdn,EDIT,         (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT,  (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Kun,EDIT,         (long *) &Kun.kun.kun, VLONG));
	Form.Add (new CUniFormField (&m_KunKrz1,EDIT,  (char *) Kun.kun.kun_krz1, VCHAR));
    Form.Add (new CFormField (&m_KunPr,EDIT,       (long *) &I_kun_prk.i_kun_prk.kun_pr, VLONG));
    Form.Add (new CUniFormField (&m_KunZusBz,EDIT, (char *) I_kun_prk.i_kun_prk.zus_bz, VCHAR));
    Form.Add (new CFormField (&m_PrGrStuf,EDIT,    (long *) &Iprgrstufk.iprgrstufk.pr_gr_stuf, VLONG));
    Form.Add (new CUniFormField (&m_ZusBz,EDIT,    (char *) Iprgrstufk.iprgrstufk.zus_bz, VCHAR));
    Form.Add (new CFormField (&m_GueAb,DATETIMEPICKER,  (DATE_STRUCT *) &Ivpr.iv_pr.gue_ab, VDATE));
    
	Ivpr.sqlin ((short *)  &Ivpr.iv_pr.mdn, SQLSHORT, 0);
	Ivpr.sqlin ((long *)  &Ivpr.iv_pr.pr_gr_stuf,  SQLLONG, 0);
	Ivpr.sqlin ((long *)  &Ivpr.iv_pr.kun_pr,  SQLLONG, 0);
	Ivpr.sqlin ((long *)  &Ivpr.iv_pr.kun,  SQLLONG, 0);
	Ivpr.sqlout ((double *) &Ivpr.iv_pr.a,  SQLDOUBLE, 0);
	IprCursor = Ivpr.sqlcursor (_T("select a from iv_pr ")
		                       _T("where mdn = ? ")
		                       _T("and pr_gr_stuf = ? ")
		                       _T("and kun_pr = ? ")
		                       _T("and kun = ? ")
		                       _T("order by a"));
	Ivpr.sqlin ((short *)  &Ivpr.iv_pr.mdn, SQLSHORT, 0);
	Ivpr.sqlin ((long *)  &Ivpr.iv_pr.pr_gr_stuf,  SQLLONG, 0);
	Ivpr.sqlin ((long *)  &Ivpr.iv_pr.kun_pr,  SQLLONG, 0);
	Ivpr.sqlin ((long *)  &Ivpr.iv_pr.kun,  SQLLONG, 0);
	IprDelCursor = Ivpr.sqlcursor (_T("delete from iv_pr ")
								  _T("where mdn = ? ")
		                       _T("and pr_gr_stuf = ? ")
		                       _T("and kun_pr = ? ")
		                       _T("and kun = ? "));
	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_KunPrList.SetImageList (&image, LVSIL_SMALL);   
	}

	m_KunPrList.Mode = m_KunPrList.TERMIN;
	FillList = m_KunPrList;
	m_KunPrList.ActiveListRow = FALSE;
	FillList.SetStyle (LVS_REPORT);
	if (m_KunPrList.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Artikel"),     m_KunPrList.PosA, 130, LVCFMT_RIGHT);
	FillList.SetCol (_T("Bezeichnung"), m_KunPrList.PosABz1, 250, LVCFMT_LEFT);
	FillList.SetCol (_T("VK-Preis"),    m_KunPrList.PosVkPr, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Laden-Preis"), m_KunPrList.PosLdPr, 100, LVCFMT_RIGHT);
	FillList.SetCol (_T("VK-Pr. alt"),    m_KunPrList.PosVkPr + 2, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Laden-Pr. alt"), m_KunPrList.PosLdPr + 2, 100, LVCFMT_RIGHT);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	KunGrid.Create (this, 1, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	CtrlGrid.CreateChoiceButton (m_KunChoice, IDC_KUNCHOICE, this);
	CCtrlInfo *c_KunChoice = new CCtrlInfo (&m_KunChoice, 1, 0, 1, 1);
	KunGrid.Add (c_KunChoice);


	KunPrGrid.Create (this, 1, 2);
    KunPrGrid.SetBorder (0, 0);
    KunPrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_KunPr = new CCtrlInfo (&m_KunPr, 0, 0, 1, 1);
	KunPrGrid.Add (c_KunPr);
//	CtrlGrid.CreateChoiceButton (m_KunPrChoice, IDC_KUNPRCHOICE, this);
//	CCtrlInfo *c_KunPrChoice = new CCtrlInfo (&m_KunPrChoice, 1, 0, 1, 1);
//	KunPrGrid.Add (c_KunPrChoice);

	PrGrGrid.Create (this, 1, 2);
    PrGrGrid.SetBorder (0, 0);
    PrGrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_PrGrStuf = new CCtrlInfo (&m_PrGrStuf, 0, 0, 1, 1);
	PrGrGrid.Add (c_PrGrStuf);
//	CtrlGrid.CreateChoiceButton (m_PrGrChoice, IDC_PRGRCHOICE, this);
//	CCtrlInfo *c_PrGrChoice = new CCtrlInfo (&m_PrGrChoice, 1, 0, 1, 1);
//	PrGrGrid.Add (c_PrGrChoice);

	ButtonGrid.Create (this, 5, 5);
    ButtonGrid.SetBorder (0, 0);
    ButtonGrid.SetCellHeight (20);
    ButtonGrid.SetGridSpace (0, 10);
	CCtrlInfo *c_Cancel = new CCtrlInfo (&m_Cancel, 0, 0, 1, 1);
	ButtonGrid.Add (c_Cancel);
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 1, 1, 1);
	ButtonGrid.Add (c_Save);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ButtonGrid.Add (c_Delete);
	CCtrlInfo *c_Insert = new CCtrlInfo (&m_Insert, 0, 3, 1, 1);
	ButtonGrid.Add (c_Insert);

	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LKun      = new CCtrlInfo (&m_LKun, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid   = new CCtrlInfo (&KunGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_KunGrid);
	CCtrlInfo *c_LKunKrz1  = new CCtrlInfo (&m_LKunKrz1, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LKunKrz1);
	CCtrlInfo *c_KunKrz1  = new CCtrlInfo (&m_KunKrz1, 1, 2, 3, 1); 
	CtrlGrid.Add (c_KunKrz1);

	CCtrlInfo *c_LKunPr      = new CCtrlInfo (&m_LKunPr, 0, 3, 1, 1); 
	CtrlGrid.Add (c_LKunPr);
	CCtrlInfo *c_KunPrGrid   = new CCtrlInfo (&KunPrGrid, 1, 3, 1, 1); 
	CtrlGrid.Add (c_KunPrGrid);
	CCtrlInfo *c_LKunZusBz  = new CCtrlInfo (&m_LKunZusBz, 0, 4, 1, 1); 
	CtrlGrid.Add (c_LKunZusBz);
	CCtrlInfo *c_KunZusBz  = new CCtrlInfo (&m_KunZusBz, 1, 4, 3, 1); 
	CtrlGrid.Add (c_KunZusBz);

	CCtrlInfo *c_LPrGrStuf     = new CCtrlInfo (&m_LPrGrStuf, 0, 5, 1, 1); 
	CtrlGrid.Add (c_LPrGrStuf);
	CCtrlInfo *c_PrGrGrid   = new CCtrlInfo (&PrGrGrid, 1, 5, 1, 1); 
	CtrlGrid.Add (c_PrGrGrid);
	CCtrlInfo *c_LZusBz  = new CCtrlInfo (&m_LZusBz, 0, 6, 1, 1); 
	CtrlGrid.Add (c_LZusBz);
	CCtrlInfo *c_ZusBz  = new CCtrlInfo (&m_ZusBz, 1, 6, 3, 1); 
	CtrlGrid.Add (c_ZusBz);
	CCtrlInfo *c_LGueAb  = new CCtrlInfo (&m_LGueAb, 6, 6, 1, 1); 
	CtrlGrid.Add (c_LGueAb);
	CCtrlInfo *c_GueAb  = new CCtrlInfo (&m_GueAb, 7, 6, 1, 1); 
	CtrlGrid.Add (c_GueAb);


	CCtrlInfo *c_ButtonGrid = new CCtrlInfo (&ButtonGrid, DOCKRIGHT, 5, 1, 3); 
	CtrlGrid.Add (c_ButtonGrid);
	CCtrlInfo *c_KunPrList  = new CCtrlInfo (&m_KunPrList, 0, 7, DOCKRIGHT, DOCKBOTTOM); 
	c_KunPrList->rightspace = RightListSpace;
	CtrlGrid.Add (c_KunPrList);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();
	memcpy (&Kun.kun, &kun, sizeof (KUN));
	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk, sizeof (IPRGRSTUFK));
	memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk, sizeof (I_KUN_PRK));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
	Form.Show ();
	ReadMdn ();
	EnableHeadControls (TRUE);
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}


void CIvKunPage::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CIvKunPage::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CIvKunPage::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CIvKunPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CIvKunPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_KunPrList.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_KunPrList &&
					GetFocus ()->GetParent () != &m_KunPrList )
				{

					break;
			    }
				m_KunPrList.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnKunPrchoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Kun)
				{
					OnKunchoice ();
					return TRUE;
				}
				m_KunPrList.OnKey9 ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CIvKunPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_Kun)
	{
		if (!Read ())
		{
			m_Kun.SetFocus ();
			return FALSE;
		}
	}

	if (Control != &m_KunPrList &&
		Control->GetParent ()!= &m_KunPrList )
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CIvKunPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_KunPrList &&
		Control->GetParent ()!= &m_KunPrList )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
	return FALSE;
}


BOOL CIvKunPage::ReadMdn ()
{
	int mdn;

	mdn = Mdn.mdn.mdn;
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Choice != NULL && mdn != Mdn.mdn.mdn)
	{
		delete Choice;
		Choice = NULL;
	}
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		FillPrGrStufCombo ();
		FillKunPrCombo ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CIvKunPage::read ()
{
	if (m_Kun.IsWindowEnabled ())
	{
		return FALSE;
	}
	return Read ();

}

BOOL CIvKunPage::Read ()
{
	if (ModalChoice)
	{
		CString cPrGrStuf;
		m_Kun.GetWindowText (cPrGrStuf);
		if (!CStrFuncs::IsDecimal (cPrGrStuf))
		{
			Search = cPrGrStuf;
			OnKunchoice ();
			Search = "";
			if (!ChoiceStat)
			{
				m_Kun.SetFocus ();
				m_Kun.SetSel (0, -1);
				return FALSE;
			}
		}
	}

	memcpy (&Kun.kun, &kun_null, sizeof (KUN));
	Form.Get ();
	strcpy (Kun.kun.kun_krz1, ""); 
	strcpy (I_kun_prk.i_kun_prk.zus_bz, ""); 
	strcpy (Iprgrstufk.iprgrstufk.zus_bz, ""); 
	if (Kun.kun.kun == 0l)
	{
		MessageBox (_T("Der Kunde darf nicht 0 sein"),
			        NULL,
					MB_OK | MB_ICONERROR);
		m_Kun.SetFocus ();
		m_Kun.SetSel (0, -1);
		return FALSE;
	}
	Kun.kun.mdn = Mdn.mdn.mdn;
	if (Kun.dbreadfirst () == 0)
	{
		I_kun_prk.i_kun_prk.mdn = Mdn.mdn.mdn;
		I_kun_prk.i_kun_prk.pr_gr_stuf = Kun.kun.pr_stu;
		I_kun_prk.i_kun_prk.kun_pr = Kun.kun.pr_lst;
		I_kun_prk.dbreadfirst ();
		Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
		Iprgrstufk.iprgrstufk.pr_gr_stuf = Kun.kun.pr_stu;
		Iprgrstufk.dbreadfirst ();
		DATE_STRUCT sDate;
		memcpy (&sDate, &Ivpr.iv_pr.gue_ab, sizeof (DATE_STRUCT));
		if (ReadList () == FALSE) 
		{
			m_Kun.SetFocus ();
			m_Kun.SetSel (0, -1);
			return FALSE;
		}
	    EnableHeadControls (FALSE);
		memcpy (&Ivpr.iv_pr.gue_ab, &sDate, sizeof (DATE_STRUCT));
		Form.Show ();
	}
	Form.Show ();
	return TRUE;
}

BOOL CIvKunPage::ReadPrGrStuf ()
{
	if (ModalChoicePrGrStuf)
	{
		CString cPrGrStuf;
		m_PrGrStuf.GetWindowText (cPrGrStuf);
		if (!CStrFuncs::IsDecimal (cPrGrStuf))
		{
			Search = cPrGrStuf;
			OnPrGrchoice ();
			Search = "";
			if (!ChoiceStat)
			{
				m_PrGrStuf.SetFocus ();
				m_PrGrStuf.SetSel (0, -1);
				return FALSE;
			}
		}
	}

	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
	Form.Get ();
	Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
	if (Iprgrstufk.dbreadfirst () == 0)
	{
		if (ReadList () == FALSE) 
		{
			return FALSE;
		}
	    EnableHeadControls (FALSE);
		Form.Show ();
		m_PrGrStuf.SetFocus ();
		m_PrGrStuf.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Preisgruppenstufe %ld nicht gefunden"),
			Iprgrstufk.iprgrstufk.pr_gr_stuf);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);

		memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
		Form.Show ();
		m_PrGrStuf.SetFocus ();
		m_PrGrStuf.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}


BOOL CIvKunPage::ReadList ()
{
	if (Kun.kun.kun == 0l)
	{
		MessageBox (_T("Der Kunde darf nicht 0 sein"),
			        NULL,
					MB_OK | MB_ICONERROR);
		m_KunPr.SetFocus ();
		m_KunPr.SetSel (0, -1);
		return FALSE;
	}
	m_KunPrList.DeleteAllItems ();
	m_KunPrList.vSelect.clear ();
	m_KunPrList.ListRows.Init ();
	int i = 0;
	memcpy (&Ivpr.iv_pr, &iv_pr_null, sizeof (IV_PR));
	Ivpr.iv_pr.mdn = Mdn.mdn.mdn;
	Ivpr.iv_pr.kun_pr       = I_kun_prk.i_kun_prk.kun_pr;
	Ivpr.iv_pr.pr_gr_stuf   = Iprgrstufk.iprgrstufk.pr_gr_stuf;
	Ivpr.iv_pr.kun          = Kun.kun.kun;
	m_KunPrList.mdn        = Mdn.mdn.mdn;
	m_KunPrList.kun_pr     = I_kun_prk.i_kun_prk.kun_pr;
	m_KunPrList.pr_gr_stuf = Iprgrstufk.iprgrstufk.pr_gr_stuf;
	m_KunPrList.kun		   = Kun.kun.kun;
	Ivpr.sqlopen (IprCursor);
	while (Ivpr.sqlfetch (IprCursor) == 0)
	{
		Ivpr.dbreadfirst ();
		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		memcpy (&Ipr.ipr, &ipr_null, sizeof (IPR));
		Ipr.ipr.mdn          = Ivpr.iv_pr.mdn;
		Ipr.ipr.pr_gr_stuf   = Ivpr.iv_pr.pr_gr_stuf;
		Ipr.ipr.kun_pr       = Ivpr.iv_pr.kun_pr;
		Ipr.ipr.kun          = Ivpr.iv_pr.kun;
		Ipr.ipr.a		     = Ivpr.iv_pr.a;
		Ipr.dbreadfirst ();
		A_bas.a_bas.a = Ivpr.iv_pr.a;
		A_bas.dbreadfirst ();
		FillList.InsertItem (i, 0);
		CString A;
		A.Format (_T("%.0lf"), Ivpr.iv_pr.a);
		FillList.SetItemText (A.GetBuffer (), i, m_KunPrList.PosA);
		CString ABz1;
		ABz1 = A_bas.a_bas.a_bz1;
		ABz1 += _T("   ");
		ABz1 += A_bas.a_bas.a_bz2;
		FillList.SetItemText (ABz1.GetBuffer (), i, m_KunPrList.PosABz1);
		CString VkPr;
		m_KunPrList.DoubleToString (Ivpr.iv_pr.vk_pr_eu, VkPr, 4);
//		CDecimal *decVK = new CDecimal (VkPr);
//		delete decVK;
		Ivpr.iv_pr.vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
		FillList.SetItemText (VkPr.GetBuffer (), i, m_KunPrList.PosVkPr);
		CString LdPr;
		m_KunPrList.DoubleToString (Ivpr.iv_pr.ld_pr_eu, LdPr, 2);
		FillList.SetItemText (LdPr.GetBuffer (), i, m_KunPrList.PosLdPr);

		m_KunPrList.DoubleToString (Ipr.ipr.vk_pr_eu, VkPr, 4);
		FillList.SetItemText (VkPr.GetBuffer (), i, m_KunPrList.PosVkPr + 2);
		m_KunPrList.DoubleToString (Ipr.ipr.ld_pr_eu, LdPr, 2);
		FillList.SetItemText (LdPr.GetBuffer (), i, m_KunPrList.PosLdPr + 2);

		CVPreise *iv_pr = new CVPreise (VkPr, LdPr, Ivpr.iv_pr);
		DbRows.Add (iv_pr);
		i ++;
	}
	return TRUE;
}

BOOL CIvKunPage::IsChanged (CVPreise *pIpr)
{
	DbRows.FirstPosition ();
	CVPreise *iv_pr;
	while ((iv_pr = (CVPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ivpr.iv_pr, &pIpr->iv_pr, sizeof (IV_PR));
		if (Ivpr == iv_pr->iv_pr) break;
	}
	if (iv_pr == NULL)
	{
		return TRUE;
	}
	if (pIpr->cEk != iv_pr->cEk) return TRUE;
	if (pIpr->cVk != iv_pr->cVk) return TRUE;
	return FALSE;
}

BOOL CIvKunPage::InList (IV_PR_CLASS& Ivpr)
{
	ListRows.FirstPosition ();
	CVPreise *iv_pr;
	while ((iv_pr = (CVPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Ivpr == iv_pr->iv_pr) return TRUE;
	}
    return FALSE;
}

void CIvKunPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CVPreise *iv_pr;
	while ((iv_pr = (CVPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ivpr.iv_pr, &iv_pr->iv_pr, sizeof (IV_PR));
		if (!InList (Ivpr))
		{
			Ivpr.dbdelete ();		
//			PgrProt.Write (1);
		}
	}
}

BOOL CIvKunPage::Write ()
{
	extern short sql_mode;
	short sql_s;
	if (!TerminOK ())
	{
		m_KunPrList.StopEnter ();
		m_GueAb.SetFocus ();
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	Ivpr.beginwork ();
	m_KunPrList.StopEnter ();
	int count = m_KunPrList.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 IV_PR *iv_pr = new IV_PR;
		 memcpy (iv_pr, &iv_pr_null, sizeof (IV_PR));
         CString Text;
		 Text = m_KunPrList.GetItemText (i, m_KunPrList.PosA);
		 iv_pr->a = CStrFuncs::StrToDouble (Text);
     	 CString VkPr =  m_KunPrList.GetItemText (i, m_KunPrList.PosVkPr);
		 iv_pr->vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
		 iv_pr->vk_pr_i = iv_pr->vk_pr_eu;
		 CString LdPr =  m_KunPrList.GetItemText (i, m_KunPrList.PosLdPr);
		 iv_pr->ld_pr_eu = CStrFuncs::StrToDouble (LdPr);
		 iv_pr->ld_pr = iv_pr->ld_pr_eu;
		 iv_pr->mdn = Mdn.mdn.mdn; 
		 iv_pr->pr_gr_stuf = Iprgrstufk.iprgrstufk.pr_gr_stuf; 
		 iv_pr->kun_pr = I_kun_prk.i_kun_prk.kun_pr;
		 iv_pr->kun = Kun.kun.kun;
		 iv_pr->gue_ab = Ivpr.iv_pr.gue_ab;
		 CVPreise *pr = new CVPreise (VkPr, LdPr, *iv_pr);
		 if (iv_pr->vk_pr_eu != 0.0 || 
			 iv_pr->ld_pr_eu != 0.0)
		 {
				ListRows.Add (pr);
		 }
		 delete iv_pr;
	}

	DeleteDbRows ();

	ListRows.FirstPosition ();
	CVPreise *iv_pr;
	while ((iv_pr = (CVPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&Ivpr.iv_pr, &iv_pr->iv_pr, sizeof (IV_PR));
		Ivpr.dbupdate ();
		if (IsChanged (iv_pr))
		{
//			PgrProt.Write ();
		}
	}
	EnableHeadControls (TRUE);
	m_Kun.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	Ivpr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

BOOL CIvKunPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_KunPr.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Kundenpreisliste l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	Ivpr.beginwork ();
	m_KunPrList.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	m_KunPrList.DeleteAllItems ();
	I_kun_prk.dbdelete ();

	EnableHeadControls (TRUE);
	m_KunPr.SetFocus ();
	Ivpr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

void CIvKunPage::OnKunPrchoice ()
{
    ChoiceStat = TRUE;
	Form.Get ();
	if (Choice == NULL)
	{
		ChoiceKunPr = new CChoiceIKunPr (this);
	    ChoiceKunPr->IsModal = TRUE;
		ChoiceKunPr->m_Mdn = Mdn.mdn.mdn;
		ChoiceKunPr->CreateDlg ();
	}

    ChoiceKunPr->SetDbClass (&A_bas);
	ChoiceKunPr->SearchText = Search;
	ChoiceKunPr->DoModal();
    if (ChoiceKunPr->GetState ())
    {
		  CIKunPrList *abl = ChoiceKunPr->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
          I_kun_prk.i_kun_prk.mdn = Mdn.mdn.mdn;
          I_kun_prk.i_kun_prk.kun_pr = abl->kun_pr;
		  if (I_kun_prk.dbreadfirst () == 0)
		  {
			Form.Show ();
//			m_KunPr.EnableWindow (TRUE);
			EnableHeadControls (TRUE);
			m_PrGrStuf.SetSel (0, -1, TRUE);
			m_PrGrStuf.SetFocus ();
			if (Search == "")
			{
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			}
		  }
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}

void CIvKunPage::OnKunchoice ()
{
    ChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceKun (this);
	    Choice->IsModal = ModalChoice;
		Choice->m_Mdn = Mdn.mdn.mdn;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->SearchText = Search;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
/*
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		cy -= 100;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
*/
		Choice->MoveWindow (&rect);
		Choice->SetFocus ();

		return;
	}
    if (Choice->GetState ())
    {
		  CKunList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
          Kun.kun.mdn = Mdn.mdn.mdn;
          Kun.kun.kun = abl->kun;
		  if (Kun.dbreadfirst () == 0)
		  {
			Form.Show ();
//			m_KunPr.EnableWindow (TRUE);
			EnableHeadControls (TRUE);
			m_Kun.SetFocus ();
			if (Search == "")
			{
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			}
		  }
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}

void CIvKunPage::OnKunSelected ()
{
	if (Choice == NULL) return;
    CKunList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    Kun.kun.kun = abl->kun;
    if (Kun.dbreadfirst () == 0)
	{
		m_Kun.EnableWindow (TRUE);
		m_Kun.SetFocus ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	if (CloseChoice)
	{
		OnKunCanceled (); 
	}
    Form.Show ();
	if (Choice->FocusBack)
	{
		DATE_STRUCT sDate;
		memcpy (&sDate, &Ivpr.iv_pr.gue_ab,
				sizeof (DATE_STRUCT));
		Read ();
		ReadList ();
		memcpy (&Ivpr.iv_pr.gue_ab,&sDate, 
				sizeof (DATE_STRUCT));
		Form.Show ();
		EnableHeadControls (FALSE);
		m_GueAb.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CIvKunPage::OnKunCanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CIvKunPage::StepBack ()
{
	if (m_Kun.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		m_KunPrList.StopEnter ();
		EnableHeadControls (TRUE);
		m_Kun.SetFocus ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
		m_KunPrList.DeleteAllItems ();
	}
	return TRUE;
}

void CIvKunPage::OnCancel ()
{
	StepBack ();
}

void CIvKunPage::OnSave ()
{
	Write ();
}

void CIvKunPage::OnPrGrchoice ()
{
    ChoiceStat = TRUE;
	Form.Get ();
	if (ChoicePrGrStuf != NULL && !ModalChoicePrGrStuf)
	{
		ChoicePrGrStuf->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoicePrGrStuf == NULL)
	{
		ChoicePrGrStuf = new CChoicePrGrStuf (this);
	    ChoicePrGrStuf->IsModal = ModalChoicePrGrStuf;
		ChoicePrGrStuf->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoicePrGrStuf->SetDbClass (&A_bas);
	ChoicePrGrStuf->m_Mdn = Mdn.mdn.mdn;
	ChoicePrGrStuf->SearchText = Search;
	if (ModalChoicePrGrStuf)
	{
			ChoicePrGrStuf->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		ChoicePrGrStuf->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		cy -= 100;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
		ChoicePrGrStuf->MoveWindow (&rect);
		ChoicePrGrStuf->SetFocus ();

		return;
	}
    if (ChoicePrGrStuf->GetState ())
    {
		  CPrGrStufList *abl = ChoicePrGrStuf->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
          Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
          Iprgrstufk.iprgrstufk.pr_gr_stuf = abl->pr_gr_stuf;
		  if (Iprgrstufk.dbreadfirst () == 0)
		  {
			Form.Show ();
			m_PrGrStuf.EnableWindow (TRUE);
			m_PrGrStuf.SetSel (0, -1, TRUE);
			m_PrGrStuf.SetFocus ();
			if (Search == "")
			{
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			}
		  }
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}


void CIvKunPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CIvKunPage::FillPrGrStufCombo ()
{
}

void CIvKunPage::FillKunPrCombo ()
{
}

void CIvKunPage::OnDelete ()
{
	if (m_KunPr.IsWindowEnabled ())
	{
		Form.Get ();
		if (I_kun_prk.i_kun_prk.kun_pr == 0)
		{
			return;
		}
		CString Message;
		Message.Format (_T("Kundebpreisliste %ld l�schen"), I_kun_prk.i_kun_prk.kun_pr);

		int ret = MessageBox (Message, NULL, 
			MB_YESNO | MB_DEFBUTTON2 | MB_ICONQUESTION);
		if (ret == IDYES)
		{
			I_kun_prk.dbdelete ();
			m_KunPr.SetFocus ();
 			memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
			memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk_null, sizeof (I_KUN_PRK));
			Form.Show ();
			DestroyRows (DbRows);
			DestroyRows (ListRows);
			m_KunPrList.DeleteAllItems ();
			Ivpr.sqlexecute (IprDelCursor);
		}
		m_KunPr.SetFocus ();
		m_KunPr.SetSel (0, -1);
		return;
	}
	m_KunPrList.DeleteRow ();
}

void CIvKunPage::OnInsert ()
{
	m_KunPrList.InsertRow ();
}

/*
BOOL CIvKunPage::Print ()
{
	CProcess print;
	CString Command = "70001 11112";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}
*/

BOOL CIvKunPage::Print ()
{
// I_kun_prk.i_kun_prk.kun_pr
	CProcess print;
	Form.Get ();
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11136.llf", tmp);
	}
	else
	{
		dName = "11136.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11136\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "kun %ld %ld\n", Kun.kun.kun,
			                          Kun.kun.kun);
		fclose (fp);
		Command.Format ("dr70001 -name 11136 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11136";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

BOOL CIvKunPage::PrintAll ()
{
	CProcess print;
	Form.Get ();
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11136.llf", tmp);
	}
	else
	{
		dName = "11136.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11136\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "MITRANGE 1\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "kun %ld %ld\n", (long) 1,
			                             (long) 99999999);
		fclose (fp);
		Command.Format ("dr70001 -name 11136 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11136";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CIvKunPage::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_KunPrList.StartPauseEnter ();
}

void CIvKunPage::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_KunPrList.EndPauseEnter ();
}

void CIvKunPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Enable (enable);
	PosControls.Enable (!enable);
}

void CIvKunPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CVPreise *iv_pr;
	while ((iv_pr = (CVPreise *) Rows.GetNext ()) != NULL)
	{
		delete iv_pr;
	}
	Rows.Init ();
}

void CIvKunPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_KunPrList.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("RemoveKun", cfg_v) == TRUE)
    {
			RemoveKun = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_KunPrList.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_KunPrList.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_KunPrList.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CIvKunPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_KunPrList.ListEdit ||
        Control == &m_KunPrList.ListComboBox ||
		Control == &m_KunPrList.SearchListCtrl.Edit)	
	{
		ListCopy ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CIvKunPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CIvKunPage::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_KunPrList.ListEdit.m_hWnd) ||
		IsWindow (m_KunPrList.ListComboBox) ||
		IsWindow (m_KunPrList.SearchListCtrl.Edit))
	{
		m_KunPrList.StopEnter ();
		m_KunPrList.StartEnter (m_KunPrList.EditCol, m_KunPrList.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
		for (int i = 0; i < m_KunPrList.GetItemCount (); i ++)
		{
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = "";
			int cols = m_KunPrList.GetHeaderCtrl ()->GetItemCount ();
			for (int j = 0; j < cols; j ++)
			{
				if (Row != "")
				{
					Row += Separator;
				}
				CString Field = m_KunPrList.GetItemText (i, j);
				Field.TrimRight ();
				Row += Field; 
			}
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

BOOL CIvKunPage::OnKillActive ()
{
/*
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
*/
	return TRUE;
}

BOOL CIvKunPage::TerminOK ()
{
	DbTime SysDate;

	Form.Get ();

	DbTime GueAb (&Ivpr.iv_pr.gue_ab);

	if (GueAb < SysDate)
	{
		MessageBox (_T("Das Aktivierungsdatum ist kleiner als das Tagesdatum"),
			        NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}
	return TRUE;
}