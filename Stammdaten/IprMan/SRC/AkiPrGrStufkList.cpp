#include "StdAfx.h"
#include "AkiPrGrStufkList.h"

CAkiPrGrStufkList::CAkiPrGrStufkList(void)
{
	mdn = 0;
	aki_nr = 0;
	pr_gr_stuf = 0l;
	zus_bz = _T("");
}

CAkiPrGrStufkList::CAkiPrGrStufkList(short mdn, long aki_nr, long pr_gr_stuf, LPTSTR zus_bz)
{
	this->mdn    = mdn;
    this->aki_nr = aki_nr;
	this->pr_gr_stuf = pr_gr_stuf;
	this->zus_bz = zus_bz;
}

CAkiPrGrStufkList::~CAkiPrGrStufkList(void)
{
}
