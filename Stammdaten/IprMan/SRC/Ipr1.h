#pragma once
#include "DbFormView.h"
#include "KunPrSheet.h"
#include "PrArtPage.h"
#include "PrGrPage.h"
#include "PrKunPage.h"
#include "KunPage.h"
#include "mo_progcfg.h"



// CIpr1-Formularansicht

class CIpr1 : public DbFormView
{
	DECLARE_DYNCREATE(CIpr1)

protected:
	CIpr1();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CIpr1();

public:
	enum { IDD = IDD_IPR1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
    afx_msg void OnSize(UINT, int, int);
	DECLARE_MESSAGE_MAP()
public:
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	int StartSize;
	int tabx;
	int taby;
	CKunPrSheet dlg;
	CPrArtPage Page1;
	CPrGrPage Page2;
	CPrKunPage Page3;
	CKunPage Page4;
	HBRUSH hBrush;
	HBRUSH staticBrush;
	void DeletePropertySheet ();
	afx_msg void OnLanguage();
	afx_msg void OnFileSave();
	afx_msg void OnBack();
	void ReadCfg ();

	afx_msg void OnDelete();
	afx_msg void OnInsert();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnDeleteall();
	afx_msg void OnFilePrint();
	afx_msg void OnPrintAll();
};


