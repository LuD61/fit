#ifndef AFX_CHOICEX_H__031B3EF8_AD61_4AD0_9790_87CE2C1EFCCE__INCLUDED_
#define AFX_CHOICEX_H__031B3EF8_AD61_4AD0_9790_87CE2C1EFCCE__INCLUDED_

// Choice.h : Header-Datei
//

#define SELECTED 5001
#define CANCELED 5002

#include "resource.h"
#include "dbclass.h"
#include "ListDropTarget.h"
//#include "resource.h"

#ifndef IDC_SEARCH
#define IDC_SEARCH                      5002
#define IDC_LISTTYPE                    5003
#define IDC_CHOICE                      5004
#define IDI_ICON2                       134
//#define IDB_BITMAP2                     132
#define IDM_LIST                        32763
#define IDM_ICON                        32764
#define IDM_SMALLICON                   32765
#define IDM_REPORT                      32766
#define IDR_VIEWTYPE                    139
#endif

#define DLGBUTTON 0x0080
#define DLGEDIT 0x0081
#define DLGSTATIC 0x0082
#define DLGLISTBOX 0x0083
#define DLGSCROLLBAR 0x0084
#define DLGCOMBOBOX 0x0085

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CChoice 

class CChoiceX : public CDialog
{

private :
// Konstruktion
    static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
    static DLGTEMPLATE DlgHeader;
    static DLGITEMTEMPLATE SearchEdit;
    static DLGITEMTEMPLATE SearchLabel;
    static DLGITEMTEMPLATE SearchList;
    static DLGITEMTEMPLATE SearchOK;
    static DLGITEMTEMPLATE SearchCancel;
    static DLGITEMTEMPLATE ListType;

    DWORD DlgMenu;
    DWORD DlgClass;
    LPTSTR DlgCaption;
    void *pDlgTemplate;
    BYTE *DlgItempos;
    CWnd *pParent;
protected :
	DECLARE_MESSAGE_MAP()
	BOOL PreTranslateMessage(MSG* pMsg);
public:
	BOOL InSort;
	int SelectedItem;
	CString CloseTxt;

	CListDropTarget dropTarget;
	CChoiceX(CWnd* pParent = NULL);   // Standardkonstruktor
    ~CChoiceX ();
	BOOL IsModal;
    BOOL DlgOK (void);
    WORD *AddHeader (WORD *, LPSTR);
    WORD *AddFont (WORD *, WORD, LPSTR);
    BOOL AddItem (DLGITEMTEMPLATE *, WORD,  LPCSTR);
    BOOL AddItem (DLGITEMTEMPLATE *, WORD,  CString&);
    BOOL AddItem (DLGITEMTEMPLATE *, LPCSTR, LPCSTR);
    void SetDbClass (DB_CLASS *);
    void SetDlgSize (int, int);
    void SetDlgStyle (DWORD);
    DWORD GetDlgStyle (void);
    void CreateDlg (void);
    void CreateDlg (int, int);
    void MoveCtrl (void);
    void MoveCtrl (int, int);

    HIMAGELIST hLarge;   // image list for icon view 
    HIMAGELIST hSmall;   // image list for other views 
    DB_CLASS *DbClass;
    int iSelect;
    TCHAR SelText [256];
	CString SearchText;
    BOOL State;
    DWORD ListStyle;
    void *SelectedRow;
	static int SortRow;

    LPTSTR GetSelText ()
    {
        return SelText;
    }

    BOOL GetState ()
    {
        return State;
    }

    void ScrolltoIdx(CListCtrl *, int); 
    DWORD SetStyle (DWORD);
	DWORD SetExtendedStyle (DWORD);
    CListCtrl *GetListView (void);
    BOOL SetCol (LPTSTR, int, int, int);
    BOOL SetCol (LPTSTR, int, int);
    BOOL InsertItem (int);
    BOOL InsertItem (int, int);
    BOOL SetItemText (LPTSTR, int, int);
    void DragList ();

    virtual void BezLabel (CListCtrl *);
    virtual void NumLabel (CListCtrl *);
    virtual void FillList (void);
    virtual void RefreshList (void);
    virtual void Search (void);
    virtual void Sort (CListCtrl *);
	virtual void SetSelText (CListCtrl *, int);
	virtual void *GetSelectedRow ();
    void SetItem (int, BOOL); 
    void UnSetItem (int); 
	void SetListFocus ();
	BOOL FocusBack;

    static double StrToDouble (LPTSTR);
    static double StrToDouble (CString&);

// Dialogfelddaten
	//{{AFX_DATA(CChoiceX)
//	enum { IDD = IDD_CHOICE };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CChoice)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CChoice)
	virtual BOOL OnInitDialog();
	afx_msg void OnColumnclickChoice(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkChoice(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemChanged(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnListtype();
	afx_msg void OnChangeSearch();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnNMSetfocusList (NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnLvnBeginrdragList(NMHDR *pNMHDR, LRESULT *pResult);
	//}}AFX_MSG
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_CHOICE_H__031B3EF8_AD61_4AD0_9790_87CE2C1EFCCE__INCLUDED_
