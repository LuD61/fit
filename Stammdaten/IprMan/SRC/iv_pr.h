#ifndef _IV_PR_DEF
#define _IV_PR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct IV_PR {
   short          mdn;
   long           pr_gr_stuf;
   long           kun_pr;
   double         a;
   double         vk_pr_i;
   double         vk_pr_eu;
   double         ld_pr;
   double         ld_pr_eu;
   DATE_STRUCT    gue_ab;
   short          waehrung;
   long           kun;
};
extern struct IV_PR iv_pr, iv_pr_null;

#line 8 "iv_pr.rh"

class IV_PR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               IV_PR iv_pr;
               IV_PR_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (IV_PR&);  
};
#endif
