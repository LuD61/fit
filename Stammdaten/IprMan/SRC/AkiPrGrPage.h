// DialogTestDlg.h : Headerdatei
//

#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "PrGrListCtrl.h"
#include "PrArtPageConst.h"
#include "FillList.h"
#include "a_bas.h"
#include "mdn.h"
#include "adr.h"
#include "akiprgrstp.h"
#include "Ipr.h"
#include "PGrProt.h"
#include "Iprgrstufk.h"
#include "Akiprgrstk.h"
#include "I_kun_prk.h"
#include "Kun.h"
#include "ChoicePrGrStuf.h"
#include "ChoiceAkiPrGrStuf.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "FormTab.h"
#include "mo_progcfg.h"
#include "Controls.h"
#include "AkiPreise.h"

#define IDC_PRGRCHOICE 4000
#define IDC_MDNCHOICE 3001
#define IDC_AKINRCHOICE 3002

// CPrArtPreise Dialogfeld
class CAkiPrGrPage : public CDbPropertyPage
{
// Konstruktion
public:
	CAkiPrGrPage(CWnd* pParent = NULL);	// Standardkonstruktor
	CAkiPrGrPage(UINT nIDTemplate);	// Standardkonstruktor

	~CAkiPrGrPage ();

// Dialogfelddaten
//	enum { IDD = IDD_DIALOGTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung
    virtual BOOL PreTranslateMessage(MSG* pMsg);


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public :
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	BOOL RemoveKun;
	int StartSize;

	CControls HeadControls;
	CControls PosControls;
	CControls ButtonControls;
	BOOL HideButtons;
	int RightListSpace;
	CVector DbRows;
	CVector ListRows;
	int IprCursor;
	int IprGrStufkCursor;
	int IKunPrkCursor;
	int IprDelCursor;
	CWnd *Frame;
	CCtrlGrid CtrlGrid;
	CCtrlGrid PrGrGrid;
	CCtrlGrid MdnGrid;
	CButton m_MdnChoice;
	CCtrlGrid AkiNrGrid;
	CButton m_AkiNrChoice;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CNumEdit m_MdnName;
	CStatic m_LAkiNr;
	CNumEdit m_AkiNr;
	CButton m_PrGrChoice;
	CStatic m_LZusBez;
	CEdit m_ZusBez;
	CStatic m_LPrGrStuf;
	CNumEdit m_PrGrStuf;
	CEdit m_LZusBz;
	CTextEdit m_ZusBz;
	CStatic m_LAkiVon;
	CDateTimeCtrl m_AkiVon;
	CStatic m_LAkiBis;
	CDateTimeCtrl m_AkiBis;
	CFillList FillList;
	CPrGrListCtrl m_PrGrList;
	CButton m_Cancel;
	CButton m_Save;
	CButton m_Delete;
	CButton m_Insert;
	int RighListSpace;
	CCtrlGrid ButtonGrid;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	A_BAS_CLASS A_bas;
// l�schen
//	IV_PR_CLASS Ivpr;
	AKIPRGRSTP_CLASS Akiprgrstp;
	IPR_CLASS Ipr;
	CPGrProt PgrProt;
	IPRGRSTUFK_CLASS Iprgrstufk;
// l�schen
//	IVPRGRSTK_CLASS Ivprgrstk;
	AKIPRGRSTK_CLASS Akiprgrstk;
	PRG_PROT_CLASS Prg_prot;
	I_KUN_PRK_CLASS I_kun_prk;
	KUN_CLASS Kun;
	ADR_CLASS KunAdr;
	CFormTab Form;
	CChoicePrGrStuf *ChoicePrGrStuf;
	CChoiceAkiPrGrStuf *Choice;
	BOOL ModalChoice;
	BOOL CloseChoice;
	BOOL ChoiceStat;
	CString Search;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceKun *ChoiceKun;
	BOOL ModalChoiceKun;
	BOOL ModalChoicePrGrStuf;
	CFont Font;
	CFont lFont;
	CString PersName;
	CString Separator;
    CImageList image; 
	int CellHeight;

	virtual void OnSize (UINT, int, int);
	void OnPrGrchoice ();
	void OnChoice ();
    void OnSelected ();
    void OnCanceled ();
    void OnMdnchoice(); 
	virtual BOOL Read ();
	virtual BOOL Write ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	virtual BOOL StepBack ();
	virtual void OnCancel ();
	virtual void OnSave ();
	BOOL ReadMdn ();
	BOOL ReadPrGrStuf ();
	BOOL ReadList ();
    void FillPrGrStufCombo ();
    void FillKunPrCombo ();
	virtual BOOL DeleteAll ();
	virtual BOOL Print ();
	virtual BOOL PrintAll ();
	virtual void OnDelete ();
	virtual void OnInsert ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult);
    void EnableHeadControls (BOOL enable);
    void WriteRow (int row);
    void DestroyRows(CVector &Rows);
    void DeleteDbRows ();
    BOOL IsChanged (CAkiPreise* Akipr);
    BOOL InList (AKIPRGRSTP_CLASS& Akiprgrstp);
	void ReadCfg ();
    void ListCopy ();
	BOOL TerminOK ();
};
