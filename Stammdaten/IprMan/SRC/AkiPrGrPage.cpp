// AktPrGrPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
// #include "IPrDialog.h"
#include "IprMan.h"
#include "AkiPrGrPage.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Decimal.h"
#include "Process.h"
#include "DbTime.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

// CAkiPrGrPage Dialogfeld

CAkiPrGrPage::CAkiPrGrPage(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage()
{
	Choice = NULL;
	ModalChoice = TRUE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ChoicePrGrStuf = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	ModalChoicePrGrStuf = TRUE;
	IprCursor = -1;
	IprDelCursor = -1;
	IprGrStufkCursor = -1;
	IKunPrkCursor = -1;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_AkiNr);
/*
	HeadControls.Add (&m_ZusBez);
	HeadControls.Add (&m_PrGrStuf);
	HeadControls.Add (&m_ZusBz);
	HeadControls.Add (&m_AkiVon);
	HeadControls.Add (&m_AkiBis);
*/
	PosControls.Add (&m_ZusBez);
	PosControls.Add (&m_PrGrStuf);
	PosControls.Add (&m_ZusBz);
	PosControls.Add (&m_AkiVon);
	PosControls.Add (&m_AkiBis);
	PosControls.Add (&m_PrGrList);

	ButtonControls.Add (&m_Cancel);
	ButtonControls.Add (&m_Save);
	ButtonControls.Add (&m_Delete);
	ButtonControls.Add (&m_Insert);

    	HideButtons = FALSE;

	Frame = NULL;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Search = _T("");
	Separator = _T(";");
	CellHeight = 0;
	Cfg.SetProgName( _T("IPrDialog"));
}

CAkiPrGrPage::CAkiPrGrPage(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	ChoiceMdn = NULL;
	ChoicePrGrStuf = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	ModalChoicePrGrStuf = TRUE;
	Frame = NULL;
	IprCursor = -1;
	IprDelCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
}

CAkiPrGrPage::~CAkiPrGrPage()
{
	Font.DeleteObject ();
	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	Akiprgrstp.dbclose ();
	Ipr.dbclose ();
	Iprgrstufk.dbclose ();
	Akiprgrstk.dbclose ();
	Kun.dbclose ();
	KunAdr.dbclose ();
	if (IprCursor == -1)
	{
		Ipr.sqlclose (IprCursor);
	}
	if (IprDelCursor == -1)
	{
		Ipr.sqlclose (IprDelCursor);
	}
	if (IprGrStufkCursor == -1)
	{
		A_bas.sqlclose (IprGrStufkCursor);
	}
	if (IKunPrkCursor == -1)
	{
		A_bas.sqlclose (IKunPrkCursor);
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);

}

void CAkiPrGrPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LAKI_NR, m_LAkiNr);
	DDX_Control(pDX, IDC_AKI_NR, m_AkiNr);
	DDX_Control(pDX, IDC_LZUS_BEZ, m_LZusBez);
	DDX_Control(pDX, IDC_ZUS_BEZ, m_ZusBez);
	DDX_Control(pDX, IDC_LPR_GR_STUF, m_LPrGrStuf);
	DDX_Control(pDX, IDC_PR_GR_STUF, m_PrGrStuf);
	DDX_Control(pDX, IDC_LZUS_BZ, m_LZusBz);
	DDX_Control(pDX, IDC_ZUS_BZ, m_ZusBz);
	DDX_Control(pDX, IDC_LAKI_VON, m_LAkiVon);
	DDX_Control(pDX, IDC_AKI_VON, m_AkiVon);
	DDX_Control(pDX, IDC_LAKI_BIS, m_LAkiBis);
	DDX_Control(pDX, IDC_AKI_BIS, m_AkiBis);
	DDX_Control(pDX, IDC_PRGR_LIST, m_PrGrList);

	DDX_Control(pDX, IDC_CANCEL, m_Cancel);
	DDX_Control(pDX, IDC_SAVE, m_Save);
	DDX_Control(pDX, IDC_DELETE, m_Delete);
	DDX_Control(pDX, IDC_INSERT, m_Insert);

}

BEGIN_MESSAGE_MAP(CAkiPrGrPage, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_SIZE ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_PRGRCHOICE ,  OnPrGrchoice)
	ON_BN_CLICKED(IDC_AKINRCHOICE ,  OnChoice)
	ON_BN_CLICKED(IDC_CANCEL ,   OnCancel)
	ON_BN_CLICKED(IDC_SAVE ,     OnSave)
	ON_BN_CLICKED(IDC_DELETE ,   OnDelete)
	ON_BN_CLICKED(IDC_INSERT ,   OnInsert)
	ON_COMMAND (SELECTED, OnSelected)
	ON_COMMAND (CANCELED, OnCanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
END_MESSAGE_MAP()


// CAkiPrGrPage Meldungshandler

BOOL CAkiPrGrPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
//	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
//	ASSERT(IDM_ABOUTBOX < 0xF000);

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	A_bas.opendbase (_T("bws"));

	CUtil::GetPersName (PersName);
//	PgrProt.Construct (PersName, CString ("11121"), &Ivpr);

	ReadCfg ();

	if (HideButtons)
	{
		ButtonControls.SetVisible (FALSE);
		RightListSpace = 15;
	}
	else
	{
		ButtonControls.SetVisible (TRUE);
		RightListSpace = 125;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	m_ZusBz.SetReadOnly ();
	m_ZusBz.ModifyStyle (WS_TABSTOP, 0);

	m_Cancel.SetWindowText (_T("Beenden"));
    HBITMAP HbF5 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F5T", "F5MASKT");
	m_Cancel.SetBitmap (HbF5);

    HBITMAP HbF12 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F12T", "F12MASKT");
	m_Save.SetBitmap (HbF12);

    HBITMAP HbDel = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "DELT", "DELMASKT");
	m_Delete.SetBitmap (HbDel);

    HBITMAP HbInsert = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "INSERTT", "INSERTMASKT");
	m_Insert.SetBitmap (HbInsert);

    memcpy (&Akiprgrstk.akiprgrstk, &akiprgrstk_null, sizeof (AKIPRGRSTK));
    memcpy (&Akiprgrstp.akiprgrstp, &akiprgrstp_null, sizeof (AKIPRGRSTP));

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
#ifdef SHORTAKI
    Form.Add (new CFormField (&m_AkiNr,EDIT,      (short *) &Akiprgrstk.akiprgrstk.aki_nr, VSHORT));
#else
    Form.Add (new CFormField (&m_AkiNr,EDIT,      (long *) &Akiprgrstk.akiprgrstk.aki_nr, VLONG));
#endif
    Form.Add (new CFormField (&m_ZusBez,EDIT,     (char *) &Akiprgrstk.akiprgrstk.zus_bz, VCHAR));
    Form.Add (new CFormField (&m_AkiVon,DATETIMEPICKER,  (DATE_STRUCT *) &Akiprgrstk.akiprgrstk.aki_von, VDATE));
    Form.Add (new CFormField (&m_AkiBis,DATETIMEPICKER,  (DATE_STRUCT *) &Akiprgrstk.akiprgrstk.aki_bis, VDATE));
    Form.Add (new CFormField (&m_PrGrStuf,EDIT,   (long *) &Iprgrstufk.iprgrstufk.pr_gr_stuf, VLONG));
    Form.Add (new CUniFormField (&m_ZusBz,EDIT,   (char *) Iprgrstufk.iprgrstufk.zus_bz, VCHAR));
    
#ifdef SHORTAKI
	Akiprgrstp.sqlin ((short *)  &Akiprgrstp.akiprgrstp.aki_nr, SQLSHORT, 0);
#else
	Akiprgrstp.sqlin ((long *)  &Akiprgrstp.akiprgrstp.aki_nr, SQLLONG, 0);
#endif
	Akiprgrstp.sqlout ((double *) &Akiprgrstp.akiprgrstp.a,  SQLDOUBLE, 0);
	Akiprgrstp.sqlout ((double *) &Akiprgrstp.akiprgrstp.me_min,  SQLDOUBLE, 0);
	IprCursor = Akiprgrstp.sqlcursor (_T("select a, me_min from akiprgrstp ")
		                       _T("where aki_nr = ? "));
#ifdef SHORTAKI
	Akiprgrstp.sqlin ((short *)  &Akiprgrstp.akiprgrstp.aki_nr, SQLSHORT, 0);
#else
	Akiprgrstp.sqlin ((long *)  &Akiprgrstp.akiprgrstp.aki_nr, SQLLONG, 0);
#endif
	IprDelCursor = Akiprgrstp.sqlcursor (_T("delete from akiprgrstp ")
		                 	     _T("where aki_nr = ? "));

	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (16,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		CBitmap uncheck;
		BOOL b = uncheck.LoadBitmap (IDB_UNCHECK);
		image.Add (&uncheck, RGB (0,0,0));
		CBitmap check;
		b = check.LoadBitmap (IDB_CHECK);
		image.Add (&check, RGB (0,0,0));
		m_PrGrList.SetImageList (&image, LVSIL_SMALL);   
	}

	m_PrGrList.Mode = m_PrGrList.AKTION;
	FillList = m_PrGrList;
	FillList.SetStyle (LVS_REPORT);
	if (m_PrGrList.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Artikel"),            m_PrGrList.PosA, 130, LVCFMT_RIGHT);
	FillList.SetCol (_T("Bezeichnung"),        m_PrGrList.PosABz1, 250, LVCFMT_LEFT);
	FillList.SetCol (_T("VK-Preis Aktion"),    m_PrGrList.PosVkPr, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Laden-Preis Aktion"), m_PrGrList.PosLdPr, 100, LVCFMT_RIGHT);
	FillList.SetCol (_T("VK-Pr."),             m_PrGrList.PosVkPr + 2, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Laden-Pr."),          m_PrGrList.PosLdPr + 2, 100, LVCFMT_RIGHT);
//	FillList.SetCol (_T("Rabatt-Kz"),          m_PrGrList.PosRabKz,  80, LVCFMT_IMAGE | LVCFMT_CENTER);
	FillList.SetCol (_T("Rabatt-Kz"),          m_PrGrList.PosRabKz,  80, LVCFMT_CENTER);
	FillList.SetCol (_T("Rabatt"),             m_PrGrList.PosRab, 100, LVCFMT_RIGHT);

	m_PrGrList.ColType.Add (new CColType (m_PrGrList.PosRabKz, m_PrGrList.CheckBox));

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	AkiNrGrid.Create (this, 2, 2);
    AkiNrGrid.SetBorder (0, 0);
    AkiNrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_AkiNr = new CCtrlInfo (&m_AkiNr, 0, 0, 1, 1);
	AkiNrGrid.Add (c_AkiNr);
	CtrlGrid.CreateChoiceButton (m_AkiNrChoice, IDC_AKINRCHOICE, this);
	CCtrlInfo *c_AkiNrChoice = new CCtrlInfo (&m_AkiNrChoice, 1, 0, 1, 1);
	AkiNrGrid.Add (c_AkiNrChoice);

	PrGrGrid.Create (this, 1, 2);
    PrGrGrid.SetBorder (0, 0);
    PrGrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_PrGrStuf = new CCtrlInfo (&m_PrGrStuf, 0, 0, 1, 1);
	PrGrGrid.Add (c_PrGrStuf);
	CtrlGrid.CreateChoiceButton (m_PrGrChoice, IDC_PRGRCHOICE, this);
	CCtrlInfo *c_PrGrChoice = new CCtrlInfo (&m_PrGrChoice, 1, 0, 1, 1);
	PrGrGrid.Add (c_PrGrChoice);

	ButtonGrid.Create (this, 5, 5);
    ButtonGrid.SetBorder (0, 0);
    ButtonGrid.SetCellHeight (20);
    ButtonGrid.SetGridSpace (0, 10);
	CCtrlInfo *c_Cancel = new CCtrlInfo (&m_Cancel, 0, 0, 1, 1);
	ButtonGrid.Add (c_Cancel);
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 1, 1, 1);
	ButtonGrid.Add (c_Save);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ButtonGrid.Add (c_Delete);
	CCtrlInfo *c_Insert = new CCtrlInfo (&m_Insert, 0, 3, 1, 1);
	ButtonGrid.Add (c_Insert);

	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_LAkiNr     = new CCtrlInfo (&m_LAkiNr, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LAkiNr);
	CCtrlInfo *c_AkiNrGrid   = new CCtrlInfo (&AkiNrGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_AkiNrGrid);
	CCtrlInfo *c_LZusBez     = new CCtrlInfo (&m_LZusBez, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LZusBez);
	CCtrlInfo *c_ZusBez     = new CCtrlInfo (&m_ZusBez, 1, 2, 3, 1); 
	CtrlGrid.Add (c_ZusBez);

	CCtrlInfo *c_LPrGrStuf     = new CCtrlInfo (&m_LPrGrStuf, 0, 3, 1, 1); 
	CtrlGrid.Add (c_LPrGrStuf);
	CCtrlInfo *c_PrGrGrid   = new CCtrlInfo (&PrGrGrid, 1, 3, 1, 1); 
	CtrlGrid.Add (c_PrGrGrid);
	CCtrlInfo *c_LZusBz  = new CCtrlInfo (&m_LZusBz, 0, 4, 1, 1); 
	CtrlGrid.Add (c_LZusBz);
	CCtrlInfo *c_ZusBz  = new CCtrlInfo (&m_ZusBz, 1, 4, 3, 1); 
	CtrlGrid.Add (c_ZusBz);

	CCtrlInfo *c_LAkiVon  = new CCtrlInfo (&m_LAkiVon, 0, 5, 1, 1); 
	CtrlGrid.Add (c_LAkiVon);
	CCtrlInfo *c_AkiVon  = new CCtrlInfo (&m_AkiVon, 1, 5, 1, 1); 
	CtrlGrid.Add (c_AkiVon);
	CCtrlInfo *c_LAkiBis  = new CCtrlInfo (&m_LAkiBis, 0, 6, 1, 1); 
	CtrlGrid.Add (c_LAkiBis);
	CCtrlInfo *c_AkiBis  = new CCtrlInfo (&m_AkiBis, 1, 6, 1, 1); 
	CtrlGrid.Add (c_AkiBis);

	CCtrlInfo *c_ButtonGrid = new CCtrlInfo (&ButtonGrid, DOCKRIGHT, 4, 1, 4); 
	CtrlGrid.Add (c_ButtonGrid);
	CCtrlInfo *c_PrList  = new CCtrlInfo (&m_PrGrList, 0, 8, DOCKRIGHT, DOCKBOTTOM); 
	c_PrList->rightspace = RightListSpace;
	CtrlGrid.Add (c_PrList);

	SetFont (&Font);
        CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();
	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk, sizeof (IPRGRSTUFK));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
	Form.Show ();
	ReadMdn ();
	EnableHeadControls (TRUE);
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}


void CAkiPrGrPage::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CAkiPrGrPage::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CAkiPrGrPage::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CAkiPrGrPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CAkiPrGrPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_PrGrList.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_PrGrList &&
					GetFocus ()->GetParent () != &m_PrGrList )
				{

					break;
			    }
				m_PrGrList.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				m_PrGrList.OnKeyD (VK_DOWN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				m_PrGrList.OnKeyD (VK_UP);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnChoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_AkiNr)
				{
					OnChoice ();
					return TRUE;
				}
				if (GetFocus () == &m_PrGrStuf)
				{
					OnPrGrchoice ();
					return TRUE;
				}
				m_PrGrList.OnKey9 ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CAkiPrGrPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_AkiNr)
	{
		if (!Read ())
		{
			m_AkiNr.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_PrGrStuf)
	{
		if (!ReadPrGrStuf ())
		{
			m_PrGrStuf.SetFocus ();
			return FALSE;
		}
	}

	if (Control != &m_PrGrList &&
		Control->GetParent ()!= &m_PrGrList)
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CAkiPrGrPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_PrGrList &&
		Control->GetParent ()!= &m_PrGrList )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_PrGrList.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}


BOOL CAkiPrGrPage::ReadMdn ()
{
	int mdn;

	mdn = Mdn.mdn.mdn;
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Choice != NULL && mdn != Mdn.mdn.mdn)
	{
		delete Choice;
		Choice = NULL;
	}

	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		FillPrGrStufCombo ();
		FillKunPrCombo ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CAkiPrGrPage::Read ()
{
	if (ModalChoice)
	{
		CString cAkiNr;
		m_AkiNr.GetWindowText (cAkiNr);
		if (!CStrFuncs::IsDecimal (cAkiNr))
		{
			Search = cAkiNr;
//			OnAkiNrchoice ();
			Search = "";
			if (!ChoiceStat)
			{
				m_AkiNr.SetFocus ();
				m_AkiNr.SetSel (0, -1);
				return FALSE;
			}
		}
	}
	memcpy (&Akiprgrstk.akiprgrstk, &akiprgrstk_null, sizeof (AKIPRGRSTK));
	Form.Get ();
	Akiprgrstk.akiprgrstk.mdn = Mdn.mdn.mdn;
	if (Akiprgrstk.akiprgrstk.aki_nr == 0)
	{
		MessageBox (_T("Aktionsnummer > 0 eingeben"), NULL, MB_OK | MB_ICONERROR);
		m_AkiNr.SetFocus ();
		return FALSE;
	}
	Akiprgrstp.beginwork ();
    if (Akiprgrstk.dblock () < 0)
	{
		MessageBox ("Die Aktion wird an einem anderen Arbeitsplatz bearbeitet");
		Akiprgrstp.commitwork ();
		return FALSE;
	}

    if (Akiprgrstk.dbreadfirst () == 0)
	{
		Iprgrstufk.iprgrstufk.pr_gr_stuf = Akiprgrstk.akiprgrstk.pr_gr_stuf;
		Form.Show (); 
		ReadPrGrStuf ();
	}
	EnableHeadControls (FALSE);
	ReadList ();
	return TRUE;
}

BOOL CAkiPrGrPage::ReadPrGrStuf ()
{
	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
	Form.Get ();
	strcpy (Iprgrstufk.iprgrstufk.zus_bz, ""); 
	Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
	if (Iprgrstufk.dbreadfirst () == 0)
	{
		Akiprgrstk.akiprgrstk.pr_gr_stuf = Iprgrstufk.iprgrstufk.pr_gr_stuf;
	}
	else
	{
		MessageBox (_T("Preigruppenstufe nicht gefunden"), NULL, MB_OK | MB_ICONERROR);
		return FALSE;
	}

	Form.Show ();
	return TRUE;
}

BOOL CAkiPrGrPage::ReadList ()
{
	m_PrGrList.DeleteAllItems ();
	m_PrGrList.vSelect.clear ();
	int i = 0;
	memcpy (&Akiprgrstp.akiprgrstp, &akiprgrstp_null, sizeof (AKIPRGRSTP));
	Akiprgrstp.akiprgrstp.mdn = Mdn.mdn.mdn;
	Akiprgrstp.akiprgrstp.aki_nr = Akiprgrstk.akiprgrstk.aki_nr;
	m_PrGrList.mdn = Mdn.mdn.mdn;
	m_PrGrList.pr_gr_stuf = Iprgrstufk.iprgrstufk.pr_gr_stuf; 
	Akiprgrstp.sqlopen (IprCursor);
	while (Akiprgrstp.sqlfetch (IprCursor) == 0)
	{
		Akiprgrstp.dbreadfirst ();
		memcpy (&Ipr.ipr, &ipr_null, sizeof (IPR));
		Ipr.ipr.mdn          = Mdn.mdn.mdn;
		Ipr.ipr.pr_gr_stuf   = Akiprgrstk.akiprgrstk.pr_gr_stuf;
		Ipr.ipr.kun_pr       = 0;
		Ipr.ipr.kun          = 0;
		Ipr.ipr.a		     = Akiprgrstp.akiprgrstp.a;
		Ipr.dbreadfirst ();
		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas.a_bas.a = Akiprgrstp.akiprgrstp.a;
		A_bas.dbreadfirst ();
		FillList.InsertItem (i, 0);
		CString A;
		A.Format (_T("%.0lf"), Akiprgrstp.akiprgrstp.a);
		FillList.SetItemText (A.GetBuffer (), i, m_PrGrList.PosA);
		CString ABz1;
		ABz1 = A_bas.a_bas.a_bz1;
		ABz1 += _T("   ");
		ABz1 += A_bas.a_bas.a_bz2;
		FillList.SetItemText (ABz1.GetBuffer (), i, m_PrGrList.PosABz1);
		CString VkPr;
		m_PrGrList.DoubleToString (Akiprgrstp.akiprgrstp.aki_pr_eu, VkPr, 4);
//		CDecimal *decVK = new CDecimal (VkPr);
//		delete decVK;
		Akiprgrstp.akiprgrstp.aki_pr_eu = CStrFuncs::StrToDouble (VkPr);
		FillList.SetItemText (VkPr.GetBuffer (), i, m_PrGrList.PosVkPr);
		CString LdPr;
		m_PrGrList.DoubleToString (Akiprgrstp.akiprgrstp.ld_pr_eu, LdPr, 2);
		FillList.SetItemText (LdPr.GetBuffer (), i, m_PrGrList.PosLdPr);

		m_PrGrList.DoubleToString (Ipr.ipr.vk_pr_eu, VkPr, 4);
		FillList.SetItemText (VkPr.GetBuffer (), i, m_PrGrList.PosVkPr + 2);
		m_PrGrList.DoubleToString (Ipr.ipr.ld_pr_eu, LdPr, 2);
		FillList.SetItemText (LdPr.GetBuffer (), i, m_PrGrList.PosLdPr + 2);

		if (Akiprgrstp.akiprgrstp.sa_kz)
		{
			FillList.SetItemText ("X", i, m_PrGrList.PosRabKz);
		}
		else
		{
			FillList.SetItemText (" ", i, m_PrGrList.PosRabKz);
		}

		CString Rab;
		m_PrGrList.DoubleToString (Akiprgrstp.akiprgrstp.sa_rab, Rab,2);
		FillList.SetItemText (Rab.GetBuffer (), i, m_PrGrList.PosRab);

		CAkiPreise *aki_pr = new CAkiPreise (VkPr, LdPr, Akiprgrstp.akiprgrstp);
		DbRows.Add (aki_pr);
		i ++;
	}
	return TRUE;
}

BOOL CAkiPrGrPage::IsChanged (CAkiPreise *pIpr)
{
	DbRows.FirstPosition ();
	CAkiPreise *aki_pr;
	while ((aki_pr = (CAkiPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Akiprgrstp.akiprgrstp, &pIpr->akiprgrstp, sizeof (AKIPRGRSTP));
		if (Akiprgrstp.akiprgrstp.aki_nr == aki_pr->akiprgrstp.aki_nr &&
			Akiprgrstp.akiprgrstp.a == aki_pr->akiprgrstp.a) break;
	}
	if (aki_pr == NULL)
	{
		return TRUE;
	}
	if (pIpr->cEk != aki_pr->cEk) return TRUE;
	if (pIpr->cVk != aki_pr->cVk) return TRUE;
	return FALSE;
}

BOOL CAkiPrGrPage::InList (AKIPRGRSTP_CLASS& Akiprgrstp)
{
	ListRows.FirstPosition ();
	CAkiPreise *aki_pr;
	while ((aki_pr = (CAkiPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Akiprgrstp.akiprgrstp.aki_nr == aki_pr->akiprgrstp.aki_nr &&
			Akiprgrstp.akiprgrstp.a == aki_pr->akiprgrstp.a) return TRUE;
	}
    return FALSE;
}

void CAkiPrGrPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CAkiPreise *aki_pr;
	while ((aki_pr = (CAkiPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Akiprgrstp.akiprgrstp, &aki_pr->akiprgrstp, sizeof (AKIPRGRSTP));
		if (!InList (Akiprgrstp))
		{
			Akiprgrstp.dbdelete ();
//			PgrProt.Write (1);
		}
	}
}

BOOL CAkiPrGrPage::Write ()
{
	extern short sql_mode;
	short sql_s;

	if (!TerminOK ())
	{
		m_PrGrList.StopEnter ();
//		EnableHeadControls (TRUE);
		m_AkiVon.SetFocus ();
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
//	Akiprgrstp.beginwork ();
	m_PrGrList.StopEnter ();
	int count = m_PrGrList.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 AKIPRGRSTP *akiprgrstp = new AKIPRGRSTP;
		 memcpy (akiprgrstp, &akiprgrstp_null, sizeof (AKIPRGRSTP));
         CString Text;
		 Text = m_PrGrList.GetItemText (i, m_PrGrList.PosA);
		 akiprgrstp->a = CStrFuncs::StrToDouble (Text);
     	 CString VkPr =  m_PrGrList.GetItemText (i, m_PrGrList.PosVkPr);
		 akiprgrstp->aki_pr_eu = CStrFuncs::StrToDouble (VkPr);
		 akiprgrstp->aki_pr = akiprgrstp->aki_pr_eu;
		 CString LdPr =  m_PrGrList.GetItemText (i, m_PrGrList.PosLdPr);
		 akiprgrstp->ld_pr_eu = CStrFuncs::StrToDouble (LdPr);
		 akiprgrstp->ld_pr = akiprgrstp->ld_pr_eu;
         CString RabKz = m_PrGrList.GetItemText (i, m_PrGrList.PosRabKz);
		 if (RabKz == _T("X"))
		 {
			 akiprgrstp->sa_kz = 1;
		 }
		 else
		 {
			 akiprgrstp->sa_kz = 0;
		 }
		 CString Rab =  m_PrGrList.GetItemText (i, m_PrGrList.PosRab);
		 akiprgrstp->sa_rab = CStrFuncs::StrToDouble (Rab);

		 akiprgrstp->mdn = Mdn.mdn.mdn; 
		 akiprgrstp->aki_nr = Akiprgrstk.akiprgrstk.aki_nr; 
		 CAkiPreise *pr = new CAkiPreise (VkPr, LdPr, *akiprgrstp);
		 if (akiprgrstp->aki_pr_eu != 0.0 || 
			 akiprgrstp->ld_pr_eu != 0.0)
		 {
				ListRows.Add (pr);
		 }
		 delete akiprgrstp;
	}

	Akiprgrstp.sqlexecute (IprDelCursor);
	DeleteDbRows ();

	ListRows.FirstPosition ();
	CAkiPreise *aki_pr;
	while ((aki_pr = (CAkiPreise *) ListRows.GetNext ()) != NULL)
	{
		memcpy (&Akiprgrstp.akiprgrstp, &aki_pr->akiprgrstp, sizeof (AKIPRGRSTP));
		Akiprgrstp.dbupdate ();
/*
		if (IsChanged (iv_pr))
		{
			PgrProt.Write ();
		}
*/
	}
	EnableHeadControls (TRUE);
	m_AkiNr.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	Akiprgrstk.akiprgrstk.mdn = Mdn.mdn.mdn;
	Akiprgrstk.dbupdate ();
	Akiprgrstp.commitwork ();
	sql_mode = sql_s;
	if (Choice != NULL)
	{
		Choice->FillList ();
	}
	return TRUE;
}

BOOL CAkiPrGrPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_AkiNr.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Aktion komplett l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
//	Akiprgrstp.beginwork ();
	m_PrGrList.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	m_PrGrList.DeleteAllItems ();
	Form.Get ();
	Akiprgrstk.dbdelete ();

	memcpy (&Akiprgrstk.akiprgrstk, &akiprgrstk_null, sizeof (AKIPRGRSTK));
	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
	Form.Show ();
	EnableHeadControls (TRUE);
	m_AkiNr.SetFocus ();
	Akiprgrstp.commitwork ();
	sql_mode = sql_s;
	if (Choice != NULL)
	{
		Choice->FillList ();
	}
	return TRUE;
}


void CAkiPrGrPage::OnPrGrchoice ()
{
	Form.Get ();
	if (ChoicePrGrStuf != NULL && !ModalChoicePrGrStuf)
	{
		ChoicePrGrStuf->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoicePrGrStuf == NULL)
	{
		ChoicePrGrStuf = new CChoicePrGrStuf (this);
	    ChoicePrGrStuf->IsModal = ModalChoicePrGrStuf;
	    ChoicePrGrStuf->m_Mdn = Mdn.mdn.mdn;
		ChoicePrGrStuf->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoicePrGrStuf->SetDbClass (&A_bas);
	ChoicePrGrStuf->SearchText = Search;
	if (ModalChoicePrGrStuf)
	{
			ChoicePrGrStuf->DoModal();
	}
	else
	{

		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoicePrGrStuf->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
/*
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		cy -= 100;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
*/
		ChoicePrGrStuf->MoveWindow (&rect);
		ChoicePrGrStuf->SetFocus ();

		return;
	}
    if (ChoicePrGrStuf->GetState ())
    {
		  CPrGrStufList *abl = ChoicePrGrStuf->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
          Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
          Iprgrstufk.iprgrstufk.pr_gr_stuf = abl->pr_gr_stuf;
		  if (Iprgrstufk.dbreadfirst () == 0)
		  {
			Akiprgrstk.akiprgrstk.mdn = Mdn.mdn.mdn;
			Akiprgrstk.akiprgrstk.pr_gr_stuf = abl->pr_gr_stuf;
            Akiprgrstk.dbreadfirst ();
			Form.Show ();
			EnableHeadControls (TRUE);
//			m_ZusBz.SetFocus ();
			m_AkiVon.SetFocus ();
			if (Search == "")
			{
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			}
		  }
    }
}

void CAkiPrGrPage::OnChoice ()
{
    ChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceAkiPrGrStuf (this);
	    Choice->IsModal = ModalChoice;
	    Choice->m_Mdn = Mdn.mdn.mdn;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->SearchText = Search;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{

		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
		Choice->MoveWindow (&rect);
		Choice->SetFocus ();

		return;
	}
    if (Choice->GetState ())
    {
		  CAkiPrGrStufkList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  Akiprgrstk.akiprgrstk.mdn = Mdn.mdn.mdn;
#ifdef SHORTAKI
		  Akiprgrstk.akiprgrstk.aki_nr = (short) abl->aki_nr;
#else
		  Akiprgrstk.akiprgrstk.aki_nr = abl->aki_nr;
#endif
          Akiprgrstk.dbreadfirst ();
 		  Form.Show ();
		  EnableHeadControls (TRUE);
 		  m_AkiVon.SetFocus ();
		  if (Search == "")
		  {
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}

void CAkiPrGrPage::OnSelected ()
{
	if (Choice == NULL) return;
    CAkiPrGrStufkList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    Akiprgrstk.akiprgrstk.mdn = Mdn.mdn.mdn;
#ifdef SHORTAKI
    Akiprgrstk.akiprgrstk.aki_nr = (short) abl->aki_nr;
#else
    Akiprgrstk.akiprgrstk.aki_nr = abl->aki_nr;
#endif
    if (Akiprgrstk.dbreadfirst () == 0)
	{
		Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
		Iprgrstufk.iprgrstufk.pr_gr_stuf = abl->pr_gr_stuf;
        Iprgrstufk.dbreadfirst ();
		m_AkiNr.EnableWindow (TRUE);
		m_AkiNr.SetFocus ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	if (CloseChoice)
	{
		OnCanceled (); 
	}
    Form.Show ();
	if (Choice->FocusBack)
	{
		DATE_STRUCT sDateVon;
		DATE_STRUCT sDateBis;
		memcpy (&sDateVon, &Akiprgrstk.akiprgrstk.aki_von,
				sizeof (DATE_STRUCT));
		memcpy (&sDateBis, &Akiprgrstk.akiprgrstk.aki_bis,
				sizeof (DATE_STRUCT));
		Read ();
		ReadList ();
		memcpy (&Akiprgrstk.akiprgrstk.aki_von,&sDateVon, 
				sizeof (DATE_STRUCT));
		memcpy (&Akiprgrstk.akiprgrstk.aki_bis,&sDateBis, 
				sizeof (DATE_STRUCT));
		Form.Show ();
		m_ZusBez.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CAkiPrGrPage::OnCanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CAkiPrGrPage::StepBack ()
{
	if (m_AkiNr.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		m_PrGrList.StopEnter ();
		EnableHeadControls (TRUE);
		m_AkiNr.SetFocus ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
		m_PrGrList.DeleteAllItems ();
	}
	return TRUE;
}

void CAkiPrGrPage::OnCancel ()
{
	StepBack ();
}

void CAkiPrGrPage::OnSave ()
{
	Write ();
}

void CAkiPrGrPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
		  if (Choice != NULL && abl->mdn != Mdn.mdn.mdn)
	  	  {
			delete Choice;
			Choice = NULL;
		  }
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CAkiPrGrPage::FillPrGrStufCombo ()
{
}

void CAkiPrGrPage::FillKunPrCombo ()
{
}

void CAkiPrGrPage::OnDelete ()
{
	if (m_AkiNr.IsWindowEnabled ())
	{
		Form.Get ();

		if (Akiprgrstk.akiprgrstk.aki_nr == 0)
		{
			return;
		}

		CString Message;
		Message.Format (_T("Aktion %ld l�schen"), Akiprgrstk.akiprgrstk.aki_nr);

		int ret = MessageBox (Message, NULL, 
			MB_YESNO | MB_DEFBUTTON2 | MB_ICONQUESTION);
		if (ret == IDYES)
		{
			Akiprgrstk.dbdelete ();
			m_PrGrStuf.SetFocus ();
 			memcpy (&Akiprgrstk.akiprgrstk, &akiprgrstk_null, sizeof (AKIPRGRSTK));
			Form.Show ();
			DestroyRows (DbRows);
			DestroyRows (ListRows);
			m_PrGrList.DeleteAllItems ();
			Akiprgrstp.sqlexecute (IprDelCursor);
		}
		m_AkiNr.SetFocus ();
		m_AkiNr.SetSel (0, -1);
		if (Choice != NULL)
		{
			Choice->FillList ();
		}
		return;
	}
	m_PrGrList.DeleteRow ();
}

void CAkiPrGrPage::OnInsert ()
{
	m_PrGrList.InsertRow ();
}

BOOL CAkiPrGrPage::Print ()
{
	CProcess print;
	Form.Get ();
// Iprgrstufk.iprgrstufk.pr_gr_stuf
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11123.llf", tmp);
	}
	else
	{
		dName = "11123.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11123\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "aki_nr %ld %ld\n", Akiprgrstk.akiprgrstk.aki_nr,
			                                 Akiprgrstk.akiprgrstk.aki_nr);
		fclose (fp);
		Command.Format ("dr70001 -name 11123 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11123";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

BOOL CAkiPrGrPage::PrintAll ()
{
	CProcess print;
	Form.Get ();
// Iprgrstufk.iprgrstufk.pr_gr_stuf
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11123.llf", tmp);
	}
	else
	{
		dName = "11123.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11123\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "MITRANGE 1\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "aki_nr %ld %ld\n", (long) 0,
			                             (long) 99999999);
		fclose (fp);
		Command.Format ("dr70001 -name 11123 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11123";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CAkiPrGrPage::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_PrGrList.StartPauseEnter ();
}

void CAkiPrGrPage::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_PrGrList.EndPauseEnter ();
}

void CAkiPrGrPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Enable (enable);
	PosControls.Enable (!enable);
}

void CAkiPrGrPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CAkiPreise *aki_pr;
	while ((aki_pr = (CAkiPreise *) Rows.GetNext ()) != NULL)
	{
		delete aki_pr;
	}
	Rows.Init ();
}

void CAkiPrGrPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_PrGrList.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("RemoveKun", cfg_v) == TRUE)
    {
			RemoveKun = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_PrGrList.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_PrGrList.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_PrGrList.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CAkiPrGrPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_PrGrList.ListEdit ||
        Control == &m_PrGrList.ListComboBox ||
		Control == &m_PrGrList.SearchListCtrl.Edit)	
	{
		ListCopy ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CAkiPrGrPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CAkiPrGrPage::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_PrGrList.ListEdit.m_hWnd) ||
		IsWindow (m_PrGrList.ListComboBox) ||
		IsWindow (m_PrGrList.SearchListCtrl.Edit))
	{
		m_PrGrList.StopEnter ();
		m_PrGrList.StartEnter (m_PrGrList.EditCol, m_PrGrList.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
		for (int i = 0; i < m_PrGrList.GetItemCount (); i ++)
		{
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = "";
			int cols = m_PrGrList.GetHeaderCtrl ()->GetItemCount ();
			for (int j = 0; j < cols; j ++)
			{
				if (Row != "")
				{
					Row += Separator;
				}
				CString Field = m_PrGrList.GetItemText (i, j);
				Field.TrimRight ();
				Row += Field; 
			}
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

BOOL CAkiPrGrPage::TerminOK ()
{
	DbTime SysDate;

	Form.Get ();

	DbTime AkiVon (&Akiprgrstk.akiprgrstk.aki_von);
	DbTime AkiBis (&Akiprgrstk.akiprgrstk.aki_bis);

/*
	if (AkiVon < SysDate)
	{
		MessageBox (_T("Das Startdatum ist kleiner als das Tagesdatum"),
			        NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}
*/
	if (AkiBis < AkiVon)
	{
		MessageBox (_T("Das Enddatum ist kleiner als das Startdatum"),
			        NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}
	if (AkiBis < SysDate)
	{
		MessageBox (_T("Das Enddatum ist kleiner als das Tagesdatum"),
			        NULL,
					MB_OK | MB_ICONERROR);
		return FALSE;
	}
	return TRUE;
}