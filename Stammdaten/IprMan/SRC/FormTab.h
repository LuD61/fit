#pragma once
#ifndef _FORMTAB_DEF
#define _FORMTAB_DEF
#include "vector.h"
// #include "OemFormField.h"
#include "FormField.h"
#include <vector>

class CFormTab :
	public CVector
{
public:
	static CFormTab *v;
	CFormTab(void);
	~CFormTab(void);
	void Show ();
	void Get ();
	CFormField *GetFormField (CWnd*);
};
#endif