// DialogTestDlg.h : Headerdatei
//

#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "KunPrListCtrl.h"
#include "PrArtPageConst.h"
#include "PrGrPageConst.h"
#include "PrKunPageConst.h"
#include "KunPageConst.h"
#include "FillList.h"
#include "a_bas.h"
#include "mdn.h"
#include "adr.h"
#include "Ipr.h"
#include "PGrProt.h"
#include "Iprgrstufk.h"
#include "I_kun_prk.h"
#include "Kun.h"
#include "ChoicePrGrStuf.h"
#include "ChoiceIKunPr.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "FormTab.h"
#include "mo_progcfg.h"
#include "Controls.h"
#include "Preise.h"

#ifndef IDC_KUNCHOICE
#define IDC_KUNCHOICE 4002
#endif
#ifndef IDC_KUNPRCHOICE
#define IDC_KUNPRCHOICE 4001
#endif
#ifndef IDC_PRGRCHOICE
#define IDC_PRGRCHOICE 4000
#endif
#ifndef IDC_MDNCHOICE
#define IDC_MDNCHOICE 3001
#endif

// CPrArtPreise Dialogfeld
class CKunPage : public CDbPropertyPage
{
// Konstruktion
public:
	CKunPage(CWnd* pParent = NULL);	// Standardkonstruktor
	CKunPage(UINT nIDTemplate);	// Standardkonstruktor

	~CKunPage ();

// Dialogfelddaten
//	enum { IDD = IDD_DIALOGTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung
    virtual BOOL PreTranslateMessage(MSG* pMsg);


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public :
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	BOOL RemoveKun;
	int StartSize;

	CControls HeadControls;
	CControls PosControls;
	CControls ButtonControls;
	BOOL HideButtons;
	int RightListSpace;
	CVector DbRows;
	CVector ListRows;
	int IprCursor;
	int IKunPrkCursor;
	int IprDelCursor;
	CWnd *Frame;
	CCtrlGrid CtrlGrid;
	CCtrlGrid KunGrid;
	CCtrlGrid KunPrGrid;
	CCtrlGrid PrGrGrid;
	CCtrlGrid MdnGrid;
	CButton m_MdnChoice;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CNumEdit m_MdnName;
	CButton m_KunChoice;
	CButton m_KunPrChoice;
	CButton m_PrGrChoice;
	CStatic m_LKun;
	CNumEdit m_Kun;
	CEdit m_LKunKrz1;
	CTextEdit m_KunKrz1;
	CStatic m_LKunPr;
	CEdit m_KunPr;
	CEdit m_LKunZusBz;
	CTextEdit m_KunZusBz;
	CStatic m_LPrGrStuf;
	CEdit m_PrGrStuf;
	CEdit m_LZusBz;
	CEdit m_ZusBz;
	CButton m_AddEk;
	CFillList FillList;
	CKunPrListCtrl m_KunPrList;
	CButton m_Cancel;
	CButton m_Save;
	CButton m_Delete;
	CButton m_Insert;
	int RighListSpace;
	CCtrlGrid ButtonGrid;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	A_BAS_CLASS A_bas;
	IPR_CLASS Ipr;
	CPGrProt PgrProt;
	PRG_PROT_CLASS Prg_prot;
	IPRGRSTUFK_CLASS Iprgrstufk;
	I_KUN_PRK_CLASS I_kun_prk;
	KUN_CLASS Kun;
	ADR_CLASS KunAdr;
	CFormTab Form;
	CChoiceKun *Choice;
	CChoiceIKunPr *ChoiceKunPr;
	BOOL ModalChoice;
	BOOL CloseChoice;
	BOOL ChoiceStat;
	CString Search;
	CChoicePrGrStuf *ChoicePrGrStuf;
	BOOL ModalChoicePrGrStuf;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CFont Font;
	CFont lFont;
	CString PersName;
	CString Separator;
    CImageList image; 
	int CellHeight;

	virtual void OnSize (UINT, int, int);
	void OnKunchoice ();
	void OnKunPrchoice ();
	void OnPrGrchoice ();
    void OnKunSelected ();
    void OnKunCanceled ();
    void OnMdnchoice(); 
	virtual BOOL Read ();
	virtual BOOL read ();
	virtual BOOL Write ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	virtual BOOL StepBack ();
	virtual void OnCancel ();
	virtual void OnSave ();
	virtual void OnAddEk ();
	BOOL ReadMdn ();
	BOOL ReadPrGrStuf ();
	BOOL ReadList ();
    void FillPrGrStufCombo ();
    void FillKunPrCombo ();
	virtual BOOL DeleteAll ();
	virtual BOOL Print ();
	virtual BOOL PrintAll ();
	virtual void OnDelete ();
	virtual void OnInsert ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult);
    void EnableHeadControls (BOOL enable);
    void WriteRow (int row);
    void DestroyRows(CVector &Rows);
    void DeleteDbRows ();
    BOOL IsChanged (CPreise* Ipr);
    BOOL InList (IPR_CLASS& Ipr);
	void ReadCfg ();
    void ListCopy ();
	BOOL TestDecValues (IPR *ipr);
};
