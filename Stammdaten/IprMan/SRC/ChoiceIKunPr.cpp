#include "stdafx.h"
#include "ChoiceIKunPr.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceIKunPr::Sort1 = -1;
int CChoiceIKunPr::Sort2 = -1;
int CChoiceIKunPr::Sort3 = -1;
int CChoiceIKunPr::Sort4 = -1;

CChoiceIKunPr::CChoiceIKunPr(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_Mdn = 0;
	Rows = NULL;
}

CChoiceIKunPr::~CChoiceIKunPr() 
{
	DestroyList ();
}

void CChoiceIKunPr::DestroyList() 
{
	for (std::vector<CIKunPrList *>::iterator pabl = IKunPrList.begin (); pabl != IKunPrList.end (); ++pabl)
	{
		CIKunPrList *abl = *pabl;
		delete abl;
	}
    IKunPrList.clear ();
}

void CChoiceIKunPr::FillList () 
{
    short  mdn;
	long kun_pr;
    TCHAR zus_bz [34];
	int cursor;

	DestroyList ();
	CListCtrl *listView = GetListView ();
	listView->DeleteAllItems ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Kunden-Preislisten"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Listennummer"), 1, 80, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),  2, 250);

	if (IKunPrList.size () == 0 && Rows == NULL)
	{
		if (m_Mdn != 0)
		{
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((long *)&kun_pr,   SQLLONG, 0);
			DbClass->sqlout ((LPTSTR)  zus_bz, SQLCHAR, sizeof (zus_bz));
			DbClass->sqlin ((LPTSTR)  &m_Mdn,   SQLSHORT, 0);
			cursor = DbClass->sqlcursor (_T("select mdn, kun_pr, zus_bz ")
				                             _T("from i_kun_prk where mdn = ? and kun_pr >= 0 "));
		}
		else
		{
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((long *)&kun_pr,      SQLLONG, 0);
			DbClass->sqlout ((LPTSTR)  zus_bz, SQLCHAR, sizeof (zus_bz));
			cursor = DbClass->sqlcursor (_T("select mdn, kun_pr, zus_bz ")
				                             _T("from i_kun_prk where kun_pr >= 0 "));
		}
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) zus_bz;
			CDbUniCode::DbToUniCode (zus_bz, pos);
			CIKunPrList *abl = new CIKunPrList (mdn, kun_pr, zus_bz);
			IKunPrList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}
	else if (IKunPrList.size () == 0 && Rows != NULL)
	{
		mdn = (short) m_Mdn;
		Rows->FirstPosition ();
		CString *c;
		while ((c = (CString *) Rows->GetNext ()) != NULL)
		{
			int pos = 0;
			CString KunPr = c->Tokenize (" ", pos);
			if (KunPr != "")
			{
				kun_pr = atol (KunPr.GetBuffer ());
                int len = KunPr.GetLength ();
				CString ZusBz = KunPr.Mid (len + 1, -1);
				ZusBz.Trim ();
				strcpy (zus_bz, ZusBz.GetBuffer ());
			    CIKunPrList *abl = new CIKunPrList (mdn, kun_pr, zus_bz);
			    IKunPrList.push_back (abl);
			}
		}

	}

	for (std::vector<CIKunPrList *>::iterator pabl = IKunPrList.begin (); pabl != IKunPrList.end (); ++pabl)
	{
		CIKunPrList *abl = *pabl;
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString KunPr;
		KunPr.Format (_T("%ld"), abl->kun_pr); 
		CString Num;
		CString Bez;
		_tcscpy (zus_bz, abl->zus_bz.GetBuffer ());

		CString LText;
		LText.Format (_T("%ld %s"), abl->kun_pr, 
									abl->zus_bz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = KunPr;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (zus_bz, i, 2);
        i ++;
    }

	SortRow = 2;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceIKunPr::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceIKunPr::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceIKunPr::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceIKunPr::SearchZusBz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();
		CString cSearch = Search;
		cSearch.MakeUpper ();

        if (_tmemcmp (cSearch.GetBuffer (), iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceIKunPr::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             SearchZusBz (ListBox, EditText.GetBuffer (8));
             break;
    }
}

int CChoiceIKunPr::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceIKunPr::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
    CString cItem1 = strItem1;
    CString cItem2 = strItem2;
	cItem1.MakeUpper ();
	cItem2.MakeUpper ();
//	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
	return (cItem2.CompareNoCase (cItem1)) * Sort3;
   }
   return 0;
}


void CChoiceIKunPr::Sort (CListCtrl *ListBox)
{
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CIKunPrList *abl = IKunPrList [i];
		   
		   abl->kun_pr     = _tstol (ListBox->GetItemText (i, 1));
		   abl->zus_bz     = ListBox->GetItemText (i, 2);
	}
}

void CChoiceIKunPr::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = IKunPrList [idx];
}

CIKunPrList *CChoiceIKunPr::GetSelectedText ()
{
	CIKunPrList *abl = (CIKunPrList *) SelectedRow;
	return abl;
}

