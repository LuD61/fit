#include "stdafx.h"
#include "resource.h"
#include "ToolButton.h"
#include "Bmap.h"

CToolButton::CToolButton ()
{
      startx = 8;
      tbx = 8;
      tby = 8;

      bitmapcx = 23;
      bitmapcy = 23;

      vertsepcx = 8;
      vertsepcy = 23;

      horzyup = 4;
      horzydown = 35;
      horzcx = 632;
      horzcy = 2;

      spacecx = 5;
      wspacecx = 10;
	  Toolbar = NULL;
}

CToolButton::~CToolButton ()
{
	if (Toolbar != NULL)
	{
		Toolbar->FirstPosition ();
		CToolbarItem *Tb;
		while ((Tb = (CToolbarItem *) Toolbar->GetNext ()) != NULL)
		{
			delete Tb;
		}
		Toolbar = NULL;
	}
}

void CToolButton::SetSpace (int spacecx)
{
    this->spacecx = spacecx;
}

void CToolButton::SetWSpace (int wspacecx)
{
    this->wspacecx = wspacecx;
}

void CToolButton::Create (CWnd *Parent)
{
    HBITMAP Hb = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F5", "F5MASK");
    CToolbarItem *Tb =  new CToolbarItem (IDC_F5, Hb, Parent); 
    Tb->SetTooltipText ("abbrechen");
    StdToolbar.Add (Tb); 

    Hb = LoadBitmap (AfxGetApp()->m_hInstance, "F12");
    Tb = new CToolbarItem (IDC_F12, Hb, Parent); 
    Tb->SetTooltipText ("speichern");
    StdToolbar.Add (Tb); 

    StdToolbar.Add (new CToolbarItem (IDC_SEPERATOR, VERT, Parent)); 

    Hb = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "DEL", "DELMASK");
    Tb = new CToolbarItem (IDC_F7, Hb, Parent); 
    Tb->SetTooltipText ("l�schen");
    StdToolbar.Add (Tb); 

    Hb = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "Print", "PrintMASK");
    Tb = new CToolbarItem (IDC_PRINT2, Hb, Parent); 
    Tb->SetTooltipText ("drucken");
    StdToolbar.Add (Tb); 

    StdToolbar.Add (new CToolbarItem (IDC_LINE, TBDOWN, Parent)); 
    StdToolbar.Add (new CToolbarItem (IDC_LINE, TBUP, Parent)); 
    Create (&StdToolbar);
    Toolbar = &StdToolbar;
}


void CToolButton::Create (CVector *Toolbar)
{
    int x, y, cx, cy;

    CToolbarItem * Tb;
    Toolbar->FirstPosition ();
    x = startx;
    while ((Tb = (CToolbarItem *) Toolbar->GetNext ()) != NULL)
    {
        int Seperator = Tb->GetSeperator ();
        if (Seperator == 0)
        {
            x = tbx;
            y = tby;
            cx = bitmapcx;
            cy = bitmapcy;
            tbx += (cx + spacecx);
        }
        else if (Seperator == TBTEXT) 
        {
            x = tbx;
            y = tby;
            CDC *cDC = Tb->GetParent ()->GetDC ();
            CFont *Font = SetFont (cDC);
            CFont *oldFont = cDC->SelectObject (Font);
            CSize Size = cDC->GetTextExtent (Tb->GetText ());
            cDC->SelectObject (oldFont);
            delete Font;

            cx = Size.cx + 10;
            cy = Size.cy + 2;
            y += max (0, (vertsepcy - Size.cy) / 2);
            tbx += (cx + spacecx);
        }
        else if (Seperator == VERT) 
        {
            x = tbx;
            y = tby;
            cx = vertsepcx;
            cy = vertsepcy;
            tbx += (cx + spacecx);
        }
        else if (Seperator == TBDOWN)
        {
            x  = startx;
            y  = horzydown;
            cx = GetCX (Tb);
            cy = horzcy;
        }
        else if (Seperator == TBUP)
        {
            x  = startx;
            y  = horzyup;
            cx = GetCX (Tb);
            cy = horzcy;
        }
        else if (Seperator == SPACE)
        {
            tbx += wspacecx;
        }

        if (Seperator != SPACE)
        {
            Tb->CreateButton (x, y, cx, cy);
        }
    }
}

void CToolButton::Add (CToolbarItem *Tb)
{
    int x,y, cx, cy;

    Toolbar->Add (Tb);  
    int Seperator = Tb->GetSeperator ();
    if (Seperator == 0)
    {
           CPoint p;
           Tb->GetHbSize (&p);
           x = tbx;
           y = tby;
           cx = bitmapcx;
           cy = bitmapcy;
           if (p.x > cx) cx = p.x;
           if (p.y > cy) cy = p.y;
           tbx += (cx + spacecx);
    }
    else if (Seperator == TBTEXT) 
    {
            x = tbx;
            y = tby;
            CDC *cDC = Tb->GetParent ()->GetDC ();
            CFont *Font = SetFont (cDC);
            CFont *oldFont = cDC->SelectObject (Font);
            CSize Size = cDC->GetTextExtent (Tb->GetText ());
            cDC->SelectObject (oldFont);
            delete Font;

            cx = Size.cx + 10;
            cy = Size.cy + 2;
            y += max (0, (vertsepcy - Size.cy) / 2);
            tbx += (cx + spacecx);
    }
    else if (Seperator == VERT) 
    {
            x = tbx;
            y = tby;
            cx = vertsepcx;
            cy = vertsepcy;
            tbx += (cx + spacecx);
     }
     else if (Seperator == TBDOWN)
     {
            x  = startx;
            y  = horzydown;
            cx = horzcx;
            cy = horzcy;
     }
     else if (Seperator == TBUP)
     {
            x  = startx;
            y  = horzyup;
            cx = horzcx;
            cy = horzcy;
     }
     else if (Seperator == SPACE)
     {
            tbx += wspacecx;
     }

     if (Seperator != SPACE)
     {
            Tb->CreateButton (x, y, cx, cy);
     }
}


BOOL CToolButton::DrawToolbar (int Id, HDC hDC)
{
    CToolbarItem * Tb;
    Toolbar->FirstPosition ();
   
    while ((Tb = (CToolbarItem *) Toolbar->GetNext ()) != NULL)
    {
        if (Tb->IsCtrl (Id))
        {
            Tb->CentItem (hDC);
            return TRUE;
        }
    }
    return FALSE;
}


BOOL CToolButton::OnToolTipNotify(int nIDCtl,NMHDR *pNMHDR, LRESULT *pResult)
{
     
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *) pNMHDR;

    UINT nID = (UINT) pNMHDR->idFrom;
    if (pTTT->uFlags & TTF_IDISHWND)
    {
        CToolbarItem * Tb;
        Toolbar->FirstPosition ();
   
        while ((Tb = (CToolbarItem *) Toolbar->GetNext ()) != NULL)
        {
             if (Tb->IsTooltipCtrl ((HWND) nID, pTTT))
             {
                     return TRUE;
             }
        }
    }
    return FALSE;
}

CFont *CToolButton::SetFont (CDC *pDC)
{
    CFont *Font;

    char *FontName = "Arial";
    int size = 15;
    int attribute = 0;

    Font = new CFont ();
    Font->CreateFont(size, 0, 0, 0, attribute, 0, 0, 0, 0, 0, 0, 0, 0, 
                     FontName);
    return Font;
}

int CToolButton::GetCX (CToolbarItem *Tb)
{
    CRect Rect;

    int cx = horzcx;

    CWnd *Parent = Tb->GetParent ();
    Parent->GetClientRect (&Rect);
    cx = Rect.right - 2 * startx;
    return cx; 
}

