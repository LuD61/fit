#ifndef _AKI_GR_STUF_LIST_DEF
#define _AKI_GR_STUF_LIST_DEF
#pragma once

class CAkiPrGrStufkList
{
public:
	short mdn;
    long aki_nr;
	long pr_gr_stuf;
	CString zus_bz;
	CAkiPrGrStufkList(void);
	CAkiPrGrStufkList(short, long, long, LPTSTR);
	~CAkiPrGrStufkList(void);
};
#endif
