// IprManDoc.h : Schnittstelle der Klasse CIprManDoc
//


#pragma once

class CIprManDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CIprManDoc();
	DECLARE_DYNCREATE(CIprManDoc)

// Attribute
public:

// Operationen
public:

// Überschreibungen
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CIprManDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


