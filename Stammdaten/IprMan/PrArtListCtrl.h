#pragma once
#include <vector>
#include "editlistctrl.h"
#include "Kun.h"
#include "Adr.h"
#include "I_kun_prk.h"
#include "Iprgrstufk.h"
#include "ChoiceKun.h"
#include "ChoiceIKunPr.h"
#include "ChoicePrGrStuf.h"
#include "Preise.h"	// 301111

#define MAXLISTROWS 30

class CPrArtListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
	};

	enum LISTPOS
	{
		POSGRPRSTUF = 1,
		POSKUNPR    = 2,
		POSKUN      = 3,
		POSKUNNAME  = 4,
		POSVKPR     = 5,
		POSLDPR     = 6,
		POSVKPRKALK = 7,
		POSEKABS  = 8,
		POSEKPROZ = 9,
		POSSK     = 10,
		POSSP     = 11,
	};

	int PosPrGrStuf;
    int PosKunPr;
	int PosKun;
	int PosKunName;
	int PosVkPr;
	int PosLdPr;
	int PosVkPrKalk;
	int PosEkAbs;
	int PosEkProz;
	int PosSk;
	int PosSp;

    int *Position[12];

	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	short m_Mdn;
	int oldsel;
	std::vector<BOOL> vSelect;
	CVector PrGrStufCombo;
	CVector KunPrCombo;
	CChoiceKun *ChoiceKun;
	BOOL ModalChoiceKun;
	BOOL KunChoiceStat;
	CChoiceIKunPr *ChoiceIKunPr;
	BOOL ModalChoiceIKunPr;
	BOOL IKunPrChoiceStat;
	CChoicePrGrStuf *ChoicePrGrStuf;
	BOOL ModalChoicePrGrStuf;
	BOOL PrGrStufChoiceStat;
	CVector ListRows;

	KUN_CLASS Kun;
	ADR_CLASS KunAdr;
	I_KUN_PRK_CLASS I_kun_prk;
	IPRGRSTUFK_CLASS Iprgrstufk;
	double sk_vollk;
	double spanne;
	double fil_ek_vollk;
	double fil_vk_vollk;

	CPrArtListCtrl(void);
	~CPrArtListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	void FillPrGrStufCombo (CVector&);
	void FillKunPrCombo (CVector&);
	void OnChoice ();
	void OnKunChoice (CString &);
    void OnIKunPrChoice (CString& Search);
    void OnPrGrStufChoice (CString& Search);
	void OnKey9 ();
    void ReadKunName ();
    void ReadKunPr ();
    void ReadPrGrStuf ();
    void GetColValue (int row, int col, CString& Text);
    void TestIprIndex ();
	void ScrollPositions (int pos);
	void GrundArtikelpreise (CPreise *p ,int EditRow, int EditCol );	// 301111

	BOOL LastCol ();
};
