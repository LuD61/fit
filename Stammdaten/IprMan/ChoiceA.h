#ifndef _CHOICEA_DEF
#define _CHOICEA_DEF

#include "ChoiceX.h"
#include "ABasList.h"
#include "Ag.h"
#include <vector>

class CChoiceA : public CChoiceX
{
    protected :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
        static int Sort7;
        static int Sort8;
        static int Sort9;
        static int Sort10;
        static int Sort11;
		int ptcursor;
		int wgcursor;
		int hwgcursor;
		TCHAR ptitem [19];
		TCHAR ptwert [4];
		TCHAR ptbez [37];
		TCHAR ptbezk [17];
		short wg;
		short hwg;
		TCHAR wg_bz1 [25];
		TCHAR hwg_bz1 [25];
      
    public :
		enum
		{
			Standard = 0,
			Material = 1,
		};
		CString Title;
        int ChoiceType;
		BOOL WithSmt;
		CString Where;
		CString Types;
	    std::vector<CABasList *> ABasList;
	    std::vector<CABasList *> SelectList;
      	CChoiceA(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceA(); 
		AG_CLASS Ag;
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchABz1 (CListCtrl *, LPTSTR);
        void SearchABz2 (CListCtrl *, LPTSTR);
        void SearchCol (CListCtrl *, LPTSTR, int);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
	    virtual void SaveSelection (CListCtrl *);
		CABasList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR, LPSTR);
        int GetWgBez ();
        int GetHwgBez ();
		void DestroyList ();
		virtual void SetDefault ();
		virtual void OnEnter ();
	    virtual void OnFilter ();
		int TestType (short a_typ, short a_typ2);
};
#endif
