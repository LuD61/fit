#ifndef _FRAMEDLG_DEF
#define _FRAMEDLG_DEF
#pragma once
#include "resource.h"
#include "PrArtPage.h"
#include "PrGrPage.h"
#include "PrKunPage.h"
#include "KunPrSheet.h"
#include "ToolButton.h"
#include "mo_progcfg.h"

// CFrameDlg-Dialogfeld

class CFrameDlg : public CDialog
{
	DECLARE_DYNAMIC(CFrameDlg)

public:
	CFrameDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CFrameDlg();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	virtual BOOL OnInitDialog ();
	DECLARE_MESSAGE_MAP()
public :
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	int StartSize;
	CToolButton ToolButton;
	int tabx;
	int taby;
	CKunPrSheet dlg;
	CPrArtPage Page1;
	CPrGrPage Page2;
	CPrKunPage Page3;
	virtual void OnSize (UINT, int, int);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg BOOL OnToolTipNotify(int nIDCtl,NMHDR *pNMHDR, LRESULT *pResult);
	void ReadCfg ();
};
#endif