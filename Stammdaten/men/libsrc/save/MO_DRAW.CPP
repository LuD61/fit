#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "strfkt.h"
#include "itemc.h"
#include "mo_draw.h"
#include "wmaskc.h"

struct HEXASTRUCT
{
             HWND phWnd;
             int cx;
             int x;
             int y;
             int xright;
             int xleft;
             int maxx;
             int ymed;
             int ymax;
             COLORREF color;
             int mode;
             HFONT hFont;
};

static HGLOBAL hglbDlg = NULL;

void FIGURES::SetRand (int rd)
/**
Randgroesse setzen.
**/
{
        rand = rd;
}

void FIGURES::PaintHexagon (HDC hdc, int slen, int x, int y, COLORREF color)
/**
Sechseck erzeugen.
**/
{
        HBRUSH hBrush, oldbrush;
        double sinus;
        double cosinus;

        sinus = sin (0.5) * (double) slen;
        cosinus = cos (0.5) * (double) slen;

        hexapoint[0].x = x;
        hexapoint[0].y = y;

        x += slen;
        hexapoint[1].x = x;
        hexapoint[1].y = y;


        x = (int) ((double) x + sinus + 0.5);
        y = (int) ((double) y + cosinus * stretch);
        hexapoint[2].x = x;
        hexapoint[2].y = y;

        x = hexapoint[1].x;
        y = y + hexapoint[2].y - hexapoint[1].y;
        hexapoint[3].x = x;
        hexapoint[3].y = y;

        x = hexapoint[0].x;
        y = hexapoint[3].y;
        hexapoint[4].x = x;
        hexapoint[4].y = y;

        x = (int) ((double) x - (sinus + 0.5));
        y = hexapoint[2].y;
        hexapoint[5].x = x;
        hexapoint[5].y = y;

        SetPolyFillMode (hdc, WINDING);
        hBrush = CreateSolidBrush (color);
        oldbrush = SelectObject (hdc, hBrush);
        SelectObject (hdc, GetStockObject (BLACK_PEN));
        Polygon (hdc, hexapoint, 6);
        SelectObject (hdc, oldbrush);
        DeleteObject (hBrush);
}

void FIGURES::PaintHexaRand (HDC hdc, int rd)
/**
Sechseck erzeugen.
**/
{
        HBRUSH hBrush, oldbrush;
        static int wd = 1;

        dim3 [0].y += rd;
        dim3 [1].y += (rd + wd);
        dim3 [2].y += (rd + wd);
        dim3 [3].y += (rd + wd);
        dim3 [4].y += (rd + wd);
        dim3 [5].y += rd;
        dim3 [6].y += rd;
        dim3 [7].y += rd;

        SetPolyFillMode (hdc, WINDING);
        hBrush = CreateSolidBrush (BLACKCOL);
        oldbrush = SelectObject (hdc, hBrush);
        SelectObject (hdc, GetStockObject (BLACK_PEN));
        Polygon (hdc, dim3, 8);
        SelectObject (hdc, oldbrush);
        DeleteObject (hBrush);
}        


void FIGURES::PaintHexagon3Dim (HDC hdc, int slen, int x, int y,
                                COLORREF color, int mode)
/**
Sechseck erzeugen.
**/
{
        HBRUSH hBrush, oldbrush;
        int rd;
        int blue, red, green;

        if (mode == 2)
        {
                   rd = rand / 2;
                   y += rd;
        }
        else
        {
                   rd = rand;
        }

        PaintHexagon (hdc, slen, x, y, color);
        dim3 [0].x = hexapoint [2].x;
        dim3 [0].y = hexapoint [2].y;
        dim3 [1].x = hexapoint [2].x;
        dim3 [1].y = hexapoint [2].y + rd;
        dim3 [2].x = hexapoint [3].x;
        dim3 [2].y = hexapoint [3].y + rd;
        dim3 [3].x = hexapoint [4].x;
        dim3 [3].y = hexapoint [4].y + rd;
        dim3 [4].x = hexapoint [5].x;
        dim3 [4].y = hexapoint [5].y + rd;
        dim3 [5].x = hexapoint [5].x;
        dim3 [5].y = hexapoint [5].y;
        dim3 [6].x = hexapoint [4].x;
        dim3 [6].y = hexapoint [4].y;
        dim3 [7].x = hexapoint [3].x;
        dim3 [7].y = hexapoint [3].y;

        SetPolyFillMode (hdc, WINDING);
        blue = GetBValue (color);
        red  = GetRValue (color);
        green = GetGValue (color);
        if (blue > shad)   blue -= shad;
        if (red > shad)    red -= shad;
        if (green > shad)  green -= shad;
        color = RGB (red, green, blue);
        hBrush = CreateSolidBrush (color);
        oldbrush = SelectObject (hdc, hBrush);
        SelectObject (hdc, GetStockObject (BLACK_PEN));
        Polygon (hdc, dim3, 8);
        MoveToEx (hdc, hexapoint[3].x, hexapoint[3].y, NULL);
        LineTo (hdc, dim3[2].x, dim3[2].y);
        MoveToEx (hdc, hexapoint[4].x, hexapoint[4].y, NULL);
        LineTo (hdc, dim3[3].x, dim3[3].y);
        SelectObject (hdc, oldbrush);
        DeleteObject (hBrush);
        // if (mode == 2) PaintHexaRand (hdc, rd); 
}        

void FIGURES::DelHexagon (HDC hdc, int slen, int x, int y, HBRUSH hBrush)
/**
Sechseck erzeugen.
**/
{
        double sinus;
        double cosinus;
        slen += 2;
        x -= 1;

        sinus = sin (0.5) * (double) slen;
        cosinus = cos (0.5) * (double) slen;

        hexapoint[0].x = x;
        hexapoint[0].y = y;

        x += slen;
        hexapoint[1].x = x;
        hexapoint[1].y = y;


        x = (int) ((double) x + sinus + 0.5);
        y = (int) ((double) y + cosinus * stretch);
        hexapoint[2].x = x;
        hexapoint[2].y = y;

        x = hexapoint[1].x;
        y = y + hexapoint[2].y - hexapoint[1].y;
        hexapoint[3].x = x;
        hexapoint[3].y = y;

        x = hexapoint[0].x;
        y = hexapoint[3].y;
        hexapoint[4].x = x;
        hexapoint[4].y = y;

        x = (int) ((double) x - (sinus + 0.5));
        y = hexapoint[2].y;
        hexapoint[5].x = x;
        hexapoint[5].y = y;

        SetPolyFillMode (hdc, WINDING);
        SelectObject (hdc, hBrush);
        SelectObject (hdc, GetStockObject (NULL_PEN));
        Polygon (hdc, hexapoint, 6);
}

void FIGURES::DelHexagon3Dim (HDC hdc, int slen, int x, int y,
                                HBRUSH hBrush, int mode)
/**
Sechseck erzeugen.
**/
{
        int rd;

        slen += 2;
        x -= 1;

        if (mode == 2)
        {
                   rd = rand / 2;
                   y += rd;
        }
        else
        {
                   rd = rand;
        }

        DelHexagon (hdc, slen, x, y, hBrush);
        dim3 [0].x = hexapoint [2].x;
        dim3 [0].y = hexapoint [2].y;
        dim3 [1].x = hexapoint [2].x;
        dim3 [1].y = hexapoint [2].y + rd;
        dim3 [2].x = hexapoint [3].x;
        dim3 [2].y = hexapoint [3].y + rd;
        dim3 [3].x = hexapoint [4].x;
        dim3 [3].y = hexapoint [4].y + rd;
        dim3 [4].x = hexapoint [5].x;
        dim3 [4].y = hexapoint [5].y + rd;
        dim3 [5].x = hexapoint [5].x;
        dim3 [5].y = hexapoint [5].y;
        dim3 [6].x = hexapoint [4].x;
        dim3 [6].y = hexapoint [4].y;
        dim3 [7].x = hexapoint [3].x;
        dim3 [7].y = hexapoint [3].y;

        SetPolyFillMode (hdc, WINDING);
        SelectObject (hdc, hBrush);
        SelectObject (hdc, GetStockObject (NULL_PEN));
        Polygon (hdc, dim3, 8);
}        

HWND FIGURES::CreateHexagon (HWND hWnd, int x, int y, int cx,
                             COLORREF color,
                             int mode)
/**
Hexagon erzeugen.
**/
{
       int i;
       double sinus;
       double cosinus;

       for (i = 0; i < 1000; i ++)
       {
                  if (hexastructs [i] == NULL)
                  {
                                break;
                  }
       }

       if (i == 1000) return NULL;

       hexastructs [i] = (struct HEXASTRUCT *)
                               malloc (sizeof (struct HEXASTRUCT));
       if (hexastructs [i] == NULL) return NULL;
       hexastructs [i]->phWnd  = hWnd;

       hexastructs[i]->maxx = cx;
       sinus = 1 + 2 * sin (0.5);
       cosinus = 1 + 2 * cos(0.5);
       hexastructs[i]->xleft = x;
       hexastructs[i]->xright = x + cx;
       hexastructs[i]->cx = (int) ((double) cx / sinus);
       cosinus = cos(0.5) * (double) hexastructs[i]->cx * stretch;
       hexastructs[i]->ymed = (int) (cosinus);
       hexastructs[i]->ymax = 2 * hexastructs[i]->ymed;
       hexastructs [i]->x      = x + (cx - hexastructs[i]->cx) / 2; 
       hexastructs [i]->y      = y; 
       hexastructs [i]->color  = color; 
       hexastructs [i]->mode   = mode;
       hexastructs [i]->hFont  = NULL;
       return (HWND) hexastructs [i];
}

void FIGURES::DestroyHexagon (HWND hWnd)
/**
Hexagon freigeben.
**/
{
       struct HEXASTRUCT *hx;
       int i;
       HBRUSH hBrush;
       HDC hdc;

       hx = (struct HEXASTRUCT *) hWnd;

       for (i = 0; i < 1000; i++)
       {
                   if (hx == hexastructs [i]) break;
       }
       if (i == 1000) return;

       hBrush = (HGDIOBJ) GetClassLong (hx->phWnd,
                                      GCL_HBRBACKGROUND);

       hdc = GetDC (hx->phWnd);
       if (hx->mode == 0)
       {
                DelHexagon (hdc, hx->cx, hx->x, hx->y, hBrush);
       }
       else
       {
                DelHexagon3Dim (hdc, hx->cx, hx->x, hx->y, hBrush, hx->mode);
       }
       ReleaseDC (hx->phWnd, hdc);
       free (hx);
       hexastructs [i] = NULL;
}

void FIGURES::SetSpezColor (COLORREF Color, COLORREF BkColor, HDC hdc)
/**
Spezialfarbe fuer Hot-Key-Buchstaben setzen.
**/
{
      int red;
      int blue;
      int green;
      int bkred;
      int bkblue;
      int bkgreen;

      blue = GetBValue (Color);
      red  = GetRValue (Color);
      green = GetGValue (Color);
      bkblue = GetBValue (BkColor);
      bkred  = GetRValue (BkColor);
      bkgreen = GetGValue (BkColor);

      if (red < 255 && bkred < 255)
      {
              SetTextColor   (hdc, REDCOL);
              return;
      }

      if (bkred < 255 && blue > 100 && green > 100)
      {
              SetTextColor   (hdc, REDCOL);
              return;
      }

      if (red == 0 && blue == 0 && green == 0)
      {
              SetTextColor (hdc, BLUECOL);
      }
                 
      SetTextColor   (hdc, BLACKCOL);
}
       

void FIGURES::PrintText (struct HEXASTRUCT *hx, HDC hdc,
                         char *text, int x, int y,
                         COLORREF Color,
                         COLORREF BkColor,
                         int mode)
/**
Text fuer Hexagon anzeigen.
**/
{
      TEXTMETRIC tm ;
      int textlen;
      SIZE size;
      int cx;
      int anz;
      int xstart;
      char OneChar [2];

      if (hx->hFont)
      {
               SelectObject (hdc, hx->hFont);
      }

      anz = zsplit (text, '&');
      textlen = strlen (text);

      GetTextMetrics (hdc, &tm) ;
      GetTextExtentPoint (hdc, text, textlen, &size);
      if (y == -1)
      {

/*  Horizontal zentrieren.              */

                   y = hx->ymed;
      }

      cx = (int) ((double) y * tan (0.5) / stretch + 0.5);
      y = y - size.cy / 2;

      if (mode == 2)
      {
                  y += (rand / 2);
      }

      xstart = max (1, hx->x - cx);
      cx = 2 * cx + hx->cx;

      if (x == -1)
      {

/*  Vertikal zentrieren.              */

                 x = max ( 1, (cx - size.cx) / 2);
      }
      else
      {
                 x = max (2, x);
      }
      while ((x + size.cx) > (cx - 1))
      {
               textlen --;
               text [textlen] = (char) 0;
               if (textlen == 0) break;
               GetTextExtentPoint (hdc, text, textlen, &size);
      }

      y += hx->y;
      x += xstart;

      SetBkMode (hdc, OPAQUE);
      SetTextColor   (hdc, Color);
      SetBkColor (hdc, BkColor);
      if (anz < 2 && text[0] != '&')
      {
                
                TextOut (hdc, x, y, text, strlen (text));
                return;
      }
      if (anz < 2)
      {
                SetSpezColor (Color, BkColor, hdc);
                OneChar[0] = zwort[1][0];
                OneChar[1] = (char) 0;
                TextOut (hdc, x, y, OneChar, 1);
                GetTextExtentPoint32 (hdc, OneChar, 1, &size);
                x += size.cx;
                SetTextColor   (hdc, Color);
                TextOut (hdc, x, y, &zwort [1][1], strlen (zwort[1]) - 1);
                return;
        }
         
        TextOut (hdc, x, y, zwort [1], strlen (zwort[1]));
        GetTextExtentPoint32 (hdc, zwort[1], strlen (zwort[1]), &size);
        x += size.cx;
        SetSpezColor (Color, BkColor, hdc);
        OneChar[0] = zwort[2][0];
        OneChar[1] = (char) 0;
        TextOut (hdc, x, y, OneChar, 1);
        GetTextExtentPoint32 (hdc, OneChar, 1, &size);
        x += size.cx;
        SetTextColor   (hdc, Color);
        TextOut (hdc, x, y, &zwort [2][1], strlen (zwort[2]) - 1);
}

void FIGURES::PrintHexaText (HDC hdc, HWND hWnd, char *feld)
/**
Text fuer Hexagon anzeigen.
**/
{
       ColButton *CuB;
       struct HEXASTRUCT *hx;
       COLORREF Color, BkColor;

       hx = (struct HEXASTRUCT *) hWnd;

       CuB = (ColButton *) feld;

       Color   = CuB->Color;
       BkColor = CuB->BkColor;
       if (CuB->text1)
       {
                    PrintText (hx, hdc,
                               CuB->text1,
                               CuB->tx1, CuB->ty1,
                               Color,
                               BkColor,
                               hx->mode);
       }
}


void FIGURES::DisplayHexaText (HWND hWnd, HDC hdc)
/**
Nach Text fuer Hexagon suchen.
**/
{
       field *feld;

       feld = GetFormFeld (hWnd);

       if (feld == NULL) return;
       PrintHexaText (hdc, feld->feldid, feld->item->GetFeldPtr ());
}

void FIGURES::UpdateHexagon (HWND hDlg, HWND hWnd)
/**
Hexagon zeichnen.
**/
{
       struct HEXASTRUCT *hx;
       int i;
       HDC hdc;

       hx = (struct HEXASTRUCT *) hWnd;

       for (i = 0; i < 1000; i++)
       {
                   if (hx == hexastructs [i]) break;
       }
       if (i == 1000) return;

       hdc = GetDC (hDlg);
       if (hx->mode)
       {
                PaintHexagon3Dim (hdc, hx->cx, hx->x,
                                       hx->y, hx->color, hx->mode);
       }
       else
       {
                PaintHexagon (hdc, hx->cx, hx->x,
                                       hx->y, hx->color);
       }
       DisplayHexaText ((HWND) hx, hdc);
       ReleaseDC (hDlg, hdc);
}


void FIGURES::PaintAllHexagon (HWND hWnd, HDC hdc)
/**
Alle Hexagons in einem Fenster anzeigen.
**/
{
       struct HEXASTRUCT *hx;
       int i;

       for (i = 0; i < 1000; i ++)
       {
                 if (hexastructs [i])
                 {
                    if (hexastructs[i]->phWnd == hWnd)
                    {
                        hx = hexastructs [i];
                        PaintHexagon3Dim (hdc, hx->cx, hx->x,
                                       hx->y, hx->color, hx->mode);
                        DisplayHexaText ((HWND) hx, hdc);
                    }
                 }
        }
}

void FIGURES::SetHexaFont (HWND hWnd, HFONT hFont)
/**
Font fuer Hexagon setzen.
**/
{
       struct HEXASTRUCT *hx;
       int i;

       hx = (struct HEXASTRUCT *) hWnd;

       for (i = 0; i < 1000; i++)
       {
                   if (hx == hexastructs [i]) break;
       }
       if (i == 1000) return;
       hx->hFont = hFont;
}

HFONT FIGURES::GetHexaFont (HWND hWnd)
/**
Font fuer Hexagon holen.
**/
{
       struct HEXASTRUCT *hx;

       hx = (struct HEXASTRUCT *) hWnd;

       return hx->hFont;
}

HWND FIGURES::GetHexaParent (HWND hWnd)
/**
phWnd fuer Hexagon holen.
**/
{
       struct HEXASTRUCT *hx;

       hx = (struct HEXASTRUCT *) hWnd;

       return hx->phWnd;
}

int FIGURES::MouseInHexagon (HWND hWnd, POINT *mpos)
/**
Test, ob Mouse ueber Hexagon steht.
**/
{
       struct HEXASTRUCT *hx;
       int x,y;
       int yend;
       int xend, cx;

       if (hWnd == 0) return FALSE;

       hx = (struct HEXASTRUCT *) hWnd;
       
       yend = hx->y + hx->ymax;

       if (mpos->y < hx->y) return FALSE;
       if (mpos->y > yend) return FALSE;

       y = mpos->y - hx->y;
       if (y > hx->ymed)
       {
               y = hx->ymed - (y - hx->ymed);
       }

       cx = (int) ((double) y * tan (0.5) / stretch);
       x = max (0, hx->x - cx);
       xend = x + 2 * cx + hx->cx;
       if (mpos->x < x) return FALSE;
       if (mpos->x > xend) return FALSE;
       return TRUE;
}

void BMAP::DestroyBitmap (void)
/**
Aktuelle Bitmap loeschen.
**/
{
       if (bmap)
       {
           GlobalFree (bmap);
           bmap = NULL;
           hglbDlg = NULL;
       }
}


HBITMAP BMAP::ReadBitmap (HDC hdc, char *bname)
/**
Graphic ausgeben.
**/
{
         FILE *bp;
         char far *rptr;
         DWORD bytes;
         unsigned rbytes;
         BITMAPFILEHEADER bmpheader;
         DWORD bmbitsoffs;
         const char far *bmbits;

         bp = fopen (bname, "rb");
         if (bp == (FILE *) 0)
         {
                     disp_mess ("Bitmap-Datei kann nicht ge�ffnet werden", 2);
                     return NULL;
         }

         bytes = fread (&bmpheader, 1, sizeof (BITMAPFILEHEADER), bp);
         bmbitsoffs = bmpheader.bfOffBits - sizeof (BITMAPFILEHEADER);
         hglbDlg = GlobalAlloc (GMEM_FIXED, bmpheader.bfSize);
         if (hglbDlg == NULL)
         {
                     disp_mess ("Speicher kann nicht zugeteilt werden", 2);
                     fclose (bp);
                     return NULL;
         }
         bmap = (BITMAPINFO FAR*) GlobalLock (hglbDlg);
         if (bmap == NULL)
         {
                     disp_mess ("Speicher kann nicht zugeteilt werden", 2);
                     fclose (bp);
                     return NULL;
         }

         rptr = (char far *) bmap;
         rbytes = fread ((char *) rptr, 1, 0x1000, bp);
         while (rbytes == 0x1000)
         {
                 rptr += 0x1000;
                 rbytes = fread ((BYTE *) rptr, 1, 0x1000, bp);
         }

         bmbits = (const char far *) bmap + bmbitsoffs;
         hBitmap = CreateDIBitmap (hdc, (BITMAPINFOHEADER FAR *) bmap,
                                   CBM_INIT, bmbits, bmap, DIB_RGB_COLORS);
         fclose (bp);
         hBitmapOrg = hBitmap;
         return hBitmap;
}

void BMAP::DrawBitmap (HDC hdc, HBITMAP hBitmap, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
//        DWORD dwsize;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}


void BMAP::DrawBitmap (HDC hdc, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void BMAP::DrawBitmapPart (HDC hdc, int xStart, int yStart, 
                                    int xFrom,  int yFrom,
                                    int cx,     int cy)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;
        int bmx, bmy;
        RECT rect;

        GetClientRect (bmphWnd, &rect); 
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);

        bmx = max (0, (rect.right  - bm.bmWidth)  / 2);
        bmy = max (0, (rect.bottom - bm.bmHeight) / 2);

        if (xStart == 0)
        {
            xStart = bmx;
        }
        if (yStart == 0)
        {
            yStart = bmy;
        }

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        if ((bm.bmWidth - xFrom) < cx)
        {
            cx = bm.bmWidth - xFrom;
        }
        if ((bm.bmHeight - yFrom) < cy)
        {
            cy = bm.bmHeight - yFrom;
        }

        ptSize.x = cx;
        ptSize.y = cy;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = xFrom;
        ptOrg.y = yFrom;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void BMAP::DrawBitmapPart (HDC hdc, HBITMAP hBitmap,
                                    int xStart, int yStart, 
                                    int xFrom,  int yFrom,
                                    int cx,     int cy)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;
        int bmx, bmy;
        RECT rect;

        GetClientRect (bmphWnd, &rect); 
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);

        bmx = max (0, (rect.right  - bm.bmWidth)  / 2);
        bmy = max (0, (rect.bottom - bm.bmHeight) / 2);

        if (xStart == 0)
        {
            xStart = bmx;
        }
        if (yStart == 0)
        {
            yStart = bmy;
        }

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        if ((bm.bmWidth - xFrom) < cx)
        {
            cx = bm.bmWidth - xFrom;
        }
        if ((bm.bmHeight - yFrom) < cy)
        {
            cy = bm.bmHeight - yFrom;
        }

        ptSize.x = cx;
        ptSize.y = cy;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = xFrom;
        ptOrg.y = yFrom;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}


void BMAP::ZoomBitmap (HDC hdc, int xStart, int yStart, 
                                int xFrom,  int yFrom,
                                int cx,     int cy,
                                double zoom)
/**
Bitmap zeichnen.
**/
{

        if (zoom == 1)
        {
            hBitmap = hBitmapOrg;
            DrawBitmapPart (hdc, xStart, yStart, xFrom, yFrom,
                                 cx, cy);
        }
        else if (hBitmapZoom)
        {
            DrawBitmapPart (hdc, hBitmapZoom, xStart, yStart,
                                 xFrom, yFrom,
                                 cx, cy);
        }
        else
        {
            hBitmap = hBitmapOrg;
            DrawBitmapPart (hdc, xStart, yStart, xFrom, yFrom,
                                 cx, cy);
        }

}


void BMAP::StrechBitmap (HDC hdc, int cx, int cy)
{

        HDC hdcMemory;
        HDC hbmOld;
        BITMAP bm;

        GetObject (hBitmap, sizeof (BITMAP), &bm);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hBitmap);

        SetViewportOrgEx (hdc, 0, 0, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, 0, 0,
                         cx, cy,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, SRCCOPY);
        
        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
}

void BMAP::StrechBitmapMem (HWND hWnd, int cx, int cy)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
        HDC hdc;


        if (hBitmapZoom)
        {
            DeleteObject (hBitmapZoom);
        }
        hdc = GetDC (hWnd);
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hBitmapOrg);
        hBitmapZoom = CreateCompatibleBitmap (hdc, cx, cy);
        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapZoom);

        SetViewportOrgEx (hdc, 0, 0, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdcMemoryZ, 0, 0,
                         cx, cy,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, SRCCOPY);
        
        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        ReleaseDC (hWnd, hdc);
        hBitmap = hBitmapZoom;
}

