FILE *_fopen (char *, char *);
size_t _fwrite (char *, size_t, size_t, FILE *);
size_t _fread (char *, size_t, size_t, FILE *);
int _fclose(FILE *);
int _fflush(FILE *);
long _fseek(FILE *, long, int);
long _ftell(FILE *);
