#ifndef _MO_DB_DEF
#define _MO_DB_DEF
#define DBCHAR 0
#define DBSHORT 1
#define DBLONG 2
#define DBDOUBLE 3

#define SELECT 0
#define UPDATE 1
#define DELETE 2
#define INSERT 3
#define UNDEL 4
#define DBSCROLL 5

#define MAXTABLES 100
#define MAXORDERS 100
#define MAXCURSOR 100
#define IDXANZ 5

#ifndef CLENGTH
#define CLENGTH 512
#endif

typedef struct
           {    char name [21];
                short length;
                char type;
                short nkomma;
           }  DBFIELD;

typedef struct
           {   short slen;
               short status;
               char rec[1];
           } DBREC;

typedef struct
           {   char iname [10];
               short ilength;
               short ipos;
               short itype;
           } DBIDX;

extern DBIDX inull;
extern DBIDX idxnull[];

typedef struct
           {  char tabname [21];
              DBIDX *index;
              short tabfields;
              DBFIELD *record;
           } DBTABLE;

typedef struct
           { char feldname [21];
             char feldtype;
             char cwert [CLENGTH];
             short swert;
             long lwert;
             double dwert;
             char *cptr;
             short *sptr;
             long *lptr;
             double *dptr;
             char op;
             int testgrp;
           }  DBSL;

typedef struct
          {  char feldname [21];
             char  cwert [512];
             short swert;
             long lwert;
             double dwert; 
          } DBGRP; 

typedef struct
           { char fname1 [21];
             char fname2 [21];
             char fname3 [21];
             char fname4 [21];
             char fname5 [21];
           } DBOR;

typedef struct
           { DBTABLE *table;
             short tablepos;
             FILE *lockid;
             FILE *dbid;
             FILE *dbidx;
             long dbpos;
             long dbcurrent_pos;
             void *updcurrent;
             short oflag;
             short idxflag;
             short idxonly;
             short idxnr;
             short modus;
             DBSL *dbin;
             DBSL *dbout;
             DBOR *dborder;
             DBGRP *dbgroup;
             short grpstat;
             short dbdesc;
             FILE *dbofile;
             short reclen;
             DBREC *dbrec;
             int testgrp;
           } CURSOR;

extern int dbstatus;         /* Status nach Datenbankoperation    */

void cr_ignore (int);
void on_dberr (int (*) ());
int set_order_mode (int);
int set_quik_order (void);
int unset_quik_order (void);
int del_table (char *);
int create_table (DBTABLE *);
int copy_table (DBTABLE *, char *);
int create_index (char *, short, char *, char *);
int del_index (char *);
int drop_index (char *);
int prepcurs (char *, short, char *,char *);
int set_op (int, char *, char);
int db_in (short, char *, void *);
int db_out (short, char *, void *);
int db_current (int, int);
int db_order (int, char *);
int db_group (short, char *);
int db_summen (short, char *);
int selcurs (short);
int executenext (short);
int executeprior (short);
int executecurs (short);
int flushcurs (short);
int opencurs (short);
int closecurs (short);
int erasecurs (short);
void eraseall (void);
char *recadr (short);
int akt_rec_len (void);
int rec_len (DBTABLE *);
char *feldadr (char *,char *);
int akt_rec_pos (char *);
int rec_pos (DBTABLE *, char *);
int rec_type (DBTABLE *, char *);
int akt_rec_type (char *);
int akt_rec_length (char *);
int rec_length (DBTABLE *, char *);
int make_bed (char *);
int set_out_field (char *, DBSL *);
int dbnwert (short, char *, char *);
int dbwert (int, char *, short);
int dbnswert (short, char *, char *);
int dbswert (short, char *, short);
int dbfeldinfo (short, char *, char *, short);
int dbfeldupd (short, char *, char *);
int make_upd (char *, char *, char *);
int make_upd_rows (char *, char *, char *,char *);
int make_ins (char *);
int tabinfo (char *);
int get_dbpid (void);
int mit_pid (char *);
int envpid (char *, char *, char *);
int dbinit (char *);
int dbcopy (char *, char *);
int printl (char *, short);
int dbfeldins (int, char *, char *);
int dlong_to_asc (long, char *);
long dasc_to_long (char *);
long datetolong (char *);
char * datetochar(char *, char *, long);
long timetolong(char *);
char * timetochar(char *, char *, long);

int make_ins_felder (char *, char *, int);
#endif