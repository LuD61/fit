#ifndef A_KUNDEF
#define A_KUNDEF
#include "dbclass.h"

#include "kun.h"
#include "dbclass.h"

struct A_KUN {
   short     mdn;
   short     fil;
   long      kun;
   double    a;
   char      a_kun[13];
   char      a_bz1[25];
   short     me_einh_kun;
   double    inh;
   char      kun_bran2[3];
   double    tara;
   double    ean;
   char      a_bz2[25];
   short     hbk_ztr;
   long      kopf_text;
   char      pr_rech_kz[2];
   char      modif[2];
   long      text_nr;
   short     devise;
   char      geb_eti[2];
   short     geb_fill;
   long      geb_anz;
   char      pal_eti[2];
   short     pal_fill;
   long      pal_anz;
   char      pos_eti[2];
   short     sg1;
   short     sg2;
   short     pos_fill;
   short     ausz_art;
   long      text_nr2;
   short     cab;
   char      a_bz3[25];
   char      a_bz4[25];
};
extern struct A_KUN a_kun, a_kun_null;

#line 9 "a_kun.rh"

class A_KUN_CLASS : public KUN_CLASS 
{
       private :
               short cursor_bra;
               void prepare (void);
               int prep_awcursor (void);
               int prep_awcursor (char *);
               void fill_aw (int);

       public :
               A_KUN_CLASS () : KUN_CLASS ()
               {
                       cursor_bra = -1;
               }
               int dbreadfirst (void);
               int dbreadfirst_bra(void);
               int dbread_bra (void);
               int ShowAllBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND hWnd, int ws_flag, char *);
               int QueryBu (HWND hWnd, int ws_flag);
};
#endif
