#ifndef _CMASK_DEF
#define _CMASK_DEF
#include <windows.h>
#include "wmaskc.h"
#include "image.h"

#define CEDIT         0
#define CDISPLAYONLY  1
#define CREADONLY     2
#define CBUTTON       3
#define CCOLBUTTON    4
#define CLISTBOX      5
#define COWNLISTBOX   6
#define CCOMBOBOX     7
#define CCAPTBORDER   8 
#define CFFORM        9
#define CREMOVED     10
#define CSTATIC      11  
#define CRIEDIT     100 
#define NOATTRIBUT  999

#define POS 0
#define XFLOW 1

#define CCHECKBUTTON  1 

class CHS
{
         private :
			    int chsizex;
			    int chsizey;
				int chposx;
				int chposy;
			    int x;
				int y;
				int cx;
				int cy;
				int cxp;
				int cyp;
				double xfaktor;
				double yfaktor;
				double cxfaktor;
				double cyfaktor;
				int xspace;
				int yspace;

         public :
			    CHS ()
				{
					chsizex = 0;
					chsizey = 0;
					chposx  = 0;
					chposy  = 0;
				}

				void Setchsize (int chsizex, int chsizey)
				{
					this->chsizex = chsizex;
					this->chsizey = chsizey;
				}

				int Getchsizex ()
				{
					return chsizex;
				}

				int Getchsizey ()
				{
					return chsizey;
				}

				void Setchpos (int chposx, int chposy)
				{
					this->chposx = chposx;
					this->chposy = chposy;
				}

				int Getchposx ()
				{
					return chposx;
				}
				int Getchposy ()
				{
					return chposy;
				}

				void SetValues (int x, int y, int cx, int cy)
				{
					this->x  = x;
					this->y  = y;
					this->cx = cx;
					this->cy = cy;
				}

				void SetPValues (int cxp, int cyp)
				{
					this->cxp = cxp;
					this->cyp = cyp;
				}

				void SetSize (HWND hWndMain)
				{
					RECT rect;

					cxfaktor = cyfaktor = 0.0;
					GetClientRect (hWndMain, &rect);
					cxp = rect. right;
					cyp = rect.bottom;
					if (cxp)
					{
						  xfaktor  = (double) x / cxp;
					      cxfaktor = (double) cx / cxp;
						  xspace = cxp - x;
					}

					if (cyp)
					{
						  yfaktor  = (double) y / cyp;
					      cyfaktor = (double) cy / cyp;
						  yspace = cyp - y;
					}

				}

				void NewSize (HWND hWndMain)
				{
					RECT rect;
					int cxp0;
					int cyp0;

					cxp0 = cxp;
					cyp0 = cyp;
					GetClientRect (hWndMain, &rect);
					cxp = rect. right;
					cyp = rect.bottom;
 
					if (chsizex == 1)
					{
					          cx = (int) (double) ((double) cxp * cxfaktor);
					}
					else if (chsizex == 2)
					{
						      cx = cx + cxp - cxp0;
					}
					if (chsizey == 1)
					{
					          cy = (int) (double) ((double) cyp * cyfaktor);
					}
					else if (chsizey == 2)
					{
						      cy = cy + cyp - cyp0;
					}
				}


				void NewPos (HWND hWndMain)
				{
					RECT rect;
					int cxp0;
					int cyp0;

					cxp0 = cxp;
					cyp0 = cyp;
					GetClientRect (hWndMain, &rect);
					cxp = rect. right;
					cyp = rect.bottom;

					if (chposx == 1)
					{
					         x = (int) (double) ((double) cxp  * xfaktor);
					}
					else if (chposx == 2)
					{
						     x = x + cxp - cxp0;
						     x = cxp - xspace;
					}
					if (chposy == 1)
					{
					         y = (int) (double) ((double) cyp  * yfaktor);
					}
					else if (chposy == 2)
					{
						     y = cyp - yspace;
					}
				}

				int GetX (void)
				{
					return x;
				}

				int GetY (void)
				{
					return y;
				}

				int GetCX (void)
				{
					return cx;
				}

				int GetCY (void)
				{
					return cy;
				}
};


class CFIELD
{
          private :
                void *feld;
				int start;
				int maxstart;
				int minstart;
				int xstart;
				int xmaxstart;
				int xminstart;
				char *name;
				int length;
				int cx;
                int cy;
				int x;
				int y;
				int xorg;
				int yorg;
				int cxp;
				int cxorg;
				int cyorg;
                int cyp;
				int xp;
				int yp;
				HWND hWndcl;
				HWND hWnd;
				HWND hWndMain;
				char *picture;
				DWORD attribut;
				DWORD ID;
				MFONT *mFont;
				HFONT hFont;
				HFONT oldfont;
				MFONT *PosmFont;
				HFONT PoshFont;
				int   UpMax;
				int   UpMin;
				int   UpValue;
				BOOL  UpDown;
				BOOL  Pixel;
				BOOL  Pixelorg;
				BOOL  AbsPos;
				int BkMode;
				int (*beforeprog) (void);
				int (*afterprog) (void);
				CHS  Chs;
				IMG **Image;
          public :
			    CFIELD (char *name,
					    void *feld, int cx, int cy, int x, int y, HWND hWnd, 
					    char *picture,
						DWORD attribut,
					    DWORD ID, 
					    MFONT *mFont,
						BOOL Pixel,
						int BkMode)
				{
					beforeprog = NULL;
					afterprog = NULL;
					Image = NULL;
					this->name     = name;
					this->feld     = feld;
					this->cx       = cx;
					this->cy       = cy;
					this->x        = x;
					this->y        = y;
					xorg           = x;
                    yorg           = y;
					cxorg          = cx;
                    cyorg          = cy;
					this->hWnd     = hWnd;
					this->picture  = picture;
					this->attribut = attribut;
                    this->ID       = ID;
					this->mFont    = mFont;
					this->hFont    = NULL;
					this->Pixel    = Pixel;
					this->Pixelorg = Pixel;
					if (Pixel == 0)
					{
						length = cx;
					}
                    this->BkMode   = BkMode;
					this->AbsPos = FALSE;
					start = 0;
					maxstart = 9999;
					minstart = -9999;
					xstart = 0;
					xmaxstart = 9999;
					xminstart = -9999;
					hWndMain = NULL;
					UpDown = FALSE;
                    PosmFont = mFont;
				}

			    CFIELD (char *name,
					    void *feld, int cx, int cy, int x, int y, HWND hWnd, 
					    char *picture,
						DWORD attribut,
					    DWORD ID, 
					    MFONT *mFont,
						BOOL Pixel,
						int BkMode,
						int (*beforeprog) (void),
						int (*afterprog) (void))
				{
					this->beforeprog = beforeprog;
					this->afterprog  = afterprog;
					this->name     = name;
					this->feld     = feld;
					this->cx       = cx;
					this->cy       = cy;
					this->x        = x;
					this->y        = y;
					xorg           = x;
                    yorg           = y;
					cxorg          = cx;
                    cyorg          = cy;
					this->hWnd     = hWnd;
					this->picture  = picture;
					this->attribut = attribut;
                    this->ID       = ID;
					this->mFont    = mFont;
					this->hFont    = NULL;
					this->Pixel    = Pixel;
					this->Pixelorg = Pixel;
					if (Pixel == 0)
					{
						length = cx;
					}
                    this->BkMode   = BkMode;
					this->AbsPos = FALSE;
					start = 0;
					maxstart = 9999;
					minstart = -9999;
					xstart = 0;
					xmaxstart = 9999;
					xminstart = -9999;
					hWndMain = NULL;
					UpDown = FALSE;
                    PosmFont = mFont;
				}

				void SethWndMain (HWND MainWindow)
				{
					 hWndMain = MainWindow;
				}

				HWND GethWndMain (void)
				{
					  return hWndMain;
				}

				int GetStart (void)
				{
					return this->start;
				}

				int xGetStart (void)
				{
					return this->xstart;
				}

				void SetLength (int length)
				{
					this->length = length;
				}

				void SetName (char *name)
				{
					this->name = name;
				}

				void SetUpDown (BOOL mode, int max, int min, int value)
				{
					UpDown = mode;
					UpMax  = max;
					UpMin  = min;
                    UpValue = value;
				}

				void SetCX (int cx)
				{
					this->cx = cx;
					this->cxorg = cx;
				}

				void SetCY (int cy)
				{
					this->cy = cy;
					this->cyorg = cy;
				}

				void SetPX (int x)
				{
					this->x = x;
				}

				void SetX (int x)
				{
					this->x = x;
					this->xorg = x;
				}

				void SetY (int y)
				{
					this->y = y;
					this->yorg = y;
				}

				void SethWnd (HWND hWnd)
				{
					this->hWnd = hWnd;
				}

				void SetAttribut (DWORD attribut)
				{
					this->attribut = attribut;
				}

				void SetPicture (char *picture)
				{
					this->picture = picture;
				}

				void SetBkMode (int BkMode)
				{
                    this->BkMode   = BkMode;
				}

				void SetID (DWORD ID)
				{
					this->ID = ID;
				}

				void SetFont (MFONT *mFont)
				{
					this->mFont = mFont;
				}

				void SetPosFont (MFONT *PosmFont)
				{
					this->PosmFont = PosmFont;
				}

				char *GetName (void)
				{
					return name;
				}

				BOOL GetUpDown (void)
				{
					return UpDown;
				}

				char *GetFeld (void)
				{
					return (char *) feld;
				}

				int GetLength (void)
				{
					return this->length;
				}

				void SetImage (IMG **img)
				{
					Image = img;
				}

				IMG **GetImage (void)
				{
					return Image;
				}

				char *GetFeld (DWORD);
				BOOL SetImage (IMG **, DWORD);
				IMG **GetImage (DWORD);
				void TimeFormat (void);
				void DatFormat (void);
				void ToFormat (void);
				int GetCX (void);
				int GetCY (void);
				int GetX (void);
				int GetY (void);
				int GetXP (void);
				int GetYP (void);
				int GetCXP (void);
				int GetCYP (void);

				void Setchsize (int, int);
				void Setchpos (int, int);
				int GetWidth (void);
				int GetHeight (void);
                void StartPlus (int plus);
                void StartMinus (int minus);
                void xStartPlus (int);
                void xStartMinus (int);
				void SetMaxStart (int);
				void SetMinStart (int);
				void SetStart (int);
				void xSetMaxStart (int);
				void xSetMinStart (int);
				void xSetStart (int);
				void Move (void);
				void MoveWindow (void);

				DWORD GetAttribut (void)
				{
					return attribut;
				}

				char *GetPicture (void)
				{
					return picture;
				}

				HWND GethWnd (void)
				{
					return hWnd;
				}

				DWORD GetID (void)
				{
					return ID;
				}

				MFONT *GetFont (void)
				{
					return mFont;
				}

				void SetBefore (int (*beforeprog) (void))
				{
					this->beforeprog = beforeprog;
				}

				void SetAfter (int (*afterprog) (void))
				{
					this->afterprog = afterprog;
				}
				virtual int before (void)
				{
					if (beforeprog)
					{
					         return (*beforeprog) ();
					}
					return 0;
				}

				virtual int after (void)
				{
					if (afterprog)
					{
					         return (*afterprog) ();
					}
					return 0;
				}

				int dobefore (DWORD);
				int dobefore (char *);
				int doafter (DWORD);
				int doafter (char *);
                int GetNextPos (HWND);
			    void SetAbsPos (void);
			    void SetAbsPos (int, int);
				DWORD GetFirstID (void);
				int NextField (int);
			    int PriorField (int);
                void SetText ();
                void GetText ();
   			    void display (HWND,HDC);
				void enter (void);
				void displaytxt (HWND, HDC);
				void displaybu (HWND, HDC);
				void displaystatic (HWND, HDC);
				void displayedit (HWND, HDC);
				void displayredit (HWND, HDC);
				void displaycolbu (HWND, HDC);
				void displayrdonly (HWND, HDC);
				void displaycp (HWND, HDC);
				void displaylbox (HWND, HDC);
				void destroy ();
 		        BOOL IsID (DWORD);
 		        CFIELD *IsCfield (DWORD);
 		        char *IsCurrentID (DWORD);
 			    DWORD IsCurrentName (char *);
 			    DWORD IsCurrenthWnd (HWND);
 		        BOOL SetFocusID (DWORD);
 			    BOOL SetFocusName (char *);
				void SetFocus (void);
		        BOOL EnableID (DWORD, BOOL);
 			    BOOL EnableName (char *, BOOL);
				void Enable (BOOL);
		        BOOL CheckID (DWORD, BOOL);
 			    BOOL CheckName (char *, BOOL);
				void Check (BOOL); 
			    HWND GethWndID (DWORD);
				HWND GethWndName (char *);
                DWORD GetAttrhWnd (HWND hWnd);
				void SetFeld (char *feld);
				BOOL SetFeld (char *feld, DWORD ID);
				BOOL SetUpDown (BOOL, int, int, int, DWORD);
				void Update (void);
                void SetFieldPicture (char *, char *);
                void SetFieldBkMode (char *, int);
};


class CFORM
{
			   private :
				   int fieldanz;
				   CFIELD **cfield;
				   int start;
				   int maxstart;
				   int minstart;
				   int xstart;
				   int xmaxstart;
				   int xminstart;
				   int currentfield;
				   MFONT *mFont;
				   HFONT hFont;
				   HWND hWndMain;
				   int layout;
			   public :
				   CFORM (int fieldanz, CFIELD **cfield) : fieldanz (fieldanz),
														  cfield (cfield),
														  currentfield (0)
				   {
					start = 0;
					maxstart = 9999;
					minstart = -9999;
					xstart = 0;
					xmaxstart = 9999;
					xminstart = -9999;
					mFont = NULL;
					hFont = NULL;
					layout = POS;

				   }

                   int before (DWORD);
                   int before (char *);
				   int after (DWORD);
				   int after (char *);

				   void Invalidate (BOOL mode)
				   {
					   RECT rect;
					   RECT rect0;

                       if (hWndMain == NULL) return;
					   GetClientRect (hWndMain, &rect0);
					   GetRectP (&rect);
					   rect.left  = rect0.left;
					   rect.right = rect0.right;
					   InvalidateRect (hWndMain, &rect, mode);
//					   InvalidateRect (hWndMain, NULL, mode);
				   }
                     
				   void SethWndMain (HWND MainWindow)
				   {
					 hWndMain = MainWindow;
				   }

				   HWND GethWndMain (void)
				   {
					  return hWndMain;
				   }
				
				   void SetFont (MFONT *mFont)
				   {
				 	   this->mFont = mFont;
				   }

				   MFONT *GetFont (void)
				   {
                       if (mFont)
					   {
				 	         return mFont;
					   }
					   return cfield[0]->GetFont ();
				   }

				   void SetLayout (int layout)
				   {
					   this->layout = layout;
				   }


				   char  * GetFeld (int current)
				   {
					   return cfield[current]->GetFeld ();
				   }

				   void SetCurrent (int current)
				   {
					   currentfield = current;
				   }
				   int GetCurrent (void)
				   {
					   return currentfield;
				   }

				   void SetFieldanz (int fieldanz)
				   {
					   this->fieldanz = fieldanz;
				   }

				   void SetCfield (CFIELD **cfield)
				   {
					   this->cfield = cfield;
				   }

				   int GetFieldanz (void)
				   {
					   return fieldanz;
				   }

				   CFIELD **GetCfield (void)
				   {
					   return cfield;
				   }

				   char *GetFeld (DWORD);
				   void Setchsize (DWORD, int, int);
				   void Setchpos (DWORD, int, int);

				   void Setchsize (char *, int, int);
				   void Setchpos (char *, int, int);

				   void Setchsize (int, int);
				   void Setchpos (int, int);

				   void SetMaxStart (int);
				   void SetMinStart (int);
 				   void SetStart (int);
				   int GetStart (void);
                   void StartPlus (int);
                   void StartMinus (int);
				   void xSetMaxStart (int);
				   void xSetMinStart (int);
				   void xSetStart (int);
				   int xGetStart (void);
                   void xStartPlus (int);
                   void xStartMinus (int);
				   int GetTm (MFONT *, HWND hWnd);
				   int xGetTm (MFONT *, HWND hWnd);
				   void TmStartPlus (int, MFONT *, HWND);
				   void TmStartMinus (int, MFONT *, HWND);
				   void xTmStartPlus (int, MFONT *, HWND);
				   void xTmStartMinus (int, MFONT *, HWND);
				   void Move (void);
				   void MoveWindow (void);

				   DWORD GetFirstID (void);
				   void SetFocus (void);
				   int SetFirstFocus (void);
				   int NextField (void);
				   int PriorField (void);
				   int NextFormField (void);
				   int PriorFormField (void);
				   void Centx (HWND);
				   void Centy (HWND);
				   void Rightx (HWND);
				   void Bottomy (HWND);
				   void display (HWND, HDC);
				   void enter ();
				   void displaytxt (HWND, HDC);
  				   void destroy ();
				   void GetRect (int *, int *);
				   void GetRect (RECT *);
				   void GetRectP (RECT *);
				   BOOL SetImage (IMG **, DWORD);
				   IMG **GetImage (DWORD);
 		           BOOL IsID (DWORD);
 			       BOOL SetCurrentID (DWORD);
 			       BOOL SetCurrentName (char *);
 			       BOOL SetCurrentFieldID (DWORD);
 			       BOOL EnableID (DWORD, BOOL);
 			       BOOL EnableName (char *, BOOL);
 			       BOOL CheckID (DWORD, BOOL);
 			       BOOL CheckName (char *, BOOL);
				   DWORD GetID (char *);
				   DWORD GetID (HWND);
				   char *GetName (DWORD);
				   HWND GethWndID (DWORD);
				   HWND GethWndName (char *);
				   void Update (int);
				   void Update (void);
                   void SetText ();
				   void SetFeld (char * feld, int current);
				   BOOL SetFeld (char * feld, DWORD ID);
				   BOOL SetUpDown (BOOL mode,  int, int, int, DWORD ID);
				   DWORD GetAttribut (void);
				   CFIELD *GetCfield (DWORD);
                   void SetFieldPicture (char *, char *);
                   void SetFieldBkMode (char *, int);
};


class WFORM
{
          private :
               form *wFrm;
			   HWND whWnd;
               CFORM *cwFrm;               
          public :     
			   WFORM ()
			   {
			   }
			   void SetwForm (form *frm)
			   {
				   wFrm = frm;
			   }
			   form * GetwForm (void)
			   {
				   return wFrm;
			   }
			   void SetcwForm (CFORM *cfrm)
			   {
				   cwFrm = cfrm;
			   }
			   CFORM *GetcwForm (void)
			   {
				   return cwFrm;
			   }
			   void ProcessMessages (void);
               static void ShowDlg (HDC, form *);
               static CALLBACK Proc(HWND,UINT, WPARAM,LPARAM);
               static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
};
	     
class MESSCLASS : public WFORM
{
          private :
			   int tlines;
			   HWND hMainWindow;
			   HANDLE hMainInst;
          public :
			  MESSCLASS () : WFORM ()
               {
               }
               HWND OpenWindow (HANDLE, HWND, char **, mfont *);
               void MessageText (HANDLE, HWND, char **, mfont *);
               void DestroyText (void);
               HWND COpenWindow (HANDLE, HWND, char **, mfont **);
               HWND COpenWindowF (HANDLE, HWND, CFORM *);
               void CMessageText (HANDLE, HWND, char **, mfont **, mfont *);
               void CMessageTextOK (HANDLE, HWND, char **, mfont **, BOOL);
               void CMessageTextF (HANDLE, HWND, CFORM *, BOOL);
               void CDestroyText (void);
               void CDestroyTextF (void);
};
#endif