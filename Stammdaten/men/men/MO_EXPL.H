
class MEN_EXPL
{
         private :
                int init_ok;
                int enter_break;
                HWND ExpWindow;
                HWND ExpMainWindow;
                void break_enter (void)
                {
                       enter_break = 1;
                }
                int IsDlgMessage (MSG *);
                void RegisterExplorer (HWND hWnd, HANDLE hMainInst);
                void OpenExplorer (HWND hWnd, HANDLE hMainInst);
         public :
                MEN_EXPL (void)
                {
                       init_ok = 0;
                       enter_break = 0;
                }
                void MenueExplorer (HWND hWnd, HANDLE hMainInst);
};
