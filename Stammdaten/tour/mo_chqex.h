#ifndef _MO_CHQEX_DEF
#define _MO_CHQEX_DEF

class CHQEX
{
          private :
			  HWND hMainWindow;
			  HINSTANCE hInstance;
              mfont *Font;
			  DWORD currentfield;

			  static  HWND hWnd;
              static int listpos;
			  static int (*OkFunc) (int); 
			  static int (*OkFuncE) (char *); 
			  static int (*DialFunc) (int); 
              static int (*FillDb) (char *); 
              static int (*SearchLst) (char *); 
			  static char EBuff [256];
			  static char SBuff [256];
              int cx, cy;
          public :
			  CHQEX (int, int, char *, char *);
			  ~CHQEX ();
              BOOL IsFocusGet (int, int);
 		      void ProcessMessages (void);
              static void ShowDlg (HDC, form *);
              static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
			  static void SetOkFunc (int (*) (int));
			  static void SetOkFuncE (int (*) (char *));
			  static void SetDialFunc (int (*) (int));
			  static void SetFillDb (int (*) (char *));
			  static void SetSearchLst (int (*) (char *));
              static int  GetSel (void);
              static void  SetSel (int);
              static void MoveWindow (void);
              void OpenWindow (HANDLE, HWND);
              void DestroyWindow (void);
			  void SetCurrentID (DWORD);
			  void SetCurrentName (char *);
			  void EnableID (DWORD, BOOL);
			  void EnableName (char *, BOOL);
			  void CheckID (DWORD, BOOL);
			  void CheckName (char *, BOOL);
              void VLines (char *, int);
			  void InsertCaption (char *);
			  void InsertRecord  (char *);
			  void UpdateRecord  (char *, int);
              void GetText (char *);
              BOOL TestButtons (HWND);
              BOOL TestEdit (HWND);
};
#endif

