#ifndef _SEARCHADR_DEF
#define _SEARCHADRDEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

struct SADR
{
	  long adr;
	  char adr_krz [17];
	  char adr_nam1 [37];
	  char adr_nam2 [37];
	  char pf [17];
	  char plz [9];
	  char str [37];
	  char ort1 [37];
	  char ort2 [37];
	  short adr_typ;
};

class SEARCHADR
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static struct SADR *sadrtab;
           static struct SADR sadr;
           static int idx;
           static long adranz;
           static CHQEX *Query;
           static int soadr; 
           static int soadr_krz; 
           static int soadr_nam1; 
           static int soort; 

           int SearchPos;
           int OKPos;
           int CAPos;
           HWND adrwin;
           short adr_typ;
        public :
           SEARCHADR ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
           }

           ~SEARCHADR ()
           {
                  if (sadrtab != NULL)
                  {
                      delete sadrtab;
                      sadrtab = NULL;
                  }
           }

           SADR *GetSadr (void)
           {
               return &sadr;
           }

           static int sortadr (const void *, const void *);
           static void SortAdr (HWND);
           static int sortadr_krz (const void *, const void *);
           static void SortAdr_krz (HWND);
           static int sortadr_nam1 (const void *, const void *);
           static void SortAdr_nam1 (HWND);
           static int sortort (const void *, const void *);
           static void SortOrt (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int ReadAdr (char *);
           void SetParams (HINSTANCE, HWND);
           void SearchAdr (void);
};  
#endif