#ifndef _WOTOUDEF
#define _WOTOUDEF
#include "dbclass.h"

struct WO_TOU {
   short     mdn;
   short     fil;
   long      kun;
   short     ta_nr;
   char      kun_krz1[17];
   short     kun_fil;
   long      posi;
   long      mo_tour;
   long      di_tour;
   long      mi_tour;
   long      do_tour;
   long      fr_tour;
   long      sa_tour;
   long      so_tour;
};
extern struct WO_TOU wo_tou, wo_tou_null;

#line 6 "wo_tou.rh"

class WO_TOU_CLASS : public DB_CLASS 
{
       private :
               int cursor_kun;
               void prepare (char *);
               int prep_awcursor (void);
               int prep_awcursor (char *);
               void fill_aw (int);

       public :
               WO_TOU_CLASS () : DB_CLASS ()
               {
                       cursor_kun = -1;
               }
               int dbreadfirst (char *);
               int dbreadkunfirst (char *);
               int dbreadkun (void);
               int ShowAllBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND hWnd, int ws_flag, char *);
               int QueryBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND, int); 
               int PrepareQuery (form *, char *[]);
};
#endif
