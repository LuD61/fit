#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "tou.h"
#include "pers.h"
#include "lgr.h"
#include "inflib.h"
#include "bmlib.h"
#include "mo_qtou.h"
#include "mdn.h"
#include "ptab.h"
#include "searchadr.h"
#include "dbclass.h"
#include "palette.h"
#include "mo_progcfg.h"


int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
static int ProcessMessages(void);
void     InitFirstInstance(HANDLE);
BOOL     InitNewInstance(HANDLE, int);
void     PrintLines (HDC hdc);
void     DisplayLines (void);


static int MoveMain = FALSE;
static HANDLE  hMainInst;
HWND    hMainWindow;
HWND    mamain1;
HWND    mamain2 = NULL; 

HWND ForegroundWindow;

void GetForeground (void)
{
	ForegroundWindow = GetForegroundWindow ();
}

void SetForeground (void)
{
	SetForegroundWindow (ForegroundWindow);
}


extern HWND  ftasten;
extern HWND  ftasten2;
static HICON hIcon;


static char *TXT_WORK   = "Bearbeiten";
static char *TXT_SHOW   = "Anzeigen";
static char *TXT_DEL    = "L�schen";
static char *TXT_PRINT  = "Drucken";

static char *TXT_ACTIVE = TXT_WORK;


static int IDM_ACTIVE = IDM_WORK;

HMENU hMenu;

struct PMENUE dateimen[] = {
	                        "&1 �ffnen", "C", NULL, IDM_WORK, 
                            "&2 Anzeigen",   " ", NULL, IDM_SHOW,
						    "&3 L�schen",    " ", NULL, IDM_DEL,
						    "&4 Drucken",    " ", NULL, IDM_PRINT,
							"",               "S", NULL, 0, 
	                        "B&eenden", " ", NULL, KEY5,
						     NULL, NULL, NULL, 0};

struct PMENUE bearbmen[] = {
	                        "&Funktion f�r aktives Eingaefeld F2",  " ", NULL, KEY2, 
                            "&Text in Zutaten ersetzen",  " ", NULL, IDM_TEXT,
                            "&Etiketten-Typ bearbeiten",  " ", NULL, IDM_ETI, 
 NULL, NULL, NULL, 0};

struct PMENUE menuetab[] = {"&Datei",      "M", dateimen, 0, 
//                            "&Bearbeiten", "M", bearbmen, 0, 
						     NULL, NULL, NULL, 0};



extern HWND hWndToolTip;
HWND hwndTB;

static TBBUTTON tbb[] =
{
 0,               IDM_WORK,   TBSTATE_ENABLED | TBSTATE_CHECKED, 
	                          TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,

 1,               IDM_SHOW,   TBSTATE_ENABLED, 
	                          TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 2,               IDM_DEL,    TBSTATE_ENABLED, 
                              TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 3,               IDM_PRINT,  TBSTATE_INDETERMINATE, 
                              TBSTYLE_BUTTON,

 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED,
                  TBSTYLE_SEP, 
 0, 0, 0, 0,
 6,               IDM_INFO,   TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 7,               KEYPGD, TBSTATE_INDETERMINATE, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 8,               KEYPGU, TBSTATE_INDETERMINATE, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
};

static char *qInfo [] = {"�ffnen",
                         "Anzeigen",
                         "L�schen",
                         "Drucken",
                         "Funktion f�r aktives Eingabefeld",
                         "n�chste Seite",
                         "vorhergehende Seite",
                         "Text in Zutaten ersetzen",
                         "Etiketten-Typ bearbeiten", 
                         0};

static UINT qIdfrom [] = {IDM_WORK, IDM_SHOW, IDM_DEL,
                         IDM_PRINT, IDM_INFO,
                         KEYPGD, KEYPGU,IDM_TEXT, IDM_ETI, 0};
static char *qhWndInfo [] = {""};
static HWND *qhWndFrom [] = {NULL}; 


struct TOUS {
   char      tou [10];
   char      htou [10];
   char      adr [10];
   char      adr_krz [20];
   char      tou_bz[50];
   char      fz_kla[4];
   char      fz[14];
   char      str_zeit[7];
   char      dau[7];
   char      lng[10];
   char      fah_1[14];
   char      fah_2[14];
   char      fah_1_nam[40];
   char      fah_2_nam[40];
   char      lgr [10];
   char      lgr_bz [37];
   char      leitw_typ [4];
   char      leitw_bz [37];
   char      auto_typ [4];
   char      auto_bz [37];
   char      palette [14];
   char      palette_bz [25];
   char      palette_anz [10];

};

struct TOUS tous;

static int akt_fz = 0;
static int SetKey9Adr (void);
static int ShowFz (void);
static int SetKey9Drv (void);
static int SetKey9Lgr (void);
static int SetKey9Palette (void);
static int SetKey9Leitw (void);
static int SetKey9Auto (void);
static int ReadAdr (void);
static int ReadLgr (void);
static int ReadDriver (void);
static int ReadLeitw0 (void);
static int ReadLeitw (void);
static int ReadAuto0 (void);
static int ReadAuto (void);
static int ReadA_bas (void);
static int ReadA_bas0 (void);

ITEM itou      ("tou",        tous.tou,      "Tour-Nr            ", 0); 
ITEM ihtou     ("htou",       tous.htou,     "Haupt-Tour         ", 0); 
ITEM iadr      ("adr",        tous.adr,      "Adress-Nr   ", 0); 
ITEM iadr_krz  ("adr_krz",    tous.adr_krz,   "", 0); 
ITEM itou_bz   ("tou_bz",     tous.tou_bz,   "Tour-Bezeichnung   ", 0); 
ITEM ifz_kla   ("fz_kla",     tous.fz_kla,   "Fahrzeug-Klasse    ", 0); 
ITEM ifz       ("fz",         tous.fz,       "Fahrzeug           ", 0); 
ITEM istr_zeit ("str_zeit",   tous.str_zeit, "Start-Zeit         ", 0); 
ITEM idau      ("dou",        tous.dau,      "Dauer              ", 0); 
ITEM ilng      ("lng",        tous.lng,      "L�nge              ", 0); 
ITEM ipalette  ("palette",    tous.palette,  "Paletten           ", 0); 
ITEM ipalette_bz  ("palette_bz",     tous.palette_bz,  "", 0); 
ITEM ipalette_anz  ("palette_anz",    tous.palette_anz,  "Anzahl", 0); 
ITEM ifah_1    ("fah_1",      tous.fah_1,    "Fahrer 1           ", 0); 
ITEM ifah_2    ("fah_2",      tous.fah_2,    "Fahrer 2           ", 0); 
ITEM ifah_1_nam ("fah_1_nam", tous.fah_1_nam,  "", 0); 
ITEM ifah_2_nam ("fah_2_nam", tous.fah_2_nam,  "", 0); 
ITEM ilgr      ("lgr",        tous.lgr,      "Lager              ", 0); 
ITEM ilgr_bz   ("lgr_bz",     tous.lgr_bz,   "", 0); 
ITEM ileitw_typ ("leitw_typ", tous.leitw_typ,"Leitweg-Typ        ", 0); 
ITEM ileitw_bz   ("leitw_bz", tous.leitw_bz,   "", 0); 
ITEM iauto_typ ("auto_typ", tous.auto_typ,   "Automatisierung    ", 0); 
ITEM iauto_bz   ("auto_bz", tous.auto_bz,   "", 0); 

static field _tform[] = {
&itou,         9, 0, 1, 1, 0, "%8d", EDIT, 0, 0, 0,
};

static form tform = {1, 0, 0, _tform, 0, 0, 0, 0, NULL};            

static field _dataform[] = {
&ihtou,        9, 0, 1, 1, 0, "%8d", EDIT, 0, 0, 0,  
&iadr,         9, 0, 1,35, 0, "%8d", EDIT, SetKey9Adr, ReadAdr, 0,
&iadr_krz,    17, 0, 1,59, 0, "",    READONLY, 0, 0, 0,  
&itou_bz,     49, 0, 2, 1, 0, "",    EDIT, 0, 0, 0,  
&ifz_kla,      3, 0, 3, 1, 0, "",    EDIT, 0, ShowFz, 0,  
&ifz,         13, 0, 4, 1, 0, "",    EDIT, 0, 0, 0,  
&istr_zeit,    6, 0, 5, 1, 0, "hh:mm", EDIT, 0, 0, 0,  
&idau,         6, 0, 6, 1, 0, "",    EDIT, 0, 0, 0,  
&ilng,         8, 0, 7, 1, 0, "%5d", EDIT, 0, 0, 0,  
&ipalette,    13, 0, 8, 1, 0, "%.0lf", EDIT, SetKey9Palette, ReadA_bas, 0,  
&ipalette_bz, 24, 0, 8,35, 0, "",    READONLY, 0, 0, 0,  
&ipalette_anz, 8, 0, 8,60, 0, "%5d", EDIT, 0, 0, 0,  
&ifah_1,      13, 0,10, 1, 0, "",    EDIT, SetKey9Drv, ReadDriver, 0,  
&ifah_1_nam,  37, 0,10,35, 0, "",    READONLY, 0, 0, 0,  
&ifah_2,      13, 0,11, 1, 0, "",    EDIT, SetKey9Drv, ReadDriver, 0,  
&ifah_2_nam,  37, 0,11,35, 0, "",    READONLY, 0, 0, 0,  
&ilgr,         9, 0,12, 1, 0, "%8d", EDIT,  SetKey9Lgr, ReadLgr, 0,  
&ilgr_bz,     24, 0,12,35, 0, "",    READONLY, 0, 0, 0,  
&ileitw_typ,   3, 0,13, 1, 0, "%2d", EDIT, SetKey9Leitw, ReadLeitw, 0,  
&ileitw_bz,   24, 0,13,35, 0, "",    READONLY, 0, 0, 0,  
&iauto_typ,    3, 0,14, 1, 0, "%2d", EDIT, SetKey9Auto, ReadAuto, 0,  
&iauto_bz,    24, 0,14,35, 0, "",    READONLY, 0, 0, 0,  

};

static form dataform = {22, 0, 0, _dataform, 0, 0, 0, 0, NULL};

static FRMDB dbtou[] = {
//&tform,    0, (int *)  &tou.tou,     2, 0, "%ld",
&dataform, 0, (char *) &tou.htou,      2,0,  "%ld",
&dataform, 1, (char *) &tou.adr,       2,0,  "%ld",
&dataform, 3, (char *) tou.tou_bz,     0,48, "",
&dataform, 4, (char *) tou.fz_kla,     0, 2, "",
&dataform, 5, (char *) tou.fz,         0,12, "",
&dataform, 6, (char *) tou.srt_zeit,   0, 5, "",
&dataform, 7, (char *) tou.dau,        0, 5, "",
&dataform, 8, (char *) &tou.lng,       2, 0, "%ld",
&dataform, 9, (char *) &tou.palette,   3, 0, "%.0lf",
&dataform,11, (char *) &tou.palette_anz,  2, 0, "%ld",
&dataform,12, (char *) tou.fah_1,      0,12, "",
&dataform,14, (char *) tou.fah_2,      0,12, "",
&dataform,16, (char *) &tou.lgr,       2, 0, "%ld",
&dataform,17, (char *) lgr.lgr_bz,     0,24, "",
&dataform,18, (char *) &tou.leitw_typ, 2, 0, "%ld",
&dataform,20, (char *) &tou.auto_typ,  1, 0, "%d",
NULL,      0,          NULL,           0, 0, "",
};


TOU_CLASS Tour;
LGR_CLASS Lgr;
PALETTE_CLASS Palette;
ADR_CLASS Adr;
extern PERS_CLASS Pers;
QueryTou QClass;
PTAB_CLASS ptab_class;
DB_CLASS DbClass;

static PROG_CFG ProgCfg ("Tour");

static BOOL Fah2IsBearbeiter = TRUE;

int InfoTou (void)
{
        char where [80];

        if (atol (tous.tou))
        {
            sprintf (where, "where tou = %ld", atol (tous.tou));
        }
        else
        {
            return 0;
        }
        _CallInfoEx (hMainWindow, NULL,
							      "tou", 
								  where, 0l);
        return 0;
}


int dokey5 ()
/**
Aktion bei Taste F5
**/
{
        rollbackwork (); 
        break_enter ();
        return 1;
}


BOOL BmExist (void)
/**
Test, ob eine Bitmap existiert.
**/
{
    char buffer [80];
    char *bws;
    HANDLE fp;

    bws = getenv ("BWS");
    if (bws == NULL)
    {
        bws = "c:\\user\\fit";
    }
    sprintf (buffer, "%s\\bilder\\fahrzeuge\\%d.bmp",bws, atoi (tous.fz_kla));
    fp = CreateFile (buffer, GENERIC_READ, 0, NULL, 
                         OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fp == INVALID_HANDLE_VALUE)
    {
                         return FALSE;
    }
    CloseHandle (fp);
    return TRUE;
}


void DestroyBm (void)
/**
Bitmap loeschen.
**/
{
    akt_fz = 0;
    _DestroyBm ();
}


void ShowBm (void)
/**
Bitmap anzeigen.
**/
{
    RECT rect;
    TEXTMETRIC tm;
    int x, y, cx, cy;
    char buffer [80];
    char *bws;

    if (atoi (tous.fz_kla) == 0) return;
    if (atoi (tous.fz_kla) == akt_fz) return;


    if (BmExist () == FALSE)
    {
        DestroyBm ();
        return;
    }

    akt_fz = atoi (tous.fz_kla);
    bws = getenv ("BWS");
    if (bws == NULL)
    {
        bws = "c:\\user\\fit";
    }
    GetWindowRect (mamain2, &rect);
//    GetClientRect (mamain2, &rect);
    stdfont ();
    SetTmFont (mamain2, &tm);

    cx = 15;
    cy = 5;
    x = (rect.right - 2) / tm.tmAveCharWidth;
    x = x - cx - 9;
	
    y = rect.top;
    y = (int) (double) ((double) y / 
                                (double)(1 + (double) 1/3));
    y /= tm.tmHeight;
    y += 4;

	y = 13;
	x = 45;

    sprintf (buffer, "-b4 -m2 -z%d -s%d -h%d -w%d "
             "%s\\bilder\\fahrzeuge\\%d.bmp",
             y, x, cy, cx, bws, atoi (tous.fz_kla));
    _ShowBm (buffer);
}


int ShowFz (void)
/**
Bild zum Fahrzeug anzeigen.
**/
{
    if (testkeys ()) return 0;
    ShowBm ();
    return 0;
}


int QueryTou (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;

       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
       ret = QClass.querytou (mamain1);
       set_fkt (dokey5, 5);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       sprintf (tous.tou, "%ld", tou.tou);
       display_field (mamain1, &tform.mask[0], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

int QueryDrv (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;
	   int fah1;
	   int fah2;
	   int fah1pos;
	   int fah2pos;

	   fah1 = fah2 = 0;
	   fah1pos = GetItemPos (&dataform, "fah_1");
	   fah2pos = GetItemPos (&dataform, "fah_2");

	   if (currentfield == fah1pos)
	   {
		   fah1 = 1;
	   }
	   else
	   {
		   fah2 = 1;
	   }

       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain2);
       ret = QClass.querypers (mamain2);
       set_fkt (dokey5, 5);
       SetAktivWindow (mamain2);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
	   if (fah1)
	   {
                sprintf (tous.fah_1, "%s", pers.pers);
                display_field (mamain1, &dataform.mask[fah1pos], 0, 0);
	   }
	   else
	   {
                sprintf (tous.fah_2, "%s", pers.pers);
                display_field (mamain1, &dataform.mask[fah2pos], 0, 0);
	   }
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

int ReadPers0 (char *fahrer, char *fah_nam)
/**
Fahrer un Fahrer-Name lesen.
**/
{
	    int dsqlstatus;

		strcpy (fah_nam, " ");
        dsqlstatus = 0; 
		if (strcmp (fahrer, "               ") > 0)
		{
			strcpy (pers.pers, fahrer);
			dsqlstatus = Pers.dbreadfirst ();
			if (dsqlstatus == 0)
			{
				dsqlstatus = Adr.lese_adr (pers.adr);
				if (dsqlstatus == 0)
				{
					strcpy (fah_nam, _adr.adr_nam1);
				}
			}
		}
		return dsqlstatus;
}

int ReadAdr0 (void)
{
        long adr;

        strcpy (tous.adr_krz, "");
        adr = atol (tous.adr);

        DbClass.sqlin ((short *) &adr, 2, 0);
        DbClass.sqlout ((char *) tous.adr_krz, 0, 17);
        DbClass.sqlcomm ("select adr_krz from adr where adr = ?");
		return 0;
}
int ReadA_bas0 (void)
{
        strcpy (tous.palette_bz, "");
        DbClass.sqlin ((double *) &a_bas.a, 3, 0);
        DbClass.sqlout ((char *) tous.palette_bz, 0, 24);
        DbClass.sqlcomm ("select a_bz1 from a_bas where a = ? and a_typ = 11");   //nur Leergut !
		if (sqlstatus == 100 && a_bas.a > 0)
		{
			print_mess (2, "Palette %.0lf ist nicht angelegt", a_bas.a);
            SetCurrentField (currentfield);
			return 0;
		}

		return 0;
}

int ReadAdr (void)
{
	    if (testkeys ()) return (0);

	    set_fkt (NULL, 9);
		SetFkt (9, leer, NULL);
        ReadAdr0 ();
		display_form (mamain1, current_form, 0, 0);
		return 0;
}

int ReadA_bas (void)
{
	    if (testkeys ()) return (0);

	    set_fkt (NULL, 9);
		SetFkt (9, leer, NULL);
        a_bas.a = ratod (tous.palette);
        ReadA_bas0 ();
		display_form (mamain1, current_form, 0, 0);
		return 0;
}

int ReadDriver (void)
/**
Lager lesen.
**/
{
	   int fah1;
	   int fah2;
	   int fah1pos;
	   int fah2pos;
	   int dsqlstatus;

	   if (testkeys ()) return 0;

	   fah1 = fah2 = 0;
	   fah1pos = GetItemPos (&dataform, "fah_1");
	   fah2pos = GetItemPos (&dataform, "fah_2");
	   if (fah1pos == currentfield)
	   {
		   fah1 = 1;
	       dsqlstatus = ReadPers0 (tous.fah_1, tous.fah_1_nam);
	   }
	   else
	   {
		   fah2 = 1;
	       dsqlstatus = ReadPers0 (tous.fah_2, tous.fah_2_nam);
	   }
	   if (dsqlstatus == 100)
	   {
			print_mess (2, "Fahrer %s ist nicht angelegt", clipped (pers.pers));
            SetCurrentField (currentfield);
			return 0;
		}
 	    if (fah1)
		{
                display_field (mamain1, &dataform.mask[fah1pos + 1], 0, 0);
		}
	    else
		{
                display_field (mamain1, &dataform.mask[fah2pos + 1], 0, 0);
		}
		return 0;
}


int ReadLgr (void)
/**
Lager lesen.
**/
{

	    if (testkeys ()) return (0);

	    set_fkt (NULL, 9);
		SetFkt (9, leer, NULL);
        lgr.lgr = atol (tous.lgr);
		if (lgr.lgr == 0l) return (0);

		Lgr.dbreadfirst ();
		if (sqlstatus == 100)
		{
			print_mess (2, "Lager %ld ist nicht angelegt", lgr.lgr);
            SetCurrentField (currentfield);
			return 0;
		}
		return 0;
}

int ReadLeitw0 (void)
{
	    char wert [5]; 

		sprintf (wert, "%ld", atol (tous.leitw_typ));
	    if (ptab_class.lese_ptab ("leitw_typ", wert)
			!= 0)
		{
			        tous.leitw_bz[0] = 0;
					return 100;
		}
		strcpy (tous.leitw_typ, ptabn.ptwert);
		strcpy (tous.leitw_bz, ptabn.ptbezk);
		return 0;
}

int ReadAuto0 (void)
{
	    char wert [5]; 
		sprintf (wert, "%ld", atol (tous.auto_typ));
	    if (ptab_class.lese_ptab ("auto_typ", wert)
			!= 0)
		{
			        tous.auto_bz[0] = 0;
					return 100;
		}
		strcpy (tous.auto_typ, ptabn.ptwert);
		strcpy (tous.auto_bz, ptabn.ptbezk);
		return 0;
}


int ReadLeitw (void)
{
	    if (testkeys ()) return (0);

	    set_fkt (NULL, 9);
		SetFkt (9, leer, NULL);
        if (ReadLeitw0 () == 100)
		{
			        disp_mess ("Falsche Eingabe", 2);
					SetCurrentField (currentfield);
					return 0;
		}
		display_form (mamain1, current_form, 0, 0);
		return 0;
}

int ReadAuto (void)
{
	    if (testkeys ()) return (0);

	    set_fkt (NULL, 9);
		SetFkt (9, leer, NULL);
        if (ReadAuto0 () == 100)
		{
			        disp_mess ("Falsche Eingabe", 2);
					SetCurrentField (currentfield);
					return 0;
		}
		display_form (mamain1, current_form, 0, 0);
		return 0;
}

int ShowAdr ()
{
        struct SADR *sadr;
        SEARCHADR SearchAdr;
        int adrfield;
        int adr_krzfield;

		adrfield     = GetItemPos (&dataform, "adr");
		adr_krzfield = GetItemPos (&dataform, "adr_krz");
        SearchAdr.SetParams (hMainInst, hMainWindow);
        SearchAdr.SearchAdr ();
        if (syskey == KEY5)
        {
            SetCurrentFocus (currentfield);
            return 0;
        }
        sadr = SearchAdr.GetSadr ();
        sprintf (tous.adr, "%8ld", sadr->adr); 
        sprintf (tous.adr_krz, "%s", sadr->adr_krz); 
        display_field (mamain1, &dataform.mask[adrfield], 0, 0);
        display_field (mamain1, &dataform.mask[adr_krzfield], 0, 0);
        PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
        return 0;
}


int ShowLgr (void)
/**
Lager anzeigen.
**/
{
	    int savefield;
		int lgrfield;
		int lgrbzfield;

		lgrfield = GetItemPos (&dataform, "lgr");
		lgrbzfield = GetItemPos (&dataform, "lgr_bz");
		savefield = currentfield;
		save_fkt (5);
        save_fkt (11);
        save_fkt (12);
        save_fkt (6);
        save_fkt (7);
        if (Lgr.PrepareQuery (NULL, NULL) == 0)
        {
                      Lgr.ShowBuQuery (mamain1, 0);
        }
        else
        {
                      syskey = KEY5;
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        SetAktivWindow (mamain1);
        if (syskey  == KEY5)
        {
           SetCurrentFocus (currentfield);
           return 0;
        }
        sprintf (tous.lgr, "%ld", lgr.lgr);
        sprintf (tous.lgr_bz, "%s", lgr.lgr_bz);
        display_field (mamain1, &dataform.mask[lgrfield], 0, 0);
        display_field (mamain1, &dataform.mask[lgrbzfield], 0, 0);
        PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
        return 0;
}


int ShowPalette (void)
/**
Lager anzeigen.
**/
{
	    int savefield;
		int palettefield;
		int palettebzfield;

		palettefield = GetItemPos (&dataform, "palette");
		palettebzfield = GetItemPos (&dataform, "palette_bz");
		savefield = currentfield;
		save_fkt (5);
        save_fkt (11);
        save_fkt (12);
        save_fkt (6);
        save_fkt (7);
        if (Palette.PrepareQuery (NULL, NULL) == 0)
        {
                      Palette.ShowBuQuery (mamain1, 0);
        }
        else
        {
                      syskey = KEY5;
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        SetAktivWindow (mamain1);
        if (syskey  == KEY5)
        {
           SetCurrentFocus (currentfield);
           return 0;
        }
        sprintf (tous.palette, "%.0lf", a_bas.a);
        sprintf (tous.palette_bz, "%s", a_bas.a_bz1);
        display_field (mamain1, &dataform.mask[palettefield], 0, 0);
        display_field (mamain1, &dataform.mask[palettebzfield], 0, 0);
        PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
        return 0;
}


int ShowLeitw (void)
{
		save_fkt (5);
        save_fkt (11);
        save_fkt (12);
        save_fkt (6);
        save_fkt (7);
	    ptab_class.Show ("leitw_typ");
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5 || syskey == KEYESC) return 0;
		strcpy (tous.leitw_typ, ptabn.ptwert);
		strcpy (tous.leitw_bz, ptabn.ptbezk);
		display_form (mamain1, current_form, 0, 0);
		return 1;
}

int ShowAuto (void)
{
		save_fkt (5);
        save_fkt (11);
        save_fkt (12);
        save_fkt (6);
        save_fkt (7);
	    ptab_class.Show ("auto_typ");
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5 || syskey == KEYESC) return 0;
		strcpy (tous.auto_typ, ptabn.ptwert);
		strcpy (tous.auto_bz, ptabn.ptbezk);
		display_form (mamain1, current_form, 0, 0);
		return 1;
}

int SetKey9Adr (void)
/**
Taste key9 fuer Lagerazeige setzen.
**/
{
       SetFkt (9, ansehen, KEY9);
       set_fkt (ShowAdr,9);
	   return 0;
}
  
	   
int SetKey9Drv (void)
/**
Taste key9 fuer Lagerazeige setzen.
**/
{
       SetFkt (9, ansehen, KEY9);
       set_fkt (QueryDrv,9);
	   return 0;
}
  

int SetKey9Lgr (void)
/**
Taste key9 fuer Lagerazeige setzen.
**/
{
       SetFkt (9, ansehen, KEY9);
       set_fkt (ShowLgr,9);
	   return 0;
}


int SetKey9Palette (void)
/**
Taste key9 fuer Lagerazeige setzen.
**/
{
       SetFkt (9, ansehen, KEY9);
       set_fkt (ShowPalette,9);
	   return 0;
}
  
int SetKey9Leitw (void)
/**
Taste key9 fuer Lagerazeige setzen.
**/
{
       SetFkt (9, ansehen, KEY9);
       set_fkt (ShowLeitw,9);
	   return 0;
}

int SetKey9Auto (void)
/**
Taste key9 fuer Lagerazeige setzen.
**/
{
       SetFkt (9, ansehen, KEY9);
       set_fkt (ShowAuto,9);
	   return 0;
}
  

int DelTour (void)
/**
Satz in Tourenstamm schreiben.
**/
{
        if (abfragejn (mamain1, "Tour l�schen ?", "N") == 0)
        {
            SetCurrentField (currentfield);
            return 0;
        }
        Tour.dbdelete ();
        CloseControls (&dataform);
        commitwork ();
        disp_mess ("Satz wurde gel�scht", 1);
        return (0);
}
                  
int WriteTour (void)
/**
Satz in Tourenstamm schreiben.
**/
{
        FrmtoDB (dbtou);
        Tour.dbupdate ();
        break_enter ();
        DestroyBm ();
        return (0);
}

int BreakTour ()
/**
Aktion bei Taste F5
**/
{
        
        if (abfragejn (mamain2, "Tour speichern ?", "J"))
        {
            return WriteTour ();
        }
        DestroyBm ();
        break_enter ();
        return 1;
}

void ReadPers (void)
/**
Fahrer un Fahrer-Name lesen.
**/
{
	    int dsqlstatus;

	    if (strcmp (tou.fah_1, "            "))
		{
			strcpy (pers.pers, tou.fah_1);
			dsqlstatus = Pers.dbreadfirst ();
			if (dsqlstatus == 0)
			{
				dsqlstatus = Adr.lese_adr (pers.adr);
				if (dsqlstatus == 0)
				{
					strcpy (tous.fah_1_nam, _adr.adr_nam1);
				}
			}
		}
	    if (strcmp (tou.fah_2, "            "))
		{
			strcpy (pers.pers, tou.fah_2);
			dsqlstatus = Pers.dbreadfirst ();
			if (dsqlstatus == 0)
			{
				dsqlstatus = Adr.lese_adr (pers.adr);
				if (dsqlstatus == 0)
				{
					strcpy (tous.fah_2_nam, _adr.adr_nam1);
				}
			}
		}
}



int ReadTour (BOOL withlock)
/**
Satz in Tourenstamm lesen.
**/
{
        int dsqlstatus;

		strcpy (tous.fah_1_nam, " ");
		strcpy (tous.fah_2_nam, " ");
        strcpy (tous.adr_krz, " ");
        tou.tou = atol (tous.tou);
        dsqlstatus = Tour.dbreadfirst ();
        if (dsqlstatus == 100)
        {
            if (IDM_ACTIVE != IDM_WORK)
            {
                disp_mess ("Satz nicht gefunden",2);
                return 0;
            }
            InitForm (&dataform);
            FrmtoDB (dbtou);
        }
        else
        {
            if (withlock)
            {
                 Tour.dblock ();
            }
			ReadPers ();
			lgr.lgr = tou.lgr;
			memset (lgr.lgr_bz, ' ', 24);
            lgr.lgr_bz[24] = 0;
			Lgr.dbreadfirst ();
			a_bas.a = tou.palette;
			memset (a_bas.a_bz1, ' ', 24);
            a_bas.a_bz1[24] = 0;
			ReadA_bas0();

            DBtoFrm (dbtou);
            ReadAdr0 ();
			ReadLeitw0 ();
			ReadAuto0();
            ShowBm ();
        }
        display_form (mamain2, &dataform, 0, 0);
        return (0);
}


void DisableMenu (void)
/**
Mineu auf Inaktiv schalten.
**/
{
	   EnableMenuItem (hMenu, IDM_WORK, MF_GRAYED);
	   EnableMenuItem (hMenu, IDM_SHOW, MF_GRAYED);
	   EnableMenuItem (hMenu, IDM_DEL,  MF_GRAYED);
	   EnableMenuItem (hMenu, IDM_PRINT,MF_GRAYED);

       ToolBar_SetState(hwndTB,IDM_WORK, TBSTATE_INDETERMINATE);
       ToolBar_SetState(hwndTB,IDM_SHOW, TBSTATE_INDETERMINATE);
       ToolBar_SetState(hwndTB,IDM_DEL, TBSTATE_INDETERMINATE);
       ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_INDETERMINATE);
       ToolBar_PressButton(hwndTB,IDM_ACTIVE, TRUE);
}

void EnableMenu (void)
/**
Mineu auf Inaktiv schalten.
**/
{
	   EnableMenuItem (hMenu, IDM_WORK, MF_ENABLED);
	   EnableMenuItem (hMenu, IDM_SHOW, MF_ENABLED);
	   EnableMenuItem (hMenu, IDM_DEL,  MF_ENABLED);
	   EnableMenuItem (hMenu, IDM_PRINT,MF_ENABLED);

       ToolBar_SetState(hwndTB,IDM_WORK, TBSTATE_ENABLED);
       ToolBar_SetState(hwndTB,IDM_SHOW, TBSTATE_ENABLED);
       ToolBar_SetState(hwndTB,IDM_DEL, TBSTATE_ENABLED);
//       ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_ENABLED);
       ToolBar_PressButton(hwndTB,IDM_ACTIVE, TRUE);
}


void EnterData (void)
/**
Daten zur Tour-Nr eingeben.
**/
{
       DisableMenu (); 
       no_break_end ();
       set_fkt (BreakTour, 5);
       set_fkt (WriteTour, 12);
       DisplayAfterEnter (FALSE);
       enter_form (mamain2, &dataform, 0, 0);
       set_fkt (dokey5, 5);
       set_fkt (NULL, 12);
       commitwork ();
       EnableMenu ();
       DisplayAfterEnter (TRUE);
}


void DisplayData (void)
/**
Daten anzeigen.
**/
{
       display_form (mamain2, &dataform, 0, 0);
}


void EnterTour (void)
/**
Tour-Nr eingeben.
**/
{
       set_fkt (InfoTou, 4); 
       while (TRUE)
       {
           break_end ();
           set_fkt (QueryTou, 10);
           SetFkt (10, auswahl, KEY10);
           enter_form (mamain1, &tform, 0, 0);
           set_fkt (NULL, 10);
           SetFkt (10, leer, 0);
           if (syskey == KEY5 || syskey == KEYESC)
           {
               return;
           }

           if (IDM_ACTIVE ==IDM_WORK)
           {
               beginwork ();
               ReadTour (1);
               EnterData ();
               CloseControls (&dataform);
           }
           else if (IDM_ACTIVE ==IDM_SHOW)
           {
               ReadTour (0);
               DisplayData ();
           }
           else if (IDM_ACTIVE ==IDM_DEL)
           {
               beginwork ();
               ReadTour (1);
               DisplayData ();
               set_fkt (DelTour, 12);
           }
       }
       set_fkt (NULL, 4); 
}

void MoveMamain (void)
/**
Koordinaten in $BWSETC lesen.
**/
{
        char *etc;
        char buffer [256];
        FILE *fp;
        int anz;
		static BOOL scrfOK = FALSE;
		RECT rect, rectold;
   	    int xfull, yfull;



	    if (MoveMain == FALSE) return;
        xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
	    if (xfull < 900) return;

     etc = getenv ("BWSETC");
        if (etc == NULL)
        {
             etc = "\\user\\fit\\etc";
        }
        sprintf (buffer, "%s\\fit.rct", etc);

        fp = fopen (buffer, "r");
        if (fp == NULL) return;

        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        fclose (fp);

        anz = wsplit (buffer, " ");
        if (anz < 4) 
        {
            return;
        }

        rect.left   = atoi (wort[0]);
        rect.top    = atoi (wort[1]);
        rect.right  = atoi (wort[2]);
        rect.bottom = atoi (wort[3]);
		rect.left ++; 
		rect.top ++; 
		rect.right  = rect.right  - rect.left - 2;
		rect.bottom = rect.bottom - rect.top - 2;
		GetWindowRect (mamain1, &rectold);

		rectold.right  = rectold.right - rectold.left; 
		rectold.bottom = rectold.bottom - rectold.top; 

		rectold.left   = rect.left - rectold.left;
		rectold.top    = rect.top  - rectold.top;
		rectold.right  = rect.right - rectold.right;
		rectold.bottom = rect.bottom - rectold.bottom;

        MoveWindow (hMainWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);

        return;
}


int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
	   char *mv; 
       char cfg_v [80];

	   GetForeground ();   
       opendbase ("bws");
	   mamain2 = NULL;
       SetStdProc (WndProc);
       InitFirstInstance (hInstance);

       if (ProgCfg.GetCfgValue ("Fah2IsBearbeiter", cfg_v) == TRUE)
       {
                    Fah2IsBearbeiter = atoi (cfg_v);
	   }
       mv = getenv_default ("MOVEMAIN");
 	   if (mv)
	   {
		   MoveMain = min (1, max (0, atoi (mv)));
	   }

       if (Fah2IsBearbeiter)
	   {
	       LPSTR Bearbeiter = "Bearbeiter";
		   memcpy (ifah_2.GetItemText (), Bearbeiter, strlen (Bearbeiter));
	   }
       SetModuleName ("Touren");
       hMainWindow = OpenWindow (26, 80, 2, 0, 0, hInstance);
	   hMenu = MakeMenue (menuetab);
	   SetMenu (hMainWindow, hMenu);
	   hwndTB = MakeToolBarEx (hInstance, 
		                       hMainWindow,tbb, 8,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);

//       DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 1);

       SetAktivWindow (hMainWindow);
       mamain1 = OpenMamain1 (18, 77, 2, 1, hInstance, WndProc,
                                  NULL, NULL);
       InvalidateRect (mamain1, NULL, TRUE);
       ftasten = OpenFktM (hInstance);
       SetAktivWindow (mamain1);
       SetBorder (WS_CHILD |WS_DLGFRAME | WS_VISIBLE);
       mamain2 = OpenWindowCh (15, 77, 3, 0, hMainInst);
       MoveMamain ();
       SetAktivWindow (mamain1);
       DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 1);
       set_fkt (dokey5, 5);
       EnterTour ();
	   SetForeground ();
	   closedbase ();
       return 0;
}

void InitFirstInstance(HANDLE hInstance)
{
        WNDCLASS wc;
        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "ROSIICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hStdWindow";

        RegisterClass(&wc);

        hMainInst = hInstance;
        return;
}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        int cx, cy;
        TEXTMETRIC tm;
        HDC hdc;
        HFONT hFont, oldFont;
        SIZE size;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (0, hdc);

        cx = 78 * tm.tmAveCharWidth;
        cy = 25 * tm.tmHeight;

        hMainWindow = CreateWindow ("hStdWindow",
                                    "Testbox",
                                    WS_DLGFRAME | WS_CAPTION | WS_SYSMENU,
                                    CW_USEDEFAULT, CW_USEDEFAULT,
                                    cx, cy,
                                    NULL,
                                    NULL,
                                    hInstance,
                                    NULL);

        ShowWindow (hMainWindow, SW_SHOW);
        UpdateWindow (hMainWindow);
        DisplayLines ();
        return 0;
}

static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                       return msg.wParam;
                       }
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
        return msg.wParam;
}

void DisplayLines (void)
{
         HDC hdc;

         hdc = GetDC (hMainWindow);
         PrintLines (hdc);
         ReleaseDC (hMainWindow, hdc);
}


void PrintLines (HDC hdc)
/**
Linien am Bildschirm anzeigen.
**/
{
         static TEXTMETRIC tm;
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         RECT rect;
         int x, y;

         GetTextMetrics (hdc, &tm);

         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         GetClientRect (hMainWindow, &rect);
         SelectObject (hdc, hPenG);
         x = rect.right;
         y = 2 * tm.tmHeight;
         y = y + y / 3;
		 y -= 10;
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x, y);

         SelectObject (hdc, hPenW);
         x = rect.right;
         y = 2 * tm.tmHeight;
         y = y + y / 3 + 1;
		 y -= 10;
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x, y);
}

void PrintIcons (HDC hdc)
/**
Icons am Bildschirm anzeigen.
**/
{
         static TEXTMETRIC tm;
         SIZE size;
         int x, y;

         GetTextMetrics (hdc, &tm);
         GetTextExtentPoint32 (hdc, "X", 1, &size);
         tm.tmAveCharWidth = size.cx;
         x = 65 * tm.tmAveCharWidth;
         y = 2 * tm.tmHeight;
         y = y + y / 3;
         DrawIcon (hdc, x, y, hIcon);
}


void MoveMamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
	 RECT rectm1, rectm2;
	 int cx2, cy2;
     int x,y, cx, cy;


     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);
     GetWindowRect (mamain1, &rectm1);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;
     MoveWindow (mamain1, x, y, cx, cy, TRUE);
	 if (mamain2 == NULL) return;
     GetWindowRect (mamain1, &rectm2);
     cx2 = (rectm2.right  - rectm2.left)  - (rectm1.right  - rectm1.left);
     cy2 = (rectm2.bottom - rectm2.top)   - (rectm1.bottom - rectm1.top);
     GetWindowRect (mamain1, &rectm1);
     GetWindowRect (mamain2, &rectm2);
	 x = rectm2.left - rectm1.left;
	 y = rectm2.top  - rectm1.top;
	 cx = (rectm2.right  - rectm2.left) + cx2;
     cy = (rectm2.bottom - rectm2.top)  + cy2;
     MoveWindow (mamain2, x, y, cx, cy, TRUE);
}


LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
        HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                      if (hWnd == mamain1)
                      {
                              hdc = BeginPaint (mamain1, &ps);
                              // PrintLines (hdc);
                              EndPaint (hWnd, &ps);
                      }
                      else if (hWnd == hMainWindow)
                      {
                              hdc = BeginPaint (hMainWindow, &ps);
//                              PrintLines (hdc);
                              EndPaint (mamain1, &ps);
                      }
                      else if (hWnd == mamain2)
                      {
                              hdc = BeginPaint (hMainWindow, &ps);
//                              PrintRect (hdc);
                              EndPaint (mamain1, &ps);
                      }
                      break;
              case WM_MOVE :
                      if (hWnd == hMainWindow)
                      {
                               _MoveBm ();
					  }
                      break;

              case WM_SIZE :
                      if (hWnd == hMainWindow)
                      {
                              MoveFkt ();
                              MoveMamain1 ();  
                              hdc = GetDC (hMainWindow);
                              ReleaseDC (hMainWindow, hdc);
                      }
					  break;
              case WM_NOTIFY :
                    {
                      LPNMHDR pnmh = (LPNMHDR) lParam;

                      if (pnmh->code == TTN_NEEDTEXT)
                      {
                           LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
                           if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                               == FALSE)
                           {
                                    QuickHwndCpy (lpttt);
                           }
                      }
                      break;
                    }

              case WM_DESTROY :
                      if (hWnd == hMainWindow)
                      {
	                         SetForeground ();
	                         closedbase ();
                             ExitProcess (0);
                             return 0;
                      }
                      break;

              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY2)
                    {
                            syskey = KEY2;
                            SendKey (VK_F2);
                            break;
                    }
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
					else if (LOWORD (wParam) == IDM_WORK)
                    {
						    CheckMenuItem (hMenu, IDM_ACTIVE,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_WORK,
								           MF_CHECKED); 
                            ToolBar_PressButton(hwndTB,
                                                IDM_ACTIVE, FALSE);
                            ToolBar_SetState(hwndTB,
                                               IDM_ACTIVE, TBSTATE_ENABLED);
                            ToolBar_PressButton(hwndTB,
                                                IDM_WORK, TRUE);
		  	                IDM_ACTIVE = IDM_WORK;
                            TXT_ACTIVE = TXT_WORK;
                            set_fkt (NULL, 12);
                            DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 1);
                   }
					else if (LOWORD (wParam) == IDM_SHOW)
                    {
						    CheckMenuItem (hMenu, IDM_ACTIVE,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_SHOW,
								           MF_CHECKED); 
                            ToolBar_PressButton(hwndTB,
                                                IDM_ACTIVE, FALSE);
                            ToolBar_SetState(hwndTB,
                                               IDM_ACTIVE, TBSTATE_ENABLED);
                            ToolBar_PressButton(hwndTB,
                                                IDM_SHOW, TRUE);
							IDM_ACTIVE = IDM_SHOW;
                            TXT_ACTIVE = TXT_SHOW;
                            set_fkt (NULL, 12);
                            DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 1);
                    }
					else if (LOWORD (wParam) == IDM_DEL)
                    {
						    CheckMenuItem (hMenu, IDM_ACTIVE,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_DEL,
								           MF_CHECKED); 
                            ToolBar_PressButton(hwndTB,
                                                IDM_ACTIVE, FALSE);
                            ToolBar_SetState(hwndTB,
                                               IDM_ACTIVE, TBSTATE_ENABLED);
                            ToolBar_PressButton(hwndTB,
                                                IDM_DEL, TRUE);
							IDM_ACTIVE = IDM_DEL;
                            TXT_ACTIVE = TXT_DEL;
                            DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 1);
                    }
                    else if (HIWORD (wParam) == CBN_DROPDOWN)
                    {
                                  opencombobox = TRUE;
                                  return 0;
                    }
                    else if (HIWORD (wParam) == CBN_CLOSEUP)
                    {
                           
                                  opencombobox = FALSE;
                                  return 0;
                    }
                    else if (LOWORD (wParam) == IDM_INFO)
                    {
                                  syskey = KEY4;
                                  SendKey (VK_F4);
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

