#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "tou.h"

struct TOU tou, tou_null;

/* Variablen fuer Auswahl mit Buttonueberschrift    */

#include "itemc.h"
#define MAXSORT 10000

static char *sqltext;

static int dosort1 ();
static int dosort2 ();

struct SORT_AW
{
	     char sort1 [10];
	     char sort2 [49];
	     int  sortidx;
};

static struct SORT_AW sort_aw, sort_awtab[MAXSORT];
static int sort_awanz;
static int sort1 = -1;
static int sort2 = -1;
static int sort_idx = 0;

static ITEM isort1    ("", sort_aw.sort1, "", 0);
static ITEM isort2    ("", sort_aw.sort2, "", 0);
static ITEM ifillub   ("", " ",             "", 0); 

static field _fsort_aw[] = {
&isort1,      9, 1, 0,  0, 0, "%8d",       NORMAL, 0, 0, 0,
&isort2,     48, 1, 0, 10, 0, "",          NORMAL, 0, 0, 0,
&ifillub,    80, 1, 0, 58, 0, "",         NORMAL, 0, 0, 0,
};

static form fsort_aw = {2, 0, 0, _fsort_aw, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "Tour",         "", 0);
static ITEM isort2ub    ("", "Bezeichnung",   "", 0);

static field _fsort_awub[] = {
&isort1ub,        9, 1, 0,  0, 0, "", BUTTON, 0, dosort1,    102,
&isort2ub,       49, 1, 0,  9, 0, "", BUTTON, 0, dosort2,    103,
&ifillub,        80, 1, 0, 58, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {3, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0,  9, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 58, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awl = {2, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};

static int dosort10 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return ((int) (atol(el1->sort1) - atol (el2->sort1)) * 
				                                  sort1);
}


static int dosort1 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort10);
	sort1 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static int dosort20 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return (strcmp(el1->sort2, el2->sort2) * sort2);
}


static int dosort2 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort20);
	sort2 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        sort_idx = idx;
        break_list ();
        return;
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

/* Ende Variablen fuer Auswahl mit Buttonueberschrift    */

static void FillUb  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%8s %-13s",
                          "Tour ", "Bezeichnung");
}


static void FillValues  (char *buffer)
{
         sprintf (buffer, "%8ld %s",
                           tou.tou, tou.tou_bz);
}

void TOU_CLASS::prepare (void)
{
         ins_quest ((char *) &tou.tou, 2, 0);

    out_quest ((char *) &tou.tou,2,0);
    out_quest ((char *) tou.tou_bz,0,49);
    out_quest ((char *) tou.fz_kla,0,3);
    out_quest ((char *) tou.fz,0,13);
    out_quest ((char *) tou.srt_zeit,0,6);
    out_quest ((char *) tou.dau,0,6);
    out_quest ((char *) &tou.lng,2,0);
    out_quest ((char *) tou.fah_1,0,13);
    out_quest ((char *) tou.fah_2,0,13);
    out_quest ((char *) &tou.delstatus,1,0);
         cursor = prepare_sql ("select tou.tou,  tou.tou_bz,  "
"tou.fz_kla,  tou.fz,  tou.srt_zeit,  tou.dau,  tou.lng,  tou.fah_1,  tou.fah_2,  "
"tou.delstatus from tou "

#line 162 "tou.rpp"
                               "where tou = ? ");

    ins_quest ((char *) &tou.tou,2,0);
    ins_quest ((char *) tou.tou_bz,0,49);
    ins_quest ((char *) tou.fz_kla,0,3);
    ins_quest ((char *) tou.fz,0,13);
    ins_quest ((char *) tou.srt_zeit,0,6);
    ins_quest ((char *) tou.dau,0,6);
    ins_quest ((char *) &tou.lng,2,0);
    ins_quest ((char *) tou.fah_1,0,13);
    ins_quest ((char *) tou.fah_2,0,13);
    ins_quest ((char *) &tou.delstatus,1,0);
         sqltext = "update tou set tou.tou = ?,  tou.tou_bz = ?,  "
"tou.fz_kla = ?,  tou.fz = ?,  tou.srt_zeit = ?,  tou.dau = ?,  "
"tou.lng = ?,  tou.fah_1 = ?,  tou.fah_2 = ?,  tou.delstatus = ? "

#line 165 "tou.rpp"
                               "where tou = ? ";
  
         ins_quest ((char *) &tou.tou, 2, 0);

         upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &tou.tou,2,0);
    ins_quest ((char *) tou.tou_bz,0,49);
    ins_quest ((char *) tou.fz_kla,0,3);
    ins_quest ((char *) tou.fz,0,13);
    ins_quest ((char *) tou.srt_zeit,0,6);
    ins_quest ((char *) tou.dau,0,6);
    ins_quest ((char *) &tou.lng,2,0);
    ins_quest ((char *) tou.fah_1,0,13);
    ins_quest ((char *) tou.fah_2,0,13);
    ins_quest ((char *) &tou.delstatus,1,0);
         ins_cursor = prepare_sql ("insert into tou (tou,  "
"tou_bz,  fz_kla,  fz,  srt_zeit,  dau,  lng,  fah_1,  fah_2,  delstatus) "

#line 172 "tou.rpp"
                                   "values "
                                   "(?,?,?,?,?,?,?,?,?,?)");

#line 174 "tou.rpp"

         ins_quest ((char *) &tou.tou, 2, 0);
         del_cursor = prepare_sql ("delete from tou "
                               "where tou = ? ");

         ins_quest ((char *) &tou.tou, 2, 0);
         test_upd_cursor = prepare_sql ("select tou from tou "
                               "where tou = ? ");
}

 
int TOU_CLASS::prep_awcursor (void)
/**
Cursor fuer Auswahl vorbereiten.
**/
{
         int cursor_ausw;

         out_quest ((char *) &tou.tou, 2, 0);

         cursor_ausw = prepare_scroll ("select tou, tou_bz from tou "
                                       "where tou > 0 "
                                       "order by tou");
         if (sqlstatus)
         {
                     return (-1);
         }
         return (cursor_ausw);
}
int TOU_CLASS::prep_awcursor (char *sqlstring)
/**
Cursor fuer Auswahl vorbereiten.
**/
{
         int cursor_ausw;


         out_quest ((char *) &tou.tou, 2, 0);

         cursor_ausw = prepare_scroll (sqlstring);
         if (sqlstatus)
         {
                     return (-1);
         }
         return (cursor_ausw);
}


int TOU_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

void TOU_CLASS::fill_aw (int cursor_ausw)
/**
Struktur fuer Auswahl fuellen.
**/
{
 
        sort_awanz = 0; 
	  while (fetch_scroll (cursor_ausw, NEXT) == 0)
        {
			       sprintf (sort_awtab[sort_awanz].sort1, "%ld",
						      tou.tou);
			       sprintf (sort_awtab[sort_awanz].sort2, "%s",
						      tou.tou_bz);
				  sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
				  sort_awanz ++;
                          if (sort_awanz == MAXSORT) break;
        }
        return;
}


int TOU_CLASS::ShowAllBu (HWND hWnd, int ws_flag)
/**
Auswahl ueber Gruppen anzeigen.
**/
{
         int cursor_ausw;

         cursor_ausw = prep_awcursor ();
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         fill_aw (cursor_ausw);

         this->DB_CLASS::ShowAllBu (hWnd,
                                    ws_flag,
                                    cursor_ausw, 
                                    endsort,  
                                    IsAwClck,
                                    (char *) sort_awtab,
                                    sort_awanz,
                                    (char *) &sort_aw,
                                    (int) sizeof (struct SORT_AW),
                                    &fsort_aw,  
                                    &fsort_awl,
                                    &fsort_awub);
	    sort_idx = sort_awtab[sort_idx].sortidx;
        fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
        close_sql (cursor_ausw);
        return (0);
}

int TOU_CLASS::ShowBuQuery (HWND hWnd, int ws_flag, 
								           char *sqlstring )
/**
Auswahl ueber Gruppen anzeigen.
**/
{
         int cursor_ausw;

         cursor_ausw = prep_awcursor (sqlstring);
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         fill_aw (cursor_ausw);

		 if (fsort_aw.mask[0].item == NULL)
		 {
		            disp_mess ("Fehler", 2);
         }

         this->DB_CLASS::ShowAllBu (hWnd,
                                    ws_flag,
                                    cursor_ausw, 
                                    endsort,  
                                    IsAwClck,
                                    (char *) sort_awtab,
                                    sort_awanz,
                                    (char *) &sort_aw,
                                    (int) sizeof (struct SORT_AW),
                                    &fsort_aw,  
                                    &fsort_awl,
                                    &fsort_awub);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 1;
        }
	    sort_idx = sort_awtab[sort_idx].sortidx;
        fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
        close_sql (cursor_ausw);
        return (0);
}


int TOU_CLASS::QueryBu (HWND hWnd, int ws_flag)
/**
Auswahl mit Query-String.
**/
{
	     char sqlstring [1000]; 

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select tou, tou_bz "
							"from tou "
                            "where %s "
							"order by tou", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select tou, tou_bz "
							"from tou "
                            "where tou > 0 order by tou");
         }
         return this->ShowBuQuery (hWnd, ws_flag, sqlstring);
}

int TOU_CLASS::ShowBuQuery (HWND hWnd, int ws_flag) 
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;

	    savecurrent = current_form;
 		sort_awanz = 0;
		while (fetch_scroll (cursor_ausw, NEXT) == 0)
        {
			         sprintf (sort_awtab[sort_awanz].sort1, "%ld",
						      tou.tou);
			         sprintf (sort_awtab[sort_awanz].sort2, "%s",
						      tou.tou_bz);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					 if (sort_awanz == MAXSORT) break;
        }

		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
        eWindow = OpenListWindowEnF (10, 40, 8, 20, 0);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
		Setlistenter (1);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
		 sort_idx = sort_awtab[sort_idx].sortidx;
         fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
         close_sql (cursor_ausw);
         return 0;
}

int TOU_CLASS::PrepareQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 

         ReadQuery (qform, qnamen); 

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select tou, tou_bz "
							"from tou "
                            "where %s "
							"order by tou", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select tou, tou_bz "
							"from tou "
                            "where tou > 0 order by tou");
         }
         out_quest ((char *) &tou.tou, 2, 0);
         out_quest ((char *) tou.tou_bz, 0, 49);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prepare_scroll (sqlstring);
         sql_mode = old_mode;
         if (sqlstatus < 0)
         {
                       return (sqlstatus);
         }
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}

