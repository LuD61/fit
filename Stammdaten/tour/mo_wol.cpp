#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "ptab.h"
#include "mo_wol.h"
#include "mo_qtou.h"
#include "mo_qkun.h"
#include "mo_qfil.h"
#include "mdn.h"
#include "fil.h"
#include "kun.h"
#include "wo_tou.h"
#include "tou.h"
#include "mo_progcfg.h"

#define MAXLEN 40
#define MAXPOS 30000
#define LPLUS 1

extern HANDLE  hMainInst;

static unsigned char ausgabebuffer [0x1000];
static HWND mamain1;

static int ListFocus = 4;
static double RowHeight = 1.5;

static 	int kunsearch;
static long tour_div = 1;
static BOOL kun_test_kz = TRUE;
static BOOL AbrKz = FALSE;

 
struct WO_TOUS 
{
   char     mdn [6];
   char     fil [6];
   char     kun [10];
   char     ta_nr [6];
   char     kun_krz1[18];
   char     kun_fil [3];
   char     posi [10];
   char      mo_tour [10];
   char      mo_lgr  [5];
   char      di_tour [10];
   char      di_lgr  [5];
   char      mi_tour [10];
   char      mi_lgr  [5];
   char      do_tour [10];
   char      do_lgr  [5];
   char      fr_tour [10];
   char      fr_lgr  [5];
   char      sa_tour [10];
   char      sa_lgr  [5];
   char      so_tour [10];
   char      so_lgr  [5];
   char      mo_tourb [50];
   char      di_tourb [50];
   char      mi_tourb [50];
   char      do_tourb [50];
   char      fr_tourb [50];
   char      sa_tourb [50];
   char      so_tourb [50];
};

struct WO_TOUS wo_tous, wo_toutab [MAXPOS];

char *toutab[] = {0,
                  wo_tous.mo_tour,
                  wo_tous.di_tour, 
                  wo_tous.mi_tour, 
                  wo_tous.do_tour, 
                  wo_tous.fr_tour, 
                  wo_tous.sa_tour, 
                  wo_tous.so_tour, 
				  0,
};

char *toubtab[] = {0,
                  wo_tous.mo_tourb,
                  wo_tous.di_tourb, 
                  wo_tous.mi_tourb, 
                  wo_tous.do_tourb, 
                  wo_tous.fr_tourb, 
                  wo_tous.sa_tourb, 
                  wo_tous.so_tourb, 
				  0,
};

char *toultab[] = {0,
                  wo_tous.mo_lgr,
                  wo_tous.di_lgr, 
                  wo_tous.mi_lgr, 
                  wo_tous.do_lgr, 
                  wo_tous.fr_lgr, 
                  wo_tous.sa_lgr, 
                  wo_tous.so_lgr, 
};

static int touanz = 7;

ITEM iposi        ("posi",        wo_tous.posi,       "", 0);
ITEM ikun         ("kun",         wo_tous.kun,        "", 0);
ITEM ikun_krz1    ("kun_krz1",    wo_tous.kun_krz1,   "", 0);
ITEM ita_nr       ("ta_nr",       wo_tous.ta_nr,      "", 0);
ITEM imo_tour     ("mo_tour",     wo_tous.mo_tour,    "", 0);
ITEM imo_lgr      ("mo_lgr",      wo_tous.mo_lgr,     "", 0);

ITEM idi_tour     ("di_tour",     wo_tous.di_tour,    "", 0);
ITEM idi_lgr      ("mo_lgr",      wo_tous.di_lgr,     "", 0);

ITEM imi_tour     ("mi_tour",     wo_tous.mi_tour,    "", 0);
ITEM imi_lgr      ("mo_lgr",      wo_tous.mi_lgr,     "", 0);

ITEM ido_tour     ("do_tour",     wo_tous.do_tour,    "", 0);
ITEM ido_lgr      ("mo_lgr",      wo_tous.do_lgr,     "", 0);

ITEM ifr_tour     ("fr_tour",     wo_tous.fr_tour,    "", 0);
ITEM ifr_lgr      ("mo_lgr",      wo_tous.fr_lgr,     "", 0);

ITEM isa_tour     ("sa_tour",     wo_tous.sa_tour,    "", 0);
ITEM isa_lgr      ("mo_lgr",      wo_tous.sa_lgr,     "", 0);

ITEM iso_tour     ("so_tour",     wo_tous.so_tour,    "", 0);
ITEM iso_lgr      ("mo_lgr",      wo_tous.so_lgr,     "", 0);

ITEM imo_tourb    ("mo_tourb",    wo_tous.mo_tourb,    "", 0);
ITEM idi_tourb    ("di_tourb",    wo_tous.di_tourb,    "", 0);
ITEM imi_tourb    ("mi_tourb",    wo_tous.mi_tourb,    "", 0);
ITEM ido_tourb    ("do_tourb",    wo_tous.do_tourb,    "", 0);
ITEM ifr_tourb    ("fr_tourb",    wo_tous.fr_tourb,    "", 0);
ITEM isa_tourb    ("sa_tourb",    wo_tous.sa_tourb,    "", 0);
ITEM iso_tourb    ("so_tourb",    wo_tous.so_tourb,    "", 0);

ITEM islash        ("",            "/",                 "", 0);   


static field  _dataform[] = {
&ikun,        16, 0, 0, 7, 0, "%8d", DISPLAYONLY, 0, 0, 0,

&imo_tour,     5, 0, 0,24, 0, "%4d",EDIT,        0, 0, 0, 
&islash,       1, 0, 0,29, 0, "C",   DISPLAYONLY, 0, 0, 0,
&imo_lgr,      3, 0, 0,30, 0, "%2d", EDIT,        0, 0, 0, 

&idi_tour,     5, 0, 0,34, 0, "%4d", EDIT,        0, 0, 0, 
&islash,       1, 0, 0,39, 0, "C",    DISPLAYONLY, 0, 0, 0,
&idi_lgr,      3, 0, 0,40, 0, "%2d", EDIT,        0, 0, 0, 

&imi_tour,     5, 0, 0,44, 0, "%4d", EDIT,        0, 0, 0, 
&islash,       1, 0, 0,49, 0, "C",    DISPLAYONLY, 0, 0, 0,
&imi_lgr,      3, 0, 0,50, 0, "%2d", EDIT,        0, 0, 0, 

&ido_tour,     5, 0, 0,54, 0, "%4d", EDIT,        0, 0, 0, 
&islash,       1, 0, 0,59, 0, "C",    DISPLAYONLY, 0, 0, 0,
&ido_lgr,      3, 0, 0,60, 0, "%2d", EDIT,        0, 0, 0, 

&ifr_tour,     5, 0, 0,64, 0, "%4d", EDIT,        0, 0, 0, 
&islash,       1, 0, 0,59, 0, "C",    DISPLAYONLY, 0, 0, 0,
&ifr_lgr,      3, 0, 0,70, 0, "%2d", EDIT,        0, 0, 0, 

&isa_tour,     5, 0, 0,74, 0, "%4d", EDIT,        0, 0, 0, 
&islash,       1, 0, 0,79, 0, "C",    DISPLAYONLY, 0, 0, 0,
&isa_lgr,      3, 0, 0,80, 0, "%2d", EDIT,        0, 0, 0, 

&iso_tour,     5, 0, 0,84, 0, "%4d", EDIT,        0, 0, 0, 
&islash,       1, 0, 0,89, 0, "C",    DISPLAYONLY, 0, 0, 0,
&iso_lgr,      3, 0, 0,90, 0, "%2d", EDIT,        0, 0, 0, 

&ikun_krz1,   16, 0, 1, 7, 0, "", DISPLAYONLY,    0, 0, 0,
&imo_tourb,    9, 0, 1,24, 0, "", DISPLAYONLY,    0, 0, 0, 
&idi_tourb,    9, 0, 1,34, 0, "", DISPLAYONLY,    0, 0, 0, 
&imi_tourb,    9, 0, 1,44, 0, "", DISPLAYONLY,    0, 0, 0, 
&ido_tourb,    9, 0, 1,54, 0, "", DISPLAYONLY,    0, 0, 0, 
&ifr_tourb,    9, 0, 1,64, 0, "", DISPLAYONLY,    0, 0, 0, 
&isa_tourb,    9, 0, 1,74, 0, "", DISPLAYONLY,    0, 0, 0, 
&iso_tourb,    9, 0, 1,84, 0, "", DISPLAYONLY,    0, 0, 0, 
};

static form dataform = {30, 0, 0, _dataform, 0, 0, 0, 0, NULL}; 

static int ubrows[] = {0, 
                       1, 1, 1,
                       2, 2, 2,
                       3, 3, 3,
                       4, 4, 4,
                       5, 5, 5,
                       6, 6, 6,
                       7, 7, 7,
                       8, 9, 10,
                       11, 12, 13, 14, 15};

struct CHATTR ChAttr [] = {"kun",   DISPLAYONLY, EDIT,
                            NULL,  0,           0};

static FRMDB dbwo_tou[] = {
&dataform, 0, (char *) &wo_tou.kun,        2, 0, "%ld",
&dataform, 1, (char *) &wo_tou.mo_tour,     2, 0, "%ld",
&dataform, 4, (char *) &wo_tou.di_tour,     2, 0, "%ld",
&dataform, 7, (char *) &wo_tou.mi_tour,     2, 0, "%ld",
&dataform,10, (char *) &wo_tou.do_tour,     2, 0, "%ld",
&dataform,13, (char *) &wo_tou.fr_tour,     2, 0, "%ld",
&dataform,16, (char *) &wo_tou.sa_tour,     2, 0, "%ld",
&dataform,19, (char *) &wo_tou.so_tour,     2, 0, "%ld",
&dataform,22, (char *)  wo_tou.kun_krz1,     0,16, "",
// &dataform,23, (char *)  wo_tou.kun_krz1,     0,16, "",
NULL,      0,          NULL,         0, 0, "",
};


static char *KText = "Kunde"; 
static char *FText = "Filiale"; 
static char *Kun_Fil = KText; 

ITEM iuposi        ("posi",         wo_tous.posi,  "", 0);
ITEM iukun         ("kun",          Kun_Fil,       "", 0);
ITEM iumo_tour     ("mo_tour",     "Montag",      "", 0);
ITEM iudi_tour     ("di_tour",     "Dientag",     "", 0);
ITEM iumi_tour     ("mi_tour",     "Mittwoch",    "", 0);
ITEM iudo_tour     ("do_tour",     "Donnerstag",  "", 0);
ITEM iufr_tour     ("fr_tour",     "Freitag",     "", 0);
ITEM iusa_tour     ("sa_tour",     "Samstag",     "", 0);
ITEM iuso_tour     ("so_tour",     "Sonntag",     "", 0);

static field  _ubform[] = {
&iukun,        17, 0, 0, 6, 0, "", BUTTON,        0, 0, 0,
&iumo_tour,    10, 0, 0,23, 0, "", BUTTON,        0, 0, 0,
&iudi_tour,    10, 0, 0,33, 0, "", BUTTON,        0, 0, 0, 
&iumi_tour,    10, 0, 0,43, 0, "", BUTTON,        0, 0, 0, 
&iudo_tour,    10, 0, 0,53, 0, "", BUTTON,        0, 0, 0, 
&iufr_tour,    10, 0, 0,63, 0, "", BUTTON,        0, 0, 0, 
&iusa_tour,    10, 0, 0,73, 0, "", BUTTON,        0, 0, 0, 
&iuso_tour,    10, 0, 0,83, 0, "", BUTTON,        0, 0, 0, 
};


static form ubform = {9,0, 0, _ubform, 0, 0, 0, 0, NULL}; 

ITEM iline ("", "1", "", 0);


static field  _lineform[] = {
&iline,      1, 0, 0,23, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0,33, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0,43, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0,53, 0, "",  NORMAL, 0, 0, 0, 
&iline,      1, 0, 0,63, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,73, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,83, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,93, 0, "",  NORMAL, 0, 0, 0,
};

static form lineform = {8, 0, 0, _lineform, 0, 0, 0, 0, NULL}; 


static ListClassDB eListe;
static QueryKun QClass;
static QueryFil QFClass;
static QueryTou QTClass;
static WO_TOU_CLASS Wotour;
static TOU_CLASS tou_class;
static KUN_CLASS kun_class;
static FIL_CLASS fil_class;
static PROG_CFG ProgCfg ("wo_tour");


static char KunItem[] = {"kuna"};
static double AktKun;
static BOOL auf_me_pr_0;

static int InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System
**/
{

    clipped (*Item);
//    if (strcmp (*Item, "a") == 0)
    {
//        *Item = KunItem;
//        sprintf (where, "where kun = %ld and a = %.0lf", 
//                         aufk.kun, ratod (Value));
//        return 1;
    }
    return 0;
}

static int InfoKun (char **Item, char *Value, char *where)
/**
Info auf Feld Kunde.
**/
{

        eListe.GetFocusText ();
        *Item = "kun";
        if (atol (wo_tous.kun))
        {
            sprintf (where, "where kun = %ld", atol (wo_tous.kun));
        }
        else
        {
            sprintf (where, "where kun > 0");
        }
        return 1;
}

static int InfoTou (char **Item, char *Value, char *where)
/**
Info auf Feld Kunde.
**/
{
        int col;

        eListe.GetFocusText ();
        col = eListe.GetAktColumn ();
		col = (col - 1) / 3 + 1;
        *Item = "tou";
        if (atol (toutab[col]))
        {
            sprintf (where, "where tou = %ld", atol (toutab[col]));
        }
        else
        {
            sprintf (where, "where tou = 1");
        }
        return 1;
}





WOTLIST:: WOTLIST ()
{
    int i;

    mamainmax = 0;
    mamainmin = 0; 
    eingabesatz = NULL;
    ausgabesatz = ausgabebuffer;
    zlen = 0;
    feld_anz = 0;
    banz = 0;
    PageView = 0;
    LstZiel = NULL;
    LstSatz   = NULL;
    Lstbanz = 0;
    SwRecs = 0;
    AktRow = 0;
    AktColumn = 0;
    scrollpos = 0;
    InfoCaption[0] = (char) 0; 
    WithMenue   = 1;
    WithToolBar = 1;
	tour_div = 1;
    dataform.after  = WriteRow; 
    dataform.before = SetRowItem; 
    dataform.mask[0].before = SaveKun; 
    dataform.mask[0].after  = fetchKun; 
    for (i = 0; i < 7; i ++)
    {
           dataform.mask[i * 3 + 1].before = SetTouKey9; 
           dataform.mask[i * 3 + 1].after  = TestTou; 
    }
    ListAktiv = 0;
    this->hMainWindow = NULL;
}

void WOTLIST::SetAbrKz (BOOL kz)
{
     AbrKz = max (0, min (1, kz));
}

void WOTLIST::SetTourDiv (long div)
/**
Tour-Dov setzen und Maske anpassen.
**/
{
	int i;

	tour_div = div;  
	if (tour_div == 1000)
	{
		for (i = 1; i < 22; i += 3)
		{
                dataform.mask[i].length = 4;
                dataform.mask[i].picture = "%3d";
				dataform.mask[i + 1].pos[1] = dataform.mask[i].pos[1] + 4;
				dataform.mask[i + 2].length = 4;
                dataform.mask[i].picture = "%3d";
				dataform.mask[i + 2].pos[1] = dataform.mask[i + 1].pos[1] + 1;
		}
	}
	else if (tour_div == 100)
	{
		for (i = 1; i < 22; i += 3)
		{
                dataform.mask[i].length = 5; 
                dataform.mask[i].picture = "%4d";
				dataform.mask[i + 1].pos[1] = dataform.mask[i].pos[1] + 5;
				dataform.mask[i + 2].length = 3;
                dataform.mask[i].picture = "%2d";
				dataform.mask[i + 2].pos[1] = dataform.mask[i + 1].pos[1] + 1;
		}
	}
	else if (tour_div == 1)
	{
		for (i = 1; i < 22; i += 3)
		{
                dataform.mask[i].length = 4;    
                dataform.mask[i].picture = "%3d";
				dataform.mask[i + 1].pos[1] = dataform.mask[i].pos[1] + 4;
				dataform.mask[i + 2].length = 4;
                dataform.mask[i].picture = "%3d";
				dataform.mask[i + 2].pos[1] = dataform.mask[i + 1].pos[1] + 1;
		}
	}
}

void WOTLIST::GetCfgValues (void)
/**
Werte aus 51100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [5];

	   if (cfgOK) return;

	   cfgOK = TRUE;
       if (ProgCfg.GetCfgValue ("kunsearch", cfg_v) ==TRUE)
       {
                    kunsearch = atoi (cfg_v);
       }
       else
       {
                    kunsearch = 0;
       }
       if (ProgCfg.GetCfgValue ("searchmodekun", cfg_v) == TRUE)
       {
		            kun_class.SetSearchModeKun (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchfieldkun", cfg_v) == TRUE)
       {
		           kun_class.SetSearchFieldKun (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("kun_test_kz", cfg_v) == TRUE)
       {
		           kun_test_kz = atoi (cfg_v);
       }
}


void WOTLIST::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return;

         dataform.mask[i].attribut = attr;
}


int WOTLIST::SetRowItem (void)
{
//       eListe.SetRowItem ("a", aufptab[eListe.GetAktRow ()].a);
       return 0;
}

int WOTLIST::TestAppend (void)
/**
Testen, ob ein Satz angehaengt werden darf.
**/
{

    if (eListe.GetRecanz () == 0);
    else if (atol (wo_tous.kun) == (double) 0.0)
    {
        return FALSE;
    }

  
    eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
    return TRUE;
}

int WOTLIST::DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        wo_tou.kun = atol (wo_tous.kun);
        if (wo_tou.kun != 0l)
        {
                Wotour.dbdelete ();
        }
        eListe.DeleteLine ();
        return 0;
}

int WOTLIST::InsertLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
        eListe.InsertLine ();
        return 0;
}

int WOTLIST::AppendLine (void)
/**
Zeile an Liste anfuegen.
**/
{
        eListe.AppendLine ();
        return 0;
}

int WOTLIST::WriteRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{

    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    if (atol (wo_tous.kun) == 0l)
    {
        eListe.DeleteLine ();
        return (1);
    }
    return (0);
}

void WOTLIST::GenNewPosi (void)
/**
Neue Positionsnummern in 10-er Schritten generieren.
**/
{
    int i;
    int row;
    int recs;
    long posi;

    row  = eListe.GetAktRow ();
    memcpy (&wo_toutab[row], &wo_tous, sizeof (struct WO_TOUS));
    recs = eListe.GetRecanz ();
    posi = 10;
    for (i = 0; i < recs; i ++, posi += 10)
    {
        sprintf (wo_toutab[i].posi, "%ld", posi);
    }
    eListe.DisplayList ();
    memcpy (&wo_tous, &wo_toutab[row], sizeof (struct WO_TOUS));
}


int WOTLIST::PosiEnd (long posi)
/**
Positionnummer testen.
**/
{
    int row;
    int recs;
    long nextposi;

    row  = eListe.GetAktRow ();
    recs = eListe.GetRecanz ();

    if (row >= recs - 1)
    {
            return FALSE;
    }

    nextposi = atol (wo_toutab [row + 1].posi);
    if (nextposi <= posi)
    {
        GenNewPosi ();
        return TRUE;
    }
    return FALSE;
}

int WOTLIST::TouInfo (void)
{
       int col;

       col = eListe.GetAktColumn ();
       tou.tou =  atol (toutab[col]);
       if (tou.tou == 0l) return 0;
       QTClass.infotou (eListe.Getmamain3 ());
       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);
       SetFkt (11, vollbild, KEY11);
       return 0;
}


int WOTLIST::QueryTou (void)
{
       int col;
       int ret;

       col = eListe.GetAktColumn ();
	   col = (col - 1) / 3 + 1;
       ret = QTClass.querytou (eListe.Getmamain3 ());
       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);
       SetFkt (11, vollbild, KEY11);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
           return 0;
       }
       sprintf (toutab[col], "%ld", tou.tou);
       
       memcpy (&wo_toutab[eListe.GetAktRow()], &wo_tous, sizeof (struct WO_TOUS));
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

int WOTLIST::Querykun (void)
{
       int ret;
	   QClass.SetTestKunProc (NULL);
       ret = QClass.querykun (eListe.Getmamain3 ());
       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);
       SetFkt (11, vollbild, KEY11);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
           return 0;
       }
       sprintf (wo_toutab[eListe.GetAktRow ()].kun, "%ld", kun.kun);
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

int WOTLIST::TestKunList (long dkun)
/**
Test, ob der Kunde schon zugeordnet ist.
**/
{
	   int zeile;
	   int recanz;

	   recanz = eListe.GetRecanz ();
	   for (zeile = 0; zeile < recanz; zeile ++)
	   {
                if (dkun == atol (wo_toutab[zeile].kun))
				{
					return TRUE;
				}
	   }
    
       return FALSE;
}
       
int WOTLIST::SearchKun (void)
{
	   int zeile;
	   int recanz;

       int ret;

	   if (kunsearch)
	   {
	             QClass.SetTestKunProc (TestKunList);
	   }
       ret = QClass.querykun (eListe.Getmamain3 ());
       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);
       SetFkt (11, vollbild, KEY11);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
           return 0;
       }

	   recanz = eListe.GetRecanz ();
	   for (zeile = 0; zeile < recanz; zeile ++)
	   {
                if (kun.kun == atol (wo_toutab[zeile].kun))
				{
					break;
				}
	   }
   
       if (zeile < recanz)
	   {
                 eListe.SetVPos (zeile);
                 eListe.SetPos (zeile, eListe.FirstColumn ());
                 eListe.ShowAktRow ();
                 eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
	   }
	   else
	   {
	              eListe.AppendLine ();
                  sprintf (wo_toutab[eListe.GetAktRow ()].kun, "%ld", kun.kun);
                  eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                  eListe.ShowAktRow ();
                  eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
                  PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
	   }
       return 0;
}

       
int WOTLIST::QueryFil (void)
{
       int ret;

       ret = QFClass.queryfil (eListe.Getmamain3 ());
       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);
       SetFkt (11, vollbild, KEY11);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                 eListe.GetAktColumn ());
           return 0;
       }
       sprintf (wo_toutab[eListe.GetAktRow ()].kun, "%hd", _fil.fil);
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}
       
int WOTLIST::SetTouKey9 (void)
{
       set_fkt (QueryTou, 9);
       set_fkt (SearchKun, 10);
       SetFkt (9, auswahl, KEY9);
       SetFkt (10, suchen, KEY10);
       eListe.SetInfoProc (InfoTou);
       return 0;
}

int WOTLIST::TestTou (void)
/**
Tour holen.
**/
{
       int dsqlstatus;
       long posi;
       int i;
       int col;

//       SetFkt (9, leer, NULL);
       posi = atol (wo_tous.posi);
       if (posi == 0)
       {
             i = eListe.GetAktRow ();
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (wo_toutab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (wo_tous.posi, "%ld", posi);
             }
       }

       col = eListe.GetAktColumn ();

       col = (col - 1) / 3 + 1;
       tou.tou = atol (toutab[col]);
       if (tou.tou == 0l) 
	   {
		         strcpy (tou.tou_bz, " ");
                 sprintf (toubtab[col], "%s",  tou.tou_bz);
       
                 memcpy (&wo_toutab[eListe.GetAktRow()], &wo_tous, sizeof (struct WO_TOUS));
                 eListe.ShowAktRow ();
                 set_fkt (NULL, 9);
		         return 0;
	   }

       dsqlstatus = tou_class.dbreadfirst ();
       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB 
               && eListe.IsAppend ())
           {
               return (-1);
           }
           print_mess (2, "Tour %.ld nicht gefunden", tou.tou);
                          
           sprintf (toutab[col], "%ld", 0l);
           memcpy (&wo_toutab[eListe.GetAktRow ()], &wo_tous, sizeof (struct WO_TOUS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

       sprintf (toubtab[col], "%s",  tou.tou_bz);
       
       memcpy (&wo_toutab[eListe.GetAktRow()], &wo_tous, sizeof (struct WO_TOUS));
       
       eListe.ShowAktRow ();
       set_fkt (NULL, 9);
       return 0;
}


int WOTLIST::SaveKun (void)
{
       AktKun = atol (wo_tous.kun);
       if (wo_tou.kun_fil == 0)
       {
               set_fkt (Querykun, 9);
       }
       else
       {
               set_fkt (QueryFil, 9);
       }

       SetFkt (9, auswahl, KEY9);
       set_fkt (NULL, 10);
       SetFkt (10, leer, 0);
       eListe.SetInfoProc (InfoKun);
       return 0;
}

int WOTLIST::KunInWoTou (void)
/**
Test, ob schon ein Eintrag fuer den Kunden existiert.
**/
{
	    int i;
		int anz;
		int aktrow;

        aktrow = eListe.GetAktRow ();
        anz = eListe.GetRecanz ();
		for (i = 0; i < anz; i ++)
		{
			if (i == aktrow) continue;
			if (atol (wo_tous.kun) == atol (wo_toutab[i].kun))
			{
				break;
			}
		}
		if (i == anz) return 0;

		if (AbrKz)
		{
                  if (abfragejn (mamain1, 
					  "Der Kunde wurde schon angelegt. Eintrag anzeigen ?", "J") == 0)
				  {
			             return 1;
				  }
		}
		else if (kun_test_kz == FALSE)
		{
			      return 0; 
        } 
		DeleteLine ();
		if (aktrow < i && i > 0) i --;
        eListe.SetVPos (i);
        eListe.SetPos (i , eListe.FirstColumn ());
		return 2;
}


int WOTLIST::fetchKun (void)
/**
Kunde holen.
**/
{
       int dsqlstatus;
       long posi;
       int i;

       SetFkt (9, leer, NULL);
       posi = atol (wo_tous.posi);
       if (posi == 0)
       {
             i = eListe.GetAktRow ();
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (wo_toutab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (wo_tous.posi, "%ld", posi);
             }
       }

	   switch (KunInWoTou ())
	   {
	         case 0 :
		           break;
			 case 1:
                   if (syskey != KEYCR &&syskey != KEYTAB 
                          && eListe.IsAppend ())
				   {
                            return (-1);
				   }
                   sprintf (wo_toutab[eListe.GetAktRow ()].kun, "%ld", AktKun);
                   memcpy (&wo_tous, &wo_toutab[eListe.GetAktRow ()], sizeof (struct WO_TOUS));
                   eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                   eListe.ShowAktRow ();
                   return (-1);
			 case 2:
				   return 0;;
       }
	   

       dsqlstatus = kun_class.lese_kun (wo_tou.mdn, wo_tou.fil, 
                                        atol (wo_tous.kun));
       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB 
               && eListe.IsAppend ())
           {
               return (-1);
           }
           print_mess (2, "Kunde %.ld nicht gefunden",
                          atol (wo_tous.kun));
           sprintf (wo_toutab[eListe.GetAktRow ()].kun, "%ld", AktKun);
           memcpy (&wo_tous, &wo_toutab[eListe.GetAktRow ()], sizeof (struct WO_TOUS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

       sprintf (wo_tous.kun_krz1,       "%s",     kun.kun_krz1);
       
       memcpy (&wo_toutab[eListe.GetAktRow()], &wo_tous, sizeof (struct WO_TOUS));
       
       eListe.ShowAktRow ();
//       eListe.SetRowItem ("a", aufptab[eListe.GetAktRow ()].a);
       set_fkt (NULL, 9);
       return 0;
}

int WOTLIST::fetchFil (void)
/**
Kunde holen.
**/
{
       int dsqlstatus;
       long posi;
       int i;

       SetFkt (9, leer, NULL);
       posi = atol (wo_tous.posi);
       if (posi == 0)
       {
             i = eListe.GetAktRow ();
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (wo_toutab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (wo_tous.posi, "%ld", posi);
             }
       }

       dsqlstatus = fil_class.lese_fil (wo_tou.mdn, 
                                   (short) atol (wo_tous.kun));
       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB 
               && eListe.IsAppend ())
           {
               return (-1);
           }
           print_mess (2, " Filiale %.ld nicht gefunden",
                          atol (wo_tous.kun));
           sprintf (wo_toutab[eListe.GetAktRow ()].kun, "%ld", AktKun);
           memcpy (&wo_tous, &wo_toutab[eListe.GetAktRow ()], sizeof (struct WO_TOUS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

       sprintf (wo_tous.kun_krz1,       "%s",     _adr.adr_krz);
       
       memcpy (&wo_toutab[eListe.GetAktRow()], &wo_tous, sizeof (struct WO_TOUS));
       
       eListe.ShowAktRow ();
//       eListe.SetRowItem ("a", aufptab[eListe.GetAktRow ()].a);
       set_fkt (NULL, 9);
       return 0;
}

void WOTLIST::WritePos (int pos)
/**
Position schreiben.
**/
{
       int col;

       memcpy (&wo_tous, &wo_toutab[pos], sizeof (WO_TOUS));
       wo_tou.posi  = (pos + 1) * 10;
       for (col = 1; col <= touanz; col ++)
       {
               sprintf (toutab[col], "%d", atol (toutab[col]) * tour_div + 
                                           atoi (toultab[col]));
       }
       FrmtoDB (dbwo_tou);
       if (wo_tou.kun ==  0l)
       {
           return;
       }
       Wotour.dbupdate ();
}

int WOTLIST::TestRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
     
    if (atol (wo_tous.kun) == 0l) return 0;
    return (0);
}

int WOTLIST::WriteAllPos (void)
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;
    int row;

    row     = eListe.GetAktRow ();
//    if (eListe.TestAfterRow () == -1)
	if (TestRow () == -1)
    {
            eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                  eListe.GetAktColumn ());
            return -1;
    }
    memcpy (&wo_toutab[row], &wo_tous, sizeof (struct WO_TOUS));
    recs = eListe.GetRecanz ();
 //   Wotour.dbdelete ();
//    GenNewPosi ();
    for (i = 0; i < recs; i ++)
    {
        WritePos (i);
    }
    eListe.BreakList ();
    commitwork ();
    return 0;
}


void WOTLIST::SaveWotou (void)
{
    save_fkt (5);
    save_fkt (6);
    save_fkt (7);
    save_fkt (8);
    save_fkt (12);
}

void WOTLIST::SetWotou (void)
{
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);
    set_fkt (AppendLine, 8);
    set_fkt (WriteAllPos, 12);
}

void WOTLIST::RestoreWotou (void)
{
    restore_fkt (5);
    restore_fkt (6);
    restore_fkt (7);
    restore_fkt (8);
    restore_fkt (12);
}

int WOTLIST::dokey5 (void)
/**
Listenerfassung abbrechen.
**/
{
    if (abfragejn (mamain1, "Positionen speichern ?", "J"))
    {
           WriteAllPos ();
           syskey = KEY12;
    }
    syskey = KEY5;
    RestoreWotou ();
    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
    eListe.BreakList ();
    rollbackwork ();
    return 1;
}


void WOTLIST::InitSwSaetze (void)
{
       int i;

       for (i = 0; i < MAXPOS; i ++)
       {
               SwSaetze [i] = (char *) &wo_toutab [i];
       }
}

void WOTLIST::SetRecHeight (void)
/**
Anzahl Zeilen pro Listzeile ermitteln.
**/
{
       int height;
       int i;

       height = 0;
       for (i = 0; i < dataform.fieldanz; i ++)
       {
           if (dataform.mask[i].attribut & REMOVED) continue;
           if (dataform.mask[i].pos[0] > height)
           {
               height = dataform.mask[i].pos[0];
           }
       }
       height ++; 
       eListe.SetRecHeight (height);
}

int WOTLIST::ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       eListe.SetRecanz (pos + 1);
       return 0;
}


void WOTLIST::SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{

       return;
}


void WOTLIST::uebertragen (void)
/**
Eingabesatz in ASCII-Striktur uebertragen.
**/
{
       int col;
       int dsqlstatus;
       int tou_lgr;

       DBtoFrm (dbwo_tou); 

       for (col = 1; col <= touanz; col ++)
       {
               memcpy (&tou, &tou_null, sizeof (TOU));
               tou.tou = atol (toutab[col]);
               tou_lgr = tou.tou % tour_div;
               sprintf (toultab[col], "%d", tou_lgr);
               if (tour_div > 0) tou.tou /= tour_div; 
               sprintf (toutab[col], "%d", tou.tou);
               if (tou.tou)
               {
                     dsqlstatus = tou_class.dbreadfirst ();
                     if (dsqlstatus == 100)
                     {
                         sprintf (toutab[col], "%ld", 0l);
                     }
               }
               sprintf (toubtab[col], "%s",  tou.tou_bz);
       }
}


void WOTLIST::ShowDB (void)
/**
Auftragspositionen lesen und anzeigen.
**/
{
        int i;
        HCURSOR oldcursor;
        extern short do_exit;
        int dsqlstatus;


        InitSwSaetze ();
        eListe.SetRecanz (0);
        i = eListe.GetRecanz ();
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        eListe.SetUbForm (&ubform);
        dsqlstatus = Wotour.dbreadfirst ("order by kun");
        while (dsqlstatus == 0)
        {
                     uebertragen ();
                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXPOS) break; 
                     dsqlstatus = Wotour.dbread ();
        }

        SetRecHeight ();
        SetCursor (oldcursor);
        i = eListe.GetRecanz ();

        eListe.SetRecanz (i);
        SwRecs = eListe.GetRecanz ();

        eListe.SetSaetze (SwSaetze);
        eListe.SetDataForm0 (&dataform, &lineform);
        eListe.SetChAttr (ChAttr); 
        eListe.SetUbRows (ubrows);
        if (i == 0)
        {
                 eListe.AppendLine ();
                 i = eListe.GetRecanz ();
        }
        SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
        InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
}


void WOTLIST::ReadDB (void)
/**
Auftragspositionen lesen.
**/
{
        ausgabesatz = (unsigned char *) &wo_tous;
        zlen = sizeof (struct WO_TOUS);

        PageView = 0;
        eListe.SetPageView (0);
        feld_anz = dataform.fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Setzlen (zlen);
        eListe.Initscrollpos ();
        eListe.SetAusgabeSatz (ausgabesatz);
}

void WOTLIST::Enter (short mdn, short fil, short kun_fil)
/**
Auftragsliste bearbeiten.
**/

{
       static int initenter = 0;

	   GetCfgValues ();
       beginwork ();
       wo_tou.mdn = mdn;
       wo_tou.fil = fil;
       wo_tou.kun_fil = kun_fil;

       kun.mdn = mdn;
       _fil.mdn = mdn;

       if (kun_fil == 0)
       {
           dataform.mask[0].after  = fetchKun; 
           Kun_Fil = KText;
       }
       else
       {
           dataform.mask[0].after  = fetchFil; 
           Kun_Fil = FText;
       }
       strcpy (iukun.GetFeldPtr (), Kun_Fil);

       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       set_fkt (Schirm, 11);

       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);
       SetFkt (11, vollbild, KEY11);
       if (initenter == 0)
       {
                 eListe.SetInfoProc (InfoProc);
                 eListe.SetTestAppend (TestAppend);
 //              sprintf (InfoCaption, "Auftrag %ld", auf);
                 mamain1 = CreateMainWindow ();
                 eListe.InitListWindow (mamain1);
                 ReadDB ();
                 initenter = 1;
       }

       eListe.SetListFocus (ListFocus);
	   eListe.SetRowHeight (RowHeight);
       eListe.Initscrollpos ();
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB ();

//       eListe.SetRowItem ("a", aufptab[0].a);
       
       ListAktiv = 1;
       eListe.ProcessMessages ();

       InitMax ();
       InitMin ();
       MoveMamain1 ();

       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (9, leer, NULL);
       SetFkt (10, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 6);
       set_fkt (NULL, 7);
       set_fkt (NULL, 8);
       set_fkt (NULL, 9);
       set_fkt (NULL, 10);
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       initenter = 0;
       return;
}

void WOTLIST::Work ()
/**
Liste bearbeiten.
**/

{

       beginwork ();

       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);

       eListe.SetDataForm0 (&dataform, &lineform);
       eListe.SetChAttr (ChAttr); 
       eListe.SetUbRows (ubrows);
       eListe.SetListFocus (ListFocus);
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;

       SetAktivWindow (eListe.Getmamain2 ());

       ListAktiv = 1;
       eListe.ProcessMessages ();

       commitwork ();
       if (syskey == KEY5)
       {
                  eListe.Initscrollpos ();
                  ShowDB ();
       }
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
       return;
}


void WOTLIST::DestroyMainWindow (void)
/**
Hauptfenster fuer Liste loeschen.
**/
{
        if (mamain1 == NULL) return;
        DestroyWindow (mamain1);
        mamain1 = NULL;
}


HWND WOTLIST::CreateMainWindow (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWindow == NULL) return NULL;    
           
        if (mamain1) return mamain1;

        eListe.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


        y = (wrect.bottom - 20 * tm.tmHeight);
        x = wrect.left + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;

        mamain1 = CreateWindow ("ListMain",
                                    InfoCaption,
                                    WS_THICKFRAME | 
                                    WS_POPUP,
                                    x, y,
                                    cx, cy,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (mamain1, SW_SHOWNORMAL);
        UpdateWindow (mamain1);
        return mamain1;
}

static int IsMax = 0;
static int IsMin = 0;

void WOTLIST::SetMax0 (int val)
{
    IsMax = val;
    if (IsMax)
    {
             SetFkt (11, fenster, KEY11);
    }
    else
    {
             SetFkt (11, vollbild, KEY11);
    }
}

void WOTLIST::SetMin0 (int val)
{
    IsMin = val;
}


int WOTLIST::Schirm (void)
{
         if (IsMin) return 0;
         
         if (IsMax)
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_RESTORE, 0l);
             IsMax = 0;
         }
         else
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_MAXIMIZE, 0l);
             IsMax = 1;
         }
         return 0;
}


void WOTLIST::MoveMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        eListe.GetTextMetric (&tm);
        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        if (mamainmax)
        {
                 x = wrect.left + 2;
                 y = wrect.top + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        else
        {
                 y = (wrect.bottom - 20 * tm.tmHeight);
                 x = wrect.left + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}


void WOTLIST::MaximizeMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        x = wrect.left + 2;
        y = wrect.top + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}

HWND WOTLIST::GetMamain1 (void)
{
       return (mamain1);
}

void WOTLIST::SethwndTB (HWND hwndTB)
{
         this->hwndTB = hwndTB; 
         eListe.SethwndTB (hwndTB);
}

void WOTLIST::SetTextMetric (TEXTMETRIC *tm)
{
         eListe.SetTextMetric (tm);
}


void WOTLIST::SetLineRow (int LineRow)
{
         eListe.SetLineRow (0);
}

void WOTLIST::SetListLines (int i)
{ 
         eListe.SetListLines (i);
}

void WOTLIST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnPaint (hWnd, msg, wParam, lParam);
}


void WOTLIST::MoveListWindow (void)
{
        eListe.MoveListWindow ();
}


void WOTLIST::BreakList (void)
{
        eListe.BreakList ();
}


void WOTLIST::OnHScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnHScroll (hWnd, msg,wParam, lParam);
}

void WOTLIST::OnVScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        eListe.OnVScroll (hWnd, msg,wParam, lParam);
}


void WOTLIST::FunkKeys (WPARAM wParam, LPARAM lParam)
{
                    eListe.FunkKeys (wParam, lParam);
}


int WOTLIST::GetRecanz (void)
{
          return eListe.GetRecanz ();
}


void WOTLIST::SwitchPage0 (int rows)
{
           eListe.SwitchPage0 (rows);
}


HWND WOTLIST::Getmamain2 (void)
{
           return eListe.Getmamain2 ();
}


HWND WOTLIST::Getmamain3 (void)
{
           return eListe.Getmamain3 ();
}


void WOTLIST::ChoiseFont (mfont *lfont)
{
           eListe.ChoiseFont (lfont);
}

void WOTLIST::SetFont (mfont *lfont)
{
           eListe.SetFont (lfont);
}

void WOTLIST::SetListFont (mfont *lfont)
{
           eListe.SetListFont (lfont);
}

void WOTLIST::FindString (void)
{
           eListe.FindString ();
}


void WOTLIST::SetLines (int Lines)
{
           eListe.SetLines (Lines);
}


int WOTLIST::GetAktRow (void)
{
                 return eListe.GetAktRow ();
}

int WOTLIST::GetAktRowS (void)
{
                 return eListe.GetAktRowS ();
}

void WOTLIST::SetColors (COLORREF Color, COLORREF BkColor)
{
                 eListe.SetColors (Color, BkColor); 
}

void WOTLIST::SetListFocus (void)
{
                 eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                       eListe.GetAktColumn ()); 
}

