#ifndef LB_TAB_DEF
#define LB_TAB_DEF
#include "dbclass.h"

struct KONFIG_TAG {
   char      a_modif[2];
   char      a_sum_kz[2];
   char      bed_sum_kz[2];
   char      err_kz[2];
   short     fil;
   char      freq_ums_kz[2];
   char      kase_sum_kz[2];
   char      kasi_sum_kz[2];
   char      lng_bon_kz[2];
   short     mdn;
   char      mwst_kz[2];
   int       sys;
   char      verk_sum_kz[2];
   char      waa_sum_kz[2];
   char      wg_sum_kz[2];
   short     auto_abruf;
   char      pers_zeit_kz [2];
   char      ret_kz [2];
   char      bestell_kz [2];
   char      treu_kz [2];
   char      lb_inv_kz [2];
   char      lb_ret_kz [2];
};

struct LB {
   int       bon;
   short     abt;
   short     waa;
   short     bed;
   short     buch_art;
   short     wg;
   short     par3_sint;
   short     par4_sint;
   short     mwst_verbu;
   short     mwst;
   double    pr_vk;
   double    ums_vk;
   int       me_vk_pos;
   double    me_vk;
   double    a;
   double    lief_me;
   short     stat;
   long      dat;
   char      zeit[7];
   short     mdn;
   short     fil;
   int       sys;
   short     pos;
   int       a_krz;
   short     nr;
   double    ums_mwst;
   short     sa;
   double    pr_ek;
   double    pr_vk_waa;
   short     minus_pos;
   double    minus_sum;
   short     sto_pos_bws;
   short     sto_pos_dir;
   double    sto_sum_bws;
   double    sto_sum_dir;
};

/* 12.03.08 fuer Storno
minus_pos            smallint                                yes
minus_sum            decimal(8,2)                            yes
sto_pos_bws          smallint                                yes
sto_pos_dir          smallint                                yes
sto_sum_bws          decimal(8,2)                            yes
sto_sum_dir          decimal(8,2)                            yes
*/

struct A_TAG_LB {
   double    a;
   short     abt;
   long      dat;
   short     delstatus;
   short     fil;
   short     mdn;
   double    me_vk_tag;
   int       me_vk_tag_pos;
   double    ums_ek_tag;
   double    ums_vk_tag;
   short     verk_st;
   short     host_trans;
   short     mwst;
   double    ums_mwst;
};

struct A_SA_TAG_LB {
   double    a;
   short     abt;
   short     akt;
   long      dat;
   short     delstatus;
   short     fil;
   short     mdn;
   double    me_vk_sa_tag;
   int       me_vk_sa_tag_pos;
   double    ums_ek_sa_tag;
   double    ums_vk_sa_tag;
   short     verk_st;
   short     mwst;
   double    ums_mwst;
};

struct WG_TAG_LB {
   short     delstatus;
   long      dat;
   short     fil;
   short     mdn;
   double    me_vk_tag;
   int       me_vk_tag_pos;
   double    me_vk_sa_tag;
   int       me_vk_sa_tag_pos;
   double    pausch_vk_tag;
   double    ums_ek_sa_tag;
   double    ums_ek_tag;
   double    ums_vk_sa_tag;
   double    ums_vk_tag;
   short     verk_st;
   short     wg;
   short     mwst;
   double    ums_mwst;
};

struct A_WO_LB {
   double    a;
   short     abt;
   short     delstatus;
   short     fil;
   short     jr;
   short     mdn;
   double    me_vk_wo;
   int       me_vk_wo_pos;
   double    ums_ek_wo;
   double    ums_vk_wo;
   short     verk_st;
   short     wo;
   short     mwst;
   double    ums_mwst;
};

struct A_SA_WO_LB {
   double    a;
   short     abt;
   short     akt;
   short     delstatus;
   short     fil;
   short     jr;
   double    me_vk_sa_wo;
   int       me_vk_sa_wo_pos;
   short     mdn;
   double    ums_ek_sa_wo;
   double    ums_vk_sa_wo;
   short     verk_st;
   short     wo;
   short     mwst;
   double    ums_mwst;
};

struct A_MO_LB {
   double    a;
   short     abt;
   short     delstatus;
   short     fil;
   short     jr;
   short     mdn;
   double    me_vk_mo;
   int       me_vk_mo_pos;
   short     mo;
   double    ums_ek_mo;
   double    ums_vk_mo;
   short     verk_st;
   short     mwst;
   double    ums_mwst;
};
	
struct A_SA_MO_LB {
   double    a;
   short     abt;
   short     akt;
   short     delstatus;
   short     fil;
   short     jr;
   short     mo;
   double    me_vk_sa_mo;
   int       me_vk_sa_mo_pos;
   short     mdn;
   double    ums_ek_sa_mo;
   double    ums_vk_sa_mo;
   short     verk_st;
   short     mwst;
   double    ums_mwst;
};

struct WG_WO_LB {
   short     delstatus;
   short     fil;
   short     jr;
   short     mdn;
   short     wo;
   double    me_vk_wo;
   int       me_vk_wo_pos;
   double    me_vk_sa_wo;
   int       me_vk_sa_wo_pos;
   double    pausch_vk_wo;
   double    ums_ek_sa_wo;
   double    ums_ek_wo;
   double    ums_vk_sa_wo;
   double    ums_vk_wo;
   short     verk_st;
   short     wg;
   short     mwst;
   double    ums_mwst;
};

struct WG_MO_LB {
   short     delstatus;
   short     fil;
   short     jr;
   short     mdn;
   double    me_vk_mo;
   int       me_vk_mo_pos;
   double    me_vk_sa_mo;
   int       me_vk_sa_mo_pos;
   short     mo;
   double    pausch_vk_mo;
   double    ums_ek_sa_mo;
   double    ums_ek_mo;
   double    ums_vk_sa_mo;
   double    ums_vk_mo;
   short     verk_st;
   short     wg;
   short     mwst;
   double    ums_mwst;
};

struct WAA_SUM_LB {
	short    abt;
    short    bed;
    long     dat;
	short    fil;
    short    kun_anz_x;
	short    kun_anz_xx;
    double   kun_sum_x;
    double   kun_sum_xx;
    short    mdn;
    short    minus_pos;
    double   minus_sum;
    short    pos_gew;
    short    pos_hnd;
    short    pos_o_nr;
    short    sto_pos_bws;
	short    sto_pos_dir;
    double   sto_sum_bws;
    double   sto_sum_dir;
    double   ums_gew;
    double   ums_hnd;
    double   ums_o_nr;
    short    verk_st;
    short    waa;
    short    waa_z_zaehl;
    short    host_trans;
};

struct BED_SUM_LB {
	short    abt;
    short    bed;
    long     dat;
	short    fil;
    short    kun_anz_x;
	short    kun_anz_xx;
    double   kun_sum_x;
    double   kun_sum_xx;
    short    mdn;
    short    minus_pos;
    double   minus_sum;
    short    pos_gew;
    short    pos_hnd;
    short    pos_o_nr;
    short    sto_pos_bws;
	short    sto_pos_dir;
    double   sto_sum_bws;
    double   sto_sum_dir;
    double   ums_gew;
    double   ums_hnd;
    double   ums_o_nr;
    short    verk_st;
    short    waa;
    short    host_trans;
};

struct FREQ_KASE_LB {
    long     dat;
	short    fil;
	short    kase;
    short    kun_anz;
    short    mdn;
    double   gew;
	short    me_stk;
    double   ums_vk_freq;
	short    verk_st;
	char     zeit_bis[7];
	char     zeit_von[7];
};

struct FREQ_CLS {
    short    mdn;
	short    fil;
    int      sys;
	short    ber;
	short    end_std;
	short    end_min;
	short    start_std;
	short    start_min;
	short    startstunde;
};

struct MWST_SUM_LB {
    short    mdn;
	short    fil;
	short    verk_st;
    long     dat;
    short    mwst;
	double   mwst_betr;
    double   me_vk;
    int      me_vk_pos;
    double   ums_vk;
};

#endif
	