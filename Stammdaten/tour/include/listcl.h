#ifndef _eListDef
#define _eListDef

#define MAXFIELD 200

struct CHATTR
{
    char *feldname;
    int  UpdAttr;
    int  InsAttr;
};

class LKEYS
{
   public :
    virtual BOOL InsertLine (int)
    {
        return FALSE;
    }

    virtual BOOL AppendLine (int)
    {
        return FALSE;
    }
};


      

void SetComboMess (BOOL (*) (MSG *));

class ListClass
{
      private :
		     BOOL InvalidateFocusFrame; 
             int ListBreak; 
             char *eingabesatz;
             int NoScroll;
             int lPagelen;
             int LineRow;
             HWND    hwndTB;
             HWND    hTrack;
             HWND    vTrack;

             char frmrow [0x1000];
             TEXTMETRIC tm;
             PAINTSTRUCT aktpaint;

             BOOL IsArrow;
             HCURSOR oldcursor;
             HCURSOR arrow;
             HCURSOR aktcursor;
             int movfield; 
             int InMove;
			 RECT FocusRect;
             HFONT EditFont;
             HFONT ListhFont;
             HFONT oldhFont;
             mfont UbFont;
             mfont UbSFont;
             mfont SFont;
             mfont ListFont;
             int LineColor;
             int InAppend;
             int NewRec;
             int paintallways;
             COLORREF Color;
             COLORREF BkColor;
             COLORREF Color0;
             COLORREF BkColor0;
             struct CHATTR *ChAttr;
             int UbHeight;
			 double RowHeight;
			 BOOL listclipp;
			 BOOL setsel;
			 BOOL focusframe;
             BOOL NoRecNr;
			 int    RowHeightP;
			 int    UbRowsP;
             int    Plus3D;
			 BOOL posset;
             LKEYS *Lkeys;

      public :
             unsigned char *ausgabesatz;
             int SwRecs;
             char **SwSaetze;
             char **LstSaetze;
             unsigned char *LstSatz;
//             FELDER *LstZiel;
             int zlenS;
             int scrollpos;
             int scrollposS;
             int recanz;
             int recHeight ;
             int PageView;
             HANDLE  hMainInst;
             HWND    hMainWindow; 
             HWND    mamain2; 
             HWND    mamain3; 
             field _UbForm [MAXFIELD];
             form UbForm;
             field _DataForm [MAXFIELD];
             form DataForm;
             field _LineForm [MAXFIELD];
             form LineForm;
             field _fUbSatzNr[1];
             form fUbSatzNr;
             form *UbRow;
             form *DataRow;
             form *lineRow;
             int *UbRows;
             int banz;
             int zlen;
             int banzS;
			 int WithFocus;
			 int  AktRow;
			 int  SelRow;
			 int AktColumn;
			 int  AktRowS;
             int AktPage;
			 int AktColumnS;
             int FindRow;
             int FindColumn;
			 char AktItem [21];
			 char AktValue [80];
			 char RowItem [21];
			 char RowValue [80];
             BOOL UseRowItem;
			 int  ListFocus;
			 int  AktFocus;
			 HWND FocusWindow;
             int RowMode;
             int UseZiel;
             int FrmRow;
             int (*InfoProc) (char **, char *, char *);
             int (*TestAppend) (void);
			 void (*RowProc) (COLORREF *, COLORREF *);
			 BOOL print3D;


			 void SetPlus3D (int plus)
			 {
				 Plus3D = min (2, (max (plus,1)));
			 }

			 void Set3D (BOOL flag)
			 {
				 print3D = flag;
			 }

             void SetListFont (mfont *lFont)
             {
				 CopyFonts (lFont);
             }

             BOOL IsNewRec (void)
             {
                 return NewRec;
             }

             BOOL IsAppend (void)
             {
                 return InAppend;
             }

             void SetInfoProc (int (*Proc) (char **, char *, char *))
             {
                 InfoProc = Proc;
             }

             void SetTestAppend (int (*Proc) (void))
             {
                 TestAppend = Proc;
             }

             void SetRowProc (void (*Proc) (COLORREF *, COLORREF *))
             {
                 RowProc = Proc;
             }

			 char *GetAktItem (void)
			 {
                 if (UseRowItem)
                 {
                     return RowItem;

                 }
				 return AktItem;
			 }

			 char *GetAktValue (void)
			 {
                 if (UseRowItem)
                 {
                     return RowValue;

                 }
				 return AktValue;
			 }

             void SetChAttr (struct CHATTR *cha)
             {
                 ChAttr = cha;
             }

             ListClass ()
             {
                    InvalidateFocusFrame = FALSE;
                    IsArrow = FALSE;
                    oldcursor = NULL;;
                    arrow = NULL;
                    aktcursor = LoadCursor (NULL, IDC_ARROW);
                    movfield = 0; 
                    InMove = 0;
					AktRow = 0;
					SelRow = 0;
					AktColumn = 0;
					AktRowS = 0;
					AktColumnS = 0;
					WithFocus = 1;
					recanz = 0;
					recHeight = 1;
					banz = 0;
					ListFocus = 1;
					FocusWindow = NULL;
                    RowMode = 0;
//                    LstZiel = NULL;
                    PageView = 0;
                    EditFont = NULL;
                    LineColor = 3;
					oldhFont = NULL;
                    UseZiel = 0;
                    InfoProc = NULL;
                    TestAppend = NULL;
                    RowProc = NULL;
                    TestAppend = 0;
                    paintallways = 0;
                    ausgabesatz = NULL;
                    Color = RGB (0, 0, 0);;
                    BkColor = RGB (255, 255, 255); 
                    NewRec = 0;
                    InAppend = 0;
                    ChAttr = NULL;
                    UseRowItem = FALSE;
                    FrmRow = 0;
                    UbRows = NULL;
                    fUbSatzNr.mask = _fUbSatzNr;
                    _fUbSatzNr[0].feldid = NULL;
					RowHeight = (double) 1.0;
					listclipp = FALSE;
					setsel = TRUE;
					focusframe = TRUE;
					Plus3D = 1;
					NoRecNr = FALSE;
                    Lkeys = NULL;
             }

			 ~ListClass ()
			 {
			        if (ListhFont != NULL)
					{
            				 DeleteObject (ListhFont);
					}
			 }

             void SetLkeys (LKEYS *Lkeys)
             {
                    this->Lkeys = Lkeys;
             }

			 void SetRowHeight (double Height)
			 {
				    RowHeight = Height;
			 }

			 void SetListclipp (BOOL flag)
			 {
				 listclipp = flag;
			 }

			 void SetSetsel (BOOL flag)
			 {
				 setsel = flag;
			 }

			 void SetFocusframe (BOOL flag)
			 {
				 focusframe = flag;
			 }

             void SetUbRows (int *ubr)
             {
                    UbRows = ubr;
             }

             int GetUbSpalte (int spalte)
             {
                    if (UbRows)
                    {
                        return UbRows[spalte];
                    }
                    return spalte;
             }

             void SetRowItem (char *name, char *value)
             {
                     strcpy (RowItem, name);
                     strcpy (RowValue, value);
                     UseRowItem = TRUE;
             }

             void InitRowItem (void)
             {
                     UseRowItem = FALSE;
             }

             void SetAusgabeSatz (unsigned char *satz)
             {
                    ausgabesatz = satz;
             }

             void BreakList (void)
             {
                    ListBreak = 1;
                    PostQuitMessage (0);
             }

             void SetListFocus (int focus)
             {
                    ListFocus = focus;
             }

             void SetLines (int LineNr)
             {
                     LineColor = LineNr;
             }

             void SetPageView (int pg)
             {
                     PageView = pg;
             }

/*
             void SetLstZiel (FELDER *zl)
             {
                     LstZiel = zl;
             }
*/

             void SetRowMode (int mode)
             {
                    RowMode = mode;
             }

             void SetPos (int zeile, int spalte)
             {
					AktRow = zeile;
					AktColumn = spalte;
					posset = TRUE;
             }

             void Initscrollpos (void)
             {
                    scrollpos = 0;
             }

             void Setscrollpos (int pos)
             {
                 scrollpos = pos;
             }

             int Getscrollpos (void)
             {
                 return scrollpos;
             }


             void InitRecanz (void)
             {
                 recanz = 0;
             }

             int  GetRecanz (void)
             {
                 return recanz;
             }

             int GetRecHeight (void)
             {
                 return recHeight;
             }

             int  GetAktRow (void)
             {
                 return AktRow;
             }

             int  GetAktRowS (void)
             {
                 return AktRowS;
             }

             int  GetAktColumn (void)
             {
                 return AktColumn;
             }

             void SethwndTB (HWND hwndTB)
             {
                 this->hwndTB = hwndTB;
             }

             void SetSaetze (char **Saetze)
             {
                 SwSaetze = Saetze;
             }

             void Setbanz (int banz)
             {
                 this->banz = banz;
             }

             void Setzlen (int zlen)
             {
                 this->zlen = zlen;
             }

             void SetRecanz (int anz)
             {
                 recanz = anz;
             }

             void SetRecHeight (int height)
             {
                 recHeight = height;
             }

             void SetPagelen (int Pagelen)
             {
                 lPagelen = Pagelen;
             }

             void SetLineRow (int LineRow)
             {
                 this->LineRow = LineRow;
             }

             void SetTextMetric (TEXTMETRIC *ttm)
             {
                 memcpy (&tm, ttm, sizeof (tm));
				 if (RowHeight == (double) 0.0) 
				 {
					 RowHeight = (double) 2;
				 }
             }

             void GetTextMetric (TEXTMETRIC *ttm)
             {
                 memcpy (ttm, &tm, sizeof (tm));
             }
             void SetAktPaint (PAINTSTRUCT *pm)
             {
                 memcpy (&aktpaint, pm, sizeof (aktpaint));
             }

             HWND Getmamain2 (void)
             {
                  return mamain2;
             }

             HWND Getmamain3 (void)
             {
                  return mamain3;
             }

             HWND GethTrack (void)
             {
                  return  hTrack;
             }

             HWND GetvTrack (void)
             {
                  return  vTrack;
             }

             BOOL IsRowMode (void)
             {
                 return RowMode;
             }

             HWND     InitListWindow (HWND);
             void     GetPageLen (void);
             BOOL     TrackNeeded (void);
             void     CreateTrack (void);
             void     DestroyTrack (void); 

             BOOL     VTrackNeeded (void);
             void     CreateVTrack (void);
             void     DestroyVTrack (void);
             void     TestTrack (void);
             void     TestVTrack (void);
             void     TestMamain3 (void);
             void     MoveListWindow (void);
             void     SetDataStart (void);
             BOOL     MustPaint (int);
             void     PrintUSlinesSatzNr (HDC);
             void     PrintSlinesSatzNr (HDC);
             void     PrintVlineSatzNr (HDC);
             void     Print3DVlines (HDC);
             void     PrintVlines (HDC);
             void     Print3DVline (HDC, int);
             void     PrintVline (HDC, int);
             void     PrintHlinesSatzNr (HDC);
             void     PrintHlineSatzNr (HDC, int);
             void     Print3DHlinesEnd (HDC);
             void     Print3DHlines (HDC);
             void     PrintHlines (HDC);
             void     Print3DHline (HDC, int);
             void     Print3DHlineEnd (HDC, int);
             void     PrintHline (HDC, int);
			 void     ShowFocusText (HDC, int, int);
			 void     GetFocusText (void);
			 void     ShowWindowText (int, int);
			 void     DestroyFocusWindow (void);
             void     SetFeldEdit (int, int);
             void     SetFocusFrame (HDC, int, int);
             void     SetFeldEdit0 (int, int, DWORD);
			 void     SetFeldFrame (HDC, int, int);
			 void     SetFeldFocus (HDC, int, int);
			 void     SetFeldFocus0 (int, int);
             int      DelRec (void); 
#ifndef _PFONT
             void     FillFrmRow (char *, form *, int);
#else
             void     FillFrmRow (HDC, char *, form *, int, int);
#endif
             int      GetMidPos (int y);
             void     ShowRowRect (COLORREF, int);
             void     ShowFrmRow (HDC, int, int);
             void     ShowSatzNr (HDC, int);
             void     ShowDataForms (HDC);
             void     ScrollLeft (void);
             void     ScrollRight (void);
             void     ScrollPageDown (void);
             void     ScrollPageUp (void);
             void     SetPos (int);
             void     SetTop (void);
             void     SetBottom (void);
             void     HScroll (WPARAM, LPARAM);
             void     SetNewRow (int);
             void     SetVPos (int);
             void     ScrollWindow0 (HWND, int, int, RECT *, RECT *);
             void     ScrollDown (void);
             void     ScrollUp (void);
             void     ScrollVPageDown (void);
             void     ScrollVPageUp (void);
             void     VScroll (WPARAM, LPARAM);
			 void     FocusLeft (void);
			 void     FocusFirst (void);
			 void     FocusRight (void);
			 void     FocusUp (void);
			 void     FocusDown (void);
//             void     ZielFormat (char *, FELDER *);
             BOOL     IsInListArea (MSG *);
             void     OnPaint (HWND, UINT, WPARAM, LPARAM);
             void     OnSize (HWND, UINT, WPARAM, LPARAM);
             void     OnHScroll (HWND, UINT, WPARAM, LPARAM);
             void     OnVScroll (HWND, UINT, WPARAM, LPARAM);
             void     FunkKeys (WPARAM, LPARAM);
             void     FillUbForm (field *, char *, int, int,int);
             void     FillDataForm (field *, char *, int, int);
             void     FillLineForm (field *, int);
             void     MakeLineForm (form *);
             void     MakeDataForm (form *);
             void     MakeUbForm (void);
             void     MakeDataForm0 (void);

             void     SetLineForm (form *);
             form *   GetDataForm ();
             void     SetDataForm (form *);
             void     SetUbForm (form *);
             void     SetDataForm0 (form *, form *);

             void     FreeLineForm (void);
             void     FreeDataForm (void);
             void     FreeUbForm (void);
             BOOL     IsMouseMessage (MSG *);
             void     StopMove (void);
             void     StopMove0 (void);
             void     StartMove (void);
             void     MoveLine (void);
             void     TestNextLine (void);
             int      GetFieldPos (char *);
             void     DestroyField (int);
             void     ShowArrow (BOOL);
             int      IsUbEnd (MSG *);
             int      IsUbRow (MSG *);
			 void     EditScroll (void);
             void     ToDlgRec (char *);
             void     FromDlgRec (char *);
             void     ToDlgRec0 (char *);
             void     FromDlgRec0 (char *);
             void     FreeDlgSaetze (void);
             void     SwitchPage (int);
             void     SetPage (int);
             void     SetPage0 (int);
             void     PriorPageRow (void);
             void     NextPageRow (void);
             void     SetFont (mfont *);
             void     CopyFonts (mfont *);
             void     ChoiseFont (mfont *);
             void     ChoiseLines (HDC);
             BOOL     InRec (char *, int *);
             void     find (char *);
             void     FindString (void);
             void     FindNext (void);
             BOOL     NewSearchString ();
             void     SetListLines (int); 
             void     TestScroll ();
             int      ProcessMessages(void);
             void     SetColors (COLORREF, COLORREF);

             BOOL    IsEditColumn (int);
             int     FirstColumn (void);
             int     PriorColumn (void);
             int     NextColumn (void);
             int     FirstColumnR (void);
             int     PriorColumnR (void);
             int     NextColumnR (void);
             int     TestAfter (void);
             int     TestBefore (void);
             int     TestAfterR (void);
             int     TestBeforeR (void);
             int     TestAfterRow (void);
             int     TestBeforeRow (void);
             void    InsertLine (void);
             void    AppendLine (void);
             void    DeleteLine (void);
             void    ShowAktRow (void);
             void    ShowAktColumn (HDC, int, int, int);
             void    DisplayList (void);
             void    KillListFocus (void);
             void    SetListFocus (void);
             void    SetInsAttr (void);
             void    SetFieldAttr (CHATTR *);
             void    DestroyRmFields (void);
             void    DestroyListWindow (void);
             void    ScrollUbForm (void);
			 void    PaintUb (void);
             void    SetNoRecNr (BOOL);
};

class ListClassDB : public ListClass
{
     public :
            ListClassDB () : ListClass ()
            {
            }
            void     FreeBezTab (void);
            void     FillUbForm (field *, char *, int, int,int);
            void     MakeUbForm (void);
            void     SwitchPage (int);
            void     SwitchPage0 (int);
            char *   GetItemName (char *); 
};

#endif 
