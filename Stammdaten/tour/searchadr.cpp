#include <windows.h>
#include "searchadr.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


struct SADR *SEARCHADR::sadrtab = NULL;
struct SADR SEARCHADR::sadr;
int SEARCHADR::idx;
long SEARCHADR::adranz;
CHQEX *SEARCHADR::Query = NULL;
DB_CLASS SEARCHADR::DbClass; 
HINSTANCE SEARCHADR::hMainInst;
HWND SEARCHADR::hMainWindow;
int SEARCHADR::soadr = 1; 
int SEARCHADR::soadr_krz = -1; 
int SEARCHADR::soadr_nam1 = 1; 
int SEARCHADR::soort = 1; 

int SEARCHADR::sortadr (const void *elem1, const void *elem2)
{
	      struct SADR *el1; 
	      struct SADR *el2; 

		  el1 = (struct SADR *) elem1;
		  el2 = (struct SADR *) elem2;
	      return ((int) (el1->adr - el2->adr) * soadr);
}

void SEARCHADR::SortAdr (HWND hWnd)
{
   	   qsort (sadrtab, adranz, sizeof (struct SADR),
				   sortadr);
       soadr *= -1;
}

int SEARCHADR::sortadr_krz (const void *elem1, const void *elem2)
{
	      struct SADR *el1; 
	      struct SADR *el2; 

		  el1 = (struct SADR *) elem1;
		  el2 = (struct SADR *) elem2;
	      return (strcmp (el1->adr_krz,el2->adr_krz) * soadr_krz);
}

void SEARCHADR::SortAdr_krz (HWND hWnd)
{
   	   qsort (sadrtab, adranz, sizeof (struct SADR),
				   sortadr_krz);
       soadr_krz *= -1;
}

int SEARCHADR::sortadr_nam1 (const void *elem1, const void *elem2)
{
	      struct SADR *el1; 
	      struct SADR *el2; 

		  el1 = (struct SADR *) elem1;
		  el2 = (struct SADR *) elem2;
	      return (strcmp (el1->adr_nam1,el2->adr_nam1) * soadr_nam1);
}

void SEARCHADR::SortAdr_nam1 (HWND hWnd)
{
   	   qsort (sadrtab, adranz, sizeof (struct SADR),
				   sortadr_nam1);
       soadr_nam1 *= -1;
}

int SEARCHADR::sortort (const void *elem1, const void *elem2)
{
	      struct SADR *el1; 
	      struct SADR *el2; 

		  el1 = (struct SADR *) elem1;
		  el2 = (struct SADR *) elem2;
	      return (strcmp (el1->ort1,el2->ort1) * soort);
}

void SEARCHADR::SortOrt (HWND hWnd)
{
   	   qsort (sadrtab, adranz, sizeof (struct SADR),
				   sortort);
       soort *= -1;
}


void SEARCHADR::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortAdr (hWnd);
                  break;
              case 1 :
                  SortAdr_krz (hWnd);
                  break;
              case 2 :
                  SortAdr_nam1 (hWnd);
                  break;
              case 3 :
                  SortOrt (hWnd);
                  break;
              default :
                  return;
       }
       UpdateList ();
}


void SEARCHADR::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < adranz; i ++)
       {
 	      sprintf (buffer, " %-8ld \"%-16s\" \"%-36s\" \"%-36s\"", sadrtab[i].adr, 
                            sadrtab[i].adr_krz,
                            sadrtab[i].adr_nam1, sadrtab[i].ort1); 
	      Query->UpdateRecord (buffer, i);
       }
}


int SEARCHADR::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   int i;
	   int len;

	   if (sadrtab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < adranz; i ++)
	   {
		   len = min (16, strlen (sebuff));
		   if (strupcmp (sebuff, sadrtab[i].adr_krz, len) == 0) break;
	   }
	   if (i == adranz) return 0;
	   Query->SetSel (i);
	   return 0;
}

int SEARCHADR::ReadAdr (char *adr_name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (sadrtab) 
	  {
           delete sadrtab;
           sadrtab = NULL;
	  }


	  adranz = 0;
      clipped (adr_name);
      if (strcmp (adr_name, " ") == 0) adr_name[0] = 0;
	  sprintf (buffer, "select count (*) from adr where adr_nam1 matches \"%s*\"", adr_name);
      DbClass.sqlout ((int *) &adranz, 2, 0);
	  DbClass.sqlcomm (buffer);
	  if (adranz == 0) return 0;

	  sadrtab = new struct SADR [adranz + 2];
	  sprintf (buffer, "select adr, adr_krz, "
		               "adr_nam1, adr_nam2, pf, plz, "
					   "str, ort1, ort2, adr_typ from adr "
                       "where adr_krz matches \"%s*\" "
					   "order by adr_krz", adr_name);
      DbClass.sqlout ((long *) &sadr.adr, 2, 0);
      DbClass.sqlout ((char *) sadr.adr_krz, 0, 17);
      DbClass.sqlout ((char *) sadr.adr_nam1, 0, 37);
      DbClass.sqlout ((char *) sadr.adr_nam2, 0, 37);
      DbClass.sqlout ((char *) sadr.pf, 0, 17);
      DbClass.sqlout ((char *) sadr.plz, 0, 9);
      DbClass.sqlout ((char *) sadr.str, 0, 37);
      DbClass.sqlout ((char *) sadr.ort1, 0, 37);
      DbClass.sqlout ((char *) sadr.ort2, 0, 37);
      DbClass.sqlout ((short *) &sadr.adr_typ, 1, 0);
	  cursor = DbClass.sqlcursor (buffer);
	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
	  {
		  memcpy (&sadrtab[i], &sadr, sizeof (sadr));
 	      sprintf (buffer, " %-8ld \"%-16s\" \"%-36s\" \"%-36s\"", sadr.adr, sadr.adr_krz,
                            sadr.adr_nam1, sadr.ort1); 
	      Query->InsertRecord (buffer);
		  i ++;
	  }
	  DbClass.sqlclose (cursor);
 	  Wmess.Destroy ();
	  return 0;
}

void SEARCHADR::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
}

void SEARCHADR::SearchAdr (void)
{
  	  int cx, cy;
	  char buffer [256];
	  form *scurrent;

	  scurrent = current_form;
	  idx = -1;
      cx = 80;
      cy = 20;
      SetSortProc (SortLst);
      Query = new CHQEX (cx, cy, "Name", "");
      Query->OpenWindow (hMainInst, hMainWindow);
	  sprintf (buffer, " %9s %16s %36s %36s", "1", "1", "1", "1"); 
	  Query->VLines (buffer, 0);
	  EnableWindow (hMainWindow, FALSE);
	  EnableWindow (adrwin, FALSE);

	  sprintf (buffer, " %-8s %-16s %-36s %-36s", "AdrNr", "Kurz-Name", "Name", "Ort"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadAdr);
	  Query->SetSearchLst (SearchLst);
	  Query->ProcessMessages ();
        idx = Query->GetSel ();
	  EnableWindow (hMainWindow, TRUE);
	  EnableWindow (adrwin, TRUE);
        Query->DestroyWindow ();
        SetActiveWindow (adrwin);
	  if (idx == -1) return;
	  memcpy (&sadr, &sadrtab[idx], sizeof (sadr));
	  current_form = scurrent;
      SetSortProc (NULL);
	  if (syskey == KEY5) return;
}

