#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lgr.h"

struct LGR lgr, lgr_null;

/* Variablen fuer Auswahl mit Buttonueberschrift    */

#include "itemc.h"
#define MAXSORT 10000

static char *sqltext;

static int dosort1 ();
static int dosort2 ();

struct SORT_AW
{
	     char sort1 [10];
	     char sort2 [49];
	     int  sortidx;
};

static struct SORT_AW sort_aw, sort_awtab[MAXSORT];
static int sort_awanz;
static int sort1 = -1;
static int sort2 = -1;
static int sort_idx = 0;

static ITEM isort1    ("", sort_aw.sort1, "", 0);
static ITEM isort2    ("", sort_aw.sort2, "", 0);
static ITEM ifillub   ("", " ",             "", 0); 

static field _fsort_aw[] = {
&isort1,      9, 1, 0,  0, 0, "%8d",       NORMAL, 0, 0, 0,
&isort2,     48, 1, 0,  9, 0, "",          NORMAL, 0, 0, 0,
&ifillub,    80, 1, 0, 57, 0, "",         NORMAL, 0, 0, 0,
};

static form fsort_aw = {2, 0, 0, _fsort_aw, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "Lager",         "", 0);
static ITEM isort2ub    ("", "Bezeichnung",   "", 0);

static field _fsort_awub[] = {
&isort1ub,        9, 1, 0,  0, 0, "", BUTTON, 0, dosort1,    102,
&isort2ub,       48, 1, 0,  9, 0, "", BUTTON, 0, dosort2,    103,
&ifillub,        80, 1, 0, 57, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {3, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0,  9, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 57, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awl = {2, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};

static int dosort10 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return ((int) (atol(el1->sort1) - atol (el2->sort1)) * 
				                                  sort1);
}


static int dosort1 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort10);
	sort1 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static int dosort20 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return (strcmp(el1->sort2, el2->sort2) * sort2);
}


static int dosort2 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort20);
	sort2 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        sort_idx = idx;
        break_list ();
        return;
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

/* Ende Variablen fuer Auswahl mit Buttonueberschrift    */

static void FillUb  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%8s %-13s",
                          "Lager", "Bezeichnung");
}


static void FillValues  (char *buffer)
{
         sprintf (buffer, "%8ld %s",
                           lgr.lgr, lgr.lgr_bz);
}

void LGR_CLASS::prepare (void)
{
         ins_quest ((char *) &lgr.lgr, 2, 0);

    out_quest ((char *) &lgr.mdn,1,0);
    out_quest ((char *) &lgr.fil,1,0);
    out_quest ((char *) &lgr.lgr,2,0);
    out_quest ((char *) lgr.lgr_bz,0,25);
    out_quest ((char *) &lgr.delstatus,1,0);
    out_quest ((char *) &lgr.lgr_kz,1,0);
    out_quest ((char *) lgr.lgr_smt_kz,0,4);
    out_quest ((char *) &lgr.lgr_gr,1,0);
    out_quest ((char *) lgr.lgr_kla,0,2);
    out_quest ((char *) lgr.abr_period,0,2);
    out_quest ((char *) &lgr.adr,2,0);
    out_quest ((char *) &lgr.afl,1,0);
    out_quest ((char *) lgr.bli_kz,0,2);
    out_quest ((char *) &lgr.dat_ero,2,0);
    out_quest ((char *) &lgr.fl_lad,3,0);
    out_quest ((char *) &lgr.fl_nto,3,0);
    out_quest ((char *) &lgr.fl_vk_ges,3,0);
    out_quest ((char *) &lgr.frm,1,0);
    out_quest ((char *) &lgr.iakv,2,0);
    out_quest ((char *) lgr.inv_rht,0,2);
    out_quest ((char *) lgr.ls_abgr,0,2);
    out_quest ((char *) lgr.ls_kz,0,2);
    out_quest ((char *) &lgr.ls_sum,1,0);
    out_quest ((char *) lgr.pers,0,13);
    out_quest ((char *) &lgr.pers_anz,1,0);
    out_quest ((char *) lgr.pos_kum,0,2);
    out_quest ((char *) lgr.pr_ausw,0,2);
    out_quest ((char *) lgr.pr_bel_entl,0,2);
    out_quest ((char *) lgr.pr_lgr_kz,0,2);
    out_quest ((char *) &lgr.pr_lst,2,0);
    out_quest ((char *) lgr.pr_vk_kz,0,2);
    out_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    out_quest ((char *) &lgr.reg_kt_lng,3,0);
    out_quest ((char *) &lgr.reg_kue_lng,3,0);
    out_quest ((char *) &lgr.reg_lng,3,0);
    out_quest ((char *) &lgr.reg_tks_lng,3,0);
    out_quest ((char *) &lgr.reg_tkt_lng,3,0);
    out_quest ((char *) lgr.smt_kz,0,2);
    out_quest ((char *) &lgr.sonst_einh,1,0);
    out_quest ((char *) &lgr.sprache,1,0);
    out_quest ((char *) lgr.sw_kz,0,2);
    out_quest ((char *) &lgr.tou,2,0);
    out_quest ((char *) &lgr.vrs_typ,1,0);
    out_quest ((char *) lgr.inv_akv,0,2);
         cursor = prepare_sql ("select lgr.mdn,  lgr.fil,  lgr.lgr,  "
"lgr.lgr_bz,  lgr.delstatus,  lgr.lgr_kz,  lgr.lgr_smt_kz,  lgr.lgr_gr,  "
"lgr.lgr_kla,  lgr.abr_period,  lgr.adr,  lgr.afl,  lgr.bli_kz,  lgr.dat_ero,  "
"lgr.fl_lad,  lgr.fl_nto,  lgr.fl_vk_ges,  lgr.frm,  lgr.iakv,  lgr.inv_rht,  "
"lgr.ls_abgr,  lgr.ls_kz,  lgr.ls_sum,  lgr.pers,  lgr.pers_anz,  "
"lgr.pos_kum,  lgr.pr_ausw,  lgr.pr_bel_entl,  lgr.pr_lgr_kz,  lgr.pr_lst,  "
"lgr.pr_vk_kz,  lgr.reg_bed_theke_lng,  lgr.reg_kt_lng,  "
"lgr.reg_kue_lng,  lgr.reg_lng,  lgr.reg_tks_lng,  lgr.reg_tkt_lng,  "
"lgr.smt_kz,  lgr.sonst_einh,  lgr.sprache,  lgr.sw_kz,  lgr.tou,  "
"lgr.vrs_typ,  lgr.inv_akv from lgr "

#line 162 "lgr.rpp"
                               "where lgr = ? ");

    ins_quest ((char *) &lgr.mdn,1,0);
    ins_quest ((char *) &lgr.fil,1,0);
    ins_quest ((char *) &lgr.lgr,2,0);
    ins_quest ((char *) lgr.lgr_bz,0,25);
    ins_quest ((char *) &lgr.delstatus,1,0);
    ins_quest ((char *) &lgr.lgr_kz,1,0);
    ins_quest ((char *) lgr.lgr_smt_kz,0,4);
    ins_quest ((char *) &lgr.lgr_gr,1,0);
    ins_quest ((char *) lgr.lgr_kla,0,2);
    ins_quest ((char *) lgr.abr_period,0,2);
    ins_quest ((char *) &lgr.adr,2,0);
    ins_quest ((char *) &lgr.afl,1,0);
    ins_quest ((char *) lgr.bli_kz,0,2);
    ins_quest ((char *) &lgr.dat_ero,2,0);
    ins_quest ((char *) &lgr.fl_lad,3,0);
    ins_quest ((char *) &lgr.fl_nto,3,0);
    ins_quest ((char *) &lgr.fl_vk_ges,3,0);
    ins_quest ((char *) &lgr.frm,1,0);
    ins_quest ((char *) &lgr.iakv,2,0);
    ins_quest ((char *) lgr.inv_rht,0,2);
    ins_quest ((char *) lgr.ls_abgr,0,2);
    ins_quest ((char *) lgr.ls_kz,0,2);
    ins_quest ((char *) &lgr.ls_sum,1,0);
    ins_quest ((char *) lgr.pers,0,13);
    ins_quest ((char *) &lgr.pers_anz,1,0);
    ins_quest ((char *) lgr.pos_kum,0,2);
    ins_quest ((char *) lgr.pr_ausw,0,2);
    ins_quest ((char *) lgr.pr_bel_entl,0,2);
    ins_quest ((char *) lgr.pr_lgr_kz,0,2);
    ins_quest ((char *) &lgr.pr_lst,2,0);
    ins_quest ((char *) lgr.pr_vk_kz,0,2);
    ins_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    ins_quest ((char *) &lgr.reg_kt_lng,3,0);
    ins_quest ((char *) &lgr.reg_kue_lng,3,0);
    ins_quest ((char *) &lgr.reg_lng,3,0);
    ins_quest ((char *) &lgr.reg_tks_lng,3,0);
    ins_quest ((char *) &lgr.reg_tkt_lng,3,0);
    ins_quest ((char *) lgr.smt_kz,0,2);
    ins_quest ((char *) &lgr.sonst_einh,1,0);
    ins_quest ((char *) &lgr.sprache,1,0);
    ins_quest ((char *) lgr.sw_kz,0,2);
    ins_quest ((char *) &lgr.tou,2,0);
    ins_quest ((char *) &lgr.vrs_typ,1,0);
    ins_quest ((char *) lgr.inv_akv,0,2);
         sqltext = "update lgr set lgr.mdn = ?,  lgr.fil = ?,  "
"lgr.lgr = ?,  lgr.lgr_bz = ?,  lgr.delstatus = ?,  lgr.lgr_kz = ?,  "
"lgr.lgr_smt_kz = ?,  lgr.lgr_gr = ?,  lgr.lgr_kla = ?,  "
"lgr.abr_period = ?,  lgr.adr = ?,  lgr.afl = ?,  lgr.bli_kz = ?,  "
"lgr.dat_ero = ?,  lgr.fl_lad = ?,  lgr.fl_nto = ?,  lgr.fl_vk_ges = ?,  "
"lgr.frm = ?,  lgr.iakv = ?,  lgr.inv_rht = ?,  lgr.ls_abgr = ?,  "
"lgr.ls_kz = ?,  lgr.ls_sum = ?,  lgr.pers = ?,  lgr.pers_anz = ?,  "
"lgr.pos_kum = ?,  lgr.pr_ausw = ?,  lgr.pr_bel_entl = ?,  "
"lgr.pr_lgr_kz = ?,  lgr.pr_lst = ?,  lgr.pr_vk_kz = ?,  "
"lgr.reg_bed_theke_lng = ?,  lgr.reg_kt_lng = ?,  "
"lgr.reg_kue_lng = ?,  lgr.reg_lng = ?,  lgr.reg_tks_lng = ?,  "
"lgr.reg_tkt_lng = ?,  lgr.smt_kz = ?,  lgr.sonst_einh = ?,  "
"lgr.sprache = ?,  lgr.sw_kz = ?,  lgr.tou = ?,  lgr.vrs_typ = ?,  "
"lgr.inv_akv = ? "

#line 165 "lgr.rpp"
                               "where lgr = ? ";
  
         ins_quest ((char *) &lgr.lgr, 2, 0);

         upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &lgr.mdn,1,0);
    ins_quest ((char *) &lgr.fil,1,0);
    ins_quest ((char *) &lgr.lgr,2,0);
    ins_quest ((char *) lgr.lgr_bz,0,25);
    ins_quest ((char *) &lgr.delstatus,1,0);
    ins_quest ((char *) &lgr.lgr_kz,1,0);
    ins_quest ((char *) lgr.lgr_smt_kz,0,4);
    ins_quest ((char *) &lgr.lgr_gr,1,0);
    ins_quest ((char *) lgr.lgr_kla,0,2);
    ins_quest ((char *) lgr.abr_period,0,2);
    ins_quest ((char *) &lgr.adr,2,0);
    ins_quest ((char *) &lgr.afl,1,0);
    ins_quest ((char *) lgr.bli_kz,0,2);
    ins_quest ((char *) &lgr.dat_ero,2,0);
    ins_quest ((char *) &lgr.fl_lad,3,0);
    ins_quest ((char *) &lgr.fl_nto,3,0);
    ins_quest ((char *) &lgr.fl_vk_ges,3,0);
    ins_quest ((char *) &lgr.frm,1,0);
    ins_quest ((char *) &lgr.iakv,2,0);
    ins_quest ((char *) lgr.inv_rht,0,2);
    ins_quest ((char *) lgr.ls_abgr,0,2);
    ins_quest ((char *) lgr.ls_kz,0,2);
    ins_quest ((char *) &lgr.ls_sum,1,0);
    ins_quest ((char *) lgr.pers,0,13);
    ins_quest ((char *) &lgr.pers_anz,1,0);
    ins_quest ((char *) lgr.pos_kum,0,2);
    ins_quest ((char *) lgr.pr_ausw,0,2);
    ins_quest ((char *) lgr.pr_bel_entl,0,2);
    ins_quest ((char *) lgr.pr_lgr_kz,0,2);
    ins_quest ((char *) &lgr.pr_lst,2,0);
    ins_quest ((char *) lgr.pr_vk_kz,0,2);
    ins_quest ((char *) &lgr.reg_bed_theke_lng,3,0);
    ins_quest ((char *) &lgr.reg_kt_lng,3,0);
    ins_quest ((char *) &lgr.reg_kue_lng,3,0);
    ins_quest ((char *) &lgr.reg_lng,3,0);
    ins_quest ((char *) &lgr.reg_tks_lng,3,0);
    ins_quest ((char *) &lgr.reg_tkt_lng,3,0);
    ins_quest ((char *) lgr.smt_kz,0,2);
    ins_quest ((char *) &lgr.sonst_einh,1,0);
    ins_quest ((char *) &lgr.sprache,1,0);
    ins_quest ((char *) lgr.sw_kz,0,2);
    ins_quest ((char *) &lgr.tou,2,0);
    ins_quest ((char *) &lgr.vrs_typ,1,0);
    ins_quest ((char *) lgr.inv_akv,0,2);
         ins_cursor = prepare_sql ("insert into lgr (mdn,  fil,  "
"lgr,  lgr_bz,  delstatus,  lgr_kz,  lgr_smt_kz,  lgr_gr,  lgr_kla,  abr_period,  adr,  "
"afl,  bli_kz,  dat_ero,  fl_lad,  fl_nto,  fl_vk_ges,  frm,  iakv,  inv_rht,  ls_abgr,  "
"ls_kz,  ls_sum,  pers,  pers_anz,  pos_kum,  pr_ausw,  pr_bel_entl,  pr_lgr_kz,  "
"pr_lst,  pr_vk_kz,  reg_bed_theke_lng,  reg_kt_lng,  reg_kue_lng,  reg_lng,  "
"reg_tks_lng,  reg_tkt_lng,  smt_kz,  sonst_einh,  sprache,  sw_kz,  tou,  vrs_typ,  "
"inv_akv) "

#line 172 "lgr.rpp"
                                   "values "
                                   "(?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?)");

#line 174 "lgr.rpp"

         ins_quest ((char *) &lgr.lgr, 2, 0);
         del_cursor = prepare_sql ("delete from lgr "
                               "where lgr = ? ");

         ins_quest ((char *) &lgr.lgr, 2, 0);
         test_upd_cursor = prepare_sql ("select lgr from lgr "
                               "where lgr = ? ");
}

 
int LGR_CLASS::prep_awcursor (void)
/**
Cursor fuer Auswahl vorbereiten.
**/
{
         int cursor_ausw;

         out_quest ((char *) &lgr.lgr, 2, 0);

         cursor_ausw = prepare_scroll ("select lgr, lgr_bz from lgr "
                                       "where lgr > 0 "
                                       "order by lgr_bz");
         if (sqlstatus)
         {
                     return (-1);
         }
         return (cursor_ausw);
}
int LGR_CLASS::prep_awcursor (char *sqlstring)
/**
Cursor fuer Auswahl vorbereiten.
**/
{
         int cursor_ausw;


         out_quest ((char *) &lgr.lgr, 2, 0);

         cursor_ausw = prepare_scroll (sqlstring);
         if (sqlstatus)
         {
                     return (-1);
         }
         return (cursor_ausw);
}


int LGR_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

void LGR_CLASS::fill_aw (int cursor_ausw)
/**
Struktur fuer Auswahl fuellen.
**/
{
 
        sort_awanz = 0; 
	  while (fetch_scroll (cursor_ausw, NEXT) == 0)
        {
			       sprintf (sort_awtab[sort_awanz].sort1, "%ld",
						      lgr.lgr);
			       sprintf (sort_awtab[sort_awanz].sort2, "%s",
						      lgr.lgr_bz);
				  sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
				  sort_awanz ++;
                          if (sort_awanz == MAXSORT) break;
        }
        return;
}


int LGR_CLASS::ShowAllBu (HWND hWnd, int ws_flag)
/**
Auswahl ueber Gruppen anzeigen.
**/
{
         int cursor_ausw;

         cursor_ausw = prep_awcursor ();
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         fill_aw (cursor_ausw);

         this->DB_CLASS::ShowAllBu (hWnd,
                                    ws_flag,
                                    cursor_ausw, 
                                    endsort,  
                                    IsAwClck,
                                    (char *) sort_awtab,
                                    sort_awanz,
                                    (char *) &sort_aw,
                                    (int) sizeof (struct SORT_AW),
                                    &fsort_aw,  
                                    &fsort_awl,
                                    &fsort_awub);
	    sort_idx = sort_awtab[sort_idx].sortidx;
        fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
        close_sql (cursor_ausw);
        return (0);
}

int LGR_CLASS::ShowBuQuery (HWND hWnd, int ws_flag, 
								           char *sqlstring )
/**
Auswahl ueber Gruppen anzeigen.
**/
{
         int cursor_ausw;

         cursor_ausw = prep_awcursor (sqlstring);
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         fill_aw (cursor_ausw);

		 if (fsort_aw.mask[0].item == NULL)
		 {
		            disp_mess ("Fehler", 2);
         }

         this->DB_CLASS::ShowAllBu (hWnd,
                                    ws_flag,
                                    cursor_ausw, 
                                    endsort,  
                                    IsAwClck,
                                    (char *) sort_awtab,
                                    sort_awanz,
                                    (char *) &sort_aw,
                                    (int) sizeof (struct SORT_AW),
                                    &fsort_aw,  
                                    &fsort_awl,
                                    &fsort_awub);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 1;
        }
	    sort_idx = sort_awtab[sort_idx].sortidx;
        fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
        close_sql (cursor_ausw);
        return (0);
}


int LGR_CLASS::QueryBu (HWND hWnd, int ws_flag)
/**
Auswahl mit Query-String.
**/
{
	     char sqlstring [1000]; 

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select lgr, lgr_bz "
							"from lgr "
                            "where %s "
							"order by lgr", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select lgr, lgr_bz "
							"from lgr "
                            "where lgr > 0 order by lgr");
         }
         return this->ShowBuQuery (hWnd, ws_flag, sqlstring);
}

int LGR_CLASS::ShowBuQuery (HWND hWnd, int ws_flag) 
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;

	    savecurrent = current_form;
 		sort_awanz = 0;
		while (fetch_scroll (cursor_ausw, NEXT) == 0)
        {
			         sprintf (sort_awtab[sort_awanz].sort1, "%ld",
						      lgr.lgr);
			         sprintf (sort_awtab[sort_awanz].sort2, "%s",
						      lgr.lgr_bz);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					 if (sort_awanz == MAXSORT) break;
        }

		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
        eWindow = OpenListWindowEnF (10, 40, 8, 20, 0);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
		Setlistenter (1);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
		 sort_idx = sort_awtab[sort_idx].sortidx;
         fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
         close_sql (cursor_ausw);
         return 0;
}

int LGR_CLASS::PrepareQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 

         ReadQuery (qform, qnamen); 

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select lgr, lgr_bz "
							"from lgr "
                            "where %s "
							"order by lgr", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select lgr, lgr_bz "
							"from lgr "
                            "where lgr > 0 order by lgr");
         }
         out_quest ((char *) &lgr.lgr, 2, 0);
         out_quest ((char *) lgr.lgr_bz, 0, 49);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prepare_scroll (sqlstring);
         sql_mode = old_mode;
         if (sqlstatus < 0)
         {
                       return (sqlstatus);
         }
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}

