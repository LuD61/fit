#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "kun.h"
#include "mo_meld.h"
#include "mo_qkun.h"

KUN_CLASS kun_class;

static BOOL WithTest = FALSE;

static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                       syskey = KEY11;
                       break_enter ();
                       return 1;
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY11)
				{
					syskey = KEY11;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		}
        return 0;
}

void QueryKun::SetTestKunProc (int (*Proc) (long))
{
	    kun_class.SetTestKunProc (Proc);
		if (Proc)
		{
			WithTest = TRUE;
		}
		else
		{
			WithTest = FALSE;
		}
}

int QueryKun::querykun (HWND hWnd)
/**
Query ueber Textnummer.
**/
{

        HANDLE hMainInst;

        static char lkun [41];
        static char lkun_krz1 [41];
        static char lort1 [41];
		static char OK[20];
		static char CA[20];
		static char NE[20];

        static ITEM ikunval ("kun", 
                              lkun, 
                              "Kunden-Nr.....:", 
                              0);

        static ITEM ikun_krz1val ("kun_krz1", 
                                   lkun_krz1, 
                                   "Kunden-Name...:", 
                                   0);

        static ITEM ikun_ort1val   ("ort1", 
                                   lort1, 
                                   "Ort...........:", 
                                   0);


        static ITEM iOK  ("", OK, "", 0);
        static ITEM iCA  ("", CA, "", 0);
        static ITEM iNEW ("", NE, "", 0);

        static field _qtxtform[] = {
           &ikunval,      40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krz1val, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_ort1val, 40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,          15, 0, 5, 5, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,          15, 0, 5,22, 0, "", BUTTON, 0,testquery ,KEY5,
           &iNEW,         15, 0, 5,39, 0, "", BUTTON, 0,testquery ,KEY11,
		};

        static form qtxtform = {6, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"kun.kun", "adr.adr_krz", 
                                 "adr.ort1",  NULL, NULL, NULL};

        HWND query;
		int savefield;
		form *savecurrent;

        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        SetFkt (6, leer, NULL);
        set_fkt (NULL, 7);
        SetFkt (7, leer, NULL);
        set_fkt (NULL, 9);
        SetFkt (9, leer, NULL);
 
		strcpy (OK, "OK");
		strcpy (CA, "Cancel");
		strcpy (NE, "Neu");
		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (9, 60, 11, 10, hMainInst,
                               "Suchkriterien f�r Kunden");
        syskey = 0;
		no_break_end ();
        EnableWindows (hWnd, FALSE); 
        enter_form (query, &qtxtform, 0, 0);

		SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE); 
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
		SetActiveWindow (hWnd);
        if (syskey == KEY12 || syskey == KEY11 || syskey == KEYCR)
        {
			      if (WithTest)
				  {
  					     if (syskey == KEY11) kun_class.SetNewEx (); 
                         if (kun_class.PrepareQueryEx (&qtxtform, qnamen) == 0)
						 {
                              kun_class.ShowBuQueryEx (hWnd, 0);
						 }

                         else
						 {
                                syskey = KEY5;
						 }
				  }
				  else
				  {
  					     if (syskey == KEY11) kun_class.SetNew (); 
                         if (kun_class.PrepareQuery (&qtxtform, qnamen) == 0)
						 {
                              kun_class.ShowBuQuery (hWnd, 0);
						 }

                         else
						 {
                                syskey = KEY5;
						 }
				 }
        }
		WithTest = FALSE;

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}

