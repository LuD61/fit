#ifndef _MO_WOTL_DEF
#define _MO_WITL_DEF

#define MAXLEN 40

class WOTLIST
{
            private :
                int mamainmax;
                int mamainmin;
                HWND    hwndTB;
                char *eingabesatz;
                unsigned char *ausgabesatz;
                int zlen;
                int feld_anz;
                int banz;
                PAINTSTRUCT aktpaint;
                char *SwSaetze[30000];
                int PageView;
                unsigned char DlgSatz [20 + MAXLEN + 2];
                FELDER *LstZiel;
                unsigned char *LstSatz;
                int Lstzlen;
                int Lstbanz;
                int SwRecs;
                int AktRow;
                int AktColumn;
                int scrollpos;
                TEXTMETRIC tm;
                char InfoCaption [80];
                int WithMenue;
                int WithToolBar;
                BOOL ListAktiv;
                HWND hMainWindow;

             public :
                static void SetMax0 (int);
                static void SetMin0 (int);
                WOTLIST ();

                void SetMenue (int with)
                {
                    WithMenue = with;
                }

                void SetToolMenue (int with)
                {
                    WithToolBar = with;
                }

                void SetMax (void)
                {
                    mamainmax = 1;
                    SetMax0 (mamainmax);
                }

                void SetMin (void)
                {
                    mamainmin = 1;
                    SetMin0 (mamainmin);
                }

                void InitMax (void)
                {
                    mamainmax = 0;
                    SetMax0 (mamainmax);
                }

                void InitMin (void)
                {
                    mamainmin = 0;
                    SetMin0 (mamainmin);
                }

                BOOL IsListAktiv (void)
                {
                    return ListAktiv;
                }

                void SethMainWindow (HWND hMainWindow)
                {
                    this->hMainWindow = hMainWindow;
                }

                HWND GetMamain1 (void); 
                static int SetRowItem (void);
                static int WriteRow (void);
                static void GenNewPosi (void);
                static int PosiEnd (long posi);
                static int Querykun (void);
                static int SearchKun (void);
                static int QueryFil (void);
                static int QueryTou (void);
                static int TouInfo (void);
                static int TestKunList (long);

                static int SaveKun (void);
                static int KunInWoTou (void);
                static int fetchKun (void);
                static int fetchFil (void);
                static int SetTouKey9 (void);
                static int TestTou (void);

                static int TestAppend (void);
                static int DeleteLine (void);
                static int InsertLine (void);
                static int AppendLine (void);
                static void WritePos (int);
                static int TestRow (void);
                static int WriteAllPos (void);
                static int dokey5 (void);
                static void SaveWotou (void);
                static void SetWotou (void);
                static void RestoreWotou (void);
                static int Schirm (void);
                
                void GetCfgValues (void);
                void InitSwSaetze (void);
                int ToMemory (int pos);
                void SetStringEnd (char *, int);
                void uebertragen (void);
                void ShowDB (void);
                void ReadDB (void);
                void Enter (short, short, short);
                void Work (void);
                void DestroyMainWindow (void);
                HWND CreateMainWindow (void);
                void MoveMamain1 ();
                void MaximizeMamain1 ();

                void SetAbrKz (BOOL kz);
                void SetFieldAttr (char *, int);
                void SetRecHeight (void);
                void SethwndTB (HWND);
                void SetTextMetric (TEXTMETRIC *);
                void SetLineRow (int);
                void SetListLines (int); 
                void OnPaint (HWND, UINT, WPARAM, LPARAM);
                void MoveListWindow (void);
                void BreakList (void);
                void OnHScroll (HWND, UINT, WPARAM, LPARAM);
                void OnVScroll (HWND, UINT, WPARAM, LPARAM);
                void FunkKeys (WPARAM, LPARAM);
                int  GetRecanz (void);
                void SwitchPage0 (int);
                HWND Getmamain2 (void);
                HWND Getmamain3 (void);
                void SetFont (mfont *); 
                void SetListFont (mfont *);
                void ChoiseFont (mfont *);
                void FindString (void);
                void SetLines (int);
                int GetAktRow (void);
                int GetAktRowS (void);
                void SetColors (COLORREF, COLORREF);
                void SetListFocus (void);
                void SetTourDiv (long);
 
};
#endif