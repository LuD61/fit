#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbfunc.h"
#include "tou.h"
#include "mo_meld.h"
#include "mo_qa.h"

TOU_CLASS tour;

static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
        return 0;
}


int QueryClass::querytou (HWND hWnd)
/**
Query ueber Touren.
**/
{

        HANDLE hMainInst;

        static char touval[41];
        static char tou_bezval[41];
        static char fz_klaval[41];
        static char fzval[41];
        static char str_zeitval[41];
        static char fah_1val[41];
        static char fah_2val[41];
         
        static ITEM itouval ("tou", 
                             touval, 
                             "Tour.........:", 
                             0);
        static ITEM itou_bezval ("tou_bez",
                             tou_bezval,
                             "Tour-Name....:",
                             0);
        static ITEM ifz_klaval ("fz_kla",
                             fz_klaval,
                             "FZ-Klasse....:", 
                             0);
        static ITEM ifzval ("fz", 
                             fzval, 
                             "Fahrzeug.....:", 
                             0);
        static ITEM istr_zeitval ("srt_zeit",
                             str_zeitval, 
                             "Startzeit....:", 
                             0);
        static ITEM ifah_1val ("fah_1", 
                             fah_1val, 
                             "Fahrer 1.....:", 
                             0);
        static ITEM ifah_2val ("fah_2", 
                             fah_2val, 
                             "Fahrer 2.....:", 
                             0);


        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);

        static field _qtxtform[] = {
           &itouval,        40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &itou_bezval,    40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ifz_klaval,     40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ifzval      ,   40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &istr_zeitval,   40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ifah_1val,      40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &ifah_2val,      40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,            15, 0, 9,15, 0, "", BUTTON, 0, 0,KEY12,
           &iCA,            15, 0, 9,32, 0, "", BUTTON, 0, 0,KEY5,
		};

        static form qtxtform = {9, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"tou", "tou_bz", "fz_kla", 
                                 "fz", "srt_zeit",
                                 "fah_1", "fah_2",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (12, 62,9, 10, hMainInst,
                               "Suchkriterien f�r Auftr�ge");
        enter_form (query, &qtxtform, 0, 0);

        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                  if (tour.PrepareQuery (&qtxtform, qnamen) == 0)
                  {
                             tour.ShowBuQuery (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}
