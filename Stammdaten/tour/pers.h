#ifndef _PERSDEF
#define _PERSDEF
#include "dbclass.h"

struct PERS {
   short     abt;
   long      adr;
   short     anz_tag_wo;
   double    arb_zeit_tag;
   double    arb_zeit_wo;
   char      beschaef_kz[2];
   long      eintr_dat;
   short     fil;
   short     kost_st;
   short     mdn;
   char      pers[13];
   long      pers_mde;
   double    pers_pkt;
   double    rab_proz;
   double    std_satz;
   char      taet[25];
   char      taet_kz[2];
   short     tar_gr;
   char      zahlw[2];
   long      txt_nr;
   short     delstatus;
};
extern struct PERS pers, pers_null;

#line 6 "pers.rh"

class PERS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
               int prep_awcursor (void);
               int prep_awcursor (char *);
               void fill_aw (int);

       public :
               PERS_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
               int ShowAllBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND hWnd, int ws_flag, char *);
               int QueryBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND, int); 
               int PrepareQuery (form *, char *[]);
};
#endif
