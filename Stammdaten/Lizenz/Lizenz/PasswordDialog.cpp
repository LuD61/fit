// PasswordDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Lizenz.h"
#include "PasswordDialog.h"
#include "PasswordHandler.h"


// CPasswordDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CPasswordDialog, CDialog)

#define PHANDLER CPasswordHandler::GetInstance ()
#define MAXTRIES 3

CPasswordDialog::CPasswordDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPasswordDialog::IDD, pParent)
{
//	m_DlgColor = GetSysColor (COLOR_3DFACE);
//	m_DlgColor = RGB (255, 255, 204);
	m_DlgColor = RGB (223, 227, 236);
	m_DlgBrush = NULL;
	User     = _T("");
	Password = _T("");
	Message = _T("Bitte geben Sie die Zugangsdaten ein");
	MessageColor = RGB (102, 153, 255);
	ErrorColor = RGB (255, 0, 0);
	Error = FALSE;
	trycount = 0;
}

CPasswordDialog::~CPasswordDialog()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	PHANDLER->DestroyInstance ();
}

void CPasswordDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAPTION, m_Caption);
	DDX_Control(pDX, IDOK, m_Ok);
	DDX_Control(pDX, IDCANCEL, m_Cancel);
	DDX_Control(pDX, IDC_LUSER, m_LUser);
	DDX_Control(pDX, IDC_USER, m_User);
	DDX_Control(pDX, IDC_LPASSWORD, m_LPassword);
	DDX_Control(pDX, IDC_PASSWORD, m_Password);
	DDX_Control(pDX, IDC_MESSAGE, m_Message);
}


BEGIN_MESSAGE_MAP(CPasswordDialog, CDialog)
	ON_WM_CTLCOLOR ()
	ON_COMMAND (IDOK, OnOK)
	ON_COMMAND (IDCANCEL, OnCancel)
END_MESSAGE_MAP()


BOOL CPasswordDialog::OnInitDialog()
{

	HICON m_Icon = LoadIcon (AfxGetInstanceHandle (),MAKEINTRESOURCE (IDR_MAINFRAME));
	SetIcon (m_Icon, FALSE);

	SetWindowText (_T(" Benutzer und Passwort"));
	CDialog::OnInitDialog();
	m_Caption.SetParts (60);
	m_Caption.SetWindowText (_T(" Benutzer und Passwort"));
	m_Caption.SetBoderStyle (m_Caption.Solide);
	m_Caption.SetOrientation (m_Caption.Left);
	m_Caption.SetDynamicColor (TRUE);
	m_Caption.SetTextColor (RGB (255, 255, 153));
	m_Caption.SetBkColor (RGB (192, 192, 192));

//	COLORREF BkColor (CColorButton::DynColorGray);
//	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF BkColor (RGB (0, 0, 255));
//	COLORREF DynColor = RGB (0, 0, 255);
	COLORREF RolloverColor = CColorButton::DynColorBlue;
	COLORREF DynColor = CColorButton::DynColorBlue;
//	COLORREF RolloverColor = RGB (192, 192, 192);

	m_Ok.SetWindowText (_T("OK"));
	m_Ok.nID = IDOK;
//	m_Ok.SetToolTip (_T("Bearbeiten"));
	m_Ok.Orientation = m_Ok.Center;
	m_Ok.TextStyle = m_Ok.Standard;
	m_Ok.BorderStyle = m_Ok.Solide;
	m_Ok.DynamicColor = TRUE;
	m_Ok.TextColor =RGB (0, 0, 0);
	m_Ok.SetBkColor (BkColor);
	m_Ok.DynColor = DynColor;
	m_Ok.RolloverColor = RolloverColor;
	m_Ok.LoadBitmap (IDB_OK);
	m_Ok.LoadMask (IDB_OKMASK);

	m_Cancel.SetWindowText (_T("Abbrechen"));
	m_Cancel.nID = IDCANCEL;
//	m_Cancel.SetToolTip (_T("Bearbeiten"));
	m_Cancel.Orientation = m_Cancel.Center;
	m_Cancel.TextStyle = m_Cancel.Standard;
	m_Cancel.BorderStyle = m_Cancel.Solide;
	m_Cancel.DynamicColor = TRUE;
	m_Cancel.TextColor =RGB (0, 0, 0);
	m_Cancel.SetBkColor (BkColor);
	m_Cancel.DynColor = DynColor;
	m_Cancel.RolloverColor = RolloverColor;
	m_Cancel.LoadBitmap (IDB_CANCEL);
	m_Cancel.LoadMask (IDB_CANCELMASK);

    Form.Add (new CFormField (&m_User, EDIT,        (CString *) &User, VSTRING));
    Form.Add (new CFormField (&m_Password, EDIT,    (CString *) &Password, VSTRING));
    Form.Add (new CFormField (&m_Message, EDIT,     (CString *) &Message, VSTRING));

	Font.CreatePointFont (95, _T("Dlg"));

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	StaticFont.CreateFontIndirect (&l);
	l.lfHeight = 25;
	CaptionFont.CreateFontIndirect (&l);
	m_Caption.SetFont (&CaptionFont);

	m_Ok.SetFont (&Font);
	m_Cancel.SetFont (&Font);

	m_LUser.SetFont (&StaticFont);
	m_User.SetFont (&Font);
	m_LPassword.SetFont (&StaticFont);
	m_Password.SetFont (&Font);
	m_Message.SetFont (&StaticFont);

	Form.Show ();

	return TRUE;
}

HBRUSH CPasswordDialog::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	CRect rect;
	if (nCtlColor == CTLCOLOR_DLG && m_DlgColor != NULL)
	{
		    if (m_DlgBrush == NULL)
			{
//				m_DlgBrush = CreateSolidBrush (m_DlgColor);
                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC)
	{
		    if (m_DlgBrush == NULL)
			{
				m_DlgBrush = CreateSolidBrush (m_DlgColor);
			}
			if (*pWnd == m_Message)
			{
				if (!Error)
				{
					pDC->SetTextColor (MessageColor);
				}
				else
				{
					pDC->SetTextColor (ErrorColor);
				}
			}
			else
			{
				pDC->SetTextColor (RGB (92, 92, 92));
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}


// CPasswordDialog-Meldungshandler

void CPasswordDialog::OnOK ()
{
	Form.Get ();
	trycount ++;
	if (PHANDLER->TestUser (User, Password))
	{
		CDialog::OnOK ();
	}
	else if (trycount < MAXTRIES)
	{
		Error = TRUE;
		MessageBeep (MB_ICONASTERISK);
		Message = _T("Benutzer oder Passwort sind falsch");
		Password = _T("");
		m_User.SetFocus ();
		Form.GetFormField (&m_Message)->Show ();
	}
	else
	{
		CDialog::OnCancel ();
	}
}

