#include "StdAfx.h"
#include "dbunicode.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#ifdef UNICODE
#define _tsmemset wmemset
#else
#define _tsmemset memset
#endif

CDbUniCode::CDbUniCode(void)
{
}

CDbUniCode::~CDbUniCode(void)
{
}

void CDbUniCode::DbFromUniCode (LPTSTR tstr, LPSTR str, int len)
{
#ifdef UNICODE
		WideCharToMultiByte(CP_ACP, WC_COMPOSITECHECK , tstr, 
							-1,  str,  len,  NULL,  NULL);
#else
	    strcpy (str, tstr);
#endif
}

void CDbUniCode::DbToUniCode (LPTSTR tstr, LPSTR str)
{
#ifdef UNICODE
	LPSTR aString;
	size_t len = strlen (str) + 2;
	aString = new char [len * 2];
	if (aString == NULL) return;
	memcpy (aString, str, len);
	TrimRight ((UCHAR *) aString);
	len = strlen (aString) + 1;
	_tcscpy (tstr, _T(""));
    MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, aString, -1, tstr, (int) len);
	tstr[len] = 0;
	CString c = tstr;
	c.TrimRight ();
	_tcscpy (tstr, c.GetBuffer ());
	delete aString;
#else
	strcpy (tstr, str); 
#endif
}


void CDbUniCode::DbToUniCode (CCodeProperties& Code, LPTSTR tstr, LPSTR str, int len)
{
	LPSTR s;
    s = new char [len];
	strcpy (s, str);
	TrimRight ((UCHAR *) s);
	_tsmemset (tstr, 0, len / 2 + 1);
	int i = 0;
	for (i = 0; i < (int) strlen (s); i ++)
	{
		if (i >= len) break;
        short source = (UCHAR) s[i];
		short target = Code.GetSource (source);
		if (target != 0)
		{
			tstr[i] = (TCHAR) target;
		}
		else 
		{
			tstr[i] = (TCHAR) source;
		}
	}
	tstr[i] = (TCHAR) 0;
	delete s;
}

void CDbUniCode::DbFromUniCode (CCodeProperties& Code, LPTSTR tstr, LPSTR str, int len)
{
	LPTSTR s;

	CString C = tstr;
	C.TrimRight ();
	_tcscpy (tstr, C.GetBuffer ());
	s = new TCHAR [len];
   _tcscpy (s, tstr);
	memset (str, 0, len);
	int i = 0;
	for (i = 0; i < (int) _tcslen (s); i ++)
	{
		if (i >= len) break;
        short target = (TCHAR) s[i];
		short source = Code.GetTarget (target);
		if (source != 0)
		{
			str[i] = (UCHAR) source;
		}
		else if (target < 256)
		{
			str[i] = (CHAR) target;
		}
		else
		{
			str[i] = '?';
		}
	}
	str[i] = (char) 0;
	delete s;
}

void CDbUniCode::TrimRight (UCHAR *s)
{
	UCHAR *str = s + strlen ((LPSTR) s);
	for (; (*str <= 0x20) && (str != s); str -= 1);
	if (*str > 0x20) str += 1;
	*str = 0;
}
