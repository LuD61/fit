#ifndef _LOG_LIST_DEF
#define _LOG_LIST_DEF
#pragma once
#include <odbcinst.h>

class CLogList
{
public:
    TCHAR Date [11];
    TCHAR Time [10];
	TCHAR Text[256];

	CLogList(void);
	CLogList(LPTSTR Date, LPTSTR Time, LPTSTR Text);
	~CLogList(void);
	void SetDate (LPTSTR Date);
	void SetDate (CString& Date);
	void SetTime (LPTSTR Time);
	void SetTime (CString& Time);
	void SetText (LPTSTR Text);
	void SetText (CString& Text);
};
#endif
