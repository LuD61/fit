#include "StdAfx.h"
#include "LogList.h"

CLogList::CLogList(void)
{
	memset (Date, 0, sizeof (Date));
	memset (Time, 0, sizeof (Time));
	memset (Text, 0, sizeof (Text));
}

CLogList::CLogList(LPTSTR Date, LPTSTR Time, LPTSTR Text)
{
	memset (this->Date, 0, sizeof (this->Date));
	memset (this->Time, 0, sizeof (this->Time));
	memset (this->Text, 0, sizeof (this->Text));
	_tcsncpy (this->Date, Date, sizeof (this->Date) - 1);
	_tcsncpy (this->Time, Time, sizeof (this->Time) - 1);
	_tcsncpy (this->Text, Text, sizeof (this->Text) - 1);
}


CLogList::~CLogList(void)
{
}


void CLogList::SetDate (LPTSTR Date)
{
	memset (this->Date, 0, sizeof (this->Date));
	_tcsncpy (this->Date, Date, sizeof (this->Date) - 1);
}

void CLogList::SetDate (CString& Date)
{
	memset (this->Date, 0, sizeof (this->Date));
	_tcsncpy (this->Date, Date.GetBuffer (), sizeof (this->Date) - 1);
}

void CLogList::SetTime (LPTSTR Time)
{
	memset (this->Time, 0, sizeof (this->Time));
	_tcsncpy (this->Time, Time, sizeof (this->Time) - 1);
}

void CLogList::SetTime (CString& Time)
{
	memset (this->Time, 0, sizeof (this->Time));
	_tcsncpy (this->Time, Time.GetBuffer (), sizeof (this->Time) - 1);
}

void CLogList::SetText (LPTSTR Text)
{
	memset (this->Text, 0, sizeof (this->Text));
	_tcsncpy (this->Text, Text, sizeof (this->Text) - 1);
}

void CLogList::SetText (CString& Text)
{
	memset (this->Text, 0, sizeof (this->Text));
	_tcsncpy (this->Text, Text.GetBuffer (), sizeof (this->Text) - 1);
}
