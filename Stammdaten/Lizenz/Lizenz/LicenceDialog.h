#pragma once
#include "ColorButton.h"
#include "Label.h"
#include "VLabel.h"
#include "VHLabel.h"
#include "afxwin.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "Stc.h"
#include "DbClass.h"
#include "BkBitmap.h"
#include "ChoiceUser.h"
#include "ChoiceLog.h"
#include "afxdtctl.h"
#include "ButtonContainer.h"


// CLicenceDialog-Dialogfeld

class CLicenceDialog : public CDialog
{
	DECLARE_DYNAMIC(CLicenceDialog)

public:
	CLicenceDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CLicenceDialog();

// Dialogfelddaten
	enum { IDD = IDD_LIZENZ_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);

	DECLARE_MESSAGE_MAP()

private:
    short licence_count;
	CString kun_name;
	CString text;
	CString cDate;
	CBkBitmap BkBitmap;
	DB_CLASS DbClass;
	CButtonContainer Container;

	COLORREF m_DlgColor;
	HBRUSH m_DlgBrush;
	CFont Font;
	CFont CaptionFont;
	CFont StaticFont;
	CFormTab Form;
	CChoiceUser *Choice;
	CChoiceLog *ChoiceLog;

	CLabel m_Caption;
	CColorButton m_Ok;
	CColorButton m_Cancel;
	CColorButton m_ChoiceUser;
	CColorButton m_ChoiceLog;
	CColorButton m_FreeLicence;
	CColorButton m_SetLicence;
	CNumEdit m_LicenceCount;
	CSpinButtonCtrl m_SpinLicenceCount;
	CStatic m_LLicenceCount;
	CStatic m_LKunName;
	CTextEdit m_KunName;
	CStatic m_LText;
	CTextEdit m_Text;

public:
	afx_msg void OnOK ();
	afx_msg void OnUserChoice ();
	afx_msg void OnLogChoice ();
	afx_msg void OnFreeLicence ();
	afx_msg void OnSetLicence ();
public:
	CDateTimeCtrl m_TestDate;
public:
	CStatic m_LTestDate;
};
