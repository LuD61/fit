#pragma once
#include "DataCollection.h"

class CPasswordItem
{
private:
	CString *User;
	CString *Password;
public:
    void SetUser (CString *User)
	{
		this->User = new CString (*User);
	}

	CString *GetUser ()
	{
		return User;
	}

    void SetPassword (CString *Password)
	{
		this->Password = new CString (*Password);
	}

	CString *GetPassword ()
	{
		return Password;
	}

	CPasswordItem ()
	{
		User = NULL;
		Password = NULL;
	}

	CPasswordItem (CString *User, CString *Password)
	{
		this->User = new CString (*User);
		this->Password = new CString (*Password);
	}
	CPasswordItem (LPTSTR User, LPTSTR Password)
	{
		this->User = new CString (User);
		this->Password = new CString (Password);
	}
	~CPasswordItem ()
	{
		if (User != NULL)
		{
			delete User;
		}
		if (Password != NULL)
		{
			delete Password;
		}
	}
};

class CPasswordHandler
{
private:
	static CPasswordHandler *Instance;

	CDataCollection<CPasswordItem *> PasswordList;
	
protected:
	CPasswordHandler(void);
	~CPasswordHandler(void);
public:
	static CPasswordHandler *GetInstance ();
	static void DestroyInstance ();
	BOOL TestUser (CString& User, CString& Password);
};
