#include "StdAfx.h"
#include "ChoiceUser.h"
#include "DbUniCode.h"
#include "StrFuncs.h"
#include "Token.h"
#include "DynDialog.h"
#include "DbQuery.h"
#include "DbTime.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#define BLUE_COL 1

int CChoiceUser::Sort1 = -1;
int CChoiceUser::Sort2 = -1;
int CChoiceUser::Sort3 = -1;

CChoiceUser::CChoiceUser(CWnd* pParent) 
        : CChoiceX(pParent)
{
	ptcursor = -1;
	Where = "";
	Bean.ArchiveName = _T("UserList.prp");
	FromFilter = FALSE;
}

CChoiceUser::~CChoiceUser() 
{
	DestroyList ();
	if (ptcursor != -1) DbClass->sqlclose (ptcursor);
	ptcursor = -1;
}

void CChoiceUser::DoDataExchange(CDataExchange* pDX)
{
	CChoiceX::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChoice)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChoiceUser, CChoiceX)
	//{{AFX_MSG_MAP(CChoiceX)
	ON_NOTIFY(NM_DBLCLK, IDC_CHOICE, OnDblclkChoice)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CChoiceUser::OnInitDialog() 
{
	CChoiceX::OnInitDialog ();

#ifdef BLUE_COL
	COLORREF BkColor (RGB (100, 180, 255));
	COLORREF DynColor = m_NewAngTyp.DynColorBlue;
	COLORREF RolloverColor = RGB (204, 204, 255);
#else
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
#endif

	return TRUE;
}


void CChoiceUser::DestroyList() 
{
	for (std::vector<CUserList *>::iterator pabl = UserList.begin (); pabl != UserList.end (); ++pabl)
	{
		CUserList *abl = *pabl;
		delete abl;
	}
    UserList.clear ();
}


void CChoiceUser::FillList () 
{
    TCHAR computer [31];
    TCHAR fit_user [31];
	extern short sql_mode;
	short sql_s;
	CString Sql = _T("");

	sql_s = sql_mode;
	sql_mode = 2;
	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);

    int i = 0;

    SetWindowText (_T("Angemeldete Rechner und Benutzer"));
	int p = 0;
    SetCol (_T(""),      p++, 0, LVCFMT_LEFT);
    SetCol (_T("Rechner"),                        p++, 150, LVCFMT_RIGHT);
    SetCol (_T("Angemeldete Benutzer"),           p++, 150, LVCFMT_RIGHT);
	SortRow = 1;


	if (UserList.size () == 0)
	{
		DbClass->sqlout ((char *)computer,  SQLCHAR, sizeof (computer));
		DbClass->sqlout ((char *)fit_user,  SQLCHAR, sizeof (fit_user));
		Sql  =     _T("select computer, fit_user from stcu");
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe f�r den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}
		DbClass->sqlopen (cursor);
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) computer;
			CDbUniCode::DbToUniCode (computer, pos);
			pos = (LPSTR) fit_user;
			CDbUniCode::DbToUniCode (fit_user, pos);
			CUserList *abl = new CUserList (computer, fit_user);
			UserList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
		Load ();
	}
	sql_mode = sql_s;

	for (std::vector<CUserList *>::iterator pabl = UserList.begin (); pabl != UserList.end (); ++pabl)
	{
		CUserList *abl = *pabl;
		_tcscpy (computer, abl->computer);
		_tcscpy (fit_user, abl->fit_user);

		int pos = 1;
        int ret = InsertItem (i, -1);
        ret = SetItemText (computer, i, pos ++);
        ret = SetItemText (fit_user, i, pos ++);
        i ++;
    }


    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort (listView);
	m_List.SetFocus ();
	if (UserList.size () > 0)
	{
		m_List.SetItemState (0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
	}
}


void CChoiceUser::SearchCol (CListCtrl *ListBox, LPTSTR Search, int col)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, col);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceUser::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
	if ((EditText.Find (_T("*")) != -1) || EditText.Find (_T("?")) != -1)
	{
		CChoiceX::SearchMatch (EditText);
		return;
	}

    SearchCol (ListBox, EditText.GetBuffer (), SortRow);
}

int CChoiceUser::GetPtBez (LPTSTR ptitem, LPTSTR ptwert, LPTSTR ptbez)
{
	return 0;
}

int CALLBACK CChoiceUser::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 1)
   {
	   return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort2;
   }
   else if (SortRow == 2)
   {
	   return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
	
   return 0;
}

void CChoiceUser::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CUserList *abl = UserList [i];
		   
		   abl->SetComputer (ListBox->GetItemText (i, 1));
		   abl->SetFitUser  (ListBox->GetItemText (i, 2));
	}


	for (int i = 1; i <= 13; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);

}

void CChoiceUser::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = UserList [idx];
}

CUserList *CChoiceUser::GetSelectedText ()
{
	CUserList *abl = (CUserList *) SelectedRow;
	return abl;
}


void CChoiceUser::SetDefault ()
{
	CChoiceX::SetDefault ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    ListBox->SetColumnWidth (0, 0);
    ListBox->SetColumnWidth (1, 150);
    ListBox->SetColumnWidth (2, 150);
}

void CChoiceUser::OnF5 ()
{
	OnCancel ();
}


void CChoiceUser::OnFilter ()
{
}


