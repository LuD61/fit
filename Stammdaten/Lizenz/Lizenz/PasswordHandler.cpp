#include "StdAfx.h"
#include "PasswordHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CPasswordHandler *CPasswordHandler::Instance = NULL;

CPasswordHandler::CPasswordHandler(void)
{
	PasswordList.Add (new CPasswordItem (_T("SERVICE"), _T("lt0pfit")));
	PasswordList.Add (new CPasswordItem (_T("WILLE"),   _T("210989")));
	PasswordList.Add (new CPasswordItem (_T("PADDY"),   _T("210989")));
}

CPasswordHandler::~CPasswordHandler(void)
{
	PasswordList.DestroyElements ();
	PasswordList.Destroy ();
}

CPasswordHandler *CPasswordHandler::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CPasswordHandler ();
	}
	return Instance;
}


void CPasswordHandler::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

BOOL CPasswordHandler::TestUser (CString &User, CString& Password)
{
	BOOL ret = FALSE;
	CPasswordItem **it;
	CPasswordItem *p;
	CString *pUser;

	User.Trim ().MakeUpper ();
	PasswordList.Start ();
	while ((it = PasswordList.GetNext ()) != NULL)
	{
		p = *it;
		if (p != NULL)
		{
			pUser = p->GetUser ();
			pUser->MakeUpper ();
			if (User == *pUser)
			{
				if (Password.Trim () == *(p->GetPassword ()))
				{
					ret = TRUE;
					break;
				}
			}
		}
	}
	return ret;
}

