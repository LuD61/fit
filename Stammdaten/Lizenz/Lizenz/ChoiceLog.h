#pragma once
#include "choicex.h"
#include "LogList.h"
#include "ColorButton.h"
#include <vector>

#define IDC_TAKE 6006                      

class CChoiceLog :
	public CChoiceX
{
public:
	CChoiceLog(CWnd* pParent = NULL);
	~CChoiceLog();

    protected :
	    static DLGITEMTEMPLATE Take;

        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
		int ptcursor;
		TCHAR ptitem [19];
		TCHAR ptwert [4];
		TCHAR ptbez [37];
		TCHAR ptbezk [17];
		short KunFil;
		BOOL FromFilter;

		DECLARE_MESSAGE_MAP()
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
		virtual BOOL OnInitDialog();

      
    public :
		CString Where;
	    std::vector<CLogList *> LogList;
	    std::vector<CLogList *> SelectList;
        virtual void FillList (void);
	    void SearchCol (CListCtrl *, LPTSTR, int);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
//	    virtual void SaveSelection (CListCtrl *);
        int GetPtBez (LPTSTR, LPTSTR, LPSTR);
		void DestroyList ();
		virtual void SetDefault ();
	    virtual void OnFilter ();
		virtual void OnF5 ();
		CLogList *GetSelectedText ();
};
