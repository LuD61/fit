#pragma once
#include "ColorButton.h"
#include "DataCollection.h"

class CButtonContainer : public CControlContainer
{
private:
	CDataCollection<CColorButton *> Container;

public:
	CButtonContainer(void);
public:
	~CButtonContainer(void);
	void Add (CColorButton *Button);
	virtual void ElementSetFocus (CWnd *cWnd);

};
