#include "StdAfx.h"
#include "UserList.h"

CUserList::CUserList(void)
{
	memset (computer, 0, sizeof (computer));
	memset (fit_user, 0, sizeof (fit_user));
}

CUserList::CUserList(LPTSTR computer, LPTSTR fit_user)
{
	memset (this->computer, 0, sizeof (this->computer));
	memset (this->fit_user, 0, sizeof (this->fit_user));
	_tcsncpy (this->computer, computer, sizeof (this->computer) - 1);
	_tcsncpy (this->fit_user, fit_user, sizeof (this->fit_user) - 1);
}


CUserList::~CUserList(void)
{
}


void CUserList::SetComputer (LPTSTR computer)
{
	memset (this->computer, 0, sizeof (this->computer));
	_tcsncpy (this->computer, computer, sizeof (this->computer) - 1);
}

void CUserList::SetComputer (CString& computer)
{
	memset (this->computer, 0, sizeof (this->computer));
	_tcsncpy (this->computer, computer.GetBuffer (), sizeof (this->computer) - 1);
}

void CUserList::SetFitUser (LPTSTR fit_user)
{
	memset (this->fit_user, 0, sizeof (this->fit_user));
	_tcsncpy (this->fit_user, fit_user, sizeof (this->fit_user) - 1);
}

void CUserList::SetFitUser (CString& fit_user)
{
	memset (this->fit_user, 0, sizeof (this->fit_user));
	_tcsncpy (this->fit_user, fit_user.GetBuffer (), sizeof (this->fit_user) - 1);
}
