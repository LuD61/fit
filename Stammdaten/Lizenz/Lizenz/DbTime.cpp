/*
 * Copyright (c) 2004 Wilhelm Roth, 
 * Company  SETEC-GMBH Inc. All  Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * -Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright
 *  notice, this list of conditions and the following disclaimer in
 *  the documentation and/or other materials provided with the distribution.
 * 
 * Neither the name of SETEC-GMBH, Inc. or the names of contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING
 * ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL NOT
 * BE LIABLE FOR ANY DAMAGES OR LIABILITIES SUFFERED BY LICENSEE AS A RESULT
 * OF OR RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST
 * REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL,
 * INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY
 * OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE SOFTWARE, EVEN
 * IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or intended for
 * use in the design, construction, operation or maintenance of any nuclear
 * facility.
 */
#include "stdafx.h"
#include "DbTime.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


DayTime::DayTime (char *t)
{
	     Create (t);
}

DayTime::DayTime (CString *t)
{
	     Create (t);
}

DayTime *DayTime::Create (char *t)
{
	     CString txt (t);
	     Create (&txt);
		 return this;
}

DayTime *DayTime::Create (CString *t)
{
	     char hour[] = {"00"};
	     char min[]  = {"00"};
	     char sec[]  = {"00"};
         Time = *t;
		 int len = t->GetLength ();
		 if (len >=2)
		 {
			 memcpy (hour, Time.Mid (0, 2), 2);
		 }
		 if (len >=5)
		 {
			 memcpy (min, Time.Mid (3, 2), 2);
		 }
		 if (len >=8)
		 {
			 memcpy (sec, Time.Mid (6, 2), 2);
		 }
/*
		 lTime = atoi (hour) * 24 * 3600 + 
			     atoi (min) * 3600 +
				 atoi (sec);
*/
		 lTime = atoi (hour) * 3600 + 
			     atoi (min)  * 60 +
				 atoi (sec);
		 return this;
}

long DayTime::GetLongTime ()
{
	     return lTime;
}

CString *DayTime::GetTextTime ()
{
	     return &Time;
}

DbTime::DbTime ()
{
		 time_t timer;
		 struct tm *ltime;

		 time (&timer);
		 ltime = localtime (&timer);
		 ds.day = ltime->tm_mday;
		 ds.month = ltime->tm_mon + 1;
		 ds.year = ltime->tm_year + 1900;
	     st.wYear  = ds.year;
	     st.wMonth = ds.month;
	     st.wDay   = ds.day;
	     st.wHour  = 0;
	     st.wMinute = 0;
		 st.wSecond = 0;
		 st.wMilliseconds = 0;
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
	     SystemTimeToFileTime (&st, &ft);
	     FileTimeToSystemTime (&ft, &st);
         memcpy (&ul, &ft, sizeof (ft));
/*
		 ldat = (time_t) (ul.QuadPart / (1000 * 3600 * 24));
		 ldat /= 10000;
*/
		 ldat = timer;
}

DbTime::DbTime (DATE_STRUCT *ds)
{
		 struct tm ltime;

	     memcpy (&this->ds, ds, sizeof (ds));  
	     st.wYear  = ds->year;
	     st.wMonth = ds->month;
	     st.wDay   = ds->day;
	     st.wHour  = 0;
	     st.wMinute = 0;
		 st.wSecond = 0;
		 st.wMilliseconds = 0;
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
	     SystemTimeToFileTime (&st, &ft);
	     FileTimeToSystemTime (&ft, &st);
         memcpy (&ul, &ft, sizeof (ft));
/*
		 ldat = (time_t) (ul.QuadPart / (1000 * 3600 * 24));
		 ldat /= 10000;
*/
		 memset (&ltime, 0, sizeof (ltime));
		 ltime.tm_mday = ds->day;
		 ltime.tm_mon  = ds->month - 1;
		 ltime.tm_year = ds->year - 1900;
		 ldat = mktime (&ltime);
		 ldat /= (3600 * 24);
}

DbTime::DbTime (long ldate)
{
	SetTime ((time_t) ldate);
}

BOOL DbTime::IsNull ()
{
	if (ds.year == 0 || ds.month == 0 || ds.day == 0)
	{
		return TRUE;
	}
	if (ds.year < 1900)
	{
		return TRUE;
	}
	if (ds.year == 1900 && ds.month == 1 && ds.day == 1)
	{
		return TRUE;
	}
	return FALSE;
}


time_t DbTime::GetTime ()
{
	     return ldat;
}

int DbTime::GetWeekDay ()
{
	     return st.wDayOfWeek;
}

CString* DbTime::GetStringTime ()
{
	     return &tDate;
}

void DbTime::SetDateStruct ()
{
	     ds.year  = st.wYear;
	     ds.month = st.wMonth;
	     ds.day   = st.wDay;
}

DATE_STRUCT *DbTime::GetDateStruct (DATE_STRUCT *d)
{
	     memcpy (d, &ds, sizeof (ds));
         return d;
}

void DbTime::SetTime (time_t t)
{
		 struct tm *ltime;
	     ldat = t;
		 ltime = localtime (&ldat);
		 ds.day = ltime->tm_mday;
		 ds.month = ltime->tm_mon + 1;
		 ds.year = ltime->tm_year + 1900;

	     st.wYear  = ds.year;
	     st.wMonth = ds.month;
	     st.wDay   = ds.day;
	     st.wHour  = 0;
	     st.wMinute = 0;
		 st.wSecond = 0;
		 st.wMilliseconds = 0;
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
	     SystemTimeToFileTime (&st, &ft);
	     FileTimeToSystemTime (&ft, &st);
         memcpy (&ul, &ft, sizeof (ft));
}

#ifdef WIN32
DbTime::operator time_t () const
{
		 return ldat;
}

DbTime::operator DATE_STRUCT () const
{
		 return ds;
}

BOOL DbTime::operator== (long time)
{
		 return time == ldat;
}
DbTime& DbTime::operator+ (int d)
{
	     time_t t1 = GetTime ();
         t1 += d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return *this;
}

DbTime& DbTime::operator- (int d)
{
	     time_t t1 = GetTime ();
         t1 -= d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return *this;
}

BOOL DbTime::operator== (DbTime&dbt)
{
		return Equals (dbt);
}

BOOL DbTime::operator< (DbTime&dbt)
{
		return Less (dbt);
}

BOOL DbTime::operator> (DbTime&dbt)
{
		return Greater (dbt);
}

BOOL DbTime::operator<= (DbTime&dbt)
{
		return LessEqual (dbt);
}

BOOL DbTime::operator>= (DbTime&dbt)
{
		return GreaterEqual (dbt);
}


#endif

DbTime* DbTime::Add (int d)
{
	     time_t t1 = GetTime ();
         t1 += d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return this;
}

DbTime* DbTime::Sub (int d)
{
	     time_t t1 = GetTime ();
         t1 -= d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return this;
}

BOOL DbTime::Equals (DbTime& dbt)
{
	if (GetTime () == dbt.GetTime ())
	{
		return TRUE;
	}
	return FALSE;
}

BOOL DbTime::Less (DbTime& dbt)
{
	if (GetTime () < dbt.GetTime ())
	{
		return TRUE;
	}
	return FALSE;
}

BOOL DbTime::Greater (DbTime& dbt)
{
	if (GetTime () > dbt.GetTime ())
	{
		return TRUE;
	}
	return FALSE;
}

BOOL DbTime::LessEqual (DbTime& dbt)
{
	if (GetTime () <= dbt.GetTime ())
	{
		return TRUE;
	}
	return FALSE;
}

BOOL DbTime::GreaterEqual (DbTime& dbt)
{
	if (GetTime () >= dbt.GetTime ())
	{
		return TRUE;
	}
	return FALSE;
}
