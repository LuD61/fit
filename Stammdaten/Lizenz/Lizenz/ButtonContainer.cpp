#include "StdAfx.h"
#include "ButtonContainer.h"

CButtonContainer::CButtonContainer(void)
{
}

CButtonContainer::~CButtonContainer(void)
{
}

void CButtonContainer::Add (CColorButton * Button)
{
	Container.Add (Button);
}

void CButtonContainer::ElementSetFocus (CWnd *cWnd)
{
	CColorButton **it;
	CColorButton *c;

	Container.Start ();
	while ((it = Container.GetNext ()) != NULL)
	{
		c = *it;
		if (c != cWnd)
		{
			c->SetActive (FALSE);
		}
	}
}
