#pragma once
#include "afxwin.h"
#include "QuikInfo.h"

class CStaticButton :
	public CStatic
//	public CButton
{
	DECLARE_DYNCREATE(CStaticButton)

protected:
	DWORD Style;
	DECLARE_MESSAGE_MAP()
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
//	virtual BOOL PreTranslateMessage(MSG* pMsg ); 
public:
	enum
	{
		Center = 0,
		Left = 1,
		Right = 2,
	};
	enum
	{
		Standard = 0,
		Underline = 1,
		Bold = 2,
		UnderlineBold = 3,
	};

	enum
	{
		Solide = 0,
		Dot = 1,
	};

	int Orientation;
	int TextStyle;
	int BorderStyle;
	BOOL DynamicColor;
	static COLORREF DynColorGray;
	static COLORREF DynColorBlue;
	COLORREF DynColor;
//	CToolTipCtrl Tooltip;
//	CStatic Tooltip;
	CQuikInfo Tooltip;
	COLORREF TextColor;
	HCURSOR Hand;
	HCURSOR Arrow;
    BOOL ButtonCursor;
	CBitmap Bitmap;
	CBitmap Mask;
	UINT nID;
	COLORREF BkColor;

	BOOL ColorSet;
	BOOL NoUpdate;
	CFont Font;
	BOOL FontSet;
	CStaticButton(void);
	~CStaticButton (void);
    virtual BOOL Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect,
                        CWnd* pParentWnd,   UINT nID = 0xffff);
	void SetToolTip (LPTSTR Text);
	void LoadBitmap (UINT ID);
	void LoadMask (UINT ID);
	void SetBkColor (COLORREF color);
	void Draw (CDC&);
    void SetButtonCursor (BOOL b);
    BOOL InClient (CPoint p);
    void SetButtonCursor (CPoint p);
	void SetFont ();
	virtual void SetFont (CFont *f, BOOL Redrae=TRUE);
    afx_msg void OnMouseMove(UINT nFlags,  CPoint p);
    afx_msg void OnLButtonDown(UINT nFlags,  CPoint p);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest,  UINT message );
	afx_msg void OnKeyDown(UINT nChar,  UINT nRepCnt,  UINT nFlags);
	afx_msg void OnSetFocus(CWnd *cWnd);
	afx_msg void OnKillFocus(CWnd *cWnd);
	void FillRectParts (CDC &cDC, COLORREF color, CRect rect);
};
