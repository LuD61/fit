// LicenceDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Lizenz.h"
#include "LicenceDialog.h"

#define STC CStc::GetInstance ()

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CLicenceDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CLicenceDialog, CDialog)

CLicenceDialog::CLicenceDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLicenceDialog::IDD, pParent)
{
//	m_DlgColor = GetSysColor (COLOR_3DFACE);
//	m_DlgColor = RGB (255, 255, 204);
	m_DlgColor = RGB (223, 227, 236);
	m_DlgBrush = NULL;
	licence_count = 0;
	kun_name = _T("SETEC");
	text = _T("Telefon : 07423-81095-0");
	DbClass.opendbase (_T("bws"));
	Choice = NULL;
	ChoiceLog = NULL;
}

CLicenceDialog::~CLicenceDialog()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	STC->DestroyInstance ();
}

void CLicenceDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAPTION, m_Caption);
	DDX_Control(pDX, IDOK, m_Ok);
	DDX_Control(pDX, IDCANCEL, m_Cancel);
	DDX_Control(pDX, IDC_CHOICE_USER, m_ChoiceUser);
	DDX_Control(pDX, IDC_CHOICE_LOG, m_ChoiceLog);
	DDX_Control(pDX, IDC_LICENCECOUNT, m_LicenceCount);
	DDX_Control(pDX, IDC_FREE_LICENCE, m_FreeLicence);
	DDX_Control(pDX, IDC_SET_LICENCE, m_SetLicence);
	DDX_Control(pDX, IDC_SPIN1, m_SpinLicenceCount);
	DDX_Control(pDX, IDC_LLICENCE_COUNT, m_LLicenceCount);
	DDX_Control(pDX, IDC_LKUN_NAME, m_LKunName);
	DDX_Control(pDX, IDC_KUN_NAME, m_KunName);
	DDX_Control(pDX, IDC_LTEXT, m_LText);
	DDX_Control(pDX, IDC_TEXT, m_Text);
	DDX_Control(pDX, IDC_TEST_DATE, m_TestDate);
	DDX_Control(pDX, IDC_LTEST_DATE, m_LTestDate);
}


BEGIN_MESSAGE_MAP(CLicenceDialog, CDialog)
	ON_WM_CTLCOLOR ()
	ON_COMMAND (IDOK, OnOK)
	ON_COMMAND (IDCANCEL, OnCancel)
	ON_COMMAND (IDC_CHOICE_USER, OnUserChoice)
	ON_COMMAND (IDC_CHOICE_LOG, OnLogChoice)
	ON_COMMAND (IDC_FREE_LICENCE, OnFreeLicence)
	ON_COMMAND (IDC_SET_LICENCE, OnSetLicence)
END_MESSAGE_MAP()


BOOL CLicenceDialog::OnInitDialog()
{
	BOOL ret;
	ret = CDialog::OnInitDialog ();

	HICON m_Icon = LoadIcon (AfxGetInstanceHandle (),MAKEINTRESOURCE (IDR_MAINFRAME));
	SetIcon (m_Icon, FALSE);

	m_LLicenceCount.SetWindowText (_T("Anzahl Lizenzen"));
	m_LTestDate.EnableWindow ();
	m_TestDate.EnableWindow ();

	SetWindowText (_T(" Eingabe der Lizenzdaten"));
	CDialog::OnInitDialog();
	m_Caption.SetParts (60);
	m_Caption.SetWindowText (_T(" Eingabe der Lizenzdaten"));
	m_Caption.SetBoderStyle (m_Caption.Solide);
	m_Caption.SetOrientation (m_Caption.Left);
	m_Caption.SetDynamicColor (TRUE);
	m_Caption.SetTextColor (RGB (255, 255, 153));
	m_Caption.SetBkColor (RGB (192, 192, 192));

//	COLORREF BkColor (CColorButton::DynColorGray);
//	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF BkColor (RGB (0, 0, 255));
//	COLORREF DynColor = RGB (0, 0, 255);
	COLORREF RolloverColor = CColorButton::DynColorBlue;
	COLORREF DynColor = CColorButton::DynColorBlue;
//	COLORREF RolloverColor = RGB (192, 192, 192);

	m_Ok.SetWindowText (_T("Benutzer setzen"));
	m_Ok.nID = IDOK;
//	m_Ok.SetToolTip (_T("Bearbeiten"));
	m_Ok.Orientation = m_Ok.Left;
	m_Ok.TextStyle = m_Ok.Standard;
	m_Ok.BorderStyle = m_Ok.Solide;
	m_Ok.DynamicColor = TRUE;
	m_Ok.TextColor =RGB (0, 0, 0);
	m_Ok.SetBkColor (BkColor);
	m_Ok.DynColor = DynColor;
	m_Ok.RolloverColor = RolloverColor;
	m_Ok.LoadBitmap (IDB_OK);
	m_Ok.LoadMask (IDB_OKMASK);
	m_Ok.SetToolTip (_T("Anzahl Benutzerlizenzen\nsetzen"));
	m_Ok.Tooltip.WindowOrientation = CQuikInfo::Left;
	m_Ok.Container = &Container;
	Container.Add (&m_Ok);

	m_Cancel.SetWindowText (_T("Schie�en"));
	m_Cancel.nID = IDCANCEL;
//	m_Cancel.SetToolTip (_T("Bearbeiten"));
	m_Cancel.Orientation = m_Cancel.Left;
	m_Cancel.TextStyle = m_Cancel.Standard;
	m_Cancel.BorderStyle = m_Cancel.Solide;
	m_Cancel.DynamicColor = TRUE;
	m_Cancel.TextColor =RGB (0, 0, 0);
	m_Cancel.SetBkColor (BkColor);
	m_Cancel.DynColor = DynColor;
	m_Cancel.RolloverColor = RolloverColor;
	m_Cancel.LoadBitmap (IDB_CANCEL);
	m_Cancel.LoadMask (IDB_CANCELMASK);
	m_Cancel.Container = &Container;
	Container.Add (&m_Cancel);

	m_FreeLicence.SetWindowText (_T("Unbegrenzte Lizenz"));
	m_FreeLicence.nID = IDC_FREE_LICENCE;
	m_FreeLicence.Orientation = m_FreeLicence.Left;
//	m_FreeLicence.SetToolTip (_T("Bearbeiten"));
	m_FreeLicence.Orientation = m_FreeLicence.Center;
	m_FreeLicence.TextStyle = m_FreeLicence.Standard;
	m_FreeLicence.BorderStyle = m_FreeLicence.Solide;
	m_FreeLicence.DynamicColor = TRUE;
	m_FreeLicence.TextColor =RGB (0, 0, 0);
	m_FreeLicence.SetBkColor (BkColor);
	m_FreeLicence.DynColor = DynColor;
	m_FreeLicence.RolloverColor = RolloverColor;
	m_FreeLicence.LoadBitmap (IDB_FREE_LICENCE);
	m_FreeLicence.LoadMask (IDB_FREE_LICENCE_MASK);
	m_FreeLicence.SetToolTip (_T("Lizenz zu unbegrenzten\nBenutzung freischalten"));
	m_FreeLicence.Tooltip.WindowOrientation = CQuikInfo::Left;
	m_FreeLicence.Container = &Container;
	Container.Add (&m_FreeLicence);

    m_SetLicence.SetWindowText (_T("Testlizenz"));
	m_SetLicence.nID = IDC_SET_LICENCE;
	m_SetLicence.Orientation = m_SetLicence.Left;
//	m_SetLicence.SetToolTip (_T("Bearbeiten"));
	m_SetLicence.TextStyle = m_SetLicence.Standard;
	m_SetLicence.BorderStyle = m_SetLicence.Solide;
	m_SetLicence.DynamicColor = TRUE;
	m_SetLicence.TextColor =RGB (0, 0, 0);
	m_SetLicence.SetBkColor (BkColor);
	m_SetLicence.DynColor = DynColor;
	m_SetLicence.RolloverColor = RolloverColor;
	m_SetLicence.LoadBitmap (IDB_LICENCE_DAT);
	m_SetLicence.LoadMask (IDB_LICENCE_DAT_MASK);
	m_SetLicence.SetToolTip (_T("Testlizenz f�r 30 Tage\nab Datum f�r Testlizenz setzen"));
	m_SetLicence.Tooltip.WindowOrientation = CQuikInfo::Left;
	m_SetLicence.Container = &Container;
	Container.Add (&m_SetLicence);

	m_ChoiceUser.SetWindowText (_T("Benutzerliste"));
	m_ChoiceUser.nID = IDC_CHOICE_USER;
	m_ChoiceUser.Orientation = m_ChoiceUser.Left;
//	m_ChoiceUser.SetToolTip (_T("Bearbeiten"));
	m_ChoiceUser.TextStyle = m_ChoiceUser.Standard;
	m_ChoiceUser.BorderStyle = m_ChoiceUser.Solide;
	m_ChoiceUser.DynamicColor = TRUE;
	m_ChoiceUser.TextColor =RGB (0, 0, 0);
	m_ChoiceUser.SetBkColor (BkColor);
	m_ChoiceUser.DynColor = DynColor;
	m_ChoiceUser.RolloverColor = RolloverColor;
	m_ChoiceUser.LoadBitmap (IDB_USERLIST);
	m_ChoiceUser.LoadMask (IDB_USERLISTMASK);
	m_ChoiceUser.SetToolTip (_T("Aktuell angemeldete Benutze anzeigen"));
	m_ChoiceUser.Tooltip.WindowOrientation = CQuikInfo::Left;
	m_ChoiceUser.Container = &Container;
	Container.Add (&m_ChoiceUser);

	m_ChoiceLog.SetWindowText (_T("Protokoll"));
	m_ChoiceLog.nID = IDC_CHOICE_LOG;
	m_ChoiceLog.Orientation = m_ChoiceLog.Left;
//	m_ChoiceLog.SetToolTip (_T("Bearbeiten"));
	m_ChoiceLog.TextStyle = m_ChoiceLog.Standard;
	m_ChoiceLog.BorderStyle = m_ChoiceLog.Solide;
	m_ChoiceLog.DynamicColor = TRUE;
	m_ChoiceLog.TextColor =RGB (0, 0, 0);
	m_ChoiceLog.SetBkColor (BkColor);
	m_ChoiceLog.DynColor = DynColor;
	m_ChoiceLog.RolloverColor = RolloverColor;
	m_ChoiceLog.LoadBitmap (IDB_USERLIST);
	m_ChoiceLog.LoadMask (IDB_USERLISTMASK);
	m_ChoiceLog.SetToolTip (_T("Protokoll der Lizen�berschreitungen anzeigen"));
	m_ChoiceLog.Tooltip.WindowOrientation = CQuikInfo::Left;
	m_ChoiceLog.Container = &Container;
	Container.Add (&m_ChoiceLog);

	m_SpinLicenceCount.SetRange32 (0, 1000);
	m_SpinLicenceCount.SetBuddy (&m_LicenceCount);

	Font.CreatePointFont (95, _T("Dlg"));

    Form.Add (new CFormField (&m_LicenceCount, EDIT, (short *) &licence_count, VSHORT));
    Form.Add (new CFormField (&m_KunName, EDIT, (CString *) &kun_name, VSTRING));
    Form.Add (new CFormField (&m_Text, EDIT,    (CString *) &text, VSTRING));
    Form.Add (new CFormField (&m_TestDate, DATETIMEPICKER, (CString *) &cDate, VSTRING));

	STC->GetLicenceData ();
	licence_count = STC->GetLicenceCount ();
	kun_name = STC->GetKunName ();
	text = STC->GetText ();

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	StaticFont.CreateFontIndirect (&l);
	l.lfHeight = 25;
	CaptionFont.CreateFontIndirect (&l);
	m_Caption.SetFont (&CaptionFont);

	m_Ok.SetFont (&Font);
	m_Ok.Tooltip.SetFont (&Font);
	m_Ok.Tooltip.SetSize ();
	m_Cancel.SetFont (&Font);
//	m_Cancel.Tooltip.SetFont (&Font);
//	m_Cancel.Tooltip.SetSize ();
	m_ChoiceUser.SetFont (&Font);
	m_ChoiceUser.Tooltip.SetFont (&Font);
	m_ChoiceUser.Tooltip.SetSize ();
	m_ChoiceLog.SetFont (&Font);
	m_ChoiceLog.Tooltip.SetFont (&Font);
	m_ChoiceLog.Tooltip.SetSize ();
	m_LLicenceCount.SetFont (&StaticFont);
	m_LicenceCount.SetFont (&Font);
	m_FreeLicence.SetFont (&Font);
	m_FreeLicence.Tooltip.SetFont (&Font);
	m_FreeLicence.Tooltip.SetSize ();
	m_SetLicence.SetFont (&Font);
	m_SetLicence.Tooltip.SetFont (&Font);
	m_SetLicence.Tooltip.SetSize ();
	m_LKunName.SetFont (&StaticFont);
	m_KunName.SetFont (&Font);
	m_LText.SetFont (&StaticFont);
	m_Text.SetFont (&Font);
	m_LTestDate.SetFont (&StaticFont);
	m_TestDate.SetFont (&Font);

	m_Text.LimitText (80);

	Form.Show ();

	return TRUE;
}

// CLicenceDialog-Meldungshandler


HBRUSH CLicenceDialog::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	CRect rect;
	if (nCtlColor == CTLCOLOR_DLG && m_DlgColor != NULL)
	{
		    if (m_DlgBrush == NULL)
			{
//				m_DlgBrush = CreateSolidBrush (m_DlgColor);

                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC)
	{
		    if (m_DlgBrush == NULL)
			{
				m_DlgBrush = CreateSolidBrush (m_DlgColor);
			}
			pDC->SetTextColor (RGB (92, 92, 92));
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CLicenceDialog::OnOK ()
{
	CString msg;

	Form.Get ();
	STC->SetKunName (kun_name.GetBuffer ());
	STC->SetText (text.GetBuffer ());
	STC->SetTryCount (0);
	STC->SetLicenceCount (licence_count);
	msg.Format (_T("Lizenz wurde auf %d Benutzer gesetzt"), licence_count);
	MessageBox (msg, _T(""), MB_OK | MB_ICONINFORMATION);
	m_Ok.SetFocus ();
//	CDialog::OnOK ();
}

void CLicenceDialog::OnUserChoice ()
{
	if (Choice == NULL)
	{
		Choice = new CChoiceUser (this);
	    Choice->IsModal = TRUE;
		Choice->DlgBkColor = RGB (223, 227, 236);
		Choice->HideCancel = TRUE;
		Choice->CreateDlg ();
	}

    Choice->SetDbClass (&DbClass);
	Choice->DoModal();
	delete Choice;
	Choice = NULL;
	m_ChoiceUser.SetFocus ();
}

void CLicenceDialog::OnLogChoice ()
{
	if (ChoiceLog == NULL)
	{
		ChoiceLog = new CChoiceLog (this);
	    ChoiceLog->IsModal = TRUE;
		ChoiceLog->DlgBkColor = RGB (223, 227, 236);
		ChoiceLog->HideCancel = TRUE;
		ChoiceLog->SetDlgSize (500, 226);
		ChoiceLog->CreateDlg ();
	}

    ChoiceLog->SetDbClass (&DbClass);
	ChoiceLog->DoModal();
	delete ChoiceLog;
	ChoiceLog = NULL;
	m_ChoiceLog.SetFocus ();
}


void CLicenceDialog::OnFreeLicence()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CString lDate = _T("05.07.1994");
	STC->SetLicence (&lDate);
	MessageBox (_T("Lizenz wurde auf unbegrenzt freigeschaltet"), _T(""), MB_OK | MB_ICONINFORMATION);
	m_FreeLicence.SetFocus ();
}

void CLicenceDialog::OnSetLicence()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CString msg;
	Form.Get ();
	STC->SetLicence (&cDate);
	msg.Format (_T("Testlizenz wurde auf 30 Tage ab dem %s gesetzt"), cDate.GetBuffer ());
	MessageBox (msg, _T(""), MB_OK | MB_ICONINFORMATION);
	m_SetLicence.SetFocus ();

}
