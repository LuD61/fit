#include "StdAfx.h"
#include "ChoiceLog.h"
#include "DbUniCode.h"
#include "StrFuncs.h"
#include "Token.h"
#include "DynDialog.h"
#include "DbQuery.h"
#include "DbTime.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#define BLUE_COL 1

int CChoiceLog::Sort1 = -1;
int CChoiceLog::Sort2 = -1;
int CChoiceLog::Sort3 = -1;
int CChoiceLog::Sort4 = -1;

CChoiceLog::CChoiceLog(CWnd* pParent) 
        : CChoiceX(pParent)
{
	ptcursor = -1;
	Where = "";
	Bean.ArchiveName = _T("LogList.prp");
	FromFilter = FALSE;
}

CChoiceLog::~CChoiceLog() 
{
	DestroyList ();
	if (ptcursor != -1) DbClass->sqlclose (ptcursor);
	ptcursor = -1;
}

void CChoiceLog::DoDataExchange(CDataExchange* pDX)
{
	CChoiceX::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChoice)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChoiceLog, CChoiceX)
	//{{AFX_MSG_MAP(CChoiceX)
	ON_NOTIFY(NM_DBLCLK, IDC_CHOICE, OnDblclkChoice)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CChoiceLog::OnInitDialog() 
{
	CChoiceX::OnInitDialog ();

#ifdef BLUE_COL
	COLORREF BkColor (RGB (100, 180, 255));
	COLORREF DynColor = m_NewAngTyp.DynColorBlue;
	COLORREF RolloverColor = RGB (204, 204, 255);
#else
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
#endif

	return TRUE;
}


void CChoiceLog::DestroyList() 
{
	for (std::vector<CLogList *>::iterator pabl = LogList.begin (); pabl != LogList.end (); ++pabl)
	{
		CLogList *abl = *pabl;
		delete abl;
	}
    LogList.clear ();
}


void CChoiceLog::FillList () 
{
    DATE_STRUCT date;
    TCHAR time [10];
    TCHAR text [256];
	CString CDate;
	CString Text;
	extern short sql_mode;
	short sql_s;
	CString Sql = _T("");

	sql_s = sql_mode;
	sql_mode = 2;
	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);

    int i = 0;

    SetWindowText (_T("Anmeldeprotokoll"));
	int p = 0;
    SetCol (_T(""),      p++, 0, LVCFMT_LEFT);
    SetCol (_T("Datum"),               p++, 100);
    SetCol (_T("Zeit"),                p++, 100);
    SetCol (_T("Text"),                p++, 500);
	SortRow = 1;


	if (LogList.size () == 0)
	{
		DbClass->sqlout ((DATE_STRUCT *)&date, SQLDATE, 0);
		DbClass->sqlout ((char *)       time,  SQLCHAR, sizeof (time));
		DbClass->sqlout ((char *)       text,  SQLCHAR, sizeof (text));
		Sql  =     _T("select dat, time, text from stcp order by dat desc, time desc");
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe f�r den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}
		DbClass->sqlopen (cursor);
		while (DbClass->sqlfetch (cursor) == 0)
		{
			DB_CLASS::FromDbDate (CDate, &date);
			char *pos = (LPSTR) time;
			CDbUniCode::DbToUniCode (time, pos);
			pos = (LPSTR) text;
			CDbUniCode::DbToUniCode (text, pos);
			Text = text;
			Text.TrimRight ();
			CLogList *abl = new CLogList (CDate.GetBuffer (), time, Text.GetBuffer ());
			LogList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
		Load ();
	}
	sql_mode = sql_s;

	for (std::vector<CLogList *>::iterator pabl = LogList.begin (); pabl != LogList.end (); ++pabl)
	{
		CLogList *abl = *pabl;
		CDate = abl->Date;
		_tcscpy (time, abl->Time);
		_tcscpy (text, abl->Text);

		int pos = 1;
        int ret = InsertItem (i, -1);
        ret = SetItemText (CDate.GetBuffer (), i, pos ++);
        ret = SetItemText (time, i, pos ++);
        ret = SetItemText (text, i, pos ++);
        i ++;
    }


    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
//    Sort (listView);
	m_List.SetFocus ();
	if (LogList.size () > 0)
	{
		m_List.SetItemState (0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
	}
}


void CChoiceLog::SearchCol (CListCtrl *ListBox, LPTSTR Search, int col)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, col);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLog::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
	if ((EditText.Find (_T("*")) != -1) || EditText.Find (_T("?")) != -1)
	{
		CChoiceX::SearchMatch (EditText);
		return;
	}

    SearchCol (ListBox, EditText.GetBuffer (), SortRow);
}

int CChoiceLog::GetPtBez (LPTSTR ptitem, LPTSTR ptwert, LPTSTR ptbez)
{
	return 0;
}

int CALLBACK CChoiceLog::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);
   DATE_STRUCT dat1;
   DATE_STRUCT dat2;
   int ret = 0;

   if (SortRow == 1)
   {
	   DB_CLASS::ToDbDate (strItem1, &dat1); 
	   DB_CLASS::ToDbDate (strItem2, &dat2); 
	   DbTime t1 (&dat1);
	   DbTime t2 (&dat2);
	   if (t2.GetTime () > t1.GetTime ())
	   {
		   ret = 1;
	   }
	   else if (t2.GetTime () < t1.GetTime ())
	   {
		   ret = -1;
	   }
	   else
	   {
		   ret = 0;
	   }
	   return ret * Sort2;
   }
   else if (SortRow == 2)
   {
	   return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	   return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}

void CChoiceLog::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CLogList *abl = LogList [i];
		   
		   abl->SetDate (ListBox->GetItemText (i, 1));
		   abl->SetTime (ListBox->GetItemText (i, 2));
		   abl->SetText (ListBox->GetItemText (i, 2));
	}


	for (int i = 1; i <= 13; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);

}

void CChoiceLog::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = LogList [idx];
}

CLogList *CChoiceLog::GetSelectedText ()
{
	CLogList *abl = (CLogList *) SelectedRow;
	return abl;
}


void CChoiceLog::SetDefault ()
{
	CChoiceX::SetDefault ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    ListBox->SetColumnWidth (0, 0);
    ListBox->SetColumnWidth (1, 100);
    ListBox->SetColumnWidth (2, 100);
    ListBox->SetColumnWidth (3, 500);
}

void CChoiceLog::OnF5 ()
{
	OnCancel ();
}


void CChoiceLog::OnFilter ()
{
}


