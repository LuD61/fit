#include "stdafx.h"
#include "bmap.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

HBITMAP BMAP::hMask = NULL;

void BMAP::DestroyBitmap (void)
/**
Aktuelle Bitmap loeschen.
**/
{
       if (bmap)
       {
           GlobalFree (bmap);
           bmap = NULL;
           hglbDlg = NULL;
       }
}


CBitmap *BMAP::ReadBitmap (HDC hdc, char *bname)
/**
Graphic ausgeben.
**/
{
         FILE *bp;
         char far *rptr;
         DWORD bytes;
         unsigned rbytes;
         BITMAPFILEHEADER bmpheader;
         DWORD bmbitsoffs;
         const char far *bmbits;

         bp = fopen (bname, "rb");
         if (bp == (FILE *) 0)
         {
                     return NULL;
         }

         bytes = (DWORD) fread (&bmpheader, 1, sizeof (BITMAPFILEHEADER), bp);
         bmbitsoffs = bmpheader.bfOffBits - sizeof (BITMAPFILEHEADER);
         hglbDlg = GlobalAlloc (GMEM_FIXED, bmpheader.bfSize);
         if (hglbDlg == NULL)
         {
                     fclose (bp);
                     return NULL;
         }
         bmap = (BITMAPINFO FAR*) GlobalLock (hglbDlg);
         if (bmap == NULL)
         {
                     fclose (bp);
                     return NULL;
         }

         rptr = (char far *) bmap;
         rbytes = (unsigned) fread ((char *) rptr, 1, 0x1000, bp);
         while (rbytes == 0x1000)
         {
                 rptr += 0x1000;
                 rbytes = (unsigned) fread ((BYTE *) rptr, 1, 0x1000, bp);
         }

         bmbits = (const char far *) bmap + bmbitsoffs;
         hBitmap = CreateDIBitmap (hdc, (BITMAPINFOHEADER FAR *) bmap,
                                   CBM_INIT, bmbits, bmap, DIB_RGB_COLORS);
         fclose (bp);
         hBitmapOrg = hBitmap;
         cBitmap    = CBitmap::FromHandle (hBitmap);
         cBitmapOrg = CBitmap::FromHandle (hBitmap);
         return cBitmap;
}

void BMAP::DrawBitmap (HDC hdc, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void BMAP::DrawBitmap (CDC *pDC, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        HDC hdc = pDC->m_hDC;
        DrawBitmap (hdc, xStart, yStart);
}

void BMAP::DrawBitmap (HDC hdc, HBITMAP hBitmap, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
//        DWORD dwsize;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}



void BMAP::DrawBitmap (CDC *pDC, HBITMAP hBitmap, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        HDC hdc = pDC->m_hDC;
        DrawBitmap (hdc, hBitmap, xStart, yStart);
}

void BMAP::DrawTransparentBitmap (HDC hdc, HBITMAP hBitmap, HBITMAP hMask, 
								  int xStart, int yStart, COLORREF BkColor)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;
//		HBRUSH hBrush, oldbrush;


        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hMask);
//        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hMask, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
//        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
//        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCAND);

        SelectObject (hdcMem, hBitmap);
//        SetMapMode (hdcMem, GetMapMode (hdc)); 
//        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
//        ptSize.x = bm.bmWidth;
//        ptSize.y = bm.bmHeight;
//        DPtoLP (hdc, &ptSize, 1); 
        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCPAINT);
        DeleteDC (hdcMem); 
}

void BMAP::DrawTransparentBitmap (CDC *pDC, HBITMAP hBitmap, HBITMAP hMask, 
								  int xStart, int yStart, COLORREF BkColor)
{
        HDC hdc = pDC->m_hDC;
        DrawTransparentBitmap (hdc, hBitmap, hMask, xStart, yStart, BkColor);
}


HBITMAP BMAP::PrintTransparentBitmap (HBITMAP hBitmapOrg, COLORREF BkColor)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hdcMask;
        HDC hdc;
        BITMAP bm;
		HBITMAP hBitmapMem;
		HBITMAP hMask;
		HBRUSH hBrush;


//		CDC *cDC = cWnd->GetDC ();
//		hdc = cDC->m_hDC;
		hdc = GetDC (NULL);
		GetObject (hBitmapOrg, sizeof (BITMAP), &bm);

// Speicher f�r transparenrte Bitmap erstellen

        hdcMemory  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemory, hBitmapOrg);

// Speicher f�r Maske erstellen

		hMask = CreateBitmap (bm.bmWidth, bm.bmHeight, 1, 1, NULL);
        hdcMask  = CreateCompatibleDC(hdc);
        SelectObject (hdcMask, hMask);

// Transparente Farbe einstellen 
// geladene Bitmap in monochrome Bitmap umkopieren
// -> alles ausser HG erscheint nun in schwarzer Farbe

        COLORREF    BkgndColor = GetPixel (hdcMemory, 0, 0);
		SetBkColor (hdcMemory, BkgndColor);
		BitBlt (hdcMask, 0, 0, bm.bmWidth, bm.bmHeight, 
				hdcMemory, 0, 0, SRCCOPY);    
// monochrome Bitmapmaske fertig.


		SetTextColor (hdcMemory, RGB (255,255,255));
		SetBkColor (hdcMemory, RGB (0,0,0));
		BitBlt(hdcMemory, 0,0,bm.bmWidth, bm.bmHeight,
                         hdcMask,0,0, SRCAND);
// Orginal Bitmap fertig konvertiert.

// Neue kombinierte Bitmap erstellen

       hdcMemoryZ  = CreateCompatibleDC(hdc);
       hBitmapMem = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);
       SelectObject (hdcMemoryZ, hBitmapMem);
	   hBrush = CreateSolidBrush (BkColor);
       SelectObject (hdcMemoryZ, hBrush);
       BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                NULL, 0, 0, PATCOPY); 

       BitBlt(hdcMemoryZ, 0,0,bm.bmWidth, bm.bmHeight,
                          hdcMask,0,0, SRCAND );

       BitBlt(hdcMemoryZ, 0,0,bm.bmWidth, bm.bmHeight,
                          hdcMemory,0,0, SRCPAINT );

        DeleteDC (hdcMask);        
        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        DeleteObject (hMask);        
        DeleteObject (hBitmapOrg);        
        DeleteObject (hBrush);        
		return hBitmapMem;

}

HBITMAP BMAP::CreateBitmapMask (HBITMAP hBitmapOrg, COLORREF BkColor)
{

        HDC hdcMemory;
        HDC hdcMask;
        HDC hdc;
        BITMAP bm;
		HBITMAP hMask;

		hdc = GetDC (NULL);
		GetObject (hBitmapOrg, sizeof (BITMAP), &bm);

// Speicher f�r transparenrte Bitmap erstellen

        hdcMemory  = CreateCompatibleDC(hdc);

		SelectObject (hdcMemory, hBitmapOrg);
        COLORREF    BkgndColor = GetPixel (hdcMemory, 0, 0);
		if (BkColor != -1)
		{
			BkgndColor = BkColor;
		}

// Speicher f�r Maske erstellen

		hMask = CreateBitmap (bm.bmWidth, bm.bmHeight, 1, 1, NULL);
        hdcMask  = CreateCompatibleDC(NULL);
        SelectObject (hdcMask, hMask);

// Transparente Farbe einstellen 
// geladene Bitmap in monochrome Bitmap umkopieren
// -> alles ausser HG erscheint nun in schwarzer Farbe

		SetBkColor (hdcMemory, BkgndColor);
		BitBlt (hdcMask, 0, 0, bm.bmWidth, bm.bmHeight, 
				hdcMemory, 0, 0, SRCCOPY);    


// monochrome Bitmapmaske fertig.

//		BitBlt(hdcMemory, 0,0,bm.bmWidth, bm.bmHeight,
//                         hdcMask,0,0, SRCINVERT);


       DeleteDC (hdcMask);        
       DeleteDC (hdcMemory);        
       return hMask;        
}

void BMAP::PrintTransparentBitmap (HBITMAP hBitmapOrg, CDC *cDC, int x, int y, COLORREF BkColor)
{

        HDC hdcMemory;
        HDC hdcMask;
        HDC hdc;
        BITMAP bm;
		HBITMAP hMask;

		hdc = cDC->m_hDC;
		GetObject (hBitmapOrg, sizeof (BITMAP), &bm);

// Speicher f�r transparenrte Bitmap erstellen

        hdcMemory  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemory, hBitmapOrg);
        COLORREF    BkgndColor = -1;
        BkgndColor = GetPixel (hdcMemory, 0, 0);
		if (BkColor != -1)
		{
			BkgndColor = BkColor;
		}

// Speicher f�r Maske erstellen

		hMask = CreateBitmap (bm.bmWidth, bm.bmHeight, 1, 1, NULL);
        hdcMask  = CreateCompatibleDC(NULL);
        SelectObject (hdcMask, hMask);

// Transparente Farbe einstellen 
// geladene Bitmap in monochrome Bitmap umkopieren
// -> alles ausser HG erscheint nun in schwarzer Farbe

		SetBkColor (hdcMemory, BkgndColor);
		BitBlt (hdcMask, 0, 0, bm.bmWidth, bm.bmHeight, 
				hdcMemory, 0, 0, SRCCOPY);    

// monochrome Bitmapmaske fertig.

		SetTextColor (hdcMemory, RGB (255,255,255));
		SetBkColor (hdcMemory, RGB (0,0,0));
		BitBlt(hdcMemory, 0,0,bm.bmWidth, bm.bmHeight,
                         hdcMask,0,0, SRCAND);

// Orginal Bitmap fertig konvertiert.

// Neue Bitmap auf Device schreiben

       BitBlt(hdc, x, y, bm.bmWidth, bm.bmHeight,
                         hdcMask,0,0, SRCAND );

       BitBlt(hdc, x,y, bm.bmWidth, bm.bmHeight,
                        hdcMemory,0,0, SRCPAINT );

       DeleteDC (hdcMask);        
       DeleteDC (hdcMemory);        
       DeleteObject (hMask);        
}

void BMAP::PrintTransparentBitmap (HBITMAP hBitmapOrg, HBITMAP hMask, CDC *cDC, int x, int y)
{

        HDC hdcMemory;
        HDC hdcMask;
        HDC hdc;
        BITMAP bm;

		hdc = cDC->m_hDC;
		GetObject (hBitmapOrg, sizeof (BITMAP), &bm);

// Speicher f�r transparenrte Bitmap erstellen

        hdcMemory  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemory, hBitmapOrg);
        COLORREF    BkgndColor = GetPixel (hdcMemory, 0, 0);

// Speicher f�r Maske erstellen

        hdcMask  = CreateCompatibleDC(hdc);
        SelectObject (hdcMask, hMask);

// monochrome Bitmapmaske fertig.

//		BitBlt(hdcMemory, 0,0,bm.bmWidth, bm.bmHeight,
//                         hdcMask,0,0, SRCINVERT);


// Orginal Bitmap fertig konvertiert.

// Neue Bitmap auf Device schreiben

       BitBlt(hdc, x, y, bm.bmWidth, bm.bmHeight,
                         hdcMask,0,0, SRCAND );

       BitBlt(hdc, x,y, bm.bmWidth, bm.bmHeight,
                        hdcMemory,0,0, SRCPAINT );

       DeleteDC (hdcMask);        
       DeleteDC (hdcMemory);        
}


HBITMAP BMAP::PrintBitmapMem (HBITMAP hBitmapOrg, HBITMAP hMask, COLORREF BkColor)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
        HDC hdc;
		HBITMAP hBitmapMem;
		RECT rect;
		HBRUSH hBrush, oldbrush;

 
		hdc = GetDC (NULL);
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        SetViewportOrgEx (hdc, 0, 0, NULL);

		rect.left = 0;
		rect.top = 0;
		rect.right  = bm.bmWidth;
		rect.bottom = bm.bmHeight;
		hBrush = CreateSolidBrush (BkColor);

        hdcMemory  = CreateCompatibleDC(hdc);

        hBitmapMem = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);

        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapMem);
        oldbrush = (HBRUSH) SelectObject (hdcMemoryZ, hBrush);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                NULL, 0, 0, PATCOPY); 

        hbmOld = (HDC) SelectObject (hdcMemory, hMask);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCAND); 

        SelectObject (hdcMemory, hBitmapOrg);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCPAINT); 


        SelectObject (hdcMemory, hbmOld);

		DeleteObject (hBrush);
        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        ReleaseDC (NULL, hdc);

        DeleteObject (hBitmapOrg);
        DeleteObject (hMask);

        return hBitmapMem;
}

void BMAP::BitmapSize (HDC hdc, HBITMAP hBitmap, POINT *ptSize)
/**
Bitmap-Recheck holen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize->x = bm.bmWidth;
        ptSize->y = bm.bmHeight;
        DeleteDC (hdcMem); 
}

void BMAP::BitmapSize (CDC *pDC, HBITMAP hBitmap, POINT *ptSize)
/**
Bitmap-Recheck holen.
**/
{
        HDC hdc = pDC->m_hDC;
        BitmapSize (hdc, hBitmap, ptSize);
}
 

HBITMAP BMAP::LoadBitmap (HINSTANCE hInstance, char *BitmapName, char *MaskName)
{
	return PrintBitmapMem (::LoadBitmap (hInstance, BitmapName),
		                   ::LoadBitmap (hInstance, MaskName),
						   GetSysColor (COLOR_3DFACE));
}

HBITMAP BMAP::LoadBitmap (HINSTANCE hInstance, char *BitmapName, char *MaskName, COLORREF BkColor)
{
	return PrintBitmapMem (::LoadBitmap (hInstance, BitmapName),
		                   ::LoadBitmap (hInstance, MaskName),
						   BkColor);
}


