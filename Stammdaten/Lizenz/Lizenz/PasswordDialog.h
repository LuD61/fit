#pragma once
#include "ColorButton.h"
#include "Label.h"
#include "VLabel.h"
#include "VHLabel.h"
#include "afxwin.h"
#include "FormTab.h"
#include "TextEdit.h"
#include "BkBitmap.h"


// CPasswordDialog-Dialogfeld

class CPasswordDialog : public CDialog
{
	DECLARE_DYNAMIC(CPasswordDialog)

public:
	CPasswordDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CPasswordDialog();

// Dialogfelddaten
	enum { IDD = IDD_PASSWORD_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);

	DECLARE_MESSAGE_MAP()

private:
	int trycount;
	CString User;
	CString Password;
	CString Message;
	BOOL Error;
	COLORREF MessageColor;
	COLORREF ErrorColor;
	COLORREF m_DlgColor;
	HBRUSH m_DlgBrush;
	CBkBitmap BkBitmap;
	CFont Font;
	CFont CaptionFont;
	CFont StaticFont;
	CFormTab Form;

	CLabel m_Caption;
	CColorButton m_Ok;
	CColorButton m_Cancel;

	CStatic m_LUser;
	CEdit m_User;
	CStatic m_LPassword;
	CEdit m_Password;
	CStatic m_Message;

public:
	afx_msg void OnOK ();
};
