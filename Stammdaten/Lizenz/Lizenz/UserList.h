#ifndef _PARTYK_LIST_DEF
#define _PARTY_LIST_DEF
#pragma once
#include <odbcinst.h>

class CUserList
{
public:
	TCHAR computer[31];
	TCHAR fit_user[31];

	CUserList(void);
	CUserList(LPTSTR computer, LPTSTR fit_user);
	~CUserList(void);
	void SetComputer (LPTSTR computer);
	void SetComputer (CString& computer);
	void SetFitUser (LPTSTR fit_user);
	void SetFitUser (CString& fit_user);
};
#endif
