#include "StdAfx.h"
#include "ColorButton.h"
#include "Bmap.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//#ifndef IDC_HAND
// #define IDC_HAND 32649
//#endif

 IMPLEMENT_DYNCREATE(CColorButton, CStatic)
// IMPLEMENT_DYNAMIC(CColorButton, CButton)

COLORREF CColorButton::DynColorGray = RGB (150, 150, 150);
COLORREF CColorButton::DynColorBlue = RGB (100, 180, 255);

CColorButton::CColorButton(void)
{
	ButtonCursor = FALSE;
	nID = 0;
	TextColor = RGB (0, 0, 255);
	ColorSet = FALSE;
	NoUpdate = FALSE;
	Orientation = Center;
	TextStyle = Underline;
	BorderStyle = Solide;
	DynamicColor = FALSE;
	DynColor = DynColorGray;
	RolloverColor = DynColorGray;
	ActiveColor = RGB (0, 203, 203);
	IsActive = FALSE;
	IsSelected = FALSE;
    BkColor = GetSysColor (COLOR_3DFACE);
	Hand = LoadCursor (NULL,  MAKEINTRESOURCE (IDC_HAND));
	FontSet = FALSE;
	hTransBitmap = NULL;
	hMask = NULL;
	xSpace = 10;
	Container = NULL;
}

CColorButton::~CColorButton(void)
{
}

 BEGIN_MESSAGE_MAP(CColorButton, CStatic)
// BEGIN_MESSAGE_MAP(CColorButton, CButton)
	ON_WM_SIZE ()
	ON_WM_MOUSEMOVE ()
	ON_WM_LBUTTONDOWN ()
	ON_WM_SETCURSOR ()
	ON_WM_KEYDOWN ()
	ON_WM_SETFOCUS ()
	ON_WM_KILLFOCUS ()
	ON_WM_SHOWWINDOW ()
END_MESSAGE_MAP()

BOOL CColorButton::Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect,
                          CWnd* pParentWnd,   UINT nID)
{
	    this->nID = nID;
	    Style = dwStyle;
        dwStyle |= SS_OWNERDRAW | SS_NOTIFY;
//        dwStyle |= BS_OWNERDRAW | BS_NOTIFY;
		Arrow = LoadCursor (NULL,  IDC_ARROW);
//		Hand  = LoadCursor (NULL,  IDC_HAND);
//		Hand = LoadCursor (NULL,  MAKEINTRESOURCE (IDC_HAND));


		BOOL ret = CStatic::Create (lpszText, dwStyle, rect, pParentWnd, nID);
//		BOOL ret = CButton::Create (lpszText, dwStyle, rect, pParentWnd, nID);
		return ret;
}

void CColorButton::SetToolTip (LPTSTR Text)
{
	BOOL ret = FALSE;

	if (!IsWindow (Tooltip.m_hWnd))
	{
		RECT rect = {0, 0, 60, 20};
		Tooltip.Create (Text, this, 0, rect, GetParent ());
	}
}

void CColorButton::LoadBitmap (UINT Id)
{
	Bitmap.LoadBitmap (Id);
	TransBitmap.LoadBitmap (Id);
	hTransBitmap = BMAP::PrintTransparentBitmap ((HBITMAP) TransBitmap, RGB (0, 132, 132));
//	hMask = BMAP::hMask;
	hMask = BMAP::CreateBitmapMask ((HBITMAP) Bitmap, RGB ( 0, 132, 132));
}

void CColorButton::LoadMask (UINT Id)
{
	BOOL ret = Mask.LoadBitmap (Id);
	if (!ret)
	{
		return;
	}
}

/*
BOOL CColorButton::PreTranslateMessage(MSG* pMsg )
{
	switch (pMsg->message)
	{
	case WM_KILLFOCUS :
		OnKillFocus ((CWnd *)pMsg->wParam);
        return TRUE;
	}
	return CStatic::PreTranslateMessage(pMsg );
}
*/

void CColorButton::SetBkColor (COLORREF BkColor)
{
	this->BkColor = BkColor;
	ColorSet = TRUE;
}

void CColorButton::FillRectParts (CDC &cDC, COLORREF BkColor, CRect rect)
{
	int blue = (BkColor >> 16) & 0xFF;
	int green = (BkColor >> 8) & 0xFF;
	int red = (BkColor) & 0xFF;
	int height = rect.bottom - rect.top;
    int parts = 10;
	if (height > 50)
	{
		parts = 25;
	}
	else
	{
	    parts = 15;
	}
	int colordiff = 5;
	int part = (rect.bottom) / parts;
	CRect rect1 (rect.left, rect.top, rect.right, part); 
	int redplus = 240 - red;
	int greenplus = 240 - green;
	int blueplus = 240 - blue;
	int redstep = redplus / parts;
	int greenstep = greenplus / parts;
	int bluestep = blueplus / parts;
	red = min (red + redplus, 240);
	green = min (green + greenplus, 240);
	blue = min (blue + blueplus, 240);
	if (part == 1) 
	{
		part = 2;
	}

	for (int i = 0; i < parts; i ++)
	{
		cDC.FillRect (&rect1, &CBrush (RGB (red, green, blue)));
		rect1.top += part - 1;
		rect1.bottom += part;
		if (rect1.top >= rect.bottom - part) return;
		red -= redstep;
		green -= greenstep;
		blue -= bluestep;
	}
	rect1.bottom = rect.bottom; 
	cDC.FillRect (&rect1, &CBrush (RGB (red, green, blue)));
}

/*
void CColorButton::FillRectParts (CDC &cDC, COLORREF BkColor, CRect rect)
{
	int blue = (BkColor >> 16) & 0xFF;
	int green = (BkColor >> 8) & 0xFF;
	int red = (BkColor) & 0xFF;
	int parts = 15;
	int colordiff = 5;
	int part = (rect.bottom) / parts;
	CRect rect1 (rect.left, rect.top, rect.right, part); 
	int redplus = 240 - red;
	int greenplus = 240 - green;
	int blueplus = 240 - blue;
	int redstep = redplus / parts;
	int greenstep = greenplus / parts;
	int bluestep = blueplus / parts;
	red = min (red + redplus, 240);
	green = min (green + greenplus, 240);
	blue = min (blue + blueplus, 240);

	for (int i = 0; i < parts; i ++)
	{
		cDC.FillRect (&rect1, &CBrush (RGB (red, green, blue)));
		rect1.top += part - 1;
		rect1.bottom += part;
		red -= redstep;
		green -= greenstep;
		blue -= bluestep;
	}
	rect1.bottom = rect.bottom; 
	cDC.FillRect (&rect1, &CBrush (RGB (red, green, blue)));
}
*/

void CColorButton::FillRoundedRect (CDC& cDC, CRect *rect, COLORREF Color)
{
	CPen *Pen = 0;
	CPen *oldPen = NULL;
	CBrush *Brush = NULL;
	CBrush *oldBrush = NULL;

	Pen = new CPen (PS_SOLID, 0, RGB (128, 128, 128));
	oldPen = cDC.SelectObject (Pen);
	Brush = new CBrush (Color);
	oldBrush = cDC.SelectObject (Brush);
	cDC.RoundRect (rect, CPoint (5, 5));
	cDC.SelectObject (oldBrush);
	delete Brush;
	cDC.SelectObject (oldPen);
	delete Pen;
}


void CColorButton::Draw (CDC& cDC)
{
	int x = 1;
	int y = 0;
	CString Text;
	GetWindowText (Text);
	CRect rect;
	CRect fillRect;
	CSize Size;
	int ret = 0;
//	if (NoUpdate) return;

	NoUpdate = TRUE;
	GetClientRect (&rect);
	COLORREF Background;
	fillRect = rect;

	if (ColorSet)
	{
		Background = BkColor; 

		if (IsActive || IsSelected)
		{
			if (BorderStyle != Rounded)
			{
				cDC.FillRect (&rect, &CBrush (ActiveColor));
			}
		}
		else if (ButtonCursor)
		{
			if (BorderStyle != Rounded)
			{
				cDC.FillRect (&rect, &CBrush (RolloverColor));
			}
		}
		else if (DynamicColor && BorderStyle != Rounded)
		{
			FillRectParts (cDC, DynColor, rect);
		}
		else
		{
			if (BorderStyle != Rounded)
			{
				cDC.FillRect (&rect, &CBrush (Background));
			}
		}
		if ((GetFocus () == this) || (BorderStyle == Solide) || BorderStyle == Rounded)
		{
			CPen *Pen = 0;
			CPen *oldPen = NULL;
			CBrush *Brush = NULL;
			CBrush *oldBrush = NULL;
			if (BorderStyle == Dot)
			{
				Pen = new CPen (PS_DOT, 1, RGB (0, 0, 0));
				oldPen = cDC.SelectObject (Pen);
			}
			else if (BorderStyle == Solide || BorderStyle == Rounded)
			{
				Pen = new CPen (PS_SOLID, 0, RGB (128, 128, 128));
				oldPen = cDC.SelectObject (Pen);
			}
			if (BorderStyle == Rounded)
			{
				Brush = new CBrush (Background);
				oldBrush = cDC.SelectObject (Brush);
				cDC.RoundRect (&rect, CPoint (5, 5));
				cDC.SelectObject (oldBrush);
				delete Brush;
			}
			else
			{
				cDC.Rectangle (&rect);
			}
			cDC.SelectObject (oldPen);
			if (Pen != NULL) delete Pen;

			fillRect = rect;
			fillRect.left ++;
			fillRect.top ++;
			fillRect.right --;
			fillRect.bottom --;


			if (IsActive || IsSelected)
			{
				if (BorderStyle != Rounded)
				{
					cDC.FillRect (&fillRect, &CBrush (ActiveColor));
				}
				else
				{
					FillRoundedRect (cDC, &rect, ActiveColor);
				}
			}
			else if (ButtonCursor)
			{
				if (BorderStyle != Rounded)
				{
					cDC.FillRect (&fillRect, &CBrush (RolloverColor));
				}
				else
				{
					FillRoundedRect (cDC, &rect, RolloverColor);
				}
			}
			else if (DynamicColor && BorderStyle != Rounded)
			{
				FillRectParts (cDC, DynColor, fillRect);
				cDC.FrameRect (&rect, &CBrush (RGB (128, 128, 128)));
			}
			else
			{
				if (BorderStyle != Rounded)
				{
					cDC.FillRect (&fillRect, &CBrush (Background));
				}
			}
		}
	}
	else
	{
		Background = BkColor; 
		cDC.FillRect (&rect, &CBrush (GetSysColor (COLOR_3DFACE)));
		ClientToScreen (&fillRect);
		GetParent ()-> ScreenToClient (&fillRect);
		if (GetFocus () == this)
		{
			CPen *Pen = NULL;
			CPen *oldPen = NULL;
			if (BorderStyle == Dot)
			{
				Pen = new CPen (PS_DOT, 1, RGB (0, 0, 0));
				oldPen = cDC.SelectObject (Pen);
			}
			else if (BorderStyle == Solide)
			{
				Pen = new CPen  (PS_SOLID, 1, RGB (255, 255, 255));
				oldPen = cDC.SelectObject (Pen);
			}
			cDC.Rectangle (&rect);
			cDC.SelectObject (oldPen);
			if (Pen != NULL) delete Pen;
			fillRect.left ++;
			fillRect.top ++;
			fillRect.right --;
			fillRect.bottom --;
		}
	}

	if ((HBITMAP) Bitmap != NULL)
	{
		POINT Size;
		BMAP Bmap;
		Bmap.BitmapSize (cDC, (HBITMAP) Bitmap, &Size);
		x = xSpace;
		if (Text.GetLength () == 0)
		{
			x = 0;
			if (Orientation == Center)
			{
				x = max (0, (rect.right - Size.x) / 2);
			}
			else if (Orientation == Right)
			{
				x = max (0, (rect.right - Size.x));
			}
		}

		y = max (0, (rect.bottom - Size.y) / 2); 

		if ((HBITMAP) Mask != NULL)
		{
//			Bmap.DrawTransparentBitmap (&cDC, (HBITMAP) Bitmap, (HBITMAP) Mask, x, y, NULL);
			Bmap.PrintTransparentBitmap ((HBITMAP) Bitmap, (HBITMAP) Mask, &cDC, x, y);
		}
		else if (hMask != NULL)
		{
			Bmap.PrintTransparentBitmap ((HBITMAP) Bitmap, hMask, &cDC, x, y);
		}
		else
		{
//			Bmap.PrintTransparentBitmap (hBitmap, &cDC, x, y);
			Bmap.PrintTransparentBitmap ((HBITMAP) Bitmap, &cDC, x, y);
		}
//		Bmap.DrawBitmap (&cDC, (HBITMAP) Bitmap, x, y);
		x += Size.x + 10;
	}

	CString sText = Text;
	LPTSTR t = strtok (sText.GetBuffer (), "\n");
	y = 0;
	int row = 0;
	while (t != NULL)
	{
		CString T = t;
		Size = cDC.GetTextExtent (T);
		y = row * Size.cy;
		row ++;
	    t = strtok (NULL, "\n");
	}
	if (row > 0)
	{
		y = row * Size.cy;
	}
    
	int start = max (0, (rect.bottom - y) / 2);

	sText = Text;
	t = strtok (sText.GetBuffer (), "\n");
	row = 0;
	while (t != NULL)
	{
		CString T = t;
		Size = cDC.GetTextExtent (T);
		if (Orientation == Center)
		{
			x = max (x, (rect.right - Size.cx) / 2);
		}
		else if (Orientation == Right)
		{
			x = max (x, (rect.right - Size.cx));
		}
		y = row * Size.cy + start;
		cDC.SetBkMode (TRANSPARENT);
		if (IsWindowEnabled ())
		{
			cDC.SetTextColor (TextColor);
			cDC.TextOut (x, y, t, (int) _tcslen (t));
		}
		else
		{
			cDC.SetTextColor (RGB (192, 192, 192));
			cDC.TextOut (x, y, t, (int) _tcslen (t));
			cDC.SetTextColor (RGB (255, 255, 255));
			cDC.TextOut (x + 1, y + 1, t, (int) _tcslen (t));
		}
		row ++;
	    t = strtok (NULL, "\n");
	}
	NoUpdate = FALSE;
}

void CColorButton::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC cDC;

	SetFont ();
	cDC.Attach (lpDrawItemStruct->hDC);
	Draw (cDC);
}

void CColorButton::OnSize (UINT nType, int cx, int cy)
{
	Invalidate ();
}

void CColorButton::SetActive (BOOL b)
{
	IsActive = b;
	Invalidate ();
}

void CColorButton::SetSelected (BOOL b)
{
	IsSelected = b;
	SetButtonCursor (b);
	Invalidate ();
}

void CColorButton::SetButtonCursor (BOOL b)
{
	if (b)
	{
		if (ButtonCursor) return;
		ButtonCursor = TRUE;

        NMHDR hdr;
		hdr.hwndFrom = m_hWnd;
		hdr.idFrom = (UINT_PTR) m_hWnd;
		SetCapture ();
		::SetCursor (Hand);
		Invalidate ();
		Tooltip.Show ();
	}
	else
	{
		if (!ButtonCursor) return;
		ButtonCursor = FALSE;
		ReleaseCapture ();
		Invalidate ();
		Tooltip.Hide ();
	}
}

BOOL CColorButton::InClient (CPoint p)
{
	RECT rect;

	if (!IsWindowEnabled ())
	{
		return FALSE;
	}
	ClientToScreen (&p);
	GetWindowRect (&rect);
	if ((p.x >= rect.left) && (p.x <= rect.right) &&
		(p.y >= rect.top) && (p.y <= rect.bottom))
	{
			return TRUE;
	}
	return FALSE;
}


void CColorButton::SetButtonCursor (CPoint p)
{
	if (InClient (p))
	{
		SetButtonCursor (TRUE);
		return;
	}
	SetButtonCursor (FALSE);
}


void CColorButton::OnMouseMove(UINT nFlags,  CPoint p)
{
	SetButtonCursor (p);
}

void CColorButton::OnLButtonDown(UINT nFlags,  CPoint p)
{
	if (InClient (p))
	{
		GetParent ()->SendMessage (WM_COMMAND, MAKELONG (nID, 0), (LPARAM) m_hWnd);
		if (IsWindow (Tooltip.m_hWnd))
		{
			Tooltip.Hide ();
		}
	}
}

BOOL CColorButton::OnSetCursor(CWnd* pWnd, UINT nHitTest,  UINT message )
{
	if (ButtonCursor)
	{
		::SetCursor (Hand);
		return TRUE;
	}
	return FALSE;
}

void CColorButton::OnKeyDown(UINT nChar,  UINT nRepCnt,  UINT nFlags)
{
	if (nChar == VK_SPACE)
	{
		GetParent ()->SendMessage (WM_COMMAND, MAKELONG (nID, 0), (LPARAM) m_hWnd);
	}
	if (IsWindow (Tooltip.m_hWnd))
	{
		Tooltip.ShowWindow (SW_HIDE);
	}
}

void CColorButton::OnSetFocus(CWnd *cWnd)
{
	IsSelected = TRUE;
	Invalidate ();
	if (Container != NULL)
	{
		Container->ElementSetFocus (this);
	}
}

void CColorButton::OnKillFocus(CWnd *cWnd)
{
	IsSelected = FALSE;
	SetButtonCursor (FALSE);
	Invalidate ();
}

void CColorButton::OnShowWindow (BOOL bShow, UINT nStatus)
{
	IsSelected = FALSE;
	SetActive (FALSE);
	SetButtonCursor (FALSE);
	Invalidate ();
}

void CColorButton::SetFont ()
{
	static BOOL SetFont = FALSE;

    if (FontSet) return;
	if (SetFont) return;

	SetFont = TRUE;
	LOGFONT lFont;
	CFont *f = GetFont ();
	if (f != NULL)
	{
		f->GetLogFont (&lFont);
		if (TextStyle == Underline ||
			TextStyle == UnderlineBold)
		{
			lFont.lfUnderline = TRUE;
		}
		if (TextStyle == Bold ||
			TextStyle == UnderlineBold)
		{
			lFont.lfWeight = FW_BOLD;
		}
		Font.CreateFontIndirect (&lFont);
		CWnd::SetFont (&Font, TRUE);
		Invalidate ();
		FontSet = TRUE;
		SetFont = FALSE;
	}
}
void CColorButton::SetFont (CFont *f, BOOL Redraw)
{
	LOGFONT lFont;

    if (!FontSet)
	{
		f->GetLogFont (&lFont);
		if (TextStyle == Underline ||
			TextStyle == UnderlineBold)
		{
			lFont.lfUnderline = TRUE;
		}
		if (TextStyle == Bold ||
			TextStyle == UnderlineBold)
		{
			lFont.lfWeight = FW_BOLD;
		}
		Font.CreateFontIndirect (&lFont);
		CWnd::SetFont (&Font, Redraw);
	}
	FontSet = TRUE;
}