// Stc.h: Schnittstelle f�r die Klasse CStc.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STC_H__5DEACFA0_29D5_460D_99B8_37E6C3DDC4C9__INCLUDED_)
#define AFX_STC_H__5DEACFA0_29D5_460D_99B8_37E6C3DDC4C9__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <string>

using namespace std;

#include "DbClass.h"

class Lc :public DB_CLASS
{
private:
	int dsqlstatus;
	SQL_MODE sql_s;
	string sql_create;
	virtual void prepare ();
	long ldat;
	short tag;
	short mon;
	short jr;
	long ldat2;
public:
	void SetLDat (long ldat)
	{
		this->ldat = ldat;
	}

	long GetLDat ()
	{
		return ldat;
	}

	void SetLDat2 (long ldat2)
	{
		this->ldat2 = ldat2;
	}

	long GetLDat2 ()
	{
		return ldat2;
	}

	void SetTag (short tag)
	{
		this->tag = tag;
	}

	long GetTag ()
	{
		return tag;
	}

	void SetMon (short mon)
	{
		this->mon = mon;
	}

	long GetMon ()
	{
		return mon;
	}

	void SetJr (short jr)
	{
		this->jr = jr;
	}

	long GetJr ()
	{
		return jr;
	}

public:
	Lc ();
	~Lc ();
	void SetLicence (CString*date=NULL);
};

class Stc : public DB_CLASS
{
private:
	int dsqlstatus;
	SQL_MODE sql_s;
	string sql_create;
	virtual void prepare ();
	short stc_number;
	char kun_name [37];
	char text [81];
	short try_count;
public:
	void SetStcNumber (short stc_number)
	{
		this->stc_number = stc_number;
	}
    short GetStcNumber ()
	{
		return stc_number;
	}

	void SetKunName (char *kun_name)
	{
		memset (this->kun_name, 0, sizeof (this->kun_name));
		strncpy (this->kun_name, kun_name, sizeof (this->kun_name) - 1);
	}

	char *GetKunName (char *kun_name=NULL, int size=0)
	{
		char *ret = this->kun_name;
		if (kun_name != NULL && size != 0)
		{
			memset (kun_name, 0, size);
			strncpy (kun_name, this->kun_name, size - 1);
			ret = kun_name;
		}
		return ret;
	}

	void SetText (char *text)
	{
		memset (this->text, 0, sizeof (this->text));
		strncpy (this->text, text, sizeof (this->text) - 1);
	}

	char *GetText (char *text=NULL, int size=0)
	{
		char *ret = this->text;
		if (text != NULL && size != 0)
		{
			memset (text, 0, size);
			strncpy (text, this->text, size - 1);
			ret = text;
		}
		return ret;
	}

	void SetTryCount (short try_count)
	{
		this->try_count = try_count;
	}
    short GetTryCount ()
	{
		return try_count;
	}


public:
	Stc ();
	~Stc ();
	void IncTryCount ();
};

class Stcu : public DB_CLASS
{
private:
	int dsqlstatus;
	SQL_MODE sql_s;
	string sql_create;
	string idx_create;
	int count_cursor;
	int test_cursor;
	virtual void prepare ();
	char computer [31];
	char user [31];
	long count;

public : 
	void SetComputer (char computer[])
	{
		memset (this->computer, 0, sizeof (this->computer));
		strncpy (this->computer, computer, sizeof (this->computer) - 1);
	}

	char *GetComputer (char *computer=NULL, int length=0)
	{
		char *ret = this->computer;
        if (computer != NULL)
		{
			memset (user, 0, length);
			strncpy (computer, this->computer, length - 1);
			ret = computer;
		}
		return ret;
	}

	void SetUser (char user[])
	{
		memset (this->user, 0, sizeof (this->user));
		strncpy (this->user, user, sizeof (this->user) - 1);
	}

	char *GetUser (char *user= NULL, int length=0)
	{
		char *ret = this->user;
		if (user != NULL)
		{
			memset (user, 0, length);
			strncpy (user, this->user, length - 1);
		}
		return ret;
	}

public:
	Stcu ();
	~Stcu ();
	long GetCount ();
    BOOL Exist ();
	int Insert ();
	int Delete ();
};

class Stcp : public DB_CLASS
{
private:
	int dsqlstatus;
	SQL_MODE sql_s;
	string sql_create;
	DATE_STRUCT dat;
	char time [10];
	char text[256];
	virtual void prepare ();

public:
	void SetText (char *text)
	{
		strcpy (this->text, text);
	}
	char *GetText ()
	{
		return text;
	}
	Stcp ();
	~Stcp ();
	int Insert ();
};

class CStc  
{
public:
	enum TRY_ERROR
	{
		NoError,
		NoWarning,
        FirstWarning,  
		SecondWarning,
		Abbort
	};
private:
	static CStc *Instance;
	string user;
	string computer;
	Stc *m_Stc;
	Stcu *m_Stcu;
	Lc *m_Lc;
	string kun_name;
	string text;
	TRY_ERROR TryError;
	string ErrorText[5];
protected:
	CStc();
	virtual ~CStc();

public :
	TRY_ERROR GetTryError ()
	{
		return TryError;
	}

	char *GetErrorText (char *Text=NULL, int size=0);
	static CStc *GetInstance ();
	static void DestroyInstance ();
    bool SetLicenceCount (short licence_count);
    bool TestLicenceCount ();
    bool AddUser ();
    bool DropUser ();
	int GetUserCount ();
	int GetLicenceCount ();
    void SetKunName (char *kun_name);
	char *GetKunName ();
    void SetText (char *text);
	char *GetText ();
	void GetLicenceData ();
	void SetTryCount (int try_count);
	void SetLicence (CString *date=NULL);
};

#endif // !defined(AFX_STC_H__5DEACFA0_29D5_460D_99B8_37E6C3DDC4C9__INCLUDED_)
