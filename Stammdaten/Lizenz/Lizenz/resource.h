//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Lizenz.rc
//
#define IDC_FREE_LICENCE                3
#define IDC_SET_LICENCE                 4
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_LIZENZ_DIALOG               102
#define IDB_OK                          129
#define IDB_OKMASK                      130
#define IDB_CANCEL                      131
#define IDB_CANCELMASK                  132
#define IDR_MAINFRAME                   133
#define IDB_BITMAP1                     134
#define IDD_DIALOG1                     135
#define IDD_PASSWORD_DIALOG             136
#define IDB_USERLIST                    137
#define IDB_BITMAP3                     138
#define IDB_USERLISTMASK                138
#define IDB_FREE_LICENCE                139
#define IDB_FREE_LICENCE_MASK           140
#define IDB_LICENCE_DAT                 141
#define IDB_LICENCE_DAT_MASK            142
#define IDC_CAPTION                     1000
#define IDC_LICENCECOUNT                1001
#define IDC_SPIN1                       1002
#define IDC_LLICENCE_COUNT              1017
#define IDC_LKUN_NAME                   1004
#define IDC_KUN_NAME                    1005
#define IDC_TEXT                        1006
#define IDC_LTEXT                       1007
#define IDC_LUSER                       1008
#define IDC_USER                        1009
#define IDC_LPASSWORD                   1010
#define IDC_PASSWORD                    1011
#define IDC_MESSAGE                     1012
#define IDC_CHOICE_USER                 1013
#define IDC_CHOICE_LOG                  1014
#define IDC_TEST_DATE                   1015
#define IDC_LTEST_DATE                  1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
