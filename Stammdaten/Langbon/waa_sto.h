#ifndef _WAA_STO_DEF
#define _WAA_STO_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct WAA_STO {
   double         a;
   long           bon;
   TCHAR          bon_kz[2];
   short          bed;
   TCHAR          bsd_kz[2];
   DATE_STRUCT    dat;
   TCHAR          dr_status[2];
   short          fil;
   long           lfd;
   double         me_ist;
   short          mdn;
   TCHAR          pers_nam[9];
   long           prima;
   double         pr_sto;
   double         pr_vk;
   double         sto_me;
   short          sto_pos_bws;
   TCHAR          txt[61];
   short          verk_st;
   short          waa;
   TCHAR          zeit[7];
   TCHAR          lad_akv[2];
   double         pr_ek;
};
extern struct WAA_STO waa_sto, waa_sto_null;

#line 8 "waa_sto.rh"

class WAA_STO_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               WAA_STO waa_sto;  
               WAA_STO_CLASS () : DB_CLASS ()
               {
               }
};
#endif
