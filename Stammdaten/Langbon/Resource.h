//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Langbon.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_FORMVIEW                    101
#define IDR_MAINFRAME                   128
#define IDR_LangbonTYPE                 129
#define IDD_DIALOG1                     131
#define IDB_OPEN                        132
#define IDB_OK                          133
#define IDB_BITMAP1                     134
#define IDB_CANCEL                      134
#define IDC_EDIT1                       1000
#define IDC_MDN                         1000
#define IDC_SYS                         1000
#define IDC_RECH2                       1001
#define IDC_KUN                         1002
#define IDC_DATE_VON                    1003
#define IDC_DATE_BIS                    1004
#define IDC_LSYS                        1005
#define IDC_LIST1                       1006
#define IDC_KUN_KRZ1                    1007
#define IDC_TIME_FROM                   1007
#define IDC_LDAT_VON                    1008
#define IDC_LDAT_BIS                    1009
#define IDC_SYSNAME                     1010
#define IDC_LBON                        1011
#define IDC_DATE_VON2                   1012
#define IDC_BON                         1012
#define IDC_LTIME_FROM                  1013
#define IDC_LTIME_TO                    1014
#define IDC_TIME_TO                     1015
#define IDC_BORDER                      1016
#define IDC_LBED                        1017
#define IDC_BED                         1018
#define IDC_KUNCHOICE                   1019
#define IDC_BEDNAME                     1019
#define IDC_BLG_TYP                     1020
#define IDC_LIST2                       1020
#define IDC_POSLIST                     1020
#define IDC_BONSTORNO                   1021
#define IDC_LWAA                        1021
#define IDC_POSSTORNO                   1022
#define IDC_WAA                         1022
#define ID_BACK                         32773
#define ID_STORNO                       32776
#define ID_DATEI_LANGBONSTORNIEREN      32777
#define ID_SAVE                         32779
#define ID_PRINT                        32780
#define ID_BONVIEW                      32783
#define ID_BUTTON32784                  32784
#define ID_BUTTON32785                  32785

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32786
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
