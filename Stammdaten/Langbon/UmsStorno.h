#pragma once
#include "A_bas.h"

class CUmsStorno
{
public:
	BOOL LbVerbuch;
	DATE_STRUCT dat;
	short mdn;
	short fil;
	double a;
	short mwst;
	double ums_vk_mwst;
	double ums_vk;
	double me_vk;
	long me_vk_pos;
	double ums_vk_mwst_sa;
	double ums_vk_sa;
	double me_vk_sa;
	double me_vk_pos_sa;
	short wg;
	short wo;
	short mo;
	short year;

	int atagcursor;
	int asatagcursor;
	int awocursor;
	int amocursor;
	int wgtagcursor;
	int wgwocursor;
	int wgmocursor;

	int ataglbcursor;
	int asataglbcursor;
	int awolbcursor;
	int amolbcursor;
	int wgtaglbcursor;
	int wgwolbcursor;
	int wgmolbcursor;
	int kasesumcursor;

	DB_CLASS DbClass;
	A_BAS_CLASS A_bas;
	CUmsStorno(void);
	~CUmsStorno(void);
	virtual void Prepare ();
	virtual BOOL Update ();
	BOOL UpdateAtag ();
	BOOL UpdateASatag ();
	BOOL UpdateAWo ();
	BOOL UpdateAMo ();
	BOOL UpdateWgtag ();
	BOOL UpdateWgwo ();
	BOOL UpdateWgmo ();

	BOOL UpdateAtagLb ();
	BOOL UpdateASatagLb ();
	BOOL UpdateAWoLb ();
	BOOL UpdateAMoLb ();
	BOOL UpdateWgtagLb ();
	BOOL UpdateWgwoLb ();
	BOOL UpdateWgmoLb ();

};
