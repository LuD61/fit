#ifdef WIN32
#include <windows.h>
#endif
#include <odbcinst.h>
#include <time.h>

class DayTime
{
     public :
		 long lTime;
		 int Hour;
		 int Min;
		 int Sec;
		 CString Time;
		 DayTime ();
		 DayTime (char *t);
		 DayTime (CString *t);
		 DayTime *Create (char *t);
		 DayTime *Create (CString *t);
		 DayTime *Create (CString *t, LPTSTR format);
		 long GetLongTime ();
		 CString *GetTextTime ();
};

class DbTime
{
     private :
		  CString tDate;
		  DATE_STRUCT ds;
          SYSTEMTIME st;
		  FILETIME ft;
		  ULARGE_INTEGER ul;
		  time_t ldat;
		  int Mo;
		  int Wo;
		  static short mon_tab[];
     public :
	      DbTime (DATE_STRUCT *);
		  time_t GetTime ();
          int GetWeekDay ();
		  void SetTime (time_t);
		  void SetDateStruct ();
		  CString* GetStringTime ();
		  DATE_STRUCT *GetDateStruct (DATE_STRUCT *);
#ifdef WIN32
          DbTime& operator+ (int);
          DbTime& operator- (int);
#endif
          DbTime* Add (int);
          DbTime* Sub (int);

		  short GetYear();
		  short GetMonth();
		  short GetWeek();
	      short GetDay();
	  	  short GetYearDay();
	      short GetSchaltJahr();
          static short WeekKorr(short,short);
		  static short WeekPre(char *);

		  static short GetYear(LPTSTR date);
		  static short GetMonth(LPTSTR date);
	      static short GetDay(LPTSTR date);
		  static short GetWeek(LPTSTR datum);
		  static long FirstKwDay (short kw, short year);
		  static long LastKwDay (short kw, short year);
	      static short GetSchaltJahr(int year);
          static long GetDays (DATE_STRUCT *ds);
          static long GetDays (LPTSTR dat);
	      static void FromDays (long ldat, LPTSTR date);
	  	  static short GetYearDay(LPTSTR date);
          int FirstKwDayC (short kw, short jr, LPTSTR datum);
		  int LastKwDayC (short kw, short jr, LPTSTR datum);
};
