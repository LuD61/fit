#include "stdafx.h"
#include "ptabn.h"

struct PTABN ptabn, ptabn_null;

void PTABN_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((char *)   ptabn.ptitem, SQLCHAR, sizeof (ptabn.ptitem));
            sqlin ((char *)   ptabn.ptwert, SQLCHAR, sizeof (ptabn.ptwert));
    sqlout ((TCHAR *) ptabn.ptitem,SQLCHAR,19);
    sqlout ((long *) &ptabn.ptlfnr,SQLLONG,0);
    sqlout ((TCHAR *) ptabn.ptwert,SQLCHAR,4);
    sqlout ((TCHAR *) ptabn.ptbez,SQLCHAR,33);
    sqlout ((TCHAR *) ptabn.ptbezk,SQLCHAR,9);
    sqlout ((TCHAR *) ptabn.ptwer1,SQLCHAR,9);
    sqlout ((TCHAR *) ptabn.ptwer2,SQLCHAR,9);
    sqlout ((short *) &ptabn.delstatus,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &ptabn.akv,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &ptabn.bearb,SQLDATE,0);
    sqlout ((TCHAR *) ptabn.pers_nam,SQLCHAR,9);
            cursor = sqlcursor (_T("select ptabn.ptitem,  ")
_T("ptabn.ptlfnr,  ptabn.ptwert,  ptabn.ptbez,  ptabn.ptbezk,  ptabn.ptwer1,  ")
_T("ptabn.ptwer2,  ptabn.delstatus,  ptabn.akv,  ptabn.bearb,  ")
_T("ptabn.pers_nam from ptabn ")

#line 13 "ptabn.rpp"
                                  _T("where ptitem = ? ")
				  _T("and ptwert = ?"));
    sqlin ((TCHAR *) ptabn.ptitem,SQLCHAR,19);
    sqlin ((long *) &ptabn.ptlfnr,SQLLONG,0);
    sqlin ((TCHAR *) ptabn.ptwert,SQLCHAR,4);
    sqlin ((TCHAR *) ptabn.ptbez,SQLCHAR,33);
    sqlin ((TCHAR *) ptabn.ptbezk,SQLCHAR,9);
    sqlin ((TCHAR *) ptabn.ptwer1,SQLCHAR,9);
    sqlin ((TCHAR *) ptabn.ptwer2,SQLCHAR,9);
    sqlin ((short *) &ptabn.delstatus,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &ptabn.akv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &ptabn.bearb,SQLDATE,0);
    sqlin ((TCHAR *) ptabn.pers_nam,SQLCHAR,9);
            sqltext = _T("update ptabn set ptabn.ptitem = ?,  ")
_T("ptabn.ptlfnr = ?,  ptabn.ptwert = ?,  ptabn.ptbez = ?,  ")
_T("ptabn.ptbezk = ?,  ptabn.ptwer1 = ?,  ptabn.ptwer2 = ?,  ")
_T("ptabn.delstatus = ?,  ptabn.akv = ?,  ptabn.bearb = ?,  ")
_T("ptabn.pers_nam = ? ")

#line 16 "ptabn.rpp"
                                  _T("where ptitem = ? ")
				  _T("and ptwert = ?");
            sqlin ((char *)   ptabn.ptitem, SQLCHAR, sizeof (ptabn.ptitem));
            sqlin ((char *)   ptabn.ptwert, SQLCHAR, sizeof (ptabn.ptwert));
            upd_cursor = sqlcursor (sqltext);

            sqlin ((char *)   ptabn.ptitem, SQLCHAR, sizeof (ptabn.ptitem));
            sqlin ((char *)   ptabn.ptwert, SQLCHAR, sizeof (ptabn.ptwert));
            test_upd_cursor = sqlcursor (_T("select ptwert from ptabn ")
                                  _T("where ptitem = ? ")
				  _T("and ptwert = ?"));
            sqlin ((char *)   ptabn.ptitem, SQLCHAR, sizeof (ptabn.ptitem));
            sqlin ((char *)   ptabn.ptwert, SQLCHAR, sizeof (ptabn.ptwert));
            del_cursor = sqlcursor (_T("delete from ptabn ")
                                  _T("where ptitem = ? ")
				  _T("and ptwert = ?"));
    sqlin ((TCHAR *) ptabn.ptitem,SQLCHAR,19);
    sqlin ((long *) &ptabn.ptlfnr,SQLLONG,0);
    sqlin ((TCHAR *) ptabn.ptwert,SQLCHAR,4);
    sqlin ((TCHAR *) ptabn.ptbez,SQLCHAR,33);
    sqlin ((TCHAR *) ptabn.ptbezk,SQLCHAR,9);
    sqlin ((TCHAR *) ptabn.ptwer1,SQLCHAR,9);
    sqlin ((TCHAR *) ptabn.ptwer2,SQLCHAR,9);
    sqlin ((short *) &ptabn.delstatus,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &ptabn.akv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &ptabn.bearb,SQLDATE,0);
    sqlin ((TCHAR *) ptabn.pers_nam,SQLCHAR,9);
            ins_cursor = sqlcursor (_T("insert into ptabn (")
_T("ptitem,  ptlfnr,  ptwert,  ptbez,  ptbezk,  ptwer1,  ptwer2,  delstatus,  akv,  bearb,  ")
_T("pers_nam) ")

#line 33 "ptabn.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?)")); 

#line 35 "ptabn.rpp"
}
