#pragma once

class CStrFuncs
{
public:
	CStrFuncs(void);
	~CStrFuncs(void);
    static double StrToDouble (LPTSTR);
    static double StrToDouble (CString&);
	static void SysDate (CString& Date);
    static void ToClipboard (CString& Text);
    static void FromClipboard (CString& Text);
	static void Trim (LPSTR);
};
