#ifndef _LB_DEF
#define _LB_DEF

#include "dbclass.h"

struct LB {
   long           bon;
   short          abt;
   short          waa;
   short          bed;
   short          buch_art;
   short          wg;
   short          par3_sint;
   short          par4_sint;
   short          mwst_verbu;
   short          mwst;
   double         pr_vk;
   double         ums_vk;
   long           me_vk_pos;
   double         me_vk;
   double         a;
   double         lief_me;
   short          stat;
   DATE_STRUCT    dat;
   char           zeit[7];
   short          mdn;
   short          fil;
   long           sys;
   short          fitok;
   short          pos;
   long           a_krz;
   short          nr;
   double         ums_mwst;
   short          sa;
   double         pr_ek;
   double         pr_vk_waa;
   double         sto_me_vk;
   long           sto_vk_pos;
   double         sto_ums_vk;
   double         sto_ums_mwst;
   short          sto_fit;
   short          sto_kz;
   short          kase;
};
extern struct LB lb, lb_null;

#line 7 "lb.rh"

class LB_CLASS : public DB_CLASS 
{
       private :
               virtual void prepare ();
               int del_acursor;

       public :
               LB lb;  
               LB_CLASS (): DB_CLASS ()
               {
		    del_acursor = -1;	
	       }
               int dbdeletea ();
};
#endif