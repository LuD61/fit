#include "stdafx.h"
#include "bed.h"

struct BED bed, bed_null;

void BED_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &bed.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &bed.fil,  SQLSHORT, 0);
            sqlin ((short *)   &bed.bed,  SQLSHORT, 0);
            sqlin ((short *)   &bed.verk_st,  SQLSHORT, 0);
            sqlin ((char *)    bed.pers,  SQLCHAR, sizeof (bed.pers));
    sqlout ((short *) &bed.bed,SQLSHORT,0);
    sqlout ((TCHAR *) bed.bed_nam,SQLCHAR,29);
    sqlout ((short *) &bed.fil,SQLSHORT,0);
    sqlout ((short *) &bed.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) bed.pers,SQLCHAR,13);
    sqlout ((short *) &bed.verk_st,SQLSHORT,0);
            cursor = sqlcursor (_T("select bed.bed,  ")
_T("bed.bed_nam,  bed.fil,  bed.mdn,  bed.pers,  bed.verk_st from bed ")

#line 16 "Bed.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and bed = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and pers = ? "));
    sqlin ((short *) &bed.bed,SQLSHORT,0);
    sqlin ((TCHAR *) bed.bed_nam,SQLCHAR,29);
    sqlin ((short *) &bed.fil,SQLSHORT,0);
    sqlin ((short *) &bed.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) bed.pers,SQLCHAR,13);
    sqlin ((short *) &bed.verk_st,SQLSHORT,0);
            sqltext = _T("update bed set bed.bed = ?,  ")
_T("bed.bed_nam = ?,  bed.fil = ?,  bed.mdn = ?,  bed.pers = ?,  ")
_T("bed.verk_st = ? ")

#line 22 "Bed.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and bed = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and pers = ? ");
            sqlin ((short *)   &bed.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &bed.fil,  SQLSHORT, 0);
            sqlin ((short *)   &bed.bed,  SQLSHORT, 0);
            sqlin ((short *)   &bed.verk_st,  SQLSHORT, 0);
            sqlin ((char *)    bed.pers,  SQLCHAR, sizeof (bed.pers));
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &bed.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &bed.fil,  SQLSHORT, 0);
            sqlin ((short *)   &bed.bed,  SQLSHORT, 0);
            sqlin ((short *)   &bed.verk_st,  SQLSHORT, 0);
            sqlin ((char *)    bed.pers,  SQLCHAR, sizeof (bed.pers));
            test_upd_cursor = sqlcursor (_T("select bed from bed ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and bed = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and pers = ? "));
            sqlin ((short *)   &bed.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &bed.fil,  SQLSHORT, 0);
            sqlin ((short *)   &bed.bed,  SQLSHORT, 0);
            sqlin ((short *)   &bed.verk_st,  SQLSHORT, 0);
            sqlin ((char *)    bed.pers,  SQLCHAR, sizeof (bed.pers));
            del_cursor = sqlcursor (_T("delete from bed ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and bed = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and pers = ? "));
    sqlin ((short *) &bed.bed,SQLSHORT,0);
    sqlin ((TCHAR *) bed.bed_nam,SQLCHAR,29);
    sqlin ((short *) &bed.fil,SQLSHORT,0);
    sqlin ((short *) &bed.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) bed.pers,SQLCHAR,13);
    sqlin ((short *) &bed.verk_st,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into bed (bed,  ")
_T("bed_nam,  fil,  mdn,  pers,  verk_st) ")

#line 57 "Bed.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?)")); 

#line 59 "Bed.rpp"
}
