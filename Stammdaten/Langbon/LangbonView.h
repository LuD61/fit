// LangbonView.h : Schnittstelle der Klasse CLangbonView
//


#pragma once
#include "DbFormView.h"
#include "FillList.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "CtrlGrid.h"
#include "ChoiceSys.h"
#include "ChoiceBed.h"
#include "Sys_peri.h"
#include "FormTab.h"
#include "Mdn.h"
#include "Fil.h"
#include "Adr.h"
#include "Lb.h"
#include "Bed.h"
#include "A_bas.h"
#include "ptabn.h"
#include "PaintListCtrl.h"
#include "Vector.h"
#include <vector>
#include "waa_sto.h"
#include "kase_sto.h"
#include "waa_sum.h"
#include "kase_sum.h"
#include "bed_sum.h"
#include "mwst_sum.h"
#include "fil_einn.h"
#include "sys_par.h"
#include "UmsStorno.h"
#include "SplitPane.h"
#include "ListRowUpdate.h"

#define IDC_SYSCHOICE 1050
#define IDC_BEDCHOICE 1051
#define IDC_BONVIEW 1052


class CLangbonView : public DbFormView, CListRowUpdate

{
protected: // Nur aus Serialisierung erstellen
	HBRUSH bonBrush;
	CLangbonView();
	DECLARE_DYNCREATE(CLangbonView)
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterstützung

// Attribute
public:
//	enum { IDD = IDD_LANGBONDLG };
	enum { IDD = IDD_FORMVIEW };
	CLangbonDoc* GetDocument() const;
    virtual void OnInitialUpdate ();
// Operationen
public:

// Überschreibungen
	public:
	virtual void OnDraw(CDC* pDC);  // Überladen, um diese Ansicht darzustellen
virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~CLangbonView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	BOOL BonViewActive;
public :
	enum 
	{
		Storno = 0,
		Test = 1,
	};
	static int StoMode;
	BOOL LbVerbuch;
    SYS_PAR_CLASS Sys_par;
    SYS_PERI_CLASS Sys_peri;
    MDN_CLASS Mdn;
    FIL_CLASS Fil;
    ADR_CLASS Adr;
	LB_CLASS Lb;
	BED_CLASS Bed;
	A_BAS_CLASS A_bas;
	PTABN_CLASS Ptabn;
	WAA_STO_CLASS Waa_sto;
	KASE_STO_CLASS Kase_sto;
	WAA_SUM_CLASS Waa_sum;
	KASE_SUM_CLASS Kase_sum;
	BED_SUM_CLASS Bed_sum;
	MWST_SUM_CLASS Mwst_sum;
	FIL_EINN_CLASS Fil_einn;
	CUmsStorno UmsStorno;
	CVector PosTab;
	std::vector<short> MwstTab;

	BOOL UseWaaSto;
	BOOL ModalChoice;
	CChoiceSys *Choice;
	BOOL ModalChoiceBed;
	CChoiceBed *ChoiceBed;
	CFormTab Form;
	CFillList FillList;
	CFillList FillPos;
	CCtrlGrid CtrlGrid;
	CCtrlGrid SysGrid;
	CCtrlGrid BedGrid;

	CString sDatVon;
	CString sDatBis;
	CString sBed;
	CString sWaa;

	DATE_STRUCT DatVon;
	DATE_STRUCT DatBis;
	char TimeFrom [6];
	char TimeTo [6];
	int BedCursor;
	int PosCursor;

	CEdit m_Sys;
	CButton m_SysChoice;
	CStatic m_LSys;
	CStatic m_LDatVon;
	CEdit m_DatVon;
	CStatic m_LDatBis;
	CEdit m_DatBis;
//	CListCtrl m_LbList;
	CPaintListCtrl m_LbList;
	CSplitPane SplitPane;
    afx_msg void OnSize(UINT, int, int);
	CEdit m_SysName;
	void OnSyschoice ();
	void OnBedchoice ();
    virtual BOOL OnKeyup ();
	virtual BOOL OnReturn ();
    virtual BOOL Read ();
    BOOL ReadBed ();
	afx_msg void OnFileOpen();
	void FillBons ();
	CString Bon;
	CEdit m_Bon;
	CStatic m_LBon;
	afx_msg void OnBack();
	CStatic m_LTimeFrom;
	CEdit m_TimeFrom;
	CString sTimeFrom;
	CString sTimeTo;
	CStatic m_LTimeTo;
	CEdit m_TimeTo;
	CStatic m_Border;
	CButton m_BedChoice;
	CEdit m_Bed;
	CStatic m_LBed;
	CEdit m_BedName;
	CRichEditCtrl m_BonView;
//	CListCtrl m_PosList;
	CPaintListCtrl m_PosList;
	afx_msg void OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	void FillLbPos ();
	afx_msg void OnStorno();
	afx_msg void OnPrint();
	BOOL WaaStorno ();
	BOOL KaseStorno ();
	BOOL BedStorno ();
	BOOL MwstStorno ();
	BOOL FilEinnStorno ();
	double NettoValue (short mwst, double value);
	BOOL BonStorno ();
	BOOL PosStorno (int pidx);
	CStatic m_LWaa;
	CEdit m_Waa;
	afx_msg void OnViewbon();
	void SetBonView ();
	void UnsetBonView ();
    void FillBonView ();
	virtual BOOL SetRowColor (int Row, COLORREF *Color);
};

#ifndef _DEBUG  // Debugversion in LangbonView.cpp
inline CLangbonDoc* CLangbonView::GetDocument() const
   { return reinterpret_cast<CLangbonDoc*>(m_pDocument); }
#endif


