#ifndef _BED_DEF
#define _BED_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct BED {
   short          bed;
   TCHAR          bed_nam[29];
   short          fil;
   short          mdn;
   TCHAR          pers[13];
   short          verk_st;
};
extern struct BED bed, bed_null;

#line 8 "Bed.rh"

class BED_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               BED bed;  
               BED_CLASS () : DB_CLASS ()
               {
               }
};
#endif
