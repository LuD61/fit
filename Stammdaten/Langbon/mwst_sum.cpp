#include "stdafx.h"
#include "mwst_sum.h"

struct MWST_SUM mwst_sum, mwst_sum_null;

void MWST_SUM_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &mwst_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.mwst,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &mwst_sum.dat,  SQLDATE, 0);
    sqlout ((short *) &mwst_sum.mdn,SQLSHORT,0);
    sqlout ((short *) &mwst_sum.fil,SQLSHORT,0);
    sqlout ((short *) &mwst_sum.verk_st,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &mwst_sum.dat,SQLDATE,0);
    sqlout ((short *) &mwst_sum.mwst,SQLSHORT,0);
    sqlout ((double *) &mwst_sum.mwst_betr,SQLDOUBLE,0);
    sqlout ((double *) &mwst_sum.me_vk,SQLDOUBLE,0);
    sqlout ((long *) &mwst_sum.me_vk_pos,SQLLONG,0);
    sqlout ((double *) &mwst_sum.ums_vk,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select mwst_sum.mdn,  ")
_T("mwst_sum.fil,  mwst_sum.verk_st,  mwst_sum.dat,  mwst_sum.mwst,  ")
_T("mwst_sum.mwst_betr,  mwst_sum.me_vk,  mwst_sum.me_vk_pos,  ")
_T("mwst_sum.ums_vk from mwst_sum ")

#line 16 "mwst_sum.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and mwst = ? ")
                                  _T("and dat = ? "));
    sqlin ((short *) &mwst_sum.mdn,SQLSHORT,0);
    sqlin ((short *) &mwst_sum.fil,SQLSHORT,0);
    sqlin ((short *) &mwst_sum.verk_st,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &mwst_sum.dat,SQLDATE,0);
    sqlin ((short *) &mwst_sum.mwst,SQLSHORT,0);
    sqlin ((double *) &mwst_sum.mwst_betr,SQLDOUBLE,0);
    sqlin ((double *) &mwst_sum.me_vk,SQLDOUBLE,0);
    sqlin ((long *) &mwst_sum.me_vk_pos,SQLLONG,0);
    sqlin ((double *) &mwst_sum.ums_vk,SQLDOUBLE,0);
            sqltext = _T("update mwst_sum set ")
_T("mwst_sum.mdn = ?,  mwst_sum.fil = ?,  mwst_sum.verk_st = ?,  ")
_T("mwst_sum.dat = ?,  mwst_sum.mwst = ?,  mwst_sum.mwst_betr = ?,  ")
_T("mwst_sum.me_vk = ?,  mwst_sum.me_vk_pos = ?,  mwst_sum.ums_vk = ? ")

#line 22 "mwst_sum.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and mwst = ? ")
                                  _T("and dat = ? ");
            sqlin ((short *)   &mwst_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.mwst,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &mwst_sum.dat,  SQLDATE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &mwst_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.mwst,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &mwst_sum.dat,  SQLDATE, 0);
            test_upd_cursor = sqlcursor (_T("select mwst from mwst_sum ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and mwst = ? ")
                                  _T("and dat = ? "));
            sqlin ((short *)   &mwst_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &mwst_sum.mwst,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &mwst_sum.dat,  SQLDATE, 0);
            del_cursor = sqlcursor (_T("delete from mwst_sum ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and mwst = ? ")
                                  _T("and dat = ? "));
    sqlin ((short *) &mwst_sum.mdn,SQLSHORT,0);
    sqlin ((short *) &mwst_sum.fil,SQLSHORT,0);
    sqlin ((short *) &mwst_sum.verk_st,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &mwst_sum.dat,SQLDATE,0);
    sqlin ((short *) &mwst_sum.mwst,SQLSHORT,0);
    sqlin ((double *) &mwst_sum.mwst_betr,SQLDOUBLE,0);
    sqlin ((double *) &mwst_sum.me_vk,SQLDOUBLE,0);
    sqlin ((long *) &mwst_sum.me_vk_pos,SQLLONG,0);
    sqlin ((double *) &mwst_sum.ums_vk,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into mwst_sum (")
_T("mdn,  fil,  verk_st,  dat,  mwst,  mwst_betr,  me_vk,  me_vk_pos,  ums_vk) ")

#line 57 "mwst_sum.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?)")); 

#line 59 "mwst_sum.rpp"
}
