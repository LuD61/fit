#ifndef _CHOICEBED_DEF
#define _CHOICEBED_DEF

#include "ChoiceX.h"
#include "BedList.h"
#include <vector>

class CChoiceBed : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
      
    public :
		short mdn;
		short fil;
	    std::vector<CBedList *> BedList;
      	CChoiceBed(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceBed(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchAdrKrz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
        virtual void SetSelText (CListCtrl *, int);
	CBedList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
	void DestroyList ();
};
#endif
