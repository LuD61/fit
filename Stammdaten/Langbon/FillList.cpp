#include "StdAfx.h"
#include "filllist.h"

CFillList::CFillList(void)
{
	listView = NULL;
}

CFillList::CFillList(CListCtrl *listView)
{
	this->listView = listView;
}

CFillList::~CFillList(void)
{
}

void CFillList::SetListView (CListCtrl *ListView)
{
	this->listView = listView;
}

CFillList& CFillList::operator= (CListCtrl& listView)
{
	this->listView = &listView;
	return *this;
}

BOOL CFillList::SetCol (char *Txt, int idx, int Width)
{  
   LV_COLUMN Col;

   HWND hWndListView = listView->m_hWnd;

   Col.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;   
   Col.fmt = LVCFMT_LEFT;
   Col.cx  = Width;
   Col.pszText = Txt;
   Col.iSubItem = idx;
   return ListView_InsertColumn(hWndListView, idx, &Col); 
}

BOOL CFillList::SetCol (char *Txt, int idx, int Width, int Align)
{  
   LV_COLUMN Col;

   HWND hWndListView = listView->m_hWnd;

   Col.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;   
   Col.fmt = Align;
   Col.cx  = Width;
   Col.pszText = Txt;
   Col.iSubItem = idx;
   return listView->InsertColumn (idx, Txt, Align, Width); 
//   return ListView_InsertColumn(hWndListView, idx, &Col); 
}

DWORD CFillList::SetStyle (DWORD st)
{

    DWORD Style = GetWindowLong (listView->m_hWnd, GWL_STYLE);

    Style &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | st);
    SetWindowLong (listView->m_hWnd, GWL_STYLE, Style |= st); 
    return Style;
}

DWORD CFillList::SetExtendedStyle (DWORD st)
{

    DWORD style = listView->GetExtendedStyle ();
    style |= st;
    listView->SetExtendedStyle (style);

    return style;
}

int CFillList::InsertItem (int idx, int Image)
{
   LV_ITEM Item;

   Item.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
//   Item.mask = LVIF_IMAGE | LVIF_PARAM; 
   Item.state      = 0; 
   Item.stateMask  = 0; 
   Item.iImage     = Image;
   Item.iItem      = idx;
   Item.iSubItem   = 0;
   Item.pszText    = NULL;
   Item.lParam     = (LPARAM) idx;
   return listView->InsertItem (&Item);
}

BOOL CFillList::SetItemImage (int idx, int Image)
{
   LV_ITEM Item;

//   Item.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
   Item.mask = LVIF_IMAGE; 
   Item.state      = 0; 
   Item.stateMask  = 0; 
   Item.iImage     = Image;
   Item.iItem      = idx;
   Item.iSubItem   = 0;
   Item.pszText    = NULL;
   Item.lParam     = (LPARAM) idx;
   return listView->SetItem (&Item);
}

BOOL CFillList::SetItemImage (int idx, int sub, int Image)
{
   LV_ITEM Item;

//   Item.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
   Item.mask = LVIF_IMAGE; 
   Item.state      = 0; 
   Item.stateMask  = 0; 
   Item.iImage     = Image;
   Item.iItem      = idx;
   Item.iSubItem   = sub;
   Item.pszText    = NULL;
   Item.lParam     = (LPARAM) idx;
   BOOL ret = listView->SetItem (&Item);
   return ret;
}

int CFillList::GetItemImage (int idx)
{
   LV_ITEM Item;
   Item.mask = LVIF_IMAGE; 
   Item.state      = 0; 
   Item.stateMask  = 0; 
   Item.iItem      = idx;
   Item.iSubItem   = 0;
   Item.pszText    = NULL;
   Item.lParam     = (LPARAM) idx;
   listView->GetItem (&Item);
   return Item.iImage;
}

BOOL CFillList::SetItemText (char *Txt, int idx, int pos)
{

   return listView->SetItemText (idx, pos, Txt);
}

