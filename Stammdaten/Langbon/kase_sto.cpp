#include "stdafx.h"
#include "kase_sto.h"

struct KASE_STO kase_sto, kase_sto_null;

void KASE_STO_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &kase_sto.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sto.fil,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sto.verk_st,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &kase_sto.dat,  SQLDATE, 0);
            sqlin ((char *)    kase_sto.zeit,  SQLCHAR, sizeof (kase_sto.zeit));
            sqlin ((long *)   &kase_sto.bon,  SQLLONG, 0);
            sqlin ((double *) &kase_sto.a,  SQLDOUBLE, 0);
    sqlout ((double *) &kase_sto.a,SQLDOUBLE,0);
    sqlout ((long *) &kase_sto.bon,SQLLONG,0);
    sqlout ((TCHAR *) kase_sto.bon_kz,SQLCHAR,2);
    sqlout ((short *) &kase_sto.bed,SQLSHORT,0);
    sqlout ((TCHAR *) kase_sto.bsd_kz,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &kase_sto.dat,SQLDATE,0);
    sqlout ((TCHAR *) kase_sto.dr_status,SQLCHAR,2);
    sqlout ((short *) &kase_sto.fil,SQLSHORT,0);
    sqlout ((long *) &kase_sto.lfd,SQLLONG,0);
    sqlout ((TCHAR *) kase_sto.lad_akv,SQLCHAR,2);
    sqlout ((double *) &kase_sto.me_ist,SQLDOUBLE,0);
    sqlout ((short *) &kase_sto.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) kase_sto.pers_nam,SQLCHAR,9);
    sqlout ((long *) &kase_sto.prima,SQLLONG,0);
    sqlout ((double *) &kase_sto.pr_sto,SQLDOUBLE,0);
    sqlout ((double *) &kase_sto.pr_ek,SQLDOUBLE,0);
    sqlout ((double *) &kase_sto.pr_vk,SQLDOUBLE,0);
    sqlout ((double *) &kase_sto.sto_me,SQLDOUBLE,0);
    sqlout ((short *) &kase_sto.sto_pos_bws,SQLSHORT,0);
    sqlout ((TCHAR *) kase_sto.txt,SQLCHAR,61);
    sqlout ((short *) &kase_sto.verk_st,SQLSHORT,0);
    sqlout ((short *) &kase_sto.kase,SQLSHORT,0);
    sqlout ((TCHAR *) kase_sto.zeit,SQLCHAR,7);
            cursor = sqlcursor (_T("select kase_sto.a,  ")
_T("kase_sto.bon,  kase_sto.bon_kz,  kase_sto.bed,  kase_sto.bsd_kz,  ")
_T("kase_sto.dat,  kase_sto.dr_status,  kase_sto.fil,  kase_sto.lfd,  ")
_T("kase_sto.lad_akv,  kase_sto.me_ist,  kase_sto.mdn,  kase_sto.pers_nam,  ")
_T("kase_sto.prima,  kase_sto.pr_sto,  kase_sto.pr_ek,  kase_sto.pr_vk,  ")
_T("kase_sto.sto_me,  kase_sto.sto_pos_bws,  kase_sto.txt,  ")
_T("kase_sto.verk_st,  kase_sto.kase,  kase_sto.zeit from kase_sto ")

#line 18 "kase_sto.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and dat = ? ")
                                  _T("and zeit = ? ")
                                  _T("and bon = ? ")
                                  _T("and a = ? "));
    sqlin ((double *) &kase_sto.a,SQLDOUBLE,0);
    sqlin ((long *) &kase_sto.bon,SQLLONG,0);
    sqlin ((TCHAR *) kase_sto.bon_kz,SQLCHAR,2);
    sqlin ((short *) &kase_sto.bed,SQLSHORT,0);
    sqlin ((TCHAR *) kase_sto.bsd_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &kase_sto.dat,SQLDATE,0);
    sqlin ((TCHAR *) kase_sto.dr_status,SQLCHAR,2);
    sqlin ((short *) &kase_sto.fil,SQLSHORT,0);
    sqlin ((long *) &kase_sto.lfd,SQLLONG,0);
    sqlin ((TCHAR *) kase_sto.lad_akv,SQLCHAR,2);
    sqlin ((double *) &kase_sto.me_ist,SQLDOUBLE,0);
    sqlin ((short *) &kase_sto.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) kase_sto.pers_nam,SQLCHAR,9);
    sqlin ((long *) &kase_sto.prima,SQLLONG,0);
    sqlin ((double *) &kase_sto.pr_sto,SQLDOUBLE,0);
    sqlin ((double *) &kase_sto.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &kase_sto.pr_vk,SQLDOUBLE,0);
    sqlin ((double *) &kase_sto.sto_me,SQLDOUBLE,0);
    sqlin ((short *) &kase_sto.sto_pos_bws,SQLSHORT,0);
    sqlin ((TCHAR *) kase_sto.txt,SQLCHAR,61);
    sqlin ((short *) &kase_sto.verk_st,SQLSHORT,0);
    sqlin ((short *) &kase_sto.kase,SQLSHORT,0);
    sqlin ((TCHAR *) kase_sto.zeit,SQLCHAR,7);
            sqltext = _T("update kase_sto set kase_sto.a = ?,  ")
_T("kase_sto.bon = ?,  kase_sto.bon_kz = ?,  kase_sto.bed = ?,  ")
_T("kase_sto.bsd_kz = ?,  kase_sto.dat = ?,  kase_sto.dr_status = ?,  ")
_T("kase_sto.fil = ?,  kase_sto.lfd = ?,  kase_sto.lad_akv = ?,  ")
_T("kase_sto.me_ist = ?,  kase_sto.mdn = ?,  kase_sto.pers_nam = ?,  ")
_T("kase_sto.prima = ?,  kase_sto.pr_sto = ?,  kase_sto.pr_ek = ?,  ")
_T("kase_sto.pr_vk = ?,  kase_sto.sto_me = ?,  ")
_T("kase_sto.sto_pos_bws = ?,  kase_sto.txt = ?,  kase_sto.verk_st = ?,  ")
_T("kase_sto.kase = ?,  kase_sto.zeit = ? ")

#line 26 "kase_sto.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and dat = ? ")
                                  _T("and zeit = ? ")
                                  _T("and bon = ? ")
                                  _T("and a = ? ");
            sqlin ((short *)   &kase_sto.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sto.fil,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sto.verk_st,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &kase_sto.dat,  SQLDATE, 0);
            sqlin ((char *)    kase_sto.zeit,  SQLCHAR, sizeof (kase_sto.zeit));
            sqlin ((long *)   &kase_sto.bon,  SQLLONG, 0);
            sqlin ((double *) &kase_sto.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &kase_sto.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sto.fil,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sto.verk_st,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &kase_sto.dat,  SQLDATE, 0);
            sqlin ((char *)    kase_sto.zeit,  SQLCHAR, sizeof (kase_sto.zeit));
            sqlin ((long *)   &kase_sto.bon,  SQLLONG, 0);
            sqlin ((double *) &kase_sto.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select bon from kase_sto ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and dat = ? ")
                                  _T("and zeit = ? ")
                                  _T("and bon = ? ")
                                  _T("and a = ? "));
            sqlin ((short *)   &kase_sto.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sto.fil,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sto.verk_st,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &kase_sto.dat,  SQLDATE, 0);
            sqlin ((char *)    kase_sto.zeit,  SQLCHAR, sizeof (kase_sto.zeit));
            sqlin ((long *)   &kase_sto.bon,  SQLLONG, 0);
            sqlin ((double *) &kase_sto.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from kase_sto ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and dat = ? ")
                                  _T("and zeit = ? ")
                                  _T("and bon = ? ")
                                  _T("and a = ? "));
    sqlin ((double *) &kase_sto.a,SQLDOUBLE,0);
    sqlin ((long *) &kase_sto.bon,SQLLONG,0);
    sqlin ((TCHAR *) kase_sto.bon_kz,SQLCHAR,2);
    sqlin ((short *) &kase_sto.bed,SQLSHORT,0);
    sqlin ((TCHAR *) kase_sto.bsd_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &kase_sto.dat,SQLDATE,0);
    sqlin ((TCHAR *) kase_sto.dr_status,SQLCHAR,2);
    sqlin ((short *) &kase_sto.fil,SQLSHORT,0);
    sqlin ((long *) &kase_sto.lfd,SQLLONG,0);
    sqlin ((TCHAR *) kase_sto.lad_akv,SQLCHAR,2);
    sqlin ((double *) &kase_sto.me_ist,SQLDOUBLE,0);
    sqlin ((short *) &kase_sto.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) kase_sto.pers_nam,SQLCHAR,9);
    sqlin ((long *) &kase_sto.prima,SQLLONG,0);
    sqlin ((double *) &kase_sto.pr_sto,SQLDOUBLE,0);
    sqlin ((double *) &kase_sto.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &kase_sto.pr_vk,SQLDOUBLE,0);
    sqlin ((double *) &kase_sto.sto_me,SQLDOUBLE,0);
    sqlin ((short *) &kase_sto.sto_pos_bws,SQLSHORT,0);
    sqlin ((TCHAR *) kase_sto.txt,SQLCHAR,61);
    sqlin ((short *) &kase_sto.verk_st,SQLSHORT,0);
    sqlin ((short *) &kase_sto.kase,SQLSHORT,0);
    sqlin ((TCHAR *) kase_sto.zeit,SQLCHAR,7);
            ins_cursor = sqlcursor (_T("insert into kase_sto (")
_T("a,  bon,  bon_kz,  bed,  bsd_kz,  dat,  dr_status,  fil,  lfd,  lad_akv,  me_ist,  mdn,  ")
_T("pers_nam,  prima,  pr_sto,  pr_ek,  pr_vk,  sto_me,  sto_pos_bws,  txt,  verk_st,  kase,  ")
_T("zeit) ")

#line 73 "kase_sto.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 75 "kase_sto.rpp"
}
