#include "stdafx.h"
#include "waa_sum.h"

struct WAA_SUM waa_sum, waa_sum_null;

void WAA_SUM_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &waa_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.waa,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &waa_sum.dat,  SQLDATE, 0);
    sqlout ((short *) &waa_sum.abt,SQLSHORT,0);
    sqlout ((short *) &waa_sum.bed,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &waa_sum.dat,SQLDATE,0);
    sqlout ((short *) &waa_sum.fil,SQLSHORT,0);
    sqlout ((short *) &waa_sum.kun_anz_x,SQLSHORT,0);
    sqlout ((short *) &waa_sum.kun_anz_xx,SQLSHORT,0);
    sqlout ((double *) &waa_sum.kun_sum_x,SQLDOUBLE,0);
    sqlout ((double *) &waa_sum.kun_sum_xx,SQLDOUBLE,0);
    sqlout ((short *) &waa_sum.mdn,SQLSHORT,0);
    sqlout ((short *) &waa_sum.minus_pos,SQLSHORT,0);
    sqlout ((double *) &waa_sum.minus_sum,SQLDOUBLE,0);
    sqlout ((short *) &waa_sum.pos_gew,SQLSHORT,0);
    sqlout ((short *) &waa_sum.pos_hnd,SQLSHORT,0);
    sqlout ((short *) &waa_sum.pos_o_nr,SQLSHORT,0);
    sqlout ((short *) &waa_sum.sto_pos_bws,SQLSHORT,0);
    sqlout ((short *) &waa_sum.sto_pos_dir,SQLSHORT,0);
    sqlout ((double *) &waa_sum.sto_sum_bws,SQLDOUBLE,0);
    sqlout ((double *) &waa_sum.sto_sum_dir,SQLDOUBLE,0);
    sqlout ((double *) &waa_sum.ums_gew,SQLDOUBLE,0);
    sqlout ((double *) &waa_sum.ums_hnd,SQLDOUBLE,0);
    sqlout ((double *) &waa_sum.ums_o_nr,SQLDOUBLE,0);
    sqlout ((short *) &waa_sum.verk_st,SQLSHORT,0);
    sqlout ((short *) &waa_sum.waa,SQLSHORT,0);
    sqlout ((short *) &waa_sum.waa_z_zaehl,SQLSHORT,0);
    sqlout ((short *) &waa_sum.host_trans,SQLSHORT,0);
            cursor = sqlcursor (_T("select waa_sum.abt,  ")
_T("waa_sum.bed,  waa_sum.dat,  waa_sum.fil,  waa_sum.kun_anz_x,  ")
_T("waa_sum.kun_anz_xx,  waa_sum.kun_sum_x,  waa_sum.kun_sum_xx,  ")
_T("waa_sum.mdn,  waa_sum.minus_pos,  waa_sum.minus_sum,  waa_sum.pos_gew,  ")
_T("waa_sum.pos_hnd,  waa_sum.pos_o_nr,  waa_sum.sto_pos_bws,  ")
_T("waa_sum.sto_pos_dir,  waa_sum.sto_sum_bws,  waa_sum.sto_sum_dir,  ")
_T("waa_sum.ums_gew,  waa_sum.ums_hnd,  waa_sum.ums_o_nr,  ")
_T("waa_sum.verk_st,  waa_sum.waa,  waa_sum.waa_z_zaehl,  ")
_T("waa_sum.host_trans from waa_sum ")

#line 16 "waa_sum.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and waa = ? ")
                                  _T("and dat = ? "));
    sqlin ((short *) &waa_sum.abt,SQLSHORT,0);
    sqlin ((short *) &waa_sum.bed,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &waa_sum.dat,SQLDATE,0);
    sqlin ((short *) &waa_sum.fil,SQLSHORT,0);
    sqlin ((short *) &waa_sum.kun_anz_x,SQLSHORT,0);
    sqlin ((short *) &waa_sum.kun_anz_xx,SQLSHORT,0);
    sqlin ((double *) &waa_sum.kun_sum_x,SQLDOUBLE,0);
    sqlin ((double *) &waa_sum.kun_sum_xx,SQLDOUBLE,0);
    sqlin ((short *) &waa_sum.mdn,SQLSHORT,0);
    sqlin ((short *) &waa_sum.minus_pos,SQLSHORT,0);
    sqlin ((double *) &waa_sum.minus_sum,SQLDOUBLE,0);
    sqlin ((short *) &waa_sum.pos_gew,SQLSHORT,0);
    sqlin ((short *) &waa_sum.pos_hnd,SQLSHORT,0);
    sqlin ((short *) &waa_sum.pos_o_nr,SQLSHORT,0);
    sqlin ((short *) &waa_sum.sto_pos_bws,SQLSHORT,0);
    sqlin ((short *) &waa_sum.sto_pos_dir,SQLSHORT,0);
    sqlin ((double *) &waa_sum.sto_sum_bws,SQLDOUBLE,0);
    sqlin ((double *) &waa_sum.sto_sum_dir,SQLDOUBLE,0);
    sqlin ((double *) &waa_sum.ums_gew,SQLDOUBLE,0);
    sqlin ((double *) &waa_sum.ums_hnd,SQLDOUBLE,0);
    sqlin ((double *) &waa_sum.ums_o_nr,SQLDOUBLE,0);
    sqlin ((short *) &waa_sum.verk_st,SQLSHORT,0);
    sqlin ((short *) &waa_sum.waa,SQLSHORT,0);
    sqlin ((short *) &waa_sum.waa_z_zaehl,SQLSHORT,0);
    sqlin ((short *) &waa_sum.host_trans,SQLSHORT,0);
            sqltext = _T("update waa_sum set waa_sum.abt = ?,  ")
_T("waa_sum.bed = ?,  waa_sum.dat = ?,  waa_sum.fil = ?,  ")
_T("waa_sum.kun_anz_x = ?,  waa_sum.kun_anz_xx = ?,  ")
_T("waa_sum.kun_sum_x = ?,  waa_sum.kun_sum_xx = ?,  waa_sum.mdn = ?,  ")
_T("waa_sum.minus_pos = ?,  waa_sum.minus_sum = ?,  ")
_T("waa_sum.pos_gew = ?,  waa_sum.pos_hnd = ?,  waa_sum.pos_o_nr = ?,  ")
_T("waa_sum.sto_pos_bws = ?,  waa_sum.sto_pos_dir = ?,  ")
_T("waa_sum.sto_sum_bws = ?,  waa_sum.sto_sum_dir = ?,  ")
_T("waa_sum.ums_gew = ?,  waa_sum.ums_hnd = ?,  waa_sum.ums_o_nr = ?,  ")
_T("waa_sum.verk_st = ?,  waa_sum.waa = ?,  waa_sum.waa_z_zaehl = ?,  ")
_T("waa_sum.host_trans = ? ")

#line 22 "waa_sum.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and waa = ? ")
                                  _T("and dat = ? ");
            sqlin ((short *)   &waa_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.waa,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &waa_sum.dat,  SQLDATE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &waa_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.waa,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &waa_sum.dat,  SQLDATE, 0);
            test_upd_cursor = sqlcursor (_T("select waa from waa_sum ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and waa = ? ")
                                  _T("and dat = ? "));
            sqlin ((short *)   &waa_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sum.waa,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &waa_sum.dat,  SQLDATE, 0);
            del_cursor = sqlcursor (_T("delete from waa_sum ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and waa = ? ")
                                  _T("and dat = ? "));
    sqlin ((short *) &waa_sum.abt,SQLSHORT,0);
    sqlin ((short *) &waa_sum.bed,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &waa_sum.dat,SQLDATE,0);
    sqlin ((short *) &waa_sum.fil,SQLSHORT,0);
    sqlin ((short *) &waa_sum.kun_anz_x,SQLSHORT,0);
    sqlin ((short *) &waa_sum.kun_anz_xx,SQLSHORT,0);
    sqlin ((double *) &waa_sum.kun_sum_x,SQLDOUBLE,0);
    sqlin ((double *) &waa_sum.kun_sum_xx,SQLDOUBLE,0);
    sqlin ((short *) &waa_sum.mdn,SQLSHORT,0);
    sqlin ((short *) &waa_sum.minus_pos,SQLSHORT,0);
    sqlin ((double *) &waa_sum.minus_sum,SQLDOUBLE,0);
    sqlin ((short *) &waa_sum.pos_gew,SQLSHORT,0);
    sqlin ((short *) &waa_sum.pos_hnd,SQLSHORT,0);
    sqlin ((short *) &waa_sum.pos_o_nr,SQLSHORT,0);
    sqlin ((short *) &waa_sum.sto_pos_bws,SQLSHORT,0);
    sqlin ((short *) &waa_sum.sto_pos_dir,SQLSHORT,0);
    sqlin ((double *) &waa_sum.sto_sum_bws,SQLDOUBLE,0);
    sqlin ((double *) &waa_sum.sto_sum_dir,SQLDOUBLE,0);
    sqlin ((double *) &waa_sum.ums_gew,SQLDOUBLE,0);
    sqlin ((double *) &waa_sum.ums_hnd,SQLDOUBLE,0);
    sqlin ((double *) &waa_sum.ums_o_nr,SQLDOUBLE,0);
    sqlin ((short *) &waa_sum.verk_st,SQLSHORT,0);
    sqlin ((short *) &waa_sum.waa,SQLSHORT,0);
    sqlin ((short *) &waa_sum.waa_z_zaehl,SQLSHORT,0);
    sqlin ((short *) &waa_sum.host_trans,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into waa_sum (")
_T("abt,  bed,  dat,  fil,  kun_anz_x,  kun_anz_xx,  kun_sum_x,  kun_sum_xx,  mdn,  ")
_T("minus_pos,  minus_sum,  pos_gew,  pos_hnd,  pos_o_nr,  sto_pos_bws,  ")
_T("sto_pos_dir,  sto_sum_bws,  sto_sum_dir,  ums_gew,  ums_hnd,  ums_o_nr,  ")
_T("verk_st,  waa,  waa_z_zaehl,  host_trans) ")

#line 57 "waa_sum.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 59 "waa_sum.rpp"
}
