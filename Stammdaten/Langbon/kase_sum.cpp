#include "stdafx.h"
#include "kase_sum.h"

struct KASE_SUM kase_sum, kase_sum_null;

void KASE_SUM_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &kase_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.kase,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &kase_sum.dat,  SQLDATE, 0);
    sqlout ((double *) &kase_sum.ausgaben,SQLDOUBLE,0);
    sqlout ((short *) &kase_sum.bon_anz,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &kase_sum.dat,SQLDATE,0);
    sqlout ((short *) &kase_sum.fil,SQLSHORT,0);
    sqlout ((double *) &kase_sum.gra_tot_bto,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.gra_tot_nto,SQLDOUBLE,0);
    sqlout ((short *) &kase_sum.kase,SQLSHORT,0);
    sqlout ((double *) &kase_sum.kase_ab_am_expr,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_ab_bar,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_ab_chq,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_ab_diners,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_ab_euro_ca,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_ab_gut,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_ab_sonst,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_am_expr,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_bar,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_chq,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_diners,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_euro_ca,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_gut,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_sonst,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_visa,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_ab_visa,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_soll,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kredit_wrt,SQLDOUBLE,0);
    sqlout ((short *) &kase_sum.kun_anz,SQLSHORT,0);
    sqlout ((short *) &kase_sum.mdn,SQLSHORT,0);
    sqlout ((short *) &kase_sum.minus_pos,SQLSHORT,0);
    sqlout ((double *) &kase_sum.minus_sum,SQLDOUBLE,0);
    sqlout ((short *) &kase_sum.nullbon_anz,SQLSHORT,0);
    sqlout ((double *) &kase_sum.pfa_wrt,SQLDOUBLE,0);
    sqlout ((short *) &kase_sum.pos_ges,SQLSHORT,0);
    sqlout ((double *) &kase_sum.rab_nach,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.rech_bez,SQLDOUBLE,0);
    sqlout ((short *) &kase_sum.sto_pos_bon,SQLSHORT,0);
    sqlout ((short *) &kase_sum.sto_pos_dir,SQLSHORT,0);
    sqlout ((double *) &kase_sum.sto_sum_bon,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.sto_sum_dir,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.ums_ges,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.ums_mwst1,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.ums_mwst2,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.ums_mwst3,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.ums_mwst4,SQLDOUBLE,0);
    sqlout ((short *) &kase_sum.verk_st,SQLSHORT,0);
    sqlout ((double *) &kase_sum.wechsg,SQLDOUBLE,0);
    sqlout ((long *) &kase_sum.z_zaehl,SQLLONG,0);
    sqlout ((double *) &kase_sum.kase_ec_cash,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_ec_glb,SQLDOUBLE,0);
    sqlout ((long *) &kase_sum.kase_eccash_pos,SQLLONG,0);
    sqlout ((long *) &kase_sum.kase_ecglb_pos,SQLLONG,0);
    sqlout ((double *) &kase_sum.abr_sum_bon,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.abr_pos_bon,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.einzahlung,SQLDOUBLE,0);
    sqlout ((double *) &kase_sum.kase_ist,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select kase_sum.ausgaben,  ")
_T("kase_sum.bon_anz,  kase_sum.dat,  kase_sum.fil,  kase_sum.gra_tot_bto,  ")
_T("kase_sum.gra_tot_nto,  kase_sum.kase,  kase_sum.kase_ab_am_expr,  ")
_T("kase_sum.kase_ab_bar,  kase_sum.kase_ab_chq,  ")
_T("kase_sum.kase_ab_diners,  kase_sum.kase_ab_euro_ca,  ")
_T("kase_sum.kase_ab_gut,  kase_sum.kase_ab_sonst,  ")
_T("kase_sum.kase_am_expr,  kase_sum.kase_bar,  kase_sum.kase_chq,  ")
_T("kase_sum.kase_diners,  kase_sum.kase_euro_ca,  kase_sum.kase_gut,  ")
_T("kase_sum.kase_sonst,  kase_sum.kase_visa,  kase_sum.kase_ab_visa,  ")
_T("kase_sum.kase_soll,  kase_sum.kredit_wrt,  kase_sum.kun_anz,  ")
_T("kase_sum.mdn,  kase_sum.minus_pos,  kase_sum.minus_sum,  ")
_T("kase_sum.nullbon_anz,  kase_sum.pfa_wrt,  kase_sum.pos_ges,  ")
_T("kase_sum.rab_nach,  kase_sum.rech_bez,  kase_sum.sto_pos_bon,  ")
_T("kase_sum.sto_pos_dir,  kase_sum.sto_sum_bon,  kase_sum.sto_sum_dir,  ")
_T("kase_sum.ums_ges,  kase_sum.ums_mwst1,  kase_sum.ums_mwst2,  ")
_T("kase_sum.ums_mwst3,  kase_sum.ums_mwst4,  kase_sum.verk_st,  ")
_T("kase_sum.wechsg,  kase_sum.z_zaehl,  kase_sum.kase_ec_cash,  ")
_T("kase_sum.kase_ec_glb,  kase_sum.kase_eccash_pos,  ")
_T("kase_sum.kase_ecglb_pos,  kase_sum.abr_sum_bon,  ")
_T("kase_sum.abr_pos_bon,  kase_sum.einzahlung,  kase_sum.kase_ist from kase_sum ")

#line 16 "kase_sum.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and kase = ? ")
                                  _T("and dat = ? "));
    sqlin ((double *) &kase_sum.ausgaben,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.bon_anz,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &kase_sum.dat,SQLDATE,0);
    sqlin ((short *) &kase_sum.fil,SQLSHORT,0);
    sqlin ((double *) &kase_sum.gra_tot_bto,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.gra_tot_nto,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.kase,SQLSHORT,0);
    sqlin ((double *) &kase_sum.kase_ab_am_expr,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_bar,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_chq,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_diners,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_euro_ca,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_gut,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_sonst,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_am_expr,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_bar,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_chq,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_diners,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_euro_ca,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_gut,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_sonst,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_visa,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_visa,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_soll,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kredit_wrt,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.kun_anz,SQLSHORT,0);
    sqlin ((short *) &kase_sum.mdn,SQLSHORT,0);
    sqlin ((short *) &kase_sum.minus_pos,SQLSHORT,0);
    sqlin ((double *) &kase_sum.minus_sum,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.nullbon_anz,SQLSHORT,0);
    sqlin ((double *) &kase_sum.pfa_wrt,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.pos_ges,SQLSHORT,0);
    sqlin ((double *) &kase_sum.rab_nach,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.rech_bez,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.sto_pos_bon,SQLSHORT,0);
    sqlin ((short *) &kase_sum.sto_pos_dir,SQLSHORT,0);
    sqlin ((double *) &kase_sum.sto_sum_bon,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.sto_sum_dir,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.ums_ges,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.ums_mwst1,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.ums_mwst2,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.ums_mwst3,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.ums_mwst4,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.verk_st,SQLSHORT,0);
    sqlin ((double *) &kase_sum.wechsg,SQLDOUBLE,0);
    sqlin ((long *) &kase_sum.z_zaehl,SQLLONG,0);
    sqlin ((double *) &kase_sum.kase_ec_cash,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ec_glb,SQLDOUBLE,0);
    sqlin ((long *) &kase_sum.kase_eccash_pos,SQLLONG,0);
    sqlin ((long *) &kase_sum.kase_ecglb_pos,SQLLONG,0);
    sqlin ((double *) &kase_sum.abr_sum_bon,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.abr_pos_bon,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.einzahlung,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ist,SQLDOUBLE,0);
            sqltext = _T("update kase_sum set ")
_T("kase_sum.ausgaben = ?,  kase_sum.bon_anz = ?,  kase_sum.dat = ?,  ")
_T("kase_sum.fil = ?,  kase_sum.gra_tot_bto = ?,  ")
_T("kase_sum.gra_tot_nto = ?,  kase_sum.kase = ?,  ")
_T("kase_sum.kase_ab_am_expr = ?,  kase_sum.kase_ab_bar = ?,  ")
_T("kase_sum.kase_ab_chq = ?,  kase_sum.kase_ab_diners = ?,  ")
_T("kase_sum.kase_ab_euro_ca = ?,  kase_sum.kase_ab_gut = ?,  ")
_T("kase_sum.kase_ab_sonst = ?,  kase_sum.kase_am_expr = ?,  ")
_T("kase_sum.kase_bar = ?,  kase_sum.kase_chq = ?,  ")
_T("kase_sum.kase_diners = ?,  kase_sum.kase_euro_ca = ?,  ")
_T("kase_sum.kase_gut = ?,  kase_sum.kase_sonst = ?,  ")
_T("kase_sum.kase_visa = ?,  kase_sum.kase_ab_visa = ?,  ")
_T("kase_sum.kase_soll = ?,  kase_sum.kredit_wrt = ?,  ")
_T("kase_sum.kun_anz = ?,  kase_sum.mdn = ?,  kase_sum.minus_pos = ?,  ")
_T("kase_sum.minus_sum = ?,  kase_sum.nullbon_anz = ?,  ")
_T("kase_sum.pfa_wrt = ?,  kase_sum.pos_ges = ?,  ")
_T("kase_sum.rab_nach = ?,  kase_sum.rech_bez = ?,  ")
_T("kase_sum.sto_pos_bon = ?,  kase_sum.sto_pos_dir = ?,  ")
_T("kase_sum.sto_sum_bon = ?,  kase_sum.sto_sum_dir = ?,  ")
_T("kase_sum.ums_ges = ?,  kase_sum.ums_mwst1 = ?,  ")
_T("kase_sum.ums_mwst2 = ?,  kase_sum.ums_mwst3 = ?,  ")
_T("kase_sum.ums_mwst4 = ?,  kase_sum.verk_st = ?,  ")
_T("kase_sum.wechsg = ?,  kase_sum.z_zaehl = ?,  ")
_T("kase_sum.kase_ec_cash = ?,  kase_sum.kase_ec_glb = ?,  ")
_T("kase_sum.kase_eccash_pos = ?,  kase_sum.kase_ecglb_pos = ?,  ")
_T("kase_sum.abr_sum_bon = ?,  kase_sum.abr_pos_bon = ?,  ")
_T("kase_sum.einzahlung = ?,  kase_sum.kase_ist = ? ")

#line 22 "kase_sum.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and kase = ? ")
                                  _T("and dat = ? ");
            sqlin ((short *)   &kase_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.kase,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &kase_sum.dat,  SQLDATE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &kase_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.kase,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &kase_sum.dat,  SQLDATE, 0);
            test_upd_cursor = sqlcursor (_T("select kase from kase_sum ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and kase = ? ")
                                  _T("and dat = ? "));
            sqlin ((short *)   &kase_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &kase_sum.kase,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &kase_sum.dat,  SQLDATE, 0);
            del_cursor = sqlcursor (_T("delete from kase_sum ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and kase = ? ")
                                  _T("and dat = ? "));
    sqlin ((double *) &kase_sum.ausgaben,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.bon_anz,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &kase_sum.dat,SQLDATE,0);
    sqlin ((short *) &kase_sum.fil,SQLSHORT,0);
    sqlin ((double *) &kase_sum.gra_tot_bto,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.gra_tot_nto,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.kase,SQLSHORT,0);
    sqlin ((double *) &kase_sum.kase_ab_am_expr,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_bar,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_chq,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_diners,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_euro_ca,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_gut,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_sonst,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_am_expr,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_bar,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_chq,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_diners,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_euro_ca,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_gut,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_sonst,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_visa,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ab_visa,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_soll,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kredit_wrt,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.kun_anz,SQLSHORT,0);
    sqlin ((short *) &kase_sum.mdn,SQLSHORT,0);
    sqlin ((short *) &kase_sum.minus_pos,SQLSHORT,0);
    sqlin ((double *) &kase_sum.minus_sum,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.nullbon_anz,SQLSHORT,0);
    sqlin ((double *) &kase_sum.pfa_wrt,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.pos_ges,SQLSHORT,0);
    sqlin ((double *) &kase_sum.rab_nach,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.rech_bez,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.sto_pos_bon,SQLSHORT,0);
    sqlin ((short *) &kase_sum.sto_pos_dir,SQLSHORT,0);
    sqlin ((double *) &kase_sum.sto_sum_bon,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.sto_sum_dir,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.ums_ges,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.ums_mwst1,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.ums_mwst2,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.ums_mwst3,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.ums_mwst4,SQLDOUBLE,0);
    sqlin ((short *) &kase_sum.verk_st,SQLSHORT,0);
    sqlin ((double *) &kase_sum.wechsg,SQLDOUBLE,0);
    sqlin ((long *) &kase_sum.z_zaehl,SQLLONG,0);
    sqlin ((double *) &kase_sum.kase_ec_cash,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ec_glb,SQLDOUBLE,0);
    sqlin ((long *) &kase_sum.kase_eccash_pos,SQLLONG,0);
    sqlin ((long *) &kase_sum.kase_ecglb_pos,SQLLONG,0);
    sqlin ((double *) &kase_sum.abr_sum_bon,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.abr_pos_bon,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.einzahlung,SQLDOUBLE,0);
    sqlin ((double *) &kase_sum.kase_ist,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into kase_sum (")
_T("ausgaben,  bon_anz,  dat,  fil,  gra_tot_bto,  gra_tot_nto,  kase,  ")
_T("kase_ab_am_expr,  kase_ab_bar,  kase_ab_chq,  kase_ab_diners,  ")
_T("kase_ab_euro_ca,  kase_ab_gut,  kase_ab_sonst,  kase_am_expr,  kase_bar,  ")
_T("kase_chq,  kase_diners,  kase_euro_ca,  kase_gut,  kase_sonst,  kase_visa,  ")
_T("kase_ab_visa,  kase_soll,  kredit_wrt,  kun_anz,  mdn,  minus_pos,  minus_sum,  ")
_T("nullbon_anz,  pfa_wrt,  pos_ges,  rab_nach,  rech_bez,  sto_pos_bon,  ")
_T("sto_pos_dir,  sto_sum_bon,  sto_sum_dir,  ums_ges,  ums_mwst1,  ums_mwst2,  ")
_T("ums_mwst3,  ums_mwst4,  verk_st,  wechsg,  z_zaehl,  kase_ec_cash,  ")
_T("kase_ec_glb,  kase_eccash_pos,  kase_ecglb_pos,  abr_sum_bon,  ")
_T("abr_pos_bon,  einzahlung,  kase_ist) ")

#line 57 "kase_sum.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 59 "kase_sum.rpp"
}
