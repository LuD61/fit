/*
 * Copyright (c) 2004 Wilhelm Roth, 
 * Company  SETEC-GMBH Inc. All  Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * -Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright
 *  notice, this list of conditions and the following disclaimer in
 *  the documentation and/or other materials provided with the distribution.
 * 
 * Neither the name of SETEC-GMBH, Inc. or the names of contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING
 * ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL NOT
 * BE LIABLE FOR ANY DAMAGES OR LIABILITIES SUFFERED BY LICENSEE AS A RESULT
 * OF OR RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST
 * REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL,
 * INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY
 * OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE SOFTWARE, EVEN
 * IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or intended for
 * use in the design, construction, operation or maintenance of any nuclear
 * facility.
 */
#include "stdafx.h"
#include "DbTime.h"
#include "Token.h"

#define   NEU   (short)1
#define   ALT   (short)2

short DbTime::mon_tab[12]={0,31,28,31,30,31,30,31,31,30,31,30};


DayTime::DayTime ()
{
	Create ("00:00:00");
}

DayTime::DayTime (char *t)
{
	     Create (t);
}

DayTime::DayTime (CString *t)
{
	     Create (t);
}

DayTime *DayTime::Create (char *t)
{
	     CString txt (t);
	     Create (&txt);
		 return this;
}

DayTime *DayTime::Create (CString *t)
{
	     char hour[] = {"00"};
	     char min[]  = {"00"};
	     char sec[]  = {"00"};
		 Hour = 0;
		 Min = 0;
		 Sec = 0;
         Time = *t;
		 BOOL pTime = FALSE;
		 if (Time.Find (":") != -1)
		 {
			 pTime = TRUE;
		 }
		 int len = t->GetLength ();
		 if (pTime)
		 {
			 int pos = 0;
			 CString c = Time.Tokenize (":", pos);
			 if (c != "")
			 {
				 Hour = atoi (c.GetBuffer ());
			     c = Time.Tokenize (":", pos);
			 }
			 if (c != "")
			 {
				 Min = atoi (c.GetBuffer ());
			     c = Time.Tokenize (":", pos);
			 }
			 if (c != "")
			 {
				 Sec = atoi (c.GetBuffer ());
			 }
		 }
		 else
		 {
			if (len >=2)
			{
				 memcpy (hour, Time.Mid (0, 2), 2);
			}
			if (len >=5)
			{
				 memcpy (min, Time.Mid (3, 2), 2);
			}
			if (len >=8)
			{
				 memcpy (sec, Time.Mid (6, 2), 2);
			 }
			 Hour = atoi (hour);
			 Min = atoi (min);
			 Sec = atoi (sec);
	  	 }
		 lTime = Hour * 3600 + 
			     Min  * 60 +
				 Sec;
		 return this;
}

DayTime *DayTime::Create (CString *t, LPTSTR format)
{
	     char hour[] = {"00"};
	     char min[]  = {"00"};
	     char sec[]  = {"00"};
		 Hour = 0;
		 Min = 0;
		 Sec = 0;
         Time = *t;
		 BOOL pTime = FALSE;
		 if (Time.Find (":") != -1)
		 {
			 pTime = TRUE;
		 }
		 int len = t->GetLength ();
		 if (pTime)
		 {
			 int pos = 0;
			 CString c = Time.Tokenize (":", pos);
			 if (c != "")
			 {
				 Hour = atoi (c.GetBuffer ());
			     c = Time.Tokenize (":", pos);
			 }
			 if (c != "")
			 {
				 Min = atoi (c.GetBuffer ());
			     c = Time.Tokenize (":", pos);
			 }
			 if (c != "")
			 {
				 Sec = atoi (c.GetBuffer ());
			 }
		 }
		 else
		 {
			if (len >=2)
			{
				 memcpy (hour, Time.Mid (0, 2), 2);
			}
			if (len >=5)
			{
				 memcpy (min, Time.Mid (3, 2), 2);
			}
			if (len >=8)
			{
				 memcpy (sec, Time.Mid (6, 2), 2);
			 }
			 Hour = atoi (hour);
			 Min = atoi (min);
			 Sec = atoi (sec);
	  	 }
		 lTime = Hour * 3600 + 
			     Min  * 60 +
				 Sec;
		 CString Format = format;
		 if (Format == "%2h:%2m:%2s")
		 {
			 Time.Format ("%02hd:%02hd:%02hd", Hour, Min, Sec);
		 }
		 else if (Format == "%2h:%2m")
		 {
			 Time.Format ("%02hd:%02hd", Hour, Min);
		 }
		 else if (Format == "%2h.%2m.%2s")
		 {
			 Time.Format ("%02hd.%02hd.%02hd", Hour, Min, Sec);
		 }
		 else if (Format == "%2h.%2m")
		 {
			 Time.Format ("%02hd.%02hd", Hour, Min);
		 }
		 else if (Format == "%2h%2m%2s")
		 {
			 Time.Format ("%02hd%02hd%02hd", Hour, Min, Sec);
		 }
		 else if (Format == "%2h%2m")
		 {
			 Time.Format ("%02hd%02hd", Hour, Min);
		 }
		 return this;
}

long DayTime::GetLongTime ()
{
	     return lTime;
}

CString *DayTime::GetTextTime ()
{
	     return &Time;
}


DbTime::DbTime (DATE_STRUCT *ds)
{
	     memcpy (&this->ds, ds, sizeof (ds));  
	     st.wYear  = ds->year;
	     st.wMonth = ds->month;
	     st.wDay   = ds->day;
	     st.wHour  = 0;
	     st.wMinute = 0;
		 st.wSecond = 0;
		 st.wMilliseconds = 0;
		 tDate.Format (_T("%02hd.%02hd.%04hd"), st.wDay, st.wMonth, st.wYear);
	     SystemTimeToFileTime (&st, &ft);
	     FileTimeToSystemTime (&ft, &st);
         memcpy (&ul, &ft, sizeof (ft));
		 ldat = (time_t) (ul.QuadPart / (1000 * 3600 * 24));
		 ldat /= 10000;
}

time_t DbTime::GetTime ()
{
	     return ldat;
}

int DbTime::GetWeekDay ()
{
	     return st.wDayOfWeek;
}

CString* DbTime::GetStringTime ()
{
	     return &tDate;
}

void DbTime::SetDateStruct ()
{
	     ds.year  = st.wYear;
	     ds.month = st.wMonth;
	     ds.day   = st.wDay;
}

DATE_STRUCT *DbTime::GetDateStruct (DATE_STRUCT *d)
{
	     memcpy (d, &ds, sizeof (ds));
         return d;
}

void DbTime::SetTime (time_t t)
{
	     ldat = t;
		 ldat *= 10000;
		 ul.QuadPart = ldat;
		 ul.QuadPart *= (1000 * 3600 * 24);
         memcpy (&ft, &ul,  sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%04hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
}

#ifdef WIN32
DbTime& DbTime::operator+ (int d)
{
	     time_t t1 = GetTime ();
         t1 += d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return *this;
}

DbTime& DbTime::operator- (int d)
{
	     time_t t1 = GetTime ();
         t1 -= d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%04hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return *this;
}
#endif

DbTime* DbTime::Add (int d)
{
	     time_t t1 = GetTime ();
         t1 += d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%04hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return this;
}

DbTime* DbTime::Sub (int d)
{
	     time_t t1 = GetTime ();
         t1 -= d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%04hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return this;
}


short DbTime::GetYear()
{
	LPTSTR datum = tDate.GetBuffer ();
	char  dat[16];
	strcpy(dat,datum);
    return((short)(atoi(&dat[6])));
}

short DbTime::GetMonth()
{
	char  dat[16];
	LPTSTR datum = tDate.GetBuffer ();
	strcpy(dat,datum);
	dat[5]='\0';
    return((short)(atoi(&dat[3])));
}

short DbTime::GetDay()
{
	char  dat[16];
	LPTSTR datum = tDate.GetBuffer ();
	strcpy(dat,datum);
	dat[2]='\0';
	return((short)(atoi(dat)));
}

short DbTime::GetYearDay()
{
	short i,l_tag=0,tag,mon;
	LPTSTR datum = tDate.GetBuffer ();
	mon=DbTime::GetMonth(datum);
	tag=DbTime::GetDay(datum);
	for(i=1;i<mon;i++)
    {
		l_tag += mon_tab[i];
    }
	if(mon > 2)
	{
		 l_tag += tag + DbTime::GetSchaltJahr(DbTime::GetYear(datum));
	}
	else
	{
		 l_tag += tag;
	}
	return(l_tag);
}

short DbTime::GetSchaltJahr(int jahr)   /* jahr 2-Stellig */
{
	short i;
	i=jahr%4;
	if(i == 0) 
		return(1);
	else
		return(0);
}

short DbTime::GetWeek()
{
	LPTSTR datum = tDate.GetBuffer ();
	return GetWeek (datum);
}

short DbTime::GetWeek(LPTSTR datum)
{
	short woche;
	short jahr;
	char  buff[16];

	woche = WeekPre(datum);
	if(woche == 0) /* Korrektur bei Anfang des Jahres */
	{
		jahr = GetYear(datum);
		if(jahr == 0)
		{
			jahr = 99;
		}
		else
		{
			jahr--;
		}
		sprintf(buff,"31.12.%02d",jahr);
        woche = WeekPre(buff);
	}
	return(woche);
}

short DbTime::WeekPre(LPTSTR datum)
{
	short woche,jahr,rest;
	short korr_neu;
	short l_tag;

	l_tag = GetYearDay(datum);
	jahr = GetYear(datum);
	korr_neu = WeekKorr(jahr,NEU);
	woche = (short)(l_tag / 7);
	rest  = (short)(l_tag % 7);

	if(rest == 0)
	{
		woche--;
		rest = 7;
	}
	if(korr_neu <= 4)
		woche++;
	if(korr_neu > 1)
		if(rest >= ((short)9 - korr_neu))
			woche++;
	if(woche == 53)
	{
		if(WeekKorr(jahr,ALT) <= 3)
		woche = 1;
	}
	if(woche == 0)
	{
	}
	return(woche);
}

short DbTime::WeekKorr(short jahr_ein,short par)
{
	short jahr;
//	short l_tag;
	short wochentag;
	short i;
	int   rest=0;
	int   tage=0;

	jahr = jahr_ein;
	if(par == ALT) /* Ende des Jahres */
		jahr++;
	if(jahr < 90 )
		jahr += 100;

	for(i=90;i<jahr;i++)
	{
		rest = (int)(i % 4);
		if(rest != 0)
			tage += 365;
		else
			tage += 366;
	}
	if(par == NEU) /* Anfang des Jahres */
		tage++;

	wochentag = (short)(tage % 7);
	if(wochentag == 0)
		return(7);

	return(wochentag);
}

long DbTime::FirstKwDay (short kw, short jr)
{
     char datum [11];
     long date;
     long daplus;
     short wo;

     if (kw == 1)
     {
               sprintf (datum, "31.12.%hd", jr - 1);
               date = GetDays (datum) + 1;
     }
     else
     {
               sprintf (datum, "01.01.%hd", jr);
               date = GetDays (datum);
               daplus = (long) (kw - 1) * 7;
               date = date + daplus - (long) 10;
     }
     FromDays (date, datum); 
     wo = GetWeek (datum); 
     while (wo != kw)
     {
               date ++;
               FromDays (date, datum); 
               wo = GetWeek (datum); 
     }
     return (date);
}
	 
long DbTime::LastKwDay (short kw, short jr)
{
     long date;

     date = FirstKwDay (kw, jr) + 6;
     return (date);
}

int DbTime::FirstKwDayC (short kw, short jr, LPTSTR datum)
{
     long date;

     date = FirstKwDay (kw, jr);
     FromDays (date, datum);
     return (0);
}
        
int DbTime::LastKwDayC (short kw, short jr, LPTSTR datum)
/**
1.Tag von Kalenderwoche holen. Datum als String
**/
{
     long date;

     date = LastKwDay (kw, jr);
     FromDays (date, datum);
     return (0);
}

long DbTime::GetDays (DATE_STRUCT *ds)
{
	DbTime Time (ds);
	return (long) Time.GetTime ();
}

long DbTime::GetDays (LPSTR date)
{
     CToken t;
	 t.SetSep (".");
	 t = date;
	 if (t.GetAnzToken () < 3)
	 {
		 return 0l;
	 }
	 DATE_STRUCT ds;
	 ds.day   = atoi (t.GetToken (0));
	 ds.month = atoi (t.GetToken (1));
	 ds.year  = atoi (t.GetToken (2));
	 return GetDays (&ds);
}

void DbTime::FromDays (long ldat, LPTSTR date)
{
    SYSTEMTIME st;
    FILETIME ft;
    ULARGE_INTEGER ul; 

	ldat *= 10000;
	ul.QuadPart = ldat * (1000 * 3600 * 24);
    memcpy (&ft, &ul, sizeof (ft));
    SystemTimeToFileTime (&st, &ft);
	CString Date;
	Date.Format ("%02hd.%02hd.%04hd", st.wDay, st.wMonth, st.wYear);
	strcpy (date, Date.GetBuffer ());
}

short DbTime::GetYear(LPTSTR datum)
{
	char  dat[16];
	strcpy(dat,datum);
    return((short)(atoi(&dat[6])));
}

short DbTime::GetMonth(LPTSTR datum)
{
	char  dat[16];
	strcpy(dat,datum);
	dat[5]='\0';
    return((short)(atoi(&dat[3])));
}

short DbTime::GetDay(LPTSTR datum)
{
	char  dat[16];
	strcpy(dat,datum);
	dat[2]='\0';
	return((short)(atoi(dat)));
}

short DbTime::GetYearDay(LPTSTR datum)
{
	short i,l_tag=0,tag,mon;
	mon=DbTime::GetMonth(datum);
	tag=DbTime::GetDay(datum);
	for(i=1;i<mon;i++)
    {
		l_tag += mon_tab[i];
    }
	if(mon > 2)
	{
		 l_tag += tag + DbTime::GetSchaltJahr(DbTime::GetYear(datum));
	}
	else
	{
		 l_tag += tag;
	}
	return(l_tag);
}
