#include "stdafx.h"
#include "waa_sto.h"

struct WAA_STO waa_sto, waa_sto_null;

void WAA_STO_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &waa_sto.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sto.fil,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sto.verk_st,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &waa_sto.dat,  SQLDATE, 0);
            sqlin ((char *)    waa_sto.zeit,  SQLCHAR, sizeof (waa_sto.zeit));
            sqlin ((long *)   &waa_sto.bon,  SQLLONG, 0);
            sqlin ((double *) &waa_sto.a,  SQLDOUBLE, 0);
    sqlout ((double *) &waa_sto.a,SQLDOUBLE,0);
    sqlout ((long *) &waa_sto.bon,SQLLONG,0);
    sqlout ((TCHAR *) waa_sto.bon_kz,SQLCHAR,2);
    sqlout ((short *) &waa_sto.bed,SQLSHORT,0);
    sqlout ((TCHAR *) waa_sto.bsd_kz,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &waa_sto.dat,SQLDATE,0);
    sqlout ((TCHAR *) waa_sto.dr_status,SQLCHAR,2);
    sqlout ((short *) &waa_sto.fil,SQLSHORT,0);
    sqlout ((long *) &waa_sto.lfd,SQLLONG,0);
    sqlout ((double *) &waa_sto.me_ist,SQLDOUBLE,0);
    sqlout ((short *) &waa_sto.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) waa_sto.pers_nam,SQLCHAR,9);
    sqlout ((long *) &waa_sto.prima,SQLLONG,0);
    sqlout ((double *) &waa_sto.pr_sto,SQLDOUBLE,0);
    sqlout ((double *) &waa_sto.pr_vk,SQLDOUBLE,0);
    sqlout ((double *) &waa_sto.sto_me,SQLDOUBLE,0);
    sqlout ((short *) &waa_sto.sto_pos_bws,SQLSHORT,0);
    sqlout ((TCHAR *) waa_sto.txt,SQLCHAR,61);
    sqlout ((short *) &waa_sto.verk_st,SQLSHORT,0);
    sqlout ((short *) &waa_sto.waa,SQLSHORT,0);
    sqlout ((TCHAR *) waa_sto.zeit,SQLCHAR,7);
    sqlout ((TCHAR *) waa_sto.lad_akv,SQLCHAR,2);
    sqlout ((double *) &waa_sto.pr_ek,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select waa_sto.a,  ")
_T("waa_sto.bon,  waa_sto.bon_kz,  waa_sto.bed,  waa_sto.bsd_kz,  ")
_T("waa_sto.dat,  waa_sto.dr_status,  waa_sto.fil,  waa_sto.lfd,  ")
_T("waa_sto.me_ist,  waa_sto.mdn,  waa_sto.pers_nam,  waa_sto.prima,  ")
_T("waa_sto.pr_sto,  waa_sto.pr_vk,  waa_sto.sto_me,  waa_sto.sto_pos_bws,  ")
_T("waa_sto.txt,  waa_sto.verk_st,  waa_sto.waa,  waa_sto.zeit,  ")
_T("waa_sto.lad_akv,  waa_sto.pr_ek from waa_sto ")

#line 18 "waa_sto.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and dat = ? ")
                                  _T("and zeit = ? ")
                                  _T("and bon = ? ")
                                  _T("and a = ? "));
    sqlin ((double *) &waa_sto.a,SQLDOUBLE,0);
    sqlin ((long *) &waa_sto.bon,SQLLONG,0);
    sqlin ((TCHAR *) waa_sto.bon_kz,SQLCHAR,2);
    sqlin ((short *) &waa_sto.bed,SQLSHORT,0);
    sqlin ((TCHAR *) waa_sto.bsd_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &waa_sto.dat,SQLDATE,0);
    sqlin ((TCHAR *) waa_sto.dr_status,SQLCHAR,2);
    sqlin ((short *) &waa_sto.fil,SQLSHORT,0);
    sqlin ((long *) &waa_sto.lfd,SQLLONG,0);
    sqlin ((double *) &waa_sto.me_ist,SQLDOUBLE,0);
    sqlin ((short *) &waa_sto.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) waa_sto.pers_nam,SQLCHAR,9);
    sqlin ((long *) &waa_sto.prima,SQLLONG,0);
    sqlin ((double *) &waa_sto.pr_sto,SQLDOUBLE,0);
    sqlin ((double *) &waa_sto.pr_vk,SQLDOUBLE,0);
    sqlin ((double *) &waa_sto.sto_me,SQLDOUBLE,0);
    sqlin ((short *) &waa_sto.sto_pos_bws,SQLSHORT,0);
    sqlin ((TCHAR *) waa_sto.txt,SQLCHAR,61);
    sqlin ((short *) &waa_sto.verk_st,SQLSHORT,0);
    sqlin ((short *) &waa_sto.waa,SQLSHORT,0);
    sqlin ((TCHAR *) waa_sto.zeit,SQLCHAR,7);
    sqlin ((TCHAR *) waa_sto.lad_akv,SQLCHAR,2);
    sqlin ((double *) &waa_sto.pr_ek,SQLDOUBLE,0);
            sqltext = _T("update waa_sto set waa_sto.a = ?,  ")
_T("waa_sto.bon = ?,  waa_sto.bon_kz = ?,  waa_sto.bed = ?,  ")
_T("waa_sto.bsd_kz = ?,  waa_sto.dat = ?,  waa_sto.dr_status = ?,  ")
_T("waa_sto.fil = ?,  waa_sto.lfd = ?,  waa_sto.me_ist = ?,  ")
_T("waa_sto.mdn = ?,  waa_sto.pers_nam = ?,  waa_sto.prima = ?,  ")
_T("waa_sto.pr_sto = ?,  waa_sto.pr_vk = ?,  waa_sto.sto_me = ?,  ")
_T("waa_sto.sto_pos_bws = ?,  waa_sto.txt = ?,  waa_sto.verk_st = ?,  ")
_T("waa_sto.waa = ?,  waa_sto.zeit = ?,  waa_sto.lad_akv = ?,  ")
_T("waa_sto.pr_ek = ? ")

#line 26 "waa_sto.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and dat = ? ")
                                  _T("and zeit = ? ")
                                  _T("and bon = ? ")
                                  _T("and a = ? ");
            sqlin ((short *)   &waa_sto.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sto.fil,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sto.verk_st,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &waa_sto.dat,  SQLDATE, 0);
            sqlin ((char *)    waa_sto.zeit,  SQLCHAR, sizeof (waa_sto.zeit));
            sqlin ((long *)   &waa_sto.bon,  SQLLONG, 0);
            sqlin ((double *) &waa_sto.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &waa_sto.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sto.fil,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sto.verk_st,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &waa_sto.dat,  SQLDATE, 0);
            sqlin ((char *)    waa_sto.zeit,  SQLCHAR, sizeof (waa_sto.zeit));
            sqlin ((long *)   &waa_sto.bon,  SQLLONG, 0);
            sqlin ((double *) &waa_sto.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select bon from waa_sto ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and dat = ? ")
                                  _T("and zeit = ? ")
                                  _T("and bon = ? ")
                                  _T("and a = ? "));
            sqlin ((short *)   &waa_sto.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sto.fil,  SQLSHORT, 0);
            sqlin ((short *)   &waa_sto.verk_st,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &waa_sto.dat,  SQLDATE, 0);
            sqlin ((char *)    waa_sto.zeit,  SQLCHAR, sizeof (waa_sto.zeit));
            sqlin ((long *)   &waa_sto.bon,  SQLLONG, 0);
            sqlin ((double *) &waa_sto.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from waa_sto ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and dat = ? ")
                                  _T("and zeit = ? ")
                                  _T("and bon = ? ")
                                  _T("and a = ? "));
    sqlin ((double *) &waa_sto.a,SQLDOUBLE,0);
    sqlin ((long *) &waa_sto.bon,SQLLONG,0);
    sqlin ((TCHAR *) waa_sto.bon_kz,SQLCHAR,2);
    sqlin ((short *) &waa_sto.bed,SQLSHORT,0);
    sqlin ((TCHAR *) waa_sto.bsd_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &waa_sto.dat,SQLDATE,0);
    sqlin ((TCHAR *) waa_sto.dr_status,SQLCHAR,2);
    sqlin ((short *) &waa_sto.fil,SQLSHORT,0);
    sqlin ((long *) &waa_sto.lfd,SQLLONG,0);
    sqlin ((double *) &waa_sto.me_ist,SQLDOUBLE,0);
    sqlin ((short *) &waa_sto.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) waa_sto.pers_nam,SQLCHAR,9);
    sqlin ((long *) &waa_sto.prima,SQLLONG,0);
    sqlin ((double *) &waa_sto.pr_sto,SQLDOUBLE,0);
    sqlin ((double *) &waa_sto.pr_vk,SQLDOUBLE,0);
    sqlin ((double *) &waa_sto.sto_me,SQLDOUBLE,0);
    sqlin ((short *) &waa_sto.sto_pos_bws,SQLSHORT,0);
    sqlin ((TCHAR *) waa_sto.txt,SQLCHAR,61);
    sqlin ((short *) &waa_sto.verk_st,SQLSHORT,0);
    sqlin ((short *) &waa_sto.waa,SQLSHORT,0);
    sqlin ((TCHAR *) waa_sto.zeit,SQLCHAR,7);
    sqlin ((TCHAR *) waa_sto.lad_akv,SQLCHAR,2);
    sqlin ((double *) &waa_sto.pr_ek,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into waa_sto (")
_T("a,  bon,  bon_kz,  bed,  bsd_kz,  dat,  dr_status,  fil,  lfd,  me_ist,  mdn,  pers_nam,  prima,  ")
_T("pr_sto,  pr_vk,  sto_me,  sto_pos_bws,  txt,  verk_st,  waa,  zeit,  lad_akv,  pr_ek) ")

#line 73 "waa_sto.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 75 "waa_sto.rpp"
}
