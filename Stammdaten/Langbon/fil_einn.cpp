#include "stdafx.h"
#include "fil_einn.h"

struct FIL_EINN fil_einn, fil_einn_null;

void FIL_EINN_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &fil_einn.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &fil_einn.fil,  SQLSHORT, 0);
            sqlin ((short *)   &fil_einn.kase,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &fil_einn.einn_dat,  SQLDATE, 0);
    sqlout ((double *) &fil_einn.einn,SQLDOUBLE,0);
    sqlout ((TCHAR *) fil_einn.einn_bem,SQLCHAR,13);
    sqlout ((DATE_STRUCT *) &fil_einn.einn_dat,SQLDATE,0);
    sqlout ((double *) &fil_einn.einn_nto,SQLDOUBLE,0);
    sqlout ((short *) &fil_einn.fil,SQLSHORT,0);
    sqlout ((double *) &fil_einn.fil_std,SQLDOUBLE,0);
    sqlout ((short *) &fil_einn.kase,SQLSHORT,0);
    sqlout ((double *) &fil_einn.kred,SQLDOUBLE,0);
    sqlout ((long *) &fil_einn.kun_anz_int,SQLLONG,0);
    sqlout ((short *) &fil_einn.mdn,SQLSHORT,0);
    sqlout ((double *) &fil_einn.pers_rab_wrt,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.pers_pkt,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &fil_einn.buch_dat,SQLDATE,0);
    sqlout ((long *) &fil_einn.buch_lauf,SQLLONG,0);
    sqlout ((double *) &fil_einn.kase_bar,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.rech_bez,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ausgaben,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ums_mwst1,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ums_mwst2,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ums_mwst3,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ums_mwst4,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ums_mwst1_net,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ums_mwst2_net,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ums_mwst3_net,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ums_mwst4_net,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.mwst1_proz,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.mwst2_proz,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.mwst3_proz,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.mwst4_proz,SQLDOUBLE,0);
    sqlout ((short *) &fil_einn.buch_kz,SQLSHORT,0);
    sqlout ((double *) &fil_einn.kase_ist,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.wsw_ist,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.markt_ums,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ein_sto_bon,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.ein_sto_dir,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.wechsg_anf,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.wechsg,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.sto_mwst1,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.sto_mwst2,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.sto_mwst1_net,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.sto_mwst2_net,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.kase_gut,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.kase_chk,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.bank_bar,SQLDOUBLE,0);
    sqlout ((double *) &fil_einn.bank_unbar,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select fil_einn.einn,  ")
_T("fil_einn.einn_bem,  fil_einn.einn_dat,  fil_einn.einn_nto,  ")
_T("fil_einn.fil,  fil_einn.fil_std,  fil_einn.kase,  fil_einn.kred,  ")
_T("fil_einn.kun_anz_int,  fil_einn.mdn,  fil_einn.pers_rab_wrt,  ")
_T("fil_einn.pers_pkt,  fil_einn.buch_dat,  fil_einn.buch_lauf,  ")
_T("fil_einn.kase_bar,  fil_einn.rech_bez,  fil_einn.ausgaben,  ")
_T("fil_einn.ums_mwst1,  fil_einn.ums_mwst2,  fil_einn.ums_mwst3,  ")
_T("fil_einn.ums_mwst4,  fil_einn.ums_mwst1_net,  ")
_T("fil_einn.ums_mwst2_net,  fil_einn.ums_mwst3_net,  ")
_T("fil_einn.ums_mwst4_net,  fil_einn.mwst1_proz,  fil_einn.mwst2_proz,  ")
_T("fil_einn.mwst3_proz,  fil_einn.mwst4_proz,  fil_einn.buch_kz,  ")
_T("fil_einn.kase_ist,  fil_einn.wsw_ist,  fil_einn.markt_ums,  ")
_T("fil_einn.ein_sto_bon,  fil_einn.ein_sto_dir,  fil_einn.wechsg_anf,  ")
_T("fil_einn.wechsg,  fil_einn.sto_mwst1,  fil_einn.sto_mwst2,  ")
_T("fil_einn.sto_mwst1_net,  fil_einn.sto_mwst2_net,  ")
_T("fil_einn.kase_gut,  fil_einn.kase_chk,  fil_einn.bank_bar,  ")
_T("fil_einn.bank_unbar from fil_einn ")

#line 15 "fil_einn.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kase = ? ")
                                  _T("and einn_dat = ? "));
    sqlin ((double *) &fil_einn.einn,SQLDOUBLE,0);
    sqlin ((TCHAR *) fil_einn.einn_bem,SQLCHAR,13);
    sqlin ((DATE_STRUCT *) &fil_einn.einn_dat,SQLDATE,0);
    sqlin ((double *) &fil_einn.einn_nto,SQLDOUBLE,0);
    sqlin ((short *) &fil_einn.fil,SQLSHORT,0);
    sqlin ((double *) &fil_einn.fil_std,SQLDOUBLE,0);
    sqlin ((short *) &fil_einn.kase,SQLSHORT,0);
    sqlin ((double *) &fil_einn.kred,SQLDOUBLE,0);
    sqlin ((long *) &fil_einn.kun_anz_int,SQLLONG,0);
    sqlin ((short *) &fil_einn.mdn,SQLSHORT,0);
    sqlin ((double *) &fil_einn.pers_rab_wrt,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.pers_pkt,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &fil_einn.buch_dat,SQLDATE,0);
    sqlin ((long *) &fil_einn.buch_lauf,SQLLONG,0);
    sqlin ((double *) &fil_einn.kase_bar,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.rech_bez,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ausgaben,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst1,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst2,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst3,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst4,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst1_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst2_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst3_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst4_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.mwst1_proz,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.mwst2_proz,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.mwst3_proz,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.mwst4_proz,SQLDOUBLE,0);
    sqlin ((short *) &fil_einn.buch_kz,SQLSHORT,0);
    sqlin ((double *) &fil_einn.kase_ist,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.wsw_ist,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.markt_ums,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ein_sto_bon,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ein_sto_dir,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.wechsg_anf,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.wechsg,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.sto_mwst1,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.sto_mwst2,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.sto_mwst1_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.sto_mwst2_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.kase_gut,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.kase_chk,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.bank_bar,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.bank_unbar,SQLDOUBLE,0);
            sqltext = _T("update fil_einn set ")
_T("fil_einn.einn = ?,  fil_einn.einn_bem = ?,  fil_einn.einn_dat = ?,  ")
_T("fil_einn.einn_nto = ?,  fil_einn.fil = ?,  fil_einn.fil_std = ?,  ")
_T("fil_einn.kase = ?,  fil_einn.kred = ?,  fil_einn.kun_anz_int = ?,  ")
_T("fil_einn.mdn = ?,  fil_einn.pers_rab_wrt = ?,  ")
_T("fil_einn.pers_pkt = ?,  fil_einn.buch_dat = ?,  ")
_T("fil_einn.buch_lauf = ?,  fil_einn.kase_bar = ?,  ")
_T("fil_einn.rech_bez = ?,  fil_einn.ausgaben = ?,  ")
_T("fil_einn.ums_mwst1 = ?,  fil_einn.ums_mwst2 = ?,  ")
_T("fil_einn.ums_mwst3 = ?,  fil_einn.ums_mwst4 = ?,  ")
_T("fil_einn.ums_mwst1_net = ?,  fil_einn.ums_mwst2_net = ?,  ")
_T("fil_einn.ums_mwst3_net = ?,  fil_einn.ums_mwst4_net = ?,  ")
_T("fil_einn.mwst1_proz = ?,  fil_einn.mwst2_proz = ?,  ")
_T("fil_einn.mwst3_proz = ?,  fil_einn.mwst4_proz = ?,  ")
_T("fil_einn.buch_kz = ?,  fil_einn.kase_ist = ?,  ")
_T("fil_einn.wsw_ist = ?,  fil_einn.markt_ums = ?,  ")
_T("fil_einn.ein_sto_bon = ?,  fil_einn.ein_sto_dir = ?,  ")
_T("fil_einn.wechsg_anf = ?,  fil_einn.wechsg = ?,  ")
_T("fil_einn.sto_mwst1 = ?,  fil_einn.sto_mwst2 = ?,  ")
_T("fil_einn.sto_mwst1_net = ?,  fil_einn.sto_mwst2_net = ?,  ")
_T("fil_einn.kase_gut = ?,  fil_einn.kase_chk = ?,  ")
_T("fil_einn.bank_bar = ?,  fil_einn.bank_unbar = ? ")

#line 20 "fil_einn.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kase = ? ")
                                  _T("and einn_dat = ? ");
            sqlin ((short *)   &fil_einn.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &fil_einn.fil,  SQLSHORT, 0);
            sqlin ((short *)   &fil_einn.kase,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &fil_einn.einn_dat,  SQLDATE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &fil_einn.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &fil_einn.fil,  SQLSHORT, 0);
            sqlin ((short *)   &fil_einn.kase,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &fil_einn.einn_dat,  SQLDATE, 0);
            test_upd_cursor = sqlcursor (_T("select kase from fil_einn ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kase = ? ")
                                  _T("and einn_dat = ? "));
            sqlin ((short *)   &fil_einn.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &fil_einn.fil,  SQLSHORT, 0);
            sqlin ((short *)   &fil_einn.kase,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &fil_einn.einn_dat,  SQLDATE, 0);
            del_cursor = sqlcursor (_T("delete from fil_einn ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kase = ? ")
                                  _T("and einn_dat = ? "));
    sqlin ((double *) &fil_einn.einn,SQLDOUBLE,0);
    sqlin ((TCHAR *) fil_einn.einn_bem,SQLCHAR,13);
    sqlin ((DATE_STRUCT *) &fil_einn.einn_dat,SQLDATE,0);
    sqlin ((double *) &fil_einn.einn_nto,SQLDOUBLE,0);
    sqlin ((short *) &fil_einn.fil,SQLSHORT,0);
    sqlin ((double *) &fil_einn.fil_std,SQLDOUBLE,0);
    sqlin ((short *) &fil_einn.kase,SQLSHORT,0);
    sqlin ((double *) &fil_einn.kred,SQLDOUBLE,0);
    sqlin ((long *) &fil_einn.kun_anz_int,SQLLONG,0);
    sqlin ((short *) &fil_einn.mdn,SQLSHORT,0);
    sqlin ((double *) &fil_einn.pers_rab_wrt,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.pers_pkt,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &fil_einn.buch_dat,SQLDATE,0);
    sqlin ((long *) &fil_einn.buch_lauf,SQLLONG,0);
    sqlin ((double *) &fil_einn.kase_bar,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.rech_bez,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ausgaben,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst1,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst2,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst3,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst4,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst1_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst2_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst3_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ums_mwst4_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.mwst1_proz,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.mwst2_proz,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.mwst3_proz,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.mwst4_proz,SQLDOUBLE,0);
    sqlin ((short *) &fil_einn.buch_kz,SQLSHORT,0);
    sqlin ((double *) &fil_einn.kase_ist,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.wsw_ist,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.markt_ums,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ein_sto_bon,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.ein_sto_dir,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.wechsg_anf,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.wechsg,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.sto_mwst1,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.sto_mwst2,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.sto_mwst1_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.sto_mwst2_net,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.kase_gut,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.kase_chk,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.bank_bar,SQLDOUBLE,0);
    sqlin ((double *) &fil_einn.bank_unbar,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into fil_einn (")
_T("einn,  einn_bem,  einn_dat,  einn_nto,  fil,  fil_std,  kase,  kred,  kun_anz_int,  mdn,  ")
_T("pers_rab_wrt,  pers_pkt,  buch_dat,  buch_lauf,  kase_bar,  rech_bez,  ")
_T("ausgaben,  ums_mwst1,  ums_mwst2,  ums_mwst3,  ums_mwst4,  ums_mwst1_net,  ")
_T("ums_mwst2_net,  ums_mwst3_net,  ums_mwst4_net,  mwst1_proz,  mwst2_proz,  ")
_T("mwst3_proz,  mwst4_proz,  buch_kz,  kase_ist,  wsw_ist,  markt_ums,  ")
_T("ein_sto_bon,  ein_sto_dir,  wechsg_anf,  wechsg,  sto_mwst1,  sto_mwst2,  ")
_T("sto_mwst1_net,  sto_mwst2_net,  kase_gut,  kase_chk,  bank_bar,  bank_unbar) ")

#line 49 "fil_einn.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?)")); 

#line 51 "fil_einn.rpp"
}
