#pragma once

class CFillList
{
protected :
	CListCtrl *listView;
public:
	CFillList(void);
	CFillList(CListCtrl *);
	~CFillList(void);
	CFillList& operator= (CListCtrl&);
	void SetListView (CListCtrl *);
    BOOL SetCol (char *, int, int, int);
    BOOL SetCol (char *, int, int);
    int InsertItem (int, int);
    BOOL SetItemImage (int, int);
    BOOL SetItemImage (int, int, int);
    int GetItemImage (int);
    BOOL SetItemText (char *, int, int);
    DWORD SetStyle (DWORD);
    DWORD SetExtendedStyle (DWORD);
};
