// LangbonDoc.cpp : Implementierung der Klasse CLangbonDoc
//

#include "stdafx.h"
#include "Langbon.h"

#include "LangbonDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLangbonDoc

IMPLEMENT_DYNCREATE(CLangbonDoc, CDocument)

BEGIN_MESSAGE_MAP(CLangbonDoc, CDocument)
END_MESSAGE_MAP()


// CLangbonDoc Erstellung/Zerst�rung

CLangbonDoc::CLangbonDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CLangbonDoc::~CLangbonDoc()
{
}

BOOL CLangbonDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CLangbonDoc Serialisierung

void CLangbonDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CLangbonDoc Diagnose

#ifdef _DEBUG
void CLangbonDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLangbonDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CLangbonDoc-Befehle
