#pragma once

class CSplitPane :
	public CStatic
{
protected:
	DECLARE_MESSAGE_MAP()
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

	CWnd *View1;
	CWnd *View2;
	int Direction;
	int FirstPart;
	int x;
	int y;
	int cx;
	int cy;
	int cx1;
	int cy1;
	int cx2;
	int cy2;
	int size;
	int startx;
	int starty;
	int maxX;
	int maxY;
	int m_lx;
	int m_ly;
	HCURSOR horizontalCursor;
	HCURSOR verticalCursor;
	BOOL m_bIsPressed;
	CPoint point;
	CRect lRect;

public:
	enum
	{
		Horizontal = 1,
		Vertical = 2,
	} DIRECTION;
	CSplitPane(void);
	~CSplitPane(void);
	DECLARE_DYNCREATE(CSplitPane)
	BOOL Create (CWnd *Parent, int x, int y, 
				CWnd *View1, CWnd *View2, int FirstPart, int Direction);
	//{{AFX_MSG(CSplitterControl)
	afx_msg void OnSize( UINT nType, int cx,  int cy);
	afx_msg void OnMove(int x,  int y);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	void SetView1 (CWnd *View1);
	void SetView2 (CWnd *View2);
	void MoveWindow(int x,int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	void MoveWindow(LPRECT rect, BOOL bRepaint = TRUE);
    void SetNewSplit ();
	void DrawLine ();
    void PrintLine (CDC *cDC, int x, int y);
};
