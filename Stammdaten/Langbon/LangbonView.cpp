// LangbonView.cpp : Implementierung der Klasse CLangbonView
//

#include "stdafx.h"
#include "Langbon.h"

#include "LangbonDoc.h"
#include "LangbonView.h"
#include "langbonview.h"
#include "langbonview.h"
#include "UniFormField.h"
#include "langbonview.h"
#include "DbTime.h"
#include "langbonview.h"
#include "StrFuncs.h"
#include "langbonview.h"
#include "StornoDialog.h"
#include "langbonview.h"
#include "MainFrm.h"
#include "Token.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLangbonView

IMPLEMENT_DYNCREATE(CLangbonView, DbFormView)

int CLangbonView::StoMode = Storno;

void CLangbonView::DoDataExchange(CDataExchange* pDX)
{
	DbFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SYS, m_Sys);
	DDX_Control(pDX, IDC_LSYS, m_LSys);
	DDX_Control(pDX, IDC_LDAT_VON, m_LDatVon);
	DDX_Control(pDX, IDC_DATE_VON, m_DatVon);
	DDX_Control(pDX, IDC_LDAT_BIS, m_LDatBis);
	DDX_Control(pDX, IDC_DATE_BIS, m_DatBis);
	DDX_Control(pDX, IDC_LIST1, m_LbList);
	DDX_Control(pDX, IDC_SYSNAME, m_SysName);
	DDX_Control(pDX, IDC_BON, m_Bon);
	DDX_Control(pDX, IDC_LBON, m_LBon);
	DDX_Control(pDX, IDC_LTIME_FROM, m_LTimeFrom);
	DDX_Control(pDX, IDC_TIME_FROM, m_TimeFrom);
	DDX_Control(pDX, IDC_LTIME_TO, m_LTimeTo);
	DDX_Control(pDX, IDC_TIME_TO, m_TimeTo);
	DDX_Control(pDX, IDC_BORDER, m_Border);
	DDX_Control(pDX, IDC_BED, m_Bed);
	DDX_Control(pDX, IDC_LBED, m_LBed);
	DDX_Control(pDX, IDC_BEDNAME, m_BedName);
	DDX_Control(pDX, IDC_POSLIST, m_PosList);
	DDX_Control(pDX, IDC_LWAA, m_LWaa);
	DDX_Control(pDX, IDC_WAA, m_Waa);
}

BEGIN_MESSAGE_MAP(CLangbonView, DbFormView)
	// Standarddruckbefehle
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
	ON_BN_CLICKED(IDC_SYSCHOICE , OnSyschoice)
	ON_BN_CLICKED(IDC_BEDCHOICE , OnBedchoice)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_COMMAND(ID_BACK, OnBack)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, OnLvnItemchangedList1)
	ON_COMMAND(ID_STORNO, OnStorno)
	ON_COMMAND(ID_PRINT, OnPrint)
	ON_COMMAND(ID_BONVIEW, OnViewbon)
END_MESSAGE_MAP()

// CLangbonView Erstellung/Zerst�rung

CLangbonView::CLangbonView()
			: DbFormView (CLangbonView::IDD)
{
	// TODO: Hier Code zum Erstellen einf�gen
	LbVerbuch = TRUE;
	ModalChoice = TRUE;
	Choice = NULL;
	ModalChoiceBed = TRUE;
	ChoiceBed = NULL;
	Sys_peri.opendbase ("bws");
	strcpy (sys_par.sys_par_nam, "lb_verbuch");
	if (Sys_par.dbreadfirst () == 0)
	{
		LbVerbuch = _tstoi (sys_par.sys_par_wrt);
	}
	memcpy (&Sys_peri.sys_peri, &sys_peri_null, sizeof (SYS_PERI));
	memcpy (&Adr.adr, &adr_null, sizeof (ADR));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&Lb.lb, &lb_null, sizeof (LB));
	memcpy (&Bed.bed, &bed_null, sizeof (BED));
	Bon = "";
	sDatVon = "";
	sDatBis = "";
	sTimeFrom = "";
	sTimeTo = "";
	sBed = "";
	sWaa = "";
	PosTab.Init ();
	bonBrush = NULL;
	BonViewActive = FALSE;
	UseWaaSto = FALSE;
}

CLangbonView::~CLangbonView()
{
	if (bonBrush != NULL)
	{
		DeleteObject (bonBrush);
	}
	Sys_peri.dbclose ();
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	if (ChoiceBed != NULL)
	{
		delete ChoiceBed;
		ChoiceBed = NULL;
	}
	Lb.sqlclose (BedCursor);
	Lb.sqlclose (PosCursor);
	memcpy (&Sys_peri.sys_peri, &sys_peri_null, sizeof (SYS_PERI));
	PosTab.DestroyAll ();
}

BOOL CLangbonView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CView::PreCreateWindow(cs);
}

// CLangbonView-Zeichnung

void CLangbonView::OnDraw(CDC* /*pDC*/)
{
	CLangbonDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: Code zum Zeichnen der systemeigenen Daten hinzuf�gen
}


// CLangbonView drucken

BOOL CLangbonView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void CLangbonView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void CLangbonView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// CLangbonView Diagnose

#ifdef _DEBUG
void CLangbonView::AssertValid() const
{
	CView::AssertValid();
}

void CLangbonView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CLangbonDoc* CLangbonView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLangbonDoc)));
	return (CLangbonDoc*)m_pDocument;
}
#endif //_DEBUG


// CLangbonView Meldungshandler

void CLangbonView::OnInitialUpdate ()
{
	DbFormView::OnInitialUpdate ();

	CString CommandLine = GetCommandLine ();
	CToken t (CommandLine, _T(" "));
	if (t.GetAnzToken () > 2)
	{
		StoMode = Test;
	}
	CRect brect;
	brect.left = 10;
	brect.top = 10;
	brect.right = 100;
	brect.bottom = 100;
	m_BonView.CreateEx (WS_EX_CLIENTEDGE, ES_MULTILINE | WS_CHILD | WS_TABSTOP | 
		ES_READONLY 
		| ES_AUTOVSCROLL | ES_AUTOHSCROLL | WS_HSCROLL | WS_VSCROLL, 
		brect, this, IDC_BONVIEW);
//	m_BonView.ModifyStyleEx (0, WS_EX_CLIENTEDGE);
	m_BonView.SetBackgroundColor (FALSE, RGB (255, 255, 185));
	m_BonView.SetFont (GetFont ());

	SplitPane.Create (this, 200, 20, &m_LbList, &m_PosList, 50, CSplitPane::Vertical);
	FillList = &m_LbList;
	FillList.SetStyle (LVS_REPORT);
	FillList.SetExtendedStyle (LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
//	m_PosList.VLines = 1;

	FillList.SetCol ("", 0, 0);
	FillList.SetCol ("Datum", 0, 100);
	FillList.SetCol ("Zeit", 1, 100);
	FillList.SetCol ("Bon-Nr  ", 2, 80, LVCFMT_RIGHT);
	FillList.SetCol ("  Bediener", 3, 150);
	FillList.SetCol ("  Kasse", 4, 80, LVCFMT_RIGHT);
	FillList.SetCol ("  Umsatz", 5, 80, LVCFMT_RIGHT);

	FillPos = &m_PosList;
	FillPos.SetStyle (LVS_REPORT);
	FillPos.SetExtendedStyle (LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
//	m_PosList.VLines = 1;

	FillPos.SetCol ("", 0, 0);
	FillPos.SetCol ("Artikel  ",   0, 100, LVCFMT_RIGHT);
	FillPos.SetCol ("Bezeichnung", 1, 150);
	FillPos.SetCol ("Waage",       2, 80, LVCFMT_RIGHT);
	FillPos.SetCol ("Sto.",        3, 40, LVCFMT_RIGHT);
	m_PosList.AddAttribute (3, CAttribute::CheckBox);
	FillPos.SetCol ("Umsatz   ",   4, 80, LVCFMT_RIGHT);
	FillPos.SetCol ("Menge  ",     5, 80, LVCFMT_RIGHT);
	FillPos.SetCol ("Posten  ",    6,  70, LVCFMT_RIGHT);
	FillPos.SetCol ("MWST",        7,  70);
	FillPos.SetCol (" MWST Umsatz",8,  80, LVCFMT_RIGHT);
	FillPos.SetCol ("Sto Umsatz   ",   9, 80, LVCFMT_RIGHT);
	FillPos.SetCol ("Sto Menge  ",    10, 80, LVCFMT_RIGHT);
	FillPos.SetCol ("Sto Posten  ",   11,  70, LVCFMT_RIGHT);
	FillPos.SetCol ("Sto MWST",       12,  80, LVCFMT_RIGHT);

	SysGrid.Create (this, 1, 2);
    SysGrid.SetBorder (0, 0);
    SysGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Sys = new CCtrlInfo (&m_Sys, 0, 0, 1, 1);
	SysGrid.Add (c_Sys);
	CtrlGrid.CreateChoiceButton (m_SysChoice, IDC_SYSCHOICE, this);
	CCtrlInfo *c_SysChoice = new CCtrlInfo (&m_SysChoice, 1, 0, 1, 1);
	SysGrid.Add (c_SysChoice);

	BedGrid.Create (this, 1, 2);
    BedGrid.SetBorder (0, 0);
    BedGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Bed = new CCtrlInfo (&m_Bed, 0, 0, 1, 1);
	BedGrid.Add (c_Bed);
	CtrlGrid.CreateChoiceButton (m_BedChoice, IDC_BEDCHOICE, this);
	CCtrlInfo *c_BedChoice = new CCtrlInfo (&m_BedChoice, 1, 0, 1, 1);
	BedGrid.Add (c_BedChoice);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (8, 8);

	CCtrlInfo *c_LSys = new CCtrlInfo (&m_LSys, 1, 1, 1, 1);
	CtrlGrid.Add (c_LSys);

	CCtrlInfo *c_SysGrid = new CCtrlInfo (&SysGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_SysGrid);

	CCtrlInfo *c_SysName = new CCtrlInfo (&m_SysName, 1, 2, 2, 1);
	CtrlGrid.Add (c_SysName);

//	CCtrlInfo *c_Border = new CCtrlInfo (&m_Border, 0, 3, 3, DOCKBOTTOM);
	CCtrlInfo *c_Border = new CCtrlInfo (&m_Border, 0, 3, 3, 11);
	CtrlGrid.Add (c_Border);
	CtrlGrid.SetItemCharHeight (&m_Border, 12);

	CCtrlInfo *c_LBon = new CCtrlInfo (&m_LBon, 1, 4, 1, 1);
	CtrlGrid.Add (c_LBon);
	CCtrlInfo *c_Bon = new CCtrlInfo (&m_Bon, 2, 4, 1, 1);
	CtrlGrid.Add (c_Bon);

	CCtrlInfo *c_LDatVon = new CCtrlInfo (&m_LDatVon, 1, 6, 1, 1);
	CtrlGrid.Add (c_LDatVon);
	CCtrlInfo *c_DatVon = new CCtrlInfo (&m_DatVon, 2, 6, 1, 1);
	CtrlGrid.Add (c_DatVon);

	CCtrlInfo *c_LDatBis = new CCtrlInfo (&m_LDatBis, 1, 7, 1, 1);
	CtrlGrid.Add (c_LDatBis);
	CCtrlInfo *c_DatBis = new CCtrlInfo (&m_DatBis, 2, 7, 1, 1);
	CtrlGrid.Add (c_DatBis);

	CCtrlInfo *c_LTimeFrom = new CCtrlInfo (&m_LTimeFrom, 1, 8, 1, 1);
	CtrlGrid.Add (c_LTimeFrom);
	CCtrlInfo *c_TimeFrom = new CCtrlInfo (&m_TimeFrom, 2, 8, 1, 1);
	CtrlGrid.Add (c_TimeFrom);

	CCtrlInfo *c_LTimeTo = new CCtrlInfo (&m_LTimeTo, 1, 9, 1, 1);
	CtrlGrid.Add (c_LTimeTo);
	CCtrlInfo *c_TimeTo = new CCtrlInfo (&m_TimeTo, 2, 9, 1, 1);
	CtrlGrid.Add (c_TimeTo);

	CCtrlInfo *c_LBed = new CCtrlInfo (&m_LBed, 1, 11, 1, 1);
	CtrlGrid.Add (c_LBed);
//	CCtrlInfo *c_BedGrid = new CCtrlInfo (&BedGrid, 2, 11, 1, 1);
	CCtrlInfo *c_BedGrid = new CCtrlInfo (&BedGrid, 2, 11, 1, 1);
	CtrlGrid.Add (c_BedGrid);
	CCtrlInfo *c_BedName = new CCtrlInfo (&m_BedName, 1, 12, 2, 1);
	CtrlGrid.Add (c_BedName);

	CCtrlInfo *c_LWaa = new CCtrlInfo (&m_LWaa, 1, 13, 1, 1);
	CtrlGrid.Add (c_LWaa);
	CCtrlInfo *c_Waa = new CCtrlInfo (&m_Waa, 2, 13, 1, 1);
	CtrlGrid.Add (c_Waa);

/*
	CCtrlInfo *c_LbList = new CCtrlInfo (&m_LbList, 4, 8, DOCKRIGHT, DOCKBOTTOM);
    c_LbList->rightspace = 30; 
	CtrlGrid.Add (c_LbList);

	CCtrlInfo *c_PosList = new CCtrlInfo (&m_PosList, 4, 1, DOCKRIGHT, 6);
    c_PosList->rightspace = 30; 
	CtrlGrid.Add (c_PosList);
*/


	CCtrlInfo *c_PosList = new CCtrlInfo (&SplitPane, 4, 1, DOCKRIGHT, DOCKBOTTOM);
    c_PosList->rightspace = 30; 
	CtrlGrid.Add (c_PosList);

	Form.Add (new CFormField (&m_Sys,EDIT,       (long *)   &Sys_peri.sys_peri.sys, VLONG));
	Form.Add (new CUniFormField (&m_SysName,EDIT,   (char *)   Adr.adr.adr_krz, VCHAR));

	Form.Add (new CFormField (&m_Bon, EDIT,      (CString *)&Bon,     VSTRING));
	Form.Add (new CFormField (&m_DatVon, EDIT,   (CString *)&sDatVon, VSTRING));
	Form.Add (new CFormField (&m_DatBis, EDIT,   (CString *)&sDatBis, VSTRING));
	Form.Add (new CFormField (&m_TimeFrom, EDIT, (CString *)&sTimeFrom, VSTRING));
	Form.Add (new CFormField (&m_TimeTo,  EDIT,  (CString *)&sTimeTo, VSTRING));
	Form.Add (new CFormField (&m_Bed,     EDIT,  (CString *) &sBed, VSTRING));
	Form.Add (new CFormField (&m_BedName,  EDIT,  (char *)  Bed.bed.bed_nam, VCHAR));
	Form.Add (new CFormField (&m_Waa,      EDIT,  (CString *) &sWaa, VSTRING));

	Bed.sqlin ((short *) Bed.bed.mdn, SQLSHORT, sizeof (Bed.bed.bed_nam));
	Bed.sqlin ((short *) Bed.bed.fil, SQLSHORT, sizeof (Bed.bed.bed_nam));
	Bed.sqlin ((short *) Bed.bed.verk_st, SQLSHORT, sizeof (Bed.bed.bed_nam));
	Bed.sqlin ((char *)  Bed.bed.bed_nam, SQLCHAR, sizeof (Bed.bed.bed_nam));
	BedCursor = Bed.sqlcursor (_T("select bed from bed ")
							   _T("where mdn = ? ")
							   _T("and fil = ? ")
							   _T("and verk_st = ? ")
							   _T("and bed_nam matches ?"));

	Lb.sqlin ((long *)       &Lb.lb.sys, SQLLONG, 0);
	Lb.sqlin ((DATE_STRUCT*) &Lb.lb.dat, SQLDATE, 0);
	Lb.sqlin ((long *)       &Lb.lb.bon, SQLLONG, 0);
	Lb.sqlout ((short *)     &Lb.lb.pos, SQLSHORT, 0);
	Lb.sqlout ((double *)    &Lb.lb.a,   SQLDOUBLE, 0);
	Lb.sqlout ((short *)     &Lb.lb.waa, SQLSHORT, 0);
	Lb.sqlout ((short *)     &Lb.lb.sto_kz, SQLSHORT, 0);
	PosCursor = Lb.sqlcursor (_T("select pos, a, waa, sto_kz from lb ")
		                      _T("where sys = ? ")
							  _T("and dat = ? ")
							  _T("and bon = ? ")
							  _T("and sto_kz < 2"));
	m_LbList.SetBkColor (RGB (255, 255, 185));
	m_LbList.SetTextBkColor (RGB (255, 255, 185));
	m_PosList.SetBkColor (RGB (255, 255, 185));
	m_PosList.SetTextBkColor (RGB (255, 255, 185));
	m_PosList.ListRowUpdate = this;
	CtrlGrid.Display ();
	Form.Show ();
}

HBRUSH CLangbonView::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
/*
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (hBrush != NULL)
			{
				pDC->SetBkMode (TRANSPARENT);
				return hBrush;
			}
	}
	else if ((pWnd->GetDlgCtrlID() == IDC_STATIC_URL2) && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return hBrush;
	}
*/
	if (pWnd == &m_BonView)
	{
		pDC->SetBkColor (RGB (255, 255, 185));
		bonBrush = CreateSolidBrush (RGB (255, 255, 185));
		return bonBrush;
	}
	return DbFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}


BOOL CLangbonView::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
//				m_LbList.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetFocus () != &m_Sys&&
					GetFocus ()->GetParent () != &m_LbList )
				{

					break;
			    }
//				m_LbList.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CListCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CListCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F12)
			{
//				write ();;
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				OnStorno ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					break;
				}
				if (Choice != NULL)
				{
					Choice->ShowWindow (SW_HIDE);
				}
				return TRUE;
			}

			else if (pMsg->wParam == VK_F9)
			{
				OnSyschoice ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F2)
			{
//				OnBnClickedLeft();
			}
			else if (pMsg->wParam == VK_F3)
			{
//				OnBnClickedRight();
			}
			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
//					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
//					   OnPaste ();
					   return TRUE;
				}
			}

	}
    return CFormView::PreTranslateMessage(pMsg);
}

void CLangbonView::OnSize(UINT nType, int cx, int cy)
{
	DbFormView::OnSize (nType, cx, cy);
	if (m_LSys.m_hWnd != NULL)
	{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
	}
}

void CLangbonView::OnSyschoice ()
{
    CString Text;
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		Choice->SetListFocus ();
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceSys (this);
	    Choice->IsModal = ModalChoice;
		Choice->CreateDlg ();
	}

    Choice->SetDbClass (&Sys_peri);
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
		Choice->MoveWindow (&rect);
		Choice->SetListFocus ();
		return;
	}
    if (Choice->GetState ())
    {
		  CSysList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  Sys_peri.sys_peri.sys = abl->sys; 
		  Sys_peri.dbreadfirst ();
		  Form.Show ();
		  m_Sys.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CLangbonView::OnBedchoice ()
{
    CString Text;
	if (Sys_peri.sys_peri.sys == 0l)
	{
        return;
	}
	if (Sys_peri.sys_peri.mdn == 0)
	{
        return;
	}

	if (ChoiceBed != NULL && !ModalChoiceBed)
	{
		ChoiceBed->ShowWindow (SW_SHOWNORMAL);
		ChoiceBed->SetListFocus ();
		return;
	}
	if (ChoiceBed == NULL)
	{
		ChoiceBed = new CChoiceBed (this);
	    ChoiceBed->IsModal = ModalChoiceBed;
		ChoiceBed->CreateDlg ();
	}

	ChoiceBed->mdn = Sys_peri.sys_peri.mdn;
	ChoiceBed->fil = Sys_peri.sys_peri.fil;
    ChoiceBed->SetDbClass (&Sys_peri);
	if (ModalChoiceBed)
	{
			ChoiceBed->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		ChoiceBed->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
		ChoiceBed->MoveWindow (&rect);
		ChoiceBed->SetListFocus ();
		return;
	}
    if (ChoiceBed->GetState ())
    {
		  CBedList *abl = ChoiceBed->GetSelectedText (); 
		  if (abl == NULL) return;
		  sBed.Format ("%hd", abl->bed);
		  Form.Show ();
		  m_Bed.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

BOOL CLangbonView::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Sys)
	{
		if (!Read ())
		{
			m_Sys.SetFocus ();
			return FALSE;
		}

	}

	if (Control == &m_Bed)
	{
		if (!ReadBed ())
		{
			m_Bed.SetFocus ();
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CLangbonView::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_Sys &&
		Control->GetParent ()!= &m_LbList )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
    return FALSE;
}

BOOL CLangbonView::Read ()
{
	CString Sys;

	memcpy (&Sys_peri.sys_peri, &sys_peri_null, sizeof (sys_peri));
	memcpy (&sys_peri, &sys_peri_null, sizeof (sys_peri));
	m_Sys.GetWindowText (Sys);
	Sys_peri.sys_peri.sys = _tstol (Sys.GetBuffer ());
	if (Sys_peri.dbreadfirst () != 0)
	{
		MessageBox (_T("Ger�te-Nummer nicht gefunden"), _T("Fehler"),
			            MB_OK | MB_ICONERROR);
		m_Sys.GetFocus ();
		return TRUE;
	}
	Adr.adr.adr = 0;
	strcpy (Adr.adr.adr_krz, _T(""));
    if (Sys_peri.sys_peri.mdn == 0)
	{
        strcpy (Adr.adr.adr_krz, _T("Zentrale"));
	}  
    else if (Sys_peri.sys_peri.fil == 0)
	{
		Mdn.mdn.adr = 0;
		Mdn.mdn.mdn = Sys_peri.sys_peri.mdn;
		Mdn.dbreadfirst ();
		Adr.adr.adr = Mdn.mdn.adr;
	}
	else
	{
		Fil.fil.adr = 0;
		Fil.fil.mdn = Sys_peri.sys_peri.mdn;
		Fil.fil.fil = Sys_peri.sys_peri.fil;
		Fil.dbreadfirst ();
		Adr.adr.adr = Fil.fil.adr;
	}
	if (Adr.adr.adr != 0)
	{
		Adr.dbreadfirst ();
	}
	Form.Show ();
    return TRUE;
}

BOOL CLangbonView::ReadBed ()
{
	memcpy (&Bed.bed, &bed_null, sizeof (bed));
	memcpy (&bed, &bed_null, sizeof (bed));
	Form.Get ();
	Bed.bed.bed = atoi (sBed.GetBuffer ());
	Bed.bed.mdn = Sys_peri.sys_peri.mdn;
	Bed.bed.fil = Sys_peri.sys_peri.fil;
	Bed.bed.verk_st = 0;
	_tcscpy (Bed.bed.pers, "");
	if (Bed.dbreadfirst () != 0)
	{
		MessageBox (_T("Bediener nicht gefunden"), _T("Fehler"),
			            MB_OK | MB_ICONERROR);
		m_Bed.GetFocus ();
		return TRUE;
	}
	Form.Show ();
    return TRUE;
}

void CLangbonView::OnFileOpen()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	Form.Get ();
	Lb.lb.sys = Sys_peri.sys_peri.sys;
	Bed.bed.mdn = Sys_peri.sys_peri.mdn;
	Bed.bed.fil = Sys_peri.sys_peri.fil;
	sDatVon.Trim ();
	sDatBis.Trim ();
	FillBons ();
}

void CLangbonView::FillBons ()
{

	CString sDate;
	CString sZeit;
	CString sBon;
	CString Smt = "";
	CString sKase = "";
	CString sUmsVk = "";

	Smt = _T("select distinct dat, zeit, bon, bed, kase, sum (ums_vk) from lb ")
		  _T("where sys = ? and sto_kz < 2");
	if (LbVerbuch)
	{
		Smt += _T(" and stat = 0");
	}
	Lb.sqlin ((long *) &Lb.lb.sys, SQLLONG, 0);
	Bon.Trim ();
	if (Bon != "")
	{
		Lb.lb.bon = atol (Bon.GetBuffer ());
		Lb.sqlin ((long *) &Lb.lb.bon, SQLLONG, 0);
		Smt += _T(" and bon = ?");
	}
	if (sDatVon != "" || sDatBis != "")
	{
		Lb.ToDbDate (sDatVon, &DatVon);
		Lb.ToDbDate (sDatBis, &DatBis);
		Lb.FromDbDate (sDatVon, &DatVon);
		Lb.FromDbDate (sDatBis, &DatBis);
		Form.Show ();
		Lb.sqlin ((DATE_STRUCT *) &DatVon, SQLDATE, 0);
		Lb.sqlin ((DATE_STRUCT *) &DatBis, SQLDATE, 0);
		Smt += _T(" and dat >= ?")
			   _T(" and dat <= ?");
	}
	if (sTimeFrom != "" || sTimeTo != "")
	{
		DayTime T;
		T.Create (&sTimeFrom, "%2h:%2m");
		sTimeFrom = *(T.GetTextTime ());
		T.Create (&sTimeTo, "%2h:%2m");
		sTimeTo = *(T.GetTextTime ());
		Form.Show ();
		strcpy (TimeFrom, sTimeFrom.GetBuffer ());
		strcpy (TimeTo, sTimeTo.GetBuffer ());
		Lb.sqlin ((char *) TimeFrom, SQLCHAR, sizeof (TimeFrom));
		Lb.sqlin ((char *) TimeTo, SQLCHAR, sizeof (TimeTo));
		Smt += _T(" and zeit >= ?")
			   _T(" and zeit <= ?");
	}
	if (sBed != "")
	{
		ReadBed ();
		Lb.lb.bed = atoi (sBed.GetBuffer ());
		Lb.sqlin ((short *) &Lb.lb.bed, SQLSHORT, 0);
		Smt += _T(" and bed = ?");
	}
	else
	{
		strcpy (Bed.bed.bed_nam, "");
		Form.Show ();
	}
	if (sWaa != "")
	{
		Lb.lb.waa = atoi (sWaa.GetBuffer ());
		Lb.sqlin ((short *) &Lb.lb.waa, SQLSHORT, 0);
		Smt += _T(" and waa = ?");
	}

	Smt += "  group by dat, zeit, bon, bed, kase order by dat, zeit, bon";
	Lb.sqlout ((DATE_STRUCT *) &Lb.lb.dat, SQLDATE, 0);
	Lb.sqlout ((char *) Lb.lb.zeit, SQLCHAR, sizeof (Lb.lb.zeit));
	Lb.sqlout ((long *) &Lb.lb.bon, SQLLONG, 0);
	Lb.sqlout ((short *) &Lb.lb.bed, SQLSHORT, 0);
	Lb.sqlout ((short *) &Lb.lb.kase, SQLSHORT, 0);
	Lb.sqlout ((double *) &Lb.lb.ums_vk, SQLDOUBLE, 0);
	int cursor = Lb.sqlcursor (Smt.GetBuffer ());
	
	m_LbList.DeleteAllItems ();
	Lb.sqlopen (cursor);
	int i = 0;
	while (Lb.sqlfetch (cursor) == 0)
	{
		Lb.FromDbDate (sDate, &Lb.lb.dat);
		sZeit = Lb.lb.zeit;
		CString cBed;
		sBon.Format ("%ld", Lb.lb.bon);
        cBed.Format ("%hd", Lb.lb.bed);
		Bed.bed.bed = Lb.lb.bed;
		if (Bed.dbreadfirst () == 0)
		{
			cBed = Bed.bed.bed_nam;
		}
		sKase.Format ("%hd", Lb.lb.kase);
		sUmsVk.Format ("%.2lf", Lb.lb.ums_vk);
		FillList.InsertItem (i, 0);
		FillList.SetItemText (sDate.GetBuffer (), i, 0);
		FillList.SetItemText (sZeit.GetBuffer (), i, 1);
		FillList.SetItemText (sBon.GetBuffer (), i, 2);
		FillList.SetItemText (cBed.GetBuffer (), i, 3);
		FillList.SetItemText (sKase.GetBuffer (), i, 4);
		FillList.SetItemText (sUmsVk.GetBuffer (), i, 5);
        i ++;
	}
	Lb.sqlclose (cursor);
	strcpy (Bed.bed.bed_nam, "");
	if (i > 0)
	{
	    m_LbList.SetItemState (0, LVIS_SELECTED | LVIS_FOCUSED, 
								  LVIS_SELECTED | LVIS_FOCUSED);
		m_LbList.SetFocus ();
	}
}

void CLangbonView::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	GetParent ()->DestroyWindow ();
}


void CLangbonView::OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;
	int SelCount = m_LbList.GetSelectedCount ();
	if (SelCount != 1) return;
    int idx   = m_LbList.GetNextItem (-1, LVNI_SELECTED);
	if (idx == -1) return;
	CString cDat = m_LbList.GetItemText (idx, 0);
	CString cBon = m_LbList.GetItemText (idx, 2);
	Lb.ToDbDate (cDat, &Lb.lb.dat);
	Lb.lb.bon = atol (cBon.GetBuffer ());
    FillLbPos ();
}

void CLangbonView::FillLbPos ()
{

	MwstTab.clear ();
	CString Date;
	Lb.FromDbDate (Date, &Lb.lb.dat);
	CString Message;
	Message.Format ("sys = %ld Dat = %s Bon = %ld",
		             Lb.lb.sys, Date, Lb.lb.bon);

	PosTab.DestroyAll ();
	PosTab.Init ();
	m_PosList.DeleteAllItems ();
	Lb.sqlopen (PosCursor);
	int i = 0;
	while (Lb.sqlfetch (PosCursor) == 0)
	{
        if (Lb.dbreadfirst () != 0)
		{
			continue;
		}
		LB *LbItem = new LB;
		memcpy (LbItem, &Lb.lb, sizeof (LB));
		PosTab.Add (LbItem);
		FillPos.InsertItem (i, 0);
		CString cA;
        cA.Format ("%.0lf", Lb.lb.a);
		FillPos.SetItemText (cA.GetBuffer (), i, 0);
		A_bas.a_bas.a = Lb.lb.a;
		strcpy (A_bas.a_bas.a_bz1, "");
		A_bas.dbreadfirst ();
		FillPos.SetItemText (A_bas.a_bas.a_bz1, i, 1);

		CString cWaa;
        cWaa.Format ("%hd", Lb.lb.waa);
		FillPos.SetItemText (cWaa.GetBuffer (), i, 2);

		CString cSto;
        cSto.Format ("%hd", Lb.lb.sto_kz);
		FillPos.SetItemText (cSto.GetBuffer (), i, 3);

        CString cUmsVk;
		cUmsVk.Format ("%.2lf", Lb.lb.ums_vk);
		FillPos.SetItemText (cUmsVk.GetBuffer (), i, 4);

		CString cMeVk;
		cMeVk.Format ("%.3lf", Lb.lb.me_vk);
		FillPos.SetItemText (cMeVk.GetBuffer (), i, 5);

		CString cMeVkPos;
		cMeVkPos.Format ("%ld", Lb.lb.me_vk_pos);
		FillPos.SetItemText (cMeVkPos.GetBuffer (), i, 6);
		strcpy (Ptabn.ptabn.ptitem, "mwst");
		sprintf (Ptabn.ptabn.ptwert, "%hd", Lb.lb.mwst);
		strcpy (Ptabn.ptabn.ptbezk, "");
		Ptabn.dbreadfirst ();
		FillPos.SetItemText (Ptabn.ptabn.ptbezk, i, 7);
		MwstTab.push_back (Lb.lb.mwst);

        CString cUmsMwst;
		cUmsMwst.Format ("%.2lf", Lb.lb.ums_mwst);
		FillPos.SetItemText (cUmsMwst.GetBuffer (), i, 8);

        CString cStoUmsVk;
		cUmsVk.Format ("%.2lf", Lb.lb.sto_ums_vk);
		FillPos.SetItemText (cStoUmsVk.GetBuffer (), i, 9);

		CString cStoMeVk;
		cStoMeVk.Format ("%.3lf", Lb.lb.sto_me_vk);
		FillPos.SetItemText (cStoMeVk.GetBuffer (), i, 10);

		CString cStoMeVkPos;
		cStoMeVkPos.Format ("%ld", Lb.lb.sto_vk_pos);
		FillPos.SetItemText (cStoMeVkPos.GetBuffer (), i, 11);

        CString cStoUmsMwst;
		cStoUmsMwst.Format ("%.2lf", Lb.lb.sto_ums_mwst);
		FillPos.SetItemText (cStoUmsMwst.GetBuffer (), i, 12);

        i ++;
	}
	FillBonView ();
}

void CLangbonView::FillBonView ()
{
	PARAFORMAT2 pf;
	CHARFORMAT2 cf;
	double sumgew;
	double sumums;

	pf.cbSize = sizeof(PARAFORMAT2);
	pf.dwMask = PFM_BORDER;
	pf.wBorders = 0x5201;
	m_BonView.SetParaFormat(pf);

	cf.cbSize = sizeof(CHARFORMAT2);
	cf.dwMask = CFM_STRIKEOUT|CFM_BOLD;
	cf.dwEffects = CFE_STRIKEOUT;
//	m_BonView.SetDefaultCharFormat(cf);

	CString Date;
	Lb.FromDbDate (Date, &Lb.lb.dat);
	CString Message;
	Message.Format ("sys = %ld Dat = %s Bon = %ld",
		             Lb.lb.sys, Date, Lb.lb.bon);

/*
	PosTab.DestroyAll ();
	PosTab.Init ();
	m_PosList.DeleteAllItems ();
*/
	Lb.sqlopen (PosCursor);
	int i = 0;
	CString sDate;
	Lb.FromDbDate (sDate, &Lb.lb.dat);
	CString cBed;
	strcpy (Bed.bed.bed_nam, "");
	Bed.bed.bed = Lb.lb.bed;
	Bed.dbreadfirst ();
	CString Text = "";
	Text.Format ("\n\t\t\tDatum %s    Zeit %s\n\n"
         "\t\t\tBon-Nr\t\t%ld\n"
		 "\t\t\tBediener\t\t%s\n\n\n"
		 "\t\t\tkg\tEuro/kg\t\tEuro\t\t\t\t\t\n\n",
		 sDate.GetBuffer () , Lb.lb.zeit, Lb.lb.bon, Bed.bed.bed_nam);
	sumgew  = 0.0;
	sumums = 0.0;
	CString Line;
	while (Lb.sqlfetch (PosCursor) == 0)
	{
        if (Lb.dbreadfirst () != 0)
		{
			continue;
		}

		if (Lb.lb.sto_kz != 0)
		{
			continue;
		}
/*
		LB *LbItem = new LB;
		memcpy (LbItem, &Lb.lb, sizeof (LB));
		PosTab.Add (LbItem);
*/
		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas.a_bas.a = Lb.lb.a;
		A_bas.dbreadfirst ();
		CString Line;
		Line.Format ("\t\t\t%.0lf\n\t\t\t%s\n\t%s\n"
					 "\t\t\t%8.3lf\t%8.2lf\t\t%8.2lf\n\n",
					 Lb.lb.a, A_bas.a_bas.a_bz1, A_bas.a_bas.a_bz2,
					 Lb.lb.me_vk,
					 Lb.lb.pr_vk,
					 Lb.lb.ums_vk);
		Text += Line;
		sumgew  += Lb.lb.me_vk;
		sumums += Lb.lb.ums_vk;
        i ++;
	}
	Line.Format ("\t\t\t\t\t\t\t\t\t%8.8s\t\t%8.8s\t\t\t%8.8s\t\t\t\t\n"
				 "\t\t\t%8.3lf\t\t\t%8.2lf\n\n",
				 " ", " ", " ",
					 sumgew,
					 sumums);

	Text += Line;
	m_BonView.SetWindowText (Text);
	int LineCount = m_BonView.GetLineCount ();
	int lastline = LineCount - 5;

	int start = m_BonView.LineIndex (1);
	char buffer [256];
	m_BonView.GetLine (1, buffer, 255);
	int end = start + (int) strlen (buffer);
	m_BonView.SetSel (start + 2, end);
	m_BonView.GetSelectionCharFormat (cf);
	cf.dwMask = CFM_UNDERLINE | CFM_SIZE | CFM_FACE;
	cf.dwEffects = CFE_UNDERLINE;
	cf.yHeight = 295;
	strcpy (cf.szFaceName, "MS SAN SHERIF");
	m_BonView.SetSelectionCharFormat(cf);


	start = m_BonView.LineIndex (3);
	m_BonView.GetLine (3, buffer, 255);
	end = start + (int) strlen (buffer);
	m_BonView.SetSel (start, end);
	m_BonView.GetSelectionCharFormat (cf);
	cf.dwMask = CFM_SIZE |CFM_BOLD | CFM_FACE;
	cf.dwEffects = CFE_BOLD;
	cf.yHeight = 255;
	strcpy (cf.szFaceName, "MS SAN SHERIF");
	m_BonView.SetSelectionCharFormat(cf);

	start = m_BonView.LineIndex (4);
	m_BonView.GetLine (4, buffer, 255);
	end = start + (int) strlen (buffer);
	m_BonView.SetSel (start, end);
	m_BonView.GetSelectionCharFormat (cf);
	cf.dwMask = CFM_SIZE |CFM_BOLD | CFM_FACE;
	cf.dwEffects = CFE_BOLD;
	cf.yHeight = 255;
	strcpy (cf.szFaceName, "MS SAN SHERIF");
	m_BonView.SetSelectionCharFormat(cf);

	start = m_BonView.LineIndex (7);
	m_BonView.GetLine (7, buffer, 255);
	end = start + (int) strlen (buffer);
	m_BonView.SetSel (start + 2, end);
	cf.dwMask = CFM_UNDERLINE;
	cf.dwEffects = CFE_UNDERLINE;
	m_BonView.SetSelectionCharFormat(cf);

	start = m_BonView.LineIndex (lastline);
	m_BonView.GetLine (7, buffer, 255);
	end = start + (int) strlen (buffer);
	m_BonView.SetSel (start + 2, end);
	cf.dwMask = CFM_UNDERLINE;
	cf.dwEffects = CFE_UNDERLINE;
	m_BonView.SetSelectionCharFormat(cf);

}

void CLangbonView::OnStorno()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_LbList.GetItemCount () == 0)
	{
		MessageBox (_T("Es ist kein Langbon selectiert"), _T (""), MB_OK | MB_ICONERROR);
		return;
	}
	int bidx = m_LbList.GetNextItem (-1,  LVNI_SELECTED);
	if (bidx == -1)
	{
		MessageBox (_T("Es ist kein Langbon selectiert"), _T (""), MB_OK | MB_ICONERROR);
		return;
	}

	if (StoMode != Storno)
	{
		UseWaaSto = FALSE;
	}

	int pidx = m_PosList.GetNextItem (-1,  LVNI_SELECTED);
	if (pidx == -1)
	{
		INT_PTR ret= IDYES;
		if (StoMode == Storno)
		{
		      ret = MessageBox (_T("Bon stornieren ?"), "", MB_YESNO | MB_ICONQUESTION);
		}
		if (ret == IDYES)
		{
			if (BonStorno ())
			{
				if (StoMode == Storno)
				{
					MessageBox (_T("Bon wurde storniert"));
					FillBonView ();
				}
				else
				{
					m_LbList.DeleteItem (bidx);
					if (m_LbList.GetItemCount () == 0)
					{
						m_PosList.DeleteAllItems ();
						return;
					}
					CString cDat = m_LbList.GetItemText (bidx, 0);
					CString cBon = m_LbList.GetItemText (bidx, 2);
					Lb.ToDbDate (cDat, &Lb.lb.dat);
					Lb.lb.bon = atol (cBon.GetBuffer ());
					FillLbPos ();
				}
			}
		}
		m_LbList.SetItemState (bidx, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		return;
	}

	CStornoDialog dlg;
	if (dlg.DoModal () == IDOK)
	{
		if (dlg.BonStorno)
		{
			if (BonStorno ())
			{
				if (StoMode == Storno)
				{
					MessageBox (_T("Bon wurde storniert"));
					FillBonView ();
				}
				else
				{
					m_LbList.DeleteItem (bidx);
					if (m_LbList.GetItemCount () == 0)
					{
						m_PosList.DeleteAllItems ();
						return;
					}
					CString cDat = m_LbList.GetItemText (bidx, 0);
					CString cBon = m_LbList.GetItemText (bidx, 2);
					Lb.ToDbDate (cDat, &Lb.lb.dat);
					Lb.lb.bon = atol (cBon.GetBuffer ());
					FillLbPos ();
				}
			}
		}
		else if (dlg.PosStorno)
		{
			if (PosStorno (pidx))
			{
				if (StoMode == Storno)
				{
					MessageBox (_T("Position wurde storniert"));
					FillBonView ();
				}
			}
		}
	}
}

BOOL CLangbonView::BonStorno()
{
	for (int i = 0; i < PosTab.GetCount (); i ++)
	{
		if (!PosStorno (i)) return FALSE;
	}
	return TRUE;
}

BOOL CLangbonView::PosStorno(int pidx)
{
	LB *LbItem = (LB* )PosTab.Get (pidx);
	if (LbItem == NULL) return FALSE;
	memcpy (&Lb.lb, LbItem, sizeof (LB));
	Lb.dbdelete ();
	Lb.lb.sto_kz = 1;
	Lb.dbupdate ();
	Lb.lb.sto_me_vk = Lb.lb.me_vk;
	Lb.lb.me_vk = 0.0;
	Lb.lb.sto_ums_vk = Lb.lb.ums_vk;
	Lb.lb.ums_vk = 0.0;
	Lb.lb.sto_vk_pos = Lb.lb.me_vk_pos;
	Lb.lb.sto_ums_mwst = Lb.lb.ums_mwst;
	Lb.lb.me_vk_pos = 0;
	Lb.lb.ums_mwst = 0.0;
	Lb.lb.sto_kz = 2;
	Lb.lb.sto_fit = 1;
	Lb.lb.stat = 0;
	if (Lb.dbreadfirst () == 0) return FALSE;
	Lb.dbupdate ();
	WaaStorno ();
	KaseStorno ();
	if (UseWaaSto)
	{
		return TRUE;
	}
	BedStorno ();
	UmsStorno.Prepare ();
//	memcpy (&Lb.lb, LbItem, sizeof (LB));
	memcpy (&UmsStorno.dat, &Lb.lb.dat, sizeof (DATE_STRUCT));
	UmsStorno.mdn = LbItem->mdn;
	UmsStorno.fil = LbItem->fil;
	UmsStorno.a   = LbItem->a;
	UmsStorno.mwst = LbItem->mwst;
	UmsStorno.ums_vk_mwst = LbItem->ums_mwst;
	UmsStorno.ums_vk      = LbItem->ums_vk;
	if (LbItem->me_vk > 0.0)
	{
		UmsStorno.me_vk       = LbItem->me_vk;
	}
	else
	{
		UmsStorno.me_vk       = LbItem->me_vk_pos;
	}
	UmsStorno.me_vk_pos   = LbItem->me_vk_pos;
	if (Lb.lb.sa != 0)
	{
		UmsStorno.ums_vk_mwst_sa = LbItem->ums_mwst;
		UmsStorno.ums_vk_sa      = LbItem->ums_vk;
		UmsStorno.me_vk_sa       = UmsStorno.me_vk;
		UmsStorno.me_vk_pos_sa   = LbItem->me_vk_pos;
	}
	else
	{
		UmsStorno.ums_vk_mwst_sa = 0.0;
		UmsStorno.ums_vk_sa = 0.0;
		UmsStorno.me_vk_sa = 0.0;
		UmsStorno.me_vk_pos_sa = 0.0;
	}
    MwstStorno ();
	FilEinnStorno ();
	UmsStorno.LbVerbuch = LbVerbuch;
	UmsStorno.Update ();
	FillPos.SetItemText ("1", pidx, 3);
	if (StoMode != Storno)
	{
		Lb.dbdeletea ();
/*
		Lb.dbdelete ();
		Lb.lb.sto_kz = 0;
		Lb.lb.sto_me_vk = 0.0;
		Lb.lb.sto_ums_vk = 0.0;
		Lb.lb.sto_vk_pos = 0;
		Lb.lb.sto_ums_mwst = 0;
		Lb.dbupdate ();
*/
	}

	return TRUE;
}

BOOL CLangbonView::WaaStorno ()
{
	if (UseWaaSto)
	{
		memcpy (&Waa_sto.waa_sto, &waa_sto_null, sizeof (WAA_STO));
		Waa_sto.waa_sto.mdn = Lb.lb.mdn;
		Waa_sto.waa_sto.fil = Lb.lb.fil;
		Waa_sto.waa_sto.bon = Lb.lb.bon;
		Waa_sto.waa_sto.a   = Lb.lb.a;
		Waa_sto.waa_sto.waa = Lb.lb.waa;
		Waa_sto.waa_sto.bed = Lb.lb.bed;
		Waa_sto.waa_sto.verk_st = 0;
		memcpy (&Waa_sto.waa_sto.dat, &Lb.lb.dat,  sizeof (Waa_sto.waa_sto.dat));
		memcpy (&Waa_sto.waa_sto.zeit, &Lb.lb.zeit, sizeof (Waa_sto.waa_sto.zeit));
		Waa_sto.waa_sto.sto_me = Lb.lb.sto_me_vk;
		Waa_sto.waa_sto.sto_pos_bws = 1;
		Waa_sto.waa_sto.pr_ek = Lb.lb.pr_ek;
		Waa_sto.waa_sto.pr_vk = Lb.lb.pr_vk;
		sprintf (Waa_sto.waa_sto.lad_akv, "%1.1hd", Lb.lb.sa); 
		Waa_sto.waa_sto.lfd = 1;
		strcpy (Waa_sto.waa_sto.dr_status, " ");
		if (Waa_sto.dbreadfirst () == 0)
		{
			Waa_sto.waa_sto.lfd ++;
		}
		Waa_sto.dbupdate ();
		return TRUE;
	}

	memcpy (&Waa_sum.waa_sum, &waa_sum_null, sizeof (WAA_SUM));
	Waa_sum.waa_sum.mdn = Lb.lb.mdn;
	Waa_sum.waa_sum.fil = Lb.lb.fil;
	Waa_sum.waa_sum.waa = Lb.lb.waa;
    Waa_sum.waa_sum.verk_st = 0;
	memcpy (&Waa_sum.waa_sum.dat, &Lb.lb.dat,  sizeof (Waa_sum.waa_sum.dat));
	Waa_sum.dbreadfirst ();
	if (StoMode == Storno)
	{
		Waa_sum.waa_sum.sto_pos_bws += (short) Lb.lb.sto_vk_pos;
		Waa_sum.waa_sum.sto_sum_bws += Lb.lb.sto_ums_vk;
	}
	Waa_sum.waa_sum.pos_gew -= (short) Lb.lb.sto_vk_pos;
	Waa_sum.waa_sum.ums_gew -= Lb.lb.sto_ums_vk;
	Waa_sum.dbupdate ();

	return TRUE;
}

BOOL CLangbonView::KaseStorno ()
{

	if (!UseWaaSto)
	{
		memcpy (&Kase_sum.kase_sum, &kase_sum_null, sizeof (KASE_SUM));
		Kase_sum.kase_sum.mdn = Lb.lb.mdn;
		Kase_sum.kase_sum.fil = Lb.lb.fil;
		Kase_sum.kase_sum.kase = Lb.lb.kase;
		Kase_sum.kase_sum.verk_st = 0;
		memcpy (&Kase_sum.kase_sum.dat, &Lb.lb.dat,  sizeof (Kase_sum.kase_sum.dat));
		if (Kase_sum.dbreadfirst () != 0)
		{
			return FALSE;
		}
		if (StoMode == Storno)
		{
			Kase_sum.kase_sum.sto_pos_bon += (short) Lb.lb.sto_vk_pos;
			Kase_sum.kase_sum.sto_sum_bon += Lb.lb.sto_ums_vk;
		}
		Kase_sum.kase_sum.pos_ges -= (short) Lb.lb.sto_vk_pos;
		Kase_sum.kase_sum.ums_ges -= Lb.lb.sto_ums_vk;
		if (Lb.lb.mwst == 1)
		{
			Kase_sum.kase_sum.ums_mwst1 -= Lb.lb.sto_ums_vk;
//			Kase_sum.kase_sum.ums_mwst1 -= Lb.lb.ums_vk;
		}
		else if (Lb.lb.mwst == 2)
		{
			Kase_sum.kase_sum.ums_mwst2 -= Lb.lb.sto_ums_vk;
//			Kase_sum.kase_sum.ums_mwst2 -= Lb.lb.ums_vk;
		}
		else if (Lb.lb.mwst == 3)
		{
			Kase_sum.kase_sum.ums_mwst3 -= Lb.lb.sto_ums_vk;
//			Kase_sum.kase_sum.ums_mwst3 -= Lb.lb.ums_vk;
		}
		else if (Lb.lb.mwst == 4)
		{
			Kase_sum.kase_sum.ums_mwst4 -= Lb.lb.sto_ums_vk;
//			Kase_sum.kase_sum.ums_mwst4 -= Lb.lb.ums_vk;
		}
		Kase_sum.dbupdate ();
		return TRUE;
	}

	memcpy (&Kase_sto.kase_sto, &kase_sto_null, sizeof (KASE_STO));
	if (StoMode == Storno)
	{
		Kase_sto.kase_sto.mdn = Lb.lb.mdn;
		Kase_sto.kase_sto.fil = Lb.lb.fil;
		Kase_sto.kase_sto.bon = Lb.lb.bon;
		Kase_sto.kase_sto.a   = Lb.lb.a;
		Kase_sto.kase_sto.kase = Lb.lb.kase;
		Kase_sto.kase_sto.verk_st = 0;
		memcpy (&Kase_sto.kase_sto.dat, &Lb.lb.dat,  sizeof (Kase_sto.kase_sto.dat));
		memcpy (&Kase_sto.kase_sto.zeit, Lb.lb.zeit, sizeof (Kase_sto.kase_sto.zeit));
		Kase_sto.kase_sto.sto_me = Lb.lb.sto_me_vk;
		Kase_sto.kase_sto.sto_pos_bws = 1;
		Kase_sto.kase_sto.pr_ek = Lb.lb.pr_ek;
		Kase_sto.kase_sto.pr_vk = Lb.lb.pr_vk;
		sprintf (Kase_sto.kase_sto.lad_akv, "%1.1hd", Lb.lb.sa); 
		Kase_sto.kase_sto.lfd = 1;
		strcpy (Kase_sto.kase_sto.dr_status, " ");
		if (Kase_sto.dbreadfirst () == 0)
		{
			Kase_sto.kase_sto.lfd ++;
		}
		Kase_sto.dbupdate ();
	}
	return TRUE;
}

BOOL CLangbonView::BedStorno ()
{

	memcpy (&Bed_sum.bed_sum, &bed_sum_null, sizeof (BED_SUM));
	Bed_sum.bed_sum.mdn = Lb.lb.mdn;
	Bed_sum.bed_sum.fil = Lb.lb.fil;
	Bed_sum.bed_sum.bed = Lb.lb.bed;
	Bed_sum.bed_sum.waa = Lb.lb.waa;
    Bed_sum.bed_sum.verk_st = 0;
	memcpy (&Bed_sum.bed_sum.dat, &Lb.lb.dat,  sizeof (Bed_sum.bed_sum.dat));
	Bed_sum.dbreadfirst ();
	if (StoMode == Storno)
	{
		Bed_sum.bed_sum.sto_pos_bws += (short) Lb.lb.sto_vk_pos;
		Bed_sum.bed_sum.sto_sum_bws += Lb.lb.sto_ums_vk;
	}
	Bed_sum.bed_sum.pos_gew -= (short) Lb.lb.sto_vk_pos;
	Bed_sum.bed_sum.ums_gew -= Lb.lb.sto_ums_vk;
	Bed_sum.dbupdate ();

	return TRUE;
}

BOOL CLangbonView::MwstStorno ()
{

// Die Mengenfelder werden nicht ber�cksichtigt, so definiert von KB

	memcpy (&Mwst_sum.mwst_sum, &mwst_sum_null, sizeof (MWST_SUM));
	Mwst_sum.mwst_sum.mdn = Lb.lb.mdn;
	Mwst_sum.mwst_sum.fil = Lb.lb.fil;
	Mwst_sum.mwst_sum.mwst = Lb.lb.mwst;
    Mwst_sum.mwst_sum.verk_st = 0;
	memcpy (&Mwst_sum.mwst_sum.dat, &Lb.lb.dat,  sizeof (Mwst_sum.mwst_sum.dat));
	Mwst_sum.dbreadfirst ();

//	Mwst_sum.mwst_sum.me_vk     -= UmsStorno.me_vk;
//	Mwst_sum.mwst_sum.me_vk_pos -= UmsStorno.me_vk_pos;
	Mwst_sum.mwst_sum.ums_vk    -= UmsStorno.ums_vk;
	Mwst_sum.mwst_sum.mwst_betr -= UmsStorno.ums_vk_mwst;
	Mwst_sum.dbupdate ();

	return TRUE;
}

BOOL CLangbonView::FilEinnStorno ()
{

// Die Stornofelder werden nicht best�ckt, sondern die Umsattzfelder werden korrigiert, so definiert von KB

	memcpy (&Fil_einn.fil_einn, &fil_einn_null, sizeof (FIL_EINN));
	Fil_einn.fil_einn.mdn = Lb.lb.mdn;
	Fil_einn.fil_einn.fil = Lb.lb.fil;
	Fil_einn.fil_einn.kase = Lb.lb.kase;
	memcpy (&Fil_einn.fil_einn.einn_dat, &Lb.lb.dat,  sizeof (Fil_einn.fil_einn.einn_dat));
	Fil_einn.dbreadfirst ();

	Fil_einn.fil_einn.kase_bar    -=  UmsStorno.ums_vk;
	Fil_einn.fil_einn.einn        -=  UmsStorno.ums_vk;
	Fil_einn.fil_einn.einn_nto    -=  NettoValue (UmsStorno.mwst, UmsStorno.ums_vk); 
	if (UmsStorno.mwst == 1)
	{
		Fil_einn.fil_einn.ums_mwst1      -= UmsStorno.ums_vk;
		Fil_einn.fil_einn.ums_mwst1_net  -= NettoValue (UmsStorno.mwst, UmsStorno.ums_vk); 
	}
	if (UmsStorno.mwst == 2)
	{
		Fil_einn.fil_einn.ums_mwst2      -= UmsStorno.ums_vk;
		Fil_einn.fil_einn.ums_mwst2_net  -= NettoValue (UmsStorno.mwst, UmsStorno.ums_vk); 
	}
	if (UmsStorno.mwst == 3)
	{
		Fil_einn.fil_einn.ums_mwst3      -= UmsStorno.ums_vk;
		Fil_einn.fil_einn.ums_mwst3_net  -= NettoValue (UmsStorno.mwst, UmsStorno.ums_vk); 
	}
	if (UmsStorno.mwst == 4)
	{
		Fil_einn.fil_einn.ums_mwst4      -= UmsStorno.ums_vk;
		Fil_einn.fil_einn.ums_mwst4_net  -= NettoValue (UmsStorno.mwst, UmsStorno.ums_vk); 
	}
	Fil_einn.dbupdate ();

	return TRUE;
}

double CLangbonView::NettoValue (short mwst, double value)
{
	double ntoValue;
	double ptwer2 = 0.0;

    ntoValue = value;
	memcpy (&Ptabn.ptabn, &ptabn_null, sizeof (PTABN)),
	strcpy (Ptabn.ptabn.ptitem, "mwst");
	sprintf (Ptabn.ptabn.ptwert, "%hd", mwst);
	if (Ptabn.dbreadfirst () == 0)
	{
		ptwer2 = CStrFuncs::StrToDouble (Ptabn.ptabn.ptwer2);
		if (ptwer2 != 0.0)
		{
			ntoValue /= ptwer2;
		}
	}
	return ntoValue;
}


void CLangbonView::OnPrint()
{
     CDC *pDC;

	CPrintDialog dlg(FALSE);
	if (dlg.DoModal() != IDOK)
	{
		return;
	}
	
	HDC hdc = dlg.GetPrinterDC();
	if (hdc == NULL)
	{
		return;
	}
	pDC =  CDC::FromHandle (hdc);

	FORMATRANGE fr;

// Get the page width and height from the printer.
	long lPageWidth = ::MulDiv(pDC->GetDeviceCaps(PHYSICALWIDTH),
    1440, pDC->GetDeviceCaps(LOGPIXELSX));
    long lPageHeight = ::MulDiv(pDC->GetDeviceCaps(PHYSICALHEIGHT),
    1440, pDC->GetDeviceCaps(LOGPIXELSY));
	CRect rcPage(0, 0, lPageWidth, lPageHeight);

	pDC->StartDoc ("Test");
    if (StartPage (hdc) == 0) return;
// Format the text and render it to the printer.
	fr.hdc = pDC->m_hDC;
	fr.hdcTarget = pDC->m_hDC;
	fr.rc = rcPage;
	fr.rcPage = rcPage;
	fr.chrg.cpMin = 0;
	fr.chrg.cpMax = -1;
	long idx = m_BonView.FormatRange(&fr, TRUE);

	// Update the display with the new formatting.
	RECT rcClient;
	m_BonView.GetClientRect(&rcClient);
    int ret = m_BonView.DisplayBand(&rcClient);
/*
    long lLineWidth = ::MulDiv(pDC->GetDeviceCaps(PHYSICALWIDTH),
						1440, pDC->GetDeviceCaps(LOGPIXELSX));

	m_BonView.SetTargetDevice(*pDC, lLineWidth);
*/
	pDC->EndPage ();
	pDC->EndDoc ();
	DeleteDC (hdc);
}

void CLangbonView::OnViewbon()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	CMenu *menu = GetParent ()->GetMenu ();
	CToolBar *tb = &((CMainFrame *)GetParent ())->m_wndToolBar;
	CToolBarCtrl *tbc = &tb->GetToolBarCtrl ();
	if (!BonViewActive)
	{
		menu->CheckMenuItem (ID_BONVIEW, MF_CHECKED);
		tbc->CheckButton (tb->GetItemID (9), TRUE);
		BonViewActive = TRUE;
		SetBonView ();
	}
	else
	{
		menu->CheckMenuItem (ID_BONVIEW, MF_UNCHECKED);
		tbc->CheckButton (tb->GetItemID (9), FALSE);
		BonViewActive = FALSE;
		UnsetBonView ();
	}
}

void CLangbonView::SetBonView ()
{
	m_PosList.ShowWindow (SW_HIDE);
	m_BonView.ShowWindow (SW_SHOWNORMAL);
	SplitPane.SetView2 (&m_BonView);
}

void CLangbonView::UnsetBonView ()
{
	m_PosList.ShowWindow (SW_SHOWNORMAL);
	m_BonView.ShowWindow (SW_HIDE);
	SplitPane.SetView2 (&m_PosList);
}

BOOL CLangbonView::SetRowColor (int Row, COLORREF *Color)
{
	short mwst = MwstTab[Row];
	if (mwst == 1)
	{
		*Color = RGB (128, 255, 255);
		return TRUE;
	}
	else if (mwst == 2)
	{
//		*Color = RGB (235, 235, 235);
		*Color = RGB (217, 255, 217);
		return TRUE;
	}
	return FALSE;
}
