#ifndef _KASE_STO_DEF
#define _KASE_STO_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct KASE_STO {
   double         a;
   long           bon;
   TCHAR          bon_kz[2];
   short          bed;
   TCHAR          bsd_kz[2];
   DATE_STRUCT    dat;
   TCHAR          dr_status[2];
   short          fil;
   long           lfd;
   TCHAR          lad_akv[2];
   double         me_ist;
   short          mdn;
   TCHAR          pers_nam[9];
   long           prima;
   double         pr_sto;
   double         pr_ek;
   double         pr_vk;
   double         sto_me;
   short          sto_pos_bws;
   TCHAR          txt[61];
   short          verk_st;
   short          kase;
   TCHAR          zeit[7];
};
extern struct KASE_STO kase_sto, kase_sto_null;

#line 8 "kase_sto.rh"

class KASE_STO_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               KASE_STO kase_sto;  
               KASE_STO_CLASS () : DB_CLASS ()
               {
               }
};
#endif
