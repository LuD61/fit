#pragma once
#include "afxwin.h"


// CStornoDialog-Dialogfeld

class CStornoDialog : public CDialog
{
	DECLARE_DYNAMIC(CStornoDialog)

public:
	CStornoDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CStornoDialog();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
    virtual BOOL OnInitDialog ();
	virtual void OnOK ();
	DECLARE_MESSAGE_MAP()
public:
	BOOL BonStorno;
	BOOL PosStorno;
	CButton m_BonStorno;
	CButton m_PosStorno;
};
