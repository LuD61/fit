#ifndef _KASE_SUM_DEF
#define _KASE_SUM_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct KASE_SUM {
   double         ausgaben;
   short          bon_anz;
   DATE_STRUCT    dat;
   short          fil;
   double         gra_tot_bto;
   double         gra_tot_nto;
   short          kase;
   double         kase_ab_am_expr;
   double         kase_ab_bar;
   double         kase_ab_chq;
   double         kase_ab_diners;
   double         kase_ab_euro_ca;
   double         kase_ab_gut;
   double         kase_ab_sonst;
   double         kase_am_expr;
   double         kase_bar;
   double         kase_chq;
   double         kase_diners;
   double         kase_euro_ca;
   double         kase_gut;
   double         kase_sonst;
   double         kase_visa;
   double         kase_ab_visa;
   double         kase_soll;
   double         kredit_wrt;
   short          kun_anz;
   short          mdn;
   short          minus_pos;
   double         minus_sum;
   short          nullbon_anz;
   double         pfa_wrt;
   short          pos_ges;
   double         rab_nach;
   double         rech_bez;
   short          sto_pos_bon;
   short          sto_pos_dir;
   double         sto_sum_bon;
   double         sto_sum_dir;
   double         ums_ges;
   double         ums_mwst1;
   double         ums_mwst2;
   double         ums_mwst3;
   double         ums_mwst4;
   short          verk_st;
   double         wechsg;
   long           z_zaehl;
   double         kase_ec_cash;
   double         kase_ec_glb;
   long           kase_eccash_pos;
   long           kase_ecglb_pos;
   double         abr_sum_bon;
   double         abr_pos_bon;
   double         einzahlung;
   double         kase_ist;
};
extern struct KASE_SUM kase_sum, kase_sum_null;

#line 8 "kase_sum.rh"

class KASE_SUM_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               KASE_SUM kase_sum;  
               KASE_SUM_CLASS () : DB_CLASS ()
               {
               }
};
#endif
