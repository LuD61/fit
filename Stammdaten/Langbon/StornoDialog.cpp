// StornoDialog.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Langbon.h"
#include "StornoDialog.h"


// CStornoDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CStornoDialog, CDialog)
CStornoDialog::CStornoDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CStornoDialog::IDD, pParent)
	, BonStorno(TRUE)
	, PosStorno(FALSE)
{
}

CStornoDialog::~CStornoDialog()
{
}

void CStornoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BONSTORNO, m_BonStorno);
	DDX_Control(pDX, IDC_POSSTORNO, m_PosStorno);
}


BEGIN_MESSAGE_MAP(CStornoDialog, CDialog)
END_MESSAGE_MAP()


// CStornoDialog-Meldungshandler

BOOL CStornoDialog::OnInitDialog ()
{
	CDialog::OnInitDialog ();
	m_BonStorno.SetCheck (BST_CHECKED);
	return TRUE;
}

void CStornoDialog::OnOK ()
{
	BonStorno = PosStorno = FALSE;
	if (m_BonStorno.GetCheck () == BST_CHECKED)
	{
		BonStorno = TRUE;
	}
	else if (m_PosStorno.GetCheck () == BST_CHECKED)
	{
		PosStorno = TRUE;
	}
	CDialog::OnOK ();
}

