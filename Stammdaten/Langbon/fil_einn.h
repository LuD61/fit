#ifndef _FIL_EINN_SUM_DEF
#define _FIL_EINN_SUM_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct FIL_EINN {
   double         einn;
   TCHAR          einn_bem[13];
   DATE_STRUCT    einn_dat;
   double         einn_nto;
   short          fil;
   double         fil_std;
   short          kase;
   double         kred;
   long           kun_anz_int;
   short          mdn;
   double         pers_rab_wrt;
   double         pers_pkt;
   DATE_STRUCT    buch_dat;
   long           buch_lauf;
   double         kase_bar;
   double         rech_bez;
   double         ausgaben;
   double         ums_mwst1;
   double         ums_mwst2;
   double         ums_mwst3;
   double         ums_mwst4;
   double         ums_mwst1_net;
   double         ums_mwst2_net;
   double         ums_mwst3_net;
   double         ums_mwst4_net;
   double         mwst1_proz;
   double         mwst2_proz;
   double         mwst3_proz;
   double         mwst4_proz;
   short          buch_kz;
   double         kase_ist;
   double         wsw_ist;
   double         markt_ums;
   double         ein_sto_bon;
   double         ein_sto_dir;
   double         wechsg_anf;
   double         wechsg;
   double         sto_mwst1;
   double         sto_mwst2;
   double         sto_mwst1_net;
   double         sto_mwst2_net;
   double         kase_gut;
   double         kase_chk;
   double         bank_bar;
   double         bank_unbar;
};
extern struct FIL_EINN fil_einn, fil_einn_null;

#line 8 "fil_einn.rh"

class FIL_EINN_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               FIL_EINN fil_einn;  
               FIL_EINN_CLASS () : DB_CLASS ()
               {
               }
};
#endif
