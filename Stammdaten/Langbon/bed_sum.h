#ifndef _BED_SUM_DEF
#define _BED_SUM_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct BED_SUM {
   short          abt;
   short          bed;
   DATE_STRUCT    dat;
   short          fil;
   short          kun_anz_x;
   short          kun_anz_xx;
   double         kun_sum_x;
   double         kun_sum_xx;
   short          mdn;
   short          minus_pos;
   double         minus_sum;
   short          pos_gew;
   short          pos_hnd;
   short          pos_o_nr;
   short          sto_pos_bws;
   short          sto_pos_dir;
   double         sto_sum_bws;
   double         sto_sum_dir;
   double         ums_gew;
   double         ums_hnd;
   double         ums_o_nr;
   short          verk_st;
   short          waa;
   short          host_trans;
};
extern struct BED_SUM bed_sum, bed_sum_null;

#line 8 "bed_sum.rh"

class BED_SUM_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               BED_SUM bed_sum;  
               BED_SUM_CLASS () : DB_CLASS ()
               {
               }
};
#endif
