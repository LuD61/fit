#ifndef _CHOICESYS_DEF
#define _CHOICESYS_DEF

#include "ChoiceX.h"
#include "SysList.h"
#include <vector>

class CChoiceSys : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
      
    public :
	    std::vector<CSysList *> SysList;
      	CChoiceSys(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceSys(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchAdrKrz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CSysList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
