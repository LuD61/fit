#include "StdAfx.h"
#include "formtab.h"


CFormTab *CFormTab::v;

CFormTab::CFormTab(void)
{
	v = this;
}

CFormTab::~CFormTab(void)
{
	FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) GetNext ()) != NULL)
	{
		delete f;
	}
}

void CFormTab::Show(void)
{
	FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) GetNext ()) != NULL)
	{
		f->Show ();
	}
}

void CFormTab::Get (void)
{
	FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) GetNext ()) != NULL)
	{
		f->Get ();
	}
}

CFormField *CFormTab::GetFormField (CWnd *ctrl)
{
	FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) GetNext ()) != NULL)
	{
		if (*f == ctrl) return f;
	}
	return NULL;
}

