#ifndef _MWST_SUM_DEF
#define _MWST_SUM_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct MWST_SUM {
   short          mdn;
   short          fil;
   short          verk_st;
   DATE_STRUCT    dat;
   short          mwst;
   double         mwst_betr;
   double         me_vk;
   long           me_vk_pos;
   double         ums_vk;
};
extern struct MWST_SUM mwst_sum, mwst_sum_null;

#line 8 "mwst_sum.rh"

class MWST_SUM_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               MWST_SUM mwst_sum;  
               MWST_SUM_CLASS () : DB_CLASS ()
               {
               }
};
#endif
