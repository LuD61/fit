#include "stdafx.h"
#include "ChoiceBed.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceBed::Sort1 = -1;
int CChoiceBed::Sort2 = -1;
int CChoiceBed::Sort3 = -1;

CChoiceBed::CChoiceBed(CWnd* pParent) 
        : CChoiceX(pParent)
{
}

CChoiceBed::~CChoiceBed() 
{
	DestroyList ();
}

void CChoiceBed::DestroyList() 
{
	for (std::vector<CBedList *>::iterator pabl = BedList.begin (); pabl != BedList.end (); ++pabl)
	{
		CBedList *abl = *pabl;
		delete abl;
	}
    BedList.clear ();
}

void CChoiceBed::FillList () 
{
    short bed;
    TCHAR bed_nam [29];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Bediener"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Bediener"),  1, 100, LVCFMT_RIGHT);
    SetCol (_T("Name"),  2, 250);

	if (BedList.size () == 0)
	{
		DbClass->sqlout ((short *)&bed,      SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR) bed_nam,   SQLCHAR, sizeof (bed_nam));
		DbClass->sqlin  ((short *)&mdn,      SQLSHORT, 0);
		DbClass->sqlin  ((short *)&fil,      SQLSHORT, 0);

		int cursor = DbClass->sqlcursor (_T("select bed, bed_nam ")
			                             _T("from bed where bed > 0 ")
										 _T("and mdn = ? ")
										 _T("and fil = ?"));

		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) bed_nam;
			CDbUniCode::DbToUniCode (bed_nam, pos);
			CBedList *abl = new CBedList (bed, bed_nam);
			BedList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CBedList *>::iterator pabl = BedList.begin (); pabl != BedList.end (); ++pabl)
	{
		CBedList *abl = *pabl;
		CString Bed;
		Bed.Format (_T("%hd"), abl->bed); 
		CString Num;
		CString Bez;
		_tcscpy (bed_nam, abl->bed_nam.GetBuffer ());

		CString LText;
		LText.Format (_T("%hd %s"), abl->bed, 
									abl->bed_nam.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Bed;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (bed_nam, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort (listView);
}


void CChoiceBed::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceBed::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceBed::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceBed::SearchAdrKrz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceBed::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchAdrKrz (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceBed::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceBed::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   return 0;
}


void CChoiceBed::Sort (CListCtrl *ListBox)
{
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CBedList *abl = BedList [i];
		   
		   abl->bed     = _tstoi (ListBox->GetItemText (i, 1));
		   abl->bed_nam = ListBox->GetItemText (i, 2);
	}
}

void CChoiceBed::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = BedList [idx];
}

CBedList *CChoiceBed::GetSelectedText ()
{
	CBedList *abl = (CBedList *) SelectedRow;
	return abl;
}

