#include "StdAfx.h"
#include "umsstorno.h"
#include "DbTime.h"

CUmsStorno::CUmsStorno(void)
{
//	dat;
	LbVerbuch = FALSE;
	mdn = 0;
	fil = 0;
	a = 0.0;
	ums_vk_mwst = 0.0;
	ums_vk = 0.0;
	me_vk = 0.0;
	me_vk_pos = 0.0;
	ums_vk_mwst_sa = 0.0;
	ums_vk_sa = 0.0;
	me_vk_sa = 0.0;
	me_vk_pos_sa = 0.0;
	wg = 0;
	wo = 0;
	mo = 0;

	atagcursor = -1;
	asatagcursor = -1;
	awocursor = -1;
	amocursor = -1;
	wgtagcursor = -1;
	wgwocursor = -1;
	wgmocursor = -1;

	ataglbcursor = -1;
	asataglbcursor = -1;
	awolbcursor = -1;
	amolbcursor = -1;
	wgtaglbcursor = -1;
	wgwolbcursor = -1;
	wgmolbcursor = -1;
	kasesumcursor = -1;
}

CUmsStorno::~CUmsStorno(void)
{
	DbClass.sqlclose (atagcursor);
	DbClass.sqlclose (asatagcursor);
	DbClass.sqlclose (awocursor);
	DbClass.sqlclose (amocursor);
	DbClass.sqlclose (wgtagcursor);
	DbClass.sqlclose (wgwocursor);
	DbClass.sqlclose (wgmocursor);

	DbClass.sqlclose (ataglbcursor);
	DbClass.sqlclose (asataglbcursor);
	DbClass.sqlclose (awolbcursor);
	DbClass.sqlclose (amolbcursor);
	DbClass.sqlclose (wgtaglbcursor);
	DbClass.sqlclose (wgwolbcursor);
	DbClass.sqlclose (wgmolbcursor);
	DbClass.sqlclose (kasesumcursor);
}

void CUmsStorno::Prepare ()
{
// Cursor f�r Umsatztabellen vorbereiten.

	if (atagcursor != -1) return;

	DbClass.sqlin ((double *) &ums_vk,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,      SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((DATE_STRUCT *)  &dat,  SQLDATE, 0);
	DbClass.sqlin ((double *)       &a,      SQLDOUBLE, 0);
	atagcursor = DbClass.sqlcursor (_T("update a_tag set ums_vk_tag = ums_vk_tag-?,")
		                                              _T("me_vk_tag = me_vk_tag-?,")                                       
		                                              _T("me_vk_tag_pos = me_vk_tag_pos-? ")                                       
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and dat = ? ")
									_T("and a = ?"));

	DbClass.sqlin ((double *) &ums_vk_sa,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk_sa,      SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos_sa,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((DATE_STRUCT *)  &dat,  SQLDATE, 0);
	DbClass.sqlin ((double *)       &a,    SQLDOUBLE, 0);
	asatagcursor = DbClass.sqlcursor (_T("update a_sa_tag set ums_vk_sa_tag = ums_vk_sa_tag-?,")
												 	   _T("me_vk_sa_tag = me_vk_sa_tag-?,")                                       
		                                               _T("me_vk_sa_tag_pos = me_vk_sa_tag_pos-? ")                                       
			                            _T("where mdn = ? ")
										_T("and fil = ? ")
										_T("and dat = ? ")
										_T("and a = ?"));

	DbClass.sqlin ((double *) &ums_vk,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,      SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &wo,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &year, SQLSHORT, 0);
	DbClass.sqlin ((double *)       &a,    SQLDOUBLE, 0);
	awocursor = DbClass.sqlcursor (_T("update a_wo set ums_vk_wo = ums_vk_wo-?,")
		                                              _T("me_vk_wo = me_vk_wo-?,")                                       
		                                              _T("me_vk_wo_pos = me_vk_wo_pos-? ")
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and wo = ? ")
									_T("and jr = ? ")
									_T("and a = ?"));


	DbClass.sqlin ((double *) &ums_vk,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,      SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &mo,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &year, SQLSHORT, 0);
	DbClass.sqlin ((double *)       &a,    SQLDOUBLE, 0);
	amocursor = DbClass.sqlcursor (_T("update a_mo set ums_vk_mo = ums_vk_mo-?,")
		                                              _T("me_vk_mo = me_vk_mo-?,")                                       
		                                              _T("me_vk_mo_pos = me_vk_mo_pos-? ")                                       
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and mo = ? ")
									_T("and jr = ? ")
									_T("and a = ?"));

	DbClass.sqlin ((double *) &ums_vk,        SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,         SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,     SQLLONG, 0);
	DbClass.sqlin ((double *) &ums_vk_sa,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk_sa,      SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos_sa,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((DATE_STRUCT *)  &dat,  SQLDATE, 0);
	DbClass.sqlin ((short *)        &wg,   SQLSHORT, 0);

	wgtagcursor = DbClass.sqlcursor (_T("update wg_tag set ums_vk_tag = ums_vk_tag-?,")
		                                              _T("me_vk_tag = me_vk_tag-?,")                                       
		                                              _T("me_vk_tag_pos = me_vk_tag_pos-?,")                                       
													  _T("ums_vk_sa_tag = ums_vk_sa_tag-?,")				
		                                              _T("me_vk_sa_tag = me_vk_sa_tag-?,")                                       
		                                              _T("me_vk_sa_tag_pos = me_vk_sa_tag_pos-? ")                                       
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and dat = ? ")
									_T("and wg = ?"));


	DbClass.sqlin ((double *) &ums_vk,        SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,         SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,     SQLLONG, 0);
	DbClass.sqlin ((double *) &ums_vk_sa,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk_sa,      SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos_sa,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &wo,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &year, SQLSHORT, 0);
	DbClass.sqlin ((short *)        &wg,   SQLSHORT, 0);
	wgwocursor = DbClass.sqlcursor (_T("update wg_wo set ums_vk_wo = ums_vk_wo-?,")
		                                              _T("me_vk_wo = me_vk_wo-?,")                                       
		                                              _T("me_vk_wo_pos = me_vk_wo_pos-?,")                                       
													  _T("ums_vk_sa_wo = ums_vk_sa_wo-?,")				
		                                              _T("me_vk_sa_wo = me_vk_sa_wo-?,")                                       
		                                              _T("me_vk_sa_wo_pos = me_vk_sa_wo_pos-? ")                                       
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and wo = ? ")
									_T("and jr = ? ")
									_T("and wg = ?"));

	DbClass.sqlin ((double *) &ums_vk,        SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,         SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,     SQLLONG, 0);
	DbClass.sqlin ((double *) &ums_vk_sa,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk_sa,      SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos_sa,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &mo,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &year, SQLSHORT, 0);
	DbClass.sqlin ((short *)        &wg,   SQLSHORT, 0);
	wgmocursor = DbClass.sqlcursor (_T("update wg_mo set ums_vk_mo = ums_vk_mo-?,")
		                                              _T("me_vk_mo = me_vk_mo-?,")                                       
		                                              _T("me_vk_mo_pos = me_vk_mo_pos-?,")                                       
													  _T("ums_vk_sa_mo = ums_vk_sa_mo-?,")				
		                                              _T("me_vk_sa_mo = me_vk_sa_mo-?,")                                       
		                                              _T("me_vk_sa_mo_pos = me_vk_sa_mo_pos-? ")                                       
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and mo = ? ")
									_T("and jr = ? ")
									_T("and wg = ?"));

// Cursor f�r Umsatztabellen f�r LB vorbereiten.

	DbClass.sqlin ((double *) &ums_vk,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,      SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &ums_vk_mwst,SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((DATE_STRUCT *)  &dat,  SQLDATE, 0);
	DbClass.sqlin ((double *)       &a,     SQLDOUBLE, 0);
	DbClass.sqlin ((short *)        &mwst,   SQLSHORT, 0);
	ataglbcursor = DbClass.sqlcursor (_T("update a_tag_lb set ums_vk_tag = ums_vk_tag-?,")
		                                              _T("me_vk_tag = me_vk_tag-?,")                                       
		                                              _T("ums_mwst = ums_mwst-?,")                                       
		                                              _T("me_vk_tag_pos = me_vk_tag_pos-? ")                                       
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and dat = ? ")
									_T("and a = ? ")
									_T("and mwst = ? "));

	DbClass.sqlin ((double *) &ums_vk_sa,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk_sa,      SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &ums_vk_mwst_sa,SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos_sa,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((DATE_STRUCT *)  &dat,  SQLDATE, 0);
	DbClass.sqlin ((double *)       &a,    SQLDOUBLE, 0);
	DbClass.sqlin ((short *)        &mwst, SQLSHORT, 0);
	asataglbcursor = DbClass.sqlcursor (_T("update a_sa_tag_lb set ums_vk_sa_tag = ums_vk_sa_tag-?,")
														 _T("me_vk_sa_tag = me_vk_sa_tag-?,")                                       
														 _T("ums_mwst = ums_mwst-?,")                                       
		                                                 _T("me_vk_sa_tag_pos = me_vk_sa_tag_pos-? ")                                       
			                            _T("where mdn = ? ")
										_T("and fil = ? ")
										_T("and dat = ? ")
										_T("and a = ? ")
										_T("and mwst = ?"));

	DbClass.sqlin ((double *) &ums_vk,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,      SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &ums_vk_mwst,SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &wo,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &year, SQLSHORT, 0);
	DbClass.sqlin ((double *)       &a,    SQLDOUBLE, 0);
	DbClass.sqlin ((short *)        &mwst, SQLSHORT, 0);
	awolbcursor = DbClass.sqlcursor (_T("update a_wo_lb set ums_vk_wo = ums_vk_wo-?,")
		                                              _T("me_vk_wo = me_vk_wo-?,")                                       
													  _T("ums_mwst = ums_mwst-?,")                                       
		                                              _T("me_vk_wo_pos = me_vk_wo_pos-? ")
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and wo = ? ")
									_T("and jr = ? ")
									_T("and a = ? ")
									_T("and mwst = ?"));

	DbClass.sqlin ((double *) &ums_vk,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,      SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &ums_vk_mwst,SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &mo,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &year, SQLSHORT, 0);
	DbClass.sqlin ((double *)       &a,    SQLDOUBLE, 0);
	DbClass.sqlin ((short *)        &mwst, SQLSHORT, 0);
	amolbcursor = DbClass.sqlcursor (_T("update a_mo_lb set ums_vk_mo = ums_vk_mo-?,")
		                                              _T("me_vk_mo = me_vk_mo-?,")                                       
													  _T("ums_mwst = ums_mwst-?,")                                       
		                                              _T("me_vk_mo_pos = me_vk_mo_pos-? ")                                       
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and mo = ? ")
									_T("and jr = ? ")
									_T("and a = ? ")
									_T("and mwst = ?"));

	DbClass.sqlin ((double *) &ums_vk,        SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,         SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &ums_vk_mwst,SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,     SQLLONG, 0);
	DbClass.sqlin ((double *) &ums_vk_sa,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk_sa,      SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos_sa,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((DATE_STRUCT *)  &dat,  SQLDATE, 0);
	DbClass.sqlin ((short *)        &wg,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &mwst, SQLSHORT, 0);

	wgtaglbcursor = DbClass.sqlcursor (_T("update wg_tag_lb set ums_vk_tag = ums_vk_tag-?,")
		                                              _T("me_vk_tag = me_vk_tag-?,")                                       
													  _T("ums_mwst = ums_mwst-?,")                                       
		                                              _T("me_vk_tag_pos = me_vk_tag_pos-?,")                                       
													  _T("ums_vk_sa_tag = ums_vk_sa_tag-?,")				
		                                              _T("me_vk_sa_tag = me_vk_sa_tag-?,")                                       
		                                              _T("me_vk_sa_tag_pos = me_vk_sa_tag_pos-? ")                                       
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and dat = ? ")
									_T("and wg = ? ")
									_T("and mwst = ?"));


	DbClass.sqlin ((double *) &ums_vk,        SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,         SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &ums_vk_mwst,   SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,     SQLLONG, 0);
	DbClass.sqlin ((double *) &ums_vk_sa,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk_sa,      SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos_sa,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &wo,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &year, SQLSHORT, 0);
	DbClass.sqlin ((short *)        &wg,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &mwst, SQLSHORT, 0);
	wgwolbcursor = DbClass.sqlcursor (_T("update wg_wo_lb set ums_vk_wo = ums_vk_wo-?,")
		                                              _T("me_vk_wo = me_vk_wo-?,")                                       
													  _T("ums_mwst = ums_mwst-?,")                                       
		                                              _T("me_vk_wo_pos = me_vk_wo_pos-?,")                                       
													  _T("ums_vk_sa_wo = ums_vk_sa_wo-?,")				
		                                              _T("me_vk_sa_wo = me_vk_sa_wo-?,")                                       
		                                              _T("me_vk_sa_wo_pos = me_vk_sa_wo_pos-? ")                                       
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and wo = ? ")
									_T("and jr = ? ")
									_T("and wg = ? ")
									_T("and mwst = ?"));

	DbClass.sqlin ((double *) &ums_vk,        SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk,         SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &ums_vk_mwst,   SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos,     SQLLONG, 0);
	DbClass.sqlin ((double *) &ums_vk_sa,     SQLDOUBLE, 0);
	DbClass.sqlin ((double *) &me_vk_sa,      SQLDOUBLE, 0);
	DbClass.sqlin ((long *)   &me_vk_pos_sa,  SQLLONG, 0);

	DbClass.sqlin ((short *)        &mdn,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &fil,  SQLSHORT, 0);
	DbClass.sqlin ((short *)        &mo,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &year, SQLSHORT, 0);
	DbClass.sqlin ((short *)        &wg,   SQLSHORT, 0);
	DbClass.sqlin ((short *)        &mwst, SQLSHORT, 0);
	wgmolbcursor = DbClass.sqlcursor (_T("update wg_mo_lb set ums_vk_mo = ums_vk_mo-?,")
		                                              _T("me_vk_mo = me_vk_mo-?,")                                       
													  _T("ums_mwst = ums_mwst-?,")                                       
		                                              _T("me_vk_mo_pos = me_vk_mo_pos-?,")                                       
													  _T("ums_vk_sa_mo = ums_vk_sa_mo-?,")				
		                                              _T("me_vk_sa_mo = me_vk_sa_mo-?,")                                       
		                                              _T("me_vk_sa_mo_pos = me_vk_sa_mo_pos-? ")                                       
                                    _T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and mo = ? ")
									_T("and jr = ? ")
									_T("and wg = ? ")
									_T("and mwst = ?"));
}

BOOL CUmsStorno::Update ()
{
	UpdateAtag ();
	UpdateASatag ();
	UpdateAWo ();
	UpdateAMo ();
	UpdateWgtag ();
	UpdateWgwo ();
	UpdateWgmo ();
	if (!LbVerbuch) return TRUE;

	UpdateAtagLb ();
	UpdateASatagLb ();
	UpdateAWoLb ();
	UpdateAMoLb ();
	UpdateWgtagLb ();
	UpdateWgwoLb ();
	UpdateWgmoLb ();

	return TRUE;
}
BOOL CUmsStorno::UpdateAtag ()
{
	DbClass.sqlexecute (atagcursor);
	return TRUE;
}
BOOL CUmsStorno::UpdateASatag ()
{
	if (ums_vk_sa != 0.0 || me_vk_sa != 0.0)
	{
		DbClass.sqlexecute (asatagcursor);
	}
	return TRUE;
}
BOOL CUmsStorno::UpdateAWo ()
{
	DbTime dbTime (&dat);
	wo = dbTime.GetWeek ();
	year = dbTime.GetYear ();
	DbClass.sqlexecute (awocursor);
	return TRUE;
}

BOOL CUmsStorno::UpdateAMo ()
{
	DbTime dbTime (&dat);
	mo = dbTime.GetMonth ();
	year = dbTime.GetYear ();
	DbClass.sqlexecute (amocursor);
	return TRUE;
}
BOOL CUmsStorno::UpdateWgtag ()
{
	wg = 0;
	A_bas.a_bas.a = a;
	if (A_bas.dbreadfirst () != 0)
	{
		return FALSE;
	}
	wg = A_bas.a_bas.wg;
	DbClass.sqlexecute (wgtagcursor);
	return TRUE;
}
BOOL CUmsStorno::UpdateWgwo ()
{
	wg = 0;
	A_bas.a_bas.a = a;
	if (A_bas.dbreadfirst () != 0)
	{
		return FALSE;
	}
	wg = A_bas.a_bas.wg;
	DbTime dbTime (&dat);
	wo = dbTime.GetWeek ();
	year = dbTime.GetYear ();
	DbClass.sqlexecute (wgwocursor);
	return TRUE;
}
BOOL CUmsStorno::UpdateWgmo ()
{
	wg = 0;
	A_bas.a_bas.a = a;
	if (A_bas.dbreadfirst () != 0)
	{
		return FALSE;
	}
	wg = A_bas.a_bas.wg;
	DbTime dbTime (&dat);
	mo = dbTime.GetMonth ();
	year = dbTime.GetYear ();
	DbClass.sqlexecute (wgmocursor);
	return TRUE;
}

BOOL CUmsStorno::UpdateAtagLb ()
{
	DbClass.sqlexecute (ataglbcursor);
	return TRUE;
}
BOOL CUmsStorno::UpdateASatagLb ()
{
	if (ums_vk_sa != 0.0 || me_vk_sa != 0.0)
	{
		DbClass.sqlexecute (asataglbcursor);
	}
	return TRUE;
}
BOOL CUmsStorno::UpdateAWoLb ()
{
	DbTime dbTime (&dat);
	wo = dbTime.GetWeek ();
	year = dbTime.GetYear ();
	DbClass.sqlexecute (awolbcursor);
	return TRUE;
}
BOOL CUmsStorno::UpdateAMoLb ()
{
	DbTime dbTime (&dat);
	mo = dbTime.GetMonth ();
	year = dbTime.GetYear ();
	DbClass.sqlexecute (amolbcursor);
	return TRUE;
}
BOOL CUmsStorno::UpdateWgtagLb ()
{
	wg = 0;
	A_bas.a_bas.a = a;
	if (A_bas.dbreadfirst () != 0)
	{
		return FALSE;
	}
	wg = A_bas.a_bas.wg;
	DbClass.sqlexecute (wgtaglbcursor);
	return TRUE;
}
BOOL CUmsStorno::UpdateWgwoLb ()
{
	wg = 0;
	A_bas.a_bas.a = a;
	if (A_bas.dbreadfirst () != 0)
	{
		return FALSE;
	}
	wg = A_bas.a_bas.wg;
	DbTime dbTime (&dat);
	wo = dbTime.GetWeek ();
	year = dbTime.GetYear ();
	DbClass.sqlexecute (wgwolbcursor);
	return TRUE;
}
BOOL CUmsStorno::UpdateWgmoLb ()
{
	wg = 0;
	A_bas.a_bas.a = a;
	if (A_bas.dbreadfirst () != 0)
	{
		return FALSE;
	}
	wg = A_bas.a_bas.wg;
	DbTime dbTime (&dat);
	mo = dbTime.GetMonth ();
	year = dbTime.GetYear ();
	DbClass.sqlexecute (wgmolbcursor);
	return TRUE;
}
