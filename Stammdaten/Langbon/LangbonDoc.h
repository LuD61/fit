// LangbonDoc.h : Schnittstelle der Klasse CLangbonDoc
//


#pragma once

class CLangbonDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CLangbonDoc();
	DECLARE_DYNCREATE(CLangbonDoc)

// Attribute
public:

// Operationen
public:

// Überschreibungen
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CLangbonDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


