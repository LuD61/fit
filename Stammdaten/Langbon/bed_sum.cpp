#include "stdafx.h"
#include "bed_sum.h"

struct BED_SUM bed_sum, bed_sum_null;

void BED_SUM_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &bed_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.bed,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &bed_sum.dat,  SQLDATE, 0);
    sqlout ((short *) &bed_sum.abt,SQLSHORT,0);
    sqlout ((short *) &bed_sum.bed,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &bed_sum.dat,SQLDATE,0);
    sqlout ((short *) &bed_sum.fil,SQLSHORT,0);
    sqlout ((short *) &bed_sum.kun_anz_x,SQLSHORT,0);
    sqlout ((short *) &bed_sum.kun_anz_xx,SQLSHORT,0);
    sqlout ((double *) &bed_sum.kun_sum_x,SQLDOUBLE,0);
    sqlout ((double *) &bed_sum.kun_sum_xx,SQLDOUBLE,0);
    sqlout ((short *) &bed_sum.mdn,SQLSHORT,0);
    sqlout ((short *) &bed_sum.minus_pos,SQLSHORT,0);
    sqlout ((double *) &bed_sum.minus_sum,SQLDOUBLE,0);
    sqlout ((short *) &bed_sum.pos_gew,SQLSHORT,0);
    sqlout ((short *) &bed_sum.pos_hnd,SQLSHORT,0);
    sqlout ((short *) &bed_sum.pos_o_nr,SQLSHORT,0);
    sqlout ((short *) &bed_sum.sto_pos_bws,SQLSHORT,0);
    sqlout ((short *) &bed_sum.sto_pos_dir,SQLSHORT,0);
    sqlout ((double *) &bed_sum.sto_sum_bws,SQLDOUBLE,0);
    sqlout ((double *) &bed_sum.sto_sum_dir,SQLDOUBLE,0);
    sqlout ((double *) &bed_sum.ums_gew,SQLDOUBLE,0);
    sqlout ((double *) &bed_sum.ums_hnd,SQLDOUBLE,0);
    sqlout ((double *) &bed_sum.ums_o_nr,SQLDOUBLE,0);
    sqlout ((short *) &bed_sum.verk_st,SQLSHORT,0);
    sqlout ((short *) &bed_sum.waa,SQLSHORT,0);
    sqlout ((short *) &bed_sum.host_trans,SQLSHORT,0);
            cursor = sqlcursor (_T("select bed_sum.abt,  ")
_T("bed_sum.bed,  bed_sum.dat,  bed_sum.fil,  bed_sum.kun_anz_x,  ")
_T("bed_sum.kun_anz_xx,  bed_sum.kun_sum_x,  bed_sum.kun_sum_xx,  ")
_T("bed_sum.mdn,  bed_sum.minus_pos,  bed_sum.minus_sum,  bed_sum.pos_gew,  ")
_T("bed_sum.pos_hnd,  bed_sum.pos_o_nr,  bed_sum.sto_pos_bws,  ")
_T("bed_sum.sto_pos_dir,  bed_sum.sto_sum_bws,  bed_sum.sto_sum_dir,  ")
_T("bed_sum.ums_gew,  bed_sum.ums_hnd,  bed_sum.ums_o_nr,  ")
_T("bed_sum.verk_st,  bed_sum.waa,  bed_sum.host_trans from bed_sum ")

#line 16 "bed_sum.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and bed = ? ")
                                  _T("and dat = ? "));
    sqlin ((short *) &bed_sum.abt,SQLSHORT,0);
    sqlin ((short *) &bed_sum.bed,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &bed_sum.dat,SQLDATE,0);
    sqlin ((short *) &bed_sum.fil,SQLSHORT,0);
    sqlin ((short *) &bed_sum.kun_anz_x,SQLSHORT,0);
    sqlin ((short *) &bed_sum.kun_anz_xx,SQLSHORT,0);
    sqlin ((double *) &bed_sum.kun_sum_x,SQLDOUBLE,0);
    sqlin ((double *) &bed_sum.kun_sum_xx,SQLDOUBLE,0);
    sqlin ((short *) &bed_sum.mdn,SQLSHORT,0);
    sqlin ((short *) &bed_sum.minus_pos,SQLSHORT,0);
    sqlin ((double *) &bed_sum.minus_sum,SQLDOUBLE,0);
    sqlin ((short *) &bed_sum.pos_gew,SQLSHORT,0);
    sqlin ((short *) &bed_sum.pos_hnd,SQLSHORT,0);
    sqlin ((short *) &bed_sum.pos_o_nr,SQLSHORT,0);
    sqlin ((short *) &bed_sum.sto_pos_bws,SQLSHORT,0);
    sqlin ((short *) &bed_sum.sto_pos_dir,SQLSHORT,0);
    sqlin ((double *) &bed_sum.sto_sum_bws,SQLDOUBLE,0);
    sqlin ((double *) &bed_sum.sto_sum_dir,SQLDOUBLE,0);
    sqlin ((double *) &bed_sum.ums_gew,SQLDOUBLE,0);
    sqlin ((double *) &bed_sum.ums_hnd,SQLDOUBLE,0);
    sqlin ((double *) &bed_sum.ums_o_nr,SQLDOUBLE,0);
    sqlin ((short *) &bed_sum.verk_st,SQLSHORT,0);
    sqlin ((short *) &bed_sum.waa,SQLSHORT,0);
    sqlin ((short *) &bed_sum.host_trans,SQLSHORT,0);
            sqltext = _T("update bed_sum set bed_sum.abt = ?,  ")
_T("bed_sum.bed = ?,  bed_sum.dat = ?,  bed_sum.fil = ?,  ")
_T("bed_sum.kun_anz_x = ?,  bed_sum.kun_anz_xx = ?,  ")
_T("bed_sum.kun_sum_x = ?,  bed_sum.kun_sum_xx = ?,  bed_sum.mdn = ?,  ")
_T("bed_sum.minus_pos = ?,  bed_sum.minus_sum = ?,  ")
_T("bed_sum.pos_gew = ?,  bed_sum.pos_hnd = ?,  bed_sum.pos_o_nr = ?,  ")
_T("bed_sum.sto_pos_bws = ?,  bed_sum.sto_pos_dir = ?,  ")
_T("bed_sum.sto_sum_bws = ?,  bed_sum.sto_sum_dir = ?,  ")
_T("bed_sum.ums_gew = ?,  bed_sum.ums_hnd = ?,  bed_sum.ums_o_nr = ?,  ")
_T("bed_sum.verk_st = ?,  bed_sum.waa = ?,  bed_sum.host_trans = ? ")

#line 22 "bed_sum.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and bed = ? ")
                                  _T("and dat = ? ");
            sqlin ((short *)   &bed_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.bed,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &bed_sum.dat,  SQLDATE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &bed_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.bed,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &bed_sum.dat,  SQLDATE, 0);
            test_upd_cursor = sqlcursor (_T("select bed from bed_sum ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and bed = ? ")
                                  _T("and dat = ? "));
            sqlin ((short *)   &bed_sum.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.fil,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.verk_st,  SQLSHORT, 0);
            sqlin ((short *)   &bed_sum.bed,  SQLSHORT, 0);
            sqlin ((DATE_STRUCT *)   &bed_sum.dat,  SQLDATE, 0);
            del_cursor = sqlcursor (_T("delete from bed_sum ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and verk_st = ? ")
                                  _T("and bed = ? ")
                                  _T("and dat = ? "));
    sqlin ((short *) &bed_sum.abt,SQLSHORT,0);
    sqlin ((short *) &bed_sum.bed,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &bed_sum.dat,SQLDATE,0);
    sqlin ((short *) &bed_sum.fil,SQLSHORT,0);
    sqlin ((short *) &bed_sum.kun_anz_x,SQLSHORT,0);
    sqlin ((short *) &bed_sum.kun_anz_xx,SQLSHORT,0);
    sqlin ((double *) &bed_sum.kun_sum_x,SQLDOUBLE,0);
    sqlin ((double *) &bed_sum.kun_sum_xx,SQLDOUBLE,0);
    sqlin ((short *) &bed_sum.mdn,SQLSHORT,0);
    sqlin ((short *) &bed_sum.minus_pos,SQLSHORT,0);
    sqlin ((double *) &bed_sum.minus_sum,SQLDOUBLE,0);
    sqlin ((short *) &bed_sum.pos_gew,SQLSHORT,0);
    sqlin ((short *) &bed_sum.pos_hnd,SQLSHORT,0);
    sqlin ((short *) &bed_sum.pos_o_nr,SQLSHORT,0);
    sqlin ((short *) &bed_sum.sto_pos_bws,SQLSHORT,0);
    sqlin ((short *) &bed_sum.sto_pos_dir,SQLSHORT,0);
    sqlin ((double *) &bed_sum.sto_sum_bws,SQLDOUBLE,0);
    sqlin ((double *) &bed_sum.sto_sum_dir,SQLDOUBLE,0);
    sqlin ((double *) &bed_sum.ums_gew,SQLDOUBLE,0);
    sqlin ((double *) &bed_sum.ums_hnd,SQLDOUBLE,0);
    sqlin ((double *) &bed_sum.ums_o_nr,SQLDOUBLE,0);
    sqlin ((short *) &bed_sum.verk_st,SQLSHORT,0);
    sqlin ((short *) &bed_sum.waa,SQLSHORT,0);
    sqlin ((short *) &bed_sum.host_trans,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into bed_sum (")
_T("abt,  bed,  dat,  fil,  kun_anz_x,  kun_anz_xx,  kun_sum_x,  kun_sum_xx,  mdn,  ")
_T("minus_pos,  minus_sum,  pos_gew,  pos_hnd,  pos_o_nr,  sto_pos_bws,  ")
_T("sto_pos_dir,  sto_sum_bws,  sto_sum_dir,  ums_gew,  ums_hnd,  ums_o_nr,  ")
_T("verk_st,  waa,  host_trans) ")

#line 57 "bed_sum.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 59 "bed_sum.rpp"
}
