// Langbon.h : Hauptheaderdatei f�r die Langbon-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error 'stdafx.h' muss vor dieser Datei in PCH eingeschlossen werden.
#endif

#include "resource.h"       // Hauptsymbole


// CLangbonApp:
// Siehe Langbon.cpp f�r die Implementierung dieser Klasse
//

class CLangbonApp : public CWinApp
{
public:
	CLangbonApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();
	afx_msg void OnFileNew ();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CLangbonApp theApp;
