#pragma once
#include "afxcmn.h"
#include "Vector.h"
#include "ListRowUpdate.h"

class CAttribute
{
public :
	enum
	{
		Label = 1,
		CheckBox = 2,
	} Att;

	int col;
    int attribute;
	CAttribute ();
	CAttribute (int col, int attribute)
	{
		this->col = col;
		this->attribute = attribute;
	}
};

class CAttributes : public CVector 
{
public:
	int GetAttribute (int col);
};

class CPaintListCtrl :
	public CListCtrl
{
protected:
	CAttributes Attributes;
	DECLARE_MESSAGE_MAP()
	BOOL IsAlternateColor;
	virtual void OnPaint ();
	virtual void OnLButtonDown (UINT cFlags, CPoint point);
    afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags );
    void PrintGridLines (CDC *cDC, int VLines, int HLines);
    void PrintCheckBox (CDC *cDC, CRect *ItemRect, CString& Text);
public:
	CListRowUpdate *ListRowUpdate;
	COLORREF Color;
	COLORREF AlternateColor;
	COLORREF SelectedColor;
	int VLines;
	int HLines;
	CPaintListCtrl(void);
	~CPaintListCtrl(void);
    void AddAttribute (int col, int attribute);
};
