#include "stdafx.h"
#include "lb.h"

LB lb, lb_null;


void LB_CLASS::prepare (void)
{
     TCHAR *sqltext;

     sqlin ((long *) &lb.sys, SQLLONG, 0);
     sqlin ((DATE_STRUCT *) &lb.dat, SQLDATE, 0);
     sqlin ((long *) &lb.bon, SQLLONG, 0);
     sqlin ((short *)  &lb.pos, SQLSHORT, 0);
     sqlin ((short *)  &lb.waa, SQLSHORT, 0);
     sqlin ((short *)  &lb.sto_kz, SQLSHORT, 0);
     sqlin ((double *) &lb.a, SQLDOUBLE, 0);
    sqlout ((long *) &lb.bon,SQLLONG,0);
    sqlout ((short *) &lb.abt,SQLSHORT,0);
    sqlout ((short *) &lb.waa,SQLSHORT,0);
    sqlout ((short *) &lb.bed,SQLSHORT,0);
    sqlout ((short *) &lb.buch_art,SQLSHORT,0);
    sqlout ((short *) &lb.wg,SQLSHORT,0);
    sqlout ((short *) &lb.par3_sint,SQLSHORT,0);
    sqlout ((short *) &lb.par4_sint,SQLSHORT,0);
    sqlout ((short *) &lb.mwst_verbu,SQLSHORT,0);
    sqlout ((short *) &lb.mwst,SQLSHORT,0);
    sqlout ((double *) &lb.pr_vk,SQLDOUBLE,0);
    sqlout ((double *) &lb.ums_vk,SQLDOUBLE,0);
    sqlout ((long *) &lb.me_vk_pos,SQLLONG,0);
    sqlout ((double *) &lb.me_vk,SQLDOUBLE,0);
    sqlout ((double *) &lb.a,SQLDOUBLE,0);
    sqlout ((double *) &lb.lief_me,SQLDOUBLE,0);
    sqlout ((short *) &lb.stat,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &lb.dat,SQLDATE,0);
    sqlout ((char *) lb.zeit,SQLCHAR,7);
    sqlout ((short *) &lb.mdn,SQLSHORT,0);
    sqlout ((short *) &lb.fil,SQLSHORT,0);
    sqlout ((long *) &lb.sys,SQLLONG,0);
    sqlout ((short *) &lb.fitok,SQLSHORT,0);
    sqlout ((short *) &lb.pos,SQLSHORT,0);
    sqlout ((long *) &lb.a_krz,SQLLONG,0);
    sqlout ((short *) &lb.nr,SQLSHORT,0);
    sqlout ((double *) &lb.ums_mwst,SQLDOUBLE,0);
    sqlout ((short *) &lb.sa,SQLSHORT,0);
    sqlout ((double *) &lb.pr_ek,SQLDOUBLE,0);
    sqlout ((double *) &lb.pr_vk_waa,SQLDOUBLE,0);
    sqlout ((double *) &lb.sto_me_vk,SQLDOUBLE,0);
    sqlout ((long *) &lb.sto_vk_pos,SQLLONG,0);
    sqlout ((double *) &lb.sto_ums_vk,SQLDOUBLE,0);
    sqlout ((double *) &lb.sto_ums_mwst,SQLDOUBLE,0);
    sqlout ((short *) &lb.sto_fit,SQLSHORT,0);
    sqlout ((short *) &lb.sto_kz,SQLSHORT,0);
    sqlout ((short *) &lb.kase,SQLSHORT,0);
     cursor = sqlcursor (_T("select lb.bon,  lb.abt,  lb.waa,  lb.bed,  "
"lb.buch_art,  lb.wg,  lb.par3_sint,  lb.par4_sint,  lb.mwst_verbu,  lb.mwst,  "
"lb.pr_vk,  lb.ums_vk,  lb.me_vk_pos,  lb.me_vk,  lb.a,  lb.lief_me,  lb.stat,  "
"lb.dat,  lb.zeit,  lb.mdn,  lb.fil,  lb.sys,  lb.fitok,  lb.pos,  lb.a_krz,  lb.nr,  "
"lb.ums_mwst,  lb.sa,  lb.pr_ek,  lb.pr_vk_waa,  lb.sto_me_vk,  "
"lb.sto_vk_pos,  lb.sto_ums_vk,  lb.sto_ums_mwst,  lb.sto_fit,  lb.sto_kz,  "
"lb.kase from lb ")

#line 19 "lb.rpp"
                           _T("where sys = ? ")
                           _T("and dat = ? ")
                           _T("and bon = ? ")
                           _T("and pos = ? ")
                           _T("and waa = ? ")
                           _T("and sto_kz = ? ")
                           _T("and a = ?"));


    sqlin ((long *) &lb.bon,SQLLONG,0);
    sqlin ((short *) &lb.abt,SQLSHORT,0);
    sqlin ((short *) &lb.waa,SQLSHORT,0);
    sqlin ((short *) &lb.bed,SQLSHORT,0);
    sqlin ((short *) &lb.buch_art,SQLSHORT,0);
    sqlin ((short *) &lb.wg,SQLSHORT,0);
    sqlin ((short *) &lb.par3_sint,SQLSHORT,0);
    sqlin ((short *) &lb.par4_sint,SQLSHORT,0);
    sqlin ((short *) &lb.mwst_verbu,SQLSHORT,0);
    sqlin ((short *) &lb.mwst,SQLSHORT,0);
    sqlin ((double *) &lb.pr_vk,SQLDOUBLE,0);
    sqlin ((double *) &lb.ums_vk,SQLDOUBLE,0);
    sqlin ((long *) &lb.me_vk_pos,SQLLONG,0);
    sqlin ((double *) &lb.me_vk,SQLDOUBLE,0);
    sqlin ((double *) &lb.a,SQLDOUBLE,0);
    sqlin ((double *) &lb.lief_me,SQLDOUBLE,0);
    sqlin ((short *) &lb.stat,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &lb.dat,SQLDATE,0);
    sqlin ((char *) lb.zeit,SQLCHAR,7);
    sqlin ((short *) &lb.mdn,SQLSHORT,0);
    sqlin ((short *) &lb.fil,SQLSHORT,0);
    sqlin ((long *) &lb.sys,SQLLONG,0);
    sqlin ((short *) &lb.fitok,SQLSHORT,0);
    sqlin ((short *) &lb.pos,SQLSHORT,0);
    sqlin ((long *) &lb.a_krz,SQLLONG,0);
    sqlin ((short *) &lb.nr,SQLSHORT,0);
    sqlin ((double *) &lb.ums_mwst,SQLDOUBLE,0);
    sqlin ((short *) &lb.sa,SQLSHORT,0);
    sqlin ((double *) &lb.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &lb.pr_vk_waa,SQLDOUBLE,0);
    sqlin ((double *) &lb.sto_me_vk,SQLDOUBLE,0);
    sqlin ((long *) &lb.sto_vk_pos,SQLLONG,0);
    sqlin ((double *) &lb.sto_ums_vk,SQLDOUBLE,0);
    sqlin ((double *) &lb.sto_ums_mwst,SQLDOUBLE,0);
    sqlin ((short *) &lb.sto_fit,SQLSHORT,0);
    sqlin ((short *) &lb.sto_kz,SQLSHORT,0);
    sqlin ((short *) &lb.kase,SQLSHORT,0);
     sqltext = _T("update lb set lb.bon = ?,  lb.abt = ?,  "
"lb.waa = ?,  lb.bed = ?,  lb.buch_art = ?,  lb.wg = ?,  lb.par3_sint = ?,  "
"lb.par4_sint = ?,  lb.mwst_verbu = ?,  lb.mwst = ?,  lb.pr_vk = ?,  "
"lb.ums_vk = ?,  lb.me_vk_pos = ?,  lb.me_vk = ?,  lb.a = ?,  "
"lb.lief_me = ?,  lb.stat = ?,  lb.dat = ?,  lb.zeit = ?,  lb.mdn = ?,  "
"lb.fil = ?,  lb.sys = ?,  lb.fitok = ?,  lb.pos = ?,  lb.a_krz = ?,  "
"lb.nr = ?,  lb.ums_mwst = ?,  lb.sa = ?,  lb.pr_ek = ?,  "
"lb.pr_vk_waa = ?,  lb.sto_me_vk = ?,  lb.sto_vk_pos = ?,  "
"lb.sto_ums_vk = ?,  lb.sto_ums_mwst = ?,  lb.sto_fit = ?,  "
"lb.sto_kz = ?,  lb.kase = ? ") 

#line 29 "lb.rpp"
                           _T("where sys = ? ")
                           _T("and dat = ? ")
                           _T("and bon = ? ")
                           _T("and pos = ? ")
                           _T("and waa = ? ")
                           _T("and sto_kz = ? ")
                           _T("and a = ?");
     sqlin ((long *) &lb.sys, SQLLONG, 0);
     sqlin ((DATE_STRUCT *) &lb.dat, SQLDATE, 0);
     sqlin ((long *) &lb.bon, SQLLONG, 0);
     sqlin ((short *)  &lb.pos, SQLSHORT, 0);
     sqlin ((short *)  &lb.waa, SQLSHORT, 0);
     sqlin ((short *)  &lb.sto_kz, SQLSHORT, 0);
     sqlin ((double *) &lb.a, SQLDOUBLE, 0);
     upd_cursor  = sqlcursor (sqltext); 

    sqlin ((long *) &lb.bon,SQLLONG,0);
    sqlin ((short *) &lb.abt,SQLSHORT,0);
    sqlin ((short *) &lb.waa,SQLSHORT,0);
    sqlin ((short *) &lb.bed,SQLSHORT,0);
    sqlin ((short *) &lb.buch_art,SQLSHORT,0);
    sqlin ((short *) &lb.wg,SQLSHORT,0);
    sqlin ((short *) &lb.par3_sint,SQLSHORT,0);
    sqlin ((short *) &lb.par4_sint,SQLSHORT,0);
    sqlin ((short *) &lb.mwst_verbu,SQLSHORT,0);
    sqlin ((short *) &lb.mwst,SQLSHORT,0);
    sqlin ((double *) &lb.pr_vk,SQLDOUBLE,0);
    sqlin ((double *) &lb.ums_vk,SQLDOUBLE,0);
    sqlin ((long *) &lb.me_vk_pos,SQLLONG,0);
    sqlin ((double *) &lb.me_vk,SQLDOUBLE,0);
    sqlin ((double *) &lb.a,SQLDOUBLE,0);
    sqlin ((double *) &lb.lief_me,SQLDOUBLE,0);
    sqlin ((short *) &lb.stat,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &lb.dat,SQLDATE,0);
    sqlin ((char *) lb.zeit,SQLCHAR,7);
    sqlin ((short *) &lb.mdn,SQLSHORT,0);
    sqlin ((short *) &lb.fil,SQLSHORT,0);
    sqlin ((long *) &lb.sys,SQLLONG,0);
    sqlin ((short *) &lb.fitok,SQLSHORT,0);
    sqlin ((short *) &lb.pos,SQLSHORT,0);
    sqlin ((long *) &lb.a_krz,SQLLONG,0);
    sqlin ((short *) &lb.nr,SQLSHORT,0);
    sqlin ((double *) &lb.ums_mwst,SQLDOUBLE,0);
    sqlin ((short *) &lb.sa,SQLSHORT,0);
    sqlin ((double *) &lb.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &lb.pr_vk_waa,SQLDOUBLE,0);
    sqlin ((double *) &lb.sto_me_vk,SQLDOUBLE,0);
    sqlin ((long *) &lb.sto_vk_pos,SQLLONG,0);
    sqlin ((double *) &lb.sto_ums_vk,SQLDOUBLE,0);
    sqlin ((double *) &lb.sto_ums_mwst,SQLDOUBLE,0);
    sqlin ((short *) &lb.sto_fit,SQLSHORT,0);
    sqlin ((short *) &lb.sto_kz,SQLSHORT,0);
    sqlin ((short *) &lb.kase,SQLSHORT,0);
     ins_cursor = sqlcursor (_T("insert into lb (bon,  abt,  waa,  "
"bed,  buch_art,  wg,  par3_sint,  par4_sint,  mwst_verbu,  mwst,  pr_vk,  ums_vk,  "
"me_vk_pos,  me_vk,  a,  lief_me,  stat,  dat,  zeit,  mdn,  fil,  sys,  fitok,  pos,  a_krz,  nr,  "
"ums_mwst,  sa,  pr_ek,  pr_vk_waa,  sto_me_vk,  sto_vk_pos,  sto_ums_vk,  "
"sto_ums_mwst,  sto_fit,  sto_kz,  kase) ")

#line 46 "lb.rpp"
                              _T("values ")
                              _T("(?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 48 "lb.rpp"

     sqlin ((long *) &lb.sys, SQLLONG, 0);
     sqlin ((DATE_STRUCT *) &lb.dat, SQLDATE, 0);
     sqlin ((long *) &lb.bon, SQLLONG, 0);
     sqlin ((short *)  &lb.pos, SQLSHORT, 0);
     sqlin ((short *)  &lb.waa, SQLSHORT, 0);
     sqlin ((short *)  &lb.sto_kz, SQLSHORT, 0);
     sqlin ((double *) &lb.a, SQLDOUBLE, 0);
     test_upd_cursor = sqlcursor (_T("select lb.bon from lb ")
                           _T("where sys = ? ")
                           _T("and dat = ? ")
                           _T("and bon = ? ")
                           _T("and pos = ? ")
                           _T("and waa = ? ")
                           _T("and sto_kz = ? ")
                           _T("and a = ?"));
     sqlin ((long *) &lb.sys, SQLLONG, 0);
     sqlin ((DATE_STRUCT *) &lb.dat, SQLDATE, 0);
     sqlin ((long *) &lb.bon, SQLLONG, 0);
     sqlin ((short *)  &lb.pos, SQLSHORT, 0);
     sqlin ((short *)  &lb.waa, SQLSHORT, 0);
     sqlin ((short *)  &lb.sto_kz, SQLSHORT, 0);
     sqlin ((double *) &lb.a, SQLDOUBLE, 0);
     del_cursor = sqlcursor (_T("delete from lb ")
                           _T("where sys = ? ")
                           _T("and dat = ? ")
                           _T("and bon = ? ")
                           _T("and pos = ? ")
                           _T("and waa = ? ")
                           _T("and sto_kz = ? ")
                           _T("and a = ?"));
     sqlin ((long *) &lb.sys, SQLLONG, 0);
     sqlin ((DATE_STRUCT *) &lb.dat, SQLDATE, 0);
     sqlin ((long *) &lb.bon, SQLLONG, 0);
     sqlin ((short *)  &lb.pos, SQLSHORT, 0);
     sqlin ((short *)  &lb.waa, SQLSHORT, 0);
     sqlin ((double *) &lb.a, SQLDOUBLE, 0);
     del_acursor = sqlcursor (_T("delete from lb ")
                           _T("where sys = ? ")
                           _T("and dat = ? ")
                           _T("and bon = ? ")
                           _T("and pos = ? ")
                           _T("and waa = ? ")
                           _T("and a = ?"));
}


int LB_CLASS::dbdeletea ()
{
     if (del_acursor == 1)
     {
             prepare ();
     }
     if (del_acursor == -1) return -1;
     return sqlexecute (del_acursor); 
}
     