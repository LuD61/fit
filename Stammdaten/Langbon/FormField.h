#pragma once
#ifndef _FORMFIELD_DEF
#define _FORMFIELD_DEF

#include <vector>

#define EDIT 1
#define CHECKBOX 2
#define COMBOBOX 3

#define VCHAR 0
#define VSHORT 1
#define VLONG 2
#define VDOUBLE 3
#define VDATE 4
#define VSTRING 5

class CFormField
{
public:
    CWnd *Control;
	int CtrlType;
	void *Vadr;
	int VType;
	int Scale;
	int len;
	std::vector<CString *> ComboValues;
	CFormField(void);
	CFormField(CWnd *, int, void *, int);
	CFormField(CWnd *, int, void *, int, int, int);
	~CFormField(void);
	void FillComboBox ();
    void DestroyComboBox ();
    BOOL operator== (CWnd *);
    BOOL equals (CWnd *);
	void SetSel (int);
	void SetTextSelected (CString&);
	BOOL GetSelectedText (CString&);
	virtual void Get ();
	virtual void Show ();
	virtual void OemToAnsi (CString&);
	virtual void AnsiToOem (CString&);
	virtual void OemToAnsi (LPSTR);
	virtual void AnsiToOem (LPSTR);
};
#endif
