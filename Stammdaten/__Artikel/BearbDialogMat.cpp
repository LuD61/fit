// BearbDialogMat.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "BearbDialogMat.h"

// CBearbDialogMat-Dialogfeld

IMPLEMENT_DYNAMIC(CBearbDialogMat, CDialog)

CBearbDialogMat::CBearbDialogMat(CWnd* pParent /*=NULL*/)
	: CDialog(CBearbDialogMat::IDD, pParent)
{
	A_bas = NULL;
	A_kalk_mat = NULL;
	StdCellHeight = 15;
	MarktSpPar = Aufschlag;
	mwst = 1.0;
	visible = FALSE;
}

CBearbDialogMat::~CBearbDialogMat()
{
}

void CBearbDialogMat::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMAT_O_B, m_LMatoB);
	DDX_Control(pDX, IDC_LBEARB_WEG, m_LBearbWeg);
	DDX_Control(pDX, IDC_LHK_VOLLK, m_LHkVollk);

	DDX_Control(pDX, IDC_MAT_O_B, m_MatoB);
	DDX_Control(pDX, IDC_BEARB_WEG, m_BearbWeg);
	DDX_Control(pDX, IDC_HK_VOLLK, m_HkVollk);

}


BEGIN_MESSAGE_MAP(CBearbDialogMat, CDialog)
//	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CBearbDialogMat-Meldungshandler

BOOL CBearbDialogMat::OnInitDialog ()
{
	CDialog::OnInitDialog ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	Form.Add (new CFormField (&m_MatoB,EDIT,    (double *) &A_kalk_mat->a_kalk_mat.mat_o_b, VDOUBLE, 7,3));

	Form.Add (new CFormField (&m_BearbWeg, EDIT,  (short *)  &A_kalk_mat->a_kalk_mat.bearb_sk, VSHORT));
	Form.Add (new CFormField (&m_HkVollk,  EDIT,  (double *) &A_kalk_mat->a_kalk_mat.hk_vollk, VDOUBLE, 7,3));

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LMatoB     = new CCtrlInfo (&m_LMatoB, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LMatoB);

	CCtrlInfo *c_MatoB     = new CCtrlInfo (&m_MatoB, 1, 1, 1, 1); 
	CtrlGrid.Add (c_MatoB);

	CCtrlInfo *c_LBearbWeg     = new CCtrlInfo (&m_LBearbWeg, 1, 3, 1, 1); 
	CtrlGrid.Add (c_LBearbWeg);
	CCtrlInfo *c_LHkVollk     = new CCtrlInfo (&m_LHkVollk, 2, 3, 1, 1); 
	CtrlGrid.Add (c_LHkVollk);

	CCtrlInfo *c_BearbWeg     = new CCtrlInfo (&m_BearbWeg, 1, 4, 1, 1); 
	CtrlGrid.Add (c_BearbWeg);

	CCtrlInfo *c_HkVollk     = new CCtrlInfo (&m_HkVollk, 2, 4, 1, 1); 
	CtrlGrid.Add (c_HkVollk);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);

	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display (); 
	return TRUE;
}

void CBearbDialogMat::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}


BOOL CBearbDialogMat::PreTranslateMessage(MSG* pMsg)
{
	return FALSE;
}

void CBearbDialogMat::Show ()
{
	if (!visible) return;
	Form.Show ();
	Calculate ();
}

void CBearbDialogMat::Get ()
{
	if (!visible) return;
	Calculate ();
	Form.Get ();
}

void CBearbDialogMat::SetVisible (BOOL visible)
{
	this->visible = visible;
	if (visible)
	{
		ShowWindow (SW_SHOWNORMAL);
	}
	else
	{
		ShowWindow (SW_HIDE);
	}
}

void CBearbDialogMat::Calculate ()
{
	Form.Get ();
	Form.Show ();
}