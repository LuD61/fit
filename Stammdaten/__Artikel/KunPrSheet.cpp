#include "StdAfx.h"
#include "kunprsheet.h"
#include "DbPropertyPage.h"

IMPLEMENT_DYNAMIC(CKunPrSheet, CDbPropertySheet)

CKunPrSheet::CKunPrSheet(void)
{
}

CKunPrSheet::~CKunPrSheet(void)
{
}

BOOL CKunPrSheet::Write ()
{
//	CDbPropertySheet::Write ();
	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	if (Page == NULL) return FALSE;
	BOOL ret = Page->Write ();
	if (ret )Page->AfterWrite ();

	int idx = GetActiveIndex ();
	if (idx == 0)
	{
		int count = GetPageCount ();
		for (int i = 1; i < count; i ++)
		{
			CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (i);
			if (IsWindow (Page->m_hWnd))
			{
				Page->read ();
			}
		}
	}
	else
	{
		CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
		if (IsWindow (Page->m_hWnd))
		{
				Page->read ();
		}
	}
	return ret;
}

