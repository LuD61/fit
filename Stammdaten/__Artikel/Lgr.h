#ifndef _LGR_DEF
#define _LGR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LGR {
   short          lgr;
   short          mdn;
   short          fil;
   TCHAR          lgr_smt_kz[4];
   TCHAR          lgr_bz[25];
   short          lgr_kz;
   short          lgr_gr;
   TCHAR          lgr_kla[2];
   TCHAR          abr_period[2];
   long           adr;
   short          afl;
   TCHAR          bli_kz[2];
   DATE_STRUCT    dat_ero;
   double         fl_lad;
   double         fl_nto;
   double         fl_vk_ges;
   short          frm;
   DATE_STRUCT    iakv;
   TCHAR          inv_rht[2];
   TCHAR          ls_abgr[2];
   TCHAR          ls_kz[2];
   short          ls_sum;
   TCHAR          pers[13];
   short          pers_anz;
   TCHAR          pos_kum[2];
   TCHAR          pr_ausw[2];
   TCHAR          pr_bel_entl[2];
   TCHAR          pr_lgr_kz[2];
   long           pr_lst;
   TCHAR          pr_vk_kz[2];
   double         reg_bed_theke_lng;
   double         reg_kt_lng;
   double         reg_kue_lng;
   double         reg_lng;
   double         reg_tks_lng;
   double         reg_tkt_lng;
   TCHAR          smt_kz[2];
   short          sonst_einh;
   short          sprache;
   TCHAR          sw_kz[2];
   long           tou;
   short          vrs_typ;
   TCHAR          inv_akv[2];
   short          delstatus;
};
extern struct LGR lgr, lgr_null;

#line 8 "Lgr.rh"

class LGR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LGR lgr;
               LGR_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (LGR&);  
};
#endif
