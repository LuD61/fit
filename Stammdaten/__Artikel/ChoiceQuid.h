#ifndef _CHOICEQUID_DEF
#define _CHOICEQUID_DEF

#include "ChoiceX.h"
#include "QuidList.h"
#include <vector>

class CChoiceQuid : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
	protected:     
		DECLARE_MESSAGE_MAP()

    public :
	    std::vector<CQuidList *> QuidList;
      	CChoiceQuid(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceQuid(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchQuidNr (CListCtrl *,  LPTSTR);
        void SearchTxt (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CQuidList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		virtual void OnEnter ();
};
#endif
