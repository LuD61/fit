#include "StdAfx.h"
#include "DllPreise.h"

HANDLE CDllPreise::PriceLib = NULL;

CDllPreise::CDllPreise ()
{
    SetPriceDbName =NULL;
	preise_holen = NULL;
	CString Bws;

	BOOL bws = Bws.GetEnvironmentVariable (_T("bws"));

	CString PreisDll;
	if (bws)
	{
		PreisDll.Format (_T("%s\\bin\\preisewa.dll"), Bws.GetBuffer ());
	}
	else
	{
		PreisDll = _T("preisewa.dll");
	}
	PriceLib = LoadLibrary (PreisDll.GetBuffer ());
	if (PriceLib != NULL)
	{
		preise_holen = (int (*) (short, short, short, int ,double, 
		            LPSTR, short*, double *, double *)) 
					GetProcAddress ((HMODULE) PriceLib, "preise_holen");

		fetch_preis_tag = (int (*) (short, short, short, short, double, 
		                   LPSTR, double *, double *)) 
					GetProcAddress ((HMODULE) PriceLib, "fetch_preis_tag");

		fetch_preis_lad = (int (*) (short, short, short, short, double, 
						   double *, double *)) 
					GetProcAddress ((HMODULE) PriceLib, "fetch_preis_lad");

		SetPriceDbName = (int (*) (LPSTR))
					GetProcAddress ((HMODULE) PriceLib, "SetPriceDbName");
		if (SetPriceDbName != NULL)
		{
			(*SetPriceDbName) ("bws");
		}
	}
}

