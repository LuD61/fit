#pragma once
#include "NumEdit.h"
#include "TextEdit.h"
#include "a_bas.h"
#include "a_kalkhndw.h"
#include "a_kalk_eig.h"
#include "a_kalk_mat.h"
#include "a_kalkpreis.h"
#include "a_pr.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "CtrlGrid.h"
#include "DbUniCode.h"
#include "StaticButton.h"
#include "Mdn.h"
#include "Fil.h"
#include "Adr.h"
#include "ChoiceMdn.h"
#include "ChoiceFil.h"
#include "ptabn.h"
#include "SpannDialog.h"
#include "BearbDialog.h"
#include "SpannDialogHndw.h"
#include "BearbDialogHndw.h"
#include "SpannDialogMat.h"
#include "BearbDialogMat.h"
#include "Sys_par.h"

#define IDC_KALKSAVE 3010
#define IDC_KALKDELETE 3011

#ifndef IDC_MDNCHOICE
#define IDC_MDNCHOICE 3001
#endif

#ifndef IDC_FILCHOICE
#define IDC_FILCHOICE 3002
#endif

// CKalkDialog-Dialogfeld

class CKalkDialog : public CDialog
{
	DECLARE_DYNAMIC(CKalkDialog)

public:
	CKalkDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CKalkDialog();

// Dialogfelddaten
	enum { IDD = IDD_KALK_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	BOOL WindowIsClipped;
	BOOL AprFound;
public:
	int MarktSpPar;
	short a_typ;
    A_BAS_CLASS *A_bas;
	PTABN_CLASS Ptabn;
	SYS_PAR_CLASS Sys_par;
    MDN_CLASS Mdn;
    FIL_CLASS Fil;
    ADR_CLASS MdnAdr;
    ADR_CLASS FilAdr;
	A_KALKHNDW_CLASS A_kalkhndw;
	A_KALK_EIG_CLASS A_kalk_eig;
	A_KALK_MAT_CLASS A_kalk_mat;
	A_KALKPREIS_CLASS A_kalkpreis;
	APR_CLASS A_pr;
	CChoiceMdn *ChoiceMdn;
	CChoiceFil *ChoiceFil;
 
	CStaticButton m_Save;
	CStaticButton m_Delete;
	CStatic m_KalkGroup;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	CEdit m_MdnName;
	CStatic m_LFil;
	CNumEdit m_Fil;
	CButton m_FilChoice;
	CEdit m_FilName;

	CStatic m_LKostKz;
	CComboBox m_KostKz;

	CSpannDialog m_SpannDialog;
	CBearbDialog m_BearbDialog;
	CSpannDialogHndw m_SpannDialogHndw;
	CBearbDialogHndw m_BearbDialogHndw;
	CSpannDialogMat m_SpannDialogMat;
	CBearbDialogMat m_BearbDialogMat;

	CFormTab Form;
	CFormTab Keys;
	CFont Font;
	CCtrlGrid CtrlGrid;
	CCtrlGrid KeyGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid FilGrid;
	CCtrlGrid ToolGrid;
	virtual BOOL OnReturn ();
	virtual BOOL OnKeyup ();
	virtual void OnSave ();
	virtual void Delete ();
	BOOL ReadMdn ();
	BOOL ReadFil ();
	BOOL Read ();
    void OnMdnchoice(); 
    void OnFilchoice(); 
	void FillCombo (LPTSTR Item, CWnd *control);
	void ReadApr ();
	void SelectPage ();
public:
	afx_msg void OnCbnSelchangeKostKz();
	void ClipWindow ();
};
