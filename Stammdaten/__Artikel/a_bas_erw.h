#ifndef _A_BAS_ERW_DEF
#define _A_BAS_ERW_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_BAS_ERW {
   double         a;
   TCHAR          pp_a_bz1[100];
   TCHAR          pp_a_bz2[100];
   TCHAR          lgr_tmpr[100];
   TCHAR          lupine[2];
   TCHAR          schutzgas[2];
   short          huelle;
   short          shop_wg1;
   short          shop_wg2;
   short          shop_wg3;
   short          shop_wg4;
   short          shop_wg5;
   double         tara2;
   short          a_tara2;
   double         tara3;
   short          a_tara3;
   double         tara4;
   short          a_tara4;
   double         salz;
   double         davonfett;
   double         davonzucker;
   double         ballaststoffe;
   TCHAR          shop_neu[2];
   TCHAR          shop_tv[2];
   double         shop_agew;
   TCHAR          a_bez[63];
   TCHAR          shop_aktion[2];
   short          userdef1;
   short          userdef2;
   short          userdef3;
   TCHAR          zutat[2001];
   short          sg;
   short          minstaffgr;
   short          l_pack;
   short          b_pack;
   short          h_pack;
   short          karton_id;
   DATE_STRUCT    shop_neu_bis;
};
extern struct A_BAS_ERW a_bas_erw, a_bas_erw_null;

#line 8 "a_bas_erw.rh"

class A_BAS_ERW_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               A_BAS_ERW a_bas_erw;
               A_BAS_ERW_CLASS () : DB_CLASS ()
               {
               }
               int dbreadzutat (double a);  
               int dbupdatezutat (double a);  
};
#endif
