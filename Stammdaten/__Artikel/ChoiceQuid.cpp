#include "stdafx.h"
#include "ChoiceQuid.h"
#include "DbUniCode.h"
#include "Process.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceQuid::Sort1 = -1;
int CChoiceQuid::Sort2 = -1;
int CChoiceQuid::Sort3 = -1;
int CChoiceQuid::Sort4 = -1;

BEGIN_MESSAGE_MAP(CChoiceQuid, CChoiceX)
END_MESSAGE_MAP()

CChoiceQuid::CChoiceQuid(CWnd* pParent) 
        : CChoiceX(pParent)
{
	Bean.ArchiveName = _T("QuidList.prp");
}

CChoiceQuid::~CChoiceQuid() 
{
	DestroyList ();
}

void CChoiceQuid::DestroyList() 
{
	for (std::vector<CQuidList *>::iterator pabl = QuidList.begin (); pabl != QuidList.end (); ++pabl)
	{
		CQuidList *abl = *pabl;
		delete abl;
	}
    QuidList.clear ();
}

void CChoiceQuid::FillList () 
{
    short  quid_nr;
    TCHAR  bezeichnung [81];

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Quid"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Quid-Nr"),       1,  40, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),   2, 400);

	if (QuidList.size () == 0)
	{
		DbClass->sqlout ((short *) &quid_nr,      SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR) bezeichnung,    SQLCHAR, sizeof (bezeichnung));
		int cursor = DbClass->sqlcursor (_T("select quid_nr, bezeichnung ")
			                             _T("from quid where quid_nr > 0"));

		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) bezeichnung;
			CDbUniCode::DbToUniCode (bezeichnung, pos);
			CQuidList *abl = new CQuidList (quid_nr, bezeichnung);
			QuidList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
		Load ();
	}

	for (std::vector<CQuidList *>::iterator pabl = QuidList.begin (); pabl != QuidList.end (); ++pabl)
	{
		CQuidList *abl = *pabl;
		CString QuidNr;
		CString Bezeichnung;
		QuidNr.Format (_T("%hd"), abl->quid_nr); 
		Bezeichnung = abl->bezeichnung;
        int ret = InsertItem (i, -1);
        ret = SetItemText (QuidNr.GetBuffer (), i, 1);
        ret = SetItemText (Bezeichnung.GetBuffer (), i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceQuid::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceQuid::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceQuid::SearchQuidNr (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceQuid::SearchTxt (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceQuid::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchQuidNr (ListBox, EditText.GetBuffer ());
             break;
        case 2 :
             SearchTxt (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceQuid::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceQuid::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   short li1 = (short) _tstoi (strItem1.GetBuffer ());
	   short li2 = (short) _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}


void CChoiceQuid::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CQuidList *abl = QuidList [i];
		   
		   abl->quid_nr     = (short) _tstoi (ListBox->GetItemText (i, 1));
		   abl->bezeichnung = ListBox->GetItemText (i, 2);
	}
	for (int i = 1; i <= 4; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);
}

void CChoiceQuid::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = QuidList [idx];
}

CQuidList *CChoiceQuid::GetSelectedText ()
{
	CQuidList *abl = (CQuidList *) SelectedRow;
	return abl;
}


void CChoiceQuid::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cQuidNr = m_List.GetItemText (idx, 1);  
		short quid_nr = (short) _tstoi (cQuidNr.GetBuffer ());
//		Message.Format (_T("QuidNr=%hd"), quid_nr);
		Message.Format (_T("%hd"), quid_nr);
		ToClipboard (Message);
	}
	p.SetCommand (_T("14300"));
	HANDLE Pid = p.Start (SW_SHOWNORMAL);
}
