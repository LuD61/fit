#pragma once
#include "BasisDatenPage.h"
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "a_bas.h"
#include "a_bas_erw.h"
#include "a_hndw.h"
#include "a_eig.h"
#include "ptabn.h"
#include "atext.h"
#include "A_kun_gx.h"
#include "pr_a_gx.h"
#include "DbUniCode.h"
#include "afxwin.h"
#include "vector"
#include "ImageCtrl.h"
#include "ImageListener.h"

// CProductPage1-Dialogfeld

class CProductPage1 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CProductPage1)

public:
	enum
	{
		ZutFromAtexte = 1,
		ZutFromPrAusz = 2,
		ZutFromA_bas_erw = 3,
		ZutFromRezepture = 4,
	};
	CProductPage1();
	virtual ~CProductPage1();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCT_PAGE1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()

	std::vector<long *> TxtTab;
	std::vector<LPSTR> PrAuszTab;
public:
	int tabx;
	int taby;
	BOOL HideButtons;
	CBasisdatenPage *Basis;
	CWnd *Frame;
	CFormTab Form;
	CFormTab DisableEigs;
	CCtrlGrid CtrlGrid;
	CFont Font;
	CFont BoldFont;
	COLORREF BoldColor;
	BOOL ProductPage1Read;


	A_BAS_CLASS *A_bas;
	A_BAS_ERW_CLASS *A_bas_erw;
	A_HNDW_CLASS *A_hndw;
	A_EIG_CLASS *A_eig;
	A_KRZ_CLASS *A_krz;
	PTABN_CLASS Ptabn;
	ATEXTE_CLASS Atexte;
	A_KUN_GX_CLASS A_kun_gx;
	PR_A_GX_CLASS Pr_ausz_gx;

public:
	int ZutSource;
	int ZutIdx;
	CStatic m_LA_bz1;
	CTextEdit m_A_bz1;
	CTextEdit m_A_bz2;
	CStatic m_LProdDocu;
	CTextEdit m_ProdDocu;
	CStatic m_LZutat;
	CTextEdit m_Zutat;
	CStatic m_LGewData;
	CStatic m_LAGew;
	CNumEdit m_AGew;
	CStatic m_LInh;
	CNumEdit m_Inh;
	CStatic m_LKunBestEinh;
	CComboBox m_KunBestEinh;
	CStatic m_LHbkZtr;
	CNumEdit m_HbkZtr;
	CStatic m_LLosKennung;
	CNumEdit m_LosKennung;
	CSpinButtonCtrl m_SpinHbk;
	CStatic m_LLaufzeit;
	CNumEdit m_Laufzeit;
	CSpinButtonCtrl m_SpinLaufzeit;
	CComboBox m_HbkKz;
	CStatic m_LLgrTmpr;
	CTextEdit m_LgrTmpr;
	CStatic m_LProdMass;
	CTextEdit m_ProdMass;
	CImageCtrl m_ImageArticle;
	CStatic m_LStand;
	CDateTimeCtrl m_Stand;
	CStatic m_LGueltig;
	CTextEdit m_Gueltig;
	CStatic m_LVersion;
	CTextEdit m_Version;

	void ReadZutat ();
	void ReadZutFromAtexte ();
	void ReadZutFromA_bas_erw ();
	void UpdateZutA_bas_erw ();
	void ReadZutFromPrAuszGx ();
	void UpdateAtexte ();
	void UpdatePrAuszGx ();

	virtual void UpdatePage ();
	virtual void OnDelete ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeydown ();
    virtual BOOL OnKeyup ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    void FillCombo (LPTSTR Item, CWnd *control);
	void FillCombo2 (LPTSTR Item, CWnd *control);

	CNumEdit m_Staffelung;
	CStatic m_LStaffelung;
	CStatic m_LBestMe;
	CNumEdit m_BestMe;
	CStatic m_LInhKarton;
	CNumEdit m_InhKarton;
	CStatic m_LPackungKarton;
	CNumEdit m_PackungKarton;
	CStatic m_LPack_Vol;
	CNumEdit m_Pack_Vol;
	BOOL Read ();

	CTextEdit m_a_bz01;
	CTextEdit m_a_bz02;
	CStatic m_LVB;
	CStatic m_LLaufzeitEinheit;
	CStatic m_LBH;
	CStatic m_LInGramm;
	CStatic m_LInStueck;
	CButton m_check1;
	CButton m_check2;
	CButton m_check3;
	CStatic m_KlaData;
	CStatic m_LMinStaffGr;
	CComboBox m_MinStaffGr;
};
