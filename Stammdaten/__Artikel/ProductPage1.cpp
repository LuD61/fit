// ProductPage1.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "ProductPage1.h"
#include "UniFormField.h"
#include "CtrlLine.h"
#include "Token.h"

static int LeseZutatentext = 0;  //über atexte.a

// CProductPage1-Dialogfeld

IMPLEMENT_DYNAMIC(CProductPage1, CDbPropertyPage)

CProductPage1::CProductPage1()
	: CDbPropertyPage(CProductPage1::IDD)
{
	Basis = NULL;

	tabx = 0;
	taby = 21;
	ProductPage1Read = FALSE;
	ZutSource = this->ZutFromAtexte;
	ZutIdx = 1;
	BoldColor = RGB (0,128,192);
}

CProductPage1::~CProductPage1()
{
}

void CProductPage1::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LA_BZ1, m_LA_bz1);
	DDX_Control(pDX, IDC_A_BZ1, m_A_bz1);
	DDX_Control(pDX, IDC_A_BZ2, m_A_bz2);
	DDX_Control(pDX, IDC_LPROD_DOCU, m_LProdDocu);
	DDX_Control(pDX, IDC_PROD_DOCU, m_ProdDocu);
	DDX_Control(pDX, IDC_LZUTAT, m_LZutat);
	DDX_Control(pDX, IDC_ZUTAT, m_Zutat);
	DDX_Control(pDX, IDC_LGEW_DATA, m_LGewData);
	DDX_Control(pDX, IDC_LA_GEW, m_LAGew);
	DDX_Control(pDX, IDC_A_GEW, m_AGew);
	DDX_Control(pDX, IDC_LINH, m_LInh);
	DDX_Control(pDX, IDC_INH, m_Inh);
	DDX_Control(pDX, IDC_LKUN_BEST_EINH, m_LKunBestEinh);
	DDX_Control(pDX, IDC_KUN_BEST_EINH, m_KunBestEinh);
	DDX_Control(pDX, IDC_LHBK_ZTR, m_LHbkZtr);
	DDX_Control(pDX, IDC_LLOSKENNZEICHNUNG, m_LLosKennung);
	DDX_Control(pDX, IDC_LOSKENNZEICHNUNG, m_LosKennung);
	DDX_Control(pDX, IDC_SPIN_HBK, m_SpinHbk);
	DDX_Control(pDX, IDC_HBK_ZTR, m_HbkZtr);
	DDX_Control(pDX, IDC_HBK_KZ, m_HbkKz);
	DDX_Control(pDX, IDC_LLGR_TMPR, m_LLgrTmpr);
	DDX_Control(pDX, IDC_LGR_TMPR, m_LgrTmpr);
	DDX_Control(pDX, IDC_LPROD_MASS, m_LProdMass);
	DDX_Control(pDX, IDC_PROD_MASS, m_ProdMass);
	DDX_Control(pDX, IDC_LLAUFZEIT, m_LLaufzeit);
	DDX_Control(pDX, IDC_LAUFZEIT, m_Laufzeit);
	DDX_Control(pDX, IDC_SPIN_LAUFZEIT, m_SpinLaufzeit);
	DDX_Control(pDX, IDC_LSTAND, m_LStand);
	DDX_Control(pDX, IDC_STAND, m_Stand);
	DDX_Control(pDX, IDC_LGUELTIG, m_LGueltig);
	DDX_Control(pDX, IDC_GUELTIG, m_Gueltig);
	DDX_Control(pDX, IDC_LVERSION, m_LVersion);
	DDX_Control(pDX, IDC_VERSION, m_Version);
	DDX_Control(pDX, IDC_LBESTME, m_LBestMe);
	DDX_Control(pDX, IDC_BESTME, m_BestMe);
	DDX_Control(pDX, IDC_STAFFELUNG, m_Staffelung);
	DDX_Control(pDX, IDC_LSTAFFELUNG, m_LStaffelung);
	DDX_Control(pDX, IDC_LINHKARTON, m_LInhKarton);
	DDX_Control(pDX, IDC_INHKARTON, m_InhKarton);
	DDX_Control(pDX, IDC_LPACKUNGKARTON, m_LPackungKarton);
	DDX_Control(pDX, IDC_PACKUNGKARTON, m_PackungKarton);
	DDX_Control(pDX, IDC_LPACK_VOL, m_LPack_Vol);
	DDX_Control(pDX, IDC_PACK_VOL2, m_Pack_Vol);
	DDX_Control(pDX, IDC_A_BZ01, m_a_bz01);
	DDX_Control(pDX, IDC_A_BZ02, m_a_bz02);
	DDX_Control(pDX, IDC_LVB, m_LVB);
	DDX_Control(pDX, IDC_LLAUFZEITEINH, m_LLaufzeitEinheit);
	DDX_Control(pDX, IDC_LLBH, m_LBH);
	DDX_Control(pDX, IDC_LINGRAMM, m_LInGramm);
	DDX_Control(pDX, IDC_LINSTUECK, m_LInStueck);
	DDX_Control(pDX, IDC_CHECK1, m_check1);
	DDX_Control(pDX, IDC_CHECK2, m_check2);
	DDX_Control(pDX, IDC_CHECK3, m_check3);
	DDX_Control(pDX, IDC_LKLA_DATA, m_KlaData);
	DDX_Control(pDX, IDC_LMINSTAFFGR, m_LMinStaffGr);
	DDX_Control(pDX, IDC_MINSTAFFGR, m_MinStaffGr);
}


BEGIN_MESSAGE_MAP(CProductPage1, CPropertyPage)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()

BOOL CProductPage1::OnInitDialog()
{
    char cfg_v [256];
	PROG_CFG Cfg ("Artikel"); 

	ProductPage1Read = TRUE;

	CPropertyPage::OnInitDialog();
	this->A_bas  = &Basis->A_bas;
	this->A_bas_erw  = &Basis->A_bas_erw;
	this->A_hndw = &Basis->A_hndw;
	this->A_eig = &Basis->A_eig;
	this->DlgBkColor = Basis->DlgBkColor;

	Basis->Form.Get ();
    Basis->ProductPage1Read = TRUE;

//	Basis->ZutSource = Basis->ZutFromAtexte;
	ZutSource = this->ZutFromAtexte;

    if (Cfg.GetCfgValue ("LeseZutatentext", cfg_v) == TRUE) //wird jetzt aber übersteuert durch den sys_par zutat_at
    {
			LeseZutatentext = atoi (cfg_v);
    }
	strcpy (sys_par.sys_par_nam, "zutat_at");
	if (Basis->Sys_par.dbreadfirst () == 0)
	{
		LeseZutatentext = atoi (sys_par.sys_par_wrt);
//		Basis->ZutSource = Basis->ZutFromA_bas_erw;
		ZutSource = this->ZutFromA_bas_erw;

	}

 

	TxtTab.push_back (&A_bas->a_bas.txt_nr1);
	TxtTab.push_back (&A_bas->a_bas.txt_nr2);
	TxtTab.push_back (&A_bas->a_bas.txt_nr3);
	TxtTab.push_back (&A_bas->a_bas.txt_nr4);
	TxtTab.push_back (&A_bas->a_bas.txt_nr5);
	TxtTab.push_back (&A_bas->a_bas.txt_nr6);
	TxtTab.push_back (&A_bas->a_bas.txt_nr7);
	TxtTab.push_back (&A_bas->a_bas.txt_nr8);
	TxtTab.push_back (&A_bas->a_bas.txt_nr9);
	TxtTab.push_back (&A_bas->a_bas.txt_nr10);

	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt1);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt2);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt3);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt4);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt5);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt6);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt7);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt8);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt9);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt10);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt11);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt12);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt13);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt14);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt15);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt16);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt17);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt18);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt19);
	PrAuszTab.push_back ((LPSTR) Pr_ausz_gx.pr_ausz_gx.txt20);

	ReadZutat ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	BoldFont.CreateFontIndirect (&l);

    m_ImageArticle.Create (_T("Bild"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, CRect (0, 0, 250, 200),
		                   this, IDC_ARTIMAGE);

	m_ProdDocu.SetLimitText (512);
	m_LgrTmpr.SetLimitText (99);
	m_ProdMass.SetLimitText (30);

    Form.Add (new CUniFormField (&m_a_bz01,EDIT,   (char *) A_bas->a_bas.a_bz1, VCHAR, sizeof (A_bas->a_bas.a_bz1) - 1));
    Form.Add (new CUniFormField (&m_a_bz02,EDIT,   (char *) A_bas->a_bas.a_bz2, VCHAR, sizeof (A_bas->a_bas.a_bz2) - 1));

    Form.Add (new CUniFormField (&m_A_bz1,EDIT,   (char *) A_bas_erw->a_bas_erw.pp_a_bz1, VCHAR, sizeof (A_bas_erw->a_bas_erw.pp_a_bz1) - 1));//FS-148
    Form.Add (new CUniFormField (&m_A_bz2,EDIT,   (char *) A_bas_erw->a_bas_erw.pp_a_bz2, VCHAR, sizeof (A_bas_erw->a_bas_erw.pp_a_bz2) - 1));//FS-148

    Form.Add (new CUniFormField (&m_ProdDocu,EDIT,  (char *)   A_bas->a_bas.produkt_info, VCHAR));
	Form.Add (new CUniFormField (&m_Zutat,EDIT,     (char *)   A_bas_erw->a_bas_erw.zutat, VCHAR));
	Form.Add (new CFormField (&m_AGew,EDIT,         (double *) &A_bas->a_bas.a_gew, VDOUBLE, 8,3));
	Form.Add (new CFormField (&m_Inh,EDIT,          (double *) &A_hndw->a_hndw.inh, VDOUBLE, 8, 3));
	Form.Add (new CFormField (&m_KunBestEinh,COMBOBOX,    (short *) &A_hndw->a_hndw.me_einh_kun, VSHORT));
	Form.Add (new CFormField (&m_HbkZtr, EDIT,     (short *) &A_bas->a_bas.hbk_ztr, VSHORT));
	Form.Add (new CFormField (&m_LosKennung, EDIT, (char *)  A_bas->a_bas.loskennung, VCHAR));
    Form.Add (new CFormField (&m_HbkKz, COMBOBOX,  (char *)  A_bas->a_bas.hbk_kz, VCHAR));
	Form.Add (new CFormField (&m_LgrTmpr, EDIT,  (char *)    A_bas_erw->a_bas_erw.lgr_tmpr , VCHAR));
	Form.Add (new CFormField (&m_ProdMass, EDIT,  (char *)    A_bas->a_bas.prod_mass , VCHAR));
	Form.Add (new CFormField (&m_Laufzeit, EDIT,  (short *)    &A_bas->a_bas.restlaufzeit , VSHORT));
	Form.Add (new CFormField (&m_Stand, DATETIMEPICKER,  (DATE_STRUCT *)   &A_bas->a_bas.stand , VDATE));
	Form.Add (new CFormField (&m_Gueltig, EDIT,  (char *)    A_bas->a_bas.gueltigkeit , VCHAR));
	Form.Add (new CFormField (&m_Version, EDIT,  (char *)    A_bas->a_bas.version_nr , VCHAR));
	Form.Add (new CFormField (&m_Staffelung, EDIT,  (double *)    &A_bas->a_bas.staffelung , VDOUBLE, 10 , 3));
	Form.Add (new CFormField (&m_BestMe, EDIT,  (double *)    &A_bas->a_bas.min_bestellmenge , VDOUBLE, 10 , 3));
	Form.Add (new CFormField (&m_MinStaffGr,COMBOBOX, (short *) &A_bas_erw->a_bas_erw.minstaffgr, VSHORT));
//	Form.Add (new CFormField (&m_InhKarton, EDIT,  (short *)    &A_bas->a_bas.inh_karton , VSHORT));
//	Form.Add (new CFormField (&m_PackungKarton, EDIT,  (short *)    &A_bas->a_bas.packung_karton , VSHORT));
//	Form.Add (new CFormField (&m_Pack_Vol, EDIT,  (double *)    &A_bas->a_bas.pack_vol , VDOUBLE, 8,3));
	Form.Add (new CFormField (&m_check1,CHECKBOX, (short *) &A_bas_erw->a_bas_erw.userdef1, VSHORT));
	Form.Add (new CFormField (&m_check2,CHECKBOX, (short *) &A_bas_erw->a_bas_erw.userdef2, VSHORT));
	Form.Add (new CFormField (&m_check3,CHECKBOX, (short *) &A_bas_erw->a_bas_erw.userdef3, VSHORT));

	FillCombo (_T("me_einh"), &m_KunBestEinh);
	FillCombo2 (_T("minstaffgr"), &m_MinStaffGr);
	FillCombo (_T("hbk_kz"), &m_HbkKz);

	if (LeseZutatentext == 2)
	{
		m_LZutat.SetWindowTextA("Zutaten   (Grundartikel)");
	}


    CtrlGrid.Create (this, 40, 40);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LA_bz1  = new CCtrlInfo (&m_LA_bz1, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LA_bz1);
	CCtrlInfo *c_a_bz01  = new CCtrlInfo (&m_a_bz01, 2, 0, 1, 1); 
	CtrlGrid.Add (c_a_bz01);
	CCtrlInfo *c_a_bz02  = new CCtrlInfo (&m_a_bz02, 3, 0, 1, 1); 
	CtrlGrid.Add (c_a_bz02);

	CCtrlInfo *c_LVB  = new CCtrlInfo (&m_LVB, 1, 1, 1, 1); 
	CtrlGrid.Add (c_LVB);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 2, 1, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 2, 2, 3, 1); 
	CtrlGrid.Add (c_A_bz2);

	CCtrlLine *Line1 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 3, DOCKRIGHT, 1);
	CtrlGrid.Add (Line1);

	CCtrlInfo *c_LProdDocu  = new CCtrlInfo (&m_LProdDocu, 1, 4, 1, 6); 
	CtrlGrid.Add (c_LProdDocu);
	CCtrlInfo *c_ProdDocu  = new CCtrlInfo (&m_ProdDocu, 2, 4, 3, 6); 
	CtrlGrid.Add (c_ProdDocu);

	CCtrlLine *Line2 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 8, DOCKRIGHT, 1);
	CtrlGrid.Add (Line2);

	CCtrlInfo *c_LZutat  = new CCtrlInfo (&m_LZutat, 1, 9, 1, 1); 
	CtrlGrid.Add (c_LZutat);
	CCtrlInfo *c_Zutat  = new CCtrlInfo (&m_Zutat, 2, 9, 3, 1); 
	CtrlGrid.Add (c_Zutat);

	CCtrlLine *Line3 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 13, DOCKRIGHT, 1);
	CtrlGrid.Add (Line3);

	CCtrlInfo *c_LGewData  = new CCtrlInfo (&m_LGewData, 1, 14, 1, 1); 
	CtrlGrid.Add (c_LGewData);
	CCtrlInfo *c_LAGew  = new CCtrlInfo (&m_LAGew, 2, 14, 1, 1); 
	CtrlGrid.Add (c_LAGew);
	CCtrlInfo *c_AGew  = new CCtrlInfo (&m_AGew, 3, 14, 1, 1); 
	CtrlGrid.Add (c_AGew);
	CCtrlInfo *c_LInGramm  = new CCtrlInfo (&m_LInGramm, 4, 14, 1, 1); 
	CtrlGrid.Add (c_LInGramm);

	CCtrlInfo *c_LInh  = new CCtrlInfo (&m_LInh, 2, 15, 1, 1); 
	CtrlGrid.Add (c_LInh);
	CCtrlInfo *c_Inh  = new CCtrlInfo (&m_Inh, 3, 15, 1, 1); 
	CtrlGrid.Add (c_Inh);
	CCtrlInfo *c_LInStueck  = new CCtrlInfo (&m_LInStueck, 4, 15, 1, 1); 
	CtrlGrid.Add (c_LInStueck);
	CCtrlInfo *c_LKunBestEinh  = new CCtrlInfo (&m_LKunBestEinh, 2, 16, 1, 1); 
	CtrlGrid.Add (c_LKunBestEinh);
	CCtrlInfo *c_KunBestEinh  = new CCtrlInfo (&m_KunBestEinh, 3, 16, 2, 1); 
	CtrlGrid.Add (c_KunBestEinh);

	CCtrlInfo *c_LHbkZtr  = new CCtrlInfo (&m_LHbkZtr, 2, 17, 1, 1); 
	CtrlGrid.Add (c_LHbkZtr);
	CCtrlInfo *c_HbkZtr  = new CCtrlInfo (&m_HbkZtr, 3, 17, 1, 1); 
	CtrlGrid.Add (c_HbkZtr);
	CCtrlInfo *c_HbkKz  = new CCtrlInfo (&m_HbkKz, 4, 17, 1, 1); 
	CtrlGrid.Add (c_HbkKz);

	CCtrlInfo *c_LLosKennung  = new CCtrlInfo (&m_LLosKennung, 2, 17, 1, 1); 
	CtrlGrid.Add (c_LLosKennung);
	CCtrlInfo *c_LosKennung  = new CCtrlInfo (&m_LosKennung, 3, 17, 1, 1); 
	CtrlGrid.Add (c_LosKennung);

	CCtrlInfo *c_LLgrTmpr  = new CCtrlInfo (&m_LLgrTmpr, 2, 18, 1, 1); 
	CtrlGrid.Add (c_LLgrTmpr);
	CCtrlInfo *c_LgrTmpr  = new CCtrlInfo (&m_LgrTmpr, 3, 18, 2, 1); 
	CtrlGrid.Add (c_LgrTmpr);
	CCtrlInfo *c_LProdMass  = new CCtrlInfo (&m_LProdMass, 2, 19, 1, 1); 
	CtrlGrid.Add (c_LProdMass);
	CCtrlInfo *c_ProdMass  = new CCtrlInfo (&m_ProdMass, 3, 19, 2, 1); 
	CtrlGrid.Add (c_ProdMass);
	CCtrlInfo *c_LBH  = new CCtrlInfo (&m_LBH, 4, 19, 2, 1); 
	CtrlGrid.Add (c_LBH);
	CCtrlInfo *c_LLaufzeit  = new CCtrlInfo (&m_LLaufzeit, 2, 20, 1, 1); 
	CtrlGrid.Add (c_LLaufzeit);
	CCtrlInfo *c_Laufzeit  = new CCtrlInfo (&m_Laufzeit, 3, 20, 2, 1); 
	CtrlGrid.Add (c_Laufzeit);
	CCtrlInfo *c_LLaufzeitEinheit  = new CCtrlInfo (&m_LLaufzeitEinheit, 4, 20, 1, 1); 
	CtrlGrid.Add (c_LLaufzeitEinheit);

	CCtrlInfo *c_LStand  = new CCtrlInfo (&m_LStand, 2, 21, 1, 1); 
	CtrlGrid.Add (c_LStand);
	CCtrlInfo *c_Stand  = new CCtrlInfo (&m_Stand, 3, 21, 2, 1); 
	CtrlGrid.Add (c_Stand);

	CCtrlInfo *c_LGueltig  = new CCtrlInfo (&m_LGueltig, 2, 22, 1, 1); 
	CtrlGrid.Add (c_LGueltig);
	CCtrlInfo *c_Gueltig  = new CCtrlInfo (&m_Gueltig, 3, 22, 2, 1); 
	CtrlGrid.Add (c_Gueltig);

	CCtrlInfo *c_LVersion  = new CCtrlInfo (&m_LVersion, 2, 23, 1, 1); 
	CtrlGrid.Add (c_LVersion);
	CCtrlInfo *c_Version  = new CCtrlInfo (&m_Version, 3, 23, 2, 1); 
	CtrlGrid.Add (c_Version);

	CCtrlInfo *c_LBestMe  = new CCtrlInfo (&m_LBestMe, 2, 24, 1, 1); 
	CtrlGrid.Add (c_LBestMe);
	CCtrlInfo *c_BestMe  = new CCtrlInfo (&m_BestMe, 3, 24, 2, 1); 
	CtrlGrid.Add (c_BestMe);

	CCtrlInfo *c_LStaffelung  = new CCtrlInfo (&m_LStaffelung, 2, 25, 1, 1); 
	CtrlGrid.Add (c_LStaffelung);
	CCtrlInfo *c_Staffelung  = new CCtrlInfo (&m_Staffelung, 3, 25, 2, 1); 
	CtrlGrid.Add (c_Staffelung);

	CCtrlInfo *c_LMinStaffGr  = new CCtrlInfo (&m_LMinStaffGr, 2, 26, 1, 1); 
	CtrlGrid.Add (c_LMinStaffGr);
	CCtrlInfo *c_MinStaffGr  = new CCtrlInfo (&m_MinStaffGr, 3, 26, 2, 1); 
	CtrlGrid.Add (c_MinStaffGr);

	CCtrlLine *Line4 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 28, DOCKRIGHT, 1);
	CtrlGrid.Add (Line4);

	CCtrlInfo *c_LKlaData  = new CCtrlInfo (&m_KlaData, 1, 29, 1, 1); 
	CtrlGrid.Add (c_LKlaData);
	CCtrlInfo *c_userdef1  = new CCtrlInfo (&m_check1, 2, 29, 2, 1); 
	CtrlGrid.Add (c_userdef1);
	CCtrlInfo *c_userdef2  = new CCtrlInfo (&m_check2, 3, 29, 2, 1); 
	CtrlGrid.Add (c_userdef2);
	CCtrlInfo *c_userdef3  = new CCtrlInfo (&m_check3, 4, 29, 2, 1); 
	CtrlGrid.Add (c_userdef3);

	/***
	CCtrlInfo *c_LInhKarton  = new CCtrlInfo (&m_LInhKarton, 2, 26, 1, 1); 
	CtrlGrid.Add (c_LInhKarton);
	CCtrlInfo *c_InhKarton  = new CCtrlInfo (&m_InhKarton, 3, 26, 2, 1); 
	CtrlGrid.Add (c_InhKarton);

	CCtrlInfo *c_LPackungKarton  = new CCtrlInfo (&m_LPackungKarton, 2, 27, 1, 1); 
	CtrlGrid.Add (c_LPackungKarton);
	CCtrlInfo *c_PackungKarton  = new CCtrlInfo (&m_PackungKarton, 3, 27, 2, 1); 
	CtrlGrid.Add (c_PackungKarton);

	CCtrlInfo *c_LPack_Vol  = new CCtrlInfo (&m_LPack_Vol, 2, 28, 1, 1); 
	CtrlGrid.Add (c_LPack_Vol);
	CCtrlInfo *c_Pack_Vol  = new CCtrlInfo (&m_Pack_Vol, 3, 28, 2, 1); 
	CtrlGrid.Add (c_Pack_Vol);

	**/

// ==== Bild =====
	CCtrlInfo *c_ImageArticle = new CCtrlInfo (&m_ImageArticle, 5, 14, 2, 7);
	CtrlGrid.Add (c_ImageArticle);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_check1.ShowWindow (SW_HIDE);
	m_check2.ShowWindow (SW_HIDE);
	m_check3.ShowWindow (SW_HIDE);
    if (Cfg.GetCfgValue ("Bez_Checkbox1", cfg_v) == TRUE) 
    {
		m_check1.SetWindowTextA(cfg_v);
		m_check1.ShowWindow (SW_SHOWNORMAL);
    }
    if (Cfg.GetCfgValue ("Bez_Checkbox2", cfg_v) == TRUE) 
    {
		m_check2.SetWindowTextA(cfg_v);
		m_check2.ShowWindow (SW_SHOWNORMAL);
    }
    if (Cfg.GetCfgValue ("Bez_Checkbox3", cfg_v) == TRUE) 
    {
		m_check3.SetWindowTextA(cfg_v);
		m_check3.ShowWindow (SW_SHOWNORMAL);
    }






	m_LA_bz1.SetFont (&BoldFont);
	m_LVB.SetFont (&BoldFont);
	m_LProdDocu.SetFont (&BoldFont);
	m_LZutat.SetFont (&BoldFont);
	m_LGewData.SetFont (&BoldFont);
	m_KlaData.SetFont (&BoldFont);

	m_LHbkZtr.ShowWindow (SW_HIDE); 
	m_HbkZtr.ShowWindow (SW_HIDE); 
	m_HbkKz.ShowWindow (SW_HIDE); 
	m_SpinHbk.ShowWindow (SW_HIDE);

	m_LInhKarton.ShowWindow (SW_HIDE); 
	m_InhKarton.ShowWindow (SW_HIDE); 

	m_LPackungKarton.ShowWindow (SW_HIDE); 
	m_PackungKarton.ShowWindow (SW_HIDE); 

	m_LPack_Vol.ShowWindow (SW_HIDE); 
	m_Pack_Vol.ShowWindow (SW_HIDE); 

	Form.Show ();
	CtrlGrid.Display ();
	m_ImageArticle.Show (A_bas->a_bas.bild);
//	m_SpinHbk.SetBuddy (&m_HbkZtr);
	m_SpinLaufzeit.SetBuddy (&m_Laufzeit);
	PostMessage (WM_SETFOCUS, (WPARAM) m_A_bz1.m_hWnd, 0l);
	return FALSE;
}

// CProductPage1-Meldungshandler

HBRUSH CProductPage1::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
		    if (pWnd == &m_LA_bz1)
			{
				pDC->SetTextColor (BoldColor);
			}
			if (pWnd == &m_LVB)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LProdDocu)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LZutat)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LGewData)
			{
				pDC->SetTextColor (BoldColor);
			}
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDbPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CProductPage1::OnSize(UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	GetWindowRect (&pRect);
	GetParent ()->ScreenToClient (&pRect);
	int pcx = pRect.right -pRect.left - tabx;
	int pcy = pRect.bottom - pRect.top - taby;
	if (cx > pcx)
	{
		pcx = cx;
	}
	if (cy > pcy)
	{
		pcy = cy;
	}
	MoveWindow (tabx, taby, pcx, pcy);
	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = pcx - 10 - tabx;
	frame.bottom = pcy - 10 - taby;
	frame.top += 20;
	frame.left += 2;
    frame.right -= 10;
	frame.bottom -= 10;
}

BOOL CProductPage1::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return FALSE;
			}
/*
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					if (OnKeydown ())
					{
						return TRUE;
					}
				}
				else
				{
					if (OnKeyup ())
					{
						return TRUE;
					}
				}
				break;
			}
*/

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeydown ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ()) 
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return FALSE;
}

void CProductPage1::UpdatePage ()
{
	Basis->Form.Get ();
	ReadZutat ();
	Form.Show ();
	m_ImageArticle.Show (A_bas->a_bas.bild);
	m_ImageArticle.Invalidate ();
}

BOOL CProductPage1::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_ProdDocu) return FALSE;
	if (Control == &m_Zutat) return FALSE;

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CProductPage1::OnKeydown ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_ProdDocu) return FALSE;
	if (Control == &m_Zutat) return FALSE;

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CProductPage1::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_ProdDocu) return FALSE;
	if (Control == &m_Zutat) return FALSE;

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

void CProductPage1::OnDelete ()
{
}

BOOL CProductPage1::OnKillActive ()
{
	Form.Get ();
	if (ZutSource == this->ZutFromAtexte)
	{
		UpdateAtexte ();
	}
	else if (ZutSource == this->ZutFromPrAusz)
	{
		UpdatePrAuszGx ();
	}
	else if (ZutSource == this->ZutFromA_bas_erw)
	{
		UpdateZutA_bas_erw ();
	}

	return TRUE;
}

BOOL CProductPage1::OnSetActive ()
{
	if (Basis != NULL)
	{
		Basis->Form.Get ();
	}
	DlgBkColor = Basis->DlgBkColor;
    DeleteObject (DlgBrush);
    DlgBrush = NULL;
//	InvalidateRect (NULL);
	Form.Show ();
	m_ImageArticle.Show (A_bas->a_bas.bild);
	m_ImageArticle.Invalidate ();
	PostMessage (WM_SETFOCUS, (WPARAM) m_A_bz1.m_hWnd, 0l);
	return TRUE;
}

void CProductPage1::ReadZutat ()
{
	if (ZutSource == this->ZutFromAtexte)
	{
//WAL-130		UpdateAtexte ();
		ReadZutFromAtexte (); //WAL-130
	}
	else if (ZutSource == this->ZutFromPrAusz)
	{
//WAL-130		UpdatePrAuszGx ();
		ReadZutFromPrAuszGx (); //WAL-130
	}
	else if (ZutSource == this->ZutFromA_bas_erw)
	{
//WAL-130		UpdateZutA_bas_erw ();
		ReadZutFromA_bas_erw (); //WAL-130
	}
}

void CProductPage1::ReadZutFromA_bas_erw ()
{
	if (LeseZutatentext == 2)
	{
		A_bas_erw->dbreadzutat (A_bas->a_bas.a_grund);
	}

}
void CProductPage1::UpdateZutA_bas_erw ()
{
//	LPSTR txt;
//    CToken t;
	if (LeseZutatentext == 2)
	{
		A_bas_erw->dbupdatezutat (A_bas->a_bas.a_grund);
		//nur zum test so kann man es auseinanderpflücken
		/*
		t.SetSep (_T("\n"));
		t = (LPSTR) A_bas_erw->a_bas_erw.zutat;
		for (int i = 0; (i < t.GetAnzToken ()) && (i < 20); i ++)
		{
			txt = PrAuszTab[i];
			strcpy (txt, t.GetToken (i)); 
		}
		*/

	}

}

void CProductPage1::ReadZutFromAtexte ()
{
	memcpy (&Atexte.atexte, &atexte_null, sizeof (ATEXTE));
	if (TxtTab.size () < (unsigned int) ZutIdx) return;

	Atexte.atexte.txt_nr = *TxtTab[ZutIdx - 1];
	if (Atexte.atexte.txt_nr == 0l)
	{
			Atexte.atexte.txt_nr = (long) A_bas->a_bas.a ;
			*TxtTab[ZutIdx - 1] = Atexte.atexte.txt_nr;
	}
	Atexte.atexte.a = A_bas->a_bas.a;
	if (LeseZutatentext == 1)	Atexte.atexte.a = A_bas->a_bas.a;
	if (LeseZutatentext == 2)	Atexte.atexte.a = A_bas->a_bas.a_grund;
	if (LeseZutatentext == 0)
	{
		if (Atexte.dbreadfirst () == 100)
		{
			Atexte.atexte.txt_nr = (long) A_bas->a_bas.a * 1000 + ZutIdx;
			*TxtTab[ZutIdx - 1] = Atexte.atexte.txt_nr;
			if (Atexte.dbreadfirst () == 100)
			{
				ReadZutFromPrAuszGx ();
			}
		}
	}
	else
	{
		if (Atexte.dbreadfirsta () == 100)
		{
			Atexte.atexte.txt_nr = (long) A_bas->a_bas.a * 1000 + ZutIdx;
		}
	}
	strcpy (A_bas_erw->a_bas_erw.zutat, Atexte.atexte.disp_txt);

}

void CProductPage1::UpdateAtexte ()
{
	strncpy (Atexte.atexte.disp_txt, A_bas_erw->a_bas_erw.zutat,sizeof (Atexte.atexte.disp_txt) );
	strcpy (Atexte.atexte.txt, Atexte.atexte.disp_txt);
	Atexte.atexte.a = A_bas->a_bas.a;
	if (LeseZutatentext == 1) Atexte.atexte.a = A_bas->a_bas.a;
	if (LeseZutatentext == 2) Atexte.atexte.a = A_bas->a_bas.a_grund;
	Atexte.dbupdate ();
}

void CProductPage1::UpdatePrAuszGx ()
{
	LPSTR txt;
	if (Pr_ausz_gx.pr_ausz_gx.text_nr == 0l) return;
    CToken t;
	t.SetSep (_T("\n"));
	t = (LPSTR) Atexte.atexte.disp_txt;
	for (int i = 0; (i < t.GetAnzToken ()) && (i < 20); i ++)
	{
		txt = PrAuszTab[i];
		strcpy (txt, t.GetToken (i)); 
	}
	Pr_ausz_gx.dbupdate ();
}

void CProductPage1::ReadZutFromPrAuszGx ()
{
	memcpy (&A_kun_gx.a_kun_gx, &a_kun_gx_null, sizeof (A_KUN_GX));
	memcpy (&Pr_ausz_gx.pr_ausz_gx, &pr_ausz_gx_null, sizeof (PR_AUSZ_GX));
	A_kun_gx.a_kun_gx.mdn = A_bas->a_bas.mdn;
	A_kun_gx.a_kun_gx.fil = A_bas->a_bas.fil;
	A_kun_gx.a_kun_gx.a = A_bas->a_bas.a;
	A_kun_gx.a_kun_gx.kun = 0l;
	strcpy (A_kun_gx.a_kun_gx.kun_bran2, (LPSTR) "0");
	int dsqlstatus = A_kun_gx.dbreadfirst ();
	if (dsqlstatus == 100)
	{
		strcpy (A_kun_gx.a_kun_gx.kun_bran2, (LPSTR) "");
		dsqlstatus = A_kun_gx.dbreadfirst ();
	}
	if (dsqlstatus != 0) return;
	Pr_ausz_gx.pr_ausz_gx.text_nr = A_kun_gx.a_kun_gx.text_nr / 100;
	Pr_ausz_gx.pr_ausz_gx.eti_typ = (short) ((long) A_kun_gx.a_kun_gx.text_nr % 100);
	dsqlstatus = Pr_ausz_gx.dbreadfirst ();
	if (dsqlstatus == 100) return;

	CString text = _T("");
	for (std::vector<LPSTR>::iterator TabTxt = PrAuszTab.begin (); 
	                                              TabTxt != PrAuszTab.end ();
												  ++TabTxt)
	{
		LPSTR txt = *TabTxt;
		if (text != _T(""))
		{
			text += _T("\n");
		}
		text += txt;
	}

	text.TrimRight ();
	strcpy (Atexte.atexte.txt, text.GetBuffer ()); 
	strcpy (Atexte.atexte.disp_txt, text.GetBuffer ()); 
}

void CProductPage1::FillCombo (LPTSTR Item, CWnd *control)
{
	CFormField *f = Form.GetFormField (control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		//f->Get ();
	}
}

void CProductPage1::FillCombo2 (LPTSTR Item, CWnd *control)
{
	CFormField *f = Form.GetFormField (control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		//f->Get ();
	}
}


void CProductPage1::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CProductPage1::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

BOOL CProductPage1::Read ()
{
	if (!ProductPage1Read)
	{
		return FALSE;
	}
	Form.Show ();

return TRUE;

}
