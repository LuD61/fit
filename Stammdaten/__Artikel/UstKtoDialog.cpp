// UstKtoDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "UstKtoDialog.h"
#include "UniFormField.h"

#define WM_MDN_FOCUS WM_USER+10

extern int StdCellHeight;

// CUstKtoDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CUstKtoDialog, CDialog)

CUstKtoDialog::CUstKtoDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CUstKtoDialog::IDD, pParent)
{
	ChoiceMdn = NULL;
	ChoiceFil = NULL;
	A_bas = NULL;
}

CUstKtoDialog::~CUstKtoDialog()
{
}

void CUstKtoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_UST_KTO_GROUP, m_UstKtoGroup);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LFIL, m_LFil);
	DDX_Control(pDX, IDC_FIL, m_Fil);
	DDX_Control(pDX, IDC_FIL_NAME, m_FilName);
	DDX_Control(pDX, IDC_LMWST, m_LMwst);
	DDX_Control(pDX, IDC_MWST, m_Mwst);
	DDX_Control(pDX, IDC_LWE_KTO, m_LWeKto);
	DDX_Control(pDX, IDC_WE_KTO, m_WeKto);
	DDX_Control(pDX, IDC_LERL_KTO, m_LErlKto);
	DDX_Control(pDX, IDC_ERL_KTO, m_ErlKto);
	DDX_Control(pDX, IDC_LSKTO_KTO, m_LSktoKto);
	DDX_Control(pDX, IDC_SKTO_KTO, m_SktoKto);
}


BEGIN_MESSAGE_MAP(CUstKtoDialog, CDialog)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_BN_CLICKED(IDC_FILCHOICE , OnFilchoice)
	ON_BN_CLICKED(IDC_UST_SAVE ,   OnSave)
	ON_BN_CLICKED(IDC_UST_DELETE , Delete)
END_MESSAGE_MAP()


// CUstKtoDialog-Meldungshandler

BOOL CUstKtoDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
    m_Save.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_UST_SAVE);
	m_Save.LoadBitmap (IDB_SAVE);
	m_Save.SetToolTip (_T("speichern"));

    m_Delete.Create (_T("l�schen"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_UST_DELETE);
	m_Delete.LoadBitmap (IDB_DELETE);
	m_Delete.LoadMask (IDB_DELETE_MASK);
	m_Delete.SetToolTip (_T("l�schen"));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

    Keys.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Keys.Add (new CFormField (&m_Fil,EDIT,        (short *) &Fil.fil.fil, VSHORT));

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Fil,EDIT,        (short *) &Fil.fil.fil, VSHORT));
    Form.Add (new CUniFormField (&m_FilName,EDIT,   (char *) FilAdr.adr.adr_krz, VCHAR));
    Form.Add (new CUniFormField (&m_WeKto,EDIT,(long *) &A_ust_kto.a_ust_kto.we_kto, VLONG));
    Form.Add (new CUniFormField (&m_ErlKto,EDIT,(long *) &A_ust_kto.a_ust_kto.erl_kto, VLONG));
    Form.Add (new CUniFormField (&m_SktoKto,EDIT,(long *) &A_ust_kto.a_ust_kto.skto_kto, VLONG));
    Form.Add (new CFormField (&m_Mwst,COMBOBOX,  (short *) &A_ust_kto.a_ust_kto.mwst, VSHORT));

	FillCombo (_T("mwst"), &m_Mwst);

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 0, 12, 20);
    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//Grid Mdn
	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

//Grid Fil
	FilGrid.Create (this, 2, 2);
    FilGrid.SetBorder (0, 0);
    FilGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Fil = new CCtrlInfo (&m_Fil, 0, 0, 1, 1);
	FilGrid.Add (c_Fil);
	CtrlGrid.CreateChoiceButton (m_FilChoice, IDC_FILCHOICE, this);
	CCtrlInfo *c_FilChoice = new CCtrlInfo (&m_FilChoice, 1, 0, 1, 1);
	FilGrid.Add (c_FilChoice);


//Grid Toolbar
	ToolGrid.Create (this, 2, 2);
    ToolGrid.SetBorder (0, 0);
    ToolGrid.SetGridSpace (0, 0);

	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 0, 1, 1);
	ToolGrid.Add (c_Save);

	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 1, 0, 1, 1);
	ToolGrid.Add (c_Delete);

	CCtrlInfo *c_ToolGrid = new CCtrlInfo (&ToolGrid, 1, 0, 1, 1);
	c_ToolGrid->SetCellPos (0, 0, 0, 0);
	CtrlGrid.Add (c_ToolGrid);

//Gruppe
	CCtrlInfo *c_UstKtoGroup     = new CCtrlInfo (&m_UstKtoGroup, 0, 1, DOCKRIGHT, DOCKBOTTOM); 
	c_UstKtoGroup->rightspace = 5;
	CtrlGrid.Add (c_UstKtoGroup);

//Mandant
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 1, 2, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 2, 2, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 3, 2, 1, 1); 
	CtrlGrid.Add (c_MdnName);

//Filiale
	CCtrlInfo *c_LFil     = new CCtrlInfo (&m_LFil, 1, 3, 1, 1); 
	CtrlGrid.Add (c_LFil);
	CCtrlInfo *c_FilGrid   = new CCtrlInfo (&FilGrid, 2, 3, 1, 1); 
	CtrlGrid.Add (c_FilGrid);
	CCtrlInfo *c_FilName     = new CCtrlInfo (&m_FilName, 3, 3, 1, 1); 
	CtrlGrid.Add (c_FilName);


//Mehrwertsteuer
	CCtrlInfo *c_LMwst     = new CCtrlInfo (&m_LMwst, 1, 5, 1, 1); 
	CtrlGrid.Add (c_LMwst);
	CCtrlInfo *c_Mwst   = new CCtrlInfo (&m_Mwst, 2, 5, 3, 1); 
	CtrlGrid.Add (c_Mwst);


//Wareneingangskonto
	CCtrlInfo *c_LWeKto     = new CCtrlInfo (&m_LWeKto, 1, 7, 1, 1); 
	CtrlGrid.Add (c_LWeKto);
	CCtrlInfo *c_WeKto   = new CCtrlInfo (&m_WeKto, 2, 7, 2, 1); 
	CtrlGrid.Add (c_WeKto);


//Erl�skonto
	CCtrlInfo *c_LErlKto     = new CCtrlInfo (&m_LErlKto, 1, 8, 1, 1); 
	CtrlGrid.Add (c_LErlKto);
	CCtrlInfo *c_ErlKto   = new CCtrlInfo (&m_ErlKto, 2, 8, 2, 1); 
	CtrlGrid.Add (c_ErlKto);


//Skontokonto
	CCtrlInfo *c_LSktoKto   = new CCtrlInfo (&m_LSktoKto, 1, 9, 1, 1); 
	CtrlGrid.Add (c_LSktoKto);
	CCtrlInfo *c_SktoKto   = new CCtrlInfo (&m_SktoKto, 2, 9, 2, 1); 
	CtrlGrid.Add (c_SktoKto);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Save.Tooltip.SetFont (&Font);
	m_Save.Tooltip.SetSize ();
	m_Delete.Tooltip.SetFont (&Font);
	m_Delete.Tooltip.SetSize ();

	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
	memcpy (&A_ust_kto.a_ust_kto, &a_ust_kto_null, sizeof (A_UST_KTO));

	CtrlGrid.Display ();
	Form.Show ();

	ReadMdn ();
	if (Mdn.mdn.mdn != 0)
	{
		m_Mdn.EnableWindow (FALSE);
		m_MdnChoice.EnableWindow (FALSE);
	}
	return TRUE;
}

void CUstKtoDialog::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CUstKtoDialog::PreTranslateMessage(MSG* pMsg)
{
//	CWnd *cWnd = NULL;
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
                if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					if (OnReturn ())
					{
						return TRUE;
					}
				}
				else
				{
					Control = GetNextDlgTabItem (Control, TRUE);
					if (Control != NULL)
					{
						Control->SetFocus ();
						return TRUE;
					}
				}
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				OnSave ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Fil)
				{
					OnFilchoice ();
					return TRUE;
				}
				return TRUE;
			}
			break;

		case WM_MDN_FOCUS:
			m_Mdn.SetFocus ();
			return TRUE;
	}
	return FALSE;
}

BOOL CUstKtoDialog::OnReturn ()
{
	CWnd *Control = GetFocus ();
	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_Fil)
	{
		if (!Read ())
		{
			m_Fil.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_SktoKto)
	{
		OnSave ();
		return TRUE;
	}
	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return FALSE;
	}
	return TRUE;
}

BOOL CUstKtoDialog::ReadMdn ()
{
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();

	if (Mdn.mdn.mdn == 0)
	{
		_tcscpy (MdnAdr.adr.adr_krz, _T("Zentrale"));
		Form.Show ();
		return TRUE;
	}
	
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
//		m_Mdn.SetFocus ();
//		m_Mdn.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CUstKtoDialog::ReadFil ()
{
	memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();

	if (Fil.fil.fil == 0)
	{
		_tcscpy (FilAdr.adr.adr_krz, _T("Mandant"));
		Form.Show ();
		return TRUE;
	}
	
	if (Fil.dbreadfirst () == 0)
	{
		FilAdr.adr.adr = Fil.fil.adr;
		FilAdr.dbreadfirst ();
		Form.Show ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Filiale %hd nicht gefunden"),Fil.fil.fil);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Fil.SetFocus ();
		m_Fil.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CUstKtoDialog::Read ()
{
	memcpy (&A_ust_kto.a_ust_kto, &a_ust_kto_null, sizeof (A_UST_KTO));
	Keys.Get ();
	Form.Show ();


    A_ust_kto.a_ust_kto.mdn  = Mdn.mdn.mdn;
	A_ust_kto.a_ust_kto.fil  = Fil.fil.fil;
    A_ust_kto.a_ust_kto.a    = A_bas->a_bas.a;
	int dsqlstatus = A_ust_kto.dbreadfirst ();
	Form.Show ();
	return TRUE;
}

void CUstKtoDialog::OnSave ()
{
	Form.Get ();
    A_ust_kto.a_ust_kto.mdn  = Mdn.mdn.mdn;
	A_ust_kto.a_ust_kto.fil  = Fil.fil.fil;
    A_ust_kto.a_ust_kto.a   = A_bas->a_bas.a;
	A_ust_kto.dbupdate ();
	Form.Show ();
	PostMessage (WM_MDN_FOCUS, 0, 0l);
}

void CUstKtoDialog::Delete ()
{
	Form.Get ();
	if (MessageBox (_T("Satz l�schen ?"), "", MB_YESNO | MB_ICONQUESTION) != IDYES)
	{
		return;
	}
    A_ust_kto.a_ust_kto.mdn  = Mdn.mdn.mdn;
	A_ust_kto.a_ust_kto.fil  = Fil.fil.fil;
    A_ust_kto.a_ust_kto.a   = A_bas->a_bas.a;
	A_ust_kto.dbdelete ();
	memcpy (&A_ust_kto.a_ust_kto, &a_ust_kto_null, sizeof (A_UST_KTO));
	Form.Show ();
	PostMessage (WM_MDN_FOCUS, 0, 0l);
}

void CUstKtoDialog::OnMdnchoice ()
{

	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = TRUE;
		ChoiceMdn->CreateDlg ();
	}

    ChoiceMdn->SetDbClass (&Mdn);
	ChoiceMdn->DoModal();
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CUstKtoDialog::OnFilchoice ()
{

	Keys.Get ();
	if (ChoiceFil == NULL)
	{
		ChoiceFil = new CChoiceFil (this);
	    ChoiceFil->IsModal = TRUE;
		ChoiceFil->CreateDlg ();
	}

	ChoiceFil->m_Mdn = Mdn.mdn.mdn;
    ChoiceFil->SetDbClass (&Fil);
	ChoiceFil->DoModal();
    if (ChoiceFil->GetState ())
    {
		  CFilList *abl = ChoiceFil->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		  memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
		  Fil.fil.fil = abl->fil;
		  if (Fil.dbreadfirst () == 0)
		  {
			  FilAdr.adr.adr = Fil.fil.adr;
			  FilAdr.dbreadfirst ();
		  }
		  if (Fil.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Fil.SetSel (0, -1, TRUE);
		  m_Fil.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CUstKtoDialog::FillCombo (LPTSTR Item, CWnd *control)
{
	CFormField *f = Form.GetFormField (control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

