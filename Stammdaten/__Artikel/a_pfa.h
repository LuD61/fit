#ifndef _A_PFA_DEF
#define _A_PFA_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_PFA {
   double         a;
   long           a_krz;
   short          delstatus;
   short          fil;
   short          mdn;
   short          sg1;
   short          sg2;
   TCHAR          verk_art[2];
   TCHAR          mwst_ueb[2];
   DATE_STRUCT    verk_beg;
};
extern struct A_PFA a_pfa, a_pfa_null;

#line 8 "a_pfa.rh"

class A_PFA_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_PFA a_pfa;  
               A_PFA_CLASS () : DB_CLASS ()
               {
               }
};
#endif
