#ifndef _MO_AUTO_DEF
#define _MO_AUTO_DEF
#include "auto_nr.h"

class AUTO_CLASS
{
           protected :
                int dsqlstatus;
                int AufEinh;
				char* clipped (LPTSTR);
           public :
			    long Nr;
				AUTO_NR_CLASS AutoNrClass;
                AUTO_CLASS ()
                {
                }
				BOOL GenAutoNr (short mdn, short fil, LPTSTR nr_nam);
                int nvholidk2 (short, short, char *);
                int nvholid (short, short, char *);
                int nvanmprf (short, short, char *, long, long, long, char *);
                int nveinid0 (short, short, char *, long);
                int nveinid (short, short, char *, long);
};
#endif
