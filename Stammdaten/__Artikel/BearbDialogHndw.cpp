// BearbDialogHndw.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "BearbDialogHndw.h"

// CBearbDialogHndw-Dialogfeld

IMPLEMENT_DYNAMIC(CBearbDialogHndw, CDialog)

CBearbDialogHndw::CBearbDialogHndw(CWnd* pParent /*=NULL*/)
	: CDialog(CBearbDialogHndw::IDD, pParent)
{
	A_bas = NULL;
	A_kalkhndw = NULL;
	StdCellHeight = 15;
	SpEkIst = 0.0;
	SpVkIst = 0.0;
	MarktSpPar = Aufschlag;
	mwst = 1.0;
	visible = FALSE;
	EkDurchschnitt = 0.0;
}

CBearbDialogHndw::~CBearbDialogHndw()
{
}

void CBearbDialogHndw::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LPR_EK1, m_LPrEk1);
	DDX_Control(pDX, IDC_LEK_DURCHSCHNITT, m_LEkDurchschnitt);
	DDX_Control(pDX, IDC_LBEARB_WEG, m_LBearbWeg);
	DDX_Control(pDX, IDC_LSK, m_LSk);
	DDX_Control(pDX, IDC_LFIL_EK, m_LFilEk);
	DDX_Control(pDX, IDC_LVK, m_LVk);
	DDX_Control(pDX, IDC_LVOLLKOSTEN, m_LVollkosten);
	DDX_Control(pDX, IDC_LIST_PR, m_LIstPr);
	DDX_Control(pDX, IDC_LIST_SP, m_LIstSp);

	DDX_Control(pDX, IDC_PR_EK1, m_PrEk1);
	DDX_Control(pDX, IDC_EK_DURCHSCHNITT, m_EkDurchschnitt);

	DDX_Control(pDX, IDC_BEARB_SK, m_BearbSk);
	DDX_Control(pDX, IDC_BEARB_FIL, m_BearbFil);
	DDX_Control(pDX, IDC_BEARB_LAD, m_BearbLad);

	DDX_Control(pDX, IDC_SK, m_Sk);
	DDX_Control(pDX, IDC_FIL_EK, m_FilEk);
	DDX_Control(pDX, IDC_VK, m_Vk);

	DDX_Control(pDX, IDC_PR_EK,  m_PrEk);
	DDX_Control(pDX, IDC_APR_VK, m_APrVk);

	DDX_Control(pDX, IDC_SP_EK_IST, m_SpEkIst);
	DDX_Control(pDX, IDC_SP_VK_IST, m_SpVkIst);
}


BEGIN_MESSAGE_MAP(CBearbDialogHndw, CDialog)
//	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CBearbDialogHndw-Meldungshandler

BOOL CBearbDialogHndw::OnInitDialog ()
{
	CDialog::OnInitDialog ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	Form.Add (new CFormField (&m_PrEk1,EDIT,    (double *) &A_kalkhndw->a_kalkhndw.pr_ek1, VDOUBLE, 7,3));
	Form.Add (new CFormField (&m_EkDurchschnitt,EDIT,  (double *) &EkDurchschnitt, VDOUBLE, 7,3));

	Form.Add (new CFormField (&m_BearbSk,EDIT,     (short *) &A_kalkhndw->a_kalkhndw.bearb_sk,  VSHORT));
	Form.Add (new CFormField (&m_BearbFil,EDIT,    (short *) &A_kalkhndw->a_kalkhndw.bearb_fil, VSHORT));
	Form.Add (new CFormField (&m_BearbLad,EDIT,    (short *) &A_kalkhndw->a_kalkhndw.bearb_lad, VSHORT));

	Form.Add (new CFormField (&m_Sk,EDIT,     (double *) &A_kalkhndw->a_kalkhndw.sk_vollk,     VDOUBLE, 7,3));
	Form.Add (new CFormField (&m_FilEk,EDIT,  (double *) &A_kalkhndw->a_kalkhndw.fil_ek_vollk, VDOUBLE, 7,3));
	Form.Add (new CFormField (&m_Vk,EDIT,     (double *) &A_kalkhndw->a_kalkhndw.lad_vk_vollk, VDOUBLE, 7,3));

	Form.Add (new CFormField (&m_PrEk,EDIT,  (double *) &A_pr->a_pr.pr_ek_euro, VDOUBLE, 8,4));
	Form.Add (new CFormField (&m_APrVk,EDIT, (double *) &A_pr->a_pr.pr_vk_euro, VDOUBLE, 6,2));

	Form.Add (new CFormField (&m_SpEkIst,EDIT, (double *) &SpEkIst, VDOUBLE, 7,3));
	Form.Add (new CFormField (&m_SpVkIst,EDIT, (double *) &SpVkIst, VDOUBLE, 7,3));

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LPrEk1     = new CCtrlInfo (&m_LPrEk1, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LPrEk1);
	CCtrlInfo *c_LEkDurchschnitt     = new CCtrlInfo (&m_LEkDurchschnitt, 2, 0, 1, 1); 
	CtrlGrid.Add (c_LEkDurchschnitt);

	CCtrlInfo *c_PrEk1     = new CCtrlInfo (&m_PrEk1, 1, 1, 1, 1); 
	CtrlGrid.Add (c_PrEk1);
	CCtrlInfo *c_EkDurchschnitt     = new CCtrlInfo (&m_EkDurchschnitt, 2, 1, 1, 1); 
	CtrlGrid.Add (c_EkDurchschnitt);

	CCtrlInfo *c_LBearbWeg     = new CCtrlInfo (&m_LBearbWeg, 1, 3, 1, 1); 
	CtrlGrid.Add (c_LBearbWeg);
	CCtrlInfo *c_LSk     = new CCtrlInfo (&m_LSk, 0, 4, 1, 1); 
	CtrlGrid.Add (c_LSk);
	CCtrlInfo *c_LFilEk     = new CCtrlInfo (&m_LFilEk, 0, 5, 1, 1); 
	CtrlGrid.Add (c_LFilEk);
	CCtrlInfo *c_LVk     = new CCtrlInfo (&m_LVk, 0, 6, 1, 1); 
	CtrlGrid.Add (c_LVk);
	CCtrlInfo *c_LVollKosten = new CCtrlInfo (&m_LVollkosten, 2, 3, 1, 1); 
	CtrlGrid.Add (c_LVollKosten);
	CCtrlInfo *c_LIstPr     = new CCtrlInfo (&m_LIstPr, 3, 3, 1, 1); 
	CtrlGrid.Add (c_LIstPr);
	CCtrlInfo *c_LIstSp     = new CCtrlInfo (&m_LIstSp, 4, 3, 1, 1); 
	CtrlGrid.Add (c_LIstSp);

	CCtrlInfo *c_BearbSk     = new CCtrlInfo (&m_BearbSk, 1, 4, 1, 1); 
	CtrlGrid.Add (c_BearbSk);
	CCtrlInfo *c_BearbFil     = new CCtrlInfo (&m_BearbFil, 1, 5, 1, 1); 
	CtrlGrid.Add (c_BearbFil);
	CCtrlInfo *c_BearbLad     = new CCtrlInfo (&m_BearbLad, 1, 6, 1, 1); 
	CtrlGrid.Add (c_BearbLad);

	CCtrlInfo *c_Sk     = new CCtrlInfo (&m_Sk, 2, 4, 1, 1); 
	CtrlGrid.Add (c_Sk);
	CCtrlInfo *c_FilEk     = new CCtrlInfo (&m_FilEk, 2, 5, 1, 1); 
	CtrlGrid.Add (c_FilEk);
	CCtrlInfo *c_Vk     = new CCtrlInfo (&m_Vk, 2, 6, 1, 1); 
	CtrlGrid.Add (c_Vk);

	CCtrlInfo *c_PrEk     = new CCtrlInfo (&m_PrEk, 3, 5, 1, 1); 
	CtrlGrid.Add (c_PrEk);
	CCtrlInfo *c_APrVk     = new CCtrlInfo (&m_APrVk, 3, 6, 1, 1); 
	CtrlGrid.Add (c_APrVk);

	CCtrlInfo *c_SpEkIst     = new CCtrlInfo (&m_SpEkIst, 4, 5, 1, 1); 
	CtrlGrid.Add (c_SpEkIst);
	CCtrlInfo *c_SpVkIst     = new CCtrlInfo (&m_SpVkIst, 4, 6, 1, 1); 
	CtrlGrid.Add (c_SpVkIst);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display (); 
	return TRUE;
}

void CBearbDialogHndw::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}


BOOL CBearbDialogHndw::PreTranslateMessage(MSG* pMsg)
{
	return FALSE;
}

void CBearbDialogHndw::Show ()
{
	if (!visible) return;
	Form.Show ();
	Calculate ();
}

void CBearbDialogHndw::Get ()
{
	if (!visible) return;
	Calculate ();
	Form.Get ();
}

void CBearbDialogHndw::SetVisible (BOOL visible)
{
	this->visible = visible;
	if (visible)
	{
		ShowWindow (SW_SHOWNORMAL);
	}
	else
	{
		ShowWindow (SW_HIDE);
	}
}

void CBearbDialogHndw::Calculate ()
{
	double pr_vk = A_pr->a_pr.pr_vk_euro;

	Form.Get ();

	switch (MarktSpPar)
	{
		case AufschlagPlus:
		case AbschlagPlus:
			pr_vk /= mwst;
			break;
	}


	switch (MarktSpPar)
	{
	case Aufschlag:
	case AufschlagPlus:
/*
		A_kalkhndw->a_kalkhndw.sk_vollk = A_kalkhndw->a_kalkhndw.hk_vollk * (1 + A_kalkhndw->a_kalkhndw.sp_vk / 100);
		A_kalkhndw->a_kalkhndw.fil_ek_vollk = A_kalkhndw->a_kalkhndw.sk_vollk * (1 + A_kalkhndw->a_kalkhndw.sp_fil / 100);
		A_kalkhndw->a_kalkhndw.lad_vk_vollk = A_kalkhndw->a_kalkhndw.fil_ek_vollk * (1 + A_kalkhndw->a_kalkhndw.sp_lad / 100);
*/
		if (A_kalkhndw->a_kalkhndw.sk_vollk > 0.0)
		{
			SpEkIst = (A_pr->a_pr.pr_ek_euro / A_kalkhndw->a_kalkhndw.sk_vollk - 1) * 100;
		}
		if (A_pr->a_pr.pr_ek_euro > 0.0)
		{
			SpVkIst = (pr_vk / A_pr->a_pr.pr_ek_euro - 1) * 100;
		}
		break;
	case Abschlag:
	case AbschlagPlus:
/*
		if (A_kalkhndw->a_kalkhndw.sp_vk >= 100) 
		{
			A_kalkhndw->a_kalkhndw.sp_vk = 0.0;
		}
		A_kalkhndw->a_kalkhndw.sk_vollk = A_kalkhndw->a_kalkhndw.hk_vollk / (1 - A_kalkhndw->a_kalkhndw.sp_vk / 100);
		if (A_kalkhndw->a_kalkhndw.sp_fil >= 100) 
		{
			A_kalkhndw->a_kalkhndw.sp_fil = 0.0;
		}
		A_kalkhndw->a_kalkhndw.fil_ek_vollk = A_kalkhndw->a_kalkhndw.sk_vollk / (1 - A_kalkhndw->a_kalkhndw.sp_fil / 100);
		if (A_kalkhndw->a_kalkhndw.sp_lad >= 100) 
		{
			A_kalkhndw->a_kalkhndw.sp_lad = 0.0;
		}
		A_kalkhndw->a_kalkhndw.lad_vk_vollk = A_kalkhndw->a_kalkhndw.fil_ek_vollk / (1 - A_kalkhndw->a_kalkhndw.sp_lad / 100);
*/
		if (A_pr->a_pr.pr_ek_euro > 0.0)
		{
			SpEkIst = (1 - A_kalkhndw->a_kalkhndw.sk_vollk / A_pr->a_pr.pr_ek_euro) * 100;
		}
		if (pr_vk > 0.0)
		{
			SpVkIst = (1 - A_pr->a_pr.pr_ek_euro / pr_vk) * 100;
		}
		break;
	}

/*
	switch (MarktSpPar)
	{
		case AufschlagPlus:
		case AbschlagPlus:
			A_kalkhndw->a_kalkhndw.lad_vk_vollk	*= mwst;
			break;
	}
*/
	EkDurchschnitt = (A_kalkhndw->a_kalkhndw.pr_ek1 * A_kalkhndw->a_kalkhndw.we_me1 +
					  A_kalkhndw->a_kalkhndw.pr_ek2 * A_kalkhndw->a_kalkhndw.we_me2 + 
					  A_kalkhndw->a_kalkhndw.pr_ek3 * A_kalkhndw->a_kalkhndw.we_me3) /
					 (A_kalkhndw->a_kalkhndw.we_me1 +
					  A_kalkhndw->a_kalkhndw.we_me2 + 
					  A_kalkhndw->a_kalkhndw.we_me3);
	Form.Show ();
}