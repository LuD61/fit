#pragma once
#include "CtrlGrid.h"
#include "FormTab.h"
#include "afxwin.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "BasisdatenPage.h"
#include "UpdateEvent.h"
#include "A_bas.h"
#include "A_hndw.h"
#include "A_eig.h"
#include "A_ean.h"
#include "A_krz.h"
#include "Sys_par.h"
#include "StaticButton.h"

#define IDC_EANSAVE 3010

// CEan-Dialogfeld

class CEan : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CEan)

public:
	CEan(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CEan();

// Dialogfelddaten
	enum { IDD = IDD_EANDATA };

protected:
	int KzCursor;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual void OnSize (UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()

public:
	int dcs_a_par;
	int waa_a_par;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	CBasisdatenPage *Basis;
	CCtrlGrid CtrlGrid;
	CFormTab Form;
	CFont Font;
	A_BAS_CLASS *A_bas;
	A_HNDW_CLASS *A_hndw;
	A_EIG_CLASS *A_eig;
	A_EAN_CLASS *A_ean;
	A_KRZ_CLASS *A_krz;
	SYS_PAR_CLASS *Sys_par;
	long KzCount;
	int TestKzCursor;

	CStatic m_LEanNr;
	CNumEdit m_EanNr;
	CStatic m_LEanBz;
	CTextEdit m_EanBz;
	CButton m_HEan;
	CStatic m_LAkrz;
	CNumEdit m_Akrz;
	CStaticButton m_Save;
	BOOL AkrzGen;
	CStatic m_EanGroup;
	void SetBasis (CBasisdatenPage *Basis);
	virtual void Update ();
	CStatic m_LA;
	CEdit m_A;
	virtual BOOL OnReturn ();
	void UpdateList ();
	BOOL TestEan ();
	void SetAkrzField ();
	void FillAkrz ();
	BOOL GenAkrz ();
	virtual void Delete ();
	virtual void DeleteRow ();
	afx_msg void OnBnClickedHean();
	afx_msg void OnSave ();
    afx_msg BOOL OnToolTipNotify(UINT, NMHDR *,  LRESULT *);
	afx_msg void OnEnKillfocusEanNr();
};
