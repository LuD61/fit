#pragma once
#include "afxwin.h"
#include "BasisdatenPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "mdn.h"
#include "adr.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "a_eig.h"
#include "a_krz.h"
#include "ptabn.h"
#include "DbUniCode.h"
#include "FormTab.h"
#include "StaticButton.h"
#include "DbUniCode.h"
#include "A_varb.h"
#include "A_zut.h"
#include "A_huel.h"
#include "A_mat.h"
#include "Quid.h"
#include "Tier.h"

#define IDC_ACHOICE 3000
#define IDC_QUIDCHOICE 3001
#define IDC_TIERCHOICE 3002

// CMat-Dialogfeld

class CMat : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CMat)

public:
	CMat();
	virtual ~CMat();

// Dialogfelddaten
	enum { IDD = IDD_MATPAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()

public:
	BOOL HideButtons;
	CWnd *Frame;
	CBasisdatenPage *Basis;
	MDN_CLASS *Mdn;
	ADR_CLASS *MdnAdr;
	A_BAS_CLASS *A_bas;
	A_HNDW_CLASS *A_hndw;
	A_EIG_CLASS *A_eig;
	A_KRZ_CLASS *A_krz;
	A_VARB_CLASS *A_varb;
	A_ZUT_CLASS *A_zut;
	A_HUEL_CLASS *A_huel;
	A_MAT_CLASS *A_mat;
	PTABN_CLASS Ptabn;
	QUID_CLASS Quid;
	TIER_CLASS Tier;
	CFont Font;
	CFont lFont;
	CFormTab Form;
	CCtrlGrid CtrlGrid;
	CCtrlGrid AGewGrid;
	CCtrlGrid AGrid;
	CCtrlGrid VarbGrid;
	CCtrlGrid ZutGrid;
	CCtrlGrid HuelGrid;
	CCtrlGrid QuidGrid;
	CCtrlGrid TierGrid;
	CString QuidBez;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CEdit m_MdnName;
	CStatic m_LA;
	CNumEdit m_A;
	CButton m_AChoice;
	CEdit m_A_bz1;
	CEdit m_A_bz2;
	CStatic m_LMat;
	CNumEdit m_Mat;
	CStatic m_LA_gew;
	CNumEdit m_A_gew;
	CStatic m_VarbGroup;
	CStatic m_LTier;
	CNumEdit m_Tier;
//	CComboBox m_Tier;
	CButton m_TierChoice;
	CEdit m_TierBz;
	CStatic m_LAKz;
	CComboBox m_AKz;
	CStatic m_LVpkTyp;
	CComboBox m_VpkTyp;
	CStatic m_LArbZeitFakt;
	CNumEdit m_ArbZeitFakt;
	CStatic m_LEiwGes;
	CNumEdit m_EiwGes;
	CStatic m_LFwp;
	CNumEdit m_Fwp;
	CStatic m_LFettEiw;
	CNumEdit m_FettEiw;
	CStatic m_LBefe;
	CNumEdit m_Befe;
	CStatic m_LBefeIFett;
	CNumEdit m_BefeIFett;
	CStatic m_LLeits;
	CTextEdit m_Leits;
	CStatic m_LFett;
	CNumEdit m_Fett;
	CStatic m_LH2o;
	CNumEdit m_H2o;
	CStatic m_LQuidNr;
	CNumEdit m_QuidNr;
	CButton m_QuidChoice;
	CEdit m_QuidBez;

	CStatic m_LEwg;
	CTextEdit m_Ewg;
    CButton m_DklKz;
	CStatic m_LVerwend;
	CTextEdit m_Verwend;
	CStatic m_LZugMenMax;
	CTextEdit m_ZugMenMax;
	CStatic m_LLgrBdg;
	CTextEdit m_LgrBdg;
	CStatic m_LBemOffs;
	CNumEdit m_BemOffs;
	CStatic m_ZutGroup;

	CStatic m_LKal;
	CTextEdit m_Kal;
	CStatic m_LFarbe;
	CTextEdit m_Farbe;
	CStatic m_LSw;
	CNumEdit m_Sw;
	CStatic m_LTmprMax;
	CNumEdit m_TmprMax;
	CStatic m_LFuelVol;
	CNumEdit m_FuelVol;
	CStatic m_LFuelGew;
	CNumEdit m_FuelGew;
	CStatic m_LHuelTxt;
	CTextEdit m_HuelTxt;
	CStatic m_LBemOffs2;
	CTextEdit m_BemOffs2;
	CStatic m_HuelGroup;

	virtual void UpdatePage ();
	virtual void GetPage ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	void EnableATyp ();
    void FillPtabCombo (CWnd *Control, LPTSTR Item);
	void OnAchoice ();
	void OnQuidchoice ();
	void OnTierchoice ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
	BOOL ReadQuid ();
	BOOL ReadTier ();
};
