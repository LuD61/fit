#include "stdafx.h"
#include "wg.h"

struct WG wg, wg_null;

void WG_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &wg.wg,  SQLSHORT, 0);
    sqlout ((short *) &wg.abt,SQLSHORT,0);
    sqlout ((short *) &wg.bearb_fil,SQLSHORT,0);
    sqlout ((short *) &wg.bearb_lad,SQLSHORT,0);
    sqlout ((short *) &wg.bearb_sk,SQLSHORT,0);
    sqlout ((TCHAR *) wg.best_auto,SQLCHAR,2);
    sqlout ((TCHAR *) wg.bsd_kz,SQLCHAR,2);
    sqlout ((TCHAR *) wg.dec_kz,SQLCHAR,2);
    sqlout ((short *) &wg.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) wg.durch_fil,SQLCHAR,2);
    sqlout ((TCHAR *) wg.durch_sk,SQLCHAR,2);
    sqlout ((TCHAR *) wg.durch_sw,SQLCHAR,2);
    sqlout ((TCHAR *) wg.durch_vk,SQLCHAR,2);
    sqlout ((long *) &wg.erl_kto,SQLLONG,0);
    sqlout ((TCHAR *) wg.hnd_gew,SQLCHAR,2);
    sqlout ((short *) &wg.hwg,SQLSHORT,0);
    sqlout ((double *) &wg.inv_ab_anp,SQLDOUBLE,0);
    sqlout ((double *) &wg.inv_ab_letzt,SQLDOUBLE,0);
    sqlout ((double *) &wg.inv_ab_stnd,SQLDOUBLE,0);
    sqlout ((TCHAR *) wg.kost_kz,SQLCHAR,3);
    sqlout ((short *) &wg.me_einh,SQLSHORT,0);
    sqlout ((short *) &wg.mwst,SQLSHORT,0);
    sqlout ((double *) &wg.pers_rab,SQLDOUBLE,0);
    sqlout ((TCHAR *) wg.pfa,SQLCHAR,2);
    sqlout ((TCHAR *) wg.pr_ueb,SQLCHAR,2);
    sqlout ((TCHAR *) wg.reg_eti,SQLCHAR,2);
    sqlout ((short *) &wg.sais1,SQLSHORT,0);
    sqlout ((short *) &wg.sais2,SQLSHORT,0);
    sqlout ((short *) &wg.sg1,SQLSHORT,0);
    sqlout ((short *) &wg.sg2,SQLSHORT,0);
    sqlout ((TCHAR *) wg.smt,SQLCHAR,2);
    sqlout ((double *) &wg.sp_fil,SQLDOUBLE,0);
    sqlout ((TCHAR *) wg.sp_kz,SQLCHAR,2);
    sqlout ((double *) &wg.sp_sk,SQLDOUBLE,0);
    sqlout ((double *) &wg.sp_vk,SQLDOUBLE,0);
    sqlout ((TCHAR *) wg.stk_lst_kz,SQLCHAR,2);
    sqlout ((double *) &wg.sw,SQLDOUBLE,0);
    sqlout ((short *) &wg.teil_smt,SQLSHORT,0);
    sqlout ((TCHAR *) wg.theke_eti,SQLCHAR,2);
    sqlout ((TCHAR *) wg.ums_verb,SQLCHAR,2);
    sqlout ((TCHAR *) wg.verk_art,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &wg.verk_beg,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &wg.verk_end,SQLDATE,0);
    sqlout ((TCHAR *) wg.waren_eti,SQLCHAR,2);
    sqlout ((long *) &wg.we_kto,SQLLONG,0);
    sqlout ((short *) &wg.wg,SQLSHORT,0);
    sqlout ((TCHAR *) wg.wg_bz1,SQLCHAR,25);
    sqlout ((TCHAR *) wg.wg_bz2,SQLCHAR,25);
    sqlout ((long *) &wg.erl_kto_1,SQLLONG,0);
    sqlout ((long *) &wg.erl_kto_2,SQLLONG,0);
    sqlout ((long *) &wg.erl_kto_3,SQLLONG,0);
    sqlout ((long *) &wg.we_kto_1,SQLLONG,0);
    sqlout ((long *) &wg.we_kto_2,SQLLONG,0);
    sqlout ((long *) &wg.we_kto_3,SQLLONG,0);
            cursor = sqlcursor (_T("select wg.abt,  ")
_T("wg.bearb_fil,  wg.bearb_lad,  wg.bearb_sk,  wg.best_auto,  wg.bsd_kz,  ")
_T("wg.dec_kz,  wg.delstatus,  wg.durch_fil,  wg.durch_sk,  wg.durch_sw,  ")
_T("wg.durch_vk,  wg.erl_kto,  wg.hnd_gew,  wg.hwg,  wg.inv_ab_anp,  ")
_T("wg.inv_ab_letzt,  wg.inv_ab_stnd,  wg.kost_kz,  wg.me_einh,  wg.mwst,  ")
_T("wg.pers_rab,  wg.pfa,  wg.pr_ueb,  wg.reg_eti,  wg.sais1,  wg.sais2,  wg.sg1,  ")
_T("wg.sg2,  wg.smt,  wg.sp_fil,  wg.sp_kz,  wg.sp_sk,  wg.sp_vk,  wg.stk_lst_kz,  ")
_T("wg.sw,  wg.teil_smt,  wg.theke_eti,  wg.ums_verb,  wg.verk_art,  ")
_T("wg.verk_beg,  wg.verk_end,  wg.waren_eti,  wg.we_kto,  wg.wg,  wg.wg_bz1,  ")
_T("wg.wg_bz2,  wg.erl_kto_1,  wg.erl_kto_2,  wg.erl_kto_3,  wg.we_kto_1,  ")
_T("wg.we_kto_2,  wg.we_kto_3 from wg ")

#line 12 "wg.rpp"
                                  _T("where wg = ?"));
    sqlin ((short *) &wg.abt,SQLSHORT,0);
    sqlin ((short *) &wg.bearb_fil,SQLSHORT,0);
    sqlin ((short *) &wg.bearb_lad,SQLSHORT,0);
    sqlin ((short *) &wg.bearb_sk,SQLSHORT,0);
    sqlin ((TCHAR *) wg.best_auto,SQLCHAR,2);
    sqlin ((TCHAR *) wg.bsd_kz,SQLCHAR,2);
    sqlin ((TCHAR *) wg.dec_kz,SQLCHAR,2);
    sqlin ((short *) &wg.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) wg.durch_fil,SQLCHAR,2);
    sqlin ((TCHAR *) wg.durch_sk,SQLCHAR,2);
    sqlin ((TCHAR *) wg.durch_sw,SQLCHAR,2);
    sqlin ((TCHAR *) wg.durch_vk,SQLCHAR,2);
    sqlin ((long *) &wg.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) wg.hnd_gew,SQLCHAR,2);
    sqlin ((short *) &wg.hwg,SQLSHORT,0);
    sqlin ((double *) &wg.inv_ab_anp,SQLDOUBLE,0);
    sqlin ((double *) &wg.inv_ab_letzt,SQLDOUBLE,0);
    sqlin ((double *) &wg.inv_ab_stnd,SQLDOUBLE,0);
    sqlin ((TCHAR *) wg.kost_kz,SQLCHAR,3);
    sqlin ((short *) &wg.me_einh,SQLSHORT,0);
    sqlin ((short *) &wg.mwst,SQLSHORT,0);
    sqlin ((double *) &wg.pers_rab,SQLDOUBLE,0);
    sqlin ((TCHAR *) wg.pfa,SQLCHAR,2);
    sqlin ((TCHAR *) wg.pr_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) wg.reg_eti,SQLCHAR,2);
    sqlin ((short *) &wg.sais1,SQLSHORT,0);
    sqlin ((short *) &wg.sais2,SQLSHORT,0);
    sqlin ((short *) &wg.sg1,SQLSHORT,0);
    sqlin ((short *) &wg.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) wg.smt,SQLCHAR,2);
    sqlin ((double *) &wg.sp_fil,SQLDOUBLE,0);
    sqlin ((TCHAR *) wg.sp_kz,SQLCHAR,2);
    sqlin ((double *) &wg.sp_sk,SQLDOUBLE,0);
    sqlin ((double *) &wg.sp_vk,SQLDOUBLE,0);
    sqlin ((TCHAR *) wg.stk_lst_kz,SQLCHAR,2);
    sqlin ((double *) &wg.sw,SQLDOUBLE,0);
    sqlin ((short *) &wg.teil_smt,SQLSHORT,0);
    sqlin ((TCHAR *) wg.theke_eti,SQLCHAR,2);
    sqlin ((TCHAR *) wg.ums_verb,SQLCHAR,2);
    sqlin ((TCHAR *) wg.verk_art,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &wg.verk_beg,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &wg.verk_end,SQLDATE,0);
    sqlin ((TCHAR *) wg.waren_eti,SQLCHAR,2);
    sqlin ((long *) &wg.we_kto,SQLLONG,0);
    sqlin ((short *) &wg.wg,SQLSHORT,0);
    sqlin ((TCHAR *) wg.wg_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) wg.wg_bz2,SQLCHAR,25);
    sqlin ((long *) &wg.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &wg.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &wg.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &wg.we_kto_1,SQLLONG,0);
    sqlin ((long *) &wg.we_kto_2,SQLLONG,0);
    sqlin ((long *) &wg.we_kto_3,SQLLONG,0);
            sqltext = _T("update wg set wg.abt = ?,  ")
_T("wg.bearb_fil = ?,  wg.bearb_lad = ?,  wg.bearb_sk = ?,  ")
_T("wg.best_auto = ?,  wg.bsd_kz = ?,  wg.dec_kz = ?,  wg.delstatus = ?,  ")
_T("wg.durch_fil = ?,  wg.durch_sk = ?,  wg.durch_sw = ?,  ")
_T("wg.durch_vk = ?,  wg.erl_kto = ?,  wg.hnd_gew = ?,  wg.hwg = ?,  ")
_T("wg.inv_ab_anp = ?,  wg.inv_ab_letzt = ?,  wg.inv_ab_stnd = ?,  ")
_T("wg.kost_kz = ?,  wg.me_einh = ?,  wg.mwst = ?,  wg.pers_rab = ?,  ")
_T("wg.pfa = ?,  wg.pr_ueb = ?,  wg.reg_eti = ?,  wg.sais1 = ?,  ")
_T("wg.sais2 = ?,  wg.sg1 = ?,  wg.sg2 = ?,  wg.smt = ?,  wg.sp_fil = ?,  ")
_T("wg.sp_kz = ?,  wg.sp_sk = ?,  wg.sp_vk = ?,  wg.stk_lst_kz = ?,  ")
_T("wg.sw = ?,  wg.teil_smt = ?,  wg.theke_eti = ?,  wg.ums_verb = ?,  ")
_T("wg.verk_art = ?,  wg.verk_beg = ?,  wg.verk_end = ?,  ")
_T("wg.waren_eti = ?,  wg.we_kto = ?,  wg.wg = ?,  wg.wg_bz1 = ?,  ")
_T("wg.wg_bz2 = ?,  wg.erl_kto_1 = ?,  wg.erl_kto_2 = ?,  ")
_T("wg.erl_kto_3 = ?,  wg.we_kto_1 = ?,  wg.we_kto_2 = ?,  ")
_T("wg.we_kto_3 = ? ")

#line 14 "wg.rpp"
                                  _T("where wg = ?");
            sqlin ((double *)   &wg.wg,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &wg.wg,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select wg from wg ")
                                  _T("where wg = ?"));
            sqlin ((double *)   &wg.wg,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from wg ")
                                  _T("where wg = ?"));
    sqlin ((short *) &wg.abt,SQLSHORT,0);
    sqlin ((short *) &wg.bearb_fil,SQLSHORT,0);
    sqlin ((short *) &wg.bearb_lad,SQLSHORT,0);
    sqlin ((short *) &wg.bearb_sk,SQLSHORT,0);
    sqlin ((TCHAR *) wg.best_auto,SQLCHAR,2);
    sqlin ((TCHAR *) wg.bsd_kz,SQLCHAR,2);
    sqlin ((TCHAR *) wg.dec_kz,SQLCHAR,2);
    sqlin ((short *) &wg.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) wg.durch_fil,SQLCHAR,2);
    sqlin ((TCHAR *) wg.durch_sk,SQLCHAR,2);
    sqlin ((TCHAR *) wg.durch_sw,SQLCHAR,2);
    sqlin ((TCHAR *) wg.durch_vk,SQLCHAR,2);
    sqlin ((long *) &wg.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) wg.hnd_gew,SQLCHAR,2);
    sqlin ((short *) &wg.hwg,SQLSHORT,0);
    sqlin ((double *) &wg.inv_ab_anp,SQLDOUBLE,0);
    sqlin ((double *) &wg.inv_ab_letzt,SQLDOUBLE,0);
    sqlin ((double *) &wg.inv_ab_stnd,SQLDOUBLE,0);
    sqlin ((TCHAR *) wg.kost_kz,SQLCHAR,3);
    sqlin ((short *) &wg.me_einh,SQLSHORT,0);
    sqlin ((short *) &wg.mwst,SQLSHORT,0);
    sqlin ((double *) &wg.pers_rab,SQLDOUBLE,0);
    sqlin ((TCHAR *) wg.pfa,SQLCHAR,2);
    sqlin ((TCHAR *) wg.pr_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) wg.reg_eti,SQLCHAR,2);
    sqlin ((short *) &wg.sais1,SQLSHORT,0);
    sqlin ((short *) &wg.sais2,SQLSHORT,0);
    sqlin ((short *) &wg.sg1,SQLSHORT,0);
    sqlin ((short *) &wg.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) wg.smt,SQLCHAR,2);
    sqlin ((double *) &wg.sp_fil,SQLDOUBLE,0);
    sqlin ((TCHAR *) wg.sp_kz,SQLCHAR,2);
    sqlin ((double *) &wg.sp_sk,SQLDOUBLE,0);
    sqlin ((double *) &wg.sp_vk,SQLDOUBLE,0);
    sqlin ((TCHAR *) wg.stk_lst_kz,SQLCHAR,2);
    sqlin ((double *) &wg.sw,SQLDOUBLE,0);
    sqlin ((short *) &wg.teil_smt,SQLSHORT,0);
    sqlin ((TCHAR *) wg.theke_eti,SQLCHAR,2);
    sqlin ((TCHAR *) wg.ums_verb,SQLCHAR,2);
    sqlin ((TCHAR *) wg.verk_art,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &wg.verk_beg,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &wg.verk_end,SQLDATE,0);
    sqlin ((TCHAR *) wg.waren_eti,SQLCHAR,2);
    sqlin ((long *) &wg.we_kto,SQLLONG,0);
    sqlin ((short *) &wg.wg,SQLSHORT,0);
    sqlin ((TCHAR *) wg.wg_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) wg.wg_bz2,SQLCHAR,25);
    sqlin ((long *) &wg.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &wg.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &wg.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &wg.we_kto_1,SQLLONG,0);
    sqlin ((long *) &wg.we_kto_2,SQLLONG,0);
    sqlin ((long *) &wg.we_kto_3,SQLLONG,0);
            ins_cursor = sqlcursor (_T("insert into wg (abt,  ")
_T("bearb_fil,  bearb_lad,  bearb_sk,  best_auto,  bsd_kz,  dec_kz,  delstatus,  ")
_T("durch_fil,  durch_sk,  durch_sw,  durch_vk,  erl_kto,  hnd_gew,  hwg,  inv_ab_anp,  ")
_T("inv_ab_letzt,  inv_ab_stnd,  kost_kz,  me_einh,  mwst,  pers_rab,  pfa,  pr_ueb,  ")
_T("reg_eti,  sais1,  sais2,  sg1,  sg2,  smt,  sp_fil,  sp_kz,  sp_sk,  sp_vk,  stk_lst_kz,  sw,  ")
_T("teil_smt,  theke_eti,  ums_verb,  verk_art,  verk_beg,  verk_end,  waren_eti,  ")
_T("we_kto,  wg,  wg_bz1,  wg_bz2,  erl_kto_1,  erl_kto_2,  erl_kto_3,  we_kto_1,  ")
_T("we_kto_2,  we_kto_3) ")

#line 25 "wg.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 27 "wg.rpp"
}
