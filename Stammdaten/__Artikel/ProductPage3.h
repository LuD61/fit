#pragma once
#include "BasisDatenPage.h"
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "a_bas.h"
#include "a_bas_erw.h"
#include "a_hndw.h"
#include "a_eig.h"
#include "ptabn.h"
#include "DbUniCode.h"
#include "afxwin.h"


// CProductPage3-Dialogfeld

class CProductPage3 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CProductPage3)

public:
	CProductPage3();
	virtual ~CProductPage3();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCT_PAGE3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()
public:
	int tabx;
	int taby;
	BOOL HideButtons;
	CBasisdatenPage *Basis;
	CWnd *Frame;
	CFormTab Form;
	CFormTab DisableEigs;
	CCtrlGrid CtrlGrid;
	CFont Font;
	CFont BoldFont;
	A_BAS_CLASS *A_bas;
	A_BAS_ERW_CLASS *A_bas_erw;
	COLORREF BoldColor;
	BOOL ProductPage3Read;


	CStatic m_Aub1;
	CStatic m_Aub2;
    CStatic m_LGlg1; 
    CStatic m_LGlg2; 
    CStatic m_LKrebs1; 
    CStatic m_LKrebs2; 
    CStatic m_LEi1; 
    CStatic m_LEi2; 
    CStatic m_LFisch1; 
    CStatic m_LFisch2; 
    CStatic m_LErdnuss1; 
    CStatic m_LErdnuss2; 
    CStatic m_LSoja1; 
    CStatic m_LSoja2; 
    CStatic m_LMilch1; 
    CStatic m_LMilch2; 
    CStatic m_LSchal1; 
    CStatic m_LSchal2; 
    CStatic m_LSellerie1; 
    CStatic m_LSellerie2; 
    CStatic m_LSenfSaat1; 
    CStatic m_LSenfSaat2; 
    CStatic m_LSesamSamen1; 
    CStatic m_LSesamSamen2; 
    CStatic m_LSulfit; 
    CStatic m_LWeichtier; 
    CStatic m_LSonstige; 

    CTextEdit m_Glg; 
    CTextEdit m_Krebs; 
    CTextEdit m_Ei; 
    CTextEdit m_Fisch; 
    CTextEdit m_Erdnuss; 
    CTextEdit m_Soja; 
    CTextEdit m_Milch; 
    CTextEdit m_Schal; 
    CTextEdit m_Sellerie; 
    CTextEdit m_SenfSaat; 
    CTextEdit m_SesamSamen; 
    CTextEdit m_Sulfit; 
    CTextEdit m_Weichtier; 
    CTextEdit m_Sonstige; 

	virtual void UpdatePage ();
	virtual void OnDelete ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeydown ();
    virtual BOOL OnKeyup ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
    virtual void OnCopy ();
    virtual void OnPaste ();
	BOOL Read ();

	CStatic m_LLupine;
	CButton m_Lupine;
};
