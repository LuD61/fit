#ifndef _SAIS_DEF
#define _SAIS_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct SAIS {
   short          delstatus;
   short          jr;
   TCHAR          pers_nam[9];
   short          sais;
   DATE_STRUCT    st_aktiv;
   DATE_STRUCT    st_bearbeit;
   TCHAR          st_bz_1[25];
   TCHAR          st_bz_2[25];
   DATE_STRUCT    st_nordb;
   DATE_STRUCT    st_nordv;
   DATE_STRUCT    st_vkztb;
   DATE_STRUCT    st_vkztv;
   DATE_STRUCT    st_vordb;
   DATE_STRUCT    st_vordv;
};
extern struct SAIS sais, sais_null;

#line 8 "Sais.rh"

class SAIS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SAIS sais;
               SAIS_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (SAIS&);  
};
#endif
