// KtoDialog.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "KtoDialog.h"


// CKtoDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CKtoDialog, CDialog)
CKtoDialog::CKtoDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CKtoDialog::IDD, pParent)
{
}

CKtoDialog::~CKtoDialog()
{
}

void CKtoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_WEKTO1, m_WeKto1);
	DDX_Control(pDX, IDC_WEKTO2, m_WeKto2);
	DDX_Control(pDX, IDC_WEKTO3, m_WeKto3);
	DDX_Control(pDX, IDC_ERLKTO1, m_ErlKto1);
	DDX_Control(pDX, IDC_ERLKTO2, m_ErlKto2);
	DDX_Control(pDX, IDC_ERLKTO3, m_ErlKto3);
	DDX_Control(pDX, IDC_WE_KTO, m_WeKto);
}


BEGIN_MESSAGE_MAP(CKtoDialog, CDialog)
	ON_COMMAND(IDC_WE_KTO, OnWeKto)
END_MESSAGE_MAP()


// CKtoDialog-Meldungshandler

BOOL CKtoDialog::OnInitDialog ()
{
	BOOL ret = CDialog::OnInitDialog ();

	m_WeKto.nID = IDC_WE_KTO;
	m_WeKto.SetWindowText (_T("Konten\nf�r Mandanten"));

    Form.Add (new CFormField (&m_WeKto1,EDIT, (long *) &A_bas.a_bas.we_kto_1, VLONG));
    Form.Add (new CFormField (&m_WeKto2,EDIT, (long *) &A_bas.a_bas.we_kto_2, VLONG));
    Form.Add (new CFormField (&m_WeKto3,EDIT, (long *) &A_bas.a_bas.we_kto_3, VLONG));
    Form.Add (new CFormField (&m_ErlKto1,EDIT, (long *) &A_bas.a_bas.erl_kto_1, VLONG));
    Form.Add (new CFormField (&m_ErlKto2,EDIT, (long *) &A_bas.a_bas.erl_kto_2, VLONG));
    Form.Add (new CFormField (&m_ErlKto3,EDIT, (long *) &A_bas.a_bas.erl_kto_3, VLONG));
	Form.Show ();
    
    return TRUE;
}

BOOL CKtoDialog::PreTranslateMessage(MSG* pMsg)
{
//	CWnd *cWnd = NULL;
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_DOWN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
	}
	return CDialog::PreTranslateMessage (pMsg);
}

void CKtoDialog::OnOK ()
{
	Form.Get ();
	CDialog::OnOK ();
}

void CKtoDialog::OnWeKto()
{
	CUstKtoDialog dlg;
	dlg.A_bas =  &A_bas;
	INT_PTR ret = dlg.DoModal ();
}
