#ifndef _A_EMB_DEF
#define _A_EMB_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_EMB {
   short          anz_emb;
   short          delstatus;
   double         emb;
   TCHAR          emb_bz[25];
   TCHAR          emb_vk_kz[2];
   double         tara;
   double         tara_proz;
   double         unt_emb;
   double         a_pfa;
};
extern struct A_EMB a_emb, a_emb_null;

#line 8 "a_emb.rh"

class A_EMB_CLASS : public DB_CLASS 
{
       private :
               int UntEmbCursor;
               int EmbCursor;
               void prepare (void);
       public :
               A_EMB a_emb;  
               A_EMB_CLASS () : DB_CLASS ()
               {
		    UntEmbCursor = -1;		
		    EmbCursor = -1;		
               }
               ~A_EMB_CLASS ()
               {
		    if (UntEmbCursor != -1)
                    {
                           sqlclose (UntEmbCursor);
                    }		
		    if (EmbCursor != -1)		
                    {
                           sqlclose (EmbCursor);
                    }		
               }

               int dbreadfirst_unt_emb ();  
               int dbread_unt_emb ();  
               int dbreadfirst_emb ();  
               int dbread_emb ();  
};
#endif
