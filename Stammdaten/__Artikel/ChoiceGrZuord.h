#ifndef _CHOICEGR_ZUORD_DEF
#define _CHOICEGR_ZUORD_DEF

#include "ChoiceX.h"
#include "GrZuordList.h"
#include <vector>

class CChoiceGrZuord : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
      
    public :
		short m_Mdn;
	    std::vector<CGrZuordList *> GrZuordList;
      	CChoiceGrZuord(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceGrZuord(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchGrBz1 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CGrZuordList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
