// ProductPage2.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "ProductPage2.h"
#include "UniFormField.h"
#include "CtrlLine.h"


// CProductPage2-Dialogfeld

IMPLEMENT_DYNAMIC(CProductPage2, CDbPropertyPage)

CProductPage2::CProductPage2()
	: CDbPropertyPage(CProductPage2::IDD)
{
	tabx = 0;
	taby = 21;
	ProductPage2Read = FALSE;
//	BoldColor = RGB (0,0,255);
	BoldColor = RGB (0,128,192);

	MibValues[0] = Prp_mibwerte.prp_mibwerte.wert1;
	MibValues[1] = Prp_mibwerte.prp_mibwerte.wert2;
	MibValues[2] = Prp_mibwerte.prp_mibwerte.wert3;
	MibValues[3] = Prp_mibwerte.prp_mibwerte.wert4;
	MibValues[4] = Prp_mibwerte.prp_mibwerte.wert5;
	MibValues[5] = Prp_mibwerte.prp_mibwerte.wert6;
	MibValues[6] = Prp_mibwerte.prp_mibwerte.wert7;
	MibValues[7] = Prp_mibwerte.prp_mibwerte.wert8;
	MibValues[8] = Prp_mibwerte.prp_mibwerte.wert9;
	MibValues[9] = Prp_mibwerte.prp_mibwerte.wert10;
	MibValues[10] = Prp_mibwerte.prp_mibwerte.wert11;
	MibValues[11] = Prp_mibwerte.prp_mibwerte.wert12;
	MibValues[12] = Prp_mibwerte.prp_mibwerte.wert13;
	MibValues[13] = Prp_mibwerte.prp_mibwerte.wert14;
	MibValues[14] = Prp_mibwerte.prp_mibwerte.wert15;
	MibValues[15] = Prp_mibwerte.prp_mibwerte.wert16;
	MibValues[16] = Prp_mibwerte.prp_mibwerte.wert17;
	MibValues[17] = Prp_mibwerte.prp_mibwerte.wert18;
	MibValues[18] = Prp_mibwerte.prp_mibwerte.wert19;
	MibValues[19] = Prp_mibwerte.prp_mibwerte.wert20;
	MibValues[20] = Prp_mibwerte.prp_mibwerte.wert21;
	MibValues[21] = Prp_mibwerte.prp_mibwerte.wert22;
	MibValues[22] = Prp_mibwerte.prp_mibwerte.wert23;
	MibValues[23] = Prp_mibwerte.prp_mibwerte.wert24;
	MibValues[24] = Prp_mibwerte.prp_mibwerte.wert25;
	MibValues[25] = Prp_mibwerte.prp_mibwerte.wert26;
	MibValues[26] = Prp_mibwerte.prp_mibwerte.wert27;
	MibValues[27] = Prp_mibwerte.prp_mibwerte.wert28;
	MibValues[28] = Prp_mibwerte.prp_mibwerte.wert29;
	MibValues[29] = Prp_mibwerte.prp_mibwerte.wert30;

	MibRecordNames[0] = Prp_mibwerte.prp_mibwerte.name1;
	MibRecordNames[1] = Prp_mibwerte.prp_mibwerte.name2;
	MibRecordNames[2] = Prp_mibwerte.prp_mibwerte.name3;
	MibRecordNames[3] = Prp_mibwerte.prp_mibwerte.name4;
	MibRecordNames[4] = Prp_mibwerte.prp_mibwerte.name5;
	MibRecordNames[5] = Prp_mibwerte.prp_mibwerte.name6;
	MibRecordNames[6] = Prp_mibwerte.prp_mibwerte.name7;
	MibRecordNames[7] = Prp_mibwerte.prp_mibwerte.name8;
	MibRecordNames[8] = Prp_mibwerte.prp_mibwerte.name9;
	MibRecordNames[9] = Prp_mibwerte.prp_mibwerte.name10;
	MibRecordNames[10] = Prp_mibwerte.prp_mibwerte.name11;
	MibRecordNames[11] = Prp_mibwerte.prp_mibwerte.name12;
	MibRecordNames[12] = Prp_mibwerte.prp_mibwerte.name13;
	MibRecordNames[13] = Prp_mibwerte.prp_mibwerte.name14;
	MibRecordNames[14] = Prp_mibwerte.prp_mibwerte.name15;
	MibRecordNames[15] = Prp_mibwerte.prp_mibwerte.name16;
	MibRecordNames[16] = Prp_mibwerte.prp_mibwerte.name17;
	MibRecordNames[17] = Prp_mibwerte.prp_mibwerte.name18;
	MibRecordNames[18] = Prp_mibwerte.prp_mibwerte.name19;
	MibRecordNames[19] = Prp_mibwerte.prp_mibwerte.name20;
	MibRecordNames[20] = Prp_mibwerte.prp_mibwerte.name21;
	MibRecordNames[21] = Prp_mibwerte.prp_mibwerte.name22;
	MibRecordNames[22] = Prp_mibwerte.prp_mibwerte.name23;
	MibRecordNames[23] = Prp_mibwerte.prp_mibwerte.name24;
	MibRecordNames[24] = Prp_mibwerte.prp_mibwerte.name25;
	MibRecordNames[25] = Prp_mibwerte.prp_mibwerte.name26;
	MibRecordNames[26] = Prp_mibwerte.prp_mibwerte.name27;
	MibRecordNames[27] = Prp_mibwerte.prp_mibwerte.name28;
	MibRecordNames[28] = Prp_mibwerte.prp_mibwerte.name29;
	MibRecordNames[29] = Prp_mibwerte.prp_mibwerte.name30;

	CppValues[0] = Prp_cpp.prp_cpp.wert1;
	CppValues[1] = Prp_cpp.prp_cpp.wert2;
	CppValues[2] = Prp_cpp.prp_cpp.wert3;
	CppValues[3] = Prp_cpp.prp_cpp.wert4;
	CppValues[4] = Prp_cpp.prp_cpp.wert5;
	CppValues[5] = Prp_cpp.prp_cpp.wert6;
	CppValues[6] = Prp_cpp.prp_cpp.wert7;
	CppValues[7] = Prp_cpp.prp_cpp.wert8;
	CppValues[8] = Prp_cpp.prp_cpp.wert9;
	CppValues[9] = Prp_cpp.prp_cpp.wert10;
	CppValues[10] = Prp_cpp.prp_cpp.wert11;
	CppValues[11] = Prp_cpp.prp_cpp.wert12;
	CppValues[12] = Prp_cpp.prp_cpp.wert13;
	CppValues[13] = Prp_cpp.prp_cpp.wert14;
	CppValues[14] = Prp_cpp.prp_cpp.wert15;
	CppValues[15] = Prp_cpp.prp_cpp.wert16;
	CppValues[16] = Prp_cpp.prp_cpp.wert17;
	CppValues[17] = Prp_cpp.prp_cpp.wert18;
	CppValues[18] = Prp_cpp.prp_cpp.wert19;
	CppValues[19] = Prp_cpp.prp_cpp.wert20;
	CppValues[20] = Prp_cpp.prp_cpp.wert21;
	CppValues[21] = Prp_cpp.prp_cpp.wert22;
	CppValues[22] = Prp_cpp.prp_cpp.wert23;
	CppValues[23] = Prp_cpp.prp_cpp.wert24;
	CppValues[24] = Prp_cpp.prp_cpp.wert25;
	CppValues[25] = Prp_cpp.prp_cpp.wert26;
	CppValues[26] = Prp_cpp.prp_cpp.wert27;
	CppValues[27] = Prp_cpp.prp_cpp.wert28;
	CppValues[28] = Prp_cpp.prp_cpp.wert29;
	CppValues[29] = Prp_cpp.prp_cpp.wert30;

	CppRecordNames[0] = Prp_cpp.prp_cpp.name1;
	CppRecordNames[1] = Prp_cpp.prp_cpp.name2;
	CppRecordNames[2] = Prp_cpp.prp_cpp.name3;
	CppRecordNames[3] = Prp_cpp.prp_cpp.name4;
	CppRecordNames[4] = Prp_cpp.prp_cpp.name5;
	CppRecordNames[5] = Prp_cpp.prp_cpp.name6;
	CppRecordNames[6] = Prp_cpp.prp_cpp.name7;
	CppRecordNames[7] = Prp_cpp.prp_cpp.name8;
	CppRecordNames[8] = Prp_cpp.prp_cpp.name9;
	CppRecordNames[9] = Prp_cpp.prp_cpp.name10;
	CppRecordNames[10] = Prp_cpp.prp_cpp.name11;
	CppRecordNames[11] = Prp_cpp.prp_cpp.name12;
	CppRecordNames[12] = Prp_cpp.prp_cpp.name13;
	CppRecordNames[13] = Prp_cpp.prp_cpp.name14;
	CppRecordNames[14] = Prp_cpp.prp_cpp.name15;
	CppRecordNames[15] = Prp_cpp.prp_cpp.name16;
	CppRecordNames[16] = Prp_cpp.prp_cpp.name17;
	CppRecordNames[17] = Prp_cpp.prp_cpp.name18;
	CppRecordNames[18] = Prp_cpp.prp_cpp.name19;
	CppRecordNames[19] = Prp_cpp.prp_cpp.name20;
	CppRecordNames[20] = Prp_cpp.prp_cpp.name21;
	CppRecordNames[21] = Prp_cpp.prp_cpp.name22;
	CppRecordNames[22] = Prp_cpp.prp_cpp.name23;
	CppRecordNames[23] = Prp_cpp.prp_cpp.name24;
	CppRecordNames[24] = Prp_cpp.prp_cpp.name25;
	CppRecordNames[25] = Prp_cpp.prp_cpp.name26;
	CppRecordNames[26] = Prp_cpp.prp_cpp.name27;
	CppRecordNames[27] = Prp_cpp.prp_cpp.name28;
	CppRecordNames[28] = Prp_cpp.prp_cpp.name29;
	CppRecordNames[29] = Prp_cpp.prp_cpp.name30;

	VikValues[0] = Prp_vik.prp_vik.wert1;
	VikValues[1] = Prp_vik.prp_vik.wert2;
	VikValues[2] = Prp_vik.prp_vik.wert3;
	VikValues[3] = Prp_vik.prp_vik.wert4;
	VikValues[4] = Prp_vik.prp_vik.wert5;
	VikValues[5] = Prp_vik.prp_vik.wert6;
	VikValues[6] = Prp_vik.prp_vik.wert7;
	VikValues[7] = Prp_vik.prp_vik.wert8;
	VikValues[8] = Prp_vik.prp_vik.wert9;
	VikValues[9] = Prp_vik.prp_vik.wert10;
	VikValues[10] = Prp_vik.prp_vik.wert11;
	VikValues[11] = Prp_vik.prp_vik.wert12;
	VikValues[12] = Prp_vik.prp_vik.wert13;
	VikValues[13] = Prp_vik.prp_vik.wert14;
	VikValues[14] = Prp_vik.prp_vik.wert15;
	VikValues[15] = Prp_vik.prp_vik.wert16;
	VikValues[16] = Prp_vik.prp_vik.wert17;
	VikValues[17] = Prp_vik.prp_vik.wert18;
	VikValues[18] = Prp_vik.prp_vik.wert19;
	VikValues[19] = Prp_vik.prp_vik.wert20;
	VikValues[20] = Prp_vik.prp_vik.wert21;
	VikValues[21] = Prp_vik.prp_vik.wert22;
	VikValues[22] = Prp_vik.prp_vik.wert23;
	VikValues[23] = Prp_vik.prp_vik.wert24;
	VikValues[24] = Prp_vik.prp_vik.wert25;
	VikValues[25] = Prp_vik.prp_vik.wert26;
	VikValues[26] = Prp_vik.prp_vik.wert27;
	VikValues[27] = Prp_vik.prp_vik.wert28;
	VikValues[28] = Prp_vik.prp_vik.wert29;
	VikValues[29] = Prp_vik.prp_vik.wert30;

	VikRecordNames[0] = Prp_vik.prp_vik.name1;
	VikRecordNames[1] = Prp_vik.prp_vik.name2;
	VikRecordNames[2] = Prp_vik.prp_vik.name3;
	VikRecordNames[3] = Prp_vik.prp_vik.name4;
	VikRecordNames[4] = Prp_vik.prp_vik.name5;
	VikRecordNames[5] = Prp_vik.prp_vik.name6;
	VikRecordNames[6] = Prp_vik.prp_vik.name7;
	VikRecordNames[7] = Prp_vik.prp_vik.name8;
	VikRecordNames[8] = Prp_vik.prp_vik.name9;
	VikRecordNames[9] = Prp_vik.prp_vik.name10;
	VikRecordNames[10] = Prp_vik.prp_vik.name11;
	VikRecordNames[11] = Prp_vik.prp_vik.name12;
	VikRecordNames[12] = Prp_vik.prp_vik.name13;
	VikRecordNames[13] = Prp_vik.prp_vik.name14;
	VikRecordNames[14] = Prp_vik.prp_vik.name15;
	VikRecordNames[15] = Prp_vik.prp_vik.name16;
	VikRecordNames[16] = Prp_vik.prp_vik.name17;
	VikRecordNames[17] = Prp_vik.prp_vik.name18;
	VikRecordNames[18] = Prp_vik.prp_vik.name19;
	VikRecordNames[19] = Prp_vik.prp_vik.name20;
	VikRecordNames[20] = Prp_vik.prp_vik.name21;
	VikRecordNames[21] = Prp_vik.prp_vik.name22;
	VikRecordNames[22] = Prp_vik.prp_vik.name23;
	VikRecordNames[23] = Prp_vik.prp_vik.name24;
	VikRecordNames[24] = Prp_vik.prp_vik.name25;
	VikRecordNames[25] = Prp_vik.prp_vik.name26;
	VikRecordNames[26] = Prp_vik.prp_vik.name27;
	VikRecordNames[27] = Prp_vik.prp_vik.name28;
	VikRecordNames[28] = Prp_vik.prp_vik.name29;
	VikRecordNames[29] = Prp_vik.prp_vik.name30;

	Cfg.SetProgName( _T("Artikel"));
	UserFields = FALSE;
	ReadCfg ();

}

CProductPage2::~CProductPage2()
{
}

void CProductPage2::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LHUEL, m_LHuel);
	DDX_Control(pDX, IDC_LHUEL_ART, m_LHuelArt);
	DDX_Control(pDX, IDC_HUEL_ART, m_HuelArt);
	DDX_Control(pDX, IDC_LHUEL_MAT, m_LHuelMat);
	DDX_Control(pDX, IDC_HUEL_MAT, m_HuelMat);
	DDX_Control(pDX, IDC_LAUSZ_ART, m_LAuszArt);
	DDX_Control(pDX, IDC_AUSZ_ART, m_AuszArt);
	DDX_Control(pDX, IDC_LEAN_PACKUNG, m_LEanPackung);
	DDX_Control(pDX, IDC_EAN_PACKUNG, m_EanPackung);
	DDX_Control(pDX, IDC_LEAN_KARTON, m_LEanKarton);
	DDX_Control(pDX, IDC_EAN_KARTON, m_EanKarton);
	DDX_Control(pDX, IDC_LPR_AUSZ, m_LPrAusz);
	DDX_Control(pDX, IDC_PR_AUSZ, m_PrAusz);
	DDX_Control(pDX, IDC_HUEL_PEEL, m_HuelPeel);
	DDX_Control(pDX, IDC_GRUENER_PUNKT, m_GruenerPunkt);
	DDX_Control(pDX, IDC_VAKUUMIERT, m_Vakuumiert);
	DDX_Control(pDX, IDC_LGEBINDE, m_LGebinde);
	DDX_Control(pDX, IDC_LGEBINDE_ART, m_LGebindeArt);
	DDX_Control(pDX, IDC_GEBINDE_ART, m_GebindeArt);
	DDX_Control(pDX, IDC_LGEBINDE_GROESSE, m_LGebindeGroesse);
	DDX_Control(pDX, IDC_GEBINDE_GROESSE, m_GebindeGroesse);
	DDX_Control(pDX, IDC_LGEBINDE_LEER_GEW, m_LGebindeLeerGew);
	DDX_Control(pDX, IDC_GEBINDE_LEER_GEW, m_GebindeLeerGew);
	DDX_Control(pDX, IDC_LGEBINDE_BRUTTO_GEW, m_LGebindeBruttoGew);
	DDX_Control(pDX, IDC_GEBINDE_BRUTTO_GEW, m_GebindeBruttoGew);
	DDX_Control(pDX, IDC_LGEBINDE_PACK, m_LGebindePack);
	DDX_Control(pDX, IDC_GEBINDE_PACK, m_GebindePack);
	DDX_Control(pDX, IDC_LGEBINDEPROLAGE, m_LGebindeProLage);
	DDX_Control(pDX, IDC_GEBINDEPROLAGE, m_GebindeProLage);
	DDX_Control(pDX, IDC_LGEBINDE_ANZ_LAGEN, m_LGebindeAnzLagen);
	DDX_Control(pDX, IDC_GEBINDE_ANZ_LAGEN, m_GebindeAnzLagen);
	DDX_Control(pDX, IDC_LGEBINDE_PALETTE, m_LGebindePalette);
	DDX_Control(pDX, IDC_GEBINDE_PALETTE, m_GebindePalette);
	DDX_Control(pDX, IDC_LCHEM_PHYS_PARAM, m_LChemPhysParam);
	DDX_Control(pDX, IDC_LBEFFE, m_LBeffe);
	DDX_Control(pDX, IDC_BEFFE, m_Beffe);
	DDX_Control(pDX, IDC_LBEFFE_FE, m_LBeffeFe);
	DDX_Control(pDX, IDC_BEFFE_FE, m_BeffeFe);
	DDX_Control(pDX, IDC_LNAEHRWERT, m_LNaehrWert);
	DDX_Control(pDX, IDC_LNAEHRWERT_TEXT, m_LNaehrWertText);
	DDX_Control(pDX, IDC_LBRENNWERT, m_LBrennWert);
	DDX_Control(pDX, IDC_BRENNWERT, m_BrennWert);
	DDX_Control(pDX, IDC_LEIWEISS, m_LEiWeiss);
	DDX_Control(pDX, IDC_EIWEISS, m_EiWeiss);
	DDX_Control(pDX, IDC_LKH, m_LKh);
	DDX_Control(pDX, IDC_KH, m_Kh);
	DDX_Control(pDX, IDC_LFETT, m_LFett);
	DDX_Control(pDX, IDC_FETT, m_Fett);
	DDX_Control(pDX, IDC_LMIKRO, m_LMikro);
	DDX_Control(pDX, IDC_LAEROB, m_LAerob);
	DDX_Control(pDX, IDC_AEROB, m_Aerob);
	DDX_Control(pDX, IDC_LCOLI, m_LColi);
	DDX_Control(pDX, IDC_COLI, m_Coli);
	DDX_Control(pDX, IDC_LKEIME, m_LKeime);
	DDX_Control(pDX, IDC_KEIME, m_Keime);
	DDX_Control(pDX, IDC_LLISTERIEN, m_LListerien);
	DDX_Control(pDX, IDC_LISTERIEN, m_Listerien);
	DDX_Control(pDX, IDC_LLIST_MONO, m_LListMono);
	DDX_Control(pDX, IDC_LIST_MONO, m_ListMono);
	DDX_Control(pDX, IDC_LPRODPASS_EXTRA, m_LProdPassExtra);
	DDX_Control(pDX, IDC_PRODPASS_EXTRA, m_ProdPassExtra);
	DDX_Control(pDX, IDC_LGMO_GVO_STATUS, m_LGmoGvoStatus);
	DDX_Control(pDX, IDC_GMO_GVO_STATUS, m_GmoGvoStatus);
	DDX_Control(pDX, IDC_MIBWLIST, m_MibwList);
	DDX_Control(pDX, IDC_CPPLIST, m_CppList);
	DDX_Control(pDX, IDC_LVIK, m_LVik);
	DDX_Control(pDX, IDC_VIKLIST, m_VikList);
	DDX_Control(pDX, IDC_SCHUTZGAS, m_Schutzgas);
	DDX_Control(pDX, IDC_LHUELLE, m_Huelle);
	DDX_Control(pDX, IDC_HUELLE, m_CHuelle);
	DDX_Control(pDX, IDC_LSALZ, m_LSalz);
	DDX_Control(pDX, IDC_SALZ, m_Salz);
	DDX_Control(pDX, IDC_LDAVONFETTS, m_LDavonFetts);
	DDX_Control(pDX, IDC_DAVONFETTS, m_DavonFetts);
	DDX_Control(pDX, IDC_LDAVONZUCKER, m_LDavonZucker);
	DDX_Control(pDX, IDC_DAVONZUCKER, m_DavonZucker);
	DDX_Control(pDX, IDC_LBALLASTSTOFF, m_LBallaststoff);
//	DDX_Control(pDX, IDC_LLBH, m_LBH);
	DDX_Control(pDX, IDC_BALLASTSOFF, m_Ballaststoff);
}


BEGIN_MESSAGE_MAP(CProductPage2, CPropertyPage)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_STN_CLICKED(IDC_LDAVONFETTS, &CProductPage2::OnStnClickedLdavonfetts)
END_MESSAGE_MAP()


// CProductPage2-Meldungshandler

BOOL CProductPage2::OnInitDialog()
{

	ProductPage2Read = TRUE;

	CPropertyPage::OnInitDialog();
	this->A_bas  = &Basis->A_bas;
	this->A_bas_erw  = &Basis->A_bas_erw;
	this->DlgBkColor = Basis->DlgBkColor;

	memcpy (&Prp_mibwerte.prp_mibwerte, &prp_mibwerte_null, sizeof (PRP_MIBWERTE));
	Prp_mibwerte.prp_mibwerte.a  = -1.0;
	Prp_mibwerte.dbreadfirst ();
	FillNames (MibNames, MibRecordNames);

	memcpy (&Prp_cpp.prp_cpp, &prp_cpp_null, sizeof (PRP_CPP));
	Prp_cpp.prp_cpp.a  = -1.0;
	Prp_cpp.dbreadfirst ();
	FillNames (CppNames, CppRecordNames);

	memcpy (&Prp_vik.prp_vik, &prp_vik_null, sizeof (PRP_VIK));
	Prp_vik.prp_vik.a  = -1.0;
	Prp_vik.dbreadfirst ();
	FillNames (VikNames, VikRecordNames);

	Basis->Form.Get ();
    Basis->ProductPage2Read = TRUE;

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	BoldFont.CreateFontIndirect (&l);

	if (UserFields)
	{
		m_LAerob.ShowWindow (SW_HIDE); 
		m_Aerob.ShowWindow (SW_HIDE);
		m_LColi.ShowWindow (SW_HIDE); 
		m_Coli.ShowWindow (SW_HIDE); 
		m_LKeime.ShowWindow (SW_HIDE); 
		m_Keime.ShowWindow (SW_HIDE); 
		m_LListerien.ShowWindow (SW_HIDE); 
		m_Listerien.ShowWindow (SW_HIDE); 
		m_LListMono.ShowWindow (SW_HIDE); 
		m_ListMono.ShowWindow (SW_HIDE); 
		m_LBeffe.ShowWindow (SW_HIDE);
		m_Beffe.ShowWindow (SW_HIDE);
		m_LBeffeFe.ShowWindow (SW_HIDE);
		m_BeffeFe.ShowWindow (SW_HIDE);
	}
	else
	{	
		m_MibwList.ShowWindow (SW_HIDE); 
		m_CppList.ShowWindow (SW_HIDE); 
		m_VikList.ShowWindow (SW_HIDE); 
		m_LVik.ShowWindow (SW_HIDE); 
	}	

    Form.Add (new CUniFormField (&m_HuelArt,EDIT,     (char *)   A_bas->a_bas.huel_art, VCHAR));
    Form.Add (new CUniFormField (&m_HuelMat,EDIT,     (char *)   A_bas->a_bas.huel_mat, VCHAR));
    Form.Add (new CUniFormField (&m_AuszArt,EDIT,     (char *)   A_bas->a_bas.ausz_art, VCHAR));
    Form.Add (new CUniFormField (&m_EanPackung,EDIT,     (char *)   A_bas->a_bas.ean_packung, VCHAR));
	Form.Add (new CUniFormField (&m_EanKarton,EDIT,     (char *)   A_bas->a_bas.ean_karton, VCHAR));
	Form.Add (new CUniFormField (&m_EanKarton,EDIT,     (char *)   A_bas->a_bas.ean_karton, VCHAR));
	Form.Add (new CUniFormField (&m_PrAusz,EDIT,     (char *)   A_bas->a_bas.pr_ausz, VCHAR));
	Form.Add (new CUniFormField (&m_CHuelle,COMBOBOX,     (short *)   &A_bas_erw->a_bas_erw.huelle, VSHORT));
	Form.Add (new CUniFormField (&m_HuelPeel,CHECKBOX,     (char *)   A_bas->a_bas.huel_peel, VCHAR));
	Form.Add (new CUniFormField (&m_Vakuumiert,CHECKBOX,     (char *) A_bas->a_bas.vakuumiert, VCHAR));
	Form.Add (new CUniFormField (&m_Schutzgas,CHECKBOX,     (char *) A_bas_erw->a_bas_erw.schutzgas, VCHAR));
	Form.Add (new CUniFormField (&m_GruenerPunkt,CHECKBOX,    (char *)   A_bas->a_bas.gruener_punkt, VCHAR));

	Form.Add (new CUniFormField (&m_GebindeArt,EDIT,         (char *)   A_bas->a_bas.gebinde_art, VCHAR));
	Form.Add (new CUniFormField (&m_GebindeGroesse,EDIT,     (char *)   A_bas->a_bas.gebinde_mass, VCHAR));
	Form.Add (new CUniFormField (&m_GebindeLeerGew,EDIT,     (char *)   A_bas->a_bas.gebinde_leer_gew, VCHAR));
	Form.Add (new CUniFormField (&m_GebindeBruttoGew,EDIT,     (char *)   A_bas->a_bas.gebinde_brutto_gew, VCHAR));
	Form.Add (new CFormField (&m_GebindePack,EDIT,     (short *)   &A_bas->a_bas.gebinde_pack, VSHORT));
	Form.Add (new CFormField (&m_GebindeProLage,EDIT,     (short *) &A_bas->a_bas.gebindeprolage, VSHORT));
	Form.Add (new CFormField (&m_GebindeAnzLagen,EDIT,     (short *) &A_bas->a_bas.gebindeprolage, VSHORT));
	Form.Add (new CFormField (&m_GebindePalette,EDIT,     (short *) &A_bas->a_bas.gebindepalette, VSHORT));
	Form.Add (new CUniFormField (&m_Beffe,EDIT,     (char *)   A_bas->a_bas.beffe, VCHAR));
	Form.Add (new CUniFormField (&m_BeffeFe,EDIT,     (char *)   A_bas->a_bas.beffe_fe, VCHAR));
	Form.Add (new CUniFormField (&m_BrennWert,EDIT,     (char *)   A_bas->a_bas.brennwert, VCHAR));
	Form.Add (new CFormField (&m_EiWeiss,EDIT,     (double *)   &A_bas->a_bas.eiweiss, VDOUBLE, 4,2));
	Form.Add (new CFormField (&m_Kh,EDIT,     (double *)   &A_bas->a_bas.kh, VDOUBLE, 4,2));
	Form.Add (new CFormField (&m_Fett,EDIT,     (double *)   &A_bas->a_bas.fett, VDOUBLE, 4,2));
	Form.Add (new CFormField (&m_Salz,EDIT,     (double *)   &A_bas_erw->a_bas_erw.salz, VDOUBLE, 4,2));
	Form.Add (new CFormField (&m_DavonFetts,EDIT,     (double *)   &A_bas_erw->a_bas_erw.davonfett, VDOUBLE, 4,2));
	Form.Add (new CFormField (&m_DavonZucker,EDIT,     (double *)   &A_bas_erw->a_bas_erw.davonzucker, VDOUBLE, 4,2));
	Form.Add (new CFormField (&m_Ballaststoff,EDIT,     (double *)   &A_bas_erw->a_bas_erw.ballaststoffe, VDOUBLE, 4,2));

	Form.Add (new CFormField (&m_Aerob,EDIT,     (short *)   A_bas->a_bas.aerob, VCHAR));
	Form.Add (new CFormField (&m_Coli,EDIT,     (short *)   A_bas->a_bas.coli, VCHAR));
	Form.Add (new CFormField (&m_Keime,EDIT,     (short *)   A_bas->a_bas.keime, VCHAR));
	Form.Add (new CFormField (&m_Listerien,EDIT,     (short *)   A_bas->a_bas.listerien, VCHAR));
	Form.Add (new CFormField (&m_ListMono,EDIT,     (short *)   A_bas->a_bas.list_mono, VCHAR));
	Form.Add (new CFormField (&m_GmoGvoStatus,EDIT,     (char *)   A_bas->a_bas.gmo_gvo, VCHAR));
	Form.Add (new CFormField (&m_ProdPassExtra,EDIT,     (char *)   A_bas->a_bas.prodpass_extra, VCHAR));

	FillHuelleCombo();



	m_Aerob.SetLimitText (sizeof (a_bas.aerob));
	m_Coli.SetLimitText (sizeof (a_bas.coli));
	m_Keime.SetLimitText (sizeof (a_bas.keime));
	m_Listerien.SetLimitText (sizeof (a_bas.listerien));
	m_ListMono.SetLimitText (sizeof (a_bas.list_mono));
	m_ProdPassExtra.SetLimitText (sizeof (a_bas.prodpass_extra));

// Liste f�r mikrobiologische Werte
	FillList = m_MibwList;
	FillList.SetStyle (LVS_REPORT);
	if (m_MibwList.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Bezeichnung"), 1, 200, LVCFMT_LEFT);
	FillList.SetCol (_T("Wert"), 2, 300, LVCFMT_LEFT);

// Liste f�r chemisch-physikalische Parameter
	CppList = m_CppList;
	CppList.SetStyle (LVS_REPORT);
	if (m_CppList.GridLines)
	{
		CppList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		CppList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	CppList.SetCol (_T(""), 0, 0);
	CppList.SetCol (_T("Bezeichnung"), 1, 200, LVCFMT_LEFT);
	CppList.SetCol (_T("Wert"), 2, 300, LVCFMT_LEFT);

// Liste f�r visuelle und organolep. Kriterien
	VikList = m_VikList;
	VikList.SetStyle (LVS_REPORT);
	if (m_VikList.GridLines)
	{
		VikList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		VikList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	VikList.SetCol (_T(""), 0, 0);
	VikList.SetCol (_T("Bezeichnung"), 1, 200, LVCFMT_LEFT);
	VikList.SetCol (_T("Wert"), 2, 300, LVCFMT_LEFT);

    CtrlGrid.Create (this, 40, 40);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LHuel  = new CCtrlInfo (&m_LHuel, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LHuel);
	CCtrlInfo *c_LHuelArt  = new CCtrlInfo (&m_LHuelArt, 2, 0, 1, 1); 
	CtrlGrid.Add (c_LHuelArt);
	CCtrlInfo *c_HuelArt  = new CCtrlInfo (&m_HuelArt, 3, 0, 2, 1); 
	CtrlGrid.Add (c_HuelArt);
	CCtrlInfo *c_LHuelMat  = new CCtrlInfo (&m_LHuelMat, 2, 1, 1, 1); 
	CtrlGrid.Add (c_LHuelMat);
	CCtrlInfo *c_HuelMat  = new CCtrlInfo (&m_HuelMat, 3, 1, 2, 1); 
	CtrlGrid.Add (c_HuelMat);
	CCtrlInfo *c_LHuelle  = new CCtrlInfo (&m_Huelle, 2, 2, 1, 1); 
	CtrlGrid.Add (c_LHuelle);
	CCtrlInfo *c_Huelle  = new CCtrlInfo (&m_CHuelle, 3, 2, 2, 1); 
	CtrlGrid.Add (c_Huelle);
	CCtrlInfo *c_LAuszArt  = new CCtrlInfo (&m_LAuszArt, 2, 3, 1, 1); 
	CtrlGrid.Add (c_LAuszArt);
	CCtrlInfo *c_AuszArt  = new CCtrlInfo (&m_AuszArt, 3, 3, 2, 1); 
	CtrlGrid.Add (c_AuszArt);
	CCtrlInfo *c_LEanPackung  = new CCtrlInfo (&m_LEanPackung, 2, 4, 1, 1); 
	CtrlGrid.Add (c_LEanPackung);
	CCtrlInfo *c_EanPackung  = new CCtrlInfo (&m_EanPackung, 3, 4, 2, 1); 
	CtrlGrid.Add (c_EanPackung);
	CCtrlInfo *c_LEanKarton  = new CCtrlInfo (&m_LEanKarton, 2, 5, 1, 1); 
	CtrlGrid.Add (c_LEanKarton);
	CCtrlInfo *c_EanKarton  = new CCtrlInfo (&m_EanKarton, 3, 5, 2, 1); 
	CtrlGrid.Add (c_EanKarton);
	CCtrlInfo *c_LPrAusz  = new CCtrlInfo (&m_LPrAusz, 2, 6, 1, 1); 
	CtrlGrid.Add (c_LPrAusz);
	CCtrlInfo *c_PrAusz  = new CCtrlInfo (&m_PrAusz, 3, 6, 2, 1); 
	CtrlGrid.Add (c_PrAusz);
	CCtrlInfo *c_Vakuumiert  = new CCtrlInfo (&m_Vakuumiert, 2, 7, 2, 1); 
	CtrlGrid.Add (c_Vakuumiert);
	CCtrlInfo *c_Schutzgas  = new CCtrlInfo (&m_Schutzgas, 2, 8, 2, 1); 
	CtrlGrid.Add (c_Schutzgas);
	CCtrlInfo *c_HuelPeel  = new CCtrlInfo (&m_HuelPeel, 3, 7, 2, 1); 
	CtrlGrid.Add (c_HuelPeel);
	CCtrlInfo *c_GruenerPunkt  = new CCtrlInfo (&m_GruenerPunkt, 3, 8, 2, 1); 
	CtrlGrid.Add (c_GruenerPunkt);

	CCtrlLine *Line2 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 9, DOCKSIZE, 1);
	CtrlGrid.Add (Line2);

	CCtrlInfo *c_LGebinde  = new CCtrlInfo (&m_LGebinde, 1, 10, 1, 1); 
	CtrlGrid.Add (c_LGebinde);

	CCtrlInfo *c_LGebindeArt  = new CCtrlInfo (&m_LGebindeArt, 2, 10, 1, 1); 
	CtrlGrid.Add (c_LGebindeArt);
	CCtrlInfo *c_GebindeArt  = new CCtrlInfo (&m_GebindeArt, 3, 10, 2, 1); 
	CtrlGrid.Add (c_GebindeArt);

	CCtrlInfo *c_LGebindeGroesse  = new CCtrlInfo (&m_LGebindeGroesse, 2, 11, 1, 1); 
	CtrlGrid.Add (c_LGebindeGroesse);
	CCtrlInfo *c_GebindeGroesse  = new CCtrlInfo (&m_GebindeGroesse, 3, 11, 2, 1); 
	CtrlGrid.Add (c_GebindeGroesse);

	CCtrlInfo *c_LGebindeLeerGew  = new CCtrlInfo (&m_LGebindeLeerGew, 2, 12, 1, 1); 
	CtrlGrid.Add (c_LGebindeLeerGew);
	CCtrlInfo *c_GebindeLeerGew  = new CCtrlInfo (&m_GebindeLeerGew, 3, 12, 1, 1); 
	CtrlGrid.Add (c_GebindeLeerGew);

	CCtrlInfo *c_LGebindeBruttoGew  = new CCtrlInfo (&m_LGebindeBruttoGew, 2, 13, 1, 1); 
	CtrlGrid.Add (c_LGebindeBruttoGew);
	CCtrlInfo *c_GebindeBruttoGew  = new CCtrlInfo (&m_GebindeBruttoGew, 3, 13, 1, 1); 
	CtrlGrid.Add (c_GebindeBruttoGew);

	CCtrlInfo *c_LGebindePack  = new CCtrlInfo (&m_LGebindePack, 2, 14, 1, 1); 
	CtrlGrid.Add (c_LGebindePack);
	CCtrlInfo *c_GebindePack  = new CCtrlInfo (&m_GebindePack, 3, 14, 1, 1); 
	CtrlGrid.Add (c_GebindePack);

	CCtrlInfo *c_LGebindeProLage  = new CCtrlInfo (&m_LGebindeProLage, 2, 15, 1, 1); 
	CtrlGrid.Add (c_LGebindeProLage);
	CCtrlInfo *c_GebindeProLage  = new CCtrlInfo (&m_GebindeProLage, 3, 15, 1, 1); 
	CtrlGrid.Add (c_GebindeProLage);

	CCtrlInfo *c_LGebindeAnzLagen  = new CCtrlInfo (&m_LGebindeAnzLagen, 2, 16, 1, 1); 
	CtrlGrid.Add (c_LGebindeAnzLagen);
	CCtrlInfo *c_GebindeAnzLagen  = new CCtrlInfo (&m_GebindeAnzLagen, 3, 16, 1, 1); 
	CtrlGrid.Add (c_GebindeAnzLagen);

	CCtrlInfo *c_LGebindePalette  = new CCtrlInfo (&m_LGebindePalette, 2, 17, 1, 1); 
	CtrlGrid.Add (c_LGebindePalette);
	CCtrlInfo *c_GebindePalette  = new CCtrlInfo (&m_GebindePalette, 3, 17, 1, 1); 
	CtrlGrid.Add (c_GebindePalette);

	CCtrlLine *Line3 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 18, DOCKSIZE, 1);
	CtrlGrid.Add (Line3);

	CCtrlInfo *c_LChemPhysParam  = new CCtrlInfo (&m_LChemPhysParam, 1, 19, 2, 1); 
	CtrlGrid.Add (c_LChemPhysParam);

	CCtrlInfo *c_CppList  = new CCtrlInfo (&m_CppList, 1, 20, 3, 5); 
//	c_CppList->BottomSpace = 50;
	CtrlGrid.Add (c_CppList);

	CCtrlInfo *c_LVik  = new CCtrlInfo (&m_LVik, 1, 26, 2, 1); 
	CtrlGrid.Add (c_LVik);

	CCtrlInfo *c_VikList  = new CCtrlInfo (&m_VikList, 1, 27, 3, DOCKBOTTOM); 
	c_VikList->BottomSpace = 20;
	CtrlGrid.Add (c_VikList);

	CCtrlInfo *c_LBeffe  = new CCtrlInfo (&m_LBeffe, 2, 20, 1, 1); 
	CtrlGrid.Add (c_LBeffe);
	CCtrlInfo *c_Beffe  = new CCtrlInfo (&m_Beffe, 3, 20, 1, 1); 
	CtrlGrid.Add (c_Beffe);

	CCtrlInfo *c_LBeffeFe  = new CCtrlInfo (&m_LBeffeFe, 2, 21, 1, 1); 
	CtrlGrid.Add (c_LBeffeFe);
	CCtrlInfo *c_BeffeFe  = new CCtrlInfo (&m_BeffeFe, 3, 21, 1, 1); 
	CtrlGrid.Add (c_BeffeFe);

	CCtrlLine *Line4 = new CCtrlLine (this, VERTICAL, 1, 1, 5, 0, 1, DOCKBOTTOM);
	CtrlGrid.Add (Line4);
	Line2->RightDockControl = Line4->cWnd;
	Line3->RightDockControl = Line4->cWnd;

	CCtrlInfo *c_LNaehrWert  = new CCtrlInfo (&m_LNaehrWert, 6, 0, 3, 1); 
	CtrlGrid.Add (c_LNaehrWert);
	CCtrlInfo *c_LNaehrWertText  = new CCtrlInfo (&m_LNaehrWertText, 6, 1, 3, 1); 
	CtrlGrid.Add (c_LNaehrWertText);
	CCtrlInfo *c_LBrennWert  = new CCtrlInfo (&m_LBrennWert, 7, 2, 1, 1); 
	CtrlGrid.Add (c_LBrennWert);
	CCtrlInfo *c_BrennWert  = new CCtrlInfo (&m_BrennWert, 8, 2, 1, 1); 
	CtrlGrid.Add (c_BrennWert);
	CCtrlInfo *c_LEiWeiss  = new CCtrlInfo (&m_LEiWeiss, 7, 3, 1, 1); 
	CtrlGrid.Add (c_LEiWeiss);
	CCtrlInfo *c_EiWeiss  = new CCtrlInfo (&m_EiWeiss, 8, 3, 1, 1); 
	CtrlGrid.Add (c_EiWeiss);
	CCtrlInfo *c_LKh  = new CCtrlInfo (&m_LKh, 7, 4, 1, 1); 
	CtrlGrid.Add (c_LKh);
	CCtrlInfo *c_Kh  = new CCtrlInfo (&m_Kh, 8, 4, 1, 1); 
	CtrlGrid.Add (c_Kh);
	CCtrlInfo *c_LDavonZucker  = new CCtrlInfo (&m_LDavonZucker, 7, 5, 1, 1); 
	CtrlGrid.Add (c_LDavonZucker);
	CCtrlInfo *c_DavonZucker  = new CCtrlInfo (&m_DavonZucker, 8, 5, 1, 1); 
	CtrlGrid.Add (c_DavonZucker);
	CCtrlInfo *c_LFett  = new CCtrlInfo (&m_LFett, 7, 6, 1, 1); 
	CtrlGrid.Add (c_LFett);
	CCtrlInfo *c_Fett  = new CCtrlInfo (&m_Fett, 8, 6, 1, 1); 
	CtrlGrid.Add (c_Fett);
	CCtrlInfo *c_LDavonFetts  = new CCtrlInfo (&m_LDavonFetts, 7, 7, 1, 1); 
	CtrlGrid.Add (c_LDavonFetts);
	CCtrlInfo *c_DavonFetts  = new CCtrlInfo (&m_DavonFetts, 8, 7, 1, 1); 
	CtrlGrid.Add (c_DavonFetts);
	CCtrlInfo *c_LSalz  = new CCtrlInfo (&m_LSalz, 7, 8, 1, 1); 
	CtrlGrid.Add (c_LSalz);
	CCtrlInfo *c_Salz  = new CCtrlInfo (&m_Salz, 8, 8, 1, 1); 
	CtrlGrid.Add (c_Salz);
	CCtrlInfo *c_LBallaststoff  = new CCtrlInfo (&m_LBallaststoff, 7, 9, 1, 1); 
	CtrlGrid.Add (c_LBallaststoff);
	CCtrlInfo *c_Ballaststoff  = new CCtrlInfo (&m_Ballaststoff, 8, 9, 1, 1); 
	CtrlGrid.Add (c_Ballaststoff);

	CCtrlLine *Line5 = new CCtrlLine (this, HORIZONTAL, 200, 1, 5, 10, DOCKRIGHT, 5);
	CtrlGrid.Add (Line5);

	CCtrlInfo *c_LMikro  = new CCtrlInfo (&m_LMikro, 6, 11, 3, 1); 
	CtrlGrid.Add (c_LMikro);

	m_LGmoGvoStatus.SetReadOnly (TRUE);
	m_LGmoGvoStatus.ModifyStyle (WS_TABSTOP, 0); 
	CCtrlInfo *c_MibwList  = new CCtrlInfo (&m_MibwList, 7, 12, DOCKRIGHT, 5); 
	c_MibwList->SetBottomControl (this, &m_LGmoGvoStatus, 25);
	c_MibwList->rightspace = 10;
	CtrlGrid.Add (c_MibwList);

	CCtrlInfo *c_LAerob  = new CCtrlInfo (&m_LAerob, 7, 12, 1, 1); 
	CtrlGrid.Add (c_LAerob);
	CCtrlInfo *c_Aerob  = new CCtrlInfo (&m_Aerob, 8, 12, 1, 1); 
	CtrlGrid.Add (c_Aerob);

	CCtrlInfo *c_LColi  = new CCtrlInfo (&m_LColi, 7, 13, 1, 1); 
	CtrlGrid.Add (c_LColi);
	CCtrlInfo *c_Coli  = new CCtrlInfo (&m_Coli, 8, 13, 1, 1); 
	CtrlGrid.Add (c_Coli);

	CCtrlInfo *c_LKeime  = new CCtrlInfo (&m_LKeime, 7, 14, 1, 1); 
	CtrlGrid.Add (c_LKeime);
	CCtrlInfo *c_Keime  = new CCtrlInfo (&m_Keime, 8, 14, 1, 1); 
	CtrlGrid.Add (c_Keime);

	CCtrlInfo *c_LListerien  = new CCtrlInfo (&m_LListerien, 7, 15, 1, 1); 
	CtrlGrid.Add (c_LListerien);
	CCtrlInfo *c_Listerien  = new CCtrlInfo (&m_Listerien, 8, 15, 1, 1); 
	CtrlGrid.Add (c_Listerien);

	CCtrlInfo *c_LListMono  = new CCtrlInfo (&m_LListMono, 7, 16, 1, 1); 
	CtrlGrid.Add (c_LListMono);
	CCtrlInfo *c_ListMono  = new CCtrlInfo (&m_ListMono, 8, 16, 1, 1); 
	CtrlGrid.Add (c_ListMono);

	CCtrlLine *Line6 = new CCtrlLine (this, HORIZONTAL, 200, 1, 5, 17, DOCKRIGHT, 1);
	CtrlGrid.Add (Line6);

/*
	m_LGmoGvoStatus.ShowWindow (SW_HIDE);
	m_GmoGvoStatus.ShowWindow (SW_HIDE);
*/
	m_LGmoGvoStatus.SetWindowText (_T("GMO/GVO"));
	CCtrlInfo *c_LGmoGvoStatus  = new CCtrlInfo (&m_LGmoGvoStatus, 6, 18, 2, 1); 
	CtrlGrid.Add (c_LGmoGvoStatus);
	CCtrlInfo *c_GmoGvoStatus  = new CCtrlInfo (&m_GmoGvoStatus, 7, 19, DOCKRIGHT, 5); 
	c_GmoGvoStatus->SetBottomControl (this, &m_LProdPassExtra, 10);
	c_GmoGvoStatus->rightspace = 10;
	CtrlGrid.Add (c_GmoGvoStatus);

	CCtrlInfo *c_LProdPassExtra  = new CCtrlInfo (&m_LProdPassExtra, 6, 22, 2, 1); 
	CtrlGrid.Add (c_LProdPassExtra);
	CCtrlInfo *c_ProdPassExtra  = new CCtrlInfo (&m_ProdPassExtra, 7, 23, DOCKRIGHT, DOCKBOTTOM); 
	c_ProdPassExtra->rightspace = 10;
	c_ProdPassExtra->BottomSpace = 50;
	CtrlGrid.Add (c_ProdPassExtra);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_LHuel.SetFont (&BoldFont);
	m_LGebinde.SetFont (&BoldFont);
	m_LChemPhysParam.SetFont (&BoldFont);
	m_LVik.SetFont (&BoldFont);
	m_LNaehrWert.SetFont (&BoldFont);
	m_LMikro.SetFont (&BoldFont);
	m_LProdPassExtra.SetFont (&BoldFont);
	m_LGmoGvoStatus.SetFont (&BoldFont);

    Read ();
	Form.Show ();
	CtrlGrid.Display ();
	return TRUE;
}

HBRUSH CProductPage2::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
		    if (pWnd == &m_LHuel)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LGebinde)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LChemPhysParam)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LVik)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LNaehrWert)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LMikro)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LProdPassExtra)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LGmoGvoStatus)
			{
				pDC->SetTextColor (BoldColor);
			}
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
    else if (pWnd == &m_LGmoGvoStatus)
	{
			pDC->SetTextColor (BoldColor);
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDbPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CProductPage2::OnSize(UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	GetWindowRect (&pRect);
	GetParent ()->ScreenToClient (&pRect);
	int pcx = pRect.right -pRect.left - tabx;
	int pcy = pRect.bottom - pRect.top - taby;
	if (cx > pcx)
	{
		pcx = cx;
	}
	if (cy > pcy)
	{
		pcy = cy;
	}
	MoveWindow (tabx, taby, pcx, pcy);
	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = pcx - 10 - tabx;
	frame.bottom = pcy - 10 - taby;
	frame.top += 20;
	frame.left += 2;
    frame.right -= 10;
	frame.bottom -= 10;
	CtrlGrid.Move (0, 0);
}


BOOL CProductPage2::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (ListActive (VK_RETURN))
				{
					return TRUE;
				}

				if (OnReturn ())
				{
					return TRUE;
				}
				return FALSE;
			}
			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}

				if (ListActive (0))
				{
					break;
				}

				if (OnKeydown ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (ListActive (0))
				{
					break;
				}

				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return FALSE;
}

void CProductPage2::UpdatePage ()
{
	Basis->Form.Get ();
	Read ();
	Form.Show ();
}

BOOL CProductPage2::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_ProdPassExtra) return FALSE;
	if (Control == &m_GmoGvoStatus) return FALSE;

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CProductPage2::OnKeydown ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_ProdPassExtra) return FALSE;
	if (Control == &m_GmoGvoStatus) return FALSE;

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CProductPage2::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_ProdPassExtra) return FALSE;
	if (Control == &m_GmoGvoStatus) return FALSE;

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

void CProductPage2::OnDelete ()
{
}

BOOL CProductPage2::OnKillActive ()
{
	Form.Get ();
	Write ();
	return TRUE;
}

BOOL CProductPage2::OnSetActive ()
{
	if (Basis != NULL)
	{
		Basis->Form.Get ();
	}
	DlgBkColor = Basis->DlgBkColor;
    DeleteObject (DlgBrush);
    DlgBrush = NULL;
//	InvalidateRect (NULL);
	Read ();
	Form.Show ();
	PostMessage (WM_SETFOCUS, (WPARAM) m_HuelArt.m_hWnd, 0l);
	return TRUE;
}

BOOL CProductPage2::Read ()
{

	if (!ProductPage2Read)
	{
		return FALSE;
	}

// Liste mikrobliolgische Werte
	m_MibwList.StopEnter ();
	m_MibwList.DeleteAllItems ();
	m_MibwList.vSelect.clear ();
	m_MibwList.ListRows.Init ();
	int i = 0;

	memcpy (&Prp_mibwerte.prp_mibwerte, &prp_mibwerte_null, sizeof (PRP_MIBWERTE));
	Prp_mibwerte.prp_mibwerte.a = A_bas->a_bas.a;
	Prp_mibwerte.dbreadfirst ();
	ToRecordNames (MibRecordNames, MibValues, MibNames);

	for (i = 0; i < MAXENTRIES; i ++)
	{
		if (MibNames[i] != "")
		{
			FillList.InsertItem (i, 0);
			FillList.SetItemText (MibNames[i].GetBuffer (), i, 1);
			CString value = MibValues[i];
			value.TrimRight ();
			if (value != "")
			{
				FillList.SetItemText (value.GetBuffer (), i, 2);
			}
			else
			{
				FillList.SetItemText (_T(""), i, 2);
			}
		}
	}
	if (m_MibwList.GetItemCount () == 0)
	{
		FillList.InsertItem (0, 0);
	}


// Liste chemisch-physikalische Parameter
	m_CppList.StopEnter ();
	m_CppList.DeleteAllItems ();
	m_CppList.vSelect.clear ();
	m_CppList.ListRows.Init ();

	memcpy (&Prp_cpp.prp_cpp, &prp_cpp_null, sizeof (PRP_CPP));
	Prp_cpp.prp_cpp.a = A_bas->a_bas.a;
	Prp_cpp.dbreadfirst ();
	ToRecordNames (CppRecordNames, CppValues, CppNames);

	for (i = 0; i < MAXENTRIES; i ++)
	{
		if (CppNames[i] != "")
		{
			CppList.InsertItem (i, 0);
			CppList.SetItemText (CppNames[i].GetBuffer (), i, 1);
			CString value = CppValues[i];
			value.TrimRight ();
			if (value != "")
			{
				CppList.SetItemText (value.GetBuffer (), i, 2);
			}
			else
			{
				CppList.SetItemText (_T(""), i, 2);
			}
		}
	}
	if (m_CppList.GetItemCount () == 0)
	{
		CppList.InsertItem (0, 0);
	}

// Liste visuelle und organolep. Kriterien
	m_VikList.StopEnter ();
	m_VikList.DeleteAllItems ();
	m_VikList.vSelect.clear ();
	m_VikList.ListRows.Init ();

	memcpy (&Prp_vik.prp_vik, &prp_vik_null, sizeof (PRP_VIK));
	Prp_vik.prp_vik.a = A_bas->a_bas.a;
	Prp_vik.dbreadfirst ();
	ToRecordNames (VikRecordNames, VikValues, VikNames);

	for (i = 0; i < MAXENTRIES; i ++)
	{
		if (VikNames[i] != "")
		{
			VikList.InsertItem (i, 0);
			VikList.SetItemText (VikNames[i].GetBuffer (), i, 1);
			CString value = VikValues[i];
			value.TrimRight ();
			if (value != "")
			{
				VikList.SetItemText (value.GetBuffer (), i, 2);
			}
			else
			{
				VikList.SetItemText (_T(""), i, 2);
			}
		}
	}
	if (m_VikList.GetItemCount () == 0)
	{
		VikList.InsertItem (0, 0);
	}

	Form.Show ();
	return TRUE;
}

void CProductPage2::FillNames (CString Names[], char *RecordNames [])
{
	for (int i = 0; i < MAXENTRIES; i ++)
	{
		Names[i] = RecordNames[i];
		Names[i].Trim ();
	}
}


void CProductPage2::ToRecordNames (char *RecordNames [], char *Values[], CString Names[])
{
	for (int i = 0; i < MAXENTRIES; i ++)
	{
		if (Names[i] != "")
		{
			strncpy (RecordNames[i], Names[i].GetBuffer (), 48);
		}
	}
}

void CProductPage2::FromRecordNames (char *RecordNames[], CString Names[])
{
	for (int i = 0; i < MAXENTRIES; i ++)
	{
		CString name = RecordNames[i];
		name.Trim ();
//		if (name != "")
		{
			Names[i] = name;
		}
	}
}

BOOL CProductPage2::Write ()
{
	extern short sql_mode;
	short sql_s;

	if (!ProductPage2Read)
	{
		return FALSE;
	}

	Form.Get ();
	sql_s = sql_mode;
	sql_mode = 1;

// Liste Mikrobiologische Werte
	m_MibwList.StopEnter ();
	int count = m_MibwList.GetItemCount ();
	memcpy (&Prp_mibwerte.prp_mibwerte, &prp_mibwerte_null, sizeof (PRP_MIBWERTE));
	Prp_mibwerte.prp_mibwerte.a  = A_bas->a_bas.a;
	for (int i = 0, j = 0; i < count; i ++)
	{
         CString Name;
		 Name = m_MibwList.GetItemText (i, 1);
		 Name.TrimRight ();
		 if (Name.GetLength () == 0)
		 {
			 strcpy (MibRecordNames[j], ""); 
			 continue;
		 }
		 strncpy (MibRecordNames[j], Name.GetBuffer (), 48); 
         CString Wert;
		 Wert = m_MibwList.GetItemText (i, 2);
		 Wert.TrimRight ();
		 strncpy (MibValues[j], Wert.GetBuffer (), 511); 
		 j ++;
	}
	Prp_mibwerte.dbupdate ();

	Prp_mibwerte.prp_mibwerte.a  = -1.0;
	FromRecordNames (CppRecordNames, CppNames);
	Prp_mibwerte.dbupdate ();

// Liste chemisch-physikalische Parameter
	m_CppList.StopEnter ();
	count = m_CppList.GetItemCount ();
	memcpy (&Prp_cpp.prp_cpp, &prp_cpp_null, sizeof (PRP_CPP));
	Prp_cpp.prp_cpp.a  = A_bas->a_bas.a;
	for (int i = 0, j = 0; i < count; i ++)
	{
         CString Name;
		 Name = m_CppList.GetItemText (i, 1);
		 Name.TrimRight ();
		 if (Name.GetLength () == 0)
		 {
			 strcpy (CppRecordNames[j], ""); 
			 continue;
		 }
		 strncpy (CppRecordNames[j], Name.GetBuffer (), 48); 
         CString Wert;
		 Wert = m_CppList.GetItemText (i, 2);
		 Wert.TrimRight ();
		 strncpy (CppValues[j], Wert.GetBuffer (), 511); 
		 j ++;
	}
	Prp_cpp.dbupdate ();

	Prp_cpp.prp_cpp.a  = -1.0;
	FromRecordNames (CppRecordNames, CppNames);
	Prp_cpp.dbupdate ();

// Liste visuelle und organolep. Kriterien 
	m_VikList.StopEnter ();
	count = m_VikList.GetItemCount ();
	memcpy (&Prp_vik.prp_vik, &prp_vik_null, sizeof (PRP_VIK));
	Prp_vik.prp_vik.a  = A_bas->a_bas.a;
	for (int i = 0, j = 0; i < count; i ++)
	{
         CString Name;
		 Name = m_VikList.GetItemText (i, 1);
		 Name.TrimRight ();
		 if (Name.GetLength () == 0)
		 {
			 strcpy (VikRecordNames[j], ""); 
			 continue;
		 }
		 strncpy (VikRecordNames[j], Name.GetBuffer (), 48); 
         CString Wert;
		 Wert = m_VikList.GetItemText (i, 2);
		 Wert.TrimRight ();
		 strncpy (VikValues[j], Wert.GetBuffer (), 511); 
		 j ++;
	}
	Prp_vik.dbupdate ();

	Prp_vik.prp_vik.a  = -1.0;
	FromRecordNames (VikRecordNames, VikNames);
	Prp_vik.dbupdate ();

	return TRUE;
}

BOOL CProductPage2::ListActive (WPARAM Key)
{
	if (GetFocus () == &m_MibwList ||
		GetFocus ()->GetParent () == &m_MibwList)
	{
		    if (Key != 0)
			{
				m_MibwList.OnKeyD (Key);
			}
			return TRUE;
	}
	if (GetFocus () == &m_CppList ||
		GetFocus ()->GetParent () == &m_CppList)
	{
		    if (Key != 0)
			{
				m_CppList.OnKeyD (Key);
			}
			return TRUE;
	}
	if (GetFocus () == &m_VikList ||
		GetFocus ()->GetParent () == &m_VikList)
	{
		    if (Key != 0)
			{
				m_VikList.OnKeyD (Key);
			}
			return TRUE;
	}
	return FALSE;
}

void CProductPage2::ReadCfg ()
{
    char cfg_v [256];

    if (Cfg.GetCfgValue ("UserFields", cfg_v) == TRUE)
    {
			UserFields = atoi (cfg_v);
    }
}

void CProductPage2::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CProductPage2::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}


void CProductPage2::FillHuelleCombo ()
{
	CFormField *f = Form.GetFormField (&m_CHuelle);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"huelle\" ")
				     _T("order by ptlfnr"));
        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
// FS-150 mit SetSel und dann Get wird der erste Wert in das Combofeld als initialisierung geschrieben normalerweise werden dann danach die Werte aus der Datgenbank geholt, da hier die Werte aber schon gelesen sind, d�rfen sie durch das Get nicht �berschrieben werden ! 
//		f->Get ();   
	}
}

void CProductPage2::OnStnClickedLdavonfetts()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}
