#ifndef _A_PROT_DEF
#define _A_PROT_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_PROT {
   short          delstatus;
   double         a;
   DATE_STRUCT    bearb;
   TCHAR          pers_nam[9];
   long           prima;
   TCHAR          dr_status[2];
};
extern struct A_PROT a_prot, a_prot_null;

#line 8 "A_prot.rh"

class A_PROT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_PROT a_prot;  
               A_PROT_CLASS () : DB_CLASS ()
               {
               }
               
               int dbreadfirst ();
               int dbread ();
               int dblock ();
               int dbupdate ();
               int dbdelete ();
               void dbclose ();
};
#endif
