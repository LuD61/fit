#ifndef _A_LGR_DEF
#define _A_LGR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_LGR {
   double         a;
   double         a_lgr_kap;
   short          delstatus;
   short          fil;
   long           lgr;
   short          lgr_pla_kz;
   short          lgr_typ;
   short          mdn;
   TCHAR          haupt_lgr[2];
   TCHAR          lgr_platz[11];
   double         min_bestand;
   double         meld_bestand;
   double         hoechst_bestand;
   long           inh_wanne;
   double         buch_artikel;
};
extern struct A_LGR a_lgr, a_lgr_null;

#line 8 "a_lgr.rh"

class A_LGR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_LGR a_lgr;
               A_LGR_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (A_LGR&);  
};
#endif
