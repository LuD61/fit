#include "StdAfx.h"
#include "ProductPassSheet.h"
#include "DbPropertyPage.h"

IMPLEMENT_DYNAMIC(CProductPassSheet, CDbPropertySheet)

CProductPassSheet::CProductPassSheet(void)
{
}

CProductPassSheet::~CProductPassSheet(void)
{
}

BOOL CProductPassSheet::Write ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	if (Page == NULL) return FALSE;
	BOOL ret = Page->Write ();
	if (ret == FALSE) return ret;
    SetActivePage (0);
	if (ret )
	{
		Page->AfterWrite ();
	}
	return ret;
}

void CProductPassSheet::StepBack ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	if (Page == NULL) return;
    SetActivePage (0);
	Page->StepBack ();
}

BOOL CProductPassSheet::Delete ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	if (Page == NULL) return FALSE;
	BOOL ret = Page->Delete ();
	if (ret)
	{
		SetActivePage (0);
	}
	return ret;
}
