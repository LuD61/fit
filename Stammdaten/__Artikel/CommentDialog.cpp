// CommentDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "PartyWa.h"
#include "CommentDialog.h"


// CCommentDialog-Dialogfeld

#define PARTYK CDatatables::GetInstance ()->GetPartyk ()->partyk

IMPLEMENT_DYNAMIC(CCommentDialog, CDialog)

CCommentDialog::CCommentDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CCommentDialog::IDD, pParent)
{

}

CCommentDialog::~CCommentDialog()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
}

void CCommentDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LCOMM_ANGEBOT, m_LCommAngebot);
	DDX_Control(pDX, IDC_COMM_ANGEBOT, m_CommAngebot);
	DDX_Control(pDX, IDC_LCOMM_RECHNUNG, m_LCommRechnung);
	DDX_Control(pDX, IDC_COMM_RECHNUNG, m_CommRechnung);
	DDX_Control(pDX, IDC_LCOMM_INTERN, m_LCommIntern);
	DDX_Control(pDX, IDC_COMM_INTERN, m_CommIntern);
}


BEGIN_MESSAGE_MAP(CCommentDialog, CDialog)
END_MESSAGE_MAP()


// CCommentDialog-Meldungshandler

BOOL CCommentDialog::OnInitDialog()
{
	CDialog::OnInitDialog ();

	Form.Add (new CFormField (&m_CommAngebot,EDIT,  (char *)   PARTYK.kommang, VCHAR));
	Form.Add (new CFormField (&m_CommRechnung,EDIT,  (char *)   PARTYK.kommrech, VCHAR));
	Form.Add (new CFormField (&m_CommIntern,EDIT,  (char *)   PARTYK.kommintern, VCHAR));
	m_CommAngebot.SetLimitText (255);
	m_CommRechnung.SetLimitText (255);
	m_CommIntern.SetLimitText (255);

    Form.Show (); 
	return TRUE;
}

BOOL CCommentDialog::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
			if (pMsg->wParam == VK_F12)
			{
				OnOK ();
				return TRUE;
			}
			break;
	}
	return CDialog::PreTranslateMessage (pMsg);
}

void CCommentDialog::OnOK ()
{
	Form.Get ();
	CDialog::OnOK ();
}

void CCommentDialog::OnCancel ()
{
	OnOK ();
}
