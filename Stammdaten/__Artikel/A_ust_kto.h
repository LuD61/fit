#ifndef _A_UST_KTO_DEF
#define _A_UST_KTO_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_UST_KTO {
   double         a;
   short          mdn;
   short          fil;
   long           erl_kto;
   short          mwst;
   long           skto_kto;
   long           we_kto;
};
extern struct A_UST_KTO a_ust_kto, a_ust_kto_null;

#line 8 "A_ust_kto.rh"

class A_UST_KTO_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_UST_KTO a_ust_kto;  
               A_UST_KTO_CLASS () : DB_CLASS ()
               {
               }
};
#endif
