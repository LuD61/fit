#pragma once
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "DbUniCode.h"
#include "FormTab.h"
#include "StaticButton.h"
#include "Mdn.h"
#include "Adr.h"
#include "A_bas.h"
#include "A_lgr.h"
#include "Lgr.h"
#include "ChoiceMdn.h"
#include "ChoiceLgr.h"
#include "ChoiceA.h"

#define IDC_LGRSAVE 3010
#define IDC_LGRDELETE 3011

#ifndef IDC_MDNCHOICE
#define IDC_MDNCHOICE 3001
#endif
#define IDC_LGRCHOICE 3002
#define IDC_ACHOICE 3000

// CLagerDialog-Dialogfeld

class CLagerDialog : public CDialog
{
	DECLARE_DYNAMIC(CLagerDialog)

public:
	CLagerDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CLagerDialog();

// Dialogfelddaten
	enum { IDD = IDD_LAGER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
	int FirstLgrCursor;
	int FirstLgrCursorMdn;
    MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
    A_LGR_CLASS A_lgr;
    LGR_CLASS Lgr;
    A_BAS_CLASS *A_bas;
    A_BAS_CLASS BuchA_bas;
	CChoiceMdn *ChoiceMdn;
	CChoiceLgr *ChoiceLgr;
	CChoiceA *ChoiceA;
 
	CStaticButton m_Save;
	CStaticButton m_Delete;
	CStatic m_LgrGroup;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	CEdit m_MdnName;
	CStatic m_LLgr;
	CNumEdit m_Lgr;
	CButton m_LgrChoice;
	CEdit m_LgrBz;
	CStatic m_LLgrPlatz;
	CNumEdit m_LgrPlatz;
	CStatic m_LMinBestand;
	CNumEdit m_MinBestand;
	CStatic m_LMeldBestand;
	CNumEdit m_MeldBestand;
	CStatic m_LHoechstBestand;
	CNumEdit m_HoechstBestand;
	CStatic m_LBuchArtikel;
	CNumEdit m_BuchArtikel;
	CEdit m_BuchBz1;
	CButton m_AChoice;

	CFormTab Form;
	CFormTab Keys;
	CFont Font;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid LgrGrid;
	CCtrlGrid BuchArtikelGrid;
	CCtrlGrid ToolGrid;
	virtual BOOL OnReturn ();
	virtual void OnSave ();
	virtual void Delete ();
	BOOL ReadMdn ();
	void FirstLager ();
	BOOL Read ();
    void OnMdnchoice(); 
    void OnLgrchoice(); 
    void OnAchoice(); 
public:
	afx_msg void OnEnKillfocusBuchArtikel();
};
