#include "stdafx.h"
#include "a_zut.h"

struct A_ZUT a_zut, a_zut_null, a_zut_def;

void A_ZUT_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_zut.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_zut.a,SQLDOUBLE,0);
    sqlout ((long *) &a_zut.bem_offs,SQLLONG,0);
    sqlout ((short *) &a_zut.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) a_zut.dkl_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_zut.ewg,SQLCHAR,7);
    sqlout ((short *) &a_zut.fil,SQLSHORT,0);
    sqlout ((TCHAR *) a_zut.lgr_bdg,SQLCHAR,49);
    sqlout ((short *) &a_zut.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) a_zut.verwend,SQLCHAR,49);
    sqlout ((TCHAR *) a_zut.zug_men_max,SQLCHAR,49);
            cursor = sqlcursor (_T("select a_zut.a,  ")
_T("a_zut.bem_offs,  a_zut.delstatus,  a_zut.dkl_kz,  a_zut.ewg,  a_zut.fil,  ")
_T("a_zut.lgr_bdg,  a_zut.mdn,  a_zut.verwend,  a_zut.zug_men_max from a_zut ")

#line 12 "a_zut.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_zut.a,SQLDOUBLE,0);
    sqlin ((long *) &a_zut.bem_offs,SQLLONG,0);
    sqlin ((short *) &a_zut.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) a_zut.dkl_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_zut.ewg,SQLCHAR,7);
    sqlin ((short *) &a_zut.fil,SQLSHORT,0);
    sqlin ((TCHAR *) a_zut.lgr_bdg,SQLCHAR,49);
    sqlin ((short *) &a_zut.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) a_zut.verwend,SQLCHAR,49);
    sqlin ((TCHAR *) a_zut.zug_men_max,SQLCHAR,49);
            sqltext = _T("update a_zut set a_zut.a = ?,  ")
_T("a_zut.bem_offs = ?,  a_zut.delstatus = ?,  a_zut.dkl_kz = ?,  ")
_T("a_zut.ewg = ?,  a_zut.fil = ?,  a_zut.lgr_bdg = ?,  a_zut.mdn = ?,  ")
_T("a_zut.verwend = ?,  a_zut.zug_men_max = ? ")

#line 14 "a_zut.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_zut.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_zut.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_zut ")
                                  _T("where a = ? for update"));
            sqlin ((double *)   &a_zut.a,  SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_zut ")
                                  _T("set delstatus = 0 where a = ? and delstatus = 0 for update"));
            sqlin ((double *)   &a_zut.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_zut ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_zut.a,SQLDOUBLE,0);
    sqlin ((long *) &a_zut.bem_offs,SQLLONG,0);
    sqlin ((short *) &a_zut.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) a_zut.dkl_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_zut.ewg,SQLCHAR,7);
    sqlin ((short *) &a_zut.fil,SQLSHORT,0);
    sqlin ((TCHAR *) a_zut.lgr_bdg,SQLCHAR,49);
    sqlin ((short *) &a_zut.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) a_zut.verwend,SQLCHAR,49);
    sqlin ((TCHAR *) a_zut.zug_men_max,SQLCHAR,49);
            ins_cursor = sqlcursor (_T("insert into a_zut (a,  ")
_T("bem_offs,  delstatus,  dkl_kz,  ewg,  fil,  lgr_bdg,  mdn,  verwend,  zug_men_max) ")

#line 28 "a_zut.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?)"));

#line 30 "a_zut.rpp"
}
