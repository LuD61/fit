// MoreTxt.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "MoreTxt.h"
#include ".\moretxt.h"
#include "Process.h"
#include "StrFuncs.h"


// CMoreTxt-Dialogfeld

IMPLEMENT_DYNAMIC(CMoreTxt, CDialog)
CMoreTxt::CMoreTxt(CWnd* pParent /*=NULL*/)
	: CDialog(CMoreTxt::IDD, pParent)
{
	ChoiceAtxt = NULL;
	ActiveEdit = NULL;
}

CMoreTxt::~CMoreTxt()
{
	if (ChoiceAtxt != NULL)
	{
		delete ChoiceAtxt;
		ChoiceAtxt = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
}

void CMoreTxt::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TXT_NR1,  m_TxtNr1);
	DDX_Control(pDX, IDC_TXT_NR2,  m_TxtNr2);
	DDX_Control(pDX, IDC_TXT_NR3,  m_TxtNr3);
	DDX_Control(pDX, IDC_TXT_NR4,  m_TxtNr4);
	DDX_Control(pDX, IDC_TXT_NR5,  m_TxtNr5);
	DDX_Control(pDX, IDC_TXT_NR6,  m_TxtNr6);
	DDX_Control(pDX, IDC_TXT_NR7,  m_TxtNr7);
	DDX_Control(pDX, IDC_TXT_NR8,  m_TxtNr8);
	DDX_Control(pDX, IDC_TXT_NR9,  m_TxtNr9);
	DDX_Control(pDX, IDC_TXT_NR10, m_TxtNr10);
	DDX_Control(pDX, IDC_ENTERTXT ,m_Enter);
}


BEGIN_MESSAGE_MAP(CMoreTxt, CDialog)
	ON_BN_CLICKED(IDC_BUTXT1, OnBnClickedButxt1)
	ON_BN_CLICKED(IDC_BUTXT2, OnBnClickedButxt2)
	ON_BN_CLICKED(IDC_BUTXT3, OnBnClickedButxt3)
	ON_BN_CLICKED(IDC_BUTXT4, OnBnClickedButxt4)
	ON_BN_CLICKED(IDC_BUTXT5, OnBnClickedButxt5)
	ON_BN_CLICKED(IDC_BUTXT6, OnBnClickedButxt6)
	ON_BN_CLICKED(IDC_BUTXT7, OnBnClickedButxt7)
	ON_BN_CLICKED(IDC_BUTXT8, OnBnClickedButxt8)
	ON_BN_CLICKED(IDC_BUTXT9, OnBnClickedButxt9)
	ON_BN_CLICKED(IDC_BUTXT10, OnBnClickedButxt10)
	ON_COMMAND(IDC_ENTERTXT, OnEnterTxt)
	ON_EN_SETFOCUS(IDC_TXT_NR1,  OnEnSetfocusTxtNr1)
	ON_EN_SETFOCUS(IDC_TXT_NR2,  OnEnSetfocusTxtNr2)
	ON_EN_SETFOCUS(IDC_TXT_NR3,  OnEnSetfocusTxtNr3)
	ON_EN_SETFOCUS(IDC_TXT_NR4,  OnEnSetfocusTxtNr4)
	ON_EN_SETFOCUS(IDC_TXT_NR5,  OnEnSetfocusTxtNr5)
	ON_EN_SETFOCUS(IDC_TXT_NR6,  OnEnSetfocusTxtNr6)
	ON_EN_SETFOCUS(IDC_TXT_NR7,  OnEnSetfocusTxtNr7)
	ON_EN_SETFOCUS(IDC_TXT_NR8,  OnEnSetfocusTxtNr8)
	ON_EN_SETFOCUS(IDC_TXT_NR9,  OnEnSetfocusTxtNr9)
	ON_EN_SETFOCUS(IDC_TXT_NR10, OnEnSetfocusTxtNr10)
END_MESSAGE_MAP()


// CMoreTxt-Meldungshandler


BOOL CMoreTxt::OnInitDialog ()
{
	BOOL ret = CDialog::OnInitDialog ();

	m_Enter.nID = IDC_ENTERTXT;
	m_Enter.SetWindowText (_T("Bearbeiten"));
    m_Enter.ShowWindow (SW_SHOWNORMAL);

	Form.Add (new CFormField (&m_TxtNr1,EDIT, (long *) &A_bas.a_bas.txt_nr1, VLONG));
	Form.Add (new CFormField (&m_TxtNr2,EDIT, (long *) &A_bas.a_bas.txt_nr2, VLONG));
	Form.Add (new CFormField (&m_TxtNr3,EDIT, (long *) &A_bas.a_bas.txt_nr3, VLONG));
	Form.Add (new CFormField (&m_TxtNr4,EDIT, (long *) &A_bas.a_bas.txt_nr4, VLONG));
	Form.Add (new CFormField (&m_TxtNr5,EDIT, (long *) &A_bas.a_bas.txt_nr5, VLONG));
	Form.Add (new CFormField (&m_TxtNr6,EDIT, (long *) &A_bas.a_bas.txt_nr6, VLONG));
	Form.Add (new CFormField (&m_TxtNr7,EDIT, (long *) &A_bas.a_bas.txt_nr7, VLONG));
	Form.Add (new CFormField (&m_TxtNr8,EDIT, (long *) &A_bas.a_bas.txt_nr8, VLONG));
	Form.Add (new CFormField (&m_TxtNr9,EDIT, (long *) &A_bas.a_bas.txt_nr9, VLONG));
	Form.Add (new CFormField (&m_TxtNr10,EDIT, (long *) &A_bas.a_bas.txt_nr10, VLONG));
	Form.Show ();
//	LOGFONT l;
    CFont *Font = new CFont ();
	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font->CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font->CreatePointFont (95, _T("Dlg"));
	}
	m_Enter.SetFont (Font);
    return TRUE;
}

BOOL CMoreTxt::PreTranslateMessage(MSG* pMsg)
{
//	CWnd *cWnd = NULL;
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
/*
			if (pMsg->wParam == VK_RETURN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
*/
			if (pMsg->wParam == VK_DOWN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				OnOK ();
				return TRUE;
			}
	}
	return CDialog::PreTranslateMessage (pMsg);
}

void CMoreTxt::OnOK ()
{
	Form.Get ();
	CDialog::OnOK ();
}

void CMoreTxt::OnTxtchoice (long *TxtNr)
{
	Form.Get ();
	if (ChoiceAtxt == NULL)
	{
		ChoiceAtxt = new CChoiceAtxt (this);
	    ChoiceAtxt->IsModal = TRUE;
	    ChoiceAtxt->HideEnter = FALSE;

//		ChoiceAtxt->DlgBkColor = ListBkColor;
		ChoiceAtxt->IdArrDown = IDI_HARROWDOWN;
		ChoiceAtxt->IdArrUp   = IDI_HARROWUP;
		ChoiceAtxt->IdArrNo   = IDI_HARROWNO;
		ChoiceAtxt->CreateDlg ();
	}

    ChoiceAtxt->SetDbClass (&A_bas);
	ChoiceAtxt->SearchText = _T("");
	ChoiceAtxt->DoModal();

    if (ChoiceAtxt->GetState ())
    {
		  CATextList *abl = ChoiceAtxt->GetSelectedText (); 
		  if (abl == NULL) return;
		  *TxtNr = abl->txt_nr;
		  Form.Show ();
    }
	delete ChoiceAtxt;
	ChoiceAtxt = NULL;
}

void CMoreTxt::OnBnClickedButxt1()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnTxtchoice (&A_bas.a_bas.txt_nr1);
}

void CMoreTxt::OnBnClickedButxt2()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnTxtchoice (&A_bas.a_bas.txt_nr2);
}
void CMoreTxt::OnBnClickedButxt3()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnTxtchoice (&A_bas.a_bas.txt_nr3);
}
void CMoreTxt::OnBnClickedButxt4()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnTxtchoice (&A_bas.a_bas.txt_nr4);
}
void CMoreTxt::OnBnClickedButxt5()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnTxtchoice (&A_bas.a_bas.txt_nr5);
}
void CMoreTxt::OnBnClickedButxt6()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnTxtchoice (&A_bas.a_bas.txt_nr6);
}
void CMoreTxt::OnBnClickedButxt7()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnTxtchoice (&A_bas.a_bas.txt_nr7);
}
void CMoreTxt::OnBnClickedButxt8()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnTxtchoice (&A_bas.a_bas.txt_nr8);
}
void CMoreTxt::OnBnClickedButxt9()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnTxtchoice (&A_bas.a_bas.txt_nr9);
}
void CMoreTxt::OnBnClickedButxt10()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnTxtchoice (&A_bas.a_bas.txt_nr10);
}

void CMoreTxt::OnEnterTxt()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	CProcess p;

/*
	if (ActiveEdit != NULL)
	{
		CString Message;

		CString cTextNr; 
		ActiveEdit->GetWindowText (cTextNr);
		long txt_nr = _tstol (cTextNr.GetBuffer ());
		Message.Format (_T("TextNr=%ld"), txt_nr);
		CStrFuncs::ToClipboard (Message);
	}
*/

	CString Command = _T("atexte");

	if (ActiveEdit != NULL)
	{
		CString Message;

		CString cTextNr; 
		ActiveEdit->GetWindowText (cTextNr);
		long txt_nr = _tstol (cTextNr.GetBuffer ());
		Message.Format (_T("TextNr=%ld"), txt_nr);
		Command += _T(" ");
		Command += Message;
	}

	p.SetCommand (Command);
	HANDLE Pid = p.Start ();
}

void CMoreTxt::OnEnSetfocusTxtNr1()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ActiveEdit = &m_TxtNr1;
}

void CMoreTxt::OnEnSetfocusTxtNr2()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ActiveEdit = &m_TxtNr2;
}

void CMoreTxt::OnEnSetfocusTxtNr3()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ActiveEdit = &m_TxtNr3;
}

void CMoreTxt::OnEnSetfocusTxtNr4()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ActiveEdit = &m_TxtNr4;
}

void CMoreTxt::OnEnSetfocusTxtNr5()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ActiveEdit = &m_TxtNr5;
}

void CMoreTxt::OnEnSetfocusTxtNr6()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ActiveEdit = &m_TxtNr6;
}

void CMoreTxt::OnEnSetfocusTxtNr7()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ActiveEdit = &m_TxtNr7;
}

void CMoreTxt::OnEnSetfocusTxtNr8()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ActiveEdit = &m_TxtNr8;
}

void CMoreTxt::OnEnSetfocusTxtNr9()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ActiveEdit = &m_TxtNr9;
}

void CMoreTxt::OnEnSetfocusTxtNr10()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	ActiveEdit = &m_TxtNr10;
}
