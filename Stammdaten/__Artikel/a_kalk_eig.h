#ifndef _A_KALK_EIG_DEF
#define _A_KALK_EIG_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_KALK_EIG {
   double         a;
   short          bearb_fil;
   short          bearb_lad;
   short          bearb_sk;
   short          delstatus;
   short          fil;
   double         fil_ek_teilk;
   double         fil_ek_vollk;
   double         hk_teilk;
   double         hk_vollk;
   double         kost;
   double         lad_vk_teilk;
   double         lad_vk_vollk;
   double         mat_o_b;
   short          mdn;
   double         sk_teilk;
   double         sk_vollk;
   double         sp_fil;
   double         sp_lad;
   double         sp_vk;
   DATE_STRUCT    dat;
   DATE_STRUCT    aend_dat;
};
extern struct A_KALK_EIG a_kalk_eig, a_kalk_eig_null;

#line 8 "a_kalk_eig.rh"

class A_KALK_EIG_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_KALK_EIG a_kalk_eig;  
               A_KALK_EIG_CLASS () : DB_CLASS ()
               {
               }
};
#endif
