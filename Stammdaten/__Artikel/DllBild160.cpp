#include "StdAfx.h"
#include ".\dllbild160.h"

HANDLE CDllBild160::Bild160Lib = NULL;

CDllBild160::CDllBild160(void)
{
	bild160 = NULL;
	OpenDatabase = NULL;
	char *bws;

	bws = getenv ("BWS");

	CString Bild160;
	Bild160 = "bild160dll.dll";
	Bild160Lib = LoadLibrary (Bild160.GetBuffer ());
	if (Bild160Lib != NULL)
	{
		bild160 = (int (*) (LPSTR)) 
					GetProcAddress ((HMODULE) Bild160Lib, "bild160");
		OpenDatabase = (void (*) ()) 
					GetProcAddress ((HMODULE) Bild160Lib, "OpenDatabase");
	}
	if (OpenDatabase != NULL)
	{
		(*OpenDatabase) ();
	}
}

CDllBild160::~CDllBild160(void)
{
}

int CDllBild160::Send (LPSTR Command)
{
	if (Bild160Lib == NULL) return -1;
	if (bild160 == NULL) return -1;
	return (*bild160) (Command);
}
