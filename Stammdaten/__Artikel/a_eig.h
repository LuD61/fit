#ifndef _A_EIG_DEF
#define _A_EIG_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_EIG {
   double         a;
   long           a_krz;
   short          anz_theke_eti;
   long           bem_offs;
   short          delstatus;
   short          fil;
   double         gew_bto;
   double         inh;
   long           mat;
   short          mdn;
   short          me_einh_ek;
   TCHAR          pr_ausz[2];
   TCHAR          pr_man[2];
   TCHAR          pr_ueb[2];
   TCHAR          rez[9];
   short          sg1;
   short          sg2;
   double         tara;
   TCHAR          theke_eti[2];
   TCHAR          verk_art[2];
   TCHAR          mwst_ueb[2];
   DATE_STRUCT    verk_beg;
   short          me_einh_abverk;
   double         inh_abverk;
   TCHAR          hnd_gew_abverk[2];
   TCHAR          reg_eti[2];
   short          anz_reg_eti;
   TCHAR          waren_eti[2];
   short          anz_waren_eti;
};
extern struct A_EIG a_eig, a_eig_null;

#line 8 "a_eig.rh"

class A_EIG_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_EIG a_eig;  
               A_EIG_CLASS () : DB_CLASS ()
               {
               }
};
#endif
