#pragma once
#include "afxcmn.h"
#include "a_bas.h"
#include "a_emb.h"
#include "a_ean.h"

class CEmbTreeCtrl :
	public CTreeCtrl
{
public:
	int UntEmbCursor;
	HTREEITEM ArtItem;
	A_BAS_CLASS *A_bas;
	A_EAN_CLASS *A_ean;
	A_EMB_CLASS *A_emb;
	CEmbTreeCtrl(void);
	~CEmbTreeCtrl(void);
	void Init ();
    HTREEITEM AddRootItem (LPSTR, int, int);
    HTREEITEM AddChildItem (HTREEITEM, LPSTR, int, int, int , DWORD);
    void SetTreeStyle (DWORD);
	BOOL Read ();
	BOOL ReadATree ();
	BOOL ReadEmbTreeVertical (HTREEITEM RootItem);
	BOOL ReadEanTree ();
	BOOL ReadEanEmbTree (int idx);
	void ReadEmbTree (HTREEITEM ChildItem);
};
