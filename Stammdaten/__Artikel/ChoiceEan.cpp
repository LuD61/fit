#include "stdafx.h"
#include "ChoiceEan.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceEan::Sort1 = -1;
int CChoiceEan::Sort2 = -1;
int CChoiceEan::Sort3 = -1;
int CChoiceEan::Sort4 = -1;
int CChoiceEan::Sort5 = -1;

CChoiceEan::CChoiceEan(CWnd* pParent) 
        : CChoiceX(pParent)
{
	Where = "";
	Bean.ArchiveName = _T("EanList.prp");
}

CChoiceEan::~CChoiceEan() 
{
	DestroyList ();
}

void CChoiceEan::DestroyList() 
{
	for (std::vector<CEanList *>::iterator pabl = EanList.begin (); pabl != EanList.end (); ++pabl)
	{
		CEanList *abl = *pabl;
		delete abl;
	}
    EanList.clear ();
}

void CChoiceEan::FillList () 
{
    double  ean;
    TCHAR ean_bz [50];
    double a;
    TCHAR a_bz1 [50];

	DestroyList ();
	ClearList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl �ber EAN-Nummern"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("EAN-Nr"),              1, 150, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),         2, 250);
    SetCol (_T("Artikel-Nr"),          3, 150, LVCFMT_RIGHT);
    SetCol (_T("Artikelbezeichnung"),  4, 250);
	SortRow = 1;

	if (EanList.size () == 0)
	{
		DbClass->sqlout ((double *) &ean,        SQLDOUBLE, 0);
		DbClass->sqlout ((LPTSTR)  ean_bz,      SQLCHAR, sizeof (ean_bz));
		DbClass->sqlout ((double *) &a,         SQLDOUBLE, 0);
		DbClass->sqlout ((LPTSTR)  a_bz1,       SQLCHAR, sizeof (a_bz1));

		CString Sql = _T("select a_ean.ean, a_ean.ean_bz, a_bas.a, a_bas.a_bz1 "
					     "from a_ean,a_bas where ean > 0 and a_bas.a = a_ean.a");
		Sql += " ";
		Sql += Where;

		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
/*
		int cursor = DbClass->sqlcursor (_T("select a_ean.ean, a_ean.ean_bz, a_bas.a, a_bas.a_bz1 "
						    "from a_ean,a_bas where ean > 0 and a_bas.a = a_ean.a"));
*/
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) ean_bz;
			CDbUniCode::DbToUniCode (ean_bz, pos);
			pos = (LPSTR) a_bz1;
			CDbUniCode::DbToUniCode (a_bz1, pos);
			CEanList *abl = new CEanList (ean, ean_bz, a, a_bz1);
			EanList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
		Load ();
	}

	for (std::vector<CEanList *>::iterator pabl = EanList.begin (); pabl != EanList.end (); ++pabl)
	{
		CEanList *abl = *pabl;
		CString Ean;
		Ean.Format (_T("%.0lf"), abl->ean); 
		CString Num;
		CString Bez;
		_tcscpy (ean_bz, abl->ean_bz.GetBuffer ());
		_tcscpy (a_bz1,  abl->a_bz1.GetBuffer ());
		CString Art;
		Art.Format (_T("%.0lf"), abl->a); 

		CString LText;
		LText.Format (_T("%.0lf %s"), abl->ean, 
									  abl->ean_bz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Ean;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (ean_bz, i, 2);
        ret = SetItemText (Art.GetBuffer (), i, 3);
        ret = SetItemText (a_bz1, i, 4);
        i ++;
    }

    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort (listView);
}


void CChoiceEan::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceEan::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceEan::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceEan::SearchEanBz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceEan::SearchA (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceEan::SearchABz1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 4);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceEan::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
	if ((EditText.Find (_T("*")) != -1) || EditText.Find (_T("?")) != -1)
	{
		CChoiceX::SearchMatch (EditText);
		return;
	}
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (13));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchABz1 (ListBox, EditText.GetBuffer ());
             break;
        case 3 :
             SearchA (ListBox, EditText.GetBuffer (13));
             break;
        case 4 :
             EditText.MakeUpper ();
             SearchABz1 (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceEan::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceEan::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort4;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort4);
	   }
	   return 0;
   }
   else if (SortRow == 4)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort5;
   }
   return 0;
}


void CChoiceEan::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
        case 4:
              Sort5 *= -1;
			  if (Sort5 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CEanList *abl = EanList [i];
		   
		   abl->ean = _tstof (ListBox->GetItemText (i, 1));
		   abl->ean_bz = ListBox->GetItemText (i, 2);
		   abl->a  = _tstof (ListBox->GetItemText (i, 3));
		   abl->a_bz1 = ListBox->GetItemText (i, 4);
	}
	for (int i = 1; i <= 9; i ++)
	{
		SetItemSort (i, 2);
	}
	SetItemSort (SortRow, SortPos);
}

void CChoiceEan::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = EanList [idx];
}

CEanList *CChoiceEan::GetSelectedText ()
{
	CEanList *abl = (CEanList *) SelectedRow;
	return abl;
}

void CChoiceEan::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (EanList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

void CChoiceEan::SendSelect ()
{
	pParent->SendMessage (WM_COMMAND, EANSELECTED, 0l);
}

void CChoiceEan::SendCancel ()
{
	pParent->SendMessage (WM_COMMAND, EANCANCELED, 0l);
}

void CChoiceEan::SetDefault ()
{
	CChoiceX::SetDefault ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    ListBox->SetColumnWidth (0, 0);
    ListBox->SetColumnWidth (1, 150);
    ListBox->SetColumnWidth (2, 250);
    ListBox->SetColumnWidth (3, 150);
    ListBox->SetColumnWidth (4, 250);
}
