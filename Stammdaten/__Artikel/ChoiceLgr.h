#ifndef _CHOICELGR_DEF
#define _CHOICELGR_DEF

#include "ChoiceX.h"
#include "LgrList.h"
#include "A_lgr.h"
#include <vector>

class CChoiceLgr : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;

		static BOOL LagerImageCreated;
		static int LagerImagePos;
private:
		A_LGR_CLASS A_lgr;
		int a_lgr_cursor;

    public :
		CString Where;
		double m_Artikel;
	    std::vector<CLgrList *> LgrList;
	    std::vector<CLgrList *> SelectList;
        CChoiceLgr(CWnd* pParent = NULL);   // Standardkonstruktor
        ~CChoiceLgr();
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchName (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CLgrList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		void SaveSelection (CListCtrl *ListBox);
		virtual void SetImages ();
		virtual void SetHeaderImages ();
		virtual void AddHeaderImageList ();
};
#endif
