#include "stdafx.h"
#include "a_huel.h"

struct A_HUEL a_huel, a_huel_null, a_huel_def;

void A_HUEL_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_huel.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_huel.a,SQLDOUBLE,0);
    sqlout ((long *) &a_huel.bem_offs,SQLLONG,0);
    sqlout ((short *) &a_huel.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_huel.farbe,SQLSHORT,0);
    sqlout ((short *) &a_huel.fil,SQLSHORT,0);
    sqlout ((double *) &a_huel.fuel_gew,SQLDOUBLE,0);
    sqlout ((long *) &a_huel.fuel_vol,SQLLONG,0);
    sqlout ((TCHAR *) a_huel.huel_txt,SQLCHAR,49);
    sqlout ((TCHAR *) a_huel.kal,SQLCHAR,9);
    sqlout ((short *) &a_huel.mdn,SQLSHORT,0);
    sqlout ((double *) &a_huel.sw,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_huel.tmpr_max,SQLCHAR,9);
            cursor = sqlcursor (_T("select a_huel.a,  ")
_T("a_huel.bem_offs,  a_huel.delstatus,  a_huel.farbe,  a_huel.fil,  ")
_T("a_huel.fuel_gew,  a_huel.fuel_vol,  a_huel.huel_txt,  a_huel.kal,  ")
_T("a_huel.mdn,  a_huel.sw,  a_huel.tmpr_max from a_huel ")

#line 12 "a_huel.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_huel.a,SQLDOUBLE,0);
    sqlin ((long *) &a_huel.bem_offs,SQLLONG,0);
    sqlin ((short *) &a_huel.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_huel.farbe,SQLSHORT,0);
    sqlin ((short *) &a_huel.fil,SQLSHORT,0);
    sqlin ((double *) &a_huel.fuel_gew,SQLDOUBLE,0);
    sqlin ((long *) &a_huel.fuel_vol,SQLLONG,0);
    sqlin ((TCHAR *) a_huel.huel_txt,SQLCHAR,49);
    sqlin ((TCHAR *) a_huel.kal,SQLCHAR,9);
    sqlin ((short *) &a_huel.mdn,SQLSHORT,0);
    sqlin ((double *) &a_huel.sw,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_huel.tmpr_max,SQLCHAR,9);
            sqltext = _T("update a_huel set a_huel.a = ?,  ")
_T("a_huel.bem_offs = ?,  a_huel.delstatus = ?,  a_huel.farbe = ?,  ")
_T("a_huel.fil = ?,  a_huel.fuel_gew = ?,  a_huel.fuel_vol = ?,  ")
_T("a_huel.huel_txt = ?,  a_huel.kal = ?,  a_huel.mdn = ?,  a_huel.sw = ?,  ")
_T("a_huel.tmpr_max = ? ")

#line 14 "a_huel.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_huel.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_huel.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_huel ")
                                  _T("where a = ? for update"));
            sqlin ((double *)   &a_huel.a,  SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_huel ")
                                  _T("set delstatus = 0 where a = ? and delstatus = 0 for update"));
            sqlin ((double *)   &a_huel.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_huel ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_huel.a,SQLDOUBLE,0);
    sqlin ((long *) &a_huel.bem_offs,SQLLONG,0);
    sqlin ((short *) &a_huel.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_huel.farbe,SQLSHORT,0);
    sqlin ((short *) &a_huel.fil,SQLSHORT,0);
    sqlin ((double *) &a_huel.fuel_gew,SQLDOUBLE,0);
    sqlin ((long *) &a_huel.fuel_vol,SQLLONG,0);
    sqlin ((TCHAR *) a_huel.huel_txt,SQLCHAR,49);
    sqlin ((TCHAR *) a_huel.kal,SQLCHAR,9);
    sqlin ((short *) &a_huel.mdn,SQLSHORT,0);
    sqlin ((double *) &a_huel.sw,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_huel.tmpr_max,SQLCHAR,9);
            ins_cursor = sqlcursor (_T("insert into a_huel (")
_T("a,  bem_offs,  delstatus,  farbe,  fil,  fuel_gew,  fuel_vol,  huel_txt,  kal,  mdn,  sw,  ")
_T("tmpr_max) ")

#line 28 "a_huel.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?)"));

#line 30 "a_huel.rpp"
}
