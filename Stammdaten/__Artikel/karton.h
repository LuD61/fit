#ifndef _KARTON_DEF
#define _KARTON_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct KARTON {
   short          id;
   TCHAR          bz[65];
   double         laenge;
   double         hoehe;
   double         breite;
   double         vol;
   double         tara;
   double         inh;
};
extern struct KARTON karton, karton_null;

#line 8 "karton.rh"

class KARTON_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               KARTON karton;
               KARTON_CLASS () : DB_CLASS ()
               {
               }
};
#endif
