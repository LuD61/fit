// SendToSystem.cpp : Implementierungsdatei
//

#include "stdafx.h"
//#include "ATexte.h"
#include "SendToSystem.h"
#include "ChoiceSys.h"
#include "sendtosystem.h"
#include "UniFormField.h"
#include ".\sendtosystem.h"


// CSendToSystem-Dialogfeld

IMPLEMENT_DYNAMIC(CSendToSystem, CDialog)
CSendToSystem::CSendToSystem(CWnd* pParent /*=NULL*/)
	: CDialog(CSendToSystem::IDD, pParent)
{
}

CSendToSystem::~CSendToSystem()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
}

void CSendToSystem::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SYS, m_Sys);
	DDX_Control(pDX, IDC_ADR_KRZ, m_AdrKrz);
}


BEGIN_MESSAGE_MAP(CSendToSystem, CDialog)
	ON_BN_CLICKED(IDC_SYSCH, OnBnClickedSyschoice)
	ON_BN_CLICKED(IDC_SEND, OnBnClickedSend)
END_MESSAGE_MAP()


// CSendToSystem-Meldungshandler

BOOL CSendToSystem::OnInitDialog ()
{
	CDialog::OnInitDialog ();
	memcpy (&Sys_peri.sys_peri, &sys_peri_null, sizeof (SYS_PERI));
	memcpy (&Adr.adr, &adr_null, sizeof (ADR));
    Form.Add (new CFormField (&m_Sys,EDIT,   (long *)  &Sys_peri.sys_peri.sys, VLONG));
    Form.Add (new CUniFormField (&m_AdrKrz,EDIT,   (char *)  &Adr.adr.adr_krz, VCHAR));
	Form.Show ();
	return TRUE;
}

void CSendToSystem::OnBnClickedSyschoice()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
    CString Text;
	CChoiceSys SysChoice;
    SysChoice.IsModal = TRUE;
	SysChoice.IdArrDown = IDI_HARROWDOWN;
	SysChoice.IdArrUp   = IDI_HARROWUP;
	SysChoice.IdArrNo   = IDI_HARROWNO;
	SysChoice.HideOK       = FALSE;
	SysChoice.CreateDlg ();

    SysChoice.SetDbClass (&Sys_peri);
	SysChoice.SingleSelection = FALSE;
	SysChoice.DoModal();
    if (SysChoice.GetState ())
    {
		  CSysList *abl;
		  if (SysChoice.SelectList.size () > 1)
		  {
				for (std::vector<CSysList *>::iterator pabl = SysChoice.SelectList.begin (); pabl != SysChoice.SelectList.end (); ++pabl)
				{
					abl = *pabl;
					Sys_peri.sys_peri.sys = abl->sys; 
					Form.Show ();
					ReadSys ();
					Form.Show ();
					UpdateWindow ();
					SendSys (Sys_peri.sys_peri.sys);
				}
	   	  }
		  else
		  {
			abl = SysChoice.GetSelectedText (); 
			if (abl == NULL) return;
		  }
	      Sys_peri.sys_peri.sys = abl->sys; 
          Form.Show ();
		  ReadSys ();
		  Form.Show ();
		  m_Sys.SetFocus ();
    }
}

void CSendToSystem::OnBnClickedSend()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (!ReadSys ())
	{
		AfxMessageBox (_T("System nicht angelegt"), MB_OK | MB_ICONERROR);
	}
	else
	{
		Form.Show ();
		m_Sys.SetFocus ();
		m_Sys.SetSel (0, -1);
		SendSys (Sys_peri.sys_peri.sys);
	}
}

void CSendToSystem::SendSys (long sys)
{
	char Command[80];
	sprintf (Command, "bild160 s %ld 13 0 0 0 0", sys);
	Bild160.Send (Command);
	Sleep (500);
}

void CSendToSystem::OnOK ()
{
	if (!ReadSys ())
	{
		AfxMessageBox (_T("System nicht angelegt"), MB_OK | MB_ICONERROR);
	}
	else
	{
		Form.Show ();
	}
	m_Sys.SetFocus ();
	m_Sys.SetSel (0, -1);
}

BOOL CSendToSystem::ReadSys ()
{
    memcpy (&Sys_peri.sys_peri, &sys_peri_null, sizeof (SYS_PERI)); 
    memcpy (&Fil.fil, &fil_null, sizeof (FIL)); 
    memcpy (&Adr.adr, &adr_null, sizeof (ADR)); 
	Form.Get ();
    int dsqlstatus = Sys_peri.dbreadfirst ();
    if (dsqlstatus == 0)
    {
		  Fil.fil.mdn = Sys_peri.sys_peri.mdn;
		  Fil.fil.fil = Sys_peri.sys_peri.fil;
		  dsqlstatus = Fil.dbreadfirst ();
		  if (dsqlstatus == 0)
		  {
				 Adr.adr.adr = Fil.fil.adr;
				 dsqlstatus = Adr.dbreadfirst ();
		  }
    }
	else
	{
		return FALSE;
	}
	return TRUE;
}

