#pragma once
#include <stdio.h>
#include "vector.h"
#include "propertyitem.h"
#include "Token.h"

namespace PropExtension
{

class CProperties :
	public CVector
{
public:
	int AllTabMode ;	// 160109 : Alltabmode ist OHNE Section und schluessel in 2.tem Wert 
	CString FileName;
	CString SectionName;
	CProperties(void);
	~CProperties(void);
    CString GetValue (CString Name);
	CString GetValues (CString Name);
    CString GetName (CString Value);
	int Find (CString Name);
	int FindValue (CString Value);
	void Set (CString Name, CString Value, int idx);
	void Set (CString& Record);
	virtual void SetWithSep (CString& Record, LPTSTR Sep);
	void SetWithSep (CString& Record, LPTSTR Sep1, LPTSTR Sep2);
	BOOL FindSection (FILE *fp);
	void Load ();
	void SetAllTabMode( int) ;	// 160109 : Ueberschreibung zur Automatic ohne Testsection
								// ( Zuordnung Schluessel<->Wert) ist moeglich
};
}
