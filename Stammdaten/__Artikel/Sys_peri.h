#ifndef _SYS_PERI_DEF
#define _SYS_PERI_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct SYS_PERI {
   short          anschluss;
   short          anz_waehl_wied;
   short          auszeit;
   short          baudrate;
   DATE_STRUCT    dat_abruf;
   DATE_STRUCT    dat_update;
   short          daten_bit;
   long           drv_pid;
   short          fil;
   short          kanal;
   short          mdn;
   TCHAR          modem_typ[3];
   TCHAR          parity[2];
   TCHAR          peri_nam[21];
   short          peri_typ;
   short          port_typ;
   short          protokoll;
   short          retry_max;
   short          status_abruf;
   short          stop_bit;
   long           sys;
   short          sys_abruf;
   short          sys_update;
   TCHAR          tel[17];
   TCHAR          term[13];
   TCHAR          txt[61];
   TCHAR          zeit_abruf[7];
   short          stat;
   TCHAR          b_check[2];
   short          la_zahl;
};
extern struct SYS_PERI sys_peri, sys_peri_null;

#line 8 "sys_peri.rh"

class SYS_PERI_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SYS_PERI sys_peri;  
               SYS_PERI_CLASS () : DB_CLASS ()
               {
               }
};
#endif
