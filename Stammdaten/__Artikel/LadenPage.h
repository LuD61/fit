#pragma once
#include "afxwin.h"
#include "BasisdatenPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGridColor.h"
#include "FillList.h"
#include "mdn.h"
#include "adr.h"
#include "a_bas.h"
#include "a_bas_erw.h"
#include "a_hndw.h"
#include "a_eig.h"
#include "a_pfa.h"
#include "a_leer.h"
#include "a_krz.h"
#include "ag.h"
#include "a_ean.h"
#include "a_emb.h"
#include "ptabn.h"
#include "DbUniCode.h"
#include "FormTab.h"
#include "StaticButton.h"
#include "ptabn.h"
#include "DbUniCode.h"
#include "ChoiceAtxt.h"
#include "afxcmn.h"

#define IDC_ACHOICE 3000
#define IDC_TXTCHOICE1 3001
#define IDC_TXTCHOICE2 3002
#define IDC_TXTCHOICE3 3003
#define IDC_TXTCHOICE4 3004
// #define IDC_MORETXT 3005
// #define IDC_ENTERTXT 3006

// CLadenPage-Dialogfeld

class CLadenPage : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CLadenPage)

public:
	BOOL HideButtons;
	CWnd *Frame;
	CLadenPage();
	virtual ~CLadenPage();

// Dialogfelddaten
	enum { IDD = IDD_LADENPAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()

public:
    enum
	{
		NoKrz = 0,
		Auto = 1,
		Hand = 2,
	};

	CChoiceAtxt *ChoiceAtxt;
	int TestAkrzCursor;
	int dcs_a_par;
	int waa_a_par;
	long MaxAkrzKasse;
	long MaxAkrzWaa;
	CBasisdatenPage *Basis;
	MDN_CLASS *Mdn;
	ADR_CLASS *MdnAdr;
	SYS_PAR_CLASS *Sys_par;
	A_BAS_CLASS *A_bas;
	A_HNDW_CLASS *A_hndw;
	A_EIG_CLASS *A_eig;
	A_PFA_CLASS *A_pfa;
	A_LEER_CLASS *A_leer;
	A_KRZ_CLASS *A_krz;
	AG_CLASS *Ag;
	A_BAS_ERW_CLASS *A_bas_erw;
	A_EAN_CLASS A_ean;
	A_EMB_CLASS A_emb;
	PTABN_CLASS Ptabn;
	CFont Font;
	CFont lFont;
	CFormTab HeadForm;
	CFormTab Form;
	CFormTab DisableEigs;
	CFormTab DisablePfas;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid AGrid;
	CCtrlGrid LadenGrid;
	CCtrlGrid HbkGrid;
	CCtrlGrid EtiGrid;
	CCtrlGrid GewGrid;
	CCtrlGrid TxtGrid;
	CCtrlGrid Txt1Grid;
	CCtrlGrid Txt2Grid;
	CCtrlGrid Txt3Grid;
	CCtrlGrid Txt4Grid;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CTextEdit m_MdnName;
	CStatic m_LA;
	CNumEdit m_A;
	CColorButton m_AChoice;
	CTextEdit m_A_bz1;
	CTextEdit m_A_bz2;
	CStatic m_LdGroup;
	CStatic m_LVerkArt;
	CComboBox m_VerkArt;
	CButton m_MwstUeb;
	CButton m_PrAusz;
	CButton m_PrMan;
	CButton m_FilKtrl;
	virtual void UpdatePage ();
	virtual void GetPage ();
    void FillPtabCombo (CWnd *Control, LPTSTR Item, BOOL blang);
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	void OnAchoice ();
	CStatic m_EtiGroup;
	CStatic m_LReg_eti;
	CComboBox m_Reg_eti;
	CComboBox m_Waren_eti;
	CComboBox m_Theke_eti;
	CStatic m_LA_krz;
	CNumEdit m_A_krz;
	void EnableTyp ();
	CButton m_Pr_ueb;
	CStatic m_LSg1;
	CNumEdit m_Sg1;
	CStatic m_LSg2;
	CNumEdit m_Sg2;
	CStatic m_LTara;
	CNumEdit m_Tara;
	CStatic m_LGew_bto;
	CNumEdit m_Gew_bto;
	CStatic m_LA_gew;
	CNumEdit m_A_gew;
	CStatic m_GewGroup;
	void TestAkrz ();
	void TestAkrzKasse ();
	void TestAkrzWaage ();
	BOOL EanEmbExist ();
	afx_msg void OnCbnKillfocusVerlArt();
	afx_msg void OnCbnCloseupVerlArt();
    void GenAkrz ();
	CStatic m_LAllgText1;
	CNumEdit m_AllgText1;
	CButton m_AllgTextChoice1;
	CStatic m_LAllgText2;
	CNumEdit m_AllgText2;
	CButton m_AllgTextChoice2;
	CStatic m_LAllgText3;
	CNumEdit m_AllgText3;
	CButton m_AllgTextChoice3;
	CStatic m_LAllgText4;
	CNumEdit m_AllgText4;
	CButton m_AllgTextChoice4;
	CStatic m_TxtGroup;
	CStaticButton m_DruckeRegEti;
	CStaticButton m_DruckeThekeEti;
	CStaticButton m_MoreTxt;
	CStaticButton m_EnterTxt;
	void OnAtxtchoice (long *TextNr);
	void OnAtxt1choice ();
	void OnAtxt2choice ();
	void OnAtxt3choice ();
	void OnAtxt4choice ();
	void OnDlgBackground();
	void OnMoreTxt ();
	void OnEnterTxt ();
	void PrintRegEti ();
	void PrintThekeEti ();
	void OnCopy ();
	void OnPaste ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
	virtual void OnDelete ();
	CStatic m_LAnzRegEti;
	CEdit m_AnzRegEti;
	CStatic m_LAnzWarenEti;
	CEdit m_AnzWarenEti;
	CStatic m_LAnzThekeEti;
	CEdit m_AnzThekeEti;
	CSpinButtonCtrl m_SpinRegEti;
	CSpinButtonCtrl m_SpinWarenEti;
	CSpinButtonCtrl m_SpinThekeEti;
	CStatic m_LVkGr;
	CComboBox m_VkGr;
	CStatic m_LHbkZtr;
	CNumEdit m_HbkZtr;
	CStatic m_LGnPktGeb;
	CNumEdit m_GnPktGeb;
	CSpinButtonCtrl m_SpinHbk;
	CComboBox m_HbkKz;
	CStatic m_LWarenEti;
	CStatic m_LThekenEti;
	CStatic m_LTara2;
	CStatic m_LTara3;
	CStatic m_LTara4;
	CNumEdit m_Tara2;
	CNumEdit m_Tara3;
	CNumEdit m_Tara4;
	CComboBox m_TaraMat2;
	CComboBox m_TaraMat3;
	CComboBox m_TaraMat4;
	afx_msg void OnEnKillfocusTara2();
	afx_msg void OnEnKillfocusTara3();
	afx_msg void OnEnKillfocusTara4();
	afx_msg void OnEnKillfocusTara();
	afx_msg void OnEnKillfocusHbkZtr();
};
