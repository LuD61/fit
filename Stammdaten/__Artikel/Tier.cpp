#include "stdafx.h"
#include "tier.h"

struct TIER tier, tier_null;

void TIER_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &tier.tier,  SQLSHORT, 0);
    sqlout ((short *) &tier.tier,SQLSHORT,0);
    sqlout ((TCHAR *) tier.tier_bz,SQLCHAR,21);
    sqlout ((short *) &tier.tier_gatt,SQLSHORT,0);
    sqlout ((short *) &tier.delstatus,SQLSHORT,0);
    sqlout ((double *) &tier.pr_neu,SQLDOUBLE,0);
    sqlout ((double *) &tier.pr_alt,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select tier.tier,  ")
_T("tier.tier_bz,  tier.tier_gatt,  tier.delstatus,  tier.pr_neu,  ")
_T("tier.pr_alt from tier ")

#line 12 "Tier.rpp"
                                  _T("where tier = ?"));
    sqlin ((short *) &tier.tier,SQLSHORT,0);
    sqlin ((TCHAR *) tier.tier_bz,SQLCHAR,21);
    sqlin ((short *) &tier.tier_gatt,SQLSHORT,0);
    sqlin ((short *) &tier.delstatus,SQLSHORT,0);
    sqlin ((double *) &tier.pr_neu,SQLDOUBLE,0);
    sqlin ((double *) &tier.pr_alt,SQLDOUBLE,0);
            sqltext = _T("update tier set tier.tier = ?,  ")
_T("tier.tier_bz = ?,  tier.tier_gatt = ?,  tier.delstatus = ?,  ")
_T("tier.pr_neu = ?,  tier.pr_alt = ? ")

#line 14 "Tier.rpp"
                                  _T("where tier = ?");
            sqlin ((short *)   &tier.tier,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &tier.tier,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select tier from tier ")
                                  _T("where tier = ?"));
            sqlin ((short *)   &tier.tier,  SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from tier ")
                                  _T("where tier = ?"));
    sqlin ((short *) &tier.tier,SQLSHORT,0);
    sqlin ((TCHAR *) tier.tier_bz,SQLCHAR,21);
    sqlin ((short *) &tier.tier_gatt,SQLSHORT,0);
    sqlin ((short *) &tier.delstatus,SQLSHORT,0);
    sqlin ((double *) &tier.pr_neu,SQLDOUBLE,0);
    sqlin ((double *) &tier.pr_alt,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into tier (")
_T("tier,  tier_bz,  tier_gatt,  delstatus,  pr_neu,  pr_alt) ")

#line 25 "Tier.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?)"));

#line 27 "Tier.rpp"
}
