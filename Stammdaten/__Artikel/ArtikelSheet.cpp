#include "StdAfx.h"
#include "Artikelsheet.h"
#include "DbPropertyPage.h"
#include "PageHandler.h"

IMPLEMENT_DYNAMIC(CArtikelSheet, CDbPropertySheet)

CArtikelSheet::CArtikelSheet(void)
{
	CPageHandler::GetInstance ()->RegisterSheet (this);
}

CArtikelSheet::~CArtikelSheet(void)
{
}

BOOL CArtikelSheet::Write ()
{
//	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	if (Page == NULL) return FALSE;
    CPageHandler::GetInstance ()->SetActivePage (0);
	BOOL ret = Page->Write ();
	if (ret == FALSE) return ret;
	if (ret )
	{
		Page->AfterWrite (); 
	}
	return ret;
}

void CArtikelSheet::StepBack ()
{
//	CDbPropertyPage *Page = (CDbPropertyPage *) GetActivePage ();
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	if (Page == NULL) return;
    CPageHandler::GetInstance ()->SetActivePage (0);
	Page->StepBack ();
}

BOOL CArtikelSheet::Delete ()
{
//    CDbPropertyPage *ActPage = (CDbPropertyPage *) GetActivePage ();
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	if (Page == NULL) return FALSE;
//	SetActivePage (0);
	BOOL ret = Page->Delete ();
	if (ret)
	{
		SetActivePage (0);
//		SetActivePage (ActPage);
	}
	return ret;
}
