#include "StdAfx.h"
#include ".\embtreectrl.h"
#include <vector>

class EmbTreeEntry
{
public :
	double emb;
    HTREEITEM Item;
	EmbTreeEntry (double emb, HTREEITEM Item)
	{
		this->emb = emb;
		this->Item = Item;
	}
};


CEmbTreeCtrl::CEmbTreeCtrl(void)
{
	A_bas = NULL;
	A_ean = NULL;
	A_emb = NULL;
	UntEmbCursor = -1;
}

CEmbTreeCtrl::~CEmbTreeCtrl(void)
{
}

void CEmbTreeCtrl::Init ()
{
	A_emb->sqlin ((double *) &A_emb->a_emb.unt_emb, SQLDOUBLE, 0);
	A_emb->sqlout ((double *) &A_emb->a_emb.emb, SQLDOUBLE, 0);
	UntEmbCursor = A_emb->sqlcursor (_T("select emb from a_emb where unt_emb = ?"));
    SetTreeStyle (TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS);
	CString Text;
	Text.Format (_T("Artikel %.0lf"), A_bas->a_bas.a);
	HTREEITEM RootItem = AddRootItem (Text.GetBuffer (), 0, 5);
}

HTREEITEM CEmbTreeCtrl::AddRootItem (LPSTR Text, int idx, int Childs)
{
   TV_INSERTSTRUCT TvItem;

   TvItem.hParent     = TVI_ROOT;
   TvItem.hInsertAfter = TVI_LAST;

   TvItem.item.mask = TVIF_CHILDREN |  TVIF_CHILDREN | TVIF_HANDLE |
                      TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE |
                      TVIF_STATE | TVIF_TEXT;
   TvItem.item.hItem = NULL;
   TvItem.item.state      = TVIS_STATEIMAGEMASK | TVIS_EXPANDED; 
   TvItem.item.stateMask  = TVIS_STATEIMAGEMASK | TVIS_EXPANDED; 
   TvItem.item.pszText    = Text;
   TvItem.item.cchTextMax = (int) strlen (Text);      
   TvItem.item.iImage     = 0;          
   TvItem.item.iSelectedImage = 0;  
   TvItem.item.cChildren = Childs;       
   TvItem.item.lParam     = (LPARAM) idx;

   return InsertItem (&TvItem);
}

HTREEITEM CEmbTreeCtrl::AddChildItem (HTREEITEM hParent,  LPSTR Text, int idx, int Childs, 
                                   int iImage, DWORD state)
{
   TV_INSERTSTRUCT TvItem;

   TvItem.hParent     = hParent;
   TvItem.hInsertAfter = TVI_LAST;

   TvItem.item.mask = TVIF_CHILDREN | TVIF_HANDLE |
                      TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE |
                      TVIF_STATE | TVIF_TEXT;
   TvItem.item.hItem = NULL;
   TvItem.item.state      = state; 
   TvItem.item.stateMask  = state; 
   TvItem.item.pszText    = Text;
   TvItem.item.cchTextMax = (int) strlen (Text);      
   TvItem.item.iImage     = iImage;
   TvItem.item.iSelectedImage = iImage;  
   TvItem.item.cChildren = Childs;       
   TvItem.item.lParam     = (LPARAM) idx;

   return InsertItem (&TvItem);
}

void CEmbTreeCtrl::SetTreeStyle (DWORD style)
{
    DWORD oldStyle = GetWindowLong (m_hWnd, GWL_STYLE);

    SetWindowLong (m_hWnd, GWL_STYLE, oldStyle | style);
}

BOOL CEmbTreeCtrl::Read ()
{
	DeleteAllItems ();
    SetTreeStyle (TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS);

	ReadATree ();
	ReadEanTree ();
    SelectItem (ArtItem);
    return TRUE;
}

BOOL CEmbTreeCtrl::ReadATree ()
{
	std::vector<EmbTreeEntry *> EmbTab;
	CString Text;
	Text.Format (_T("Artikel %.0lf"), A_bas->a_bas.a);
	ArtItem = AddRootItem (Text.GetBuffer (), 0, 5);
	HTREEITEM ChildItem = ArtItem;
	A_emb->a_emb.unt_emb = A_bas->a_bas.a;
	A_emb->sqlopen (UntEmbCursor);
	while (A_emb->sqlfetch (UntEmbCursor) == 0)
	{
		if (A_emb->dbreadfirst () != 0) continue;
		Text.Format (_T("%.0lf %s"), A_emb->a_emb.emb, A_emb->a_emb.emb_bz);
		ChildItem = AddChildItem (ArtItem,  Text.GetBuffer (), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED);
		EmbTreeEntry *ete = new EmbTreeEntry (A_emb->a_emb.emb, ChildItem);
		EmbTab.push_back (ete);
	}
	for (std::vector<EmbTreeEntry *>::iterator e = EmbTab.begin (); e != EmbTab.end (); ++e)
	{
		EmbTreeEntry *ete = *e;
		A_emb->a_emb.unt_emb = ete->emb;
		ReadEmbTree (ete->Item);
		delete ete;
	}
	EmbTab.clear ();
	return TRUE;
}

void CEmbTreeCtrl::ReadEmbTree (HTREEITEM ArtItem)
{
	std::vector<EmbTreeEntry *> EmbTab;
	CString Text;
	A_emb->sqlopen (UntEmbCursor);
	while (A_emb->sqlfetch (UntEmbCursor) == 0)
	{
		if (A_emb->dbreadfirst () != 0) continue;
		Text.Format (_T("%.0lf %s"), A_emb->a_emb.emb, A_emb->a_emb.emb_bz);
		HTREEITEM ChildItem = AddChildItem (ArtItem,  Text.GetBuffer (), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED);
		EmbTreeEntry *ete = new EmbTreeEntry (A_emb->a_emb.emb, ChildItem);
		EmbTab.push_back (ete);
	}
	for (std::vector<EmbTreeEntry *>::iterator e = EmbTab.begin (); e != EmbTab.end (); ++e)
	{
		EmbTreeEntry *ete = *e;
		A_emb->a_emb.unt_emb = ete->emb;
		ReadEmbTree (ete->Item);
		delete ete;
	}
	EmbTab.clear ();
}

BOOL CEmbTreeCtrl::ReadEanTree ()
{
	A_ean->a_ean.a = A_bas->a_bas.a;
	int dsqlstatus = A_ean->dbreadafirst ();
	int idx = 1;
	while (dsqlstatus == 0)
	{
		ReadEanEmbTree (idx ++);
		dsqlstatus = A_ean->dbreada ();
	}
	return TRUE;
}

BOOL CEmbTreeCtrl::ReadEanEmbTree (int idx)
{
	std::vector<EmbTreeEntry *> EmbTab;
	CString Text;
	Text.Format (_T("EAN %.0lf"), A_ean->a_ean.ean);
	HTREEITEM RootItem = AddRootItem (Text.GetBuffer (), idx, 5);
	HTREEITEM ChildItem = RootItem;
	A_emb->a_emb.unt_emb = A_ean->a_ean.ean;
	while (A_emb->dbreadfirst_unt_emb () == 0)
	{
		Text.Format (_T("%.0lf %s"), A_emb->a_emb.emb, A_emb->a_emb.emb_bz);
		ChildItem = AddChildItem (ChildItem,  Text.GetBuffer (), 
			                               0, 1, 
										   0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED);
		A_emb->a_emb.unt_emb = A_emb->a_emb.emb;
	}
	A_emb->sqlopen (UntEmbCursor);
	while (A_emb->sqlfetch (UntEmbCursor) == 0)
	{
		if (A_emb->dbreadfirst () != 0) continue;
		Text.Format (_T("%.0lf %s"), A_emb->a_emb.emb, A_emb->a_emb.emb_bz);
		ChildItem = AddChildItem (RootItem,  Text.GetBuffer (), 
	                          0, 1, 
	 					      0, TVIS_STATEIMAGEMASK | TVIS_EXPANDED);
		EmbTreeEntry *ete = new EmbTreeEntry (A_emb->a_emb.emb, ChildItem);
		EmbTab.push_back (ete);
	}
	for (std::vector<EmbTreeEntry *>::iterator e = EmbTab.begin (); e != EmbTab.end (); ++e)
	{
		EmbTreeEntry *ete = *e;
		A_emb->a_emb.unt_emb = ete->emb;
		ReadEmbTree (ete->Item);
		delete ete;
	}
	EmbTab.clear ();
	return TRUE;
}
