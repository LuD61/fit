#pragma once
#include "DbPropertySheet.h"
#include "DbPropertyPage.h"

class CPageHandler
{
private:
	static CPageHandler *Instance;
protected:
	CPageHandler(void);
	~CPageHandler(void);
	CDbPropertySheet *m_Sheet;
	CDbPropertyPage *m_Page;
	CWnd *m_ActiveControl;
public:
	static CPageHandler *GetInstance ();
	static void DestroyInstance ();
	void RegisterSheet (CDbPropertySheet *Sheet);
	void RegisterPage (CDbPropertyPage *Page);
	void RegisterPage ();
	void SetPage (BOOL UnRegister=TRUE);
	BOOL CanSetPage ();
	BOOL SetActivePage (int Page);
	BOOL SetActivePage (CDbPropertyPage *Page);
};
