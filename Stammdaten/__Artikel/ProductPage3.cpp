// ProductPage3.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "ProductPage3.h"
#include "UniFormField.h"
#include "CtrlLine.h"


// CProductPage3-Dialogfeld

IMPLEMENT_DYNAMIC(CProductPage3, CDbPropertyPage)

CProductPage3::CProductPage3()
	: CDbPropertyPage(CProductPage3::IDD)
{
	ProductPage3Read = FALSE;
//	BoldColor = RGB (0,0,255);
	BoldColor = RGB (0,128,192);
}

CProductPage3::~CProductPage3()
{
}

void CProductPage3::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_AUB1, m_Aub1);
	DDX_Control(pDX, IDC_AUB2, m_Aub2);
	DDX_Control(pDX, IDC_LGLG1, m_LGlg1);
	DDX_Control(pDX, IDC_LGLG2, m_LGlg2);
	DDX_Control(pDX, IDC_GLG, m_Glg);
	DDX_Control(pDX, IDC_LKREBS1, m_LKrebs1);
	DDX_Control(pDX, IDC_LKREBS2, m_LKrebs2);
	DDX_Control(pDX, IDC_KREBS, m_Krebs);
	DDX_Control(pDX, IDC_LEI1, m_LEi1);
	DDX_Control(pDX, IDC_LEI2, m_LEi2);
	DDX_Control(pDX, IDC_EI, m_Ei);
	DDX_Control(pDX, IDC_LFISCH1, m_LFisch1);
	DDX_Control(pDX, IDC_LFISCH2, m_LFisch2);
	DDX_Control(pDX, IDC_FISCH, m_Fisch);
	DDX_Control(pDX, IDC_LERDNUSS1, m_LErdnuss1);
	DDX_Control(pDX, IDC_LERDNUSS2, m_LErdnuss2);
	DDX_Control(pDX, IDC_ERDNUSS, m_Erdnuss);
	DDX_Control(pDX, IDC_LSOJA1, m_LSoja1);
	DDX_Control(pDX, IDC_LSOJA2, m_LSoja2);
	DDX_Control(pDX, IDC_SOJA, m_Soja);
	DDX_Control(pDX, IDC_LMILCH1, m_LMilch1);
	DDX_Control(pDX, IDC_LMILCH2, m_LMilch2);
	DDX_Control(pDX, IDC_MILCH, m_Milch);
	DDX_Control(pDX, IDC_LSCHAL1, m_LSchal1);
	DDX_Control(pDX, IDC_LSCHAL2, m_LSchal2);
	DDX_Control(pDX, IDC_SCHAL, m_Schal);
	DDX_Control(pDX, IDC_LSELLERIE1, m_LSellerie1);
	DDX_Control(pDX, IDC_LSELLERIE2, m_LSellerie2);
	DDX_Control(pDX, IDC_SELLERIE, m_Sellerie);
	DDX_Control(pDX, IDC_LSENFSAAT1, m_LSenfSaat1);
	DDX_Control(pDX, IDC_LSENFSAAT2, m_LSenfSaat2);
	DDX_Control(pDX, IDC_SENFSAAT, m_SenfSaat);
	DDX_Control(pDX, IDC_LSESAMSAMEN1, m_LSesamSamen1);
	DDX_Control(pDX, IDC_LSESAMSAMEN2, m_LSesamSamen2);
	DDX_Control(pDX, IDC_SESAMSAMEN, m_SesamSamen);
	DDX_Control(pDX, IDC_LSULFIT, m_LSulfit);
	DDX_Control(pDX, IDC_SULFIT, m_Sulfit);
	DDX_Control(pDX, IDC_LWEICHTIER, m_LWeichtier);
	DDX_Control(pDX, IDC_WEICHTIER, m_Weichtier);
	DDX_Control(pDX, IDC_LSONSTIGE, m_LSonstige);
	DDX_Control(pDX, IDC_SONSTIGE, m_Sonstige);
	DDX_Control(pDX, IDC_LLUPINE, m_LLupine);
	DDX_Control(pDX, IDC_LUPINE, m_Lupine);
}

BEGIN_MESSAGE_MAP(CProductPage3, CPropertyPage)
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()

BOOL CProductPage3::OnInitDialog()
{
	ProductPage3Read = TRUE;

	CPropertyPage::OnInitDialog();
	this->A_bas  = &Basis->A_bas;
	this->A_bas_erw  = &Basis->A_bas_erw;
	this->DlgBkColor = Basis->DlgBkColor;

	Basis->Form.Get ();
    Basis->ProductPage3Read = TRUE;

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	BoldFont.CreateFontIndirect (&l);

	Form.Add (new CUniFormField (&m_Glg,CHECKBOX,     (char *)   A_bas->a_bas.glg, VCHAR));
	Form.Add (new CUniFormField (&m_Krebs,CHECKBOX,     (char *)   A_bas->a_bas.krebs, VCHAR));
	Form.Add (new CUniFormField (&m_Ei,CHECKBOX,     (char *)   A_bas->a_bas.ei, VCHAR));
	Form.Add (new CUniFormField (&m_Fisch,CHECKBOX,     (char *)   A_bas->a_bas.fisch, VCHAR));
	Form.Add (new CUniFormField (&m_Erdnuss,CHECKBOX,     (char *)   A_bas->a_bas.erdnuss, VCHAR));
	Form.Add (new CUniFormField (&m_Soja,CHECKBOX,     (char *)   A_bas->a_bas.soja, VCHAR));
	Form.Add (new CUniFormField (&m_Milch,CHECKBOX,     (char *)   A_bas->a_bas.milch, VCHAR));
	Form.Add (new CUniFormField (&m_Schal,CHECKBOX,     (char *)   A_bas->a_bas.schal, VCHAR));
	Form.Add (new CUniFormField (&m_Sellerie,CHECKBOX,     (char *)   A_bas->a_bas.sellerie, VCHAR));
	Form.Add (new CUniFormField (&m_SenfSaat,CHECKBOX,     (char *)   A_bas->a_bas.senfsaat, VCHAR));
	Form.Add (new CUniFormField (&m_SesamSamen,CHECKBOX,     (char *)   A_bas->a_bas.sesamsamen, VCHAR));
	Form.Add (new CUniFormField (&m_Sulfit,CHECKBOX,     (char *)   A_bas->a_bas.sulfit, VCHAR));
	Form.Add (new CUniFormField (&m_Weichtier,CHECKBOX,     (char *)   A_bas->a_bas.weichtier, VCHAR));
	Form.Add (new CUniFormField (&m_Lupine,CHECKBOX,     (char *)   A_bas_erw->a_bas_erw.lupine, VCHAR));
	Form.Add (new CUniFormField (&m_Sonstige,CHECKBOX,     (char *)   A_bas->a_bas.sonstige, VCHAR));

    CtrlGrid.Create (this, 40, 40);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 2);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_Aub1  = new CCtrlInfo (&m_Aub1, 0, 0, 1, 1); 
	CtrlGrid.Add (c_Aub1);
	CCtrlInfo *c_Aub2  = new CCtrlInfo (&m_Aub2, 2, 0, 1, 1); 
	CtrlGrid.Add (c_Aub2);

	CCtrlLine *LineH2 = new CCtrlLine (this, VERTICAL, 1, 1, 3, 0, 1, DOCKBOTTOM);
	CCtrlLine *Line1 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 1, DOCKSIZE, 1);
	Line1->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line1);

	CCtrlInfo *c_LGlg1  = new CCtrlInfo (&m_LGlg1, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LGlg1);
	CCtrlInfo *c_LGlg2  = new CCtrlInfo (&m_LGlg2, 0, 3, 1, 1); 
	CtrlGrid.Add (c_LGlg2);
	CCtrlInfo *c_Glg  = new CCtrlInfo (&m_Glg, 2, 2, 1, 1); 
	c_Glg->xplus = 50;
	CtrlGrid.Add (c_Glg);

	CCtrlLine *Line2 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 4, DOCKSIZE, 1);
	Line2->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line2);

	CCtrlInfo *c_LKrebs1  = new CCtrlInfo (&m_LKrebs1, 0, 5, 1, 1); 
	CtrlGrid.Add (c_LKrebs1);
	CCtrlInfo *c_LKrebs2  = new CCtrlInfo (&m_LKrebs2, 0, 6, 1, 1); 
	CtrlGrid.Add (c_LKrebs2);
	CCtrlInfo *c_Krebs  = new CCtrlInfo (&m_Krebs, 2, 5, 1, 1); 
	c_Krebs->xplus = 50;
	CtrlGrid.Add (c_Krebs);

	CCtrlLine *Line3 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 7, DOCKSIZE, 1);
	Line3->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line3);

	CCtrlInfo *c_LEi1  = new CCtrlInfo (&m_LEi1, 0, 8, 1, 1); 
	CtrlGrid.Add (c_LEi1);
	CCtrlInfo *c_LEi2  = new CCtrlInfo (&m_LEi2, 0, 9, 1, 1); 
	CtrlGrid.Add (c_LEi2);
	CCtrlInfo *c_Ei  = new CCtrlInfo (&m_Ei, 2, 8, 1, 1); 
	c_Ei->xplus = 50;
	CtrlGrid.Add (c_Ei);

	CCtrlLine *Line4 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 10, DOCKSIZE, 1);
	Line4->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line4);

	CCtrlInfo *c_LFisch1  = new CCtrlInfo (&m_LFisch1, 0, 11, 1, 1); 
	CtrlGrid.Add (c_LFisch1);
	CCtrlInfo *c_LFisch2  = new CCtrlInfo (&m_LFisch2, 0, 12, 1, 1); 
	CtrlGrid.Add (c_LFisch2);
	CCtrlInfo *c_Fisch  = new CCtrlInfo (&m_Fisch, 2, 11, 1, 1); 
	c_Fisch->xplus = 50;
	CtrlGrid.Add (c_Fisch);

	CCtrlLine *Line5 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 13, DOCKSIZE, 1);
	Line5->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line5);

	CCtrlInfo *c_LErdnuss1  = new CCtrlInfo (&m_LErdnuss1, 0, 14, 1, 1); 
	CtrlGrid.Add (c_LErdnuss1);
	CCtrlInfo *c_LErdnuss2  = new CCtrlInfo (&m_LErdnuss2, 0, 15, 1, 1); 
	CtrlGrid.Add (c_LErdnuss2);
	CCtrlInfo *c_Erdnuss  = new CCtrlInfo (&m_Erdnuss, 2, 14, 1, 1); 
	c_Erdnuss->xplus = 50;
	CtrlGrid.Add (c_Erdnuss);

	CCtrlLine *Line6 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 16, DOCKSIZE, 1);
	Line6->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line6);

	CCtrlInfo *c_LSoja1  = new CCtrlInfo (&m_LSoja1, 0, 17, 1, 1); 
	CtrlGrid.Add (c_LSoja1);
	CCtrlInfo *c_LSoja2  = new CCtrlInfo (&m_LSoja2, 0, 18, 1, 1); 
	CtrlGrid.Add (c_LSoja2);
	CCtrlInfo *c_Soja  = new CCtrlInfo (&m_Soja, 2, 17, 1, 1); 
	c_Soja->xplus = 50;
	CtrlGrid.Add (c_Soja);

	CCtrlLine *Line7 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 19, DOCKSIZE, 1);
	Line7->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line7);

	CCtrlInfo *c_LMilch1  = new CCtrlInfo (&m_LMilch1, 0, 20, 1, 1); 
	CtrlGrid.Add (c_LMilch1);
	CCtrlInfo *c_LMilch2  = new CCtrlInfo (&m_LMilch2, 0, 21, 1, 1); 
	CtrlGrid.Add (c_LMilch2);
	CCtrlInfo *c_Milch  = new CCtrlInfo (&m_Milch, 2, 20, 1, 1); 
	c_Milch->xplus = 50;
	CtrlGrid.Add (c_Milch);

	CCtrlLine *Line8 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 22, DOCKSIZE, 1);
	Line8->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line8);

	CCtrlInfo *c_LSchal1  = new CCtrlInfo (&m_LSchal1, 0, 23, 1, 1); 
	CtrlGrid.Add (c_LSchal1);
	CCtrlInfo *c_LSchal2  = new CCtrlInfo (&m_LSchal2, 0, 24, 1, 1); 
	CtrlGrid.Add (c_LSchal2);
	CCtrlInfo *c_Schal  = new CCtrlInfo (&m_Schal, 2, 23, 1, 1); 
	c_Schal->xplus = 50;
	CtrlGrid.Add (c_Schal);

	CCtrlLine *Line9 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 25, DOCKSIZE, 1);
	Line9->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line9);

	CCtrlInfo *c_LSellerie1  = new CCtrlInfo (&m_LSellerie1, 0, 26, 1, 1); 
	CtrlGrid.Add (c_LSellerie1);
	CCtrlInfo *c_LSellerie2  = new CCtrlInfo (&m_LSellerie2, 0, 27, 1, 1); 
	CtrlGrid.Add (c_LSellerie2);
	CCtrlInfo *c_Sellerie  = new CCtrlInfo (&m_Sellerie, 2, 26, 1, 1); 
	c_Sellerie->xplus = 50;
	CtrlGrid.Add (c_Sellerie);

	CCtrlLine *Line10 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 28, DOCKSIZE, 1);
	Line10->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line10);

	CCtrlInfo *c_LSenfSaat1  = new CCtrlInfo (&m_LSenfSaat1, 0, 29, 1, 1); 
	CtrlGrid.Add (c_LSenfSaat1);
	CCtrlInfo *c_LSenfSaat2  = new CCtrlInfo (&m_LSenfSaat2, 0, 30, 1, 1); 
	CtrlGrid.Add (c_LSenfSaat2);
	CCtrlInfo *c_SenfSaat  = new CCtrlInfo (&m_SenfSaat, 2, 29, 1, 1); 
	c_SenfSaat->xplus = 50;
	CtrlGrid.Add (c_SenfSaat);

	CCtrlLine *Line11 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 31, DOCKSIZE, 1);
	Line11->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line11);

	CCtrlInfo *c_LSesamSamen1  = new CCtrlInfo (&m_LSesamSamen1, 0, 32, 1, 1); 
	CtrlGrid.Add (c_LSesamSamen1);
	CCtrlInfo *c_LSesamSamen2  = new CCtrlInfo (&m_LSesamSamen2, 0, 33, 1, 1); 
	CtrlGrid.Add (c_LSesamSamen2);
	CCtrlInfo *c_SesamSamen  = new CCtrlInfo (&m_SesamSamen, 2, 32, 1, 1); 
	c_SesamSamen->xplus = 50;
	CtrlGrid.Add (c_SesamSamen);

	CCtrlLine *Line12 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 34, DOCKSIZE, 1);
	Line12->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line12);

	CCtrlInfo *c_LSulfit  = new CCtrlInfo (&m_LSulfit, 0, 35, 1, 1); 
	CtrlGrid.Add (c_LSulfit);
	CCtrlInfo *c_Sulfit  = new CCtrlInfo (&m_Sulfit, 2, 35, 1, 1); 
	c_Sulfit->xplus = 50;
	CtrlGrid.Add (c_Sulfit);

	CCtrlLine *Line13 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 37, DOCKSIZE, 1);
	Line13->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line13);

	CCtrlInfo *c_LWeichtier  = new CCtrlInfo (&m_LWeichtier, 0, 38, 1, 1); 
	CtrlGrid.Add (c_LWeichtier);
	CCtrlInfo *c_Weichtier  = new CCtrlInfo (&m_Weichtier, 2, 38, 1, 1); 
	c_Weichtier->xplus = 50;
	CtrlGrid.Add (c_Weichtier);

	CCtrlLine *Line14 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 40, DOCKSIZE, 1);
	Line14->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line14);

	CCtrlInfo *c_LLupide  = new CCtrlInfo (&m_LLupine, 0, 41, 1, 1); 
	CtrlGrid.Add (c_LLupide);
	CCtrlInfo *c_Lupide  = new CCtrlInfo (&m_Lupine, 2, 41, 1, 1); 
	c_Lupide->xplus = 50;
	CtrlGrid.Add (c_Lupide);

	CCtrlLine *Line15 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 43, DOCKSIZE, 1);
	Line15->RightDockControl = LineH2->cWnd;
	CtrlGrid.Add (Line15);

	CCtrlInfo *c_LSonstige  = new CCtrlInfo (&m_LSonstige, 0, 44, 1, 1); 
	CtrlGrid.Add (c_LSonstige);
	CCtrlInfo *c_Sonstige  = new CCtrlInfo (&m_Sonstige, 2, 44, 1, 1); 
	c_Sonstige->xplus = 50;
	CtrlGrid.Add (c_Sonstige);

	CCtrlLine *LineH = new CCtrlLine (this, VERTICAL, 1, 1, 1, 0, 1, DOCKBOTTOM);
	CtrlGrid.Add (LineH);
	CtrlGrid.Add (LineH2);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_Aub1.SetFont (&BoldFont);
	m_Aub2.SetFont (&BoldFont);
	m_LGlg1.SetFont (&BoldFont);
    m_LKrebs1.SetFont (&BoldFont); 
    m_LEi1.SetFont (&BoldFont); 
    m_LFisch1.SetFont (&BoldFont); 
    m_LErdnuss1.SetFont (&BoldFont); 
    m_LSoja1.SetFont (&BoldFont); 
    m_LMilch1.SetFont (&BoldFont); 
    m_LSchal1.SetFont (&BoldFont); 
    m_LSellerie1.SetFont (&BoldFont); 
    m_LSenfSaat1.SetFont (&BoldFont); 
    m_LSesamSamen1.SetFont (&BoldFont); 
    m_LSulfit.SetFont (&BoldFont); 
    m_LWeichtier.SetFont (&BoldFont); 
	m_LLupine.SetFont (&BoldFont); 
    m_LSonstige.SetFont (&BoldFont); 

	CtrlGrid.Display ();
	return TRUE;
}

// CProductPage3-Meldungshandler

HBRUSH CProductPage3::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
		    if (pWnd == &m_LGlg1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LKrebs1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LEi1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LFisch1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LErdnuss1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LSoja1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LMilch1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LSchal1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LSellerie1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LSenfSaat1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LSesamSamen1)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LSulfit)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LWeichtier)
			{
				pDC->SetTextColor (BoldColor);
			}
			else if (pWnd == &m_LLupine)
			{
				pDC->SetTextColor (BoldColor);
			}
		    else if (pWnd == &m_LSonstige)
			{
				pDC->SetTextColor (BoldColor);
			}
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDbPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CProductPage3::OnSize(UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	GetWindowRect (&pRect);
	GetParent ()->ScreenToClient (&pRect);
	int pcx = pRect.right -pRect.left - tabx;
	int pcy = pRect.bottom - pRect.top - taby;
	if (cx > pcx)
	{
		pcx = cx;
	}
	if (cy > pcy)
	{
		pcy = cy;
	}
	MoveWindow (tabx, taby, pcx, pcy);
	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = pcx - 10 - tabx;
	frame.bottom = pcy - 10 - taby;
	frame.top += 20;
	frame.left += 2;
    frame.right -= 10;
	frame.bottom -= 10;
}


BOOL CProductPage3::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return FALSE;
			}
			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeydown ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return FALSE;
}

void CProductPage3::UpdatePage ()
{
	Basis->Form.Get ();
	Form.Show ();
}

BOOL CProductPage3::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CProductPage3::OnKeydown ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CProductPage3::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

void CProductPage3::OnDelete ()
{
}

BOOL CProductPage3::OnKillActive ()
{
	Form.Get ();
	return TRUE;
}

BOOL CProductPage3::OnSetActive ()
{
	if (Basis != NULL)
	{
		Basis->Form.Get ();
	}
	DlgBkColor = Basis->DlgBkColor;
    DeleteObject (DlgBrush);
    DlgBrush = NULL;
	Form.Show ();
//	PostMessage (WM_SETFOCUS, (WPARAM) m_HuelArt.m_hWnd, 0l);
	return TRUE;
}

void CProductPage3::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CProductPage3::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

BOOL CProductPage3::Read ()
{
	if (!ProductPage3Read)
	{
		return FALSE;
	}
	Form.Show ();
    return TRUE;
}
