// ArtikelDoc.h : Schnittstelle der Klasse CArtikelDoc
//


#pragma once

class CArtikelDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CArtikelDoc();
	DECLARE_DYNCREATE(CArtikelDoc)

// Attribute
public:

// Operationen
public:

// Überschreibungen
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CArtikelDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


