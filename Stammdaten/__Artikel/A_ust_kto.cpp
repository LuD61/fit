#include "stdafx.h"
#include "a_ust_kto.h"

struct A_UST_KTO a_ust_kto, a_ust_kto_null, a_ust_kto_def;

void A_UST_KTO_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)    &a_ust_kto.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_ust_kto.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_ust_kto.a,   SQLDOUBLE, 0);
    sqlout ((double *) &a_ust_kto.a,SQLDOUBLE,0);
    sqlout ((short *) &a_ust_kto.mdn,SQLSHORT,0);
    sqlout ((short *) &a_ust_kto.fil,SQLSHORT,0);
    sqlout ((long *) &a_ust_kto.erl_kto,SQLLONG,0);
    sqlout ((short *) &a_ust_kto.mwst,SQLSHORT,0);
    sqlout ((long *) &a_ust_kto.skto_kto,SQLLONG,0);
    sqlout ((long *) &a_ust_kto.we_kto,SQLLONG,0);
            cursor = sqlcursor (_T("select a_ust_kto.a,  ")
_T("a_ust_kto.mdn,  a_ust_kto.fil,  a_ust_kto.erl_kto,  a_ust_kto.mwst,  ")
_T("a_ust_kto.skto_kto,  a_ust_kto.we_kto from a_ust_kto ")

#line 14 "a_ust_kto.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
    sqlin ((double *) &a_ust_kto.a,SQLDOUBLE,0);
    sqlin ((short *) &a_ust_kto.mdn,SQLSHORT,0);
    sqlin ((short *) &a_ust_kto.fil,SQLSHORT,0);
    sqlin ((long *) &a_ust_kto.erl_kto,SQLLONG,0);
    sqlin ((short *) &a_ust_kto.mwst,SQLSHORT,0);
    sqlin ((long *) &a_ust_kto.skto_kto,SQLLONG,0);
    sqlin ((long *) &a_ust_kto.we_kto,SQLLONG,0);
            sqltext = _T("update a_ust_kto set ")
_T("a_ust_kto.a = ?,  a_ust_kto.mdn = ?,  a_ust_kto.fil = ?,  ")
_T("a_ust_kto.erl_kto = ?,  a_ust_kto.mwst = ?,  ")
_T("a_ust_kto.skto_kto = ?,  a_ust_kto.we_kto = ? ")

#line 18 "a_ust_kto.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?");
            sqlin ((short *)    &a_ust_kto.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_ust_kto.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_ust_kto.a,   SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)    &a_ust_kto.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_ust_kto.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_ust_kto.a,   SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_ust_kto ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
            sqlin ((short *)    &a_ust_kto.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_ust_kto.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_ust_kto.a,   SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_ust_kto set a = a ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
            sqlin ((short *)    &a_ust_kto.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_ust_kto.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_ust_kto.a,   SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_ust_kto ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
    sqlin ((double *) &a_ust_kto.a,SQLDOUBLE,0);
    sqlin ((short *) &a_ust_kto.mdn,SQLSHORT,0);
    sqlin ((short *) &a_ust_kto.fil,SQLSHORT,0);
    sqlin ((long *) &a_ust_kto.erl_kto,SQLLONG,0);
    sqlin ((short *) &a_ust_kto.mwst,SQLSHORT,0);
    sqlin ((long *) &a_ust_kto.skto_kto,SQLLONG,0);
    sqlin ((long *) &a_ust_kto.we_kto,SQLLONG,0);
            ins_cursor = sqlcursor (_T("insert into a_ust_kto (")
_T("a,  mdn,  fil,  erl_kto,  mwst,  skto_kto,  we_kto) ")

#line 48 "a_ust_kto.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?)"));

#line 50 "a_ust_kto.rpp"
}
