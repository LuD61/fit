// PrintRegEtiDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "PrintRegEtiDlg.h"
#include "DbUniCode.h"


// CPrintRegEtiDlg-Dialogfeld

IMPLEMENT_DYNAMIC(CPrintRegEtiDlg, CDialog)
CPrintRegEtiDlg::CPrintRegEtiDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintRegEtiDlg::IDD, pParent)
{
	ChoiceHWG = NULL;
	ChoiceWG = NULL;
	ChoiceAG = NULL;
	ChoiceA = NULL;

	Hwg   = _T("");
	Wg    = _T("");
	Ag    = _T("");
	A     = _T("");

	AFrom = _T("");
	ATo   = _T("");
}

CPrintRegEtiDlg::~CPrintRegEtiDlg()
{
}

void CPrintRegEtiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ETI_DAT, m_EtiDat);
	DDX_Control(pDX, IDC_ETI_TYPE, m_EtiType);
	DDX_Control(pDX, IDC_ART_STM, m_ArtStm);
	DDX_Control(pDX, IDC_PRICE, m_Price);
	DDX_Control(pDX, IDC_CHANGES, m_Changes);
	DDX_Control(pDX, IDC_HWG, m_Hwg);
	DDX_Control(pDX, IDC_WG, m_Wg);
	DDX_Control(pDX, IDC_AG, m_Ag);
	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_A_FROM, m_AFrom);
	DDX_Control(pDX, IDC_A_TO, m_ATo);
	DDX_Control(pDX, IDC_DAT_FROM, m_DatFrom);
	DDX_Control(pDX, IDC_DAT_TO, m_DatTo);
}


BEGIN_MESSAGE_MAP(CPrintRegEtiDlg, CDialog)
	ON_BN_CLICKED(IDC_CH_HWG, OnChoiceHwg)
	ON_BN_CLICKED(IDC_CH_WG,  OnChoiceWg)
	ON_BN_CLICKED(IDC_CH_AG,  OnChoiceAg)
	ON_BN_CLICKED(IDC_CH_A,   OnChoiceA)
	ON_BN_CLICKED(IDC_CHANGES, OnBnClickedChanges)
END_MESSAGE_MAP()


// CPrintRegEtiDlg-Meldungshandler

BOOL CPrintRegEtiDlg::OnInitDialog ()
{
	CDialog::OnInitDialog ();

    Form.Add (new CFormField (&m_EtiDat,EDIT,      (CString *) &EtiDat,  VSTRING));
    Form.Add (new CFormField (&m_EtiType,COMBOBOX, (CString *) &EtiType, VSHORT));
    Form.Add (new CFormField (&m_Hwg,EDIT,       (CString *) &Hwg,     VSTRING));
    Form.Add (new CFormField (&m_Wg,EDIT,        (CString *) &Wg,      VSTRING));
    Form.Add (new CFormField (&m_Ag,EDIT,        (CString *) &Ag,      VSTRING));
    Form.Add (new CFormField (&m_A,EDIT,         (CString *) &A,       VSTRING));
    Form.Add (new CFormField (&m_AFrom,EDIT,     (CString *) &AFrom,   VSTRING));
    Form.Add (new CFormField (&m_ATo,EDIT,       (CString *) &ATo,     VSTRING));
    Form.Add (new CFormField (&m_DatFrom,EDIT,   (CString *) &DatFrom, VSTRING));
    Form.Add (new CFormField (&m_DatTo,EDIT,     (CString *) &DatTo, VSTRING));

	if (Anzahl > 0)
	{
		m_Changes.SetCheck (BST_UNCHECKED);
		EnableChoiceFields (TRUE);
        AFrom.Format (_T("%.0lf"), A_bas->a_bas.a);
        ATo.Format (_T("%.0lf"), A_bas->a_bas.a);
	}
	else
	{
		m_Changes.SetCheck (BST_CHECKED);
		EnableChoiceFields (FALSE);
	}


	FillEtiTypeCombo (&m_EtiType, _T("reg_eti"));

	Form.Show ();

	return TRUE;
}

void CPrintRegEtiDlg::OnChoiceHwg()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	if (ChoiceHWG == NULL)
	{
		ChoiceHWG = new CChoiceHWG (this);
	    ChoiceHWG->IsModal = TRUE;
		ChoiceHWG->IdArrDown = IDI_HARROWDOWN;
		ChoiceHWG->IdArrUp   = IDI_HARROWUP;
		ChoiceHWG->IdArrNo   = IDI_HARROWNO;
	    ChoiceHWG->HideEnter = FALSE;
		ChoiceHWG->CreateDlg ();
		ChoiceHWG->SingleSelection = FALSE;
	}

//    ChoiceHWG->SetDbClass (&A_bas);

	ChoiceHWG->DoModal();

    if (ChoiceHWG->GetState ())
    {
		  CHWGList *abl; 

		  CString Text = "";
		  if (ChoiceHWG->SelectList.size () > 1)
		  {
				for (std::vector<CHWGList *>::iterator pabl = ChoiceHWG->SelectList.begin (); pabl != ChoiceHWG->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cHwg;
					cHwg.Format (_T("%hd"), abl->hwg);
					Text += cHwg;
				}
	   	  }
		  else
		  {
			  abl = ChoiceHWG->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%hd"), abl->hwg);
		  }
		  m_Hwg.SetWindowText (Text);
    }
}

void CPrintRegEtiDlg::OnChoiceWg()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (ChoiceWG == NULL)
	{
		ChoiceWG = new CChoiceWG (this);
	    ChoiceWG->IsModal = TRUE;
		ChoiceWG->IdArrDown = IDI_HARROWDOWN;
		ChoiceWG->IdArrUp   = IDI_HARROWUP;
		ChoiceWG->IdArrNo   = IDI_HARROWNO;
	    ChoiceWG->HideEnter = FALSE;
		ChoiceWG->CreateDlg ();
		ChoiceWG->SingleSelection = FALSE;
	}


	ChoiceWG->DoModal();

    if (ChoiceWG->GetState ())
    {
		  CWGList *abl; 

		  CString Text = "";
		  if (ChoiceWG->SelectList.size () > 1)
		  {
				for (std::vector<CWGList *>::iterator pabl = ChoiceWG->SelectList.begin (); pabl != ChoiceWG->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cWg;
					cWg.Format (_T("%hd"), abl->wg);
					Text += cWg;
				}
	   	  }
		  else
		  {
			  abl = ChoiceWG->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%hd"), abl->wg);
		  }
		  m_Wg.SetWindowText (Text);
    }
}
void CPrintRegEtiDlg::OnChoiceAg()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (ChoiceAG == NULL)
	{
		ChoiceAG = new CChoiceAG (this);
	    ChoiceAG->IsModal = TRUE;
		ChoiceAG->IdArrDown = IDI_HARROWDOWN;
		ChoiceAG->IdArrUp   = IDI_HARROWUP;
		ChoiceAG->IdArrNo   = IDI_HARROWNO;
	    ChoiceAG->HideEnter = FALSE;
		ChoiceAG->CreateDlg ();
		ChoiceAG->SingleSelection = FALSE;
	}


	ChoiceAG->DoModal();

    if (ChoiceAG->GetState ())
    {
		  CAGList *abl; 

		  CString Text = "";
		  if (ChoiceAG->SelectList.size () > 1)
		  {
				for (std::vector<CAGList *>::iterator pabl = ChoiceAG->SelectList.begin (); pabl != ChoiceAG->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cAg;
					cAg.Format (_T("%ld"), abl->ag);
					Text += cAg;
				}
	   	  }
		  else
		  {
			  abl = ChoiceAG->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%ld"), abl->ag);
		  }
		  m_Ag.SetWindowText (Text);
    }
}
void CPrintRegEtiDlg::OnChoiceA()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (ChoiceA == NULL)
	{
		ChoiceA = new CChoiceA (this);
	    ChoiceA->IsModal = TRUE;
		ChoiceA->IdArrDown = IDI_HARROWDOWN;
		ChoiceA->IdArrUp   = IDI_HARROWUP;
		ChoiceA->IdArrNo   = IDI_HARROWNO;
	    ChoiceA->HideEnter = FALSE;
		ChoiceA->CreateDlg ();
		ChoiceA->SingleSelection = FALSE;
	}


	ChoiceA->DoModal();

    if (ChoiceA->GetState ())
    {
		  CABasList *abl; 

		  CString Text = "";
		  if (ChoiceA->SelectList.size () > 1)
		  {
				for (std::vector<CABasList *>::iterator pabl = ChoiceA->SelectList.begin (); pabl != ChoiceA->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cA;
					cA.Format (_T("%.0lf"), abl->a);
					Text += cA;
				}
	   	  }
		  else
		  {
			  abl = ChoiceA->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%.0lf"), abl->a);
		  }
		  m_A.SetWindowText (Text);
    }
}

void CPrintRegEtiDlg::EnableChoiceFields (BOOL b)
{
	m_Hwg.EnableWindow (b);
	m_Wg.EnableWindow (b);
	m_Ag.EnableWindow (b);
	m_A.EnableWindow (b);

	m_AFrom.EnableWindow (b);
	m_ATo.EnableWindow (b);

	m_ArtStm.EnableWindow (!b);
	m_Price.EnableWindow (!b);
	m_DatFrom.EnableWindow (!b);
	m_DatTo.EnableWindow (!b);
}

void CPrintRegEtiDlg::OnBnClickedChanges()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (m_Changes.GetCheck () == BST_CHECKED)
	{
		EnableChoiceFields (FALSE);
	}
	else
	{
		EnableChoiceFields ();
	}
}

void CPrintRegEtiDlg::OnOK ()
{
	CString Where = "";
	CString Typ = "RegEti";
	Form.Get ();

	if (m_Changes.GetCheck () == BST_CHECKED)
	{
		Where.Format (_T("where dat between \"%s\" and \"%s\""), DatFrom, DatTo);
		if (m_ArtStm.GetCheck () == BST_CHECKED &&
			m_Price.GetCheck () == BST_CHECKED)
		{
			Where += _T(" and (erf_kz = \"S\" or erf_kz = \"P\")");
		}
		else if (m_ArtStm.GetCheck () == BST_CHECKED)
		{
			Where += _T(" and erf_kz = \"S\"");
		}
		else if (m_Price.GetCheck () == BST_CHECKED)
		{
			Where += _T(" and erf_kz = \"P\"");
		}
		Etikett.EtiType = EtiType;
		Etikett.RePrint (Where, EtiDat);
	}
	else
	{
		Where = _T(" where a > 0");
		if (Hwg.Trim () != "")
		{
			Where += _T(" and hwg in (");
			Where += Hwg;
			Where += _T(")");
		}
		if (Wg.Trim () != "")
		{
			Where += _T(" and wg in (");
			Where += Wg;
			Where += _T(")");
		}
		if (Ag.Trim () != "")
		{
			Where += _T(" and ag in (");
			Where += Ag;
			Where += _T(")");
		}
		if (A.Trim () != "")
		{
			Where += _T(" and a in (");
			Where += A;
			Where += _T(")");
		}
		if (AFrom.Trim () != "")
		{
			Where += _T(" and a >= ");
			Where += AFrom;
		}
		if (ATo.Trim () != "")
		{
			Where += _T(" and a <= ");
			Where += ATo;
		}
		Etikett.EtiType = EtiType;
		Etikett.RunPrintFromStm (Where, EtiDat,Typ, Anzahl);
	}
//	MessageBox (_T("Etikettendruck beendet"));
}

void CPrintRegEtiDlg::FillEtiTypeCombo (CComboBox *Control, LPTSTR Item)
{
	Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
	Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
	Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 

	CString Sql;
    Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

    int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
	while (Ptabn.sqlfetch (cursor) == 0)
	{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			CString Ptwert = Ptabn.ptabn.ptwert;
			if (Ptwert.Trim () == _T("N")) continue;

			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString ComboValue;
			ComboValue.Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
			Control->AddString (ComboValue.GetBuffer ()); 
	}
	Ptabn.sqlclose (cursor);
}
