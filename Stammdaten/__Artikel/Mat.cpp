// Mat.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "ChoiceQuid.h"
#include "ChoiceTier.h"
#include "Mat.h"
#include "UniFormField.h"


// CMat-Dialogfeld

IMPLEMENT_DYNAMIC(CMat, CDbPropertyPage)
CMat::CMat()
	: CDbPropertyPage(CMat::IDD)
{
	Basis = NULL;
	Mdn = NULL;
	MdnAdr = NULL;
	A_bas = NULL;
	A_varb = NULL;
	A_zut = NULL;
	A_huel = NULL;
	A_mat = NULL;
}

CMat::~CMat()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
}

void CMat::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_A_BZ1, m_A_bz1);
	DDX_Control(pDX, IDC_A_BZ2, m_A_bz2);
	DDX_Control(pDX, IDC_LMAT, m_LMat);
	DDX_Control(pDX, IDC_MAT, m_Mat);
	DDX_Control(pDX, IDC_LA_GEW2, m_LA_gew);
	DDX_Control(pDX, IDC_A_GEW2, m_A_gew);
	DDX_Control(pDX, IDC_MAT_GROUP, m_VarbGroup);
	DDX_Control(pDX, IDC_LTIER, m_LTier);
	DDX_Control(pDX, IDC_TIER, m_Tier);
	DDX_Control(pDX, IDC_LA_KZ, m_LAKz);
	DDX_Control(pDX, IDC_A_KZ, m_AKz);
	DDX_Control(pDX, IDC_LVPK_TYP, m_LVpkTyp);
	DDX_Control(pDX, IDC_VPK_TYP, m_VpkTyp);
	DDX_Control(pDX, IDC_LARB_ZEIT_FAKT, m_LArbZeitFakt);
	DDX_Control(pDX, IDC_ARB_ZEIT_FAKT, m_ArbZeitFakt);
	DDX_Control(pDX, IDC_LEIW_GES, m_LEiwGes);
	DDX_Control(pDX, IDC_EIW_GES, m_EiwGes);
	DDX_Control(pDX, IDC_LFWP, m_LFwp);
	DDX_Control(pDX, IDC_FWP, m_Fwp);
	DDX_Control(pDX, IDC_LFETT_EIW, m_LFettEiw);
	DDX_Control(pDX, IDC_FETT_EIW, m_FettEiw);
	DDX_Control(pDX, IDC_LBEFE, m_LBefe);
	DDX_Control(pDX, IDC_BEFE, m_Befe);
	DDX_Control(pDX, IDC_LBEFE_I_FETT, m_LBefeIFett);
	DDX_Control(pDX, IDC_BEFE_I_FETT, m_BefeIFett);
	DDX_Control(pDX, IDC_LLEITS, m_LLeits);
	DDX_Control(pDX, IDC_LEITS, m_Leits);
	DDX_Control(pDX, IDC_LFETT, m_LFett);
	DDX_Control(pDX, IDC_FETT, m_Fett);
	DDX_Control(pDX, IDC_LH2O, m_LH2o);
	DDX_Control(pDX, IDC_H2O, m_H2o);
	DDX_Control(pDX, IDC_LQUID_NR, m_LQuidNr);
	DDX_Control(pDX, IDC_QUID_NR, m_QuidNr);
	DDX_Control(pDX, IDC_LEWG, m_LEwg);
	DDX_Control(pDX, IDC_EWG, m_Ewg);
	DDX_Control(pDX, IDC_DKL_KZ, m_DklKz);
	DDX_Control(pDX, IDC_LVERWEND, m_LVerwend);
	DDX_Control(pDX, IDC_VERWEND, m_Verwend);
	DDX_Control(pDX, IDC_LZUG_MEN_MAX, m_LZugMenMax);
	DDX_Control(pDX, IDC_ZUG_MEN_MAX, m_ZugMenMax);
	DDX_Control(pDX, IDC_LLGR_BDG, m_LLgrBdg);
	DDX_Control(pDX, IDC_LGR_BDG, m_LgrBdg);
	DDX_Control(pDX, IDC_LBEM_OFFS, m_LBemOffs);
	DDX_Control(pDX, IDC_BEM_OFFS, m_BemOffs);
	DDX_Control(pDX, IDC_ZUT_GROUP, m_ZutGroup);
	DDX_Control(pDX, IDC_LKAL, m_LKal);
	DDX_Control(pDX, IDC_KAL, m_Kal);
	DDX_Control(pDX, IDC_LFARBE, m_LFarbe);
	DDX_Control(pDX, IDC_FARBE, m_Farbe);
	DDX_Control(pDX, IDC_LSW, m_LSw);
	DDX_Control(pDX, IDC_SW, m_Sw);
	DDX_Control(pDX, IDC_LTMPR_MAX, m_LTmprMax);
	DDX_Control(pDX, IDC_TMPR_MAX, m_TmprMax);
	DDX_Control(pDX, IDC_LFUEL_VOL, m_LFuelVol);
	DDX_Control(pDX, IDC_FUEL_VOL, m_FuelVol);
	DDX_Control(pDX, IDC_LFUEL_GEW, m_LFuelGew);
	DDX_Control(pDX, IDC_FUEL_GEW, m_FuelGew);
	DDX_Control(pDX, IDC_LHUEL_TXT, m_LHuelTxt);
	DDX_Control(pDX, IDC_HUEL_TXT, m_HuelTxt);
	DDX_Control(pDX, IDC_LBEM_OFFS2, m_LBemOffs2);
	DDX_Control(pDX, IDC_BEM_OFFS2, m_BemOffs2);
	DDX_Control(pDX, IDC_HUEL_GROUP, m_HuelGroup);
	DDX_Control(pDX, IDC_QUID_BEZ, m_QuidBez);
	DDX_Control(pDX, IDC_TIER_BZ, m_TierBz);
}


BEGIN_MESSAGE_MAP(CMat, CDbPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_SIZE ()
	ON_BN_CLICKED(IDC_ACHOICE ,  OnAchoice)
	ON_BN_CLICKED(IDC_QUIDCHOICE ,  OnQuidchoice)
	ON_BN_CLICKED(IDC_TIERCHOICE ,  OnTierchoice)
END_MESSAGE_MAP()


// CMat-Meldungshandler

BOOL CMat::OnInitDialog()
{
	CPropertyPage::OnInitDialog();


	Mdn     = &Basis->Mdn;
	MdnAdr  = &Basis->MdnAdr;
	A_bas   = &Basis->A_bas;
	A_varb  = &Basis->A_varb;
	A_zut   = &Basis->A_zut;
	A_huel   = &Basis->A_huel;
	A_mat    = &Basis->A_mat;

	ArchiveName = _T("Artikel.prp");
	Load ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

    Form.Add (new CFormField (&m_Mdn,EDIT,          (short *) &Mdn->mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT,   (char *) MdnAdr->adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_A,EDIT,            (double *) &A_bas->a_bas.a, VDOUBLE, 13, 0));
    Form.Add (new CUniFormField (&m_A_bz1,EDIT,     (char *) A_bas->a_bas.a_bz1, VCHAR));
    Form.Add (new CUniFormField (&m_A_bz2,EDIT,     (char *) A_bas->a_bas.a_bz2, VCHAR));
    Form.Add (new CFormField (&m_Mat,EDIT,          (long *) &A_mat->a_mat.mat, VLONG));
    Form.Add (new CFormField (&m_A_gew, EDIT,      (double *) &A_bas->a_bas.a_gew, VDOUBLE, 8, 3));
    Form.Add (new CFormField (&m_Tier,EDIT,         (short *)&A_varb->a_varb.tier, VSHORT));
    Form.Add (new CUniFormField (&m_TierBz,EDIT,    (char *) Tier.tier.tier_bz, VCHAR));
    Form.Add (new CUniFormField (&m_AKz,COMBOBOX,    (char *) A_varb->a_varb.a_kz, VCHAR));
    Form.Add (new CFormField (&m_VpkTyp,COMBOBOX,    (short *) &A_varb->a_varb.vpk_typ, VSHORT));
    Form.Add (new CFormField (&m_ArbZeitFakt,EDIT,   (double *) &A_varb->a_varb.arb_zeit_fakt, VDOUBLE, 5, 2));
    Form.Add (new CFormField (&m_EiwGes,EDIT,        (double *) &A_varb->a_varb.eiw_ges, VDOUBLE, 4, 1));
    Form.Add (new CFormField (&m_Fwp,EDIT,           (double *) &A_varb->a_varb.fwp, VDOUBLE, 5, 2));
    Form.Add (new CFormField (&m_FettEiw,EDIT,       (double *) &A_varb->a_varb.fett_eiw, VDOUBLE, 5, 2));
    Form.Add (new CFormField (&m_Befe,EDIT,          (double *) &A_varb->a_varb.befe, VDOUBLE, 4, 1));
    Form.Add (new CFormField (&m_BefeIFett,EDIT,     (double *) &A_varb->a_varb.befe_i_fett, VDOUBLE, 4, 1));
    Form.Add (new CUniFormField (&m_Leits,EDIT,      (char *) A_varb->a_varb.leits, VCHAR));
    Form.Add (new CFormField (&m_Fett,EDIT,          (double *) &A_varb->a_varb.fett, VDOUBLE, 5, 2));
    Form.Add (new CFormField (&m_H2o,EDIT,           (double *) &A_varb->a_varb.h2o, VDOUBLE, 4, 1));
    Form.Add (new CFormField (&m_QuidNr,EDIT,        (short *) &A_varb->a_varb.quid_nr, VSHORT));
    Form.Add (new CFormField (&m_QuidBez,EDIT,       (CString *) &QuidBez, VSTRING));
	FillPtabCombo (&m_AKz, _T("a_kz"));
	FillPtabCombo (&m_VpkTyp, _T("vpk_typ"));
//	FillPtabCombo (&m_Tier, _T("tier_kz"));
	m_Tier.SetLimitText (4);
	m_QuidNr.SetLimitText (4);
	m_Leits.SetLimitText (9);


    Form.Add (new CUniFormField (&m_Ewg,EDIT,        (char *) A_zut->a_zut.ewg, VCHAR));
    Form.Add (new CUniFormField (&m_DklKz,CHECKBOX,  (char *) A_zut->a_zut.dkl_kz, VCHAR));
    Form.Add (new CUniFormField (&m_Verwend,EDIT,    (char *) A_zut->a_zut.verwend, VCHAR));
    Form.Add (new CUniFormField (&m_ZugMenMax,EDIT,  (char *) A_zut->a_zut.zug_men_max, VCHAR));
    Form.Add (new CUniFormField (&m_LgrBdg,EDIT,     (char *) A_zut->a_zut.lgr_bdg, VCHAR));
    Form.Add (new CFormField (&m_BemOffs,EDIT,       (long *) &A_zut->a_zut.bem_offs, VLONG));
	m_Ewg.SetLimitText (6);
	m_Verwend.SetLimitText (48);
	m_ZugMenMax.SetLimitText (48);
	m_LgrBdg.SetLimitText (48);
	m_BemOffs.SetLimitText (8);

    Form.Add (new CUniFormField (&m_Kal,EDIT,        (char *) A_huel->a_huel.kal, VCHAR));
    Form.Add (new CUniFormField (&m_Farbe,COMBOBOX,  (short *)&A_huel->a_huel.farbe, VSHORT));
    Form.Add (new CUniFormField (&m_Sw,EDIT,         (double *)&A_huel->a_huel.sw, VDOUBLE, 3, 1));
    Form.Add (new CUniFormField (&m_TmprMax,EDIT,    (char *) A_huel->a_huel.tmpr_max, VCHAR));
    Form.Add (new CUniFormField (&m_FuelVol,EDIT,    (long *) &A_huel->a_huel.fuel_vol, VLONG));
    Form.Add (new CUniFormField (&m_FuelGew,EDIT,    (double *)&A_huel->a_huel.fuel_gew, VDOUBLE, 8, 3));
    Form.Add (new CUniFormField (&m_HuelTxt,EDIT,    (char *) A_huel->a_huel.huel_txt, VCHAR));
    Form.Add (new CFormField (&m_BemOffs2,EDIT,      (long *) &A_huel->a_huel.bem_offs, VLONG));
	FillPtabCombo (&m_Farbe, _T("farbe"));
	m_Kal.SetLimitText (8);
	m_TmprMax.SetLimitText (8);
	m_FuelVol.SetLimitText (8);
	m_HuelTxt.SetLimitText (48);
	m_BemOffs2.SetLimitText (8);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

// Artikelgewicht
    AGewGrid.Create (this, 2, 2);
    AGewGrid.SetBorder (0, 0);
    AGewGrid.SetCellHeight (15);
    AGewGrid.SetFontCellHeight (this, &Font);
    AGewGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

// Artikel mit Auswahl
	AGrid.Create (this, 1, 2);
    AGrid.SetBorder (0, 0);
    AGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 0, 0, 1, 1);
	AGrid.Add (c_A);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);


	//Mandant
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_Mdn  = new CCtrlInfo (&m_Mdn, 2, 0, 1, 1); 
	CtrlGrid.Add (c_Mdn);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 3, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

//Artikel
	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 1, 1, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 2, 1, 1, 1); 
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 3, 1, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 4, 1, 4, 1); 
	CtrlGrid.Add (c_A_bz2);

// Materialnummer
	CCtrlInfo *c_LMat     = new CCtrlInfo (&m_LMat, 1, 2, 1, 1); 
	CtrlGrid.Add (c_LMat);
	CCtrlInfo *c_Mat     = new CCtrlInfo (&m_Mat, 2, 2, 1, 1); 
	CtrlGrid.Add (c_Mat);

// Artikelgewicht
	CCtrlInfo *c_LA_gew     = new CCtrlInfo (&m_LA_gew, 0, 0, 1, 1); 
	AGewGrid.Add (c_LA_gew);
	CCtrlInfo *c_A_gew     = new CCtrlInfo (&m_A_gew, 1, 0, 1, 1); 
	AGewGrid.Add (c_A_gew);

	CCtrlInfo *c_AGewGrid     = new CCtrlInfo (&AGewGrid, 3, 2, 1, 1); 
	CtrlGrid.Add (c_AGewGrid);

// Grid Verarbeitungsmaterial

    VarbGrid.Create (this, 20, 20);
    VarbGrid.SetBorder (12, 20);
    VarbGrid.SetCellHeight (15);
    VarbGrid.SetFontCellHeight (this, &Font);
    VarbGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

// Rahmen Verarbeitungsmaterial

	CCtrlInfo *c_VarbGroup = new CCtrlInfo (&m_VarbGroup, 0, 0, DOCKRIGHT, 8);
	c_VarbGroup->rightspace = 20;
	c_VarbGroup->SetBottomControl (this, &m_Fwp, 10, CCtrlInfo::Bottom);
	VarbGrid.Add (c_VarbGroup);

// Grid f�r Tier

	TierGrid.Create (this, 1, 3);
    TierGrid.SetBorder (0, 0);
    TierGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Tier = new CCtrlInfo (&m_Tier, 0, 0, 1, 1);
	TierGrid.Add (c_Tier);
	TierGrid.CreateChoiceButton (m_TierChoice, IDC_TIERCHOICE, this);
	CCtrlInfo *c_TierChoice = new CCtrlInfo (&m_TierChoice, 1, 0, 1, 1);
	TierGrid.Add (c_TierChoice);
	CCtrlInfo *c_TierBz = new CCtrlInfo (&m_TierBz, 2, 0, 1, 1);
	c_TierBz->SetCellPos (5, 0);
	TierGrid.Add (c_TierBz);

	CCtrlInfo *c_LTier = new CCtrlInfo (&m_LTier, 1, 1, 1, 1);
	VarbGrid.Add (c_LTier);

	CCtrlInfo *c_TierGrid = new CCtrlInfo (&TierGrid, 2, 1, 1, 1);
	VarbGrid.Add (c_TierGrid);

/*
	CCtrlInfo *c_Tier = new CCtrlInfo (&m_Tier, 2, 1, 1, 1);
	VarbGrid.Add (c_Tier);
*/

	CCtrlInfo *c_LAKz = new CCtrlInfo (&m_LAKz, 1, 2, 1, 1);
	VarbGrid.Add (c_LAKz);
	CCtrlInfo *c_AKz = new CCtrlInfo (&m_AKz, 2, 2, 1, 1);
	VarbGrid.Add (c_AKz);

	CCtrlInfo *c_LVpkTyp = new CCtrlInfo (&m_LVpkTyp, 1, 3, 1, 1);
	VarbGrid.Add (c_LVpkTyp);
	CCtrlInfo *c_VpkTyp = new CCtrlInfo (&m_VpkTyp, 2, 3, 1, 1);
	VarbGrid.Add (c_VpkTyp);

	CCtrlInfo *c_LArbZeitFakt = new CCtrlInfo (&m_LArbZeitFakt, 1, 4, 1, 1);
	VarbGrid.Add (c_LArbZeitFakt);
	CCtrlInfo *c_ArbZeitFakt = new CCtrlInfo (&m_ArbZeitFakt, 2, 4, 1, 1);
	VarbGrid.Add (c_ArbZeitFakt);

	CCtrlInfo *c_LFwp = new CCtrlInfo (&m_LFwp, 1, 5, 1, 1);
	VarbGrid.Add (c_LFwp);
	CCtrlInfo *c_Fwp = new CCtrlInfo (&m_Fwp, 2, 5, 1, 1);
	VarbGrid.Add (c_Fwp);

    CCtrlInfo *c_VarbSpace1 = new CCtrlInfo (this, 50, 3, 1, 1, 1);  
	VarbGrid.Add (c_VarbSpace1);

	CCtrlInfo *c_LEiwGes = new CCtrlInfo (&m_LEiwGes, 4, 1, 1, 1);
	VarbGrid.Add (c_LEiwGes);
	CCtrlInfo *c_EiwGes = new CCtrlInfo (&m_EiwGes, 5, 1, 1, 1);
	VarbGrid.Add (c_EiwGes);

	CCtrlInfo *c_LFettEiw = new CCtrlInfo (&m_LFettEiw, 4, 2, 1, 1);
	VarbGrid.Add (c_LFettEiw);
	CCtrlInfo *c_FettEiw = new CCtrlInfo (&m_FettEiw, 5, 2, 1, 1);
	VarbGrid.Add (c_FettEiw);

	CCtrlInfo *c_LBefe = new CCtrlInfo (&m_LBefe, 4, 3, 1, 1);
	VarbGrid.Add (c_LBefe);
	CCtrlInfo *c_Befe = new CCtrlInfo (&m_Befe, 5, 3, 1, 1);
	VarbGrid.Add (c_Befe);

	CCtrlInfo *c_LBefeIFett = new CCtrlInfo (&m_LBefeIFett, 4, 4, 1, 1);
	VarbGrid.Add (c_LBefeIFett);
	CCtrlInfo *c_BefeIFett = new CCtrlInfo (&m_BefeIFett, 5, 4, 1, 1);
	VarbGrid.Add (c_BefeIFett);

    CCtrlInfo *c_VarbSpace2 = new CCtrlInfo (this, 50, 6, 1, 1, 1);  
	VarbGrid.Add (c_VarbSpace2);

	CCtrlInfo *c_LLeits = new CCtrlInfo (&m_LLeits, 7, 1, 1, 1);
	VarbGrid.Add (c_LLeits);
	CCtrlInfo *c_Leits = new CCtrlInfo (&m_Leits, 8, 1, 1, 1);
	VarbGrid.Add (c_Leits);

	CCtrlInfo *c_LFett = new CCtrlInfo (&m_LFett, 7, 2, 1, 1);
	VarbGrid.Add (c_LFett);
	CCtrlInfo *c_Fett = new CCtrlInfo (&m_Fett, 8, 2, 1, 1);
	VarbGrid.Add (c_Fett);

	CCtrlInfo *c_LH2o = new CCtrlInfo (&m_LH2o, 7, 3, 1, 1);
	VarbGrid.Add (c_LH2o);
	CCtrlInfo *c_H2o = new CCtrlInfo (&m_H2o, 8, 3, 1, 1);
	VarbGrid.Add (c_H2o);

// Grid fpr Quid

	QuidGrid.Create (this, 1, 2);
    QuidGrid.SetBorder (0, 0);
    QuidGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_QuidNr = new CCtrlInfo (&m_QuidNr, 0, 0, 1, 1);
	QuidGrid.Add (c_QuidNr);
	CtrlGrid.CreateChoiceButton (m_QuidChoice, IDC_QUIDCHOICE, this);
	CCtrlInfo *c_QuidChoice = new CCtrlInfo (&m_QuidChoice, 1, 0, 1, 1);
	QuidGrid.Add (c_QuidChoice);

	CCtrlInfo *c_LQuidNr = new CCtrlInfo (&m_LQuidNr, 7, 4, 1, 1);
	VarbGrid.Add (c_LQuidNr);
	CCtrlInfo *c_QuidGrid = new CCtrlInfo (&QuidGrid, 8, 4, 1, 1);
	VarbGrid.Add (c_QuidGrid);
	CCtrlInfo *c_QuidBez = new CCtrlInfo (&m_QuidBez, 7, 5, 3, 1);
	VarbGrid.Add (c_QuidBez);
/*
	CCtrlInfo *c_QuidNr = new CCtrlInfo (&m_QuidNr, 8, 4, 1, 1);
	VarbGrid.Add (c_QuidNr);
*/

	CCtrlInfo *c_VarbGrid   = new CCtrlInfo (&VarbGrid, 0, 4, 5, 5); 
	CtrlGrid.Add (c_VarbGrid);

// Grid Zutaten

    ZutGrid.Create (this, 20, 20);
    ZutGrid.SetBorder (12, 20);
    ZutGrid.SetCellHeight (15);
    ZutGrid.SetFontCellHeight (this, &Font);
    ZutGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

// Rahmen Zutaten

	CCtrlInfo *c_ZutGroup = new CCtrlInfo (&m_ZutGroup, 0, 0, DOCKRIGHT, 8);
	c_ZutGroup->rightspace = 20;
	c_ZutGroup->SetBottomControl (this, &m_LgrBdg, 10, CCtrlInfo::Bottom);
	ZutGrid.Add (c_ZutGroup);

	CCtrlInfo *c_LEwg = new CCtrlInfo (&m_LEwg, 1, 1, 1, 1);
	ZutGrid.Add (c_LEwg);
	CCtrlInfo *c_Ewg = new CCtrlInfo (&m_Ewg, 2, 1, 1, 1);
	ZutGrid.Add (c_Ewg);

	CCtrlInfo *c_DklKz = new CCtrlInfo (&m_DklKz, 2, 2, 1, 1);
	ZutGrid.Add (c_DklKz);

	CCtrlInfo *c_LVerwend = new CCtrlInfo (&m_LVerwend, 1, 3, 1, 1);
	ZutGrid.Add (c_LVerwend);
	CCtrlInfo *c_Verwend = new CCtrlInfo (&m_Verwend, 2, 3, 1, 1);
	ZutGrid.Add (c_Verwend);

	CCtrlInfo *c_LZugMenMax = new CCtrlInfo (&m_LZugMenMax, 1, 4, 1, 1);
	ZutGrid.Add (c_LZugMenMax);
	CCtrlInfo *c_ZugMenMax = new CCtrlInfo (&m_ZugMenMax, 2, 4, 1, 1);
	ZutGrid.Add (c_ZugMenMax);

	CCtrlInfo *c_LLgrBdg = new CCtrlInfo (&m_LLgrBdg, 1, 5, 1, 1);
	ZutGrid.Add (c_LLgrBdg);
	CCtrlInfo *c_LgrBdg = new CCtrlInfo (&m_LgrBdg, 2, 5, 1, 1);
	ZutGrid.Add (c_LgrBdg);

    CCtrlInfo *c_ZutSpace1 = new CCtrlInfo (this, 50, 3, 1, 1, 1);  
	ZutGrid.Add (c_ZutSpace1);

	CCtrlInfo *c_LBemOffs = new CCtrlInfo (&m_LBemOffs, 4, 2, 1, 1);
	ZutGrid.Add (c_LBemOffs);
	CCtrlInfo *c_BemOffs = new CCtrlInfo (&m_BemOffs, 5, 2, 1, 1);
	ZutGrid.Add (c_BemOffs);

	CCtrlInfo *c_ZutGrid   = new CCtrlInfo (&ZutGrid, 0, 11, 5, 5); 
	CtrlGrid.Add (c_ZutGrid);


// Grid H�llen

    HuelGrid.Create (this, 20, 20);
    HuelGrid.SetBorder (12, 20);
    HuelGrid.SetCellHeight (15);
    HuelGrid.SetFontCellHeight (this, &Font);
    HuelGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand


// Rahmen H�llen

	CCtrlInfo *c_HuelGroup = new CCtrlInfo (&m_HuelGroup, 0, 0, DOCKRIGHT, 8);
	c_HuelGroup->rightspace = 20;
	c_HuelGroup->SetBottomControl (this, &m_HuelTxt, 10, CCtrlInfo::Bottom);
	HuelGrid.Add (c_HuelGroup);

	CCtrlInfo *c_LKal = new CCtrlInfo (&m_LKal, 1, 1, 1, 1);
	HuelGrid.Add (c_LKal);
	CCtrlInfo *c_Kal = new CCtrlInfo (&m_Kal, 2, 1, 1, 1);
	HuelGrid.Add (c_Kal);

	CCtrlInfo *c_LFarbe = new CCtrlInfo (&m_LFarbe, 1, 2, 1, 1);
	HuelGrid.Add (c_LFarbe);
	CCtrlInfo *c_Farbe = new CCtrlInfo (&m_Farbe, 2, 2, 1, 1);
	HuelGrid.Add (c_Farbe);

	CCtrlInfo *c_LHuelTxt = new CCtrlInfo (&m_LHuelTxt, 1, 3, 1, 1);
	HuelGrid.Add (c_LHuelTxt);
	CCtrlInfo *c_HuelTxt = new CCtrlInfo (&m_HuelTxt, 2, 3, 4, 1);
	HuelGrid.Add (c_HuelTxt);

    CCtrlInfo *c_HuelSpace1 = new CCtrlInfo (this, 50, 3, 1, 1, 1);  
	HuelGrid.Add (c_HuelSpace1);

	CCtrlInfo *c_LTmprMax = new CCtrlInfo (&m_LTmprMax, 4, 1, 1, 1);
	HuelGrid.Add (c_LTmprMax);
	CCtrlInfo *c_TmprMax = new CCtrlInfo (&m_TmprMax, 5, 1, 1, 1);
	HuelGrid.Add (c_TmprMax);

	CCtrlInfo *c_LSw = new CCtrlInfo (&m_LSw, 4, 2, 1, 1);
	HuelGrid.Add (c_LSw);
	CCtrlInfo *c_Sw = new CCtrlInfo (&m_Sw, 5, 2, 1, 1);
	HuelGrid.Add (c_Sw);

    CCtrlInfo *c_HuelSpace2 = new CCtrlInfo (this, 50, 6, 1, 1, 1);  
	HuelGrid.Add (c_HuelSpace2);

	CCtrlInfo *c_LFuelVol = new CCtrlInfo (&m_LFuelVol, 7, 1, 1, 1);
	HuelGrid.Add (c_LFuelVol);
	CCtrlInfo *c_FuelVol = new CCtrlInfo (&m_FuelVol, 8, 1, 1, 1);
	HuelGrid.Add (c_FuelVol);

	CCtrlInfo *c_LFuelGew = new CCtrlInfo (&m_LFuelGew, 7, 2, 1, 1);
	HuelGrid.Add (c_LFuelGew);
	CCtrlInfo *c_FuelGew = new CCtrlInfo (&m_FuelGew, 8, 2, 1, 1);
	HuelGrid.Add (c_FuelGew);

	CCtrlInfo *c_LBemOffs2 = new CCtrlInfo (&m_LBemOffs2, 7, 3, 1, 1);
	HuelGrid.Add (c_LBemOffs2);
	CCtrlInfo *c_BemOffs2 = new CCtrlInfo (&m_BemOffs2, 8, 3, 1, 1);
	HuelGrid.Add (c_BemOffs2);

	CCtrlInfo *c_HuelGrid   = new CCtrlInfo (&HuelGrid, 0, 18, 5, 5); 
	CtrlGrid.Add (c_HuelGrid);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
    QuidBez = _T("");
	memcpy (&Tier.tier, &tier_null, sizeof (TIER));

	Form.Show ();
	CtrlGrid.Display ();
	EnableATyp ();
    return TRUE;
}

void CMat::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

HBRUSH CMat::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CMat::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					if (OnReturn ())
					{
						return TRUE;
					}
				}
				else
				{
					if (OnKeyup ())
					{
						return TRUE;
					}
				}
				break;
			}

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return FALSE;
}

BOOL CMat::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == (CWnd *) &m_QuidNr)
	{
		if (!ReadQuid ())
		{
			m_QuidNr.SetFocus ();
			return FALSE;
		}
	}

	if (Control == (CWnd *) &m_Tier)
	{
		if (!ReadTier ())
		{
			m_Tier.SetFocus ();
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CMat::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

void CMat::UpdatePage ()
{
	Form.Show ();
	ReadQuid ();
	ReadTier ();
	EnableATyp ();
}

void CMat::GetPage ()
{
	Form.Get ();
}

void CMat::EnableATyp ()
{
	if (Basis->a_typ == Basis->Varb)
	{
		VarbGrid.Enable (TRUE);
		ZutGrid.Enable (FALSE);
		HuelGrid.Enable (FALSE);
		m_Mat.EnableWindow (TRUE);
	}
	else if (Basis->a_typ == Basis->Zut)
	{
		VarbGrid.Enable (FALSE);
		ZutGrid.Enable (TRUE);
		HuelGrid.Enable (FALSE);
		m_Mat.EnableWindow (TRUE);
	}
	else if (Basis->a_typ == Basis->Huel)
	{
		VarbGrid.Enable (FALSE);
		ZutGrid.Enable (FALSE);
		HuelGrid.Enable (TRUE);
		m_Mat.EnableWindow (TRUE);
	}
	else
	{
		VarbGrid.Enable (FALSE);
		ZutGrid.Enable (FALSE);
		HuelGrid.Enable (FALSE);
		m_Mat.EnableWindow (FALSE);
	}
}

void CMat::FillPtabCombo (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 

		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
//		f->SetSel (0);
//		f->Get ();
	}
}

void CMat::OnAchoice ()
{
	Basis->OnAchoice ();
}

void CMat::OnQuidchoice ()
{
    CChoiceQuid Choice;

	Form.Get ();
    Choice.IsModal = TRUE;
	Choice.IdArrDown = IDI_HARROWDOWN;
	Choice.IdArrUp   = IDI_HARROWUP;
	Choice.IdArrNo   = IDI_HARROWNO;
    Choice.HideEnter = FALSE;
	Choice.CreateDlg ();

    Choice.SetDbClass (A_bas);
	Choice.SearchText = _T("");
	Choice.DoModal();
    if (Choice.GetState ())
    {
          QuidBez = _T("");
		  CQuidList *abl = Choice.GetSelectedText (); 
		  if (abl == NULL) return;
		  A_varb->a_varb.quid_nr = abl->quid_nr;
		  QuidBez = abl->bezeichnung;
		  Form.Show ();
    }
}

void CMat::OnTierchoice ()
{
    CChoiceTier Choice;

	Form.Get ();
    Choice.IsModal = TRUE;
	Choice.IdArrDown = IDI_HARROWDOWN;
	Choice.IdArrUp   = IDI_HARROWUP;
	Choice.IdArrNo   = IDI_HARROWNO;
    Choice.HideEnter = FALSE;
	Choice.CreateDlg ();

    Choice.SetDbClass (A_bas);
	Choice.SearchText = _T("");
	Choice.DoModal();
    if (Choice.GetState ())
    {
		  CTierList *abl = Choice.GetSelectedText (); 
		  if (abl == NULL) return;
		  A_varb->a_varb.tier = abl->tier;
		  _tcscpy (Tier.tier.tier_bz, abl->tier_bz.GetBuffer ());
		  Form.Show ();
    }
}

BOOL CMat::OnKillActive ()
{
	Form.Get ();
	return TRUE;
}

BOOL CMat::OnSetActive ()
{
	if (Basis != NULL)
	{
		Basis->Form.Get ();
	}
	EnableATyp ();
	Form.Show ();
	return TRUE;
}

BOOL CMat::ReadQuid ()
{
	Form.Get ();
	if (A_varb->a_varb.quid_nr == 0) 
	{
		QuidBez = _T("");
		Form.Show ();
		return TRUE;
	}
	Quid.quid.quid_nr = A_varb->a_varb.quid_nr;
	if (Quid.dbreadfirst () == 100)
	{
		MessageBox (_T("Die Quid-Nr ist nicht angelegt"), NULL, MB_OK | MB_ICONERROR);
		Form.Show ();
		return FALSE;
	}
	QuidBez = Quid.quid.bezeichnung;
	Form.Show ();
	return TRUE;
}

BOOL CMat::ReadTier ()
{
	Form.Get ();
	if (A_varb->a_varb.tier == 0) 
	{
		_tcscpy (Tier.tier.tier_bz,  _T(""));
		Form.Show ();
		return TRUE;
	}
	Tier.tier.tier = A_varb->a_varb.tier;
	if (Tier.dbreadfirst () == 100)
	{
		MessageBox (_T("Die Tierart ist nicht angelegt"), NULL, MB_OK | MB_ICONERROR);
		return FALSE;
	}
	Form.Show ();
	return TRUE;
}