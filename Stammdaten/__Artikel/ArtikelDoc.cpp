// ArtikelDoc.cpp : Implementierung der Klasse CArtikelDoc
//

#include "stdafx.h"
#include "Artikel.h"

#include "ArtikelDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CArtikelDoc

IMPLEMENT_DYNCREATE(CArtikelDoc, CDocument)

BEGIN_MESSAGE_MAP(CArtikelDoc, CDocument)
	ON_COMMAND(ID_FILE_SEND_MAIL, OnFileSendMail)
	ON_UPDATE_COMMAND_UI(ID_FILE_SEND_MAIL, OnUpdateFileSendMail)
END_MESSAGE_MAP()


// CArtikelDoc Erstellung/Zerst�rung

CArtikelDoc::CArtikelDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CArtikelDoc::~CArtikelDoc()
{
}

BOOL CArtikelDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CArtikelDoc Serialisierung

void CArtikelDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CArtikelDoc Diagnose

#ifdef _DEBUG
void CArtikelDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CArtikelDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CArtikelDoc-Befehle
