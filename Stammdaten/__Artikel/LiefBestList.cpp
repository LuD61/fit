#include "StdAfx.h"
#include "LiefBestList.h"

CLiefBestList::CLiefBestList(void)
{
	a_best   = _T("");
    lief     = _T("");
    adr_krz  = _T("");
	a_bz1    = _T("");
    a = 0.0; 
}

CLiefBestList::CLiefBestList(LPTSTR a_best, LPTSTR lief, LPTSTR adr_krz, LPTSTR a_bz1, double a)
{
	this->a_best  = a_best;
	this->lief    = lief;
	this->adr_krz = adr_krz;
	this->a_bz1   = a_bz1;
	this->a       = a;
}

CLiefBestList::~CLiefBestList(void)
{
}
