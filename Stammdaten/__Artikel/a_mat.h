#ifndef _A_MAT_DEF
#define _A_MAT_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_MAT {
   double         a;
   short          delstatus;
   long           mat;
};
extern struct A_MAT a_mat, a_mat_null;

#line 8 "a_mat.rh"

class A_MAT_CLASS : public DB_CLASS
{
       private :
               int acursor; 
               int del_cursora; 
               void prepare (void);
       public :
               A_MAT a_mat;
               A_MAT_CLASS () : DB_CLASS ()
               {
			acursor = -1;
			del_cursora = -1;
               }
               int dbreadfirsta ();
               int dbreada (); 
               int dbdeletea (); 
};
#endif
