#include "stdafx.h"
#include "ChoiceWG.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceWG::Sort1 = -1;
int CChoiceWG::Sort2 = -1;
int CChoiceWG::Sort3 = -1;
int CChoiceWG::Sort4 = -1;

CChoiceWG::CChoiceWG(CWnd* pParent)
        : CChoiceX(pParent)
{
}

CChoiceWG::~CChoiceWG()
{
	DestroyList ();
}

void CChoiceWG::DestroyList()
{
	for (std::vector<CWGList *>::iterator pabl = WGList.begin (); pabl != WGList.end (); ++pabl)
	{
		CWGList *abl = *pabl;
		delete abl;
	}
    WGList.clear ();
	SelectList.clear ();
}

void CChoiceWG::FillList ()
{
    long  wg;
    TCHAR wg_bz1 [50];
    TCHAR wg_bz2 [50];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Warengruppe"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Warengruppe"),        1, 150, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung 1"),  2, 250);
    SetCol (_T("Bezeichnung 2"),  3, 250);

	if (WGList.size () == 0)
	{
		DbClass->sqlout ((long *)&wg,        SQLLONG, 0);
		DbClass->sqlout ((LPTSTR)  wg_bz1,     SQLCHAR, sizeof (wg_bz1));
		DbClass->sqlout ((LPTSTR)  wg_bz2,     SQLCHAR, sizeof (wg_bz2));
		int cursor = DbClass->sqlcursor (_T("select wg, wg_bz1, wg_bz2 from wg where wg > 0"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) wg_bz1;
			CDbUniCode::DbToUniCode (wg_bz1, pos);
			pos = (LPSTR) wg_bz2;
			CDbUniCode::DbToUniCode (wg_bz2, pos);
			CWGList *abl = new CWGList (wg, wg_bz1, wg_bz2);
			WGList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CWGList *>::iterator pabl = WGList.begin (); pabl != WGList.end (); ++pabl)
	{
		CWGList *abl = *pabl;
		CString Art;
		Art.Format (_T("%d"), abl->wg);
		CString Num;
		CString Bez;
		_tcscpy (wg_bz1, abl->wg_bz1.GetBuffer ());
		_tcscpy (wg_bz2, abl->wg_bz2.GetBuffer ());

		CString LText;
		LText.Format (_T("%d %s"), abl->wg,
									  abl->wg_bz1.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Art;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (wg_bz1, i, 2);
        ret = SetItemText (wg_bz2, i, 3);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceWG::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceWG::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceWG::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceWG::SearchABz1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceWG::SearchABz2 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceWG::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchABz1 (ListBox, EditText.GetBuffer ());
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchABz2 (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceWG::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceWG::CompareProc(LPARAM lParam1,
						 		     LPARAM lParam2,
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}


void CChoiceWG::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CWGList *abl = WGList [i];

		   abl->wg = _tstol (ListBox->GetItemText (i, 1));
		   abl->wg_bz1 = ListBox->GetItemText (i, 2);
		   abl->wg_bz2 = ListBox->GetItemText (i, 3);
	}
	for (int i = 1; i <= 9; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);
}

void CChoiceWG::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = WGList [idx];
}

CWGList *CChoiceWG::GetSelectedText ()
{
	CWGList *abl = (CWGList *) SelectedRow;
	return abl;
}

void CChoiceWG::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (WGList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}
