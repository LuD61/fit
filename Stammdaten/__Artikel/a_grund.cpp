#include "stdafx.h"
#include "a_grund.h"

struct A_GRUND a_grund, a_grund_null;

void A_GRUND_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((long *)   &a_grund.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_grund.a,SQLDOUBLE,0);
    sqlout ((short *) &a_grund.rgb_r,SQLSHORT,0);
    sqlout ((short *) &a_grund.rgb_g,SQLSHORT,0);
    sqlout ((short *) &a_grund.rgb_b,SQLSHORT,0);
            cursor = sqlcursor (_T("select a_grund.a,  ")
_T("a_grund.rgb_r,  a_grund.rgb_g,  a_grund.rgb_b from a_grund ")

#line 12 "a_grund.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_grund.a,SQLDOUBLE,0);
    sqlin ((short *) &a_grund.rgb_r,SQLSHORT,0);
    sqlin ((short *) &a_grund.rgb_g,SQLSHORT,0);
    sqlin ((short *) &a_grund.rgb_b,SQLSHORT,0);
            sqltext = _T("update a_grund set a_grund.a = ?,  ")
_T("a_grund.rgb_r = ?,  a_grund.rgb_g = ?,  a_grund.rgb_b = ? ")

#line 14 "a_grund.rpp"
                                  _T("where a = ?");
            sqlin ((long *)   &a_grund.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_grund.a,  SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_grund ")
                                  _T("set a = a where a = ? "));


            sqlin ((long *)   &a_grund.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_grund ")
                                  _T("where a = ?"));
            sqlin ((long *)   &a_grund.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_grund ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_grund.a,SQLDOUBLE,0);
    sqlin ((short *) &a_grund.rgb_r,SQLSHORT,0);
    sqlin ((short *) &a_grund.rgb_g,SQLSHORT,0);
    sqlin ((short *) &a_grund.rgb_b,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into a_grund (")
_T("a,  rgb_r,  rgb_g,  rgb_b) ")

#line 30 "a_grund.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?)"));

#line 32 "a_grund.rpp"
}

