#pragma once

class CDllBild160
{
public:
	static HANDLE Bild160Lib;
    int (*bild160)(LPSTR Command); 
	void (*OpenDatabase) ();

	CDllBild160(void);
	~CDllBild160(void);
	int Send (LPSTR Command);
};
