// ProductPage1.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "ProductPage6.h"
#include "UniFormField.h"
#include "CtrlLine.h"
#include "Token.h"
#include "DbUniCode.h"


// CProductPage6-Dialogfeld

IMPLEMENT_DYNAMIC(CProductPage6, CDbPropertyPage)

CProductPage6::CProductPage6()
	: CDbPropertyPage(CProductPage6::IDD)
{
	tabx = 0;
	taby = 21;
	ProductPage6Read = FALSE;

	ChoiceWG = NULL;

	BoldColor = RGB (0,128,192);
}

CProductPage6::~CProductPage6()
{
}

void CProductPage6::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LA_BZ1, m_LA_bz1);
	DDX_Control(pDX, IDC_A_BZ01, m_a_bz01);
	DDX_Control(pDX, IDC_A_BZ02, m_a_bz02);

	DDX_Control(pDX, IDC_LVERPACKUNG, m_LVerpackung);
	DDX_Control(pDX, IDC_LINHKARTON, m_LInhKarton);
	DDX_Control(pDX, IDC_INHKARTON, m_InhKarton);
	DDX_Control(pDX, IDC_LPACKUNGKARTON, m_LPackungKarton);
	DDX_Control(pDX, IDC_PACKUNGKARTON, m_PackungKarton);
	DDX_Control(pDX, IDC_LPACK_VOL, m_LPack_Vol);
	DDX_Control(pDX, IDC_PACK_VOL, m_Pack_Vol);
	DDX_Control(pDX, IDC_LVERPACKUNG2, m_LWGShopZuordnungen);
	DDX_Control(pDX, IDC_SHOPWG1, m_SWG1);
	DDX_Control(pDX, IDC_SHOPWG2, m_SWG2);
	DDX_Control(pDX, IDC_SHOPWG3, m_SWG3);
	DDX_Control(pDX, IDC_SHOPWG4, m_SWG4);
	DDX_Control(pDX, IDC_SHOPWG5, m_SWG5);
	DDX_Control(pDX, IDC_LSWG1, m_LSWG1);
	DDX_Control(pDX, IDC_LSWG2, m_LSWG2);
	DDX_Control(pDX, IDC_LSWG3, m_LSWG3);
	DDX_Control(pDX, IDC_LSWG4, m_LSWG4);
	DDX_Control(pDX, IDC_LSWG5, m_LSWG5);
	DDX_Control(pDX, IDC_SHOPWG1BEZ, m_ShopWGBez1);
	DDX_Control(pDX, IDC_SHOPWG2BEZ, m_ShopWGBez2);
	DDX_Control(pDX, IDC_SHOPWG3BEZ, m_ShopWGBez3);
	DDX_Control(pDX, IDC_SHOPWG4BEZ, m_ShopWGBez4);
	DDX_Control(pDX, IDC_SHOPWG5BEZ, m_ShopWGBez5);
	DDX_Control(pDX, IDC_SHOPAKTION, m_ShopAktion);
	DDX_Control(pDX, IDC_SHOPTV, m_ShopTV);
	DDX_Control(pDX, IDC_LSHOPAKTION, m_LShopAktion);
	DDX_Control(pDX, IDC_LSHOPNEU, m_LShopNeu);
	DDX_Control(pDX, IDC_LSHOPTV, m_LShopTV);
	DDX_Control(pDX, IDC_LSHOPAGEW, m_LShopAGew);
	DDX_Control(pDX, IDC_SHOPAGEW, m_ShopAGew);
	DDX_Control(pDX, IDC_LLPACK, m_LLPack);
	DDX_Control(pDX, IDC_LPACK, m_LPack);
	DDX_Control(pDX, IDC_LHPACK, m_LHPack);
	DDX_Control(pDX, IDC_HPACK, m_HPack);
	DDX_Control(pDX, IDC_LBPACK, m_LBPack);
	DDX_Control(pDX, IDC_BPACK, m_BPack);
	DDX_Control(pDX, IDC_COMBOVERPACKUNG, m_ComboVerpackung);
	DDX_Control(pDX, IDC_DATETIMESHOPNEU, m_DateTimeShopNeu);
}


BEGIN_MESSAGE_MAP(CProductPage6, CPropertyPage)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_EN_CHANGE(IDC_A_BZ02, &CProductPage6::OnEnChangeABz02)
	ON_EN_CHANGE(IDC_SHOPWG1, &CProductPage6::OnEnChangeShopwg1)
	ON_BN_CLICKED(IDC_WG1CHOICE, &CProductPage6::OnBnClickedChSwg1)
	ON_BN_CLICKED(IDC_WG2CHOICE, &CProductPage6::OnBnClickedChSwg2)
	ON_BN_CLICKED(IDC_WG3CHOICE, &CProductPage6::OnBnClickedChSwg3)
	ON_BN_CLICKED(IDC_WG4CHOICE, &CProductPage6::OnBnClickedChSwg4)
	ON_BN_CLICKED(IDC_WG5CHOICE, &CProductPage6::OnBnClickedChSwg5)
	ON_EN_KILLFOCUS(IDC_LPACK, &CProductPage6::OnEnKillfocusLpack)
	ON_EN_KILLFOCUS(IDC_HPACK, &CProductPage6::OnEnKillfocusHpack)
	ON_EN_KILLFOCUS(IDC_BPACK, &CProductPage6::OnEnKillfocusBpack)
	ON_EN_KILLFOCUS(IDC_PACK_VOL, &CProductPage6::OnEnKillfocusPackVol)
	ON_CBN_SELCHANGE(IDC_COMBOVERPACKUNG, &CProductPage6::OnCbnSelchangeComboverpackung)
	ON_CBN_DBLCLK(IDC_COMBOVERPACKUNG, &CProductPage6::OnCbnDblclkComboverpackung)
	ON_BN_CLICKED(IDC_SHOPTV, &CProductPage6::OnBnClickedShoptv)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER1, &CProductPage6::OnDtnDatetimechangeDatetimepicker1)
END_MESSAGE_MAP()

BOOL CProductPage6::OnInitDialog()
{
	ProductPage6Read = TRUE;

	CPropertyPage::OnInitDialog();
	this->A_bas  = &Basis->A_bas;
	this->A_bas_erw  = &Basis->A_bas_erw;
	this->A_hndw = &Basis->A_hndw;
	this->A_eig = &Basis->A_eig;
	this->DlgBkColor = Basis->DlgBkColor;

	Basis->Form.Get ();
    Basis->ProductPage6Read = FALSE;


	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	BoldFont.CreateFontIndirect (&l);



    m_ImageArticle.Create (_T("Bild"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, CRect (0, 0, 250, 200),
		                   this, IDC_ARTIMAGE);

  	Form.Add (new CFormField (&m_DateTimeShopNeu, DATETIMEPICKER,  (DATE_STRUCT *)   &A_bas_erw->a_bas_erw.shop_neu_bis , VDATE));
    Form.Add (new CUniFormField (&m_a_bz01,EDIT,   (char *) A_bas->a_bas.a_bz1, VCHAR, sizeof (A_bas->a_bas.a_bz1) - 1));
    Form.Add (new CUniFormField (&m_a_bz02,EDIT,   (char *) A_bas->a_bas.a_bz2, VCHAR, sizeof (A_bas->a_bas.a_bz2) - 1));
	Form.Add (new CFormField (&m_InhKarton, EDIT,  (short *)    &A_bas->a_bas.inh_karton , VSHORT));
	Form.Add (new CFormField (&m_PackungKarton, EDIT,  (short *)    &A_bas->a_bas.packung_karton , VSHORT));
	Form.Add (new CFormField (&m_Pack_Vol, EDIT,  (double *)    &A_bas->a_bas.pack_vol , VDOUBLE, 8,3));
	Form.Add (new CFormField (&m_SWG1, EDIT,  (short *)    &A_bas_erw->a_bas_erw.shop_wg1 , VSHORT));
	Form.Add (new CFormField (&m_SWG2, EDIT,  (short *)    &A_bas_erw->a_bas_erw.shop_wg2 , VSHORT));
	Form.Add (new CFormField (&m_SWG3, EDIT,  (short *)    &A_bas_erw->a_bas_erw.shop_wg3 , VSHORT));
	Form.Add (new CFormField (&m_SWG4, EDIT,  (short *)    &A_bas_erw->a_bas_erw.shop_wg4 , VSHORT));
	Form.Add (new CFormField (&m_SWG5, EDIT,  (short *)    &A_bas_erw->a_bas_erw.shop_wg5 , VSHORT));	
	Form.Add (new CUniFormField (&m_ShopAktion,CHECKBOX,     (char *)   A_bas_erw->a_bas_erw.shop_aktion, VCHAR));
//	Form.Add (new CFormField (&m_DateTimeShopNeu, DATETIMEPICKER,  (DATE_STRUCT *)   &A_bas_erw->a_bas_erw.shop_neu_bis , VDATE));
	Form.Add (new CUniFormField (&m_ShopTV,CHECKBOX,     (char *)   A_bas_erw->a_bas_erw.shop_tv, VCHAR));
	Form.Add (new CFormField (&m_ShopAGew, EDIT,  (double *)    &A_bas_erw->a_bas_erw.shop_agew , VDOUBLE, 8,3));
	Form.Add (new CFormField (&m_LPack, EDIT,  (short *)    &A_bas_erw->a_bas_erw.l_pack , VSHORT));	
	Form.Add (new CFormField (&m_HPack, EDIT,  (short *)    &A_bas_erw->a_bas_erw.h_pack , VSHORT));	
	Form.Add (new CFormField (&m_BPack, EDIT,  (short *)    &A_bas_erw->a_bas_erw.b_pack , VSHORT));	
	Form.Add (new CFormField (&m_ComboVerpackung,COMBOBOX, (short *) &A_bas_erw->a_bas_erw.karton_id, VSHORT));   

	SetVerpackung ();

	FillVerpackungCombo ();


    CtrlGrid.Create (this, 40, 40);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LA_bz1  = new CCtrlInfo (&m_LA_bz1, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LA_bz1);
	CCtrlInfo *c_a_bz01  = new CCtrlInfo (&m_a_bz01, 2, 0, 1, 1); 
	CtrlGrid.Add (c_a_bz01);
	CCtrlInfo *c_a_bz02  = new CCtrlInfo (&m_a_bz02, 3, 0, 1, 1); 
	CtrlGrid.Add (c_a_bz02);

	CCtrlLine *Line1 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 2, DOCKRIGHT, 1);
	CtrlGrid.Add (Line1);

	CCtrlInfo *c_LVerpackung  = new CCtrlInfo (&m_LVerpackung, 1, 4, 1, 1); 
	CtrlGrid.Add (c_LVerpackung);

	CCtrlInfo *c_LInhKarton  = new CCtrlInfo (&m_LInhKarton, 2, 4, 1, 1); 
	CtrlGrid.Add (c_LInhKarton);
	CCtrlInfo *c_InhKarton  = new CCtrlInfo (&m_InhKarton, 3, 4, 2, 1); 
	CtrlGrid.Add (c_InhKarton);

	CCtrlInfo *c_LPackungKarton  = new CCtrlInfo (&m_LPackungKarton, 2, 5, 1, 1); 
	CtrlGrid.Add (c_LPackungKarton);
	CCtrlInfo *c_PackungKarton  = new CCtrlInfo (&m_PackungKarton, 3, 5, 2, 1); 
	CtrlGrid.Add (c_PackungKarton);

	//LAC-138
	CCtrlInfo *c_ComboVerpackung  = new CCtrlInfo (&m_ComboVerpackung, 2, 6, 2, 2); 
	CtrlGrid.Add (c_ComboVerpackung);


	CCtrlInfo *c_LLPack  = new CCtrlInfo (&m_LLPack, 2, 7, 1, 1); 
	CtrlGrid.Add (c_LLPack);
	CCtrlInfo *c_LPack  = new CCtrlInfo (&m_LPack, 3, 7, 2, 1); 
	CtrlGrid.Add (c_LPack);

	CCtrlInfo *c_LBPack  = new CCtrlInfo (&m_LBPack, 2, 8, 1, 1); 
	CtrlGrid.Add (c_LBPack);
	CCtrlInfo *c_BPack  = new CCtrlInfo (&m_BPack, 3, 8, 2, 1); 
	CtrlGrid.Add (c_BPack);

	CCtrlInfo *c_LHPack  = new CCtrlInfo (&m_LHPack, 2, 9, 1, 1); 
	CtrlGrid.Add (c_LHPack);
	CCtrlInfo *c_HPack  = new CCtrlInfo (&m_HPack, 3, 9, 2, 1); 
	CtrlGrid.Add (c_HPack);

	CCtrlInfo *c_LPack_Vol  = new CCtrlInfo (&m_LPack_Vol, 2, 10, 1, 1); 
	CtrlGrid.Add (c_LPack_Vol);
	CCtrlInfo *c_Pack_Vol  = new CCtrlInfo (&m_Pack_Vol, 3, 10, 2, 1); 
	CtrlGrid.Add (c_Pack_Vol);

	CCtrlInfo *c_LShopAGew  = new CCtrlInfo (&m_LShopAGew, 2, 11, 1, 1); 
	CtrlGrid.Add (c_LShopAGew);
	CCtrlInfo *c_ShopAGew  = new CCtrlInfo (&m_ShopAGew, 3, 11, 2, 1); 
	CtrlGrid.Add (c_ShopAGew);

	CCtrlInfo *c_LShopZuordnungen  = new CCtrlInfo (&m_LWGShopZuordnungen, 1, 13, 2, 5); 
	CtrlGrid.Add (c_LShopZuordnungen);

//Grid WG1
	WG1Grid.Create (this, 1, 2);
    WG1Grid.SetBorder (0, 0);
    WG1Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_wg1 = new CCtrlInfo (&m_SWG1, 0, 0, 1, 1);
	WG1Grid.Add (c_wg1);
	CtrlGrid.CreateChoiceButton (m_SWGButton1, IDC_WG1CHOICE, this);
	CCtrlInfo *c_WG1Choice = new CCtrlInfo (&m_SWGButton1, 1, 0, 1, 1);
	WG1Grid.Add (c_WG1Choice);

	CCtrlInfo *c_LShopWG1  = new CCtrlInfo (&m_LSWG1, 1, 14, 1, 1); 
	CtrlGrid.Add (c_LShopWG1);
	CCtrlInfo *c_CShopWG  = new CCtrlInfo (&WG1Grid, 2, 14, 2, 1); 
	CtrlGrid.Add (c_CShopWG);
	CCtrlInfo *c_CShopWGBez1  = new CCtrlInfo (&m_ShopWGBez1, 3, 14, 2, 1); 
	CtrlGrid.Add (c_CShopWGBez1);

	/**
	CCtrlInfo *c_LShopWG1  = new CCtrlInfo (&m_LSWG1, 1, 14, 1, 1); 
	CtrlGrid.Add (c_LShopWG1);
	CCtrlInfo *c_CShopWG  = new CCtrlInfo (&m_SWG1, 2, 14, 2, 1); 
	CtrlGrid.Add (c_CShopWG);
	CCtrlInfo *c_CShopWGBez1  = new CCtrlInfo (&m_ShopWGBez1, 3, 14, 2, 1); 
	CtrlGrid.Add (c_CShopWGBez1);
	CCtrlInfo *c_CShopWG1Button  = new CCtrlInfo (&m_SWGButton1, 4, 14, 2, 1); 
	CtrlGrid.Add (c_CShopWG1Button);
	**/
//Grid WG2
	WG2Grid.Create (this, 1, 2);
    WG2Grid.SetBorder (0, 0);
    WG2Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_WG2 = new CCtrlInfo (&m_SWG2, 0, 0, 1, 1);
	WG2Grid.Add (c_WG2);
	CtrlGrid.CreateChoiceButton (m_SWGButton2, IDC_WG2CHOICE, this);
	CCtrlInfo *c_WG2Choice = new CCtrlInfo (&m_SWGButton2, 1, 0, 1, 1);
	WG2Grid.Add (c_WG2Choice);

	CCtrlInfo *c_LShopWG2  = new CCtrlInfo (&m_LSWG2, 1, 15, 1, 1); 
	CtrlGrid.Add (c_LShopWG2);
	CCtrlInfo *c_CShopWG2  = new CCtrlInfo (&WG2Grid, 2, 15, 1, 1); 
	CtrlGrid.Add (c_CShopWG2);
	CCtrlInfo *c_CShopWGBez2  = new CCtrlInfo (&m_ShopWGBez2, 3, 15, 2, 1); 
	CtrlGrid.Add (c_CShopWGBez2);
//Grid WG3
	WG3Grid.Create (this, 1, 2);
    WG3Grid.SetBorder (0, 0);
    WG3Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_WG3 = new CCtrlInfo (&m_SWG3, 0, 0, 1, 1);
	WG3Grid.Add (c_WG3);
	CtrlGrid.CreateChoiceButton (m_SWGButton3, IDC_WG3CHOICE, this);
	CCtrlInfo *c_WG3Choice = new CCtrlInfo (&m_SWGButton3, 1, 0, 1, 1);
	WG3Grid.Add (c_WG3Choice);

	CCtrlInfo *c_LShopWG3  = new CCtrlInfo (&m_LSWG3, 1, 16, 1, 1); 
	CtrlGrid.Add (c_LShopWG3);
	CCtrlInfo *c_CShopWG3  = new CCtrlInfo (&WG3Grid, 2, 16, 2, 1); 
	CtrlGrid.Add (c_CShopWG3);
	CCtrlInfo *c_CShopWGBez3  = new CCtrlInfo (&m_ShopWGBez3, 3, 16, 2, 1); 
	CtrlGrid.Add (c_CShopWGBez3);

//Grid WG4
	WG4Grid.Create (this, 1, 2);
    WG4Grid.SetBorder (0, 0);
    WG4Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_WG4 = new CCtrlInfo (&m_SWG4, 0, 0, 1, 1);
	WG4Grid.Add (c_WG4);
	CtrlGrid.CreateChoiceButton (m_SWGButton4, IDC_WG4CHOICE, this);
	CCtrlInfo *c_WG4Choice = new CCtrlInfo (&m_SWGButton4, 1, 0, 1, 1);
	WG4Grid.Add (c_WG4Choice);

	CCtrlInfo *c_LShopWG4  = new CCtrlInfo (&m_LSWG4, 1, 17, 1, 1); 
	CtrlGrid.Add (c_LShopWG4);
	CCtrlInfo *c_CShopWG4  = new CCtrlInfo (&WG4Grid, 2, 17, 2, 1); 
	CtrlGrid.Add (c_CShopWG4);
	CCtrlInfo *c_CShopWGBez4  = new CCtrlInfo (&m_ShopWGBez4, 3, 17, 2, 1); 
	CtrlGrid.Add (c_CShopWGBez4);


//Grid WG5
	WG5Grid.Create (this, 1, 2);
    WG5Grid.SetBorder (0, 0);
    WG5Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_WG5 = new CCtrlInfo (&m_SWG5, 0, 0, 1, 1);
	WG5Grid.Add (c_WG5);
	CtrlGrid.CreateChoiceButton (m_SWGButton5, IDC_WG5CHOICE, this);
	CCtrlInfo *c_WG5Choice = new CCtrlInfo (&m_SWGButton5, 1, 0, 1, 1);
	WG5Grid.Add (c_WG5Choice);

	CCtrlInfo *c_LShopWG5  = new CCtrlInfo (&m_LSWG5, 1, 18, 1, 1); 
	CtrlGrid.Add (c_LShopWG5);
	CCtrlInfo *c_CShopWG5  = new CCtrlInfo (&WG5Grid, 2, 18, 2, 1); 
	CtrlGrid.Add (c_CShopWG5);
	CCtrlInfo *c_CShopWGBez5  = new CCtrlInfo (&m_ShopWGBez5, 3, 18, 2, 1); 
	CtrlGrid.Add (c_CShopWGBez5);

	CCtrlInfo *c_LShopAktion  = new CCtrlInfo (&m_LShopAktion, 1, 20, 1, 1); 
	CtrlGrid.Add (c_LShopAktion);
	CCtrlInfo *c_CShopAktion  = new CCtrlInfo (&m_ShopAktion, 2, 20, 2, 1); 
	CtrlGrid.Add (c_CShopAktion);


	CCtrlInfo *c_LShopTV  = new CCtrlInfo (&m_LShopTV, 1, 21, 1, 1); 
	CtrlGrid.Add (c_LShopTV);
	CCtrlInfo *c_CShopTV  = new CCtrlInfo (&m_ShopTV, 2, 21, 2, 1); 
	CtrlGrid.Add (c_CShopTV);

	CCtrlInfo *c_LShopNeu  = new CCtrlInfo (&m_LShopNeu, 1, 23, 1, 1); 
	CtrlGrid.Add (c_LShopNeu);
	CCtrlInfo *c_CShopNeu  = new CCtrlInfo (&m_DateTimeShopNeu, 2, 23, 2, 1); 
	CtrlGrid.Add (c_CShopNeu);

// ==== Bild =====
	CCtrlInfo *c_ImageArticle = new CCtrlInfo (&m_ImageArticle, 6, 4, 1, 1);
	CtrlGrid.Add (c_ImageArticle);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_LA_bz1.SetFont (&BoldFont);
	m_LVerpackung.SetFont (&BoldFont);
	m_LWGShopZuordnungen.SetFont (&BoldFont);





	Form.Show ();
	CtrlGrid.Display ();
	m_ImageArticle.Show (A_bas->a_bas.bild);
	PostMessage (WM_SETFOCUS, (WPARAM) m_InhKarton.m_hWnd, 0l);
	return FALSE;
}

// CProductPage1-Meldungshandler

HBRUSH CProductPage6::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
//		    if (pWnd == &m_LA_bz1)
//			{
//				pDC->SetTextColor (BoldColor);
//			}
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDbPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CProductPage6::OnSize(UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	GetWindowRect (&pRect);
	GetParent ()->ScreenToClient (&pRect);
	int pcx = pRect.right -pRect.left - tabx;
	int pcy = pRect.bottom - pRect.top - taby;
	if (cx > pcx)
	{
		pcx = cx;
	}
	if (cy > pcy)
	{
		pcy = cy;
	}
	MoveWindow (tabx, taby, pcx, pcy);
	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = pcx - 10 - tabx;
	frame.bottom = pcy - 10 - taby;
	frame.top += 20;
	frame.left += 2;
    frame.right -= 10;
	frame.bottom -= 10;
}

BOOL CProductPage6::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return FALSE;
			}

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeydown ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_SWG1)
				{
					OnChoiceWg(1);
					return TRUE;
				}
				if (GetFocus () == &m_SWG2)
				{
					OnChoiceWg(2);
					return TRUE;
				}
				if (GetFocus () == &m_SWG3)
				{
					OnChoiceWg(3);
					return TRUE;
				}
				if (GetFocus () == &m_SWG4)
				{
					OnChoiceWg(4);
					return TRUE;
				}
				if (GetFocus () == &m_SWG5)
				{
					OnChoiceWg(5);
					return TRUE;
				}
				return TRUE;
			}
	}
	return FALSE;
}

void CProductPage6::UpdatePage ()
{
	Basis->Form.Get ();
	Form.Show ();
	m_ImageArticle.Show (A_bas->a_bas.bild);
	m_ImageArticle.Invalidate ();
}

BOOL CProductPage6::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CProductPage6::OnKeydown ()
{
	CWnd *Control = GetFocus ();


	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CProductPage6::OnKeyup ()
{
	CWnd *Control = GetFocus ();


	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

void CProductPage6::OnDelete ()
{
}

BOOL CProductPage6::OnKillActive ()
{
	Form.Get ();
	return TRUE;
}

BOOL CProductPage6::OnSetActive ()
{
	if (Basis != NULL)
	{
		Basis->Form.Get ();
	}
	DlgBkColor = Basis->DlgBkColor;
    DeleteObject (DlgBrush);
    DlgBrush = NULL;
	Form.Show ();
	m_ImageArticle.Show (A_bas->a_bas.bild);
	m_ImageArticle.Invalidate ();
	ReadWGBez();
	PostMessage (WM_SETFOCUS, (WPARAM) m_InhKarton.m_hWnd, 0l);
	return TRUE;
}


void CProductPage6::FillCombo (LPTSTR Item, CWnd *control)
{
	CFormField *f = Form.GetFormField (control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CProductPage6::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CProductPage6::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

BOOL CProductPage6::Read ()
{
	if (!ProductPage6Read)
	{
		return FALSE;
	}

	Form.Show ();
	ReadWGBez();

	return TRUE;

}

void CProductPage6::OnEnChangeABz02()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDbPropertyPage::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
}



void CProductPage6::OnEnChangeShopwg1()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDbPropertyPage::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
}


void CProductPage6::OnChoiceWg(int wg)
{
	CWnd *Control = GetFocus ();

	if (ChoiceWG == NULL)
	{
		ChoiceWG = new CChoiceWG (this);
	    ChoiceWG->IsModal = TRUE;
		ChoiceWG->IdArrDown = IDI_HARROWDOWN;
		ChoiceWG->IdArrUp   = IDI_HARROWUP;
		ChoiceWG->IdArrNo   = IDI_HARROWNO;
	    ChoiceWG->HideEnter = FALSE;
		ChoiceWG->CreateDlg ();
		ChoiceWG->SingleSelection = FALSE;
	}

	ChoiceWG->DoModal();

    if (ChoiceWG->GetState ())
    {
		  CWGList *abl; 

		  CString Text = "";
		  CString bz1 = "";

		  if (ChoiceWG->SelectList.size () > 1)
		  {
				for (std::vector<CWGList *>::iterator pabl = ChoiceWG->SelectList.begin (); pabl != ChoiceWG->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					CString cWg;
					CString cBez;
					cWg.Format (_T("%hd"), abl->wg);
					Text += cWg;
					cBez = abl->wg_bz1;
					bz1 += cBez;
				}
	   	  }
		  else
		  {
			  abl = ChoiceWG->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text.Format (_T("%hd"), abl->wg);
			  bz1 =  abl->wg_bz1;
		  }

		  if ( wg == 1 ) {
				m_SWG1.SetWindowText (Text);
				m_ShopWGBez1.SetWindowText (bz1);
		  } else if ( wg == 2 ){
				m_SWG2.SetWindowText (Text);
				m_ShopWGBez2.SetWindowText (bz1);
		  } else if ( wg == 3 ){
				m_SWG3.SetWindowText (Text);
				m_ShopWGBez3.SetWindowText (bz1);
		  } else if ( wg == 4 ){
				m_SWG4.SetWindowText (Text);
				m_ShopWGBez4.SetWindowText (bz1);
		  } else {
				m_SWG5.SetWindowText (Text);
				m_ShopWGBez5.SetWindowText (bz1);
		  }	  
		  Control->SetFocus();
    }
}

void CProductPage6::OnBnClickedChSwg1()
{
	OnChoiceWg(1);
}

void CProductPage6::OnBnClickedChSwg2()
{
	OnChoiceWg(2);
}

void CProductPage6::OnBnClickedChSwg3()
{
	OnChoiceWg(3);
}

void CProductPage6::OnBnClickedChSwg4()
{
	OnChoiceWg(4);
}

void CProductPage6::OnBnClickedChSwg5()
{
	OnChoiceWg(5);
}


void CProductPage6::ReadWGBez()
{
	Wg.wg.wg = A_bas_erw->a_bas_erw.shop_wg1;
	if (Wg.dbreadfirst () == 0) {
		m_ShopWGBez1.SetWindowText (_T(Wg.wg.wg_bz1));
	} else {
		m_ShopWGBez1.SetWindowText ("");
	}
	
	Wg.wg.wg = A_bas_erw->a_bas_erw.shop_wg2;
	if (Wg.dbreadfirst () == 0) {
		m_ShopWGBez2.SetWindowText (_T(Wg.wg.wg_bz1));
	} else {
		m_ShopWGBez2.SetWindowText ("");
	}
	
	Wg.wg.wg = A_bas_erw->a_bas_erw.shop_wg3;
	if (Wg.dbreadfirst () == 0) {
		m_ShopWGBez3.SetWindowText (_T(Wg.wg.wg_bz1));	
	} else {
		m_ShopWGBez3.SetWindowText ("");
	}
	
	Wg.wg.wg = A_bas_erw->a_bas_erw.shop_wg4;
	if (Wg.dbreadfirst () == 0) {
		m_ShopWGBez4.SetWindowText (_T(Wg.wg.wg_bz1));
	} else {
		m_ShopWGBez4.SetWindowText ("");
	}
	
	Wg.wg.wg = A_bas_erw->a_bas_erw.shop_wg5;
	if (Wg.dbreadfirst () == 0) {
		m_ShopWGBez5.SetWindowText (_T(Wg.wg.wg_bz1));
	} else {
		m_ShopWGBez5.SetWindowText ("");
	}
}



void CProductPage6::OnEnKillfocusLpack()
{
	//LAC-122
	Form.Get ();
	if (A_bas_erw->a_bas_erw.b_pack > 0 && A_bas_erw->a_bas_erw.h_pack > 0  && A_bas_erw->a_bas_erw.l_pack > 0)
	{
		A_bas->a_bas.pack_vol = A_bas_erw->a_bas_erw.b_pack * A_bas_erw->a_bas_erw.h_pack * A_bas_erw->a_bas_erw.l_pack;
		m_Pack_Vol.EnableWindow(FALSE);
	}
	else
	{
		m_Pack_Vol.EnableWindow(TRUE);
	}
	Form.Show ();
}

void CProductPage6::OnEnKillfocusHpack()
{
	//LAC-122
	Form.Get ();
	if (A_bas_erw->a_bas_erw.b_pack > 0 && A_bas_erw->a_bas_erw.h_pack > 0  && A_bas_erw->a_bas_erw.l_pack > 0)
	{
		A_bas->a_bas.pack_vol = A_bas_erw->a_bas_erw.b_pack * A_bas_erw->a_bas_erw.h_pack * A_bas_erw->a_bas_erw.l_pack;
		m_Pack_Vol.EnableWindow(FALSE);
	}
	else
	{
		m_Pack_Vol.EnableWindow(TRUE);
	}
	Form.Show ();
}

void CProductPage6::OnEnKillfocusBpack()
{
	//LAC-122
	Form.Get ();
	if (A_bas_erw->a_bas_erw.b_pack > 0 && A_bas_erw->a_bas_erw.h_pack > 0  && A_bas_erw->a_bas_erw.l_pack > 0)
	{
		A_bas->a_bas.pack_vol = A_bas_erw->a_bas_erw.b_pack * A_bas_erw->a_bas_erw.h_pack * A_bas_erw->a_bas_erw.l_pack;
		m_Pack_Vol.EnableWindow(FALSE);
	}
	else
	{
		m_Pack_Vol.EnableWindow(TRUE);
	}
	Form.Show ();
}

void CProductPage6::OnEnKillfocusPackVol()
{
}


void CProductPage6::FillVerpackungCombo ()
{
	CFormField *f = Form.GetFormField (&m_ComboVerpackung);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlin ((short *) &A_bas_erw->a_bas_erw.karton_id, SQLSHORT, 0); 
		Ptabn.sqlout ((short *) &Karton.karton.id, SQLSHORT, 0); 
		Ptabn.sqlout ((char *) Karton.karton.bz, SQLCHAR, sizeof (Karton.karton.bz)); 
		int cursor = -1;
		cursor = Ptabn.sqlcursor (_T("select id,bz from karton ")
									  _T("order by id"));


		CString *ComboValue = new CString ();
		ComboValue->Format (_T("Karton ausw�hlen"));
		f->ComboValues.push_back (ComboValue); 
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Karton.karton.bz;
			CDbUniCode::DbToUniCode (Karton.karton.bz, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %s"), Karton.karton.id, Karton.karton.bz);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (A_bas_erw->a_bas_erw.karton_id);
		f->Get ();
	}
}

void CProductPage6::OnCbnSelchangeComboverpackung()
{
	Form.Get ();
	SetVerpackung ();
	Form.Show ();
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CProductPage6::SetVerpackung()
{
	Form.Add (new CFormField (&m_LPack, EDIT,  (short *)    &A_bas_erw->a_bas_erw.l_pack , VSHORT));	
	Form.Add (new CFormField (&m_HPack, EDIT,  (short *)    &A_bas_erw->a_bas_erw.h_pack , VSHORT));	
	Form.Add (new CFormField (&m_BPack, EDIT,  (short *)    &A_bas_erw->a_bas_erw.b_pack , VSHORT));	

	m_InhKarton.EnableWindow(TRUE);
	if (A_bas_erw->a_bas_erw.karton_id > 0)
	{
		Karton.karton.id = A_bas_erw->a_bas_erw.karton_id;
		if (Karton.dbreadfirst () == 0)
		{
			A_bas_erw->a_bas_erw.l_pack = (short) Karton.karton.laenge;
			A_bas_erw->a_bas_erw.b_pack = (short) Karton.karton.breite;
			A_bas_erw->a_bas_erw.h_pack = (short) Karton.karton.hoehe;
			A_bas->a_bas.pack_vol = Karton.karton.vol;
			if (Karton.karton.inh > 0.0)
			{
				if (Basis != NULL)
				{
					Basis->Form.Get ();
					A_bas->a_bas.inh_lief = Karton.karton.inh;
					A_bas->a_bas.inh_karton = (short) Karton.karton.inh;
					m_InhKarton.EnableWindow(FALSE);
					Basis->Form.Show ();
				}
			}


		}
		m_Pack_Vol.EnableWindow(FALSE);
		m_LPack.EnableWindow(FALSE);
		m_HPack.EnableWindow(FALSE);
		m_BPack.EnableWindow(FALSE);
	}
	else
	{
		if (A_bas_erw->a_bas_erw.b_pack > 0 && A_bas_erw->a_bas_erw.h_pack > 0  && A_bas_erw->a_bas_erw.l_pack > 0)
		{
			A_bas->a_bas.pack_vol = A_bas_erw->a_bas_erw.b_pack * A_bas_erw->a_bas_erw.h_pack * A_bas_erw->a_bas_erw.l_pack;
			m_LPack.EnableWindow(TRUE);
			m_BPack.EnableWindow(TRUE);
			m_HPack.EnableWindow(TRUE);
			m_Pack_Vol.EnableWindow(FALSE);
		}
		else
		{
			m_LPack.EnableWindow(TRUE);
			m_BPack.EnableWindow(TRUE);
			m_HPack.EnableWindow(TRUE);
			m_Pack_Vol.EnableWindow(TRUE);
		}
	}
	Basis->FillInhLief ();

}

void CProductPage6::OnCbnDblclkComboverpackung()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	int di = 0;
}

void CProductPage6::OnBnClickedShoptv()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CProductPage6::OnDtnDatetimechangeDatetimepicker1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;
}
