#include "StdAfx.h"
#include "PtabList.h"

CPtabList::CPtabList(void)
{
	ptwert =  _T("");
	ptbez  = _T("");
	ptbezk = _T("");
	ptwer1 =  _T("");
	ptwer2 =  _T("");
}

CPtabList::CPtabList(LPSTR ptwert, LPTSTR ptbez, LPTSTR ptbezk, LPSTR ptwer1,LPSTR ptwer2)
{
	this->ptwert = ptwert;
	this->ptbez  = ptbez;
	this->ptbezk = ptbezk;
	this->ptwer1 = ptwer1;
	this->ptwer2 = ptwer2;
}

CPtabList::~CPtabList(void)
{
}
