// ArtikelView.cpp : Implementierung der Klasse CArtikelView
//

#include "stdafx.h"
#include "Artikel.h"

#include "ArtikelDoc.h"
#include "ArtikelView.h"
#include ".\artikelview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CArtikelView

IMPLEMENT_DYNCREATE(CArtikelView, CView)

BEGIN_MESSAGE_MAP(CArtikelView, CView)
	// Standarddruckbefehle
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CArtikelView Erstellung/Zerst�rung

CArtikelView::CArtikelView()
{
	// TODO: Hier Code zum Erstellen einf�gen

}

CArtikelView::~CArtikelView()
{
}

BOOL CArtikelView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CView::PreCreateWindow(cs);
}

// CArtikelView-Zeichnung

void CArtikelView::OnDraw(CDC* /*pDC*/)
{
	CArtikelDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: Code zum Zeichnen der systemeigenen Daten hinzuf�gen
}


// CArtikelView drucken

BOOL CArtikelView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void CArtikelView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void CArtikelView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// CArtikelView Diagnose

#ifdef _DEBUG
void CArtikelView::AssertValid() const
{
	CView::AssertValid();
}

void CArtikelView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CArtikelDoc* CArtikelView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CArtikelDoc)));
	return (CArtikelDoc*)m_pDocument;
}
#endif //_DEBUG


// CArtikelView Meldungshandler

