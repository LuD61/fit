#include "stdafx.h"
#include "a_mat.h"

struct A_MAT a_mat, a_mat_null;

void A_MAT_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((long *)   &a_mat.mat,  SQLLONG, 0);
    sqlout ((double *) &a_mat.a,SQLDOUBLE,0);
    sqlout ((short *) &a_mat.delstatus,SQLSHORT,0);
    sqlout ((long *) &a_mat.mat,SQLLONG,0);
            cursor = sqlcursor (_T("select a_mat.a,  ")
_T("a_mat.delstatus,  a_mat.mat from a_mat ")

#line 12 "a_mat.rpp"
                                  _T("where mat = ?"));
    sqlin ((double *) &a_mat.a,SQLDOUBLE,0);
    sqlin ((short *) &a_mat.delstatus,SQLSHORT,0);
    sqlin ((long *) &a_mat.mat,SQLLONG,0);
            sqltext = _T("update a_mat set a_mat.a = ?,  ")
_T("a_mat.delstatus = ?,  a_mat.mat = ? ")

#line 14 "a_mat.rpp"
                                  _T("where mat = ?");
            sqlin ((long *)   &a_mat.mat,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &a_mat.mat,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select mat from a_mat ")
                                  _T("where mat = ?"));
            sqlin ((long *)   &a_mat.mat,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from a_mat ")
                                  _T("where mat = ?"));
    sqlin ((double *) &a_mat.a,SQLDOUBLE,0);
    sqlin ((short *) &a_mat.delstatus,SQLSHORT,0);
    sqlin ((long *) &a_mat.mat,SQLLONG,0);
            ins_cursor = sqlcursor (_T("insert into a_mat (a,  ")
_T("delstatus,  mat) ")

#line 25 "a_mat.rpp"
                                      _T("values ")
                                      _T("(?,?,?)"));

#line 27 "a_mat.rpp"
            sqlin ((double *)   &a_mat.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_mat.a,SQLDOUBLE,0);
    sqlout ((short *) &a_mat.delstatus,SQLSHORT,0);
    sqlout ((long *) &a_mat.mat,SQLLONG,0);
            acursor = sqlcursor (_T("select a_mat.a,  ")
_T("a_mat.delstatus,  a_mat.mat from a_mat ")

#line 29 "a_mat.rpp"
                                  _T("where a = ?"));
            sqlin ((double *)   &a_mat.a,  SQLDOUBLE, 0);
            del_cursora = sqlcursor (_T("delete from a_mat ")
                                  _T("where a = ?"));
}


int A_MAT_CLASS::dbreadfirsta ()
{
	    if (acursor == -1)
            {
                   prepare ();
            }	
            int dsqlstatus = sqlopen (acursor);
            if (dsqlstatus == 0)
            {
            	dsqlstatus = sqlfetch (acursor);
            }    
            return dsqlstatus;
}

int A_MAT_CLASS::dbreada ()
{
            return sqlfetch (acursor);
}

int A_MAT_CLASS::dbdeletea ()
{
	    if (del_cursora == -1)
            {
                   prepare ();
            }	
            return sqlexecute (del_cursora);
}
