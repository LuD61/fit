#include "stdafx.h"
#include "a_bas_erw.h"

struct A_BAS_ERW a_bas_erw, a_bas_erw_null;

void A_BAS_ERW_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_bas_erw.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_bas_erw.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas_erw.pp_a_bz1,SQLCHAR,100);
    sqlout ((TCHAR *) a_bas_erw.pp_a_bz2,SQLCHAR,100);
    sqlout ((TCHAR *) a_bas_erw.lgr_tmpr,SQLCHAR,100);
    sqlout ((TCHAR *) a_bas_erw.lupine,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas_erw.schutzgas,SQLCHAR,2);
    sqlout ((short *) &a_bas_erw.huelle,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.shop_wg1,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.shop_wg2,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.shop_wg3,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.shop_wg4,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.shop_wg5,SQLSHORT,0);
    sqlout ((double *) &a_bas_erw.tara2,SQLDOUBLE,0);
    sqlout ((short *) &a_bas_erw.a_tara2,SQLSHORT,0);
    sqlout ((double *) &a_bas_erw.tara3,SQLDOUBLE,0);
    sqlout ((short *) &a_bas_erw.a_tara3,SQLSHORT,0);
    sqlout ((double *) &a_bas_erw.tara4,SQLDOUBLE,0);
    sqlout ((short *) &a_bas_erw.a_tara4,SQLSHORT,0);
    sqlout ((double *) &a_bas_erw.salz,SQLDOUBLE,0);
    sqlout ((double *) &a_bas_erw.davonfett,SQLDOUBLE,0);
    sqlout ((double *) &a_bas_erw.davonzucker,SQLDOUBLE,0);
    sqlout ((double *) &a_bas_erw.ballaststoffe,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas_erw.shop_neu,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas_erw.shop_tv,SQLCHAR,2);
    sqlout ((double *) &a_bas_erw.shop_agew,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas_erw.a_bez,SQLCHAR,63);
    sqlout ((TCHAR *) a_bas_erw.shop_aktion,SQLCHAR,2);
    sqlout ((short *) &a_bas_erw.userdef1,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.userdef2,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.userdef3,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas_erw.zutat,SQLCHAR,2001);
    sqlout ((short *) &a_bas_erw.sg,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.minstaffgr,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.l_pack,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.b_pack,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.h_pack,SQLSHORT,0);
    sqlout ((short *) &a_bas_erw.karton_id,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &a_bas_erw.shop_neu_bis,SQLDATE,0);
            cursor = sqlcursor (_T("select a_bas_erw.a,  ")
_T("a_bas_erw.pp_a_bz1,  a_bas_erw.pp_a_bz2,  a_bas_erw.lgr_tmpr,  ")
_T("a_bas_erw.lupine,  a_bas_erw.schutzgas,  a_bas_erw.huelle,  ")
_T("a_bas_erw.shop_wg1,  a_bas_erw.shop_wg2,  a_bas_erw.shop_wg3,  ")
_T("a_bas_erw.shop_wg4,  a_bas_erw.shop_wg5,  a_bas_erw.tara2,  ")
_T("a_bas_erw.a_tara2,  a_bas_erw.tara3,  a_bas_erw.a_tara3,  ")
_T("a_bas_erw.tara4,  a_bas_erw.a_tara4,  a_bas_erw.salz,  ")
_T("a_bas_erw.davonfett,  a_bas_erw.davonzucker,  ")
_T("a_bas_erw.ballaststoffe,  a_bas_erw.shop_neu,  a_bas_erw.shop_tv,  ")
_T("a_bas_erw.shop_agew,  a_bas_erw.a_bez,  a_bas_erw.shop_aktion,  ")
_T("a_bas_erw.userdef1,  a_bas_erw.userdef2,  a_bas_erw.userdef3,  ")
_T("a_bas_erw.zutat,  a_bas_erw.sg,  a_bas_erw.minstaffgr,  ")
_T("a_bas_erw.l_pack,  a_bas_erw.b_pack,  a_bas_erw.h_pack,  ")
_T("a_bas_erw.karton_id,  a_bas_erw.shop_neu_bis from a_bas_erw ")

#line 12 "a_bas_erw.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_bas_erw.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_erw.pp_a_bz1,SQLCHAR,100);
    sqlin ((TCHAR *) a_bas_erw.pp_a_bz2,SQLCHAR,100);
    sqlin ((TCHAR *) a_bas_erw.lgr_tmpr,SQLCHAR,100);
    sqlin ((TCHAR *) a_bas_erw.lupine,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_erw.schutzgas,SQLCHAR,2);
    sqlin ((short *) &a_bas_erw.huelle,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.shop_wg1,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.shop_wg2,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.shop_wg3,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.shop_wg4,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.shop_wg5,SQLSHORT,0);
    sqlin ((double *) &a_bas_erw.tara2,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_erw.a_tara2,SQLSHORT,0);
    sqlin ((double *) &a_bas_erw.tara3,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_erw.a_tara3,SQLSHORT,0);
    sqlin ((double *) &a_bas_erw.tara4,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_erw.a_tara4,SQLSHORT,0);
    sqlin ((double *) &a_bas_erw.salz,SQLDOUBLE,0);
    sqlin ((double *) &a_bas_erw.davonfett,SQLDOUBLE,0);
    sqlin ((double *) &a_bas_erw.davonzucker,SQLDOUBLE,0);
    sqlin ((double *) &a_bas_erw.ballaststoffe,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_erw.shop_neu,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_erw.shop_tv,SQLCHAR,2);
    sqlin ((double *) &a_bas_erw.shop_agew,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_erw.a_bez,SQLCHAR,63);
    sqlin ((TCHAR *) a_bas_erw.shop_aktion,SQLCHAR,2);
    sqlin ((short *) &a_bas_erw.userdef1,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.userdef2,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.userdef3,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_erw.zutat,SQLCHAR,2001);
    sqlin ((short *) &a_bas_erw.sg,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.minstaffgr,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.l_pack,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.b_pack,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.h_pack,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.karton_id,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &a_bas_erw.shop_neu_bis,SQLDATE,0);
            sqltext = _T("update a_bas_erw set ")
_T("a_bas_erw.a = ?,  a_bas_erw.pp_a_bz1 = ?,  a_bas_erw.pp_a_bz2 = ?,  ")
_T("a_bas_erw.lgr_tmpr = ?,  a_bas_erw.lupine = ?,  ")
_T("a_bas_erw.schutzgas = ?,  a_bas_erw.huelle = ?,  ")
_T("a_bas_erw.shop_wg1 = ?,  a_bas_erw.shop_wg2 = ?,  ")
_T("a_bas_erw.shop_wg3 = ?,  a_bas_erw.shop_wg4 = ?,  ")
_T("a_bas_erw.shop_wg5 = ?,  a_bas_erw.tara2 = ?,  ")
_T("a_bas_erw.a_tara2 = ?,  a_bas_erw.tara3 = ?,  ")
_T("a_bas_erw.a_tara3 = ?,  a_bas_erw.tara4 = ?,  ")
_T("a_bas_erw.a_tara4 = ?,  a_bas_erw.salz = ?,  ")
_T("a_bas_erw.davonfett = ?,  a_bas_erw.davonzucker = ?,  ")
_T("a_bas_erw.ballaststoffe = ?,  a_bas_erw.shop_neu = ?,  ")
_T("a_bas_erw.shop_tv = ?,  a_bas_erw.shop_agew = ?,  ")
_T("a_bas_erw.a_bez = ?,  a_bas_erw.shop_aktion = ?,  ")
_T("a_bas_erw.userdef1 = ?,  a_bas_erw.userdef2 = ?,  ")
_T("a_bas_erw.userdef3 = ?,  a_bas_erw.zutat = ?,  a_bas_erw.sg = ?,  ")
_T("a_bas_erw.minstaffgr = ?,  a_bas_erw.l_pack = ?,  ")
_T("a_bas_erw.b_pack = ?,  a_bas_erw.h_pack = ?,  ")
_T("a_bas_erw.karton_id = ?,  a_bas_erw.shop_neu_bis = ? ")

#line 14 "a_bas_erw.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_bas_erw.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_bas_erw.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_bas_erw ")
                                  _T("where a = ?"));
            sqlin ((double *)   &a_bas_erw.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_bas_erw ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_bas_erw.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_erw.pp_a_bz1,SQLCHAR,100);
    sqlin ((TCHAR *) a_bas_erw.pp_a_bz2,SQLCHAR,100);
    sqlin ((TCHAR *) a_bas_erw.lgr_tmpr,SQLCHAR,100);
    sqlin ((TCHAR *) a_bas_erw.lupine,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_erw.schutzgas,SQLCHAR,2);
    sqlin ((short *) &a_bas_erw.huelle,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.shop_wg1,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.shop_wg2,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.shop_wg3,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.shop_wg4,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.shop_wg5,SQLSHORT,0);
    sqlin ((double *) &a_bas_erw.tara2,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_erw.a_tara2,SQLSHORT,0);
    sqlin ((double *) &a_bas_erw.tara3,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_erw.a_tara3,SQLSHORT,0);
    sqlin ((double *) &a_bas_erw.tara4,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_erw.a_tara4,SQLSHORT,0);
    sqlin ((double *) &a_bas_erw.salz,SQLDOUBLE,0);
    sqlin ((double *) &a_bas_erw.davonfett,SQLDOUBLE,0);
    sqlin ((double *) &a_bas_erw.davonzucker,SQLDOUBLE,0);
    sqlin ((double *) &a_bas_erw.ballaststoffe,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_erw.shop_neu,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_erw.shop_tv,SQLCHAR,2);
    sqlin ((double *) &a_bas_erw.shop_agew,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_erw.a_bez,SQLCHAR,63);
    sqlin ((TCHAR *) a_bas_erw.shop_aktion,SQLCHAR,2);
    sqlin ((short *) &a_bas_erw.userdef1,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.userdef2,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.userdef3,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_erw.zutat,SQLCHAR,2001);
    sqlin ((short *) &a_bas_erw.sg,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.minstaffgr,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.l_pack,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.b_pack,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.h_pack,SQLSHORT,0);
    sqlin ((short *) &a_bas_erw.karton_id,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &a_bas_erw.shop_neu_bis,SQLDATE,0);
            ins_cursor = sqlcursor (_T("insert into a_bas_erw (")
_T("a,  pp_a_bz1,  pp_a_bz2,  lgr_tmpr,  lupine,  schutzgas,  huelle,  shop_wg1,  ")
_T("shop_wg2,  shop_wg3,  shop_wg4,  shop_wg5,  tara2,  a_tara2,  tara3,  a_tara3,  ")
_T("tara4,  a_tara4,  salz,  davonfett,  davonzucker,  ballaststoffe,  shop_neu,  ")
_T("shop_tv,  shop_agew,  a_bez,  shop_aktion,  userdef1,  userdef2,  userdef3,  ")
_T("zutat,  sg,  minstaffgr,  l_pack,  b_pack,  h_pack,  karton_id,  shop_neu_bis) ")

#line 25 "a_bas_erw.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 27 "a_bas_erw.rpp"
}

int A_BAS_ERW_CLASS::dbreadzutat (double a)
{
            sqlin ((double *)   &a,  SQLDOUBLE, 0);
	    sqlout ((TCHAR *) a_bas_erw.zutat,SQLCHAR,2001);
         return   sqlcomm (_T("select a_bas_erw.zutat from a_bas_erw ")
                                  _T("where a = ?"));
}

int A_BAS_ERW_CLASS::dbupdatezutat (double a)
{
	    sqlin ((TCHAR *) a_bas_erw.zutat,SQLCHAR,2001);
            sqlin ((double *)   &a,  SQLDOUBLE, 0);
         return   sqlcomm (_T("update a_bas_erw set zutat = ? ")
                                  _T("where a = ?"));
}
