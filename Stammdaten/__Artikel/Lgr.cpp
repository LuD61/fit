#include "stdafx.h"
#include "lgr.h"

struct LGR lgr, lgr_null;

void LGR_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &lgr.mdn, SQLSHORT, 0);
            sqlin ((short *)    &lgr.lgr, SQLSHORT, 0);
    sqlout ((short *) &lgr.lgr,SQLSHORT,0);
    sqlout ((short *) &lgr.mdn,SQLSHORT,0);
    sqlout ((short *) &lgr.fil,SQLSHORT,0);
    sqlout ((TCHAR *) lgr.lgr_smt_kz,SQLCHAR,4);
    sqlout ((TCHAR *) lgr.lgr_bz,SQLCHAR,25);
    sqlout ((short *) &lgr.lgr_kz,SQLSHORT,0);
    sqlout ((short *) &lgr.lgr_gr,SQLSHORT,0);
    sqlout ((TCHAR *) lgr.lgr_kla,SQLCHAR,2);
    sqlout ((TCHAR *) lgr.abr_period,SQLCHAR,2);
    sqlout ((long *) &lgr.adr,SQLLONG,0);
    sqlout ((short *) &lgr.afl,SQLSHORT,0);
    sqlout ((TCHAR *) lgr.bli_kz,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &lgr.dat_ero,SQLDATE,0);
    sqlout ((double *) &lgr.fl_lad,SQLDOUBLE,0);
    sqlout ((double *) &lgr.fl_nto,SQLDOUBLE,0);
    sqlout ((double *) &lgr.fl_vk_ges,SQLDOUBLE,0);
    sqlout ((short *) &lgr.frm,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &lgr.iakv,SQLDATE,0);
    sqlout ((TCHAR *) lgr.inv_rht,SQLCHAR,2);
    sqlout ((TCHAR *) lgr.ls_abgr,SQLCHAR,2);
    sqlout ((TCHAR *) lgr.ls_kz,SQLCHAR,2);
    sqlout ((short *) &lgr.ls_sum,SQLSHORT,0);
    sqlout ((TCHAR *) lgr.pers,SQLCHAR,13);
    sqlout ((short *) &lgr.pers_anz,SQLSHORT,0);
    sqlout ((TCHAR *) lgr.pos_kum,SQLCHAR,2);
    sqlout ((TCHAR *) lgr.pr_ausw,SQLCHAR,2);
    sqlout ((TCHAR *) lgr.pr_bel_entl,SQLCHAR,2);
    sqlout ((TCHAR *) lgr.pr_lgr_kz,SQLCHAR,2);
    sqlout ((long *) &lgr.pr_lst,SQLLONG,0);
    sqlout ((TCHAR *) lgr.pr_vk_kz,SQLCHAR,2);
    sqlout ((double *) &lgr.reg_bed_theke_lng,SQLDOUBLE,0);
    sqlout ((double *) &lgr.reg_kt_lng,SQLDOUBLE,0);
    sqlout ((double *) &lgr.reg_kue_lng,SQLDOUBLE,0);
    sqlout ((double *) &lgr.reg_lng,SQLDOUBLE,0);
    sqlout ((double *) &lgr.reg_tks_lng,SQLDOUBLE,0);
    sqlout ((double *) &lgr.reg_tkt_lng,SQLDOUBLE,0);
    sqlout ((TCHAR *) lgr.smt_kz,SQLCHAR,2);
    sqlout ((short *) &lgr.sonst_einh,SQLSHORT,0);
    sqlout ((short *) &lgr.sprache,SQLSHORT,0);
    sqlout ((TCHAR *) lgr.sw_kz,SQLCHAR,2);
    sqlout ((long *) &lgr.tou,SQLLONG,0);
    sqlout ((short *) &lgr.vrs_typ,SQLSHORT,0);
    sqlout ((TCHAR *) lgr.inv_akv,SQLCHAR,2);
    sqlout ((short *) &lgr.delstatus,SQLSHORT,0);
            cursor = sqlcursor (_T("select lgr.lgr,  lgr.mdn,  ")
_T("lgr.fil,  lgr.lgr_smt_kz,  lgr.lgr_bz,  lgr.lgr_kz,  lgr.lgr_gr,  ")
_T("lgr.lgr_kla,  lgr.abr_period,  lgr.adr,  lgr.afl,  lgr.bli_kz,  lgr.dat_ero,  ")
_T("lgr.fl_lad,  lgr.fl_nto,  lgr.fl_vk_ges,  lgr.frm,  lgr.iakv,  lgr.inv_rht,  ")
_T("lgr.ls_abgr,  lgr.ls_kz,  lgr.ls_sum,  lgr.pers,  lgr.pers_anz,  ")
_T("lgr.pos_kum,  lgr.pr_ausw,  lgr.pr_bel_entl,  lgr.pr_lgr_kz,  lgr.pr_lst,  ")
_T("lgr.pr_vk_kz,  lgr.reg_bed_theke_lng,  lgr.reg_kt_lng,  ")
_T("lgr.reg_kue_lng,  lgr.reg_lng,  lgr.reg_tks_lng,  lgr.reg_tkt_lng,  ")
_T("lgr.smt_kz,  lgr.sonst_einh,  lgr.sprache,  lgr.sw_kz,  lgr.tou,  ")
_T("lgr.vrs_typ,  lgr.inv_akv,  lgr.delstatus from lgr ")

#line 13 "Lgr.rpp"
                                  _T("where mdn = ? ")
				  _T("and lgr = ? "));
    sqlin ((short *) &lgr.lgr,SQLSHORT,0);
    sqlin ((short *) &lgr.mdn,SQLSHORT,0);
    sqlin ((short *) &lgr.fil,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.lgr_smt_kz,SQLCHAR,4);
    sqlin ((TCHAR *) lgr.lgr_bz,SQLCHAR,25);
    sqlin ((short *) &lgr.lgr_kz,SQLSHORT,0);
    sqlin ((short *) &lgr.lgr_gr,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.lgr_kla,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.abr_period,SQLCHAR,2);
    sqlin ((long *) &lgr.adr,SQLLONG,0);
    sqlin ((short *) &lgr.afl,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.bli_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &lgr.dat_ero,SQLDATE,0);
    sqlin ((double *) &lgr.fl_lad,SQLDOUBLE,0);
    sqlin ((double *) &lgr.fl_nto,SQLDOUBLE,0);
    sqlin ((double *) &lgr.fl_vk_ges,SQLDOUBLE,0);
    sqlin ((short *) &lgr.frm,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &lgr.iakv,SQLDATE,0);
    sqlin ((TCHAR *) lgr.inv_rht,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.ls_abgr,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.ls_kz,SQLCHAR,2);
    sqlin ((short *) &lgr.ls_sum,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.pers,SQLCHAR,13);
    sqlin ((short *) &lgr.pers_anz,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.pos_kum,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.pr_ausw,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.pr_bel_entl,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.pr_lgr_kz,SQLCHAR,2);
    sqlin ((long *) &lgr.pr_lst,SQLLONG,0);
    sqlin ((TCHAR *) lgr.pr_vk_kz,SQLCHAR,2);
    sqlin ((double *) &lgr.reg_bed_theke_lng,SQLDOUBLE,0);
    sqlin ((double *) &lgr.reg_kt_lng,SQLDOUBLE,0);
    sqlin ((double *) &lgr.reg_kue_lng,SQLDOUBLE,0);
    sqlin ((double *) &lgr.reg_lng,SQLDOUBLE,0);
    sqlin ((double *) &lgr.reg_tks_lng,SQLDOUBLE,0);
    sqlin ((double *) &lgr.reg_tkt_lng,SQLDOUBLE,0);
    sqlin ((TCHAR *) lgr.smt_kz,SQLCHAR,2);
    sqlin ((short *) &lgr.sonst_einh,SQLSHORT,0);
    sqlin ((short *) &lgr.sprache,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.sw_kz,SQLCHAR,2);
    sqlin ((long *) &lgr.tou,SQLLONG,0);
    sqlin ((short *) &lgr.vrs_typ,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.inv_akv,SQLCHAR,2);
    sqlin ((short *) &lgr.delstatus,SQLSHORT,0);
            sqltext = _T("update lgr set lgr.lgr = ?,  ")
_T("lgr.mdn = ?,  lgr.fil = ?,  lgr.lgr_smt_kz = ?,  lgr.lgr_bz = ?,  ")
_T("lgr.lgr_kz = ?,  lgr.lgr_gr = ?,  lgr.lgr_kla = ?,  ")
_T("lgr.abr_period = ?,  lgr.adr = ?,  lgr.afl = ?,  lgr.bli_kz = ?,  ")
_T("lgr.dat_ero = ?,  lgr.fl_lad = ?,  lgr.fl_nto = ?,  lgr.fl_vk_ges = ?,  ")
_T("lgr.frm = ?,  lgr.iakv = ?,  lgr.inv_rht = ?,  lgr.ls_abgr = ?,  ")
_T("lgr.ls_kz = ?,  lgr.ls_sum = ?,  lgr.pers = ?,  lgr.pers_anz = ?,  ")
_T("lgr.pos_kum = ?,  lgr.pr_ausw = ?,  lgr.pr_bel_entl = ?,  ")
_T("lgr.pr_lgr_kz = ?,  lgr.pr_lst = ?,  lgr.pr_vk_kz = ?,  ")
_T("lgr.reg_bed_theke_lng = ?,  lgr.reg_kt_lng = ?,  ")
_T("lgr.reg_kue_lng = ?,  lgr.reg_lng = ?,  lgr.reg_tks_lng = ?,  ")
_T("lgr.reg_tkt_lng = ?,  lgr.smt_kz = ?,  lgr.sonst_einh = ?,  ")
_T("lgr.sprache = ?,  lgr.sw_kz = ?,  lgr.tou = ?,  lgr.vrs_typ = ?,  ")
_T("lgr.inv_akv = ?,  lgr.delstatus = ? ")

#line 16 "Lgr.rpp"
                                  _T("where mdn = ? ")
				  _T("and lgr = ? ");
            sqlin ((short *)   &lgr.mdn, SQLSHORT, 0);
            sqlin ((short *)    &lgr.lgr, SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &lgr.mdn, SQLSHORT, 0);
            sqlin ((short *)    &lgr.lgr, SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select lgr from lgr ")
                                  _T("where mdn = ? ")
				  _T("and lgr = ? "));
            sqlin ((short *)   &lgr.mdn, SQLSHORT, 0);
            sqlin ((short *)    &lgr.lgr, SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from lgr ")
                                  _T("where mdn = ? ")
				  _T("and lgr = ? "));
    sqlin ((short *) &lgr.lgr,SQLSHORT,0);
    sqlin ((short *) &lgr.mdn,SQLSHORT,0);
    sqlin ((short *) &lgr.fil,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.lgr_smt_kz,SQLCHAR,4);
    sqlin ((TCHAR *) lgr.lgr_bz,SQLCHAR,25);
    sqlin ((short *) &lgr.lgr_kz,SQLSHORT,0);
    sqlin ((short *) &lgr.lgr_gr,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.lgr_kla,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.abr_period,SQLCHAR,2);
    sqlin ((long *) &lgr.adr,SQLLONG,0);
    sqlin ((short *) &lgr.afl,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.bli_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &lgr.dat_ero,SQLDATE,0);
    sqlin ((double *) &lgr.fl_lad,SQLDOUBLE,0);
    sqlin ((double *) &lgr.fl_nto,SQLDOUBLE,0);
    sqlin ((double *) &lgr.fl_vk_ges,SQLDOUBLE,0);
    sqlin ((short *) &lgr.frm,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &lgr.iakv,SQLDATE,0);
    sqlin ((TCHAR *) lgr.inv_rht,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.ls_abgr,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.ls_kz,SQLCHAR,2);
    sqlin ((short *) &lgr.ls_sum,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.pers,SQLCHAR,13);
    sqlin ((short *) &lgr.pers_anz,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.pos_kum,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.pr_ausw,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.pr_bel_entl,SQLCHAR,2);
    sqlin ((TCHAR *) lgr.pr_lgr_kz,SQLCHAR,2);
    sqlin ((long *) &lgr.pr_lst,SQLLONG,0);
    sqlin ((TCHAR *) lgr.pr_vk_kz,SQLCHAR,2);
    sqlin ((double *) &lgr.reg_bed_theke_lng,SQLDOUBLE,0);
    sqlin ((double *) &lgr.reg_kt_lng,SQLDOUBLE,0);
    sqlin ((double *) &lgr.reg_kue_lng,SQLDOUBLE,0);
    sqlin ((double *) &lgr.reg_lng,SQLDOUBLE,0);
    sqlin ((double *) &lgr.reg_tks_lng,SQLDOUBLE,0);
    sqlin ((double *) &lgr.reg_tkt_lng,SQLDOUBLE,0);
    sqlin ((TCHAR *) lgr.smt_kz,SQLCHAR,2);
    sqlin ((short *) &lgr.sonst_einh,SQLSHORT,0);
    sqlin ((short *) &lgr.sprache,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.sw_kz,SQLCHAR,2);
    sqlin ((long *) &lgr.tou,SQLLONG,0);
    sqlin ((short *) &lgr.vrs_typ,SQLSHORT,0);
    sqlin ((TCHAR *) lgr.inv_akv,SQLCHAR,2);
    sqlin ((short *) &lgr.delstatus,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into lgr (lgr,  ")
_T("mdn,  fil,  lgr_smt_kz,  lgr_bz,  lgr_kz,  lgr_gr,  lgr_kla,  abr_period,  adr,  afl,  ")
_T("bli_kz,  dat_ero,  fl_lad,  fl_nto,  fl_vk_ges,  frm,  iakv,  inv_rht,  ls_abgr,  ls_kz,  ")
_T("ls_sum,  pers,  pers_anz,  pos_kum,  pr_ausw,  pr_bel_entl,  pr_lgr_kz,  pr_lst,  ")
_T("pr_vk_kz,  reg_bed_theke_lng,  reg_kt_lng,  reg_kue_lng,  reg_lng,  ")
_T("reg_tks_lng,  reg_tkt_lng,  smt_kz,  sonst_einh,  sprache,  sw_kz,  tou,  vrs_typ,  ")
_T("inv_akv,  delstatus) ")

#line 33 "Lgr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?)")); 

#line 35 "Lgr.rpp"
}

BOOL LGR_CLASS::operator== (LGR& lgr)
{
            return TRUE;
} 
