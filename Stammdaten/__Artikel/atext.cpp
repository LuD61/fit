#include "stdafx.h"
#include "atext.h"

struct ATEXTE atexte, atexte_null;

void ATEXTE_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((long *)   &atexte.txt_nr,  SQLLONG, 0);
    sqlout ((long *) &atexte.sys,SQLLONG,0);
    sqlout ((short *) &atexte.waa,SQLSHORT,0);
    sqlout ((long *) &atexte.txt_nr,SQLLONG,0);
    sqlout ((TCHAR *) atexte.txt,SQLCHAR,1025);
    sqlout ((TCHAR *) atexte.disp_txt,SQLCHAR,1025);
    sqlout ((short *) &atexte.alignment,SQLSHORT,0);
    sqlout ((short *) &atexte.send_ctrl,SQLSHORT,0);
    sqlout ((short *) &atexte.txt_typ,SQLSHORT,0);
    sqlout ((short *) &atexte.txt_platz,SQLSHORT,0);
    sqlout ((double *) &atexte.a,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select atexte.sys,  ")
_T("atexte.waa,  atexte.txt_nr,  atexte.txt,  atexte.disp_txt,  ")
_T("atexte.alignment,  atexte.send_ctrl,  atexte.txt_typ,  ")
_T("atexte.txt_platz,  atexte.a from atexte ")

#line 12 "atext.rpp"
                                  _T("where txt_nr = ? "));
    sqlin ((long *) &atexte.sys,SQLLONG,0);
    sqlin ((short *) &atexte.waa,SQLSHORT,0);
    sqlin ((long *) &atexte.txt_nr,SQLLONG,0);
    sqlin ((TCHAR *) atexte.txt,SQLCHAR,1025);
    sqlin ((TCHAR *) atexte.disp_txt,SQLCHAR,1025);
    sqlin ((short *) &atexte.alignment,SQLSHORT,0);
    sqlin ((short *) &atexte.send_ctrl,SQLSHORT,0);
    sqlin ((short *) &atexte.txt_typ,SQLSHORT,0);
    sqlin ((short *) &atexte.txt_platz,SQLSHORT,0);
    sqlin ((double *) &atexte.a,SQLDOUBLE,0);
            sqltext = _T("update atexte set atexte.sys = ?,  ")
_T("atexte.waa = ?,  atexte.txt_nr = ?,  atexte.txt = ?,  ")
_T("atexte.disp_txt = ?,  atexte.alignment = ?,  atexte.send_ctrl = ?,  ")
_T("atexte.txt_typ = ?,  atexte.txt_platz = ?,  atexte.a = ? ")

#line 14 "atext.rpp"
                                  _T("where txt_nr = ? ");
            sqlin ((long *)   &atexte.txt_nr,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &atexte.txt_nr,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select txt_nr from atexte ")
                                  _T("where txt_nr = ? "));
            sqlin ((long *)   &atexte.txt_nr,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from atexte ")
                                  _T("where txt_nr = ? "));
    sqlin ((long *) &atexte.sys,SQLLONG,0);
    sqlin ((short *) &atexte.waa,SQLSHORT,0);
    sqlin ((long *) &atexte.txt_nr,SQLLONG,0);
    sqlin ((TCHAR *) atexte.txt,SQLCHAR,1025);
    sqlin ((TCHAR *) atexte.disp_txt,SQLCHAR,1025);
    sqlin ((short *) &atexte.alignment,SQLSHORT,0);
    sqlin ((short *) &atexte.send_ctrl,SQLSHORT,0);
    sqlin ((short *) &atexte.txt_typ,SQLSHORT,0);
    sqlin ((short *) &atexte.txt_platz,SQLSHORT,0);
    sqlin ((double *) &atexte.a,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into atexte (")
_T("sys,  waa,  txt_nr,  txt,  disp_txt,  alignment,  send_ctrl,  txt_typ,  txt_platz,  a) ")

#line 25 "atext.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?)")); 

#line 27 "atext.rpp"
            sqlin ((double *)   &atexte.a,  SQLDOUBLE, 0);
    sqlout ((long *) &atexte.sys,SQLLONG,0);
    sqlout ((short *) &atexte.waa,SQLSHORT,0);
    sqlout ((long *) &atexte.txt_nr,SQLLONG,0);
    sqlout ((TCHAR *) atexte.txt,SQLCHAR,1025);
    sqlout ((TCHAR *) atexte.disp_txt,SQLCHAR,1025);
    sqlout ((short *) &atexte.alignment,SQLSHORT,0);
    sqlout ((short *) &atexte.send_ctrl,SQLSHORT,0);
    sqlout ((short *) &atexte.txt_typ,SQLSHORT,0);
    sqlout ((short *) &atexte.txt_platz,SQLSHORT,0);
    sqlout ((double *) &atexte.a,SQLDOUBLE,0);
            acursor = sqlcursor (_T("select atexte.sys,  ")
_T("atexte.waa,  atexte.txt_nr,  atexte.txt,  atexte.disp_txt,  ")
_T("atexte.alignment,  atexte.send_ctrl,  atexte.txt_typ,  ")
_T("atexte.txt_platz,  atexte.a from atexte ")

#line 29 "atext.rpp"
                                  _T("where a = ? "));
}

int ATEXTE_CLASS::dbreadfirsta ()
{
	    if (acursor == -1)
            {
                   prepare ();
            }	
            int dsqlstatus = sqlopen (acursor);
            if (dsqlstatus == 0)
            {
            	dsqlstatus = sqlfetch (acursor);
            }    
            return dsqlstatus;
}

int ATEXTE_CLASS::dbreada ()
{
            return sqlfetch (acursor);
}