// Atyp2.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "Atyp2.h"


// CAtyp2-Dialogfeld

IMPLEMENT_DYNAMIC(CAtyp2, CDialog)

CAtyp2::CAtyp2(CWnd* pParent /*=NULL*/)
	: CDialog(CAtyp2::IDD, pParent)
{
	A_bas = NULL;
	ATyp2Combo = NULL;
	a_typ_pos = 0;
	Add0 = TRUE;
}

CAtyp2::~CAtyp2()
{
}

void CAtyp2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ATYP2, m_Atyp2);
}


BEGIN_MESSAGE_MAP(CAtyp2, CDialog)
	ON_CBN_SELCHANGE(IDC_ATYP2, &CAtyp2::OnCbnSelchangeAtyp2)
END_MESSAGE_MAP()


// CAtyp2-Meldungshandler

BOOL CAtyp2::OnInitDialog()
{
	if (a_typ_pos ==0)
	{
		OnCancel ();
		return FALSE;
	}
	if (A_bas == NULL)
	{
		OnCancel ();
		return FALSE;
	}

	if (ATyp2Combo == NULL)
	{
		OnCancel ();
		return FALSE;
	}

	CDialog::OnInitDialog ();
	FillCombo ();
	return TRUE;
}

BOOL CAtyp2::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
	}
	return FALSE;
}

void CAtyp2::FillCombo ()
{

	if (Add0)
	{
		m_Atyp2.AddString (_T("0 kein 2. Artiketyp"));
	}
	for (std::vector<CString *>::iterator valp = ATyp2Combo->begin (); 
											valp != ATyp2Combo->end ();
											valp ++)
	{
		CString *value = *valp;
		m_Atyp2.AddString (value->GetBuffer ());
	}
	CString Text;
	if (a_typ_pos == 1)
	{
		Text.Format (_T("%hd"), A_bas->a_bas.a_typ2);
	}
	else
	{
		Text.Format (_T("%hd"), A_bas->a_bas.a_typ);
	}

	int idx = m_Atyp2.FindString (-1, Text.TrimRight ().GetBuffer ());
	if (idx < 0) idx = 0;
	m_Atyp2.SetCurSel (idx);
}
void CAtyp2::OnCbnSelchangeAtyp2()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	int idx = m_Atyp2.GetCurSel ();
	if (idx >= 0)
	{
		CString Text;
		m_Atyp2.GetLBText (idx, Text);
		Text.Trim ();
		if (a_typ_pos == 1)
		{
			A_bas->a_bas.a_typ2 = _tstoi (Text.GetBuffer ());
		}
		else
		{
			A_bas->a_bas.a_typ = _tstoi (Text.GetBuffer ());
		}
	}
}
