#pragma once
#include "afxwin.h"
#include "BasisDatenPage.h"
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "DbUniCode.h"
#include "afxwin.h"
#include "a_bas.h"
#include "a_bas_erw.h"
#include "prp_user.h"
#include "PrpUserListCtrl.h"
#include "FillList.h"
#include "vector"
#include "afxcmn.h"

// CProduktPage4-Dialogfeld

#define MAXENTRIES 30

class CProductPage4 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CProductPage4)
private:
	A_BAS_CLASS *A_bas;
	A_BAS_ERW_CLASS *A_bas_erw;
	PRP_USER_CLASS Prp_user;
	CFillList FillList;
	int cursor;
	int name_cursor;
	int delcursor;

public:
	int tabx;
	int taby;
	BOOL HideButtons;
	CBasisdatenPage *Basis;
	CWnd *Frame;
	CFormTab Form;
	CFormTab DisableEigs;
	CCtrlGrid CtrlGrid;
	CFont Font;
	BOOL ProductPage4Read;
	CString ZusNames[MAXENTRIES];
	char *ZusRecordNames [MAXENTRIES];
	char *ZusValues[MAXENTRIES];

	CProductPage4();
	virtual ~CProductPage4();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCT_PAGE4 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	virtual BOOL OnInitDialog();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnSize (UINT, int, int);
	virtual BOOL OnSetActive ();
	virtual BOOL OnKillActive ();
	virtual BOOL Delete ();
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()
public:
     

	CStatic m_LDescription;
public:
	CPrpUserListCtrl m_PrpUserList;
	BOOL Read ();
	BOOL Write ();
	void FillNames ();
	void ToRecordNames ();
	void FromRecordNames ();
	virtual void UpdatePage ();
    virtual void OnCopy ();
    virtual void OnPaste ();
};
