// LadenPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "LadenPage.h"
#include "UniFormField.h"
#include "PrintRegEtiDlg.h"
#include "PrintThekeEtiDlg.h"
#include "ladenpage.h"
#include "MoreTxt.h"
#include "Process.h"

// CLadenPage-Dialogfeld

IMPLEMENT_DYNAMIC(CLadenPage, CDbPropertyPage)
CLadenPage::CLadenPage()
	: CDbPropertyPage(CLadenPage::IDD)
{
	Basis = NULL;
	Mdn = NULL;
	MdnAdr = NULL;
	A_bas = NULL;
	A_hndw = NULL;
	A_eig = NULL;
	A_pfa = NULL;
	A_leer = NULL;
	Sys_par = NULL;
	ArchiveName = _T("Artikel.prp");
	Load ();
	dcs_a_par = -1;
	waa_a_par = -1;
	MaxAkrzKasse = 999999;
	MaxAkrzWaa   = 999999;
	TestAkrzCursor = -1;
	ChoiceAtxt = NULL;
}

CLadenPage::~CLadenPage()
{
	if (ChoiceAtxt != NULL)
	{
		delete ChoiceAtxt;
		ChoiceAtxt = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DisableEigs.FirstPosition ();
	while ((f = (CFormField *) DisableEigs.GetNext ()) != NULL)
	{
		delete f;
	}
	DisablePfas.FirstPosition ();
	while ((f = (CFormField *) DisablePfas.GetNext ()) != NULL)
	{
		delete f;
	}

}

void CLadenPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
	DDX_Control(pDX, IDC_A_BZ1, m_A_bz1);
	DDX_Control(pDX, IDC_A_BZ2, m_A_bz2);
	DDX_Control(pDX, IDC_LDGROUP, m_LdGroup);
	DDX_Control(pDX, IDC_LVERK_ART, m_LVerkArt);
	DDX_Control(pDX, IDC_VERL_ART, m_VerkArt);
	DDX_Control(pDX, IDC_MWST_URB, m_MwstUeb);
	DDX_Control(pDX, IDC_PRAUSZ, m_PrAusz);
	DDX_Control(pDX, IDC_PRMAN, m_PrMan);
	DDX_Control(pDX, IDC_FIL_KTRL, m_FilKtrl);
	DDX_Control(pDX, IDC_GROUPETI, m_EtiGroup);
	DDX_Control(pDX, IDC_LREGETI, m_LReg_eti);
	DDX_Control(pDX, IDC_REGETI, m_Reg_eti);
	DDX_Control(pDX, IDC_WARENETI, m_Waren_eti);
	DDX_Control(pDX, IDC_THEKEETI, m_Theke_eti);
	DDX_Control(pDX, IDC_LA_KRZ, m_LA_krz);
	DDX_Control(pDX, IDC_A_KRZ, m_A_krz);
	DDX_Control(pDX, IDC_PRUEB, m_Pr_ueb);
	DDX_Control(pDX, IDC_LSG1, m_LSg1);
	DDX_Control(pDX, IDC_SG1, m_Sg1);
	DDX_Control(pDX, IDC_LSG2, m_LSg2);
	DDX_Control(pDX, IDC_SG2, m_Sg2);
	DDX_Control(pDX, IDC_LTARA, m_LTara);
	DDX_Control(pDX, IDC_TARA, m_Tara);
	DDX_Control(pDX, IDC_LGEW_BTO, m_LGew_bto);
	DDX_Control(pDX, IDC_GEW_BTO, m_Gew_bto);
	DDX_Control(pDX, IDC_LTARA2, m_LTara2);
	DDX_Control(pDX, IDC_LTARA3, m_LTara3);
	DDX_Control(pDX, IDC_LTARA4, m_LTara4);
	DDX_Control(pDX, IDC_TARA2, m_Tara2);
	DDX_Control(pDX, IDC_TARA3, m_Tara3);
	DDX_Control(pDX, IDC_TARA4, m_Tara4);	
	DDX_Control(pDX, IDC_LA_GEW, m_LA_gew);
	DDX_Control(pDX, IDC_A_GEW, m_A_gew);
	DDX_Control(pDX, IDC_GEWGROUP, m_GewGroup);
	DDX_Control(pDX, IDC_LALLGTEXT1, m_LAllgText1);
	DDX_Control(pDX, IDC_ALLGTEXT1, m_AllgText1);
	DDX_Control(pDX, IDC_LALLGTEXT2, m_LAllgText2);
	DDX_Control(pDX, IDC_ALLGTEXT2, m_AllgText2);
	DDX_Control(pDX, IDC_LALLGTEXT3, m_LAllgText3);
	DDX_Control(pDX, IDC_ALLGTEXT3, m_AllgText3);
	DDX_Control(pDX, IDC_LALLGTEXT4, m_LAllgText4);
	DDX_Control(pDX, IDC_ALLGTEXT4, m_AllgText4);
	DDX_Control(pDX, IDC_TEXTGROUP, m_TxtGroup);
	DDX_Control(pDX, IDC_LANZ_REG_ETI, m_LAnzRegEti);
	DDX_Control(pDX, IDC_ANZ_REG_ETI, m_AnzRegEti);
	DDX_Control(pDX, IDC_LANZ_WAREN_ETI2, m_LAnzWarenEti);
	DDX_Control(pDX, IDC_ANZ_WAREN_ETI3, m_AnzWarenEti);
	DDX_Control(pDX, IDC_LANZ_THEKE_ETI, m_LAnzThekeEti);
	DDX_Control(pDX, IDC_ANZ_THEKE_ETI, m_AnzThekeEti);
	DDX_Control(pDX, IDC_SPIN_REG_ETI, m_SpinRegEti);
	DDX_Control(pDX, IDC_SPIN_WAREN_ETI, m_SpinWarenEti);
	DDX_Control(pDX, IDC_SPIN_THEKE_ETI, m_SpinThekeEti);
	DDX_Control(pDX, IDC_LVK_PR, m_LVkGr);
	DDX_Control(pDX, IDC_VK_GR, m_VkGr);
	DDX_Control(pDX, IDC_LHBK_ZTR, m_LHbkZtr);
	DDX_Control(pDX, IDC_HBK_ZTR, m_HbkZtr);
	DDX_Control(pDX, IDC_HBK_ZTR, m_HbkZtr);
	DDX_Control(pDX, IDC_LGN_PKT_GEB, m_LGnPktGeb);
	DDX_Control(pDX, IDC_GN_PKT_GEB, m_GnPktGeb);
	DDX_Control(pDX, IDC_SPIN_HBK, m_SpinHbk);
	DDX_Control(pDX, IDC_HBK_KZ, m_HbkKz);
	DDX_Control(pDX, IDC_MORETXT, m_MoreTxt);
	DDX_Control(pDX, IDC_DRUCKEREGETI, m_DruckeRegEti);
	DDX_Control(pDX, IDC_DRUCKETHEKEETI, m_DruckeThekeEti);
	DDX_Control(pDX, IDC_ENTERTXT, m_EnterTxt);
	DDX_Control(pDX, IDC_LWARENETI, m_LWarenEti);
	DDX_Control(pDX, IDC_LTHEKE_ETI, m_LThekenEti);

	DDX_Control(pDX, IDC_TARAMAT2, m_TaraMat2);
	DDX_Control(pDX, IDC_TARAMAT3, m_TaraMat3);
	DDX_Control(pDX, IDC_TARAMAT4, m_TaraMat4);
}


BEGIN_MESSAGE_MAP(CLadenPage, CDbPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_SIZE ()
	ON_BN_CLICKED(IDC_ACHOICE ,  OnAchoice)
	ON_CBN_KILLFOCUS(IDC_VERL_ART, OnCbnKillfocusVerlArt)
	ON_CBN_CLOSEUP(IDC_VERL_ART, OnCbnCloseupVerlArt)
	ON_BN_CLICKED(IDC_TXTCHOICE1 ,  OnAtxt1choice)
	ON_BN_CLICKED(IDC_TXTCHOICE2 ,  OnAtxt2choice)
	ON_BN_CLICKED(IDC_TXTCHOICE3 ,  OnAtxt3choice)
	ON_BN_CLICKED(IDC_TXTCHOICE4 ,  OnAtxt4choice)
	ON_BN_CLICKED(IDC_ENTERTXT ,    OnEnterTxt)
	ON_BN_CLICKED(IDC_MORETXT ,     OnMoreTxt)
	ON_BN_CLICKED(IDC_DRUCKEREGETI ,     PrintRegEti)
	ON_BN_CLICKED(IDC_DRUCKETHEKEETI ,     PrintThekeEti)
	ON_EN_KILLFOCUS(IDC_TARA2, &CLadenPage::OnEnKillfocusTara2)
	ON_EN_KILLFOCUS(IDC_TARA3, &CLadenPage::OnEnKillfocusTara3)
	ON_EN_KILLFOCUS(IDC_TARA4, &CLadenPage::OnEnKillfocusTara4)
	ON_EN_KILLFOCUS(IDC_TARA, &CLadenPage::OnEnKillfocusTara)
	ON_EN_KILLFOCUS(IDC_HBK_ZTR, &CLadenPage::OnEnKillfocusHbkZtr)
END_MESSAGE_MAP()


// CLadenPage-Meldungshandler

BOOL CLadenPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();


	Mdn     = &Basis->Mdn;
	MdnAdr  = &Basis->MdnAdr;
	A_bas   = &Basis->A_bas;
	A_hndw  = &Basis->A_hndw;
	A_eig   = &Basis->A_eig;
	A_pfa   = &Basis->A_pfa;
	A_leer   = &Basis->A_leer;
	A_krz   = &Basis->A_krz;
	Ag	    = &Basis->Ag;
	A_bas_erw  = &Basis->A_bas_erw;
	Sys_par = &Basis->Sys_par;


	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

//	m_Theke_eti.ShowWindow (SW_HIDE);

    HeadForm.Add (new CFormField (&m_Mdn,EDIT,       (short *) &Mdn->mdn.mdn, VSHORT));
    HeadForm.Add (new CUniFormField (&m_MdnName,EDIT,(char *) MdnAdr->adr.adr_krz, VCHAR));
    HeadForm.Add (new CFormField (&m_A,EDIT,        (double *) &A_bas->a_bas.a, VDOUBLE, 13, 0));
    HeadForm.Add (new CUniFormField (&m_A_bz1,EDIT, (char *) A_bas->a_bas.a_bz1, VCHAR));
    HeadForm.Add (new CUniFormField (&m_A_bz2,EDIT, (char *) A_bas->a_bas.a_bz2, VCHAR));
    Form.Add (new CFormField (&m_VerkArt, COMBOBOX, (char *) A_hndw->a_hndw.verk_art, VCHAR));
    Form.Add (new CFormField (&m_MwstUeb, CHECKBOX, (char *) A_hndw->a_hndw.mwst_ueb, VCHAR));
    Form.Add (new CFormField (&m_PrAusz, CHECKBOX,  (char *) A_hndw->a_hndw.pr_ausz, VCHAR));
    Form.Add (new CFormField (&m_PrMan, CHECKBOX,   (char *) A_hndw->a_hndw.pr_man, VCHAR));
    Form.Add (new CFormField (&m_FilKtrl, CHECKBOX,   (char *) &A_bas->a_bas.fil_ktrl, VSHORT));
    Form.Add (new CFormField (&m_Reg_eti, COMBOBOX, (char *) A_hndw->a_hndw.reg_eti, VCHAR));
    Form.Add (new CFormField (&m_AnzRegEti, EDIT,   (char *) &A_hndw->a_hndw.anz_reg_eti, VSHORT));
    Form.Add (new CFormField (&m_Waren_eti, COMBOBOX, (char *) A_hndw->a_hndw.waren_eti, VCHAR));
    Form.Add (new CFormField (&m_AnzWarenEti, EDIT,   (char *) &A_hndw->a_hndw.anz_waren_eti, VSHORT));
    Form.Add (new CFormField (&m_Theke_eti, COMBOBOX, (char *) A_hndw->a_hndw.theke_eti, VCHAR));
    Form.Add (new CFormField (&m_AnzThekeEti, EDIT,   (char *) &A_hndw->a_hndw.anz_theke_eti, VSHORT));
    Form.Add (new CFormField (&m_VkGr, COMBOBOX,    (char *) &A_bas->a_bas.vk_gr, VSHORT));

    Form.Add (new CFormField (&m_A_krz, EDIT,        (long *) &A_hndw->a_hndw.a_krz, VLONG));
    Form.Add (new CFormField (&m_Pr_ueb, CHECKBOX, (char *) A_hndw->a_hndw.pr_ueb, VCHAR));
    Form.Add (new CFormField (&m_Sg1, EDIT,        (short *) &A_hndw->a_hndw.sg1, VSHORT));
    Form.Add (new CFormField (&m_Sg2, EDIT,        (short *) &A_hndw->a_hndw.sg2, VSHORT));
	Form.Add (new CFormField (&m_HbkZtr, EDIT,     (short *) &A_bas->a_bas.hbk_ztr, VSHORT));
    Form.Add (new CFormField (&m_HbkKz, COMBOBOX,  (char *)  A_bas->a_bas.hbk_kz, VCHAR));
    Form.Add (new CFormField (&m_GnPktGeb, EDIT,  (double *)  &A_bas->a_bas.gn_pkt_gbr, VDOUBLE, 8, 4));
    Form.Add (new CFormField (&m_A_gew, EDIT,      (double *) &A_bas->a_bas.a_gew, VDOUBLE, 8, 3));
    Form.Add (new CFormField (&m_Tara, EDIT,       (double *) &A_hndw->a_hndw.tara, VDOUBLE, 8, 3));
	Form.Add (new CFormField (&m_Tara2, EDIT,       (double *) &A_bas_erw->a_bas_erw.tara2, VDOUBLE, 8, 3));
	Form.Add (new CFormField (&m_Tara3, EDIT,       (double *) &A_bas_erw->a_bas_erw.tara3, VDOUBLE, 8, 3));
	Form.Add (new CFormField (&m_Tara4, EDIT,       (double *) &A_bas_erw->a_bas_erw.tara4, VDOUBLE, 8, 3));
    Form.Add (new CFormField (&m_TaraMat2, COMBOBOX,(char *) &A_bas_erw->a_bas_erw.a_tara2, VSHORT));
    Form.Add (new CFormField (&m_TaraMat3, COMBOBOX,(char *) &A_bas_erw->a_bas_erw.a_tara3, VSHORT));
    Form.Add (new CFormField (&m_TaraMat4, COMBOBOX,(char *) &A_bas_erw->a_bas_erw.a_tara4, VSHORT));
    Form.Add (new CFormField (&m_Gew_bto,EDIT,     (double *) &A_hndw->a_hndw.gew_bto, VDOUBLE, 8, 3));
	Form.Add (new CFormField (&m_AllgText1, EDIT,  (long *) &A_bas->a_bas.allgtxt_nr1, VLONG));
	Form.Add (new CFormField (&m_AllgText2, EDIT,  (long *) &A_bas->a_bas.allgtxt_nr2, VLONG));
	Form.Add (new CFormField (&m_AllgText3, EDIT,  (long *) &A_bas->a_bas.allgtxt_nr3, VLONG));
	Form.Add (new CFormField (&m_AllgText4, EDIT,  (long *) &A_bas->a_bas.allgtxt_nr4, VLONG));

//    DisableEigs.Add (new CFormField (&m_Reg_eti, COMBOBOX, (char *) A_hndw->a_hndw.reg_eti, VCHAR));
    DisableEigs.Add (new CFormField (&m_Waren_eti, COMBOBOX, (char *) A_hndw->a_hndw.waren_eti, VCHAR));
    DisableEigs.Add (new CFormField (&m_AnzWarenEti, CHECKBOX, (char *) A_hndw->a_hndw.waren_eti, VCHAR));
//    DisableEigs.Add (new CFormField (&m_Theke_eti, CHECKBOX, (char *) A_hndw->a_hndw.theke_eti, VCHAR));
//    DisableEigs.Add (new CFormField (&m_Gew_bto,EDIT,      (double *) &A_hndw->a_hndw.gew_bto, VDOUBLE, 8, 3));

    DisablePfas.Add (new CFormField (&m_Waren_eti, COMBOBOX, (char *) A_hndw->a_hndw.waren_eti, VCHAR));
    DisablePfas.Add (new CFormField (&m_AnzWarenEti, CHECKBOX, (char *) A_hndw->a_hndw.waren_eti, VCHAR));
    DisablePfas.Add (new CFormField (&m_PrAusz, CHECKBOX,  (char *) A_hndw->a_hndw.pr_ausz, VCHAR));
    DisablePfas.Add (new CFormField (&m_PrMan, CHECKBOX,   (char *) A_hndw->a_hndw.pr_man, VCHAR));
    DisablePfas.Add (new CFormField (&m_FilKtrl, CHECKBOX,   (char *) &A_bas->a_bas.fil_ktrl, VSHORT));
    DisablePfas.Add (new CFormField (&m_Reg_eti, COMBOBOX, (char *) A_hndw->a_hndw.reg_eti, VCHAR));
    DisablePfas.Add (new CFormField (&m_AnzRegEti, EDIT,   (char *) &A_hndw->a_hndw.anz_reg_eti, VSHORT));
    DisablePfas.Add (new CFormField (&m_Waren_eti, COMBOBOX, (char *) A_hndw->a_hndw.waren_eti, VCHAR));
    DisablePfas.Add (new CFormField (&m_AnzWarenEti, EDIT,   (char *) &A_hndw->a_hndw.anz_waren_eti, VSHORT));
    DisablePfas.Add (new CFormField (&m_Theke_eti, COMBOBOX, (char *) A_hndw->a_hndw.theke_eti, VCHAR));
    DisablePfas.Add (new CFormField (&m_AnzThekeEti, EDIT,   (char *) &A_hndw->a_hndw.anz_theke_eti, VSHORT));
    DisablePfas.Add (new CFormField (&m_VkGr, COMBOBOX,    (char *) &A_bas->a_bas.vk_gr, VSHORT));
    DisablePfas.Add (new CFormField (&m_Pr_ueb, CHECKBOX, (char *) A_hndw->a_hndw.pr_ueb, VCHAR));

	//FS-170
    DisablePfas.Add (new CFormField (&m_Tara, EDIT,       (double *) &A_hndw->a_hndw.tara, VDOUBLE, 8, 3));
	DisablePfas.Add (new CFormField (&m_Tara2, EDIT,       (double *) &A_bas_erw->a_bas_erw.tara2, VDOUBLE, 8, 3));
	DisablePfas.Add (new CFormField (&m_Tara3, EDIT,       (double *) &A_bas_erw->a_bas_erw.tara3, VDOUBLE, 8, 3));
	DisablePfas.Add (new CFormField (&m_Tara4, EDIT,       (double *) &A_bas_erw->a_bas_erw.tara4, VDOUBLE, 8, 3));
    DisablePfas.Add (new CFormField (&m_TaraMat2, COMBOBOX,(char *) &A_bas_erw->a_bas_erw.a_tara2, VSHORT));
    DisablePfas.Add (new CFormField (&m_TaraMat3, COMBOBOX,(char *) &A_bas_erw->a_bas_erw.a_tara3, VSHORT));
    DisablePfas.Add (new CFormField (&m_TaraMat4, COMBOBOX,(char *) &A_bas_erw->a_bas_erw.a_tara4, VSHORT));

//	memcpy (&a_hndw, &A_hndw->a_hndw, sizeof (a_hndw));
	FillPtabCombo (&m_VerkArt, _T("verk_art"),FALSE);
	FillPtabCombo (&m_Reg_eti, _T("reg_eti"),FALSE);
	FillPtabCombo (&m_Theke_eti, _T("theke_eti"),FALSE);
//	FillPtabCombo (&m_Waren_eti, _T("waren_eti"));
	FillPtabCombo (&m_Waren_eti, _T("etikett"),TRUE);   // etikett , wird auch im Kundenstamm benutzt
	FillPtabCombo (&m_HbkKz, _T("hbk_kz"),FALSE);
//	memcpy (&A_hndw->a_hndw, &a_hndw, sizeof (a_hndw));
	FillPtabCombo (&m_TaraMat2, _T("a_tara"),FALSE);
	FillPtabCombo (&m_TaraMat3, _T("a_tara"),FALSE);
	FillPtabCombo (&m_TaraMat4, _T("a_tara"),FALSE);

	m_VkGr.AddString (_T("0 automatisch"));
	m_VkGr.AddString (_T("1 Preis pro 100 g"));
	m_VkGr.AddString (_T("2 Preis pro KG"));
/*
    m_MoreTxt.Create (_T("weitere Texte"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 100, 20),
		                          this, IDC_MORETXT);
    m_EnterTxt.Create (_T("Texte erfassen"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 110, 20),
		                          this, IDC_ENTERTXT);
*/

	m_MoreTxt.nID = IDC_MORETXT;
	m_MoreTxt.MoveWindow (CRect (0, 0, 75, 12));
	m_MoreTxt.SetWindowText (_T("weitere Texte"));

	m_EnterTxt.nID = IDC_ENTERTXT;
	m_EnterTxt.MoveWindow (CRect (0, 0, 80, 12));
	m_EnterTxt.SetWindowText (_T("Texte erfassen"));

	m_DruckeRegEti.nID = IDC_DRUCKEREGETI;
	m_DruckeRegEti.MoveWindow (CRect (0, 0, 75, 12));
	m_DruckeRegEti.SetWindowText (_T("  Drucken"));

	m_DruckeThekeEti.nID = IDC_DRUCKETHEKEETI;
	m_DruckeThekeEti.MoveWindow (CRect (0, 0, 75, 12));
	m_DruckeThekeEti.SetWindowText (_T("  Drucken"));

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
//    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

// Artikel mit Auswahl
	AGrid.Create (this, 1, 2);
    AGrid.SetBorder (0, 0);
    AGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 0, 0, 1, 1);
	AGrid.Add (c_A);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);

// TextNr1 mit Auswahl
	Txt1Grid.Create (this, 1, 2);
    Txt1Grid.SetBorder (0, 0);
    Txt1Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_AllgText1 = new CCtrlInfo (&m_AllgText1, 0, 0, 1, 1);
	Txt1Grid.Add (c_AllgText1);
	CtrlGrid.CreateChoiceButton (m_AllgTextChoice1, IDC_TXTCHOICE1, this);
	CCtrlInfo *c_AllgTextChoice1 = new CCtrlInfo (&m_AllgTextChoice1, 1, 0, 1, 1);
	Txt1Grid.Add (c_AllgTextChoice1);

// TextNr2 mit Auswahl
	Txt2Grid.Create (this, 1, 2);
    Txt2Grid.SetBorder (0, 0);
    Txt2Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_AllgText2 = new CCtrlInfo (&m_AllgText2, 0, 0, 1, 1);
	Txt2Grid.Add (c_AllgText2);
	CtrlGrid.CreateChoiceButton (m_AllgTextChoice2, IDC_TXTCHOICE2, this);
	CCtrlInfo *c_AllgTextChoice2 = new CCtrlInfo (&m_AllgTextChoice2, 1, 0, 1, 1);
	Txt2Grid.Add (c_AllgTextChoice2);

// TextNr3 mit Auswahl
	Txt3Grid.Create (this, 1, 2);
    Txt3Grid.SetBorder (0, 0);
    Txt3Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_AllgText3 = new CCtrlInfo (&m_AllgText3, 0, 0, 1, 1);
	Txt3Grid.Add (c_AllgText3);
	CtrlGrid.CreateChoiceButton (m_AllgTextChoice3, IDC_TXTCHOICE3, this);
	CCtrlInfo *c_AllgTextChoice3 = new CCtrlInfo (&m_AllgTextChoice3, 1, 0, 1, 1);
	Txt3Grid.Add (c_AllgTextChoice3);

// TextNr4 mit Auswahl
	Txt4Grid.Create (this, 1, 2);
    Txt4Grid.SetBorder (0, 0);
    Txt4Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_AllgText4 = new CCtrlInfo (&m_AllgText4, 0, 0, 1, 1);
	Txt4Grid.Add (c_AllgText4);
	CtrlGrid.CreateChoiceButton (m_AllgTextChoice4, IDC_TXTCHOICE4, this);
	CCtrlInfo *c_AllgTextChoice4 = new CCtrlInfo (&m_AllgTextChoice4, 1, 0, 1, 1);
	Txt4Grid.Add (c_AllgTextChoice4);


//Grid Etiketten
	 EtiGrid.Create (this, 10, 10);
     EtiGrid.SetBorder (0, 0);
     EtiGrid.SetCellHeight (15);
//     EtiGrid.SetFontCellHeight (this);
     EtiGrid.SetFontCellHeight (this, &Font);
     EtiGrid.SetGridSpace (5, 8);
// Rahmen Etiketten
	CCtrlInfo *c_EtiGroup = new CCtrlInfo (&m_EtiGroup, 0, 0, 4, 8);
	EtiGrid.Add (c_EtiGroup);

// Regaletikett
	CCtrlInfo *c_LReg_eti = new CCtrlInfo (&m_LReg_eti, 1, 1, 1, 1);
	EtiGrid.Add (c_LReg_eti);
	CCtrlInfo *c_Reg_eti = new CCtrlInfo (&m_Reg_eti, 2, 1, 4, 4);
	EtiGrid.Add (c_Reg_eti);
	CCtrlInfo *c_LAnzRegEti = new CCtrlInfo (&m_LAnzRegEti, 1, 2, 1, 1);
	EtiGrid.Add (c_LAnzRegEti);
	CCtrlInfo *c_AnzRegEti = new CCtrlInfo (&m_AnzRegEti, 2, 2, 2, 2);
	EtiGrid.Add (c_AnzRegEti);
	CCtrlInfo *c_DruckeRegEti = new CCtrlInfo (&m_DruckeRegEti, 3, 2, 3, 3);
	EtiGrid.Add (c_DruckeRegEti);
// Thekenetikett
	CCtrlInfo *c_LTheke_eti = new CCtrlInfo (&m_LThekenEti, 1, 3, 1, 1);
	EtiGrid.Add (c_LTheke_eti);
	CCtrlInfo *c_Theke_eti = new CCtrlInfo (&m_Theke_eti, 2, 3, 4, 4);
	EtiGrid.Add (c_Theke_eti);
	CCtrlInfo *c_LAnzThekeEti = new CCtrlInfo (&m_LAnzThekeEti, 1, 4, 1, 1);
	EtiGrid.Add (c_LAnzThekeEti);
	CCtrlInfo *c_AnzThekeEti = new CCtrlInfo (&m_AnzThekeEti, 2, 4, 1, 1);
	EtiGrid.Add (c_AnzThekeEti);
	CCtrlInfo *c_DruckeThekeEti = new CCtrlInfo (&m_DruckeThekeEti, 3, 4, 3, 3);
	EtiGrid.Add (c_DruckeThekeEti);

// Warenetikett
	CCtrlInfo *c_LWaren_eti = new CCtrlInfo (&m_LWarenEti, 1, 5, 1, 1);
	EtiGrid.Add (c_LWaren_eti);
	CCtrlInfo *c_Waren_eti = new CCtrlInfo (&m_Waren_eti, 2, 5, 4, 4);
	EtiGrid.Add (c_Waren_eti);
	CCtrlInfo *c_LAnzWarenEti = new CCtrlInfo (&m_LAnzWarenEti, 1, 6, 1, 1);
	EtiGrid.Add (c_LAnzWarenEti);
	CCtrlInfo *c_AnzWarenEti = new CCtrlInfo (&m_AnzWarenEti, 2, 6, 1, 1);
	EtiGrid.Add (c_AnzWarenEti);


	CCtrlInfo *c_LVkGr = new CCtrlInfo (&m_LVkGr, 1, 7, 1, 1);
	EtiGrid.Add (c_LVkGr);
	CCtrlInfo *c_VkGr = new CCtrlInfo (&m_VkGr, 2, 7, 4, 4);
	EtiGrid.Add (c_VkGr);

//Grid Laden
	LadenGrid.Create (this, 10, 10);
    LadenGrid.SetBorder (0, 0);
    LadenGrid.SetCellHeight (15);
//    LadenGrid.SetFontCellHeight (this);
    LadenGrid.SetFontCellHeight (this, &Font);
    LadenGrid.SetGridSpace (5, 8);

// Rahmen Laden
	CCtrlInfo *c_LdGroup = new CCtrlInfo (&m_LdGroup, 0, 0, DOCKRIGHT, 8);
	c_LdGroup->rightspace = 0;
	LadenGrid.Add (c_LdGroup);

// Verkaufsart
	CCtrlInfo *c_LVerkArt = new CCtrlInfo (&m_LVerkArt, 1, 1, 1, 1);
	LadenGrid.Add (c_LVerkArt);
	CCtrlInfo *c_VerkArt = new CCtrlInfo (&m_VerkArt, 2, 1, 1, 1);
	LadenGrid.Add (c_VerkArt);

// Mehrwertsteuer�berschreibung
	CCtrlInfo *c_MwstUeb = new CCtrlInfo (&m_MwstUeb, 2, 3, 1, 1);
	LadenGrid.Add (c_MwstUeb);

// Preisauszeichnung
	CCtrlInfo *c_PrAusz = new CCtrlInfo (&m_PrAusz, 2, 4, 1, 1);
	LadenGrid.Add (c_PrAusz);

// Preise manuell
	CCtrlInfo *c_PrMan = new CCtrlInfo (&m_PrMan, 2, 5, 1, 1);
	LadenGrid.Add (c_PrMan);

// Filialkontrollrechnung
	CCtrlInfo *c_FilKtrl = new CCtrlInfo (&m_FilKtrl, 2, 6, 1, 1);
	LadenGrid.Add (c_FilKtrl);

	CCtrlInfo *c_EtiGrid = new CCtrlInfo (&EtiGrid, 1, 7, 5, 6);
	LadenGrid.Add (c_EtiGrid);

// Kurznummer
	CCtrlInfo *c_LA_krz = new CCtrlInfo (&m_LA_krz, 3, 1, 1, 1);
	LadenGrid.Add (c_LA_krz);
	CCtrlInfo *c_A_krz = new CCtrlInfo (&m_A_krz, 4, 1, 1, 1);
	LadenGrid.Add (c_A_krz);

//Preis�berschreibung
	CCtrlInfo *c_Pr_ueb = new CCtrlInfo (&m_Pr_ueb, 4, 2, 1, 1);
	LadenGrid.Add (c_Pr_ueb);

// Schriftgr��e
	CCtrlInfo *c_LSg1 = new CCtrlInfo (&m_LSg1, 3, 3, 1, 1);
	LadenGrid.Add (c_LSg1);
	CCtrlInfo *c_A_Sg1 = new CCtrlInfo (&m_Sg1, 4, 3, 1, 1);
	LadenGrid.Add (c_A_Sg1);
	CCtrlInfo *c_LSg2 = new CCtrlInfo (&m_LSg2, 3, 4, 1, 1);
	LadenGrid.Add (c_LSg2);
	CCtrlInfo *c_Sg2 = new CCtrlInfo (&m_Sg2, 4, 4, 1, 1);
	LadenGrid.Add (c_Sg2);

	CCtrlInfo *c_LHbkZtr = new CCtrlInfo (&m_LHbkZtr, 3, 5, 1, 1);
	LadenGrid.Add (c_LHbkZtr);

//Grid HBK
	 HbkGrid.Create (this, 2, 2);
     HbkGrid.SetBorder (0, 0);
     HbkGrid.SetCellHeight (15);
     HbkGrid.SetFontCellHeight (this);
     HbkGrid.SetFontCellHeight (this, &Font);
     HbkGrid.SetGridSpace (20, 8);

	 CCtrlInfo *c_HbkZtr = new CCtrlInfo (&m_HbkZtr, 0, 0, 1, 1);
	 HbkGrid.Add (c_HbkZtr);
	 CCtrlInfo *c_HbkKz = new CCtrlInfo (&m_HbkKz, 1, 0, 1, 1);
	 HbkGrid.Add (c_HbkKz);

	 CCtrlInfo *c_HbkGrid = new CCtrlInfo (&HbkGrid, 4, 5, 2, 1);
	 LadenGrid.Add (c_HbkGrid);

	CCtrlInfo *c_LGnPktGeb = new CCtrlInfo (&m_LGnPktGeb, 3, 6, 1, 1);
	LadenGrid.Add (c_LGnPktGeb);
	CCtrlInfo *c_GnPktGeb = new CCtrlInfo (&m_GnPktGeb, 4, 6, 1, 1);
	LadenGrid.Add (c_GnPktGeb);


//Grid Gewichte
	 GewGrid.Create (this, 10, 10);
     GewGrid.SetBorder (0, 0);
     GewGrid.SetCellHeight (15);
     GewGrid.SetFontCellHeight (this);
     GewGrid.SetFontCellHeight (this, &Font);
     GewGrid.SetGridSpace (5, 8);

// Rahmen Gewichte
	CCtrlInfo *c_GewGroup = new CCtrlInfo (&m_GewGroup, 0, 0, 4, 8);
	GewGrid.Add (c_GewGroup);

// Artikelgewicht
	CCtrlInfo *c_LA_gew = new CCtrlInfo (&m_LA_gew, 1, 1, 1, 1);
	GewGrid.Add (c_LA_gew);
	CCtrlInfo *c_A_gew = new CCtrlInfo (&m_A_gew, 2, 1, 1, 1);
	GewGrid.Add (c_A_gew);

// Tara
	CCtrlInfo *c_LTara = new CCtrlInfo (&m_LTara, 1, 2, 1, 1);
	GewGrid.Add (c_LTara);
	CCtrlInfo *c_Tara = new CCtrlInfo (&m_Tara, 2, 2, 1, 1);
	GewGrid.Add (c_Tara);

// Bruttogewicht
	CCtrlInfo *c_LGew_bto = new CCtrlInfo (&m_LGew_bto, 1, 3, 1, 1);
	GewGrid.Add (c_LGew_bto);
	CCtrlInfo *c_Gew_bto = new CCtrlInfo (&m_Gew_bto, 2, 3, 1, 1);
	GewGrid.Add (c_Gew_bto);

// Tara weitere Verpackungen
	CCtrlInfo *c_LTara2 = new CCtrlInfo (&m_LTara2, 1, 5, 1, 1);
	GewGrid.Add (c_LTara2);
	CCtrlInfo *c_Tara2 = new CCtrlInfo (&m_Tara2, 2, 5, 1, 1);
	GewGrid.Add (c_Tara2);
	CCtrlInfo *c_TaraMat2 = new CCtrlInfo (&m_TaraMat2, 3, 5, 1, 1);
	GewGrid.Add (c_TaraMat2);

	CCtrlInfo *c_LTara3 = new CCtrlInfo (&m_LTara3, 1, 6, 1, 1);
	GewGrid.Add (c_LTara3);
	CCtrlInfo *c_Tara3 = new CCtrlInfo (&m_Tara3, 2, 6, 1, 1);
	GewGrid.Add (c_Tara3);
	CCtrlInfo *c_TaraMat3 = new CCtrlInfo (&m_TaraMat3, 3, 6, 1, 1);
	GewGrid.Add (c_TaraMat3);

	CCtrlInfo *c_LTara4 = new CCtrlInfo (&m_LTara4, 1, 7, 1, 1);
	GewGrid.Add (c_LTara4);
	CCtrlInfo *c_Tara4 = new CCtrlInfo (&m_Tara4, 2, 7, 1, 1);
	GewGrid.Add (c_Tara4);
	CCtrlInfo *c_TaraMat4 = new CCtrlInfo (&m_TaraMat4, 3, 7, 1, 1);
	GewGrid.Add (c_TaraMat4);


//	CCtrlInfo *c_GewGrid = new CCtrlInfo (&GewGrid, 3, 7, 6, 4);
//	LadenGrid.Add (c_GewGrid);
	CCtrlInfo *c_GewGrid = new CCtrlInfo (&GewGrid, 5, 0, 1, 4);
	EtiGrid.Add (c_GewGrid);

//Grid Texte
	 TxtGrid.Create (this, 10, 10);
     TxtGrid.SetBorder (0, 0);
     TxtGrid.SetCellHeight (15);
//     TxtGrid.SetFontCellHeight (this);
     TxtGrid.SetFontCellHeight (this, &Font);
     TxtGrid.SetGridSpace (5, 8);

// Rahmen Texte
	CCtrlInfo *c_TxtGroup = new CCtrlInfo (&m_TxtGroup, 0, 0, 5, 8);
	TxtGrid.Add (c_TxtGroup);

// Text1
	CCtrlInfo *c_LAllgText1 = new CCtrlInfo (&m_LAllgText1, 1, 1, 1, 1);
	TxtGrid.Add (c_LAllgText1);
	CCtrlInfo *c_Txt1Grid = new CCtrlInfo (&Txt1Grid, 2, 1, 1, 1);
	TxtGrid.Add (c_Txt1Grid);
// Text2
	CCtrlInfo *c_LAllgText2 = new CCtrlInfo (&m_LAllgText2, 1, 2, 1, 1);
	TxtGrid.Add (c_LAllgText2);
	CCtrlInfo *c_Txt2Grid = new CCtrlInfo (&Txt2Grid, 2, 2, 1, 1);
	TxtGrid.Add (c_Txt2Grid);
// Text3
	CCtrlInfo *c_LAllgText3 = new CCtrlInfo (&m_LAllgText3, 1, 3, 1, 1);
	TxtGrid.Add (c_LAllgText3);
	CCtrlInfo *c_Txt3Grid = new CCtrlInfo (&Txt3Grid, 2, 3, 1, 1);
	TxtGrid.Add (c_Txt3Grid);
// Text4
	CCtrlInfo *c_LAllgText4 = new CCtrlInfo (&m_LAllgText4, 1, 4, 1, 1);
	TxtGrid.Add (c_LAllgText4);
	CCtrlInfo *c_Txt4Grid = new CCtrlInfo (&Txt4Grid, 2, 4, 1, 1);
	TxtGrid.Add (c_Txt4Grid);

	CCtrlInfo *c_MoreTxt = new CCtrlInfo (&m_MoreTxt, 1, 6, 1, 1);
	TxtGrid.Add (c_MoreTxt);

	CCtrlInfo *c_EnterTxt = new CCtrlInfo (&m_EnterTxt, 1, 7, 1, 1);
	TxtGrid.Add (c_EnterTxt);

	CCtrlInfo *c_TxtGrid = new CCtrlInfo (&TxtGrid, 8, 0, 1, 4);
	EtiGrid.Add (c_TxtGrid);

//Mandant
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_Mdn  = new CCtrlInfo (&m_Mdn, 2, 0, 1, 1); 
	CtrlGrid.Add (c_Mdn);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 3, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

//Artikel
	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 1, 1, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 2, 1, 1, 1); 
	CtrlGrid.Add (c_AGrid);
//	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 2, 1, 1, 1); 
//	CtrlGrid.Add (c_A);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 3, 1, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 4, 1, 4, 1); 
	CtrlGrid.Add (c_A_bz2);

	CCtrlInfo *c_LadenGrid = new CCtrlInfo (&LadenGrid, 0, 3, 5, 5);
	CtrlGrid.Add (c_LadenGrid);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_SpinRegEti.SetRange32 (0, 1000);
//	m_SpinRegEti.SetPos (1);
	m_SpinWarenEti.SetRange32 (0, 1000);
//	m_SpinWarenEti.SetPos (1);
	m_SpinThekeEti.SetRange32 (0, 1000);

	m_SpinHbk.SetRange32 (0, 1000);

    CtrlGrid.Display (); 
	Form.Show ();
	HeadForm.Show ();
	m_SpinRegEti.SetBuddy (&m_AnzRegEti);
	m_SpinWarenEti.SetBuddy (&m_AnzWarenEti);
	m_SpinThekeEti.SetBuddy (&m_AnzThekeEti);
	m_SpinHbk.SetBuddy (&m_HbkZtr);
    EnableTyp ();
	TestAkrz ();

//FS-167
	if (A_bas_erw->a_bas_erw.tara2 > 0.0 || A_bas_erw->a_bas_erw.tara3 > 0.0  || A_bas_erw->a_bas_erw.tara4 > 0.0)
	{
		A_hndw->a_hndw.tara = A_bas_erw->a_bas_erw.tara2 + A_bas_erw->a_bas_erw.tara3 + A_bas_erw->a_bas_erw.tara4;
		m_Tara.EnableWindow (FALSE);
		m_Tara2.EnableWindow (TRUE);
		m_Tara3.EnableWindow (TRUE);
		m_Tara4.EnableWindow (TRUE);
	}
	else
	{
		m_Tara.EnableWindow (TRUE);
		if (A_hndw->a_hndw.tara > 0.0)
		{
			m_Tara2.EnableWindow (FALSE);
			m_Tara3.EnableWindow (FALSE);
			m_Tara4.EnableWindow (FALSE);
		}
	}


	return TRUE;
}

void CLadenPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
/*
		if (IsWindow (m_AnzRegEti.m_hWnd))
		{
			m_SpinRegEti.SetBuddy (&m_AnzRegEti);
			m_SpinWarenEti.SetBuddy (&m_AnzWarenEti);
			m_SpinThekeEti.SetBuddy (&m_AnzThekeEti);
		}
*/
}

HBRUSH CLadenPage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			m_MoreTxt.SetBkColor (DlgBkColor);
			m_DruckeRegEti.SetBkColor (DlgBkColor);
			m_DruckeThekeEti.SetBkColor (DlgBkColor);
			m_EnterTxt.SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CLadenPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					if (OnReturn ())
					{
						return TRUE;
					}
				}
				else
				{
					if (OnKeyup ())
					{
						return TRUE;
					}
				}
				break;
			}

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Update->Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return FALSE;
}


void CLadenPage::UpdatePage ()
{
	TestAkrz ();
	Form.Show ();
	HeadForm.Show ();
    EnableTyp ();
}

void CLadenPage::GetPage ()
{
	Form.Get ();
	TestAkrz ();
}

void CLadenPage::FillPtabCombo (CWnd *Control, LPTSTR Item, BOOL blang)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 

		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			CString *ComboValue = new CString ();
			if (blang == FALSE)
			{
				pos = (LPSTR) Ptabn.ptabn.ptbezk;
				CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
				ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
			}
			else
			{
				pos = (LPSTR) Ptabn.ptabn.ptbez;
				ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
			}
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
//		f->SetSel (0);
//		f->Get ();
	}
}


BOOL CLadenPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_AllgText4)
	{
		m_MoreTxt.SetFocus ();
		return TRUE;
	}

	if (Control == &m_MoreTxt)
	{
		m_EnterTxt.SetFocus ();
		return TRUE;
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CLadenPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_MoreTxt)
	{
		m_AllgText4.SetFocus ();
		return TRUE;
	}

	if (Control == &m_EnterTxt)
	{
		m_MoreTxt.SetFocus ();
		return TRUE;
	}

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

void CLadenPage::OnAchoice ()
{
	Basis->OnAchoice ();
}

void CLadenPage::EnableTyp ()
{
	DisableEigs.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) DisableEigs.GetNext ()) != NULL)
	{
		f->Control->EnableWindow (TRUE);
	}
	DisablePfas.FirstPosition ();
	while ((f = (CFormField *) DisablePfas.GetNext ()) != NULL)
	{
		f->Control->EnableWindow (TRUE);
	}

	if (A_bas->a_bas.a_typ == Basis->Eig)
	{
		DisableEigs.FirstPosition ();
		while ((f = (CFormField *) DisablePfas.GetNext ()) != NULL)
		{
			f->Control->EnableWindow (FALSE);
		}
	}
	else if (A_bas->a_bas.a_typ == Basis->Pfa)
	{
		DisablePfas.FirstPosition ();
		while ((f = (CFormField *) DisablePfas.GetNext ()) != NULL)
		{
			f->Control->EnableWindow (FALSE);
		}
	}
	else if (A_bas->a_bas.a_typ == Basis->Leer)
	{
		DisablePfas.FirstPosition ();
		while ((f = (CFormField *) DisablePfas.GetNext ()) != NULL)
		{
			f->Control->EnableWindow (FALSE);
		}
	}

}

void CLadenPage::TestAkrz ()
{
	if (dcs_a_par == -1)
	{
		_tcscpy (sys_par.sys_par_nam, "dcs_a_par");
		if (Basis->Sys_par.dbreadfirst () == 0)
		{
			dcs_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}
	if (waa_a_par == -1)
	{
		_tcscpy (sys_par.sys_par_nam, "waa_a_par");
		if (Basis->Sys_par.dbreadfirst () == 0)
		{
			waa_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}

	if (A_hndw->a_hndw.verk_art[0] == _T('K'))
	{
		if (Ag->ag.a_krz_kz[0] !=  _T('J'))
		{
			TestAkrzKasse ();
		}
		else
		{
			TestAkrzWaage ();
		}
	}
	else if (A_hndw->a_hndw.verk_art[0] == _T('B'))
	{
		TestAkrzWaage ();
	}
	else if (A_hndw->a_hndw.verk_art[0] == _T('S'))
	{
		TestAkrzWaage ();
	}
	else if (A_hndw->a_hndw.verk_art[0] == _T('C'))
	{
		TestAkrzWaage ();
	}
}

void CLadenPage::TestAkrzKasse ()
{
	if (dcs_a_par == NoKrz) 
	{
		m_LA_krz.ShowWindow (SW_HIDE);
		m_A_krz.ShowWindow (SW_HIDE);
		return;
	}
	m_LA_krz.ShowWindow (SW_SHOWNORMAL);
	m_A_krz.ShowWindow (SW_SHOWNORMAL);
	if (dcs_a_par == Auto) 
	{
		m_A_krz.EnableWindow (FALSE); 
	}
	else
	{
		m_A_krz.EnableWindow (TRUE); 
	}

	if (A_hndw->a_hndw.a_krz != 0l) return;

	A_krz->a_krz.a = A_bas->a_bas.a;
	int dsqlstatus = A_krz->dbreadafirst ();
	if (dsqlstatus == 0 && strcmp ((LPSTR) A_krz->a_krz.herk_kz, "1") != 0)
	{
		if (EanEmbExist ())
		{
			return;
		}
		A_krz->dbdelete ();
		strcpy (A_krz->a_krz.herk_kz, "1");
		A_krz->a_krz.a_herk = A_krz->a_krz.a;
	}
	dsqlstatus = A_krz->dbreada ();
	if (dsqlstatus == 0 && strcmp ((LPSTR) A_krz->a_krz.herk_kz, "1") != 0)
	{
		if (EanEmbExist ())
		{
			return;
		}
		A_krz->dbdelete ();
		strcpy (A_krz->a_krz.herk_kz, "1");
		A_krz->a_krz.a_herk = A_krz->a_krz.a;
	}

	if (dcs_a_par == Hand)
	{
		if (A_bas->a_bas.a < (long) MaxAkrzKasse)
		{
			A_hndw->a_hndw.a_krz = (long) A_bas->a_bas.a;
		}
	}

//	Basis->AutoNr.GenAutoNr (0, 0, _T("a_krz"));
//	A_hndw->a_hndw.a_krz = Basis->AutoNr.Nr;
	GenAkrz ();
	Basis->a_krz_gen = A_hndw->a_hndw.a_krz;
	Form.Show ();
	A_bas->beginwork ();
}

void CLadenPage::GenAkrz ()
{
	if (TestAkrzCursor == -1)
	{
		A_hndw->sqlin ((long *)   &A_hndw->a_hndw.a_krz, SQLLONG, 0);
		A_hndw->sqlin ((double *) &A_hndw->a_hndw.a, SQLDOUBLE, 0);
		TestAkrzCursor = A_hndw->sqlcursor (_T("select a_krz from a_krz ")
			                                _T("where a_krz = ? ")
											_T("and a != ?"));
	}
	if (TestAkrzCursor == -1) return;

	Basis->AutoNr.GenAutoNr (0, 0, _T("a_krz"));
	A_hndw->a_hndw.a_krz = Basis->AutoNr.Nr;
    if (A_hndw->a_hndw.a_krz == 0l) return;
    A_hndw->sqlopen (TestAkrzCursor);
	int i = 0;
	while (A_hndw->sqlfetch (TestAkrzCursor) == 0)
	{
		if (i == 100)
		{
			A_hndw->a_hndw.a_krz = 0;
			return;
		}
		Sleep (100);
		i ++;
		Basis->AutoNr.GenAutoNr (0, 0, _T("a_krz"));
		A_hndw->a_hndw.a_krz = Basis->AutoNr.Nr;
		if (A_hndw->a_hndw.a_krz == 0l) return;
		A_hndw->sqlopen (TestAkrzCursor);
	}
}

void CLadenPage::TestAkrzWaage ()
{
	if (waa_a_par == NoKrz) 
	{
		m_LA_krz.ShowWindow (SW_HIDE);
		m_A_krz.ShowWindow (SW_HIDE);
		return;
	}
	m_LA_krz.ShowWindow (SW_SHOWNORMAL);
	m_A_krz.ShowWindow (SW_SHOWNORMAL);
	m_LA_krz.ShowWindow (SW_SHOWNORMAL);
	m_A_krz.ShowWindow (SW_SHOWNORMAL);
	if (waa_a_par == Auto) 
	{
		m_A_krz.EnableWindow (FALSE); 
	}
	else
	{
		m_A_krz.EnableWindow (TRUE); 
	}

	if (A_hndw->a_hndw.a_krz != 0l) return;

	A_krz->a_krz.a = A_bas->a_bas.a;
	int dsqlstatus = A_krz->dbreadafirst ();
	if (dsqlstatus == 0 && strcmp ((LPSTR) A_krz->a_krz.herk_kz, "1") != 0)
	{
		if (EanEmbExist ())
		{
			return;
		}
		A_krz->dbdelete ();
		strcpy (A_krz->a_krz.herk_kz, "1");
		A_krz->a_krz.a_herk = A_krz->a_krz.a;
	}
	else if (dsqlstatus == 0 && A_krz->a_krz.a_krz != 0l)
	{
		if (A_bas->a_bas.a >= (long) MaxAkrzWaa)
		{
			A_hndw->a_hndw.a_krz = A_krz->a_krz.a_krz;
		}
		else 
		{
			A_hndw->a_hndw.a_krz = (long) A_bas->a_bas.a;
		}
		Form.GetFormField (&m_A_krz)->Show ();
		return;
	}
	if (A_bas->a_bas.a < (long) MaxAkrzWaa)
	{
			A_hndw->a_hndw.a_krz = (long) A_bas->a_bas.a;
	}
	else if (waa_a_par == Auto)
	{
		MessageBox (_T("A�Die Artikelnummer ist zu gro� f�r die Kurznummer!\n")
			        _T("Die Kurznummer kan nicht generiert werden"), 0,
					MB_OK | MB_ICONERROR);
	}
	Form.Show ();
}

void CLadenPage::OnCbnKillfocusVerlArt()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CString VerkArt = A_hndw->a_hndw.verk_art;
	Form.Get();
	CString NewVerkArt = A_hndw->a_hndw.verk_art;
	if (dcs_a_par == Auto)
	{
		if (VerkArt == _T("K") && NewVerkArt != _T("K"))
		{
			Basis->AutoNr.nveinid (0, 0, "a_krz", A_hndw->a_hndw.a_krz);
			Basis->a_krz_gen = 0l;
		}
	}
	if (VerkArt != NewVerkArt)
	{
		A_hndw->a_hndw.a_krz = 0l;
		Form.GetFormField (&m_A_krz)->Show ();
		TestAkrz ();
	}
}

BOOL CLadenPage::EanEmbExist ()
{
	BOOL ret = TRUE;
	if (A_krz->a_krz.herk_kz[0] == '2')
	{
		A_ean.a_ean.ean = A_krz->a_krz.a_herk;
		if (A_ean.dbreadfirst () == 100)
		{
			 ret = FALSE;
		}
	}
	else if (A_krz->a_krz.herk_kz[0] == '3')
	{
		A_emb.a_emb.emb = A_krz->a_krz.a_herk;
		if (A_emb.dbreadfirst_emb () == 100)
		{
			ret = FALSE;
		}
	}
	return ret;
}

void CLadenPage::OnCbnCloseupVerlArt()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnCbnKillfocusVerlArt ();
}

void CLadenPage::OnAtxtchoice (long *TxtNr)
{
	Form.Get ();
	if (ChoiceAtxt == NULL)
	{
		ChoiceAtxt = new CChoiceAtxt (this);
	    ChoiceAtxt->IsModal = TRUE;
	    ChoiceAtxt->HideEnter = FALSE;
		ChoiceAtxt->DlgBkColor = ListBkColor;
		ChoiceAtxt->IdArrDown = IDI_HARROWDOWN;
		ChoiceAtxt->IdArrUp   = IDI_HARROWUP;
		ChoiceAtxt->IdArrNo   = IDI_HARROWNO;
		ChoiceAtxt->CreateDlg ();
	}

    ChoiceAtxt->SetDbClass (A_bas);
	ChoiceAtxt->SearchText = _T("");
	ChoiceAtxt->DoModal();

    if (ChoiceAtxt->GetState ())
    {
		  CATextList *abl = ChoiceAtxt->GetSelectedText (); 
		  if (abl == NULL) return;
		  *TxtNr = abl->txt_nr;
		  Form.Show ();
    }
	delete ChoiceAtxt;
	ChoiceAtxt = NULL;
}

void CLadenPage::OnAtxt1choice ()
{
	OnAtxtchoice (&A_bas->a_bas.allgtxt_nr1);
}

void CLadenPage::OnAtxt2choice ()
{
	OnAtxtchoice (&A_bas->a_bas.allgtxt_nr2);
}

void CLadenPage::OnAtxt3choice ()
{
	OnAtxtchoice (&A_bas->a_bas.allgtxt_nr3);
}

void CLadenPage::OnAtxt4choice ()
{
	OnAtxtchoice (&A_bas->a_bas.allgtxt_nr4);
}

void CLadenPage::PrintRegEti ()
{
	CPrintRegEtiDlg dlg;
	dlg.A_bas = A_bas;
	Form.Get ();
	dlg.Anzahl = A_hndw->a_hndw.anz_reg_eti;
	dlg.EtiType = atoi(A_hndw->a_hndw.reg_eti);
	INT_PTR ret = dlg.DoModal ();
}
void CLadenPage::PrintThekeEti ()
{
	CPrintThekeEtiDlg dlg;
	dlg.A_bas = A_bas;
	Form.Get ();
	dlg.Anzahl = A_hndw->a_hndw.anz_theke_eti;
	dlg.EtiType = atoi(A_hndw->a_hndw.theke_eti);
	INT_PTR ret = dlg.DoModal ();
}

void CLadenPage::OnEnterTxt ()
{
	CProcess p;
	p.SetCommand (_T("atexte"));
	HANDLE Pid = p.Start ();
}

void CLadenPage::OnMoreTxt ()
{
	CMoreTxt dlg;
	memcpy (&dlg.A_bas.a_bas, &A_bas->a_bas, sizeof (A_BAS));
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		memcpy (&A_bas->a_bas, &dlg.A_bas.a_bas, sizeof (A_BAS));
	}
}

void CLadenPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CLadenPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

BOOL CLadenPage::OnKillActive ()
{
	Form.Get ();
	if (A_bas->a_bas.a_typ == Basis->Hndw || A_bas->a_bas.a == -1)
	{
		if (A_hndw->a_hndw.verk_beg.year == 0)
		{
			A_hndw->a_hndw.verk_beg.year = 1980;
			A_hndw->a_hndw.verk_beg.month = 1;
			A_hndw->a_hndw.verk_beg.day = 1;
		}
	}
	else if (A_bas->a_bas.a_typ == Basis->Eig)
	{
		if (A_eig->a_eig.verk_beg.year == 0)
		{
			A_eig->a_eig.verk_beg.year = 1980;
			A_eig->a_eig.verk_beg.month = 1;
			A_eig->a_eig.verk_beg.day = 1;
		}
	}
	else if (A_bas->a_bas.a_typ == Basis->Pfa)
	{
		if (A_pfa->a_pfa.verk_beg.year == 0)
		{
			A_pfa->a_pfa.verk_beg.year = 1980;
			A_pfa->a_pfa.verk_beg.month = 1;
			A_pfa->a_pfa.verk_beg.day = 1;
		}
	}
	else if (A_bas->a_bas.a_typ == Basis->Leer)
	{
		if (A_leer->a_leer.verk_beg.year == 0)
		{
			A_leer->a_leer.verk_beg.year = 1980;
			A_leer->a_leer.verk_beg.month = 1;
			A_leer->a_leer.verk_beg.day = 1;
		}
	}
	if (A_bas->a_bas.a_typ == Basis->Hndw || A_bas->a_bas.a == -1)
	{
		A_hndw->a_hndw.a = A_bas->a_bas.a;
		A_hndw->dbupdate ();
/*
		if (A_hndw->a_hndw.a_krz != 0l)
		{
			A_krz->a_krz.a_krz = A_hndw->a_hndw.a_krz;
			A_krz->a_krz.a = A_hndw->a_hndw.a;
			A_krz->a_krz.a_herk = A_hndw->a_hndw.a;
			strcpy ((LPSTR) A_krz->a_krz.herk_kz, "1");
			strcpy ((LPSTR) A_krz->a_krz.verk_art, (LPSTR) A_hndw->a_hndw.verk_art);
			strcpy ((LPSTR) A_krz->a_krz.modif, "N");
			A_krz->a_krz.hwg = A_bas->a_bas.hwg;
			A_krz->a_krz.wg = A_bas->a_bas.wg;
			A_krz->a_krz.anz_a = 1;
			A_krz->dbupdate ();
		}
*/
	}
	else if (A_bas->a_bas.a_typ == Basis->Eig)
	{
		A_eig->a_eig.a = A_bas->a_bas.a;
		A_eig->a_eig.a_krz = A_hndw->a_hndw.a_krz;
		A_eig->a_eig.sg1 = A_hndw->a_hndw.sg1;
		A_eig->a_eig.sg2 = A_hndw->a_hndw.sg2;
		A_eig->a_eig.tara = A_hndw->a_hndw.tara;
		A_eig->a_eig.me_einh_ek = A_hndw->a_hndw.me_einh_kun;
		A_eig->a_eig.inh = A_hndw->a_hndw.inh;
		_tcscpy (A_eig->a_eig.verk_art, A_hndw->a_hndw.verk_art);
		_tcscpy (A_eig->a_eig.mwst_ueb, A_hndw->a_hndw.mwst_ueb);
		_tcscpy (A_eig->a_eig.pr_ausz, A_hndw->a_hndw.pr_ausz);
		_tcscpy ( A_eig->a_eig.pr_man, A_hndw->a_hndw.pr_man);
		A_eig->dbupdate ();
/*
		if (A_eig->a_eig.a_krz != 0l)
		{
			A_krz->a_krz.a_krz = A_eig->a_eig.a_krz;
			A_krz->a_krz.a = A_eig->a_eig.a;
			A_krz->a_krz.a_herk = A_eig->a_eig.a;
			strcpy ((LPSTR) A_krz->a_krz.herk_kz, "1");
			strcpy ((LPSTR) A_krz->a_krz.verk_art, (LPSTR) A_eig.a_eig.verk_art);
			strcpy ((LPSTR) A_krz->a_krz.modif, "N");
			A_krz->a_krz.hwg = A_bas->a_bas.hwg;
			A_krz->a_krz.wg = A_bas->a_bas.wg;
			A_krz->a_krz.anz_a = 1;
			A_krz->dbupdate ();
		}
*/
	}
	else if (A_bas->a_bas.a_typ == Basis->Pfa)
	{
		A_pfa->a_pfa.a = A_bas->a_bas.a;
		A_pfa->a_pfa.a_krz = A_hndw->a_hndw.a_krz;
		A_pfa->a_pfa.sg1 = A_hndw->a_hndw.sg1;
		A_pfa->a_pfa.sg2 = A_hndw->a_hndw.sg2;
		_tcscpy (A_pfa->a_pfa.verk_art, A_hndw->a_hndw.verk_art);
		_tcscpy (A_pfa->a_pfa.mwst_ueb, A_hndw->a_hndw.mwst_ueb);
		A_pfa->dbupdate ();
	}
	else if (A_bas->a_bas.a_typ == Basis->Leer)
	{
		A_leer->a_leer.a = A_bas->a_bas.a;
		A_leer->a_leer.a_krz = A_hndw->a_hndw.a_krz;
		A_leer->a_leer.sg1 = A_hndw->a_hndw.sg1;
		A_leer->a_leer.sg2 = A_hndw->a_hndw.sg2;
		_tcscpy (A_leer->a_leer.verk_art, A_hndw->a_hndw.verk_art);
		_tcscpy (A_leer->a_leer.mwst_ueb, A_hndw->a_hndw.mwst_ueb);
		A_leer->dbupdate ();
	}
	return TRUE;
}

BOOL CLadenPage::OnSetActive ()
{
	if (Basis != NULL)
	{
		Basis->Form.Get ();
	}
	Form.Show ();
	HeadForm.Show ();
	return TRUE;
}

void CLadenPage::OnDelete ()
{
	Update->Delete ();
}

void CLadenPage::OnEnKillfocusTara2()
{
	//FS-167
	Form.Get ();
	if (A_bas_erw->a_bas_erw.tara2 > 0.0 || A_bas_erw->a_bas_erw.tara3 > 0.0  || A_bas_erw->a_bas_erw.tara4 > 0.0)
	{
		A_hndw->a_hndw.tara = A_bas_erw->a_bas_erw.tara2 + A_bas_erw->a_bas_erw.tara3 + A_bas_erw->a_bas_erw.tara4;
		m_Tara.EnableWindow (FALSE);
	}
	else
	{
		m_Tara.EnableWindow (TRUE);
	}
	Form.Show ();
}
void CLadenPage::OnEnKillfocusTara3()
{
	//FS-167
	Form.Get ();
	if (A_bas_erw->a_bas_erw.tara2 > 0.0 || A_bas_erw->a_bas_erw.tara3 > 0.0  || A_bas_erw->a_bas_erw.tara4 > 0.0)
	{
		A_hndw->a_hndw.tara = A_bas_erw->a_bas_erw.tara2 + A_bas_erw->a_bas_erw.tara3 + A_bas_erw->a_bas_erw.tara4;
		m_Tara.EnableWindow (FALSE);
	}
	else
	{
		m_Tara.EnableWindow (TRUE);
	}
	Form.Show ();
}
void CLadenPage::OnEnKillfocusTara4()
{
	//FS-167
	Form.Get ();
	if (A_bas_erw->a_bas_erw.tara2 > 0.0 || A_bas_erw->a_bas_erw.tara3 > 0.0  || A_bas_erw->a_bas_erw.tara4 > 0.0)
	{
		A_hndw->a_hndw.tara = A_bas_erw->a_bas_erw.tara2 + A_bas_erw->a_bas_erw.tara3 + A_bas_erw->a_bas_erw.tara4;
		m_Tara.EnableWindow (FALSE);
	}
	else
	{
		m_Tara.EnableWindow (TRUE);
	}
	Form.Show ();

}

void CLadenPage::OnEnKillfocusTara()
{
	//FS-167
	Form.Get ();
	if (A_hndw->a_hndw.tara > 0)
	{
		m_Tara2.EnableWindow (FALSE);
		m_Tara3.EnableWindow (FALSE);
		m_Tara4.EnableWindow (FALSE);
	}
	else
	{
		m_Tara2.EnableWindow (TRUE);
		m_Tara3.EnableWindow (TRUE);
		m_Tara4.EnableWindow (TRUE);
	}
	Form.Show ();
}

void CLadenPage::OnEnKillfocusHbkZtr() //WAL-130
{
	Form.Get ();
}
