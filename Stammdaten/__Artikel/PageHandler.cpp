#include "StdAfx.h"
#include "PageHandler.h"

CPageHandler *CPageHandler::Instance = NULL;

CPageHandler::CPageHandler(void)
{
	m_Sheet = NULL;
	m_Page  = NULL;
	m_ActiveControl = NULL;
}

CPageHandler::~CPageHandler(void)
{
}

CPageHandler *CPageHandler::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CPageHandler ();
	}
	return Instance;
}

void CPageHandler::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

void CPageHandler::RegisterSheet (CDbPropertySheet *Sheet)
{
	m_Sheet = Sheet;
}

void CPageHandler::RegisterPage (CDbPropertyPage *Page)
{
	m_Page = Page;
	m_ActiveControl = m_Page->GetFocus ();
}

void CPageHandler::RegisterPage ()
{
	if (m_Sheet != NULL)
	{
		m_Page = (CDbPropertyPage *) m_Sheet->GetActivePage ();
		m_ActiveControl = m_Page->GetFocus ();
	}
}

void CPageHandler::SetPage (BOOL UnRegister)
{
	if (m_Sheet != NULL && m_Page != NULL)
	{
		m_Sheet->SetActivePage (m_Page);
		if (m_ActiveControl != NULL)
		{
			m_ActiveControl->SetFocus ();
		}
	}
	if (UnRegister)
	{
		m_Page = NULL;
		m_ActiveControl = NULL;
	}
}

BOOL CPageHandler::CanSetPage ()
{
	BOOL ret = TRUE;
	if (m_Sheet != NULL && m_Page != NULL)
	{
		ret = FALSE;
	}
	return ret;
}

BOOL CPageHandler::SetActivePage (int Page)
{
	int ret = FALSE;
	if (m_Sheet != NULL && CanSetPage ())
	{
		ret = m_Sheet->SetActivePage (Page);
	}
	return ret;
}

BOOL CPageHandler::SetActivePage (CDbPropertyPage *Page)
{
	int ret = FALSE;
	if (m_Sheet != NULL && CanSetPage ())
	{
		ret = m_Sheet->SetActivePage (Page);
	}
	return ret;
}

