#ifdef WIN32
#include <windows.h>
#endif
#include <odbcinst.h>
#include <time.h>

class DayTime
{
     public :
		 long lTime;
		 CString Time;
		 DayTime (char *t);
		 DayTime (CString *t);
		 DayTime *Create (char *t);
		 DayTime *Create (CString *t);
		 long GetLongTime ();
		 CString *GetTextTime ();
};

class DbTime
{
     private :
		  CString tDate;
		  DATE_STRUCT ds;
          SYSTEMTIME st;
		  FILETIME ft;
		  ULARGE_INTEGER ul;
		  time_t ldat;
     public :
	      DbTime ();
		  DbTime (char *date);
	      DbTime (DATE_STRUCT *);
	      DbTime (long ldate);
		  BOOL IsNull ();
		  time_t GetTime ();
          int GetWeekDay ();
		  void SetTime (time_t);
		  void SetDateStruct ();
		  CString* GetStringTime ();
		  DATE_STRUCT *GetDateStruct (DATE_STRUCT *);
		  static long ParseLong (DATE_STRUCT *date);
		  static void ToDateStruct (DATE_STRUCT *date, long ldate); 
		  static void TestDateValue (DATE_STRUCT *date); 
#ifdef WIN32
		  operator time_t () const;
		  operator DATE_STRUCT () const;
          DbTime& operator+ (int);
          DbTime& operator- (int);
		  BOOL operator== (long);
#endif
          DbTime* Add (int);
          DbTime* Sub (int);
};
