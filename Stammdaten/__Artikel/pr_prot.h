#ifndef _PR_PROT_DEF
#define _PR_PROT_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct PR_PROT {
   short          delstatus;
   double         a;
   DATE_STRUCT    bearb;
   short          fil;
   short          fil_gr;
   DATE_STRUCT    lad_akt_von;
   DATE_STRUCT    lad_akt_bis;
   short          mdn;
   short          mdn_gr;
   TCHAR          pers_nam[9];
   double         pr_vk;
   double         pr_vk_alt;
   double         pr_vk_sa;
   long           prima;
   TCHAR          dr_status[2];
   double         pr_vk_euro;
   double         pr_vk_alt_euro;
   double         pr_vk_sa_euro;
};
extern struct PR_PROT pr_prot, pr_prot_null;

#line 8 "pr_prot.rh"

class PR_PROT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PR_PROT pr_prot;
               PR_PROT_CLASS () : DB_CLASS ()
               {
               }
               int dbinsert ();
};
#endif
