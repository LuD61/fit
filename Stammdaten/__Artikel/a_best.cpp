#include "stdafx.h"
#include "a_best.h"

struct A_BEST a_best, a_best_null;

void A_BEST_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &a_best.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_best.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_best.a, SQLDOUBLE, 0);
    sqlout ((double *) &a_best.a,SQLDOUBLE,0);
    sqlout ((short *) &a_best.best_anz,SQLSHORT,0);
    sqlout ((TCHAR *) a_best.best_auto,SQLCHAR,2);
    sqlout ((double *) &a_best.bsd_min,SQLDOUBLE,0);
    sqlout ((double *) &a_best.bsd_min_emp,SQLDOUBLE,0);
    sqlout ((double *) &a_best.bsd_max_emp,SQLDOUBLE,0);
    sqlout ((short *) &a_best.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) a_best.erf_gew_kz,SQLCHAR,2);
    sqlout ((short *) &a_best.fil,SQLSHORT,0);
    sqlout ((TCHAR *) a_best.letzt_lief_nr,SQLCHAR,17);
    sqlout ((TCHAR *) a_best.lief,SQLCHAR,17);
    sqlout ((TCHAR *) a_best.lief_best,SQLCHAR,17);
    sqlout ((long *) &a_best.lief_s,SQLLONG,0);
    sqlout ((short *) &a_best.mdn,SQLSHORT,0);
    sqlout ((short *) &a_best.me_einh_ek,SQLSHORT,0);
    sqlout ((TCHAR *) a_best.umk_kz,SQLCHAR,2);
            cursor = sqlcursor (_T("select a_best.a,  ")
_T("a_best.best_anz,  a_best.best_auto,  a_best.bsd_min,  ")
_T("a_best.bsd_min_emp,  a_best.bsd_max_emp,  a_best.delstatus,  ")
_T("a_best.erf_gew_kz,  a_best.fil,  a_best.letzt_lief_nr,  a_best.lief,  ")
_T("a_best.lief_best,  a_best.lief_s,  a_best.mdn,  a_best.me_einh_ek,  ")
_T("a_best.umk_kz from a_best ")

#line 14 "a_best.rpp"
                                  _T("where mdn = ? ")
				  _T("and fil = ? ")
				  _T("and a = ?"));
    sqlin ((double *) &a_best.a,SQLDOUBLE,0);
    sqlin ((short *) &a_best.best_anz,SQLSHORT,0);
    sqlin ((TCHAR *) a_best.best_auto,SQLCHAR,2);
    sqlin ((double *) &a_best.bsd_min,SQLDOUBLE,0);
    sqlin ((double *) &a_best.bsd_min_emp,SQLDOUBLE,0);
    sqlin ((double *) &a_best.bsd_max_emp,SQLDOUBLE,0);
    sqlin ((short *) &a_best.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) a_best.erf_gew_kz,SQLCHAR,2);
    sqlin ((short *) &a_best.fil,SQLSHORT,0);
    sqlin ((TCHAR *) a_best.letzt_lief_nr,SQLCHAR,17);
    sqlin ((TCHAR *) a_best.lief,SQLCHAR,17);
    sqlin ((TCHAR *) a_best.lief_best,SQLCHAR,17);
    sqlin ((long *) &a_best.lief_s,SQLLONG,0);
    sqlin ((short *) &a_best.mdn,SQLSHORT,0);
    sqlin ((short *) &a_best.me_einh_ek,SQLSHORT,0);
    sqlin ((TCHAR *) a_best.umk_kz,SQLCHAR,2);
            sqltext = _T("update a_best set a_best.a = ?,  ")
_T("a_best.best_anz = ?,  a_best.best_auto = ?,  a_best.bsd_min = ?,  ")
_T("a_best.bsd_min_emp = ?,  a_best.bsd_max_emp = ?,  ")
_T("a_best.delstatus = ?,  a_best.erf_gew_kz = ?,  a_best.fil = ?,  ")
_T("a_best.letzt_lief_nr = ?,  a_best.lief = ?,  a_best.lief_best = ?,  ")
_T("a_best.lief_s = ?,  a_best.mdn = ?,  a_best.me_einh_ek = ?,  ")
_T("a_best.umk_kz = ? ")

#line 18 "a_best.rpp"
                                  _T("where mdn = ? ")
				  _T("and fil = ? ")
				  _T("and a = ?");
            sqlin ((short *)   &a_best.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_best.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_best.a, SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &a_best.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_best.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_best.a, SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_best ")
                                  _T("where mdn = ? ")
				  _T("and fil = ? ")
				  _T("and a = ?"));
            sqlin ((short *)   &a_best.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_best.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_best.a, SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_best ")
                                  _T("where mdn = ? ")
				  _T("and fil = ? ")
				  _T("and a = ?"));
    sqlin ((double *) &a_best.a,SQLDOUBLE,0);
    sqlin ((short *) &a_best.best_anz,SQLSHORT,0);
    sqlin ((TCHAR *) a_best.best_auto,SQLCHAR,2);
    sqlin ((double *) &a_best.bsd_min,SQLDOUBLE,0);
    sqlin ((double *) &a_best.bsd_min_emp,SQLDOUBLE,0);
    sqlin ((double *) &a_best.bsd_max_emp,SQLDOUBLE,0);
    sqlin ((short *) &a_best.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) a_best.erf_gew_kz,SQLCHAR,2);
    sqlin ((short *) &a_best.fil,SQLSHORT,0);
    sqlin ((TCHAR *) a_best.letzt_lief_nr,SQLCHAR,17);
    sqlin ((TCHAR *) a_best.lief,SQLCHAR,17);
    sqlin ((TCHAR *) a_best.lief_best,SQLCHAR,17);
    sqlin ((long *) &a_best.lief_s,SQLLONG,0);
    sqlin ((short *) &a_best.mdn,SQLSHORT,0);
    sqlin ((short *) &a_best.me_einh_ek,SQLSHORT,0);
    sqlin ((TCHAR *) a_best.umk_kz,SQLCHAR,2);
            ins_cursor = sqlcursor (_T("insert into a_best (")
_T("a,  best_anz,  best_auto,  bsd_min,  bsd_min_emp,  bsd_max_emp,  delstatus,  ")
_T("erf_gew_kz,  fil,  letzt_lief_nr,  lief,  lief_best,  lief_s,  mdn,  me_einh_ek,  ")
_T("umk_kz) ")

#line 41 "a_best.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?)")); 

#line 43 "a_best.rpp"
}

BOOL A_BEST_CLASS::operator== (A_BEST& a_best)
{
            return TRUE;
} 
