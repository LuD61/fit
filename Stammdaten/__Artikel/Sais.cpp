#include "stdafx.h"
#include "sais.h"

struct SAIS sais, sais_null;

void SAIS_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &sais.jr, SQLSHORT, 0);
            sqlin ((short *)   &sais.sais, SQLSHORT, 0);
    sqlout ((short *) &sais.delstatus,SQLSHORT,0);
    sqlout ((short *) &sais.jr,SQLSHORT,0);
    sqlout ((TCHAR *) sais.pers_nam,SQLCHAR,9);
    sqlout ((short *) &sais.sais,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &sais.st_aktiv,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &sais.st_bearbeit,SQLDATE,0);
    sqlout ((TCHAR *) sais.st_bz_1,SQLCHAR,25);
    sqlout ((TCHAR *) sais.st_bz_2,SQLCHAR,25);
    sqlout ((DATE_STRUCT *) &sais.st_nordb,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &sais.st_nordv,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &sais.st_vkztb,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &sais.st_vkztv,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &sais.st_vordb,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &sais.st_vordv,SQLDATE,0);
            cursor = sqlcursor (_T("select sais.delstatus,  ")
_T("sais.jr,  sais.pers_nam,  sais.sais,  sais.st_aktiv,  sais.st_bearbeit,  ")
_T("sais.st_bz_1,  sais.st_bz_2,  sais.st_nordb,  sais.st_nordv,  ")
_T("sais.st_vkztb,  sais.st_vkztv,  sais.st_vordb,  sais.st_vordv from sais ")

#line 13 "Sais.rpp"
                                  _T("where jr = ? ")
				  _T("and sais = ?"));
    sqlin ((short *) &sais.delstatus,SQLSHORT,0);
    sqlin ((short *) &sais.jr,SQLSHORT,0);
    sqlin ((TCHAR *) sais.pers_nam,SQLCHAR,9);
    sqlin ((short *) &sais.sais,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &sais.st_aktiv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_bearbeit,SQLDATE,0);
    sqlin ((TCHAR *) sais.st_bz_1,SQLCHAR,25);
    sqlin ((TCHAR *) sais.st_bz_2,SQLCHAR,25);
    sqlin ((DATE_STRUCT *) &sais.st_nordb,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_nordv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_vkztb,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_vkztv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_vordb,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_vordv,SQLDATE,0);
            sqltext = _T("update sais set sais.delstatus = ?,  ")
_T("sais.jr = ?,  sais.pers_nam = ?,  sais.sais = ?,  sais.st_aktiv = ?,  ")
_T("sais.st_bearbeit = ?,  sais.st_bz_1 = ?,  sais.st_bz_2 = ?,  ")
_T("sais.st_nordb = ?,  sais.st_nordv = ?,  sais.st_vkztb = ?,  ")
_T("sais.st_vkztv = ?,  sais.st_vordb = ?,  sais.st_vordv = ? ")

#line 16 "Sais.rpp"
                                  _T("where jr = ? ")
				  _T("and sais = ?");
            sqlin ((short *)   &sais.jr, SQLSHORT, 0);
            sqlin ((short *)   &sais.sais, SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &sais.jr, SQLSHORT, 0);
            sqlin ((short *)   &sais.sais, SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select jr from sais ")
                                  _T("where jr = ? ")
				  _T("and sais = ?"));
            sqlin ((short *)   &sais.jr, SQLSHORT, 0);
            sqlin ((short *)   &sais.sais, SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from sais ")
                                  _T("where jr = ? ")
				  _T("and sais = ?"));
    sqlin ((short *) &sais.delstatus,SQLSHORT,0);
    sqlin ((short *) &sais.jr,SQLSHORT,0);
    sqlin ((TCHAR *) sais.pers_nam,SQLCHAR,9);
    sqlin ((short *) &sais.sais,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &sais.st_aktiv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_bearbeit,SQLDATE,0);
    sqlin ((TCHAR *) sais.st_bz_1,SQLCHAR,25);
    sqlin ((TCHAR *) sais.st_bz_2,SQLCHAR,25);
    sqlin ((DATE_STRUCT *) &sais.st_nordb,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_nordv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_vkztb,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_vkztv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_vordb,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sais.st_vordv,SQLDATE,0);
            ins_cursor = sqlcursor (_T("insert into sais (")
_T("delstatus,  jr,  pers_nam,  sais,  st_aktiv,  st_bearbeit,  st_bz_1,  st_bz_2,  ")
_T("st_nordb,  st_nordv,  st_vkztb,  st_vkztv,  st_vordb,  st_vordv) ")

#line 33 "Sais.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?)")); 

#line 35 "Sais.rpp"
}

BOOL SAIS_CLASS::operator== (SAIS& sais)
{
            if (this->sais.jr     != sais.jr) return FALSE;  
            if (this->sais.sais   != sais.sais) return FALSE;  
            return TRUE;
} 
