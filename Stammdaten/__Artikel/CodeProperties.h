#pragma once
#include "vector.h"

class CCodeProperties :
	public CVector
{
public:
	CCodeProperties(void);
	~CCodeProperties(void);
	void Load (CString&);
	short GetSource (short target);
	short GetTarget (short source);
};
