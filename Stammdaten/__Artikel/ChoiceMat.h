#ifndef _CHOICEMAT_DEF
#define _CHOICEMAT_DEF

#define MATSELECTED 5010
#define MATCANCELED 5011

#include "ChoiceX.h"
#include "MatList.h"
#include <vector>

class CChoiceMat : public CChoiceX
{
    protected :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
		CString Where;
	    std::vector<CMatList *> MatList;
	    std::vector<CMatList *> SelectList;
      	CChoiceMat(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceMat(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchA (CListCtrl *,  LPTSTR);
        void SearchABz1 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
	    virtual void SaveSelection (CListCtrl *);
		virtual void SendSelect (); 
		virtual void SendCancel (); 
		CMatList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		virtual void SetDefault ();
};
#endif
