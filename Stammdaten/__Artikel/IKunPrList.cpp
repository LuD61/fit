#include "StdAfx.h"
#include "IKunPrList.h"

CIKunPrList::CIKunPrList(void)
{
	mdn = 0;
	kun_pr = 0l;
	zus_bz = _T("");
}

CIKunPrList::CIKunPrList(short mdn, long kun_pr, LPTSTR zus_bz)
{
	this->mdn    = mdn;
	this->kun_pr = kun_pr;
	this->zus_bz = zus_bz;
}

CIKunPrList::~CIKunPrList(void)
{
}
