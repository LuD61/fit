#include "StdAfx.h"
#include "PrGrListCtrl.h"
#include "StrFuncs.h"
#include "resource.h"

CPrGrListCtrl::CPrGrListCtrl(void)
{
	ChoiceA = NULL;
	ModalChoiceA = TRUE;
	m_Mdn = 1;
	PosA     = POSA;
	PosABz1  = POSABZ1;
	PosVkPr  = POSVKPR;
	PosLdPr  = POSLDPR;
	PosEkAbs = POSEKABS;
	PosEkProz = POSEKPROZ;
	PosRabKz = POSRABKZ;
	PosRab = POSRAB;

	Position[0] = &PosA;
	Position[1] = &PosABz1;
	Position[2] = &PosVkPr;
	Position[3] = &PosLdPr;
	Position[4] = &PosEkAbs;
	Position[5] = &PosEkProz;
	Position[6] = &PosRabKz;
	Position[7] = &PosRab;
	Position[8] = NULL;
	MaxComboEntries = 20;
	mdn = 0;
	pr_gr_stuf = 0;
	ListRows.Init ();
	Aufschlag = LIST;
	Mode = STANDARD;
	check.LoadFromResource (AfxGetInstanceHandle (), IDB_CHECK);
	uncheck.LoadFromResource (AfxGetInstanceHandle (), IDB_UNCHECK);
}

CPrGrListCtrl::~CPrGrListCtrl(void)
{
	if (ChoiceA != NULL)
	{
		delete ChoiceA;
	}
	DestroyRows (ListRows);
}

BEGIN_MESSAGE_MAP(CPrGrListCtrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
//	ON_WM_PAINT ()
END_MESSAGE_MAP()


void CPrGrListCtrl::OnPaint ()
{
	CEditListCtrl::OnPaint ();

	CRect Rect;
	CDC *cDC = GetDC ();
	int ItemCount = GetItemCount ();
	for (int i = 0; i < ItemCount; i ++)
	{
		GetSubItemRect (i, PosRabKz, LVIR_BOUNDS, Rect);

		CString IsChecked = GetItemText (i, PosRabKz);

		if (IsChecked == _T("X"))
		{
			int cx = check.GetWidth ();
			int cy = check.GetHeight ();
			int x = (Rect.right - Rect.left - cx) / 2;
			x = (x > 0) ? x : 0;
			x += Rect.left;
			int y = (Rect.bottom - Rect.top - cy) / 2;
			y = (y > 0) ? y : 0;
			y += Rect.top;
			check.BitBlt (*cDC, x, y);
		}
		else
		{
			int cx = uncheck.GetWidth ();
			int cy = uncheck.GetHeight ();
			int x = (Rect.right - Rect.left - cx) / 2;
			x = (x > 0) ? x : 0;
			x += Rect.left;
			int y = (Rect.bottom - Rect.top - cy) / 2;
			y = (y > 0) ? y : 0;
			y += Rect.top;
			uncheck.BitBlt (*cDC, x, y);
		}
	}
	ReleaseDC (cDC);
}

void CPrGrListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (PosVkPr, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (PosA, 0);
	}
	PerformListChangeHandler ();
}

void CPrGrListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	if (col == PosABz1) return;
	if (Mode == TERMIN && col == PosVkPr + 2) return;
	if (Mode == TERMIN && col == PosLdPr + 2) return;
	if (col == PosA)
	{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
	}
	else if (col == PosRabKz)
	{
		CEditListCtrl::StartEnterCheckBox (col, row);
	}
	else
	{
		CEditListCtrl::StartEnter (col, row);
	}
}

void CPrGrListCtrl::StopEnter ()
{

//	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		int check = ListCheckBox.GetCheck ();
        if (check == BST_CHECKED)
		{
			FillList.SetItemText (_T("X"), EditRow, EditCol);
		}
		else
		{
			FillList.SetItemText (_T(" "), EditRow, EditCol);
		}
		ListCheckBox.DestroyWindow ();
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	CPreise *p = (CPreise *) ListRows.Get (EditRow);
	if (p != NULL)
	{
 		CString A =  GetItemText (EditRow, PosA);
 		CString VkPr =  GetItemText (EditRow, PosVkPr);
		p->ipr.a = CStrFuncs::StrToDouble (A);
		p->ipr.vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
		p->ipr.vk_pr_i = p->ipr.vk_pr_eu;
		CString LdPr =  GetItemText (EditRow, PosLdPr);
		p->ipr.ld_pr_eu = CStrFuncs::StrToDouble (LdPr);
		p->ipr.ld_pr = p->ipr.ld_pr_eu;
	}
}

void CPrGrListCtrl::SetSel (CString& Text)
{

   if (EditCol == PosA || EditCol == PosVkPr ||
	   EditCol == PosLdPr || EditCol == PosEkAbs ||
	   EditCol == PosEkProz || EditCol == PosRab)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CPrGrListCtrl::FormatText (CString& Text)
{
    if (EditCol == PosVkPr)
	{
		DoubleToString (StrToDouble (Text), Text, 4);
	}
    else if (EditCol == PosLdPr)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
    else if (EditCol == PosEkAbs)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
    else if (EditCol == PosEkProz)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
    else if (Mode == AKTION && EditCol == PosRab)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
/*
    else if (EditCol == 4)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
    else if (EditCol == 5)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
*/
}

void CPrGrListCtrl::NextRow ()
{
    GetEnter ();
	int count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if (Mode == AKTION)
		{
			CString Rab = GetItemText (EditRow, PosRab);
			CString RabKz = GetItemText (EditRow, PosRabKz);
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0) && 
				(RabKz != "X" || StrToDouble (Rab) == 0.0))
			{
				return;
			}
		}
		else
		{
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0))
			{
				return;
			}
		}
	}
	if (EditCol == PosA)
	{
		ReadABz1 ();
	}
	SetEditText ();
	TestIprIndex ();
    count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;
	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	PerformListChangeHandler ();
}

void CPrGrListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
    GetEnter ();
	int count = GetItemCount ();
	if (EditRow == count - 1)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if ((StrToDouble (VkPr) == 0.0) &&
			(StrToDouble (LdPr) == 0.0))
		{
		    ListRows.Drop (EditRow);
	        DeleteItem (EditRow);
		}
	}
	else
	{
		if (EditRow <= 0)
		{
			return;
		}
		if (IsWindow (SearchListCtrl.Edit.m_hWnd) && EditCol == PosA)
		{
			ReadABz1 ();
		}
		TestIprIndex ();
	}

	StopEnter ();
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
	PerformListChangeHandler ();
}

BOOL CPrGrListCtrl::LastCol ()
{
	if (EditCol < PosLdPr) return FALSE;
	if (Mode == TERMIN)
	{
		if (EditCol >= PosLdPr)
		{
			return TRUE;
		}
		return FALSE;
	}

	if (Mode == AKTION)
	{
		if (EditCol >= PosRab)
		{
			return TRUE;
		}
		return FALSE;
	}

	if (Aufschlag == LIST || Aufschlag == ALL)
	{
		if (EditCol < PosEkProz) return FALSE;
	}
	return TRUE;
}

void CPrGrListCtrl::OnReturn ()
{
    GetEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if (Mode == AKTION)
		{
			CString Rab = GetItemText (EditRow, PosRab);
			CString RabKz = GetItemText (EditRow, PosRabKz);
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0) && 
				(RabKz != "X" || StrToDouble (Rab) == 0.0))
			{
			return;
			}
		}
		else
		{
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0))
			{
			return;
			}
		}
	}
	if (EditCol == PosA)
	{
		ReadABz1 ();
	}
	if (LastCol ())
	{
		TestIprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
		if (EditRow == rowCount)
		{
			EditCol = PosA;
		}
		else
		{
			EditCol = PosVkPr;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
		if (EditCol == PosABz1)
		{
			EditCol ++;
		}
		if (Mode == AKTION)
		{
			if (EditCol == PosLdPr + 1)
			{
				EditCol += 2;
			}
		}
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	PerformListChangeHandler ();
}

void CPrGrListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol == PosLdPr)
	{
		return;
	}
	if (EditCol == PosA)
	{
		ReadABz1 ();
	}
	StopEnter ();
	EditCol ++;
	if (EditCol == PosABz1)
	{
		EditCol ++;
	}
	if (Mode == AKTION)
	{
			if (EditCol == PosLdPr + 1)
			{
				EditCol += 2;
			}
	}
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CPrGrListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	if (EditCol == PosA)
	{
		ReadABz1 ();
	}
	StopEnter ();
	EditCol --;
	if (EditCol == PosABz1)
	{
		EditCol --;
	}
	if (Mode == AKTION)
	{
			if (EditCol == PosLdPr + 2)
			{
				EditCol -= 2;
			}
	}
    StartEnter (EditCol, EditRow);
}

BOOL CPrGrListCtrl::InsertRow ()
{
	CString VkPr = GetItemText (EditRow, PosVkPr);
	CString LdPr = GetItemText (EditRow, PosLdPr);
	if (Mode == AKTION)
	{
		CString Rab = GetItemText (EditRow, PosRab);
		CString RabKz = GetItemText (EditRow, PosRabKz);
		if ((StrToDouble (VkPr) == 0.0) &&
			(StrToDouble (LdPr) == 0.0) && 
			(RabKz != "X" || StrToDouble (Rab) == 0.0))
		{
			return FALSE;
		}
	}
	else
	{
		if ((StrToDouble (VkPr) == 0.0) &&
			(StrToDouble (LdPr) == 0.0))
		{
			return FALSE;
		}
	}

	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T("0 "), EditRow, PosA);
	FillList.SetItemText (_T(""), EditRow, PosABz1);
	FillList.SetItemText (_T("0,00 "), EditRow, PosVkPr);
	FillList.SetItemText (_T("0,00 "), EditRow, PosLdPr);
	FillList.SetItemText (_T("0,00 "), EditRow, PosEkAbs);
	FillList.SetItemText (_T("0,00 "), EditRow, PosEkProz);
	IPR ipr;
	memcpy (&ipr, &ipr_null, sizeof (IPR));
	ipr.mdn = mdn;
	ipr.pr_gr_stuf = pr_gr_stuf;
    CPreise *p = new CPreise (CString ("0,00"), CString ("0,00"),
							  ipr);	
	ListRows.Insert (EditRow, p);
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CPrGrListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	ListRows.Drop (EditRow);
	return CEditListCtrl::DeleteRow ();
}

BOOL CPrGrListCtrl::AppendEmpty ()
{

	int rowCount = GetItemCount ();
	if (rowCount > 0)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if (Mode == AKTION)
		{
			CString Rab = GetItemText (EditRow, PosRab);
			CString RabKz = GetItemText (EditRow, PosRabKz);
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0) && 
				(RabKz != "X" || StrToDouble (Rab) == 0.0))
			{
				return FALSE;
			}
		}
		else
		{
			if ((StrToDouble (VkPr) == 0.0) &&
				(StrToDouble (LdPr) == 0.0))
			{
				return FALSE;
			}
		}
	}
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText (_T("0 "), rowCount, PosA);
	FillList.SetItemText (_T(""), rowCount, PosABz1);
	FillList.SetItemText (_T("0,00 "), rowCount, PosVkPr);
	FillList.SetItemText (_T("0,00 "), rowCount, PosLdPr);
	FillList.SetItemText (_T("0,00 "), rowCount, PosEkAbs);
	FillList.SetItemText (_T("0,00 "), rowCount, PosEkProz);
	IPR ipr;
	memcpy (&ipr, &ipr_null, sizeof (IPR));
	ipr.mdn = mdn;
	ipr.pr_gr_stuf = pr_gr_stuf;
    CPreise *p = new CPreise (CString ("0,00"), CString ("0,00"),
							  ipr);	
	ListRows.Add (p);
	return TRUE;
}

void CPrGrListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CPrGrListCtrl::RunItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
*/
}

void CPrGrListCtrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CPrGrListCtrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CPrGrListCtrl::OnChoice ()
{
	if (EditCol == PosA)
	{
		OnAChoice (CString (_T("")));
	}
}

void CPrGrListCtrl::OnAChoice (CString& Search)
{
	AChoiceStat = TRUE;
	if (ChoiceA != NULL && !ModalChoiceA)
	{
		ChoiceA->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceA == NULL)
	{
		ChoiceA = new CChoiceA (this);
	    ChoiceA->IsModal = ModalChoiceA;
	    ChoiceA->HideEnter = FALSE;
		ChoiceA->IdArrDown = IDI_HARROWDOWN;
		ChoiceA->IdArrUp   = IDI_HARROWUP;
		ChoiceA->IdArrNo   = IDI_HARROWNO;
		ChoiceA->CreateDlg ();
	}

    ChoiceA->SetDbClass (&A_bas);
	ChoiceA->SearchText = Search;
	if (ModalChoiceA)
	{
			ChoiceA->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceA->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceA->MoveWindow (&rect);
		ChoiceA->SetFocus ();
		return;
	}
    if (ChoiceA->GetState ())
    {
		  CABasList *abl = ChoiceA->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  AChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		  A_bas.a_bas.a = abl->a;
		  A_bas.dbreadfirst ();
		  CString Text;
		  Text.Format (_T("%.0lf"), A_bas.a_bas.a);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
		  CString ABz1;
		  ABz1.Format (_T("%s"), A_bas.a_bas.a_bz1);
		  FillList.SetItemText (ABz1.GetBuffer (), EditRow, PosABz1);
    }
	else
	{
		  AChoiceStat = FALSE;
		  SearchListCtrl.Edit.SetFocus ();
		  CString Text;
		  SearchListCtrl.Edit.GetWindowText (Text);
          SetSearchSel (Text);
	}
}


void CPrGrListCtrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}

void CPrGrListCtrl::ReadABz1 ()
{
	if (EditCol != PosA) return;
    memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
    CString Text;
	SearchListCtrl.Edit.GetWindowText (Text);
	if (!CStrFuncs::IsDecimal (Text))
	{
		OnAChoice (Text);
	    SearchListCtrl.Edit.GetWindowText (Text);
		Text.Format (_T("%.0lf"), CStrFuncs::StrToDouble (Text.GetBuffer ()));
	    SearchListCtrl.Edit.SetWindowText (Text);
		if (!AChoiceStat)
		{
			EditCol --;
			return;
		}
	}
	A_bas.a_bas.a = CStrFuncs::StrToDouble (Text);
	if (A_bas.dbreadfirst () != 0)
	{
		MessageBox (_T("Artikel nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return;
	}
    CString ABz1;
	ABz1.Format (_T("%s"), A_bas.a_bas.a_bz1);
	FillList.SetItemText (ABz1.GetBuffer (), EditRow, PosABz1);
	if (Mode == STANDARD)
	{
		return;
	}
	memcpy (&Ipr.ipr, &ipr_null, sizeof (IPR));
	Ipr.ipr.mdn = mdn;
	Ipr.ipr.pr_gr_stuf = pr_gr_stuf;
	Ipr.ipr.kun_pr = 0l;
	Ipr.ipr.kun = 0l;
	Ipr.ipr.a = A_bas.a_bas.a;
	Ipr.dbreadfirst ();
	Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 301111

	CString VkPr;
	DoubleToString (Ipr.ipr.vk_pr_eu, VkPr, 4);
	FillList.SetItemText (VkPr.GetBuffer (), EditRow, PosVkPr + 2);
	CString LdPr;
	DoubleToString (Ipr.ipr.ld_pr_eu, LdPr, 2);
	FillList.SetItemText (LdPr.GetBuffer (), EditRow, PosLdPr + 2);
}

void CPrGrListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	Text = cText.Trim ();
}

void CPrGrListCtrl::TestIprIndex ()
{
	int Items = GetItemCount ();
	if (Items <= 1) return;
	CString Value;
	GetColValue (EditRow, PosA, Value);
	double rA = CStrFuncs::StrToDouble (Value);
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosA, Value);
 	    double lA = CStrFuncs::StrToDouble (Value);
		if (lA != rA) continue;
		ListRows.Drop (EditRow);
 	    DeleteItem (i);
		InvalidateRect (NULL);
		if ( i < EditRow) EditRow --;
		return;
	}
}

void CPrGrListCtrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}

void CPrGrListCtrl::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

