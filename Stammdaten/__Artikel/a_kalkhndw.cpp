#include "stdafx.h"
#include "a_kalkhndw.h"

struct A_KALKHNDW a_kalkhndw, a_kalkhndw_null, a_kalkhndw_def;

void A_KALKHNDW_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)    &a_kalkhndw.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalkhndw.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalkhndw.a,   SQLDOUBLE, 0);
    sqlout ((double *) &a_kalkhndw.a,SQLDOUBLE,0);
    sqlout ((short *) &a_kalkhndw.bearb_fil,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.bearb_lad,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.bearb_sk,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_kalkhndw.fil,SQLSHORT,0);
    sqlout ((double *) &a_kalkhndw.fil_ek_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.fil_ek_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.lad_vk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.lad_vk_vollk,SQLDOUBLE,0);
    sqlout ((short *) &a_kalkhndw.mdn,SQLSHORT,0);
    sqlout ((double *) &a_kalkhndw.pr_ek1,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.pr_ek2,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.pr_ek3,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sk_teilk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sk_vollk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sp_fil,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sp_lad,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.sp_vk,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.we_me1,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.we_me2,SQLDOUBLE,0);
    sqlout ((double *) &a_kalkhndw.we_me3,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &a_kalkhndw.dat,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &a_kalkhndw.aend_dat,SQLDATE,0);
            cursor = sqlcursor (_T("select a_kalkhndw.a,  ")
_T("a_kalkhndw.bearb_fil,  a_kalkhndw.bearb_lad,  a_kalkhndw.bearb_sk,  ")
_T("a_kalkhndw.delstatus,  a_kalkhndw.fil,  a_kalkhndw.fil_ek_teilk,  ")
_T("a_kalkhndw.fil_ek_vollk,  a_kalkhndw.lad_vk_teilk,  ")
_T("a_kalkhndw.lad_vk_vollk,  a_kalkhndw.mdn,  a_kalkhndw.pr_ek1,  ")
_T("a_kalkhndw.pr_ek2,  a_kalkhndw.pr_ek3,  a_kalkhndw.sk_teilk,  ")
_T("a_kalkhndw.sk_vollk,  a_kalkhndw.sp_fil,  a_kalkhndw.sp_lad,  ")
_T("a_kalkhndw.sp_vk,  a_kalkhndw.we_me1,  a_kalkhndw.we_me2,  ")
_T("a_kalkhndw.we_me3,  a_kalkhndw.dat,  a_kalkhndw.aend_dat from a_kalkhndw ")

#line 14 "a_kalkhndw.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
    sqlin ((double *) &a_kalkhndw.a,SQLDOUBLE,0);
    sqlin ((short *) &a_kalkhndw.bearb_fil,SQLSHORT,0);
    sqlin ((short *) &a_kalkhndw.bearb_lad,SQLSHORT,0);
    sqlin ((short *) &a_kalkhndw.bearb_sk,SQLSHORT,0);
    sqlin ((short *) &a_kalkhndw.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_kalkhndw.fil,SQLSHORT,0);
    sqlin ((double *) &a_kalkhndw.fil_ek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.fil_ek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.lad_vk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.lad_vk_vollk,SQLDOUBLE,0);
    sqlin ((short *) &a_kalkhndw.mdn,SQLSHORT,0);
    sqlin ((double *) &a_kalkhndw.pr_ek1,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.pr_ek2,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.pr_ek3,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.sp_fil,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.sp_lad,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.sp_vk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.we_me1,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.we_me2,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.we_me3,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_kalkhndw.dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_kalkhndw.aend_dat,SQLDATE,0);
            sqltext = _T("update a_kalkhndw set ")
_T("a_kalkhndw.a = ?,  a_kalkhndw.bearb_fil = ?,  ")
_T("a_kalkhndw.bearb_lad = ?,  a_kalkhndw.bearb_sk = ?,  ")
_T("a_kalkhndw.delstatus = ?,  a_kalkhndw.fil = ?,  ")
_T("a_kalkhndw.fil_ek_teilk = ?,  a_kalkhndw.fil_ek_vollk = ?,  ")
_T("a_kalkhndw.lad_vk_teilk = ?,  a_kalkhndw.lad_vk_vollk = ?,  ")
_T("a_kalkhndw.mdn = ?,  a_kalkhndw.pr_ek1 = ?,  a_kalkhndw.pr_ek2 = ?,  ")
_T("a_kalkhndw.pr_ek3 = ?,  a_kalkhndw.sk_teilk = ?,  ")
_T("a_kalkhndw.sk_vollk = ?,  a_kalkhndw.sp_fil = ?,  ")
_T("a_kalkhndw.sp_lad = ?,  a_kalkhndw.sp_vk = ?,  ")
_T("a_kalkhndw.we_me1 = ?,  a_kalkhndw.we_me2 = ?,  ")
_T("a_kalkhndw.we_me3 = ?,  a_kalkhndw.dat = ?,  ")
_T("a_kalkhndw.aend_dat = ? ")

#line 18 "a_kalkhndw.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?");
            sqlin ((short *)    &a_kalkhndw.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalkhndw.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalkhndw.a,   SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)    &a_kalkhndw.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalkhndw.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalkhndw.a,   SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_kalkhndw ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
            sqlin ((short *)    &a_kalkhndw.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalkhndw.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalkhndw.a,   SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_kalkhndw set delstatus = 0 ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
            sqlin ((short *)    &a_kalkhndw.mdn, SQLSHORT, 0);
            sqlin ((short *)    &a_kalkhndw.fil, SQLSHORT, 0);
            sqlin ((double *)   &a_kalkhndw.a,   SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_kalkhndw ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and a = ?"));
    sqlin ((double *) &a_kalkhndw.a,SQLDOUBLE,0);
    sqlin ((short *) &a_kalkhndw.bearb_fil,SQLSHORT,0);
    sqlin ((short *) &a_kalkhndw.bearb_lad,SQLSHORT,0);
    sqlin ((short *) &a_kalkhndw.bearb_sk,SQLSHORT,0);
    sqlin ((short *) &a_kalkhndw.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_kalkhndw.fil,SQLSHORT,0);
    sqlin ((double *) &a_kalkhndw.fil_ek_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.fil_ek_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.lad_vk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.lad_vk_vollk,SQLDOUBLE,0);
    sqlin ((short *) &a_kalkhndw.mdn,SQLSHORT,0);
    sqlin ((double *) &a_kalkhndw.pr_ek1,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.pr_ek2,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.pr_ek3,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.sk_teilk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.sp_fil,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.sp_lad,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.sp_vk,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.we_me1,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.we_me2,SQLDOUBLE,0);
    sqlin ((double *) &a_kalkhndw.we_me3,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_kalkhndw.dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_kalkhndw.aend_dat,SQLDATE,0);
            ins_cursor = sqlcursor (_T("insert into a_kalkhndw (")
_T("a,  bearb_fil,  bearb_lad,  bearb_sk,  delstatus,  fil,  fil_ek_teilk,  ")
_T("fil_ek_vollk,  lad_vk_teilk,  lad_vk_vollk,  mdn,  pr_ek1,  pr_ek2,  pr_ek3,  ")
_T("sk_teilk,  sk_vollk,  sp_fil,  sp_lad,  sp_vk,  we_me1,  we_me2,  we_me3,  dat,  ")
_T("aend_dat) ")

#line 48 "a_kalkhndw.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 50 "a_kalkhndw.rpp"
}
