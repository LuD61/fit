#include "StdAfx.h"
#include "Calculate.h"

CCalculate::CCalculate(void)
{
	value1 = 0.0;
	value2 = 0.0;
	percent = 0.0;
	MarktSpPar = ReadSysPar;
	mwst = 19.0;
}

CCalculate::~CCalculate(void)
{
}

void CCalculate::SetValues (double value1, double value2)
{
	this->value1 = value1;
	this->value2 = value2;
}

void CCalculate::SetMwst (double mwst)
{
	this->mwst = mwst;
}

double CCalculate::GetPercent ()
{
	return percent;
}

double CCalculate::execute (double value1, double value2)
{
	this->value1 = value1;
	this->value2 = value2;
	return execute ();
}

double CCalculate::execute ()
{
	if (MarktSpPar == ReadSysPar)
	{
		_tcscpy (sys_par.sys_par_nam, "markt_sp_par");
		int dsqlstatus = Sys_par.dbreadfirst ();
		if (dsqlstatus == 0)
		{
			MarktSpPar = _tstoi (sys_par.sys_par_wrt);
		}
		else
		{
			MarktSpPar = AufschlagBrutto;
		}
	}

	if (value1 == 0.0 || value2 == 0.0)
	{
		return 0.0;
	}
	if (MarktSpPar == AufschlagBrutto)
	{
		double v1 = value1;
		double v2 = value2;
		double mwstp = (100 - mwst) / 100;
		v2 *= mwstp;
		percent = v2/v1 * 100 - 100;
	}

	else if (MarktSpPar == AufschlagNetto)
	{
		double v1 = value1;
		double v2 = value2;
		percent = v2/v1 * 100 - 100;
	}

	else if (MarktSpPar == AbschlagBrutto)
	{
		double v1 = value1;
		double v2 = value2;
		double mwstp = (100 - mwst) / 100;
		v2 *= mwstp;
		percent = 100 - v1/v2*100;
	}

	else if (MarktSpPar == AbschlagNetto)
	{
		double v1 = value1;
		double v2 = value2;
		percent = 100 - v1/v2*100;
	}

	return percent;
}

double CCalculate::GetSk (short mdn, short fil, double a)
{
	memcpy (&A_kalkpreis.a_kalkpreis, &a_kalkpreis_null, sizeof (A_KALKPREIS));
	A_kalkpreis.a_kalkpreis.mdn = mdn;
	A_kalkpreis.a_kalkpreis.fil = fil;
	A_kalkpreis.a_kalkpreis.a   = a;
	while (A_kalkpreis.dbreadfirst () != 0)
	{
		if (A_kalkpreis.a_kalkpreis.fil > 0)
		{
			A_kalkpreis.a_kalkpreis.fil = 0;
		}
		else if (A_kalkpreis.a_kalkpreis.mdn > 0)
		{
			A_kalkpreis.a_kalkpreis.mdn = 0;
		}
		else
		{
			break;
		}
	}
	return A_kalkpreis.a_kalkpreis.sk_vollk;
}

double CCalculate::GetEk ()
{
	return A_kalkpreis.a_kalkpreis.fil_ek_vollk;
}

double CCalculate::GetVk ()
{
	return A_kalkpreis.a_kalkpreis.fil_vk_vollk;
}
