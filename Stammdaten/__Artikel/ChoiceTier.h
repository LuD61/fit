#ifndef _CHOICETIER_DEF
#define _CHOICETIER_DEF

#include "ChoiceX.h"
#include "TierList.h"
#include <vector>

class CChoiceTier : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
	protected:     
		DECLARE_MESSAGE_MAP()

    public :
	    std::vector<CTierList *> TierList;
      	CChoiceTier(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceTier(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchTier (CListCtrl *,  LPTSTR);
        void SearchTxt (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CTierList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		virtual void OnEnter ();
};
#endif
