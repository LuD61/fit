// Emb.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "Emb.h"
#include "UniFormField.h"
#include "choiceaquery.h"
#include ".\emb.h"
#include "StrFuncs.h"


// CEmb-Dialogfeld

IMPLEMENT_DYNAMIC(CEmb, CDialog)
CEmb::CEmb(CWnd* pParent /*=NULL*/)
	: CDialog(CEmb::IDD, pParent)
{
	AkrzGen = FALSE;
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
	dcs_a_par = -1;
	waa_a_par = -1;
    MdnCursor = -1;
	FilCursor = -1;
}

CEmb::~CEmb()
{
}

void CEmb::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LUNT_EMB, m_LUntEmb);
	DDX_Control(pDX, IDC_UNT_EMB, m_UntEmb);
	DDX_Control(pDX, IDC_LEMB, m_LEmb);
	DDX_Control(pDX, IDC_EMB, m_Emb);
	DDX_Control(pDX, IDC_LEMB_BZ, m_LEmbBz);
	DDX_Control(pDX, IDC_EMB_BZ, m_EmbBz);
	DDX_Control(pDX, IDC_LANZ_EMB, m_LAnzEmb);
	DDX_Control(pDX, IDC_ANZ_EMB, m_AnzEmb);
	DDX_Control(pDX, IDC_LA_PFA, m_LAPfa);
	DDX_Control(pDX, IDC_A_PFA, m_APfa);
	DDX_Control(pDX, IDC_EMB_VK_KZ, m_EmbVkKz);
	DDX_Control(pDX, IDC_LTARA_PROZ, m_LTaraProz);
	DDX_Control(pDX, IDC_TARA_PROZ, m_TaraProz);
	DDX_Control(pDX, IDC_LTARA, m_LTara);
	DDX_Control(pDX, IDC_TARA, m_Tara);
	DDX_Control(pDX, IDC_LA_KRZ, m_LAkrz);
	DDX_Control(pDX, IDC_A_KRZ, m_Akrz);
	DDX_Control(pDX, IDC_EMB_GROUP, m_EmbGroup);
	DDX_Control(pDX, IDC_PR_VK, m_PrVk);
}


BEGIN_MESSAGE_MAP(CEmb, CDialog)
	ON_WM_CTLCOLOR ()
	ON_COMMAND(IDC_APFACHOICE, OnChoiceAPfa)
	ON_BN_CLICKED(IDC_EMBSAVE, OnSave)
	ON_BN_CLICKED(IDC_EMB_VK_KZ, OnBnClickedEmbVkKz)
END_MESSAGE_MAP()



// CEmb-Meldungshandler

BOOL CEmb::OnInitDialog()
{

	CDialog::OnInitDialog();

    m_Save.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_EMBSAVE);
	m_Save.LoadBitmap (IDB_SAVE);
	m_Save.SetToolTip (_T("Emballage\nspeichern"));

	A_bas  = &Basis->A_bas;
	A_hndw = &Basis->A_hndw;
	A_eig  = &Basis->A_eig;
	A_emb  = &Basis->A_emb;
	A_krz  = &Basis->A_krz;
	Sys_par = &Basis->Sys_par;

    SetAkrzField ();


	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));

	Form.Add (new CFormField    (&m_UntEmb,EDIT,     (double *) &A_emb->a_emb.unt_emb, VDOUBLE));
	Form.Add (new CFormField    (&m_Emb,EDIT,        (double *) &A_emb->a_emb.emb, VDOUBLE));
    Form.Add (new CUniFormField (&m_EmbBz,EDIT,      (LPTSTR)   A_emb->a_emb.emb_bz, VCHAR));
	Form.Add (new CFormField    (&m_AnzEmb,EDIT,     (short *)  &A_emb->a_emb.anz_emb, VSHORT));
	Form.Add (new CFormField    (&m_APfa,EDIT,       (double *) &A_emb->a_emb.a_pfa, VDOUBLE));
    Form.Add (new CUniFormField (&m_EmbVkKz,CHECKBOX,(LPTSTR)   A_emb->a_emb.emb_vk_kz, VCHAR));
	Form.Add (new CFormField    (&m_TaraProz,EDIT,   (double *) &A_emb->a_emb.tara_proz, VDOUBLE));
	Form.Add (new CFormField    (&m_Tara,    EDIT,   (double *) &A_emb->a_emb.tara, VDOUBLE));
	Form.Add (new CFormField    (&m_Akrz,    EDIT,   (double *) &A_krz->a_krz.a_krz, VLONG));
	Form.Add (new CFormField    (&m_PrVk,    EDIT,   (double *) &A_pr.a_pr.pr_vk, VDOUBLE, 6, 2));
	m_Emb.SetLimitText (13);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (10, 10);
    CtrlGrid.SetCellHeight (15);
//    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetFontCellHeight (this, &Font);
	CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	m_UntEmb.SetReadOnly ();
	CCtrlInfo *c_EmbGroup = new CCtrlInfo (&m_EmbGroup, 0, 0, 5, 8);
	CtrlGrid.Add (c_EmbGroup);

	CCtrlInfo *c_LUntEmb = new CCtrlInfo (&m_LUntEmb, 1, 1, 1, 1);
	CtrlGrid.Add (c_LUntEmb);

	CCtrlInfo *c_UntEmb = new CCtrlInfo (&m_UntEmb, 2, 1, 1, 1);
	CtrlGrid.Add (c_UntEmb);

	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 3, 1, 1, 1);
//	c_Save->SetCellPos (30, 0, 0, 0);
	CtrlGrid.Add (c_Save);

	CCtrlInfo *c_LEmb = new CCtrlInfo (&m_LEmb, 1, 2, 1, 1);
	CtrlGrid.Add (c_LEmb);

	CCtrlInfo *c_Emb = new CCtrlInfo (&m_Emb, 2, 2, 1, 1);
	CtrlGrid.Add (c_Emb);

	CCtrlInfo *c_LEmbBz = new CCtrlInfo (&m_LEmbBz, 1, 3, 1, 1);
	CtrlGrid.Add (c_LEmbBz);

	CCtrlInfo *c_EmbBz = new CCtrlInfo (&m_EmbBz, 2, 3, 2, 1);
	CtrlGrid.Add (c_EmbBz);

	CCtrlInfo *c_LAnzEmb = new CCtrlInfo (&m_LAnzEmb, 1, 5, 1, 1);
	CtrlGrid.Add (c_LAnzEmb);

	CCtrlInfo *c_AnzEmb = new CCtrlInfo (&m_AnzEmb, 2, 5, 1, 1);
	CtrlGrid.Add (c_AnzEmb);

	CCtrlInfo *c_LAPfa = new CCtrlInfo (&m_LAPfa, 1, 6, 1, 1);
	CtrlGrid.Add (c_LAPfa);

//Grid Pfandartikel 
	APfaGrid.Create (this, 1, 2);
    APfaGrid.SetBorder (0, 0);
    APfaGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_APfa = new CCtrlInfo (&m_APfa, 0, 0, 1, 1);
	APfaGrid.Add (c_APfa);
	CtrlGrid.CreateChoiceButton (m_APfaChoice, IDC_APFACHOICE, this);
	CCtrlInfo *c_APfaChoice = new CCtrlInfo (&m_APfaChoice, 1, 0, 1, 1);
	APfaGrid.Add (c_APfaChoice);

	CCtrlInfo *c_APfaGrid = new CCtrlInfo (&APfaGrid, 2, 6, 1, 1);
	CtrlGrid.Add (c_APfaGrid);
/*
	CCtrlInfo *c_APfa = new CCtrlInfo (&m_APfa, 2, 6, 1, 1);
	CtrlGrid.Add (c_APfa);
*/

	CCtrlInfo *c_EmbVkKz = new CCtrlInfo (&m_EmbVkKz, 2, 7, 1, 1);
	CtrlGrid.Add (c_EmbVkKz);

	CCtrlInfo *c_PrVk = new CCtrlInfo (&m_PrVk, 3, 7, 1, 1);
	CtrlGrid.Add (c_PrVk);

	CCtrlInfo *c_LTaraProz = new CCtrlInfo (&m_LTaraProz, 1, 8, 1, 1);
	CtrlGrid.Add (c_LTaraProz);

	CCtrlInfo *c_TaraProz = new CCtrlInfo (&m_TaraProz, 2, 8, 1, 1);
	CtrlGrid.Add (c_TaraProz);

	CCtrlInfo *c_LTara = new CCtrlInfo (&m_LTara, 1, 9, 1, 1);
	CtrlGrid.Add (c_LTara);

	CCtrlInfo *c_Tara = new CCtrlInfo (&m_Tara, 2, 9, 1, 1);
	CtrlGrid.Add (c_Tara);

	CCtrlInfo *c_LAkrz = new CCtrlInfo (&m_LAkrz, 1, 10, 1, 1);
	CtrlGrid.Add (c_LAkrz);

	CCtrlInfo *c_Akrz = new CCtrlInfo (&m_Akrz, 2, 10, 1, 1);
	CtrlGrid.Add (c_Akrz);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_Save.Tooltip.SetFont (&Font);
	m_Save.Tooltip.SetSize ();

	m_PrVk.ShowWindow (SW_HIDE);

	CtrlGrid.Display ();

	Form.Show ();
    m_Emb.SetFocus ();
	return FALSE;
}

HBRUSH CEmb::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			m_Save.SetBkColor(DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CEmb::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}
	
BOOL CEmb::PreTranslateMessage(MSG* pMsg)
{
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
                if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				((CDbPropertyPage *) GetParent ()->GetParent ())->Update->Back ();
//				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Basis->OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				UpdateList ();
				((CDbPropertyPage *) GetParent ()->GetParent ())->Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return FALSE;
}

void CEmb::SetBasis (CBasisdatenPage *Basis)
{
	this->Basis = Basis;
}

void CEmb::Update ()
{
	SetAkrzField ();
	memcpy (&A_krz->a_krz, &a_krz_null, sizeof (A_KRZ));
	A_krz->a_krz.a_herk = A_emb->a_emb.emb;
	A_krz->dbreada_herkfirst ();
	if (A_krz->a_krz.a_krz == 0l && A_emb->a_emb.emb != 0.0)
	{
		FillAkrz ();
	}
	CString EmbVkKz;
	EmbVkKz = A_emb->a_emb.emb_vk_kz;
	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	if (EmbVkKz == _T("J"))
	{
		    if (MdnCursor == -1)
			{
				A_pr.sqlin ((short *) &A_pr.a_pr.mdn, SQLSHORT, 0);
				A_pr.sqlout ((short *) &A_pr.a_pr.mdn_gr, SQLSHORT, 0);
				MdnCursor = A_pr.sqlcursor (_T("select mdn_gr from mdn where mdn = ?"));
				A_pr.sqlin ((short *) &A_pr.a_pr.mdn, SQLSHORT, 0);
				A_pr.sqlin ((short *) &A_pr.a_pr.fil, SQLSHORT, 0);
				A_pr.sqlout ((short *) &A_pr.a_pr.fil_gr, SQLSHORT, 0);
				FilCursor = A_pr.sqlcursor (_T("select fil_gr from fil where mdn = ? and fil = ?"));
			}
		    m_PrVk.ShowWindow (SW_SHOWNORMAL);
			A_pr.a_pr.a = A_emb->a_emb.emb;
			A_pr.a_pr.key_typ_dec13 = A_emb->a_emb.emb;
			A_pr.a_pr.mdn_gr = 0;
			A_pr.a_pr.mdn = A_bas->a_bas.mdn;
			if (A_pr.a_pr.mdn != 0)
			{
				A_pr.sqlopen (MdnCursor);
				A_pr.sqlfetch (MdnCursor);
			}
			A_pr.a_pr.fil_gr = 0;
			A_pr.a_pr.fil = A_bas->a_bas.fil;
			if (A_pr.a_pr.fil != 0)
			{
				A_pr.sqlopen (FilCursor);
				A_pr.sqlfetch (FilCursor);
			}
			A_pr.dbreadfirst ();
	}
	else
	{
		    m_PrVk.ShowWindow (SW_HIDE);
	}
	Form.Show ();
}


BOOL CEmb::OnReturn ()
{
	CWnd * Control = GetFocus ();
	if (Control == &m_Emb)
	{
		if (!TestEmb ()) return FALSE;
	}
	else if (!AkrzGen && Control == &m_Akrz)
	{
		UpdateList ();
		return TRUE;
	}
	else if (Control == &m_Tara)
	{
		UpdateList ();
		return TRUE;
	}
	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
	return TRUE;
}

BOOL CEmb::TestEmb ()
{
	Form.Get ();
	double unt_emb = A_emb->a_emb.unt_emb;
	if (A_emb->dbreadfirst () == 100)
	{
		_tcscpy (A_emb->a_emb.emb_bz, A_bas->a_bas.a_bz1);
		FillAkrz ();
		Form.Show ();
		return TRUE;
	}


	if (unt_emb == A_emb->a_emb.unt_emb)
	{
		return TRUE;
	}

	CString Msg;
    Msg.Format (_T("Die Embalage %.0lf ist mit der Embalage %.0lf verkn�pft"),
		        A_emb->a_emb.emb, A_emb->a_emb.unt_emb);
	MessageBox (Msg.GetBuffer ());
    A_emb->a_emb.unt_emb = unt_emb;
	return FALSE;
}

void CEmb::UpdateList ()
{
	Form.Get ();
	if (A_emb->a_emb.unt_emb == A_emb->a_emb.emb)
	{
		MessageBox (_T("Untere Embalage unf Embalage d�rfen nicht gleich sein"), 
			        NULL, MB_ICONERROR);
		m_Emb.SetFocus ();
		return;
	}
	double unt_emb = A_emb->a_emb.unt_emb;
	int dsqlstatus = A_emb->dbreadfirst_emb ();
	if (A_emb->a_emb.unt_emb != unt_emb)
	{
		MessageBox (_T("Die Emballage existiert schon in einer anderen Emballagenkette"),
			        NULL,
					MB_ICONERROR);
		Form.Get ();
		m_Emb.SetFocus ();
		return;
	}
	if (A_emb->a_emb.anz_emb <= 0)
	{
		MessageBox (_T("Die Anzahl mu� gr��er als 0 sein"),
			        NULL,
					MB_ICONERROR);
		Form.Get ();
		m_AnzEmb.SetFocus ();
		return;
	}

	Form.Get ();
	A_emb->dbupdate ();
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update ();
	}
	m_Emb.SetFocus ();
	CString EmbVkKz = A_emb->a_emb.emb_vk_kz;

	if (EmbVkKz == _T("J"))
	{
		A_BAS_CLASS Artikel;
		memcpy (&Artikel.a_bas, &A_bas->a_bas, sizeof (A_bas->a_bas));
		Artikel.a_bas.a = A_emb->a_emb.emb;
		_tcscpy (Artikel.a_bas.a_bz1, A_emb->a_emb.emb_bz);
        Artikel.a_bas.a_typ = 99;
		Artikel.dbupdate ();
		if (A_pr.a_pr.pr_vk != 0.0)
		{
		    if (MdnCursor == -1)
			{
				A_pr.sqlin ((short *) &A_pr.a_pr.mdn, SQLSHORT, 0);
				A_pr.sqlout ((short *) &A_pr.a_pr.mdn_gr, SQLSHORT, 0);
				MdnCursor = A_pr.sqlcursor (_T("select mdn_gr from mdn where mdn = ?"));
				A_pr.sqlin ((short *) &A_pr.a_pr.mdn, SQLSHORT, 0);
				A_pr.sqlin ((short *) &A_pr.a_pr.fil, SQLSHORT, 0);
				A_pr.sqlout ((short *) &A_pr.a_pr.fil_gr, SQLSHORT, 0);
				FilCursor = A_pr.sqlcursor (_T("select fil_gr from fil where mdn = ? and fil = ?"));
			}
			A_pr.a_pr.a = A_emb->a_emb.emb;
			A_pr.a_pr.key_typ_dec13 = A_emb->a_emb.emb;
			A_pr.a_pr.akt = -1;
			A_pr.a_pr.mdn_gr = 0;
			A_pr.a_pr.mdn = A_bas->a_bas.mdn;
			if (A_pr.a_pr.mdn != 0)
			{
				A_pr.sqlopen (MdnCursor);
				A_pr.sqlfetch (MdnCursor);
			}
			A_pr.a_pr.fil_gr = 0;
			A_pr.a_pr.fil = A_bas->a_bas.fil;
			if (A_pr.a_pr.fil != 0)
			{
				A_pr.sqlopen (FilCursor);
				A_pr.sqlfetch (FilCursor);
			}
			A_pr.dbreadfirst ();
			A_pr.a_pr.pr_vk_euro = A_pr.a_pr.pr_vk;
			CString Date;
			CStrFuncs::SysDate (Date);
			A_pr.ToDbDate (Date, &A_pr.a_pr.bearb);
			A_pr.dbupdate ();
		}
	}
}


void CEmb::SetAkrzField ()
{
	if (dcs_a_par == -1)
	{
		strcpy ((LPSTR) sys_par.sys_par_nam, "dcs_a_par");
		if (Sys_par->dbreadfirst () == 0)
		{
			LPSTR p = (LPSTR) sys_par.sys_par_wrt;
			CDbUniCode::DbToUniCode (sys_par.sys_par_wrt, p);
			dcs_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}

	if (waa_a_par == -1)
	{
		strcpy ((LPSTR) sys_par.sys_par_nam, "waa_a_par");
		if (Sys_par->dbreadfirst () == 0)
		{
			LPSTR p = (LPSTR) sys_par.sys_par_wrt;
			CDbUniCode::DbToUniCode (sys_par.sys_par_wrt, p);
			waa_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}

	if (A_bas->a_bas.a_typ == Basis->Hndw && _tcscmp (A_hndw->a_hndw.verk_art, _T("K")) == 0)
	{
		if (dcs_a_par == 0)
		{
			AkrzGen = FALSE;
			m_LAkrz.ShowWindow (SW_HIDE);
			m_Akrz.ShowWindow (SW_HIDE);
		}
		else if (dcs_a_par == 1)
		{
			AkrzGen = TRUE;
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (FALSE);
			AkrzGen = TRUE;
		}
		else if (dcs_a_par == 2)
		{
			AkrzGen = FALSE;
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (TRUE);
			AkrzGen = FALSE;
		}
	}
	else if (A_bas->a_bas.a_typ == Basis->Hndw && (_tcscmp (A_hndw->a_hndw.verk_art, _T("B")) == 0 ||
												 _tcscmp (A_hndw->a_hndw.verk_art, _T("C")) == 0 ||
												 _tcscmp (A_hndw->a_hndw.verk_art, _T("S")) == 0))
	{
		AkrzGen = FALSE;
		if (waa_a_par == 0)
		{
			m_LAkrz.ShowWindow (SW_HIDE);
			m_Akrz.ShowWindow (SW_HIDE);
		}
		else if (waa_a_par == 1)
		{
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (FALSE);
			AkrzGen = TRUE;
		}
		else if (waa_a_par == 2)
		{
			AkrzGen = FALSE;
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (TRUE);
			AkrzGen = FALSE;
		}
	}

	else if (A_bas->a_bas.a_typ == Basis->Eig && _tcscmp (A_eig->a_eig.verk_art, _T("K")) == 0)
	{
		AkrzGen = FALSE;
		if (dcs_a_par == 0)
		{
			m_LAkrz.ShowWindow (SW_HIDE);
			m_Akrz.ShowWindow (SW_HIDE);
		}
		else if (dcs_a_par == 1)
		{
			AkrzGen = TRUE;
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (FALSE);
		}
		else if (dcs_a_par == 2)
		{
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (TRUE);
		}
	}
	else if (A_bas->a_bas.a_typ == Basis->Eig && (_tcscmp (A_eig->a_eig.verk_art, _T("B")) == 0 ||
												 _tcscmp (A_eig->a_eig.verk_art, _T("C")) == 0 ||
												 _tcscmp (A_eig->a_eig.verk_art, _T("S")) == 0))
	{
		AkrzGen = FALSE;
		if (waa_a_par == 0)
		{
			m_LAkrz.ShowWindow (SW_HIDE);
			m_Akrz.ShowWindow (SW_HIDE);
		}
		else if (waa_a_par == 1)
		{
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (FALSE);
			AkrzGen = TRUE;
		}
		else if (waa_a_par == 2)
		{
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (TRUE);
		}
	}
	else
	{
			m_LAkrz.ShowWindow (SW_HIDE);
			m_Akrz.ShowWindow (SW_HIDE);
	}
}

void CEmb::FillAkrz ()
{

	if (A_hndw->a_hndw.a_krz != 0l)
	{
		A_krz->a_krz.a = A_bas->a_bas.a;
		A_krz->a_krz.a_krz = A_hndw->a_hndw.a_krz;
		A_krz->a_krz.a_herk = A_emb->a_emb.emb;
		strcpy ((LPSTR) A_krz->a_krz.herk_kz, "3"); 
		A_krz->a_krz.anz_a = 1;
		A_krz->dbupdate ();
		A_hndw->a_hndw.a_krz = 0l;
		return;
	}
	if (_tcscmp (A_hndw->a_hndw.verk_art, _T("K")) == 0)
	{
		if (dcs_a_par == 1)
		{
			if (!GenAkrz ())
			{
				MessageBox (_T("Es konnte keien LKurznummer generiert werden"));
				return;
			}
			A_krz->a_krz.a = A_bas->a_bas.a;
			A_krz->a_krz.a_herk = A_emb->a_emb.emb;
			strcpy ((LPSTR) A_krz->a_krz.herk_kz, "3"); 
			A_krz->a_krz.anz_a = A_emb->a_emb.anz_emb;
			A_krz->dbupdate ();
		}
		else if (dcs_a_par == 2)
		{
			if (A_bas->a_bas.a <= 999999.0)
			{
				A_krz->a_krz.a_krz = (long) A_bas->a_bas.a;
				A_krz->a_krz.a = A_hndw->a_hndw.a;
				A_krz->a_krz.a_herk = A_emb->a_emb.emb;
				strcpy ((LPSTR) A_krz->a_krz.herk_kz, "3"); 
				A_krz->a_krz.anz_a = A_emb->a_emb.anz_emb;
				A_krz->dbupdate ();
			}
		}
	}
	else if (_tcscmp (A_hndw->a_hndw.verk_art, _T("B")) == 0 ||
			 _tcscmp (A_hndw->a_hndw.verk_art, _T("C")) == 0 ||
			 _tcscmp (A_hndw->a_hndw.verk_art, _T("S")) == 0)
	{
			if (waa_a_par != 0)
			{
				if (A_bas->a_bas.a <= 999999.0)
				{
					A_krz->a_krz.a_krz = (long) A_bas->a_bas.a;
					A_krz->a_krz.a = A_hndw->a_hndw.a;
					A_krz->a_krz.a_herk = A_emb->a_emb.emb;
					strcpy ((LPSTR) A_krz->a_krz.herk_kz, "3"); 
					A_krz->a_krz.anz_a = A_emb->a_emb.anz_emb;
					A_krz->dbupdate ();
				}
			}
	}
}

BOOL CEmb::GenAkrz ()
{
	Basis->AutoNr.GenAutoNr (0, 0, _T("a_krz"));
	A_krz->a_krz.a_krz = Basis->AutoNr.Nr;
    if (A_krz->a_krz.a_krz == 0l) return FALSE;
	memcpy (&a_krz, &A_krz->a_krz, sizeof (A_KRZ));
    int dsqlstatus = A_krz->dbreadfirst ();
	if (dsqlstatus == 100) return TRUE;
	int i = 0;
	while (dsqlstatus != 100)
	{
		if (A_krz->a_krz.a == A_bas->a_bas.a) break;
		memcpy (&A_krz->a_krz, &a_krz, sizeof (A_KRZ));
		if (i == 100)
		{
			A_krz->a_krz.a_krz = 0;
			return FALSE;
		}
		Sleep (100);
		i ++;
		Basis->AutoNr.GenAutoNr (0, 0, _T("a_krz"));
		A_krz->a_krz.a_krz = Basis->AutoNr.Nr;
		if (A_krz->a_krz.a_krz == 0l) 
		{
			A_krz->a_krz.a_krz = 0;
			return FALSE;
		}
		memcpy (&a_krz, &A_krz->a_krz, sizeof (A_KRZ));
        dsqlstatus = A_krz->dbreadfirst ();
	}
	return TRUE;
}

void CEmb::OnChoiceAPfa()
{
	CChoiceAQuery Choice;

	Choice.Query = _T("where a_typ = 4");
	Choice.CreateDlg ();
    Choice.SetDbClass (A_bas);
	Choice.DoModal ();
    if (Choice.GetState ())
    {
		CABasList *abl = Choice.GetSelectedText (); 
		if (abl == NULL) return;
		A_emb->a_emb.a_pfa = abl->a;
		Form.Show ();
		m_APfa.SetFocus ();
    }
}

void CEmb::OnSave ()
{
	if (!TestEmb ()) return;
	UpdateList ();
	PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
}

void CEmb::OnBnClickedEmbVkKz()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	CString EmbVkKz = A_emb->a_emb.emb_vk_kz;
	if (EmbVkKz == _T("J"))
	{
		m_PrVk.ShowWindow (SW_SHOWNORMAL);
	}
	else
	{
		m_PrVk.ShowWindow (SW_HIDE);
	}
}
