#include "StdAfx.h"
#include "CtrlGridColor.h"

CCtrlGridColor::CCtrlGridColor(void) :
				CCtrlGrid ()
{
}

CCtrlGridColor::~CCtrlGridColor(void)
{
}

void CCtrlGridColor::CreateColorChoiceButton (CColorButton& Button, int Id, CWnd *Parent)
{
	TEXTMETRIC tm;
    CDC *cDC = Parent->GetDC ();
	cDC->SelectObject (Parent->GetFont ());
	cDC->GetTextMetrics (&tm);
	Parent->ReleaseDC (cDC);

	int bsize = GetSystemMetrics (SM_CYBORDER);
    int cWidth = tm.tmAveCharWidth;
	int cHeight = tm.tmHeight;
    int hPoint = (int) (double) ((double) cHeight / 8 + 0.9); 
    cHeight += 4 * bsize;
    cHeight = hPoint * 8 + 4 * bsize;
	int wPoint = (int) (double) ((double) cWidth / 4 + 0.9); 
	cWidth = wPoint * 4 + 2;

	CRect Rect;
	Rect.left = 0;
	Rect.top = 0;
	Rect.right = 2 * cWidth;
	Rect.bottom = cHeight;
	Button.Create (_T(".."), WS_CHILD | WS_VISIBLE, Rect,
							   Parent, Id);

	Button.ModifyStyle (WS_TABSTOP, 0);

//	COLORREF DynButtonColor = RGB (192, 192, 192);
	COLORREF DynButtonColor = CColorButton::DynColorGray;
//	COLORREF DynButtonColor = CColorButton::DynColorBlue;
//	COLORREF DynButtonColor = GetSysColor (COLOR_3DFACE);

	Button.nID = Id;
	Button.Orientation = Button.Center;
	Button.TextStyle = Button.Standard;
	Button.BorderStyle = Button.Solide;
//	Button.BorderStyle = Button.Rounded;
	Button.DynamicColor = TRUE;
	Button.TextColor =RGB (0, 0, 0);
//	Button.SetBkColor (DynButtonColor);
	Button.SetBkColor (GetSysColor (COLOR_3DFACE));
	Button.DynColor = DynButtonColor;
	Button.RolloverColor = RGB (192, 192, 192);
	Button.SetWindowText (_T(".."));
//	Button.LoadBitmap (IDB_POS_TEXT);
//	Button.LoadMask (IDB_POS_TEXT_MASK);
}

void CCtrlGridColor::CreateChoiceButton (CColorButton& Button, int Id, CWnd *Parent)
{
		CreateColorChoiceButton (Button, Id, Parent);
}

void CCtrlGridColor::CreateChoiceButton (CButton& Button, int Id, CWnd *Parent)
{
		CCtrlGrid::CreateChoiceButton (Button, Id, Parent);
}
