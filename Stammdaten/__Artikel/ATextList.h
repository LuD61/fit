#ifndef _REZ_LIST_DEF
#define _REZ_LIST_DEF
#pragma once

class CATextList
{
public:
	long sys;
	long txt_nr;
	CString txt;
	CATextList(void);
	CATextList(long, long, LPTSTR);
	~CATextList(void);
};
#endif
