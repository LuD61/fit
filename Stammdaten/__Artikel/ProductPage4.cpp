// ProduktPage4.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "ProductPage4.h"


// CProductPage4-Dialogfeld

IMPLEMENT_DYNAMIC(CProductPage4, CDbPropertyPage)

CProductPage4::CProductPage4()
	: CDbPropertyPage(CProductPage4::IDD)
{
	tabx = 0;
	taby = 21;
	cursor = -1;
	name_cursor = -1;
	delcursor = -1;
	ProductPage4Read = FALSE;
	ZusValues[0] = Prp_user.prp_user.wert1;
	ZusValues[1] = Prp_user.prp_user.wert2;
	ZusValues[2] = Prp_user.prp_user.wert3;
	ZusValues[3] = Prp_user.prp_user.wert4;
	ZusValues[4] = Prp_user.prp_user.wert5;
	ZusValues[5] = Prp_user.prp_user.wert6;
	ZusValues[6] = Prp_user.prp_user.wert7;
	ZusValues[7] = Prp_user.prp_user.wert8;
	ZusValues[8] = Prp_user.prp_user.wert9;
	ZusValues[9] = Prp_user.prp_user.wert10;
	ZusValues[10] = Prp_user.prp_user.wert11;
	ZusValues[11] = Prp_user.prp_user.wert12;
	ZusValues[12] = Prp_user.prp_user.wert13;
	ZusValues[13] = Prp_user.prp_user.wert14;
	ZusValues[14] = Prp_user.prp_user.wert15;
	ZusValues[15] = Prp_user.prp_user.wert16;
	ZusValues[16] = Prp_user.prp_user.wert17;
	ZusValues[17] = Prp_user.prp_user.wert18;
	ZusValues[18] = Prp_user.prp_user.wert19;
	ZusValues[19] = Prp_user.prp_user.wert20;
	ZusValues[20] = Prp_user.prp_user.wert21;
	ZusValues[21] = Prp_user.prp_user.wert22;
	ZusValues[22] = Prp_user.prp_user.wert23;
	ZusValues[23] = Prp_user.prp_user.wert24;
	ZusValues[24] = Prp_user.prp_user.wert25;
	ZusValues[25] = Prp_user.prp_user.wert26;
	ZusValues[26] = Prp_user.prp_user.wert27;
	ZusValues[27] = Prp_user.prp_user.wert28;
	ZusValues[28] = Prp_user.prp_user.wert29;
	ZusValues[29] = Prp_user.prp_user.wert30;

	ZusRecordNames[0] = Prp_user.prp_user.name1;
	ZusRecordNames[1] = Prp_user.prp_user.name2;
	ZusRecordNames[2] = Prp_user.prp_user.name3;
	ZusRecordNames[3] = Prp_user.prp_user.name4;
	ZusRecordNames[4] = Prp_user.prp_user.name5;
	ZusRecordNames[5] = Prp_user.prp_user.name6;
	ZusRecordNames[6] = Prp_user.prp_user.name7;
	ZusRecordNames[7] = Prp_user.prp_user.name8;
	ZusRecordNames[8] = Prp_user.prp_user.name9;
	ZusRecordNames[9] = Prp_user.prp_user.name10;
	ZusRecordNames[10] = Prp_user.prp_user.name11;
	ZusRecordNames[11] = Prp_user.prp_user.name12;
	ZusRecordNames[12] = Prp_user.prp_user.name13;
	ZusRecordNames[13] = Prp_user.prp_user.name14;
	ZusRecordNames[14] = Prp_user.prp_user.name15;
	ZusRecordNames[15] = Prp_user.prp_user.name16;
	ZusRecordNames[16] = Prp_user.prp_user.name17;
	ZusRecordNames[17] = Prp_user.prp_user.name18;
	ZusRecordNames[18] = Prp_user.prp_user.name19;
	ZusRecordNames[19] = Prp_user.prp_user.name20;
	ZusRecordNames[20] = Prp_user.prp_user.name21;
	ZusRecordNames[21] = Prp_user.prp_user.name22;
	ZusRecordNames[22] = Prp_user.prp_user.name23;
	ZusRecordNames[23] = Prp_user.prp_user.name24;
	ZusRecordNames[24] = Prp_user.prp_user.name25;
	ZusRecordNames[25] = Prp_user.prp_user.name26;
	ZusRecordNames[26] = Prp_user.prp_user.name27;
	ZusRecordNames[27] = Prp_user.prp_user.name28;
	ZusRecordNames[28] = Prp_user.prp_user.name29;
	ZusRecordNames[29] = Prp_user.prp_user.name30;
}

CProductPage4::~CProductPage4()
{
}

void CProductPage4::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LDESCRIPTION, m_LDescription);
	DDX_Control(pDX, IDC_PRP_USER_LIST, m_PrpUserList);
}


BEGIN_MESSAGE_MAP(CProductPage4, CDbPropertyPage)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()


// CProductPage4-Meldungshandler

BOOL CProductPage4::OnInitDialog()
{
	CPropertyPage::OnInitDialog();
	this->A_bas  = &Basis->A_bas;
	this->DlgBkColor = Basis->DlgBkColor;

	memcpy (&Prp_user.prp_user, &prp_user_null, sizeof (PRP_USER));
	Prp_user.prp_user.a  = -1.0;
	Prp_user.dbreadfirst ();
	FillNames ();
	ProductPage4Read = TRUE;
    Basis->ProductPage4Read = TRUE;

/*
	Prp_user.sqlout ((char *) &Prp_user.prp_user.name, SQLCHAR, sizeof (Prp_user.prp_user.name));
	name_cursor = Prp_user.sqlcursor (_T("select distinct name from prp_user order by name"));
*/

//	Prp_user.sqlin ((double *) &Prp_user.prp_user.a, SQLDOUBLE, 0);
//	Prp_user.sqlin ((char *) &Prp_user.prp_user.name, SQLCHAR, sizeof (Prp_user.prp_user.name));
//	Prp_user.sqlout ((char *) &Prp_user.prp_user.wert, SQLCHAR, sizeof (Prp_user.prp_user.wert));
//	cursor = Prp_user.sqlcursor (_T("select name, wert from prp_user where a = ? and name = ?"));

/*
	Prp_user.sqlin ((double *) &Prp_user.prp_user.a, SQLDOUBLE, 0);
	delcursor = Prp_user.sqlcursor (_T("delete from prp_user where a = ?"));
*/

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
/*
	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	BoldFont.CreateFontIndirect (&l);
*/

//	Form.Add (new CFormField (&m_ProdMass, EDIT,  (char *)    A_bas->a_bas.prod_mass , VCHAR));


	FillList = m_PrpUserList;
	FillList.SetStyle (LVS_REPORT);
	if (m_PrpUserList.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}

	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Bezeichnung"), 1, 300, LVCFMT_LEFT);
	FillList.SetCol (_T("Wert"), 2, 600, LVCFMT_LEFT);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LDescription  = new CCtrlInfo (&m_LDescription, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LDescription);
	CCtrlInfo *c_PrpUserList  = new CCtrlInfo (&m_PrpUserList, 1, 2, DOCKRIGHT, DOCKBOTTOM); 
	CtrlGrid.Add (c_PrpUserList);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
    Read ();
	Form.Show ();
	CtrlGrid.Display ();
	return FALSE;
}

HBRUSH CProductPage4::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
/*
		    if (pWnd == &m_LA_bz1)
			{
				pDC->SetTextColor (BoldColor);
			}
*/
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDbPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CProductPage4::OnSize(UINT nType, int cx, int cy)
{
/*
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	GetWindowRect (&pRect);
	GetParent ()->ScreenToClient (&pRect);
	int pcx = pRect.right -pRect.left - tabx;
	int pcy = pRect.bottom - pRect.top - taby;
	if (cx > pcx)
	{
		pcx = cx;
	}
	if (cy > pcy)
	{
		pcy = cy;
	}
	MoveWindow (tabx, taby, pcx, pcy);
	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = pcx - 10 - tabx;
	frame.bottom = pcy - 10 - taby;
	frame.top += 20;
	frame.left += 2;
    frame.right -= 10;
	frame.bottom -= 10;
*/
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
}

BOOL CProductPage4::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
/*
				if (OnReturn ())
				{
					return TRUE;
				}
*/
				m_PrpUserList.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_PrpUserList &&
					GetFocus ()->GetParent () != &m_PrpUserList )
				{

					break;
			    }
				m_PrpUserList.OnKeyD (VK_TAB);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}
			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
/*
				if (OnKeydown ())
				{
					return TRUE;
				}
*/
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
/*
				if (OnKeyup ())
				{
					return TRUE;
				}
*/
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
//				Update->Delete ();
				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return FALSE;
}

/*
BOOL CProductPage4::Read ()
{
	if (cursor == -1)
	{
		return FALSE;
	}
	m_PrpUserList.StopEnter ();
	m_PrpUserList.DeleteAllItems ();
	m_PrpUserList.vSelect.clear ();
	m_PrpUserList.ListRows.Init ();
	int i = 0;
	memcpy (&Prp_user.prp_user, &prp_user_null, sizeof (PRP_USER));
	Prp_user.prp_user.a  = A_bas->a_bas.a;
	Prp_user.sqlopen (cursor);
	while (Prp_user.sqlfetch (cursor) == 0)
	{
		FillList.InsertItem (i, 0);
		FillList.SetItemText (Prp_user.prp_user.name, i, 1);
		FillList.SetItemText (Prp_user.prp_user.wert, i, 2);
		i ++;
	}
	m_PrpUserList.StartEnter (1, 0);
	return TRUE;
}
*/

BOOL CProductPage4::Read ()
{

/*
	if (name_cursor == -1)
	{
		return FALSE;
	}
*/
	if (!ProductPage4Read)
	{
		return FALSE;
	}

	m_PrpUserList.StopEnter ();
	m_PrpUserList.DeleteAllItems ();
	m_PrpUserList.vSelect.clear ();
	m_PrpUserList.ListRows.Init ();
	int i = 0;
	memcpy (&Prp_user.prp_user, &prp_user_null, sizeof (PRP_USER));
	Prp_user.prp_user.a  = A_bas->a_bas.a;

/*
	Prp_user.sqlopen (name_cursor);
	while (Prp_user.sqlfetch (name_cursor) == 0)
	{
		FillList.InsertItem (i, 0);
		FillList.SetItemText (Prp_user.prp_user.name, i, 1);
		if (Prp_user.dbreadfirst () == 0)
		{
			FillList.SetItemText (Prp_user.prp_user.wert, i, 2);
		}
		else
		{
			FillList.SetItemText (_T(""), i, 2);
		}
		i ++;
	}
*/
	Prp_user.dbreadfirst ();
	ToRecordNames ();

	for (i = 0; i < MAXENTRIES; i ++)
	{
		if (ZusNames[i] != "")
		{
			FillList.InsertItem (i, 0);
			FillList.SetItemText (ZusNames[i].GetBuffer (), i, 1);
			CString value = ZusValues[i];
			value.TrimRight ();
			if (value != "")
			{
				FillList.SetItemText (value.GetBuffer (), i, 2);
			}
			else
			{
				FillList.SetItemText (_T(""), i, 2);
			}
		}
	}
	if (m_PrpUserList.GetItemCount () == 0)
	{
		FillList.InsertItem (0, 0);
	}

	m_PrpUserList.StartEnter (1, 0);
	return TRUE;
}

void CProductPage4::FillNames ()
{
	for (int i = 0; i < MAXENTRIES; i ++)
	{
		ZusNames[i] = ZusRecordNames[i];
		ZusNames[i].Trim ();
	}
}

void CProductPage4::ToRecordNames ()
{
	for (int i = 0; i < MAXENTRIES; i ++)
	{
		if (ZusNames[i] != "")
		{
			strncpy (ZusRecordNames[i], ZusNames[i].GetBuffer (), 48);
		}
	}
}

void CProductPage4::FromRecordNames ()
{
	for (int i = 0; i < MAXENTRIES; i ++)
	{
		CString name = ZusRecordNames[i];
		name.Trim ();
//		if (name != "")
		{
			ZusNames[i] = name;
		}
	}
}

BOOL CProductPage4::Write ()
{
	extern short sql_mode;
	short sql_s;
/*
	if (delcursor == -1)
	{
		return FALSE;
	}
*/
	if (!ProductPage4Read)
	{
		return FALSE;
	}

	sql_s = sql_mode;
	sql_mode = 1;
	Prp_user.sqlexecute (delcursor);
	m_PrpUserList.StopEnter ();
	int count = m_PrpUserList.GetItemCount ();

	memcpy (&Prp_user.prp_user, &prp_user_null, sizeof (PRP_USER));
	Prp_user.prp_user.a  = A_bas->a_bas.a;
	for (int i = 0, j = 0; i < count; i ++)
	{
         CString Name;
		 Name = m_PrpUserList.GetItemText (i, 1);
		 Name.TrimRight ();
		 if (Name.GetLength () == 0)
		 {
			 strcpy (ZusRecordNames[j], ""); 
			 continue;
		 }
		 strncpy (ZusRecordNames[j], Name.GetBuffer (), 48); 
         CString Wert;
		 Wert = m_PrpUserList.GetItemText (i, 2);
		 Wert.TrimRight ();
		 strncpy (ZusValues[j], Wert.GetBuffer (), 511); 
		 j ++;
	}
	Prp_user.dbupdate ();

	Prp_user.prp_user.a  = -1.0;
	FromRecordNames ();
	Prp_user.dbupdate ();
/*
	for (int i = 0; i < count; i ++)
	{
         CString Name;
		 Name = m_PrpUserList.GetItemText (i, 1);
		 Name.TrimRight ();
		 if (Name.GetLength () == 0)
		 {
			 continue;
		 }
         CString Wert;
		 Wert = m_PrpUserList.GetItemText (i, 2);
		 Wert.TrimRight ();
		 if (Wert.GetLength () == 0)
		 {
			 continue;
		 }
		 strcpy (Prp_user.prp_user.name, Name.GetBuffer ());
		 strcpy (Prp_user.prp_user.wert, Wert.GetBuffer ());
		 Prp_user.dbupdate ();
	}
*/

	return TRUE;
}

BOOL CProductPage4::OnSetActive ()
{
	if (Basis != NULL)
	{
		Basis->Form.Get ();
	}
	DlgBkColor = Basis->DlgBkColor;
    DeleteObject (DlgBrush);
    DlgBrush = NULL;
	Form.Show ();
	if (m_PrpUserList.ListEdit.m_hWnd != NULL)
	{
		PostMessage (WM_SETFOCUS, (WPARAM) m_PrpUserList.ListEdit.m_hWnd, 0l);
	}
	return TRUE;
}

BOOL CProductPage4::OnKillActive ()
{
	Write ();
	return TRUE;
}

BOOL CProductPage4::Delete ()
{
    FillList.SetItemText ( _T(""), m_PrpUserList.EditRow, 2);
	if (m_PrpUserList.EditCol == 2 && 
		m_PrpUserList.ListEdit.m_hWnd != NULL)
	{
		m_PrpUserList.ListEdit.SetWindowText (_T(""));
	}
    return TRUE;
}

void CProductPage4::UpdatePage ()
{
	Basis->Form.Get ();
	Read ();
}

void CProductPage4::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CProductPage4::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}
