#pragma once
#include "Prg_prot.h"
#include "Ipr.h"

class CPGrProt
{
public:
	CString PersName;
    CString Programm;

	IPR_CLASS *Ipr;
	PRG_PROT_CLASS Prg_prot;

	CPGrProt(void);
	CPGrProt(CString& PersName, CString& Programm, IPR_CLASS *Ipr);
	~CPGrProt(void);
	void Construct (CString& PersName, CString& Programm, IPR_CLASS *Ipr);
	void Write (int loesch=0);
};
