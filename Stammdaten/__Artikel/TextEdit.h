#pragma once
#include "afxwin.h"

class CTextEdit :
	public CEdit
{
public:
	CTextEdit(void);
	~CTextEdit(void);
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSetFocus (CWnd *);
};
