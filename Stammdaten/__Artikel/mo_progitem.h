#ifndef _PROG_ITEM
#define _PROG_ITEM


class PROG_ITEM 
         {
          private :
                FILE *progfp;
          public :
              char progname [80];
			  PROG_ITEM () 
			  {
				  progfp = NULL;
				  strcpy (progname, "");
			  }

              PROG_ITEM (char *progname) : progfp (NULL)
              {
                       if (progname)
                       {
                               strcpy (this->progname, progname);
                       }
              }
			  ~PROG_ITEM ();
              void SetProgName (char *progname)
              {
                       if (progname)
                       {
                               strcpy (this->progname, progname);
                       }
              }
              BOOL OpenItem (void);
              void CloseItem (void);
              BOOL GetItemValue (char *, char *);
			  static void clipped (char *);
			  static void cr_weg (char *);
			  static int strupcmp (char *, char *, int);
         };
#endif         
                          
                               

