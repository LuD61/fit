#include "StdAfx.h"
#include ".\smt.h"

CSmt::CSmt(void)
{
	A_bas = NULL;
	MdnCursor = -1;
	FilCursor = -1;
	HwgCursor = -1;
	HwgCursor2 = -1;
	WgCursor = -1;
	WgCursor2 = -1;
	AgCursor = -1;
	testmode = 0;
	CString env;
	if (env.GetEnvironmentVariable (_T("testmode")))
	{    
		testmode = atoi (env.GetBuffer());
    }
}

CSmt::~CSmt(void)
{
	if (MdnCursor != -1)
	{
		Smt_zuord.sqlclose (MdnCursor);
		MdnCursor = -1;
	}
	if (FilCursor != -1)
	{
		Smt_zuord.sqlclose (FilCursor);
		FilCursor = -1;
	}
	if (HwgCursor != -1)
	{
		Smt_zuord.sqlclose (HwgCursor);
		HwgCursor = -1;
	}
	if (HwgCursor2 != -1)
	{
		Smt_zuord.sqlclose (HwgCursor2);
		HwgCursor2 = -1;
	}
	if (WgCursor != -1)
	{
		Smt_zuord.sqlclose (WgCursor);
		WgCursor = -1;
	}
	if (WgCursor2 != -1)
	{
		Smt_zuord.sqlclose (WgCursor2);
		WgCursor2 = -1;
	}
	if (AgCursor != -1)
	{
		Smt_zuord.sqlclose (AgCursor);
		AgCursor = -1;
	}
}

void CSmt::Prepare ()
{
	if (MdnCursor != -1) return;
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.mdn, SQLSHORT, 0);
	MdnCursor = Smt_zuord.sqlcursor (_T("select mdn from smt_zuord where mdn = ?"));

	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.mdn, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.fil, SQLSHORT, 0);
	FilCursor = Smt_zuord.sqlcursor (_T("select fil from smt_zuord ")
									_T("where mdn = ? and fil = ?"));
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.mdn, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.fil, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.hwg, SQLSHORT, 0);
	HwgCursor = Smt_zuord.sqlcursor (_T("select hwg from smt_zuord ")
									_T("where mdn = ? and fil = ? ")
									_T("and hwg = ?"));
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.mdn, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.fil, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.hwg, SQLSHORT, 0);
	HwgCursor2 = Smt_zuord.sqlcursor (_T("select hwg from smt_zuord ")
									_T("where mdn = ? and fil = ? ")
									_T("and hwg = ? and wg > 0"));
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.mdn, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.fil, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.hwg, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.wg, SQLSHORT, 0);
	WgCursor = Smt_zuord.sqlcursor (_T("select wg from smt_zuord ")
									_T("where mdn = ? and fil = ? ")
									_T("and hwg = ? and wg = ?"));
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.mdn, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.fil, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.hwg, SQLSHORT, 0);
	Smt_zuord.sqlin ((short *) &Smt_zuord.smt_zuord.wg, SQLSHORT, 0);
	WgCursor2 = Smt_zuord.sqlcursor (_T("select hwg from smt_zuord ")
									_T("where mdn = ? and fil = ? ")
									_T("and hwg = ? and wg = ? and ag > 0"));
}

BOOL CSmt::TestSmt (short mdn, short fil)
{
	if (A_bas == NULL) return FALSE;
	
	int dsqlstatus = 0;

	Prepare ();
	Smt_zuord.smt_zuord.mdn = mdn;
	Smt_zuord.smt_zuord.fil = fil;
	Smt_zuord.sqlopen (MdnCursor);
	dsqlstatus = Smt_zuord.sqlfetch (MdnCursor);
	if (dsqlstatus != 0) return TRUE;

// Existieren Eintr�ge f�r die Filiale
	Smt_zuord.sqlopen (FilCursor);
	dsqlstatus = Smt_zuord.sqlfetch (FilCursor);
	if (dsqlstatus == 100) 
	{
// Keine Eintr�ge mit Filiale 0 weiter pr�fen
		Smt_zuord.smt_zuord.fil = 0;
	}

// Existiert ein Eintrag f�r HWG

	Smt_zuord.smt_zuord.hwg = A_bas->a_bas.hwg;
	Smt_zuord.sqlopen (HwgCursor);
	dsqlstatus = Smt_zuord.sqlfetch (HwgCursor);
	if (dsqlstatus != 0) return FALSE;

// Existieren ein Eintr�ge f�r HWG und WG != 0
	Smt_zuord.sqlopen (HwgCursor2);
	dsqlstatus = Smt_zuord.sqlfetch (HwgCursor2);
	if (dsqlstatus == 100) return TRUE;

// Existiert ein Eintrag f�r WG
	Smt_zuord.smt_zuord.wg = A_bas->a_bas.wg;
	Smt_zuord.sqlopen (WgCursor);
	dsqlstatus = Smt_zuord.sqlfetch (WgCursor);
	if (dsqlstatus != 0) return FALSE;

// Existieren Eintr�ge f�r WG und AG != 0
	Smt_zuord.sqlopen (WgCursor2);
	dsqlstatus = Smt_zuord.sqlfetch (WgCursor2);
	if (dsqlstatus != 0) return TRUE;

// Existiert ein Eintr�ge f�r AG

	dsqlstatus = Smt_zuord.dbreadfirst ();
	if (dsqlstatus != 0) return FALSE;
	return TRUE;
}


BOOL CSmt::TestPrSmt (short mdn, short fil)
{


	if (testmode) AfxMessageBox ("Aufruf: TestPrSmt");
	if (A_bas == NULL) return FALSE;
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&A_pr.a_pr, &a_pr_null, sizeof (A_PR));
	Mdn.mdn.mdn = mdn;
	Fil.fil.mdn = mdn;
	Fil.fil.fil = fil;
	Mdn.dbreadfirst ();
	if (fil != 0)
	{
		if (testmode) AfxMessageBox ("Aufruf: TestPrSmt fil != 0 LeseFil");
		Fil.dbreadfirst ();
	}
    A_pr.a_pr.a = A_bas->a_bas.a;
    A_pr.a_pr.mdn_gr = Mdn.mdn.mdn_gr;
    A_pr.a_pr.mdn    = Mdn.mdn.mdn;
    A_pr.a_pr.fil_gr = Fil.fil.fil_gr;
    A_pr.a_pr.fil    = Fil.fil.fil;
	while (A_pr.dbreadfirst () == 100)
	{
		if (A_pr.a_pr.fil > 0)
		{
			if (testmode) AfxMessageBox ("TestPrSmt: a_pr setze fil = 0");
			A_pr.a_pr.fil = 0;
		}
		else if (A_pr.a_pr.fil_gr > 0)
		{
			if (testmode) AfxMessageBox ("TestPrSmt: a_pr setze fil_gr = 0");
			A_pr.a_pr.fil_gr = 0;
		}
		else if (A_pr.a_pr.mdn > 0)
		{
			if (testmode) AfxMessageBox ("TestPrSmt: a_pr setze mdn = 0");
			A_pr.a_pr.mdn = 0;
		}
		else if (A_pr.a_pr.mdn_gr > 0)
		{
			if (testmode) AfxMessageBox ("TestPrSmt: a_pr setze mdn_gr = 0");
			A_pr.a_pr.mdn_gr = 0;
		}
		else
		{
			if (testmode) AfxMessageBox ("TestPrSmt: a_pr nicht gef. break");
			break;
		}
	}
	if (A_pr.a_pr.pr_ek_euro == -1.0 &&
		A_pr.a_pr.pr_vk_euro == 0.0)
	{
		if (testmode) AfxMessageBox ("TestPrSmt: FALSE");
		return FALSE;
	}
	if (testmode) AfxMessageBox ("TestPrSmt: TRUE");
	return TRUE;
}

