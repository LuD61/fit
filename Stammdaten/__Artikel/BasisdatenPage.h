// DialogTestDlg.h : Headerdatei
//

#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGridColor.h"
#include "FillList.h"
#include "a_bas_prot.h"
#include "a_bas.h"
#include "a_bas_erw.h"
#include "a_prot.h"
#include "sys_ben.h"
#include "a_hndw.h"
#include "a_eig.h"
#include "a_pfa.h"
#include "a_leer.h"
#include "a_krz.h"
#include "work.h"
#include "ag.h"
#include "sys_par.h"
#include "mdn.h"
#include "adr.h"
#include "Ipr.h"
#include "PGrProt.h"
#include "Iprgrstufk.h"
#include "I_kun_prk.h"
#include "Kun.h"
#include "ChoiceA.h"
#include "ChoiceEan.h"
#include "ChoiceAkrz.h"
#include "ChoiceMdn.h"
#include "ChoiceAG.h"
#include "ChoiceLiefBest.h"
#include "ChoiceMat.h"
#include "FormTab.h"
#include "mo_progcfg.h"
#include "Controls.h"
#include "Preise.h"
#include "ptabn.h"
#include "DbUniCode.h"
#include "afxwin.h"
#include "ImageCtrl.h"
#include "ImageListener.h"
#include "StaticButton.h"
#include "mo_auto.h"
#include "A_ean.h"
#include "A_emb.h"
#include "A_varb.h"
#include "A_zut.h"
#include "A_huel.h"
#include "A_mat.h"
#include "A_grund.h"
#include "ADel.h"
#include "Etikett.h"
#include "Sais.h"
#include "karton.h"

#define IDC_ACHOICE 3000
#define IDC_MDNCHOICE 3001
#define IDC_AGCHOICE 3005
#define IDC_GRUNDCHOICE 3003
#define IDC_ERSATZCHOICE 3004

#define IDC_ARTIMAGE 3005
// #define IDC_MOREKTO 3006

#define IDC_APFACHOICE 3007
#define IDC_ALEERCHOICE 3008
#define IDC_ALEIHCHOICE 3009
//#define IDC_LAGER 3010
#define IDC_ZUORDBOTTOM 3010
#define IDC_INHLIEFCHOICE 3011

// CPrArtPreise Dialogfeld
class CBasisdatenPage : public CDbPropertyPage,
	                    public CImageListener
{
// Konstruktion
public:
	enum
	{
		ZutFromAtexte = 1,
		ZutFromPrAusz = 2,
		ZutFromA_bas_erw = 3,
		ZutFromRezepture = 4,
	};
	int ZutSource;

	CBasisdatenPage(CWnd* pParent = NULL);	// Standardkonstruktor
	CBasisdatenPage(UINT nIDTemplate);	// Standardkonstruktor

	~CBasisdatenPage ();

// Dialogfelddaten
	enum { IDD = IDD_BASISDATEN_PAGE };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung
    virtual BOOL PreTranslateMessage(MSG* pMsg);


// Implementierung
protected:
	HICON m_hIcon;
	BOOL DelOK;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public :
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

    enum
	{
		Hndw = 1,
		Eig = 2,
		Pfa = 4,
		Varb = 5,
		Zut = 6,
		Huel = 9,
		Leer = 11,
		Leih = 12,
	};

	enum
	{
		Standard,
		Material,
	};

	enum
	{
		Message = 0,
		Activate = 1,
	};
	enum
	{
		Keine = 0,
		Warnung = 1,
		Verbieten = 2,
	}; //FS-18

	BOOL ProductPage1Read;
	BOOL ProductPage2Read;
	BOOL ProductPage3Read;
	BOOL ProductPage4Read;
	BOOL ProductPage5Read;
	BOOL ProductPage6Read;
	BOOL NoRecChange;
	int TypeMode;
	int AgRestriktion; //FS-18
	BOOL a_ersatz_nicht_pruefen;
	int NeuerArtikelAnzTage;

	long currentAg;
	COLORREF m_clrFarbe;

	CDbPropertyPage *ProductPage1;
	CDbPropertyPage *ProductPage2;
	CDbPropertyPage *ProductPage3;
	CDbPropertyPage *ProductPage4;
	CDbPropertyPage *ProductPage5;
	CDbPropertyPage *ProductPage6;
	BOOL Write160;
	static HANDLE Write160Lib;
    int (*dbild160)(LPSTR); 
    BOOL (*dOpenDbase)(LPSTR); 
	CADel *ADel;
	static HANDLE InfoLib;
    BOOL (*_CallInfoEx) (HWND hMainWindow, RECT *rect,char *item, char *ItemValue,DWORD WinMode); 

	int PageType;
	CString Where;
	CString AgWhere;
	CString Types;
	CString PeriTypes;
	BOOL PeriData;
	short a_typ;
	int a_typ_pos;
	std::vector<CString *> ATyp1Combo;
	std::vector<CString *> ATyp2Combo;
/*
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	COLORREF ListBkColor;
	CString ArchiveName;
*/
	PROG_CFG Cfg;
	int StartSize;
	BOOL NeuerArtikel;
	BOOL ArtikelGesperrt;
	BOOL GrundArtikelGesperrt; //FS-337
	char ctext[60];
	struct A_BAS a_bas_def;
	struct A_HNDW a_hndw_def;

	CControls HeadControls;
	CControls PosControls;
	CControls ButtonControls;
	BOOL HideButtons;
	int RightListSpace;
	CVector DbRows;
	CVector ListRows;
	int IprCursor;
	int IprGrStufkCursor;
	int IKunPrkCursor;
	int SaisCursor;
	int AGrundCursor;
	int GrundCursor;
	CWnd *Frame;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid AGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid AGGrid;
	CCtrlGrid GrundGrid;
	CCtrlGrid ErsatzGrid;
	CCtrlGrid BestandGrid;
	CCtrlGrid BsdGrid;
	CColorButton m_MdnChoice;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CTextEdit m_MdnName;
	CColorButton m_AChoice;
	CColorButton m_AGChoice;
	CColorButton m_GrundChoice;
	CColorButton m_ErsatzChoice;
	CColorButton m_InhLiefChoice; //LAC-115
	CButton m_A_bz3_hide;
	CButton m_Abt_hide;
	CButton m_Stueckl_hide;
	CButton m_Teilsmt_hide;
	CButton m_Smt_hide;
	CButton m_Grund_hide;
	CButton m_Check_bestand;
	CButton m_ShopKZ;
	CButton m_Deaktiv;
	CButton m_FilDeaktiv;
	CButton m_Ersatz_hide;
	CStatic m_LA;
	CNumEdit m_A;
//	CEdit m_LA_bz1;
	CEdit m_LMatchcode;
	CTextEdit m_A_bz1;
	CTextEdit m_A_bz2;
	CTextEdit m_A_bz3;
	CStatic m_Gruppen;
	CStatic m_Static_ag;
	CStatic m_Static_wg;
	CStatic m_Static_hwg;
	CStatic m_Static_abt;
	CNumEdit m_Edit_ag;
	CNumEdit m_Edit_wg;
	CNumEdit m_Edit_hwg;
	CNumEdit m_Edit_abt;
	CEdit m_Ag_bz1;
	CEdit m_Wg_bez;
	CEdit m_Hwg_bez;
	CStatic m_Static_stueckl;
	CStatic m_Static_teilsmt;
	CStatic m_LSmt;
	CStatic m_Static_grund;
	CStatic m_Static_farbe;
	CStatic m_Static_ersatz;
	CNumEdit m_Edit_ersatz;
	CNumEdit m_Edit_grund;
	CComboBoxEx m_Combo_stueckl;
	CComboBoxEx m_Smt;
	CComboBoxEx m_Combo_teilsmt;
	CStatic m_Gruppen_bestand;
	CButton m_Radio_bestand_j;
	CButton m_Radio_bestand_n;
	CButton m_Radio_bestand_a;

	CCtrlGrid ZuordGrid;
	CCtrlGrid BestAutoGrid;
	CStatic m_Zuordnungen;
	CStatic m_ZuordBottom;
	CStatic m_LWe_kto;
	CNumEdit m_We_kto;
	CStatic m_LIntra_stat;
	CNumEdit m_Intra_stat;
	CStatic m_LErl_kto;
	CNumEdit m_Erl_kto;
	CStaticButton m_Atyp2;
	CStaticButton m_MoreKto;
	CStaticButton m_Lager;
	CStaticButton m_Best;
	CStaticButton m_Kalk;
	CStaticButton m_Farbe;

	CCtrlGrid LinkGrid;
	CStatic m_Link;
	CStatic m_LAPfa;
	CNumEdit m_APfa;
	CStatic m_LALeer;
	CNumEdit m_ALeer;
	CStatic m_LALeih;
	CNumEdit m_ALeih;

	CButton m_APfaChoice;
	CButton m_ALeerChoice;
	CButton m_ALeihChoice;
	CCtrlGrid APfaGrid;
	CCtrlGrid ALeerGrid;
	CCtrlGrid ALeihGrid;

	CStatic m_GroupEinh;
	CStatic m_LHndGew;
	CComboBoxEx m_HndGew;
	CStatic m_LMeEinh;
	CComboBoxEx m_MeEinh;
	CStatic m_LLiefEinh;
	CComboBoxEx m_LiefEinh;
	CStatic m_LInhLief;
	CNumEdit m_InhLief;

	CStatic m_LEinhAbverk;
	CComboBoxEx m_EinhAbverk;
	CStatic m_LInhAbverk;
	CNumEdit m_InhAbverk;
	CStatic m_LHndGewAbverk;
	CComboBoxEx m_HndGewAbverk;

	CStatic m_LKunBestEinh;
	CComboBoxEx m_KunBestEinh;
	CStatic m_LKunInh;
	CNumEdit m_KunInh;

	CButton m_HndGewHide;
	CButton m_MeEinhHide;
	CButton m_LiefEinhHide;
	CButton m_EinhAbverkHide;
	CButton m_KunBestEinhHide;

	CStatic m_LBereich;
	CComboBox m_Bereich;
	CStatic m_LSais;
	CComboBox m_Sais;

	CCtrlGrid EinhGrid;
	CCtrlGrid InhLiefGrid; //LAC-115


//	CFillList FillList;
	CButton m_Gesperrt;
	CButton m_Cancel;
	CButton m_Save;
	CButton m_Delete;
	CButton m_Insert;
	int RighListSpace;
	CCtrlGrid ButtonGrid;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	A_BAS_CLASS A_bas;
	A_BAS_ERW_CLASS A_bas_erw;
	A_BAS_PROT_CLASS A_bas_prot;
	A_PROT_CLASS A_prot;
	SYS_BEN_CLASS Sys_ben;
	A_HNDW_CLASS A_hndw;
	A_EIG_CLASS A_eig;
	A_PFA_CLASS A_pfa;
	A_LEER_CLASS A_leer;
	A_KRZ_CLASS A_krz;
	PTABN_CLASS Ptabn;
	AUTO_CLASS AutoNr;
	A_EAN_CLASS A_ean;
	A_EMB_CLASS A_emb;
	A_VARB_CLASS A_varb;
	A_ZUT_CLASS A_zut;
	A_HUEL_CLASS A_huel;
	A_MAT_CLASS A_mat;
	A_GRUND_CLASS A_grund;
	KARTON_CLASS Karton;
	WORK Work;
	AG_CLASS Ag;
	SYS_PAR_CLASS Sys_par;
	IPR_CLASS Ipr;
	SAIS_CLASS Sais;
	CPGrProt PgrProt;
	IPRGRSTUFK_CLASS IprGrStufk;
	I_KUN_PRK_CLASS I_kun_prk;
	KUN_CLASS Kun;
	ADR_CLASS KunAdr;
	CEtikett Etikett;
	CFormTab Form;
	CFormTab Keys;
	CChoiceA *Choice;
	CChoiceEan *ChoiceEan;
	CChoiceAkrz *ChoiceAkrz;
	CChoiceLiefBest *ChoiceLiefBest;
	CChoiceMat *ChoiceMat;
	CChoiceA *ChoiceM;
	BOOL ModalChoice;
	BOOL ModalChoiceEan;
	BOOL ModalChoiceAkrz;
	BOOL ModalChoiceLiefBest;
	BOOL ModalChoiceMat;
	BOOL CloseChoice;
	BOOL AChoiceStat;
	BOOL AChoiceStatM;
	BOOL AGChoiceStat;
	CString SearchA;
	CString SearchEan;
	CString SearchAkrz;
	CString SearchLiefBest;
	CString SearchAG;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceAG *ChoiceAG;
	BOOL ModalChoiceAG;
	CFont Font;
	CFont lFont;
	int FontHeight;
	CString PersName;
	CString Separator;
    CImageList image; 
	BOOL ShowImage;
	int CellHeight;
	long a_krz_gen;
	TCHAR m_VerkArt[2];
   	TCHAR da_bz3_hide[2], dabt_hide[2];
   	TCHAR dstueckl_hide[2], dsmt_hide[2], dteilsmt_hide[2], dgrund_hide[2], dersatz_hide[2];
	TCHAR dbestand_j[2],dbestand_n[2],dbestand_a[2];
	TCHAR dhnd_gew_hide[2], dme_einh_hide[2], dlief_einh_hide[2], deinh_abverk_hide[2],
		  dkun_einh_hide[2];

	virtual void OnSize (UINT, int, int);
	void FillStuecklCombo ();
	void FillSmtCombo ();
	void InsA_bas_Prot (short aktivitaet);
	void FillSais ();
	void FillInhLief ();
	void FillTeilsmtCombo ();
	void FillAtypCombo ();
	void FillAtyp2Combo ();
	void FillMwstCombo ();
	void FillHndGewCombo ();
	void FillMeEinhCombo ();
	void FillLiefEinhCombo ();
	void FillEinhAbverkCombo ();
	void FillHndGewAbverkCombo ();
	void FillKunEinhCombo ();
	void FillCombo (LPTSTR Item, CWnd *control);
	void OnAchoice ();
	void OnEanchoice ();
	void OnAkrzchoice ();
	void OnLiefBestchoice ();
	void OnMatchoice ();
	void OnAchoiceM ();
	void OnAchoiceM_grund ();
	void OnAchoiceM_ersatz ();
	void OnAGchoice ();
	void OnInhLiefChoice ();
	void OnASelected ();
    void OnEanSelected ();
    void OnAkrzSelected ();
    void OnLiefBestSelected ();
    void OnMatSelected ();
    void OnAShow ();
    void OnACanceled ();
    void OnEanCanceled ();
    void OnAkrzCanceled ();
    void OnLiefBestCanceled ();
    void OnMatCanceled ();
    void OnMdnchoice(); 
	virtual BOOL Read ();
	virtual BOOL ReadAG ();
	virtual BOOL TestGroups ();
	virtual BOOL Read_a_grund ();
	virtual BOOL Read_a_ersatz ();
	virtual BOOL Write ();
	virtual BOOL WriteIpr ();
	virtual BOOL Print ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	virtual BOOL StepBack ();
	virtual void OnCancel ();
	virtual void OnSave ();
	BOOL ReadMdn ();
	BOOL ReadList ();
    void FillPrGrStufCombo ();
    void FillKunPrCombo ();
	virtual BOOL DeleteAll ();
	virtual void OnDelete ();
	virtual BOOL Delete ();
	virtual void OnInsert ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult);
    void DisableTabControl (BOOL disable);
    void WriteRow (int row);
    void DestroyRows(CVector &Rows);
    void DestroyRowsEx(CVector &Rows);
    void DeleteDbRows ();
    BOOL IsChanged (CPreise *pIpr);
    BOOL InList (IPR_CLASS& Ipr);
	void ReadCfg ();
    void ListCopy ();
    void sw_hide (BOOL hide);
    void ReadDefaultHide ();
    void WriteDefaultHide ();
    void SetDefaultHide ();
	void MatHide ();
	void HoleArtikel(double a);
	void LockArtikel(double a); //WAL-130
	long HoleGrundArtikel(double a); //FS-337
	void HoleAG(long ag);
	afx_msg void OnEnChangeA();
	afx_msg void OnEnKillfocusEditAg();
	afx_msg void OnEnKillfocusMdn();
	afx_msg void OnEnKillfocusA();
	afx_msg void OnEnKillfocusEditGrund();
	afx_msg void OnEnKillfocusEditErsatz();
    void EnableFields (BOOL b);
	CStatic m_LAtyp;
	CComboBox m_Atyp;
	CStatic m_LMwst;
	CComboBox m_Mwst;
	CButton m_PersRab;
	CButton m_BestAuto;
	CStatic m_LBestAuto2;
	CComboBox m_BestAuto2;
	CStatic m_LCharge;
    CComboBox m_Charge;
	CButton m_ZerlEti;
	CStatic m_LSchwund;
	CNumEdit m_Schwund;
	CImageCtrl m_ImageArticle;
    afx_msg void OnRButtonDown( UINT flags, CPoint point);
    afx_msg void OnLButtonDblClk( UINT flags, CPoint  point);
	void DisplayImage ();
    void ActionPerformed (int action);
	afx_msg void OnLinkImage();
	afx_msg void OnUnlinkImage();
	afx_msg void OnOpenImage();
	afx_msg void OnOpenImageWith();
	afx_msg void OnLager();
	afx_msg void OnBest();
	afx_msg void OnKalk();
	afx_msg void OnFarbe();
	afx_msg void OnMoreKto();
	afx_msg void OnAtyp2();
	afx_msg void OnChoiceAPfa();
	afx_msg void OnChoiceALeer();
	afx_msg void OnChoiceALeih();
	void EnableLinks ();
	afx_msg void OnCbnSelchangeATyp();
	BOOL ChoiceHasElements ();
	BOOL ChoiceEanHasElements ();
	BOOL ChoiceAkrzHasElements ();
	BOOL ChoiceLiefBestHasElements ();
	void FirstRec ();
	void PriorRec ();
	void NextRec ();
	void LastRec ();
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	void OnDlgBackground();
	void OnChoicebackground();
	void OnChoiceaDefault();
	void OnChoiceeanDefault();
	void OnChoiceakrzDefault();
	void OnChoiceLiefBestDefault();
	void OnFlatLayout();
	void SetWhere ();
	void SetAgWhere ();
	void SetPeriTypes ();
	BOOL TestType ();
	BOOL TestAgType ();
	BOOL TestMat ();
	void OnInfo ();
    void AgDefaults ();
	BOOL SendImages ();
	void PrintRegEti ();
	void PrintThekeEti ();
	afx_msg void OnBsdClicked ();
	void ActivateTypePage ();
	void ActivateTypePageEx ();
	void OnRecChange();
	virtual BOOL OnKillActive ();
public:
	afx_msg void OnEnSetfocusA();
	afx_msg void OnStnClickedStaticGrundmclrfarbe2();
	afx_msg void OnBnClickedFildeaktiv();
	afx_msg void OnBnClickedDeaktiv();
	afx_msg void OnCbnSelchangeHndGew();
};
