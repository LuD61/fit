#pragma once
#include "EanListDlg.h"
#include "EmbListDlg.h"
#include "SplitPane.h"
#include "BasisdatenPage.h"
#include "UpdateEvent.h"
#include "PageUpdate.h"

// CEanEmbListDlg-Dialogfeld

class CEanEmbListDlg : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CEanEmbListDlg)

public:
	CEanEmbListDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CEanEmbListDlg();

// Dialogfelddaten
	enum { IDD = IDD_EAN_EMB_LIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnSize (UINT, int, int);

	DECLARE_MESSAGE_MAP()
public:
	CPageUpdate *PageUpdate;
	CBasisdatenPage *Basis;
	CEanListDlg m_EanListDlg;
	CEmbListDlg m_EmbListDlg;
	CSplitPane SplitPane;
	void SetBasis (CBasisdatenPage *Basis);
	virtual void Update ();
	virtual void Read ();
	void AddUpdateEvent (CUpdateEvent *UpdateEvent);
};
