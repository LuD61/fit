#include "StdAfx.h"
#include "ATextList.h"

CATextList::CATextList(void)
{
	sys = 0;
	txt_nr = 0;
	txt = _T("");
}

CATextList::CATextList(long sys, long txt_nr, LPTSTR txt)
{
	this->sys     = sys;
	this->txt_nr  = txt_nr;
	this->txt     = txt;
}

CATextList::~CATextList(void)
{
}
