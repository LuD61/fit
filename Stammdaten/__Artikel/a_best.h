#ifndef _A_BEST_DEF
#define _A_BEST_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_BEST {
   double         a;
   short          best_anz;
   TCHAR          best_auto[2];
   double         bsd_min;
   double         bsd_min_emp;
   double         bsd_max_emp;
   short          delstatus;
   TCHAR          erf_gew_kz[2];
   short          fil;
   TCHAR          letzt_lief_nr[17];
   TCHAR          lief[17];
   TCHAR          lief_best[17];
   long           lief_s;
   short          mdn;
   short          me_einh_ek;
   TCHAR          umk_kz[2];
};
extern struct A_BEST a_best, a_best_null;

#line 8 "a_best.rh"

class A_BEST_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_BEST a_best;
               A_BEST_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (A_BEST&);  
};
#endif
