// PrArtPreise.cpp : Implementierungsdatei
//

#include "stdafx.h"
// #include "IPrDialog.h"
//#include "IprMan.h"
#include "PrGrPage.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Decimal.h"
#include "Process.h"
#include "AddEk.h"
#include "Token.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

// CPrGrPage Dialogfeld

CPrGrPage::CPrGrPage(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage()
{
	Choice = NULL;
	ModalChoice = TRUE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	IprCursor = -1;
	IprDelCursor = -1;
	IprGrStufkCursor = -1;
	IKunPrkCursor = -1;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_PrGrStuf);
	PosControls.Add (&m_ZusBz);
	PosControls.Add (&m_PrGrList);
	PosControls.Add (&m_AddEk);

	ButtonControls.Add (&m_Cancel);
	ButtonControls.Add (&m_Save);
	ButtonControls.Add (&m_Delete);
	ButtonControls.Add (&m_Insert);

    HideButtons = FALSE;

	Frame = NULL;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Search = _T("");
	Separator = _T("|");
	CellHeight = 0;
	Cfg.SetProgName( _T("IPrDialog"));
}

CPrGrPage::CPrGrPage(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	Frame = NULL;
	IprCursor = -1;
	IprDelCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
}

CPrGrPage::~CPrGrPage()
{
	Font.DeleteObject ();
	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	Ipr.dbclose ();
	Iprgrstufk.dbclose ();
	Kun.dbclose ();
	KunAdr.dbclose ();
	if (IprCursor == -1)
	{
		Ipr.sqlclose (IprCursor);
	}
	if (IprDelCursor == -1)
	{
		Ipr.sqlclose (IprDelCursor);
	}
	if (IprGrStufkCursor == -1)
	{
		A_bas.sqlclose (IprGrStufkCursor);
	}
	if (IKunPrkCursor == -1)
	{
		A_bas.sqlclose (IKunPrkCursor);
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	DestroyRows (m_PrGrList.ListRows);

}

void CPrGrPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LPR_GR_STUF, m_LPrGrStuf);
	DDX_Control(pDX, IDC_PR_GR_STUF, m_PrGrStuf);
	DDX_Control(pDX, IDC_LZUS_BZ, m_LZusBz);
	DDX_Control(pDX, IDC_ZUS_BZ, m_ZusBz);
	DDX_Control(pDX, IDC_PRGR_LIST, m_PrGrList);
	DDX_Control(pDX, IDC_ADDEK, m_AddEk);

	DDX_Control(pDX, IDC_CANCEL, m_Cancel);
	DDX_Control(pDX, IDC_SAVE, m_Save);
	DDX_Control(pDX, IDC_DELETE, m_Delete);
	DDX_Control(pDX, IDC_INSERT, m_Insert);

}

BEGIN_MESSAGE_MAP(CPrGrPage, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_SIZE ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_PRGRCHOICE ,  OnPrGrchoice)
	ON_BN_CLICKED(IDC_CANCEL ,   OnCancel)
	ON_BN_CLICKED(IDC_SAVE ,     OnSave)
	ON_BN_CLICKED(IDC_DELETE ,   OnDelete)
	ON_BN_CLICKED(IDC_INSERT ,   OnInsert)
	ON_BN_CLICKED(IDC_ADDEK ,   OnAddEk)
	ON_COMMAND (SELECTED, OnPrGrSelected)
	ON_COMMAND (CANCELED, OnPrGrCanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
END_MESSAGE_MAP()


// CPrGrPage Meldungshandler

BOOL CPrGrPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
//	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
//	ASSERT(IDM_ABOUTBOX < 0xF000);

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	A_bas.opendbase (_T("bws"));

	CUtil::GetPersName (PersName);
	PgrProt.Construct (PersName, CString ("11121"), &Ipr);

	GrundPreis.Create (IDD_GRUND_PREIS, this);

	ReadCfg ();

	if (HideButtons)
	{
		ButtonControls.SetVisible (FALSE);
		RightListSpace = 15;
	}
	else
	{
		ButtonControls.SetVisible (TRUE);
		RightListSpace = 125;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	m_Cancel.SetWindowText (_T("Beenden"));
    HBITMAP HbF5 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F5T", "F5MASKT");
	m_Cancel.SetBitmap (HbF5);

    HBITMAP HbF12 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F12T", "F12MASKT");
	m_Save.SetBitmap (HbF12);

    HBITMAP HbDel = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "DELT", "DELMASKT");
	m_Delete.SetBitmap (HbDel);

    HBITMAP HbInsert = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "INSERTT", "INSERTMASKT");
	m_Insert.SetBitmap (HbInsert);

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_PrGrStuf,EDIT,   (long *) &Iprgrstufk.iprgrstufk.pr_gr_stuf, VLONG));
    Form.Add (new CUniFormField (&m_ZusBz,EDIT,   (char *) Iprgrstufk.iprgrstufk.zus_bz, VCHAR));
    
	Ipr.sqlin ((short *)  &Ipr.ipr.mdn, SQLSHORT, 0);
	Ipr.sqlin ((long *)  &Ipr.ipr.pr_gr_stuf, SQLLONG, 0);
	Ipr.sqlout ((double *) &Ipr.ipr.a,  SQLDOUBLE, 0);
	IprCursor = Ipr.sqlcursor (_T("select a from ipr ")
		                       _T("where mdn = ? ")
		                       _T("and pr_gr_stuf = ? ")
		                       _T("and kun_pr = 0 ")
		                       _T("and kun = 0 ")
		                       _T("order by a"));
	Ipr.sqlin ((short *)  &Ipr.ipr.mdn, SQLSHORT, 0);
	Ipr.sqlin ((long *)  &Ipr.ipr.pr_gr_stuf, SQLLONG, 0);
	IprDelCursor = Ipr.sqlcursor (_T("delete from ipr ")
		                       _T("where mdn = ? ")
		                       _T("and pr_gr_stuf = ? ")
		                       _T("and kun_pr = 0 ")
		                       _T("and kun = 0 "));

	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_PrGrList.SetImageList (&image, LVSIL_SMALL);   
	}

	FillList = m_PrGrList;
	FillList.SetStyle (LVS_REPORT);
	if (m_PrGrList.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Artikel"),     m_PrGrList.PosA, 130, LVCFMT_RIGHT);
	FillList.SetCol (_T("Bezeichnung"), m_PrGrList.PosABz1, 250, LVCFMT_LEFT);
	FillList.SetCol (_T("VK-Preis"),    m_PrGrList.PosVkPr, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Laden-Preis"), m_PrGrList.PosLdPr, 100, LVCFMT_RIGHT);

	if (m_PrGrList.Aufschlag == m_PrGrList.LIST ||
		m_PrGrList.Aufschlag == m_PrGrList.ALL)
	{
		FillList.SetCol (_T("Aufschlag"),  m_PrGrList.PosEkAbs, 100, LVCFMT_RIGHT);
		FillList.SetCol (_T("Aufschlag %"),m_PrGrList.PosEkProz, 100, LVCFMT_RIGHT);
	}

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	PrGrGrid.Create (this, 1, 2);
    PrGrGrid.SetBorder (0, 0);
    PrGrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_PrGrStuf = new CCtrlInfo (&m_PrGrStuf, 0, 0, 1, 1);
	PrGrGrid.Add (c_PrGrStuf);
	CtrlGrid.CreateChoiceButton (m_PrGrChoice, IDC_PRGRCHOICE, this);
	CCtrlInfo *c_PrGrChoice = new CCtrlInfo (&m_PrGrChoice, 1, 0, 1, 1);
	PrGrGrid.Add (c_PrGrChoice);

	ButtonGrid.Create (this, 5, 5);
    ButtonGrid.SetBorder (0, 0);
    ButtonGrid.SetCellHeight (20);
    ButtonGrid.SetGridSpace (0, 10);
	CCtrlInfo *c_Cancel = new CCtrlInfo (&m_Cancel, 0, 0, 1, 1);
	ButtonGrid.Add (c_Cancel);
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 1, 1, 1);
	ButtonGrid.Add (c_Save);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ButtonGrid.Add (c_Delete);
	CCtrlInfo *c_Insert = new CCtrlInfo (&m_Insert, 0, 3, 1, 1);
	ButtonGrid.Add (c_Insert);

	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_GrundPreis = new CCtrlInfo (&GrundPreis, DOCKRIGHT, 0, 6, 1); 
	CtrlGrid.Add (c_GrundPreis);

	CCtrlInfo *c_LPrGrStuf     = new CCtrlInfo (&m_LPrGrStuf, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LPrGrStuf);
	CCtrlInfo *c_PrGrGrid   = new CCtrlInfo (&PrGrGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_PrGrGrid);
	CCtrlInfo *c_LZusBz  = new CCtrlInfo (&m_LZusBz, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LZusBz);
	CCtrlInfo *c_ZusBz  = new CCtrlInfo (&m_ZusBz, 1, 2, 3, 1); 
	CtrlGrid.Add (c_ZusBz);
	CCtrlInfo *c_AddEk  = new CCtrlInfo (&m_AddEk, 0, 3, 1, 1); 
	CtrlGrid.Add (c_AddEk);

	CCtrlInfo *c_ButtonGrid = new CCtrlInfo (&ButtonGrid, DOCKRIGHT, 4, 1, 3); 
	CtrlGrid.Add (c_ButtonGrid);
	CCtrlInfo *c_PrList  = new CCtrlInfo (&m_PrGrList, 0, 4, DOCKRIGHT, DOCKBOTTOM); 
	c_PrList->rightspace = RightListSpace;
	CtrlGrid.Add (c_PrList);

	m_PrGrList.AddListChangeHandler (this);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	if (m_PrGrList.Aufschlag != m_PrGrList.BUTTON
		&& m_PrGrList.Aufschlag != m_PrGrList.ALL)
	{
		m_AddEk.ShowWindow (SW_HIDE);
	}

	CtrlGrid.Display ();
	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk, sizeof (IPRGRSTUFK));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
	if (PersName.Trim () != "")
	{
		_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
		if (Sys_ben.dbreadfirstpers_nam () == 0)
		{
			if (Sys_ben.sys_ben.mdn != 0)
			{
				Mdn.mdn.mdn = Sys_ben.sys_ben.mdn;
				m_Mdn.SetReadOnly ();
				m_Mdn.ModifyStyle (WS_TABSTOP, 0);
			}
		}
	}
	Form.Show ();
	ReadMdn ();
	EnableHeadControls (TRUE);
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}


void CPrGrPage::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CPrGrPage::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CPrGrPage::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CPrGrPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CPrGrPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_PrGrList.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_PrGrList &&
					GetFocus ()->GetParent () != &m_PrGrList )
				{

					break;
			    }
				m_PrGrList.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F11)
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					ShowGrundPreis ();
				}
				else
				{
					OnAddEk ();
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnPrGrchoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_PrGrStuf)
				{
					OnPrGrchoice ();
					return TRUE;
				}
				m_PrGrList.OnKey9 ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CPrGrPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_PrGrStuf)
	{
		if (!Read ())
		{
			m_PrGrStuf.SetFocus ();
			return FALSE;
		}
	}
/*
	else if (Control == &m_ZusBz)
	{
		Form.Get ();
		if (!ReadList ())
		{
			m_PrGrStuf.SetFocus ();
			m_PrGrStuf.SetSel (0, -1);
			return FALSE;
		}
	    EnableHeadControls (FALSE);

	}
*/

	if (Control != &m_PrGrList &&
		Control->GetParent ()!= &m_PrGrList )
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CPrGrPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_PrGrList &&
		Control->GetParent ()!= &m_PrGrList )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_PrGrList.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}


BOOL CPrGrPage::ReadMdn ()
{
	int mdn;

	mdn = Mdn.mdn.mdn;
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Choice != NULL && mdn != Mdn.mdn.mdn)
	{
		delete Choice;
		Choice = NULL;
	}
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		FillPrGrStufCombo ();
		FillKunPrCombo ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CPrGrPage::read ()
{
	if (m_PrGrStuf.IsWindowEnabled ())
	{
		return FALSE;
	}
	return Read ();

}

BOOL CPrGrPage::Read ()
{
	if (ModalChoice)
	{
		CString cPrGrStuf;
		m_PrGrStuf.GetWindowText (cPrGrStuf);
		if (!CStrFuncs::IsDecimal (cPrGrStuf))
		{
			Search = cPrGrStuf;
			OnPrGrchoice ();
			Search = "";
			if (!ChoiceStat)
			{
				m_PrGrStuf.SetFocus ();
				m_PrGrStuf.SetSel (0, -1);
				return FALSE;
			}
		}
	}

	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
	Form.Get ();
	strcpy (Iprgrstufk.iprgrstufk.zus_bz, ""); 
	Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
	Iprgrstufk.dbreadfirst ();
	ReadList ();
    EnableHeadControls (FALSE);
	Form.Show ();
	return TRUE;
}

BOOL CPrGrPage::ReadList ()
{
	m_PrGrList.DeleteAllItems ();
	m_PrGrList.vSelect.clear ();
	m_PrGrList.ListRows.Init ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	DestroyRows (m_PrGrList.ListRows);
	int i = 0;
	memcpy (&Ipr.ipr, &ipr_null, sizeof (IPR));
	Ipr.ipr.mdn = Mdn.mdn.mdn;
	Ipr.ipr.pr_gr_stuf   = Iprgrstufk.iprgrstufk.pr_gr_stuf;
	m_PrGrList.mdn = Mdn.mdn.mdn;
	m_PrGrList.pr_gr_stuf = Iprgrstufk.iprgrstufk.pr_gr_stuf; 
	Ipr.sqlopen (IprCursor);
	while (Ipr.sqlfetch (IprCursor) == 0)
	{
		Ipr.dbreadfirst ();
		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas.a_bas.a = Ipr.ipr.a;
		A_bas.dbreadfirst ();
		Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 301111

		FillList.InsertItem (i, 0);
		CString A;
		A.Format (_T("%.0lf"), Ipr.ipr.a);
		FillList.SetItemText (A.GetBuffer (), i, m_PrGrList.PosA);
		CString ABz1;
		ABz1 = A_bas.a_bas.a_bz1;
		ABz1 += _T("   ");
		ABz1 += A_bas.a_bas.a_bz2;
		FillList.SetItemText (ABz1.GetBuffer (), i, m_PrGrList.PosABz1);
		CString VkPr;
		m_PrGrList.DoubleToString (Ipr.ipr.vk_pr_eu, VkPr, 4);
		Ipr.ipr.vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
		FillList.SetItemText (VkPr.GetBuffer (), i, m_PrGrList.PosVkPr);
		CString LdPr;
		m_PrGrList.DoubleToString (Ipr.ipr.ld_pr_eu, LdPr, 2);
		FillList.SetItemText (LdPr.GetBuffer (), i, m_PrGrList.PosLdPr);

		CString EkAbs;
		m_PrGrList.DoubleToString (Ipr.ipr.add_ek_abs, EkAbs, 2);
		FillList.SetItemText (EkAbs.GetBuffer (), i, m_PrGrList.PosEkAbs);
		CString EkProz;
		m_PrGrList.DoubleToString (Ipr.ipr.add_ek_proz, EkProz, 2);
		FillList.SetItemText (EkProz.GetBuffer (), i, m_PrGrList.PosEkProz);

		CPreise *ipr = new CPreise (VkPr, LdPr, Ipr.ipr);
		DbRows.Add (ipr);
		ipr = new CPreise (VkPr, LdPr, Ipr.ipr);
		m_PrGrList.ListRows.Add (ipr);
		i ++;
	}
	return TRUE;
}

BOOL CPrGrPage::IsChanged (CPreise *pIpr)
{
	DbRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ipr.ipr, &pIpr->ipr, sizeof (IPR));
		if (Ipr == ipr->ipr) break;
	}
	if (ipr == NULL)
	{
		return TRUE;
	}
	if (pIpr->cEk != ipr->cEk) return TRUE;
	if (pIpr->cVk != ipr->cVk) return TRUE;
	return FALSE;
}

BOOL CPrGrPage::InList (IPR_CLASS& Ipr)
{
	ListRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Ipr == ipr->ipr) return TRUE;
	}
    return FALSE;
}

void CPrGrPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ipr.ipr, &ipr->ipr, sizeof (IPR));
		if (!InList (Ipr))
		{
			Ipr.dbdelete ();
			PgrProt.Write (1);
		}
	}
}

BOOL CPrGrPage::Write ()
{
	extern short sql_mode;
	short sql_s;
	if (m_PrGrStuf.IsWindowEnabled ())
	{
		return FALSE;
	}
	Form.Get ();
	sql_s = sql_mode;
	sql_mode = 1;
	Ipr.beginwork ();
	m_PrGrList.StopEnter ();
	int count = m_PrGrList.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 IPR *ipr = new IPR;
		 memcpy (ipr, &ipr_null, sizeof (IPR));
         CString Text;
		 Text = m_PrGrList.GetItemText (i, m_PrGrList.PosA);
		 ipr->a = CStrFuncs::StrToDouble (Text);
     	 CString VkPr =  m_PrGrList.GetItemText (i, m_PrGrList.PosVkPr);
		 ipr->vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
		 ipr->vk_pr_i = ipr->vk_pr_eu;
		 CString LdPr =  m_PrGrList.GetItemText (i, m_PrGrList.PosLdPr);
		 ipr->ld_pr_eu = CStrFuncs::StrToDouble (LdPr);
		 ipr->ld_pr = ipr->ld_pr_eu;
		 if (m_PrGrList.Aufschlag == m_PrGrList.LIST ||
			 m_PrGrList.Aufschlag == m_PrGrList.ALL)
		 {
			CString EkAbs =  m_PrGrList.GetItemText (i, m_PrGrList.PosEkAbs);
			ipr->add_ek_abs = CStrFuncs::StrToDouble (EkAbs);
			CString EkProz =  m_PrGrList.GetItemText (i, m_PrGrList.PosEkProz);
			ipr->add_ek_proz = CStrFuncs::StrToDouble (EkProz);
		 }
		 else
		 {
			 CPreise *lpr = (CPreise *) m_PrGrList.ListRows.Get (i);
			 ipr->add_ek_abs = lpr->ipr.add_ek_abs;
			 ipr->add_ek_proz = lpr->ipr.add_ek_proz;
		 }

		 if (!TestDecValues (ipr)) 
		 {
			 MessageBox (_T("Maximalwert f�r Preisfeld �berschritten"));
			 return FALSE;
		 }
		 ipr->mdn = Mdn.mdn.mdn; 
		 ipr->pr_gr_stuf = Iprgrstufk.iprgrstufk.pr_gr_stuf; 
		 ipr->kun_pr = 0l;
		 ipr->kun = 0l;
		 CPreise *pr = new CPreise (VkPr, LdPr, *ipr);
		 if (ipr->vk_pr_eu != 0.0 || 
			 ipr->ld_pr_eu != 0.0)
		 {
				ListRows.Add (pr);
		 }
		 delete ipr;
	}

	DeleteDbRows ();

	ListRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) ListRows.GetNext ()) != NULL)
	{
		if (ipr->ipr.a == 0.0) continue;
	    if (ipr->ipr.vk_pr_eu != 0.0 || 
			 ipr->ipr.ld_pr_eu != 0.0 ||
			 ipr->ipr.add_ek_abs != 0 ||
			 ipr->ipr.add_ek_proz != 0)
		{
			memcpy (&Ipr.ipr, &ipr->ipr, sizeof (IPR));
			Ipr.dbupdate ();
			if (IsChanged (ipr))
			{
				PgrProt.Write ();
			}
		}
	}
	EnableHeadControls (TRUE);
	m_PrGrStuf.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	DestroyRows (m_PrGrList.ListRows);
	CString Date;
	CStrFuncs::SysDate (Date);
	Iprgrstufk.ToDbDate (Date, &Iprgrstufk.iprgrstufk.gue_ab);
	Iprgrstufk.dbupdate ();
	Ipr.commitwork ();
	sql_mode = sql_s;
	if (Choice != NULL)
	{
		Choice->FillList ();
	}
	GrundPreis.SetVisible (FALSE);
	return TRUE;
}

BOOL CPrGrPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_PrGrStuf.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Preisgruppenstufe l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	Ipr.beginwork ();
	m_PrGrList.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	DestroyRows (m_PrGrList.ListRows);
	m_PrGrList.DeleteAllItems ();
	Iprgrstufk.dbdelete ();
	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
	Form.Show ();

	EnableHeadControls (TRUE);
	m_PrGrStuf.SetFocus ();
	Ipr.commitwork ();
	sql_mode = sql_s;
	if (Choice != NULL)
	{
		Choice->FillList ();
	}
	return TRUE;
}


void CPrGrPage::OnPrGrchoice ()
{
    ChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoicePrGrStuf (this);
	    Choice->IsModal = ModalChoice;
		Choice->m_Mdn = Mdn.mdn.mdn;
		Choice->SearchText = Search;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->m_Mdn = Mdn.mdn.mdn;
	Choice->SearchText = Search;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
/*
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		cy -= 100;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
*/
		Choice->MoveWindow (&rect);
		Choice->SetFocus ();

		return;
	}
    if (Choice->GetState ())
    {
		  CPrGrStufList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
          Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
          Iprgrstufk.iprgrstufk.pr_gr_stuf = abl->pr_gr_stuf;
		  if (Iprgrstufk.dbreadfirst () == 0)
		  {
			Form.Show ();
			EnableHeadControls (TRUE);
			m_ZusBz.SetFocus ();
			if (Search == "")
			{
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			}
		  }
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}

void CPrGrPage::OnPrGrSelected ()
{
	if (Choice == NULL) return;
    CPrGrStufList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    Iprgrstufk.iprgrstufk.pr_gr_stuf = abl->pr_gr_stuf;
    if (Iprgrstufk.dbreadfirst () == 0)
	{
		m_ZusBz.EnableWindow (TRUE);
		m_ZusBz.SetFocus ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	if (CloseChoice)
	{
		OnPrGrCanceled (); 
	}
    Form.Show ();
	if (Choice->FocusBack)
	{
		Read ();
		Form.Show ();
		m_ZusBz.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CPrGrPage::OnPrGrCanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CPrGrPage::StepBack ()
{
	if (m_PrGrStuf.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		m_PrGrList.StopEnter ();
		EnableHeadControls (TRUE);
		m_PrGrStuf.SetFocus ();
		DestroyRows (DbRows);
		DestroyRows (ListRows);
	    DestroyRows (m_PrGrList.ListRows);
		m_PrGrList.DeleteAllItems ();
		GrundPreis.SetVisible (FALSE);
	}
	return TRUE;
}

void CPrGrPage::OnCancel ()
{
	StepBack ();
}

void CPrGrPage::OnSave ()
{
	Write ();
}

void CPrGrPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CPrGrPage::FillPrGrStufCombo ()
{
}

void CPrGrPage::FillKunPrCombo ()
{
}

void CPrGrPage::OnDelete ()
{
	if (m_PrGrStuf.IsWindowEnabled ())
	{
		Form.Get ();

		if (Iprgrstufk.iprgrstufk.pr_gr_stuf == 0)
		{
			return;
		}

		CString Message;
		Message.Format (_T("Preisgruppenstufe %ld l�schen"), Iprgrstufk.iprgrstufk.pr_gr_stuf);

		int ret = MessageBox (Message, NULL, 
			MB_YESNO | MB_DEFBUTTON2 | MB_ICONQUESTION);
		if (ret == IDYES)
		{
			Iprgrstufk.dbdelete ();
			m_PrGrStuf.SetFocus ();
 			memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
			Form.Show ();
			DestroyRows (DbRows);
			DestroyRows (ListRows);
	        DestroyRows (m_PrGrList.ListRows);
			m_PrGrList.DeleteAllItems ();
			Ipr.sqlexecute (IprDelCursor);
		}
		m_PrGrStuf.SetFocus ();
		m_PrGrStuf.SetSel (0, -1);
		return;
	}
	m_PrGrList.DeleteRow ();
}

void CPrGrPage::OnInsert ()
{
	m_PrGrList.InsertRow ();
}

BOOL CPrGrPage::Print ()
{
	CProcess print;
	Form.Get ();
// Iprgrstufk.iprgrstufk.pr_gr_stuf
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11121.llf", tmp);
	}
	else
	{
		dName = "11121.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11121\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "pr_gr_stuf %ld %ld\n", Iprgrstufk.iprgrstufk.pr_gr_stuf,
			                               Iprgrstufk.iprgrstufk.pr_gr_stuf);
		fclose (fp);
		Command.Format ("dr70001 -name 11121 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11121";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

BOOL CPrGrPage::PrintAll ()
{
	CProcess print;
	Form.Get ();
// Iprgrstufk.iprgrstufk.pr_gr_stuf
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11121.llf", tmp);
	}
	else
	{
		dName = "11121.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11121\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "MITRANGE 1\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "pr_gr_stuf %ld %ld\n", (long) 0,
			                                 (long) 99999999);
		fclose (fp);
		Command.Format ("dr70001 -name 11121 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11121";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CPrGrPage::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_PrGrList.StartPauseEnter ();
}

void CPrGrPage::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_PrGrList.EndPauseEnter ();
}

void CPrGrPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Enable (enable);
	PosControls.Enable (!enable);
}

void CPrGrPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

void CPrGrPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_PrGrList.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("RemoveKun", cfg_v) == TRUE)
    {
			RemoveKun = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_PrGrList.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_PrGrList.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_PrGrList.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("Aufschlag", cfg_v) == TRUE)
    {
			m_PrGrList.Aufschlag = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CPrGrPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_PrGrList.ListEdit ||
        Control == &m_PrGrList.ListComboBox ||
		Control == &m_PrGrList.SearchListCtrl.Edit)	
	{
		ListCopy ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CPrGrPage::OnPaste ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_PrGrList.ListEdit ||
        Control == &m_PrGrList.ListComboBox ||
		Control == &m_PrGrList.SearchListCtrl.Edit)	
	{
		ListPaste ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CPrGrPage::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_PrGrList.ListEdit.m_hWnd) ||
		IsWindow (m_PrGrList.ListComboBox) ||
		IsWindow (m_PrGrList.SearchListCtrl.Edit))
	{
		m_PrGrList.StopEnter ();
		m_PrGrList.StartEnter (m_PrGrList.EditCol, m_PrGrList.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
		for (int i = 0; i < m_PrGrList.GetItemCount (); i ++)
		{
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = "";
			int cols = m_PrGrList.GetHeaderCtrl ()->GetItemCount ();
			for (int j = 0; j < cols; j ++)
			{
				if (Row != "")
				{
					Row += Separator;
				}
				CString Field = m_PrGrList.GetItemText (i, j);
				Field.TrimRight ();
				if (Field.GetLength () == 0)
				{
					Field = " ";
				}
				Row += Field; 
			}
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

void CPrGrPage::ListPaste ()
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return;
    }

	CString Text;
    HGLOBAL hglbCopy;
    LPTSTR data;

	try
	{
		 hglbCopy =  ::GetClipboardData(CF_TEXT);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
		 data = (LPTSTR) GlobalLock ((HGLOBAL) hglbCopy);
         Text = data;
		 GlobalUnlock ((HGLOBAL) hglbCopy);
		 if (Text != "")
		 {
			m_PrGrList.DeleteAllItems ();
			m_PrGrList.vSelect.clear ();
			m_PrGrList.ListRows.Init ();
			DestroyRows (DbRows);
			DestroyRows (ListRows);
			DestroyRows (m_PrGrList.ListRows);
			m_PrGrList.StopEnter ();
			TCHAR sep [] = {13, 10, 0};
			CToken r;
			r.SetSep (sep);
			r = Text;
			int pos = 0;
			for (int i = 0; i < r.GetAnzToken ();)
			{
				CString Row = r.GetToken (i);
				int cols = m_PrGrList.GetHeaderCtrl ()->GetItemCount ();
				CToken t;
				t.SetSep (Separator.GetBuffer ());
				t = Row;
				if (t.GetAnzToken () < cols - 1) continue;

		        memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
				A_bas.a_bas.a = CStrFuncs::StrToDouble (t.GetToken (m_PrGrList.PosA));
				Ipr.ipr.a = A_bas.a_bas.a;
				A_bas.dbreadfirst ();
				Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 301111

				FillList.InsertItem (i, 0);
				CString A;
				A.Format (_T("%.0lf"), Ipr.ipr.a);
				FillList.SetItemText (A.GetBuffer (), i, m_PrGrList.PosA);
				CString ABz1;
				ABz1 = A_bas.a_bas.a_bz1;
				ABz1 += _T("   ");
				ABz1 += A_bas.a_bas.a_bz2;
				FillList.SetItemText (ABz1.GetBuffer (), i, m_PrGrList.PosABz1);

                Ipr.ipr.vk_pr_eu = CStrFuncs::StrToDouble (t.GetToken (m_PrGrList.PosVkPr));
				CString VkPr;
				m_PrGrList.DoubleToString (Ipr.ipr.vk_pr_eu, VkPr, 4);
				Ipr.ipr.vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
				FillList.SetItemText (VkPr.GetBuffer (), i, m_PrGrList.PosVkPr);

                Ipr.ipr.ld_pr_eu = CStrFuncs::StrToDouble (t.GetToken (m_PrGrList.PosLdPr));
				CString LdPr;
				m_PrGrList.DoubleToString (Ipr.ipr.ld_pr_eu, LdPr, 2);
				FillList.SetItemText (LdPr.GetBuffer (), i, m_PrGrList.PosLdPr);

				Ipr.ipr.add_ek_abs = CStrFuncs::StrToDouble (t.GetToken (m_PrGrList.PosEkAbs));
				CString EkAbs;
				m_PrGrList.DoubleToString (Ipr.ipr.add_ek_abs, EkAbs, 2);
				FillList.SetItemText (EkAbs.GetBuffer (), i, m_PrGrList.PosEkAbs);

				Ipr.ipr.add_ek_proz = CStrFuncs::StrToDouble (t.GetToken (m_PrGrList.PosEkProz));
				CString EkProz;
				m_PrGrList.DoubleToString (Ipr.ipr.add_ek_proz, EkProz, 2);
				FillList.SetItemText (EkProz.GetBuffer (), i, m_PrGrList.PosEkProz);

			    CPreise *ipr = new CPreise (VkPr, LdPr, Ipr.ipr);
				DbRows.Add (ipr);
				ipr = new CPreise (VkPr, LdPr, Ipr.ipr);
				m_PrGrList.ListRows.Add (ipr);
				i ++;
			}
			m_PrGrList.StartEnter (m_PrGrList.PosVkPr, 0);
			
		}
	}
	catch (...) {};
    CloseClipboard();
}

void CPrGrPage::OnAddEk ()
{

	if (m_PrGrStuf.IsWindowEnabled ())
	{
		return;
	}

	if (m_PrGrList.Aufschlag != m_PrGrList.BUTTON &&
		m_PrGrList.Aufschlag != m_PrGrList.ALL)
	{
		return;
	}
	int Row = m_PrGrList.EditRow;
	if (Row < 0) return;
	CPreise *p = (CPreise *) m_PrGrList.ListRows.Get (Row);

	if (m_PrGrList.Aufschlag == m_PrGrList.ALL)
	{
		CString EkAbs =  m_PrGrList.GetItemText (Row, m_PrGrList.PosEkAbs);
		p->ipr.add_ek_abs = CStrFuncs::StrToDouble (EkAbs);
		CString EkProz =  m_PrGrList.GetItemText (Row, m_PrGrList.PosEkProz);
		p->ipr.add_ek_proz = CStrFuncs::StrToDouble (EkProz);
	}

	CAddEk dlg;
	dlg.EkProz.Format ("%.2lf", p->ipr.add_ek_proz);
	CStrFuncs::PointToKomma (dlg.EkProz); 
	dlg.EkAbs.Format ("%.2lf", p->ipr.add_ek_abs);
	CStrFuncs::PointToKomma (dlg.EkAbs); 
	dlg.Font = GetFont ();
	dlg.Parent = this;
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		p->ipr.add_ek_proz = CStrFuncs::StrToDouble (dlg.EkProz);
		p->ipr.add_ek_abs = CStrFuncs::StrToDouble (dlg.EkAbs);
		m_PrGrList.DoubleToString (p->ipr.add_ek_abs, dlg.EkAbs, 2);
		m_PrGrList.DoubleToString (p->ipr.add_ek_proz, dlg.EkProz, 2);
		FillList.SetItemText (dlg.EkAbs.GetBuffer (), Row, m_PrGrList.PosEkAbs);
		FillList.SetItemText (dlg.EkProz.GetBuffer (), Row, m_PrGrList.PosEkProz);
	}
}

BOOL CPrGrPage::TestDecValues (IPR *ipr)
{
	if (ipr->vk_pr_eu > 9999.9999 ||
		ipr->vk_pr_eu < -9999.9999)
	{
		return FALSE;
	}

	if (ipr->ld_pr_eu > 9999.99 ||
		ipr->ld_pr_eu < -9999.99)
	{
		return FALSE;
	}
    
	if (ipr->add_ek_proz > 9999.99 ||
		ipr->add_ek_proz < -9999.99)
	{
		return false;
	}

	if (ipr->add_ek_abs > 9999.99 ||
		ipr->add_ek_abs < -9999.99)
	{
		return false;
	}
	return TRUE;
}

void CPrGrPage::ShowGrundPreis ()
{
	if (m_PrGrStuf.IsWindowEnabled ()) return;

    CString Text;
	Text = m_PrGrList.GetItemText (m_PrGrList.EditRow, m_PrGrList.PosA);
	Ipr.ipr.a = CStrFuncs::StrToDouble (Text);
	A_bas.a_bas.a = Ipr.ipr.a;
	A_bas.dbreadfirst ();
	Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 301111

	if (!GrundPreis.IsWindowVisible ())
	{
		GrundPreis.SetVisible ();
		GrundPreis.Show (Ipr.ipr.mdn, 0, Ipr.ipr.a, A_bas.a_bas.a_typ, A_bas.a_bas.me_einh);
		m_MdnName.ShowWindow (SW_HIDE);
	}
	else
	{
		GrundPreis.SetVisible (FALSE);
		m_MdnName.ShowWindow (SW_SHOWNORMAL);
	}
	CView *parent = (CView *) GetParent ()->GetParent ();
	CDocument *doc = parent->GetDocument ();
	doc->UpdateAllViews (parent);
}

void CPrGrPage::SetGrundPreis ()
{
	if (m_PrGrStuf.IsWindowEnabled ()) return;
	if (GrundPreis.IsWindowVisible ())
	{
		GrundPreis.Show (Ipr.ipr.mdn, 0, Ipr.ipr.a, A_bas.a_bas.a_typ, A_bas.a_bas.me_einh);
	}
}

void CPrGrPage::RowChanged (int NewRow)
{
    CString Text;
	Text = m_PrGrList.GetItemText (NewRow, m_PrGrList.PosA);
	Ipr.ipr.a = CStrFuncs::StrToDouble (Text);
	A_bas.a_bas.a = Ipr.ipr.a;
	A_bas.dbreadfirst ();
	Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 301111

	SetGrundPreis ();
}
