#include "stdafx.h"
#include "smt_zuord.h"

struct SMT_ZUORD smt_zuord, smt_zuord_null;

void SMT_ZUORD_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &smt_zuord.mdn,    SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.fil,    SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.hwg,    SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.wg,    SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.ag,    SQLSHORT, 0);
    sqlout ((short *) &smt_zuord.fil,SQLSHORT,0);
    sqlout ((short *) &smt_zuord.hwg,SQLSHORT,0);
    sqlout ((short *) &smt_zuord.mdn,SQLSHORT,0);
    sqlout ((short *) &smt_zuord.verk_st,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &smt_zuord.zuord_dat,SQLDATE,0);
    sqlout ((short *) &smt_zuord.fil_gr,SQLSHORT,0);
    sqlout ((short *) &smt_zuord.wg,SQLSHORT,0);
    sqlout ((short *) &smt_zuord.ag,SQLSHORT,0);
            cursor = sqlcursor (_T("select smt_zuord.fil,  ")
_T("smt_zuord.hwg,  smt_zuord.mdn,  smt_zuord.verk_st,  ")
_T("smt_zuord.zuord_dat,  smt_zuord.fil_gr,  smt_zuord.wg,  smt_zuord.ag from smt_zuord ")

#line 17 "smt_zuord.rpp"
                                _T("where mdn = ? ")
                                _T("and fil_gr = ? ")
                                _T("and fil = ? ")
                                _T("and hwg = ? ")
                                _T("and wg = ? ")
                                _T("and ag = ?"));  
    sqlin ((short *) &smt_zuord.fil,SQLSHORT,0);
    sqlin ((short *) &smt_zuord.hwg,SQLSHORT,0);
    sqlin ((short *) &smt_zuord.mdn,SQLSHORT,0);
    sqlin ((short *) &smt_zuord.verk_st,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &smt_zuord.zuord_dat,SQLDATE,0);
    sqlin ((short *) &smt_zuord.fil_gr,SQLSHORT,0);
    sqlin ((short *) &smt_zuord.wg,SQLSHORT,0);
    sqlin ((short *) &smt_zuord.ag,SQLSHORT,0);
            sqltext = _T("update smt_zuord set ")
_T("smt_zuord.fil = ?,  smt_zuord.hwg = ?,  smt_zuord.mdn = ?,  ")
_T("smt_zuord.verk_st = ?,  smt_zuord.zuord_dat = ?,  ")
_T("smt_zuord.fil_gr = ?,  smt_zuord.wg = ?,  smt_zuord.ag = ? ")

#line 24 "smt_zuord.rpp"
                                _T("where mdn = ? ")
                                _T("and fil_gr = ? ")
                                _T("and fil = ? ")
                                _T("and hwg = ? ")
                                _T("and wg = ? ")
                                _T("and ag = ?");  
            sqlin ((short *)   &smt_zuord.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.fil_gr,  SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.fil,  SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.hwg,    SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.wg,    SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.ag,    SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &smt_zuord.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.fil_gr,  SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.fil,  SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.hwg,    SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.wg,    SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.ag,    SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select mdn from smt_zuord ")
                                _T("where mdn = ? ")
                                _T("and fil_gr = ? ")
                                _T("and fil = ? ")
                                _T("and hwg = ? ")
                                _T("and wg = ? ")
                                _T("and ag = ?"));  
            sqlin ((short *)   &smt_zuord.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.fil_gr,  SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.fil,  SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.hwg,    SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.wg,    SQLSHORT, 0);
            sqlin ((short *)   &smt_zuord.ag,    SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from smt_zuord ")
                                _T("where mdn = ? ")
                                _T("and fil_gr = ? ")
                                _T("and fil = ? ")
                                _T("and hwg = ? ")
                                _T("and wg = ? ")
                                _T("and ag = ?"));  
    sqlin ((short *) &smt_zuord.fil,SQLSHORT,0);
    sqlin ((short *) &smt_zuord.hwg,SQLSHORT,0);
    sqlin ((short *) &smt_zuord.mdn,SQLSHORT,0);
    sqlin ((short *) &smt_zuord.verk_st,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &smt_zuord.zuord_dat,SQLDATE,0);
    sqlin ((short *) &smt_zuord.fil_gr,SQLSHORT,0);
    sqlin ((short *) &smt_zuord.wg,SQLSHORT,0);
    sqlin ((short *) &smt_zuord.ag,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into smt_zuord (")
_T("fil,  hwg,  mdn,  verk_st,  zuord_dat,  fil_gr,  wg,  ag) ")

#line 65 "smt_zuord.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?)")); 

#line 67 "smt_zuord.rpp"
}
