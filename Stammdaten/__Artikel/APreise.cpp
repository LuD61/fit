#include "StdAfx.h"
#include "apreise.h"

CAPreise::CAPreise(void)
{
	cEk = _T("0.0");
	cVk = _T("0.0");
	memcpy (&a_pr, &a_pr_null, sizeof (A_PR));
}

CAPreise::CAPreise(CString& cEk, CString& cVk, A_PR& a_pr)
{
	this->cEk = cEk.Trim ();
	this->cVk = cVk.Trim ();
	memcpy (&this->a_pr, &a_pr, sizeof (A_PR));
}

CAPreise::~CAPreise(void)
{
}
