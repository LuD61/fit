#ifndef _QUID_DEF
#define _QUID_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct QUID {
   short          quid_nr;
   TCHAR          bezeichnung[81];
   TCHAR          quid_kat[4];
   short          fleisch_kz;
   short          fett_grenze;
};
extern struct QUID quid, quid_null;

#line 8 "Quid.rh"

class QUID_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               QUID quid;
               QUID_CLASS () : DB_CLASS ()
               {
               }
};
#endif
