#ifndef _HST_INFO_DEF
#define _HST_INFO_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct HST_INFO {
   double         a;
   TCHAR          rh_stoff_gew[513];
   TCHAR          herstellung[513];
   TCHAR          zbr_empf[513];
   TCHAR          blg_empf[513];
};
extern struct HST_INFO hst_info, hst_info_null;

#line 8 "hst_info.rh"

class HST_INFO_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               HST_INFO hst_info;  
               HST_INFO_CLASS () : DB_CLASS ()
               {
               }
};
#endif
