#pragma once
#include "NumEdit.h"
#include "TextEdit.h"
#include "a_bas.h"
#include "a_kalk_mat.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "Ptabn.h"

// CSpannDialogMat-Dialogfeld

class CSpannDialogMat : public CDialog
{
	DECLARE_DYNAMIC(CSpannDialogMat)

public:
	CSpannDialogMat(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CSpannDialogMat();

// Dialogfelddaten
	enum { IDD = IDD_SPANNE_MAT };

protected:
	BOOL visible;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	CFont Font;
	CFormTab Form;
	CCtrlGrid CtrlGrid;

public:
	enum
	{
		Aufschlag = 1,
		AufschlagPlus = 2,
		Abschlag = 3,
		AbschlagPlus = 4,
	};

	int MarktSpPar;
	double mwst;
	double SpEkIst;
	double SpVkIst;

	int StdCellHeight;
	A_BAS_CLASS *A_bas;
	A_KALK_MAT_CLASS *A_kalk_mat;
	PTABN_CLASS Ptabn;

	CStatic m_LMatoB;
	CStatic m_LKost;
	CStatic m_LHkVollk;
	CStatic m_LSpanne;
	CStatic m_LSk;
	CStatic m_LKalk;

	CNumEdit m_MatoB;
	CNumEdit m_Kost;
	CNumEdit m_HkVollk;

	CNumEdit m_PSk;

	CNumEdit m_Sk;

	void Calculate ();
	void Show ();
	void Get ();
	void SetVisible (BOOL visible);
};
