// EanListDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "EanListDlg.h"
#include ".\eanlistdlg.h"
#include "StrFuncs.h"


// CEanListDlg-Dialogfeld

IMPLEMENT_DYNAMIC(CEanListDlg, CDialog)
CEanListDlg::CEanListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEanListDlg::IDD, pParent)
{
	A_bas = NULL;
	Basis = NULL;
	PageUpdate = NULL;
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
	dcs_a_par = -1;
	waa_a_par = -1;
}

CEanListDlg::~CEanListDlg()
{
}

void CEanListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EAN_LIST, m_EanList);
	DDX_Control(pDX, IDC_EAN_TEXT, m_LEanText);
}


BEGIN_MESSAGE_MAP(CEanListDlg, CDialog)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_EAN_LIST, OnLvnItemchangedEanList)
END_MESSAGE_MAP()


// CEanListDlg-Meldungshandler

BOOL CEanListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	A_bas   = &Basis->A_bas;
	A_ean   = &Basis->A_ean;
	A_krz   = &Basis->A_krz;
	A_hndw  = &Basis->A_hndw;
	Sys_par = &Basis->Sys_par;
	m_EanList.A_bas = A_bas;
	m_EanList.A_ean = A_ean;
	m_EanList.A_krz = A_krz;

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		LFont.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		LFont.CreatePointFont (95, _T("Dlg"));
	}
	m_EanList.Init ();
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0, 0, 0);
    CtrlGrid.SetCellHeight (15);
//    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 0);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LEanText = new CCtrlInfo (&m_LEanText, 1, 1, 1, 1);
	c_LEanText->BottomControl = &m_EanList;
	CtrlGrid.Add (c_LEanText);

	CCtrlInfo *c_EanList = new CCtrlInfo (&m_EanList, 0, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_EanList);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_EanList.SetFont (&LFont);

	CtrlGrid.Display ();

	return FALSE;
}

HBRUSH CEanListDlg::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CEanListDlg::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CEanListDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			if (pMsg->wParam == VK_F5)
			{
				PageUpdate->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}
/*
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
*/
	}
	return FALSE;
}


void CEanListDlg::SetBasis (CBasisdatenPage *Basis)
{
	this->Basis = Basis;
}

void CEanListDlg::Update ()
{
	memcpy (&A_ean->a_ean, &a_ean_null, sizeof (A_EAN)); 
	m_EanList.Read ();
}

void CEanListDlg::Read ()
{
	memcpy (&A_ean->a_ean, &a_ean_null, sizeof (A_EAN)); 
	m_EanList.Read ();
	m_EanList.SetItemState (0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	SetSelectedItem (0);
	for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
	{
			CUpdateEvent *event = *e;
			event->Update ();
	}
}

void CEanListDlg::OnLvnItemchangedEanList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;
	int idx = m_EanList.GetNextItem (-1, LVNI_SELECTED);
	if (idx != -1)
	{
		SetSelectedItem (idx);
		for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
		{
			CUpdateEvent *event = *e;
			event->Update ();
		}
	}
}

void CEanListDlg::SetSelectedItem (int idx)
{
	if (idx != -1)
	{
		CString Ean = m_EanList.GetItemText (idx, 1);
		CString EanBz = m_EanList.GetItemText (idx, 2);
		CString HEan = m_EanList.GetItemText (idx, 3);
		CString AKrz = m_EanList.GetItemText (idx, 4);

		A_ean->a_ean.ean = CStrFuncs::StrToDouble (Ean);
		_tcscpy (A_ean->a_ean.ean_bz, EanBz.GetBuffer ()); 
		if (HEan == _T("X"))
		{
			_tcscpy (A_ean->a_ean.h_ean_kz, _T("1"));
		}
		else
		{
			_tcscpy (A_ean->a_ean.h_ean_kz, _T("0"));
		}
	}
}

BOOL CEanListDlg::HasFocus ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_EanList) return FALSE;
	return TRUE;
}

void CEanListDlg::Delete ()
{
	if (!HasFocus ()) return;
	DeleteRow ();
}

void CEanListDlg::DeleteRow ()
{
	int idx = m_EanList.GetNextItem (-1, LVNI_SELECTED);
	if (idx == -1) return;
	CString AKrz = m_EanList.GetItemText (idx, 4);
	A_krz->a_krz.a_krz = _tstol (AKrz.GetBuffer ());
	A_krz->dbreadfirst ();
	CString Ean = m_EanList.GetItemText (idx, 1);
	A_ean->a_ean.ean = CStrFuncs::StrToDouble (Ean);
	A_ean->dbdelete ();
	m_EanList.DeleteItem (idx);
	if (idx >= m_EanList.GetItemCount ())
	{
		idx = m_EanList.GetItemCount () - 1;
		SetSelectedItem (idx);
	}
	if (m_EanList.GetItemCount () > 0)
	{
		Ean = m_EanList.GetItemText (idx, 1);
		CString EanBz = m_EanList.GetItemText (idx, 2);
		CString HEan = m_EanList.GetItemText (idx, 3);

		A_ean->a_ean.ean = CStrFuncs::StrToDouble (Ean);
		_tcscpy (A_ean->a_ean.ean_bz, EanBz.GetBuffer ()); 
		if (HEan == _T("X"))
		{
			_tcscpy (A_ean->a_ean.h_ean_kz, _T("1"));
		}
		else
		{
			_tcscpy (A_ean->a_ean.h_ean_kz, _T("0"));
		}
		if (A_bas->a_bas.a_typ == Basis->Hndw || A_bas->a_bas.a_typ == Basis->Eig)
		{
			SetSysPar ();
			A_krz->dbdelete ();
			if (_tcscmp (A_hndw->a_hndw.verk_art, _T("K")) == 0 &&
				dcs_a_par == 1)
			{
				Basis->AutoNr.nveinid (0, 0, "a_krz", A_krz->a_krz.a_krz);
			}
		}
	}
	else
	{
		memcpy (&A_ean->a_ean, &a_ean_null, sizeof (A_EAN));
		if (A_krz->a_krz.a_krz != 0 && 
			A_bas->a_bas.a_typ == Basis->Hndw || A_bas->a_bas.a_typ == Basis->Eig)
		{
			A_krz->a_krz.a_herk = A_bas->a_bas.a;
			_tcscpy (A_krz->a_krz.herk_kz, _T("1"));
			A_krz->dbupdate ();
			A_hndw->a_hndw.a_krz = A_krz->a_krz.a_krz;
		}
	}
	for (std::vector<CUpdateEvent *>::iterator e = UpdateTab.begin ();
			                                       e != UpdateTab.end (); ++ e)
	{
		CUpdateEvent *event = *e;
		event->Update ();
	}
}

void CEanListDlg::SetSysPar()
{
	if (dcs_a_par == -1)
	{
		strcpy ((LPSTR) sys_par.sys_par_nam, "dcs_a_par");
		if (Sys_par->dbreadfirst () == 0)
		{
			LPSTR p = (LPSTR) sys_par.sys_par_wrt;
			CDbUniCode::DbToUniCode (sys_par.sys_par_wrt, p);
			dcs_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}

	if (waa_a_par == -1)
	{
		strcpy ((LPSTR) sys_par.sys_par_nam, "waa_a_par");
		if (Sys_par->dbreadfirst () == 0)
		{
			LPSTR p = (LPSTR) sys_par.sys_par_wrt;
			CDbUniCode::DbToUniCode (sys_par.sys_par_wrt, p);
			waa_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}
}
