#ifndef _IMAGE_CTRL_DEF
#define _IMAGE_CTRL_DEF
#pragma once
#include "afxwin.h"
#include "atlimage.h"
#include "imagelistener.h"

class CImageCtrl :
	public CStatic
{
protected :
	DECLARE_DYNAMIC(CImageCtrl)
	CString ImageFile;
    HBITMAP hBitmapZoom;
	int rx;
	int ry;
	DWORD Style;
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
	DECLARE_MESSAGE_MAP()
public:

	enum 
	{
		OrgSize = 1,
		ZoomToOrg = 2,
		ZoomToWindow = 3
	} IMAGE_MODE;

	enum
	{
		No = 1,
		Left = 2,
		Right = 3
	} DRAW_MODE;

	enum
	{
		ImageShow = 1,
		Link = 2,
		Unlink = 3,
	};

    BOOL IsLoading;
	CImageListener *ImageListener;
	CImage Image;
	CImage RImage;
	BOOL ShowImage;
    int Mode;
	int Rotation;
	CImageCtrl(void);
	~CImageCtrl(void);
	HRESULT SetImageFile (CString &);
	HRESULT SetImageFile (LPTSTR);
	CString& GetImageFile ();
	void Draw (CDC&);
	void DrawText (CDC&);
	void DrawBlackRect (CDC&);
    void StretchBlt (HDC hdc, int cx, int cy);
    void StretchBlt (HDC hdc, int x, int y, int cx, int cy);
    void StretchBlt (HDC hdc, int x, int y, int cx, int cy, int draw);
    virtual BOOL Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect,
                        CWnd* pParentWnd,   UINT nID = 0xffff);
    afx_msg void OnVScroll(UINT nSBCode,  UINT nPos,CScrollBar* pScrollBar);
	BOOL Show (LPSTR);
    BOOL OnRButtonDown( UINT flags, CPoint point);
    BOOL OnLButtonDblClk( UINT flags, CPoint  point);
    afx_msg void OnShowImage();
    afx_msg void OnLinkImage();
    afx_msg void OnUnlinkImage();
    afx_msg void OnOpenImage();
    afx_msg void OnOpenImageWith();
};
#endif