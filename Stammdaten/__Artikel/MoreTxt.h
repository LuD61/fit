#pragma once
#include "afxwin.h"
#include "a_bas.h"
#include "formtab.h"
#include "NumEdit.h"
#include "ChoiceAtxt.h"
#include "StaticButton.h"


// CMoreTxt-Dialogfeld

class CMoreTxt : public CDialog
{
	DECLARE_DYNAMIC(CMoreTxt)

public:
	CMoreTxt(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CMoreTxt();

// Dialogfelddaten
	enum { IDD = IDD_MORE_TXT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
	A_BAS_CLASS A_bas;
	CChoiceAtxt *ChoiceAtxt;
	CFormTab Form;
	CNumEdit m_TxtNr1;
	CNumEdit m_TxtNr2;
	CNumEdit m_TxtNr3;
	CNumEdit m_TxtNr4;
	CNumEdit m_TxtNr5;
	CNumEdit m_TxtNr6;
	CNumEdit m_TxtNr7;
	CNumEdit m_TxtNr8;
	CNumEdit m_TxtNr9;
	CNumEdit m_TxtNr10;
	CNumEdit *ActiveEdit;
	CStaticButton m_Enter;

	virtual void OnOK ();

	void OnTxtchoice (long *TxtNr);
	afx_msg void OnBnClickedButxt1();
	afx_msg void OnBnClickedButxt2();
	afx_msg void OnBnClickedButxt3();
	afx_msg void OnBnClickedButxt4();
	afx_msg void OnBnClickedButxt5();
	afx_msg void OnBnClickedButxt6();
	afx_msg void OnBnClickedButxt7();
	afx_msg void OnBnClickedButxt8();
	afx_msg void OnBnClickedButxt9();
	afx_msg void OnBnClickedButxt10();
	afx_msg void OnEnterTxt();
	afx_msg void OnEnSetfocusTxtNr1();
	afx_msg void OnEnSetfocusTxtNr2();
	afx_msg void OnEnSetfocusTxtNr3();
	afx_msg void OnEnSetfocusTxtNr4();
	afx_msg void OnEnSetfocusTxtNr5();
	afx_msg void OnEnSetfocusTxtNr6();
	afx_msg void OnEnSetfocusTxtNr7();
	afx_msg void OnEnSetfocusTxtNr8();
	afx_msg void OnEnSetfocusTxtNr9();
	afx_msg void OnEnSetfocusTxtNr10();
};
