#ifndef _A_IPRGRSTUFK_DEF
#define _A_IPRGRSTUFK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct IPRGRSTUFK {
   short          mdn;
   long           pr_gr_stuf;
   char           zus_bz[25];
   DATE_STRUCT    gue_ab;
   short          delstatus;
   short          waehrung;
};
extern struct IPRGRSTUFK iprgrstufk, iprgrstufk_null;

#line 8 "Iprgrstufk.rh"

class IPRGRSTUFK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               IPRGRSTUFK iprgrstufk;
               IPRGRSTUFK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
