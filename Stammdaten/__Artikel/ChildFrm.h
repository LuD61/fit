// ChildFrm.h : Schnittstelle der Klasse CChildFrame
//


#pragma once
#include "vector.h"


class CChildFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CChildFrame)
public:
	CChildFrame();

// Attribute
public:

// Operationen
public:

// Überschreibungen
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual BOOL OnCreateClient(LPCREATESTRUCT, CCreateContext*);

// Implementierung
public:
	virtual ~CChildFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
public:
	static CVector ArtWnd;
	static CVector MatWnd;
	static CVector FreiWnd;
	static CVector APrWnd;
	static CVector StdPrWnd;
	BOOL InitSize;

	afx_msg void OnDestroy();
    afx_msg void OnSize(UINT, int, int);
	void OnRecChangeMat();
	void OnRecChangeArt();
	void OnActivateMat();
	void OnActivateArt();
    void OnRecChange();
};
