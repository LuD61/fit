#pragma once
#include "CtrlGrid.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "EmbTreeCtrl.h"
#include "BasisdatenPage.h"
#include "UpdateEvent.h"
#include "PageUpdate.h"
#include "a_bas.h"
#include "A_hndw.h"
#include "a_emb.h"
#include "a_ean.h"
#include "A_krz.h"
#include "Sys_par.h"


// CEmbListDlg-Dialogfeld

class CEmbListDlg : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CEmbListDlg)

public:
	CEmbListDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CEmbListDlg();

// Dialogfelddaten
	enum { IDD = IDD_EMB_LIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual void OnSize (UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()

public:
	int dcs_a_par;
	int waa_a_par;
	CBasisdatenPage *Basis;
	CPageUpdate *PageUpdate;
	A_BAS_CLASS *A_bas;
	A_HNDW_CLASS *A_hndw;
	A_EAN_CLASS *A_ean;
	A_EMB_CLASS *A_emb;
	A_KRZ_CLASS *A_krz;
	SYS_PAR_CLASS *Sys_par;
	CFont Font;
	CFont LFont;
	CCtrlGrid CtrlGrid;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	CStatic m_LEmbText;
	CEmbTreeCtrl m_EmbList;
	void SetBasis (CBasisdatenPage *Basis);
	virtual void Update ();
	virtual void Read ();
	virtual void Delete ();
	BOOL HasFocus ();
	afx_msg void OnTvnSelchangedEmbList(NMHDR *pNMHDR, LRESULT *pResult);
	void SetSysPar();
	virtual void DeleteRow ();
};
