#include "stdafx.h"
#include "gr_zuord.h"

struct GR_ZUORD gr_zuord, gr_zuord_null;

void GR_ZUORD_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &gr_zuord.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &gr_zuord.gr,   SQLSHORT, 0);
    sqlout ((short *) &gr_zuord.gr,SQLSHORT,0);
    sqlout ((TCHAR *) gr_zuord.gr_bz1,SQLCHAR,25);
    sqlout ((TCHAR *) gr_zuord.gr_bz2,SQLCHAR,25);
    sqlout ((short *) &gr_zuord.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) gr_zuord.smt_kz,SQLCHAR,2);
            cursor = sqlcursor (_T("select gr_zuord.gr,  ")
_T("gr_zuord.gr_bz1,  gr_zuord.gr_bz2,  gr_zuord.mdn,  gr_zuord.smt_kz from gr_zuord ")

#line 13 "gr_zuord.rpp"
                                  _T("where mdn = ? ")
                                  _T("and gr = ?"));
    sqlin ((short *) &gr_zuord.gr,SQLSHORT,0);
    sqlin ((TCHAR *) gr_zuord.gr_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) gr_zuord.gr_bz2,SQLCHAR,25);
    sqlin ((short *) &gr_zuord.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) gr_zuord.smt_kz,SQLCHAR,2);
            sqltext = _T("update gr_zuord set ")
_T("gr_zuord.gr = ?,  gr_zuord.gr_bz1 = ?,  gr_zuord.gr_bz2 = ?,  ")
_T("gr_zuord.mdn = ?,  gr_zuord.smt_kz = ? ")

#line 16 "gr_zuord.rpp"
                                  _T("where mdn = ? ")
                                  _T("and gr = ?");
            sqlin ((short *)   &gr_zuord.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &gr_zuord.gr,   SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &gr_zuord.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &gr_zuord.gr,   SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select gr from gr_zuord ")
                                  _T("where mdn = ? ")
                                  _T("and gr = ?"));
            sqlin ((short *)   &gr_zuord.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &gr_zuord.gr,   SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from gr_zuord ")
                                  _T("where mdn = ? ")
                                  _T("and gr = ?"));
    sqlin ((short *) &gr_zuord.gr,SQLSHORT,0);
    sqlin ((TCHAR *) gr_zuord.gr_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) gr_zuord.gr_bz2,SQLCHAR,25);
    sqlin ((short *) &gr_zuord.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) gr_zuord.smt_kz,SQLCHAR,2);
            ins_cursor = sqlcursor (_T("insert into gr_zuord (")
_T("gr,  gr_bz1,  gr_bz2,  mdn,  smt_kz) ")

#line 33 "gr_zuord.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?)"));

#line 35 "gr_zuord.rpp"
}
