#include "StdAfx.h"
#include "PrpUserListCtrl.h"
#include "StrFuncs.h"
#include "resource.h"

CPrpUserListCtrl::CPrpUserListCtrl(void)
{
	ListRows.Init ();
}

CPrpUserListCtrl::~CPrpUserListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CPrpUserListCtrl, CEditListCtrl)
//	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CPrpUserListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (1, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (1, 0);
	}
}

void CPrpUserListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	CEditListCtrl::StartEnter (col, row);
}

void CPrpUserListCtrl::StopEnter ()
{
	CEditListCtrl::StopEnter ();
}

void CPrpUserListCtrl::SetSel (CString& Text)
{
/*
   {
		ListEdit.SetSel (0, -1);
   }
*/
	Text.TrimRight ();
	int cpos = Text.GetLength ();
	ListEdit.SetSel (cpos, cpos);
}

void CPrpUserListCtrl::FormatText (CString& Text)
{
}

void CPrpUserListCtrl::NextRow ()
{
	int count = GetItemCount ();
	SetEditText ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;

	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
	StartEnter (EditCol, EditRow);
}

void CPrpUserListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CPrpUserListCtrl::LastCol ()
{
	if (EditCol < 2) return FALSE;
	return TRUE;
}

void CPrpUserListCtrl::OnReturn ()
{

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (LastCol ())
	{
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
//		EditCol = 0;
		if (EditRow == rowCount)
		{
			EditCol = 1;
		}
		else
		{
			EditCol = 2;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CPrpUserListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CPrpUserListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	StopEnter ();
	EditCol --;
    StartEnter (EditCol, EditRow);
}

BOOL CPrpUserListCtrl::InsertRow ()
{
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T("  "), EditRow, 1);
	FillList.SetItemText (_T("  "), EditRow, 2);
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CPrpUserListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	return CEditListCtrl::DeleteRow ();
}

BOOL CPrpUserListCtrl::AppendEmpty ()
{
	int rowCount = GetItemCount ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CWnd *header = GetHeaderCtrl ();
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText (_T("  "), rowCount, 1);
	FillList.SetItemText (_T(" "), rowCount, 2);
	rowCount = GetItemCount ();
	return TRUE;
}

void CPrpUserListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CPrpUserListCtrl::RunItemClicked (int Item)
{
}

void CPrpUserListCtrl::RunCtrlItemClicked (int Item)
{
}

void CPrpUserListCtrl::RunShiftItemClicked (int Item)
{
}

void CPrpUserListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	Text = cText.Trim ();
}

void CPrpUserListCtrl::ScrollPositions (int pos)
{
}

