#pragma once
#include "A_bas.h"

class CArtImages
{
public:
	A_BAS_CLASS A_bas;
	CFile File;
	int cursor;
	CString FileName;
	WIN32_FIND_DATA fData;
	CString Path;
    CString ServerPath;
	CArtImages(void);
	~CArtImages(void);
	BOOL Open ();
	void Close ();
	BOOL WriteFromPath ();
    BOOL WriteLinked ();
	BOOL MakeServerPath (CString& Path, CString& ServerPath); 
	BOOL CopyToServer (CString& Path);
};
