// Artikel.cpp : Definiert das Klassenverhalten f�r die Anwendung.
//

#include "stdafx.h"
#include "Artikel.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "ArtikelDoc.h"
#include "ArtikelView.h"
#include "Art1.h"
#include "Art2.h"
#include "Ipr1.h"
#include "Apr1.h"
#include ".\artikel.h"
#include "mo_progcfg.h"
#include "Process.h"
//#include "SToken.h" //LAC-133
//#include "Util.h" //LAC-133

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CArtikelApp

BEGIN_MESSAGE_MAP(CArtikelApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// Dateibasierte Standarddokumentbefehle
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	// Standarddruckbefehl "Seite einrichten"
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
	ON_COMMAND(ID_ARTIKEL, OnArtikelstamm)
	ON_COMMAND(ID_MATERIAL, OnMaterialstamm)
	ON_COMMAND(ID_APR, OnAPr)
	ON_COMMAND(ID_STD_PR, OnStdPr)
	ON_EN_CHANGE(IDC_A, OnEnChangeA)
	ON_COMMAND(ID_EKPR, &CArtikelApp::OnEkpr)
	ON_COMMAND(ID_SPRACHE, &CArtikelApp::OnSprache)
END_MESSAGE_MAP()

CDocTemplate *ArtInstance = NULL; 
CDocTemplate *MatInstance = NULL; 
CDocTemplate *APrInstance = NULL; 
CDocTemplate *StdPrInstance = NULL; 
CDocTemplate *FreiInstance = NULL; 

// CArtikelApp-Erstellung

PROG_CFG Cfg ("Artikel");

CString CArtikelApp::EnterEk = _T("PreisErfassung");
CString CArtikelApp::EnterSprache = _T("d:\\user\\fit\\bin\\SprachenEditor.jar");
BOOL CArtikelApp::WithEK = FALSE;
BOOL CArtikelApp::WithSpracheneditor = TRUE;

CArtikelApp::CArtikelApp()
{
	EnableHtmlHelp();

	// TODO: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance positionieren
}


// Das einzige CArtikelApp-Objekt

CArtikelApp theApp;

// CArtikelApp Initialisierung

BOOL CArtikelApp::InitInstance()
{
	// InitCommonControls() ist f�r Windows XP erforderlich, wenn ein Anwendungsmanifest
	// die Verwendung von ComCtl32.dll Version 6 oder h�her zum Aktivieren
	// von visuellen Stilen angibt. Ansonsten treten beim Erstellen von Fenstern Fehler auf.
	InitCommonControls();
    LoadLibrary (_T("RICHED32.DLL"));
    a = 0;
	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// OLE-Bibliotheken initialisieren
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standardinitialisierung
	// Wenn Sie diese Features nicht verwenden und die Gr��e
	// der ausf�hrbaren Datei verringern m�chten, entfernen Sie
	// die nicht erforderlichen Initialisierungsroutinen.
	// �ndern Sie den Registrierungsschl�ssel unter dem Ihre Einstellungen gespeichert sind.
	// TODO: �ndern Sie diese Zeichenfolge entsprechend,
	// z.B. zum Namen Ihrer Firma oder Organisation.
	SetRegistryKey(_T("Vom lokalen Anwendungs-Assistenten generierte Anwendungen"));
	LoadStdProfileSettings(4);  // Standard INI-Dateioptionen laden (einschlie�lich MRU)
	// Dokumentvorlagen der Anwendung registrieren. Dokumentvorlagen
	//  dienen als Verbindung zwischen Dokumenten, Rahmenfenstern und Ansichten.
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_ArtikelTYPE,
		RUNTIME_CLASS(CArtikelDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CArt1));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	pDocTemplate = new CMultiDocTemplate(IDR_MatTYPE,
		RUNTIME_CLASS(CArtikelDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CArt2));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	pDocTemplate = new CMultiDocTemplate(IDR_IprManTYPE,
		RUNTIME_CLASS(CArtikelDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CApr1));
	AddDocTemplate(pDocTemplate);

	pDocTemplate = new CMultiDocTemplate(IDR_IprManTYPE,
		RUNTIME_CLASS(CArtikelDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
		RUNTIME_CLASS(CIpr1));
	AddDocTemplate(pDocTemplate);

    char cfg_v [256];

    if (Cfg.GetCfgValue ("WithEK", cfg_v) == TRUE)
    {
			WithEK = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("EnterEK", cfg_v) == TRUE)
    {
			EnterEk = cfg_v;
    }
    if (Cfg.GetCfgValue ("EnterSpracheneditor", cfg_v) == TRUE)
    {
			EnterSprache = cfg_v;
    }
    if (Cfg.GetCfgValue ("WithSpracheneditor", cfg_v) == TRUE)
    {
			WithSpracheneditor = atoi (cfg_v);
    }

	// Haupt-MDI-Rahmenfenster erstellen
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;
	// Rufen Sie DragAcceptFiles nur auf, wenn eine Suffix vorhanden ist.
	//  In einer MDI-Anwendung ist dies unmittelbar nach dem Festlegen von m_pMainWnd erforderlich
	// Befehlszeile parsen, um zu pr�fen auf Standardumgebungsbefehle DDE, Datei offen
	CCommandLineInfo cmdInfo;
//	ParseCommandLine(cmdInfo);
	// Verteilung der in der Befehlszeile angegebenen Befehle. Es wird FALSE zur�ckgegeben, wenn
	// die Anwendung mit /RegServer, /Register, /Unregserver oder /Unregister gestartet wurde.

//	if (!ProcessShellCommand(cmdInfo))
//		return FALSE;
	// Das Hauptfenster ist initialisiert und kann jetzt angezeigt und aktualisiert werden.

	pMainFrame->ShowWindow(SW_SHOWMAXIMIZED);
	pMainFrame->UpdateWindow();
	StartPage = Artikel;
    if (Cfg.GetCfgValue ("StartPage", cfg_v) == TRUE)
    {
			StartPage = atoi (cfg_v);
			StartPage = (StartPage < None) ? StartPage = None : StartPage;
			StartPage = (StartPage > Material) ? StartPage = Material : StartPage;
    }
    switch (StartPage)
	{
	case Artikel:
			OnArtikelstamm();
			break;
	case Material:
			OnMaterialstamm();
			break;
	}
	return TRUE;
}


int CArtikelApp::ExitInstance() 
{
   // TODO: Add your specialized code here and/or
   // call the base class.

// Das ist eine Notl�sung, da in der Releaseversion von CWinApp::ExitInstance() 
// eine dubiose Fehlermeldung �ber Fehler beim Dateiinfoschreiben erzeugt wird.
// Keine Ahnung, woran das liegt.
   ExitProcess (0);
   return CWinApp::ExitInstance();
}

// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

class CAboutDlgEx : public CDialog
{
public:
	CAboutDlgEx();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlgEx::CAboutDlgEx() : CDialog(CAboutDlgEx::IDD)
{
}

void CAboutDlgEx::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

}

BEGIN_MESSAGE_MAP(CAboutDlgEx, CDialog)
END_MESSAGE_MAP()

// Anwendungsbefehl zum Ausf�hren des Dialogfelds

void CArtikelApp::OnAppAbout()
{
	CAboutDlgEx aboutDlg;
	aboutDlg.DoModal();
}


// CArtikelApp Meldungshandler

void CArtikelApp::OnArtikelstamm()
{
	POSITION pos;
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		if (dt == ArtInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->ArtWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->ArtWnd);
				MainFrame->ArtWnd->SetFocus ();
				return;
			}
		}

		ArtInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
			dt->SetDefaultTitle (dc);
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}

void CArtikelApp::OnMaterialstamm()
{
	POSITION pos;
	BasisdatenPage->Write (); //WAL-130
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		if (dt == MatInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->MatWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->MatWnd);
				return;
			}
		}

		MatInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
			dt->SetDefaultTitle (dc);
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}

void CArtikelApp::OnAPr()
{
	BasisdatenPage->Write (); //WAL-130
	//LAC-133 A
	PersName = "";
	CString Line = GetCommandLine ();
	char * pLine = NULL;
	char * pLine2 = NULL;
	CString Msg;

	pLine = strstr ( Line.GetBuffer (),".exe");

	if (pLine != NULL)
	{
		if (strlen(pLine) > 4)   strncpy (pLine,"    ",4);
	}
	// jetzt noch die beiden Hochkomma entfernen
	if (pLine != NULL)
	{
		pLine = strstr (pLine,"\"");
	}
    if (pLine != NULL)
	{
		if (strlen(pLine) > 0) strncpy (pLine," ",1);
	     pLine2 = strstr (pLine,"\"");
	}
    if (pLine2 != NULL)
	{
		if (strlen(pLine) > 0) strncpy (pLine2," ",1);
	}

	if (pLine != NULL) PersName.Format (_T("%s"), pLine);
	if (PersName.Trim () != "")
	{
		if (PersName.GetLength() < 9)
		{
			_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
			if (Sys_ben.dbreadfirstpers_nam () == 0)
			{

				if (Sys_ben.sys_ben.berecht > 0) return ; 
			}
		}
	}
	//LAC-133 E

	POSITION pos;
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		dt = GetNextDocTemplate (pos);
	}
	if (dt != NULL)
	{
		if (dt == APrInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->APrWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->APrWnd);
				return;
			}
		}

		APrInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
//			dt->SetDefaultTitle (dc);
			dc->SetTitle (_T("Ladenpreise"));
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}



void CArtikelApp::OnStdPr()
{
	BasisdatenPage->Write (); //WAL-130
	//LAC-133 A
	PersName = "";
	CString Line = GetCommandLine ();
	char * pLine = NULL;
	char * pLine2 = NULL;
	CString Msg;

	pLine = strstr ( Line.GetBuffer (),".exe");

	if (pLine != NULL)
	{
		if (strlen(pLine) > 4)   strncpy (pLine,"    ",4);
	}
	// jetzt noch die beiden Hochkomma entfernen
	if (pLine != NULL)
	{
		pLine = strstr (pLine,"\"");
	}
    if (pLine != NULL)
	{
		if (strlen(pLine) > 0) strncpy (pLine," ",1);
	     pLine2 = strstr (pLine,"\"");
	}
    if (pLine2 != NULL)
	{
		if (strlen(pLine) > 0) strncpy (pLine2," ",1);
	}

	if (pLine != NULL) PersName.Format (_T("%s"), pLine);
	if (PersName.Trim () != "")
	{
		if (PersName.GetLength() < 9)
		{
			_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
			if (Sys_ben.dbreadfirstpers_nam () == 0)
			{
//				AfxMessageBox (PersName.GetBuffer ()); //testtest

				if (Sys_ben.sys_ben.berecht > 0) return ; 
			}
		}
	}
	//LAC-133 E


	POSITION pos;
	pos = GetFirstDocTemplatePosition ();
	CDocTemplate *dt = GetNextDocTemplate (pos);
	dt = GetNextDocTemplate (pos);
	dt = GetNextDocTemplate (pos);
	if (dt != NULL)
	{
		dt = GetNextDocTemplate (pos);
	}
	if (dt != NULL)
	{
		if (dt == StdPrInstance)
		{
            CMainFrame *MainFrame =  (CMainFrame *) m_pMainWnd;
			if (MainFrame->StdPrWnd != NULL)
			{
				MainFrame->MDIActivate (MainFrame->StdPrWnd);
				return;
			}
		}

		StdPrInstance = dt;
		CDocument *dc = dt->CreateNewDocument ();
		if (dc !=NULL)
		{
//			dt->SetDefaultTitle (dc);
			dc->SetTitle (_T("Standardpreise"));
		}
		CFrameWnd *Frame = dt->CreateNewFrame (dc, NULL);
		if (Frame != NULL)
		{
			dt->InitialUpdateFrame (Frame, NULL, TRUE);
		}
	}
}


void CArtikelApp::OnEnChangeA()
{
}


void CArtikelApp::OnSprache()
{
	CProcess p;
	char text[60];
	CString CCommand;
	strcpy (text,EnterSprache.GetBuffer());
	if (a != 0)
	{
		CCommand.Format(_T("javaw -jar %s %.0lf"), text, a);
	}
	else
	{
		CCommand.Format(_T("javaw -jar %s "), text);
	}
	p.SetCommand (CCommand);
	HANDLE Pid = p.Start ();
}

void CArtikelApp::OnEkpr()
{
	//LAC-133 A
//	CUtil::GetPersName (PersName);F�hrt zum Absturz !!! 
	PersName = "";
	CString Line = GetCommandLine ();
	//Token f�hrt zum, Absturz !!! 
	//CString Msg;
	//Msg.Format (_T("Line = %s"), Line.GetBuffer ());
	//CSToken t (Line, " ");
	
	//if (t.GetAnzToken () > 1)
	//{
	//	PersName = t.GetToken (1);
	//}
//	dann m�ssen wir uns anders behelfen
	char * pLine = NULL;
	char * pLine2 = NULL;
	CString Msg;

	pLine = strstr ( Line.GetBuffer (),".exe");

	if (pLine != NULL)
	{
		if (strlen(pLine) > 4)   strncpy (pLine,"    ",4);
	}
	// jetzt noch die beiden Hochkomma entfernen
	if (pLine != NULL)
	{
		pLine = strstr (pLine,"\"");
	}
    if (pLine != NULL)
	{
		if (strlen(pLine) > 0) strncpy (pLine," ",1);
	     pLine2 = strstr (pLine,"\"");
	}
    if (pLine2 != NULL)
	{
		if (strlen(pLine) > 0) strncpy (pLine2," ",1);
	}

	if (pLine != NULL) PersName.Format (_T("%s"), pLine);
	if (PersName.Trim () != "")
	{
		if (PersName.GetLength() < 9)
		{
			_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
			if (Sys_ben.dbreadfirstpers_nam () == 0)
			{
//				AfxMessageBox (PersName.GetBuffer ()); //testtest

				if (Sys_ben.sys_ben.berecht > 0) return ; 
			}
		}
	}
	//LAC-133 E


	CProcess p;
	p.SetCommand (EnterEk);
	HANDLE Pid = p.Start ();
}
