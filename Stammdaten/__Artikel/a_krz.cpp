#include "stdafx.h"
#include "a_krz.h"

struct A_KRZ a_krz, a_krz_null, a_krz_def;

void A_KRZ_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((long *)   &a_krz.a_krz,  SQLLONG, 0);
    sqlout ((double *) &a_krz.a,SQLDOUBLE,0);
    sqlout ((double *) &a_krz.a_herk,SQLDOUBLE,0);
    sqlout ((long *) &a_krz.a_krz,SQLLONG,0);
    sqlout ((long *) &a_krz.anz_a,SQLLONG,0);
    sqlout ((short *) &a_krz.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) a_krz.herk_kz,SQLCHAR,2);
    sqlout ((short *) &a_krz.hwg,SQLSHORT,0);
    sqlout ((TCHAR *) a_krz.modif,SQLCHAR,2);
    sqlout ((TCHAR *) a_krz.verk_art,SQLCHAR,2);
    sqlout ((short *) &a_krz.wg,SQLSHORT,0);
            cursor = sqlcursor (_T("select a_krz.a,  ")
_T("a_krz.a_herk,  a_krz.a_krz,  a_krz.anz_a,  a_krz.delstatus,  ")
_T("a_krz.herk_kz,  a_krz.hwg,  a_krz.modif,  a_krz.verk_art,  a_krz.wg from a_krz ")

#line 12 "A_krz.rpp"
                                  _T("where a_krz = ?"));
    sqlin ((double *) &a_krz.a,SQLDOUBLE,0);
    sqlin ((double *) &a_krz.a_herk,SQLDOUBLE,0);
    sqlin ((long *) &a_krz.a_krz,SQLLONG,0);
    sqlin ((long *) &a_krz.anz_a,SQLLONG,0);
    sqlin ((short *) &a_krz.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) a_krz.herk_kz,SQLCHAR,2);
    sqlin ((short *) &a_krz.hwg,SQLSHORT,0);
    sqlin ((TCHAR *) a_krz.modif,SQLCHAR,2);
    sqlin ((TCHAR *) a_krz.verk_art,SQLCHAR,2);
    sqlin ((short *) &a_krz.wg,SQLSHORT,0);
            sqltext = _T("update a_krz set a_krz.a = ?,  ")
_T("a_krz.a_herk = ?,  a_krz.a_krz = ?,  a_krz.anz_a = ?,  ")
_T("a_krz.delstatus = ?,  a_krz.herk_kz = ?,  a_krz.hwg = ?,  ")
_T("a_krz.modif = ?,  a_krz.verk_art = ?,  a_krz.wg = ? ")

#line 14 "A_krz.rpp"
                                  _T("where a_krz = ?");
            sqlin ((long *)   &a_krz.a_krz,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &a_krz.a_krz,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_krz ")
                                  _T("where a_krz = ? for update"));
            sqlin ((long *)   &a_krz.a,  SQLLONG, 0);
            test_lock_cursor = sqlcursor (_T("update a_krz ")
                                  _T("set delstatus = 0 where a_krz = ? and delstatus = 0 for update"));
            sqlin ((long *)   &a_krz.a_krz,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from a_krz ")
                                  _T("where a_krz = ?"));
    sqlin ((double *) &a_krz.a,SQLDOUBLE,0);
    sqlin ((double *) &a_krz.a_herk,SQLDOUBLE,0);
    sqlin ((long *) &a_krz.a_krz,SQLLONG,0);
    sqlin ((long *) &a_krz.anz_a,SQLLONG,0);
    sqlin ((short *) &a_krz.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) a_krz.herk_kz,SQLCHAR,2);
    sqlin ((short *) &a_krz.hwg,SQLSHORT,0);
    sqlin ((TCHAR *) a_krz.modif,SQLCHAR,2);
    sqlin ((TCHAR *) a_krz.verk_art,SQLCHAR,2);
    sqlin ((short *) &a_krz.wg,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into a_krz (a,  ")
_T("a_herk,  a_krz,  anz_a,  delstatus,  herk_kz,  hwg,  modif,  verk_art,  wg) ")

#line 28 "A_krz.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?)"));

#line 30 "A_krz.rpp"

            sqlin ((double *)   &a_krz.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_krz.a,SQLDOUBLE,0);
    sqlout ((double *) &a_krz.a_herk,SQLDOUBLE,0);
    sqlout ((long *) &a_krz.a_krz,SQLLONG,0);
    sqlout ((long *) &a_krz.anz_a,SQLLONG,0);
    sqlout ((short *) &a_krz.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) a_krz.herk_kz,SQLCHAR,2);
    sqlout ((short *) &a_krz.hwg,SQLSHORT,0);
    sqlout ((TCHAR *) a_krz.modif,SQLCHAR,2);
    sqlout ((TCHAR *) a_krz.verk_art,SQLCHAR,2);
    sqlout ((short *) &a_krz.wg,SQLSHORT,0);
            a_cursor = sqlcursor (_T("select a_krz.a,  ")
_T("a_krz.a_herk,  a_krz.a_krz,  a_krz.anz_a,  a_krz.delstatus,  ")
_T("a_krz.herk_kz,  a_krz.hwg,  a_krz.modif,  a_krz.verk_art,  a_krz.wg from a_krz ")

#line 33 "A_krz.rpp"
                                  _T("where a = ?"));
            sqlin ((double *)   &a_krz.a_herk,  SQLDOUBLE, 0);
    sqlout ((double *) &a_krz.a,SQLDOUBLE,0);
    sqlout ((double *) &a_krz.a_herk,SQLDOUBLE,0);
    sqlout ((long *) &a_krz.a_krz,SQLLONG,0);
    sqlout ((long *) &a_krz.anz_a,SQLLONG,0);
    sqlout ((short *) &a_krz.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) a_krz.herk_kz,SQLCHAR,2);
    sqlout ((short *) &a_krz.hwg,SQLSHORT,0);
    sqlout ((TCHAR *) a_krz.modif,SQLCHAR,2);
    sqlout ((TCHAR *) a_krz.verk_art,SQLCHAR,2);
    sqlout ((short *) &a_krz.wg,SQLSHORT,0);
            a_herk_cursor = sqlcursor (_T("select a_krz.a,  ")
_T("a_krz.a_herk,  a_krz.a_krz,  a_krz.anz_a,  a_krz.delstatus,  ")
_T("a_krz.herk_kz,  a_krz.hwg,  a_krz.modif,  a_krz.verk_art,  a_krz.wg from a_krz ")

#line 36 "A_krz.rpp"
                                  _T("where a_herk = ?"));
}

int A_KRZ_CLASS::dbreadafirst ()
{
	if (a_cursor == -1)
	{
		prepare ();
        }
	if (a_cursor == -1) return -1;
        int dsqlstatus = sqlopen (a_cursor);
        if (dsqlstatus == 0)
	{ 
		return sqlfetch (a_cursor); 	   	
        }
        return dsqlstatus;
}

int A_KRZ_CLASS::dbreada ()
{
	if (a_cursor == -1) return -1;
	return sqlfetch (a_cursor); 	   	
}

int A_KRZ_CLASS::dbreada_herkfirst ()
{
	if (a_herk_cursor == -1)
	{
		prepare ();
        }
	if (a_herk_cursor == -1) return -1;
        int dsqlstatus = sqlopen (a_herk_cursor);
        if (dsqlstatus == 0)
	{ 
		return sqlfetch (a_herk_cursor); 	   	
        }
        return dsqlstatus;
}

int A_KRZ_CLASS::dbreada_herk ()
{
	if (a_herk_cursor == -1) return -1;
	return sqlfetch (a_herk_cursor); 	   	
}
