#pragma once
#include <vector>
#include "UpdateHandler.h"
#include "DatabaseCore.h"
#include "Choicex.h"

using namespace std;

class CWindowHandler
{
public:
	enum SPLIT_DIR
	{
		Horizontal = 0,
		Vertical = 1,
		NoSplit = 2,
	};

	enum VIEWTYPE
	{
		MainView = 0,
		Choice  = 1,
	};

	enum ORIENTATION
	{
		Left = 0,
		Right = 1,
	};
private:
     static CWindowHandler *Instance;
	 vector<CUpdateHandler *> UpdateTable;
	 CSplitterWnd *m_Splitter;
	 BOOL m_DockChoice;
	 CChoiceX *m_Choice;
	 CWnd *m_Base;
	 BOOL m_StartWithDock;
	 BOOL m_ChoiceDockable;
	 BOOL m_ChoiceIsCreated;
	 CWnd *m_MainFrame;
	 CPropertySheet *m_Sheet;
	 BOOL m_CloseFromMemory;
	 COLORREF m_DlgBkColor;
	 HBRUSH m_DlgBrush;
	 SPLIT_DIR m_SplitDir;

protected:
	CWindowHandler(void);
	~CWindowHandler(void);
public:
     static CWindowHandler *GetInstance ();
     static void DestroyInstance ();

	 void SetSheet (CPropertySheet *sheet)
	 {
		 m_Sheet = sheet;
	 }
     
	 CPropertySheet *GetSheet ()
	 {
		 return m_Sheet;
	 }

	 SPLIT_DIR GetSplitDir ()
	 {
		 return m_SplitDir;
	 }

     void set_DockChoice (BOOL dockChoice)
	 {
		 m_DockChoice = dockChoice;
	 }
	 
	 BOOL DockChoice ()
	 {
		 return m_DockChoice;
	 }

	 void SetDlgBkColor (COLORREF DlgBkColor)
	 {
		 m_DlgBkColor = DlgBkColor;
	 }

	 COLORREF GetDlgBkColor ()
	 {
		 return m_DlgBkColor;
	 }

	 void SetDlgBrush (HBRUSH DlgBrush)
	 {
		 m_DlgBrush = DlgBrush;
	 }

	 HBRUSH GetDlgBrush ()
	 {
		 return m_DlgBrush;
	 }

	 BOOL IsCloseFromMemory ()
	 {
		 return m_CloseFromMemory;
	 }

	 void SetMainFrame (CWnd* MainFrame)
	 {
		 m_MainFrame = MainFrame;
	 }

	 void SetSplitter (CSplitterWnd *splitter)
	 {
		 m_Splitter = splitter;
	 }

	 void SetDockChoice (BOOL DockChoice)
	 {
		 m_ChoiceDockable = DockChoice;
	 }

	 void StartWithDock (BOOL StartWithDock)
	 {
		 m_StartWithDock = StartWithDock;
		 if (m_StartWithDock && m_Choice != NULL)
		 {
			 if (!m_DockChoice && !IsWindowVisible (m_Choice->m_hWnd))
			 {
				 delete m_Choice;
				 m_Choice = NULL;
			 }
		 }
	 }

	 BOOL GetDockChoice ()
	 {
		 return m_DockChoice;
	 }

	 void SetBase (CWnd* base)
	 {
		 m_Base = base;
		 if (m_Choice != NULL)
		 {
			 m_Choice->SetParent (m_Base);
		 }
	 }

	 CWnd * GetBase ()
	 {
		 return m_Base;
	 }

	 void SetChoice (CChoiceX *Choice)
	 {
		 m_Choice = Choice;
	 }

	 CChoiceX *GetActiveChoice ()
	 {
		 return m_Choice;
	 }

	 void RegisterUpdate (CUpdateHandler *UpdateHandler);
	 void UnregisterUpdate (CUpdateHandler *UpdateHandler);
	 void Update ();
	 void UpdateWrite ();
	 void OnChoice (CChoiceX *Choice, CWnd *Parent);
	 void OnDeleteChoice ();
	 void OnBack ();
	 void OnWrite ();
	 void OnDelete ();
	 BOOL IsDockPlace (int x, int y);
	 BOOL IsVerticaleDockPlace (int x, int y);
	 void DestroyFrame ();
	 void SplitChoice ();
	 void DeleteChoice ();
	 void SplitBottomChoice ();
	 void DeleteBottomChoice ();
	 void DeleteActiveChoice ();
	 CChoiceX *GetChoice ();
	 void NewSplit ();
};

#define SplitDir GetSplitDir
#define WINDOWHANDLER CWindowHandler::GetInstance ()
