// 16400View.cpp : Implementierung der Klasse CDistributorView
//

#include "stdafx.h"
#include "16400.h"

#include "16400Doc.h"
#include "DistributorView.h"
#include "WindowHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define CXPLUS 33
#define CYPLUS 45


// CDistributorView

IMPLEMENT_DYNCREATE(CDistributorView, CFormView)

BEGIN_MESSAGE_MAP(CDistributorView, CFormView)
	ON_WM_SIZE ()
//	ON_WM_CTLCOLOR ()
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_DELETE, OnUpdateDelete)
	ON_COMMAND(ID_BACK, OnBack)
END_MESSAGE_MAP()

// CDistributorView-Erstellung/Zerst�rung

CDistributorView::CDistributorView()
	: CFormView(CDistributorView::IDD)
{
	// TODO: Hier Code zur Konstruktion einf�gen
	m_Sheet = NULL;
	m_BasePage = NULL;
	m_BasePage2 = NULL;
	StartSize.cx = 0;
	StartSize.cy = 0;
}

CDistributorView::~CDistributorView()
{
	if (m_Sheet != NULL)
	{
		delete m_Sheet;
	}
	if (m_BasePage != NULL)
	{
		delete m_BasePage;
	}
	if (m_BasePage2 != NULL)
	{
		delete m_BasePage2;
	}
}

void CDistributorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL CDistributorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CFormView::PreCreateWindow(cs);
}

void CDistributorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CDocument *dc = GetDocument ();
	if (dc != NULL)
	{
		dc->SetTitle (_T("Stammdaten"));
	}

//	m_Sheet = new CDistributorSheet(_T("Adresse/Basisdaten"));
	m_Sheet = new CDistributorSheet();
    m_Sheet->m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
	m_BasePage = new CBasePage;
	m_Sheet->AddPage (m_BasePage);
	m_BasePage2 = new CBasePage2;
	m_Sheet->AddPage (m_BasePage2);

    m_Sheet->m_psh.hwndParent = m_hWnd;
//    m_Sheet->m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
    m_Sheet->m_psh.pszCaption = _T("Simple");
    m_Sheet->m_psh.nStartPage = 0;

    m_Sheet->m_psh.dwSize = sizeof (m_Sheet->m_psh);
	m_Sheet->Create (this, WS_CHILD | WS_VISIBLE);
    CTabCtrl* pTab = m_Sheet->GetTabControl ();
	DWORD style = ::GetWindowLong (pTab->m_hWnd, GWL_STYLE);
	style |= TCS_BUTTONS | TCS_FLATBUTTONS;
//    ::SetWindowLong (pTab->m_hWnd, GWL_STYLE, style); 
	WINDOWHANDLER->SetSheet (m_Sheet);
}

HBRUSH CDistributorView::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	DlgBkColor = WINDOWHANDLER->GetDlgBkColor ();
	DlgBrush   = WINDOWHANDLER->GetDlgBrush ();

	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			WINDOWHANDLER->SetDlgBkColor (DlgBkColor);
			WINDOWHANDLER->SetDlgBrush (DlgBrush);
			return DlgBrush;
	}
	return CFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CDistributorView::OnSize(UINT nType, int cx, int cy)
{
//	int tabWidth;
//	int tabHeight;
//	int tabCount;
	CRect tabRect;
	CRect itemRect;

	if (m_Sheet != NULL && StartSize.cx == 0)
	{

		m_Sheet->SetWindowPos (NULL, 0, 0, cx, cy, SWP_NOZORDER);
//  	    m_BasePage->SetWindowPos (NULL, 0, 20, cx, cy, SWP_NOZORDER);
//  	    m_BasePage2->SetWindowPos (NULL, 0, 20, cx, cy, SWP_NOZORDER);
/*
	    CTabCtrl* pTab = m_Sheet->GetTabControl ();
		pTab->GetClientRect (&tabRect);
		tabCount = pTab->GetItemCount ();
		tabWidth = 0;
		for (int i = 0; i < tabCount; i ++)
		{
			pTab->GetItemRect (i, &itemRect);
			tabWidth += itemRect.right + 7 - itemRect.left;
			tabHeight = itemRect.bottom;
		}
		pTab->SetWindowPos (NULL, 0, 0, tabWidth, tabHeight, SWP_NOZORDER);
*/
		StartSize.cx = cx;
		StartSize.cy = cy;
	}
	SetScrollSizes (MM_TEXT, StartSize);

}


// CDistributorView-Diagnose

#ifdef _DEBUG
void CDistributorView::AssertValid() const
{
	CFormView::AssertValid();
}

void CDistributorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CMy16400Doc* CDistributorView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMy16400Doc)));
	return (CMy16400Doc*)m_pDocument;
}
#endif //_DEBUG


// CDistributorView-Meldungshandler

void CDistributorView::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     m_Sheet->Write ();
}

void CDistributorView::OnUpdateFileSave(CCmdUI *pCmdUI)
{
	BOOL enable = FALSE;
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	if (m_Sheet->GetPageCount () > 0)
	{
	   CBasePage *Page = (CBasePage *) m_Sheet->GetPage (0);
	   if (Page != NULL && Page->CanWrite ())
	   {
		   enable = TRUE;
	   }
	}
    pCmdUI->Enable (enable);
}

void CDistributorView::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     m_Sheet->DeleteAll ();
}

void CDistributorView::OnUpdateDelete(CCmdUI *pCmdUI)
{
	BOOL enable = FALSE;
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	if (m_Sheet->GetPageCount () > 0)
	{
	   CBasePage *Page = (CBasePage *) m_Sheet->GetPage (0);
	   if (Page != NULL && Page->CanWrite ())
	   {
		   enable = TRUE;
	   }
	}
    pCmdUI->Enable (enable);
}

void CDistributorView::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     if (!m_Sheet->StepBack ())
	 {
		 WINDOWHANDLER->DestroyFrame ();
	 }
}

