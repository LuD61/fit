#pragma once

#include <vector>

class CDistributorCore
{
private:
	static CDistributorCore *Instance; 
	class FilKeys 
	{
	public:
		short m_Mdn;
		short m_Fil;
		FilKeys (short mdn, short fil) 
		{
			m_Mdn  = mdn;
			m_Fil  = fil;
		}
		void Restore (short *mdn, short *fil)
		{
			*mdn = m_Mdn;
			*fil = m_Fil;
		}
	};
	class LiefKeys 
	{
	public:
		short m_Mdn;
		short m_Fil;
		CString m_Lief;
		LiefKeys (short mdn, short fil, TCHAR *lief) 
		{
			m_Mdn  = mdn;
			m_Fil  = fil;
			m_Lief = lief;
		}
		void Restore (short *mdn, short *fil, TCHAR *lief)
		{
			*mdn = m_Mdn;
			*fil = m_Fil;
			_tcscpy (lief, m_Lief.GetBuffer ());
		}
	};

	short m_Mdn;
	long  m_Adr;
	FilKeys *m_FilKeys;
	LiefKeys *m_LiefKeys;

protected:
	CDistributorCore(void);
	~CDistributorCore(void);
public:
	static CDistributorCore *GetInstance ();
	static void DestroyInstance ();
	void Opendbase ();
	BOOL ReadMdn ();
	BOOL ReadFil ();
	BOOL ReadLief ();
	void SaveLief ();
    void RestoreLief ();
	BOOL WriteLief ();
	BOOL DeleteLief ();
	BOOL ReadAdr ();
	BOOL ReadPtabn ();
	BOOL ReadZahlKond ();
	BOOL WriteZahlKond ();
	void FillPtabCombo (std::vector<CString *> & ComboValues, LPTSTR Item);
};

#define CORE CDistributorCore::GetInstance ()