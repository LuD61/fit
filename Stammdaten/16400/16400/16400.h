// 16400.h : Hauptheaderdatei f�r die 16400-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"       // Hauptsymbole


// CMy16400App:
// Siehe 16400.cpp f�r die Implementierung dieser Klasse
//

class CMy16400App : public CWinApp
{
public:
	CMy16400App();
	~CMy16400App();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CMy16400App theApp;