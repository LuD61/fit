#ifndef _DB_CLASSDEF
#define _DB_CLASSDEF
#include "OdbcClass.h"
#ifdef INFORMIX
#include "mo_curso.h"
#endif
#include <string.h>

#define NEXT 1
#define PRIOR 2
#define PREVIOUS 2
#define FIRST 3
#define LAST 4
#define CURRENT 5
#define RELATIV 7
#define DBABSOLUTE 6

#define SQLCHAR   0
#define SQLSHORT  1
#define SQLLONG   2
#define SQLDOUBLE 3
#undef DQLDATE
#define SQLDATE 7


class DB_CLASS 
{
       public :
               short cursor;
               short test_upd_cursor;
               short test_lock_cursor;
               short upd_cursor;
               short ins_cursor;
               short del_cursor;
               short cursor_ausw;
               int   scrollpos;
			   static int SqlErrorMode;
               static short ShortNull;
               static long LongNull;
               static double DoubleNull;
			   static BOOL UseOdbc;
               static BOOL DbIsOpened;

               DB_CLASS ()
               {
                         cursor          = -1;
                         test_upd_cursor = -1;
                         upd_cursor      = -1;
                         ins_cursor      = -1;
                         del_cursor      = -1;
                         scrollpos       = 1;
               }

			   ~DB_CLASS ()
			   {
#ifdef INFORMIX
				   if (cursor > -1) close_sql (cursor);
				   if (test_upd_cursor > -1) close_sql (test_upd_cursor);
				   if (upd_cursor > -1) close_sql (upd_cursor);
				   if (ins_cursor > -1) close_sql (ins_cursor);
				   if (del_cursor > -1) close_sql (del_cursor);
#endif
			   }


			   void opendbase (LPTSTR);
			   void closedbase (LPTSTR);
			   int beginwork ();
			   int commitwork ();
			   int rollbackwork ();
			   virtual void clone (void *object) {}
			   virtual void CopyData (void *object) {}
			   virtual BOOL Copy () {return TRUE;}
			   virtual BOOL Paste (){return TRUE;}
               virtual void prepare (void) 
               {
               }
               virtual int dbreadfirst (void);
               virtual int dbread (void);
               virtual int dbupdate (void);
               virtual int dblock (void);
               virtual int dbdelete (void);
               virtual void dbclose (void);

               int dbmove (int);
               int dbmove (int, int);
               int dbcanmove (int);
               int dbcanmove (int, int);
			   void cleanin ();
			   void cleanout ();
               int sqlin  (void *, int, int);
               int sqlout (void *, int, int);
               int sqlcursor (LPTSTR);
               int sqlclose (int);
               int sqlopen (int);
               int sqlfetch (int);
               int sqlexecute (int);
               int sqlcomm (LPTSTR);
               int PrintSyntaxError ();
               int PrintColError ();
               void _SQLF (LPTSTR, long);
			   static int PrepareTables (char *Table);
			   static int PrepareColumns (char *Table, char *Column);

			   static int GetColLength ();
			   static LPSTR GetColName (LPSTR name);
			   static int GetColType ();

			   static int GetColLength (int colpos, int cursor);
			   static LPSTR GetColName (int colpos, int cursor, LPSTR name);
			   static int GetColType (int position, int cursor);
               static void ToDbDate (CString&, void *);
               static void ToDbDate (LPSTR, void *);
               static void FromDbDate (CString&, void *);
               static void FromDbDate (LPSTR, void *);
               static void Sysdate (CString&);
               static void Sysdate (LPSTR);
			   static int CompareDate (DATE_STRUCT *,DATE_STRUCT *);
			   static long DAscToLong (LPSTR sdate);
			   static LPSTR DLongToAsc (long ldate, LPSTR sdate);
			   static void SetCSqlDebug (BOOL mode);

			   static BOOL IsShortnull (short value);
			   static BOOL IsLongnull (long value);
			   static BOOL IsDoublenull (double value);

			   static short *GetShortnull ();
			   static long *GetLongnull ();
			   static double *GetDoublenull ();

};

#endif