#include <odbcinst.h>
#ifndef _KST_DEF
#define _KST_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct KST {
   short          mdn;
   long           kst;
   char           kst_bz1[25];
   char           kst_bz2[25];
};
extern struct KST kst, kst_null;

class KST_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               KST kst;  
               KST_CLASS () : DB_CLASS ()
               {
               }
};
#endif
