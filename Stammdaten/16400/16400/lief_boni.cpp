#include "stdafx.h"
#include "lief_boni.h"

struct LIEF_BONI lief_boni, lief_boni_null, lief_boni_def;

void LIEF_BONI_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &lief_boni.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_boni.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_boni.lief,   SQLCHAR, sizeof (lief_boni.lief));
            sqlin ((long *)   &lief_boni.lfd,   SQLLONG, 0);
    sqlout ((double *) &lief_boni.bonus_proz,SQLDOUBLE,0);
    sqlout ((double *) &lief_boni.bonus_grenz,SQLDOUBLE,0);
    sqlout ((double *) &lief_boni.bonus_vj,SQLDOUBLE,0);
    sqlout ((short *) &lief_boni.fil,SQLSHORT,0);
    sqlout ((long *) &lief_boni.lfd,SQLLONG,0);
    sqlout ((TCHAR *) lief_boni.lief,SQLCHAR,17);
    sqlout ((long *) &lief_boni.lief_s,SQLLONG,0);
    sqlout ((short *) &lief_boni.mdn,SQLSHORT,0);
            cursor = sqlcursor (_T("select ")
_T("lief_boni.bonus_proz,  lief_boni.bonus_grenz,  lief_boni.bonus_vj,  ")
_T("lief_boni.fil,  lief_boni.lfd,  lief_boni.lief,  lief_boni.lief_s,  ")
_T("lief_boni.mdn from lief_boni ")

#line 15 "lief_boni.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
    sqlin ((double *) &lief_boni.bonus_proz,SQLDOUBLE,0);
    sqlin ((double *) &lief_boni.bonus_grenz,SQLDOUBLE,0);
    sqlin ((double *) &lief_boni.bonus_vj,SQLDOUBLE,0);
    sqlin ((short *) &lief_boni.fil,SQLSHORT,0);
    sqlin ((long *) &lief_boni.lfd,SQLLONG,0);
    sqlin ((TCHAR *) lief_boni.lief,SQLCHAR,17);
    sqlin ((long *) &lief_boni.lief_s,SQLLONG,0);
    sqlin ((short *) &lief_boni.mdn,SQLSHORT,0);
            sqltext = _T("update lief_boni set ")
_T("lief_boni.bonus_proz = ?,  lief_boni.bonus_grenz = ?,  ")
_T("lief_boni.bonus_vj = ?,  lief_boni.fil = ?,  lief_boni.lfd = ?,  ")
_T("lief_boni.lief = ?,  lief_boni.lief_s = ?,  lief_boni.mdn = ? ")

#line 20 "lief_boni.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?");
            sqlin ((short *)   &lief_boni.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_boni.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_boni.lief,   SQLCHAR, sizeof (lief_boni.lief));
            sqlin ((long *)   &lief_boni.lfd,   SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &lief_boni.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_boni.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_boni.lief,   SQLCHAR, sizeof (lief_boni.lief));
            sqlin ((long *)   &lief_boni.lfd,   SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select lief from lief_boni ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
            sqlin ((short *)   &lief_boni.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_boni.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_boni.lief,   SQLCHAR, sizeof (lief_boni.lief));
            sqlin ((long *)   &lief_boni.lfd,   SQLLONG, 0);
            test_lock_cursor = sqlcursor (_T("update lief_boni set fil = fil ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
            sqlin ((short *)   &lief_boni.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_boni.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_boni.lief,   SQLCHAR, sizeof (lief_boni.lief));
            sqlin ((long *)   &lief_boni.lfd,   SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from lief_boni ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
    sqlin ((double *) &lief_boni.bonus_proz,SQLDOUBLE,0);
    sqlin ((double *) &lief_boni.bonus_grenz,SQLDOUBLE,0);
    sqlin ((double *) &lief_boni.bonus_vj,SQLDOUBLE,0);
    sqlin ((short *) &lief_boni.fil,SQLSHORT,0);
    sqlin ((long *) &lief_boni.lfd,SQLLONG,0);
    sqlin ((TCHAR *) lief_boni.lief,SQLCHAR,17);
    sqlin ((long *) &lief_boni.lief_s,SQLLONG,0);
    sqlin ((short *) &lief_boni.mdn,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into lief_boni (")
_T("bonus_proz,  bonus_grenz,  bonus_vj,  fil,  lfd,  lief,  lief_s,  mdn) ")

#line 58 "lief_boni.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?)"));

#line 60 "lief_boni.rpp"
}
