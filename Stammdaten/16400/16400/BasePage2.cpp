// BasePage2.cpp : implementation file
//

#include "stdafx.h"
#include "16400.h"
#include "kst.h"
#include "BasePage2.h"
#include "uniformfield.h"
#include "WindowHandler.h"
#include "ZahlKondDialog.h"
#include "LiefRKteDlg.h"
#include "LiefRStfDlg.h"
#include "LiefRGrDlg.h"
#include "LiefRLisDlg.h"
#include "LiefBoniDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// #define BLUE_COL

// CBasePage2 dialog

IMPLEMENT_DYNAMIC(CBasePage2, CDbPropertyPage)

CBasePage2::CBasePage2(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage(CBasePage2::IDD)
{
	ChoiceZahlKond = NULL;
	ChoiceBlz = NULL;
	WINDOWHANDLER->RegisterUpdate (this);
}

CBasePage2::~CBasePage2()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
	if (ChoiceZahlKond != NULL)
	{
		delete ChoiceZahlKond;
	}
	if (ChoiceBlz != NULL)
	{
		delete ChoiceBlz;
	}

	Kst.dbclose();
}

void CBasePage2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LFIL, m_LFil);
	DDX_Control(pDX, IDC_FIL, m_Fil);
	DDX_Control(pDX, IDC_FIL_NAME, m_FilName);
	DDX_Control(pDX, IDC_LLIEF, m_LLief);
	DDX_Control(pDX, IDC_LIEF, m_Lief);
	DDX_Control(pDX, IDC_LIEFNAME, m_LiefName);
	DDX_Control(pDX, IDC_DATABORDER, m_DataBorder);
	DDX_Control(pDX, IDC_LEG_BETIEBSNR, m_LEgBetriebsnr);
	DDX_Control(pDX, IDC_EG_BETIEBSNR, m_EgBetriebsnr);
	DDX_Control(pDX, IDC_LSTEUER_KZ, m_LSteuerKz);
	DDX_Control(pDX, IDC_STEUER_KZ, m_SteuerKz);
	DDX_Control(pDX, IDC_ORDR_KZ, m_OrdrKz);
	DDX_Control(pDX, IDC_LABR, m_LAbr);
	DDX_Control(pDX, IDC_ABR, m_Abr);
	DDX_Control(pDX, IDC_LMAHN_STU, m_LMahnStu);
	DDX_Control(pDX, IDC_MAHN_STU, m_MahnStu);
	DDX_Control(pDX, IDC_LRECH_KZ, m_LRechKz);
	DDX_Control(pDX, IDC_RECH_KZ, m_RechKz);
	DDX_Control(pDX, IDC_LRECH_TOLER, m_LRechToler);
	DDX_Control(pDX, IDC_RECH_TOLER, m_RechToler);
	DDX_Control(pDX, IDC_LWE_ERF_KZ, m_LWeErfKz);
	DDX_Control(pDX, IDC_WE_ERF_KZ, m_WeErfKz);
	DDX_Control(pDX, IDC_LWE_PR_KZ, m_LWePrKz);
	DDX_Control(pDX, IDC_WE_PR_KZ, m_WePrKz);
	DDX_Control(pDX, IDC_LINK, m_LInk);
	DDX_Control(pDX, IDC_INK, m_Ink);
	DDX_Control(pDX, IDC_ZAHL_KOND_GROUP, m_ZahlKondGroup);
	DDX_Control(pDX, IDC_LZAHLW, m_LZahlw);
	DDX_Control(pDX, IDC_ZAHLW, m_Zahlw);
	DDX_Control(pDX, IDC_LZAHL_KOND, m_LZahlKond);
	DDX_Control(pDX, IDC_ZAHL_KOND, m_ZahlKond);
	DDX_Control(pDX, IDC_ZIEL1, m_Ziel1);
	DDX_Control(pDX, IDC_ZIEL2, m_Ziel2);
	DDX_Control(pDX, IDC_ZIEL3, m_Ziel3);
	DDX_Control(pDX, IDC_TXT, m_Txt);
	DDX_Control(pDX, IDC_LZIEL1, m_LZiel1);
	DDX_Control(pDX, IDC_LZIEL2, m_LZiel2);
	DDX_Control(pDX, IDC_LZIEL3, m_LZiel3);
	DDX_Control(pDX, IDC_SKTO1, m_Skto1);
	DDX_Control(pDX, IDC_SKTO2, m_Skto2);
	DDX_Control(pDX, IDC_SKTO3, m_Skto3);
	DDX_Control(pDX, IDC_LSKTO1, m_LSkto1);
	DDX_Control(pDX, IDC_LSKTO2, m_LSkto2);
	DDX_Control(pDX, IDC_LSKTO3, m_LSkto3);
	DDX_Control(pDX, IDC_LVIE_BAS, m_LViehBas);
	DDX_Control(pDX, IDC_VIEH_BAS, m_ViehBas);
	DDX_Control(pDX, IDC_ZERTIFIKAT1, m_Zertifikat1);
	DDX_Control(pDX, IDC_LSLASH, m_LSlash);
	DDX_Control(pDX, IDC_ZERTIFIKAT2, m_Zertifikat2);
	DDX_Control(pDX, IDC_LEG_KZ, m_LEgKz);
	DDX_Control(pDX, IDC_EG_KZ, m_EgKz);
	DDX_Control(pDX, IDC_FRACHT_KZ, m_FrachtKz);
	DDX_Control(pDX, IDC_LRAB_KZ, m_LRabKz);
	DDX_Control(pDX, IDC_RAB_KZ, m_RabKz);
	DDX_Control(pDX, IDC_LRAB_ABL, m_LRabAbl);
	DDX_Control(pDX, IDC_RAB_ABL, m_RabAbl);
	DDX_Control(pDX, IDC_LLIST_NR, m_LListNr);
	DDX_Control(pDX, IDC_ILN, m_ListNr);
	DDX_Control(pDX, IDC_LMIN_ZUSCH_PROZ, m_LMinZuschProz);
	DDX_Control(pDX, IDC_MIN_ZUSCH_PROZ, m_MinZuschProz);
	DDX_Control(pDX, IDC_LLIEF_ZUSCH, m_LLiefZusch);
	DDX_Control(pDX, IDC_LIEF_ZUSCH, m_LiefZusch);
	DDX_Control(pDX, IDC_LVORKGR, m_LVorkgr);
	DDX_Control(pDX, IDC_VORKGR, m_Vorkgr);
	DDX_Control(pDX, IDC_LHDKLPARGR, m_LHhdklpargr);
	DDX_Control(pDX, IDC_HDKLPARGR, m_Hhdklpargr);
	DDX_Control(pDX, IDC_HEADBORDER, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDER2, m_DataBorder2);
	DDX_Control(pDX, IDC_BANK_GROUP, m_BankGroup);
	DDX_Control(pDX, IDC_LTXT, m_LTxt);
	DDX_Control(pDX, IDC_LBLZ, m_LBlz);
	DDX_Control(pDX, IDC_BLZ, m_Blz);
	DDX_Control(pDX, IDC_LBANK_NAM, m_LBankNam);
	DDX_Control(pDX, IDC_BANK_NAM, m_BankNam);
	DDX_Control(pDX, IDC_LIBAN, m_LIban);
	DDX_Control(pDX, IDC_LKTO, m_LKto);
	DDX_Control(pDX, IDC_KTO, m_Kto);
	DDX_Control(pDX, IDC_LUST_ID, m_LUstId);
	DDX_Control(pDX, IDC_UST_ID, m_UstId);
	DDX_Control(pDX, IDC_LSOND_KOND, m_LSondKond);
	DDX_Control(pDX, IDC_SOND_KOND, m_SondKond);
	DDX_Control(pDX, IDC_LMIN_ZUSCH, m_LMinZusch);
	DDX_Control(pDX, IDC_MIN_ZUSCH, m_MinZusch);
	DDX_Control(pDX, IDC_ENTER_ZAHL_KOND, m_EnterZahlKond);
	DDX_Control(pDX, IDC_ENTER_RAB_ABL, m_EnterRabAbl);
	DDX_Control(pDX, IDC_ENTER_BONI, m_EnterBoni);
	DDX_Control(pDX, IDC_LMWST, m_LMwst);
	DDX_Control(pDX, IDC_MWST, m_Mwst);
	DDX_Control(pDX, IDC_LSTNR, m_LSteuerNummer);
	DDX_Control(pDX, IDC_STNUMMER, m_SteuerNummer);
	DDX_Control(pDX, IDC_LKOSTEN, m_LKosten);
	DDX_Control(pDX, IDC_KOSTEN, m_Kosten);
	DDX_Control(pDX, IDC_IBAN, m_Iban);
	DDX_Control(pDX, IDC_LBIC, m_LBIC);
	DDX_Control(pDX, IDC_BIC, m_BIC);
	DDX_Control(pDX, IDC_LTAGEBIS, m_LTageBis);
	DDX_Control(pDX, IDC_TAGEBIS, m_TageBis);
}


BEGIN_MESSAGE_MAP(CBasePage2, CDialog)
	ON_WM_DESTROY ()
	ON_WM_CTLCOLOR ()
	ON_EN_KILLFOCUS(IDC_ZAHL_KOND, &CBasePage2::OnEnKillfocusZahlKond)
	ON_COMMAND(IDC_LIEFCHOICE , OnChoice)
	ON_BN_CLICKED(IDC_ZAHL_KONDCHOICE , OnZahlKondchoice)
	ON_BN_CLICKED(IDC_ENTER_ZAHL_KOND , OnEnterZahlKond)
	ON_BN_CLICKED(IDC_BLZCHOICE , OnBlzchoice)
	ON_BN_CLICKED(IDC_ENTER_RAB_ABL , OnEnterRabAbl)
	ON_BN_CLICKED(IDC_ENTER_BONI , OnEnterBoni)
	ON_CBN_SELCHANGE(IDC_STEUER_KZ, &CBasePage2::OnCbnSelchangeSteuerKz)
END_MESSAGE_MAP()


// CBasePage2 message handlers

BOOL CBasePage2::OnInitDialog ()
{
	BOOL ret = CDbPropertyPage::OnInitDialog ();


#ifdef BLUE_COL
	COLORREF BkColor (RGB (100, 180, 255));
	COLORREF DynColor = CColorButton::DynColorBlue;
	COLORREF RolloverColor = RGB (204, 204, 255);
#else
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
#endif

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	m_EnterZahlKond.SetWindowText (_T("Bearbeiten"));
//	m_EnterZahlKond.SetToolTip (_T("Zahlungskonditionen bearbeiten"));
//	m_EnterZahlKond.Tooltip.WindowOrientation = CQuikInfo::Left;
	m_EnterZahlKond.nID = IDC_ENTER_ZAHL_KOND;
	m_EnterZahlKond.Orientation = m_EnterZahlKond.Left;
	m_EnterZahlKond.TextStyle = m_EnterZahlKond.Standard;
	m_EnterZahlKond.BorderStyle = m_EnterZahlKond.Solide;
//	m_EnterZahlKond.BorderStyle = m_EnterZahlKond.Rounded;
	m_EnterZahlKond.DynamicColor = TRUE;
	m_EnterZahlKond.TextColor =RGB (0, 0, 0);
	m_EnterZahlKond.SetBkColor (BkColor);
	m_EnterZahlKond.DynColor = DynColor;
	m_EnterZahlKond.RolloverColor = RolloverColor;
	m_EnterZahlKond.LoadBitmap (IDB_ENTER);

	m_EnterRabAbl.SetWindowText (_T("Bearbeiten"));
//	m_EnterRabAbl.SetToolTip (_T("Zahlungskonditionen bearbeiten"));
//	m_EnterRabAbl.Tooltip.WindowOrientation = CQuikInfo::Left;
	m_EnterRabAbl.nID = IDC_ENTER_RAB_ABL;
	m_EnterRabAbl.Orientation = m_EnterRabAbl.Left;
	m_EnterRabAbl.TextStyle = m_EnterRabAbl.Standard;
	m_EnterRabAbl.BorderStyle = m_EnterRabAbl.Solide;
//	m_EnterRabAbl.BorderStyle = m_EnterRabAbl.Rounded;
	m_EnterRabAbl.DynamicColor = TRUE;
	m_EnterRabAbl.TextColor =RGB (0, 0, 0);
	m_EnterRabAbl.SetBkColor (BkColor);
	m_EnterRabAbl.DynColor = DynColor;
	m_EnterRabAbl.RolloverColor = RolloverColor;
	m_EnterRabAbl.LoadBitmap (IDB_ENTER);

	m_EnterBoni.SetWindowText (_T("Bonus"));
//	m_EnterBoni.SetToolTip (_T("Zahlungskonditionen bearbeiten"));
//	m_EnterBoni.Tooltip.WindowOrientation = CQuikInfo::Left;
	m_EnterBoni.nID = IDC_ENTER_BONI;
	m_EnterBoni.Orientation = m_EnterBoni.Left;
	m_EnterBoni.TextStyle = m_EnterBoni.Standard;
	m_EnterBoni.BorderStyle = m_EnterBoni.Solide;
//	m_EnterBoni.BorderStyle = m_EnterBoni.Rounded;
	m_EnterBoni.DynamicColor = TRUE;
	m_EnterBoni.TextColor =RGB (0, 0, 0);
	m_EnterBoni.SetBkColor (BkColor);
	m_EnterBoni.DynColor = DynColor;
	m_EnterBoni.RolloverColor = RolloverColor;
	m_EnterBoni.LoadBitmap (IDB_ENTER);

	Form.Add (new CFormField (&m_Mdn,EDIT,              (short *) &TMDN.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT,       (char *)  &TMDN_ADR.adr_krz, VCHAR, sizeof (TMDN_ADR.adr_krz) - 1));
	Form.Add (new CFormField (&m_Fil,EDIT,              (short *) &TFIL.fil, VSHORT));
    Form.Add (new CUniFormField (&m_FilName,EDIT,       (char *)  &TFIL_ADR.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Lief,EDIT,             (char *)  TLIEF.lief, VCHAR));
	Form.Add (new CUniFormField (&m_LiefName,EDIT,      (char *)  TLIEF_ADR.adr_krz, VCHAR, sizeof (TLIEF_ADR.adr_krz) - 1));
	Form.Add (new CUniFormField (&m_LiefName,EDIT,      (char *)  TLIEF_ADR.adr_krz, VCHAR, sizeof (TLIEF_ADR.adr_krz) - 1));

	Form.Add (new CUniFormField (&m_EgBetriebsnr,EDIT,  (char *)  TLIEF.eg_betriebsnr, VCHAR, sizeof (TLIEF.eg_betriebsnr) - 1));
	Form.Add (new CUniFormField (&m_SteuerNummer,EDIT,  (char *)  TLIEF.steuer_nr, VCHAR));
	Form.Add (new CUniFormField (&m_SteuerKz,COMBOBOX,  (char *)  TLIEF.steuer_kz, VCHAR));
	Form.Add (new CFormField (&m_OrdrKz,CHECKBOX,       (char *)  TLIEF.ordr_kz, VCHAR));
	Form.Add (new CUniFormField (&m_Abr,COMBOBOX,       (char *)  TLIEF.abr, VCHAR));
	Form.Add (new CFormField (&m_MahnStu,EDIT,          (short *) &TLIEF.mahn_stu, VSHORT, 4));
	Form.Add (new CUniFormField (&m_RechKz,COMBOBOX,    (char *)  TLIEF.rech_kz, VCHAR));
	Form.Add (new CFormField (&m_RechToler,EDIT,        (double *) &TLIEF.rech_toler, VDOUBLE, 8, 4));
	Form.Add (new CUniFormField (&m_WeErfKz,COMBOBOX,   (char *)  TLIEF.we_erf_kz, VCHAR));
	Form.Add (new CUniFormField (&m_WePrKz,COMBOBOX,    (char *)  TLIEF.we_pr_kz, VCHAR));
	Form.Add (new CUniFormField (&m_Ink,EDIT,           (char *)  TLIEF.ink, VCHAR, sizeof (TLIEF.ink) - 1));
	Form.Add (new CUniFormField (&m_Zahlw,COMBOBOX,     (char *)  TLIEF.zahlw, VCHAR));
	Form.Add (new CFormField (&m_ZahlKond,EDIT,         (short *) &TLIEF.zahl_kond, VSHORT, 4));
	Form.Add (new CFormField (&m_Skto1,EDIT,            (double *)&TZAHL_KOND.skto1, VDOUBLE, 3,1));
	Form.Add (new CFormField (&m_Skto2,EDIT,            (double *)&TZAHL_KOND.skto2, VDOUBLE, 3,1));
	Form.Add (new CFormField (&m_Skto3,EDIT,            (double *)&TZAHL_KOND.skto3, VDOUBLE, 3,1));
	Form.Add (new CFormField (&m_Ziel1,EDIT,            (short  *)&TZAHL_KOND.ziel1, VSHORT, 2));
	Form.Add (new CFormField (&m_Ziel2,EDIT,            (short  *)&TZAHL_KOND.ziel2, VSHORT, 2));
	Form.Add (new CFormField (&m_Ziel3,EDIT,            (short  *)&TZAHL_KOND.ziel3, VSHORT, 2));
	Form.Add (new CUniFormField (&m_Txt,EDIT,           (char *)  TZAHL_KOND.txt, VCHAR, sizeof (TZAHL_KOND.txt) - 1));
	Form.Add (new CUniFormField (&m_ViehBas,EDIT,       (char *)  TLIEF.vieh_bas, VCHAR, sizeof (TLIEF.vieh_bas) - 1));
	Form.Add (new CFormField (&m_Zertifikat1,EDIT,      (short *) &TLIEF.zertifikat1, VSHORT, 2));
	Form.Add (new CFormField (&m_Zertifikat2,EDIT,      (short *) &TLIEF.zertifikat2, VSHORT, 2));
	Form.Add (new CUniFormField (&m_EgKz,COMBOBOX,      (short *) &TLIEF.eg_kz, VSHORT));
	Form.Add (new CFormField (&m_FrachtKz,CHECKBOX,     (char *)  TLIEF.fracht_kz, VCHAR));
	Form.Add (new CUniFormField (&m_RabKz,COMBOBOX,     (char *)  TLIEF.rab_kz, VCHAR));
	Form.Add (new CUniFormField (&m_RabAbl,COMBOBOX,    (char *)  TLIEF.rab_abl, VCHAR));
	Form.Add (new CFormField (&m_ListNr,EDIT,           (short *) &TLIEF.lst_nr, VSHORT, 4));
	Form.Add (new CFormField (&m_MinZuschProz,EDIT,     (double *) &TLIEF.min_zusch_proz, VDOUBLE, 5, 2));
	Form.Add (new CFormField (&m_MinZusch,EDIT,         (double *) &TLIEF.min_zusch, VDOUBLE, 8, 2));
	Form.Add (new CFormField (&m_LiefZusch,EDIT,        (double *) &TLIEF.lief_zusch, VDOUBLE, 8, 2));
	Form.Add (new CFormField (&m_Vorkgr,EDIT,           (long *)   &TLIEF.vorkgr, VLONG, 8));
	Form.Add (new CFormField (&m_Hhdklpargr,EDIT,       (short *)  &TLIEF.hdklpargr, VSHORT, 4));
	Form.Add (new CFormField (&m_Blz,EDIT,              (long *)   &TLIEF.blz, VLONG, 8));
	Form.Add (new CFormField (&m_Kto,EDIT,              (char *)  TLIEF.kto, VCHAR, sizeof (TLIEF.kto) - 1));
	Form.Add (new CUniFormField (&m_BankNam,EDIT,       (char *)  TLIEF.bank_nam, VCHAR, sizeof (TLIEF.bank_nam) - 1));
	Form.Add (new CUniFormField (&m_Iban,EDIT,         (char *)  TLIEF.iban, VCHAR, sizeof (TLIEF.iban) - 1));
	Form.Add (new CUniFormField (&m_BIC,EDIT,         (char *)  TLIEF.swift, VCHAR, sizeof (TLIEF.swift) - 1));
	Form.Add (new CUniFormField (&m_Kto,EDIT,           (char *)  TLIEF.kto, VCHAR, sizeof (TLIEF.kto) - 1));
	Form.Add (new CUniFormField (&m_UstId,EDIT,         (char *)  TLIEF.ust_id, VCHAR, sizeof (TLIEF.ust_id) - 1));
	Form.Add (new CFormField (&m_SondKond,EDIT,         (long *)  &TLIEF.son_kond, VLONG, 8));
	Form.Add (new CFormField (&m_Mwst,COMBOBOX,				(short *) &TLIEF.mwst, VSHORT, 4));
	Form.Add (new CFormField (&m_Kosten,COMBOBOX,        (long *)  &TLIEF.kst , VLONG));
	Form.Add (new CFormField (&m_TageBis,EDIT,       (short *)  &TLIEF.tage_bis, VSHORT, 4));

	FillPtabCombo (&m_SteuerKz, _T("steuer_kz"));
	FillPtabCombo (&m_Abr, _T("abr"));
	FillPtabCombo (&m_RechKz, _T("rech_kz"));
	FillPtabCombo (&m_WePrKz, _T("we_pr_kz"));
	FillPtabCombo (&m_WeErfKz, _T("we_erf_kz"));
	FillPtabCombo (&m_Zahlw,  _T("zahlw"));
	FillPtabCombo (&m_EgKz,   _T("eg_kz"));
	FillPtabCombo (&m_RabKz,  _T("rab_kz"));
	FillPtabCombo (&m_RabAbl, _T("rab_abl"));
	FillPtabCombo (&m_Mwst, _T("mwst"));

	FillKosten();

	CString kz;
	kz =  TLIEF.steuer_kz;

	if (kz == _T("O"))
	{
		m_Mwst.EnableWindow(FALSE);
	}
	else
	{
		m_Mwst.EnableWindow(TRUE);
	}

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);
//	CtrlGrid.SetNewStyle (TRUE);

	MdnGrid.Create (this, 1, 3);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1);
	c_MdnName->SetCellPos (5, 0);
	MdnGrid.Add (c_MdnName);

	FilGrid.Create (this, 1, 3);
    FilGrid.SetBorder (0, 0);
    FilGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Fil = new CCtrlInfo (&m_Fil, 0, 0, 1, 1);
	FilGrid.Add (c_Fil);
	CCtrlInfo *c_FilName = new CCtrlInfo (&m_FilName, 2, 0, 1, 1);
	c_FilName->SetCellPos (5, 0);
	FilGrid.Add (c_FilName);

	LiefGrid.Create (this, 1, 3);
    LiefGrid.SetBorder (0, 0);
    LiefGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Lief = new CCtrlInfo (&m_Lief, 0, 0, 1, 1);
	LiefGrid.Add (c_Lief);
	CtrlGrid.CreateChoiceButton (m_LiefChoice, IDC_LIEFCHOICE, this);
	CCtrlInfo *c_LiefChoice = new CCtrlInfo (&m_LiefChoice, 1, 0, 1, 1);
	LiefGrid.Add (c_LiefChoice);
	CCtrlInfo *c_LiefName = new CCtrlInfo (&m_LiefName, 2, 0, 1, 1);
	c_LiefName->SetCellPos (5, 0);
	LiefGrid.Add (c_LiefName);

	ZahlKondGrid.Create (this, 5, 5);
    ZahlKondGrid.SetBorder (0, 0);
    ZahlKondGrid.SetGridSpace (0, 2);
	CCtrlInfo *c_ZahlKond = new CCtrlInfo (&m_ZahlKond, 0, 0, 1, 1);
	ZahlKondGrid.Add (c_ZahlKond);
	CtrlGrid.CreateChoiceButton (m_ZahlKondChoice, IDC_ZAHL_KONDCHOICE, this);
	CCtrlInfo *c_ZahlKondChoice = new CCtrlInfo (&m_ZahlKondChoice, 1, 0, 1, 1);
	ZahlKondGrid.Add (c_ZahlKondChoice);
	CCtrlInfo *c_EnterZahlKond = new CCtrlInfo (&m_EnterZahlKond, 4, 0, 3, 1);
//	c_EnterZahlKond->SetCellPos (0, -10);
	ZahlKondGrid.Add (c_EnterZahlKond);
	CCtrlInfo *c_Ziel1 = new CCtrlInfo (&m_Ziel1, 0, 1, 1, 1);
	ZahlKondGrid.Add (c_Ziel1);
	CCtrlInfo *c_LZiel1 = new CCtrlInfo (&m_LZiel1, 1, 1, 1, 1);
	ZahlKondGrid.Add (c_LZiel1);
	CCtrlInfo *c_Skto1 = new CCtrlInfo (&m_Skto1, 2, 1, 1, 1);
	ZahlKondGrid.Add (c_Skto1);
	CCtrlInfo *c_LSkto1 = new CCtrlInfo (&m_LSkto1, 3, 1, 1, 1);
	ZahlKondGrid.Add (c_LSkto1);

	CCtrlInfo *c_Ziel2 = new CCtrlInfo (&m_Ziel2, 0, 2, 1, 1);
	ZahlKondGrid.Add (c_Ziel2);
	CCtrlInfo *c_LZiel2 = new CCtrlInfo (&m_LZiel2, 1, 2, 1, 1);
	ZahlKondGrid.Add (c_LZiel2);
	CCtrlInfo *c_Skto2 = new CCtrlInfo (&m_Skto2, 2, 2, 1, 1);
	ZahlKondGrid.Add (c_Skto2);
	CCtrlInfo *c_LSkto2 = new CCtrlInfo (&m_LSkto2, 3, 2, 1, 1);
	ZahlKondGrid.Add (c_LSkto2);

	CCtrlInfo *c_Ziel3 = new CCtrlInfo (&m_Ziel3, 0, 3, 1, 1);
	ZahlKondGrid.Add (c_Ziel3);
	CCtrlInfo *c_LZiel3 = new CCtrlInfo (&m_LZiel3, 1, 3, 1, 1);
	ZahlKondGrid.Add (c_LZiel3);
	CCtrlInfo *c_Skto3 = new CCtrlInfo (&m_Skto3, 2, 3, 1, 1);
	ZahlKondGrid.Add (c_Skto3);
	CCtrlInfo *c_LSkto3 = new CCtrlInfo (&m_LSkto3, 3, 3, 1, 1);
	ZahlKondGrid.Add (c_LSkto3);

	BlzGrid.Create (this, 0, 2);
    BlzGrid.SetBorder (0, 0);
    BlzGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Blz = new CCtrlInfo (&m_Blz, 0, 0, 1, 1);
	BlzGrid.Add (c_Blz);
	CtrlGrid.CreateChoiceButton (m_BlzChoice, IDC_BLZCHOICE, this);
	CCtrlInfo *c_BlzChoice = new CCtrlInfo (&m_BlzChoice, 1, 0, 1, 1);
	BlzGrid.Add (c_BlzChoice);

	BankGrid.Create (this, 5, 5);
    BankGrid.SetBorder (0, 0);
    BankGrid.SetGridSpace (5, 8);
	CCtrlInfo *c_LBlz = new CCtrlInfo (&m_LBlz, 0, 0, 1, 1);
	BankGrid.Add (c_LBlz);
	CCtrlInfo *c_BlzGrid = new CCtrlInfo (&BlzGrid, 1, 0, 1, 1);
	BankGrid.Add (c_BlzGrid);

	CCtrlInfo *c_LBankNam = new CCtrlInfo (&m_LBankNam, 0, 1, 1, 1);
	BankGrid.Add (c_LBankNam);
	CCtrlInfo *c_BankNam = new CCtrlInfo (&m_BankNam, 1, 1, 1, 1);
	BankGrid.Add (c_BankNam);

	CCtrlInfo *c_LIban = new CCtrlInfo (&m_LIban, 0, 2, 1, 1);
	BankGrid.Add (c_LIban);
	CCtrlInfo *c_Iban = new CCtrlInfo (&m_Iban, 1, 2, 1, 1);
	BankGrid.Add (c_Iban);

	CCtrlInfo *c_LBIC = new CCtrlInfo (&m_LBIC, 0, 3, 1, 1);
	BankGrid.Add (c_LBIC);
	CCtrlInfo *c_BIC = new CCtrlInfo (&m_BIC, 1, 3, 1, 1);
	BankGrid.Add (c_BIC);

	CCtrlInfo *c_LKto = new CCtrlInfo (&m_LKto, 3, 0, 1, 1);
	BankGrid.Add (c_LKto);
	CCtrlInfo *c_Kto = new CCtrlInfo (&m_Kto, 4, 0, 1, 1);
	BankGrid.Add (c_Kto);

	CCtrlInfo *c_LUstId = new CCtrlInfo (&m_LUstId, 3, 1, 1, 1);
	BankGrid.Add (c_LUstId);
	CCtrlInfo *c_UstId = new CCtrlInfo (&m_UstId, 4, 1, 1, 1);
	BankGrid.Add (c_UstId);

	CCtrlInfo *c_LSondKond = new CCtrlInfo (&m_LSondKond, 3, 2, 1, 1);
	BankGrid.Add (c_LSondKond);
	CCtrlInfo *c_SondKond = new CCtrlInfo (&m_SondKond, 4, 2, 1, 1);
	BankGrid.Add (c_SondKond);

	CCtrlInfo *c_LTageBis = new CCtrlInfo (&m_LTageBis, 3, 3, 1, 1);
	BankGrid.Add (c_LTageBis);
	CCtrlInfo *c_TageBis = new CCtrlInfo (&m_TageBis, 4, 3, 1, 1);
	BankGrid.Add (c_TageBis);

	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 4);
    c_HeadBorder->rightspace = 10; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);

	CCtrlInfo *c_LFil = new CCtrlInfo (&m_LFil, 1, 2, 1, 1);
	CtrlGrid.Add (c_LFil);
	CCtrlInfo *c_FilGrid = new CCtrlInfo (&FilGrid, 2, 2, 2, 1);
	CtrlGrid.Add (c_FilGrid);

	CCtrlInfo *c_LLief = new CCtrlInfo (&m_LLief, 1, 3, 1, 1);
	CtrlGrid.Add (c_LLief);
	CCtrlInfo *c_LiefGrid = new CCtrlInfo (&LiefGrid, 2, 3, 3, 1);
	CtrlGrid.Add (c_LiefGrid);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 5, 3, DOCKBOTTOM);
	c_DataBorder->SetBottomControl (this, &m_BankGroup, 10, CCtrlInfo::Top);
    CtrlGrid.Add (c_DataBorder);

	CCtrlInfo *c_LEgBetriebsnr = new CCtrlInfo (&m_LEgBetriebsnr, 1, 6, 1, 1);
	CtrlGrid.Add (c_LEgBetriebsnr);
	CCtrlInfo *c_EgBetriebsnr = new CCtrlInfo (&m_EgBetriebsnr, 2, 6, 1, 1);
	CtrlGrid.Add (c_EgBetriebsnr);

	CCtrlInfo *c_LSteuerNr = new CCtrlInfo (&m_LSteuerNummer, 1, 7, 1, 1);
	CtrlGrid.Add (c_LSteuerNr);
	CCtrlInfo *c_SteuerNr = new CCtrlInfo (&m_SteuerNummer, 2, 7, 1, 1);
	CtrlGrid.Add (c_SteuerNr);

	CCtrlInfo *c_LSteuerKz = new CCtrlInfo (&m_LSteuerKz, 1, 8, 1, 1);
	CtrlGrid.Add (c_LSteuerKz);
	CCtrlInfo *c_SteuerKz = new CCtrlInfo (&m_SteuerKz, 2, 8, 1, 1);
	CtrlGrid.Add (c_SteuerKz);

	CCtrlInfo *c_LMwst = new CCtrlInfo (&m_LMwst, 1, 9, 1, 1);
	CtrlGrid.Add (c_LMwst);
	CCtrlInfo *c_Mwst = new CCtrlInfo (&m_Mwst, 2, 9, 1, 1);
	CtrlGrid.Add (c_Mwst);

	CCtrlInfo *c_OrdrKz = new CCtrlInfo (&m_OrdrKz, 2, 10, 1, 1);
	CtrlGrid.Add (c_OrdrKz);

	CCtrlInfo *c_LAbr = new CCtrlInfo (&m_LAbr, 1, 12, 1, 1);
	CtrlGrid.Add (c_LAbr);
	CCtrlInfo *c_Abr = new CCtrlInfo (&m_Abr, 2, 12, 1, 1);
	CtrlGrid.Add (c_Abr);

	CCtrlInfo *c_LMahnStu = new CCtrlInfo (&m_LMahnStu, 1, 13, 1, 1);
	CtrlGrid.Add (c_LMahnStu);
	CCtrlInfo *c_MahnStu = new CCtrlInfo (&m_MahnStu, 2, 13, 1, 1);
	CtrlGrid.Add (c_MahnStu);

	CCtrlInfo *c_LRechKz = new CCtrlInfo (&m_LRechKz, 1, 14, 1, 1);
	CtrlGrid.Add (c_LRechKz);
	CCtrlInfo *c_RechKz = new CCtrlInfo (&m_RechKz, 2, 14, 1, 1);
	CtrlGrid.Add (c_RechKz);

	CCtrlInfo *c_LRechToler = new CCtrlInfo (&m_LRechToler, 1, 15, 1, 1);
	CtrlGrid.Add (c_LRechToler);
	CCtrlInfo *c_RechToler = new CCtrlInfo (&m_RechToler, 2, 15, 1, 1);
	CtrlGrid.Add (c_RechToler);

	CCtrlInfo *c_LWeErfKz = new CCtrlInfo (&m_LWeErfKz, 1, 16, 1, 1);
	CtrlGrid.Add (c_LWeErfKz);
	CCtrlInfo *c_WeErfKz = new CCtrlInfo (&m_WeErfKz, 2, 16, 1, 1);
	CtrlGrid.Add (c_WeErfKz);

	CCtrlInfo *c_LWePrKz = new CCtrlInfo (&m_LWePrKz, 1, 17, 1, 1);
	CtrlGrid.Add (c_LWePrKz);
	CCtrlInfo *c_WePrKz = new CCtrlInfo (&m_WePrKz, 2, 17, 1, 1);
	CtrlGrid.Add (c_WePrKz);

	CCtrlInfo *c_LInk = new CCtrlInfo (&m_LInk, 1, 18, 1, 1);
	CtrlGrid.Add (c_LInk);
	CCtrlInfo *c_Ink = new CCtrlInfo (&m_Ink, 2, 18, 1, 1);
	CtrlGrid.Add (c_Ink);

	CCtrlInfo *c_LZahlw = new CCtrlInfo (&m_LZahlw, 1, 19, 1, 1);
	CtrlGrid.Add (c_LZahlw);
	CCtrlInfo *c_Zahlw = new CCtrlInfo (&m_Zahlw, 2, 19, 1, 1);
	CtrlGrid.Add (c_Zahlw);

	CCtrlInfo *c_LKosten = new CCtrlInfo (&m_LKosten, 1, 21, 1, 1);
	CtrlGrid.Add (c_LKosten);
	CCtrlInfo *c_Kosten = new CCtrlInfo (&m_Kosten, 2, 21, 1, 1);
	CtrlGrid.Add (c_Kosten);

	CCtrlInfo *c_ZahlKondGroup = new CCtrlInfo (&m_ZahlKondGroup, 3, 18, DOCKRIGHT, DOCKBOTTOM);
	c_ZahlKondGroup->SetBottomControl (this, &m_BankGroup, 5, CCtrlInfo::Top);
	c_ZahlKondGroup->rightspace = 10; 
    CtrlGrid.Add (c_ZahlKondGroup);

	CCtrlInfo *c_LZahlKond = new CCtrlInfo (&m_LZahlKond, 4, 19, 1, 1);
	CtrlGrid.Add (c_LZahlKond);

	CCtrlInfo *c_ZahlKondGrid = new CCtrlInfo (&ZahlKondGrid, 5, 19, 3, 3);
	CtrlGrid.Add (c_ZahlKondGrid);

	CCtrlInfo *c_LTxt = new CCtrlInfo (&m_LTxt, 4, 22, 1, 1);
	CtrlGrid.Add (c_LTxt);
	CCtrlInfo *c_Txt = new CCtrlInfo (&m_Txt, 5, 22, 1, 1);
	CtrlGrid.Add (c_Txt);

	CCtrlInfo *c_BankGroup = new CCtrlInfo (&m_BankGroup, 0, 24, DOCKRIGHT, DOCKBOTTOM);
//	c_BankGroup->SetBottomControl (this, &m_Iban1, 5, CCtrlInfo::Bottom);
    CtrlGrid.Add (c_BankGroup);

	CCtrlInfo *c_BankGrid = new CCtrlInfo (&BankGrid, 1, 25, 8, DOCKBOTTOM);
    CtrlGrid.Add (c_BankGrid);
	c_BankGroup->SetBottomControl (this, &m_TageBis, 5, CCtrlInfo::Bottom);

	CCtrlInfo *c_DataBorder2 = new CCtrlInfo (&m_DataBorder2, 3, 5, DOCKRIGHT, DOCKBOTTOM);
    c_DataBorder2->rightspace = 10; 
//	c_DataBorder2->SetBottomControl (this, &m_Hhdklpargr, 5, CCtrlInfo::Bottom);
	c_DataBorder2->SetBottomControl (this, &m_ZahlKondGroup, 5, CCtrlInfo::Top);
	CtrlGrid.Add (c_DataBorder2);

	CCtrlInfo *c_LViehBas = new CCtrlInfo (&m_LViehBas, 4, 6, 1, 1);
	CtrlGrid.Add (c_LViehBas);
	CCtrlInfo *c_ViehBas = new CCtrlInfo (&m_ViehBas, 5, 6, 1, 1);
	CtrlGrid.Add (c_ViehBas);
	CCtrlInfo *c_Zertifikat1 = new CCtrlInfo (&m_Zertifikat1, 6, 6, 1, 1);
	CtrlGrid.Add (c_Zertifikat1);
	CCtrlInfo *c_LSlash = new CCtrlInfo (&m_LSlash, 7, 6, 1, 1);
	CtrlGrid.Add (c_LSlash);
	CCtrlInfo *c_Zertifikat2 = new CCtrlInfo (&m_Zertifikat2, 8, 6, 1, 1);
	CtrlGrid.Add (c_Zertifikat2);

	CCtrlInfo *c_LEgKz = new CCtrlInfo (&m_LEgKz, 4, 7, 1, 1);
	CtrlGrid.Add (c_LEgKz);
	CCtrlInfo *c_EgKz = new CCtrlInfo (&m_EgKz, 5, 7, 1, 1);
	CtrlGrid.Add (c_EgKz);

	CCtrlInfo *c_FrachtKz = new CCtrlInfo (&m_FrachtKz, 5, 8, 1, 1);
	CtrlGrid.Add (c_FrachtKz);

	CCtrlInfo *c_LRabKz = new CCtrlInfo (&m_LRabKz, 4, 9, 1, 1);
	CtrlGrid.Add (c_LRabKz);
	CCtrlInfo *c_RabKz = new CCtrlInfo (&m_RabKz, 5, 9, 4, 1);
	CtrlGrid.Add (c_RabKz);

	CCtrlInfo *c_LRabAbl = new CCtrlInfo (&m_LRabAbl, 4, 10, 1, 1);
	CtrlGrid.Add (c_LRabAbl);
	CCtrlInfo *c_RabAbl = new CCtrlInfo (&m_RabAbl, 5, 10, 4, 1);
	CtrlGrid.Add (c_RabAbl);
	CCtrlInfo *c_EnterRabAbl = new CCtrlInfo (&m_EnterRabAbl, 6, 10, 4, 1);
	CtrlGrid.Add (c_EnterRabAbl);

	CCtrlInfo *c_EnterBoni = new CCtrlInfo (&m_EnterBoni, 6, 12, 4, 1);
	CtrlGrid.Add (c_EnterBoni);

	CCtrlInfo *c_LListNr = new CCtrlInfo (&m_LListNr, 4, 11, 1, 1);
	CtrlGrid.Add (c_LListNr);
	CCtrlInfo *c_ListNr = new CCtrlInfo (&m_ListNr, 5, 11, 1, 1);
	CtrlGrid.Add (c_ListNr);

	CCtrlInfo *c_LMinZuschProz = new CCtrlInfo (&m_LMinZuschProz, 4, 12, 1, 1);
	CtrlGrid.Add (c_LMinZuschProz);
	CCtrlInfo *c_MinZuschProz = new CCtrlInfo (&m_MinZuschProz, 5, 12, 1, 1);
	CtrlGrid.Add (c_MinZuschProz);

	CCtrlInfo *c_LMinZusch = new CCtrlInfo (&m_LMinZusch, 4, 13, 1, 1);
	CtrlGrid.Add (c_LMinZusch);
	CCtrlInfo *c_MinZusch = new CCtrlInfo (&m_MinZusch, 5, 13, 1, 1);
	CtrlGrid.Add (c_MinZusch);

	CCtrlInfo *c_LLiefZusch = new CCtrlInfo (&m_LLiefZusch, 4, 14, 1, 1);
	CtrlGrid.Add (c_LLiefZusch);
	CCtrlInfo *c_LiefZusch = new CCtrlInfo (&m_LiefZusch, 5, 14, 1, 1);
	CtrlGrid.Add (c_LiefZusch);

	CCtrlInfo *c_LVorkgr = new CCtrlInfo (&m_LVorkgr, 4, 15, 1, 1);
	CtrlGrid.Add (c_LVorkgr);
	CCtrlInfo *c_Vorkgr = new CCtrlInfo (&m_Vorkgr, 5, 15, 1, 1);
	CtrlGrid.Add (c_Vorkgr);

	CCtrlInfo *c_LHhdklpargr = new CCtrlInfo (&m_LHhdklpargr, 4, 16, 1, 1);
	CtrlGrid.Add (c_LHhdklpargr);
	CCtrlInfo *c_Hhdklpargr = new CCtrlInfo (&m_Hhdklpargr, 5, 16, 1, 1);
	CtrlGrid.Add (c_Hhdklpargr);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	if (IsWindow (m_EnterZahlKond.Tooltip.m_hWnd))
	{
		m_EnterZahlKond.Tooltip.SetFont (&Font);
		m_EnterZahlKond.Tooltip.SetSize ();
	}

	CtrlGrid.Display ();
	Form.Show ();

	return FALSE;
}

HBRUSH CBasePage2::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	DlgBkColor = WINDOWHANDLER->GetDlgBkColor ();
	DlgBrush   = WINDOWHANDLER->GetDlgBrush ();
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			WINDOWHANDLER->SetDlgBkColor (DlgBkColor);
			WINDOWHANDLER->SetDlgBrush (DlgBrush);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CBasePage2::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				WINDOWHANDLER->OnBack ();
				return TRUE;
			}
/*
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
*/
			else if (pMsg->wParam == VK_F12)
			{
				Form.Get();

				 CORE->WriteLief (); //testtest
				WINDOWHANDLER->OnWrite ();
				return TRUE;
			}

/*
			else if (pMsg->wParam == VK_F8)
			{
				OnChoice ();
			}
*/

/*
			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}

 		    if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
*/
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CBasePage2::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CBasePage2::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

void CBasePage2::FillPtabCombo (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		CORE->FillPtabCombo (f->ComboValues, Item);
		f->FillComboBox ();
	}
}

void  CBasePage2::OnDestroy ()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
	CtrlGrid.DestroyGrid ();
}

BOOL CBasePage2::OnSetActive ()
{
//testtest	Form.Show ();
	return TRUE;
}

void CBasePage2::Update ()
{
	Form.Show ();
}

void CBasePage2::UpdateWrite ()
{
	Form.Get ();
	 CORE->WriteLief (); //testtest


}

void CBasePage2::OnEnKillfocusZahlKond()
{
	BOOL ret = FALSE;
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.GetFormField (&m_ZahlKond)->Get ();
	TZAHL_KOND.zahl_kond = TLIEF.zahl_kond;
	ret = DATABASE->ReadZahlKond ();
	if (!ret)
	{
		OnEnterZahlKond ();
	}
	Form.Get (); //testtest
	Form.Show ();
}

void CBasePage2::OnChoice ()
{
	CString MdnNr;
	CString FilNr;
	CChoiceLief *Choice = (CChoiceLief *) WINDOWHANDLER->GetChoice ();
	m_Mdn.GetWindowText (MdnNr);
	m_Fil.GetWindowText (FilNr);
	if (Choice != NULL)
	{
		if (!WINDOWHANDLER->GetDockChoice ())
		{
			if (atoi (MdnNr.GetBuffer ()) != Choice->m_Mdn ||
				atoi (FilNr.GetBuffer ()) != Choice->m_Fil)
			{
				delete Choice;
				Choice = NULL;
				WINDOWHANDLER->SetChoice (NULL);
			}
		}
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceLief (WINDOWHANDLER->GetBase ());
		Choice->IsModal = FALSE;
		Choice->m_Mdn = _tstoi (MdnNr.GetBuffer ());
		Choice->m_Fil = _tstoi (FilNr.GetBuffer ());
		Choice->SetBitmapButton (TRUE);
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->HideFilter = FALSE;
		Choice->HideOK    = HideOK;
		Choice->SetDbClass (&DATABASE->Lief);
		Choice->DlgBkColor = DlgBkColor;
	}
	WINDOWHANDLER->OnChoice (Choice, GetParent ());
}

void CBasePage2::OnZahlKondchoice ()
{
	ChoiceZahlKond = new CChoiceZahlKond (this);
    ChoiceZahlKond->IsModal = TRUE;
	ChoiceZahlKond->SetBitmapButton (TRUE);
	ChoiceZahlKond->CreateDlg ();

	ChoiceZahlKond->HideFilter = TRUE;
	ChoiceZahlKond->SetDbClass (&DATABASE->Lief);
	ChoiceZahlKond->DoModal();
    if (ChoiceZahlKond->GetState ())
    {
		  CZahlKondList *abl = ChoiceZahlKond->GetSelectedText (); 
		  if (abl == NULL) return;
		  DATABASE->Lief.lief.zahl_kond = abl->zahl_kond;
		  TZAHL_KOND.zahl_kond = abl->zahl_kond;
		  CORE->ReadZahlKond ();
		  Form.Show ();
		  m_ZahlKond.SetSel (0, -1, TRUE);
		  m_ZahlKond.SetFocus ();
//          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
    delete ChoiceZahlKond;
    ChoiceZahlKond = NULL;
}

void CBasePage2::OnEnterZahlKond ()
{
	CZahlKondDialog dlg;

	dlg.SetReadOnly ();
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		TLIEF.zahl_kond = TZAHL_KOND.zahl_kond;
		Form.Show ();
	}
}

void CBasePage2::OnBlzchoice ()
{
	ChoiceBlz = new CChoiceBlz (this);
    ChoiceBlz->IsModal = TRUE;
	ChoiceBlz->SetBitmapButton (TRUE);
	ChoiceBlz->CreateDlg ();

//	ChoiceBlz->HideFilter = TRUE;
	ChoiceBlz->HideEnter = TRUE;
	ChoiceBlz->SetDbClass (&DATABASE->Lief);
	ChoiceBlz->DoModal();
    if (ChoiceBlz->GetState ())
    {
		  CBlzList *abl = ChoiceBlz->GetSelectedText (); 
		  if (abl == NULL) return;
		  DATABASE->Lief.lief.blz = _tstol (abl->blz.GetBuffer ());
		  _tcscpy (DATABASE->Lief.lief.bank_nam, abl->bank_name.GetBuffer ());
		  Form.Show ();
		  m_Blz.SetSel (0, -1, TRUE);
		  m_Blz.SetFocus ();
//          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
    delete ChoiceBlz;
    ChoiceBlz = NULL;
}

void CBasePage2::OnEnterRabAbl ()
{
	CLiefRKteDlg LiefRKteDlg;
	CLiefRStfDlg LiefRStfDlg;
	CLiefRGrDlg LiefRGrDlg;
	CLiefRLisDlg LiefRLisDlg;

	Form.Get ();
	if (TLIEF.rab_abl[0] == _T('K'))
	{
		INT_PTR ret = LiefRKteDlg.DoModal ();
	}
	else if (TLIEF.rab_abl[0] == _T('S'))
	{
		INT_PTR ret = LiefRStfDlg.DoModal ();
	}
	else if (TLIEF.rab_abl[0] == _T('G'))
	{
		INT_PTR ret = LiefRGrDlg.DoModal ();
	}
	else if (TLIEF.rab_abl[0] == _T('L'))
	{
		INT_PTR ret = LiefRLisDlg.DoModal ();
	}
}

void CBasePage2::OnEnterBoni ()
{
	CLiefBoniDlg LiefBoniDlg;

	INT_PTR ret = LiefBoniDlg.DoModal ();
}


void CBasePage2::OnCbnSelchangeSteuerKz()
{
	CString kz;
	
	Form.Get();

	kz =  TLIEF.steuer_kz;

	if (kz == _T("O"))
	{
		m_Mwst.EnableWindow(FALSE);
	}
	else
	{
		m_Mwst.EnableWindow(TRUE);
	}
}


void CBasePage2::FillKosten ()
{
	CFormField *f = Form.GetFormField (&m_Kosten);
	if (f != NULL)
	{
		f->ComboValues.clear ();
	
		Kst.sqlout ((long *) &Kst.kst.kst, SQLLONG, 0); 
		Kst.sqlout ((char *) Kst.kst.kst_bz1  , SQLCHAR, sizeof (Kst.kst.kst_bz1)); 
		
        int cursor = Kst.sqlcursor (_T("select kst, kst_bz1 from kst ")
			                          	  _T("order by kst"));
		
		
		
		while (Kst.sqlfetch (cursor) == 0)
		{
			Kst.dbreadfirst ();

			

			//LPSTR pos = (LPSTR) Iprgrstufk.iprgrstufk.pr_gr_stuf;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			//pos = (LPSTR) Iprgrstufk.iprgrstufk.zus_bz;
			//CDbUniCode::DbToUniCode (Iprgrstufk.iprgrstufk.pr_gr_stuf, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%ld %s"), Kst.kst.kst,
											 Kst.kst.kst_bz1);
		    f->ComboValues.push_back (ComboValue); 
		}
		Kst.sqlclose (cursor);
		f->FillComboBox ();
		//f->SetSel (0);
		//f->Get ();
	}
}
