#include "StdAfx.h"
#include "bmpfunc.h"
#include "Token.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

HGLOBAL CBmpFunc::hglbDlg = NULL;

CBmpFunc::CBmpFunc(void)
{
    hBitmap = NULL;
	hBitmapZoom = NULL;
    bmap = NULL;
}

CBmpFunc::~CBmpFunc(void)
{
}

void CBmpFunc::SetBitmap (HBITMAP hBitmap)
{
	this-> hBitmap = hBitmap;
	hBitmapOrg = hBitmap;
}

HBITMAP CBmpFunc::GetBitmap (void)
{
    return hBitmap;
}

void CBmpFunc::DestroyBitmap (void)
/**
Aktuelle Bitmap loeschen.
**/
{
       if (bmap)
       {
           GlobalFree (bmap);
           bmap = NULL;
           hglbDlg = NULL;
       }
}

void CBmpFunc::TestEnvironment (LPTSTR dest, LPTSTR source)
{
         CString Source;
         CToken token;

         Source = source;
         token.SetSep (_T("$"));
         token = Source;

         _tcscpy (dest, _T(""));
         if (token.GetAnzToken () < 2)
         {
                  Source = token.GetToken (0);
         }
         else
         {
                  Source = token.GetToken (0);
                  _tcscat (dest, Source.GetBuffer ());
                  Source = token.GetToken (1);
         }

         token.SetSep (_T("\\"));
         token = Source;
         CString text = token.NextToken ();
         LPTSTR dir = _tgetenv (text.GetBuffer ());
         if (dir == NULL)
         {
                  _tcscat (dest, text.GetBuffer ());
         }
         else
         {
                  _tcscat (dest, dir);
         }

         LPTSTR p;
         while ((p = token.NextToken ()) != NULL)
         {
             _tcscat (dest, _T("\\"));
             _tcscat (dest, p);
         }
}


HBITMAP CBmpFunc::ReadBitmap (HDC hdc, LPTSTR bname)
/**
Graphic ausgeben.
**/
{
         FILE *bp;
         char far *rptr;
         size_t bytes;
         size_t rbytes;
         BITMAPFILEHEADER bmpheader;
         DWORD bmbitsoffs;
         const char far *bmbits;
         _TCHAR fname[512];


         TestEnvironment (fname, bname);
         bp = _tfopen (fname, _T("rb"));
         if (bp == (FILE *) 0)
         {
//                     disp_mess ("Bitmap-Datei kann nicht ge�ffnet werden", 2);
                     return NULL;
         }

         bytes = fread (&bmpheader, 1, sizeof (BITMAPFILEHEADER), bp);
         bmbitsoffs = bmpheader.bfOffBits - sizeof (BITMAPFILEHEADER);
         hglbDlg = GlobalAlloc (GMEM_FIXED, bmpheader.bfSize);
         if (hglbDlg == NULL)
         {
                     fclose (bp);
                     return NULL;
         }
         bmap = (BITMAPINFO FAR*) GlobalLock (hglbDlg);
         if (bmap == NULL)
         {
                     AfxMessageBox (_T("Speicher kann nicht zugeteilt werden"), MB_OK, 0);
                     fclose (bp);
                     return NULL;
         }

         rptr = (char far *) bmap;
         rbytes = fread ((char *) rptr, 1, 0x1000, bp);
         while (rbytes == 0x1000)
         {
                 rptr += 0x1000;
                 rbytes = fread ((BYTE *) rptr, 1, 0x1000, bp);
         }

         bmbits = (const char far *) bmap + bmbitsoffs;
         hBitmap = CreateDIBitmap (hdc, (BITMAPINFOHEADER FAR *) bmap,
                                   CBM_INIT, bmbits, bmap, DIB_RGB_COLORS);
         fclose (bp);
         hBitmapOrg = hBitmap;
         return hBitmap;
}

void CBmpFunc::DrawBitmap (HDC hdc, HBITMAP hBitmap, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
//        DWORD dwsize;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void CBmpFunc::DrawCompatibleBitmap (HDC hdc, HBITMAP hBitmap, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
//        DWORD dwsize;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        hBitmap = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);

        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void CBmpFunc::DrawCompatibleBitmap (HDC hdc, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
		HBITMAP hBitmapZoom;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        hBitmapZoom = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);

        SelectObject (hdcMem, hBitmapZoom);

        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);

        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void CBmpFunc::BitmapSize (HDC hdc,POINT *ptSize)
/**
Bitmap-Recheck holen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize->x = bm.bmWidth;
        ptSize->y = bm.bmHeight;
        DeleteDC (hdcMem); 
}
   
void CBmpFunc::BitmapSize (HDC hdc, HBITMAP hBitmap, POINT *ptSize)
/**
Bitmap-Recheck holen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize->x = bm.bmWidth;
        ptSize->y = bm.bmHeight;
        DeleteDC (hdcMem); 
}

void CBmpFunc::BitmapSize (CDC *pDC, HBITMAP hBitmap, POINT *ptSize)
/**
Bitmap-Recheck holen.
**/
{
        HDC hdc = pDC->m_hDC;
        BitmapSize (hdc, hBitmap, ptSize);
}

void CBmpFunc::DrawBitmap (HDC hdc, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void CBmpFunc::DrawBitmapExt (HDC hdc, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
		SIZE size;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);

//        SetMapMode (hdcMem, MM_LOENGLISH); 
        SetMapMode (hdcMem, MM_ANISOTROPIC);

		GetViewportExtEx (hdc, &size);
		SetWindowExtEx (hdc, 1, 1, NULL);
		SetViewportExtEx (hdc, size.cx, size.cy, NULL);
		SetViewportOrgEx (hdc, 0, 0, NULL);
		
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void CBmpFunc::DrawBitmapEx (HDC hdc, int xStart, int yStart, DWORD drawmode)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, drawmode);
        DeleteDC (hdcMem); 
}


void CBmpFunc::DrawBitmapPart (HDC hdc, int xStart, int yStart, 
                                    int xFrom,  int yFrom,
                                    int cx,     int cy)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;
        int bmx, bmy;
        RECT rect;

        GetClientRect (bmphWnd, &rect); 
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);

        bmx = max (0, (rect.right  - bm.bmWidth)  / 2);
        bmy = max (0, (rect.bottom - bm.bmHeight) / 2);

        if (xStart == 0)
        {
            xStart = bmx;
        }
        if (yStart == 0)
        {
            yStart = bmy;
        }

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        if ((bm.bmWidth - xFrom) < cx)
        {
            cx = bm.bmWidth - xFrom;
        }
        if ((bm.bmHeight - yFrom) < cy)
        {
            cy = bm.bmHeight - yFrom;
        }

        ptSize.x = cx;
        ptSize.y = cy;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = xFrom;
        ptOrg.y = yFrom;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}

void CBmpFunc::DrawBitmapPart (HDC hdc, HBITMAP hBitmap,
                                    int xStart, int yStart, 
                                    int xFrom,  int yFrom,
                                    int cx,     int cy)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;
        int bmx, bmy;
        RECT rect;

        GetClientRect (bmphWnd, &rect); 
        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);

        bmx = max (0, (rect.right  - bm.bmWidth)  / 2);
        bmy = max (0, (rect.bottom - bm.bmHeight) / 2);

        if (xStart == 0)
        {
            xStart = bmx;
        }
        if (yStart == 0)
        {
            yStart = bmy;
        }

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmap);
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        if ((bm.bmWidth - xFrom) < cx)
        {
            cx = bm.bmWidth - xFrom;
        }
        if ((bm.bmHeight - yFrom) < cy)
        {
            cy = bm.bmHeight - yFrom;
        }

        ptSize.x = cx;
        ptSize.y = cy;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = xFrom;
        ptOrg.y = yFrom;
        DPtoLP (hdcMem, &ptOrg, 1); 

        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCCOPY);
        DeleteDC (hdcMem); 
}


void CBmpFunc::ZoomBitmap (HDC hdc, int xStart, int yStart, 
                                int xFrom,  int yFrom,
                                int cx,     int cy,
                                double zoom)
/**
Bitmap zeichnen.
**/
{

        if (zoom == 1)
        {
            hBitmap = hBitmapOrg;
            DrawBitmapPart (hdc, xStart, yStart, xFrom, yFrom,
                                 cx, cy);
        }
        else if (hBitmapZoom)
        {
            DrawBitmapPart (hdc, hBitmapZoom, xStart, yStart,
                                 xFrom, yFrom,
                                 cx, cy);
        }
        else
        {
            hBitmap = hBitmapOrg;
            DrawBitmapPart (hdc, xStart, yStart, xFrom, yFrom,
                                 cx, cy);
        }

}


void CBmpFunc::StrechBitmap (HDC hdc, int cx, int cy)
{

        HDC hdcMemory;
        HDC hbmOld;
        BITMAP bm;

        GetObject (hBitmap, sizeof (BITMAP), &bm);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = (HDC) SelectObject (hdcMemory, hBitmap);

        SetViewportOrgEx (hdc, 0, 0, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, 0, 0,
                         cx, cy,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, SRCCOPY);
        
        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
}

void CBmpFunc::StrechBitmap (HDC hdc, int xstart, int ystart, double fx, double fy)
{

        HDC hdcMemory;
        HDC hbmOld;
        BITMAP bm;
		int cx, cy;

        GetObject (hBitmap, sizeof (BITMAP), &bm);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = (HDC) SelectObject (hdcMemory, hBitmap);

        SetViewportOrgEx (hdc, 0, 0, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
		cx = (int) (double) ((double) bm.bmWidth  * fx);
		cy = (int) (double) ((double) bm.bmHeight * fy);
        StretchBlt (hdc, xstart, ystart,
                         cx, cy,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, SRCCOPY);
        
        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
}

void CBmpFunc::StrechBitmap (HDC whdc, HDC hdc, int xstart, int ystart)
{

        HDC hdcMemory;
        HDC hbmOld;
        BITMAP bm;
		int cx, cy;
		int cxd, cyd;
		int cxp, cyp;
		double fx, fy;

		cxd = GetDeviceCaps (whdc, HORZRES);
		cyd = GetDeviceCaps (whdc, VERTRES);
		cxp = GetDeviceCaps (hdc, HORZRES);
		cyp = GetDeviceCaps (hdc, VERTRES);

		fx = (double) cxp / cxd;
		fy = (double) cyp / cyd;
		fx = (fx + fy) / 2;
		fy = fx;

        GetObject (hBitmap, sizeof (BITMAP), &bm);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = (HDC) SelectObject (hdcMemory, hBitmap);

        SetViewportOrgEx (hdc, 0, 0, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
		cx = (int) (double) ((double) bm.bmWidth  * fx);
		cy = (int) (double) ((double) bm.bmHeight * fy);
        StretchBlt (hdc, xstart, ystart,
                         cx, cy,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, SRCCOPY);
        
        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
}

void CBmpFunc::StrechBitmapMem (HWND hWnd, int cx, int cy)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
        HDC hdc;


        if (hBitmapZoom)
        {
            DeleteObject (hBitmapZoom);
        }
        hdc = GetDC (hWnd);
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = (HDC) SelectObject (hdcMemory, hBitmapOrg);
        hBitmapZoom = CreateCompatibleBitmap (hdc, cx, cy);
        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapZoom);

        SetViewportOrgEx (hdc, 0, 0, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdcMemoryZ, 0, 0,
                         cx, cy,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, SRCCOPY);
        
        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        ReleaseDC (hWnd, hdc);
        hBitmap = hBitmapZoom;

}


void CBmpFunc::DrawImage (HDC hdc, HBRUSH hBrush, CRect *rect,
						  HBITMAP hBitmapOrg, HBITMAP hMask, int xStart, int yStart)
/**
Bitmap zeichnen.
**/
{
        BITMAP bm;
        HDC hdcMem;
        POINT ptSize, ptOrg;
        int bytes;

        hdcMem = CreateCompatibleDC (hdc);
        SelectObject (hdcMem, hBitmapOrg);
        HBRUSH oldbrush;
        SetMapMode (hdcMem, GetMapMode (hdc)); 

        bytes = GetObject (hBitmap, sizeof (BITMAP), &bm);
        ptSize.x = bm.bmWidth;
        ptSize.y = bm.bmHeight;
        DPtoLP (hdc, &ptSize, 1); 

        ptOrg.x = 0;
        ptOrg.y = 0;
        DPtoLP (hdcMem, &ptOrg, 1); 

        if (hBrush != NULL)
		{
				oldbrush = (HBRUSH) SelectObject (hdc, hBrush);
				BitBlt (hdc, 0, 0, rect->right, rect->bottom,
						NULL, ptOrg.x, ptOrg.y, PATCOPY);
		}

        SelectObject (hdcMem, hMask);
        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCAND);

        SelectObject (hdcMem, hBitmapOrg);
        BitBlt (hdc, xStart, yStart, ptSize.x, ptSize.y,
                 hdcMem, ptOrg.x, ptOrg.y, SRCPAINT);
        if (hBrush != NULL)
		{
				SelectObject (hdc, oldbrush);
		}
        DeleteDC (hdcMem); 
}

void CBmpFunc::DrawBackground (HDC hdc, HBRUSH hBrush, CRect *rect)
						       
/**
Bitmap zeichnen.
**/
{
        HBRUSH oldbrush;

        if (hBrush != NULL)
		{
				oldbrush = (HBRUSH) SelectObject (hdc, hBrush);
				BitBlt (hdc, 0, 0, rect->right, rect->bottom,
						NULL, 0, 0, PATCOPY);
		       SelectObject (hdc, oldbrush);
		}
}

HBITMAP CBmpFunc::PrintBitmapMem (HBITMAP hBitmapOrg, HBITMAP hMask, COLORREF BkColor)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
        HDC hdc;
		HBITMAP hBitmapMem;
		RECT rect;
		HBRUSH hBrush, oldbrush;

 
		hdc = GetDC (NULL);
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        SetViewportOrgEx (hdc, 0, 0, NULL);

		rect.left = 0;
		rect.top = 0;
		rect.right  = bm.bmWidth;
		rect.bottom = bm.bmHeight;
		hBrush = CreateSolidBrush (BkColor);

        hdcMemory  = CreateCompatibleDC(hdc);

        hBitmapMem = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);

        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapMem);
		oldbrush = (HBRUSH) SelectObject (hdcMemoryZ, hBrush);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                NULL, 0, 0, PATCOPY); 

        hbmOld = (HDC) SelectObject (hdcMemory, hMask);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCAND); 

        SelectObject (hdcMemory, hBitmapOrg);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCPAINT); 


        SelectObject (hdcMemory, hbmOld);

		DeleteObject (hBrush);
        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        ReleaseDC (NULL, hdc);

        DeleteObject (hBitmapOrg);
        DeleteObject (hMask);

        return hBitmapMem;
}

HBITMAP CBmpFunc::PrintBitmapMem (HBITMAP hBitmapOrg, HBITMAP hMask, HBRUSH hBrush)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
        HDC hdc;
		HBITMAP hBitmapMem;
		RECT rect;
		HBRUSH oldbrush;

 
		hdc = GetDC (NULL);
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        SetViewportOrgEx (hdc, 0, 0, NULL);

		rect.left = 0;
		rect.top = 0;
		rect.right  = bm.bmWidth;
		rect.bottom = bm.bmHeight;

        hdcMemory  = CreateCompatibleDC(hdc);

//        hBitmapMem = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);
        hBitmapMem = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);

        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapMem);
		oldbrush = (HBRUSH) SelectObject (hdcMemoryZ, hBrush);
		BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                NULL, 0, 0, PATCOPY); 

        hbmOld = (HDC) SelectObject (hdcMemory, hMask);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCAND); 

        SelectObject (hdcMemory, hBitmapOrg);
        BitBlt (hdcMemoryZ, 0, 0, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCPAINT); 

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        ReleaseDC (NULL, hdc);

        DeleteObject (hBitmapOrg);
        DeleteObject (hMask);

        return hBitmapMem;
}


HBITMAP CBmpFunc::PrintBitmapMem (HBITMAP hBitmapOrg, HBITMAP hMask, CRect *mRect, COLORREF BkColor)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
        HDC hdc;
		HBITMAP hBitmapMem;
		RECT rect;
		HBRUSH hBrush,oldbrush;

 
		hdc = GetDC (NULL);
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        SetViewportOrgEx (hdc, 0, 0, NULL);
		hBrush = CreateSolidBrush (BkColor);

		rect.left = 0;
		rect.top = 0;
		rect.right  = bm.bmWidth;
		rect.bottom = bm.bmHeight;

        hdcMemory  = CreateCompatibleDC(hdc);

//        hBitmapMem = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);
        hBitmapMem = CreateCompatibleBitmap (hdc, mRect->right, mRect->bottom);

        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapMem);
		oldbrush = (HBRUSH) SelectObject (hdcMemoryZ, hBrush);
		BitBlt (hdcMemoryZ, 0, 0, mRect->right, mRect->bottom,
                NULL, 0, 0, PATCOPY); 

		int x = (mRect->right - bm.bmWidth) / 2;
		int y = (mRect->bottom - bm.bmHeight) / 2;
        hbmOld = (HDC) SelectObject (hdcMemory, hMask);
        BitBlt (hdcMemoryZ, x, y, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCAND); 

        SelectObject (hdcMemory, hBitmapOrg);
        BitBlt (hdcMemoryZ, x, y, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCPAINT); 


        SelectObject (hdcMemory, hbmOld);

		DeleteObject (hBrush);
        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        ReleaseDC (NULL, hdc);

        DeleteObject (hBitmapOrg);
        DeleteObject (hMask);

        return hBitmapMem;
}

HBITMAP CBmpFunc::PrintBitmapBottom (HBITMAP hBitmapOrg, HBITMAP hMask, CRect *mRect, COLORREF BkColor)
{
        return PrintBitmapBottom (hBitmapOrg, hMask, mRect, BkColor, 0);
}

HBITMAP CBmpFunc::PrintBitmapBottom (HBITMAP hBitmapOrg, HBITMAP hMask, CRect *mRect, COLORREF BkColor, int diff)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
        HDC hdc;
		HBITMAP hBitmapMem;
		RECT rect;
		HBRUSH hBrush,oldbrush;

 
		hdc = GetDC (NULL);
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        SetViewportOrgEx (hdc, 0, 0, NULL);
		hBrush = CreateSolidBrush (BkColor);

		rect.left = 0;
		rect.top = 0;
		rect.right  = bm.bmWidth;
		rect.bottom = bm.bmHeight;

        hdcMemory  = CreateCompatibleDC(hdc);

//        hBitmapMem = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);
        hBitmapMem = CreateCompatibleBitmap (hdc, mRect->right, mRect->bottom);

        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapMem);
		oldbrush = (HBRUSH) SelectObject (hdcMemoryZ, hBrush);
		BitBlt (hdcMemoryZ, 0, 0, mRect->right, mRect->bottom,
                NULL, 0, 0, PATCOPY); 

		int x = (mRect->right - bm.bmWidth) / 2;
		int y = (mRect->bottom - bm.bmHeight - diff);
        hbmOld = (HDC) SelectObject (hdcMemory, hMask);
        BitBlt (hdcMemoryZ, x, y, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCAND); 

        SelectObject (hdcMemory, hBitmapOrg);
        BitBlt (hdcMemoryZ, x, y, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCPAINT); 


        SelectObject (hdcMemory, hbmOld);

		DeleteObject (hBrush);
        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        ReleaseDC (NULL, hdc);

        DeleteObject (hBitmapOrg);
        DeleteObject (hMask);

        return hBitmapMem;
}

HBITMAP CBmpFunc::PrintBitmapMem (HBITMAP hBitmapOrg, HBITMAP hMask, CRect *mRect, HBRUSH hBrush)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
        HDC hdc;
		HBITMAP hBitmapMem;
		RECT rect;
		HBRUSH oldbrush;

 
		hdc = GetDC (NULL);
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        SetViewportOrgEx (hdc, 0, 0, NULL);

		rect.left = 0;
		rect.top = 0;
		rect.right  = bm.bmWidth;
		rect.bottom = bm.bmHeight;

        hdcMemory  = CreateCompatibleDC(hdc);

//        hBitmapMem = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);
        hBitmapMem = CreateCompatibleBitmap (hdc, mRect->right, mRect->bottom);

        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapMem);
		oldbrush = (HBRUSH) SelectObject (hdcMemoryZ, hBrush);
		BitBlt (hdcMemoryZ, 0, 0, mRect->right, mRect->bottom,
                NULL, 0, 0, PATCOPY); 

        hbmOld = (HDC) SelectObject (hdcMemory, hMask);
		int x = (mRect->right - bm.bmWidth) / 2;
		int y = (mRect->bottom - bm.bmHeight) / 2;
        BitBlt (hdcMemoryZ, x, y, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCAND); 

        SelectObject (hdcMemory, hBitmapOrg);
        BitBlt (hdcMemoryZ, x, y, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCPAINT); 


        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        ReleaseDC (NULL, hdc);

        DeleteObject (hBitmapOrg);
        DeleteObject (hMask);

        return hBitmapMem;
}
HBITMAP CBmpFunc::PrintBitmapBottom (HBITMAP hBitmapOrg, HBITMAP hMask, CRect *mRect, HBRUSH hBrush)
{
        return PrintBitmapBottom (hBitmapOrg, hMask, mRect, hBrush, 0);
}

HBITMAP CBmpFunc::PrintBitmapBottom (HBITMAP hBitmapOrg, HBITMAP hMask, CRect *mRect, HBRUSH hBrush, int diff)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
        HDC hdc;
		HBITMAP hBitmapMem;
		RECT rect;
		HBRUSH oldbrush;

 
		hdc = GetDC (NULL);
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        SetViewportOrgEx (hdc, 0, 0, NULL);

		rect.left = 0;
		rect.top = 0;
		rect.right  = bm.bmWidth;
		rect.bottom = bm.bmHeight;

        hdcMemory  = CreateCompatibleDC(hdc);

//        hBitmapMem = CreateCompatibleBitmap (hdc, bm.bmWidth, bm.bmHeight);
        hBitmapMem = CreateCompatibleBitmap (hdc, mRect->right, mRect->bottom);

        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapMem);
		oldbrush = (HBRUSH) SelectObject (hdcMemoryZ, hBrush);
		BitBlt (hdcMemoryZ, 0, 0, mRect->right, mRect->bottom,
                NULL, 0, 0, PATCOPY); 

        hbmOld = (HDC) SelectObject (hdcMemory, hMask);
		int x = (mRect->right - bm.bmWidth) / 2;
		int y = (mRect->bottom - bm.bmHeight - diff);
        BitBlt (hdcMemoryZ, x, y, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCAND); 

        SelectObject (hdcMemory, hBitmapOrg);
        BitBlt (hdcMemoryZ, x, y, bm.bmWidth, bm.bmHeight,
                 hdcMemory, 0, 0, SRCPAINT); 


        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        ReleaseDC (NULL, hdc);

        DeleteObject (hBitmapOrg);
        DeleteObject (hMask);

        return hBitmapMem;
}

HBITMAP CBmpFunc::LoadBitmap (HINSTANCE hInstance, LPTSTR BitmapName, LPTSTR MaskName)
{
	return PrintBitmapMem (::LoadBitmap (hInstance, BitmapName),
		                   ::LoadBitmap (hInstance, MaskName),
						   GetSysColor (COLOR_3DFACE));
}

HBITMAP CBmpFunc::LoadBitmap (HINSTANCE hInstance, LPTSTR BitmapName, LPTSTR MaskName, COLORREF BkColor)
{
	return PrintBitmapMem (::LoadBitmap (hInstance, BitmapName),
		                   ::LoadBitmap (hInstance, MaskName),
						   BkColor);
}

HBITMAP CBmpFunc::LoadBitmap (HINSTANCE hInstance, DWORD BitmapId, DWORD MaskId, COLORREF BkColor)
{
	return PrintBitmapMem (::LoadBitmap (hInstance, MAKEINTRESOURCE (BitmapId)),
		                   ::LoadBitmap (hInstance, MAKEINTRESOURCE (MaskId)),
						   BkColor);
}

