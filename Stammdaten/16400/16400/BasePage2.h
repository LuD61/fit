#pragma once
#include "DbPropertyPage.h"
#include "resource.h"
#include "distributorcore.h"
#include "StaticButton.h"
#include "ColorButton.h"
#include "kst.h"
#include "CtrlGridColor.h"
#include "FormTab.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "afxwin.h"
#include "DataCollection.h"
#include "ChoiceLief.h"
#include "ChoiceZahlKond.h"
#include "ChoiceBlz.h"
#include "UpdateHandler.h"
#include "ChoiceLief.h"

#define IDC_LIEFCHOICE 4032
#define IDC_ZAHL_KONDCHOICE 4033
#define IDC_BLZCHOICE 4034

// CBasePage2 dialog

class CBasePage2 : public CDbPropertyPage,
				   CUpdateHandler 	
{
	DECLARE_DYNAMIC(CBasePage2)

public:
	CBasePage2(CWnd* pParent = NULL);   // standard constructor
	virtual ~CBasePage2();

// Dialog Data
	enum { IDD = IDD_BASE_PAGE2 };

protected:
	CChoiceZahlKond *ChoiceZahlKond;
	CChoiceBlz *ChoiceBlz;


	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog ();
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnDestroy ();

public:
	KST_CLASS Kst;
public:	
	CUpdateHandler *UpdateHandler;
public:
	CStatic m_HeadBorder;
public:
	CEdit m_Mdn;
public:
	CStatic m_LMdn;
public:
	CEdit m_MdnName;
public:
	CStatic m_LFil;
public:
	CEdit m_Fil;
public:
	CEdit m_FilName;
public:
	CStatic m_LLief;
public:
	CEdit m_Lief;
	CColorButton m_LiefChoice;
public:
	CEdit m_LiefName;
public:
	CStatic m_DataBorder;
public:
	CStatic m_LEgBetriebsnr;
public:
	CTextEdit m_EgBetriebsnr;
public:
	CStatic m_LSteuerKz;
public:
	CComboBox m_SteuerKz;
public:
	CButton m_OrdrKz;
public:
	CStatic m_LAbr;
public:
	CComboBox m_Abr;
public:
	CStatic m_LMahnStu;
public:
	CNumEdit m_MahnStu;
public:
	CStatic m_LRechKz;
public:
	CComboBox m_RechKz;
public:
	CStatic m_LRechToler;
public:
	CDecimalEdit m_RechToler;
public:
	CStatic m_LWeErfKz;
public:
	CComboBox m_WeErfKz;
public:
	CStatic m_LWePrKz;
public:
	CComboBox m_WePrKz;
public:
	CStatic m_LInk;
public:
	CNumEdit m_Ink;
public:
	CStatic m_ZahlKondGroup;
public:
	CStatic m_LZahlw;
public:
	CComboBox m_Zahlw;
public:
	CStatic m_LZahlKond;
public:
	CNumEdit m_ZahlKond;
	CColorButton m_ZahlKondChoice;
	CColorButton m_EnterZahlKond;
public:
	CNumEdit m_Ziel1;
public:
	CNumEdit m_Ziel2;
public:
	CNumEdit m_Ziel3;
public:
	CTextEdit m_Txt;
public:
	CStatic m_LZiel1;
public:
	CStatic m_LZiel2;
public:
	CStatic m_LZiel3;
public:
	CDecimalEdit m_Skto1;
public:
	CDecimalEdit m_Skto2;
public:
	CDecimalEdit m_Skto3;
public:
	CStatic m_LSkto1;
public:
	CStatic m_LSkto2;
public:
	CStatic m_LSkto3;
public:
	CStatic m_LViehBas;
public:
	CNumEdit m_ViehBas;
public:
	CNumEdit m_Zertifikat1;
public:
	CStatic m_LSlash;
public:
	CNumEdit m_Zertifikat2;
public:
	CStatic m_LEgKz;
public:
	CComboBox m_EgKz;
public:
	CButton m_FrachtKz;
public:
	CStatic m_LRabKz;
public:
	CComboBox m_RabKz;
public:
	CStatic m_LRabAbl;
	CColorButton m_EnterRabAbl;
	CColorButton m_EnterBoni;
public:
	CComboBox m_RabAbl;
public:
	CStatic m_LListNr;
public:
	CNumEdit m_ListNr;
public:
	CStatic m_LMinZuschProz;
public:
	CDecimalEdit m_MinZuschProz;
public:
	CStatic m_LLiefZusch;
public:
	CDecimalEdit m_LiefZusch;
public:
	CStatic m_LVorkgr;
public:
	CNumEdit m_Vorkgr;
public:
	CStatic m_LHhdklpargr;
public:
	CNumEdit m_Hhdklpargr;

public:
	CStatic m_DataBorder2;
public:
	CStatic m_BankGroup;
public:
	CStatic m_LTxt;
public:
	CStatic m_LBlz;
public:
	CDecimalEdit m_Blz;
	CColorButton m_BlzChoice;
public:
	CStatic m_LBankNam;
public:
	CEdit m_BankNam;
public:
	CStatic m_LIban;
public:
	CDecimalEdit m_Iban1;
public:
	CDecimalEdit m_Iban2;
public:
	CDecimalEdit m_Iban3;
public:
	CDecimalEdit m_Iban4;
public:
	CDecimalEdit m_Iban5;
public:
	CDecimalEdit m_Iban6;
public:
	CDecimalEdit m_Iban7;
public:
	CStatic m_LKto;
public:
	CDecimalEdit m_Kto;
public:
	CStatic m_LUstId;
public:
	CEdit m_UstId;
public:
	CStatic m_LSondKond;
public:
	CEdit m_SondKond;

	CFormTab Form;

	CFont Font;
	CFont lFont;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid FilGrid;
	CCtrlGrid LiefGrid;
	CCtrlGrid ZahlKondGrid;
	CCtrlGrid BankGrid;
	CCtrlGrid BlzGrid;
	CCtrlGrid IbanGrid;

	void FillPtabCombo (CWnd *Control, LPTSTR Item);
	void FillKosten ();
    BOOL OnReturn ();
    BOOL OnKeyup ();

public:
	CStatic m_LMinZusch;
public:
	CDecimalEdit m_MinZusch;

	virtual BOOL OnSetActive ();
	virtual void Update ();
	virtual void UpdateWrite ();
public:
	afx_msg void OnEnKillfocusZahlKond();
	afx_msg void OnChoice ();
	afx_msg void OnZahlKondchoice ();
	afx_msg void OnEnterZahlKond ();
	afx_msg void OnBlzchoice ();
	afx_msg void OnEnterRabAbl ();
	afx_msg void OnEnterBoni ();
	CStatic m_LMwst;
	CComboBox m_Mwst;
	CStatic m_LSteuerNummer;
	CEdit m_SteuerNummer;
	afx_msg void OnCbnSelchangeSteuerKz();
	CStatic m_LKosten;
	CComboBox m_Kosten;
	CEdit m_Iban;
	CStatic m_LBIC;
	CEdit m_BIC;
	CStatic m_LTageBis;
	CNumEdit m_TageBis;
};
