// ChildFrm.cpp : Implementierung der Klasse CChildFrame
//
#include "stdafx.h"
#include "16400.h"

#include "ChildFrm.h"
#include "windowhandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
//	ON_WM_CTLCOLOR ()
	ON_COMMAND(ID_DOCK_ENABLED, &CChildFrame::OnDockEnabled)
	ON_COMMAND(ID_DOCK_ALWAYS, &CChildFrame::OnDockAlways)
	ON_COMMAND(ID_DOCK_CHOICE, &CChildFrame::OnDockChoice)
	ON_COMMAND(ID_DOCK_BOTTOM, &CChildFrame::OnDockBottom)
END_MESSAGE_MAP()


// CChildFrame-Erstellung/Zerst�rung

CChildFrame::CChildFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
	DlgBkColor = RGB (235, 235, 245);
	DlgBrush = NULL;
}

CChildFrame::~CChildFrame()
{
}


BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie die Fensterklasse oder die Stile hier, indem Sie CREATESTRUCT �ndern
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}


// CChildFrame-Diagnose

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// CChildFrame-Meldungshandler

BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	BOOL ret = TRUE;
	ret = DistributorSplitter.Create (this, 2, 2, CSize (100, 100),pContext);
	WINDOWHANDLER->SetSplitter (&DistributorSplitter);

	MDIMaximize ();

	return ret;
}

void CChildFrame::OnDockEnabled()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	BOOL mode = TRUE;
	CMDIFrameWnd *frame = this->GetMDIFrame ();
	if (frame != NULL)
	{
		CMenu *menu = frame->GetMenu ();
		if (menu != NULL)
		{
			int state = menu->GetMenuState (ID_DOCK_ENABLED, MF_BYCOMMAND);
			if (state == MF_CHECKED)
			{
				menu->CheckMenuItem (ID_DOCK_ENABLED, MF_BYCOMMAND | MF_UNCHECKED);
				mode = FALSE;
			}
			else
			{
				menu->CheckMenuItem (ID_DOCK_ENABLED, MF_BYCOMMAND | MF_CHECKED);
				mode = TRUE;
			}
		}
	}
	WINDOWHANDLER->SetDockChoice (mode);
}

void CChildFrame::OnDockAlways()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	BOOL mode = TRUE;
	CMDIFrameWnd *frame = this->GetMDIFrame ();
	if (frame != NULL)
	{
		CMenu *menu = frame->GetMenu ();
		if (menu != NULL)
		{
			int state = menu->GetMenuState (ID_DOCK_ALWAYS, MF_BYCOMMAND);
			if (state == MF_CHECKED)
			{
				menu->CheckMenuItem (ID_DOCK_ALWAYS, MF_BYCOMMAND | MF_UNCHECKED);
				mode = FALSE;
			}
			else
			{
				menu->CheckMenuItem (ID_DOCK_ALWAYS, MF_BYCOMMAND | MF_CHECKED);
				mode = TRUE;
			}
		}
	}
	WINDOWHANDLER->StartWithDock (mode);
}

void CChildFrame::OnDockChoice()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CMDIFrameWnd *frame = this->GetMDIFrame ();
	if (frame != NULL)
	{
		CMenu *menu = frame->GetMenu ();
		if (WINDOWHANDLER->DockChoice () && (WINDOWHANDLER->SplitDir () == CWindowHandler::Horizontal))
		{
			menu->CheckMenuItem (ID_DOCK_CHOICE, MF_UNCHECKED);
			WINDOWHANDLER->DeleteChoice ();
		}
		else
		{
			menu->CheckMenuItem (ID_DOCK_BOTTOM, MF_UNCHECKED);
			menu->CheckMenuItem (ID_DOCK_CHOICE, MF_CHECKED);
			WINDOWHANDLER->SplitChoice ();
		}
	}
}

void CChildFrame::OnDockBottom()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CMDIFrameWnd *frame = this->GetMDIFrame ();
	if (frame != NULL)
	{
		CMenu *menu = frame->GetMenu ();
		if (WINDOWHANDLER->DockChoice () && (WINDOWHANDLER->SplitDir () == CWindowHandler::Vertical))
		{
			menu->CheckMenuItem (ID_DOCK_BOTTOM, MF_UNCHECKED);
			WINDOWHANDLER->DeleteBottomChoice ();
		}
		else
		{
			menu->CheckMenuItem (ID_DOCK_CHOICE, MF_UNCHECKED);
			menu->CheckMenuItem (ID_DOCK_BOTTOM, MF_CHECKED);
			WINDOWHANDLER->SplitBottomChoice ();
		}
	}
}


HBRUSH CChildFrame::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CMDIChildWnd::OnCtlColor (pDC, pWnd,nCtlColor);
}
