#include "StdAfx.h"
#include "ctrlinfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

BOOL CCtrlInfo::NewStyle = FALSE;

IMPLEMENT_DYNAMIC(CCtrlInfo, CObject)
CCtrlInfo::CCtrlInfo(void)
{
	gridx = 0;
	gridy = 0;
	gridcx = 1;
	gridcy = 1;
	xplus = 0;
	yplus = 0;
	cxplus = 0;
	cyplus = 0;
	rightspace = 0;
	cWnd = NULL;
	Grid = NULL;
	width = 10;
	RightDockControl = NULL;
	pWidth = 0;
	Direction = HORIZONTAL;
	Parent = NULL;
	BottomControl = NULL;
	BottomSpace = 0;
}

CCtrlInfo::CCtrlInfo(CWnd *cWnd, int gridx, int gridy, 
					             int gridcx, int gridcy)
{
	this->cWnd = cWnd;
	xplus = 0;
	yplus = 0;
	cxplus = 0;
	cyplus = 0;
	SetPos (gridx, gridy, gridcx, gridcy);
	rightspace = 0;
	RightDockControl = NULL;
	Grid = NULL;
	SetWidth ();
	pWidth = 0;
	Direction = HORIZONTAL;
	if (NewStyle)
	{
		if (cWnd->IsKindOf (RUNTIME_CLASS (CEdit)))
		{
			long styleex = GetWindowLong (cWnd->m_hWnd, GWL_EXSTYLE);
			styleex &= ~WS_EX_CLIENTEDGE;
			SetWindowLong (cWnd->m_hWnd, GWL_EXSTYLE, styleex);
			long style = GetWindowLong (cWnd->m_hWnd, GWL_STYLE);
			style |= WS_BORDER;
			SetWindowLong (cWnd->m_hWnd, GWL_STYLE, style);
		}
		else if (cWnd->IsKindOf (RUNTIME_CLASS (CButton)))
		{
			long style = GetWindowLong (cWnd->m_hWnd, GWL_STYLE);
			style |= BS_FLAT;
			SetWindowLong (cWnd->m_hWnd, GWL_STYLE, style);
		}
	}
	Parent = NULL;
	BottomControl = NULL;
	BottomSpace = 0;
}

CCtrlInfo::CCtrlInfo(void *Grid, int gridx, int gridy, 
					             int gridcx, int gridcy)
{
	this->cWnd = NULL;
	xplus = 0;
	yplus = 0;
	cxplus = 0;
	cyplus = 0;
	SetPos (gridx, gridy, gridcx, gridcy);
	rightspace = 0;
	RightDockControl = NULL;
	this->Grid = Grid;
	SetWidth ();
	pWidth = 0;
	Direction = HORIZONTAL;
	Parent = NULL;
	BottomControl = NULL;
	BottomSpace = 0;
}

CCtrlInfo::CCtrlInfo(CWnd *pParent, int width, int gridx, int gridy, 
 		             int gridcx, int gridcy)
{
	Space.Create (NULL, WS_CHILD | WS_VISIBLE, CRect (0, 0, width, 12),
		         pParent);
	this->cWnd = &Space;
	xplus = 0;
	yplus = 0;
	cxplus = 0;
	cyplus = 0;
	SetPos (gridx, gridy, gridcx, gridcy);
	rightspace = 0;
	RightDockControl = NULL;
	Grid = NULL;
	SetWidth ();
	pWidth = 0;
	Direction = HORIZONTAL;
	Parent = NULL;
	BottomControl = NULL;
	BottomSpace = 0;
}


CCtrlInfo::~CCtrlInfo(void)
{
}

void CCtrlInfo::Create(CWnd *cWnd, int gridx, int gridy, 
					             int gridcx, int gridcy)
{
	this->cWnd = cWnd;
	SetPos (gridx, gridy, gridcx, gridcy);
}

void CCtrlInfo::SetPos(int gridx, int gridy, 
					   int gridcx, int gridcy)
{
	this->gridx = gridx;
	this->gridy = gridy;
	this->gridcx = gridcx;
	this->gridcy = gridcy;
}

void CCtrlInfo::SetCellPos (int xplus, int yplus)
{
	this->xplus = xplus;
	this->yplus = yplus;
}

void CCtrlInfo::SetCellPos (int xplus, int yplus, int cxplus, int cyplus)
{
	this->xplus = xplus;
	this->yplus = yplus;
	this->cxplus = cxplus;
	this->cyplus = cyplus;
}

void CCtrlInfo::SetWidth ()
{
	TEXTMETRIC tm;
	if (cWnd == NULL) return;

	CFont *font = cWnd->GetFont ();

	CDC *cDC = cWnd->GetDC ();
	cDC->SelectObject (font);
	CSize size = cDC->GetTextExtent (_T("X"), 1);
	cDC->GetTextMetrics (&tm);
	cWnd->ReleaseDC (cDC);

	FontSize = size;
	tmHeight = tm.tmHeight;
	tmWidth  = tm.tmAveCharWidth;
	CRect rect;
	cWnd->GetClientRect (&rect);
	width = rect.right / tm.tmAveCharWidth;
}

void CCtrlInfo::SetFont (CFont *font)
{
	TEXTMETRIC tm;
	if (cWnd == NULL) return;

	cWnd->SetFont (font);
	cWnd->SetFont (font);
	CDC *cDC = cWnd->GetDC ();
	cDC->SelectObject (font);
	CSize size = cDC->GetTextExtent (_T("X"), 1);
	cDC->GetTextMetrics (&tm);
	cWnd->ReleaseDC (cDC);

	double fWidth  = (double)  size.cx / FontSize.cx;  
	double fHeight = (double)  size.cy / FontSize.cy;  

	CRect rect;
	cWnd->GetClientRect (&rect);
	rect.right  = (int) (double) (fWidth * rect.right + 0.5);
	rect.top    = (int) (double) (fHeight * rect.top + 0.5);
	rect.bottom = (int) (double) (fHeight * rect.bottom + 0.5);
	cWnd->MoveWindow (&rect, FALSE);
}

void CCtrlInfo::SetNewStyle ()
{
	if (NewStyle)
	{
		if (cWnd->IsKindOf (RUNTIME_CLASS (CEdit)))
		{
			cWnd->ShowWindow (SW_HIDE);
			long styleex = GetWindowLong (cWnd->m_hWnd, GWL_EXSTYLE);
			styleex &= ~WS_EX_CLIENTEDGE;
			SetWindowLong (cWnd->m_hWnd, GWL_EXSTYLE, styleex);
			long style = GetWindowLong (cWnd->m_hWnd, GWL_STYLE);
			style |= WS_BORDER;
			SetWindowLong (cWnd->m_hWnd, GWL_STYLE, style);
			cWnd->Invalidate ();
			cWnd->ShowWindow (SW_NORMAL);
		}
		else if (cWnd->IsKindOf (RUNTIME_CLASS (CButton)))
		{
			long style = GetWindowLong (cWnd->m_hWnd, GWL_STYLE);
			style |= BS_FLAT;
			SetWindowLong (cWnd->m_hWnd, GWL_STYLE, style);
			cWnd->Invalidate ();
		}
	}
	else
	{
		if (cWnd->IsKindOf (RUNTIME_CLASS (CEdit)))
		{
			long styleex = GetWindowLong (cWnd->m_hWnd, GWL_EXSTYLE);
			styleex |= WS_EX_CLIENTEDGE;
			SetWindowLong (cWnd->m_hWnd, GWL_EXSTYLE, styleex);
			long style = GetWindowLong (cWnd->m_hWnd, GWL_STYLE);
			style &= ~WS_BORDER;
			SetWindowLong (cWnd->m_hWnd, GWL_STYLE, style);
			cWnd->Invalidate ();
		}
		else if (cWnd->IsKindOf (RUNTIME_CLASS (CButton)))
		{
			long style = GetWindowLong (cWnd->m_hWnd, GWL_STYLE);
			style &= ~BS_FLAT;
			SetWindowLong (cWnd->m_hWnd, GWL_STYLE, style);
			cWnd->Invalidate ();
		}
	}
}
void CCtrlInfo::SetBottomControl (CWnd *Parent, CWnd *Control, int Space, int Side)
{
	this->Parent = Parent;
	BottomControl = Control;
	BottomSpace = Space;
	BottomSide = Side;
}

void CCtrlInfo::SetBottom ()
{
	CRect cRect;
	CRect Rect;

	if (BottomControl == NULL) return;

	if (Parent == NULL)
	{
		Parent = cWnd->GetParent ();
	}
	if (Parent == NULL)
	{
		return;
	}
	BottomControl->GetWindowRect (&cRect);
	Parent->ScreenToClient (&cRect);
	cWnd->GetWindowRect (&Rect);
	Parent->ScreenToClient (&Rect);

	if (BottomSide == Bottom)
	{
		Rect.bottom = cRect.bottom + BottomSpace;
	}
	else
	{
		Rect.bottom = cRect.top - BottomSpace;
	}
	cWnd->MoveWindow (&Rect);
}


void CCtrlInfo::GetWindowRect (CRect& rect)
{
	if (cWnd == NULL)
	{
		return;
	}
	cWnd->GetWindowRect (&rect);
	if (cWnd->IsKindOf (RUNTIME_CLASS (CStatic)))
	{
		CFont *font = cWnd->GetFont ();
		CDC *cDc = cWnd->GetDC ();
		CFont *oldFont = cDc->SelectObject (font);
		CString text;
		((CStatic *) cWnd)->GetWindowText (text);
		CSize size = cDc->GetTextExtent (text);
		cDc->SelectObject (oldFont);
		cWnd->ReleaseDC (cDc);
		int width  = rect.right - rect.left;
		if (width < size.cx + 4)
		{
			rect.right = rect.left + size.cx + 4;
		}
		int height  = rect.bottom - rect.top;
		if (height < size.cy + 4)
		{
			rect.bottom = rect.top + size.cy + 4;
		}
	}
}

void CCtrlInfo::RepaintText ()
{
	if (cWnd->IsKindOf (RUNTIME_CLASS (CStatic)))
	{
		CString text;
		cWnd->GetWindowText (text);
		cWnd->SetWindowText (text);
	}

}


