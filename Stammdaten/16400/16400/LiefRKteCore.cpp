#include "StdAfx.h"
#include "LiefRKteCore.h"
#include "DbTime.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CLiefRKteCore *CLiefRKteCore::Instance = NULL; 

CLiefRKteCore::CLiefRKteCore(void)
{
	CLIEF_R_KTE.sqlin ((short *) &TLIEF_R_KTE.mdn, SQLSHORT, 0);
	CLIEF_R_KTE.sqlin ((short *) &TLIEF_R_KTE.fil, SQLSHORT, 0);
	CLIEF_R_KTE.sqlin ((char *)  TLIEF_R_KTE.lief, SQLCHAR, sizeof (TLIEF_R_KTE.lief));
	CLIEF_R_KTE.sqlout ((long *) &TLIEF_R_KTE.lfd, SQLLONG, 0);
	m_Cursor = CLIEF_R_KTE.sqlcursor (_T("select lfd from lief_r_kte ")
									  _T("where mdn = ? ")
									  _T("and fil = ? ")
									  _T("and lief = ? ")
									  _T("order by lfd"));
	CLIEF_R_KTE.sqlin ((short *) &TLIEF_R_KTE.mdn, SQLSHORT, 0);
	CLIEF_R_KTE.sqlin ((short *) &TLIEF_R_KTE.fil, SQLSHORT, 0);
	CLIEF_R_KTE.sqlin ((char *)  TLIEF_R_KTE.lief, SQLCHAR, sizeof (TLIEF_R_KTE.lief));
	m_DeleteCursor = CLIEF_R_KTE.sqlcursor (_T("delete from lief_r_kte ")
											_T("where mdn = ? ")
											_T("and fil = ? ")
											_T("and lief = ? "));
	m_ListRow = NULL;
	m_ProzSum = 0.0;
}

CLiefRKteCore::~CLiefRKteCore(void)
{
	CLIEF_R_KTE.sqlclose (m_Cursor);
	m_LiefRKteTab.DestroyElements ();
	m_LiefRKteTab.Destroy ();
}

CLiefRKteCore *CLiefRKteCore::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CLiefRKteCore ();
	}
	return Instance;
}

void CLiefRKteCore::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

BOOL CLiefRKteCore::Read ()
{
	BOOL ret = FALSE;
	int dsqlstatus;
	m_LiefRKteTab.DestroyElements ();

	if (m_ListRow != NULL)
	{
		dsqlstatus = CLIEF_R_KTE.sqlopen (m_Cursor);
		if (dsqlstatus == 0)
		{
			while (CLIEF_R_KTE.sqlfetch (m_Cursor) == 0)
			{
				ret = TRUE;
				DATABASE->ReadLiefRKte ();
				m_ListRow->Add ();
				LIEF_R_KTE *tabrow = new LIEF_R_KTE;
				memcpy (tabrow, &TLIEF_R_KTE, sizeof (LIEF_R_KTE)); 
				m_LiefRKteTab.Add (tabrow);
			}
		}
		CalcProzSum ();
		m_ListRow->Update ();
	}
	return ret;
}

BOOL CLiefRKteCore::Write ()
{
	BOOL ret = TRUE;
	LIEF_R_KTE **it;
	LIEF_R_KTE *tabrow;

	CalcProzSum ();
	m_LiefRKteTab.Start ();
	while ((it = m_LiefRKteTab.GetNext ()) != NULL)
	{
		tabrow = *it;
		if (tabrow != NULL)
		{
			if (tabrow->rab_proz > 999.99 ||
				tabrow->rab_proz < -999.99)
			{
				ret = FALSE;
				break;
			}
		}
	}

	if (ret)
	{
		CLIEF_R_KTE.sqlexecute (m_DeleteCursor);
		long lfd = 1;
		m_LiefRKteTab.Start ();
		while ((it = m_LiefRKteTab.GetNext ()) != NULL)
		{
			tabrow = *it;
			if (tabrow != NULL && tabrow->rab_proz != 0.0)
			{
				memcpy (&TLIEF_R_KTE, &lief_r_kte_null, sizeof (LIEF_R_KTE));
				TLIEF_R_KTE.mdn = TLIEF.mdn;
				TLIEF_R_KTE.fil = TLIEF.fil;
				_tcscpy (TLIEF_R_KTE.lief, TLIEF.lief);
				TLIEF_R_KTE.lfd = lfd;
				TLIEF_R_KTE.rab_proz = tabrow->rab_proz;
				_tcscpy (TLIEF_R_KTE.rab_bz, tabrow->rab_bz);
				TLIEF_R_KTE.proz_sum = m_ProzSum;
				TLIEF_R_KTE.von_dat = DbTime (&TLIEF_R_KTE.von_dat);   
				TLIEF_R_KTE.bis_dat = DbTime (&TLIEF_R_KTE.bis_dat);   
				TLIEF_R_KTE.ae_dat = DbTime (&TLIEF_R_KTE.ae_dat);   
				memcpy (tabrow, &TLIEF_R_KTE, sizeof (LIEF_R_KTE));
				TLIEF_R_KTE.lief_s = _tstol (TLIEF_R_KTE.lief);
				CLIEF_R_KTE.dbupdate ();
				lfd ++;
			}
		}
	}
	return ret;
}


long CLiefRKteCore::NextLfd ()
{
	long lfd = 0l;
	int rows = m_LiefRKteTab.anz;
	if (rows > 0)
	{
		LIEF_R_KTE *row = *m_LiefRKteTab.Get (rows - 1);
		if (row != NULL)
		{
			lfd = row->lfd;
		}
	}
	return ++lfd;
}

void CLiefRKteCore::CalcProzSum ()
{
	LIEF_R_KTE **it;
	LIEF_R_KTE *tabrow;

	m_ProzSum = 1.0;
	m_LiefRKteTab.Start ();
	while ((it = m_LiefRKteTab.GetNext ()) != NULL)
	{
		tabrow = *it;
		if (tabrow != NULL)
		{
			m_ProzSum *= (1.0 - tabrow->rab_proz / 100);
		}
	}
	m_ProzSum = (1 - m_ProzSum) * 100.0;
}

void CLiefRKteCore::UpdateRow (int row)
{
	int rows = m_LiefRKteTab.anz;
	if (row >= rows)
	{
		LIEF_R_KTE *tabrow = new LIEF_R_KTE;
		memcpy (tabrow, &TLIEF_R_KTE, sizeof (LIEF_R_KTE)); 
		m_LiefRKteTab.Add (tabrow);
	}
	else
	{
	    LIEF_R_KTE *tabrow = *m_LiefRKteTab.Get (row);
		memcpy (tabrow, &TLIEF_R_KTE, sizeof (LIEF_R_KTE)); 
	}
	CalcProzSum ();
	m_ListRow->Update ();
}

void CLiefRKteCore::DeleteRow (int row)
{
    if (m_LiefRKteTab.Get (row) != NULL)
	{
		LIEF_R_KTE *tabrow = *m_LiefRKteTab.Get (row);
		m_LiefRKteTab.Drop (row);
		delete tabrow;
		CalcProzSum ();
		m_ListRow->Update ();
	}
}
