#pragma once
#include "afxdtctl.h"

class CListDateTimeCtrl :
	public CDateTimeCtrl
{
public:
	CListDateTimeCtrl(void);
	~CListDateTimeCtrl(void);
protected :
	DECLARE_MESSAGE_MAP()
    afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};
