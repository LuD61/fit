#ifndef _LIEF_R_STF_DEF
#define _LIEF_R_STF_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LIEF_R_STF {
   double         abn_wrt;
   short          fil;
   long           lfd;
   TCHAR          lief[17];
   long           lief_s;
   short          mdn;
   double         rab_proz;
};
extern struct LIEF_R_STF lief_r_stf, lief_r_stf_null;

#line 8 "Lief_r_stf.rh"

class LIEF_R_STF_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LIEF_R_STF lief_r_stf;  
               LIEF_R_STF_CLASS () : DB_CLASS ()
               {
               }
};
#endif
