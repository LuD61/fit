#include "StdAfx.h"
#include "DistributorCore.h"
#include "DatabaseCore.h"
#include "LiefRKteCore.h"
#include "LiefRStfCore.h"
#include "LiefRLisCore.h"
#include "LiefRGrCore.h"
#include "LiefBoniCore.h"
#include "DbUniCode.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CDistributorCore *CDistributorCore::Instance = NULL; 

CDistributorCore::CDistributorCore(void)
{
	m_FilKeys  = NULL;
	m_LiefKeys = NULL;
}

CDistributorCore::~CDistributorCore(void)
{
	if (m_FilKeys != NULL)
	{
		delete m_FilKeys;
	}
	if (m_LiefKeys != NULL)
	{
		delete m_LiefKeys;
	}
}

CDistributorCore *CDistributorCore::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CDistributorCore ();
	}
	return Instance;
}

void CDistributorCore::DestroyInstance ()
{
	LIEF_R_KTE_CORE->DestroyInstance ();
	LIEF_R_STF_CORE->DestroyInstance ();
	LIEF_R_STF_CORE->DestroyInstance ();
	LIEF_R_LIS_CORE->DestroyInstance ();
	LIEF_R_GR_CORE->DestroyInstance ();
	LIEF_BONI_CORE->DestroyInstance ();
	DATABASE->DestroyInstance ();
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

void CDistributorCore::Opendbase(void)
{
	DATABASE->Opendbase ();
}

BOOL CDistributorCore::ReadMdn ()
{
	int ret = FALSE;
	m_Mdn = DATABASE->Mdn.mdn.mdn;
	DATABASE->InitMdn ();
	DATABASE->Mdn.mdn.mdn = m_Mdn;
	if (m_Mdn == 0)
	{
		_tcscpy (DATABASE->MdnAdr.adr.adr_krz, _T("Zentrale"));
		ret = TRUE;
	}
	else
	{
		ret = DATABASE->ReadMdn ();
	}
	return ret;
}

BOOL CDistributorCore::ReadFil ()
{
	int ret = FALSE;
    DATABASE->Fil.fil.mdn = DATABASE->Mdn.mdn.mdn;
	m_FilKeys = new FilKeys (DATABASE->Fil.fil.mdn,
							  DATABASE->Fil.fil.fil);
	DATABASE->InitFil ();
	m_FilKeys->Restore (&DATABASE->Fil.fil.mdn,
					    &DATABASE->Fil.fil.fil);
	if (DATABASE->Fil.fil.fil == 0)
	{
		_tcscpy (DATABASE->FilAdr.adr.adr_krz, _T("Mandant"));
		ret = TRUE;
	}
	else
	{
		ret = DATABASE->ReadFil ();
	}
	delete m_FilKeys;
	m_FilKeys = NULL;
	return ret;
}

BOOL CDistributorCore::ReadAdr ()
{
	int ret = FALSE;
    m_Adr = DATABASE->LiefAdr.adr.adr;
	DATABASE->InitAdr ();
    DATABASE->LiefAdr.adr.adr = m_Adr;
	ret = DATABASE->ReadAdr ();
	return ret;
}

BOOL CDistributorCore::ReadPtabn ()
{
	int ret = FALSE;
	int dsqlstatus;
	CString PtItem;
	CString PtWert;
	PtItem = DATABASE->Ptabn.ptabn.ptitem;
	PtWert = DATABASE->Ptabn.ptabn.ptwert;
	memcpy (&DATABASE->Ptabn.ptabn, &ptabn_null, sizeof (PTABN));
	strcpy (DATABASE->Ptabn.ptabn.ptitem, PtItem.GetBuffer ());
	strcpy (DATABASE->Ptabn.ptabn.ptwert, PtWert.GetBuffer ());
	dsqlstatus = DATABASE->Ptabn.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	return ret;
}

BOOL CDistributorCore::ReadZahlKond ()
{
	int ret = FALSE;
	int dsqlstatus;
	short zahl_kond;
	zahl_kond = TZAHL_KOND.zahl_kond;
	memcpy (&TZAHL_KOND, &zahl_kond_null, sizeof (ZAHL_KOND));
	TZAHL_KOND.zahl_kond = zahl_kond;
	dsqlstatus = CZAHL_KOND.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	return ret;
}

BOOL CDistributorCore::WriteZahlKond ()
{
	int ret = FALSE;
	int dsqlstatus;
	dsqlstatus = CZAHL_KOND.dbupdate ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	return ret;
}
void CDistributorCore::SaveLief ()
{
	if (m_LiefKeys != NULL)
	{
		delete m_LiefKeys;
	}
    DATABASE->Lief.lief.mdn = DATABASE->Mdn.mdn.mdn;
    DATABASE->Lief.lief.fil = DATABASE->Fil.fil.fil;
	m_LiefKeys = new LiefKeys (DATABASE->Lief.lief.mdn,
						 	   DATABASE->Lief.lief.fil,
						 	   DATABASE->Lief.lief.lief);
}

void CDistributorCore::RestoreLief ()
{
	if (m_LiefKeys != NULL)
	{
		m_LiefKeys->Restore (&DATABASE->Lief.lief.mdn,
					 		 &DATABASE->Lief.lief.fil,
							 DATABASE->Lief.lief.lief);
	}
}

BOOL CDistributorCore::ReadLief ()
{
	LiefKeys *m_LiefKeys;
    DATABASE->Lief.lief.mdn = DATABASE->Mdn.mdn.mdn;
    DATABASE->Lief.lief.fil = DATABASE->Fil.fil.fil;
	m_LiefKeys = new LiefKeys (DATABASE->Lief.lief.mdn,
						 	   DATABASE->Lief.lief.fil,
						 	   DATABASE->Lief.lief.lief);
	DATABASE->InitLief ();
	m_LiefKeys->Restore (&DATABASE->Lief.lief.mdn,
					 	 &DATABASE->Lief.lief.fil,
						 DATABASE->Lief.lief.lief);
	delete m_LiefKeys;
	m_LiefKeys = NULL;
	return DATABASE->ReadLief ();
}

BOOL CDistributorCore::WriteLief ()
{
	return DATABASE->WriteLief ();
}

BOOL CDistributorCore::DeleteLief ()
{
	BOOL ret = TRUE;
	if (CLIEF.dbdelete () != 0)
	{
		ret = FALSE;
	}
	else
	{
		DATABASE->InitLief ();
	}
	return ret;
}

void CDistributorCore::FillPtabCombo (std::vector<CString *> & ComboValues, LPTSTR Item)
{
		ComboValues.clear ();
		CPTABN.sqlout ((char *) TPTABN.ptitem, SQLCHAR, sizeof (TPTABN.ptitem)); 
		CPTABN.sqlout ((char *) TPTABN.ptwert, SQLCHAR, sizeof (TPTABN.ptwert)); 
		CPTABN.sqlout ((long *) &TPTABN.ptlfnr, SQLLONG, 0); 

		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			        _T("where ptitem = \"%s\" ")
				    _T("order by ptlfnr"), Item);

        int cursor = CPTABN.sqlcursor (Sql.GetBuffer ());
		while (CPTABN.sqlfetch (cursor) == 0)
		{
			CPTABN.dbreadfirst ();

			LPSTR pos = (LPSTR) TPTABN.ptwert;
			CDbUniCode::DbToUniCode (TPTABN.ptwert, pos);
			pos = (LPSTR) TPTABN.ptbez;
			CDbUniCode::DbToUniCode (TPTABN.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), TPTABN.ptwert,
				                             TPTABN.ptbez);
		    ComboValues.push_back (ComboValue); 
		}
		CPTABN.sqlclose (cursor);
}

