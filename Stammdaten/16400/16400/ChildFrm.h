// ChildFrm.h : Schnittstelle der Klasse CChildFrame
//


#pragma once
#include "MainSplitter.h"


class CChildFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CChildFrame)
public:
	CChildFrame();

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementierung
public:
	virtual ~CChildFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generierte Funktionen f�r die Meldungstabellen
protected:
	CMainSplitter DistributorSplitter;
	DECLARE_MESSAGE_MAP()
	BOOL OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
public:
	afx_msg void OnDockEnabled();
public:
	afx_msg void OnDockAlways();
	afx_msg void OnDockChoice();
	afx_msg void OnDockBottom();
private:
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
};
