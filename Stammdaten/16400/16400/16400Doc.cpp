// 16400Doc.cpp : Implementierung der Klasse CMy16400Doc
//

#include "stdafx.h"
#include "16400.h"

#include "16400Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMy16400Doc

IMPLEMENT_DYNCREATE(CMy16400Doc, CDocument)

BEGIN_MESSAGE_MAP(CMy16400Doc, CDocument)
END_MESSAGE_MAP()


// CMy16400Doc-Erstellung/Zerst�rung

CMy16400Doc::CMy16400Doc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CMy16400Doc::~CMy16400Doc()
{
}

BOOL CMy16400Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CMy16400Doc-Serialisierung

void CMy16400Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CMy16400Doc-Diagnose

#ifdef _DEBUG
void CMy16400Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMy16400Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMy16400Doc-Befehle
