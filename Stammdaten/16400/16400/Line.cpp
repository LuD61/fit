#include "StdAfx.h"
#include "line.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CLine::CLine(void)
{
	this->Direction = HORIZONTAL;
}

CLine::~CLine(void)
{
}

void CLine::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	HDC hDC = lpDrawItemStruct->hDC;
	CDC cDC;
	cDC.Attach (hDC);
	CPen blackPen (PS_SOLID, 1, RGB (120, 120, 120));
	CPen grayPen (PS_SOLID, 2, RGB (190, 190, 190));
	CPen whitePen (PS_SOLID, 1, RGB (255, 255, 255));
	CPen * oldPen = (CPen *) cDC.SelectObject (&blackPen);
	CRect rect;
	GetClientRect (&rect);
	if (Direction == HORIZONTAL)
	{
		cDC.MoveTo (0,0);
		cDC.LineTo (rect.right, 0);
		cDC.SelectObject (&grayPen);
		cDC.SelectObject (&whitePen);
		cDC.MoveTo (0,1);
		cDC.LineTo (rect.right, 1);
	}
	else
	{
		cDC.MoveTo (0,0);
		cDC.LineTo (0, rect.bottom);
		cDC.SelectObject (&grayPen);
		cDC.SelectObject (&whitePen);
		cDC.MoveTo (1,0);
		cDC.LineTo (1, rect.bottom);
	}
	cDC.SelectObject (oldPen);
}
