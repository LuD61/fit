// BasePage.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16400.h"
#include "BasePage.h"
#include "databasecore.h"
#include "distributorcore.h"
#include "uniformfield.h"
#include "WindowHandler.h"
#include "Process.h"
#include "Util.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CBasePage-Dialogfeld

IMPLEMENT_DYNAMIC(CBasePage, CDbPropertyPage)

CBasePage::CBasePage()
	: CDbPropertyPage(CBasePage::IDD)
{
	m_CanWrite = FALSE;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_Fil);
	HeadControls.Add (&m_Lief);
	Choice = NULL;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ChoiceFil = NULL;
	ModalChoiceFil = TRUE;
	ChoiceAdr = NULL;
	ModalChoiceAdr = TRUE;
	IncludeLand = TRUE;
	WINDOWHANDLER->SetBase (this);
	WINDOWHANDLER->RegisterUpdate (this);
}

CBasePage::~CBasePage()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
	HeadControls.Destroy ();
	Choice = (CChoiceLief *) WINDOWHANDLER->GetActiveChoice ();
	if (Choice != NULL && IsWindow (Choice->m_hWnd))
	{
		delete Choice;
	}
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
	}
	if (ChoiceFil != NULL)
	{
		delete ChoiceFil;
	}
	CORE->DestroyInstance ();
	WINDOWHANDLER->DestroyInstance ();
}


void  CBasePage::OnDestroy ()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
	HeadControls.Destroy ();
	CtrlGrid.DestroyGrid ();
}

void CBasePage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LFIL, m_LFil);
	DDX_Control(pDX, IDC_FIL, m_Fil);
	DDX_Control(pDX, IDC_FIL_NAME, m_FilName);
	DDX_Control(pDX, IDC_LLIEF, m_LLief);
	DDX_Control(pDX, IDC_LIEF, m_Lief);
	DDX_Control(pDX, IDC_LIEFNAME, m_LiefName);
	DDX_Control(pDX, IDC_HEADBORDER, m_HeadBorder);
	DDX_Control(pDX, IDC_DATABORDER, m_DataBorder);
	DDX_Control(pDX, IDC_LANR, m_LAnr);
	DDX_Control(pDX, IDC_ANR, m_Anr);
	DDX_Control(pDX, IDC_LADR, m_LAdr);
	DDX_Control(pDX, IDC_ADR, m_Adr);
	DDX_Control(pDX, IDC_ENTER_PARTNER, m_EnterPartner);
	DDX_Control(pDX, IDC_LNAME1, m_LName1);
	DDX_Control(pDX, IDC_NAME, m_Name1);
	DDX_Control(pDX, IDC_NAME2, m_Name2);
	DDX_Control(pDX, IDC_LNAME2, m_LName2);
	DDX_Control(pDX, IDC_NAME3, m_Name3);
	DDX_Control(pDX, IDC_LNAME3, m_LName3);
	DDX_Control(pDX, IDC_STRASSE, m_Strasse);
	DDX_Control(pDX, IDC_LSTRASSE, m_LStrasse);
	DDX_Control(pDX, IDC_PLZ, m_Plz);
	DDX_Control(pDX, IDC_LPLZ, m_LPlz);
	DDX_Control(pDX, IDC_ORT, m_Ort);
	DDX_Control(pDX, IDC_LORT, m_LOrt);
	DDX_Control(pDX, IDC_LPOSTFACH, m_LPostfach);
	DDX_Control(pDX, IDC_POSTFACH, m_Postfach);
	DDX_Control(pDX, IDC_TELEFON, m_Telefon);
	DDX_Control(pDX, IDC_LTELEFON, m_LTelefon);
	DDX_Control(pDX, IDC_FAX, m_Fax);
	DDX_Control(pDX, IDC_POSTFACH2, m_plzPostfach);
	DDX_Control(pDX, IDC_EMAIL, m_EMAIL);
	DDX_Control(pDX, IDC_LEMAIL, m_LEmail);
	DDX_Control(pDX, IDC_PARTNER, m_Partner);
	DDX_Control(pDX, IDC_LPARTNER, m_LPartner);
	DDX_Control(pDX, IDC_DATABORDER2, m_DataBorder2);
	DDX_Control(pDX, IDC_STAAT, m_Staat);
	DDX_Control(pDX, IDC_LSTAAT, m_LStaat);
	DDX_Control(pDX, IDC_LAND, m_Land);
	DDX_Control(pDX, IDC_INCLUDE_LAND, m_IncludeLand);
	DDX_Control(pDX, IDC_LLAND, m_LLand);
	DDX_Control(pDX, IDC_LLIEF_KUN, m_LLiefKun);
	DDX_Control(pDX, IDC_LSPRACHE, m_LSprache);
	DDX_Control(pDX, IDC_SPRACHE, m_Sprache);
	DDX_Control(pDX, IDC_LBEST_TRANS_KZ, m_LBestTransKz);
	DDX_Control(pDX, IDC_BEST_TRANS_KZ, m_BestTransKz);
	DDX_Control(pDX, IDC_LME_KZ, m_LMeKz);
	DDX_Control(pDX, IDC_ME_KZ, m_MeKz);
	DDX_Control(pDX, IDC_LLIEF_RHT, m_LLiefRht);
	DDX_Control(pDX, IDC_LIEF_RHT, m_LiefRht);
	DDX_Control(pDX, IDC_LLIEF_ZEIT, m_LLiefZeit);
	DDX_Control(pDX, IDC_LIEF_ZEIT, m_LiefZeit);
	DDX_Control(pDX, IDC_LWAEHRUNG, m_LWaehrung);
	DDX_Control(pDX, IDC_WAEHRUNG, m_Waehrung);
	DDX_Control(pDX, IDC_LWE_KONT, m_LWeKont);
	DDX_Control(pDX, IDC_WE_KONT, m_WeKont);
	DDX_Control(pDX, IDC_LWE_TOLER, m_LWeToler);
	DDX_Control(pDX, IDC_WE_TOLER, m_WeToler);
	DDX_Control(pDX, IDC_LLIEF_TYP, m_LLiefTyp);
	DDX_Control(pDX, IDC_LIEF_TYP, m_LiefTyp);
	DDX_Control(pDX, IDC_LILN, m_LIln);
	DDX_Control(pDX, IDC_ILN, m_Iln);
	DDX_Control(pDX, IDC_LLIEF_KUN, m_LLiefKun);
	DDX_Control(pDX, IDC_LIEF_KUN, m_LiefKun);
	DDX_Control(pDX, IDC_LKREDITOR, m_LKreditor);
	DDX_Control(pDX, IDC_KREDITOR, m_Kreditor);
	DDX_Control(pDX, IDC_LBEST_SORT, m_LBestSort);
	DDX_Control(pDX, IDC_BEST_SORT, m_BestSort);
	DDX_Control(pDX, IDC_NACH_LIEF, m_NachLief);
	DDX_Control(pDX, IDC_BONUS_KZ, m_BonusKz);
}


BEGIN_MESSAGE_MAP(CBasePage, CPropertyPage)
	ON_WM_DESTROY ()
	ON_WM_CTLCOLOR ()
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_BN_CLICKED(IDC_FILCHOICE , OnFilchoice)
	ON_COMMAND(IDC_LIEFCHOICE , OnChoice)
	ON_COMMAND (SELECTED, OnSelected)
	ON_COMMAND (CANCELED, OnCanceled)
	ON_BN_CLICKED(IDC_ADRCHOICE , OnAdrChoice)
	ON_BN_CLICKED(IDC_PLZCHOICE , OnPlzChoice)
	ON_BN_CLICKED(IDC_ENTER_PARTNER , OnEnterPartner)
END_MESSAGE_MAP()


// CBasePage-Meldungshandler

BOOL CBasePage::OnInitDialog ()
{
	BOOL ret = CDbPropertyPage::OnInitDialog ();

	DATABASE->InitMdn ();
	DATABASE->InitFil ();
	DATABASE->InitLief ();

#ifdef BLUE_COL
	COLORREF BkColor (RGB (100, 180, 255));
	COLORREF DynColor = CColorButton::DynColorBlue;
	COLORREF RolloverColor = RGB (204, 204, 255);
#else
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
#endif

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	m_EnterPartner.SetWindowText (_T("Partner"));
//	m_EnterZahlKond.SetToolTip (_T("Zahlungskonditionen bearbeiten"));
//	m_EnterZahlKond.Tooltip.WindowOrientation = CQuikInfo::Left;
	m_EnterPartner.nID = IDC_ENTER_PARTNER;
	m_EnterPartner.Orientation = m_EnterPartner.Left;
	m_EnterPartner.TextStyle = m_EnterPartner.Standard;
	m_EnterPartner.BorderStyle = m_EnterPartner.Solide;
//	m_EnterZahlKond.BorderStyle = m_EnterZahlKond.Rounded;
	m_EnterPartner.DynamicColor = TRUE;
	m_EnterPartner.TextColor =RGB (0, 0, 0);
	m_EnterPartner.SetBkColor (BkColor);
	m_EnterPartner.DynColor = DynColor;
	m_EnterPartner.RolloverColor = RolloverColor;
	m_EnterPartner.LoadBitmap (IDB_ENTER);


	Form.Add (new CFormField (&m_Mdn,EDIT,              (short *) &TMDN.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT,       (char *)  &TMDN_ADR.adr_krz, VCHAR, sizeof (TMDN_ADR.adr_krz) - 1));
	Form.Add (new CFormField (&m_Fil,EDIT,              (short *) &TFIL.fil, VSHORT));
    Form.Add (new CUniFormField (&m_FilName,EDIT,       (char *)  &TFIL_ADR.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Lief,EDIT,             (char *)  TLIEF.lief, VCHAR));
	Form.Add (new CUniFormField (&m_LiefName,EDIT,      (char *)  TLIEF_ADR.adr_krz, VCHAR, sizeof (TLIEF_ADR.adr_krz) - 1));
	Form.Add (new CFormField (&m_Adr,EDIT,              (long *)  &TLIEF.adr, VLONG));
	Form.Add (new CFormField (&m_Anr,COMBOBOX,          (short *) &TLIEF_ADR.anr, VSHORT));
	Form.Add (new CFormField (&m_Name1,EDIT,            (char *)  TLIEF_ADR.adr_nam1, VCHAR, sizeof (TLIEF_ADR.adr_nam1) - 1));
	Form.Add (new CFormField (&m_Name2,EDIT,            (char *)  TLIEF_ADR.adr_nam2, VCHAR, sizeof (TLIEF_ADR.adr_nam2) - 1));
	Form.Add (new CFormField (&m_Name3,EDIT,            (char *)  TLIEF_ADR.adr_nam3, VCHAR, sizeof (TLIEF_ADR.adr_nam3) - 1));
	Form.Add (new CFormField (&m_Strasse,EDIT,          (char *)  TLIEF_ADR.str, VCHAR, sizeof (TLIEF_ADR.str) - 1));
	Form.Add (new CFormField (&m_Postfach,EDIT,         (char *)  TLIEF_ADR.pf, VCHAR, sizeof (TLIEF_ADR.pf) - 1));
	Form.Add (new CFormField (&m_plzPostfach,EDIT,      (char *)  TLIEF_ADR.plz_pf, VCHAR, sizeof (TLIEF_ADR.plz_pf) - 1));
	Form.Add (new CFormField (&m_Plz,EDIT,              (char *)  TLIEF_ADR.plz, VCHAR, sizeof (TLIEF_ADR.plz) - 1));
	Form.Add (new CFormField (&m_Ort,EDIT,              (char *)  TLIEF_ADR.ort1, VCHAR, sizeof (TLIEF_ADR.ort1) - 1));
	Form.Add (new CFormField (&m_Telefon,EDIT,          (char *)  TLIEF_ADR.tel, VCHAR, sizeof (TLIEF_ADR.tel) - 1));
	Form.Add (new CFormField (&m_Fax,EDIT,              (char *)  TLIEF_ADR.fax, VCHAR, sizeof (TLIEF_ADR.fax) - 1));
	Form.Add (new CFormField (&m_EMAIL,EDIT,            (char *)  TLIEF_ADR.email, VCHAR, sizeof (TLIEF_ADR.email) - 1));
	Form.Add (new CFormField (&m_Partner,EDIT,          (char *)  TLIEF_ADR.partner, VCHAR, sizeof (TLIEF_ADR.partner) - 1));
	Form.Add (new CFormField (&m_Staat,COMBOBOX,        (short *) &TLIEF_ADR.staat, VSHORT));
	Form.Add (new CFormField (&m_Land,COMBOBOX,         (short *) &TLIEF_ADR.land, VSHORT));
	Form.Add (new CFormField (&m_IncludeLand,CHECKBOX,  (short *) &IncludeLand, VSHORT));
	Form.Add (new CFormField (&m_Sprache,COMBOBOX,      (short *) &TLIEF.sprache, VSHORT));
	Form.Add (new CFormField (&m_Waehrung,COMBOBOX,     (short *) &TLIEF.waehrung , VSHORT));
	Form.Add (new CFormField (&m_BestTransKz,COMBOBOX,  (char *)  TLIEF.best_trans_kz , VCHAR));
	Form.Add (new CFormField (&m_MeKz,COMBOBOX,         (char *)  TLIEF.me_kz , VCHAR));
	Form.Add (new CFormField (&m_LiefRht,COMBOBOX,      (char *)  TLIEF.lief_rht , VCHAR));
	Form.Add (new CFormField (&m_WeKont,EDIT,           (char *)  TLIEF.we_kontr , VCHAR, sizeof (TLIEF.we_kontr) - 1));
	Form.Add (new CFormField (&m_LiefTyp,COMBOBOX,      (short *) &TLIEF.lief_typ , VSHORT));
	Form.Add (new CFormField (&m_Iln,EDIT,              (char *)  TLIEF.iln , VCHAR, sizeof (TLIEF.iln) - 1));
	Form.Add (new CFormField (&m_LiefKun,EDIT,          (char *)  TLIEF.lief_kun , VCHAR, sizeof (TLIEF.lief_kun) - 1));
	Form.Add (new CFormField (&m_WeToler,EDIT,         (double *) &TLIEF.we_toler, VDOUBLE, 4, 1));
	Form.Add (new CFormField (&m_Kreditor,EDIT,         (long *)  &TLIEF.kreditor , VLONG));
	Form.Add (new CFormField (&m_BestSort,COMBOBOX,     (short *) &TLIEF.best_sort , VSHORT));
	Form.Add (new CFormField (&m_NachLief,CHECKBOX,     (char *)  TLIEF.nach_lief , VCHAR));
	Form.Add (new CFormField (&m_BonusKz,CHECKBOX,      (char *)  TLIEF.bonus_kz , VCHAR));
	Form.Add (new CFormField (&m_LiefZeit,EDIT,        (short *) &TLIEF.lief_zeit , VSHORT));

	FillPtabCombo (&m_Anr, _T("anr"));
	FillPtabCombo (&m_Staat, _T("staat"));
	FillPtabCombo (&m_Land, _T("land"));
	FillPtabCombo (&m_Sprache, _T("sprache"));
	FillPtabCombo (&m_Waehrung, _T("waehrung"));
	FillPtabCombo (&m_LiefTyp, _T("lief_typ"));
	FillPtabCombo (&m_MeKz, _T("me_kz"));
	FillPtabCombo (&m_LiefRht, _T("lief_rht"));
	FillPtabCombo (&m_BestTransKz, _T("best_trans_kz"));
	FillPtabCombo (&m_BestSort, _T("best_sort"));

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 1, 3);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1);
	c_MdnName->SetCellPos (5, 0);
	MdnGrid.Add (c_MdnName);

	FilGrid.Create (this, 1, 3);
    FilGrid.SetBorder (0, 0);
    FilGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Fil = new CCtrlInfo (&m_Fil, 0, 0, 1, 1);
	FilGrid.Add (c_Fil);
	CtrlGrid.CreateChoiceButton (m_FilChoice, IDC_FILCHOICE, this);
	CCtrlInfo *c_FilChoice = new CCtrlInfo (&m_FilChoice, 1, 0, 1, 1);
	FilGrid.Add (c_FilChoice);
	CCtrlInfo *c_FilName = new CCtrlInfo (&m_FilName, 2, 0, 1, 1);
	c_FilName->SetCellPos (5, 0);
	FilGrid.Add (c_FilName);

	LiefGrid.Create (this, 1, 3);
    LiefGrid.SetBorder (0, 0);
    LiefGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Lief = new CCtrlInfo (&m_Lief, 0, 0, 1, 1);
	LiefGrid.Add (c_Lief);
	CtrlGrid.CreateChoiceButton (m_LiefChoice, IDC_LIEFCHOICE, this);
	CCtrlInfo *c_LiefChoice = new CCtrlInfo (&m_LiefChoice, 1, 0, 1, 1);
	LiefGrid.Add (c_LiefChoice);
	CCtrlInfo *c_LiefName = new CCtrlInfo (&m_LiefName, 2, 0, 1, 1);
	c_LiefName->SetCellPos (5, 0);
	LiefGrid.Add (c_LiefName);

	AdrGrid.Create (this, 1, 3);
    AdrGrid.SetBorder (0, 0);
    AdrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Adr = new CCtrlInfo (&m_Adr, 0, 0, 1, 1);
	AdrGrid.Add (c_Adr);
	CtrlGrid.CreateChoiceButton (m_AdrChoice, IDC_ADRCHOICE, this);
	CCtrlInfo *c_AdrChoice = new CCtrlInfo (&m_AdrChoice, 1, 0, 1, 1);
	AdrGrid.Add (c_AdrChoice);
	CCtrlInfo *c_EnterPartner = new CCtrlInfo (&m_EnterPartner, 2, 0, 1, 1);
	c_EnterPartner->SetCellPos (80, 0);
	AdrGrid.Add (c_EnterPartner);

	PlzGrid.Create (this, 1, 3);
    PlzGrid.SetBorder (0, 0);
    PlzGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Plz = new CCtrlInfo (&m_Plz, 0, 0, 1, 1);
	PlzGrid.Add (c_Plz);
	CtrlGrid.CreateChoiceButton (m_PlzChoice, IDC_PLZCHOICE, this);
	CCtrlInfo *c_PlzChoice = new CCtrlInfo (&m_PlzChoice, 1, 0, 1, 1);
	PlzGrid.Add (c_PlzChoice);
	CCtrlInfo *c_IncludeLand = new CCtrlInfo (&m_IncludeLand, 2, 0, 1, 1);
	c_IncludeLand->SetCellPos (5, 0);
	PlzGrid.Add (c_IncludeLand);

	TelFaxGrid.Create (this, 1, 2);
    TelFaxGrid.SetBorder (0, 0);
    TelFaxGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Telefon = new CCtrlInfo (&m_Telefon, 0, 0, 1, 1);
	TelFaxGrid.Add (c_Telefon);
	CCtrlInfo *c_Fax = new CCtrlInfo (&m_Fax, 1, 0, 1, 1);
	TelFaxGrid.Add (c_Fax);

	PLZPostfachGrid.Create (this, 1, 2);
    PLZPostfachGrid.SetBorder (0, 0);
    PLZPostfachGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Postfach = new CCtrlInfo (&m_Postfach, 0, 0, 1, 1);
	PLZPostfachGrid.Add (c_Postfach);
	CCtrlInfo *c_plzPostfach = new CCtrlInfo (&m_plzPostfach, 1, 0, 1, 1);
	PLZPostfachGrid.Add (c_plzPostfach);

	CCtrlInfo *c_HeadBorder = new CCtrlInfo (&m_HeadBorder, 0, 0, DOCKRIGHT, 4);
    c_HeadBorder->rightspace = 10; 
	CtrlGrid.Add (c_HeadBorder);

	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 1, 1, 1, 1);
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_MdnGrid);

	CCtrlInfo *c_LFil = new CCtrlInfo (&m_LFil, 1, 2, 1, 1);
	CtrlGrid.Add (c_LFil);
	CCtrlInfo *c_FilGrid = new CCtrlInfo (&FilGrid, 2, 2, 2, 1);
	CtrlGrid.Add (c_FilGrid);

	CCtrlInfo *c_LLief = new CCtrlInfo (&m_LLief, 1, 3, 1, 1);
	CtrlGrid.Add (c_LLief);
	CCtrlInfo *c_LiefGrid = new CCtrlInfo (&LiefGrid, 2, 3, 3, 1);
	CtrlGrid.Add (c_LiefGrid);

	CCtrlInfo *c_DataBorder = new CCtrlInfo (&m_DataBorder, 0, 5, 3, DOCKBOTTOM);
    CtrlGrid.Add (c_DataBorder);

	CCtrlInfo *c_DataBorder2 = new CCtrlInfo (&m_DataBorder2, 3, 5, DOCKRIGHT, DOCKBOTTOM);
    c_DataBorder2->rightspace = 10; 
	CtrlGrid.Add (c_DataBorder2);

	CCtrlInfo *c_LAdr = new CCtrlInfo (&m_LAdr, 1, 6, 1, 1);
	CtrlGrid.Add (c_LAdr);
	CCtrlInfo *c_AdrGrid = new CCtrlInfo (&AdrGrid, 2, 6, 1, 1);
	CtrlGrid.Add (c_AdrGrid);
	CCtrlInfo *c_LAnr = new CCtrlInfo (&m_LAnr, 1, 7, 1, 1);
	CtrlGrid.Add (c_LAnr);
	CCtrlInfo *c_Anr = new CCtrlInfo (&m_Anr, 2, 7, 1, 1);
	CtrlGrid.Add (c_Anr);
	CCtrlInfo *c_LName1 = new CCtrlInfo (&m_LName1, 1, 8, 1, 1);
	CtrlGrid.Add (c_LName1);
	CCtrlInfo *c_Name1 = new CCtrlInfo (&m_Name1, 2, 8, 1, 1);
	CtrlGrid.Add (c_Name1);

	CCtrlInfo *c_LName2 = new CCtrlInfo (&m_LName2, 1, 9, 1, 1);
	CtrlGrid.Add (c_LName2);
	CCtrlInfo *c_Name2 = new CCtrlInfo (&m_Name2, 2, 9, 1, 1);
	CtrlGrid.Add (c_Name2);

	CCtrlInfo *c_LName3 = new CCtrlInfo (&m_LName3, 1, 10, 1, 1);
	CtrlGrid.Add (c_LName3);
	CCtrlInfo *c_Name3 = new CCtrlInfo (&m_Name3, 2, 10, 1, 1);
	CtrlGrid.Add (c_Name3);

	CCtrlInfo *c_LStrasse = new CCtrlInfo (&m_LStrasse, 1, 11, 1, 1);
	CtrlGrid.Add (c_LStrasse);
	CCtrlInfo *c_Strasse = new CCtrlInfo (&m_Strasse, 2, 11, 1, 1);
	CtrlGrid.Add (c_Strasse);

	CCtrlInfo *c_LPostfach = new CCtrlInfo (&m_LPostfach, 1, 12, 1, 1);
	CtrlGrid.Add (c_LPostfach);
	CCtrlInfo *c_PLZPostfachGrid = new CCtrlInfo (&PLZPostfachGrid, 2, 12, 1, 1);
	CtrlGrid.Add (c_PLZPostfachGrid);

	CCtrlInfo *c_LPlz = new CCtrlInfo (&m_LPlz, 1, 13, 1, 1);
	CtrlGrid.Add (c_LPlz);
	CCtrlInfo *c_PlzGrid = new CCtrlInfo (&PlzGrid, 2, 13, 1, 1);
	CtrlGrid.Add (c_PlzGrid);
	

	CCtrlInfo *c_LOrt = new CCtrlInfo (&m_LOrt, 1, 14, 1, 1);
	CtrlGrid.Add (c_LOrt);
	CCtrlInfo *c_Ort = new CCtrlInfo (&m_Ort, 2, 14, 1, 1);
	CtrlGrid.Add (c_Ort);

	CCtrlInfo *c_LTelefon = new CCtrlInfo (&m_LTelefon, 1, 15, 1, 1);
	CtrlGrid.Add (c_LTelefon);
	CCtrlInfo *c_TelFaxGrid = new CCtrlInfo (&TelFaxGrid, 2, 15, 1, 1);
	CtrlGrid.Add (c_TelFaxGrid);
	
	CCtrlInfo *c_LEmail = new CCtrlInfo (&m_LEmail, 1, 16, 1, 1);
	CtrlGrid.Add (c_LEmail);
	CCtrlInfo *c_EMAIL = new CCtrlInfo (&m_EMAIL, 2, 16, 1, 1);
	CtrlGrid.Add (c_EMAIL);

	CCtrlInfo *c_LPartner = new CCtrlInfo (&m_LPartner, 1, 17, 1, 1);
	CtrlGrid.Add (c_LPartner);
	CCtrlInfo *c_Partner = new CCtrlInfo (&m_Partner, 2, 17, 1, 1);
	CtrlGrid.Add (c_Partner);

	CCtrlInfo *c_LStaat = new CCtrlInfo (&m_LStaat, 1, 19, 1, 1);
	CtrlGrid.Add (c_LStaat);
	CCtrlInfo *c_Staat = new CCtrlInfo (&m_Staat, 2, 19, 1, 1);
	CtrlGrid.Add (c_Staat);

	CCtrlInfo *c_LLand = new CCtrlInfo (&m_LLand, 1, 20, 1, 1);
	CtrlGrid.Add (c_LLand);
	CCtrlInfo *c_Land = new CCtrlInfo (&m_Land, 2, 20, 1, 1);
	CtrlGrid.Add (c_Land);

	CCtrlInfo *c_LLiefKun = new CCtrlInfo (&m_LLiefKun, 4, 6, 1, 1);
	CtrlGrid.Add (c_LLiefKun);
	CCtrlInfo *c_LiefKun = new CCtrlInfo (&m_LiefKun, 5, 6, 1, 1);
	CtrlGrid.Add (c_LiefKun);

	CCtrlInfo *c_LSprache = new CCtrlInfo (&m_LSprache, 4, 7, 1, 1);
	CtrlGrid.Add (c_LSprache);
	CCtrlInfo *c_Sprache = new CCtrlInfo (&m_Sprache, 5, 7, 1, 1);
	CtrlGrid.Add (c_Sprache);

	CCtrlInfo *c_LLiefTyp = new CCtrlInfo (&m_LLiefTyp, 4, 8, 1, 1);
	CtrlGrid.Add (c_LLiefTyp);
	CCtrlInfo *c_LiefTyp = new CCtrlInfo (&m_LiefTyp, 5, 8, 1, 1);
	CtrlGrid.Add (c_LiefTyp);

	CCtrlInfo *c_LKreditor = new CCtrlInfo (&m_LKreditor, 4, 10, 1, 1);
	CtrlGrid.Add (c_LKreditor);
	CCtrlInfo *c_Kreditor = new CCtrlInfo (&m_Kreditor, 5, 10, 1, 1);
	CtrlGrid.Add (c_Kreditor);

	CCtrlInfo *c_LIln = new CCtrlInfo (&m_LIln, 4, 11, 1, 1);
	CtrlGrid.Add (c_LIln);
	CCtrlInfo *c_Iln = new CCtrlInfo (&m_Iln, 5, 11, 1, 1);
	CtrlGrid.Add (c_Iln);

	CCtrlInfo *c_LBestTransKz = new CCtrlInfo (&m_LBestTransKz, 4, 13, 1, 1);
	CtrlGrid.Add (c_LBestTransKz);
	CCtrlInfo *c_BestTransKz = new CCtrlInfo (&m_BestTransKz, 5, 13, 1, 1);
	CtrlGrid.Add (c_BestTransKz);

	CCtrlInfo *c_LBestSort = new CCtrlInfo (&m_LBestSort, 4, 14, 1, 1);
	CtrlGrid.Add (c_LBestSort);
	CCtrlInfo *c_BestSort = new CCtrlInfo (&m_BestSort, 5, 14, 1, 1);
	CtrlGrid.Add (c_BestSort);

	CCtrlInfo *c_NachLief = new CCtrlInfo (&m_NachLief, 4, 16, 1, 1);
	CtrlGrid.Add (c_NachLief);
	CCtrlInfo *c_BonusKz = new CCtrlInfo (&m_BonusKz, 5, 16, 1, 1);
	CtrlGrid.Add (c_BonusKz);

	CCtrlInfo *c_LMeKz = new CCtrlInfo (&m_LMeKz, 4, 18, 1, 1);
	CtrlGrid.Add (c_LMeKz);
	CCtrlInfo *c_MeKz = new CCtrlInfo (&m_MeKz, 5, 18, 1, 1);
	CtrlGrid.Add (c_MeKz);

	CCtrlInfo *c_LLiefRht = new CCtrlInfo (&m_LLiefRht, 4, 19, 1, 1);
	CtrlGrid.Add (c_LLiefRht);
	CCtrlInfo *c_LiefRht = new CCtrlInfo (&m_LiefRht, 5, 19, 1, 1);
	CtrlGrid.Add (c_LiefRht);

	CCtrlInfo *c_LLiefZeit = new CCtrlInfo (&m_LLiefZeit, 4, 20, 1, 1);
	CtrlGrid.Add (c_LLiefZeit);
	CCtrlInfo *c_LiefZeit = new CCtrlInfo (&m_LiefZeit, 5, 20, 1, 1);
	CtrlGrid.Add (c_LiefZeit);

	CCtrlInfo *c_LWaehrung = new CCtrlInfo (&m_LWaehrung, 4, 21, 1, 1);
	CtrlGrid.Add (c_LWaehrung);
	CCtrlInfo *c_Waehrung = new CCtrlInfo (&m_Waehrung, 5, 21, 1, 1);
	CtrlGrid.Add (c_Waehrung);

	CCtrlInfo *c_LWeKont = new CCtrlInfo (&m_LWeKont, 4, 22, 1, 1);
	CtrlGrid.Add (c_LWeKont);
	CCtrlInfo *c_WeKont = new CCtrlInfo (&m_WeKont, 5, 22, 1, 1);
	CtrlGrid.Add (c_WeKont);

	CCtrlInfo *c_LWeToler = new CCtrlInfo (&m_LWeToler, 4, 23, 1, 1);
	CtrlGrid.Add (c_LWeToler);
	CCtrlInfo *c_WeToler = new CCtrlInfo (&m_WeToler, 5, 23, 1, 1);
	CtrlGrid.Add (c_WeToler);


	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	Form.Show ();
	CtrlGrid.Display ();
	m_Mdn.SetFocus ();
//	DisableTabControl (TRUE);
	EnableFields (FALSE);
	return FALSE;
}

HBRUSH CBasePage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	DlgBkColor = WINDOWHANDLER->GetDlgBkColor ();
	DlgBrush   = WINDOWHANDLER->GetDlgBrush ();
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			WINDOWHANDLER->SetDlgBkColor (DlgBkColor);
			WINDOWHANDLER->SetDlgBrush (DlgBrush);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}


BOOL CBasePage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				if (!StepBack ())
				{
					WINDOWHANDLER->DestroyFrame ();
				}
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				write ();
				WINDOWHANDLER->OnWrite (); //testtest
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				OnChoice ();
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Fil)
				{
					OnFilchoice ();
					return TRUE;
				}
				else if (GetFocus () == &m_Lief)
				{
					OnChoice ();
					return TRUE;
				}
				else if (GetFocus () == &m_Adr)
				{
					OnAdrChoice();
					return TRUE;
				}
				else if (GetFocus () == &m_Plz)
				{
					OnPlzChoice();
					return TRUE;
				}
				break;
			}

			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}

/*
 		    if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
*/
	}
    return CPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CBasePage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_Fil)
	{
		if (!ReadFil ())
		{
			m_Fil.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_Lief)
	{
		if (!Read ())
		{
			m_Lief.SetFocus ();
			return FALSE;
		}
	}
/*
	if (Control == &m_Adr)
	{
		if (OnAdrSelected())
		{
			m_Anr.SetFocus ();
		}
		else
		{
			m_Adr1.SetFocus ();
		}
		return FALSE;
		
	}


	if (Control == &m_Plz)
	{
		Form.GetFormField (&m_Plz)->Get ();
		Form.GetFormField (&m_Telefon)->Get ();
		PLZ_CLASS Plz = EnterPlz.GetPlzFromPlz (KunAdr.adr.plz);
		if (EnterPlz.Found ())
		{
			_tcscpy (KunAdr.adr.ort1, Plz.plz.ort);
			Form.GetFormField (&m_Ort)->Show ();
			if (_tcscmp (KunAdr.adr.tel, _T("0")) < 0)
			{
				_tcscpy (KunAdr.adr.tel, Plz.plz.vorwahl);
				Form.GetFormField (&m_Telefon)->Show ();
			}
			CFormField *fLand = Form.GetFormField (&m_Land);
			int idx = EnterPlz.GetComboIdx (Plz.plz.bundesland, fLand->ComboValues);
			if (idx != -1)
			{
				m_Land.SetCurSel (idx);
				fLand->Get ();
			}
		}
	}

	if (Control == &m_Ort)
	{
		Form.GetFormField (&m_Ort)->Get ();
		Form.GetFormField (&m_Telefon)->Get ();
		PLZ_CLASS Plz = EnterPlz.GetPlzFromOrt (KunAdr.adr.ort1);
		if (EnterPlz.Found ())
		{
			_tcscpy (KunAdr.adr.plz, Plz.plz.plz);
			Form.GetFormField (&m_Plz)->Show ();
			_tcscpy (KunAdr.adr.ort1, Plz.plz.ort);
			Form.GetFormField (&m_Ort)->Show ();
			if (_tcscmp (KunAdr.adr.tel, _T("0")) < 0)
			{
				_tcscpy (KunAdr.adr.tel, Plz.plz.vorwahl);
				Form.GetFormField (&m_Telefon)->Show ();
			}
			CFormField *fLand = Form.GetFormField (&m_Land);
			int idx = EnterPlz.GetComboIdx (Plz.plz.bundesland, fLand->ComboValues);
			if (idx != -1)
			{
				m_Land.SetCurSel (idx);
				fLand->Get ();
			}
		}
	}
*/

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CBasePage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

BOOL CBasePage::ReadMdn ()
{
	Form.Get ();
	BOOL ret = FALSE;
	ret = CORE->ReadMdn ();
	if (!ret)
	{
		AfxMessageBox (_T("Mandant nicht gefunden"), MB_OK | MB_ICONERROR);
	}
	Form.Show ();
	return ret;
}

BOOL CBasePage::ReadFil ()
{
	Form.Get ();
	BOOL ret = FALSE;
	ret = CORE->ReadFil ();
	if (!ret)
	{
		AfxMessageBox (_T("Filiale nicht gefunden"), MB_OK | MB_ICONERROR);
	}
	Form.Show ();
	return ret;
}

BOOL CBasePage::Read ()
{
	CDatabaseCore::SQL_STATE error;
	Form.Get ();
	CString cLief = TLIEF.lief;
    if (cLief.Trim () == "" || cLief.Trim () == "0")
	{
		AfxMessageBox (_T("Lieferntennummer nicht korrekt"));
		return FALSE;
	}

	BOOL ret = FALSE;
	ret = CORE->ReadLief ();
	if (!ret)
	{
		error = DATABASE->GetSqlState ();
		if (error == CDatabaseCore::SqlNoData)
		{
			AfxMessageBox (_T("Lieferant wird neu angelegt"), MB_OK | MB_ICONERROR);
			TLIEF.adr = _tstol (TLIEF.lief);
			TLIEF.lief_s = TLIEF.adr;
			ret = TRUE;
		}
		else if (error == CDatabaseCore::SqlLock)
		{
			AfxMessageBox (_T("Der Lieferant wird im Moment bearbeitet"), MB_OK | MB_ICONERROR);
		}
		if (!ret)
		{
			m_CanWrite = FALSE;
			return ret;
		}
	}
	Form.Show ();
	EnableFields (TRUE);
	m_CanWrite = TRUE;
	DisableTabControl (FALSE);
	WINDOWHANDLER->Update ();
	return ret;
}

BOOL CBasePage::Write ()
{
	BOOL ret = FALSE;
	Form.Get ();
	if (CanWrite ())
	{
		ret = CORE->WriteLief ();
	}
	if (ret)
	{
		EnableFields (FALSE);
		m_Lief.SetFocus ();
		m_CanWrite = FALSE;
		DisableTabControl (TRUE);
		if (Choice != NULL)
		{
			Choice->FillList ();
		}
	}
	return ret;
}

BOOL CBasePage::DeleteAll ()
{
	BOOL ret = FALSE;
	Form.Get ();
	if (CanWrite ())
	{
		if (MessageBox (_T("Lieferanten komplett l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) == IDYES)
		{
			ret = CORE->DeleteLief ();
		}
	}
	if (ret)
	{
		EnableFields (FALSE);
		m_Lief.SetFocus ();
		m_CanWrite = FALSE;
		DisableTabControl (TRUE);
		if (Choice != NULL)
		{
			Choice->FillList ();
		}
		Form.Show ();
	}
	return ret;
}

void CBasePage::FillPtabCombo (CWnd *Control, LPTSTR Item)
{
	CFormField *f = Form.GetFormField (Control);
	if (f != NULL)
	{
		CORE->FillPtabCombo (f->ComboValues, Item);
		f->FillComboBox ();
	}
}

BOOL CBasePage::StepBack ()
{
	if (!CanWrite ())
	{
		return FALSE;
	}
	DATABASE->Lief.rollbackwork ();
	EnableFields (FALSE);
	m_CanWrite = FALSE;
	m_Lief.SetFocus ();
	DisableTabControl (TRUE);
	return TRUE;
}

void CBasePage::EnableFields (BOOL b)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (!b)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	m_AdrChoice.EnableWindow (b);
	m_PlzChoice.EnableWindow (b);
	m_EnterPartner.EnableWindow (b);
	EnableHeadControls (!b);
	m_MdnChoice.EnableWindow (!b);
	m_FilChoice.EnableWindow (!b);
}

void CBasePage::EnableHeadControls (BOOL enable)
{
	HeadControls.Start ();
	CWnd **it;
	CWnd *Control;
	while ((it = HeadControls.GetNext ()) != NULL)
	{
		Control = *it;
		if (Control != NULL)
		{
			Control->EnableWindow (enable);
		}
	}
}

void CBasePage::OnChoice ()
{
	CString MdnNr;
	CString FilNr;
	m_Mdn.GetWindowText (MdnNr);
	m_Fil.GetWindowText (FilNr);
	if (Choice != NULL && WINDOWHANDLER->GetActiveChoice () == NULL)
	{
		Choice = NULL;
	}
	if (Choice != NULL)
	{
		if (!WINDOWHANDLER->GetDockChoice ())
		{
			if (atoi (MdnNr.GetBuffer ()) != Choice->m_Mdn ||
				atoi (FilNr.GetBuffer ()) != Choice->m_Fil)
			{
				delete Choice;
				Choice = NULL;
				WINDOWHANDLER->SetChoice (NULL);
			}
		}
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceLief (this);
		Choice->IsModal = FALSE;
		Choice->m_Mdn = _tstoi (MdnNr.GetBuffer ());
		Choice->m_Fil = _tstoi (FilNr.GetBuffer ());
		Choice->SetBitmapButton (TRUE);
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->HideFilter = FALSE;
		Choice->HideOK    = HideOK;
		Choice->SetDbClass (&DATABASE->Lief);
		Choice->DlgBkColor = DlgBkColor;
	}
	WINDOWHANDLER->OnChoice (Choice, GetParent ());
}

void CBasePage::OnSelected ()
{
	int ret = TRUE;
	BOOL CanRestore = FALSE;
	Choice = (CChoiceLief *) WINDOWHANDLER->GetChoice ();
	if (Choice == NULL) return;
	Form.Get ();
	if (CanWrite ())
	{
		WINDOWHANDLER->UpdateWrite ();
		ret = CORE->WriteLief ();
		CanRestore = TRUE;
	}
    CLiefList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
	CORE->SaveLief ();
	DATABASE->InitMdn ();
	DATABASE->InitFil ();
	DATABASE->InitLief ();
    TLIEF.mdn = abl->mdn;
    TLIEF.fil = abl->fil;
	_tcscpy (TLIEF.lief, abl->lief.GetBuffer ());
    TMDN.mdn = abl->mdn;
    TFIL.mdn = abl->mdn;
    TFIL.fil = abl->fil;
	CORE->ReadMdn ();
	CORE->ReadFil ();
    Form.Show ();
	if (!Read () && CanRestore)
	{
		CORE->RestoreLief ();
		TMDN.mdn = TLIEF.mdn;
		TFIL.mdn = TLIEF.mdn;
		TFIL.fil = TLIEF.fil;
		CORE->ReadMdn ();
		CORE->ReadFil ();
		Form.Show ();
		Read ();
	}
	if (Choice->FocusBack)
	{
		Choice->SetListFocus ();
	}
}

void CBasePage::OnCanceled ()
{
	Choice = (CChoiceLief *) WINDOWHANDLER->GetChoice ();
	Choice->ShowWindow (SW_HIDE);
}


void CBasePage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
		ChoiceMdn->SetBitmapButton (TRUE);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
	ChoiceMdn->SetDbClass (&DATABASE->Lief);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
		  DATABASE->Lief.lief.mdn = abl->mdn;
          memcpy (&DATABASE->Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&DATABASE->MdnAdr.adr, &adr_null, sizeof (ADR));
		  DATABASE->Mdn.mdn.mdn = abl->mdn;
		  CORE->ReadMdn ();
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  delete ChoiceMdn;
		  ChoiceMdn = NULL;
    }
}

void CBasePage::OnFilchoice ()
{
    Form.Get ();
	if (ChoiceFil != NULL && !ModalChoiceFil)
	{
		ChoiceFil->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceFil == NULL)
	{
		ChoiceFil = new CChoiceFil (this);
	    ChoiceFil->IsModal = ModalChoiceFil;
		ChoiceFil->SetBitmapButton (TRUE);
		ChoiceFil->m_Mdn = DATABASE->Mdn.mdn.mdn;
		ChoiceFil->CreateDlg ();
	}

	ChoiceFil->SetDbClass (&DATABASE->Lief);
	if (ModalChoiceFil)
	{
			ChoiceFil->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceFil->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceFil->MoveWindow (&rect);
		ChoiceFil->SetFocus ();
		return;
	}
    if (ChoiceFil->GetState ())
    {
		  CFilList *abl = ChoiceFil->GetSelectedText (); 
		  if (abl == NULL) return;
		  DATABASE->Lief.lief.mdn = abl->mdn;
		  DATABASE->Lief.lief.fil = abl->fil;
          memcpy (&DATABASE->Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&DATABASE->MdnAdr.adr, &adr_null, sizeof (ADR));
		  DATABASE->Mdn.mdn.mdn = abl->mdn;
		  CORE->ReadMdn ();
          memcpy (&DATABASE->Fil.fil, &fil_null, sizeof (FIL));
		  memcpy (&DATABASE->FilAdr.adr, &adr_null, sizeof (ADR));
		  DATABASE->Fil.fil.mdn = abl->mdn;
		  DATABASE->Fil.fil.fil = abl->fil;
		  CORE->ReadFil ();
		  Form.Show ();
		  m_Fil.SetSel (0, -1, TRUE);
		  m_Fil.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  delete ChoiceFil;
		  ChoiceFil = NULL;
    }
}

void CBasePage::OnAdrChoice ()
{
	CString AdrNr;
	m_Adr.GetWindowText (AdrNr);
    
	if (ChoiceAdr != NULL && !ModalChoiceAdr)
	{
		ChoiceAdr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceAdr == NULL)
	{
		ChoiceAdr = new CChoiceAdr (this);
	    ChoiceAdr->IsModal = ModalChoiceAdr;
		ChoiceAdr->SetBitmapButton (TRUE);
		ChoiceAdr->m_Adr = _tstol (AdrNr.GetBuffer ());
		ChoiceAdr->m_AdrTyp = 30;
		ChoiceAdr->CreateDlg ();
	}

    ChoiceAdr->SetDbClass (&DATABASE->Lief);
	
	if (ModalChoiceAdr)
	{
			ChoiceAdr->DoModal();
	}
	
    if (ChoiceAdr->GetState ())
    {
		  CAdrList *abl = ChoiceAdr->GetSelectedText (); 
		  if (abl == NULL) return;
		  DATABASE->Lief.lief.adr = abl->adr;
		  DATABASE->LiefAdr.adr.adr = abl->adr;
		  CORE->ReadAdr ();
		  Form.Show ();	
		  m_Adr.SetSel (0, -1, TRUE);
		  m_Adr.SetFocus ();
	}
    delete ChoiceAdr;
    ChoiceAdr = NULL;
}

void CBasePage::OnPlzChoice ()
{
	memcpy (&DATABASE->Ptabn.ptabn, &ptabn_null, sizeof (PTABN));
	memcpy (&DATABASE->Plz.plz, &plz_null, sizeof (PLZ));
	Form.GetFormField (&m_Ort)->Get ();
	Form.GetFormField (&m_Telefon)->Get ();
	Form.GetFormField (&m_Land)->Get ();
	Form.GetFormField (&m_IncludeLand)->Get ();
	_tcscpy (DATABASE->Plz.plz.ort, DATABASE->LiefAdr.adr.ort1);
	if (IncludeLand)
	{
		_tcscpy (DATABASE->Ptabn.ptabn.ptitem, _T("land"));
		_stprintf (DATABASE->Ptabn.ptabn.ptwert, "%hd", DATABASE->LiefAdr.adr.land);
		if (CORE->ReadPtabn ())
		{
			_tcscpy (DATABASE->Plz.plz.bundesland, DATABASE->Ptabn.ptabn.ptbez);
		}
	}
	CPlzList *abl = EnterPlz.Choice (DATABASE->Plz);
	if (abl != NULL)
	{
		
		_tcscpy (DATABASE->LiefAdr.adr.plz, abl->plz.GetBuffer ());
		Form.GetFormField (&m_Plz)->Show ();
		_tcscpy (DATABASE->LiefAdr.adr.ort1, abl->ort.GetBuffer ());
		Form.GetFormField (&m_Ort)->Show ();
		if (_tcscmp (DATABASE->LiefAdr.adr.tel, _T("0")) < 0)
		{
			_tcscpy (DATABASE->LiefAdr.adr.tel, abl->vorwahl.GetBuffer ());
			Form.GetFormField (&m_Telefon)->Show ();
		}
		CFormField *fLand = Form.GetFormField (&m_Land);
		int idx = EnterPlz.GetComboIdx (abl->bundesland.GetBuffer (), 
			                            fLand->ComboValues);
		if (idx != -1)
		{
			m_Land.SetCurSel (idx);
			fLand->Get ();
		}
	}
}


void CBasePage::UpdateWrite ()
{
	write ();
}

void CBasePage::Update ()
{
	Form.Show ();
}


void CBasePage::DisableTabControl (BOOL disable)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (disable)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}
}

void CBasePage::OnEnterPartner ()
{
	CProcess p;
    CString command;
	CString persName;

	CUtil::GetPersName (persName);

	command.Format (_T("lpartner %s"), persName.GetBuffer ());
	p.SetCommand (command);
	HANDLE id = p.Start ();
	DWORD exitCode = p.WaitForEnd ();
}
