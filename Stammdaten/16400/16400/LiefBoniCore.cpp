#include "StdAfx.h"
#include "LiefBoniCore.h"
#include "DbTime.h"
#include "StrFuncs.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CLiefBoniCore *CLiefBoniCore::Instance = NULL; 

CLiefBoniCore::CLiefBoniCore(void)
{
	CLIEF_BONI.sqlin ((short *) &TLIEF_BONI.mdn, SQLSHORT, 0);
	CLIEF_BONI.sqlin ((short *) &TLIEF_BONI.fil, SQLSHORT, 0);
	CLIEF_BONI.sqlin ((char *)  TLIEF_BONI.lief, SQLCHAR, sizeof (TLIEF_BONI.lief));
	CLIEF_BONI.sqlout ((long *) &TLIEF_BONI.lfd, SQLLONG, 0);
	m_Cursor = CLIEF_BONI.sqlcursor (_T("select lfd from lief_boni ")
									  _T("where mdn = ? ")
									  _T("and fil = ? ")
									  _T("and lief = ? ")
									  _T("order by lfd"));
	CLIEF_BONI.sqlin ((short *) &TLIEF_BONI.mdn, SQLSHORT, 0);
	CLIEF_BONI.sqlin ((short *) &TLIEF_BONI.fil, SQLSHORT, 0);
	CLIEF_BONI.sqlin ((char *)  TLIEF_BONI.lief, SQLCHAR, sizeof (TLIEF_BONI.lief));
	m_DeleteCursor = CLIEF_BONI.sqlcursor (_T("delete from lief_boni ")
											_T("where mdn = ? ")
											_T("and fil = ? ")
											_T("and lief = ? "));
	m_ListRow = NULL;
	m_ProzSum = 0.0;
}

CLiefBoniCore::~CLiefBoniCore(void)
{
	CLIEF_BONI.sqlclose (m_Cursor);
	m_LiefBoniTab.DestroyElements ();
	m_LiefBoniTab.Destroy ();
}

CLiefBoniCore *CLiefBoniCore::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CLiefBoniCore ();
	}
	return Instance;
}

void CLiefBoniCore::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

BOOL CLiefBoniCore::Read ()
{
	BOOL ret = FALSE;
	int dsqlstatus;
	m_LiefBoniTab.DestroyElements ();

	if (m_ListRow != NULL)
	{
		dsqlstatus = CLIEF_BONI.sqlopen (m_Cursor);
		if (dsqlstatus == 0)
		{
			while (CLIEF_BONI.sqlfetch (m_Cursor) == 0)
			{
				ret = TRUE;
				DATABASE->ReadLiefBoni ();
				m_ListRow->Add ();
				LIEF_BONI *tabrow = new LIEF_BONI;
				memcpy (tabrow, &TLIEF_BONI, sizeof (LIEF_BONI)); 
				m_LiefBoniTab.Add (tabrow);
			}
		}
		m_ListRow->Update ();
	}
	return ret;
}

BOOL CLiefBoniCore::Write ()
{
	BOOL ret = TRUE;
	LIEF_BONI **it;
	LIEF_BONI *tabrow;

	m_LiefBoniTab.Start ();
	while ((it = m_LiefBoniTab.GetNext ()) != NULL)
	{
		tabrow = *it;
		if (tabrow != NULL)
		{
			if (tabrow->bonus_proz > 999.99 ||
				tabrow->bonus_proz < -999.99)
			{
				ret = FALSE;
				break;
			}
		}
	}

	if (ret)
	{
		CLIEF_BONI.sqlexecute (m_DeleteCursor);
		long lfd = 1;
		m_LiefBoniTab.Start ();
		while ((it = m_LiefBoniTab.GetNext ()) != NULL)
		{
			tabrow = *it;
			if (tabrow != NULL && tabrow->bonus_proz != 0.0)
			{
				memcpy (&TLIEF_BONI, &lief_boni_null, sizeof (LIEF_BONI));
				TLIEF_BONI.mdn = TLIEF.mdn;
				TLIEF_BONI.fil = TLIEF.fil;
				_tcscpy (TLIEF_BONI.lief, TLIEF.lief);
				TLIEF_BONI.lfd = lfd;
				TLIEF_BONI.bonus_proz  = tabrow->bonus_proz;
				TLIEF_BONI.bonus_grenz =  tabrow->bonus_grenz;
				TLIEF_BONI.bonus_vj    =  tabrow->bonus_vj;
				memcpy (tabrow, &TLIEF_BONI, sizeof (LIEF_BONI));
				TLIEF_BONI.lief_s = _tstol (TLIEF_BONI.lief);
				CLIEF_BONI.dbupdate ();
				lfd ++;
			}
		}
	}
	return ret;
}


long CLiefBoniCore::NextLfd ()
{
	long lfd = 0l;
	int rows = m_LiefBoniTab.anz;
	if (rows > 0)
	{
		LIEF_BONI *row = *m_LiefBoniTab.Get (rows - 1);
		if (row != NULL)
		{
			lfd = row->lfd;
		}
	}
	return ++lfd;
}

void CLiefBoniCore::UpdateRow (int row)
{
	int rows = m_LiefBoniTab.anz;
	if (row >= rows)
	{
		LIEF_BONI *tabrow = new LIEF_BONI;
		memcpy (tabrow, &TLIEF_BONI, sizeof (LIEF_BONI)); 
		m_LiefBoniTab.Add (tabrow);
	}
	else
	{
	    LIEF_BONI *tabrow = *m_LiefBoniTab.Get (row);
		memcpy (tabrow, &TLIEF_BONI, sizeof (LIEF_BONI)); 
	}
	m_ListRow->Update ();
}

void CLiefBoniCore::DeleteRow (int row)
{
    if (m_LiefBoniTab.Get (row) != NULL)
	{
		LIEF_BONI *tabrow = *m_LiefBoniTab.Get (row);
		m_LiefBoniTab.Drop (row);
		delete tabrow;
		m_ListRow->Update ();
	}
}
