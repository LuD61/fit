#include "StdAfx.h"
#include "DatabaseCore.h"
#include "DbTime.h"
#include "DbUnicode.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CDatabaseCore *CDatabaseCore::Instance = NULL; 

CDatabaseCore::CDatabaseCore(void)
{
}

CDatabaseCore::~CDatabaseCore(void)
{
	m_SqlState = SqlOk;
}

CDatabaseCore *CDatabaseCore::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CDatabaseCore ();
	}
	return Instance;
}

void CDatabaseCore::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

void CDatabaseCore::Opendbase ()
{
	Dbase.opendbase ("bws");
}

void CDatabaseCore::InitMdn ()
{
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
}

BOOL CDatabaseCore::ReadMdn ()
{
	BOOL ret = FALSE;
	int dsqlstatus = 0;

	m_SqlState = SqlOk;
	dsqlstatus = Mdn.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		dsqlstatus = MdnAdr.dbreadfirst ();
	}
	if (dsqlstatus == 0)
	{
		ret = true;
	}
	else if (dsqlstatus == 100)
	{
		m_SqlState = SqlNoData;
	}
	else
	{
		m_SqlState = SqlError;
	}
	return ret;
}
void CDatabaseCore::InitFil ()
{
	memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
}

BOOL CDatabaseCore::ReadFil ()
{
	BOOL ret = FALSE;
	int dsqlstatus = 0;

	m_SqlState = SqlOk;
	dsqlstatus = Fil.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		FilAdr.adr.adr = Fil.fil.adr;
		dsqlstatus = FilAdr.dbreadfirst ();
	}
	if (dsqlstatus == 0)
	{
		ret = true;
	}
	else if (dsqlstatus == 100)
	{
		m_SqlState = SqlNoData;
	}
	else
	{
		m_SqlState = SqlError;
	}
	return ret;
}

void CDatabaseCore::InitLief ()
{
	memcpy (&Lief.lief, &lief_null, sizeof (LIEF));
	memcpy (&LiefAdr.adr, &adr_null, sizeof (ADR));
}

void CDatabaseCore::InitAdr ()
{
	memcpy (&LiefAdr.adr, &adr_null, sizeof (ADR));
}

BOOL CDatabaseCore::ReadAdr ()
{
	BOOL ret = FALSE;
	int dsqlstatus;
	m_SqlState = SqlOk;
	dsqlstatus = LiefAdr.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	else if (dsqlstatus == 100)
	{
		m_SqlState = SqlNoData;
	}
	else
	{
		m_SqlState = SqlError;
	}
	return ret;
}

BOOL CDatabaseCore::ReadLief ()
{
	BOOL ret = TRUE;
	int dsqlstatus = 0;
	short sql_save;
	extern short sql_mode;

	m_SqlState = SqlOk;
	sql_save = sql_mode;
	sql_mode = 1;
	Lief.beginwork ();
	dsqlstatus = Lief.dblock ();
	sql_mode = sql_save;
	if (dsqlstatus < 0)
	{
		m_SqlState = SqlLock;
		Lief.commitwork ();
		ret = FALSE;
	}
	if (ret)
	{
		dsqlstatus = Lief.dbreadfirst ();
		if (dsqlstatus == 0)
		{
			LiefAdr.adr.adr = Lief.lief.adr;
			dsqlstatus = LiefAdr.dbreadfirst ();
			Zahl_kond.zahl_kond.zahl_kond = Lief.lief.zahl_kond;
			ReadZahlKond ();
		}
		if (dsqlstatus == 100)
		{
			m_SqlState = SqlNoData;
			ret = FALSE;
		}
		else if (dsqlstatus != 0)
		{
			m_SqlState = SqlError;
			ret = FALSE;
		}
	}
	return ret;
}

BOOL CDatabaseCore::ReadZahlKond ()
{
	int ret = FALSE;
	int dsqlstatus;
	short zahl_kond;
	zahl_kond = TZAHL_KOND.zahl_kond;
	memcpy (&TZAHL_KOND, &zahl_kond_null, sizeof (ZAHL_KOND));
	TZAHL_KOND.zahl_kond = zahl_kond;
	dsqlstatus = CZAHL_KOND.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	return ret;
}

void CDatabaseCore::SaveZahlKond ()
{
	memcpy (&m_ZahlKondSave, &TZAHL_KOND, sizeof (ZAHL_KOND));
}

void CDatabaseCore::RestoreZahlKond ()
{
	memcpy (&TZAHL_KOND, &m_ZahlKondSave, sizeof (ZAHL_KOND));
}

BOOL CDatabaseCore::WriteLief ()
{
	BOOL ret = TRUE;

	Lief.lief.letzt_lief = DbTime (&Lief.lief.letzt_lief);
	LiefAdr.adr.geb_dat = DbTime (&LiefAdr.adr.geb_dat);
	LiefAdr.adr.adr_typ = Distributor;

	Lief.dbupdate ();
	LiefAdr.adr.adr = Lief.lief.adr;
	LiefAdr.dbupdate ();
	Lief.commitwork ();
	return ret;
}

BOOL CDatabaseCore::ReadLiefRKte ()
{
	int ret = FALSE;
	int dsqlstatus;
	short mdn;
	short fil;
	char lief [sizeof (TLIEF_R_KTE.lief)];
	long lfd;
	mdn = TLIEF_R_KTE.mdn;
	fil = TLIEF_R_KTE.fil;
	strcpy (lief, (char *) TLIEF_R_KTE.lief); 
	lfd = TLIEF_R_KTE.lfd;
	memcpy (&TLIEF_R_KTE, &lief_r_kte_null, sizeof (LIEF_R_KTE));
	TLIEF_R_KTE.mdn = mdn;
	TLIEF_R_KTE.fil = fil;
	strcpy (TLIEF_R_KTE.lief, lief); 
	TLIEF_R_KTE.lfd = lfd;
	dsqlstatus = Lief_r_kte.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	return ret;
}

BOOL CDatabaseCore::ReadLiefRStf ()
{
	int ret = FALSE;
	int dsqlstatus;
	short mdn;
	short fil;
	char lief [sizeof (TLIEF_R_KTE.lief)];
	long lfd;
	mdn = TLIEF_R_STF.mdn;
	fil = TLIEF_R_STF.fil;
	strcpy (lief, (char *) TLIEF_R_STF.lief); 
	lfd = TLIEF_R_STF.lfd;
	memcpy (&TLIEF_R_STF, &lief_r_stf_null, sizeof (LIEF_R_STF));
	TLIEF_R_STF.mdn = mdn;
	TLIEF_R_STF.fil = fil;
	strcpy (TLIEF_R_STF.lief, lief); 
	TLIEF_R_STF.lfd = lfd;
	dsqlstatus = Lief_r_stf.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	return ret;
}

BOOL CDatabaseCore::ReadLiefRGr ()
{
	int ret = FALSE;
	int dsqlstatus;
	short mdn;
	short fil;
	char lief [sizeof (TLIEF_R_GR.lief)];
	long lfd;
	mdn = TLIEF_R_GR.mdn;
	fil = TLIEF_R_GR.fil;
	strcpy (lief, (char *) TLIEF_R_GR.lief); 
	lfd = TLIEF_R_GR.lfd;
	memcpy (&TLIEF_R_GR, &lief_r_gr_null, sizeof (LIEF_R_GR));
	TLIEF_R_GR.mdn = mdn;
	TLIEF_R_GR.fil = fil;
	strcpy (TLIEF_R_GR.lief, lief); 
	TLIEF_R_GR.lfd = lfd;
	dsqlstatus = Lief_r_gr.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	return ret;
}

BOOL CDatabaseCore::ReadLiefRLisBez ()
{
	BOOL ret = TRUE;

	CString RabTyp = TLIEF_R_LIS.rab_typ;
	if (RabTyp.MakeUpper () == _T ("AR"))
	{
		A_bas.a_bas.a = TLIEF_R_LIS.a;
		if (A_bas.dbreadfirst () == 0)
		{	
		    CDbUniCode::DbToUniCode (A_bas.a_bas.a_bz1, (LPSTR) A_bas.a_bas.a_bz1);
		    CDbUniCode::DbToUniCode (A_bas.a_bas.a_bz2, (LPSTR) A_bas.a_bas.a_bz2);
			m_LiefRLisBez1 = A_bas.a_bas.a_bz1;
			m_LiefRLisBez2 = A_bas.a_bas.a_bz2;
		}
	}
	else if (RabTyp.MakeUpper () == _T ("AG"))
	{
		Ag.ag.ag = (short) TLIEF_R_LIS.a;
		if (Ag.dbreadfirst () == 0)
		{	
		    CDbUniCode::DbToUniCode (Ag.ag.ag_bz1, (LPSTR) Ag.ag.ag_bz1);
		    CDbUniCode::DbToUniCode (Ag.ag.ag_bz2, (LPSTR) Ag.ag.ag_bz2);
			m_LiefRLisBez1 = Ag.ag.ag_bz1;
			m_LiefRLisBez2 = Ag.ag.ag_bz2;
		}
	}
	else if (RabTyp.MakeUpper () == _T ("WG"))
	{
		Wg.set_Wg ((short) TLIEF_R_LIS.a);
		if (Wg.dbreadfirst () == 0)
		{
			TCHAR *wg_bz1 = Wg.WgBz1 ();
			TCHAR *wg_bz2 = Wg.WgBz2 ();
		    CDbUniCode::DbToUniCode (wg_bz1, (LPSTR) wg_bz1);
		    CDbUniCode::DbToUniCode (wg_bz2, (LPSTR) wg_bz2);
			m_LiefRLisBez1 = wg_bz1;
			m_LiefRLisBez2 = wg_bz2;
		}
	}
	return ret;
}

BOOL CDatabaseCore::ReadLiefRLis ()
{
	LPSTR pos;
	int ret = FALSE;
	int dsqlstatus;
	short mdn;
	short fil;
	long lst_nr;
	double a;
 	char rab_typ[sizeof (TLIEF_R_LIS.rab_typ)];
	mdn = TLIEF_R_LIS.mdn;
	fil = TLIEF_R_LIS.fil;
	lst_nr = TLIEF_R_LIS.lst_nr;
	a = TLIEF_R_LIS.a;
	strcpy (rab_typ, (char *) TLIEF_R_LIS.rab_typ); 
	memcpy (&TLIEF_R_LIS, &lief_r_lis_null, sizeof (LIEF_R_LIS));
	m_LiefRLisBez1 = _T("");
	m_LiefRLisBez2 = _T("");
	TLIEF_R_LIS.mdn = mdn;
	TLIEF_R_LIS.fil = fil;
	TLIEF_R_LIS.lst_nr = lst_nr;
	TLIEF_R_LIS.a = a;
	strcpy (TLIEF_R_LIS.rab_typ, rab_typ); 
	dsqlstatus = Lief_r_lis.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		pos = (LPSTR) TLIEF_R_LIS.rab_typ;
	    CDbUniCode::DbToUniCode (TLIEF_R_LIS.rab_typ, pos);
        ReadLiefRLisBez ();
		ret = TRUE;
	}
	return ret;
}

BOOL CDatabaseCore::ReadLiefBoni ()
{
	int ret = FALSE;
	int dsqlstatus;
	short mdn;
	short fil;
	char lief [sizeof (TLIEF_BONI.lief)];
	long lfd;
	mdn = TLIEF_BONI.mdn;
	fil = TLIEF_BONI.fil;
	strcpy (lief, (char *) TLIEF_BONI.lief); 
	lfd = TLIEF_BONI.lfd;
	memcpy (&TLIEF_BONI, &lief_boni_null, sizeof (LIEF_BONI));
	TLIEF_BONI.mdn = mdn;
	TLIEF_BONI.fil = fil;
	strcpy (TLIEF_BONI.lief, lief); 
	TLIEF_BONI.lfd = lfd;
	dsqlstatus = Lief_boni.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	return ret;
}

void CDatabaseCore::WG_CLASS::prepare ()
{
	sqlin ((short *) &m_Wg, SQLSHORT, 0);
	sqlout ((char *) m_WgBz1, SQLCHAR, sizeof (m_WgBz1));
	sqlout ((char *) m_WgBz2, SQLCHAR, sizeof (m_WgBz2));
	cursor = sqlcursor (_T("select wg_bz1, wg_bz2 from wg where wg = ?"));
}

