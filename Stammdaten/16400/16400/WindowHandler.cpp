#include "StdAfx.h"
#include "WindowHandler.h"
#include "BasePage.h"
#include "mainSplitter.h"
#include "DbPropertySheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CWindowHandler *CWindowHandler::Instance = NULL;

CWindowHandler::CWindowHandler(void)
{
	m_DockChoice = FALSE;
	m_Choice = NULL;
	m_Base = NULL;
	m_MainFrame = NULL;
	m_ChoiceIsCreated = FALSE;
	m_StartWithDock = FALSE;
	m_ChoiceDockable = TRUE;
	m_CloseFromMemory = FALSE;
	m_DlgBkColor = RGB (235, 235, 245);
	m_DlgBrush = NULL;
    m_SplitDir = Horizontal;
	m_Sheet = NULL;
}

CWindowHandler::~CWindowHandler(void)
{
}

CWindowHandler *CWindowHandler::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CWindowHandler ();
	}
	return Instance;
}

void CWindowHandler::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

void CWindowHandler::RegisterUpdate (CUpdateHandler *UpdateHandler)
{
	UpdateTable.push_back (UpdateHandler);
}

void CWindowHandler::UnregisterUpdate (CUpdateHandler *UpdateHandler)
{
	CUpdateHandler *u;

	for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
	{
		u = *it;
		if (u == UpdateHandler)
		{
			UpdateTable.erase (it);
		}
	}
}

void CWindowHandler::Update ()
{
	CUpdateHandler *UpdateHandler;

	for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
	{
		UpdateHandler = *it;
		UpdateHandler->Update ();
	}
}

void CWindowHandler::UpdateWrite ()
{
	CUpdateHandler *UpdateHandler;

	for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
	{
		UpdateHandler = *it;
		UpdateHandler->UpdateWrite ();
	}
}


BOOL CWindowHandler::IsDockPlace (int x, int y)
{
	BOOL ret = FALSE;

	if (m_ChoiceDockable && m_ChoiceIsCreated && m_Splitter != NULL && m_Choice != NULL)
	{
		CRect rect;
		CRect cRect;

		if (m_Splitter->GetColumnCount () < 2)
		{
			m_Splitter->GetWindowRect (&rect);
			m_Choice->GetClientRect (&cRect);
			if (y >= rect.top + 20 && y <= rect.top + 25)
			{
				if (x > rect.right - 400)
				{
					m_Choice->DestroyWindow ();
					m_Splitter->GetClientRect (&rect);
					m_Splitter->SplitColumn (rect.right - cRect.right);
					m_DockChoice = TRUE;
					m_SplitDir = Horizontal;
					m_Choice->SetParent (m_Base);
					ret = TRUE;
				}
			}
		}
		return IsVerticaleDockPlace (x, y);
	}
	return ret;
}

BOOL CWindowHandler::IsVerticaleDockPlace (int x, int y)
{
	BOOL ret = FALSE;

	if (m_ChoiceDockable && m_ChoiceIsCreated && m_Splitter != NULL && m_Choice != NULL)
	{
		CRect rect;
		CRect cRect;

		if (m_Splitter->GetRowCount () < 2)
		{
			m_Splitter->GetWindowRect (&rect);
			m_Choice->GetClientRect (&cRect);
			if (x >= rect.left && x <= rect.left + 25)
			{
				if (y > rect.bottom - 300)
				{
					m_Choice->DestroyWindow ();
					m_Splitter->GetClientRect (&rect);
					m_Splitter->SplitRow (rect.bottom - 300);
					m_DockChoice = TRUE;
					m_SplitDir = Vertical;
					m_Choice->SetParent (m_Base);
					ret = TRUE;
				}
			}
		}
	}
	return ret;
}

void CWindowHandler::SplitChoice ()
{
	CRect rect;
	CRect cRect;
	CChoiceX *choice = NULL;

	choice = m_Choice;

	if (!m_DockChoice && choice != NULL)
	{
		choice->DestroyWindow ();
		delete choice;
		m_Choice = NULL;
	}
	if (m_DockChoice && m_SplitDir == Vertical)
	{
		DeleteBottomChoice ();
	}
	if (m_Splitter->GetColumnCount () < 2)
	{
		m_Splitter->GetClientRect (&rect);
		m_Splitter->SplitColumn (rect.right - 400);
		choice = m_Choice;

		m_DockChoice = TRUE;
		m_SplitDir = Horizontal;
		choice->DlgBkColor = m_DlgBkColor;
		choice->SetParent (m_Base);
	}
}

void CWindowHandler::SplitBottomChoice ()
{
	CRect rect;
	CRect cRect;
	CChoiceX *choice = NULL;

	choice = m_Choice;
	if (!m_DockChoice && choice != NULL)
	{
		choice->DestroyWindow ();
		delete choice;
		m_Choice = NULL;
	}
	if (m_DockChoice && m_SplitDir == Horizontal)
	{
		DeleteChoice ();
	}
	if (m_Splitter->GetRowCount () < 2)
	{
		m_Splitter->GetClientRect (&rect);
		m_Splitter->SplitRow (rect.bottom - 400);
		choice = m_Choice;
		m_DockChoice = TRUE;
		m_SplitDir = Vertical;
		choice->DlgBkColor = m_DlgBkColor;
		choice->SetParent (m_Base);
	}
}

void CWindowHandler::OnChoice (CChoiceX *Choice, CWnd *Parent)
{
	CRect sRect;

    if ((m_StartWithDock || m_DockChoice) && m_Splitter != NULL)
	{
		m_Choice = Choice;
		if (!m_DockChoice)
		{
			m_Splitter->GetClientRect (&sRect);
			m_Splitter->SplitColumn (sRect.right - 400);
		}
		m_DockChoice = TRUE;
		m_Choice->SetParent (m_Base);
		m_SplitDir = Horizontal;
		return;
	}
	else
	{
		if (m_Choice != NULL)
		{
			m_Choice->ShowWindow (SW_SHOWNORMAL);
		}
		else
		{
			m_ChoiceIsCreated = FALSE;
			m_Choice = Choice;
			Choice->CreateDlg ();
			CRect mrect;
			Parent->GetWindowRect (&mrect);
			CRect rect;
			Choice->GetWindowRect (&rect);
			int scx = GetSystemMetrics (SM_CXSCREEN);
			int scy = GetSystemMetrics (SM_CYSCREEN);
			rect.top = 50;
			rect.right = scx - 2;
			rect.left = rect.right - 300;
			rect.bottom = scy - 50;
			Choice->MoveWindow (&rect);
			m_ChoiceIsCreated = TRUE;
		}
		Choice->SetListFocus ();
	}
}

void CWindowHandler::OnDeleteChoice ()
{
    if (m_DockChoice && m_Splitter != NULL)
	{
		if (m_SplitDir == Horizontal)
		{
			m_Splitter->DeleteColumn (CMainSplitter::ChoiceLief);
		}
		else if (m_SplitDir == Vertical)
		{
			m_Splitter->DeleteRow (CMainSplitter::ChoiceLief);
		}

		m_Choice->DestroyWindow ();
		delete m_Choice;
		m_Choice = NULL;
		m_DockChoice = FALSE;
	}
	else
	{
		m_Choice->ShowWindow (SW_HIDE);
	}
}

void CWindowHandler::DeleteChoice ()
{
	CChoiceX *choice = NULL;

	choice = m_Choice;
	if (m_Splitter->GetColumnCount () > Choice)
	{
		m_Splitter->DeleteColumn (Choice);
	}
	if (choice != NULL)
	{
		choice->DestroyWindow ();
		delete choice;
		m_Choice = NULL;
		m_DockChoice = FALSE;
	}
	m_DockChoice = FALSE;
}

void CWindowHandler::DeleteBottomChoice ()
{
	CChoiceX *choice = NULL;

	choice = m_Choice;
	if (m_Splitter->GetRowCount () > Choice)
	{
		m_Splitter->DeleteRow (Choice);
	}
	if (m_Choice != NULL)
	{
		choice->DestroyWindow ();
		delete choice;
		m_Choice = NULL;
		m_DockChoice = FALSE;
	}
	m_DockChoice = FALSE;
}

 CChoiceX *CWindowHandler::GetChoice ()
 {
	 if (m_Choice == NULL)
	 {
		m_Choice = new CChoiceLief ();
	 }
	 return m_Choice;
 }

void CWindowHandler::OnBack ()
{
	CUpdateHandler *UpdateHandler;

	if (m_Sheet != NULL)
	{
		((CDbPropertySheet *) m_Sheet)->StepBack ();
	}
	else
	{
		for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
		{
			UpdateHandler = *it;
			if (!UpdateHandler->StepBack ())
			{
				 DestroyFrame ();
				 return;
			}
		}
	}
}


void CWindowHandler::OnWrite ()
{
	if (m_Sheet != NULL)
	{
		((CDbPropertySheet *) m_Sheet)->Write ();
	}
}

void CWindowHandler::OnDelete ()
{
	if (m_Sheet != NULL)
	{
		((CDbPropertySheet *) m_Sheet)->DeleteAll ();
	}
}

void CWindowHandler::DestroyFrame ()
{
	if (m_MainFrame != NULL)
	{
		m_CloseFromMemory = TRUE;
		m_MainFrame->PostMessage (WM_SYSCOMMAND, SC_CLOSE, 0);
	}
}
