#include "StdAfx.h"
#include "LiefRLisListCtrl.h"
#include "LiefRLisCore.h"
#include "DatabaseCore.h"
#include "StrFuncs.h"
#include "resource.h"


CString CLiefRLisListCtrl::m_RabTypTab[] = {CString (_T("AR")),
										    CString (_T("AG")),
											CString (_T("WG"))
};

CString CLiefRLisListCtrl::m_RabStTab[] = {CString (_T("%")),
										   CString (_T("Wert"))
};
CLiefRLisListCtrl::CLiefRLisListCtrl(void)
{
	ListRows.Init ();
	m_RabTypValues.Add (&m_RabTypTab[0]);
	m_RabTypValues.Add (&m_RabTypTab[1]);
	m_RabTypValues.Add (&m_RabTypTab[2]);

	m_RabStValues.Add (&m_RabStTab[0]);
	m_RabStValues.Add (&m_RabStTab[1]);
}

CLiefRLisListCtrl::~CLiefRLisListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CLiefRLisListCtrl, CEditListCtrl)
//	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()

void CLiefRLisListCtrl::InitList ()
{
	m_ImageList.Create(20, 20, ILC_MASK, 0, 4);
	SetImageList (&m_ImageList, LVSIL_SMALL);
}

void CLiefRLisListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (1, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (1, 0);
	}
}

void CLiefRLisListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	int count = GetHeaderCtrl ()->GetItemCount ();
	while (IsDisplayOnly (col) && col < count - 1) col ++;
	if (col == PosRabTyp)
	{
		CEditListCtrl::StartEnterCombo (col, row, &m_RabTypValues);
	}
	else if (col == PosRabSt)
	{
		CEditListCtrl::StartEnterCombo (col, row, &m_RabStValues);
	}
	else
	{
		CEditListCtrl::StartEnter (col, row);
	}
	if (col == PosA)
	{
		ListEdit.SetLimitText (13);
	}
	if (col == PosRabWrt)
	{
		ListEdit.SetLimitText (8);
	}
}

void CLiefRLisListCtrl::StopEnter ()
{
	CString cA;
	CString cRabWrt;
	CEditListCtrl::StopEnter ();
	if (EditCol == PosA)
	{
		CString cRabTyp       =  GetItemText (EditRow, PosRabTyp);
		CString cA            =  GetItemText (EditRow, PosA);
		TLIEF_R_LIS.a = CStrFuncs::StrToDouble (cA);
		_tcscpy (TLIEF_R_LIS.rab_typ, cRabTyp.GetBuffer ());
		DATABASE->ReadLiefRLisBez ();
		FillList.SetItemText (LIEF_R_LIS_BZ1, EditRow, PosBez);
	}
	UpdateRow ();
	cA.Format      (_T("%.0lf"), TLIEF_R_LIS.a);
	cRabWrt.Format (_T("%.2lf"), TLIEF_R_LIS.rab_wrt);
	FillList.SetItemText (TLIEF_R_LIS.rab_typ, EditRow, 1);
	FillList.SetItemText (cA.GetBuffer (), EditRow, 2);
	FillList.SetItemText (LIEF_R_LIS_BZ1, EditRow, 3);
	FillList.SetItemText (cRabWrt.GetBuffer (), EditRow, 4);
	FillList.SetItemText (TLIEF_R_LIS.rab_st, EditRow, 5);
}

void CLiefRLisListCtrl::SetSel (CString& Text)
{
/*
   {
		ListEdit.SetSel (0, -1);
   }
*/
   if (EditCol == PosRabWrt)
   {
		ListEdit.SetSel (0, -1);
   }
   else if (EditCol == PosA)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CLiefRLisListCtrl::FormatText (CString& Text)
{
}

void CLiefRLisListCtrl::NextRow ()
{
	int count = GetItemCount ();
	SetEditText ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;

	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
	StartEnter (EditCol, EditRow);
}

void CLiefRLisListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CLiefRLisListCtrl::LastCol ()
{
	if (EditCol < PosRabSt) return FALSE;
	return TRUE;
}

void CLiefRLisListCtrl::UpdateRow ()
{
	CString cRabTyp       =  GetItemText (EditRow, PosRabTyp);
	CString cA            =  GetItemText (EditRow, PosA);
	CString cRabWrt       =  GetItemText (EditRow, PosRabWrt);
	CString cRabSt        =  GetItemText (EditRow, PosRabSt);
	_tcscpy (TLIEF_R_LIS.rab_typ, cRabTyp.GetBuffer ());
	TLIEF_R_LIS.a = CStrFuncs::StrToDouble (cA);
	TLIEF_R_LIS.rab_wrt = CStrFuncs::StrToDouble (cRabWrt);
	_tcscpy (TLIEF_R_LIS.rab_st, cRabSt.GetBuffer ());
	LIEF_R_LIS_CORE->UpdateRow (EditRow);
}

void CLiefRLisListCtrl::OnReturn ()
{

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (LastCol ())
	{
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
//		EditCol = 0;
		if (EditRow == rowCount)
		{
			EditCol = 1;
		}
		else
		{
			EditCol = 2;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
		if (IsDisplayOnly (EditCol))
		{
			EditCol ++;
		}
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CLiefRLisListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CLiefRLisListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	StopEnter ();
	EditCol --;
	while (IsDisplayOnly (EditCol) && EditCol >= 0) EditCol --;
    StartEnter (EditCol, EditRow);
}

BOOL CLiefRLisListCtrl::InsertRow ()
{
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T("  "), EditRow, 1);
	FillList.SetItemText (_T("  "), EditRow, 2);
	FillList.SetItemText (_T("  "), EditRow, 3);
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CLiefRLisListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	int row = EditRow;
	BOOL ret = CEditListCtrl::DeleteRow ();
	LIEF_R_LIS_CORE->DeleteRow (row);
	return ret;
}

BOOL CLiefRLisListCtrl::AppendEmpty ()
{
	int rowCount = GetItemCount ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CWnd *header = GetHeaderCtrl ();
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText ("AR", rowCount, PosRabTyp);
	FillList.SetItemText (_T("0"), rowCount, PosA);
	FillList.SetItemText (_T("0,00"), rowCount, PosRabWrt);
	FillList.SetItemText (_T("%"), rowCount, PosRabSt);
	rowCount = GetItemCount ();
	return TRUE;
}

void CLiefRLisListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CLiefRLisListCtrl::RunItemClicked (int Item)
{
}

void CLiefRLisListCtrl::RunCtrlItemClicked (int Item)
{
}

void CLiefRLisListCtrl::RunShiftItemClicked (int Item)
{
}

void CLiefRLisListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	Text = cText.Trim ();
}

void CLiefRLisListCtrl::ScrollPositions (int pos)
{
}

