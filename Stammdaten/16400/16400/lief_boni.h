#ifndef _LIEF_BONI_DEF
#define _LIEF_BONI_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LIEF_BONI {
   double         bonus_proz;
   double         bonus_grenz;
   double         bonus_vj;
   short          fil;
   long           lfd;
   TCHAR          lief[17];
   long           lief_s;
   short          mdn;
};
extern struct LIEF_BONI lief_boni, lief_boni_null;

#line 8 "lief_boni.rh"

class LIEF_BONI_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LIEF_BONI lief_boni;  
               LIEF_BONI_CLASS () : DB_CLASS ()
               {
               }
};
#endif
