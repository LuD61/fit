#pragma once
#include "formfield.h"
#include "CodeProperties.h"

class CUniFormField :
	public CFormField
{
public:
	static CCodeProperties *Code;
	static CCodeProperties NullCode;
	CUniFormField(void);
	CUniFormField(CWnd *Control, int CtrlType, void *Vadr, int VType, int Length=0);
	CUniFormField(CWnd *Control, int CtrlType, void *Vadr, int VType, int len, int Scale);
	~CUniFormField(void);

    virtual void Show ();
    virtual void Get ();
};
