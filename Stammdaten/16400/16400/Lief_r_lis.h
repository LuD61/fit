#ifndef _LIEF_R_LIS_DEF
#define _LIEF_R_LIS_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LIEF_R_LIS {
   double         a;
   short          fil;
   long           lst_nr;
   short          mdn;
   TCHAR          rab_st[5];
   double         rab_wrt;
   TCHAR          rab_typ[3];
   short          waehrung;
};
extern struct LIEF_R_LIS lief_r_lis, lief_r_lis_null;

#line 8 "Lief_r_lis.rh"

class LIEF_R_LIS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LIEF_R_LIS lief_r_lis;  
               LIEF_R_LIS_CLASS () : DB_CLASS ()
               {
               }
};
#endif
