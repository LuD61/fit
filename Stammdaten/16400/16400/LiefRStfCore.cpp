#include "StdAfx.h"
#include "LiefRStfCore.h"
#include "DbTime.h"
#include "StrFuncs.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CLiefRStfCore *CLiefRStfCore::Instance = NULL; 

CLiefRStfCore::CLiefRStfCore(void)
{
	CLIEF_R_STF.sqlin ((short *) &TLIEF_R_STF.mdn, SQLSHORT, 0);
	CLIEF_R_STF.sqlin ((short *) &TLIEF_R_STF.fil, SQLSHORT, 0);
	CLIEF_R_STF.sqlin ((char *)  TLIEF_R_STF.lief, SQLCHAR, sizeof (TLIEF_R_STF.lief));
	CLIEF_R_STF.sqlout ((long *) &TLIEF_R_STF.lfd, SQLLONG, 0);
	m_Cursor = CLIEF_R_STF.sqlcursor (_T("select lfd from lief_r_stf ")
									  _T("where mdn = ? ")
									  _T("and fil = ? ")
									  _T("and lief = ? ")
									  _T("order by lfd"));
	CLIEF_R_STF.sqlin ((short *) &TLIEF_R_STF.mdn, SQLSHORT, 0);
	CLIEF_R_STF.sqlin ((short *) &TLIEF_R_STF.fil, SQLSHORT, 0);
	CLIEF_R_STF.sqlin ((char *)  TLIEF_R_STF.lief, SQLCHAR, sizeof (TLIEF_R_STF.lief));
	m_DeleteCursor = CLIEF_R_STF.sqlcursor (_T("delete from lief_r_stf ")
											_T("where mdn = ? ")
											_T("and fil = ? ")
											_T("and lief = ? "));
	m_ListRow = NULL;
	m_ProzSum = 0.0;
}

CLiefRStfCore::~CLiefRStfCore(void)
{
	CLIEF_R_STF.sqlclose (m_Cursor);
	m_LiefRStfTab.DestroyElements ();
	m_LiefRStfTab.Destroy ();
}

CLiefRStfCore *CLiefRStfCore::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CLiefRStfCore ();
	}
	return Instance;
}

void CLiefRStfCore::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

BOOL CLiefRStfCore::Read ()
{
	BOOL ret = FALSE;
	int dsqlstatus;
	m_LiefRStfTab.DestroyElements ();

	if (m_ListRow != NULL)
	{
		dsqlstatus = CLIEF_R_STF.sqlopen (m_Cursor);
		if (dsqlstatus == 0)
		{
			while (CLIEF_R_STF.sqlfetch (m_Cursor) == 0)
			{
				ret = TRUE;
				DATABASE->ReadLiefRStf ();
				m_ListRow->Add ();
				LIEF_R_STF *tabrow = new LIEF_R_STF;
				memcpy (tabrow, &TLIEF_R_STF, sizeof (LIEF_R_STF)); 
				m_LiefRStfTab.Add (tabrow);
			}
		}
		m_ListRow->Update ();
	}
	return ret;
}

BOOL CLiefRStfCore::Write ()
{
	BOOL ret = TRUE;
	LIEF_R_STF **it;
	LIEF_R_STF *tabrow;

	m_LiefRStfTab.Start ();
	while ((it = m_LiefRStfTab.GetNext ()) != NULL)
	{
		tabrow = *it;
		if (tabrow != NULL)
		{
			if (tabrow->rab_proz > 999.99 ||
				tabrow->rab_proz < -999.99)
			{
				ret = FALSE;
				break;
			}
		}
	}

	if (ret)
	{
		CLIEF_R_STF.sqlexecute (m_DeleteCursor);
		long lfd = 1;
		m_LiefRStfTab.Start ();
		while ((it = m_LiefRStfTab.GetNext ()) != NULL)
		{
			tabrow = *it;
			if (tabrow != NULL && tabrow->rab_proz != 0.0)
			{
				memcpy (&TLIEF_R_STF, &lief_r_stf_null, sizeof (LIEF_R_STF));
				TLIEF_R_STF.mdn = TLIEF.mdn;
				TLIEF_R_STF.fil = TLIEF.fil;
				_tcscpy (TLIEF_R_STF.lief, TLIEF.lief);
				TLIEF_R_STF.lfd = lfd;
				TLIEF_R_STF.rab_proz = tabrow->rab_proz;
				TLIEF_R_STF.abn_wrt =  tabrow->abn_wrt;
				memcpy (tabrow, &TLIEF_R_STF, sizeof (LIEF_R_STF));
				TLIEF_R_STF.lief_s = _tstol (TLIEF_R_STF.lief);
				CLIEF_R_STF.dbupdate ();
				lfd ++;
			}
		}
	}
	return ret;
}


long CLiefRStfCore::NextLfd ()
{
	long lfd = 0l;
	int rows = m_LiefRStfTab.anz;
	if (rows > 0)
	{
		LIEF_R_STF *row = *m_LiefRStfTab.Get (rows - 1);
		if (row != NULL)
		{
			lfd = row->lfd;
		}
	}
	return ++lfd;
}

void CLiefRStfCore::UpdateRow (int row)
{
	int rows = m_LiefRStfTab.anz;
	if (row >= rows)
	{
		LIEF_R_STF *tabrow = new LIEF_R_STF;
		memcpy (tabrow, &TLIEF_R_STF, sizeof (LIEF_R_STF)); 
		m_LiefRStfTab.Add (tabrow);
	}
	else
	{
	    LIEF_R_STF *tabrow = *m_LiefRStfTab.Get (row);
		memcpy (tabrow, &TLIEF_R_STF, sizeof (LIEF_R_STF)); 
	}
	m_ListRow->Update ();
}

void CLiefRStfCore::DeleteRow (int row)
{
    if (m_LiefRStfTab.Get (row) != NULL)
	{
		LIEF_R_STF *tabrow = *m_LiefRStfTab.Get (row);
		m_LiefRStfTab.Drop (row);
		delete tabrow;
		m_ListRow->Update ();
	}
}
