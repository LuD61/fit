#pragma once

class CUpdateHandler
{
public:
	CUpdateHandler(void);
public:
	~CUpdateHandler(void);
	virtual void Update ()=0;
	virtual void UpdateWrite () {}
	virtual BOOL StepBack () 
	{
		return TRUE;
	}
};
