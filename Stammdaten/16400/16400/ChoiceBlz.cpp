#include "stdafx.h"
#include "ChoiceBlz.h"
#include "DbUniCode.h"
#include "DynDialog.h"
#include "DbQuery.h"
#include "Process.h"
#include "windowhandler.h"
#include "StrFuncs.h"
#include "DatabaseCore.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceBlz::Sort1 = -1;
int CChoiceBlz::Sort2 = -1;
int CChoiceBlz::Sort3 = -1;
int CChoiceBlz::Sort4 = -1;
// std::vector<CBlzList *> CChoiceBlz::BlzList;

CChoiceBlz::CChoiceBlz(CWnd* pParent) 
        : CChoiceX(pParent)
{
	HideEnter = FALSE;
	HideFilter = FALSE;
}

CChoiceBlz::~CChoiceBlz() 
{
	DestroyList ();
}

void CChoiceBlz::DestroyList() 
{
	for (std::vector<CBlzList *>::iterator pabl = BlzList.begin (); pabl != BlzList.end (); ++pabl)
	{
		CBlzList *abl = *pabl;
		delete abl;
	}
    BlzList.clear ();
}

BEGIN_MESSAGE_MAP(CChoiceBlz, CChoiceX)
	//{{AFX_MSG_MAP(CChoiceBlz)
//	ON_WM_MOVE ()
END_MESSAGE_MAP()

void CChoiceBlz::FillList () 
{

        char          blz[10];
        char          bank_name[69];
        char          plz[6];
        char          ort[36];

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Zahlungskonditionen"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("BLZ"),       1,  80);
    SetCol (_T("Bankname"),  2, 180);
    SetCol (_T("PLZ"),       3,  80);
    SetCol (_T("Ort"),       4, 180);

	if (FirstRead && !HideFilter && BlzList.size () == 0)
	{
		FirstRead = FALSE;
		Load ();
//		if (!IsModal)
		{
			PostMessage (WM_COMMAND, IDC_FILTER, 0l);
		}
		return;
	}

	if (BlzList.size () == 0)
	{
		CString Sql = _T("");
	    DbClass->sqlout ((TCHAR *) blz,SQLCHAR,10);
	    DbClass->sqlout ((TCHAR *) bank_name,SQLCHAR,69);
	    DbClass->sqlout ((TCHAR *) plz,SQLCHAR,6);
	    DbClass->sqlout ((TCHAR *) ort,SQLCHAR,36);
	    Sql = _T("select blz,  bank_name, plz, ort ")
			  _T("from blz"); 
		if (QueryString != _T(""))
		{
			Sql += _T(" where ");
			Sql += QueryString;
		}

		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe f�r den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}

		DbClass->sqlopen (cursor);
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) blz;
			CDbUniCode::DbToUniCode (blz, pos);
			pos = (LPSTR) bank_name;
			CDbUniCode::DbToUniCode (bank_name, pos);
			pos = (LPSTR) plz;
			CDbUniCode::DbToUniCode (plz, pos);
			pos = (LPSTR) ort;
			CDbUniCode::DbToUniCode (ort, pos);
			CBlzList *abl = new CBlzList ();
			abl->blz = blz;
			abl->bank_name = bank_name;
			abl->plz = plz;
			abl->ort = ort;
			BlzList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CBlzList *>::iterator pabl = BlzList.begin (); pabl != BlzList.end (); ++pabl)
	{
		CBlzList *abl = *pabl;

        int ret = InsertItem (i, -1);
		int pos = 0; 
        ret = SetItemText (abl->blz.GetBuffer (), i, ++pos);
        ret = SetItemText (abl->bank_name.GetBuffer (), i, ++pos);
        ret = SetItemText (abl->plz.GetBuffer (), i, ++pos);
        ret = SetItemText (abl->ort.GetBuffer (), i, ++pos);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
//    FirstSelection ();
	GetDlgItem (IDC_SEARCH)->SetFocus ();
	m_Idx = -1;
}


void CChoiceBlz::SearchItem (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, SortRow);
        iText.MakeUpper ();
		CString cSearch = Search;
		cSearch.MakeUpper ();

        if (_tmemcmp (cSearch.GetBuffer (), iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceBlz::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    EditText.MakeUpper ();
    SearchItem (ListBox, EditText.GetBuffer ());
}

int CChoiceBlz::GetPtBez (LPTSTR item, LPTSTR wert, LPTSTR bez)
{
	TCHAR ptitem [9];
	TCHAR ptwert [5];
	TCHAR ptbez [37];
	static int cursor = -1;

	_tcscpy (ptbez, _T(""));
	if (cursor == -1)
	{
		DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
		DbClass->sqlin ((LPTSTR) ptitem, SQLCHAR, 9);
		DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
		cursor = DbClass->sqlcursor (_T("select ptbez from ptabn where ptitem = ? ")
								_T("and ptwert = ?"));
	}
	strcpy (ptitem, item);
	strcpy (ptwert, wert);
	DbClass->sqlopen (cursor);
	int dsqlstatus = DbClass->sqlfetch (cursor);
	strcpy (bez, ptbez);
    return dsqlstatus;
}

int CChoiceBlz::GetPtBez (LPTSTR item, int wert, LPTSTR bez)
{
	TCHAR ptitem [9];
	TCHAR ptwert [5];
	TCHAR ptbez [37];
	static int cursor = -1;

	_tcscpy (ptbez, _T(""));
	if (cursor == -1)
	{
		DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
		DbClass->sqlin ((LPTSTR) ptitem, SQLCHAR, 9);
		DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
		cursor = DbClass->sqlcursor (_T("select ptbez from ptabn where ptitem = ? ")
								_T("and ptwert = ?"));
	}
	strcpy (ptitem, item);
	sprintf (ptwert,"%d", wert);
	DbClass->sqlopen (cursor);
	int dsqlstatus = DbClass->sqlfetch (cursor);
	strcpy (bez, ptbez);
    return dsqlstatus;
}

int CALLBACK CChoiceBlz::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   int ret = 0;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   ret = (strItem2.CompareNoCase (strItem1));
   switch (SortRow)
   {
	   case 0:
		   ret *= Sort1;
		   break;
	   case 1:
		   ret *= Sort2;
		   break;
	   case 2:
		   ret *= Sort3;
		   break;
	   case 3:
		   ret *= Sort4;
		   break;
   }
   return ret;
}


void CChoiceBlz::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
	}
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CBlzList *abl = BlzList [i];
		   
		   abl->blz        = ListBox->GetItemText (i, 1);
		   abl->bank_name        = ListBox->GetItemText (i, 2);
		   abl->plz        = ListBox->GetItemText (i, 3);
		   abl->ort        = ListBox->GetItemText (i, 4);
	}
	for (int i = 1; i <= 2; i ++)
	{
		SetItemSort (i, 2);
	}
	SetItemSort (SortRow, SortPos);
}

void CChoiceBlz::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = BlzList [idx];
}

CBlzList *CChoiceBlz::GetSelectedText ()
{
	CBlzList *abl = (CBlzList *) SelectedRow;
	return abl;
}


void CChoiceBlz::OnEnter ()
{
}

void CChoiceBlz::OnFilter ()
{
	CDynDialog dlg;
	CDynDialogItem *Item;
	static int headercy = 200;

	dlg.DlgCaption = "Filter";
	dlg.UseFilter = UseFilter;

	if (vFilter.size () > 0)
	{
		for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
		{
			CDynDialogItem *i = *it;
			Item = new CDynDialogItem ();
			*Item = *i;
			if (Item->Name == _T("reset"))
			{
				if (UseFilter)
				{
					Item->Value = _T("aktiv");
				}
				else
				{
					Item->Value = _T("nicht aktiv");
				}
			}
			dlg.AddItem (Item);

		}
		dlg.Header.cy = headercy;	
	}
	else
	{
		int y = 10;
		Item = new CDynDialogItem ();
		Item->Name = _T("lblz");
		Item->CtrClass = "static";
		Item->Value = _T("BLZ");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1001;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("blz");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1002;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lbank_name");
		Item->CtrClass = "static";
		Item->Value = _T("Bankname");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1003;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("bank_name");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lplz");
		Item->CtrClass = "static";
		Item->Value = _T("PLZ");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1005;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("plz");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1006;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lort");
		Item->CtrClass = "static";
		Item->Value = _T("Ort");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1007;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("ort");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1009;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		dlg.Header.cy = y + 30;
		headercy = dlg.Header.cy;

		Item = new CDynDialogItem ();
		Item->Name = _T("OK");
		Item->CtrClass = "button";
		Item->Value = _T("OK");
		Item->Template.x = 15;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDOK;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("cancel");
		Item->CtrClass = "button";
		Item->Value = _T("abbrechen");
		Item->Template.x = 70;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDCANCEL;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("reset");
		Item->CtrClass = "button";
		if (UseFilter)
		{
			Item->Value = _T("aktiv");
		}
		else
		{
			Item->Value = _T("nicht aktiv");
		}
		Item->Template.x = 125;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = ID_USE;
		dlg.AddItem (Item);
	}

	dlg.CreateModal ();
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK) return;
 
	UseFilter = dlg.UseFilter;
	int count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = dlg.Items.begin (); it != dlg.Items.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (count >= (int) vFilter.size ())
			{
				Item = new CDynDialogItem ();
				*Item = *i;
				vFilter.push_back (Item);
			}
			*vFilter[count] = *i;
			count ++;
	}
    CString Query;
    CDbQuery DbQuery;

	QueryString = _T("");
	if (!UseFilter)
	{
		FillList ();
		return;
	}
	count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (strcmp (i->CtrClass, "edit") != 0) continue; 
			if (i->Value.Trim () != _T(""))
			{
				if (i->Type == i->String || i->Type == i->Date)
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, TRUE);
				}
				else
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, FALSE);
				}
				if (count > 0)
				{
					QueryString += _T(" and ");
				}
                QueryString += Query;
				count ++;
			}
	}
	DestroyList ();
	FillList ();
}

void CChoiceBlz::OnMove (int x, int y)
{
	WINDOWHANDLER->IsDockPlace (x, y);
}