#ifndef _CHOICELIEF_DEF
#define _CHOICELIEF_DEF

#include "ChoiceX.h"
#include "LiefList.h"
#include <vector>

class CChoiceLief : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
        static int Sort7;
        static int Sort8;
        static int Sort9;
        static int Sort10;
        static int Sort11;
        static int Sort12;
      
    public :
		long m_Mdn;
		long m_Fil;
	    std::vector<CLiefList *> LiefList;
      	CChoiceLief(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceLief(); 
        virtual void FillList (void);
        void SearchItem (CListCtrl *ListBox, LPTSTR SearchText);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CLiefList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR,LPTSTR );
        int GetPtBez (LPTSTR, int, LPTSTR);
		void DestroyList ();
		virtual void OnEnter ();
	    virtual void OnFilter ();
	    afx_msg void OnMove (int x, int y);

		DECLARE_MESSAGE_MAP()

};
#endif
