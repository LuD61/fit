#include "StdAfx.h"
#include "LiefBoniListCtrl.h"
#include "LiefBoniCore.h"
#include "DatabaseCore.h"
#include "StrFuncs.h"
#include "resource.h"

CLiefBoniListCtrl::CLiefBoniListCtrl(void)
{
	ListRows.Init ();
}

CLiefBoniListCtrl::~CLiefBoniListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CLiefBoniListCtrl, CEditListCtrl)
//	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CLiefBoniListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (1, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (1, 0);
	}
}

void CLiefBoniListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	int count = GetHeaderCtrl ()->GetItemCount ();
	while (IsDisplayOnly (col) && col < count - 1) col ++;
	CEditListCtrl::StartEnter (col, row);
	if (col == PosBonusGrenz)
	{
		ListEdit.SetLimitText (9);
	}
	else if (col == PosBonusProz)
	{
		ListEdit.SetLimitText (8);
	}
}

void CLiefBoniListCtrl::StopEnter ()
{
	CString cLfd;
	CString cBonusProz;
	CString cBonusGrenz;
	CString cBonusVj;
	CEditListCtrl::StopEnter ();
	UpdateRow ();
	cLfd.Format (_T("%ld"), TLIEF_BONI.lfd);
	cBonusProz.Format  (_T("%.2lf"), TLIEF_BONI.bonus_proz);
	cBonusGrenz.Format (_T("%.3lf"), TLIEF_BONI.bonus_grenz);
	cBonusVj.Format    (_T("%.3lf"), TLIEF_BONI.bonus_vj);
	FillList.SetItemText (cLfd.GetBuffer (), EditRow, 1);
	FillList.SetItemText (cBonusProz.GetBuffer (), EditRow, 2);
	FillList.SetItemText (cBonusGrenz.GetBuffer (), EditRow, 3);
	FillList.SetItemText (cBonusVj.GetBuffer (), EditRow, 4);
}

void CLiefBoniListCtrl::SetSel (CString& Text)
{
/*
   {
		ListEdit.SetSel (0, -1);
   }
*/
   if (EditCol == PosBonusProz)
   {
		ListEdit.SetSel (0, -1);
   }
   else if (EditCol == PosBonusGrenz)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CLiefBoniListCtrl::FormatText (CString& Text)
{
}

void CLiefBoniListCtrl::NextRow ()
{
	int count = GetItemCount ();
	SetEditText ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;

	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
	StartEnter (EditCol, EditRow);
}

void CLiefBoniListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CLiefBoniListCtrl::LastCol ()
{
	if (EditCol < PosBonusGrenz) return FALSE;
	return TRUE;
}

void CLiefBoniListCtrl::UpdateRow ()
{
	CString cLfd         =  GetItemText (EditRow, PosLfd);
	CString cBonusProz   =  GetItemText (EditRow, PosBonusProz);
	CString cBonusGrenz  =  GetItemText (EditRow, PosBonusGrenz);
	CString cBonusVj     =  GetItemText (EditRow, PosBonusVj);
	TLIEF_BONI.lfd      = _tstol (cLfd.GetBuffer ());
	TLIEF_BONI.bonus_proz = CStrFuncs::StrToDouble (cBonusProz);
	TLIEF_BONI.bonus_grenz = CStrFuncs::StrToDouble (cBonusGrenz);
	TLIEF_BONI.bonus_vj    = CStrFuncs::StrToDouble (cBonusVj);
	LIEF_BONI_CORE->UpdateRow (EditRow);
}

void CLiefBoniListCtrl::OnReturn ()
{

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (LastCol ())
	{
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
//		EditCol = 0;
		if (EditRow == rowCount)
		{
			EditCol = 1;
		}
		else
		{
			EditCol = 2;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
		if (IsDisplayOnly (EditCol))
		{
			EditCol ++;
		}
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CLiefBoniListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CLiefBoniListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	StopEnter ();
	EditCol --;
	while (IsDisplayOnly (EditCol) && EditCol >= 0) EditCol --;
    StartEnter (EditCol, EditRow);
}

BOOL CLiefBoniListCtrl::InsertRow ()
{
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T("0"), EditRow, 1);
	FillList.SetItemText (_T("0,00"), EditRow, 2);
	FillList.SetItemText (_T("0,000  "), EditRow, 3);
	FillList.SetItemText (_T("0,000  "), EditRow, 4);
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CLiefBoniListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	int row = EditRow;
	BOOL ret = CEditListCtrl::DeleteRow ();
	LIEF_BONI_CORE->DeleteRow (row);
	return ret;
}

BOOL CLiefBoniListCtrl::AppendEmpty ()
{
	long lfd = 0;
	CString cLfd;
	int rowCount = GetItemCount ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CWnd *header = GetHeaderCtrl ();
	lfd = LIEF_BONI_CORE->NextLfd ();
	FillList.InsertItem (rowCount, -1);
	cLfd.Format (_T("%ld"), lfd);
	FillList.SetItemText (cLfd.GetBuffer (), rowCount, PosLfd);
	FillList.SetItemText (_T("0,00"), rowCount, PosBonusProz);
	FillList.SetItemText (_T("0,000"), rowCount, PosBonusGrenz);
	FillList.SetItemText (_T("0,000"), rowCount, PosBonusVj);
	rowCount = GetItemCount ();
	return TRUE;
}

void CLiefBoniListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CLiefBoniListCtrl::RunItemClicked (int Item)
{
}

void CLiefBoniListCtrl::RunCtrlItemClicked (int Item)
{
}

void CLiefBoniListCtrl::RunShiftItemClicked (int Item)
{
}

void CLiefBoniListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	Text = cText.Trim ();
}

void CLiefBoniListCtrl::ScrollPositions (int pos)
{
}

