#include "stdafx.h"
#include "lief_r_stf.h"

struct LIEF_R_STF lief_r_stf, lief_r_stf_null, lief_r_stf_def;

void LIEF_R_STF_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &lief_r_stf.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_stf.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_stf.lief,   SQLCHAR, sizeof (lief_r_stf.lief));
            sqlin ((long *)   &lief_r_stf.lfd,   SQLLONG, 0);
    sqlout ((double *) &lief_r_stf.abn_wrt,SQLDOUBLE,0);
    sqlout ((short *) &lief_r_stf.fil,SQLSHORT,0);
    sqlout ((long *) &lief_r_stf.lfd,SQLLONG,0);
    sqlout ((TCHAR *) lief_r_stf.lief,SQLCHAR,17);
    sqlout ((long *) &lief_r_stf.lief_s,SQLLONG,0);
    sqlout ((short *) &lief_r_stf.mdn,SQLSHORT,0);
    sqlout ((double *) &lief_r_stf.rab_proz,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select ")
_T("lief_r_stf.abn_wrt,  lief_r_stf.fil,  lief_r_stf.lfd,  ")
_T("lief_r_stf.lief,  lief_r_stf.lief_s,  lief_r_stf.mdn,  ")
_T("lief_r_stf.rab_proz from lief_r_stf ")

#line 15 "Lief_r_stf.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
    sqlin ((double *) &lief_r_stf.abn_wrt,SQLDOUBLE,0);
    sqlin ((short *) &lief_r_stf.fil,SQLSHORT,0);
    sqlin ((long *) &lief_r_stf.lfd,SQLLONG,0);
    sqlin ((TCHAR *) lief_r_stf.lief,SQLCHAR,17);
    sqlin ((long *) &lief_r_stf.lief_s,SQLLONG,0);
    sqlin ((short *) &lief_r_stf.mdn,SQLSHORT,0);
    sqlin ((double *) &lief_r_stf.rab_proz,SQLDOUBLE,0);
            sqltext = _T("update lief_r_stf set ")
_T("lief_r_stf.abn_wrt = ?,  lief_r_stf.fil = ?,  lief_r_stf.lfd = ?,  ")
_T("lief_r_stf.lief = ?,  lief_r_stf.lief_s = ?,  lief_r_stf.mdn = ?,  ")
_T("lief_r_stf.rab_proz = ? ")

#line 20 "Lief_r_stf.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?");
            sqlin ((short *)   &lief_r_stf.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_stf.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_stf.lief,   SQLCHAR, sizeof (lief_r_stf.lief));
            sqlin ((long *)   &lief_r_stf.lfd,   SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &lief_r_stf.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_stf.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_stf.lief,   SQLCHAR, sizeof (lief_r_stf.lief));
            sqlin ((long *)   &lief_r_stf.lfd,   SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select lief from lief_r_stf ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
            sqlin ((short *)   &lief_r_stf.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_stf.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_stf.lief,   SQLCHAR, sizeof (lief_r_stf.lief));
            sqlin ((long *)   &lief_r_stf.lfd,   SQLLONG, 0);
            test_lock_cursor = sqlcursor (_T("update lief_r_stf set fil = fil ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
            sqlin ((short *)   &lief_r_stf.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_stf.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_stf.lief,   SQLCHAR, sizeof (lief_r_stf.lief));
            sqlin ((long *)   &lief_r_stf.lfd,   SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from lief_r_stf ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
    sqlin ((double *) &lief_r_stf.abn_wrt,SQLDOUBLE,0);
    sqlin ((short *) &lief_r_stf.fil,SQLSHORT,0);
    sqlin ((long *) &lief_r_stf.lfd,SQLLONG,0);
    sqlin ((TCHAR *) lief_r_stf.lief,SQLCHAR,17);
    sqlin ((long *) &lief_r_stf.lief_s,SQLLONG,0);
    sqlin ((short *) &lief_r_stf.mdn,SQLSHORT,0);
    sqlin ((double *) &lief_r_stf.rab_proz,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into lief_r_stf (")
_T("abn_wrt,  fil,  lfd,  lief,  lief_s,  mdn,  rab_proz) ")

#line 58 "Lief_r_stf.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?)"));

#line 60 "Lief_r_stf.rpp"
}
