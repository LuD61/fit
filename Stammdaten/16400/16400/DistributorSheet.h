#pragma once
#include "dbpropertysheet.h"

class CDistributorSheet :
	public CDbPropertySheet
{
public:
	CDistributorSheet(void);
	CDistributorSheet(LPTSTR title);
	DECLARE_DYNCREATE(CDistributorSheet)
public:
	~CDistributorSheet(void);
protected:
protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog ();
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
public:
	virtual BOOL StepBack ();
	virtual BOOL Write ();
	virtual BOOL DeleteAll ();
};
