#ifndef _LIEF_R_GR_DEF
#define _LIEF_R_GR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LIEF_R_GR {
   short          fil;
   short          grp;
   long           lfd;
   TCHAR          lief[17];
   long           lief_s;
   short          mdn;
   double         rab_proz;
   double         ums_grenz;
};
extern struct LIEF_R_GR lief_r_gr, lief_r_gr_null;

#line 8 "Lief_r_gr.rh"

class LIEF_R_GR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LIEF_R_GR lief_r_gr;  
               LIEF_R_GR_CLASS () : DB_CLASS ()
               {
               }
};
#endif
