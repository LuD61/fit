#pragma once
#include "resource.h"
#include "distributorcore.h"
#include "StaticButton.h"
#include "ColorButton.h"
#include "CtrlGridColor.h"
#include "FormTab.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "afxwin.h"
#include "BkBitmap.h"
#include "VLabel.h"


// CZahlKondDialog-Dialogfeld

class CZahlKondDialog : public CDialog
{
	DECLARE_DYNAMIC(CZahlKondDialog)

public:
	CZahlKondDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CZahlKondDialog();

// Dialogfelddaten
	enum { IDD = IDD_ZAHL_KOND_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) ;
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    BOOL OnReturn ();
    BOOL OnDown ();
    BOOL OnKeyup ();
	DECLARE_MESSAGE_MAP()

private:
	COLORREF m_DlgColor;
	HBRUSH m_DlgBrush;
	CBkBitmap BkBitmap;
	CFormTab Form;
	CFont Font;
	CFont CaptionFont;
	CFont StaticFont;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid ZahlKondGrid;
	CCtrlGrid ControlGrid;
	BOOL m_ReadOnly;

public:
	void SetReadOnly (BOOL readonly=TRUE)
	{
		m_ReadOnly = readonly;
	}

	CLabel m_Caption;
	CColorButton m_OkEx;
	CColorButton m_CancelEx;

	CStatic m_ZahlKondGroup;
	CStatic m_LZahlKond;
	CNumEdit m_ZahlKond;
	CColorButton m_ZahlKondChoice;
	CColorButton m_EnterZahlKond;
	CNumEdit m_Ziel1;
	CNumEdit m_Ziel2;
	CNumEdit m_Ziel3;
	CTextEdit m_Txt;
	CStatic m_LZiel1;
	CStatic m_LZiel2;
	CStatic m_LZiel3;
	CDecimalEdit m_Skto1;
	CDecimalEdit m_Skto2;
	CDecimalEdit m_Skto3;
	CStatic m_LSkto1;
	CStatic m_LSkto2;
	CStatic m_LSkto3;
	CStatic m_LTxt;

public:
	CButton m_OK;
public:
	CButton m_Cancel;
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedCancel();
};
