#include "stdafx.h"
#include "lief_r_gr.h"

struct LIEF_R_GR lief_r_gr, lief_r_gr_null, lief_r_gr_def;

void LIEF_R_GR_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &lief_r_gr.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_gr.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_gr.lief,   SQLCHAR, sizeof (lief_r_gr.lief));
            sqlin ((long *)   &lief_r_gr.lfd,   SQLLONG, 0);
    sqlout ((short *) &lief_r_gr.fil,SQLSHORT,0);
    sqlout ((short *) &lief_r_gr.grp,SQLSHORT,0);
    sqlout ((long *) &lief_r_gr.lfd,SQLLONG,0);
    sqlout ((TCHAR *) lief_r_gr.lief,SQLCHAR,17);
    sqlout ((long *) &lief_r_gr.lief_s,SQLLONG,0);
    sqlout ((short *) &lief_r_gr.mdn,SQLSHORT,0);
    sqlout ((double *) &lief_r_gr.rab_proz,SQLDOUBLE,0);
    sqlout ((double *) &lief_r_gr.ums_grenz,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select lief_r_gr.fil,  ")
_T("lief_r_gr.grp,  lief_r_gr.lfd,  lief_r_gr.lief,  lief_r_gr.lief_s,  ")
_T("lief_r_gr.mdn,  lief_r_gr.rab_proz,  lief_r_gr.ums_grenz from lief_r_gr ")

#line 15 "Lief_r_gr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
    sqlin ((short *) &lief_r_gr.fil,SQLSHORT,0);
    sqlin ((short *) &lief_r_gr.grp,SQLSHORT,0);
    sqlin ((long *) &lief_r_gr.lfd,SQLLONG,0);
    sqlin ((TCHAR *) lief_r_gr.lief,SQLCHAR,17);
    sqlin ((long *) &lief_r_gr.lief_s,SQLLONG,0);
    sqlin ((short *) &lief_r_gr.mdn,SQLSHORT,0);
    sqlin ((double *) &lief_r_gr.rab_proz,SQLDOUBLE,0);
    sqlin ((double *) &lief_r_gr.ums_grenz,SQLDOUBLE,0);
            sqltext = _T("update lief_r_gr set ")
_T("lief_r_gr.fil = ?,  lief_r_gr.grp = ?,  lief_r_gr.lfd = ?,  ")
_T("lief_r_gr.lief = ?,  lief_r_gr.lief_s = ?,  lief_r_gr.mdn = ?,  ")
_T("lief_r_gr.rab_proz = ?,  lief_r_gr.ums_grenz = ? ")

#line 20 "Lief_r_gr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?");
            sqlin ((short *)   &lief_r_gr.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_gr.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_gr.lief,   SQLCHAR, sizeof (lief_r_gr.lief));
            sqlin ((long *)   &lief_r_gr.lfd,   SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &lief_r_gr.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_gr.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_gr.lief,   SQLCHAR, sizeof (lief_r_gr.lief));
            sqlin ((long *)   &lief_r_gr.lfd,   SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select lief from lief_r_gr ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
            sqlin ((short *)   &lief_r_gr.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_gr.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_gr.lief,   SQLCHAR, sizeof (lief_r_gr.lief));
            sqlin ((long *)   &lief_r_gr.lfd,   SQLLONG, 0);
            test_lock_cursor = sqlcursor (_T("update lief_r_gr set fil = fil ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
            sqlin ((short *)   &lief_r_gr.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_gr.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_gr.lief,   SQLCHAR, sizeof (lief_r_gr.lief));
            sqlin ((long *)   &lief_r_gr.lfd,   SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from lief_r_gr ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
    sqlin ((short *) &lief_r_gr.fil,SQLSHORT,0);
    sqlin ((short *) &lief_r_gr.grp,SQLSHORT,0);
    sqlin ((long *) &lief_r_gr.lfd,SQLLONG,0);
    sqlin ((TCHAR *) lief_r_gr.lief,SQLCHAR,17);
    sqlin ((long *) &lief_r_gr.lief_s,SQLLONG,0);
    sqlin ((short *) &lief_r_gr.mdn,SQLSHORT,0);
    sqlin ((double *) &lief_r_gr.rab_proz,SQLDOUBLE,0);
    sqlin ((double *) &lief_r_gr.ums_grenz,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into lief_r_gr (")
_T("fil,  grp,  lfd,  lief,  lief_s,  mdn,  rab_proz,  ums_grenz) ")

#line 58 "Lief_r_gr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?)"));

#line 60 "Lief_r_gr.rpp"
}
