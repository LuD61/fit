#include "StdAfx.h"
#include "LiefRStfListCtrl.h"
#include "LiefRStfCore.h"
#include "DatabaseCore.h"
#include "StrFuncs.h"
#include "resource.h"

CLiefRStfListCtrl::CLiefRStfListCtrl(void)
{
	ListRows.Init ();
}

CLiefRStfListCtrl::~CLiefRStfListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CLiefRStfListCtrl, CEditListCtrl)
//	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CLiefRStfListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (1, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (1, 0);
	}
}

void CLiefRStfListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	int count = GetHeaderCtrl ()->GetItemCount ();
	while (IsDisplayOnly (col) && col < count - 1) col ++;
	CEditListCtrl::StartEnter (col, row);
	if (col == PosAbnWrt)
	{
		ListEdit.SetLimitText (9);
	}
	else if (col == PosRab)
	{
		ListEdit.SetLimitText (8);
	}
}

void CLiefRStfListCtrl::StopEnter ()
{
	CString cLfd;
	CString cRabatt;
	CString cAbnWrt;

	CEditListCtrl::StopEnter ();
	UpdateRow ();
	cLfd.Format (_T("%ld"), TLIEF_R_STF.lfd);
	cRabatt.Format (_T("%.2lf"), TLIEF_R_STF.rab_proz);
	cAbnWrt.Format (_T("%.3lf"), TLIEF_R_STF.abn_wrt);
	FillList.SetItemText (cLfd.GetBuffer (), EditRow, 1);
	FillList.SetItemText (cRabatt.GetBuffer (), EditRow, 2);
	FillList.SetItemText (cAbnWrt.GetBuffer (), EditRow, 3);
}

void CLiefRStfListCtrl::SetSel (CString& Text)
{
/*
   {
		ListEdit.SetSel (0, -1);
   }
*/
   if (EditCol == PosRab)
   {
		ListEdit.SetSel (0, -1);
   }
   else if (EditCol == PosAbnWrt)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CLiefRStfListCtrl::FormatText (CString& Text)
{
}

void CLiefRStfListCtrl::NextRow ()
{
	int count = GetItemCount ();
	SetEditText ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;

	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
	StartEnter (EditCol, EditRow);
}

void CLiefRStfListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CLiefRStfListCtrl::LastCol ()
{
	if (EditCol < PosAbnWrt) return FALSE;
	return TRUE;
}

void CLiefRStfListCtrl::UpdateRow ()
{
	CString cLfd         =  GetItemText (EditRow, PosLfd);
	CString cRab         =  GetItemText (EditRow, PosRab);
	CString cRabBz       =  GetItemText (EditRow, PosAbnWrt);
	TLIEF_R_STF.lfd      = _tstol (cLfd.GetBuffer ());
	TLIEF_R_STF.rab_proz = CStrFuncs::StrToDouble (cRab);
	TLIEF_R_STF.abn_wrt = CStrFuncs::StrToDouble (cRabBz);
	LIEF_R_STF_CORE->UpdateRow (EditRow);
}

void CLiefRStfListCtrl::OnReturn ()
{

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (LastCol ())
	{
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
//		EditCol = 0;
		if (EditRow == rowCount)
		{
			EditCol = 1;
		}
		else
		{
			EditCol = 2;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
		if (IsDisplayOnly (EditCol))
		{
			EditCol ++;
		}
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CLiefRStfListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CLiefRStfListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	StopEnter ();
	EditCol --;
	while (IsDisplayOnly (EditCol) && EditCol >= 0) EditCol --;
    StartEnter (EditCol, EditRow);
}

BOOL CLiefRStfListCtrl::InsertRow ()
{
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T("  "), EditRow, 1);
	FillList.SetItemText (_T("  "), EditRow, 2);
	FillList.SetItemText (_T("  "), EditRow, 3);
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CLiefRStfListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	int row = EditRow;
	BOOL ret = CEditListCtrl::DeleteRow ();
	LIEF_R_STF_CORE->DeleteRow (row);
	return ret;
}

BOOL CLiefRStfListCtrl::AppendEmpty ()
{
	long lfd = 0;
	CString cLfd;
	int rowCount = GetItemCount ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CWnd *header = GetHeaderCtrl ();
	lfd = LIEF_R_STF_CORE->NextLfd ();
	FillList.InsertItem (rowCount, -1);
	cLfd.Format (_T("%ld"), lfd);
	FillList.SetItemText (cLfd.GetBuffer (), rowCount, PosLfd);
	FillList.SetItemText (_T("0,00"), rowCount, PosRab);
	FillList.SetItemText (_T(" "), rowCount, PosAbnWrt);
	rowCount = GetItemCount ();
	return TRUE;
}

void CLiefRStfListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CLiefRStfListCtrl::RunItemClicked (int Item)
{
}

void CLiefRStfListCtrl::RunCtrlItemClicked (int Item)
{
}

void CLiefRStfListCtrl::RunShiftItemClicked (int Item)
{
}

void CLiefRStfListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	Text = cText.Trim ();
}

void CLiefRStfListCtrl::ScrollPositions (int pos)
{
}

