#pragma once
#include "DbClass.h"
#include "Lief.h"
#include "Adr.h"
#include "Mdn.h"
#include "Fil.h"
#include "Plz.h"
#include "Ptabn.h"
#include "zahl_kond.h"
#include "lief_r_kte.h"
#include "lief_r_stf.h"
#include "lief_r_gr.h"
#include "lief_r_lis.h"
#include "lief_boni.h"
#include "a_bas.h"
#include "ag.h"

class CDatabaseCore
{
public :
	enum SQL_STATE
	{
		SqlError = -1,
		SqlOk = 0,
		SqlNoData = 100,
		SqlLock = 101,
	};
	enum ADR_TYP
	{
		Distributor = 30,
	};

private:
	static CDatabaseCore *Instance; 
	class WG_CLASS : public DB_CLASS
	{
	private:
		short m_Wg;
		TCHAR m_WgBz1[25];  
		TCHAR m_WgBz2[25];  
		virtual void prepare ();
	public:
		void set_Wg (short wg)
		{
			m_Wg = wg;
		}
		short Wg ()
		{
			return m_Wg;
		}
   
		void set_WgBz1 (LPTSTR wgBz1)
		{
			_tcscpy (m_WgBz1, wgBz1);
		}


		LPTSTR WgBz1 ()
		{
			return m_WgBz1;
		}

		void set_WgBz2 (LPTSTR wgBz2)
		{
			_tcscpy (m_WgBz2, wgBz2);
		}

		LPTSTR WgBz2 ()
		{
			return m_WgBz2;
		}
	} Wg;

	DB_CLASS Dbase;
	SQL_STATE m_SqlState;
protected:
	CDatabaseCore(void);
	~CDatabaseCore(void);
public:
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	FIL_CLASS Fil;
	ADR_CLASS FilAdr;
	LIEF_CLASS Lief;
	ADR_CLASS LiefAdr;
	PLZ_CLASS Plz;
	PTABN_CLASS Ptabn;
	ZAHL_KOND_CLASS Zahl_kond;
	ZAHL_KOND m_ZahlKondSave;
	LIEF_R_KTE_CLASS Lief_r_kte;
	LIEF_R_STF_CLASS Lief_r_stf;
	LIEF_R_GR_CLASS Lief_r_gr;
	LIEF_R_LIS_CLASS Lief_r_lis;
	LIEF_BONI_CLASS Lief_boni;
	A_BAS_CLASS A_bas;
	AG_CLASS Ag;
	CString m_LiefRLisBez1;
	CString m_LiefRLisBez2;

	LPTSTR LiefRLisBez1 ()
	{
		return m_LiefRLisBez1.GetBuffer ();
	}

	LPTSTR LiefRLisBez2 ()
	{
		return m_LiefRLisBez2.GetBuffer ();
	}

	SQL_STATE GetSqlState ()
	{
		return m_SqlState;
	}
	static CDatabaseCore *GetInstance ();
	static void DestroyInstance ();
	void Opendbase ();
	void InitMdn ();
	BOOL ReadMdn ();
	void InitFil ();
	BOOL ReadFil ();
	void InitLief ();
	void InitAdr ();
	BOOL ReadAdr ();
	BOOL ReadLief ();
	BOOL WriteLief ();
	BOOL ReadZahlKond ();
	void SaveZahlKond ();
	void RestoreZahlKond ();
	BOOL ReadLiefRKte ();
	BOOL ReadLiefRStf ();
	BOOL ReadLiefRGr ();
	BOOL ReadLiefRLis ();
	BOOL ReadLiefRLisBez (); 
	BOOL ReadLiefBoni ();
};

#define DATABASE CDatabaseCore::GetInstance ()
#define SQLSTATE DATABASE->GetSqlState ()


#define CABAS DATABASE->A_bas
#define CAG DATABASE->Ag
#define CMDN DATABASE->Mdn
#define CMDN_ADR DATABASE->MdnAdr
#define CFIL DATABASE->Fil
#define CFIL_ADR DATABASE->FilAdr
#define CLIEF DATABASE->Lief
#define CLIEF_ADR DATABASE->LiefAdr
#define CPTABN DATABASE->Ptabn
#define CZAHL_KOND DATABASE->Zahl_kond
#define CLIEF_R_KTE DATABASE->Lief_r_kte
#define CLIEF_R_STF DATABASE->Lief_r_stf
#define CLIEF_R_GR DATABASE->Lief_r_gr
#define CLIEF_R_LIS DATABASE->Lief_r_lis
#define CLIEF_BONI DATABASE->Lief_boni

#define TABAS DATABASE->A_bas.a_bas
#define TAG DATABASE->Ag.a_bas
#define TMDN DATABASE->Mdn.mdn
#define TMDN_ADR DATABASE->MdnAdr.adr
#define TFIL DATABASE->Fil.fil
#define TFIL_ADR DATABASE->FilAdr.adr
#define TLIEF DATABASE->Lief.lief
#define TLIEF_ADR DATABASE->LiefAdr.adr
#define TPTABN DATABASE->Ptabn.ptabn
#define TZAHL_KOND DATABASE->Zahl_kond.zahl_kond
#define TLIEF_R_KTE DATABASE->Lief_r_kte.lief_r_kte
#define TLIEF_R_STF DATABASE->Lief_r_stf.lief_r_stf
#define TLIEF_R_GR DATABASE->Lief_r_gr.lief_r_gr
#define TLIEF_R_LIS DATABASE->Lief_r_lis.lief_r_lis
#define TLIEF_BONI DATABASE->Lief_boni.lief_boni
#define LIEF_R_LIS_BZ1 DATABASE->LiefRLisBez1 ()
#define LIEF_R_LIS_BZ2 DATABASE->LiefRLisBez2 ()
