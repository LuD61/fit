#pragma once
#include "DbPropertyPage.h"
#include "resource.h"
#include "distributorcore.h"
#include "StaticButton.h"
#include "ColorButton.h"
#include "EnterPlz.h"
#include "CtrlGridColor.h"
#include "FormTab.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "afxwin.h"
#include "DataCollection.h"
#include "ChoiceMdn.h"
#include "ChoiceFil.h"
#include "ChoiceLief.h"
#include "ChoiceAdr.h"
#include "EnterPlz.h"
#include "UpdateHandler.h"

#define IDC_MDNCHOICE 4001
#define IDC_FILCHOICE 4002
#define IDC_LIEFCHOICE 4032
#define IDC_ADRCHOICE 4004
#define IDC_PLZCHOICE 4006

// CBasePage-Dialogfeld

class CBasePage : public CDbPropertyPage,
	                     CUpdateHandler     
{
	DECLARE_DYNAMIC(CBasePage)
private:
	BOOL m_CanWrite;
	CDataCollection<CWnd *> HeadControls;

public:
	BOOL CanWrite ()
	{
		return m_CanWrite;
	}

	CBasePage();
	virtual ~CBasePage();

// Dialogfelddaten
	enum { IDD = IDD_BASE_PAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnDestroy ();

private:
	CEnterPlz EnterPlz;
	CFormTab Form;

	CFont Font;
	CFont lFont;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid FilGrid;
	CCtrlGrid LiefGrid;
	CCtrlGrid AdrGrid;
	CCtrlGrid TelFaxGrid;
	CCtrlGrid PLZPostfachGrid;
	CCtrlGrid VertreterGrid;
	CCtrlGrid InkassoGrid;
	CCtrlGrid UnterGruppeGrid;
	CCtrlGrid PrGrGrid;
	CCtrlGrid PrListGrid;
	CCtrlGrid PlzGrid;

public:
// keyfields
	CChoiceLief *Choice;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceFil *ChoiceFil;
	BOOL ModalChoiceFil;
	CChoiceAdr *ChoiceAdr;
	BOOL ModalChoiceAdr;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CColorButton m_MdnChoice;
	CEdit m_MdnName;
	CStatic m_LFil;
	CNumEdit m_Fil;
	CColorButton m_FilChoice;
	CEdit m_FilName;
	CStatic m_LLief;
	CTextEdit m_Lief;
	CColorButton m_LiefChoice;
	CEdit m_LiefName;
	CStatic m_HeadBorder;
	CStatic m_DataBorder;

//adressfields
	CStatic m_LAnr;
	CComboBox m_Anr;
	CStatic m_LAdr;
	CNumEdit m_Adr;
	CColorButton m_EnterPartner;
	CColorButton m_AdrChoice;
	CStatic m_LName1;
	CTextEdit m_Name1;
	CTextEdit m_Name2;
	CStatic m_LName2;
	CTextEdit m_Name3;
	CStatic m_LName3;
	CTextEdit m_Strasse;
	CStatic m_LStrasse;
	CNumEdit m_Plz;
	CColorButton m_PlzChoice;
	CStatic m_LPlz;
	CTextEdit m_Ort;
	CStatic m_LOrt;
	CStatic m_LPostfach;
	CNumEdit m_Postfach;
	CNumEdit m_Telefon;
	CStatic m_LTelefon;
	CNumEdit m_Fax;
	CNumEdit m_plzPostfach;
	CNumEdit m_EMAIL;
	CStatic m_LEmail;
	CTextEdit m_Partner;
	CStatic m_LPartner;
	CStatic m_DataBorder2;
	CComboBox m_Staat;
	CStatic m_LStaat;
	CComboBox m_Land;
	CButton m_IncludeLand;
	BOOL IncludeLand;
	CStatic m_LLand;

// distributor data
	CStatic m_LSprache;
	CComboBox m_Sprache;
	CStatic m_LBestTransKz;
	CComboBox m_BestTransKz;
	CStatic m_LMeKz;
	CComboBox m_MeKz;
	CStatic m_LLiefRht;
	CComboBox m_LiefRht;
	CStatic m_LWaehrung;
	CComboBox m_Waehrung;
	CStatic m_LWeKont;
	CTextEdit m_WeKont;
	CStatic m_LWeToler;
	CDecimalEdit m_WeToler;
	CStatic m_LLiefTyp;
	CComboBox m_LiefTyp;
	CStatic m_LIln;
    CTextEdit m_Iln;
	CStatic m_LLiefZeit;
	CNumEdit m_LiefZeit;
	CStatic m_LLiefKun;
	CTextEdit m_LiefKun;
	CStatic m_LKreditor;
	CNumEdit m_Kreditor;
	CStatic m_LBestSort;
	CComboBox m_BestSort;
	CButton m_NachLief;
	CButton m_BonusKz;

//	void Register ();

	void FillPtabCombo (CWnd *Control, LPTSTR Item);
    BOOL OnReturn ();
    BOOL OnKeyup ();
	BOOL ReadMdn ();
	BOOL ReadFil ();
	BOOL Read ();
	BOOL Write ();
	BOOL DeleteAll ();
	void EnableFields (BOOL b);
	void EnableHeadControls (BOOL b);
	void OnChoice ();
    void OnSelected ();
    void OnCanceled ();

	virtual void Update ();
	virtual void UpdateWrite ();
	virtual BOOL StepBack ();
	void DisableTabControl (BOOL disable);
    void OnMdnchoice(); 
    void OnFilchoice(); 
	void OnAdrChoice();
	void OnPlzChoice();
	void OnEnterPartner();
};
