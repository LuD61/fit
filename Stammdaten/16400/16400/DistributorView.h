// 16400View.h : Schnittstelle der Klasse CDistributorView
//


#pragma once
#include "DistributorSheet.h"
#include "BasePage.h"
#include "BasePage2.h"

class CDistributorView : public CFormView
{
protected: // Nur aus Serialisierung erstellen
	CDistributorView();
	DECLARE_DYNCREATE(CDistributorView)

public:
	enum{ IDD = IDD_16400_FORM };

// Attribute
private:
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
public:
	CMy16400Doc* GetDocument() const;

// Vorgänge
public:

// Überschreibungen
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);

// Implementierung
public:
	virtual ~CDistributorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
public :
	afx_msg void OnFileSave();
	afx_msg void OnDelete();
	afx_msg void OnUpdateFileSave(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDelete(CCmdUI *pCmdUI);
    afx_msg void OnSize(UINT, int, int);
    afx_msg void OnBack();

	CDistributorSheet *m_Sheet;
	CBasePage *m_BasePage;
	CBasePage2 *m_BasePage2;
private:
	CSize StartSize;
};

#ifndef _DEBUG  // Debugversion in 16400View.cpp
inline CMy16400Doc* CDistributorView::GetDocument() const
   { return reinterpret_cast<CMy16400Doc*>(m_pDocument); }
#endif

