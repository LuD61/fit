#pragma once
#include "DatabaseCore.h"
#include "ListRow.h"
#include "DataCollection.h"

class CLiefBoniCore
{
private:
	static CLiefBoniCore *Instance; 
	int m_Cursor;
	int m_DeleteCursor;
	int m_Rows;
	CListRow *m_ListRow;
	CDataCollection<LIEF_BONI *> m_LiefBoniTab;

public:

	double m_ProzSum;
	void SetListRow (CListRow *ListRow)
	{
		m_ListRow = ListRow;
	}

	void SetRow (int rows)
	{
		m_Rows = rows;
	}

	int GetRow ()
	{
		return m_Rows;
	}

	CLiefBoniCore(void);
	~CLiefBoniCore(void);

	static CLiefBoniCore *GetInstance ();
	static void DestroyInstance ();
	BOOL Read ();
	BOOL Write ();
	long NextLfd ();
	void UpdateRow (int row);
	void DeleteRow (int row);
};

#define LIEF_BONI_CORE CLiefBoniCore::GetInstance ()