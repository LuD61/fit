#include "StdAfx.h"
#include "dbformview.h"

IMPLEMENT_DYNCREATE(DbFormView, CFormView)

DbFormView::DbFormView() : 
			CFormView ((UINT) 0)
{
}

DbFormView::DbFormView(UINT Id) : CFormView (Id)
{
}

DbFormView::~DbFormView(void)
{
}


DROPEFFECT DbFormView::OnDrop (CWnd* pWnd,  COleDataObject* pDataObject,
					                 	  DROPEFFECT dropDefault, DROPEFFECT dropList,
										  CPoint point)
{
	return DROPEFFECT_NONE;
}

void DbFormView::write ()
{
}

BOOL DbFormView::TextCent ()
{
	return TRUE;
}

BOOL DbFormView::TextLeft ()
{
	return TRUE;
}

BOOL DbFormView::TextRight ()
{
	return TRUE;
}

void DbFormView::Save ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	File.Open (Path.GetBuffer (), CFile::modeCreate | CFile::modeWrite);
	CArchive archive(&File, CArchive::store);

	try
	{
			archive.Write (&DlgBkColor, sizeof (DlgBkColor));
	}
	catch (...) 
	{
		return;
	}


	archive.Close ();
	File.Close ();
}

void DbFormView::Load ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	File.Open (Path.GetBuffer (), CFile::modeRead);
	CArchive archive(&File, CArchive::load);

	try
	{
			archive.Read (&DlgBkColor, sizeof (DlgBkColor));
	}
	catch (...) 
	{
		return;
	}


	archive.Close ();
	File.Close ();
}

