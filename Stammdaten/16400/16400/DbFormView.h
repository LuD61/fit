#pragma once
#include "afxext.h"

class DbFormView :
	public CFormView
{
	DECLARE_DYNCREATE(DbFormView)
public:
	CString ArchiveName;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	DbFormView();
	DbFormView(UINT);
	~DbFormView(void);
    virtual DROPEFFECT OnDrop (CWnd* , COleDataObject*,  DROPEFFECT,  DROPEFFECT, CPoint);
	virtual void write ();
	virtual BOOL TextCent ();
	virtual BOOL TextLeft ();
	virtual BOOL TextRight ();
	virtual void Save ();
	virtual void Load ();
};
