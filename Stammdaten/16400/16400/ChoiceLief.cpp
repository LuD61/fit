#include "stdafx.h"
#include "ChoiceLief.h"
#include "DbUniCode.h"
#include "DynDialog.h"
#include "DbQuery.h"
#include "Process.h"
#include "windowhandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceLief::Sort1 = -1;
int CChoiceLief::Sort2 = -1;
int CChoiceLief::Sort3 = -1;
int CChoiceLief::Sort4 = -1;
int CChoiceLief::Sort5 = -1;
int CChoiceLief::Sort6 = -1;
int CChoiceLief::Sort7 = -1;
int CChoiceLief::Sort8 = -1;
int CChoiceLief::Sort9 = -1;
int CChoiceLief::Sort10 = -1;
int CChoiceLief::Sort11 = -1;
int CChoiceLief::Sort12 = -1;

CChoiceLief::CChoiceLief(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_Mdn = 0;
	m_Fil = 0;
//	HideEnter = FALSE;
//	HideFilter = FALSE;
}

CChoiceLief::~CChoiceLief() 
{
	DestroyList ();
}

void CChoiceLief::DestroyList() 
{
	for (std::vector<CLiefList *>::iterator pabl = LiefList.begin (); pabl != LiefList.end (); ++pabl)
	{
		CLiefList *abl = *pabl;
		delete abl;
	}
    LiefList.clear ();
}

BEGIN_MESSAGE_MAP(CChoiceLief, CChoiceX)
	//{{AFX_MSG_MAP(CChoiceLief)
	ON_WM_MOVE ()
END_MESSAGE_MAP()

void CChoiceLief::FillList () 
{
    short  mdn;
    short  fil;
	TCHAR lief [17];
    TCHAR adr_krz [34];
	TCHAR adr_nam1 [37];
	TCHAR adr_nam2 [37];
	TCHAR plz [9];
	TCHAR ort1 [37];
	TCHAR str [37];
	TCHAR tel [21];
	TCHAR fax [21];
	TCHAR email [37];
	short staat;
	short land;
	char ptstaat [37];
	char ptland [37];
	int cursor;

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Lieferanten"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Lieferant"),          1, 80, LVCFMT_RIGHT);
    SetCol (_T("Kurzname"),  2, 250);
    SetCol (_T("Name1"),  3, 250);
    SetCol (_T("Name2"),  4, 250);
    SetCol (_T("PLZ"),  5, 100);
    SetCol (_T("Ort"),  6, 250);
    SetCol (_T("Strasse"),  7, 250);
    SetCol (_T("Telefon"),  8, 250);
    SetCol (_T("Fax"),  9, 250);
    SetCol (_T("Email"),  10, 250);
    SetCol (_T("Staat"),  11, 250);
    SetCol (_T("Land"),  12, 250);

	if (FirstRead && !HideFilter)
	{
		FirstRead = FALSE;
		Load ();
//		if (!IsModal)
		{
			PostMessage (WM_COMMAND, IDC_FILTER, 0l);
		}
		return;
	}

	if (LiefList.size () == 0)
	{
		CString Sql = _T("");
		if (m_Mdn != 0 && m_Fil != 0)
		{
			DbClass->sqlin ((short *)  &m_Mdn,   SQLSHORT, 0);
			DbClass->sqlin ((short *)  &m_Fil,   SQLSHORT, 0);
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((short *)&fil,     SQLSHORT, 0);
			DbClass->sqlout ((char *)lief,      SQLCHAR, sizeof (lief));
			DbClass->sqlout ((LPTSTR)  adr_krz, SQLCHAR, sizeof (adr_krz));
			DbClass->sqlout ((LPTSTR)  adr_nam1, SQLCHAR, sizeof (adr_nam1));
			DbClass->sqlout ((LPTSTR)  adr_nam2, SQLCHAR, sizeof (adr_nam2));
			DbClass->sqlout ((LPTSTR)  plz, SQLCHAR, sizeof (plz));
			DbClass->sqlout ((LPTSTR)  ort1, SQLCHAR, sizeof (ort1));
			DbClass->sqlout ((LPTSTR)  str, SQLCHAR, sizeof (str));
			DbClass->sqlout ((LPTSTR)  tel, SQLCHAR, sizeof (tel));
			DbClass->sqlout ((LPTSTR)  fax, SQLCHAR, sizeof (fax));
			DbClass->sqlout ((LPTSTR)  email, SQLCHAR, sizeof (email));
			DbClass->sqlout ((LPTSTR)  &staat, SQLSHORT, 0);
			DbClass->sqlout ((LPTSTR)  &land, SQLSHORT, 0);
			Sql = _T("select lief.mdn, lief.fil, lief.lief, adr.adr_krz, ")
				  _T("adr.adr_nam1, adr.adr_nam2, adr.plz, adr.ort1, adr.str,")
				  _T("adr.tel, adr.fax, adr.email, adr.staat, adr.land ")
				  _T("from lief, adr where lief.lief > 0 ")
				  _T("and adr.adr = lief.adr ")
				  _T("and lief.mdn = ?");
				  _T("and lief.fil = ?");
		}
		else if (m_Mdn != 0)
		{
			DbClass->sqlin ((short *)  &m_Mdn,   SQLSHORT, 0);
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((short *)&fil,     SQLSHORT, 0);
			DbClass->sqlout ((char *)lief,      SQLCHAR, sizeof (lief));
			DbClass->sqlout ((LPTSTR)  adr_krz, SQLCHAR, sizeof (adr_krz));
			DbClass->sqlout ((LPTSTR)  adr_nam1, SQLCHAR, sizeof (adr_nam1));
			DbClass->sqlout ((LPTSTR)  adr_nam2, SQLCHAR, sizeof (adr_nam2));
			DbClass->sqlout ((LPTSTR)  plz, SQLCHAR, sizeof (plz));
			DbClass->sqlout ((LPTSTR)  ort1, SQLCHAR, sizeof (ort1));
			DbClass->sqlout ((LPTSTR)  str, SQLCHAR, sizeof (str));
			DbClass->sqlout ((LPTSTR)  tel, SQLCHAR, sizeof (tel));
			DbClass->sqlout ((LPTSTR)  fax, SQLCHAR, sizeof (fax));
			DbClass->sqlout ((LPTSTR)  email, SQLCHAR, sizeof (email));
			DbClass->sqlout ((LPTSTR)  &staat, SQLSHORT, 0);
			DbClass->sqlout ((LPTSTR)  &land, SQLSHORT, 0);
			Sql = _T("select lief.mdn, lief.fil, lief.lief, adr.adr_krz, ")
				  _T("adr.adr_nam1, adr.adr_nam2, adr.plz, adr.ort1, adr.str,")
				  _T("adr.tel, adr.fax, adr.email, adr.staat, adr.land ")
				  _T("from lief, adr where lief.lief > 0 ")
				  _T("and adr.adr = lief.adr ")
				  _T("and lief.mdn = ?");
		}
		else
		{
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((short *)&fil,     SQLSHORT, 0);
			DbClass->sqlout ((char *)lief,      SQLCHAR, sizeof (lief));
			DbClass->sqlout ((LPTSTR)  adr_krz, SQLCHAR, sizeof (adr_krz));
			DbClass->sqlout ((LPTSTR)  adr_nam1, SQLCHAR, sizeof (adr_nam1));
			DbClass->sqlout ((LPTSTR)  adr_nam2, SQLCHAR, sizeof (adr_nam2));
			DbClass->sqlout ((LPTSTR)  plz, SQLCHAR, sizeof (plz));
			DbClass->sqlout ((LPTSTR)  ort1, SQLCHAR, sizeof (ort1));
			DbClass->sqlout ((LPTSTR)  str, SQLCHAR, sizeof (str));
			DbClass->sqlout ((LPTSTR)  tel, SQLCHAR, sizeof (tel));
			DbClass->sqlout ((LPTSTR)  fax, SQLCHAR, sizeof (fax));
			DbClass->sqlout ((LPTSTR)  email, SQLCHAR, sizeof (email));
			DbClass->sqlout ((LPTSTR)  &staat, SQLSHORT, 0);
			DbClass->sqlout ((LPTSTR)  &land, SQLSHORT, 0);
			Sql = _T("select lief.mdn, lief.fil, lief.lief, adr.adr_krz, ")
				  _T("adr.adr_nam1, adr.adr_nam2, adr.plz, adr.ort1, adr.str,")
				  _T("adr.tel, adr.fax, adr.email, adr.staat, adr.land ")
				  _T("from lief, adr where lief.lief > 0 ")
				  _T("and adr.adr = lief.adr ");
		}
		if (QueryString != _T(""))
		{
			Sql += _T(" and ");
			Sql += QueryString;
		}

		cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe f�r den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}

		DbClass->sqlopen (cursor);
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) adr_krz;
			CDbUniCode::DbToUniCode (adr_krz, pos);
			CLiefList *abl = new CLiefList (mdn, fil, lief, adr_krz);
			abl->adr_nam1 = adr_nam1;
			abl->adr_nam2 = adr_nam2;
			abl->adr_nam2 = adr_nam2;
			abl->plz = plz;
			abl->ort1 = ort1;
			abl->str = str;
			abl->tel = tel;
			abl->fax = fax;
			abl->email = email;
			abl->staat = staat;
			abl->land = land;
			LiefList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CLiefList *>::iterator pabl = LiefList.begin (); pabl != LiefList.end (); ++pabl)
	{
		CLiefList *abl = *pabl;
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString Fil;
		Fil.Format (_T("%hd"), abl->fil); 
		_tcscpy (lief, abl->lief.GetBuffer ());
		_tcscpy (adr_krz, abl->adr_krz.GetBuffer ());
		_tcscpy (adr_nam1, abl->adr_nam1.GetBuffer ());
		_tcscpy (adr_nam2, abl->adr_nam2.GetBuffer ());
		_tcscpy (plz, abl->plz.GetBuffer ());
		_tcscpy (ort1, abl->ort1.GetBuffer ());
		_tcscpy (str, abl->str.GetBuffer ());
		_tcscpy (tel, abl->tel.GetBuffer ());
		_tcscpy (fax, abl->fax.GetBuffer ());
		_tcscpy (email, abl->email.GetBuffer ());
		GetPtBez (_T("staat"), staat, ptstaat);
		GetPtBez (_T("land"), land, ptland);

        int ret = InsertItem (i, -1);
		int pos = 0; 
        ret = SetItemText (lief, i, ++pos);
        ret = SetItemText (adr_krz, i, ++pos);
        ret = SetItemText (adr_nam1, i, ++pos);
        ret = SetItemText (adr_nam2, i, ++pos);
        ret = SetItemText (plz, i, ++pos);
        ret = SetItemText (ort1, i, ++pos);
        ret = SetItemText (str, i, ++pos);
        ret = SetItemText (tel, i, ++pos);
        ret = SetItemText (fax, i, ++pos);
        ret = SetItemText (email, i, ++pos);
        ret = SetItemText (ptstaat, i, ++pos);
        ret = SetItemText (ptland, i, ++pos);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort7 = -1;
    Sort8 = -1;
    Sort9 = -1;
    Sort10 = -1;
    Sort11 = -1;
    Sort12 = -1;
    Sort (listView);
//    FirstSelection ();
	GetDlgItem (IDC_SEARCH)->SetFocus ();
	m_Idx = -1;
}


void CChoiceLief::SearchItem (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();
		CString cSearch = Search;
		cSearch.MakeUpper ();

        if (_tmemcmp (cSearch.GetBuffer (), iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLief::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    EditText.MakeUpper ();
    SearchItem (ListBox, EditText.GetBuffer ());
}

int CChoiceLief::GetPtBez (LPTSTR item, LPTSTR wert, LPTSTR bez)
{
	TCHAR ptitem [9];
	TCHAR ptwert [5];
	TCHAR ptbez [37];
	static int cursor = -1;

	_tcscpy (ptbez, _T(""));
	if (cursor == -1)
	{
		DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
		DbClass->sqlin ((LPTSTR) ptitem, SQLCHAR, 9);
		DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
		cursor = DbClass->sqlcursor (_T("select ptbez from ptabn where ptitem = ? ")
								_T("and ptwert = ?"));
	}
	strcpy (ptitem, item);
	strcpy (ptwert, wert);
	DbClass->sqlopen (cursor);
	int dsqlstatus = DbClass->sqlfetch (cursor);
	strcpy (bez, ptbez);
    return dsqlstatus;
}

int CChoiceLief::GetPtBez (LPTSTR item, int wert, LPTSTR bez)
{
	TCHAR ptitem [9];
	TCHAR ptwert [5];
	TCHAR ptbez [37];
	static int cursor = -1;

	_tcscpy (ptbez, _T(""));
	if (cursor == -1)
	{
		DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
		DbClass->sqlin ((LPTSTR) ptitem, SQLCHAR, 9);
		DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
		cursor = DbClass->sqlcursor (_T("select ptbez from ptabn where ptitem = ? ")
								_T("and ptwert = ?"));
	}
	strcpy (ptitem, item);
	sprintf (ptwert,"%d", wert);
	DbClass->sqlopen (cursor);
	int dsqlstatus = DbClass->sqlfetch (cursor);
	strcpy (bez, ptbez);
    return dsqlstatus;
}

int CALLBACK CChoiceLief::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   int ret = 0;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   ret = (strItem2.CompareNoCase (strItem1));
   switch (SortRow)
   {
	   case 0:
		   ret *= Sort1;
		   break;
	   case 1:
		   ret *= Sort2;
		   break;
	   case 2:
		   ret *= Sort3;
		   break;
	   case 3:
		   ret *= Sort4;
		   break;
	   case 4:
		   ret *= Sort5;
		   break;
	   case 5:
		   ret *= Sort6;
		   break;
	   case 6:
		   ret *= Sort7;
		   break;
	   case 7:
		   ret *= Sort8;
		   break;
	   case 8:
		   ret *= Sort9;
		   break;
	   case 9:
		   ret *= Sort10;
		   break;
	   case 10:
		   ret *= Sort11;
		   break;
	   case 11:
		   ret *= Sort12;
		   break;
   }
   return ret;
}


void CChoiceLief::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
        case 4:
              Sort5 *= -1;
			  if (Sort5 < 0) SortPos = 1;
              break;
        case 5:
              Sort6 *= -1;
			  if (Sort6 < 0) SortPos = 1;
              break;
        case 6:
              Sort7 *= -1;
			  if (Sort7 < 0) SortPos = 1;
              break;
        case 7:
              Sort8 *= -1;
			  if (Sort8 < 0) SortPos = 1;
              break;
        case 8:
              Sort9*= -1;
			  if (Sort9 < 0) SortPos = 1;
              break;
        case 9:
              Sort10*= -1;
			  if (Sort10 < 0) SortPos = 1;
              break;
        case 10:
              Sort11*= -1;
			  if (Sort11 < 0) SortPos = 1;
              break;
        case 11:
              Sort12*= -1;
			  if (Sort12 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CLiefList *abl = LiefList [i];
		   
		   abl->lief    = ListBox->GetItemText (i, 1);
		   abl->adr_krz = ListBox->GetItemText (i, 2);
		   abl->adr_nam1 = ListBox->GetItemText (i, 3);
		   abl->adr_nam2 = ListBox->GetItemText (i, 4);
		   abl->plz = ListBox->GetItemText (i, 5);
		   abl->ort1 = ListBox->GetItemText (i,6);
		   abl->str = ListBox->GetItemText (i, 7);
		   abl->tel = ListBox->GetItemText (i, 8);
		   abl->fax = ListBox->GetItemText (i, 9);
		   abl->email = ListBox->GetItemText (i, 10);
		   abl->staat = (short) atoi (ListBox->GetItemText (i, 11));
		   abl->land  = (short) atoi (ListBox->GetItemText (i, 12));
	}
	for (int i = 1; i <= 2; i ++)
	{
		SetItemSort (i, 2);
	}
	SetItemSort (SortRow, SortPos);
}

void CChoiceLief::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = LiefList [idx];
}

CLiefList *CChoiceLief::GetSelectedText ()
{
	CLiefList *abl = (CLiefList *) SelectedRow;
	return abl;
}


void CChoiceLief::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cLief = m_List.GetItemText (idx, 1);  
		TCHAR *lief = cLief.GetBuffer ();
		Message.Format (_T("LieferantenNr=%s"), lief);
		ToClipboard (Message);
	}
	p.SetCommand (_T("16400"));
	HANDLE Pid = p.Start ();
}

void CChoiceLief::OnFilter ()
{
	CDynDialog dlg;
	CDynDialogItem *Item;
	static int headercy = 200;

	dlg.DlgCaption = "Filter";
	dlg.UseFilter = UseFilter;

	if (vFilter.size () > 0)
	{
		for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
		{
			CDynDialogItem *i = *it;
			Item = new CDynDialogItem ();
			*Item = *i;
			if (Item->Name == _T("reset"))
			{
				if (UseFilter)
				{
					Item->Value = _T("aktiv");
				}
				else
				{
					Item->Value = _T("nicht aktiv");
				}
			}
			dlg.AddItem (Item);

		}
		dlg.Header.cy = headercy;	
	}
	else
	{
		int y = 10;
		Item = new CDynDialogItem ();
		Item->Name = _T("llief");
		Item->CtrClass = "static";
		Item->Value = _T("Lieferant");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1001;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("lief");
//		Item->Type = Item->Decimal;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1002;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("ladr_krz");
		Item->CtrClass = "static";
		Item->Value = _T("Kurzname 1");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1003;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("adr.adr_krz");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("ladr_nam1");
		Item->CtrClass = "static";
		Item->Value = _T("Name");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1007;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("adr_nam1");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1008;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lplz");
		Item->CtrClass = "static";
		Item->Value = _T("PLZ");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1009;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("plz");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1010;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lort1");
		Item->CtrClass = "static";
		Item->Value = _T("Ort");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1011;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("ort1");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1012;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lstr");
		Item->CtrClass = "static";
		Item->Value = _T("Strasse");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1013;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("str");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1014;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("ltel");
		Item->CtrClass = "static";
		Item->Value = _T("Telefon");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1015;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("tel");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1016;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lstaat");
		Item->CtrClass = "static";
		Item->Value = _T("Staat");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1017;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("staat");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1018;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lland");
		Item->CtrClass = "static";
		Item->Value = _T("Land");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1019;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("land");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1020;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		dlg.Header.cy = y + 30;
		headercy = dlg.Header.cy;

		Item = new CDynDialogItem ();
		Item->Name = _T("OK");
		Item->CtrClass = "button";
		Item->Value = _T("OK");
		Item->Template.x = 15;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDOK;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("cancel");
		Item->CtrClass = "button";
		Item->Value = _T("abbrechen");
		Item->Template.x = 70;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDCANCEL;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("reset");
		Item->CtrClass = "button";
		if (UseFilter)
		{
			Item->Value = _T("aktiv");
		}
		else
		{
			Item->Value = _T("nicht aktiv");
		}
		Item->Template.x = 125;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = ID_USE;
		dlg.AddItem (Item);
	}

	dlg.CreateModal ();
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK) return;
 
	UseFilter = dlg.UseFilter;
	int count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = dlg.Items.begin (); it != dlg.Items.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (count >= (int) vFilter.size ())
			{
				Item = new CDynDialogItem ();
				*Item = *i;
				vFilter.push_back (Item);
			}
			*vFilter[count] = *i;
			count ++;
	}
    CString Query;
    CDbQuery DbQuery;

	QueryString = _T("");
	if (!UseFilter)
	{
		FillList ();
		return;
	}
	count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (strcmp (i->CtrClass, "edit") != 0) continue; 
			if (i->Value.Trim () != _T(""))
			{
				if (i->Type == i->String || i->Type == i->Date)
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, TRUE);
				}
				else
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, FALSE);
				}
				if (count > 0)
				{
					QueryString += _T(" and ");
				}
                QueryString += Query;
				count ++;
			}
	}
	FillList ();
}

void CChoiceLief::OnMove (int x, int y)
{
	WINDOWHANDLER->IsDockPlace (x, y);
}