#include "StdAfx.h"
#include "DistributorSheet.h"
#include "DbPropertyPage.h"
#include "windowhandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CDistributorSheet, CDbPropertySheet)

CDistributorSheet::CDistributorSheet(void) : CDbPropertySheet ()
{
}

CDistributorSheet::CDistributorSheet(LPTSTR title) : CDbPropertySheet (title)
{
}

CDistributorSheet::~CDistributorSheet(void)
{
}

BEGIN_MESSAGE_MAP(CDistributorSheet, CDbPropertySheet)
//	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()

BOOL CDistributorSheet::OnInitDialog ()
{
	BOOL ret = CDbPropertySheet::OnInitDialog ();

	SetWindowPos (NULL, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
	return ret;
}

HBRUSH CDistributorSheet::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	DlgBkColor = WINDOWHANDLER->GetDlgBkColor ();
	DlgBrush   = WINDOWHANDLER->GetDlgBrush ();

	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (OPAQUE);
			WINDOWHANDLER->SetDlgBkColor (DlgBkColor);
			WINDOWHANDLER->SetDlgBrush (DlgBrush);
			return DlgBrush;
	}
	return CDbPropertySheet::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CDistributorSheet::StepBack ()
{
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	this->SetActivePage (0);
	return Page->StepBack ();
}

BOOL CDistributorSheet::Write ()
{
	int count = GetPageCount ();

	for (int i = 0; i < count; i ++)
	{
		SetActivePage (i);
	}

	WINDOWHANDLER->UpdateWrite ();
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	this->SetActivePage (0);
	return Page->Write ();
}

BOOL CDistributorSheet::DeleteAll ()
{
	WINDOWHANDLER->UpdateWrite ();
	CDbPropertyPage *Page = (CDbPropertyPage *) GetPage (0);
	this->SetActivePage (0);
	return Page->DeleteAll ();
}
