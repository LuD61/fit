#include "stdafx.h"
#include "lief_r_lis.h"

struct LIEF_R_LIS lief_r_lis, lief_r_lis_null, lief_r_lis_def;

void LIEF_R_LIS_CLASS::prepare (void)
{
            TCHAR *sqltext;

	    sqlin ((short *) &lief_r_lis.mdn,SQLSHORT,0);
	    sqlin ((short *) &lief_r_lis.fil,SQLSHORT,0);
	    sqlin ((long *) &lief_r_lis.lst_nr,SQLLONG,0);
	    sqlin ((double *) &lief_r_lis.a,SQLDOUBLE,0);
	    sqlin ((TCHAR *) lief_r_lis.rab_typ,SQLCHAR,3);
    sqlout ((double *) &lief_r_lis.a,SQLDOUBLE,0);
    sqlout ((short *) &lief_r_lis.fil,SQLSHORT,0);
    sqlout ((long *) &lief_r_lis.lst_nr,SQLLONG,0);
    sqlout ((short *) &lief_r_lis.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) lief_r_lis.rab_st,SQLCHAR,5);
    sqlout ((double *) &lief_r_lis.rab_wrt,SQLDOUBLE,0);
    sqlout ((TCHAR *) lief_r_lis.rab_typ,SQLCHAR,3);
    sqlout ((short *) &lief_r_lis.waehrung,SQLSHORT,0);
            cursor = sqlcursor (_T("select lief_r_lis.a,  ")
_T("lief_r_lis.fil,  lief_r_lis.lst_nr,  lief_r_lis.mdn,  ")
_T("lief_r_lis.rab_st,  lief_r_lis.rab_wrt,  lief_r_lis.rab_typ,  ")
_T("lief_r_lis.waehrung from lief_r_lis ")

#line 16 "lief_r_lis.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lst_nr  = ? ")
                                  _T("and a   = ? ")
				  _T("and rab_typ = ?"));
    sqlin ((double *) &lief_r_lis.a,SQLDOUBLE,0);
    sqlin ((short *) &lief_r_lis.fil,SQLSHORT,0);
    sqlin ((long *) &lief_r_lis.lst_nr,SQLLONG,0);
    sqlin ((short *) &lief_r_lis.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) lief_r_lis.rab_st,SQLCHAR,5);
    sqlin ((double *) &lief_r_lis.rab_wrt,SQLDOUBLE,0);
    sqlin ((TCHAR *) lief_r_lis.rab_typ,SQLCHAR,3);
    sqlin ((short *) &lief_r_lis.waehrung,SQLSHORT,0);
            sqltext = _T("update lief_r_lis set ")
_T("lief_r_lis.a = ?,  lief_r_lis.fil = ?,  lief_r_lis.lst_nr = ?,  ")
_T("lief_r_lis.mdn = ?,  lief_r_lis.rab_st = ?,  ")
_T("lief_r_lis.rab_wrt = ?,  lief_r_lis.rab_typ = ?,  ")
_T("lief_r_lis.waehrung = ? ")

#line 22 "lief_r_lis.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lst_nr  = ? ")
                                  _T("and a   = ? ")
				  _T("and rab_typ = ?");
	    sqlin ((short *) &lief_r_lis.mdn,SQLSHORT,0);
	    sqlin ((short *) &lief_r_lis.fil,SQLSHORT,0);
	    sqlin ((long *) &lief_r_lis.lst_nr,SQLLONG,0);
	    sqlin ((double *) &lief_r_lis.a,SQLDOUBLE,0);
	    sqlin ((TCHAR *) lief_r_lis.rab_typ,SQLCHAR,3);
            upd_cursor = sqlcursor (sqltext);

	    sqlin ((short *) &lief_r_lis.mdn,SQLSHORT,0);
	    sqlin ((short *) &lief_r_lis.fil,SQLSHORT,0);
	    sqlin ((long *) &lief_r_lis.lst_nr,SQLLONG,0);
	    sqlin ((double *) &lief_r_lis.a,SQLDOUBLE,0);
	    sqlin ((TCHAR *) lief_r_lis.rab_typ,SQLCHAR,3);
            test_upd_cursor = sqlcursor (_T("select a from lief_r_lis ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lst_nr  = ? ")
                                  _T("and a   = ? ")
				  _T("and rab_typ = ?"));
	    sqlin ((short *) &lief_r_lis.mdn,SQLSHORT,0);
	    sqlin ((short *) &lief_r_lis.fil,SQLSHORT,0);
	    sqlin ((long *) &lief_r_lis.lst_nr,SQLLONG,0);
	    sqlin ((double *) &lief_r_lis.a,SQLDOUBLE,0);
	    sqlin ((TCHAR *) lief_r_lis.rab_typ,SQLCHAR,3);
            test_lock_cursor = sqlcursor (_T("update lief_r_lis set fil = fil ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lst_nr  = ? ")
                                  _T("and a   = ? ")
				  _T("and rab_typ = ?"));
	    sqlin ((short *) &lief_r_lis.mdn,SQLSHORT,0);
	    sqlin ((short *) &lief_r_lis.fil,SQLSHORT,0);
	    sqlin ((long *) &lief_r_lis.lst_nr,SQLLONG,0);
	    sqlin ((double *) &lief_r_lis.a,SQLDOUBLE,0);
	    sqlin ((TCHAR *) lief_r_lis.rab_typ,SQLCHAR,3);
            del_cursor = sqlcursor (_T("delete from lief_r_lis ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lst_nr  = ? ")
                                  _T("and a   = ? ")
				  _T("and rab_typ = ?"));
    sqlin ((double *) &lief_r_lis.a,SQLDOUBLE,0);
    sqlin ((short *) &lief_r_lis.fil,SQLSHORT,0);
    sqlin ((long *) &lief_r_lis.lst_nr,SQLLONG,0);
    sqlin ((short *) &lief_r_lis.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) lief_r_lis.rab_st,SQLCHAR,5);
    sqlin ((double *) &lief_r_lis.rab_wrt,SQLDOUBLE,0);
    sqlin ((TCHAR *) lief_r_lis.rab_typ,SQLCHAR,3);
    sqlin ((short *) &lief_r_lis.waehrung,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into lief_r_lis (")
_T("a,  fil,  lst_nr,  mdn,  rab_st,  rab_wrt,  rab_typ,  waehrung) ")

#line 68 "lief_r_lis.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?)"));

#line 70 "lief_r_lis.rpp"
}
