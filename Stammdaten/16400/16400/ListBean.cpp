#include "StdAfx.h"
#include "ListBean.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CListBean::CListBean(void)
{
	ArchiveName = "";
	ListCtrl = NULL;
	SortRow = 1;
}

CListBean::CListBean(LPTSTR ArchiveName)
{
	this->ArchiveName = ArchiveName;
	ListCtrl = NULL;
	SortRow = 1;
}

CListBean::~CListBean(void)
{
}

void CListBean::Save ()
{
	CFile File;
	CString Path;
	CString Etc;
//    LV_COLUMN Col;

	if (ListCtrl == NULL) return;
	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	File.Open (Path.GetBuffer (), CFile::modeCreate | CFile::modeWrite);
	CArchive archive(&File, CArchive::store);

	try
	{
		CHeaderCtrl *Header = ListCtrl->GetHeaderCtrl ();
		if (Header != NULL)
		{
			int iCount = Header->GetItemCount ();
			int *OrderArray = new int [iCount];
			Header->GetOrderArray (OrderArray, iCount);
			archive.Write (&iCount, sizeof (iCount));
			archive.Write (OrderArray, iCount * sizeof (int));
			for (int i = 0; i < iCount; i ++)
			{
				int cx = ListCtrl->GetColumnWidth (i);
				archive.Write (&cx, sizeof (int));
//			    Col.mask =  LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;   
//			    Col.mask =  LVCF_SUBITEM;   
//				ListCtrl->GetColumn (i, &Col);
//				archive.Write (&Col, sizeof (Col));

			}
			delete OrderArray;
			archive.Write (&SortRow, sizeof (SortRow));
		}
	}
	catch (...) 
	{
		return;
	}


	archive.Close ();
	File.Close ();
}

void CListBean::Load ()
{
	CFile File;
	CString Path;
	CString Etc;
//    LV_COLUMN Col;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	if (ListCtrl == NULL) return;

	if (!File.Open (Path.GetBuffer (), CFile::modeRead)) return;
	CArchive archive(&File, CArchive::load);

	try
	{
		CHeaderCtrl *Header = ListCtrl->GetHeaderCtrl ();
		if (Header != NULL)
		{
			int iCount = 0;
			archive.Read (&iCount, sizeof (iCount));
			int *OrderArray = new int [iCount];
			archive.Read (OrderArray, iCount * sizeof (int));
			Header->SetOrderArray (iCount, OrderArray);
			delete OrderArray;
			for (int i = 0; i < iCount; i ++)
			{
				int cx = 0;
				archive.Read (&cx, sizeof (int));
				ListCtrl->SetColumnWidth (i, cx);
//				archive.Read (&Col, sizeof (Col));
//			    Col.mask =  LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;   
//			    Col.mask =  LVCF_SUBITEM;   
//				ListCtrl->SetColumn (i, &Col);
			}
			archive.Read (&SortRow, sizeof (SortRow));
		}
	}
	catch (...) 
	{

		return;
	}

	archive.Close ();
	File.Close ();
}

void CListBean::DropArchive ()
{
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	DeleteFile (Path.GetBuffer ());
}

void CListBean::SetListCtrl (CListCtrl* ListCtrl)
{
	this->ListCtrl = ListCtrl;
}

CListCtrl* CListBean::GetListCtrl ()
{
	return ListCtrl;
}

void CListBean::SetSortRow (int SortRow)
{
	this->SortRow = SortRow;
}

int CListBean::GetSortRow ()
{
	return SortRow;
}
