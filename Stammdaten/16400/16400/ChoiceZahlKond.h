#ifndef _CHOICE_ZAHL_KOND_DEF
#define _CHOICE_ZAHL_KOND_DEF

#include "ChoiceX.h"
#include "ZahlKondList.h"
#include <vector>

class CChoiceZahlKond : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
        static int Sort7;
        static int Sort8;
        static int Sort9;
      
    public :
	    std::vector<CZahlKondList *> ZahlKondList;
      	CChoiceZahlKond(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceZahlKond(); 
        virtual void FillList (void);


        void SearchItem (CListCtrl *ListBox, LPTSTR SearchText);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CZahlKondList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR,LPTSTR );
        int GetPtBez (LPTSTR, int, LPTSTR);
		void DestroyList ();
		virtual void OnEnter ();
	    virtual void OnFilter ();
	    afx_msg void OnMove (int x, int y);

	DECLARE_MESSAGE_MAP()
};
#endif
