#pragma once
#include <vector>
#include "editlistctrl.h"

class CLiefRLisListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:
	enum LIST_POS
	{
		PosRabTyp = 1,
		PosA      = 2,
		PosBez = 3,
		PosRabWrt = 4,
		PosRabSt  = 5,
	};

	static CString m_RabTypTab[];
	static CString m_RabStTab[];
	CImageList m_ImageList;
	std::vector<BOOL> vSelect;
	CVector ListRows;
	CVector m_RabTypValues;
	CVector m_RabStValues;

	CLiefRLisListCtrl(void);
	~CLiefRLisListCtrl(void);
	void InitList ();
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
        void GetColValue (int row, int col, CString& Text);
	void ScrollPositions (int pos);
	BOOL LastCol ();
	void UpdateRow ();
};
