#pragma once

class CListChangeHandler
{
public:
	CListChangeHandler(void);
	~CListChangeHandler(void);
	virtual void RowChanged (int NewRow) = 0;
	virtual void ColChanged (int Row, int Col) = 0;
};
