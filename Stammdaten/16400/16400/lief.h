#ifndef _LIEF_DEF
#define _LIEF_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LIEF {
   TCHAR          abr[2];
   long           adr;
   TCHAR          bank_nam[37];
   long           bbn;
   short          best_sort;
   TCHAR          best_trans_kz[2];
   long           blz;
   TCHAR          bonus_kz[2];
   short          fil;
   TCHAR          fracht_kz[2];
   TCHAR          ink[17];
   long           kreditor;
   TCHAR          kto[17];
   DATE_STRUCT    letzt_lief;
   TCHAR          lief[17];
   TCHAR          lief_kun[17];
   TCHAR          lief_rht[2];
   long           lief_s;
   short          lief_typ;
   short          lief_zeit;
   double         lief_zusch;
   long           lst_nr;
   short          mahn_stu;
   short          mdn;
   TCHAR          me_kz[2];
   TCHAR          min_me_kz[2];
   double         min_zusch;
   double         min_zusch_proz;
   TCHAR          nach_lief[2];
   TCHAR          ordr_kz[2];
   TCHAR          rab_abl[2];
   TCHAR          rab_kz[2];
   TCHAR          rech_kz[2];
   double         rech_toler;
   short          sdr_nr;
   long           son_kond;
   short          sprache;
   TCHAR          steuer_kz[2];
   TCHAR          unt_lief_kz[2];
   TCHAR          vieh_bas[2];
   TCHAR          we_erf_kz[2];
   TCHAR          we_kontr[2];
   TCHAR          we_pr_kz[2];
   double         we_toler;
   short          zahl_kond;
   TCHAR          zahlw[2];
   short          hdklpargr;
   long           vorkgr;
   short          delstatus;
   short          mwst;
   short          liefart;
   TCHAR          modif[2];
   TCHAR          ust_id[17];
   TCHAR          eg_betriebsnr[21];
   short          eg_kz;
   short          waehrung;
   TCHAR          iln[17];
   short          zertifikat1;
   short          zertifikat2;
   TCHAR          iban1[5];
   TCHAR          iban2[5];
   TCHAR          iban3[5];
   TCHAR          iban4[5];
   TCHAR          iban5[5];
   TCHAR          iban6[5];
   TCHAR          iban7[4];
   TCHAR          esnum[11];
   TCHAR          eznum[11];
   short          geburt;
   short          mast;
   short          schlachtung;
   short          zerlegung;
   TCHAR          steuer_nr[37];
   long           kst;
   TCHAR          iban[25];
   TCHAR          swift[25];
   short          tage_bis;
};
extern struct LIEF lief, lief_null;

#line 8 "lief.rh"

class LIEF_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LIEF lief;  
               LIEF_CLASS () : DB_CLASS ()
               {
               }
};
#endif
