// 16400View.cpp : Implementierung der Klasse CMy16400View
//

#include "stdafx.h"
#include "16400.h"

#include "16400Doc.h"
#include "16400View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMy16400View

IMPLEMENT_DYNCREATE(CMy16400View, CFormView)

BEGIN_MESSAGE_MAP(CMy16400View, CFormView)
END_MESSAGE_MAP()

// CMy16400View-Erstellung/Zerst�rung

CMy16400View::CMy16400View()
	: CFormView(CMy16400View::IDD)
{
	// TODO: Hier Code zur Konstruktion einf�gen

}

CMy16400View::~CMy16400View()
{
}

void CMy16400View::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL CMy16400View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CFormView::PreCreateWindow(cs);
}

void CMy16400View::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	CDocument *dc = GetDocument ();
	if (dc != NULL)
	{
		dc->SetTitle (_T("Stammdaten"));
	}
//	ResizeParentToFit();
    
}


// CMy16400View-Diagnose

#ifdef _DEBUG
void CMy16400View::AssertValid() const
{
	CFormView::AssertValid();
}

void CMy16400View::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CMy16400Doc* CMy16400View::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMy16400Doc)));
	return (CMy16400Doc*)m_pDocument;
}
#endif //_DEBUG


// CMy16400View-Meldungshandler

