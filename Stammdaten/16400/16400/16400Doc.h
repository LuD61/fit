// 16400Doc.h : Schnittstelle der Klasse CMy16400Doc
//


#pragma once


class CMy16400Doc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CMy16400Doc();
	DECLARE_DYNCREATE(CMy16400Doc)

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CMy16400Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


