#include "StdAfx.h"
#include "LiefRLisCore.h"
#include "DbTime.h"
#include "StrFuncs.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CLiefRLisCore *CLiefRLisCore::Instance = NULL; 

CLiefRLisCore::CLiefRLisCore(void)
{
	CLIEF_R_LIS.sqlin ((short *) &TLIEF_R_LIS.mdn, SQLSHORT, 0);
	CLIEF_R_LIS.sqlin ((short *) &TLIEF_R_LIS.fil, SQLSHORT, 0);
	CLIEF_R_LIS.sqlin ((long *)  &TLIEF_R_LIS.lst_nr, SQLLONG, 0);
	CLIEF_R_LIS.sqlout ((double *) &TLIEF_R_LIS.a, SQLDOUBLE, 0);
	CLIEF_R_LIS.sqlout ((char *)  TLIEF_R_LIS.rab_typ, SQLCHAR, sizeof (TLIEF_R_LIS.rab_typ));
	m_Cursor = CLIEF_R_LIS.sqlcursor (_T("select a, rab_typ from lief_r_lis ")
									  _T("where mdn = ? ")
									  _T("and fil = ? ")
									  _T("and lst_nr = ? ")
									  _T("order by rab_typ, a"));
	CLIEF_R_LIS.sqlin ((short *) &TLIEF_R_LIS.mdn, SQLSHORT, 0);
	CLIEF_R_LIS.sqlin ((short *) &TLIEF_R_LIS.fil, SQLSHORT, 0);
	CLIEF_R_LIS.sqlin ((long *)  &TLIEF_R_LIS.lst_nr, SQLLONG, 0);
	m_DeleteCursor = CLIEF_R_LIS.sqlcursor (_T("delete from lief_r_lis ")
									  _T("where mdn = ? ")
									  _T("and fil = ? ")
									  _T("and lst_nr = ? "));
	m_ListRow = NULL;
	m_ProzSum = 0.0;
}

CLiefRLisCore::~CLiefRLisCore(void)
{
	CLIEF_R_LIS.sqlclose (m_Cursor);
	m_LiefRLisTab.DestroyElements ();
	m_LiefRLisTab.Destroy ();
}

CLiefRLisCore *CLiefRLisCore::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CLiefRLisCore ();
	}
	return Instance;
}

void CLiefRLisCore::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

BOOL CLiefRLisCore::Read ()
{
	BOOL ret = FALSE;
	int dsqlstatus;
	m_LiefRLisTab.DestroyElements ();

	if (m_ListRow != NULL)
	{
		dsqlstatus = CLIEF_R_LIS.sqlopen (m_Cursor);
		if (dsqlstatus == 0)
		{
			while (CLIEF_R_LIS.sqlfetch (m_Cursor) == 0)
			{
				ret = TRUE;
				dsqlstatus = DATABASE->ReadLiefRLis ();
				double rab_wrt = TLIEF_R_LIS.rab_wrt;
				CString RabSt = TLIEF_R_LIS.rab_st;
				m_ListRow->Add ();
				LIEF_R_LIS *tabrow = new LIEF_R_LIS;
				memcpy (tabrow, &TLIEF_R_LIS, sizeof (LIEF_R_LIS)); 
				m_LiefRLisTab.Add (tabrow);
			}
		}
		m_ListRow->Update ();
	}
	return ret;
}

BOOL CLiefRLisCore::Write ()
{
	BOOL ret = TRUE;
	LIEF_R_LIS **it;
	LIEF_R_LIS *tabrow;

	m_LiefRLisTab.Start ();
	while ((it = m_LiefRLisTab.GetNext ()) != NULL)
	{
		tabrow = *it;
		if (tabrow != NULL)
		{
			if (tabrow->rab_wrt > 999999.99 ||
				tabrow->rab_wrt < -999999.99)
			{
				ret = FALSE;
				break;
			}
		}
	}

	if (ret)
	{
		CLIEF_R_LIS.sqlexecute (m_DeleteCursor);
		long lfd = 1;
		m_LiefRLisTab.Start ();
		while ((it = m_LiefRLisTab.GetNext ()) != NULL)
		{
			tabrow = *it;
			if (tabrow != NULL && tabrow->rab_wrt != 0.0)
			{
				memcpy (&TLIEF_R_LIS, &lief_r_lis_null, sizeof (LIEF_R_LIS));
				TLIEF_R_LIS.mdn = TLIEF.mdn;
				TLIEF_R_LIS.fil = TLIEF.fil;
				TLIEF_R_LIS.lst_nr = TLIEF.lst_nr;
				TLIEF_R_LIS.a = tabrow->a;
				_tcscpy (TLIEF_R_LIS.rab_st, tabrow->rab_st);
				_tcscpy (TLIEF_R_LIS.rab_typ, tabrow->rab_typ);
				TLIEF_R_LIS.rab_wrt =  tabrow->rab_wrt;
				memcpy (tabrow, &TLIEF_R_LIS, sizeof (LIEF_R_LIS));
				CLIEF_R_LIS.dbupdate ();
				lfd ++;
			}
		}
	}
	return ret;
}


void CLiefRLisCore::UpdateRow (int row)
{
	int rows = m_LiefRLisTab.anz;
	if (row >= rows)
	{
		LIEF_R_LIS *tabrow = new LIEF_R_LIS;
		memcpy (tabrow, &TLIEF_R_LIS, sizeof (LIEF_R_LIS)); 
		m_LiefRLisTab.Add (tabrow);
	}
	else
	{
	    LIEF_R_LIS *tabrow = *m_LiefRLisTab.Get (row);
		memcpy (tabrow, &TLIEF_R_LIS, sizeof (LIEF_R_LIS)); 
	}
	m_ListRow->Update ();
}

void CLiefRLisCore::DeleteRow (int row)
{
    if (m_LiefRLisTab.Get (row) != NULL)
	{
		LIEF_R_LIS *tabrow = *m_LiefRLisTab.Get (row);
		m_LiefRLisTab.Drop (row);
		delete tabrow;
		m_ListRow->Update ();
	}
}
