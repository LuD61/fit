#include "stdafx.h"
#include "ChoiceAdr.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceAdr::Sort1 = -1;
int CChoiceAdr::Sort2 = -1;
int CChoiceAdr::Sort3 = -1;

CChoiceAdr::CChoiceAdr(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_AdrTyp = 25;
}

CChoiceAdr::~CChoiceAdr() 
{
	DestroyList ();
}

void CChoiceAdr::DestroyList() 
{
	for (std::vector<CAdrList *>::iterator pabl = AdrList.begin (); pabl != AdrList.end (); ++pabl)
	{
		CAdrList *abl = *pabl;
		delete abl;
	}
    AdrList.clear ();
}

void CChoiceAdr::FillList () 
{
    long  adr;
    TCHAR adr_krz [34];
	CString Sql;

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Adressen"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Adresse"),  1, 100, LVCFMT_RIGHT);
    SetCol (_T("Name"),  2, 250);

	if (AdrList.size () == 0)
	{
		DbClass->sqlout ((long *)&adr,      SQLLONG, 0);
	    DbClass->sqlout ((LPTSTR)  adr_krz,   SQLCHAR, sizeof (adr_krz));
		if (m_AdrTyp == 0)
		{
			Sql =  (_T("select adr.adr, adr.adr_krz ")
					 _T("from adr where adr.adr > 0 "));
		}
		else
		{
			Sql.Format (_T("select adr.adr, adr.adr_krz ")
					    _T("from adr where adr.adr > 0 and adr_typ = %hd"), m_AdrTyp);
		}
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
										 
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) adr_krz;
			CDbUniCode::DbToUniCode (adr_krz, pos);
			CAdrList *abl = new CAdrList (adr, adr_krz);
			AdrList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CAdrList *>::iterator pabl = AdrList.begin (); pabl != AdrList.end (); ++pabl)
	{
		CAdrList *abl = *pabl;
		CString Adr;
		Adr.Format (_T("%ld"), abl->adr); 
		CString Num;
		CString Bez;
		_tcscpy (adr_krz, abl->adr_krz.GetBuffer ());

		CString LText;
		LText.Format (_T("%ld %s"), abl->adr, 
									abl->adr_krz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Adr;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (adr_krz, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort (listView);
}


void CChoiceAdr::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAdr::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAdr::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

	for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAdr::SearchAdrKrz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAdr::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchAdrKrz (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceAdr::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceAdr::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {
	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   return 0;
}


void CChoiceAdr::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CAdrList *abl = AdrList [i];
		   
		   abl->adr     = _tstoi (ListBox->GetItemText (i, 1));
		   abl->adr_krz = ListBox->GetItemText (i, 2);
	}
}

void CChoiceAdr::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = AdrList [idx];
}

CAdrList *CChoiceAdr::GetSelectedText ()
{
	CAdrList *abl = (CAdrList *) SelectedRow;
	return abl;
}

