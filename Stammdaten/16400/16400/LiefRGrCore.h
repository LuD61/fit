#pragma once
#include "DatabaseCore.h"
#include "ListRow.h"
#include "DataCollection.h"

class CLiefRGrCore
{
private:
	static CLiefRGrCore *Instance; 
	int m_Cursor;
	int m_DeleteCursor;
	int m_Rows;
	CListRow *m_ListRow;
	CDataCollection<LIEF_R_GR *> m_LiefRGrTab;

public:

	double m_ProzSum;
	void SetListRow (CListRow *ListRow)
	{
		m_ListRow = ListRow;
	}

	void SetRow (int rows)
	{
		m_Rows = rows;
	}

	int GetRow ()
	{
		return m_Rows;
	}

	CLiefRGrCore(void);
	~CLiefRGrCore(void);

	static CLiefRGrCore *GetInstance ();
	static void DestroyInstance ();
	BOOL Read ();
	BOOL Write ();
	long NextLfd ();
	void UpdateRow (int row);
	void DeleteRow (int row);
};

#define LIEF_R_GR_CORE CLiefRGrCore::GetInstance ()