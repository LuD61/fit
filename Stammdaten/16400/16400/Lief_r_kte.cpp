#include "stdafx.h"
#include "lief_r_kte.h"

struct LIEF_R_KTE lief_r_kte, lief_r_kte_null, lief_r_kte_def;

void LIEF_R_KTE_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &lief_r_kte.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_kte.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_kte.lief,   SQLCHAR, sizeof (lief_r_kte.lief));
            sqlin ((long *)   &lief_r_kte.lfd,   SQLLONG, 0);
    sqlout ((short *) &lief_r_kte.fil,SQLSHORT,0);
    sqlout ((long *) &lief_r_kte.lfd,SQLLONG,0);
    sqlout ((TCHAR *) lief_r_kte.lief,SQLCHAR,17);
    sqlout ((long *) &lief_r_kte.lief_s,SQLLONG,0);
    sqlout ((short *) &lief_r_kte.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) lief_r_kte.rab_bz,SQLCHAR,25);
    sqlout ((double *) &lief_r_kte.rab_proz,SQLDOUBLE,0);
    sqlout ((double *) &lief_r_kte.proz_sum,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &lief_r_kte.von_dat,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &lief_r_kte.bis_dat,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &lief_r_kte.ae_dat,SQLDATE,0);
    sqlout ((TCHAR *) lief_r_kte.pers_nam,SQLCHAR,9);
            cursor = sqlcursor (_T("select lief_r_kte.fil,  ")
_T("lief_r_kte.lfd,  lief_r_kte.lief,  lief_r_kte.lief_s,  lief_r_kte.mdn,  ")
_T("lief_r_kte.rab_bz,  lief_r_kte.rab_proz,  lief_r_kte.proz_sum,  ")
_T("lief_r_kte.von_dat,  lief_r_kte.bis_dat,  lief_r_kte.ae_dat,  ")
_T("lief_r_kte.pers_nam from lief_r_kte ")

#line 15 "Lief_r_kte.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
    sqlin ((short *) &lief_r_kte.fil,SQLSHORT,0);
    sqlin ((long *) &lief_r_kte.lfd,SQLLONG,0);
    sqlin ((TCHAR *) lief_r_kte.lief,SQLCHAR,17);
    sqlin ((long *) &lief_r_kte.lief_s,SQLLONG,0);
    sqlin ((short *) &lief_r_kte.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) lief_r_kte.rab_bz,SQLCHAR,25);
    sqlin ((double *) &lief_r_kte.rab_proz,SQLDOUBLE,0);
    sqlin ((double *) &lief_r_kte.proz_sum,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &lief_r_kte.von_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &lief_r_kte.bis_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &lief_r_kte.ae_dat,SQLDATE,0);
    sqlin ((TCHAR *) lief_r_kte.pers_nam,SQLCHAR,9);
            sqltext = _T("update lief_r_kte set ")
_T("lief_r_kte.fil = ?,  lief_r_kte.lfd = ?,  lief_r_kte.lief = ?,  ")
_T("lief_r_kte.lief_s = ?,  lief_r_kte.mdn = ?,  lief_r_kte.rab_bz = ?,  ")
_T("lief_r_kte.rab_proz = ?,  lief_r_kte.proz_sum = ?,  ")
_T("lief_r_kte.von_dat = ?,  lief_r_kte.bis_dat = ?,  ")
_T("lief_r_kte.ae_dat = ?,  lief_r_kte.pers_nam = ? ")

#line 20 "Lief_r_kte.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?");
            sqlin ((short *)   &lief_r_kte.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_kte.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_kte.lief,   SQLCHAR, sizeof (lief_r_kte.lief));
            sqlin ((long *)   &lief_r_kte.lfd,   SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &lief_r_kte.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_kte.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_kte.lief,   SQLCHAR, sizeof (lief_r_kte.lief));
            sqlin ((long *)   &lief_r_kte.lfd,   SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select lief from lief_r_kte ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
            sqlin ((short *)   &lief_r_kte.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_kte.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_kte.lief,   SQLCHAR, sizeof (lief_r_kte.lief));
            sqlin ((long *)   &lief_r_kte.lfd,   SQLLONG, 0);
            test_lock_cursor = sqlcursor (_T("update lief_r_kte set fil = fil ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
            sqlin ((short *)   &lief_r_kte.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lief_r_kte.fil,  SQLSHORT, 0);
            sqlin ((char *)   lief_r_kte.lief,   SQLCHAR, sizeof (lief_r_kte.lief));
            sqlin ((long *)   &lief_r_kte.lfd,   SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from lief_r_kte ")
                                  _T("where mdn = ? ")
                                  _T("and fil   = ? ")
                                  _T("and lief  = ? ")
                                  _T("and lfd   = ?"));
    sqlin ((short *) &lief_r_kte.fil,SQLSHORT,0);
    sqlin ((long *) &lief_r_kte.lfd,SQLLONG,0);
    sqlin ((TCHAR *) lief_r_kte.lief,SQLCHAR,17);
    sqlin ((long *) &lief_r_kte.lief_s,SQLLONG,0);
    sqlin ((short *) &lief_r_kte.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) lief_r_kte.rab_bz,SQLCHAR,25);
    sqlin ((double *) &lief_r_kte.rab_proz,SQLDOUBLE,0);
    sqlin ((double *) &lief_r_kte.proz_sum,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &lief_r_kte.von_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &lief_r_kte.bis_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &lief_r_kte.ae_dat,SQLDATE,0);
    sqlin ((TCHAR *) lief_r_kte.pers_nam,SQLCHAR,9);
            ins_cursor = sqlcursor (_T("insert into lief_r_kte (")
_T("fil,  lfd,  lief,  lief_s,  mdn,  rab_bz,  rab_proz,  proz_sum,  von_dat,  bis_dat,  ")
_T("ae_dat,  pers_nam) ")

#line 58 "Lief_r_kte.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?)"));

#line 60 "Lief_r_kte.rpp"
}
