#include "StdAfx.h"
#include "LiefRKteListCtrl.h"
#include "LiefRKteCore.h"
#include "DatabaseCore.h"
#include "StrFuncs.h"
#include "resource.h"

CLiefRKteListCtrl::CLiefRKteListCtrl(void)
{
	ListRows.Init ();
}

CLiefRKteListCtrl::~CLiefRKteListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CLiefRKteListCtrl, CEditListCtrl)
//	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CLiefRKteListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (1, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (1, 0);
	}
}

void CLiefRKteListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	int count = GetHeaderCtrl ()->GetItemCount ();
	while (IsDisplayOnly (col) && col < count - 1) col ++;
	CEditListCtrl::StartEnter (col, row);
	if (col == PosRabBz)
	{
		ListEdit.SetLimitText (sizeof (TLIEF_R_KTE.rab_bz) -1);
	}
	else if (col == PosRab)
	{
		ListEdit.SetLimitText (8);
	}
}

void CLiefRKteListCtrl::StopEnter ()
{
	CString cLfd;
	CString cRabatt;
	CString cRabBz;

	CEditListCtrl::StopEnter ();
	UpdateRow ();
	cLfd.Format (_T("%ld"), TLIEF_R_KTE.lfd);
	cRabatt.Format (_T("%.2lf"), TLIEF_R_KTE.rab_proz);
	cRabBz = TLIEF_R_KTE.rab_bz;
	cRabBz.TrimRight (); 
	FillList.SetItemText (cLfd.GetBuffer (), EditRow, 1);
	FillList.SetItemText (cRabatt.GetBuffer (), EditRow, 2);
	FillList.SetItemText (cRabBz.GetBuffer (), EditRow, 3);
}

void CLiefRKteListCtrl::SetSel (CString& Text)
{
/*
   {
		ListEdit.SetSel (0, -1);
   }
*/
   if (EditCol == PosRab)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CLiefRKteListCtrl::FormatText (CString& Text)
{
}

void CLiefRKteListCtrl::NextRow ()
{
	int count = GetItemCount ();
	SetEditText ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;

	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
	StartEnter (EditCol, EditRow);
}

void CLiefRKteListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CLiefRKteListCtrl::LastCol ()
{
	if (EditCol < PosRabBz) return FALSE;
	return TRUE;
}

void CLiefRKteListCtrl::UpdateRow ()
{
	CString cLfd         =  GetItemText (EditRow, PosLfd);
	CString cRab         =  GetItemText (EditRow, PosRab);
	CString cRabBz       =  GetItemText (EditRow, PosRabBz);
	TLIEF_R_KTE.lfd      = _tstol (cLfd.GetBuffer ());
	TLIEF_R_KTE.rab_proz = CStrFuncs::StrToDouble (cRab);
	_tcscpy (TLIEF_R_KTE.rab_bz, cRabBz.GetBuffer ());
	LIEF_R_KTE_CORE->UpdateRow (EditRow);
}

void CLiefRKteListCtrl::OnReturn ()
{

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (LastCol ())
	{
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
//		EditCol = 0;
		if (EditRow == rowCount)
		{
			EditCol = 1;
		}
		else
		{
			EditCol = 2;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
		if (IsDisplayOnly (EditCol))
		{
			EditCol ++;
		}
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CLiefRKteListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CLiefRKteListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	StopEnter ();
	EditCol --;
	while (IsDisplayOnly (EditCol) && EditCol >= 0) EditCol --;
    StartEnter (EditCol, EditRow);
}

BOOL CLiefRKteListCtrl::InsertRow ()
{
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T("  "), EditRow, 1);
	FillList.SetItemText (_T("  "), EditRow, 2);
	FillList.SetItemText (_T("  "), EditRow, 3);
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CLiefRKteListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	int row = EditRow;
	BOOL ret = CEditListCtrl::DeleteRow ();
	LIEF_R_KTE_CORE->DeleteRow (row);
	return ret;
}

BOOL CLiefRKteListCtrl::AppendEmpty ()
{
	long lfd = 0;
	CString cLfd;
	int rowCount = GetItemCount ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CWnd *header = GetHeaderCtrl ();
	lfd = LIEF_R_KTE_CORE->NextLfd ();
	FillList.InsertItem (rowCount, -1);
	cLfd.Format (_T("%ld"), lfd);
	FillList.SetItemText (cLfd.GetBuffer (), rowCount, PosLfd);
	FillList.SetItemText (_T("0,00"), rowCount, PosRab);
	FillList.SetItemText (_T(" "), rowCount, PosRabBz);
	rowCount = GetItemCount ();
	return TRUE;
}

void CLiefRKteListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CLiefRKteListCtrl::RunItemClicked (int Item)
{
}

void CLiefRKteListCtrl::RunCtrlItemClicked (int Item)
{
}

void CLiefRKteListCtrl::RunShiftItemClicked (int Item)
{
}

void CLiefRKteListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	Text = cText.Trim ();
}

void CLiefRKteListCtrl::ScrollPositions (int pos)
{
}

