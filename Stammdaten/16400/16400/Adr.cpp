#include "stdafx.h"
#include "adr.h"

struct ADR adr, adr_null;

void ADR_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((long *)   &adr.adr,  SQLLONG, 0);
    sqlout ((long *) &adr.adr,SQLLONG,0);
    sqlout ((TCHAR *) adr.adr_krz,SQLCHAR,17);
    sqlout ((TCHAR *) adr.adr_nam1,SQLCHAR,37);
    sqlout ((TCHAR *) adr.adr_nam2,SQLCHAR,37);
    sqlout ((short *) &adr.adr_typ,SQLSHORT,0);
    sqlout ((TCHAR *) adr.adr_verkt,SQLCHAR,17);
    sqlout ((short *) &adr.anr,SQLSHORT,0);
    sqlout ((short *) &adr.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) adr.fax,SQLCHAR,21);
    sqlout ((short *) &adr.fil,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &adr.geb_dat,SQLDATE,0);
    sqlout ((short *) &adr.land,SQLSHORT,0);
    sqlout ((short *) &adr.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) adr.merkm_1,SQLCHAR,3);
    sqlout ((TCHAR *) adr.merkm_2,SQLCHAR,3);
    sqlout ((TCHAR *) adr.merkm_3,SQLCHAR,3);
    sqlout ((TCHAR *) adr.merkm_4,SQLCHAR,3);
    sqlout ((TCHAR *) adr.merkm_5,SQLCHAR,3);
    sqlout ((TCHAR *) adr.modem,SQLCHAR,21);
    sqlout ((TCHAR *) adr.ort1,SQLCHAR,37);
    sqlout ((TCHAR *) adr.ort2,SQLCHAR,37);
    sqlout ((TCHAR *) adr.partner,SQLCHAR,37);
    sqlout ((TCHAR *) adr.pf,SQLCHAR,17);
    sqlout ((TCHAR *) adr.plz,SQLCHAR,9);
    sqlout ((short *) &adr.staat,SQLSHORT,0);
    sqlout ((TCHAR *) adr.str,SQLCHAR,37);
    sqlout ((TCHAR *) adr.tel,SQLCHAR,21);
    sqlout ((TCHAR *) adr.telex,SQLCHAR,21);
    sqlout ((long *) &adr.txt_nr,SQLLONG,0);
    sqlout ((TCHAR *) adr.plz_postf,SQLCHAR,9);
    sqlout ((TCHAR *) adr.plz_pf,SQLCHAR,9);
    sqlout ((TCHAR *) adr.iln,SQLCHAR,33);
    sqlout ((TCHAR *) adr.email,SQLCHAR,37);
    sqlout ((TCHAR *) adr.swift,SQLCHAR,25);
    sqlout ((TCHAR *) adr.adr_nam3,SQLCHAR,37);
            cursor = sqlcursor (_T("select adr.adr,  ")
_T("adr.adr_krz,  adr.adr_nam1,  adr.adr_nam2,  adr.adr_typ,  adr.adr_verkt,  ")
_T("adr.anr,  adr.delstatus,  adr.fax,  adr.fil,  adr.geb_dat,  adr.land,  adr.mdn,  ")
_T("adr.merkm_1,  adr.merkm_2,  adr.merkm_3,  adr.merkm_4,  adr.merkm_5,  ")
_T("adr.modem,  adr.ort1,  adr.ort2,  adr.partner,  adr.pf,  adr.plz,  adr.staat,  ")
_T("adr.str,  adr.tel,  adr.telex,  adr.txt_nr,  adr.plz_postf,  adr.plz_pf,  ")
_T("adr.iln,  adr.email,  adr.swift,  adr.adr_nam3 from adr ")

#line 12 "adr.rpp"
                                  _T("where adr = ?"));
    sqlin ((long *) &adr.adr,SQLLONG,0);
    sqlin ((TCHAR *) adr.adr_krz,SQLCHAR,17);
    sqlin ((TCHAR *) adr.adr_nam1,SQLCHAR,37);
    sqlin ((TCHAR *) adr.adr_nam2,SQLCHAR,37);
    sqlin ((short *) &adr.adr_typ,SQLSHORT,0);
    sqlin ((TCHAR *) adr.adr_verkt,SQLCHAR,17);
    sqlin ((short *) &adr.anr,SQLSHORT,0);
    sqlin ((short *) &adr.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) adr.fax,SQLCHAR,21);
    sqlin ((short *) &adr.fil,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &adr.geb_dat,SQLDATE,0);
    sqlin ((short *) &adr.land,SQLSHORT,0);
    sqlin ((short *) &adr.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) adr.merkm_1,SQLCHAR,3);
    sqlin ((TCHAR *) adr.merkm_2,SQLCHAR,3);
    sqlin ((TCHAR *) adr.merkm_3,SQLCHAR,3);
    sqlin ((TCHAR *) adr.merkm_4,SQLCHAR,3);
    sqlin ((TCHAR *) adr.merkm_5,SQLCHAR,3);
    sqlin ((TCHAR *) adr.modem,SQLCHAR,21);
    sqlin ((TCHAR *) adr.ort1,SQLCHAR,37);
    sqlin ((TCHAR *) adr.ort2,SQLCHAR,37);
    sqlin ((TCHAR *) adr.partner,SQLCHAR,37);
    sqlin ((TCHAR *) adr.pf,SQLCHAR,17);
    sqlin ((TCHAR *) adr.plz,SQLCHAR,9);
    sqlin ((short *) &adr.staat,SQLSHORT,0);
    sqlin ((TCHAR *) adr.str,SQLCHAR,37);
    sqlin ((TCHAR *) adr.tel,SQLCHAR,21);
    sqlin ((TCHAR *) adr.telex,SQLCHAR,21);
    sqlin ((long *) &adr.txt_nr,SQLLONG,0);
    sqlin ((TCHAR *) adr.plz_postf,SQLCHAR,9);
    sqlin ((TCHAR *) adr.plz_pf,SQLCHAR,9);
    sqlin ((TCHAR *) adr.iln,SQLCHAR,33);
    sqlin ((TCHAR *) adr.email,SQLCHAR,37);
    sqlin ((TCHAR *) adr.swift,SQLCHAR,25);
    sqlin ((TCHAR *) adr.adr_nam3,SQLCHAR,37);
            sqltext = _T("update adr set adr.adr = ?,  ")
_T("adr.adr_krz = ?,  adr.adr_nam1 = ?,  adr.adr_nam2 = ?,  ")
_T("adr.adr_typ = ?,  adr.adr_verkt = ?,  adr.anr = ?,  adr.delstatus = ?,  ")
_T("adr.fax = ?,  adr.fil = ?,  adr.geb_dat = ?,  adr.land = ?,  adr.mdn = ?,  ")
_T("adr.merkm_1 = ?,  adr.merkm_2 = ?,  adr.merkm_3 = ?,  adr.merkm_4 = ?,  ")
_T("adr.merkm_5 = ?,  adr.modem = ?,  adr.ort1 = ?,  adr.ort2 = ?,  ")
_T("adr.partner = ?,  adr.pf = ?,  adr.plz = ?,  adr.staat = ?,  adr.str = ?,  ")
_T("adr.tel = ?,  adr.telex = ?,  adr.txt_nr = ?,  adr.plz_postf = ?,  ")
_T("adr.plz_pf = ?,  adr.iln = ?,  adr.email = ?,  adr.swift = ?,  ")
_T("adr.adr_nam3 = ? ")

#line 14 "adr.rpp"
                                  _T("where adr = ?");
            sqlin ((long *)   &adr.adr,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &adr.adr,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select adr from adr ")
                                  _T("where adr = ?"));
            sqlin ((long *)   &adr.adr,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from adr ")
                                  _T("where adr = ?"));
    sqlin ((long *) &adr.adr,SQLLONG,0);
    sqlin ((TCHAR *) adr.adr_krz,SQLCHAR,17);
    sqlin ((TCHAR *) adr.adr_nam1,SQLCHAR,37);
    sqlin ((TCHAR *) adr.adr_nam2,SQLCHAR,37);
    sqlin ((short *) &adr.adr_typ,SQLSHORT,0);
    sqlin ((TCHAR *) adr.adr_verkt,SQLCHAR,17);
    sqlin ((short *) &adr.anr,SQLSHORT,0);
    sqlin ((short *) &adr.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) adr.fax,SQLCHAR,21);
    sqlin ((short *) &adr.fil,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &adr.geb_dat,SQLDATE,0);
    sqlin ((short *) &adr.land,SQLSHORT,0);
    sqlin ((short *) &adr.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) adr.merkm_1,SQLCHAR,3);
    sqlin ((TCHAR *) adr.merkm_2,SQLCHAR,3);
    sqlin ((TCHAR *) adr.merkm_3,SQLCHAR,3);
    sqlin ((TCHAR *) adr.merkm_4,SQLCHAR,3);
    sqlin ((TCHAR *) adr.merkm_5,SQLCHAR,3);
    sqlin ((TCHAR *) adr.modem,SQLCHAR,21);
    sqlin ((TCHAR *) adr.ort1,SQLCHAR,37);
    sqlin ((TCHAR *) adr.ort2,SQLCHAR,37);
    sqlin ((TCHAR *) adr.partner,SQLCHAR,37);
    sqlin ((TCHAR *) adr.pf,SQLCHAR,17);
    sqlin ((TCHAR *) adr.plz,SQLCHAR,9);
    sqlin ((short *) &adr.staat,SQLSHORT,0);
    sqlin ((TCHAR *) adr.str,SQLCHAR,37);
    sqlin ((TCHAR *) adr.tel,SQLCHAR,21);
    sqlin ((TCHAR *) adr.telex,SQLCHAR,21);
    sqlin ((long *) &adr.txt_nr,SQLLONG,0);
    sqlin ((TCHAR *) adr.plz_postf,SQLCHAR,9);
    sqlin ((TCHAR *) adr.plz_pf,SQLCHAR,9);
    sqlin ((TCHAR *) adr.iln,SQLCHAR,33);
    sqlin ((TCHAR *) adr.email,SQLCHAR,37);
    sqlin ((TCHAR *) adr.swift,SQLCHAR,25);
    sqlin ((TCHAR *) adr.adr_nam3,SQLCHAR,37);
            ins_cursor = sqlcursor (_T("insert into adr (adr,  ")
_T("adr_krz,  adr_nam1,  adr_nam2,  adr_typ,  adr_verkt,  anr,  delstatus,  fax,  fil,  ")
_T("geb_dat,  land,  mdn,  merkm_1,  merkm_2,  merkm_3,  merkm_4,  merkm_5,  modem,  ort1,  ")
_T("ort2,  partner,  pf,  plz,  staat,  str,  tel,  telex,  txt_nr,  plz_postf,  plz_pf,  iln,  ")
_T("email,  swift,  adr_nam3) ")

#line 25 "adr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 27 "adr.rpp"
}
