#ifndef _LINE_DEF
#define _LINE_DEF
#pragma once
#include "afxwin.h"

#define HORIZONTAL 1
#define VERTICAL 2

class CLine :
	public CStatic
{
public:
	int Direction;
	CLine(void);
	~CLine(void);
protected:
	virtual void DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct);
};
#endif
