#pragma once
#include "LiefRStfListCtrl.h"
#include "FillList.h"
#include "BkBitmap.h"
#include "VLabel.h"
#include "ColorButton.h"
#include "CtrlGridColor.h"
#include "ListRow.h"
#include "FormTab.h"
#include "afxwin.h"

// CLiefRStfDlg-Dialogfeld

class CLiefRStfDlg : public CDialog, CListRow
{
	DECLARE_DYNAMIC(CLiefRStfDlg)

public:
	CLiefRStfDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CLiefRStfDlg();

// Dialogfelddaten
	enum { IDD = IDD_LIEF_R_STF_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) ;
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnSize (UINT, int, int);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC); 

	DECLARE_MESSAGE_MAP()

private:
	COLORREF m_DlgColor;
	HBRUSH m_DlgBrush;
	HBRUSH m_ProzSumBrush;
	CBkBitmap BkBitmap;
	CFillList FillList;
	CFont Font;
	CFont CaptionFont;
	CFont StaticFont;
	CFormTab Form;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid ControlGrid;

	CVLabel m_Caption;
	CColorButton m_OkEx;
	CColorButton m_Delete;
	CColorButton m_CancelEx;
	CLiefRStfListCtrl m_List;
	int m_ListRow;

public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedDelete();
	afx_msg void OnBnClickedCancel();
	BOOL Read ();
	BOOL Write ();

// ListRow

	void Add ();
	void Update ();
};
