#ifndef _LIEF_LIST_DEF
#define _LIEF_LIST_DEF
#pragma once

class CLiefList
{
public:
	short mdn;
	short fil;
	CString lief;
	CString adr_krz;
	CString kun_bran2;
	CString adr_nam1;
	CString adr_nam2;
	CString str;
	CString plz;
	CString ort1;
	CString tel;
	CString fax;
	CString email;
	short staat;
	short land;
	CLiefList(void);
	CLiefList(short, short, LPTSTR, LPTSTR);
	~CLiefList(void);
};
#endif
