#pragma once
#include "DatabaseCore.h"
#include "ListRow.h"
#include "DataCollection.h"

class CLiefRLisCore
{
private:
	static CLiefRLisCore *Instance; 
	int m_Cursor;
	int m_DeleteCursor;
	int m_Rows;
	CListRow *m_ListRow;
	CDataCollection<LIEF_R_LIS *> m_LiefRLisTab;
	CString m_Bez;

public:

	double m_ProzSum;

	LPTSTR Bez ()
	{
		return m_Bez.GetBuffer ();
	}

	void SetListRow (CListRow *ListRow)
	{
		m_ListRow = ListRow;
	}

	void SetRow (int rows)
	{
		m_Rows = rows;
	}

	int GetRow ()
	{
		return m_Rows;
	}

	CLiefRLisCore(void);
	~CLiefRLisCore(void);

	static CLiefRLisCore *GetInstance ();
	static void DestroyInstance ();
	BOOL Read ();
	BOOL Write ();
	void UpdateRow (int row);
	void DeleteRow (int row);
};

#define LIEF_R_LIS_CORE CLiefRLisCore::GetInstance ()