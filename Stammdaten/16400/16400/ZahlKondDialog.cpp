// ZahlKondDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16400.h"
#include "ZahlKondDialog.h"
#include "uniformfield.h"
#include "DatabaseCore.h"
#include "WindowHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CZahlKondDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CZahlKondDialog, CDialog)

CZahlKondDialog::CZahlKondDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CZahlKondDialog::IDD, pParent)
{
	m_DlgBrush = NULL;
	m_DlgColor = RGB (250, 250, 202);
	m_ReadOnly = FALSE;
}

CZahlKondDialog::~CZahlKondDialog()
{
	if (m_DlgBrush != NULL)
	{
		DeleteObject (m_DlgBrush);
		m_DlgBrush = 0;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
	CtrlGrid.DestroyGrid ();
}

void CZahlKondDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAPTION, m_Caption);
	DDX_Control(pDX, IDC_ZAHL_KOND_GROUP, m_ZahlKondGroup);
	DDX_Control(pDX, IDC_LZAHL_KOND, m_LZahlKond);
	DDX_Control(pDX, IDC_ZAHL_KOND, m_ZahlKond);
	DDX_Control(pDX, IDC_ZIEL1, m_Ziel1);
	DDX_Control(pDX, IDC_ZIEL2, m_Ziel2);
	DDX_Control(pDX, IDC_ZIEL3, m_Ziel3);
	DDX_Control(pDX, IDC_TXT, m_Txt);
	DDX_Control(pDX, IDC_LZIEL1, m_LZiel1);
	DDX_Control(pDX, IDC_LZIEL2, m_LZiel2);
	DDX_Control(pDX, IDC_LZIEL3, m_LZiel3);
	DDX_Control(pDX, IDC_SKTO1, m_Skto1);
	DDX_Control(pDX, IDC_SKTO2, m_Skto2);
	DDX_Control(pDX, IDC_SKTO3, m_Skto3);
	DDX_Control(pDX, IDC_LSKTO1, m_LSkto1);
	DDX_Control(pDX, IDC_LSKTO2, m_LSkto2);
	DDX_Control(pDX, IDC_LSKTO3, m_LSkto3);
	DDX_Control(pDX, IDC_LTXT, m_LTxt);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDCANCEL, m_Cancel);
	DDX_Control(pDX, IDC_OK, m_OkEx);
	DDX_Control(pDX, IDC_CANCEL, m_CancelEx);
}


BEGIN_MESSAGE_MAP(CZahlKondDialog, CDialog)
	ON_WM_CTLCOLOR ()
	ON_BN_CLICKED(IDC_OK, &CZahlKondDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CANCEL, &CZahlKondDialog::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CZahlKondDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CZahlKondDialog::OnBnClickedCancel)
END_MESSAGE_MAP()


// CZahlKondDialog-Meldungshandler

BOOL CZahlKondDialog::OnInitDialog ()
{
	BOOL ret = CDialog::OnInitDialog ();

	m_OK.ShowWindow (SW_HIDE);
	m_Cancel.ShowWindow (SW_HIDE);

	m_Caption.SetParts (60);
	m_Caption.SetWindowText (_T(" Zahlungskonditionen"));
	m_Caption.SetBoderStyle (m_Caption.Solide);
	m_Caption.SetOrientation (m_Caption.Left);
	m_Caption.SetDynamicColor (TRUE);
	m_Caption.SetTextColor (RGB (255, 255, 153));
	m_Caption.SetBkColor (RGB (192, 192, 192));

//	COLORREF BkColor (CColorButton::DynColorGray);
//	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF BkColor (RGB (0, 0, 255));
//	COLORREF DynColor = RGB (0, 0, 255);
	COLORREF RolloverColor = CColorButton::DynColorBlue;
	COLORREF DynColor = CColorButton::DynColorBlue;
//	COLORREF RolloverColor = RGB (192, 192, 192);

	m_OkEx.SetWindowText (_T("OK"));
	m_OkEx.nID = IDC_OK;
//	m_OkEx.SetToolTip (_T("Bearbeiten"));
	m_OkEx.Orientation = m_OkEx.Center;
	m_OkEx.TextStyle = m_OkEx.Standard;
	m_OkEx.BorderStyle = m_OkEx.Solide;
	m_OkEx.DynamicColor = TRUE;
	m_OkEx.TextColor =RGB (0, 0, 0);
	m_OkEx.SetBkColor (BkColor);
	m_OkEx.DynColor = DynColor;
	m_OkEx.RolloverColor = RolloverColor;
	m_OkEx.LoadBitmap (IDB_OKEX);
	m_OkEx.LoadMask (IDB_OKEX_MASK);

	m_CancelEx.SetWindowText (_T("Abbrechen"));
	m_CancelEx.nID = IDC_CANCEL;
//	m_CancelEx.SetToolTip (_T("Bearbeiten"));
	m_CancelEx.Orientation = m_CancelEx.Center;
	m_CancelEx.TextStyle = m_CancelEx.Standard;
	m_CancelEx.BorderStyle = m_CancelEx.Solide;
	m_CancelEx.DynamicColor = TRUE;
	m_CancelEx.TextColor =RGB (0, 0, 0);
	m_CancelEx.SetBkColor (BkColor);
	m_CancelEx.DynColor = DynColor;
	m_CancelEx.RolloverColor = RolloverColor;
	m_CancelEx.LoadBitmap (IDB_CANCELEX);
	m_CancelEx.LoadMask (IDB_CANCELEX_MASK);

	if (m_ReadOnly)
	{
		m_ZahlKond.SetReadOnly ();
		m_ZahlKond.ModifyStyle (WS_TABSTOP, 0);
	}


	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	Form.Add (new CFormField (&m_ZahlKond,EDIT,         (short *) &TLIEF.zahl_kond, VSHORT, 4));
	Form.Add (new CFormField (&m_Skto1,EDIT,            (double *)&TZAHL_KOND.skto1, VDOUBLE, 3,1));
	Form.Add (new CFormField (&m_Skto2,EDIT,            (double *)&TZAHL_KOND.skto2, VDOUBLE, 3,1));
	Form.Add (new CFormField (&m_Skto3,EDIT,            (double *)&TZAHL_KOND.skto3, VDOUBLE, 3,1));
	Form.Add (new CFormField (&m_Ziel1,EDIT,            (short  *)&TZAHL_KOND.ziel1, VSHORT, 2));
	Form.Add (new CFormField (&m_Ziel2,EDIT,            (short  *)&TZAHL_KOND.ziel2, VSHORT, 2));
	Form.Add (new CFormField (&m_Ziel3,EDIT,            (short  *)&TZAHL_KOND.ziel3, VSHORT, 2));
	Form.Add (new CUniFormField (&m_Txt,EDIT,           (char *)  TZAHL_KOND.txt, VCHAR, sizeof (TZAHL_KOND.txt) - 1));

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	ZahlKondGrid.Create (this, 5, 5);
    ZahlKondGrid.SetBorder (0, 0);
    ZahlKondGrid.SetGridSpace (0, 2);
	CCtrlInfo *c_ZahlKond = new CCtrlInfo (&m_ZahlKond, 0, 0, 1, 1);
	ZahlKondGrid.Add (c_ZahlKond);
//	CtrlGrid.CreateChoiceButton (m_ZahlKondChoice, IDC_ZAHL_KONDCHOICE, this);
//	CCtrlInfo *c_ZahlKondChoice = new CCtrlInfo (&m_ZahlKondChoice, 1, 0, 1, 1);
//	ZahlKondGrid.Add (c_ZahlKondChoice);
//	CCtrlInfo *c_EnterZahlKond = new CCtrlInfo (&m_EnterZahlKond, 4, 0, 3, 1);
//	c_EnterZahlKond->SetCellPos (0, -10);
//	ZahlKondGrid.Add (c_EnterZahlKond);
	CCtrlInfo *c_Ziel1 = new CCtrlInfo (&m_Ziel1, 0, 1, 1, 1);
	ZahlKondGrid.Add (c_Ziel1);
	CCtrlInfo *c_LZiel1 = new CCtrlInfo (&m_LZiel1, 1, 1, 1, 1);
	ZahlKondGrid.Add (c_LZiel1);
	CCtrlInfo *c_Skto1 = new CCtrlInfo (&m_Skto1, 2, 1, 1, 1);
	ZahlKondGrid.Add (c_Skto1);
	CCtrlInfo *c_LSkto1 = new CCtrlInfo (&m_LSkto1, 3, 1, 1, 1);
	ZahlKondGrid.Add (c_LSkto1);

	CCtrlInfo *c_Ziel2 = new CCtrlInfo (&m_Ziel2, 0, 2, 1, 1);
	ZahlKondGrid.Add (c_Ziel2);
	CCtrlInfo *c_LZiel2 = new CCtrlInfo (&m_LZiel2, 1, 2, 1, 1);
	ZahlKondGrid.Add (c_LZiel2);
	CCtrlInfo *c_Skto2 = new CCtrlInfo (&m_Skto2, 2, 2, 1, 1);
	ZahlKondGrid.Add (c_Skto2);
	CCtrlInfo *c_LSkto2 = new CCtrlInfo (&m_LSkto2, 3, 2, 1, 1);
	ZahlKondGrid.Add (c_LSkto2);

	CCtrlInfo *c_Ziel3 = new CCtrlInfo (&m_Ziel3, 0, 3, 1, 1);
	ZahlKondGrid.Add (c_Ziel3);
	CCtrlInfo *c_LZiel3 = new CCtrlInfo (&m_LZiel3, 1, 3, 1, 1);
	ZahlKondGrid.Add (c_LZiel3);
	CCtrlInfo *c_Skto3 = new CCtrlInfo (&m_Skto3, 2, 3, 1, 1);
	ZahlKondGrid.Add (c_Skto3);
	CCtrlInfo *c_LSkto3 = new CCtrlInfo (&m_LSkto3, 3, 3, 1, 1);
	ZahlKondGrid.Add (c_LSkto3);

	CCtrlInfo *c_Caption = new CCtrlInfo (&m_Caption, 0, 0, DOCKRIGHT, 1);
    CtrlGrid.Add (c_Caption);

	CCtrlInfo *c_ZahlKondGroup = new CCtrlInfo (&m_ZahlKondGroup, 1, 2, DOCKRIGHT, DOCKBOTTOM);
	c_ZahlKondGroup->SetBottomControl (this, &m_OkEx, 5, CCtrlInfo::Top);
	c_ZahlKondGroup->rightspace = 5;
    CtrlGrid.Add (c_ZahlKondGroup);

	CCtrlInfo *c_LZahlKond = new CCtrlInfo (&m_LZahlKond, 2, 3, 1, 1);
	CtrlGrid.Add (c_LZahlKond);

	CCtrlInfo *c_ZahlKondGrid = new CCtrlInfo (&ZahlKondGrid, 3, 3, 3, 3);
	CtrlGrid.Add (c_ZahlKondGrid);

	CCtrlInfo *c_LTxt = new CCtrlInfo (&m_LTxt, 2, 7, 1, 1);
	c_LTxt->SetCellPos (0, -10);
	CtrlGrid.Add (c_LTxt);
	CCtrlInfo *c_Txt = new CCtrlInfo (&m_Txt, 3, 7, 1, 1);
	c_Txt->SetCellPos (0, -10);
	CtrlGrid.Add (c_Txt);

	ControlGrid.Create (this, 1, 2);
    ControlGrid.SetBorder (0, 0);
    ControlGrid.SetGridSpace (5, 2);
	CCtrlInfo *c_OkEx = new CCtrlInfo (&m_OkEx, 0, 0, 1, 1);
	ControlGrid.Add (c_OkEx);
	CCtrlInfo *c_CancelEx = new CCtrlInfo (&m_CancelEx, 1, 0, 1, 1);
	ControlGrid.Add (c_CancelEx);

	CCtrlInfo *c_ControlGrid = new CCtrlInfo (&ControlGrid, 1, 8, 4, 1);
	CtrlGrid.Add (c_ControlGrid);

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	StaticFont.CreateFontIndirect (&l);
	l.lfHeight = 25;
	CaptionFont.CreateFontIndirect (&l);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Caption.SetFont (&CaptionFont);

	CORE->ReadZahlKond ();
	CtrlGrid.Display ();
	Form.Show ();

	return FALSE;
}


HBRUSH CZahlKondDialog::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	CRect rect;
	if (nCtlColor == CTLCOLOR_DLG && m_DlgColor != NULL)
	{
		    if (m_DlgBrush == NULL)
			{
//				m_DlgBrush = CreateSolidBrush (m_DlgColor);
                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC)
	{
		    if (m_DlgBrush == NULL)
			{
				m_DlgBrush = CreateSolidBrush (m_DlgColor);
			}
/*
			if (*pWnd == m_Message)
			{
				if (!Error)
				{
					pDC->SetTextColor (MessageColor);
				}
				else
				{
					pDC->SetTextColor (ErrorColor);
				}
			}
			else
			{
				pDC->SetTextColor (RGB (92, 92, 92));
			}
*/
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

/*
HBRUSH CZahlKondDialog::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}
*/

BOOL CZahlKondDialog::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnDown ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				break;
			}
			else if (pMsg->wParam == VK_F12)
			{
				OnOK ();
				break;
			}
	}
    return CDialog::PreTranslateMessage(pMsg);
}

BOOL CZahlKondDialog::OnReturn ()
{
	CWnd *Control = GetFocus ();
	if (Control == &m_OkEx ||
        Control == &m_CancelEx ||
		Control == &m_OK ||
		Control == &m_Cancel)
	{
		return FALSE;
	}

	return OnDown ();

}

BOOL CZahlKondDialog::OnDown ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CZahlKondDialog::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

void CZahlKondDialog::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	CORE->WriteZahlKond ();
	OnOK();
}

void CZahlKondDialog::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnCancel();
}
