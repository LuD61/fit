#include "StdAfx.h"
#include "LiefRGrCore.h"
#include "DbTime.h"
#include "StrFuncs.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CLiefRGrCore *CLiefRGrCore::Instance = NULL; 

CLiefRGrCore::CLiefRGrCore(void)
{
	CLIEF_R_GR.sqlin ((short *) &TLIEF_R_GR.mdn, SQLSHORT, 0);
	CLIEF_R_GR.sqlin ((short *) &TLIEF_R_GR.fil, SQLSHORT, 0);
	CLIEF_R_GR.sqlin ((char *)  TLIEF_R_GR.lief, SQLCHAR, sizeof (TLIEF_R_GR.lief));
	CLIEF_R_GR.sqlout ((long *) &TLIEF_R_GR.lfd, SQLLONG, 0);
	m_Cursor = CLIEF_R_GR.sqlcursor (_T("select lfd from lief_r_gr ")
									  _T("where mdn = ? ")
									  _T("and fil = ? ")
									  _T("and lief = ? ")
									  _T("order by lfd"));
	CLIEF_R_GR.sqlin ((short *) &TLIEF_R_GR.mdn, SQLSHORT, 0);
	CLIEF_R_GR.sqlin ((short *) &TLIEF_R_GR.fil, SQLSHORT, 0);
	CLIEF_R_GR.sqlin ((char *)  TLIEF_R_GR.lief, SQLCHAR, sizeof (TLIEF_R_GR.lief));
	m_DeleteCursor = CLIEF_R_GR.sqlcursor (_T("delete from lief_r_gr ")
											_T("where mdn = ? ")
											_T("and fil = ? ")
											_T("and lief = ? "));
	m_ListRow = NULL;
	m_ProzSum = 0.0;
}

CLiefRGrCore::~CLiefRGrCore(void)
{
	CLIEF_R_GR.sqlclose (m_Cursor);
	m_LiefRGrTab.DestroyElements ();
	m_LiefRGrTab.Destroy ();
}

CLiefRGrCore *CLiefRGrCore::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CLiefRGrCore ();
	}
	return Instance;
}

void CLiefRGrCore::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

BOOL CLiefRGrCore::Read ()
{
	BOOL ret = FALSE;
	int dsqlstatus;
	m_LiefRGrTab.DestroyElements ();

	if (m_ListRow != NULL)
	{
		dsqlstatus = CLIEF_R_GR.sqlopen (m_Cursor);
		if (dsqlstatus == 0)
		{
			while (CLIEF_R_GR.sqlfetch (m_Cursor) == 0)
			{
				ret = TRUE;
				DATABASE->ReadLiefRGr ();
				m_ListRow->Add ();
				LIEF_R_GR *tabrow = new LIEF_R_GR;
				memcpy (tabrow, &TLIEF_R_GR, sizeof (LIEF_R_GR)); 
				m_LiefRGrTab.Add (tabrow);
			}
		}
		m_ListRow->Update ();
	}
	return ret;
}

BOOL CLiefRGrCore::Write ()
{
	BOOL ret = TRUE;
	LIEF_R_GR **it;
	LIEF_R_GR *tabrow;

	m_LiefRGrTab.Start ();
	while ((it = m_LiefRGrTab.GetNext ()) != NULL)
	{
		tabrow = *it;
		if (tabrow != NULL)
		{
			if (tabrow->rab_proz > 999.99 ||
				tabrow->rab_proz < -999.99)
			{
				ret = FALSE;
				break;
			}
		}
	}

	if (ret)
	{
		CLIEF_R_GR.sqlexecute (m_DeleteCursor);
		long lfd = 1;
		m_LiefRGrTab.Start ();
		while ((it = m_LiefRGrTab.GetNext ()) != NULL)
		{
			tabrow = *it;
			if (tabrow != NULL && tabrow->rab_proz != 0.0)
			{
				memcpy (&TLIEF_R_GR, &lief_r_gr_null, sizeof (LIEF_R_GR));
				TLIEF_R_GR.mdn = TLIEF.mdn;
				TLIEF_R_GR.fil = TLIEF.fil;
				_tcscpy (TLIEF_R_GR.lief, TLIEF.lief);
				TLIEF_R_GR.lfd = lfd;
				TLIEF_R_GR.rab_proz = tabrow->rab_proz;
				TLIEF_R_GR.ums_grenz =  tabrow->ums_grenz;
				memcpy (tabrow, &TLIEF_R_GR, sizeof (LIEF_R_GR));
				TLIEF_R_GR.lief_s = _tstol (TLIEF_R_GR.lief);
				CLIEF_R_GR.dbupdate ();
				lfd ++;
			}
		}
	}
	return ret;
}


long CLiefRGrCore::NextLfd ()
{
	long lfd = 0l;
	int rows = m_LiefRGrTab.anz;
	if (rows > 0)
	{
		LIEF_R_GR *row = *m_LiefRGrTab.Get (rows - 1);
		if (row != NULL)
		{
			lfd = row->lfd;
		}
	}
	return ++lfd;
}

void CLiefRGrCore::UpdateRow (int row)
{
	int rows = m_LiefRGrTab.anz;
	if (row >= rows)
	{
		LIEF_R_GR *tabrow = new LIEF_R_GR;
		memcpy (tabrow, &TLIEF_R_GR, sizeof (LIEF_R_GR)); 
		m_LiefRGrTab.Add (tabrow);
	}
	else
	{
	    LIEF_R_GR *tabrow = *m_LiefRGrTab.Get (row);
		memcpy (tabrow, &TLIEF_R_GR, sizeof (LIEF_R_GR)); 
	}
	m_ListRow->Update ();
}

void CLiefRGrCore::DeleteRow (int row)
{
    if (m_LiefRGrTab.Get (row) != NULL)
	{
		LIEF_R_GR *tabrow = *m_LiefRGrTab.Get (row);
		m_LiefRGrTab.Drop (row);
		delete tabrow;
		m_ListRow->Update ();
	}
}
