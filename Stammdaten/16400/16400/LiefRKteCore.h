#pragma once
#include "DatabaseCore.h"
#include "ListRow.h"
#include "DataCollection.h"

class CLiefRKteCore
{
private:
	static CLiefRKteCore *Instance; 
	int m_Cursor;
	int m_DeleteCursor;
	int m_Rows;
	CListRow *m_ListRow;
	CDataCollection<LIEF_R_KTE *> m_LiefRKteTab;

public:

	double m_ProzSum;
	void SetListRow (CListRow *ListRow)
	{
		m_ListRow = ListRow;
	}

	void SetRow (int rows)
	{
		m_Rows = rows;
	}

	int GetRow ()
	{
		return m_Rows;
	}

	CLiefRKteCore(void);
	~CLiefRKteCore(void);

	static CLiefRKteCore *GetInstance ();
	static void DestroyInstance ();
	BOOL Read ();
	BOOL Write ();
	long NextLfd ();
	void UpdateRow (int row);
	void DeleteRow (int row);
	void CalcProzSum ();
};

#define LIEF_R_KTE_CORE CLiefRKteCore::GetInstance ()