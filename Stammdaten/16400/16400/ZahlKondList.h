#ifndef _ZAHL_KOND_LIST_DEF
#define _ZAHL_KOND_LIST_DEF
#pragma once

class CZahlKondList
{
public:
	
	double         skto1;
        double         skto2;
        double         skto3;
        CString        txt;
        short          zahl_kond;
        short          ziel1;
        short          ziel2;
        short          ziel3;
	CZahlKondList(void);
	~CZahlKondList(void);
};
#endif
