#include "stdafx.h"
#include "lief.h"

struct LIEF lief, lief_null, lief_def;

void LIEF_CLASS::prepare (void)
{
            TCHAR *sqltext;

 

           	sqlin ((short *)   &lief.mdn,  SQLSHORT, 0);
		sqlin ((short *)   &lief.fil,  SQLSHORT, 0);
		sqlin ((char *)    lief.lief,  SQLCHAR, sizeof (lief.lief));
    sqlout ((TCHAR *) lief.abr,SQLCHAR,2);
    sqlout ((long *) &lief.adr,SQLLONG,0);
    sqlout ((TCHAR *) lief.bank_nam,SQLCHAR,37);
    sqlout ((long *) &lief.bbn,SQLLONG,0);
    sqlout ((short *) &lief.best_sort,SQLSHORT,0);
    sqlout ((TCHAR *) lief.best_trans_kz,SQLCHAR,2);
    sqlout ((long *) &lief.blz,SQLLONG,0);
    sqlout ((TCHAR *) lief.bonus_kz,SQLCHAR,2);
    sqlout ((short *) &lief.fil,SQLSHORT,0);
    sqlout ((TCHAR *) lief.fracht_kz,SQLCHAR,2);
    sqlout ((TCHAR *) lief.ink,SQLCHAR,17);
    sqlout ((long *) &lief.kreditor,SQLLONG,0);
    sqlout ((TCHAR *) lief.kto,SQLCHAR,17);
    sqlout ((DATE_STRUCT *) &lief.letzt_lief,SQLDATE,0);
    sqlout ((TCHAR *) lief.lief,SQLCHAR,17);
    sqlout ((TCHAR *) lief.lief_kun,SQLCHAR,17);
    sqlout ((TCHAR *) lief.lief_rht,SQLCHAR,2);
    sqlout ((long *) &lief.lief_s,SQLLONG,0);
    sqlout ((short *) &lief.lief_typ,SQLSHORT,0);
    sqlout ((short *) &lief.lief_zeit,SQLSHORT,0);
    sqlout ((double *) &lief.lief_zusch,SQLDOUBLE,0);
    sqlout ((long *) &lief.lst_nr,SQLLONG,0);
    sqlout ((short *) &lief.mahn_stu,SQLSHORT,0);
    sqlout ((short *) &lief.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) lief.me_kz,SQLCHAR,2);
    sqlout ((TCHAR *) lief.min_me_kz,SQLCHAR,2);
    sqlout ((double *) &lief.min_zusch,SQLDOUBLE,0);
    sqlout ((double *) &lief.min_zusch_proz,SQLDOUBLE,0);
    sqlout ((TCHAR *) lief.nach_lief,SQLCHAR,2);
    sqlout ((TCHAR *) lief.ordr_kz,SQLCHAR,2);
    sqlout ((TCHAR *) lief.rab_abl,SQLCHAR,2);
    sqlout ((TCHAR *) lief.rab_kz,SQLCHAR,2);
    sqlout ((TCHAR *) lief.rech_kz,SQLCHAR,2);
    sqlout ((double *) &lief.rech_toler,SQLDOUBLE,0);
    sqlout ((short *) &lief.sdr_nr,SQLSHORT,0);
    sqlout ((long *) &lief.son_kond,SQLLONG,0);
    sqlout ((short *) &lief.sprache,SQLSHORT,0);
    sqlout ((TCHAR *) lief.steuer_kz,SQLCHAR,2);
    sqlout ((TCHAR *) lief.unt_lief_kz,SQLCHAR,2);
    sqlout ((TCHAR *) lief.vieh_bas,SQLCHAR,2);
    sqlout ((TCHAR *) lief.we_erf_kz,SQLCHAR,2);
    sqlout ((TCHAR *) lief.we_kontr,SQLCHAR,2);
    sqlout ((TCHAR *) lief.we_pr_kz,SQLCHAR,2);
    sqlout ((double *) &lief.we_toler,SQLDOUBLE,0);
    sqlout ((short *) &lief.zahl_kond,SQLSHORT,0);
    sqlout ((TCHAR *) lief.zahlw,SQLCHAR,2);
    sqlout ((short *) &lief.hdklpargr,SQLSHORT,0);
    sqlout ((long *) &lief.vorkgr,SQLLONG,0);
    sqlout ((short *) &lief.delstatus,SQLSHORT,0);
    sqlout ((short *) &lief.mwst,SQLSHORT,0);
    sqlout ((short *) &lief.liefart,SQLSHORT,0);
    sqlout ((TCHAR *) lief.modif,SQLCHAR,2);
    sqlout ((TCHAR *) lief.ust_id,SQLCHAR,17);
    sqlout ((TCHAR *) lief.eg_betriebsnr,SQLCHAR,21);
    sqlout ((short *) &lief.eg_kz,SQLSHORT,0);
    sqlout ((short *) &lief.waehrung,SQLSHORT,0);
    sqlout ((TCHAR *) lief.iln,SQLCHAR,17);
    sqlout ((short *) &lief.zertifikat1,SQLSHORT,0);
    sqlout ((short *) &lief.zertifikat2,SQLSHORT,0);
    sqlout ((TCHAR *) lief.iban1,SQLCHAR,5);
    sqlout ((TCHAR *) lief.iban2,SQLCHAR,5);
    sqlout ((TCHAR *) lief.iban3,SQLCHAR,5);
    sqlout ((TCHAR *) lief.iban4,SQLCHAR,5);
    sqlout ((TCHAR *) lief.iban5,SQLCHAR,5);
    sqlout ((TCHAR *) lief.iban6,SQLCHAR,5);
    sqlout ((TCHAR *) lief.iban7,SQLCHAR,4);
    sqlout ((TCHAR *) lief.esnum,SQLCHAR,11);
    sqlout ((TCHAR *) lief.eznum,SQLCHAR,11);
    sqlout ((short *) &lief.geburt,SQLSHORT,0);
    sqlout ((short *) &lief.mast,SQLSHORT,0);
    sqlout ((short *) &lief.schlachtung,SQLSHORT,0);
    sqlout ((short *) &lief.zerlegung,SQLSHORT,0);
    sqlout ((TCHAR *) lief.steuer_nr,SQLCHAR,37);
    sqlout ((long *) &lief.kst,SQLLONG,0);
    sqlout ((TCHAR *) lief.iban,SQLCHAR,25);
    sqlout ((TCHAR *) lief.swift,SQLCHAR,25);
    sqlout ((short *) &lief.tage_bis,SQLSHORT,0);
            	cursor = sqlcursor (_T("select lief.abr,  lief.adr,  ")
_T("lief.bank_nam,  lief.bbn,  lief.best_sort,  lief.best_trans_kz,  ")
_T("lief.blz,  lief.bonus_kz,  lief.fil,  lief.fracht_kz,  lief.ink,  ")
_T("lief.kreditor,  lief.kto,  lief.letzt_lief,  lief.lief,  lief.lief_kun,  ")
_T("lief.lief_rht,  lief.lief_s,  lief.lief_typ,  lief.lief_zeit,  ")
_T("lief.lief_zusch,  lief.lst_nr,  lief.mahn_stu,  lief.mdn,  lief.me_kz,  ")
_T("lief.min_me_kz,  lief.min_zusch,  lief.min_zusch_proz,  ")
_T("lief.nach_lief,  lief.ordr_kz,  lief.rab_abl,  lief.rab_kz,  ")
_T("lief.rech_kz,  lief.rech_toler,  lief.sdr_nr,  lief.son_kond,  ")
_T("lief.sprache,  lief.steuer_kz,  lief.unt_lief_kz,  lief.vieh_bas,  ")
_T("lief.we_erf_kz,  lief.we_kontr,  lief.we_pr_kz,  lief.we_toler,  ")
_T("lief.zahl_kond,  lief.zahlw,  lief.hdklpargr,  lief.vorkgr,  ")
_T("lief.delstatus,  lief.mwst,  lief.liefart,  lief.modif,  lief.ust_id,  ")
_T("lief.eg_betriebsnr,  lief.eg_kz,  lief.waehrung,  lief.iln,  ")
_T("lief.zertifikat1,  lief.zertifikat2,  lief.iban1,  lief.iban2,  ")
_T("lief.iban3,  lief.iban4,  lief.iban5,  lief.iban6,  lief.iban7,  lief.esnum,  ")
_T("lief.eznum,  lief.geburt,  lief.mast,  lief.schlachtung,  lief.zerlegung,  ")
_T("lief.steuer_nr,  lief.kst,  lief.iban,  lief.swift,  lief.tage_bis from lief ")

#line 16 "lief.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and lief = ?"));

            	sqlin ((short *)   &lief.mdn,  SQLSHORT, 0);
		sqlin ((short *)   &lief.fil,  SQLSHORT, 0);
		sqlin ((char *)    lief.lief,  SQLCHAR, sizeof (lief.lief));
            	test_upd_cursor = sqlcursor (_T("select lief from lief ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and lief = ? for update"));

            	sqlin ((short *)   &lief.mdn,  SQLSHORT, 0);
		sqlin ((short *)   &lief.fil,  SQLSHORT, 0);
		sqlin ((char *)    lief.lief,  SQLCHAR, sizeof (lief.lief));
	        test_lock_cursor = sqlcursor (_T("update lief ")
                                  _T("set delstatus = 0 where mdn = ? and fil = ? and lief = ? and delstatus = 0"));


    sqlin ((TCHAR *) lief.abr,SQLCHAR,2);
    sqlin ((long *) &lief.adr,SQLLONG,0);
    sqlin ((TCHAR *) lief.bank_nam,SQLCHAR,37);
    sqlin ((long *) &lief.bbn,SQLLONG,0);
    sqlin ((short *) &lief.best_sort,SQLSHORT,0);
    sqlin ((TCHAR *) lief.best_trans_kz,SQLCHAR,2);
    sqlin ((long *) &lief.blz,SQLLONG,0);
    sqlin ((TCHAR *) lief.bonus_kz,SQLCHAR,2);
    sqlin ((short *) &lief.fil,SQLSHORT,0);
    sqlin ((TCHAR *) lief.fracht_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.ink,SQLCHAR,17);
    sqlin ((long *) &lief.kreditor,SQLLONG,0);
    sqlin ((TCHAR *) lief.kto,SQLCHAR,17);
    sqlin ((DATE_STRUCT *) &lief.letzt_lief,SQLDATE,0);
    sqlin ((TCHAR *) lief.lief,SQLCHAR,17);
    sqlin ((TCHAR *) lief.lief_kun,SQLCHAR,17);
    sqlin ((TCHAR *) lief.lief_rht,SQLCHAR,2);
    sqlin ((long *) &lief.lief_s,SQLLONG,0);
    sqlin ((short *) &lief.lief_typ,SQLSHORT,0);
    sqlin ((short *) &lief.lief_zeit,SQLSHORT,0);
    sqlin ((double *) &lief.lief_zusch,SQLDOUBLE,0);
    sqlin ((long *) &lief.lst_nr,SQLLONG,0);
    sqlin ((short *) &lief.mahn_stu,SQLSHORT,0);
    sqlin ((short *) &lief.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) lief.me_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.min_me_kz,SQLCHAR,2);
    sqlin ((double *) &lief.min_zusch,SQLDOUBLE,0);
    sqlin ((double *) &lief.min_zusch_proz,SQLDOUBLE,0);
    sqlin ((TCHAR *) lief.nach_lief,SQLCHAR,2);
    sqlin ((TCHAR *) lief.ordr_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.rab_abl,SQLCHAR,2);
    sqlin ((TCHAR *) lief.rab_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.rech_kz,SQLCHAR,2);
    sqlin ((double *) &lief.rech_toler,SQLDOUBLE,0);
    sqlin ((short *) &lief.sdr_nr,SQLSHORT,0);
    sqlin ((long *) &lief.son_kond,SQLLONG,0);
    sqlin ((short *) &lief.sprache,SQLSHORT,0);
    sqlin ((TCHAR *) lief.steuer_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.unt_lief_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.vieh_bas,SQLCHAR,2);
    sqlin ((TCHAR *) lief.we_erf_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.we_kontr,SQLCHAR,2);
    sqlin ((TCHAR *) lief.we_pr_kz,SQLCHAR,2);
    sqlin ((double *) &lief.we_toler,SQLDOUBLE,0);
    sqlin ((short *) &lief.zahl_kond,SQLSHORT,0);
    sqlin ((TCHAR *) lief.zahlw,SQLCHAR,2);
    sqlin ((short *) &lief.hdklpargr,SQLSHORT,0);
    sqlin ((long *) &lief.vorkgr,SQLLONG,0);
    sqlin ((short *) &lief.delstatus,SQLSHORT,0);
    sqlin ((short *) &lief.mwst,SQLSHORT,0);
    sqlin ((short *) &lief.liefart,SQLSHORT,0);
    sqlin ((TCHAR *) lief.modif,SQLCHAR,2);
    sqlin ((TCHAR *) lief.ust_id,SQLCHAR,17);
    sqlin ((TCHAR *) lief.eg_betriebsnr,SQLCHAR,21);
    sqlin ((short *) &lief.eg_kz,SQLSHORT,0);
    sqlin ((short *) &lief.waehrung,SQLSHORT,0);
    sqlin ((TCHAR *) lief.iln,SQLCHAR,17);
    sqlin ((short *) &lief.zertifikat1,SQLSHORT,0);
    sqlin ((short *) &lief.zertifikat2,SQLSHORT,0);
    sqlin ((TCHAR *) lief.iban1,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban2,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban3,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban4,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban5,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban6,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban7,SQLCHAR,4);
    sqlin ((TCHAR *) lief.esnum,SQLCHAR,11);
    sqlin ((TCHAR *) lief.eznum,SQLCHAR,11);
    sqlin ((short *) &lief.geburt,SQLSHORT,0);
    sqlin ((short *) &lief.mast,SQLSHORT,0);
    sqlin ((short *) &lief.schlachtung,SQLSHORT,0);
    sqlin ((short *) &lief.zerlegung,SQLSHORT,0);
    sqlin ((TCHAR *) lief.steuer_nr,SQLCHAR,37);
    sqlin ((long *) &lief.kst,SQLLONG,0);
    sqlin ((TCHAR *) lief.iban,SQLCHAR,25);
    sqlin ((TCHAR *) lief.swift,SQLCHAR,25);
    sqlin ((short *) &lief.tage_bis,SQLSHORT,0);
            	sqltext = _T("update lief set lief.abr = ?,  ")
_T("lief.adr = ?,  lief.bank_nam = ?,  lief.bbn = ?,  lief.best_sort = ?,  ")
_T("lief.best_trans_kz = ?,  lief.blz = ?,  lief.bonus_kz = ?,  ")
_T("lief.fil = ?,  lief.fracht_kz = ?,  lief.ink = ?,  lief.kreditor = ?,  ")
_T("lief.kto = ?,  lief.letzt_lief = ?,  lief.lief = ?,  ")
_T("lief.lief_kun = ?,  lief.lief_rht = ?,  lief.lief_s = ?,  ")
_T("lief.lief_typ = ?,  lief.lief_zeit = ?,  lief.lief_zusch = ?,  ")
_T("lief.lst_nr = ?,  lief.mahn_stu = ?,  lief.mdn = ?,  lief.me_kz = ?,  ")
_T("lief.min_me_kz = ?,  lief.min_zusch = ?,  lief.min_zusch_proz = ?,  ")
_T("lief.nach_lief = ?,  lief.ordr_kz = ?,  lief.rab_abl = ?,  ")
_T("lief.rab_kz = ?,  lief.rech_kz = ?,  lief.rech_toler = ?,  ")
_T("lief.sdr_nr = ?,  lief.son_kond = ?,  lief.sprache = ?,  ")
_T("lief.steuer_kz = ?,  lief.unt_lief_kz = ?,  lief.vieh_bas = ?,  ")
_T("lief.we_erf_kz = ?,  lief.we_kontr = ?,  lief.we_pr_kz = ?,  ")
_T("lief.we_toler = ?,  lief.zahl_kond = ?,  lief.zahlw = ?,  ")
_T("lief.hdklpargr = ?,  lief.vorkgr = ?,  lief.delstatus = ?,  ")
_T("lief.mwst = ?,  lief.liefart = ?,  lief.modif = ?,  lief.ust_id = ?,  ")
_T("lief.eg_betriebsnr = ?,  lief.eg_kz = ?,  lief.waehrung = ?,  ")
_T("lief.iln = ?,  lief.zertifikat1 = ?,  lief.zertifikat2 = ?,  ")
_T("lief.iban1 = ?,  lief.iban2 = ?,  lief.iban3 = ?,  lief.iban4 = ?,  ")
_T("lief.iban5 = ?,  lief.iban6 = ?,  lief.iban7 = ?,  lief.esnum = ?,  ")
_T("lief.eznum = ?,  lief.geburt = ?,  lief.mast = ?,  ")
_T("lief.schlachtung = ?,  lief.zerlegung = ?,  lief.steuer_nr = ?,  ")
_T("lief.kst = ?,  lief.iban = ?,  lief.swift = ?,  lief.tage_bis = ? ")

#line 36 "lief.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and lief = ?");
		
		sqlin ((short *)   &lief.mdn,  SQLSHORT, 0);
		sqlin ((short *)   &lief.fil,  SQLSHORT, 0);
		sqlin ((char *)    lief.lief,  SQLCHAR, sizeof (lief.lief));
           	upd_cursor = sqlcursor (sqltext);


         	sqlin ((short *)   &lief.mdn,  SQLSHORT, 0);
		sqlin ((short *)   &lief.fil,  SQLSHORT, 0);
		sqlin ((char *)    lief.lief,  SQLCHAR, sizeof (lief.lief));
		del_cursor = sqlcursor (_T("delete from lief ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and lief = ?"));
            	

    sqlin ((TCHAR *) lief.abr,SQLCHAR,2);
    sqlin ((long *) &lief.adr,SQLLONG,0);
    sqlin ((TCHAR *) lief.bank_nam,SQLCHAR,37);
    sqlin ((long *) &lief.bbn,SQLLONG,0);
    sqlin ((short *) &lief.best_sort,SQLSHORT,0);
    sqlin ((TCHAR *) lief.best_trans_kz,SQLCHAR,2);
    sqlin ((long *) &lief.blz,SQLLONG,0);
    sqlin ((TCHAR *) lief.bonus_kz,SQLCHAR,2);
    sqlin ((short *) &lief.fil,SQLSHORT,0);
    sqlin ((TCHAR *) lief.fracht_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.ink,SQLCHAR,17);
    sqlin ((long *) &lief.kreditor,SQLLONG,0);
    sqlin ((TCHAR *) lief.kto,SQLCHAR,17);
    sqlin ((DATE_STRUCT *) &lief.letzt_lief,SQLDATE,0);
    sqlin ((TCHAR *) lief.lief,SQLCHAR,17);
    sqlin ((TCHAR *) lief.lief_kun,SQLCHAR,17);
    sqlin ((TCHAR *) lief.lief_rht,SQLCHAR,2);
    sqlin ((long *) &lief.lief_s,SQLLONG,0);
    sqlin ((short *) &lief.lief_typ,SQLSHORT,0);
    sqlin ((short *) &lief.lief_zeit,SQLSHORT,0);
    sqlin ((double *) &lief.lief_zusch,SQLDOUBLE,0);
    sqlin ((long *) &lief.lst_nr,SQLLONG,0);
    sqlin ((short *) &lief.mahn_stu,SQLSHORT,0);
    sqlin ((short *) &lief.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) lief.me_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.min_me_kz,SQLCHAR,2);
    sqlin ((double *) &lief.min_zusch,SQLDOUBLE,0);
    sqlin ((double *) &lief.min_zusch_proz,SQLDOUBLE,0);
    sqlin ((TCHAR *) lief.nach_lief,SQLCHAR,2);
    sqlin ((TCHAR *) lief.ordr_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.rab_abl,SQLCHAR,2);
    sqlin ((TCHAR *) lief.rab_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.rech_kz,SQLCHAR,2);
    sqlin ((double *) &lief.rech_toler,SQLDOUBLE,0);
    sqlin ((short *) &lief.sdr_nr,SQLSHORT,0);
    sqlin ((long *) &lief.son_kond,SQLLONG,0);
    sqlin ((short *) &lief.sprache,SQLSHORT,0);
    sqlin ((TCHAR *) lief.steuer_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.unt_lief_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.vieh_bas,SQLCHAR,2);
    sqlin ((TCHAR *) lief.we_erf_kz,SQLCHAR,2);
    sqlin ((TCHAR *) lief.we_kontr,SQLCHAR,2);
    sqlin ((TCHAR *) lief.we_pr_kz,SQLCHAR,2);
    sqlin ((double *) &lief.we_toler,SQLDOUBLE,0);
    sqlin ((short *) &lief.zahl_kond,SQLSHORT,0);
    sqlin ((TCHAR *) lief.zahlw,SQLCHAR,2);
    sqlin ((short *) &lief.hdklpargr,SQLSHORT,0);
    sqlin ((long *) &lief.vorkgr,SQLLONG,0);
    sqlin ((short *) &lief.delstatus,SQLSHORT,0);
    sqlin ((short *) &lief.mwst,SQLSHORT,0);
    sqlin ((short *) &lief.liefart,SQLSHORT,0);
    sqlin ((TCHAR *) lief.modif,SQLCHAR,2);
    sqlin ((TCHAR *) lief.ust_id,SQLCHAR,17);
    sqlin ((TCHAR *) lief.eg_betriebsnr,SQLCHAR,21);
    sqlin ((short *) &lief.eg_kz,SQLSHORT,0);
    sqlin ((short *) &lief.waehrung,SQLSHORT,0);
    sqlin ((TCHAR *) lief.iln,SQLCHAR,17);
    sqlin ((short *) &lief.zertifikat1,SQLSHORT,0);
    sqlin ((short *) &lief.zertifikat2,SQLSHORT,0);
    sqlin ((TCHAR *) lief.iban1,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban2,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban3,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban4,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban5,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban6,SQLCHAR,5);
    sqlin ((TCHAR *) lief.iban7,SQLCHAR,4);
    sqlin ((TCHAR *) lief.esnum,SQLCHAR,11);
    sqlin ((TCHAR *) lief.eznum,SQLCHAR,11);
    sqlin ((short *) &lief.geburt,SQLSHORT,0);
    sqlin ((short *) &lief.mast,SQLSHORT,0);
    sqlin ((short *) &lief.schlachtung,SQLSHORT,0);
    sqlin ((short *) &lief.zerlegung,SQLSHORT,0);
    sqlin ((TCHAR *) lief.steuer_nr,SQLCHAR,37);
    sqlin ((long *) &lief.kst,SQLLONG,0);
    sqlin ((TCHAR *) lief.iban,SQLCHAR,25);
    sqlin ((TCHAR *) lief.swift,SQLCHAR,25);
    sqlin ((short *) &lief.tage_bis,SQLSHORT,0);
		ins_cursor = sqlcursor (_T("insert into lief (abr,  adr,  ")
_T("bank_nam,  bbn,  best_sort,  best_trans_kz,  blz,  bonus_kz,  fil,  fracht_kz,  ink,  ")
_T("kreditor,  kto,  letzt_lief,  lief,  lief_kun,  lief_rht,  lief_s,  lief_typ,  ")
_T("lief_zeit,  lief_zusch,  lst_nr,  mahn_stu,  mdn,  me_kz,  min_me_kz,  min_zusch,  ")
_T("min_zusch_proz,  nach_lief,  ordr_kz,  rab_abl,  rab_kz,  rech_kz,  rech_toler,  ")
_T("sdr_nr,  son_kond,  sprache,  steuer_kz,  unt_lief_kz,  vieh_bas,  we_erf_kz,  ")
_T("we_kontr,  we_pr_kz,  we_toler,  zahl_kond,  zahlw,  hdklpargr,  vorkgr,  ")
_T("delstatus,  mwst,  liefart,  modif,  ust_id,  eg_betriebsnr,  eg_kz,  waehrung,  iln,  ")
_T("zertifikat1,  zertifikat2,  iban1,  iban2,  iban3,  iban4,  iban5,  iban6,  iban7,  ")
_T("esnum,  eznum,  geburt,  mast,  schlachtung,  zerlegung,  steuer_nr,  kst,  iban,  ")
_T("swift,  tage_bis) ")

#line 56 "lief.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?)"));

#line 58 "lief.rpp"
}
