// 16400View.h : Schnittstelle der Klasse CMy16400View
//


#pragma once


class CMy16400View : public CFormView
{
protected: // Nur aus Serialisierung erstellen
	CMy16400View();
	DECLARE_DYNCREATE(CMy16400View)

public:
	enum{ IDD = IDD_16400_FORM };

// Attribute
public:
	CMy16400Doc* GetDocument() const;

// Vorgänge
public:

// Überschreibungen
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung

// Implementierung
public:
	virtual ~CMy16400View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // Debugversion in 16400View.cpp
inline CMy16400Doc* CMy16400View::GetDocument() const
   { return reinterpret_cast<CMy16400Doc*>(m_pDocument); }
#endif

