#include "StdAfx.h"
#include "LiefRGrListCtrl.h"
#include "LiefRGrCore.h"
#include "DatabaseCore.h"
#include "StrFuncs.h"
#include "resource.h"

CLiefRGrListCtrl::CLiefRGrListCtrl(void)
{
	ListRows.Init ();
}

CLiefRGrListCtrl::~CLiefRGrListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CLiefRGrListCtrl, CEditListCtrl)
//	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CLiefRGrListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (1, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (1, 0);
	}
}

void CLiefRGrListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	int count = GetHeaderCtrl ()->GetItemCount ();
	while (IsDisplayOnly (col) && col < count - 1) col ++;
	CEditListCtrl::StartEnter (col, row);
	if (col == PosUmsGrenz)
	{
		ListEdit.SetLimitText (9);
	}
	else if (col == PosRab)
	{
		ListEdit.SetLimitText (8);
	}
}

void CLiefRGrListCtrl::StopEnter ()
{
	CString cLfd;
	CString cRabatt;
	CString cUmsGrenz;

	CEditListCtrl::StopEnter ();
	UpdateRow ();
	cLfd.Format (_T("%ld"), TLIEF_R_GR.lfd);
	cRabatt.Format (_T("%.2lf"), TLIEF_R_GR.rab_proz);
	cUmsGrenz.Format (_T("%.3lf"), TLIEF_R_GR.ums_grenz);
	FillList.SetItemText (cLfd.GetBuffer (), EditRow, 1);
	FillList.SetItemText (cRabatt.GetBuffer (), EditRow, 2);
	FillList.SetItemText (cUmsGrenz.GetBuffer (), EditRow, 3);
}

void CLiefRGrListCtrl::SetSel (CString& Text)
{
/*
   {
		ListEdit.SetSel (0, -1);
   }
*/
   if (EditCol == PosRab)
   {
		ListEdit.SetSel (0, -1);
   }
   else if (EditCol == PosUmsGrenz)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CLiefRGrListCtrl::FormatText (CString& Text)
{
}

void CLiefRGrListCtrl::NextRow ()
{
	int count = GetItemCount ();
	SetEditText ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;

	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
	StartEnter (EditCol, EditRow);
}

void CLiefRGrListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CLiefRGrListCtrl::LastCol ()
{
	if (EditCol < PosUmsGrenz) return FALSE;
	return TRUE;
}

void CLiefRGrListCtrl::UpdateRow ()
{
	CString cLfd         =  GetItemText (EditRow, PosLfd);
	CString cRab         =  GetItemText (EditRow, PosRab);
	CString cRabBz       =  GetItemText (EditRow, PosUmsGrenz);
	TLIEF_R_GR.lfd      = _tstol (cLfd.GetBuffer ());
	TLIEF_R_GR.rab_proz = CStrFuncs::StrToDouble (cRab);
	TLIEF_R_GR.ums_grenz = CStrFuncs::StrToDouble (cRabBz);
	LIEF_R_GR_CORE->UpdateRow (EditRow);
}

void CLiefRGrListCtrl::OnReturn ()
{

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (LastCol ())
	{
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
//		EditCol = 0;
		if (EditRow == rowCount)
		{
			EditCol = 1;
		}
		else
		{
			EditCol = 2;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
		if (IsDisplayOnly (EditCol))
		{
			EditCol ++;
		}
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CLiefRGrListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CLiefRGrListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	StopEnter ();
	EditCol --;
	while (IsDisplayOnly (EditCol) && EditCol >= 0) EditCol --;
    StartEnter (EditCol, EditRow);
}

BOOL CLiefRGrListCtrl::InsertRow ()
{
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T("  "), EditRow, 1);
	FillList.SetItemText (_T("  "), EditRow, 2);
	FillList.SetItemText (_T("  "), EditRow, 3);
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CLiefRGrListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	int row = EditRow;
	BOOL ret = CEditListCtrl::DeleteRow ();
	LIEF_R_GR_CORE->DeleteRow (row);
	return ret;
}

BOOL CLiefRGrListCtrl::AppendEmpty ()
{
	long lfd = 0;
	CString cLfd;
	int rowCount = GetItemCount ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CWnd *header = GetHeaderCtrl ();
	lfd = LIEF_R_GR_CORE->NextLfd ();
	FillList.InsertItem (rowCount, -1);
	cLfd.Format (_T("%ld"), lfd);
	FillList.SetItemText (cLfd.GetBuffer (), rowCount, PosLfd);
	FillList.SetItemText (_T("0,00"), rowCount, PosRab);
	FillList.SetItemText (_T(" "), rowCount, PosUmsGrenz);
	rowCount = GetItemCount ();
	return TRUE;
}

void CLiefRGrListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CLiefRGrListCtrl::RunItemClicked (int Item)
{
}

void CLiefRGrListCtrl::RunCtrlItemClicked (int Item)
{
}

void CLiefRGrListCtrl::RunShiftItemClicked (int Item)
{
}

void CLiefRGrListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	Text = cText.Trim ();
}

void CLiefRGrListCtrl::ScrollPositions (int pos)
{
}

