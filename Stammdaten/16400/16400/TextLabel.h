#pragma once
#include "label.h"

class CTextLabel :
	public CLabel
{
	DECLARE_DYNCREATE(CTextLabel)
public:
	CTextLabel(void);
public:
	~CTextLabel(void);
protected:
	BOOL m_IsTransparent;
	virtual void Draw (CDC& cDC);
};
