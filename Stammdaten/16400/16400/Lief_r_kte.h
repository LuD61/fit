#ifndef _LIEF_R_KTE_DEF
#define _LIEF_R_KTE_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LIEF_R_KTE {
   short          fil;
   long           lfd;
   TCHAR          lief[17];
   long           lief_s;
   short          mdn;
   TCHAR          rab_bz[25];
   double         rab_proz;
   double         proz_sum;
   DATE_STRUCT    von_dat;
   DATE_STRUCT    bis_dat;
   DATE_STRUCT    ae_dat;
   TCHAR          pers_nam[9];
};
extern struct LIEF_R_KTE lief_r_kte, lief_r_kte_null;

#line 8 "Lief_r_kte.rh"

class LIEF_R_KTE_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LIEF_R_KTE lief_r_kte;  
               LIEF_R_KTE_CLASS () : DB_CLASS ()
               {
               }
};
#endif
