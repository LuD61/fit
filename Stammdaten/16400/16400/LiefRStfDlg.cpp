// LiefRStfDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "16400.h"
#include "LiefRStfDlg.h"
#include "LiefRStfCore.h"
#include "Bmap.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CLiefRStfDlg-Dialogfeld

IMPLEMENT_DYNAMIC(CLiefRStfDlg, CDialog)

CLiefRStfDlg::CLiefRStfDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLiefRStfDlg::IDD, pParent)
{
	m_DlgBrush = NULL;
	m_ProzSumBrush = NULL;
	m_DlgColor = RGB (250, 250, 202);
	BkBitmap.SetBkType (BkBitmap.DynamicGrey);
//	BkBitmap.SetBkType (BkBitmap.DynamicMetal);
//	BkBitmap.SetDirection (BkBitmap.Vertical);
	LIEF_R_STF_CORE->SetListRow (this);
}

CLiefRStfDlg::~CLiefRStfDlg()
{
	if (m_DlgBrush != NULL)
	{
		DeleteObject (m_DlgBrush);
		m_DlgBrush = NULL;
	}
	if (m_ProzSumBrush != NULL)
	{
		DeleteObject (m_ProzSumBrush);
		m_ProzSumBrush = NULL;
	}
	CtrlGrid.DestroyAll ();
	ControlGrid.DestroyAll ();
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
}

void CLiefRStfDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAPTION, m_Caption);
	DDX_Control(pDX, IDC_OK,      m_OkEx);
	DDX_Control(pDX, IDC_DELETE,  m_Delete);
	DDX_Control(pDX, IDC_CANCEL,  m_CancelEx);
	DDX_Control(pDX, IDC_LIST,    m_List);
}


BEGIN_MESSAGE_MAP(CLiefRStfDlg, CDialog)
	ON_WM_CTLCOLOR ()
	ON_WM_SIZE ()
	ON_WM_ERASEBKGND () 
	ON_BN_CLICKED(IDC_OK,     OnBnClickedOk)
	ON_BN_CLICKED(IDC_DELETE, OnBnClickedDelete)
	ON_BN_CLICKED(IDC_CANCEL, OnBnClickedCancel)
END_MESSAGE_MAP()


// CLiefRStfDlg-Meldungshandler

BOOL CLiefRStfDlg::OnInitDialog()
{
	BOOL ret = CDialog::OnInitDialog();

	m_Caption.SetParts (60);
	m_Caption.SetWindowText (_T(" Staffelrabatt"));
	m_Caption.SetBoderStyle (m_Caption.Solide);
	m_Caption.SetOrientation (m_Caption.Left);
	m_Caption.SetDynamicColor (TRUE);
	m_Caption.SetTextColor (RGB (255, 255, 153));
	m_Caption.SetBkColor (RGB (192, 192, 192));

//	COLORREF BkColor (CColorButton::DynColorGray);
//	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF BkColor (RGB (0, 0, 255));
//	COLORREF DynColor = RGB (0, 0, 255);
	COLORREF RolloverColor = CColorButton::DynColorBlue;
	COLORREF DynColor = CColorButton::DynColorBlue;
//	COLORREF RolloverColor = RGB (192, 192, 192);

	m_OkEx.SetWindowText (_T("OK"));
	m_OkEx.nID = IDC_OK;
//	m_OkEx.SetToolTip (_T("Bearbeiten"));
	m_OkEx.Orientation = m_OkEx.Center;
	m_OkEx.TextStyle = m_OkEx.Standard;
	m_OkEx.BorderStyle = m_OkEx.Solide;
	m_OkEx.DynamicColor = TRUE;
	m_OkEx.TextColor =RGB (0, 0, 0);
	m_OkEx.SetBkColor (BkColor);
	m_OkEx.DynColor = DynColor;
	m_OkEx.RolloverColor = RolloverColor;
	m_OkEx.LoadBitmap (IDB_OKEX);
	m_OkEx.LoadMask (IDB_OKEX_MASK);

	m_Delete.SetWindowText (_T("L�schen"));
	m_Delete.nID = IDC_DELETE;
//	Delete.SetToolTip (_T("Bearbeiten"));
	m_Delete.Orientation = m_Delete.Center;
	m_Delete.TextStyle = m_Delete.Standard;
	m_Delete.BorderStyle = m_Delete.Solide;
	m_Delete.DynamicColor = TRUE;
	m_Delete.TextColor =RGB (0, 0, 0);
	m_Delete.SetBkColor (BkColor);
	m_Delete.DynColor = DynColor;
	m_Delete.RolloverColor = RolloverColor;
	m_Delete.LoadBitmap (IDB_DELETE);
	m_Delete.SetTansParentColor (RGB (0,0,0));
//	Delete.LoadMask (IDB_CANCELEX_MASK);

	m_CancelEx.SetWindowText (_T("Abbrechen"));
	m_CancelEx.nID = IDC_CANCEL;
//	m_CancelEx.SetToolTip (_T("Bearbeiten"));
	m_CancelEx.Orientation = m_CancelEx.Center;
	m_CancelEx.TextStyle = m_CancelEx.Standard;
	m_CancelEx.BorderStyle = m_CancelEx.Solide;
	m_CancelEx.DynamicColor = TRUE;
	m_CancelEx.TextColor =RGB (0, 0, 0);
	m_CancelEx.SetBkColor (BkColor);
	m_CancelEx.DynColor = DynColor;
	m_CancelEx.RolloverColor = RolloverColor;
	m_CancelEx.LoadBitmap (IDB_CANCELEX);
	m_CancelEx.LoadMask (IDB_CANCELEX_MASK);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	FillList = m_List;
	FillList.SetStyle (LVS_REPORT);

	if (m_List.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}

	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("LFD"),         1, 50,  LVCFMT_RIGHT);
	FillList.SetCol (_T("Rabatt"),      2, 80,  LVCFMT_RIGHT);
	FillList.SetCol (_T("Abnahmewert"), 3, 100, LVCFMT_RIGHT);
	m_List.AddDisplayOnly (1);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	CCtrlInfo *c_Caption = new CCtrlInfo (&m_Caption, 0, 0, DOCKRIGHT, 1);
    CtrlGrid.Add (c_Caption);

	ControlGrid.Create (this, 1, 2);
    ControlGrid.SetBorder (0, 0);
    ControlGrid.SetGridSpace (5, 2);
	CCtrlInfo *c_OkEx = new CCtrlInfo (&m_OkEx, 0, 0, 1, 1);
	ControlGrid.Add (c_OkEx);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ControlGrid.Add (c_Delete);
	CCtrlInfo *c_CancelEx = new CCtrlInfo (&m_CancelEx, 0, 4, 1, 1);
	ControlGrid.Add (c_CancelEx);


	CCtrlInfo *c_List = new CCtrlInfo (&m_List, 1, 3, DOCKSIZE, DOCKBOTTOM);
	c_List->RightDockControl = &m_OkEx; 
    CtrlGrid.Add (c_List);

	CCtrlInfo *c_ControlGrid = new CCtrlInfo (&ControlGrid, DOCKRIGHT, 3, 1, 1);
	c_ControlGrid->rightspace = 5;
	CtrlGrid.Add (c_ControlGrid);


	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	StaticFont.CreateFontIndirect (&l);
	l.lfHeight = 25;
	CaptionFont.CreateFontIndirect (&l);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	CFont *oldFont = cDC->SelectObject (&Font);
	cDC->SelectObject (oldFont);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Caption.SetFont (&CaptionFont);
	Read ();

	CtrlGrid.Display ();

	return FALSE;
}

HBRUSH CLiefRStfDlg::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	CRect rect;
/*
	if (nCtlColor == CTLCOLOR_DLG && m_DlgColor != NULL)
	{
		    if (m_DlgBrush == NULL)
			{
				m_DlgBrush = CreateSolidBrush (m_DlgColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && m_DlgColor != NULL)
	{
            pDC->SetBkColor (m_DlgColor);
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
*/

	if (nCtlColor == CTLCOLOR_DLG)
	{
		    if (m_DlgBrush == NULL)
			{
//				m_DlgBrush = CreateSolidBrush (m_DlgColor);
                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
				bitmap->DeleteObject ();
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC)
	{
		    if (m_DlgBrush == NULL)
			{
				m_DlgBrush = CreateSolidBrush (m_DlgColor);
                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
				bitmap->DeleteObject ();
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}


BOOL CLiefRStfDlg::OnEraseBkgnd (CDC* pDC) 
{
	CRect rect;
	BMAP Bmap;
    GetClientRect (&rect);
	BkBitmap.SetWidth (rect.right); 
	BkBitmap.SetHeight (rect.bottom);
	BkBitmap.SetPlanes (32);
	CBitmap *bitmap = BkBitmap.Create (); 
	Bmap.DrawBitmap (pDC, *bitmap, 0, 0);
	bitmap->DeleteObject ();
	return TRUE;
}

void CLiefRStfDlg::OnSize(UINT nType, int cx, int cy)
{
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
}

BOOL CLiefRStfDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_ESCAPE ||
				pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
			if (pMsg->wParam == VK_F12)
			{
				OnOK ();
				return TRUE;
			}
/*
			if (pMsg->wParam == VK_RETURN)
			{
				m_PrpUserList.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_PrpUserList &&
					GetFocus ()->GetParent () != &m_PrpUserList )
				{

					break;
			    }
				m_PrpUserList.OnKeyD (VK_TAB);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}
			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
*/
	}

	return FALSE;
}

void CLiefRStfDlg::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (Write ())
	{
		OnOK();
	}
	else
	{
		AfxMessageBox (_T("Rabtte konnten nicht gespeichert werden"), MB_OK | MB_ICONERROR);
	}
}

void CLiefRStfDlg::OnBnClickedDelete()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	m_List.DeleteRow ();
}

void CLiefRStfDlg::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnCancel();
}

BOOL CLiefRStfDlg::Read ()
{
	BOOL ret = FALSE;
	m_ListRow = 0;
	TLIEF_R_STF.mdn = TLIEF.mdn;
	TLIEF_R_STF.fil = TLIEF.fil;
	_tcscpy (TLIEF_R_STF.lief, TLIEF.lief);
	LIEF_R_STF_CORE->Read ();
	return ret;
}

BOOL CLiefRStfDlg::Write ()
{
	BOOL ret = TRUE;
	m_List.StopEnter ();
	ret = LIEF_R_STF_CORE->Write ();
	return ret;
}

// ListRow

void CLiefRStfDlg::Add ()
{
	FillList.InsertItem (m_ListRow, 0);
	CString cLfd;
	CString cRabatt;
	CString cAbnWrt;

	cLfd.Format (_T("%ld"), TLIEF_R_STF.lfd);
	cRabatt.Format (_T("%.2lf"), TLIEF_R_STF.rab_proz);
	cAbnWrt.Format (_T("%.3lf"), TLIEF_R_STF.abn_wrt);
	FillList.SetItemText (cLfd.GetBuffer (), m_ListRow, 1);
	FillList.SetItemText (cRabatt.GetBuffer (), m_ListRow, 2);
	FillList.SetItemText (cAbnWrt.GetBuffer (), m_ListRow, 3);
	m_ListRow ++;
}

void CLiefRStfDlg::Update ()
{
	Form.Show ();
}