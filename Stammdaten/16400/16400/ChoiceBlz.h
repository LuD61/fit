#ifndef _CHOICE_BLZ_DEF
#define _CHOICE_BLZ_DEF

#include "ChoiceX.h"
#include "BlzList.h"
#include <vector>

class CChoiceBlz : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
	    std::vector<CBlzList *> BlzList;
      	CChoiceBlz(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceBlz(); 
        virtual void FillList (void);


        void SearchItem (CListCtrl *ListBox, LPTSTR SearchText);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CBlzList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR,LPTSTR );
        int GetPtBez (LPTSTR, int, LPTSTR);
		void DestroyList ();
		virtual void OnEnter ();
	    virtual void OnFilter ();
	    afx_msg void OnMove (int x, int y);

	DECLARE_MESSAGE_MAP()
};
#endif
