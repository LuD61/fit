#pragma once
#include "afxext.h"
#include "resource.h"
#include "choicex.h"
#include "ctrlgrid.h"
#include "VLabel.h"

#define IDC_TITLE 1001

class CChoiceView :
	public CFormView
{
private:
	CChoiceX *m_Choice;
	CCtrlGrid CtrlGrid;
//	CVLabel m_Title;
	CColorButton m_Title;
	CSize m_TitleReg;
protected:
	CChoiceView(void);
	DECLARE_DYNCREATE(CChoiceView)
public:
	enum{ IDD = IDD_CHOICE_VIEW };
public:
	virtual ~CChoiceView(void);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
	virtual void OnSize (UINT, int, int);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnTitle ();
    afx_msg void OnBack();
    afx_msg void OnDestroy ();
	afx_msg void OnFileSave ();

};
