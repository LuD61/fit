#include "stdafx.h"
#include "ChoiceZahlKond.h"
#include "DbUniCode.h"
#include "DynDialog.h"
#include "DbQuery.h"
#include "Process.h"
#include "windowhandler.h"
#include "StrFuncs.h"
#include "ZahlKondDialog.h"
#include "DatabaseCore.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceZahlKond::Sort1 = -1;
int CChoiceZahlKond::Sort2 = -1;
int CChoiceZahlKond::Sort3 = -1;
int CChoiceZahlKond::Sort4 = -1;
int CChoiceZahlKond::Sort5 = -1;
int CChoiceZahlKond::Sort6 = -1;
int CChoiceZahlKond::Sort7 = -1;
int CChoiceZahlKond::Sort8 = -1;
int CChoiceZahlKond::Sort9 = -1;

CChoiceZahlKond::CChoiceZahlKond(CWnd* pParent) 
        : CChoiceX(pParent)
{
	HideEnter = FALSE;
	HideFilter = FALSE;
}

CChoiceZahlKond::~CChoiceZahlKond() 
{
	DestroyList ();
}

void CChoiceZahlKond::DestroyList() 
{
	for (std::vector<CZahlKondList *>::iterator pabl = ZahlKondList.begin (); pabl != ZahlKondList.end (); ++pabl)
	{
		CZahlKondList *abl = *pabl;
		delete abl;
	}
    ZahlKondList.clear ();
}

BEGIN_MESSAGE_MAP(CChoiceZahlKond, CChoiceX)
	//{{AFX_MSG_MAP(CChoiceZahlKond)
//	ON_WM_MOVE ()
END_MESSAGE_MAP()

void CChoiceZahlKond::FillList () 
{

	double         skto1;
        double         skto2;
        double         skto3;
        TCHAR          txt[61];
        short          zahl_kond;
        short          ziel1;
        short          ziel2;
        short          ziel3;
	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Zahlungskonditionen"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Zahlungskondition"),      1, 80, LVCFMT_RIGHT);
    SetCol (_T("Tage1"),  2, 80, LVCFMT_RIGHT);
    SetCol (_T("Skonto1"),  3, 80, LVCFMT_RIGHT);
    SetCol (_T("Tage2"),  4, 80, LVCFMT_RIGHT);
    SetCol (_T("Skonto2"),  5, 80, LVCFMT_RIGHT);
    SetCol (_T("Tage3"),  6, 80, LVCFMT_RIGHT);
    SetCol (_T("Skonto3"),  7, 80, LVCFMT_RIGHT);
    SetCol (_T("Text"),  8, 250);

	if (FirstRead && !HideFilter)
	{
		FirstRead = FALSE;
		Load ();
//		if (!IsModal)
		{
			PostMessage (WM_COMMAND, IDC_FILTER, 0l);
		}
		return;
	}

	if (ZahlKondList.size () == 0)
	{
		CString Sql = _T("");
        DbClass->sqlout ((short *)   &zahl_kond,  SQLSHORT, 0);
	    DbClass->sqlout ((short *) &ziel1,SQLSHORT,0);
	    DbClass->sqlout ((double *) &skto1,SQLDOUBLE,0);
	    DbClass->sqlout ((short *) &ziel2,SQLSHORT,0);
	    DbClass->sqlout ((double *) &skto2,SQLDOUBLE,0);
	    DbClass->sqlout ((short *) &ziel3,SQLSHORT,0);
	    DbClass->sqlout ((double *) &skto3,SQLDOUBLE,0);
	    DbClass->sqlout ((TCHAR *) txt,SQLCHAR,61);
	    Sql = _T("select zahl_kond.zahl_kond,  zahl_kond.ziel1, zahl_kond.skto1,  ")
		      _T("zahl_kond.ziel2, zahl_kond.skto2, zahl_kond.ziel3, zahl_kond.skto3,  zahl_kond.txt ")
			  _T("from zahl_kond"); 
		if (QueryString != _T(""))
		{
			Sql += _T(" and ");
			Sql += QueryString;
		}

		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe f�r den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}

		DbClass->sqlopen (cursor);
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) txt;
			CDbUniCode::DbToUniCode (txt, pos);
			CZahlKondList *abl = new CZahlKondList ();
			abl->zahl_kond = zahl_kond;
			abl->ziel1 = ziel1;
			abl->skto1 = skto1;
			abl->ziel2 = ziel2;
			abl->skto2 = skto2;
			abl->ziel3 = ziel3;
			abl->skto2 = skto2;
			abl->txt   = txt;
			ZahlKondList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CZahlKondList *>::iterator pabl = ZahlKondList.begin (); pabl != ZahlKondList.end (); ++pabl)
	{
		CZahlKondList *abl = *pabl;
        CString ZahlKond;
        CString Ziel1;
        CString Skto1;
        CString Ziel2;
        CString Skto2;
        CString Ziel3;
        CString Skto3;
        ZahlKond.Format (_T("%hd") ,abl->zahl_kond);   
        Ziel1.Format (_T("%hd") ,abl->ziel1);   
        Skto1.Format (_T("%.2lf") ,abl->skto1);   
        Ziel2.Format (_T("%hd") ,abl->ziel2);   
        Skto2.Format (_T("%.2lf") ,abl->skto2);   
        Ziel3.Format (_T("%hd") ,abl->ziel3);   
        Skto3.Format (_T("%.2lf") ,abl->skto3);   

        int ret = InsertItem (i, -1);
		int pos = 0; 
        ret = SetItemText (ZahlKond.GetBuffer (), i, ++pos);
        ret = SetItemText (Ziel1.GetBuffer (), i, ++pos);
        ret = SetItemText (Skto1.GetBuffer (), i, ++pos);
        ret = SetItemText (Ziel2.GetBuffer (), i, ++pos);
        ret = SetItemText (Skto2.GetBuffer (), i, ++pos);
        ret = SetItemText (Ziel3.GetBuffer (), i, ++pos);
        ret = SetItemText (Skto3.GetBuffer (), i, ++pos);
        ret = SetItemText (Skto3.GetBuffer (), i, ++pos);
        ret = SetItemText (abl->txt.GetBuffer (), i, ++pos);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort7 = -1;
    Sort8 = -1;
    Sort9 = -1;
    Sort (listView);
//    FirstSelection ();
	GetDlgItem (IDC_SEARCH)->SetFocus ();
	m_Idx = -1;
}


void CChoiceZahlKond::SearchItem (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();
		CString cSearch = Search;
		cSearch.MakeUpper ();

        if (_tmemcmp (cSearch.GetBuffer (), iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceZahlKond::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    EditText.MakeUpper ();
    SearchItem (ListBox, EditText.GetBuffer ());
}

int CChoiceZahlKond::GetPtBez (LPTSTR item, LPTSTR wert, LPTSTR bez)
{
	TCHAR ptitem [9];
	TCHAR ptwert [5];
	TCHAR ptbez [37];
	static int cursor = -1;

	_tcscpy (ptbez, _T(""));
	if (cursor == -1)
	{
		DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
		DbClass->sqlin ((LPTSTR) ptitem, SQLCHAR, 9);
		DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
		cursor = DbClass->sqlcursor (_T("select ptbez from ptabn where ptitem = ? ")
								_T("and ptwert = ?"));
	}
	strcpy (ptitem, item);
	strcpy (ptwert, wert);
	DbClass->sqlopen (cursor);
	int dsqlstatus = DbClass->sqlfetch (cursor);
	strcpy (bez, ptbez);
    return dsqlstatus;
}

int CChoiceZahlKond::GetPtBez (LPTSTR item, int wert, LPTSTR bez)
{
	TCHAR ptitem [9];
	TCHAR ptwert [5];
	TCHAR ptbez [37];
	static int cursor = -1;

	_tcscpy (ptbez, _T(""));
	if (cursor == -1)
	{
		DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
		DbClass->sqlin ((LPTSTR) ptitem, SQLCHAR, 9);
		DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
		cursor = DbClass->sqlcursor (_T("select ptbez from ptabn where ptitem = ? ")
								_T("and ptwert = ?"));
	}
	strcpy (ptitem, item);
	sprintf (ptwert,"%d", wert);
	DbClass->sqlopen (cursor);
	int dsqlstatus = DbClass->sqlfetch (cursor);
	strcpy (bez, ptbez);
    return dsqlstatus;
}

int CALLBACK CChoiceZahlKond::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   int ret = 0;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   ret = (strItem2.CompareNoCase (strItem1));
   switch (SortRow)
   {
	   case 0:
		   ret *= Sort1;
		   break;
	   case 1:
		   ret *= Sort2;
		   break;
	   case 2:
		   ret *= Sort3;
		   break;
	   case 3:
		   ret *= Sort4;
		   break;
	   case 4:
		   ret *= Sort5;
		   break;
	   case 5:
		   ret *= Sort6;
		   break;
	   case 6:
		   ret *= Sort7;
		   break;
	   case 7:
		   ret *= Sort8;
		   break;
	   case 8:
		   ret *= Sort9;
		   break;
   }
   return ret;
}


void CChoiceZahlKond::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
        case 4:
              Sort5 *= -1;
			  if (Sort5 < 0) SortPos = 1;
              break;
        case 5:
              Sort6 *= -1;
			  if (Sort6 < 0) SortPos = 1;
              break;
        case 6:
              Sort7 *= -1;
			  if (Sort7 < 0) SortPos = 1;
              break;
        case 7:
              Sort8 *= -1;
			  if (Sort8 < 0) SortPos = 1;
              break;
        case 8:
              Sort9*= -1;
			  if (Sort9 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CZahlKondList *abl = ZahlKondList [i];
		   
		   abl->zahl_kond  = _tstoi (ListBox->GetItemText (i, 1));
		   abl->ziel1      = _tstoi (ListBox->GetItemText (i, 2));
		   abl->skto1      = CStrFuncs::StrToDouble (ListBox->GetItemText (i, 3));
		   abl->ziel2      = _tstoi (ListBox->GetItemText (i, 4));
		   abl->skto2      = CStrFuncs::StrToDouble (ListBox->GetItemText (i, 5));
		   abl->ziel3      = _tstoi (ListBox->GetItemText (i, 6));
		   abl->skto3      = CStrFuncs::StrToDouble (ListBox->GetItemText (i, 7));
		   abl->txt        = ListBox->GetItemText (i, 8);
	}
	for (int i = 1; i <= 2; i ++)
	{
		SetItemSort (i, 2);
	}
	SetItemSort (SortRow, SortPos);
}

void CChoiceZahlKond::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = ZahlKondList [idx];
}

CZahlKondList *CChoiceZahlKond::GetSelectedText ()
{
	CZahlKondList *abl = (CZahlKondList *) SelectedRow;
	return abl;
}


void CChoiceZahlKond::OnEnter ()
{
	CZahlKondDialog dlg;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cZahlKond = m_List.GetItemText (idx, 1);  
		DATABASE->SaveZahlKond ();
		TZAHL_KOND.zahl_kond = _tstoi (cZahlKond.GetBuffer ());
		dlg.SetReadOnly ();
		INT_PTR ret = dlg.DoModal ();
		if (ret = IDOK)
		{
           CZahlKondList *abl = ZahlKondList [idx];
		   abl->zahl_kond  = TZAHL_KOND.zahl_kond;
		   abl->ziel1      = TZAHL_KOND.ziel1;
		   abl->ziel2      = TZAHL_KOND.ziel2;
		   abl->ziel3      = TZAHL_KOND.ziel3;
		   abl->skto1      = TZAHL_KOND.skto1;
		   abl->skto2      = TZAHL_KOND.skto2;
		   abl->skto3      = TZAHL_KOND.skto3;
		   abl->txt        = TZAHL_KOND.txt;

		   CString ZahlKond;
		   CString Ziel1;
		   CString Skto1;
		   CString Ziel2;
		   CString Skto2;
		   CString Ziel3;
		   CString Skto3;
		   ZahlKond.Format (_T("%hd") ,abl->zahl_kond);   
		   Ziel1.Format (_T("%hd") ,abl->ziel1);   
		   Skto1.Format (_T("%.2lf") ,abl->skto1);   
		   Ziel2.Format (_T("%hd") ,abl->ziel2);   
		   Skto2.Format (_T("%.2lf") ,abl->skto2);   
		   Ziel3.Format (_T("%hd") ,abl->ziel3);   
		   Skto3.Format (_T("%.2lf") ,abl->skto3);   

		   int pos =0;
		   SetItemText (ZahlKond.GetBuffer (), idx, ++pos);
		   SetItemText (Ziel1.GetBuffer (), idx, ++pos);
		   SetItemText (Skto1.GetBuffer (), idx, ++pos);
		   SetItemText (Ziel2.GetBuffer (), idx, ++pos);
		   SetItemText (Skto2.GetBuffer (), idx, ++pos);
		   SetItemText (Ziel3.GetBuffer (), idx, ++pos);
		   SetItemText (Skto3.GetBuffer (), idx, ++pos);
		   SetItemText (Skto3.GetBuffer (), idx, ++pos);
		   SetItemText (abl->txt.GetBuffer (), idx, ++pos);
		}
		DATABASE->RestoreZahlKond ();
	}
}

void CChoiceZahlKond::OnFilter ()
{
	CDynDialog dlg;
	CDynDialogItem *Item;
	static int headercy = 200;

	dlg.DlgCaption = "Filter";
	dlg.UseFilter = UseFilter;

	if (vFilter.size () > 0)
	{
		for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
		{
			CDynDialogItem *i = *it;
			Item = new CDynDialogItem ();
			*Item = *i;
			if (Item->Name == _T("reset"))
			{
				if (UseFilter)
				{
					Item->Value = _T("aktiv");
				}
				else
				{
					Item->Value = _T("nicht aktiv");
				}
			}
			dlg.AddItem (Item);

		}
		dlg.Header.cy = headercy;	
	}
	else
	{
		int y = 10;
		Item = new CDynDialogItem ();
		Item->Name = _T("lzahl_kond");
		Item->CtrClass = "static";
		Item->Value = _T("Zahlungskondition");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1001;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("zahl_kond");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1002;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lziel1");
		Item->CtrClass = "static";
		Item->Value = _T("Ziel 1");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1003;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("ziel1");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		dlg.Header.cy = y + 30;
		headercy = dlg.Header.cy;

		Item = new CDynDialogItem ();
		Item->Name = _T("OK");
		Item->CtrClass = "button";
		Item->Value = _T("OK");
		Item->Template.x = 15;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDOK;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("cancel");
		Item->CtrClass = "button";
		Item->Value = _T("abbrechen");
		Item->Template.x = 70;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDCANCEL;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("reset");
		Item->CtrClass = "button";
		if (UseFilter)
		{
			Item->Value = _T("aktiv");
		}
		else
		{
			Item->Value = _T("nicht aktiv");
		}
		Item->Template.x = 125;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = ID_USE;
		dlg.AddItem (Item);
	}

	dlg.CreateModal ();
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK) return;
 
	UseFilter = dlg.UseFilter;
	int count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = dlg.Items.begin (); it != dlg.Items.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (count >= (int) vFilter.size ())
			{
				Item = new CDynDialogItem ();
				*Item = *i;
				vFilter.push_back (Item);
			}
			*vFilter[count] = *i;
			count ++;
	}
    CString Query;
    CDbQuery DbQuery;

	QueryString = _T("");
	if (!UseFilter)
	{
		FillList ();
		return;
	}
	count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (strcmp (i->CtrClass, "edit") != 0) continue; 
			if (i->Value.Trim () != _T(""))
			{
				if (i->Type == i->String || i->Type == i->Date)
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, TRUE);
				}
				else
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, FALSE);
				}
				if (count > 0)
				{
					QueryString += _T(" and ");
				}
                QueryString += Query;
				count ++;
			}
	}
	FillList ();
}

void CChoiceZahlKond::OnMove (int x, int y)
{
	WINDOWHANDLER->IsDockPlace (x, y);
}