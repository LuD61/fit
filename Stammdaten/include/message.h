#ifndef _MSGDEF
#define _MSG

class MELDUNG 
{
     private :
         HWND MesshWnd;     
         HWND WaithWnd;     
         int Fktx, Fkty, Fktcx, Fktcy;
         int Fktx2, Fkty2, Fktcx2, Fktcy2;
     public :
		 form *messform;
         MELDUNG() : messform (NULL), MesshWnd (NULL), WaithWnd (NULL)
		 {
		 }

		 void Message (HWND, char *, int);
         void SaveMessPos (void);
         void MoveMess (void);
         HWND OpenMessage (void);
		 void Message (char *);
		 void CloseMessage (void);
         void CloseMesshWnd (void);
		 void WaitWindow (char *);
		 void CloseWaitWindow (void);
};
#endif
