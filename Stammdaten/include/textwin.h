#ifndef _TEXTWIN_DEF
#define _TEXTWIN_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"

#define OK_CTL 603
#define CANCEL_CTL 604 

class TEXTWIN : virtual public DLG 
{
          private :
              CFIELD **_fWork;
              CFORM *fWork;
              CFIELD **_fText;
              CFORM *fText;
//              int cx, cy;
              char *head;
              char *itemdata;
              long hlen;
              long dlen;
              long offset;
              int items;
              char **Rows;
              HWND hMainWindow;
              HINSTANCE hInstance;
              static COLORREF Background;
              int mx;
              int my;
              
          public :
   	          TEXTWIN (char *, HINSTANCE, HWND, int, int);
  	          ~TEXTWIN ();

 	          void Init (char *);
              BOOL MakeCform (int); 
              void GetText (char *, int , int);
              void SetWinBackground (COLORREF);
              BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
              BOOL OnRButtonUp (HWND,UINT,WPARAM,LPARAM);
              static void SetBackground (COLORREF);
};
#endif
