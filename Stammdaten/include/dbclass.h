#ifndef DB_CLASSDEF
#define DB_CLASSDEF
#ifdef MO_CURSC
#include "mo_cursc.h"
#else
#include "mo_curso.h"
#endif
#include <windows.h>
#include <string.h>
#include "wmaskc.h"

#define NEXT 1
#define PRIOR 2
#define PREVIOUS 2
#define FIRST 3
#define LAST 4
#define CURRENT 5
#define RELATIV 7
#define DBABSOLUTE 6

#define SQLCHAR   0
#define SQLSHORT  1
#define SQLLONG   2
#define SQLDOUBLE 3
//#define SQLDATE 4


class DB_CLASS 
{
       public :
               short cursor;
               short test_upd_cursor;
               short upd_cursor;
               short ins_cursor;
               short del_cursor;
               short cursor_ausw;
               int   scrollpos;
               static short ShortNull;
               static long LongNull;
               static double DoubleNull;

               DB_CLASS ()
               {
                         cursor          = -1;
                         test_upd_cursor = -1;
                         upd_cursor      = -1;
                         ins_cursor      = -1;
                         del_cursor      = -1;
                         scrollpos       = 1;
               }

			   ~DB_CLASS ()
			   {
				   if (cursor > -1) close_sql (cursor);
				   if (test_upd_cursor > -1) close_sql (test_upd_cursor);
				   if (upd_cursor > -1) close_sql (upd_cursor);
				   if (ins_cursor > -1) close_sql (ins_cursor);
				   if (del_cursor > -1) close_sql (del_cursor);
			   }


               virtual void prepare (void) 
               {
               }
               int dbreadfirst (void);
               int dbread (void);
               int dbupdate (void);
               int dblock (void);
               int dbdelete (void);
               void dbclose (void);

               int dbmove (int);
               int dbmove (int, int);
               int dbcanmove (int);
               int dbcanmove (int, int);
			   void cleanin ();
			   void cleanout ();
               int sqlin  (void *, int, int);
               int sqlout (void *, int, int);
               int sqlcursor (char *);
               int sqlclose (int);
               int sqlopen (int);
               int sqlfetch (int);
               int sqlexecute (int);
               int sqlcomm (char *);
               int IsShortnull (short);
               int IsLongnull (long);
               int IsDoublenull (double);

#ifndef CONSOLE
               int ShowAllBu (HWND hWnd, int, int, 
                              int (*) (void), void (*) (int),
                              char *, int,  char *, int,
                              form *, form *, form *);  
#endif
};
#endif