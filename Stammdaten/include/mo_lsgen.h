#ifndef _LSGEN_DEF
#define LSGEN

void DruckeFile (void);
void WaitListEnd (void);
int IsError ();
void print_file (void);
void ShowFile (void);
void printerfile (void);
BOOL LeseFormat (char *form_nr);
BOOL FillFormFeld (char *feldname, char *valfrom, char *valto);
void BelegeDrucker (void);
void BelegeGdiDrucker (void);
void SetFormular (BOOL frml);
void drucker_akt (char *);
void StartPrint  (void);
void ChoisePrinter (void);
void ChoisePrinter0 (void);
void ChoisePrinter1 (void);
void SetAusgabe (int);
#endif

