#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "spezdlg.h" 
#include "mo_gdruck.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#include "searchmdn.h"
#include "searchfil.h"
#include "searchhwg.h"
#include "ptab.h"
#ifdef BIWAK
#include "conf_env.h"
#endif
#include "mo_progcfg.h"
#include "datum.h"


static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;

static struct PMENUE MenuePosEnter = {"Positionen erfassen F6",      "G",  NULL, IDM_LIST}; 
static struct PMENUE MenueInsert   = {"einf�gen F6",                 "G",  NULL, IDM_INSERT}; 


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont checkfonth = {
                           "ARIAL", STDSIZE, 0, 0,
                            BLACKCOL,
                            LTGRAYCOL,
                            0,
                            NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


static mfont cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};

struct DEFAULTFF
{
      char mdn [7];
      char mdn_krz [20];
      char fil [7];
      char fil_krz [18];
      char hwg [10];
      char hwg_bz1 [28];
      char kw [5];
      char a [15];
      char a_bz1 [28];
} defaultlistf, defaultlistf_null;



COLORREF SpezDlg::Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL};

COLORREF SpezDlg::BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL};

HBITMAP SpezDlg::ListBitmap  = NULL;
HBITMAP SpezDlg::ListBitmapi = NULL;
BitImage SpezDlg::ImageDelete;
BitImage SpezDlg::ImageInsert;
BitImage SpezDlg::ImageList;
COLORREF SpezDlg::SysBkColor = LTGRAYCOL;
BOOL SpezDlg::ListButtons = FALSE;
int SpezDlg::ButtonSize = SMALL;

mfont *SpezDlg::Font = &dlgposfont;

CFIELD *SpezDlg::_fHead [] = {

// Hier Kopffelder eintragen

                     new CFIELD ("mdn_txt", "Mandant",  0, 0, 1, 1,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("mdn", defaultlistf.mdn,  6, 0, 18, 1,  NULL, "%4d", CEDIT,
                                 MDN_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("mdn_choise", "", 2, 0, 24, 1, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("mdn_krz", defaultlistf.mdn_krz, 19, 0, 30, 1,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("fil_txt", "Filiale",  0, 0, 50, 1,  NULL, "", 
                                 CREMOVED,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("fil", defaultlistf.fil,  6, 0, 58, 1,  NULL, "%4d", CREMOVED,
                                 FIL_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("fil_choise", "", 2, 0, 64, 1, NULL, "", CREMOVED,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("fil_krz", defaultlistf.fil_krz, 17, 0, 67, 1,  NULL, "", 
                                 CREMOVED,
                                 500, Font, 0, 0),
                     new CFIELD ("hwg_txt", "Hautpwarengruppe",  0, 0, 1, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("hwg", defaultlistf.hwg, 6, 0, 18, 2,  NULL, "%4d", CEDIT,
                                 HWG_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("hwg_choise", "", 2, 0, 24, 2, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("hwg_bz1", defaultlistf.hwg_bz1, 26, 0, 30, 2,  NULL, "", 
                                 CREADONLY,
                                 500, Font, 0, 0),
                     new CFIELD ("kw_txt", "Woche",  0, 0, 1, 3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kw", defaultlistf.kw, 4, 0, 18, 3,  NULL, "%2d", CEDIT,
                                 KW_CTL, Font, 0, ES_RIGHT),
/*
                     new CFIELD ("a_txt", "Artikel",  0, 0, 1, 4,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("a", defaultlistf.a, 13, 0, 18, 4,  NULL, "%13.0lf", CEDIT,
                                 A_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("a_choice", "", 2, 0, 31, 4, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
*/
//                     new CFIELD ("a_bz1", defaultlistf.a_bz1, 26, 0, 35, 4,  NULL, "", 
//                                 CREADONLY,
//                                 500, Font, 0, 0),
                     NULL,
};

CFORM SpezDlg::fHead (6, _fHead);


CFIELD *SpezDlg::_fPos [] = {
                     new CFIELD ("pos_frame", "",    200,13, 0, 5,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    

// Hier Positionsfelder eintragen

                     NULL,
};



CFORM SpezDlg::fPos (1, _fPos);


CFIELD *SpezDlg::_fcField [] = {
                     new CFIELD ("fcField1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEADCTL, Font, 0, 0),
                     new CFIELD ("fcField2", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   POSCTL, Font, 0, 0),
                     NULL,                      
};

CFORM SpezDlg::fcForm (2, _fcField);

CFIELD *SpezDlg::_fcField0 [] = { 
                     new CFIELD ("fcForm", (CFORM *) &fcForm, 0, 0, -1, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM SpezDlg::fcForm0 (1, _fcField0);

Work SpezDlg::work;
char *SpezDlg::HelpName = "defaultlist.cmd";
SpezDlg *SpezDlg::ActiveSpezDlg = NULL;
HBITMAP SpezDlg::SelBmp;
int SpezDlg::pos = 0;
char SpezDlg::ptwert[5];


void SpezDlg::ChangeF6 (DWORD Id)
{

    if (Id == IDM_LIST)
    {
         if (DeleteMenu (ActiveDlg->GethMenu (), IDM_LIST , MF_BYCOMMAND)) 
         {
                InsertMenu (ActiveDlg->GethMenu (), IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
                            MenueInsert.menid, MenueInsert.text);
                            
         }
    }
    else if (Id == IDM_INSERT)
    {
         if (DeleteMenu (ActiveDlg->GethMenu (), IDM_INSERT , MF_BYCOMMAND))
         {
                 InsertMenu (ActiveDlg->GethMenu (), IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
                            MenuePosEnter.menid, MenuePosEnter.text);
         }
    }
}

int SpezDlg::ReadMdn (void)
{
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;

         fPos.GetText ();
         FromForm (dbheadfields);
         work.GetMdnName (defaultlistf.mdn_krz,   atoi (defaultlistf.mdn));
         if (atoi (defaultlistf.mdn) == 0)
         {
             sprintf (defaultlistf.fil, "%4d", 0);
             work.GetFilName (defaultlistf.fil_krz,   atoi (defaultlistf.mdn), atoi (defaultlistf.fil));
             Enable (&fcForm, EnableHeadFil, FALSE); 
             fHead.SetCurrentName ("text1");
         }
         else
         {
             if (fHead.GetCfield ("fil")->IsDisabled ())
             {
                    Enable (&fcForm, EnableHeadFil, TRUE); 
                    fHead.SetCurrentName ("fil");
             }
         }
         if (syskey == KEYUP)
         {
                    fHead.SetCurrentName ("kun");
         }
         fHead.SetText ();
         return 0;
}

int SpezDlg::ReadFil (void)
{
          
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
         FromForm (dbheadfields);
         work.GetFilName (defaultlistf.fil_krz,   atoi (defaultlistf.mdn), atoi (defaultlistf.fil));
         fHead.SetText ();
         return 0;
}

int SpezDlg::ReadHwg (void)
{
//         short wg; 
//         long ag; 
          
         if (syskey == 0) return 0;
         if (syskey == KEY9) return 0;
         fPos.GetText ();
		 if (work.GetMatrix () == LIEFERANT && atoi (defaultlistf.hwg) == 0)
		 {
//             Enable (&fcForm, EnableA, TRUE); 
			 return 0;
		 }

         Enable (&fcForm, EnableA, FALSE); 
         FromForm (dbheadfields);
         if (work.GetHwgBez (defaultlistf.hwg_bz1,   atoi (defaultlistf.hwg)) == 100)
         {
                 disp_mess ("Haupwarengruppe nicht gefunden",2);
                 fHead.SetCurrentName ("hwg");
                 return 0;
         }
/*
         wg = atoi (defaultlistf.wg);
         work.TestWgHwg (&wg, atoi (defaultlistf.hwg));
         sprintf (defaultlistf.wg, "%d", wg);
         if (wg == 0)
         {
             strcpy (defaultlistf.wg_bz1, "");
         }
         ag = atol (defaultlistf.ag);
         work.TestAgWg (&ag, wg);
         sprintf (defaultlistf.ag, "%ld", ag);
         if (ag == 0l)
         {
             strcpy (defaultlistf.ag_bz1, "");
         }
*/
         fHead.SetText ();
         return 0;
}


int SpezDlg::EnterPos (void)
{

//Hier Postionsdialog eintragen, falls vorhanden.
/*
         switch (syskey)
         {
               case KEYDOWN :
               case KEYTAB :
                   fHead.SetCurrentName ("mdn");
                   return TRUE;
         }
        
         if (DLG::MousePressed)
         {
             return FALSE;
         }

         if (syskey != KEYCR)
         {
             return FALSE;
         }

         
         fHead.GetText ();

         FromForm (dbheadfields);

         work.BeginWork ();
*/

         ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_ENABLED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
         EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
         EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
         EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
         if (ActiveSpezDlg->GetToolbar2 () != NULL)
         {
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6") != NULL)
                 {
                      ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
                                   ImageList.Image;
                 }
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("insert")->GetFeld ())->bmp = 
					 ImageInsert.Inactive;
                 ActiveSpezDlg->GetToolbar2 ()->display ();
         }
// Bei zus�tzlichen Positionseingaben Positionsfelder aktiv setzen
//         Enable (&fcForm, EnableHead, FALSE); 
//         Enable (&fcForm, EnablePos,  TRUE); 
// Erstes Positionsfeld setzen. 
//         fPos.SetCurrentName ("adr_krz");

         return 0;
}


int SpezDlg::GetPtab (void)
{
//         char wert [5];
         CFIELD *Cfield;


         fPos.GetText ();

         Cfield = ActiveDlg->GetCurrentCfield ();
         return 0;
}


ItProg *SpezDlg::After [] = {
                                   new ItProg ("mdn",         ReadMdn),
                                   new ItProg ("fil",         ReadFil),
                                   new ItProg ("hwg",         ReadHwg),
                                   NULL,
};

ItProg *SpezDlg::Before [] = {NULL,
};

ItFont *SpezDlg::itFont [] = {
                                  new ItFont ("mdn_krz",       &ltgrayfont),
                                  new ItFont ("hwg_bz1",          &ltgrayfont),
                                  NULL
};

FORMFIELD *SpezDlg::dbheadfields [] = {
//         new FORMFIELD ("mdn",       defaultlistf.mdn,       (short *)   &partner.mdn,       FSHORT,   NULL),
//         new FORMFIELD ("fil",       defaultlistf.fil,       (short *)   &partner.fil,       FSHORT,   NULL),
           NULL,
};

FORMFIELD *SpezDlg::dbfields [] = {
         NULL,
};



char *SpezDlg::EnableHead [] = {
                                "mdn",
                                "mdn_choise",
                                "fil",
                                "fil_choise",
                                "hwg",
                                "hwg_choise",
                                "kw",
                                 NULL
};

char *SpezDlg::EnableHeadFil [] = {
                                     "fil",
                                     NULL
};


char *SpezDlg::EnablePos[] = {
                               NULL,
};

char *SpezDlg::EnableA [] = {
                                     "a",
                                     "a_choice",
                                     NULL
};

                         
SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

SpezDlg::SpezDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType, int EnterM) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
//             EnterMode = EnterM;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}


void SpezDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             PROG_CFG ProgCfg ("18510");
//             char cfg_v [256];
             int i;
             int xfull, yfull;

//             Work.Prepare ();

             ImageDelete.Image    = BMAP::LoadBitmap (hInstance, "DEL",       "DELMASK",      SysBkColor);
             ImageDelete.Inactive = BMAP::LoadBitmap (hInstance, "DELIA",     "DELMASK",      SysBkColor);
             ImageInsert.Image    = BMAP::LoadBitmap (hInstance, "INSERT",    "INSERTMASK",   SysBkColor);
             ImageInsert.Inactive = BMAP::LoadBitmap (hInstance, "INSERTIA",  "INSERTMASK",   SysBkColor);
             ImageList.Image      = BMAP::LoadBitmap (hInstance, "ARRDOWND",  "ARRDOWNDMASK", SysBkColor);
             ImageList.Inactive   = BMAP::LoadBitmap (hInstance, "ARRDOWNDI","ARRDOWNDMASK", SysBkColor);
             hwndCombo1 = NULL;
             hwndCombo2 = NULL;
             Combo1 = NULL;
             Combo2 = NULL;
             ColorSet = FALSE;
             LineSet = FALSE;
             Toolbar2 = NULL;
             DockList = TRUE;
             ListDlg = NULL;
             hwndList = NULL;

             ActiveSpezDlg = this; 
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

             ltgrayfont.FontBkColor = SysBkColor;

             StdSize = Size;
             dlgfont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             cpfont.FontHeight     = cpsize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 checkfonth.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 checkfonth.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }

             fHead.SetFieldanz ();
             fPos.SetFieldanz ();
             fcForm.SetFieldanz ();

             fHead.GetCfield ("mdn_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("fil_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
             fHead.GetCfield ("hwg_choise")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));
//             fHead.GetCfield ("a_choice")->SetBitmap 
//                 (LoadBitmap (hInstance, "ARRDOWN"));

             fHead.GetCfield ("mdn_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("fil_choise")->SetTabstop (FALSE); 
             fHead.GetCfield ("hwg_choise")->SetTabstop (FALSE); 
//             fHead.GetCfield ("a_choice")->SetTabstop (FALSE); 


             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             SetTmFont (&dlgfont);
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fcForm);
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fcForm);
             }


             for (i = 0; itFont[i] != NULL; i ++)
             {
                         itFont[i]->SetFont (&fcForm);
             }
             fPos.GetCfield ("pos_frame")->SetBorder (GRAYCOL, WHITECOL, RAISEDHLINE); 
//             fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
/*
             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; itFont[i] != NULL; i ++)
                {
                         itFont[i]->SetFont (&fcForm);
                }

                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; itFont[i] != NULL; i ++)
                {
                         itFont[i]->SetFont (&fcForm);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }
*/

             char datum [12];
             sysdate (datum);
             int kw = get_woche (datum);
             sprintf (defaultlistf.kw, "%d", kw);
             Enable (&fcForm, EnableHead, TRUE); 
             Enable (&fcForm, EnablePos,  FALSE);
             SetDialog (&fcForm0);
}

void SpezDlg::RemoveA (BOOL b)
{
/*
	    int attrATxt = fWork->GetCfield ("a_txt")->GetAttribut ();
	    int attrA = fWork->GetCfield ("a")->GetAttribut ();
	    int attrCh = fWork->GetCfield ("a_choice")->GetAttribut ();
		if (b)
		{
			attrATxt = CREMOVED;
			attrA = CREMOVED;
			attrCh = CREMOVED;
		}
		else if ((attrA & CREMOVED) != 0)
		{
			attrATxt = CDISPLAYONLY;
			attrA = CEDIT;
			attrCh = CBUTTON;
		}
        fWork->GetCfield ("a_txt")->SetAttribut (attrATxt); 
        fWork->GetCfield ("a")->SetAttribut (attrA); 
        fWork->GetCfield ("a_choice")->SetAttribut (attrCh); 
*/
}





BOOL SpezDlg::OnKeyReturn (void)
{
        HWND hWnd;
        
        hWnd = GetFocus ();
        return FALSE;
}


BOOL SpezDlg::OnKeyDown (void)
{
        return FALSE;
}


BOOL SpezDlg::OnKeyUp (void)
{
        return FALSE;
}


BOOL SpezDlg::OnKey2 ()
{
        return TRUE;
}

BOOL SpezDlg::OnKeyHome ()
{
        return TRUE;
}

BOOL SpezDlg::OnKeyEnd ()
{
        return TRUE;

}
BOOL SpezDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL SpezDlg::OnKey3 ()
{
        syskey = KEY3;
        return TRUE;
}

BOOL SpezDlg::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

BOOL SpezDlg::OnKey5 ()
{

        syskey = KEY5;
        syskey = KEY5;
        if (fHead.GetCfield ("mdn")->IsDisabled () == FALSE)
        {
                  DestroyWindow ();
                  return TRUE;
        }
        Enable (&fcForm, EnableHead, TRUE); 
        Enable (&fcForm, EnablePos,  FALSE);
        work.RollbackWork ();
        work.InitRec ();
        ToForm (dbfields);
        fPos.SetText ();
        fHead.SetCurrentName ("mdn");
        ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL,   TBSTATE_INDETERMINATE);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
        EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
        if (ActiveSpezDlg->GetToolbar2 () != NULL)
        {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
        }
        return TRUE;
}

BOOL SpezDlg::OnKey12 ()
{
        syskey = KEY12;

        fWork->GetText ();
        FromForm (dbfields); 
        if (fHead.GetCfield ("kmdn")->IsDisabled ())
        {
               Enable (&fcForm, EnableHead, TRUE); 
               Enable (&fcForm, EnablePos,  FALSE);
               work.CommitWork ();
               work.InitRec ();
               ToForm (dbfields);

               fPos.SetText ();
               fHead.SetCurrentName ("mdn");
               ToolBar_SetState (ActiveDlg->GethwndTB (),   IDM_DELALL, TBSTATE_INDETERMINATE);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_LIST,     MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELETE,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      IDM_DELALL,   MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_INSERT,    MF_GRAYED);
               EnableMenuItem (ActiveDlg->GethMenu (),      VK_F10,       MF_GRAYED);
               if (ActiveSpezDlg->GetToolbar2 () != NULL)
               {
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("f6")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)ActiveSpezDlg->GetToolbar2 ()->GetCfield ("del")->GetFeld ())->bmp = 
					 ImageDelete.Inactive;
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Inactive;
                 if  (ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte") != NULL)
                 {
                      ActiveSpezDlg->GetToolbar2 ()->GetCfield ("Texte")->Enable (FALSE);
                 }
                 ActiveSpezDlg->GetToolbar2 ()->display ();
               }
        }
        return TRUE;
}


BOOL SpezDlg::OnKey10 (void)
{
      return FALSE;
}


BOOL SpezDlg::OnKeyDelete (void)
{
        return FALSE;
}

BOOL SpezDlg::OnKeyInsert ()
{
        return FALSE;
}


BOOL SpezDlg::OnKeyPrior ()
{
        return FALSE;
}

BOOL SpezDlg::OnKeyNext ()
{
        return FALSE;
}


BOOL SpezDlg::OnKey6 ()
{
        if (ListDlg != NULL)
        {
            return TRUE;
        }

        if (atoi (defaultlistf.mdn) == 0)
        {
            disp_mess ("Es wurde kein Mandant erfasst",2);
            fHead.SetCurrentName ("mdn");
            return TRUE;
        }

        if (work.GetMatrix () == ARTIKEL && atoi (defaultlistf.hwg) == 0)
        {
            disp_mess ("Es wurde kein Sortiment erfasst",2);
            fHead.SetCurrentName ("hwg");
            return TRUE;
        }


		LISTDLG::hwg = (short) atoi (defaultlistf.hwg);
        if (atoi (defaultlistf.kw) == 0)
        {
            disp_mess ("Es wurde keine Kalenderwoche erfasst erfasst",2);
            fHead.SetCurrentName ("kw");
            return TRUE;
        }

        DLG::OnMove (hMainWindow, WM_MOVE, 0, 0l);
        UpdateWindow (hMainWindow);
        fHead.GetText (); 
                 ((ColButton *)Toolbar2->GetCfield ("insert")->GetFeld ())->bmp = 
                     ImageInsert.Image;
        LISTDLG::work = &work; 
        ListDlg = new LISTDLG ((short) atoi (defaultlistf.mdn), 
                               (short) atoi (defaultlistf.hwg),
                               (short) atoi (defaultlistf.kw));
        ShowList ();
        ListDlg->SetWork (&work);
// Button, wenn vorhanden Initialisieren
        if (work.GetLsts () != NULL)
        {
//              work.GetLsts ()->choise [0] = NULL;
        }
// Liste ist schon vorhanden, Liste l�schen und dann Editiermodus mit ListEnter
//        ListDlg->DestroyWindows ();

// Liste ist schon vorhanden, Wechsel in Editiermodus 
/*
        if (DockList)
        {
              ListDlg->WorkList ();
        }
        else
        {
              ListDlg->EnterList ();
        }
*/

// Liste nicht vorhanden, Editiermodus mit EnterListe
        Enable (&fcForm, EnableHead, FALSE); 
        work.BeginWork ();
        ListDlg->EnterList ();
        Enable (&fcForm, EnableHead, TRUE); 

// Liste nach EnterList wieder anzeigen, wie in ShowList
//      if (DockList)
//        {
//              ListDlg->ShowList ();
//        }

        delete ListDlg;
        ListDlg = NULL;
        fHead.SetCurrentName ("mdn");
        DLG::OnMove (hMainWindow, WM_MOVE, 0, 0l);
        return TRUE;
} 


BOOL SpezDlg::OnKey7 ()
{
        return FALSE;
}


BOOL SpezDlg::ShowMdn (void)
{
             char mdn_nr [6];
             SEARCHMDN SearchMdn;
 
             SearchMdn.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
             SearchMdn.Setawin (ActiveDlg->GethMainWindow ());
             SearchMdn.Search ();
             if (SearchMdn.GetKey (mdn_nr) == TRUE)
             {
                 sprintf (defaultlistf.mdn, "%4d", atoi (mdn_nr));
                 work.GetMdnName (defaultlistf.mdn_krz,   atoi (defaultlistf.mdn));
                 fHead.SetText ();
                 fHead.SetCurrentName ("mdn");
             }
             return TRUE;
}


BOOL SpezDlg::ShowHwg (void)
{
             short hwg; 
//             short wg;
//             long ag;
             SEARCHHWG SearchHwg;
 
             SearchHwg.SetParams (DLG::hInstance, ActiveDlg->GethWnd ()); 
             SearchHwg.Setawin (ActiveDlg->GethMainWindow ());
                                                  
             SearchHwg.Search ();
             if (SearchHwg.GetKey (&hwg) == TRUE)
             {
                 sprintf (defaultlistf.hwg, "%d", hwg);
                 work.GetHwgBez (defaultlistf.hwg_bz1, hwg);
/*
                 wg = atoi (defaultlistf.wg);
                 work.TestWgHwg (&wg, atoi (defaultlistf.hwg));
                 sprintf (defaultlistf.wg, "%d", wg);
                 if (wg == 0)
                 {
                           strcpy (defaultlistf.wg_bz1, "");
                 }
                 ag = atol (defaultlistf.ag);
                 work.TestAgWg (&ag, wg);
                 sprintf (defaultlistf.ag, "%ld", ag);
                 if (ag == 0l)
                 { 
                       strcpy (defaultlistf.ag_bz1, "");
                 }
*/
                 fHead.SetText ();
                 fHead.SetCurrentName ("hwg");
             }
             return TRUE;
}


BOOL SpezDlg::ShowFil (void)
{
             char fil_nr [6];
             SEARCHFIL SearchFil;
 
             if (atoi (defaultlistf.mdn) == 0) return TRUE;

             SearchFil.SetParams (DLG::hInstance, ActiveDlg->GethWnd (), 
                                                  atoi (defaultlistf.mdn));
             SearchFil.Setawin (ActiveDlg->GethMainWindow ());
             SearchFil.Search ();
             if (SearchFil.GetKey (fil_nr) == TRUE)
             {
                 sprintf (defaultlistf.fil, "%4d", atoi (fil_nr));
                 work.GetFilName (defaultlistf.fil_krz,   atoi (defaultlistf.mdn), atoi (defaultlistf.fil));
                 fHead.SetText ();
                 fHead.SetCurrentName ("fil");
             }
             return TRUE;
}


BOOL SpezDlg::OnKey9 ()
{
         CFIELD *Cfield;
         HWND hWnd;

         syskey = KEY9;
         hWnd = GetFocus ();


         if (hWnd == fHead.GetCfield ("mdn_choise")->GethWnd ())
         {
             return ShowMdn ();
         }

         if (hWnd == fHead.GetCfield ("fil_choise")->GethWnd ())
         {
             return ShowFil ();
         }

         if (hWnd == fHead.GetCfield ("hwg_choise")->GethWnd ())
         {
             return ShowHwg ();
         }


         Cfield = ActiveDlg->GetCurrentCfield ();

         if (strcmp (Cfield->GetName (), "mdn") == 0)
         {
             return ShowMdn ();
         }

         if (strcmp (Cfield->GetName (), "fil") == 0)
         {
             return ShowFil ();
         }
         if (strcmp (Cfield->GetName (), "hwg") == 0)
         {
             return ShowHwg ();
         }
         return FALSE;
}


BOOL SpezDlg::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL SpezDlg::OnMove (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (hWnd != this->hMainWindow) return FALSE; 
        DLG::OnMove (hWnd,msg,wParam,lParam); 
        if (ListDlg != NULL)
        {
            ListDlg->MoveMamain1 ();
        }
        return FALSE;
}

BOOL SpezDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}

BOOL SpezDlg::ChangeDockList (void)
{
        if (DockList)
        {
            DockList = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_DOCKLIST,   MF_UNCHECKED);
        }
        else
        {
            DockList = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_DOCKLIST,   MF_CHECKED);
        }
        return TRUE;
}


BOOL SpezDlg::ChangeScrPrint (void)
{
        if (LISTDLG::ScrPrint)
        {
            LISTDLG::ScrPrint = FALSE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_SCRPRINT,   MF_UNCHECKED);
        }
        else
        {
            LISTDLG::ScrPrint = TRUE;
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_SCRPRINT,   MF_CHECKED);
        }
        return TRUE;
}


BOOL SpezDlg::ChangeMatrix (void)
{
        if (work.GetMatrix () == LIEFERANT)
        {
            work.SetMatrix (ARTIKEL);
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_MALIEF,   MF_UNCHECKED);
        }
        else
        {
            work.SetMatrix (LIEFERANT);
            CheckMenuItem (ActiveDlg->GethMenu (), IDM_MALIEF,   MF_CHECKED);
        }
        return TRUE;
}


BOOL SpezDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (ListDlg != NULL)
        {
              if (ListDlg->OnCommand (hWnd,msg,wParam,lParam))
              {
                  return TRUE;
              }
        }
        if (fWork)
        {

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }
              if (LOWORD (wParam) == IDM_CHOISE)
              {
                  return OnKey9 ();
              }

              if (LOWORD (wParam) == IDM_LIST)
              {
                  return OnKey6 ();
              }

              if (LOWORD (wParam) == IDM_PRINT)
              {
                  if (ListDlg != NULL)
                  {
                         return ListDlg->OnCommand (hWnd,msg,wParam,lParam);
                  }
              }

              if (LOWORD (wParam) == IDM_SCRPRINT)
              {
                  return ChangeScrPrint ();
              }

              if (LOWORD (wParam) == IDM_DOCKLIST)
              {
                  return ChangeDockList ();
              }

              if (LOWORD (wParam) == IDM_MALIEF)
              {
                  return ChangeMatrix ();
              }

              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
//                  return ListChanged (wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL SpezDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            ExitProcess (0);;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL SpezDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             PostQuitMessage (0);
             fcForm.destroy ();
        }
        return TRUE;
}

void SpezDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          cpfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}


void SpezDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void SpezDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void SpezDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}

HWND SpezDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
//          FillListCaption ();
          EnterPos ();
          return hWnd;
}

void SpezDlg::DestroyPos (void)
{
    fPos.destroy ();
    fcForm.SetFieldanz (fcForm.GetFieldanz () - 1);
}

void SpezDlg::SetPos (void)
{
    fcForm.SetFieldanz (2);
    fcForm0.Invalidate (TRUE);
    fcForm0.MoveWindow ();
    UpdateWindow (hWnd);
}

HWND SpezDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          EnableMenuItem (ActiveSpezDlg->GethMenu (),    IDM_DEL,   MF_GRAYED);

          Settchar ('|');
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
          EnterPos ();
//          ListDlg = new LISTDLG ();
          return hWnd;
}

void SpezDlg::ShowList (void)
{
          int DockY;
          RECT rect;

		  ListDlg->SetDlg (this);
          ListDlg->SetWork (&work);
          if (DockList == FALSE)
          {
                 ListDlg->SetWinStyle (ListDlg->GetWinStyle () | WS_CAPTION | 
                                                                 WS_SYSMENU |
                                                                 WS_MINIMIZEBOX |
                                                                 WS_MAXIMIZEBOX);
                ListDlg->SetDockWin (FALSE);
                ListDlg->SetDimension (750, 400);
                ListDlg->SetModal (TRUE);
          }
          else
          {
                if (DockMode)
                {
                   ListDlg->SetWinStyle (WS_THICKFRAME | WS_CHILD | WS_CLIPSIBLINGS);
                   Enable (&fcForm, EnablePos, TRUE); 
//                DestroyPos ();
                }
                GetWindowRect (hWnd, &rect);
                DockY = fPos.GetCfield ("pos_frame")->GetYP () + rect.top;
//                DockY = 309;
                ListDlg->SetDockY (DockY);
          }

          ListDlg->SethMenu (hMenu);
          ListDlg->SethwndTB (hwndTB);
          ListDlg->SethMainWindow (hWnd);
          ListDlg->SetTextMetric (&DlgTm);
          ListDlg->SetLineRow (100);
// Liste gleich anzeigen
//          if (DockList)
//          {
//               ListDlg->ShowList ();
//          }
}

void SpezDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void SpezDlg::CallInfo (void)
{
          DLG::CallInfo ();
}



void SpezDlg::SetListLines (int ListLines)
{
          this->ListLines = ListLines;
          if (ListClass::ActiveList != NULL)
          {
		      ListClass::ActiveList->SetListLines (ListLines);
          }
          LineSet         = TRUE;
}

void SpezDlg::SetListColors (COLORREF ListColor, COLORREF ListBkColor)
{
          this->ListColor   = ListColor;
          this->ListBkColor = ListBkColor;
          if (ListClass::ActiveList != NULL)
          {
		      ListClass::ActiveList->SetColors (ListColor,ListBkColor);
          }
          ColorSet          = TRUE;

}


HWND SpezDlg::SetToolCombo (int Id, int tbn1, int tbn2, int ComboPtr)
{
         HWND hwndCombo; 

		 hwndCombo = DLG::SetToolCombo (Id, tbn1, tbn2);

		 switch (ComboPtr)
		 {
		     case 0:
                   hwndCombo1 = hwndCombo;
				   break;
			 case 1:
                   hwndCombo2 = hwndCombo;
				   break;
		 }
         return hwndCombo;
}

void SpezDlg::SetComboTxt (HWND hwndCombo, char **Combo, int Select, int ComboPtr)
{

	  DLG::SetComboTxt (hwndCombo, Combo, Select);

 	  switch (ComboPtr)
	  {
		     case 0:
                   Combo1 = Combo;
				   break;
			 case 1:
                   Combo2 = Combo;
				   break;
	  }
}


BOOL SpezDlg::OnCloseUp (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	    if ((HWND) lParam == hwndCombo1)
		{
			ChoiseCombo1 ();
			return TRUE;
		}
		else if ((HWND) lParam == hwndCombo2)
		{
			ChoiseCombo2 ();
			return TRUE;
		}
		return FALSE;
}


void SpezDlg::ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   SetListLines (i);
                   break;
              }
          }
          if (ActiveDlg != NULL && 
              ActiveDlg->GetCurrentCfield ()->IsDisabled == FALSE)
          {
              ActiveDlg->SetCurrentFocus ();
          }
}

void SpezDlg::ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   SetListColors (Colors[i], BkColors[i]);
                   break;
              }
          }
          if (ActiveDlg != NULL && 
              ActiveDlg->GetCurrentCfield ()->IsDisabled == FALSE)
          {
              ActiveDlg->SetCurrentFocus ();
          }
}


void SpezDlg::ProcessMessages (void)
{
//          ShowList ();
          DLG::ProcessMessages ();
}


