#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "item.h"
#include "dbfunc.h"

struct DBITEM _item;


int ITEM_CLASS::lese_item (char *name)
/**
Tabelle Item.
**/
{
         if (cursor_item == -1)
         {
                    ins_quest (_item.name, 0, 19);
                    out_quest (_item.bezgkurz, 0, 4);
                    out_quest (_item.bezkurz, 0, 11);
                    out_quest (_item.bezlang, 0, 19);
                    out_quest (_item.itype, 0, 16);
                    out_quest (_item.iformat, 0, 17);
                    out_quest (_item.bereich, 0, 49);
                    out_quest (_item.vorbeleg, 0, 16);

                    cursor_item = prepare_sql
                               ("select bezgkurz, bezkurz, bezlang,"
                                "itype, iformat, bereich, vorbeleg "
                                "from item "
                                "where name = ?");

                    if (sqlstatus) return (-1);
         }
         strcpy (_item.name, name);
         clipped (_item.name);
         open_sql (cursor_item);
         fetch_sql (cursor_item);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int ITEM_CLASS::lese_item (void)
/**
Naechsten Satz aus Tabelle item lesen.
**/
{
         fetch_sql (cursor_item);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

void ITEM_CLASS::erase_item (void)
/**
Cursor freigeben.
**/
{
         if (cursor_item == -1) return;
         close_sql (cursor_item);
}

