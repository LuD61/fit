#ifndef MDNFILDEF
#define MDNFILDEF
#include "mdn.h"
#include "fil.h"

class MDNFIL : public MDN_CLASS, FIL_CLASS 
{
       private :
//            MDN_CLASS mdn_class;
//            FIL_CLASS fil_class;

            void SetMandantAttr (int);
            void SetFilialAttr (int);
       public :
            MDNFIL () : MDN_CLASS (),FIL_CLASS () 
            {
            } 
            int showmdn (void);
            int showfil (void);
            int GetMdn (void);
            int GetFil (void);
            int eingabemdnfil (HWND, char *, char *);
};
#endif
