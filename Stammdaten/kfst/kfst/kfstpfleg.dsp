# Microsoft Developer Studio Project File - Name="kfstpfleg" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=kfstpfleg - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "kfstpfleg.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "kfstpfleg.mak" CFG="kfstpfleg - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "kfstpfleg - Win32 Release" (basierend auf\
  "Win32 (x86) External Target")
!MESSAGE "kfstpfleg - Win32 Debug" (basierend auf\
  "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "kfstpfleg - Win32 Release"

# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Cmd_Line "NMAKE /f kfstpfleg.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "kfstpfleg.exe"
# PROP BASE Bsc_Name "kfstpfleg.bsc"
# PROP BASE Target_Dir ""
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Cmd_Line "NMAKE /f kfstpfleg.mak"
# PROP Rebuild_Opt "/a"
# PROP Target_File "kfstpfleg.exe"
# PROP Bsc_Name "kfstpfleg.bsc"
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "kfstpfleg - Win32 Debug"

# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Cmd_Line "NMAKE /f kfstpfleg.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "kfstpfleg.exe"
# PROP BASE Bsc_Name "kfstpfleg.bsc"
# PROP BASE Target_Dir ""
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Cmd_Line "NMAKE /f kfstpfleg.mak"
# PROP Rebuild_Opt "/a"
# PROP Target_File "kfstpfleg.exe"
# PROP Bsc_Name "kfstpfleg.bsc"
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "kfstpfleg - Win32 Release"
# Name "kfstpfleg - Win32 Debug"

!IF  "$(CFG)" == "kfstpfleg - Win32 Release"

!ELSEIF  "$(CFG)" == "kfstpfleg - Win32 Debug"

!ENDIF 

# Begin Source File

SOURCE=.\kfstpfleg.dsp
# End Source File
# End Target
# End Project
