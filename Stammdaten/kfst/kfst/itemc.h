#ifndef ITEMDEF
#define ITEMDEF
#include "stdio.h"
#include "stdarg.h"

class ITEM
{
   private :
		  int ittextnum;
          char itemname [21];
		  char itemtext[80];
	      virtual int GetDBItem (char *item);

    public :
        char *feld;
        ITEM (char *itname, char *value, char *ittext, int itnum)
          {

                feld =  value;
                ittextnum = itnum;
                this->itemname[0] = (char) 0;
                this->itemtext[0] = (char) 0;
                if (itname && strlen (itname))
                {
                         strcpy ((char *) itemname, itname);
                }
                if (ittext && strlen (ittext))
                {
                         strcpy ((char *) itemtext, ittext);
                }

				if (strcmp (itemname, " "))
                {
					     GetDBItem (itemname);
                }
          }

          void SetFeld (char *value)
          {
                strcpy (feld, value);
          }

          void SetFeldPtr (char *value)
          {
                feld = value;
          }

          char *GetFeld (char *value)
          {
                strcpy (value, feld);
                return value;
          } 
           
          char *GetFeldPtr (void)
          {
                return feld;
          } 

		  char *GetItemText (void)
          {
                 if (strlen (itemtext))
                 {	 
			               return itemtext;
                 }
				 return NULL;
		  }		 

		  char *GetItemName (void)
          {
			     if (strlen (itemname))
				 {	 
			               return itemname;
                 }
				 return NULL;
		  }		 
           
          void SetFormatFeld (char *format, ...);
          void PrintInfo (void);                 
          void PrintHelp (void);  
          virtual void CallInfo ();
};
#endif 
