#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <windows.h>
#include "strfkt.h"
#include "mo_send.h"

#define MAXTRIES 10000

#define DCS        1
#define MMS        2
#define SW_KASSE   3
#define ALPHADOS   4
#define SW_WAAGE   5
#define PHT        8
#define METTLER_L1 9
#define METTLER_L2 10
#define P8000      15
#define GX         16
#define CWS        26
#define HOST       31


static int dsqlstatus;

int CLASS_SEND::writelog (char * format, ...)
/**
Debugausgabe.
**/
{
          static int log_ok = 0;
          va_list args;
          FILE *logfile;
          char logtext [245];
          char logsatz [256];
          char datum [12];
          char zeit[12];
          long lpos;
          char *tmp;

          if (log_ok == 0)
          {
                   tmp = getenv ("TMPPATH");
                   if (tmp == (char *) 0)
                   {
                                return 0;
                   }
                   sprintf (logname, "%s\\bild160.log", tmp);
                   tmp = getenv ("LOGZEILEN");
                   if (tmp)
                   {
                                logzeilen = atol (tmp + 10);
                   }
                   log_ok = 1;
          }

          logfile = fopen (logname, "rb+");
          if (logfile == (FILE *) 0)
          {
                     logfile = fopen (logname, "wb");
                     if (logfile == (FILE *) 0) return 0;
                     lpos = 0l;
                     sprintf (logsatz, "%8ld", lpos + 1);
                     fprintf (logfile, "%-78.78s%s", logsatz, crnl);
                     fclose (logfile);
                     logfile = fopen (logname, "rb+");
                     if (logfile == (FILE *) 0) return 0;
          }

          fgets (logsatz, 20, logfile);
          lpos = atol (logsatz);
          lpos -= (long) 1;

          if (lpos >= logzeilen) lpos = 0l;

          va_start (args, format);
          vsprintf (logtext, format, args);
          va_end (args);

          sysdate (datum);
          systime (zeit);
          sprintf (logsatz, "%-10.10s %-5.5s  %-60.60s", datum, zeit,logtext);

          lpos += 1;
          fseek (logfile, lpos * (long) 80, 0);
          fprintf (logfile, "%s%s", logsatz, crnl);

          fseek (logfile, 0l, 0);
          sprintf (logsatz, "%8ld", lpos + 1);
          fprintf (logfile, "%-78.78s%s", logsatz, crnl);
          fclose (logfile);
          return 0;
}

         
void CLASS_SEND::write_160 (char *command)
/**
Kommando in Warteschreife bearbeiten.
**/
{
          static int statname_ok = 0;
          char status [40];
          FILE *stat;
          char *tmp;
          int count;


          if (statname_ok == 0)
          {
                   tmp = getenv ("SERV160");
                   if (tmp == (char *) 0)
                   {
                                0;
                   }
                   sprintf (statname, "%s\\mess160.sta", tmp);
                    
                   statname_ok = 1;
          }
          count = 0;
          while (TRUE)
          {
                   stat = NULL;
                   stat = fopen (statname, "r");
                   if (stat)
                   {
                           status [0] = (char) 0;
                           fgets (status, 39, stat);
                           if (status[0] != '*')
                           {
                                  fclose (stat);
                           }
                           else
                           {
                                  break;
                           }
                   }
                   else
                   {
                           stat = fopen (statname, "w");
                           if (stat) break;
                   }
                   count ++;
                   if (MAXTRIES && count >= MAXTRIES) return;
                   Sleep (100);
          }

          cr_weg (command);
          if (stat) fclose (stat);
          stat = fopen (statname, "w");
          if (stat == (FILE *) 0)
          {
                   return;
          }

          fprintf (stat, "%s\n", command);
          fclose (stat);
          writelog ("Bearbeite %s", command);
          return;
}
