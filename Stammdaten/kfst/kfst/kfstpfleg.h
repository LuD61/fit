#include "mo_meld.h"
/////////////////////////////////////////////////////////////////////////////
//
// Bitmap
//

FTOOLBAR                BITMAP  MOVEABLE PURE   "Toolbar.bmp"

/////////////////////////////////////////////////////////////////////////////
//
// Toolbar
//

FTOOLBAR TOOLBAR DISCARDABLE  16, 15
BEGIN
    BUTTON      IDM_WORK
    BUTTON      IDM_SHOW
    BUTTON      IDM_DEL
    BUTTON      IDM_INFO
    SEPARATOR
    BUTTON      IDM_INFO
    BUTTON      IDM_INFO
    BUTTON      IDM_INFO
    BUTTON      IDM_TEXT
    BUTTON      IDM_ETI
END
