#ifndef _ATST_DEF
#define _ATST_DEF
#include "dbclass.h"
struct ATST {
   long      text_nr;
   char      txt1[71];
   char      txt2[71];
   char      txt3[71];
   char      txt4[71];
   char      txt5[71];
   char      txt6[71];
   char      txt7[71];
   char      txt8[71];
   char      txt9[71];
   char      txt10[71];
   char      txt11[71];
   char      txt12[71];
   char      txt13[71];
   char      txt14[71];
   short     sg1;
   short     sg2;
   short     sg3;
   short     sg4;
   short     sg5;
   short     sg6;
   short     sg7;
   short     sg8;
   short     sg9;
   short     sg10;
   short     sg11;
   short     sg12;
   short     sg13;
   short     sg14;
};
extern struct ATST atst, atst_null;

#line 5 "atst.rh"

class ATST_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
               int prep_awcursor (void);
               int prep_awcursor (char *);
               void fill_aw (int);
               int CallDbAuswahl (int);
               void TestQueryTxt (char *, char *);
               void TestQueryTxtn (char *);

       public :
               ATST_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
               int ShowAllBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND hWnd, int ws_flag, char *);
               int QueryBu (HWND hWnd, int ws_flag);
               void ReadQuery (form *, char *[]);
               int PrepareQuery (char *);
               int PrepareQuery (form *, char *[]);
               int ShowAllBu (HWND hWnd, int, int, 
                              int (*) (void), void (*) (int),
                              char *, int,  char *, int,
                              form *, form *, form *);  
};
#endif