#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "message.h"
#include "kfst.h"
#include "atst.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "dbfunc.h"
#include "mdnfil.h"
#include "ptab.h"
#include "mo_menu.h"
#include "mo_dbspez.h"
#include "eti_typ.h"
#include "sys_peri.h"
#include "mo_send.h"


#define IDM_SEND            108

static int dsqlstatus;

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
static int ProcessMessages(void);
void     InitFirstInstance(HANDLE);
BOOL     InitNewInstance(HANDLE, int);

static int mit_trans = 1;
static int page;

static HANDLE  hMainInst;
HWND    hMainWindow;
HWND    mamain1;
HWND    mamain2 = NULL; 
extern HWND  ftasten;
extern HWND  ftasten2;
static HICON hIcon;

static char *TXT_WORK   = "Bearbeiten";
static char *TXT_SHOW   = "Anzeigen";
static char *TXT_DEL    = "L�schen";
static char *TXT_PRINT  = "Drucken";

static char *TXT_ACTIVE = TXT_WORK;


static int IDM_ACTIVE = IDM_WORK;

HMENU hMenu;

struct PMENUE dateimen[] = {
	                        "&1 Bearbeiten", "C", NULL, IDM_WORK, 
						    "&3 L�schen",    " ", NULL, IDM_DEL,
						    "&4 Drucken",    " ", NULL, IDM_PRINT,
							"",              "S", NULL, 0, 
						    "&5 Senden",     " ", NULL, IDM_SEND,
							"",              "S", NULL, 0, 
	                        "B&eenden", " ", NULL, KEY5,
						     NULL, NULL, NULL, 0};

struct PMENUE bearbmen[] = {
	                        "&Funktion f�r aktives Eingaefeld F2",  " ", NULL, KEY2, 
                            "&Text in Zutaten ersetzen",  " ", NULL, IDM_TEXT,
                            "&Etiketten-Typ bearbeiten",  " ", NULL, IDM_ETI, 
 NULL, NULL, NULL, 0};

struct PMENUE menuetab[] = {"&Datei",      "M", dateimen, 0, 
                            "&Bearbeiten", "M", bearbmen, 0, 
						     NULL, NULL, NULL, 0};



extern HWND hWndToolTip;
HWND hwndTB;

static TBBUTTON tbb[] =
{
 0,               IDM_WORK,   TBSTATE_ENABLED |TBSTATE_CHECKED, 
                               TBSTYLE_CHECKGROUP, 
 0, 0, 0, 0,
 /*
 1,               IDM_SHOW,   TBSTATE_ENABLED, 
	                          TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 */
 2,               IDM_DEL,    TBSTATE_ENABLED, 
                              TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 3,               IDM_PRINT, TBSTATE_INDETERMINATE, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
/*
 3,               IDM_PRINT,  TBSTATE_ENABLED, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 */
 0, 0,            TBSTATE_ENABLED,
                  TBSTYLE_SEP, 
 0, 0, 0, 0,
 4,               IDM_INFO,   TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 5,               KEYPGD, TBSTATE_INDETERMINATE, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 6,               KEYPGU, TBSTATE_INDETERMINATE, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 
 7,               IDM_TEXT, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 8,               IDM_ETI, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
};

static char *qInfo [] = {"Bearbeiten",
                         "Anzeigen",
                         "L�schen",
                         "Drucken",
                         "Funktion f�r aktives Eingabefeld",
                         "n�chste Seite",
                         "vorhergehende Seite",
                         "Allgemeine Texte bearbeiten",
                         "Etiketten-Typ bearbeiten", 
                         0};

static UINT qIdfrom [] = {IDM_WORK, IDM_SHOW, IDM_DEL,
                         IDM_PRINT, IDM_INFO,
                         KEYPGD, KEYPGU,IDM_TEXT, IDM_ETI, 0};
static char *qhWndInfo [] = {""};
static HWND *qhWndFrom [] = {NULL}; 

/*
static char *qhWndInfo [] = {"Auswahl Drucker"};
static HWND *qhWndFrom [] = {&hWndCombo, NULL}; 
*/

ETI_TYP_CLASS eti_typ_class;
ATST_CLASS atst_class;
KFST_CLASS kfst_class;
MENUE_CLASS menue_class;
PTAB_CLASS ptab_class;
DBSPEZ_CLASS dbspez_class;
SYS_PERI_CLASS sys_pclass;

MELDUNG mess;
CLASS_SEND class_send ((long) 10000, (char *) "\15\12");

struct SGTAB
{
	      short *sg;
		  short *ze;
};


struct SGTAB sgtab[20] = 
{
	                      &eti_typ.sg01, &eti_typ.ze01,
                          &eti_typ.sg02, &eti_typ.ze02,
                          &eti_typ.sg03, &eti_typ.ze03,
                          &eti_typ.sg04, &eti_typ.ze04,
                          &eti_typ.sg05, &eti_typ.ze05,
                          &eti_typ.sg06, &eti_typ.ze06,
                          &eti_typ.sg07, &eti_typ.ze07,
                          &eti_typ.sg08, &eti_typ.ze08,
                          &eti_typ.sg09, &eti_typ.ze09,
                          &eti_typ.sg10, &eti_typ.ze10,
                          &eti_typ.sg11, &eti_typ.ze11,
                          &eti_typ.sg12, &eti_typ.ze12,
                          &eti_typ.sg13, &eti_typ.ze13,
                          &eti_typ.sg14, &eti_typ.ze14,
                          &eti_typ.sg15, &eti_typ.ze15,
                          &eti_typ.sg16, &eti_typ.ze16,
                          &eti_typ.sg17, &eti_typ.ze17,
                          &eti_typ.sg18, &eti_typ.ze18,
                          &eti_typ.sg19, &eti_typ.ze19,
                          &eti_typ.sg20, &eti_typ.ze20
};


static char text_nr [10];

static char stext1[] = {"Kopfzeilen"};
static char stext2[] = {"Fusszeilen"};
static char *stext = stext1;


static ITEM isortl ("", "1", "", 0);

ITEM itxttext  ("", "Text-Nr",    "", 0);

ITEM itxtval   ("text_nr",   text_nr,"", 0);

// ITEM istext    ("", stext,   "", 0);

 

static int fil_par = 0;


static int setkey9txt (void);

static int (*BearbProc) (void) = NULL;

static field _etitext [] = {
&itxttext,    12, 0, 1, 5, 0, "",    DISPLAYONLY, 0, 0,0,
&itxtval,      9, 0, 2, 6, 0, "%8d", NORMAL,    setkey9txt, 0,0,
};


static form etitext= {2, 0,0, _etitext, 0, 0, 0, 0, NULL};

mfont kfstfont = {"Arial", 90, 100, 0, RGB (255, 255, 255),
                                       RGB (0, 0, 255),
                                       0,
                                       NULL};

HBITMAP   bKopf;
HBITMAP   bKopfu;
HBITMAP   bBrief;
HBITMAP   bBriefu;
HBITMAP   bFuss;

ColButton Ckfst = { stext1, -1, -1,
                    NULL, 0, 0,
                    NULL, 0, 0,
                    bKopf,4, 8,
                    NULL, 0, 0,
                    RGB (255, 255, 255),
                    BLUECOL,
                    0};

ColButton CSend = { "Senden", -1, -1,
                    NULL, 0, 0,
                    NULL, 0, 0,
                    bBrief, 8, 13,
                    NULL, 0, 0,
                    RGB (255, 255, 255),
                    BLUECOL,
                    0};

ITEM istext    ("", (char *) &Ckfst,   "", 0);
ITEM isend    ("",  (char *) &CSend,   "", 0);

static field _kfsttext [] = {
&istext,      25, 3, 1,49, 0, "",    COLBUTTON, 0, 0, KEYPGD,
&isend,       25, 3, 1,76, 0, "",    COLBUTTON, 0, 0, IDM_SEND,
};


static form kfsttext= {2, 0,0, _kfsttext, 0, 0, 0, 0, &kfstfont};

/* ************************************************************
Maskenvariablen fuer Texte
Seite 1
*/

static char max_zei_pos [4];
static char sg_std [5];
static char sg [14][5];
static char ze [14][5];
static char txt[14][72];
static char txtbuffer[20][72];
static int txt_active = 0;

static int setkey9sg (void);
static  int testsg (void);
static  int testsg2 (void);

ColButton Ctxtsg = {  "SG", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Ctxtze = {  "ZE", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Ctxttxt = { "Text", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};


ITEM iLine   ("", "1",  "", 0);
/*
ITEM itxtsg  ("", "SG",   "", 0);
ITEM itxtze  ("", "Ze", "", 0);
ITEM itxttxt ("", "Text",           "", 0);
*/

ITEM itxtsg  ("", (char *) &Ctxtsg,   "", 0);
ITEM itxtze  ("", (char *) &Ctxtze,   "", 0);
ITEM itxttxt ("", (char *) &Ctxttxt,   "", 0);

ITEM isg0  ("", sg[0], "", 0);    
ITEM isg1  ("", sg[1], "", 0);    
ITEM isg2  ("", sg[2], "", 0);    
ITEM isg3  ("", sg[3], "", 0);    
ITEM isg4  ("", sg[4], "", 0);    
ITEM isg5  ("", sg[5], "", 0);    
ITEM isg6  ("", sg[6], "", 0);    
ITEM isg7  ("", sg[7], "", 0);    
ITEM isg8  ("", sg[8], "", 0);    
ITEM isg9  ("", sg[9], "", 0);    
ITEM isg10 ("", sg[10], "", 0);    
ITEM isg11 ("", sg[11], "", 0);    
ITEM isg12 ("", sg[12], "", 0);    
ITEM isg13 ("", sg[13], "", 0);    
ITEM isg14 ("", sg[14], "", 0);    

ITEM ize0  ("", ze[0], "", 0);    
ITEM ize1  ("", ze[1], "", 0);    
ITEM ize2  ("", ze[2], "", 0);    
ITEM ize3  ("", ze[3], "", 0);    
ITEM ize4  ("", ze[4], "", 0);    
ITEM ize5  ("", ze[5], "", 0);    
ITEM ize6  ("", ze[6], "", 0);    
ITEM ize7  ("", ze[7], "", 0);    
ITEM ize8  ("", ze[8], "", 0);    
ITEM ize9  ("", ze[9], "", 0);    
ITEM ize10 ("", ze[10], "", 0);    
ITEM ize11 ("", ze[11], "", 0);    
ITEM ize12 ("", ze[12], "", 0);    
ITEM ize13 ("", ze[13], "", 0);    
ITEM ize14 ("", ze[14], "", 0);    
	
ITEM itxt0  ("", txt[0], "", 0);    
ITEM itxt1  ("", txt[1], "", 0);    
ITEM itxt2  ("", txt[2], "", 0);    
ITEM itxt3  ("", txt[3], "", 0);    
ITEM itxt4  ("", txt[4], "", 0);    
ITEM itxt5  ("", txt[5], "", 0);    
ITEM itxt6  ("", txt[6], "", 0);    
ITEM itxt7  ("", txt[7], "", 0);    
ITEM itxt8  ("", txt[8], "", 0);    
ITEM itxt9  ("", txt[9], "", 0);    
ITEM itxt10 ("", txt[10], "", 0);    
ITEM itxt11 ("", txt[11], "", 0);    
ITEM itxt12 ("", txt[12], "", 0);    
ITEM itxt13 ("", txt[13], "", 0);    
ITEM itxt14 ("", txt[14], "", 0);    

static field _txt_ub[] = {
&itxtsg,     4, 0, 0, 1, 0, "", COLBUTTON, 0, 0, 0,
&itxtze,     3, 0, 0, 5, 0, "", COLBUTTON, 0, 0, 0,
&itxttxt,   68, 0, 0, 8, 0, "", COLBUTTON, 0, 0, 0,
};

static form txt_ub = {3, 0, 0, _txt_ub, 0, 0, 0, 0, NULL};

static field _txt_data[] = {
&isg0,       3, 0, 1, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&ize0,       3, 0, 1, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt0,     70, 0, 1, 8,  0, "",    NORMAL, 0, 0, 0,

&isg1,       3, 0, 2, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&ize1,       3, 0, 2, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt1,     70, 0, 2, 8,  0, "", NORMAL, 0, 0, 0,

&isg2,       3, 0, 3, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&ize2,       3, 0, 3, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt2,     70, 0, 3, 8,  0, "", NORMAL, 0, 0, 0,

&isg3,       3, 0, 4, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&ize3,       3, 0, 4, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt3,     70, 0, 4, 8,  0, "", NORMAL, 0, 0, 0,

&isg4,       3, 0, 5, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&ize4,       3, 0, 5, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt4,     70, 0, 5, 8,  0, "", NORMAL, 0, 0, 0,

&isg5,       3, 0, 6, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&ize5,       3, 0, 6, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt5,     70, 0, 6, 8,  0, "", NORMAL, 0, 0, 0,

&isg6,       3, 0, 7, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&ize6,       3, 0, 7, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt6,     70, 0, 7, 8,  0, "", NORMAL, 0, 0, 0,

&isg7,       3, 0, 8, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&ize7,       3, 0, 8, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt7,     70, 0, 8, 8,  0, "", NORMAL, 0, 0, 0,

&isg8,       3, 0, 9, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&ize8,       3, 0, 9, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt8,     70, 0, 9, 8,  0, "",    NORMAL, 0, 0, 0,


&ize9 ,      3, 0,10, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&isg9 ,      3, 0,10, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt9 ,    70, 0,10, 8,  0, "",    NORMAL, 0, 0, 0,

&ize10,      3, 0,11, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&isg10,      3, 0,11, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt10,    70, 0,11, 8, 0,  "",    NORMAL, 0, 0, 0,

&ize11,      3, 0,12, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&isg11,      3, 0,12, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt11,    70, 0,12, 8,  0,  "",   NORMAL, 0, 0, 0,

&ize12,      3, 0,13, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&isg12,      3, 0,13, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt12,    70, 0,13, 8,  0,  "",    NORMAL, 0, 0, 0,
// &iLine,      1,18, 1, 4,  0, "VLINE", DISPLAYONLY, 0, 0, 0,   

&ize13,      3, 0,14, 1,  0, "%2d", NORMAL, setkey9sg, testsg, 0, 	
&isg13,      3, 0,14, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt13,    70, 0,14, 8,  0,  "",   NORMAL, 0, 0, 0,
};

static form txt_data = {39, 0, 0, _txt_data, 0, 0, 0, 0, NULL};


/* ************************************************************
Ende Maskenvariablen fuer Texte Seite 1
*/

/* ************************************************************
Maskenvariablen fuer Texte
Seite 2
*/

static char sg2 [14][5];
static char ze2 [14][5];
static char txt2[14][72];
static char txtbuffer2[14][72];


ITEM isg0_2  ("", sg2[0], "", 0);    
ITEM isg1_2  ("", sg2[1], "", 0);    
ITEM isg2_2  ("", sg2[2], "", 0);    
ITEM isg3_2  ("", sg2[3], "", 0);    
ITEM isg4_2  ("", sg2[4], "", 0);    
ITEM isg5_2  ("", sg2[5], "", 0);    
ITEM isg6_2  ("", sg2[6], "", 0);    
ITEM isg7_2  ("", sg2[7], "", 0);    
ITEM isg8_2  ("", sg2[8], "", 0);    
ITEM isg9_2  ("", sg2[9], "", 0);    
ITEM isg10_2 ("", sg2[10], "", 0);    
ITEM isg11_2 ("", sg2[11], "", 0);    
ITEM isg12_2 ("", sg2[12], "", 0);    
ITEM isg13_2 ("", sg2[13], "", 0);    
ITEM isg14_2 ("", sg2[14], "", 0);    

ITEM ize0_2  ("", ze2[0], "", 0);    
ITEM ize1_2  ("", ze2[1], "", 0);    
ITEM ize2_2  ("", ze2[2], "", 0);    
ITEM ize3_2  ("", ze2[3], "", 0);    
ITEM ize4_2  ("", ze2[4], "", 0);    
ITEM ize5_2  ("", ze2[5], "", 0);    
ITEM ize6_2  ("", ze2[6], "", 0);    
ITEM ize7_2  ("", ze2[7], "", 0);    
ITEM ize8_2  ("", ze2[8], "", 0);    
ITEM ize9_2  ("", ze2[9], "", 0);    
ITEM ize10_2 ("", ze2[10], "", 0);    
ITEM ize11_2 ("", ze2[11], "", 0);    
ITEM ize12_2 ("", ze2[12], "", 0);    
ITEM ize13_2 ("", ze2[13], "", 0);    
ITEM ize14_2 ("", ze2[14], "", 0);    
	
ITEM itxt0_2  ("", txt2[0], "", 0);    
ITEM itxt1_2  ("", txt2[1], "", 0);    
ITEM itxt2_2  ("", txt2[2], "", 0);    
ITEM itxt3_2  ("", txt2[3], "", 0);    
ITEM itxt4_2  ("", txt2[4], "", 0);    
ITEM itxt5_2  ("", txt2[5], "", 0);    
ITEM itxt6_2  ("", txt2[6], "", 0);    
ITEM itxt7_2  ("", txt2[7], "", 0);    
ITEM itxt8_2  ("", txt2[8], "", 0);    
ITEM itxt9_2  ("", txt2[9], "", 0);    
ITEM itxt10_2 ("", txt2[10], "", 0);    
ITEM itxt11_2 ("", txt2[11], "", 0);    
ITEM itxt12_2 ("", txt2[12], "", 0);    
ITEM itxt13_2 ("", txt2[13], "", 0);    
ITEM itxt14_2 ("", txt2[14], "", 0);    

static field _txt_data2[] = {
&isg0_2,       3, 0, 1, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&ize0_2,       3, 0, 1, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt0_2,     70, 0, 1, 8,  0, "",    NORMAL, 0, 0, 0,

&isg1_2,       3, 0, 2, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&ize1_2,       3, 0, 2, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt1_2,     70, 0, 2, 8,  0, "", NORMAL, 0, 0, 0,

&isg2_2,       3, 0, 3, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&ize2_2,       3, 0, 3, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt2_2,     70, 0, 3, 8,  0, "", NORMAL, 0, 0, 0,

&isg3_2,       3, 0, 4, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&ize3_2,       3, 0, 4, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt3_2,     70, 0, 4, 8,  0, "", NORMAL, 0, 0, 0,

&isg4_2,       3, 0, 5, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&ize4_2,       3, 0, 5, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt4_2,     70, 0, 5, 8,  0, "", NORMAL, 0, 0, 0,

&isg5_2,       3, 0, 6, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&ize5_2,       3, 0, 6, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt5_2,     70, 0, 6, 8,  0, "", NORMAL, 0, 0, 0,

&isg6_2,       3, 0, 7, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&ize6_2,       3, 0, 7, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt6_2,     70, 0, 7, 8,  0, "", NORMAL, 0, 0, 0,

&isg7_2,       3, 0, 8, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&ize7_2,       3, 0, 8, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt7_2,     70, 0, 8, 8,  0, "", NORMAL, 0, 0, 0,

&isg8_2,       3, 0, 9, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&ize8_2,       3, 0, 9, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt8_2,     70, 0, 9, 8,  0, "",    NORMAL, 0, 0, 0,


&ize9_2 ,      3, 0,10, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&isg9_2 ,      3, 0,10, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt9_2 ,    70, 0,10, 8,  0, "",    NORMAL, 0, 0, 0,

&ize10_2,      3, 0,11, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&isg10_2,      3, 0,11, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt10_2,    70, 0,11, 8, 0,  "",    NORMAL, 0, 0, 0,

&ize11_2,      3, 0,12, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&isg11_2,      3, 0,12, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt11_2,    70, 0,12, 8,  0,  "",   NORMAL, 0, 0, 0,

&ize12_2,      3, 0,13, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&isg12_2,      3, 0,13, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt12_2,    70, 0,13, 8,  0,  "",    NORMAL, 0, 0, 0,
// &iLine,      1,18, 1, 4,  0, "VLINE", DISPLAYONLY, 0, 0, 0,   

&ize13_2,      3, 0,14, 1,  0, "%2d", NORMAL, setkey9sg, testsg2, 0, 	
&isg13_2,      3, 0,14, 5,  0, "%2d", READONLY, 0, 0, 0, 	
&itxt13_2,    70, 0,14, 8,  0,  "",   NORMAL, 0, 0, 0,
};

static form txt_data2 = {39, 0, 0, _txt_data2, 0, 0, 0, 0, NULL};


/* ************************************************************
Ende Maskenvariablen fuer Texte Seite 2
*/

static int querytxt (void);
static int setkey9ptab (void);
static int testptab (void);

static char mdn [10];
static char fil [10];

int dokey5 ()
/**
Aktion bei Taste F5
**/
{
        break_enter ();
		if (IDM_ACTIVE == IDM_WORK ||
			IDM_ACTIVE == IDM_DEL)
		{
                    rollbackwork ();
		}
        return 1;
}

char vsys [41];

static ITEM vOK     ("",  "   OK    ",  "", 0);
static ITEM vCancel ("",  "Abbrechen", "", 0);

static ITEM vSysTxt    ("", "Gr�te-Nr...:", "", 0);
static ITEM vSys       ("", vsys, "", 0);

static field _fsys [] = {
&vSysTxt,     12, 0,  1, 2,  0, "", DISPLAYONLY, 0, 0, 0,
&vSys,        40,10,  1,16,  0, "", COMBOBOX | COMBOLIST, 0, 0, 0,     
&vOK,         15, 0,  4,12,  0, "", BUTTON, 0, 0, KEY12,
&vCancel,     15, 0,  4,29,  0, "", BUTTON, 0, 0, KEY5, 
}
;

static char systab [300][41];

static combofield  cbfield[] = {1, 41, 0, (char *) systab,
                                0,   0, 0, NULL};


static form fsys = {4, 0, 0, _fsys, 0, 0, 0, cbfield, NULL}; 

int eBreak (void)
{
       syskey = KEY5;
       break_enter ();
       return 0;
}

int eOk (void)
{
       syskey = KEY12;

       break_enter ();
       return 0;
}

int SysOK (void)
{
    static int systab[] = {3,5,13,29,30 0};
    int i;

    for (i = 0; systab[i]; i ++)
    {
        if (sys_peri.peri_typ == systab[i])
        {
            return TRUE;
        }
    }
    return FALSE;
}

void FillBox (HWND hSys)
{
       int i;
       int dsqlstatus;
       char txt [21];

       i = 0;
       dsqlstatus = sys_pclass.lese_sys_peri ("order by sys"); 
       while (dsqlstatus == 0) 
       {
               if (SysOK ())
               {
                       if (strlen (sys_peri.txt) > 20)
                       {
                           memcpy (txt, sys_peri.txt, 20);
                           txt[20] = (char) 0;
                       }
                       else
                       {
                           strcpy (txt, sys_peri.txt);
                       }
                       sprintf (systab[i], "%8ld %s", sys_peri.sys,
                                                      txt);
                       i ++;
               }
               dsqlstatus = sys_pclass.lese_sys_peri ();
       }
       sprintf (systab[i], "%8ld %s", 0l, "Alle Ger�te");
       cbfield[0].cbanz = i + 1; 
       strcpy (vsys, systab[0]);
}
 

void SendToPeri (void)
/**
Daten an Geraete senden.
**/
{
        char command[512];
        static int BoxOk = 0;
        int cuf;
        form *cu;
        HWND hSys;
        HWND AkthWnd;

        AkthWnd = AktivWindow;
        cu = current_form;
        cuf = currentfield;
        save_fkt (5);
        save_fkt (12);

        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        break_end ();
        DisablehWnd (hMainWindow);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_DLGFRAME);
        hSys = OpenWindowChC (6, 60, 10, 10, hMainInst,
                "");

        if (BoxOk == 0)
        {
                 FillBox (hSys);
                 BoxOk = 1;
        }

        enter_form (hSys, &fsys, 0, 0);
        DestroyFrmWindow ();
        hSys = NULL;
        AktivWindow = mamain1;
        current_form = cu;
        currentfield = cuf;
        restore_fkt (5);
        restore_fkt (12);
        if (syskey == KEY5) return;

        wsplit (fsys.mask[1].item->GetFeldPtr (), " ");
        sys_peri.sys = atoi (wort[0]);
        sprintf (command, "s %ld 3 0 0 0", sys_peri.sys);
        class_send.write_160 (command);
        if (sys_peri.sys)
        {
               print_mess (1, "�betragung an Ger�t %ld gestartet", 
                           sys_peri.sys);
        }
        else
        {
               print_mess (1, "�betragung an alle Ger�t gestartet"); 
        }
}


int GetSgPos (int pos)
/**
Position in sgtab ohne 0-Eintraege holen.
**/
{
	    int i, z;

		for (i = 0, z = 0; i < 20; i ++)
        {
		    if (*sgtab[i].sg == 0) continue;
			if (z == pos) return (i);
			z ++;
        }
		return (-1);
}



static int showsg ()
/**
Moegliche Schriftgroessen anzeigen.
**/
{
	 
	     int i;   
		 int pos;
         char buffer [256];
		 form *scurrent;


         static field _fsort_awl[] = {
         &isortl,          1, 1, 0,  8, 0, "", NORMAL, 0, 0, 0,
         &isortl,          1, 1, 0, 20, 0, "", NORMAL, 0, 0, 0,
		 };
         static form fsort_awl = {2, 0, 0, 
			                      _fsort_awl, 0, 0, 0, 0, NULL};


		 save_fkt (5);
		 scurrent = current_form;
         SaveMenue ();
         InitMenue ();
		 SetHLines (0);
		 SetVLines (1);
         OpenListWindowBu (10, 40, 8, 20, 1);
         sprintf (buffer, "%8s %s",
			              "SG",
						  "Zeichen");
         InsertListUb (buffer);
         ElistVl (&fsort_awl);
		 for (i = 0; i < 20; i ++)
         {
			 if (*sgtab[i].sg > 0)
             {
				     sprintf (buffer, "%8d %d",
						              *sgtab[i].sg,
									  *sgtab[i].ze);
                     InsertListRow (buffer);
			 }
         }
         i = EnterQueryListBox ();
         i = GetSgPos (i);
         RestoreMenue ();
         SetLbox ();
         SetFocus (txt_data.mask[currentfield].feldid);
		 if (i == -1) 
		 {
		          restore_fkt (5);
		          current_form = scurrent;
		          display_form (mamain2, current_form, 0, 0);
			      return -1;
		 }
		 pos = currentfield; 
 	     sprintf (txt_data.mask[pos].item->GetFeldPtr (), 
                  "%hd", *sgtab[i].sg);
	     sprintf (txt_data.mask[pos + 1].item->GetFeldPtr (),
 	              "%hd", *sgtab[i].ze);
		 display_form (mamain2, &txt_data, 0, 0);
         
		 restore_fkt (5);
		 current_form = scurrent;
         return i;
}

static int showsg2 ()
/**
Moegliche Schriftgroessen anzeigen.
**/
{
	 
	     int i;   
		 int pos;
         char buffer [256];
		 form *scurrent;


         static field _fsort_awl[] = {
         &isortl,          1, 1, 0,  8, 0, "", NORMAL, 0, 0, 0,
         &isortl,          1, 1, 0, 20, 0, "", NORMAL, 0, 0, 0,
		 };
         static form fsort_awl = {2, 0, 0, 
			                      _fsort_awl, 0, 0, 0, 0, NULL};


		 save_fkt (5);
		 scurrent = current_form;
         SaveMenue ();
         InitMenue ();
		 SetHLines (0);
		 SetVLines (1);
         OpenListWindowBu (10, 40, 8, 20, 1);
         sprintf (buffer, "%8s %s",
			              "SG",
						  "Zeichen");
         InsertListUb (buffer);
         ElistVl (&fsort_awl);
		 for (i = 0; i < 20; i ++)
         {
			 if (*sgtab[i].sg > 0)
             {
				     sprintf (buffer, "%8d %d",
						              *sgtab[i].sg,
									  *sgtab[i].ze);
                     InsertListRow (buffer);
			 }
         }
         i = EnterQueryListBox ();
         i = GetSgPos (i);
         RestoreMenue ();
         SetLbox ();
         SetFocus (txt_data2.mask[currentfield].feldid);
		 if (i == -1) 
		 {
		          restore_fkt (5);
		          current_form = scurrent;
		          display_form (mamain2, current_form, 0, 0);
			      return -1;
		 }
		 pos = currentfield; 
 	     sprintf (txt_data2.mask[pos].item->GetFeldPtr (), 
                  "%hd", *sgtab[i].sg);
	     sprintf (txt_data2.mask[pos + 1].item->GetFeldPtr (),
 	              "%hd", *sgtab[i].ze);
		 display_form (mamain2, &txt_data2, 0, 0);
         
		 restore_fkt (5);
		 current_form = scurrent;
         return i;
}


static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                case KEY12 :
                       break_enter ();
                       return 1;
        }
        return 0;
}


static int querytxt ()
/**
Query ueber Textnummer.
**/
{

        static char ltext_nr [41];
        static char leti [41];
        static char ltxtn [41];

        static ITEM itxtval  ("text_nr", ltext_nr, "Text-Nr.......:", 0);
        static ITEM itxtnval ("txt1",    ltxtn,    "Text..........:", 0);


        static field _qtxtform[] = {
           &itxtval,      40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &itxtnval,     40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
		};

        static form qtxtform = {2, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"text_nr", "txtn"};

        HWND query;
		int savefield;
		form *savecurrent;
        
		save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
        SetFkt (6, leer, NULL);
        set_fkt (NULL, 6);
        SetFkt (7, leer, NULL);
        set_fkt (NULL, 7);
        SetFkt (9, leer, NULL);
        set_fkt (NULL, 9);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (mamain1);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU);
        query = OpenWindowChC (5, 60, 11, 10, hMainInst,
                               "Suchkriterien f�r Texte");
        enter_form (query, &qtxtform, 0, 0);

        DestroyWindow (query);
		SetAktivWindow (mamain1);
        if (syskey == KEY12)
        {
                   kfst_class.ReadQuery (&qtxtform, qnamen);

                   if (kfst_class.QueryBu (mamain1, 0) == 0)
                   {
                                 sprintf (text_nr, "%ld", 
									      kfst.text_nr);
                                 break_enter ();
                   }
        }

		currentfield = savefield;
		restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (9);
        display_form (mamain1, current_form, 0, 0);
	    SetCurrentFocus (currentfield);
		syskey = 1;
        return 0;
}


int SetBed (void)
/**
Verknuepfung zu allgemeinem Text setzen.
**/
{
         int i;
         char buffer [10];

         i = currentfield;
         if ((i % 3) == 0)
         {
             i += 2;
         }
         else if ((i % 3) == 1)
         {
             i += 1;
         }

         sprintf (buffer, "&$BENA;");
         if (page == 0)
         {
               strcpy (txt_data.mask[i].item->GetFeldPtr (),
                       buffer);
               display_field (mamain2, &txt_data.mask[i], 0, 0); 
         }
         else
         {
               strcpy (txt_data2.mask[i].item->GetFeldPtr (),
                       buffer);
               display_field (mamain2, &txt_data2.mask[i], 0, 0); 
         }
         return 0;
}


void SetAllgText (void)
/**
Verknuepfung zu allgemeinem Text setzen.
**/
{
         int i;
         char buffer [10];

         i = currentfield;
         if ((i % 3) == 0)
         {
             i += 2;
         }
         else if ((i % 3) == 1)
         {
             i += 1;
         }

         sprintf (buffer, "&$ALTE%ld;", atst.text_nr);
         if (page == 0)
         {
               strcpy (txt_data.mask[i].item->GetFeldPtr (),
                       buffer);
               display_field (mamain2, &txt_data.mask[i], 0, 0); 
         }
         else
         {
               strcpy (txt_data2.mask[i].item->GetFeldPtr (),
                       buffer);
               display_field (mamain2, &txt_data2.mask[i], 0, 0); 
         }
}


static int queryatst ()
/**
Query ueber Textnummer.
**/
{

        static char ltext_nr [41];
        static char leti [41];
        static char ltxtn [41];

        static ITEM itxtval  ("text_nr", ltext_nr, "Text-Nr.......:", 0);
        static ITEM itxtnval ("txt1",    ltxtn,    "Text..........:", 0);


        static field _qtxtform[] = {
           &itxtval,      40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &itxtnval,     40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
		};

        static form qtxtform = {2, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"text_nr", "txtn"};

        HWND query;
		int savefield;
		form *savecurrent;
        
		save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
        SetFkt (6, leer, NULL);
        set_fkt (NULL, 6);
        SetFkt (7, leer, NULL);
        set_fkt (NULL, 7);
        SetFkt (9, leer, NULL);
        set_fkt (NULL, 9);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (mamain1);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU);
        query = OpenWindowChC (5, 60, 11, 10, hMainInst,
                               "Suchkriterien f�r Texte");
        enter_form (query, &qtxtform, 0, 0);

        DestroyWindow (query);
		SetAktivWindow (mamain1);
        if (syskey == KEY12)
        {
                   atst_class.ReadQuery (&qtxtform, qnamen);

                   if (atst_class.QueryBu (mamain1, 0) == 0)
                   {
 		               currentfield = savefield;
                       SetAllgText ();
                   }
        }

		currentfield = savefield;
		restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (9);
        display_form (mamain1, current_form, 0, 0);
	    SetCurrentFocus (currentfield);
		syskey = 1;
        return 0;
}


static int setkey9txt ()
/**
Taste KEY9 auf aktiv setzen.
**/
{
       set_fkt (querytxt, 10);
       SetFkt (10, auswahl, KEY10);
       return 0;
}


static int setkey9sg ()
/**
Taste KEY9 auf aktiv setzen.
**/
{
	   mess.CloseMessage ();
       if (page == 0)
       {
                set_fkt (showsg, 9);
       }
       else
       {
                set_fkt (showsg2, 9);
       }
       SetFkt (9, ansehen, KEY9);
       return 0;
}

static int showptab (void)
/**
Prueftabelle fuer Kundenbestell-Einheit anzeigen.
**/
{

        ptab_class.Show 
        (current_form->mask[currentfield].item->GetItemName ());
        if (syskey == KEY5 || syskey == KEYESC) return 0;
		strcpy (current_form->mask[currentfield].item->GetFeldPtr (), 
                ptabn.ptwert);
		display_field (mamain2, &current_form->mask[currentfield],
                       0, 0);
		return 1;
}



static int setkey9ptab ()
/**
Taste KEY9 auf aktiv setzen.
**/
{
       set_fkt (showptab, 9);
       SetFkt (9, ansehen, KEY9);
       return 0;
}

static int testptab0 (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
	    if (ptab_class.lese_ptab 
                 (current_form->mask[currentfield].item->GetItemName (), 
                  current_form->mask[currentfield].item->GetFeldPtr ())
			!= 0)
		{
					return 100;
		}
		return 0;
}


static int testptab (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
	    if (testkeys ()) return 0;
		
        set_fkt (NULL, 9);
		SetFkt (9, leer, NULL);
		if (testptab0 () == 100)
		{
			        disp_mess ("Falsche Eingabe", 2);
                    SetCurrentField (currentfield);
					return 0;
		}
		display_field (mamain2, &current_form->mask[currentfield], 
                       0, 0); 
		return 0;
}

static int sgOK (short sg, short *ze)
/**
Gueltige Schriftgroesse pruefen.
**/
{
	     int i;
		 
		 for (i = 0; i < 20; i ++)
         {
			 if (*sgtab[i].sg == sg)
             {
                 if (page == 0)
                 {
 		            sprintf (txt_data.mask[currentfield + 1].item->GetFeldPtr (),
			             "%hd", *sgtab[i].ze);
                 }
                 else
                 {
 		            sprintf (txt_data2.mask[currentfield + 1].item->GetFeldPtr (),
			             "%hd", *sgtab[i].ze);
                 }
				 *ze = *sgtab[i].ze;
                 if (*ze == 0) *ze = 70;
				 return TRUE;
			 }
         }
         *ze = 70;
		 return FALSE;
}



static int testsg ()
/**
Schriftgroesse testen.
**/
{
	   short sgn;
	   short ze;

	   if (testkeys ()) return 1;


       sgn = atoi 
		   (txt_data.mask[currentfield].item->GetFeldPtr ());
	   if (sgn == 0 && syskey != KEYUP) 
       {
                display_form (mamain2, &txt_data, 0, 0);
		        SetCurrentField (currentfield);
				return 0;
       }


	   if (! sgOK (sgn, &ze)) 
       {
		        print_mess (2, "Fasche Schriftgr��e %hd", sgn); 
		        SetCurrentField (currentfield);
                display_form (mamain2, &txt_data, 0, 0);
				return 0;
       }

       move_field (&txt_data, &txt_data.mask[currentfield + 2],
		          txt_data.mask[currentfield + 2].pos[0],
		          txt_data.mask[currentfield + 2].pos[1],
		          0,
		          ze,0,0);
       display_form (mamain2, &txt_data, 0, 0);
       set_fkt (NULL, 9);
       SetFkt (9, leer, NULL);
	   return 0;
}

static int testsg2 ()
/**
Schriftgroesse testen.
**/
{
	   short sgn;
	   short ze;

	   if (testkeys ()) return 1;


       sgn = atoi 
		   (txt_data2.mask[currentfield].item->GetFeldPtr ());
	   if (sgn == 0 && syskey != KEYUP) 
       {
                display_form (mamain2, &txt_data2, 0, 0);
		        SetCurrentField (currentfield);
				return 0;
       }


	   if (! sgOK (sgn, &ze)) 
       {
		        print_mess (2, "Fasche Schriftgr��e %hd", sgn); 
		        SetCurrentField (currentfield);
                display_form (mamain2, &txt_data2, 0, 0);
				return 0;
       }

       move_field (&txt_data2, &txt_data2.mask[currentfield + 2],
		          txt_data2.mask[currentfield + 2].pos[0],
		          txt_data2.mask[currentfield + 2].pos[1],
		          0,
		          ze,0,0);
       display_form (mamain2, &txt_data2, 0, 0);
       set_fkt (NULL, 9);
       SetFkt (9, leer, NULL);
	   return 0;
}


int txt_copy (void)
/**
Texte in Buffer kopieren.
**/
{
	    int i;

		for (i = 0; i < 20; i ++)
		{
			 strcpy (txtbuffer[i],txt[i]);
		}
		mess.Message (hMainWindow, 
			             "Text in Buffer �bernommen",
						 22);

		return 0;
}


int init_buffer (void)
/**
Texte in Buffer kopieren.
**/
{
	    int i;

		for (i = 0; i < 20; i ++)
		{
			        txt[i][0] = (char) 0;
		}
		return 0;
}


int txt_insert (void)
/**
Texte in Buffer kopieren.
**/
{
	    int i;
		int OK;

        OK = 0;
		for (i = 0; i < 20; i ++)
		{
			 if (strlen (txtbuffer[i])) OK = 1;
		}
		if (OK == 0) return 0;

		for (i = 0; i < 20; i ++)
		{
			 strcpy (txt[i],txtbuffer[i]);
		}
		display_form (mamain2, current_form ,0, 0);
		return 0;
}


void init_txt (void)
/**
Etikettendaten initialilsieren.
**/
{
	    int i;

		for (i = 0; i < 14; i ++) 
        {
			strcpy (sg[i], "0"); 
			strcpy (ze[i], "0"); 
			memset (txt[i], ' ', 70);
			txt[i][70] = (char) 0;
			if (i * 3 + 2 < txt_data.fieldanz)
			{
                  txt_data.mask[i * 3 + 2].length = 70;
			}
       
        }
}

void init_txt2 (void)
/**
Etikettendaten initialilsieren.
**/
{
	    int i;

		for (i = 0; i < 14; i ++) 
        {
			strcpy (sg2[i], "0"); 
			strcpy (ze2[i], "0"); 
			memset (txt2[i], ' ', 70);
			txt2[i][70] = (char) 0;
			if (i * 3 + 2 < txt_data2.fieldanz)
			{
                  txt_data2.mask[i * 3 + 2].length = 70;
			}
       
        }
}


int GetZe (short sg)
/**
Anzahl Zeichen fuer sg holen.
**/
{
        int i;

		if (sg == 0) return (0);

		for (i = 0; i < 20; i ++)
		{
	             if (sg == *sgtab[i].sg) 
				 {
					 return (*sgtab[i].ze);
				 }
		}

		return (0);
}


void txt_to_form (void)
/**
Datenbankwerte in Maske uebertragen.
**/
{
	    int i;
		int zen;

	    sprintf (sg[0], "%hd", kfst.sg1);
	    sprintf (sg[1], "%hd", kfst.sg2);
	    sprintf (sg[2], "%hd", kfst.sg3);
	    sprintf (sg[3], "%hd", kfst.sg4);
	    sprintf (sg[4], "%hd", kfst.sg5);
	    sprintf (sg[5], "%hd", kfst.sg6);
	    sprintf (sg[6], "%hd", kfst.sg7);
	    sprintf (sg[7], "%hd", kfst.sg8);
	    sprintf (sg[8], "%hd", kfst.sg9);
	    sprintf (sg[9], "%hd", kfst.sg10);
	    sprintf (sg[10], "%hd", kfst.sg11);
	    sprintf (sg[11], "%hd", kfst.sg12);
	    sprintf (sg[12], "%hd", kfst.sg13);
	    sprintf (sg[13], "%hd", kfst.sg14);

	    strcpy (txt[0], kfst.txt1);
	    strcpy (txt[1], kfst.txt2);
	    strcpy (txt[2], kfst.txt3);
	    strcpy (txt[3], kfst.txt4);
	    strcpy (txt[4], kfst.txt5);
	    strcpy (txt[5], kfst.txt6);
	    strcpy (txt[6], kfst.txt7);
	    strcpy (txt[7], kfst.txt8);
	    strcpy (txt[8], kfst.txt9);
	    strcpy (txt[9], kfst.txt10);
	    strcpy (txt[10], kfst.txt11);
	    strcpy (txt[11], kfst.txt12);
	    strcpy (txt[12], kfst.txt13);
	    strcpy (txt[13], kfst.txt14);

		for (i = 0; i < 14; i ++)
		{
			   zen = GetZe (atoi (sg[i]));
		       sprintf (ze2[i],"%d", zen);
			   if ((i) * 3 + 2 >= txt_data.fieldanz)
               {
				        continue;
			   }
			   if (zen > 0)
               {
			            txt_data.mask[(i) * 3 + 2].length = zen + 1;
			   }
			   else
               {
			            txt_data.mask[(i) * 3 + 2].length = 70;
			   }
		}
}

void txt_to_form2 (void)
/**
Datenbankwerte in Maske uebertragen.
**/
{
	    int i;
		int zen;

	    sprintf (sg2[0], "%hd", kfst.sg1);
	    sprintf (sg2[1], "%hd", kfst.sg2);
	    sprintf (sg2[2], "%hd", kfst.sg3);
	    sprintf (sg2[3], "%hd", kfst.sg4);
	    sprintf (sg2[4], "%hd", kfst.sg5);
	    sprintf (sg2[5], "%hd", kfst.sg6);
	    sprintf (sg2[6], "%hd", kfst.sg7);
	    sprintf (sg2[7], "%hd", kfst.sg8);
	    sprintf (sg2[8], "%hd", kfst.sg9);
	    sprintf (sg2[9], "%hd", kfst.sg10);
	    sprintf (sg2[10], "%hd", kfst.sg11);
	    sprintf (sg2[11], "%hd", kfst.sg12);
	    sprintf (sg2[12], "%hd", kfst.sg13);
	    sprintf (sg2[13], "%hd", kfst.sg14);

	    strcpy (txt2[0], kfst.txt1);
	    strcpy (txt2[1], kfst.txt2);
	    strcpy (txt2[2], kfst.txt3);
	    strcpy (txt2[3], kfst.txt4);
	    strcpy (txt2[4], kfst.txt5);
	    strcpy (txt2[5], kfst.txt6);
	    strcpy (txt2[6], kfst.txt7);
	    strcpy (txt2[7], kfst.txt8);
	    strcpy (txt2[8], kfst.txt9);
	    strcpy (txt2[9], kfst.txt10);
	    strcpy (txt2[10], kfst.txt11);
	    strcpy (txt2[11], kfst.txt12);
	    strcpy (txt2[12], kfst.txt13);
	    strcpy (txt2[13], kfst.txt14);

		for (i = 0; i < 14; i ++)
		{
			   zen = GetZe (atoi (sg2[i]));
		       sprintf (ze[i],"%d", zen);
			   if ((i) * 3 + 2 >= txt_data2.fieldanz)
               {
				        continue;
			   }
			   if (zen > 0)
               {
			            txt_data2.mask[(i) * 3 + 2].length = zen + 1;
			   }
			   else
               {
			            txt_data2.mask[(i) * 3 + 2].length = 70;
			   }
		}
}


void form_to_txt (void)
/**
Datenbankwerte in Maske uebertragen.
**/
{

		kfst.sg1 = atoi (sg[0]);
		kfst.sg2 = atoi (sg[1]);
		kfst.sg3 = atoi (sg[2]);
		kfst.sg4 = atoi (sg[3]);
		kfst.sg5 = atoi (sg[4]);
		kfst.sg6 = atoi (sg[5]);
		kfst.sg7 = atoi (sg[6]);
		kfst.sg8 = atoi (sg[7]);
		kfst.sg9 = atoi (sg[8]);
		kfst.sg10 = atoi (sg[9]);
		kfst.sg11 = atoi (sg[10]);
		kfst.sg12 = atoi (sg[11]);
		kfst.sg13 = atoi (sg[12]);
		kfst.sg14 = atoi (sg[13]);

		strcpy (kfst.txt1, txt[0]);
		strcpy (kfst.txt2, txt[1]);
		strcpy (kfst.txt3, txt[2]);
		strcpy (kfst.txt4, txt[3]);
		strcpy (kfst.txt5, txt[4]);
		strcpy (kfst.txt6, txt[5]);
		strcpy (kfst.txt7, txt[6]);
		strcpy (kfst.txt8, txt[7]);
		strcpy (kfst.txt9, txt[8]);
		strcpy (kfst.txt10, txt[9]);
		strcpy (kfst.txt11, txt[10]);
		strcpy (kfst.txt12, txt[11]);
		strcpy (kfst.txt13, txt[12]);
		strcpy (kfst.txt14, txt[13]);
}

void form_to_txt2 (void)
/**
Datenbankwerte in Maske uebertragen.
**/
{

		kfst.sg1 = atoi (sg2[0]);
		kfst.sg2 = atoi (sg2[1]);
		kfst.sg3 = atoi (sg2[2]);
		kfst.sg4 = atoi (sg2[3]);
		kfst.sg5 = atoi (sg2[4]);
		kfst.sg6 = atoi (sg2[5]);
		kfst.sg7 = atoi (sg2[6]);
		kfst.sg8 = atoi (sg2[7]);
		kfst.sg9 = atoi (sg2[8]);
		kfst.sg10 = atoi (sg2[9]);
		kfst.sg11 = atoi (sg2[10]);
		kfst.sg12 = atoi (sg2[11]);
		kfst.sg13 = atoi (sg2[12]);
		kfst.sg14 = atoi (sg2[13]);

		strcpy (kfst.txt1, txt2[0]);
		strcpy (kfst.txt2, txt2[1]);
		strcpy (kfst.txt3, txt2[2]);
		strcpy (kfst.txt4, txt2[3]);
		strcpy (kfst.txt5, txt2[4]);
		strcpy (kfst.txt6, txt2[5]);
		strcpy (kfst.txt7, txt2[6]);
		strcpy (kfst.txt8, txt2[7]);
		strcpy (kfst.txt9, txt2[8]);
		strcpy (kfst.txt10, txt2[9]);
		strcpy (kfst.txt11, txt2[10]);
		strcpy (kfst.txt12, txt2[11]);
		strcpy (kfst.txt13, txt2[12]);
		strcpy (kfst.txt14, txt2[13]);
}


int LeseTxt (void)
/**
Etiketten-Nummer aus Datenbank holen.
**/
{

        if (atol (text_nr) <= 0)
		{
			
			        disp_mess ("Text-Nr ist 0", 2);
					return 0;
		}

        if (atol (text_nr) > 49)
        {
			        disp_mess ("Maximale Text-Nr ist 49", 2);
					return 0;
        }

		if (IDM_ACTIVE == IDM_WORK ||
			IDM_ACTIVE == IDM_DEL)
		{
                    beginwork ();
		}
			
		kfst.text_nr = atol (text_nr);
		
        if (kfst_class.dbreadfirst () == 100) 
        {
			          init_txt ();
					  if (IDM_ACTIVE != IDM_WORK)
                      {
						          disp_mess ("Satz nicht gefunden", 2);
								  return 0;
					  }
        }
		else
        {
			          txt_to_form ();
        }
        kfst.text_nr += 50;
        if (kfst_class.dbreadfirst () == 100) 
        {
                init_txt2 ();
        }
        else
        {
                txt_to_form2 ();
        }

       
        SetFkt (9, "" , 0);
			
		kfst.text_nr = atol (text_nr);
        display_form (mamain1, current_form , 0, 0);
        return 1;
}

int SchreibeTxt2 (void)
/**
Satz fuer Etikettentyp schreiben. Seite 2
**/
{

         
         kfst.text_nr = atol (text_nr) + 50;
	     form_to_txt2 ();
         if (IDM_ACTIVE == IDM_WORK)
         {
	                kfst_class.dbupdate ();
         }
         else if (IDM_ACTIVE == IDM_DEL)
		 {
	                kfst_class.dbdelete ();
		 }
		 return  1;
}

int SchreibeTxt (void)
/**
Satz fuer Etikettentyp scrheiben.
**/
{

         
         SchreibeTxt2 ();
         kfst.text_nr = atol (text_nr);
	     form_to_txt ();
         if (IDM_ACTIVE == IDM_WORK)
         {
	                kfst_class.dbupdate ();
                    commitwork ();
                    disp_mess ("Satz wurde geschrieben", 1);
                    break_enter ();
         }
         else if (IDM_ACTIVE == IDM_DEL)
		 {
	                kfst_class.dbdelete ();
                    commitwork ();
                    CloseControls (&txt_ub);
                    CloseControls (&txt_data);
                    CloseControls (&txt_data2);
                    disp_mess ("Satz wurde gel�scht", 1);
                    SetCurrentFocus (currentfield);
		 }
		 return  1;
}


void DisableMenu (void)
/**
Mineu auf Inaktiv schalten.
**/
{
	   EnableMenuItem (hMenu, IDM_WORK, MF_GRAYED);
	   EnableMenuItem (hMenu, IDM_SHOW, MF_GRAYED);
	   EnableMenuItem (hMenu, IDM_DEL,  MF_GRAYED);
	   EnableMenuItem (hMenu, IDM_PRINT,MF_GRAYED);

       ToolBar_SetState(hwndTB,IDM_WORK, TBSTATE_INDETERMINATE);
       ToolBar_SetState(hwndTB,IDM_SHOW, TBSTATE_INDETERMINATE);
       ToolBar_SetState(hwndTB,IDM_DEL, TBSTATE_INDETERMINATE);
       ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_INDETERMINATE);
       ToolBar_PressButton(hwndTB,IDM_ACTIVE, TRUE);
}

void EnableMenu (void)
/**
Mineu auf Inaktiv schalten.
**/
{
	   EnableMenuItem (hMenu, IDM_WORK, MF_ENABLED);
	   EnableMenuItem (hMenu, IDM_SHOW, MF_ENABLED);
	   EnableMenuItem (hMenu, IDM_DEL,  MF_ENABLED);
	   EnableMenuItem (hMenu, IDM_PRINT,MF_ENABLED);

       ToolBar_SetState(hwndTB,IDM_WORK, TBSTATE_ENABLED);
       ToolBar_SetState(hwndTB,IDM_SHOW, TBSTATE_ENABLED);
       ToolBar_SetState(hwndTB,IDM_DEL, TBSTATE_ENABLED);
       ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_ENABLED);
       ToolBar_PressButton(hwndTB,IDM_ACTIVE, TRUE);
}


int dokeypgd (void)
/**
Start der Funktion Texte erfassen.
**/
{
	  break_enter ();
	  return 1;
}


int bearb_txt_data2 (void)
{


      DisplayKopf3 (hwndTB, TXT_ACTIVE, 2, 2);
      Ckfst.text1 = stext2;
      Ckfst.bmp   = bFuss;
      display_field (mamain1, &kfsttext.mask[0], 0, 0);
      while (TRUE)
      {
               enter_form (mamain2, &txt_data2, 0, 0);
               CloseControls (&txt_data2);
               if (syskey == KEY5 || syskey == KEYESC ||
				   syskey == KEY12)
               {
                   break;
               }

               if (syskey == KEYPGD || syskey == KEYPGU)
               {
                   break;
               }
      }
	  return 0;
}

static int FirstCall = 1;


int bearb_txt_data1 (void)
{


      DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 2);
      Ckfst.text1 = stext1;
      Ckfst.bmp   = bKopf;
      CSend.bmp   = bBriefu;
      display_form (mamain1, &kfsttext, 0, 0);
      if (mamain2)
	  { 
		  CloseControls (&txt_data);
	  }


      DisableMenu ();

	  if (mamain2 == NULL)
      {
             SetAktivWindow (mamain1);
 	         SetBorder (WS_CHILD | WS_DLGFRAME);
             mamain2 = OpenWindowCh (15, 77, 3, 0, hMainInst);
	  }
      display_form (mamain2, &txt_ub, 0, 0);
      while (TRUE)
      {
 	           DisplayAfterEnter (FALSE);
               if (FirstCall)
               {
                     display_form (mamain2, &txt_data, 0, 0);
                     FirstCall = 0;
               }
               
               enter_form (mamain2, &txt_data, 0, 0);
               CloseControls (&txt_data);
               if (syskey == KEY5 || syskey == KEYESC ||
				   syskey == KEY12)
               {
                   break;
               }
               if (syskey == KEYPGD || syskey == KEYPGU)
               {
                   break;
               }
	}
    return 0;
}


int bearb_txt_data (void)
{
      if (LeseTxt () == FALSE)
      {
          return 0;
      }
      txt_active = FirstCall = 1;
      SetvScrollStart (2);
	  save_fkt (80);
	  save_fkt (10);
	  save_fkt (12);
	  set_fkt (txt_copy, 6);
	  set_fkt (txt_insert, 7);
      set_fkt (SetBed, 8);
      set_fkt (queryatst, 10);
	  SetFkt (6,  kopieren, KEY6);
	  SetFkt (7,  einfuegen, KEY7);
	  SetFkt (8,  bediener,  KEY8);
	  SetFkt (10, allgtexte,  KEY10);
      set_fkt (SchreibeTxt, 12);
      set_keypgu (dokeypgd);
      set_keypgd (dokeypgd);
      ToolBar_SetState(hwndTB,KEYPGD, TBSTATE_ENABLED);
      ToolBar_SetState(hwndTB,KEYPGU, TBSTATE_ENABLED);
      DisplayAfterEnter (FALSE);

      CloseControls (&txt_ub);
      CloseControls (&txt_data);
      CloseControls (&txt_data2);

      if (IDM_ACTIVE == IDM_WORK) DisableMenu ();

	  if (mamain2 == NULL)
      {
             SetAktivWindow (mamain1);
 	         SetBorder (WS_CHILD | WS_DLGFRAME);
             mamain2 = OpenWindowCh (15, 77, 3, 0, hMainInst);
	  }

      if (IDM_ACTIVE == IDM_DEL)
      {
  	         restore_fkt (8);
  	         restore_fkt (10);
	         restore_fkt (12);
 	         SetFkt (6, leer, NULL);
 	         SetFkt (7, leer, NULL);
 	         SetFkt (9, leer, NULL);
             set_fkt (NULL, 6);
             set_fkt (NULL, 7);
             set_fkt (NULL, 9);
             display_form (mamain2, &txt_ub, 0, 0);
             display_form (mamain2, &txt_data, 0, 0);
             return 0;
      }

      page = 0;
      while (TRUE)
      {
           switch (page)
           {
                  case 0:
                      bearb_txt_data1 ();
                      break;
                  case 1:
                      bearb_txt_data2 ();
                      break;
           }

           if (syskey == KEY5 || syskey == KEYESC ||
				   syskey == KEY12) break;

           if (syskey == KEYPGD)
           {
                   page += 1;
           }
           else if (syskey == KEYPGU)
           {
                   page += 1;
           }
           page %= 2;
      }

      EnableMenu ();
      DisplayAfterEnter (TRUE);
      break_end ();
	  restore_fkt (10);
	  restore_fkt (12);
      set_keypgu (NULL);
      set_keypgd (NULL);
      set_fkt (NULL, 12);
      set_fkt (NULL, 6);
      set_fkt (NULL, 7);
      set_fkt (NULL, 9);
	  SetFkt (6, leer, NULL);
	  SetFkt (7, leer, NULL);
	  SetFkt (9, leer, NULL);
      DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 1);
      ToolBar_SetState(hwndTB,KEYPGD, TBSTATE_INDETERMINATE);
      ToolBar_SetState(hwndTB,KEYPGU, TBSTATE_INDETERMINATE);
	  return 0;
}


void EnterEti (void)
/**
Mandant und Filiale eingeben.
**/
{
      ActivateColButton (mamain1, &kfsttext, 0, -1, 0);
      while (TRUE)
      {
               break_end ();
               Ckfst.text1 = stext1;
               Ckfst.bmp   = bKopfu;
               CSend.bmp   = bBrief;
               display_form (mamain1, &kfsttext, 0, 0);
               ActivateColButton (mamain1, &kfsttext, 0, -1, 0);
               enter_form (mamain1, &etitext, 0, 0);
               if (syskey == KEY5 || syskey == KEYESC) break;
               ActivateColButton (mamain1, &kfsttext, 0,  0, 1);
               ActivateColButton (mamain1, &kfsttext, 1, -1, 1);
               bearb_txt_data ();
               ActivateColButton (mamain1, &kfsttext, 0, -1, 1);
               ActivateColButton (mamain1, &kfsttext, 1,  0, 1);
      }
      set_fkt (NULL, 12);
}


void EingabeMdnFil (void)
{
       extern MDNFIL cmdnfil;

       EnterEti (); 
}

int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{

       opendbase ("bws");
	   init_buffer ();
       menue_class.SetSysBen ();

	   mamain2 = NULL;
       SetStdProc (WndProc);
       InitFirstInstance (hInstance);

       SetModuleName ("Kopf/Fuss-Texte");
       hMainWindow = OpenWindow (26, 80, 2, 0, 0, hInstance);
	   hMenu = MakeMenue (menuetab);
	   SetMenu (hMainWindow, hMenu);
	   hwndTB = MakeToolBarEx (hInstance, 
		                       hMainWindow,tbb, 8,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);

       DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 1);

       SetAktivWindow (hMainWindow);
       mamain1 = OpenMamain1 (18, 77, 2, 1, hInstance, WndProc,
                                  NULL, NULL);
       InvalidateRect (mamain1, NULL, TRUE);
       ftasten = OpenFktM (hInstance);
       SetAktivWindow (mamain1);
       set_fkt (dokey5, 5);
       eti_typ_class.lese_eti_typ (99);

       bKopf  = LoadBitmap (hInstance, "KOPF");
       bKopfu = LoadBitmap (hInstance, "KOPFU");
       bBrief  = LoadBitmap (hInstance, "BRIEF");
       bBriefu = LoadBitmap (hInstance, "BRIEFU");
       bFuss  = LoadBitmap (hInstance, "FUSS");
       SetAktivWindow (mamain1);
       SetBorder (WS_CHILD | WS_DLGFRAME);
       mamain2 = OpenWindowCh (15, 77, 3, 0, hMainInst);
       display_form (mamain2, &txt_ub, 0, 0);
	   EingabeMdnFil ();
       return 0;
}

void InitFirstInstance(HANDLE hInstance)
{
        WNDCLASS wc;
        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "ROSIICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hStdWindow";

        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hMainWindow";
        hMainInst = hInstance;
        return;
}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        int cx, cy;
        TEXTMETRIC tm;
        HDC hdc;
        HFONT hFont, oldFont;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (0, hdc);

        cx = 78 * tm.tmAveCharWidth;
        cy = 25 * tm.tmHeight;

        hMainWindow = CreateWindow ("hStdWindow",
                                    "Testbox",
                                    WS_DLGFRAME | WS_CAPTION | WS_SYSMENU,
                                    CW_USEDEFAULT, CW_USEDEFAULT,
                                    cx, cy,
                                    NULL,
                                    NULL,
                                    hInstance,
                                    NULL);

        ShowWindow (hMainWindow, SW_SHOW);
        UpdateWindow (hMainWindow);
        return 0;
}

static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                       return msg.wParam;
                       }
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
        return msg.wParam;
}


void PrintLines (HDC hdc)
/**
Linien am Bildschirm anzeigen.
**/
{
         static TEXTMETRIC tm;
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         int x, y;

         GetTextMetrics (hdc, &tm);

         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         SelectObject (hdc, hPenG);
         x = 80 * tm.tmAveCharWidth;
         y = 2 * tm.tmHeight;
         y = y + y / 3;
		 y -= 10;
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x, y);

         SelectObject (hdc, hPenW);
         x = 80 * tm.tmAveCharWidth;
         y = 2 * tm.tmHeight;
         y = y + y / 3 + 1;
		 y -= 10;
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x, y);
}


void PrintIcons (HDC hdc)
/**
Icons am Bildschirm anzeigen.
**/
{
         static TEXTMETRIC tm;
         int x, y;

         GetTextMetrics (hdc, &tm);
         x = 65 * tm.tmAveCharWidth;
         y = 2 * tm.tmHeight;
         y = y + y / 3;
         DrawIcon (hdc, x, y, hIcon);
}


LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
        HDC hdc;
        char command[256];

        switch(msg)
        {
              case WM_PAINT :
                      if (hWnd == mamain1)
                      {
                              hdc = BeginPaint (mamain1, &ps);
                              EndPaint (mamain1, &ps);
                      }
                      else if (hWnd == hMainWindow)
                      {
                              hdc = BeginPaint (hMainWindow, &ps);
                              PrintLines (hdc);
                              EndPaint (mamain1, &ps);
                      }
                      else if (hWnd == mamain2)
                      {
                              hdc = BeginPaint (hMainWindow, &ps);
//                              PrintRect (hdc);
                              EndPaint (mamain1, &ps);
                      }
                      break;
              case WM_NOTIFY :
                    {
                      LPNMHDR pnmh = (LPNMHDR) lParam;

                      if (pnmh->code == TTN_NEEDTEXT)
                      {
                           LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
                           if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                               == FALSE)
                           {
                                    QuickHwndCpy (lpttt);
                           }
                      }
                      break;
                    }

              case WM_DESTROY :
                      if (hWnd == hMainWindow)
                      {
                             ExitProcess (0);
                             return 0;
                      }
                      break;

              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY2)
                    {
                            syskey = KEY2;
                            SendKey (VK_F2);
                            break;
                    }
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
					else if (LOWORD (wParam) == IDM_WORK)
                    {
						    CheckMenuItem (hMenu, IDM_ACTIVE,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_WORK,
								           MF_CHECKED); 
                            ToolBar_PressButton(hwndTB,
                                                IDM_ACTIVE, FALSE);
                            ToolBar_SetState(hwndTB,
                                               IDM_ACTIVE, TBSTATE_ENABLED);
                            ToolBar_PressButton(hwndTB,
                                                IDM_WORK, TRUE);
		  	                IDM_ACTIVE = IDM_WORK;
                            TXT_ACTIVE = TXT_WORK;
                            DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 1);
                   }
					else if (LOWORD (wParam) == IDM_SHOW)
                    {
						    CheckMenuItem (hMenu, IDM_ACTIVE,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_SHOW,
								           MF_CHECKED); 
                            ToolBar_PressButton(hwndTB,
                                                IDM_ACTIVE, FALSE);
                            ToolBar_SetState(hwndTB,
                                               IDM_ACTIVE, TBSTATE_ENABLED);
                            ToolBar_PressButton(hwndTB,
                                                IDM_SHOW, TRUE);
							IDM_ACTIVE = IDM_SHOW;
                            TXT_ACTIVE = TXT_SHOW;
                            DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 1);
                    }
					else if (LOWORD (wParam) == IDM_DEL)
                    {
						    CheckMenuItem (hMenu, IDM_ACTIVE,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_DEL,
								           MF_CHECKED); 
                            ToolBar_PressButton(hwndTB,
                                                IDM_ACTIVE, FALSE);
                            ToolBar_SetState(hwndTB,
                                               IDM_ACTIVE, TBSTATE_ENABLED);
                            ToolBar_PressButton(hwndTB,
                                                IDM_DEL, TRUE);
							IDM_ACTIVE = IDM_DEL;
                            TXT_ACTIVE = TXT_DEL;
                            DisplayKopf3 (hwndTB, TXT_ACTIVE, 1, 1);
                    }
					else if (LOWORD (wParam) == IDM_PRINT)
                    {
                            sprintf (command, "70001 14560 %s",
                                     sys_ben.pers_nam);
                            ProcExec (command, SW_SHOWNORMAL,
                                       -1, 0, -1, 0); 
                    }
                    else if (LOWORD (wParam) == IDM_TEXT)
                    {
                           	     ProcExec ("atst", 
                                     SW_SHOWNORMAL, 
                                     -1, 0, -1, 0); 
                    }
                    else if (HIWORD (wParam) == CBN_DROPDOWN)
                    {
                                  opencombobox = TRUE;
                                  return 0;
                    }
                    else if (HIWORD (wParam) == CBN_CLOSEUP)
                    {
                           
                                  opencombobox = FALSE;
                                  return 0;
                    }
                    else if (LOWORD (wParam) == IDM_INFO)
                    {
                                  syskey = KEY4;
                                  SendKey (VK_F4);
                    }
                    else if (LOWORD (wParam) == IDM_SEND)
                    {
                                  SendToPeri ();
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

