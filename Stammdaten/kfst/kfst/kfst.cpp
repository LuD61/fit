#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "kfst.h"
#include "dbclass.h"
#include "dbfunc.h"

KFST kfst, kfst_null;

/* Variablen fuer Auswahl mit Buttonueberschrift    */

#include "itemc.h"
#define MAXSORT 10000

static char *sqltext;

static int dosort1 ();
static int dosort2 ();
static int dosort3 ();

struct SORT_AW
{
	     char sort1 [10];
	     char sort2 [6];
	     char sort3 [70];
	     char sort4 [70];
	     int  sortidx;
};

static struct SORT_AW sort_aw, sort_awtab[MAXSORT];
static int sort_awanz;
static int sort1 = -1;
static int sort2 = -1;
static int sort3 = -1;
static int sort4 = -1;
static int sort_idx = 0;

static ITEM isort1    ("", sort_aw.sort1, "", 0);
static ITEM isort2    ("", sort_aw.sort2, "", 0);
static ITEM isort3    ("", sort_aw.sort3, "", 0);

static field _fsort_aw[] = {
&isort1,      9, 1, 0, 0, 0, "", NORMAL, 0, 0, 0,
&isort2,     70, 1, 0, 9, 0, "",    NORMAL, 0, 0, 0,
&isort3,     70, 1, 0,80, 0, "",    NORMAL, 0, 0, 0,
};

static form fsort_aw = {3, 0, 0, _fsort_aw, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "Text_Nr",  "", 0);
static ITEM isort2ub    ("", "Zeile1",        "", 0);
static ITEM isort3ub    ("", "Zeile2",        "", 0);
static ITEM ifillub     ("", " ",             "", 0); 

static field _fsort_awub[] = {
&isort1ub,        9, 1, 0,  0, 0, "", BUTTON, 0, dosort1,    102,
&isort2ub,       70, 1, 0,  9, 0, "", BUTTON, 0, dosort3,    104,
&isort3ub,       70, 1, 0, 80, 0, "", BUTTON, 0, dosort3,    105,
&ifillub,        80, 1, 0,160, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {4, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0,  9, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 80, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0,160, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awl = {3, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};

static int dosort10 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return ((int) (atol(el1->sort1) - atol (el2->sort1)) * 
				                                  sort1);
}


static int dosort1 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort10);
	sort1 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static int dosort20 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return ( strcmp(el1->sort3, el2->sort3) * sort3);
}

static int dosort2 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort20);
	sort3 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static int dosort30 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return ( strcmp(el1->sort4, el2->sort4) * sort4);
}


static int dosort3 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort20);
	sort4 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        sort_idx = idx;
        break_list ();
        return;
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

/* Ende Variablen fuer Auswahl mit Buttonueberschrift    */

static void FillUb  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%8s %-24s",
                          "Nummer ", "Bezeichnung");
}


static void FillValues  (char *buffer)
{
         sprintf (buffer, "%8ld",
                           kfst.text_nr);
}

void KFST_CLASS::prepare (void)
{
     char sqltext [0x1000];
     ins_quest ((char *) &kfst.text_nr, 2, 0);
    out_quest ((char *) &kfst.text_nr,2,0);
    out_quest ((char *) kfst.txt1,0,71);
    out_quest ((char *) kfst.txt2,0,71);
    out_quest ((char *) kfst.txt3,0,71);
    out_quest ((char *) kfst.txt4,0,71);
    out_quest ((char *) kfst.txt5,0,71);
    out_quest ((char *) kfst.txt6,0,71);
    out_quest ((char *) kfst.txt7,0,71);
    out_quest ((char *) kfst.txt8,0,71);
    out_quest ((char *) kfst.txt9,0,71);
    out_quest ((char *) kfst.txt10,0,71);
    out_quest ((char *) kfst.txt11,0,71);
    out_quest ((char *) kfst.txt12,0,71);
    out_quest ((char *) kfst.txt13,0,71);
    out_quest ((char *) kfst.txt14,0,71);
    out_quest ((char *) &kfst.sg1,1,0);
    out_quest ((char *) &kfst.sg2,1,0);
    out_quest ((char *) &kfst.sg3,1,0);
    out_quest ((char *) &kfst.sg4,1,0);
    out_quest ((char *) &kfst.sg5,1,0);
    out_quest ((char *) &kfst.sg6,1,0);
    out_quest ((char *) &kfst.sg7,1,0);
    out_quest ((char *) &kfst.sg8,1,0);
    out_quest ((char *) &kfst.sg9,1,0);
    out_quest ((char *) &kfst.sg10,1,0);
    out_quest ((char *) &kfst.sg11,1,0);
    out_quest ((char *) &kfst.sg12,1,0);
    out_quest ((char *) &kfst.sg13,1,0);
    out_quest ((char *) &kfst.sg14,1,0);
     cursor = prepare_sql ("select kfst.text_nr,  kfst.txt1,  "
"kfst.txt2,  kfst.txt3,  kfst.txt4,  kfst.txt5,  kfst.txt6,  kfst.txt7,  "
"kfst.txt8,  kfst.txt9,  kfst.txt10,  kfst.txt11,  kfst.txt12,  kfst.txt13,  "
"kfst.txt14,  kfst.sg1,  kfst.sg2,  kfst.sg3,  kfst.sg4,  kfst.sg5,  kfst.sg6,  "
"kfst.sg7,  kfst.sg8,  kfst.sg9,  kfst.sg10,  kfst.sg11,  kfst.sg12,  kfst.sg13,  "
"kfst.sg14 from kfst "

#line 193 "kfst.rpp"
                           "where text_nr = ?");

    ins_quest ((char *) &kfst.text_nr,2,0);
    ins_quest ((char *) kfst.txt1,0,71);
    ins_quest ((char *) kfst.txt2,0,71);
    ins_quest ((char *) kfst.txt3,0,71);
    ins_quest ((char *) kfst.txt4,0,71);
    ins_quest ((char *) kfst.txt5,0,71);
    ins_quest ((char *) kfst.txt6,0,71);
    ins_quest ((char *) kfst.txt7,0,71);
    ins_quest ((char *) kfst.txt8,0,71);
    ins_quest ((char *) kfst.txt9,0,71);
    ins_quest ((char *) kfst.txt10,0,71);
    ins_quest ((char *) kfst.txt11,0,71);
    ins_quest ((char *) kfst.txt12,0,71);
    ins_quest ((char *) kfst.txt13,0,71);
    ins_quest ((char *) kfst.txt14,0,71);
    ins_quest ((char *) &kfst.sg1,1,0);
    ins_quest ((char *) &kfst.sg2,1,0);
    ins_quest ((char *) &kfst.sg3,1,0);
    ins_quest ((char *) &kfst.sg4,1,0);
    ins_quest ((char *) &kfst.sg5,1,0);
    ins_quest ((char *) &kfst.sg6,1,0);
    ins_quest ((char *) &kfst.sg7,1,0);
    ins_quest ((char *) &kfst.sg8,1,0);
    ins_quest ((char *) &kfst.sg9,1,0);
    ins_quest ((char *) &kfst.sg10,1,0);
    ins_quest ((char *) &kfst.sg11,1,0);
    ins_quest ((char *) &kfst.sg12,1,0);
    ins_quest ((char *) &kfst.sg13,1,0);
    ins_quest ((char *) &kfst.sg14,1,0);
     sprintf (sqltext, "update kfst set kfst.text_nr = ?,  "
"kfst.txt1 = ?,  kfst.txt2 = ?,  kfst.txt3 = ?,  kfst.txt4 = ?,  "
"kfst.txt5 = ?,  kfst.txt6 = ?,  kfst.txt7 = ?,  kfst.txt8 = ?,  "
"kfst.txt9 = ?,  kfst.txt10 = ?,  kfst.txt11 = ?,  kfst.txt12 = ?,  "
"kfst.txt13 = ?,  kfst.txt14 = ?,  kfst.sg1 = ?,  kfst.sg2 = ?,  "
"kfst.sg3 = ?,  kfst.sg4 = ?,  kfst.sg5 = ?,  kfst.sg6 = ?,  kfst.sg7 = ?,  "
"kfst.sg8 = ?,  kfst.sg9 = ?,  kfst.sg10 = ?,  kfst.sg11 = ?,  "
"kfst.sg12 = ?,  kfst.sg13 = ?,  kfst.sg14 = ? " 

#line 196 "kfst.rpp"
                       "where kfst.text_nr = ? ");
     ins_quest ((char *) &kfst.text_nr, 2, 0);
     upd_cursor  = prepare_sql (sqltext); 

    ins_quest ((char *) &kfst.text_nr,2,0);
    ins_quest ((char *) kfst.txt1,0,71);
    ins_quest ((char *) kfst.txt2,0,71);
    ins_quest ((char *) kfst.txt3,0,71);
    ins_quest ((char *) kfst.txt4,0,71);
    ins_quest ((char *) kfst.txt5,0,71);
    ins_quest ((char *) kfst.txt6,0,71);
    ins_quest ((char *) kfst.txt7,0,71);
    ins_quest ((char *) kfst.txt8,0,71);
    ins_quest ((char *) kfst.txt9,0,71);
    ins_quest ((char *) kfst.txt10,0,71);
    ins_quest ((char *) kfst.txt11,0,71);
    ins_quest ((char *) kfst.txt12,0,71);
    ins_quest ((char *) kfst.txt13,0,71);
    ins_quest ((char *) kfst.txt14,0,71);
    ins_quest ((char *) &kfst.sg1,1,0);
    ins_quest ((char *) &kfst.sg2,1,0);
    ins_quest ((char *) &kfst.sg3,1,0);
    ins_quest ((char *) &kfst.sg4,1,0);
    ins_quest ((char *) &kfst.sg5,1,0);
    ins_quest ((char *) &kfst.sg6,1,0);
    ins_quest ((char *) &kfst.sg7,1,0);
    ins_quest ((char *) &kfst.sg8,1,0);
    ins_quest ((char *) &kfst.sg9,1,0);
    ins_quest ((char *) &kfst.sg10,1,0);
    ins_quest ((char *) &kfst.sg11,1,0);
    ins_quest ((char *) &kfst.sg12,1,0);
    ins_quest ((char *) &kfst.sg13,1,0);
    ins_quest ((char *) &kfst.sg14,1,0);
     ins_cursor = prepare_sql ("insert into kfst (text_nr,  "
"txt1,  txt2,  txt3,  txt4,  txt5,  txt6,  txt7,  txt8,  txt9,  txt10,  txt11,  txt12,  txt13,  "
"txt14,  sg1,  sg2,  sg3,  sg4,  sg5,  sg6,  sg7,  sg8,  sg9,  sg10,  sg11,  sg12,  sg13,  sg14) "

#line 201 "kfst.rpp"
                              "values "
                              "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 203 "kfst.rpp"

     ins_quest ((char *) &kfst.text_nr, 2, 0);
     del_cursor = prepare_sql ("delete from kfst "
                               "where kfst.text_nr = ? ");
     ins_quest ((char *) &kfst.text_nr, 2, 0);
     test_upd_cursor = prepare_sql ("select kfst.text_nr from kfst "
                                    "where kfst.text_nr = ? "
                                    "for update");
}

int KFST_CLASS::prep_awcursor (void)
/**
Cursor fuer Auswahl vorbereiten.
**/
{
         int cursor_ausw;

         out_quest ((char *) &kfst.text_nr, 2, 0);
         out_quest ( (char *) kfst.txt1, 0, 70);
         out_quest ( (char *) kfst.txt2, 0, 70);

         cursor_ausw = prepare_scroll ("select text_nr, txt1, txt2 from kfst "
                                       "where text_nr > 0"
                                       "order by text_nr");
         if (sqlstatus)
         {
                     return (-1);
         }
         return (cursor_ausw);
}

int KFST_CLASS::prep_awcursor (char *sqlstring)
/**
Cursor fuer Auswahl vorbereiten.
**/
{
         int cursor_ausw;

         out_quest ((char *) &kfst.text_nr, 2, 0);
         out_quest ( (char *) kfst.txt1, 0, 71);
         out_quest ( (char *) kfst.txt2, 0, 71);
         out_quest ( (char *) kfst.txt3, 0, 70);
         out_quest ( (char *) kfst.txt4, 0, 70);
         out_quest ( (char *) kfst.txt5, 0, 70);
         out_quest ( (char *) kfst.txt6, 0, 70);
         out_quest ( (char *) kfst.txt7, 0, 70);

         cursor_ausw = prepare_scroll (sqlstring);
         if (sqlstatus)
         {
                     return (-1);
         }
         return (cursor_ausw);
}


int KFST_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

void KFST_CLASS::TestQueryTxt (char *qname, char *qfeld)
/**
Sqlstring fuer Texte zusammenbauen.
**/
{
         int anz;
         int i;
         char *pos;

         strcat (qstring, qname);
         strcat (qstring, " ");
         if (pos = strchr (qfeld, '=')) 
         {
                    strcat (qstring, "= ");
                    testquot (qstring, qname);
                    strcat (qstring, pos + 1);
                    testquot (qstring, qname);
         }
         else if (pos = strchr (qfeld, '>')) 
         {
                    strcat (qstring, "> ");
                    testquot (qstring, qname);
                    strcat (qstring, pos + 1);
                    testquot (qstring, qname);
         }
         else if (pos = strchr (qfeld, '<')) 
         {
                    strcat (qstring, "< ");
                    testquot (qstring, qname);
                    strcat (qstring, pos + 1);
                    testquot (qstring, qname);
         }
         else if (strchr (qfeld, '*')) 
         {
                    strcat (qstring, "matches \"");
                    strcat (qstring, qfeld);
                    strcat (qstring, "\"");
         }
         else if (strchr (qfeld, ':'))
         {
                    anz = zsplit (qfeld, ':');
                    if (anz > 1)
                    {
                            strcat (qstring, "between ");
                            testquot (qstring, qname);
                            strcat (qstring, zwort[1]);
                            testquot (qstring, qname);
                            strcat (qstring, " and ");
                            testquot (qstring, qname);
                            strcat (qstring, zwort[2]);
                            testquot (qstring, qname);
                    }
         }
         else if (strchr (qfeld, ','))
         {
                    anz = zsplit (qfeld, ',');
                    if (anz > 1)
                    {
                            strcat (qstring, "in (");
                            strcat (qstring, zwort[1]);
                            for (i = 2; i <= anz; i ++)
                            {
                                  strcat (qstring, ",");
                                  strcat (qstring, zwort[i]);
                            }
                            strcat (qstring, ")");
                     }
         }
         else if (strchr (qfeld, ','))
         {
                    anz = zsplit (qfeld, ',');
                    if (anz > 1)
                    {
                            strcat (qstring, "in (");
                            strcat (qstring, zwort[1]);
                            for (i = 2; i <= anz; i ++)
                            {
                                  strcat (qstring, ",");
                                  strcat (qstring, zwort[i]);
                            }
                            strcat (qstring, ")");
                     }
         }
         else 
         {

                    strcat (qstring, "matches \"");
                    strcat (qstring, "*");
                    strcat (qstring, qfeld);
                    strcat (qstring, "*");
                    strcat (qstring, "\"");
         }
}


void KFST_CLASS::TestQueryTxtn (char *qname)
/**
Sqlstring fuer Texte zusammenbauen.
**/
{

         if (qname == NULL) return;

         if (strlen (qstring))
         {
                    strcat (qstring, " and");
         }
         strcat (qstring, " (");
         this->TestQueryTxt ("txt1", qname);
         strcat (qstring, " or ");
         this->TestQueryTxt ("txt2", qname);
         strcat (qstring, " or ");
         this->TestQueryTxt ("txt3", qname);
         strcat (qstring, " or ");
         this->TestQueryTxt ("txt4", qname);    
         strcat (qstring, " or ");
         this->TestQueryTxt ("txt5", qname);
         strcat (qstring, " or ");
         this->TestQueryTxt ("txt6", qname);
         strcat (qstring, " or ");
         this->TestQueryTxt ("txt7", qname);
         strcat (qstring, ")");
}

int KFST_CLASS::PrepareQuery (char *qstring)
/**
Query-Eingabe bearbeiten.
**/
{
	     char sqlstring [1000]; 

         if (qstring)
         {
                 sprintf (sqlstring ,
                           "select text_nr, " 
                            "txt1,"
                            "txt2,"
                            "txt3,"
                            "txt4,"
                            "txt5,"
                            "txt6,"
                            "txt7 "
				   "from kfst "
                            "where text_nr  > 0 and %s "
							"order by text_nr", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select text_nr, txt1, txt2 "
							"from kfst "
                            "where text_nr  > 0 order by text_nr");
         }
         cursor_ausw = prep_awcursor (sqlstring);
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}


int KFST_CLASS::ShowAllBu (HWND hWnd, int ws_flag)
/**
Auswahl ueber Gruppen anzeigen.
**/
{
         int cursor_ausw;

         cursor_ausw = prep_awcursor ();
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         fill_aw (cursor_ausw);

         this->DB_CLASS::ShowAllBu (hWnd,
                                    ws_flag,
                                    cursor_ausw, 
                                    endsort,  
                                    IsAwClck,
                                    (char *) sort_awtab,
                                    sort_awanz,
                                    (char *) &sort_aw,
                                    (int) sizeof (struct SORT_AW),
                                    &fsort_aw,  
                                    &fsort_awl,
                                    &fsort_awub);
	  sort_idx = sort_awtab[sort_idx].sortidx;
        fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
        close_sql (cursor_ausw);
        return (0);
}




void KFST_CLASS::fill_aw (int cursor_ausw)
/**
Struktur fuer Auswahl fuellen.
**/
{
        static char *txttab[] = {kfst.txt1,
                                 kfst.txt2,
                                 kfst.txt3,
                                 kfst.txt4,
                                 NULL,
                                 kfst.txt5,
                                 kfst.txt6,
                                 kfst.txt7,
                                  NULL};
        int zeile = 0;
        int sortidx = 0;
        int dsqlstatus;
        
	    sort_awanz = 0;
	    while (fetch_scroll (cursor_ausw, NEXT) == 0)
		{

//  Kopftexte

            for (zeile = 0; txttab[zeile]; zeile ++)
            {
                    clipped (txttab[zeile]);
                    if (zeile > 0 && strcmp (txttab[zeile], " ") <= 0) 
                    {
                         break; 
                    }
                    if (zeile == 0)
                    {
			             sprintf (sort_awtab[sort_awanz].sort1, "%ld",
						      kfst.text_nr);
                          sortidx ++;
                    }
				    strcpy (sort_awtab[sort_awanz].sort2, 
						    txttab[zeile]);
				    sort_awtab[sort_awanz].sortidx = sortidx;
				    sort_awanz ++;
                    if (sort_awanz == MAXSORT) break;
            }

//  Fusstexte holen 

            kfst.text_nr += 50;
            dsqlstatus = this->dbreadfirst ();
            if (dsqlstatus == 0)
            {
               for (zeile = 0; txttab[zeile]; zeile ++)
               {
                    clipped (txttab[zeile]);
                    if (zeile > 0 && strcmp (txttab[zeile], " ") <= 0) 
                    {
                         break; 
                    }
				    strcpy (sort_awtab[sort_awanz].sort2, 
						    txttab[zeile]);
				    sort_awtab[sort_awanz].sortidx = sortidx;
				    sort_awanz ++;
                    if (sort_awanz == MAXSORT) break;
               }
            }

 	        sort_awtab[sort_awanz].sortidx = sortidx;
            if (sort_awanz == MAXSORT) break;
            sort_awanz ++;
            if (sort_awanz == MAXSORT) break;
		}
        return;
}


void KFST_CLASS::ReadQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         int i;

         qstring[0] = (char) 0;

         for (i = 0; i < qform->fieldanz; i ++)
         {
                   if (strcmp (qnamen[i], "txtn"))
                   {
                            TestQueryField 
                              (qform->mask[i].item->GetFeldPtr (),
                               qnamen[i]);
                   }
                   else
                   {
                            TestQueryTxtn (qform->mask[i].item->GetFeldPtr ());
                   }
         }
}

int KFST_CLASS::PrepareQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
	     char sqlstring [1000]; 

         this->ReadQuery (qform, qnamen); 

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select text_nr, "
							"txt1,"
                            "txt2,"
                            "txt3,"
                            "txt4,"
                            "txt5,"
                            "txt6,"
                            "txt7 "
							"from kfst "
                            "where text_nr  > 0 and %s "
							"order by text_nr", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select text_nr, txt1, txt2 "
							"from kfst "
                            "where text_nr  > 0 order by text_nr");
         }
         cursor_ausw = prep_awcursor (sqlstring);
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}

int KFST_CLASS::ShowAllBu (HWND hWnd, int ws_flag,
                         int cursor_ausw, 
                         int (*endsort) (void),  
                         void (*IsAwClck) (int),
                         char *sort_awtab,
                         int sort_awanz,
                         char *sort_aw,
                         int size,
                         form *fsort_aw,
                         form *fsort_awl,
                         form *fsort_awub)  
/**
Auswahl ueber Gruppen anzeigen.
**/
{
	    form *savecurrent;
	    HWND eWindow;
		
		savecurrent = current_form;
	    save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
		SetHLines (0);
        eWindow = OpenListWindowEnF (18, 60, 5, 10, ws_flag);
        ElistVl (fsort_awl);
        ElistUb (fsort_awub);
		Setlistenter (1);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) sort_aw,
                   (int) size,
                   fsort_aw);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) sort_aw,
                             (int) size,
                             fsort_aw);
        current_form = savecurrent;
        restore_fkt (5); 
	    SetMouseLock (FALSE);
      //  CloseControls (fsort_awub); 
        CloseUbControls (); 
        CloseEWindow (eWindow);
        set_fkt (NULL, 5);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
        }
        return 1;
}


int KFST_CLASS::ShowBuQuery (HWND hWnd, int ws_flag, 
								           char *sqlstring )
/**
Auswahl ueber Gruppen anzeigen.
**/
{
         extern short sql_mode;
         short old_mode;
         int cursor_ausw;

         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prep_awcursor (sqlstring);
         sql_mode = old_mode;
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         fill_aw (cursor_ausw);

//         this->DB_CLASS::ShowAllBu (hWnd,
         this->ShowAllBu           (hWnd,
                                    ws_flag,
                                    cursor_ausw, 
                                    endsort,  
                                    IsAwClck,
                                    (char *) sort_awtab,
                                    sort_awanz,
                                    (char *) &sort_aw,
                                    (int) sizeof (struct SORT_AW),
                                    &fsort_aw,  
                                    &fsort_awl,
                                    &fsort_awub);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 1;
        }
	    sort_idx = sort_awtab[sort_idx].sortidx;
        fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
        close_sql (cursor_ausw);
        return (0);
}

int KFST_CLASS::QueryBu (HWND hWnd, int ws_flag)
/**
Auswahl mit Query-String.
**/
{
	     char sqlstring [1000]; 

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select text_nr, "
							"txt1,"
                            "txt2,"
                            "txt3,"
                            "txt4,"
                            "txt5,"
                            "txt6,"
                            "txt7 "
							"from kfst "
                            "where text_nr  > 0 and text_nr < 50 "
                            "and %s "
							"order by text_nr", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select text_nr, txt1, txt2 "
							"from kfst "
                            "where text_nr  > 0 and text_nr < 50 "
                            "order by text_nr");
         }
         return this->ShowBuQuery (hWnd, ws_flag, sqlstring);
}

