# Microsoft Developer Studio Project File - Name="kfst" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=kfst - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "kfst.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "kfst.mak" CFG="kfst - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "kfst - Win32 Release" (basierend auf\
  "Win32 (x86) Console Application")
!MESSAGE "kfst - Win32 Debug" (basierend auf\
  "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "kfst - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "kfst___W"
# PROP BASE Intermediate_Dir "kfst___W"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "kfst___W"
# PROP Intermediate_Dir "kfst___W"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "kfst - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "kfst___0"
# PROP BASE Intermediate_Dir "kfst___0"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "kfst___0"
# PROP Intermediate_Dir "kfst___0"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /Zp1 /W3 /Gm /GX /Zi /Od /I "e:\informix\incl\esql" /I "e:\user\teis\cs\include" /D "WIN32" /D "_DEBUG" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib isqlt07c.lib /nologo /subsystem:console /debug /machine:I386 /out:"c:\user\fit\bin\kfst.exe" /pdbtype:sept /libpath:"e:\informix\lib"

!ENDIF 

# Begin Target

# Name "kfst - Win32 Release"
# Name "kfst - Win32 Debug"
# Begin Source File

SOURCE=.\A_BAS.CPP
# End Source File
# Begin Source File

SOURCE=.\A_BAS.H
# End Source File
# Begin Source File

SOURCE=.\atst.h
# End Source File
# Begin Source File

SOURCE=.\COMCTHLP.H
# End Source File
# Begin Source File

SOURCE=..\libsrc\dbclass.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\DBFUNC.CPP
# End Source File
# Begin Source File

SOURCE=.\DBFUNC.H
# End Source File
# Begin Source File

SOURCE=.\eti_typ.h
# End Source File
# Begin Source File

SOURCE=.\inflib.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\itemc.cpp
# End Source File
# Begin Source File

SOURCE=.\kfst.cpp
# End Source File
# Begin Source File

SOURCE=.\kfst.h
# End Source File
# Begin Source File

SOURCE=.\kfstpfleg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MDN.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mdnfil.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\message.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_CURSO.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_CURSO.H
# End Source File
# Begin Source File

SOURCE=.\mo_dbspez.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_DRAW.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MELD.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_MELD.H
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MENU.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_PASSW.H
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_progcfg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_send.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_send.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\STDFKT.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\STRFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\TCURSOR.H
# End Source File
# Begin Source File

SOURCE=..\libsrc\wmaskc.cpp
# End Source File
# End Target
# End Project
