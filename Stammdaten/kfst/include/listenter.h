#ifndef _LISTENTER_DEF
#define _LISTENTER_DEF

#define MAXLEN 40
#include "mo_intp.h"
#include "wmaskc.h"
#include "listclex.h"
#include "cmask.h"
#include "mo_progcfg.h"


class LISTENTER
{
            private :
                BOOL cfgOK;
                PROG_CFG *ProgCfg;
			protected :
                HINSTANCE  hMainInst;
                int ListFocus;
                static BOOL NoRecNr;
                BOOL FormOK;
 	            BOOL SetOK;

                HWND hMainWin;
                unsigned char *ausgabebuffer;
                HWND mamain1;
                HWND eWindow;
                PAINTSTRUCT aktpaint;

                double RowHeight;
                int UbHeight;

                TEXTMETRIC textm; 


                int mamainmax;
                int mamainmin;
                HWND    hwndTB;
                char *eingabesatz;
                unsigned char *ausgabesatz;
                int zlen;
                int feld_anz;
                int banz;
                char *SwSaetze[30000];
                int PageView;
                unsigned char DlgSatz [20 + MAXLEN + 2];
                FELDER *LstZiel;
                unsigned char *LstSatz;
                int Lstzlen;
                int Lstbanz;
                int SwRecs;
                int AktRow;
                int AktColumn;
                int scrollpos;
                TEXTMETRIC tm;
                char InfoCaption [80];
                int WithMenue;
                int WithToolBar;
                BOOL ListAktiv;
				HWND hMainWindow;
				int Posanz;
				int liney;
                HMENU hMenu;
                form *ubform;
                form *lineform;
				form *dataform;
                int *ubrows; 
                ListClassDB eListe;
                DWORD WinStyle;
                BOOL DockWin;
                int DockY;
                int DockDiff;
                int winX;
                int winY;
                int winCX;
                int winCY;
                BOOL Modal;
                CFORM *fWork;
                static HWND MessWindow;
                static char MessText [];
				void *Dlg;
                int initlist;
                static BOOL NoRecNrSet;
                int DockButtonSpace;
                int DockButtonSpaceP;

             public :
                static void SetMax0 (int);
                static void SetMin0 (int);
                static void PrintMess (char *);
                LISTENTER ();
                ~LISTENTER ();

 	            void SetDlg (void *Dlg)
				{
					 this->Dlg = Dlg;
				}

                void SetModal (BOOL Modal)
                {
                    this->Modal = Modal;
                }

                void SetDockButtonSpace (int sp)
                {
                    DockButtonSpace = sp;
                }

                int GetDockButtonSpace (void)
                {
                    return DockButtonSpace;
                }

                void SetDockButtonSpaceP (int spP)
                {
                    DockButtonSpaceP = spP;
                }

                int GetDockButtonSpaceP (void)
                {
                    return DockButtonSpaceP;
                }

                BOOL GetModal (void)
                {
                    return Modal;
                }

                
                ListClass *GeteListe (void)
                {
                    return &eListe;
                }

                void SetWinStyle (DWORD WinStyle)
                {
                    this->WinStyle = WinStyle;
                }

                DWORD GetWinStyle (void)
                {
                    return WinStyle;
                }

                void SetDockWin (BOOL DockWin)
                {
                    this->DockWin = DockWin;
                }

                BOOL GetDockWin (void)
                {
                    return DockWin;
                }

                void SetDockY (int DockY)
                {
                    this->DockY = DockY;
                    DockWin = TRUE;
                }

                int GetDockY (void)
                {
                    return DockY;
                }

                void SetLocation (int x, int y)
                {
                     winX = x;
                     winY = y;
                     DockWin = FALSE;
                }

                void SetDimension (int cx, int cy)
                {
                     winCX = cx;
                     winCY = cy;
                     DockWin = FALSE;
                }

				void SetUbForm (form *ubform)
				{
					this->ubform = ubform;
				}

                void SetCaption (char *Caption)
                {
                    strcpy (InfoCaption, Caption);
                }

                char *GetInfoCaption (void)
                {
                    return (char *) InfoCaption;
                }

				form *GetUbForm (void)
				{
					return ubform;
				}

				void SetLineForm (form *lineform)
				{
					this->lineform = lineform;
				}

				form *GetLineForm (void)
				{
					return lineform;
				}

				void SetDataForm (form *dataform)
				{
					this->dataform = dataform;
				}

				form *GetDataForm (void)
				{
					return dataform;
				}

				void SetUbRows (int *ubrows)
				{
					this->ubrows = ubrows;
				}

				int *GetUbRows (void)
				{
					return ubrows;
				}

				void SetLiney (int ly)
				{
					liney = ly;
				}

                void SetMenue (int with)
                {
                    WithMenue = with;
                }

                void SetToolMenue (int with)
                {
                    WithToolBar = with;
                }

                void SetMax (void)
                {
                    mamainmax = 1;
                    SetMax0 (mamainmax);
                }

                void SetMin (void)
                {
                    mamainmin = 1;
                    SetMin0 (mamainmin);
                }

                void InitMax (void)
                {
                    mamainmax = 0;
                    SetMax0 (mamainmax);
                }

                void InitMin (void)
                {
                    mamainmin = 0;
                    SetMin0 (mamainmin);
                }


                BOOL IsListAktiv (void)
                {
                    return ListAktiv;
                }


				int GetPosanz (void)
				{
					return Posanz;
				}

                void SetMenu (HMENU hMenu)
                {
                    this->hMenu = hMenu;
                }

                void SetMessWindow (HWND);
                HWND GetMessWindow (void);
                void SetNewPicture (char *, char *);
                void SetNewLen (char *, int);
                void SetNewRow (char *, int);
                void SethMainWindow (HWND);
                HWND GetMamain1 (void);
                void TestMessage (void);
                BOOL TestInsCount (void);

                void InitSwSaetze (void);
                void SetStringEnd (char *, int);
                void ShowDB (void);
                void ReadDB (void);
                void SetSchirm (void);
                static int  GetListLine (char *);
                static int GetListColor (COLORREF *, char *);
                void GetCfgValues (void);
                void EnterList (void);
                void SetAktFocus (void);
                void ShowList (void);
                void SetWorkMode (BOOL);
                void DestroyWindows (void);
                void WorkList (void);
                void DestroyMainWindow (void);
                HWND CreateMainWindow (void);
                void MoveMamain1 ();
                void MaximizeMamain1 ();
                void SetMessColors (COLORREF, COLORREF);

                void SetFieldAttr (char *, int);
                void OrFieldAttr (char *, int);
                int  GetFieldAttr (char *);
                void SetRecHeight (void);

                void SethwndTB (HWND);
                void SethMenu (HMENU hMenu);
                void SetTextMetric (TEXTMETRIC *);
                void SetLineRow (int);
                void SetListLines (int); 
                void OnPaint (HWND, UINT, WPARAM, LPARAM);
                void MoveListWindow (void);
                void BreakList (void);
                void OnHScroll (HWND, UINT, WPARAM, LPARAM);
                void OnVScroll (HWND, UINT, WPARAM, LPARAM);
                void OnSize (HWND, UINT, WPARAM, LPARAM);
                void FunkKeys (WPARAM, LPARAM);
                int  GetRecanz (void);
                void SwitchPage0 (int);
                HWND Getmamain2 (void);
                HWND Getmamain3 (void);
                void SetFont (mfont *); 
                void SetListFont (mfont *);
                void ChoiseFont (mfont *);
                void FindString (void);
                void SetLines (int);
                int GetAktRow (void);
                int GetAktRowS (void);
                void SetColors (COLORREF, COLORREF);
                void SetListFocus (void);
 			    void PaintUb (void);

                void addField (form *, field *, int, int, int);
                int addFieldName (form *, field *, char *, int);
                void DelFormField (form *, int);
                void DelFormFieldEx (form *, int, int);
                void DelListField (char *);
                void DelEListField (char *);
                void SetFieldLen (char *, int);
                void SetUbHeight (void);
                int  GetUblen (char *);
                void DestroyFormField (form *, char *, int);
                void DestroyLine (char *, int);
                void ScrollUbRows (char *);
                void DestroyListField (char *);
                virtual void StartList (void);
                virtual void SetNoRecNr (void);
                virtual int FromMemory (int);
                virtual int ToMemory (int);
                virtual void InitRow (void);
                virtual void uebertragen (void);
                virtual void SetChAttr (int);
                virtual int DeleteLine (void);
                virtual void DoBreak (void);
                virtual int InsertLine (void);
                virtual int AppendLine (void);
                virtual int Schirm (void);
                virtual int SetRowItem (void);
                virtual void Prepare (void); 
                virtual int  Fetch (void); 
                virtual void Close (void); 
				virtual char *GetListTab (int);
				virtual void FromTab (int);
				virtual void ToTab (int);
				virtual unsigned char *GetDataRec (void);
				virtual int GetDataRecLen (void);
                virtual void CalcSum (void);
                virtual void FillDB (void);
};
#endif