#ifndef _WE_POS_DEF
#define _WE_POS_DEF

#include "dbclass.h"

struct WE_POS {
   double    a;
   double    abw_lief_proz;
   double    abw_rab_betr;
   double    abw_rab_nat;
   double    abw_rab_proz;
   double    abw_zusch_proz;
   long      akv;
   long      alarm_prio;
   double    anz_einh;
   long      bearb;
   long      best_blg;
   double    best_me;
   char      best_zuord_kz[2];
   char      blg_typ[2];
   char      diff_kz[2];
   short     fil;
   long      hbk_dat;
   double    inh;
   long      int_pos;
   char      lief[17];
   char      lief_best[17];
   char      lief_rech_nr[17];
   long      lief_s;
   short     mdn;
   double    me;
   short     me_einh;
   short     mwst;
   short     p_num;
   char      pers_nam[9];
   char      pr_aend_kz[2];
   double    pr_ek;
   double    pr_ek_nto;
   double    pr_vk;
   char      qua_kz[2];
   double    rab_eff;
   char      rab_kz[2];
   char      sa_kz[2];
   char      skto_kz[2];
   double    tara;
   double    tara_proz;
   long      we_txt;
   char      sti_pro_kz[2];
   char      me_kz[2];
   long      a_krz;
   long      we_kto;
   short     buch_kz;
   double    pr_fil_ek;
   long      lager;
};
extern struct WE_POS we_pos, we_pos_null;

#line 7 "we_pos.rh"

class WE_POS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               WE_POS_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
