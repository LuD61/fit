#ifndef _COLBUT_DEF
#define _COLBUT_DEF
#include "wmaskc.h"

void SetColBorderM (BOOL);
HWND CreateColButton (HWND, ColButton *, int, int, int, int, HFONT, HINSTANCE, DWORD);
#endif
