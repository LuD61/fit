#ifndef _CONSOLE
extern char qstring [];

int StatusQuery ();
void testquot (char *, char *);
void TestQueryField (char *, char *);
void ReadQuery (form *, char *[]);
int DbAuswahl (short , void (*) (char *), void (*) (char *));
int DbAuswahlEx (short , void (*) (char *), void (*) (char *), form *);
void SetDbAwSpez (int (*) (short, void (*) (char *), void (*) (char *)));
#endif
