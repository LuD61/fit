#ifndef _WE_KOPF_DEF
#define _WE_KOPF_DEF

#include "dbclass.h"

struct WE_KOPF {
   char      a_best_kz[2];
   double    abw_lief_zusch;
   double    abw_rab_betr;
   double    abw_rab_proz;
   double    abw_zusch_betr;
   double    abw_zusch_proz;
   long      akv;
   char      bsd_verb_kz[2];
   long      anz_alarm;
   long      anz_mang;
   long      anz_pos;
   long      bearb;
   long      best0_term;
   long      best1_term;
   long      best2_term;
   long      best3_term;
   long      best4_term;
   long      best5_term;
   long      best6_term;
   long      best7_term;
   long      best8_term;
   long      best9_term;
   char      blg_typ[2];
   short     fil;
   char      koresp_best0[17];
   char      koresp_best1[17];
   char      koresp_best2[17];
   char      koresp_best3[17];
   char      koresp_best4[17];
   char      koresp_best5[17];
   char      koresp_best6[17];
   char      koresp_best7[17];
   char      koresp_best8[17];
   char      koresp_best9[17];
   long      lfd;
   char      lief[17];
   long      lief0_term;
   long      lief1_term;
   long      lief2_term;
   long      lief3_term;
   long      lief4_term;
   long      lief5_term;
   long      lief6_term;
   long      lief7_term;
   long      lief8_term;
   long      lief9_term;
   char      lief_rech_nr[17];
   long      lief_s;
   double    lief_wrt_ab;
   double    lief_wrt_nto;
   double    lief_wrt_o;
   double    lief_zusch;
   short     mdn;
   double    mwst_ges;
   char      pers_nam[9];
   char      pr_erf_kz[2];
   double    rab_eff;
   double    rab_faeh_betr;
   double    skto;
   double    skto_faeh_betr;
   char      skto_kz[2];
   double    skto_proz;
   long      txt_nr;
   long      we_dat;
   short     we_status;
   double    zusch;
   long      fil_adr;
   long      lief_adr;
   long      blg_eing_dat;
   char      lief_ink[17];
   long      lief_ink_s;
   double    ink_proz;
   double    ink_zusch;
   double    vk_wrt;
   short     buch_kz;
   double    rab_eff_lief;
   double    rab_eff_a;
   double    fil_ek_wrt;
};
extern struct WE_KOPF we_kopf, we_kopf_null;

#line 7 "we_kopf.rh"

class WE_KOPF_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               WE_KOPF_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
