int DCcopy (LPSTR, LPSTR);
int WaitConsoleExec (LPSTR, DWORD);
int ConsoleExec (LPSTR, DWORD);
int ProcExec (LPSTR, WORD, int, int, int, int);
HANDLE ProcExecPid (LPSTR, WORD, int, int, int, int);
void ClosePid (HANDLE);
DWORD WaitPid (HANDLE);
DWORD ProcWaitExec (LPSTR prog, WORD, int, int, int, int);
int CreateBatch (char *format, ...);
int AppendBatch (char *format, ...);
int RunBatch (char *format, ...);
int RunBatchPause (char *format, ...);

