#ifndef _ENTERFUNC_DEF
#define _ENTERFUNC_DEF
#include "mo_enter.h"

class EnterF {
          private :
              fEnter *Enter;
          public :
              EnterF ()
              {
              }
              ~EnterF ()
              {
              } 
              static int BreakOk (void);
              static int BreakCancel (void);
              static int BreakEnter (void);

              void EnterText (HWND, char *, char *);
              void EnterUpDown (HWND, char *, char *);
};
#endif
