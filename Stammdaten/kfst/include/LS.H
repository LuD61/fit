/*   Datenbankfunktionen                                  */

struct LSK {
   long      ls;
   long      adr;
   short     mdn;
   long      auf;
   short     kun_fil;
   long      kun;
   short     fil;
   char      feld_bz1[20];
   long      lieferdat;
   char      lieferzeit[6];
   char      hinweis[49];
   short     ls_stat;
   char      kun_krz1[17];
   double    auf_sum;
   char      feld_bz2[12];
   double    lim_er;
   char      partner[37];
   long      pr_lst;
   char      feld_bz3[8];
   short     pr_stu;
   long      vertr;
   long      tou;
   char      adr_nam1[37];
   char      adr_nam2[37];
   char      pf[17];
   char      str[37];
   char      plz[9];
   char      ort1[37];
   double    of_po;
   short     delstatus;
   long      rech;
   char      blg_typ[2];
   double    zeit_dec;
   long      kopf_txt;
   long      fuss_txt;
   long      inka_nr;
   char      auf_ext[17];
   short     teil_smt;
   char      pers_nam[9];
   double    brutto;
   long      komm_dat;
   double    of_ek;
   double    of_po_euro;
   double    of_po_fremd;
   double    of_ek_euro;
   double    of_ek_fremd;
   short     waehrung;
   char      ueb_kz[2];
   short     gruppe;
   short     ccmarkt;
   short     auf_art;
   short     fak_typ;
   long      tou_nr;
};


struct LSP {
   short     mdn;
   short     fil;
   long      ls;
   double    a;
   double    auf_me;
   char      auf_me_bz[7];
   double    lief_me;
   char      lief_me_bz[7];
   double    ls_vk_pr;
   double    ls_lad_pr;
   short     delstatus;
   double    tara;
   long      posi;
   long      lsp_txt;
   short     sa_kz_sint;
   char      erf_kz[2];
   short     pos_stat;
   double    prov_satz;
   short     leer_pos;
   long      hbk_date;
   double    auf_me_vgl;
   char      ls_charge [21];
   double    ls_vk_euro;
   double    ls_vk_fremd;
   double    ls_lad_euro;
   double    ls_lad_fremd;
   double    rab_satz;
   short     me_einh_kun;
   short     me_einh;
   short     me_einh_kun1;
   double    auf_me1; 
   double    inh1; 
   short     me_einh_kun2;
   double    auf_me2; 
   double    inh2; 
   short     me_einh_kun3;
   double    auf_me3; 
   double    inh3; 
};

struct AUFK {
   short     mdn;
   short     fil;
   long      ang;
   long      auf;
   long      adr;
   short     kun_fil;
   long      kun;
   long      lieferdat;
   char      lieferzeit[6];
   char      hinweis[49];
   short     auf_stat;
   char      kun_krz1[17];
   char      feld_bz1[20];
   char      feld_bz2[12];
   char      feld_bz3[8];
   short     delstatus;
   double    zeit_dec;
   long      kopf_txt;
   long      fuss_txt;
   long      vertr;
   char      auf_ext[17];
   long      tou;
   char      pers_nam[9];
   long      komm_dat;
   long      best_dat;
   short     waehrung;
   short     gruppe;
   short     ccmarkt;
   short     auf_art;
   short     fak_typ;
   long      tou_nr;
};

struct AUFP {
   short     mdn;
   short     fil;
   long      auf;
   long      posi;
   long      aufp_txt;
   double    a;
   double    auf_me;
   char      auf_me_bz[7];
   double    lief_me;
   char      lief_me_bz[7];
   double    auf_vk_pr;
   double    auf_lad_pr;
   short     delstatus;
   short     sa_kz_sint;
   double    prov_satz;
   long      ksys;
   long      pid;
   long      auf_klst;
   short     teil_smt;
   short     dr_folge;
   double    auf_vk_euro;
   double    auf_vk_fremd;
   double    auf_lad_euro;
   double    auf_lad_fremd;
   double    inh;
   double    rab_satz;
   short     me_einh_kun;
   short     me_einh;
   short     me_einh_kun1;
   double    auf_me1; 
   double    inh1; 
   short     me_einh_kun2;
   double    auf_me2; 
   double    inh2; 
   short     me_einh_kun3;
   double    auf_me3; 
   double    inh3; 
   long      gruppe;
};


extern struct LSK  lsk,  lsk_nuul;
extern struct LSP  lsp,  lsp_null;
extern struct AUFK aufk, aufk_null;
extern struct AUFP aufp, aufp_null;

class LS_CLASS
{
       private :

//  Cursor fuer lsk und lsp

            char lsstring0[0x1000];
            int lschange;
            short cursor_lsk;
            short cursor_lsk_auf;
            short test_lsk_upd;
            short cursor_lsk_auf_ausw;
            short cursor_lsk_upd;
            short cursor_lsk_ins;

            short cursor_lsp;
            short cursor_lsp_ap;
            short test_lsp_upd;
            short cursor_lsp_upd;
            short cursor_lsp_ins;
            short cursor_lsp_del;
            short cursor_lsp_dells;

//  Cursor fuer aufk und aufp

            char aufstring0[0x1000];
            int aufchange;
            short cursor_aufk;
            short test_aufk_upd;
            short cursor_aufk_upd;
            short cursor_aufk_ins;
            short cursor_aufk_del;

            short cursor_aufp;
            short cursor_aufp_o;
            short cursor_aufp_ap;
            short cursor_aufp_a;
            short test_aufp_upd;
            short cursor_aufp_upd;
            short cursor_aufp_ins;
            short cursor_aufp_del;
            short cursor_aufp_delauf;
            short cursor_ausw;

       public:
           LS_CLASS ()
           {
                    strcpy (lsstring0, "Start");
                    strcpy (aufstring0, "Start");
                    cursor_lsk = -1;
                    cursor_lsk_auf = -1;
                    cursor_lsp = -1;
                    cursor_lsp_ap = -1;
                    test_lsp_upd = -1;
                    cursor_lsk_upd = -1;
                    cursor_lsp_upd = -1;
                    cursor_lsp_ins = -1;
                    cursor_lsp_dells = -1;

                    cursor_aufk = -1;
                    cursor_aufp = -1;
                    cursor_aufp_o = -1;
                    cursor_aufp_ap = -1;
                    cursor_aufp_a = -1;
                    cursor_aufk_upd = -1;
                    cursor_aufk_ins = -1;
                    cursor_aufk_del = -1; 
                    cursor_aufp_upd = -1;
                    cursor_aufp_ins = -1;
                    cursor_aufp_del = -1;
                    cursor_aufp_delauf = -1;
                    cursor_ausw = -1;
           }

           void prepare_aufk (void);
           void prepare_aufk_upd (void);
           void prepare_aufk_del (void);
           void prepare_aufp (void);
           void prepare_aufp (char *);
           void prepare_aufp_ap (void);
           void prepare_aufp_a (void);
           void prepare_aufp_upd (void);
           void prepare_aufp_del (void);

           void prepare (void);
           void prepare_lsp (char *);
           void prepare_lsp_ap (void);
           void prepare_lsk_upd (void);
           void prepare_lsp_upd (void);
           void prepare_lsp_del  (void);

           int lese_lsk (short, short, long);
           int lese_lsk (void);
           int lese_lsk_auf (short, short, long);
           int lese_lsk_auf (void);
           int update_lsk (short, short, long);
           int lock_lsk (short, short, long);
           int lese_lsp (short, short, long, char *);
           int lese_lsp (void);
           void close_lsp (void);
           int lese_lsp_ap (short, short, long, double, long);
           int lese_lsp_ap (void);
//           int lese_lsp_a (short, short, long, double);
//           int lese_lsp_a (void);
           int lese_lsp_upd (void);
           int delete_lspls (short, short, long);
           int Auswahl_lsk_aufQuery (void);
           int Showlsk (char *);
           // int delete_lsk (short, short, long);
           int update_lsp (short, short, long, double, long);
           int lock_lsp (short, short, long, double, long);

           int ShowBuAufQuery (HWND, int); 
           int PrepareAufQuery (form *, char **);

           int ShowBuAufQueryEx (HWND, int); 
           int PrepareAufQueryEx (form *, char **);
		   void ListToMamain1 (HWND);

           int lese_aufk (short, short, long);
           int lese_aufk (void);
           void close_aufk (void);
           int update_aufk (short, short, long);
           int lock_aufk (short, short, long);
           int delete_aufk (short, short, long);
           int delete_auf (short, short, long);
           void close_aufk_del (void);
           int lese_aufp (short, short, long);
           int lese_aufp (void);
           void close_aufp (void);
           int lese_aufp_o (short, short, long, char *);
           int lese_aufp_o (void);
           void close_aufp_o (void);
           int lese_aufp_ap (short, short, long, double, long);
           int lese_aufp_ap (void);
           int lese_aufp_a (short, short, long, double);
           int lese_aufp_a (void);
           void close_aufp_ap (void);
           int lese_aufp_upd (void);
           int update_aufp (short, short, long, double, long);
           int lock_aufp (short, short, long, double, long);
           int delete_aufp (short, short, long, double, long);
           int delete_aufpauf (short, short, long);
           void close_aufp_delauf (void);
};


