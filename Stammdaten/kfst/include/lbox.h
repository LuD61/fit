#ifndef _LBOX_DEF
#define _LBOX_DEF
#include "image.h"

#define LB_CAPTSIZE  8998
#define LB_ROWSIZE   8999

void SetMenSelectM (BOOL);
void RegisterLboxM (HWND hWnd);
void SetListFontM (BOOL);
void SetMenStartM (int);
void Settchar (char);
char Gettchar (void);
void SetImage (IMG **);
#endif
