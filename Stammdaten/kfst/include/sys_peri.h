#ifndef SYS_PERI_DEF
#define SYS_PERI_DEF
#include "dbclass.h"
//#include "sys_peri.h"


//21.07.10 Tabelle konfig_pfl

struct KONFIG_PFL {
	   char a_pfl[2];
	   char a_modif[2];
       char a_txt_kz[2];
       char bed_nam_kz[2];
       char bon_kz[2];
       char dat_uhr_kz[2];
       short fil;
       char freq_ber_kz[2];
       short mdn;
       char mwst_kz[2];
       long sys;
       char waa_nam_kz[2];
       char waa_ref_kz[2];
       char wg_kz[2];
       char a_prot[2];
       char bilder[2];
};


class KONFIG_PFL_CLASS 
{
           private :
                  int cursor_fil_pfl;
                  int cursor_sys_pfl;
                  void prepare_fil_pfl  (void);
                  void prepare_sys_pfl  (void);
                  void out_quest_all_pfl  (void);
                  void close_fil_pfl (void);
                  void close_sys_pfl (void);
                
           public :
           KONFIG_PFL_CLASS ()
           {
                    cursor_fil_pfl  = -1;
                    cursor_sys_pfl  = -1;
           }
           int lese_fil_pfl (short);
           int lese_sys_pfl (long);
};


struct MODARR {
       long sys;
       long baudrate;
       short anschluss;
       short status;
       char modem_name[12];
       char dev[21];
       char tel[17];
       char modem_typ[3];
       short uebstatus;
};

struct ROUTARR {
       long sys;
       short nr;
       short kanal;
       short status;
       char router_name[21];
       char ip[21];
       char mask[21];
       short uebstatus;
	   int  err;
	   char tel[21];
};

struct SYSARR {
       long sys;
	   int  fil;
       long baudrate;
       short anschluss;
       short status;
       int  pid;
       char * dev;
       char * tel;
       char * modem_typ;
};

struct DFUE_PID {
   long      sys;
   long      pid;
   short     baudrate;
   short     anschluss;
   char      modem_typ[3];
   char      tty[21];
   short     peri_typ;
   short     status;
};

struct ROUTER_PID {
   short     nr;
   short     kanal;
   short     status;
   char      router_name[21];
   char      ip[21];
   char      mask[21];
};

struct SYS_PERI {
   short     anschluss;
   short     anz_waehl_wied;
   short     auszeit;
   short     baudrate;
   long      dat_abruf;
   long      dat_update;
   short     daten_bit;
   long      drv_pid;
   short     fil;
   short     kanal;
   short     mdn;
   char      modem_typ[3];
   char      parity[2];
   char      peri_nam[21];
   short     peri_typ;
   short     port_typ;
   short     protokoll;
   short     retry_max;
   short     stat;
   short     status_abruf;
   short     stop_bit;
   long      sys;
   short     sys_abruf;
   short     sys_update;
   char      tel[17];
   char      term[13];
   char      txt[61];
   char      zeit_abruf[7];
   char      b_check[2];
   short     la_zahl;
};

struct DAT_ABS
{
       long akt_tag;
        char a_heute [2];
};
 
struct KONFIG_TAG {
   char      a_modif[2];
   char      a_sum_kz[2];
   char      bed_sum_kz[2];
   char      err_kz[2];
   short     fil;
   char      freq_ums_kz[2];
   char      kase_sum_kz[2];
   char      kasi_sum_kz[2];
   char      lng_bon_kz[2];
   short     mdn;
   char      mwst_kz[2];
   long      sys;
   char      verk_sum_kz[2];
   char      waa_sum_kz[2];
   char      wg_sum_kz[2];
   short     auto_abruf;
   char      pers_zeit_kz [2];
   char      ret_kz [2];
   char      bestell_kz [2];
   char      treu_kz [2];
   char      lb_inv_kz [2];
   char      lb_ret_kz [2];
};

struct OPT_CLS {
   long      a_anz;
   char      a_sp_kz[2];
   long      anz_txt;
   char      dfue_passwd[33];
   short     fil;
   char      hbk_kz[2];
   long      max_a;
   short     mdn;
   char      mwst_sp_kz[2];
   char      pr_ek_kz[2];
   char      pr_sond_kz[2];
   char      pr_ueb[2];
   short     stnd_sg;
   char      sum_kun_bon[2];
   long      sys;
   short     zus_txt;
};

struct ABS_STAT {
   long      sys;
   short     peri_typ;
   char      par[49];
   short     status;
   long      sqlstat;
};

extern struct SYS_PERI sys_peri, sys_peri_null;
extern struct DAT_ABS dat_abs, dat_abs_null;
extern struct KONFIG_TAG konfig_tag, konfig_tag_null;
extern struct OPT_CLS opt_cls, opt_cls_null;
extern struct ABS_STAT abs_stat, abs_stat_null;


class SYS_PERI_CLASS
{
       private :
            short cursor_sys;
            short cursor_mdn;
            short cursor_fil;
            short cursor_sys_peri;
            short cursor_sys_peri_upd_mod;

            void out_quest_all (void);
            void prepare_sys (void);       
            void prepare_mdn (void);       
            void prepare_fil (void);       
            void prepare_sys_peri (char *);       
            void prepare_sys_peri_upd_mod (void);       
       public:
           SYS_PERI_CLASS ()
           {
                    cursor_sys      = -1;
                    cursor_mdn      = -1;
                    cursor_fil      = -1;
                    cursor_sys_peri = -1;
                    cursor_sys_peri_upd_mod = -1;
           }
           int lese_sys (long);
           int lese_sys (void);
           int lese_mdn (short);
           int lese_mdn (void);
           int lese_fil (short, short);
           int lese_fil (void);
           int lese_sys_peri (char *);
           int lese_sys_peri (void);
           int upd_sys_peri_mod (void);


           void close_sys (void);
           void close_mdn (void);
           void close_fil (void);
           void close_sys_peri (void);
           void close_sys_peri_upd_mod (void);
};


class DAT_ABS_CLASS 
{
           private :
                  int cursor_dat_abs;
                  int update_cursor;
                  int insert_cursor;
                  int delete_cursor;
                  void prepare (void);
                 
           public :
                  DAT_ABS_CLASS ()  
                  {
                             cursor_dat_abs = -1;
                  }

                  int Lesedat_abs (void);
                  int Updatedat_abs (void);
                  int Insertdat_abs (void);
                  int Deletedat_abs (void);
                  void Closedat_abs (void);
};


#ifndef _NODBCLASS
class KONFIG_TAG_CLASS : public DB_CLASS 
{
            private :
                 void prepare (void);
            public :
                 KONFIG_TAG_CLASS () : DB_CLASS ()
                 {
                 }
                 int dbreadfirst (void);
};

class OPT_CLS_CLASS : public DB_CLASS 
{
            private :
                 void prepare (void);
            public :
                 OPT_CLS_CLASS () : DB_CLASS ()
                 {
                 }
                 int dbreadfirst (void);
};
#endif

class ABS_STAT_CLASS 
{
           private :
                  int cursor;
                  int cursor_par;
                  int update_cursor;
                  int insert_cursor;
                  int delete_cursor;
                  int test_upd_cursor;
                  void prepare (void);
                 
           public :
                  ABS_STAT_CLASS ()  
                  {
                             cursor = -1;
                  }

                  int dbreadfirst (void);
                  int dbread (void);
                  int dbreadfirstpar (void);
                  int dbreadpar (void);
                  int dbupdate (void);
                  int dbdelall (void);
};


#endif

            
