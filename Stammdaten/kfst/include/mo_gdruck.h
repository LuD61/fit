#ifndef _mo_gdruck_def
#define mo_gdruck_def
void SetPrnBreak (BOOL);
HDC GetPrinterDC (void);
int Gdruck (char *);
BOOL SetPage (void);
BOOL SelectPrinter (void);
char *GetNextPrinter (void);
void GetAllPrinters (void);
HDC GetPrinterbyName (char *);
char *GetPrinter (char *);
void SetPrinter (char *);
int IsGdiPrint (void);
void SetPersName (char *);
void ShowGdiPrinters (void);
void SetGdiPrinter (char *);
void SetpDevMode (DEVMODE *);
#endif	   
       

