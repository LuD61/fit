// WeExtraDataCollection.h: Schnittstelle f�r die Klasse CWeExtraDataCollection.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WEEXTRADATACOLLECTION_H__20369820_C11A_4187_8D9D_AF2043456EAE__INCLUDED_)
#define AFX_WEEXTRADATACOLLECTION_H__20369820_C11A_4187_8D9D_AF2043456EAE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "WeExtraData.h"
#include "DataCollection.h"


class CWeExtraDataCollection  
{
public:
	CDataCollection<CWeExtraData> Collection;
	CWeExtraDataCollection();
	virtual ~CWeExtraDataCollection();

	void Destroy ();
	void Add (CWeExtraData);
	CWeExtraData *Get (int idx);
	CWeExtraData *Find (int ListPos);
};

#endif // !defined(AFX_WEEXTRADATACOLLECTION_H__20369820_C11A_4187_8D9D_AF2043456EAE__INCLUDED_)
