#pragma once
#include "afxwin.h"
#include "FormTab.h"
#include "CtrlGrid.h"
#include "DbUniCode.h"
#include "StaticButton.h"
#include "ptabn.h"
#include "ExtraData.h"
#include "ExtraDataMfc.h"
#include "TextEdit.h"


#define IDC_SAVE 3010
#define IDC_DELETE 3011


// CExtraDialog-Dialogfeld

class CExtraDialog : public CDialog
{
	DECLARE_DYNAMIC(CExtraDialog)

public:
	CExtraDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CExtraDialog();

// Dialogfelddaten
	enum { IDD = IDD_EXTRA_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnDestroy ();

	DECLARE_MESSAGE_MAP()
public:
    CExtraData ExtraData;
	CFont Font;
	CStaticButton m_Save;
	CStaticButton m_Delete;
	CStatic m_LMultiCharge;
	CComboBox m_MultiCharge;
	CStatic m_LEz;
	CTextEdit m_Ez;
	CStatic m_LEs;
	CTextEdit m_Es;
	CStatic m_LCountries;
	CComboBox m_Countries;
	CStatic m_LIdentNr;
	CTextEdit m_IdentNr;
	CStatic m_ExtraGroup;
	PTABN_CLASS Ptabn;
	CCtrlGrid CtrlGrid;
	CCtrlGrid ToolGrid;
	CFormTab Form;
	afx_msg void OnSave ();
	afx_msg void OnDelete ();
	virtual BOOL OnReturn ();
	virtual BOOL OnTab ();
	virtual BOOL OnKeyup ();
	CWnd *GetNextDlgItem (CWnd *control, BOOL Dir);

	void SetExtraData (CExtraData *ExtraData);
	void GetExtraData (CExtraData *ExtraData);
	void FillCombo (LPTSTR Item, CWnd *control);
};
