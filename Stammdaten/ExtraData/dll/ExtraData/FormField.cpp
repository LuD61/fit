#include "StdAfx.h"
#include "formfield.h"
#include "StrFuncs.h"
#include "DecimalEdit.h"
#include "RichTextEdit.h"

CFormField::CFormField(void)
{
	Control  = NULL;
	CtrlType = EDIT;
	Vadr     = NULL;
	VType    = VCHAR;
	Scale = 0;
	len = 0;
}

CFormField::CFormField(CWnd *Control, int CtrlType, void *Vadr, int VType)
{
	this->Control  = Control;
	this->CtrlType = CtrlType;
	this->Vadr     = Vadr;
	this->VType    = VType;
	Scale = 0;
}


CFormField::CFormField(CWnd *Control, int CtrlType, void *Vadr, int VType, int len, int Scale)
{
	this->Control  = Control;
	this->CtrlType = CtrlType;
	this->Vadr     = Vadr;
	this->VType    = VType;
	this->len      = len;
	this->Scale    = Scale;
	if (Control->IsKindOf (RUNTIME_CLASS (CDecimalEdit)))
	{
		((CDecimalEdit *) Control)->scale = Scale;
	}
}

CFormField::~CFormField(void)
{
	for (std::vector<CString *>::iterator valp = ComboValues.begin (); 
											valp != ComboValues.end ();
											valp ++)
	{
		CString *value = *valp;
		delete value;
	}
	ComboValues.clear ();
}

void CFormField::FillComboBox ()
{
	if (CtrlType != COMBOBOX && CtrlType != COMBOBOXEX)
	{
		return;
	}
	((CComboBox *) Control)->ResetContent ();
	for (std::vector<CString *>::iterator valp = ComboValues.begin (); 
											valp != ComboValues.end ();
											valp ++)
	{
		CString *value = *valp;
		((CComboBox *) Control)->AddString (value->GetBuffer (0));
	}
}

void CFormField::DestroyComboBox ()
{
	if (CtrlType != COMBOBOX && CtrlType != COMBOBOXEX)
	{
		return;
	}
	((CComboBox *) Control)->ResetContent ();
	for (std::vector<CString *>::iterator valp = ComboValues.begin (); 
											valp != ComboValues.end ();
											valp ++)
	{
		CString *value = *valp;
		delete value;
	}
	ComboValues.clear ();
}

BOOL CFormField::operator== (CWnd *Control)
{
	if (Control == this->Control)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL CFormField::equals (CWnd *Control)
{
	if (Control == this->Control)
	{
		return TRUE;
	}
	return FALSE;
}

void CFormField::SetSel (int idx)
{
	if (CtrlType != COMBOBOX && CtrlType != COMBOBOXEX)
	{
		return;
	}
	((CComboBox *) Control)->SetCurSel (idx);
}

void CFormField::SetTextSelected (CString& Text)
{
	if (CtrlType != COMBOBOX && CtrlType != COMBOBOXEX)
	{
		return;
	}
	int idx = ((CComboBox *) Control)->FindString (-1, Text.TrimRight ().GetBuffer (0));
	if (idx < 0) idx = 0;
	SetSel (idx);
}

BOOL CFormField::GetSelectedText (CString& Text)
{
	if (CtrlType != COMBOBOX && CtrlType != COMBOBOXEX)
	{
		Text = "";
		return FALSE;
	}
	int idx = ((CComboBox *) Control)->GetCurSel ();
	if (idx < 0) 
	{
		Text = "";
		return FALSE;
	}
    ((CComboBox *) Control)->GetLBText (idx, Text);
	return TRUE;
}

void CFormField::Show ()
{
	CString Text;
//	LPSTR pos;
//	LPTSTR text;

	switch (VType)
	{
	case VCHAR :
		Text = (LPTSTR) Vadr;
		break;
	case VSHORT :
		Text.Format (_T("%hd"), *((short *) Vadr));
		break;
	case VLONG :
		Text.Format (_T("%ld"), *((long *) Vadr));
		break;
	case VSTRING :
		Text = *(CString *) Vadr;
		break;
	case VDOUBLE :
		CString frm;
		frm.Format (_T("%c.%dlf"), '%', Scale);
		Text.Format (frm.GetBuffer (0), *((double *) Vadr));
		int pos = Text.Find ('.');
		if (pos != -1)
		{
			Text.GetBuffer ()[pos] = ',';
		}
		break;
	}
	Text.TrimRight ();
	if (CtrlType == EDIT)
	{
		    ((CEdit *) Control)->SetWindowText (Text);
	}
	else if (CtrlType == RICHTEXTEDIT)
	{
		    ((CRichTextEdit *) Control)->SetWindowText (Text);
	}
	else if (CtrlType == COMBOBOX)
	{
			SetTextSelected (Text);
	}
	else if (CtrlType == COMBOBOXEX)
	{
			SetTextSelected (Text);
	}
	else if (CtrlType == CHECKBOX)
	{
		if ((Text.CollateNoCase (_T("J")) == 0) ||
			Text.Trim ().Compare (_T("1")) == 0)
		{
			((CButton *) Control)->SetCheck (BST_CHECKED);
		}
		else
		{
			((CButton *) Control)->SetCheck (BST_UNCHECKED);
		}
	}
}

void CFormField::Get ()
{
	CString Text;
//	LPSTR p;
//	LPTSTR text;

	if (CtrlType == EDIT)
	{
		    ((CEdit *) Control)->GetWindowText (Text);
	}
	else if (CtrlType == RICHTEXTEDIT)
	{
		    ((CRichTextEdit *) Control)->GetWindowText (Text);
	}
	
	else if (CtrlType == COMBOBOX)
	{
			GetSelectedText (Text);
	}
	else if (CtrlType == COMBOBOXEX)
	{
			GetSelectedText (Text);
			int iStart = 0;
			Text = Text.Tokenize (_T(" "), iStart);
	}
	else if (CtrlType == CHECKBOX)
	{
		int checked = ((CButton *) Control)->GetCheck ();
		if (checked == BST_CHECKED)
		{
			if (VType == VCHAR)
			{
				Text = "J";
			}
			else
			{
				Text = "1";
			}
		}
		else if (checked == BST_UNCHECKED)
		{
			if (VType == VCHAR)
			{
				Text = "N";
			}
			else
			{
				Text = "0";
			}
		}
	}
	switch (VType)
	{
		case VCHAR :
			_tcscpy ((LPTSTR) Vadr, Text.GetBuffer ());
			break;
		case VSHORT :
			*((short*) Vadr) = _tstoi (Text.GetBuffer(0));
			break;
		case VLONG :
			*((long*) Vadr) =  _tstol (Text.GetBuffer(0));
			break;
		case VDOUBLE :
			*((double *) Vadr) = CStrFuncs::StrToDouble (Text);
			break;
		case VSTRING :
			*(CString *)Vadr = Text;
			break;
	}
}

void CFormField::OemToAnsi (CString &Str)
{
}

void CFormField::AnsiToOem (CString &Str)
{
}

void CFormField::OemToAnsi (LPSTR str)
{
}

void CFormField::AnsiToOem (LPSTR str)
{
}