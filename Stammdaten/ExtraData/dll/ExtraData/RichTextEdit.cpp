#include "StdAfx.h"
#include "richtextedit.h"
#include "Token.h"
#include ".\richtextedit.h"
#include "resource.h"

IMPLEMENT_DYNCREATE(CRichTextEdit, CRichEditCtrl)

CRichTextEdit::CRichTextEdit(void)
{

	SgTab.Add (new CPropertyItem (CString (_T("9")), CString (_T ("295")))); 
	SgTab.Add (new CPropertyItem (CString (_T("8")), CString (_T ("255")))); 
	SgTab.Add (new CPropertyItem (CString (_T("7")), CString (_T ("215")))); 
	SgTab.Add (new CPropertyItem (CString (_T("6")), CString (_T ("195")))); 
	SgTab.Add (new CPropertyItem (CString (_T("5")), CString (_T ("175")))); 
	SgTab.Add (new CPropertyItem (CString (_T("4")), CString (_T ("160")))); 
	SgTab.Add (new CPropertyItem (CString (_T("3")), CString (_T ("150")))); 
	SgTab.Add (new CPropertyItem (CString (_T("2")), CString (_T ("135")))); 
	lastSize = 9;
	Alignment = Left;
}

CRichTextEdit::~CRichTextEdit(void)
{
	CPropertyItem *p;
	SgTab.FirstPosition ();
	while ((p = (CPropertyItem *) SgTab.GetNext ()) != NULL)
	{
		delete p;
	}
	SgTab.Init ();
	CTxtSg *ts;
	TextFormat.FirstPosition ();
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		delete ts;
	}
	TextFormat.Init ();
}

void CRichTextEdit::DestroyFormat ()
{
	CTxtSg *ts;
	TextFormat.FirstPosition ();
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		delete ts;
	}
	TextFormat.Init ();
}

void CRichTextEdit::SetWindowText (LPCTSTR txt)
{
    DestroyFormat ();
	TextFormat.Add (new CTxtSg (NULL, 0, 0, 295));
	CToken t;
    t.SetSep (_T("\n"));
	t = CString (txt);
	int count = t.GetAnzToken ();

	CString nTxt = _T("");
	CString Txt;

	for (int row = 0; row < count; row ++)
	{
		 Txt = t.GetToken (row);
		 Txt.TrimRight ();
		 GetTextSg (row, &Txt);
		 if (row > 0)
		 {
			 nTxt += _T('\n');
		 }
		 nTxt += Txt;
	}

	CWnd::SetWindowText (nTxt.GetBuffer ());

	CTxtSg *ts;
	TextFormat.FirstPosition ();
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		int pos = 0;
		GetPos (pos, ts->row, ts->col);
		SetSel (pos, -1);
		CHARFORMAT2 cf;
		GetSelectionCharFormat (cf);
		cf.dwMask = CFM_SIZE;
		cf.yHeight = ts->size;
		SetSelectionCharFormat(cf);
	}
	SetSel (0, 0);
}

void CRichTextEdit::GetWindowText (CString& txt)
{
	CString Text;
	CWnd::GetWindowText (Text);

	DisplayText = Text;
	CToken t;
    t.SetSep (_T("\n"));
	t = Text;
	int count = t.GetAnzToken ();

	CString nTxt = _T("");
	CString Txt;

	for (int row = 0; row < count; row ++)
	{
		 Txt = t.GetToken (row);
		 Txt.TrimRight ();
		 GetTextSg (row, &Txt);
		 if (row > 0)
		 {
			 nTxt += _T('\n');
		 }
		 nTxt += Txt;
	}
	Text = nTxt;

	txt = Text;
	int size = TextFormat.GetCount (); 
	if (size == 0) return;
	try
	{
		SgPosis = new SgPos* [size + 2];
	}
	catch (...)
	{
		return;
	}
	CTxtSg *ts;
	TextFormat.FirstPosition ();
	int is = 0;
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		int pos = 0;
		GetPos (pos, ts->row, ts->col);
		CString S;
		S.Format (_T("%d"), ts->size);
		int i = SgTab.FindValue (S);
		if (i == -1) continue;
		CPropertyItem *p = (CPropertyItem *) SgTab.Get (i);
		CString Sg;
		Sg = _T("^");
		Sg += p->Name;
		Sg += _T(";");
		SgPosis[is] = new SgPos ();
		SgPosis[is]->pos = pos;
		SgPosis[is]->Sg = Sg;
		is ++;
	}

	qsort (SgPosis, size, sizeof (SgPos *), ComparePos);

	for (int i = 0; i < size; i ++)
	{
		CString Start = Text.Left (SgPosis[i]->pos);
		CString End = Text.Mid (SgPosis[i]->pos);
		Text = Start;
		Text += SgPosis[i]->Sg;
		Text += End;
	}
    txt = Text;
	for (int i = 0; i < size; i ++)
	{
		delete SgPosis [i];
	}
	delete SgPosis;

}


void CRichTextEdit::Copy ()
{
	CHARRANGE cr;
	CString Text;
	CString Sg;
	GetSel (cr);
	if (cr.cpMin == cr.cpMax) return;
    GetWindowText (Text); 
	int tps = 0;
	int tp = 0;
	for (tps = 0,tp = 0; tp <= cr.cpMin;tps ++)
	{
		if (Text.GetBuffer ()[tps] == _T('^'))
		{
			Sg = _T("^");
			tps ++;
			while (Text.GetBuffer ()[tps] != _T(';'))
			{
				Sg += Text.Mid (tps, 1);
				tps ++;
			}
            Sg += _T(";");
		}
		tp ++;
	}

	CString newText = _T("");
	newText += Sg;

	if (Text.Mid (tps,1) == _T("\n"))
	{
		tps ++;
	}

	for (; tp <= cr.cpMax;tps ++)
	{
		if (Text.GetBuffer ()[tps] == _T('^'))
		{
			newText += _T("^");
			tps ++;
			while (Text.GetBuffer ()[tps] != _T(';'))
			{
				newText += Text.Mid (tps, 1);
				tps ++;
			}
			newText += _T(";");
			tps ++;
		}
		newText += Text.Mid (tps, 1);
		tp ++;
	}

	ToClipboard (newText);
}

void CRichTextEdit::Paste ()
{
	CHARRANGE cr;
	GetSel (cr);
	CString Text;
	FromClipboard (Text);
	CString oldText;
	GetWindowText (oldText);
	CString newText = _T("");
	if (oldText.GetLength () == 0)
	{
		cr.cpMin = -1;
		cr.cpMax = -1;
	}

	int tps = 0, tp = 0;
	for (tps = 0, tp = 0; tp < cr.cpMin; tps ++)
	{
			if (oldText.GetBuffer ()[tps] == _T('^'))
			{
				newText += _T("^");
				tps ++;
				while (oldText.GetBuffer ()[tps] != _T(';'))
				{
					newText += oldText.Mid(tps, 1);
					tps ++;
				}
				newText += _T(";");
				tps ++;
			}
			newText += oldText.Mid(tps, 1);
			tp ++;
	}

	newText += Text;

	for (; tp <= cr.cpMax; tps ++)
	{
		if (oldText.GetBuffer ()[tps] == _T('^'))
		{
			tps ++;
			while (oldText.GetBuffer ()[tps] != _T(';'))
			{
				tps ++;
			}
			tps ++;
		}
		tp ++;
	}

    newText += oldText.Mid (tps); 
	SetWindowText (newText.GetBuffer ());
}


void CRichTextEdit::ToClipboard (CString& Text)
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	try
	{
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Text.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Text.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

void CRichTextEdit::FromClipboard (CString& Text)
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return;
    }

    HGLOBAL hglbCopy;
    LPTSTR data;

	try
	{
		 hglbCopy =  ::GetClipboardData(CF_TEXT);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
		 data = (LPTSTR) GlobalLock ((HGLOBAL) hglbCopy);
         Text = data;
		 GlobalUnlock ((HGLOBAL) hglbCopy);
	}
	catch (...) {};
    CloseClipboard();
}

void CRichTextEdit::SetDefaultSize (int size)
{
	CString DevSize;
	DevSize.Format (_T("%d"), size);
	int i = SgTab.Find (DevSize);
	if (i == -1) return;
	CPropertyItem *p = (CPropertyItem *) SgTab.Get (i);
	size = _tstoi (p->Value.GetBuffer ());
	CHARFORMAT2 cf;
	GetDefaultCharFormat (cf);
//	cf.dwMask = CFM_SIZE | CFM_FACE;
	cf.dwMask = CFM_SIZE;
	cf.yHeight = size;
//	_tcscpy (cf.szFaceName, _T("MS SAN SHERIF"));
	SetDefaultCharFormat(cf);
	TextFormat.Add (new CTxtSg (NULL, 0, 0, size));
}

void CRichTextEdit::SetSelectionCharSize (int size)
{
	CString DevSize;
	lastSize = size;
	DevSize.Format (_T("%d"), size);
	int i = SgTab.Find (DevSize);
	if (i == -1) return;
	CPropertyItem *p = (CPropertyItem *) SgTab.Get (i);
	size = _tstoi (p->Value.GetBuffer ());
	CHARRANGE cr;
	GetSel (cr);
//	if (cr.cpMin == cr.cpMax) return;

	CHARFORMAT2 cf;

	SetSel (cr.cpMax, cr.cpMax);
	GetSelectionCharFormat (cf);
	SetTextSg (cf.yHeight);
	SetSel (cr);

    SetTextSg (size);
	cf.dwMask = CFM_SIZE;
	cf.yHeight = size;
	SetSelectionCharFormat(cf);
}

void CRichTextEdit::SetTextSg (int size)
{
	CHARRANGE cr;
	GetSel (cr);

	int row = 0;
	int column = 0;

	GetLinePos (cr.cpMin, row, column);
	SetTextFormat (row, column, size);

	CHARFORMAT2 cf;
	GetSelectionCharFormat (cf);
	int actsize = cf.yHeight;
}

void CRichTextEdit::GetTextSg (int row, CString* Txt)
{
	TCHAR sg [5];
	CString Row = _T("");
    LPTSTR p = Txt->GetBuffer ();
    int length = Txt->GetLength ();

	int col = 0;
	for (int i = 0; i < length; i ++, p += 1)
	{
		if (*p == _T('^'))
		{
			p +=1;
			int j = 0;
			for (j = 0; (*p != _T(';')) && (*p != 0); p += 1, j ++)
			{
				if (j == 4) break;
				sg[j] = *p;
			}
            sg[j] = 0;
			int isg = SgTab.Find (CString (sg));
			if (isg > -1)
			{
				CPropertyItem *p = (CPropertyItem *) SgTab.Get (isg);
				int size = _tstoi (p->Value.GetBuffer ());
                SetTextFormat (row, col, size);
			}
			if (*p == 0) break;
		}
		else
		{
			Row += *p;
			col ++;
		}
	}
	*Txt = Row.GetBuffer ();
}

void CRichTextEdit::SetTextFormat (int row, int column, int size)
{
	CTxtSg *ts;
	TextFormat.FirstPosition ();
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		if (*ts == CPoint (column, row))
		{
			break;
		}
	}
	if (ts != NULL)
	{
		ts->size = size;
	}
	else
	{
		TextFormat.Add (new CTxtSg (NULL, row, column, size));
	}
}

void CRichTextEdit::GetLinePos (int pos, int& row, int& column)
{
	CString Text;
	CToken t;

	CWnd::GetWindowText (Text);
    t.SetSep (_T("\n"));
	t = Text;
	CString Line;
	int count = t.GetAnzToken ();
	int i = 0;
	for (row = 0; row < count; row ++)
	{
		Line = t.GetToken (row);
		i += Line.GetLength ();
		if (i > pos) break;
	}
    if (row == count)
	{
//		row --;
//		column = Line.GetLength ();
		column = 0;
	}
	else
	{
		i -= Line.GetLength ();
		column = pos - i;
	}
}

void CRichTextEdit::GetPos (int& pos, int row, int column)
{
	CString Text;
	CToken t;

	CWnd::GetWindowText (Text);
    t.SetSep (_T("\n"));
	t = Text;
	CString Line;
	int count = t.GetAnzToken ();
	int i = 0;
	pos = 0;
	for (i = 0; i < row; i ++)
	{
		Line = t.GetToken (i);
		pos += Line.GetLength ();
	}
	pos += column;
}

int __cdecl CRichTextEdit::ComparePos (const void *el1, const void *el2)
{
    SgPos *elem1 = *(SgPos **) el1; 
    SgPos *elem2 = *(SgPos **) el2; 

	if (elem1->pos > elem2->pos) return -1;
	if (elem1->pos < elem2->pos) return 1;
	return 0;
}
BEGIN_MESSAGE_MAP(CRichTextEdit, CRichEditCtrl)
	ON_COMMAND(ID_TEXT_CENT, OnTextCent)
	ON_COMMAND(ID_TEXT_LEFT, OnTextLeft)
	ON_COMMAND(ID_TEXT_RIGHT, OnTextRight)
END_MESSAGE_MAP()

void CRichTextEdit::OnTextCent()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CString Text;
	GetWindowText (Text);

	PARAFORMAT pf;
	memset (&pf, 0, sizeof (pf));
    pf.cbSize = sizeof (pf);
    pf.dwMask = PFM_ALIGNMENT;
    pf.wAlignment = PFA_CENTER;
	CHARRANGE cr;
	GetSel (cr);
	SetSel (0, -1);
	SetParaFormat (pf);
	SetSel (cr);
	Alignment = Center;
}

void CRichTextEdit::OnTextLeft()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	PARAFORMAT pf;
	memset (&pf, 0, sizeof (pf));
    pf.cbSize = sizeof (pf);
    pf.dwMask = PFM_ALIGNMENT;
    pf.wAlignment = PFA_LEFT;
	CHARRANGE cr;
	GetSel (cr);
	SetSel (0, -1);
	SetParaFormat (pf);
	SetSel (cr);
	Alignment = Left;
}

void CRichTextEdit::OnTextRight()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	PARAFORMAT pf;
	memset (&pf, 0, sizeof (pf));
    pf.cbSize = sizeof (pf);
    pf.dwMask = PFM_ALIGNMENT;
    pf.wAlignment = PFA_RIGHT;
	CHARRANGE cr;
	GetSel (cr);
	SetSel (0, -1);
	SetParaFormat (pf);
	SetSel (cr);
	Alignment = Right;
}

void CRichTextEdit::SetAlignment (int Alignment)
{
	switch (Alignment)
	{
	case Left:
		GetParent ()->PostMessage (WM_COMMAND, ID_TEXT_LEFT, 0l);
		break;
	case Center:
		GetParent ()->PostMessage (WM_COMMAND, ID_TEXT_CENT, 0l);
		break;
	case Right:
		GetParent ()->PostMessage (WM_COMMAND, ID_TEXT_RIGHT, 0l);
		break;
	}
}

void CRichTextEdit::GetText (CString& txt)
{
	CWnd::GetWindowText (txt);
}
