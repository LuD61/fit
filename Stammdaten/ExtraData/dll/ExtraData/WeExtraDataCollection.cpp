// WeExtraDataCollection.cpp: Implementierung der Klasse CWeExtraDataCollection.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WeExtraDataCollection.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CWeExtraDataCollection::CWeExtraDataCollection ()
{

}

CWeExtraDataCollection::~CWeExtraDataCollection ()
{
}


void CWeExtraDataCollection::Destroy ()
{
	if (Collection.Arr != NULL)
	{
		delete Collection.Arr;
		Collection.Arr = NULL;
	}
}


void CWeExtraDataCollection::Add (CWeExtraData Element)
{
	Collection.Add (Element);
}


CWeExtraData *CWeExtraDataCollection::Get (int idx)
{
	return Collection.Get (idx);
}

CWeExtraData *CWeExtraDataCollection::Find (int ListPos)
{
	CWeExtraData *d; 
	Collection.pos = 0;
	while ((d = Collection.GetNext ()) != NULL)
	{
		if (d->ListPos == ListPos)
		{
			return d;
		}
	}
	return NULL;
}
