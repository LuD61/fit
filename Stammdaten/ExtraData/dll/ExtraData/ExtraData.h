// ExtraData.h : Hauptheaderdatei f�r die ExtraData-DLL
//

#pragma once
#include "ExtraDataMfc.h"

#ifndef __AFXWIN_H__
	#error "'stdafx.h' vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"		// Hauptsymbole


#undef EXPORT
#define EXPORT extern "C" _declspec (dllexport)

EXPORT void Start (TCHAR *params[]);

// CExtraDataApp
// Siehe ExtraData.cpp f�r die Implementierung dieser Klasse
//

EXPORT CExtraData *ExtraData (CExtraData *);

class CExtraDataApp : public CWinApp
{
public:
	CExtraDataApp();

// �berschreibungen
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
