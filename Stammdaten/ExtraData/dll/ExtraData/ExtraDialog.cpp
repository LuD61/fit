// ExtraDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "ExtraData.h"
#include "ExtraDialog.h"

int StdCellHeight = 20;

// CExtraDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CExtraDialog, CDialog)

CExtraDialog::CExtraDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CExtraDialog::IDD, pParent)
{
	Ptabn.opendbase (_T("bws"));
	strcpy (ExtraData.EZNr, "");
	strcpy (ExtraData.ESNr, "");
	strcpy (ExtraData.IdentNr, "");
}

CExtraDialog::~CExtraDialog()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Ptabn.dbclose ();
}

void CExtraDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMULTI_CHARGE, m_LMultiCharge);
	DDX_Control(pDX, IDC_MULTI_CHARCHE, m_MultiCharge);
	DDX_Control(pDX, IDC_LEZ, m_LEz);
	DDX_Control(pDX, IDC_EZ, m_Ez);
	DDX_Control(pDX, IDC_LES, m_LEs);
	DDX_Control(pDX, IDC_ES, m_Es);
	DDX_Control(pDX, IDC_LCOUNTRIES, m_LCountries);
	DDX_Control(pDX, ID_COUNTIES, m_Countries);
	DDX_Control(pDX, IDC_LIDENT_NR, m_LIdentNr);
	DDX_Control(pDX, IDC_IDENT_NR, m_IdentNr);
	DDX_Control(pDX, IDC_EXTA_GROUP, m_ExtraGroup);
}


BEGIN_MESSAGE_MAP(CExtraDialog, CDialog)
	ON_WM_DESTROY () 
	ON_BN_CLICKED(IDC_SAVE ,   OnSave)
	ON_BN_CLICKED(IDC_DELETE , OnDelete)
END_MESSAGE_MAP()


// CExtraDialog-Meldungshandler

BOOL CExtraDialog::OnInitDialog()
{
	SCharge *Charge;

	CDialog::OnInitDialog();

    m_Save.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_SAVE);
	m_Save.LoadBitmap (IDB_SAVE);
	m_Save.SetToolTip (_T("speichern"));

    m_Delete.Create (_T("l�schen"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_DELETE);
	m_Delete.LoadBitmap (IDB_DELETE);
	m_Delete.LoadMask (IDB_DELETE_MASK);
	m_Delete.SetToolTip (_T("l�schen"));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	Form.Add (new CFormField (&m_Ez,EDIT,             (char *) ExtraData.EZNr, VCHAR));
	Form.Add (new CFormField (&m_Es,EDIT,             (char *) ExtraData.ESNr, VCHAR));
	Form.Add (new CFormField (&m_Countries,COMBOBOX,  (char *) ExtraData.Countries, VCHAR));
	Form.Add (new CFormField (&m_IdentNr,EDIT,          (char *) ExtraData.IdentNr, VCHAR));
    FillCombo (_T("tier_land"), &m_Countries);
	ExtraData.Chargen.Start ();
	while ((Charge = ExtraData.Chargen.GetNext ()) != NULL)
	{
		m_MultiCharge.AddString (Charge->Charge);
	}
	if (ExtraData.Chargen.anz > 0)
	{
		m_MultiCharge.SetCurSel (0);
	}

//Hauptgrid
    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 0, 12, 20);
    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 15);  //Spaltenabstand und Zeilenabstand

//Grid Toolbar
	ToolGrid.Create (this, 2, 2);
    ToolGrid.SetBorder (0, 0);
    ToolGrid.SetGridSpace (0, 0);

	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 0, 1, 1);
	ToolGrid.Add (c_Save);

	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 1, 0, 1, 1);
	ToolGrid.Add (c_Delete);

	CCtrlInfo *c_ToolGrid = new CCtrlInfo (&ToolGrid, 1, 0, 1, 1);
	c_ToolGrid->SetCellPos (0, 0, 0, 0);
	CtrlGrid.Add (c_ToolGrid);

//Gruppe
	CCtrlInfo *c_ExtraGroup     = new CCtrlInfo (&m_ExtraGroup, 0, 1, DOCKRIGHT, DOCKBOTTOM); 
	c_ExtraGroup->rightspace = 5;
	CtrlGrid.Add (c_ExtraGroup);

//MultiCharge
	CCtrlInfo *c_LMultiCharge     = new CCtrlInfo (&m_LMultiCharge, 1, 2, 1, 1); 
	CtrlGrid.Add (c_LMultiCharge);
	CCtrlInfo *c_MultiCharge     = new CCtrlInfo (&m_MultiCharge, 2, 2, 1, 1); 
	CtrlGrid.Add (c_MultiCharge);

//EZ-Nummer
	CCtrlInfo *c_LEz     = new CCtrlInfo (&m_LEz, 1, 3, 1, 1); 
	CtrlGrid.Add (c_LEz);
	CCtrlInfo *c_Ez     = new CCtrlInfo (&m_Ez, 2, 3, 1, 1); 
	CtrlGrid.Add (c_Ez);

//ES-Nummer
	CCtrlInfo *c_LEs     = new CCtrlInfo (&m_LEs, 1, 4, 1, 1); 
	CtrlGrid.Add (c_LEs);
	CCtrlInfo *c_Es     = new CCtrlInfo (&m_Es, 2, 4, 1, 1); 
	CtrlGrid.Add (c_Es);

//L�nder
	CCtrlInfo *c_LCountries     = new CCtrlInfo (&m_LCountries, 1, 5, 1, 1); 
	CtrlGrid.Add (c_LCountries);
	CCtrlInfo *c_Countries     = new CCtrlInfo (&m_Countries, 2, 5, 1, 1); 
	CtrlGrid.Add (c_Countries);

//Ident-Nummer
	CCtrlInfo *c_LIdentNr     = new CCtrlInfo (&m_LIdentNr, 1, 6, 1, 1); 
	CtrlGrid.Add (c_LIdentNr);
	CCtrlInfo *c_IdentNr     = new CCtrlInfo (&m_IdentNr, 2, 6, 1, 1); 
	CtrlGrid.Add (c_IdentNr);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Save.Tooltip.SetFont (&Font);
	m_Save.Tooltip.SetSize ();
	m_Delete.Tooltip.SetFont (&Font);
	m_Delete.Tooltip.SetSize ();

	CtrlGrid.Display ();
	Form.Show ();
	return TRUE;
}

BOOL CExtraDialog::PreTranslateMessage(MSG* pMsg)
{
//	CWnd *cWnd = NULL;
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
                if (OnReturn ())
				{
						return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (Control->GetParent () == &m_MultiCharge)
				{
					return FALSE;
				}
				if (Control == &m_Countries)
				{
					return FALSE;
				}
                if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (Control->GetParent () == &m_MultiCharge)
				{
					return FALSE;
				}
				if (Control == &m_Countries)
				{
					return FALSE;
				}
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					if (OnTab ())
					{
						return TRUE;
					}
				}
				else
				{
					if (OnKeyup ())
					{
						return TRUE;
					}
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				OnSave ();
				return TRUE;
			}
	}
	return FALSE;
}

CWnd *CExtraDialog::GetNextDlgItem (CWnd *Control, BOOL Dir)
{
	if (Control->GetParent () == &m_MultiCharge)
	{
		if (Dir)
		{
			return &m_IdentNr;
		}
		else
		{
			return &m_Ez;
		}
	}

	if (Control == &m_Ez)
	{
		if (Dir)
		{
			return &m_MultiCharge;
		}
		else
		{
			return &m_Es;
		}
	}

	if (Control == &m_Es)
	{
		if (Dir)
		{
			return &m_Ez;
		}
		else
		{
			return &m_Countries;
		}
	}

	if (Control == &m_Countries)
	{
		if (Dir)
		{
			return &m_Es;
		}
		else
		{
			return &m_IdentNr;
		}
	}

	if (Control == &m_IdentNr)
	{
		if (Dir)
		{
			return &m_Countries;
		}
		else
		{
			return &m_MultiCharge;
		}
	}
	return Control;
}

BOOL CExtraDialog::OnTab ()
{
	CWnd *Control = GetFocus ();
	if (Control->GetParent () == &m_MultiCharge)
	{
		CString cText;
		m_MultiCharge.GetWindowText (cText);
		cText.Trim ();
		if (cText != "" && m_MultiCharge.FindStringExact (-1, cText.GetBuffer ()) == CB_ERR)
		{
			m_MultiCharge.AddString (cText.GetBuffer ());
		}
	}
	Control = GetNextDlgItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
	return FALSE;
}

BOOL CExtraDialog::OnReturn ()
{
	CWnd *Control = GetFocus ();
	if (Control->GetParent () == &m_MultiCharge)
	{
		CString cText;
		m_MultiCharge.GetWindowText (cText);
		cText.Trim ();

		if (cText != "" && m_MultiCharge.FindStringExact (-1, cText.GetBuffer ()) == CB_ERR)
		{
			m_MultiCharge.AddString (cText.GetBuffer ());
			return TRUE;
		}
	}

	Control = GetNextDlgItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
	return TRUE;
}

BOOL CExtraDialog::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}



void CExtraDialog::SetExtraData (CExtraData *ExtraData)
{
	SCharge *Charge;
	if (ExtraData == NULL)
	{
		strcpy (this->ExtraData.ESNr,"");
		strcpy (this->ExtraData.EZNr,"");
		strcpy (this->ExtraData.IdentNr,"");
		strcpy (this->ExtraData.Countries,"");
		this->ExtraData.Chargen.Clear ();
	}
	else
	{
		strcpy (this->ExtraData.ESNr,ExtraData->ESNr);
		strcpy (this->ExtraData.EZNr,ExtraData->EZNr);
		strcpy (this->ExtraData.IdentNr,ExtraData->IdentNr);
		strcpy (this->ExtraData.Countries,ExtraData->Countries);
		this->ExtraData.Chargen.Clear ();
		ExtraData->Chargen.Start ();
		while ((Charge = ExtraData->Chargen.GetNext ()) != NULL)
		{
			this->ExtraData.Chargen.Add (*Charge);
		}
	}
}

void CExtraDialog::GetExtraData (CExtraData *ExtraData)
{
	SCharge *Charge;
	strcpy (ExtraData->ESNr,this->ExtraData.ESNr);
	strcpy (ExtraData->EZNr,this->ExtraData.EZNr);
	strcpy (ExtraData->IdentNr,this->ExtraData.IdentNr);
	strcpy (ExtraData->Countries,this->ExtraData.Countries);
	ExtraData->Chargen.Clear ();
	this->ExtraData.Chargen.Start ();
	while ((Charge = this->ExtraData.Chargen.GetNext ()) != NULL)
	{
		ExtraData->Chargen.Add (*Charge);
	}
}

void CExtraDialog::OnDestroy ()
{
	SCharge Charge;
	Form.Get ();
	ExtraData.Chargen.Clear ();
	for (int i = 0; i < m_MultiCharge.GetCount (); i ++)
	{
		m_MultiCharge.GetLBText (i, Charge.Charge); 
		ExtraData.Chargen.Add (Charge);
	}
}

void CExtraDialog::OnSave ()
{
	CString Countries = "";
	SCharge Charge;
	Form.Get ();
	m_Countries.GetWindowText (Countries);
	ExtraData.Chargen.Clear ();
	strcpy (ExtraData.Countries, Countries.GetBuffer ());
	for (int i = 0; i < m_MultiCharge.GetCount (); i ++)
	{
		m_MultiCharge.GetLBText (i, Charge.Charge); 
		ExtraData.Chargen.Add (Charge);
	}
	OnOK ();
}

void CExtraDialog::OnDelete ()
{
	strcpy (this->ExtraData.ESNr,"");
	strcpy (this->ExtraData.EZNr,"");
	strcpy (this->ExtraData.IdentNr,"");
	strcpy (this->ExtraData.Countries,"");
	this->ExtraData.Chargen.Clear ();
	Form.Show ();
	m_MultiCharge.ResetContent ();
}

void CExtraDialog::FillCombo (LPTSTR Item, CWnd *control)
{
	CFormField *f = Form.GetFormField (control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s"), Ptabn.ptabn.ptwer1);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
//		f->SetSel (0);
//		f->Get ();
	}
}

