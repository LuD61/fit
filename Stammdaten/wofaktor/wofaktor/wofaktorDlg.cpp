// wofaktorDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "wofaktor.h"
#include "wofaktorDlg.h"
#include "DbClass.h"
#include "dbwofaktor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern DB_CLASS dbClass ;
WOFAKTOR_CLASS Wofaktor ;

char bufh[599];


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CwofaktorDlg-Dialogfeld


CwofaktorDlg::CwofaktorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CwofaktorDlg::IDD, pParent)
	, v_jahr(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CwofaktorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_Jahr, m_jahr);
	DDX_Text(pDX, IDC_Jahr, v_jahr);
	DDV_MinMaxLong(pDX, v_jahr, 2000, 2099);
	DDX_Control(pDX, IDC_EDIT1, m_wo1);
	DDX_Text(pDX, IDC_EDIT1, v_wo1);
	DDX_Control(pDX, IDC_EDIT2, m_wo2);
	DDX_Text(pDX, IDC_EDIT2, v_wo2);
	DDX_Control(pDX, IDC_EDIT3, m_wo3);
	DDX_Text(pDX, IDC_EDIT3, v_wo3);
	DDX_Control(pDX, IDC_EDIT4, m_wo4);
	DDX_Text(pDX, IDC_EDIT4, v_wo4);
	DDX_Control(pDX, IDC_EDIT5, m_wo5);
	DDX_Text(pDX, IDC_EDIT5, v_wo5);
	DDX_Control(pDX, IDC_EDIT6, m_wo6);
	DDX_Text(pDX, IDC_EDIT6, v_wo6);
	DDX_Control(pDX, IDC_EDIT7, m_wo7);
	DDX_Text(pDX, IDC_EDIT7, v_wo7);
	DDX_Control(pDX, IDC_EDIT8, m_wo8);
	DDX_Text(pDX, IDC_EDIT8, v_wo8);
	DDX_Control(pDX, IDC_EDIT9, m_wo9);
	DDX_Text(pDX, IDC_EDIT9, v_wo9);
	DDX_Control(pDX, IDC_EDIT10, m_wo10);
	DDX_Text(pDX, IDC_EDIT10, v_wo10);
	DDX_Control(pDX, IDC_EDIT11, m_wo11);
	DDX_Text(pDX, IDC_EDIT11, v_wo11);
	DDX_Control(pDX, IDC_EDIT12, m_wo12);
	DDX_Text(pDX, IDC_EDIT12, v_wo12);
	DDX_Control(pDX, IDC_EDIT13, m_wo13);
	DDX_Text(pDX, IDC_EDIT13, v_wo13);
	DDX_Control(pDX, IDC_EDIT14, m_wo14);
	DDX_Text(pDX, IDC_EDIT14, v_wo14);
	DDX_Control(pDX, IDC_EDIT15, m_wo15);
	DDX_Text(pDX, IDC_EDIT15, v_wo15);
	DDX_Control(pDX, IDC_EDIT16, m_wo16);
	DDX_Text(pDX, IDC_EDIT16, v_wo16);
	DDX_Control(pDX, IDC_EDIT17, m_wo17);
	DDX_Text(pDX, IDC_EDIT17, v_wo17);
	DDX_Control(pDX, IDC_EDIT18, m_wo18);
	DDX_Text(pDX, IDC_EDIT18, v_wo18);
	DDX_Control(pDX, IDC_EDIT19, m_wo19);
	DDX_Text(pDX, IDC_EDIT19, v_wo19);
	DDX_Control(pDX, IDC_EDIT20, m_wo20);
	DDX_Text(pDX, IDC_EDIT20, v_wo20);
	DDX_Control(pDX, IDC_EDIT21, m_wo21);
	DDX_Text(pDX, IDC_EDIT21, v_wo21);
	DDX_Control(pDX, IDC_EDIT22, m_wo22);
	DDX_Text(pDX, IDC_EDIT22, v_wo22);
	DDX_Control(pDX, IDC_EDIT23, m_wo23);
	DDX_Text(pDX, IDC_EDIT23, v_wo23);
	DDX_Control(pDX, IDC_EDIT24, m_wo24);
	DDX_Text(pDX, IDC_EDIT24, v_wo24);
	DDX_Control(pDX, IDC_EDIT25, m_wo25);
	DDX_Text(pDX, IDC_EDIT25, v_wo25);
	DDX_Control(pDX, IDC_EDIT26, m_wo26);
	DDX_Text(pDX, IDC_EDIT26, v_wo26);
	DDX_Control(pDX, IDC_EDIT27, m_wo27);
	DDX_Text(pDX, IDC_EDIT27, v_wo27);
	DDX_Control(pDX, IDC_EDIT28, m_wo28);
	DDX_Text(pDX, IDC_EDIT28, v_wo28);
	DDX_Control(pDX, IDC_EDIT29, m_wo29);
	DDX_Text(pDX, IDC_EDIT29, v_wo29);
	DDX_Control(pDX, IDC_EDIT30, m_wo30);
	DDX_Text(pDX, IDC_EDIT30, v_wo30);
	DDX_Control(pDX, IDC_EDIT31, m_wo31);
	DDX_Text(pDX, IDC_EDIT31, v_wo31);
	DDX_Control(pDX, IDC_EDIT32, m_wo32);
	DDX_Text(pDX, IDC_EDIT32, v_wo32);
	DDX_Control(pDX, IDC_EDIT33, m_wo33);
	DDX_Text(pDX, IDC_EDIT33, v_wo33);
	DDX_Control(pDX, IDC_EDIT34, m_wo34);
	DDX_Text(pDX, IDC_EDIT34, v_wo34);
	DDX_Control(pDX, IDC_EDIT35, m_wo35);
	DDX_Text(pDX, IDC_EDIT35, v_wo35);
	DDX_Control(pDX, IDC_EDIT36, m_wo36);
	DDX_Text(pDX, IDC_EDIT36, v_wo36);
	DDX_Control(pDX, IDC_EDIT37, m_wo37);
	DDX_Text(pDX, IDC_EDIT37, v_wo37);
	DDX_Control(pDX, IDC_EDIT38, m_wo38);
	DDX_Text(pDX, IDC_EDIT38, v_wo38);
	DDX_Control(pDX, IDC_EDIT39, m_wo39);
	DDX_Text(pDX, IDC_EDIT39, v_wo39);
	DDX_Control(pDX, IDC_EDIT40, m_wo40);
	DDX_Text(pDX, IDC_EDIT40, v_wo40);
	DDX_Control(pDX, IDC_EDIT41, m_wo41);
	DDX_Text(pDX, IDC_EDIT41, v_wo41);
	DDX_Control(pDX, IDC_EDIT42, m_wo42);
	DDX_Text(pDX, IDC_EDIT42, v_wo42);
	DDX_Control(pDX, IDC_EDIT43, m_wo43);
	DDX_Text(pDX, IDC_EDIT43, v_wo43);
	DDX_Control(pDX, IDC_EDIT44, m_wo44);
	DDX_Text(pDX, IDC_EDIT44, v_wo44);
	DDX_Control(pDX, IDC_EDIT45, m_wo45);
	DDX_Text(pDX, IDC_EDIT45, v_wo45);
	DDX_Control(pDX, IDC_EDIT46, m_wo46);
	DDX_Text(pDX, IDC_EDIT46, v_wo46);
	DDX_Control(pDX, IDC_EDIT47, m_wo47);
	DDX_Text(pDX, IDC_EDIT47, v_wo47);
	DDX_Control(pDX, IDC_EDIT48, m_wo48);
	DDX_Text(pDX, IDC_EDIT48, v_wo48);
	DDX_Control(pDX, IDC_EDIT49, m_wo49);
	DDX_Text(pDX, IDC_EDIT49, v_wo49);
	DDX_Control(pDX, IDC_EDIT50, m_wo50);
	DDX_Text(pDX, IDC_EDIT50, v_wo50);
	DDX_Control(pDX, IDC_EDIT51, m_wo51);
	DDX_Text(pDX, IDC_EDIT51, v_wo51);
	DDX_Control(pDX, IDC_EDIT52, m_wo52);
	DDX_Text(pDX, IDC_EDIT52, v_wo52);
	DDX_Control(pDX, IDC_EDIT53, m_wo53);
	DDX_Text(pDX, IDC_EDIT53, v_wo53);
}

BEGIN_MESSAGE_MAP(CwofaktorDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDC_EDIT1, &CwofaktorDlg::OnEnKillfocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT2, &CwofaktorDlg::OnEnKillfocusEdit2)
	ON_EN_KILLFOCUS(IDC_EDIT3, &CwofaktorDlg::OnEnKillfocusEdit3)
	ON_EN_KILLFOCUS(IDC_EDIT4, &CwofaktorDlg::OnEnKillfocusEdit4)
	ON_EN_KILLFOCUS(IDC_EDIT5, &CwofaktorDlg::OnEnKillfocusEdit5)
	ON_EN_KILLFOCUS(IDC_EDIT6, &CwofaktorDlg::OnEnKillfocusEdit6)
	ON_EN_KILLFOCUS(IDC_EDIT7, &CwofaktorDlg::OnEnKillfocusEdit7)
	ON_EN_KILLFOCUS(IDC_EDIT8, &CwofaktorDlg::OnEnKillfocusEdit8)
	ON_EN_KILLFOCUS(IDC_EDIT9, &CwofaktorDlg::OnEnKillfocusEdit9)
	ON_EN_KILLFOCUS(IDC_EDIT10, &CwofaktorDlg::OnEnKillfocusEdit10)
	ON_EN_KILLFOCUS(IDC_EDIT11, &CwofaktorDlg::OnEnKillfocusEdit11)
	ON_EN_KILLFOCUS(IDC_EDIT12, &CwofaktorDlg::OnEnKillfocusEdit12)
	ON_EN_KILLFOCUS(IDC_EDIT13, &CwofaktorDlg::OnEnKillfocusEdit13)
	ON_EN_KILLFOCUS(IDC_EDIT14, &CwofaktorDlg::OnEnKillfocusEdit14)
	ON_EN_KILLFOCUS(IDC_EDIT15, &CwofaktorDlg::OnEnKillfocusEdit15)
	ON_EN_KILLFOCUS(IDC_EDIT16, &CwofaktorDlg::OnEnKillfocusEdit16)
	ON_EN_KILLFOCUS(IDC_EDIT17, &CwofaktorDlg::OnEnKillfocusEdit17)
	ON_EN_KILLFOCUS(IDC_EDIT18, &CwofaktorDlg::OnEnKillfocusEdit18)
	ON_EN_KILLFOCUS(IDC_EDIT19, &CwofaktorDlg::OnEnKillfocusEdit19)
	ON_EN_KILLFOCUS(IDC_EDIT20, &CwofaktorDlg::OnEnKillfocusEdit20)
	ON_EN_KILLFOCUS(IDC_EDIT21, &CwofaktorDlg::OnEnKillfocusEdit21)
	ON_EN_KILLFOCUS(IDC_EDIT22, &CwofaktorDlg::OnEnKillfocusEdit22)
	ON_EN_KILLFOCUS(IDC_EDIT23, &CwofaktorDlg::OnEnKillfocusEdit23)
	ON_EN_KILLFOCUS(IDC_EDIT24, &CwofaktorDlg::OnEnKillfocusEdit24)
	ON_EN_KILLFOCUS(IDC_EDIT25, &CwofaktorDlg::OnEnKillfocusEdit25)
	ON_EN_KILLFOCUS(IDC_EDIT26, &CwofaktorDlg::OnEnKillfocusEdit26)
	ON_EN_KILLFOCUS(IDC_EDIT27, &CwofaktorDlg::OnEnKillfocusEdit27)
	ON_EN_KILLFOCUS(IDC_EDIT28, &CwofaktorDlg::OnEnKillfocusEdit28)
	ON_EN_KILLFOCUS(IDC_EDIT29, &CwofaktorDlg::OnEnKillfocusEdit29)
	ON_EN_KILLFOCUS(IDC_EDIT30, &CwofaktorDlg::OnEnKillfocusEdit30)
	ON_EN_KILLFOCUS(IDC_EDIT31, &CwofaktorDlg::OnEnKillfocusEdit31)
	ON_EN_KILLFOCUS(IDC_EDIT32, &CwofaktorDlg::OnEnKillfocusEdit32)
	ON_EN_KILLFOCUS(IDC_EDIT33, &CwofaktorDlg::OnEnKillfocusEdit33)
	ON_EN_KILLFOCUS(IDC_EDIT34, &CwofaktorDlg::OnEnKillfocusEdit34)
	ON_EN_KILLFOCUS(IDC_EDIT35, &CwofaktorDlg::OnEnKillfocusEdit35)
	ON_EN_KILLFOCUS(IDC_EDIT36, &CwofaktorDlg::OnEnKillfocusEdit36)
	ON_EN_KILLFOCUS(IDC_EDIT37, &CwofaktorDlg::OnEnKillfocusEdit37)
	ON_EN_KILLFOCUS(IDC_EDIT38, &CwofaktorDlg::OnEnKillfocusEdit38)
	ON_EN_KILLFOCUS(IDC_EDIT39, &CwofaktorDlg::OnEnKillfocusEdit39)
	ON_EN_KILLFOCUS(IDC_EDIT40, &CwofaktorDlg::OnEnKillfocusEdit40)
	ON_EN_KILLFOCUS(IDC_EDIT41, &CwofaktorDlg::OnEnKillfocusEdit41)
	ON_EN_KILLFOCUS(IDC_EDIT42, &CwofaktorDlg::OnEnKillfocusEdit42)
	ON_EN_KILLFOCUS(IDC_EDIT43, &CwofaktorDlg::OnEnKillfocusEdit43)
	ON_EN_KILLFOCUS(IDC_EDIT44, &CwofaktorDlg::OnEnKillfocusEdit44)
	ON_EN_KILLFOCUS(IDC_EDIT45, &CwofaktorDlg::OnEnKillfocusEdit45)
	ON_EN_KILLFOCUS(IDC_EDIT46, &CwofaktorDlg::OnEnKillfocusEdit46)
	ON_EN_KILLFOCUS(IDC_EDIT47, &CwofaktorDlg::OnEnKillfocusEdit47)
	ON_EN_KILLFOCUS(IDC_EDIT48, &CwofaktorDlg::OnEnKillfocusEdit48)
	ON_EN_KILLFOCUS(IDC_EDIT49, &CwofaktorDlg::OnEnKillfocusEdit49)
	ON_EN_KILLFOCUS(IDC_EDIT50, &CwofaktorDlg::OnEnKillfocusEdit50)
	ON_EN_KILLFOCUS(IDC_EDIT51, &CwofaktorDlg::OnEnKillfocusEdit51)
	ON_EN_KILLFOCUS(IDC_EDIT52, &CwofaktorDlg::OnEnKillfocusEdit52)
	ON_EN_KILLFOCUS(IDC_EDIT53, &CwofaktorDlg::OnEnKillfocusEdit53)


	ON_BN_CLICKED(IDC_BUTTup, &CwofaktorDlg::OnBnClickedButtup)
	ON_BN_CLICKED(IDC_BUTTdown, &CwofaktorDlg::OnBnClickedButtdown)
	ON_BN_CLICKED(IDCANCEL, &CwofaktorDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CwofaktorDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CwofaktorDlg-Meldungshandler

BOOL CwofaktorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen
	time_t timer;
	struct tm *ltime;
	time (&timer);
 	ltime = localtime (&timer);
//	sprintf ( bufh , "%02d.%02d.%04d", ltime->tm_mday ,ltime->tm_mon + 1 ,*/ ltime->tm_year + 1900 );

	v_jahr = ltime->tm_year + 1900 ;


	satzlesen();



	UpdateData(FALSE);

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CwofaktorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CwofaktorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CwofaktorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CwofaktorDlg::OnEnKillfocusEdit1()
{
	UpdateData(TRUE);
	v_wo1.Replace(",",".");
	double hi = atof ( v_wo1.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo1.Format("%1.2f",hi);
	UpdateData(FALSE);

}
void CwofaktorDlg::OnEnKillfocusEdit2()
{
	UpdateData(TRUE);
	v_wo2.Replace(",",".");
	double hi = atof ( v_wo2.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo2.Format("%1.2f",hi);
	UpdateData(FALSE);
}
void CwofaktorDlg::OnEnKillfocusEdit3()
{
	UpdateData(TRUE);
	v_wo3.Replace(",",".");
	double hi = atof ( v_wo3.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo3.Format("%1.2f",hi);
	UpdateData(FALSE);

}
void CwofaktorDlg::OnEnKillfocusEdit4()
{
	UpdateData(TRUE);
	v_wo4.Replace(",",".");
	double hi = atof ( v_wo4.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo4.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit5()
{
	UpdateData(TRUE);
	v_wo5.Replace(",",".");
	double hi = atof ( v_wo5.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo5.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit6()
{
	UpdateData(TRUE);
	v_wo6.Replace(",",".");
	double hi = atof ( v_wo6.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo6.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit7()
{
	UpdateData(TRUE);
	v_wo7.Replace(",",".");
	double hi = atof ( v_wo7.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo7.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit8()
{
	UpdateData(TRUE);
	v_wo8.Replace(",",".");
	double hi = atof ( v_wo8.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo8.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit9()
{
	UpdateData(TRUE);
	v_wo9.Replace(",",".");
	double hi = atof ( v_wo9.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo9.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit10()
{
	UpdateData(TRUE);
	v_wo10.Replace(",",".");
	double hi = atof ( v_wo10.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo10.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit11()
{
	UpdateData(TRUE);
	v_wo11.Replace(",",".");
	double hi = atof ( v_wo11.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo11.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit12()
{
	UpdateData(TRUE);
	v_wo12.Replace(",",".");
	double hi = atof ( v_wo12.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo12.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit13()
{
	UpdateData(TRUE);
	v_wo13.Replace(",",".");
	double hi = atof ( v_wo13.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo13.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit14()
{
	UpdateData(TRUE);
	v_wo14.Replace(",",".");
	double hi = atof ( v_wo14.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo14.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit15()
{
	UpdateData(TRUE);
	v_wo15.Replace(",",".");
	double hi = atof ( v_wo15.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo15.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit16()
{
	UpdateData(TRUE);
	v_wo16.Replace(",",".");
	double hi = atof ( v_wo16.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo16.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit17()
{
	UpdateData(TRUE);
	v_wo17.Replace(",",".");
	double hi = atof ( v_wo17.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo17.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit18()
{
	UpdateData(TRUE);
	v_wo18.Replace(",",".");
	double hi = atof ( v_wo18.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo18.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit19()
{
	UpdateData(TRUE);
	v_wo19.Replace(",",".");
	double hi = atof ( v_wo19.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo19.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit20()
{
	UpdateData(TRUE);
	v_wo20.Replace(",",".");
	double hi = atof ( v_wo20.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo20.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit21()
{
	UpdateData(TRUE);
	v_wo21.Replace(",",".");
	double hi = atof ( v_wo21.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo21.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit22()
{
	UpdateData(TRUE);
	v_wo22.Replace(",",".");
	double hi = atof ( v_wo22.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo22.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit23()
{
	UpdateData(TRUE);
	v_wo23.Replace(",",".");
	double hi = atof ( v_wo23.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo23.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit24()
{
	UpdateData(TRUE);
	v_wo24.Replace(",",".");
	double hi = atof ( v_wo24.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo24.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit25()
{
	UpdateData(TRUE);
	v_wo25.Replace(",",".");
	double hi = atof ( v_wo25.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo25.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit26()
{
	UpdateData(TRUE);
	v_wo26.Replace(",",".");
	double hi = atof ( v_wo26.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo26.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit27()
{
	UpdateData(TRUE);
	v_wo27.Replace(",",".");
	double hi = atof ( v_wo27.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo27.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit28()
{
	UpdateData(TRUE);
	v_wo28.Replace(",",".");
	double hi = atof ( v_wo28.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo28.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit29()
{
	UpdateData(TRUE);
	v_wo29.Replace(",",".");
	double hi = atof ( v_wo29.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo29.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit30()
{
	UpdateData(TRUE);
	v_wo30.Replace(",",".");
	double hi = atof ( v_wo30.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo30.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit31()
{
	UpdateData(TRUE);
	v_wo31.Replace(",",".");
	double hi = atof ( v_wo31.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo31.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit32()
{
	UpdateData(TRUE);
	v_wo32.Replace(",",".");
	double hi = atof ( v_wo32.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo32.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit33()
{
	UpdateData(TRUE);
	v_wo33.Replace(",",".");
	double hi = atof ( v_wo33.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo33.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit34()
{
	UpdateData(TRUE);
	v_wo34.Replace(",",".");
	double hi = atof ( v_wo34.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo34.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit35()
{
	UpdateData(TRUE);
	v_wo35.Replace(",",".");
	double hi = atof ( v_wo35.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo35.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit36()
{
	UpdateData(TRUE);
	v_wo36.Replace(",",".");
	double hi = atof ( v_wo36.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo36.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit37()
{
	UpdateData(TRUE);
	v_wo37.Replace(",",".");
	double hi = atof ( v_wo37.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo37.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit38()
{
	UpdateData(TRUE);
	v_wo38.Replace(",",".");
	double hi = atof ( v_wo38.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo38.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit39()
{
	UpdateData(TRUE);
	v_wo39.Replace(",",".");
	double hi = atof ( v_wo39.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo39.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit40()
{
	UpdateData(TRUE);
	v_wo40.Replace(",",".");
	double hi = atof ( v_wo40.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo40.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit41()
{
	UpdateData(TRUE);
	v_wo41.Replace(",",".");
	double hi = atof ( v_wo41.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo41.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit42()
{
	UpdateData(TRUE);
	v_wo42.Replace(",",".");
	double hi = atof ( v_wo42.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo42.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit43()
{
	UpdateData(TRUE);
	v_wo43.Replace(",",".");
	double hi = atof ( v_wo43.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo43.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit44()
{
	UpdateData(TRUE);
	v_wo44.Replace(",",".");
	double hi = atof ( v_wo44.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo44.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit45()
{
	UpdateData(TRUE);
	v_wo45.Replace(",",".");
	double hi = atof ( v_wo45.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo45.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit46()
{
	UpdateData(TRUE);
	v_wo46.Replace(",",".");
	double hi = atof ( v_wo46.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo46.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit47()
{
	UpdateData(TRUE);
	v_wo47.Replace(",",".");
	double hi = atof ( v_wo47.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo47.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit48()
{
	UpdateData(TRUE);
	v_wo48.Replace(",",".");
	double hi = atof ( v_wo48.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo48.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit49()
{
	UpdateData(TRUE);
	v_wo49.Replace(",",".");
	double hi = atof ( v_wo49.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo49.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit50()
{
	UpdateData(TRUE);
	v_wo50.Replace(",",".");
	double hi = atof ( v_wo50.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo50.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit51()
{
	UpdateData(TRUE);
	v_wo51.Replace(",",".");
	double hi = atof ( v_wo51.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo51.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit52()
{
	UpdateData(TRUE);
	v_wo52.Replace(",",".");
	double hi = atof ( v_wo52.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo52.Format("%1.2f",hi);
	UpdateData(FALSE);


}
void CwofaktorDlg::OnEnKillfocusEdit53()
{
	UpdateData(TRUE);
	v_wo53.Replace(",",".");
	double hi = atof ( v_wo53.GetBuffer());
	if ( hi > 999.99 ) hi = 999.99 ;
	if ( hi < -99.99 ) hi = -99.99 ;
	v_wo53.Format("%1.2f",hi);
	UpdateData(FALSE);


}

void CwofaktorDlg::OnBnClickedButtup()
{
	UpdateData(TRUE);
	satzschreiben();
	if ( v_jahr < 2099 )
		v_jahr ++ ;
	satzlesen();
	UpdateData(FALSE);

}

void CwofaktorDlg::OnBnClickedButtdown()
{
	UpdateData(TRUE);
	satzschreiben();
	if ( v_jahr > 2000 )
		v_jahr -- ;
	satzlesen();
	UpdateData(FALSE);
}

void CwofaktorDlg::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnCancel();
}

void CwofaktorDlg::OnBnClickedOk()
{
	UpdateData(TRUE);
	satzschreiben();
	OnOK();
}

void CwofaktorDlg::satzschreiben(void)
{


	double hi;

	wofaktor.jr = (short) v_jahr ;
	for ( int i = 1 ; i < 54 ; i ++ )
	{
		switch (i)
		{
		case 1 :

			hi = atof ( v_wo1.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo1 )
			{
				wofaktor.kw =1;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;

		case 2 :

			hi = atof ( v_wo2.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo2 )
			{
				wofaktor.kw =2;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;

		case 3 :

			hi = atof ( v_wo3.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo3 )
			{
				wofaktor.kw =3;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 4 :

			hi = atof ( v_wo4.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo4 )
			{
				wofaktor.kw =4;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 5 :

			hi = atof ( v_wo5.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo5 )
			{
				wofaktor.kw =5;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 6 :

			hi = atof ( v_wo6.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo6 )
			{
				wofaktor.kw =6;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 7 :

			hi = atof ( v_wo7.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo7 )
			{
				wofaktor.kw =7;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 8 :

			hi = atof ( v_wo8.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo8 )
			{
				wofaktor.kw =8;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 9 :

			hi = atof ( v_wo9.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo9 )
			{
				wofaktor.kw =9;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 10 :

			hi = atof ( v_wo10.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo10 )
			{
				wofaktor.kw =10;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 11 :

			hi = atof ( v_wo11.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo11 )
			{
				wofaktor.kw =11;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 12 :

			hi = atof ( v_wo12.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo12 )
			{
				wofaktor.kw =12;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 13 :

			hi = atof ( v_wo13.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo13 )
			{
				wofaktor.kw =13;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 14 :

			hi = atof ( v_wo14.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo14 )
			{
				wofaktor.kw =14;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 16 :

			hi = atof ( v_wo16.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo16 )
			{
				wofaktor.kw =16;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 17 :

			hi = atof ( v_wo17.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo17 )
			{
				wofaktor.kw =17;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 18 :

			hi = atof ( v_wo18.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo18 )
			{
				wofaktor.kw =18;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 19 :

			hi = atof ( v_wo19.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo19 )
			{
				wofaktor.kw =19;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 20 :

			hi = atof ( v_wo20.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo20 )
			{
				wofaktor.kw =20;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 21 :

			hi = atof ( v_wo21.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo21 )
			{
				wofaktor.kw =21;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 22 :

			hi = atof ( v_wo22.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo22 )
			{
				wofaktor.kw =22;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 23 :

			hi = atof ( v_wo23.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo23 )
			{
				wofaktor.kw =23;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 24 :

			hi = atof ( v_wo24.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo24 )
			{
				wofaktor.kw =24;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 25 :

			hi = atof ( v_wo25.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo25 )
			{
				wofaktor.kw =25;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 26 :

			hi = atof ( v_wo26.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo26 )
			{
				wofaktor.kw =26;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 27 :

			hi = atof ( v_wo27.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo27 )
			{
				wofaktor.kw =27;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 28 :

			hi = atof ( v_wo28.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo28 )
			{
				wofaktor.kw =28;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 29 :

			hi = atof ( v_wo29.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo29 )
			{
				wofaktor.kw =29;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 30 :

			hi = atof ( v_wo30.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo30 )
			{
				wofaktor.kw =30;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 31 :

			hi = atof ( v_wo31.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo31 )
			{
				wofaktor.kw =31;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 32 :

			hi = atof ( v_wo32.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo32 )
			{
				wofaktor.kw =32;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 33 :

			hi = atof ( v_wo33.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo33 )
			{
				wofaktor.kw =33;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 34 :

			hi = atof ( v_wo34.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo34 )
			{
				wofaktor.kw =34;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 35 :

			hi = atof ( v_wo35.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo35 )
			{
				wofaktor.kw =35;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 36 :

			hi = atof ( v_wo36.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo36 )
			{
				wofaktor.kw =36;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 37 :

			hi = atof ( v_wo37.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo37 )
			{
				wofaktor.kw =37;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 38 :

			hi = atof ( v_wo38.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo38 )
			{
				wofaktor.kw =38;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 39 :

			hi = atof ( v_wo39.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo39 )
			{
				wofaktor.kw =39;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 40 :

			hi = atof ( v_wo40.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo40 )
			{
				wofaktor.kw =40;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 41 :

			hi = atof ( v_wo41.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo41 )
			{
				wofaktor.kw =41;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 42 :

			hi = atof ( v_wo42.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo42 )
			{
				wofaktor.kw =42;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 43 :

			hi = atof ( v_wo43.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo43 )
			{
				wofaktor.kw =43;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 44 :

			hi = atof ( v_wo44.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo44 )
			{
				wofaktor.kw =44;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 45 :

			hi = atof ( v_wo45.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo45 )
			{
				wofaktor.kw =45;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 46 :

			hi = atof ( v_wo46.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo46 )
			{
				wofaktor.kw =46;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 47 :

			hi = atof ( v_wo47.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo47 )
			{
				wofaktor.kw =47;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 48 :

			hi = atof ( v_wo48.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo48 )
			{
				wofaktor.kw =48;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 49 :

			hi = atof ( v_wo49.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo49 )
			{
				wofaktor.kw =49;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 50 :

			hi = atof ( v_wo50.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo50 )
			{
				wofaktor.kw =50;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 51 :

			hi = atof ( v_wo51.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo51 )
			{
				wofaktor.kw =51;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 52 :

			hi = atof ( v_wo52.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo52 )
			{
				wofaktor.kw =52;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;
		case 53 :

			hi = atof ( v_wo53.GetBuffer());
			if ( hi > 999.99 ) hi = 999.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			if ( hi != s_wo53 )
			{
				wofaktor.kw =53;
				wofaktor.faktor = hi;
				Wofaktor.wofaktordelete();
				Wofaktor.wofaktorinsert();
			}
			break;

		}
	}
}

void CwofaktorDlg::satzlesen(void)
{

	wofaktor.jr = (short) v_jahr;
	int di = Wofaktor.openwofaktor();

	di = Wofaktor.lesewofaktor();

	if ( di ) wofaktor.kw = 0;

	for ( int i = 1 ; i < 54 ; i ++ )
	{

		switch ( i )
		{
		case 1 : 
			if ( di  || wofaktor.kw != 1 )
			{
				v_wo1.Format( _T("1,00"));
				s_wo1 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 1 )
			{
				v_wo1.Format( "%1.2f", wofaktor.faktor);
				s_wo1 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo1.Format( _T("1,00"));
				s_wo1 = 0;
			}
			break;

		case 2 : 
			if ( di  || wofaktor.kw != 2 )
			{
				v_wo2.Format( _T("1,00"));
				s_wo2 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 2 )
			{
				v_wo2.Format( "%1.2f",wofaktor.faktor);
				s_wo2 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo2.Format( _T("1,00"));
				s_wo2 = 0;

			}
			break;

		case 3 : 
			if ( di  || wofaktor.kw != 3 )
			{
				v_wo3.Format( _T("1,00"));
				s_wo3 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 3 )
			{
				v_wo3.Format( "%1.2f", wofaktor.faktor);
				s_wo3 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo3.Format( _T("1,00"));
				s_wo3 = 0;
			}
			break;

		case 4 : 
			if ( di  || wofaktor.kw != 4 )
			{
				v_wo4.Format( _T("1,00"));
				s_wo4 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 4 )
			{
				v_wo4.Format( "%1.2f", wofaktor.faktor);
				s_wo4 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo4.Format( _T("1,00"));
				s_wo4 = 0;
			}
			break;

		case 5 : 
			if ( di  || wofaktor.kw != 5 )
			{
				v_wo5.Format( _T("1,00"));
				s_wo5 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 5 )
			{
				v_wo5.Format( "%1.2f", wofaktor.faktor);
				s_wo5 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo5.Format( _T("1,00"));
				s_wo5 = 0;
			}
			break;

		case 6 : 
			if ( di  || wofaktor.kw != 6 )
			{
				v_wo6.Format( _T("1,00"));
				s_wo6 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 6 )
			{
				v_wo6.Format( "%1.2f", wofaktor.faktor);
				s_wo6 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo6.Format( _T("1,00"));
				s_wo6 = 0;
			}
			break;

		case 7 : 
			if ( di  || wofaktor.kw != 7 )
			{
				v_wo7.Format( _T("1,00"));
				s_wo7 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 7 )
			{
				v_wo7.Format( "%1.2f", wofaktor.faktor);
				s_wo7 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo7.Format( _T("1,00"));
				s_wo7 = 0;
			}
			break;

		case 8 : 
			if ( di  || wofaktor.kw != 8 )
			{
				v_wo8.Format( _T("1,00"));
				s_wo8 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 8 )
			{
				v_wo8.Format( "%1.2f", wofaktor.faktor);
				s_wo8 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo8.Format( _T("1,00"));
				s_wo8 = 0;
			}
			break;

		case 9 : 
			if ( di  || wofaktor.kw != 9 )
			{
				v_wo9.Format( _T("1,00"));
				s_wo9 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 9 )
			{
				v_wo9.Format( "%1.2f", wofaktor.faktor);
				s_wo9 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo9.Format( _T("1,00"));
				s_wo9 = 0;
			}
			break;

		case 10 : 
			if ( di  || wofaktor.kw != 10 )
			{
				v_wo10.Format( _T("1,00"));
				s_wo10 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 10 )
			{
				v_wo10.Format( "%1.2f", wofaktor.faktor);
				s_wo10 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo10.Format( _T("1,00"));
				s_wo10 = 0;
			}
			break;

		case 11 : 
			if ( di  || wofaktor.kw != 11 )
			{
				v_wo11.Format( _T("1,00"));
				s_wo11 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 11 )
			{
				v_wo11.Format( "%1.2f", wofaktor.faktor);
				s_wo11 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo11.Format( _T("1,00"));
				s_wo11 = 0;
			}
			break;

		case 12 : 
			if ( di  || wofaktor.kw != 12 )
			{
				v_wo12.Format( _T("1,00"));
				s_wo12 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 12 )
			{
				v_wo12.Format( "%1.2f", wofaktor.faktor);
				s_wo12 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo12.Format( _T("1,00"));
				s_wo12 = 0;
			}
			break;

		case 13 : 
			if ( di  || wofaktor.kw != 13 )
			{
				v_wo13.Format( _T("1,00"));
				s_wo13 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 13 )
			{
				v_wo13.Format( "%1.2f", wofaktor.faktor);
				s_wo13 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo13.Format( _T("1,00"));
				s_wo13 = 0;
			}
			break;

		case 14 : 
			if ( di  || wofaktor.kw != 14 )
			{
				v_wo14.Format( _T("1,00"));
				s_wo14 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 14 )
			{
				v_wo14.Format( "%1.2f", wofaktor.faktor);
				s_wo14 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo14.Format( _T("1,00"));
				s_wo14 = 0;
			}
			break;

		case 15 : 
			if ( di  || wofaktor.kw != 15 )
			{
				v_wo15.Format( _T("1,00"));
				s_wo15 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 15 )
			{
				v_wo15.Format( "%1.2f", wofaktor.faktor);
				s_wo15 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo15.Format( _T("1,00"));
				s_wo15 = 0;
			}
			break;

		case 16 : 
			if ( di  || wofaktor.kw != 16 )
			{
				v_wo16.Format( _T("1,00"));
				s_wo16 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 16 )
			{
				v_wo16.Format( "%1.2f", wofaktor.faktor);
				s_wo16 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo16.Format( _T("1,00"));
				s_wo16 = 0;
			}
			break;

		case 17 : 
			if ( di  || wofaktor.kw != 17 )
			{
				v_wo17.Format( _T("1,00"));
				s_wo17 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 17 )
			{
				v_wo17.Format( "%1.2f", wofaktor.faktor);
				s_wo17 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo17.Format( _T("1,00"));
				s_wo17 = 0;
			}
			break;

		case 18 : 
			if ( di  || wofaktor.kw != 18 )
			{
				v_wo18.Format( _T("1,00"));
				s_wo18 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 18 )
			{
				v_wo18.Format( "%1.2f", wofaktor.faktor);
				s_wo18 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo18.Format( _T("1,00"));
				s_wo18 = 0;
			}
			break;

		case 19 : 
			if ( di  || wofaktor.kw != 19 )
			{
				v_wo19.Format( _T("1,00"));
				s_wo19 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 19 )
			{
				v_wo19.Format( "%1.2f", wofaktor.faktor);
				s_wo19 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo19.Format( _T("1,00"));
				s_wo19 = 0;
			}
			break;

		case 20 : 
			if ( di  || wofaktor.kw != 20 )
			{
				v_wo20.Format( _T("1,00"));
				s_wo20 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 20 )
			{
				v_wo20.Format( "%1.2f", wofaktor.faktor);
				s_wo20 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo20.Format( _T("1,00"));
				s_wo20 = 0;
			}
			break;

		case 21 : 
			if ( di  || wofaktor.kw != 21 )
			{
				v_wo21.Format( _T("1,00"));
				s_wo21 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 21 )
			{
				v_wo21.Format( "%1.2f", wofaktor.faktor);
				s_wo21 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo21.Format( _T("1,00"));
				s_wo21 = 0;
			}
			break;

		case 22 : 
			if ( di  || wofaktor.kw != 22 )
			{
				v_wo22.Format( _T("1,00"));
				s_wo22 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 22 )
			{
				v_wo22.Format( "%1.2f", wofaktor.faktor);
				s_wo22 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo22.Format( _T("1,00"));
				s_wo22 = 0;
			}
			break;

		case 23 : 
			if ( di  || wofaktor.kw != 23 )
			{
				v_wo23.Format( _T("1,00"));
				s_wo23 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 23 )
			{
				v_wo23.Format( "%1.2f", wofaktor.faktor);
				s_wo23 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo23.Format( _T("1,00"));
				s_wo23 = 0;
			}
			break;

		case 24 : 
			if ( di  || wofaktor.kw != 24 )
			{
				v_wo24.Format( _T("1,00"));
				s_wo24 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 24 )
			{
				v_wo24.Format( "%1.2f", wofaktor.faktor);
				s_wo24 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo24.Format( _T("1,00"));
				s_wo24 = 0;
			}
			break;

		case 25 : 
			if ( di  || wofaktor.kw != 25 )
			{
				v_wo25.Format( _T("1,00"));
				s_wo25 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 25 )
			{
				v_wo25.Format( "%1.2f", wofaktor.faktor);
				s_wo25 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo25.Format( _T("1,00"));
				s_wo25 = 0;
			}
			break;

		case 26 : 
			if ( di  || wofaktor.kw != 26 )
			{
				v_wo26.Format( _T("1,00"));
				s_wo26 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 26 )
			{
				v_wo26.Format( "%1.2f", wofaktor.faktor);
				s_wo26 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo26.Format( _T("1,00"));
				s_wo26 = 0;
			}
			break;

		case 27 : 
			if ( di  || wofaktor.kw != 27 )
			{
				v_wo27.Format( _T("1,00"));
				s_wo27 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 27 )
			{
				v_wo27.Format( "%1.2f", wofaktor.faktor);
				s_wo27 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo27.Format( _T("1,00"));
				s_wo27 = 0;
			}
			break;

		case 28 : 
			if ( di  || wofaktor.kw != 28 )
			{
				v_wo28.Format( _T("1,00"));
				s_wo28 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 28 )
			{
				v_wo28.Format( "%1.2f", wofaktor.faktor);
				s_wo28 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo28.Format( _T("1,00"));
				s_wo28 = 0;
			}
			break;

		case 29 : 
			if ( di  || wofaktor.kw != 29 )
			{
				v_wo29.Format( _T("1,00"));
				s_wo29 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 29 )
			{
				v_wo29.Format( "%1.2f", wofaktor.faktor);
				s_wo29 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo29.Format( _T("1,00"));
				s_wo29 = 0;
			}
			break;

		case 30 : 
			if ( di  || wofaktor.kw != 30 )
			{
				v_wo30.Format( _T("1,00"));
				s_wo30 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 30 )
			{
				v_wo30.Format( "%1.2f", wofaktor.faktor);
				s_wo30 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo30.Format( _T("1,00"));
				s_wo30 = 0;
			}
			break;

		case 31 : 
			if ( di  || wofaktor.kw != 31 )
			{
				v_wo31.Format( _T("1,00"));
				s_wo31 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 31 )
			{
				v_wo31.Format( "%1.2f", wofaktor.faktor);
				s_wo31 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo31.Format( _T("1,00"));
				s_wo31 = 0;
			}
			break;

		case 32 : 
			if ( di  || wofaktor.kw != 32 )
			{
				v_wo32.Format( _T("1,00"));
				s_wo32 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 32 )
			{
				v_wo32.Format( "%1.2f", wofaktor.faktor);
				s_wo32 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo32.Format( _T("1,00"));
				s_wo32 = 0;
			}
			break;

		case 33 : 
			if ( di  || wofaktor.kw != 33 )
			{
				v_wo33.Format( _T("1,00"));
				s_wo33 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 33 )
			{
				v_wo33.Format( "%1.2f", wofaktor.faktor);
				s_wo33 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo33.Format( _T("1,00"));
				s_wo33 = 0;
			}
			break;

		case 34 : 
			if ( di  || wofaktor.kw != 34 )
			{
				v_wo34.Format( _T("1,00"));
				s_wo34 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 34 )
			{
				v_wo34.Format( "%1.2f", wofaktor.faktor);
				s_wo34 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo34.Format( _T("1,00"));
				s_wo34 = 0;
			}
			break;

		case 35 : 
			if ( di  || wofaktor.kw != 35 )
			{
				v_wo35.Format( _T("1,00"));
				s_wo35 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 35 )
			{
				v_wo35.Format( "%1.2f", wofaktor.faktor);
				s_wo35 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo35.Format( _T("1,00"));
				s_wo35 = 0;
			}
			break;

		case 36 : 
			if ( di  || wofaktor.kw != 36 )
			{
				v_wo36.Format( _T("1,00"));
				s_wo36 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 36 )
			{
				v_wo36.Format( "%1.2f", wofaktor.faktor);
				s_wo36 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo36.Format( _T("1,00"));
				s_wo36 = 0;
			}
			break;

		case 37 : 
			if ( di  || wofaktor.kw != 37 )
			{
				v_wo37.Format( _T("1,00"));
				s_wo37 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 37 )
			{
				v_wo37.Format( "%1.2f", wofaktor.faktor);
				s_wo37 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo37.Format( _T("1,00"));
				s_wo37 = 0;
			}
			break;

		case 38 : 
			if ( di  || wofaktor.kw != 38 )
			{
				v_wo38.Format( _T("1,00"));
				s_wo38 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 38 )
			{
				v_wo38.Format( "%1.2f", wofaktor.faktor);
				s_wo38 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo38.Format( _T("1,00"));
				s_wo38 = 0;
			}
			break;

		case 39 : 
			if ( di  || wofaktor.kw != 39 )
			{
				v_wo39.Format( _T("1,00"));
				s_wo39 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 39 )
			{
				v_wo39.Format( "%1.2f", wofaktor.faktor);
				s_wo39 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo39.Format( _T("1,00"));
				s_wo39 = 0;
			}
			break;

		case 40 : 
			if ( di  || wofaktor.kw != 40 )
			{
				v_wo40.Format( _T("1,00"));
				s_wo40 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 40 )
			{
				v_wo40.Format( "%1.2f", wofaktor.faktor);
				s_wo40 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo40.Format( _T("1,00"));
				s_wo40 = 0;
			}
			break;

		case 41 : 
			if ( di  || wofaktor.kw != 41 )
			{
				v_wo41.Format( _T("1,00"));
				s_wo41 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 41 )
			{
				v_wo41.Format( "%1.2f", wofaktor.faktor);
				s_wo41 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo41.Format( _T("1,00"));
				s_wo41 = 0;
			}
			break;

		case 42 : 
			if ( di  || wofaktor.kw != 42 )
			{
				v_wo42.Format( _T("1,00"));
				s_wo42 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 42 )
			{
				v_wo42.Format( "%1.2f", wofaktor.faktor);
				s_wo42 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo42.Format( _T("1,00"));
				s_wo42 = 0;
			}
			break;

		case 43 : 
			if ( di  || wofaktor.kw != 43 )
			{
				v_wo43.Format( _T("1,00"));
				s_wo43 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 43 )
			{
				v_wo43.Format( "%1.2f", wofaktor.faktor);
				s_wo43 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo43.Format( _T("1,00"));
				s_wo43 = 0;
			}
			break;

		case 44 : 
			if ( di  || wofaktor.kw != 44 )
			{
				v_wo44.Format( _T("1,00"));
				s_wo44 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 44 )
			{
				v_wo44.Format( "%1.2f", wofaktor.faktor);
				s_wo44 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo44.Format( _T("1,00"));
				s_wo44 = 0;
			}
			break;

		case 45 : 
			if ( di  || wofaktor.kw != 45 )
			{
				v_wo45.Format( _T("1,00"));
				s_wo45 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 45 )
			{
				v_wo45.Format( "%1.2f", wofaktor.faktor);
				s_wo45 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo45.Format( _T("1,00"));
				s_wo45 = 0;
			}
			break;

		case 46 : 
			if ( di  || wofaktor.kw != 46 )
			{
				v_wo46.Format( _T("1,00"));
				s_wo46 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 46 )
			{
				v_wo46.Format( "%1.2f", wofaktor.faktor);
				s_wo46 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo46.Format( _T("1,00"));
				s_wo46 = 0;
			}
			break;

		case 47 : 
			if ( di  || wofaktor.kw != 47 )
			{
				v_wo47.Format( _T("1,00"));
				s_wo47 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 47 )
			{
				v_wo47.Format( "%1.2f", wofaktor.faktor);
				s_wo47 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo47.Format( _T("1,00"));
				s_wo47 = 0;
			}
			break;

		case 48 : 
			if ( di  || wofaktor.kw != 48 )
			{
				v_wo48.Format( _T("1,00"));
				s_wo48 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 48 )
			{
				v_wo48.Format( "%1.2f", wofaktor.faktor);
				s_wo48 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo48.Format( _T("1,00"));
				s_wo48 = 0;
			}
			break;

		case 49 : 
			if ( di  || wofaktor.kw != 49 )
			{
				v_wo49.Format( _T("1,00"));
				s_wo49 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 49 )
			{
				v_wo49.Format( "%1.2f", wofaktor.faktor);
				s_wo49 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo49.Format( _T("1,00"));
				s_wo49 = 0;
			}
			break;

		case 50 : 
			if ( di  || wofaktor.kw != 50 )
			{
				v_wo50.Format( _T("1,00"));
				s_wo50 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 50 )
			{
				v_wo50.Format( "%1.2f", wofaktor.faktor);
				s_wo50 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo50.Format( _T("1,00"));
				s_wo50 = 0;
			}
			break;

		case 51 : 
			if ( di  || wofaktor.kw != 51 )
			{
				v_wo51.Format( _T("1,00"));
				s_wo51 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 51 )
			{
				v_wo51.Format( "%1.2f", wofaktor.faktor);
				s_wo51 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo51.Format( _T("1,00"));
				s_wo51 = 0;
			}
			break;

		case 52 : 
			if ( di  || wofaktor.kw != 52 )
			{
				v_wo52.Format( _T("1,00"));
				s_wo52 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 52 )
			{
				v_wo52.Format( "%1.2f", wofaktor.faktor);
				s_wo52 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo52.Format( _T("1,00"));
				s_wo52 = 0;
			}
			break;

		case 53 : 
			if ( di  || wofaktor.kw != 53 )
			{
				v_wo53.Format( _T("1,00"));
				s_wo53 = 0;
				if ( di ) break;
			}
			if ( wofaktor.kw == 53 )
			{
				v_wo53.Format( "%1.2f", wofaktor.faktor);
				s_wo53 = wofaktor.faktor;
				di = Wofaktor.lesewofaktor();
				break;
			}
			else
			{
				v_wo53.Format( _T("1,00"));
				s_wo53 = 0;
			}
			break;

		}
	}

}

BOOL CwofaktorDlg::PreTranslateMessage(LPMSG lpMsg)
{
	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
		 {
		 
		case VK_RETURN :
			cWnd = GetFocus ();
			if(cWnd == GetDlgItem (IDOK))
				return CDialog::PreTranslateMessage(lpMsg);
			if(cWnd == GetDlgItem (IDCANCEL))
			{
					OnCancel ();
 					return TRUE;
			}

/* --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- */
			NextDlgCtrl();
					return TRUE;

			case VK_F5 :
                   OnCancel ();
 			       return TRUE;
/* ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- */

/* ---->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ----- */


			case VK_DOWN :
				NextDlgCtrl();
	 			cWnd = GetFocus ();
				GetNextDlgTabItem(cWnd ,FALSE) ;
				return TRUE;
			case VK_UP :
 				PrevDlgCtrl();
				cWnd = GetFocus ();
			 	GetNextDlgTabItem(cWnd ,FALSE) ;
				return TRUE;
 			}
	 }

	return CDialog::PreTranslateMessage(lpMsg);

}


