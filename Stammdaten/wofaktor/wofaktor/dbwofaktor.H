#ifndef _WOFAKTOR_DEF
#define _WOFAKTOR_DEF

struct WOFAKTOR {

short jr;
short kw;
double faktor ;
};

extern struct WOFAKTOR wofaktor, wofaktor_null;


class WOFAKTOR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :

               int wofaktorinsert (void);
               int wofaktordelete (void);
               int lesewofaktor (void);
               int openwofaktor (void);
               WOFAKTOR_CLASS () : DB_CLASS ()
               {
               }
};


#endif

