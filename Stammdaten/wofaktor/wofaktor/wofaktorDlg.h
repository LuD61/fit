// wofaktorDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CwofaktorDlg-Dialogfeld
class CwofaktorDlg : public CDialog
{
// Konstruktion
public:
	CwofaktorDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_WOFAKTOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:


	void satzschreiben(void);
	void satzlesen(void);
	afx_msg BOOL PreTranslateMessage(LPMSG); 
	double s_wo1;
	double s_wo2;
	double s_wo3;
	double s_wo4;
	double s_wo5;
	double s_wo6;
	double s_wo7;
	double s_wo8;
	double s_wo9;
	double s_wo10;
	double s_wo11;
	double s_wo12;
	double s_wo13;
	double s_wo14;
	double s_wo15;
	double s_wo16;
	double s_wo17;
	double s_wo18;
	double s_wo19;
	double s_wo20;
	double s_wo21;
	double s_wo22;
	double s_wo23;
	double s_wo24;
	double s_wo25;
	double s_wo26;
	double s_wo27;
	double s_wo28;
	double s_wo29;
	double s_wo30;
	double s_wo31;
	double s_wo32;
	double s_wo33;
	double s_wo34;
	double s_wo35;
	double s_wo36;
	double s_wo37;
	double s_wo38;
	double s_wo39;
	double s_wo40;
	double s_wo41;
	double s_wo42;
	double s_wo43;
	double s_wo44;
	double s_wo45;
	double s_wo46;
	double s_wo47;
	double s_wo48;
	double s_wo49;
	double s_wo50;
	double s_wo51;
	double s_wo52;
	double s_wo53;


	CEdit m_jahr;
	long v_jahr;
	CEdit m_wo1;
	CEdit m_wo2;
	CEdit m_wo3;
	CEdit m_wo4;
	CEdit m_wo5;
	CEdit m_wo6;
	CEdit m_wo7;
	CEdit m_wo8;
	CEdit m_wo9;
	CEdit m_wo10;
	CEdit m_wo11;
	CEdit m_wo12;
	CEdit m_wo13;
	CEdit m_wo14;
	CEdit m_wo15;
	CEdit m_wo16;
	CEdit m_wo17;
	CEdit m_wo18;
	CEdit m_wo19;
	CEdit m_wo20;
	CEdit m_wo21;
	CEdit m_wo22;
	CEdit m_wo23;
	CEdit m_wo24;
	CEdit m_wo25;
	CEdit m_wo26;
	CEdit m_wo27;
	CEdit m_wo28;
	CEdit m_wo29;
	CEdit m_wo30;
	CEdit m_wo31;
	CEdit m_wo32;
	CEdit m_wo33;
	CEdit m_wo34;
	CEdit m_wo35;
	CEdit m_wo36;
	CEdit m_wo37;
	CEdit m_wo38;
	CEdit m_wo39;
	CEdit m_wo40;
	CEdit m_wo41;
	CEdit m_wo42;
	CEdit m_wo43;
	CEdit m_wo44;
	CEdit m_wo45;
	CEdit m_wo46;
	CEdit m_wo47;
	CEdit m_wo48;
	CEdit m_wo49;
	CEdit m_wo50;
	CEdit m_wo51;
	CEdit m_wo52;
	CEdit m_wo53;
	CString v_wo1;
	CString v_wo2;
	CString v_wo3;
	CString v_wo4;
	CString v_wo5;
	CString v_wo6;
	CString v_wo7;
	CString v_wo8;
	CString v_wo9;
	CString v_wo10;
	CString v_wo11;
	CString v_wo12;
	CString v_wo13;
	CString v_wo14;
	CString v_wo15;
	CString v_wo16;
	CString v_wo17;
	CString v_wo18;
	CString v_wo19;
	CString v_wo20;
	CString v_wo21;
	CString v_wo22;
	CString v_wo23;
	CString v_wo24;
	CString v_wo25;
	CString v_wo26;
	CString v_wo27;
	CString v_wo28;
	CString v_wo29;
	CString v_wo30;
	CString v_wo31;
	CString v_wo32;
	CString v_wo33;
	CString v_wo34;
	CString v_wo35;
	CString v_wo36;
	CString v_wo37;
	CString v_wo38;
	CString v_wo39;
	CString v_wo40;
	CString v_wo41;
	CString v_wo42;
	CString v_wo43;
	CString v_wo44;
	CString v_wo45;
	CString v_wo46;
	CString v_wo47;
	CString v_wo48;
	CString v_wo49;
	CString v_wo50;
	CString v_wo51;
	CString v_wo52;
	CString v_wo53;
	afx_msg void OnEnKillfocusEdit1();
	afx_msg void OnEnKillfocusEdit2();
	afx_msg void OnEnKillfocusEdit3();
	afx_msg void OnEnKillfocusEdit4();
	afx_msg void OnEnKillfocusEdit5();
	afx_msg void OnEnKillfocusEdit6();
	afx_msg void OnEnKillfocusEdit7();
	afx_msg void OnEnKillfocusEdit8();
	afx_msg void OnEnKillfocusEdit9();
	afx_msg void OnEnKillfocusEdit10();
	afx_msg void OnEnKillfocusEdit11();
	afx_msg void OnEnKillfocusEdit12();
	afx_msg void OnEnKillfocusEdit13();
	afx_msg void OnEnKillfocusEdit14();
	afx_msg void OnEnKillfocusEdit15();
	afx_msg void OnEnKillfocusEdit16();
	afx_msg void OnEnKillfocusEdit17();
	afx_msg void OnEnKillfocusEdit18();
	afx_msg void OnEnKillfocusEdit19();
	afx_msg void OnEnKillfocusEdit20();
	afx_msg void OnEnKillfocusEdit21();
	afx_msg void OnEnKillfocusEdit22();
	afx_msg void OnEnKillfocusEdit23();
	afx_msg void OnEnKillfocusEdit24();
	afx_msg void OnEnKillfocusEdit25();
	afx_msg void OnEnKillfocusEdit26();
	afx_msg void OnEnKillfocusEdit27();
	afx_msg void OnEnKillfocusEdit28();
	afx_msg void OnEnKillfocusEdit29();
	afx_msg void OnEnKillfocusEdit30();
	afx_msg void OnEnKillfocusEdit31();
	afx_msg void OnEnKillfocusEdit32();
	afx_msg void OnEnKillfocusEdit33();
	afx_msg void OnEnKillfocusEdit34();
	afx_msg void OnEnKillfocusEdit35();
	afx_msg void OnEnKillfocusEdit36();
	afx_msg void OnEnKillfocusEdit37();
	afx_msg void OnEnKillfocusEdit38();
	afx_msg void OnEnKillfocusEdit39();
	afx_msg void OnEnKillfocusEdit40();
	afx_msg void OnEnKillfocusEdit41();
	afx_msg void OnEnKillfocusEdit42();
	afx_msg void OnEnKillfocusEdit43();
	afx_msg void OnEnKillfocusEdit44();
	afx_msg void OnEnKillfocusEdit45();
	afx_msg void OnEnKillfocusEdit46();
	afx_msg void OnEnKillfocusEdit47();
	afx_msg void OnEnKillfocusEdit48();
	afx_msg void OnEnKillfocusEdit49();
	afx_msg void OnEnKillfocusEdit50();
	afx_msg void OnEnKillfocusEdit51();
	afx_msg void OnEnKillfocusEdit52();
	afx_msg void OnEnKillfocusEdit53();

	afx_msg void OnBnClickedButtup();
	afx_msg void OnBnClickedButtdown();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
};
