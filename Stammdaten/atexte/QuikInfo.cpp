#include "StdAfx.h"
#include ".\quikinfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CQuikInfo::CQuikInfo(void)
{
	NoUpdate = FALSE;
	TextColor = RGB (0, 0, 0);
	BkColor = RGB (255, 255, 255);
	BorderStyle = Simple;
	WindowOrientation = Right;
	Orientation = Center;
	Delay = 2000;
	Tool = NULL;
	Text = "";
}

CQuikInfo::~CQuikInfo(void)
{
}

BEGIN_MESSAGE_MAP(CQuikInfo, CButton)
	ON_WM_TIMER ()
	ON_WM_LBUTTONDOWN ()
	ON_WM_KEYDOWN ()
END_MESSAGE_MAP()

BOOL CQuikInfo::Create(LPCTSTR lpszText, CWnd *Tool, DWORD dwStyle, 
					   RECT& rect, CWnd* pParentWnd)
{
	Text = lpszText;
	this->Tool = Tool;
//	dwStyle |= SS_OWNERDRAW;
	dwStyle |= BS_OWNERDRAW;
//	return CStatic::Create (lpszText, dwStyle, rect, pParentWnd);
	return CButton::Create (lpszText, dwStyle, rect, pParentWnd, 7000);
}

void CQuikInfo::Draw (CDC& cDC)
{
	CRect rect;
	CRect fillRect;
	CSize Size;

	NoUpdate = TRUE;
	GetClientRect (&rect);
	COLORREF Background;
	fillRect = rect;

	Background = BkColor; 
	cDC.FillRect (&rect, &CBrush (Background));
	CBrush Brush;
	if (BorderStyle == Simple)
	{
		Brush.CreateSolidBrush (RGB (0, 0, 0));
		cDC.FrameRect (&rect, &Brush);
	}
	else if (BorderStyle == Dialog)
	{
		Brush.CreateSolidBrush (RGB (255, 255, 255));
		cDC.FrameRect (&rect, &Brush);
		rect.top ++;
		rect.left ++;
		Brush.DeleteObject ();
		Brush.CreateSolidBrush (RGB (0, 0, 0));
		cDC.FrameRect (&rect, &Brush);
		rect.right --;
		rect.bottom --;
		Brush.DeleteObject ();
		Brush.CreateSolidBrush (RGB (192, 192, 192));
		cDC.FrameRect (&rect, &Brush);
	}
	CString T = Text;
	LPTSTR t = _tcstok (T.GetBuffer (), _T("\n"));
	int y = 0;
	int row = 0;
	while (t != NULL)
	{
		CString T = t;
		Size = cDC.GetTextExtent (T);
		y = row * Size.cy;
		row ++;
	    t = _tcstok (NULL, _T("\n"));
	}
	if (row > 0)
	{
		y = row * Size.cy;
	}
    
	int start = max (0, (rect.bottom - y) / 2);

	T = Text;
	t = _tcstok (T.GetBuffer (), _T("\n"));
	row = 0;

	while (t != NULL)
	{
		CString T = t;
		Size = cDC.GetTextExtent (T);
		int x = 0;
		if (Orientation == Center)
		{
			x = max (0, (rect.right - Size.cx) / 2);
		}
		else if (Orientation == Right)
		{
			x = max (0, (rect.right - Size.cx));
		}
		y = row * Size.cy + start;
		cDC.SetBkMode (TRANSPARENT);
		if (IsWindowEnabled ())
		{
			cDC.SetTextColor (TextColor);
			cDC.TextOut (x, y, t, (int) _tcslen (t));
		}
		else
		{
			cDC.SetTextColor (RGB (192, 192, 192));
			cDC.TextOut (x, y, t, (int) _tcslen (t));
			cDC.SetTextColor (RGB (255, 255, 255));
			cDC.TextOut (x + 1, y + 1, t, (int) _tcslen (t));
		}
		row ++;
	    t = _tcstok (NULL, _T("\n"));
	}
	NoUpdate = FALSE;

}

void CQuikInfo::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC cDC;
	cDC.Attach (lpDrawItemStruct->hDC);
	Draw (cDC);
}

void CQuikInfo::Show ()
{
	if (Tool == NULL) return;

	if (IsWindow (m_hWnd))
	{
		CRect rect;
		CRect crect;
		CRect prect;
        GetParent ()->GetClientRect (&prect);
		Tool->GetWindowRect (&rect);
		GetParent ()->ScreenToClient (&rect);
		GetClientRect (&crect);
		rect.top = rect.bottom;
		rect.bottom = rect.top + crect.bottom;
		if (WindowOrientation == Right)
		{
			if (rect.left + crect.right < prect.right)
			{
				rect.left = rect.right;
				rect.right = rect.left + crect.right;
			}
			else
			{
				rect.right = rect.left;
				rect.left = rect.right - crect.right;
			}
		}
		else if (WindowOrientation == Left)
		{
			if (rect.right - crect.right > 0)
			{
				rect.right = rect.left;
				rect.left = rect.right - crect.right;
			}
			else
			{
				rect.left = rect.right;
				rect.right = rect.left + crect.right;
			}
		}
		else if (WindowOrientation == Center)
		{
			rect.left = rect.left + (rect.right - rect.left - crect.right) / 2;
			if (rect.left + crect.right < prect.right)
			{
				rect.right = rect.left + crect.right;
			}
			else if (rect.left > 0)
			{
				rect.left = prect.right - crect.right;
				rect.right = rect.left + crect.right;
			}
			if (rect.left < 0) rect.left = 0;
		}
		MoveWindow (&rect, FALSE);

		ShowWindow (SW_SHOWNORMAL);
		SetTimer (1, Delay, NULL);
	}
}

void CQuikInfo::Show (CPoint p)
{
	if (Tool == NULL) return;

	if (IsWindow (m_hWnd))
	{
		CRect crect;
		GetWindowRect (&crect);
		int cx = crect.right - crect.left;
		int cy = crect.bottom - crect.top;
		crect.left = p.x;
		crect.top = p.y;
		crect.right = crect.left + cx;
		crect.bottom = crect.top + cy;
        Tool->ClientToScreen (&crect);
		GetParent ()->ScreenToClient (&crect);

		MoveWindow (&crect, FALSE);

		ShowWindow (SW_SHOWNORMAL);
//		SetTimer (2, Delay, NULL);
		SetCapture ();

	}
}

void CQuikInfo::Hide ()
{
	if (IsWindow (m_hWnd))
	{
		ShowWindow (SW_HIDE);
		KillTimer (1);
		ReleaseCapture ();
	}
}

void CQuikInfo::OnTimer(UINT_PTR nIDEvent)
{
	Hide ();
}

void CQuikInfo::SetSize ()
{
	if (!IsWindow (m_hWnd)) return;
	CString T = Text;
	LPTSTR t = _tcstok (T.GetBuffer (), _T("\n"));
	int row = 0;
	int cx = 0;
	int cy = 0;

	CDC *cDC = GetDC ();
	while (t != NULL)
	{
		CString T = t;
		CSize Size = cDC->GetTextExtent (T);
		if (Size.cx > cx) cx = Size.cx;
		row ++;
	    t = _tcstok (NULL, _T("\n"));
	}
	TEXTMETRIC tm;
	cDC->GetTextMetrics (&tm);
	cy = row * tm.tmHeight + 2;
	CRect rect;
	GetWindowRect (&rect);
	GetParent ()->ScreenToClient (&rect);
	rect.right = rect.left + cx + 2;
	rect.bottom = rect.top + cy;
	MoveWindow (&rect);
	ReleaseDC (cDC);
}

void CQuikInfo::SetText (LPTSTR Text)
{
	this->Text = Text;
	SetSize ();
}

void CQuikInfo::OnLButtonDown (UINT Flags, CPoint point)
{
	GetParent ()->ClientToScreen (&point);
	Hide ();
}

void CQuikInfo::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags )
{
	Hide ();
}
