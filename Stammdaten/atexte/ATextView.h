#pragma once
#include "afxwin.h"
#include "numedit.h"
#include "richtextedit.h"
#include "CtrlGrid.h"
#include "DbFormView.h"
#include "EditLine.h"
#include "AText.h"
#include "ChoiceSys.h"
#include "ChoiceAtxt.h"
#include "Sys_peri.h"
#include "Fil.h"
#include "Adr.h"
#include "eti_typ.h"
#include "FormTab.h"
#include "PTabn.h"
#include "afxcmn.h"

#define IDC_SYSCHOICE 1050
#define IDC_TXTCHOICE 1051

#define IDC_EDITLINE 1052

// CATextView-Formularansicht

class CATextView : public DbFormView
{
	DECLARE_DYNCREATE(CATextView)

protected:
	CATextView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CATextView();

public:
	enum { IDD = IDD_AText };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
    virtual void OnInitialUpdate ();

public:
	CChoiceAtxt *Choice;
	BOOL ModalChoice;
	CChoiceSys *SysChoice;
	BOOL ModalChoiceSys;

    ATEXTE_CLASS ATexte;
	SYS_PERI_CLASS Sys_peri;
	FIL_CLASS Fil;
	ADR_CLASS Adr;
	ETI_TYP_CLASS Eti_typ;

	short *SgTab[20];
	short *ZeTab[20];
	short sg;

	CVector HeadControls;

	CStatic m_LSys;
	CNumEdit m_Sys;
	CStatic m_LTxtNr;
	CNumEdit m_TxtNr;
	CButton m_SendCtrl;
	CRichTextEdit m_Txt;
	CStatic m_LSg;
	CComboBox m_Sg;
	CButton m_SysChoice;
	CButton m_TxtChoice;
	CEditLine m_EditLine;

	CFormTab Keys;
	CFormTab Form;
	CFormTab TextForm;

	CCtrlGrid CtrlGrid;
	CCtrlGrid SysGrid;
	CCtrlGrid TxtGrid;
	CCtrlGrid SgGrid;
	CCtrlGrid EditGrid;
	CCtrlGrid IdxGrid;
    afx_msg void OnSize(UINT, int, int);
	afx_msg void OnBack();
	afx_msg void OnFileSave();
	afx_msg void OnSysChoice();
	afx_msg void OnChoice();
	CEdit m_AdrNam1;
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnSelected();
	afx_msg void OnCanceled();
    BOOL OnReturn ();
    BOOL OnKeyup ();
	BOOL ReadSys ();
	BOOL Read ();
	void write ();
	void OnDelete ();
	void FillSgCombo ();
    void EnableFields (BOOL b);
    void EnableHeadControls (BOOL enable);
	afx_msg void OnCbnSelchangeSg();
	afx_msg void OnTextCent();
	afx_msg void OnTextLeft();
	afx_msg void OnTextRight();
	afx_msg void OnUpdateDelete(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFileSave(CCmdUI *pCmdUI);
	afx_msg void OnUpdateTextCent(CCmdUI *pCmdUI);
	afx_msg void OnUpdateTextLeft(CCmdUI *pCmdUI);
	afx_msg void OnUpdateTextRight(CCmdUI *pCmdUI);
	afx_msg void OnEnChangeTxt();
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
	afx_msg void OnCopySys();
	afx_msg void OnUpdateCopySys(CCmdUI *pCmdUI);
	afx_msg void OnJoinarticle();
	afx_msg void OnUpdateJoinarticle(CCmdUI *pCmdUI);
	afx_msg void OnSend();
	afx_msg void OnFirst();
	afx_msg void OnUpdateFirst(CCmdUI *pCmdUI);
	afx_msg void OnPrior();
	afx_msg void OnUpdatePrior(CCmdUI *pCmdUI);
	afx_msg void OnNext();
	afx_msg void OnUpdateNext(CCmdUI *pCmdUI);
	afx_msg void OnLast();
	afx_msg void OnUpdateLast(CCmdUI *pCmdUI);
	afx_msg void OnFett();
	afx_msg void OnUpdateFett(CCmdUI *pCmdUI);
	afx_msg void OnKursiv();
	afx_msg void OnUpdateKursiv(CCmdUI *pCmdUI);
	afx_msg void OnUnderline();
	afx_msg void OnUpdateUnderline(CCmdUI *pCmdUI);

    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	CStatic m_LTxtType;
	CComboBox m_TxtType;
	PTABN_CLASS Ptabn;
    void FillTxtType ();
	CStatic m_LTxtIdx;
	CEdit m_TxtIdx;
	CSpinButtonCtrl m_TxtIdxSpin;
	afx_msg void OnDlgBackground();
	afx_msg void OnChoicebackground();
	long GetTextNrFromCommandline ();
};


