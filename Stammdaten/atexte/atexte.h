// ATexte.h : Hauptheaderdatei f�r die ATexte-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error 'stdafx.h' muss vor dieser Datei in PCH eingeschlossen werden.
#endif

#include "resource.h"       // Hauptsymbole


// CATexteApp:
// Siehe ATexte.cpp f�r die Implementierung dieser Klasse
//

class CATexteApp : public CWinApp
{
public:
	CATexteApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CATexteApp theApp;
