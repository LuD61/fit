#include "StdAfx.h"
#include "ATextList.h"

CATextList::CATextList(void)
{
	sys = 0;
	txt_nr = 0;
	disp_txt = _T("");
	txt = _T("");
}

CATextList::CATextList(long sys, long txt_nr, LPTSTR disp_txt, LPTSTR txt)
{
	this->sys     = sys;
	this->txt_nr  = txt_nr;
	this->disp_txt= disp_txt;
	this->txt     = txt;
}

CATextList::~CATextList(void)
{
}
