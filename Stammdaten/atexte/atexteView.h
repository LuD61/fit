// atexteView.h : Schnittstelle der Klasse CatexteView
//


#pragma once


class CatexteView : public CView
{
protected: // Nur aus Serialisierung erstellen
	CatexteView();
	DECLARE_DYNCREATE(CatexteView)

// Attribute
public:
	CATexteDoc* GetDocument() const;

// Operationen
public:

// �berschreibungen
	public:
	virtual void OnDraw(CDC* pDC);  // �berladen, um diese Ansicht darzustellen
virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~CatexteView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};

/*
#ifndef _DEBUG  // Debugversion in atexteView.cpp
inline CATexteDoc* CATextView::GetDocument() const
   { return reinterpret_cast<CATexteDoc*>(m_pDocument); }
#endif
*/

