#include "StdAfx.h"
#include ".\joinarticle.h"
#include "ABasList.h"

CJoinArticle::CJoinArticle(void)
{
	Cursor = -1;
	txt_nr = 0;
	idx = 1;
}

CJoinArticle::CJoinArticle(long txt_nr, short idx)
{
	Cursor = -1;
	this->txt_nr = txt_nr;
	this->idx = idx;
}

CJoinArticle::~CJoinArticle(void)
{
	Close ();
}

void CJoinArticle::Close ()
{
	A_bas.dbclose ();
}

void CJoinArticle::Join(CVector &AbasTab)
{
	AbasTab.FirstPosition ();
	CABasList *al;
	while ((al = (CABasList *) AbasTab.GetNext ()) != NULL)
	{
		A_bas.a_bas.a = al->a;
		A_bas.dbreadfirst ();
		switch (idx)
		{
		case 1:
			A_bas.a_bas.txt_nr1 = txt_nr;
			break;
		case 2:
			A_bas.a_bas.txt_nr2 = txt_nr;
			break;
		case 3:
			A_bas.a_bas.txt_nr3 = txt_nr;
			break;
		case 4:
			A_bas.a_bas.txt_nr4 = txt_nr;
			break;
		case 5:
			A_bas.a_bas.txt_nr5 = txt_nr;
			break;
		case 6:
			A_bas.a_bas.txt_nr6 = txt_nr;
			break;
		case 7:
			A_bas.a_bas.txt_nr7 = txt_nr;
			break;
		case 8:
			A_bas.a_bas.txt_nr8 = txt_nr;
			break;
		case 9:
			A_bas.a_bas.txt_nr9 = txt_nr;
			break;
		case 10:
			A_bas.a_bas.txt_nr10 = txt_nr;
			break;
		}
		A_bas.dbupdate ();
	}
}

