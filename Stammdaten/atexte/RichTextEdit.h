#pragma once
#include "afxcmn.h"
#include "Properties.h"
#include "TxtSg.h"
#include "Vector.h"
#include "QuikInfo.h"

class SgPos
{
public:
	int pos;
	CString Sg;
	SgPos ()
	{
		pos = 0;
		Sg = _T("");
	}

};


class CRichTextEdit :
	public CRichEditCtrl
{
	DECLARE_DYNCREATE(CRichTextEdit)
protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	enum
	{
		Left = 1,
		Center = 2,
		Right = 3
	};

	CString DisplayText;
	int lastSize;
	CString FaceName;
	BOOL Fett;
	BOOL Underline;
	BOOL Kursiv;
	int Alignment;
	CQuikInfo SgInfo;
	SgPos **SgPosis;
	CVector TextFormat;
	CProperties SgTab;
	CRichTextEdit(void);
	~CRichTextEdit(void);
	void SetWindowText (LPCTSTR txt);
	void GetWindowText (CString& txt);
	void GetText (CString& txt);
    void CRichTextEdit::SetDefaultSize (int size);
    void SetSelectionCharSize (int size);
	void CleanSelected (CHARRANGE& cr);
    void SetTextSg (int size);
	void GetTextSg (int row, CString *Txt);
	void GetLinePos (int pos, int& row, int& column);
	int GetSelectedSG ();
    void GetPos (int& pos, int row, int column);
	void SetTextFormat (int row, int column, int size);
	void SetTextFormat (int row, int column, int size, 
 					    int Fett, int Kursiv, int Underline);
	void SetTextOut (int row, int column, int Attribut);
    void DestroyFormat ();
	void Copy ();
	void Paste ();
    void ToClipboard (CString& Text);
    void FromClipboard (CString& Text);
	void SetAlignment (int Alignment);
	static int __cdecl ComparePos (const void *el1, const void *el2);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTextCent();
	afx_msg void OnTextLeft();
	afx_msg void OnTextRight();
	void CleanText (CString& Text);
};
