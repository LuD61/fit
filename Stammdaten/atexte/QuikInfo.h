#pragma once
#include "afxwin.h"

class CQuikInfo :
	public CButton
{

protected:
	CString Text;
	BOOL NoUpdate;
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
	DECLARE_MESSAGE_MAP()

public:
	CQuikInfo(void);
	~CQuikInfo(void);

	enum
	{
		Center = 0,
		Left = 1,
		Right = 2,
	};

	enum enumBorderStyle
	{
		No,
		Simple,
		Dialog
	};

	int Orientation;
	int WindowOrientation;
	COLORREF TextColor;
	COLORREF BkColor;
	enumBorderStyle BorderStyle;
	long Delay;
	CWnd* Tool;

	virtual BOOL Create(LPCTSTR lpszText, CWnd *Tool,  
		                DWORD dwStyle, RECT& rect,   
						CWnd* pParentWnd); 

	void Draw (CDC&);

	void Show ();
	void Show (CPoint p);
	void Hide ();
	void SetSize ();
	void SetText (LPTSTR Text);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLButtonDown (UINT Flags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags );
};
