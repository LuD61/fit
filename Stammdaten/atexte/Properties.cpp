#include "StdAfx.h"
#include "properties.h"
#include "Token.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CProperties::CProperties(void)
{
}

CProperties::~CProperties(void)
{
}

CString CProperties::GetValue (CString Name)
{
	FirstPosition ();
	CPropertyItem *p;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Name.CompareNoCase (Name) == 0)
		{
			return p->Value;
		}
	}
	return Name;
}

CString CProperties::GetName (CString Value)
{
	FirstPosition ();
	CPropertyItem *p;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Value.CompareNoCase (Value) == 0)
		{
			return p->Name;
		}
	}
	return Value;
}

int CProperties::Find (CString Name)
{
	FirstPosition ();
	CPropertyItem *p;
	int i = 0;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Name.CompareNoCase (Name) == 0)
		{
			return i;
		}
		i ++;
	}
	return -1;
}

int CProperties::FindValue (CString Value)
{
	FirstPosition ();
	CPropertyItem *p;
	int i = 0;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Value.CompareNoCase (Value) == 0)
		{
			return i;
		}
		i ++;
	}
	return -1;
}

void CProperties::Set (CString Name, CString Value, int idx)
{
	CPropertyItem *p;
	p = (CPropertyItem *) Get (idx);
	if (p != NULL)
	{
		p->Name = Name;
		p->Value = Value;
	}
}

BOOL CProperties::Load (LPTSTR FileName)
{
	FILE *fp;
	LPTSTR etc;
	CString Path;
	TCHAR record [256];

	etc = _tgetenv (_T("BWSETC"));
	if (etc != NULL)
	{
		Path.Format (_T("%s\\%s"), etc, FileName);
	}
	else
	{
		Path = FileName;
	}

	fp = _tfopen (Path.GetBuffer (), _T("r"));
	if (fp == NULL) return FALSE;
    
    DestroyAll ();
	Init ();
	while (_fgetts (record, 255, fp) != NULL)
	{
		if (record[0] == _T('#')) continue;
		CToken t (record, _T("="));
		if (t.GetAnzToken () < 2) continue;
        CString Name  = t.GetToken (0); 
        CString Value = t.GetToken (1); 
		Name.Trim ();
		Value.Trim ();
        Add (new CPropertyItem (Name, Value));
	}
    fclose (fp); 
	return TRUE;
}

