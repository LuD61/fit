#pragma once

class CTxtSg
{
public:
	enum
	{
		No = 0,
		On = 1,
		Off = 2,
	};

	enum
	{
		FETT = 0,
		KURSIV = 1,
		UNDERLINE = 2,
	};

	CFont *PcFont;
	CString DevFontName;
	int row;
	int col;
	int size;
	int attribute;
	CString FaceName;
	int Fett; 
	int Kursiv;
	int Underline;
	CTxtSg(void);
	CTxtSg(CFont* PcFont, int row, int col, int size);
	CTxtSg(int row, int col, LPTSTR FaceName, int size, int Fett, int Kursiv, int Underline);
	~CTxtSg(void);
	BOOL operator== (CPoint& p);
	BOOL equals (int row, int col);
};
