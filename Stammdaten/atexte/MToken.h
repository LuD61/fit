#pragma once
#include "token.h"

#ifdef UNICODE
#define TStrTok WStrTok
#else
#define TStrTok StrTok
#endif

class CMToken :
	public CToken
{
public:
	CMToken(void);
	~CMToken(void);
     virtual void GetTokens (LPTSTR);
     const CMToken& operator=(LPTSTR);
     const CMToken& operator=(CString&);
     static char * StrTok (char * str, char * sep);
     static wchar_t * WStrTok (wchar_t * str, wchar_t * sep);
};
