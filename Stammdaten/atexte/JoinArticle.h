#pragma once
#include "a_bas.h"
#include "Vector.h"

class CJoinArticle
{
public:
	int Cursor;
	long txt_nr;
	short idx;
	A_BAS_CLASS A_bas;
	CJoinArticle(void);
	CJoinArticle(long txt_nr, short idx);
	~CJoinArticle(void);
	void Close ();
	void Join (CVector& AbasTab);
};
