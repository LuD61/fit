#pragma once
#include "afxwin.h"


// CCopyDialog-Dialogfeld

class CCopyDialog : public CDialog
{
	DECLARE_DYNAMIC(CCopyDialog)

public:
	CCopyDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CCopyDialog();

// Dialogfelddaten
	enum { IDD = IDD_COPYDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
    virtual BOOL OnInitDialog ();

	DECLARE_MESSAGE_MAP()
public:
	BOOL CopyAll;
	CButton m_AktText;
	CButton m_AllText;
	afx_msg void OnBnClickedActtext();
	afx_msg void OnBnClickedAlltext();
};
