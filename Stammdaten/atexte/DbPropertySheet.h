#ifndef DB_PROPERTYSHEET_DEF
#define DB_PROPERTYSHEET_DEF
#pragma once
#include "afxdlgs.h"

class CDbPropertySheet :
	public CPropertySheet
{
	DECLARE_DYNAMIC(CDbPropertySheet)
protected :
	virtual BOOL OnInitDialog();
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	DECLARE_MESSAGE_MAP()

public:
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	BOOL Tabs;
	CDbPropertySheet(void);
	CDbPropertySheet(LPCTSTR);
	~CDbPropertySheet(void);
	virtual BOOL Read ();
	virtual BOOL Delete ();
	virtual BOOL Insert ();
	virtual BOOL DeleteAll ();
	virtual BOOL Write ();
	virtual BOOL Print ();
	virtual BOOL PrintAll ();
	virtual BOOL EnablePrint (CCmdUI *);
	virtual BOOL TextCent ();
	virtual BOOL EnableTextCent (CCmdUI *);
	virtual BOOL TextLeft ();
	virtual BOOL EnableTextLeft (CCmdUI *);
	virtual BOOL TextRight ();
	virtual BOOL EnableTextRight (CCmdUI *);
	virtual void Show ();
	virtual void StepBack ();
	virtual void NextRecord ();
	virtual void PriorRecord ();
	virtual void FirstRecord ();
	virtual void LastRecord ();
    virtual void OnCopy ();
    virtual void OnPaste ();
	afx_msg void OnBreak();
    virtual void OnMarkAll ();
    virtual void OnUnMarkAll ();
};
#endif