#include "StdAfx.h"
#include "dbformview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(DbFormView, CFormView)

DbFormView::DbFormView() : 
			CFormView ((UINT) 0)
{
	ArchiveName = _T("Formular.prp");
	DlgBkColor = NULL;
	DlgBrush = NULL;
	HideOK = FALSE;
}

DbFormView::DbFormView(UINT Id) : CFormView (Id)
{
	ArchiveName = _T("Formular.prp");
	DlgBkColor = NULL;
	DlgBrush = NULL;
	HideOK = FALSE;
}

DbFormView::~DbFormView(void)
{
}


DROPEFFECT DbFormView::OnDrop (CWnd* pWnd,  COleDataObject* pDataObject,
					                 	  DROPEFFECT dropDefault, DROPEFFECT dropList,
										  CPoint point)
{
	return DROPEFFECT_NONE;
}

void DbFormView::write ()
{
}

BOOL DbFormView::TextCent ()
{
	return TRUE;
}

BOOL DbFormView::TextLeft ()
{
	return TRUE;
}

BOOL DbFormView::TextRight ()
{
	return TRUE;
}

void DbFormView::Save ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	File.Open (Path.GetBuffer (), CFile::modeCreate | CFile::modeWrite);
	CArchive archive(&File, CArchive::store);

	try
	{
			archive.Write (&DlgBkColor, sizeof (DlgBkColor));
			archive.Write (&ListBkColor, sizeof (ListBkColor));
			archive.Write (&FlatLayout, sizeof (FlatLayout));
			archive.Write (&HideOK, sizeof (HideOK));
	}
	catch (...) 
	{
		return;
	}


	archive.Close ();
	File.Close ();
}

void DbFormView::Load ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	if (!File.Open (Path.GetBuffer (), CFile::modeRead)) return;
    CArchive archive(&File, CArchive::load);
	try
	{

			archive.Read (&DlgBkColor, sizeof (DlgBkColor));
			archive.Read (&ListBkColor, sizeof (ListBkColor));
			archive.Read (&FlatLayout, sizeof (FlatLayout));
			archive.Read (&HideOK, sizeof (HideOK));
	}
	catch (...) 
	{
		return;
	}

	archive.Close ();
	File.Close ();
}

