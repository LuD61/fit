#pragma once
#include "token.h"

#ifdef UNICODE
#define TStrTok WStrTok
#else
#define TStrTok StrTok
#endif

class CStrToken :
	public CToken
{
public:
	CStrToken(void);
	~CStrToken(void);
     virtual void GetTokens (LPTSTR);
     const CStrToken& operator=(LPTSTR);
     const CStrToken& operator=(CString&);
     static char * StrTok (char * str, char * sep);
     static wchar_t * WStrTok (wchar_t * str, wchar_t * sep);
};
