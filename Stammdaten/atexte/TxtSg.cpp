#include "StdAfx.h"
#include ".\txtsg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CTxtSg::CTxtSg(void)
{
	this->PcFont = NULL;
	this->row = 0;
	this->col = 0;
	this->size = 9;
	FaceName = _T("");
	Fett = FALSE;
	Kursiv = FALSE;
	Underline = FALSE;
}

CTxtSg::CTxtSg(CFont* PcFont, int row, int col, int size)
{
	this->PcFont = PcFont;
	this->row = row;
	this->col = col;
	this->size = size;
	FaceName = _T("");
	Fett = No;
	Kursiv = No;
	Underline = No;
}

CTxtSg::CTxtSg(int row, int col, LPTSTR FaceName, int size, int Fett, int Kursiv, int Underline)
{
	this->PcFont = NULL;
	this->row = row;
	this->col = col;
	this->size = size;
	this->FaceName = FaceName;
	this->Fett = Fett;
	this->Kursiv = Kursiv;
	this->Underline = Underline;
}

CTxtSg::~CTxtSg(void)
{
	if (PcFont != NULL)
	{
		delete PcFont;
	}
}

BOOL CTxtSg::equals (int row, int col)
{
	if (this->row == row &&
		this->col == col)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL CTxtSg::operator== (CPoint& p)
{
	return equals (p.y, p.x);
}