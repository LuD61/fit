#include "StdAfx.h"
#include ".\dbpropertypage.h"

IMPLEMENT_DYNAMIC(CDbPropertyPage, CPropertyPage)

CDbPropertyPage::CDbPropertyPage(void)
{
	DlgBkColor = NULL;
	DlgBrush = NULL;
	HideOK = FALSE;
	Update = NULL;
}

CDbPropertyPage::CDbPropertyPage(UINT IDD)
	: CPropertyPage(IDD)
{
	InData = FALSE;
	DlgBkColor = NULL;
	DlgBrush = NULL;
	HideOK = FALSE;
	Update = NULL;
}

CDbPropertyPage::~CDbPropertyPage(void)
{
	if (DlgBrush != NULL)
	{
		DeleteObject (DlgBrush);
		DlgBrush = 0;
	}
}

BOOL CDbPropertyPage::Read ()
{
	return TRUE;
}

BOOL CDbPropertyPage::Print ()
{
	return TRUE;
}

BOOL CDbPropertyPage::PrintAll ()
{
	return TRUE;
}

BOOL CDbPropertyPage::EnablePrint (CCmdUI *pCmdUI)
{
	return TRUE;
}

BOOL CDbPropertyPage::TextCent ()
{
	return TRUE;
}

BOOL CDbPropertyPage::EnableTextCent (CCmdUI *pCmdUI)
{
	return TRUE;
}

BOOL CDbPropertyPage::TextLeft ()
{
	return TRUE;
}

BOOL CDbPropertyPage::EnableTextLeft (CCmdUI *pCmdUI)
{
	return TRUE;
}
BOOL CDbPropertyPage::TextRight ()
{
	return TRUE;
}

BOOL CDbPropertyPage::EnableTextRight (CCmdUI *pCmdUI)
{
	return TRUE;
}

BOOL CDbPropertyPage::DeleteAll ()
{
	return TRUE;
}


BOOL CDbPropertyPage::Delete ()
{
	OnDelete ();
	return TRUE;
}

BOOL CDbPropertyPage::Insert ()
{
	OnInsert ();
	return TRUE;
}

BOOL CDbPropertyPage::Write ()
{
	return TRUE;
}

void CDbPropertyPage::write ()
{
}

void CDbPropertyPage::OnInsert ()
{
}

void CDbPropertyPage::OnDelete ()
{
}

BOOL CDbPropertyPage::AfterWrite ()
{
	return TRUE;
}

BOOL CDbPropertyPage::Show ()
{
	return TRUE;
}

BOOL CDbPropertyPage::StepBack ()
{
	return FALSE;
}

void CDbPropertyPage::NextRecord ()
{
}

void CDbPropertyPage::PriorRecord ()
{
}

void CDbPropertyPage::FirstRecord ()
{
}

void CDbPropertyPage::LastRecord ()
{
}

void CDbPropertyPage::OnCopy ()
{
}

void CDbPropertyPage::OnPaste ()
{
}

void CDbPropertyPage::OnMarkAll ()
{
}

void CDbPropertyPage::OnUnMarkAll ()
{
}

DROPEFFECT CDbPropertyPage::OnDrop (CWnd* pWnd,  COleDataObject* pDataObject,
					              	  DROPEFFECT dropDefault, DROPEFFECT dropList,
									  CPoint point)
{
	return DROPEFFECT_NONE;
}

void CDbPropertyPage::Save ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	File.Open (Path.GetBuffer (), CFile::modeCreate | CFile::modeWrite);
	CArchive archive(&File, CArchive::store);

	try
	{
			archive.Write (&DlgBkColor, sizeof (DlgBkColor));
			archive.Write (&ListBkColor, sizeof (ListBkColor));
			archive.Write (&FlatLayout, sizeof (FlatLayout));
			archive.Write (&HideOK, sizeof (HideOK));
	}
	catch (...) 
	{
		return;
	}


	archive.Close ();
	File.Close ();
}

void CDbPropertyPage::Load ()
{
    if (ArchiveName == "") return;
	CFile File;
	CString Path;
	CString Etc;

	if (Etc.GetEnvironmentVariable (_T("BWSETC")))
	{
		Path.Format (_T("%s\\%s"), Etc, ArchiveName);
	}
	else
	{
		Path = ArchiveName;
	}
	File.Open (Path.GetBuffer (), CFile::modeRead);
	CArchive archive(&File, CArchive::load);

	try
	{
			archive.Read (&DlgBkColor, sizeof (DlgBkColor));
			archive.Read (&ListBkColor, sizeof (ListBkColor));
			archive.Read (&FlatLayout, sizeof (FlatLayout));
			archive.Read (&HideOK, sizeof (HideOK));
	}
	catch (...) 
	{
		return;
	}


	archive.Close ();
	File.Close ();
}


void CDbPropertyPage::SetDefault ()
{
	if (ArchiveName == "") return;
}

void CDbPropertyPage::UpdatePage ()
{
}

void CDbPropertyPage::GetPage ()
{
}
