// ATexteDoc.cpp : Implementierung der Klasse CATexteDoc
//

#include "stdafx.h"
#include "ATexte.h"

#include "ATexteDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CATexteDoc

IMPLEMENT_DYNCREATE(CATexteDoc, CDocument)

BEGIN_MESSAGE_MAP(CATexteDoc, CDocument)
END_MESSAGE_MAP()


// CATexteDoc Erstellung/Zerst�rung

CATexteDoc::CATexteDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CATexteDoc::~CATexteDoc()
{
}

BOOL CATexteDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CATexteDoc Serialisierung

void CATexteDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CATexteDoc Diagnose

#ifdef _DEBUG
void CATexteDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CATexteDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CATexteDoc-Befehle
