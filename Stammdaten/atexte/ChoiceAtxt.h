#ifndef _CHOICEREZ_DEF
#define _CHOICEREZ_DEF

#include "ChoiceX.h"
#include "AtextList.h"
#include <vector>

class CChoiceAtxt : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
      
    public :
	    std::vector<CATextList *> AtxtList;
      	CChoiceAtxt(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceAtxt(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchSys (CListCtrl *,  LPTSTR);
        void SearchTxtNr (CListCtrl *,  LPTSTR);
        void SearchDispTxt (CListCtrl *, LPTSTR);
        void SearchTxt (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CATextList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		void SetDefault ();
};
#endif
