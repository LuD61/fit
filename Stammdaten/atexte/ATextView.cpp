// ATextView.cpp : Implementierungsdatei
//

#include "stdafx.h"
//#include "ATexteCtrl.h"
#include "ATextView.h"
#include "UniFormField.h"
#include "atextview.h"
#include "DbUniCode.h"
#include "StrToken.h"
#include "MToken.h"
#include "atextview.h"
#include "atextview.h"
#include "syscopy.h"
#include "CopyDialog.h"
#include "atextview.h"
#include "ChoiceA.h"
#include "JoinArticle.h"
#include "SendToSystem.h"
#include "atextview.h"
#include "StrFuncs.h"

// CATextView

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CATextView, DbFormView)

CATextView::CATextView()
	: DbFormView(CATextView::IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	SysChoice = NULL;
	ModalChoiceSys = TRUE;

	SgTab[0] = &Eti_typ.eti_typ.sg01;
	SgTab[1] = &Eti_typ.eti_typ.sg02;
	SgTab[2] = &Eti_typ.eti_typ.sg03;
	SgTab[3] = &Eti_typ.eti_typ.sg04;
	SgTab[4] = &Eti_typ.eti_typ.sg05;
	SgTab[5] = &Eti_typ.eti_typ.sg06;
	SgTab[6] = &Eti_typ.eti_typ.sg07;
	SgTab[7] = &Eti_typ.eti_typ.sg08;
	SgTab[8] = &Eti_typ.eti_typ.sg09;
	SgTab[9] = &Eti_typ.eti_typ.sg10;
	SgTab[10] = &Eti_typ.eti_typ.sg11;
	SgTab[11] = &Eti_typ.eti_typ.sg12;
	SgTab[12] = &Eti_typ.eti_typ.sg13;
	SgTab[13] = &Eti_typ.eti_typ.sg14;
	SgTab[14] = &Eti_typ.eti_typ.sg15;
	SgTab[15] = &Eti_typ.eti_typ.sg16;
	SgTab[16] = &Eti_typ.eti_typ.sg17;
	SgTab[17] = &Eti_typ.eti_typ.sg18;
	SgTab[18] = &Eti_typ.eti_typ.sg19;
	SgTab[19] = &Eti_typ.eti_typ.sg20;

	ZeTab[0] = &Eti_typ.eti_typ.ze01;
	ZeTab[1] = &Eti_typ.eti_typ.ze02;
	ZeTab[2] = &Eti_typ.eti_typ.ze03;
	ZeTab[3] = &Eti_typ.eti_typ.ze04;
	ZeTab[4] = &Eti_typ.eti_typ.ze05;
	ZeTab[5] = &Eti_typ.eti_typ.ze06;
	ZeTab[6] = &Eti_typ.eti_typ.ze07;
	ZeTab[7] = &Eti_typ.eti_typ.ze08;
	ZeTab[8] = &Eti_typ.eti_typ.ze09;
	ZeTab[9] = &Eti_typ.eti_typ.ze10;
	ZeTab[10] = &Eti_typ.eti_typ.ze11;
	ZeTab[11] = &Eti_typ.eti_typ.ze12;
	ZeTab[12] = &Eti_typ.eti_typ.ze13;
	ZeTab[13] = &Eti_typ.eti_typ.ze14;
	ZeTab[14] = &Eti_typ.eti_typ.ze15;
	ZeTab[15] = &Eti_typ.eti_typ.ze16;
	ZeTab[16] = &Eti_typ.eti_typ.ze17;
	ZeTab[17] = &Eti_typ.eti_typ.ze18;
	ZeTab[18] = &Eti_typ.eti_typ.ze19;
	ZeTab[19] = &Eti_typ.eti_typ.ze20;

//	HeadControls.Add (&m_Sys);
//	HeadControls.Add (&m_SysChoice);
	HeadControls.Add (&m_TxtNr);
	DlgBkColor = NULL;
	DlgBrush = NULL;
	ListBkColor = NULL;
	ArchiveName = _T("atexte.prp");
	Load ();
}

CATextView::~CATextView()
{
/*
	if (SysChoice != NULL)
	{
		delete SysChoice;
	}
*/

	Save ();
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();


	Keys.FirstPosition ();
	while ((f = (CFormField *) Keys.GetNext ()) != NULL)
	{
		delete f;
	}
	Keys.Init ();


	TextForm.FirstPosition ();
	while ((f = (CFormField *) TextForm.GetNext ()) != NULL)
	{
		delete f;
	}
	TextForm.Init ();

	Sys_peri.dbclose ();
	ATexte.dbclose ();
	Fil.dbclose ();
	Adr.dbclose ();
	Eti_typ.dbclose ();
	if (Choice != NULL)
	{
		delete Choice;
	}
	if (SysChoice != NULL)
	{
		delete SysChoice;
	}
}

void CATextView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LSYS, m_LSys);
	DDX_Control(pDX, IDC_SYS, m_Sys);
	DDX_Control(pDX, IDC_LTXT_NR, m_LTxtNr);
	DDX_Control(pDX, IDC_TXT_NT, m_TxtNr);
	DDX_Control(pDX, IDC_TXT, m_Txt);
	DDX_Control(pDX, IDC_LSG, m_LSg);
	DDX_Control(pDX, IDC_SG, m_Sg);
	DDX_Control(pDX, IDC_ADR_NAM1, m_AdrNam1);
	DDX_Control(pDX, IDC_SENDCTRL, m_SendCtrl);
	DDX_Control(pDX, IDC_LTXTTYP, m_LTxtType);
	DDX_Control(pDX, IDC_TXTTYP, m_TxtType);
	DDX_Control(pDX, IDC_LTEXTIDX, m_LTxtIdx);
	DDX_Control(pDX, IDC_TEXTIDX, m_TxtIdx);
	DDX_Control(pDX, IDC_ZxzIdxSpin, m_TxtIdxSpin);
}

BEGIN_MESSAGE_MAP(CATextView, DbFormView)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_COMMAND(ID_BACK, OnBack)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND (IDC_SYSCHOICE, OnSysChoice)
	ON_COMMAND (IDC_TXTCHOICE, OnChoice)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_COMMAND (SELECTED, OnSelected)
	ON_COMMAND (CANCELED, OnCanceled)
	ON_CBN_SELCHANGE(IDC_SG, OnCbnSelchangeSg)
	ON_COMMAND(ID_TEXT_CENT, OnTextCent)
	ON_COMMAND(ID_TEXT_LEFT, OnTextLeft)
	ON_COMMAND(ID_TEXT_RIGHT, OnTextRight)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_UPDATE_COMMAND_UI(ID_DELETE, OnUpdateDelete)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_TEXT_CENT, OnUpdateTextCent)
	ON_UPDATE_COMMAND_UI(ID_TEXT_LEFT, OnUpdateTextLeft)
	ON_UPDATE_COMMAND_UI(ID_TEXT_RIGHT, OnUpdateTextRight)
	ON_COMMAND(ID_SFETT, OnFett)
	ON_UPDATE_COMMAND_UI(ID_SFETT, OnUpdateFett)
	ON_COMMAND(ID_SKURSIV, OnKursiv)
	ON_UPDATE_COMMAND_UI(ID_SKURSIV, OnUpdateKursiv)
	ON_COMMAND(ID_SUNDERLINE, OnUnderline)
	ON_UPDATE_COMMAND_UI(ID_SUNDERLINE, OnUpdateUnderline)


	ON_EN_CHANGE(IDC_TXT, OnEnChangeTxt)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_COMMAND(ID_COPY_SYS, OnCopySys)
	ON_UPDATE_COMMAND_UI(ID_COPY_SYS, OnUpdateCopySys)
	ON_COMMAND(ID_JOINARTICLE, OnJoinarticle)
	ON_UPDATE_COMMAND_UI(ID_JOINARTICLE, OnUpdateJoinarticle)
	ON_COMMAND(ID_SEND, OnSend)
	ON_COMMAND(ID_FIRST, OnFirst)
	ON_UPDATE_COMMAND_UI(ID_FIRST, OnUpdateFirst)
	ON_COMMAND(ID_PRIOR, OnPrior)
	ON_UPDATE_COMMAND_UI(ID_PRIOR, OnUpdatePrior)
	ON_COMMAND(ID_NEXT, OnNext)
	ON_UPDATE_COMMAND_UI(ID_NEXT, OnUpdateNext)
	ON_COMMAND(ID_LAST, OnLast)
	ON_UPDATE_COMMAND_UI(ID_LAST, OnUpdateLast)
	ON_COMMAND(ID_DLG_BACKGROUND, OnDlgBackground)
	ON_COMMAND(ID_CHOICEBACKGROUND, OnChoicebackground)
END_MESSAGE_MAP()


// CATextView-Diagnose

#ifdef _DEBUG
void CATextView::AssertValid() const
{
	CFormView::AssertValid();
}

void CATextView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG


// CATextView-Meldungshandler

void CATextView::OnInitialUpdate ()
{
	DbFormView::OnInitialUpdate ();

	ATexte.opendbase (_T("bws"));

	m_Txt.SetEventMask (m_Txt.GetEventMask () | ENM_CHANGE);


//    Form.Add (new CUniFormField (&m_KunName,EDIT,   (char *)   &KunAdr.adr.adr_krz, VCHAR));

	memcpy (&Sys_peri.sys_peri, &sys_peri_null, sizeof (SYS_PERI));
	memcpy (&ATexte.atexte, &atexte_null, sizeof (ATEXTE));
	memcpy (&Adr.adr, &adr_null, sizeof (ADR));
	sg = 0;

	Keys.Add (new CFormField (&m_TxtNr,EDIT, (long *)  &ATexte.atexte.txt_nr, VLONG));

    Form.Add (new CFormField (&m_Sys,EDIT,   (long *)  &Sys_peri.sys_peri.sys, VLONG));
    Form.Add (new CUniFormField (&m_AdrNam1,EDIT,   (char *)  &Adr.adr.adr_nam1, VCHAR));
	Form.Add (new CFormField (&m_TxtNr,EDIT, (long *)  &ATexte.atexte.txt_nr, VLONG));
	Form.Add (new CUniFormField (&m_SendCtrl, CHECKBOX  ,         (char *) &ATexte.atexte.send_ctrl, VSHORT));
	Form.Add (new CUniFormField (&m_Txt, RICHTEXTEDIT  , (char *) &ATexte.atexte.txt, VCHAR));
	Form.Add (new CFormField (&m_Sg,COMBOBOX,   (short *) &sg, VSHORT));
	Form.Add (new CFormField (&m_TxtType,COMBOBOX,   (short *) &ATexte.atexte.txt_typ, VSHORT));
	Form.Add (new CFormField (&m_TxtIdx,EDIT,   (short *) &ATexte.atexte.txt_platz, VSHORT));
	FillTxtType ();

	TextForm.Add (new CUniFormField (&m_SendCtrl, CHECKBOX  ,         (char *) &ATexte.atexte.send_ctrl, VSHORT));
	TextForm.Add (new CUniFormField (&m_Txt,      RICHTEXTEDIT  ,     (char *) &ATexte.atexte.txt, VCHAR));

	SysGrid.Create (this, 1, 2);
    SysGrid.SetBorder (0, 0);
    SysGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Sys = new CCtrlInfo (&m_Sys, 0, 0, 1, 1);
	SysGrid.Add (c_Sys);
	CtrlGrid.CreateChoiceButton (m_SysChoice, IDC_SYSCHOICE, this);
	CCtrlInfo *c_SysChoice = new CCtrlInfo (&m_SysChoice, 1, 0, 1, 1);
	SysGrid.Add (c_SysChoice);

	TxtGrid.Create (this, 1, 2);
    TxtGrid.SetBorder (0, 0);
    TxtGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_TxtNr = new CCtrlInfo (&m_TxtNr, 0, 0, 1, 1);
	TxtGrid.Add (c_TxtNr);
	CtrlGrid.CreateChoiceButton (m_TxtChoice, IDC_TXTCHOICE, this);
	CCtrlInfo *c_TxtChoice = new CCtrlInfo (&m_TxtChoice, 1, 0, 1, 1);
	TxtGrid.Add (c_TxtChoice);

	SgGrid.Create (this, 1, 2);
    SgGrid.SetBorder (0, 0);
    SgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_LSg = new CCtrlInfo (&m_LSg, 0, 0, 1, 1);
	SgGrid.Add (c_LSg);
	CCtrlInfo *c_Sg = new CCtrlInfo (&m_Sg, 1, 0, 1, 1);
	SgGrid.Add (c_Sg);

	EditGrid.Create (this, 2, 1);
    EditGrid.SetBorder (0, 0);
    EditGrid.SetGridSpace (0, 10);
	m_EditLine.Create (this, 0, 0, 20, 15, IDC_EDITLINE);
	CCtrlInfo *c_EditLine0 = new CCtrlInfo (&m_EditLine, 0, 0, 1, 1);
	EditGrid.Add (c_EditLine0);
	CCtrlInfo *c_Txt0 = new CCtrlInfo (&m_Txt, 0, 1, 1, 1);
	EditGrid.Add (c_Txt0);

	IdxGrid.Create (this, 1, 2);
    IdxGrid.SetBorder (0, 0);
    IdxGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_TxtIdx = new CCtrlInfo (&m_TxtIdx, 4, 2, 1, 1);
	IdxGrid.Add (c_TxtIdx);
	CCtrlInfo *c_TxtIdxSpin = new CCtrlInfo (&m_TxtIdxSpin, 5, 2, 1, 1);
	IdxGrid.Add (c_TxtIdxSpin);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (8, 2);

	CCtrlInfo *c_LTxtNr = new CCtrlInfo (&m_LTxtNr, 1, 0, 1, 1);
	CtrlGrid.Add (c_LTxtNr);

	CCtrlInfo *c_TxtGrid = new CCtrlInfo (&TxtGrid, 2, 0, 2, 1);
	CtrlGrid.Add (c_TxtGrid);

	CCtrlInfo *c_LSys = new CCtrlInfo (&m_LSys, 1, 1, 1, 1);
	CtrlGrid.Add (c_LSys);

	CCtrlInfo *c_SysGrid = new CCtrlInfo (&SysGrid, 2, 1, 2, 1);
	CtrlGrid.Add (c_SysGrid);

	CCtrlInfo *c_AdrNam1 = new CCtrlInfo (&m_AdrNam1, 4, 1, 1, 1);
	CtrlGrid.Add (c_AdrNam1);

	CCtrlInfo *c_LTxtType = new CCtrlInfo (&m_LTxtType, 1, 2, 1, 1);
	CtrlGrid.Add (c_LTxtType);

	CCtrlInfo *c_TxtType = new CCtrlInfo (&m_TxtType, 2, 2, 1, 1);
	CtrlGrid.Add (c_TxtType);

	CCtrlInfo *c_SendCtrl = new CCtrlInfo (&m_SendCtrl, 2, 3, 1, 1);
	c_SendCtrl->SetCellPos (0, 5);
	CtrlGrid.Add (c_SendCtrl);

	CCtrlInfo *c_LTxtIdx = new CCtrlInfo (&m_LTxtIdx, 3, 2, 1, 1);
	CtrlGrid.Add (c_LTxtIdx);

	CCtrlInfo *TxtIdx = new CCtrlInfo (&IdxGrid, 4, 2, 1, 1);
	CtrlGrid.Add (TxtIdx);
	m_TxtIdxSpin.SetRange (1, 10); 
	m_TxtIdxSpin.SetPos (1); 

	CCtrlInfo *c_SgGrid = new CCtrlInfo (&SgGrid, DOCKRIGHT, 3, 2, 1);
	CtrlGrid.Add (c_SgGrid);

	CCtrlInfo *c_EditLine = new CCtrlInfo (&m_EditLine, 0, 4, DOCKRIGHT, 1);
    c_EditLine->rightspace = 20; 
	c_EditLine->SetCellPos (0, 10);
	CtrlGrid.Add (c_EditLine);

	CCtrlInfo *c_Txt = new CCtrlInfo (&m_Txt, 0, 5, DOCKRIGHT, DOCKBOTTOM);
    c_Txt->rightspace = 20; 
	c_Txt->SetCellPos (0, 0);
	CtrlGrid.Add (c_Txt);

	FillSgCombo ();

	Form.Show ();
	m_Sys.SetFocus ();
	EnableFields (FALSE);
	Form.Get ();
	m_Txt.SetDefaultSize (sg);
	CString Message = _T("");
//	m_Txt.FromClipboard (Message); 
/*
	CStrFuncs::FromClipboard (Message, CF_TEXT);
	CToken t (Message, _T("="));
	if (t.GetAnzToken () > 1)
	{
		CString c = t.GetToken (0);
		if (c == _T("TextNr"))
		{
			CStrFuncs::ToClipboard (CString (""), CF_TEXT);
			ATexte.atexte.txt_nr = _tstol (t.GetToken (1));
			Form.Show ();
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		}
	}
*/
	ATexte.atexte.txt_nr = GetTextNrFromCommandline ();
	if (ATexte.atexte.txt_nr != 0l)
	{
		Form.Show ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
	}
}

HBRUSH CATextView::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CATextView::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (GetFocus () == NULL)
				{
					m_TxtNr.SetFocus ();
				}
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CRichEditCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CRichEditCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CRichEditCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				OnBack ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
//				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnChoice ();
				}
			}

			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Sys)
				{
					OnSysChoice ();
					return TRUE;
				}
				else if (GetFocus () == &m_TxtNr)
				{
					OnChoice ();
					return TRUE;
				}
			}
/*
			if (pMsg->wParam == 'C')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopy ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'V')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPaste ();
					   return TRUE;
				}
			}
*/

/*
			if (pMsg->wParam == 'Z')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnCopyEx ();
					   return TRUE;
				}
			}

			if (pMsg->wParam == 'L')
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					   OnPasteEx ();
					   return TRUE;
				}
			}
*/
	}
    return CFormView::PreTranslateMessage(pMsg);
}

void CATextView::OnSize(UINT nType, int cx, int cy)
{
	DbFormView::OnSize (nType, cx, cy);
	if (m_LSys.m_hWnd != NULL)
	{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
	}
}

void CATextView::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_TxtNr.IsWindowEnabled ())
	{
	    GetParent ()->DestroyWindow ();
	}
	else
	{
		EnableFields (FALSE);
		m_TxtNr.SetFocus ();
	}
}

void CATextView::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	write ();
}

void CATextView::OnChoice ()
{
    CString Text;
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		Choice->SetListFocus ();
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceAtxt (this);
	    Choice->IsModal = ModalChoice;
		Choice->DlgBkColor = ListBkColor;
//		Choice->Where = Where;
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->HideOK    = HideOK;
		Choice->CreateDlg ();
	}

    Choice->SetDbClass (&Sys_peri);
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{

		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
/*
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
*/

		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

		Choice->MoveWindow (&rect);
		Choice->SetListFocus ();
		return;
	}
    if (Choice->GetState ())
    {
		  CATextList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
		  Sys_peri.sys_peri.sys = abl->sys;
		  ATexte.atexte.sys = abl->sys;
		  ATexte.atexte.txt_nr = abl->txt_nr;
		  Form.Show ();
		  EnableFields (FALSE);
		  m_TxtNr.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}


void CATextView::OnSelected ()
{
	if (Choice == NULL) return;
    CATextList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
	ATexte.atexte.txt_nr = abl->txt_nr;
    Keys.Show ();
	Read ();

	if (Choice->FocusBack)
	{
		Choice->SetListFocus ();
		m_Txt.EmptyUndoBuffer ();
	}

}

void CATextView::OnCanceled ()
{
	Choice->ShowWindow (SW_HIDE);
	if (m_Txt.IsWindowEnabled ())
	{
		m_Txt.SetFocus ();
	}
	else
	{
		m_TxtNr.SetFocus ();
	}
}


void CATextView::OnSysChoice ()
{
    CString Text;
	if (SysChoice != NULL && !ModalChoiceSys)
	{
		SysChoice->ShowWindow (SW_SHOWNORMAL);
		SysChoice->SetListFocus ();
		return;
	}
	if (SysChoice == NULL)
	{
		SysChoice = new CChoiceSys (this);
	    SysChoice->IsModal = ModalChoiceSys;
		SysChoice->DlgBkColor = ListBkColor;
		SysChoice->IdArrDown = IDI_HARROWDOWN;
		SysChoice->IdArrUp   = IDI_HARROWUP;
		SysChoice->IdArrNo   = IDI_HARROWNO;
		SysChoice->HideOK       = FALSE;
		SysChoice->CreateDlg ();
	}

    SysChoice->SetDbClass (&Sys_peri);
	if (ModalChoiceSys)
	{
			SysChoice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		SysChoice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
		SysChoice->MoveWindow (&rect);
		SysChoice->SetListFocus ();
		return;
	}
    if (SysChoice->GetState ())
    {
		  CSysList *abl = SysChoice->GetSelectedText (); 
		  if (abl == NULL) return;
		  memcpy (&Sys_peri.sys_peri, &sys_peri_null, sizeof (SYS_PERI)); 
		  memcpy (&Fil.fil, &fil_null, sizeof (FIL)); 
		  memcpy (&Adr.adr, &adr_null, sizeof (ADR)); 
		  Sys_peri.sys_peri.sys = abl->sys; 
		  int dsqlstatus = Sys_peri.dbreadfirst ();
          if (dsqlstatus == 0)
		  {
			  Fil.fil.mdn = Sys_peri.sys_peri.mdn;
			  Fil.fil.fil = Sys_peri.sys_peri.fil;
			  dsqlstatus = Fil.dbreadfirst ();
		  }
          if (dsqlstatus == 0)
		  {
			  Adr.adr.adr = Fil.fil.adr;
			  dsqlstatus = Adr.dbreadfirst ();
		  }
		  Form.Show ();
		  m_Sys.SetFocus ();
    }
}

void CATextView::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CWnd *control = GetFocus ();
	if (control->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) control)->Copy (); 
	}
	else if (control->IsKindOf (RUNTIME_CLASS (CRichTextEdit)))
	{
		((CRichTextEdit *) control)->Copy (); 
	}
}

void CATextView::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CWnd *control = GetFocus ();
	if (control->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) control)->Paste (); 
	}
	else if (control->IsKindOf (RUNTIME_CLASS (CRichTextEdit)))
	{
		((CRichTextEdit *) control)->Paste (); 
	}
}

BOOL CATextView::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Sys)
	{
		if (!ReadSys ())
		{
			m_Sys.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_TxtNr)
	{
		if (!Read ())
		{
			m_TxtNr.SetFocus ();
			return FALSE;
		}

	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CATextView::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

BOOL CATextView::ReadSys ()
{
	memcpy (&Sys_peri.sys_peri, &sys_peri_null, sizeof (SYS_PERI));
	memcpy (&Adr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Sys_peri.sys_peri.sys == 0l)
	{
		wcscpy_s (Adr.adr.adr_krz, _T(""));
		Form.Show ();
		return TRUE;
	}
	int dsqlstatus = Sys_peri.dbreadfirst ();
	if (dsqlstatus != 0)
	{
		MessageBox (_T("Ger�tenummer nicht gefunden"), NULL, MB_OK | MB_ICONERROR);
		return FALSE;
	}
    Fil.fil.mdn = Sys_peri.sys_peri.mdn;
    Fil.fil.fil = Sys_peri.sys_peri.fil;
    dsqlstatus = Fil.dbreadfirst ();
    if (dsqlstatus == 0)
    {
	  Adr.adr.adr = Fil.fil.adr;
	  dsqlstatus = Adr.dbreadfirst ();
    }
	Form.Show ();
	return TRUE;
}

BOOL CATextView::Read ()
{
	m_Txt.LockWindowUpdate ();
	memcpy (&ATexte.atexte, &atexte_null, sizeof (ATEXTE));
	ATexte.atexte.alignment = m_Txt.Left;
	Keys.Get ();
	ATexte.atexte.sys = Sys_peri.sys_peri.sys;
	ATexte.atexte.txt_platz = 1;
	int dsqlstatus = ATexte.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		CStrToken t;
		t.SetSep (_T("@0A"));
		LPSTR pos = (LPSTR) ATexte.atexte.txt;
		CDbUniCode::DbToUniCode (ATexte.atexte.txt, pos);
		t = ATexte.atexte.txt;
		int count = t.GetAnzToken ();
		CString Txt = _T("");
		for (int i = 0; i < count; i ++)
		{
			if (i > 0)
			{
				Txt += _T("\n");
			}
			Txt += t.GetToken (i);
		}
		LPSTR p = (LPSTR) ATexte.atexte.txt;
		CDbUniCode::DbFromUniCode (Txt.GetBuffer (), p, sizeof (ATexte.atexte.txt) / 2);
		if (ATexte.atexte.txt_platz <= 0) ATexte.atexte.txt_platz = 1;
		m_TxtIdxSpin.SetPos (ATexte.atexte.txt_platz); 
	}

//	CtrlGrid.Display ();

	Form.GetFormField (&m_TxtType)->Show ();
	TextForm.Show ();
	EnableFields (TRUE);
	m_Txt.SetAlignment (ATexte.atexte.alignment);
	if (dsqlstatus != 0)
	{
		sg = 9;
		Form.GetFormField (&m_Sg)->Show ();
	}
	Form.Get ();
	m_Txt.UnlockWindowUpdate ();
	return TRUE;
}

void CATextView::write ()
{
	if (m_TxtNr.IsWindowEnabled ()) return;
	Form.Get ();
	ATexte.atexte.sys = Sys_peri.sys_peri.sys;
	if (ATexte.atexte.txt_typ <= 0) ATexte.atexte.txt_typ = 1;
	CToken t;
	t.SetSep (_T("\n"));
	LPSTR pos = (LPSTR) ATexte.atexte.txt;
	CDbUniCode::DbToUniCode (ATexte.atexte.txt, pos);
	t = ATexte.atexte.txt;
	int count = t.GetAnzToken ();
	CString Txt = _T("");
	for (int i = 0; i < count; i ++)
	{
		if (i > 0)
		{
			Txt += _T("@0A");
		}
		Txt += t.GetToken (i);
	}
	LPSTR p = (LPSTR) ATexte.atexte.txt; 
	CDbUniCode::DbFromUniCode (Txt.GetBuffer (), p, sizeof (ATexte.atexte.txt) / 2);
/*
	t = m_Txt.DisplayText;
	count = t.GetAnzToken ();
	Txt = _T("");
	for (int i = 0; i < count; i ++)
	{
		if (i > 0)
		{
			Txt += _T("@0A");
		}
		Txt += t.GetToken (i);
	}
*/

	Txt = m_Txt.DisplayText;
	p = (LPSTR) ATexte.atexte.disp_txt;
	CDbUniCode::DbFromUniCode (Txt.GetBuffer (), p, sizeof (ATexte.atexte.disp_txt) / 2);

	ATexte.atexte.alignment = m_Txt.Alignment;

    ATexte.dbupdate ();
	EnableFields (FALSE);
	if (Choice != NULL)
	{
		Choice->FillList ();
	}
	m_TxtNr.SetFocus ();
}

void CATextView::OnDelete ()
{
	if (AfxMessageBox (_T("Rezeptur l�schen ?"), MB_YESNO | MB_ICONQUESTION) != IDYES)
	{
		return;
	}
	Form.Get ();
	ATexte.dbdelete ();
	if (Choice != NULL)
	{
		Choice->FillList ();
	}
}

void CATextView::FillSgCombo ()
{
	Eti_typ.eti_typ.eti = 99;
	if (Eti_typ.dbreadfirst () != 0) return;
	CFormField *f_Sg = Form.GetFormField (&m_Sg);
	int idx = 0;
	if (f_Sg != NULL)
	{
		f_Sg->DestroyComboBox ();
		for (int i = 1; i < 20; i ++)
		{
			  if (*SgTab[i] == 0) continue;
              CString *Sg = new CString ();
			  Sg->Format (_T("%hd/%hd"), *SgTab[i], *ZeTab[i]);
		      f_Sg->ComboValues.push_back (Sg);
			  if (*SgTab[i] == Eti_typ.eti_typ.sg_std)
			  {
				  sg = Eti_typ.eti_typ.sg_std;
				  idx = i;
			  }
		}
		f_Sg->FillComboBox ();
		m_Sg.SetCurSel (idx);
	}
}

void CATextView::EnableFields (BOOL b)
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);
//	if (ModalChoice)
	{
//		m_AKunGxChoice.EnableWindow (!b);
	}
}

void CATextView::EnableHeadControls (BOOL enable)
{
	HeadControls.FirstPosition ();
	CWnd *Control;
	while ((Control = (CWnd *) HeadControls.GetNext ()) != NULL)
	{
        Control->EnableWindow (enable);
	}

	GetDocument ()->UpdateAllViews (this);
	m_Txt.EmptyUndoBuffer ();
}


void CATextView::OnCbnSelchangeSg()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	m_Txt.SetSelectionCharSize (sg);
	m_Txt.SetFocus ();
}

void CATextView::OnTextCent()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_Txt.OnTextCent ();
}

void CATextView::OnTextLeft()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_Txt.OnTextLeft ();
}

void CATextView::OnTextRight()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_Txt.OnTextRight ();
}

void CATextView::OnUpdateDelete(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.

	if (m_TxtNr.IsWindowEnabled ())
	{
		pCmdUI->Enable (FALSE);
	}
	else
	{
		pCmdUI->Enable ();
	}
}

void CATextView::OnUpdateFileSave(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	if (m_TxtNr.IsWindowEnabled ())
	{
		pCmdUI->Enable (FALSE);
	}
	else
	{
		pCmdUI->Enable ();
	}
}

void CATextView::OnUpdateTextCent(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	if (m_TxtNr.IsWindowEnabled ())
	{
		pCmdUI->Enable (FALSE);
	}
	else
	{
		pCmdUI->Enable ();
	}
}

void CATextView::OnUpdateTextLeft(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	if (m_TxtNr.IsWindowEnabled ())
	{
		pCmdUI->Enable (FALSE);
	}
	else
	{
		pCmdUI->Enable ();
	}
}

void CATextView::OnUpdateTextRight(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	if (m_TxtNr.IsWindowEnabled ())
	{
		pCmdUI->Enable (FALSE);
	}
	else
	{
		pCmdUI->Enable ();
	}
}

void CATextView::OnEnChangeTxt()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den DbFormView::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.

	CHARRANGE cr;
	m_Txt.GetSel (cr);

	int row = 0;
	int column = 0;

	m_Txt.GetLinePos (cr.cpMin, row, column);
	if (row != 1 || column != 0) return;

	CString Text;
	m_Txt.GetText (Text);
	if (Text.Right (1) == _T("\n"))
	{
		Text.TrimRight ();
		Text += _T("\n");
	}
	CMToken t;
	t.SetSep (_T("\n"));
	t = Text;
	int count = t.GetAnzToken ();
	if (count == 2)
	{
		CString Line2 = t.GetToken (1);
		Line2.TrimRight ();
		if (Line2.GetLength () == 0)
		{
			sg = 7;
			Form.GetFormField (&m_Sg)->Show ();
			PostMessage (WM_COMMAND, MAKEWPARAM (IDC_SG, CBN_SELCHANGE), (LPARAM) m_Sg.m_hWnd);
		}
	}
}

void CATextView::OnEditUndo()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (m_Txt.CanUndo ())
	{
		m_Txt.Undo ();
	}
/*
	else if (m_Txt.CanRedo ())
	{
		m_Txt.Redo ();
	}
*/
}

void CATextView::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	if (m_TxtNr.IsWindowEnabled ())
	{
		pCmdUI->Enable (FALSE);
	}
	else
	{
		pCmdUI->Enable ();
	}
}

void CATextView::OnCopySys()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

    CString Text;
    CChoiceSys SysChoice;

	SysChoice.IsModal = TRUE;
	SysChoice.SingleSelection = FALSE;
	SysChoice.CreateDlg ();

    SysChoice.SetDbClass (&Sys_peri);
	SysChoice.DoModal();
    if (SysChoice.GetState ())
    {
		  write ();
		  CCopyDialog dlg;
		  INT_PTR ret = dlg.DoModal ();
		  if (ret == IDOK)
		  {
			CVector (SysTab);
			for (std::vector<CSysList *>::iterator pabl = SysChoice.SelectList.begin (); pabl != SysChoice.SelectList.end (); ++pabl)
			{
				CSysList *abl = *pabl;
				size_t vSys = abl->sys;
				SysTab.Add ((void *) vSys);
			}
			CSysCopy SysCopy;
			SysCopy.CopyAll = dlg.CopyAll;
			SysCopy.Copy (ATexte, SysTab);
			ATexte.atexte.sys = Sys_peri.sys_peri.sys;
			if (Choice != NULL)
			{
				Choice->FillList ();
			}
		  }
    }
}

void CATextView::OnUpdateCopySys(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	if (m_TxtNr.IsWindowEnabled ())
	{
		pCmdUI->Enable (FALSE);
	}
	else
	{
		pCmdUI->Enable ();
	}
}

void CATextView::OnJoinarticle()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CString Text;
    CChoiceA AChoice;

	Form.Get ();
	if (ATexte.atexte.txt_nr == 0l) return;
	AChoice.IsModal = TRUE;
	AChoice.SingleSelection = FALSE;
	AChoice.CreateDlg ();

    AChoice.SetDbClass (&Sys_peri);
	AChoice.DoModal();
    if (AChoice.GetState ())
    {
			CVector ABasTab;
			for (std::vector<CABasList *>::iterator pabl = AChoice.SelectList.begin (); pabl != AChoice.SelectList.end (); ++pabl)
			{
				CABasList *abl = *pabl;
				ABasTab.Add (abl);
			}
			CJoinArticle JoinArticle (ATexte.atexte.txt_nr, ATexte.atexte.txt_platz);
			JoinArticle.Join (ABasTab);
			CString Message;
			Message.Format (_T("Text-Nr %ld wurde im %hd. Textfeld\nder selektierten Artikel zugeordnet"),
				ATexte.atexte.txt_nr, ATexte.atexte.txt_platz);
			MessageBox (Message.GetBuffer ());
    }
}

void CATextView::OnUpdateJoinarticle(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	if (m_TxtNr.IsWindowEnabled ())
	{
		pCmdUI->Enable (FALSE);
	}
	else
	{
		pCmdUI->Enable ();
	}
}

void CATextView::OnSend()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CSendToSystem dlg;
	INT_PTR ret = dlg.DoModal ();
}


void CATextView::FillTxtType ()
{
	CFormField *f = Form.GetFormField (&m_TxtType);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"text_typ\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
	}
}

void CATextView::OnFirst()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (Choice == NULL) return;

	Choice->FirstSelection ();
	OnSelected ();
	if (!Choice->IsWindowVisible ())
	{
		m_Txt.SetFocus ();
	}
}

void CATextView::OnUpdateFirst(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   if (Choice == NULL)
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CATextView::OnPrior()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (Choice == NULL) return;

	Choice->DecSelection ();
	OnSelected ();
	if (!Choice->IsWindowVisible ())
	{
		m_Txt.SetFocus ();
	}
}

void CATextView::OnUpdatePrior(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   if (Choice == NULL)
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CATextView::OnNext()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (Choice == NULL) return;

	Choice->IncSelection ();
	OnSelected ();
	if (!Choice->IsWindowVisible ())
	{
		m_Txt.SetFocus ();
	}
}

void CATextView::OnUpdateNext(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   if (Choice == NULL)
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CATextView::OnLast()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (Choice == NULL) return;

	Choice->LastSelection ();
	OnSelected ();
	if (!Choice->IsWindowVisible ())
	{
		m_Txt.SetFocus ();
	}
}

void CATextView::OnUpdateLast(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   if (Choice == NULL)
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CATextView::OnDlgBackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CColorDialog cdlg;
	cdlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	cdlg.m_cc.rgbResult = Color;
	if (cdlg.DoModal() == IDOK)
	{
		DlgBkColor = cdlg.GetColor();
	    DeleteObject (DlgBrush);
	    DlgBrush = NULL;
		InvalidateRect (NULL);
	}
}

void CATextView::OnChoicebackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CColorDialog dlg;
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	dlg.m_cc.rgbResult = Color;
	if (dlg.DoModal() == IDOK)
	{
		ListBkColor = dlg.GetColor();
		if (Choice != NULL)
		{
			Choice->DlgBkColor = dlg.GetColor();
			DeleteObject (Choice->DlgBrush);
			Choice->DlgBrush = NULL;
			Choice->InvalidateRect (NULL);
		}
	}
}

void CATextView::OnUpdateFett(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	   pCmdUI->Enable ();
}

void CATextView::OnFett()
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.

	   if (!m_Txt.Fett)
	   {
		   m_Txt.Fett = TRUE;
	   }
	   else
	   {
		   m_Txt.Fett = FALSE;
	   }
	   m_Txt.SetSelectionCharSize (sg);
}

void CATextView::OnUpdateKursiv(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	   pCmdUI->Enable ();
}

void CATextView::OnKursiv()
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.

	   if (!m_Txt.Kursiv)
	   {
		   m_Txt.Kursiv = TRUE;
	   }
	   else
	   {
		   m_Txt.Kursiv = FALSE;
	   }
	   m_Txt.SetSelectionCharSize (sg);
}

void CATextView::OnUpdateUnderline(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	   pCmdUI->Enable ();
}

void CATextView::OnUnderline()
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.

	   if (!m_Txt.Underline)
	   {
		   m_Txt.Underline = TRUE;
	   }
	   else
	   {
		   m_Txt.Underline = FALSE;
	   }
	   m_Txt.SetSelectionCharSize (sg);
}

long CATextView::GetTextNrFromCommandline ()
{
	long text_nr = 0l;
	CString c;

	c = ::GetCommandLine ();
	int idx = c.Find (_T("TextNr="));
	if (idx != -1)
	{
		CString TextParam = c.Mid (idx);
		CToken t (TextParam, _T(" "));
		TextParam = t.GetToken (0);
		t.SetSep (_T("="));
		t = TextParam;
		if (t.GetAnzToken () > 1)
		{
			CString c = t.GetToken (1);
			text_nr = _tstol (c.GetBuffer ());
		}
	}
	return text_nr;
}