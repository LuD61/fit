#include "StdAfx.h"
#include ".\syscopy.h"

CSysCopy::CSysCopy(void)
{
}

CSysCopy::~CSysCopy(void)
{
}

void CSysCopy::Copy (ATEXTE_CLASS& ATexte, CVector& SysTab)
{
	if (CopyAll)
	{
		CopyAllTxt (ATexte, SysTab);
		return;
	}
	void *vSys;
	SysTab.FirstPosition ();
	while ((vSys = SysTab.GetNext ()) != NULL)
	{
		ATexte.atexte.sys = (long) LOWORD (vSys);
		ATexte.dbupdate ();
	}
}

void CSysCopy::CopyAllTxt (ATEXTE_CLASS& ATexte, CVector& SysTab)
{
	Rez.sqlin ((long *) &ATexte.atexte.sys, SQLLONG, 0);
	Rez.sqlout ((long *) &ATexte.atexte.txt_nr, SQLLONG, 0);
	Cursor = Rez.sqlcursor (_T("select txt_nr from ATexte where sys = ?"));
	if (Cursor == -1) return;

	void *vSys;
	SysTab.FirstPosition ();
	while ((vSys = SysTab.GetNext ()) != NULL)
	{
		ATexte.atexte.sys = ATexte.atexte.sys;
		Rez.sqlopen (Cursor);
  	    while (Rez.sqlfetch (Cursor) == 0)
		{
			if (Rez.dbreadfirst () != 0) continue;
			ATexte.atexte.sys = (long) LOWORD (vSys);
			Rez.dbupdate ();
			ATexte.atexte.sys = ATexte.atexte.sys;
		}
	}
	Rez.sqlclose (Cursor);
}

