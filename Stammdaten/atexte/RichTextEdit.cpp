#include "StdAfx.h"
#include "richtextedit.h"
#include "Token.h"
#include ".\richtextedit.h"
#include "resource.h"
#include "QuikInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CRichTextEdit, CRichEditCtrl)

CRichTextEdit::CRichTextEdit(void)
{

	SgTab.Add (new CPropertyItem (CString (_T("9")), CString (_T ("295")))); 
	SgTab.Add (new CPropertyItem (CString (_T("8")), CString (_T ("255")))); 
	SgTab.Add (new CPropertyItem (CString (_T("7")), CString (_T ("215")))); 
	SgTab.Add (new CPropertyItem (CString (_T("6")), CString (_T ("195")))); 
	SgTab.Add (new CPropertyItem (CString (_T("5")), CString (_T ("175")))); 
	SgTab.Add (new CPropertyItem (CString (_T("4")), CString (_T ("160")))); 
	SgTab.Add (new CPropertyItem (CString (_T("3")), CString (_T ("150")))); 
	SgTab.Add (new CPropertyItem (CString (_T("2")), CString (_T ("135")))); 
//	lastSize = 9;
	SgTab.Load (_T("sgmap.cfg"));
	FaceName = _T("MS SAN SHERIF");
//	FaceName = _T("Courier");
	Alignment = Left;
	Fett = FALSE;
	Underline = FALSE;
	Kursiv = FALSE;
}

CRichTextEdit::~CRichTextEdit(void)
{
	CPropertyItem *p;
	SgTab.FirstPosition ();
	while ((p = (CPropertyItem *) SgTab.GetNext ()) != NULL)
	{
		delete p;
	}
	SgTab.Init ();
	CTxtSg *ts;
	TextFormat.FirstPosition ();
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		delete ts;
	}
	TextFormat.Init ();
}

BOOL CRichTextEdit::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (SgInfo.IsWindowVisible ())
			{
				SgInfo.Hide ();
				return TRUE;
			}
			break;
		case WM_RBUTTONDOWN:
			int sg = GetSelectedSG ();
			CPoint p (pMsg->lParam);
//			GetParent ()->ClientToScreen (&p);
			CString Text;
			Text.Format (_T("Schriftgr��e in Auswahl = %d"), sg);
			SgInfo.SetText (Text.GetBuffer ());
			SgInfo.Show (p);
			return TRUE;
	}
	return CRichEditCtrl::PreTranslateMessage (pMsg);
}


void CRichTextEdit::DestroyFormat ()
{
	CTxtSg *ts;
	TextFormat.FirstPosition ();
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		delete ts;
	}
	TextFormat.Init ();
}

void CRichTextEdit::SetWindowText (LPCTSTR txt)
{
	if (!IsWindow (SgInfo.m_hWnd))
	{
		SgInfo.BkColor = GetSysColor (COLOR_3DFACE);
		SgInfo.BorderStyle = CQuikInfo::Dialog;
		RECT rect = {0, 0, 62, 22};
		SgInfo.Create (_T(""), this, 0, rect, GetParent ());
		SgInfo.SetFont (GetParent ()->GetFont ());
	}
    DestroyFormat ();
//	TextFormat.Add (new CTxtSg (NULL, 0, 0, 295));
	TextFormat.Add (new CTxtSg (0, 0, FaceName.GetBuffer (), 
					295, CTxtSg::No, CTxtSg::No, CTxtSg::No));
	CToken t;
    t.SetSep (_T("\n"));
	t = CString (txt);
	int count = t.GetAnzToken ();

	CString nTxt = _T("");
	CString Txt;

	for (int row = 0; row < count; row ++)
	{
		 Txt = t.GetToken (row);
		 Txt.TrimRight ();
		 GetTextSg (row, &Txt);
		 if (row > 0)
		 {
			 nTxt += _T('\n');
		 }
		 nTxt += Txt;
	}

	CWnd::SetWindowText (nTxt.GetBuffer ());

	CTxtSg *ts;
	TextFormat.FirstPosition ();
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		int pos = 0;
		GetPos (pos, ts->row, ts->col);
		SetSel (pos, -1);
		CHARFORMAT2 cf;
		GetSelectionCharFormat (cf);
		cf.dwEffects = 0;
		if (ts->Fett == ts->On)
		{
			cf.dwEffects |= CFM_BOLD;
		}
		else if (ts->Fett == ts->Off)
		{
			cf.dwEffects &= ~CFM_BOLD;
		}
		if (ts->Kursiv == ts->On)
		{
			cf.dwEffects |= CFM_ITALIC;
		}
		else if (ts->Kursiv == ts->Off)
		{
			cf.dwEffects &= ~CFM_ITALIC;
		}
		if (ts->Underline == ts->On) 
		{
			cf.dwEffects |= CFM_UNDERLINE;
		}
		else if (ts->Underline == ts->Off) 
		{
			cf.dwEffects &= ~CFM_UNDERLINE;
		}
		if (ts->size > 0)
		{
			cf.yHeight = ts->size;
			cf.dwMask = CFM_SIZE | CFM_FACE | CFM_UNDERLINE | CFM_ITALIC | CFM_BOLD;
		}
		else
		{
			cf.dwMask = CFM_FACE | CFM_UNDERLINE | CFM_ITALIC | CFM_BOLD;
		}
		SetSelectionCharFormat(cf);
	}
	SetSel (0, 0);
}

void CRichTextEdit::GetWindowText (CString& txt)
{
	CString Text;
	CWnd::GetWindowText (Text);

	DisplayText = Text;
// txt = Text;
// return;
	CToken t;
    t.SetSep (_T("\n"));
	t = Text;
	int count = t.GetAnzToken ();

	CString nTxt = _T("");
	CString Txt;

	for (int row = 0; row < count; row ++)
	{
		 Txt = t.GetToken (row);
		 Txt.TrimRight ();
		 GetTextSg (row, &Txt);
		 if (row > 0)
		 {
			 nTxt += _T('\n');
		 }
		 nTxt += Txt;
	}
	if (Text.Right (1) ==_T("\n"))
	{
			 nTxt += _T('\n');
	}
	Text = nTxt;

	txt = Text;
	int size = TextFormat.GetCount (); 
	if (size == 0) return;
	try
	{
		SgPosis = new SgPos* [size + 2];
	}
	catch (...)
	{
		return;
	}
	CTxtSg *ts;
	TextFormat.FirstPosition ();
	int is = 0;
	int fsize = TextFormat.GetCount (); 
//	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	for (int fi = 0; fi < fsize; fi ++)
	{
		ts = (CTxtSg *) TextFormat.Get (fi);
		if (ts == NULL) 
		{
			size = fi;
			break;
		}
		int pos = 0;
		GetPos (pos, ts->row, ts->col);
		CString Sg;
		if (ts->size != 0)
		{
			CString S;
			S.Format (_T("%d"), ts->size);
			int i = SgTab.FindValue (S);
			if (i == -1) 
			{
				size --;
				continue;
			}
			CPropertyItem *p = (CPropertyItem *) SgTab.Get (i);
			Sg = _T("^");
			Sg += p->Name;
			Sg += _T(";");
		}
		else
		{
			Sg = _T("");
		}
		if (fi > 0)
		{
				CTxtSg *ts1;
				ts1 = (CTxtSg *) TextFormat.Get (fi - 1);

				if ((ts->Fett == ts->On))
				{
							Sg += _T("^b+");
				}
				else if ((ts->Fett == ts->Off))
				{
							Sg += _T("^b-");
				}
				if ((ts->Kursiv == ts->On))
				{
							Sg += _T("^i+");
				}
				else if ((ts->Kursiv == ts->Off))
				{
							Sg += _T("^i-");
				}
				if ((ts->Underline == ts->On))
				{
							Sg += _T("^u+");
				}
				else if ((ts->Underline == ts->Off))
				{
							Sg += _T("^u-");
				}

/*
				if ((ts->Fett == ts->On) && !(ts1->Fett == ts->On))
				{
							Sg += _T("^b+");
				}
				else if ((ts->Fett == ts->Off) && !(ts1->Fett == ts->Off))
				{
							Sg += _T("^b-");
				}
				if ((ts->Kursiv == ts->On) && !(ts1->Kursiv == ts->On))
				{
							Sg += _T("^i+");
				}
				else if ((ts->Kursiv == ts->Off) && !(ts1->Kursiv == ts->Off))
				{
							Sg += _T("^i-");
				}
				if ((ts->Underline == ts->On) && !(ts1->Underline == ts->On))
				{
							Sg += _T("^u+");
				}
				else if ((ts->Underline == ts->Off) && !(ts1->Underline == ts->Off))
				{
							Sg += _T("^u-");
				}
*/
		}
		else
		{
				if (ts->Fett == ts->On)
				{
							Sg += _T("^b+");
				}
				if (ts->Kursiv == ts->On)
				{
							Sg += _T("^i+");
				}
				if (ts->Underline == ts->On)
				{
							Sg += _T("^u+");
				}
		}

		SgPosis[is] = new SgPos ();
		SgPosis[is]->pos = pos;
		SgPosis[is]->Sg = Sg;
		is ++;
	}

	qsort (SgPosis, size, sizeof (SgPos *), ComparePos);

	for (int i = 0; i < size; i ++)
	{
		CString Start = Text.Left (SgPosis[i]->pos);
		CString End = Text.Mid (SgPosis[i]->pos);
		Text = Start;
		Text += SgPosis[i]->Sg;
		Text += End;
	}

//	CleanText (Text);

    txt = Text;
	for (int i = 0; i < size; i ++)
	{
		delete SgPosis [i];
	}
	delete SgPosis;

}

int CRichTextEdit::GetSelectedSG ()
{
	int sg = 0;
	CHARRANGE cr;
	CString Text;
	CString Sg;
	GetSel (cr);
	if (cr.cpMin == cr.cpMax) return sg;
    GetWindowText (Text); 
	for (int tps = 0,tp = 0; tp <= cr.cpMin;tps ++)
	{
		if (Text.GetBuffer ()[tps] == _T('^'))
		{
			Sg = _T("^");
			tps ++;
			while (Text.GetBuffer ()[tps] != _T(';'))
			{
				Sg += Text.Mid (tps, 1);
				tps ++;
			}
			sg = _tstoi (&Sg.GetBuffer ()[1]);
            Sg += _T(";");
		}
		else
		{
			tp ++;
		}
	}
    return sg;
}

void CRichTextEdit::Copy ()
{
	CHARRANGE cr;
	CString Text;
	CString Sg;
	GetSel (cr);
	if (cr.cpMin == cr.cpMax) return;
    GetWindowText (Text); 
	for (int tps = 0,tp = 0; tp <= cr.cpMin;)
	{
		if (Text.GetBuffer ()[tps] == _T('^'))
		{
			Sg = _T("^");
			tps ++;
			while (Text.GetBuffer ()[tps] != _T(';'))
			{
				Sg += Text.Mid (tps, 1);
				tps ++;
			}
            Sg += _T(";");
		}
		else
		{
			tp ++;
			tps ++;
		}
	}

	CString newText = _T(""); 
	newText += Sg;

	if (Text.Mid (tps,1) == _T("\n"))
	{
		tps ++;
	}

	for (; tp <= cr.cpMax;tps ++)
	{
		if (Text.GetBuffer ()[tps] == _T('^'))
		{
			newText += _T("^");
			tps ++;
			while (Text.GetBuffer ()[tps] != _T(';'))
			{
				newText += Text.Mid (tps, 1);
				tps ++;
			}
			newText += _T(";");
			tps ++;
		}
		newText += Text.Mid (tps, 1);
		tp ++;
	}

	ToClipboard (newText);
}

void CRichTextEdit::Paste ()
{
	CHARRANGE cr;
	GetSel (cr);
	CString Text;
	FromClipboard (Text);
	CString oldText;
	GetWindowText (oldText);
	CString newText = _T("");
	if (oldText.GetLength () == 0)
	{
		cr.cpMin = -1;
		cr.cpMax = -1;
	}

	for (int tps = 0, tp = 0; tp < cr.cpMin; tps ++)
	{
			while (oldText.GetBuffer ()[tps] == _T('^'))
			{
				newText += _T("^");
				tps ++;
				while (oldText.GetBuffer ()[tps] != _T(';'))
				{
					newText += oldText.Mid(tps, 1);
					tps ++;
				}
				newText += _T(";");
				tps ++;
			}
			newText += oldText.Mid(tps, 1);
			tp ++;
	}

	newText += Text;

	for (; tp <= cr.cpMax; tps ++)
	{
		while (oldText.GetBuffer ()[tps] == _T('^'))
		{
			tps ++;
			while (oldText.GetBuffer ()[tps] != _T(';'))
			{
				tps ++;
			}
			tps ++;
		}
		tp ++;
	}

	cr.cpMin = newText.GetLength ();
	cr.cpMax = newText.GetLength ();
    
    newText += oldText.Mid (tps); 
	SetWindowText (newText.GetBuffer ());
	SetSel (cr);
}


void CRichTextEdit::ToClipboard (CString& Text)
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	try
	{
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Text.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Text.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_UNICODETEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

void CRichTextEdit::FromClipboard (CString& Text)
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return;
    }

    HGLOBAL hglbCopy;
    LPTSTR data;

	try
	{
		 hglbCopy =  ::GetClipboardData(CF_UNICODETEXT);
		 if (hglbCopy != NULL)
		 {
			data = (LPTSTR) GlobalLock ((HGLOBAL) hglbCopy);
			Text = data;
			GlobalUnlock ((HGLOBAL) hglbCopy);
		 }
		 else 
		 {
			hglbCopy =  ::GetClipboardData(CF_TEXT);
			if (hglbCopy == NULL)
			{
				throw 1;
			}
			data = (LPTSTR) GlobalLock ((HGLOBAL) hglbCopy);
			Text = data;
			GlobalUnlock ((HGLOBAL) hglbCopy);
		}
	}
	catch (...) {};
    CloseClipboard();
}

void CRichTextEdit::SetDefaultSize (int size)
{
	CString DevSize;
	DevSize.Format (_T("%d"), size);
	int i = SgTab.Find (DevSize);
	if (i == -1) return;
	CPropertyItem *p = (CPropertyItem *) SgTab.Get (i);
	size = _tstoi (p->Value.GetBuffer ());
	CHARFORMAT2 cf;
	GetDefaultCharFormat (cf);
	cf.dwMask = CFM_SIZE | CFM_FACE | CFM_UNDERLINE | CFM_ITALIC | CFM_BOLD;
	cf.dwEffects = 0;
	if (Fett) cf.dwEffects |= CFM_BOLD;
	if (Kursiv) cf.dwEffects |= CFM_ITALIC;
	if (Underline) cf.dwEffects |= CFM_UNDERLINE;
	cf.yHeight = size;
	_tcscpy (cf.szFaceName, FaceName.GetBuffer ());
	SetDefaultCharFormat(cf);
//	TextFormat.Add (new CTxtSg (NULL, 0, 0, size));
	TextFormat.Add (new CTxtSg (0, 0, FaceName.GetBuffer (), 
			                        size, FALSE, FALSE, FALSE));
}

void CRichTextEdit::SetSelectionCharSize (int size)
{
	CString DevSize;
//	lastSize = size;
	DevSize.Format (_T("%d"), size);
	int i = SgTab.Find (DevSize);
	if (i == -1)
	{
		if (SgTab.GetCount () > 0)
		{
			i = 0;
		}
		else
		{
			return;
		}
	}

	CPropertyItem *p = (CPropertyItem *) SgTab.Get (i);
	size = _tstoi (p->Value.GetBuffer ());
	CHARRANGE cr;
	GetSel (cr);
//	if (cr.cpMin == cr.cpMax) return;

	CHARFORMAT2 cf;

	SetSel (cr.cpMax, cr.cpMax);
	GetSelectionCharFormat (cf);
	SetSel (cr);
	SetTextSg (cf.yHeight);

    SetTextSg (size);
	cf.dwMask = CFM_SIZE | CFM_FACE | CFM_UNDERLINE | CFM_ITALIC | CFM_BOLD;
	cf.dwEffects = 0;
	if (Fett) cf.dwEffects |= CFM_BOLD;
	if (Kursiv) cf.dwEffects |= CFM_ITALIC;
	if (Underline) cf.dwEffects |= CFM_UNDERLINE;
	cf.yHeight = size;
	SetSelectionCharFormat(cf);
}

void CRichTextEdit::CleanSelected (CHARRANGE& cr)
{
	int rowstart;
	int colstart;
	int rowend;
	int colend;
	GetLinePos (cr.cpMin, rowstart, colstart);
	GetLinePos (cr.cpMax, rowend, colend);

	CTxtSg *ts;
	int count = TextFormat.GetCount ();
	for (int i = 0; i < count; i ++)
	{
		ts = (CTxtSg *) TextFormat.Get (i);
		if (ts->row < rowstart) continue;
		if (ts->row > rowend) continue;
		if ((ts->row == rowstart) && (ts->col < colstart)) continue;
		if ((ts->row == rowend) && (ts->col > colend)) continue;
		TextFormat.Drop (i);
		i --;
		count --;
	}
}

void CRichTextEdit::SetTextSg (int size)
{
	CHARRANGE cr;
	GetSel (cr);
	CleanSelected (cr);

	int row = 0;
	int column = 0;

	GetLinePos (cr.cpMin, row, column);
	SetTextFormat (row, column, size);
	GetLinePos (cr.cpMax, row, column);
	if (Fett)
	{
		SetTextOut (row, column, CTxtSg::FETT);
	}
	if (Kursiv)
	{
		SetTextOut (row, column, CTxtSg::KURSIV);
	}
	if (Underline)
	{
		SetTextOut (row, column, CTxtSg::UNDERLINE);
	}

	CHARFORMAT2 cf;
	GetSelectionCharFormat (cf);
	int actsize = cf.yHeight;
}

void CRichTextEdit::GetTextSg (int row, CString* Txt)
{
	TCHAR sg [5];
	int Fett = CTxtSg::No;
	int Kursiv = CTxtSg::No;
	int Underline = CTxtSg::No;

	CString Row = _T("");
    LPTSTR p = Txt->GetBuffer ();
    int length = Txt->GetLength ();

	int col = 0;
	for (int i = 0; i < length; i ++, p += 1)
	{
		if (*p == _T('^'))
		{
			_tcscpy (sg, _T("0"));
			p +=1;
			if (_tcslen (p) < 2) break;

			if (memcmp (p, _T("b+"), 2 * sizeof (TCHAR)) == 0)
			{
				Fett      = CTxtSg::On;
				p += 1;
			}
			else if (memcmp (p, _T("b-"), 2 * sizeof (TCHAR)) == 0)
			{
				Fett      = CTxtSg::Off;
				p += 1;
			}
			else if (memcmp (p, _T("i+"), 2 * sizeof (TCHAR)) == 0)
			{
				Kursiv    = CTxtSg::On;
				p += 1;
			}
			else if (memcmp (p, _T("i-"), 2 * sizeof (TCHAR)) == 0)
			{
				Kursiv    = CTxtSg::Off;
				p += 1;
			}
			else if (memcmp (p, _T("u+"), 2 * sizeof (TCHAR)) == 0)
			{
				Underline    = CTxtSg::On;
				p += 1;
			}
			else if (memcmp (p, _T("u-"), 2 * sizeof (TCHAR)) == 0)
			{
				Underline    = CTxtSg::Off;
				p += 1;
			}
			else
			{
				for (int j = 0; (*p != _T(';')) && (*p != 0); p += 1, j ++)
				{
					if (j == 4) break;
					sg[j] = *p;
				}
			    sg[j] = 0;
			}
			if (_tstoi (sg) != 0)
			{
				int isg = SgTab.Find (CString (sg));
				if (isg > -1)
				{
					CPropertyItem *p = (CPropertyItem *) SgTab.Get (isg);
					int size = _tstoi (p->Value.GetBuffer ());
					SetTextFormat (row, col, size, CTxtSg::No, CTxtSg::No, CTxtSg::No);
				}
			}
			else
			{
					SetTextFormat (row, col, 0, Fett, Kursiv, Underline);
			}
			if (*p == 0) break;
		}
		else
		{
			Row += *p;
			col ++;
		}
	}
	*Txt = Row.GetBuffer ();
}

void CRichTextEdit::SetTextFormat (int row, int column, int size)
{
	CTxtSg *ts;
	TextFormat.FirstPosition ();
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		if (*ts == CPoint (column, row))
		{
			break;
		}
	}
	if (ts != NULL)
	{
		if (size != 0) ts->size = size;
		if (Fett) 
		{
			ts->Fett = ts->On;
		}
		else
		{
			ts->Fett = ts->No;
		}
		if (Kursiv) 
		{
			ts->Kursiv = ts->On;
		}
		else
		{
			ts->Kursiv = ts->No;
		}
		if (Underline) 
		{
			ts->Underline = ts->On;
		}
		else
		{
			ts->Underline = ts->No;
		}
	}
	else
	{
//		TextFormat.Add (new CTxtSg (NULL, row, column, size));
		TextFormat.Add (new CTxtSg (row, column, FaceName.GetBuffer (), 
			                        size, Fett, Kursiv, Underline));
	}
}

void CRichTextEdit::SetTextFormat (int row, int column, int size, 
								   int Fett, int Kursiv, int Underline)
{
	CTxtSg *ts;
	TextFormat.FirstPosition ();
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		if (*ts == CPoint (column, row))
		{
			break;
		}
	}
	if (ts != NULL)
	{
		if (size != 0) ts->size = size;
		if (Fett != ts->No) ts->Fett = Fett;
		if (Kursiv != ts->No)ts->Kursiv = Kursiv;
		if (Underline != ts->No)ts->Underline = Underline;
	}
	else
	{
		TextFormat.Add (new CTxtSg (row, column, FaceName.GetBuffer (), 
			                        size, Fett, Kursiv, Underline));
	}
}

void CRichTextEdit::SetTextOut (int row, int column, int Attribute)
{
	CTxtSg *ts;
	TextFormat.FirstPosition ();
	while ((ts = (CTxtSg *) TextFormat.GetNext ()) != NULL)
	{
		if (*ts == CPoint (column, row))
		{
			break;
		}
	}

    int Fett      = ts->No;
    int Kursiv    = ts->No;
    int Underline = ts->No;

	if (ts != NULL)
	{
		Fett      = ts->Fett;
		Kursiv    = ts->Kursiv;
		Underline = ts->Underline;
	}

	switch (Attribute)
	{
		case ts->FETT:
			Fett = ts->Off;
			break;
		case ts->KURSIV:
			Kursiv = ts->Off;
			break;
		case ts->UNDERLINE:
			Underline = ts->Off;
			break;
	}

	if (ts != NULL)
	{
		ts->Fett      = Fett;
		ts->Kursiv    = Kursiv;
		ts->Underline = Underline;
	}
	else
	{

		TextFormat.Add (new CTxtSg (row, column, FaceName.GetBuffer (), 
			                        0, Fett, Kursiv, Underline));
	}
}

void CRichTextEdit::GetLinePos (int pos, int& row, int& column)
{
	CString Text;
	CToken t;

	CWnd::GetWindowText (Text);
    t.SetSep (_T("\n"));
	t = Text;
	CString Line;
	int count = t.GetAnzToken ();
	int i = 0;
	for (row = 0; row < count; row ++)
	{
		Line = t.GetToken (row);
		i += Line.GetLength ();
		if (i > pos) break;
	}
    if (row == count)
	{
//		row --;
//		column = Line.GetLength ();
		column = 0;
	}
	else
	{
		i -= Line.GetLength ();
		column = pos - i;
	}
}

void CRichTextEdit::GetPos (int& pos, int row, int column)
{
	CString Text;
	CToken t;

	CWnd::GetWindowText (Text);
    t.SetSep (_T("\n"));
	t = Text;
	CString Line;
	int count = t.GetAnzToken ();
	int i = 0;
	pos = 0;
	for (i = 0; i < row; i ++)
	{
		Line = t.GetToken (i);
		pos += Line.GetLength ();
	}
	pos += column;
}

int __cdecl CRichTextEdit::ComparePos (const void *el1, const void *el2)
{
    SgPos *elem1 = *(SgPos **) el1; 
    SgPos *elem2 = *(SgPos **) el2; 

	if (elem1->pos > elem2->pos) return -1;
	if (elem1->pos < elem2->pos) return 1;
	return 0;
}
BEGIN_MESSAGE_MAP(CRichTextEdit, CRichEditCtrl)
	ON_COMMAND(ID_TEXT_CENT, OnTextCent)
	ON_COMMAND(ID_TEXT_LEFT, OnTextLeft)
	ON_COMMAND(ID_TEXT_RIGHT, OnTextRight)
END_MESSAGE_MAP()

void CRichTextEdit::OnTextCent()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
    CString Text;
	GetWindowText (Text);

	PARAFORMAT pf;
	memset (&pf, 0, sizeof (pf));
    pf.cbSize = sizeof (pf);
    pf.dwMask = PFM_ALIGNMENT;
    pf.wAlignment = PFA_CENTER;
	CHARRANGE cr;
	GetSel (cr);
	SetSel (0, -1);
	SetParaFormat (pf);
	SetSel (cr);
	Alignment = Center;
}

void CRichTextEdit::OnTextLeft()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	PARAFORMAT pf;
	memset (&pf, 0, sizeof (pf));
    pf.cbSize = sizeof (pf);
    pf.dwMask = PFM_ALIGNMENT;
    pf.wAlignment = PFA_LEFT;
	CHARRANGE cr;
	GetSel (cr);
	SetSel (0, -1);
	SetParaFormat (pf);
	SetSel (cr);
	Alignment = Left;
}

void CRichTextEdit::OnTextRight()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	PARAFORMAT pf;
	memset (&pf, 0, sizeof (pf));
    pf.cbSize = sizeof (pf);
    pf.dwMask = PFM_ALIGNMENT;
    pf.wAlignment = PFA_RIGHT;
	CHARRANGE cr;
	GetSel (cr);
	SetSel (0, -1);
	SetParaFormat (pf);
	SetSel (cr);
	Alignment = Right;
}

void CRichTextEdit::SetAlignment (int Alignment)
{
	switch (Alignment)
	{
	case Left:
		GetParent ()->PostMessage (WM_COMMAND, ID_TEXT_LEFT, 0l);
		break;
	case Center:
		GetParent ()->PostMessage (WM_COMMAND, ID_TEXT_CENT, 0l);
		break;
	case Right:
		GetParent ()->PostMessage (WM_COMMAND, ID_TEXT_RIGHT, 0l);
		break;
	}
}

void CRichTextEdit::GetText (CString& txt)
{
	CWnd::GetWindowText (txt);
}

void CRichTextEdit::CleanText (CString& txt)
{
	LPTSTR buffer;
	TCHAR Sg[10];
	int len = txt.GetLength ();
	buffer = new TCHAR [len + 1];
    LPTSTR t = txt.GetBuffer ();

	int p = 0;
	int InSg = 0;
	for (int i = 0, j = 0; i < len; i ++)
	{
		if (InSg > 0)
		{
			if (t[i] == _T(';'))
			{
				Sg[p] = t[i];
				p ++;
				InSg = 0;
			}
			else if (t[i] == _T('^'))
			{
				p = 0;
				InSg = 1;
			}
			else
			{
				Sg[p] = t[i];
				p ++;
			}
		}
		else if (t[i] == _T('^'))
		{
			p = 0;
			InSg = 1;
			Sg[p] = t[i];
			p ++;
		}
		else if (t[i] == _T('\n'))
		{
            buffer[j] = t[i];
			j ++;
		}
		else
		{
			if (p > 0)
			{
				memcpy (&buffer[j], Sg, p * sizeof (TCHAR));
				j += p;
				p = 0;
			}
			buffer[j] = t[i];
			j ++;
		}
	}
	if (p > 0 && j == 0)
	{
		memcpy (&buffer[j], Sg, p * sizeof (TCHAR));
		j += p;
		p = 0;
	}
    buffer[j] = 0; 
	txt = buffer;
	delete buffer;
}