//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by atexte.rc
//
#define IDD_AText                       9
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_ATexteTYPE                  129
#define IDR_MAINFRAME1                  129
#define IDD_COPYDLG                     130
#define IDR_MAINFRAME2                  130
#define IDD_SEND_TO_SYS                 131
#define IDI_HARROWDOWN                  133
#define IDI_HARROWUP                    134
#define IDI_HARROWNO                    135
#define IDB_BITMAP2                     139
#define IDC_LSYS                        1000
#define IDC_SYS                         1001
#define IDC_LTXT_NR                     1002
#define IDC_TXT_NT                      1003
#define IDC_TXT                         1004
#define IDC_COMBO1                      1005
#define IDC_SG                          1005
#define IDC_LSG                         1006
#define IDC_ADR_NAM1                    1007
#define IDC_ACTTEXT                     1008
#define IDC_ALLTEXT                     1009
#define IDC_SENDCTRL                    1009
#define IDC_SYSCH                       1011
#define IDC_SEND                        1012
#define IDC_ADR_KRZ                     1013
#define IDC_BUTTON1                     1014
#define IDC_LTXTTYP                     1015
#define IDC_TXTTYP                      1016
#define IDC_TxtIdx                      1017
#define IDC_TEXTIDX                     1017
#define IDC_LTEXTIDX                    1018
#define IDC_ZxzIdxSpin                  1019
#define ID_TEXT_CENT                    32771
#define ID_BACK                         32772
#define ID_FILE_RIGHT                   32773
#define ID_DATEI_SCHLIE                 32775
#define ID_TEXT_LEFT                    32776
#define ID_TEXT_RIGHT                   32781
#define ID_COPY_SYS                     32783
#define ID_JOINARTICLE                  32785
#define ID_SEND                         32787
#define ID_FIRST                        32788
#define ID_PRIOR                        32789
#define ID_NEXT                         32790
#define ID_LAST                         32791
#define ID_ANSICHT_HINTERGRUND          32801
#define ID_DLG_BACKGROUND               32803
#define ID_CHOICEBACKGROUND             32805
#define ID_SFETT                        32807
#define ID_SKURSIV                      32808
#define ID_UNDERLINE                    32809
#define ID_SUNDERLINE                   32809
#define ID_DELETE                       32810
#define ID_LANGUAGE                     32812

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32810
#define _APS_NEXT_CONTROL_VALUE         1020
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
