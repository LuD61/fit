// ATexteDoc.h : Schnittstelle der Klasse CATexteDoc
//


#pragma once

class CATexteDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CATexteDoc();
	DECLARE_DYNCREATE(CATexteDoc)

// Attribute
public:

// Operationen
public:

// Überschreibungen
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CATexteDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


