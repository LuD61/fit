// atexteView.cpp : Implementierung der Klasse CatexteView
//

#include "stdafx.h"
#include "atexte.h"

#include "atexteDoc.h"
#include "atexteView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CatexteView

IMPLEMENT_DYNCREATE(CatexteView, CView)

BEGIN_MESSAGE_MAP(CatexteView, CView)
	// Standarddruckbefehle
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CatexteView Erstellung/Zerst�rung

CatexteView::CatexteView()
{
	// TODO: Hier Code zum Erstellen einf�gen

}

CatexteView::~CatexteView()
{
}

BOOL CatexteView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CView::PreCreateWindow(cs);
}

// CatexteView-Zeichnung

void CatexteView::OnDraw(CDC* /*pDC*/)
{
	CatexteDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: Code zum Zeichnen der systemeigenen Daten hinzuf�gen
}


// CatexteView drucken

BOOL CatexteView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void CatexteView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void CatexteView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// CatexteView Diagnose

#ifdef _DEBUG
void CatexteView::AssertValid() const
{
	CView::AssertValid();
}

void CatexteView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CatexteDoc* CatexteView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CatexteDoc)));
	return (CatexteDoc*)m_pDocument;
}
#endif //_DEBUG


// CatexteView Meldungshandler
