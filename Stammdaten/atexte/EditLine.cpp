#include "StdAfx.h"
#include "editline.h"

#define POINTFIVE 10
#define POINTTEN 20
#define STEP 5

CEditLine::CEditLine(void)
{
}

CEditLine::~CEditLine(void)
{
}

void CEditLine::Create(CWnd *Parent, int x, int y, int cx, int cy, DWORD Id)
{
	CStatic::Create (NULL, SS_OWNERDRAW | WS_VISIBLE, CRect (x, y, cx, cy), Parent, Id);
}

void CEditLine::DrawItem(LPDRAWITEMSTRUCT lpD)
{
	CRect rect;
	GetClientRect (&rect);
	CDC cDC;
	cDC.Attach (lpD->hDC);
	cDC.FillSolidRect (&rect, RGB (255, 255, 255));
	CBrush Brush;
	Brush.CreateSolidBrush (RGB (120, 120, 120));
	cDC.FrameRect (&rect, &Brush);
	int ystart = 2;
	int yend = rect.bottom - 1;
	int xstart = STEP;
	for (; xstart < rect.right; xstart += STEP)
	{
		if ((xstart % (STEP * 10)) == 0)
		{
			ystart = rect.bottom / 4;
		}
		else if ((xstart % (STEP * 5)) == 0)
		{
			ystart = rect.bottom / 2;
		}
		else
		{
			ystart = rect.bottom - rect.bottom / 4;
		}
		cDC.MoveTo (xstart, ystart);
		cDC.LineTo (xstart, yend);
	}
}
