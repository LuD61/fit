// CopyDialog.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "ATexte.h"
#include "CopyDialog.h"
#include ".\copydialog.h"


// CCopyDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CCopyDialog, CDialog)
CCopyDialog::CCopyDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CCopyDialog::IDD, pParent)
{
	CopyAll = FALSE;
}

CCopyDialog::~CCopyDialog()
{
}

void CCopyDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ACTTEXT, m_AktText);
	DDX_Control(pDX, IDC_ALLTEXT, m_AllText);
}


BEGIN_MESSAGE_MAP(CCopyDialog, CDialog)
	ON_BN_CLICKED(IDC_ACTTEXT, OnBnClickedActtext)
	ON_BN_CLICKED(IDC_ALLTEXT, OnBnClickedAlltext)
END_MESSAGE_MAP()


// CCopyDialog-Meldungshandler

BOOL CCopyDialog::OnInitDialog ()
{
	CDialog::OnInitDialog ();
	m_AktText.SetCheck (BST_CHECKED);
	return TRUE;
}

void CCopyDialog::OnBnClickedActtext()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CopyAll = FALSE;
}

void CCopyDialog::OnBnClickedAlltext()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CopyAll = TRUE;
}
