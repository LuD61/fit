// UpdateDisplaytextDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "UpdateDisplaytext.h"
#include "UpdateDisplaytextDlg.h"
#include "../UniFormField.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CUpdateDisplaytextDlg Dialogfeld



CUpdateDisplaytextDlg::CUpdateDisplaytextDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUpdateDisplaytextDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUpdateDisplaytextDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RICHEDIT21, m_Txt);
}

BEGIN_MESSAGE_MAP(CUpdateDisplaytextDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// CUpdateDisplaytextDlg Meldungshandler

BOOL CUpdateDisplaytextDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.


	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	ATexte.opendbase (_T("bws"));

	Form.Add (new CUniFormField (&m_Txt, RICHTEXTEDIT  , (char *) &ATexte.atexte.txt, VCHAR));
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (20, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (8, 2);

	CCtrlInfo *c_Txt = new CCtrlInfo (&m_Txt, 0, 0, DOCKRIGHT, DOCKBOTTOM);
    c_Txt->rightspace = 20; 
	c_Txt->SetCellPos (0, 0);
	CtrlGrid.Add (c_Txt);
	Form.Show ();
	CtrlGrid.Display ();

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CUpdateDisplaytextDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CUpdateDisplaytextDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CUpdateDisplaytextDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
