// UpdateDisplaytextDlg.h : Headerdatei
//

#pragma once
#include "afxcmn.h"
#include "../FormTab.h"
#include "../richtextedit.h"
#include "../CtrlGrid.h"
#include "../AText.h"


// CUpdateDisplaytextDlg Dialogfeld
class CUpdateDisplaytextDlg : public CDialog
{
// Konstruktion
public:
	CUpdateDisplaytextDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_UPDATEDISPLAYTEXT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

    ATEXTE_CLASS ATexte;
public:
	CCtrlGrid CtrlGrid;
	CRichEditCtrl m_Txt;
	CFormTab Form;
};
