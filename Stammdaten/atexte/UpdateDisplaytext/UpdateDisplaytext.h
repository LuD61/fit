// UpdateDisplaytext.h : Hauptheaderdatei f�r die UpdateDisplaytext-Anwendung
//

#pragma once

#ifndef __AFXWIN_H__
	#error 'stdafx.h' muss vor dieser Datei in PCH eingeschlossen werden.
#endif

#include "resource.h"		// Hauptsymbole


// CUpdateDisplaytextApp:
// Siehe UpdateDisplaytext.cpp f�r die Implementierung dieser Klasse
//

class CUpdateDisplaytextApp : public CWinApp
{
public:
	CUpdateDisplaytextApp();

// �berschreibungen
	public:
	virtual BOOL InitInstance();

// Implementierung

	DECLARE_MESSAGE_MAP()
};

extern CUpdateDisplaytextApp theApp;
