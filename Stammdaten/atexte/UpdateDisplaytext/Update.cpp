// Update.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "UpdateDisplaytext.h"
#include "Update.h"


// CUpdate-Dialogfeld

IMPLEMENT_DYNAMIC(CUpdate, CDialog)
CUpdate::CUpdate(CWnd* pParent /*=NULL*/)
	: CDialog(CUpdate::IDD, pParent)
{
}

CUpdate::~CUpdate()
{
}

void CUpdate::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CUpdate, CDialog)
END_MESSAGE_MAP()


// CUpdate-Meldungshandler
