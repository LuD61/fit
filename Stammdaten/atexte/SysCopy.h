#pragma once
#include "Vector.h"
#include "AText.h"

class CSysCopy
{
public:
	BOOL CopyAll;
	int Cursor;
	ATEXTE_CLASS Rez;
	CSysCopy(void);
	~CSysCopy(void);
	void Copy (ATEXTE_CLASS& ATexte, CVector& SysTab);
	void CopyAllTxt (ATEXTE_CLASS& ATexte, CVector& SysTab);
};
