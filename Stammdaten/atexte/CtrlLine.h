#ifndef _CTRLLINE_DEF
#define _CTRLLINE_DEF
#pragma once
#include "ctrlinfo.h"

class CCtrlLine :
	public CCtrlInfo
{
	DECLARE_DYNAMIC(CCtrlLine)
public:
    CCtrlLine(CWnd *, int, int, int, int, int,int, int);
	CCtrlLine::~CCtrlLine(void);
};
#endif
