#include "stdafx.h"
#include "ChoiceSys.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceSys::Sort1 = -1;
int CChoiceSys::Sort2 = -1;
int CChoiceSys::Sort3 = -1;

CChoiceSys::CChoiceSys(CWnd* pParent) 
        : CChoiceX(pParent)
{
}

CChoiceSys::~CChoiceSys() 
{
	DestroyList ();
}

void CChoiceSys::DestroyList() 
{
	for (std::vector<CSysList *>::iterator pabl = SysList.begin (); pabl != SysList.end (); ++pabl)
	{
		CSysList *abl = *pabl;
		delete abl;
	}
    SysList.clear ();
}

void CChoiceSys::FillList () 
{
    long  sys;
    short mdn;
	short fil;
    TCHAR adr_krz [34];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Ger�te"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Ger�tenummer"),  1, 100, LVCFMT_RIGHT);
    SetCol (_T("Filiale"),  2, 250);

	if (SysList.size () == 0)
	{
		DbClass->sqlout ((long *)&sys,      SQLLONG, 0);
		DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
		DbClass->sqlout ((short *)&fil,     SQLSHORT, 0);
		int cursor = DbClass->sqlcursor (_T("select sys, mdn, fil ")
			                             _T("from sys_peri where sys > 0"));

		DbClass->sqlin ((short *)&mdn,     SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)adr_krz,  SQLCHAR, sizeof (adr_krz));
		int mdn_cursor = DbClass->sqlcursor (_T("select adr_krz from mdn, adr ")
			                                 _T("where mdn.mdn = ? ")
											 _T("and adr.adr = mdn.adr"));
		DbClass->sqlin ((short *)&mdn,     SQLSHORT, 0);
		DbClass->sqlin ((short *)&fil,     SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)adr_krz,  SQLCHAR, sizeof (adr_krz));
		int fil_cursor = DbClass->sqlcursor (_T("select adr_krz from fil, adr ")
			                                 _T("where fil.mdn = ? ")
											 _T("and fil.fil = ? ")
											 _T("and adr.adr = fil.adr"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			_tcscpy (adr_krz, _T(""));
			if (fil == 0)
			{
				DbClass->sqlopen (mdn_cursor);
				DbClass->sqlfetch (mdn_cursor);
			}
			else
			{
				DbClass->sqlopen (fil_cursor);
				DbClass->sqlfetch (fil_cursor);
			}
			LPSTR pos = (LPSTR) adr_krz;
			CDbUniCode::DbToUniCode (adr_krz, pos);
			CSysList *abl = new CSysList (sys, adr_krz);
			SysList.push_back (abl);
		}
		DbClass->sqlclose (fil_cursor);
		DbClass->sqlclose (mdn_cursor);
		DbClass->sqlclose (cursor);
		Load ();
	}

	for (std::vector<CSysList *>::iterator pabl = SysList.begin (); pabl != SysList.end (); ++pabl)
	{
		CSysList *abl = *pabl;
		CString Sys;
		Sys.Format (_T("%hd"), abl->sys); 
		CString Num;
		CString Bez;
		_tcscpy (adr_krz, abl->adr_krz.GetBuffer ());

		CString LText;
		LText.Format (_T("%hd %s"), abl->sys, 
									abl->adr_krz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Sys;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (adr_krz, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort (listView);
}


void CChoiceSys::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceSys::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceSys::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceSys::SearchAdrKrz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
    int i;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceSys::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchAdrKrz (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceSys::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceSys::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   return 0;
}


void CChoiceSys::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CSysList *abl = SysList [i];
		   
		   abl->sys     = _tstoi (ListBox->GetItemText (i, 1));
		   abl->adr_krz = ListBox->GetItemText (i, 2);
	}
	for (int i = 1; i <= 4; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);
}

void CChoiceSys::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = SysList [idx];
}

void CChoiceSys::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (SysList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}


CSysList *CChoiceSys::GetSelectedText ()
{
	CSysList *abl = (CSysList *) SelectedRow;
	return abl;
}

