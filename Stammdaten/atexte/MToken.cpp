#include "StdAfx.h"
#include "mtoken.h"

CMToken::CMToken(void)
{
}

CMToken::~CMToken(void)
{
}


void CMToken::GetTokens (LPTSTR Txt)
{
     AnzToken = 0;
//     Buffer.TrimRight ();
     LPTSTR b = Buffer.GetBuffer (512);
     LPTSTR p = TStrTok (b, sep.GetBuffer (512));
     while (p != NULL)
     {
             AnzToken ++;
             p = TStrTok (NULL, sep.GetBuffer (512));
     }
     Tokens = new CString *[AnzToken];
     if (Tokens == NULL)
     {
             return;
     }

     Buffer = Txt;
//     Buffer.TrimRight ();
     b = Buffer.GetBuffer (0);
     p = TStrTok (b, sep.GetBuffer (0));
     int i = 0;
     while (p != NULL)
     {
             Tokens[i] = new CString (p);
             p = TStrTok (NULL, sep.GetBuffer (512));
             i ++;
     }
     AktToken = 0;
}

char *CMToken::StrTok (char *str, char *sep)
{
	static char *apos = NULL;
	char *p;
	int i, j;


	if (str != NULL) apos = str;
	if (str == NULL && apos == NULL) return NULL;

	p = apos;
	for (i = 0; apos[i] != 0; i ++)
	{
		for (j = 0; sep[j] != 0; j ++)
		{
			if (apos[i] == sep[j])
			{
				apos[i] = 0;
				apos = &apos[i + 1];
				return (p);
			}
		}
	}
	apos = NULL;
	return p;
}

wchar_t *CMToken::WStrTok (wchar_t *str, wchar_t *sep)
{
	static wchar_t *apos = NULL;
	wchar_t *p;
	int i, j;

	if (str != NULL) apos = str;
	if (str == NULL && apos == NULL) return NULL;

	p = apos;
	for (i = 0; apos[i] != 0; i ++)
	{
		for (j = 0; sep[j] != 0; j ++)
		{
			if (apos[i] == sep[j])
			{
				apos[i] = 0;
				apos = &apos[i + 1];
				return (p);
			}
		}
	}
	apos = NULL;
	return p;
}

const CMToken& CMToken::operator=(LPTSTR Txt)
{
     if (Tokens != NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt);
     return *this;
}

const CMToken& CMToken::operator=(CString& Txt)
{
     if (Tokens != NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt.GetBuffer (512));
     return *this;
}
