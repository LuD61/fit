#pragma once
#include "afxwin.h"

class CEditLine :
	public CStatic
{
public:
	CEditLine(void);
	~CEditLine(void);
	void Create (CWnd* Parent, int x, int y, int cx, int cy, DWORD Id);
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
};
