// ChoiceView.cpp: Implementierungsdatei
//
#include "stdafx.h"
#include "Groups.h"
#include "MainSplitter.h"
#include "ChoiceView.h"
#include "windowhandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CChoiceView

IMPLEMENT_DYNCREATE(CChoiceView, CFormView)

CChoiceView::CChoiceView()
	: CFormView(CChoiceView::IDD)
{
	m_Choice = NULL;
}

CChoiceView::~CChoiceView()
{
}

void CChoiceView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TITLE, m_Title);
}

BEGIN_MESSAGE_MAP(CChoiceView, CFormView)
	ON_WM_SIZE ()
	ON_WM_DESTROY ()
//	ON_WM_CTLCOLOR ()
//	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND (IDC_TITLE, OnTitle)
//	ON_COMMAND(ID_BACK, OnBack)
END_MESSAGE_MAP()

void CChoiceView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

//	m_Title.SetBoderStyle (CLabel::Solide);
//	m_Title.SetDynamicColor (TRUE);
//	m_Title.SetWindowText (_T("X"));

	m_Title.nID = IDC_TITLE;
	m_Title.Orientation = m_Title.Right;
	m_Title.TextStyle = m_Title.Bold;
	m_Title.BorderStyle = m_Title.Solide;
	m_Title.DynamicColor = TRUE;
	m_Title.TextColor =RGB (255, 255, 255);
	m_Title.SetBkColor (RGB (100, 180, 255));
//	m_Title.SetBkColor (m_Title.DynColorGray);
	m_Title.DynColor = RGB (64, 64, 64);
	m_Title.EnableRollover = FALSE;
	m_Title.m_ActiveReg = m_Title.RightActive;
	m_TitleReg.cx = 12;
	m_TitleReg.cy = -1;
	m_Title.m_ActiveSize = &m_TitleReg;
//	m_Title.DynColor = m_Title.DynColorGray;
//	m_Title.RolloverColor = RGB (204, 204, 255);
	m_Title.SetWindowText (_T("X "));


	m_Choice = WINDOWHANDLER->GetChoice ();

	if (m_Choice != NULL)
	{
		m_Choice->SetParent (this);
		m_Choice->HideFilter = FALSE;
		m_Choice->HideEnter = TRUE;
		m_Choice->HideOK    = TRUE;
		m_Choice->HideCancel = TRUE;
		m_Choice->SetEmbeddedStyle (0);
		m_Choice->IsModal = FALSE;
		m_Choice->FirstRead = FALSE;
		m_Choice->CreateDlg ();
		m_Choice->ShowWindow (SW_SHOWNORMAL);

		CtrlGrid.Create (this, 20, 20);
		CtrlGrid.SetBorder (0, 0);
		CtrlGrid.SetCellHeight (15);
		CtrlGrid.SetFontCellHeight (this);
		CtrlGrid.SetGridSpace (0, 0);

	    CCtrlInfo *c_Title = new CCtrlInfo (&m_Title, 0, 0, DOCKRIGHT, 1);
        CtrlGrid.Add (c_Title);
	    CCtrlInfo *c_Choice = new CCtrlInfo (m_Choice, 0, 1, DOCKRIGHT, DOCKBOTTOM);
        CtrlGrid.Add (c_Choice);

		CtrlGrid.Display ();
		m_Choice->PostMessage (WM_SETFOCUS, 0, 0l);
	}
}

void CChoiceView::OnSize (UINT nType, int cx, int cy)
{
	if (m_Choice != NULL && IsWindow (m_Choice->m_hWnd))
	{
		CRect r;
		m_Choice->GetWindowRect (&r);
		ScreenToClient (&r);
		r.right = r.left + cx - 100;
		r.bottom = r.top + cy - 100;
//		m_Choice->MoveWindow (&r);
		CtrlGrid.Move (0, 0);
	}
}

void CChoiceView::OnTitle ()
{
	WINDOWHANDLER->OnDeleteChoice ();
}

void CChoiceView::OnBack ()
{
	WINDOWHANDLER->OnBack ();	 
}

void CChoiceView::OnDestroy ()
{
	if (m_Choice != NULL)
	{
	}
}

void CChoiceView::OnFileSave ()
{
}

// CChoiceView-Diagnose

#ifdef _DEBUG
void CChoiceView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CChoiceView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CChoiceView-Meldungshandler
