#pragma once
#include "CtrlGrid.h"
#include "afxwin.h"
#include "RunMessage.h"
#include "MessageHandler.h"
//#include "AngebotPage.h"
#include "CtrlGrid.h"
#include "Label.h"
#include "VLabel.h"
#include "FavoriteTreeCtrl.h"
#include "afxcmn.h"


// CFavoriteView-Formularansicht

class CFavoriteView : public CFormView
{
	DECLARE_DYNCREATE(CFavoriteView)

protected:
	CFavoriteView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CFavoriteView();

public:
	enum { IDD = IDD_FAVORITEN };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()
private:
	CFont Font;
	CFont TitleFont;
	int FontHeight;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	HBRUSH TitleBrush;
	CCtrlGrid TitleGrid;
	CCtrlGrid AngebotGrid;
	CCtrlGrid CtrlGrid;
	CVLabel m_FavoriteTitle;
	CFavoriteTreeCtrl m_FavoriteTree;
	afx_msg void OnNMDblclkFavoriteTree(NMHDR *pNMHDR, LRESULT *pResult);
};


