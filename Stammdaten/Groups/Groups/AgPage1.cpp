// AgPage1.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Groups.h"
#include "AgPage1.h"
#include "WindowHandler.h"
#include "Core.h"
#include "DatabaseCore.h"
#include "uniformfield.h"
#include "AgSheet.h"


// CAgPage1-Dialogfeld

IMPLEMENT_DYNAMIC(CAgPage1, CPropertyPage)

CAgPage1::CAgPage1()
	: CPropertyPage(CAgPage1::IDD)
{
	Choice = NULL;
	ChoiceWg = NULL;
	HideOK = FALSE;
	DlgBkColor = NULL;
	DlgBrush = NULL;
	m_CanWrite = FALSE;
	HeadControls.Add (&m_Ag);
	m_TestFocus = TRUE;
	m_CurrentFocus = NULL;
	WINDOWHANDLER->SetBase (this);
	WINDOWHANDLER->RegisterUpdate (this);
}

CAgPage1::~CAgPage1()
{
	WINDOWHANDLER->SetBase (NULL);
	WINDOWHANDLER->UnregisterUpdate (this);
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
	HeadControls.Destroy ();
	Choice = (CChoiceAG *) WINDOWHANDLER->ChoiceAg ();
	if (Choice != NULL && IsWindow (Choice->m_hWnd))
	{
		delete Choice;
	}
}

void CAgPage1::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LAG, m_LAg);
	DDX_Control(pDX, IDC_AG, m_Ag);
	DDX_Control(pDX, IDC_AG_GROUP, m_AgGroup);
	DDX_Control(pDX, IDC_LAG_BZ, m_LAgBz);
	DDX_Control(pDX, IDC_AG_BZ1, m_AgBz1);
	DDX_Control(pDX, IDC_AG_BZ2, m_AgBz2);
	DDX_Control(pDX, IDC_LWG, m_LWg);
	DDX_Control(pDX, IDC_WG, m_Wg);
	DDX_Control(pDX, IDC_WG_BZ1, m_WgBz1);
	DDX_Control(pDX, IDC_LHWG, m_LHwg);
	DDX_Control(pDX, IDC_HWG, m_Hwg);
	DDX_Control(pDX, IDC_HWG_BZ1, m_HwgBz1);
	DDX_Control(pDX, IDC_LABT, m_LAbt);
	DDX_Control(pDX, IDC_ABT, m_Abt);
	DDX_Control(pDX, IDC_LTIER_KZ, m_LTierKz);
	DDX_Control(pDX, IDC_TIER_KZ, m_TierKz);
	DDX_Control(pDX, IDC_LWA_KTO, m_LWeKto);
	DDX_Control(pDX, IDC_WE_KTO, m_WeKto);
	DDX_Control(pDX, IDC_LERL_KTO, m_LErlKto);
	DDX_Control(pDX, IDC_ERL_KTO, m_ErlKto);
	DDX_Control(pDX, IDC_LMWST, m_LMwst);
	DDX_Control(pDX, IDC_MWST, m_Mwst);
	DDX_Control(pDX, IDC_LME_EINH, m_LMeEinh);
	DDX_Control(pDX, IDC_ME_EINH, m_MeEinh);
	DDX_Control(pDX, IDC_LBEST_AUTO, m_LBestAuto);
	DDX_Control(pDX, IDC_BEST_AUTO, m_BestAuto);
	//	DDX_Control(pDX, IDC_LPERS_RAB, m_LPersRab);
	//	DDX_Control(pDX, IDC_PERS_RAB, m_PersRab);
	DDX_Control(pDX, IDC_LSMT, m_LSmt);
	DDX_Control(pDX, IDC_SMT, m_Smt);
	DDX_Control(pDX, IDC_LTEIL_SMT, m_LTeilSmt);
	DDX_Control(pDX, IDC_TEIL_SMT, m_TeilSmt);
	DDX_Control(pDX, IDC_CALC_GROUP, m_CalcGroup);
	DDX_Control(pDX, IDC_LKOST_KZ, m_LKostKz);
	DDX_Control(pDX, IDC_KOST_KZ, m_KostKz);
	DDX_Control(pDX, IDC_LSP_KZ, m_LSpKz);
	DDX_Control(pDX, IDC_SP_KZ, m_SpKz);
	DDX_Control(pDX, IDC_LSP_VK, m_LSpVk);
	DDX_Control(pDX, IDC_SP_VK, m_SpVk);
	DDX_Control(pDX, IDC_LSP_FIL, m_LSpFil);
	DDX_Control(pDX, IDC_SP_FIL, m_SpFil);
	DDX_Control(pDX, IDC_LSP_SK, m_LSpSk);
	DDX_Control(pDX, IDC_SP_SK, m_SpSk);
	DDX_Control(pDX, IDC_LSW, m_LSw);
	DDX_Control(pDX, IDC_SW, m_Sw);

	DDX_Control(pDX, IDC_LBEARB_LAD, m_LBearbLad);
	DDX_Control(pDX, IDC_BEARB_LAD, m_BearbLad);
	DDX_Control(pDX, IDC_LBEARB_FIL, m_LBearbFil);
	DDX_Control(pDX, IDC_BEARB_FIL, m_BearbFil);
	DDX_Control(pDX, IDC_LBEARB_SK, m_LBearbSk);
	DDX_Control(pDX, IDC_BEARB_SK, m_BearbSk);

	DDX_Control(pDX, IDC_DURCH_VK,  m_DurchVk);
	DDX_Control(pDX, IDC_DURCH_FIL, m_DurchFil);
	DDX_Control(pDX, IDC_DURCH_SK,  m_DurchSk);
	DDX_Control(pDX, IDC_DURCH_SW,  m_DurchSw);
	DDX_Control(pDX, IDC_LVERK_BEG, m_LVerkBeg);
	DDX_Control(pDX, IDC_VERK_BEG, m_VerkBeg);
	DDX_Control(pDX, IDC_LVERK_END, m_LVerkEnd);
	DDX_Control(pDX, IDC_VERK_END, m_VerkEnd);
	DDX_Control(pDX, IDC_LVERK_ART, m_LVerkArt);
	DDX_Control(pDX, IDC_LART_TYP, m_LArtTyp);
	DDX_Control(pDX, IDC_VERK_ART, m_VerkArt);
	//	DDX_Control(pDX, IDC_LUMS_VERB, m_LUmsVerb);
	//	DDX_Control(pDX, IDC_UMS_VERB, m_UmsVerb);
	DDX_Control(pDX, IDC_ART_TYP, m_ArtTyp);
}


BEGIN_MESSAGE_MAP(CAgPage1, CPropertyPage)
//	ON_WM_SHOWWINDOW ()
	ON_WM_ACTIVATE ()
	ON_WM_CTLCOLOR ()
	ON_COMMAND(IDC_AGCHOICE , OnChoice)
	ON_COMMAND (SELECTED, OnSelected)
	ON_COMMAND (CANCELED, OnCanceled)
	ON_COMMAND(IDC_WGCHOICE , OnWgChoice)
	ON_EN_KILLFOCUS(IDC_WG, &CAgPage1::OnEnKillfocusWg)
END_MESSAGE_MAP()


// CAgPage1-Meldungshandler

BOOL CAgPage1::OnInitDialog ()
{
	BOOL ret = CPropertyPage::OnInitDialog ();
	DATABASE->InitAg ();

#ifdef BLUE_COL
	COLORREF BkColor (RGB (100, 180, 255));
	COLORREF DynColor = m_NewAngTyp.DynColorBlue;
	COLORREF RolloverColor = RGB (204, 204, 255);
#else
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
#endif

// Notl�sung, weil der Basisfont nicht dem erwartetem Font entspricht, dadurch werden beim Setzen eines neuen Fonts im Grid
// die Controlgr��en verf�lscht.

	CFont *f = GetParent ()->GetParent ()->GetFont ();
	m_Ag.SetFont (f);
	CWnd *item = this->GetNextDlgTabItem (&m_Ag);
	while (item != &m_Ag)
	{
		item->SetFont (f);
		item = this->GetNextDlgTabItem (item);
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else if (GetSystemMetrics (SM_CXFULLSCREEN) <= 1300)
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (105, _T("Dlg"));
	}

	CRect rect;

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 17;
	TitleFont.CreateFontIndirect (&l);

    Form.Add (new CFormField (&m_Ag,EDIT,         (long *) &TAG.ag, VLONG));
    Form.Add (new CUniFormField (&m_AgBz1,EDIT,   (char *)  TAG.ag_bz1, VCHAR, sizeof (TAG.ag_bz1)));
    Form.Add (new CUniFormField (&m_AgBz2,EDIT,   (char *)  TAG.ag_bz2, VCHAR, sizeof (TAG.ag_bz2)));
    Form.Add (new CFormField (&m_Wg,EDIT,         (short *)  &TWG_AG.wg, VSHORT));
    Form.Add (new CUniFormField (&m_WgBz1,EDIT,   (char *)  TWG_AG.wg_bz1, VCHAR, sizeof (TWG.wg_bz1)));
    Form.Add (new CFormField (&m_Hwg,EDIT,        (short *) &THWG_AG.hwg, VSHORT));
    Form.Add (new CUniFormField (&m_HwgBz1,EDIT,   (char *)  THWG_AG.hwg_bz1, VCHAR, sizeof (THWG.hwg_bz1)));
    Form.Add (new CFormField (&m_Abt,EDIT,        (short *) &TAG.abt, VSHORT, 4));

    Form.Add (new CFormField (&m_TierKz,COMBOBOX, (char *)   TAG.tier_kz, VCHAR));
	CString *ComboValue = new CString (_T(""));
	Form.GetFormField (&m_TierKz)->ComboValues.push_back (ComboValue);
	CORE->AppendCombo (_T("tier_kz"), &Form.GetFormField (&m_TierKz)->ComboValues);
	Form.GetFormField (&m_TierKz)->FillComboBox ();
	m_TierKz.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_WeKto,EDIT,       (long * ) &TAG.we_kto, VLONG, 8));
    Form.Add (new CFormField (&m_ErlKto,EDIT,      (long * ) &TAG.erl_kto, VLONG, 8));
    Form.Add (new CFormField (&m_Smt,  COMBOBOX,   (char * ) TAG.smt, VCHAR));
	CORE->FillCombo (_T("smt"), &Form.GetFormField (&m_Smt)->ComboValues);
	Form.GetFormField (&m_Smt)->FillComboBox ();
	m_Smt.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_TeilSmt,  COMBOBOX,   (short * ) &TAG.teil_smt, VSHORT));
	CORE->FillCombo (_T("teil_smt"), &Form.GetFormField (&m_TeilSmt)->ComboValues);
	Form.GetFormField (&m_TeilSmt)->FillComboBox ();
	m_TeilSmt.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_Mwst,  COMBOBOX,  (short * )&TAG.mwst, VSHORT));
	CORE->FillCombo (_T("mwst"), &Form.GetFormField (&m_Mwst)->ComboValues);
	Form.GetFormField (&m_Mwst)->FillComboBox ();

    Form.Add (new CFormField (&m_MeEinh,  COMBOBOX,  (short * )&TAG.me_einh, VSHORT));
	CORE->FillCombo (_T("me_einh"), &Form.GetFormField (&m_MeEinh)->ComboValues);
	Form.GetFormField (&m_MeEinh)->FillComboBox ();
	m_MeEinh.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_BestAuto,  COMBOBOX,  (char * )TAG.best_auto, VCHAR));
	CORE->FillCombo (_T("best_auto"), &Form.GetFormField (&m_BestAuto)->ComboValues);
	Form.GetFormField (&m_BestAuto)->FillComboBox ();
	m_BestAuto.SetDroppedWidth (300);

//    Form.Add (new CFormField (&m_PersRab,EDIT,       (double * ) &TAG.pers_rab, VDOUBLE, 5, 2));

    Form.Add (new CFormField (&m_KostKz,  COMBOBOX,  (char * )  TAG.kost_kz, VCHAR));
	CORE->FillCombo (_T("kost_kz"), &Form.GetFormField (&m_KostKz)->ComboValues);
	Form.GetFormField (&m_KostKz)->FillComboBox ();
	m_KostKz.SetDroppedWidth (300);

	Form.Add (new CFormField (&m_SpKz,  COMBOBOX,  (char * )  TAG.sp_kz, VCHAR));
	CORE->FillCombo (_T("sp_kz"), &Form.GetFormField (&m_SpKz)->ComboValues);
	Form.GetFormField (&m_SpKz)->FillComboBox ();
	m_SpKz.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_VerkArt,  COMBOBOX,  (char * )  TAG.verk_art, VCHAR));
	CORE->FillCombo (_T("verk_art"), &Form.GetFormField (&m_VerkArt)->ComboValues);
	Form.GetFormField (&m_VerkArt)->FillComboBox ();
	m_VerkArt.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_ArtTyp,  COMBOBOX,  (short * )  &TAG.a_typ, VSHORT));
	CORE->FillCombo (_T("a_typ"), &Form.GetFormField (&m_ArtTyp)->ComboValues);
	Form.GetFormField (&m_ArtTyp)->FillComboBox ();
	m_ArtTyp.SetDroppedWidth (250);

	
//    Form.Add (new CFormField (&m_UmsVerb,  COMBOBOX,  (char * )  TAG.ums_verb, VCHAR));
//	CORE->FillCombo (_T("ums_verb"), &Form.GetFormField (&m_UmsVerb)->ComboValues);
//	Form.GetFormField (&m_UmsVerb)->FillComboBox ();

    Form.Add (new CFormField (&m_VerkBeg,DATETIMEPICKER,   (DATE_STRUCT * ) &TAG.verk_beg, VDATE));
    Form.Add (new CFormField (&m_VerkEnd,DATETIMEPICKER,   (DATE_STRUCT * ) &TAG.verk_end, VDATE));
    Form.Add (new CFormField (&m_SpVk,EDIT,       (double * ) &TAG.sp_vk, VDOUBLE, 5, 1));
    Form.Add (new CFormField (&m_SpFil,EDIT,      (double * ) &TAG.sp_fil, VDOUBLE, 5, 1));
    Form.Add (new CFormField (&m_SpSk,EDIT,       (double * ) &TAG.sp_sk, VDOUBLE, 5, 1));
    Form.Add (new CFormField (&m_Sw,EDIT,         (double * ) &TAG.sw, VDOUBLE, 3, 1));

    Form.Add (new CFormField (&m_BearbLad, EDIT,   (short * )  &TAG.bearb_lad, VSHORT, 4));
    Form.Add (new CFormField (&m_BearbFil, EDIT,   (short * )  &TAG.bearb_fil, VSHORT, 4));
    Form.Add (new CFormField (&m_BearbSk,  EDIT,   (short * )  &TAG.bearb_sk,  VSHORT, 4));

    Form.Add (new CFormField (&m_DurchVk,  CHECKBOX,   (char * )   TAG.durch_vk, VCHAR));
    Form.Add (new CFormField (&m_DurchFil, CHECKBOX,   (char * )   TAG.durch_fil, VCHAR));
    Form.Add (new CFormField (&m_DurchSk,  CHECKBOX,   (char * )   TAG.durch_sk, VCHAR));
    Form.Add (new CFormField (&m_DurchSw,  CHECKBOX,   (char * )   TAG.durch_sw, VCHAR));


    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//Grid Wgnummer

	AgGrid.Create (this, 2, 2);
    AgGrid.SetBorder (0, 0);
    AgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Ag = new CCtrlInfo (&m_Ag, 0, 0, 1, 1);
	AgGrid.Add (c_Ag);
	CtrlGrid.CreateChoiceButton (m_AgChoice, IDC_AGCHOICE, this);
	CCtrlInfo *c_AgChoice = new CCtrlInfo (&m_AgChoice, 1, 0, 1, 1);
	AgGrid.Add (c_AgChoice);

//Grid Wg

	WgGrid.Create (this, 3, 3);
    WgGrid.SetBorder (0, 0);
    WgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Wg = new CCtrlInfo (&m_Wg, 0, 0, 1, 1);
	WgGrid.Add (c_Wg);
	CtrlGrid.CreateChoiceButton (m_WgChoice, IDC_WGCHOICE, this);
	CCtrlInfo *c_WgChoice = new CCtrlInfo (&m_WgChoice, 1, 0, 1, 1);
	WgGrid.Add (c_WgChoice);
	CCtrlInfo *c_WgBz1 = new CCtrlInfo (&m_WgBz1, 2, 0, 1, 1);
	c_WgBz1->SetCellPos (10, 0);
	WgGrid.Add (c_WgBz1);

//Grid Hwg

	HwgGrid.Create (this, 3, 3);
    HwgGrid.SetBorder (0, 0);
    HwgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Hwg = new CCtrlInfo (&m_Hwg, 0, 0, 1, 1);
	HwgGrid.Add (c_Hwg);
//	CtrlGrid.CreateChoiceButton (m_WgChoice, IDC_WGCHOICE, this);
//	CCtrlInfo *c_WgChoice = new CCtrlInfo (&m_WgChoice, 1, 0, 1, 1);
//	HwgGrid.Add (c_WgChoice);
	CCtrlInfo *c_HwgBz1 = new CCtrlInfo (&m_HwgBz1, 1, 0, 1, 1);
	c_HwgBz1->SetCellPos (10, 0);
	HwgGrid.Add (c_HwgBz1);

    DataGrid.Create (this, 20, 20);
    DataGrid.SetBorder (0, 0);
	DataGrid.SetCellHeight (15);
    DataGrid.SetGridSpace (5, 15);  //Spaltenabstand und Zeilenabstand
	CCtrlInfo *c_AgGroup = new CCtrlInfo (&m_AgGroup, 0, 0, DOCKRIGHT, DOCKBOTTOM);
	c_AgGroup->SetBottomControl (this, &m_VerkBeg, 10, CCtrlInfo::Bottom);
	DataGrid.Add (c_AgGroup);
	CCtrlInfo *c_LAgBz = new CCtrlInfo (&m_LAgBz, 1, 1, 1, 1);
	DataGrid.Add (c_LAgBz);

	CCtrlInfo *c_AgBz1 = new CCtrlInfo (&m_AgBz1, 2, 1, 1, 1);
	DataGrid.Add (c_AgBz1);

	CCtrlInfo *c_AgBz2 = new CCtrlInfo (&m_AgBz2, 2, 2, 1, 1);
	DataGrid.Add (c_AgBz2);

	CCtrlInfo *c_LWg = new CCtrlInfo (&m_LWg, 1, 3, 1, 1);
	DataGrid.Add (c_LWg);

	CCtrlInfo *c_WgGrid = new CCtrlInfo (&WgGrid, 2, 3, 4, 1);
	DataGrid.Add (c_WgGrid);

	CCtrlInfo *c_LHwg = new CCtrlInfo (&m_LHwg, 1, 4, 1, 1);
	DataGrid.Add (c_LHwg);
	CCtrlInfo *c_HwgGrid = new CCtrlInfo (&HwgGrid, 2, 4, 4, 1);
	DataGrid.Add (c_HwgGrid);

	CCtrlInfo *c_LAbt = new CCtrlInfo (&m_LAbt, 1, 5, 1, 1);
	DataGrid.Add (c_LAbt);
	CCtrlInfo *c_Abt = new CCtrlInfo (&m_Abt, 2, 5, 1, 1);
	DataGrid.Add (c_Abt);

	CCtrlInfo *c_LTierKz = new CCtrlInfo (&m_LTierKz, 1, 6, 1, 1);
	DataGrid.Add (c_LTierKz);
	CCtrlInfo *c_TierKz = new CCtrlInfo (&m_TierKz, 2, 6, 1, 1);
	DataGrid.Add (c_TierKz);

	CCtrlInfo *c_LWeKto = new CCtrlInfo (&m_LWeKto, 1, 7, 1, 1);
	DataGrid.Add (c_LWeKto);
	CCtrlInfo *c_WeKto = new CCtrlInfo (&m_WeKto, 2, 7, 1, 1);
	DataGrid.Add (c_WeKto);
	CCtrlInfo *c_LErlKto = new CCtrlInfo (&m_LErlKto, 1, 8, 1, 1);
	DataGrid.Add (c_LErlKto);
	CCtrlInfo *c_ErlKto = new CCtrlInfo (&m_ErlKto, 2, 8, 1, 1);
	DataGrid.Add (c_ErlKto);
	CCtrlInfo *c_LSmt = new CCtrlInfo (&m_LSmt, 3, 7, 1, 1);
	DataGrid.Add (c_LSmt);
	CCtrlInfo *c_Smt = new CCtrlInfo (&m_Smt, 4, 7, 1, 1);
	DataGrid.Add (c_Smt);
	CCtrlInfo *c_LTeilSmt = new CCtrlInfo (&m_LTeilSmt, 3, 8, 1, 1);
	DataGrid.Add (c_LTeilSmt);
	CCtrlInfo *c_TeilSmt = new CCtrlInfo (&m_TeilSmt, 4, 8, 1, 1);
	DataGrid.Add (c_TeilSmt);

	CCtrlInfo *c_LMwst = new CCtrlInfo (&m_LMwst, 1, 10, 1, 1);
	DataGrid.Add (c_LMwst);
	CCtrlInfo *c_Mwst = new CCtrlInfo (&m_Mwst, 2, 10, 1, 1);
	DataGrid.Add (c_Mwst);
	CCtrlInfo *c_LMeEinh = new CCtrlInfo (&m_LMeEinh, 1, 11, 1, 1);
	DataGrid.Add (c_LMeEinh);
	CCtrlInfo *c_MeEinh = new CCtrlInfo (&m_MeEinh, 2, 11, 1, 1);
	DataGrid.Add (c_MeEinh);
	CCtrlInfo *c_LBestAuto = new CCtrlInfo (&m_LBestAuto, 1, 12, 1, 1);
	DataGrid.Add (c_LBestAuto);
	CCtrlInfo *c_BestAuto = new CCtrlInfo (&m_BestAuto, 2, 12, 1, 1);
	DataGrid.Add (c_BestAuto);
//	CCtrlInfo *c_LPersRab = new CCtrlInfo (&m_LPersRab, 3, 10, 1, 1);
//	DataGrid.Add (c_LPersRab);
//	CCtrlInfo *c_PersRab = new CCtrlInfo (&m_PersRab, 4, 10, 1, 1);
//	DataGrid.Add (c_PersRab);

	CCtrlInfo *c_LArtTyp = new CCtrlInfo (&m_LArtTyp, 3, 10, 1, 1);
	DataGrid.Add (c_LArtTyp);
	CCtrlInfo *c_ArtTyp = new CCtrlInfo (&m_ArtTyp, 4, 10, 1, 1);
	DataGrid.Add (c_ArtTyp);


	CCtrlInfo *c_LVerkArt = new CCtrlInfo (&m_LVerkArt, 3, 11, 1, 1);
	DataGrid.Add (c_LVerkArt);
	CCtrlInfo *c_VerkArt = new CCtrlInfo (&m_VerkArt, 4, 11, 1, 1);
	DataGrid.Add (c_VerkArt);
//	CCtrlInfo *c_LUmsVerb = new CCtrlInfo (&m_LUmsVerb, 3, 12, 1, 1);
//	DataGrid.Add (c_LUmsVerb);
//	CCtrlInfo *c_UmsVerb = new CCtrlInfo (&m_UmsVerb, 4, 12, 1, 1);
//	DataGrid.Add (c_UmsVerb);

	CCtrlInfo *c_LVerkBeg = new CCtrlInfo (&m_LVerkBeg, 1, 14, 1, 1);
	DataGrid.Add (c_LVerkBeg);
	CCtrlInfo *c_VerkBeg = new CCtrlInfo (&m_VerkBeg, 2, 14, 1, 1);
	DataGrid.Add (c_VerkBeg);
	CCtrlInfo *c_LVerkEnd = new CCtrlInfo (&m_LVerkEnd, 3, 14, 1, 1);
	DataGrid.Add (c_LVerkEnd);
	CCtrlInfo *c_VerkEnd = new CCtrlInfo (&m_VerkEnd, 4, 14, 1, 1);
	DataGrid.Add (c_VerkEnd);

	CCtrlInfo *c_CalcGroup = new CCtrlInfo (&m_CalcGroup, 0, 15, DOCKRIGHT, DOCKBOTTOM);
	c_CalcGroup->SetBottomControl (this, &m_Sw, 10, CCtrlInfo::Bottom);
	DataGrid.Add (c_CalcGroup);

	CCtrlInfo *c_LKostKz = new CCtrlInfo (&m_LKostKz, 1, 16, 1, 1);
	DataGrid.Add (c_LKostKz);
	CCtrlInfo *c_KostKz = new CCtrlInfo (&m_KostKz, 2, 16, 1, 1);
	DataGrid.Add (c_KostKz);

	CCtrlInfo *c_LSpKz = new CCtrlInfo (&m_LSpKz, 3, 16, 1, 1);
	DataGrid.Add (c_LSpKz);
	CCtrlInfo *c_SpKz = new CCtrlInfo (&m_SpKz, 4, 16, 1, 1);
	DataGrid.Add (c_SpKz);

    CalcGrid.Create (this, 20, 20);
    CalcGrid.SetBorder (0, 0);
	CalcGrid.SetCellHeight (15);
    CalcGrid.SetGridSpace (5, 15);  //Spaltenabstand und Zeilenabstand
	CCtrlInfo *c_LSpVk = new CCtrlInfo (&m_LSpVk, 1, 0, 1, 1);
	CalcGrid.Add (c_LSpVk);
	CCtrlInfo *c_SpVk = new CCtrlInfo (&m_SpVk, 2, 0, 1, 1);
	CalcGrid.Add (c_SpVk);
	CCtrlInfo *c_LSpFil = new CCtrlInfo (&m_LSpFil, 1, 1, 1, 1);
	CalcGrid.Add (c_LSpFil);
	m_SpFil.GetClientRect (&rect); 
	CCtrlInfo *c_SpFil = new CCtrlInfo (&m_SpFil, 2, 1, 1, 1);
//	c_SpFil->Name = _T("sp_fil");
	CalcGrid.Add (c_SpFil);
	CCtrlInfo *c_LSpSk = new CCtrlInfo (&m_LSpSk, 1, 2, 1, 1);
	CalcGrid.Add (c_LSpSk);
	CCtrlInfo *c_SpSk = new CCtrlInfo (&m_SpSk, 2, 2, 1, 1);
	CalcGrid.Add (c_SpSk);
	CCtrlInfo *c_LSw = new CCtrlInfo (&m_LSw, 1, 3, 1, 1);
	CalcGrid.Add (c_LSw);
	CCtrlInfo *c_Sw = new CCtrlInfo (&m_Sw, 2, 3, 1, 1);
	CalcGrid.Add (c_Sw);

	CCtrlInfo *c_LBearbLad = new CCtrlInfo (&m_LBearbLad, 3, 0, 1, 1);
	CalcGrid.Add (c_LBearbLad);
	CCtrlInfo *c_BearbLad = new CCtrlInfo (&m_BearbLad, 4, 0, 1, 1);
	CalcGrid.Add (c_BearbLad);
	CCtrlInfo *c_LBearbFil = new CCtrlInfo (&m_LBearbFil, 3, 1, 1, 1);
	CalcGrid.Add (c_LBearbFil);
	CCtrlInfo *c_BearbFil = new CCtrlInfo (&m_BearbFil, 4, 1, 1, 1);
	CalcGrid.Add (c_BearbFil);
	CCtrlInfo *c_LBearbSk = new CCtrlInfo (&m_LBearbSk, 3, 2, 1, 1);
	CalcGrid.Add (c_LBearbSk);
	CCtrlInfo *c_BearbSk = new CCtrlInfo (&m_BearbSk, 4, 2, 1, 1);
	CalcGrid.Add (c_BearbSk);

	CCtrlInfo *c_DurchVk = new CCtrlInfo (&m_DurchVk, 5, 0, 1, 1);
	c_DurchVk->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchVk);
	CCtrlInfo *c_DurchFil = new CCtrlInfo (&m_DurchFil, 5, 1, 1, 1);
	c_DurchFil->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchFil);
	CCtrlInfo *c_DurchSk = new CCtrlInfo (&m_DurchSk, 5, 2, 1, 1);
	c_DurchSk->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchSk);
	CCtrlInfo *c_DurchSw = new CCtrlInfo (&m_DurchSw, 5, 3, 1, 1);
	c_DurchSw->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchSw);


	CCtrlInfo *c_CalcGrid = new CCtrlInfo (&CalcGrid, 1, 18, 4, 1);
	DataGrid.Add (c_CalcGrid);

	CCtrlInfo *c_LAg = new CCtrlInfo (&m_LAg, 0, 0, 1, 1);
	CtrlGrid.Add (c_LAg);

	CCtrlInfo *c_AgGrid = new CCtrlInfo (&AgGrid, 1, 0, 1, 1);
	CtrlGrid.Add (c_AgGrid);

	CCtrlInfo *c_DataGrid = new CCtrlInfo (&DataGrid, 0, 2, DOCKRIGHT, 1);
	CtrlGrid.Add (c_DataGrid);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_SpFil.GetClientRect (&rect); 

	Form.Show ();
	CtrlGrid.Display ();
	EnableFields (FALSE);

	return ret;
}

HBRUSH CAgPage1::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	DlgBkColor = WINDOWHANDLER->GetDlgBkColor ();
	DlgBrush   = WINDOWHANDLER->GetDlgBrush ();
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
				WINDOWHANDLER->SetDlgBrush (DlgBrush);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
				WINDOWHANDLER->SetDlgBrush (DlgBrush);
			}
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CAgPage1::OnSize (UINT nType, int cx, int cy)
{
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
	CtrlGrid.Display ();
}

void CAgPage1::OnShowWindow (BOOL bShow, UINT nStatus)
{
	if (bShow)
	{
		if (CurrentFocus () == NULL)
		{
			set_CurrentFocus (&m_Ag);
		}
		CurrentFocus ()->SetFocus ();
	}
	else
	{
		set_CurrentFocus (GetFocus ());
	}
}

void CAgPage1::OnActivate (UINT nState, CWnd* pWndOther,  BOOL bMinimized) 
{
	if (nState == WA_ACTIVE || nState == WA_CLICKACTIVE)
	{
		if (CurrentFocus () == NULL)
		{
			set_CurrentFocus (&m_Ag);
		}
		CurrentFocus ()->SetFocus ();
	}
	else if (nState == WA_INACTIVE) 
	{
		set_CurrentFocus (GetFocus ());
	}
}

BOOL CAgPage1::OnSetActive( )
{
	if (CurrentFocus () == NULL)
	{
		if (IsWindow (m_Ag.m_hWnd))
		{
			set_CurrentFocus (&m_Ag);
			m_Ag.SetFocus ();
		}
	}
	else
	{
		if (IsWindow (CurrentFocus ()->m_hWnd))
		{
			CurrentFocus ()->SetFocus ();
		}
	}
	return TRUE;
}

BOOL CAgPage1::OnKillActive( )
{
	set_CurrentFocus (GetFocus ());
	return TRUE;
}

BOOL CAgPage1::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F5)
			{
				if (!StepBack ())
				{
					WINDOWHANDLER->DestroyFrame ();
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				OnChoice ();
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Ag)
				{
					OnChoice ();
					return TRUE;
				}
			}
			break;
	}
	return FALSE;
}

BOOL CAgPage1::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Ag)
	{
		if (!Read ())
		{
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CAgPage1::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

void CAgPage1::OnChoice ()
{
	if (Choice != NULL && WINDOWHANDLER->ChoiceAg () == NULL)
	{
		Choice = NULL;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceAG (this);
		Choice->IsModal = FALSE;
		Choice->SetBitmapButton (TRUE);
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->HideFilter = FALSE;
		Choice->HideOK    = HideOK;
//		Choice->SetDbClass (&DATABASE->Hwg);
		Choice->DlgBkColor = DlgBkColor;
	}
	WINDOWHANDLER->OnChoice (Choice, GetParent ());
}

void CAgPage1::OnSelected ()
{
	int ret = TRUE;
	BOOL CanRestore = FALSE;
	Choice = (CChoiceAG *) WINDOWHANDLER->GetChoice ();
	if (Choice == NULL) return;

	isNewRec = FALSE;
	Form.Get ();
	if (CanWrite ())
	{
		Write ();
		CanRestore = TRUE;
	}
    CAGList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
	CORE->SaveAg ();
	TAG.ag = abl->ag;
    Form.Show ();
	if (!DATABASE->ReadAg () && CanRestore)
	{
		if (DATABASE->GetSqlState () == CDatabaseCore::SqlLock)
		{
			AfxMessageBox (_T("Der Satz wird im Moment bearbeitet"), MB_OK | MB_ICONERROR);
		}
		CORE->RestoreAg ();
		Form.Show ();
		Read ();
	}
	Form.Show ();
	EnableFields (TRUE);
	if (Choice->FocusBack)
	{
		Choice->SetListFocus ();
	}
	m_CanWrite = TRUE;
	WINDOWHANDLER->set_CanWrite (m_CanWrite);
	WINDOWHANDLER->Update ();
	DisableTabControl (FALSE);
}

void CAgPage1::OnCanceled ()
{
	Choice = (CChoiceAG *) WINDOWHANDLER->GetChoice ();
	Choice->ShowWindow (SW_HIDE);
}

void CAgPage1::OnWgChoice ()
{
	m_TestFocus = FALSE;
	ChoiceWg = new CChoiceWg (); 
	ChoiceWg->CreateDlg ();
	ChoiceWg->SetBitmapButton (TRUE);
	ChoiceWg->IdArrDown = IDI_HARROWDOWN;
	ChoiceWg->IdArrUp   = IDI_HARROWUP;
	ChoiceWg->IdArrNo   = IDI_HARROWNO;
	ChoiceWg->DoModal ();
    if (ChoiceWg->GetState ())
    {
		CWgList *abl = ChoiceWg->GetSelectedText (); 
		if (abl == NULL) return;
		if (TWG_AG.wg != abl->wg)
		{
			TWG_AG.wg = abl->wg;
			CWG_AG.dbreadfirst ();
			GetWgDefaults ();
			THWG_AG.hwg = TWG_AG.hwg;
			CHWG_AG.dbreadfirst ();
		}
		Form.Show ();
		m_Wg.SetFocus ();
    }
	delete ChoiceWg;
	ChoiceWg = NULL;
	m_TestFocus = TRUE;
}


void CAgPage1::EnableFields (BOOL b)
{

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	m_WgChoice.EnableWindow (b);
	EnableHeadControls (!b);
}

void CAgPage1::EnableHeadControls (BOOL enable)
{
	HeadControls.Start ();
	CWnd **it;
	CWnd *Control;
	while ((it = HeadControls.GetNext ()) != NULL)
	{
		Control = *it;
		if (Control != NULL)
		{
			Control->EnableWindow (enable);
		}
	}
}

BOOL CAgPage1::StepBack ()
{
	CAgSheet *parent = (CAgSheet *) GetParent ();
	if (parent->IsWindowVisible ())
	{
		isNewRec = FALSE;
		if (!CanWrite ())
		{
			return FALSE;
		}
	//	DATABASE->Lief.rollbackwork ();
		EnableFields (FALSE);
		m_CanWrite = FALSE;
		m_Ag.SetFocus ();
		DisableTabControl (TRUE);
		parent->SetActivePage (0);
	}
	return TRUE;
}

void CAgPage1::DisableTabControl (BOOL disable)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (disable)
	{
		if (tab->IsWindowVisible ())
		{
			tab->ShowWindow (SW_HIDE);
		}
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}
}

BOOL CAgPage1::Read ()
{
	BOOL ret = TRUE;
	isNewRec = FALSE;
	CDatabaseCore::SQL_STATE sqlstate = CDatabaseCore::SqlOk;
	Form.Get ();
	if (!DATABASE->ReadAg ())
	{
		sqlstate = DATABASE->GetSqlState ();
	}
	if (sqlstate == CDatabaseCore::SqlLock)
	{
		AfxMessageBox (_T("Der Satz wird im Moment bearbeitet"), MB_OK | MB_ICONERROR);
		m_Ag.SetFocus ();
		ret = FALSE;
	}
	if  (sqlstate == CDatabaseCore::SqlNoData)
	{
		AfxMessageBox (_T("Neuer Satz"));
		isNewRec = TRUE;
	}
	if (ret)
	{
		Form.Show ();
		EnableFields (TRUE);
		DisableTabControl (FALSE);
		m_AgBz1.SetFocus ();
		m_CanWrite = TRUE;
		WINDOWHANDLER->set_CanWrite (m_CanWrite);
		WINDOWHANDLER->Update ();
	}
	return ret;
}

void CAgPage1::Write ()
{
/*
	CAgSheet *parent = (CAgSheet *) GetParent ();
	if (!parent->IsWindowVisible ())
	{
		return;
	}
*/
	CWnd *focus = GetFocus ();
	WINDOWHANDLER->Get ();
	Form.Get ();
	CORE->SetWgDateFields ();
	if (DATABASE->UpdateAg ())
	{
		EnableFields (FALSE);
		m_Ag.SetFocus ();
		m_CanWrite = FALSE;
		WINDOWHANDLER->set_CanWrite (m_CanWrite);
		focus = &m_Wg;
		Choice = (CChoiceAG *) WINDOWHANDLER->GetChoice ();
		if (Choice != NULL && isNewRec)
		{
			if (Choice->AGList.size () != 0) // nur dann neu laden, wenn schon geladen wurde
			{
				Choice->FillList ();
			}
			else
			{
				Choice = NULL;
			}
			
		}
		else if (Choice != NULL)
		{
			if (Choice->AGList.size () == 0)
			{
				Choice = NULL;
			}
		}
	}
	else
	{
		AfxMessageBox (_T("Satz kann nicht geschrieben werden"), MB_OK | MB_ICONERROR);
	}
	focus->SetFocus ();
	isNewRec = FALSE;
	DisableTabControl (TRUE);
}

void CAgPage1::Delete ()
{
	CWnd *focus = GetFocus ();
	if (AfxMessageBox (_T("Artikelgruppe l�schen ?"), MB_YESNO| MB_ICONQUESTION) != IDYES)
	{
		focus->SetFocus ();
		return;
	}
	Form.Get ();

	if (DATABASE->DeleteAg ())
	{
		EnableFields (FALSE);
		m_Wg.SetFocus ();
		m_CanWrite = FALSE;
		WINDOWHANDLER->set_CanWrite (m_CanWrite);
		focus = &m_Wg;
		Choice = (CChoiceAG *) WINDOWHANDLER->GetChoice ();
		if (Choice != NULL)
		{
			if (Choice->AGList.size () != 0) // nur dann neu laden, wenn schon geladen wurde
			{
				Choice->FillList ();
			}
		}
		DATABASE->InitAg ();
		Form.Show ();
		DisableTabControl (TRUE);
	}
	else
	{
		AfxMessageBox (_T("Satz kan nicht gel�scht werden"), MB_OK | MB_ICONERROR);
	}

	focus->SetFocus ();
}


void CAgPage1::UpdateWrite ()
{
	CAgSheet *parent = (CAgSheet *) GetParent ();
	if (!parent->IsWindowVisible ())
	{
		return;
	}
	Write ();
	parent->SetActivePage (0);
}

void CAgPage1::UpdateDelete ()
{
	CAgSheet *parent = (CAgSheet *) GetParent ();
	if (!parent->IsWindowVisible ())
	{
		Delete ();
		parent->SetActivePage (0);
	}
}

void CAgPage1::Update ()
{

	CWG_AG.dbreadfirst ();
	CHWG_AG.dbreadfirst ();
	Form.Show ();
}

void CAgPage1::OnEnKillfocusWg()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (m_TestFocus)
	{
		short wg = TWG_AG.wg;
		Form.Get ();
		if (wg != TWG_AG.wg)
		{
			if (CWG_AG.dbreadfirst () != 0)
			{
				AfxMessageBox (_T("Warengruppe nicht gefunden"), MB_OK | MB_ICONERROR);
				m_Wg.SetFocus ();
			}
			else
			{
				GetWgDefaults ();
				Form.Show ();
			}
		}
	}
}

void CAgPage1::GetWgDefaults ()
{
	BOOL getDefaults = TRUE;
	if (!isNewRec)
	{
		if (AfxMessageBox (_T("Grundwerte von Warengruppe �bernehmen ?"), MB_YESNO | MB_ICONQUESTION) != IDYES)
		{
			getDefaults = FALSE;
		}
	}
	if (getDefaults)
	{
		CORE->GetWgDefaults ();
		Form.Show ();
	}
}


