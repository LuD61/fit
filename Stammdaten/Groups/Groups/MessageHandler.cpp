#include "StdAfx.h"
#include "MessageHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CMessageHandler *CMessageHandler::Instance = NULL; 

CMessageHandler::CMessageHandler(void)
{
	StopMessages = FALSE;
}

CMessageHandler *CMessageHandler::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CMessageHandler ();
	}
	return Instance;
}

void CMessageHandler::DeleteInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
	}
}

void CMessageHandler::Register (CRunMessage *rm)
{
	MessageMap.Add (rm);
}

void CMessageHandler::Unregister (CRunMessage *rm)
{
	MessageMap.Drop (rm);
}

void CMessageHandler::RunMessage(DWORD Id)
{
	CRunMessage **it;
//	MessageMap.Start ();

//	while ((it = MessageMap.GetNext ()) != NULL)
	for (int i = 0; (it = MessageMap.Get (i)) != NULL; i ++)
	{
		CRunMessage *rm = *it;
		if (rm->GetId () == Id)
		{
			rm->Run ();
			if (Id == ID_APP_EXIT)
			{
				return;
			}
			if (i >= MessageMap.anz)
			{
				return;
			}
		}
	}
}

BOOL CMessageHandler::TestMessage(DWORD Id)
{
	BOOL ret = FALSE;
	CRunMessage **it;
	MessageMap.Start ();
	while ((it = MessageMap.GetNext ()) != NULL)
	{
		CRunMessage *rm = *it;
		if (rm->GetId () == Id)
		{
			ret = rm->Test ();
			if (ret)
			{
				break;
			}
		}
	}
	return ret;
}
