// GroupsView.cpp : Implementierung der Klasse CGroupsView
//

#include "stdafx.h"
#include "Groups.h"

#include "GroupsDoc.h"
#include "GroupsView.h"
#include "WindowHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGroupsView

IMPLEMENT_DYNCREATE(CGroupsView, CFormView)


// CGroupsView-Erstellung/Zerst�rung

CGroupsView::CGroupsView()
	: CFormView(CGroupsView::IDD)
{
	// TODO: Hier Code zur Konstruktion einf�gen
	FontHeight = 95;
	DlgBkColor = NULL;
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBkColor = RGB (168, 193, 218);
	DlgBkColor = RGB (235, 235, 245);
	DlgBrush = NULL;
	TitleBrush = NULL;
	HwgMessage = NULL;
	StartSize.cx = 0;
	StartSize.cy = 0;
	m_FocusHwg = NULL;
	m_FocusWg = NULL;
	m_FocusAg = NULL;
	agSet = FALSE;
}

CGroupsView::~CGroupsView()
{
	if (HwgMessage != NULL)
	{
		MessageHandler->Unregister (HwgMessage);
		delete HwgMessage;
	}
	if (WgMessage != NULL)
	{
		MessageHandler->Unregister (WgMessage);
		delete WgMessage;
	}
	if (AgMessage != NULL)
	{
		MessageHandler->Unregister (AgMessage);
		delete AgMessage;
	}
}

void CGroupsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TITLE, m_Title);
}

BEGIN_MESSAGE_MAP(CGroupsView, CFormView)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()

void CGroupsView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CSize sizeTotal;
	// TODO: Gesamte Gr��e dieser Ansicht berechnen
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);

	MessageHandler = CMessageHandler::GetInstance ();
	HwgMessage = new CHwgMessage (this);
	WgMessage = new CWgMessage (this);
	AgMessage = new CAgMessage (this);

	MessageHandler->Register (HwgMessage);
	MessageHandler->Register (WgMessage);
	MessageHandler->Register (AgMessage);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else if (GetSystemMetrics (SM_CXFULLSCREEN) <= 1300)
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (105, _T("Dlg"));
	}

	m_Title.SetBoderStyle (CLabel::Solide);
	m_Title.SetDynamicColor (TRUE);
//	m_AngebotTitle.SetBkColor (RGB (100,180,255));

	m_HwgPage.Create (IDD_HWG_PAGE, this);
	m_HwgPage.ShowWindow (SW_SHOWNORMAL);
	m_FocusHwg = &m_HwgPage.m_Hwg;

	m_WgPage.Create (IDD_WG_PAGE, this);
	m_WgPage.ShowWindow (SW_HIDE);
	m_FocusWg = &m_WgPage.m_Wg;

    m_AgProperty.m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
	m_AgProperty.AddPage (&m_AgPage1);
	m_AgProperty.AddPage (&m_AgPage2);
    m_AgProperty.m_psh.hwndParent = m_hWnd;
    m_AgProperty.m_psh.pszCaption = _T("Simple");
    m_AgProperty.m_psh.nStartPage = 0;

    m_AgProperty.m_psh.dwSize = sizeof (m_AgProperty.m_psh);
	m_AgProperty.Create (this, WS_CHILD);
	m_FocusAg = &m_AgPage1.m_Ag;

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 21;
	TitleFont.CreateFontIndirect (&l);

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (15);
//    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    HwgGrid.Create (this, 20, 20);
    HwgGrid.SetBorder (12, 20);
    HwgGrid.SetCellHeight (15);
    HwgGrid.SetFontCellHeight (this, &Font);
    HwgGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    WgGrid.Create (this, 20, 20);
    WgGrid.SetBorder (12, 20);
    WgGrid.SetCellHeight (15);
    WgGrid.SetFontCellHeight (this, &Font);
    WgGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    AgGrid.Create (this, 20, 20);
    AgGrid.SetBorder (12, 20);
    AgGrid.SetCellHeight (15);
    AgGrid.SetFontCellHeight (this, &Font);
    AgGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_HwgPage = new CCtrlInfo (&m_HwgPage, 0, 0, 1, 1);
	HwgGrid.Add (c_HwgPage);

	CCtrlInfo *c_WgPage = new CCtrlInfo (&m_WgPage, 0, 0, 1, 1);
	WgGrid.Add (c_WgPage);

	CCtrlInfo *c_AgProperty = new CCtrlInfo (&m_AgProperty, 0, 0, 1, 1);
	AgGrid.Add (c_AgProperty);

	CCtrlInfo *c_Title = new CCtrlInfo (&m_Title, 0, 0, DOCKRIGHT, 1);
	CtrlGrid.Add (c_Title);
	CCtrlInfo *c_HwgGrid = new CCtrlInfo (&HwgGrid, 1, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_HwgGrid);
	CCtrlInfo *c_WgGrid = new CCtrlInfo (&WgGrid, 1, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_WgGrid);
	CCtrlInfo *c_AgGrid = new CCtrlInfo (&AgGrid, 1, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_AgGrid);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_Title.SetFont (&TitleFont);
	m_Title.SetWindowText (_T("Hauptwarengruppen"));
	WINDOWHANDLER->SetBase (&m_HwgPage);

	CtrlGrid.Display ();
}

HBRUSH CGroupsView::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (pWnd == &m_Title)
	{
		pDC->SetTextColor (RGB (255,255,255));
//		pDC->SetBkColor (RGB (102,102,102));
		pDC->SetBkColor (RGB (100,180,255));
		if (TitleBrush == NULL)
		{
//			TitleBrush = CreateSolidBrush (RGB (102,102,102));
			TitleBrush = CreateSolidBrush (RGB (100,180,255));
		}
		return TitleBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CGroupsView::OnSize (UINT nType, int cx, int cy)
{
//	CFormView::OnSize (nType, cx, cy);
	if (StartSize.cx == 0)
	{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
	//	CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
		CtrlGrid.Display ();
		{
			StartSize.cx = cx;
			StartSize.cy = cy;
		}
	}
	SetScrollSizes (MM_TEXT, StartSize);
	if (!agSet && IsWindow (m_AgProperty.m_hWnd))
	{
		m_AgProperty.SetWindowPos (NULL, 0, 25, cx, cy, SWP_NOZORDER | SWP_NOSIZE);
		agSet = TRUE;
	}
}

void CGroupsView::OnHwg ()
{
	if (CORE->PageSelect () == CCore::ActiveHwg)
	{
		return;
	}
	switch (CORE->PageSelect ())
	{
	case CCore::ActiveWg :
		m_FocusWg = GetFocus ();
	case CCore::ActiveAg :
		m_FocusAg = GetFocus ();
	}
	m_HwgPage.ShowWindow (SW_SHOWNORMAL);
	m_WgPage.ShowWindow (SW_HIDE);
	m_AgProperty.ShowWindow (SW_HIDE);
	CORE->set_PageSelect (CCore::ActiveHwg);
	WINDOWHANDLER->DeleteActiveChoice ();
	WINDOWHANDLER->set_ActivePage (CWindowHandler::HwgPage);
	WINDOWHANDLER->NewSplit ();
	m_Title.SetWindowText (_T("Hauptwarengruppen"));
	if (m_FocusHwg != NULL && IsWindow (m_FocusHwg->m_hWnd))
	{
		PostMessage (WM_SETFOCUS, (WPARAM) m_FocusHwg->m_hWnd, 0l);
	}
	WINDOWHANDLER->SetBase (&m_HwgPage);
}

void CGroupsView::OnWg ()
{
	if (CORE->PageSelect () == CCore::ActiveWg)
	{
		return;
	}
	switch (CORE->PageSelect ())
	{
	case CCore::ActiveHwg :
		m_FocusHwg = GetFocus ();
	case CCore::ActiveAg :
		m_FocusAg = GetFocus ();
	}
	m_HwgPage.ShowWindow (SW_HIDE);
	m_WgPage.ShowWindow (SW_SHOWNORMAL);
	m_AgProperty.ShowWindow (SW_HIDE);
	CORE->set_PageSelect (CCore::ActiveWg);
	WINDOWHANDLER->DeleteActiveChoice ();
	WINDOWHANDLER->set_ActivePage (CWindowHandler::WgPage);
	WINDOWHANDLER->NewSplit ();
	m_Title.SetWindowText (_T("Warengruppen"));
	if (m_FocusWg != NULL && IsWindow (m_FocusWg->m_hWnd))
	{
		PostMessage (WM_SETFOCUS, (WPARAM) m_FocusWg->m_hWnd, 0l);
	}
	WINDOWHANDLER->SetBase (&m_WgPage);
}

void CGroupsView::OnAg ()
{
	if (CORE->PageSelect () == CCore::ActiveAg)
	{
		return;
	}

	switch (CORE->PageSelect ())
	{
	case CCore::ActiveHwg :
		m_FocusHwg = GetFocus ();
	case CCore::ActiveWg :
		m_FocusWg = GetFocus ();
	}

	m_HwgPage.ShowWindow (SW_HIDE);
	m_WgPage.ShowWindow (SW_HIDE);
	m_AgProperty.ShowWindow (SW_SHOWNORMAL);
	CORE->set_PageSelect (CCore::ActiveAg);
	WINDOWHANDLER->DeleteActiveChoice ();
	WINDOWHANDLER->set_ActivePage (CWindowHandler::AgPage);
	WINDOWHANDLER->NewSplit ();
	m_Title.SetWindowText (_T("Artikelgruppen"));
//	PostMessage (WM_SETFOCUS, (WPARAM) m_FocusAg->m_hWnd, 0l);
	if (m_AgProperty.GetActivePage () == &m_AgPage1)
	{
		m_AgPage1.set_CurrentFocus (m_FocusAg);
	}
/*
	else if (m_AgProperty.GetActivePage () == &m_AgPage2)
	{
		m_AgPage2.set_CurrentFocus (&m_AgFocus);
	}
*/
	m_AgProperty.PostMessage (WM_ACTIVATE, (WPARAM) WA_ACTIVE, (LPARAM) GetActiveWindow ());
//	WINDOWHANDLER->SetBase (&m_AgProperty);
	WINDOWHANDLER->SetBase (&m_AgPage1);
}

CGroupsView::CHwgMessage::CHwgMessage (CGroupsView *p) : CRunMessage ()
{
	        SetId (IDC_BHWG); 
			this->p = p;
}

void CGroupsView::CHwgMessage::Run ()
{
	p->OnHwg ();
}

CGroupsView::CWgMessage::CWgMessage (CGroupsView *p) : CRunMessage ()
{
	        SetId (IDC_BWG); 
			this->p = p;
}

void CGroupsView::CWgMessage::Run ()
{
	p->OnWg ();
}

CGroupsView::CAgMessage::CAgMessage (CGroupsView *p) : CRunMessage ()
{
	        SetId (IDC_BAG); 
			this->p = p;
}

void CGroupsView::CAgMessage::Run ()
{
	p->OnAg ();
}

// CGroupsView-Diagnose

#ifdef _DEBUG
void CGroupsView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CGroupsView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

/*
CGroupsDoc* CGroupsView::GetDocument() const 
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGroupsDoc)));
	return (CGroupsDoc*)m_pDocument;
}
*/
#endif //_DEBUG


// CGroupsView-Meldungshandler
