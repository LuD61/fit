#pragma once
#include "afxext.h"

class CSplitView :
		public CFormView
{
public:
	enum ORIENTATION
	{
		Vertical   = 0,
		Horizontal = 1,
	};
private:
	CString m_Name;
	ORIENTATION m_Orientation;
protected:
	CSplitView(int Id);

public:
	void set_Name (CString& name)
	{
		m_Name = name;
	}
	void set_Name (LPSTR name)
	{
		m_Name = name;
	}

	const LPTSTR Name ()
	{
		return m_Name.GetBuffer ();
	}

	void set_Orientation (ORIENTATION orientation)
	{
		m_Orientation = orientation;
	}



	~CSplitView ();
};
