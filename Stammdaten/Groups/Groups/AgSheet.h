#pragma once

class CAgSheet :
	public CPropertySheet
{
public:
	CAgSheet(void);
	CAgSheet(LPTSTR title);
	DECLARE_DYNCREATE(CAgSheet)
public:
	~CAgSheet(void);
private:
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog ();
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
	afx_msg void OnShowWindow (BOOL bShow, UINT nStatus);
	afx_msg void OnActivate (UINT nState, CWnd* pWndOther,  BOOL bMinimized); 
public:
};
