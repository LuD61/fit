#pragma once
#include "afxwin.h"

class CLabel :
	public CStatic
{
	DECLARE_DYNCREATE(CLabel)
public:
	enum
	{
		Center = 0,
		Left = 1,
		Right = 2,
	};
	enum
	{
		Standard = 0,
		Underline = 1,
		Bold = 2,
		UnderlineBold = 3,
	};
	enum
	{
		None,
		Solide,
		Dot,
	};
protected:
	virtual void OnSize (UINT, int, int);

	DECLARE_MESSAGE_MAP()

protected:
	CString Value;
	int Orientation;
	int TextStyle;
	int BorderStyle;
	COLORREF TextColor;
	COLORREF BkColor;
	BOOL DynamicColor;
	CBitmap Bitmap;
	CBitmap TransBitmap;
	HBITMAP hTransBitmap;
	CBitmap Mask;
	HBITMAP hMask;
	int parts;

public:
    void SetParts (int parts)
	{
		this->parts = parts;
	}

	int GetParts ()
	{
		return parts;
	}

	void SetBkColor (COLORREF BkColor)
	{
		this->BkColor = BkColor;
	}
	void SetTextColor (COLORREF TextColor)
	{
		this->TextColor = TextColor;
	}
	void SetBoderStyle (int BorderStyle)
	{
		this->BorderStyle = BorderStyle;
	}
    void SetDynamicColor (BOOL DynamicColor)
	{
		this->DynamicColor = DynamicColor;
	}

    void SetOrientation (int Orientation)
	{
		this->Orientation = Orientation;
	}

	CLabel(void);
public:
	~CLabel(void);
	void Properties (LPTSTR Label, int BorderStyle, int Orientation, 
		             BOOL DynamicColor, COLORREF TextColor,
					 COLORREF BkColor);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

protected:
	virtual void Draw (CDC& cDC);
	virtual void FillRectParts (CDC &cDC, COLORREF color, CRect rect);
};
