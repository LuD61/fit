#include "StdAfx.h"
#include "GroupSplitter.h"
#include "choiceview.h"
#include "GroupsView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CGroupSplitter, CSplitterWnd)

CGroupSplitter::CGroupSplitter(void) : CSplitterWnd ()
{
}

CGroupSplitter::~CGroupSplitter(void)
{
}

BEGIN_MESSAGE_MAP(CGroupSplitter, CSplitterWnd)
//	ON_WM_CTLCOLOR ()
	ON_WM_DESTROY ()
END_MESSAGE_MAP()

void  CGroupSplitter::OnDestroy ()
{
	Destroy ();
}

void  CGroupSplitter::Destroy ()
{
}

BOOL CGroupSplitter::CreateView( int row,int col,CRuntimeClass* pViewClass,SIZE sizeInit,CCreateContext* pContext)
{
// Hier kann eine andere pViewClass als CRuntimeClass eingesetzte werden RUNTIME_CLASS (COwnViw)
//	return CSplitterWnd::CreateView (row, col, pViewClass, sizeInit, pContext);

	if (col == Choice)
	{
		return CSplitterWnd::CreateView (row, col, RUNTIME_CLASS (CChoiceView), sizeInit, pContext);
	}
	else if (row == Choice)
	{
		return CSplitterWnd::CreateView (row, col, RUNTIME_CLASS (CChoiceView), sizeInit, pContext);
	}
	else
	{
		return CSplitterWnd::CreateView (row, col, RUNTIME_CLASS (CGroupsView), sizeInit, pContext);
	}

}

HBRUSH CGroupSplitter::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CSplitterWnd::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CGroupSplitter::OnDrawSplitter(CDC*cDC, CSplitterWnd::ESplitType type, const CRect& rect)
{
	CSplitterWnd::OnDrawSplitter (cDC, type, rect);
}

void CGroupSplitter::OnInvertTracker(const CRect& rect)
{
	CSplitterWnd::OnInvertTracker(rect);
}
