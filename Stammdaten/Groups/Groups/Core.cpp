#include "StdAfx.h"
#include "Core.h"
#include "Process.h"
#include "DatabaseCore.h"
#include "DbTime.h"
#include "DbUnicode.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CCore *CCore::Instance = NULL; 

CCore::CCore(void)
{
	m_PageSelect = ActiveHwg;
}

CCore::~CCore(void)
{
}

CCore *CCore::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CCore ();
	}
	return Instance;
}

void CCore::DeleteInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
	}
}

BOOL CCore::StartFaProg (CString& Name)
{
	CFavoriteItem *Item;
	CFavoriteItem **it;
	if (Stammdaten != NULL)
	{
		Stammdaten->Start ();
		while ((it = Stammdaten->GetNext ()) != NULL)
		{
			Item = *it;
			if (Name == Item->GetName ())
			{
				CString Progname = Item->GetProgrammCall ();
				return StartProg (Progname);
			}
		}
	}

	if (Warenausgang != NULL)
	{
		Warenausgang->Start ();
		while ((it = Warenausgang->GetNext ()) != NULL)
		{
			Item = *it;
			if (Name == Item->GetName ())
			{
				CString Progname = Item->GetProgrammCall ();
				return StartProg (Progname);
			}
		}
	}
    return FALSE;
}

BOOL CCore::StartProg (CString& Name)
{
	CProcess p;

	p.SetCommand (Name.GetBuffer ());
	HANDLE Pid = p.Start ();
	if (Pid != NULL)
	{
		return TRUE;
	}
	return FALSE;
}

void CCore::SaveHwg ()
{
	m_Hwg = THWG.hwg;
}

void CCore::RestoreHwg ()
{
	 THWG.hwg = m_Hwg;
}

void CCore::SetHwgDateFields ()
{
	THWG.bearb = (DATE_STRUCT) DbTime ();
	if (DbTime::IsNull (THWG.akv))
	{
		THWG.akv = (DATE_STRUCT) DbTime ();
	}
	
}

void CCore::SaveWg ()
{
	m_Wg = TWG.wg;
}

void CCore::RestoreWg ()
{
	 TWG.wg = m_Wg;
}

void CCore::SetWgDateFields ()
{
/*
	TWG.bearb = (DATE_STRUCT) DbTime ();
	if (DbTime::IsNull (TWG.akv))
	{
		TWG.akv = (DATE_STRUCT) DbTime ();
	}
*/	
}

void CCore::SaveAg ()
{
	m_Ag = TAG.ag;
}

void CCore::RestoreAg ()
{
	 TAG.ag = m_Ag;
}

void CCore::SetAgDateFields ()
{
/*
	TAG.bearb = (DATE_STRUCT) DbTime ();
	if (DbTime::IsNull (TAG.akv))
	{
		TAG.akv = (DATE_STRUCT) DbTime ();
	}
*/	
}

void CCore::GetHwgDefaults ()
{
	TWG.mwst = THWG_WG.mwst;
	TWG.me_einh = THWG_WG.me_einh;
	_tcscpy (TWG.best_auto, THWG_WG.best_auto);
	TWG.pers_rab = THWG_WG.pers_rab;
	_tcscpy (TWG.smt, THWG_WG.smt);
	TWG.teil_smt = THWG_WG.teil_smt;
	_tcscpy (TWG.sp_kz, THWG_WG.sp_kz);
	_tcscpy (TWG.kost_kz, THWG_WG.kost_kz);
	TWG.sp_fil = THWG_WG.sp_fil;
	TWG.sp_sk = THWG_WG.sp_sk;
	TWG.sp_vk = THWG_WG.sp_vk;
	TWG.bearb_fil = THWG_WG.bearb_fil;
	TWG.bearb_sk = THWG_WG.bearb_sk;
	TWG.bearb_lad = THWG_WG.bearb_lad;
	TWG.sw = THWG_WG.sw;
	_tcscpy (TWG.durch_fil, THWG_WG.durch_fil);
	_tcscpy (TWG.durch_sk, THWG_WG.durch_sk);
	_tcscpy (TWG.durch_vk, THWG_WG.durch_vk);
	_tcscpy (TWG.durch_sw, THWG_WG.durch_sw);
}

void CCore::GetWgDefaults ()
{
	TAG.mwst = TWG_AG.mwst;
	TAG.me_einh = TWG_AG.me_einh;
	_tcscpy (TAG.best_auto, TWG_AG.best_auto);
//	TAG.pers_rab = TWG_AG.pers_rab;
	_tcscpy (TAG.smt, TWG_AG.smt);
	TAG.teil_smt = TWG_AG.teil_smt;
	_tcscpy (TAG.sp_kz, TWG_AG.sp_kz);
	_tcscpy (TAG.kost_kz, TWG_AG.kost_kz);
	TAG.sp_fil = TWG_AG.sp_fil;
	TAG.sp_sk = TWG_AG.sp_sk;
	TAG.sp_vk = TWG_AG.sp_vk;
	TAG.bearb_fil = TWG_AG.bearb_fil;
	TAG.bearb_sk = TWG_AG.bearb_sk;
	TAG.bearb_lad = TWG_AG.bearb_lad;
	TAG.sw = TWG_AG.sw;
	_tcscpy (TAG.durch_fil, TWG_AG.durch_fil);
	_tcscpy (TAG.durch_sk, TWG_AG.durch_sk);
	_tcscpy (TAG.durch_vk, TWG_AG.durch_vk);
	_tcscpy (TAG.durch_sw, TWG_AG.durch_sw);
}

void CCore::FillCombo (LPTSTR Item, std::vector<CString *> *ComboValues)
{
		CString Sql;
		ComboValues->clear ();
		CPTABN.sqlout ((char *) TPTABN.ptitem, SQLCHAR, sizeof (TPTABN.ptitem)); 
		CPTABN.sqlout ((char *) TPTABN.ptwert, SQLCHAR, sizeof (TPTABN.ptwert)); 
		CPTABN.sqlout ((long *) &TPTABN.ptlfnr, SQLLONG, 0); 
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = CPTABN.sqlcursor (Sql.GetBuffer ());
		while (CPTABN.sqlfetch (cursor) == 0)
		{
			CPTABN.dbreadfirst ();

			LPSTR pos = (LPSTR) TPTABN.ptwert;
			CDbUniCode::DbToUniCode (TPTABN.ptwert, pos);
			pos = (LPSTR) TPTABN.ptbez;
			CDbUniCode::DbToUniCode (TPTABN.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), TPTABN.ptwert,
				                             TPTABN.ptbez);
		    ComboValues->push_back (ComboValue); 
		}
		CPTABN.sqlclose (cursor);
}

void CCore::AppendCombo (LPTSTR Item, std::vector<CString *> *ComboValues)
{
		CString Sql;
		CPTABN.sqlout ((char *) TPTABN.ptitem, SQLCHAR, sizeof (TPTABN.ptitem)); 
		CPTABN.sqlout ((char *) TPTABN.ptwert, SQLCHAR, sizeof (TPTABN.ptwert)); 
		CPTABN.sqlout ((long *) &TPTABN.ptlfnr, SQLLONG, 0); 
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = CPTABN.sqlcursor (Sql.GetBuffer ());
		while (CPTABN.sqlfetch (cursor) == 0)
		{
			CPTABN.dbreadfirst ();

			LPSTR pos = (LPSTR) TPTABN.ptwert;
			CDbUniCode::DbToUniCode (TPTABN.ptwert, pos);
			pos = (LPSTR) TPTABN.ptbez;
			CDbUniCode::DbToUniCode (TPTABN.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), TPTABN.ptwert,
				                             TPTABN.ptbez);
		    ComboValues->push_back (ComboValue); 
		}
		CPTABN.sqlclose (cursor);
}
