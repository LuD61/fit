#pragma once

class CRunMessage
{
private:
	DWORD Id;
public:
	void SetId (DWORD Id)
	{
		this->Id = Id;
	}
	DWORD GetId ()
	{
		return Id;
	}
public:
	CRunMessage(void);
	~CRunMessage(void);
	virtual void Run () = 0; 
	virtual BOOL Test () 
	{
		return FALSE;
	}
};
