// FavoriteView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Groups.h"
#include "FavoriteView.h"
#include "Core.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFavoriteView

IMPLEMENT_DYNCREATE(CFavoriteView, CFormView)

CFavoriteView::CFavoriteView()
	: CFormView(CFavoriteView::IDD)
{
	FontHeight = 95;
//	DlgBkColor = RGB (51, 51, 51);
//	DlgBkColor = RGB (153, 153, 153);
//	DlgBkColor = RGB (204, 204, 204);
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
	TitleBrush = NULL;

}

CFavoriteView::~CFavoriteView()
{
}

void CFavoriteView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FAVORITE_TITLE, m_FavoriteTitle);
	DDX_Control(pDX, IDC_FAVORITE_TREE, m_FavoriteTree);
}

BEGIN_MESSAGE_MAP(CFavoriteView, CFormView)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_NOTIFY(NM_DBLCLK, IDC_FAVORITE_TREE, &CFavoriteView::OnNMDblclkFavoriteTree)
END_MESSAGE_MAP()


void CFavoriteView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (FontHeight, _T("Dlg"));
	}

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 18;
	TitleFont.CreateFontIndirect (&l);
	m_FavoriteTitle.SetBoderStyle (CLabel::Solide);
	m_FavoriteTitle.SetWindowText (_T(" Favoriten"));
	m_FavoriteTitle.SetTextColor (RGB (0,0,0));
	m_FavoriteTitle.SetBkColor (RGB (194,194,194));
	m_FavoriteTree.Init ();
    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetGridSpace (0, 0);  //Spaltenabstand und Zeilenabstand
	CCtrlInfo *c_FavoriteTitle = new CCtrlInfo (&m_FavoriteTitle, 0, 0, DOCKRIGHT, 1);
	CtrlGrid.Add (c_FavoriteTitle);

	CCtrlInfo *c_FavoriteTree = new CCtrlInfo (&m_FavoriteTree, 0, 2, DOCKRIGHT, DOCKBOTTOM);
	c_FavoriteTree->SetCellPos (2, 0);
	CtrlGrid.Add (c_FavoriteTree);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_FavoriteTitle.SetFont (&TitleFont);

	CtrlGrid.Display ();
}

HBRUSH CFavoriteView::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
//			m_MoreKto.SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (pWnd == &m_FavoriteTitle)
	{
		pDC->SetTextColor (RGB (0,0,0));
		pDC->SetBkColor (RGB (194,194,194));
		if (TitleBrush == NULL)
		{
			TitleBrush = CreateSolidBrush (RGB (194,194,194));
		}
		return TitleBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CFavoriteView::OnSize (UINT nType, int cx, int cy)
{
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
	CtrlGrid.Display ();
}

// CFavoriteView-Diagnose

#ifdef _DEBUG
void CFavoriteView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CFavoriteView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CFavoriteView-Meldungshandler

void CFavoriteView::OnNMDblclkFavoriteTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;
	HTREEITEM ht = m_FavoriteTree.GetSelectedItem ();
	CString Text = m_FavoriteTree.GetItemText (ht);
	CORE->StartFaProg (Text);
}
