#include "StdAfx.h"
#include "SplitFrame.h"
#include "GroupsView.h"
#include "ChoiceView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CSplitFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CSplitFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE ()
END_MESSAGE_MAP()

CSplitFrame::CSplitFrame(void)
{
	sizeOK = FALSE;
	m_GroupSplitter = NULL;
}

CSplitFrame::~CSplitFrame(void)
{
	if (m_GroupSplitter != NULL)
	{
		delete m_GroupSplitter;
	}
}

int CSplitFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
	{
		return -1;
	}
	return 0;
}

BOOL CSplitFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return TRUE;
}

BOOL CSplitFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	BOOL  ret = TRUE;

	m_GroupSplitter = new CGroupSplitter ();
	ret = m_GroupSplitter->Create (this, 2, 2, CSize (100, 100), pContext, WS_CHILD | WS_VISIBLE | SPLS_DYNAMIC_SPLIT);
	WINDOWHANDLER->set_GroupSplitter (m_GroupSplitter);

	return ret;
}

void CSplitFrame::OnSize (UINT nType, int cx, int cy)
{
	CFrameWnd::OnSize (nType, cx, cy);
	if (!sizeOK && m_GroupSplitter != NULL && IsWindow (m_GroupSplitter->m_hWnd))
	{
		if (WINDOWHANDLER->DockChoice ())
		{
			m_GroupSplitter->SetColumnInfo (0, cx - 450, 10);
			m_GroupSplitter->SetColumnInfo (1, 450, 0);
		}
		sizeOK = TRUE;
	}
}
