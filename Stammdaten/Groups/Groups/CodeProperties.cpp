#include "StdAfx.h"
#include "codeproperties.h"
#include "codeproperty.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CCodeProperties::CCodeProperties(void)
{
}

CCodeProperties::~CCodeProperties(void)
{
/*
	CCodeProperty  *cp;
	FirstPosition ();
	while ((cp = (CCodeProperty  *) GetNext ()) != NULL)
	{
		delete p;
	}
	Init ();
*/
	DestroyAll ();
}

void CCodeProperties::Load (CString& CodeFile)
{
	FILE *fp;
	TCHAR buffer [512];

	fp = _tfopen (CodeFile.GetBuffer (), _T("r"));
	if (fp == NULL) return;
    while (_fgetts (buffer, 511, fp) != NULL)
	{
		if (buffer[0] == _T('#')) continue;
		CCodeProperty  *cp = new CCodeProperty (buffer);
		Add (cp);
	}
}

short CCodeProperties::GetSource (short target)
{
	CCodeProperty  *cp;
	FirstPosition ();
	while ((cp = (CCodeProperty  *) GetNext ()) != NULL)
	{
		short source = cp->GetSource (target);
		if (source != 0) return source;
	}
	return 0;
}

short CCodeProperties::GetTarget (short source)
{
	CCodeProperty  *cp;
	FirstPosition ();
	while ((cp = (CCodeProperty  *) GetNext ()) != NULL)
	{
		short target = cp->GetTarget (source);
		if (target != 0) 
		{
			return target;
		}
	}
	return 0;
}
