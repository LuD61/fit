#pragma once
#include "DataCollection.h"
#include "RunMessage.h"

#define ID_DATA_BACK 10001 

class CMessageHandler
{
private:
	static CMessageHandler *Instance;
	CDataCollection<CRunMessage *> MessageMap; 
	BOOL StopMessages;

protected:
	CMessageHandler(void);
public:
	static CMessageHandler *GetInstance ();
	static void DeleteInstance ();
	void Register (CRunMessage *rm);
	void Unregister (CRunMessage *rm);
	void RunMessage (DWORD Id);
	BOOL TestMessage (DWORD Id);
};

#define MESSAGEHANDLER CMessageHandler::GetInstance ()
