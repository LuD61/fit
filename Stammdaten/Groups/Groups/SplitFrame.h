#ifndef __SPLIT_FRAME_DEF
#define __SPLIT_FRAME_DEF
#pragma once
#include "afxwin.h"
#include "WindowHandler.h"
#include "Splitter.h"
#include "MainSplitter.h"

class CSplitFrame :
	public CFrameWnd
{
public:
	CSplitFrame(void);
	DECLARE_DYNCREATE(CSplitFrame)
	~CSplitFrame(void);
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg BOOL OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext);
	DECLARE_MESSAGE_MAP()
	virtual void OnSize (UINT nType, int cx, int cy);
//	virtual void OnDestroy ();
private:
	CGroupSplitter *m_GroupSplitter;
	BOOL sizeOK;
};
#endif