#pragma once
#include "afxext.h"
#include "MainMenu.h"

class CMainSplitter :
	public CSplitterWnd
{
private:
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
public:
	CMainSplitter(void);
	DECLARE_DYNCREATE(CMainSplitter)
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnDestroy ();
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
public:
	~CMainSplitter(void);
	void Destroy ();
	virtual BOOL CreateView( int row,int col,CRuntimeClass* pViewClass,SIZE sizeInit,CCreateContext* pContext);
};
