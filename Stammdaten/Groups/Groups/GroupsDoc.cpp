// GroupsDoc.cpp : Implementierung der Klasse CGroupsDoc
//

#include "stdafx.h"
#include "Groups.h"

#include "GroupsDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGroupsDoc

IMPLEMENT_DYNCREATE(CGroupsDoc, CDocument)

BEGIN_MESSAGE_MAP(CGroupsDoc, CDocument)
END_MESSAGE_MAP()


// CGroupsDoc-Erstellung/Zerst�rung

CGroupsDoc::CGroupsDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CGroupsDoc::~CGroupsDoc()
{
}

BOOL CGroupsDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CGroupsDoc-Serialisierung

void CGroupsDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CGroupsDoc-Diagnose

#ifdef _DEBUG
void CGroupsDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGroupsDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CGroupsDoc-Befehle
