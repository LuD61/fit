#include "StdAfx.h"
#include "controls.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CControls::CControls(void)
{
}

CControls::~CControls(void)
{
}

void CControls::SetVisible (BOOL v)
{
	CWnd *Control;
	FirstPosition ();
	while ((Control = (CWnd *) GetNext ()) != NULL)
	{
		if (v)
		{
			Control->ShowWindow (SW_SHOWNORMAL);
		}
		else
		{
			Control->ShowWindow (SW_HIDE);
		}
	}
}

void CControls::Enable (BOOL b)
{
	CWnd *Control;
	FirstPosition ();
	while ((Control = (CWnd *) GetNext ()) != NULL)
	{
		Control->EnableWindow (b);
	}
}
