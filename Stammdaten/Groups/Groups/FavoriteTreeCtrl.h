#pragma once
#include "afxcmn.h"
#include "DataCollection.h"
#include "FavoriteItem.h"
//#include "AngebotCore.h"

class CFavoriteTreeCtrl :
	public CTreeCtrl
{
	DECLARE_DYNCREATE(CFavoriteTreeCtrl)

private:
	CDataCollection<CFavoriteItem *> Stammdaten;
	CDataCollection<CFavoriteItem *> Warenausgang;
	CImageList ImageList;
public:
	CFavoriteTreeCtrl(void);
public:
	~CFavoriteTreeCtrl(void);
    void Init ();
    HTREEITEM AddRootItem (LPSTR Text, int idx, int Childs);
    HTREEITEM AddChildItem (HTREEITEM hParent,  LPSTR Text, int idx, int Childs, 
                                  int iImage, DWORD state);
	void SetTreeStyle (DWORD style);
	static BOOL ShowWa;
};
