#pragma once

class CStartViewInt
{
public:
	CStartViewInt(void);
	~CStartViewInt(void);
	virtual void SetNewSplitView (BOOL docked) = 0;
};
