#pragma once
#include "DataCollection.h"
#include "FavoriteItem.h"
#include "vector"

class CCore
{
private:
	static CCore *Instance;
	CDataCollection<CFavoriteItem *> *Stammdaten;
	CDataCollection<CFavoriteItem *> *Warenausgang;
protected:
	CCore(void);
	~CCore(void);
public:
	static CCore *GetInstance ();
	static void DeleteInstance ();
public:
	enum ACTIVE_PAGE
	{
		ActiveHwg,
		ActiveWg,
		ActiveAg,
	};

    CDataCollection<CFavoriteItem *> *GetStammdaten ()
	{
		return Stammdaten;
	}

	void SetStammdaten (CDataCollection<CFavoriteItem *> *Stammdaten)
	{
		this->Stammdaten = Stammdaten;
	}

    CDataCollection<CFavoriteItem *> *GetWarenausgang ()
	{
		return Warenausgang;
	}

	void SetWarenausgang (CDataCollection<CFavoriteItem *> *Warenausgang)
	{
		this->Warenausgang = Warenausgang;
	}

	BOOL StartFaProg (CString& Name);
	BOOL StartProg (CString& Programm);
private:
	short m_Hwg;
	short m_Wg;
	long m_Ag;
	ACTIVE_PAGE m_PageSelect;
public:
	void set_PageSelect (ACTIVE_PAGE pageSelect)
	{
		m_PageSelect = pageSelect;
	}

	ACTIVE_PAGE PageSelect ()
	{
		return m_PageSelect;
	}
	void SaveHwg ();
	void RestoreHwg ();
	void SetHwgDateFields ();

	void SaveWg ();
	void RestoreWg ();
	void SetWgDateFields ();

	void SaveAg ();
	void RestoreAg ();
	void SetAgDateFields ();

	void GetHwgDefaults ();
	void GetWgDefaults ();

	void FillCombo (LPTSTR Item, std::vector<CString *> *ComboValues);
	void AppendCombo (LPTSTR Item, std::vector<CString *> *ComboValues);

};

#define CORE CCore::GetInstance ()