#include "StdAfx.h"
#include "WindowHandler.h"
#include "Splitter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CWindowHandler *CWindowHandler::Instance = NULL;

CWindowHandler::CWindowHandler(void)
{
	m_Base = NULL;
	m_MainFrame = NULL;
	m_ChoiceIsCreated = FALSE;
	m_StartWithDock = FALSE;
	m_CloseFromMemory = FALSE;
	m_DlgBkColor = RGB (235, 235, 245);
	m_DlgBrush = NULL;
    m_SplitDir = Horizontal;
	m_Sheet = NULL;
	m_ViewType = MainView;
	m_MenVisible = FALSE;
	m_DockChoice = FALSE;
    m_StartView = NULL;
	m_GroupSplitter = NULL;
	m_ActivePage = HwgPage;
	m_ChoiceHwg = NULL;
	m_ChoiceWg = NULL;
	m_ChoiceAg = NULL;
	m_ChoiceDockable = TRUE;
	m_Menu = NULL;
	m_CanWrite = FALSE;
	m_SplitSave = NoSplit;
}

CWindowHandler::~CWindowHandler(void)
{
	if (m_ChoiceHwg != NULL)
	{
		delete m_ChoiceHwg;
	}
	if (m_ChoiceWg != NULL)
	{
		delete m_ChoiceWg;
	}
	if (m_ChoiceAg != NULL)
	{
		delete m_ChoiceAg;
	}
}

CWindowHandler *CWindowHandler::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CWindowHandler ();
	}
	return Instance;
}

void CWindowHandler::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

void CWindowHandler::RegisterUpdate (CUpdateHandler *UpdateHandler)
{
	UpdateTable.push_back (UpdateHandler);
}

void CWindowHandler::UnregisterUpdate (CUpdateHandler *UpdateHandler)
{
	CUpdateHandler *u;

	for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
	{
		u = *it;
		if (u == UpdateHandler)
		{
			UpdateTable.erase (it);
		}
	}
}

void CWindowHandler::Update ()
{
	CUpdateHandler *UpdateHandler;

	for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
	{
		UpdateHandler = *it;
		UpdateHandler->Update ();
	}
}

void CWindowHandler::Get ()
{
	CUpdateHandler *UpdateHandler;

	for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
	{
		UpdateHandler = *it;
		UpdateHandler->Get ();
	}
}

void CWindowHandler::UpdateWrite ()
{
	CUpdateHandler *UpdateHandler;

	for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
	{
		UpdateHandler = *it;
		UpdateHandler->UpdateWrite ();
	}
}


BOOL CWindowHandler::IsDockPlace (int x, int y)
{
	BOOL ret = FALSE;
	CChoiceX *choice = NULL;

	switch (ActivePage ())
	{
	case HwgPage:
		choice = m_ChoiceHwg;
		break;
	case WgPage:
		choice = m_ChoiceWg;
		break;
	case AgPage:
		choice = m_ChoiceAg;
		break;
	}

	if (m_ChoiceDockable && m_ChoiceIsCreated && m_GroupSplitter != NULL && choice != NULL)
	{
		CRect rect;
		CRect cRect;
		if (m_GroupSplitter->GetColumnCount () < 2)
		{
			m_GroupSplitter->GetWindowRect (&rect);
			choice->GetClientRect (&cRect);
			if (y >= rect.top + 20 && y <= rect.top + 25)
			{
				if (x > rect.right - 400)
				{
					choice->DestroyWindow ();
					m_GroupSplitter->GetClientRect (&rect);
					m_GroupSplitter->SplitColumn (rect.right - cRect.right);
					m_DockChoice = TRUE;
					m_SplitDir = Horizontal;
					choice->SetParent (m_Base);
					ret = TRUE;
				}
			}
		}

		return IsVerticaleDockPlace (x, y);
	}

	return ret;
}

void CWindowHandler::SplitChoice ()
{
	CRect rect;
	CRect cRect;
	CChoiceX *choice = NULL;

	switch (ActivePage ())
	{
	case HwgPage:
		choice = m_ChoiceHwg;
		break;
	case WgPage:
		choice = m_ChoiceWg;
		break;
	case AgPage:
		choice = m_ChoiceAg;
		break;
	}

	if (!m_DockChoice && choice != NULL)
	{
		choice->DestroyWindow ();
		delete choice;
		switch (ActivePage ())
		{
		case HwgPage:
			m_ChoiceHwg = NULL;
			break;
		case WgPage:
			m_ChoiceWg = NULL;
			break;
		case AgPage:
			m_ChoiceAg = NULL;
			break;
		}
	}
	if (m_DockChoice && m_SplitDir == Vertical)
	{
		DeleteBottomChoice ();
	}
	if (m_GroupSplitter->GetColumnCount () < 2)
	{
		m_GroupSplitter->GetClientRect (&rect);
		m_GroupSplitter->SplitColumn (rect.right - 400);
		switch (ActivePage ())
		{
		case HwgPage:
			choice = m_ChoiceHwg;
			break;
		case WgPage:
			choice = m_ChoiceWg;
			break;
		case AgPage:
			choice = m_ChoiceAg;
			break;
		}

		m_DockChoice = TRUE;
		m_SplitDir = Horizontal;
		choice->DlgBkColor = m_DlgBkColor;
		choice->SetParent (m_Base);
	}
}

void CWindowHandler::SplitBottomChoice ()
{
	CRect rect;
	CRect cRect;
	CChoiceX *choice = NULL;

	switch (ActivePage ())
	{
	case HwgPage:
		choice = m_ChoiceHwg;
		break;
	case WgPage:
		choice = m_ChoiceWg;
		break;
	case AgPage:
		choice = m_ChoiceAg;
		break;
	}
	if (!m_DockChoice && choice != NULL)
	{
		choice->DestroyWindow ();
		delete choice;
		switch (ActivePage ())
		{
		case HwgPage:
			m_ChoiceHwg = NULL;
			break;
		case WgPage:
			m_ChoiceWg = NULL;
			break;
		case AgPage:
			m_ChoiceAg = NULL;
			break;
		}
	}
	if (m_DockChoice && m_SplitDir == Horizontal)
	{
		DeleteChoice ();
	}
	if (m_GroupSplitter->GetRowCount () < 2)
	{
		m_GroupSplitter->GetClientRect (&rect);
		m_GroupSplitter->SplitRow (rect.bottom - 400);
		switch (ActivePage ())
		{
		case HwgPage:
			choice = m_ChoiceHwg;
			break;
		case WgPage:
			choice = m_ChoiceWg;
			break;
		case AgPage:
			choice = m_ChoiceAg;
			break;
		}
		m_DockChoice = TRUE;
		m_SplitDir = Vertical;
		choice->DlgBkColor = m_DlgBkColor;
		choice->SetParent (m_Base);
	}
}

void CWindowHandler::DeleteChoice ()
{
	CChoiceX *choice = NULL;

	switch (ActivePage ())
	{
	case HwgPage:
		choice = m_ChoiceHwg;
		break;
	case WgPage:
		choice = m_ChoiceWg;
		break;
	case AgPage:
		choice = m_ChoiceAg;
		break;
	}
	if (m_GroupSplitter->GetColumnCount () > Choice)
	{
		m_GroupSplitter->DeleteColumn (Choice);
	}
	if (choice != NULL)
	{
		choice->DestroyWindow ();
		delete choice;
		switch (ActivePage ())
		{
		case HwgPage:
			m_ChoiceHwg = NULL;
			break;
		case WgPage:
			m_ChoiceWg = NULL;
			break;
		case AgPage:
			m_ChoiceAg = NULL;
			break;
		}
		m_DockChoice = FALSE;
	}
	m_DockChoice = FALSE;
}

void CWindowHandler::DeleteBottomChoice ()
{
	CChoiceX *choice = NULL;

	switch (ActivePage ())
	{
	case HwgPage:
		choice = m_ChoiceHwg;
		break;
	case WgPage:
		choice = m_ChoiceWg;
		break;
	case AgPage:
		choice = m_ChoiceAg;
		break;
	}
	if (m_GroupSplitter->GetRowCount () > Choice)
	{
		m_GroupSplitter->DeleteRow (Choice);
	}
	if (m_ChoiceHwg != NULL)
	{
		choice->DestroyWindow ();
		delete choice;
		switch (ActivePage ())
		{
		case HwgPage:
			m_ChoiceHwg = NULL;
			break;
		case WgPage:
			m_ChoiceWg = NULL;
			break;
		case AgPage:
			m_ChoiceAg = NULL;
			break;
		}
		m_DockChoice = FALSE;
	}
	m_DockChoice = FALSE;
}

void CWindowHandler::DeleteActiveChoice ()
{

	CChoiceX *choice = NULL;

	switch (ActivePage ())
	{
	case HwgPage:
		choice = m_ChoiceHwg;
		break;
	case WgPage:
		choice = m_ChoiceWg;
		break;
	case AgPage:
		choice = m_ChoiceAg;
		break;
	}
	m_SplitSave = NoSplit;
	if (!m_DockChoice && choice != NULL)
	{
		choice->DestroyWindow ();
		delete choice;
		switch (ActivePage ())
		{
		case HwgPage:
			m_ChoiceHwg = NULL;
			break;
		case WgPage:
			m_ChoiceWg = NULL;
			break;
		case AgPage:
			m_ChoiceAg = NULL;
			break;
		}
	}

	if (m_GroupSplitter->GetRowCount () > Choice)
	{
		m_SplitSave = Vertical;
	}

	else if (m_GroupSplitter->GetColumnCount () > Choice)
	{
		m_SplitSave = Horizontal;
	}

	if (m_DockChoice && m_SplitDir == Horizontal)
	{
		DeleteChoice ();
	}

	else if (m_DockChoice && m_SplitDir == Vertical)
	{
		DeleteBottomChoice ();
	}
}

void CWindowHandler::NewSplit ()
{
	switch (SplitSave ())
	{
	case Horizontal :
		SplitChoice ();
		break;
	case Vertical :
		SplitBottomChoice ();
		break;
	}
}

void CWindowHandler::OnChoice (CChoiceX *Choice, CWnd *Parent)
{
	CRect sRect;

    if ((m_StartWithDock || m_DockChoice) && m_GroupSplitter != NULL)
	{
		switch (ActivePage ())
		{
		case HwgPage:
			m_ChoiceHwg = (CChoiceHwg *) Choice;
			break;
		case WgPage:
			m_ChoiceWg = (CChoiceWg *) Choice;
			break;
		case AgPage:
			m_ChoiceAg = (CChoiceAG *) Choice;
			break;
		}
		if (!m_DockChoice)
		{
			m_GroupSplitter->GetClientRect (&sRect);
			m_GroupSplitter->SplitColumn (sRect.right - 400);
		}
		m_DockChoice = TRUE;
		m_SplitDir = Horizontal;
		return;
	}
	else
	{
		if (ActiveChoice () != NULL && m_ChoiceIsCreated)
		{
			Choice->ShowWindow (SW_SHOWNORMAL);
		}
		else
		{
			m_ChoiceIsCreated = FALSE;
			switch (ActivePage ())
			{
			case HwgPage:
				m_ChoiceHwg = (CChoiceHwg *) Choice;
				break;
			case WgPage:
				m_ChoiceWg = (CChoiceWg *) Choice;
				break;
			case AgPage:
				m_ChoiceAg = (CChoiceAG *) Choice;
				break;
			}
			Choice->CreateDlg ();
			CRect mrect;
			Parent->GetWindowRect (&mrect);
			CRect rect;
			Choice->GetWindowRect (&rect);
			int scx = GetSystemMetrics (SM_CXSCREEN);
			int scy = GetSystemMetrics (SM_CYSCREEN);
			rect.top = 50;
			rect.right = scx - 2;
			rect.left = rect.right - 300;
			rect.bottom = scy - 50;
			Choice->MoveWindow (&rect);
			m_ChoiceIsCreated = TRUE;
		}
		Choice->SetListFocus ();
	}
}

void CWindowHandler::OnDeleteChoice ()
{
	CChoiceX *choice = NULL;

	switch (ActivePage ())
	{
	case HwgPage:
		choice = m_ChoiceHwg;
		break;
	case WgPage:
		choice = m_ChoiceWg;
		break;
	case AgPage:
		choice = m_ChoiceAg;
		break;
	}
    if (m_DockChoice && m_GroupSplitter != NULL)
	{
		if (m_SplitDir == Horizontal)
		{
			m_GroupSplitter->DeleteColumn (CGroupSplitter::Choice);
		}
		else if (m_SplitDir == Vertical)
		{
			m_GroupSplitter->DeleteRow (Choice);
		}

		if (choice != NULL)
		{
			choice->DestroyWindow ();
			delete choice;
			switch (ActivePage ())
			{
			case HwgPage:
				m_ChoiceHwg = NULL;
				break;
			case WgPage:
				m_ChoiceWg = NULL;
				break;
			case AgPage:
				m_ChoiceAg = NULL;
				break;
			}
			m_DockChoice = FALSE;
		}
	}
	if (m_Menu != NULL)
	{
		m_Menu->CheckMenuItem (ID_DOCK_CHOICE, MF_UNCHECKED);
		m_Menu->CheckMenuItem (ID_DOCK_BOTTOM, MF_UNCHECKED);
	}

}


BOOL CWindowHandler::IsVerticaleDockPlace (int x, int y)
{
	BOOL ret = FALSE;
	CChoiceX *choice = NULL;

	switch (ActivePage ())
	{
	case HwgPage:
		choice = m_ChoiceHwg;
		break;
	case WgPage:
		choice = m_ChoiceWg;
		break;
	case AgPage:
		choice = m_ChoiceAg;
		break;
	}

	if (m_ChoiceDockable && m_ChoiceIsCreated && m_GroupSplitter != NULL && choice != NULL)
	{
		CRect rect;
		CRect cRect;

		if (m_GroupSplitter->GetRowCount () < 2)
		{
			m_GroupSplitter->GetWindowRect (&rect);
			choice->GetClientRect (&cRect);
			if (x >= rect.left && x <= rect.left + 25)
			{
				if (y > rect.bottom - 300)
				{
					choice->DestroyWindow ();
					m_GroupSplitter->GetClientRect (&rect);
					m_GroupSplitter->SplitRow (rect.bottom - 300);
					m_DockChoice = TRUE;
					m_SplitDir = Vertical;
					choice->SetParent (m_Base);
					ret = TRUE;
				}
			}
		}
	}

	return ret;
}


void CWindowHandler::OnBack ()
{
	CUpdateHandler *UpdateHandler;
	for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
	{
		UpdateHandler = *it;
		if (!UpdateHandler->StepBack ())
		{
			 DestroyFrame ();
			 return;
		}
	}
}

void CWindowHandler::OnWrite ()
{
	CUpdateHandler *UpdateHandler;
	for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
	{
		UpdateHandler = *it;
		UpdateHandler->UpdateWrite ();
	}
}

void CWindowHandler::OnDelete ()
{
	CUpdateHandler *UpdateHandler;
	for (vector<CUpdateHandler *>::iterator it = UpdateTable.begin (); it != UpdateTable.end (); ++it)
	{
		UpdateHandler = *it;
		UpdateHandler->UpdateDelete ();
	}
}

void CWindowHandler::DestroyFrame ()
{
	if (m_MainFrame != NULL)
	{
		m_CloseFromMemory = TRUE;
		m_MainFrame->PostMessage (WM_SYSCOMMAND, SC_CLOSE, 0);
	}
}

CChoiceX *CWindowHandler::GetChoice ()
{
	if (m_ActivePage == HwgPage)
	{
		if (m_ChoiceHwg == NULL)
		{
			m_ChoiceHwg = new CChoiceHwg ();
		}
		return m_ChoiceHwg;
	}
	else if (m_ActivePage == WgPage)
	{
		if (m_ChoiceWg == NULL)
		{
			m_ChoiceWg = new CChoiceWg ();
		}
		return m_ChoiceWg;
	}
	else if (m_ActivePage == AgPage)
	{
		if (m_ChoiceAg == NULL)
		{
			m_ChoiceAg = new CChoiceAG ();
		}
		return m_ChoiceAg;
	}
	return NULL;
}

CChoiceX *CWindowHandler::ActiveChoice ()
{
	CChoiceX *choice = NULL;

	switch (ActivePage ())
	{
	case HwgPage:
		choice = m_ChoiceHwg;
		break;
	case WgPage:
		choice = m_ChoiceWg;
		break;
	case AgPage:
		choice = m_ChoiceAg;
		break;
	}
	return choice;
}

void CWindowHandler::UpdateChoice ()
{
	CChoiceX *choice = NULL;

	switch (ActivePage ())
	{
	case HwgPage:
		choice = m_ChoiceHwg;
		break;
	case WgPage:
		choice = m_ChoiceWg;
		break;
	case AgPage:
		choice = m_ChoiceAg;
		break;
	}
    if (choice != NULL)
	{
		choice->SetParent (m_Base);
	}
}
