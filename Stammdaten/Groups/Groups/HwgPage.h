#pragma once
#include "afxwin.h"
#include "StaticButton.h"
#include "ColorButton.h"
#include "Label.h"
#include "VLabel.h"
#include "RunMessage.h"
#include "MessageHandler.h"
#include "CtrlGridColor.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "ChoiceHwg.h"
#include "UpdateHandler.h"

#define IDC_HWGCHOICE 3001

// CHwgPage-Dialogfeld

class CHwgPage : public CDialog,
                        CUpdateHandler     
{
	DECLARE_DYNAMIC(CHwgPage)

public:
	CHwgPage(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CHwgPage();

// Dialogfelddaten
	enum { IDD = IDD_HWG_PAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
	virtual void OnSize (UINT, int, int);

	void OnChoice ();
    void OnSelected ();
    void OnCanceled ();

	DECLARE_MESSAGE_MAP()
private:
	CMessageHandler *MessageHandler;
	CFont Font;
	CFont TitleFont;
	int FontHeight;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	BOOL MustCreate;
	BOOL IsCreated;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid HeadGrid;
	CCtrlGrid HwgGrid;
	CCtrlGrid DataGrid;
	CCtrlGrid CalcGrid;
	CChoiceHwg *Choice;
	CFormTab Form;
	BOOL HideOK;
	BOOL m_CanWrite;
	BOOL isNewRec;
	CDataCollection<CWnd *> HeadControls;

public:
	BOOL CanWrite ()
	{
		return m_CanWrite;
	}
	CColorButton m_HwgChoice;
	CStatic m_HwgGroup;
	CStatic m_CalcGroup;
	void EnableFields (BOOL b);
	void EnableHeadControls (BOOL b);
	virtual BOOL StepBack ();
	virtual void Update ();
	virtual void UpdateWrite ();
	virtual void UpdateDelete ();
	void FillPtabCombo (CWnd *Control, LPTSTR Item);
    BOOL OnReturn ();
    BOOL OnKeyup ();
	BOOL Read ();
	void Write ();
	void Delete ();

	CStatic m_LHwg;
	CNumEdit m_Hwg;
	CStatic m_LHwgBz;
	CTextEdit m_HwgBz1;
	CTextEdit m_HwgBz2;
	CStatic m_LWeKto;
	CNumEdit m_WeKto;
	CStatic m_LErlKto;
	CNumEdit m_ErlKto;
	CStatic m_LShopSort;
	CNumEdit m_ShopSort;
	CStatic m_LSmt;
	CComboBox m_Smt;
	CStatic m_LTeilSmt;
	CComboBox m_TeilSmt;
	CStatic m_LMwst;
	CComboBox m_Mwst;
	CStatic m_LMeEinh;
	CComboBox m_MeEinh;
	CStatic m_LBestAuto;
	CComboBox m_BestAuto;
	CStatic m_LPersRab;
	CDecimalEdit m_PersRab;
	CButton m_SamEan;
	CStatic m_LKostKz;
	CComboBox m_KostKz;
	CStatic m_LSpKz;
	CComboBox m_SpKz;
	CStatic m_LSpVk;
	CDecimalEdit m_SpVk;
	CStatic m_LSpFil;
	CDecimalEdit m_SpFil;
	CStatic m_LSpSk;
	CDecimalEdit m_SpSk;
	CStatic m_LSw;
	CDecimalEdit m_Sw;
	CStatic m_LBearbLad;
	CNumEdit m_BearbLad;
	CStatic m_LBearbFil;
	CNumEdit m_BearbFil;
	CStatic m_LBearbSk;
	CNumEdit m_BearbSk;
	CButton m_DurchVk;
	CButton m_DurchFil;
	CButton m_DurchSk;
	CButton m_DurchSw;
	
};
