// AgPage2.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Groups.h"
#include "AgPage2.h"
#include "Groups.h"
#include "AgPage1.h"
#include "WindowHandler.h"
#include "Core.h"
#include "DatabaseCore.h"
#include "uniformfield.h"


// CAgPage2-Dialogfeld

IMPLEMENT_DYNAMIC(CAgPage2, CPropertyPage)

CAgPage2::CAgPage2()
	: CPropertyPage(CAgPage2::IDD)
{
	HideOK = FALSE;
	DlgBkColor = NULL;
	DlgBrush = NULL;
	m_CanWrite = FALSE;
	m_TestFocus = TRUE;
	m_CurrentFocus = NULL;
//	WINDOWHANDLER->SetBase (this);
	WINDOWHANDLER->RegisterUpdate (this);
}

CAgPage2::~CAgPage2()
{
	WINDOWHANDLER->SetBase (NULL);
	WINDOWHANDLER->UnregisterUpdate (this);
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
}

void CAgPage2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LAG, m_LAg);
	DDX_Control(pDX, IDC_AG, m_Ag);
	DDX_Control(pDX, IDC_AG_GROUP, m_AgGroup);
	DDX_Control(pDX, IDC_LAG_BZ, m_LAgBz);
	DDX_Control(pDX, IDC_AG_BZ1, m_AgBz1);
	DDX_Control(pDX, IDC_AG_BZ2, m_AgBz2);
	DDX_Control(pDX, IDC_LWG, m_LWg);
	DDX_Control(pDX, IDC_WG, m_Wg);
	DDX_Control(pDX, IDC_WG_BZ1, m_WgBz1);
	DDX_Control(pDX, IDC_LHWG, m_LHwg);
	DDX_Control(pDX, IDC_HWG, m_Hwg);
	DDX_Control(pDX, IDC_HWG_BZ1, m_HwgBz1);
	DDX_Control(pDX, IDC_LABT, m_LAbt);
	DDX_Control(pDX, IDC_ABT, m_Abt);
	DDX_Control(pDX, IDC_BSD_KZ, m_BsdKz);
	DDX_Control(pDX, IDC_LBEST_AUTO, m_LBestAuto);
	DDX_Control(pDX, IDC_BEST_AUTO, m_BestAuto);
	DDX_Control(pDX, IDC_PFA, m_Pfa);
	DDX_Control(pDX, IDC_LSTK_LST_KZ, m_LStkLstKz);
	DDX_Control(pDX, IDC_STK_LST_KZ, m_StkLstKz);
	DDX_Control(pDX, IDC_LHND_GEW, m_LHndGew);
	DDX_Control(pDX, IDC_HND_GEW, m_HndGew);
	DDX_Control(pDX, IDC_LSG1, m_LSg1);
	DDX_Control(pDX, IDC_SG1, m_Sg1);
	DDX_Control(pDX, IDC_SPIN_SG1, m_SpinSg1);
	DDX_Control(pDX, IDC_LSG2, m_LSg2);
	DDX_Control(pDX, IDC_SG2, m_Sg2);
	DDX_Control(pDX, IDC_SPIN_SG2, m_SpinSg2);
	DDX_Control(pDX, IDC_LWAREN_ETI, m_LWarenEti);
	DDX_Control(pDX, IDC_WAREN_ETI, m_WarenEti);
	DDX_Control(pDX, IDC_LREG_ETI, m_LRegEti);
	DDX_Control(pDX, IDC_REG_ETI, m_RegEti);
	DDX_Control(pDX, IDC_LTHEKE_ETI, m_LThekeEti);
	DDX_Control(pDX, IDC_THEKE_ETI, m_ThekeEti);
	DDX_Control(pDX, IDC_LSAIS1, m_LSais1);
	DDX_Control(pDX, IDC_LSAIS2, m_LSais2);
	DDX_Control(pDX, IDC_SAIS1, m_Sais1);
	DDX_Control(pDX, IDC_SAIS2, m_Sais2);
	DDX_Control(pDX, IDC_LMHD_KZ, m_LMhdKz);
	DDX_Control(pDX, IDC_MHD_KZ, m_MhdKz);
	DDX_Control(pDX, IDC_LMHD_ZTR, m_LMhdZtr);
	DDX_Control(pDX, IDC_MHD_ZTR, m_MhdZtr);
	DDX_Control(pDX, IDC_SPIN_MHD_ZTR, m_SpinMhdZtr);
	DDX_Control(pDX, IDC_PR_UEB, m_PrUeb);
	DDX_Control(pDX, IDC_LTARA, m_LTara);
	DDX_Control(pDX, IDC_TARA, m_Tara);
}


BEGIN_MESSAGE_MAP(CAgPage2, CPropertyPage)
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()

BOOL CAgPage2::OnInitDialog ()
{
	BOOL ret = CPropertyPage::OnInitDialog ();

#ifdef BLUE_COL
	COLORREF BkColor (RGB (100, 180, 255));
	COLORREF DynColor = m_NewAngTyp.DynColorBlue;
	COLORREF RolloverColor = RGB (204, 204, 255);
#else
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
#endif

// Notlösung, weil der Basisfont nicht dem erwartetem Font entspricht, dadurch werden beim Setzen eines neuen Fonts im Grid
// die Controlgrößen verfälscht.

	CFont *f = GetParent ()->GetParent ()->GetFont ();
	m_BsdKz.SetFont (f);
	CWnd *item = this->GetNextDlgTabItem (&m_Ag);
	while (item != &m_BsdKz)
	{
		item->SetFont (f);
		item = this->GetNextDlgTabItem (item);
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else if (GetSystemMetrics (SM_CXFULLSCREEN) <= 1300)
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (105, _T("Dlg"));
	}

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 17;
	TitleFont.CreateFontIndirect (&l);

    Form.Add (new CFormField (&m_Ag,EDIT,         (long *) &TAG.ag, VLONG));
    Form.Add (new CUniFormField (&m_AgBz1,EDIT,   (char *)  TAG.ag_bz1, VCHAR, sizeof (TAG.ag_bz1)));
    Form.Add (new CUniFormField (&m_AgBz2,EDIT,   (char *)  TAG.ag_bz2, VCHAR, sizeof (TAG.ag_bz2)));
    Form.Add (new CFormField (&m_Wg,EDIT,         (short *)  &TWG_AG.wg, VSHORT));
    Form.Add (new CUniFormField (&m_WgBz1,EDIT,   (char *)  TWG_AG.wg_bz1, VCHAR, sizeof (TWG.wg_bz1)));
    Form.Add (new CFormField (&m_Hwg,EDIT,        (short *) &THWG_AG.hwg, VSHORT));
    Form.Add (new CUniFormField (&m_HwgBz1,EDIT,  (char *)  THWG_AG.hwg_bz1, VCHAR, sizeof (THWG.hwg_bz1)));
    Form.Add (new CFormField (&m_Abt,EDIT,        (short *) &TAG.abt, VSHORT, 4));
    Form.Add (new CFormField (&m_BsdKz,CHECKBOX,  (char *)  TAG.bsd_kz, VCHAR));

    Form.Add (new CFormField (&m_BestAuto,COMBOBOX, (char *)  TAG.best_auto, VCHAR));
	CORE->FillCombo (_T("best_auto"), &Form.GetFormField (&m_BestAuto)->ComboValues);
	Form.GetFormField (&m_BestAuto)->FillComboBox ();
	m_BestAuto.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_Pfa,CHECKBOX,   (char *)  TAG.pfa, VCHAR));

    Form.Add (new CFormField (&m_StkLstKz,COMBOBOX, (char *)  TAG.stk_lst_kz, VCHAR));
	CORE->FillCombo (_T("stk_lst_kz"), &Form.GetFormField (&m_StkLstKz)->ComboValues);
	Form.GetFormField (&m_StkLstKz)->FillComboBox ();
	m_StkLstKz.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_HndGew,COMBOBOX, (char *)  TAG.hnd_gew, VCHAR));
	CORE->FillCombo (_T("hnd_gew"), &Form.GetFormField (&m_HndGew)->ComboValues);
	Form.GetFormField (&m_HndGew)->FillComboBox ();
	m_HndGew.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_Sg1,EDIT,        (short *) &TAG.sg1, VSHORT));
    Form.Add (new CFormField (&m_Sg2,EDIT,        (short *) &TAG.sg2, VSHORT));

    Form.Add (new CFormField (&m_WarenEti,COMBOBOX, (char *)  TAG.waren_eti, VCHAR));
	CORE->FillCombo (_T("waren_eti"), &Form.GetFormField (&m_WarenEti)->ComboValues);
	Form.GetFormField (&m_WarenEti)->FillComboBox ();
	m_WarenEti.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_RegEti,COMBOBOX, (char *)  TAG.reg_eti, VCHAR));
	CORE->FillCombo (_T("reg_eti"), &Form.GetFormField (&m_RegEti)->ComboValues);
	Form.GetFormField (&m_RegEti)->FillComboBox ();
	m_RegEti.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_ThekeEti,COMBOBOX, (char *)  TAG.theke_eti, VCHAR));
	CORE->FillCombo (_T("theke_eti"), &Form.GetFormField (&m_ThekeEti)->ComboValues);
	Form.GetFormField (&m_ThekeEti)->FillComboBox ();
	m_ThekeEti.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_Sais1,EDIT,        (short *) &TAG.sais1, VSHORT, 2));
    Form.Add (new CFormField (&m_Sais2,EDIT,        (short *) &TAG.sais2, VSHORT, 2));

    Form.Add (new CFormField (&m_MhdKz,COMBOBOX, (char *)  TAG.hbk_kz, VCHAR));
	CORE->FillCombo (_T("hbk_kz"), &Form.GetFormField (&m_MhdKz)->ComboValues);
	Form.GetFormField (&m_MhdKz)->FillComboBox ();
	m_MhdKz.SetDroppedWidth (300);
    Form.Add (new CFormField (&m_MhdZtr,EDIT, (short *)  &TAG.hbk_ztr, VSHORT));
    Form.Add (new CFormField (&m_PrUeb,CHECKBOX, (char *)  TAG.pr_ueb, VCHAR));
    Form.Add (new CFormField (&m_Tara, EDIT, (long *)  &TAG.tara,   VLONG));

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (10, 0);
	CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//Grid Wgnummer

	AgGrid.Create (this, 2, 2);
    AgGrid.SetBorder (0, 0);
    AgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Ag = new CCtrlInfo (&m_Ag, 0, 0, 1, 1);
	AgGrid.Add (c_Ag);
//	CtrlGrid.CreateChoiceButton (m_AgChoice, IDC_AGCHOICE, this);
//	CCtrlInfo *c_AgChoice = new CCtrlInfo (&m_AgChoice, 1, 0, 1, 1);
//	AgGrid.Add (c_AgChoice);

//Grid Wg

	WgGrid.Create (this, 3, 3);
    WgGrid.SetBorder (0, 0);
    WgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Wg = new CCtrlInfo (&m_Wg, 0, 0, 1, 1);
	WgGrid.Add (c_Wg);
//	CtrlGrid.CreateChoiceButton (m_WgChoice, IDC_WGCHOICE, this);
//	CCtrlInfo *c_WgChoice = new CCtrlInfo (&m_WgChoice, 1, 0, 1, 1);
//	WgGrid.Add (c_WgChoice);
	CCtrlInfo *c_WgBz1 = new CCtrlInfo (&m_WgBz1, 2, 0, 1, 1);
	c_WgBz1->SetCellPos (10, 0);
	WgGrid.Add (c_WgBz1);

//Grid Hwg

	HwgGrid.Create (this, 3, 3);
    HwgGrid.SetBorder (0, 0);
    HwgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Hwg = new CCtrlInfo (&m_Hwg, 0, 0, 1, 1);
	HwgGrid.Add (c_Hwg);
//	CtrlGrid.CreateChoiceButton (m_WgChoice, IDC_WGCHOICE, this);
//	CCtrlInfo *c_WgChoice = new CCtrlInfo (&m_WgChoice, 1, 0, 1, 1);
//	HwgGrid.Add (c_WgChoice);
	CCtrlInfo *c_HwgBz1 = new CCtrlInfo (&m_HwgBz1, 1, 0, 1, 1);
	c_HwgBz1->SetCellPos (10, 0);
	HwgGrid.Add (c_HwgBz1);

    DataGrid.Create (this, 20, 20);
    DataGrid.SetBorder (0, 0);
	DataGrid.SetCellHeight (15);
    DataGrid.SetGridSpace (5, 15);  //Spaltenabstand und Zeilenabstand
	CCtrlInfo *c_AgGroup = new CCtrlInfo (&m_AgGroup, 0, 0, DOCKRIGHT, DOCKBOTTOM);
	c_AgGroup->SetBottomControl (this, &m_Tara, 10, CCtrlInfo::Bottom);
	DataGrid.Add (c_AgGroup);
	CCtrlInfo *c_LAgBz = new CCtrlInfo (&m_LAgBz, 1, 1, 1, 1);
	DataGrid.Add (c_LAgBz);

	CCtrlInfo *c_AgBz1 = new CCtrlInfo (&m_AgBz1, 2, 1, 1, 1);
	DataGrid.Add (c_AgBz1);

	CCtrlInfo *c_AgBz2 = new CCtrlInfo (&m_AgBz2, 2, 2, 1, 1);
	DataGrid.Add (c_AgBz2);

	CCtrlInfo *c_LWg = new CCtrlInfo (&m_LWg, 1, 3, 1, 1);
	DataGrid.Add (c_LWg);

	CCtrlInfo *c_WgGrid = new CCtrlInfo (&WgGrid, 2, 3, 4, 1);
	DataGrid.Add (c_WgGrid);

	CCtrlInfo *c_LHwg = new CCtrlInfo (&m_LHwg, 1, 4, 1, 1);
	DataGrid.Add (c_LHwg);
	CCtrlInfo *c_HwgGrid = new CCtrlInfo (&HwgGrid, 2, 4, 4, 1);
	DataGrid.Add (c_HwgGrid);

	CCtrlInfo *c_LAbt = new CCtrlInfo (&m_LAbt, 1, 5, 1, 1);
	DataGrid.Add (c_LAbt);
	CCtrlInfo *c_Abt = new CCtrlInfo (&m_Abt, 2, 5, 1, 1);
	DataGrid.Add (c_Abt);

	CCtrlInfo *c_BsdKz = new CCtrlInfo (&m_BsdKz, 1, 7, 1, 1);
	DataGrid.Add (c_BsdKz);

	CCtrlInfo *c_LBestAuto = new CCtrlInfo (&m_LBestAuto, 1, 8, 1, 1);
	DataGrid.Add (c_LBestAuto);
	CCtrlInfo *c_BestAuto = new CCtrlInfo (&m_BestAuto, 2, 8, 1, 1);
	DataGrid.Add (c_BestAuto);

	CCtrlInfo *c_Pfa = new CCtrlInfo (&m_Pfa, 1, 9, 1, 1);
	DataGrid.Add (c_Pfa);

	CCtrlInfo *c_LStkLstKz = new CCtrlInfo (&m_LStkLstKz, 1, 10, 1, 1);
	DataGrid.Add (c_LStkLstKz);
	CCtrlInfo *c_StkLstKz = new CCtrlInfo (&m_StkLstKz, 2, 10, 1, 1);
	DataGrid.Add (c_StkLstKz);

	CCtrlInfo *c_LHndGew = new CCtrlInfo (&m_LHndGew, 1, 11, 1, 1);
	DataGrid.Add (c_LHndGew);
	CCtrlInfo *c_HndGew = new CCtrlInfo (&m_HndGew, 2, 11, 1, 1);
	DataGrid.Add (c_HndGew);

	CCtrlInfo *c_LSg1 = new CCtrlInfo (&m_LSg1, 3, 7, 1, 1);
	DataGrid.Add (c_LSg1);
	CCtrlInfo *c_Sg1 = new CCtrlInfo (&m_Sg1, 4, 7, 1, 1);
	DataGrid.Add (c_Sg1);

	CCtrlInfo *c_LSg2 = new CCtrlInfo (&m_LSg2, 3, 8, 1, 1);
	DataGrid.Add (c_LSg2);
	CCtrlInfo *c_Sg2 = new CCtrlInfo (&m_Sg2, 4, 8, 1, 1);
	DataGrid.Add (c_Sg2);

	CCtrlInfo *c_LWarenEti = new CCtrlInfo (&m_LWarenEti, 3, 9, 1, 1);
	DataGrid.Add (c_LWarenEti);
	CCtrlInfo *c_WarenEti = new CCtrlInfo (&m_WarenEti, 4, 9, 1, 1);
	DataGrid.Add (c_WarenEti);

	CCtrlInfo *c_LRegEti = new CCtrlInfo (&m_LRegEti, 3, 10, 1, 1);
	DataGrid.Add (c_LRegEti);
	CCtrlInfo *c_RegEti = new CCtrlInfo (&m_RegEti, 4, 10, 1, 1);
	DataGrid.Add (c_RegEti);

	CCtrlInfo *c_LThekeEti = new CCtrlInfo (&m_LThekeEti, 3, 11, 1, 1);
	DataGrid.Add (c_LThekeEti);
	CCtrlInfo *c_ThekeEti = new CCtrlInfo (&m_ThekeEti, 4, 11, 1, 1);
	DataGrid.Add (c_ThekeEti);

	CCtrlInfo *c_LSais1 = new CCtrlInfo (&m_LSais1, 1, 13, 1, 1);
	DataGrid.Add (c_LSais1);
	CCtrlInfo *c_Sais1 = new CCtrlInfo (&m_Sais1, 2, 13, 1, 1);
	DataGrid.Add (c_Sais1);

	CCtrlInfo *c_LSais2 = new CCtrlInfo (&m_LSais2, 1, 14, 1, 1);
	DataGrid.Add (c_LSais2);
	CCtrlInfo *c_Sais2 = new CCtrlInfo (&m_Sais2, 2, 14, 1, 1);
	DataGrid.Add (c_Sais2);

	CCtrlInfo *c_LMhdKz = new CCtrlInfo (&m_LMhdKz, 1, 15, 1, 1);
	DataGrid.Add (c_LMhdKz);
	CCtrlInfo *c_MhdKz = new CCtrlInfo (&m_MhdKz, 2, 15, 1, 1);
	DataGrid.Add (c_MhdKz);

	CCtrlInfo *c_LMhdZtr = new CCtrlInfo (&m_LMhdZtr, 1, 16, 1, 1);
	DataGrid.Add (c_LMhdZtr);
	CCtrlInfo *c_MhdZtr = new CCtrlInfo (&m_MhdZtr, 2, 16, 1, 1);
	DataGrid.Add (c_MhdZtr);

	CCtrlInfo *c_PrUeb = new CCtrlInfo (&m_PrUeb, 3, 15, 2, 1);
	DataGrid.Add (c_PrUeb);

	CCtrlInfo *c_LTara = new CCtrlInfo (&m_LTara, 3, 16, 1, 1);
	DataGrid.Add (c_LTara);
	CCtrlInfo *c_Tara = new CCtrlInfo (&m_Tara, 4, 16, 1, 1);
	DataGrid.Add (c_Tara);

	CCtrlInfo *c_LAg = new CCtrlInfo (&m_LAg, 0, 0, 1, 1);
	CtrlGrid.Add (c_LAg);

	CCtrlInfo *c_AgGrid = new CCtrlInfo (&AgGrid, 1, 0, 1, 1);
	CtrlGrid.Add (c_AgGrid);

	CCtrlInfo *c_DataGrid = new CCtrlInfo (&DataGrid, 0, 2, DOCKRIGHT, 1);
	CtrlGrid.Add (c_DataGrid);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_SpinSg1.SetRange32 (0, 100);
	m_SpinSg2.SetRange32 (0, 100);
	m_SpinMhdZtr.SetRange32 (0, 1000);


	Form.Show ();
	CtrlGrid.Display ();
	m_SpinSg1.SetBuddy (&m_Sg1);
	m_SpinSg2.SetBuddy (&m_Sg2);
	m_SpinMhdZtr.SetBuddy (&m_MhdZtr);

	return ret;
}

// CAgPage2-Meldungshandler

HBRUSH CAgPage2::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	DlgBkColor = WINDOWHANDLER->GetDlgBkColor ();
	DlgBrush   = WINDOWHANDLER->GetDlgBrush ();
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
				WINDOWHANDLER->SetDlgBrush (DlgBrush);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
				WINDOWHANDLER->SetDlgBrush (DlgBrush);
			}
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CAgPage2::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F5)
			{
				WINDOWHANDLER->OnBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F7)
			{
				WINDOWHANDLER->OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				WINDOWHANDLER->OnWrite ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
//				OnChoice ();
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Ag)
				{
//					OnChoice ();
					return TRUE;
				}
			}
			break;
	}
	return FALSE;
}

BOOL CAgPage2::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CAgPage2::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

BOOL CAgPage2::StepBack ()
{
	return TRUE;
}


void CAgPage2::UpdateWrite ()
{
}

void CAgPage2::UpdateDelete ()
{
}

void CAgPage2::Update ()
{
	Form.Show ();
}

void CAgPage2::Get ()
{
	Form.Get ();
}
