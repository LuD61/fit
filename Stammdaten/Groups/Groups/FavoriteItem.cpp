#include "StdAfx.h"
#include "FavoriteItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CFavoriteItem::CFavoriteItem(void)
{
	Name = _T("");
	ProgrammCall = _T("");
}

CFavoriteItem::CFavoriteItem(LPTSTR Name, LPTSTR ProgrammCall)
{
	this->Name = Name;
	this->ProgrammCall = ProgrammCall;
}

CFavoriteItem::~CFavoriteItem(void)
{
}
