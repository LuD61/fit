#include "stdafx.h"
#include "ChoiceWg.h"
#include "DbUniCode.h"
#include "Process.h"
#include "WindowHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceWg::Sort1 = -1;
int CChoiceWg::Sort2 = -1;
int CChoiceWg::Sort3 = -1;
int CChoiceWg::Sort4 = -1;
int CChoiceWg::Sort5 = -1;

CChoiceWg::CChoiceWg(CWnd* pParent)
        : CChoiceX(pParent)
{
	Where = "";
}

CChoiceWg::~CChoiceWg()
{
	DestroyList ();
}

BEGIN_MESSAGE_MAP(CChoiceWg, CChoiceX)
	//{{AFX_MSG_MAP(CChoiceLief)
	ON_WM_MOVE ()
END_MESSAGE_MAP()

void CChoiceWg::DestroyList()
{
	for (std::vector<CWgList *>::iterator pabl = WgList.begin (); pabl != WgList.end (); ++pabl)
	{
		CWgList *abl = *pabl;
		delete abl;
	}
    WgList.clear ();
	SelectList.clear ();
}

void CChoiceWg::FillList ()
{
    short  wg;
    TCHAR wg_bz1 [50];
    TCHAR wg_bz2 [50];
    TCHAR hwg_bz1 [50];

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Warengruppe"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Warengruppe"),      1, 150, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung 1"),	2, 250);
    SetCol (_T("Bezeichnung 2"),	3, 250);
    SetCol (_T("Hauptarengruppe"),  4,  250);

	if (WgList.size () == 0)
	{
		DbClass->sqlout ((short *)&wg,         SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  wg_bz1,     SQLCHAR, sizeof (wg_bz1));
		DbClass->sqlout ((LPTSTR)  wg_bz2,     SQLCHAR, sizeof (wg_bz2));
		DbClass->sqlout ((LPTSTR)  hwg_bz1,    SQLCHAR, sizeof (wg_bz2));
		CString Sql = _T("select wg.wg, wg_bz1, wg_bz2, hwg_bz1 from wg,hwg where hwg.hwg = wg.hwg and wg > 0");
		Sql += " ";
		Sql += Where;
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) wg_bz1;
			CDbUniCode::DbToUniCode (wg_bz1, pos);
			pos = (LPSTR) wg_bz2;
			CDbUniCode::DbToUniCode (wg_bz2, pos);
			CWgList *abl = new CWgList (wg, wg_bz1, wg_bz2);
			pos = (LPSTR) hwg_bz1;
			CDbUniCode::DbToUniCode (hwg_bz1, pos);
			abl->hwg_bz1 = hwg_bz1;
			WgList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CWgList *>::iterator pabl = WgList.begin (); pabl != WgList.end (); ++pabl)
	{
		CWgList *abl = *pabl;
		CString Wg;
		Wg.Format (_T("%d"), abl->wg);
		CString Num;
		CString Bez;
		_tcscpy (wg_bz1,  abl->wg_bz1.GetBuffer ());
		_tcscpy (wg_bz2,  abl->wg_bz2.GetBuffer ());
		_tcscpy (hwg_bz1, abl->hwg_bz1.GetBuffer ());

        int ret = InsertItem (i, -1);
        ret = SetItemText (Wg.GetBuffer (), i, 1);
        ret = SetItemText (wg_bz1,  i, 2);
        ret = SetItemText (wg_bz2,  i, 3);
        ret = SetItemText (hwg_bz1, i, 4);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort (listView);
}


void CChoiceWg::Search (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, SortRow);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceWg::Search ()
{
    CString EditText;

    CEdit *SearchText = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (SearchText == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    SearchText->GetWindowText (EditText);
    Search (ListBox, EditText.GetBuffer (8));
}

int CChoiceWg::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceWg::CompareProc(LPARAM lParam1,
						 		     LPARAM lParam2,
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   else if (SortRow == 4)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort5;
   }
   return 0;
}


void CChoiceWg::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
        case 4:
              Sort5 *= -1;
			  if (Sort5 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CWgList *abl = WgList [i];

		   abl->wg = _tstoi (ListBox->GetItemText (i, 1));
		   abl->wg_bz1 = ListBox->GetItemText (i, 2);
		   abl->wg_bz2 = ListBox->GetItemText (i, 3);
		   abl->hwg_bz1 = ListBox->GetItemText (i, 4);
	}
	for (int i = 1; i <= 9; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);
}

void CChoiceWg::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = WgList [idx];
}

CWgList *CChoiceWg::GetSelectedText ()
{
	CWgList *abl = (CWgList *) SelectedRow;
	return abl;
}

void CChoiceWg::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (WgList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

void CChoiceWg::OnEnter ()
{
/*
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cAg = m_List.GetItemText (idx, 1);  
		long ag = _tstol (cAg.GetBuffer ());
		Message.Format (_T("Wg=%ld"), ag);
		ToClipboard (Message);
	}
	p.SetCommand (_T("rswrun 13300"));
	HANDLE Pid = p.Start (SW_SHOWNORMAL);
*/
}

void CChoiceWg::OnMove (int x, int y)
{
	WINDOWHANDLER->IsDockPlace (x, y);
}