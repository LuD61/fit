#ifndef _CHOICEHWG_DEF
#define _CHOICEHWG_DEF

#include "ChoiceX.h"
#include "HwgList.h"
#include <vector>

class CChoiceHwg : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;

    public :
		CString Where;
	    std::vector<CHwgList *> HwgList;
	    std::vector<CHwgList *> SelectList;
        CChoiceHwg(CWnd* pParent = NULL);   // Standardkonstruktor
        ~CChoiceHwg();
        virtual void FillList (void);
        void Search (CListCtrl *,  LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
        virtual void SetSelText (CListCtrl *, int);
	CHwgList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		void SaveSelection (CListCtrl *ListBox);
		void OnEnter ();
	    afx_msg void OnMove (int x, int y);

		DECLARE_MESSAGE_MAP()

};
#endif
