// CStartView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Groups.h"
#include "StartView.h"
#include "GroupsView.h"
#include "WindowHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CStartView

IMPLEMENT_DYNCREATE(CStartView, CFormView)

CStartView::CStartView()
	: CFormView(CStartView::IDD)
{
	m_SplitFrame = NULL;
}

CStartView::~CStartView()
{
	if (m_SplitFrame != NULL)
	{
//		delete m_SplitFrame;
	}
}

void CStartView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CStartView, CFormView)
	ON_WM_SIZE ()
//	ON_COMMAND(ID_BACK, OnBack)
//	ON_COMMAND(ID_WRITE, OnWrite)
END_MESSAGE_MAP()

void CStartView::OnInitialUpdate()
{
	CRect rect;
	CFormView::OnInitialUpdate();
	WINDOWHANDLER->set_StartView (this);
	SetFrame ();
/*
	struct CCreateContext cc;
    memset (&cc, 0, sizeof (CCreateContext));
	cc.m_pNewViewClass = RUNTIME_CLASS (CGroupsView);
	m_SplitFrame = new CSplitFrame ();
	cc.m_pCurrentFrame = m_SplitFrame;
	GetClientRect (&rect);
	m_SplitFrame->Create (NULL, _T("Kalkulationsansicht"), WS_CHILD | WS_VISIBLE , rect, this, NULL, NULL, &cc);
*/
}

void CStartView::OnSize (UINT nType, int cx, int cy)
{
	if (m_SplitFrame != NULL && IsWindow (m_SplitFrame->m_hWnd))
	{
		m_SplitFrame->MoveWindow (0, 0, cx, cy);
	}
}

void CStartView::SetFrame ()
{
	CRect rect;

	if (m_SplitFrame != NULL)
	{
		if (IsWindow (m_SplitFrame->m_hWnd))
		{
			m_SplitFrame->DestroyWindow ();
		}
		m_SplitFrame = NULL;
	}
	struct CCreateContext cc;
    memset (&cc, 0, sizeof (CCreateContext));
	cc.m_pNewViewClass = RUNTIME_CLASS (CGroupsView);
	m_SplitFrame = new CSplitFrame ();
	cc.m_pCurrentFrame = m_SplitFrame;
	GetClientRect (&rect);
	m_SplitFrame->Create (NULL, _T(""), WS_CHILD | WS_VISIBLE , rect, this, NULL, NULL, &cc);
}

void CStartView::SetNewSplitView (BOOL docked)
{
	if (docked != WINDOWHANDLER->DockChoice ())
	{
		WINDOWHANDLER->set_DockChoice (TRUE);
		SetFrame ();
	}
}

void CStartView::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	WINDOWHANDLER->OnBack ();
}

void CStartView::OnWrite()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	WINDOWHANDLER->OnWrite ();
}

// CStartView-Diagnose

#ifdef _DEBUG
void CStartView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CStartView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CStartView-Meldungshandler
