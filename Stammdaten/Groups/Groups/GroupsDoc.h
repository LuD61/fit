// GroupsDoc.h : Schnittstelle der Klasse CGroupsDoc
//


#pragma once


class CGroupsDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CGroupsDoc();
	DECLARE_DYNCREATE(CGroupsDoc)

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CGroupsDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


