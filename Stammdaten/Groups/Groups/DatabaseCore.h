#pragma once
#include "DbClass.h"
#include "hwg.h"
#include "wg.h"
#include "ag.h"
#include "Ptabn.h"

class CDatabaseCore
{
public :
	enum SQL_STATE
	{
		SqlError = -1,
		SqlOk = 0,
		SqlNoData = 100,
		SqlLock = 101,
	};
private:
	static CDatabaseCore *Instance; 

	DB_CLASS Dbase;
	SQL_STATE m_SqlState;
	short m_Hwg;
	short m_Wg;
	long m_Ag;
protected:
	CDatabaseCore(void);
	~CDatabaseCore(void);
public:
	HWG_CLASS Hwg;
	HWG_CLASS HwgWg;
	HWG_CLASS HwgAg;
	WG_CLASS Wg;
	WG_CLASS WgAg;
	AG_CLASS Ag;
	PTABN_CLASS Ptabn;
	SQL_STATE GetSqlState ()
	{
		return m_SqlState;
	}
	static CDatabaseCore *GetInstance ();
	static void DestroyInstance ();
	void Opendbase ();
	void InitHwg ();
	void SaveHwgKey ();
	void RestoreHwgKey ();
	BOOL ReadHwg ();
	BOOL UpdateHwg ();
	BOOL DeleteHwg ();
	void InitWg ();
	void InitHwgWg ();
	void SaveWgKey ();
	void RestoreWgKey ();
	BOOL ReadWg ();
	BOOL UpdateWg ();
	BOOL DeleteWg ();
	void InitAg ();
	void SaveAgKey ();
	void RestoreAgKey ();
	BOOL ReadAg ();
	BOOL UpdateAg ();
	BOOL DeleteAg ();
};

#define DATABASE CDatabaseCore::GetInstance ()
#define SQLSTATE DATABASE->GetSqlState ()


#define CHWG DATABASE->Hwg
#define CHWG_WG DATABASE->HwgWg
#define CHWG_AG DATABASE->HwgAg
#define CWG DATABASE->Wg
#define CWG_AG DATABASE->WgAg
#define CAG DATABASE->Ag
#define CPTABN DATABASE->Ptabn

#define THWG DATABASE->Hwg.hwg
#define THWG_WG DATABASE->HwgWg.hwg
#define THWG_AG DATABASE->HwgAg.hwg
#define TWG DATABASE->Wg.wg
#define TWG_AG DATABASE->WgAg.wg
#define TAG DATABASE->Ag.ag
#define TPTABN DATABASE->Ptabn.ptabn
