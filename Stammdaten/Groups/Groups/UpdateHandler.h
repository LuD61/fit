#pragma once

class CUpdateHandler
{
public:
	CUpdateHandler(void);
public:
	~CUpdateHandler(void);
	virtual void Update ()=0;
	virtual void Get () {};
	virtual void UpdateWrite () {}
	virtual void UpdateDelete () {}
	virtual BOOL StepBack ()
	{
		return TRUE;
	}
};
