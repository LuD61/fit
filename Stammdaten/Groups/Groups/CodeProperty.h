#pragma once

class CCodeProperty
{
public:
	short source;
	short target;
	CCodeProperty(void);
	CCodeProperty(LPTSTR);
	CCodeProperty(CString&);
	~CCodeProperty(void);
	void SetValues (CString&);
	short GetSource (short source);
	short GetTarget (short target);
};
