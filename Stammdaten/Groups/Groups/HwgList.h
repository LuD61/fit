#ifndef _HWG_LIST_DEF
#define _HWG_LIST_DEF
#pragma once

class CHwgList
{
public:
	short hwg;
	CString hwg_bz1;
	CString hwg_bz2;
	CHwgList(void);
	CHwgList(short, LPTSTR, LPTSTR);
	~CHwgList(void);
};
#endif
