// WgPage.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Groups.h"
#include "WgPage.h"
#include "HwgPage.h"
#include "WindowHandler.h"
#include "Core.h"
#include "DatabaseCore.h"
#include "uniformfield.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CWgPage-Dialogfeld

IMPLEMENT_DYNAMIC(CWgPage, CDialog)

CWgPage::CWgPage(CWnd* pParent /*=NULL*/)
	: CDialog(CWgPage::IDD, pParent)
{
	Choice = NULL;
	ChoiceHwg = NULL;
	HideOK = FALSE;
	DlgBkColor = NULL;
	DlgBrush = NULL;
	m_CanWrite = FALSE;
	HeadControls.Add (&m_Wg);
	m_TestFocus = TRUE;
	WINDOWHANDLER->SetBase (this);
	WINDOWHANDLER->RegisterUpdate (this);
}

CWgPage::~CWgPage()
{
	WINDOWHANDLER->SetBase (NULL);
	WINDOWHANDLER->UnregisterUpdate (this);
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
	HeadControls.Destroy ();
	Choice = (CChoiceWg *) WINDOWHANDLER->ChoiceWg ();
	if (Choice != NULL && IsWindow (Choice->m_hWnd))
	{
		delete Choice;
	}
}

void CWgPage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LWG, m_LWg);
	DDX_Control(pDX, IDC_WG, m_Wg);
	DDX_Control(pDX, IDC_WG_GROUP, m_WgGroup);
	DDX_Control(pDX, IDC_LWG_BZ, m_LWgBz);
	DDX_Control(pDX, IDC_WG_BZ1, m_WgBz1);
	DDX_Control(pDX, IDC_WG_BZ2, m_WgBz2);
	DDX_Control(pDX, IDC_LHWG, m_LHwg);
	DDX_Control(pDX, IDC_HWG, m_Hwg);
	DDX_Control(pDX, IDC_HWG_BZ1, m_HwgBz1);
	DDX_Control(pDX, IDC_LABT, m_LAbt);
	DDX_Control(pDX, IDC_ABT, m_Abt);
	DDX_Control(pDX, IDC_LWA_KTO, m_LWeKto);
	DDX_Control(pDX, IDC_WE_KTO, m_WeKto);
	DDX_Control(pDX, IDC_LERL_KTO, m_LErlKto);
	DDX_Control(pDX, IDC_ERL_KTO, m_ErlKto);
	DDX_Control(pDX, IDC_LMWST, m_LMwst);
	DDX_Control(pDX, IDC_MWST, m_Mwst);
	DDX_Control(pDX, IDC_LME_EINH, m_LMeEinh);
	DDX_Control(pDX, IDC_ME_EINH, m_MeEinh);
	DDX_Control(pDX, IDC_LBEST_AUTO, m_LBestAuto);
	DDX_Control(pDX, IDC_BEST_AUTO, m_BestAuto);
	DDX_Control(pDX, IDC_LPERS_RAB, m_LPersRab);
	DDX_Control(pDX, IDC_PERS_RAB, m_PersRab);
	DDX_Control(pDX, IDC_LSMT, m_LSmt);
	DDX_Control(pDX, IDC_SMT, m_Smt);
	DDX_Control(pDX, IDC_LTEIL_SMT, m_LTeilSmt);
	DDX_Control(pDX, IDC_TEIL_SMT, m_TeilSmt);
	DDX_Control(pDX, IDC_CALC_GROUP, m_CalcGroup);
	DDX_Control(pDX, IDC_LKOST_KZ, m_LKostKz);
	DDX_Control(pDX, IDC_KOST_KZ, m_KostKz);
	DDX_Control(pDX, IDC_LSP_KZ, m_LSpKz);
	DDX_Control(pDX, IDC_SP_KZ, m_SpKz);
	DDX_Control(pDX, IDC_LSP_VK, m_LSpVk);
	DDX_Control(pDX, IDC_SP_VK, m_SpVk);
	DDX_Control(pDX, IDC_LSP_FIL, m_LSpFil);
	DDX_Control(pDX, IDC_SP_FIL, m_SpFil);
	DDX_Control(pDX, IDC_LSP_SK, m_LSpSk);
	DDX_Control(pDX, IDC_SP_SK, m_SpSk);
	DDX_Control(pDX, IDC_LSW, m_LSw);
	DDX_Control(pDX, IDC_SW, m_Sw);

	DDX_Control(pDX, IDC_LBEARB_LAD, m_LBearbLad);
	DDX_Control(pDX, IDC_BEARB_LAD, m_BearbLad);
	DDX_Control(pDX, IDC_LBEARB_FIL, m_LBearbFil);
	DDX_Control(pDX, IDC_BEARB_FIL, m_BearbFil);
	DDX_Control(pDX, IDC_LBEARB_SK, m_LBearbSk);
	DDX_Control(pDX, IDC_BEARB_SK, m_BearbSk);

	DDX_Control(pDX, IDC_DURCH_VK,  m_DurchVk);
	DDX_Control(pDX, IDC_DURCH_FIL, m_DurchFil);
	DDX_Control(pDX, IDC_DURCH_SK,  m_DurchSk);
	DDX_Control(pDX, IDC_DURCH_SW,  m_DurchSw);
	DDX_Control(pDX, IDC_LVERK_BEG, m_LVerkBeg);
	DDX_Control(pDX, IDC_VERK_BEG, m_VerkBeg);
	DDX_Control(pDX, IDC_LVERK_END, m_LVerkEnd);
	DDX_Control(pDX, IDC_VERK_END, m_VerkEnd);
	DDX_Control(pDX, IDC_LVERK_ART, m_LVerkArt);
	DDX_Control(pDX, IDC_VERK_ART, m_VerkArt);
	DDX_Control(pDX, IDC_LUMS_VERB, m_LUmsVerb);
	DDX_Control(pDX, IDC_UMS_VERB, m_UmsVerb);
	DDX_Control(pDX, IDC_SHOPKZ, m_ShopKZ);
}


BEGIN_MESSAGE_MAP(CWgPage, CDialog)
	ON_WM_CTLCOLOR ()
	ON_COMMAND(IDC_WGCHOICE , OnChoice)
	ON_COMMAND (SELECTED, OnSelected)
	ON_COMMAND (CANCELED, OnCanceled)
	ON_COMMAND(IDC_HWGCHOICE , OnHwgChoice)
	ON_EN_KILLFOCUS(IDC_HWG, &CWgPage::OnEnKillfocusHwg)
END_MESSAGE_MAP()


// CWgPage-Meldungshandler

BOOL CWgPage::OnInitDialog()
{

	CDialog::OnInitDialog();
	DATABASE->InitWg ();

#ifdef BLUE_COL
	COLORREF BkColor (RGB (100, 180, 255));
	COLORREF DynColor = m_NewAngTyp.DynColorBlue;
	COLORREF RolloverColor = RGB (204, 204, 255);
#else
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
#endif

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else if (GetSystemMetrics (SM_CXFULLSCREEN) <= 1300)
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (105, _T("Dlg"));
	}

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 17;
	TitleFont.CreateFontIndirect (&l);

    Form.Add (new CFormField (&m_Wg,EDIT,         (short *) &TWG.wg, VSHORT));
    Form.Add (new CUniFormField (&m_WgBz1,EDIT,   (char *)  TWG.wg_bz1, VCHAR, sizeof (TWG.wg_bz1)));
    Form.Add (new CUniFormField (&m_WgBz2,EDIT,   (char *)  TWG.wg_bz2, VCHAR, sizeof (TWG.wg_bz2)));
    Form.Add (new CFormField (&m_Hwg,EDIT,        (short *) &THWG_WG.hwg, VSHORT));
    Form.Add (new CUniFormField (&m_HwgBz1,EDIT,   (char *)  THWG_WG.hwg_bz1, VCHAR, sizeof (THWG.hwg_bz1)));
    Form.Add (new CFormField (&m_Abt,EDIT,        (short *) &TWG.abt, VSHORT, 4));
    Form.Add (new CFormField (&m_WeKto,EDIT,       (long * ) &TWG.we_kto, VLONG, 8));
    Form.Add (new CFormField (&m_ErlKto,EDIT,      (long * ) &TWG.erl_kto, VLONG, 8));
    Form.Add (new CFormField (&m_Smt,  COMBOBOX,   (char * ) TWG.smt, VCHAR));
	CORE->FillCombo (_T("smt"), &Form.GetFormField (&m_Smt)->ComboValues);
	Form.GetFormField (&m_Smt)->FillComboBox ();
	m_Smt.SetDroppedWidth (200);

    Form.Add (new CFormField (&m_TeilSmt,  COMBOBOX,   (short * ) &TWG.teil_smt, VSHORT));
	CORE->FillCombo (_T("teil_smt"), &Form.GetFormField (&m_TeilSmt)->ComboValues);
	Form.GetFormField (&m_TeilSmt)->FillComboBox ();
	m_TeilSmt.SetDroppedWidth (200);

    Form.Add (new CFormField (&m_Mwst,  COMBOBOX,  (short * )&TWG.mwst, VSHORT));
	CORE->FillCombo (_T("mwst"), &Form.GetFormField (&m_Mwst)->ComboValues);
	Form.GetFormField (&m_Mwst)->FillComboBox ();

    Form.Add (new CFormField (&m_MeEinh,  COMBOBOX,  (short * )&TWG.me_einh, VSHORT));
	CORE->FillCombo (_T("me_einh"), &Form.GetFormField (&m_MeEinh)->ComboValues);
	Form.GetFormField (&m_MeEinh)->FillComboBox ();

    Form.Add (new CFormField (&m_BestAuto,  COMBOBOX,  (char * )TWG.best_auto, VCHAR));
	CORE->FillCombo (_T("best_auto"), &Form.GetFormField (&m_BestAuto)->ComboValues);
	Form.GetFormField (&m_BestAuto)->FillComboBox ();
	m_BestAuto.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_PersRab,EDIT,       (double * ) &TWG.pers_rab, VDOUBLE, 5, 2));

    Form.Add (new CFormField (&m_KostKz,  COMBOBOX,  (char * )  TWG.kost_kz, VCHAR));
	CORE->FillCombo (_T("kost_kz"), &Form.GetFormField (&m_KostKz)->ComboValues);
	Form.GetFormField (&m_KostKz)->FillComboBox ();
	m_KostKz.SetDroppedWidth (300);

	Form.Add (new CFormField (&m_SpKz,  COMBOBOX,  (char * )  TWG.sp_kz, VCHAR));
	CORE->FillCombo (_T("sp_kz"), &Form.GetFormField (&m_SpKz)->ComboValues);
	Form.GetFormField (&m_SpKz)->FillComboBox ();
	m_SpKz.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_VerkArt,  COMBOBOX,  (char * )  TWG.verk_art, VCHAR));
	CORE->FillCombo (_T("verk_art"), &Form.GetFormField (&m_VerkArt)->ComboValues);
	Form.GetFormField (&m_VerkArt)->FillComboBox ();
	m_VerkArt.SetDroppedWidth (200);

    Form.Add (new CFormField (&m_UmsVerb,  COMBOBOX,  (char * )  TWG.ums_verb, VCHAR));
	CORE->FillCombo (_T("ums_verb"), &Form.GetFormField (&m_UmsVerb)->ComboValues);
	Form.GetFormField (&m_UmsVerb)->FillComboBox ();
	m_UmsVerb.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_VerkBeg,DATETIMEPICKER,   (DATE_STRUCT * ) &TWG.verk_beg, VDATE));
    Form.Add (new CFormField (&m_VerkEnd,DATETIMEPICKER,   (DATE_STRUCT * ) &TWG.verk_end, VDATE));
    Form.Add (new CFormField (&m_SpVk,EDIT,       (double * ) &TWG.sp_vk, VDOUBLE, 5, 1));
    Form.Add (new CFormField (&m_SpFil,EDIT,      (double * ) &TWG.sp_fil, VDOUBLE, 5, 1));
    Form.Add (new CFormField (&m_SpSk,EDIT,       (double * ) &TWG.sp_sk, VDOUBLE, 5, 1));
    Form.Add (new CFormField (&m_Sw,EDIT,         (double * ) &TWG.sw, VDOUBLE, 3, 1));

    Form.Add (new CFormField (&m_BearbLad, EDIT,   (short * )  &TWG.bearb_lad, VSHORT, 4));
    Form.Add (new CFormField (&m_BearbFil, EDIT,   (short * )  &TWG.bearb_fil, VSHORT, 4));
    Form.Add (new CFormField (&m_BearbSk,  EDIT,   (short * )  &TWG.bearb_sk,  VSHORT, 4));

    Form.Add (new CFormField (&m_DurchVk,  CHECKBOX,   (char * )   TWG.durch_vk, VCHAR));
    Form.Add (new CFormField (&m_DurchFil, CHECKBOX,   (char * )   TWG.durch_fil, VCHAR));
    Form.Add (new CFormField (&m_DurchSk,  CHECKBOX,   (char * )   TWG.durch_sk, VCHAR));
    Form.Add (new CFormField (&m_DurchSw,  CHECKBOX,   (char * )   TWG.durch_sw, VCHAR));
	Form.Add (new CFormField (&m_ShopKZ,	CHECKBOX,   (char * )   TWG.shop_kz, VCHAR));

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand


//Grid Wgnummer

	WgGrid.Create (this, 2, 2);
    WgGrid.SetBorder (0, 0);
    WgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Wg = new CCtrlInfo (&m_Wg, 0, 0, 1, 1);
	WgGrid.Add (c_Wg);
	CtrlGrid.CreateChoiceButton (m_WgChoice, IDC_WGCHOICE, this);
	CCtrlInfo *c_WgChoice = new CCtrlInfo (&m_WgChoice, 1, 0, 1, 1);
	WgGrid.Add (c_WgChoice);

//Grid Hwg

	HwgGrid.Create (this, 3, 3);
    HwgGrid.SetBorder (0, 0);
    HwgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Hwg = new CCtrlInfo (&m_Hwg, 0, 0, 1, 1);
	HwgGrid.Add (c_Hwg);
	CtrlGrid.CreateChoiceButton (m_HwgChoice, IDC_HWGCHOICE, this);
	CCtrlInfo *c_HwgChoice = new CCtrlInfo (&m_HwgChoice, 1, 0, 1, 1);
	HwgGrid.Add (c_HwgChoice);
	CCtrlInfo *c_HwgBz1 = new CCtrlInfo (&m_HwgBz1, 2, 0, 1, 1);
	c_HwgBz1->SetCellPos (10, 0);
	HwgGrid.Add (c_HwgBz1);

    DataGrid.Create (this, 20, 20);
    DataGrid.SetBorder (0, 0);
	DataGrid.SetCellHeight (15);
    DataGrid.SetGridSpace (5, 15);  //Spaltenabstand und Zeilenabstand
	CCtrlInfo *c_WgGroup = new CCtrlInfo (&m_WgGroup, 0, 0, DOCKRIGHT, DOCKBOTTOM);
	c_WgGroup->SetBottomControl (this, &m_VerkBeg, 10, CCtrlInfo::Bottom);
	DataGrid.Add (c_WgGroup);
	CCtrlInfo *c_LWgBz = new CCtrlInfo (&m_LWgBz, 1, 1, 1, 1);
	DataGrid.Add (c_LWgBz);

	CCtrlInfo *c_WgBz1 = new CCtrlInfo (&m_WgBz1, 2, 1, 1, 1);
	DataGrid.Add (c_WgBz1);

	CCtrlInfo *c_ShopKZ = new CCtrlInfo (&m_ShopKZ, 4, 1, 1, 1);
	DataGrid.Add (c_ShopKZ);

	CCtrlInfo *c_WgBz2 = new CCtrlInfo (&m_WgBz2, 2, 2, 1, 1);
	DataGrid.Add (c_WgBz2);

	CCtrlInfo *c_LHwg = new CCtrlInfo (&m_LHwg, 1, 3, 1, 1);
	DataGrid.Add (c_LHwg);

	CCtrlInfo *c_HwgGrid = new CCtrlInfo (&HwgGrid, 2, 3, 4, 1);
	DataGrid.Add (c_HwgGrid);

	CCtrlInfo *c_LAbt = new CCtrlInfo (&m_LAbt, 1, 4, 1, 1);
	DataGrid.Add (c_LAbt);
	CCtrlInfo *c_Abt = new CCtrlInfo (&m_Abt, 2, 4, 1, 1);
	DataGrid.Add (c_Abt);

	CCtrlInfo *c_LWeKto = new CCtrlInfo (&m_LWeKto, 1, 6, 1, 1);
	DataGrid.Add (c_LWeKto);
	CCtrlInfo *c_WeKto = new CCtrlInfo (&m_WeKto, 2, 6, 1, 1);
	DataGrid.Add (c_WeKto);
	CCtrlInfo *c_LErlKto = new CCtrlInfo (&m_LErlKto, 1, 7, 1, 1);
	DataGrid.Add (c_LErlKto);
	CCtrlInfo *c_ErlKto = new CCtrlInfo (&m_ErlKto, 2, 7, 1, 1);
	DataGrid.Add (c_ErlKto);
	CCtrlInfo *c_LSmt = new CCtrlInfo (&m_LSmt, 3, 6, 1, 1);
	DataGrid.Add (c_LSmt);
	CCtrlInfo *c_Smt = new CCtrlInfo (&m_Smt, 4, 6, 1, 1);
	DataGrid.Add (c_Smt);
	CCtrlInfo *c_LTeilSmt = new CCtrlInfo (&m_LTeilSmt, 3, 7, 1, 1);
	DataGrid.Add (c_LTeilSmt);
	CCtrlInfo *c_TeilSmt = new CCtrlInfo (&m_TeilSmt, 4, 7, 1, 1);
	DataGrid.Add (c_TeilSmt);

	CCtrlInfo *c_LMwst = new CCtrlInfo (&m_LMwst, 1, 9, 1, 1);
	DataGrid.Add (c_LMwst);
	CCtrlInfo *c_Mwst = new CCtrlInfo (&m_Mwst, 2, 9, 1, 1);
	DataGrid.Add (c_Mwst);
	CCtrlInfo *c_LMeEinh = new CCtrlInfo (&m_LMeEinh, 1, 10, 1, 1);
	DataGrid.Add (c_LMeEinh);
	CCtrlInfo *c_MeEinh = new CCtrlInfo (&m_MeEinh, 2, 10, 1, 1);
	DataGrid.Add (c_MeEinh);
	CCtrlInfo *c_LBestAuto = new CCtrlInfo (&m_LBestAuto, 1, 11, 1, 1);
	DataGrid.Add (c_LBestAuto);
	CCtrlInfo *c_BestAuto = new CCtrlInfo (&m_BestAuto, 2, 11, 1, 1);
	DataGrid.Add (c_BestAuto);
	CCtrlInfo *c_LPersRab = new CCtrlInfo (&m_LPersRab, 3, 9, 1, 1);
	DataGrid.Add (c_LPersRab);
	CCtrlInfo *c_PersRab = new CCtrlInfo (&m_PersRab, 4, 9, 1, 1);
	DataGrid.Add (c_PersRab);

	CCtrlInfo *c_LVerkArt = new CCtrlInfo (&m_LVerkArt, 3, 10, 1, 1);
	DataGrid.Add (c_LVerkArt);
	CCtrlInfo *c_VerkArt = new CCtrlInfo (&m_VerkArt, 4, 10, 1, 1);
	DataGrid.Add (c_VerkArt);
	CCtrlInfo *c_LUmsVerb = new CCtrlInfo (&m_LUmsVerb, 3, 11, 1, 1);
	DataGrid.Add (c_LUmsVerb);
	CCtrlInfo *c_UmsVerb = new CCtrlInfo (&m_UmsVerb, 4, 11, 1, 1);
	DataGrid.Add (c_UmsVerb);

	CCtrlInfo *c_LVerkBeg = new CCtrlInfo (&m_LVerkBeg, 1, 13, 1, 1);
	DataGrid.Add (c_LVerkBeg);
	CCtrlInfo *c_VerkBeg = new CCtrlInfo (&m_VerkBeg, 2, 13, 1, 1);
	DataGrid.Add (c_VerkBeg);
	CCtrlInfo *c_LVerkEnd = new CCtrlInfo (&m_LVerkEnd, 3, 13, 1, 1);
	DataGrid.Add (c_LVerkEnd);
	CCtrlInfo *c_VerkEnd = new CCtrlInfo (&m_VerkEnd, 4, 13, 1, 1);
	DataGrid.Add (c_VerkEnd);

	CCtrlInfo *c_CalcGroup = new CCtrlInfo (&m_CalcGroup, 0, 15, DOCKRIGHT, DOCKBOTTOM);
	c_CalcGroup->SetBottomControl (this, &m_Sw, 10, CCtrlInfo::Bottom);
	DataGrid.Add (c_CalcGroup);

	CCtrlInfo *c_LKostKz = new CCtrlInfo (&m_LKostKz, 1, 16, 1, 1);
	DataGrid.Add (c_LKostKz);
	CCtrlInfo *c_KostKz = new CCtrlInfo (&m_KostKz, 2, 16, 1, 1);
	DataGrid.Add (c_KostKz);

	CCtrlInfo *c_LSpKz = new CCtrlInfo (&m_LSpKz, 3, 16, 1, 1);
	DataGrid.Add (c_LSpKz);
	CCtrlInfo *c_SpKz = new CCtrlInfo (&m_SpKz, 4, 16, 1, 1);
	DataGrid.Add (c_SpKz);

    CalcGrid.Create (this, 20, 20);
    CalcGrid.SetBorder (0, 0);
	CalcGrid.SetCellHeight (15);
    CalcGrid.SetGridSpace (5, 15);  //Spaltenabstand und Zeilenabstand
	CCtrlInfo *c_LSpVk = new CCtrlInfo (&m_LSpVk, 1, 0, 1, 1);
	CalcGrid.Add (c_LSpVk);
	CCtrlInfo *c_SpVk = new CCtrlInfo (&m_SpVk, 2, 0, 1, 1);
	CalcGrid.Add (c_SpVk);
	CCtrlInfo *c_LSpFil = new CCtrlInfo (&m_LSpFil, 1, 1, 1, 1);
	CalcGrid.Add (c_LSpFil);
	CCtrlInfo *c_SpFil = new CCtrlInfo (&m_SpFil, 2, 1, 1, 1);
	CalcGrid.Add (c_SpFil);
	CCtrlInfo *c_LSpSk = new CCtrlInfo (&m_LSpSk, 1, 2, 1, 1);
	CalcGrid.Add (c_LSpSk);
	CCtrlInfo *c_SpSk = new CCtrlInfo (&m_SpSk, 2, 2, 1, 1);
	CalcGrid.Add (c_SpSk);
	CCtrlInfo *c_LSw = new CCtrlInfo (&m_LSw, 1, 3, 1, 1);
	CalcGrid.Add (c_LSw);
	CCtrlInfo *c_Sw = new CCtrlInfo (&m_Sw, 2, 3, 1, 1);
	CalcGrid.Add (c_Sw);

	CCtrlInfo *c_LBearbLad = new CCtrlInfo (&m_LBearbLad, 3, 0, 1, 1);
	CalcGrid.Add (c_LBearbLad);
	CCtrlInfo *c_BearbLad = new CCtrlInfo (&m_BearbLad, 4, 0, 1, 1);
	CalcGrid.Add (c_BearbLad);
	CCtrlInfo *c_LBearbFil = new CCtrlInfo (&m_LBearbFil, 3, 1, 1, 1);
	CalcGrid.Add (c_LBearbFil);
	CCtrlInfo *c_BearbFil = new CCtrlInfo (&m_BearbFil, 4, 1, 1, 1);
	CalcGrid.Add (c_BearbFil);
	CCtrlInfo *c_LBearbSk = new CCtrlInfo (&m_LBearbSk, 3, 2, 1, 1);
	CalcGrid.Add (c_LBearbSk);
	CCtrlInfo *c_BearbSk = new CCtrlInfo (&m_BearbSk, 4, 2, 1, 1);
	CalcGrid.Add (c_BearbSk);

	CCtrlInfo *c_DurchVk = new CCtrlInfo (&m_DurchVk, 5, 0, 1, 1);
	c_DurchVk->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchVk);
	CCtrlInfo *c_DurchFil = new CCtrlInfo (&m_DurchFil, 5, 1, 1, 1);
	c_DurchFil->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchFil);
	CCtrlInfo *c_DurchSk = new CCtrlInfo (&m_DurchSk, 5, 2, 1, 1);
	c_DurchSk->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchSk);
	CCtrlInfo *c_DurchSw = new CCtrlInfo (&m_DurchSw, 5, 3, 1, 1);
	c_DurchSw->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchSw);


	CCtrlInfo *c_CalcGrid = new CCtrlInfo (&CalcGrid, 1, 18, 4, 1);
	DataGrid.Add (c_CalcGrid);

	CCtrlInfo *c_LWg = new CCtrlInfo (&m_LWg, 0, 0, 1, 1);
	CtrlGrid.Add (c_LWg);

	CCtrlInfo *c_WgGrid = new CCtrlInfo (&WgGrid, 1, 0, 1, 1);
	CtrlGrid.Add (c_WgGrid);

	CCtrlInfo *c_DataGrid = new CCtrlInfo (&DataGrid, 0, 2, DOCKRIGHT, 1);
	CtrlGrid.Add (c_DataGrid);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	Form.Show ();
	CtrlGrid.Display ();
	EnableFields (FALSE);

	return FALSE;
}

HBRUSH CWgPage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	DlgBkColor = WINDOWHANDLER->GetDlgBkColor ();
	DlgBrush   = WINDOWHANDLER->GetDlgBrush ();
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
				WINDOWHANDLER->SetDlgBrush (DlgBrush);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
				WINDOWHANDLER->SetDlgBrush (DlgBrush);
			}
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CWgPage::OnSize (UINT nType, int cx, int cy)
{
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
	CtrlGrid.Display ();
}


BOOL CWgPage::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F5)
			{
				if (!StepBack ())
				{
					WINDOWHANDLER->DestroyFrame ();
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F7)
			{
//				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				OnChoice ();
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Hwg)
				{
					OnChoice ();
					return TRUE;
				}
			}
			break;
	}
	return FALSE;
}

BOOL CWgPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Wg)
	{
		if (!Read ())
		{
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CWgPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}

void CWgPage::OnChoice ()
{
	if (Choice != NULL && WINDOWHANDLER->ChoiceWg () == NULL)
	{
		Choice = NULL;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceWg (this);
		Choice->IsModal = FALSE;
		Choice->SetBitmapButton (TRUE);
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->HideFilter = FALSE;
		Choice->HideOK    = HideOK;
//		Choice->SetDbClass (&DATABASE->Hwg);
		Choice->DlgBkColor = DlgBkColor;
	}
	WINDOWHANDLER->OnChoice (Choice, GetParent ());
}

void CWgPage::OnSelected ()
{
	int ret = TRUE;
	BOOL CanRestore = FALSE;
	Choice = (CChoiceWg *) WINDOWHANDLER->GetChoice ();
	if (Choice == NULL) return;

	isNewRec = FALSE;
	Form.Get ();
	if (CanWrite ())
	{
		Write ();
		CanRestore = TRUE;
	}
    CWgList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
	CORE->SaveWg ();
	TWG.wg = abl->wg;
    Form.Show ();
	if (!DATABASE->ReadWg () && CanRestore)
	{
		if (DATABASE->GetSqlState () == CDatabaseCore::SqlLock)
		{
			AfxMessageBox (_T("Der Satz wird im Moment bearbeitet"), MB_OK | MB_ICONERROR);
		}
		CORE->RestoreWg ();
		Form.Show ();
		Read ();
	}
	Form.Show ();
	EnableFields (TRUE);
	if (Choice->FocusBack)
	{
		Choice->SetListFocus ();
	}
	m_CanWrite = TRUE;
	WINDOWHANDLER->set_CanWrite (m_CanWrite);
}

void CWgPage::OnCanceled ()
{
	Choice = (CChoiceWg *) WINDOWHANDLER->GetChoice ();
	Choice->ShowWindow (SW_HIDE);
}

void CWgPage::OnHwgChoice ()
{
	m_TestFocus = FALSE;
	ChoiceHwg = new CChoiceHwg (); 
	ChoiceHwg->CreateDlg ();
	ChoiceHwg->SetBitmapButton (TRUE);
	ChoiceHwg->IdArrDown = IDI_HARROWDOWN;
	ChoiceHwg->IdArrUp   = IDI_HARROWUP;
	ChoiceHwg->IdArrNo   = IDI_HARROWNO;
	ChoiceHwg->DoModal ();
    if (ChoiceHwg->GetState ())
    {
		CHwgList *abl = ChoiceHwg->GetSelectedText (); 
		if (abl == NULL) return;
		if (THWG_WG.hwg != abl->hwg)
		{
			THWG_WG.hwg = abl->hwg;
			CHWG_WG.dbreadfirst ();
			GetHwgDefaults ();
		}
		Form.Show ();
		m_Hwg.SetFocus ();
    }
	delete ChoiceHwg;
	ChoiceHwg = NULL;
	m_TestFocus = TRUE;
}


void CWgPage::EnableFields (BOOL b)
{

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	m_HwgChoice.EnableWindow (b);
	EnableHeadControls (!b);
}

void CWgPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Start ();
	CWnd **it;
	CWnd *Control;
	while ((it = HeadControls.GetNext ()) != NULL)
	{
		Control = *it;
		if (Control != NULL)
		{
			Control->EnableWindow (enable);
		}
	}
}

BOOL CWgPage::StepBack ()
{
	if (IsWindowVisible ())
	{
		isNewRec = FALSE;
		if (!CanWrite ())
		{
			return FALSE;
		}
	//	DATABASE->Lief.rollbackwork ();
		EnableFields (FALSE);
		m_CanWrite = FALSE;
		m_Wg.SetFocus ();
	}
	return TRUE;
}

BOOL CWgPage::Read ()
{
	BOOL ret = TRUE;
	isNewRec = FALSE;
	CDatabaseCore::SQL_STATE sqlstate = CDatabaseCore::SqlOk;
	Form.Get ();
	if (!DATABASE->ReadWg ())
	{
		sqlstate = DATABASE->GetSqlState ();
	}
	if (sqlstate == CDatabaseCore::SqlLock)
	{
		AfxMessageBox (_T("Der Satz wird im Moment bearbeitet"), MB_OK | MB_ICONERROR);
		m_Wg.SetFocus ();
		ret = FALSE;
	}
	if  (sqlstate == CDatabaseCore::SqlNoData)
	{
		AfxMessageBox (_T("Neuer Satz"));
		isNewRec = TRUE;
	}
	if (ret)
	{
		Form.Show ();
		EnableFields (TRUE);
		m_WgBz1.SetFocus ();
		m_CanWrite = TRUE;
		WINDOWHANDLER->set_CanWrite (m_CanWrite);
	}
	return ret;
}

void CWgPage::Write ()
{
	if (!IsWindowVisible ())
	{
		return;
	}
	CWnd *focus = GetFocus ();
	Form.Get ();
	CORE->SetWgDateFields ();
	if (DATABASE->UpdateWg ())
	{
		EnableFields (FALSE);
		m_Wg.SetFocus ();
		m_CanWrite = FALSE;
		WINDOWHANDLER->set_CanWrite (m_CanWrite);
		focus = &m_Wg;
		Choice = (CChoiceWg *) WINDOWHANDLER->GetChoice ();
		if (Choice != NULL && isNewRec)
		{
			if (Choice->WgList.size () != 0) // nur dann neu laden, wenn schon geladen wurde
			{
				Choice->FillList ();
			}
			else
			{
				Choice = NULL;
			}
			
		}
		else if (Choice != NULL)
		{
			if (Choice->WgList.size () == 0)
			{
				Choice = NULL;
			}
		}

	}
	else
	{
		AfxMessageBox (_T("Satz kan nicht geschrieben werden"), MB_OK | MB_ICONERROR);
	}
	focus->SetFocus ();
	isNewRec = FALSE;
}

void CWgPage::Delete ()
{
	CWnd *focus = GetFocus ();
	if (AfxMessageBox (_T("Warengruppe l�schen ?"), MB_YESNO| MB_ICONQUESTION) != IDYES)
	{
		focus->SetFocus ();
		return;
	}
	Form.Get ();

	if (DATABASE->DeleteWg ())
	{
		EnableFields (FALSE);
		m_Wg.SetFocus ();
		m_CanWrite = FALSE;
		WINDOWHANDLER->set_CanWrite (m_CanWrite);
		focus = &m_Wg;
		Choice = (CChoiceWg *) WINDOWHANDLER->GetChoice ();
		if (Choice != NULL)
		{
			if (Choice->WgList.size () != 0) // nur dann neu laden, wenn schon geladen wurde
			{
				Choice->FillList ();
			}
		}
		DATABASE->InitWg ();
		Form.Show ();
	}
	else
	{
		AfxMessageBox (_T("Satz kan nicht gel�scht werden"), MB_OK | MB_ICONERROR);
	}

	focus->SetFocus ();
}


void CWgPage::UpdateWrite ()
{
	if (IsWindowVisible ())
	{
		Write ();
	}
}

void CWgPage::UpdateDelete ()
{
	if (IsWindowVisible ())
	{
		Delete ();
	}
}

void CWgPage::Update ()
{
//	if (IsWindowVisible ())
	{

		CHWG_WG.dbreadfirst ();
		Form.Show ();
	}
}

void CWgPage::OnEnKillfocusHwg()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (m_TestFocus)
	{
		short hwg = THWG_WG.hwg;
		Form.Get ();
		if (hwg != THWG_WG.hwg)
		{
			if (CHWG_WG.dbreadfirst () != 0)
			{
				AfxMessageBox (_T("Hauptwarengruppe nicht gefunden"), MB_OK | MB_ICONERROR);
				m_Hwg.SetFocus ();
			}
			else
			{
				GetHwgDefaults ();
				Form.Show ();
			}
		}
	}
}

void CWgPage::GetHwgDefaults ()
{
	BOOL getDefaults = TRUE;
	if (!isNewRec)
	{
		if (AfxMessageBox (_T("Grundwerte von Hauptwarengruppe �bernehmen ?"), MB_YESNO | MB_ICONQUESTION) != IDYES)
		{
			getDefaults = FALSE;
		}
	}
	if (getDefaults)
	{
		CORE->GetHwgDefaults ();
		Form.Show ();
	}
}
