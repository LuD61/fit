#ifndef _PROCESS_DEF
#define _PROCESS_DEF
#include <atlstr.h>

class CProcess
{
  private :
        HANDLE Pid;
		DWORD ProcessId;
        DWORD ExitCode; 
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
		CString Command;
		int x, y, cx, cy;
		WORD ShowMode;

  public :
	 CProcess ();
	 CProcess (LPSTR);
	 CProcess (CString&);
	 ~CProcess ();
	 void Init ();
	 void SetCommand (LPSTR);
	 void SetCommand (CString&);
	 void SetSize (int,int,int,int);
     void SetShowMode (WORD);
	 HANDLE Start ();
	 HANDLE Start (WORD);
     DWORD WaitForEnd ();
	 HANDLE GetPid ();
	 DWORD GetProcessId ();
	 BOOL Stop ();
	 BOOL IsActive ();
	 BOOL IsActive (DWORD);
	 BOOL Terminate (DWORD);
};
#endif