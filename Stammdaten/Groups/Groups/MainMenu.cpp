// MainMenu.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Groups.h"
#include "MainMenu.h"
#include "Core.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainMenu

IMPLEMENT_DYNCREATE(CMainMenu, CFormView)

CMainMenu::CMainMenu()
	: CFormView(CMainMenu::IDD)
{
	FontHeight = 95;
//	DlgBkColor = RGB (51, 51, 51);
	DlgBkColor = RGB (153, 153, 153);
//	DlgBkColor = RGB (204, 204, 204);
	DlgBrush = NULL;
	TitleBrush = NULL;
	Activate = NULL;
}

CMainMenu::~CMainMenu()
{
	if (Activate != NULL)
	{
		delete Activate;
	}
}

void CMainMenu::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TITLE, m_Title);
	DDX_Control(pDX, IDC_BHWG, m_BHwg);
	DDX_Control(pDX, IDC_BWG, m_BWg);
	DDX_Control(pDX, IDC_BAG, m_BAg);
}

BEGIN_MESSAGE_MAP(CMainMenu, CFormView)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_COMMAND (IDC_BHWG, OnHwg)
	ON_COMMAND (IDC_BWG, OnWg)
	ON_COMMAND (IDC_BAG, OnAg)
END_MESSAGE_MAP()

void CMainMenu::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	MessageHandler = CMessageHandler::GetInstance ();
	m_BHwg.nID = IDC_BHWG;
	m_BHwg.Orientation = m_BHwg.Left;
	m_BHwg.TextStyle = m_BHwg.Bold;
	m_BHwg.BorderStyle = m_BHwg.Solide;
	m_BHwg.DynamicColor = TRUE;
	m_BHwg.TextColor =RGB (0, 0, 0);
	m_BHwg.SetBkColor (RGB (100, 180, 255));
	m_BHwg.DynColor = m_BHwg.DynColorBlue;
//	m_BHwg.RolloverColor = m_BHwg.DynColorBlue;
	m_BHwg.RolloverColor = RGB (204, 204, 255);
	m_BHwg.SetWindowText (_T(" Hauptwarengruppen"));
	m_BHwg.LoadBitmap (IDB_DATABASE);
//	m_BHwg.LoadMask (IDB_ANGEBOT_MASK);
	m_BHwg.Container = this;
	Container.Add (&m_BHwg);

	m_BWg.nID = IDC_BWG;
	m_BWg.Orientation = m_BWg.Left;
	m_BWg.TextStyle = m_BWg.Bold;
	m_BWg.BorderStyle = m_BWg.Solide;
	m_BWg.DynamicColor = TRUE;
	m_BWg.TextColor =RGB (0, 0, 0);
	m_BWg.SetBkColor (RGB (100, 180, 255));
	m_BWg.DynColor = m_BWg.DynColorBlue;
//	m_BWg.RolloverColor = m_BWg.DynColorBlue;
	m_BWg.RolloverColor = RGB (204, 204, 255);
	m_BWg.SetWindowText (_T(" Warengruppen"));
	m_BWg.LoadBitmap (IDB_DATABASE);
//	m_BWg.LoadMask (IDB_ANGEBOT_MASK);
	m_BWg.Container = this;
	Container.Add (&m_BWg);

	m_BAg.nID = IDC_BAG;
	m_BAg.Orientation = m_BAg.Left;
	m_BAg.TextStyle = m_BAg.Bold;
	m_BAg.BorderStyle = m_BAg.Solide;
	m_BAg.DynamicColor = TRUE;
	m_BAg.TextColor =RGB (0, 0, 0);
	m_BAg.SetBkColor (RGB (100, 180, 255));
	m_BAg.DynColor = m_BAg.DynColorBlue;
//	m_BWg.RolloverColor = m_BWg.DynColorBlue;
	m_BAg.RolloverColor = RGB (204, 204, 255);
	m_BAg.SetWindowText (_T(" Artikelgruppen"));
	m_BAg.LoadBitmap (IDB_DATABASE);
//	m_BWg.LoadMask (IDB_ANGEBOT_MASK);
	m_BAg.Container = this;
	Container.Add (&m_BAg);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (FontHeight, _T("Dlg"));
	}

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 21;
	TitleFont.CreateFontIndirect (&l);

    CRect ButtonRect;
	m_BHwg.GetClientRect (&ButtonRect);

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (ButtonRect.bottom + 6);
//    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (0, 0);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_Title = new CCtrlInfo (&m_Title, 0, 0, DOCKRIGHT, 1);
	CtrlGrid.Add (c_Title);
	CCtrlInfo *c_BHwg = new CCtrlInfo (&m_BHwg, 0, 1, DOCKRIGHT, 1);
	c_BHwg->SetCellPos (0, -5);
	CtrlGrid.Add (c_BHwg);
	CCtrlInfo *c_BWg = new CCtrlInfo (&m_BWg, 0, 2, DOCKRIGHT, 1);
	c_BWg->SetCellPos (0, -5);
	CtrlGrid.Add (c_BWg);
	CCtrlInfo *c_BAg = new CCtrlInfo (&m_BAg, 0, 3, DOCKRIGHT, 1);
	c_BAg->SetCellPos (0, -5);
	CtrlGrid.Add (c_BAg);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_Title.SetFont (&TitleFont);

	CtrlGrid.Display ();
	Activate = new CActivate (this);
	MESSAGEHANDLER->Register (Activate);
    OnHwg ();
}

void CMainMenu::OnSize (UINT nType, int cx, int cy)
{
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
	CtrlGrid.Display ();
}

HBRUSH CMainMenu::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
//			m_MoreKto.SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (pWnd == &m_Title)
	{
		pDC->SetTextColor (RGB (255,255,255));
		pDC->SetBkColor (RGB (102,102,102));
		if (TitleBrush == NULL)
		{
			TitleBrush = CreateSolidBrush (RGB (102,102,102));
		}
		return TitleBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CMainMenu::ElementSetFocus (CWnd *cWnd)
{
	CColorButton **it;
	CColorButton *c;

	Container.Start ();
	while ((it = Container.GetNext ()) != NULL)
	{
		c = *it;
		if (c != cWnd)
		{
			c->SetActive (FALSE);
		}
	}
}

void CMainMenu::OnHwg ()
{
	m_BHwg.SetActive (TRUE);
	m_BWg.SetActive (FALSE);
	m_BAg.SetActive (FALSE);
	MessageHandler->RunMessage (IDC_BHWG);
}

void CMainMenu::OnWg ()
{
	m_BHwg.SetActive (FALSE);
	m_BWg.SetActive (TRUE);
	m_BAg.SetActive (FALSE);
	MessageHandler->RunMessage (IDC_BWG);
}

void CMainMenu::OnAg ()
{
	m_BHwg.SetActive (FALSE);
	m_BWg.SetActive (FALSE);
	m_BAg.SetActive (TRUE);
	MessageHandler->RunMessage (IDC_BAG);
}

CMainMenu::CActivate::CActivate (CMainMenu *p)
{
	this->p = p;
}

void CMainMenu::CActivate::Run ()
{

	switch (CORE->PageSelect ())
	{
		case CCore::ActiveHwg:
			p->OnHwg ();
			break;
		case CCore::ActiveWg:
			p->OnWg ();
			break;
		case CCore::ActiveAg:
			p->OnAg ();
			break;
	}
}

// CMainMenu-Diagnose

#ifdef _DEBUG
void CMainMenu::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CMainMenu::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CMainMenu-Meldungshandler
