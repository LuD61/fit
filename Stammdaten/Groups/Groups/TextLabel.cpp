#include "StdAfx.h"
#include "TextLabel.h"
#include "Bmap.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CTextLabel, CLabel)

CTextLabel::CTextLabel(void)
{
	Orientation = Left;
	TextStyle = Standard;
	BorderStyle = None;
	TextColor = RGB (0, 0, 0);
	BkColor = RGB (102,102,102);
	DynamicColor = FALSE;
	parts = 120;
	m_IsTransparent = TRUE;
}

CTextLabel::~CTextLabel(void)
{
}

void CTextLabel::Draw (CDC& cDC)
{
	int x = 1;
	int y = 0;
	CString Text;
	GetWindowText (Text);
	CRect rect;
	CRect fillRect;
	CSize Size;
	int ret = 0;

	GetClientRect (&rect);
	fillRect = rect;

	if (DynamicColor)
	{
			FillRectParts (cDC, BkColor, rect);
	}
	else if (!m_IsTransparent)
	{
			cDC.FillRect (&rect, &CBrush (BkColor));
	}
    CPen *oldPen = NULL;

	if (BorderStyle == Dot)
	{
			CPen Pen (PS_DOT, 1, RGB (0, 0, 0));
			oldPen = cDC.SelectObject (&Pen);
			cDC.Rectangle (&rect);
	}
	else if (BorderStyle == Solide)
	{
				CPen Pen (PS_SOLID, 1, RGB (128, 128, 128));
				oldPen = cDC.SelectObject (&Pen);
				cDC.Rectangle (&rect);
	}
	cDC.SelectObject (oldPen);
	if (BorderStyle != None)
	{
		fillRect = rect;
		fillRect.left ++;
		fillRect.top ++;
		fillRect.right --;
		fillRect.bottom --;
		if (DynamicColor)
		{
				FillRectParts (cDC, BkColor, fillRect);
				cDC.FrameRect (&rect, &CBrush (RGB (128, 128, 128)));
		}
		else
		{
				cDC.FillRect (&fillRect, &CBrush (BkColor));
		}
	}

	if ((HBITMAP) Bitmap != NULL)
	{
		POINT Size;
		BMAP Bmap;
		Bmap.BitmapSize (cDC, (HBITMAP) Bitmap, &Size);
		x = 10; 
		y = max (0, (rect.bottom - Size.y) / 2); 

		if ((HBITMAP) Mask != NULL)
		{
			Bmap.PrintTransparentBitmap ((HBITMAP) Bitmap, (HBITMAP) Mask, &cDC, x, y);
		}
		else if (hMask != NULL)
		{
			Bmap.PrintTransparentBitmap ((HBITMAP) Bitmap, hMask, &cDC, x, y);
		}
		else
		{
			Bmap.PrintTransparentBitmap ((HBITMAP) Bitmap, &cDC, x, y);
		}
		x += Size.x + 10;
	}

	CString sText = Text;

	LPTSTR t = strtok (sText.GetBuffer (), "\n");
	y = 0;
	int row = 0;
	while (t != NULL)
	{
		CString T = t;
		Size = cDC.GetTextExtent (T);
		y = row * Size.cy;
		row ++;
	    t = strtok (NULL, "\n");
	}
	if (row > 0)
	{
		y = row * Size.cy;
	}
    
	int start = max (0, (rect.bottom - y) / 2);

	sText = Text;
	t = strtok (sText.GetBuffer (), "\n");
	row = 0;
	cDC.SetBkMode (TRANSPARENT);
	while (t != NULL)
	{
		CString T = t;
		Size = cDC.GetTextExtent (T);
		if (Orientation == Center)
		{
			x = max (x, (rect.right - Size.cx) / 2);
		}
		else if (Orientation == Right)
		{
			x = max (x, (rect.right - Size.cx - 2));
		}
		y = row * Size.cy + start;

		if (IsWindowEnabled ())
		{
			cDC.SetTextColor (TextColor);
			cDC.TextOut (x, y, t, (int) _tcslen (t));
		}
		else
		{
			cDC.SetTextColor (RGB (192, 192, 192));
			cDC.TextOut (x, y, t, (int) _tcslen (t));
			cDC.SetTextColor (RGB (255, 255, 255));
			cDC.TextOut (x + 1, y + 1, t, (int) _tcslen (t));
		}

		row ++;
	    t = strtok (NULL, "\n");
	}
}

