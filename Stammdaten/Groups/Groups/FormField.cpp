#include "StdAfx.h"
#include "formfield.h"
#include "StrFuncs.h"
#include "DbClass.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CFormField::CFormField(void)
{
	Control  = NULL;
	CtrlType = EDIT;
	Vadr     = NULL;
	VType    = VCHAR;
	Scale = 0;
	len = 0;
}

CFormField::CFormField(CWnd *Control, int CtrlType, void *Vadr, int VType, int Length)
{
	this->Control  = Control;
	this->CtrlType = CtrlType;
	this->Vadr     = Vadr;
	this->VType    = VType;
	this->len = Length;
	Scale = 0;
}


CFormField::CFormField(CWnd *Control, int CtrlType, void *Vadr, int VType, int len, int Scale)
{
	this->Control  = Control;
	this->CtrlType = CtrlType;
	this->Vadr     = Vadr;
	this->VType    = VType;
	this->len      = len;
	this->Scale    = Scale;
}

CFormField::~CFormField(void)
{
	for (std::vector<CString *>::iterator valp = ComboValues.begin (); 
											valp != ComboValues.end ();
											valp ++)
	{
		CString *value = *valp;
		delete value;
	}
	ComboValues.clear ();
}

void CFormField::FillComboBox ()
{
	if (CtrlType != COMBOBOX)
	{
		return;
	}
	((CComboBox *) Control)->ResetContent ();
	for (std::vector<CString *>::iterator valp = ComboValues.begin (); 
											valp != ComboValues.end ();
											valp ++)
	{
		CString *value = *valp;
		((CComboBox *) Control)->AddString (value->GetBuffer (0));
	}
}

void CFormField::DestroyComboBox ()
{
	if (CtrlType != COMBOBOX)
	{
		return;
	}
	((CComboBox *) Control)->ResetContent ();
	for (std::vector<CString *>::iterator valp = ComboValues.begin (); 
											valp != ComboValues.end ();
											valp ++)
	{
		CString *value = *valp;
		delete value;
	}
	ComboValues.clear ();
}

BOOL CFormField::operator== (CWnd *Control)
{
	if (Control == this->Control)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL CFormField::equals (CWnd *Control)
{
	if (Control == this->Control)
	{
		return TRUE;
	}
	return FALSE;
}

void CFormField::SetSel (int idx)
{
	if (CtrlType != COMBOBOX)
	{
		return;
	}
	((CComboBox *) Control)->SetCurSel (idx);
}

int CFormField::FindInComboBox (LPTSTR item)
{
	int ret = -1;
	int idx = 0;
	CString Item = item;
	Item.TrimRight ();
	for (std::vector<CString *>::iterator valp = ComboValues.begin (); 
											valp != ComboValues.end ();
											valp ++)
	{
		CString *value = *valp;
		value->Trim ();
		if (value->GetLength () >= Item.GetLength ())
		{
			CString part = value->Left (Item.GetLength ());
			if (Item == part)
			{
				ret = idx;
				break;
			}
		}
		idx ++;
	}
	return ret;
}


void CFormField::SetTextSelected (CString& Text)
{
	CString Entry;
	if (CtrlType != COMBOBOX)
	{
		return;
	}
	int idx = -1;
	idx = ((CComboBox *) Control)->FindString (-1, Text.TrimRight ().GetBuffer (0));
	if (idx < 0)
	{
		idx = FindInComboBox (Text.GetBuffer ());
	}
	if (idx < 0) idx = 0;
	SetSel (idx);
}

BOOL CFormField::GetSelectedText (CString& Text)
{
	if (CtrlType != COMBOBOX)
	{
		Text = "";
		return FALSE;
	}
	int idx = ((CComboBox *) Control)->GetCurSel ();
	if (idx < 0) 
	{
		Text = "";
		return FALSE;
	}
    ((CComboBox *) Control)->GetLBText (idx, Text);
	return TRUE;
}

void CFormField::Show ()
{
	CString Text;
//	LPSTR pos;
//	LPTSTR text;

	CString frm = "";
    int pos = 0;
	CString DateBuffer;

	switch (VType)
	{
	case VCHAR :
		Text = (LPTSTR) Vadr;
		break;
	case VSHORT :
		Text.Format (_T("%hd"), *((short *) Vadr));
		break;
	case VLONG :
		Text.Format (_T("%ld"), *((long *) Vadr));
		break;
	case VSTRING :
		Text = *(CString *) Vadr;
		break;
	case VDOUBLE :
		frm.Format (_T("%c.%dlf"), '%', Scale);
		Text.Format (frm.GetBuffer (0), *((double *) Vadr));
		pos = Text.Find ('.');
		if (pos != -1)
		{
			Text.GetBuffer ()[pos] = ',';
		}
		break;
	case VDATE :
		DB_CLASS::FromDbDate (DateBuffer, (DATE_STRUCT*) Vadr);
		Text = DateBuffer;
		break;
	}
	Text.TrimRight ();
	if (CtrlType == EDIT)
	{
		    ((CEdit *) Control)->SetWindowText (Text);
			if (len != 0)
			{
				((CEdit *) Control)->SetLimitText (len);
			}
	}
	else if (CtrlType == COMBOBOX)
	{
			SetTextSelected (Text);
	}
	else if (CtrlType == CHECKBOX)
	{
		if ((Text.CollateNoCase (_T("J")) == 0) ||
			Text.Trim ().Compare (_T("1")) == 0)
		{
			((CButton *) Control)->SetCheck (BST_CHECKED);
		}
		else
		{
			((CButton *) Control)->SetCheck (BST_UNCHECKED);
		}
	}
	else if (CtrlType == DATETIMEPICKER)
	{
		if (Text != "")
		{
			CString sday = Text.Left (2);  
			CString smonth = Text.Mid (3, 2);  
			CString syear = Text.Right (4);  
			CTime Time (_tstoi (syear.GetBuffer ()),
			        _tstoi (smonth.GetBuffer ()),
					_tstoi (sday.GetBuffer ()),
					0,0,0);
			((CDateTimeCtrl *) Control)->SetTime (&Time);
		}
		else
		{
			 CTime Time = CTime::GetCurrentTime();
			((CDateTimeCtrl *) Control)->SetTime (&Time);
		}
	}
}

void CFormField::Get ()
{
	CString Text;
	CTime Time;
//	LPSTR p;
//	LPTSTR text;

	if (CtrlType == EDIT)
	{
		    ((CEdit *) Control)->GetWindowText (Text);
	}
	else if (CtrlType == COMBOBOX)
	{
			GetSelectedText (Text);
			int iStart = 0;
			Text = Text.Tokenize (_T(" "), iStart);
	}
	else if (CtrlType == CHECKBOX)
	{
		int checked = ((CButton *) Control)->GetCheck ();
		if (checked == BST_CHECKED)
		{
			if (VType == VCHAR)
			{
				Text = "J";
			}
			else
			{
				Text = "1";
			}
		}
		else if (checked == BST_UNCHECKED)
		{
			if (VType == VCHAR)
			{
				Text = "N";
			}
			else
			{
				Text = "0";
			}
		}
	}
	else if (CtrlType == DATETIMEPICKER)
	{
		((CDateTimeCtrl *) Control)->GetTime (Time);
		int year = Time.GetYear ();
		int month = Time.GetMonth ();
		int day = Time.GetDay ();
		Text.Format ("%02d.%02d.%04d",day, month, year);
	}
	switch (VType)
	{
		case VCHAR :
			_tcscpy ((LPTSTR) Vadr, Text.GetBuffer ());
			break;
		case VSHORT :
			*((short*) Vadr) = _tstoi (Text.GetBuffer(0));
			break;
		case VLONG :
			*((long*) Vadr) =  _tstol (Text.GetBuffer(0));
			break;
		case VDOUBLE :
			*((double *) Vadr) = CStrFuncs::StrToDouble (Text);
			break;
		case VSTRING :
			*(CString *)Vadr = Text;
			break;
		case VDATE :
			DB_CLASS::ToDbDate (Text, (DATE_STRUCT *) Vadr);
			break;
	}
}

void CFormField::OemToAnsi (CString &Str)
{
}

void CFormField::AnsiToOem (CString &Str)
{
}

void CFormField::OemToAnsi (LPSTR str)
{
}

void CFormField::AnsiToOem (LPSTR str)
{
}