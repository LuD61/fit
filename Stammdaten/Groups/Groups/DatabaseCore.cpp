#include "StdAfx.h"
#include "DatabaseCore.h"
#include "DbTime.h"
#include "DbUnicode.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CDatabaseCore *CDatabaseCore::Instance = NULL; 

CDatabaseCore::CDatabaseCore(void)
{
}

CDatabaseCore::~CDatabaseCore(void)
{
	m_SqlState = SqlOk;
}

CDatabaseCore *CDatabaseCore::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CDatabaseCore ();
	}
	return Instance;
}

void CDatabaseCore::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

void CDatabaseCore::Opendbase ()
{
	Dbase.opendbase ("bws");
}

void CDatabaseCore::InitHwg ()
{
	memcpy (&Hwg.hwg, &hwg_null, sizeof (HWG));
}

void CDatabaseCore::SaveHwgKey ()
{
	m_Hwg = THWG.hwg;
}

void CDatabaseCore::RestoreHwgKey ()
{
	 THWG.hwg = m_Hwg;
}

BOOL CDatabaseCore::ReadHwg ()
{
	int ret = FALSE;
	int dsqlstatus;
	extern short sql_mode;
	short sqls;

    sqls = sql_mode;
	sql_mode = 1;
	Hwg.beginwork ();
	SaveHwgKey ();
	InitHwg ();
	RestoreHwgKey ();
	if ((dsqlstatus = Hwg.dbreadfirst ()) == 0)
	{
		m_SqlState = SqlOk;
		if (Hwg.dbupdate () != 0)
		{
			m_SqlState = SqlLock;
		}
		else
		{
			ret = TRUE;
		}
	}
	else
	{
		if (dsqlstatus == SqlNoData)
		{
			m_SqlState = SqlNoData;
		}
		else
		{
			m_SqlState = SqlLock;
		}
	}
    sql_mode = sqls;
	return ret;
}

BOOL CDatabaseCore::UpdateHwg ()
{
	extern short sql_mode;
	short sqls;

    sqls = sql_mode;
	sql_mode = 1;
	int ret = FALSE;
	if (Hwg.dbupdate () == 0)
	{
		ret = TRUE;
	}
    sql_mode = sqls;
	Hwg.commitwork ();
	return ret;
}

BOOL CDatabaseCore::DeleteHwg ()
{
	extern short sql_mode;
	short sqls;

    sqls = sql_mode;
	sql_mode = 1;
	int ret = FALSE;
	if (Hwg.dbdelete () == 0)
	{
		ret = TRUE;
	}
    sql_mode = sqls;
	Hwg.commitwork ();
	return ret;
}

void CDatabaseCore::InitWg ()
{
	memcpy (&Wg.wg, &wg_null, sizeof (WG));
	memcpy (&HwgWg.hwg, &hwg_null, sizeof (HWG));
}

void CDatabaseCore::InitAg ()
{
	memcpy (&Ag.ag, &ag_null, sizeof (AG));
	memcpy (&WgAg.wg, &wg_null, sizeof (WG));
	memcpy (&HwgAg.hwg, &hwg_null, sizeof (HWG));
}

void CDatabaseCore::SaveWgKey ()
{
	m_Wg = TWG.wg;
}

void CDatabaseCore::RestoreWgKey ()
{
	 TWG.wg = m_Wg;
}

BOOL CDatabaseCore::ReadWg ()
{
	int ret = FALSE;
	int dsqlstatus;
	extern short sql_mode;
	short sqls;

    sqls = sql_mode;
	sql_mode = 1;
	Wg.beginwork ();
	SaveWgKey ();
	InitWg ();
	RestoreWgKey ();
	if ((dsqlstatus = Wg.dbreadfirst ()) == 0)
	{
		m_SqlState = SqlOk;
		if (Wg.dbupdate () != 0)
		{
			m_SqlState = SqlLock;
		}
		else
		{
			THWG_WG.hwg = TWG.hwg;  
			HwgWg.dbreadfirst ();
			ret = TRUE;
		}
	}
	else
	{
		if (dsqlstatus == SqlNoData)
		{
			m_SqlState = SqlNoData;
		}
		else
		{
			m_SqlState = SqlLock;
		}
	}
    sql_mode = sqls;
	return ret;
}

BOOL CDatabaseCore::UpdateWg ()
{
	extern short sql_mode;
	short sqls;

    sqls = sql_mode;
	sql_mode = 1;
	int ret = FALSE;
	 TWG.hwg = THWG_WG.hwg;  
	if (Wg.dbupdate () == 0)
	{
		ret = TRUE;
	}
    sql_mode = sqls;
	Wg.commitwork ();
	return ret;
}

BOOL CDatabaseCore::DeleteWg ()
{
	extern short sql_mode;
	short sqls;

    sqls = sql_mode;
	sql_mode = 1;
	int ret = FALSE;
	if (Wg.dbdelete () == 0)
	{
		ret = TRUE;
	}
    sql_mode = sqls;
	Wg.commitwork ();
	return ret;
}

void CDatabaseCore::SaveAgKey ()
{
	m_Ag = TAG.ag;
}

void CDatabaseCore::RestoreAgKey ()
{
	 TAG.ag = m_Ag;
}

BOOL CDatabaseCore::ReadAg ()
{
	int ret = FALSE;
	int dsqlstatus;
	extern short sql_mode;
	short sqls;

    sqls = sql_mode;
	sql_mode = 1;
	Ag.beginwork ();
	SaveAgKey ();
	InitAg ();
	RestoreAgKey ();
	if ((dsqlstatus = Ag.dbreadfirst ()) == 0)
	{
		m_SqlState = SqlOk;
		if (Ag.dbupdate () != 0)
		{
			m_SqlState = SqlLock;
		}
		else
		{
			TWG_AG.wg = TAG.wg;  
			if (WgAg.dbreadfirst () == 0)
			{
				THWG_AG.hwg = TWG_AG.hwg;
				HwgAg.dbreadfirst ();
			}
			ret = TRUE;
		}
	}
	else
	{
		if (dsqlstatus == SqlNoData)
		{
			m_SqlState = SqlNoData;
		}
		else
		{
			m_SqlState = SqlLock;
		}
	}
    sql_mode = sqls;
	return ret;
}

BOOL CDatabaseCore::UpdateAg ()
{
	extern short sql_mode;
	short sqls;

    sqls = sql_mode;
	sql_mode = 1;
	int ret = FALSE;
	TAG.wg = TWG_AG.wg;  
	if (Ag.dbupdate () == 0)
	{
		ret = TRUE;
	}
    sql_mode = sqls;
	Ag.commitwork ();
	return ret;
}

BOOL CDatabaseCore::DeleteAg ()
{
	extern short sql_mode;
	short sqls;

    sqls = sql_mode;
	sql_mode = 1;
	int ret = FALSE;
	if (Ag.dbdelete () == 0)
	{
		ret = TRUE;
	}
    sql_mode = sqls;
	Ag.commitwork ();
	return ret;
}
