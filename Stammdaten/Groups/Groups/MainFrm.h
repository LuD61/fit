// MainFrm.h : Schnittstelle der Klasse CMainFrame
//


#pragma once
#include "WindowHandler.h"
#include "Splitter.h"
#include "MainSplitter.h"

class CMainFrame : public CFrameWnd
{
	
protected: // Nur aus Serialisierung erstellen
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attribute
protected:
	CSplitterWnd m_wndSplitter;
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementierung
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // Eingebettete Member der Steuerleiste
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CSplitter   *m_Splitter;
	CMainSplitter *m_MainSplitter;
	CGroupSplitter *m_GroupSplitter;

// Generierte Funktionen f�r die Meldungstabellen
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
	void OnUpdateBack (CCmdUI *pCmdUI);
	void OnUpdateFileSave (CCmdUI *pCmdUI);
	void OnUpdateDelete (CCmdUI *pCmdUI);
    afx_msg void OnBack();
    afx_msg void OnWrite();
	afx_msg void OnDockChoice();
	afx_msg void OnDockBottom();
	afx_msg void OnDockEnabled();
	afx_msg void OnDockAlways();
private:
	CMenu m_Menu;
	BOOL sizeOK;
public:
	CMenu *Menu ()
	{
		return &m_Menu;
	}
	virtual void OnSize (UINT nType, int cx, int cy);
	virtual void OnDestroy ();
};


