#ifndef _CHOICEWG_DEF
#define _CHOICEWG_DEF

#include "ChoiceX.h"
#include "WgList.h"
#include <vector>

class CChoiceWg : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;

    public :
		CString Where;
	    std::vector<CWgList *> WgList;
	    std::vector<CWgList *> SelectList;
        CChoiceWg(CWnd* pParent = NULL);   // Standardkonstruktor
        ~CChoiceWg();
        virtual void FillList (void);
        void Search (CListCtrl *,  LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
        virtual void SetSelText (CListCtrl *, int);
	CWgList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		void SaveSelection (CListCtrl *ListBox);
		void OnEnter ();
	    afx_msg void OnMove (int x, int y);

		DECLARE_MESSAGE_MAP()

};
#endif
