#pragma once
#include "token.h"

class CTokenHK : public CToken
{
protected:
	char *m_p;
public:
	CTokenHK(void);
    CTokenHK (char *, char *);
    CTokenHK (CString&, char *);
public:
	~CTokenHK(void);
	 char *StrTok (char *p, char *sep);
     virtual void GetTokens (char *);
};
