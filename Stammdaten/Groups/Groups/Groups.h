// Groups.h : Hauptheaderdatei f�r die Groups-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"       // Hauptsymbole


// CGroupsApp:
// Siehe Groups.cpp f�r die Implementierung dieser Klasse
//

class CGroupsApp : public CWinApp
{
public:
	CGroupsApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
    afx_msg void OnBack();
    afx_msg void OnWrite();
    afx_msg void OnDelete();
	DECLARE_MESSAGE_MAP()
};

extern CGroupsApp theApp;