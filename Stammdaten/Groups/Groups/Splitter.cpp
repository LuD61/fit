#include "StdAfx.h"
#include "Splitter.h"
#include "Windowhandler.h"
#include "MainMenu.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CSplitter, CSplitterWnd)

CSplitter::CSplitter(void) : CSplitterWnd ()
{
	m_MainSplitter  = NULL;;
	m_GroupSplitter = NULL;
}

CSplitter::~CSplitter(void)
{
}

BEGIN_MESSAGE_MAP(CSplitter, CSplitterWnd)
//	ON_WM_CTLCOLOR ()
	ON_WM_DESTROY ()
END_MESSAGE_MAP()

void  CSplitter::OnDestroy ()
{
	Destroy ();
}

void  CSplitter::Destroy ()
{
}

BOOL CSplitter::CreateView( int row,int col,CRuntimeClass* pViewClass,SIZE sizeInit,CCreateContext* pContext)
{
// Hier kann eine andere pViewClass als CRuntimeClass eingesetzte werden RUNTIME_CLASS (COwnViw)
	return CSplitterWnd::CreateView (row, col, pViewClass, sizeInit, pContext);
/*
	CRuntimeClass * view = pViewClass;
	CWindowHandler::VIEWTYPE viewType = WINDOWHANDLER->ViewType ();
	if (viewType == CWindowHandler::MainMen)
	{
		m_MainSplitter = new CMainSplitter ();
		return m_MainSplitter->Create (this, 2, 2, CSize (100, 100), pContext);

		view = RUNTIME_CLASS (CMainSplitter);
	}
	m_GroupSplitter = new CGroupSplitter ();
	return m_GroupSplitter->Create (this, 2, 2, CSize (100, 100), pContext);
*/
}

HBRUSH CSplitter::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CSplitterWnd::OnCtlColor (pDC, pWnd,nCtlColor);
}
