#pragma once
#include "afxext.h"

class CGroupSplitter :
	public CSplitterWnd
{
private:
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
public:
	enum COL_TYPE
	{
		Form   = 0,
		Choice = 1,
	};
	CGroupSplitter(void);
	DECLARE_DYNCREATE(CGroupSplitter)
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnDestroy ();
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
public:
	~CGroupSplitter(void);
	void Destroy ();
	virtual BOOL CreateView( int row,int col,CRuntimeClass* pViewClass,SIZE sizeInit,CCreateContext* pContext);
	virtual void OnDrawSplitter(CDC*, CSplitterWnd::ESplitType, const CRect&); 
	virtual void OnInvertTracker(const CRect& rect); 
};
