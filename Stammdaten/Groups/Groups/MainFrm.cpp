// MainFrm.cpp : Implementierung der Klasse CMainFrame
//

#include "stdafx.h"
#include "Groups.h"

#include "MainFrm.h"
#include "StartView.h"
#include "GroupsView.h"
#include "MainMenu.h"
#include "ChoiceView.h"
#include "FavoriteView.h"
#include "DatabaseCore.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SIZE ()
	ON_WM_DESTROY ()
	ON_UPDATE_COMMAND_UI(ID_BACK, &CMainFrame::OnUpdateBack)
	ON_UPDATE_COMMAND_UI(ID_WRITE, &CMainFrame::OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_DELETE, &CMainFrame::OnUpdateDelete)
	ON_COMMAND(ID_DOCK_CHOICE, &CMainFrame::OnDockChoice)
	ON_COMMAND(ID_DOCK_BOTTOM, &CMainFrame::OnDockBottom)
	ON_COMMAND(ID_DOCK_ENABLED, &CMainFrame::OnDockEnabled)
	ON_COMMAND(ID_DOCK_ALWAYS, &CMainFrame::OnDockAlways)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // Statusleistenanzeige
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame-Erstellung/Zerst�rung

CMainFrame::CMainFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
	sizeOK = FALSE;
	m_Splitter = NULL;
	m_MainSplitter = NULL;
	m_GroupSplitter = NULL;
	DATABASE->Opendbase ();
	WINDOWHANDLER->set_MainFrame (this);

}

CMainFrame::~CMainFrame()
{
	if (m_Splitter != NULL)
	{
		delete m_Splitter;
	}
	if (m_MainSplitter != NULL)
	{
		delete m_MainSplitter;
	}
	if (m_GroupSplitter != NULL)
	{
		delete m_GroupSplitter;
	}
	WINDOWHANDLER->DestroyInstance ();
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Fehler beim Erstellen der Symbolleiste.\n");
		return -1;      // Fehler beim Erstellen
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Fehler beim Erstellen der Statusleiste.\n");
		return -1;      // Fehler beim Erstellen
	}

	// TODO: L�schen Sie diese drei Zeilen, wenn Sie nicht m�chten, dass die Systemleiste andockbar ist
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	HMENU hMenu = lpCreateStruct->hMenu;
	if (hMenu != NULL)
	{
		m_Menu.Attach (hMenu);
		WINDOWHANDLER->set_Menu (&m_Menu);
	}

	return 0;
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/,
	CCreateContext* pContext)
{
	BOOL ret = TRUE;
	m_Splitter = new CSplitter ();
	m_GroupSplitter = new CGroupSplitter ();
	m_MainSplitter = new CMainSplitter ();
	ret = m_Splitter->CreateStatic(this, 1, 2,  WS_CHILD | WS_VISIBLE, AFX_IDW_PANE_FIRST); 
	m_Splitter->CreateView(0,1,RUNTIME_CLASS(CStartView), CSize(0,0), 
		pContext);

	ret = m_MainSplitter->CreateStatic(m_Splitter, 2, 1,  WS_CHILD | WS_VISIBLE, m_Splitter->IdFromRowCol (0,0)); 

	m_MainSplitter->CreateView(0,0,RUNTIME_CLASS(CMainMenu), CSize(0,0), 
      pContext);
	m_MainSplitter->CreateView(1,0,RUNTIME_CLASS(CFavoriteView), CSize(0,0), 
      pContext);

	m_Splitter->SetColumnInfo (0, 200, 10);
	m_Splitter->SetColumnInfo (1, 450, 10);

	CRect rect;
	GetClientRect (&rect);
	m_MainSplitter->SetRowInfo (0, rect.bottom - 100, 10);
	m_MainSplitter->SetRowInfo (1, 50, 0);

	WINDOWHANDLER->SetSplitter (m_Splitter);
	return ret;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE
		 | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE | WS_SYSMENU;

	return TRUE;
}


// CMainFrame-Diagnose

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame-Meldungshandler

void CMainFrame::OnSize (UINT nType, int cx, int cy)
{
	CFrameWnd::OnSize (nType, cx, cy);
	if (!sizeOK && IsWindow (m_MainSplitter->m_hWnd))
	{
/*
		if (WINDOWHANDLER->DockChoice ())
		{
			m_GroupSplitter->SetColumnInfo (0, cx - 450, 10);
			m_GroupSplitter->SetColumnInfo (1, 450, 0);
		}
*/
		m_MainSplitter->SetRowInfo (0, cy - 400, 10);
		m_MainSplitter->SetRowInfo (1, 50, 0);
		sizeOK = TRUE;
	}
}

void CMainFrame::OnDestroy ()
{
	WINDOWHANDLER->DestroyInstance ();
	MESSAGEHANDLER->DeleteInstance ();
	CORE->DeleteInstance ();
	DATABASE->DestroyInstance ();
}

void CMainFrame::OnDockChoice()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (WINDOWHANDLER->DockChoice () && (WINDOWHANDLER->SplitDir () == CWindowHandler::Horizontal))
	{
		m_Menu.CheckMenuItem (ID_DOCK_CHOICE, MF_UNCHECKED);
		WINDOWHANDLER->DeleteChoice ();
	}
	else
	{
		m_Menu.CheckMenuItem (ID_DOCK_BOTTOM, MF_UNCHECKED);
		m_Menu.CheckMenuItem (ID_DOCK_CHOICE, MF_CHECKED);
		WINDOWHANDLER->SplitChoice ();
	}
}

void CMainFrame::OnUpdateBack (CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
//	BOOL Enable = 	WINDOWHANDLER->CanWrite ();
    pCmdUI->Enable (TRUE);
}

void CMainFrame::OnUpdateFileSave (CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	BOOL Enable = 	WINDOWHANDLER->CanWrite ();
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnUpdateDelete (CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	BOOL Enable = 	WINDOWHANDLER->CanWrite ();
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	WINDOWHANDLER->OnBack ();
}

void CMainFrame::OnWrite()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	WINDOWHANDLER->OnWrite ();
}

void CMainFrame::OnDockBottom()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	if (WINDOWHANDLER->DockChoice () && (WINDOWHANDLER->SplitDir () == CWindowHandler::Vertical))
	{
		m_Menu.CheckMenuItem (ID_DOCK_BOTTOM, MF_UNCHECKED);
		WINDOWHANDLER->DeleteBottomChoice ();
	}
	else
	{
		m_Menu.CheckMenuItem (ID_DOCK_CHOICE, MF_UNCHECKED);
		m_Menu.CheckMenuItem (ID_DOCK_BOTTOM, MF_CHECKED);
		WINDOWHANDLER->SplitBottomChoice ();
	}
}

void CMainFrame::OnDockEnabled()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	BOOL mode = TRUE;
	CMenu *menu = &m_Menu;
	int state = menu->GetMenuState (ID_DOCK_ENABLED, MF_BYCOMMAND);
	if (state == MF_CHECKED)
	{
		menu->CheckMenuItem (ID_DOCK_ENABLED, MF_BYCOMMAND | MF_UNCHECKED);
		mode = FALSE;
	}
	else
	{
		menu->CheckMenuItem (ID_DOCK_ENABLED, MF_BYCOMMAND | MF_CHECKED);
		mode = TRUE;
	}
	WINDOWHANDLER->SetDockChoice (mode);
}

void CMainFrame::OnDockAlways()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	BOOL mode = TRUE;
	CMenu *menu = &m_Menu;
	if (menu != NULL)
	{
		int state = menu->GetMenuState (ID_DOCK_ALWAYS, MF_BYCOMMAND);
		if (state == MF_CHECKED)
		{
			menu->CheckMenuItem (ID_DOCK_ALWAYS, MF_BYCOMMAND | MF_UNCHECKED);
			mode = FALSE;
		}
		else
		{
			menu->CheckMenuItem (ID_DOCK_ALWAYS, MF_BYCOMMAND | MF_CHECKED);
			mode = TRUE;
		}
	}
	WINDOWHANDLER->StartWithDock (mode);
}
