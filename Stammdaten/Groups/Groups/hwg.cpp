#include "stdafx.h"
#include "hwg.h"

struct HWG hwg, hwg_null;

void HWG_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &hwg.hwg,  SQLSHORT, 0);
    sqlout ((short *) &hwg.hwg,SQLSHORT,0);
    sqlout ((TCHAR *) hwg.hwg_bz1,SQLCHAR,25);
    sqlout ((TCHAR *) hwg.hwg_bz2,SQLCHAR,25);
    sqlout ((TCHAR *) hwg.sp_kz,SQLCHAR,2);
    sqlout ((long *) &hwg.we_kto,SQLLONG,0);
    sqlout ((long *) &hwg.erl_kto,SQLLONG,0);
    sqlout ((TCHAR *) hwg.durch_vk,SQLCHAR,2);
    sqlout ((TCHAR *) hwg.durch_sk,SQLCHAR,2);
    sqlout ((TCHAR *) hwg.durch_fil,SQLCHAR,2);
    sqlout ((TCHAR *) hwg.durch_sw,SQLCHAR,2);
    sqlout ((TCHAR *) hwg.best_auto,SQLCHAR,2);
    sqlout ((double *) &hwg.sp_vk,SQLDOUBLE,0);
    sqlout ((double *) &hwg.sp_sk,SQLDOUBLE,0);
    sqlout ((double *) &hwg.sp_fil,SQLDOUBLE,0);
    sqlout ((double *) &hwg.sw,SQLDOUBLE,0);
    sqlout ((short *) &hwg.me_einh,SQLSHORT,0);
    sqlout ((short *) &hwg.mwst,SQLSHORT,0);
    sqlout ((short *) &hwg.teil_smt,SQLSHORT,0);
    sqlout ((short *) &hwg.bearb_lad,SQLSHORT,0);
    sqlout ((short *) &hwg.bearb_fil,SQLSHORT,0);
    sqlout ((short *) &hwg.bearb_sk,SQLSHORT,0);
    sqlout ((TCHAR *) hwg.sam_ean,SQLCHAR,2);
    sqlout ((TCHAR *) hwg.smt,SQLCHAR,2);
    sqlout ((TCHAR *) hwg.kost_kz,SQLCHAR,3);
    sqlout ((double *) &hwg.pers_rab,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &hwg.akv,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &hwg.bearb,SQLDATE,0);
    sqlout ((TCHAR *) hwg.pers_nam,SQLCHAR,9);
    sqlout ((short *) &hwg.delstatus,SQLSHORT,0);
    sqlout ((long *) &hwg.erl_kto_1,SQLLONG,0);
    sqlout ((long *) &hwg.erl_kto_2,SQLLONG,0);
    sqlout ((long *) &hwg.erl_kto_3,SQLLONG,0);
    sqlout ((long *) &hwg.we_kto_1,SQLLONG,0);
    sqlout ((long *) &hwg.we_kto_2,SQLLONG,0);
    sqlout ((long *) &hwg.we_kto_3,SQLLONG,0);
    sqlout ((short *) &hwg.shop_sort,SQLSHORT,0);
            cursor = sqlcursor (_T("select hwg.hwg,  ")
_T("hwg.hwg_bz1,  hwg.hwg_bz2,  hwg.sp_kz,  hwg.we_kto,  hwg.erl_kto,  ")
_T("hwg.durch_vk,  hwg.durch_sk,  hwg.durch_fil,  hwg.durch_sw,  ")
_T("hwg.best_auto,  hwg.sp_vk,  hwg.sp_sk,  hwg.sp_fil,  hwg.sw,  hwg.me_einh,  ")
_T("hwg.mwst,  hwg.teil_smt,  hwg.bearb_lad,  hwg.bearb_fil,  hwg.bearb_sk,  ")
_T("hwg.sam_ean,  hwg.smt,  hwg.kost_kz,  hwg.pers_rab,  hwg.akv,  hwg.bearb,  ")
_T("hwg.pers_nam,  hwg.delstatus,  hwg.erl_kto_1,  hwg.erl_kto_2,  ")
_T("hwg.erl_kto_3,  hwg.we_kto_1,  hwg.we_kto_2,  hwg.we_kto_3,  ")
_T("hwg.shop_sort from hwg ")

#line 12 "hwg.rpp"
                                  _T("where hwg = ?"));
    sqlin ((short *) &hwg.hwg,SQLSHORT,0);
    sqlin ((TCHAR *) hwg.hwg_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) hwg.hwg_bz2,SQLCHAR,25);
    sqlin ((TCHAR *) hwg.sp_kz,SQLCHAR,2);
    sqlin ((long *) &hwg.we_kto,SQLLONG,0);
    sqlin ((long *) &hwg.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) hwg.durch_vk,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.durch_sk,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.durch_fil,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.durch_sw,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.best_auto,SQLCHAR,2);
    sqlin ((double *) &hwg.sp_vk,SQLDOUBLE,0);
    sqlin ((double *) &hwg.sp_sk,SQLDOUBLE,0);
    sqlin ((double *) &hwg.sp_fil,SQLDOUBLE,0);
    sqlin ((double *) &hwg.sw,SQLDOUBLE,0);
    sqlin ((short *) &hwg.me_einh,SQLSHORT,0);
    sqlin ((short *) &hwg.mwst,SQLSHORT,0);
    sqlin ((short *) &hwg.teil_smt,SQLSHORT,0);
    sqlin ((short *) &hwg.bearb_lad,SQLSHORT,0);
    sqlin ((short *) &hwg.bearb_fil,SQLSHORT,0);
    sqlin ((short *) &hwg.bearb_sk,SQLSHORT,0);
    sqlin ((TCHAR *) hwg.sam_ean,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.smt,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.kost_kz,SQLCHAR,3);
    sqlin ((double *) &hwg.pers_rab,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &hwg.akv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &hwg.bearb,SQLDATE,0);
    sqlin ((TCHAR *) hwg.pers_nam,SQLCHAR,9);
    sqlin ((short *) &hwg.delstatus,SQLSHORT,0);
    sqlin ((long *) &hwg.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &hwg.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &hwg.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &hwg.we_kto_1,SQLLONG,0);
    sqlin ((long *) &hwg.we_kto_2,SQLLONG,0);
    sqlin ((long *) &hwg.we_kto_3,SQLLONG,0);
    sqlin ((short *) &hwg.shop_sort,SQLSHORT,0);
            sqltext = _T("update hwg set hwg.hwg = ?,  ")
_T("hwg.hwg_bz1 = ?,  hwg.hwg_bz2 = ?,  hwg.sp_kz = ?,  hwg.we_kto = ?,  ")
_T("hwg.erl_kto = ?,  hwg.durch_vk = ?,  hwg.durch_sk = ?,  ")
_T("hwg.durch_fil = ?,  hwg.durch_sw = ?,  hwg.best_auto = ?,  ")
_T("hwg.sp_vk = ?,  hwg.sp_sk = ?,  hwg.sp_fil = ?,  hwg.sw = ?,  ")
_T("hwg.me_einh = ?,  hwg.mwst = ?,  hwg.teil_smt = ?,  hwg.bearb_lad = ?,  ")
_T("hwg.bearb_fil = ?,  hwg.bearb_sk = ?,  hwg.sam_ean = ?,  hwg.smt = ?,  ")
_T("hwg.kost_kz = ?,  hwg.pers_rab = ?,  hwg.akv = ?,  hwg.bearb = ?,  ")
_T("hwg.pers_nam = ?,  hwg.delstatus = ?,  hwg.erl_kto_1 = ?,  ")
_T("hwg.erl_kto_2 = ?,  hwg.erl_kto_3 = ?,  hwg.we_kto_1 = ?,  ")
_T("hwg.we_kto_2 = ?,  hwg.we_kto_3 = ?,  hwg.shop_sort = ? ")

#line 14 "hwg.rpp"
                                  _T("where hwg = ?");
            sqlin ((short *)   &hwg.hwg,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &hwg.hwg,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select hwg from hwg ")
                                  _T("where hwg = ?"));
            sqlin ((short *)   &hwg.hwg,  SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from hwg ")
                                  _T("where hwg = ?"));
    sqlin ((short *) &hwg.hwg,SQLSHORT,0);
    sqlin ((TCHAR *) hwg.hwg_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) hwg.hwg_bz2,SQLCHAR,25);
    sqlin ((TCHAR *) hwg.sp_kz,SQLCHAR,2);
    sqlin ((long *) &hwg.we_kto,SQLLONG,0);
    sqlin ((long *) &hwg.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) hwg.durch_vk,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.durch_sk,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.durch_fil,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.durch_sw,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.best_auto,SQLCHAR,2);
    sqlin ((double *) &hwg.sp_vk,SQLDOUBLE,0);
    sqlin ((double *) &hwg.sp_sk,SQLDOUBLE,0);
    sqlin ((double *) &hwg.sp_fil,SQLDOUBLE,0);
    sqlin ((double *) &hwg.sw,SQLDOUBLE,0);
    sqlin ((short *) &hwg.me_einh,SQLSHORT,0);
    sqlin ((short *) &hwg.mwst,SQLSHORT,0);
    sqlin ((short *) &hwg.teil_smt,SQLSHORT,0);
    sqlin ((short *) &hwg.bearb_lad,SQLSHORT,0);
    sqlin ((short *) &hwg.bearb_fil,SQLSHORT,0);
    sqlin ((short *) &hwg.bearb_sk,SQLSHORT,0);
    sqlin ((TCHAR *) hwg.sam_ean,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.smt,SQLCHAR,2);
    sqlin ((TCHAR *) hwg.kost_kz,SQLCHAR,3);
    sqlin ((double *) &hwg.pers_rab,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &hwg.akv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &hwg.bearb,SQLDATE,0);
    sqlin ((TCHAR *) hwg.pers_nam,SQLCHAR,9);
    sqlin ((short *) &hwg.delstatus,SQLSHORT,0);
    sqlin ((long *) &hwg.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &hwg.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &hwg.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &hwg.we_kto_1,SQLLONG,0);
    sqlin ((long *) &hwg.we_kto_2,SQLLONG,0);
    sqlin ((long *) &hwg.we_kto_3,SQLLONG,0);
    sqlin ((short *) &hwg.shop_sort,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into hwg (hwg,  ")
_T("hwg_bz1,  hwg_bz2,  sp_kz,  we_kto,  erl_kto,  durch_vk,  durch_sk,  durch_fil,  ")
_T("durch_sw,  best_auto,  sp_vk,  sp_sk,  sp_fil,  sw,  me_einh,  mwst,  teil_smt,  ")
_T("bearb_lad,  bearb_fil,  bearb_sk,  sam_ean,  smt,  kost_kz,  pers_rab,  akv,  bearb,  ")
_T("pers_nam,  delstatus,  erl_kto_1,  erl_kto_2,  erl_kto_3,  we_kto_1,  we_kto_2,  ")
_T("we_kto_3,  shop_sort) ")

#line 25 "hwg.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 27 "hwg.rpp"
}
