#include "StdAfx.h"
#include "WgList.h"

CWgList::CWgList(void)
{
	wg = 0;
	wg_bz1 = _T("");
	wg_bz2 = _T("");
}

CWgList::CWgList(short wg, LPTSTR wg_bz1, LPTSTR wg_bz2)
{
	this->wg  = wg;
	this->wg_bz1 = wg_bz1;
	this->wg_bz2 = wg_bz2;
}

CWgList::~CWgList(void)
{
}
