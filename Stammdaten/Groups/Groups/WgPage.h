#pragma once
#include "StaticButton.h"
#include "ColorButton.h"
#include "Label.h"
#include "VLabel.h"
#include "RunMessage.h"
#include "MessageHandler.h"
#include "CtrlGridColor.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "ChoiceHwg.h"
#include "ChoiceWg.h"
#include "UpdateHandler.h"
#include "afxwin.h"
#include "afxdtctl.h"

#define IDC_HWGCHOICE 3001
#define IDC_WGCHOICE 3002

// CWgPage-Dialogfeld

class CWgPage : public CDialog,
                        CUpdateHandler     
{
	DECLARE_DYNAMIC(CWgPage)

public:
	CWgPage(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CWgPage();

// Dialogfelddaten
	enum { IDD = IDD_WG_PAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
	virtual void OnSize (UINT, int, int);

	void OnChoice ();
	void OnHwgChoice ();
    void OnSelected ();
    void OnCanceled ();

	DECLARE_MESSAGE_MAP()
private:
	BOOL m_TestFocus;
	CMessageHandler *MessageHandler;
	CFont Font;
	CFont TitleFont;
	int FontHeight;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	BOOL MustCreate;
	BOOL IsCreated;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid HeadGrid;
	CCtrlGrid WgGrid;
	CCtrlGrid HwgGrid;
	CCtrlGrid DataGrid;
	CCtrlGrid CalcGrid;
	CChoiceWg *Choice;
	CChoiceHwg *ChoiceHwg;
	CFormTab Form;
	BOOL HideOK;
	BOOL m_CanWrite;
	BOOL isNewRec;
	CDataCollection<CWnd *> HeadControls;

public:
	void EnableFields (BOOL b);
	void EnableHeadControls (BOOL b);
	BOOL CanWrite ()
	{
		return m_CanWrite;
	}
	CColorButton m_WgChoice;
	CColorButton m_HwgChoice;
	CStatic m_WgGroup;
	CStatic m_CalcGroup;
	CStatic m_LWg;
	CNumEdit m_Wg;
	CStatic m_LWgBz;
	CTextEdit m_WgBz1;
	CTextEdit m_WgBz2;
	CStatic m_LHwg;
	CNumEdit m_Hwg;
	CStatic m_LHwgBz;
	CTextEdit m_HwgBz1;
	CStatic m_LAbt;
	CNumEdit m_Abt;
	CStatic m_LWeKto;
	CNumEdit m_WeKto;
	CStatic m_LErlKto;
	CNumEdit m_ErlKto;
	CStatic m_LSmt;
	CComboBox m_Smt;
	CStatic m_LTeilSmt;
	CComboBox m_TeilSmt;
	CStatic m_LMwst;
	CComboBox m_Mwst;
	CStatic m_LMeEinh;
	CComboBox m_MeEinh;
	CStatic m_LBestAuto;
	CComboBox m_BestAuto;
	CStatic m_LPersRab;
	CDecimalEdit m_PersRab;
	CButton m_SamEan;
	CStatic m_LKostKz;
	CComboBox m_KostKz;
	CStatic m_LSpKz;
	CComboBox m_SpKz;
	CStatic m_LSpVk;
	CDecimalEdit m_SpVk;
	CStatic m_LSpFil;
	CDecimalEdit m_SpFil;
	CStatic m_LSpSk;
	CDecimalEdit m_SpSk;
	CStatic m_LSw;
	CDecimalEdit m_Sw;
	CStatic m_LBearbLad;
	CNumEdit m_BearbLad;
	CStatic m_LBearbFil;
	CNumEdit m_BearbFil;
	CStatic m_LBearbSk;
	CNumEdit m_BearbSk;
	CButton m_DurchVk;
	CButton m_DurchFil;
	CButton m_DurchSk;
	CButton m_DurchSw;

	CStatic m_LVerkBeg;
	CDateTimeCtrl m_VerkBeg;
	CStatic m_LVerkEnd;
	CDateTimeCtrl m_VerkEnd;

	CStatic m_LVerkArt;
	CComboBox m_VerkArt;
	CStatic m_LUmsVerb;
	CComboBox m_UmsVerb;


    BOOL OnReturn ();
    BOOL OnKeyup ();
	BOOL Read ();
	void Write ();
	void Delete ();
	void GetHwgDefaults ();

// UpdateHandler
	virtual BOOL StepBack ();
	virtual void Update ();
	virtual void UpdateWrite ();
	virtual void UpdateDelete ();


	afx_msg void OnEnKillfocusHwg();
	CButton m_ShopKZ;
};
