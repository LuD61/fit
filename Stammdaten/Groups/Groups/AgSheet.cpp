#include "StdAfx.h"
#include "AgSheet.h"
#include "WindowHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CAgSheet, CPropertySheet)

CAgSheet::CAgSheet(void) : CPropertySheet ()
{
	DlgBkColor = NULL;
	DlgBrush = NULL;
}

CAgSheet::CAgSheet(LPTSTR title) : CPropertySheet (title)
{
	DlgBkColor = NULL;
	DlgBrush = NULL;
}

CAgSheet::~CAgSheet(void)
{
}

BEGIN_MESSAGE_MAP(CAgSheet, CPropertySheet)
//	ON_WM_SIZE ()
//	ON_WM_SHOWWINDOW ()
	ON_WM_ACTIVATE ()
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()

BOOL CAgSheet::OnInitDialog ()
{
	BOOL ret = CPropertySheet::OnInitDialog ();

//	SetWindowPos (NULL, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
	CTabCtrl *pTab = GetTabControl ();
	DWORD style = ::GetWindowLong (pTab->m_hWnd, GWL_STYLE);
	style |= TCS_BUTTONS | TCS_FLATBUTTONS;
//	style |= TCS_BUTTONS;
//    ::SetWindowLong (pTab->m_hWnd, GWL_STYLE, style); 
	pTab->ShowWindow (FALSE);
	return ret;
}

HBRUSH CAgSheet::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	DlgBkColor = WINDOWHANDLER->GetDlgBkColor ();
	DlgBrush   = WINDOWHANDLER->GetDlgBrush ();

	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (OPAQUE);
			WINDOWHANDLER->SetDlgBkColor (DlgBkColor);
			WINDOWHANDLER->SetDlgBrush (DlgBrush);
			return DlgBrush;
	}
	return CPropertySheet::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CAgSheet::OnShowWindow (BOOL bShow, UINT nStatus)
{
	CPropertyPage *page = this->GetActivePage ();
	if (page != NULL)
	{
		if (bShow)
		{
			page->OnSetActive ();
		}
		else
		{
			page->OnKillActive ();
		}
	}
}

void CAgSheet::OnActivate (UINT nState, CWnd* pWndOther,  BOOL bMinimized)
{
	CPropertyPage *page = this->GetActivePage ();
	if (page != NULL)
	{
		if (nState == WA_ACTIVE || nState == WA_CLICKACTIVE)
		{
			page->OnSetActive ();
		}
		else
		{
			page->OnKillActive ();
		}
	}
}

