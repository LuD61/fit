#include "StdAfx.h"
#include "Groups.h"
#include "MainSplitter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CMainSplitter, CSplitterWnd)

CMainSplitter::CMainSplitter(void) : CSplitterWnd ()
{
}

CMainSplitter::~CMainSplitter(void)
{
}

BEGIN_MESSAGE_MAP(CMainSplitter, CSplitterWnd)
//	ON_WM_CTLCOLOR ()
	ON_WM_DESTROY ()
END_MESSAGE_MAP()

void  CMainSplitter::OnDestroy ()
{
	Destroy ();
}

void  CMainSplitter::Destroy ()
{
}

BOOL CMainSplitter::CreateView( int row,int col,CRuntimeClass* pViewClass,SIZE sizeInit,CCreateContext* pContext)
{
// Hier kann eine andere pViewClass als CRuntimeClass eingesetzte werden RUNTIME_CLASS (COwnViw)
	return CSplitterWnd::CreateView (row, col, pViewClass, sizeInit, pContext);
//		return CSplitterWnd::CreateView (row, col, RUNTIME_CLASS (CMainMenu), sizeInit, pContext);
}

HBRUSH CMainSplitter::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CSplitterWnd::OnCtlColor (pDC, pWnd,nCtlColor);
}
