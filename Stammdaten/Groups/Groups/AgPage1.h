#pragma once
#include "StaticButton.h"
#include "ColorButton.h"
#include "Label.h"
#include "VLabel.h"
#include "RunMessage.h"
#include "MessageHandler.h"
#include "CtrlGridColor.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "ChoiceHwg.h"
#include "ChoiceWg.h"
#include "ChoiceAg.h"
#include "UpdateHandler.h"
#include "afxwin.h"
#include "afxdtctl.h"

#define IDC_WGCHOICE 3002
#define IDC_AGCHOICE 3003


// CAgPage1-Dialogfeld

class CAgPage1 : public CPropertyPage,
                        CUpdateHandler     
{
	DECLARE_DYNAMIC(CAgPage1)

public:
	CAgPage1();
	virtual ~CAgPage1();

// Dialogfelddaten
	enum { IDD = IDD_AG_PAGE1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
	afx_msg void OnSize (UINT, int, int);
	afx_msg void OnShowWindow (BOOL bShow, UINT nStatus);
	afx_msg void OnActivate (UINT nState, CWnd* pWndOther,  BOOL bMinimized); 
 
 
 
	DECLARE_MESSAGE_MAP()

	void OnChoice ();
	void OnHwgChoice ();
	void OnWgChoice ();
    void OnSelected ();
    void OnCanceled ();

private:
	BOOL m_TestFocus;
	CMessageHandler *MessageHandler;
	CFont Font;
	CFont TitleFont;
	int FontHeight;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	BOOL MustCreate;
	BOOL IsCreated;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid HeadGrid;
	CCtrlGrid AgGrid;
	CCtrlGrid HwgGrid;
	CCtrlGrid WgGrid;
	CCtrlGrid DataGrid;
	CCtrlGrid CalcGrid;
	CChoiceAG *Choice;
	CChoiceWg *ChoiceWg;
	CFormTab Form;
	BOOL HideOK;
	BOOL m_CanWrite;
	BOOL isNewRec;
	CWnd *m_CurrentFocus;
	CDataCollection<CWnd *> HeadControls;

public:
	void EnableFields (BOOL b);
	void EnableHeadControls (BOOL b);
	BOOL CanWrite ()
	{
		return m_CanWrite;
	}

	void set_CurrentFocus (CWnd *currentFocus)
	{
		m_CurrentFocus = currentFocus;
	}

	CWnd *CurrentFocus ()
	{
		return m_CurrentFocus;
	}
	CColorButton m_AgChoice;
	CColorButton m_WgChoice;
	CStatic m_AgGroup;
	CStatic m_CalcGroup;
	CStatic m_LAg;
	CNumEdit m_Ag;
	CStatic m_LAgBz;
	CTextEdit m_AgBz1;
	CTextEdit m_AgBz2;
	CStatic m_LWg;
	CNumEdit m_Wg;
	CStatic m_LWgBz;
	CTextEdit m_WgBz1;
	CStatic m_LHwg;
	CNumEdit m_Hwg;
	CStatic m_LHwgBz;
	CTextEdit m_HwgBz1;
	CStatic m_LAbt;
	CNumEdit m_Abt;
	CStatic m_LTierKz;
	CComboBox m_TierKz;
	CStatic m_LWeKto;
	CNumEdit m_WeKto;
	CStatic m_LErlKto;
	CNumEdit m_ErlKto;
	CStatic m_LSmt;
	CComboBox m_Smt;
	CStatic m_LTeilSmt;
	CComboBox m_TeilSmt;
	CStatic m_LMwst;
	CComboBox m_Mwst;
	CStatic m_LMeEinh;
	CComboBox m_MeEinh;
	CStatic m_LBestAuto;
	CComboBox m_BestAuto;
	CStatic m_LPersRab;
	CDecimalEdit m_PersRab;
	CButton m_SamEan;
	CStatic m_LKostKz;
	CComboBox m_KostKz;
	CStatic m_LSpKz;
	CComboBox m_SpKz;
	CStatic m_LSpVk;
	CDecimalEdit m_SpVk;
	CStatic m_LSpFil;
	CDecimalEdit m_SpFil;
	CStatic m_LSpSk;
	CDecimalEdit m_SpSk;
	CStatic m_LSw;
	CDecimalEdit m_Sw;
	CStatic m_LBearbLad;
	CNumEdit m_BearbLad;
	CStatic m_LBearbFil;
	CNumEdit m_BearbFil;
	CStatic m_LBearbSk;
	CNumEdit m_BearbSk;
	CButton m_DurchVk;
	CButton m_DurchFil;
	CButton m_DurchSk;
	CButton m_DurchSw;

	CStatic m_LVerkBeg;
	CDateTimeCtrl m_VerkBeg;
	CStatic m_LVerkEnd;
	CDateTimeCtrl m_VerkEnd;

	CStatic m_LVerkArt;
	CComboBox m_VerkArt;

	CStatic m_LArtTyp;
	CComboBox m_ArtTyp;

	CStatic m_LUmsVerb;
	CComboBox m_UmsVerb;


    BOOL OnReturn ();
    BOOL OnKeyup ();
	BOOL Read ();
	void Write ();
	void Delete ();
	afx_msg void OnEnKillfocusWg();
	void DisableTabControl (BOOL disable);
	void GetWgDefaults ();
	virtual BOOL OnSetActive( );
	virtual BOOL OnKillActive( );

// UpdateHandler
	virtual BOOL StepBack ();
	virtual void Update ();
	virtual void UpdateWrite ();
	virtual void UpdateDelete ();
};
