#include "stdafx.h"
#include "ChoiceHwg.h"
#include "DbUniCode.h"
#include "Process.h"
#include "WindowHandler.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceHwg::Sort1 = -1;
int CChoiceHwg::Sort2 = -1;
int CChoiceHwg::Sort3 = -1;
int CChoiceHwg::Sort4 = -1;

CChoiceHwg::CChoiceHwg(CWnd* pParent)
        : CChoiceX(pParent)
{
	Where = "";
}

CChoiceHwg::~CChoiceHwg()
{
	DestroyList ();
}

BEGIN_MESSAGE_MAP(CChoiceHwg, CChoiceX)
	//{{AFX_MSG_MAP(CChoiceLief)
	ON_WM_MOVE ()
END_MESSAGE_MAP()

void CChoiceHwg::DestroyList()
{
	for (std::vector<CHwgList *>::iterator pabl = HwgList.begin (); pabl != HwgList.end (); ++pabl)
	{
		CHwgList *abl = *pabl;
		delete abl;
	}
    HwgList.clear ();
	SelectList.clear ();
}

void CChoiceHwg::FillList ()
{
    short  hwg;
    TCHAR hwg_bz1 [50];
    TCHAR hwg_bz2 [50];

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Hauptwarengruppe"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Hauptwarengruppe"), 1, 150, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung 1"),	2, 250);
    SetCol (_T("Bezeichnung 2"),	3, 250);

	if (HwgList.size () == 0)
	{
		DbClass->sqlout ((short *)&hwg,         SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  hwg_bz1,     SQLCHAR, sizeof (hwg_bz1));
		DbClass->sqlout ((LPTSTR)  hwg_bz2,     SQLCHAR, sizeof (hwg_bz2));
		CString Sql = _T("select hwg, hwg_bz1, hwg_bz2 from hwg where hwg > 0");
		Sql += " ";
		Sql += Where;
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) hwg_bz1;
			CDbUniCode::DbToUniCode (hwg_bz1, pos);
			pos = (LPSTR) hwg_bz2;
			CDbUniCode::DbToUniCode (hwg_bz2, pos);
			CHwgList *abl = new CHwgList (hwg, hwg_bz1, hwg_bz2);
			HwgList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CHwgList *>::iterator pabl = HwgList.begin (); pabl != HwgList.end (); ++pabl)
	{
		CHwgList *abl = *pabl;
		CString Hwg;
		Hwg.Format (_T("%d"), abl->hwg);
		CString Num;
		CString Bez;
		_tcscpy (hwg_bz1, abl->hwg_bz1.GetBuffer ());
		_tcscpy (hwg_bz2, abl->hwg_bz2.GetBuffer ());

        int ret = InsertItem (i, -1);
        ret = SetItemText (Hwg.GetBuffer (), i, 1);
        ret = SetItemText (hwg_bz1, i, 2);
        ret = SetItemText (hwg_bz2, i, 3);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}





void CChoiceHwg::Search (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, SortRow);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceHwg::Search ()
{
    CString EditText;

    CEdit *SearchText = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (SearchText == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    SearchText->GetWindowText (EditText);
    Search (ListBox, EditText.GetBuffer (8));
}

int CChoiceHwg::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceHwg::CompareProc(LPARAM lParam1,
						 		     LPARAM lParam2,
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}


void CChoiceHwg::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CHwgList *abl = HwgList [i];

		   abl->hwg = _tstoi (ListBox->GetItemText (i, 1));
		   abl->hwg_bz1 = ListBox->GetItemText (i, 2);
		   abl->hwg_bz2 = ListBox->GetItemText (i, 3);
	}
	for (int i = 1; i <= 9; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);
}

void CChoiceHwg::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = HwgList [idx];
}

CHwgList *CChoiceHwg::GetSelectedText ()
{
	CHwgList *abl = (CHwgList *) SelectedRow;
	return abl;
}

void CChoiceHwg::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (HwgList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

void CChoiceHwg::OnEnter ()
{
/*
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cAg = m_List.GetItemText (idx, 1);  
		long ag = _tstol (cAg.GetBuffer ());
		Message.Format (_T("HWG=%ld"), ag);
		ToClipboard (Message);
	}
	p.SetCommand (_T("rswrun 13300"));
	HANDLE Pid = p.Start (SW_SHOWNORMAL);
*/
}

void CChoiceHwg::OnMove (int x, int y)
{
	WINDOWHANDLER->IsDockPlace (x, y);
}