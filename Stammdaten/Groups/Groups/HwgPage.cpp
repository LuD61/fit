// HwgPage.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Groups.h"
#include "HwgPage.h"
#include "WindowHandler.h"
#include "Core.h"
#include "DatabaseCore.h"
#include "uniformfield.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CHwgPage-Dialogfeld

IMPLEMENT_DYNAMIC(CHwgPage, CDialog)

CHwgPage::CHwgPage(CWnd* pParent /*=NULL*/)
	: CDialog(CHwgPage::IDD, pParent)
{
	Choice = NULL;
	HideOK = FALSE;
	DlgBkColor = NULL;
	DlgBrush = NULL;
	m_CanWrite = FALSE;
	HeadControls.Add (&m_Hwg);
	WINDOWHANDLER->SetBase (this);
	WINDOWHANDLER->RegisterUpdate (this);
}

CHwgPage::~CHwgPage()
{
	WINDOWHANDLER->SetBase (NULL);
	WINDOWHANDLER->UnregisterUpdate (this);
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Form.Init ();
	HeadControls.Destroy ();
	Choice = (CChoiceHwg *) WINDOWHANDLER->ChoiceHwg ();
	if (Choice != NULL && IsWindow (Choice->m_hWnd))
	{
			delete Choice;
	}
}

void CHwgPage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LHWG, m_LHwg);
	DDX_Control(pDX, IDC_HWG, m_Hwg);
	DDX_Control(pDX, IDC_HWG_GROUP, m_HwgGroup);
	DDX_Control(pDX, IDC_LHWG_BZ, m_LHwgBz);
	DDX_Control(pDX, IDC_HWG_BZ1, m_HwgBz1);
	DDX_Control(pDX, IDC_HWG_BZ2, m_HwgBz2);
	DDX_Control(pDX, IDC_LWA_KTO, m_LWeKto);
	DDX_Control(pDX, IDC_WE_KTO, m_WeKto);
	DDX_Control(pDX, IDC_LERL_KTO, m_LErlKto);
	DDX_Control(pDX, IDC_ERL_KTO, m_ErlKto);
	DDX_Control(pDX, IDC_LSHOPSORT, m_LShopSort);
	DDX_Control(pDX, IDC_SHOPSORT, m_ShopSort);
	DDX_Control(pDX, IDC_LMWST, m_LMwst);
	DDX_Control(pDX, IDC_MWST, m_Mwst);
	DDX_Control(pDX, IDC_LME_EINH, m_LMeEinh);
	DDX_Control(pDX, IDC_ME_EINH, m_MeEinh);
	DDX_Control(pDX, IDC_LBEST_AUTO, m_LBestAuto);
	DDX_Control(pDX, IDC_BEST_AUTO, m_BestAuto);
	DDX_Control(pDX, IDC_LPERS_RAB, m_LPersRab);
	DDX_Control(pDX, IDC_PERS_RAB, m_PersRab);
	DDX_Control(pDX, IDC_LSMT, m_LSmt);
	DDX_Control(pDX, IDC_SMT, m_Smt);
	DDX_Control(pDX, IDC_LTEIL_SMT, m_LTeilSmt);
	DDX_Control(pDX, IDC_TEIL_SMT, m_TeilSmt);
	DDX_Control(pDX, IDC_SAM_EAN, m_SamEan);
	DDX_Control(pDX, IDC_CALC_GROUP, m_CalcGroup);
	DDX_Control(pDX, IDC_LKOST_KZ, m_LKostKz);
	DDX_Control(pDX, IDC_KOST_KZ, m_KostKz);
	DDX_Control(pDX, IDC_LSP_KZ, m_LSpKz);
	DDX_Control(pDX, IDC_SP_KZ, m_SpKz);
	DDX_Control(pDX, IDC_LSP_VK, m_LSpVk);
	DDX_Control(pDX, IDC_SP_VK, m_SpVk);
	DDX_Control(pDX, IDC_LSP_FIL, m_LSpFil);
	DDX_Control(pDX, IDC_SP_FIL, m_SpFil);
	DDX_Control(pDX, IDC_LSP_SK, m_LSpSk);
	DDX_Control(pDX, IDC_SP_SK, m_SpSk);
	DDX_Control(pDX, IDC_LSW, m_LSw);
	DDX_Control(pDX, IDC_SW, m_Sw);

	DDX_Control(pDX, IDC_LBEARB_LAD, m_LBearbLad);
	DDX_Control(pDX, IDC_BEARB_LAD, m_BearbLad);
	DDX_Control(pDX, IDC_LBEARB_FIL, m_LBearbFil);
	DDX_Control(pDX, IDC_BEARB_FIL, m_BearbFil);
	DDX_Control(pDX, IDC_LBEARB_SK, m_LBearbSk);
	DDX_Control(pDX, IDC_BEARB_SK, m_BearbSk);

	DDX_Control(pDX, IDC_DURCH_VK,  m_DurchVk);
	DDX_Control(pDX, IDC_DURCH_FIL, m_DurchFil);
	DDX_Control(pDX, IDC_DURCH_SK,  m_DurchSk);
	DDX_Control(pDX, IDC_DURCH_SW,  m_DurchSw);	
}


BEGIN_MESSAGE_MAP(CHwgPage, CDialog)
//	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_COMMAND(IDC_HWGCHOICE , OnChoice)
	ON_COMMAND (SELECTED, OnSelected)
	ON_COMMAND (CANCELED, OnCanceled)
END_MESSAGE_MAP()

BOOL CHwgPage::OnInitDialog()
{

	CDialog::OnInitDialog();
	DATABASE->InitHwg ();

#ifdef BLUE_COL
	COLORREF BkColor (RGB (100, 180, 255));
	COLORREF DynColor = m_NewAngTyp.DynColorBlue;
	COLORREF RolloverColor = RGB (204, 204, 255);
#else
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
#endif

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else if (GetSystemMetrics (SM_CXFULLSCREEN) <= 1300)
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (105, _T("Dlg"));
	}

	CRect rect;
	m_SpFil.GetClientRect (&rect); 

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 17;
	TitleFont.CreateFontIndirect (&l);

    Form.Add (new CFormField (&m_Hwg,EDIT,         (short *) &THWG.hwg, VSHORT));
    Form.Add (new CUniFormField (&m_HwgBz1,EDIT,   (char *)  THWG.hwg_bz1, VCHAR, sizeof (THWG.hwg_bz1)));
    Form.Add (new CUniFormField (&m_HwgBz2,EDIT,   (char *)  THWG.hwg_bz2, VCHAR, sizeof (THWG.hwg_bz2)));
    Form.Add (new CFormField (&m_WeKto,EDIT,       (long * ) &THWG.we_kto, VLONG, 8));
    Form.Add (new CFormField (&m_ErlKto,EDIT,      (long * ) &THWG.erl_kto, VLONG, 8));
	Form.Add (new CFormField (&m_ShopSort,EDIT,      (long * ) &THWG.shop_sort, VLONG, 8));
    Form.Add (new CFormField (&m_Smt,  COMBOBOX,   (char * ) THWG.smt, VCHAR));
	CORE->FillCombo (_T("smt"), &Form.GetFormField (&m_Smt)->ComboValues);
	Form.GetFormField (&m_Smt)->FillComboBox ();
	m_Smt.SetDroppedWidth (200);
    Form.Add (new CFormField (&m_TeilSmt,  COMBOBOX,   (short * ) &THWG.teil_smt, VSHORT));
	CORE->FillCombo (_T("teil_smt"), &Form.GetFormField (&m_TeilSmt)->ComboValues);
	Form.GetFormField (&m_TeilSmt)->FillComboBox ();
	m_TeilSmt.SetDroppedWidth (200);

    Form.Add (new CFormField (&m_Mwst,  COMBOBOX,  (short * )&THWG.mwst, VSHORT));
	CORE->FillCombo (_T("mwst"), &Form.GetFormField (&m_Mwst)->ComboValues);
	Form.GetFormField (&m_Mwst)->FillComboBox ();

    Form.Add (new CFormField (&m_MeEinh,  COMBOBOX,  (short * )&THWG.me_einh, VSHORT));
	CORE->FillCombo (_T("me_einh"), &Form.GetFormField (&m_MeEinh)->ComboValues);
	Form.GetFormField (&m_MeEinh)->FillComboBox ();

    Form.Add (new CFormField (&m_BestAuto,  COMBOBOX,  (char * )THWG.best_auto, VCHAR));
	CORE->FillCombo (_T("best_auto"), &Form.GetFormField (&m_BestAuto)->ComboValues);
	Form.GetFormField (&m_BestAuto)->FillComboBox ();
	m_BestAuto.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_PersRab,EDIT,       (double * ) &THWG.pers_rab, VDOUBLE, 5, 2));
    Form.Add (new CFormField (&m_SamEan, CHECKBOX,   (char * )   THWG.sam_ean, VCHAR));

    Form.Add (new CFormField (&m_KostKz,  COMBOBOX,  (char * )  THWG.kost_kz, VCHAR));
	CORE->FillCombo (_T("kost_kz"), &Form.GetFormField (&m_KostKz)->ComboValues);
	Form.GetFormField (&m_KostKz)->FillComboBox ();
	m_KostKz.SetDroppedWidth (300);

	Form.Add (new CFormField (&m_SpKz,  COMBOBOX,  (char * )  THWG.sp_kz, VCHAR));
	CORE->FillCombo (_T("sp_kz"), &Form.GetFormField (&m_SpKz)->ComboValues);
	Form.GetFormField (&m_SpKz)->FillComboBox ();
	m_SpKz.SetDroppedWidth (300);

    Form.Add (new CFormField (&m_SpVk,EDIT,       (double * ) &THWG.sp_vk, VDOUBLE, 5, 1));
    Form.Add (new CFormField (&m_SpFil,EDIT,      (double * ) &THWG.sp_fil, VDOUBLE, 5, 1));
    Form.Add (new CFormField (&m_SpSk,EDIT,       (double * ) &THWG.sp_sk, VDOUBLE, 5, 1));
    Form.Add (new CFormField (&m_Sw,EDIT,         (double * ) &THWG.sw, VDOUBLE, 3, 1));

    Form.Add (new CFormField (&m_BearbLad, EDIT,   (short * )  &THWG.bearb_lad, VSHORT, 4));
    Form.Add (new CFormField (&m_BearbFil, EDIT,   (short * )  &THWG.bearb_fil, VSHORT, 4));
    Form.Add (new CFormField (&m_BearbSk,  EDIT,   (short * )  &THWG.bearb_sk,  VSHORT, 4));

    Form.Add (new CFormField (&m_DurchVk,  CHECKBOX,   (char * )   THWG.durch_vk, VCHAR));
    Form.Add (new CFormField (&m_DurchFil, CHECKBOX,   (char * )   THWG.durch_fil, VCHAR));
    Form.Add (new CFormField (&m_DurchSk,  CHECKBOX,   (char * )   THWG.durch_sk, VCHAR));
    Form.Add (new CFormField (&m_DurchSw,  CHECKBOX,   (char * )   THWG.durch_sw, VCHAR));

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//Grid Hwgnummer

	HwgGrid.Create (this, 2, 2);
    HwgGrid.SetBorder (0, 0);
    HwgGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Hwg = new CCtrlInfo (&m_Hwg, 0, 0, 1, 1);
	HwgGrid.Add (c_Hwg);
	CtrlGrid.CreateChoiceButton (m_HwgChoice, IDC_HWGCHOICE, this);
	CCtrlInfo *c_HwgChoice = new CCtrlInfo (&m_HwgChoice, 1, 0, 1, 1);
	HwgGrid.Add (c_HwgChoice);

    DataGrid.Create (this, 20, 20);
    DataGrid.SetBorder (0, 0);
	DataGrid.SetCellHeight (15);
    DataGrid.SetGridSpace (5, 15);  //Spaltenabstand und Zeilenabstand
	CCtrlInfo *c_HwgGroup = new CCtrlInfo (&m_HwgGroup, 0, 0, DOCKRIGHT, DOCKBOTTOM);
//	c_HwgGroup->BottomSpace = 30;
	c_HwgGroup->SetBottomControl (this, &m_BestAuto, 10, CCtrlInfo::Bottom);
	DataGrid.Add (c_HwgGroup);
	CCtrlInfo *c_LHwgBz = new CCtrlInfo (&m_LHwgBz, 1, 1, 1, 1);
	DataGrid.Add (c_LHwgBz);
	CCtrlInfo *c_HwgBz1 = new CCtrlInfo (&m_HwgBz1, 2, 1, 1, 1);
	DataGrid.Add (c_HwgBz1);
	CCtrlInfo *c_HwgBz2 = new CCtrlInfo (&m_HwgBz2, 2, 2, 1, 1);
	DataGrid.Add (c_HwgBz2);
	CCtrlInfo *c_LWeKto = new CCtrlInfo (&m_LWeKto, 1, 4, 1, 1);
	DataGrid.Add (c_LWeKto);
	CCtrlInfo *c_WeKto = new CCtrlInfo (&m_WeKto, 2, 4, 1, 1);
	DataGrid.Add (c_WeKto);
	CCtrlInfo *c_LErlKto = new CCtrlInfo (&m_LErlKto, 1, 5, 1, 1);
	DataGrid.Add (c_LErlKto);
	CCtrlInfo *c_ErlKto = new CCtrlInfo (&m_ErlKto, 2, 5, 1, 1);
	DataGrid.Add (c_ErlKto);
	CCtrlInfo *c_LShopSort = new CCtrlInfo (&m_LShopSort, 1, 6, 1, 1);
	DataGrid.Add (c_LShopSort);
	CCtrlInfo *c_ShopSort = new CCtrlInfo (&m_ShopSort, 2, 6, 1, 1);
	DataGrid.Add (c_ShopSort);
	CCtrlInfo *c_LSmt = new CCtrlInfo (&m_LSmt, 3, 4, 1, 1);
	DataGrid.Add (c_LSmt);
	CCtrlInfo *c_Smt = new CCtrlInfo (&m_Smt, 4, 4, 1, 1);
	DataGrid.Add (c_Smt);
	CCtrlInfo *c_LTeilSmt = new CCtrlInfo (&m_LTeilSmt, 3, 5, 1, 1);
	DataGrid.Add (c_LTeilSmt);
	CCtrlInfo *c_TeilSmt = new CCtrlInfo (&m_TeilSmt, 4, 5, 1, 1);
	DataGrid.Add (c_TeilSmt);

	CCtrlInfo *c_LMwst = new CCtrlInfo (&m_LMwst, 1, 7, 1, 1);
	DataGrid.Add (c_LMwst);
	CCtrlInfo *c_Mwst = new CCtrlInfo (&m_Mwst, 2, 7, 1, 1);
	DataGrid.Add (c_Mwst);
	CCtrlInfo *c_LMeEinh = new CCtrlInfo (&m_LMeEinh, 1, 8, 1, 1);
	DataGrid.Add (c_LMeEinh);
	CCtrlInfo *c_MeEinh = new CCtrlInfo (&m_MeEinh, 2, 8, 1, 1);
	DataGrid.Add (c_MeEinh);
	CCtrlInfo *c_LBestAuto = new CCtrlInfo (&m_LBestAuto, 1, 9, 1, 1);
	DataGrid.Add (c_LBestAuto);
	CCtrlInfo *c_BestAuto = new CCtrlInfo (&m_BestAuto, 2, 9, 1, 1);
	DataGrid.Add (c_BestAuto);
	CCtrlInfo *c_LPersRab = new CCtrlInfo (&m_LPersRab, 3, 7, 1, 1);
	DataGrid.Add (c_LPersRab);
	CCtrlInfo *c_PersRab = new CCtrlInfo (&m_PersRab, 4, 7, 1, 1);
	DataGrid.Add (c_PersRab);
	CCtrlInfo *c_SamEan = new CCtrlInfo (&m_SamEan, 3, 8, 1, 1);
	DataGrid.Add (c_SamEan);

	CCtrlInfo *c_CalcGroup = new CCtrlInfo (&m_CalcGroup, 0, 12, DOCKRIGHT, DOCKBOTTOM);
	c_CalcGroup->SetBottomControl (this, &m_Sw, 10, CCtrlInfo::Bottom);
	DataGrid.Add (c_CalcGroup);

	CCtrlInfo *c_LKostKz = new CCtrlInfo (&m_LKostKz, 1, 13, 1, 1);
	DataGrid.Add (c_LKostKz);
	CCtrlInfo *c_KostKz = new CCtrlInfo (&m_KostKz, 2, 13, 1, 1);
	DataGrid.Add (c_KostKz);

	CCtrlInfo *c_LSpKz = new CCtrlInfo (&m_LSpKz, 3, 13, 1, 1);
	DataGrid.Add (c_LSpKz);
	CCtrlInfo *c_SpKz = new CCtrlInfo (&m_SpKz, 4, 13, 1, 1);
	DataGrid.Add (c_SpKz);

    CalcGrid.Create (this, 20, 20);
    CalcGrid.SetBorder (0, 0);
	CalcGrid.SetCellHeight (15);
    CalcGrid.SetGridSpace (5, 15);  //Spaltenabstand und Zeilenabstand
	CCtrlInfo *c_LSpVk = new CCtrlInfo (&m_LSpVk, 1, 0, 1, 1);
	CalcGrid.Add (c_LSpVk);
	CCtrlInfo *c_SpVk = new CCtrlInfo (&m_SpVk, 2, 0, 1, 1);
	CalcGrid.Add (c_SpVk);
	CCtrlInfo *c_LSpFil = new CCtrlInfo (&m_LSpFil, 1, 1, 1, 1);
	CalcGrid.Add (c_LSpFil);
	CCtrlInfo *c_SpFil = new CCtrlInfo (&m_SpFil, 2, 1, 1, 1);
//	c_SpFil->Name = _T("sp_fil");
	CalcGrid.Add (c_SpFil);
	CCtrlInfo *c_LSpSk = new CCtrlInfo (&m_LSpSk, 1, 2, 1, 1);
	CalcGrid.Add (c_LSpSk);
	CCtrlInfo *c_SpSk = new CCtrlInfo (&m_SpSk, 2, 2, 1, 1);
	CalcGrid.Add (c_SpSk);
	CCtrlInfo *c_LSw = new CCtrlInfo (&m_LSw, 1, 3, 1, 1);
	CalcGrid.Add (c_LSw);
	CCtrlInfo *c_Sw = new CCtrlInfo (&m_Sw, 2, 3, 1, 1);
	CalcGrid.Add (c_Sw);

	CCtrlInfo *c_LBearbLad = new CCtrlInfo (&m_LBearbLad, 3, 0, 1, 1);
	CalcGrid.Add (c_LBearbLad);
	CCtrlInfo *c_BearbLad = new CCtrlInfo (&m_BearbLad, 4, 0, 1, 1);
	CalcGrid.Add (c_BearbLad);
	CCtrlInfo *c_LBearbFil = new CCtrlInfo (&m_LBearbFil, 3, 1, 1, 1);
	CalcGrid.Add (c_LBearbFil);
	CCtrlInfo *c_BearbFil = new CCtrlInfo (&m_BearbFil, 4, 1, 1, 1);
	CalcGrid.Add (c_BearbFil);
	CCtrlInfo *c_LBearbSk = new CCtrlInfo (&m_LBearbSk, 3, 2, 1, 1);
	CalcGrid.Add (c_LBearbSk);
	CCtrlInfo *c_BearbSk = new CCtrlInfo (&m_BearbSk, 4, 2, 1, 1);
	CalcGrid.Add (c_BearbSk);

	CCtrlInfo *c_DurchVk = new CCtrlInfo (&m_DurchVk, 5, 0, 1, 1);
	c_DurchVk->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchVk);
	CCtrlInfo *c_DurchFil = new CCtrlInfo (&m_DurchFil, 5, 1, 1, 1);
	c_DurchFil->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchFil);
	CCtrlInfo *c_DurchSk = new CCtrlInfo (&m_DurchSk, 5, 2, 1, 1);
	c_DurchSk->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchSk);
	CCtrlInfo *c_DurchSw = new CCtrlInfo (&m_DurchSw, 5, 3, 1, 1);
	c_DurchSw->SetCellPos (20, 0);
	CalcGrid.Add (c_DurchSw);



	CCtrlInfo *c_CalcGrid = new CCtrlInfo (&CalcGrid, 1, 16, 4, 1);
	DataGrid.Add (c_CalcGrid);


	CCtrlInfo *c_LHwg = new CCtrlInfo (&m_LHwg, 0, 0, 1, 1);
	CtrlGrid.Add (c_LHwg);

	CCtrlInfo *c_HwgGrid = new CCtrlInfo (&HwgGrid, 1, 0, 1, 1);
	CtrlGrid.Add (c_HwgGrid);

	CCtrlInfo *c_DataGrid = new CCtrlInfo (&DataGrid, 0, 2, DOCKRIGHT, 1);
	CtrlGrid.Add (c_DataGrid);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	Form.Show ();
	CtrlGrid.Display ();
//	DataGrid.Enable (FALSE);
	EnableFields (FALSE);


	PostMessage (WM_SETFOCUS, (WPARAM) m_Hwg.m_hWnd, 0l);

	return FALSE;
}


// CHwgPage-Meldungshandler

HBRUSH CHwgPage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	DlgBkColor = WINDOWHANDLER->GetDlgBkColor ();
	DlgBrush   = WINDOWHANDLER->GetDlgBrush ();
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
				WINDOWHANDLER->SetDlgBrush (DlgBrush);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
				WINDOWHANDLER->SetDlgBrush (DlgBrush);
			}
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CHwgPage::OnSize (UINT nType, int cx, int cy)
{
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
	CtrlGrid.Display ();
}


BOOL CHwgPage::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F5)
			{
				if (!StepBack ())
				{
					WINDOWHANDLER->DestroyFrame ();
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F7)
			{
//				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				OnChoice ();
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Hwg)
				{
					OnChoice ();
					return TRUE;
				}
			}
			break;
	}
	return FALSE;
}

BOOL CHwgPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Hwg)
	{
		if (!Read ())
		{
			return FALSE;
		}
	}

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
	}
	return TRUE;
}

BOOL CHwgPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	Control = GetNextDlgTabItem (Control, TRUE);
    if (Control != NULL)
	{
	 	Control->SetFocus ();
		return TRUE;
	}
	return FALSE;
}


void CHwgPage::OnChoice ()
{
	if (Choice != NULL && WINDOWHANDLER->ChoiceHwg () == NULL)
	{
		Choice = NULL;
	}

	if (Choice == NULL || Choice->HwgList.size () == 0)
	{
		Choice = new CChoiceHwg (this);
		Choice->IsModal = FALSE;
		Choice->SetBitmapButton (TRUE);
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->HideFilter = FALSE;
		Choice->HideOK    = HideOK;
//		Choice->SetDbClass (&DATABASE->Hwg);
		Choice->DlgBkColor = DlgBkColor;
	}
	WINDOWHANDLER->OnChoice (Choice, GetParent ());
}

void CHwgPage::OnSelected ()
{
	int ret = TRUE;
	BOOL CanRestore = FALSE;
	Choice = (CChoiceHwg *) WINDOWHANDLER->GetChoice ();
	if (Choice == NULL) return;

	isNewRec = FALSE;
	Form.Get ();
	if (CanWrite ())
	{
		Write ();
		CanRestore = TRUE;
	}
    CHwgList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
	CORE->SaveHwg ();
	THWG.hwg = abl->hwg;
    Form.Show ();
	if (!DATABASE->ReadHwg () && CanRestore)
	{
		if (DATABASE->GetSqlState () == CDatabaseCore::SqlLock)
		{
			AfxMessageBox (_T("Der Satz wird im Moment bearbeitet"), MB_OK | MB_ICONERROR);
		}
		CORE->RestoreHwg ();
		Form.Show ();
		Read ();
	}
	Form.Show ();
	EnableFields (TRUE);
	if (Choice->FocusBack)
	{
		Choice->SetListFocus ();
	}
	m_CanWrite = TRUE;
	WINDOWHANDLER->set_CanWrite (m_CanWrite);
}

void CHwgPage::OnCanceled ()
{
	Choice = (CChoiceHwg *) WINDOWHANDLER->GetChoice ();
	Choice->ShowWindow (SW_HIDE);
}


void CHwgPage::EnableFields (BOOL b)
{

	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	EnableHeadControls (!b);
}

void CHwgPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Start ();
	CWnd **it;
	CWnd *Control;
	while ((it = HeadControls.GetNext ()) != NULL)
	{
		Control = *it;
		if (Control != NULL)
		{
			Control->EnableWindow (enable);
		}
	}
}

BOOL CHwgPage::StepBack ()
{
	if (IsWindowVisible ())
	{
		isNewRec = FALSE;
		if (!CanWrite ())
		{
			return FALSE;
		}
	//	DATABASE->Lief.rollbackwork ();
		EnableFields (FALSE);
		m_CanWrite = FALSE;
		m_Hwg.SetFocus ();
	}
	return TRUE;
}

BOOL CHwgPage::Read ()
{
	BOOL ret = TRUE;
	isNewRec = FALSE;
	CDatabaseCore::SQL_STATE sqlstate = CDatabaseCore::SqlOk;
	Form.Get ();
	if (!DATABASE->ReadHwg ())
	{
		sqlstate = DATABASE->GetSqlState ();
	}
	if (sqlstate == CDatabaseCore::SqlLock)
	{
		AfxMessageBox (_T("Der Satz wird im Moment bearbeitet"), MB_OK | MB_ICONERROR);
		m_Hwg.SetFocus ();
		ret = FALSE;
	}
	if  (sqlstate == CDatabaseCore::SqlNoData)
	{
		AfxMessageBox (_T("Neuer Satz"));
		isNewRec = TRUE;
	}
	if (ret)
	{
		Form.Show ();
		EnableFields (TRUE);
		m_HwgBz1.SetFocus ();
		m_CanWrite = TRUE;
		WINDOWHANDLER->set_CanWrite (m_CanWrite);
	}
	return ret;
}

void CHwgPage::Write ()
{
	if (!IsWindowVisible ())
	{
		return;
	}
	CWnd *focus = GetFocus ();
	Form.Get ();
	CORE->SetHwgDateFields ();
	if (DATABASE->UpdateHwg ())
	{
		EnableFields (FALSE);
		m_Hwg.SetFocus ();
		m_CanWrite = FALSE;
		WINDOWHANDLER->set_CanWrite (m_CanWrite);
		focus = &m_Hwg;
		Choice = (CChoiceHwg *) WINDOWHANDLER->GetChoice ();
		if (Choice != NULL && isNewRec)
		{
			if (Choice->HwgList.size () != 0) // nur dann neu laden, wenn schon geladen wurde
			{
				Choice->FillList ();
			}
		}
		WINDOWHANDLER->Update ();
	}
	else
	{
		AfxMessageBox (_T("Satz kan nicht geschrieben werden"), MB_OK | MB_ICONERROR);
	}
	focus->SetFocus ();
	isNewRec = FALSE;
}

void CHwgPage::Delete ()
{
	CWnd *focus = GetFocus ();
	if (AfxMessageBox (_T("Hauptwarengruppe l�schen ?"), MB_YESNO| MB_ICONQUESTION) != IDYES)
	{
		focus->SetFocus ();
		return;
	}
	Form.Get ();

	if (DATABASE->DeleteHwg ())
	{
		EnableFields (FALSE);
		m_Hwg.SetFocus ();
		m_CanWrite = FALSE;
		WINDOWHANDLER->set_CanWrite (m_CanWrite);
		focus = &m_Hwg;
		Choice = (CChoiceHwg *) WINDOWHANDLER->GetChoice ();
		if (Choice != NULL)
		{
			if (Choice->HwgList.size () != 0) // nur dann neu laden, wenn schon geladen wurde
			{
				Choice->FillList ();
			}
		}
		DATABASE->InitHwg ();
		Form.Show ();
	}
	else
	{
		AfxMessageBox (_T("Satz kan nicht gel�scht werden"), MB_OK | MB_ICONERROR);
	}

	focus->SetFocus ();
}

void CHwgPage::UpdateWrite ()
{
	if (IsWindowVisible ())
	{
		Write ();
	}
}

void CHwgPage::UpdateDelete ()
{
	if (IsWindowVisible ())
	{
		Delete ();
	}
}

void CHwgPage::Update ()
{
	if (IsWindowVisible ())
	{
		Form.Show ();
	}
}


