#include "StdAfx.h"
#include "HwgList.h"

CHwgList::CHwgList(void)
{
	hwg = 0;
	hwg_bz1 = _T("");
	hwg_bz2 = _T("");
}

CHwgList::CHwgList(short hwg, LPTSTR hwg_bz1, LPTSTR hwg_bz2)
{
	this->hwg  = hwg;
	this->hwg_bz1 = hwg_bz1;
	this->hwg_bz2 = hwg_bz2;
}

CHwgList::~CHwgList(void)
{
}
