#pragma once
#include "Groups.h"
#include "SplitFrame.h"
#include "StartViewInt.h"


// CStartView-Formularansicht

class CStartView : public CFormView, 
				  CStartViewInt
{
	DECLARE_DYNCREATE(CStartView)

protected:
	CStartView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CStartView();

public:
	enum { IDD = IDD_GROUP_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
	virtual void OnSize (UINT, int, int);
    afx_msg void OnBack();
    afx_msg void OnWrite();

	DECLARE_MESSAGE_MAP()


private:
	CSplitFrame *m_SplitFrame;
protected:
	void SetFrame ();
public:

// Schnittstelle CSartViewInt

	virtual void SetNewSplitView (BOOL docked);


};


