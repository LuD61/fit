// GroupsView.h : Schnittstelle der Klasse CGroupsView
//


#pragma once
#include "afxwin.h"
#include "CtrlGrid.h"
#include "RunMessage.h"
#include "MessageHandler.h"
//#include "HwgPage.h"
#include "VLabel.h"
#include "Core.h"
#include "HwgPage.h"
#include "WgPage.h"
#include "AgSheet.h"
#include "AgPage1.h"
#include "AgPage2.h"


class CGroupsView : public CFormView
{
protected: // Nur aus Serialisierung erstellen
	CGroupsView();
	DECLARE_DYNCREATE(CGroupsView)

public:
	enum{ IDD = IDD_BASE_VIEW };

// Attribute
public:
//	CGroupsDoc* GetDocument() const;

// Vorgänge
public:

// Überschreibungen
public:
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	DECLARE_MESSAGE_MAP()
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

// Implementierung
public:
	virtual ~CGroupsView();
	class CHwgMessage : public CRunMessage
	{
	private: 
		CGroupsView *p;
	public: 
			CHwgMessage (CGroupsView *p);
			virtual void Run ();
	};
	class CWgMessage : public CRunMessage
	{
	private: 
		CGroupsView *p;
	public: 
			CWgMessage (CGroupsView *p);
			virtual void Run ();
	};
	class CAgMessage : public CRunMessage
	{
	private: 
		CGroupsView *p;
	public: 
			CAgMessage (CGroupsView *p);
			virtual void Run ();
	};
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generierte Funktionen für die Meldungstabellen
protected:
	CMessageHandler *MessageHandler;
	CHwgMessage *HwgMessage;
	CWgMessage *WgMessage;
	CAgMessage *AgMessage;
	CFont Font;
	CFont TitleFont;
	int FontHeight;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	HBRUSH TitleBrush;
	CCtrlGrid TitleGrid;
	CCtrlGrid HwgGrid;
	CCtrlGrid WgGrid;
	CCtrlGrid AgGrid;
	CCtrlGrid CtrlGrid;
	CVLabel m_Title;
	CHwgPage m_HwgPage;
	CWgPage m_WgPage;
	CAgPage1 m_AgPage1;
	CAgPage2 m_AgPage2;
	CAgSheet m_AgProperty;
	BOOL agSet;
	CSize StartSize;
	CWnd *m_FocusHwg;
	CWnd *m_FocusWg;
	CWnd *m_FocusAg;

public:
	void OnHwg ();
	void OnWg();
	void OnAg();
};

/*
#ifndef _DEBUG  // Debugversion in GroupsView.cpp
inline CGroupsDoc* CGroupsView::GetDocument() const
   { return reinterpret_cast<CGroupsDoc*>(m_pDocument); }
#endif
*/

