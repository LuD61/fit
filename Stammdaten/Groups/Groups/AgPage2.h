#pragma once
#include "StaticButton.h"
#include "ColorButton.h"
#include "Label.h"
#include "VLabel.h"
#include "RunMessage.h"
#include "MessageHandler.h"
#include "CtrlGridColor.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "DecimalEdit.h"
#include "TextEdit.h"
#include "ChoiceHwg.h"
#include "ChoiceWg.h"
#include "ChoiceAg.h"
#include "UpdateHandler.h"
#include "afxwin.h"
#include "afxdtctl.h"
#include "afxcmn.h"


// CAgPage2-Dialogfeld

class CAgPage2 : public CPropertyPage,
                        CUpdateHandler     
{
	DECLARE_DYNAMIC(CAgPage2)

public:
	CAgPage2();   // Standardkonstruktor
	virtual ~CAgPage2();

// Dialogfelddaten
	enum { IDD = IDD_AG_PAGE2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);

	DECLARE_MESSAGE_MAP()
private:
	BOOL m_TestFocus;
	CMessageHandler *MessageHandler;
	CFont Font;
	CFont TitleFont;
	int FontHeight;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	BOOL MustCreate;
	BOOL IsCreated;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid HeadGrid;
	CCtrlGrid AgGrid;
	CCtrlGrid HwgGrid;
	CCtrlGrid WgGrid;
	CCtrlGrid DataGrid;

	CFormTab Form;
	BOOL HideOK;
	BOOL m_CanWrite;
	BOOL isNewRec;
	CWnd *m_CurrentFocus;

public:
	void set_CurrentFocus (CWnd *currentFocus)
	{
		m_CurrentFocus = currentFocus;
	}

	CWnd *CurrentFocus ()
	{
		return m_CurrentFocus;
	}

	CStatic m_AgGroup;

    BOOL OnReturn ();
    BOOL OnKeyup ();

public:
// UpdateHandler
	virtual BOOL StepBack ();
	virtual void Update ();
	virtual void Get ();
	virtual void UpdateWrite ();
	virtual void UpdateDelete ();
	CStatic m_LAg;
	CNumEdit m_Ag;
	CStatic m_LAgBz;
	CTextEdit m_AgBz1;
	CTextEdit m_AgBz2;
	CStatic m_LWg;
	CNumEdit m_Wg;
	CStatic m_LWgBz;
	CTextEdit m_WgBz1;
	CStatic m_LHwg;
	CNumEdit m_Hwg;
	CStatic m_LHwgBz;
	CTextEdit m_HwgBz1;
	CStatic m_LAbt;
	CNumEdit m_Abt;

	CButton m_BsdKz;
	CStatic m_LBestAuto;
	CComboBox m_BestAuto;
	CButton m_Pfa;
	CStatic m_LStkLstKz;
	CComboBox m_StkLstKz;
	CStatic m_LHndGew;
	CComboBox m_HndGew;
	CStatic m_LSg1;
	CNumEdit m_Sg1;
	CSpinButtonCtrl m_SpinSg1;
	CStatic m_LSg2;
	CNumEdit m_Sg2;
	CSpinButtonCtrl m_SpinSg2;
	CStatic m_LWarenEti;
	CComboBox m_WarenEti;
	CStatic m_LRegEti;
	CComboBox m_RegEti;
	CStatic m_LThekeEti;
	CComboBox m_ThekeEti;
	CStatic m_LSais1;
	CStatic m_LSais2;
	CNumEdit m_Sais1;
	CNumEdit m_Sais2;
	CStatic m_LMhdKz;
	CComboBox m_MhdKz;
	CStatic m_LMhdZtr;
	CNumEdit m_MhdZtr;
	CSpinButtonCtrl m_SpinMhdZtr;
	CButton m_PrUeb;
	CStatic m_LTara;
	CNumEdit m_Tara;
};
