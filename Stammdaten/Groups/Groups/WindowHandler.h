#ifndef __WINDOWHANDLER_DEF
#define __WINDOWHANDLER_DEF

#pragma once
#include <vector>
#include "UpdateHandler.h"
#include "StartViewInt.h"
#include "GroupSplitter.h"
#include "ChoiceHwg.h"
#include "ChoiceWg.h"
#include "ChoiceAg.h"

using namespace std;

class CWindowHandler
{
public:
	enum SPLIT_DIR
	{
		Horizontal = 0,
		Vertical = 1,
		NoSplit = 2,
	};

	enum VIEWTYPE
	{
		MainView = 0,
		Choice  = 1,
	};

	enum ORIENTATION
	{
		Left = 0,
		Right = 1,
	};

	enum PAGE
	{
		HwgPage = 0,
		WgPage  = 1,
		AgPage  = 2,
	};

private:
     static CWindowHandler *Instance;
	 vector<CUpdateHandler *> UpdateTable;
	 CSplitterWnd *m_Splitter;
	 CGroupSplitter *m_GroupSplitter;
	 CWnd *m_Base;
	 BOOL m_StartWithDock;
	 BOOL m_ChoiceDockable;
	 BOOL m_ChoiceIsCreated;
	 CFrameWnd *m_MainFrame;
	 CMenu *m_Menu;
	 CPropertySheet *m_Sheet;
	 BOOL m_CloseFromMemory;
	 COLORREF m_DlgBkColor;
	 HBRUSH m_DlgBrush;
	 SPLIT_DIR m_SplitDir;
	 SPLIT_DIR m_SplitSave;
	 VIEWTYPE m_ViewType;
	 BOOL m_MenVisible;
	 ORIENTATION m_Orientation;
	 BOOL m_DockChoice;
	 CStartViewInt *m_StartView;
	 PAGE m_ActivePage;
	 CChoiceHwg *m_ChoiceHwg;
	 CChoiceWg *m_ChoiceWg;
	 CChoiceAG *m_ChoiceAg;
	 BOOL m_CanWrite;



protected:
	CWindowHandler(void);
	~CWindowHandler(void);
public:

	CChoiceHwg *ChoiceHwg ()
	{
		return m_ChoiceHwg;
	}

	CChoiceWg *ChoiceWg ()
	{
		return m_ChoiceWg;
	}

	CChoiceAG *ChoiceAg ()
	{
		return m_ChoiceAg;
	}

	void set_ActivePage (PAGE activePage)
	{
		m_ActivePage = activePage;
	}

	PAGE ActivePage ()
	{
		return m_ActivePage;
	}


	 void set_StartView (CStartViewInt *startView)
	 {
		 m_StartView = startView;
	 }

	 CStartViewInt *StartView ()
	 {
		 return m_StartView;
	 }

     void set_DockChoice (BOOL dockChoice)
	 {
		 m_DockChoice = dockChoice;
	 }
	 
	 BOOL DockChoice ()
	 {
		 return m_DockChoice;
	 }


     static CWindowHandler *GetInstance ();
     static void DestroyInstance ();

	 BOOL MenVisible ()
	 {
		 return m_MenVisible;
	 }

	 VIEWTYPE ViewType ()
	 {
		 return m_ViewType;
	 }

	 void SetSheet (CPropertySheet *sheet)
	 {
		 m_Sheet = sheet;
	 }
     
	 CPropertySheet *GetSheet ()
	 {
		 return m_Sheet;
	 }

	 SPLIT_DIR SplitDir ()
	 {
		 return m_SplitDir;
	 }

	 SPLIT_DIR SplitSave ()
	 {
		 return m_SplitSave;
	 }

	 void SetDlgBkColor (COLORREF DlgBkColor)
	 {
		 m_DlgBkColor = DlgBkColor;
	 }

	 COLORREF GetDlgBkColor ()
	 {
		 return m_DlgBkColor;
	 }

	 void SetDlgBrush (HBRUSH DlgBrush)
	 {
		 m_DlgBrush = DlgBrush;
	 }

	 HBRUSH GetDlgBrush ()
	 {
		 return m_DlgBrush;
	 }

	 BOOL IsCloseFromMemory ()
	 {
		 return m_CloseFromMemory;
	 }

	 void set_MainFrame (CFrameWnd* MainFrame)
	 {
		 m_MainFrame = MainFrame;
	 }

	 void set_Menu (CMenu* menu)
	 {
		 m_Menu = menu;
	 }

	 void SetSplitter (CSplitterWnd *splitter)
	 {
		 m_Splitter = splitter;
	 }

	 void set_GroupSplitter (CGroupSplitter *splitter)
	 {
		 m_GroupSplitter = splitter;
	 }

	 CGroupSplitter *GroupSplitter ()
	 {
		 return m_GroupSplitter;
	 }

	 void SetDockChoice (BOOL DockChoice)
	 {
		 m_ChoiceDockable = DockChoice;
	 }

	 void SetBase (CWnd* base)
	 {
		 m_Base = base;
		 UpdateChoice ();
	 }

	 CWnd * GetBase ()
	 {
		 return m_Base;
	 }

	 void set_CanWrite (BOOL canWrite)
	 {
		 m_CanWrite = canWrite;
	 }

	 BOOL CanWrite ()
	 {
		 return m_CanWrite;
	 }

	 void StartWithDock (BOOL StartWithDock)
	 {
		 CChoiceX *choice = m_ChoiceHwg;
		 m_StartWithDock = StartWithDock;
		 if (m_StartWithDock && choice != NULL)
		 {
			 if (!m_DockChoice && !IsWindowVisible (choice->m_hWnd))
			 {
				 delete choice;
				 choice = NULL;
				 m_ChoiceHwg = NULL;
			 }
		 }
	 }

	 void RegisterUpdate (CUpdateHandler *UpdateHandler);
	 void UnregisterUpdate (CUpdateHandler *UpdateHandler);
	 void Update ();
	 void Get();
	 void UpdateWrite ();
	 void OnBack ();
	 void OnWrite ();
	 void OnDelete ();
	 BOOL IsDockPlace (int x, int y);
	 BOOL IsVerticaleDockPlace (int x, int y);
	 void SplitChoice ();
	 void DeleteChoice ();
	 void SplitBottomChoice ();
	 void DeleteBottomChoice ();
	 void DeleteActiveChoice ();
	 void NewSplit ();
	 void OnChoice (CChoiceX *Choice, CWnd *Parent);
	 void OnDeleteChoice ();
	 CChoiceX *GetChoice ();
	 CChoiceX *ActiveChoice ();
	 void UpdateChoice ();

	 void DestroyFrame ();
};

#define WINDOWHANDLER CWindowHandler::GetInstance ()
#endif