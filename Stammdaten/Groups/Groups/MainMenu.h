#pragma once
#include "Groups.h"
#include "CtrlGrid.h"
#include "ColorButton.h"
#include "MessageHandler.h"
#include "Label.h"
#include "VLabel.h"
#include "DataCollection.h"
#include "ControlContainer.h"


// CMainMenu-Formularansicht

class CMainMenu : public CFormView, 
						 CControlContainer
{
	DECLARE_DYNCREATE(CMainMenu)

protected:
	CMainMenu();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CMainMenu();

public:
	enum { IDD = IDD_MAIN_MENU };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
private:
	class CActivate : public CRunMessage
	{
	private: 
		CMainMenu *p;
	public: 
			CActivate (CMainMenu *p);
			virtual void Run ();
	};
	CFont Font;
	CFont TitleFont;
	int FontHeight;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	HBRUSH TitleBrush;
	CCtrlGrid TitleGrid;
	CCtrlGrid DataGrid;
	CCtrlGrid CtrlGrid;
	CColorButton m_BHwg;
	CColorButton m_BWg;
	CColorButton m_BAg;
	CVLabel m_Title;
	CMessageHandler *MessageHandler;
	CActivate *Activate;
	CDataCollection<CColorButton *> Container;
public:
	void OnHwg ();
	void OnWg ();
	void OnAg ();
	virtual void ElementSetFocus (CWnd *cWnd);
};


