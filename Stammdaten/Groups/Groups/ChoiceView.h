#pragma once
#include "Groups.h"
#include "choicex.h"
#include "ctrlgrid.h"
#include "VLabel.h"

#ifndef IDC_TITLE
#define IDC_TITLE 1001
#endif

// CChoiceView-Formularansicht

class CChoiceView : public CFormView
{
	DECLARE_DYNCREATE(CChoiceView)

protected:
	CChoiceView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CChoiceView();

public:
	enum { IDD = IDD_CHOICE_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
	virtual void OnSize (UINT, int, int);
	DECLARE_MESSAGE_MAP()
private:
	CChoiceX *m_Choice;
	CCtrlGrid CtrlGrid;
//	CVLabel m_Title;
	CColorButton m_Title;
	CSize m_TitleReg;
public:
	afx_msg void OnTitle ();
    afx_msg void OnBack();
    afx_msg void OnDestroy ();
	afx_msg void OnFileSave ();
};


