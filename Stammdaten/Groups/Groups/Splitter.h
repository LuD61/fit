#pragma once
#include "afxext.h"
#include "MainSplitter.h"
#include "GroupSplitter.h"

class CSplitter :
	public CSplitterWnd
{
private:
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
public:
	CSplitter(void);
	DECLARE_DYNCREATE(CSplitter)
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnDestroy ();
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
public:
	~CSplitter(void);
	void Destroy ();
	virtual BOOL CreateView( int row,int col,CRuntimeClass* pViewClass,SIZE sizeInit,CCreateContext* pContext);
private:
	CMainSplitter  *m_MainSplitter;
	CGroupSplitter *m_GroupSplitter;
};
