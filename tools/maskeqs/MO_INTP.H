/* ************************************************************************ */
/*       Definitionen und extern Variablen fuer Programme die das 
         Modul mo_intp benutzen.

         Autor : Wilhelm Roth
*/
/* ************************************************************************ */

typedef struct
       {   char  feldname [20];
           short feldlen;
           char  feldtyp [2];
           short nachkomma;
           short dimension;
       } FELDER;

typedef struct
       {   FELDER **felder;
           char   **buffer;
       } RECORD;

union FADR
      { unsigned char  *fchar;
        short *fshort;
        int   *fint;
        long  *flong;
        void  *fvoid;
        double *fdouble;
      };

typedef struct
{
 char *bedingung;
 char *anweisung;
 short bflag;
} iff;


extern int arganz;
extern char *argsp [];         /* Speicher fuer 20 Argumente                    */

extern char *wort [];             /* Bereich fuer split              */
extern char *eingabe;             /* Name der Ascii-Datei            */
extern char *info_name;           /* Name der Info-Datei             */
extern short ziel_len;      /* Laenge Zielsatz. Wird zur Laufzeit gesetzt    */
extern unsigned char *eingabesatz;   /* Buffer fuer Ascii-Eingabesatz                 */
extern short qlen;          /* Laenge Eingabesatz                            */
extern unsigned char *ausgabesatz;   /* Buffer fuer Binaer-Ausgabesatz                */
extern short zlen;          /* Laenge ausgabesatz                            */
extern char *daten;         /* Buffer fuer allgemeien Variablen              */
extern short dlen;          /* Anzahl allgemeien Daten                       */
extern char CRT[];

extern short constat;
extern short ecode;         /* Exit-Status                                   */
extern short do_exit;       /* Kennzeichen fuer exit                         */
extern FELDER *quelle;      /* Tabelle mit Satzaufbau fuer Eingabefelder     */
extern short feld_anz;      /* Anzahl Felder im Satz                         */
extern RECORD insatz;

extern FELDER *ziel;        /* Tabelle mit Tabellenaufbau aus der Datenbank  */
extern short db_anz;        /* Anzahl Felder im Satz                         */
static lesestatus;          /* SQL-Status beim Lesen                        */
extern RECORD outsatz;

extern FELDER *data;         /* Tabelle mit Variablen                       */
extern short data_anz;       /* Anzahl Variablen                           */
extern RECORD datasatz;

extern short binaer;         /* Dateimodus                                   */

/* Informationen aus SW-Kopf                                      */
extern short blklen;            /* SW-Blocklaenge                        */
extern short blkanz;            /* Anzahl SW-Bloecke                     */
extern short banz;              /* Anzahl SW-Felder pro Satz             */
extern short feld_anz;          /* Anzahl SW-Felder pro Satz             */
extern short klen;              /* Anzahl SW-Felder pro Satz             */

int lese_sw (char *, int);
int sw_info (FELDER *);
int sw_infoa (FELDER *);
int satzlen (FELDER *);
int readsw (char *, FILE *);
int uebertrage (RECORD *, char *, RECORD *, char *);
char *feldadr (char *);
int init_sw_start (void);
int belege_ziel (char *);
belege_db (char *);
int select_cursor (void);
int update_cursor (void);
int fetchsql (int);
int to_db (void);
char *GetItemName (char *);
void setand_part (char *);
int perform_begin (void);
int perform_code (void);
int perform_ende (void);
void FreeTable (void);
void CloseInf (void);
void opendb (char *);
int set_wrziel (int (*) (FILE *), FILE *);
int mov (char *, char *, ...); 
char *GetIfItem (char *, char *);
char *GetInfInfo (char *, char *);
int set_dprog (int (*prog) (iff *, short));
void set_debug (int);
int getwert (char *, char *,  short);
