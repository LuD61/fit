#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "mo_menu.h"
#include "wmask.h"

extern HWND hMainWindow;
extern char fktscolor2[];
extern char headscolor[];
extern char mamain1scolor[];

COLORREF fktcolor2 = GRAYCOL;
COLORREF headcolor = GRAYCOL;
COLORREF mamain1color = LTGRAYCOL;
int ColOk = 0;

static MENUE_CLASS menue_class;

static int hMx;
static int hMy;
static int hMcx;
static int hMcy;

mfont fkfont    = {NULL , -1, -1, 1,
                                  GRAYCOL,
                                  RGB (0, 0, 120),
                                  0,
                                  NULL};

static field _fkformt[] = {
"...........",11, 0, 0, 3, 0, "", READONLY,0, 0, 0,
"...........",11, 0, 0,16, 0, "", READONLY,0, 0, 0,
"...........",11, 0, 0,29, 0, "", READONLY,0, 0, 0,
"...........",11, 0, 0,42, 0, "", READONLY,0, 0, 0,
"...........",11, 0, 0,56, 0, "", READONLY,0, 0, 0,
"...........",11, 0, 0,70, 0, "", READONLY,0, 0, 0
};

static form fkformt = {6, 0, 0, _fkformt, 0, 0, 0, NULL, &fkfont};

static field _fkform[] = {
"6",          2, 0, 0, 1, 0, "", BUTTON | DISABLED,  0, 0, 0, 
"7",          2, 0, 0,14, 0, "", BUTTON | DISABLED,  0, 0, 0, 
"8",          2, 0, 0,27, 0, "", BUTTON | DISABLED,  0, 0, 0, 
"9",          2, 0, 0,40, 0, "", BUTTON | DISABLED,  0, 0, 0, 
"10",         3, 0, 0,53, 0, "", BUTTON | DISABLED,  0, 0, 0, 
"11",         3, 0, 0,67, 0, "", BUTTON | DISABLED,  0, 0, 0,
};

static form fkform = {6, 0, 0, _fkform, 0, 0, 0, NULL, NULL};


mfont fkfont2    = {"Courier New", 90, 100, 0,
                                       BLACKCOL,
                                       fktcolor2,
                                       0};


static field _fkform2[] = {
"  Hilfe   ",        10, 0, 0,  1, 0, "",        BUTTON, 0, 0, KEY1,
"  Zur�ck  ",        10, 0, 0, 13, 0, "",        BUTTON, 0, 0, KEYPGU,
"  Vor  ",            7, 0, 0, 25, 0, "",        BUTTON, 0, 0, KEYPGD,
"  Abbruch  ",     13, 0,   0, 34, 0,   "",      BUTTON, 0, 0, KEY5,
"  Ausf�hren  ",   15, 0,   0, 49, 0,   "",      BUTTON, 0, 0, KEY12
};

static form fkform2 = {5, 0, 0, _fkform2, 0, 0, 0, NULL, &fkfont2};

HWND ftasten = NULL;
HWND ftasten2 = NULL;
HWND head = NULL;

static char kopffeld1[19] = {"SE.TEC            "};
static char kopffeld2[26] = {"Listengenerator          "};
static char aktfeld[12] = {"           "};
static char svon[3] = {"1"};
static char sbis[3] = {"1"};


mfont kopffont2    = {NULL , -1, -1, 1,
                                  BLACKCOL,
                                  LTGRAYCOL,
                                  0,
                                  NULL};

static field _kopf1[] = {
kopffeld1,          0, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form kopf1 = {1, 0, 0, _kopf1, 0, 0, 0, 0, NULL};

static field _kopf2[] = {
kopffeld2,          0, 0, 0,20, 0, "", DISPLAYONLY, 0, 0, 0
};

static form kopf2 = {1, 0, 0, _kopf2, 0, 0, 0, 0, &kopffont2};
// static form kopf2 = {1, 0, 0, _kopf2, 0, 0, 0, 0, 0};


static field _kopf3[] = {
aktfeld,            0, 0, 0, 50, 0, "",    DISPLAYONLY, 0, 0, 0,
"Seite",            0, 0, 0, 63, 0, "",    DISPLAYONLY, 0, 0, 0,
svon,               3, 0, 0, 69, 0, "%2d", READONLY, 0, 0, 0,
"<",                2, 0, 0, 72, 0, "",    BUTTON | DISABLED, 0, 0, KEYPGU,
">",                2, 0, 0, 74, 0, "",    BUTTON| DISABLED, 0, 0, KEYPGD,
sbis,               3, 0, 0, 76, 0, "%2d", READONLY, 0, 0, 0
};

static form kopf3 = {6, 0, 0, _kopf3, 0, 0, 0, 0, NULL};


int fillcolref (char scolor[], char *colref)
/**
**/
{
       char cols [4];
       char *fpos;
       int color;

       color = 0;
       fpos = strstr (scolor, colref);

       if (fpos)
       {
                  fpos += strlen (colref);
                  memcpy (cols, fpos, 3);
                  cols[3] = (char) 0;
                  color = atoi (cols);
       }
       return color;
}

void fillcolor (char scolor[], COLORREF *color)
/**
**/
{
       int red, green, blue;

       red   = fillcolref (scolor, "red");
       green = fillcolref (scolor, "green");
       blue  = fillcolref (scolor, "blue");
       *color = RGB (red, green, blue);
}

void FillColors (void)
/**
**/
{
       if (ColOk) return;
       ColOk = 1;

       if (strcmp (fktscolor2, " "))
       {
                     fillcolor (fktscolor2, &fktcolor2);
       }

       if (strcmp (headscolor, " "))
       {
                     fillcolor (headscolor, &headcolor);
       }
       if (strcmp (mamain1scolor, " "))
       {
                     fillcolor (mamain1scolor, &mamain1color);
       }
}

HWND OpenFkt (HANDLE hInstance)
/**
Funktionstasten-Fenster oeffnen.
**/
{
       SetEnvFont ();
       FillColors ();
       SetBorder (NULL);
       SetAktivWindow (NULL);
       ftasten = OpenColWindow (1, 80, 21, 0, WS_CHILD,
                                  LTGRAYCOL, NULL);
       display_form (ftasten, &fkform, 0, 0);
       display_form (ftasten, &fkformt, 0, 0);
       SetAktivWindow (NULL);
       ftasten2 = OpenColWindow (1, 80, 23, 0, WS_CHILD,
                                 fktcolor2, NULL);
       display_form (ftasten2, &fkform2, 0, -1);
       if (headcolor != GRAYCOL)
       {
                     SetAktivWindow (NULL);
                     SetDiffPixel ( -2, 3, 7, 0);
                     head = OpenColWindowEx (1, 78, 4, 1, WS_POPUP,
                                           headcolor, NULL);
                     SetDiffPixel ( 0, 0, 0, 0);
                     GetDiffPixel (hMainWindow, head, &hMx, &hMy,
                                                      &hMcx, &hMcy);
       }
       return ftasten;
}

void MoveHeader (void)
/**
Headerwindow anpassen.
**/
{
       RECT rect;

       if (head == NULL) return;

       GetWindowRect (hMainWindow, &rect);
       MoveWindow (head, rect.left + hMx, rect.top + hMy, hMcx, hMcy,
                   TRUE);
}


HWND OpenMamain1Ex (int rows, int columns,
                    int row, int column,
                    HINSTANCE hInstance, WNDPROC WndProc,
                    form *frm, mfont *font)
/**
Haupteingabefenster oeffnen.
**/
{
       SetEnvFont ();
       FillColors ();
       HWND mamain1;

       if (mamain1color != LTGRAYCOL)
       {
              mamain1 = OpenColWindowEx (rows, columns, row , column,
                                           WS_CHILD,
                                           mamain1color, WndProc);
              if (frm != NULL && font != NULL)
              {
                        frm->font = font;
                        font->FontBkColor = mamain1color;
              }
       }
       else
       {
              mamain1  = OpenWindowEx (rows, columns, row, column,
                                       WS_CHILD, hInstance);

       }
       return mamain1;
}
       
HWND OpenMamain1 (int rows, int columns,
                    int row, int column,
                    HINSTANCE hInstance, WNDPROC WndProc,
                    form *frm, mfont *font)
/**
Haupteingabefendtsr oeffnen.
**/
{
       SetEnvFont ();
       FillColors ();
       HWND mamain1;

       if (mamain1color != LTGRAYCOL)
       {
              mamain1 = OpenColWindow (rows, columns, row , column,
                                           WS_CHILD,
                                           mamain1color, WndProc);
              if (frm != NULL && font != NULL)
              {
                        frm->font = font;
                        font->FontBkColor = mamain1color;
              }
       }
       else
       {
              mamain1  = OpenWindow (rows, columns, row, column,
                                     1, hInstance);
       }
       return mamain1;
}

void UpdateFkt (void)
/**
Update fuer Window ftasten.
**/
{
        form *savecurrent;

        savecurrent = current_form;
        current_form = &fkform;
        UpdateWindow (ftasten);
        current_form = &fkform2;
        UpdateWindow (ftasten2);
        current_form = savecurrent;
}

void SetFkt (int fktnr, char *text, int taste)
/**
Text fuer Funktionstaste setzen.
**/
{
         form *savecurrent;

         savecurrent = current_form;
         current_form = &fkform;
         if (fktnr > 11 || fktnr < 6) return;

         fktnr -= 6;
         fkformt.mask[fktnr].feld = text;
         fkform.mask[fktnr].BuId = taste;
         if (taste)
         {
                   fkform.mask[fktnr].attribut = BUTTON;
         }
         else
         {
                   fkform.mask[fktnr].attribut |= DISABLED;
         }
         CloseControl (&fkform, fktnr);
         display_field (ftasten, &fkformt.mask[fktnr], 0, 0);
         display_field (ftasten, &fkform.mask[fktnr], 0, 0);
         current_form = savecurrent;
}

int SendKey (int key)
/**
Tastaturmelung senden.
**/
{
        if (SendWindow && current_form)
        {
                  PostMessage (current_form->mask[currentfield].feldid,
                               WM_KEYDOWN,  (WPARAM) key, 0l);
        }
        else
        {
                 PostMessage (AktivWindow, WM_KEYDOWN,  (WPARAM) key, 0l);
        }
        return 0;
}

int abfragejn (HWND hDlg, char *text, char *def)
/**
Window mit Abfrage auf Ja oder Nein.
**/
{
        int ret;

        if (def[0] == 'J' || def[0] == 'j')
        {
                 ret = MessageBox (NULL, text, "", MB_YESNO |
                                                   MB_TOPMOST | 
                                                   MB_ICONWARNING);
        }
        else
        {
                 ret = MessageBox (NULL, text, "",
                                         MB_YESNO | MB_DEFBUTTON2 |
                                         MB_ICONWARNING);
        }
        if (ret == IDYES) return (1);
        return (0);
}

void DisplayKopf1 (HWND Window, char *text)
{
        int dsqlstatus;
        SetEnvFont ();
        
        dsqlstatus = menue_class.Lesesys_inst ();
        if (dsqlstatus == 0)
        {
                     strcpy (kopffeld1, sys_inst.projekt);
        } 
        strcpy (kopffeld2, text);
        display_form (Window, &kopf1, 0, 0);
        display_form (Window, &kopf2, 0, 0);
}

void DisplayKopf3 (HWND Window, char *text, int sv, int sb)
{
        static int svb = 0;

        strcpy (aktfeld, text);
        sprintf (svon, "%d", sv);
        sprintf (sbis, "%d", sb);

        if (sb > sv)
        {
                   kopf3.mask[3].attribut = BUTTON;
                   kopf3.mask[4].attribut = BUTTON;
                   if (svb == 0)
                   {
                             CloseControl (&fkform, 3);
                             CloseControl (&fkform, 4);
                   }
                   svb = 1;
        }
        else
        {
                   kopf3.mask[3].attribut |= DISABLED;
                   kopf3.mask[4].attribut |= DISABLED;
                   if (svb == 1)
                   {
                             CloseControl (&fkform, 3);
                             CloseControl (&fkform, 4);
                   }
                   svb = 0;
        }
        display_form (Window, &kopf3, 0, 0);
}
