#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include <commctrl.h>
#include "wmask.h"
#include "strfkt.h"
#ifdef BIWAK
#include "conf_env.h"
#endif

#define FONTMAX 1000
#define HWFONTMAX 1500
#define MAXWINCOLS 0x1000

LONG FAR PASCAL ListBoxProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL ShowListProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EnterListBoxProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EnterFormProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL ListProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EnterDialogBoxProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EnterButtonBoxProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL IconProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL ColButtonProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL StaticProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL RdOnlyProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL RdColProc(HWND,UINT,WPARAM,LPARAM);
void RegisterColWin (char *, COLORREF, WNDPROC);
WNDPROC WndProc = NULL;
static int (*IsNoDlgMess) (MSG *) = NULL;

static void TestIconsPressed (HWND);
static void EmSetSel (void);

static COLORREF RdonlyCol   = BLACKCOL;
static COLORREF RdonlyBkCol = WHITECOL;

void SetRdonlyCol (COLORREF Col)
{
    RdonlyCol = Col;
}

void SetRdonlyBkCol (COLORREF Col)
{
    RdonlyBkCol = Col;
}


void writelist (void);

/* Werte fuer Font                            */

struct HWFONT
{
          HWND  hWnd;
          HFONT hFont;
};

static struct LISTBU *BuTab = NULL;

static BOOL SpezBu = FALSE; 

int _WMDEBUG = 0;

static HFONT StdFont;

static struct HWFONT hwfont [HWFONTMAX];
static int hwanz = 0;

static HFONT fonttab [FONTMAX];
static fontanz = 0;

static COLORREF wincolors [MAXWINCOLS];
static WNDPROC colorprocs [MAXWINCOLS];
static int wincolanz;

static int pxp, pyp, pcxp, pcyp;

static char szFaceName[80] = {"Courier New"};
static int iDeciPtHeight =  90;
static int iDeciPtWidth  =  100;
static int iAttributes   =   0;
static int FontOK = 0;

static char FontName[80] = {"Courier New"};
static int FontHeight =  90;
static int FontWidth  =  100;
static int FontAttribute   =   0;
char fktscolor2 [20]= {" "};
char headscolor [20] = {" "};
char mamain1scolor [20] = {" "};

char *hStdWindow = "hStdWindow";

HFONT stdHfont = NULL;

/* Ende Font-Werte                            */

static int      ProcessMessages(void);
void DlgFirstD (HWND);
void BtoFirst (HWND);

extern HICON hIcon;
char *ModulName = NULL;
HWND    AktivWindow = NULL;
HWND    SendWindow = NULL;
static HANDLE  hMainInst;
static BOOL choisebox = FALSE;
static BOOL DispChoiseBox = FALSE;
static int choiseab = 4;
static HWND    lbox;
static HWND    lboxBar = NULL;
static HWND    hListBox;
static HWND    btcancel;
static HWND    btok;
static char messbuffer [265];

static int searchmode = 0;
static int searchfield = 0;

static int list_break;
static int denter = 0;
static int listenter = 0;
static int selcolor = 1;
static char tbuffer [64];
static int tbpos = 0;
static int isChild = 0;
static int fillwindow = 1;
static int VScroll = 0;
static int title_mode = 0;
static int lblaktiv = 0;
static int ListEWindow = 0;
static int NoCloseList = 0;
static BOOL MenSelect = TRUE;
static struct LMENUE menue;
static struct LMENUE menuetab[20];
static HWND lboxstack [20];
static HWND hListBoxStack [20];
static HWND AktWinStack [20];
static int listzabStack [20];
static int menuestack = 0;
static BOOL FeldScroll = 0;
static jrhstart = 70;
static jrh1 = 1900;
static jrh2 = 2000;
static BOOL ButtonTab = 0;
static void (*LbTimeProc) (void) = NULL;
static int printdir = 0;

void SetLbTimeProc (void (*LbTP) (void), long sec)
{
	    if (lbox == NULL) return; 
	    LbTimeProc = LbTP;
		if (LbTimeProc == NULL)
		{
			KillTimer (lbox, 2);
		}
		else
		{
            SetTimer (lbox, 2, sec, 0);  
		}
}

void SetMenSelect (BOOL mode)
{
	   MenSelect = max (0, min (1, mode));
}

void SetButtonTab (BOOL mode)
{
        if (mode)
        {
               ButtonTab = 1;
        }
        else
        {
               ButtonTab = 0;
        }
}

struct PTABW
       {int   ptlfnr;
        char  ptwert [4];
        char  ptbez [33];
        char ptbetk [9];
        char ptwer1 [9];
        char ptwer2 [9];
       };

struct PTABW ptabw [100];

extern HWND hMainWindow;

char AktMenuTxt [512];

static char instab [1] [0x500];

static char ptmenue [100] [42];
static char *menuecaption = 0;
static char *menuevline = 0;
static char *menueitems;
static short  menueanz;
static short  menuedim;
static int    menueidx;
static int UbMultiRows = 0;
static int MenStart = 0;
static int ListFont = 0;
static struct LST *SaveList = NULL;

int (*fkt_aktiv[])() = {0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0};

void (*BeforeList) (void) = NULL;
void (*AfterList) (void) = NULL;
void (*FillEmptyRow) (void) = NULL;
void (*DoDoubleClck) (int) = NULL;
BOOL  WithDblClck = TRUE;

static int setsel = 1;
int syskey = 0;
static int all_enter_break = 0;
static int enter_break = 0;
static int end_break = 0;
form *current_form = 0;
int currentfield = 0;
int currentbutton = 0;
static BOOL MouseLocked = 0;
static int (*MouseLockProc) (POINT *) = NULL;
static BOOL MousePressed = 0;
static HWND MousehWnd = NULL;
static int  TimerOn = 0;
static HWND TimehWnd = NULL;
static int opencombobox = 0;
static int setfield = 0;
static HWND akthDlg;
HWND AktivDialog = NULL;
form *AktivCbox = NULL;
static DWORD WinBorder = WS_CHILD;
static DWORD WinBorderEx = WS_EX_CLIENTEDGE; 
static int AktZeile, AktSpalte, AktMenZeile;
HFONT aktFont = 0;

static char *listtab;
static char *liststruct;
static form *listform;
static int listanz;
static int listdim;
static int listzab = 1;
static int listins = 0;
static int start_listins = 0;
static int aktsel = 0;
static int nolistdel = 0;
static int nolistins = 0;
static HWND listwindow;

static TEXTMETRIC tm;
static int novisible = 0;
static int sz = 1;
static hlines = 1;
static vlines = 1;

static form *frmstack [FMMAX];
static int fmptr = 0;

static char crnl[] = {13,10,0};
static char buffer [0x1000];

static HWND AktColFocus = NULL;


static int CharBuffPos = 0;
static char CharBuff [20] = {"\0"};
static int MatchCase = FALSE;
static BOOL CharBuffSet = TRUE;

static mfont Chfont    = {NULL, -1, -1, 0,
                                       BLACKCOL,
                                       LTGRAYCOL,
                                       0};

static field _fCharBuff[] =  {
"Suchbegriff..:", 15, 0, 1, 1, 0, "", DISPLAYONLY, 0, 0, 0, 
CharBuff,         20, 0,17, 1, 0, "", EDIT, 0, 0, 0,
};

static form fCharBuff = {2, 0, 0, _fCharBuff, 0, 0, 0, 0, &Chfont};



void SetSearchMode (int mode)
{
	      searchmode = min (2,mode);
}


void SetSearchField (int fieldnr)
{
	      searchfield = fieldnr;
		  if (searchmode == 0)
		  {
		         searchmode = 1;
		  }
}

void SetCharBuff (char *buffer)
{
	      strcpy (CharBuff, buffer);
		  CharBuffPos = strlen (buffer);
		  CharBuffSet = TRUE;
}


void SetStdProc (WNDPROC StdProc)
/**
WndProc setzen.
**/
{
            WndProc = StdProc;
}

void SetListFont (BOOL mode)
/**
ListFont setzen.
**/
{
            ListFont = mode;
            if (ListFont != 0) ListFont = 1;
}

void SetMenStart (int me)
/**
**/
{
            MenStart = me;
}

void GetDiffPixel (HWND wMain, HWND wChild, int *x, int *y, int *cx, int *cy)
/**
Position von wChild in wMain ermitteln.
**/
{
           RECT mrect;
           RECT crect;

           GetWindowRect (wMain,  &mrect);
           GetWindowRect (wChild, &crect);
           *x = crect.left - mrect.left;
           *y = crect.top  - mrect.top;
           *cx = crect.right - crect.left;
           *cy = crect.bottom - crect.top;
}

void SetDiffPixel (int x, int y, int cx, int cy)
/**
**/
{
              pxp  = x;
              pyp  = y;
              pcxp = cx;
              pcyp = cy;
}

void stdfont (void)
/**
Standard-Font uebertragen.
**/
{
            strcpy (szFaceName, FontName);
            iDeciPtHeight = FontHeight;
            iDeciPtWidth  = FontWidth;
            iAttributes  = FontAttribute;
}

void spezfont (mfont *Font)
/**
Speziellen font fuer Form uebertragen.
**/
{

            if (Font->FontName)  strcpy (szFaceName, Font->FontName);
            if (Font->FontHeight != -1)  iDeciPtHeight = Font->FontHeight;
            if (Font->FontWidth != -1) iDeciPtWidth  = Font->FontWidth;
            if (Font->FontAttribute != -1) iAttributes  = Font->FontAttribute;
}


void SetSscroll (int spalte, int zeile)
/**
Horzontale Scrollposition setzen.
**/
{
         HDC hdc;
         TEXTMETRIC tm;
         HFONT hFont, oldfont;
         RECT rect;

         if (spalte == 0)
         {
                       menue.srect.left = 0;
                       menue.srect.top = 0;
                       menue.srect.right = 0;
                       menue.srect.bottom = 0;
                       return;
         }

         stdfont ();
         hdc = GetDC (lbox);
         hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
         oldfont = SelectObject (hdc,hFont);
         GetTextMetrics (hdc, &tm) ;
         DeleteObject (SelectObject (hdc, oldfont)) ;
         ReleaseDC (lbox, hdc);
         DeleteObject (hFont);

         GetClientRect (lbox, &rect);

         menue.srect.left = spalte * tm.tmAveCharWidth;

         menue.srect.top = zeile * tm.tmHeight;
         menue.srect.right = rect.right;
         menue.srect.bottom = rect.bottom;
}

void SetModuleName (char *name)
/**
Modul-Namen setzen.
**/
{
            ModulName = name;
}

void SetEnvFont (void)
/**
Font aus Datei $ETC/FONT setzen.
**/
{
        FILE *fp;
        char buffer [256];
        int anz;

        if (FontOK) return;

        FontOK = 1;
#ifdef BIWAK
        if (getenv ("ETC"))
        {
                 sprintf (buffer, "%s\\FONT", getenv ("ETC"));
        }
        else
        {
                 sprintf (buffer, "FONT");
        }
#else
        if (getenv ("BWSETC"))
        {
                 sprintf (buffer, "%s\\FONT", getenv ("BWSETC"));
        }
        else
        {
                 sprintf (buffer, "FONT");
        }
#endif

        fp = fopen (buffer, "r");
        if (fp == (FILE *) 0) return;

        while (fgets (buffer, 255, fp))
        {
                   cr_weg (buffer);
                   anz = zsplit (buffer, ',');
                   if (anz < 2) continue;
                   if (strupcmp (zwort[1], "FONTNAME", 8) == 0)
                   {
                                 strcpy (szFaceName, zwort [2]);
                                 strcpy (FontName, zwort [2]);
                   }
                   else if (strupcmp (zwort[1], "HEIGHT", 6) == 0)
                   {
                                  iDeciPtHeight = atoi (zwort [2]);
                                  FontHeight = atoi (zwort [2]);
                   }
                   else if (strupcmp (zwort[1], "WIDTH", 5) == 0)
                   {
                                  iDeciPtWidth = atoi (zwort [2]);
                                  FontWidth = atoi (zwort [2]);
                   }
                   else if (strupcmp (zwort[1], "ATTRIBUTES", 10) == 0)
                   {
                                  iAttributes = atoi (zwort [2]);
                                  FontAttribute = atoi (zwort [2]);
                   }
                   else if (strupcmp (zwort[1], "FKTCOLOR2", 9) == 0)
                   {
                                   strcpy (fktscolor2, zwort [2]);

                   }
                   else if (strupcmp (zwort[1], "HEADCOLOR", 9) == 0)
                   {
                                   strcpy (headscolor, zwort [2]); 
                   }
                   else if (strupcmp (zwort[1], "MAMAIN1COLOR", 12) == 0)
                   {
                                   strcpy (mamain1scolor, zwort [2]); 
                   }
        }
        fclose (fp);
        return;
}


void SetNewStdFont (mfont *Font)
/**
Neuen StandardFont setzen.
**/
{
        if (Font->FontName)  strcpy (FontName, Font->FontName);
        if (Font->FontHeight != -1)  FontHeight = Font->FontHeight;
        if (Font->FontWidth != -1)   FontWidth  = Font->FontWidth;
        if (Font->FontAttribute != -1) FontAttribute  = Font->FontAttribute;
        stdfont ();
        stdHfont = NULL;
}


void SetFont (HFONT hFont)
/**
Font merken.
**/
{
          if (fontanz == FONTMAX) return;
          fonttab[fontanz] = hFont;
          fontanz ++;
}

void DestroyFonts (void)
/**
Alle Fonts wieder freigeben.
**/
{
          int i;

          for (i = 0; i < fontanz; i ++)
          {
                       DeleteObject (fonttab [i]);
          }
}

void SetHwFont (HWND hWnd, HFONT hFont)
/**
Font fuer Window setzen.
**/
{
          int i;

          for (i = 0; i < hwanz; i ++)
          {
                      if (hwfont[i].hWnd == hWnd)

                      {
                                  break;
                      }
          }
          hwfont[i].hWnd  = hWnd;
          hwfont[i].hFont = hFont;
          if (i == hwanz && hwanz < HWFONTMAX - 1)
          {
                     hwanz ++;
          }
}

HFONT GetHwFont (HWND hWnd)
/**
Font fuer Window holen.
**/
{
          int i;

          for (i = 0; i < hwanz; i ++)
          {
                      if (hwfont[i].hWnd == hWnd)
                      {
                                  break;
                      }
          }
          if (i == hwanz) return (NULL);
          return (hwfont[i].hFont);
}

HWND GetEWindow (void)
/**
Fenster-Handle fuer aktuelle Listbox holen.
**/
{
          return hListBox;
}

int InitFrmFont (HWND hWnd)
/**
Wenn ein Font fuer ein Fenster existiert, initialisieren.
**/
{
       int i, fm;
       form *savefrm;

       savefrm = current_form;

       for (fm = 0; fm < fmptr; fm ++)
       {
           current_form = frmstack [fm];

           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (hWnd == current_form->mask[i].feldid)
                 {
                         if (current_form->font)
                         {
                                    current_form->font->hFont = NULL;
                                    break;
                         }
                 }
            }
        }
        /*
        DeleteObject (stdHfont);
        stdHfont = NULL;
        */
        current_form = savefrm;
        return FALSE;
}


void ScrollHwFonts (int i)
/**
Fonts scrollen.
**/
{
          for (; i < hwanz; i ++)
          {
                    hwfont[i].hWnd  = hwfont [i + 1].hWnd;
                    hwfont[i].hFont = hwfont [i + 1].hFont;
          }
          if (hwanz) hwanz --;
}

void DelHwFont (HWND hWnd)
/**
Font fuer Window loeschen.
**/
{
          int i;
          HFONT hFont;

          for (i = 0; i < hwanz; i ++)
          {
                      if (hwfont[i].hWnd == hWnd)
                      {
                                  break;
                      }
          }
          if (i == hwanz) return;

          if (hwfont[i].hFont == stdHfont)
          {
                      ScrollHwFonts (i);
                      return;
          }
		  hFont = hwfont[i].hFont; 
          InitFrmFont (hWnd);
          for (i = 0; i < hwanz;)
          {
                      if (hwfont[i].hFont == hFont)
                      {
                                  ScrollHwFonts (i);
                      }
                      else
                      {
                                  i ++;
                      }
          }
}

void SetMouseLock (BOOL mode)
/**
Mouse sperren.
**/
{
        MouseLocked = mode;
}

void SetMouseLockProc (int (*proc) (POINT *))
/**
Mouse sperren.
**/
{
        MouseLockProc = proc;
}

int GetListAnz (void)
/**
Anzahl der Aktuellen Listelemente zurueckgeben.
**/
{
        return (listanz);
}

int GetListPos (void)
/**
Position des aktuellen Menues.
**/
{
        if (menue.menAnz == 0) return 0;
        return (menue.menSelect);
}

void  SetLMenue (char *items, int anz, int dim, char *vline, char *caption)
/**
Parameter fuer Auswahlmenue setzen.
**/
{
        menuecaption = caption;
        menuevline   = vline;
        menueitems = (char *) items;
        menueanz = anz;
        menuedim = dim;
}

void SetBorder (DWORD border)
/**
Border-Flag fuer Fenster setzen.
**/
{
         WinBorder = border;
}

void SetBorderEx (DWORD borderEx)
/**
Border-Flag fuer Fenster setzen.
**/
{
         WinBorderEx = borderEx;
}

void SetAktivWindow (HWND win)
/**
Aktives Fenster setzen.
**/
{
            AktivWindow = win;
}

void SetAktivCbox (form *cbox)
/**
Aktives Fenster setzen.
**/
{
            AktivCbox = cbox;
}


void SetLbox (void)
/**
ListBox auf Aktiv setzen.
**/
{
         if (lbox)
         {
             SetAktivWindow (lbox);
         }
}

void RefreshLbox (void)
/**
Listbox neu Zeichnen.
**/
{
             if (lbox)
             {
                      InvalidateRect (lbox, NULL, TRUE);
                      UpdateWindow (lbox);
             }
}

void SetCurrentField (int fieldnr)
/**
Aktuelles Feld fuer ebter_form setzen.
**/
{
            currentfield = fieldnr;
            setfield = 1;
}

void SetCurrentFocus (int fieldnr)
/**
Aktuelles Feld fuer enter_form setzen.
**/
{
            currentfield = fieldnr;
            EmSetSel ();
            SetFocus (current_form->mask[currentfield].feldid);
            setfield = 1;
}


void Setlistenter (int ls)
/**
Variable listenter setzen.
**/
{
          listenter = ls;
          if (listenter != 0) listenter = 1;
}

void SetListEWindow (int lw)
/**
Variable listenter setzen.
**/
{
          ListEWindow = lw;
          if (ListEWindow != 0) ListEWindow = 1;
}

void InitMenue (void)
/**
Menue Initialisieren.
**/
{
         int i;

         for (i = 0; i < 1000; i ++)
         {
                     menue.menArr[i] = NULL;
                     menue.menArr1[i] = NULL;
                     menue.menArr2[i] = NULL;
         }
         menue.menZeile = 0;
         menue.menSpalte = 0;
         menue.menAnz = 0;
         menue.menTAnz = 0;
         menue.menTitle[0] = NULL;
         menue.menVlpos = NULL;
         listzab = 1;
         AktMenZeile = 0;
         AktZeile = 0;
         AktSpalte = 0;
}

void MenTextSave (struct LMENUE *men)
/**
Texte von Menue sichern.
**/
{
          char *buffer;
          int i, len;

          for (i = 0; i < men->menAnz; i ++)
          {
                      if (men->menArr[i])
                      {
                             len = strlen (men->menArr[i]) + 1;
                             buffer = (char *)
                                   GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                                len);
                             strcpy (buffer, men->menArr[i]);
                             men->menArr[i] = buffer;
                      }

                      if (men->menArr1[i])
                      {
                             len = strlen (men->menArr1[i]) + 1;
                             buffer = (char *)
                                  GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               len);
                             strcpy (buffer, men->menArr1[i]);
                             men->menArr1[i] = buffer;
                      }

                      if (men->menArr2[i])
                      {
                             len = strlen (men->menArr2[i]) + 1;
                             buffer = (char *)
                                   GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                                len);
                             strcpy (buffer, men->menArr2[i]);
                             men->menArr2[i] = buffer;
                      }
          }

          for (i = 0; i < men->menTAnz; i ++)
          {
                      if (men->menTitle[i])
                      {
                             len = strlen (men->menTitle[i]) + 1;
                             buffer = (char *)
                               GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,len);
                             strcpy (buffer, men->menTitle[i]);
                             men->menTitle[i] = buffer;
                      }
          }
}


void SaveMenue (void)
/**
Aktuelles Menue sichern.
**/
{
          if (menuestack ==  20) return;
          memcpy ((char *) &menuetab[menuestack],
                  (char *) &menue, sizeof (struct LMENUE));
          MenTextSave (&menuetab[menuestack]);
          lboxstack[menuestack] = lbox;
          hListBoxStack[menuestack] = hListBox;
          AktWinStack[menuestack ] = AktivWindow;
          listzabStack[menuestack ] = listzab;
          if (menuestack < 20) menuestack ++;
}

void RestoreMenue (void)
/**
Aktuelles Menue zurueckholen.
**/
{
          if (menuestack == 0)
          {
                      lbox = NULL;
                      hListBox = NULL;
                      return;
          }
          menuestack --;
          memcpy ((char *) &menue, (
                   char *) &menuetab [menuestack], sizeof (struct LMENUE));
          lbox = lboxstack[menuestack];
          hListBox = hListBoxStack[menuestack];
          AktivWindow = AktWinStack[menuestack];
          listzab = listzabStack[menuestack ];
}

void GetMenue (struct LMENUE *men)
/**
MenueWerte holen.
**/
{
         memcpy ((char *) men, (char *) &menue, sizeof (struct LMENUE));
}

void SetMenue (struct LMENUE *men)
/**
MenueWerte setzen.
**/
{
         memcpy ((char *) &menue, (char *) men, sizeof (struct LMENUE));
}

HWND GethListBox (void)
/**
FensterHandle holen.
**/
{
          return (hListBox);
}


void SetListButtons (struct LISTBU *butab)
{
         if (butab)
         {
             SpezBu = TRUE;
             BuTab = butab;
         }
         else
         {
             SpezBu = FALSE;
         }
}



void SetHLines (int line)
{
	     hlines = line;
}

void SetVLines (int line)
{
	     vlines = line;
}


void SethListBox (HWND hWnd)
/**
FensterHandle setzen.
**/
{
          hListBox = hWnd; 
}

HWND Getlbox (void)
/**
FensterHandle holen.
**/
{
          return (lbox);
}

HWND GetlboxBar (void)
/**
FensterHandle holen.
**/
{
          return (lboxBar);
}

void Setlbox (HWND hWnd)
/**
FensterHandle setzen.
**/
{
          lbox = hWnd; 
}

void all_break_enter (void)
/**
Flag zum Abbrechen aller Dialoge uns listen.
**/
{
         all_enter_break = 1;
}


void no_break_end (void)
/**
Flag zum Abbrechen des Dialogs am Ende setzen.
**/
{
         end_break = 0;
}


void break_end (void)
/**
Flag zum Abbrechen des Dialogs am Ende setzen.
**/
{
         end_break = 1;
}


void no_break_enter (void)
/**
Flag zum Abbrechen des Dialogs setzen.
**/
{
         enter_break = 0;
}

void break_enter (void)
/**
Flag zum Abbrechen des Dialogs setzen.
**/
{
         enter_break = 1;
}

void SetBeforeList (void (*bfl_proc) (void))
/**
Procedure, die vor der Listezeilenanzeige aufgerufen wird, setzen.
**/
{
              BeforeList = bfl_proc;
}

void SetAfterList (void (*bfl_proc) ())
/**
Procedure, die nach der Listezeilenanzeige aufgerufen wird, setzen.
**/
{
              AfterList = bfl_proc;
}

void SetFillEmpty (void (*bfl_proc) (void))
/**
Procedure, die vor dem Einfuegen einer neuen Zeile aufgerufen wird, setzen.
**/
{
              FillEmptyRow = bfl_proc;
}

void SetNoDlgProc (int (*NoDlgProc) (MSG *))
/**
IsNoDlgMess setzen.
**/
{
             IsNoDlgMess = NoDlgProc;
}

void SetDblClck (void (*dblclck_proc) (int), int dblmode)
/**
Procedure, die vor der Listezeilenanzeige aufgerufen wird, setzen.
**/
{
              DoDoubleClck = dblclck_proc;
              WithDblClck =  dblmode;
}


int set_fkt (int (*fkt_proc) (), short fktnr)
/**
Procedure f�r Funtionstaste setzen.
**/
{
              if (fktnr > 12) return (-1);
              fkt_aktiv [fktnr] = fkt_proc;
              return (0);
}

int writelog (char * format, ...)
/**
Debugausgabe.
**/
{
          va_list args;
          FILE *logfile;
          static int log_ok = 0;
          char logtext [0x1000];

          if (_WMDEBUG == 0) return 0;  
          if (log_ok == 0)
          {
                     log_ok = 1;
                     logfile = fopen ("\\user\\fit\\tmp\\windeb.log",
                                      "wb");
          }
          else
          {
                     logfile = fopen ("\\user\\fit\\tmp\\windeb.log",
                                      "ab");
          }

          if (logfile == (FILE *) 0)
          {
                     return 0;
          }

          va_start (args, format);
          vsprintf (logtext, format, args);
          va_end (args);

          fprintf (logfile, "%-78.78s%s", logtext, crnl);
          fclose (logfile);
          return 0;
}

static char *clipped (char *str)
/**
Blank am Ende eines Strings abschneiden.
**/
{
          int i;

          i = strlen (str);
          if (i == 0) return (str);

          for (i = i - 1; i > 0; i --)
          {
                    if (str[i] > ' ')
                    {
                              str [i + 1] = (char) 0;
                              break;
                    }
          }
          return (str);
}


void break_list (void)
/**
Flag zum Abbrechen einer Liste setzen.
**/
{
         list_break = 1;
}

void init_break_list (void)
/**
Flag zum Abbrechen einer Liste setzen.
**/
{
         list_break = 0;
}

void disp_mess (char *text, int modus)
/**
Medlung ausgeben.
**/
{
       HWND hWnd;

       hWnd = GetFocus ();

       if (AktivWindow == NULL)
       {
                 AktivWindow = GetActiveWindow ();
       }
                 
       if (modus == 0)
       {
                 MessageBox (AktivWindow, text, "", MB_OK);
       }

       else if (modus == 1)
       {
                 MessageBox (AktivWindow, text, "", MB_OK | MB_ICONWARNING);
       }

       else if (modus == 2)
       {
                 MessageBeep (MB_ICONHAND);
                 MessageBox (AktivWindow, text, "", MB_OK | MB_ICONERROR);
       }
/*
       else
       {
                 wa = waitwindow (AktivWindow, text, "");
                 nap (modus);
                 DestroyWindow (wa);
       }
*/
       SetFocus (hWnd);
}

int print_mess (int mode, char * format, ...)
/**
Ausgabe in dbfile.
**/
{
        va_list args;
 
        va_start (args, format);
        vsprintf (messbuffer, format, args);
        va_end (args);
        disp_mess (messbuffer, mode);
        return (0);
}

HFONT EzCreateFont (HDC hdc, char * szFaceN, int iDeciPtH,
                    int iDeciPtW, int iAttrib, BOOL fLogRes)
     {
     float      cxDpi, cyDpi ;
     HFONT      hFont ;
     LOGFONT    lf ;
     POINT      pt ;
     TEXTMETRIC tm ;

     SaveDC (hdc) ;

     SetGraphicsMode (hdc, GM_ADVANCED) ;
     ModifyWorldTransform (hdc, NULL, MWT_IDENTITY) ;

     SetViewportOrgEx (hdc, 0, 0, NULL) ;
     SetWindowOrgEx   (hdc, 0, 0, NULL) ;

     if (fLogRes)
          {
          cxDpi = (float) GetDeviceCaps (hdc, LOGPIXELSX) ;
          cyDpi = (float) GetDeviceCaps (hdc, LOGPIXELSY) ;
          }
     else
          {
          cxDpi = (float) (25.4 * GetDeviceCaps (hdc, HORZRES) /
                                  GetDeviceCaps (hdc, HORZSIZE)) ;

          cyDpi = (float) (25.4 * GetDeviceCaps (hdc, VERTRES) /
                                  GetDeviceCaps (hdc, VERTSIZE)) ;
          }

     pt.x = (int) (iDeciPtWidth  * cxDpi / 72) ;
     pt.y = (int) (iDeciPtHeight * cyDpi / 72) ;

     DPtoLP (hdc, &pt, 1) ;

     lf.lfHeight         = - (int) (fabs (pt.y) / 10.0 + 0.5) ;
     lf.lfWidth          = 0 ;
     lf.lfEscapement     = 0 ;
     lf.lfOrientation    = 0 ;
     lf.lfWeight         = iAttributes & EZ_ATTR_BOLD      ? 700 : 0 ;
     lf.lfItalic         = iAttributes & EZ_ATTR_ITALIC    ?   1 : 0 ;
     lf.lfUnderline      = iAttributes & EZ_ATTR_UNDERLINE ?   1 : 0 ;
     lf.lfStrikeOut      = iAttributes & EZ_ATTR_STRIKEOUT ?   1 : 0 ;
     lf.lfCharSet        = 0 ;
     lf.lfOutPrecision   = 0 ;
     lf.lfClipPrecision  = 0 ;
     lf.lfQuality        = 0 ;
     lf.lfPitchAndFamily = 0 ;

     strcpy (lf.lfFaceName, szFaceName) ;

     hFont = CreateFontIndirect (&lf) ;

     if (iDeciPtWidth != 0)
          {
          hFont = (HFONT) SelectObject (hdc, hFont) ;

          GetTextMetrics (hdc, &tm) ;

          DeleteObject (SelectObject (hdc, hFont)) ;

          lf.lfWidth = (int) (tm.tmAveCharWidth *
                              fabs (pt.x) / fabs (pt.y) + 0.5) ;

          hFont = CreateFontIndirect (&lf) ;
          }

     RestoreDC (hdc, -1) ;
     SetFont (hFont);
     return hFont ;
}

HFONT SetWindowFont (HWND hWnd)
/**
Font fuer ein Fenster setzen.
**/
{
         HDC hdc;
         HFONT hFont;

         hdc = GetDC (hWnd);
         hFont = EzCreateFont (hdc,
                              "Courier New",
                              100,
                                0,
                                0,
                                TRUE);
         SelectObject (hdc, hFont);
         ReleaseDC (hWnd, hdc);
         return (hFont);
}


void SetTmFont (HWND hWnd, TEXTMETRIC * tm)
/**
Font fuer ein Fenster setzen.
**/
{
         HDC hdc;
         HFONT hFont, oldfont;

        hFont = SetWindowFont (hWnd);
        hdc = GetDC (hMainWindow);
        oldfont = SelectObject (hdc, hFont);
        GetTextMetrics (hdc, tm);
        DeleteObject (SelectObject (hdc, oldfont));
        ReleaseDC (hWnd, hdc);
        DeleteObject (hFont);
}


HFONT CreateStdFont (HWND hWnd)
/**
Standardfont generieren.
**/
{
         HFONT hFont;
         HDC hdc;

         if (stdHfont)
         {
                   return stdHfont;
         }
         SetEnvFont ();
         stdfont ();
         hdc = GetDC (hWnd);
         hFont = EzCreateFont (hdc,
                              "Courier New",
                              100,
                                0,
                                0,
                                TRUE);
         ReleaseDC (lbox, hdc);
         StdFont = hFont;
         stdHfont = hFont;
         return hFont;
}

BOOL IsMon31 (int mon)
/**
Monat im Datum pruefen.
**/
{
         static int mon31[] = {1,3,5,7,8,10,12};
         int i;

         for (i = 0; i < 7; i ++)
         {
                      if (mon == mon31[i]) return TRUE;
         }
         return FALSE;
}

BOOL IsMon30 (int mon)
/**
Monat im Datum pruefen.
**/
{
         static int mon30[] = {4,6,9,11};
         int i;

         for (i = 0; i < 4; i ++)
         {
                      if (mon == mon30[i]) return TRUE;
         }
         return FALSE;
}

BOOL IsMon29 (int mon, int jr)
/**
Monat im Datum pruefen.
**/
{
         static int sjr = 1954;
         int jrdiff; 

         if (mon != 2) return FALSE; 

         jrdiff = jr - sjr;
         if (jr % 4 == 0) return TRUE; 
         return FALSE;
}
 

BOOL IsMon28 (int mon, int jr)
/**
Monat im Datum pruefen.
**/
{
         static int sjr = 1954;
         int jrdiff; 

         if (mon != 2) return FALSE; 

         jrdiff = jr - sjr;
         if (jr % 4) return TRUE; 
         return FALSE;
}
 
void DatFormat (char *dest, field *feld)
/**
Datumsfeld formatieren.
**/
{
      char tags [3];
      char mons [3];
      char jrs [3];
      short tag;
      short mon;
      short jr;
      char *punkt;

      if (memcmp (feld->feld, "  ", 2) <= 0)
      {
                 strcpy (dest, feld->feld);
                 return;
      }

      if (punkt = strchr (feld->feld, '.'))
      {
                 tag = atoi (feld->feld);
                 punkt += 1;
                 mon = atoi (punkt);
                 punkt = strchr (punkt, '.');
                 if (punkt == 0)
                 {
                              jr = jrh1;
                 }
                 else
                 {
                               punkt += 1;
                               jr = atoi (punkt);
                 }
      }
      else
      {
                 memcpy (tags, feld->feld, 2);
                 tags[2] = 0;
                 memcpy (mons, &feld->feld[2], 2);
                 mons[2] = 0;
                 strcpy (jrs, &feld->feld[4]);
                 tag = atoi (tags);
                 mon = atoi (mons);
                 jr = atoi (jrs);
      }

      if (jr < 100)
      {
             if (jr < jrhstart)
             {
                  jr += jrh2;
             }
             else
             {
                  jr += jrh1;
             }
      }


      if (mon > 12 || mon < 1) 
      {
                 strcpy (dest, "02.01.1899");
                 return;
      }

      if (tag < 1)
      {
                 strcpy (dest, "01.01.1899");
                 return;
      }
                 
      if (IsMon31 (mon) && tag > 31) 
      {
                 strcpy (dest, "01.01.1899");
                 return;
      }
      if (IsMon30 (mon) && tag > 30) 
      {
                 strcpy (dest, "01.01.1899");
                 return;
      }

      if (IsMon29 (mon, jr) && tag > 29) 
      {
                 strcpy (dest, "01.01.1899");
                 return;
      }

      if (IsMon28 (mon, jr) && tag > 28) 
      {
                 strcpy (dest, "01.01.1899");
                 return;
      }

      if (jr < 100)
      {
             if (jr < jrhstart)
             {
                  jr += jrh2;
             }
             else
             {
                  jr += jrh1;
             }
      }

      if (memcmp (feld->picture, "dd.mm.yyyy", 10) == 0)
      {
                  sprintf (dest, "%02hd.%02hd.%04hd",
                                  tag,mon,jr);
      }
      else if (memcmp (feld->picture, "dd.mm.yy", 8) == 0)
      {
                  sprintf (dest, "%02hd.%02hd.%02hd",
                                  tag,mon,jr % 100);
      }
                   
      else if (memcmp (feld->picture, "ddmmyyyy", 8) == 0)
      {
                  sprintf (dest, "%02hd%02hd%04hd",
                                  tag,mon,jr);
      }
      else if (memcmp (feld->picture, "ddmmyy", 6) == 0)
      {
                  sprintf (dest, "%02hd%02hd%02hd",
                                  tag,mon,jr % 100);
      }
      else
      {
                  strcpy (dest, "01.01.1900");
      }
}


void ToFormat (char *dest, field *feld)
/**
Feld formatiert in dest uebertragen.
**/
{
        char format[16];
        double dwert;
        int vkomma;
        int nkomma;
        char *punkt;

        if (feld->attribut & REMOVED)
        {
                    memset (dest, ' ', feld->length);
                    dest[feld->length - 1] = (char) 0;
                    return;
        }
        if (strlen (feld->picture) == NULL)
        {
                    strcpy (dest, feld->feld);
                    return;
        }
        if (memcmp (feld->picture, "dd", 2) == 0)
        {
                    DatFormat (dest, feld);
                    return;
        }
        if (feld->picture[0] != '%')
        {
                    strcpy (dest, feld->feld);
                    return;
        }
        vkomma = nkomma = 0;
        vkomma = atoi (feld->picture + 1);
        punkt = strchr (feld->picture, '.');
        if (punkt)
        {
                    nkomma = atoi (punkt + 1);
        }
	   else if (punkt = strchr (feld->picture, ','))		
        {
                    nkomma = atoi (punkt + 1);
        }
        sprintf (format, "%c%d.%dlf", '%', vkomma, nkomma);

        dwert = (double) ratod (feld->feld);
        sprintf (dest, format, dwert);
        return;
}


BOOL IsDlgClient (HWND hWnd, POINT *mpos)
/**
Test, ob die Mousepostion im Dialogfenster ist.
**/
{

         POINT mpost;
         RECT wpos;
         int xlen;
         int ylen;

         memcpy (&mpost, mpos, sizeof (POINT));
         ScreenToClient (hWnd, &mpost);
/*
         GetWindowRect (hWnd,  &wpos);
         xlen = wpos.right - wpos.left;
         ylen = wpos.bottom - wpos.top;
*/

         GetClientRect (hWnd,  &wpos);
         xlen = wpos.right;
         ylen = wpos.bottom;

         if (mpost.x < 0) return FALSE;
         if (mpost.y < 0) return FALSE;
         if (mpost.x > xlen) return FALSE;
         if (mpost.y > ylen) return FALSE;
         return TRUE;
}

int MousetohWnd (HWND hWnd, POINT *mpos)
/**
Abstand von Mouse zu Window ermitteln.
**/
{

         RECT wpos;
         RECT cpos;
         int diff;

         GetClientRect (hWnd, &cpos);
         GetWindowRect (hWnd,  &wpos);
         wpos.bottom = wpos.top + cpos.bottom; 
         diff = mpos->y - wpos.bottom;
         if (diff > 20) return 2;
         if (diff > 0) return 1;
         diff = wpos.top - mpos->y; 
         if (diff > 20) return -2;
         if (diff > 0) return -1; 
         return 0;
}


void RecColorTitle (HDC hdc, COLORREF bcolor, int xchar, int ychar)
/**
Eine Zeile mit Hintergrundfarbe.
**/
{
         HBRUSH hBrush;

         hBrush = CreateSolidBrush (bcolor);
         SelectObject (hdc, hBrush);
         SelectObject (hdc, GetStockObject (NULL_PEN));
         if (menue.menTAnz == 0)
         {

                  Rectangle (hdc,0, 0, xchar * (menue.menWspalten + 1),
                                       ychar + 1);
         }
         else
         {
                  Rectangle (hdc,0, 0, xchar * (menue.menWspalten + 1),
                                       ychar * menue.menTAnz + 1);
         }
         DeleteObject (hBrush);
}


void RecColor (HDC hdc, COLORREF bcolor, int xchar, int ychar)
/**
Eine Zeile mit Hintergrundfarbe.
**/
{
         int y;
         HBRUSH hBrush;

         hBrush = CreateSolidBrush (bcolor);

         SelectObject (hdc, hBrush);
         SelectObject (hdc, GetStockObject (NULL_PEN));
         y = ((menue.menSelect - menue.menZeile) * listzab + sz) * ychar;
         Rectangle (hdc,0, y, xchar * (menue.menWspalten + 1), y + ychar + 1);
         DeleteObject (hBrush); 
}

void PrintVLine (HDC hdc, int y)
/**
Verticale Line Zeichnen.
**/
{
        HPEN hPen;
        int i;
        int x;
        TEXTMETRIC tm;

        if (vlines == 0) return;
        if (menue.menVlpos == NULL) return;

        GetTextMetrics (hdc, &tm);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        SelectObject (hdc, hPen);
        for (i = 0; i < menue.menWspalten; i ++)
        {
               if (i + menue.menSpalte >=
                   (int) strlen (menue.menVlpos)) break;

               if (menue.menVlpos[i + menue.menSpalte] > ' ')
               {
                    x = i * tm.tmAveCharWidth;
                    MoveToEx (hdc, x, y, NULL);
                    LineTo (hdc, x, y + (tm.tmHeight * listzab) - 1);
               }
        }
        DeleteObject (hPen);
}
        
void PrintSLines (HDC hdc)
/**
Horitontale Line fuer Scrollbereich zeichnen.
**/
{
        HPEN hPen;

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);

        SelectObject (hdc, hPen);
        MoveToEx (hdc, menue.srect.left, menue.srect.top, NULL);
        LineTo (hdc, menue.srect.left, menue.srect.bottom);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        SelectObject (hdc, hPen);
        MoveToEx (hdc, 0, menue.srect.top, NULL);
        LineTo (hdc, menue.srect.right, menue.srect.top);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        SelectObject (hdc, hPen);
        MoveToEx (hdc, 0, menue.srect.top + 1, NULL);
        LineTo (hdc, menue.srect.right, menue.srect.top + 1);
        DeleteObject (hPen);
}

void PrintHLine (HDC hdc, int y)
/**
Horitontale Line Zeichnen.
**/
{
        HPEN hPen;
        int ublines;
        int testzeile;
        TEXTMETRIC tm;

        if (hlines == 0) return;
        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);

        if (menue.menTAnz > 1)
        {
                   ublines = menue.menTAnz - MenStart + 1;
        }
        else
        {
                   ublines = 1;
        }
        GetTextMetrics (hdc, &tm);
        testzeile = (y / tm.tmHeight) - menue.menTAnz;
        // if (testzeile == 0) return;
        if (testzeile % ublines) return;
        
        SelectObject (hdc, hPen);
        y += (tm.tmHeight * listzab) - 2;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, menue.menWspalten * tm.tmAveCharWidth, y);
        DeleteObject (hPen);
}

void PrintVLineBlack (HDC hdc)
/**
Horitontale Line Zeichnen.
**/
{
        HPEN hPen;
        int i;
        int x;
        int y;
        int Height;

        if (vlines == 0) return;
        if (menue.menVlpos == NULL) return;

        if (menue.menTAnz > 1)
        {
                   Height = (menue.menTAnz - MenStart) * tm.tmHeight;
                   y = MenStart;
                   y *= tm.tmHeight;
        }
        else
        {
                   Height = tm.tmHeight;
                   y = 0;
        }

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        SelectObject (hdc, hPen);
        for (i = 0; i < menue.menWspalten; i ++)
        {
               if (i + menue.menSpalte >=
                   (int) strlen (menue.menVlpos)) break;

               if (menue.menVlpos[i + menue.menSpalte] > ' ')
               {
                    x = i * tm.tmAveCharWidth;
                    MoveToEx (hdc, x, y, NULL);
                    LineTo (hdc, x, y + Height - 1);
               }
        }
        DeleteObject (hPen);
}
        
void PrintVLineWhite (HDC hdc)
/**
Horitontale Line Zeichnen.
**/
{
        HPEN hPen;
        int i;
        int x;
        int y;
        int Height;

        if (vlines == 0) return;
        if (menue.menVlpos == NULL) return;

        if (menue.menTAnz > 1)
        {
                   Height = (menue.menTAnz - MenStart) * tm.tmHeight;
                   y = MenStart;
                   y *= tm.tmHeight;
        }
        else
        {
                   Height = tm.tmHeight;
                   y = 0;
        }

        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        SelectObject (hdc, hPen);
        if (menue.menSpalte == 0)
        {
                    x = 0;
                    MoveToEx (hdc, x, y, NULL);
                    LineTo (hdc, x, y + Height - 1);
        }
        for (i = 0; i < menue.menWspalten; i ++)
        {
               if (i + menue.menSpalte >=
                   (int) strlen (menue.menVlpos)) break;

               if (menue.menVlpos[i + menue.menSpalte] > ' ')
               {
                    x = i * tm.tmAveCharWidth + 1;
                    MoveToEx (hdc, x, y, NULL);
                    LineTo (hdc, x, y + Height - 1);
               }
        }
        DeleteObject (hPen);
}
        
        
void PrintHLineTitleO (HDC hdc)
/**
Horitontale Line Zeichnen.
**/
{
        HPEN hPen;
        int y;

        if (hlines == 0 && vlines == 0) return;

        if (menue.menTAnz < 2) return;

        hPen = CreatePen (PS_SOLID, 0, WHITECOL);

        SelectObject (hdc, hPen);
        y = MenStart * tm.tmHeight;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, (menue.menWspalten + 1) * tm.tmAveCharWidth, y);
        DeleteObject (hPen);
}
        
void PrintHLineTitle (HDC hdc)
/**
Horitontale Line Zeichnen.
**/
{
        HPEN hPen;
        int y;

        if (hlines == 0 && vlines == 0) return;

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);

        SelectObject (hdc, hPen);
        if (menue.menTAnz)
        {
                    y = menue.menTAnz * tm.tmHeight - 2;
        }
        else
        {
                    y = tm.tmHeight - 2;
        }
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, (menue.menWspalten + 1) * tm.tmAveCharWidth, y);
        DeleteObject (hPen);
}
        

void ShowTitle (HDC hdc, COLORREF color, COLORREF bcolor)
/**
Ueberschrift anzeigen.
**/
{
         char *text;
         int y;
         int xchar;
         int ychar;
         TEXTMETRIC tm;
         int i;

         GetTextMetrics (hdc, &tm);

         xchar = tm.tmAveCharWidth;
         ychar = tm.tmHeight;

         if (menue.menTitle[0] == NULL) return;

         SetBkMode (hdc, OPAQUE);
         SetTextColor (hdc,color);
         SetBkColor (hdc,bcolor);
         if (menue.menhFont)
         {
                     SelectObject (hdc, menue.menhFont);
                     GetTextMetrics (hdc, &tm);
         }
         y = 0;
         if (menue.menTAnz == 0)
         {
                    if (menue.menSpalte >=
                        (int) strlen (menue.menTitle[0])) return;
                    text = &menue.menTitle [0][menue.menSpalte];
                    TextOut (hdc, 0, y, text, strlen (text));
                    y += ychar;
         }
         for (i = 0; i < menue.menTAnz; i ++)
         {
                    if (menue.menSpalte <
                        (int) strlen (menue.menTitle[i]))
                    {
                               text = &menue.menTitle [i][menue.menSpalte];
                               TextOut (hdc, 0, y, text, strlen (text));
                    }
                    y += ychar;
         }
         PrintVLineWhite (hdc);
         PrintVLineBlack (hdc);
         PrintHLineTitle (hdc);
         PrintHLineTitleO (hdc);
         return;
}

void TestListBefore (void)
/**
Test, ob vor einer neu selectierten Zeile eine Funktion
ausgefuehrt werden muss.
**/
{
         int aktsel;

         if (listenter == 0) return;

         if (listform && listform->before != (int (*) ()) 0)
         {
                       aktsel = menue.menSelect;
                       memcpy (liststruct,
                               &listtab [aktsel * listdim],
                                listdim);
                       (*listform->before) ();
         }
         return;
}
                    

void ShowRow (HWND hWnd, COLORREF color, COLORREF bcolor)
/**
Menuezeilen anzeigen.
**/
{
         char *text;
         int arrpos;
         int y;
         int xchar;
         int ychar;
         HDC hdc;
         TEXTMETRIC tm;
         HFONT oldfont;

 	     if (!MenSelect) return; 
         if (menue.menSelect < menue.menZeile) return;

         if  (color == WHITECOL)  // Neu gewaehlte Zeile
         {
                  TestListBefore ();
         }

         hdc = GetDC (hWnd);

         if (menue.menhFont)
         {
                   oldfont = SelectObject (hdc, menue.menhFont);
         }
         GetTextMetrics (hdc, &tm);
         xchar = tm.tmAveCharWidth;
         ychar = tm.tmHeight;

         RecColor (hdc, bcolor,xchar, ychar);

         SetBkMode (hdc, OPAQUE);
         SetTextColor (hdc,color);
         SetBkColor (hdc,bcolor);
         arrpos = menue.menSelect;
         y = ((menue.menSelect - menue.menZeile) * listzab + sz) * ychar;
         text = &menue.menArr[arrpos][menue.menSpalte];
         TextOut (hdc, 0, y, text, strlen (text));
         SetTextColor (hdc,BLACKCOL);
         SetBkColor (hdc,WHITECOL);
         if (listzab > 1)
         {
                   if (menue.menSpalte <
                       (int) strlen (menue.menArr1[arrpos]))
                   {
                         text = &menue.menArr1[arrpos][menue.menSpalte];
                         TextOut (hdc, 0, y + ychar, text, strlen (text));
                   }
         }
         if (listzab > 2)
         {
                   if (menue.menSpalte <
                       (int) strlen (menue.menArr2[arrpos]))
                   {
                         text = &menue.menArr2[arrpos][menue.menSpalte];
                         TextOut (hdc, 0, y + 2 * ychar, text, strlen (text));
                   }
         }
         PrintHLine (hdc,y); 
         PrintVLine (hdc,y); 
         ReleaseDC (hWnd, hdc);
         if (menue.menhFont && menue.menhFont != stdHfont)
         {
                   DeleteObject (SelectObject (hdc, oldfont));
         }
         return;
}

void SelectRow (HWND hWnd, WPARAM wParam, LPARAM lParam)
/**
Zeile selectieren.
**/
{
       int zeile;
       int x,y;
       int xchar, ychar;
       int newselect;
       HDC hdc;
       TEXTMETRIC tm;
       HFONT oldfont;

	   if (!MenSelect) return; 
        selcolor = 1;

        hdc = GetDC (hWnd);

        if (menue.menhFont && menue.menhFont != stdHfont)
        {
                   oldfont = SelectObject (hdc, menue.menhFont);
        }
        GetTextMetrics (hdc, &tm);
        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight;
        if (menue.menhFont && menue.menhFont != stdHfont)
        {
                   DeleteObject (SelectObject (hdc, oldfont));
        }
        ReleaseDC (hWnd, hdc);

        x = LOWORD (lParam);
        y = HIWORD (lParam);

        y -= sz * ychar;
        zeile = y / ychar;
        zeile /= listzab;

        if (zeile < 0)
        {
                   return;
        }

        if (menue.menSelect - menue.menZeile == zeile)
        {
                   return;
        }

        if (listenter)
        {
                    syskey = KEYESC;
                    break_enter ();
        }
        newselect = zeile + menue.menZeile;
        if (newselect >= 0 && newselect < menue.menAnz)
        {
                    ShowRow (hWnd, BLACKCOL, WHITECOL);
                    menue.menSelect = newselect;
                    ShowRow (hWnd, WHITECOL, BLUECOL);
        }
        if (zeile * listzab > menue.menWzeilen  - sz - listzab &&
            menue.menZeile * listzab  < menue.menAnz  * listzab
                                        - (menue.menWzeilen - sz))
        {
                    menue.menZeile ++;
                    SetScrollPos (hWnd, SB_VERT, menue.menZeile * listzab,
                                  TRUE);
                    InvalidateRect (hWnd, NULL, TRUE);
                    UpdateWindow (hWnd);
        }
        else if (zeile < 0 && menue.menZeile > 0)
        {
                    menue.menZeile --;
                    SetScrollPos (hWnd, SB_VERT, menue.menZeile * listzab, 
                                  TRUE);
                    InvalidateRect (hWnd, NULL, TRUE);
                    UpdateWindow (hWnd);
        }
        return;
}

void SetUpdReg (RECT *rec, int idx)
/**
Update-Region fuer eine Zeile setzen.
**/
{
        rec->left = 0;
        rec->right = (menue.menWspalten + 1) * tm.tmAveCharWidth;
        rec->top = idx * tm.tmHeight;
        rec->bottom = rec->top + tm.tmHeight;
}


int InUpdReg (int x, int y, int cx, int cy, PAINTSTRUCT *ps)
/**
Feststellen, ob der Bereich neu gezeichnet werden muss.
**/
{
         int py, px, pcy, pcx;

/* Update-Region ermitteln.                              */

         cx += x;
         cy += y;

         py = ps->rcPaint.top / tm.tmHeight - 1;
         px = ps->rcPaint.left / tm.tmAveCharWidth - 1;
         pcy = ps->rcPaint.bottom / tm.tmHeight + 1;
         pcx = ps->rcPaint.right / tm.tmAveCharWidth + 1;
         if (py < 0) py = 0;
         if (px < 0) px = 0;

         if (cy < py) return (0);
         if (y > pcy) return (0);
         return (1);
}

int GetHwndMenue (HWND hWnd)
/**
Zu hWnd passendes Menue holen.
**/
{
         int i;

         for (i = 0; i < menuestack; i ++)
         {
                  if (lboxstack [i] == hWnd) break;
         }
         if (i == menuestack) return FALSE;
         memcpy (&menue, &menuetab[i], sizeof (menue));
         return TRUE;
}

static void ShowMenue (HWND hWnd)
/**
Menuezeilen anzeigen.
**/
{
         static int aktzeile = 0;
         char *text;
         int arrpos;
         int i;
         HDC hdc;
         PAINTSTRUCT ps;
         int y;
         int xchar;
         int ychar;
         struct LMENUE menuesave;

         memcpy (&menuesave, &menue, sizeof (menue));

         if (hWnd != lbox)
         {
                     if (GetHwndMenue (hWnd) == FALSE) return;
         }

         hdc = BeginPaint (hWnd, &ps);

         if (menue.menhFont)
         {
               SelectObject (hdc, menue.menhFont);
               GetTextMetrics (hdc, &tm);
               xchar = tm.tmAveCharWidth;
               ychar = tm.tmHeight;
         }
         if (InUpdReg (0, 0, menue.menWspalten, 1, &ps));
         {
               RecColorTitle (hdc, LTGRAYCOL, xchar, ychar);
               ShowTitle (hdc, BLACKCOL, LTGRAYCOL);
         }

         SetBkMode (hdc, OPAQUE);
         arrpos = menue.menZeile;
//         for (i = 0; i < menue.menWzeilen - sz; i ++)
         for (i = 0; i < menue.menWzeilen; i ++)
         {
                  if (i >= menue.menAnz) break;  

                  y = (i * listzab + sz);
                  if (!InUpdReg (0, y, menue.menWspalten, 1, &ps))
                  {
                                          arrpos ++;
                                          continue;
                  }
                  y *= ychar;
                  if (i == menue.menAnz - menue.menZeile) break;
                  if (i + menue.menZeile == menue.menSelect &&
                      selcolor && MenSelect)
                  {
                        RecColor (hdc, BLUECOL, xchar, ychar);
                        SetTextColor (hdc,WHITECOL);
                        SetBkColor (hdc,BLUECOL);
                  }
                  else
                  {
                        SetTextColor (hdc,BLACKCOL);
                        SetBkColor (hdc,WHITECOL);
                  }

                  if (menue.menSpalte < (int) strlen (menue.menArr [arrpos]))
                  {
                        text = &menue.menArr[arrpos][menue.menSpalte];
                        TextOut (hdc, 0, y, text, strlen (text));
                        SetTextColor (hdc,BLACKCOL);
                        SetBkColor (hdc,WHITECOL);
                        if (listzab > 1)
                        {
                               if (menue.menSpalte <
                                   (int) strlen (menue.menArr1[arrpos]))
                               {
                                   text =
                                   &menue.menArr1[arrpos][menue.menSpalte];
                                   TextOut (hdc, 0, y
                                        + ychar, text, strlen (text));
                               }
                        }
                        if (listzab > 2)
                        {
                               if (menue.menSpalte <
                                   (int) strlen (menue.menArr2[arrpos]))
                               {
                                   text =
                                   &menue.menArr2[arrpos][menue.menSpalte];
                                   TextOut (hdc, 0, y
                                        + 2 * ychar, text, strlen (text));
                               }
                        }
                  }
                  if (menue.srect.bottom)
                  {
                        PrintSLines (hdc);
                  }
                  PrintHLine (hdc,y); 
                  PrintVLine (hdc,y); 
                  arrpos ++;
         }
         EndPaint (hWnd, &ps);
         memcpy (&menue, &menuesave, sizeof (menue));
         return;
}

void CreateListBox (HWND hWnd, int x, int y, int cx, int cy)
/**
Listbox oeffnen.
**/
{
         HFONT hFont;

         if (novisible)
         {
                    lbox = CreateWindow ("Listbox",
                              "Test List-Box",
                              WS_CHILD |
                              CBS_DROPDOWN |
                              WS_VSCROLL | LBS_NOTIFY,
                              x, y,
                              cx, cy,
                              hWnd,
                              (HMENU) LBOX,
                              hMainInst,
                              NULL);
         }
         else
         {
                    lbox = CreateWindow ("Listbox",
                              "Test List-Box",
                              WS_CHILD |
                              WS_VISIBLE | CBS_DROPDOWN |
                              WS_VSCROLL | LBS_NOTIFY,
                              x, y,
                              cx, cy,
                              hWnd,
                              (HMENU) LBOX,
                              hMainInst,
                              NULL);
         }
         hFont = stdHfont;
         SendMessage (lbox, WM_SETFONT, (WPARAM) hFont, (LPARAM) NULL);
}


void CreateSListBox (HWND hWnd, int x, int y, int cx, int cy)
/**
Listbox oeffnen.
**/
{
         HFONT hFont;
         HDC hdc;

         if (novisible)
         {
                    lbox = CreateWindow ("showlist",
                              "",
                              WS_CHILD |
                              CBS_DROPDOWN |
                              WS_VSCROLL | LBS_NOTIFY,
                              x, y,
                              cx, cy,
                              hWnd,
                              (HMENU) LBOX,
                              hMainInst,
                              NULL);
         }
         else
         {
                    lbox = CreateWindow ("showlist",
                              "",
                              WS_CHILD |
                              WS_VISIBLE | CBS_DROPDOWN |
                              WS_VSCROLL | LBS_NOTIFY,
                              x, y,
                              cx, cy,
                              hWnd,
                              (HMENU) LBOX,
                              hMainInst,
                              NULL);
         }

         menue.rect.left = 0;
         menue.rect.top = tm.tmHeight * sz;
         menue.rect.right = cx;
         menue.rect.bottom = cy;

         menue.trect.left = 0;
         menue.trect.top = 0;
         menue.trect.right = cx;
         menue.trect.bottom = tm.tmHeight * sz;
         stdfont ();
         hdc = GetDC (lbox);
         hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);

         ReleaseDC (lbox,hdc);

         menue.menhFont = hFont;
         SendMessage (lbox, WM_SETFONT, (WPARAM) hFont, (LPARAM) NULL);
}


void CreateListBoxBu (HWND hWnd, int x, int y, int cx, int cy)
/**
Listbox oeffnen.
**/
{
         HFONT hFont;
         DWORD style;
         int lcx;
         HDC hdc;

         lcx = 20 * tm.tmAveCharWidth;
         style = 0;

         if (novisible)
         {
                    lbox = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "Listbox",
                              "Test List-Box",
                              WS_CHILD |
                              CBS_DROPDOWN |
                              WS_VSCROLL | LBS_NOTIFY | style,
                              x, y,
                              cx, cy,
                              hWnd,
                              (HMENU) LBOX,
                              hMainInst,
                              NULL);
         }
         else
         {
                    lbox = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "Listbox",
                              "Test List-Box",
                              WS_CHILD |
                              WS_VISIBLE | CBS_DROPDOWN |
                              WS_VSCROLL | LBS_NOTIFY,
                              x, y,
                              cx, cy,
                              hWnd,
                              (HMENU) LBOX,
                              hMainInst,
                              NULL);
         }

         hdc = GetDC (lbox);
         strcpy (FontName, szFaceName);
         strcpy (szFaceName, "Courier New");
         hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
         ReleaseDC (lbox,hdc);

         SendMessage (lbox, WM_SETFONT, (WPARAM) hFont, (LPARAM) NULL);
}

void CreateVListBoxBu (HWND hWnd, int x, int y, int cx, int cy)
/**
Listbox oeffnen.
**/
{
         HFONT hFont;
         DWORD style;
         int lcx;
         HDC hdc;

         lcx = 20 * tm.tmAveCharWidth;
         style = 0;

         if (novisible)
         {
                    lbox = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "listmenue",
                              "",
                              WS_CHILD | WS_VSCROLL | WS_HSCROLL,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
         }
         else
         {
                    lbox = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "listmenue",
                              "",
                              WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
         }
         menue.rect.left = 0;
         menue.rect.top = tm.tmHeight;
         menue.rect.right = cx;
         menue.rect.bottom = cy;

         menue.trect.left = 0;
         menue.trect.top = 0;
         menue.trect.right = cx;
         menue.trect.bottom = tm.tmHeight;
         menue.menTitle[0] = NULL;
         menue.menVlpos = NULL;

         hdc = GetDC (lbox);
         strcpy (FontName, szFaceName);
         strcpy (szFaceName, "Courier New");
         hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
         ReleaseDC (lbox,hFont);

         SendMessage (lbox, WM_SETFONT, (WPARAM) hFont, (LPARAM) NULL);
}

void CreateEListBox (HWND hWnd, int x, int y, int cx, int cy)
/**
Listbox oeffnen.
**/
{
         HFONT hFont;
         DWORD style;
         int lcx;
         HDC hdc;

         lcx = 20 * tm.tmAveCharWidth;
         style = 0;

         if (novisible)
         {
                    lbox = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "listmenue",
                              "",
                              WS_CHILD | WS_VSCROLL | WS_HSCROLL,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
         }
         else
         {
                    lbox = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "listmenue",
                              "",
                              WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
         }

         menue.rect.left = 0;
         menue.rect.top = tm.tmHeight;
         menue.rect.right = cx;
         menue.rect.bottom = cy;

         menue.trect.left = 0;
         menue.trect.top = 0;
         menue.trect.right = cx;
         menue.trect.bottom = tm.tmHeight;
         menue.menTitle[0] = NULL;
         menue.menVlpos = NULL;
         if (ListFont == 0) stdfont ();

         hdc = GetDC (lbox);
         strcpy (FontName, szFaceName);
         strcpy (szFaceName, "Courier New");
         hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
         ReleaseDC (lbox,hdc);
         strcpy (szFaceName, FontName);

         SendMessage (lbox, WM_SETFONT, (WPARAM) hFont, (LPARAM) NULL);
}


void CreateListButtonsSpez (HWND hWnd, int cx, int cy)
/**
Listbox mit indivuiduellen Buttons oeffnen.
**/
{
         int bulen;
         int bx, by;
         double cyd;
         int i;

         for (i = 0; BuTab[i].BuName; i ++)
         {
             bulen += BuTab[i].len;
             bulen += 2;
         }
         bulen -= 2;
         bulen *= tm.tmAveCharWidth;
         bx = max (1,(cx - bulen) / 2); 
         by = cy + tm.tmAveCharWidth;
         cyd = tm.tmHeight * 1.5;
         cy = (int) cyd;
         for (i = 0; BuTab[i].BuName; i ++)
         {
                  cx = BuTab[i].len * tm.tmAveCharWidth; 
                  BuTab[i].hWnd = CreateWindow ("BUTTON",
                                          BuTab[i].BuName,
                                          WS_CHILD |
                                          WS_VISIBLE | BS_PUSHBUTTON,
                                          bx, by,
                                          cx, cy,
                                          hWnd,
                                          (HMENU) BuTab[i].Id,
                                          hMainInst,
                                          NULL);
                  bx += ((BuTab[i].len + 2) * tm.tmAveCharWidth);
                  SendMessage (BuTab[i].hWnd,
                         WM_SETFONT, (WPARAM) StdFont, 0);
         }
}


void CreateListButtons (HWND hWnd, int cx, int cy)
/**
Listbox oeffnen.
**/
{
         int bx, by;
         double cyd;

         if (SpezBu && BuTab)
         {
             CreateListButtonsSpez (hWnd, cx, cy);
             return;
         }

         by = cy + tm.tmAveCharWidth;
         bx = 20 * tm.tmAveCharWidth;
         bx = (cx - bx) / 2;
         if (bx < 0) bx = 0;
         cx = 11 * tm.tmAveCharWidth;
         cyd = tm.tmHeight * 1.5;
         cy = (int) cyd;

         
         btcancel = CreateWindow ("BUTTON",
                                  " 5 Abbruch ",
                                  WS_CHILD |
                                  WS_VISIBLE | BS_PUSHBUTTON,
                                  bx, by,
                                  cx, cy,
                                  hWnd,
                                 (HMENU) BT_CANCEL,
                                  hMainInst,
                                  NULL);

         bx = bx + 14 * tm.tmAveCharWidth;
         cx =  6 * tm.tmAveCharWidth;

         btok = CreateWindow ("BUTTON",
                              "  OK  ",
                              WS_CHILD |
                              WS_VISIBLE | BS_DEFPUSHBUTTON,
                              bx, by,
                              cx, cy,
                              hWnd,
                              (HMENU) BT_OK,
                              hMainInst,
                              NULL);

         return;
}


void MoveListButtonsSpez (int cx, int cy)
/**
Listbox mit indivuiduellen Buttons oeffnen.
**/
{
         int bulen;
         int bx, by;
         double cyd;
         int i;
         HDC hdc;
         HFONT hFont;

         if (ListFont == 0) 
		 {
			 stdfont ();

             strcpy (FontName, szFaceName);
             strcpy (szFaceName, "Courier New");
             hdc = GetDC (lbox);
             hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
             ReleaseDC (lbox,hdc);
		 }

		 if (hListBox)
		 {
		          InvalidateRect (hListBox, NULL, FALSE);
		 }
         bulen = 0;
         for (i = 0; BuTab[i].BuName; i ++)
         {
             bulen += BuTab[i].len;
             bulen += 2;
         }
         bulen -= 2;
         bulen *= tm.tmAveCharWidth;
         bx = max (1,(cx - bulen) / 2); 
         by = cy + tm.tmAveCharWidth;
         cyd = tm.tmHeight * 1.5;
         cy = (int) cyd;
         for (i = 0; BuTab[i].BuName; i ++)
         {
                  cx = BuTab[i].len * tm.tmAveCharWidth; 
                  MoveWindow (BuTab[i].hWnd, bx, by, cx, cy, TRUE);
                  bx += ((BuTab[i].len + 2) * tm.tmAveCharWidth);
         }
}


void MoveListButtons (int cx, int cy)
/**
Listbox bewegen.
**/
{
         int bx, by;
         double cyd;
         HDC hdc;
         HFONT hFont;

         if (SpezBu && BuTab)
         {
             MoveListButtonsSpez (cx, cy);
             return;
         }
         if (ListFont == 0) 
		 {
			 stdfont ();
  
             strcpy (FontName, szFaceName);
             strcpy (szFaceName, "Courier New");
             hdc = GetDC (lbox);
             hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
             ReleaseDC (lbox,hdc);
             strcpy (szFaceName, FontName);
		 }

         by = cy + tm.tmAveCharWidth;
         bx = 20 * tm.tmAveCharWidth;
         bx = (cx - bx) / 2;
         if (bx < 0) bx = 0;
         cx = 11 * tm.tmAveCharWidth;
         cyd = tm.tmHeight * 1.5;
         cy = (int) cyd;

         MoveWindow (btcancel, bx, by, cx, cy, TRUE);
         bx = bx + 14 * tm.tmAveCharWidth;
         cx =  6 * tm.tmAveCharWidth;
         MoveWindow (btok, bx, by, cx, cy, TRUE);

         return;
}


void RegisterListBox (void)
/**
Fenster fuer Listbvbox generieren.
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered) return;

        registered = 1;
        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hMainInst;
        wc.hIcon         =  0;
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.lpszMenuName  =  "";
        wc.lpfnWndProc   =  EnterListBoxProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "hEnterbox";
        RegisterClass(&wc);

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
        wc.lpfnWndProc   =  EnterFormProc;
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hFormbox";
        RegisterClass(&wc);


        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
        wc.lpfnWndProc   =  ListBoxProc;
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hListbox";
        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "hListboxBu";
        RegisterClass(&wc);

        wc.style         =  CS_HREDRAW | CS_VREDRAW |
                            CS_DBLCLKS | CS_OWNDC;
        wc.lpfnWndProc   =  ListProc;
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "listmenue";
        RegisterClass(&wc);

        wc.style         =  CS_HREDRAW | CS_VREDRAW |
                            CS_DBLCLKS | CS_OWNDC;
        wc.lpfnWndProc   =  ShowListProc;
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "showlist";
        RegisterClass(&wc);

        return;
}

HWND CreateListWindowEnEx (int x , int y, int cx, int cy)
/**
Window fuer Listbox generieren.
**/
{
        HDC hdc;
        HWND EnWindow;
        HWND hWnd;
        HFONT hFont, oldFont;

        if (AktivWindow == NULL)
        {
                 AktivWindow = GetActiveWindow ();
        }
        hWnd = AktivWindow;
        hMainInst = (HANDLE) GetWindowLong (AktivWindow, GWL_HINSTANCE);

        RegisterListBox ();

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        if (ListFont == 0) stdfont ();
        hdc = GetDC (AktivWindow);
        strcpy (FontName, szFaceName);
        strcpy (szFaceName, "Courier New");
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);

        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (SelectObject (hdc, oldFont)); 
        ReleaseDC (AktivWindow, hdc);
        DeleteObject (hFont);

        if (VScroll) cy + tm.tmHeight;

        if (isChild)
        {
                     EnWindow  = CreateWindow ("hEnterBox",
                                 "",
                                 WS_VISIBLE | WS_CHILD,
                                 x, y,
                                 cx + tm.tmAveCharWidth,
                                 cy + tm.tmHeight,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);
         }
         else
         {
                     EnWindow = CreateWindow ("hEnterBox",
                                 "",
                                 WS_VISIBLE | WS_CAPTION,
                                 x, y,
                                 cx + 2 * tm.tmAveCharWidth,
                                 cy + tm.tmHeight,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);
        }
        CreateEListBox (EnWindow, tm.tmAveCharWidth, 0, cx, cy);

        if (vlines && menuevline)
        {
                SendMessage (lbox, LB_VPOS,  0, (LPARAM) menuevline);
        }
        hListBox = EnWindow;
        SendMessage (hListBox, WM_SIZE, 0l , 0l);
        return (EnWindow);
}


HWND CreateListWindowEn (int x , int y, int cx, int cy)
/**
Window fuer Listbox generieren.
**/
{
        HDC hdc;
        int hcx, hcy;
        double yd, cyd1, cyd2;
        HWND EnWindow;
        HWND hWnd;
        HFONT hFont, oldFont;

        if (AktivWindow == NULL)
        {
                 AktivWindow = GetActiveWindow ();
        }
        hWnd = AktivWindow;
        hMainInst = (HANDLE) GetWindowLong (AktivWindow, GWL_HINSTANCE);

        RegisterListBox ();

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        hdc = GetDC (AktivWindow);
        stdfont ();
        strcpy (FontName, szFaceName);
        strcpy (szFaceName, "Courier New");
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (SelectObject (hdc, oldFont)); 
        ReleaseDC (AktivWindow, hdc);
        DeleteObject (hFont);

        if (VScroll) cy ++;
        x  = tm.tmAveCharWidth * (x - 1);
        yd = (double) tm.tmHeight * y * 1.5;
        y  = (int) yd;
        hcx = tm.tmAveCharWidth * (cx + 5);
        cx = tm.tmAveCharWidth * (cx + 3);

        cyd1 = (double) tm.tmHeight * (cy + 1.5);

        cyd2 = (double) tm.tmHeight * (cy + 4.5);
        cy = (int) cyd1;
        hcy = (int) cyd2;

        if (isChild)
        {
                     EnWindow  = CreateWindow ("hEnterBox",
                                 "",
                                 WS_VISIBLE | WS_CHILD,
                                 x, y,
                                 hcx, hcy + tm.tmHeight,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);
         }
         else
         {
                     EnWindow = CreateWindow ("hEnterBox",
                                 "",
                                 WS_VISIBLE | WS_CAPTION,
                                 x, y,
                                 hcx, hcy + tm.tmHeight,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);
        }
        CreateEListBox (EnWindow, tm.tmAveCharWidth, 0, cx, cy);

        if (vlines && menuevline)
        {
                SendMessage (lbox, LB_VPOS,  0, (LPARAM) menuevline);
        }
        hListBox = EnWindow;
        SendMessage (hListBox, WM_SIZE, 0l , 0l);
        return (EnWindow);
}

void CreateSListWindow (int x , int y, int cx, int cy, char *caption)
/**
Window fuer Listbox generieren.
**/
{
        HDC hdc;
        int i;
        int menpos;
        HWND hWnd;
        HFONT hFont;

        AktivWindow = GetActiveWindow ();
        hWnd = AktivWindow;
        AktivWindow = GetParent (hWnd);
        if (AktivWindow == NULL) AktivWindow = hWnd;
        hMainInst = (HANDLE) GetWindowLong (AktivWindow, GWL_HINSTANCE);

        RegisterListBox ();

        AktivWindow = hMainWindow;
        stdfont ();
        hdc = GetDC (AktivWindow);
        strcpy (FontName, szFaceName);
        strcpy (szFaceName, "Courier New");
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        SelectObject (hdc,hFont);

        GetTextMetrics (hdc, &tm);
        ReleaseDC (AktivWindow, hdc);

        DeleteObject (hFont);

        x  = tm.tmAveCharWidth * x;
        y  = tm.tmHeight * y;
        y += y / 3;

        cx = tm.tmAveCharWidth * (cx + 3);
        cy = tm.tmHeight * cy;
        cy += cy / 3;

        if (isChild)
        {
                     hListBox = CreateWindow ("hListBox",
                                 caption,
                                 WS_VISIBLE | WS_CHILD | WS_CAPTION |
                                 WS_SYSMENU,
                                 x, y, cx + tm.tmAveCharWidth,
                                 cy + tm.tmHeight + tm.tmHeight / 2,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);
         }
         else
         {
                     hListBox = CreateWindow ("hListBox",
                                 "",
                                 WS_VISIBLE | WS_SYSMENU | WS_CAPTION |
                                 WS_POPUP,
                                 x, y, cx + tm.tmAveCharWidth,
                                 cy + tm.tmHeight + tm.tmHeight / 2 + 2,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);
        }

        CreateSListBox (hListBox, 0, 0, cx, cy);

        if (fillwindow == 0) return;

        for (i = 0, menpos = 0; i < menueanz; i ++, menpos += menuedim)
        {
                SendMessage (lbox, LB_INSERTSTRING, -1, 
                                 (LPARAM) (char *) &menueitems [menpos]);
        }
        SendMessage (lbox, LB_SETCURSEL, 0, 0L);
        SendMessage (hListBox, WM_SIZE, 0l , 0l);
        SetFocus (lbox);
        return;
}


void CreateListWindowBuEx (HWND hWnd, int x , int y, int cx, int cy,
                         char *caption)
/**
Window fuer Listbox generieren.
**/
{
        HDC hdc;
        int i;
        int menpos;
        int hcx, hcy;
        double yd, cyd1, cyd2;
        HFONT hFont, oldfont;

        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);

        RegisterListBox ();
        stdfont ();
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        oldfont = SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (SelectObject (hdc, oldfont));
        ReleaseDC (hMainWindow, hdc);
        DeleteObject (hFont);

        if (VScroll) cy ++;
        x  = tm.tmAveCharWidth * (x - 1);
        yd  = (double) tm.tmHeight * y * 1.5;
        y  = (int) yd;

        hcx = tm.tmAveCharWidth * (cx + 5);
        cx = tm.tmAveCharWidth * (cx + 3);

        cyd1 = (double) tm.tmHeight * (cy + 1.5);

        cyd2 = (double) tm.tmHeight * (cy + 4.5);
        cy = (int) cyd1;
        hcy = (int) cyd2;

        hListBox = CreateWindow ("hListBoxBu",
                                 NULL,
                                 WS_VISIBLE | WS_POPUP | WS_CAPTION,
                                 x, y,
                                 hcx, hcy + tm.tmHeight,
                                 hWnd,
                                 NULL,
                                 hMainInst,
                                 NULL);

        if (VScroll)
        {
                  CreateVListBoxBu (hListBox, tm.tmAveCharWidth, 0, cx, cy);
        }
        else
        {
                  CreateListBoxBu (hListBox, tm.tmAveCharWidth, 0, cx, cy);
        }
        CreateListButtons (hListBox, hcx, cy);

        if (VScroll && vlines && menuevline)
        {
                SendMessage (lbox, LB_VPOS,  0, (LPARAM) menuevline);
        }

        if (VScroll && menuecaption)
        {
                SendMessage (lbox, LB_TITLE, 0, (LPARAM) menuecaption);
        }

        if (fillwindow == 0) return;

        for (i = 0, menpos = 0; i < menueanz; i ++, menpos += menuedim)
        {
                SendMessage (lbox, LB_INSERTSTRING, -1, 
                                 (LPARAM) (char *) &menueitems [menpos]);
        }
        SendMessage (lbox, LB_SETCURSEL, 0, 0L);
        SendMessage (hListBox, WM_SIZE, 0l , 0l);
        SetFocus (lbox);
        return;
}


void CreateListWindowBu (int x , int y, int cx, int cy, char *caption)
/**
Window fuer Listbox generieren.
**/
{
        HDC hdc;
        int i;
        int menpos;
        int hcx, hcy;
        double yd, cyd1, cyd2;
        HWND hWnd;
        HFONT hFont, oldfont;

        if (AktivWindow == NULL)
        {
                   AktivWindow = GetActiveWindow ();
        }
        hWnd = AktivWindow;
        AktivWindow = GetParent (hWnd);
        if (AktivWindow == NULL) AktivWindow = hWnd;

        hMainInst = (HANDLE) GetWindowLong (AktivWindow, GWL_HINSTANCE);

        RegisterListBox ();
        stdfont ();

//        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        strcpy (FontName, szFaceName);
        strcpy (szFaceName, "Courier New");
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldfont = SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (SelectObject (hdc, oldfont));
        SelectObject (hdc, oldfont);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);

        if (VScroll) cy ++;
        x  = tm.tmAveCharWidth * (x - 1);
        yd  = (double) tm.tmHeight * y * 1.5;
        y  = (int) yd;

        hcx = tm.tmAveCharWidth * (cx + 5);
        cx = tm.tmAveCharWidth * (cx + 3);

        cyd1 = (double) tm.tmHeight * (cy + 1.5);

        cyd2 = (double) tm.tmHeight * (cy + 4.5);
        cy = (int) cyd1;
        hcy = (int) cyd2;

        hListBox = CreateWindow ("hListBoxBu",
                                 caption,
                                 WS_VISIBLE | WS_POPUP | WS_CAPTION | WS_THICKFRAME,
                                 x, y,
                                 hcx, hcy + tm.tmHeight,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);

        if (VScroll)
        {
                  CreateVListBoxBu (hListBox, tm.tmAveCharWidth, 0, cx, cy);
        }
        else
        {
                  CreateListBoxBu (hListBox, tm.tmAveCharWidth, 0, cx, cy);
        }
        CreateListButtons (hListBox, hcx, cy);

        if (VScroll && vlines && menuevline)
        {
                SendMessage (lbox, LB_VPOS,  0, (LPARAM) menuevline);
        }

        if (VScroll && menuecaption)
        {
                SendMessage (lbox, LB_TITLE, 0, (LPARAM) menuecaption);
        }

        if (fillwindow == 0) return;

        for (i = 0, menpos = 0; i < menueanz; i ++, menpos += menuedim)
        {
                SendMessage (lbox, LB_INSERTSTRING, -1, 
                                 (LPARAM) (char *) &menueitems [menpos]);
        }
        SendMessage (lbox, LB_SETCURSEL, 0, 0L);
        SendMessage (hListBox, WM_SIZE, 0l , 0l);
        SetFocus (lbox);
        return;
}

void CreateListWindowEx (HWND hWnd, int x , int y, int cx, int cy, char *caption)
/**
Window fuer Listbox generieren.
**/
{
        HDC hdc;
        int i;
        int menpos;
        double yd, cyd;

        hMainInst = (HANDLE) GetWindowLong (AktivWindow, GWL_HINSTANCE);

        RegisterListBox ();
        AktivWindow = hWnd;
        hdc = GetDC (AktivWindow);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (AktivWindow, hdc);

        x  = tm.tmAveCharWidth * x;
        yd  = (double) tm.tmHeight * y * 1.5;

        cx = tm.tmAveCharWidth * (cx + 3);
        cyd = (double) tm.tmHeight * (cy + 1.5);

        y  = (int) yd;
        cy = (int) cyd;

        hListBox = CreateWindow ("hListBox",
                                 caption,
                                 WS_VISIBLE | WS_CHILD ,
                                 x, y, cx, cy + tm.tmHeight,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);
        CreateListBox (hListBox, 0, 0, cx, cy);

        if (fillwindow == 0) return;

        for (i = 0, menpos = 0; i < menueanz; i ++, menpos += menuedim)
        {
                SendMessage (lbox, LB_INSERTSTRING, -1, 
                                 (LPARAM) (char *) &menueitems [menpos]);
        }
        SendMessage (lbox, LB_SETCURSEL, 0, 0L);
        SendMessage (hListBox, WM_SIZE, 0l , 0l);
        SetFocus (lbox);
        return;
}



void CreateListWindow (int x , int y, int cx, int cy, char *caption)
/**
Window fuer Listbox generieren.
**/
{
        HDC hdc;
        int i;
        int menpos;
        double yd, cyd;
        HWND hWnd;

        if (AktivWindow == NULL)
        {
                   AktivWindow = GetActiveWindow ();
        }
        hWnd = AktivWindow;
        AktivWindow = GetParent (hWnd);
        if (AktivWindow == NULL) AktivWindow = hWnd;
        hMainInst = (HANDLE) GetWindowLong (AktivWindow, GWL_HINSTANCE);

        RegisterListBox ();

        hdc = GetDC (AktivWindow);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (AktivWindow, hdc);

        x  = tm.tmAveCharWidth * x;
        yd  = (double) tm.tmHeight * y * 1.5;

        cx = tm.tmAveCharWidth * (cx + 3);
        cyd = (double) tm.tmHeight * (cy + 1.5);

        y  = (int) yd;
        cy = (int) cyd;

        hListBox = CreateWindow ("hListBox",
                                 caption,
                                 WS_VISIBLE,
                                 x, y, cx, cy + tm.tmHeight,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);

        CreateListBox (hListBox, 0, 0, cx, cy);

        if (fillwindow == 0) return;

        for (i = 0, menpos = 0; i < menueanz; i ++, menpos += menuedim)
        {
                SendMessage (lbox, LB_INSERTSTRING, -1, 
                                 (LPARAM) (char *) &menueitems [menpos]);
        }
        SendMessage (lbox, LB_SETCURSEL, 0, 0L);
        SendMessage (hListBox, WM_SIZE, 0l , 0l);
        SetFocus (lbox);
        return;
}

int IsListChar (MSG *msg)
/**
Character in Liste suchen.
**/ 
{
        char taste;
        int i, menpos, midx;
        int slen;
        int j;

        taste = (char) msg->wParam;
        midx = SendMessage (lbox, LB_GETCURSEL, 0, 0l);

        tbuffer[tbpos] = taste;
		tbuffer[tbpos + 1]= (char) 0;
        if (tbpos < 62) tbpos ++;
        menpos = (midx + 1);
        for (i = midx + 1; i != midx; i ++, menpos += 1)
        {
                 if (i == menue.menAnz)
                 {
                         i = 0;
                         menpos = 0;
                 }
                 if (i == midx) break;
                 slen = (int) strlen (menue.menArr[menpos]);
                 for (j = 0; j < slen - 1; j ++)
                 {
                        if (menue.menArr[menpos][j] > ' ') break;
                 }

                 if (memcmp (&menue.menArr[menpos][j], tbuffer, tbpos) == 0)
                 {
                                SendMessage (lbox, LB_SETCURSEL, i, 0l);
                                return TRUE;
                 }

/*
                 if (strstr (&menue.menArr[menpos][j], tbuffer))
                 {
                                SendMessage (lbox, LB_SETCURSEL, i, 0l);
                                return TRUE;
                 }
*/
        }
        tbpos = 0;
        return FALSE;
}

int MouseinDlg (HWND hWnd, POINT *mpos)
/**
Test, ob der Mouse-Cursor in einem Eingabefeld steht.
Ohne Dialog
**/
{
           POINT mpost;
           RECT wpos;
           int xlen;
           int ylen;

           memcpy (&mpost, mpos, sizeof (POINT));
           GetWindowRect (hWnd, &wpos);
           xlen = wpos.right - wpos.left;
           ylen = wpos.bottom - wpos.top;
           if (mpost.x < wpos.left) return FALSE;
           if (mpost.y < wpos.top)  return FALSE;
           if (mpost.x > wpos.right) return FALSE;
           if (mpost.y > wpos.bottom) return FALSE;

           return (TRUE);
}

         
int mouseindialog (HWND hWnd, POINT *mpos)
/**
Test, ob der Mouse-Cursor in einem Eingabefeld steht.
Ohne Dialog
**/
{
           if (!MouseLocked) return TRUE;

           if (MouseLockProc)
           {
                             return ((*MouseLockProc) (mpos));
           }

           return (MouseinDlg (hWnd, mpos));

}

int mouseindialogex (HWND hWnd, POINT *mpos)
/**
Test, ob der Mouse-Cursor in einem Eingabefeld steht.
Ohne Dialog
**/
{

           if (MouseLockProc)
           {
                             return ((*MouseLockProc) (mpos));
           }

           return (FALSE);
}


int MouseinClient (HWND hWnd, POINT *mpos)
/**
Test, ob der Mouse-Cursor in einem Eingabefeld steht.
Ohne Dialog
**/
{
           POINT mpost;
           RECT wpos;
           RECT clpos;
           int xlen;
           int ylen;

           memcpy (&mpost, mpos, sizeof (POINT));
           GetWindowRect (hWnd, &wpos);
           GetClientRect (hWnd, &clpos);
           wpos.right  = wpos.left + clpos.right;
           wpos.bottom = wpos.top + clpos.bottom;
           xlen = wpos.right - wpos.left;
           ylen = wpos.bottom - wpos.top;

           if (mpost.x < wpos.left) return FALSE;
           if (mpost.y < wpos.top)  return FALSE;
           if (mpost.x > wpos.right) return FALSE;
           if (mpost.y > wpos.bottom) return FALSE;

           return (TRUE);
}


int MouseinWindow (HWND hWnd, POINT *mpos)
/**
Test, ob der Mouse-Cursor in einem Eingabefeld steht.
Ohne Dialog
**/
{
           POINT mpost;
           RECT wpos;
           int xlen;
           int ylen;

           memcpy (&mpost, mpos, sizeof (POINT));
           GetWindowRect (hWnd, &wpos);
           xlen = wpos.right - wpos.left;
           ylen = wpos.bottom - wpos.top;

           if (mpost.x < wpos.left) return FALSE;
           if (mpost.y < wpos.top)  return FALSE;
           if (mpost.x > wpos.right) return FALSE;
           if (mpost.y > wpos.bottom) return FALSE;

           return (TRUE);
}

int IsListMessage (MSG *msg)
/**
Meldungen auf Listbox-Meldungen testen.
**/
{
       POINT mousepos;
       
       if (msg->message == WM_KEYDOWN)
       {
             switch (msg->wParam)
             {
                 case VK_F1 :
                         syskey = KEY1;
                         testkeys ();
                         return TRUE;
                 case VK_F2 :
                         syskey = KEY2;
                         testkeys ();
                         return TRUE;
                 case VK_F3 :
                         syskey = KEY3;
                         testkeys ();
                         return TRUE;
                 case VK_F4 :
                         syskey = KEY4;
                         testkeys ();
                         return TRUE;
                 case VK_F5 :
                         syskey = KEY5;
                         testkeys ();
                         return TRUE;
                 case VK_F6 :
                         syskey = KEY6;
                         testkeys ();
                         return TRUE;
                 case VK_F7 :
                         syskey = KEY7;
                         testkeys ();
                         return TRUE;
                 case VK_F8 :
                         syskey = KEY8;
                         testkeys ();
                         return TRUE;
                 case VK_F9 :
                         syskey = KEY9;
                         testkeys ();
                         return TRUE;
                 case VK_F10 :
                         syskey = KEY10;
                         testkeys ();
                         return TRUE;
                 case VK_F11 :
                         syskey = KEY11;
                         testkeys ();
                         return TRUE;
                 case VK_F12 :
                         syskey = KEY12;
                         testkeys ();
                         return TRUE;
                 case VK_DOWN :
                         syskey = KEYDOWN;
                         SendMessage (lbox, WM_KEYDOWN,
                                            (WPARAM) VK_DOWN, 0l);
                         return TRUE;
                 case VK_UP :
                         syskey = KEYUP;
                         SendMessage (lbox, WM_KEYDOWN,
                                            (WPARAM) VK_UP, 0l);
                         return TRUE;
                 case VK_PRIOR :
                         syskey = KEYPGD;
                         SendMessage (lbox, WM_KEYDOWN,
                                            (WPARAM) VK_PRIOR, 0l);
                         return TRUE;
                 case VK_NEXT :
                         syskey = KEYPGU;
                         SendMessage (lbox, WM_KEYDOWN,
                                            (WPARAM) VK_NEXT, 0l);
                         return TRUE;
                 case VK_RIGHT :
                 case VK_LEFT :
                         tbpos = 0;
                         return FALSE;

                 case VK_RETURN :
                         SendMessage (hListBox, WM_COMMAND,
                                            MAKELONG (LBOX, LBN_DBLCLK),
                                            (LONG) hListBox);
                         return TRUE;
             }
       }
       else if (msg->message == WM_CHAR)
       {
            if (IsListChar (msg))
            {
                         return TRUE;
            }
       }
       else if (msg->message == WM_LBUTTONDOWN)
       {

            GetCursorPos (&mousepos);

            // if (!mouseindialog (msg->hwnd, &mousepos)) return TRUE;

            tbpos = 0;
            return FALSE;
       }
       else if (msg->message == WM_MOUSEMOVE)
       {

            GetCursorPos (&mousepos);

            // if (!mouseindialog (msg->hwnd, &mousepos)) return TRUE;
       }
       else if (msg->message == WM_LBUTTONDBLCLK)
       {

            GetCursorPos (&mousepos);

            if (!mouseindialog (msg->hwnd, &mousepos)) return TRUE;
       }
       return FALSE;
}
        

static int ProcessMessages(void)
{
        MSG msg;
        int lbreaksave;

        lbreaksave = list_break;
        menueidx = -1;
        list_break = 0;
        tbpos = 0;
        while (list_break == 0)
        {
             if (all_enter_break)
             {
                  return -1;
             }
                   
             if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
             {
                  if (msg.message == WM_QUIT)
                  {
                        PostQuitMessage (0);
                        break;
                  }
                  if (IsListMessage (&msg) == 0)
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
             }
             Sleep (5);
        }
        if (NoCloseList == 0)
        {
                  DestroyWindow (hListBox);
                  hListBox = NULL;
        }
        list_break = lbreaksave;
        return msg.wParam;
}




void SetMatchCase (BOOL mcase)
{
	MatchCase = min (1,mcase);
}

BOOL SearchCompField0 (int i)
/**
Begriff in einem Feld suchen. Begriff im Feld suchen.
**/
{
    char feld [512];
	char *menstr;
	unsigned int pos;
	int len;
	int sf;

	if (searchfield >= listform->fieldanz) return FALSE;

	sf = searchfield;
    pos = listform->mask[sf].pos[1];
	len = listform->mask[sf].length;
	menstr = menue.menArr[i];
	if (strlen (menstr) <= pos) return FALSE;
	memcpy (feld, &menstr[pos], len);
	feld [len] = (char) 0;
	if (MatchCase)
	{
	      if (strstr (feld, CharBuff))
		  {
				     return TRUE;
		  }
	}
	else
	{
	      if (upstrstr (feld, CharBuff))
		  {
				     return TRUE;
		  }
    }
	return FALSE;
}


BOOL SearchCompField (int i)
/**
Begriff in einem Feld suchen. Ganzes Feld mit Begriff vergleichen.
**/
{
    char feld [512];
	char *menstr;
	unsigned int pos;
	int len;
	int sf;

	if (searchfield >= listform->fieldanz) return FALSE;

	sf = searchfield;
    pos = listform->mask[sf].pos[1];
	len = listform->mask[sf].length;
	menstr = menue.menArr[i];
	memset (feld, ' ', CharBuffPos);
    feld [CharBuffPos] = (char) 0;
	if (strlen (menstr) > pos) 
	{
          if (len > (int) strlen (&menstr[pos])) len = (int) strlen (&menstr [pos]); 
 	      memcpy (feld, &menstr[pos], len);
	      feld [len] = (char) 0;
	}

	if (MatchCase)
	{
	      if (memcmp (feld, CharBuff, strlen (CharBuff)) == 0)
		  {
				     return TRUE;
		  }
	}
	else
	{
	      if (strupcmp (feld, CharBuff, strlen (CharBuff)) == 0)
		  {
				     return TRUE;
		  }
    }
	return FALSE;
}


BOOL SearchComp0 (int i)
/**
Begriff in ganzer Listzeile suchen.
**/
{

	if (MatchCase)
	{
	      if (strstr (menue.menArr[i], CharBuff))
		  {
				     return TRUE;
		  }
	}
	else
	{
	      if (upstrstr (menue.menArr[i], CharBuff))
		  {
				     return TRUE;
		  }
    }
	return FALSE;
}


BOOL SearchComp (int i)
{
	  if (searchmode == 0)
	  {
		  return SearchComp0 (i);
	  }
	  else if (searchmode == 1)
	  {
		  return SearchCompField (i);
	  }
	  else if (searchmode == 2)
	  {
		  return SearchCompField0 (i);
	  }
	  return FALSE;
}

void SearchList (int taste)
/**
Liste nach taste absuchen.
**/
{
	   int i;
	   BOOL found = FALSE;
	   
	   if (taste == VK_BACK)
	   {
		         if (CharBuffPos)
				 {
					 CharBuffPos --;
 	                 CharBuff[CharBuffPos] = (char) 0;
				 }
	   }
	   else
	   {
	             CharBuff[CharBuffPos] = (char) taste;
	             CharBuff[CharBuffPos + 1] = (char) 0;
	   }
	   if (choisebox)
	   {
		          if (fCharBuff.mask[0].feldid);
				  {
					       SetWindowText (fCharBuff.mask[0].feldid,
							              CharBuff);
				  }
	   }

	   if (searchmode != 1)
	   {
 	        for (i = menue.menSelect; i < menue.menAnz; i ++)
			{
		           if (SearchComp (i))
				   {
				     found = TRUE;
			         break;
				   }
			}
	        if (found == FALSE)
			{
	               for (i = 0; i < menue.menSelect; i ++)
				   {
 		              if (SearchComp (i))
					  {
				            found = TRUE;
			                break;
					  }
				   }
			}
	   }
	   else if (searchmode == 1)
	   {
 	        for (i = 0; i < menue.menAnz; i ++)
			{
		           if (SearchComp (i))
				   {
				     found = TRUE;
			         break;
				   }
			}
	   }

	   if (found)
	   {
		   menue.menZeile = i;
		   if (taste != VK_BACK)  CharBuffPos ++;
           menue.menSelect = menue.menZeile;
           SetScrollPos (lbox, SB_VERT, menue.menZeile * listzab, 
                                                    TRUE);
           InvalidateRect (lbox, &menue.rect, TRUE);
		   UpdateWindow (lbox);

	   }

	   if (CharBuffPos == 20)
	   {
		   CharBuffPos = 0;
		   CharBuff[0] = (char) 0;
	   }
}

void SearchNextList ()
/**
Liste nach taste absuchen.
**/
{
	   int i;
	   BOOL found = FALSE;
	   
	   if (searchmode != 1)
	   {
 	        for (i = menue.menSelect + 1; i < menue.menAnz; i ++)
			{
		           if (SearchComp (i))
				   {
				     found = TRUE;
			         break;
				   }
			}
	        if (found == FALSE)
			{
	               for (i = 0; i < menue.menSelect; i ++)
				   {
 		              if (SearchComp (i))
					  {
				            found = TRUE;
			                break;
					  }
				   }
			}
	   }
	   else if (searchmode == 1)
	   {
 	        for (i = menue.menSelect + 1; i < menue.menAnz; i ++)
			{
		           if (SearchComp (i))
				   {
				     found = TRUE;
			         break;
				   }
			}
	   }

	   if (found)
	   {
		   menue.menZeile = i;
           menue.menSelect = menue.menZeile;
           SetScrollPos (lbox, SB_VERT, menue.menZeile * listzab, 
                                                    TRUE);
           InvalidateRect (lbox, &menue.rect, TRUE);
	   }
}



int IsListhWndMessage (MSG *msg)
/**
Meldungen auf Listbox-Meldungen testen.
**/
{
       POINT mousepos;

       SetActiveWindow (hListBox);

       if (msg->message == WM_PAINT)
       {
             return FALSE;
       }


       if (msg->hwnd == hListBox);
       else if (IsChild (hListBox, msg->hwnd));
       else
       {
             return TRUE;
       }

	   if (msg->message == WM_CHAR)
	   {
		     if (msg->wParam == VK_BACK)
			 {
		            SearchList (msg->wParam);
			 }
			 else if (msg->wParam >= (int) ' ')
			 {
		            SearchList (msg->wParam);
			 }
			 return TRUE;
	   }


       if (msg->message == WM_KEYDOWN)
       {
             switch (msg->wParam)
             {
                 case VK_F5 :
                         syskey = KEY5;
                         menueidx = -1;
                         DestroyWindow (hListBox);
                         return TRUE;
                 case VK_F1 :
                         syskey = KEY1;
                         testkeys ();
                         return TRUE;
                 case VK_F2 :
                         syskey = KEY2;
                         testkeys ();
                         return TRUE;
                 case VK_F3 :
                         syskey = KEY3;
                         testkeys ();
                         return TRUE;
                 case VK_F4 :
                         syskey = KEY4;
                         testkeys ();
                         return TRUE;
                 case VK_F6 :
                         syskey = KEY6;
                         testkeys ();
                         return TRUE;
                 case VK_F7 :
                         syskey = KEY7;
                         testkeys ();
                         return TRUE;
                 case VK_F8 :
                         syskey = KEY8;
                         testkeys ();
                         return TRUE;
                 case VK_F9 :
                         syskey = KEY9;
                         testkeys ();
                         return TRUE;
                 case VK_F10 :
                         syskey = KEY10;
                         testkeys ();
                         return TRUE;
                 case VK_F11 :
                         syskey = KEY11;
                         testkeys ();
                         return TRUE;
                 case VK_F12 :
                         syskey = KEY12;
                         testkeys ();
                         return TRUE;
                 case VK_DOWN :
                         syskey = KEYDOWN;
                         SendMessage (lbox, WM_KEYDOWN,
                                            (WPARAM) VK_DOWN, 0l);
                         return TRUE;
                 case VK_UP :
                         syskey = KEYUP;
                         SendMessage (lbox, WM_KEYDOWN,
                                            (WPARAM) VK_UP, 0l);
                         return TRUE;
                 case VK_PRIOR :
                         syskey = KEYPGD;
                         SendMessage (lbox, WM_KEYDOWN,
                                            (WPARAM) VK_PRIOR, 0l);
                         return TRUE;
                 case VK_NEXT :
                         syskey = KEYPGU;
                         SendMessage (lbox, WM_KEYDOWN,
                                            (WPARAM) VK_NEXT, 0l);
                         return TRUE;
                 case VK_RIGHT :
                 case VK_LEFT :
                         tbpos = 0;
                         return FALSE;

                 case VK_RETURN :
                         SendMessage (hListBox, WM_COMMAND,
                                            MAKELONG (LBOX, LBN_DBLCLK),
                                            (LONG) hListBox);
                         return TRUE;
             }
       }
       else if (msg->message == WM_CHAR)
       {
            if (IsListChar (msg))
            {
                         return TRUE;
            }
       }
       else if (msg->message == WM_LBUTTONDOWN)
       {

            GetCursorPos (&mousepos);

            if (!mouseindialog (msg->hwnd, &mousepos)) return TRUE;

            tbpos = 0;
            return FALSE;
       }
       else if (msg->message == WM_MOUSEMOVE)
       {

            GetCursorPos (&mousepos);

            if (!mouseindialog (msg->hwnd, &mousepos)) return TRUE;
       }
       else if (msg->message == WM_LBUTTONDBLCLK)
       {

            GetCursorPos (&mousepos);

            if (!mouseindialog (msg->hwnd, &mousepos)) return TRUE;
       }

       return FALSE;
}
        


static int ProcesshWndMessages(HWND hWnd)
{
        MSG msg;
        int lbreaksave;

        lbreaksave = list_break;
        menueidx = -1;
        list_break = 0;
        tbpos = 0;
        while (list_break == 0)
        {
             if (all_enter_break)
             {
                  return -1;
             }
             if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
             {
                  if (msg.message == WM_QUIT)
                  {
                        PostQuitMessage (0);
                        break;
                  }
                  if (IsListhWndMessage (&msg) == 0)
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
             }
             Sleep (5);
        }
        list_break = lbreaksave;
        return msg.wParam;
}

/* Funktionen fuer Dialoge ueber                    */

static void RegisterDialog ()
/**
Fenster fuer Dialog registrieren.
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered) return;

        registered = 1;
        wc.style         =  CS_OWNDC;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hMainInst;
        wc.hIcon         =  0;
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.lpszMenuName  =  "";
        wc.lpfnWndProc   =  EnterDialogBoxProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "hDialogbox";
        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hLDialogbox";
        RegisterClass(&wc);

        // wc.style         =  CS_NOCLOSE;
        wc.style         =  CS_OWNDC;
        wc.lpfnWndProc   =  EnterButtonBoxProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.hIcon         =  NULL;
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "hButtonbox";
        RegisterClass(&wc);

        wc.style         =  CS_OWNDC;
        wc.lpfnWndProc   =  StaticProc;
//        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.hbrBackground =  CreateSolidBrush (GetSysColor (COLOR_3DFACE));
        wc.lpszClassName =  "SStatic";
        RegisterClass(&wc);

        wc.style         =  CS_OWNDC;
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "LStatic";
        RegisterClass(&wc);

        wc.style         =  CS_OWNDC;
        wc.lpfnWndProc   =  RdOnlyProc;
//        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.hbrBackground =  CreateSolidBrush (RdonlyBkCol);
        wc.lpszClassName =  "Readonly";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  IconProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "WIcon";
        RegisterClass(&wc);

        wc.style         =  CS_OWNDC;
        wc.lpfnWndProc   =  ColButtonProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "ColButton";
        RegisterClass(&wc);


        wc.style         =  CS_OWNDC;
        wc.lpfnWndProc   =  RdOnlyProc;
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "LReadonly";
        RegisterClass(&wc);
}

int ScrollEfields (form *frm, int pos)
/**
Spalten um frm->frmstart Felder nach links scrollen.
**/
{
         int i;
         int x;

         x = frm->mask[pos].pos[1];
         for (i = 0; i < frm->frmstart; i ++)
         {
                    x -= (frm->mask[i + 1].pos[1] -
                          frm->mask[i].pos[1]);
         }
         return (x);
}

static void SetListZab (form *frm)
/**
Dialoggroesse fuer Elternfenster ermitteln.
**/
{
         int i;
         int rows;

         listzab = 1;

         for (i = frm->frmstart; i < frm->fieldanz; i ++)
         {
                     if (frm->mask[i].rows == 0)
                     {
                                     rows = 1;
                     }
                     else
                     {
                                     rows = frm->mask[i].rows;
                     }
                     if (frm->mask[i].pos[0] + rows >
                                               listzab)
                     {
                                     listzab = frm->mask[i].pos[0] +
                                               rows;
                     }
          }
          return;
}

void GetFrmRectEx (form *frm, mfont *Font, RECT *rect)
/**
Dialoggroesse fuer Elternfenster in speziellem Font ermitteln.
**/
{
         int i;
         TEXTMETRIC tm;
         HFONT hFont;
         HDC hdc;

         if (Font)
         {
                   spezfont (Font);
                   hFont = SetWindowFont (hMainWindow);
                   hdc = GetDC (hMainWindow);
                   SelectObject (hdc, hFont);
                   GetTextMetrics (hdc, &tm);
                   DeleteObject (hFont);
                   ReleaseDC (hMainWindow, hdc);
                   stdfont ();
         }

         rect->left = rect->top = 9999;
         rect->bottom = rect->right = 0;

         for (i = frm->frmstart; i < frm->fieldanz; i ++)
         {
                     if (frm->mask[i].pos[0] < rect->top)
                     {
                              rect->top =  frm->mask[i].pos[0];
                     }
                     if (frm->mask[i].pos[1] < rect->left)
                     {
                              rect->left =  frm->mask[i].pos[1];
                     }
                     if (frm->mask[i].pos[0] + frm->mask[i].rows >
                                               rect->bottom)
                     {
                               rect->bottom =  frm->mask[i].pos[0] +
                                              frm->mask[i].rows + 1;
                     }
                     if (frm->mask[i].pos[1] + frm->mask[i].length >
                                               rect->right)
                     {
                              rect->right =  frm->mask[i].pos[1] +
                                             frm->mask[i].length;
                     }
          }
          rect->bottom ++;
          if (Font)
          {
                 rect->left   *= tm.tmAveCharWidth;
                 rect->top    *= (tm.tmHeight + tm.tmHeight / 2);
                 rect->right  = rect->right * tm.tmAveCharWidth + 20;
                 rect->bottom *= (tm.tmHeight + tm.tmHeight / 2);
          }
          return;
}


void GetFrmRect (form *frm, RECT *rect)
/**
Dialoggroesse fuer Elternfenster ermitteln.
**/
{
         int i;

         rect->left = rect->top = 9999;
         rect->bottom = rect->right = 0;

         for (i = frm->frmstart; i < frm->fieldanz; i ++)
         {
                     if (frm->mask[i].pos[0] < rect->top)
                     {
                              rect->top =  frm->mask[i].pos[0];
                     }
                     if (frm->mask[i].pos[1] < rect->left)
                     {
                              rect->left =  frm->mask[i].pos[1];
                     }
                     if (frm->mask[i].pos[0] + frm->mask[i].rows >
                                               rect->bottom)
                     {
                               rect->bottom =  frm->mask[i].pos[0] +
                                              frm->mask[i].rows + 1;
                     }
                     if (frm->mask[i].pos[1] + frm->mask[i].length >
                                               rect->right)
                     {
                              rect->right =  frm->mask[i].pos[1] +
                                             frm->mask[i].length;
                     }
          }
          rect->bottom ++;
          if (frm->font == NULL)
          {
                 rect->left   *= tm.tmAveCharWidth;
                 rect->top    *= (tm.tmHeight + tm.tmHeight / 2);
                 rect->right  *= tm.tmAveCharWidth;
                 rect->bottom *= (tm.tmHeight + tm.tmHeight / 2);
          }
          return;
}


void GetFrmSize (form *frm, int *cx, int *cy)
/**
Dialogfroesse fuer Elternfenster ermitteln.
**/
{
         int z, s;
         int x;
         int i;
         int rows;

         z = s = 0;

         for (i = frm->frmstart; i < frm->fieldanz; i ++)
         {
                     if (frm->mask[i].rows == 0)
                     {
                                     rows = 1;
                     }
                     else
                     {
                                     rows = frm->mask[i].rows;
                     }
                     if (frm->mask[i].pos[0] + rows > z)
                     {
                              z =  frm->mask[i].pos[0] +
                                   rows;
                     }
                     // x = ScrollEfields (frm, i);
                     x = frm->mask[i].pos[1];
                     if (x + frm->mask[i].length > s)
                     {
                              s =  x + frm->mask[i].length;
                     }
          }
          if (ListEWindow)
          {
                   *cx = s + 1;
                   *cy = z;
          }
          else
          {
                   *cx = s + 2;
                   *cy = z + 2;
          }
          return;
}

int fillcombobox (form *frm, int ps)
/**
Combobox fuellen.
**/
{
        int i;
        int j;
        int id;
        combofield *cbbox;

        if (frm->cbfield == NULL) return 0;

        cbbox = frm->cbfield;

        for (i = 0; cbbox[i].cbanz; i ++)
        {
                if (ps == cbbox[i].fieldid) break;
        }

        if (cbbox[i].cbanz == 0) return 0;

        for (j = 0, id = 0; j < cbbox[i].cbanz; j ++, id += cbbox[i].cbdim)
        {
               SendMessage (frm->mask[ps].feldid, CB_ADDSTRING, 0, 
                           (LPARAM) &cbbox[i].cbwerte[id]);
        }
        return 0;
}


void SetFirstCtrText (void)
/**
Text in Edit neu Setzen.
**/
{
        if (current_form->mask[currentfield].length == 0)
        {
                current_form->mask[currentfield].length =
                       strlen (current_form->mask[currentfield].feld);
        }
        ToFormat (buffer, &current_form->mask[currentfield]);
        strcpy (current_form->mask[currentfield].feld, buffer);
        SetWindowText (current_form->mask [currentfield].feldid,
                       current_form->mask [currentfield].feld);
}


void SetAktCtrText (void)
/**
Text in Edit neu Setzen.
**/
{
        char dfield [256];

        if (current_form->mask[currentfield].length == 0)
        {
                current_form->mask[currentfield].length =
                       strlen (current_form->mask[currentfield].feld);
        }
        GetWindowText (current_form->mask [currentfield].feldid,
                       current_form->mask [currentfield].feld,
                       current_form->mask [currentfield].length);
        ToFormat (dfield, &current_form->mask[currentfield]);
        strcpy (current_form->mask[currentfield].feld, dfield);
        SetWindowText (current_form->mask [currentfield].feldid,
                       current_form->mask [currentfield].feld);
}

int testkeys (void)
/**
Funtkionstasten testen.
**/
{
             switch (syskey)
             {
                 case KEYESC :
                         break_enter ();
                         return TRUE;
                 case KEY2 :
                         if (fkt_aktiv [2] != (int (*) ()) 0)
                         {
                                     (*fkt_aktiv[2])();
                                      return TRUE;
                         }
                         break_enter ();
                         return TRUE;
                 case KEY3 :
                         if (fkt_aktiv [3] != (int (*) ()) 0)
                         {
                                     (*fkt_aktiv[3])();
                                      return TRUE;
                         }
                         break_enter ();
                         return TRUE;
                 case KEY4 :
                         if (fkt_aktiv [4] != (int (*) ()) 0)
                                 (*fkt_aktiv[4])();
                          return TRUE;
                 case KEY5 :
                         if (fkt_aktiv [5] != (int (*) ()) 0)
                                 (*fkt_aktiv[5])();
                         return TRUE;
                 case KEY6 :
                         if (fkt_aktiv [6] != (int (*) ()) 0)
                                 (*fkt_aktiv[6])();
                          return TRUE;
                 case KEY7 :
                         if (fkt_aktiv [7] != (int (*) ()) 0)
                                 (*fkt_aktiv[7])();
                          return TRUE;
                 case KEY8 :
                         if (fkt_aktiv [8] != (int (*) ()) 0)
                                 (*fkt_aktiv[8])();
                          return TRUE;
                 case KEY9 :
                         if (fkt_aktiv [9] != (int (*) ()) 0)
                                 (*fkt_aktiv[9])();
                          return TRUE;
                 case KEY10 :
                         if (fkt_aktiv [10] != (int (*) ()) 0)
                                 (*fkt_aktiv[10])();
                          return TRUE;
                 case KEY11 :
                         if (fkt_aktiv [11] != (int (*) ()) 0)
                                 (*fkt_aktiv[11])();
                          return TRUE;
                 case KEY12 :
                         if (fkt_aktiv [12] != (int (*) ()) 0)
                                 (*fkt_aktiv[12])();
                          return TRUE;
         }
         return (0);
}

void CloseControl (form *frm, int id)
/**
Controllelemente schliessen.
**/
{

         DelHwFont (frm->mask[id].feldid);
         if (frm->font && frm->font->hFont)
         {
                 frm->font->hFont = NULL;
         }
         DestroyWindow (frm->mask[id].feldid);
         frm->mask[id].feldid = 0;
}

void InitControls (form *frm)
/**
Controllelemente schliessen.
**/
{
         int i;

         for (i = 0; i < frm->fieldanz; i ++)
         {
                 if (frm->mask[i].feldid)
                 {
                          DelHwFont (frm->mask[i].feldid);
                          frm->mask[i].feldid = 0;
                 }
         }

         if (frm->font && frm->font->hFont)
         {
                 frm->font->hFont = NULL;
         }
}


void CloseControls (form *frm)
/**
Controllelemente schliessen.
**/
{
         int i;

         for (i = 0; i < frm->fieldanz; i ++)
         {
                 if (frm->mask[i].feldid)
                 {
                          DelHwFont (frm->mask[i].feldid);
                          DestroyWindow (frm->mask[i].feldid);
                          frm->mask[i].feldid = 0;
                 }
         }
         if (frm->font && frm->font->hFont)
         {
                 frm->font->hFont = NULL;
         }
}

void to_frmstack (form *frm)
/**
form auf Stack.
**/
{
         int i;

         for (i = 0; i < fmptr; i ++)
         {
                if (frm == frmstack[i]) return;
         }
         frmstack[i] = frm;
         if (fmptr < FMMAX - 1) fmptr ++;
}

void from_frmstack (form *frm)
/**
form von Stack entfernen.
**/
{
         int i;

         return;
         for (i = 0; i < fmptr; i ++)
         {
                if (frm == frmstack[i]) break;
         }
         if (i == fmptr) return;
         for (;i < fmptr; i ++)
         {
                frmstack[i] = frmstack[i + 1];
         }
         frmstack[i] = NULL;
         fmptr --;
}

void PressBorder (HDC hdc, RECT *rect)
/**
Rahmen um Fenster.
**/
{ 
         HPEN hPenBlack;
         HPEN hPenWhite;
         HPEN hPenGray;

         hPenBlack = CreatePen (PS_SOLID, 1, BLACKCOL);
         hPenWhite = CreatePen (PS_SOLID, 1, WHITECOL);
         hPenGray  = CreatePen (PS_SOLID, 1, GRAYCOL);

         // SelectObject (hdc, hPenBlack);
         SelectObject (hdc, hPenGray);

         MoveToEx (hdc, 0, 0, NULL);
         LineTo (hdc, rect->right, 0);
         MoveToEx (hdc, 0, 0, NULL);
         LineTo (hdc, 0, rect->bottom);

         // SelectObject (hdc, hPenGray);
         SelectObject (hdc, hPenBlack);

         MoveToEx (hdc, 1, 1, NULL);
         LineTo (hdc, rect->right - 1, 1);
         MoveToEx (hdc, 1, 1, NULL);
         LineTo (hdc, 1, rect->bottom - 1);

         SelectObject (hdc, hPenWhite);
         MoveToEx (hdc, 0, rect->bottom - 1, NULL);
         LineTo (hdc, rect->right, rect->bottom - 1);
         MoveToEx (hdc, rect->right - 1, 0, NULL);
         LineTo (hdc, rect->right - 1, rect->bottom - 1);

         DeleteObject (hPenWhite);
         DeleteObject (hPenBlack);
         DeleteObject (hPenGray);
}


void UnPressBorder (HDC hdc, RECT *rect)
/**
Rahmen um Fenster.
**/
{ 
         HPEN hPenBlack;
         HPEN hPenWhite;
         HPEN hPenGray;

         hPenBlack = CreatePen (PS_SOLID, 1, BLACKCOL);
         hPenWhite = CreatePen (PS_SOLID, 1, WHITECOL);
         hPenGray  = CreatePen (PS_SOLID, 1, GRAYCOL);

         SelectObject (hdc, hPenGray);

         MoveToEx (hdc, rect->right- 2, 1, NULL);
         LineTo (hdc, rect->right - 2, rect->bottom - 2);
         MoveToEx (hdc, 1, rect->bottom -2, NULL);
         LineTo (hdc, rect->right, rect->bottom - 2);


         SelectObject (hdc, hPenBlack);

         MoveToEx (hdc, rect->right - 1, 0, NULL);
         LineTo (hdc, rect->right - 1, rect->bottom);
         MoveToEx (hdc, 0, rect->bottom - 1, NULL);
         LineTo (hdc, rect->right, rect->bottom - 1);

         SelectObject (hdc, hPenWhite);

         MoveToEx (hdc, 0, 0, NULL);
         LineTo (hdc, 0, rect->bottom -1);
         MoveToEx (hdc, 0, 0, NULL);
         LineTo (hdc, rect->right, 0);

         DeleteObject (hPenWhite);
         DeleteObject (hPenBlack);
         DeleteObject (hPenGray);
}


void CreateControls (HWND hDlg, HFONT hFont, int cid, field *mask,
                     int x, int y, int cx, int cy, int enter)
/**
Kontrollelement erzeugen.
**/
{
        char *editclass;
        DWORD editstyle;
        static DWORD clip = WS_CLIPCHILDREN;
//        static DWORD clip = 0;
        ColButton *CoB;
        char ColClass [20];
		HBITMAP *BmButton;
		char *Btext;

        if (mask->feldid != 0)
        {
               if (mask->attribut == EDIT ||
                   mask->attribut == BUTTON)
               {
                      SendMessage (mask->feldid,
                                   WM_SETFONT, (WPARAM) hFont, 0);
               }
               return;
        }

        if (enter)
        {
              editclass = "Edit";
        }
        else
        {
             editclass =  "Readonly";
        }

        if (mask->attribut & BUTTON)
        {
  /* Button                       */

              if (mask->attribut & DEFBUTTON)
              {
                            editstyle = BS_DEFPUSHBUTTON;
              }
              else if (mask->attribut & CHECKBUTTON)
              {
                            editstyle = BS_AUTOCHECKBOX;
              }
              else
              {
                            editstyle = BS_PUSHBUTTON;
              }
		      if (mask->picture[0] != 'B')
			  {
				        Btext = mask->feld;  
			  }
              else
			  {
				        Btext = "";
				        BmButton = (HBITMAP *) mask->feld;  
						editstyle |= BS_BITMAP;
			  }
              mask->feldid = CreateWindow (
                                        "BUTTON",
                                        mask->feld,
                                        WS_CHILD | WS_VISIBLE | clip |
                                        editstyle,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        (HMENU) mask->BuId,
                                        hMainInst,
                                        NULL);
               SetHwFont (mask->feldid, hFont);
               SendMessage (mask->feldid,
                         WM_SETFONT, (WPARAM) hFont, 0);
               if (mask->attribut & DISABLED)
               {
                        EnableWindow (mask->feldid, FALSE);
               }
               else
               {
                        EnableWindow (mask->feldid, TRUE);
               }
			   if (mask->picture[0] == 'B')
			   {
				        BmButton = (HBITMAP *) mask->feld;  
                        SendMessage (mask->feldid,
                                BM_SETIMAGE, (WPARAM) IMAGE_BITMAP, 
								(LPARAM) *BmButton);
               }
        }
        else if (mask->attribut & ICON)
        {
              editstyle = 0;
              if (mask->attribut & BORDERED) editstyle = WS_DLGFRAME;
              mask->feldid = CreateWindow (
                                        "WIcon",
                                        "",
                                        WS_CHILD | WS_VISIBLE |
                                        BS_OWNERDRAW | clip |
                                        editstyle,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        0,
                                        hMainInst,
                                        NULL);
               SendMessage (mask->feldid,
                         WM_SETFONT, (WPARAM) hFont, 0);
        }
        else if (mask->attribut & COLBUTTON)
        {
              CoB = (ColButton *)  mask->feld;
              RegisterColWin (ColClass, CoB->BkColor, ColButtonProc);
              editstyle = 0;
              if (CoB->icon)
              {
                         cx += 2;
                         cy += 2;
                         editstyle = WS_DLGFRAME
                         ;
              }

              mask->feldid = CreateWindow (
                                        // "ColButton",
                                        ColClass,
                                        "",
                                        WS_CHILD | WS_VISIBLE |
                                        BS_OWNERDRAW | clip | editstyle,
                                        // WS_DLGFRAME,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        0,
                                        hMainInst,
                                        NULL);
               SendMessage (mask->feldid,
                         WM_SETFONT, (WPARAM) hFont, 0);
        }
        else if (mask->attribut & DISPLAYONLY)
        {
  /* Text                     */
               if (ListEWindow)
               {
                         mask->feldid = CreateWindow (
                                       "LStatic",
                                        "",
                                        WS_CHILD | WS_VISIBLE |clip,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        NULL,
                                        hMainInst,
                                        NULL);
               }
               else if (current_form->font)
               {
                         RegisterColWin (ColClass,
                                         current_form->font->FontBkColor,
                                         StaticProc);
                         mask->feldid = CreateWindow (
                                        ColClass,
                                        "",
                                        WS_CHILD | WS_VISIBLE | clip,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        NULL,
                                        hMainInst,
                                        NULL);
               }
               else
               {
                         mask->feldid = CreateWindow (
                                       "SStatic",
                                        "",
                                        WS_CHILD | WS_VISIBLE | clip,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        NULL,
                                        hMainInst,
                                        NULL);
               }
               SendMessage (mask->feldid,
                           WM_SETFONT, (WPARAM) hFont, 0);
        }
        else if (mask->attribut & LISTBOX)
        {
  /* Listbox                   */
        }
        else if (mask->attribut & REMOVED)
        {
  /* Entfernt                    */
        }
        else if (mask->attribut & COMBOBOX)
        {
  /* Combobox                   */
                editstyle = WS_CHILD | WS_VISIBLE;
                if (mask->attribut & UPSHIFT)                             
                                        editstyle |= ES_UPPERCASE;
                if (mask->attribut & PASSWORD)                             
                                        editstyle |= ES_PASSWORD;
                mask->feldid = CreateWindowEx (
                                         WS_EX_CLIENTEDGE, 
                                        "Combobox",
                                        "",       
                                        editstyle | WS_BORDER | CBS_DROPDOWN |
                                            WS_VSCROLL | CBS_DISABLENOSCROLL |
                                            clip,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        (HMENU) cid,
                                        hMainInst,
                                        NULL);
                 SetHwFont (mask->feldid, hFont);
                 SendMessage (mask->feldid,
                                         WM_SETFONT, (WPARAM) hFont, 0);
        }
        else if (mask->attribut & READONLY)
        {
  /* Edit Readonly                             */

                 if (ListEWindow)
                 {
                                     mask->feldid = CreateWindow (
                                        "LReadonly",
                                        "",       
                                        WS_CHILD | WS_VISIBLE | clip,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        NULL,
                                        hMainInst,
                                        NULL);
                 }
                 else if (current_form->font)
                 {
                         RegisterColWin  (ColClass,
                                          current_form->font->FontBkColor,
                                          RdColProc);
                         mask->feldid = CreateWindowEx (
                                          WS_EX_CLIENTEDGE, 
                                          ColClass,
                                          "",       
                                          WS_CHILD | WS_VISIBLE | clip,
                                          x, y,
                                          cx, cy,
                                          hDlg,
                                          NULL,
                                          hMainInst,
                                          NULL);
                  }
                  else
                  {
                         RegisterColWin  (ColClass,
                                          RdonlyBkCol,
                                          RdOnlyProc);
                         mask->feldid = CreateWindowEx (
                                        WS_EX_CLIENTEDGE, 
                                          ColClass,
//                                        "Readonly",
                                        "",       
                                        WS_CHILD | WS_VISIBLE | clip,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        NULL,
                                        hMainInst,
                                        NULL);
                    }
                    
                    SendMessage (mask->feldid,
                                         WM_SETFONT, (WPARAM) hFont, 0);
        }
        else
        {
  /* Edit                     */
                    editstyle = WS_CHILD | WS_VISIBLE;
                    if (mask->attribut & UPSHIFT)
                    {
                                        editstyle |= ES_UPPERCASE;
                    }
                    if (mask->attribut & PASSWORD)
                    {
                                        editstyle |= ES_PASSWORD;
                    }

                    if (mask->picture[0] == '%')
                    {
                                        editstyle |= ES_MULTILINE | ES_RIGHT;
                    }

                    if (ListEWindow)
                    {
                                    mask->feldid = CreateWindow (
                                        editclass,
                                        "",       
                                        editstyle | clip,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        (HMENU) cid,
                                        hMainInst,
                                        NULL);
                      }
                      else
                      {
                                    mask->feldid = CreateWindowEx (
                                        WS_EX_CLIENTEDGE, 
                                        editclass,
                                        "",       
                                        editstyle | clip,
                                        x, y,
                                        cx, cy,
                                        hDlg,
                                        (HMENU) cid,
                                        hMainInst,
                                        NULL);
                      }
                      SetHwFont (mask->feldid, hFont);
                      SendMessage (mask->feldid,
                                            WM_SETFONT, (WPARAM) hFont, 0);
                      SendMessage (mask->feldid,
                                            EM_SETMARGINS, 
                                            (WPARAM) EC_LEFTMARGIN,
                                            MAKELONG (0,0));
                      SendMessage (mask->feldid,
                                            EM_SETMARGINS, 
                                            (WPARAM) EC_RIGHTMARGIN,
                                            MAKELONG (0,0));
         }
}

void ShowControl (field *feld)
{

        if (feld->attribut & DISPLAYONLY)
        {
  /* Text                     */
                            InvalidateRect (feld->feldid, NULL, TRUE);
                            UpdateWindow(feld->feldid);

         }
         else if (feld->attribut & BUTTON)
         {
  /* Button                    */

                            InvalidateRect (feld->feldid, NULL, TRUE);
                            UpdateWindow(feld->feldid);
          }
         else if (feld->attribut & ICON)
         {
  /* ICON                    */

                            InvalidateRect (feld->feldid, NULL, TRUE);
                            UpdateWindow(feld->feldid);
          }
         else if (feld->attribut & COLBUTTON)
         {
  /* COLBUTTON                    */
                            InvalidateRect (feld->feldid, NULL, TRUE);
                            UpdateWindow(feld->feldid);
          }
          else if (feld->attribut & READONLY)
          { 
  /* Edit Readonly               */

                             InvalidateRect (feld->feldid, NULL, TRUE);
                             UpdateWindow(feld->feldid);
          }
}

void get_form_values (form *frm)
{
        int i;
        int savecurrent;

        savecurrent = currentfield;
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
			if (frm->mask[i].feldid != NULL)
			{
				GetWindowText (frm->mask[i].feldid, frm->mask[i].feld, frm->mask[i].length);
			}
		}
}

void ShowControls (form *frm, int enter)
{
        int i;
        int savecurrent;

        savecurrent = currentfield;
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {

                 if (frm->mask[i].attribut & DISPLAYONLY)
                 {
  /* Text                     */
                            InvalidateRect (frm->mask[i].feldid, NULL, TRUE);
                            UpdateWindow(frm->mask[i].feldid);

                 }
                 else if (frm->mask[i].attribut & BUTTON)
                 {
  /* Button                    */

                            InvalidateRect (frm->mask[i].feldid, NULL, TRUE);
                            UpdateWindow(frm->mask[i].feldid);
                 }
                 else if (frm->mask[i].attribut & ICON)
                 {

                            InvalidateRect (frm->mask[i].feldid, NULL, TRUE);
                            UpdateWindow(frm->mask[i].feldid);
                 }
/*
                 else if (frm->mask[i].attribut & COLBUTTON)
                 {

                            InvalidateRect (frm->mask[i].feldid, NULL, TRUE);
                            UpdateWindow(frm->mask[i].feldid);
                 }
*/

                 else if (frm->mask[i].attribut & LISTBOX)
                 {
  /* Listbox                   */
                 }
                 else if (frm->mask[i].attribut & REMOVED)
                 {
  /* Entfernt                   */
                 }
                 else if (frm->mask[i].attribut & COMBOBOX)
                 {
  /* Combobox                     */

                             fillcombobox (frm, i);
                             currentfield = i;    
                             SetFirstCtrText ();
                             InvalidateRect (frm->mask[i].feldid, NULL, TRUE);
                             UpdateWindow(frm->mask[i].feldid);
                 }
                 else if (frm->mask[i].attribut & READONLY)
                 {
  /* Edit Readonly               */

                             InvalidateRect (frm->mask[i].feldid, NULL, TRUE);
                             UpdateWindow(frm->mask[i].feldid);
                 }
                 else
                 {
  /* Edit                     */

                             currentfield = i;    
                             SetFirstCtrText ();
                             InvalidateRect (frm->mask[i].feldid, NULL, TRUE);
                             UpdateWindow(frm->mask[i].feldid);
                 }
        }
        currentfield = savecurrent;
}

void create_field (HWND hWnd, field *feld, int zeile, int spalte)
/**
Structur feld creieren. Combobox nicht moeglich. Nur ueber create_form.
**/
{
        int cx, cy;
        int x, y;
        int xchar, ychar;
        HDC hdc;
        HWND hDlg;
        HFONT hFont, oldFont;
        int savecurrent;
        SIZE size;

        AktZeile = zeile;
        AktSpalte = spalte;

        savecurrent = currentfield;
        RegisterDialog ();

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        AktivWindow = hDlg = hWnd;


        if (current_form->font && current_form->mask [0].feldid == NULL)
        {
                if (current_form->font->hFont)
                {
                              DeleteObject (current_form->font->hFont);
                }
                aktFont = 0;
                spezfont (current_form->font);
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                GetTextExtentPoint32 (hdc, "X", 1, &size);
                tm.tmAveCharWidth = size.cx;
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                current_form->font->hFont = hFont;
        }
        else if (current_form->font && current_form->font->hFont == NULL)
        {
                aktFont = 0;
                spezfont (current_form->font);
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                GetTextExtentPoint32 (hdc, "X", 1, &size);
                tm.tmAveCharWidth = size.cx;
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                current_form->font->hFont = hFont;
        }
        else if (current_form->font)
        {
                DeleteObject (current_form->font->hFont);
                aktFont = 0;
                spezfont (current_form->font);
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                GetTextExtentPoint32 (hdc, "X", 1, &size);
                tm.tmAveCharWidth = size.cx;
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                current_form->font->hFont = hFont;

        }
        else if (stdHfont == NULL)
        {
                stdfont ();
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                GetTextExtentPoint32 (hdc, "X", 1, &size);
                tm.tmAveCharWidth = size.cx;
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                stdHfont = hFont;
        }
        else
        {

                aktFont = stdHfont;
                hdc = GetDC (AktivWindow);
                oldFont = SelectObject (hdc,aktFont);
                GetTextMetrics (hdc, &tm);
                GetTextExtentPoint32 (hdc, "X", 1, &size);
                tm.tmAveCharWidth = size.cx;
                ReleaseDC (AktivWindow, hdc);
                hFont = aktFont;
        }


        hFont = aktFont;

// Controlfenster oeffnen

        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight;

        x = feld->pos[1] + spalte;
        x = x * xchar;
        y = ((feld->pos[0]) + zeile) * ychar;
        if (ListEWindow == 0)
        {
                 y = y + y / 3;
        }
        if (feld->length > 0)
        {
                 cx = feld->length * xchar;
        }
        else
        {          
                 feld->length = strlen (feld->feld);
                 cx = strlen (feld->feld) * xchar;
        }
        if (feld->rows)
        {
                 cy = ychar * feld->rows;
        }
        else
        {
                 cy = ychar + ychar / 3;
        }
        CreateControls (hDlg, hFont, 0, feld, x, y, cx, cy, 0);

        ShowControl (feld);
}

int PosX (field *feld)
/**
X-Position ermitteln.
**/
{
        RECT rect;
        int textlen;
        int textpilen;
        int x;

        x = feld->pos[1];
        if (x != -1) return (x);

        GetClientRect (AktivWindow, &rect);
        if (feld->length)
        {
                    textpilen = feld->length;
        }
        else
        {
                    textlen = strlen (feld->feld);
                    textpilen = textlen * tm.tmAveCharWidth;
        }
        x = max (0, (rect.right - textpilen) / 2);
        return (x);
}
        
int PosY (field *feld)
/**
Y-Position ermitteln.
**/
{
        RECT rect;
        int textpilen;
        int y;

        y = feld->pos[0];
        if (y != -1) return (y);

        if (feld->rows)
        {
                    textpilen = feld->rows;
        }
        else
        {
                    textpilen = tm.tmHeight;
        }

        GetClientRect (AktivWindow, &rect);
        y = max (0, (rect.bottom - textpilen) / 2);
        return (y);
}

int WidthX (field *feld)
/**
Breite ermitteln.
**/
{
        int textlen;
        int cx;
        SIZE size;

        if (feld->length == 0)
        {
                    textlen = strlen (feld->feld);
                    GetTextExtentPoint (GetDC (AktivWindow),
                                        feld->feld, textlen, &size);
                    cx = size.cx;
        }
        else
        {
                    cx = feld->length;
        }
        return (cx);
}

int HeightY (field *feld)
/**
Hoehe ermitteln.
**/
{
        int textrows;
        TEXTMETRIC tm;
        int cy;

        GetTextMetrics (GetDC (AktivWindow), &tm);
        if (feld->rows == 0)
        {
                    textrows = 1;
        } 
        else
        {
                    return (feld->rows);
        }
        cy = textrows * tm.tmHeight;
        return (cy);
}

void create_form (HWND hWnd, form *frm, int zeile, int spalte, int enter)
/**
Dialog ueber Structur form.
**/
{
        int i;
        int cx, cy;
        int x, y;
        int xchar, ychar;
        HDC hdc;
        HWND hDlg;
        HFONT hFont, oldFont;
        int savecurrent;
        HBRUSH oldbrush;
        HBRUSH newbrush;
        RECT rect;
        SIZE size;

        AktZeile = zeile;
        AktSpalte = spalte;

        SetEnvFont ();
        current_form = frm;
        savecurrent = currentfield;
        RegisterDialog ();

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        AktivWindow = hDlg = hWnd;

        if (current_form->font && current_form->mask [0].feldid == NULL)
        {
                if (current_form->font->hFont)
                {
                              DeleteObject (current_form->font->hFont);
                }
                aktFont = 0;
                spezfont (current_form->font);
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                GetTextExtentPoint32 (hdc, "X", 1, &size);
                tm.tmAveCharWidth = size.cx;
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                current_form->font->hFont = hFont;
        }
        else if (current_form->font && current_form->font->hFont == NULL)
        {
                aktFont = 0;
                spezfont (current_form->font);
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                GetTextExtentPoint32 (hdc, "X", 1, &size);
                tm.tmAveCharWidth = size.cx;
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                current_form->font->hFont = hFont;
        }
        else if (current_form->font)
        {
                DeleteObject (current_form->font->hFont);
                aktFont = 0;
                spezfont (current_form->font);
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                GetTextExtentPoint32 (hdc, "X", 1, &size);
                tm.tmAveCharWidth = size.cx;
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                current_form->font->hFont = hFont;

        }
        else if (stdHfont == NULL)
        {
                stdfont ();
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                GetTextExtentPoint32 (hdc, "X", 1, &size);
                tm.tmAveCharWidth = size.cx;
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                stdHfont = hFont;
        }
        else
        {
        /*
                DeleteObject (stdHfont);
                stdfont ();
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                stdHfont = hFont;
       */

                aktFont = stdHfont;
                hdc = GetDC (AktivWindow);
                oldFont = SelectObject (hdc,aktFont);
                GetTextMetrics (hdc, &tm);
                GetTextExtentPoint32 (hdc, "X", 1, &size);
                tm.tmAveCharWidth = size.cx;
                ReleaseDC (AktivWindow, hdc);
                hFont = aktFont;
        }

        hFont = aktFont;
// Controlfenster oeffnen

        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight;

        if (spalte == -1)
        {
                  GetFrmSize (frm, &cx, &cy);
                  GetClientRect (AktivWindow, &rect);
                  cx *= xchar;
                  cx = (rect.right - cx) / 2;
                  spalte = cx / xchar;
        }

        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {

                 if (frm->font && frm->font->PosMode)
                 {
                      x = PosX (&frm->mask[i]); 
                      y = PosY (&frm->mask[i]); 
                      cx = WidthX (&frm->mask[i]); 
                      cy = HeightY (&frm->mask[i]);
                 }
                 else
                 {
                      x = ScrollEfields (frm, i) + spalte;
                      x = x * xchar;

                      y = ((frm->mask[i].pos[0]) + zeile) * ychar;
                      if (ListEWindow == 0 &&
                      frm->mask[i].rows == 0)
                      {
                           y = y + y / 3;
                      }
                      if (frm->mask[i].length > 0)
                      {
                            cx = frm->mask[i].length * xchar;
                      }
                      else
                      {
                            frm->mask[i].length = strlen (frm->mask[i].feld);
                            cx = strlen (frm->mask[i].feld) * xchar;
                      }
                      if (frm->mask[i].rows)
                      {
                            cy = ychar * frm->mask[i].rows;
                      }
                      else
                      {
                            cy = ychar + ychar / 3;
                      }
                 }
                 CreateControls (hDlg, hFont, i, &frm->mask[i],
                                 x, y, cx, cy, enter);
                 if (frm->mask[i].attribut & COLBUTTON);
                 else if (frm->font && frm->font->FontBkColor != -1)
                 {
                          newbrush = CreateSolidBrush (
                                          frm->font->FontBkColor);
                          oldbrush = (HGDIOBJ) SetClassLong (
                                       frm->mask[i].feldid,
                                      GCL_HBRBACKGROUND,
                                      (long) newbrush);

                 }

        }

        ShowControls (frm, enter);
        if (enter == 1)
        {
                   currentfield = savecurrent;
                   enter_break = currentbutton = 0;
                   if (setfield == 0) currentfield = current_form->frmstart;
                   AktivDialog = hDlg;
                   SetFocus (current_form->mask[currentfield].feldid);
                   DlgFirstD (hDlg);
                   denter = 1;
        }
        else if (enter == 2)
        {
                   denter = 1;
        }
}

void create_dialog (HWND hWnd, form *frm, int zeile, int spalte)
/**
Dialog ueber Struktur form.
**/
{
        int i;
        int cx, cy;
        int x, y;
        int xchar, ychar;
        HDC hdc;
        HWND hDlg;
        HFONT hFont, oldFont;
        int savecurrent;

        savecurrent = currentfield;
        RegisterDialog ();
        GetFrmSize (frm, &cx, &cy);
        AktivWindow = hWnd;
        hMainInst = (HANDLE) GetWindowLong (AktivWindow, GWL_HINSTANCE);

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        aktFont = 0;
        if (stdHfont == NULL)
        {
                stdfont ();
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                stdHfont = hFont;
        }
        else
        {
                aktFont = stdHfont;
        }

        hFont = aktFont;
        x = spalte * tm.tmAveCharWidth;
        y = (zeile + sz) * tm.tmHeight;
        cx *= tm.tmAveCharWidth;
        cy *= tm.tmHeight;

// Dialogfenster oeffnen

        if (ListEWindow)
        {
       
                  hDlg = CreateWindow (
                            "hLDialogbox",
                            "",
                            WS_VISIBLE | WS_BORDER | WS_CHILD,
                            x, y,
                            cx, cy,
                            AktivWindow,
                            NULL,
                            hMainInst,
                            NULL);
        }
        else
        {
                  hDlg = CreateWindow (
                            "hDialogbox",
                            "",
                            WS_VISIBLE | WS_DLGFRAME | WS_CHILD,
                            x, y,
                            cx, cy,
                            AktivWindow,
                            NULL,
                            hMainInst,
                            NULL);
        }

// Controlfenster oeffnen

        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight;

        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {

                 x = ScrollEfields (frm, i);
                 x = x * xchar;
                 y = (frm->mask[i].pos[0]) * ychar;
                 if (frm->mask[i].length > 0)
                 {
                            cx = frm->mask[i].length * xchar;
                 }
                 else
                 {          frm->mask[i].length = strlen (frm->mask[i].feld);
                            cx = strlen (frm->mask[i].feld) * xchar;
                 }
                 if (frm->mask[i].rows)
                 {
                            cy = ychar * frm->mask[i].rows;
                 }
                 else
                 {
                            cy = ychar + ychar / 2;
                 }
                 CreateControls (hDlg, hFont, i, &frm->mask[i],
                                 x, y, cx, cy, 1);
        }

        ShowControls (frm, 1);
        currentfield = savecurrent;
        enter_break = currentbutton = 0;
        if (setfield == 0) currentfield = current_form->frmstart;
        AktivDialog = hDlg;
        AktivWindow = hDlg;
        SetFocus (current_form->mask[currentfield].feldid);
        DlgFirstD (hDlg);
        denter = 1;
}

void create_button_dialog (HWND hWnd, form *frm, int zeile, int spalte,
                                      int cx, int cy)
/**
Dialog ueber Structur form.
**/
{
        int i;
        int x, y;
        int xchar, ychar;
        HDC hdc;
        HWND hDlg;
        HFONT hFont, oldFont;
        // int savecurrent;

        // savecurrent = currentfield;
        RegisterDialog ();
        AktivWindow = hWnd;
        hMainInst = (HANDLE) GetWindowLong (AktivWindow, GWL_HINSTANCE);

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        if (current_form->font && current_form->mask [0].feldid == NULL)
        {
                if (current_form->font->hFont)
                {
                              DeleteObject (current_form->font->hFont);
                }
                aktFont = 0;
                spezfont (current_form->font);
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                current_form->font->hFont = hFont;
        }
        else if (current_form->font && current_form->font->hFont == NULL)
        {
                aktFont = 0;
                spezfont (current_form->font);
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                current_form->font->hFont = hFont;
        }
        else if (current_form->font)
        {
                DeleteObject (current_form->font->hFont);
                aktFont = 0;
                spezfont (current_form->font);
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                current_form->font->hFont = hFont;

        }
        else if (stdHfont == NULL)
        {
                stdfont ();
                hdc = GetDC (AktivWindow);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
                GetTextMetrics (hdc, &tm);
                ReleaseDC (AktivWindow, hdc);
                aktFont = hFont;
                stdHfont = hFont;
        }
        else
        {
                aktFont = stdHfont;
                hdc = GetDC (AktivWindow);
                oldFont = SelectObject (hdc,aktFont);
                GetTextMetrics (hdc, &tm);
                ReleaseDC (AktivWindow, hdc);
                hFont = aktFont;
        }

        hFont = aktFont;
        x = spalte * tm.tmAveCharWidth;
        y = (zeile + 1) * tm.tmHeight;
        cy += 2;
        cx *= tm.tmAveCharWidth;
        cy *= tm.tmHeight;

// Dialogfenster oeffnen

        hDlg = CreateWindowEx (NULL,
                            "hButtonbox",
                            "",
                            WS_VISIBLE | WS_DLGFRAME | WS_OVERLAPPED |
                            WS_CAPTION | WS_SYSMENU,
                            x, y,
                            cx, cy,
                            hWnd,
                            NULL,
                            hMainInst,
                            NULL);
     // Controlfenster oeffnen
        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight;

        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {

                 x = ScrollEfields (frm, i);
                 x = x * xchar;
                 y = (frm->mask[i].pos[0]) * ychar;
                 if (frm->mask[i].length > 0)
                 {
                            cx = frm->mask[i].length * xchar;
                 }
                 else
                 {          frm->mask[i].length = strlen (frm->mask[i].feld);
                            cx = strlen (frm->mask[i].feld) * xchar;
                 }
                 if (frm->mask[i].rows)
                 {
                            cy = ychar * frm->mask[i].rows;
                 }
                 else
                 {
                            cy = ychar + ychar / 2;
                 }
                 CreateControls (hDlg, hFont, i, &frm->mask[i],
                                 x, y, cx, cy, 1);
        }

        ShowControls (frm, 1);
        enter_break = currentbutton = 0;
        if (setfield == 0) currentfield = current_form->frmstart;
        AktivDialog = hDlg;
        SetFocus (current_form->mask[currentfield].feldid);
        BtoFirst (hDlg);
        denter = 1;
}


void EmSetSel (void)
/**
Text markieren.
**/
{
          if (setsel == 0) return;
          SendMessage (current_form->mask[currentfield].feldid, EM_SETSEL,
                           (WPARAM) 0, 
                           (LPARAM) -1);
//                           MAKELONG (-1, 0));
}

int PlusScrollOk (void)
/**
Testen, ob im Eingabefenster gescrollt werden muss.
**/
{
        RECT rect;
        int x, x1;
        int i;

        GetClientRect (AktivDialog, &rect);
        x = rect.right / tm.tmAveCharWidth;

        GetClientRect (GetParent (AktivDialog), &rect);
        x1 = rect.right / tm.tmAveCharWidth;
        if (x1 > 0 && x1 < x)
        {
                      x = x1;
        }

        for (i = current_form->frmstart; i > 0; i --)
        {
                      x += current_form->mask[i - 1].length;
        }

        if (x < (current_form->mask[currentfield].pos[1] +
                 current_form->mask[currentfield].length))
        {
                        return 1;
        }
        return 0;
}
        

int mouseinformd (form *frm, POINT *mpos)
/**
Test, ob der Mouse-Cursor in einem Eingabefeld steht.
Ohne Dialog
**/
{
           int i;
           int ret;
           POINT mpost;
           RECT wpos;
           int xlen;
           int ylen;
           HWND aktctrl;
           int aktcurrent;

           aktcurrent = currentfield;
           for (i = frm->frmstart;i < frm->fieldanz; i ++)
           {
               if (current_form->mask[i].attribut & DISPLAYONLY ||
                      current_form->mask[i].attribut & READONLY ||
                      current_form->mask[i].attribut & ICON ||
                      current_form->mask[i].attribut & COLBUTTON ||
                      current_form->mask[i].attribut & BUTTON)
               {
                         continue;
               }
               memcpy (&mpost, mpos, sizeof (POINT));
               aktctrl = current_form->mask[i].feldid;
               ScreenToClient (aktctrl, &mpost);
               GetWindowRect (aktctrl, &wpos);
               xlen = wpos.right - wpos.left;
               ylen = wpos.bottom - wpos.top;

               if (mpost.x < 0) continue;
               if (mpost.y < 0) continue;
               if (mpost.x > xlen) continue;
               if (mpost.y > ylen) continue;

               GetWindowText (current_form->mask [currentfield].feldid,
                              current_form->mask [currentfield].feld,
                              current_form->mask [currentfield].length);
               if (current_form->mask[currentfield].after != (int (*) ()) 0)
               {
                   ret = (*current_form->mask[currentfield].after) ();
               }

               currentfield = i;

               if (current_form->frmscroll)
               {
                     if (currentfield >= current_form->frmscroll - 1 &&
                           PlusScrollOk ())
                    {
                          current_form->frmstart += (currentfield -
                                                  current_form->frmscroll + 2);
                          current_form->frmscroll = currentfield + 2;
                          syskey = 1;
                          SetCurrentField (currentfield);
                          selcolor = 0;
                          break_enter ();
                          return TRUE;
                    }
               }
               ret = 0;
               if (current_form->before != (int (*) ()) 0)
               {
                   ret = (*current_form->before) ();
               }

               if (ret == 0)
               {
                    EmSetSel ();
                    SetFocus (current_form->mask[i].feldid);
               }
               else
               {
                     currentfield = aktcurrent;
               }
               return TRUE;
           }
           return (FALSE);
}

int GetFeldSpalten (form *frm)
/**
Anzahl Spalten fuer Form ermitteln.
**/
{
           int spalten;
           int i;

           spalten = 0;
           for (i = 0; i < frm->fieldanz; i ++)
           {
                  if (spalten < frm->mask[i].pos[1])
                  {
                         spalten = frm->mask[i].pos[1];
                  }
           }
           return spalten;
}

int GetFirstRow (form *frm)
/**
1. Zeilenposition fuer Form ermitteln.
**/
{
           int zeile;
           int i;

           zeile = 999;
           for (i = 0; i < frm->fieldanz; i ++)
           {
                  if (zeile > frm->mask[i].pos[0])
                  {
                         zeile = frm->mask[i].pos[0];
                  }
           }
           return zeile;
}


int MaxListPos (form *frm)
/**
Hoechste Listenposition fuer Form ermitteln.
**/
{
           int max;
           int spalten;
           int first;
           int i;

           spalten = max = 0;
           first = GetFirstRow (frm);
           for (i = 0; i < frm->fieldanz; i ++)
           {
                  if (frm->mask[i].pos[0] > first) continue; 
                  if (spalten < frm->mask[i].pos[1] +
                                frm->mask[i].length)
                  {
                         spalten = frm->mask[i].pos[1] +
                                   frm->mask[i].length;
                         max = i;
                  }
           }
           return max;
}


int GetFeldSpalte (int sign)
/**
Spalte fuer Feld ermitteln.
**/
{
           int spalte;
           int first;

           first = GetFirstRow (listform);
           if (menue.menFpos >= MaxListPos (listform))
           {
                        menue.menFpos = MaxListPos (listform);
           }
           else if (menue.menFpos < 0)
           {
                        menue.menFpos = 0;
           }
           if (listform->mask[menue.menFpos].pos[0] != first)
           {
                        menue.menFpos += sign;
                        return (GetFeldSpalte (sign));
           }

           if (menue.menFpos == 0)
           {
                     spalte = 0;
           }
           else
           {
                    spalte = listform->mask[menue.menFpos].pos[1];
           }
           return spalte;
}

int SetFeldSpalte (void)
/**
Spalte auf Feld setzen.
**/
{
           int i;
           int spalte;
           int pos;
           int first;

           first = GetFirstRow (listform);

           spalte = menue.menSpalte;
           pos = spalte;

           for (i = 0; i < current_form->fieldanz - 1; i ++)
           {
                     if (current_form->mask[i].pos[0] > first) continue;
                     if (pos >= current_form->mask[i].pos[1] &&
                         pos < current_form->mask[i + 1].pos[1])
                     {
                                 break;
                     }
           }
           menue.menFpos = i;
           if (menue.menFpos >= MaxListPos (listform))
           {
                        menue.menFpos = MaxListPos (listform);
           }
           if (menue.menFpos == 0)
           {
                     spalte = 0;
           }
           else
           {
                     spalte = listform->mask[menue.menFpos].pos[1];
           }
           return spalte;
}

void BtoFirst (HWND hDlg)
/**
Erstes Feld. Button
**/
{
        HWND aktfocus;
        int ret;
        int i;
        int lauf;

        aktfocus = GetFocus ();
        lauf = 0;
        i = currentfield;
        while (TRUE)
        {
               if (current_form->mask[i].attribut & BUTTON) break;
               if (current_form->mask[i].attribut & DISPLAYONLY ||
                      current_form->mask[i].attribut & ICON ||
                      current_form->mask[i].attribut & REMOVED ||
                      current_form->mask[i].attribut & READONLY)
               {
                      i ++;
                      if (i == current_form->fieldanz)
                      {
                            if (lauf == 0)
                            {
                                   lauf = 1;
                                   i = current_form->frmstart;
                            }
                            else
                            {
                                    return;
                            }
                       }
               }
               else
               {
                      break;
               }
        }
        SetFirstCtrText ();
        currentfield = i;
        if (current_form->mask[currentfield].before != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].before) ();
        }
        EmSetSel ();
        SetFocus (current_form->mask[currentfield].feldid);
        EmSetSel ();
        setfield = 0;
}

void BtoCurrent ()
/**
Aktuellen Button feststellen.
**/
{
        HWND aktfocus;
        int i;

        aktfocus = GetFocus ();

        for (i = current_form->frmstart; i < current_form->fieldanz; i ++)
        {
               if (aktfocus == current_form->mask[i].feldid)
               {
                          break;
               }
        }
        currentfield = i;
}


void BtoUp ()
/**
Keyup gedrueckt.
**/
{
        HWND aktfocus;
        int ret;
        int i;
        int lauf;

        aktfocus = GetFocus ();

        /*

        for (i = current_form->frmstart; i < current_form->fieldanz; i ++)
        {
               if (aktfocus == current_form->mask[i].feldid)
               {
                          break;
               }
        }

        if (i < current_form->fieldanz)
        {
               currentfield = i;
        }
        else
        {
               return;
        }
        */

        SetAktCtrText ();
        setfield = 0;
        if (current_form->mask[currentfield].after != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].after) ();
        }

        if (setfield == 0)
        {
                   currentfield --;
        }

        if (currentfield < 0)
        {
                   currentfield = current_form->fieldanz - 1;
        }


        lauf = 0;
        i = currentfield;
        while (TRUE)
        {
               if (current_form->mask[i].attribut & BUTTON) break;
               if (current_form->mask[i].attribut & DISPLAYONLY ||
                      current_form->mask[i].attribut & ICON ||
                      current_form->mask[i].attribut & REMOVED ||
                      current_form->mask[i].attribut & READONLY)
               {
                      i --;               
                      if (i == currentfield) break;
                      if (i < 0) i = current_form->fieldanz - 1;
                      if (i < 0)
                      {
                            if (lauf == 0)
                            {
                                   lauf = 1;
                                   i = current_form->fieldanz - 1;
                            }
                            else
                            {
                                    return;
                            }
                       }
               }
               else
               {
                      break;
               }
        }
        currentfield = i;
        if (current_form->frmscroll)
        {
              if ((currentfield < current_form->frmstart) &&
                  (current_form->frmstart > 0))
              {
                    current_form->frmscroll -= (current_form->frmstart -
                                                currentfield);
                    current_form->frmstart = currentfield;
                    syskey = 1;
                    SetCurrentField (currentfield);
                    selcolor = 0;
                    break_enter ();
                    return;
              }
        }
        if (currentfield < 0)
        {
                       currentfield = current_form->fieldanz - 1;
        }

        if (current_form->mask[currentfield].before != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].before) ();
        }
        EmSetSel ();
        SetFocus (current_form->mask[currentfield].feldid);
}

void BtoEnter (void)
/**
Enter Taste gedrueckt.
**/
{
        HWND aktfocus;
        int ret;
        int i;
        int lauf;

        aktfocus = GetFocus ();

        GetWindowText (current_form->mask [currentfield].feldid,
                       current_form->mask [currentfield].feld,
                       current_form->mask [currentfield].length);

        setfield = 0; 
        SetAktCtrText ();

        if (current_form->mask[currentfield].after != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].after) ();
                   if (enter_break) return;
        }
        if (setfield == 0)
        {
                   currentfield ++;
        }

        setfield = 0;
        if (currentfield >= current_form->fieldanz)
        {
              if (end_break)
              {
                   break_enter ();
                   return;
              }
              else
              {
                   currentfield = current_form->frmstart;
              }
        }

        lauf = 0;
        i = currentfield;
        while (TRUE)
        {

               if (current_form->mask[i].attribut & BUTTON) break;
               if (current_form->mask[i].attribut & DISPLAYONLY ||
                      current_form->mask[i].attribut & ICON ||
                      current_form->mask[i].attribut & REMOVED ||
                      current_form->mask[i].attribut & READONLY)
               {
                      i ++;
                      if (i == current_form->fieldanz && end_break)
                      {
                             break_enter ();
                             return;
                      }
                      else if (i == current_form->fieldanz)
                      {
                            if (lauf == 0)
                            {
                                   lauf = 1;
                                   i = current_form->frmstart;
                            }
                            else
                            {
                                    return;
                            }
                       }
               }
               else
               {
                      break;
               }
        }
        currentfield = i;
        if (current_form->frmscroll)
        {
              if (currentfield >= current_form->frmscroll - 1 &&
                  PlusScrollOk ())
              {
                    current_form->frmstart += (currentfield -
                                               current_form->frmscroll + 2);
                    current_form->frmscroll = currentfield + 2;
                    syskey = 1;
                    SetCurrentField (currentfield);
                    selcolor = 0;
                    break_enter ();
                    return;
              }
        }
        if (current_form->mask[currentfield].before != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].before) ();
        }
        EmSetSel ();
        SetFocus (current_form->mask[currentfield].feldid);
        setfield = 0;
}



void DlgFirstD (HWND hDlg)
/**
Erstes Feld.
**/
{
        HWND aktfocus;
        int ret;
        int i;
        int lauf;

        if (ButtonTab)
        {
            BtoFirst (hDlg);
            return;
        }
        aktfocus = GetFocus ();
        lauf = 0;
        i = currentfield;
        while (TRUE)
        {
               if (current_form->mask[i].attribut & DISPLAYONLY ||
                      current_form->mask[i].attribut & BUTTON ||
                      current_form->mask[i].attribut & ICON ||
                      current_form->mask[i].attribut & REMOVED ||
                      current_form->mask[i].attribut & READONLY)
               {
                      i ++;
                      if (i == current_form->fieldanz)
                      {
                            if (lauf == 0)
                            {
                                   lauf = 1;
                                   i = current_form->frmstart;
                            }
                            else
                            {
                                    break_enter ();
                                    return;
                            }
                       }
               }
               else
               {
                      break;
               }
        }
        SetFirstCtrText ();
        currentfield = i;
        if (current_form->mask[currentfield].before != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].before) ();
        }
        EmSetSel ();
        SetFocus (current_form->mask[currentfield].feldid);
        EmSetSel ();
        setfield = 0;
}



void DlgUpD ()
/**
Keyup gedrueckt.
**/
{
        HWND aktfocus;
        int ret;
        int i;
        int lauf;

        if (ButtonTab)
        {
            BtoUp ();
            return;
        }
        aktfocus = GetFocus ();

        for (i = current_form->frmstart; i < current_form->fieldanz; i ++)
        {
               if (aktfocus == current_form->mask[i].feldid)
               {
                          break;
               }
        }

        if (i < current_form->fieldanz)
        {
               currentfield = i;
        }
        else
        {
               return;
        }

        SetAktCtrText ();
        setfield = 0;
        if (current_form->mask[currentfield].after != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].after) ();
        }

        if (setfield == 0)
        {
                   currentfield --;
        }


        if (currentfield < 0)
        {
                       currentfield = 0;
        }

        lauf = 0;
        i = currentfield;
        while (TRUE)
        {
               if (current_form->mask[i].attribut & DISPLAYONLY ||
                      current_form->mask[i].attribut & BUTTON ||
                      current_form->mask[i].attribut & ICON ||
                      current_form->mask[i].attribut & REMOVED ||
                      current_form->mask[i].attribut & READONLY)
               {
                      i --;               
                      if (i == currentfield) break;
                      if (i < 0) i = current_form->fieldanz - 1;
                      if (i < 0)
                      {
                            if (lauf == 0)
                            {
                                   lauf = 1;
                                   i = current_form->fieldanz - 1;
                            }
                            else
                            {
                                    return;
                            }
                       }
               }
               else
               {
                      break;
               }
        }
        currentfield = i;
        if (current_form->frmscroll)
        {
              if ((currentfield < current_form->frmstart) &&
                  (current_form->frmstart > 0))
              {
                    current_form->frmscroll -= (current_form->frmstart -
                                                currentfield);
                    current_form->frmstart = currentfield;
                    syskey = 1;
                    SetCurrentField (currentfield);
                    selcolor = 0;
                    break_enter ();
                    return;
              }
        }
        if (currentfield < 0)
        {
                       currentfield = current_form->fieldanz - 1;
        }

        if (current_form->mask[currentfield].before != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].before) ();
        }
        EmSetSel ();
        SetFocus (current_form->mask[currentfield].feldid);
}

void DlgEnterD (void)
/**
Enter Taste gedrueckt.
**/
{
        HWND aktfocus;
        int ret;
        int i;
        int lauf;

        if (ButtonTab)
        {
            BtoEnter ();
            return;
        }
        aktfocus = GetFocus ();

        GetWindowText (current_form->mask [currentfield].feldid,
                       current_form->mask [currentfield].feld,
                       current_form->mask [currentfield].length);
        setfield = 0; 
        SetAktCtrText ();

        if (current_form->mask[currentfield].after != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].after) ();
                   if (enter_break) return;
        }
        if (setfield == 0)
        {
                   currentfield ++;
        }

        setfield = 0;
        if (currentfield >= current_form->fieldanz)
        {
              if (end_break)
              {
                   break_enter ();
                   return;
              }
              else
              {
                   currentfield = current_form->frmstart;
              }
        }

        lauf = 0;
        i = currentfield;
        while (TRUE)
        {

               if (current_form->mask[i].attribut & DISPLAYONLY ||
                      current_form->mask[i].attribut & BUTTON ||
                      current_form->mask[i].attribut & ICON ||
                      current_form->mask[i].attribut & REMOVED ||
                      current_form->mask[i].attribut & READONLY)
               {
                      i ++;
                      if (i == current_form->fieldanz && end_break)
                      {
                             break_enter ();
                             return;
                      }
                      else if (i == current_form->fieldanz)
                      {
                            if (lauf == 0)
                            {
                                   lauf = 1;
                                   i = current_form->frmstart;
                            }
                            else
                            {
                                    return;
                            }
                       }
               }
               else
               {
                      break;
               }
        }
        currentfield = i;
        if (current_form->frmscroll)
        {
              if (currentfield >= current_form->frmscroll - 1 &&
                  PlusScrollOk ())
              {
                    current_form->frmstart += (currentfield -
                                               current_form->frmscroll + 2);
                    current_form->frmscroll = currentfield + 2;
                    syskey = 1;
                    SetCurrentField (currentfield);
                    selcolor = 0;
                    break_enter ();
                    return;
              }
        }
        if (current_form->mask[currentfield].before != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].before) ();
        }
        EmSetSel ();
        SetFocus (current_form->mask[currentfield].feldid);
        setfield = 0;
}


void DlgFkeyD (void)
/**
Enter Taste gedrueckt.
**/
{
        HWND aktfocus;
        int ret;
        int i;
        int lauf;

        GetWindowText (current_form->mask [currentfield].feldid,
              current_form->mask [currentfield].feld,
              current_form->mask [currentfield].length);

        if (current_form->mask[currentfield].after != (int (*) ()) 0)
        {
                   ret = (*current_form->mask[currentfield].after) ();
        }
        else
        {
                    testkeys ();
        }

        aktfocus = GetFocus ();

        for (i = current_form->frmstart; i < current_form->fieldanz; i ++)
        {
               if (aktfocus == current_form->mask[i].feldid)
               {
                          break;
               }
        }

        if (i < current_form->fieldanz)
        {
               currentfield = i;
        }
        else
        {
               setfield = 0;
               return;
        }

        setfield = 0;
        if (currentfield >= current_form->fieldanz)
        {
                   currentfield = current_form->frmstart;
        }

        lauf = 0;
        i = currentfield;
        while (TRUE)
        {

               if (current_form->mask[i].attribut & DISPLAYONLY ||
                      current_form->mask[i].attribut & BUTTON ||
                      current_form->mask[i].attribut & ICON ||
                      current_form->mask[i].attribut & REMOVED ||
                      current_form->mask[i].attribut & READONLY)
               {
                      i ++;               
                      if (i == current_form->fieldanz)
                      {
                            if (lauf == 0)
                            {
                                   lauf = 1;
                                   i = current_form->frmstart;
                            }
                            else
                            {
                                    return;
                            }
                       }
               }
               else
               {
                      break;
               }
        }
        currentfield = i;
        EmSetSel ();
        SetFocus (current_form->mask[currentfield].feldid);
        setfield = 0;
}

int testnumfield (MSG *msg)
/**
Numeriwsches Feld pruefen.
**/
{
       if (current_form->mask[currentfield].picture[0] != '%')
       {
                         return TRUE;
       }
       if (msg->wParam == '.') return TRUE;
       if (msg->wParam == ',') return TRUE;
       if (msg->wParam == '-') return TRUE;
       if (msg->wParam == '+') return TRUE;
       if (msg->wParam == VK_BACK) return TRUE;
       if (msg->wParam < 0x30) return FALSE;
       if (msg->wParam > 0x39) return FALSE;
       return TRUE;
}

int isCombobox (HWND hWnd)
/**
Test, ob Fenster eine Combobox ist.
**/
{
       int i;

       for (i = 0; i < current_form->fieldanz; i ++)
       {
                  if (hWnd == current_form->mask[i].feldid) break;
       }

       if (current_form->mask[i].attribut & COMBOBOX)
       {
                  return TRUE;
       }
       return FALSE;
}

int IsBtoMessage (MSG *msg)
/**
Auf Dialogmeldung testen.
**/
{
       POINT mousepos;
       int i;
       HWND aktfocus;

       syskey = 0;
       aktfocus = GetFocus ();
       for (i = 0; i < current_form->fieldanz; i ++)
       {
             if (current_form->mask[i].feldid == aktfocus)
             {
                       currentfield = i;
                       break;
             }
       }

       SetActiveWindow (AktivDialog);
       if (msg->message == WM_PAINT)
       {
             return FALSE;
       }
    
       if (isCombobox (msg->hwnd))

       {
             return FALSE;
       }

       if (opencombobox)
       {
             return FALSE;
       }

       if (msg->message == WM_BEFORE)
       {
                      if (current_form->before != (int (*) ()) 0)
                      {
                                  (*current_form->before) ();
                      }
                      return TRUE;
       }

       else if (msg->message == WM_KEYDOWN)
       {
             switch (msg->wParam)
             {
                 case VK_ESCAPE :
                         syskey = KEYESC;
                         DlgFkeyD ();
                         break;
                 case VK_F3 :
                         syskey = KEY3;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F4 :
                         syskey = KEY4;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F5 :
                         syskey = KEY5;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F6 :
                         syskey = KEY6;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F7 :
                         syskey = KEY7;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F8 :
                         syskey = KEY8;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F9 :
                         syskey = KEY9;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F10 :
                         syskey = KEY10;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F11 :
                         syskey = KEY11;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F12 :
                         syskey = KEY12;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_DOWN :
                 case VK_RIGHT :
                         syskey = KEYDOWN;
                         BtoEnter ();
                         return TRUE;
                 case VK_UP :
                 case VK_LEFT :
                         syskey = KEYUP;
                         BtoUp ();
                         return TRUE;
                 case VK_RETURN :
                         syskey = KEYCR;
                         SendMessage (AktivDialog, WM_COMMAND, 0l, 0l);
                         return TRUE;
                 case VK_TAB :
                         if (GetKeyState (VK_SHIFT) < 0)
                         {
                              syskey = KEYSTAB; 
                              BtoUp ();
                              return TRUE;
                         }
                         syskey = KEYTAB;
                         BtoEnter ();
                         return TRUE;
              }
       }
       else if (msg->message == WM_USER)
       {
             switch (msg->wParam)
             {
                 case KEYESC :
                         syskey = KEYESC;
                         DlgFkeyD ();
                         break;
                 case KEY3 :
                         syskey = KEY3;
                         DlgFkeyD ();
                         return TRUE;
                 case KEY4 :
                         syskey = KEY4;
                         DlgFkeyD ();
                         return TRUE;
                 case KEY5 :
                         syskey = KEY5;
                         DlgFkeyD ();
                         return TRUE;
             }
       }
       else if (msg->message == WM_SYSKEYDOWN)
       {
             switch (msg->wParam)
             {
                 case VK_F10 :
                         syskey = KEY10;
                          DlgFkeyD ();
                          return TRUE;
             }
        }

        GetCursorPos (&mousepos);

        if (msg->hwnd == AktivDialog);
        else if (IsChild (AktivDialog, msg->hwnd));
        else if (mouseindialogex (msg->hwnd, &mousepos));
        /*
        else
        {
             SetFocus (current_form->mask[currentfield].feldid);
             return TRUE;
        }
        */
        if (msg->message == WM_LBUTTONDOWN)
        {
            if (!mouseindialog (msg->hwnd, &mousepos)) return TRUE;
        }
        else if (msg->message == WM_MOUSEMOVE)
        {
            if (!mouseindialog (msg->hwnd, &mousepos)) return TRUE;
        }
        else if (msg->message == WM_LBUTTONDBLCLK)
        {
            if (!mouseindialog (msg->hwnd, &mousepos)) return TRUE;
        }
        return FALSE;
}

void MouseTest (HWND hwnd, UINT msg)
/**
Meldungen Pruefen.
**/
{
        if (msg == WM_LBUTTONUP)
        {
             MousePressed = FALSE;
        }
        else if (msg == WM_MOUSEMOVE)
        {
             if (MousePressed)
             {
                          TestIconsPressed (hwnd);
             }
        }
        return;
}


int IsMouseMessage (MSG *msg)
/**
Meldungen Pruefen.
**/
{
        if (msg->message == WM_LBUTTONUP)
        {
             MousePressed = FALSE;
        }
        else if (msg->message == WM_MOUSEMOVE)
        {
             if (MousePressed)
             {
                          TestIconsPressed (msg->hwnd);
             }
        }
        return FALSE;
}


int IsDlgMessage (MSG *msg)
/**
Auf Dialogmeldung testen.
**/
{
       POINT mousepos;

       if (IsNoDlgMess && (*IsNoDlgMess) (msg))
       {
             return FALSE;
       }
             
       if (isCombobox (msg->hwnd))
       {
             return FALSE;
       }

       if (opencombobox)
       {
             return FALSE;
       }

       if (msg->message == WM_BEFORE)
       {
                      if (current_form->before != (int (*) ()) 0)
                      {
                                  (*current_form->before) ();
                      }
                      return TRUE;
       }

       else if (msg->message == WM_CHAR)
       {
             if (testnumfield (msg) == FALSE)
             {
                         return TRUE;
             }
       }
       else if (msg->message == WM_KEYDOWN)
       {
             switch (msg->wParam)
             {
                 case VK_ESCAPE :
                         syskey = KEYESC;
                         DlgFkeyD ();
                         break;
                 case VK_F2 :
                         syskey = KEY2;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F3 :
                         syskey = KEY3;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F4 :
                         syskey = KEY4;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F5 :
                         syskey = KEY5;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F6 :
                         syskey = KEY6;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F7 :
                         syskey = KEY7;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F8 :
                         syskey = KEY8;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F9 :
                         syskey = KEY9;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F10 :
                         syskey = KEY10;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F11 :
                         syskey = KEY11;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_F12 :
                         syskey = KEY12;
                         DlgFkeyD ();
                         return TRUE;
                 case VK_RETURN :
                         syskey = KEYCR;
                         DlgEnterD ();
                         return TRUE;
                 case VK_DOWN :
                         syskey = KEYDOWN;
                         DlgEnterD ();
                         return TRUE;
                 case VK_UP :
                         syskey = KEYUP;
                         DlgUpD ();
                         return TRUE;
                 case VK_NEXT :
                         syskey = KEYPGD;
                         DlgEnterD ();
                         return TRUE;
                 case VK_PRIOR :
                         syskey = KEYPGU;
                         DlgEnterD ();
                         return TRUE;
                 case VK_TAB :
                         if (GetKeyState (VK_SHIFT) < 0)
                         {
                              syskey = KEYSTAB; 
                              DlgUpD ();
                              return TRUE;
                         }
                         syskey = KEYTAB;
                         DlgEnterD ();
                         return TRUE;
              }
        }
       else if (msg->message == WM_USER)
       {
             switch (msg->wParam)
             {
                 case KEYESC :
                         syskey = KEYESC;
                         DlgFkeyD ();
                         break;
                 case KEY3 :
                         syskey = KEY3;
                         DlgFkeyD ();
                         return TRUE;
                 case KEY4 :
                         syskey = KEY4;
                         DlgFkeyD ();
                         return TRUE;
                 case KEY5 :
                         syskey = KEY5;
                         DlgFkeyD ();
                         return TRUE;
             }
       }
       else if (msg->message == WM_SYSKEYDOWN)
       {
             switch (msg->wParam)
             {
                 case VK_F10 :
                         syskey = KEY10;
                          DlgFkeyD ();
                          return TRUE;
             }
        }
        else if (msg->message == WM_LBUTTONDOWN)
        {
             
             GetCursorPos (&mousepos);

             if (mouseinformd (current_form, &mousepos)) 
             {
                          return TRUE; 
             }
             return FALSE;
        }
        else if (msg->message == WM_LBUTTONUP)
        {
             MousePressed = FALSE;
        }     
        else if (msg->message == WM_MOUSEMOVE)
        {
             if (MousePressed)
             {
                          TestIconsPressed (msg->hwnd);
             }
        }
        return FALSE;
}

void display_field (HWND hWnd, field *feld, int zeile, int spalte)
/**
Structur feld anzeigen.
**/
{
         create_field (hWnd, feld, zeile, spalte);
}

void display_form (HWND hWnd, form *frm, int zeile, int spalte)
/**
Structur form anzeigen.
**/
{
         HWND aktdlg;
         RECT rect;

         to_frmstack (frm);
         aktdlg = AktivDialog;
         current_form = frm;
         GetFrmRect (frm, &rect);
         InvalidateRect (hWnd, &rect, TRUE);
         create_form (hWnd, frm, zeile, spalte, 0);
         from_frmstack (frm);
}

void create_enter_form (HWND hWnd, form *frm, int zeile, int spalte)
/**
Dialog ueber Structur form.
**/
{

         SetListEWindow (0);    
         AktivDialog = hWnd;
         to_frmstack (frm);
         if (current_form)
         {
                CloseControls (frm);
         }
         current_form = frm;
         if (current_form->before != (int (*) ()) 0)
         {
                    (*current_form->before) ();
         }
         create_form (hWnd, frm, zeile, spalte, 2);

         return;
}


void enter_form (HWND hWnd, form *frm, int zeile, int spalte)
/**
Dialog ueber Structur form.
**/
{
         MSG msg;
         form *saveform;
         HWND aktdlg;
         RECT rect;

         SetListEWindow (0);    
         saveform = current_form;
         aktdlg = AktivDialog;
         AktivDialog = hWnd;
         to_frmstack (frm);
         if (current_form)
         {
                CloseControls (frm);
         }
         current_form = frm;

         GetFrmRect (frm, &rect);
         InvalidateRect (hWnd, &rect, TRUE);

         if (current_form->before != (int (*) ()) 0)
         {
                    (*current_form->before) ();
         }
         enter_break = 0;
		 syskey = 0;
         create_form (hWnd, frm, zeile, spalte, 1);

         while (enter_break == 0)
         {
             if (all_enter_break)
             {
                   syskey = KEYESC;
                   return;
             }
             if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
             {
                  if (msg.message == WM_QUIT)
                  {
                        PostQuitMessage (0);
                        syskey = KEYESC;
                        break;
                  }
                  if (IsDlgMessage (&msg) == 0)
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
             }
             Sleep (5); 
         }
         CloseControls (frm);
         if (current_form->after != (int (*) ()) 0)
         {
                    (*current_form->after) ();
         }
//         display_form (hWnd, frm, zeile, spalte);
         enter_break  = 0;
         AktivDialog = aktdlg;
         current_form = saveform;
         from_frmstack (frm);
         return;
}

int EnterButtonWindowEx (HWND hWnd, form *frm, int zeile, int spalte,
                       int cy, int cx)
/**
Dialog ueber Structur form.
**/
{
         MSG msg;
         int ret;
         form *saveform;
         int savecurrent;
         int choise;
         HWND aktdlg;

         aktdlg = AktivDialog;
         saveform = current_form;
         savecurrent = currentfield;
         to_frmstack (frm);
         current_form = frm;
         SetAktivCbox (frm);
         if (current_form->before != (int (*) ()) 0)
         {
                    (*current_form->before) ();
         }
         create_button_dialog (hWnd, frm, zeile, spalte, cx, cy);

         enter_break = 0;
         while (enter_break == 0)
         {
            if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
            {
                  current_form = frm;
                  ret = IsBtoMessage (&msg);
                  if (ret == 0)
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
             }
             Sleep (5); 
         }
         if (currentbutton == -1 || syskey == KEY5)
         {
                  choise = -1;
         }
         else
         {
                  choise = currentfield;
         }
         CloseControls (frm);
         DestroyWindow (AktivDialog);
         if (current_form->after != (int (*) ()) 0)
         {
                    (*current_form->after) ();
         }
         enter_break  = 0;
         AktivDialog = aktdlg;
         current_form = saveform;
         currentfield = savecurrent;
         from_frmstack (frm);
         return choise;
}

int EnterButtonWindow (HWND hWnd, form *frm, int zeile, int spalte)
/**
Dialog ueber Structur form.
**/
{
         MSG msg;
         int ret;
         form *saveform;
         int savecurrent;
         int choise;
         int cx, cy;
         HWND aktdlg;

         aktdlg = AktivDialog;
         saveform = current_form;
         savecurrent = currentfield;
         to_frmstack (frm);
         current_form = frm;
         if (current_form->before != (int (*) ()) 0)
         {
                    (*current_form->before) ();
         }
         GetFrmSize (frm, &cx, &cy);
         create_button_dialog (hWnd, frm, zeile, spalte, cx, cy);

         enter_break = 0;
         while (enter_break == 0)
         {
            if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
            {
                  ret = IsBtoMessage (&msg);
                  if (ret == 0)
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
             }
             Sleep (5);
         }
         if (currentbutton == -1)
         {
                  choise = -1;
         }
         else
         {
                  choise = currentfield;
         }
         CloseControls (frm);
         DestroyWindow (AktivDialog);
         if (current_form->after != (int (*) ()) 0)
         {
                    (*current_form->after) ();
         }
         enter_break  = 0;
         AktivDialog = aktdlg;
         current_form = saveform;
         currentfield = savecurrent;
         from_frmstack (frm);
         return choise;
}


void EnterListZeile (HWND hWnd, form *frm, int zeile, int spalte)
/**
Dialog ueber Structur form.
**/
{
         MSG msg;
         HWND aktdlg;
         HWND akthwnd;
         HWND AktSendWindow;
         int savecurrent;

         aktdlg = AktivDialog;
         akthwnd = AktivWindow;
         savecurrent = currentfield;
         AktSendWindow = SendWindow;
         to_frmstack (frm);
         current_form = frm;
         if (current_form->before != (int (*) ()) 0)
         {
                    (*current_form->before) ();
         }


         create_dialog (hWnd, frm, zeile, spalte);

         SendWindow = hWnd;
         enter_break = 0;
         while (enter_break == 0)
         {
             if (all_enter_break)
             {
                   syskey = KEYESC;
                   return;
             }
             if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
             {
                  if (msg.message == WM_QUIT)
                  {
                        syskey = KEYESC;
                        break;
                  }
                  if (IsDlgMessage (&msg) == 0)
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
             }
             Sleep (5);
         }
         InitControls (frm);
         DestroyWindow (AktivDialog);
         if (current_form->after != (int (*) ()) 0)
         {
                    (*current_form->after) ();
         }
         enter_break  = 0;
         currentfield = savecurrent;
         AktivDialog = aktdlg;
         AktivWindow = akthwnd;
         SendWindow = AktSendWindow;
         from_frmstack (frm);
         return;
}

int EnterPtListBox (int x, int y, int cx, int cy, char *caption)
{
        int i;

        isChild = 0;
        fillwindow = 1;
        menue.menAnz = menueanz;
        for (i = 0; i < menueanz; i ++)
        {
                 menue.menArr[i] = &menueitems [i * menuedim];
        }
        EnterLBox (x, y, cx, cy, caption);
        for (i = 0; i < menueanz; i ++)
        {
                 menue.menArr[i] = NULL;
        }
        return (0);
}

int EnterPtListBoxBu (int x, int y, int cx, int cy, char *caption)
{
        int i;

        isChild = 0;
        fillwindow = 1;
        menue.menAnz = menueanz;
        for (i = 0; i < menueanz; i ++)
        {
                 menue.menArr[i] = &menueitems [i * menuedim];
        }
        EnterLBoxBu (x, y, cx, cy, caption);
        for (i = 0; i < menueanz; i ++)
        {
                 menue.menArr[i] = NULL;
        }
        return (0);
}

void EnterLBox (int x, int y, int cx, int cy, char *caption)
/**
Listboxdialog ausfuehren.
**/
{

        CreateListWindow (x, y, cx, cy, caption);
        ProcessMessages ();
        return;
}

void EnterLBoxBu (int x, int y, int cx, int cy, char *caption)
/**
Listboxdialog ausfuehren.
**/
{

        VScroll = 0;
        CreateListWindowBu (x, y, cx, cy, caption);
        ProcessMessages ();
        return;
}

void EnterVLBoxBu (int x, int y, int cx, int cy, char *caption,
                   HANDLE hInstance)
/**
Listboxdialog ausfuehren.
**/
{

        VScroll = 1;
        CreateListWindowBu (x, y, cx, cy, caption);
        ProcessMessages ();
        return;
}

void RegisterColWin (char *ColClass, COLORREF col, WNDPROC wproc)
/**
Farbwindow regostrieren.
**/
{
         int i;
         WNDCLASS wc;

         if (wproc == NULL)
         {
                     wproc = WndProc;
         }
         for (i = 0; i < wincolanz; i ++)
         {
                     if ((COLORREF) col == (COLORREF) wincolors [i])
                     {
                               sprintf (ColClass, "win%d", i);
                               if (wproc == colorprocs[i]) break;
                     }
         }
     //     i = wincolanz;
         sprintf (ColClass, "win%d", i);
         wincolors[i]  = col;
         colorprocs[i] = wproc;
         if (i < wincolanz) return;

         if (i == wincolanz && wincolanz < MAXWINCOLS - 1) wincolanz ++;

         wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
         wc.lpfnWndProc   =  wproc;
         wc.cbClsExtra    =  0;
         wc.cbWndExtra    =  0;
         wc.hInstance     =  hMainInst;
         wc.hIcon         =  LoadIcon (hMainInst, "ROSIICON");
         wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
         wc.hbrBackground =  CreateSolidBrush (col);
         wc.lpszMenuName  =  "";
         wc.lpszClassName =  ColClass;

        RegisterClass(&wc);
}

HWND OpenColWindowEx (int rows, int columns, int row, int column,
                      long style, COLORREF col, WNDPROC wproc)
/**
Farbiges Window oeffnen.
**/
{
        HDC hdc;
        int x, y;
        int cx, cy;
        HWND Window;
        HFONT hFont, oldFont;
        char ColClass [20];

        RegisterColWin (ColClass, col, wproc);
        SetEnvFont ();
        if (AktivWindow == NULL)
        {
                   AktivWindow = GetActiveWindow ();
        }
        hMainInst = (HANDLE) GetWindowLong (AktivWindow, GWL_HINSTANCE);

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        hdc = GetDC (AktivWindow);
        stdfont ();
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (SelectObject (hdc, oldFont));
        ReleaseDC (AktivWindow, hdc);

        DeleteObject (hFont);

        x   =  tm.tmAveCharWidth * column;
        y   =  tm.tmHeight * row;
        cx  =  tm.tmAveCharWidth * columns;
        cy  =  tm.tmHeight * rows;
        y = y + y / 3;
        cy = cy + cy / 3;

        x  += pxp;
        y  += pyp;
        cx += pcxp;
        cy += pcyp;

        Window  = CreateWindowEx (
                                 WS_EX_CLIENTEDGE, 
                                 ColClass,
                                 "",
                                 WS_VISIBLE | WS_CHILD | style,
                                 x, y,
                                 cx, cy,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);

        ShowWindow (Window, SW_SHOW);
        UpdateWindow (Window);

        return (Window);
}


HWND OpenColWindow (int rows, int columns, int row, int column,
                    long style, COLORREF col, WNDPROC wproc)
/**
Farbiges Window oeffnen.
**/
{
        HDC hdc;
        int x, y;
        int cx, cy;
        HWND Window;
        HFONT hFont, oldFont;
        char ColClass [20];

        RegisterColWin (ColClass, col, wproc);
        SetEnvFont ();
        if (AktivWindow == NULL)
        {
                   AktivWindow = GetActiveWindow ();
        }
        hMainInst = (HANDLE) GetWindowLong (AktivWindow, GWL_HINSTANCE);

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        hdc = GetDC (AktivWindow);
        stdfont ();
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (SelectObject (hdc, oldFont));
        ReleaseDC (AktivWindow, hdc);
        DeleteObject (hFont);

        x   =  tm.tmAveCharWidth * column;
        y   =  tm.tmHeight * row;
        cx  =  tm.tmAveCharWidth * columns;
        cy  =  tm.tmHeight * rows;
        y = y + y / 3;
        cy = cy + cy / 3;

        x  += pxp;
        y  += pyp;
        cx += pcxp;
        cy += pcyp;


        Window  = CreateWindow (ColClass,
                                 "",
                                 WS_VISIBLE | WS_CHILD | style,
                                 x, y,
                                 cx, cy,
                                 AktivWindow,
                                 NULL,
                                 hMainInst,
                                 NULL);
        ShowWindow (Window, SW_SHOW);
        UpdateWindow (Window);

        return (Window);
}


char TabText[256];

HWND OpenTabControl (int rows, int columns, int row, int column, char *text,
                     HANDLE hInstance)
/**
Window oeffnen.
**/
{
        HDC hdc;
        int x, y;
        int cx, cy;
        HWND Window;
		HWND WindowC;
        HFONT hFont, oldFont;
        TC_ITEM tie;
		int i, anz;
		RECT rect;
		int tabanz;
        SIZE size;

        SetEnvFont ();
        stdfont ();
        if (AktivWindow == NULL)
        {
                   AktivWindow = GetActiveWindow ();
        }

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        hdc = GetDC (AktivWindow);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        DeleteObject (SelectObject (hdc, oldFont));
        ReleaseDC (AktivWindow, hdc);
        DeleteObject (hFont);

        x   =  tm.tmAveCharWidth * column;
        y   =  tm.tmHeight * row;
        cx  =  tm.tmAveCharWidth * columns;
        cy  =  tm.tmHeight * rows;
        y = y + y / 3;
        cy = cy + cy / 3;

       Window  = CreateWindowEx (
                                 WinBorderEx, 
								 WC_TABCONTROL,
                                 "",
								 WinBorder,
                                 x, y,
                                 cx, cy,
                                 AktivWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
        ShowWindow (Window, SW_SHOW);
        UpdateWindow (Window);

		stdHfont = CreateStdFont (Window);
        hFont = stdHfont;
        SendMessage (Window, WM_SETFONT, (WPARAM) hFont, (LPARAM) NULL);
        tie.mask = TCIF_TEXT | TCIF_IMAGE; 
        tie.iImage = -1; 
        tie.pszText = TabText; 
		anz = zsplit (text, ';');
		for (i = 1; i <=anz; i ++)
		{
	          strcpy (TabText, zwort[i]);
              TabCtrl_InsertItem(Window, i - 1, &tie);
		}

        tabanz = TabCtrl_GetRowCount (Window);

        WindowC  = CreateWindowEx (
                                 0, 
                                 "hStdWindow",
                                 "",
                                 WS_CHILD | WS_VISIBLE,
                                 0, tabanz * 25,
                                 cx - 5, cy - tabanz * 30,
                                 Window,
                                 NULL,
                                 hInstance,
                                 NULL);

        return (WindowC);
}


HWND OpenWindowEx (int rows, int columns, int row, int column, long style,
                   HANDLE hInstance)
/**
Window oeffnen.
**/
{
        HDC hdc;
        int x, y;
        int cx, cy;
        HWND Window;
        HFONT hFont, oldFont;
        SIZE size;

        SetEnvFont ();
        stdfont ();
        if (AktivWindow == NULL)
        {
                   AktivWindow = GetActiveWindow ();
        }

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        hdc = GetDC (AktivWindow);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        DeleteObject (SelectObject (hdc, oldFont));
        ReleaseDC (AktivWindow, hdc);
        DeleteObject (hFont);

        x   =  tm.tmAveCharWidth * column;
        y   =  tm.tmHeight * row;
        cx  =  tm.tmAveCharWidth * columns;
        cy  =  tm.tmHeight * rows;
        y = y + y / 3;
        cy = cy + cy / 3;

       Window  = CreateWindowEx (
                                 WinBorderEx, 
                                 hStdWindow,
                                 "",
								 WinBorder,
								 x, y,
                                 cx, cy,
                                 AktivWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
        ShowWindow (Window, SW_SHOW);
        UpdateWindow (Window);

         return (Window);
}


void WindowMove (HWND hWnd, int rows, int columns, int row, int column)
/**
Window bewegen.
**/
{
        HDC hdc;
        int x, y;
        int cx, cy;
        HWND Window;
        HFONT hFont, oldFont;
        SIZE size;

        SetEnvFont ();
        stdfont ();
        if (AktivWindow == NULL)
        {
                   AktivWindow = GetActiveWindow ();
        }

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        hdc = GetDC (AktivWindow);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        DeleteObject (SelectObject (hdc, oldFont));
        ReleaseDC (AktivWindow, hdc);
        DeleteObject (hFont);

        x   =  tm.tmAveCharWidth * column;
        y   =  tm.tmHeight * row;
        cx  =  tm.tmAveCharWidth * columns;
        cy  =  tm.tmHeight * rows;
        y = y + y / 3;
        cy = cy + cy / 3;
		MoveWindow (hWnd, x, y, cx, cy, TRUE);
}


HWND OpenWindow (int rows, int columns, int row, int column, int isChild,
                 HANDLE hInstance)
/**
Window oeffnen.
**/
{
        HDC hdc;
        int x, y;
        int cx, cy;
        HWND Window;
        HFONT hFont, oldFont;
        SIZE size;

        SetEnvFont ();
        stdfont ();
        if (AktivWindow == NULL)
        {
                   AktivWindow = GetActiveWindow ();
        }

// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        hdc = GetDC (AktivWindow);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        DeleteObject (SelectObject (hdc, oldFont));
        ReleaseDC (AktivWindow, hdc);
        DeleteObject (hFont);

        x   =  tm.tmAveCharWidth * column;
        y   =  tm.tmHeight * row;
        cx  =  tm.tmAveCharWidth * columns;
        cy  =  tm.tmHeight * rows;
        y = y + y / 3;
        cy = cy + cy / 3;

        if (isChild)
        {
                     Window  = CreateWindow ("hStdWindow",
                                 "",
                                 WS_VISIBLE | WS_CHILD | WinBorder,
                                 x, y,
                                 cx, cy,
                                 AktivWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
         }
         else if (hMainWindow)
         {
                     Window = CreateWindow ("hStdWindow",
                                 ModulName,
                                 WS_DLGFRAME | WS_CAPTION | WS_SYSMENU |
                                 WS_MINIMIZEBOX,
 				                 x, y, 
				                 cx, cy,
				                 NULL,
                                 NULL,
                                 hInstance,
                                 NULL);
        }
        else
        {
                     Window = CreateWindow ("hStdWindow",
                                 ModulName,
                                 WS_DLGFRAME | WS_CAPTION | WS_SYSMENU |
                                 WS_MINIMIZEBOX,
 				                 CW_USEDEFAULT,
				                 CW_USEDEFAULT,
				                 CW_USEDEFAULT,
				                 CW_USEDEFAULT,
                                 NULL,
                                 NULL,
                                 hInstance,
                                 NULL);
        }
        if (hMainWindow)
		{	
             ShowWindow (Window, SW_SHOW);
             UpdateWindow (Window);
		}
        return (Window);
}


HWND OpenQuery (HWND hWnd, int rows, int columns, int row, int column, HANDLE hInstance)
/**
Window fuer Query-Eingabe oeffnen.
**/
{
        HDC hdc;
        int x, y;
        int cx, cy;
        RECT rect;
        HWND Window;
        HFONT hFont, oldFont;
        SIZE size;

        if (AktivWindow == NULL)
        {
                   AktivWindow = GetActiveWindow ();
        }


// Eltern-Fenster auf benutzten Font setzen und TextMetrix ermitteln.
// Danach wieder auf alten Font zuruecksetzen.

        hdc = GetDC (AktivWindow);
        GetTextMetrics (hdc, &tm);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        DeleteObject (SelectObject (hdc, oldFont));
        ReleaseDC (AktivWindow, hdc);
        DeleteObject (hFont);

        GetWindowRect (hWnd, &rect);

        x   =  rect.left + tm.tmAveCharWidth * column;
        y   =  tm.tmHeight * row;
        cx  =  tm.tmAveCharWidth * columns;
        cy  =  tm.tmHeight * rows;
        y   =  rect.top + tm.tmHeight + tm.tmHeight / 2
               + y + y / 3;
        cy  = cy + cy / 3;

        Window  = CreateWindow ("hStdWindow",
                                 "",
                                 WS_VISIBLE | WS_POPUP,
                                 x, y,
                                 cx, cy,
                                 AktivWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
        ShowWindow (Window, SW_SHOW);
        UpdateWindow (Window);

        return (Window);
}


int InsertListUb (char *wert)
/**
Zeile in Window einfuegen. 
**/
{
         SendMessage (lbox, LB_TITLE, 0, 
                            (LPARAM) (char *) wert);
         return 0;
}

int ResetList (void)
/**
Zeile in Window einfuegen. 
**/
{
         SendMessage (lbox, LB_RESETCONTENT, 0, 0l);
         return 0;
}


int InsertListRow (char *wert)
/**
Zeile in Window einfuegen. 
**/
{
	     MSG msg;

         SendMessage (lbox, LB_INSERTSTRING, -1, 
                            (LPARAM) (char *) wert);
         if (!VScroll)
         {
              clipped (wert);
              menue.menArr[menue.menAnz] =
                       (char *) GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                              strlen (wert) + 1);
              strcpy (menue.menArr[menue.menAnz], wert);
              menue.menAnz ++;
         }

         if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
         {
			              if (msg.message == WM_PAINT ||
							  msg.message == WM_VSCROLL ||
							  msg.message == WM_HSCROLL)
						  {
                                    TranslateMessage(&msg);
                                    DispatchMessage(&msg);
						  }
         }
         return 0;
}

int UpdateListRow (char *wert, int idx)
/**
Zeile in Window einfuegen. 
**/
{
	     MSG msg;

		 if (idx > menue.menAnz) idx = menue.menAnz; 
         SendMessage (lbox, LB_INSERTSTRING, idx, 
                            (LPARAM) (char *) wert);

         if (!VScroll)
         {
              clipped (wert);
              menue.menArr[menue.menAnz] =
                       (char *) GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                              strlen (wert) + 1);
              strcpy (menue.menArr[menue.menAnz], wert);
			  if (idx == menue.menAnz) menue.menAnz ++;
         }
         if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
         {
			              if (msg.message == WM_PAINT ||
							  msg.message == WM_VSCROLL ||
							  msg.message == WM_HSCROLL)
						  {
                                    TranslateMessage(&msg);
                                    DispatchMessage(&msg);
						  }
         }
         return 0;
}

HWND OpenListWindowEx (HWND hWnd, int rows, int columns, int row, int column, BOOL vscr)
/**
Window fuer Dialog oder Listerfassung oeffnen.
**/
{
        VScroll = vscr;
        isChild = 1;
        listzab = 1;
        NoCloseList = 0;
        CreateListWindow (hWnd, column, row, columns, rows, NULL);
        return (hListBox);
}

HWND OpenListWindow (int rows, int columns, int row, int column, BOOL vscr)
/**
Window fuer Dialog oder Listerfassung oeffnen.
**/
{
        VScroll = vscr;
        isChild = 0;
        listzab = 1;
        NoCloseList = 0;
        CreateListWindow (column, row, columns, rows, NULL);
        return (hListBox);
}

HWND OpenSListWindow (int rows, int columns, int row, int column, BOOL ubcols)
/**
Anzeigefenster oeffnen.
**/
{
        fillwindow = 0;
        menue.menAnz = 0;
        menue.menTAnz = 0;
        isChild = 0;
        VScroll = 1;
        FeldScroll = 1;
        hlines = 0;
        vlines = 1;
        sz = 0;
        UbMultiRows = ubcols;
        NoCloseList = 0;
        CreateSListWindow (column, row, columns, rows, (char *) 0);
        UbMultiRows = 0;
        return 0;
}


HWND OpenListWindowBuEx (HWND hWnd, int rows,
                       int columns, int row, int column, BOOL vscr)
/**
Window fuer Dialog oder Listerfassung oeffnen.
**/
{
        VScroll = vscr;
        isChild = 1;
        listzab = 1;
        NoCloseList = 0;
        CreateListWindowBuEx (hWnd, column, row, columns, rows, NULL);
        return (hListBox);
}


HWND OpenListWindowBu (int rows, int columns, int row, int column, BOOL vscr)
/**
Window fuer Dialog oder Listerfassung oeffnen.
**/
{
        VScroll = vscr;
        isChild = 0;
        listzab = 1;
        NoCloseList = 0;
        CreateListWindowBu (column, row, columns, rows, NULL);
        return (hListBox);
}

HWND OpenListWindowBuC (int rows, int columns, int row, int column, BOOL vscr, char *caption)
/**
Window fuer Dialog oder Listerfassung oeffnen.
**/
{
        VScroll = vscr;
        isChild = 0;
        listzab = 1;
        NoCloseList = 0;
        CreateListWindowBu (column, row, columns, rows, caption);
        return (hListBox);
}

int EnterQueryListBox (void)
/**
Rosi-Schnittstelle
**/
{
        int taste;
        int i;
        int savecolor;
        int savelistenter;
        int savesz;
        BOOL ScrollSave;

        savesz = sz;
        ScrollSave = FeldScroll;
        savelistenter = listenter;
        listenter = 0;
        FeldScroll = 0;
        savecolor = selcolor;
        selcolor = 1;
        SetFocus (lbox);
        SendMessage (lbox, LB_SETCURSEL, 0, 0L);
        ProcesshWndMessages (hListBox);
        taste = menueidx;
        for (i = 0; i < menueanz; i ++)
        {
                     menue.menArr[i] = NULL;
                     menue.menArr1[i] = NULL;
                     menue.menArr2[i] = NULL;
        }
        menue.menAnz = 0;
        selcolor = savecolor;
        listenter = savelistenter;
        FeldScroll = ScrollSave;
        sz = savesz;
        return taste;
}

HWND OpenListWindowEnEx (int x, int y, int cx, int cy)
/**
Window fuer Dialog oder Listerfassung oeffnen.
**/
{
        VScroll = 1;
        isChild = 0;
        NoCloseList = 1;
        return (CreateListWindowEnEx (x, y, cx, cy));
}


HWND OpenListWindowEn (int rows, int columns, int row, int column)
/**
Window fuer Dialog oder Listerfassung oeffnen.
**/
{
        VScroll = isChild = 1;
        NoCloseList = 1;
        return (CreateListWindowEn (column, row, columns, rows));
}

void ElistVl (form* frm)
/**
Vertikale Linien festlegen.
**/
{
        char dfield [256];
        int i;
        int pos;

        memset (buffer, ' ', 0x1000);
        if (BeforeList != (void (*) ()) 0)
        {
                         (*BeforeList) ();
        }
        for (i = 0; i < frm->fieldanz; i ++)
        {
                if (frm->mask[i].pos[0] == 0)
                {
                          pos = frm->mask[i].pos[1];
                          ToFormat (dfield, &frm->mask[i]); 
                          memcpy (&buffer[pos], dfield, strlen (dfield));
                }
        }
        for (i = 0x1000 - 2; i > 0; i --)
        {
                if (buffer [i] > ' ')
                {
                          buffer [i + 1] = 0;
                          break;
                }
        }
        SendMessage (lbox, LB_VPOS, 0, (LPARAM) (char *) buffer);
        return;
}

void ElistUb (form* frm)
/**
Vertikale Linien festlegen.
**/
{
        char dfield [256];
        int i;
        int pos;

        memset (buffer, ' ', 0x1000);
        for (i = 0; i < frm->fieldanz; i ++)
        {
                if (frm->mask[i].pos[0] == 0)
                {
                          pos = frm->mask[i].pos[1];
                          ToFormat (dfield, &frm->mask[i]); 
                          memcpy (&buffer[pos], dfield, strlen (dfield));
                }
        }
        for (i = 0x1000 - 2; i > 0; i --)
        {
                if (buffer [i] > ' ')
                {
                          buffer [i + 1] = 0;
                          break;
                }
        }
        SendMessage (lbox, LB_TITLE, 0, (LPARAM) (char *) buffer);
        return;
}

void InsertElistRow (form *frm, int idx)
/**
Eine Zeile formatiert in die Liste anhaengen.
**/
{
        char dfield [256];
        int i;
        int pos;
        int zeile;
        int select;

        select = menue.menSelect;
        for (zeile = 0; zeile < listzab; zeile ++)
        {
           AktMenZeile = zeile;
           menue.menSelect = idx;
           if (BeforeList != (void (*) ()) 0)
           {
                        (*BeforeList) ();
           }
           memset (buffer, ' ', 0x1000);
           for (i = 0; i < frm->fieldanz; i ++)
           {
                if (frm->mask[i].pos[0] == zeile)
                {
                          pos = frm->mask[i].pos[1];
                          ToFormat (dfield, &frm->mask[i]); 
                          memcpy (&buffer[pos], dfield, strlen (dfield));
                }
           }
           for (i = 0x1000 - 2; i > 0; i --)
           {
                if (buffer [i] > ' ')
                {
                          buffer [i + 1] = 0;
                          break;
                }
           }
           SendMessage (lbox, LB_INSERTSTRING, idx,  
                                 (LPARAM) (char *) buffer);
        }
        menue.menSelect = select;
        return;
}

void ShowNewElist (char *tabstruct, int fanz, int structdim)
/**
Liste formatiert anzeigen.
**/
{
         int i;
         int pos;
         char *structsave;


         structsave = (char *) GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                              listdim);
         // memcpy (structsave, &tabstruct[aktsel * listdim], listdim);

         listanz = fanz;
         for (i = 0, pos = 0; i < fanz; i ++, pos += structdim)
         {
                  memcpy (liststruct, &tabstruct[pos], structdim);
                  InsertElistRow (current_form, i);
         }
         menue.menAnz = listanz;

         while (menue.menAnz < menue.menZeile)
         {
                  menue.menZeile --;
         }

         if (menue.menSelect > menue.menAnz - 1)
         {
                   menue.menSelect = menue.menAnz - 1;
         }

         selcolor = 1;
         memcpy (liststruct, &tabstruct[aktsel * listdim], listdim);
         GlobalFree (structsave);
         InvalidateRect (lbox, NULL, TRUE);
         UpdateWindow (lbox);
         return;
}


void ShowElist (char *tabstruct, int fanz, char *formstruct,
                int structdim, form* frm)
/**
Liste formatiert anzeigen.
**/
{
         int i;
         int pos;
         form *savefrm;

         to_frmstack (frm);
         SetListZab (frm);
         menue.menTAnz = 0;
         savefrm = current_form;
         current_form = frm;
         for (i = 0, pos = 0; i < fanz; i ++, pos += structdim)
         {
                  memcpy (formstruct, &tabstruct[pos], structdim);
                  InsertElistRow (frm, i);
         }
         SendMessage (lbox, LB_SETCURSEL, 0, 0L);
         SetFocus (lbox);
         current_form = savefrm;
         from_frmstack (frm);
         return;
}

void InsertListLine (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
        int i, j;
        int len;
        int pos;
        RECT rect;

        for (i = listanz; i > menue.menSelect; i --)
        {
                  memcpy (&listtab[i * listdim],
                          &listtab[(i - 1) * listdim],
                          listdim);
        }

        for (j = 0; j < listform->fieldanz; j ++)
        {
                  len = strlen (listform->mask[j].feld);
                  memset (listform->mask[j].feld, ' ', len);
        }
        memcpy (&listtab[i * listdim], liststruct, listdim);
        listanz ++;

        for (i = 0, pos = 0; i < listanz; i ++, pos += listdim)
        {
                  memcpy (liststruct, &listtab[pos], listdim);
                  InsertElistRow (listform, i);
        }
        SetUpdReg (&rect, listanz); 
        InvalidateRect (lbox, NULL, TRUE);
}

void AppendListLine (int zplus)
/**
Eine Zeile in Liste anfuegen.
**/
{
        int i, j;
        int len;
        int pos;
        RECT rect;

        if (listanz) menue.menSelect += zplus;
        if ((menue.menSelect - menue.menZeile) * listzab >
                              menue.menWzeilen - 1 - sz)
        {
               menue.menZeile ++;
               SetScrollPos (lbox, SB_VERT,
                             menue.menZeile, 
                             TRUE);
        }
        for (i = listanz; i > menue.menSelect; i --)
        {
                  memcpy (&listtab[i * listdim],
                          &listtab[(i - 1) * listdim],
                          listdim);
        }

        if (FillEmptyRow != (void (*) ()) 0)
        {
                  (*FillEmptyRow) ();
        }
        else
        {
                  for (j = 0; j < listform->fieldanz; j ++)
                  {
                           len = strlen (listform->mask[j].feld);
                           memset (listform->mask[j].feld, ' ', len);
                  }
        }
        memcpy (&listtab[i * listdim], liststruct, listdim);
        listanz ++;
        menue.menAnz ++;

        for (i = 0, pos = 0; i < listanz; i ++, pos += listdim)
        {
                  memcpy (liststruct, &listtab[pos], listdim);
                  InsertElistRow (listform, i);
        }
        SetUpdReg (&rect, listanz); 
        InvalidateRect (lbox, NULL, TRUE);
}
                             
                             
void DeleteListLine (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
        int i, j;
        int pos;
        RECT rect;

        for (i = menue.menSelect; i < listanz - 1; i ++)
        {
                  memcpy (&listtab[i * listdim],
                          &listtab[(i + 1) * listdim],
                          listdim);
        }

        for (j = 0; j < listform->fieldanz; j ++)
        {
                  memset (listform->mask[j].feld, ' ',
                             listform->mask[j].length);
                  listform->mask[j].feld[listform->mask[j].length] = (char) 0;
        }
        memcpy (&listtab[i * listdim], liststruct, listdim);
        if (listanz > 0) listanz --;

        for (i = 0, pos = 0; i < listanz; i ++, pos += listdim)
        {
                  memcpy (liststruct, &listtab[pos], listdim);
                  InsertElistRow (listform, i);
        }
        menue.menAnz --;
        if (menue.menSelect >= menue.menAnz)
        {
                  menue.menSelect = menue.menAnz - 1;
        }
        pos = menue.menSelect;
        memcpy (liststruct, &listtab[pos], listdim);
        SetUpdReg (&rect, listanz); 
        InvalidateRect (lbox, NULL, TRUE);
}

void DisplayLZeile (void)
/**
Aktuelle Listezeile anzeigen.
**/
{
         display_form (lbox, listform, (menue.menSelect -  menue.menZeile)
                                                           * listzab, 0);
}

void EnterListRows ()
/**
Listzeilen eingeben.
**/
{
       int scrollstart;
       int app_plus;

       to_frmstack (listform);
       current_form = listform;
       scrollstart = current_form->frmscroll;
       syskey = KEYCR;
       break_end ();
       selcolor = 0;
       setfield = 0;
       current_form->frmstart = 0;
       while (TRUE)
       {
                  nolistdel = 1;
                  aktsel = menue.menSelect;
                  memcpy (liststruct,
                          &listtab [aktsel * listdim],
                          listdim);
                  EnterListZeile (lbox,
                              listform,
                              (menue.menSelect -
                              menue.menZeile) * listzab, 0);
                  if (listins == 2)
                  {
                              if (menue.menAnz - 1 == menue.menSelect)
                              {
                                           app_plus = 1;
                              }
                              else
                              {
                                           app_plus = 0;
                              }
                  }
                  nolistdel = 0;
                  if (listins == 0)
                  {
                          memcpy (&listtab [aktsel * listdim],
                                  liststruct,
                                  listdim);
                          InsertElistRow (listform, aktsel);
                  }
                  else
                  {
                          memcpy (&listtab [aktsel * listdim],
                                  liststruct,
                                  listdim);
					  }

                  if (syskey == KEY5 || syskey == KEYESC)
                  {
                          if (listins)
                          {
                                  listins = 0;
                          }
                          break;
                  }

                  if (syskey == 1) continue;
                  if (syskey == -1) continue;

                  setfield = 0;
                  current_form->frmstart = 0;

                  current_form->frmscroll = scrollstart;
                  InsertElistRow (current_form, aktsel);

                  if (syskey == KEYUP)
                  {
                              listins = 0;
                              SendMessage (lbox, WM_KEYDOWN,
                                           (WPARAM) VK_UP, 0l);
                  }
                  else if (syskey == KEYDOWN &&
                           menue.menSelect - menue.menZeile <
                           menue.menAnz)
                  {
                              SendMessage (lbox, WM_KEYDOWN,
                                           (WPARAM)  VK_DOWN, 0l);
                  }
                  else if (syskey == KEYCR &&
                           menue.menSelect - menue.menZeile <
                           menue.menAnz)
                  {
                              SendMessage (lbox, WM_KEYDOWN,
                                           (WPARAM)  VK_DOWN, 0l);
                  }
                  else if (syskey == KEYPGD)
                  {
                              listins = 0;
                              SendMessage (lbox, WM_KEYDOWN,
                                           (WPARAM)  VK_NEXT, 0l);
                  }
                  else if (syskey == KEYPGU)
                  {
                              listins = 0;
                              SendMessage (lbox, WM_KEYDOWN,
                                           (WPARAM)  VK_PRIOR, 0l);
                  }
                  if (listins == 1)
                  {
                              InsertListLine ();
                  }
                  if (listins == 2)
                  {
                              AppendListLine (app_plus);
                  }
      }
      listins = 0;
      current_form->frmscroll = scrollstart;
      selcolor = 1;
      from_frmstack (listform);
      return;
}

void EnterElist (HWND hWnd,
                 char *tabstruct, int fanz, char *formstruct,
                 int structdim, form* frm)
/**
Liste erfassen.
**/
{
         form *savefrm;
         BOOL ScrollSave;
         char *slisttab;
         int slistanz;
         char *sliststruct;
         int slistdim;
         form *slistform;

         slisttab    = listtab;
         slistanz    = listanz;
         sliststruct = liststruct;
         slistdim    = listdim;
         slistform   = listform;

         ScrollSave = FeldScroll;
         FeldScroll = TRUE;
         savefrm = current_form;
         SetListZab (frm);
         listtab = tabstruct;
         listanz = fanz;
         liststruct = formstruct;
         listdim = structdim;
         listform = frm;
         current_form = frm;
         menue.menSAnz = GetFeldSpalten (frm);
         SetScrollRange (lbox, SB_HORZ, 0,
                         menue.menSAnz,
                         TRUE);
         listwindow = hWnd;
         TestListBefore ();
         SendMessage (lbox, LB_SETCURSEL, 0, 0L);
         SetFocus (lbox);
         if (listanz == 0 && DoDoubleClck == NULL)
         {
                 SendMessage (hListBox, WM_COMMAND,
                            (WPARAM) LBN_INSERTROW, 1l);
         }
         ProcessMessages ();
         current_form = savefrm;
         FeldScroll = ScrollSave;
         SetListEWindow (0);

         listtab    = slisttab;
         listanz    = slistanz;
         liststruct = sliststruct;
         listdim    = slistdim;
         listform   = slistform;
}

int IsFirstListPos (void)
/**
Pruefen, ob die 1. Zeile der Liste erreicht wurde.
**/
{
         if (menue.menSelect > 0)
         {
                      return 0;
         }
         return 1;
}

int IsLastEnterField (int pos)
{
          int i;
          for (i = current_form->fieldanz - 1; i >= 0; i --)
          {
               if (current_form->mask[i].attribut & DISPLAYONLY ||
                      current_form->mask[i].attribut & BUTTON ||
                      current_form->mask[i].attribut & ICON ||
                      current_form->mask[i].attribut & REMOVED ||
                      current_form->mask[i].attribut & READONLY)
               {
                              continue;
               }
               else
               {
                      break;
               }
         }
         if (i == pos)
         {
               return TRUE;
         }
         return FALSE;
}

int IsLastListPos (void)
/**
Pruefen, ob die letzte Zeile der Liste erreicht wurde.
**/
{
         if (menue.menSelect < menue.menAnz - 1)
         {
                      return 0;
         }
         return 1;
}


void CloseEWindow (HWND hWnd)
/**
Window entfernen.
**/
{
        if (hWnd)
        {
                 DestroyWindow (hWnd);
        }
}

void SetVeLines (int mode)
/**
Verikale Linien aus oder einschalten.
**/
{
        vlines = mode;
        if (vlines != 0) vlines = 1;
}

void SetHoLines (int mode)
/**
Horizontale Linien aus oder einschalten.
**/
{
        hlines = mode;
        if (hlines != 0) hlines = 1;
}



int InsertMenueZeile (WPARAM wParam, LPARAM lParam)
/**
Zeile in Menue einfuegen.
**/
{
        char **menArr;
        int idx;

        switch (AktMenZeile)
        {
                case 0 :
                          menArr = menue.menArr;
                          break;
                case 1 :
                          menArr = menue.menArr1;
                          break;
                case 2 :
                          menArr = menue.menArr2;
                          break;
        }
        clipped ((char *) lParam);
        if ((int) wParam == -1 && AktMenZeile == 0)
        {
                menue.menArr[menue.menAnz] =
                         (char *)
                               GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
                               strcpy (menue.menArr[menue.menAnz],
                                       (char *) lParam);
                               idx = menue.menAnz;
                               menue.menAnz ++;
         }
         else        
         {
                 if (menArr[(int) wParam])
                 {
                           GlobalFree ((char *) menArr[(int) wParam]);
                 }
                 menArr[(int) wParam] =
                           (char *)
                                 GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
                                 strcpy ((char *) menArr[(int) wParam],
                                       (char *) lParam);
                 if ((int) wParam >= menue.menAnz)
                 {
                                  menue.menAnz = wParam + 1;
                 }
                 idx = (int) wParam;
          }
          return (idx);
}

LONG FAR PASCAL ListProcBu(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {
              case WM_PAINT :
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


LONG FAR PASCAL ListProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
        POINT mousepos;
        CREATESTRUCT *cr;
        static int xchar, ychar, cxCaps;
        static int cxClient, cyClient;
        int idx;
        RECT rect, srect;
        int i;
        int Slen;
        int diff;
        static int budown = 0;
        static BOOL MousePressed = 0;
        static int InTimer = 0;
 	    SCROLLINFO scinfo;

        switch(msg)
        {
              case WM_CREATE :
                    hdc = GetDC (hWnd);
                    GetTextMetrics (hdc, &tm);
                    cr = (CREATESTRUCT *) lParam;
                    menue.menWzeilen  = cr->cy / tm.tmHeight - 1;
                    menue.menWspalten = cr->cx / tm.tmAveCharWidth;
                    menue.menZeile = 0;
                    menue.menSpalte = 0;
                    menue.menFpos = 0;
                    menue.menSelect = -1;
                    menue.menAnz = 0;
                    menue.menTAnz = 0;
                    menue.menSAnz = 0;
                    menue.menhFont = 0;
                    xchar = tm.tmAveCharWidth;
                    ychar = tm.tmHeight + tm.tmExternalLeading;
                    cxCaps = (tm.tmPitchAndFamily &1 ? 3 : 2) * xchar / 2;
                    ReleaseDC (hWnd, hdc);
                    return 0;

              case WM_SIZE :
                    hdc = GetDC (hWnd);
                    GetTextMetrics (hdc, &tm);
                    ReleaseDC (hWnd, hdc);
                    cyClient = HIWORD (lParam);
                    cxClient = LOWORD (lParam);
                    cxCaps = (tm.tmPitchAndFamily & 1 ? 3 : 2) * xchar / 2;
                    menue.menWzeilen  = HIWORD (lParam) / tm.tmHeight;
                    menue.menWspalten = LOWORD (lParam) / tm.tmAveCharWidth;

                    {
	                           scinfo.cbSize = sizeof (SCROLLINFO);
	                           scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                               scinfo.nPage  = 1;
     	                   	   scinfo.nMin   = 0;
		                       scinfo.nMax   = menue.menAnz - 
                                  (menue.menWzeilen - 1) / listzab,
                               SetScrollInfo  (hWnd, SB_VERT, &scinfo, TRUE);
                    }

                    if (menue.menSAnz)
                    {
                               if (listenter && listform)
                               {
	                                 scinfo.cbSize = sizeof (SCROLLINFO);
	                                 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                                     scinfo.nPage  = 1;
     	                   	         scinfo.nMin   = 0;
		                             scinfo.nMax   = listform->fieldanz - 1; 
                                     SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                               }
                               else if (listenter == 0)
                               {
	                                 scinfo.cbSize = sizeof (SCROLLINFO);
	                                 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                                     scinfo.nPage  = menue.menWspalten + 1;;
     	                   	         scinfo.nMin   = 0;
		                             scinfo.nMax   = menue.menSAnz; 
                                     SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                               }
                    }

                    SetScrollPos (hWnd, SB_VERT, menue.menZeile, TRUE);
                    SetScrollPos (hWnd, SB_HORZ, menue.menSpalte, TRUE);
                    return 0;

              case WM_VSCROLL :
                    lboxBar = (HWND) lParam;
                    memcpy (&srect, &menue.rect, sizeof (RECT));
                    switch (LOWORD (wParam))
                    {
                          case SB_LINEUP :
                                 if (menue.menZeile > 0)
                                 {
                                       menue.menZeile -= 1;
                                       ScrollWindow (lbox, 0, tm.tmHeight,
                                          &menue.rect, &menue.rect);
                                       srect.bottom = srect.top + 3 * tm.tmHeight;
                                 }
                                 break;
                          case SB_LINEDOWN :
                                 if (menue.menZeile < menue.menAnz 
                                     -  (menue.menWzeilen - 1))
                                 {
                                           menue.menZeile += 1;
                                           ScrollWindow (lbox, 0, -tm.tmHeight,
                                                   &menue.rect, &menue.rect);
                                           srect.top = srect.bottom - 3 * tm.tmHeight;
                                 }
                                 break;
                          case SB_PAGEUP :
                                 SendMessage (hWnd, WM_KEYDOWN,
                                                    VK_PRIOR, 0l);
                                 return 0;
                          case SB_PAGEDOWN :
                                 SendMessage (hWnd, WM_KEYDOWN,
                                                    VK_NEXT, 0l);
                                 return 0;
                          case SB_THUMBPOSITION :
                                 diff = menue.menZeile - HIWORD (wParam);
                                 menue.menZeile = HIWORD (wParam);

                                 menue.menZeile = max (0, min (menue.menZeile,
                                          menue.menAnz
                                         - (menue.menWzeilen - 1) / listzab));
                                 if (menue.menZeile
                                         == GetScrollPos (hWnd, SB_VERT))
                                 {
                                     diff = 0;
                                 }

                                 if (diff < 0)
                                 {
                                         ScrollWindow (lbox, 0, diff *tm.tmHeight,
                                                   &menue.rect, &menue.rect);
                                         srect.top = 
                                                   srect.bottom + (diff - 3) * tm.tmHeight;

                                 }
                                 else if (diff > 0)
                                 {
                                         ScrollWindow (lbox, 0, diff *tm.tmHeight,
                                                   &menue.rect, &menue.rect);
                                         srect.bottom = 
                                             srect.top + (diff + 3) * tm.tmHeight;
                                 }
                                 break;

                          case SB_THUMBTRACK :
                                 diff = menue.menZeile - HIWORD (wParam);
                                 menue.menZeile = HIWORD (wParam);

                                 menue.menZeile = max (0, min (menue.menZeile,
                                          menue.menAnz
                                         - (menue.menWzeilen - 1) / listzab));
                                 if (menue.menZeile
                                         == GetScrollPos (hWnd, SB_VERT))
                                 {
                                     diff = 0;
                                 }

                                 if (diff < 0)
                                 {
                                         ScrollWindow (lbox, 0, diff *tm.tmHeight,
                                                   &menue.rect, &menue.rect);
                                         srect.top = 
                                             srect.bottom + (diff - 3) * tm.tmHeight;
                                 }
                                 else if (diff > 0)
                                 {
                                         srect.bottom = 
                                         ScrollWindow (lbox, 0, diff *tm.tmHeight,
                                                   &menue.rect, &menue.rect);
                                         srect.bottom = 
                                                  srect.top + (diff + 3) * tm.tmHeight;
                                 }
                                 break;

                          case SB_ENDSCROLL :
                          default :
                                 return DefWindowProc(hWnd, msg,
                                                      wParam, lParam);

                    }
                    menue.menZeile = max (0, min (menue.menZeile,
                                         menue.menAnz
                                         - (menue.menWzeilen - 1) / listzab));
                    if (menue.menZeile
                         != GetScrollPos (hWnd, SB_VERT))
                    {
                          SetScrollPos (hWnd, SB_VERT, menue.menZeile,
                                        TRUE);
                          InvalidateRect (hWnd, &srect, TRUE);
                    }
                    return 0;
              case WM_HSCROLL :
                    switch (LOWORD (wParam))
                    {
                          case SB_LINEUP :
                                 menue.menSpalte -= 1;
                                 menue.menFpos --;
                                 if (listenter)
                                 {
                                       menue.menSpalte =
                                              GetFeldSpalte (-1);
                                 }
                                 break;
                          case SB_LINEDOWN :
                                 menue.menSpalte += 1;
                                 menue.menFpos ++;
                                 if (listenter)
                                 {
                                       menue.menSpalte =
                                              GetFeldSpalte (1);
                                 }
                                 break;
                          case SB_PAGEUP :
                                 menue.menSpalte -= menue.menWspalten;
                                 break;
                          case SB_PAGEDOWN :
                                 menue.menSpalte += menue.menWspalten;
                                 break;
                          case SB_THUMBPOSITION :
                          case SB_THUMBTRACK :
                                 menue.menSpalte =   HIWORD (wParam);
                                 if (listenter)
                                 {
                                       menue.menFpos = menue.menSpalte;
                                       menue.menSpalte =
                                              SetFeldSpalte ();
                                 }
                                 break;
                          case SB_ENDSCROLL :
                          default :
                                 return DefWindowProc(hWnd, msg,
                                                      wParam, lParam);
                    }
                    if (listenter == 0)
                    {
                         menue.menSpalte = max (0, min (menue.menSpalte,
                                       menue.menSAnz - menue.menWspalten));
                    }
                    if (listenter == 0 && 
                        menue.menSpalte != GetScrollPos (hWnd, SB_HORZ))
                    {
                          SetScrollPos (hWnd, SB_HORZ, menue.menSpalte, TRUE);  
                          InvalidateRect (hWnd, NULL, TRUE);
                    }
                    else if (listenter && 
                        menue.menFpos != GetScrollPos (hWnd, SB_HORZ))
                    {
                          SetScrollPos (hWnd, SB_HORZ, menue.menFpos, TRUE);  
                          InvalidateRect (hWnd, NULL, TRUE);
                    }
                    return 0;
              case WM_PAINT :
                    ShowMenue (hWnd);
                    return TRUE;
              case WM_KEYDOWN :
                    memcpy (&srect, &menue.rect, sizeof (RECT));
                    switch (wParam)
                    {
                       case VK_DOWN :
                         if (MenSelect == FALSE)
						 {
                                 if (menue.menZeile < menue.menAnz 
                                     -  (menue.menWzeilen - 1))
                                 {
                                            menue.menZeile ++;
                                            SetScrollPos (hWnd, SB_VERT,
                                                    menue.menZeile, 
                                                    TRUE);
                                            ScrollWindow (lbox, 0, -tm.tmHeight,
                                                   &menue.rect, &menue.rect);
                                            srect.top = srect.bottom - 3 * tm.tmHeight;
                                            InvalidateRect (hWnd, &srect, TRUE);
                                 }
                                 break;
						 }
                         if (menue.menSelect < menue.menAnz - 1)
                         {
                               ShowRow (hWnd, BLACKCOL, WHITECOL);
                               menue.menSelect ++;
							   if (selcolor)
                               {
                                   ShowRow (hWnd, WHITECOL, BLUECOL);
							   }
                         }
                         if ((menue.menSelect - menue.menZeile) * listzab >
                                          menue.menWzeilen - 1 - sz)
                         {
                               menue.menZeile ++;
                               SetScrollPos (hWnd, SB_VERT,
                                                    menue.menZeile, 
                                                    TRUE);
                               ScrollWindow (lbox, 0, -tm.tmHeight,
                                                   &menue.rect, &menue.rect);
                               srect.top = srect.bottom - 2 * tm.tmHeight;
                               InvalidateRect (hWnd, &srect, TRUE);
                         }
                         return 0;
                       case VK_UP :
                         if (MenSelect == FALSE)
						 {
                                 if (menue.menZeile > 0)
                                 {
                                            menue.menZeile --;
                                            SetScrollPos (hWnd, SB_VERT,
                                                         menue.menZeile, TRUE);
                                            ScrollWindow (lbox, 0, tm.tmHeight,
                                                   &menue.rect, &menue.rect);
                                            srect.bottom = srect.top + 3 * tm.tmHeight;
                                            InvalidateRect (hWnd, &srect, TRUE);
                                 }
                                 break;
						 }
                         if (menue.menSelect > 0)
                         {
                               ShowRow (hWnd, BLACKCOL, WHITECOL);
                               menue.menSelect --;
							   if (selcolor)
                               {
                                   ShowRow (hWnd, WHITECOL, BLUECOL);
							   }
                         }
                         if (menue.menSelect < menue.menZeile)
                         {
                               menue.menZeile --;
                               SetScrollPos (hWnd, SB_VERT,
                                             menue.menZeile, TRUE);
                               ScrollWindow (lbox, 0, tm.tmHeight,
                                                   &menue.rect, &menue.rect);
                               srect.bottom = srect.top + 2 * tm.tmHeight;
                               InvalidateRect (hWnd, &srect, TRUE);
                         }
                         return 0;
                       case VK_RIGHT :
                         if (menue.menSpalte <  5 + menue.menSAnz -
                                                    menue.menWspalten ||
                             listenter)
                         {
                               menue.menSpalte ++;
                               menue.menFpos ++;
                               if (listenter)
                               {
                                       menue.menSpalte =
                                              GetFeldSpalte (1);
                                       SetScrollPos (hWnd, SB_HORZ,
                                                    menue.menFpos, TRUE);
                               }
                               else
                               {
                                      SetScrollPos (hWnd, SB_HORZ,
                                                    menue.menSpalte, TRUE);
                               }
                               InvalidateRect (hWnd, NULL, TRUE);
                         }
                         return 0;
                       case VK_LEFT :
                         if (menue.menSpalte > 0)
                         {
                               menue.menSpalte --;
                               menue.menFpos --;
                               if (listenter)
                               {
                                       menue.menSpalte =
                                              GetFeldSpalte (-1);
                                      SetScrollPos (hWnd, SB_HORZ,
                                                    menue.menFpos, 
                                                    TRUE);
                               }
                               else
                               {
                                      SetScrollPos (hWnd, SB_HORZ,
                                                    menue.menSpalte, 
                                                    TRUE);
                               }
                               InvalidateRect (hWnd, NULL, TRUE);
                         }
                         return 0;
                       case VK_NEXT :
                         if (menue.menSelect - menue.menZeile <
                             (menue.menWzeilen - 1) / listzab - sz)
                         {
                               ShowRow (hWnd, BLACKCOL, WHITECOL);
                               menue.menSelect = menue.menZeile +
                                                 (menue.menWzeilen
                                                  - 1) / listzab - sz;
                               if (menue.menSelect > menue.menAnz - 1)
                               {
                                        menue.menSelect = menue.menAnz - 1;
                               }
                               ShowRow (hWnd, WHITECOL, BLUECOL);
						 }
                         else
                         {
                               menue.menZeile += (menue.menWzeilen / listzab
                                                  - sz);

                               menue.menZeile = min (menue.menZeile,
                                          sz + menue.menAnz
                                          - menue.menWzeilen / listzab);
                               menue.menSelect = menue.menZeile;
                               SetScrollPos (hWnd, SB_VERT,
                                                    menue.menZeile * listzab, 
                                                    TRUE);
                               InvalidateRect (hWnd, &menue.rect, TRUE);
                         }
                         return 0;
                       case VK_PRIOR :
                         if (menue.menSelect > menue.menZeile)
                         {
                               ShowRow (hWnd, BLACKCOL, WHITECOL);
                               menue.menSelect = menue.menZeile;
                               ShowRow (hWnd, WHITECOL, BLUECOL);
                         }
                         else if (menue.menZeile > 0)
                         {
                               menue.menZeile -= (menue.menWzeilen / listzab
                                                 - sz);
                               menue.menZeile = max (0, menue.menZeile);
                               menue.menSelect = menue.menZeile +
                                                 (menue.menWzeilen
                                                  - 1) / listzab - sz;
                               SetScrollPos (hWnd, SB_VERT,
                                                    menue.menZeile * listzab, 
                                                    TRUE);
                               InvalidateRect (hWnd, &menue.rect, TRUE);
                         }
                         return 0;
                     }
                     return 0;
              case WM_LBUTTONDOWN :
              {
                      GetCursorPos (&mousepos);
                      if (!mouseindialog (hWnd, &mousepos)) return TRUE;
                      if (IsDlgClient (hWnd, &mousepos))
                      {
                          SelectRow (hWnd, wParam, lParam);
                          SetTimer (hWnd, 1, 200, 0);  
                          InTimer = 1;
                          MousePressed = TRUE; 
                          return 0;
                      }
                      break;
              }
              case WM_LBUTTONUP :
              {
                      KillTimer (hWnd, 1); 
                      MousePressed = FALSE;
                      InTimer = 0;
                      break;
              }
 
              case WM_TIMER :
              {
				     if (wParam == 2 && LbTimeProc)
					 {
						 (*LbTimeProc) ();
						 break;

					 }

                     GetCursorPos (&mousepos);

                     if (IsDlgClient (hWnd, &mousepos)) return 0;


                     if (GetKeyState (VK_LBUTTON) >= 0)
                     {
                               KillTimer (hWnd, 1); 
                               MousePressed = 0;
                               InTimer  = 0;
                     }

                     diff = MousetohWnd (hWnd, &mousepos);
                     if (diff == 1)
                     { 
                               if (InTimer == 2)
                               {
                                          KillTimer (hWnd, 1); 
                                          SetTimer (hWnd, 1, 200, 0);  
                                          InTimer = 1;
                               }
                               SendMessage (hWnd, WM_KEYDOWN,
                                                  VK_DOWN, lParam);
                     }
                     else if (diff == 2)
                     { 
                               if (InTimer == 1)
                               {
                                          KillTimer (hWnd, 1); 
                                          SetTimer (hWnd, 1, 50, 0);  
                                          InTimer = 2;
                               }
                               SendMessage (hWnd, WM_KEYDOWN,
                                                  VK_DOWN, lParam);
                     }
                     else if (diff == -1)
                     { 
                               if (InTimer == 2)
                               {
                                          KillTimer (hWnd, 1); 
                                          SetTimer (hWnd, 1, 200, 0);  
                                          InTimer = 1;
                               }
                               SendMessage (hWnd, WM_KEYDOWN,
                                                  VK_UP, lParam);
                     }
                     else if (diff == -2)
                     { 
                               if (InTimer == 1)
                               {
                                          KillTimer (hWnd, 1); 
                                          SetTimer (hWnd, 1, 50, 0);  
                                          InTimer = 2;
                               }
                               SendMessage (hWnd, WM_KEYDOWN,
                                                  VK_UP, lParam);
                     }
                     return 0;
              }

              case WM_LBUTTONDBLCLK :
              {


                      GetCursorPos (&mousepos);

                      if (!mouseindialog (hWnd, &mousepos)) return TRUE;

                      if (IsDlgClient (hWnd, &mousepos))
                      {
                          SelectRow (hWnd, wParam, lParam);
                          if (WithDblClck)
                          {
                                  SendMessage (GetParent (hWnd), WM_COMMAND, 
                                       MAKELONG (LBOX, LBN_DBLCLK), 0l);
                          }
                          return 0;
                      }
                      break;
              }
              case WM_MOUSEMOVE :
				      if (MenSelect == 0) break;
                      GetCursorPos (&mousepos);

                      if (!mouseindialog (hWnd, &mousepos)) return TRUE;

                      if (wParam & MK_LBUTTON)
                      {
                            SelectRow (hWnd, wParam, lParam);
                            return TRUE;
                      }
                      break;
              case LB_VPOS :
                              clipped ((char *) lParam);
                              if (strlen ((char *) lParam) == 0)
                              {
                                     return 0;
                              }
                              menue.menVlpos =
                              (char *)
                                    GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
                              strcpy (menue.menVlpos, (char *) lParam);
                              return 0;

              case LB_TITLE :
                              clipped ((char *) lParam);
                              if (strlen ((char *) lParam) == 0)
                              {
                                     return 0;
                              }
                              menue.menTitle[menue.menTAnz] =
                              (char *)
                                    GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
                              strcpy (menue.menTitle[menue.menTAnz],
                                      (char *) lParam);
                              menue.menTAnz ++;
                              sz = menue.menTAnz;
                              menue.rect.top     = menue.menTAnz *
                                                   tm.tmHeight;
                              menue.trect.bottom = menue.menTAnz * tm.tmHeight;
                              InvalidateRect (hWnd, &menue.trect, TRUE);

                              return 0;

              case LB_INSERTSTRING :
                              idx = InsertMenueZeile (wParam, lParam);

                                        
                              scinfo.cbSize = sizeof (SCROLLINFO);
                              scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                              scinfo.nPage  = 1;
               	              scinfo.nMin   = 0;
	                          scinfo.nMax   = menue.menAnz - 
                                                   (menue.menWzeilen - 1) / listzab,
                              SetScrollInfo  (hWnd, SB_VERT, &scinfo, TRUE);
                              Slen = (int) strlen ((char *) lParam)
                                             + 22 * cxCaps / xchar ;
                              if (Slen > menue.menSAnz)
                              {
                                     menue.menSAnz = Slen; 

                                     if (listform && listenter)
                                     {
	                                        scinfo.cbSize = sizeof (SCROLLINFO);
	                                        scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                                            scinfo.nPage  = 1;
     	                   	                scinfo.nMin   = 0;
		                                    scinfo.nMax   = listform->fieldanz - 1; 
                                            SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                                     }
                                     else if (listenter == 0)
                                     {
	                                        scinfo.cbSize = sizeof (SCROLLINFO);
	                                        scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                                            scinfo.nPage  = menue.menWspalten + 1;
     	                   	                scinfo.nMin   = 0;
		                                    scinfo.nMax   = menue.menSAnz; 
                                            SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                                     }


                              }
                              if (menue.menAnz * listzab < menue.menZeile +
                                                       menue.menWzeilen)
                              {
                                     SetUpdReg (&rect, idx); 
                                     InvalidateRect (hWnd, &rect, TRUE);
                              }
                              return 0;
              case LB_RESETCONTENT :
                              for (i = 0; i < menue.menAnz; i ++)
                              {
                                          if (menue.menArr[i])
                                          {
                                               GlobalFree (menue.menArr[i]);
                                               menue.menArr [i] = NULL;
                                          }
                                          if (menue.menArr1[i])
                                          {
                                               GlobalFree (menue.menArr1[i]);
                                               menue.menArr1 [i] = NULL;
                                          }
                                          if (menue.menArr2[i])
                                          {
                                               GlobalFree (menue.menArr2[i]);
                                               menue.menArr2 [i] = NULL;
                                          }
                              }
							  menue.menAnz = 0;
                              menue.menSAnz = 0; 
                              menue.menZeile = 0; 
/*
                              if (menue.menTitle[0])
                              {
                                          GlobalFree (menue.menTitle[0]);
										  menue.menTitle[0] = NULL;
                              }
                              if (menue.menVlpos)
                              {
                                          GlobalFree (menue.menVlpos);
										  menue.menVlpos = NULL;
                              }
                              if (menue.menhFont && menue.menhFont != stdHfont)
                              {
                                          DeleteObject (menue.menhFont);
										  menue.menhFont = NULL;
                              }
*/
							  return 0;
              case LB_GETCURSEL :
                              return (menue.menSelect);
              case LB_SETCURSEL :
                              menue.menSelect = (int) wParam;
                              InvalidateRect (hWnd, &menue.rect, TRUE);
                              UpdateWindow (hWnd);
                              return 0;
               case WM_SETFONT :
                              menue.menhFont = (HFONT) wParam;
                              hdc = GetDC (hWnd);
                              SelectObject (hdc, menue.menhFont);
                              GetTextMetrics (hdc, &tm);
                              ReleaseDC (hWnd, hdc);
                              xchar = tm.tmAveCharWidth;
                              ychar = tm.tmHeight + tm.tmExternalLeading;
                              cxCaps = (tm.tmPitchAndFamily & 1 ? 3 : 2) * xchar / 2;
                              menue.menWzeilen  = cyClient / tm.tmHeight;
                              menue.menWspalten = cxClient / tm.tmAveCharWidth;
                              if (menue.menAnz * listzab > menue.menWzeilen)
                              {

                                    scinfo.cbSize = sizeof (SCROLLINFO);
                                    scinfo.fMask  = SIF_PAGE | SIF_RANGE;
                                    scinfo.nPage  = 1;
                   	                scinfo.nMin   = 0;
		                            scinfo.nMax   = menue.menAnz - 
                                                   (menue.menWzeilen - 1) / listzab,
                                    SetScrollInfo  (hWnd, SB_VERT, &scinfo, TRUE);
                              }
                              else if (menue.menSAnz > menue.menWspalten)
                              {
                                     SetScrollRange (hWnd, SB_HORZ, 0,
                                     5 + menue.menSAnz * listzab 
                                    - menue.menWspalten, TRUE);

 	                                 scinfo.cbSize = sizeof (SCROLLINFO);
	                                 scinfo.fMask  = SIF_PAGE;
                                     if (listform && listenter)
									 {
                                                     scinfo.nPage  = 1;
									 }
									 else if (listenter == 0)
									 {
                                                     scinfo.nPage  = menue.menWspalten + 1;
									 }
                                     SetScrollInfo  (hWnd, SB_HORZ, &scinfo, TRUE);
                              }
                              SetScrollPos (hWnd, SB_VERT, menue.menZeile * listzab, TRUE);
                              SetScrollPos (hWnd, SB_HORZ, menue.menSpalte * listzab, TRUE);
              case WM_COMMAND :
                    if (LOWORD (wParam) == LBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
                                      syskey = KEYCR;
                                      menueidx = SendMessage
                                           (lbox, LB_GETCURSEL, 0l, 0l);
                                      DestroyWindow (hListBox);
                                      break;
                           }
                    }
                    return 0;
               case WM_DESTROY :
                              lboxBar = NULL;
                              for (i = 0; i < menue.menAnz; i ++)
                              {
                                          if (menue.menArr[i])
                                          {
                                               GlobalFree (menue.menArr[i]);
                                               menue.menArr [i] = NULL;
                                          }
                                          if (menue.menArr1[i])
                                          {
                                               GlobalFree (menue.menArr1[i]);
                                               menue.menArr1 [i] = NULL;
                                          }
                                          if (menue.menArr2[i])
                                          {
                                               GlobalFree (menue.menArr2[i]);
                                               menue.menArr2 [i] = NULL;
                                          }
                              }
                              if (menue.menTitle[0])
                              {
                                          GlobalFree (menue.menTitle[0]);
										  menue.menTitle[0] = NULL;
                              }
                              if (menue.menVlpos)
                              {
                                          GlobalFree (menue.menVlpos);
										  menue.menVlpos = NULL;
                              }
                              if (menue.menhFont && menue.menhFont != stdHfont)
                              {
                                          DeleteObject (menue.menhFont);
										  menue.menhFont = NULL;
                              }
                              break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}



LONG FAR PASCAL ShowListProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
        static TEXTMETRIC tm;
        CREATESTRUCT *cr;
        static int xchar, ychar, cxCaps;
        static int cxClient, cyClient;
        int i;

        switch(msg)
        {
              case WM_CREATE :
                    hdc = GetDC (hWnd);
                    GetTextMetrics (hdc, &tm);
                    cr = (CREATESTRUCT *) lParam;
                    menue.menWzeilen  = cr->cy / tm.tmHeight - 1;
                    menue.menWspalten = cr->cx / tm.tmAveCharWidth;
                    menue.menZeile = 0;
                    menue.menSpalte = 0;
                    menue.menSelect = -1;
                    menue.menAnz = 0;
                    menue.menSAnz = 0;
                    menue.menhFont = 0;
                    xchar = tm.tmAveCharWidth;
                    ychar = tm.tmHeight + tm.tmExternalLeading;
                    cxCaps = (tm.tmPitchAndFamily &1 ? 3 : 2) * xchar / 2;
                    ReleaseDC (hWnd, hdc);
                    return 0;

              case WM_SIZE :
                    cyClient = HIWORD (lParam);
                    cxClient = LOWORD (lParam);
                    cxCaps = (tm.tmPitchAndFamily & 1 ? 3 : 2) * xchar / 2;
                    menue.menWzeilen  = HIWORD (lParam) / tm.tmHeight;
                    menue.menWspalten = LOWORD (lParam) / tm.tmAveCharWidth;
                    if (menue.menAnz > menue.menWzeilen)
                    {
                               SetScrollRange (hWnd, SB_VERT, 0,
                                    menue.menAnz - menue.menWzeilen, TRUE);
                    }

                    if (menue.menSAnz > menue.menWspalten)
                    {
                               SetScrollRange (hWnd, SB_HORZ, 0,
                               5 + menue.menSAnz - menue.menWspalten, TRUE);
                    }
                    SetScrollPos (hWnd, SB_VERT, menue.menZeile, TRUE);
                    SetScrollPos (hWnd, SB_HORZ, menue.menSpalte, TRUE);
                    return 0;

              case WM_VSCROLL :
                    switch (LOWORD (wParam))
                    {
                          case SB_LINEUP :
                                 menue.menZeile -= 1;
                                 break;
                          case SB_LINEDOWN :
                                 menue.menZeile += 1;
                                 break;
                          case SB_THUMBPOSITION :
                                 menue.menZeile = HIWORD (wParam);
                                 break;
                          case SB_THUMBTRACK :
                                 menue.menZeile = HIWORD (wParam);
                                 break;
                          case SB_ENDSCROLL :
                          default :
                                 return DefWindowProc(hWnd, msg,
                                                      wParam, lParam);
                    }
                    menue.menZeile = max (0, min (menue.menZeile,
                                                  menue.menAnz
                                                  - menue.menWzeilen));
                    if (menue.menZeile != GetScrollPos (hWnd, SB_VERT))
                    {
                          SetScrollPos (hWnd, SB_VERT, menue.menZeile, TRUE);  
                          InvalidateRect (hWnd, &menue.rect, TRUE);
                    }
                    return 0;
              case WM_HSCROLL :
                    switch (LOWORD (wParam))
                    {
                          case SB_LINEUP :
                                 menue.menSpalte -= 1;
                                 break;
                          case SB_LINEDOWN :
                                 menue.menSpalte += 1;
                                 break;
                          case SB_THUMBPOSITION :
                                 menue.menSpalte =   HIWORD (wParam);
                                 break;
                          case SB_THUMBTRACK :
                                 menue.menSpalte    = HIWORD (wParam);
                                 break;
                          case SB_ENDSCROLL :
                          default :
                                 return DefWindowProc(hWnd, msg,
                                                      wParam, lParam);
                    }
                    menue.menSpalte = max (0, min (menue.menSpalte,
                                      5 + menue.menSAnz - menue.menWspalten));
                    if (menue.menSpalte != GetScrollPos (hWnd, SB_HORZ))
                    {
                          SetScrollPos (hWnd, SB_HORZ, menue.menSpalte, TRUE);
                          if (menue.srect.bottom == 0)
                          {
                                    InvalidateRect (hWnd, NULL, TRUE);
                          }
                          else
                          {
                                    InvalidateRect (hWnd, &menue.srect, TRUE);
                          }
                    }
                    return 0;
              case WM_PAINT :
                    ShowMenue (hWnd);
                    return TRUE;
              case WM_LBUTTONUP :
                    ReleaseCapture ();
                    return TRUE;
              case WM_KEYDOWN :
                    switch (wParam)
                    {
                       case VK_DOWN :
                         if (menue.menZeile < menue.menAnz -
                                              menue.menWzeilen - 1 - sz)
                         {
                               menue.menZeile ++;
                               SetScrollPos (hWnd, SB_VERT,
                                                    menue.menZeile, TRUE);
                               InvalidateRect (hWnd, &menue.rect, TRUE);
                         }
                         return 0;
                       case VK_UP :
                         if (menue.menZeile > 0)
                         {
                               menue.menZeile --;
                               SetScrollPos (hWnd, SB_VERT,
                                             menue.menZeile, TRUE);
                               InvalidateRect (hWnd, &menue.rect, TRUE);
                         }
                         return 0;
                       case VK_RIGHT :
                         if (menue.menSpalte <  5 + menue.menSAnz -
                                                    menue.menWspalten)
                         {
                               menue.menSpalte ++;
                               SetScrollPos (hWnd, SB_HORZ,
                                                    menue.menSpalte, TRUE);
                               if (menue.srect.bottom == 0)
                               {
                                    InvalidateRect (hWnd, NULL, TRUE);
                               }
                               else
                               {
                                    InvalidateRect (hWnd, &menue.srect, TRUE);
                               }
                         }
                         return 0;
                       case VK_LEFT :
                         if (menue.menSpalte > 0)
                         {
                               menue.menSpalte --;
                               SetScrollPos (hWnd, SB_HORZ,
                                                    menue.menSpalte, TRUE);
                               if (menue.srect.bottom == 0)
                               {
                                    InvalidateRect (hWnd, NULL, TRUE);
                               }
                               else
                               {
                                    InvalidateRect (hWnd, &menue.srect, TRUE);
                               }
                         }
                         return 0;

                       case VK_TAB :
                         if (GetKeyState (VK_SHIFT) < 0)
                         {
                               menue.menSpalte --;
                               SetScrollPos (hWnd, SB_HORZ,
                                                    menue.menSpalte, TRUE);
                         }
                         else
                         {
                               menue.menSpalte ++;
                               SetScrollPos (hWnd, SB_HORZ,
                                                   menue.menSpalte, TRUE);
                         }
                         InvalidateRect (hWnd, NULL, TRUE);
                         return 0;

                       case VK_NEXT :
                         menue.menZeile += menue.menWzeilen - sz;
                         menue.menZeile = min (menue.menZeile,
                                      menue.menAnz - (menue.menWzeilen - sz));
                         SetScrollPos (hWnd, SB_VERT, menue.menZeile, TRUE);
                         InvalidateRect (hWnd, &menue.rect, TRUE);
                         return 0;
                       case VK_PRIOR :
                         menue.menZeile -= (menue.menWzeilen - sz);
                         menue.menZeile = max (0, menue.menZeile);
                         SetScrollPos (hWnd, SB_VERT,
                                                    menue.menZeile, TRUE);
                         InvalidateRect (hWnd, &menue.rect, TRUE);
                         return 0;
                     }
                     return 0;
              case LB_VPOS :
                              clipped ((char *) lParam);
                              if (strlen ((char *) lParam) == 0)
                              {
                                     return 0;
                              }
                              menue.menVlpos =
                              (char *)
                                    GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
                              strcpy (menue.menVlpos, (char *) lParam);
                              return 0;
              case LB_TITLE :
                              clipped ((char *) lParam);
                              if (strlen ((char *) lParam) == 0)
                              {
                                     return 0;
                              }
                              if (vlines && menue.menTAnz == MenStart - 1)
                              {
                                      SendMessage (hWnd, LB_VPOS, 0, lParam);
                                      strcpy ((char *) lParam, " ");
                                      /*
                                      memset ((char *) lParam, ' ',
                                              strlen ((char *) lParam) - 1);
                                      */
                              }
                              menue.menTitle[menue.menTAnz] =
                              (char *)
                                    GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
                              strcpy (menue.menTitle[menue.menTAnz],
                                      (char *) lParam);
                              menue.menTAnz ++;
                              sz = menue.menTAnz;
                              menue.rect.top     = menue.menTAnz *
                                                   tm.tmHeight;
                              menue.trect.bottom = menue.menTAnz * tm.tmHeight;
                              InvalidateRect (hWnd, &menue.trect, TRUE);

                              return 0;
              case LB_INSERTSTRING :
                              clipped ((char *) lParam);
                              menue.menArr[menue.menAnz] =
                              (char *)
                                    GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                                               strlen ((char *) lParam) + 1);
                              strcpy (menue.menArr[menue.menAnz],
                                      (char *) lParam);
                              menue.menAnz ++;
                              if (menue.menAnz > menue.menWzeilen)
                              {
                                     SetScrollRange (hWnd, SB_VERT, 0,
                                     menue.menAnz - menue.menWzeilen, TRUE);
                              }
                              if ((int) strlen ((char *) lParam) >
                                     menue.menSAnz)
                              {
                                     menue.menSAnz =
                                          (int) strlen ((char *) lParam)
                                          + 22 * cxCaps / xchar ;
                                     SetScrollRange (hWnd, SB_HORZ, 0,
                                     5 + menue.menSAnz - menue.menWspalten,
                                                     TRUE);
                              }
                              if (menue.menAnz < menue.menZeile +
                                                       menue.menWzeilen)
                              {
                                     InvalidateRect (hWnd, &menue.rect, TRUE);
                              }
                              return 0;
               case WM_SETFONT :
                              menue.menhFont = (HFONT) wParam;
                              return 0;
               case WM_DESTROY :
                              for (i = 0; i < menue.menAnz; i ++)
                              {
                                          if (menue.menArr[i])
                                          {
                                               GlobalFree (menue.menArr[i]);
                                               menue.menArr [i] = NULL;
                                          }
                                          if (menue.menArr1[i])
                                          {
                                               GlobalFree (menue.menArr1[i]);
                                               menue.menArr1 [i] = NULL;
                                          }
                                          if (menue.menArr2[i])
                                          {
                                               GlobalFree (menue.menArr2[i]);
                                               menue.menArr2 [i] = NULL;
                                          }
                              }
                              for (i = 0; i < menue.menTAnz; i ++)
                              {
                                          GlobalFree (menue.menTitle[i]);
                              }
                              if (menue.menVlpos)
                              {
                                          GlobalFree (menue.menVlpos);
                              }
                              if (menue.menhFont && menue.menhFont != stdHfont)
                              {
                                          DeleteObject (menue.menhFont);
                              }
                              break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


LONG FAR PASCAL ListBoxProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
	    TEXTMETRIC tm;
		RECT rect; 
        int cx;
		int cy;
		HDC hdc;


        switch(msg)
        {
	      case WM_SIZE :
			        if (lbox == NULL) break;

                    GetClientRect (hWnd, &rect);
                    hdc = GetDC (lbox);
                    GetTextMetrics (hdc, &tm);
                    ReleaseDC (lbox, hdc);

					if (tm.tmHeight == 0) break;

 		            cx = (rect.right - rect.left) - 2 * tm.tmAveCharWidth;
					cy = (rect.bottom - rect.top) - 3 * tm.tmHeight;
                    menue.menWzeilen  = cy / tm.tmHeight;
                    menue.menWspalten = cx / tm.tmAveCharWidth;
				    MoveWindow (lbox, 10, 0, cx, cy, TRUE);
					MoveListButtons (rect.right, cy);
					InvalidateRect (lbox, &menue.rect, TRUE); 
                    menue.rect.bottom = cy;
                    menue.rect.right = cx;
                    break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == LBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
                                      syskey = KEYCR;
                                      menueidx = SendMessage
                                           (lbox, LB_GETCURSEL, 0l, 0l);
	                                  strcpy (AktMenuTxt, menue.menArr[menueidx]);
                                      DestroyWindow (hListBox);
                                      break;
                           }
                           break;
                    }
                    else if (LOWORD (wParam) == BT_OK)
                    {
                            syskey = KEYCR;
                            menueidx = SendMessage
                                           (lbox, LB_GETCURSEL, 0l, 0l);
                            strcpy (AktMenuTxt, menue.menArr[menueidx]);
                            DestroyWindow (hListBox);
                            break;
                    }
                    else if (LOWORD (wParam) == BT_CANCEL)
                    {
                            syskey = KEY5;
                            DestroyWindow (hListBox);
                            break;
                    }
                    break;
              case WM_DESTROY :
                    break_list ();
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL EnterFormProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {
              case WM_COMMAND :
                    if (LOWORD (wParam) == LBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
                                      syskey = KEYCR;
                                      menueidx = SendMessage
                                           (lbox, LB_GETCURSEL, 0l, 0l);
	                                  strcpy (AktMenuTxt, menue.menArr[menueidx]);
                                      DestroyWindow (hListBox);
                                      break;
                           }
                           break;
                    }
                    else if (LOWORD (wParam) == BT_OK)
                    {
                            syskey = KEYCR;
                            menueidx = SendMessage
                                           (lbox, LB_GETCURSEL, 0l, 0l);
                            strcpy (AktMenuTxt, menue.menArr[menueidx]);
                            DestroyWindow (hListBox);
                            break;
                    }
                    else if (LOWORD (wParam) == BT_CANCEL)
                    {
                            syskey = KEY5;
                            DestroyWindow (hListBox);
                            break;
                    }
                    break;
              case WM_DESTROY :
                    PostQuitMessage (0);
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

         
LONG FAR PASCAL EnterListBoxProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
	    TEXTMETRIC tm;
		RECT rect; 
		int y;
        int cx;
		int cy;
		HDC hdc;

        switch(msg)
        {
	      case WM_SIZE :
                    GetClientRect (hWnd, &rect);
                    hdc = GetDC (hWnd);
                    GetTextMetrics (hdc, &tm);
                    ReleaseDC (hWnd, hdc);
					cx = rect.right - 2 * tm.tmAveCharWidth;
					cy = rect.bottom - 3 * tm.tmHeight;
					if (choisebox)
					{
					         y = choiseab * tm.tmHeight;
					         cy -= (choiseab * tm.tmHeight);
					}
					else
					{
						     y = 0;
					}
				    MoveWindow (lbox, 10, y, cx, cy, TRUE);
					if (choisebox)
					{
					         cy += (choiseab * tm.tmHeight);
					}
					MoveListButtons (rect.right, cy);
                    menue.menWzeilen  = cy / tm.tmHeight;
                    menue.menWspalten = cx / tm.tmAveCharWidth;
                    menue.rect.bottom = cy;
                    menue.rect.right = cx;
                    break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == LBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
                                      lblaktiv = 1;
                                      if (DoDoubleClck)
                                      {
                                            menueidx = SendMessage
                                                (lbox, LB_GETCURSEL, 0l, 0l);
                                            (*DoDoubleClck) (menueidx);
                                      }
                                      else
                                      {
                                             EnterListRows ();
                                      }
                                      lblaktiv = 0;
                                      InvalidateRect (lbox, NULL, TRUE);
                                      UpdateWindow (lbox);
                                      return 0;
                           }
                           break;
                    }
                    else if (wParam == LBN_INSERTROW)
                    {
                           if (listins == 0)
                           {
                                      if (lParam == (long) 1)
                                      {
                                              listins = 2;
                                              AppendListLine (1);
                                      }
                                      else if (lParam == (long) 2)
                                      {
                                              listins = 2;
                                              InsertListLine ();
                                      }
                                      else
                                      {
                                              listins = 1;
                                              InsertListLine ();
                                      }
                                      EnterListRows ();
                                      InvalidateRect (lbox, NULL, TRUE);
                                      UpdateWindow (lbox);
                                      listins = 0;
                           }
                           return 0;
                    }
                    else if (wParam == LBN_DELETEROW)
                    {
                           if (nolistdel == 0)
                           {
                                   DeleteListLine ();
                           }
                           return 0;
                    }
                    else if (LOWORD (wParam) == BT_OK)
                    {
                            syskey = KEYCR;
                            menueidx = SendMessage
                                           (lbox, LB_GETCURSEL, 0l, 0l);
                            DestroyWindow (hListBox);
                            break;
                    }
                    else if (LOWORD (wParam) == BT_CANCEL)
                    {
                            syskey = KEY5;
                            DestroyWindow (hListBox);
                            break;
                    }
                    break;
              case WM_DESTROY :
                    break_list ();
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

/*
void colorpaint (HWND hWnd, char *text, int zeile, int spalte,
                 COLORREF vcolor, COLORREF bcolor, HFONT hFont)
/?*
Text in Fenster schreiben.
*?/
{
        HDC      hdc;
        PAINTSTRUCT ps;
        int x, y;
        int xchar;
        int ychar;

        xchar = tm.tmAveCharWidth;;
        ychar = tm.tmHeight;

        x = spalte * xchar;
        y = zeile * ychar;

        hdc = BeginPaint (hWnd, &ps);
        if (hFont)
        {
               SelectObject (hdc, hFont);
               GetTextMetrics (hdc, &tm);
        }
        SetBkMode (hdc, OPAQUE);
        SetTextColor (hdc, vcolor);
        SetBkColor (hdc, bcolor);
        TextOut (hdc, x, y, text, strlen (text));
        EndPaint (hWnd, &ps);
        return;
}
*/

void colorpaint (HWND hWnd, char *text, int zeile, int spalte,
                 COLORREF vcolor, COLORREF bcolor, HFONT hFont)
/**
Text in Fenster schreiben.
**/
{
        HDC      hdc;
        PAINTSTRUCT ps;
        int x, y;
        int xchar;
        int ychar;
        SIZE size;
        RECT rect;
        SIZE sizes;

        xchar = tm.tmAveCharWidth;;
        ychar = tm.tmHeight;

        x = spalte * xchar;
        y = zeile * ychar;

        hdc = BeginPaint (hWnd, &ps);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        x = spalte * xchar;
        y = zeile * ychar;
        if (hFont)
        {
               SelectObject (hdc, hFont);
               GetTextMetrics (hdc, &tm);
               GetTextExtentPoint32 (hdc, "X", 1, &size);
               tm.tmAveCharWidth = size.cx;
        }
        if (printdir == 1)
// rechtsb�ndig                    
        {
               GetClientRect (hWnd, &rect);
               clipped (text);
               while (*text <= ' ')
               {
                   if (*text == 0) break;
                   text += 1;
               }
               GetTextExtentPoint32 (hdc, text, strlen (text), &sizes);
               x = max (0, rect.right - sizes.cx);
        }
               
        SetBkMode (hdc, OPAQUE);
        SetTextColor (hdc, vcolor);
        SetBkColor (hdc, bcolor);
        TextOut (hdc, x, y, text, strlen (text));
        EndPaint (hWnd, &ps);
        return;
}


void paint_value (HWND hWnd, char *text, int zeile, int spalte,
                 COLORREF color, HFONT hFont)
/**
Text in Fenster schreiben.
**/
{
        HDC      hdc;
        PAINTSTRUCT ps;
        int x, y;
        int xchar;
        int ychar;

        xchar = tm.tmAveCharWidth;;
        ychar = tm.tmHeight;

        x = spalte * xchar;
        y = zeile * ychar;

        hdc = BeginPaint (hWnd, &ps);
        if (hFont)
        {
               SelectObject (hdc, hFont);
               GetTextMetrics (hdc, &tm);
        }
        SetBkMode (hdc, OPAQUE);
        SetBkColor (hdc, color);
        TextOut (hdc, x, y, text, strlen (text));
        EndPaint (hWnd, &ps);
        return;
}

static void PrintIcon (HWND hWnd, HICON hIcon)
/**
Icons am Bildschirm anzeigen.
**/
{
         HDC hdc;
         PAINTSTRUCT ps;
         static TEXTMETRIC tm;

         hdc = BeginPaint (hWnd, &ps);
         DrawIcon (hdc, 0, 0, hIcon);
         EndPaint (hWnd, &ps);
         return;
}

static void PrintColIcon (HWND hWnd, HDC hdc,
                         HICON hIcon, int x, int y)
{
         DrawIcon (hdc, x, y, hIcon);
}


static void PrintBitmap (HWND hWnd, HDC hdc,
                         HBITMAP hbr, int x, int y, DWORD mode)
{
        BITMAP bm;
        HDC    hdcMemory;
        HBITMAP  hbmOld;
        RECT rect;

        GetClientRect (hWnd, &rect);
        GetObject (hbr, sizeof (BITMAP), &bm);

        if (x == -1)
/* Spalte zentrieren               */
        {
                  x = (rect.right - bm.bmWidth) / 2;
        }

        if (y == -1)
/* Zeile zentrieren               */
        {
                  y = (rect.bottom - bm.bmHeight) / 2;
        }

        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        BitBlt (hdc, x, y, bm.bmWidth, bm.bmHeight,
                         hdcMemory,0, 0, mode);
        
        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
}

void SetSpezColor (COLORREF Color, COLORREF BkColor, HDC hdc)
/**
Spezialfarbe fuer Hot-Key-Buchstaben setzen.
**/
{
      int red;
      int blue;
      int green;
      int bkred;
      int bkblue;
      int bkgreen;

      blue = GetBValue (Color);
      red  = GetRValue (Color);
      green = GetGValue (Color);
      bkblue = GetBValue (BkColor);
      bkred  = GetRValue (BkColor);
      bkgreen = GetGValue (BkColor);

      if (red < 255 && bkred < 255)
      {
              SetTextColor   (hdc, REDCOL);
              return;
      }

      if (bkred < 255 && blue > 100 && green > 100)
      {
              SetTextColor   (hdc, REDCOL);
              return;
      }

      if (red == 0 && blue == 0 && green == 0)
      {
              SetTextColor (hdc, BLUECOL);
              return;
      }
                 
      SetTextColor   (hdc, BLACKCOL);
}
       


static void PrintColText (HWND hWnd, HDC hdc, char *text, int x, int y,
                          COLORREF color, COLORREF bkcolor, HFONT hFont)
/**
Text auf Colbutton schreiben.
**/
{
        RECT rect;
        TEXTMETRIC tm ;
        int textlen;
        int textpilen;
        SIZE size;
        int anz;
        char OneChar [2];

        textlen = strlen (text);
        anz = zsplit (text, '&');
        if (anz > 1)
        {
                textlen --;
        }
        GetClientRect (hWnd, &rect);

        if (hFont)
        {
               SelectObject (hdc, hFont);
        }
        GetTextMetrics (hdc, &tm) ;
        GetTextExtentPoint (hdc, text, textlen, &size);
        GetClientRect (hWnd, &rect);
        textpilen = size.cx;
        if (x == -1)
/* Hotizontal zentrieren                             */
        {
                         
                x = max (2, (rect.right - textpilen) / 2);
        }
        else
        {
               x = max (2, x);
        }
        while ((x + size.cx) > (rect.right - 1))
        {
               textlen --;
               text [textlen] = (char) 0;
               if (textlen == 0) break;
               GetTextExtentPoint (hdc, text, textlen, &size);
        }
        if (y == -1)
/* Hotizontal zentrieren                             */
        {
              
                y = max (2, (rect.bottom - size.cy) / 2);
        }
        else
        {
                y = max (2, y);
        }

        SetBkMode (hdc, OPAQUE);
        SetTextColor   (hdc, color);
        SetBkColor (hdc, bkcolor);
        if (anz < 2 && text[0] != '&')
        {
                
                TextOut (hdc, x, y, text, strlen (text));
                return;
        }

        if (anz < 2)
        {
                SetSpezColor (color, bkcolor, hdc);
                OneChar[0] = zwort[1][0];
                OneChar[1] = (char) 0;
                TextOut (hdc, x, y, OneChar, 1);
                GetTextExtentPoint32 (hdc, OneChar, 1, &size);
                x += size.cx;
                SetTextColor   (hdc, color);
                TextOut (hdc, x, y, &zwort [1][1], strlen (zwort[1]) - 1);
                return;
        }
         
        TextOut (hdc, x, y, zwort [1], strlen (zwort[1]));
        GetTextExtentPoint32 (hdc, zwort[1], strlen (zwort[1]), &size);
        x += size.cx;
        SetSpezColor (color, bkcolor, hdc);
        OneChar[0] = zwort[2][0];
        OneChar[1] = (char) 0;
        TextOut (hdc, x, y, OneChar, 1);
        GetTextExtentPoint32 (hdc, OneChar, 1, &size);
        x += size.cx;
        SetTextColor   (hdc, color);
        TextOut (hdc, x, y, &zwort [2][1], strlen (zwort[2]) - 1);
}

void PrintActivRect (HWND hWnd, HDC hdc, COLORREF color)
/**
Bei Colbuttom mit Focus Rechteck in Hintergrundfarbe anzeigen.
**/
{
         HBRUSH hBrush;
         RECT rect;

         GetClientRect (hWnd, &rect);
         rect.top    += 1;
         rect.left   += 1;
         rect.bottom -= 1;
         rect.right  -= 1;

         hBrush = CreateSolidBrush (color);

         SelectObject (hdc, hBrush);
         SelectObject (hdc, GetStockObject (NULL_PEN));
         Rectangle (hdc, rect.left, rect.top, rect.right, rect.bottom);
         DeleteObject (hBrush);
}


void PrintFocusRect (HWND hWnd, HDC hdc, COLORREF color)
/**
Bei Colbuttom mit Focus Rechteck in Hintergrundfarbe anzeigen.
**/
{
         HBRUSH hBrush;
         RECT rect;

         GetClientRect (hWnd, &rect);
         rect.top    += 5;
         rect.left   += 2;
         rect.bottom -= 5;
         rect.right  -= 2;

         hBrush = CreateSolidBrush (color);

         SelectObject (hdc, hBrush);
         SelectObject (hdc, GetStockObject (NULL_PEN));
         Rectangle (hdc, rect.left, rect.top, rect.right, rect.bottom);
         DeleteObject (hBrush);
}

void PrintFocusFrame (HWND hWnd, HDC hdc, COLORREF color)
/**
Bei Colbuttom mit Focus Rechteck in Hintergrundfarbe anzeigen.
**/
{
         HBRUSH hBrush;
         RECT rect;
         int blue;

         blue = GetBValue (color);
         GetClientRect (hWnd, &rect);
         rect.top    += 5;
         rect.left   += 5;
         rect.bottom -= 5;
         rect.right  -= 5;

         if (blue > 120)
         {
                   hBrush = CreateSolidBrush (WHITECOL);
         }
         else
         {
                   hBrush = CreateSolidBrush (BLACKCOL);
         }
         SelectObject (hdc, GetStockObject (PS_DASH));

         FrameRect (hdc, &rect, hBrush);

         DeleteObject (hBrush);
}


void print_colbutton (field *feld, HFONT hFont)
/**
Inhalt von ColButton anzeigen.
**/
{
       ColButton *CuB;
       HDC hdc;
       int HasFocus;
       PAINTSTRUCT ps;
       RECT rect;
       static TEXTMETRIC tm;
       COLORREF Color, BkColor;

       HasFocus = 0;
       if (GetFocus () == feld->feldid)
       {
                    HasFocus = 1;
       }
       GetClientRect (feld->feldid, &rect);
       CuB = (ColButton *) feld->feld;
       hdc = BeginPaint (feld->feldid, &ps);
       if (feld->BuId & 0x8000 || CuB->aktivate == 4 ||
           CuB->aktivate == 6)
       {
                   PressBorder (hdc, &rect);
       }
       else
       {
                   UnPressBorder (hdc, &rect);
       }

       if (HasFocus && CuB->aktivate == 1)
       {
                   Color   = CuB->BkColor;
                   BkColor = CuB->Color;
                   PrintFocusRect (feld->feldid, hdc, BkColor);
       }
       else if (HasFocus && CuB->aktivate == 2)
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
                   PrintFocusFrame (feld->feldid, hdc, BkColor);
       }
       else if (HasFocus && CuB->aktivate == 3)
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
                   PrintFocusFrame (feld->feldid, hdc, BkColor);
       }
       else if (HasFocus && CuB->aktivate == 4)
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
                   PrintFocusFrame (feld->feldid, hdc, BkColor);
       }
       else if (CuB->aktivate == 6 || CuB->aktivate == 7)
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
                   PrintActivRect (feld->feldid, hdc, BkColor);
       }
       else
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
       }

       if (CuB->text1)
       {
                    PrintColText (feld->feldid, hdc, CuB->text1,
                                  CuB->tx1, CuB->ty1,
                                  Color,
                                  BkColor,
                                  hFont);
       }

       if (CuB->text2)
       {
                    PrintColText (feld->feldid, hdc, CuB->text2,
                                  CuB->tx2, CuB->ty2,
                                  Color,
                                  BkColor,
                                  hFont);
       }

       if (CuB->text3)
       {
                    PrintColText (feld->feldid, hdc, CuB->text3,
                                  CuB->tx3, CuB->ty3,
                                  Color,
                                  BkColor,
                                  hFont);
       }

       if (CuB->bmp)
       {
                    PrintBitmap (feld->feldid,
                                 hdc, CuB->bmp, CuB->bmpx,
                                 CuB->bmpy, SRCCOPY);
       }
                    
       if (CuB->icon)
       {
                    PrintColIcon (feld->feldid,
                                 hdc, CuB->icon, CuB->icox,
                                 CuB->icoy);
       }
                    
       EndPaint (feld->feldid, &ps);
}

int disp_colbutton (HWND hWnd, HFONT hFont)
/**
Bei WM_PAINT-Meldung ColButton-Felder neu zeichnen.
**/
{
       int i, fm;
       form *savefrm;

       savefrm = current_form;
       for (fm = 0; fm < fmptr; fm ++)
       {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (hWnd == current_form->mask[i].feldid)
                 {
                           print_colbutton (&current_form->mask[i], hFont);
                 }
            }
        }
        current_form = savefrm;
        return FALSE;
}


int disp_icon (HWND hWnd)
/**
Bei WM_PAINT-Meldung Icon-Feldern neu zeichnen.
**/
{
       int i, fm;
       form *savefrm;
       HICON AktIcon;

       savefrm = current_form;
       for (fm = 0; fm < fmptr; fm ++)
       {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (hWnd == current_form->mask[i].feldid)
                 {
                           memcpy ((char *) &AktIcon,
                                   current_form->mask[i].feld,
                                   sizeof (HICON));
                           PrintIcon (hWnd, AktIcon);
                 }
            }
        }
        current_form = savefrm;
        return FALSE;
}

int DoIconProg (HWND hWnd)
/**
Bei WM_PAINT-Meldung Icon-Feldern neu Zeichnen.
**/
{
       int i, fm;
       form *savefrm;
       int ret;

       savefrm = current_form;
       for (fm = 0; fm < fmptr; fm ++)
       {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (hWnd == current_form->mask[i].feldid)
                 {
                                       
                       if (current_form->mask[i].after != (int (*) ()) 0)
                       {
                               ret = (*current_form->mask[i].after) ();
                       }
                       if (current_form->mask[i].BuId != NULL)
                       {
                               SendMessage (GetParent (hWnd), WM_COMMAND, 
                                       MAKELONG (current_form->mask[i].BuId,
                                                 hWnd),
                                        0l);
                       }
                       current_form = savefrm;
                       return TRUE;
                 }
            }
        }
        current_form = savefrm;
        return FALSE;
}

void DoIcon (field *feld)
/**
Bei WM_PAINT-Meldung Icon-Feldern neu Zeichnen.
**/
{
        int ret;
        int BuId;

        BuId = feld->BuId & 0x3FFF;
     
        if (BuId != NULL)
        {
                SendMessage (GetParent (feld->feldid), WM_COMMAND, 
                                       MAKELONG (feld->BuId,
                                                 feld->feldid),
                                        0l);
        }
        else if (feld->after != (int (*) ()) 0)
        {
                ret = (*feld->after) ();
        }
}

form *GetFeldForm (HWND hWnd)
/**
Feld suchen
**/
{
       int i, fm;
       form *savefrm;
       form *aktfrm;
       field *feld;


       savefrm = current_form;
       feld = NULL;
       for (fm = 0; fm < fmptr; fm ++)
       {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (hWnd == current_form->mask[i].feldid)
                 {
                           aktfrm = current_form;
                           break;
                 }
            }
        }
        current_form = savefrm;
        return aktfrm;
}

COLORREF CuColor;
COLORREF CuBkColor;


void KillFieldCol (field *feld, mfont *font)
/**
Focus fuer Kontrollelement setzen.
**/
{
        ColButton *CuB;  
        HBRUSH newbrush, oldbrush;

        CuB = (ColButton *) feld->feld;
        newbrush = CreateSolidBrush (font->FontBkColor);
        oldbrush = (HGDIOBJ) SetClassLong (feld->feldid,
                                           GCL_HBRBACKGROUND,
                                           (long) newbrush);
        InvalidateRect (feld->feldid, NULL, TRUE);
        UpdateWindow (feld->feldid);
        return;
}

void KillColFocus (HWND hWnd)
/**
Focus fuer Kontrollelement setzen.
**/
{
        ColButton *CuB;  
        field *feld;
        form *savefrm;
        int i, fm;
        static HBRUSH newbrush;
        static COLORREF AktCol = RGB (1, 1, 1);

        feld = NULL;
        savefrm = current_form;
        for (fm = 0; fm < fmptr; fm ++)
        {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (current_form->mask[i].attribut & ICON == 0 &&
                     current_form->mask[i].attribut & COLBUTTON == 0)
                 {
                            continue;
                 }

                 if (hWnd == current_form->mask[i].feldid)
                 {
                          CuB = (ColButton *) current_form->mask[i].feld;
                          InvalidateRect (current_form->mask[i].feldid,
                                          NULL,
                                          TRUE);
                          UpdateWindow (current_form->mask[i].feldid);
                          current_form = savefrm;
                          return;
                 }
            }
        }
        current_form = savefrm;
        return;
}

void SetColFocus (HWND hWnd)
/**
Focus fuer Kontrollelement setzen.
**/
{
        ColButton *CuB;  
        field *feld;
        form *savefrm;
        int i, fm;
        static HBRUSH newbrush;
        static COLORREF AktCol = RGB (1, 1, 1);

        feld = NULL;
        savefrm = current_form;
        for (fm = 0; fm < fmptr; fm ++)
        {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (current_form->mask[i].attribut & ICON == 0 &&
                     current_form->mask[i].attribut & COLBUTTON == 0)
                 {
                            continue;
                 }

                 if (hWnd == current_form->mask[i].feldid)
                 {
                          CuB = (ColButton *) current_form->mask[i].feld;
                          if (CuB->aktivate == FALSE)
                          {
                                        current_form = savefrm;
                                        return;
                          }
                          InvalidateRect (current_form->mask[i].feldid,
                                          NULL,
                                          TRUE);
                          UpdateWindow (current_form->mask[i].feldid);
                          AktColFocus = hWnd;
                          current_form = savefrm;
                          return;
                 }
            }
        }
        current_form = savefrm;
        return;
}

void PressIcon (HWND hWnd)
/**
Kontrollelement Button gedrueckt erzeugen.
**/
{
        int fm;
        form *savefrm;
        form *aktfrm;
        form *frm;
        HWND AktWin;
        field *feld;
        int i;
        int x, y, cx, cy;
        int xchar, ychar;
        TEXTMETRIC tm;
        HDC hdc;

        feld = NULL;
        savefrm = current_form;
        AktWin = AktivWindow;
        for (fm = 0; fm < fmptr; fm ++)
        {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (current_form->mask[i].attribut & ICON == 0)
                 {
                            continue;
                 }

                 if (hWnd == current_form->mask[i].feldid)
                 {
                            feld = &current_form->mask[i];
                            aktfrm = current_form;
                 }
                 else if (current_form->mask[i].attribut & ICON)
                 {
                            current_form->mask[i].BuId &= 0x03FF;
                 }
                 
            }
        }

        if (feld == NULL)
        {
                        current_form = savefrm;
                        return;
        }

        if (feld->attribut & ICON == 0) 
        {
                        current_form = savefrm;
                        return;
        }
        current_form = aktfrm;
        frm = current_form;
        AktWin = GetParent (hWnd);
        DestroyWindow (hWnd);

        hdc = GetDC (AktWin);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (AktWin, hdc);

        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight;
        x = feld->pos[1] * xchar;

        y = ((feld->pos[0])) * ychar;
        if (feld->rows == 0)
        {
               y = y + y / 3;
        }
        if (feld->length > 0)
        {
              cx = feld->length * xchar;
        }
        else
        {
             feld->length = strlen (feld->feld);
             cx = strlen (feld->feld) * xchar;
        }
        if (feld->rows)
        {
             cy = ychar * feld->rows;
        }
        else
        {
             cy = ychar + ychar / 3;
        }
        
        feld->feldid = CreateWindowEx (
                                        WS_EX_CLIENTEDGE, 
                                        "WIcon",
                                        "",
                                        WS_CHILD | WS_VISIBLE,
                                        x, y,
                                        cx, cy,
                                        AktWin,
                                        0,
                                        hMainInst,
                                        NULL);

        feld->BuId &= 0x03FF;
        feld->BuId |= 0x8000;
        ShowControl (feld);
        current_form = savefrm;
        AktivWindow = AktWin;
}

void UnPressIcon (HWND hWnd, int mode)
/**
Kontrollelement Button gedrueckt erzeugen.
**/
{
        int fm;
        form *aktfrm;
        form *savefrm;
        HWND AktWin;
        field *feld;
        form *frm;
        int i;
        int x, y, cx, cy;
        int xchar, ychar;
        TEXTMETRIC tm;
        HDC hdc;

        feld = NULL;
        savefrm = current_form;
        AktWin = AktivWindow;

        for (fm = 0; fm < fmptr; fm ++)
        {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (current_form->mask[i].attribut & ICON);
                 else
                 {
                            continue;
                 }

                 if (hWnd == current_form->mask[i].feldid)
                 {
                            feld = &current_form->mask[i];
                            aktfrm = current_form;
                 }
                 else if (current_form->mask[i].BuId & 0xC000)
                 {
                            current_form->mask[i].BuId &= 0x03FF;
                 }
            }
        }

        if (feld == NULL)
        {
                        current_form = savefrm;
                        return;
        }

        if ((feld->BuId & 0x8000) == 0)
        {
                        current_form = savefrm;
                        return;
        }
        current_form = aktfrm;
        frm = current_form;
        AktWin = GetParent (hWnd);
        DestroyWindow (hWnd);

        hdc = GetDC (AktWin);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (AktWin, hdc);

        xchar = tm.tmAveCharWidth;
        ychar = tm.tmHeight;

        x = feld->pos[1] * xchar;

        y = ((feld->pos[0])) * ychar;
        if (feld->rows == 0)
        {
               y = y + y / 3;
        }
        if (feld->length > 0)
        {
              cx = feld->length * xchar;
        }
        else
        {
             feld->length = strlen (feld->feld);
                          cx = strlen (feld->feld) * xchar;
        }
        if (feld->rows)
        {
             cy = ychar * feld->rows;
        }
        else
        {
             cy = ychar + ychar / 3;
        }
        
         feld->feldid = CreateWindow (
                                        "WIcon",
                                        "",
                                        WS_CHILD | WS_VISIBLE |
                                        BS_OWNERDRAW | WS_DLGFRAME,,
                                        x, y,
                                        cx, cy,
                                        AktWin,
                                        0,
                                        hMainInst,
                                        NULL);

        if (mode)
        {
               feld->BuId &= 0x3FFF;
               DoIcon (feld);
        }
        else
        {
               feld->BuId &= 0x3FFF;
               feld->BuId |= 0x4000;
        }
        frm = current_form;
        DestroyWindow (hWnd);

        ShowControl (feld);
        current_form = savefrm;
        AktivWindow = AktWin;
}

static COLORREF aktback;
static COLORREF aktvor;


void BuColMinusA (ColButton *Cub)
/**
**/
{
         aktvor  = Cub->Color; 
         aktback = Cub->BkColor; 
         Cub->Color = BLACKCOL;
         Cub->BkColor = YELLOWCOL;
}

void BuColPlusA (ColButton *Cub)
/**
**/
{
         Cub->Color = aktvor;
         Cub->BkColor = aktback;
}


void BuColMinus (ColButton *Cub)
/**
**/
{
         static int minus = 120;
         int blue, green, red;

         blue = GetBValue (Cub->BkColor);
         red  = GetRValue (Cub->BkColor);
         green = GetGValue (Cub->BkColor);
         if (blue > minus) blue -= minus;
         if (red > minus) red -= minus;
         if (green > minus) green -= minus;
         Cub->BkColor = RGB (red, green, blue);
}

void BuColPlus (ColButton *Cub)
/**
**/
{
         static int plus = 120;
         int blue, green, red;

         blue = GetBValue (Cub->BkColor);
         red  = GetRValue (Cub->BkColor);
         green = GetGValue (Cub->BkColor);
         if (blue > 0) blue += plus;
         if (red > 0) red += plus;
         if (green > 0) green += plus;
         Cub->BkColor = RGB (red, green, blue);
}

void BuColMinusH (ColButton *Cub)
/**
**/
{
         static int minus = 50;
         int blue, green, red;

         blue = GetBValue (Cub->BkColor);
         red  = GetRValue (Cub->BkColor);
         green = GetGValue (Cub->BkColor);
         if (blue + minus < 255)  blue += minus;
         if (red + minus < 255)   red += minus;
         if (green + minus < 255) green += minus;
         Cub->BkColor = RGB (red, green, blue);
}


void BuColPlusH (ColButton *Cub)
/**
**/
{
         static int plus = 50;
         int blue, green, red;

         blue = GetBValue (Cub->BkColor);
         red  = GetRValue (Cub->BkColor);
         green = GetGValue (Cub->BkColor);
         if (blue < 255) blue -= plus;
         if (red < 255) red -= plus;
         if (green < 255) green -= plus;
         Cub->BkColor = RGB (red, green, blue);
}

void InitAktiveCol (form *frm)
/**
**/
{
          int i;
          ColButton *CuB;


          for (i = 0; i < frm->fieldanz - 1; i ++)
          {
                    CuB = (ColButton *) frm->mask[i].feld;
                    if (CuB->aktivate == 6 || CuB->aktivate == 7)
                    {
                               BuColPlusA (CuB);
                               CuB->aktivate = 5;
                    }
          }
}


void UnActivate (form *frm)
/**
**/
{
        int i;
        ColButton *Cub;

        for (i = 0; i < frm->fieldanz; i ++)
        {
                  Cub = (ColButton *) frm->mask[i].feld;
                  if (Cub->aktivate == 6 ||
                      Cub->aktivate == 7)
                  {
                       frm->mask[i].BuId &= 0x03FF;
                       Cub->aktivate = 5;
                       BuColPlusA (Cub);
                       ShowControl (&frm->mask[i]);
                  }
        }
}

static HWND aktColhWnd = NULL;

void UnsetAktCol (form *frm, HWND hWnd)
/**
Farbe zuruecksetzen.
**/
{
        int i;
        for (i = 0; i < frm->fieldanz; i ++)
        {
                   if (frm->mask[i].feldid == hWnd)
                   {
                                break;
                   }
        }
        if (i < frm->fieldanz) return; 
        UnActivate (frm);
        aktColhWnd = NULL;
}


void SetAktCol (HWND hWnd)
/**
Farbe setzen, wenn die Maus ueber dem Colbutton ist.
**/
{
        int fm;
        form *savefrm;
        int brstat;
        field *feld;
        int i;
        ColButton *Cub;

        if (hWnd == aktColhWnd) return;

        savefrm = current_form;
        brstat = 0;
        feld = NULL;
        for (fm = 0; fm < fmptr; fm ++)
        {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (current_form->mask[i].attribut & COLBUTTON == 0)
                 {
                            continue;
                 }

                 if (hWnd == current_form->mask[i].feldid)
                 {
                            feld = &current_form->mask[i];
                            Cub = (ColButton *) feld->feld;
                            if (Cub->aktivate != 5)
                            {
                                         feld = NULL;
                            }
                            brstat = 1;
                            break;
                 }
            }
            if (brstat) break;
        }
        if (feld == NULL)
        {
                        current_form = savefrm;
                        return;
        }
        UnActivate (current_form);
        Cub->aktivate = 7;
        BuColMinusA (Cub);
        aktColhWnd = hWnd;
        ShowControl (feld);
        current_form = savefrm;
}

void PressControl (HWND hWnd, char *wclass)
/**
Kontrollelement Button gedrueckt erzeugen.
**/
{
        int fm;
        form *savefrm;
        form *aktfrm;
        HWND AktWin;
        field *feld;
        int i;
        ColButton *Cub;

        feld = NULL;
        savefrm = current_form;
        AktWin = AktivWindow;
        for (fm = 0; fm < fmptr; fm ++)
        {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (current_form->mask[i].attribut & ICON == 0 &&
                     current_form->mask[i].attribut & COLBUTTON == 0)
                 {
                            continue;
                 }

                 if (hWnd == current_form->mask[i].feldid)
                 {
                            feld = &current_form->mask[i];
                            aktfrm = current_form;
                 }
                 else if (current_form->mask[i].attribut & ICON ||
                          current_form->mask[i].attribut & COLBUTTON)
                 {
                            current_form->mask[i].BuId &= 0x03FF;
                 }
                 
            }
        }

        if (feld == NULL)
        {
                        current_form = savefrm;
                        return;
        }

        if (feld->attribut & ICON == 0 && feld->attribut & COLBUTTON == 0) 
        {
                        current_form = savefrm;
                        return;
        }

        Cub = (ColButton *) feld->feld;
        if (Cub->aktivate < 3 || Cub->aktivate == 10)
        {
                       feld->BuId &= 0x03FF;
                       feld->BuId |= 0x8000;
        }
        else if (Cub->aktivate == 3)
        {
                       feld->BuId &= 0x03FF;
                       feld->BuId |= 0x8000;
                       Cub->aktivate = 4;
                       DoIcon (feld);
        }
        else if (Cub->aktivate == 5)
        {
                       feld->BuId &= 0x03FF;
                       feld->BuId |= 0x8000;
                       BuColMinusA (Cub);
                       UnActivate (aktfrm);
                       Cub->aktivate = 6;
                       DoIcon (feld);
        }
        else if (Cub->aktivate == 7)
        {
                       feld->BuId &= 0x03FF;
                       feld->BuId |= 0x8000;
                       UnActivate (aktfrm);
                       Cub->aktivate = 6;
                       DoIcon (feld);
        }
        else if (Cub->aktivate == 4)
        {
                       feld->BuId &= 0x03FF;
                       feld->BuId |= 0x8000;
                       Cub->aktivate = 3;
        }

        if (Cub->aktivate == 10 && TimerOn == 0)
        {
                       DoIcon (feld);
                       TimehWnd = feld->feldid;
                       SetTimer (TimehWnd, 1, 500, 0);  
        }
        else if (Cub->aktivate == 10)
        {
                       if (TimerOn == 1)
                       {
                                 KillTimer (TimehWnd, 1);  
                                 SetTimer (TimehWnd, 1, 100, 0);
                                 TimerOn = 2;
                                 DoIcon (feld);
                       }
        }
        if (TimerOn == 0)
        {
                  ShowControl (feld);
        }
        if (Cub->aktivate == 10)
        {
                  TimerOn = 1;
        }

        current_form = savefrm;
        AktivWindow = AktWin;
}

void UnPressControl (HWND hWnd, char *wclass, int mode)
/**
Kontrollelement Button losgelassen erzeugen.
**/
{
        int fm;
        form *aktfrm;
        form *savefrm;
        HWND AktWin;
        field *feld;
        int i;
        ColButton *Cub;

        feld = NULL;
        savefrm = current_form;
        AktWin = AktivWindow;

        for (fm = 0; fm < fmptr; fm ++)
        {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (current_form->mask[i].attribut & ICON);
                 else if (current_form->mask[i].attribut & COLBUTTON);
                 else
                 {
                            continue;
                 }

                 if (hWnd == current_form->mask[i].feldid)
                 {
                            feld = &current_form->mask[i];
                            aktfrm = current_form;
                 }
                 else if (current_form->mask[i].BuId & 0xC000)
                 {
                            current_form->mask[i].BuId &= 0x03FF;
                 }
            }
        }

        if (feld == NULL)
        {
                        current_form = savefrm;
                        return;
        }


        if ((feld->BuId & 0x8000) == 0)
        {
                        current_form = savefrm;
                        return;
        }
        current_form = aktfrm;
        Cub = (ColButton *) feld->feld;

        if (Cub->aktivate == 4);
        else if (Cub->aktivate == 6);
        else if (mode)
        {
               feld->BuId &= 0x3FFF;
               if (Cub->aktivate != 10 && mode != 2)
               {
                          DoIcon (feld);
               }
               else if (TimerOn)
               {
                          KillTimer (TimehWnd, 1);
                          TimerOn = FALSE;
                          TimehWnd = NULL;
               }
        }
        else
        {
               feld->BuId &= 0x3FFF;
               feld->BuId |= 0x4000;
        }
        ShowControl (feld);
        current_form = savefrm;
        AktivWindow = AktWin;
}

void TestIconsPressed (HWND hWnd)
/**
Testen, ob ein gedrueckter Icon-Button verlassen wird.
**/
{
        int fm;
        form *savefrm;
        int i;

        savefrm = current_form;
        for (fm = 0; fm < fmptr; fm ++)
        {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (current_form->mask[i].BuId & 0x8000 &&
                     hWnd != current_form->mask[i].feldid)
                 {
                      if (current_form->mask[i].attribut & COLBUTTON)
                      {
                           UnPressControl(current_form->mask[i].feldid,
                                          "ColButton",
                                           0);

                      }
                      else if (current_form->mask[i].attribut & ICON)
                      {
                           UnPressIcon (current_form->mask[i].feldid,
                                        0);
                      }
                      break;
                 }
                 else if (current_form->mask[i].BuId & 0x4000 &&
                          hWnd == current_form->mask[i].feldid)
                 {
                      if (current_form->mask[i].attribut & COLBUTTON)
                      {
                           PressControl(current_form->mask[i].feldid,
                                       "ColButton");
                      }
                      else if (current_form->mask[i].attribut & ICON)
                      {
                           PressIcon(current_form->mask[i].feldid);
                                       
                      }
                      break;
                 }
          }
       }
       current_form = savefrm;
}

LONG FAR PASCAL ColButtonProc (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
        static HFONT hFont = 0;
        POINT mousepos;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_KEYDOWN :
                       PostMessage (GetParent (hWnd), msg, wParam, lParam);
                       return 0;
              case WM_PAINT :
                       hFont = GetHwFont (hWnd);
                       if (disp_colbutton (hWnd, hFont)) break;
                       break;
              case WM_SETFONT :
                       hFont = (HFONT) wParam;
                       SetHwFont (hWnd, hFont);
                       return 0;
              case WM_LBUTTONDOWN :
                       SetFocus (hWnd);
                       PressControl (hWnd, "ColButton");
                       MousePressed = TRUE;
                       SetTimer (hWnd, 2, 10, 0);
                       return 0;
              case WM_LBUTTONUP :
                       UnPressControl (hWnd,"ColButton", 1);
                       MousePressed = FALSE;
                       KillTimer (hWnd, 2);
                       return 0;
              case WM_TIMER :
                       switch (wParam)
                       {
                                case 1:
                                   PressControl (hWnd, "ColButton");
                                   break;
                                case 2 :
                                   if (!MouseinWindow (hWnd, &mousepos))
                                   {
                                         UnPressControl (hWnd,"ColButton", 2);
                                         MousePressed = FALSE;
                                         MousehWnd = hWnd;
                                                      
                                    }
                                    if (GetKeyState (VK_LBUTTON) >= 0)
                                    {
                                         if (hWnd == MousehWnd)
                                         {
                                                   KillTimer (hWnd, 2); 
                                                   MousehWnd = NULL;
                                         }
                                    }
                                    break;
                       }                     
                       return 0;
              case WM_MOUSEMOVE :
                       if (hWnd == MousehWnd)
                       {
                             if (GetKeyState (VK_LBUTTON) < 0)
                             {
                                    KillTimer (hWnd, 2); 
                                    MousehWnd = NULL;
                                    SendMessage (hWnd, WM_LBUTTONDOWN,
                                                 wParam, lParam);
                             }
                       }
                       break;
              case WM_SETFOCUS :
                       SetColFocus (hWnd);
                       return 0;
              case WM_KILLFOCUS :
                       KillColFocus (hWnd);
                       return 0;
              case WM_DESTROY :
                       KillTimer (hWnd, 2);
                       if (TimerOn && TimehWnd == hWnd)
                       {
                                    KillTimer (TimehWnd, 1);
                                    TimerOn = 0;
                                    TimehWnd = NULL;
                       }
                       if (MousehWnd == hWnd)
                       {
                                    MousehWnd = NULL;
                       }
                       break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


LONG FAR PASCAL IconProc (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
        static HFONT hFont = 0;

        switch(msg)
        {
              case WM_PAINT :
              {
                       if (disp_icon (hWnd)) break;
                       break;
              }
              case WM_LBUTTONDOWN :
              {
                       PressIcon (hWnd);
                       MousePressed = TRUE;
                       break;
              }
              case WM_LBUTTONUP :
              {
                       UnPressIcon (hWnd, 1);
                       MousePressed = FALSE;
                       break;
              }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

int disp_rdcol (HWND hWnd, HFONT hFont)
/**
Bei WM_PAINT-Meldung Readonly-Feldern neu Zeichnen.
**/
{
       int i, fm;
       form *savefrm;

       savefrm = current_form;
       for (fm = 0; fm < fmptr; fm ++)
       {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (hWnd == current_form->mask[i].feldid)
                 {
                            ToFormat (buffer, &current_form->mask[i]);
                            strcpy (current_form->mask[i].feld, buffer);
                            printdir = 0;
                            if (strchr (current_form->mask[i].picture, '%'))
                            {
                                printdir = 1;
                            }
                            if (current_form->font &&
                                      current_form->font->FontColor != -1)
                            {
                                       colorpaint
                                          (current_form->mask[i].feldid,
                                           current_form->mask[i].feld,
                                           0,0,
                                           current_form->font->FontColor,
                                           current_form->font->FontBkColor,
                                           hFont);
                             }
                             else
                             {
                                       colorpaint (current_form->mask[i].feldid,
                                           current_form->mask[i].feld,
                                           0,0,
                                           GRAYCOL,
                                           RdonlyCol,
                                           hFont);
                             }
                 }
            }
        }
        current_form = savefrm;
        return FALSE;
}

LONG FAR PASCAL RdColProc (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
        static HFONT hFont = 0;

        switch(msg)
        {
              case WM_PAINT :
              {
                       hFont = GetHwFont (hWnd);
                       if (disp_rdcol (hWnd, GetHwFont (hWnd))) break;
                       break;
              }
              case WM_SETFONT :
                       hFont = (HFONT) wParam;
                       SetHwFont (hWnd, hFont);
                       return 0;
               case WM_DESTROY :
                       DelHwFont (hWnd);
                       break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


int disp_rdonly (HWND hWnd, HFONT hFont)
/**
Bei WM_PAINT-Meldung Readonly-Feldern neu Zeichnen.
**/
{
       int i, fm;
       form *savefrm;
       char classname [64];
       int color;

       savefrm = current_form;
       for (fm = 0; fm < fmptr; fm ++)
       {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (hWnd == current_form->mask[i].feldid)
                 {
                            ToFormat (buffer, &current_form->mask[i]);
                            strcpy (current_form->mask[i].feld, buffer);
                            printdir = 0;
                            if (strchr (current_form->mask[i].picture, '%'))
                            {
                                printdir = 1;
                            }
                            if (ListEWindow)
                            {
                                  GetClassName (hWnd, classname, 64);
                                  if (strcmp (classname, "LReadonly") == 0)
                                  {
                                        color = WHITECOL;
                                  }
                                  else
                                  {
                                        color = LTGRAYCOL;
                                  }
                                  colorpaint (current_form->mask[i].feldid,
                                        current_form->mask[i].feld,
                                        0,0,
                                        GRAYCOL,
                                        color,
                                        hFont);
                            }
                            else
                            {
                                  if (current_form->font &&
                                      current_form->font->FontColor != -1)
                                  {
                                       colorpaint
                                          (current_form->mask[i].feldid,
                                           current_form->mask[i].feld,
                                           0,0,
                                           current_form->font->FontColor,
                                           current_form->font->FontBkColor,
                                           hFont);
                                  }
                                  else
                                  {
                                       colorpaint (current_form->mask[i].feldid,
                                           current_form->mask[i].feld,
                                           0,0,
                                           RdonlyCol,
                                           RdonlyBkCol,
                                           hFont);
                                   }
                            }
                 }
            }
        }
        current_form = savefrm;
        return FALSE;
}

         
LONG FAR PASCAL RdOnlyProc (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
        static HFONT hFont = 0;

        switch(msg)
        {
              case WM_PAINT :
              {
                       hFont = GetHwFont (hWnd);
                       if (disp_rdonly (hWnd, GetHwFont (hWnd))) break;
                       break;
              }
              case WM_SETFONT :
                       hFont = (HFONT) wParam;
                       SetHwFont (hWnd, hFont);
                       return 0;
               case WM_DESTROY :
                       DelHwFont (hWnd);
                       break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

int disp_static (HWND hWnd, HFONT hFont)
/**
Bei WM_PAINT-Meldung Statische Texte neu Zeichnen.
**/
{
       int i, fm;
       form *savefrm;
       char classname [64];
       int color;

       savefrm = current_form;


       for (fm = 0; fm < fmptr; fm ++)
       {
           current_form = frmstack [fm];

           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (hWnd == current_form->mask[i].feldid)
                 {
                            printdir = 0;
                            if (strchr (current_form->mask[i].picture, '%'))
                            {
                                printdir = 1;
                            }
                            if (ListEWindow)
                            {
                                  GetClassName (hWnd, classname, 64);
                                  if (strcmp (classname, "LStatic") == 0)
                                  {
                                        color = WHITECOL;
                                  }
                                  else
                                  {
                                        color = LTGRAYCOL;
                                  }
                                  paint_value (current_form->mask[i].feldid,
                                        current_form->mask[i].feld,
                                        0,0,
                                        color,
                                        hFont);
                            }
                            else
                            {
                                  if (current_form->font &&
                                      current_form->font->FontColor != -1)
                                  {
                                       colorpaint
                                          (current_form->mask[i].feldid,
                                           current_form->mask[i].feld,
                                           0,0,
                                           current_form->font->FontColor,
                                           current_form->font->FontBkColor,
                                           hFont);
                                  }
                                  else
                                  {
                                        paint_value
                                              (current_form->mask[i].feldid,
                                               current_form->mask[i].feld,
                                               0,0,
//                                               LTGRAYCOL,
                                               GetSysColor (COLOR_3DFACE),
                                               hFont);
                                  }
                            }
                 }
            }
        }
        current_form = savefrm;
        return FALSE;
}


LONG FAR PASCAL StaticProc (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
        static HFONT hFont = 0;

        switch(msg)
        {
              case WM_PAINT :
              {
                       hFont = GetHwFont (hWnd);
                       if (disp_static (hWnd, GetHwFont (hWnd))) break;
                       break;
              }
               case WM_SETFONT :
                       hFont = (HFONT) wParam;
                       SetHwFont (hWnd, hFont);
                       return 0;
               case WM_DESTROY :
                       DelHwFont (hWnd);
                       break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


LONG FAR PASCAL EnterDialogBoxProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {
              case WM_COMMAND :
                    if (LOWORD (wParam) == BT_OK)
                    {
                            syskey = KEYCR;
                            break;
                    }
                    else if (LOWORD (wParam) == BT_CANCEL)
                    {
                            syskey = KEY5;
                            DestroyWindow (hListBox);
                            break;
                    }
                    return 0;
              case WM_DESTROY :
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void ButtonAction (void)
/**
Aktion auf gewaehltem Button.
**/
{
        if (current_form->mask[currentfield].after)
        {
                        (*current_form->mask[currentfield].after) ();
                        return;
        }
}

LONG FAR PASCAL EnterButtonBoxProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_COMMAND :
                    if (AktivCbox) current_form = AktivCbox;
                    BtoCurrent ();
                    ButtonAction ();
                    return TRUE;
              case WM_DESTROY :
                    currentbutton = -1;
                    break_enter ();
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void writelist ()
{
        int i;
        int j;
        int pos;
        char buffer [80];
        char *structsave;

        structsave = (char *) GlobalAlloc (GMEM_FIXED | GMEM_ZEROINIT,
                              listdim);
        memcpy (structsave, liststruct, listdim);

        writelog ("menue");
        for (i = 0; i < menue.menAnz; i ++)
        {
                    writelog ("%d %s", i, menue.menArr[i]);
        }

        writelog ("listtab");

        for (i = 0, pos = 0; i < menue.menAnz; i ++, pos += listdim)
        {
                    memcpy (liststruct, &listtab[pos], listdim);
                    sprintf (buffer, "%s", listform->mask[0].feld);
                    for (j = 1; j < listform->fieldanz; j ++)
                    {
                                strcat (buffer, " ");
                                strcat (buffer, listform->mask[j].feld);
                    }
                    writelog ("%d %s", i, buffer);
         }
         memcpy (liststruct, structsave, listdim);
         GlobalFree (structsave);
}

field *GetFormFeld (HWND hWnd)
/**
Feld suchen
**/
{
       int i, fm;
       form *savefrm;
       field *feld;


       savefrm = current_form;
       feld = NULL;
       for (fm = 0; fm < fmptr; fm ++)
       {
           current_form = frmstack [fm];
           for (i = 0; i < current_form->fieldanz; i ++)
           {
                 if (hWnd == current_form->mask[i].feldid)
                 {
                           feld = &current_form->mask[i];
                           break;
                 }
            }
        }
        current_form = savefrm;
        return feld;
}
