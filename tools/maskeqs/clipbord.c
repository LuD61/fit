#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include "clipboard.h"

static char *CpBuffer = NULL;
static char *CpBSeek = NULL;

void ToClipboard (char *txt)
{
          HGLOBAL hb;
          LPVOID p;
          char *text;

		  text = txt;
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (NULL);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

char * FromClipboard (void)
{
          HGLOBAL hb;
          LPVOID p;
          char *text;

          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (NULL);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          text = malloc (strlen (p) + 1);
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          return text;
}

char *CpBopen (void)
{
          if (CpBuffer)
          {
              free (CpBuffer);
          }
          CpBuffer = FromClipboard ();
          CpBSeek = CpBuffer;
          return CpBuffer;
}

char *CpBreopen (void)
{
          CpBSeek = CpBuffer;
          return CpBuffer;
}

char *CpBfgets (char *buffer, int len, char *CpB)
{
          int i;
 
          if (CpBSeek == NULL) return NULL;

          for (i = 0; i < len; i ++, CpBSeek += 1)
          {
              if (*CpBSeek == (char) 10) 
              {
                  CpBSeek += 1;
                  break;
              }
              if (*CpBSeek == (char) 13) 
              {
                  CpBSeek += 2;
                  break;
              }
              if (*CpBSeek == (char) 0) 
              {
                  CpBSeek = NULL;
                  break;
              }
              buffer [i] = *CpBSeek;
          }
          buffer [i] = 0;
          return buffer;
}

void CpBclose (void)
{
          if (CpBuffer)
          {
              free (CpBuffer);
          }

          CpBSeek = NULL;
          CpBuffer = NULL;
}

