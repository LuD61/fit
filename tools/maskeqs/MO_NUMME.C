#include <sqlhdr.h>
extern _SQCURSOR *_iqnprep();

#if !defined(__STDC__)
#define const 
#endif

static const char *_Cn5 = "ins_auto";
static const char *_Cn4 = "del_auto_nr";
static const char *_Cn3 = "upd_auto_nr";
static const char *_Cn2 = "recread_nv";
static const char *_Cn1 = "cuu_nv";
#line 1 "mo_numme.ec"
#ifndef SINIX
#ident  "@(#) mo_nummer.rc	VLUGLOTSA	03.06.94	by RO"
#endif
/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Geisental 12, 4630 Bochum 1, Tel: 0234/5065-250
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_nummer
-
-	Autor			:	W.Roth
-	Erstellungsdatum	:	10.11.92
-
-	Projekt			:	BWS
-	Version			:	3.01
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386
-	Sprache			:       esqlc
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:  	Prozeduren zur automatischen
-					Nummernvergabe.
-
-	Holen einer Nummer aus der Nummernverwaltung
-
-	nveinid:
-	Einstellen nicht benoetigter Nummern in die Nummernverwaltung
-	als Freinummern
-
-	nvdelid:
-	Loeschen aller Saetze einer Nummer aus der Nummernverwaltung
-
-	nvanmid:
-	Neuanmelden einer Nummer in der Nummernverwaltung
-
-	nvanmprf:
-	Anmelden einer Nummer in der Nummernverwaltung mit max. Nummerngroesse;
-	Damit ist beim Holen einer so angemeldeten Nummer eine Pruefung
-	moeglich, ob die Nummer ueberlaeuft
-
-	nvsimpel:
-	Holen einer Nummer aus der Nummernverwaltung;
-	einfache Parameterversion;
-	Wird ein Nummername fuer mdn/fil nicht gefunden, wird dieser mit
-	Standardwerten angelegt.
-	
-	nvglobal:
-	Holen einer Nummer aus der Nummernverwaltung;
-	einfache Parameterversion;
-	Wird ein Nummername fuer mdn/fil nicht gefunden, wird dieser mit
-	mit den Werten aus der PTAB mit Nummername angelegt.
-
------------------------------------------------------------------------------
*/

/* 
 * $include sqlca;
 */
#line 67 "mo_numme.ec"
#line 67 "mo_numme.ec"
#line 1 "D:/INFORMIX/incl/esql/sqlca.h"
/***************************************************************************
 *
 *			   INFORMIX SOFTWARE, INC.
 *
 *			      PROPRIETARY DATA
 *
 *	THIS DOCUMENT CONTAINS TRADE SECRET DATA WHICH IS THE PROPERTY OF
 *	INFORMIX SOFTWARE, INC.  THIS DOCUMENT IS SUBMITTED TO RECIPIENT IN
 *	CONFIDENCE.  INFORMATION CONTAINED HEREIN MAY NOT BE USED, COPIED OR
 *	DISCLOSED IN WHOLE OR IN PART EXCEPT AS PERMITTED BY WRITTEN AGREEMENT
 *	SIGNED BY AN OFFICER OF INFORMIX SOFTWARE, INC.
 *
 *	THIS MATERIAL IS ALSO COPYRIGHTED AS AN UNPUBLISHED WORK UNDER
 *	SECTIONS 104 AND 408 OF TITLE 17 OF THE UNITED STATES CODE.
 *	UNAUTHORIZED USE, COPYING OR OTHER REPRODUCTION IS PROHIBITED BY LAW.
 *
 *
 *  Title:	sqlca.h
 *  Sccsid:	@(#)sqlca.h	9.4	1/18/93  11:09:48
 *  Description:
 *		SQL Control Area
 *
 ***************************************************************************
 */

#ifndef SQLCA_INCL
#define SQLCA_INCL

#ifdef __BORLANDC__
#pragma option -a8
#else
#pragma pack (8)
#endif

#ifdef __cplusplus
extern "C" {
#endif

struct sqlca_s
{
    long sqlcode;
    char sqlerrm[72]; /* error message parameters */
    char sqlerrp[8];
    long sqlerrd[6];
		    /* 0 - estimated number of rows returned */
		    /* 1 - serial value after insert or  ISAM error code */
		    /* 2 - number of rows processed */
		    /* 3 - estimated cost */
		    /* 4 - offset of the error into the SQL statement */
		    /* 5 - rowid after insert  */
#ifndef _FGL_
    struct sqlcaw_s
	{
	char sqlwarn0; /* = W if any of sqlwarn[1-7] = W */
	char sqlwarn1; /* = W if any truncation occurred or
				database has transactions */
	char sqlwarn2; /* = W if a null value returned or
				ANSI database */
	char sqlwarn3; /* = W if no. in select list != no. in into list or
				turbo backend */
	char sqlwarn4; /* = W if no where clause on prepared update, delete or
				incompatible float format */
	char sqlwarn5; /* = W if non-ANSI statement */
	char sqlwarn6; /* = W if server is in data replication secondary mode */
	char sqlwarn7; /* reserved */
	} sqlwarn;
#else
	char sqlawarn[8];
#endif
};

#define SQLNOTFOUND 100

#ifdef __cplusplus
}
#endif

#ifdef __BORLANDC__
#pragma option -a-
#else
#pragma pack ()
#endif

#endif /* SQLCA_INCL */
#line 84 "D:/INFORMIX/incl/esql/sqlca.h"
#line 68 "mo_numme.ec"

#include "stdio.h"
#include "sqlam.h"
#define sqlstatus sqlca.sqlcode
#define sqlerror  sqlca.sqlerrd

/*--- Export Section
export
	proc nvholid
	proc nveinid
	proc nvdelid
	proc nvanmid
	proc nvanmprf
	proc nvsimpel
	proc nvglobal
 $  struct taautonum {
} taautonum, taautonum_null;
*/

/*
 * $char sql[];
 */
#line 87 "mo_numme.ec"
char sql[];
/*
 *  $  struct auto_nr {
 *    short     mdn;
 *    short     fil;
 *    char      nr_nam[11];
 *    long      nr_nr;
 *    short     satz_kng;
 *    long      max_wert;
 *    char      nr_char[11];
 *    long      nr_char_lng;
 *    char      fest_teil[11];
 *    char      nr_komb[21];
 *    short     delstatus;
 * } auto_nr, auto_nr_null;
 */
#line 88 "mo_numme.ec"
struct auto_nr
  {
    short mdn;
    short fil;
    char nr_nam[11];
    long nr_nr;
    short satz_kng;
    long max_wert;
    char nr_char[11];
    long nr_char_lng;
    char fest_teil[11];
    char nr_komb[21];
    short delstatus;
  }  auto_nr, auto_nr_null;

/*--- Lokale Definitionen Modul ---*/
struct auto_nr taautonum;

/*-----------------------------------*/
/*--- externe P R O C E D U R E N ---*/
/*-----------------------------------*/

/*---------------------------------------------------------------------------
-									
-	Procedure	: nvholid
-
-	In		: dmdn		smallint	Mandantennummer
-			  dfil		smallint	Filialnummer
-			  dnr_nam	char(10)	Name der Nummer
-
-	Out		: sqlstatus/Errorcode
-
-	Errorcodes	: Anmerkung
-	---------------------------------------------------------------------
-	-250		: Nummernspeicher fuer diesen Namen durch anderen
-			  Prozess gelockt
-	-1		: Ueberlauf festgestellt; Nummer > Maximalwert
-	-2		: Unzulaessige Satzkennung in auto_nr
 $  struct taautonum {
} taautonum, taautonum_null;
-	100		: Nummer mit diesem Namen nicht in NV
-
-----------------------------------------------------------------------------
-			
-	Beschreibung	:	holen einer automaisch erzeugten Nummer
-				und der	dazugehoerigen Daten
-
-	In der NV gibt es 2 Satzarten:
-	- satz_kng = 1 => Freinummern
-	- satz_kng = 2 => Nummernspeicher
-	
-	Beim Holen erfolgt der Zugriff ueber die folgende Indexsortierung:
-	nr_nam
-	satz_kng
-	nr_nr
-	mdn
-	fil
-	=> Dieser und nur dieser Index ist Bestandteil der Source !
-
-	Das bedeutet, dass falls vorhanden, eine Freinummer geholt wird.
-	Die Nummern werden also immer aufsteigend vergeben.
-
-	Freinummern koennen nur existieren, wenn es fuer die entspr. Nummer
-	(Nummernamen) einen Nummernspeicher (satz_kng = 2) gibt.
-
-	Ist keine Freinummer vorhanden, wird eine Nummer aus dem
-	Nummernspeicher vergeben.
-
-	Geholte Nummern gelten sofort als vergeben.
-	Handelt es sich um eine Freinummer, wird diese geloescht.
-	Handelt es sich um eine Nummer aus dem Nummernspeicher wird dieser
-	inkrementiert.
-
-	Stellt sich heraus, dass eine geholte Nummer doch nicht gebraucht wird,
-	kann sie in die NV als Freinummer eingestellt werden. (NVEINID)
-
--------------------------------------------------------------------------------
*/

int nvholid (dmdn, dfil, dnr_nam)
short dmdn;
short dfil;
char *dnr_nam;
{

        short dsql1;
        short dsql2;
        short dsql3;

        short dreturn;

	dsql1 = 0;
	dsql2 = 0;
	dsql3 = 0;
	dreturn = 0;

	taautonum = auto_nr_null;
	auto_nr   = auto_nr_null;

	/*--- Update-Cursor preparieren ---*/
	        dsql1 = prepcuu_nv (dnr_nam, dmdn, dfil);

	if (dsql1 == 0)
        {

		/*--- Satz aus NV holen ---*/
		dsql2 = fetchcuu_nv ();
		switch (dsql2)
                {
			case 0 :
				/*--- Satz gefunden ---*/
				switch (auto_nr.satz_kng)
                                {
					case 1 :
						/*--- Freinummer ---*/
						dsql3 = freinummer ();
						break;
					case 2 :
						/*--- Nummernspeicher ---*/
						dsql3 =  nummernspeicher ();
						break;
					default :
						/*--- unzulaessige Satzkennung;
						      bedeutet Inkonsistenz in
						      der NV, die eigentlich
						      nicht vorkommen kann.
						---*/
						dsql3 = -2;
						break;
				} /* of switch */
				dreturn = dsql3;
				break;

			case 100 :
				/*--- kein Satz fuer angegebene Nummer in NV
				---*/
			case -250 :
				/*--- Satz ist bereits von anderem Prozess
				      gelockt
				---*/
			case -255 :
				/*--- Transaktion um NV ist nicht in Ordnung
				---*/
			default :
				/*--- wasweissich fuer schweinische SQL-Fehler
				---*/
				dreturn = dsql2;
				break;
		} /* of switch */

		closecuu_nv ();
        }
	else
        {
		/*--- SQL-Fehler bei Cursorpreparierung ---*/
		dreturn = dsql1;
	} /* if */

	return(dreturn);
}

/*---------------------------------------------------------------------------
-									
-	Procedure	: nveinid
-
-	In		: dmdn		smallint	Mandantennummer
-			  dfil		smallint	Filialnummer
-			  dnr_nam	char(10)	Name der Nummer
-			  dnr_nr	integer		Nummer
-
-	Out		: sqlstatus
-
-	Errorcodes	: Anmerkung
-	---------------------------------------------------------------------
-	-239		: so eine Freinummer gibbet schon;
-			  es duerfen nur von der NV geholte Nummern als
-			  Freinummern eingefuegt werden
-	0		: ok, Nummer als Freinummer in NV eingefuegt
-	100		: Nummer mit diesem Namen nicht in NV
-
-	Beschreibung    : stellt nicht gebrauchte Nummern in die
-			  Nummernverwaltung als Freinummern ein.
-
---------------------------------------------------------------------------*/

int nveinid (dmdn, dfil, dnr_nam, dnr_nr)
short dmdn;
short dfil;
char *dnr_nam;
long  dnr_nr;
{

/*--- lokale Definitionen ---*/
        short dsql1;
        short dreturn;

	dsql1   = 0;
	dreturn = 0;

	/*--- irgendeinen Satz der angegebenen Nummer lesen um an die
	      Zusatzdaten zu kommen
	---*/
	dsql1 =  recread_nv (dnr_nam, dmdn, dfil);

	if (dsql1 == 0)
        {
		/*--- Daten komplett beschaffen ---*/
		auto_nr.satz_kng  = 1;
		auto_nr.nr_nr     = dnr_nr;
		auto_nr.max_wert  = 0;
		auto_nr.delstatus = 0;

		nvdata_gen ();

		/*--- Freinummer in NV einfuegen ---*/
		dreturn =  recinsert_nv ();
        }
	else
        {
		/*--- SQL-Fehler ---*/
		dreturn = dsql1;
	} /* if */

	return (dreturn);

} /* of procedure nveinid */


/*---------------------------------------------------------------------------
-									
-	Procedure	: nvanmprf
-
-	In		: dmdn		smallint  Mandantennummer
-			  dfil		smallint  Filialnummer
-			  dnr_nam	char(10)  Nummernname
-			  dnr_nr	integer	  Anfangswert Nummer
-			  dmax_nr       integer   maximaler Wert der Nummer
-			  dnr_char_lng	smallint  Laenge der Characternummer
-			  dfest_teil	char(10)  Festteil
-			
-
-	Out		: sqlstatus
-			
-	Errorcodes	: Anmerkung
-	---------------------------------------------------------------------
-	0		: ok, Nummer in NV angemeldet
-	1		: Fehler Uebergabeparameter; nr_char_lng > 10
-	2		: Fehler; Nummer gibbet schon
-			  Fehler; SQL-Status <> 100 bei Test ob gibt
-
-	Beschreibung    : Meldet eine Nummer in NV an.
-			  Die Angabe der maximal zulaessigen Nummer ermoeglicht
-			  die Ueberpruefung auf Nummernueberlauf.
-
---------------------------------------------------------------------------*/

int nvanmprf (dmdn, dfil, dnr_nam, dnr_nr, dmax_wert, dnr_char_lng, dfest_teil)
short dmdn;
short dfil;
char *dnr_nam;
long  dnr_nr;
long  dmax_wert;
long  dnr_char_lng;
char *dfest_teil;
{

/*--- Lokale Definitionen ---*/
        short dsql1;
        short dreturn;

	dsql1   = 0;
	dreturn = 0;

	/*--- Uebergabeparameter testen ---*/
	if ((dnr_char_lng >  10) ||
	    (dmax_wert<= dnr_nr))
        {
		dreturn = 1;
        }
	else
        {
		/*--- Test, ob Nummer in NV vorhanden ---*/
		dsql1 =  recread_nv (dnr_nam, dmdn, dfil);

		switch (dsql1)
                {
			case 100 :
				/*--- Name noch nicht vorhanden ---*/
				/*--- AUTO_NR initialisieren ---*/

				auto_nr.mdn         = dmdn;
				auto_nr.fil         = dfil;
				strcpy (auto_nr.nr_nam, dnr_nam);
				auto_nr.satz_kng    = 2;
				auto_nr.nr_nr       = dnr_nr;
				auto_nr.max_wert    = dmax_wert;
				auto_nr.nr_char_lng = dnr_char_lng;
				strcpy (auto_nr.fest_teil, dfest_teil);
				auto_nr.delstatus   = 0;

				nvdata_gen ();

				/*--- Satz in Tabelle AUTO_NR einfuegen ---*/
				dreturn =  recinsert_nv ();

				break;
			default :
				dreturn = 2;
				break;
		} /* of switch */
	} /* if */

	return (dreturn);

} /* of procedure nvanmprf */


/*-----------------------------------*/
/*--- interne P R O C E D U R E N ---*/
/*-----------------------------------*/

/*---------------------------------------------------------------------------
-									
-	Procedure	: freinummer
-
-	In		:
-
-	Out		: sqlstatus	smallint
-			
-	Beschreibung    : notwendige Reaktionen, wenn die geholte Nummer
-			  eine Freinummer ist
-
---------------------------------------------------------------------------*/
int freinummer ()
{
/*--- lokale Definitionen ---*/
        short dsql1;
        short dreturn;

	dsql1   = 0;
	dreturn = 0;

	/*--- akt. Nummerndaten in Output ---*/
	memcpy (&taautonum, &auto_nr, sizeof (auto_nr));

	/*--- Vergebene Freinummer logisch aus NV loeschen;
	      => delstatus auf -1 setzen.
	      Ist notwendig um Transaktionsschutz und damit
	      Multiuserfaehigkeit zu erhalten.
	---*/
	auto_nr.delstatus = -1;

	dsql1 =  recupdate_nv ();

	if (dsql1 == 0)
        {
		/*--- Freinummer physikalisch aus NV loeschen;
		      hier wird durch DELETE der Tansaktionsrahmen aufgebrochen
		---*/
		dreturn = recdelete_nv ( auto_nr.nr_nam,
                               auto_nr.satz_kng,
		               auto_nr.nr_nr,
		               auto_nr.mdn,
		               auto_nr.fil);

		/*--- dreturn ist immer "0"; ist ok, denn bei Fehler wird Satz
		      bei naechstem Loeschen mitgeloescht.
		---*/
        }
	else
        {
		/*--- Fehler bei update ---*/
		dreturn = dsql1;

	} /* if */

	return(dreturn);
}

/*---------------------------------------------------------------------------
-									
-	Procedure	: nummernspeicher
-
-	In		:
-
-	Out		: sqlstatus	smallint
-			
-	Beschreibung    : notwendige Reaktionen, wenn die geholte Nummer
-			  aus dem Nummernspeicher kommt
-
---------------------------------------------------------------------------*/
int nummernspeicher ()
{

/*--- lokale Definitionen ---*/
        short dreturn;

	dreturn = 0;

	/*--- akt. Nummerndaten in Output ---*/
	memcpy (&taautonum, &auto_nr, sizeof (auto_nr));

	/*--- Pruefung, ob Ueberlauf ---*/
	if (auto_nr.nr_nr > auto_nr.max_wert)
        {
		/*--- ja, Ueberlauf ist da ---*/
		dreturn = -1;
        }
	else
        {
		/*--- nein, Nummer ist ok ---*/

		/*--- Nummer aktualisieren ---*/
		auto_nr.nr_nr ++;
		
		/*--- zus. Nummerninformationen generieren ---*/
		nvdata_gen ();

		/*--- Nummernspeicher updaten ---*/
		dreturn = recupdate_nv ();

	} /* if */

	return(dreturn);
}

/*---------------------------------------------------------------------------
-									
-	Procedure	: nvdata_gen
-
-	In		:
-
-	Out		: sqlstatus	smallint
-			
-	Beschreibung    : notwendige Reparaturen, wenn beim Holen der Nummer
-			  eine ungueltige Satzkennung vorgefunden wurde
-
---------------------------------------------------------------------------*/
int nvdata_gen ()
{

        char dnullenspeicher [11];
        int  mnr_nr;

	mnr_nr = auto_nr.nr_nr;
        sprintf (auto_nr.nr_char,"%05d",mnr_nr);

	/*--- nr_char aus Nullen und nr_nr zusammenbauen ---*/
	dnullenspeicher[0] = 0;

	while (strlen (dnullenspeicher) <
              (auto_nr.nr_char_lng - strlen(auto_nr.nr_char)))
        {
                strcat (dnullenspeicher,"0");
	}

	strcat (dnullenspeicher,auto_nr.nr_char);
	strcpy (auto_nr.nr_char, dnullenspeicher);

	/*--- nr_komb zusammenbauen ---*/
	/*--- initialisieren ---*/
	auto_nr.nr_komb[0] = 0;

	/*--- anhaengen nr_char ---*/
	strcat (auto_nr.nr_komb, auto_nr.nr_char);

	/*--- anhaengen fest_teil ---*/
	strcat (auto_nr.nr_komb, auto_nr.fest_teil);

	return;

} /* of procedure nvdata_gen */

/*---------------------------*/
/*--- P R O C E D U R E N ---*/
/*---------------------------*/

/*---------------------------------------------------------------------------
-									
-	Procedure	: prepcuu_nv
-
-	In		: dnr_nam like auto_nr.nr_nam
-			  dmdn    like auto_nr.mdn
-			  dfil    like auto_nr.fil
-
-	Out		: sqlstatus	smallint
-			
-	Beschreibung    : prepariert update-Cursor cuu_nv
-							
---------------------------------------------------------------------------*/

int prepcuu_nv (dnr_nam, dmdn, dfil)
char *dnr_nam;
short dmdn;
short dfil;

{

	/*--- Satz fuer update preparieren  ---*/
/*
 * 	$declare cuu_nv cursor for
 * 		select auto_nr.mdn,  auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,
 * auto_nr.satz_kng,  auto_nr.max_wert,  auto_nr.nr_char,
 * auto_nr.nr_char_lng,  auto_nr.fest_teil,  auto_nr.nr_komb,
 * auto_nr.delstatus
 * into
 * $auto_nr.mdn,  $auto_nr.fil,  $auto_nr.nr_nam,  $auto_nr.nr_nr,
 * $auto_nr.satz_kng,  $auto_nr.max_wert,  $auto_nr.nr_char,
 * $auto_nr.nr_char_lng,  $auto_nr.fest_teil,  $auto_nr.nr_komb,
 * $auto_nr.delstatus from auto_nr
 *                        where auto_nr.nr_nam  = $auto_nr.nr_nam
 * 		       and auto_nr.mdn       = $auto_nr.mdn
 * 		       and auto_nr.fil       = $auto_nr.fil
 * 		       and auto_nr.delstatus = 0
 * 		       for update;
 */
#line 586 "mo_numme.ec"
  {
#line 587 "mo_numme.ec"
  static const char *sqlcmdtxt[] =
#line 587 "mo_numme.ec"
    {
#line 587 "mo_numme.ec"
    " select auto_nr . mdn , auto_nr . fil , auto_nr . nr_nam , auto_nr . nr_nr , auto_nr . satz_kng , auto_nr . max_wert , auto_nr . nr_char , auto_nr . nr_char_lng , auto_nr . fest_teil , auto_nr . nr_komb , auto_nr . delstatus from auto_nr where auto_nr . nr_nam = ? and auto_nr . mdn = ? and auto_nr . fil = ? and auto_nr . delstatus = 0 for update",
    0
    };
#line 600 "mo_numme.ec"
  static struct sqlvar_struct _sqibind[] = 
    {
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.mdn), 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.fil), 0, 0, 0, 0, 0, 0, 0 },
#line 600 "mo_numme.ec"
    };
  static struct sqlvar_struct _sqobind[] = 
    {
      { 101, sizeof(auto_nr.mdn), 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.fil), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.nr_nr), 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.satz_kng), 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.max_wert), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.nr_char_lng), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 21, 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.delstatus), 0, 0, 0, 0, 0, 0, 0 },
#line 600 "mo_numme.ec"
    };
  static struct sqlda _SD0 = { 3, _sqibind, {0}, 3, 0 };
  static struct sqlda _SD1 = { 11, _sqobind, {0}, 11, 0 };
#line 600 "mo_numme.ec"
  _sqibind[0].sqldata = auto_nr.nr_nam;
#line 600 "mo_numme.ec"
  _sqibind[1].sqldata = (char *) &auto_nr.mdn;
#line 600 "mo_numme.ec"
  _sqibind[2].sqldata = (char *) &auto_nr.fil;
#line 600 "mo_numme.ec"
  _sqobind[0].sqldata = (char *) &auto_nr.mdn;
#line 600 "mo_numme.ec"
  _sqobind[1].sqldata = (char *) &auto_nr.fil;
#line 600 "mo_numme.ec"
  _sqobind[2].sqldata = auto_nr.nr_nam;
#line 600 "mo_numme.ec"
  _sqobind[3].sqldata = (char *) &auto_nr.nr_nr;
#line 600 "mo_numme.ec"
  _sqobind[4].sqldata = (char *) &auto_nr.satz_kng;
#line 600 "mo_numme.ec"
  _sqobind[5].sqldata = (char *) &auto_nr.max_wert;
#line 600 "mo_numme.ec"
  _sqobind[6].sqldata = auto_nr.nr_char;
#line 600 "mo_numme.ec"
  _sqobind[7].sqldata = (char *) &auto_nr.nr_char_lng;
#line 600 "mo_numme.ec"
  _sqobind[8].sqldata = auto_nr.fest_teil;
#line 600 "mo_numme.ec"
  _sqobind[9].sqldata = auto_nr.nr_komb;
#line 600 "mo_numme.ec"
  _sqobind[10].sqldata = (char *) &auto_nr.delstatus;
#line 600 "mo_numme.ec"
  _iqcdcl(_iqlocate_cursor((char *) _Cn1, 0), (char *) _Cn1, (char **) sqlcmdtxt, &_SD0, &_SD1, 0);
#line 600 "mo_numme.ec"
  }

        strcpy (auto_nr.nr_nam,dnr_nam);
        auto_nr.mdn = dmdn;
        auto_nr.fil = dfil;
/*
 *         $open cuu_nv;
 */
#line 605 "mo_numme.ec"
  {
#line 605 "mo_numme.ec"
#line 605 "mo_numme.ec"
  _iqdcopen(_iqlocate_cursor((char *) _Cn1, 100), (struct sqlda *) 0, (char *) 0, (struct value *) 0, 0, 0);
#line 605 "mo_numme.ec"
  }
        SQLFEHLER (!= 0, "Fehler beim open", __LINE__);
	return(sqlstatus);
}

/*---------------------------------------------------------------------------
-									
-	Procedure	: fetchcuu_nv
-
-	In		:
-
-	Out		: sqlstatus	smallint
-			
-	Beschreibung    : holt naechsten Satz von update-Cursor cuu_nv
-							
---------------------------------------------------------------------------*/

int fetchcuu_nv ()
{

/*
 * 	$fetch cuu_nv;
 */
#line 625 "mo_numme.ec"
  {
#line 625 "mo_numme.ec"
  static _FetchSpec _FS0 = { 0, 1, 0 };
#line 625 "mo_numme.ec"
  _iqcftch(_iqlocate_cursor((char *) _Cn1, 100), (struct sqlda *) 0, (struct sqlda *) 0, (char *) 0, &_FS0);
#line 625 "mo_numme.ec"
  }
	return(sqlstatus);
}


/*---------------------------------------------------------------------------
-									
-	Procedure	: closecuu_nv
-
-	In		:
-
-	Out		:
-			
-	Beschreibung    : schliesst update-Cursor cuu_nv
-							
---------------------------------------------------------------------------*/

int closecuu_nv ()
{

/*
 * 	$close cuu_nv;
 */
#line 645 "mo_numme.ec"
  {
#line 645 "mo_numme.ec"
#line 645 "mo_numme.ec"
  _iqclose(_iqlocate_cursor((char *) _Cn1, 100));
#line 645 "mo_numme.ec"
  }

	return;

} /* of procedure closecuu_nv */

/*---------------------------------------------------------------------------
-									
-	Procedure	: recread_nv
-
-	In		: dnr_nam like auto_nr.nr_nam
-			  dmdn    like auto_nr.mdn
-			  dfil    like auto_nr.fil
-
-	Out		: sqlstatus	smallint
-			
-	Beschreibung    : - liest irgendeinen Satz der angegebenen Nummer
-			    aus der NV;
-			  - Test, ob angegebene Nummer bereits in der
-			    NV vorhanden;
-							
---------------------------------------------------------------------------*/

int recread_nv (dnr_nam, dmdn, dfil)
char *dnr_nam;
short dmdn;
short dfil;
{

        short sqlfehler;

        strcpy (auto_nr.nr_nam, dnr_nam);
        auto_nr.mdn = dmdn;
        auto_nr.fil = dfil;

	/*--- Satz aus NV lesen ---*/

/*
 *         $declare recread_nv cursor for
 * 	             select auto_nr.mdn,  auto_nr.fil,  auto_nr.nr_nam,
 * auto_nr.nr_nr,  auto_nr.satz_kng,  auto_nr.max_wert,  auto_nr.nr_char,
 * auto_nr.nr_char_lng,  auto_nr.fest_teil,  auto_nr.nr_komb,
 * auto_nr.delstatus
 * into
 * $auto_nr.mdn,  $auto_nr.fil,  $auto_nr.nr_nam,
 * $auto_nr.nr_nr,  $auto_nr.satz_kng,  $auto_nr.max_wert,  $auto_nr.nr_char,
 * $auto_nr.nr_char_lng,  $auto_nr.fest_teil,  $auto_nr.nr_komb,
 * $auto_nr.delstatus from auto_nr
 *                      where auto_nr.nr_nam    = $auto_nr.nr_nam
 * 	             and  auto_nr.mdn        = $auto_nr.mdn
 * 		     and  auto_nr.fil        = $auto_nr.fil
 * 		     and  auto_nr.delstatus  = 0;
 */
#line 682 "mo_numme.ec"
  {
#line 683 "mo_numme.ec"
  static const char *sqlcmdtxt[] =
#line 683 "mo_numme.ec"
    {
#line 683 "mo_numme.ec"
    " select auto_nr . mdn , auto_nr . fil , auto_nr . nr_nam , auto_nr . nr_nr , auto_nr . satz_kng , auto_nr . max_wert , auto_nr . nr_char , auto_nr . nr_char_lng , auto_nr . fest_teil , auto_nr . nr_komb , auto_nr . delstatus from auto_nr where auto_nr . nr_nam = ? and auto_nr . mdn = ? and auto_nr . fil = ? and auto_nr . delstatus = 0",
    0
    };
#line 695 "mo_numme.ec"
  static struct sqlvar_struct _sqibind[] = 
    {
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.mdn), 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.fil), 0, 0, 0, 0, 0, 0, 0 },
#line 695 "mo_numme.ec"
    };
  static struct sqlvar_struct _sqobind[] = 
    {
      { 101, sizeof(auto_nr.mdn), 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.fil), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.nr_nr), 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.satz_kng), 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.max_wert), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.nr_char_lng), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 21, 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.delstatus), 0, 0, 0, 0, 0, 0, 0 },
#line 695 "mo_numme.ec"
    };
  static struct sqlda _SD0 = { 3, _sqibind, {0}, 3, 0 };
  static struct sqlda _SD1 = { 11, _sqobind, {0}, 11, 0 };
#line 695 "mo_numme.ec"
  _sqibind[0].sqldata = auto_nr.nr_nam;
#line 695 "mo_numme.ec"
  _sqibind[1].sqldata = (char *) &auto_nr.mdn;
#line 695 "mo_numme.ec"
  _sqibind[2].sqldata = (char *) &auto_nr.fil;
#line 695 "mo_numme.ec"
  _sqobind[0].sqldata = (char *) &auto_nr.mdn;
#line 695 "mo_numme.ec"
  _sqobind[1].sqldata = (char *) &auto_nr.fil;
#line 695 "mo_numme.ec"
  _sqobind[2].sqldata = auto_nr.nr_nam;
#line 695 "mo_numme.ec"
  _sqobind[3].sqldata = (char *) &auto_nr.nr_nr;
#line 695 "mo_numme.ec"
  _sqobind[4].sqldata = (char *) &auto_nr.satz_kng;
#line 695 "mo_numme.ec"
  _sqobind[5].sqldata = (char *) &auto_nr.max_wert;
#line 695 "mo_numme.ec"
  _sqobind[6].sqldata = auto_nr.nr_char;
#line 695 "mo_numme.ec"
  _sqobind[7].sqldata = (char *) &auto_nr.nr_char_lng;
#line 695 "mo_numme.ec"
  _sqobind[8].sqldata = auto_nr.fest_teil;
#line 695 "mo_numme.ec"
  _sqobind[9].sqldata = auto_nr.nr_komb;
#line 695 "mo_numme.ec"
  _sqobind[10].sqldata = (char *) &auto_nr.delstatus;
#line 695 "mo_numme.ec"
  _iqcdcl(_iqlocate_cursor((char *) _Cn2, 0), (char *) _Cn2, (char **) sqlcmdtxt, &_SD0, &_SD1, 0);
#line 695 "mo_numme.ec"
  }
/*
 *         $open recread_nv;
 */
#line 696 "mo_numme.ec"
  {
#line 696 "mo_numme.ec"
#line 696 "mo_numme.ec"
  _iqdcopen(_iqlocate_cursor((char *) _Cn2, 100), (struct sqlda *) 0, (char *) 0, (struct value *) 0, 0, 0);
#line 696 "mo_numme.ec"
  }
        SQLFEHLER (!=0, "Fehler beim oeffnen recread_nv",__LINE__)
/*
 *         $fetch recread_nv;
 */
#line 698 "mo_numme.ec"
  {
#line 698 "mo_numme.ec"
  static _FetchSpec _FS0 = { 0, 1, 0 };
#line 698 "mo_numme.ec"
  _iqcftch(_iqlocate_cursor((char *) _Cn2, 100), (struct sqlda *) 0, (struct sqlda *) 0, (char *) 0, &_FS0);
#line 698 "mo_numme.ec"
  }
        sqlfehler = sqlstatus;
/*
 *         $close recread_nv;
 */
#line 700 "mo_numme.ec"
  {
#line 700 "mo_numme.ec"
#line 700 "mo_numme.ec"
  _iqclose(_iqlocate_cursor((char *) _Cn2, 100));
#line 700 "mo_numme.ec"
  }
        sqlstatus = sqlfehler;
	return(sqlstatus);
}   /* of procedure recread_nv */


/*---------------------------------------------------------------------------
-									
-	Procedure	: recupdate_nv
-
-	In		:
-
-	Out		: sqlstatus	smallint
-			
-	Beschreibung    : Satz in NV wird aktualisiert;
-			  - logisches Loeschen durch delstatus
-			  - Inkrementieren des Nummernspeichers
-							
---------------------------------------------------------------------------*/

int recupdate_nv ()
{

        int sqlfehler;

        sprintf (sql,"update auto_nr set auto_nr.mdn = ?,  "
"auto_nr.fil = ?,  auto_nr.nr_nam = ?,  auto_nr.nr_nr = ?,  "
"auto_nr.satz_kng = ?,  auto_nr.max_wert = ?,  auto_nr.nr_char = ?,  "
"auto_nr.nr_char_lng = ?,  auto_nr.fest_teil = ?,  "
"auto_nr.nr_komb = ?,  auto_nr.delstatus = ? where current of cuu_nv");
/*
 *         $prepare upd_auto_nr from $sql;
 */
#line 730 "mo_numme.ec"
  {
#line 730 "mo_numme.ec"
#line 730 "mo_numme.ec"
  _iqnprep((char *) _Cn3, sql);
#line 730 "mo_numme.ec"
  }
/*
 *         $execute upd_auto_nr using $auto_nr.mdn,  $auto_nr.fil,
 * $auto_nr.nr_nam,  $auto_nr.nr_nr,  $auto_nr.satz_kng,  $auto_nr.max_wert,
 * $auto_nr.nr_char,  $auto_nr.nr_char_lng,  $auto_nr.fest_teil,
 * $auto_nr.nr_komb,  $auto_nr.delstatus;
 */
#line 731 "mo_numme.ec"
  {
#line 734 "mo_numme.ec"
  static struct sqlvar_struct _sqibind[] = 
    {
      { 101, sizeof(auto_nr.mdn), 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.fil), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.nr_nr), 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.satz_kng), 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.max_wert), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.nr_char_lng), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 21, 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.delstatus), 0, 0, 0, 0, 0, 0, 0 },
#line 734 "mo_numme.ec"
    };
  static struct sqlda _SD0 = { 11, _sqibind, {0}, 11, 0 };
#line 734 "mo_numme.ec"
  _sqibind[0].sqldata = (char *) &auto_nr.mdn;
#line 734 "mo_numme.ec"
  _sqibind[1].sqldata = (char *) &auto_nr.fil;
#line 734 "mo_numme.ec"
  _sqibind[2].sqldata = auto_nr.nr_nam;
#line 734 "mo_numme.ec"
  _sqibind[3].sqldata = (char *) &auto_nr.nr_nr;
#line 734 "mo_numme.ec"
  _sqibind[4].sqldata = (char *) &auto_nr.satz_kng;
#line 734 "mo_numme.ec"
  _sqibind[5].sqldata = (char *) &auto_nr.max_wert;
#line 734 "mo_numme.ec"
  _sqibind[6].sqldata = auto_nr.nr_char;
#line 734 "mo_numme.ec"
  _sqibind[7].sqldata = (char *) &auto_nr.nr_char_lng;
#line 734 "mo_numme.ec"
  _sqibind[8].sqldata = auto_nr.fest_teil;
#line 734 "mo_numme.ec"
  _sqibind[9].sqldata = auto_nr.nr_komb;
#line 734 "mo_numme.ec"
  _sqibind[10].sqldata = (char *) &auto_nr.delstatus;
#line 734 "mo_numme.ec"
  _iqexecute(_iqlocate_cursor((char *) _Cn3, 101), &_SD0, (char *) 0, (struct value *) 0, (struct sqlda *) 0, (char *) 0, (struct value *) 0, 0);
#line 734 "mo_numme.ec"
  }
        SQLFEHLER (!= 0," Fehler beim update auto_nr\n",__LINE__)
        sqlfehler = sqlstatus;
/*
 *         $free upd_auto_nr;
 */
#line 737 "mo_numme.ec"
  {
#line 737 "mo_numme.ec"
#line 737 "mo_numme.ec"
  _iqfree(_iqlocate_cursor((char *) _Cn3, 102));
#line 737 "mo_numme.ec"
  }
	return(sqlfehler);

} /* of procedure recupdate_nv */


/*---------------------------------------------------------------------------
-									
-	Procedure	: recdelete_nv
-
-	In		: dnr_nam   like auto_nr.nr_nam
-			  dsatz_kng like auto_nr.satz_kng
-			  dnr_nr    like auto_nr.nr_nr
-			  dmdn      like auto_nr.mdn
-			  dfil      like auto_nr.fil
-
-	Out		: sqlstatus	smallint
-			
-	Beschreibung    : loescht Satz aus Nummernverwaltung
-							
---------------------------------------------------------------------------*/

int recdelete_nv (dnr_nam, dsatz_kng, dnr_nr, dmdn, dfil)
char * dnr_nam;
short  dsatz_kng;
int    dnr_nr;
short  dmdn;
short  dfil;
{

        char name[20];

        strcpy (auto_nr.nr_nam, dnr_nam);
        auto_nr.satz_kng = dsatz_kng;
        auto_nr.nr_nr    = dnr_nr;
        auto_nr.mdn      = dmdn;
        auto_nr.fil      = dfil;

        sprintf (name,"\"%s\"",auto_nr.nr_nam);

        sprintf (sql,"delete from auto_nr %s%s%s%hd%s%d%s%hd%s%hd%s" ,
	                     "where auto_nr.nr_nam     = ",name,
                             " and auto_nr.satz_kng    = ",auto_nr.satz_kng ,
		             " and auto_nr.nr_nr       = ",auto_nr.nr_nr   ,
		             " and auto_nr.mdn         = ",auto_nr.mdn    ,
		             " and auto_nr.fil         = ",auto_nr.fil   ,
		             " and auto_nr.delstatus   = -1");
/*
 *         $prepare del_auto_nr from $sql;
 */
#line 784 "mo_numme.ec"
  {
#line 784 "mo_numme.ec"
#line 784 "mo_numme.ec"
  _iqnprep((char *) _Cn4, sql);
#line 784 "mo_numme.ec"
  }
/*
 *         $execute del_auto_nr;
 */
#line 785 "mo_numme.ec"
  {
#line 785 "mo_numme.ec"
#line 785 "mo_numme.ec"
  _iqexecute(_iqlocate_cursor((char *) _Cn4, 101), (struct sqlda *) 0, (char *) 0, (struct value *) 0, (struct sqlda *) 0, (char *) 0, (struct value *) 0, 0);
#line 785 "mo_numme.ec"
  }
/*
 *         $free del_auto_nr;
 */
#line 786 "mo_numme.ec"
  {
#line 786 "mo_numme.ec"
#line 786 "mo_numme.ec"
  _iqfree(_iqlocate_cursor((char *) _Cn4, 102));
#line 786 "mo_numme.ec"
  }
	return(0);

} /* of procedure recdelete_nv */

/*---------------------------------------------------------------------------
-									
-	Procedure	: recinsert_nv
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			
-	Beschreibung    : fuegt Freinummer in NV ein
-							
---------------------------------------------------------------------------*/

int recinsert_nv ()
{
	/*--- Satz in NV einfuegen ---*/

        short sqlfehler;

/*
 *         $declare ins_auto cursor for
 *                      insert into auto_nr
 *                      (auto_nr.mdn,  auto_nr.fil,  auto_nr.nr_nam,
 * auto_nr.nr_nr,  auto_nr.satz_kng,  auto_nr.max_wert,  auto_nr.nr_char,
 * auto_nr.nr_char_lng,  auto_nr.fest_teil,  auto_nr.nr_komb,
 * auto_nr.delstatus)
 *                      values
 *                      ($auto_nr.mdn,  $auto_nr.fil,  $auto_nr.nr_nam,
 * $auto_nr.nr_nr,  $auto_nr.satz_kng,  $auto_nr.max_wert,  $auto_nr.nr_char,
 * $auto_nr.nr_char_lng,  $auto_nr.fest_teil,  $auto_nr.nr_komb,
 * $auto_nr.delstatus);
 */
#line 809 "mo_numme.ec"
  {
#line 810 "mo_numme.ec"
  static const char *sqlcmdtxt[] =
#line 810 "mo_numme.ec"
    {
#line 810 "mo_numme.ec"
    " insert into auto_nr ( auto_nr . mdn , auto_nr . fil , auto_nr . nr_nam , auto_nr . nr_nr , auto_nr . satz_kng , auto_nr . max_wert , auto_nr . nr_char , auto_nr . nr_char_lng , auto_nr . fest_teil , auto_nr . nr_komb , auto_nr . delstatus ) values ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? )",
    0
    };
#line 819 "mo_numme.ec"
  static struct sqlvar_struct _sqibind[] = 
    {
      { 101, sizeof(auto_nr.mdn), 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.fil), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.nr_nr), 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.satz_kng), 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.max_wert), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(auto_nr.nr_char_lng), 0, 0, 0, 0, 0, 0, 0 },
      { 100, 11, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 21, 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(auto_nr.delstatus), 0, 0, 0, 0, 0, 0, 0 },
#line 819 "mo_numme.ec"
    };
  static struct sqlda _SD0 = { 11, _sqibind, {0}, 11, 0 };
#line 819 "mo_numme.ec"
  _sqibind[0].sqldata = (char *) &auto_nr.mdn;
#line 819 "mo_numme.ec"
  _sqibind[1].sqldata = (char *) &auto_nr.fil;
#line 819 "mo_numme.ec"
  _sqibind[2].sqldata = auto_nr.nr_nam;
#line 819 "mo_numme.ec"
  _sqibind[3].sqldata = (char *) &auto_nr.nr_nr;
#line 819 "mo_numme.ec"
  _sqibind[4].sqldata = (char *) &auto_nr.satz_kng;
#line 819 "mo_numme.ec"
  _sqibind[5].sqldata = (char *) &auto_nr.max_wert;
#line 819 "mo_numme.ec"
  _sqibind[6].sqldata = auto_nr.nr_char;
#line 819 "mo_numme.ec"
  _sqibind[7].sqldata = (char *) &auto_nr.nr_char_lng;
#line 819 "mo_numme.ec"
  _sqibind[8].sqldata = auto_nr.fest_teil;
#line 819 "mo_numme.ec"
  _sqibind[9].sqldata = auto_nr.nr_komb;
#line 819 "mo_numme.ec"
  _sqibind[10].sqldata = (char *) &auto_nr.delstatus;
#line 819 "mo_numme.ec"
  _iqcdcl(_iqlocate_cursor((char *) _Cn5, 0), (char *) _Cn5, (char **) sqlcmdtxt, &_SD0, (struct sqlda *) 0, 0);
#line 819 "mo_numme.ec"
  }

/*
 *         $open ins_auto;
 */
#line 821 "mo_numme.ec"
  {
#line 821 "mo_numme.ec"
#line 821 "mo_numme.ec"
  _iqdcopen(_iqlocate_cursor((char *) _Cn5, 100), (struct sqlda *) 0, (char *) 0, (struct value *) 0, 0, 0);
#line 821 "mo_numme.ec"
  }
/*
 *         $put ins_auto;
 */
#line 822 "mo_numme.ec"
  {
#line 822 "mo_numme.ec"
#line 822 "mo_numme.ec"
  _iqcput(_iqlocate_cursor((char *) _Cn5, 100), (struct sqlda *) 0, (char *) 0);
#line 822 "mo_numme.ec"
  }
/*
 *         $flush ins_auto;
 */
#line 823 "mo_numme.ec"
  {
#line 823 "mo_numme.ec"
#line 823 "mo_numme.ec"
  _iqflush(_iqlocate_cursor((char *) _Cn5, 100));
#line 823 "mo_numme.ec"
  }
        SQLFEHLER (!= 0,"Fehler beim insert in auto_nr",__LINE__);
        sqlfehler = sqlstatus;
/*
 *         $close ins_auto;
 */
#line 826 "mo_numme.ec"
  {
#line 826 "mo_numme.ec"
#line 826 "mo_numme.ec"
  _iqclose(_iqlocate_cursor((char *) _Cn5, 100));
#line 826 "mo_numme.ec"
  }
	return(sqlfehler);
} /* of procedure recinsert_nv */

#line 828 "mo_numme.ec"
