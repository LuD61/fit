#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "stdfkt.h"
#include "mo_meld.h"
#include "wmask.h"

extern HWND hMainWindow;

HWND hWndToolTip;
HWND hwndEdit;
static char **qInfo;
static UINT *qIdfrom;
static char **qhWndInfo;
static HWND **qhWndFrom; 


HMENU MakeMenue (struct PMENUE *menue)
/**
Menue generieren.
**/
{
	    int i;
        HMENU hMenu, hMenuPopup;
		struct PMENUE *nmenue;

        hMenu = CreateMenu ();

		for (i = 0; menue[i].text; i ++)
		{
			  if (menue [i].typ [0] == ' ')
			  {	  
                  AppendMenu (hMenu, MF_STRING, menue[i].menid,
                              menue[i].text);
			  }
			  else if (menue [i].typ [0] == 'G')
			  {	  
                  AppendMenu (hMenu, MF_STRING , menue[i].menid,
                              menue[i].text);
                  EnableMenuItem (hMenu, menue[i].menid, MF_GRAYED);
			  }
			  else if (menue [i].typ [0] == 'C')
			  {	  
                  AppendMenu (hMenu, MF_STRING | MF_CHECKED, 
					          menue[i].menid,
                              menue[i].text);
			  }
   			  else if (menue [i].typ [0] == 'M')
			  {
				   nmenue = (struct PMENUE *) menue[i].nmenue;
				   hMenuPopup = MakeMenue (nmenue);
		           AppendMenu (hMenu, MF_POPUP, 
					           (UINT) hMenuPopup, menue[i].text);
              }
   			  else if (menue [i].typ [0] == 'S')
			  {
		           AppendMenu (hMenu, MF_SEPARATOR, 
					                  0, NULL);
              }
		}
        return hMenu; 
}

BOOL QuickHwndCpy (LPTOOLTIPTEXT lpttt)
/**
Text in Quickinfo fuer Kindfenster der Toolbar.
**/
{
         int i;

         if (lpttt->uFlags & TTF_IDISHWND == 0) return FALSE;

         for (i = 0; qhWndFrom[i]; i ++)
         {
                       if (lpttt->hdr.idFrom == (UINT) *qhWndFrom[i])
                       {
                                           break;
                       }
        }                            

        if (qhWndFrom [i] == NULL)
        {
                        writelog ("Nicht gefunden");
                        return FALSE;
        }
        lstrcpy (lpttt->szText, qhWndInfo [i]);
        return TRUE;
}


BOOL QuickCpy (LPSTR text, UINT idFrom)
/**
Text in QuickInfo kopieren.
**/
{
        int i;

        for (i = 0; qIdfrom [i]; i ++)
        {
                    if (idFrom == qIdfrom [i]) break;
        }
        if (qIdfrom [i] == 0) return FALSE;
        lstrcpy (text, qInfo [i]);
        return TRUE;
}


BOOL ComboToolTip (HWND hwndToolBar, HWND hwndComboBox)
{
     BOOL bSuccess ;
     TOOLINFO ti ;

     // Add tooltip for main combo box
     ZeroMemory (&ti, sizeof (TOOLINFO)) ;
     ti.cbSize = sizeof (TOOLINFO) ;
     ti.uFlags = TTF_IDISHWND | TTF_CENTERTIP | TTF_SUBCLASS ;
     ti.hwnd   = hwndToolBar ;
     ti.uId    = (UINT) (HWND) hwndComboBox ;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (hWndToolTip, &ti) ;
     if (!bSuccess)
          return FALSE ;

     // Add tooltip for combo box's edit control
     hwndEdit = GetWindow (hwndComboBox, GW_CHILD) ;
     ti.uId    = (UINT) (HWND) hwndEdit ;
     bSuccess = ToolTip_AddTool (hWndToolTip, &ti) ;

     return bSuccess ;
}

HWND MakeToolBar (HWND hWnd, TBBUTTON *tbb, char **qI,UINT *qId,
				                        char **qhWI, HWND **qhWF)
{

	     HWND hWndTB;

         hWndTB = CreateToolbarEx (hWnd,
                                   WS_CHILD | WS_VISIBLE |
                                   CCS_TOP | CCS_NODIVIDER |
                                   TBSTYLE_TOOLTIPS,
                                   1, 15, HINST_COMMCTRL,
                                   IDB_STD_SMALL_COLOR,
                                   tbb,
                                   4,
                                   0, 0,
                                   0, 0,
                                   sizeof (TBBUTTON));
         hWndToolTip = ToolBar_GetToolTips (hWndTB);
		 qInfo = qI;
		 qIdfrom = qId;
		 qhWndInfo = qhWI;
		 qhWndFrom = qhWF;
		 return hWndTB;
}


HWND MakeToolBarEx (HANDLE hMainInst, HWND hWnd, TBBUTTON *tbb,
                                        int tbbanz, 
                                        char **qI,UINT *qId,
				                        char **qhWI, HWND **qhWF)
{
	     HWND hWndTB;


         hWndTB = CreateToolbarEx (hWnd,
                                   WS_CHILD | WS_VISIBLE |
                                   CCS_TOP |
                                   TBSTYLE_TOOLTIPS,
                                   1, 4, hMainInst,
                                   FTOOLBAR,
                                   &tbb[0],
                                   tbbanz,
                                   0, 0,
                                   0, 0,
                                   sizeof (TBBUTTON));
         ShowWindow (hWndTB, SW_SHOWNORMAL);
         UpdateWindow (hWndTB);
         hWndToolTip = ToolBar_GetToolTips (hWndTB);
		 qInfo = qI;
		 qIdfrom = qId;
		 qhWndInfo = qhWI;
		 qhWndFrom = qhWF;
		 return hWndTB;
}

HWND MakeToolBarCombo (HANDLE hMainInst,
                       HWND hWnd,HWND hWndTB, int Id, int tbn1, 
                                                       int tbn2)
/**
Combobox zu Toolbar
**/
{
         int x, y, cx, cy;
         RECT rect;
         HWND hWndCombo;
         HFONT hFont;

         ToolBar_GetItemRect (hWndTB, tbn1, &rect);
         x = rect.left;
         y = rect.top;
         cy = 100;
         ToolBar_GetItemRect (hWndTB, tbn2, &rect);
         cx = rect.right - x + 1;
         hWndCombo = CreateWindow ("combobox",
                                    NULL,
                                    WS_CHILD | WS_VISIBLE | WS_VSCROLL |
                                    CBS_DROPDOWNLIST,
                                    x,y, cx, cy,
                                    hWnd,
                                    (HMENU) Id,
                                    hMainInst,
                                    0);
          SetParent (hWndCombo, hWndTB);
          stdfont ();
          hFont = SetWindowFont (hWnd);
          SendMessage (hWndCombo, WM_SETFONT,
                                 (WPARAM) hFont, (LPARAM) NULL);
          return hWndCombo;
}


int SendKey (int key)
/**
Tastaturmelung senden.
**/
{
        if (SendWindow && current_form)
        {
                  PostMessage (current_form->mask[currentfield].feldid,
                               WM_KEYDOWN,  (WPARAM) key, 0l);
        }
        else
        {
                 PostMessage (AktivWindow, WM_KEYDOWN,  (WPARAM) key, 0l);
        }
        return 0;
}

