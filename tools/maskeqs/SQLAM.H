#ident  "@(#) sqlam.h	1.400	10/02/92	by AM" 
/*** 
--------------------------------------------------------------------------------
-
-       BSA         System-Engineering         BSA-Sued
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/2413
-
--------------------------------------------------------------------------------
-
-       Modulname               :       Name
-
-       Autor                   :       A.Michler
-       Erstellungsdatum        :       10.02.92
-       Modifikationsdatum      :       10.02.92
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       1.00    10.02.92	A.Michler  Grundmodul
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :	Definitionen fuer ESQL-C
-
--------------------------------------------------------------------------------
***/

/* Alte Version bis zum 30.09.92     Ro
#define SQLFEHLER(b, s, l)  if(sqlca.sqlcode b) {fprintf(stderr,"fehler : %d bei : %s Zeile : %d\n",sqlca.sqlcode,s,l); rollback_work(); exit(1); }
*/

/* Neue Version A */

#define SQLFEHLER(b, s, l) if (sqlca.sqlcode b) _SQLF(s,l);

int in_work;
