# Von Microsoft Developer Studio generierte NMAKE-Datei, basierend auf maske.dsp
!IF "$(CFG)" == ""
CFG=maske - Win32 Debug
!MESSAGE Keine Konfiguration angegeben. maske - Win32 Debug wird als Standard\
 verwendet.
!ENDIF 

!IF "$(CFG)" != "maske - Win32 Release" && "$(CFG)" != "maske - Win32 Debug"
!MESSAGE Ung�ltige Konfiguration "$(CFG)" angegeben.
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "maske.mak" CFG="maske - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "maske - Win32 Release" (basierend auf  "Win32 (x86) Application")
!MESSAGE "maske - Win32 Debug" (basierend auf  "Win32 (x86) Application")
!MESSAGE 
!ERROR Eine ung�ltige Konfiguration wurde angegeben.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "maske - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

!IF "$(RECURSE)" == "0" 

ALL : "..\..\..\fit\bin\maske.exe"

!ELSE 

ALL : "..\..\..\fit\bin\maske.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\DATUM.OBJ"
	-@erase "$(INTDIR)\MASKE.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_INTP.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\MO_PROCT.OBJ"
	-@erase "$(INTDIR)\MO_SPAWN.OBJ"
	-@erase "$(INTDIR)\MO_SW.OBJ"
	-@erase "$(INTDIR)\MO_SWKOP.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\SYSDATE.OBJ"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\wmask.obj"
	-@erase "..\..\..\fit\bin\maske.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /ML /w /W0 /GX /O2 /I "d:\informix\incl\esql" /D "WIN32" /D\
 "NDEBUG" /D "_WINDOWS" /Fp"$(INTDIR)\maske.pch" /YX /Fo"$(INTDIR)\\"\
 /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\maske.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib isqlt07c.lib comctl32.lib /nologo /subsystem:windows\
 /incremental:no /pdb:"$(OUTDIR)\maske.pdb" /machine:I386\
 /out:"d:\user\fit\bin\maske.exe" /libpath:"d:\informix\lib" 
LINK32_OBJS= \
	"$(INTDIR)\DATUM.OBJ" \
	"$(INTDIR)\MASKE.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_INTP.OBJ" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\MO_PROCT.OBJ" \
	"$(INTDIR)\MO_SPAWN.OBJ" \
	"$(INTDIR)\MO_SW.OBJ" \
	"$(INTDIR)\MO_SWKOP.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\SYSDATE.OBJ" \
	"$(INTDIR)\wmask.obj"

"..\..\..\fit\bin\maske.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "maske - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Anfang eines benutzerdefinierten Makros
OutDir=.\Debug
# Ende eines benutzerdefinierten Makros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\maske.exe"

!ELSE 

ALL : "$(OUTDIR)\maske.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\DATUM.OBJ"
	-@erase "$(INTDIR)\MASKE.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_INTP.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\MO_PROCT.OBJ"
	-@erase "$(INTDIR)\MO_SPAWN.OBJ"
	-@erase "$(INTDIR)\MO_SW.OBJ"
	-@erase "$(INTDIR)\MO_SWKOP.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\SYSDATE.OBJ"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(INTDIR)\wmask.obj"
	-@erase "$(OUTDIR)\maske.exe"
	-@erase "$(OUTDIR)\maske.ilk"
	-@erase "$(OUTDIR)\maske.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /w /W0 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /D\
 "WIN32" /D "_DEBUG" /D "_WINDOWS" /Fp"$(INTDIR)\maske.pch" /YX /Fo"$(INTDIR)\\"\
 /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\maske.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib isqlt07c.lib comctl32.lib /nologo /subsystem:windows\
 /incremental:yes /pdb:"$(OUTDIR)\maske.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)\maske.exe" /pdbtype:sept /libpath:"d:\informix\lib" 
LINK32_OBJS= \
	"$(INTDIR)\DATUM.OBJ" \
	"$(INTDIR)\MASKE.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_INTP.OBJ" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\MO_PROCT.OBJ" \
	"$(INTDIR)\MO_SPAWN.OBJ" \
	"$(INTDIR)\MO_SW.OBJ" \
	"$(INTDIR)\MO_SWKOP.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\SYSDATE.OBJ" \
	"$(INTDIR)\wmask.obj"

"$(OUTDIR)\maske.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(CFG)" == "maske - Win32 Release" || "$(CFG)" == "maske - Win32 Debug"
SOURCE=.\DATUM.C

"$(INTDIR)\DATUM.OBJ" : $(SOURCE) "$(INTDIR)"


SOURCE=.\MASKE.C

!IF  "$(CFG)" == "maske - Win32 Release"


"$(INTDIR)\MASKE.OBJ" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "maske - Win32 Debug"

DEP_CPP_MASKE=\
	".\comcthlp.h"\
	".\mo_meld.h"\
	".\wmask.h"\
	

"$(INTDIR)\MASKE.OBJ" : $(SOURCE) $(DEP_CPP_MASKE) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_A_PR.C

!IF  "$(CFG)" == "maske - Win32 Release"

DEP_CPP_MO_A_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\allgemei.h"\
	".\mo_a_pr.h"\
	".\mo_curso.h"\
	".\sqlam.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "maske - Win32 Debug"

DEP_CPP_MO_A_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\allgemei.h"\
	".\mo_a_pr.h"\
	".\mo_curso.h"\
	".\sqlam.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_CURSO.C

!IF  "$(CFG)" == "maske - Win32 Release"

DEP_CPP_MO_CU=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	".\tcursor.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "maske - Win32 Debug"

DEP_CPP_MO_CU=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	".\tcursor.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_INTP.C

!IF  "$(CFG)" == "maske - Win32 Release"

DEP_CPP_MO_IN=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\mo_spawn.h"\
	

"$(INTDIR)\MO_INTP.OBJ" : $(SOURCE) $(DEP_CPP_MO_IN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "maske - Win32 Debug"

DEP_CPP_MO_IN=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\mo_spawn.h"\
	

"$(INTDIR)\MO_INTP.OBJ" : $(SOURCE) $(DEP_CPP_MO_IN) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_NUMME.C

!IF  "$(CFG)" == "maske - Win32 Release"

DEP_CPP_MO_NU=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	".\sqlam.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "maske - Win32 Debug"

DEP_CPP_MO_NU=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	".\sqlam.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_PREIS.C

!IF  "$(CFG)" == "maske - Win32 Release"

DEP_CPP_MO_PR=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\mo_preis.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "maske - Win32 Debug"

DEP_CPP_MO_PR=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\mo_preis.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_PROCT.C

"$(INTDIR)\MO_PROCT.OBJ" : $(SOURCE) "$(INTDIR)"


SOURCE=.\MO_SPAWN.C

!IF  "$(CFG)" == "maske - Win32 Release"

DEP_CPP_MO_SP=\
	".\mo_spawn.h"\
	".\split.h"\
	

"$(INTDIR)\MO_SPAWN.OBJ" : $(SOURCE) $(DEP_CPP_MO_SP) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "maske - Win32 Debug"

DEP_CPP_MO_SP=\
	".\mo_spawn.h"\
	".\split.h"\
	

"$(INTDIR)\MO_SPAWN.OBJ" : $(SOURCE) $(DEP_CPP_MO_SP) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_SW.C

"$(INTDIR)\MO_SW.OBJ" : $(SOURCE) "$(INTDIR)"


SOURCE=.\MO_SWKOP.C

!IF  "$(CFG)" == "maske - Win32 Release"

DEP_CPP_MO_SW=\
	".\swcomhd.h"\
	".\swcomhd3.h"\
	

"$(INTDIR)\MO_SWKOP.OBJ" : $(SOURCE) $(DEP_CPP_MO_SW) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "maske - Win32 Debug"

DEP_CPP_MO_SW=\
	".\swcomhd.h"\
	".\swcomhd3.h"\
	

"$(INTDIR)\MO_SWKOP.OBJ" : $(SOURCE) $(DEP_CPP_MO_SW) "$(INTDIR)"


!ENDIF 

SOURCE=.\STRFKT.C

"$(INTDIR)\STRFKT.OBJ" : $(SOURCE) "$(INTDIR)"


SOURCE=.\SYSDATE.C

"$(INTDIR)\SYSDATE.OBJ" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wmask.c

!IF  "$(CFG)" == "maske - Win32 Release"

DEP_CPP_WMASK=\
	".\strfkt.h"\
	".\wmask.h"\
	

"$(INTDIR)\wmask.obj" : $(SOURCE) $(DEP_CPP_WMASK) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "maske - Win32 Debug"

DEP_CPP_WMASK=\
	".\strfkt.h"\
	".\wmask.h"\
	

"$(INTDIR)\wmask.obj" : $(SOURCE) $(DEP_CPP_WMASK) "$(INTDIR)"


!ENDIF 


!ENDIF 

