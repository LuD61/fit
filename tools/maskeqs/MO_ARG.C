/**
Test der an das Programm uebergebenen Argumente
**/

int scroll_arg (arg, pos, anz)
/**
Argumenete nach links scrollen.
**/
char *arg [];
short pos;
int anz;
{

       anz --;
       for (;pos < anz; pos ++)
       {
                  arg [pos] = arg [pos + 1];
       }
       return (anz);
}

argtst (arganz, arg, tst_arg)
int *arganz;
char *arg [];
void (*tst_arg) ();
{

       int anz;
       int i;

       anz = *arganz;
       for (i = 0; i < anz; i ++)
       {
              if (arg [i] [0] == '-')
              {
                            (*tst_arg) (&arg[i][1]);
                            anz = scroll_arg (arg,i,anz);
                            i --;
              }
       }
       *arganz = anz;
}
