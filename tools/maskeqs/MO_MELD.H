#ifndef _MO_MELD_DEF
#define _MO_MELD_DEF
#include <commctrl.h>
#include "comcthlp.h"

#define IDM_WORK           100
#define IDM_SHOW           101
#define IDM_DEL            102
#define IDM_PRINT          103
#define IDM_INFO           104
#define IDM_TEXT           105
#define IDM_ETI            106

#define FTOOLBAR 400

struct PMENUE
{
	     char *text;
		 char *typ;
		 void *nmenue;
		 int menid;
};

int abfragejn (HWND, char *, char *);

HMENU MakeMenue (struct PMENUE *);
HWND MakeToolBar (HWND, TBBUTTON *, char **, 
				  UINT *, char **, HWND **);
HWND MakeToolBarEx (HANDLE, HWND, TBBUTTON *, int, 
                    char **, 
				    UINT *, char **, HWND **);
HWND MakeToolBarCombo (HANDLE , HWND,HWND, int, int, int);
BOOL QuickHwndCpy (LPTOOLTIPTEXT);
BOOL QuickCpy (LPSTR, UINT);
BOOL ComboToolTip (HWND, HWND);
int SendKey (int);

#endif
