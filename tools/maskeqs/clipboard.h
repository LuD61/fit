#ifndef _CLIPBOARD_DEF
#define _CLIPBOARD_DEF
void ToClipboard (char *);
char * FromClipboard ();
char *CpBopen (void);
char *CpBreopen (void);
char *CpBfgets (char *, int, char *);
void CpBclose (void);
#endif
