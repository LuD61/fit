
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.CallableStatement;
import java.util.Properties;
	


public class InformixConnector {
	
	/**
	 * @param args
	 */
	
	private Connection cn = null;
	
	
	public Connection createConnection() {
		
		String iniFilePath = System.getenv("BWS");
		iniFilePath = iniFilePath + "\\etc\\jdbc.ini";
		
		Properties properties = null;
		try {
			properties = INIReader.load(iniFilePath);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		final String informixPort = properties.getProperty("informixPort");
		final String informixHost = properties.getProperty("informixHost");		
		final String informixServer = properties.getProperty("informixServer");
		final String informixUser = properties.getProperty("informixUser");
		final String informixPassword = properties.getProperty("informixPassword");
		
	    try { 
		    System.out.println("* Treiber laden");		    
		    Class.forName("com.informix.jdbc.IfxDriver").newInstance();
	    } 
	    catch (Exception e) { 
	        System.err.println("Unable to load driver."); 
	        e.printStackTrace();
	        System.out.println("Keine Verbindung zur Informix Datenbank m�glich, bitte Verbindung zur Datenbank pr�fen!");
	    }
	    
	    try { 
		    System.out.println("* Verbindung aufbauen");
		    String url = "jdbc:informix-sqli://" + informixHost +":" + informixPort + "/bws:INFORMIXSERVER=" + informixServer + ";user=" + informixUser + ";password="+ informixPassword + ";DB_LOCALE=DB_LOCALE=de_de.cp1252;CLIENT_LOCALE=DB_LOCALE=de_de.cp1252";
		    this.cn = DriverManager.getConnection(url);
		    this.executeStatement("set isolation to dirty read");		    
	    } catch (SQLException sqle) { 
	        System.out.println("SQLException: " + sqle.getMessage());
	        System.out.println("SQLState: " + sqle.getSQLState()); 
	        System.out.println("VendorError: " + sqle.getErrorCode()); 
	        sqle.printStackTrace();
	        System.out.println("Keine Verbindung zur Informix Datenbank m�glich, bitte Verbindung zur Datenbank pr�fen!");	        
	    }	    
	    return this.cn;
	}


	public void closeConnection() {
		
		try {
			this.cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
	}


	public void executeStatement(final String statement) {
		
		CallableStatement stmt;
		
		try {
			stmt = this.cn.prepareCall(statement);
			stmt.execute();
			System.out.println(statement);
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	
	public ResultSet executeQuery(final String statement) {
		
		CallableStatement stmt;
		ResultSet resultSet = null;
		
		try {
			stmt = this.cn.prepareCall(statement);
			resultSet = stmt.executeQuery();
			System.out.println(statement);
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return resultSet;
	}
	
	
	
	public static void main ( String args[] ) {
		
		InformixConnector informixConnector = new InformixConnector();
		informixConnector.createConnection();
		informixConnector.closeConnection();
		
	}
	
}
