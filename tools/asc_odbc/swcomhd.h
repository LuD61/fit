#ifndef SINIX
#ident "@(#) swcomhd.h	2.000d            17.08.93   by KB/FF"
#endif

/* #1628 17.08.93  Anpassung zur SW800 Version 3.00           */

/* Struktur fuer Headersatz SW-Waage                          */

#define ushort unsigned short
#define uchar  unsigned char
#define MAXFELDER 100

/* Struktur fuer Dezimalwerte, die mehr als 9 Ziffern haben    */

typedef struct 
        {
                signed long   high;
                unsigned long low;
        } TYint64;

/* Struktur fuer die Informationen ueber ein Feld             */

typedef struct
        {       char     att_symb [4];
                ushort   stellen;
                ushort   bits;
                ushort   bits1;		/* um 2 By laenger seit 28.09.92 */
        } TYdbmATT;

/* Struktur fuer Zieldaten                                   */

struct ZIELF  
{
           char  *feldbez;         /* Zeiger auf Array mit Feldbezeichnungen */
           short nlen;             /* Laenge eines Feldnamens mit Stringende */
           short *feldpos;         /* Zeiger auf Array mit Feldpositionen    */
           short *feldlen;         /* Zeiger auf Array mit Feldlaengen       */
           short *feldtyp;         /* Zeiger auf Aray mit Feldtypen          */
           void  (**fetchprog) (); /* Zeiger auf Array mit Uebertr.-Prozed.  */
           short *feldanz;         /* Zeiger auf Anzahl Felder               */
};

/* Headersatz des Scnittstellenfiles                    */

struct HD
{
     ushort        klen;
     char          dname[14];
     ushort        vz;
     ushort        rz;
     long          dttm;
     char          prnr [12];
     char          kun [80];
     char          sprname[40]; 
     ushort        vSW;
     ushort        rSW;
     long          dttmSW;
     char          prnrSW [12];
     uchar         pafeld [80];
     char          reserve [80];
     ushort        nrziel;
     ushort        feldanz;
     TYdbmATT      feldbez [100];
     ushort        blklen;
     unsigned long blkanz;
};


short bpos [MAXFELDER];                 /* Feldpositionen im Satz   */
short blen [MAXFELDER];                 /* Feldlaenge               */
short btyp [MAXFELDER];                 /* Feldtyp                  */
extern short banz;                      /* Anzahl Felder im Satz    */
