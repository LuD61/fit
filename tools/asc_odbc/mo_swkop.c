#ifndef SINIX
#ident  "@(#) mo_swkopf.c	2.001g	16.07.96	by RO"
#endif
/*                                DATE   17.05.1993         */
/*                                BY     Ro                 */
/*                                                          */
/*   TERMINOLOGY                                            */
/*                                                          */
/*   DESCRIPTION                                            */
/*       Liest und inerbretiert SW-Koepfe                   */
/*                                                          */
/*   EXPORT                    DESCRIPTION                  */
/*                                                          */
/*   USES                      FROM                         */
/*      <procedure>              <package>                  */
/*                                                          */
/*   INTERNAL                  DESCRIPTON                   */
/*      <procedure>              <procedure_beschreibung>   */
/*----------------------------------------------------------*/
/*                                                          */
/*
-       lfd. -  Version Datum       Name        Beschreibung
-       ----------------------------------------------------------------------
*/

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <malloc.h>
#include "swcomhd.h"
#include "swcomhd3.h"

#define KOPFLEN2 1346
#define KOPFLEN3 2946
#define bezeichner kopfsatz.feldbez
#define bezeichner3 kopfsatz3.feldbez

#define STRING 0
#define SHORT  1
#define LONG   2
#define INT    3
#define CHAR   4
#define HEX    6

void read_k ();         /* Kopfsatz feldweise in HD-Struktur einlesen */
void read_k2 ();        /* Kopfsatz feldweise in HD-Struktur einlesen */
void read_k3 ();        /* Kopfsatz feldweise in HD-Struktur einlesen */
void make_satz ();      /* Satzaufbau konstruieren                    */       
void make_satz2 ();     /* Satzaufbau konstruieren Version 2.x        */       
void make_satz3 ();     /* Satzaufbau konstruieren Version 3.x        */       
void sw_file ();        /* Deskripor fuer SW-Datei setzen             */
void sw_db ();          /* Flags anzeigen und test setzen             */ 
void swnamen ();        /* SWnamen aus Kopf anzeigen.                 */
char *swname ();        /* Feldname aus SW-Kopf             */

static short get_typ ();     /* Typ aus Bezeichner holen         */
static short get_len ();     /* Laenge bezeichner holen          */
static int   dec_stellen (); /* Laenge fuer numerische Felder    */
static int showfelder ();    /* SW-felder anzeigen.              */

short swversion;                 /* SW-Version  Globale variable      */
short blklen;
short klen;
long blkanz;
short banz;              /* Anzahl Felder pro Satz                 */
long blkanz_p;           /* Position im Kopf fuer Anzahl Saetze    */

static int bytes;       /* Gelesene bytes                         */
static short last_typ;  /* Typ des letzten SW-Feldes              */  

static struct HD kopfsatz;       /* Struktur Kopfsatz Version 2 */
static struct HD3 kopfsatz3;     /* Struktur Kopfsatz Version 3 */
static int swfile = -1;          /* Deskriptor fuer SW-Dateo    */
static short test = 0;           /* Kennzeichen fuer Testmodus    */
static short anzeigen = 0;       /* Kennzeichen fuer Anzeigemodus */

void sw_file (file)
/**
Desktiptor fuer SW-Datei setzen.
**/
int file;
{
        swfile = file;
}

int varsatz ()
/**
Feststellen, on die Satzlaenge des SW-Satzes variabel ist.
**/
{
         if (last_typ == 2) return (1);
         return (0);
}

void sw_db (a_flag, t_flag)  
/** 
Flags anzeigen und test setzen            
**/ 
short a_flag, t_flag;
{
       anzeigen = a_flag;
       test = t_flag;
}


void read_k ()
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       read_k
-
-       In              :      
-
-       Out             :      
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Kopfdaten einlesen.
-                             
-
--------------------------------------------------------------------------------
**/
{
	char lastfields [10];

	bytes = read (swfile,&klen,sizeof (klen));
	if (bytes <= 0)
	{
			schreibelog (
                          "ERROR :Kein Daten fuer den Kopfsatz gefunden");
			exit (1);
	}
	switch (klen)
	{
		 case 1346 :
		       swversion = 2;
		       kopfsatz.klen = klen;
		       read_k2 ();
		       break;
		 case 2946 :
		       swversion = 3;
		       kopfsatz3.klen = klen;
		       read_k3 ();
		       break;
		default :
		       schreibelog ("ERROR :Falsche Kopflaenge %d",klen);
		       exit (1);
	}
	if (anzeigen)
	{
		schreibelog ("SW-Version %hd Kopflaenge %hd",
			swversion,klen);
	}
}
		      
void read_k2 ()
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       read_k2
-
-       In              :      
-
-       Out             :      
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Kopfdaten einlesen.
-                             
-
--------------------------------------------------------------------------------
**/
{
        int i;

	read (swfile,kopfsatz.dname,sizeof (kopfsatz.dname));
	read (swfile,&kopfsatz.vz,sizeof (kopfsatz.vz));
	read (swfile,&kopfsatz.rz,sizeof (kopfsatz.rz));
	read (swfile,&kopfsatz.dttm,sizeof (kopfsatz.dttm));
	read (swfile,kopfsatz.prnr,sizeof (kopfsatz.prnr));
	read (swfile,kopfsatz.kun,sizeof (kopfsatz.kun));
	read (swfile,kopfsatz.sprname,sizeof (kopfsatz.sprname));
	read (swfile,&kopfsatz.vSW,sizeof (kopfsatz.vSW));
	read (swfile,&kopfsatz.rSW,sizeof (kopfsatz.rSW));
	read (swfile,&kopfsatz.dttmSW,sizeof (kopfsatz.dttmSW));
	read (swfile,kopfsatz.prnrSW,sizeof (kopfsatz.prnrSW));
	read (swfile,kopfsatz.pafeld,sizeof (kopfsatz.pafeld));
	read (swfile,kopfsatz.reserve,sizeof (kopfsatz.reserve));
	read (swfile,&kopfsatz.nrziel,sizeof (kopfsatz.nrziel));
	read (swfile,&kopfsatz.feldanz,sizeof (kopfsatz.feldanz));
	for (i=0; i < 100; i ++) 
	{
	      bytes = read (swfile,kopfsatz.feldbez[i].att_symb,4);
	      bytes = read (swfile,&kopfsatz.feldbez[i].stellen,2);
	      bytes = read (swfile,&kopfsatz.feldbez[i].bits,2);
	      bytes = read (swfile,&kopfsatz.feldbez[i].bits1,2);
	}
	read (swfile,&kopfsatz.blklen,sizeof (kopfsatz.blklen));
	read (swfile,&kopfsatz.blkanz,sizeof (kopfsatz.blkanz));
        blkanz_p = klen - sizeof (kopfsatz.blkanz);
	blklen = kopfsatz.blklen;
	blkanz = kopfsatz.blkanz;
	banz = kopfsatz.feldanz / sizeof (TYdbmATT);
}


void read_k3 ()
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       read_k
-
-       In              :      
-
-       Out             :      
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Kopfdaten einlesen.
-                             
-
--------------------------------------------------------------------------------
**/
{
	int i;
	int bytes;

	bytes = read (swfile,kopfsatz3.dname,sizeof (kopfsatz3.dname));
	if (bytes == -1)
	{
		    schreibelog ("ERROR :Fehler %d",
                                  errno);
		    exit (1);
	}
	read (swfile,&kopfsatz3.vz,sizeof (kopfsatz3.vz));
	read (swfile,&kopfsatz3.rz,sizeof (kopfsatz3.rz));
	read (swfile,&kopfsatz3.dttm,sizeof (kopfsatz3.dttm));
	read (swfile,kopfsatz3.prnr,sizeof (kopfsatz3.prnr));
	read (swfile,kopfsatz3.kun,sizeof (kopfsatz3.kun));
	read (swfile,kopfsatz3.sprname,sizeof (kopfsatz3.sprname));
	read (swfile,&kopfsatz3.vSW,sizeof (kopfsatz3.vSW));
	read (swfile,&kopfsatz3.rSW,sizeof (kopfsatz3.rSW));
	read (swfile,&kopfsatz3.dttmSW,sizeof (kopfsatz3.dttmSW));
	read (swfile,kopfsatz3.prnrSW,sizeof (kopfsatz3.prnrSW));
	read (swfile,kopfsatz3.pafeld,sizeof (kopfsatz3.pafeld));
	read (swfile,kopfsatz3.reserve,sizeof (kopfsatz3.reserve));
	read (swfile,&kopfsatz3.nrziel,sizeof (kopfsatz3.nrziel));
	read (swfile,&kopfsatz3.feldanz,sizeof (kopfsatz3.feldanz));
	for (i=0; i < 100; i ++) 
	{
	      bytes = read (swfile,kopfsatz3.feldbez[i].att_symb,4);
	      bytes = read (swfile,&kopfsatz3.feldbez[i].stellen,2);
	      bytes = read (swfile,&kopfsatz3.feldbez[i].minWert,8);
	      bytes = read (swfile,&kopfsatz3.feldbez[i].maxWert,8);
	      bytes = read (swfile,&kopfsatz3.feldbez[i].bits,2);
	      bytes = read (swfile,&kopfsatz3.feldbez[i].bits1,2);
	}
	read (swfile,&kopfsatz3.blklen,sizeof (kopfsatz3.blklen));
	read (swfile,&kopfsatz3.blkanz,sizeof (kopfsatz3.blkanz));
        blkanz_p = klen - sizeof (kopfsatz3.blkanz);
	blklen = kopfsatz3.blklen;
	blkanz = kopfsatz3.blkanz;
        banz   = kopfsatz3.feldanz /  26;  /* sizeof (TYdbmATT3) */
	return;
}


void make_satz (ufelder, pos, len, typ, anz)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       make_satz
-
-       In              :      
-
-       Out             :      
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Satz-Aufbau aus Kopfdaten aufbauen.
-                             
-
--------------------------------------------------------------------------------
**/
char ufelder[][5];
short *pos;                           /* Feldpositionen im Satz   */
short *len;                           /* Feldlaenge               */
short *typ;                           /* Feldtyp                  */
ushort anz;
{

	switch (swversion)
	{
		    case 2 :
			   make_satz2 (ufelder, pos, len, typ, anz);
			   break;
		    case 3 :
			   make_satz3 (ufelder, pos, len, typ, anz);
			   break;
	}
}

void make_satz2 (ufelder, pos, len, typ, anz)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       make_satz
-
-       In              :      
-
-       Out             :      
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Satz-Aufbau aus Kopfdaten aufbauen.
-                             
-
--------------------------------------------------------------------------------
**/
char ufelder[][5];
short *pos;                           /* Feldpositionen im Satz   */
short *len;                           /* Feldlaenge               */
short *typ;                           /* Feldtyp                  */
ushort anz;
{
	short i, j;

	bpos [0] = 0;        
	banz = kopfsatz.feldanz / sizeof (TYdbmATT);

	for (i = 0; i < banz; i ++)
	{
		    if (i > 0)
		    {
			 bpos[i] = bpos[i - 1] + blen [i - 1];
		    }
		    btyp [i] = get_typ (bezeichner [i].bits);
		    blen[i]  = get_len (bezeichner[i].stellen,btyp [i]);
	}
        if (i) last_typ = btyp [i - 1];

	for (j = 0; j < anz; j ++)
	{
	       for (i = 0; i < banz; i ++)
	       {

			 if (memcmp (ufelder [j],bezeichner [i].att_symb, 
				     sizeof (bezeichner [i].att_symb)) == 0)
			 {
				     typ [j] = btyp [i];
				     pos [j] = bpos [i];
				     len [j] = blen [i];
			  }
	       }
	}
	showfelder (ufelder, pos, len, typ, anz);
}

void make_satz3 (ufelder, pos, len, typ, anz)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       make_satz3
-
-       In              :      
-
-       Out             :      
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Satz-Aufbau aus Kopfdaten aufbauen.
-                             
-
--------------------------------------------------------------------------------
**/
char ufelder[][5];
short *pos;                           /* Feldpositionen im Satz   */
short *len;                           /* Feldlaenge               */
short *typ;                           /* Feldtyp                  */
ushort anz;
{
	short i, j;

	bpos [0] = 0;        
        banz = kopfsatz3.feldanz /  26;  /* sizeof (TYdbmATT3) */

	for (i = 0; i < banz; i ++)
	{
		    if (i > 0)
		    {
			 bpos[i] = bpos[i - 1] + blen [i - 1];
		    }
		    btyp [i] = get_typ (bezeichner3[i].bits);
		    blen [i] = get_len (bezeichner3[i].stellen,btyp [i]);
	}

        if (i) last_typ = btyp [i - 1];

	for (j = 0; j < anz; j ++)
	{
	       for (i = 0; i < banz; i ++)
	       {
			 if (memcmp (ufelder [j],bezeichner3[i].att_symb, 
				     sizeof (bezeichner3[i].att_symb)) == 0)
			 {
				     typ [j] = btyp [i];
				     pos [j] = bpos [i];
				     len [j] = blen [i];
			  }
	       }
	}
	showfelder (ufelder, pos, len, typ, anz);
}


get_bits (lwert,offset,len)
unsigned lwert;
short offset,len;
{
       unsigned maske;
       short i;

       if (len < 1)
       {
		     return (0);
       } 
       maske = 1;
       for (i = 1; i < len; i ++)
       {
		     maske = (maske << 1) | 1;
       }
       lwert = (lwert >> offset) & maske;
       return (lwert);
}

short get_typ (flags)
/**
Typ holen. 
**/
ushort flags;
{
       return (get_bits (flags,0,4));
}

short get_len (stellen,typ)
/**
Laenge eines Feldes holen;
**/
short stellen, typ;
{

	switch (typ)
	{
		   case 1 :
		   case 2 :
			     return (stellen);
		   case 3 :
               case 10 :
                       return ((short) dec_stellen (stellen));
		   case 4 :
			     return (0);
		   case 5 :
		   case 6 :
			     return (dec_stellen (stellen));
		   case 7 :
		   case 8 :
			     return (sizeof (short));
           case 9 :
           case 11 :
			     return (stellen);
       }
       return (0);
}

int dec_stellen (stellen)
/**
Laenge in Abhaengigkeit der Dezimalstellen zurueckgeben.
**/
short stellen;
{

       if (stellen <= 4)
       {
		    return (sizeof (short));
       }
       else if (stellen <= 9)
       { 
		    return (sizeof (long));
       }
       else if (stellen > 9)
       {
		    return (sizeof (TYint64));
       }
       return (0);
}     

showfelder (ufelder, pos, len, typ, anz)
/**
SW-Felder anzeigen.
**/
char ufelder[][5];
short *pos;
short *len;
short *typ;
short anz;
{
	   short j;
           char bez [5];

           if (test == 0) return;

           for (j = 0; j < banz; j ++)
           {
                      memcpy (bez,swname(j),4);
                      bez [4] = 0;
                      testausgabe ("\n");
           }
                      
	   for (j = 0; j < anz; j ++)
	   {
		      testausgabe ("%-10s %d  %d  %d\n",ufelder [j],
						    typ [j],
						    pos [j],
						    len [j]);
	   } 
}

char *swname (pos)
/**
Feldname aus Kopfsatz zurueckgeben.
**/
short pos; 
{
          switch (swversion) 
          {
                     case 2 :
                               return (bezeichner[pos].att_symb);
                     case 3 :
                               return (bezeichner3[pos].att_symb);
          }
}

void swnamen ()
/**
SW-Namen in aktuellem Kopf anzeigen.
**/
{
           short i;
           char bez [5];
 
           for (i = 0; i < banz; i ++)
           {
                      memcpy (bez,swname(i),4);
                      bez [4] = 0;
                      printf ("%s\n",bez);
           }
}
