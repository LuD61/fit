#ifndef SINIX
#ident  "@(#) split.h	2.000d	11.04.95	by RO"
#endif
#define TAB 9

static char *wort[0x1000];         /* Bereich fuer Woerter eines Strings    */

static split (string)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       split
-
-       In              :       string
-
-       Out             :       
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      String in Woerter aufteilen.
-
--------------------------------------------------------------------------------
**/
char *string;
{

        static short wz = 0;         /* Wortzaehler                           */

        short i;
        short j;
        short len;

        for (i = 1; i <= wz; i ++)
        {
                 free (wort[i]);
        }
        wz = 0;
        i = 0;
        len = strlen (string);

        while (1)
        {
                 while (1)
                 {
                         if (i >= len)
                         {
                                  return (wz);
                         }
                         if (string[i] > ' ')
                         {
                                  break;
                         }
                         i ++;
                  }  

                  j = 0;
                  wz ++;
                  wort[wz] = (char *) malloc (256);
                  while (1)
                  {
                         if (i >= len)
                         {
                                  wort[wz][j] = 0;
                                  return (wz);
                         }
                         if ((string[i] == ' ') ||
                             (string [i] == TAB))
                         {
                                  wort[wz][j] = 0;
                                  break;
                         }
                         wort[wz][j] = string[i];
                         i ++;
                         j ++;
                  }
        } 
        return (wz);
}
         

static split_s (string)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       split
-
-       In              :       string
-
-       Out             :       
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      String in Woerter aufteilen.
-                              '"' gilt als Schutz
--------------------------------------------------------------------------------
**/
char *string;
{

        static short wz = 0;         /* Wortzaehler                           */

        short i;
        short j;
        short len;
        short in_hk;

        for (i = 1; i <= wz; i ++)
        {
                 free (wort[i]);
        }
        wz = 0;
        i = 0;
        len = strlen (string);

        while (1)
        {
                 while (1)
                 {
                         if (i >= len)
                         {
                                  return (wz);
                         }
                         if (string[i] > ' ')
                         {
                                  break;
                         }
                         i ++;
                  }  

                  j = 0;
                  wz ++;
                  wort[wz] = (char *) malloc (256);
                  if (string [i] == '\"')    
                  {
                          in_hk = 1;
                          i ++;
                  }
                  else
                  {
                          in_hk = 0;
                  }
                  while (1)
                  {
                         if (i >= len)
                         {
                                  wort[wz][j] = 0;
                                  return (wz);
                         }
                         if (in_hk && string [i] == '\"')
                         {
                                  i ++;
                                  in_hk = 0;
                         }
                         else if (string [i] == '\"')
                         {
                                  in_hk = 1;
                                  i ++;
                         }
                         if (in_hk);
                         else if ((string[i] == ' ') ||
                             (string [i] == TAB))
                         {
                                  wort[wz][j] = 0;
                                  break;
                         }
                         wort[wz][j] = string[i];
                         i ++;
                         j ++;
                  }
        } 
        return (wz);
}
