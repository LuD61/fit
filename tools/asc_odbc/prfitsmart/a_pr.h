#ifndef A_PR_OK
#define A_PR_OK 1
struct A_PR
{
    double a;
    short akt;
    short delstatus;
    short fil;
    short fil_gr;
    double key_typ_dec13;
    short key_typ_sint;
    char lad_akv[2];
    char lief_akv[2];
    short mdn;
    short mdn_gr;
    char modif[2];
    double pr_ek;
    double pr_ek_euro;
    double pr_vk;
    double pr_vk_euro;
    long bearb;
    char pers_nam[9];
    double pr_vk1;
    double pr_vk2;
    double pr_vk3;
    };

extern struct A_PR _a_pr;

int lese_a_prf (double);
int lese_a_pr (void);
int lese_a_pr_mdnf (double, short);
int lese_a_pr_mdn (void);
int lese_a_pr_filf (double, short, short);
int lese_a_pr_fil (void);
int lese_a_pr_hf (short, short, short, short, double);
int lese_a_pr_h (void);
int update_a_pr (double, short, short, short, short);
int delete_a_pr (double, short, short, short, short);
int gueltig_a_pr (void);
#endif
