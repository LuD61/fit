#include <windows.h>

#define EXPORT _declspec (dllexport)

EXPORT void setdlldbase (HENV he, HDBC hb);

EXPORT int fetch_preis_tag (short mgruppe,
                     short mandant,
                     short fgruppe,
                     short filiale,
                     double artikel,
                     char   *datum,
                     double *ek_pr,
                     double *vk_pr);

EXPORT void ohnefilbelosa ();

EXPORT int fetch_preis_lad (short mgruppe,
                            short mandant,
                            short fgruppe,
                            short filiale,
                            double artikel,
                            double *prek,
                            double *prvk);

EXPORT int preise_holen (short dmdn, short dfil, short dkun_fil, 
						 int dkun, double da, char *ddatum, 
						 short *sa, double *pr_ek, double *pr_vk);
EXPORT void SetQuickSearch (int mode);



