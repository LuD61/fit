#ifndef AKTION_OK
#define AKTION_OK 1
struct AKT_KOPF {
   short     akt;
   char      akt_bz[25];
   short     akt_kte;
   long      bel_akt_bis;
   long      bel_akt_von;
   short     fil;
   short     fil_gr;
   char      gue[2];
   DATE_STRUCT lad_akt_bis;
   DATE_STRUCT lad_akt_von;
   DATE_STRUCT lad_akv_sa[2];
   DATE_STRUCT lief_akv_sa[2];
   short     mdn;
   short     mdn_gr;
   char      zeit_von[7];
   char      zeit_bis[7];
   short     zeit_von_n;
   short     zeit_bis_n;
};

struct AKT_POS {
   double    a;
   short     akt;
   short     key_typ_sint;
   double    pr_ek_sa;
   double    pr_ek_sa_euro;
   double    pr_vk_sa;
   double    pr_vk_sa_euro;
};

extern struct AKT_KOPF akt_kopf, akt_kopf_null;
extern struct AKT_POS akt_pos, akt_pos_null;

int lese_akt_kopff (short);
int lese_akt_kopf (void);
int lese_akt_kopf_df (short, short, short, short, char *);
int lese_akt_kopf_d (void);
int lese_guef (short, short, short);
int lese_gue ();
int lese_akt_posf (short);
int lese_akt_pos (void);
int lese_akt_pos_af (short, double);
int lese_akt_pos_a (void);
void close_akt_kopf (void);
void close_akt_kopf_d (void);
void close_gue (void);
void close_akt_pos (void);
void close_akt_pos_a (void);
#endif
