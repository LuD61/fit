#ifndef AKT_KRZ_OK
#define AKT_KRZ_OK 1
struct AKT_KRZ {
   double    a;
   DATE_STRUCT bel_akt_bis;
   DATE_STRUCT bel_akt_von;
   short     fil;
   short     fil_gr;
   DATE_STRUCT lad_akt_bis;
   DATE_STRUCT lad_akt_von;
   char      lad_akv_sa[2];
   char      lief_akv_sa[2];
   short     mdn;
   short     mdn_gr;
   double    pr_ek_sa;
   double    pr_ek_sa_euro;
   double    pr_vk_sa;
   double    pr_vk_sa_euro;
   char      zeit_bis[7];
   char      zeit_von[7];
   short     zeit_von_n;
   short     zeit_bis_n;
   double    pr_vk1_sa;
   double    pr_vk2_sa;
   double    pr_vk3_sa;
   char      dr_status[2];
};

extern struct AKT_KRZ _akt_krz, _akt_krz_null;
#endif
