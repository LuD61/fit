/***
_-------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-       A C H T U N G     Version Marktkauf
-       Modulname               :       mo_a_pr.cpp
-
        Exports                 :
					proc wr_a_pr
					proc fetch_a_pr_r
                                        proc fetch_preis
                                        proc fetch_preis_lad
                                        proc fetch_preis_tag

-       Interne Prozeduren      :

-
-       Autor                   :       RO
-       Erstellungsdatum        :       17.01.92
-       Modifikationsdatum      :       TT.MM.JJ
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       UNIX
-       Sprache                 :       ESQL-C
-
-       Aenderungsjournal       :
-
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Schreiben in die Tabelle a_pr.
                                    Hirarchisches Lesen in a_pr.
-
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-
--------------------------------------------------------------------------------
***/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#ifdef WIN32
#include <windows.h>
#endif
#include "strfkt.h"
#ifdef ODBC
#include "dbclass.h"
#else
#include "mo_curso.h"
#endif
//#include "mdn.h"
//#include "fil.h"
#include "a_pr.h"
#include "akt_krz.h"
#include "aktion.h"
#include "mo_a_pr.h"
#include "dll.h"

// extern int _WMDEBUG;
// extern int writelog (char *, ...);
//extern void disp_mess (char *, int modus);



static int tst = 0;
static int dsqlstatus;
static int mit_flb = 1;
static short  k_mgruppe;
static short k_mandant;
static short k_fgruppe;
static short k_filiale;
static double k_artikel;
static char k_datum[11];
static double  ek_preis;
static double  vk_preis;
static double  ek_preis_euro;
static double  vk_preis_euro;
static char gueltig [2];

struct
  {
    double pr_ek_sa;
    double pr_vk_sa;
    double pr_ek_sa_euro;
    double pr_vk_sa_euro;
  }  aktkrz;

struct
{ double pr_vk1;
  double pr_vk2;
  double pr_vk3;
} pr_grnd;

struct
{
  short  lfd;
  double pr_staffel;
  double gew_staffel;
  double toler_gew;
  short  pr_kz;
  short  pr_wrt;
} staffel;

short   a_pr_wr_prep    = 0;
short   a_pr_rd_prep    = 0;
short   a_pr_rd_prep0   = 0;
short   akt_krz_prep    = 0;
short   akt_lad_prep    = 0;
short   ek_vk_prep      = 0;
short   akt_ek_vk_prep  = 0;

extern char sql[];

/* Procecuren        */

static short hole_aktion (short, double *,double *);
static short hole_gueltig (double *, double *);
static int fetch_akt_dat (void);
static int fetch_akt_kopf_dat (short, short, short, short,
                              double, char *);
static int akt_comp (void);
static int debug (char *, ...);

struct AKT_KRZ _akt_krz, _akt_krz_null;

void mitfilbelosa (void)
{
           mit_flb = 1;
}

EXPORT void ohnefilbelosa ()
{
           mit_flb = 0;
}

int fetch_a_pr  (short mgruppe, short mandant,
                 short fgruppe, short filiale,
                 double artikel)

/**--------------------------------------------------------------------------------
-
-       Procedure       :       fetch_a_pr
-
-       In              :       mgruppe
                                mandant
                                fgruppe
                                filiale
                                artikel
-
-       Out             :       sqlstatus
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Eintrag in a_pr holen.
-
--------------------------------------------------------------------------------
**/

{

/* Parameter uebertragen             */

         k_mgruppe = mgruppe;
         k_mandant = mandant;
         k_fgruppe = fgruppe;
         k_filiale = filiale;
         k_artikel = artikel;


          _a_pr.pr_ek = 0;
          _a_pr.pr_vk = 0;

          debug ("fetch_a_pr : mdn_gr %hd mdn %hd fil_gr %hd, fil %hd a %.0lf",
                  k_mgruppe, k_mandant, k_fgruppe, k_filiale,artikel);

          dsqlstatus = lese_a_pr_hf (k_mgruppe, k_mandant,
                                  k_fgruppe, k_filiale, artikel);

          debug ("fetch_a_pr ek %.2lf vk = %.2lf", _a_pr.pr_ek,
                                                   _a_pr.pr_vk);
/* fetch_a_pr rekursiv aufrufen, wenn die Unternehmensebene > 0 ist  */

          if (dsqlstatus == 100)
           {
                 if (filiale > 0)
                 {
                      return (fetch_a_pr   (k_mgruppe,
                                            k_mandant,
                                            k_fgruppe,
                                            0,
                                            k_artikel));
                 }
                 else if (fgruppe > 0)
                 {
                      return (fetch_a_pr   (k_mgruppe,
                                            k_mandant,
                                            0,
                                            0,
                                            k_artikel));
                 }
                 else if (mandant > 0)
                 {
                      return (fetch_a_pr   (k_mgruppe,
                                            0,
                                            0,
                                            0,
                                            k_artikel));
                 }
                 else if (mgruppe > 0)
                 {
                      return (fetch_a_pr   (0,
                                            0,
                                            0,
                                            0,
                                            k_artikel));
                 }
          }
          return (dsqlstatus);
}


EXPORT int fetch_preis_lad (short mgruppe,
                     short mandant,
                     short fgruppe,
                     short filiale,
                     double artikel,
                     double *prek,
                     double *prvk)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       fetch_preis_lad
-
-       In              :       mgruppe
                                mandant
                                fgruppe
                                filiale
                                artikel
                                &pr_ek
                                &pr_vk
-
-       Out             :       pr_ek
                                pr_vk
                                sa         0 keine Aktion
                                           1 Aktion
                                           2 Aktion auf gueltig gesetzt.
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Preis aus a_pr holen.
                                Wenn Sonderaktionen existieren, werden
                                die Preise aus akt_krz zurueckgegeben,
                                sonst die Preise aus a_pr
-
--------------------------------------------------------------------------------
**/
{
        int sa;
        double ek, vk;



        ek  = 0.0;
        vk = 0.0;
        *prek = 0.0;
        *prvk = 0.0;
        sa = 0;

        k_mgruppe = mgruppe;
        k_mandant = mandant;
        k_fgruppe = fgruppe;
        k_filiale = filiale;
        k_artikel = artikel;


/* Preise lesen                                      */

       dsqlstatus =  fetch_a_pr (mgruppe, mandant,
                                   fgruppe, filiale,artikel);
         

       if (dsqlstatus)
       {
                    return (0);
       }

       if (_a_pr.pr_ek == -1)
       {
                    sqlstatus = 100;
                    return (0);
       }
       k_mgruppe = mgruppe;
       k_mandant = mandant;
       k_fgruppe = fgruppe;
       k_filiale = filiale;
       k_artikel = artikel;


       *prek = _a_pr.pr_ek;
       *prvk = _a_pr.pr_vk;

       pr_grnd.pr_vk1 = _a_pr.pr_vk1;
       pr_grnd.pr_vk2 = _a_pr.pr_vk2;
       pr_grnd.pr_vk3 = _a_pr.pr_vk3;

       if (_a_pr.lad_akv[0] == '1')
       {
             if (_a_pr.akt > 0)
             {
                    sa = hole_aktion (_a_pr.akt,prek,prvk);
             }
       }

       if (_a_pr.akt == -1)
       {
             sa  = hole_gueltig (prek,prvk);
       }

       if ((*prek == 0) && (*prvk == 0))
       {
                    if (filiale > 0)
                    {
                               sa = fetch_preis_lad (mgruppe,
                                                     mandant,
                                                     fgruppe,
                                                     0,
                                                     artikel,
                                                     prek,
                                                     prvk);
                    }
                    else if (fgruppe > 0)
                    {
                               sa = fetch_preis_lad (mgruppe,
                                                     mandant,
                                                     0,
                                                     0,
                                                     artikel,
                                                     prek,
                                                     prvk);
                    }
                    else if (mandant > 0)
                    {
                               sa = fetch_preis_lad (mgruppe,
                                                     0,
                                                     0,
                                                     0,
                                                     artikel,
                                                     prek,
                                                     prvk);
                    }
                    else if (mgruppe > 0)
                    {
                               sa = fetch_preis_lad (0,
                                                     0,
                                                     0,
                                                     0,
                                                     artikel,
                                                     prek,
                                                     prvk);
                    }
       }
       return (sa);
}

short hole_aktion (short aktion, double *prek,double *prvk)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       hole_aktion
-
-       In              :       aktion
                                *prek
                                *prvk
-
-       Out             :       prek
                                prvk
                                sa
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Aktionspreis holen.
-
--------------------------------------------------------------------------------
**/
{


        dsqlstatus = lese_akt_pos_af (aktion,_a_pr.a);
        if (dsqlstatus == 0)
        {
                  *prek = akt_pos.pr_ek_sa;
                  *prvk = akt_pos.pr_vk_sa;
                  return (1);
        }
        return (0);
}

short hole_gueltig (double *prek, double *prvk)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       hole_gueltig
-
-       In              :       prek
                                prvk
-
-       Out             :       prek
                                prvk
                                sa
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Aktionspreis von hoeherer Ebene mit als
                                gueltig gekennzeichneter Aktion holen.
-
--------------------------------------------------------------------------------
**/
{
         short aktion;
         double ek;
         double vk;
         short mgruppe;
         short mandant;
         short fgruppe;
         short filiale;
         short sa;

         sa = 0;
         ek = (double) 0.0;
         vk = (double) 0.0;

         mgruppe = _a_pr.mdn_gr;
         mandant = _a_pr.mdn;
         fgruppe = _a_pr.fil_gr;
         filiale = _a_pr.fil;

         while (TRUE)
         {
                  if (filiale > 0)
                  {
                            filiale = 0;
                  }
                  else if (fgruppe > 0)
                  {
                            fgruppe = 0;
                  }
                  else if (mandant > 0)
                  {
                            mandant = 0;
                  }
                  else
                  {
                            return (0);
                  }
                  dsqlstatus = lese_guef (mandant, fgruppe, filiale);
                  if (dsqlstatus == 0) break;
         }

         if (dsqlstatus) return 0;

         aktion = akt_kopf.akt;

         sa =  hole_aktion (aktion,prek,prvk);
         close_akt_pos_a ();
         if (sa == 0) return 0;
         return 2;
}

EXPORT int fetch_preis_tag (short mgruppe,
                     short mandant,
                     short fgruppe,
                     short filiale,
                     double artikel,
                     char   *datum,
                     double *ek_pr,
                     double *vk_pr)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       fetch_preis_tag
-
-       In              :       mgruppe
                                mandant
                                fgruppe
                                filiale
                                artikel
                                datum
-
-       Out             :       pr_ek
                                pr_vk
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Preis aus a_pr holen.
                                Wenn Sonderaktionen existieren, werden
                                die Preise aus akt_krz zurueckgegeben,
                                sonst die Preise aus a_pr
-
--------------------------------------------------------------------------------
**/
{

       double prek;
       double prvk;
       double prek_euro;
       double prvk_euro;
       short sa;
       static char  fil_bel_o_sa [3] = {"N "};
       static fil_bel_ok = 0;
       static short akt_mdn = 0;

       // tst = 2;

       debug ("mdn_gr %hd mdn %hd fil_gr %hd fil %hd a %.0lf",
               mgruppe, mandant, fgruppe, filiale, artikel);

       *ek_pr = (double) 0;
       *vk_pr = (double) 0;
       prek = (double) 0;
       prvk = (double) 0;
       sa = 0;


       strcpy (gueltig , " " );

       if (mit_flb)
       {
              dsqlstatus =  fetch_a_pr
                              (mgruppe,mandant,fgruppe,filiale,artikel);
              if (dsqlstatus)
              {
                               prek = prvk = (double) 0;
              }
              else
              {
                               prek = _a_pr.pr_ek;
                               prvk = _a_pr.pr_vk;
              }

              return (sa);
       }

/* Preise lesen                                      */

        k_mgruppe = mgruppe;
        k_mandant = mandant;
        k_fgruppe = fgruppe;
        k_filiale = filiale;
        k_artikel = artikel;
        strcpy (k_datum, datum);

        dsqlstatus = fetch_a_pr (mgruppe, mandant, fgruppe, filiale,
                                 artikel);
       if (_a_pr.pr_ek == -1 || dsqlstatus == 100)
       {
                        return (0);
       }

       prek = _a_pr.pr_ek;
       prvk = _a_pr.pr_vk;


       if (sa == 0)
       {
              fetch_akt_kopf_dat (mgruppe,
                                  mandant,
                                  fgruppe,
                                  filiale,
                                  artikel,
                                  datum);

              if (dsqlstatus || akt_comp ())
              {
                                sa = 0;
              }
              else
              {
                               sa = 1;
                               prek = _akt_krz.pr_ek_sa;
                               prvk = _akt_krz.pr_vk_sa;
              }
       }

       if (prek == (double) 0 && prvk == (double ) 0)
       {
                    if (filiale > 0)
                    {
                               sa = fetch_preis_tag
                                       (mgruppe,mandant,fgruppe,0,artikel,datum,
                                        ek_pr, vk_pr);
                               return (sa);
                    }
                    else if (fgruppe > 0)
                    {
                               sa = fetch_preis_tag
                                       (mgruppe,mandant,0,0,artikel,datum,
                                        ek_pr, vk_pr);
                               return (sa);
                    }
                    else if (mandant > 0)
                    {
                               sa = fetch_preis_tag (mgruppe, 0,0,0,artikel,
                                                     datum, ek_pr, vk_pr);
                               return (sa);
                    }
                    else if (mgruppe > 0)
                    {
                               sa = fetch_preis_tag
                                       (0,0,0,0,artikel,datum,ek_pr,vk_pr);
                               return (sa);
                    }
       }

       *ek_pr = prek;
       *vk_pr = prvk;
       debug ("sa = %d prek %.2lf prvk %.2lf nach fetch_preis_tag",
              sa,*ek_pr, *vk_pr);
       return (sa);
}

int fetch_akt_kopf_dat (short mgruppe, short mandant,
                        short fgruppe, short filiale,
                        double artikel, char *datum)
/**
Preis nach Datum in akt_kopf und akt_pos hierarchisch suchen.
**/
{

       short akt;
       short sql_status;

       dsqlstatus = lese_akt_kopf_df (mgruppe,
                                      mandant,
                                      fgruppe,
                                      filiale,
                                      datum);
       debug ("Nach lese_akt_kopf_d");

       while (1)
       {
              dsqlstatus = lese_akt_kopf_df (mgruppe, mandant,
                                             fgruppe, filiale,
                                             datum);
              while (dsqlstatus == 0)
              {
                       strcpy (gueltig, akt_kopf.gue);
                       akt = akt_kopf.akt;
                       dsqlstatus =
                                lese_akt_pos_af (akt,
                                                 artikel);
                       if (dsqlstatus == 0) 
                       {
                           _akt_krz.pr_ek_sa      = akt_pos.pr_ek_sa; 
                           _akt_krz.pr_vk_sa      = akt_pos.pr_vk_sa; 
                           break;
                       }
              }
              if (dsqlstatus == 0) break;
              if (filiale > 0)
              {
                        filiale = 0;
              }
              else if (fgruppe > 0)
              {
                        fgruppe = 0;
              }
              else if (mandant > 0)
              {
                        mandant = 0;
              }
              else if (mgruppe > 0)
              {
                        mgruppe = 0;
              }
              else
              {
                        break;
              }
       }
       _akt_krz.mdn_gr = mgruppe;
       _akt_krz.mdn    = mandant;
       _akt_krz.fil_gr = fgruppe;
       _akt_krz.fil    = filiale;
       sql_status      = dsqlstatus;
       close_akt_kopf_d ();
       close_akt_pos_a ();
       dsqlstatus = sql_status;
       return (dsqlstatus);
}

static int akt_comp ()
/**
Hierarchiestufe von a_pr und akt_krz vergleichen.
a_pr > akt_krz     return (1)
a_pr <= akt_krz    return (0)
**/
{
            if (gueltig [0] == 'J') return (0);

            if (_akt_krz.fil == 0 && _a_pr.fil > 0)
            {
                               return (1);
            }
            else if (_akt_krz.fil_gr == 0 && _a_pr.fil_gr > 0)
            {
                               return (1);
            }
            else if (_akt_krz.mdn == 0 && _a_pr.mdn > 0)
            {
                               return (1);
            }
            else if (_akt_krz.mdn_gr == 0 && _a_pr.mdn_gr > 0)
            {
                               return (1);
            }
            return (0);
}

static int debug (char *format, ...)
{
       va_list args;
       char buffer [256];

       if (tst == 0) return (0);

//       _WMDEBUG = 1;
       va_start (args, format);
       vsprintf (buffer, format, args);
       va_end (args);
       if (tst == 1)
       {
		            printf ("%s", buffer); 
//                  writelog (buffer);
       }
       else if (tst == 2)
       {
//                  disp_mess (buffer, 0);
       }
//       _WMDEBUG = 0;
       return (0);
}
