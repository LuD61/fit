#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#ifdef WIN32
#include <windows.h>
#endif
#include <math.h>
#ifdef ODBC
#include "dbclass.h"
#else
#include "mo_curso.h"
#endif
#include "strfkt.h"
#include "aktion.h"

struct AKT_KOPF akt_kopf, akt_kopf_null;
struct AKT_POS akt_pos, akt_pos_null;

static short cursor_akt_kopf = -1;
static short cursor_akt_kopf_d = -1;
static short cursor_gue = -1;
static short cursor_akt_pos = -1;
static short cursor_akt_pos_a = -1;
static DATE_STRUCT aktdat;

static void prepare (void);       
static void prepare_d (void);       
static void prepare_gue (void);       
static void prepare_pos (void);       
static void prepare_pos_a (void); 

void prepare  (void)
{
    ins_quest ((char *) &akt_kopf.akt, 1, 0);

    out_quest ((char *) &akt_kopf.akt,1,0);
    out_quest ((char *) akt_kopf.akt_bz,0,25);
    out_quest ((char *) &akt_kopf.akt_kte,1,0);
    out_quest ((char *) &akt_kopf.bel_akt_bis,SQLCDATE,0);
    out_quest ((char *) &akt_kopf.bel_akt_von,SQLCDATE,0);
    out_quest ((char *) &akt_kopf.fil,1,0);
    out_quest ((char *) &akt_kopf.fil_gr,1,0);
    out_quest ((char *) akt_kopf.gue,0,2);
    out_quest ((char *) &akt_kopf.lad_akt_bis,SQLCDATE,0);
    out_quest ((char *) &akt_kopf.lad_akt_von,SQLCDATE,0);
    out_quest ((char *) akt_kopf.lad_akv_sa,0,2);
    out_quest ((char *) akt_kopf.lief_akv_sa,0,2);
    out_quest ((char *) &akt_kopf.mdn,1,0);
    out_quest ((char *) &akt_kopf.mdn_gr,1,0);
    out_quest ((char *) akt_kopf.zeit_von,0,7);
    out_quest ((char *) akt_kopf.zeit_bis,0,7);
    out_quest ((char *) &akt_kopf.zeit_von_n,1,0);
    out_quest ((char *) &akt_kopf.zeit_bis_n,1,0);

    cursor_akt_kopf = prepare_sql ("select "
"akt_kopf.akt,  akt_kopf.akt_bz,  akt_kopf.akt_kte,  "
"akt_kopf.bel_akt_bis,  akt_kopf.bel_akt_von,  akt_kopf.fil,  "
"akt_kopf.fil_gr,  akt_kopf.gue,  akt_kopf.lad_akt_bis,  "
"akt_kopf.lad_akt_von,  akt_kopf.lad_akv_sa,  akt_kopf.lief_akv_sa,  "
"akt_kopf.mdn,  akt_kopf.mdn_gr,  akt_kopf.zeit_von,  "
"akt_kopf.zeit_bis,  akt_kopf.zeit_von_n,  akt_kopf.zeit_bis_n from akt_kopf "
                                          "where akt_kopf.akt = ?");

}
void prepare_d  (void)
{

    ins_quest ((char *) &akt_kopf.mdn_gr, 1, 0);
    ins_quest ((char *) &akt_kopf.mdn, 1, 0);
    ins_quest ((char *) &akt_kopf.fil_gr, 1, 0);
    ins_quest ((char *) &akt_kopf.fil, 1, 0);
    ins_quest ((char *) &aktdat, SQLCDATE, 0);
    ins_quest ((char *) &aktdat, SQLCDATE, 0);

    out_quest ((char *) &akt_kopf.akt,1,0);
    out_quest ((char *) akt_kopf.akt_bz,0,25);
    out_quest ((char *) &akt_kopf.akt_kte,1,0);
    out_quest ((char *) &akt_kopf.bel_akt_bis,SQLCDATE,0);
    out_quest ((char *) &akt_kopf.bel_akt_von,SQLCDATE,0);
    out_quest ((char *) &akt_kopf.fil,1,0);
    out_quest ((char *) &akt_kopf.fil_gr,1,0);
    out_quest ((char *) akt_kopf.gue,0,2);
    out_quest ((char *) &akt_kopf.lad_akt_bis,SQLCDATE,0);
    out_quest ((char *) &akt_kopf.lad_akt_von,SQLCDATE,0);
    out_quest ((char *) akt_kopf.lad_akv_sa,0,2);
    out_quest ((char *) akt_kopf.lief_akv_sa,0,2);
    out_quest ((char *) &akt_kopf.mdn,1,0);
    out_quest ((char *) &akt_kopf.mdn_gr,1,0);
    out_quest ((char *) akt_kopf.zeit_von,0,7);
    out_quest ((char *) akt_kopf.zeit_bis,0,7);
    out_quest ((char *) &akt_kopf.zeit_von_n,1,0);
    out_quest ((char *) &akt_kopf.zeit_bis_n,1,0);

    cursor_akt_kopf_d = prepare_sql ("select "
"akt_kopf.akt,  akt_kopf.akt_bz,  akt_kopf.akt_kte,  "
"akt_kopf.bel_akt_bis,  akt_kopf.bel_akt_von,  akt_kopf.fil,  "
"akt_kopf.fil_gr,  akt_kopf.gue,  akt_kopf.lad_akt_bis,  "
"akt_kopf.lad_akt_von,  akt_kopf.lad_akv_sa,  akt_kopf.lief_akv_sa,  "
"akt_kopf.mdn,  akt_kopf.mdn_gr,  akt_kopf.zeit_von,  "
"akt_kopf.zeit_bis,  akt_kopf.zeit_von_n,  akt_kopf.zeit_bis_n from akt_kopf "
                 "where akt_kopf.mdn_gr =  ? "
                 "and   akt_kopf.mdn    =  ? "
                 "and   akt_kopf.fil_gr =  ? "
                 "and   akt_kopf.fil    =  ? "
                 "and   akt_kopf.lad_akt_von <=  ? "
                 "and   akt_kopf.lad_akt_bis >=  ? ");
}


void prepare_gue  (void)
{
    ins_quest ((char *) &akt_kopf.mdn,    1, 0);
    ins_quest ((char *) &akt_kopf.fil_gr, 1, 0);
    ins_quest ((char *) &akt_kopf.fil,    1, 0);

    out_quest ((char *) &akt_kopf.akt,1,0);
    out_quest ((char *) akt_kopf.akt_bz,0,25);
    out_quest ((char *) &akt_kopf.akt_kte,1,0);
    out_quest ((char *) &akt_kopf.bel_akt_bis,SQLCDATE,0);
    out_quest ((char *) &akt_kopf.bel_akt_von,SQLCDATE,0);
    out_quest ((char *) &akt_kopf.fil,1,0);
    out_quest ((char *) &akt_kopf.fil_gr,1,0);
    out_quest ((char *) akt_kopf.gue,0,2);
    out_quest ((char *) &akt_kopf.lad_akt_bis,SQLCDATE,0);
    out_quest ((char *) &akt_kopf.lad_akt_von,SQLCDATE,0);
    out_quest ((char *) akt_kopf.lad_akv_sa,0,2);
    out_quest ((char *) akt_kopf.lief_akv_sa,0,2);
    out_quest ((char *) &akt_kopf.mdn,1,0);
    out_quest ((char *) &akt_kopf.mdn_gr,1,0);
    out_quest ((char *) akt_kopf.zeit_von,0,7);
    out_quest ((char *) akt_kopf.zeit_bis,0,7);
    out_quest ((char *) &akt_kopf.zeit_von_n,1,0);
    out_quest ((char *) &akt_kopf.zeit_bis_n,1,0);

    cursor_gue = prepare_sql ("select "
"akt_kopf.akt,  akt_kopf.akt_bz,  akt_kopf.akt_kte,  "
"akt_kopf.bel_akt_bis,  akt_kopf.bel_akt_von,  akt_kopf.fil,  "
"akt_kopf.fil_gr,  akt_kopf.gue,  akt_kopf.lad_akt_bis,  "
"akt_kopf.lad_akt_von,  akt_kopf.lad_akv_sa,  akt_kopf.lief_akv_sa,  "
"akt_kopf.mdn,  akt_kopf.mdn_gr,  akt_kopf.zeit_von,  "
"akt_kopf.zeit_bis,  akt_kopf.zeit_von_n,  akt_kopf.zeit_bis_n from akt_kopf "
                     "where akt_kopf.mdn = ? "
                     "and   akt_kopf.fil_gr = ? "
                     "and   akt_kopf.fil = ? "
                     "and   akt_kopf.lad_akv_sa = \"1\" "
                     "and   akt_kopf.gue = \"J\"");
}


void prepare_pos  (void)
{
    ins_quest ((char *) &akt_pos.akt, 1, 0);
    out_quest ((char *) &akt_pos.a,3,0);
    out_quest ((char *) &akt_pos.akt,1,0);
    out_quest ((char *) &akt_pos.key_typ_sint,1,0);
    out_quest ((char *) &akt_pos.pr_ek_sa,3,0);
    out_quest ((char *) &akt_pos.pr_ek_sa_euro,3,0);
    out_quest ((char *) &akt_pos.pr_vk_sa,3,0);
    out_quest ((char *) &akt_pos.pr_vk_sa_euro,3,0);
    cursor_akt_pos = prepare_sql ("select akt_pos.a,  "
"akt_pos.akt,  akt_pos.key_typ_sint,  akt_pos.pr_ek_sa,  akt_pos.pr_ek_sa_euro,  "
"akt_pos.pr_vk_sa, akt_pos.pr_vk_sa_euro from akt_pos "
                                         "where akt_pos.akt = ? "
                                         "order by a ");
}

void prepare_pos_a  (void)
{
    ins_quest ((char *) &akt_pos.akt, 1, 0);
    ins_quest ((char *) &akt_pos.a, 3, 0);
    out_quest ((char *) &akt_pos.a,3,0);
    out_quest ((char *) &akt_pos.akt,1,0);
    out_quest ((char *) &akt_pos.key_typ_sint,1,0);
    out_quest ((char *) &akt_pos.pr_ek_sa,3,0);
    out_quest ((char *) &akt_pos.pr_ek_sa_euro,3,0);
    out_quest ((char *) &akt_pos.pr_vk_sa,3,0);
    out_quest ((char *) &akt_pos.pr_vk_sa_euro,3,0);
    cursor_akt_pos_a = prepare_sql ("select akt_pos.a,  "
"akt_pos.akt,  akt_pos.key_typ_sint,  akt_pos.pr_ek_sa,  akt_pos.pr_ek_sa_euro, "
"akt_pos.pr_vk_sa, akt_pos.pr_vk_sa_euro  from akt_pos "
                                           "where akt_pos.akt = ? "
                                           "and   akt_pos.a = ?");
}

int lese_akt_kopff (short akt)
/**
Tabelle akt_kopf fuer Aktion lesen.
**/
{
         if (cursor_akt_kopf == -1)
         {
                      prepare ();
         }
         akt_kopf.akt = akt;
         open_sql (cursor_akt_kopf);
         fetch_sql (cursor_akt_kopf);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int lese_akt_kopf (void)
/**
Naechsten Satz aus Tabelle akt_kopf lesen.
**/
{
         fetch_sql (cursor_akt_kopf);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int lese_akt_kopf_df (short mdn_gr, short mdn, short fil_gr,
                                   short fil, char *datum)
/**
Tabelle akt_kopf fuer Aktion lesen.
**/
{
         if (cursor_akt_kopf_d == -1)
         {
                      prepare_d ();
         }
         akt_kopf.mdn_gr = mdn_gr;
         akt_kopf.mdn    = mdn;
         akt_kopf.fil_gr = fil_gr;
         akt_kopf.fil    = fil;
#ifdef ODBC
		 FromGerDate ((DATE_STRUCT *) &aktdat, datum);
#else
         aktdat = dasc_to_long (datum);
#endif

         open_sql (cursor_akt_kopf_d);
         fetch_sql (cursor_akt_kopf_d);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int lese_akt_kopf_d (void)
/**
Naechsten Satz aus Tabelle akt_kopf lesen.
**/
{
         fetch_sql (cursor_akt_kopf_d);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}


int lese_guef (short mdn, short fil_gr, short fil)
/**
Tabelle akt_kopf mit Kennzeichen gueltig lesen.
**/
{
         if (cursor_gue == -1)
         {
                      prepare_gue ();
         }
         akt_kopf.mdn    = mdn;
         akt_kopf.fil_gr = fil_gr;
         akt_kopf.fil    = fil;
         open_sql (cursor_gue);
         fetch_sql (cursor_gue);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}


int lese_gue (void)
/**
Naechsten Satz aus Tabelle akt_kopf lesen.
**/
{
         fetch_sql (cursor_gue);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}


int lese_akt_posf (short akt)
/**
Tabelle akt_pos fuer Aktion lesen.
**/
{
         if (cursor_akt_pos == -1)
         {
                      prepare_pos ();
         }
         akt_pos.akt = akt;
         open_sql (cursor_akt_pos);
         fetch_sql (cursor_akt_pos);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int lese_akt_pos (void)
/**
Naechsten Satz aus Tabelle akt_pos lesen.
**/
{
         fetch_sql (cursor_akt_pos);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int lese_akt_pos_af (short akt, double a)
/**
Tabelle akt_pos fuer Aktion und Artikel lesen.
**/
{
         if (cursor_akt_pos_a == -1)
         {
                      prepare_pos_a ();
         }
         akt_pos.akt = akt;
         akt_pos.a   = a;
         open_sql (cursor_akt_pos_a);
         fetch_sql (cursor_akt_pos_a);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int lese_akt_pos_a (void)
/**
Naechsten Satz aus Tabelle akt_pos lesen.
**/
{
         fetch_sql (cursor_akt_pos_a);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

void close_akt_kopf (void)
/**
Cursor akt_kopf schliessen.
**/
{
         if (cursor_akt_kopf == -1) return;

         close_sql (cursor_akt_kopf);
         cursor_akt_kopf = -1;
}

void close_akt_kopf_d (void)
/**
Cursor akt_kopf_d schliessen.
**/
{
         if (cursor_akt_kopf_d == -1) return;

         close_sql (cursor_akt_kopf_d);
         cursor_akt_kopf_d = -1;
}

void close_gue (void)
/**
Cursor gue schliessen.
**/
{
         if (cursor_gue == -1) return;

         close_sql (cursor_gue);
         cursor_gue = -1;
}


void close_akt_pos (void)
/**
Cursor akt_pos schliessen.
**/
{
         if (cursor_akt_pos == -1) return;

         close_sql (cursor_akt_pos);
         cursor_akt_pos = -1;
}

void close_akt_pos_a (void)
/**
Cursor akt_pos_a schliessen.
**/
{
         if (cursor_akt_pos_a == -1) return;

         close_sql (cursor_akt_pos_a);
         cursor_akt_pos_a = -1;
}
