#ifndef _WRITE_XML_DEF
#define _WRITE_XML_DEF
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#ifndef NULL
#define NULL (void *) 0
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef BOOL
typedef int BOOL;
#endif

#define TOKENMODE_ALL  0
#define TOKENMODE_ONE  1

typedef struct
{
	char *T;
	int Length;
	char *Sub;
} Text;

extern void TextInit (Text *);
extern void TextDelete (Text *);
extern void TextSet (Text *, char *);
extern void TextSetC (Text *, char, int);
extern void TextSetT (Text *, Text *);
extern void TextSetLen (Text *, char *, int);
extern char *TextGetBuffer (Text *);
extern char *TextSubString (Text *, int, int);
extern void TextMakeUpper (Text *text);
extern int TextCompare (Text *, Text *);
extern int TextCompareC (Text *, char *);
extern char *TextTrimLeft (Text *);
extern char *TextTrimRight (Text *);
extern char *TextTrim (Text *);
extern int TextFind (Text *, char *);
extern char *TextAddC (Text *, char);
extern char *TextPlus (Text *, char *);
extern char *TextPlusT (Text *, Text *);
extern char *TextFormat (Text *, char *, ...);

extern void ProcessExit (int);


typedef struct
{   
		   char  name [20];
           short length;
           char  type [2];
           short precision;
           short dimension;
		   Text value;
 } Field;

extern void FieldInit (Field *);
extern void FieldDelete (Field *);
extern void FieldSet (Field *, Text *, int, Text *, int);
extern char *FieldGetName (Field *, Text *);
extern int FieldGetLen (Field *);
extern char *FieldGetType (Field *, Text *);
extern int FieldGetPrecision (Field *);
extern void FieldLog (Field *, FILE *);

typedef struct
{
	Text Name;
	Text Value;
} Property;

extern void PropInit (Property *, Text*);
extern void PropDelete (Property *);
extern char *PropGet (Property *, Text *);
extern char *PropFind (Property *, Text *);
extern void PropLog (Property *, FILE *);

typedef struct 
{
	void **buffer;
	int elements;
	int idx;
} Vector;

extern void VectorInit (Vector *);
extern void VectorDestroy (Vector *);
extern void VectorAdd (Vector *,void *);
extern void VectorDrop (Vector *,void *);
extern void *VectorGetNext (Vector *);
extern void *VectorGet (Vector *, int);

typedef struct
{
	Vector v;
	Text Sep;
	char *buffer;
	int elements;
	int mode;
} Token;

extern void TokenInit (Token *);
extern void TokenDelete (Token *);
extern void TokenSet (Token *, Text *, Text *);
extern void TokenSetC (Token *, Text *, char *);
extern char *TokenGet (Token *, int);

typedef struct
{
  Text Name;
  FILE *fp;
  Text text;
} Log;

extern void LogInit (Log *);
extern void LogOutputT (Log *, Text *);
extern void LogOutput (Log *, char *);

typedef struct
{
     double value;
     Text text;
} Double;

void DoubleInit (Double *);
double DoubleSet (Double *, char *);
double StringToDouble (char *);

typedef struct
{
	Text Source;
	Text Target;
} ConvEntry;

extern void ConvEntryInit (ConvEntry *, Text*);
extern void ConvEntryDelete (ConvEntry *);
extern void ConvEntrySetString (Text *String, char *p);
extern char *ConvEntryGetTarget (ConvEntry *, Text *Source);
extern char *ConvEntryGetSorce (ConvEntry *, Text *Target);
extern void ConvEntryLog (ConvEntry *, FILE *);

typedef struct
{
	Vector Tab;
	BOOL IsLoaded;
	Text Name;
} ConvTab;

extern void ConvTabInit (ConvTab *);
extern void ConvTabDelete (ConvTab *);
extern char *ConvTabGetTarget (ConvTab *, Text *Source, int *pos);
extern char *ConvTabGetSorce (ConvTab *, Text *Target);
extern void ConvTabLog (ConvTab *, FILE *);
extern BOOL ConvTabLoad (ConvTab *, char *);
extern BOOL ConvTabChange (ConvTab *, char *, int len);
extern BOOL ConvTabChangeUTF8 (ConvTab *, char *, int len);
extern BOOL ConvTabChangeUTF16 (ConvTab *, char *, int len);

typedef struct
{
	Text AsString;
	unsigned int AsInt;
	unsigned char AsChar;
}  CVarChar;

extern void CVarCharInitS (CVarChar *vc, Text *AsString);
extern void CVarCharI (CVarChar *vc,int AsInt);
extern void CVarCharC (CVarChar *vc,char AsChar);
extern void CVarCharSetDecimal (CVarChar *vc, Text *AsString);
extern void CVarCharSetHex (CVarChar *vc,Text *AsString); 

#endif

