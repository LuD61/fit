# Microsoft Developer Studio Generated NMAKE File, Based on dtod.dsp
!IF "$(CFG)" == ""
CFG=dtod - Win32 Release
!MESSAGE No configuration specified. Defaulting to dtod - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "dtod - Win32 Release" && "$(CFG)" != "dtod - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "dtod.mak" CFG="dtod - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "dtod - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "dtod - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "dtod - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

!IF "$(RECURSE)" == "0" 

ALL : "C:\USER\FIT\BIN\dtod.exe"

!ELSE 

ALL : "C:\USER\FIT\BIN\dtod.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\DATUM.OBJ"
	-@erase "$(INTDIR)\DTOD.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\MO_ARG.OBJ"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_DTOD.OBJ"
	-@erase "$(INTDIR)\MO_EAN0.OBJ"
	-@erase "$(INTDIR)\MO_EAN_G.OBJ"
	-@erase "$(INTDIR)\MO_INTP.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\MO_SPAWN.OBJ"
	-@erase "$(INTDIR)\MO_SW.OBJ"
	-@erase "$(INTDIR)\MO_SWKOP.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\SYSDATE.OBJ"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "C:\USER\FIT\BIN\dtod.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /ML /w /W0 /GX /I"e:\INFORMIX\INCL\ESQL" /D "NDEBUG" /D\
 "WIN32" /D "_CONSOLE" /D "_MBCS" /D "CONSOLE" /Fp"$(INTDIR)\dtod.pch" /YX\
 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\dtod.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib isqlt07c.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)\dtod.pdb" /machine:I386 /out:"C:\USER\FIT\BIN\dtod.exe"\
 /libpath:"E:\INFORMIX\LIB" 
LINK32_OBJS= \
	"$(INTDIR)\DATUM.OBJ" \
	"$(INTDIR)\DTOD.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\MO_ARG.OBJ" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_DTOD.OBJ" \
	"$(INTDIR)\MO_EAN0.OBJ" \
	"$(INTDIR)\MO_EAN_G.OBJ" \
	"$(INTDIR)\MO_INTP.OBJ" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\MO_SPAWN.OBJ" \
	"$(INTDIR)\MO_SW.OBJ" \
	"$(INTDIR)\MO_SWKOP.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\SYSDATE.OBJ"

"C:\USER\FIT\BIN\dtod.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "dtod - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\dtod.exe"

!ELSE 

ALL : "$(OUTDIR)\dtod.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\DATUM.OBJ"
	-@erase "$(INTDIR)\DTOD.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\MO_ARG.OBJ"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_DTOD.OBJ"
	-@erase "$(INTDIR)\MO_EAN0.OBJ"
	-@erase "$(INTDIR)\MO_EAN_G.OBJ"
	-@erase "$(INTDIR)\MO_INTP.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\MO_SPAWN.OBJ"
	-@erase "$(INTDIR)\MO_SW.OBJ"
	-@erase "$(INTDIR)\MO_SWKOP.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\SYSDATE.OBJ"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(OUTDIR)\dtod.exe"
	-@erase "$(OUTDIR)\dtod.ilk"
	-@erase "$(OUTDIR)\dtod.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MLd /w /W0 /Gm /GX /Zi /Od /I "E:\INFORMIX\INCL\ESQL" /D\
 "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "CONSOLE"\
 /Fp"$(INTDIR)\dtod.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\dtod.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib isqlt07c.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)\dtod.pdb" /debug /machine:I386 /out:"$(OUTDIR)\dtod.exe"\
 /pdbtype:sept /libpath:"D:\INFORMIX\LIB" 
LINK32_OBJS= \
	"$(INTDIR)\DATUM.OBJ" \
	"$(INTDIR)\DTOD.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\MO_ARG.OBJ" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_DTOD.OBJ" \
	"$(INTDIR)\MO_EAN0.OBJ" \
	"$(INTDIR)\MO_EAN_G.OBJ" \
	"$(INTDIR)\MO_INTP.OBJ" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\MO_SPAWN.OBJ" \
	"$(INTDIR)\MO_SW.OBJ" \
	"$(INTDIR)\MO_SWKOP.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\SYSDATE.OBJ"

"$(OUTDIR)\dtod.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(CFG)" == "dtod - Win32 Release" || "$(CFG)" == "dtod - Win32 Debug"
SOURCE=..\DATUM.C

"$(INTDIR)\DATUM.OBJ" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\DTOD.C

"$(INTDIR)\DTOD.OBJ" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\MO_A_PR.C

!IF  "$(CFG)" == "dtod - Win32 Release"

DEP_CPP_MO_A_=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\allgemei.h"\
	"..\mo_a_pr.h"\
	"..\mo_curso.h"\
	"..\sqlam.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "dtod - Win32 Debug"

DEP_CPP_MO_A_=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\allgemei.h"\
	"..\mo_a_pr.h"\
	"..\mo_curso.h"\
	"..\sqlam.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\MO_ARG.C

"$(INTDIR)\MO_ARG.OBJ" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\MO_CURSO.C

!IF  "$(CFG)" == "dtod - Win32 Release"

DEP_CPP_MO_CU=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\mo_curso.h"\
	"..\strfkt.h"\
	"..\tcursor.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "dtod - Win32 Debug"

DEP_CPP_MO_CU=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\mo_curso.h"\
	"..\strfkt.h"\
	"..\tcursor.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\MO_DTOD.C

!IF  "$(CFG)" == "dtod - Win32 Release"

DEP_CPP_MO_DT=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\mo_curso.h"\
	

"$(INTDIR)\MO_DTOD.OBJ" : $(SOURCE) $(DEP_CPP_MO_DT) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "dtod - Win32 Debug"

DEP_CPP_MO_DT=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\mo_curso.h"\
	

"$(INTDIR)\MO_DTOD.OBJ" : $(SOURCE) $(DEP_CPP_MO_DT) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\MO_EAN0.C

!IF  "$(CFG)" == "dtod - Win32 Release"

DEP_CPP_MO_EA=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\mo_curso.h"\
	

"$(INTDIR)\MO_EAN0.OBJ" : $(SOURCE) $(DEP_CPP_MO_EA) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "dtod - Win32 Debug"

DEP_CPP_MO_EA=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\mo_curso.h"\
	

"$(INTDIR)\MO_EAN0.OBJ" : $(SOURCE) $(DEP_CPP_MO_EA) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\MO_EAN_G.C

"$(INTDIR)\MO_EAN_G.OBJ" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\MO_INTP.C

!IF  "$(CFG)" == "dtod - Win32 Release"

DEP_CPP_MO_IN=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\mo_curso.h"\
	

"$(INTDIR)\MO_INTP.OBJ" : $(SOURCE) $(DEP_CPP_MO_IN) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "dtod - Win32 Debug"

DEP_CPP_MO_IN=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\mo_curso.h"\
	

"$(INTDIR)\MO_INTP.OBJ" : $(SOURCE) $(DEP_CPP_MO_IN) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\MO_NUMME.C

!IF  "$(CFG)" == "dtod - Win32 Release"

DEP_CPP_MO_NU=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"..\sqlam.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "dtod - Win32 Debug"

DEP_CPP_MO_NU=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"..\sqlam.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\MO_PREIS.C

!IF  "$(CFG)" == "dtod - Win32 Release"

DEP_CPP_MO_PR=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\mo_curso.h"\
	"..\mo_preis.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "dtod - Win32 Debug"

DEP_CPP_MO_PR=\
	"E:\informix\incl\esql\sqlca.h"\
	"E:\informix\incl\esql\sqlda.h"\
	"E:\informix\incl\esql\sqlhdr.h"\
	"E:\informix\incl\esql\sqlproto.h"\
	"..\mo_curso.h"\
	"..\mo_preis.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\MO_SPAWN.C
DEP_CPP_MO_SP=\
	"..\mo_spawn.h"\
	"..\split.h"\
	

"$(INTDIR)\MO_SPAWN.OBJ" : $(SOURCE) $(DEP_CPP_MO_SP) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\MO_SW.C

"$(INTDIR)\MO_SW.OBJ" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\MO_SWKOP.C
DEP_CPP_MO_SW=\
	"..\swcomhd.h"\
	"..\swcomhd3.h"\
	

"$(INTDIR)\MO_SWKOP.OBJ" : $(SOURCE) $(DEP_CPP_MO_SW) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\STRFKT.C

"$(INTDIR)\STRFKT.OBJ" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\SYSDATE.C

"$(INTDIR)\SYSDATE.OBJ" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)



!ENDIF 

