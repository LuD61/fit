
#ifndef CPU68K

#include "objects.h"    /* Mario */

/* Funtionen fuer Structur Text */

void ProcessExit (int errcode)
{
	exit (errcode);
}

void O_ERROR (b,errcode)
{
	if (b)
	{
		ProcessExit (errcode);
	}
}

#define ERROR O_ERROR


void TextInit (Text *text)
{
	text->T = NULL;
	text->Length = 0;
	text->Sub = 0;
}

void TextDelete (Text *text)
{
	if (text->T != NULL)
	{
		free (text->T);
	}
	if (text->Sub != NULL)
	{
		free (text->Sub);
	}
	text->T = NULL;
	text->Sub = NULL;
	text->Length = 0;
}

void TextSet (Text *text, char *s)
{
	int len;

/*	TextDelete (text);  */

	if (text->T != NULL)
	{
		free (text->T);
	}
	text->T = NULL;
	text->Length = 0;
	len = strlen (s);
	text->T = (char *) malloc (len + 1);
	ERROR ((text->T == 0),1);
	text->Length = len;
	strcpy (text->T, s);
}

void TextSetC (Text *text, char c, int len)
{
	TextDelete (text);
	text->T = (char *) malloc (len + 1);
	ERROR ((text->T == 0),1);
	text->Length = len;
	memset (text->T, c, len);
	text->T[len] = 0;
}

void TextSetLen (Text *text, char *s, int len)
{
	TextDelete (text);
	text->T = (char *) malloc (len + 1);
	ERROR ((text->T == 0),1);
	text->Length = len;
	memcpy (text->T, s, len);
	text->T[len] = 0;
}

void TextSetT (Text *text, Text *t)
{
	TextSet (text, TextGetBuffer (t));
}


char *TextGetBuffer (Text *text)
{
	return (text->T);
}

char *TextSubString (Text *text, int start, int len)
{
	if (text->Sub != NULL)
	{
		free (text->Sub);
	}
	if (len < 0) len = text->Length - start;
	if (text->Length < start + len) len = text->Length - start;
	len = len < 0 ? 0 : len;
	text->Sub = (char *) malloc (len + 1);
	ERROR ((text->Sub == NULL), 1);
	memcpy (text->Sub, &text->T[start], len);
	text->Sub[len] = 0;
	return (text->Sub);
}

void TextMakeUpper (Text *text)
{
	int i;

	for (i = 0; i < text->Length; i ++)
	{
		text->T[i] = (char) toupper(text->T[i]);
	}
}

int TextCompare (Text *text1, Text *text2)
{
	return (strcmp (TextGetBuffer (text1), TextGetBuffer (text2)));
}

int TextCompareC (Text *text1, char *text2)
{
	return (strcmp (text1->T, text2));
}

char * TextTrimLeft (Text *text)
{
         int i;
         unsigned char *p;

		 if (text->Length == 0) return (text->T);
         p = (unsigned char *) malloc (text->Length + 1);
         if (p == NULL)
         {
             return NULL;
         }

         strcpy ((char *) p, text->T);
         for (i = 0; i < text->Length; i ++)
         {
             if (p[i] > (unsigned char ) ' ')
             {
                 break;
             }
         }
         TextSet (text, (char *) &p[i]);
         free (p);
		 return text->T;
}

char * TextTrimRight (Text *text)
{
         int i;
         unsigned char *p;

		 if (text->Length == 0) return (text->T);
         p = (unsigned char *) malloc (text->Length + 10);
         if (p == NULL)
         {
             return NULL;
         }

         strcpy ((char *) p, text->T);
         for (i = text->Length; i >= 0; i --)
         {
             if (p[i] > (unsigned char ) ' ')
             {
                 break;
             }
         }
         p [i + 1] = 0;
         TextSet (text, (char *) p);
         free (p);
		 return text->T;
}


char *TextTrim (Text *text)
{
	     TextTrimLeft (text);
	     return TextTrimRight (text);
}

int TextFind (Text *text, char *s)
{
	char *p;
	int len;
	int slen;
	int i;

	p = text->T;
	len = text->Length;
	slen = strlen (s);
	for (i = 0; i < len; i ++)
	{
		if (i + slen >= len) return (-1);
		if (p[i] == s[0] && (memcmp (&p[i], s, slen) == 0))
		{
			break;
		}
	}
	return (i);
}


char *TextAddC (Text *text1, char c)
{
	char *p;
	int plen;
	char b[2] = " ";

	plen  = text1->Length + 3;

	p = (char *) malloc (plen);
	ERROR ((p == NULL), 1);
	strcpy (p, text1->T);
	b[0] = c;
	strcat (p, b);
	TextSet (text1, p);
	free (p);
	return text1->T;
}

char *TextPlus (Text *text1, char *text2)
{
	char *p;
	int plen;

	plen  = text1->Length + strlen (text2) + 2;

	p = (char *) malloc (plen);
	ERROR ((p == NULL), 1);
	strcpy (p, text1->T);
	strcat (p, text2);
	TextSet (text1, p);
	free (p);
	return text1->T;
}

char *TextPlusT (Text *text1, Text *text2)
{
	return TextPlus (text1, text2->T);
}

char *TextFormat (Text *text, char *format, ...)
{
	va_list args;
	char buffer [512];

	va_start (args,format);
	vsprintf (buffer, format, args);
	va_end (format);
	TextSet (text, buffer);
	return (text->T);
}


/* Ende Funtionen fuer Structur Text */

/* Funtionen fuer Structur Field */

void FieldInit (Field *field)
{
	strcpy (field->name, "");
	field->length = 0;
	strcpy (field->type, "");
	field->precision = 0;
	field->dimension = 1;
	TextInit (&field->value);
}

void FieldDelete (Field *field)
{
	TextDelete (&field->value);
}

void FieldSet (Field *field, Text *name, int length, Text *type, int precision)
{
	strcpy (field->name, TextSubString (name, 0, 19));
	field->length = length;
	strcpy (field->type, TextSubString (type, 0, 1));
	field->precision = precision;
}
char *FieldGetName (Field *field, Text *text)
{
	TextSet (text, field->name);
	return field->name;
}

int FieldGetLen (Field *field)
{
	return field->length;
}

char *FieldGetType (Field *field, Text *text)
{
	TextSet (text, field->type);
	return field->type;
}
int FieldGetPrecision (Field *field)
{
	return field->precision;
}

void FieldLog (Field *field, FILE *fp)
{
	if (field->value.T == NULL)
	{
		fprintf (fp, "%s, %d, %s, %d, %d\n", field->name, 
			                                 field->length,
											 field->type,
											 field->precision,
											 field->dimension);
	}
	else
	{
		fprintf (fp, "%s, %d, %s, %d, %d %s\n", field->name, 
			                                 field->length,
											 field->type,
											 field->precision,
											 field->dimension,
											 field->value.T);
	}

}

/* Ende Funtionen fuer Structur Field */

/* Funtionen fuer Structur Property */

void PropInit (Property *p, Text *text)
{
	Token t;
	Text g;

	TextInit (&g);
	TextSet (&g, "=");
	TokenInit (&t);
	TokenSet (&t, text, &g);
	if (t.elements < 1) return;
	TextInit (&p->Name);
	TextInit (&p->Value);
	TextSet (&p->Name, TokenGet (&t, 0));
	TextTrim (&p->Name);
	if (t.elements > 1)
	{
		TextSet (&p->Value, TokenGet (&t, 1));
	}
	else
	{
		TextSet (&p->Value, "");
	}
	TextTrim (&p->Value);
}

void PropDelete (Property *p)
{
	TextDelete (&p->Name);
	TextDelete (&p->Value);
}

char *PropGet (Property *p, Text *Value)
{
	TextSetT (Value, &p->Value);
	return TextGetBuffer (&p->Value);
}
char *PropFind (Property *p, Text *Name)
{
	if (TextCompare (&p->Name, Name) == 0)
	{
		return TextGetBuffer (&p->Value);
	}
	return (NULL);
}

void PropLog (Property *p, FILE *fp)
{
	fprintf (fp, "%s = %s\n", p->Name.T, p->Value.T);
}

/* Ende Funtionen fuer Structur Property */

/* Funtionen fuer Structur Vector */

void VectorInit (Vector *v)
{
	v->buffer = NULL;
	v->elements = 0;
	v->idx = 0;
}
void VectorDestroy (Vector *v)
{
	int i;
	for (i = 0; i < v->elements; i ++)
	{
		free (v->buffer [i]);
	}
	v->buffer = NULL;
	v->elements = 0;
	v->idx = 0;
}

void VectorAdd (Vector *v, void *p)
{
	char **buffer;
	int i;

    buffer = (void *) malloc ((v->elements + 1) * sizeof (void *));
	ERROR ((buffer == NULL), 1);

	for (i = 0; i < v->elements; i ++)
	{
		buffer[i] = v->buffer[i];
	}
	buffer[i] = p;
	if (v->buffer != NULL)
	{
		free (v->buffer);
	}
	v->buffer = (void **) buffer;
	v->elements ++;
}

void VectorDrop (Vector *v, void *p)
{
	char **buffer;
	int i, j;

	if (v->elements <= 0) return;
	if (v->elements > 1)
	{
		buffer = (void *) malloc ((v->elements - 1) * sizeof (void *));
		ERROR ((buffer == NULL), 1);

		for (i = 0, j = 0; i < v->elements; i ++)
		{
			if (v->buffer [i] != p)
			{
				buffer[j] = v->buffer[i];
				j ++;
			}
		}
		if (v->buffer != NULL)
		{
			free (v->buffer);
		}
		v->buffer = (void **) buffer;
		v->elements --;
	}
	else
	{
		if (v->buffer[0] == p)
		{
			v->elements = 0;
			free (v->buffer);
			v->buffer = NULL;
		}
	}
}

void *VectorGetNext (Vector *v)
{
	if (v->idx >= v->elements) return NULL;
	v->idx ++;
	return v->buffer[v->idx - 1];
}

void *VectorGet (Vector *v, int idx)
{
	if (idx >= v->elements) return NULL;
	return v->buffer[idx];
}

/* Ende Funtionen fuer Structur Vector */

/* Funtionen fuer Structur Token */

void TokenInit (Token *t)
{
	VectorInit (&t->v);
	t->buffer = NULL;
	t->elements = 0;
	TextInit (&t->Sep);
	t->mode = TOKENMODE_ALL;
}

void TokenDelete (Token *t)
{
	VectorDestroy (&t->v);
	if (t->buffer != NULL)
	{
		free (t->buffer);
	}
	t->elements = 0;
}

char *StrTok (char *str, char *sep, int mode)
{
	static char *apos = NULL;
	char *p;
	int i, j;

	if (mode == TOKENMODE_ALL)
	{
		return strtok (str, sep);
	}

	if (str != NULL) apos = str;
	if (str == NULL && apos == NULL) return NULL;

	p = apos;
	for (i = 0; apos[i] != 0; i ++)
	{
		for (j = 0; sep[j] != 0; j ++)
		{
			if (apos[i] == sep[j])
			{
				apos[i] = 0;
				apos = &apos[i + 1];
				return (p);
			}
		}
	}
	apos = NULL;
	return p;
}

void TokenSet (Token *t, Text *tx, Text *Sep)
{
	char *p;
	Text *text;

    TokenDelete (t);
	t->buffer = (char *) malloc (tx->Length + 1);
	ERROR ((t->buffer == NULL), 1);
	strcpy (t->buffer, TextGetBuffer (tx));
	TextInit (&t->Sep);
	TextSet (&t->Sep, Sep->T);
    p = StrTok (t->buffer, t->Sep.T, t->mode); 
	t->elements = 0;
	while (p != NULL)
	{
		text = (Text *) malloc (sizeof (Text));
 	    TextInit (text);
		TextSet (text, p);
		VectorAdd (&t->v, text);
		p = StrTok (NULL, t->Sep.T, t->mode); 
  	    t->elements ++;
	}
}

void TokenSetC (Token *t, Text *tx, char *Sep)
{
	char *p;
	Text *text;

    TokenDelete (t);
	t->buffer = (char *) malloc (tx->Length + 1);
	ERROR ((t->buffer == NULL), 1);
	strcpy (t->buffer, TextGetBuffer (tx));
	TextInit (&t->Sep);
	TextSet (&t->Sep, Sep);
    p = StrTok (t->buffer, t->Sep.T, t->mode); 
	t->elements = 0;
	while (p != NULL)
	{
		text = (Text *) malloc (sizeof (Text));
 	    TextInit (text);
		TextSet (text, p);
		VectorAdd (&t->v, text);
		p = StrTok (NULL, t->Sep.T, t->mode); 
  	    t->elements ++;
	}
}

char *TokenGet (Token *t, int idx)
{
	Text *tx;
	if (idx >= t->elements) return (NULL);

	tx = (Text *) VectorGet (&t->v, idx);
	return  (tx->T);
}

/* Ende Funtionen fuer Structur Token */

/* Funtionen fuer Structur Log */

void LogInit (Log *log)
{
	TextInit (&log->Name);
	TextInit (&log->text);
	log->fp = NULL;
}

void LogOutput (Log *log, char *text)
{
	if (log->Name.Length == 0)
	{
		TextSet (&log->Name, "ParseXml.log");
	}

	if (log->fp == NULL)
	{
		log->fp = fopen (log->Name.T, "w");
	}

	TextSet (&log->text, text);
	if (log->fp == NULL)
	{
		printf ("%s", log->text.T);
		return;
	}
	fprintf (log->fp, "%s", log->text.T);
	fflush (log->fp);
}

void LogOutputT (Log *log, Text *text)
{
	if (log->Name.Length == 0)
	{
		TextSet (&log->Name, "ParseXml.log");
	}

	if (log->fp == NULL)
	{
		log->fp = fopen (log->Name.T, "w");
	}

	TextSetT (&log->text, text);
	if (log->fp == NULL)
	{
		printf ("%s", log->text.T);
		return;
	}
	fprintf (log->fp, "%s", log->text.T);
	fflush (log->fp);
}

/* Ende Funtionen fuer Structur Log */

/* Funtionen fuer Structur Double */

void DoubleInit (Double *d)
{
	TextInit (&d->text);
	d->value = 0.0;
}

double DoubleSet (Double *d, char *string)
{
	TextSet (&d->text, string);
    d->value = StringToDouble (string);
	return d->value;
}


double StringToDouble (char *string)
{
 double fl;
 double ziffer;
 double teiler;
 short minus;

 fl = 0;
 teiler = 10;
 minus = 1;
 while (*string < 0X30)
 {
  if (*string == 0)
    return (0.0);
  if (*string == '-')
    break;
  if (*string == '+')
    break;
  string ++;
 }

 if (*string == '-')
 {
  minus = -1;
  string ++;
 }
 else if (*string == '+')
 {
  string ++;
 }

 while (*string)
 {
  if (*string == '.')
  {
   break;
  }
  if (*string == ',')
  {
   break;
  }
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = (fl * teiler) + ziffer;
  string ++;
 }

 if (*string == '.')
   ;
 else if (*string == ',')
   ;
 else
 {
  fl *= minus;
  return (fl);
 }

 string ++;
 while (*string)
 {
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = fl + (ziffer / teiler);
  teiler *= 10;
  string ++;
 }
 fl *= minus;
 return (fl);
}

/* Ende Funtionen fuer Structur Double */

/* Ende Funtionen fuer Structur Field */

/* Funtionen fuer Structur ConvEntry */

void ConvEntryInit (ConvEntry *c, Text *text)
{
	Token t;
	Text g;

	TextInit (&g);
	TextSet (&g, "=");
	TokenInit (&t);
	TokenSet (&t, text, &g);
	if (t.elements < 1) return;
	TextInit (&c->Source);
	TextInit (&c->Target);
//	TextSet (&c->Source, TokenGet (&t, 0));
	ConvEntrySetString (&c->Source, TokenGet (&t, 0));
/*	TextTrim (&c->Source);    */
	if (t.elements > 1)
	{
/*		TextSet (&c->Target, TokenGet (&t, 1)); */
		ConvEntrySetString (&c->Target, TokenGet (&t, 1));
	}
	else
	{
		TextSet (&c->Target, " ");
	}
/*	TextTrim (&c->Target); */
}

void ConvEntryDelete (ConvEntry *c)
{
	TextDelete (&c->Source);
	TextDelete (&c->Target);
}

void ConvEntrySetString (Text *String, char *p)
{
	Text text;
	Text separator;
	Token t;
	int i;
	unsigned short decvalue;
	unsigned char hi;
	unsigned char lo;
	CVarChar vc;
	unsigned char *buffer;

	TextInit (&text);
	TextInit (&separator);
	TextSet (&separator, " ");
	TextSet (&text, p);
	TokenInit (&t);
	TokenSet (&t, &text, &separator);
	buffer = (unsigned char *) malloc (t.elements + 5);
	if (buffer == NULL)
	{
		TextSet (String, " ");
	}
	if (t.elements == 1)
	{
/* Der neue Wert kann als Decimalwert angegeben werden. Nur bei UTF16, da in jedem Fall 
   2 Bytes entstehen
*/
		TextSet (&text, TokenGet (&t, 0));
		TextTrim (&text);
		decvalue = atoi (text.T);
		if (decvalue != 0)
		{
			lo = decvalue & 0xFF;
			hi = (decvalue >> 8) & 0xFF;
            buffer[0] = lo;
			buffer[1] = hi;
            buffer[2] = 0; 
			TextSet (String, buffer);
			free (buffer);
			return;
		}
	}
	for (i = 0; i < t.elements; i ++)
	{
		TextSet (&text, TokenGet (&t, i));
		CVarCharInitS (&vc, &text);
		buffer[i] = vc.AsChar;
	}
    buffer[i] = 0;
	TextSet (String, buffer);
	free (buffer);
}

char *ConvEntryGetTarget (ConvEntry *c, Text *Source)
{
	if (memcmp (c->Source.T, Source->T, c->Source.Length) == 0)
	{
		return TextGetBuffer (&c->Target);
	}
	return (NULL);
}

char *ConvEntryGetSource (ConvEntry *c, Text *Target)
{
	if (TextCompare (&c->Target, Target) == 0)
	{
		return TextGetBuffer (&c->Source);
	}
	return (NULL);
}

void ConvEntryLog (ConvEntry *c, FILE *fp)
{
	fprintf (fp, "%s = %s\n", c->Source.T, c->Target.T);
}

/* Ende Funtionen fuer Structur ConvEntry */

/* Funtionen fuer Structur ConvTab */

void ConvTabInit (ConvTab *c)
{
	VectorInit (&c->Tab);
	c->IsLoaded = FALSE;
	TextInit (&c->Name);
	TextSet (&c->Name, "");
};


void ConvTabDelete (ConvTab *c)
{
	int i;
	ConvEntry *e;

	for (i = 0; i <  c->Tab.elements; i ++)
	{
		e = (ConvEntry *) c->Tab.buffer [i];
		ConvEntryDelete (e);
		free (c->Tab.buffer [i]);
	}
	c->Tab.buffer = NULL;
	c->Tab.elements = 0;
	c->Tab.idx = 0;
}

char *ConvTabGetTarget (ConvTab *c, Text *Source, int *pos)
{
	int i;
	char *p;
	ConvEntry *e;
	for (i = 0; i <  c->Tab.elements; i ++)
	{
		e = (ConvEntry *) c->Tab.buffer [i];
		p = ConvEntryGetTarget (e, Source);
		if (p != NULL)
		{
			*pos += (e->Source.Length - 1);
			return (p);
		}
	}
	return (NULL);
}
char *ConvTabGetSource (ConvTab *c, Text *Target)
{
	int i;
	char *p;
	ConvEntry *e;

	for (i = 0; i <  c->Tab.elements; i ++)
	{
		e = (ConvEntry *) c->Tab.buffer [i];
		p = ConvEntryGetSource (e, Target);
		if (p != NULL)
		{
			return (p);
		}
	}
	return (NULL);
}

char *ConvTabTabLog (ConvTab *c, FILE *fp)
{
	int i;
	ConvEntry *e;
	for (i = 0; i <  c->Tab.elements; i ++)
	{
		e = (ConvEntry *) c->Tab.buffer [i];
		ConvEntryLog (e, fp);
	}
	return (NULL);
}

BOOL ConvTabLoad (ConvTab *c, char *FileName)
{
	FILE *fp;
	char buffer [512];
	char *code;
	char *p;
	char *co;
	Text Record;
	ConvEntry *e;
	TextInit (&Record);
	fp = fopen (FileName, "r");
	if (fp == NULL)
	{
		return (FALSE);
	}

	if (fgets (buffer, 511,fp) == NULL)
	{
		fclose (fp);
		return FALSE;
	}

	if (memcmp (buffer, "[", 1) != 0)
	{
		fclose (fp);
		return FALSE;
	}
	code = (char *) malloc (strlen (buffer) + 1);
	if (code == NULL) return FALSE;
	p = &buffer[1];
	co = code;
	for (;(*p != ']' && *p != 0); p +=1) *co++ = *p;
    *co = 0;
	TextSet (&c->Name, code);
	free (code);

	while (fgets (buffer, 511, fp) != NULL)
	{
		TextSet (&Record, buffer);
		TextTrim (&Record);
		if (Record.Length == 0) continue;
		e = (ConvEntry *) malloc (sizeof (ConvEntry));
		if (e == NULL)
		{
			return FALSE;
		}
		ConvEntryInit (e, &Record);
		VectorAdd (&c->Tab, e);
	}
    fclose (fp);
/*	TextSet (&c->Name, "UTF8");  */
	TextMakeUpper (&c->Name);
	c->IsLoaded = TRUE;
	return TRUE;
}


BOOL ConvTabChange (ConvTab *c, char *text, int len)
{
	Text UTF8;
	Text UTF16;
	BOOL ret = FALSE;

	TextInit (&UTF8);
	TextInit (&UTF16);
	TextSet (&UTF8, "UTF8");
	TextSet (&UTF16, "UTF16");
	if (TextCompare (&c->Name, &UTF8) == 0)
	{
		ret = ConvTabChangeUTF8 (c, text, len);
	}
	else if (TextCompare (&c->Name, &UTF16) == 0)
	{
		ret = ConvTabChangeUTF16 (c, text, len);
	}
	TextDelete (&UTF8);
	TextDelete (&UTF16);
	return ret;
}

BOOL ConvTabChangeUTF8 (ConvTab *c, char *text, int len)
{
	int i;
	int j;
	char *p;
	int slen;
	char *ta;
	Text Source;
	Text Target;
	BOOL StrEnd = FALSE;

	TextInit (&Source);
	TextInit (&Target);

	p = (char *) malloc (len + 1);
	if (p == NULL)
	{
		return FALSE;
	}

	for (slen = 0; slen < len && text[slen] != 0; slen ++);
    if (slen < len) StrEnd = TRUE;
/*	slen = strlen (text);  */
	for (i = 0, j = 0; i < slen && j < len; i ++)
	{
		if (text[i] == 0)
		{
			p[j] = 0;
			j ++;
			break;
		}
		TextSet (&Source, &text[i]);
		ta = ConvTabGetTarget (c, &Source, &i);
		if (ta == NULL)
		{
			p[j] = text[i];
			j ++;
		}
		else if (j + (int) strlen (ta) < len)
		{
			memcpy (&p[j], ta, strlen (ta));
			j += strlen (ta);
		}
		else
		{
			break;
		}
	}
	p[j] = 0;
	if (StrEnd)
	{
		memset (text, 0, len);
		strcpy (text, p);
	}
	else
	{
		memset (text, ' ', len);
		memcpy (text, p, j);
	}
	free (p);
	return TRUE;
}

BOOL ConvTabChangeUTF16 (ConvTab *c, char *text, int len)
{
	int i;
	int j;
	char *p;
	int slen;
	char *ta;
	Text Source;
	Text Target;

	TextInit (&Source);
	TextInit (&Target);

	p = (char *) malloc (len + 1);
	if (p == NULL)
	{
		return FALSE;
	}

	slen = strlen (text);
	for (i = 0, j = 0; i < slen && j < len; i ++)
	{
		if (text[i] == 0)
		{
			p[j] = 0;
			j ++;
			break;
		}
		TextSet (&Source, &text[i]);
		ta = ConvTabGetTarget (c, &Source, &i);
		if (ta == NULL)
		{
			p[j] = text[i];
			j ++;
			p[j] = 0;
			j ++;
		}
		else if (j + (int) strlen (ta) <= len)
		{
			memcpy (&p[j], ta, strlen (ta));
			j += strlen (ta);
		}
		else
		{
			break;
		}
	}
	memcpy (text, p, j);
	free (p);
	return TRUE;
}

/* Ende Funtionen fuer Structur ConvTab */

/* Funtionen fuer Structur CVarChar */

void CVarCharInitS (CVarChar * vc,Text *AsString)
{
	char *p;
	Text kz;
	Text value;
	TextInit (&kz);
	TextInit (&value);
	TextInit (&vc->AsString);

	TextSetT (&vc->AsString,  AsString);
	if (AsString->Length == 1)
	{
		if (AsString->T[0] >= '0' &&
			AsString->T[0] <= '9')
		{
			 CVarCharSetDecimal (vc, AsString);
		}
		else
		{
            vc->AsInt = (int) AsString->T[0];  
            vc->AsChar = (unsigned char) AsString->T[0];  
		}
	}
	else
	{
		p = TextSubString (AsString, 0, 2);
		TextSet (&kz, p);
		TextMakeUpper (&kz);
		if (memcmp (kz.T, "0X", 2) == 0)
		{
			p = TextSubString (AsString, 2, -1);
			TextSet (&value, p);
			CVarCharSetHex (vc, &value);
			return;
		}
/*
		kz = AsString.Right (1);
		kz.MakeUpper ();
		if (kz == "H")
		{
			SetHex (AsString.Left (AsString.GetLength () - 1));
			return;
		}
*/
		CVarCharSetDecimal (vc, AsString);
	}
}

void CVarCharInitI (CVarChar *vc,int AsInt)
{
	vc->AsInt = (unsigned int) AsInt;
	vc->AsChar = (unsigned char) AsInt;
}

void CVarCharInitC (CVarChar *vc,char AsChar)
{
	vc->AsInt = (unsigned int) AsChar;
	vc->AsChar = (unsigned char) AsChar;
}


void CVarCharSetDecimal (CVarChar *vc, Text* DecString)
{
	char *p;
	BOOL DecOK;
	int i;
	unsigned char z;
    vc->AsChar = 0;
	p = DecString->T;
	DecOK = FALSE;
	for (i = 0; (p[i] != 0 && i < 3); i ++)
	{
		if (DecOK)
		{
			vc->AsChar *= 10;
		}
	    DecOK = FALSE;
		z = (unsigned int) p[i];
		if (z >= '0' && z <= '9')
		{
			z -= 0x30;
			vc->AsChar += z;
			DecOK = TRUE;
		}
	}
	vc->AsInt = vc->AsChar;
}

void CVarCharSetHex (CVarChar *vc, Text* HexString)
{
	char *p;
	BOOL HexOK;
	int i;
	unsigned char z;
    vc->AsChar = 0;
	TextMakeUpper (HexString);
	p = HexString->T;
	HexOK = FALSE;
	for (i = 0; (p[i] != 0 && i < 2); i ++)
	{
		if (HexOK)
		{
			vc->AsChar *= 16;
		}
		HexOK = FALSE;
		z = (unsigned int) p[i];
		if (z >= '0' && z <= '9')
		{
			z -= 0x30;
			HexOK = TRUE;
			vc->AsChar += z;
		}
		else if (z >= 'A' && z <= 'F')
		{
			z -= ('A' - 10);
			HexOK = TRUE;
			vc->AsChar += z;
		}
	}
	vc->AsInt = vc->AsChar;
}

/* Ende Funtionen fuer Structur CVarChar */

#endif  /* ifndef CPU68K */

/* Ende von objects.c */

