#ifndef SINIX
#ident  "@(#) bws_asc.c	VLUGLOTSA	28.02.97	by RO"
#endif
/***
--------------------------------------------------------------------------------
-
-       Modulname               :       asc_bws.c
-
        Exports                 :
         
        Imports                 :

-       Interne Prozeduren      :  
-
-       Externe Prozeduren      :    
                                        
-
-       Autor                   :       WR    
-       Erstellungsdatum        :       18.01.93
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ESQL-C
-
-       Aenderungsjournal       :
-
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   BWS-Tabelle in Ascii-Datei uebertragen.
-
--------------------------------------------------------------------------------
***/

#include <stdio.h>
#include "objects.h"

extern short DEBUG;
extern short FDEBUG;
extern char sql[];
extern short sql_mode;

extern char dbase [];
extern char *argsp [];      /* Speicher fuer Argumente                       */

static char *and_part = 0; /* Selektionskriterium fuer Tabelle               */
                           /* Wird als Argument mit -s uebergeben            */
                           /* z.B. -s"where ls = 1"                          */
static char *ascii_name;    /* Name der Ascii-Datei                          */
static char *info_name;     /* Name der Info-Datei                           */
static char *ename = 0;     /* Datei mit Bedingung fuer einzelne Saetze     */
static short anzeigen = 0;  /* Flag fuer Anzeigemodus                       */
static short sqldebug = 0;  /* Flag fuer SQL-Debugmode                      */
static short feldinh  = 0;  /* Feldinhalte Ziel                             */
static short test = 0;      /* Testmodus                                    */
static short binaer;        /* Dateimodus                                   */
static char trenner = ',';  /* Trennzeichen zwischen Felder                 */
static int hochkomma = 0;   /* Flag fuer Hochkomma bei Typ C                */
extern int DebRun;          /* Debug-Kennzeichen                             */

extern short show_code;     /* Code anzeigen                                */
extern int arganz;          /* Anzahl Argumente                             */
extern int chainanz;
extern int dochain;
extern char *chainarg[];
extern char *charg[];
extern char *ausgabe;             /* Name der Ascii-Datei            */
/*
extern short tabspos;
extern short funcanz;
*/

extern BOOL ConnectMode;

int tst_arg (arg)
char *arg;
{
       
          for (;*arg; arg +=1) 
          {
                  switch (*arg)
                  {
                            case 'a' :
                                     anzeigen = 1;
                                     break;
                            case 'b' :
                                     binaer = 1;
                                     break;
                            case 'e' :
                                     ename = arg + 1;
                                     return (0);
                            case 'd' :
//                                     sqldebug = 1;
                                     DebRun = 1;
                                     break;
                            case 'D' :
                                     DEBUG = 1;
                                     return (0);
                            case 'q' :
                                     FDEBUG = 1;
                                     break;
                            case 'o' :
                                     setomodus ("w");
                                     break;
                            case 't' :
                                     if (arg[1])
                                     {
                                             settrenner (arg[1]);
                                     }
                                     break;
                            case 'k' :
                                     sethochkomma ();
                                     break;
                            case 'l' :
                                     setclipped (atoi (arg + 1));
                                     break;
                            case 'f' :
                                     feldinh = 1;
                                     break;
                            case 'm' :
                                     sql_mode = atoi (arg + 1);
                                     break;
                            case 'h' :
                                     hilfe ();
                                     break;
                            case 's' :
                                     and_part = (arg + 1);
                                     return (0);
                            case 'c' :
                                     show_code = 1;
                                     break;
                  }
          }
          return (0);
}

main (anz, arg)
int anz;
char *arg [];
{

          short i;

		  Text Mode;
		  char *e;

 		  if ((e = getenv ("ConnectMode")) != NULL)
		  {
			  TextInit (&Mode);
			  TextSet (&Mode, e);
			  TextMakeUpper (&Mode);
			  if (TextCompareC (&Mode, "FALSE") == 0)
			  {
					ConnectMode = FALSE;
			  }
			  else if (TextCompareC (&Mode, "TRUE") == 0)
			  {
					ConnectMode = TRUE;
			  }
			  TextDelete (&Mode);
		  }
/* Flags mit - testen                           */

          argtst (&anz, arg, tst_arg);

          if (anz < 3)
          {
                        fprintf (stderr,
                            "Usage bws_db Ascii-Datei Info-Datei\n");
                         exit (1);
          }

          for (i = 0; i < 20; i ++)
          {
                     if (anz < i + 4) break;
                     argsp [i] = arg[i + 3];
          }
          arganz = i;

/* Static-Variablen fuer  mo_bwsasc setzen            */


//          opendbase ("bws");
          ascii_name = arg [1];
          ausgabe = arg [1];
          info_name  = RifToIf (arg [2]);

          setand_part (and_part);
          setascii_name (ascii_name);
          setinfo_name (info_name);
          setename (ename);
          setanzeigen (anzeigen);
          setsqldebug (sqldebug);
          setfeldinh (feldinh);
          settest (test);
          setbinaer (binaer);

          if (anzeigen && ename) printf ("Daten von %s\n", ename);
          do_bwsasc ();
		  if (dochain)
		  {
              dochain = 0; 
			  chainmain (chainanz, charg);
		  }
		  closedbase (dbase);
		  return (0);
}


chainmain (anz, arg)
int anz;
char *arg [];
{

          short i;

/* Flags mit - testen                           */

          argtst (&anz, arg, tst_arg);

          if (anz < 3)
          {
                         fprintf (stderr,
                            "Syntaxerror bei chain\n");
                         exit (1);
          }

          for (i = 0; i < 20; i ++)
          {
                     if (anz < i + 4) break;
                     argsp [i] = arg[i + 3];
          }
          arganz = i;
/*
		  tabspos = 0;
		  funcanz = 0;
*/

/* Static-Variablen fuer  mo_bwsasc setzen            */


          ascii_name = arg [1];
          info_name  = RifToIf (arg [2]);

          setand_part (and_part);
          setascii_name (ascii_name);
          setinfo_name (info_name);
          setename (ename);
          setanzeigen (anzeigen);
          setsqldebug (sqldebug);
          setfeldinh (feldinh);
          settest (test);
          setbinaer (binaer);

          if (anzeigen && ename) printf ("Daten von %s\n", ename);
          do_bwsasc ();
		  if (dochain)
		  {
              dochain = 0; 
			  chainmain (chainanz, charg);
		  }
		  for (i = 0; i < chainanz; i ++) 
		  {
			  if (chainarg[i]) free (chainarg[i]);
			  chainarg[i] = (char *) 0;
		  }
		  chainanz = 0;
}


int hilfe ()
{
        printf ("bws_db -abcdDkmoqfs[\"Bedingung\"] lclippmode tTrennzeichen -eDatei Dateiname Infodateiname\n"); 
//        printf ("adc_bws -adfq Dateiname Infodatei\n");
        printf ("  -a   Anzeigemodus\n");
        printf ("  -b   Binaermodus\n");
//        printf ("  -d   SQL-Debug-Modus fuer Haupttabelle\n");
        printf ("  -d   breakpoint zulassen\n");
        printf ("  -e   Suchkriterien stehen in Datei\n");
        printf ("       Aufbau Datei\n");
        printf ("       feld1 feld2 feld3 ..\n");
        printf ("       wert1 wert2 wert3 ..\n");
        printf ("       wert1 wert2 wert3 ..\n");
        printf ("\n");
        printf ("  -D   allgemeiner SQL-DEBUG - Modus\n");
        printf ("  -f   Feldinhalte von Datenbankbereich anzeigen\n");
        printf ("  -s   Bedingung fuer select\n");
        printf ("  -c   Programm-Code anzeigen\n");
        printf ("  -k   Datentyp C steht in Anfuehrungszeichen\n");
        printf ("  -lm  In Verbindung mit k werden Blanks am Schluss "
                        "entfernt.\n");
        printf ("       m = 1   leerer String = \"\"\n");
        printf ("       m = 2   Wenn mit Trennzeichen leerer String "
                                "ohne \"\"\n" );
        printf ("  -mx  Set SQL-Fehler-Modus auf x . x = (0 - 2)\n");
        printf ("  -q   Satzaufbau anzeigen\n");
        printf ("  -o   Oeffnungsmodus Ausgabedatei 'w'\n");
        printf ("  -tz  Felder sind durch Zeichen z getrennt\n");
        exit (0);
}
