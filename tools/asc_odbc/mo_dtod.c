/***
--------------------------------------------------------------------------------
-
-       Modulname               :       asc_bws.c
-
        Exports                 :
         
        Imports                 :

-       Interne Prozeduren      :  
-
-       Externe Prozeduren      :    
                                        
-
-       Autor                   :       WR    
-       Erstellungsdatum        :       18.01.93
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ESQL-C
-
-       Aenderungsjournal       :
-
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Ascii_Datei in BWS einbuchen.
                                    generieren.
-
--------------------------------------------------------------------------------
***/

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <string.h>
#ifdef ODBC
#include "dbclass.h"
#else
#include "mo_curso.h"
#endif
#ifndef WIN32
#include <fcntl.h>
#endif

#define KONVERT "KONVERT"
#define CHAR    1
#define SHORT   2
#define LONG    3
#define STRING  4
#define DOUBLE  5
#define HEX     6

#define GESPERRT 250

extern char *make_name ();
extern char *zwort[];
extern char *wort[];
extern char *clipped ();
extern double ratod ();
extern char * upstrstr ();
extern char * getenv ();

extern char *zwort[];
extern char *wort[];
extern char *clipped ();
extern double ratod ();
extern char * upstrstr ();

extern short DEBUG;
extern short FDEBUG;
extern char sql[];
extern short sql_mode;

/* Informationen aus SW-Kopf                                      */
extern short blklen;            /* SW-Blocklaenge                        */
extern short blkanz;            /* Anzahl SW-Bloecke                     */
extern short banz;              /* Anzahl SW-Felder pro Satz             */
extern short klen;              /* Anzahl SW-Felder pro Satz             */

extern short swq;                  /* Quelle ist eine SW-Datei              */ 
extern short swz;                  /* Ziel ist eine SW-Datei                */ 
short sw_rec = 0;

/* Typ-Definitionen                               */

/* Struktur fuer Feldbeschreibung                 */

typedef struct
       {   char  feldname [48];
	   short feldlen;
	   char  feldtyp [2]; 
	   short nachkomma;
	   short dimension;
       } FELDER;

typedef struct
       {   FELDER **felder;
           char   **buffer;
       } RECORD;

union FADR
      { char  *fchar;
        short *fshort;
        int   *fint;
        long  *flong;
        void  *fvoid;
        double *fdouble;
      };

extern short constat;
extern short ecode;         /* Exit-Status                                   */
extern short do_exit;       /* Kennzeichen fuer exit                         */
extern FELDER *quelle;      /* Tabelle mit Satzaufbau fuer Eingabefelder     */
extern short feld_anz;      /* Anzahl Felder im Satz                         */
extern RECORD insatz;

extern FELDER *ziel;        /* Tabelle mit Tabellenaufbau aus der Datenbank  */
extern short db_anz;        /* Anzahl Felder im Satz                         */
extern short ziel_len;      /* Laenge Zielsatz. Wird zur Laufzeit gesetzt    */
static lesestatus;          /* SQL-Status beim Lesen                        */
extern RECORD outsatz;

extern FELDER *data;         /* Tabelle mit Variablen                       */
extern short data_anz = 0;   /* Anzahl Variablen                           */
extern RECORD datasatz;

static char dbtab [20];     /* Name der Datenbanktabelle                     */
extern char *eingabe;       /* Name der Ascii-Datei                          */
extern char *ausgabe;       /* Name der Ascii-Datei                          */

extern char *eingabesatz;   /* Buffer fuer Ascii-Eingabesatz                 */
extern short qlen;          /* Laenge eingabesatz                            */
extern char *ausgabesatz;   /* Buffer fuer Binaer-Ausgabesatz                */
static char *zwsatz = (char *) 0;
                            /* Zwischenbuffer fuer Ausgabe mit Trennzeichen  */
extern short zlen;          /* Laenge ausgabesatz                            */
extern char *daten;         /* Buffer fuer allgemeien Variablen              */
extern short dlen;          /* Anzahl allgemeien Daten                       */
extern char CR[];

extern char where_part [512];     
                            /* Buffer fuer Where-Teil                        */
static char def_part [512];     
                            /* Buffer fuer Where-Teil Defaultsatz            */

static int sel_db = -1;     /* Cursor                                        */
static int sel_default = -1;/* Cursor                                        */
static int sel_upd = -1;    /* Cursor                                        */
static int ins_db = -1;
static int upd_db = -1;

int to_dat ();
static int write_var ();    /* Variable in Ausgabefile                      */ 

extern char *argsp [];      /* Speicher fuer Argumente                       */

short mit_default = 0; /* Kennzeichen, ob ein Defaultsatz gelesen wird */
short anzeigen = 0;  /* Flag fuer Anzeigemodus                       */
short sqldebug = 0;  /* Flag fuer SQL-Debugmode                      */
short feldinh  = 0;  /* Feldinhalte Ziel                             */
short quick = 0;     /* Flag fuer schnelles einbuchen                */
short test = 0;      /* Testmodus                                    */
short binaer = 0;    /* Dateimodus                                   */
short append = 0;    /* Flag fuer Append-Modus                       */
char *wr_modus = "w"; /* Dateimodus fuer Ausgabedatei                */

char *progname;             /* Aktueller Programmname                       */
char *info_name;            /* Name der Info-Datei                          */
char info_name_alt [265];   /* Zum Vergleich mit Info_name                  */
static FILE *inf;           /* Infodatei                                    */
char trenner = 0;      /* Trennzeichen zwischen Felder Ausagbedatei    */
char trenneri = 0;      /* Trennzeichen zwischen Felder Eingabedatei   */
static int hochkomma = 0;     /* Flag fuer Hochkomma bei Typ C                */
static int mit_clipp = 0;     /* Flag fuer clipped bei Typ C                  */

static char *tsatz = (char *) 0;

char dbase [256] = {"bws"};
char *tmpinfdat = NULL;


int settrenneri (tr)
char tr;
{
          if (tr == 'p') 
		  {
			  tr = '|';  
		  }
          else if (tr == 't') 
		  {
			  tr = 9;
		  }
          trenneri = tr;
}

int settrenner (tr)
char tr;
{
          if (tr == 'p') 
		  {
			  tr = '|';  
		  }
          if (tr == 't') 
		  {
			  tr = 9;
		  }
          trenner = tr;
}

int sethochkomma ()
{
          hochkomma = 1;
}

int setclipped (modus)
int modus;
{
          if (modus != 2) modus = 1;
          mit_clipp = modus;
}


do_dtod ()
/**
Ausfuehren des Konvertiermoduls itod Datei in Datei konvertieren.          
**/
{
          static short first = 1;

		  if (strchr (wr_modus, 'a') != NULL)
		  {
			  if (binaer)
			  {
				  wr_modus = "ab";
			  }
			  else
			  {
				  wr_modus = "a";
			  }
		  }
          if (first)
          {
                    new_procedure ();
                    first = 0;
          }
          if (strcmp (info_name, info_name_alt))
          {
                strcpy (info_name_alt, info_name);
                ziel_len = 0;
                belege_quelle ();
                belege_ziel ();
                if (anzeigen) 
                {
                            printf ("Satzlaenge Quelle %d\n", satzlen (quelle));
                            printf ("Satzlaenge Ziel    %d\n", satzlen (ziel));
                }
          }
          else
          {
                if (swq)
                {
                           if (lese_sw (eingabe) == GESPERRT) return (GESPERRT);
                }
                else if (swz)
                {
                           if (lese_sw (ausgabe) == GESPERRT) return (GESPERRT);
                }
                init_sw_start ();
          }
          return (dtod ());
}

int dtod ()
/**
Ascii-Datei konvertieren.
**/
{

          FILE *eingabefile;
          FILE *ausgabefile;

          if (swz) 
          {
               wr_modus = "r+b";
          }
          else if (binaer)
          {
               strcat (wr_modus, "+b");
          }       
          if (eingabe)
          {
                   if (binaer || swq)
                   {   
                          eingabefile = fopen (eingabe,"rb");
                   }
                   else   
                   {   
                          eingabefile = fopen (eingabe,"r");
                   }
                   if (eingabefile == (FILE *) 0)
                   {
                          fprintf (stderr,
                               "%s kann nicht geoeffnet werden\n", 
                                eingabe);
                          exit (0);
                   }
                   set_eingabefile (eingabefile);
          }
          if (ausgabe)
          {
                   if (anzeigen) printf ("ausgabe = %s\n", ausgabe);
				   if (trenner != 0)
				   {
					   if (strchr (wr_modus, 'a') != NULL)
					   {
						   wr_modus = "a";
					   }
					   else
					   {
						   wr_modus = "b";
					   }
				   }
                   ausgabefile = fopen (ausgabe,wr_modus);
                   if (ausgabefile == (FILE *) 0)
                   {
  //                        if (errno == 13) return (GESPERRT);
                          fprintf (stderr,
                               "%s kann nicht geoeffnet werden\n", 
                                ausgabe);
                          exit (1);
                   }
                   // chmod (ausgabe, 0400);
                   set_ausgabefile (ausgabefile);
          }
          else
          {
                  ausgabefile = stdout;
          }
          set_wrziel (to_dat, ausgabefile);
          set_wrsziel (write_var, ausgabefile);
          perform_begin ();
          if (eingabe == 0) 
          {
                       perform_ende ();
                       if (swz) writesw_anz (ausgabefile);
                       fclose (ausgabefile);
                       fclose (inf);
                       chmod (ausgabe, 0660);
                       return (0);
          }

          qlen = satzlen (quelle);
          if (binaer == 0) qlen += 3;
          while (rfgets (eingabesatz,qlen,eingabefile))
          {
                        if (do_exit) break;
                        to_ausgabe ();
                        perform_code ();
                        if (do_exit) break;
                        showfelder (&outsatz);
                        to_dat (ausgabefile); 
          }
          perform_ende ();
          fclose (eingabefile);
          if (swz) writesw_anz (ausgabefile);
          fclose (ausgabefile);
          fclose (inf);
          chmod (ausgabe, 0660);
          if (tmpinfdat != NULL)
          {
                 unlink (tmpinfdat);
                 tmpinfdat = NULL;
          }
          close_all_cursor ();
          return (0);
}

int to_ausgabe ()
/**
Eingaesatz in Ausgabesatz konvertieren.
**/
{
          short i;
          short zpos;

          if (swz) init_ausgabesatz ();

          for (i = 0; ziel[i].feldname[0]; i ++)  
          {
                   uebertrage (&outsatz, ziel [i].feldname,
                               &insatz, ziel [i].feldname);
          }
}

in_to_out (quelle, ziel, quellfeld, zielfeld)
/**
Quellfeld in Zielfeld uebertragen.
**/
FELDER *quelle;
FELDER *ziel;
FELDER *quellfeld;
FELDER *zielfeld;
{
          char quellwert [265];
          short spos;
          short len;
          char *qpos;
          union FADR zfeld;

/* Positionen im Buffer holen                        */

          spos = feldpos (quelle,quellfeld->feldname);
          qpos = &eingabesatz [spos];
          spos = feldpos (ziel,zielfeld->feldname);
          zfeld.fchar = &ausgabesatz [spos];

          memcpy (quellwert,qpos, quellfeld->feldlen);
          quellwert [quellfeld->feldlen] = 0;
          switch (zielfeld->feldtyp[0])
          {
                         case 'C' :
                         case 'A' :
                                  len = quellfeld->feldlen;
                                  if (len > zielfeld->feldlen)
                                  {
                                           len = zielfeld->feldlen;
                                  }
                                  memcpy (zfeld.fchar,quellwert,len);
                                  break;
                         case 'd' :
                                  len = quellfeld->feldlen;
                                  if (len > zielfeld->feldlen)
                                  {
                                           len = zielfeld->feldlen;
                                  }
                                  memcpy (zfeld.fchar,quellwert,len);
                                  str_to_date (zfeld.fchar, zielfeld->feldlen, 
                                               len);  
                                  break;
                         case  'S' :
                                  sscanf (quellwert,"%hd",zfeld.fshort);
                                  break;
                         case  'I' :
                                  sscanf (quellwert,"%ld",zfeld.flong);
                                  break;
                         case  'D' :
                                  sscanf (quellwert,"%lf",zfeld.fdouble);
                                  nachkomma (zfeld.fdouble, 
                                            quellfeld->nachkomma);
                                  break;
                         case  'G' :
							      FromGerDate ((DATE_STRUCT *) zfeld.fchar, quellwert); 
                                  break;
            }
}

int nachkomma (dwert, nk)
/**
Nachkommastellen auswerten.
**/
double *dwert;
short nk;
{
           short i;
           double rwert;

           if (nk == 0) return;

           rwert = *dwert;
           for (i = 0; i < nk; i ++)
           {
                       rwert /= 10;
           }
           *dwert = rwert;
}            


int str_to_date (dstring,dlen, qlen)
/**
String in Datumformat konvertieren.
**/
char *dstring;
short dlen;
short qlen;
{
          char datum [11];

          if (dstring [2] == '.' && dstring [5] == '.') return;

          memcpy (datum,dstring,2);
          datum [2] = '.';
          datum [5] = '.';
          memcpy (datum,dstring,2);
          memcpy (&datum[3],&dstring[2],2);
          memcpy (&datum[6],&dstring[4],qlen - 4);
          qlen += 2;
          if (qlen > dlen) qlen = dlen;
          memcpy (dstring,datum,qlen);
          dstring [qlen] = 0;
}
          
int to_dat (ausgabefile)
/**
Aausgabesatz in Datenbank einfuegen.
**/
FILE *ausgabefile;
{
          if (constat > 0) return;
          if (swz)
          {
                      writesw (ausgabesatz, satzlen(ziel), ausgabefile);
          }
          else if (testformat (ausgabesatz, satzlen (ziel), ausgabefile)) 
          {
                      return (0);
          }
          else if (ziel_len > 0)
          {
                      fwrite (ausgabesatz, 1, ziel_len, ausgabefile);
          }
          else
          {
                      fwrite (ausgabesatz, 1, satzlen (ziel), ausgabefile);
          }
          if (binaer == 0) fwrite (CR,1, strlen (CR), ausgabefile);
}

int memclipp (zpos, qpos, ziellen)
/**
qpos in zpos in der Laenge len kopieren.
Blanks am Ende von zpos entfernen.
**/
char *zpos;
char *qpos;
int ziellen;
{
         
         memcpy (zpos, qpos, ziellen);
         zpos [ziellen] = (char) 0;
         clipped (zpos);
         if (strcmp (zpos, " ") == 0) 
         {
                    *zpos = (char) 0;
                    return (0);
         }
         return (strlen (zpos));
}

int testblank (string, len)
/**
Testen, ob Character <> ' ' vorhanden sind.
**/
char *string;
int len;
{
         int i;

         if (trenner == (char) 0) return (1);
         if (mit_clipp < 2) return (1);
         for (i = 0; i < len; i ++)
         {
                   if (string[i] == (char) 13);
                   else if (string[i] == (char) 10);
                   else if (string[i] > ' ') return (1);
         }
         return (0);
}


int testformat (satz, len, ausgabefile)
/**
Ausgabefoemat bestimmen und Satz formatieren.
**/
char *satz;
int len;
FILE *ausgabefile;
{
         int i;
         int flen;
         char *qpos;
         char *zpos;
         int setkomma;

         if (hochkomma == 0 && trenner == (char) 0) 
         {
                         return (0);
         }

         if (mit_clipp) len = 0;
         memset (zwsatz, (char) 0, sizeof (zwsatz));
         qpos = satz;
         zpos = zwsatz;
         for (i = 0; ziel[i].feldname[0]; i ++)
         {
                   switch (ziel[i].feldtyp[0])
                   {
                         case 'C' :
                         case 'd' :
                                  setkomma = 0;
                                  if (hochkomma && testblank (qpos,
                                                       ziel[i].feldlen)) 
                                  {
                                              *zpos = '\"';	
                                              zpos += 1;
                                              len ++;
                                              setkomma = 1;
                                  }
                                  if (mit_clipp)
                                  {
                                         flen = memclipp (zpos, qpos, 
                                                          ziel[i].feldlen);
                                         zpos += flen;
                                         len += flen;
                                  }
                                  else
                                  {
                                          memcpy (zpos, qpos, ziel[i].feldlen);
                                          zpos += ziel[i].feldlen;
                                  }
                                  qpos += ziel[i].feldlen;
                                  if (hochkomma && setkomma) 
                                  {
                                              *zpos = '\"';	
                                              zpos += 1;
                                              len ++;
                                  }
                                  break;
                         case  'A' :
                         case  'N' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                         case  'S' :
                         case  'I' :
                         case  'D' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                         case  'G' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                 }
                 if (trenner != (char) 0 && ziel[i + 1].feldname [0])
                 {
                                  *zpos = trenner;
                                  zpos += 1;
                                  len ++;
                 }
      }
      fwrite (zwsatz, 1, len, ausgabefile);
      if (binaer == 0) fwrite (CR,1, strlen (CR), ausgabefile);
      return (1);
}

int write_var (satz, len, ausgabefile)
/**
Aausgabesatz in schreiben.
**/
char *satz;
short len;
FILE *ausgabefile;
{
           fwrite (satz, 1, len, ausgabefile);
           fwrite (CR,1, strlen (CR), ausgabefile);
}

/***********

Include-Datei einlesen   

*/


int includefile (incl, satz)
/**
Includedatei einlesen.
**/
FILE *incl;
char *satz;
{
          int anz;
          FILE *incfile;
          char iname [512];
		  char ename[512];
          char *konv;

          cr_weg (satz);
          anz = split (satz);
          if (anz < 2) return (-1);
          set_env (ename, wort[2]);

          strcpy (iname, ename);
		  if ((strchr (ename, '\\') == NULL) &&
              (strchr (ename, '/') == NULL))
		  {
			konv = getenv ("KONVERT");
			if (konv)
			{
#ifdef WIN32
                 sprintf (iname, "%s\\%s", konv, wort[2]);
#else
                 sprintf (iname, "%s/%s", konv, wort[2]);
#endif
			}
		  }

          incfile = fopen (iname, "r");
          if (incfile == (FILE *) 0)
          {
                 strcpy (iname, wort[2]);
				 incfile = fopen (iname, "r");
          }

          if (incfile == (FILE *) 0)
          {
                 fprintf (stderr, "Includedatei %s nichht gefunden\n", iname);
                 exit (1);
          }

          while (fgets (satz, 511, incfile))
          {
                  fputs (satz, incl);
          }
          fclose (incfile);
          return (0);
 }

int leseinclude (satz)
/**
Includes lesen.
**/
char *satz;
{
          FILE *incl;
          char *tmp;
          static char idat[512];

          tmp = getenv ("TMPPATH");
          if (tmp != NULL)
          {
                     sprintf (idat, "%s\\infdat.tmp", tmp);
          }
          else 
          {
                     sprintf (idat, "infdat.tmp");
          }
          incl = fopen (idat, "w");
          if (incl == (FILE *) 0)
          {
                     fprintf (stderr, "Fehler beim Oeffnen von temp. Datei\n");
                     exit (1);
          }

          tmpinfdat = idat;
          while (fgets (satz, 511, inf))
          {
                    if (memcmp (satz, "$INCLUDE", 8) == 0)
                    {
                               includefile (incl, satz);
                    }
                    else
                    {
                               fputs (satz, incl);
                    }
          }
          fclose (incl);
          fclose (inf);
          inf = fopen (idat, "r");
          if (inf == (FILE *) 0)
          {
                     fprintf (stderr, "Fehler beim Oeffnenn %s",
                              idat);
                     exit (1);
          }
          return (0);
}

int testinclude ()
/**
Includes suchen.
**/
{
          char satz [516];

          while (fgets (satz, 511, inf))
          {
                    if (memcmp (satz, "$INCLUDE", 8) == 0)
                    {
                               fseek (inf, 0l, 0);
                               leseinclude (satz);
                               break;
                    }
          }
          fseek (inf, 0l, 0);
          return (0);
}

/***********

Ende Include-Datei einlesen   

*/


int belege_quelle ()
/**
Satzaufbau fuer Quelldatei holen.
**/
{
          char satz [512];
          short i;
          long feldseek;
          char *konvert;
          char inf_name [256];
          char *infk;
          
          infk = getenv ("KONVERT");
#ifdef WIN32
          if (infk == NULL)
          {
                        infk = "c:\\user\\fit\\konvert";
          }
          sprintf (inf_name, "%s\\%s", infk, info_name);
#else
          sprintf (inf_name, "%s/%s", infk, info_name);
#endif
                       
          inf = fopen (inf_name, "r");
          if (inf == 0) inf = fopen (info_name,"r");
          if (inf == 0)
          {
                        fprintf (stderr,"%s kann nicht geoeffnet werden\n",
                                         info_name);
                        exit (1);
          }

          testinclude ();

          holequelle (inf, satz);   /* Position fuer Quelle holen           */
          if (memcmp (satz, "$SWQ", 4) == 0)
          {
                        mit_swq ();
                        init_sw_start ();
                        return (0);
          }      
          feldseek = ftell (inf);
          feldanz (inf, satz);   /* Anzahl Felder feststellen                */
          if (feld_anz == 0) 
          {
                        return (0);
          }
          if (quelle)
          {
                        free (quelle);
                        free (eingabesatz);
          }
          quelle = malloc ((feld_anz + 2) * sizeof (FELDER));
          if (quelle == 0)
          {
                        fprintf (stderr,"Kein Speicherplatz\n");
                        exit (1);
          }
          fseek (inf,feldseek,0);
          ins_quelle (inf, satz);
          qlen = satzlen (quelle) + 2;
          eingabesatz = malloc (qlen + 2);
          if (eingabesatz == 0)
          {
                        fprintf (stderr,"Kein Speicherplatz\n");
                        exit (1);
          }

		  if (trenneri)
		  {
			         if (tsatz) free (tsatz);
                     tsatz = malloc (qlen + 2);
                     if (tsatz == 0)
					 {
                        fprintf (stderr,"Kein Speicherplatz\n");
                        exit (1);
					 }
					 memset (tsatz, ' ', qlen);
					 tsatz[qlen] = 0;
		  }
/*
          belege_code (inf, satz);
          belege_data (inf, satz);
*/
}

int holequelle (inf, satz)
/**
Tabellenname holen.
**/
FILE *inf;
char *satz;
{

          while (fgets (satz,80,inf))
          {
                     if (strupcmp (satz,"$SWQ",4) == 0)
                     { 
                                return (1);
                     }
                     if (strupcmp (satz,"$QUELLE",7) == 0)
                     { 
                                return (0);
                     }
          }
          return (-1);
          fprintf (stderr,"Quelle nicht gefunden\n");
          exit (1);
}

int ins_quelle (inf, satz)
/**
Felder fuer Satzaufbau fuellen.
**/
FILE *inf;
char *satz;
{
         short i;
         short (anz);

         i = 0;
         while (fgets(satz,255,inf))
         {
                  cr_weg (satz);
                  if (strlen (satz) < 3) break;
                  if (strupcmp (satz,"$UPDATE",7) == 0) break;
                  if (strupcmp (satz,"$DEFAULT",8) == 0) break;
                  if (strupcmp (satz,"$CODE",5) == 0) break;
                  if (strupcmp (satz,"$BEGIN",5) == 0) break;
                  if (strupcmp (satz,"$ENDE",5) == 0) break;
                  if (strupcmp (satz,"$DATA",5) == 0) break;
                  if (strupcmp (satz,"$ZIEL",5) == 0) break;
                  anz = zsplit (satz,','); 
                  if (anz < 5) continue;
                  strncpy (quelle [i].feldname,clipped (zwort [1]),19);
                  quelle [i].feldname [19] = 0;
                  quelle [i].feldlen = atoi (zwort [2]);
                  strncpy (quelle [i].feldtyp,clipped (zwort [3]),1);
                  quelle [i].feldtyp [1] = 0;
                  quelle [i].nachkomma = atoi (zwort [4]);
                  quelle [i].dimension = atoi (zwort [5]);
                  i ++;
                  if (i == feld_anz) break; 
      }
      quelle [i].feldname [0] = 0;
      quelle [i].feldlen = 0;
      strcpy (quelle [i].feldtyp,"C");
      quelle [i].nachkomma = 0;
      quelle [i].dimension = 0;
      show_aufbau (quelle);
}

int belege_ziel ()
/**
Satzaufbau fuer Quelldatei holen.
**/
{
          char satz [256];
          short i;
          long feldseek;
          short testlen;


          fseek (inf,0l,0);
          holeziel (inf, satz);   /* Positionen fuer Ziel holen               */
          if (memcmp (satz, "$SWZ", 4) == 0)
          {
                        mit_swz ();
                        init_sw_start ();
                        belege_code (inf, satz);
                        belege_data (inf, satz);
                        belege_structs (inf, satz);
                        belege_redefines (inf, satz);
                        return (0);
          }      
          feldseek = ftell (inf);
          feldanz (inf, satz);   /* Anzahl Felder feststellen                */
          if (feld_anz == 0) 
          {
                        belege_code (inf, satz);
                        belege_data (inf, satz);
                        belege_structs (inf, satz);
                        belege_redefines (inf, satz);
                        return (0);
          }
          if (ziel)
          {
                        free (ziel);
                        free (ausgabesatz);
          }
          ziel = malloc ((feld_anz + 2) * sizeof (FELDER));
          if (ziel == 0)
          {
                        fprintf (stderr,"Kein Speicherplatz\n");
                        exit (1);
          }
          fseek (inf,feldseek,0);
          ins_ziel (inf, satz);
          show_aufbau (ziel);
          zlen = satzlen (ziel) + 2;
          ausgabesatz = malloc (zlen + 2);
          if (ausgabesatz == 0)
          {
                        fprintf (stderr,"Kein Speicherplatz\n");
                        exit (1);
          }
          if (trenner || hochkomma)
          {
                        zwsatz = malloc (zlen + (feld_anz * 4) + 2);
                        if (zwsatz == 0)
                        {
                                      fprintf (stderr,"Kein Speicherplatz\n");
                                      exit (1);
                        }
          }
          init_ausgabesatz ();
          fseek (inf,feldseek,0);
          belege_value (satz,inf,&outsatz);
          belege_code (inf, satz);
          belege_data (inf, satz);
          belege_structs (inf, satz);
          belege_redefines (inf, satz);
}

int holeziel (inf, satz)
/**
Tabellenname holen.
**/
FILE *inf;
char *satz;
{

          while (fgets (satz,80,inf))
          {
                     if (strupcmp (satz,"$SWZ",4) == 0)
                     { 
                                return (1);
                     }
                     if (strupcmp (satz,"$ZIEL",5) == 0)
                     { 
                                return (0);
                     }
          }
          return (-1);
          fprintf (stderr,"Ziel nicht gefunden\n");
          exit (1);
}

int ins_ziel (inf, satz)
/**
Felder fuer Satzaufbau fuellen.
**/
FILE *inf;
char *satz;
{
         short i;
         short (anz);

         i = 0;
         while (fgets(satz,255,inf))
         {
                  cr_weg (satz);
                  if (strlen (satz) < 3) break;
                  if (strupcmp (satz,"$UPDATE",7) == 0) break;
                  if (strupcmp (satz,"$DEFAULT",8) == 0) break;
                  if (strupcmp (satz,"$CODE",5) == 0) break;
                  if (strupcmp (satz,"$DATA",5) == 0) break;
                  anz = zsplit (satz,','); 
                  if (anz < 5) continue;
                  strncpy (ziel [i].feldname,clipped (zwort [1]),19);
                  ziel [i].feldname [19] = 0;
                  ziel [i].feldlen = atoi (zwort [2]);
                  strncpy (ziel [i].feldtyp,clipped (zwort [3]),1);
                  ziel [i].feldtyp [1] = 0;
                  ziel [i].nachkomma = atoi (zwort [4]);
                  ziel [i].dimension = atoi (zwort [5]);
                  i ++;
                  if (i == feld_anz) break; 
      }
      ziel [i].feldname [0] = 0;
      ziel [i].feldlen = 0;
      strcpy (ziel [i].feldtyp,"C");
      ziel [i].nachkomma = 0;
      ziel [i].dimension = 0;
}

int mit_swq ()
/**
Satzaufbau Quelle aud SW-Kopf holen.
**/ 
{
      if (eingabe == 0)
      {
                fprintf (stderr,"Keine SW-Eingabedatei angegeben\n");
                exit (1);
      }
      lese_sw (eingabe);
      quelle = malloc ((banz + 2) * sizeof (FELDER));
      if (quelle == 0)
      {
                        fprintf (stderr,"Kein Speicherplatz\n");
                        exit (1);
      }
      sw_info (quelle);
      qlen = satzlen (quelle);
      eingabesatz = malloc (qlen + 2);
      if (eingabesatz == 0)
      {
                        fprintf (stderr,"Kein Speicherplatz\n");
                        exit (1);
      }
      swq = 1;
      show_aufbau (quelle);
      if (sw_rec) exit (0);
}

int mit_swz ()
/**
Satzaufbau Ziel aud SW-Kopf holen.
**/ 
{
      if (ausgabe == 0)
      {
                fprintf (stderr,"Keine SW-Eingabedatei angegeben\n");
                exit (1);
      }
      lese_sw (ausgabe);
      ziel = malloc ((banz + 2) * sizeof (FELDER));
      if (ziel == 0)
      {
                        fprintf (stderr,"Kein Speicherplatz\n");
                        exit (1);
      }
      sw_info (ziel);
      zlen = satzlen (ziel);
      ausgabesatz = malloc (zlen + 2);
      if (ausgabesatz == 0)
      {
                        fprintf (stderr,"Kein Speicherplatz\n");
                        exit (1);
      }
      swz = 1;
      show_aufbau (ziel);
      if (sw_rec) exit (0);
}

int showfelder (satz)
/**
Feldinhalte eines Satzes ansehen.
**/
RECORD *satz;
{
           short fpos;
           FELDER *felder;
           char *buffer;
           short i;

           if (feldinh == 0) return;

           felder = *satz->felder;
           buffer = *satz->buffer;
           for (i = 0; felder [i].feldname [0]; i ++)
           {
                           fpos = feldpos (felder, felder[i].feldname);
                           showfeld (&felder [i], buffer, fpos);
           }
           printf ("\n");
}

int showfeld (feld, buffer, fpos)
/**
Feldinhalt anzeigen.
**/
FELDER *feld;
char *buffer;
short fpos;
{

           union FADR fadr;
		   DATE_STRUCT *Date;
           
           fadr.fchar = &buffer [fpos];
           printf ("%-20s  ", feld->feldname);
           switch (feld->feldtyp [0])
           {
                           case 'C' :
                           case 'A' :
                           case 'N' :
                           case 'd' :
                                     printlen (fadr.fchar, feld->feldlen);
                                     break;
                           case 'G' :
							         Date = (DATE_STRUCT *) fadr.fchar;
							         printf ("%02hd.%02hd.%02hd\n", Date->day, Date->month,
										                            Date->year);  
                           case 'S' :
                                     printf ("%hd\n", *fadr.fshort);
                                     break;
                           case 'I' :
                                     printf ("%ld\n", *fadr.flong);
                                     break;
                           case 'D' :
                                     printf ("%lf\n", *fadr.fdouble);
                                     break;
                           default :
                                     printf ("\n");
                                     break;
            }
}

char *ohnehk (char *p)
/**
Anfuegrungszeichen entfernen.
**/
{
	char *hk;
	char *pold;
	char dest [512];
	int i;

	pold = p;
	for (p; *p != '\"'; p += 1) if (*p == 0) break;

	if (*p == '\"') 
	{
		p ++;
	}
	else
	{
		return pold;
	}

	strcpy (dest, p);
	for (i = 0,hk = dest; *hk != '\"'; hk += 1, i ++)
	{
		if (*hk == 0)
		{
			fprintf ("Fehler im Eingabesatz %s : erwartet \" \n", p);
			exit (1);
		}
		if (*hk == '\\') 
		{
			hk += 1;
 		    if (*hk == 0)
			{
			        fprintf ("Fehler im Eingabesatz %s : erwartet \" \n", p);
			        exit (1);
			}
			p[i] = *hk;
			continue;
		}
        p[i] = *hk;
	}
	*hk = 0;
	p[i] = 0;
	return (p);
}

static char *rstrtok (string, tr)
char *string;
char *tr;
{
	static char *str = (char *) 0;
	static int strend = 0;
	char *p;
	int i;

	if (string)	
	{
		p = string;
		str = string;
	}
	else
	{
		str += 1;
		p = str;
	}
	if (strend) return (char *) 0;
	if (p == (char *) 0) return (char *) 0;

	for (str; *str; str +=1)
	{
		for (i = 0; tr[i]; i ++)
		{
			if (*str == tr[i]) break;
		}
		if (tr[i]) break;
	}

	if (*str == 0) 
	{
		strend = 1;
	}
	else
	{
	    *str = 0;
	}
	return (p);
}


static void filltrec (satz, len)
/**
Wenn Eingabesatz variabel mit Trenner,
in oeriginalLange konvertieren.
**/
char *satz;
int len;
{
	int i;
	char *p;
	char tr [2];
	char feld [40];

	if (tsatz == (char *) 0) return;

	memset (tr, 0, 2);
	tr[0] = trenneri;
	cr_weg (satz);
	strcpy (tsatz, satz);
	memset (satz, ' ', len);

	p = rstrtok (tsatz, tr);
	if (p == (char *) 0) return;

	for (i = 0; i < feld_anz; i ++)
	{
		sprintf (feld, "quelle.%s", quelle[i].feldname);
		if (hochkomma && quelle[i].feldtyp[0] == 'C')
		{
			p = ohnehk (p);
		}
		mov (feld, "%s", p);
		p = rstrtok ((char*) 0, tr);
		if (p == (char *) 0) break;
	}
}


int rfgets (satz, len , datei)
/**
In Datei lesen.
**/
char *satz;
int len;
FILE *datei;
{

           int bytes;

           if (swq)
           {
                           bytes = readsw (satz, datei);
                           return (bytes);
           }
           else if (binaer) 
           {
                           bytes = fread (satz, 1, len, datei);
                           return (bytes);
           }
             
           if (fgets (satz, len, datei))
           {
			               if (trenneri) 
						   {
							   filltrec (satz, len);
						   }
                           return (1);
           }
           return (0);
}
