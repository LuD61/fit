#ifndef SINIX
#ident  "@(#) sysdate.c	2.001g	29.09.95	by RO"
#endif
/***
--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       sysdate.c
-
        Exports                 :       sysdatec ()
                                        systime ()

-       Autor                   :       WR    
-       Erstellungsdatum        :       03.04.92
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
        Modulbeschreibung       :       Systemdatum un d Systemzeit holen
--------------------------------------------------------------------------------
***/

#include <stdio.h>
#include <time.h>
#include <string.h>

short jahrh = 1900;

/* Procedure                              */

char *_sysdatec ();
char *_systime ();
int dlong_to_asc (); 
long dasc_to_long (); 

static int rstrncpy ();
static int zlen ();

char *sysdatec ()
{
  struct tm *dm;
  time_t dttime;
  int jr;
  static char sdatum[12];

  dttime = time ((long *) 0);
  dm = localtime (&dttime);
  if (dm->tm_year < 50)
  {
               jr = 2000 + dm->tm_year;
  }
  else
  {
               jr = 1900 + dm->tm_year;
  }
  sprintf(sdatum,"%02d.%02d.%04d",dm->tm_mday,dm->tm_mon + 1,jr);
  return (sdatum);
}

/* Systemzeit holen                                 */

char *systime ()
{
  struct tm *dm;
  time_t dttime;
  extern int daylight;
  static char szeit[10];

  dttime = time ((long *) 0);
//  daylight = 1;
  dm = localtime (&dttime);
  sprintf(szeit,"%02d:%02d:%02d",dm->tm_hour,dm->tm_min,dm->tm_sec);
  return (szeit);
}

int getwkday (dstr)
char *dstr;
{
 
  unsigned wtjahr, wtmon, wtage;
  short i;
  char datum[10];
  char *ps, *dende;
  int week_day;
  static unsigned mtage[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
  

  strcpy (datum,dstr);
  dende = datum + strlen (datum);
  
  ps = strchr (datum,'.');
  while ((ps) && (ps < dende))
  {
              *ps = ' ';
              ps = strchr (ps,'.');
  }

  sscanf (datum,"%d%d%d",&wtage,&wtmon,&wtjahr);
  
  wtjahr -= 70;
  
  for (i = 1; i < wtmon; i ++)
  {
            wtage = wtage + mtage[i - 1];
  }

  wtage = wtage + (wtjahr * 365 + (wtjahr + 2) / 4);
  if (((wtjahr - 2) % 4) == 0)
  {
            if (wtmon < 3)
            {
                     wtage --;
            }
  }
  week_day = (wtage + 3) % 7;
  return (week_day);
}

/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sysdate
-
-       In              :       keine
-
-       Out             :       char - pointer
-
-       Globals         :       Name            Datentyp        Beschreibung
-
-       Errorcodes      :       Name            Nr.             Beschreibung
-
-       Beschreibung    :	Gibt das Systemdatum im DB Format zurueck
-
--------------------------------------------------------------------------------
**/
long sysdate()

{
         char zeit_array[32];
         long db_datum;
         struct tm *dm;
         long   clock;
         int jrh;

         clock = time((long *)0);
         dm    = localtime (&clock);
         if (dm->tm_year < 50)
         {
                         jrh = 2000;
         }
         else
         {
                         jrh = 1900;
         }
         sprintf (zeit_array,"%2.2d.%2.2d.%4.4d",
                  dm->tm_mday,dm->tm_mon+1,dm->tm_year+jrh);

#ifdef ODBC
		 db_datum = get_dat_long (zeit_array, 10);
#else
         if(rdefmtdate(&db_datum,"dd.mm.yyyy",zeit_array))
             return((long) -1);
#endif
         return(db_datum);
}

long dasc_to_long (datum)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sysdate
-
-       In              :       keine
-
-       Out             :       char - pointer
-
-       Globals         :       Name            Datentyp        Beschreibung
-
-       Errorcodes      :       Name            Nr.             Beschreibung
-
-       Beschreibung    :	Gibt das Systemdatum im DB Format zurueck
-
--------------------------------------------------------------------------------
**/
char *datum;
{
         char zeit_array[32];
         char tag[3];
         char monat[3];
         char jahr[5];
         short ntag,nmonat,njahr;
         long db_datum;
         short mpos;
         short jpos;
         long (ret);

         if (strchr (datum,'.'))
         {
                     mpos = 3;
                     jpos = 6;
         }
         else
         {
                     mpos = 2;
                     jpos = 4;
         }

         memcpy (tag,datum,2);
         tag[2] = 0;
         memcpy (monat,&datum[mpos],2);
         monat[2] = 0;
         strcpy (jahr,&datum[jpos]);
         ntag   = atoi (tag);
         nmonat = atoi (monat);
         njahr  = atoi (jahr);
         if (strlen (jahr) == 2)
         {
                   sprintf (zeit_array,"%2.2d.%2.2d.%4.4d",
                                       ntag,nmonat,njahr+jahrh);
         }
         else
         {
                   sprintf (zeit_array,"%2.2d.%2.2d.%4.4d",
                                       ntag,nmonat,njahr);
         }

/*
         ret = rdefmtdate(&db_datum,"dd.mm.yyyy",zeit_array);
         if(ret != 0)
             return(ret);
         return(db_datum);
*/
#ifdef ODBC
		 db_datum = get_dat_long (zeit_array, 10);
#else
         if (rdefmtdate(&db_datum,"dd.mm.yyyy",zeit_array))
             return(-1);
#endif
         return(db_datum);
}

int dlong_to_asc (longdat,datstring)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       dlong_to_asc
-
-       In              :       longdat
                                datstring
-
-       Out             :       ret = 0      ok
                                    = 1      fehler
-
-       Globals         :       Name            Datentyp        Beschreibung
-
-       Errorcodes      :       Name            Nr.             Beschreibung
-
-       Beschreibung    :	Wandelt das  im DB Format-Datum in einen String
                                um
-
--------------------------------------------------------------------------------
**/
long longdat;
char *datstring;
{
#ifdef ODBC
	    get_dat_char (datstring, longdat, 10);
        return 1;  
#else
        return (rfmtdate (longdat,"ddmmyyyy",datstring));
#endif
}

int dlong_to_asc_p (longdat,datstring)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       dlong_to_asc
-
-       In              :       longdat
                                datstring
-
-       Out             :       ret = 0      ok
                                    = 1      fehler
-
-       Globals         :       Name            Datentyp        Beschreibung
-
-       Errorcodes      :       Name            Nr.             Beschreibung
-
-       Beschreibung    :	Wandelt das  im DB Format-Datum in einen String
                                um
-
--------------------------------------------------------------------------------
**/
long longdat;
char *datstring;
{
#ifdef ODBC
	    get_dat_char (datstring, longdat, 10);
        return 1;  
#else
        return (rfmtdate (longdat,"dd.mm.yyyy",datstring));
#endif
}

int datformat (zdatum,format, dat)
/**
Datum formatiert uebertragen.
**/
char *zdatum;
char *format;
char *dat;
{
         char *ps;
         char tag [3];
         char mon [3];
         char jahr [6];
         char *jr;
         short jlen;

         jr = jahr;
         rstrncpy (tag, dat, 2);
         ps = strchr (dat, '.');
         if (ps == 0)
         {
                    rstrncpy (mon, &dat [2], 2);
                    strncpy (jr, &dat [4],4);
                    jr [4] = 0;
         }
         else
         {
                    rstrncpy (mon, &dat [3], 2);
                    strncpy (jr, &dat [6],4);
                    jr [4] = 0;
         }
         clipped (jahr);
         ps = strchr (format, 'y');
         if (ps)
         {
                    jlen = zlen (ps, 'y', 4);
         }
         else
         {
                    jlen = 2;
         }
         if (jlen  == 2 && strlen (jahr) == 4)
         {
                    jr = &jahr [2];
         }
         if (strchr (format, '.') && format[0] == 'd')
         {
                    sprintf (zdatum,"%s.%s.%s", tag, mon, jr);
         }
         else if (strchr (format, '.') == 0 && format[0] == 'd')
         {
                    sprintf (zdatum,"%s%s%s", tag, mon, jr);
         }
         else if (strchr (format, '.') && format[0] == 'y')
         {
                    sprintf (zdatum,"%s.%s.%s", jr, mon, tag);
         }
         else if (strchr (format, '.') == 0 && format[0] == 'y')
         {
                    sprintf (zdatum,"%s%s%s", jr, mon, tag);
         }
         else
         {
                    strcpy (zdatum, dat);
         }
}


int timeformat (ztime, format, zeit)
/**
Datum formatiert uebertragen.
**/
char *ztime;
char *format;
char *zeit;
{
         char *ps;
         char std [3];
         char min [3];
         char sec [3];
         short slen;

         rstrncpy (std, zeit, 2);
         ps = strchr (zeit, ':');
         if (ps == 0)
         {
                    rstrncpy (min, &zeit [2], 2);
                    strncpy (sec, &zeit [4],2);
                    sec [2] = 0;
         }
         else
         {
                    rstrncpy (min, &zeit [3], 2);
                    strncpy (sec, &zeit [6],2);
                    sec [2] = 0;
         }
         ps = strchr (format, 's');
         if (ps)
         {
                    slen = 2;
         }
         else
         {
                    slen = 0;
         }
         if (strchr (format, ':'))
         {
                    sprintf (ztime,"%s:%s:s", std, min, sec);
                    if (slen == 0) ztime [5] = 0;
         }
         else
         {
                    sprintf (ztime,"%s%s%s", std, min, sec);
                    if (slen == 0) ztime [4] = 0;
         }
}

int rstrncpy (ziel, quelle, len)
/**
quelle in der Laenge len nach ziel kopieren.
Ziel mit Stringende auf Position len belegen. 
**/
char *ziel;
char *quelle;
short len;
{
         memcpy (ziel, quelle ,len);
         ziel [len] = 0;
}

int zlen (string, zeichen, max)
/**
Anzahl zeichen bis max feststellen.
**/
char *string;
char zeichen;
short max;
{
         short i;

         for (i = 0; i < max; i ++) if (string [i] != zeichen) break;
         return (i);
}
