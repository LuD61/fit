#ifndef SINIX
#ident  "@(#) asc_bws.c	VLUGLOTSA	20.03.97	by RO"
#endif
#ifndef SINI
#endif
/***
--------------------------------------------------------------------------------
-
-       Modulname               :       asc_bws.c
-
        Exports                 :
         
        Imports                 :

-       Interne Prozeduren      :  
-
-       Externe Prozeduren      :    
                                        
-
-       Autor                   :       WR    
-       Erstellungsdatum        :       18.01.93
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ESQL-C
-
-       Aenderungsjournal       :
-
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Ascii_Datei in BWS einbuchen.
                                    generieren.
-
--------------------------------------------------------------------------------
***/

#include <stdio.h>
#include <malloc.h>
#include <fcntl.h>
#include <string.h>
//#include <windows.h>
#ifdef ODBC
#include "dbclass.h"
#else
#include "mo_curso.h"
#endif
#include "objects.h"

#define KONVERT "KONVERT"

#define CHAR    1
#define SHORT   2
#define LONG    3
#define STRING  4
#define DOUBLE  5
#define HEX     6
#define SQLCVCHAR 7

#define COLNSIZE 128

extern char *zwort[];
extern char *wort[];
extern char *clipped ();
extern double ratod ();
extern char * upstrstr ();
extern char * make_name ();

extern char *zwort[];
extern char *wort[];
extern char *clipped ();
extern double ratod ();
extern char * upstrstr ();

extern short DEBUG;
extern short FDEBUG;
extern char sql[];
extern short sql_mode;
extern short dbz;


static char stddat []= {"01.01.2000"};
/* Typ-Definitionen                               */

/* Struktur fuer Feldbeschreibung                 */

typedef struct
       {   char  feldname [48];
           short feldlen;
           char  feldtyp [2]; 
           short nachkomma;
           short dimension;
       } FELDER;

typedef struct
       {   FELDER **felder;
           char   **buffer;
       } RECORD;

union FADR
      { char  *fchar;
        short *fshort;
        int   *fint;
        long  *flong;
        void  *fvoid;
        double *fdouble;
      };

extern short constat;
extern FELDER *quelle;      /* Tabelle mit Satzaufbau fuer Eingabefelder     */
extern short feld_anz;      /* Anzahl Felder im Satz                         */
extern RECORD insatz;

extern FELDER *ziel;        /* Tabelle mit Tabellenaufbau aus der Datenbank  */
extern short db_anz;        /* Anzahl Felder im Satz                         */
static int lesestatus;      /* SQL-Status beim Lesen                        */
extern RECORD outsatz;

extern FELDER *data;         /* Tabelle mit Variablen                       */
extern short data_anz = 0;   /* Anzahl Variablen                           */
extern RECORD datasatz;

static char dbtab [41];     /* Name der Datenbanktabelle                     */
static char *ascii_name;    /* Name der Ascii-Datei                          */
static char *info_name;     /* Name der Info-Datei                           */

extern char *eingabe;       /* Name Eingabedatei                             */
extern char *eingabesatz;   /* Buffer fuer Ascii-Eingabesatz                 */
extern short qlen;          /* Laenge eingabesatz                            */
extern char *ausgabesatz;   /* Buffer fuer Binaer-Ausgabesatz                */
extern short zlen;          /* Laenge ausgabesatz                            */
extern char *daten;         /* Buffer fuer allgemeien Variablen              */
extern short dlen;          /* Anzahl allgemeien Daten                       */
extern char *CR;
static FILE *eingabefile = (FILE *) 0;

static char *tsatz = (char *) 0;

static int lese_db ();
static int to_db ();
static int init_ausgabesatz ();

extern char where_part [512];     
                            /* Buffer fuer Where-Teil                        */
static char def_part [512];     
                            /* Buffer fuer Where-Teil Defaultsatz            */

static int sel_db = -1;     /* Cursor                                        */
static int sel_default = -1;/* Cursor                                        */
static int sel_upd = -1;    /* Cursor                                        */
static int ins_db = -1;
static int upd_db = -1;

static char *wh_strings [20]; /* Strings fuer where-Teil                    */

extern char *argsp [];      /* Speicher fuer Argumente                       */

extern int write_init;
extern int read_init;


extern short show_code;      /* Flag fuer Programmbereich                    */
static short mit_default = 0; /* Kennzeichen, ob ein Defaultsatz gelesen wird */
static short anzeigen = 0;  /* Flag fuer Anzeigemodus                       */
static short binaer = 0;    /* Flag fuer Binaermodus                        */
static short sqldebug = 0;  /* Flag fuer SQL-Debugmode                      */
static short feldinh  = 0;  /* Feldinhalte Ziel                             */
static short quick = 0;     /* Flag fuer schnelles einbuchen                */
static short test = 0;      /* Testmodus                                    */
static short mit_insert = 1; /* Flag, ob nur update                         */
static short lese_upd = 0;   /* Flag : lese_upd = 1 : Lesen erst vor Update */
extern int DebRun;       /* Debug-Kennzeichen                             */

extern int arganz;          /* Anzahl Argumente                             */
extern int dochain;
extern int chainanz;
extern char *chainarg[];
extern char *charg[];


char trenner = 0;      /* Trennzeichen zwischen Felder                 */
static int hochkomma = 0;     /* Flag fuer Hochkomma bei Typ C                */

static FILE *inf;
char dbase [256] = {"bws"};
char *tmpinfdat = NULL;

int settrenner (tr)
char tr;
{
          if (tr == 'p') 
		  {
			  tr = '|';  
		  }
          else if (tr == 't') 
		  {
			  tr = 9;
		  }
          trenner = tr;
}

int sethochkomma ()
{
          hochkomma = 1;
}

int tst_arg (arg)
char *arg;
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                            case 'a' :
                                     anzeigen = 1;
                                     break;
                            case 'b' :
                                     binaer = 1;
                                     break;
                            case 'd' :
//                                 sqldebug = 1;
                                     DebRun = 1;
                                     break;
                            case 'D' :
                                     DEBUG = 1;
                                     break;
                            case 'g' :
                                     FDEBUG = 1;
                                     break;
                            case 'i' :
                                     mit_insert = 0;
                                     break;
                            case 'l' :
                                     lese_upd = 1;
                                     quick = 0;
                                     break;
/*
                            case 't' :
                                     test = 1;
                                     break;
*/
                            case 'f' :
                                     feldinh = 1;
                                     break;
                            case 'm' :
                                     sql_mode = atoi (arg + 1);
                                     break;
                            case 'q' :
                                     quick = 1;
                                     lese_upd = 1;
                                     break;
                            case 'c' :
                                     show_code = 1;
                                     break;
                            case 't' :
                                     if (arg[1])
                                     {
                                             settrenner (arg[1]);
                                     }
                                     break;
                            case 'k' :
                                     sethochkomma ();
                                     break;
                            case 'h' :
                                     hilfe ();
                                     break;
                 }
          }
          return;
}

main (anz, arg)
int anz;
char *arg [];
{
          short i;

		  Text Mode;
		  char *e;

 		  if ((e = getenv ("ConnectMode")) != NULL)
		  {
			  TextInit (&Mode);
			  TextSet (&Mode, e);
			  TextMakeUpper (&Mode);
			  if (TextCompareC (&Mode, "FALSE") == 0)
			  {
					ConnectMode = FALSE;
			  }
			  else if (TextCompareC (&Mode, "TRUE") == 0)
			  {
					ConnectMode = TRUE;
			  }
			  TextDelete (&Mode);
		  }
/* Flags mit - testen                           */

          argtst (&anz, arg, tst_arg);

          if (anz < 3)
          {
                         fprintf (stderr,
                            "Usage asc_bws Ascii-Datei Info-Datei\n");
                         exit (1);
          }

          for (i = 0; i < 20; i ++)
          {
                     if (anz < i + 4) break;
                     argsp [i] = arg[i + 3];
          }
          arganz = i;


          new_procedure ();
		  dbz = 1;

          ascii_name = arg [1];
          info_name  = RifToIf (arg [2]);
		  do_ascbws ();
		  fclose (eingabefile);
		  if (dochain)
		  {
			  dochain = 0;
			  chainmain (chainanz, charg);
		  }
          close_all_cursor ();
		  closedbase (dbase);
          return 0;
}

chainmain (anz, arg)
int anz;
char *arg [];
{

          short i;

/* Flags mit - testen                           */

          argtst (&anz, arg, tst_arg);

          if (anz < 3)
          {
                         fprintf (stderr,
                            "Usage asc_bws Ascii-Datei Info-Datei\n");
		                 closedbase (dbase);
                         exit (1);
          }

          for (i = 0; i < 20; i ++)
          {
                     if (anz < i + 4) break;
                     argsp [i] = arg[i + 3];
          }
          arganz = i;

          ascii_name = arg [1];
          info_name  = RifToIf (arg [2]);
		  do_ascbws ();
		  fclose (eingabefile);
		  if (dochain)
		  {
			  dochain = 0;
			  chainmain (chainanz, charg);
		  }

		  for (i = 0; i < chainanz; i ++) 
		  {
			  if (chainarg[i]) free (chainarg[i]);
			  chainarg[i] = (char *) 0;
		  }
		  chainanz = 0;
}


int do_ascbws ()
{ 
	      sel_db = -1;
          belege_quelle ();
          belege_db ();
          if (anzeigen) 
          {
                         printf ("Satzlaenge Ascii %d\n", satzlen (quelle));
                         printf ("Satzlaenge DB    %d\n", satzlen (ziel));
          }
          ascii_to_db ();
          return 0;
}

int ascii_to_db ()
/**
Ascii-Datei in Datenbanktabelle uebertragen.
**/
{

          long ls;

          set_rddb (lese_db);
          set_wrziel (to_db, (char *)0);
          if (binaer)
          {
                 eingabefile = fopen (ascii_name,"rb");
          }
          else
          {
                 eingabefile = fopen (ascii_name,"r");
          }
          if (eingabefile == 0)
          {
                          fprintf (stderr,
                               "Ascii-Datei %s kann nicht geoeffnet werden\n", 
                                ascii_name);
		                  closedbase (dbase);
                          exit (1);
          }

          eingabe = ascii_name;
          perform_begin ();
          while (rfgets (eingabesatz,qlen,eingabefile))
          {
                        if (read_init) init_ausgabesatz ();
                        beginwork ();
                        to_ausgabe ();
                        if (lese_upd == 0) lese_db ();
                        showfelder (&outsatz);
                        to_ausgabe ();
                        perform_code ();
                        showfelder (&outsatz);
                        to_db (); 
                        commitwork ();
          }
          commitwork ();
          perform_ende ();
          fclose (eingabefile);
}

int to_ausgabe0 ()
/**
Eingaesatz in Ausgabesatz konvertieren.
**/
{
          short i;
          short zpos;

          for (i = 0; quelle[i].feldname[0]; i ++)  
          {
                   zpos = instruct (ziel, quelle[i].feldname);
                   if (zpos != -1)
                   {
                        in_to_out (quelle, ziel,&quelle[i], &ziel [zpos]); 
                   }
          }
}

int to_ausgabe ()
/**
Eingabesatz in Ausgabesatz konvertieren.
**/
{
          short i;
          short zpos;

          for (i = 0; ziel[i].feldname[0]; i ++)  
          {
                   znullvalue (ziel[i].feldname);
                   uebertrage (&outsatz, ziel [i].feldname,
                               &insatz, ziel [i].feldname);
// printf ("%s OK\n", ziel[i].feldname);
          }
}


static int init_ausgabesatz ()
{
         short i;
         union FADR feld;
         short pos;

         pos = 0;
         for (i = 0; ziel[i].feldname[0]; i ++)
         {
                   feld.fchar = &ausgabesatz [pos];
                   switch (ziel[i].feldtyp[0])
                   {
                           case 'C' :
                                  memset (feld.fchar,' ', ziel[i].feldlen);
								  feld.fchar[ziel[i].feldlen - 1] = 0;
                                  break;
                           case 'A' :
                                  memset (feld.fchar,' ', ziel[i].feldlen);
                                  feld.fchar[0] = 0;
                                  break;
                           case 'N' :
                                  memset (feld.fchar,'0', ziel[i].feldlen);
                                  break;
                           case 'B' :
                                  memset (feld.fchar,0, ziel[i].feldlen);
                                  break;
                           case 'd' :
                                  strcpy (feld.fchar,"01.01.1900");
                                  break;
/* SQL DateStruct */
                           case 'G' :
                                  FromGerDate ((DATE_STRUCT *) feld.fchar,"01.01.1900");
                                  break;
                           case 'S' :
                                  *feld.fshort = 0;
                                  break;
                           case 'I' :
                                  *feld.flong = 0;
                                  break;
                           case 'D' :
                                  *feld.fdouble = 0.0;
                                  break;
                    }
                    pos += ziel[i].feldlen;
           }
}

in_to_out (quelle, ziel, quellfeld, zielfeld)
/**
Quellfeld in Zielfeld uebertragen.
**/
FELDER *quelle;
FELDER *ziel;
FELDER *quellfeld;
FELDER *zielfeld;
{
          char quellwert [265];
          short spos;
          short len;
          char *qpos;
          union FADR zfeld;

/* Positionen im Buffer holen                        */

          spos = feldpos (quelle,quellfeld->feldname);
          qpos = &eingabesatz [spos];
          spos = feldpos (ziel,zielfeld->feldname);
          zfeld.fchar = &ausgabesatz [spos];

          memcpy (quellwert,qpos, quellfeld->feldlen);
          quellwert [quellfeld->feldlen] = 0;
          switch (zielfeld->feldtyp[0])
          {
                         case 'C' :
                                  len = quellfeld->feldlen;
                                  if (len > zielfeld->feldlen)
                                  {
                                           len = zielfeld->feldlen;
                                  }
                                  memcpy (zfeld.fchar,quellwert,len);
                                  break;
                         case 'd' :
                                  len = quellfeld->feldlen;
                                  if (len > zielfeld->feldlen)
                                  {
                                           len = zielfeld->feldlen;
                                  }
                                  memcpy (zfeld.fchar,quellwert,len);
                                  str_to_date (zfeld.fchar, zielfeld->feldlen, 
                                               len);  
                                  break;
                         case  'S' :
                                  sscanf (quellwert,"%hd",zfeld.fshort);
                                  break;
                         case  'I' :
                                  sscanf (quellwert,"%ld",zfeld.flong);
                                  break;
                         case  'D' :
                                  sscanf (quellwert,"%lf",zfeld.fdouble);
                                  nachkomma (zfeld.fdouble, 
                                            quellfeld->nachkomma);
                                  break;
                         case 'G' :
                        		 FromGerDate ((DATE_STRUCT *) zfeld.fchar, quellwert);
                                 break;
            }
}

int nachkomma (dwert, nk)
/**
Nachkommastellen auswerten.
**/
double *dwert;
short nk;
{
           short i;
           double rwert;

           if (nk == 0) return;

           rwert = *dwert;
           for (i = 0; i < nk; i ++)
           {
                       rwert /= 10;
           }
           *dwert = rwert;
}            


int str_to_date (dstring,dlen, qlen)
/**
String in Datumformat konvertieren.
**/
char *dstring;
short dlen;
short qlen;
{
          char datum [11];

          if (dstring [2] == '.' && dstring [5] == '.') 
          {
                    dstring [dlen -1] = (char) 0;
                    return (0);
          }
          if (memcmp (dstring,"0000000000", qlen) < 0)
          {
                    if (dlen == 9) 
                    {
                            strcpy (dstring, "01.01.80");
                    }
                    else if (dlen == 11)
                    {
                            strcpy (dstring, "01.01.1980");
                    }
                    return (0);
          }
          memcpy (datum,dstring,2);
          datum [2] = '.';
          datum [5] = '.';
          memcpy (datum,dstring,2);
          memcpy (&datum[3],&dstring[2],2);
          memcpy (&datum[6],&dstring[4],qlen - 4);
          qlen += 2;
          if (qlen > dlen) qlen = dlen;
          memcpy (dstring,datum,qlen);
          dstring [qlen] = 0;
          return (0);
}
          
int lese_db ()
/**
Aausgabesatz in Datenbank einfuegen.
**/
{
          if (sel_db == -1)
          {
                        prep_upd_cursor ();
          }

          if (where_part [0])
          {
                        open_sql (sel_db);
                        fetch_sql (sel_db);
          }
          else
          {
                        sqlstatus = 100;
          }
          lesestatus = sqlstatus;

// free_sql wegen msde-Datenbank, mysql nu� noch gepr�ft werden.

		  free_sql (sel_db);

          set_rec_wert (&datasatz, "sqlstatus",  &lesestatus, 2);
          if (sqldebug) printf ("sqlstatus beim Lesen %d\n", lesestatus);
          if (lesestatus == 100 && mit_default)
          {
                        open_sql (sel_default);
                        fetch_sql (sel_default);
                        set_rec_wert (&datasatz, "sqlstatus",  &lesestatus, 2);
                        if (sqldebug)
                              printf ("sqlstatus beim Defaultlesen %d\n", 
                                      sqlstatus);
          }
}

int to_db ()
/**
Aausgabesatz in Datenbank einfuegen.
**/
{
          if (constat) return;

          if (quick == 0)
          {
                       open_sql (sel_upd);
                       fetch_sql (sel_upd);
                       lesestatus = sqlstatus;
                       set_rec_wert (&datasatz, "sqlstatus",  &lesestatus, 2);

// free_sql wegen msde-Datenbank, mysql nu� noch gepr�ft werden.

					   free_sql (sel_upd);
          }
          if (lesestatus == 100 && mit_insert)
          {
                        execute_curs (ins_db);
                        if (anzeigen)
                        {
                                  printf ("Eingefuegte Saetze %hd\n",
                                           sqlerror [2]);
                        }
                        set_rec_wert (&datasatz, "sqlstatus",  &lesestatus, 2);
                        set_rec_wert (&datasatz, "sqlerror",  &sqlerror [2], 2);
          }
          else
          if (lesestatus == 0)
          {
                        execute_curs (upd_db);
                        if (anzeigen)
                        {
                                  printf ("Geaenderte Saetze %hd\n",
                                           sqlerror [2]);
                        }
                        set_rec_wert (&datasatz, "sqlstatus",  &lesestatus, 2);
                        set_rec_wert (&datasatz, "sqlerror",  &sqlerror [2], 2);
          }

          if (write_init) init_ausgabesatz ();
}

int prep_upd_cursor ()
/**
Cursor fuer Datenbank-Update oder -Insert vorbereiten.
**/
{

          select_cursor ();
          insert_cursor ();
          update_cursor ();
}
 

int select_cursor ()
/**
Cursor zum Lesen mit eindeutigem Index.
**/
{
          short wanz;
          short i;
          short typ;
          short spos;
          short fpos;
          void *fvar;
          short selfelder;

          if (where_part [0] == 0) 
          {         
                    quick = 1;
                    sel_db = 0;
                    return;
          }
          selfelder = 0;
          wanz = split (where_part);

          if (test) printf ("Ausgabesatz %ld\n", ausgabesatz); 
          sprintf (sql,"select ");
          for (i = 0; i < db_anz; i ++)
          {
                       typ  = getdbtyp (&ziel [i]);
                       fpos = feldpos (ziel, ziel [i].feldname);
                       fvar = &ausgabesatz [fpos];
                       if (typ == 0 || typ == 7)
                       {
                             out_quest (fvar,typ, ziel[i].feldlen);
                             if (test) 
                                   printf ("out_quest (%s (%ld), %hd, %hd)\n",
                                             ziel[i].feldname,
                                             fvar,
                                             typ,
                                             ziel[i].feldlen);
                       }
                       else
                       {
                             out_quest (fvar,typ, 0);
                             if (test) 
                                   printf ("out_quest (%s (%ld), %hd, %hd)\n",
                                              ziel[i].feldname,
                                               fvar,
                                               typ,
                                               0);
                       }
                       strcat (sql,ziel [i].feldname);
                       if (i < db_anz - 1) strcat (sql, ",");
                       selfelder ++;
          }

          if (selfelder == 0) return;

          for (i = 2; i <= wanz; i ++)
          {
                     spos = instruct (ziel, clipped (wort [i]));
                     if (spos != -1)
                     {
                               if (test_q (wanz, i) == -1) continue;
                               typ = getdbtyp (&ziel [spos]);
                               fpos = feldpos (ziel, wort [i]);
                               fvar = &ausgabesatz [fpos];
                               if (typ == 0 || typ == 7)
                               {
                                      ins_quest (fvar,typ, ziel[spos].feldlen);
                                      if (test) 
                                      printf 
                                         ("ins_quest (%s (%ld), %hd, %hd)\n",
                                             ziel[spos].feldname,
                                             fvar,
                                             typ,
                                             ziel[spos].feldlen - 1);
                               }
                               else
                               {
                                      ins_quest (fvar,typ, 0);
                                      if (test) 
                                      printf 
                                         ("ins_quest (%s (%ld), %hd, %hd)\n",
                                             ziel[spos].feldname,
                                             fvar,
                                             typ,
                                             0);
                               }
                     }
           }

           strcat (sql," from ");
           strcat (sql,dbtab);
           strcat (sql," ") ;
           strcat (sql,where_part);
           strcat (sql, " ");
//           if (quick)  strcat (sql,"for update");
// sqldebug = 1;
           if (sqldebug)  printf ("%s\n", sql);
           sel_db = prepare_sql (sql);
           select_default ();
           select_upd ();
}

int select_default ()
/**
Cursor zum Lesen mit eindeutigem Index.
**/
{
          short wanz;
          short i;
          short typ;
          short spos;
          short fpos;
          void *fvar;
          short selfelder;

          if (mit_default == 0) return;

          selfelder = 0;
          wanz = split (def_part);

          sprintf (sql,"select ");
          for (i = 0; i < db_anz; i ++)
          {
                       typ  = getdbtyp (&ziel [i]);
                       fpos = feldpos (ziel, ziel [i].feldname);
                       fvar = &ausgabesatz [fpos];
                       if (typ == 0 || typ == 7)
                       {
                             out_quest (fvar,typ, ziel[i].feldlen);
                       }
                       else
                       {
                             out_quest (fvar,typ, 0);
                       }
                       strcat (sql,ziel [i].feldname);
                       if (i < db_anz - 1) strcat (sql, ",");
                       selfelder ++;
          }

          if (selfelder == 0) return;

          for (i = 2; i <= wanz; i ++)
          {
                     spos = instruct (ziel, clipped (wort [i]));
                     if (spos != -1)
                     {
                               if (test_q (wanz, i) == -1) continue;
                               typ = getdbtyp (&ziel [spos]);
                               fpos = feldpos (ziel, wort [i]);
                               fvar = &ausgabesatz [fpos];
                               if (typ == 0 || typ == 7)
                               {
                                      ins_quest (fvar,typ, 
                                                 ziel[spos].feldlen - 1);
                               }
                               else
                               {
                                      ins_quest (fvar,typ, 0);
                               }
                     }
           }

           strcat (sql," from ");
           strcat (sql,dbtab);
           strcat (sql," ") ;
           strcat (sql,def_part);
           if (sqldebug)  printf ("%s\n", sql);
           sel_default = prepare_sql (sql);
}

int select_upd ()
/**
Cursor zum Lesen mit eindeutigem Index.
**/
{
          short wanz;
          short i;
          short typ;
          short spos;
          short fpos;
          void *fvar;
          short selfelder;

          if (where_part [0] == 0) return;
          if (quick)
          {
                    sel_upd = sel_db;
                    return;
          }

          selfelder = 0;
          wanz = split (where_part);

          sprintf (sql,"select ");
          for (i = 2; i <= wanz; i ++)
          {
                     spos = instruct (ziel, clipped (wort [i]));
                     if (spos != -1)
                     {
                               if (test_q (wanz, i) == -1) continue;
                               typ = getdbtyp (&ziel [spos]);
                               fpos = feldpos (ziel, wort [i]);
                               fvar = &ausgabesatz [fpos];
                               if (typ == 0 || typ == 7)
                               {
                                      ins_quest (fvar,typ, 
                                                 ziel[spos].feldlen - 1);
                               }
                               else
                               {
                                      ins_quest (fvar,typ, 0);
                               }
                               strcat (sql,wort [i]);
                               if (i < wanz - 2) strcat (sql, ",");
                               selfelder ++;
                     }
           }
           if (selfelder == 0) return;

           strcat (sql," from ");
           strcat (sql,dbtab);
           strcat (sql," ") ;
           strcat (sql,where_part);
/*
           strcat (sql, " ");
           strcat (sql,"for update");
*/
           if (sqldebug)  printf ("%s\n", sql);
           sel_upd = prepare_sql (sql);
}

int test_q (wanz, pos)
/**
Test, ob das uebernaechste Zeichen ein ? ist.
**/
short wanz;
short pos;
{
           pos += 2;
           if (pos > wanz) return (-1);
           if (wort [pos] [0] == '?') return (0);
           return (-1);
}

int update_cursor ()
/**
Cursor fuer Update Index.
**/
{
           void *fvar;
           short typ;
           short spos;
           short fpos;
	       short wanz; 
           short selfelder;

           short i; 
           short pos;
           char *dbadr;
           static char updname [80];

           if (where_part [0] == 0) return;
           pos = 0;
           for (i = 0; i < db_anz; i ++)
           {
                      dbadr = &ausgabesatz [pos];
                      switch (ziel[i].feldtyp[0])
                      {
                            case 'd' :
                                      ins_quest (dbadr, 0, ziel[i].feldlen);
									  break;
                            case 'C' :
                                      ins_quest (dbadr, 0, ziel[i].feldlen);
                                      break;
                            case 'S' :
                                      ins_quest (dbadr, 1, 0);
                                      break;
                            case 'I' :
                                      ins_quest (dbadr, 2, 0);
                                      break;
                            case 'D' :
                                      ins_quest (dbadr, 3, 0);
                                      break;
                            case 'G' :
                                      ins_quest (dbadr, 4, 0);
                                      break;
                            case 'J' :
                                      ins_quest (dbadr, 5, 0);
                                      break;
                            case 'K' :
                                      ins_quest (dbadr, 6, 0);
                                      break;
                            case 'A' :
                                      ins_quest (dbadr, 7, ziel[i].feldlen);
                                      break;
                      }
                      pos += ziel[i].feldlen;
            }

            selfelder = 0;
            wanz = split (where_part);
            for (i = 2; i <= wanz; i ++)
			{
                     spos = instruct (ziel, clipped (wort [i]));
                     if (spos != -1)
                     {
                               if (test_q (wanz, i) == -1) continue;
                               typ = getdbtyp (&ziel [spos]);
                               fpos = feldpos (ziel, wort [i]);
                               fvar = &ausgabesatz [fpos];
                               if (typ == 0 || typ == 7)
                               {
                                      ins_quest (fvar,typ, 
                                                 ziel[spos].feldlen - 1);
                               }
                               else
                               {
                                      ins_quest (fvar,typ, 0);
                               }
                               selfelder ++;
                     }
			}
/*
            sprintf (sql,"update %s set (",dbtab);
            for (i = 0; i < db_anz - 1; i ++)
            {
                     strcat (sql,ziel[i].feldname);
                     strcat (sql,",");
            }
            strcat (sql,ziel[i].feldname);
            strcat (sql,") = (");
            for (i = 0; i < db_anz - 1; i ++)
            {
                     strcat (sql,"?,");
            }
            strcat (sql,"?)");
*/
            sprintf (sql,"update %s set ",dbtab);
            for (i = 0; i < db_anz - 1; i ++)
            {
/*
                     if (ziel[i].feldtyp[0] == 'd') continue; 
                     if (ziel[i].feldtyp[0] == 'D') continue; 
                     if (ziel[i].feldtyp[0] == 'C') continue; 
*/
                     strcat (sql,ziel[i].feldname);
					 strcat (sql," = ?");
                     strcat (sql,",");
            }
            strcat (sql,ziel[i].feldname);
 		    strcat (sql," = ?");
			strcat (sql, " ");
            strcat (sql,where_part);
/*
            sprintf (updname," where current of %s", cursor_name (sel_upd)); 
            strcat (sql,updname);
*/
            if (sqldebug)  printf ("%s\n", sql);
            upd_db = prepare_sql (sql);
}

int insert_cursor ()
/**
Insert-Cursor vorbereiten.
**/
{
           short i; 
           short pos;
           char *dbadr;
           double dwert;

           pos = 0;
           for (i = 0; i < db_anz; i ++)
           {
                      dbadr = &ausgabesatz [pos];
                      switch (ziel[i].feldtyp[0])
                      {
                            case 'd' :
                                      ins_quest (dbadr, 0, ziel[i].feldlen);
                                      break;
                            case 'C' :
                                      ins_quest (dbadr, 0, ziel[i].feldlen);
                                      break;
                            case 'S' :
                                      ins_quest (dbadr, 1, 0);
                                      break;
                            case 'I' :
                                      ins_quest (dbadr, 2, 0);
                                      break;
                            case 'D' :
                                      ins_quest (dbadr, 3, 0);
                                     break;
                            case 'G' :
                                      ins_quest (dbadr, 4, 0);
                                      break;
                            case 'J' :
                                      ins_quest (dbadr, 5, 0);
                                      break;
                            case 'K' :
                                      ins_quest (dbadr, 6, 0);
                                      break;
                            case 'A' :
                                      ins_quest (dbadr, 7, ziel[i].feldlen);
                                      break;
                      }
                      pos += ziel[i].feldlen;
            }
            sprintf (sql,"insert into %s (",dbtab);
            for (i = 0; i < db_anz - 1; i ++)
            {
                        strcat (sql,ziel[i].feldname);
                        strcat (sql,",");
            }
            strcat (sql,ziel[i].feldname);
            strcat (sql,") values (");
            for (i = 0; i < db_anz - 1; i ++)
            {
                        strcat (sql,"?,");
            }
            strcat (sql,"?)");
            if (sqldebug)  printf ("%s\n", sql);
            ins_db = prepare_sql (sql);
}


/***********

Include-Datei einlesen   

*/


int includefile (incl, satz)
/**
Includedatei einlesen.
**/
FILE *incl;
char *satz;
{
          int anz;
          FILE *incfile;
          char iname [512];
		  char ename[512];
          char *konv;

          cr_weg (satz);
          anz = split (satz);
          if (anz < 2) return (-1);

          set_env (ename, wort[2]);

          strcpy (iname, ename);
		  if ((strchr (ename, '\\') == NULL) &&
              (strchr (ename, '/') == NULL))
		  {
			konv = getenv ("KONVERT");
			if (konv)
			{
#ifdef WIN32
                 sprintf (iname, "%s\\%s", konv, wort[2]);
#else
                 sprintf (iname, "%s/%s", konv, wort[2]);
#endif
			}
		  }

          incfile = fopen (iname, "r");
          if (incfile == (FILE *) 0)
          {
                 strcpy (iname, wort[2]);
				 incfile = fopen (iname, "r");
          }
          if (incfile == (FILE *) 0)
          {
                 fprintf (stderr, "Includedatei %s nichht gefunden\n", iname);
		         closedbase (dbase);
                 exit (1);
          }

          while (fgets (satz, 511, incfile))
          {
                  fputs (satz, incl);
          }
          fclose (incfile);
          return (0);
 }

int leseinclude (satz)
/**
Includes lesen.
**/
char *satz;
{
          FILE *incl;
          char *tmp;
          static char idat[512];

          tmp = getenv ("TMPPATH");
          if (tmp != NULL) 
          {
                   sprintf (idat, "%s\\infdat.tmp", tmp);
          }
          else
          {
                   sprintf (idat, "infdat.tmp");
          }
          incl = fopen (idat, "w");
          if (incl == (FILE *) 0)
		  {
                   sprintf (idat, "infdat.tmp");
				   incl = fopen (idat, "w");
		  }
          if (incl == (FILE *) 0)
          {
                     fprintf (stderr, "Fehler beim Oeffnen von temp. Datei\n");
//		             closedbase (dbase);
                     exit (1);
          }

          tmpinfdat = idat;  
          while (fgets (satz, 511, inf))
          {
                    if (memcmp (satz, "$INCLUDE", 8) == 0)
                    {
                               includefile (incl, satz);
                    }
                    else
                    {
                               fputs (satz, incl);
                    }
          }
          fclose (incl);
          fclose (inf);
          inf = fopen (idat, "r");
          if (inf == (FILE *) 0)
          {
                     fprintf (stderr, "Fehler beim Oeffnenn %s",
                              idat);
//		             closedbase (dbase);
                     exit (1);
          }
          return (0);
}

int testinclude ()
/**
Includes suchen.
**/
{
          char satz [516];

          while (fgets (satz, 511, inf))
          {
                    if (memcmp (satz, "$INCLUDE", 8) == 0)
                    {
                               fseek (inf, 0l, 0);
                               leseinclude (satz);
							   break;
                    }
          }
          fseek (inf, 0l, 0);
          return (0);
}

/***********

Ende Include-Datei einlesen   

*/

int belege_quelle ()
/**
Satzaufbau fuer Quelldatei holen.
**/
{
//          FILE *inf;
          char satz [1024];
          short i;
          long feldseek;
		  char iname[512];
		  LPSTR Konvert;

		  Konvert = getenv ("KONVERT");

		  strcpy (iname, info_name);
		  if (Konvert != NULL)
		  {
			if ((strchr (info_name, "\\") == NULL) && (strchr (info_name, "/") == NULL))
			{
				sprintf (iname, "%s\\%s", Konvert, info_name);
			}
			
		  }
 		  inf = fopen (iname, "r");
          if (inf == 0) inf = fopen (info_name,"r");
          if (inf == 0)
          {
                        fprintf (stderr,"%s kann nicht geoeffnet werden\n",
                                         make_name (KONVERT,info_name));
		                closedbase (dbase);
                        exit (1);
          }

          testinclude (); 

          database (inf, satz);   /* Tabellenname holen                      */
		  opendbase (dbase);
 	      setddbase ();
          tabname (inf, satz);   /* Tabellenname holen                       */
          feldseek = ftell (inf);
          feldanz (inf, satz);   /* Anzahl Felder feststellen                */
          if (feld_anz == 0) 
          {
                        belege_where_part (inf, satz); 
                        belege_default (inf, satz); 
                        belege_code (inf, satz);
                        belege_data (inf, satz);
                        belege_structs (inf, satz);
                        belege_redefines (inf, satz);
                        return (0);
          }
		  if (quelle) free (quelle);
          quelle = malloc ((feld_anz + 2) * sizeof (FELDER));
          if (quelle == 0)
          {
                        fprintf (stderr,"Kein Speicherplatz\n");
       		            closedbase (dbase);
                        exit (1);
          }
          fseek (inf,feldseek,0);
          ins_quelle (inf, satz);
          if (binaer)
          {
                         qlen = satzlen (quelle);
          }
          else
          {
                         qlen = satzlen (quelle) + 3;
          }
		  if (eingabesatz) free (eingabesatz); 
          eingabesatz = malloc (qlen + 2);
          if (eingabesatz == 0)
          {
                        fprintf (stderr,"Kein Speicherplatz\n");
		                closedbase (dbase);
                        exit (1);
          }
		  if (trenner)
		  {
			         if (tsatz) free (tsatz);
                     tsatz = malloc (qlen + 2);
                     if (tsatz == 0)
					 {
                        fprintf (stderr,"Kein Speicherplatz\n");
		                closedbase (dbase);
                        exit (1);
					 }
					 memset (tsatz, ' ', qlen);
					 tsatz[qlen] = 0;
		  }

          belege_where_part (inf, satz); 
          belege_default (inf, satz); 
          belege_code (inf, satz);
          belege_data (inf, satz);
          belege_structs (inf, satz);
          belege_redefines (inf, satz);
}

int belege_where_part (inf, satz)
/**
Nach Suchkriterium im Info_file suchen.
**/
FILE *inf;
char *satz;
{
          short pos;
          char *spos;

          memset (where_part,512,0);
          fseek (inf,0l,0);
          while (fgets (satz,1023,inf))
          {
                     if (strupcmp (satz,"$UPDATE",7) == 0)
                     { 
                                   cr_weg (satz);
                                   pos = first_ze (satz, 7);
                                   if (pos == 0) return;
                                   spos = &satz [pos];
                                   spos = strstr (spos, "where");
                                   if (spos == 0) return;
                                   strcpy (where_part, spos);
                                   break;;
                     }
          }
}

int belege_default (inf, satz)
/**
Nach Suchkriterium fuer Defaultsatz Info_file suchen.
**/
FILE *inf;
char *satz;
{
          short pos;
          char *spos;

          memset (def_part,512,0);
          mit_default = 0;
          fseek (inf,0l,0);
          while (fgets (satz,1023,inf))
          {
                     if (strupcmp (satz,"$DEFAULT",8) == 0)
                     { 
                                   cr_weg (satz);
                                   pos = first_ze (satz, 7);
                                   if (pos == 0) return;
                                   spos = &satz [pos];
                                   spos = strstr (spos, "where");
                                   if (spos == 0) return;
                                   strcpy (def_part, spos);
                                   mit_default = 1;
                                   break;
                     }
          }
}


int database (inf, satz)
/**
Tabellenname holen.
**/
FILE *inf;
char *satz;
{


          while (fgets (satz,255,inf))
          {
                     if (strupcmp (satz,"$DBASE",6) == 0)
                     { 
                                fgets (satz, 255, inf);
                                cr_weg (satz); 
                                strcpy (dbase, satz);
                                clipped (dbase);
								fseek (inf, 0l, 0);
                                return (0);
                     }
          }
          fseek (inf, 0l, 0);
		  return (0);
}


int tabname (inf, satz)
/**
Tabellenname holen.
**/
FILE *inf;
char *satz;
{


          while (fgets (satz,255,inf))
          {
                     if (strupcmp (satz,"$TABLE",6) == 0)
                     { 
                                fgets (satz, 255, inf);
                                cr_weg (satz); 
                                strncpy (dbtab, satz,40);
                                dbtab [40] = 0;
                                return (0);
                     }
          }
          fprintf (stderr,"Tabellenname nicht gefunden\n");
		  closedbase (dbase);
          exit (1);
}

int ins_quelle (inf, satz)
/**
Felder fuer Satzaufbau fuellen.
**/
FILE *inf;
char *satz;
{
         short i;
         short (anz);

         i = 0;
         while (fgets(satz,255,inf))
         {
                  cr_weg (satz);
                  if (strupcmp (satz,"$UPDATE",7) == 0) break;
                  if (strupcmp (satz,"$DEFAULT",8) == 0) break;
                  if (strupcmp (satz,"$CODE",5) == 0) break;
                  if (strupcmp (satz,"$DATA",5) == 0) break;
                  anz = zsplit (satz,','); 
                  if (anz < 5) continue;
                  strncpy (quelle [i].feldname,clipped (zwort [1]),47);
                  quelle [i].feldname [47] = 0;
                  quelle [i].feldlen = atoi (zwort [2]);
                  strncpy (quelle [i].feldtyp,clipped (zwort [3]),1);
                  quelle [i].feldtyp [1] = 0;
                  quelle [i].nachkomma = atoi (zwort [4]);
                  quelle [i].dimension = atoi (zwort [5]);
                  i ++;
                  if (i == feld_anz) break; 
      }
      quelle [i].feldname [0] = 0;
      quelle [i].feldlen = 0;
      strcpy (quelle [i].feldtyp,"C");
      quelle [i].nachkomma = 0;
      quelle [i].dimension = 0;
      show_aufbau (quelle);
}

int belege_db ()
/**
Satzaufbau fuer Datenbanktabelle belegen.
**/
{
      int tabid;
      short felder;
      int colcurs;
      char colname [COLNSIZE];
      short coltype;
      short collength;
	  long precision;
      short scale; 
      char sqltext [256];
      int tabcurs;
      short i;
      short j;

#ifdef ODBC
	  sprintf (sqltext, "select * from %s", clipped (dbtab));
	  tabcurs = prepare_sql (sqltext);
      db_anz = get_colanz(tabcurs); 
#else
      execute_sql ("select tabid, ncols from systables where tabname = \"%s\"",
                    dbtab);
      tabid  = isqlvalue (0);
      db_anz = ssqlvalue (1);
#endif
	  if (ziel) free (ziel);
      ziel = malloc ((db_anz + 2) * sizeof (FELDER));
      if (ziel == 0)
      {
                        fprintf (stderr,"Kein Speicherplatz\n");
		                closedbase (dbase);
                        exit (1);
      }

      i = 0;
#ifdef ODBC
      colcurs = prepare_columns (clipped (dbtab), NULL);
	  bind_sqlcol (colcurs, 4, (char *) colname,   SQLCCHAR, COLNSIZE);
	  bind_sqlcol (colcurs, 5, (short *) &coltype, SQLCSHORT, 0);
	  bind_sqlcol (colcurs, 7, (long *) &precision, SQLCLONG, 0);
	  bind_sqlcol (colcurs, 8, (short *) &collength, SQLCSHORT, 0);
	  bind_sqlcol (colcurs, 9, (short *) &scale, SQLCSHORT, 0);
#else
      out_quest (colname,0,COLNSIZE - 1);
      out_quest (&coltype,1,0);
      out_quest (&collength,1,0);
      ins_quest (&tabid,2,0);
      colcurs = prepare_sql ("select colname, coltype,collength from "
                             "syscolumns where tabid = ?");
#endif
//#ifndef ODBC
      fetch_sql (colcurs);
      while (sqlstatus == 0)
      {
#ifdef ODBC
/* 1. Versuch die Feldinformationen zu holen. Funktioniert nicht mit jedem Treiber.
      OK bei mysql, Informix. Nicht OK bei Oracle (Linux).
*/	  
     
			     if (coltype == SQLDECIMAL)
				 {
					 collength = (precision << 8) | scale;
				 }
			     else if (coltype == SQLCHAR)
				 {
//					 collength ++;
				 }
/*
				 else if (coltype == SQLDATE)
				 {
					 collength = sizeof (DATE_STRUCT);
				 }
*/
#endif
                 belege_ziel (&ziel [i],colname, coltype, collength);
                 i ++;
                 if (i == db_anz) break;
                 fetch_sql (colcurs);
      }
//#endif
#ifdef ODBC
/* 2. Versuch die Feldinformationen zu holen. Funktioniert nicht mit jedem Treiber.
      OK bei Oracle, Informix. Nicht OK bei mysql (Windows).
*/	  
      if (i < db_anz)
      {
                 for (i = 0; i < db_anz; i ++)
                 {
                          get_colname (i + 1, tabcurs, colname);    
                          coltype = get_coltype (i + 1, tabcurs);
                          if (coltype < 0 || coltype > 100)
                          {
                                 coltype = SQLSMINT;
                                 collength = 2;
                          } 
                          collength = get_collength (i + 1, tabcurs); 
                          belege_ziel (&ziel [i],colname, coltype, collength);
                  }
                  db_anz = i;  
      }
      close_sql (tabcurs);
#endif
      ziel [i].feldname [0] = 0;
      ziel [i].feldlen = 0;
      strcpy (ziel [i].feldtyp,"C");
      ziel [i].nachkomma = 0;
      ziel [i].dimension = 0;
      close_sql (colcurs);
      show_aufbau (ziel); 
      zlen = satzlen (ziel);
	  if (ausgabesatz) free (ausgabesatz);
      ausgabesatz = malloc (zlen + 2);
      if (ausgabesatz == 0)
      {
                    fprintf (stderr,"Kein Speicherplatz\n");
		            closedbase (dbase);
                    exit (1);
      }
      if (feld_anz == 0)
      {
                    feld_anz = db_anz;
                    ascii_typ (&quelle, ziel, feld_anz);
                    show_aufbau (quelle); 
                    qlen = satzlen (quelle) + 2;
                    eingabesatz = malloc (qlen + 2);
                    if (eingabesatz == 0)
                    {
                                  fprintf (stderr,"Kein Speicherplatz\n");
		                          closedbase (dbase);
                                  exit (1);
                    }
      }
}

int belege_ziel (ziel, colname, coltype, collength)
/**
Satzstruktur fuer ein Datenbankfeld belegen.
**/
FELDER *ziel;
char *colname;
short coltype;
short collength;
{

      strcpy (ziel->feldname, clipped (colname));
      switch (coltype)
      { 
                case SQLCHAR :
                          ziel->feldlen = collength + 1;
//                          ziel->feldlen = collength;
                          strcpy (ziel->feldtyp,"C");
                          ziel->nachkomma = 0;
                          ziel->dimension = 1;
                          break;
                case SQLVCHAR :
                          ziel->feldlen = collength + 1;
                          strcpy (ziel->feldtyp,"A");
                          ziel->nachkomma = 0;
                          ziel->dimension = 1;
                          break;
                case SQLSMINT :
                          ziel->feldlen = sizeof (short);
                          strcpy (ziel->feldtyp,"S");
                          ziel->nachkomma = 0;
                          ziel->dimension = 1;
                          break;
                case SQLINT :
#ifndef ODBC
                case SQLSERIAL :
#endif
                          ziel->feldlen = sizeof (long);
                          strcpy (ziel->feldtyp,"I");
                          ziel->nachkomma = 0;
                          ziel->dimension = 1;
                          break;
                case SQLDECIMAL :
#ifndef ODBC
                case SQLMONEY :
#endif
// !!! Achtung : wegen oracle auf Linux wird hier mit Character stat Double gelesen.

                          ziel->feldlen = sizeof (double);
                          strcpy (ziel->feldtyp,"D");
                          ziel->nachkomma = collength;
/*
                           ziel->feldlen = 20;
                           strcpy (ziel->feldtyp,"C");
                          ziel->nachkomma = collength;
*/
                          ziel->dimension = 1;
                          break;
                case SQLFLOAT :
                          ziel->feldlen = sizeof (double);
                          strcpy (ziel->feldtyp,"D");
                          ziel->nachkomma = 0;
                          ziel->dimension = 1;
                          break;
//                case SQLDATE :
//                case SQLTIMESTAMP :
//                          ziel->feldlen = 11;
					      ziel->feldlen = sizeof (DATE_STRUCT);
                          strcpy (ziel->feldtyp,"G");
                          ziel->nachkomma = 0;
                          ziel->dimension = 1;
                          break;
/*
                case SQLDATE :

					      ziel->feldlen = sizeof (DATE_STRUCT);
                          strcpy (ziel->feldtyp,"G");
                          ziel->nachkomma = 0;
                          ziel->dimension = 1;
                          break;
*/
                case SQLTIME :
					      ziel->feldlen = sizeof (TIME_STRUCT);
                          strcpy (ziel->feldtyp,"J");
                          ziel->nachkomma = 0;
                          ziel->dimension = 1;
                          break;
                case SQLDATE :
                case SQLTIMESTAMP :
					      ziel->feldlen = sizeof (TIMESTAMP_STRUCT);
                          strcpy (ziel->feldtyp,"K");
                          ziel->nachkomma = 0;
                          ziel->dimension = 1;
                          break;
           }
}


int showfelder (satz)
/**
Feldinhalte eines Satzes ansehen.
**/
RECORD *satz;
{
           short fpos;
           FELDER *felder;
           char *buffer;
           short i;

           if (feldinh == 0) return;

           felder = *satz->felder;
           buffer = *satz->buffer;
           for (i = 0; felder [i].feldname [0]; i ++)
           {
                           fpos = feldpos (felder, felder[i].feldname);
                           showfeld (&felder [i], buffer, fpos);
           }
           printf ("\n");
}

int showfeld (feld, buffer, fpos)
/**
Feldinhalt anzeigen.
**/
FELDER *feld;
char *buffer;
short fpos;
{

           union FADR fadr;
		   DATE_STRUCT *Date;
           
           fadr.fchar = &buffer [fpos];
           printf ("%-20s  ", feld->feldname);
           switch (feld->feldtyp [0])
           {
                           case 'C' :
                           case 'A' :
                           case 'N' :
                           case 'd' :
//							         printf ("%s|\n",fadr.fchar); 
                                     printlen (fadr.fchar, feld->feldlen);
                                     break;
                           case 'G' :
							         Date = (DATE_STRUCT *) fadr.fchar;
							         printf ("%02hd.%02hd.%02hd\n", Date->day, Date->month,
										                            Date->year);  
                           case 'S' :
                                     printf ("%hd\n", *fadr.fshort);
                                     break;
                           case 'I' :
                                     printf ("%ld\n", *fadr.flong);
                                     break;
                           case 'D' :
                                     printf ("%lf\n", *fadr.fdouble);
                                     break;
                           default :
                                     printf ("\n");
                                     break;
            }
}

char *ohnehk (char *p)
/**
Anfuegrungszeichen entfernen.
**/
{
	char *hk;
	char *pold;
	char dest [512];
	int i;

	pold = p;
	for (p; *p != '\"'; p += 1) if (*p == 0) break;

	if (*p == '\"') 
	{
		p ++;
	}
	else
	{
		return pold;
	}

	strcpy (dest, p);
	for (i = 0,hk = dest; *hk != '\"'; hk += 1, i ++)
	{
		if (*hk == 0)
		{
			fprintf ("Fehler im Eingabesatz %s : erwartet \" \n", p);
		    closedbase (dbase);
			exit (1);
		}
		if (*hk == '\\') 
		{
			hk += 1;
 		    if (*hk == 0)
			{
			        fprintf ("Fehler im Eingabesatz %s : erwartet \" \n", p);
		            closedbase (dbase);
			        exit (1);
			}
			p[i] = *hk;
			continue;
		}
        p[i] = *hk;
	}
	*hk = 0;
	p[i] = 0;
	return (p);
}

static char *rstrtok (string, tr)
char *string;
char *tr;
{
	static char *str = (char *) 0;
	static int strend = 0;
	char *p;
	int i;

	if (string)	
	{
		p = string;
		str = string;
		strend = 0;
	}
	else
	{
		str += 1;
		p = str;
	}
	if (strend) return (char *) 0;
	if (p == (char *) 0) return (char *) 0;

	for (str; *str; str +=1)
	{
		for (i = 0; tr[i]; i ++)
		{
			if (*str == tr[i]) break;
		}
		if (tr[i]) break;
	}

	if (*str == 0) 
	{
		strend = 1;
	}
	else
	{
	    *str = 0;
	}
	return (p);
}

static void filltrec (satz, len)
/**
Wenn Eingabesatz variabel mit Trenner,
in oeriginalLange konvertieren.
**/
char *satz;
int len;
{
	int i;
	char *p;
	char tr [2];
	char feld [40];

    if (trenner == 0) return;

	if (tsatz == (char *) 0)
    {
           tsatz = malloc (qlen + 2);
           if (tsatz == 0)
           {
                        fprintf (stderr,"Kein Speicherplatz\n");
		                closedbase (dbase);
                        exit (1);
           }
		   memset (tsatz, ' ', qlen);
		  tsatz[qlen] = 0;
    }

	memset (tr, 0, 2);
	tr[0] = trenner;
	cr_weg (satz);
	strcpy (tsatz, satz);
	memset (satz, ' ', len);

	p = rstrtok (tsatz, tr);
	if (p == (char *) 0) return;

	for (i = 0; i < feld_anz; i ++)
	{
		sprintf (feld, "quelle.%s", quelle[i].feldname);
		if (hochkomma && quelle[i].feldtyp[0] == 'C')
		{
			p = ohnehk (p);
		}
		mov (feld, "%s", p);
		p = rstrtok ((char*) 0, tr);
		if (p == (char *) 0) 
		{
			break;
		}
	}
}


int rfgets (satz, len , datei)
/**
In Datei lesen.
**/
char *satz;
int len;
FILE *datei;
{

           int bytes;

           if (binaer) 
           {
                           bytes = fread (satz, 1, len, datei);
                           return (bytes);
           }
             
           if (fgets (satz, len, datei))
           {
			               if (trenner) 
						   {
							   filltrec (satz, len);
						   }
                           return (1);
           }
           return (0);
}

int hilfe ()
{
        printf ("adc_bws -abcdfgqktTrenner Dateiname Infodatei\n");
        printf ("  -a   Anzeigemodus\n");
        printf ("  -b   Binaermodus\n");
        printf ("  -c   Anzeige der Programmzeilen\n");
        printf ("  -d   Breakpoint zulassen\n");
        printf ("  -f   Feldinhalte von Datenbankbereich anzeigen\n");
        printf ("  -g   Satzaufbau anzeigen\n");
        printf ("  -i   Kennzeichen, ob die Saetze nut upgedatet werden\n");
        printf ("       i = 0 Nur Update wird durchgefuehrt\n");
        printf ("       i = 1 Update und Insert wird durchgefuehrt (Default)\n");
        printf ("  -mx  Set SQL-Fehler-Modus auf x . x = (0 - 2)\n");
        printf ("  -q   Qiuck-Modus. Es wird nur einmal in der Datenbank "
                        "gelesen\n");
        printf ("  -l   Lesemodus fuer die Datenbank\n");
        printf ("       0  Lesen vor Belegen der Datenbankfelder\n");
        printf ("       1  Lesen nur vor update\n");
        printf ("  -t   die Felder sind durch Trenner getrennt\n");
        printf ("  -k   C-Felder stehen in Anfuehrungszeichen\n");
        exit (0);
}
        
