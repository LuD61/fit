#ifndef SINIX
#ident  "@(#) mo_bwsasc.c	2.002a	17.03.97	by RO"
#endif
/***
--------------------------------------------------------------------------------
-
-       Modulname           :       mo_bwsasc.c
-
        Exports                 :
         
        Imports                 :

-       Interne Prozeduren      :  
-
-       Externe Prozeduren      :    
                                        
-
-       Autor                   :       WR    
-       Erstellungsdatum        :       18.01.93
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ESQL-C
-
-       Aenderungsjournal       :
-
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   BWS-Tabelle in Ascii-Datei uebertragen.
-
--------------------------------------------------------------------------------
***/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#ifdef ODBC
#include "dbclass.h"
#else
#include "mo_curso.h"
#endif
#ifndef WIN32
#include <fcntl.h>
#endif

#define KONVERT "KONVERT"

#define CHAR    1
#define SHORT   2
#define LONG    3
#define STRING  4
#define DOUBLE  5
#define HEX     6

#define COLNSIZE 128

extern char *zwort[];
extern char *wort[];
extern char *clipped ();
extern double ratod ();
extern char * upstrstr ();
extern char * make_name ();
extern char *getenv ();

extern short DEBUG;
extern short FDEBUG;
extern char sql[];
extern short sql_mode;

/* Informationen aus SW-Kopf                                      */
extern short blklen;            /* SW-Blocklaenge                        */
extern short blkanz;            /* Anzahl SW-Bloecke                     */
extern short banz;              /* Anzahl SW-Felder pro Satz             */
extern short klen;              /* Anzahl SW-Felder pro Satz             */
extern short ziel_len;

extern short swq;                  /* Quelle ist eine SW-Datei              */ 
extern short swz;                  /* Ziel ist eine SW-Datei                */ 
short sw_rec = 0;
char *tmpinfdat = NULL;

/* Typ-Definitionen                               */

/* Struktur fuer Feldbeschreibung                 */

typedef struct
       {   char  feldname [48];
           short feldlen;
           char  feldtyp [2]; 
           short nachkomma;
           short dimension;
       } FELDER;

typedef struct
       {   FELDER **felder;
           char   **buffer;
       } RECORD;

union FADR
      { char  *fchar;
        short *fshort;
        int   *fint;
        long  *flong;
        void  *fvoid;
        double *fdouble;
      };

extern char *argsp [];      /* Speicher fuer Argumente                       */

extern short constat;
extern FELDER *quelle;      /* Tabelle mit Satzaufbau fuer Eingabefelder     */
extern short feld_anz;      /* Anzahl Felder im Satz                         */
extern RECORD insatz;

extern FELDER *ziel;        /* Tabelle mit Tabellenaufbau aus der Datenbank  */
extern short db_anz;        /* Anzahl Felder im Satz                         */
extern RECORD outsatz;
extern char chrinit;        /* Zeichen zum Initiailieren von Strings         */

extern FELDER *data;        /* Tabelle mit Variablen                         */
extern short data_anz = 0;  /* Anzahl Variablen                              */
extern RECORD datasatz;

static char dbtab [41];     /* Name der Datenbanktabelle                     */
static char *ascii_name;    /* Name der Ascii-Datei                          */
static char *info_name;     /* Name der Info-Datei                           */

extern char *eingabesatz;   /* Buffer fuer Ascii-Eingabesatz                 */
extern short qlen;          /* Laenge eingabesatz                            */
extern char *ausgabesatz;   /* Buffer fuer Binaer-Ausgabesatz                */
static char *zwsatz = (char *) 0;
                            /* Zwischenbuffer fuer Ausgabe mit Trennzeichen  */
extern char *wertesatz;     /* Buffer mit Initialisierungswerten.            */
extern short zlen;          /* Laenge ausgabesatz                            */
extern char *daten;         /* Buffer fuer allgemeien Variablen              */
extern short dlen;          /* Anzahl allgemeien Daten                       */
extern char CR [];

extern char where_part [1024];     
                            /* Bufer fuer Where-Teil                         */

static int sel_db = -1;     /* Cursor                                        */
static int ins_db = -1;
static int upd_db = -1;
static char *and_part = 0; /* Selektionskriterium fuer Tabelle               */
                           /* Wird als Argument mit -s uebergeben            */
                           /* z.B. -s"where ls = 1"                          */

static char *ename = 0;     /* Datei mit Bedingung fuer einzelne Saetze     */
static short rd_error = 0;  /* Kennzeichen fuer ename lesen.                */

static short anzeigen = 0;  /* Flag fuer Anzeigemodus                       */
static short sqldebug = 0;  /* Flag fuer SQL-Debugmode                      */
static short feldinh  = 0;  /* Feldinhalte Ziel                             */
static short test = 0;      /* Testmodus                                    */
static short binaer;        /* Dateimodus                                   */
extern short show_code;     /* Code anzeigen                                */
extern int arganz;          /* Anzahl Argumente                             */

static int to_asc ();       /* Ausgabesatz schreiben                        */ 
static int write_var ();    /* Variable in Ausgabefile                      */ 

char trenner = 0;      /* Trennzeichen zwischen Felder                 */
int hochkomma = 0;     /* Flag fuer Hochkomma bei Typ C                */
static int mit_clipp = 0;     /* Flag fuer clipped bei Typ C                  */
//static char omodus[5] = {"a+b"};
static char omodus[5] = {"a"};
static FILE *inf;
extern int dbcursor;

char dbase [256] = {"bws"};

int setomodus (char *modus)
/**
Modus zum Ausgabefile oeffnen setzen.
**/
{
	      if (strlen (modus) > 5) return (-1);
	      strcpy (omodus, modus);
		  if (binaer && strchr (omodus, 'b') == 0)
          {
			      strcat (omodus, "+b");
          }

/*
		  if (strchr (omodus, 'b') == 0)
          {
			      strcat (omodus, "+b");
          }
*/
		  return (0);
}
int settrenner (tr)
char tr;
{
          if (tr == 'p') 
		  {
			  tr = '|';  
		  }
          else if (tr == 't') 
		  {
			  tr = 9;
		  }
          trenner = tr;
}

int sethochkomma ()
{
          hochkomma = 1;
}

int setclipped (modus)
int modus;
{
          if (modus != 2) modus = 1;
          mit_clipp = modus;
}

int setand_part (name)
char *name;
{
           and_part = name;
}
          
int setascii_name (name)
char *name;
{
           ascii_name = name;
}
          
int  setinfo_name (name)
char *name;
{
           info_name = name;
}

int  setename (name)
char *name;
{
           ename = name;
}

int setanzeigen (wert)
short wert;
{
           anzeigen = wert;
}

int setsqldebug (wert)
short wert; 
{
           sqldebug = wert;
}

int setfeldinh (wert)
short wert;
{
           feldinh = wert;
}

int settest (wert)
short wert;
{
           test = wert;
}

int setbinaer (wert)
short wert;
{
          binaer = wert;
		  if (binaer && strchr (omodus, 'b') == 0)
          {
			      strcat (omodus, "b");
          }
}

int do_bwsasc ()
/**
bws_asc ausfuehren.
**/
{
          static short first = 1;

		  sel_db = -1;
		  ins_db = -1;
          upd_db = -1; 

#ifdef drucker
          set_drkfile (ascii_name);

#endif

#ifdef drucker
          druck_procedure ();
#endif
#ifdef WIN32
          close_all_cursor ();
#endif
		  if (strchr (omodus, 'a') != NULL)
		  {
			  if (binaer)
			  {
				  strcpy (omodus, "ab");
			  }
			  else
			  {
				  strcpy (omodus, "a");
			  }
		  }
          if (first)
          {
                    new_procedure ();
                    first = 0;
          }
          belege_ziel (info_name);
          belege_db (info_name);
          if (anzeigen) 
          {
                         printf ("Satzlaenge Ascii %d\n", satzlen (ziel));
                         printf ("Satzlaenge DB    %d\n", satzlen (quelle));
          }
          ascii_to_db (ascii_name, and_part);
          if (tmpinfdat != NULL)
          {
                  unlink (tmpinfdat);
                  tmpinfdat = NULL;
          }  
          close_all_cursor ();
}

int ascii_to_db (ascii_name, and_part)
/**
Ascii-Datei in Datenbanktabelle uebertragen.
**/
char *ascii_name;
char *and_part;
{

          FILE *ausgabefile;
          extern short do_exit;

          rd_error = 0;
          if (swz)
          {
                   ausgabefile = fopen (ascii_name,"r+b");
          }
          else
          {
				   if (trenner != 0)
				   {
					   if (strchr (omodus, 'a') != NULL)
					   {
				           strcpy (omodus, "a");
					   }
					   else
					   {
				           strcpy (omodus, "w");
					   }
				   }
                   ausgabefile = fopen (ascii_name,omodus);
          }
          if (ausgabefile == 0)
          {
                          fprintf (stderr,
                               "Ascii-Datei %s kann nicht geoeffnet werden\n", 
                                ascii_name);
		                  closedbase (dbase);
                          exit (1);
          }
          set_wrziel (to_asc, ausgabefile);
          set_wrsziel (write_var, ausgabefile);
          dbcursor = select_cursor (and_part);

		  do_exit = 0;
          perform_begin ();
          if (do_exit == 0)
          {
             while (fetchsql (dbcursor) != 100 && rd_error == 0)
             {
                        if (do_exit == 1) break;
                        if (sqlstatus == 100) continue;
                        to_ausgabe ();
                        perform_code ();
                        showfelder (&outsatz);
                        to_asc (ausgabefile); 
             }
          }
          close_sql (dbcursor);
          perform_ende ();
          if (swz) writesw_anz (ausgabefile);
          fclose (ausgabefile);
}

int fetchsql (dbcursor)
/**
Aus Datenbank lesen.
**/
short dbcursor;
{
	      static int first = 1;

          if (ename == 0)
          {
                    return (fetch_sql (dbcursor));
          }

		  if (! first) 
          {
			         fetch_sql (dbcursor);
					 if (sqlstatus == 0) return (0);
          }
		  else
          {
			         first = 0;
          }
          ewerte ();
          if (rd_error != 0) return (100);
          open_sql (dbcursor);
          fetch_sql (dbcursor);
          return (0);
}

int ewerte ()
/**
Werte fuer Cursor aus Datei ename holen.
**/
{
          static FILE *ew = 0;

          static char wnamen [10] [20];
          static short felder = 0;
          char satz [256];
          short anz;
          short i;

          if (ew == 0)
          {
                           ew = fopen (ename, "r");
                           if (ew == 0)
                           {
                              fprintf (stderr, 
                                      "%s kann nicht geoeffnet werden\n",
                                       ename);
		                      closedbase (dbase);
                              exit (1);
                           }
                           if (fgets (satz, 255, ew) == 0)
                           {
                                         rd_error = -1;
                                         return (-1);
                           }
                           cr_weg (satz);
                           anz = split (satz);
                           if (anz < 1)
                           {
                                         rd_error = -1;
                                         return (-1);
                           }
                           if (anz > 10) anz = 10;
                           felder = anz;
                           for (i = 1; i <= anz; i ++)
                           {
                                   strncpy (wnamen [i - 1], wort [i], 19);
                                   wnamen [i - 1] [19] = 0;
                           }
         }


         if (fgets (satz, 255, ew) == 0)
         {
                       rd_error = -1;
                       return (-1);
         }
         cr_weg (satz);
         anz = split (satz);
         if (anz < 1)
         {
                       rd_error = -1;
                       return (-1);
         }

         if (anz > felder) anz = felder;

         for (i = 1; i <= anz; i ++)
         {
                       set_rec_wert (&insatz, wnamen [i - 1], wort [i], 0);
         }
         return (0);
}
                           

int to_ausgabe ()
/**
Eingabesatz in Ausgabesatz konvertieren.
**/
{
          short i;
          short zpos;

          for (i = 0; ziel[i].feldname[0]; i ++)  
          {
                   znullvalue (ziel[i].feldname);
                   uebertrage (&outsatz, ziel [i].feldname,
                               &insatz, ziel [i].feldname);
          }
          return (0);
}

in_to_out (quelle, ziel, quellfeld, zielfeld)
/**
Quellfeld in Zielfeld uebertragen.
**/
FELDER *quelle;
FELDER *ziel;
FELDER *quellfeld;
FELDER *zielfeld;
{
          char zielwert [265];
          char format [10];
          short spos;
          short len;
          char *zpos;
          union FADR qfeld;

/* Positionen im Buffer holen                        */

          spos = feldpos (quelle,quellfeld->feldname);
          qfeld.fchar = &eingabesatz [spos];
          spos = feldpos (ziel,zielfeld->feldname);
          zpos = &ausgabesatz [spos];

          switch (quellfeld->feldtyp[0])
          {
                         case 'C' :
                         case 'A' :
                                  len = quellfeld->feldlen;
                                  if (len > zielfeld->feldlen)
                                  {
                                           len = zielfeld->feldlen;
                                  }
                                  memcpy (zielwert,qfeld.fchar,len);
                                  zielwert [len] = 0;
                                  break;
                         case 'd' :
                                  memcpy (zielwert,qfeld.fchar,
                                          quellfeld->feldlen);
                                  zielwert [quellfeld->feldlen] = 0;
                                  date_to_str (zielwert, zielfeld);  
                                  break;
                         case  'S' :
                                  sformat (format, zielfeld);
                                  sprintf (zielwert,format,*qfeld.fshort);
                                  break;
                         case  'I' :
                                  lformat (format, zielfeld);
                                  sprintf (zielwert,format,*qfeld.flong);
                                  break;
                         case  'D' :
                                  dformat (format, zielfeld);
                                  nachkomma (qfeld.fdouble, 
                                            quellfeld->nachkomma,
                                            zielfeld->nachkomma);
                                  sprintf (zielwert,format,*qfeld.fdouble);
                                  break;
                         case 'G' :
                        		 ToGerDate ((DATE_STRUCT *) qfeld.fchar, zielwert);
                                 break;
            }
            memcpy (zpos, zielwert, strlen (zielwert));
}

int nachkomma (dwert, nk1, nk2)
/**
Nachkommastellen auswerten.
**/
double *dwert;
short nk1, nk2;
{
           short i;
           short diff;
           double faktor;
           double rwert;
           long lwert;
           rwert = *dwert;
           if (double_null (rwert)) rwert = 0.0;
           faktor = 10.0;
           for (i = 0; i < nk1; i ++)
           {
                       rwert *= faktor;
           }
           *dwert = rwert;
           diff = nk1 - nk2;

           if (diff == 0) return;
           if (diff < 0)
           {
                      diff = 0 - diff;
           }
           else
           {
                      faktor = 0.1;
           }
           for (i = 0; i < diff; i ++)
           {
                       rwert *= faktor;
           }
           lwert = rwert;
           rwert = lwert;
           *dwert = rwert;
}

int date_to_str (dstring, feld)
/**
String in Datumformat konvertieren.
**/
char *dstring;
FELDER *feld;
{
          char datum [11];

          memcpy (datum,dstring,2);
          memcpy (&datum [2],&dstring[3],2);
          strcpy (&datum[4],&dstring[6]);
          if (strlen (datum) > feld->feldlen) strcpy (&datum [4],&datum [6]);
          strcpy (dstring,datum);
}
          
int to_asc (ausgabefile)
/**
Aausgabesatz in schreiben.
**/
FILE *ausgabefile;
{

          if (constat) return (0);
          if (swz)
          {
                      writesw (ausgabesatz, satzlen(ziel), ausgabefile);
                      init_ausgabesatz ();
                      return (0);
          }
          if (testformat (ausgabesatz, satzlen (ziel), ausgabefile)) 
          {
                       return (0);
          }
          else if (ziel_len > 0)
          {
                   fwrite (ausgabesatz, 1, ziel_len, ausgabefile);
          }
          else
          {
                   fwrite (ausgabesatz, 1, satzlen (ziel), ausgabefile);
          }
//          if (binaer == 0) fwrite (CR,1, strlen (CR), ausgabefile);
          if (binaer == 0) fprintf (ausgabefile, "\n");
          /* memcpy (ausgabesatz, wertesatz, zlen + 2);  */
}

int memclipp (zpos, qpos, ziellen)
/**
qpos in zpos in der Laenge len kopieren.
Blanks am Ende von zpos entfernen.
**/
char *zpos;
char *qpos;
int ziellen;
{
         
         memcpy (zpos, qpos, ziellen);
         zpos [ziellen] = (char) 0;
         clipped (zpos);
         if (strcmp (zpos, " ") == 0) 
         {
                    *zpos = (char) 0;
                    return (0);
         }
         return (strlen (zpos));
}

int testblank (string, len)
/**
Testen, ob Character <> ' ' vorhanden sind.
**/
char *string;
int len;
{
         int i;

         if (trenner == (char) 0) return (1);
         if (mit_clipp < 2) return (1);
         for (i = 0; i < len; i ++)
         {
                   if (string[i] > ' ') return (1);
         }
         return (0);
}

int testformat (satz, len, ausgabefile)
/**
Ausgabefoemat bestimmen und Satz formatieren.
**/
char *satz;
int len;
FILE *ausgabefile;
{
         int i;
         int flen;
         char *qpos;
         char *zpos;
         int setkomma;

         if (hochkomma == 0 && trenner == (char) 0) 
         {
                         return (0);
         }

         if (mit_clipp) len = 0;
         if (zwsatz == 0)
         {
                         zwsatz = malloc (zlen + (feld_anz * 4) + 2);
         }
         if (zwsatz == 0)
         {
                         fprintf (stderr,"Kein Speicherplatz\n");
                         closedbase (dbase);
                         exit (1);
         }
         memset (zwsatz, (char) 0, sizeof (zwsatz));
         qpos = satz;
         zpos = zwsatz;
         for (i = 0; ziel[i].feldname[0]; i ++)
         {
                   switch (ziel[i].feldtyp[0])
                   {
                         case 'C' :
                         case 'd' :
                                  setkomma = 0;
                                  if (hochkomma && testblank (qpos,
                                                       ziel[i].feldlen)) 
                                  {
                                              *zpos = '\"';	
                                              zpos += 1;
                                              len ++;
                                              setkomma = 1;
                                  }
                                  if (mit_clipp)
                                  {
                                         flen = memclipp (zpos, qpos, 
                                                          ziel[i].feldlen);
                                         zpos += flen;
                                         len += flen;
                                  }
                                  else
                                  {
                                          memcpy (zpos, qpos, ziel[i].feldlen);
                                          zpos += ziel[i].feldlen;
                                  }
                                  qpos += ziel[i].feldlen;
                                  if (hochkomma && setkomma) 
                                  {
                                              *zpos = '\"';	
                                              zpos += 1;
                                              len ++;
                                  }
                                  break;
                         case  'A' :
                         case  'N' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                         case  'S' :
                         case  'I' :
                         case  'D' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                         case  'G' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                         case  'J' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                         case  'K' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                 }
                 if (trenner != (char) 0 && ziel[i + 1].feldname [0])
                 {
                                  *zpos = trenner;
                                  zpos += 1;
                                  len ++;
                 }
      }
      fwrite (zwsatz, 1, len, ausgabefile);
//      if (binaer == 0) fwrite (CR,1, strlen (CR), ausgabefile);
      if (binaer == 0) fprintf (ausgabefile, "\n");
      return (1);
}


int write_var (satz, len, ausgabefile)
/**
Aausgabesatz in schreiben.
**/
char *satz;
short len;
FILE *ausgabefile;
{
           fwrite (satz, 1, len, ausgabefile);
           fwrite (CR,1, strlen (CR), ausgabefile);
}

int select_cursor (and_part)
/**
Cursor zum Lesen.
**/
char *and_part;
{
          short i;
          short typ;
          short fpos;
          short spos;
          void *fvar;
          short wanz;
          extern char *wort [];

          sprintf (sql,"select ");
          for (i = 0; quelle [i].feldname [0]; i ++)
          {
                      typ = getdbtyp (&quelle [i]);
                      fpos = feldpos (quelle, quelle [i].feldname);
                      fvar = &eingabesatz [fpos];
                      if (typ == 0 || typ == 7)
                      {
                                out_quest (fvar,typ, quelle[i].feldlen);
                      }
                      else
                      {
                                out_quest (fvar,typ, 0);
                      }
                      strcat (sql,quelle[i].feldname);
                      if (quelle [i + 1].feldname [0]) strcat (sql, ",");
           }

           wanz = split (where_part);
           for (i = 2; i <= wanz; i ++)
           {
                     spos = instruct (quelle, clipped (wort [i]));
                     if (spos != -1)
                     {
                               if (test_q (wanz, i) == -1) continue;
                               typ = getdbtyp (&quelle [spos]);
                               fpos = feldpos (quelle, wort [i]);
                               fvar = &eingabesatz [fpos];
                               if (typ == 0 || typ == 7)
                               {
                                     ins_quest (fvar,typ, quelle[spos].feldlen);
                                     if (test) 
                                     printf 
                                         ("ins_quest (%s (%ld), %hd, %hd)\n",
                                             quelle[spos].feldname,
                                             fvar,
                                             typ,
                                             quelle[spos].feldlen);
                               }
                               else
                               {
                                      ins_quest (fvar,typ, 0);
                                      if (test) 
                                      printf 
                                         ("ins_quest (%s (%ld), %hd, %hd)\n",
                                             quelle[spos].feldname,
                                             fvar,
                                             typ,
                                             0);
                               }
                     }
           }

           strcat (sql," from ");
           strcat (sql,dbtab);

           strcat (sql," ") ;
           if (where_part [0]) strcat (sql,where_part);
           if (and_part) 
           {
                      strcat (sql,and_part);
           }
           strcat (sql," ") ;
           if (sqldebug)  printf ("%s\n", sql);
           return (prepare_sql (sql));
}

int test_q (wanz, pos)
/**
Test, ob das uebernaechste Zeichen ein ? ist.
**/
short wanz;
short pos;
{
           pos += 2;
           if (pos > wanz) return (-1);
           if (wort [pos] [0] == '?') return (0);
           return (-1);
}



/***********

Include-Datei einlesen   

*/


int includefile (incl, satz)
/**
Includedatei einlesen.
**/
FILE *incl;
char *satz;
{
          int anz;
          FILE *incfile;
          char iname [256];
          char *konv;

          cr_weg (satz);
          anz = split (satz);
          if (anz < 2) return (-1);

          konv = getenv ("KONVERT");
          if (konv)
          {
#ifdef WIN32
                 sprintf (iname, "%s\\%s", konv, wort[2]);
#else
                 sprintf (iname, "%s/%s", konv, wort[2]);
#endif
          }
          else
          {
                 strcpy (iname, wort[2]);
          }

          incfile = fopen (iname, "r");
          if (incfile == (FILE *) 0)
          {
                 fprintf (stderr, "Includedatei %s nichht gefunden\n", iname);
		         closedbase (dbase);
                 exit (1);
          }

          while (fgets (satz, 511, incfile))
          {
                  fputs (satz, incl);
          }
          fclose (incfile);
          return (0);
 }

int leseinclude (satz)
/**
Includes lesen.
**/
char *satz;
{
          FILE *incl;
          char *tmp;
          static char idat[512];

          tmp = getenv ("TMPPATH");
          if (tmp != NULL) 
          {
                   sprintf (idat, "%s\\infdat.tmp", tmp);
          }
          else
          {
                   sprintf (idat, "infdat.tmp");
          }
          incl = fopen (idat, "w");
          if (incl == (FILE *) 0)
          {
                     fprintf (stderr, "Fehler beim Oeffnen von temp. Datei\n");
		             closedbase (dbase);
                     exit (1);
          }

          tmpinfdat = idat;  
          while (fgets (satz, 511, inf))
          {
                    if (memcmp (satz, "$INCLUDE", 8) == 0)
                    {
                               includefile (incl, satz);
                    }
                    else
                    {
                               fputs (satz, incl);
                    }
          }
          fclose (incl);
          fclose (inf);
          inf = fopen (idat, "r");
          if (inf == (FILE *) 0)
          {
                     fprintf (stderr, "Fehler beim Oeffnenn %s",
                              idat);
		             closedbase (dbase);
                     exit (1);
          }
          return (0);
}

int testinclude ()
/**
Includes suchen.
**/
{
          char satz [516];

          while (fgets (satz, 511, inf))
          {
                    if (memcmp (satz, "$INCLUDE", 8) == 0)
                    {
                               fseek (inf, 0l, 0);
                               leseinclude (satz);
							   break;
                    }
          }
          fseek (inf, 0l, 0);
          return (0);
}

/***********

Ende Include-Datei einlesen   

*/


int belege_ziel (info_name)
/**
Satzaufbau fuer Quelldatei holen.
**/
char *info_name;
{
          char satz [256];
          short i;
          long feldseek;

          inf = fopen (make_name (KONVERT, info_name), "r");
          if (inf == 0) inf = fopen (info_name,"r");
          if (inf == 0)
          {
                        fprintf (stderr,"%s kann nicht geoeffnet werden\n",
                                         info_name);
		                closedbase (dbase);
                        exit (1);
          }

          testinclude (); 

		  database (inf, satz);
		  opendbase (dbase);
 	      setddbase ();
          tabname (inf, satz);   /* Tabellenname holen                       */
          feldseek = ftell (inf);
          if (test_swz (inf, satz))
          {
                        mit_swz ();

                        init_ausgabesatz ();
                        init_sw_start ();
                        belege_where_part (inf, satz); 
                        belege_code (inf, satz);
                        belege_data (inf, satz);
                        belege_structs (inf, satz);
                        belege_redefines (inf, satz);
                        return (0);
          }      
          fseek (inf,feldseek,0);
          feldanz (inf, satz);   /* Anzahl Felder feststellen                */
          if (feld_anz == 0) 
          {
                         belege_where_part (inf, satz); 
                         belege_code (inf, satz);
                         belege_data (inf, satz);
                         belege_structs (inf, satz);
                         belege_redefines (inf, satz);
                         return (0);
          }
		  if (ziel) free (ziel);
          ziel = malloc ((feld_anz + 2) * sizeof (FELDER));
          if (ziel == 0)
          {
                        fprintf (stderr,"Kein Speicherplatz\n");
		                closedbase (dbase);
                        exit (1);
          }
          fseek (inf,feldseek,0);
          ins_ziel (inf, satz);
          zlen = satzlen (ziel) + 2;
		  if (ausgabesatz) free (ausgabesatz);
          ausgabesatz = malloc (zlen + 2);
          if (ausgabesatz == 0)
          {
                        fprintf (stderr,"Kein Speicherplatz\n");
		                closedbase (dbase);
                        exit (1);
          }
          if (trenner || hochkomma)
          {
			            if (zwsatz) free (zwsatz);
                        zwsatz = malloc (zlen + (feld_anz * 4) + 2);
                        if (zwsatz == 0)
                        {
                                      fprintf (stderr,"Kein Speicherplatz\n");
		                              closedbase (dbase);
                                      exit (1);
                        }
          }
		  if (wertesatz) free (wertesatz);
          wertesatz = malloc (zlen + 2);
          if (wertesatz == 0)
          {
                        fprintf (stderr,"Kein Speicherplatz\n");
		                closedbase (dbase);
                        exit (1);
          }
          
          init_ausgabesatz ();
          fseek (inf,feldseek,0);
          belege_value (satz,inf,&outsatz);
          memcpy (wertesatz, ausgabesatz, zlen + 2);
          belege_where_part (inf, satz); 
          belege_code (inf, satz);
          belege_data (inf, satz);
          belege_structs (inf, satz);
          belege_redefines (inf, satz);
}

int test_swz (inf, satz)
/**
Tabellenname holen.
**/
FILE *inf;
char *satz;
{

          while (fgets (satz,80,inf))
          {
                     if (strupcmp (satz,"$SWZ",4) == 0)
                     { 
                                return (1);
                     }
          }
          return (0);
}

int belege_where_part (inf, satz)
/**
Nach Suchkriterium im Info_file suchen.
**/
FILE *inf;
char *satz;
{
          short pos;

          fseek (inf, 0l, 0);
          memset (where_part,1024,0);
          fseek (inf,0l,0);
          while (fgets (satz,1023,inf))
          {
                     if (strupcmp (satz,"$SELECT",7) == 0)
                     { 
                                   cr_weg (satz);
                                   pos = first_ze (satz, 7);
                                   if (pos == 0) return;
                                   strcpy (where_part, &satz[pos]);
                     }
          }
}

int database (inf, satz)
/**
Tabellenname holen.
**/
FILE *inf;
char *satz;
{


          while (fgets (satz,255,inf))
          {
                     if (strupcmp (satz,"$DBASE",6) == 0)
                     { 
                                fgets (satz, 255, inf);
                                cr_weg (satz); 
                                strcpy (dbase, satz);
                                clipped (dbase);
								fseek (inf, 0l, 0);
                                return (0);
                     }
          }
          fseek (inf, 0l, 0);
		  return (0);
}


int tabname (inf, satz)
/**
Tabellenname holen.
**/
FILE *inf;
char *satz;
{


          while (fgets (satz,255,inf))
          {
                     if (strupcmp (satz,"$TABLE",6) == 0)
                     { 
                                fgets (satz, 255, inf);
                                cr_weg (satz); 
                                strncpy (dbtab, satz,40);
                                dbtab [40] = 0;
                                return (0);
                     }
          }
          fprintf (stderr,"Tabellenname nicht gefunden\n");
		  closedbase (dbase);
          exit (1);
}

int ins_ziel (inf, satz)
/**
Felder fuer Satzaufbau fuellen.
**/
FILE *inf;
char *satz;
{
         short i;
         short (anz);

         i = 0;
         while (fgets(satz,255,inf))
         {
                  cr_weg (satz);
                  if (strupcmp (satz,"$SELECT",7) == 0) break;
                  if (strupcmp (satz,"$CODE",7) == 0) break;
                  if (strupcmp (satz,"$DATA",7) == 0) break;
                  anz = zsplit (satz,','); 
                  if (anz < 5) continue;
                  strncpy (ziel [i].feldname,clipped (zwort [1]),COLNSIZE - 1);
                  ziel [i].feldname [COLNSIZE - 1] = 0;
                  ziel [i].feldlen = atoi (zwort [2]);
                  strncpy (ziel [i].feldtyp,clipped (zwort [3]),1);
                  ziel [i].feldtyp [1] = 0;
                  ziel [i].nachkomma = atoi (zwort [4]);
                  ziel [i].dimension = atoi (zwort [5]);
                  i ++;
                  if (i == feld_anz) break; 
      }
      ziel [i].feldname [0] = 0;
      ziel [i].feldlen = 0;
      strcpy (ziel [i].feldtyp,"C");
      ziel [i].nachkomma = 0;
      ziel [i].dimension = 0;
      show_aufbau (ziel);
}

int mit_swz ()
/**
Satzaufbau Ziel aud SW-Kopf holen.
**/ 
{

      lese_sw (ascii_name);
	  if (ziel) free (ziel);
      ziel = malloc ((banz + 2) * sizeof (FELDER));
      if (ziel == 0)
      {
                        fprintf (stderr,"Kein Speicherplatz\n");
		                closedbase (dbase);
                        exit (1);
      }
      sw_info (ziel);
      zlen = satzlen (ziel);
      feld_anz = banz;
	  if (ausgabesatz) free (ausgabesatz);
      ausgabesatz = malloc (zlen + 2);
      if (ausgabesatz == 0)
      {
                        fprintf (stderr,"Kein Speicherplatz\n");
		                closedbase (dbase);
                        exit (1);
      }
      swz = 1;
      show_aufbau (ziel);
      if (sw_rec)
	  {
		  closedbase (dbase);
		  exit (0);
	  }
}

int belege_db ()
/**
Satzaufbau fuer Datenbanktabelle belegen.
**/
{
      int tabid;
      short felder;
      int colcurs;
      char colname [COLNSIZE];
      short coltype;
      short collength;
      short i;
      int cursor;
      char sqltext [256];
	  long precision;
      short scale; 
      int tabcurs;

      // prepare_sql ("select tabid from systables");

#ifdef ODBC
	  sprintf (sqltext, "select * from %s", clipped (dbtab));
	  tabcurs = prepare_sql (sqltext);
      db_anz = get_colanz(tabcurs); 
#else
      sprintf ( sqltext, "select tabid, ncols from systables where tabname = \"%s\"",
                    dbtab);
      execute_sql (sqltext);
      tabid  = isqlvalue (0);
      db_anz = ssqlvalue (1);
#endif
	  if (quelle) free (quelle);
      quelle = malloc ((db_anz + 2) * sizeof (FELDER));
      if (quelle == 0)
      {
                        fprintf (stderr,"Kein Speicherplatz\n");
		                closedbase (dbase);
                        exit (1);
      }

      i = 0;
#ifdef ODBC
          colcurs = prepare_columns (clipped (dbtab), NULL);
	  bind_sqlcol (colcurs, 4, (char *) colname,   SQLCCHAR, COLNSIZE);
	  bind_sqlcol (colcurs, 5, (short *) &coltype, SQLCSHORT, 0);
	  bind_sqlcol (colcurs, 7, (long *) &precision, SQLCLONG, 0);
	  bind_sqlcol (colcurs, 8, (short *) &collength, SQLCSHORT, 0);
	  bind_sqlcol (colcurs, 9, (short *) &scale, SQLCSHORT, 0);
#else
      out_quest (colname,0,COLNSIZE - 1);
      out_quest (&coltype,1,0);
      out_quest (&collength,1,0);
      ins_quest (&tabid,2,0);
      colcurs = prepare_sql ("select colname, coltype,collength from "
                             "syscolumns where tabid = ?");
#endif
//#ifndef ODBC
	  coltype = 0;
      fetch_sql (colcurs);
      while (sqlstatus == 0)
      {
#ifdef ODBC
/* 1. Versuch die Feldinformationen zu holen. Funktioniert nicht mit jedem Treiber.
      OK bei mysql, Informix. Nicht OK bei Oracle (Linux).
*/	  
			     if (coltype == SQLDECIMAL)
				 {
					 collength = (precision << 8) | scale;
				 }
			     else if (coltype == SQLCHAR)
				 {
//					 collength ++;
				 }
#endif
                 belege_quelle (&quelle [i],colname, coltype, collength);
                 i ++;
                 if (i == db_anz) break;
				 coltype = 0;
                 fetch_sql (colcurs);
      }
//#endif
#ifdef ODBC
/* 2. Versuch die Feldinformationen zu holen. Funktioniert nicht mit jedem Treiber.
      OK bei Oracle, Informix. Nicht OK bei mysql (Windows).
*/	  
      if (i < db_anz)
      {
                 for (i = 0; i < db_anz; i ++)
                 {
                          get_colname (i + 1, tabcurs, colname);    
                          coltype = get_coltype (i + 1, tabcurs);
                          if (coltype < 0 || coltype > 100)
                          {
                                 coltype = SQLSMINT;
                                 collength = 2;
                          } 
                          collength = get_collength (i + 1, tabcurs); 
                          belege_quelle (&quelle [i],colname, coltype, collength);
                  }
                  db_anz = i;  
      }
      close_sql (tabcurs);
#endif
      quelle [i].feldname [0] = 0;
      quelle [i].feldlen = 0;
      strcpy (quelle [i].feldtyp,"C");
      quelle [i].nachkomma = 0;
      quelle [i].dimension = 0;
      close_sql (colcurs);
      show_aufbau (quelle); 
      qlen = satzlen (quelle);
	  if (eingabesatz) free (eingabesatz);
      eingabesatz = malloc (qlen + 2);
      if (eingabesatz == 0)
      {
                    fprintf (stderr,"Kein Speicherplatz\n");
		            closedbase (dbase);
                    exit (1);
      }
      if (feld_anz == 0)
      {
                    feld_anz = db_anz;
                    ascii_typ (&ziel, quelle, feld_anz);
                    show_aufbau (ziel); 
                    zlen = satzlen (ziel) + 2;
					if (ausgabesatz) free (ausgabesatz);
                    ausgabesatz = malloc (zlen + 2);
                    if (ausgabesatz == 0)
                    {
                                  fprintf (stderr,"Kein Speicherplatz\n");
		                          closedbase (dbase);
                                  exit (1);
                    }
      }
}

int belege_quelle (quelle, colname, coltype, collength)
/**
Satzstruktur fuer ein Datenbankfeld belegen.
**/
FELDER *quelle;
char *colname;
short coltype;
short collength;
{

      strcpy (quelle->feldname, clipped (colname));
	  coltype &= 0xFF;
      switch (coltype)
      { 
                case SQLCHAR :
                          quelle->feldlen = collength + 1;
                          strcpy (quelle->feldtyp,"C");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                case SQLVCHAR :
                          quelle->feldlen = collength + 1;
                          strcpy (quelle->feldtyp,"C");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                case SQLSMINT :
                          quelle->feldlen = sizeof (short);
                          strcpy (quelle->feldtyp,"S");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                case SQLINT :
#ifndef ODBC
                case SQLSERIAL :
#endif
                          quelle->feldlen = sizeof (long);
                          strcpy (quelle->feldtyp,"I");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                case SQLDECIMAL :
#ifndef ODBC
                case SQLMONEY :
#endif
// !!! Achtung : wegen oracle auf Linux wird hier mit Character stat Double gelesen.
                           quelle->feldlen = sizeof (double);
                          strcpy (quelle->feldtyp,"D");
                          quelle->nachkomma = collength;
//                           quelle->feldlen = 20;
//                           strcpy (quelle->feldtyp,"C");
                          quelle->dimension = 1;
                          break;
                case SQLFLOAT :
                          quelle->feldlen = sizeof (double);
                          strcpy (quelle->feldtyp,"D");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
/*
                case SQLDATE :
					      quelle->feldlen = sizeof (DATE_STRUCT);
                          strcpy (quelle->feldtyp,"G");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
*/
                case SQLTIME :
					      quelle->feldlen = sizeof (TIME_STRUCT);
                          strcpy (quelle->feldtyp,"J");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                case SQLDATE :
                case SQLTIMESTAMP :
					      quelle->feldlen = sizeof (TIMESTAMP_STRUCT);
                          strcpy (quelle->feldtyp,"K");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                default :
                          quelle->feldlen = collength + 1;
                          strcpy (quelle->feldtyp,"C");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
           }
}

int showfelder (satz)
/**
Feldinhalte eines Satzes ansehen.
**/
RECORD *satz;
{
           short fpos;
           FELDER *felder;
           char *buffer;
           short i;

           if (feldinh == 0) return;

           felder = *satz->felder;
           buffer = *satz->buffer;
           for (i = 0; felder [i].feldname [0]; i ++)
           {
                           fpos = feldpos (felder, felder[i].feldname);
                           showfeld (&felder [i], buffer, fpos);
           }
           printf ("\n");
}

int showfeld (feld, buffer, fpos)
/**
Feldinhalt anzeigen.
**/
FELDER *feld;
char *buffer;
short fpos;
{

           union FADR fadr;
		   DATE_STRUCT * Date;
		   TIME_STRUCT * Time;
		   TIMESTAMP_STRUCT * Timestamp;
           
           fadr.fchar = &buffer [fpos];
           printf ("%-20s  ", feld->feldname);
           switch (feld->feldtyp [0])
           {
                           case 'C' :
                           case 'A' :
                           case 'N' :
                           case 'd' :
                                     printlen (fadr.fchar, feld->feldlen);
                                     break;
                           case 'G' :
							         Date = (DATE_STRUCT *) fadr.fchar;
							         printf ("%02hd.%02hd.%02hd\n", Date->day, Date->month,
										                            Date->year);  
                           case 'J' :
							         Time = (TIME_STRUCT *) fadr.fchar;
							         printf ("%02hd:%02hd:%02hd\n", Time->hour, Time->minute,
										                            Time->second);  
                           case 'K' :
							         Timestamp = (TIME_STRUCT *) fadr.fchar;
							         printf ("%02hd.%02hd.%02hd %02hd:%02hd:%02hd\n", 
                                                                    Timestamp->day, 
                                                                    Timestamp->month,
										                            Timestamp->year,  
                                                                    Timestamp->hour, 
                                                                    Timestamp->minute,
										                            Timestamp->second);  
                           case 'S' :
                                     printf ("%hd\n", *fadr.fshort);
                                     break;
                           case 'I' :
                                     printf ("%ld\n", *fadr.flong);
                                     break;
                           case 'D' :
                                     printf ("%lf\n", *fadr.fdouble);
                                     break;
                           default :
                                     printf ("\n");
                                     break;
            }
}

