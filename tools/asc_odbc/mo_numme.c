/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Geisental 12, 4630 Bochum 1, Tel: 0234/5065-250
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_nummer
-
-	Autor			:	W.Roth
-	Erstellungsdatum	:	10.11.92
-
-	Projekt			:	BWS
-	Version			:	3.01
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       esqlc
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:  	Prozeduren zur automatischen
-					Nummernvergabe.
-
-	Holen einer Nummer aus der Nummernverwaltung
-
-	nveinid:
-	Einstellen nicht benoetigter Nummern in die Nummernverwaltung
-	als Freinummern
-
-	nvdelid:
-	Loeschen aller Saetze einer Nummer aus der Nummernverwaltung
-
-	nvanmid:
-	Neuanmelden einer Nummer in der Nummernverwaltung
-
-	nvanmprf:
-	Anmelden einer Nummer in der Nummernverwaltung mit max. Nummerngroesse;
-	Damit ist beim Holen einer so angemeldeten Nummer eine Pruefung
-	moeglich, ob die Nummer ueberlaeuft
-
-	nvsimpel:
-	Holen einer Nummer aus der Nummernverwaltung;
-	einfache Parameterversion;
-	Wird ein Nummername fuer mdn/fil nicht gefunden, wird dieser mit
-	Standardwerten angelegt.
-	
-	nvglobal:
-	Holen einer Nummer aus der Nummernverwaltung;
-	einfache Parameterversion;
-	Wird ein Nummername fuer mdn/fil nicht gefunden, wird dieser mit
-	mit den Werten aus der PTAB mit Nummername angelegt.
-
------------------------------------------------------------------------------
*/
#include "stdio.h"
#include "string.h"
//#include <windows.h>
#include "mo_numme.h"
#ifdef ODBC
#include "dbclass.h"
#else
#include "mo_curso.h"
#endif

struct AUTO_NR auto_nr, auto_nr_null;

/*--- Lokale Definitionen Modul ---*/
struct AUTO_NR taautonum;

static char sql [256];
static short cuu_nv = 0;
static int prepcuu_nv (char *, short, short);
static int fetchcuu_nv ();
static int closecuu_nv ();
static int recread_nv (char *, short, short);
static int recupdate_nv ();
static int recdelete_nv (char *, short, int, short, short);
static int recinsert_nv ();

/*-----------------------------------*/
/*--- externe P R O C E D U R E N ---*/
/*-----------------------------------*/

/*---------------------------------------------------------------------------
-									  
-	Procedure	: nvholid
-
-	In		: dmdn		smallint	Mandantennummer
-			  dfil		smallint	Filialnummer
-			  dnr_nam	char(10)	Name der Nummer
-
-	Out		: sqlstatus/Errorcode
-
-	Errorcodes	: Anmerkung
-	---------------------------------------------------------------------
-	-250		: Nummernspeicher fuer diesen Namen durch anderen
-			  Prozess gelockt
-	-1		: Ueberlauf festgestellt; Nummer > Maximalwert
-	-2		: Unzulaessige Satzkennung in auto_nr
-	0		: ok, Nummerninformationen stehen in struct taautonum {
} taautonum, taautonum_null;

-	100		: Nummer mit diesem Namen nicht in NV
-
-----------------------------------------------------------------------------
-			  
-	Beschreibung	:	holen einer automaisch erzeugten Nummer
-				und der	dazugehoerigen Daten 
-
-	In der NV gibt es 2 Satzarten:
-	- satz_kng = 1 => Freinummern
-	- satz_kng = 2 => Nummernspeicher
-	
-	Beim Holen erfolgt der Zugriff ueber die folgende Indexsortierung:
-	nr_nam
-	satz_kng
-	nr_nr
-	mdn
-	fil
-	=> Dieser und nur dieser Index ist Bestandteil der Source !
-
-	Das bedeutet, dass falls vorhanden, eine Freinummer geholt wird.
-	Die Nummern werden also immer aufsteigend vergeben.
-
-	Freinummern koennen nur existieren, wenn es fuer die entspr. Nummer
-	(Nummernamen) einen Nummernspeicher (satz_kng = 2) gibt.
-
-	Ist keine Freinummer vorhanden, wird eine Nummer aus dem
-	Nummernspeicher vergeben.
-
-	Geholte Nummern gelten sofort als vergeben.
-	Handelt es sich um eine Freinummer, wird diese geloescht.
-	Handelt es sich um eine Nummer aus dem Nummernspeicher wird dieser
-	inkrementiert.
-
-	Stellt sich heraus, dass eine geholte Nummer doch nicht gebraucht wird,
-	kann sie in die NV als Freinummer eingestellt werden. (NVEINID)
-
--------------------------------------------------------------------------------
*/

int nvholid (short dmdn, short dfil, char *dnr_nam)
{

        short dsql1;
        short dsql2;
        short dsql3;

        short dreturn;

	dsql1 = 0;
	dsql2 = 0;
	dsql3 = 0;
	dreturn = 0;

	taautonum = auto_nr_null;
	auto_nr   = auto_nr_null;

	/*--- Update-Cursor preparieren ---*/
	        dsql1 = prepcuu_nv (dnr_nam, dmdn, dfil);

	if (dsql1 == 0)
        {

		/*--- Satz aus NV holen ---*/
		dsql2 = fetchcuu_nv ();
		switch (dsql2)
                {
			case 0 :
				/*--- Satz gefunden ---*/
				switch (auto_nr.satz_kng)
                                {
					case 1 :
						/*--- Freinummer ---*/
						dsql3 = freinummer ();
						break;
					case 2 :
						/*--- Nummernspeicher ---*/
						dsql3 =  nummernspeicher ();
						break;
					default :
						/*--- unzulaessige Satzkennung;
						      bedeutet Inkonsistenz in
						      der NV, die eigentlich
						      nicht vorkommen kann.
						---*/
						dsql3 = -2;
						break;
				} /* of switch */
				dreturn = dsql3;
				break;

			case 100 :
				/*--- kein Satz fuer angegebene Nummer in NV
				---*/
			case -250 :
				/*--- Satz ist bereits von anderem Prozess
				      gelockt
				---*/
			case -255 :
				/*--- Transaktion um NV ist nicht in Ordnung
				---*/
			default :
				/*--- wasweissich fuer schweinische SQL-Fehler
				---*/
				dreturn = dsql2;
				break;
		} /* of switch */

		closecuu_nv ();
        }
	else
        {
		/*--- SQL-Fehler bei Cursorpreparierung ---*/
		dreturn = dsql1;
	} /* if */

	return(dreturn);
}

/*---------------------------------------------------------------------------
-									  
-	Procedure	: nveinid
-
-	In		: dmdn		smallint	Mandantennummer
-			  dfil		smallint	Filialnummer
-			  dnr_nam	char(10)	Name der Nummer
-			  dnr_nr	integer		Nummer
-
-	Out		: sqlstatus
-
-	Errorcodes	: Anmerkung
-	---------------------------------------------------------------------
-	-239		: so eine Freinummer gibbet schon;
-			  es duerfen nur von der NV geholte Nummern als
-			  Freinummern eingefuegt werden
-	0		: ok, Nummer als Freinummer in NV eingefuegt
-	100		: Nummer mit diesem Namen nicht in NV
-
-	Beschreibung    : stellt nicht gebrauchte Nummern in die
-			  Nummernverwaltung als Freinummern ein.
-
---------------------------------------------------------------------------*/

int nveinid (short dmdn, short dfil, char *dnr_nam, long dnr_nr)
{

/*--- lokale Definitionen ---*/
        short dsql1;
        short dreturn;

	dsql1   = 0;
	dreturn = 0;

	/*--- irgendeinen Satz der angegebenen Nummer lesen um an die
	      Zusatzdaten zu kommen
	---*/
	dsql1 =  recread_nv (dnr_nam, dmdn, dfil);

	if (dsql1 == 0)
        {
		/*--- Daten komplett beschaffen ---*/
		auto_nr.satz_kng  = 1;
		auto_nr.nr_nr     = dnr_nr;
		auto_nr.max_wert  = 0;
		auto_nr.delstatus = 0;

		nvdata_gen ();

		/*--- Freinummer in NV einfuegen ---*/
		dreturn =  recinsert_nv ();
        }
	else
        {
		/*--- SQL-Fehler ---*/
		dreturn = dsql1;
	} /* if */

	return (dreturn);

} /* of procedure nveinid */


/*---------------------------------------------------------------------------
-									  
-	Procedure	: nvanmprf
-
-	In		: dmdn		smallint  Mandantennummer
-			  dfil		smallint  Filialnummer
-			  dnr_nam	char(10)  Nummernname
-			  dnr_nr	integer	  Anfangswert Nummer
-			  dmax_nr       integer   maximaler Wert der Nummer
-			  dnr_char_lng	smallint  Laenge der Characternummer
-			  dfest_teil	char(10)  Festteil
-			  
-
-	Out		: sqlstatus
-			  
-	Errorcodes	: Anmerkung
-	---------------------------------------------------------------------
-	0		: ok, Nummer in NV angemeldet
-	1		: Fehler Uebergabeparameter; nr_char_lng > 10
-	2		: Fehler; Nummer gibbet schon
-			  Fehler; SQL-Status <> 100 bei Test ob gibt
-
-	Beschreibung    : Meldet eine Nummer in NV an.
-			  Die Angabe der maximal zulaessigen Nummer ermoeglicht
-			  die Ueberpruefung auf Nummernueberlauf.
-
---------------------------------------------------------------------------*/

int nvanmprf (short dmdn, short dfil, char * dnr_nam,
              long dnr_nr, long dmax_wert, long dnr_char_lng,
              char * dfest_teil)
{

/*--- Lokale Definitionen ---*/
        short dsql1;
        short dreturn;

	dsql1   = 0;
	dreturn = 0;

	/*--- Uebergabeparameter testen ---*/
	if ((dnr_char_lng >  10) || 
	    (dmax_wert<= dnr_nr))
        {
		dreturn = 1;
        }
	else
        {
		/*--- Test, ob Nummer in NV vorhanden ---*/
		dsql1 =  recread_nv (dnr_nam, dmdn, dfil);

		switch (dsql1)
                {
			case 100 :
				/*--- Name noch nicht vorhanden ---*/
				/*--- AUTO_NR initialisieren ---*/


				auto_nr.mdn         = dmdn;
				auto_nr.fil         = dfil;
				strcpy (auto_nr.nr_nam, dnr_nam);
				auto_nr.satz_kng    = 2;
				auto_nr.nr_nr       = dnr_nr;
				auto_nr.max_wert    = dmax_wert;
				auto_nr.nr_char_lng = dnr_char_lng;
				strcpy (auto_nr.fest_teil, dfest_teil);
				auto_nr.delstatus   = 0;

				nvdata_gen ();

				/*--- Satz in Tabelle AUTO_NR einfuegen ---*/
				dreturn =  recinsert_nv ();

				break;
			default :
				dreturn = 2;
				break;
		} /* of switch */
	} /* if */

	return (dreturn);

} /* of procedure nvanmprf */


/*-----------------------------------*/
/*--- interne P R O C E D U R E N ---*/
/*-----------------------------------*/

/*---------------------------------------------------------------------------
-									  
-	Procedure	: freinummer
-
-	In		:
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : notwendige Reaktionen, wenn die geholte Nummer
-			  eine Freinummer ist
-
---------------------------------------------------------------------------*/
int freinummer ()
{
/*--- lokale Definitionen ---*/
        short dsql1;
        short dreturn;

	dsql1   = 0;
	dreturn = 0;

	/*--- akt. Nummerndaten in Output ---*/
	memcpy (&taautonum, &auto_nr, sizeof (auto_nr));

	/*--- Vergebene Freinummer logisch aus NV loeschen;
	      => delstatus auf -1 setzen.
	      Ist notwendig um Transaktionsschutz und damit
	      Multiuserfaehigkeit zu erhalten.
	---*/
	auto_nr.delstatus = -1;

    dsql1 =  recupdate_nv ();

	if (dsql1 == 0)
        {
		/*--- Freinummer physikalisch aus NV loeschen;
		      hier wird durch DELETE der Tansaktionsrahmen aufgebrochen
		---*/
		dreturn = recdelete_nv ( auto_nr.nr_nam, 
                               auto_nr.satz_kng,
		               auto_nr.nr_nr,
		               auto_nr.mdn,
		               auto_nr.fil);

		/*--- dreturn ist immer "0"; ist ok, denn bei Fehler wird Satz
		      bei naechstem Loeschen mitgeloescht.
		---*/
        }
	else
        {
		/*--- Fehler bei update ---*/
		dreturn = dsql1;

	} /* if */

	return(dreturn);
}


/*---------------------------------------------------------------------------
-									  
-	Procedure	: nummernspeicher
-
-	In		:
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : notwendige Reaktionen, wenn die geholte Nummer
-			  aus dem Nummernspeicher kommt
-
---------------------------------------------------------------------------*/
int nummernspeicher ()
{

/*--- lokale Definitionen ---*/
        short dreturn;

	dreturn = 0;

	/*--- akt. Nummerndaten in Output ---*/
	memcpy (&taautonum, &auto_nr, sizeof (auto_nr));

	/*--- Pruefung, ob Ueberlauf ---*/
	if (auto_nr.nr_nr > auto_nr.max_wert)
        {
		/*--- ja, Ueberlauf ist da ---*/
		dreturn = -1;
        }
	else
        {
		/*--- nein, Nummer ist ok ---*/

		/*--- Nummer aktualisieren ---*/
		auto_nr.nr_nr ++;
		
		/*--- zus. Nummerninformationen generieren ---*/
		nvdata_gen ();

		/*--- Nummernspeicher updaten ---*/
		dreturn = recupdate_nv ();

	} /* if */

	return(dreturn);
}

/*---------------------------------------------------------------------------
-									  
-	Procedure	: nvdata_gen
-
-	In		:
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : notwendige Reparaturen, wenn beim Holen der Nummer
-			  eine ungueltige Satzkennung vorgefunden wurde
-
---------------------------------------------------------------------------*/
int nvdata_gen ()
{

        char dnullenspeicher [11];
        int  mnr_nr;

	mnr_nr = auto_nr.nr_nr;
        sprintf (auto_nr.nr_char,"%05d",mnr_nr);

	/*--- nr_char aus Nullen und nr_nr zusammenbauen ---*/
	dnullenspeicher[0] = 0;

        if (auto_nr.nr_char_lng == 0) auto_nr.nr_char_lng = 10;

	while (strlen (dnullenspeicher) < 
              (auto_nr.nr_char_lng - strlen(auto_nr.nr_char)))
        {
                strcat (dnullenspeicher,"0");
	}

	strcat (dnullenspeicher,auto_nr.nr_char);
	strcpy (auto_nr.nr_char, dnullenspeicher);

	/*--- nr_komb zusammenbauen ---*/
	/*--- initialisieren ---*/
	auto_nr.nr_komb[0] = 0;

	/*--- anhaengen nr_char ---*/
	strcat (auto_nr.nr_komb, auto_nr.nr_char);

	/*--- anhaengen fest_teil ---*/
	strcat (auto_nr.nr_komb, auto_nr.fest_teil);

        return 0;

} /* of procedure nvdata_gen */

/*---------------------------*/
/*--- P R O C E D U R E N ---*/
/*---------------------------*/

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepcuu_nv
-
-	In		: dnr_nam like auto_nr.nr_nam
-			  dmdn    like auto_nr.mdn
-			  dfil    like auto_nr.fil
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : prepariert update-Cursor cuu_nv
-							
---------------------------------------------------------------------------*/

int prepcuu_nv (char *dnr_nam, short dmdn, short dfil)
{

	/*--- Satz fuer update preparieren  ---*/

        ins_quest ((char *) auto_nr.nr_nam, 0, 11);
        ins_quest ((char *) &auto_nr.mdn, 1, 0);
        ins_quest ((char *) &auto_nr.fil, 1, 0);
        ins_quest ((char *) &auto_nr.satz_kng,1,0);

    out_quest ((char *) &auto_nr.mdn,1,0);
    out_quest ((char *) &auto_nr.fil,1,0);
    out_quest ((char *) auto_nr.nr_nam,0,11);
    out_quest ((char *) &auto_nr.nr_nr,2,0);
    out_quest ((char *) &auto_nr.satz_kng,1,0);
    out_quest ((char *) &auto_nr.max_wert,2,0);
    out_quest ((char *) auto_nr.nr_char,0,11);
    out_quest ((char *) &auto_nr.nr_char_lng,2,0);
    out_quest ((char *) auto_nr.fest_teil,0,11);
    out_quest ((char *) auto_nr.nr_komb,0,21);
    out_quest ((char *) &auto_nr.delstatus,1,0);
        cuu_nv = prepare_sql ("select auto_nr.mdn,  auto_nr.fil,  "
"auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  auto_nr.max_wert,  "
"auto_nr.nr_char,  auto_nr.nr_char_lng,  auto_nr.fest_teil,  "
"auto_nr.nr_komb,  auto_nr.delstatus "

                       "from auto_nr "
                       "where auto_nr.nr_nam  = ? "
                       "and auto_nr.mdn       = ? "  
                       "and auto_nr.fil       = ? " 
                       "and auto_nr.satz_kng  = ? " 
                       "and auto_nr.delstatus = 0 "
                       "for update");

        strcpy (auto_nr.nr_nam,dnr_nam);
        auto_nr.mdn = dmdn;
        auto_nr.fil = dfil;
        open_sql (cuu_nv);
	return(sqlstatus);
}

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_nv
-
-	In		:
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : holt naechsten Satz von update-Cursor cuu_nv
-							
---------------------------------------------------------------------------*/

int fetchcuu_nv ()
{
        auto_nr.satz_kng = 1;
        open_sql (cuu_nv);
        fetch_sql (cuu_nv);
        if (sqlstatus == 0)
        {
                  return(sqlstatus);
        }
        auto_nr.satz_kng = 2;
        open_sql (cuu_nv);
        fetch_sql (cuu_nv);
        return(sqlstatus);
}


/*---------------------------------------------------------------------------
-									  
-	Procedure	: closecuu_nv
-
-	In		:
-
-	Out		:
-			  
-	Beschreibung    : schliesst update-Cursor cuu_nv
-							
---------------------------------------------------------------------------*/

int closecuu_nv ()
{

        close_sql (cuu_nv);

        return 0;

} /* of procedure closecuu_nv */

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_nv
-
-	In		: dnr_nam like auto_nr.nr_nam
-			  dmdn    like auto_nr.mdn
-			  dfil    like auto_nr.fil
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : - liest irgendeinen Satz der angegebenen Nummer
-			    aus der NV;
-			  - Test, ob angegebene Nummer bereits in der
-			    NV vorhanden;
-							
---------------------------------------------------------------------------*/

int recread_nv (char *dnr_nam, short dmdn, short dfil)
{

        int sqlfehler;
        short recread_nv;

        strcpy (auto_nr.nr_nam, dnr_nam);
        auto_nr.mdn = dmdn;
        auto_nr.fil = dfil;

	/*--- Satz aus NV lesen ---*/

        ins_quest ((char *) auto_nr.nr_nam, 0, 11);
        ins_quest ((char *) &auto_nr.mdn, 1, 0);
        ins_quest ((char *) &auto_nr.fil, 1, 0);
    out_quest ((char *) &auto_nr.mdn,1,0);
    out_quest ((char *) &auto_nr.fil,1,0);
    out_quest ((char *) auto_nr.nr_nam,0,11);
    out_quest ((char *) &auto_nr.nr_nr,2,0);
    out_quest ((char *) &auto_nr.satz_kng,1,0);
    out_quest ((char *) &auto_nr.max_wert,2,0);
    out_quest ((char *) auto_nr.nr_char,0,11);
    out_quest ((char *) &auto_nr.nr_char_lng,2,0);
    out_quest ((char *) auto_nr.fest_teil,0,11);
    out_quest ((char *) auto_nr.nr_komb,0,21);
    out_quest ((char *) &auto_nr.delstatus,1,0);
        recread_nv = prepare_sql ("select auto_nr.mdn,  "
"auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  "
"auto_nr.max_wert,  auto_nr.nr_char,  auto_nr.nr_char_lng,  "
"auto_nr.fest_teil,  auto_nr.nr_komb,  auto_nr.delstatus "

                     "from auto_nr "
                     "where auto_nr.nr_nam    = ? "
                     "and  auto_nr.mdn        = ? "
                     "and  auto_nr.fil        = ? "
                     "and  auto_nr.delstatus  = 0");
        open_sql (recread_nv);
        fetch_sql (recread_nv);
        sqlfehler = sqlstatus;
        close_sql (recread_nv);
        sqlstatus = sqlfehler; 
	return(sqlstatus);
}   /* of procedure recread_nv */


/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_nv
-
-	In		:
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Satz in NV wird aktualisiert;
-			  - logisches Loeschen durch delstatus
-			  - Inkrementieren des Nummernspeichers
-							
---------------------------------------------------------------------------*/

int recupdate_nv ()
{

        int sqlfehler;
        short upd_auto_nr;

    ins_quest ((char *) &auto_nr.mdn,1,0);
    ins_quest ((char *) &auto_nr.fil,1,0);
    ins_quest ((char *) auto_nr.nr_nam,0,11);
    ins_quest ((char *) &auto_nr.nr_nr,2,0);
    ins_quest ((char *) &auto_nr.satz_kng,1,0);
    ins_quest ((char *) &auto_nr.max_wert,2,0);
    ins_quest ((char *) auto_nr.nr_char,0,11);
    ins_quest ((char *) &auto_nr.nr_char_lng,2,0);
    ins_quest ((char *) auto_nr.fest_teil,0,11);
    ins_quest ((char *) auto_nr.nr_komb,0,21);
    ins_quest ((char *) &auto_nr.delstatus,1,0);
    
	if (auto_nr.satz_kng == 1)
	{
        sprintf (sql,"update auto_nr set auto_nr.mdn = ?,  "
"auto_nr.fil = ?,  auto_nr.nr_nam = ?,  auto_nr.nr_nr = ?,  "
"auto_nr.satz_kng = ?,  auto_nr.max_wert = ?,  auto_nr.nr_char = ?,  "
"auto_nr.nr_char_lng = ?,  auto_nr.fest_teil = ?,  "
"auto_nr.nr_komb = ?,  auto_nr.delstatus = ? "
/*
                     "where current of %s",
                      cursor_name (cuu_nv));
*/
                       "where auto_nr.nr_nam  = ? "
                       "and auto_nr.mdn       = ? "  
                       "and auto_nr.fil       = ? " 
                       "and auto_nr.nr_nr     = ? " 
                       "and auto_nr.satz_kng  = ?"); 

        ins_quest ((char *) auto_nr.nr_nam,0,11);
        ins_quest ((char *) &auto_nr.mdn,1,0);
        ins_quest ((char *) &auto_nr.fil,1,0);
        ins_quest ((char *) &auto_nr.nr_nr,2,0);
        ins_quest ((char *) &auto_nr.satz_kng,1,0);
	}
	else
	{
        sprintf (sql,"update auto_nr set auto_nr.mdn = ?,  "
"auto_nr.fil = ?,  auto_nr.nr_nam = ?,  auto_nr.nr_nr = ?,  "
"auto_nr.satz_kng = ?,  auto_nr.max_wert = ?,  auto_nr.nr_char = ?,  "
"auto_nr.nr_char_lng = ?,  auto_nr.fest_teil = ?,  "
"auto_nr.nr_komb = ?,  auto_nr.delstatus = ? "
/*
                     "where current of %s",
                      cursor_name (cuu_nv));
*/
                       "where auto_nr.nr_nam  = ? "
                       "and auto_nr.mdn       = ? "  
                       "and auto_nr.fil       = ? " 
                       "and auto_nr.satz_kng  = ?"); 

        ins_quest ((char *) auto_nr.nr_nam,0,11);
        ins_quest ((char *) &auto_nr.mdn,1,0);
        ins_quest ((char *) &auto_nr.fil,1,0);
        ins_quest ((char *) &auto_nr.satz_kng,1,0);
	}

        upd_auto_nr = prepare_sql (sql);
        execute_curs (upd_auto_nr);
        sqlfehler = sqlstatus;
        close_sql (upd_auto_nr);
	return(sqlfehler);

} /* of procedure recupdate_nv */


/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_nv
-
-	In		: dnr_nam   like auto_nr.nr_nam
-			  dsatz_kng like auto_nr.satz_kng
-			  dnr_nr    like auto_nr.nr_nr
-			  dmdn      like auto_nr.mdn
-			  dfil      like auto_nr.fil
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : loescht Satz aus Nummernverwaltung
-							
---------------------------------------------------------------------------*/

int recdelete_nv (char *dnr_nam, short dsatz_kng, int dnr_nr, short dmdn,
                  short dfil)
{

        char name[20];
        short del_auto_nr;

        strcpy (auto_nr.nr_nam, dnr_nam);
        auto_nr.satz_kng = dsatz_kng;
        auto_nr.nr_nr    = dnr_nr;
        auto_nr.mdn      = dmdn;
        auto_nr.fil      = dfil;

        sprintf (name,"\"%s\"",auto_nr.nr_nam);

        sprintf (sql,"delete from auto_nr %s%s%s%hd%s%d%s%hd%s%hd%s" ,
	                     "where auto_nr.nr_nam     = ",name,
                             " and auto_nr.satz_kng    = ",auto_nr.satz_kng ,
		             " and auto_nr.nr_nr       = ",auto_nr.nr_nr   ,
		             " and auto_nr.mdn         = ",auto_nr.mdn    ,
		             " and auto_nr.fil         = ",auto_nr.fil   ,
		             " and auto_nr.delstatus   = -1");
        del_auto_nr = prepare_sql (sql);
        execute_curs (del_auto_nr);
        close_sql (del_auto_nr);
	return(0);

} /* of procedure recdelete_nv */

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_nv
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : fuegt Freinummer in NV ein
-							
---------------------------------------------------------------------------*/

int recinsert_nv ()
{
	/*--- Satz in NV einfuegen ---*/

        int sqlfehler;
        short ins_auto;

    ins_quest ((char *) &auto_nr.mdn,1,0);
    ins_quest ((char *) &auto_nr.fil,1,0);
    ins_quest ((char *) auto_nr.nr_nam,0,11);
    ins_quest ((char *) &auto_nr.nr_nr,2,0);
    ins_quest ((char *) &auto_nr.satz_kng,1,0);
    ins_quest ((char *) &auto_nr.max_wert,2,0);
    ins_quest ((char *) auto_nr.nr_char,0,11);
    ins_quest ((char *) &auto_nr.nr_char_lng,2,0);
    ins_quest ((char *) auto_nr.fest_teil,0,11);
    ins_quest ((char *) auto_nr.nr_komb,0,21);
    ins_quest ((char *) &auto_nr.delstatus,1,0);
        ins_auto = prepare_sql ("insert into auto_nr (mdn,  fil,  "
"nr_nam,  nr_nr,  satz_kng,  max_wert,  nr_char,  nr_char_lng,  fest_teil,  "
"nr_komb,  delstatus) "

                     "values " 
                     "(?,?,?,?,?,?,?,?,?,?,?)");


        execute_curs (ins_auto);
        sqlfehler = sqlstatus;
        close_sql (ins_auto);
	return(sqlfehler);
} /* of procedure recinsert_nv */
