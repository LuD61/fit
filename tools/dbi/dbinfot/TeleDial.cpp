#include <windows.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "TeleDial.h"
#include "DdeDial.h"


char *TeleDial::ClipDial = NULL;
char *TeleDial::DdeDialProg = NULL;
BOOL TeleDial::DdeDialActive = TRUE;
char *TeleDial::DdeDialService = NULL;
char *TeleDial::DdeDialTopic = NULL;
char *TeleDial::DdeDialItem = NULL;


int TeleDial::Dial (char *tel)
{
        if (strcmp (tel, " ") <= 0)
        {
                  disp_mess ("Es ist keine Telefon-Nummer vorhanden", 2);
                  return 0;
        }
        if (ClipDial != NULL)
        {
                return RunClipDial (tel);
        }
        else if (DdeDialProg != NULL)
        {
                return RunDdeDial (tel);
        }
        else
        {
                print_mess (0, "Telefon Nummer %s\n"
                               "Kein Wählprogramm installiert", tel);
        }
        return 0;
}


void TeleDial::TestActiveDial (void)
{ 
          DWORD ExitCode;

          if (DialPid != NULL)
          {
 		        GetExitCodeProcess (DialPid, &ExitCode);
		        if (ExitCode == STILL_ACTIVE)
                {
	                     TerminateProcess (DialPid, 0);
                }
          }
          DialPid = NULL;
}


int  TeleDial::RunClipDial (char *tel)
{
          char buffer [256];

          if (FaxTrenner != 0 && (wsplit (tel, "/")) > 1)
          {
              sprintf (buffer, "%s%c%s", wort[0], FaxTrenner, wort[1]);
              ToClipboard (buffer);
          }
          else
          {
              ToClipboard (tel);
          }

          TestActiveDial ();

          DialPid = ProcExecPid (ClipDial, SW_SHOWNORMAL, -1, 0, -1, 0);
          if (DialPid == NULL)
          {
              print_mess (2, "%s konnte nicht gestartet werden", ClipDial);
          }
          return 0;
}

int  TeleDial::RunDdeDial (char *tel)
{
          char buffer [256];

          if (FaxTrenner != 0 && (wsplit (tel, "/")) > 1)
          {
              sprintf (buffer, "%s%c%s", wort[0], FaxTrenner, wort[1]);
          }
          else
          {
              strcpy (buffer, tel);
          }

//          TestActiveDial ();

          if (DdeDialActive == FALSE)
          {
              DialPid = ProcExecPid (DdeDialProg, SW_SHOWMINNOACTIVE, -1, 0, -1, 0);
              if (DialPid == NULL)
              {
                  print_mess (2, "%s konnte nicht gestartet werden", DdeDialProg);
                  return 0;
              }
              Sleep (1000);
          }
          DdeDial *ddeDial = new DdeDial ();
          ddeDial->SetServer (DdeDialProg);
          if (DdeDialService != NULL)
          {
              ddeDial->SetService (DdeDialService);
          }
          if (DdeDialTopic != NULL)
          {
              ddeDial->SetTopic (DdeDialTopic);
          }
          if (DdeDialItem != NULL)
          {
              ddeDial->SetItem (DdeDialItem);
          }
          ddeDial->Call (buffer);
          delete ddeDial;
          return 0;
}

