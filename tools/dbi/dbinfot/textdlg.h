#ifndef _TEXTDLG_DEF
#define _TEXTDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "stringwork.h"


class TextDlg : virtual public DLG 
{
          private :
              static mfont dlgfont;
              static mfont buttonfont;
              CFIELD **Cfields;
              CFORM *Cform;
              char *Updates[100];
              char **Feldnamen;
              char **Texte;
              char **Felder;
              char **Namen;
              int *Rows;
              int *Columns;
              int *Length;
              int headfieldanz;
              int fieldanz;
              int Offset;
          public :
//              TextDlg (char *, int, int, int, int, char *, int, BOOL); 
              TextDlg (StringWork *textWork, int, int, int, int, char *, int, BOOL); 
              ~TextDlg ();
              char **GetUpdates (void)
              {
                  return Updates;
              }
              void SetText (void);
              BOOL OnKey5 ();
              BOOL OnKeyReturn ();
              BOOL OnKey12 ();
              BOOL OnKeyEscape ();
              BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
              BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
};
#endif
