#ifndef _TELEDIAL_DEF
#define _TELEDIAL_DEF

class TeleDial
{
    private :
         char FaxTrenner;
         HANDLE DialPid;
 
    public :
        static char *ClipDial;
        static char *DdeDialProg;
        static BOOL DdeDialActive;
        static char *DdeDialService;
        static char *DdeDialTopic;
        static char *DdeDialItem;

        TeleDial () 
        {
            FaxTrenner = 0;
            DialPid = NULL;    
        }

        ~TeleDial ()
        {
        }

        int Dial (char *);
        void TestActiveDial (void);
        int RunClipDial (char *);
        int RunDdeDial (char *tel);
};
#endif


