#include <stdio.h>
#include "dbclass.h"
#include "strfkt.h"
#include "dll.h"

#define MAXDB 20

extern char *wort[];
extern char *zwort[];

short DEBUG = 0;
short sql_mode = 0;
static BOOL ErrProc (SDWORD, char *);
void GetError (HSTMT);

short ShortNull = (short) 0x8000;
long LongNull = (long) 0x80000000;
double DoubleNull = (double) 0xffffffffffffffff;


static int TestOut (HSTMT Cursor);
static int TestIn (HSTMT Cursor);

char SqlErrorText [512];

BOOL short_null (short s)
{
	if (s == ShortNull)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL long_null (long l)
{
	if (l == LongNull)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL double_null (double d)
{
	if (d == DoubleNull)
	{
		return TRUE;
	}
	return FALSE;
}

struct DbS
{
       char name [80];
       HDBC hdbc;
};


struct DbS DbTab [MAXDB];
static int dbpos;

static void AddDb (char *name, HDBC hdbc)
{
       if (dbpos >= MAXDB - 1)
       {
           return;
       }

       strcpy (DbTab[dbpos].name, name);
       DbTab[dbpos].hdbc = hdbc;
       dbpos ++;
}

static void DropDb (char *name)
{
       int i;
       for (i = 0; i < dbpos; i ++)
       {
           if (strcmp (name, DbTab[i].name) == 0)
           {
               break;
           }
       }

       if (dbpos > 0)
       {
           dbpos --;
       }
       for (; i < dbpos; i ++)
       {
           memcpy (&DbTab[i], &DbTab[i + 1], sizeof (struct DbS));
       }
       strcpy (DbTab[i].name, "");
       DbTab[i].hdbc = NULL;
}

int SelectDb (char *name, HDBC *hdbc)
{
       int i;
       for (i = 0; i < dbpos; i ++)
       {
           if (strcmp (name, DbTab[i].name) == 0)
           {
               break;
           }
       }
       if (i == dbpos)
       {
           return (-1);
       }
       *hdbc = DbTab[i].hdbc;
       return (0);
}

struct CURSOR_CONNECT
{
	HDBC  hdbc;
	HSTMT hstmt;
};
  
typedef struct CURSOR_CONNECT CU_CONNECT;  


static HENV    henv = NULL;
static HDBC    hdbc = NULL;
// static HDBC *  Connections = NULL;
static CU_CONNECT * Connections = NULL;
    
static char    name[80] = {""};

void setdbname (char *dbn)
{
	strcpy (name, dbn);
}

void setdb (HENV he, HDBC hd)
{
	henv = he;
	hdbc  = hd;
}

static HSTMT   hstmDirect = NULL;
static HSTMT   hstm = NULL;
static int     CursTab[MAXCURS];  
static HSTMT   HstmtTab[MAXCURS];  
static SQLVAR  OutVars[MAXVARS];
static SQLVAR  InVars[MAXVARS];
static int     OutAnz = 0;
static int     InAnz = 0;
static BOOL    InWork = FALSE;
static SDWORD  cbLen = 0;
static BOOL (*SqlErrorProc) (SDWORD, char *) = ErrProc;
static int   (*sql_proc) () = NULL;

short sql_mod = 0;
int sqlstatus = 0;
int sqlerror[3];
char sql[0x1000];


void SetSqlErrorProc (BOOL (*ErrProc) (SDWORD, char *))
{
               SqlErrorProc = ErrProc;
}

void set_sqlproc (int (*sqlproc) (void))
{
	 sql_proc = sqlproc;
}


int FromRecDate (DATE_STRUCT *sqldate, char *cdate)
{
	   int anz;
       char day [3];
       char month [3];
       char year [5];
	   int len;

	   sqldate->day = 0; 
	   sqldate->month = 0; 
	   sqldate->year = 0; 

	   len = strlen (cdate);
	   if (len < 6)
	   {
                      return (0);
	   }
       strcpy (day,   "01"); 
       strcpy (month, "01"); 
       strcpy (year,  "1900"); 
	   memcpy (day, &cdate[0], 2);
	   memcpy (month, &cdate[2], 2);
	   if (len == 8)
	   {
         	   memcpy (year, &cdate[4], 4);
	   }
	   else if (len == 6)
	   {
		       memcpy (&year[2], &cdate[4], 2);
			   if (atoi (&year[2]) < 80)
			   {
				   memcpy (year, "20", 2);
			   }
	   }


	   sqldate->day = atoi (day); 
	   sqldate->month = atoi (month); 
	   sqldate->year = atoi (year);
	   return (0);
}
		

int FromOdbcDate (DATE_STRUCT *sqldate, char *cdate)
{
	   int anz;
           char ndate [12];

	   sqldate->day = 0; 
	   sqldate->month = 0; 
	   sqldate->year = 0; 
	   anz = zsplit (cdate, '-');
	   if (anz < 3)
	   {
                      return FromRecDate (sqldate, cdate);
	   }

	   sqldate->day = atoi (zwort[3]); 
	   sqldate->month = atoi (zwort[2]); 
	   sqldate->year = atoi (zwort[1]);
	   return (0);
}
		
int FromGerDate (DATE_STRUCT *sqldate, char *cdate)
{
	   int anz;
	   sqldate->day = 0; 
	   sqldate->month = 0; 
	   sqldate->year = 0; 
	   anz = zsplit (cdate, '.');
	   if (anz < 3)
	   {
               return (FromOdbcDate (sqldate, cdate));
	   }

	   sqldate->day = atoi (zwort[1]); 
	   sqldate->month = atoi (zwort[2]); 
	   sqldate->year = atoi (zwort[3]);
	   return (0);
}

int ToGerDate (DATE_STRUCT *sqldate, char *cdate)
{
	   sprintf (cdate, "%02hd.%02hd.%02hd", sqldate->day,
		                                    sqldate->month,
											sqldate->year);
	   return (0);
}

int FromOdbcTime (TIME_STRUCT *sqltime, char *ctime)
{
	   int anz;

	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
	   anz = zsplit (ctime, '-');
	   if (anz < 3)
	   {
               return (0);
	   }

	   sqltime->hour = atoi (zwort[3]); 
	   sqltime->minute = atoi (zwort[2]); 
	   sqltime->second = atoi (zwort[1]);
	   return (0);
}

int FromGerTime (TIME_STRUCT *sqltime, char *ctime)
{
	   int anz;

	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
	   anz = zsplit (ctime, ':');
	   if (anz < 3)
	   {
               return (FromOdbcTime (sqltime, ctime));
	   }

	   sqltime->hour   = atoi (zwort[1]); 
	   sqltime->minute = atoi (zwort[2]); 
	   sqltime->second = atoi (zwort[3]);
	   return (0);
}

int ToGerTime (TIME_STRUCT *sqltime, char *ctime)
{
	   sprintf (ctime, "%02hd:%02hd:%02hd", sqltime->hour,
		                                    sqltime->minute,
											sqltime->second);
	   return (0);
}

int FromOdbcTimestamp (TIMESTAMP_STRUCT *sqltime, char *ctime)
{
       DATE_STRUCT dt;
       TIME_STRUCT tm; 

	   sqltime->day = 0; 
	   sqltime->month = 0; 
	   sqltime->year = 0; 
	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
           if (strlen (ctime) < 19)
           {
                    return (0);
           }
           FromOdbcDate (&dt, ctime);
           FromOdbcTime (&tm, &ctime[11]);

	   sqltime->day    = dt.day; 
	   sqltime->month  = dt.month; 
	   sqltime->year   = dt.year;
	   sqltime->hour   = tm.hour; 
	   sqltime->minute = tm.minute; 
	   sqltime->second = tm.second;
	   return (0);
}

int FromGerTimestamp (TIMESTAMP_STRUCT *sqltime, char *ctime)
{

       DATE_STRUCT dt;
       TIME_STRUCT tm; 

	   sqltime->day = 0; 
	   sqltime->month = 0; 
	   sqltime->year = 0; 
	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
           FromGerDate (&dt, ctime);
	   sqltime->day    = dt.day; 
	   sqltime->month  = dt.month; 
	   sqltime->year    = dt.year;
           if (strlen (ctime) < 11)
           {
                return (0);
           }
           FromGerTime (&tm, &ctime[10]);

	   sqltime->hour   = tm.hour; 
	   sqltime->minute = tm.minute; 
	   sqltime->second = tm.second;
	   return (0);
}

int ToGerTimestamp (TIMESTAMP_STRUCT *sqltime, char *ctime)
{
	   sprintf (ctime, "%02hd.%02hd.%02hd %02hd:%02hd:%02hd ",
                                            sqltime->day,
		                                    sqltime->month,
											sqltime->year,
                                            sqltime->hour,
		                                    sqltime->minute,
											sqltime->second);
	   return (0);
}

int GetCountConnections ()
{
	  int i;

	  if (Connections == NULL) return 0;

	  for (i = 0; Connections[i].hdbc != NULL; i ++);
	  return i;
}

void AddConnection (HDBC hdbc, HSTMT hstmt)
{
	  CU_CONNECT *c; 
      int cc; 
	  int i;

	  if (Connections == NULL)
	  {
		  Connections = (CU_CONNECT *) malloc (2 * sizeof (CU_CONNECT));
		  Connections[0].hdbc   = hdbc;
		  Connections[0].hstmt  = hstmt;
		  Connections[1].hdbc = NULL;
		  return;
	  }
	  else
	  {
		  cc = GetCountConnections ();
	  }
	  c = malloc ((cc + 2) * sizeof (CU_CONNECT));
      for (i = 0; i < cc; i ++)
	  {
		  memcpy (&c[i], &Connections[i], sizeof (CU_CONNECT));
	  }
	  c[i].hdbc   = hdbc;
	  c[i].hstmt  = hstmt;
	  c[i + 1].hdbc = NULL;
	  Connections = c;
	  free (c);
}
    
void DropConnection (HDBC hdbc)
{
	  CU_CONNECT *c; 
      int cc; 
	  int i;
	  int j;

	  cc = GetCountConnections ();
	  if (cc == 0) return;
	  c = malloc ((cc) * sizeof (CU_CONNECT));
      for (i = 0, j = 0; i < cc; i ++)
	  {
		  if (Connections[i].hdbc == hdbc) continue;
		  memcpy (&c[j], &Connections[i], sizeof (CU_CONNECT));
		  j ++;
	  }
	  c[j].hdbc = NULL;
	  Connections = c;
	  free (c);
}

    
HDBC GetConnection (HSTMT hstmt)
{
      int cc; 
	  int i;

	  cc = GetCountConnections ();
      for (i = 0; i < cc; i ++)
	  {
		  if (Connections[i].hstmt == hstmt) 
		  {
			  return (Connections[i].hdbc);
		  }
	  }
	  return (NULL);
}
    
		  
int opendbase (char *dbase)
{
     int retcode;
     int sql_s;
     short buflen;
     char buf[257];
     char dataSource[120];

     strcpy (name, dbase);
     retcode = SQLAllocEnv(&henv);
     if (retcode == SQL_SUCCESS); 
	 else
	 {
                printf ("Fehler bei SQLAllocEnv\n");
	        exit (1);
	 }
     retcode = SQLAllocConnect(henv, &hdbc); /* Connection handle */


     if (retcode == SQL_SUCCESS); 
	 else
	 {
                printf ("Fehler bei SQLAllocConnect\n");
	        exit (1);
	 }

     SQLSetConnectOption (hdbc, SQL_AUTOCOMMIT_OFF, 0);
//     SQLSetConnectOption (hdbc, SQL_MODE_READ_WRITE, 0);

// Connect ueber SQLConnect


#ifdef WIN32
     retcode = SQLConnect(hdbc, (UCHAR *) dbase, SQL_NTS, 
		                        (UCHAR *) "", SQL_NTS, 
								(UCHAR *) "", SQL_NTS);

      if (retcode != SQL_SUCCESS && retcode !=SQL_SUCCESS_WITH_INFO)
#endif

      {
// Connect ueber SQLDriverConnect

      if (strstr (dbase, "dsn="))
      {
              sprintf (dataSource, "%s", dbase); 
      }
      if (strstr (dbase, "DSN="))
      {
              sprintf (dataSource, "%s", dbase); 
      }
      else
      {
              sprintf (dataSource, "dsn=%s", dbase); 
      }

      retcode = SQLDriverConnect (hdbc, 0, (UCHAR *) dataSource, SQL_NTS,
      (UCHAR *) buf, sizeof (buf), &buflen, SQL_DRIVER_COMPLETE);
     }

     if (retcode == SQL_SUCCESS || retcode ==SQL_SUCCESS_WITH_INFO) 
     { 
     }
     else
     {
             GetError (NULL);
             exit (1);
     }


     AddDb (dbase, hdbc);
	 AddConnection (NULL, NULL);
     sqlstatus = 0;
     sql_s = sql_mode;
     sql_mode = 1;
 //    execute_sql ("set isolation to dirty read");  
     sql_mode = sql_s;
     return (sqlstatus);
}

void setddbase ()
{
	if (henv != NULL && hdbc != NULL)
	{
#ifdef WIN32
		setdlldbase (henv, hdbc);
#endif
	}
}

#ifdef DLL
EXPORT void setdlldbase (HENV he, HDBC hb)
{
	henv = he;
	hdbc = hb;
}
#endif


int closeconnections ()
{
	 HDBC cHdbc;

     while (Connections[0].hdbc != NULL)
	 {
		 cHdbc = Connections[0].hdbc;
		 SQLDisconnect(cHdbc);
	     SQLFreeConnect(cHdbc);
		 DropConnection (cHdbc);
	 }
}


int closedbase (char *dname)
/**
Datenbank schliessen.
**/

{

	 SQLDisconnect(hdbc);
	 SQLFreeConnect(hdbc);
     SQLFreeEnv(henv);
     hdbc = NULL;
     henv = NULL;
     sqlstatus = 0;
     DropDb (dname);
     return (sqlstatus);
}


int sqlconnect (char *server, char *user, char *passw)
{

     int retcode = SQLAllocEnv(&henv);
     if (retcode == SQL_SUCCESS); 
	 else
	 {
		     printf ("Fehler bei SQLAllocEnv\n");
		     exit (1);
	 }
     retcode = SQLAllocConnect(henv, &hdbc); /* Connection handle */

     if (retcode == SQL_SUCCESS); 
	 else
	 {
		     printf ("Fehler bei SQLAllocConnect\n");
		     exit (1);
	 }
     return (0);
}


int sqlconnectdbase (char *server, char *user, char *passw, char *dbase)
{
     opendbase (dbase);
     return sqlstatus;
}

int beginwork (void)
{
	short sql_s;

	sql_s = sql_mode;
	sql_mode = 1;
    if (InWork)
    {
        commitwork ();
    }
//    execute_sql ("begin work");
    InWork = TRUE;
	sql_mode = sql_s;
    sqlstatus = 0;
    return 0;
}

int commitwork (void)
{
	short sql_s;
        int retcode;

	sql_s = sql_mode;
//	sql_mode = 1;
    if (InWork)
    {

/*
              execute_sql ("commit work");
		if (sqlstatus < 0)
*/

		{
               retcode = SQLTransact (henv, SQL_NULL_HDBC, SQL_COMMIT);
               if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
               {
                        GetError (hstmDirect);
                        sqlstatus = -1;
                        return -1;
               }
		}
        InWork = FALSE;
    }
	sql_mode = sql_s;
    sqlstatus = 0;
    return 0;
}

int rollbackwork (void)
{
	short sql_s;

	sql_s = sql_mode;
	sql_mode = 1;
    if (InWork)
    {
/*
        execute_sql ("rollback work");
		if (sqlstatus < 0)
*/
		{
              SQLTransact (henv, SQL_NULL_HDBC, SQL_ROLLBACK);
		}
        InWork = FALSE;
    }
	sql_mode = sql_s;
    sqlstatus = 0;
    return 0;
}

int select_dbase (char *dbase)
{
     if (SelectDb (dbase, &hdbc) == -1)
     {
         return -1;
     }
     return (0);
}

int execute_sql (char *statement)
{
        int retcode; 

		if (henv == NULL)
		{
			opendbase (name);
		}

        retcode = SQLAllocStmt (hdbc, &hstmDirect);
        if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             sqlstatus = -1;
		     return -1;
	 }

     retcode = TestOut (hstmDirect);
     retcode = TestIn (hstmDirect);

     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             sqlstatus = -1;
		     return -1;
	 }
     retcode = SQLExecDirect((HSTMT) hstmDirect, 
		                  (unsigned char *) statement,
						  (SDWORD) strlen ((char *) statement));
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
     {
             GetError (hstmDirect);
             sqlstatus = -1;
             return -1;
     }

     sqlstatus = 0;
     if (strstr (statement,"select"))
     {
             retcode = SQLFetch(hstmDirect); 
             if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) 
             {
	                 sqlstatus = 0; 
             }
             else if (retcode == SQL_NO_DATA)
             {
                     sqlstatus = 100;
             }
             else
             { 
                    GetError (hstmDirect);
                    sqlstatus = -1;
                    return -1;  
             }
     }
     return sqlstatus;
}


int execute_db (char *dbase, char *statement)
{
     if (SelectDb (dbase, &hdbc) == -1)
     {
         return -1;
     }
     return execute_sql (statement);
}

int prepare_tables (char *Table)
{
	int retcode;
    int cursor;
	HSTMT Cursor;

	if (hdbc == NULL)
	{
		return -1;
	}
    sqlstatus = 0;
    for (cursor  = 0; cursor < MAXCURS; cursor ++)
    {
        if (CursTab[cursor] == 0)
        {
            CursTab[cursor] = 3;
            break;
        }
    }

	 retcode = SQLAllocStmt (hdbc, &HstmtTab[cursor]);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (NULL);
             CursTab[cursor] = 0;
                     sqlstatus = -1;
		     return -1;
	 }

     Cursor = HstmtTab[cursor];
	 retcode = SQLTables(Cursor, NULL, 0, NULL, 0, Table, SQL_NTS,
		                         NULL, 0);
	 return cursor;
}

int prepare_columns (char *Table, char *Column)
{
	 int retcode;
     int cursor;
	 HSTMT Cursor;

	 if (hdbc == NULL)
	 {
		return -1;
	 }
     sqlstatus = 0;
     for (cursor  = 0; cursor < MAXCURS; cursor ++)
     {
        if (CursTab[cursor] == 0)
        {
            CursTab[cursor] = 3;
            break;
        }
	 }

	 retcode = SQLAllocStmt (hdbc, &HstmtTab[cursor]);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (NULL);
             CursTab[cursor] = 0;
                     sqlstatus = -1;
		     return -1;
	 }

     Cursor = HstmtTab[cursor];
	 retcode = SQLColumns(Cursor, NULL, 0, NULL, 0, (UCHAR FAR *) Table, SQL_NTS,
		                          Column, SQL_NTS);
	 return cursor;
}

int prepare_sql (char *statement)
{
	int retcode;
    int cursor;
	HSTMT Cursor;

    if (henv == NULL)
	{
		opendbase (name);
	}

    sqlstatus = 0;
    for (cursor  = 0; cursor < MAXCURS; cursor ++)
    {
        if (CursTab[cursor] == 0)
        {
            CursTab[cursor] = 1;
            break;
        }
    }

	retcode = SQLAllocStmt (hdbc, &HstmtTab[cursor]);
    if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	{
             GetError (NULL);
             CursTab[cursor] = 0;
                     sqlstatus = -1;
		     return -1;
	}

    Cursor = HstmtTab[cursor];
    retcode = SQLPrepare(Cursor, 
		                  (UCHAR *)statement,
						  (SDWORD) strlen ((char *) statement));
    if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	{
             GetError (Cursor);
             CursTab[cursor] = 0;
                     sqlstatus = -1;
		     return -1;
	}

    retcode = TestIn (Cursor);
    if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	{
             GetError (Cursor);
             CursTab[cursor] = 0;
                     sqlstatus = -1;
		     return -1;
	}

    retcode = TestOut (Cursor);

    if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	{
             GetError (Cursor);
             CursTab[cursor] = 0;
                     sqlstatus = -1;
		     return -1;
	}
    return cursor;
}


int prepare_db (char *dbase, char *statement)
{
     if (SelectDb (dbase, &hdbc) == -1)
     {
         return -1;
     }
     return prepare_sql (statement);
}


int prepare_sqlconnect (char *statement)
{
	 HDBC stdhdbc = hdbc;
	 int retcode;
     char dataSource[120];
     int sql_s;
     short buflen;
     char buf[257];
	 int cursor;

     retcode = SQLAllocConnect(henv, &hdbc); /* Connection handle */


     if (retcode == SQL_SUCCESS); 
	 else
	 {
            printf ("Fehler bei SQLAllocConnect\n");
	        exit (1);
	 }

     SQLSetConnectOption (hdbc, SQL_AUTOCOMMIT_OFF, 0);

// Connect ueber SQLConnect


#ifdef WIN32
     retcode = SQLConnect(hdbc, (UCHAR *) name, SQL_NTS, 
		                        (UCHAR *) "", SQL_NTS, 
								(UCHAR *) "", SQL_NTS);

      if (retcode != SQL_SUCCESS && retcode !=SQL_SUCCESS_WITH_INFO)
#endif

      {
// Connect ueber SQLDriverConnect

      if (strstr (name, "dsn="))
      {
              sprintf (dataSource, "%s", name); 
      }
      if (strstr (name, "DSN="))
      {
              sprintf (dataSource, "%s", name); 
      }
      else
      {
              sprintf (dataSource, "dsn=%s", name); 
      }

      retcode = SQLDriverConnect (hdbc, 0, (UCHAR *) dataSource, SQL_NTS,
      (UCHAR *) buf, sizeof (buf), &buflen, SQL_DRIVER_COMPLETE);
     }

     if (retcode == SQL_SUCCESS || retcode ==SQL_SUCCESS_WITH_INFO) 
     { 
     }
     else
     {
             GetError (NULL);
             exit (1);
     }

     sqlstatus = 0;
     sql_s = sql_mode;
     sql_mode = 1;
     sql_mode = sql_s;
     cursor = prepare_sql (statement);
	 if (cursor != -1)
	 {
		AddConnection (hdbc, HstmtTab [cursor]);
	 }
	 hdbc = stdhdbc;
	 return cursor;
}


int free_sql (int cursor)
{
	 HSTMT Cursor;
	 int retcode;

     sqlstatus = 0;
     if (cursor < 0)
     {
         sqlstatus = -1;
         return -1;
     }

     if (CursTab[cursor] == 0)
     {
         sqlstatus = -1;
         return -1;
     }

     Cursor = HstmtTab[cursor];


//  	 while (SQLMoreResults (Cursor) != SQL_NO_DATA);
  	 retcode = SQLFreeStmt (Cursor, SQL_CLOSE);
     if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
     {
                 sqlstatus = 0;
     } 
     else
     {
                 GetError (Cursor);
 		         sqlstatus = 0 - retcode;
                 sqlstatus = -1;
                 return -1;
     }
     CursTab[cursor] = 1;
     return sqlstatus;
}


int open_sql (int cursor)
{
	 HSTMT Cursor;
	 int retcode;

     sqlstatus = 0;
     if (cursor < 0)
     {
         sqlstatus = -1;
         return -1;
     }

     if (CursTab[cursor] == 0)
     {
         sqlstatus = -1;
         return -1;
     }

     Cursor = HstmtTab[cursor];


//  	 while (SQLMoreResults (Cursor) != SQL_NO_DATA);
  	 retcode = SQLFreeStmt (Cursor, SQL_CLOSE);
     if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
     {
                 sqlstatus = 0;
     } 
     else
     {
                 GetError (Cursor);
 		         sqlstatus = 0 - retcode;
                 sqlstatus = -1;
                 return -1;
     }


     retcode = SQLExecute(Cursor); 
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
     {
                 GetError (Cursor);
  		         sqlstatus = 0 - retcode;
                 sqlstatus = -1;
                 return -1;
     } 

     CursTab[cursor] = 2;
     return sqlstatus;
}

int bind_sqlcol (int cursor, int colnr, void *var, int typ, int len)
{
  	    HSTMT Cursor;
	    int retcode;

        sqlstatus = 0;
        if (cursor < 0)
        {
             sqlstatus = -1;
             return -1;
        }

        if (CursTab[cursor] == 0)
        { 
             sqlstatus = -1;
             return -1;
        }
 
        Cursor = HstmtTab[cursor];
        switch (typ)
        {
                 case SQLCCHAR:
                        retcode = SQLBindCol(Cursor, colnr, 
                                         SQL_CHAR,  
                                         (char *) var, 
                                                  len, 
                                                  0); 
                        break;
                 case SQLCSHORT:
                        retcode = SQLBindCol(Cursor, colnr, 
                                         SQL_C_SHORT,  
                                         (short *) var, 
                                                   sizeof (short), 
                                                   0); 
                        break;
                 case SQLCLONG:
                        retcode = SQLBindCol(Cursor, colnr, 
                                         SQL_C_LONG,  
                                         (long *) var, 
                                                   sizeof (long), 
                                                   0); 
                        break;
                 case SQLCDOUBLE:
                        retcode = SQLBindCol(Cursor, colnr, 
                                         SQL_DOUBLE,  
                                         (double *)  var, 
                                                     sizeof (double), 
                                                     0); 
                        break;
                 case SQLCDATE:
                        retcode = SQLBindCol(Cursor, colnr, 
                                         SQL_C_DATE,  
                                         (long *)  var, 
                                                   sizeof (long), 
                                                   0); 
                        break;
     }
     return retcode;
}

int fetch_sql (int cursor)
{
 
        int dsqlstatus;
  	    HSTMT Cursor;
	    int retcode;

        sqlstatus = 0;
        if (cursor < 0)
        {
             sqlstatus = -1;
             return -1;
        }

        if (CursTab[cursor] == 0)
        { 
             sqlstatus = -1;
             return -1;
        }

        Cursor = HstmtTab[cursor];
        if (CursTab [cursor] == 1)
        {
                    open_sql (cursor);
        }
        else if (CursTab [cursor] == 3)
		{
/*
            	    retcode = SQLFreeStmt (Cursor, SQL_CLOSE);
                    if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
					{
                               sqlstatus = 0;
					}
                    else
					{
                               GetError (Cursor);
 		                       sqlstatus = 0 - retcode;
                               sqlstatus = -1;
                               return -1;
					}
*/
                    CursTab [cursor] = 2;
		}
        retcode = SQLFetch(Cursor);
        if (retcode == SQL_SUCCESS) 
        {
	           sqlstatus = 0; 
        }
        else if (retcode == SQL_NO_DATA)
        {
                 sqlstatus = 100;
        }
        else
        {

                 GetError (Cursor);
//                 sqlstatus = -1;
        }
//		SQLMoreResults (Cursor);
        dsqlstatus = sqlstatus;
        return sqlstatus;
}


int execute_curs (int cursor)
/**
Cursor oeffnen.
**/
{
	    HSTMT Cursor;
	    int retcode;

        sqlstatus = 0;
        if (cursor < 0)
        {
             sqlstatus = -1;
             return -1;
        }

        if (CursTab[cursor] == 0)
        { 
             sqlstatus = -1;
             return -1;
        }

        Cursor = HstmtTab[cursor];

  	    retcode = SQLFreeStmt (Cursor, SQL_CLOSE);
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
		{
                 sqlstatus = 0;
		} 
        else
		{
                 GetError (Cursor);
 		         sqlstatus = 0 - retcode;
                  sqlstatus = -1;
                 return -1;
		}
        retcode = SQLExecute(Cursor); 
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
        {
                 sqlstatus = 0;
        } 
	    else
        {
                 GetError (Cursor);
                 sqlstatus = -1;
        }
        return sqlstatus;
}

int get_colanz (int cursor)
{
	 HSTMT Cursor;
     SDWORD colanz;
     SWORD FAR cbDesc;
     char rgbDesc [128];
	 int ret;

     sqlstatus = 0;
     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
        sqlstatus = -1;
         return -1;
     }

     Cursor = HstmtTab[cursor];
     ret = SQLColAttributes(Cursor, 1, 
				            SQL_COLUMN_COUNT, rgbDesc, 128, &cbDesc, &colanz); 
     if (ret < 0)
	 {
	                   GetError (Cursor);
					   return (0);
	 }
	 return ((int) colanz);
}

int get_collength (int colpos, int cursor)
{
	 HSTMT Cursor;
     SDWORD collength;
     SDWORD precision;
     SDWORD scale;
     SWORD FAR cbDesc;
     char rgbDesc [128];
	 int ret;
     int type;

     sqlstatus = 0;


     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
        sqlstatus = -1;
         return -1;
     }

     Cursor = HstmtTab[cursor];
     type =  get_coltype (colpos, cursor);
     if (type == SQLDECIMAL)
     {
         ret = SQLColAttributes(Cursor, colpos, 
	  			                SQL_COLUMN_PRECISION, rgbDesc, 128, &cbDesc, &precision); 
         if (ret < 0)
         {
	                   GetError (Cursor);
					   return (0);
         }
         ret = SQLColAttributes(Cursor, colpos, 
	  			                SQL_COLUMN_SCALE, rgbDesc, 128, &cbDesc, &scale); 
         if (ret < 0)
         {
	                   GetError (Cursor);
					   return (0);
         }
         collength = (precision << 8) | scale;
         return ((int) collength);
     }

     ret = SQLColAttributes(Cursor, colpos, 
				            SQL_COLUMN_LENGTH, rgbDesc, 128, &cbDesc, &collength); 
     if (ret < 0)
	 {
	                   GetError (Cursor);
					   return (0);
	 }
     if (type == SQLCHAR)
     {
//         collength ++;
     }
	 return ((int) collength);
}


char *get_colname (int colpos, int cursor, char *name)
{
	 HSTMT Cursor;
     SDWORD collength;
     SWORD FAR cbDesc;
     char rgbDesc [128];
	 int ret;

     sqlstatus = 0;
     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
        sqlstatus = -1;
         return NULL;
     }

     Cursor = HstmtTab[cursor];
     ret = SQLColAttributes(Cursor, colpos, 
				            SQL_COLUMN_NAME, rgbDesc, 128, &cbDesc, &collength); 
     if (ret < 0)
	 {
	                   GetError (Cursor);
					   return (NULL);
	 }
     strcpy (name, rgbDesc);
	 return (name);
}

int get_coltype (int colpos, int cursor)
{
	 HSTMT Cursor;
     SDWORD coltype;
     SWORD FAR cbDesc;
     char rgbDesc [128];
	 int ret;

     sqlstatus = 0;
     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
        sqlstatus = -1;
         return -1;
     }

     Cursor = HstmtTab[cursor];
     ret = SQLColAttributes(Cursor, colpos, 
				            SQL_COLUMN_TYPE, rgbDesc, 128, &cbDesc, &coltype); 
     if (ret < 0)
	 {
	                   GetError (Cursor);
					   return (-1);
	 }
/*
         if (coltype == SQL_VARCHAR)
         {
                 coltype = SQL_CHAR;
         } 
*/
	 return ((int) coltype);
}

int get_colvalue (int colpos, int cursor, int type, void *value, int len)
{
	 HSTMT Cursor;
     SDWORD FAR cbDesc;
	 int ret;

     sqlstatus = 0;
     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
        sqlstatus = -1;
         return -1;
     }

     switch (type)
     {
         case SQLCCHAR :
             type = SQL_C_CHAR;
             break;
         case SQLCSHORT :
             type = SQL_C_SSHORT;
             break;
         case SQLCLONG :
             type = SQL_C_SLONG;
             break;
         case SQLCDOUBLE :
             type = SQL_C_DOUBLE;
             break;
         case SQLCDATE :
             type = SQL_C_DATE;
             break;
     }

     Cursor = HstmtTab[cursor];
     ret = SQLGetData(Cursor, colpos, type, value, len, &cbDesc);
     if (ret < 0)
	 {
	                   GetError (Cursor);
					   return (-1);
	 }
     if (type == SQL_C_SHORT && *(short *) value == ShortNull)
     {
         *(short *) value = 0;
     }
     else if (type == SQL_C_LONG && *(long *) value == LongNull)
     {
         *(long *) value = 0l;
     }
     else if (type == SQL_C_DOUBLE && *(double *) value == DoubleNull)
     {
         *(double *) value = 0.0;
     }
     return (0);
}

int close_sql (int cursor)
/**
Speicherbereiche fuer sqlerte freigeben.
**/
{
	 HSTMT Cursor;

     sqlstatus = 0;
     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
        sqlstatus = -1;
         return -1;
     }

     Cursor = HstmtTab[cursor];
     SQLFreeStmt(Cursor, SQL_DROP);
	 CursTab[cursor] = 0;
     return (0);
}

int close_sqlconnect (int cursor)
{
	 HSTMT Cursor;
	 HDBC hdbc;
	 
     Cursor = HstmtTab[cursor];
	 if (close_sql (cursor) == -1)
	 {
		 return (-1);
	 }
	 hdbc = GetConnection (Cursor);
	 if (hdbc == NULL)
	 {
		 return (0);
	 }
	 DropConnection (hdbc);
	 SQLDisconnect(hdbc);
	 SQLFreeConnect(hdbc);
	 return (0);
}




int close_all_cursor ()
/**
alle Cursor schliessen.
**/
{
       int i;

       for (i = 0; i < MAXCURS; i ++)
       {
		   if (CursTab[i] > 0)
		   {
               close_sql (i);
		   }
       }
       return (0);
}

void out_quest (void *var, int typ, int len)
{
     if (OutAnz == MAXVARS)
     {
         return;
     }

     OutVars[OutAnz].var = var;
     OutVars[OutAnz].len = len;
     OutVars[OutAnz].typ = typ;
     OutAnz ++;
}


void ins_quest (void *var, int typ, int len)
{
     if (InAnz == MAXVARS)
     {
         return;
     }

     InVars[InAnz].var = var;
     InVars[InAnz].len = len;
     InVars[InAnz].typ = typ;
     InAnz ++;
}

static SDWORD pcbValue;

int TestOut (HSTMT Cursor)
{
     int retcode;
	 int i;

     for (i = 0; i < OutAnz; i ++)
     {
         switch (OutVars[i].typ)
         {
                 case SQLCCHAR:
                        pcbValue = OutVars[i].len;  
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_CHAR,  
                                         (char *) OutVars[i].var, 
                                                  OutVars[i].len, 
                                                  &pcbValue); 
                        break;
                 case SQLCVCHAR:
                        pcbValue = OutVars[i].len;  
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_CHAR,  
                                         (char *) OutVars[i].var, 
                                                  OutVars[i].len, 
                                                  &pcbValue); 
                        break;
                 case SQLCSHORT:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_SHORT,  
                                         (short *) OutVars[i].var, 
                                                   sizeof (short), 
                                                   &pcbValue); 
                        break;
                 case SQLCLONG:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_LONG,  
                                         (long *) OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 
                        break;
                 case SQLCDOUBLE:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_DOUBLE,  
                                         (double *)  OutVars[i].var, 
                                                     sizeof (double), 
                                                     &pcbValue); 
                        break;
                 case SQLCDATE:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_DATE,  
                                         (long *)  OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 
                        break; 
                 case SQLCTIME:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_TIME,  
                                         (long *)  OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 
                        break; 
                 case SQLCTIMESTAMP:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_TIMESTAMP,  
                                         (long *)  OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 
                        break;
         }
     }
     OutAnz = 0;
     return retcode;
}


int TestIn (HSTMT Cursor)
{
     int retcode = 0;
	 int i;

     cbLen = SQL_NTS;
     for (i = 0; i < InAnz; i ++)
     {
         switch (InVars[i].typ)
         {
                 case SQLCCHAR:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_CHAR, SQL_CHAR, InVars[i].len, 0,  
                                         (char *) InVars[i].var, 
                                                  0, 
                                                  &cbLen); 
                        break;
                 case SQLCVCHAR:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_CHAR, SQL_VARCHAR, InVars[i].len, 0,  
                                         (char *) InVars[i].var, 
                                                  0, 
                                                  &cbLen); 
                        break;
                 case SQLCSHORT:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_SSHORT, SQL_SMALLINT, 0, 0,  
                                         (short *) InVars[i].var, 
                                                   sizeof (short), 
                                                   &cbLen); 
                        break;
                 case SQLCLONG:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_SLONG, SQL_INTEGER, 0, 0,  
                                         (long *) InVars[i].var, 
                                                   0, 
                                                   &cbLen); 
                        break;
                 case SQLCDOUBLE:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_DOUBLE, SQL_DOUBLE, 0, 0,   
                                         (double *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 
                        break;
                 case SQLCDATE:
     
					 retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_DATE, SQL_DATE, 0, 0,   
                                         (DATE_STRUCT *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 
                        break;
                 case SQLCTIME:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_TIME, SQL_TIME, 0, 0,   
                                         (TIME_STRUCT *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 
                        break;
                 case SQLCTIMESTAMP:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_TIMESTAMP, SQL_TIMESTAMP, 0, 0,   
                                         (TIMESTAMP_STRUCT *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 
                        break;
         }
     }
     InAnz = 0;
     return retcode;
}


void GetError (HSTMT Cursor)
{ 
     UCHAR szSqlState[512];
     SDWORD pfNativeError;
     UCHAR szErrorMsg [512]; 
	 char ErrText [512];

     int retcode = SQLError(henv, 
                             hdbc, 
                             Cursor, 
                             szSqlState, &pfNativeError,
                             szErrorMsg, 512, NULL);
     sprintf (SqlErrorText, "Error %ld\n%s", pfNativeError, szErrorMsg);

	 sqlstatus = -1;
     if (sql_mode != 1)
     {
             sprintf (ErrText, "Error %ld\n%s", pfNativeError, szErrorMsg);
             printf ("%s\n", ErrText);
     }
     if (pfNativeError == 0)
     {
             return;
     }
 
     if (sql_mode == 0)
     {
//            MessageBox (NULL, ErrText, "", MB_ICONERROR);
            printf ("%s\n", ErrText);
            strcpy (ErrText, (char *) szErrorMsg);
            (*SqlErrorProc) (pfNativeError, ErrText);
     }
}

BOOL ErrProc (SDWORD ErrStatus, char * ErrText)
{
#ifdef WIN32
    ExitProcess (ErrStatus);
#else
    exit (ErrStatus);
#endif
    return TRUE;
}

