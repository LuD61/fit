# Microsoft Developer Studio Generated NMAKE File, Based on dbinfo.dsp
!IF "$(CFG)" == ""
CFG=dbinfo - Win32 Debug
!MESSAGE No configuration specified. Defaulting to dbinfo - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "dbinfo - Win32 Release" && "$(CFG)" != "dbinfo - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "dbinfo.mak" CFG="dbinfo - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "dbinfo - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "dbinfo - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "dbinfo - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

!IF "$(RECURSE)" == "0" 

ALL : "..\fit\bin\dbinfo.exe"

!ELSE 

ALL : "..\fit\bin\dbinfo.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\dbinfo.obj"
	-@erase "$(INTDIR)\dbinfo.res"
	-@erase "$(INTDIR)\itemc.obj"
	-@erase "$(INTDIR)\listcl.obj"
	-@erase "$(INTDIR)\MO_DRAW.OBJ"
	-@erase "$(INTDIR)\MO_MELD.OBJ"
	-@erase "$(INTDIR)\STDFKT.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\wmaskc.obj"
	-@erase "..\fit\bin\dbinfo.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /ML /W3 /GX /I "d:\informix\incl\esql" /D "NDEBUG" /D "WIN32"\
 /D "_WINDOWS" /D "DBN" /Fp"$(INTDIR)\dbinfo.pch" /YX /Fo"$(INTDIR)\\"\
 /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\dbinfo.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\dbinfo.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib comctl32.lib isqlt07c.lib /nologo /subsystem:windows\
 /incremental:no /pdb:"$(OUTDIR)\dbinfo.pdb" /machine:I386\
 /out:"\user\fit\bin\dbinfo.exe" /libpath:"d:\informix\lib" 
LINK32_OBJS= \
	"$(INTDIR)\dbinfo.obj" \
	"$(INTDIR)\dbinfo.res" \
	"$(INTDIR)\itemc.obj" \
	"$(INTDIR)\listcl.obj" \
	"$(INTDIR)\MO_DRAW.OBJ" \
	"$(INTDIR)\MO_MELD.OBJ" \
	"$(INTDIR)\STDFKT.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\wmaskc.obj" \
	".\dbasc.lib" \
	".\inflib.lib"

"..\fit\bin\dbinfo.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "dbinfo - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\dbinfo.exe"

!ELSE 

ALL : "$(OUTDIR)\dbinfo.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\dbinfo.obj"
	-@erase "$(INTDIR)\dbinfo.res"
	-@erase "$(INTDIR)\itemc.obj"
	-@erase "$(INTDIR)\listcl.obj"
	-@erase "$(INTDIR)\MO_DRAW.OBJ"
	-@erase "$(INTDIR)\MO_MELD.OBJ"
	-@erase "$(INTDIR)\STDFKT.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(INTDIR)\wmaskc.obj"
	-@erase "$(OUTDIR)\dbinfo.exe"
	-@erase "$(OUTDIR)\dbinfo.ilk"
	-@erase "$(OUTDIR)\dbinfo.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /D\
 "_DEBUG" /D "DBN" /D "WIN32" /D "_WINDOWS" /Fp"$(INTDIR)\dbinfo.pch" /YX\
 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\dbinfo.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\dbinfo.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib comctl32.lib isqlt07c.lib /nologo /subsystem:windows\
 /incremental:yes /pdb:"$(OUTDIR)\dbinfo.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)\dbinfo.exe" /pdbtype:sept /libpath:"d:\informix\lib" 
LINK32_OBJS= \
	"$(INTDIR)\dbinfo.obj" \
	"$(INTDIR)\dbinfo.res" \
	"$(INTDIR)\itemc.obj" \
	"$(INTDIR)\listcl.obj" \
	"$(INTDIR)\MO_DRAW.OBJ" \
	"$(INTDIR)\MO_MELD.OBJ" \
	"$(INTDIR)\STDFKT.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\wmaskc.obj" \
	".\dbasc.lib" \
	".\inflib.lib"

"$(OUTDIR)\dbinfo.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(CFG)" == "dbinfo - Win32 Release" || "$(CFG)" == "dbinfo - Win32 Debug"
SOURCE=.\dbinfo.cpp

!IF  "$(CFG)" == "dbinfo - Win32 Release"

DEP_CPP_DBINF=\
	".\comcthlp.h"\
	".\inflib.h"\
	".\itemc.h"\
	".\listcl.h"\
	".\mo_draw.h"\
	".\mo_intp.h"\
	".\mo_meld.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\dbinfo.obj" : $(SOURCE) $(DEP_CPP_DBINF) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "dbinfo - Win32 Debug"

DEP_CPP_DBINF=\
	".\comcthlp.h"\
	".\inflib.h"\
	".\itemc.h"\
	".\listcl.h"\
	".\mo_draw.h"\
	".\mo_intp.h"\
	".\mo_meld.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\dbinfo.obj" : $(SOURCE) $(DEP_CPP_DBINF) "$(INTDIR)"


!ENDIF 

SOURCE=.\dbinfo.rc
DEP_RSC_DBINFO=\
	".\comcthlp.h"\
	".\mo_meld.h"\
	".\oarrow.cur"\
	".\Toolbar.bmp"\
	

"$(INTDIR)\dbinfo.res" : $(SOURCE) $(DEP_RSC_DBINFO) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\itemc.cpp
DEP_CPP_ITEMC=\
	".\itemc.h"\
	

"$(INTDIR)\itemc.obj" : $(SOURCE) $(DEP_CPP_ITEMC) "$(INTDIR)"


SOURCE=.\listcl.cpp

!IF  "$(CFG)" == "dbinfo - Win32 Release"

DEP_CPP_LISTC=\
	".\comcthlp.h"\
	".\inflib.h"\
	".\itemc.h"\
	".\listcl.h"\
	".\mo_draw.h"\
	".\mo_intp.h"\
	".\mo_meld.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\listcl.obj" : $(SOURCE) $(DEP_CPP_LISTC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "dbinfo - Win32 Debug"

DEP_CPP_LISTC=\
	".\comcthlp.h"\
	".\inflib.h"\
	".\itemc.h"\
	".\listcl.h"\
	".\mo_draw.h"\
	".\mo_intp.h"\
	".\mo_meld.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\listcl.obj" : $(SOURCE) $(DEP_CPP_LISTC) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_DRAW.CPP

!IF  "$(CFG)" == "dbinfo - Win32 Release"

DEP_CPP_MO_DR=\
	".\itemc.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\MO_DRAW.OBJ" : $(SOURCE) $(DEP_CPP_MO_DR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "dbinfo - Win32 Debug"

DEP_CPP_MO_DR=\
	".\itemc.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\MO_DRAW.OBJ" : $(SOURCE) $(DEP_CPP_MO_DR) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_MELD.CPP

!IF  "$(CFG)" == "dbinfo - Win32 Release"

DEP_CPP_MO_ME=\
	".\comcthlp.h"\
	".\itemc.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\stdfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\MO_MELD.OBJ" : $(SOURCE) $(DEP_CPP_MO_ME) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "dbinfo - Win32 Debug"

DEP_CPP_MO_ME=\
	".\comcthlp.h"\
	".\itemc.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\stdfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\MO_MELD.OBJ" : $(SOURCE) $(DEP_CPP_MO_ME) "$(INTDIR)"


!ENDIF 

SOURCE=.\STDFKT.CPP

!IF  "$(CFG)" == "dbinfo - Win32 Release"

DEP_CPP_STDFK=\
	".\itemc.h"\
	".\mo_draw.h"\
	".\stdfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\STDFKT.OBJ" : $(SOURCE) $(DEP_CPP_STDFK) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "dbinfo - Win32 Debug"

DEP_CPP_STDFK=\
	".\itemc.h"\
	".\mo_draw.h"\
	".\stdfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\STDFKT.OBJ" : $(SOURCE) $(DEP_CPP_STDFK) "$(INTDIR)"


!ENDIF 

SOURCE=.\STRFKT.CPP
DEP_CPP_STRFK=\
	".\strfkt.h"\
	

"$(INTDIR)\STRFKT.OBJ" : $(SOURCE) $(DEP_CPP_STRFK) "$(INTDIR)"


SOURCE=.\wmaskc.cpp

!IF  "$(CFG)" == "dbinfo - Win32 Release"

DEP_CPP_WMASK=\
	".\comcthlp.h"\
	".\itemc.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\wmaskc.obj" : $(SOURCE) $(DEP_CPP_WMASK) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "dbinfo - Win32 Debug"

DEP_CPP_WMASK=\
	".\comcthlp.h"\
	".\itemc.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\wmaskc.obj" : $(SOURCE) $(DEP_CPP_WMASK) "$(INTDIR)"


!ENDIF 


!ENDIF 

