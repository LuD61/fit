#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "stringwork.h" 
#include "dlg.h" 


char **StringWork::Felder = NULL;
int StringWork::oldFieldAnz = 0;


char **StringWork::GetFelder (void)
{
                     return Felder;
}      


StringWork::StringWork (char *String) 

{
     char *buffer;
     char *p, *p1;
     char z;
     int i;
     int Row = 1;
     int TextCol = 1;
//     int anz;
     int updanz = 0;
     int selanz = 0;
     int keyanz = 0;
     char datum [12];
	 BOOL NewFields = FALSE;

     Texte   = NULL;
//     Felder  = NULL;   
     Namen   = NULL;   
     Feldnamen = NULL;
     Rows = NULL;
     Columns = NULL;
     Length = NULL;
     Textlen = 20;
     Wsize[0] = 60;
     Wsize[1] = 10;
     WinColor = GetSysColor (COLOR_3DFACE);

	 orgString = String;
     buffer = new char [strlen (String) + 1];
     strcpy (buffer, String); 

     headfieldanz = 0;
     Row = 1;
 
     p = buffer;
     p1 = p;
     fieldanz = 0; 
     while (TRUE)
     {  
         p = instring (p, "$#%", &z); 
         if (p == NULL) break;
         fieldanz ++;
         p += 1;
     }

	 fieldanz /= 2;
           
     Texte = new char *[fieldanz + 1];
     if (Texte == NULL) 
     {
                fieldanz = 0;
                return;
     }

	 if (fieldanz != oldFieldAnz)
	 {

           if (Felder != NULL)
		   {
                  for (i = 0; i < oldFieldAnz; i ++)
				  {
                          delete Felder [i];
				  }
                  delete Felder;
		          Felder = NULL;
		   }
	 }

	 if (Felder == NULL)
	 {
		   NewFields = TRUE;
           Felder = new char *[fieldanz + 1];
           if (Felder == NULL) 
		   {
                fieldanz = 0;
                return;
		   }
     }
     oldFieldAnz = fieldanz;
     Namen = new char *[fieldanz + 1];
     if (Namen == NULL) 
     {
                fieldanz = 0;
                return;
     }

     Feldnamen = new char *[fieldanz+ 1];

     Rows = new int [fieldanz + 1];
     Columns = new int [fieldanz + 1];
     Length = new int [fieldanz + 1]; 

     p = buffer;
     p1 = p;
	 i = 0;
     while (TRUE)
     {  
         p = instring (p, "$#%", &z); 
         if (p == NULL) break;
         Texte [i] = new char [256];
		 if (NewFields)
		 {
             Felder [i] = new char [256];
             if (z == '$')
             {
  		            strcpy (Felder [i], "*"); 
             }
             else if (z == '%')
             {
                    sysdate (datum); 
  		            strcpy (Felder [i], datum); 
             }
             else
             {
                    strcpy (Felder[i], "0");
             }
		 }
         Feldnamen [i] = new char [256];
         Namen [i] = new char [10]; 
         p += 1;
         getLabel (Texte [i], p);
		 strcpy (Feldnamen [i], ""); 
         Rows [i] = Row;
         Columns [i] = TextCol;
         sprintf (Namen[i], "field%d", i);
         Length[i] = 20;
         p += strlen (Texte [i]) + 1;
         i ++;
		 Row ++;
      } 
         
     fieldanz = i; 
     Texte [i] = NULL;
     Namen [i] = NULL;
     Felder [i] = NULL;
	 delete buffer;
}
         
StringWork::~StringWork ()
{
     int i;

     if (Texte != NULL)
     {
         for (i = 0; i < fieldanz; i ++)
         {
             delete Texte [i];
         }
         delete Texte;
		 Texte = NULL;
     }

/*	 
     if (Felder != NULL)
     {
         for (i = 0; i < fieldanz; i ++)
         {
             delete Felder [i];
         }
         delete Felder;
		 Felder = NULL;
     }
*/

     if (Namen != NULL)
     {
         for (i = 0; i < fieldanz; i ++)
         {
             delete Namen [i];
         }
         delete Namen;
		 Namen = NULL;
     }

     if (Feldnamen != NULL)
     {
         for (i = 0; i < fieldanz; i ++)
         {
             delete Feldnamen [i];
         }
         delete Feldnamen;
		 Feldnamen = NULL;
     }

     if (Rows !=NULL)
     {
         delete Rows;
         Rows = NULL;
     }

     if (Columns != NULL)
     {
         delete Columns;
         Columns = NULL;
     }
     if (Length != NULL)
     {
         delete Length;
         Length = NULL;
     }
}

char *StringWork::instring (char *s, char *cString, char *z)
{
     int i; 
     for (; *s != 0; s += 1)
     {
             for (i = 0; cString[i] != 0; i ++)
             {
                    if (*s == cString[i]) break;
             }
             if (cString[i] != 0)
             {
                    *z = cString[i];
                    return s;
             }
      }
      return NULL;
}         
     
void StringWork::getLabel (char *Label, char *p)
{
      int i;
 
      for (i = 0;*p != 0; i ++, p += 1) 
      {
            if (*p == '#' ||
                *p == '$' ||
                *p == '%')
            {
                 break;
            }
            Label [i] = *p;
       }
       Label[i] = 0;
}

char *StringWork::setFelder (char *newString, int len)  
{
	   char *p, *p1; 
	   char *newpos;
       char datum[12];
	   char z;


	   memset (newString, ' ', len);
	   newString [len] = 0;

	   p = orgString;
       p1 = p;
	   newpos = newString;
	   int l = 0;
       for (int i = 0; i < fieldanz; i ++)
	   {
           p = instring (p1, "$#%", &z); 
		   if (p != NULL)
		   {
			   l += (int) (p - p1);
               if (l > len) 
			   {
				   return NULL;
			   }
			   memcpy (newpos, p1, p - p1);
			   newpos += (p - p1);
			   if (z == '$')
			   {
 			       l += 2;
                   if (l > len) 
				   {
				         return NULL;
				   }
				   *newpos = '\"';
				   newpos += 1;
			   }
               else if (z == '%' && (strstr (Felder[i], "today") == 0 &&
                                     strstr (Felder[i], "TODAY") == 0 &&
                                     strstr (Felder[i], "Today") == 0))
               {
 			       l += 2;
                   if (l > len) 
				   {
				         return NULL;
				   }
				   *newpos = '\"';
				   newpos += 1;
               }

			   clipped (Felder[i]);
			   l += strlen (Felder[i]) + 1;
               if (l > len) 
			   {
				   return NULL;
			   }
               clipped (Felder[i]);
               if (strcmp (Felder[i], " ") <= 0)
               {
                   if (z == '$')
                   {
                       strcpy (Felder[i], "*");
                   }
                   else if (z == '%')
                   {
                       sysdate (datum);
                       strcpy (Felder[i], datum);
                   }
                   else
                   {
                       strcpy (Felder[i], "0");
                   }
               }

			   memcpy (newpos, Felder [i], strlen (Felder [i]));
			   newpos += strlen (Felder[i]);
			   if (z == '$')
			   {
				   *newpos = '\"';
				   newpos += 1;
			   }
               else if (z == '%' && (strstr (Felder[i], "today") == 0 &&
                                     strstr (Felder[i], "TODAY") == 0 &&
                                     strstr (Felder[i], "Today") == 0))
               {
				   *newpos = '\"';
				   newpos += 1;
			   }
		   }
		   else
		   {
			   len += strlen (p1);
               if (l > len) 
			   {
				   return NULL;
			   }
			   strcpy (newpos, p1);
			   break;
		   }
		   p1 = p + strlen (Texte[i]) + 2;
	   }
	   len += strlen (p1);
       if (l > len) 
	   {
		   return NULL;
       }
	   strcpy (newpos, p1);
	   return newString;
}


char *StringWork::ChangeSysdate (char *newString, char *orgString, int len)
{
       char datum [12];
       
       strcpy (newString, "");
       char *p1 = orgString;
       sysdate (datum);
       while (TRUE)
       {
           char *p = strstr (p1, "#SYSDATE#");
           if (p == NULL)
           {
                strcat (newString, p1);
                break;
           }

           memcpy (newString, p1, (int) (p - p1));
           newString [(int) (p -  p1)] = 0;
           strcat (newString, datum);
           p1 = p + strlen ("#SYSDATE#");
       }
       return newString;
}




     

                
            
                     
    
     