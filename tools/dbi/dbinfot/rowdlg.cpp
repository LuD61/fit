#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "RowDlg.h"
#include "mo_progcfg.h"
#include "help.h"
#include "mo_intp.h"
#ifdef BIWAK
#include "conf_env.h"
#endif


static int StdSize = STDSIZE;
static int InfoSize = 150;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont InfoFont = {
                         "ARIAL", InfoSize, 0, 0,
                         BLUECOL,
                         LTGRAYCOL,
                         10,
                         NULL};

static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


struct ROWCAPTF
{
      char row [8];
      char rowname [45];
      char rowbez [81];
      char rowsize [20]; 
      char rowpic [20]; 
}; 

static struct ROWCAPTF rowcaptf, rowcaptf_null;

mfont *RowDlg::Font = &dlgposfont;

CFIELD *RowDlg::_fHead [] = {
                     new CFIELD ("pos_frame", "",  45, 10, 1, 1 ,  NULL, "",
                                CBORDER,   
								500, Font, 0, TRANSPARENT),    
                     new CFIELD ("space", " ",  1, 1, 47, 1 ,  NULL, "",
                                CDISPLAYONLY,   
								500, Font, 0, TRANSPARENT),    
                     new CFIELD ("row_txt", "Spalte",  0, 0, 2, 2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("row", rowcaptf.row,  6, 0, 15, 2,  NULL, "%4d", CEDIT,
                                 ROW_CTL, Font, 0, ES_RIGHT),

                     new CFIELD ("row_namext", "Feldname",  0, 0, 2, 4,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("rowname", rowcaptf.rowname,  25, 0, 15, 4,  NULL, "", CEDIT,
                                 ROWNAME_CTL, Font, 0, 0),

                     new CFIELD ("row_beztxt", "Text",  0, 0, 2, 5,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("rowbez", rowcaptf.rowbez,  25, 0, 15, 5,  NULL, "", CEDIT,
                                 ROWBEZ_CTL, Font, 0, 0),

                     new CFIELD ("rowsize_txt", "Spaltenbreite",  0, 0,  2, 6,  NULL, "", 
					             CDISPLAYONLY,  
                                 500, Font, 0, 0),
                     new CFIELD ("rowsize", rowcaptf.rowsize,     4, 0, 15, 6,  NULL, "%2d", CEDIT,
                                 ROWSIZE_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("rowpic_txt", "Picture",  0, 0,  2, 7,  NULL, "", 
					             CDISPLAYONLY,  
                                 500, Font, 0, 0),
                     new CFIELD ("rowpic", rowcaptf.rowpic,     12, 0, 15, 7,  NULL, "", CEDIT,
                                 ROWPIC_CTL, Font, 0, 0),
                     NULL,
};

CFORM RowDlg::fHead (12, _fHead);



CFIELD *RowDlg::_fFoot [] = {
                     new CFIELD ("ok",      "    OK    ", 10, 0, 0, 0,  NULL, "", 
                                 CBUTTON,
                                 OK_CTL, Font, 0, 0),
                     new CFIELD ("cancel", " Abbruch  ", 10, 0,12, 0,  NULL, "", 
                                 CBUTTON,
                                 CANCEL_CTL, Font, 0, 0),
                     NULL,
};


CFORM RowDlg::fFoot (2, _fFoot);


CFIELD *RowDlg::_fRowCapt [] = {
                     new CFIELD ("fRowCapt1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   HEAD_CTL, Font, 0, 0),
                     new CFIELD ("fRowCapt3", (CFORM *) &fFoot, 0, 0, -1, -2, NULL, "", CFFORM,
                                   FOOT_CTL, Font, 0, 0),
                     NULL,                      
};

CFORM RowDlg::fRowCapt (2, _fRowCapt);

CFIELD *RowDlg::_fRowCapt0 [] = { 
                     new CFIELD ("fRowCapt", (CFORM *) &fRowCapt, 0, 0, -1, 0, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM RowDlg::fRowCapt0 (1, _fRowCapt0);

char *RowDlg::HelpName = "rowcapt.cmd";
RowDlg *RowDlg::ActiveMeEinh = NULL;



ItProg *RowDlg::After [] = {
//                                   new ItProg ("a",           ReadA),
//                                   new ItProg ("me_einh0",    GetPtab),
                                   NULL,
};

ItProg *RowDlg::Before [] = {
                                   NULL,
};

ItFont *RowDlg::Fonts [] = {
                                  NULL
};



char *RowDlg::EnableHead [] = {
                                 NULL
};





RowDlg::RowDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}


void RowDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             int i;
             int xfull, yfull;


             Rows = NULL;
             ActiveMeEinh = this; 
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        


//             ImageDelete.Image    = BMAP::LoadBitmap (hInstance, "DEL",       "DELMASK", SysBkColor);
//             ImageDelete.Inactive = BMAP::LoadBitmap (hInstance, "DELIA",     "DELMASK", SysBkColor);

             StdSize = Size;
             dlgfont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             InfoFont.FontHeight    = InfoSize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 InfoFont.FontHeight    = InfoSize - 30;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 InfoFont.FontHeight    = InfoSize - 50;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
             }

             fHead.SetFieldanz ();
             fFoot.SetFieldanz ();
             fRowCapt.SetFieldanz ();

             SetTmFont (&dlgfont);
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fRowCapt);
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fRowCapt);
             }

/*
             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; Fonts[i] != NULL; i ++)
                {
                     Fonts[i]->SetFont (&fRowCapt);
                }

                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; Fonts[i] != NULL; i ++)
                {
                   Fonts[i]->SetFont (&fRowCapt);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }
*/
      
             fFoot.GetCfield ("ok")->SetTabstop (FALSE); 
             fFoot.GetCfield ("cancel")->SetTabstop (FALSE); 

             SetDialog (&fRowCapt0);
             Pack ();
}


BOOL RowDlg::OnKeyReturn (void)
{
        HWND hWnd;
        int row;
        if (Rows == NULL) return FALSE;
        

        syskey = KEYCR;

        hWnd = GetFocus ();
        
        if (hWnd == fRowCapt.GetCfield ("row")->GethWnd ())
        {
            fHead.GetText ();
            row = atoi (rowcaptf.row);
            if (row < 1 || row > feld_anz) 
            {
                    disp_mess ("Spalten-Nummer ung�ltig", 2); 
                    SetCurrentName ("row"); 
                    return TRUE;
            }
            if (row > 0 &&  row < Rows->GetRowsize ())
            {
              if (Rows->GetCols () != NULL && 
                  Rows->GetCol (row - 1) != NULL)
              {
                    Rows->GetCol (row - 1)->SetColName (ziel[row - 1].feldname);
                    strcpy (rowcaptf.rowname, Rows->GetCol (row - 1)->GetColName ());
                    clipped (rowcaptf.rowname);
                    strcpy (rowcaptf.rowbez, Rows->GetCol (row - 1)->GetCaption ());
                    clipped (rowcaptf.rowbez);
					sprintf (rowcaptf.rowsize, "%d", Rows->GetCol (row - 1)->GetLength ()); 
					sprintf (rowcaptf.rowpic, "%s", Rows->GetCol (row - 1)->GetPicture ()); 
                    clipped (rowcaptf.rowpic);
                    fRowCapt.SetText ();
              }
              else
              {
                    strcpy (rowcaptf.rowname, ziel[row - 1].feldname);
                    clipped (rowcaptf.rowname);
                    strcpy (rowcaptf.rowbez,  "");
                    strcpy (rowcaptf.rowsize, "0");
                    strcpy (rowcaptf.rowpic, "");
                    fRowCapt.SetText ();
              }
            }
        }
        else if (hWnd == fRowCapt.GetCfield ("rowpic")->GethWnd ())
        {
            if (Rows->GetCols () != NULL)
            {
              fHead.GetText ();
              row = atoi (rowcaptf.row);
              if (row > 0 &&  row < Rows->GetRowsize ())
              {
                if (Rows->GetCol (row - 1) == NULL)
                {
                           Rows->SetCol (new ColProps (), row - 1);
                }
                clipped (rowcaptf.rowbez);
                Rows->GetCol (row - 1)->SetCaption (rowcaptf.rowbez);
                Rows->GetCol (row - 1)->SetLength  (atoi (rowcaptf.rowsize));
                Rows->GetCol (row - 1)->SetPicture  (rowcaptf.rowpic);
              }
            }
        }
        return FALSE;
}

BOOL RowDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}


BOOL RowDlg::OnKey5 ()
{

        syskey = KEY5;
        DestroyWindow ();
        return TRUE;
}

BOOL RowDlg::OnKey12 ()
{
        int row;

        syskey = KEY12;

        fHead.GetText ();
        if (Rows->GetCols () != NULL)
        {
              row = atoi (rowcaptf.row);
              if (row > 0 &&  row < Rows->GetRowsize ())
              {
                if (Rows->GetCol (row - 1) == NULL)
                {
                           Rows->SetCol (new ColProps (), row - 1);
                }
                clipped (rowcaptf.rowbez);
                Rows->GetCol (row - 1)->SetCaption (rowcaptf.rowbez);
                Rows->GetCol (row - 1)->SetLength  (atoi (rowcaptf.rowsize));
                Rows->GetCol (row - 1)->SetPicture  (rowcaptf.rowpic);
              }
        }
        
        DestroyWindow ();
        return TRUE;
}


BOOL RowDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}


BOOL RowDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }

              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL RowDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL RowDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             if (NextWindow != NULL)
             {
                     EnableWindows (NextWindow, TRUE);
                     SetWindowPos (NextWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
             }
             PostQuitMessage (0);
             fRowCapt.destroy ();
        }
        return TRUE;
}

void RowDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}


void RowDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void RowDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void RowDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}


/*
HWND RowDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
          return hWnd;
}

HWND RowDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar ('|');
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
          return hWnd;
}
*/

void RowDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

/*
void RowDlg::CallInfo (void)
{
          DLG::CallInfo ();
}
*/







