#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "wmaskc.h"
#include "colbut.h"
#include "dbclass.h"
#include "cmask.h"
#include "mo_txt.h"
#include "lbox.h"

#define BuOk     1001
#define BuCancel 1002
#define LIBOX    1003
#define BuDial   1004
#define ID_LABEL 1005
#define ID_EDIT  1006 
#define ID_EDIT0 1007 

#define ELEN 0x10000


static mfont mFont = {
                     "MS Sans Serif", 
                      100, 
                      0, 
                      0, 
                      BLACKCOL, 
                      LTGRAYCOL,
                      0, 0};



static BOOL ButtonDown = FALSE; 

static CFIELD **_MText0  = NULL;
static CFORM *MText0     = NULL;

static CFIELD **_MText   = NULL;
static CFORM *MText      = NULL;

static CFIELD **_fButton = NULL;
static CFORM *fButton    = NULL;

static CFIELD **_fWork   = NULL;
static CFORM *fWork      = NULL;


int MTXT::listpos = 1;
int MTXT::textidx = 0l;
long MTXT::aktnr = 0l;
char *MTXT::EBuff0;
char *MTXT::buffer = NULL;
char *MTXT::EBuff;
int (*MTXT::OkFunc) (int) = NULL;
int (*MTXT::OkFuncE) (char *) = NULL;
int (*MTXT::DialFunc) (int)  = NULL;
int (*MTXT::FillDb) (char *) = NULL;
int (*MTXT::Read) (long) = NULL;

char *MTXT::GetText (void)
{
	         return EBuff;
}

void MTXT::InitText (void)
{
	     EBuff[0] = 0;
		 textidx = 0;
}

char *MTXT::strtok (char *string, char *token)
{
	     static char *aktpos;
		 static char *newpos;
		 int len;

		 len = strlen (token);
		 if (string)
		 {
		        aktpos = string;
		 }
	     else
		 {
			    if (*newpos == 0) return NULL;
		        aktpos = newpos;
		 }
		 newpos = aktpos;
		 for (newpos; *newpos; newpos += 1)
		 {
			 if (memcmp (newpos, token, len) == 0) break;
		 }
		 if (*newpos == 0) return aktpos;
		 *newpos = 0;
		 newpos += strlen (token);
		 return aktpos;
}


char *MTXT::FirstRow (char *row, int maxlen)
{
	     int len;
		 char *p;

		 if (buffer) delete (buffer);
		 len = strlen (EBuff);

		 buffer = new char [len + 2];
		 if (buffer == NULL) return NULL;

		 strcpy (buffer, EBuff);
		 p = strtok (buffer, "\n");
		 if (p && row)
		 {
			 strncpy (row, p, maxlen);
			 row[maxlen] = 0;
			 return row;
		 }
		 return p;
}

char *MTXT::NextRow (char *row, int maxlen)
{
		 char *p;


		 p = strtok (NULL, "\n");
		 if (p && row)
		 {
			 strncpy (row, p, maxlen);
			 row[maxlen] = 0;
			 return row;
		 }
		 return p;
}

void MTXT::AddText (char *text)
{
	     if (textidx > 0)
		 {
			 strcat (EBuff, "\n");
		 }
		 strcat (EBuff, text);
		 textidx ++;
}

void MTXT::ReplaceText (char *text, BOOL mode)
{
	      strcpy (EBuff,text);
		  if (mode)
		  {
                      SetText ();
		  }
}


void MTXT::SetText (void)
{
	     if (fWork->GethWndID (ID_EDIT))
		 {
                     SetWindowText (fWork->GethWndID (ID_EDIT), EBuff);
		 }
}

void MTXT::SetTextNr (char *text_nr)
{
         EBuff0 = text_nr;
         fWork->GetCfield ("MText0")->SetFeld (EBuff0);
}


char *MTXT::GetTextNr (void)
{
         return EBuff0;
}


void MTXT::SetRead (int (*Read) (long))
{
	       this->Read = Read;
}

int MTXT::SaveTxt (void)
{
          aktnr = atol (EBuff0);
		  return 0;
}

int MTXT::ReadTxt (void)
{
          aktnr = atol (EBuff0);

	      if (Read)
		  {
			  (*Read) (aktnr);
		  }
		  return 0;
}
	           
	 
MTXT::MTXT (int cx, int cy, char *Label, char *Label0, char *Text, int (*Read) (long))
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;

 	        this->Font = &mFont;  

			aktnr = 0l;
			this->Read = Read;
			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;
            x = strlen (Label) + 4; 
			EBuff = new char [ELEN];
			strcpy (EBuff, Text);

			_MText0     = new CFIELD * [3];
 		    _MText0[0] = new CFIELD ("Capt", "",
				                     60 + x - 2, 3, 1 , 0, NULL, "",
									             CCAPTBORDER,
												 900, Font, 0, TRANSPARENT);
			_MText0[0]->Setchsize (2, 0);
			_MText0[1]  = new CFIELD ("Label0", Label0,  
				                      0, 0, 2, 1, NULL, "", 
									             CREADONLY,
												 ID_LABEL, Font, 0, TRANSPARENT);
			_MText0[2]  = new CFIELD ("MText0", EBuff0,  
				                     9, 0, x, 1, NULL, "%8d", 
									             CEDIT,
												 ID_EDIT0, Font, 0, 
												 ES_MULTILINE | ES_RIGHT,
												 SaveTxt,
												 ReadTxt);
			 MText0 = new CFORM (3, _MText0);

			_MText     = new CFIELD * [2];
			_MText[0]  = new CFIELD ("Label", Label,  
				                      0, 0, 2,  4, NULL, "", CDISPLAYONLY,
												 ID_LABEL, Font, 0, 0);
			_MText[1]  = new CFIELD ("MText", EBuff,  
				                     60, 11, x, 4, NULL, "", CRIEDIT,
												 ID_EDIT, Font, 0, 
												 ES_MULTILINE | ES_AUTOHSCROLL
												 | ES_AUTOVSCROLL 
												 | WS_HSCROLL | WS_VSCROLL);
			_MText[1]->Setchsize (2, 2);
			 MText = new CFORM (2, _MText);

			x = 0;
			y = (cy - 2) * tm.tmHeight;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
			
			_fButton    = new CFIELD * [2];
			_fButton[0] = new CFIELD ("OK", "OK", cx, cy, x * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[1] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x + 12 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (2, _fButton);
			fButton->Setchpos (0, 2);

            _fWork    = new CFIELD * [3];
			_fWork[0] = new CFIELD ("MTextC0", MText0, 0, 0, 0, 0, NULL, "", 
				                                 CFFORM,
//                                                 CREMOVED,
				                                 983, Font, 0, 0);
			_fWork[1] = new CFIELD ("MTextC1", MText, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 982, Font, 0, 0);
			_fWork[2] = new CFIELD ("Button", fButton, 0, 0, -1, 0, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (3, _fWork);
}


MTXT::MTXT (int cx, int cy, char *Label,  char *Text, int (*Read) (long))
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;

 	        this->Font = &mFont;  

			aktnr = 0l;
			this->Read = Read;
			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;
            x = strlen (Label) + 4; 
			EBuff = new char [ELEN];
			strncpy (EBuff, Text, ELEN);

			_MText     = new CFIELD * [2];
			_MText[0]  = new CFIELD ("Label", Label,  
				                      0, 0, 2,  2, NULL, "", CDISPLAYONLY,
												 ID_LABEL, Font, 0, 0);
			_MText[1]  = new CFIELD ("MText", EBuff,  
				                     60, 12, x, 2, NULL, "", CRIEDIT,
												 ID_EDIT, Font, 0, 
												 ES_MULTILINE | ES_AUTOHSCROLL
												 | ES_AUTOVSCROLL 
												 | WS_HSCROLL | WS_VSCROLL);
			_MText[1]->Setchsize (2, 2);
			 MText = new CFORM (2, _MText);

			x = 0;
//			y = (cy - 2) * tm.tmHeight;
			y = 0;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
			
			_fButton    = new CFIELD * [2];
			_fButton[0] = new CFIELD ("OK", "OK", cx, cy, x * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[1] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x + 12 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (2, _fButton);
			fButton->Setchpos (0, 0);

            _fWork    = new CFIELD * [2];
			_fWork[0] = new CFIELD ("MText0", MText, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 982, Font, 0, 0);
			_fWork[1] = new CFIELD ("Button", fButton, 0, 0, -1, -2, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (2, _fWork);
}

MTXT::MTXT (int cx, int cy, char *Label,  char *Text, int (*Read) (long), mfont *mFont)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;

 	        this->Font = mFont;  

			aktnr = 0l;
			this->Read = Read;
			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;
            x = strlen (Label) + 4; 
			EBuff = new char [ELEN];
			strncpy (EBuff, Text, ELEN);

			_MText     = new CFIELD * [2];
			_MText[0]  = new CFIELD ("Label", Label,  
				                      0, 0, 2,  2, NULL, "", CDISPLAYONLY,
												 ID_LABEL, Font, 0, 0);
			_MText[1]  = new CFIELD ("MText", EBuff,  
				                     60, 12, x, 2, NULL, "", CRIEDIT,
												 ID_EDIT, Font, 0, 
												 ES_MULTILINE | ES_AUTOHSCROLL
												 | ES_AUTOVSCROLL 
												 | WS_HSCROLL | WS_VSCROLL);
			_MText[1]->Setchsize (2, 2);
			 MText = new CFORM (2, _MText);

			x = 0;
//			y = (cy - 2) * tm.tmHeight;
			y = 0;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
			
			_fButton    = new CFIELD * [2];
			_fButton[0] = new CFIELD ("OK", "OK", cx, cy, x * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[1] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x + 12 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (2, _fButton);
			fButton->Setchpos (0, 0);

            _fWork    = new CFIELD * [2];
			_fWork[0] = new CFIELD ("MText0", MText, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 982, Font, 0, 0);
			_fWork[1] = new CFIELD ("Button", fButton, 0, 0, -1, -2, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (2, _fWork);
}

MTXT::~MTXT ()
{
	        int i;  
	 
	        fWork->destroy (); 
			if (MText0)
			{
				for (i = 0; i < MText0->GetFieldanz (); i ++)
				{
			              if (MText0->GetCfield ()[i]->GetAttribut () != CFFORM)
						  {
			                        delete MText0->GetCfield () [i];
						  }
				}
				delete MText0->GetCfield ();
		        delete MText0;
				MText0 = NULL;
			}
			if (MText)
			{
				for (i = 0; i < MText->GetFieldanz (); i ++)
				{
			              if (MText->GetCfield ()[i]->GetAttribut () != CFFORM)
						  {
			                        delete MText->GetCfield () [i];
						  }
				}
				delete MText->GetCfield ();
		        delete MText;
				MText = NULL;
			}
			if (fButton)
			{
				for (i = 0; i < fButton->GetFieldanz (); i ++)
				{
			              if (fButton->GetCfield ()[i]->GetAttribut () != CFFORM)
						  {
			                        delete fButton->GetCfield () [i];
						  }
				}
				delete fButton->GetCfield ();
		        delete fButton;
				fButton = NULL;
			}

			if (fWork)
			{
				for (i = 0; i < fWork->GetFieldanz (); i ++)
				{
			              if (fWork->GetCfield ()[i]->GetAttribut () != CFFORM)
						  {
			                        delete fWork->GetCfield () [i];
						  }
				}
				delete fWork->GetCfield ();
		        delete fWork;
				fWork = NULL;
			}

/*
			delete _MText0[0];   
            delete _MText0[1];   
            delete _MText0[2];   
            delete _MText0;   
            delete MText0;   

            delete _MText[0];   
            delete _MText[1];   
            delete _MText;   
            delete MText;   


			delete _fButton[0];
			delete _fButton[1];
			delete fButton;

			delete _fWork[0];
			delete _fWork[1];
			delete _fWork[2];
			delete fWork;
*/
			delete EBuff;
			if (buffer)
			{
				delete buffer;
				buffer = NULL;
			}
}

void MTXT::SetOkFunc (int (*OkF) (int))
{
	       OkFunc = OkF;
}

void MTXT::SetOkFuncE (int (*OkF) (char *))
{
	       OkFuncE = OkF;
}

void MTXT::SetDialFunc (int (*DialF) (int))
{
	       DialFunc = DialF;
}

void MTXT::SetFillDb (int (*Fi) (char *))
{
	       FillDb = Fi;
}

void MTXT::SetCurrentID (DWORD ID)
{
	       if (fWork->SetCurrentID (ID))
		   {
			   currentfield = ID;
		   }
}

void MTXT::SetCurrentName (char *name)
{
	       if (fWork->SetCurrentName (name))
		   {
			   currentfield = fWork->GetID (name);
		   }
}

void MTXT::EnableID (DWORD ID, BOOL flag)
{
	       fWork->EnableID (ID, flag);
}

void MTXT::EnableName (char *name, BOOL flag)
{
	       fWork->EnableName (name, flag);
}

void MTXT::CheckID (DWORD ID, BOOL flag)
{
	       fWork->CheckID (ID, flag);
}

void MTXT::CheckName (char *name, BOOL flag)
{
	       fWork->CheckName (name, flag);
}

BOOL MTXT::TestButtons (HWND hWndBu)
{
	       if (hWndBu == fWork->GethWndID (BuOk))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuOk, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
	       if (hWndBu == fWork->GethWndID (BuCancel))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuCancel, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
		   return FALSE;
}
	       

BOOL MTXT::TestEdit (HWND hWndEd)
{
	       DWORD attr;
		   
           if (hWndEd == fWork->GethWndID (ID_EDIT0))
		   {
			   return TRUE;
		   }
		   attr = fWork->GetAttribut ();
		   if (attr == CEDIT || attr == CRIEDIT)
		   {

//  Hier koennen alternative Aktionen eingefuegt werden.

              return FALSE; 
		   }
		   return FALSE;
}



BOOL MTXT::ProcessMessages (void)
{
	      MSG msg;

		  fWork->SetCurrent (0);
		  fWork->SetFirstFocus ();
          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
			      if (msg.wParam == VK_TAB)
				  {

                      if (GetKeyState (VK_SHIFT) < 0)
                      {
                              syskey = KEYSTAB;
/*
							  if (msg.hwnd == fWork->GethWndID (ID_EDIT))
							  {
                                   fWork->PriorFormField (); 
							  }
							  else if (msg.hwnd == fWork->GethWndID (ID_EDIT0))
							  {
                                   fWork->PriorFormField (); 
							  }
							  else
							  {
                                   fWork->PriorField (); 
							  }
*/
                              if (msg.hwnd == fWork->GetCfield ("MText")->GethWnd ())
                              {
                                   fWork->SetCurrentName ("Cancel");
                              }
                              else if (msg.hwnd == fWork->GetCfield ("Cancel")->GethWnd ())
                              {
                                       fWork->SetCurrentName ("OK");
                              }
                              else if (msg.hwnd == fWork->GetCfield ("OK")->GethWnd ())
                              {
                                   fWork->SetCurrentName ("MText");
                              }
					  }
					  else
					  {
					          syskey = KEYTAB;
/*
							  if (msg.hwnd == fWork->GethWndID (ID_EDIT))
							  {
                                   fWork->NextFormField (); 
 						           GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, ELEN - 1);
							  }
							  else if (msg.hwnd == fWork->GethWndID (ID_EDIT0))
							  {
                                   fWork->NextFormField (); 
 						           GetWindowText (fWork->GethWndID (ID_EDIT0), EBuff0, 8);
							  }
							  else
							  {
                                   fWork->NextField (); 
							  }
*/
                              if (msg.hwnd == fWork->GetCfield ("MText")->GethWnd ())
                              {
                                   fWork->SetCurrentName ("OK");
                              }
                              else if (msg.hwnd == fWork->GetCfield ("OK")->GethWnd ())
                              {
                                       fWork->SetCurrentName ("Cancel");
                              }
                              else if (msg.hwnd == fWork->GetCfield ("Cancel")->GethWnd ())
                              {
                                   fWork->SetCurrentName ("MText");
                              }
					  }
					  continue;
				  }
				  else if (msg.wParam == VK_F5)
				  {
					  syskey = KEY5;
					  break;
				  }
				  else if (msg.wParam == VK_F12)
				  {
				      GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, ELEN -1);
					  syskey = KEY12;
					  break;
				  }
				  else if (msg.hwnd == fWork->GethWndName ("Liste"));
				  else if (msg.wParam == VK_DOWN)
				  {
					  syskey = KEYDOWN;
					  if (msg.hwnd == fWork->GethWndID (ID_EDIT0))
					  {
                                fWork->NextFormField (); 
 						        GetWindowText (fWork->GethWndID (ID_EDIT0), EBuff0, 8);
 					            continue;
					  }
					  else
					  {
                              if (msg.hwnd == fWork->GetCfield ("OK")->GethWnd ())
                              {
                                       fWork->SetCurrentName ("Cancel");
 					                   continue;
                              }
                              else if (msg.hwnd == fWork->GetCfield ("Cancel")->GethWnd ())
                              {
                                       fWork->SetCurrentName ("MText");
  					                   continue;
                              }
					  }
				  }
				  else if (msg.wParam == VK_UP)
				  {
					  syskey = KEYUP;
					  if (msg.hwnd == fWork->GethWndID (ID_EDIT0))
					  {
                              fWork->PriorFormField (); 
 					          continue;
					  }
					  else
					  {
                              if (msg.hwnd == fWork->GetCfield ("Cancel")->GethWnd ())
                              {
                                       fWork->SetCurrentName ("OK");
  					                   continue;
                              }
                              else if (msg.hwnd == fWork->GetCfield ("OK")->GethWnd ())
                              {
                                       fWork->SetCurrentName ("MText");
  					                   continue;
                              }
					  }
				  }
				  else if (msg.wParam == VK_RIGHT)
				  {
                      if (msg.hwnd == fWork->GetCfield ("OK")->GethWnd ())
                      {
                                       fWork->SetCurrentName ("Cancel");
                      }
                      else if (msg.hwnd == fWork->GetCfield ("Cancel")->GethWnd ())
                      {
                                   fWork->SetCurrentName ("MText");
                      }
                  }

				  else if (msg.wParam == VK_LEFT)
				  {
                      if (msg.hwnd == fWork->GetCfield ("Cancel")->GethWnd ())
                      {
                                       fWork->SetCurrentName ("OK");
                      }
                      else if (msg.hwnd == fWork->GetCfield ("OK")->GethWnd ())
                      {
                                   fWork->SetCurrentName ("MText");
                      }
                  }
				  else if (msg.wParam == VK_RETURN)
				  {
					  syskey = KEYCR;
					  if (TestButtons (msg.hwnd))
					  {
					           continue;
					  }
					  else if (TestEdit (msg.hwnd))
					  {
                                fWork->NextFormField (); 
 						        GetWindowText (fWork->GethWndID (ID_EDIT0), EBuff0, 8);
					            continue;
					  }
				  }
			  }
              TranslateMessage(&msg);
			  DispatchMessage(&msg);
		  }
		  if (syskey == KEY5) return FALSE;
		  return TRUE;
}


CALLBACK MTXT::CProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					fWork->display (hWnd, hdc);
                    EndPaint (hWnd, &ps);
					break;

              case WM_SIZE :
		            MoveWindow ();
					break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == BuOk)
                    {
						   syskey = KEYCR;

						   if (OkFuncE)
						   {
							   (*OkFuncE) (EBuff);
						   }

						   else

						   {
                                PostQuitMessage (0);
						   }
                    }
                    if (LOWORD (wParam) == BuDial)
                    {
						   syskey = KEYCR;
						   if (DialFunc)
						   {
							   (*DialFunc) (GetSel ());
						   }
                    }
                    else if (LOWORD (wParam) == BuCancel)
                    {
						   syskey = KEY5;
                           PostQuitMessage (0);
                    }
                    else if (LOWORD (wParam) == LIBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
						          syskey = KEYCR;
					 	          if (OkFunc)
								  {
							              (*OkFunc) (GetSel ());
								  }
						          else
								  {
                                           PostQuitMessage (0);
								  }
						   }
                           else if (HIWORD (wParam) == VK_RETURN)
                           {
						          syskey = KEYCR;
					 	          if (OkFunc)
								  {
							              (*OkFunc) (GetSel ());
								  }
						          else
								  {
                                           PostQuitMessage (0);
								  }
						   }
                           else if (HIWORD (wParam) == LBN_SELCHANGE)
                           {
		                          fWork->SetCurrent (listpos);
						   }
                           else if (HIWORD (wParam) == LBN_SETFOCUS)
                           {
							       SetOkFuncE (NULL);
						   }
                    }
                    else if (LOWORD (wParam) == ID_EDIT)
                    {
                           if (HIWORD (wParam) == EN_SETFOCUS)
                           {
							       SetWindowText (fWork->GethWndID (ID_EDIT), EBuff);
								   fWork->SetCurrentFieldID (ID_EDIT);
						   }
						   else if (HIWORD (wParam) == EN_KILLFOCUS)
						   {
							       GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, ELEN -1);
						   }

						   else if (HIWORD (wParam) == EN_CHANGE)
						   {
                                   GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, ELEN -1);
						   }
					}
                    else if (LOWORD (wParam) == ID_EDIT0)
                    {
                           if (HIWORD (wParam) == EN_SETFOCUS)
                           {
							       SetWindowText (fWork->GethWndID (ID_EDIT0), EBuff0);
								   fWork->SetCurrentFieldID (ID_EDIT0);
								   fWork->before (ID_EDIT0);
                                   SendMessage (fWork->GethWndID (ID_EDIT0), 
									            EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
						   }
						   else if (HIWORD (wParam) == EN_KILLFOCUS)
						   {
							       GetWindowText (fWork->GethWndID (ID_EDIT0), EBuff0, 8);
								   fWork->after (ID_EDIT0);
						   }

						   else if (HIWORD (wParam) == EN_CHANGE)
						   {
                                   GetWindowText (fWork->GethWndID (ID_EDIT0), EBuff0, 8);
						   }
					}
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void MTXT::InsertCaption (char *buffer)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);
		 
		 SendMessage (hWnd, LB_TITLE, -1,  
                                 (LPARAM) (char *) buffer);
}



void MTXT::InsertRecord (char *buffer)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

         SendMessage (hWnd, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) buffer);
}

void MTXT::UpdateRecord (char *buffer, int pos)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

         SendMessage (hWnd, LB_INSERTSTRING, pos,  
                                 (LPARAM) (char *) buffer);
}

void MTXT::GetText (char *buffer)
{
	     HWND hWnd;
		 int idx;

         hWnd = fWork->GethWndID (LIBOX);

	     idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
	     SendMessage (hWnd, LB_GETTEXT, idx, (LPARAM) buffer);
}

int MTXT::GetSel (void)
{
	     HWND hWnd;
		 int idx;

         hWnd = fWork->GethWndID (LIBOX);

	     idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
		 return idx;
}

void MTXT::SetSel (int idx)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

	     SendMessage (hWnd, LB_SETCURSEL, (WPARAM) idx, (LPARAM) 0l);
		 return;
}

void MTXT::VLines (char *vpos, int hlines)
{
	       HWND hWnd;

		   hWnd = fWork->GethWndName ("Liste");
           SendMessage (hWnd, LB_CAPTSIZE, 0,  
                                 (LPARAM) 130);
           SendMessage (hWnd, LB_VPOS, hlines,  
                                 (LPARAM) (char *) vpos);
}

void MTXT::DestroyWindow (void)
{
	       fWork->destroy ();
		   ::DestroyWindow (hWnd);
}
						
void MTXT::OpenWindow (HANDLE hInstance, HWND hMainWindow)
{
		  int x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  RECT rect, rect1;

		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
//		  SetColBorderM (TRUE);

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CTxt";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }


          int xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
          int yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

		  if (hMainWindow != NULL)
		  {
		           GetClientRect (hMainWindow, &rect);
		           GetWindowRect (hMainWindow, &rect1);
 		           x  = max (0, (rect.right - cx) / 2)  + rect1.left;
		           y  = max (0, (rect.bottom - cy) / 2) + rect1.top;
		  }
		  else
		  {
 		           x  = max (0, (xfull - cx) / 2);
		           y  = max (0, (yfull - cy) / 2);
		  }

          hWnd = CreateWindowEx (0, 
                                 "CTxt",
                                 "",
                                 WS_VISIBLE | WS_POPUP | WS_THICKFRAME | WS_CAPTION | 
								 WS_SYSMENU | WS_MINIMIZEBOX,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
		  ShowWindow (hWnd, SW_SHOWNORMAL);
		  UpdateWindow (hWnd);
          InvalidateRect (hWnd, NULL, TRUE);
}

void MTXT::MoveWindow (void)
{
//		  RECT rect;

/*
		  GetClientRect (hMainWindow, &rect);
          ::MoveWindow (hWnd, rect.left, rect.top, rect.right, rect.bottom, TRUE);
*/
		  fWork->MoveWindow ();
		  fButton->Invalidate (FALSE);
}

