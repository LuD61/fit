#ifndef _eListDef
#define _eListDef
#include "RowProps.h"

struct CHATTR
{
    char *feldname;
    int  UpdAttr;
    int  InsAttr;
};

struct ItAttr 
{
	  char name [21];
	  short attribut;
};

struct ItAfter 
{
	  char name [21];
	  char proc [21];
};

struct ItChoise
{
	  char name [21];
	  char proc [21];
	  int rows;
	  int columns;
	  int key;
	  char ActBuffer[21];
	  char ActCaption[21];
	  char ActVline [21];
	  char ListMode [21];
	  char vline [21];
	  char hline [21];
};

void SetComboMess (BOOL (*) (MSG *));

class ListClass
{
      private :
             static BOOL *fSort;
		     BOOL InvalidateFocusFrame; 
             char *eingabesatz;
             int NoScroll;
             BOOL SortEnabled;
             int lPagelen;
             int LineRow;
			 BOOL list_break;
             HWND    hwndTB;
             HWND    hTrack;
             HWND    vTrack;

             char frmrow [0x1000];
             PAINTSTRUCT aktpaint;

             BOOL IsArrow;
             HCURSOR oldcursor;
             HCURSOR arrow;
             HCURSOR aktcursor;
             int movfield; 
             int InMove;
			 RECT FocusRect;
             HFONT EditFont;
             HFONT ListhFont;
             mfont UbFont;
             mfont UbSFont;
             mfont SFont;
             mfont ListFont;
             mfont *AktFont;
             BOOL PrintBackground;
             BOOL QuerPrint;
             BOOL PrintRowCaptions;
             int InAppend;
             int LineColor;
             int NewRec;
             int paintallways;
			 ItAttr *ItemAttr;
             COLORREF Color;
             COLORREF BkColor;
             COLORREF MarkColor;
             COLORREF MarkBgColor;
             struct CHATTR *ChAttr;
			 long ScrollWait;
             void    ShowAktColumn (HDC, int, int, int);
             void    DisplayList (void);
             void (*DoBefore)  (char *);
             void (*DoAfter)  (char *);
             void (*DoChoise) (char *);
			 void (*SetKey9) (char *);
			 void (*PrintScrCaption) (char *);
             int ScrPageLen;
             BOOL StopPrint;
             int  sabs;
             BOOL PosPrintMode;
             BOOL PosPrint;
             static int pagelen;
             static int pagelen0;
             static int ublen;
             static int PageStdCols;
             static int PageStart;
             static int PageEnd;

      protected : 
             char **RowCaptions;
             RowProps *Rows;

      public :
             static BOOL ReadItemLabel;
             TEXTMETRIC tm;
			 double RowHeight;
			 int    RowHeightP;
			 int    UbHeight;
             int    ScrUbHeight;
			 int    UbRows;
             int SwRecs;
             char **SwSaetze;
             BOOL *MarkedRows;
             BOOL marked;
             char **LstSaetze;
             unsigned char *LstSatz;
             FELDER *LstZiel;
             int zlenS;
             int scrollpos;
             int printscrollpos;
             int scrollposS;
             int recanz;
             int PageView;
             BOOL PrintHdc;
             HANDLE  hMainInst;
             HWND    hMainWindow; 
             HWND    mamain2; 
             HWND    mamain3; 
             form UbForm;
             form PrintUbForm;
             static form DataForm;
             static form PrintForm;
             form LineForm;
             form *UbRow;
             form *DataRow;
             form *lineRow;
             int banz;
             int banzS;
			 int WithFocus;
			 int  AktRow;
			 int  SelRow;
			 int AktColumn;
			 int  AktRowS;
             int AktPage;
			 int AktColumnS;
             int FindRow;
             int FindColumn;
			 char AktItem [21];
			 char AktValue [80];
			 int  ListFocus;
			 int  ActiveEdit;
			 HWND FocusWindow;
             int RowMode;
             static int ColSort;
             int (*TestAppend) (void);

             void SetPosPrint (BOOL b)
             {
                 PosPrint = b;
             }

             void SetChAttr (struct CHATTR *cha)
             {
                 ChAttr = cha;
             }

			 char *GetAktItem (void)
			 {
				 return AktItem;
			 }

			 char *GetAktValue (void)
			 {
				 return AktValue;
			 }

			 void SetItAttr (ItAttr *IAttr)
			 {
				     ItemAttr = IAttr;
			 }

             void SetTestAppend (int (*Proc) (void))
             {
                 TestAppend = Proc;
             }

             ListClass ()
             {
                    SortEnabled = TRUE;
                    InvalidateFocusFrame = FALSE;
                    IsArrow = FALSE;
                    oldcursor = NULL;;
                    arrow = NULL;
                    aktcursor = LoadCursor (NULL, IDC_ARROW);
                    movfield = 0; 
                    InMove = 0;
					AktRow = 0;
					SelRow = 0;
					AktColumn = 0;
					AktRowS = 0;
					AktColumnS = 0;
					WithFocus = 1;
					recanz = 0;
					banz = 0;
					ListFocus = 1;
					ActiveEdit = 3;
					FocusWindow = NULL;
                    RowMode = 0;
                    LstZiel = NULL;
                    PageView = 0;
                    EditFont = NULL;
                    LineColor = 3;
                    Color = RGB (0, 0, 0);;
                    BkColor = RGB (255, 255, 255); 
                    MarkColor = RGB (255, 255, 255); 
                    MarkBgColor = RGB (0, 0, 128); 
                    NewRec = 0;
                    InAppend = 0;
                    ChAttr = NULL;
					ItemAttr = NULL;
					RowHeight = (double) 1;
                    RowHeightP = 1;
					UbHeight  = 1;
					UbRows  = 1;
                    ScrUbHeight = 0;
                    TestAppend = NULL;
                    paintallways = 0;
					DoAfter  = NULL;
					DoChoise = NULL;
					SetKey9  = NULL;
 			        PrintScrCaption = NULL;
					ScrollWait = 0;
                    sabs = 1;
                    marked = FALSE;
                    MarkedRows = NULL;
                    RowCaptions = NULL;
                    Rows = NULL;
                    PosPrint = FALSE;
                    PrintHdc = FALSE;
                    PrintBackground = TRUE;
                    QuerPrint = FALSE;
                    PrintRowCaptions = TRUE;
                    ScrPageLen = 0;
                    StopPrint = FALSE;
             }

             void SetScrPageLen (int ScrPageLen)
             {
                    this->ScrPageLen = ScrPageLen;
             }

             int GetScrPageLen (void)
             {
                     return ScrPageLen;
             }

             void SetPrintRowCaptions (BOOL b)
             {
                    PrintRowCaptions = b;
             }

             BOOL GetPrintRowCaptions (void)
             {
                    return PrintRowCaptions;
             }

             void SetPrintBackground (BOOL b)
             {
                    PrintBackground = b;
             }

             void SetQuerPrint (BOOL b)
             {
                    QuerPrint = b;
             }

             void SetRowCaptions (char **RowCaptions)
             {
                    this->RowCaptions = RowCaptions;
             }

             void SetRows (RowProps *Rows)
             {
                    this->Rows = Rows;
             }

             void SetMarkedRows (BOOL *MarkedRows)
             {
                 this->MarkedRows = MarkedRows;
             }

             void SetMarked (BOOL marked)
             {
                 this->marked = marked;
             }

             BOOL GetMarked (void)
             {
                 return marked;
             }

             void SetMarked (int pos, BOOL marked)
             {
                 if (MarkedRows == NULL) return;
                 if (pos >= recanz) return;
                 
                 MarkedRows [pos] = TRUE;
                 this->marked = marked;
             }

             void SetToMark (int pos)
             {
                 int i;

                 if (MarkedRows == NULL) return;
                 if (pos >= recanz) return;

                 for (i = 0; i < pos; i ++)
                 {
                     if (MarkedRows [i]) break;
                 }

                 for (; i <= pos; i ++)
                 {
                     SetMarked (i, TRUE);
                 }
             }

             void SwitchMarked (int pos)
             {
                 if (MarkedRows == NULL) return;
                 if (pos >= recanz) return;
                 
                 if (MarkedRows [pos] == FALSE)
                 {
                          MarkedRows [pos] = TRUE;
                          this->marked = marked;
                 }
                 else
                 {
                          MarkedRows [pos] = FALSE;
                 }
             }

             BOOL IsMarked (int pos)
             {
                 if (MarkedRows == NULL) return FALSE;
                 if (pos >= recanz) return FALSE;
                 return MarkedRows [pos];
             }

             BOOL IsMarked (void)
             {
                 int i;

                 for (i = 0; i < recanz; i ++)
                 {
                     if (IsMarked (i)) return TRUE;
                 }
                 return FALSE;
             }


             void InitMark (void)
             {
                 int i;
                 marked = FALSE;
                 if (MarkedRows == NULL) return;

                 for (i = 0; i < recanz; i ++)
                 {
                     MarkedRows[i] = FALSE;
                 }
             }

             void MarkAll (void)
             {
                 int i;
                 if (MarkedRows == NULL) return;

                 marked = TRUE;
                 for (i = 0; i < recanz; i ++)
                 {
                     MarkedRows[i] = TRUE;
                 }
             }


             void EnableSort (BOOL b)
             {
                    SortEnabled = b;
             }

             void SetSabs (int sabs)
             {
                    this->sabs = sabs;
             }

			 void SetScrollWait (long w)
			 {
				 ScrollWait = w;
			 }

			 void SetBefore (void (*DoBefore) (char *))
			 {
				 this->DoBefore = DoBefore;
			 }

			 void SetAfter (void (*DoAfter) (char *))
			 {
				 this->DoAfter = DoAfter;
			 }

			 void SetSetKey9 (void (*SetKey9) (char *))
			 {
				 this->SetKey9 = SetKey9;
			 }
 
			 void SetChoise (void (*DoChoise) (char *))
			 {
				 this->DoChoise = DoChoise;
			 }

			 void SetPrintScrCaption (void (*PrintScrCaption) (char *))
			 {
				 this->PrintScrCaption = PrintScrCaption;
			 }

			 void SetUbRows (int ur)
			 {
				      UbRows = ur;
			 }

			 void SetRowHeight (double rh)
			 {
				      RowHeight = rh;
					  RowHeightP = max (1, (int) (double) ((double) tm.tmHeight * rh));
			 }

             void SetLines (int LineNr)
             {
                     LineColor = LineNr;
             }

             void SetListFocus (int Focus)
             {
                     ListFocus = Focus;
					 if (ListFocus > 2)
					 {
						 ActiveEdit = ListFocus;
					 }
             }

             void SetListFont (mfont *lFont)
             {
				 CopyFonts (lFont);
             }
             BOOL IsNewRec (void)
             {
                 return NewRec;
             }

             BOOL IsAppend (void)
             {
                 return InAppend;
             }


             void SetPageView (int pg)
             {
                     PageView = pg;
             }

             void SetLstZiel (FELDER *zl)
             {
                     LstZiel = zl;
             }

             void SetRowMode (int mode)
             {
                    RowMode = mode;
             }

             void SetPos (int zeile, int spalte)
             {
					AktRow = zeile;
					AktColumn = spalte;
             }

             void Initscrollpos (void)
             {
                    scrollpos = 0;
             }

             void Setscrollpos (int pos)
             {
                 scrollpos = pos;
             }

             int Getscrollpos (void)
             {
                 return scrollpos;
             }


             void InitRecanz (void)
             {
                 recanz = 0;
             }

             int  GetRecanz (void)
             {
                 return recanz;
             }

             int  GetAktRow (void)
             {
                 return AktRow;
             }

             int  GetAktRowS (void)
             {
                 return AktRowS;
             }

             int  GetAktColumn (void)
             {
                 return AktColumn;
             }

             void SethwndTB (HWND hwndTB)
             {
                 this->hwndTB = hwndTB;
             }

             void SetSaetze (char **Saetze)
             {
                 SwSaetze = Saetze;
             }

			 char *GetSwSatz (int pos)
			 {
				 if (SwSaetze[pos])
				 {
				     return SwSaetze [pos];
				 }
				 return " ";
			 }

             void Setbanz (int banz)
             {
                 this->banz = banz;
             }

             void SetRecanz (int anz)
             {
                 recanz = anz;
             }

             void SetPagelen (int Pagelen)
             {
                 lPagelen = Pagelen;
             }

             void SetLineRow (int LineRow)
             {
                 this->LineRow = LineRow;
             }

             void SetTextMetric (TEXTMETRIC *ttm)
             {
                 memcpy (&tm, ttm, sizeof (tm));
             }

             void SetAktPaint (PAINTSTRUCT *pm)
             {
                 memcpy (&aktpaint, pm, sizeof (aktpaint));
             }

             HWND Getmamain2 (void)
             {
                  return mamain2;
             }

             HWND Getmamain3 (void)
             {
                  return mamain3;
             }

             HWND GethTrack (void)
             {
                  return  hTrack;
             }

             HWND GetvTrack (void)
             {
                  return  vTrack;
             }

             void    FormatField (char *);
             void    ShowAktRow (void);
             HWND     InitListWindow (HWND);
             void     GetPageLen (void);
             BOOL     TrackNeeded (void);
             void     CreateTrack (void);
             void     DestroyTrack (void); 

             BOOL     VTrackNeeded (void);
             void     CreateVTrack (void);
             void     DestroyVTrack (void);
             void     TestTrack (void);
             void     TestVTrack (void);
             void     TestMamain3 (void);
             void     MoveListWindow (void);
             void     SetDataStart (void);
             BOOL     MustPaint (int);
             void     PrintUSlinesSatzNr (HDC);
             void     PrintSlinesSatzNr (HDC);
             void     PrintVlineSatzNr (HDC);
             void     PrintVlines (HDC);
             void     PrintPVlines (HDC, RECT *);
             void     PrintVline (HDC, int);
             void     PrintHlinesSatzNr (HDC);
             void     PrintLastHline (HDC);
             void     PrintLastPHline (HDC);
             void     PrintHlines (HDC);
             void     PrintPHlines (HDC, RECT *);
             void     PrintHline (HDC, int);
			 void     ShowFocusText (HDC, int, int);
			 void     GetFocusText (void);
			 void     ShowWindowText (int, int);
			 void     DestroyFocusWindow (void);
             void     SetFeldEdit0 (int, int);
             void     SetFocusFrame (HDC, int, int);
             void     SetFeldEdit (int, int);
             void     SetFeldEdit1 (int, int);
			 void     SetFeldFrame (HDC, int, int);
			 void     SetFeldFocus (HDC, int, int);
			 void     SetFeldFocus0 (int, int);
			 void     SetNoFocus (void);
			 void     SetFrameFocus (void);
			 void     SetRevFocus (void);
			 void     SetEditFocus (void);
			 void     SetEdit3DFocus (void);
             int      DelRec (void); 
             void     SetInsAttr (void);
             void     SetFieldAttr (CHATTR *);
#ifdef _OLDLIST
             void     FillFrmRow (char *, form *);
#else
             BOOL     PrintPosText (HDC, char *, int , int, SIZE *);
             BOOL     PrintLines (HDC, char *, int , int, SIZE *);
             void     FillFrmPosRow (HDC, char *, form *, int);
             void     FillFrmRow (HDC, char *, form *, int);
             void     FillUbPFrmRow (HDC, char *, form *, int);
             void     FillFrmScrUbRow (HDC, int);
#endif
             void     ShowFrmRow (HDC, int);
             void     ShowDataForms (HDC);
             void     ShowPrintForms (HDC);
             void     ScrollLeft (void);
             void     ScrollRight (void);
             void     ScrollPageDown (void);
             void     ScrollPageUp (void);
             void     SetPos (int);
             void     SetTop (void);
             void     SetBottom (void);
             void     HScroll (WPARAM, LPARAM);
             void     SetVPos (int);
             void     ScrollWindow0 (HWND, int, int, RECT *, RECT *);
             void     ScrollDown (void);
             void     ScrollUp (void);
             void     ScrollVPageDown (void);
             void     ScrollVPageUp (void);
             void     VScroll (WPARAM, LPARAM);
			 void     TestChoise (void);
			 void     FocusLeft (void);
			 void     FocusRight (void);
			 void     FocusUp (void);
			 void     FocusDown (void);
             void     ZielFormat (char *, FELDER *);
             BOOL     IsInListArea (MSG *);
             void     OnPaint (HWND, UINT, WPARAM, LPARAM);
             void     OnSize (HWND, UINT, WPARAM, LPARAM);
             void     OnHScroll (HWND, UINT, WPARAM, LPARAM);
             void     OnVScroll (HWND, UINT, WPARAM, LPARAM);
             void     FunkKeys (WPARAM, LPARAM);
             void     FillUbForm (field *, char *, int, int,int);
             void     FillPrintUb (char *dest);
             void     FillPrintRow (char *dest);
             void     FillPrPrintUb (char *dest);
             void     FillPrPrintRow (char *dest);
             void     FillDataForm (field *, char *, int, int);
             void     FillLineForm (field *, int);
             void     MakeLineForm (form *);
             int      GetItemAttr (char *);
             void     MakeDataForm (form *);
             void     MakePrintForm (form *);
			 int      getFontlen (int);
			 int      getDataFontlen (int);
             void     MakeUbForm (void);
             void     MakePrintUbForm (void);
             void     MakeDataForm0 (void);
             void     FreeLineForm (void);
             void     FreeDataForm (void);
             void     FreeUbForm (void);
             BOOL     IsMouseMessage (MSG *);
             void     StopMove (void);
             void     StartMove (void);
             void     MoveLine (void);
             void     DestroyField (int);
             void     ShowArrow (BOOL);
             int      IsUbEnd (MSG *);
             int      IsUbRow (MSG *);
             static   int SortCol0 (const void *, const void *);
             void     SortCol (int);
             BOOL     TestKeySort (int);
			 void     EditScroll (void);
             void     ToDlgRec (char *);
             void     FromDlgRec (char *);
             void     FreeDlgSaetze (void);
             void     SwitchRowMode (int mode);
             void     SwitchPage (int);
             void     SetPage (int);
             void     PriorPageRow (void);
             void     NextPageRow (void);
             void     SetFont (mfont *);
             void     CopyFonts (mfont *);
             void     ChoiseFont (mfont *);
             void     ChoiseLines (HDC);
             BOOL     InRec (char *, int *);
             void     find (char *);
             void     FindString (void);
             void     FindNext (void);
             BOOL     NewSearchString ();
             void     SetListLines (int); 
             void     BreakList (WPARAM);
             int      ProcessMessages(BOOL ende_mode);
             void     SetColors (COLORREF, COLORREF);
             int      AktCol (void);
             void     InsertLine (void);
             void     AppendLine (void);
             void     CopyLine (void);
             void     DeleteLine (void);
             int      FirstColumn (void);
             int      PriorColumn (void);
             int      NextColumn (void);
             int      AktColR (void);
             int      FirstColumnR (void);
             int      PriorColumnR (void);
             int      NextColumnR (void);
             void     SetNoRecNr (void);
             void     ShowSatzNr (HDC, int);
             int      GetMidPos (int);
             void     ToClipboard (void);
             void     FromClipboard (void);
             void     MarkedToClipboard (void);
             int      GetUbFrmlen (void);
             int      GetPageRows (char *, mfont *);
             void     PrintList ();
};

class ListClassDB : public ListClass
{
     public :
            ListClassDB () : ListClass ()
            {
            }
            void     FreeBezTab (void);
            void     FillUbForm (field *, char *, int, int,int);
            void     MakeUbForm (void);
            void     SwitchPage (int);
};

#endif 
