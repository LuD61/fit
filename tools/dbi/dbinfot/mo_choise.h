#ifndef _MO_CHOISE_DEF
#define _MO_CHOISE_DEF

class CHOISE
{
          private :
			  HWND hMainWindow;
			  HINSTANCE hInstance;
			  HWND hWnd;
              mfont *Font;
			  DWORD currentfield;
			  char *Caption;
			  DWORD style;
              int cx, cy;
          public :
			  CHOISE (int, int);
			  CHOISE (int, int, BOOL);
			  ~CHOISE ();
			  void SetCaption (char *Caption)
			  {
				  this->Caption = Caption;
			  }

			  void Setstyle (DWORD style)
			  {
				  this->style = style;
			  }

 		      void ProcessMessages (void);
              static void ShowDlg (HDC, form *);
              static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
              static void MoveWindow (void);
              void OpenWindow (HANDLE, HWND);
              void DestroyWindow (void);
			  void SetCurrentID (DWORD);
			  void SetCurrentName (char *);
			  void EnableID (DWORD, BOOL);
			  void EnableName (char *, BOOL);
			  void CheckID (DWORD, BOOL);
			  void CheckName (char *, BOOL);
              void VLines (char *, int);
			  void InsertCaption (char *);
			  void InsertRecord  (char *);
              void SetMenSelect (BOOL mode);
              void GetText (char *);
              int  GetSel (void);
              BOOL TestButtons (HWND);
};
#endif

