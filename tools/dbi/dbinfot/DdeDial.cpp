#include <windows.h>
#include <stdio.h>
// #include "stdafx.h"
#include "DdeDial.h"

//char *DdeDial::szServer = "CLIPDIAL";
//char DdeDial::szService[80] = {"CLIPDIAL"};
char *DdeDial::szServer = "G:\\Alcatel\\Uahpone\\Uaphone.exe";
char DdeDial::szService[80] = {"AIS2880"};
char DdeDial::szTopic[80] = {"VOICE"};
char DdeDial::szItem[80] = {"CALL"};

DdeDial::DdeDial ()
{
    idInst = NULL;

    if (DdeInitialize (&idInst, (PFNCALLBACK) &DdeCallback,
                       APPCLASS_STANDARD | APPCMD_CLIENTONLY, 0l))
    {
                  MessageBox (NULL, "Fehler beim Initialisieren vin DDE", "", 
                              MB_OK | MB_TASKMODAL);
    }

}

DdeDial::DdeDial (char *Server, char *Service, char *Topic, char *Item)
{
    idInst = NULL;

    if (DdeInitialize (&idInst, (PFNCALLBACK) &DdeCallback,
                       APPCLASS_STANDARD | APPCMD_CLIENTONLY, 0l))
    {
                  MessageBox (NULL, "Fehler beim Initialisieren vin DDE", "", 
                              MB_OK | MB_TASKMODAL);
    }

    szServer = Server;
    strcpy (szService, Service); 
    strcpy (szTopic, Topic); 
    strcpy (szItem, Item); 
}

void DdeDial::SetServer (char *Server)
{
    szServer = Server;
}

void DdeDial::SetService (char *Service)
{
    strcpy (szService, Service);
}

void DdeDial::SetTopic (char *Topic)
{
    strcpy (szTopic, Topic);
}

void DdeDial::SetItem (char *Item)
{
    strcpy (szItem, Item);
}

void DdeDial::Call (LPSTR PhoneNumber)
{
    char Buffer [256];

    hszService = DdeCreateStringHandle (idInst, szService, 0);
    hszTopic   = DdeCreateStringHandle (idInst, szTopic, 0);

    hConv = DdeConnect (idInst, hszService, hszTopic, NULL);

    if (hConv == NULL)
    {
        Exec (szServer, SW_SHOWMINNOACTIVE);
        Sleep (1000);
        hConv = DdeConnect (idInst, hszService, hszTopic, NULL);
    }

    if (hConv == NULL)
    {
        Sleep (1000);
        hConv = DdeConnect (idInst, hszService, hszTopic, NULL);
    }

    DdeFreeStringHandle (idInst, hszService);
    DdeFreeStringHandle (idInst, hszTopic);

    if (hConv == NULL)
    {
        sprintf (Buffer, "Connect to %s Failed", szService);
        MessageBox (NULL, Buffer, "", MB_OK | MB_TASKMODAL);
        return;
    }

    hszItem   = DdeCreateStringHandle (idInst, szItem, 0);
    DdeClientTransaction ((LPBYTE) PhoneNumber, strlen (PhoneNumber), hConv, hszItem,CF_TEXT,
                          XTYP_POKE, DDE_TIMEOUT, NULL);

    DdeFreeStringHandle (idInst, hszItem);

}

int DdeDial::Exec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  CloseHandle (pi.hProcess);
        }
        return ret;
}

int DdeDial::Exec (LPSTR prog, WORD SHOW_MODE)
{
        return Exec (prog, SHOW_MODE, -1, 0, -1, 0);
}

HDDEDATA CALLBACK DdeDial::DdeCallback (UINT iType, UINT iFmt, HCONV hConv,
                                        HSZ hsz1, HSZ hsz2, HDDEDATA hData,
                                        DWORD dwData1, DWORD dwData2)
{
    return NULL;
}

