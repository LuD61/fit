#ifndef _SEARCHCOLS_DEF
#define _SEARCHCOLS_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
// #include "dbclass.h"

struct SCOLUMN
{
	  char  colname [42];
      short coltype;
      short collength;
};

class HASH 
{
    private :
       int Key;
       char *Value; 
    public :
       HASH (int Key, char *Value)
       {
           this->Key = Key;
           this->Value = Value;
       }

       void Put (int Key, char *Value)
       {
           this->Key = Key;
           this->Value = Value;
       }

       char *GetValue (void)
       {
           return Value;
       }

       char *GetValue (int Key)
       {
           if (this->Key == Key)
           {
               return Value;
           }
           return NULL;
       }
};

class HASHTABLE
{
       private :
           HASH **Tab;
           int anz;
       public :
           HASHTABLE ()
           {
               Tab = new HASH * [512];
               memset (Tab, 0, 512 * sizeof (void *));
               anz = 0;
           }

           HASHTABLE (HASH Hashtab [])
           {
               int i;

               for (anz = 0; Hashtab[anz].GetValue () != NULL; anz ++);

               if (anz > 511) anz = 511;
            
               Tab = new HASH * [anz + 1];
               memset (Tab, 0, (anz + 1) * sizeof (void *));

               for (i = 0; i < anz; i ++)
               {
                   Tab[i] = &Hashtab[i];
               }
           }

           char *GetValue (int Key)
           {
               int i;

               for (i = 0; Tab[i] != NULL; i ++)
               {
                   if (Tab[i]->GetValue (Key) != NULL)
                   {
                       return Tab[i]->GetValue (Key);
                   }
               }
               return NULL;
           }
};

     

class SEARCHCOLS
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
//           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SCOLUMN *scoltab;
           static struct SCOLUMN scolumn;
           static int idx;
           static long anz;
           static CHQEX *Query;
           static HASH HashTab[];
           static HASHTABLE SqlTypes;

           char *table;
           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHCOLS ()
           {
                  table = NULL;
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHCOLS ()
           {
                  if (scoltab != NULL)
                  {
                      delete scoltab;
                      scoltab = NULL;
                  }
           }

           void SetTable (char *table)
           {
                  this->table = table;
           }

           SCOLUMN *GetSColumn (void)
           {
               if (idx == -1) return NULL;
               return &scolumn;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           static void FillFormat (char *);
           static void FillVlines (char *);
           static void FillCaption (char *);
           static void FillRec (char *, int);
           static int SearchLst (char *);
           static int Read (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
           static void UpdateList (void);

};  
#endif