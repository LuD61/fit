#ifndef _ROWPROPS_DEF
#define _ROWPROPS_DEF
#include "ColProps.h"

class RowProps
{
    protected :
        ColProps **colProps;
        int rowsize;
        int colanz;
    public :
        RowProps ()
        {
            colProps = NULL;
            colanz = 0;
            rowsize = 0;
        }

        RowProps (int rowsize)
        {
            colProps = new ColProps * [rowsize];
            ZeroMemory (colProps, rowsize * sizeof (ColProps *));
            colanz = 0;
            this->rowsize = rowsize;
        }

        RowProps (ColProps **colProps, int rowsize)
        {
            this->colProps = colProps;
            this->colanz   = 0;
            this->rowsize  = rowsize;
        }

        ColProps **GetCols (void)
        {
            return colProps;
        }

        void SetCols (ColProps **colProps, int rowsize)
        {
            this->colProps = colProps;
            this->rowsize  = rowsize;
        }

        void SetCol (ColProps * Col, int nr)
        {
            if (nr < rowsize)
            {
                 colProps [nr] = Col;
            }
        }

        ColProps *GetCol (int nr)
        {
            if (nr < rowsize)
            {
                return colProps [nr];
            }
            return NULL;
        }

        ColProps *GetCol (char *ColName)
        {
            clipped (ColName);
            for (int i = 0; i < rowsize; i ++)
            {
                if (colProps[i] != NULL && strcmp (colProps[i]->GetColName (), ColName) == 0)
                {
                    return colProps[i];
                }
            }
            return NULL;
        }

        int GetColanz (void)
        {
            return colanz;
        }

        int GetRowsize (void)
        {
            return rowsize;
        }

        void AddCol (ColProps *Col)
        {
            if (colProps == NULL)
            {
                rowsize = 100;
                colProps = new ColProps * [rowsize];
            }
            else if (colanz >= rowsize)
            {
                ColProps **colSave = new ColProps * [rowsize];
                memcpy (colSave, colProps, rowsize * sizeof (ColProps *));
                delete colProps;
                colProps = new ColProps * [rowsize * 2];
                memcpy (colProps, colSave, rowsize * sizeof (ColProps *));
                rowsize *= 2;
                delete colSave;
            }
            colProps[colanz] = Col; 
            colanz ++;
        }

        void PutCol (ColProps *Col, int colnr)
        {
            if (colProps == NULL)
            {
                rowsize = colnr + 100;
                colProps = new ColProps * [rowsize];
            }
            else if (colnr >= rowsize)
            {
                ColProps **colSave = new ColProps * [rowsize];
                memcpy (colSave, colProps, rowsize * sizeof (ColProps *));
                delete colProps;
                colProps = new ColProps * [colnr + 100];
                memcpy (colProps, colSave, rowsize * sizeof (ColProps *));
                rowsize = colnr + 100;
                delete colSave;
            }
            colProps[colnr] = Col; 
        }
};

#endif