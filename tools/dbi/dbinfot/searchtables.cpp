#include <windows.h>
#include "searchtables.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#ifdef ODBC
#include "mo_odbc.h"
#else
#ifndef BIWAK
#include "mo_curso.h"
#endif
#endif


struct STABLE *SEARCHTABLES::stabletab = NULL;
struct STABLE SEARCHTABLES::stable;
int SEARCHTABLES::idx = -1;
long SEARCHTABLES::anz;
CHQEX *SEARCHTABLES::Query = NULL;
// DB_CLASS SEARCHTABLES::DbClass; 
HINSTANCE SEARCHTABLES::hMainInst;
HWND SEARCHTABLES::hMainWindow;
HWND SEARCHTABLES::awin;


int SEARCHTABLES::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

void SEARCHTABLES::FillFormat (char *buffer)
{
         sprintf (buffer, "%s",
                          "%s");
}


void SEARCHTABLES::FillCaption (char *buffer)
{
	    sprintf (buffer, "     %-22s", " Tabelle"); 
}

void SEARCHTABLES::FillVlines (char *buffer) 
{
	    sprintf (buffer, "     %22s", "1"); 
}


void SEARCHTABLES::FillRec (char *buffer, int i)
{
 	      sprintf (buffer, "     %-20s", stabletab[i].tabname); 
}


void SEARCHTABLES::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < anz; i ++)
       {
          FillRec (buffer, i);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHTABLES::Read (char *tabname)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (stabletab) 
	  {
           delete stabletab;
           stabletab = NULL;
	  }


      idx = -1;
	  anz = 0;
      Query->SetSBuff ("");
      clipped (tabname);
      if (strcmp (tabname, " ") == 0) tabname[0] = 0;

#ifndef ODBC
      sprintf (buffer, "select count (*) from SYSTABLES where tabname matches \"%s*\"", tabname);
      out_quest ((char *) &anz, 2, 0);
	  execute_sql (buffer);
#endif

      if (anz == 0) anz = 0x10000;

	  stabletab = new struct STABLE [anz];

#ifndef ODBC
      if (strlen (tabname) == 0)
      {
	          sprintf (buffer, "select tabname "
	 			       "from systables "
                       "where tabname > \" z\" " 
                       "order by tabname");
      }
      else
      {
	          sprintf (buffer, "select tabname "
	 			       "from systables "
                       "where tabname matches \"%s*\" " 
                       "order by tabname", tabname);
      }
      out_quest ((char *) stable.tabname, 0, 21);
      cursor = prepare_sql (buffer);
#else
      cursor = prepare_tables (clipped (tabname));
//      cursor = prepare_tables ("*");
	  bind_sqlcol (cursor, 3, (char *) stable.tabname,  SQLCCHAR, 21);
#endif

      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
//      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
//  	                                         "Die Daten werden selektiert.\n ");  
	  while (fetch_sql (cursor) == 0)
      {
		  memcpy (&stabletab[i], &stable, sizeof (struct STABLE));
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      anz = i;
	  close_sql (cursor);
// 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHTABLES::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < anz; i ++)
      {
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHTABLES::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHTABLES::Search (void)
{
  	  int cx, cy;
	  char buffer [256];
      RECT rect;

// Jedesmal neu einlesen.

      if (Query)
      {
          delete Query;
          Query = NULL;
      }
      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      cx = 50;
      cy = 25;
      EnableWindow (hMainWindow, FALSE);
      if (Query == NULL)
      {
        idx = -1;
        Query = new CHQEX (cx, cy);
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, FALSE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 0);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
  	    Query->SetSearchLst (SearchLst);
        Read ("");
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, FALSE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      Query->SetSortRow (0, TRUE);
	  Query->ProcessMessages ();
//      EnableWindow (hMainWindow, TRUE);
      idx = Query->GetSel ();
	  EnableWindow (awin, TRUE);
      SetActiveWindow (awin);
      if (stabletab == NULL)
      {
          Query->DestroyWindow ();
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  strcpy (buffer, "");
	  Query->GetText (buffer);
	  clipped (buffer);
	  char *p;
	  for (p = buffer; *p <= ' ' && *p != 0; p += 1);
	  strcpy (stable.tabname, p);
//	  memcpy (&stable, &stabletab[idx], sizeof (stable));
      if (stabletab != NULL)
      {
          Query->SavefWork ();
      }
      Query->DestroyWindow ();
}
