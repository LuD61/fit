#ifndef _SEARCHTABLES_DEF
#define _SEARCHTABLES_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
// #include "dbclass.h"

struct STABLE
{
	  char tabname [22];
};

class SEARCHTABLES
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
//           static DB_CLASS DbClass; 
           static HWND awin;
           static struct STABLE *stabletab;
           static struct STABLE stable;
           static int idx;
           static long anz;
           static CHQEX *Query;

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHTABLES ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHTABLES ()
           {
                  if (stabletab != NULL)
                  {
                      delete stabletab;
                      stabletab = NULL;
                  }
           }

           STABLE *GetSTable (void)
           {
               if (idx == -1) return NULL;
               return &stable;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           static void FillFormat (char *);
           static void FillVlines (char *);
           static void FillCaption (char *);
           static void FillRec (char *, int);
           static int SearchLst (char *);
           static int Read (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
           static void UpdateList (void);

};  
#endif