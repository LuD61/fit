#include <windows.h>
#include "searchcols.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#ifdef ODBC
#include "mo_odbc.h"
#else
#ifndef BIWAK
#include "mo_curso.h"
#include "sqltypes.h"
#endif
#endif



struct SCOLUMN *SEARCHCOLS::scoltab = NULL;
struct SCOLUMN SEARCHCOLS::scolumn;
int SEARCHCOLS::idx = -1;
long SEARCHCOLS::anz;
CHQEX *SEARCHCOLS::Query = NULL;
// DB_CLASS SEARCHCOLS::DbClass; 
HINSTANCE SEARCHCOLS::hMainInst;
HWND SEARCHCOLS::hMainWindow;
HWND SEARCHCOLS::awin;

#ifndef BIWAK
HASH SEARCHCOLS::HashTab [] = {HASH (SQLCHAR,     "CHAR"),
                               HASH (SQLSMINT,    "SMALLINT"),
                               HASH (SQLINT,      "INTEGER"),
                               HASH (SQLFLOAT,    "FLOAT"),
                               HASH (SQLSMFLOAT,  "SMALLFLOAT"),
                               HASH (SQLDECIMAL,  "DECIMAL"),
                               HASH (SQLDATE,     "DATE"),
#ifdef ODBC
                               HASH (SQLTIME,     "TIME"),
                               HASH (SQLTIMESTAMP,"TIMESTAMP"),
#endif
                               HASH (0,            NULL)};
#else
HASH SEARCHCOLS::HashTab [] = {HASH (0,           "CHAR"),
                               HASH (1,           "SHORT"),
                               HASH (2,           "LONG"),
                               HASH (3,           "DOUBLE"),
                               HASH (0,            NULL)};
                                 ;
#endif


HASHTABLE SEARCHCOLS::SqlTypes (HashTab);
                                    
/*
#define SQLCHAR		0
#define SQLSMINT	1
#define SQLINT		2
#define SQLFLOAT	3
#define SQLSMFLOAT	4
#define SQLDECIMAL	5
#define SQLSERIAL	6
#define SQLDATE		7
#define SQLMONEY	8
#define SQLNULL		9
#define SQLDTIME	10
#define SQLBYTES	11
#define SQLTEXT		12
#define SQLVCHAR	13
#define SQLINTERVAL	14
#define SQLNCHAR	15
#define SQLNVCHAR	16
#define SQLTYPE		0x1F
#define SQLNONULL	0x100
#define SQLMAXTYPES	17
*/


int SEARCHCOLS::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

void SEARCHCOLS::FillFormat (char *buffer)
{
         sprintf (buffer, "%s%s%s",
                          "%s",
                          "%s",
                          "%s");
}


void SEARCHCOLS::FillCaption (char *buffer)
{
	    sprintf (buffer, "   %-42s  %-12s  %-12s", " Feldname", "FeldType", "  L�nge"); 
}

void SEARCHCOLS::FillVlines (char *buffer) 
{
	    sprintf (buffer, "   %42s  %12s  %12s", "1", "1", "1"); 
}


void SEARCHCOLS::FillRec (char *buffer, int i)
{
#ifndef BIWAK
          char coltype [21];
          char length [10];

          if (SqlTypes.GetValue (scoltab[i].coltype) != NULL)
          {
              strcpy (coltype, SqlTypes.GetValue (scoltab[i].coltype));
          }
          else
          {
              sprintf (coltype, "Type %d", scoltab[i].coltype);
          }

#ifdef ODBC
          if (scoltab[i].coltype == SQLDECIMAL)
          {

#else          
          if (scoltab[i].coltype == SQLDECIMAL ||
              scoltab[i].coltype == SQLMONEY)
          {
#endif
              sprintf (length, "%d,%d", ((scoltab[i].collength & 0xFF00) >> 8),
                                         (scoltab[i].collength & 0x00FF));
          }
          else
          {
              sprintf (length, "%d", scoltab[i].collength);
          }

 	      sprintf (buffer, "   %-40s     %-10s      %s", scoltab[i].colname,
                                                         coltype,
                                                         length); 
#endif
}


void SEARCHCOLS::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < anz; i ++)
       {
          FillRec (buffer, i);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHCOLS::Read (char *table)
/**
Query-Liste fuellen. 
**/
{
#ifndef BIWAK
	  char buffer [512];
      char Table [48];
      char TableTab [20][48];
      int tables;
      int colanz;
      int t;
      char *p;
	  int cursor;
	  int i;
      WMESS Wmess;
      char colname [48];
#ifdef ODBC
	  long precision;
      short scale; 
#endif


	  if (scoltab) 
	  {
           delete scoltab;
           scoltab = NULL;
	  }


      tables = 0;
      strcpy (Table, table);
      p = strtok (Table, ", ");
      if (p == NULL) return 0;
      strcpy (TableTab[tables], p);
      tables ++;
      while (p = strtok (NULL, ", "))
      {
          if (tables == 20) break;
          strcpy (TableTab[tables], p);
          tables ++;
      }


      idx = -1;
	  anz = colanz = 0;
      Query->SetSBuff ("");

#ifndef ODBC
      for (t = 0; t < tables; t ++)
      {
              sprintf (buffer, "select count (*) from syscolumns,systables "
                               "where systables.tabname = \"%s\" "
                               "and syscolumns.tabid = systables.tabid",TableTab[t]);
              out_quest ((char *) &colanz, 2, 0);
	          execute_sql (buffer);
              anz += colanz;
      }
#endif

      if (anz == 0) anz = 0x10000;

	  scoltab = new struct SCOLUMN [anz];

      i = 0;
      for (t = 0; t < tables; t ++)
      {
#ifndef ODBC
              sprintf (buffer, "select colname, coltype, collength "
  		        	           "from syscolumns, systables "
                               "where systables.tabname = \"%s\" "
                               "and syscolumns.tabid = systables.tabid "
                               "order by colname",TableTab[t]);
              out_quest ((char *) colname, 0, 48);
              out_quest ((char *) &scolumn.coltype, 1, 0);
              out_quest ((char *) &scolumn.collength, 1, 0);
              cursor = prepare_sql (buffer);
#else
 	          cursor = prepare_columns (TableTab[t], NULL);
		      bind_sqlcol (cursor, 4, (char *) colname,   SQLCCHAR, 48);
		      bind_sqlcol (cursor, 5, (short *) &scolumn.coltype, SQLCSHORT, 0);
		      bind_sqlcol (cursor, 7, (long *) &precision, SQLCLONG, 0);
		      bind_sqlcol (cursor, 8, (short *) &scolumn.collength, SQLCSHORT, 0);
		      bind_sqlcol (cursor, 9, (short *) &scale, SQLCSHORT, 0);

#endif
              if (cursor < 0) 
              {
                       return -1;
              }
//      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
//  	                                         "Die Daten werden selektiert.\n ");  
	          while (fetch_sql (cursor) == 0)
              {
                       if (tables > 1)
                       {
                           sprintf (scolumn.colname, "%s$%s", TableTab[t],
                                                              colname);
                       }
                       else
                       {
                           strcpy (scolumn.colname, colname);
                       }

                       clipped (scolumn.colname);
#ifdef ODBC
 			           if (scolumn.coltype == SQLDECIMAL)
					   {
					           scolumn.collength = (precision << 8) | scale;
					   }
#endif

		               memcpy (&scoltab[i], &scolumn, sizeof (struct SCOLUMN));
                       FillRec (buffer, i);
	                   Query->InsertRecord (buffer);
		               i ++;
              }
      }
      anz = i;
	  close_sql (cursor);
// 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
#endif
	  return 0;
}

void SEARCHCOLS::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < anz; i ++)
      {
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHCOLS::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHCOLS::Search (void)
{
  	  int cx, cy;
	  char buffer [256];
      RECT rect;

// Jedesmal neu einlesen.

      if (table == NULL) return;

      if (Query)
      {
          delete Query;
          Query = NULL;
      }
      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      cx = 60;
      cy = 25;
      EnableWindow (hMainWindow, FALSE);
      if (Query == NULL)
      {
        idx = -1;
        Query = new CHQEX (cx, cy);
//        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
//  	    Query->SetSearchLst (SearchLst);
        Read (table);
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, FALSE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
//	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
//      Query->SetSortRow (0, TRUE);
	  Query->ProcessMessages ();
//      EnableWindow (hMainWindow, TRUE);
      idx = Query->GetSel ();
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (scoltab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&scolumn, &scoltab[idx], sizeof (scolumn));
      if (scoltab != NULL)
      {
          Query->SavefWork ();
      }
}
