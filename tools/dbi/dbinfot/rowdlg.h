#ifndef _ROWDLG_DEF
#define _ROWDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"

#define ROW_CTL 2000
#define ROWNAME_CTL 2001
#ifndef ROWBEZ_CTL
#define ROWBEZ_CTL 2002
#endif
#define ROWSIZE_CTL 2003
#define ROWPIC_CTL 2004
#define HEAD_CTL 3000
#define FOOT_CTL 3001

#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5

#define LISTBORDER 1
#define LISTLOW 2
#define LISTHI 3
#include "RowProps.h"


class RowDlg : virtual public DLG 
{
          private :
             static mfont *Font;
             static CFIELD *_fHead [];
             static CFORM fHead;
             static CFIELD *_fFoot [];
             static CFORM fFoot;
             static CFIELD *_fRowCapt [];
             static CFORM fRowCapt;
             static CFIELD *_fRowCapt0 [];
             static CFORM fRowCapt0;

             static ItProg *After [];
             static ItProg *Before [];
             static ItFont *Fonts [];
             static char *EnableHead[];
             static char *HelpName;
             RowProps *Rows;
             int    BorderType;

             static RowDlg *ActiveMeEinh;
          public :

            void SetRows (RowProps *Rows)
            {
                this->Rows = Rows;
            }

            RowProps*GetRows (void)
            {
                return Rows;
            }


  	        RowDlg (int, int, int, int, char *, int, BOOL);
 	        void Init (int, int, int, int, char *, int, BOOL);
            void OrPosAttr (DWORD);
            void OrPosAttrEx (DWORD);
            BOOL OnKey1 (void);
/*
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
*/
            BOOL OnKey5 (void);
/*
            BOOL OnKey6 (void);
            BOOL OnKey7 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
*/
            BOOL OnKey12 (void);
            BOOL OnKeyReturn (void);
/*
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
*/
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            void PrintComment (char *);
//            HWND OpenWindow (HANDLE, HWND);
//            HWND OpenScrollWindow (HANDLE, HWND);
            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
};
#endif
