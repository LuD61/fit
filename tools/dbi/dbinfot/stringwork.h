#ifndef _TEXTWORK_DEF
#define _TEXTWORK_DEF

class StringWork 
{
          private :
              static char **Felder;
			  static int oldFieldAnz;
              int Wsize [2];
              char **Feldnamen;
              char **Texte;
              char **Namen;
              int *Rows;
              int *Columns;
              int *Length;
              int headfieldanz;
              int fieldanz;
              int Startrow;
              int Textlen;
			  char *orgString;
              COLORREF WinColor;
          public :
              int *GetWsize (void)
              {
                     return Wsize;
              }

              char **GetFeldnamen (void)
              {
                     return Feldnamen;
              }

              char **GetTexte (void)
              {
                     return Texte;
              }      
              char **GetNamen (void)
              {
                     return Namen;
              }      

              int *GetRows (void)
              {
                     return Rows;
              }

              int *GetColumns (void)
              {
                     return Columns;
              }

              int *GetLength (void)
              {
                     return Length;
              }

              int GetFieldanz (void)
              {
                     return fieldanz;
              } 

              int GetTextlen (void)
              {
                     return Textlen;
              }

              COLORREF GetWinColor (void)
              {
                  return WinColor;
              }

              StringWork (char *); 
              ~StringWork ();
             char *instring (char *, char *, char *);
             void getLabel (char *, char *);
             char *setFelder (char *, int);  
             char **GetFelder (void);
             static char *ChangeSysdate (char *, char *, int);
};
#endif
