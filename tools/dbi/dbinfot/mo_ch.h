#ifndef _MO_CH_DEF
#define _MO_CH_DEF

class CH
{
          private :
			  HWND hMainWindow;
			  HINSTANCE hInstance;
			  HWND hWnd;
              mfont *Font;
			  DWORD currentfield;
			  static int (*OkFunc) (int); 
			  static int (*DialFunc) (int); 
              int cx, cy;
          public :
			  CH (int, int);
			  ~CH ();
 		      void ProcessMessages (void);
              static void ShowDlg (HDC, form *);
              static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
			  static void SetOkFunc (int (*) (int));
			  static void SetDialFunc (int (*) (int));
              static int  GetSel (void);
              static void MoveWindow (void);
              void OpenWindow (HANDLE, HWND);
              void DestroyWindow (void);
			  void SetCurrentID (DWORD);
			  void SetCurrentName (char *);
			  void EnableID (DWORD, BOOL);
			  void EnableName (char *, BOOL);
			  void CheckID (DWORD, BOOL);
			  void CheckName (char *, BOOL);
              void VLines (char *, int);
			  void InsertCaption (char *);
			  void InsertRecord  (char *);
			  void UpdateRecord  (char *, int);
              void GetText (char *);
              BOOL TestButtons (HWND);
};
#endif

