#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "textdlg.h" 

#define BuOk       1001
#define BuCancel   1002

mfont TextDlg::dlgfont = {
                          "ARIAL", STDSIZE, 0, 0,
                          BLACKCOL,
                          GRAYCOL,
                          0,
                          NULL};

mfont TextDlg::buttonfont = {
                          "ARIAL", 90, 0, 0,
                          BLACKCOL,
                          GRAYCOL,
                          0,
                          NULL};

TextDlg::TextDlg (StringWork *textWork, int x, int y, int cx, int cy, 
                  char *Caption, int Size, BOOL Pixel) : 
         DLG (x, y, cx, cy, Caption, Size, Pixel)

{
     int i, f;
     int updanz = 0;
     int *Wsize;
 	 SIZE size;
	TEXTMETRIC tm;

     Cfields = NULL;

     Offset = 2000;
     fieldanz = textWork->GetFieldanz ();;

     Wsize = textWork->GetWsize ();

     SetDimension (Wsize[0], Wsize[1]);
     Cfields = new CFIELD * [fieldanz * 2 + 3];
     if (Cfields == NULL) 
     {
                return;
     }

     Texte = textWork->GetTexte ();
     if (Texte == NULL) 
     {
                fieldanz = 0;
                return;
     }
     Felder = textWork->GetFelder ();
     if (Felder == NULL) 
     {
                fieldanz = 0;
                return;
     }

     Namen = textWork->GetNamen ();
     if (Namen == NULL) 
     {
                fieldanz = 0;
                return;
     }

     Feldnamen = textWork->GetFeldnamen ();

     Rows    = textWork->GetRows ();
     Columns = textWork->GetColumns ();
     Length  = textWork->GetLength ();

     this->Font = &dlgfont;  
 	 HDC hdc = GetDC (NULL);
     HFONT hFont = SetDeviceFont (hdc, Font, &tm);
     HFONT oldfont = SelectObject (hdc, hFont);
	 GetTextMetrics (hdc, &tm);
	 GetTextExtentPoint32 (hdc, "X", 1, &size);
 	 DeleteObject (SelectObject (hdc, oldfont));
     i = f = 0;
     Cfields [f] = new CFIELD ("pos_frame", "",  textWork->GetTextlen () +  Length [0] + 2, 
		                        fieldanz + 4, 1, 1 ,  NULL, "",
                                CBORDER,   
								500, Font, 0, TRANSPARENT),    
//     Cfields[f]->SetBorder (BLACKCOL, WHITECOL, HIGH); 
     f ++;
     while (i < fieldanz)
     {
            Cfields[f] = new CFIELD ("", Texte [i], 0, 0, Columns [i] + 1, 
                                                          Rows [i] + 1, NULL, "",
                                     CDISPLAYONLY, 500, &dlgfont, 0, 0); 
            f ++;
            Cfields[f] = new CFIELD (Namen[i], Felder [i], Length [i], 0, 
                                     Columns [i] + 1 + textWork->GetTextlen (),
                                     Rows [i] + 1, NULL, "",
                                     CEDIT, Offset + i, &dlgfont, 0, ES_AUTOHSCROLL); 
			Cfields[f]->SetLength (255);
            f ++;
            i ++;
     }

     Cfields [f] = NULL;

     CFORM *Cforms = new CFORM (fieldanz, Cfields); 
/*
 	 x = 0;
	 y = (Columns [i - 1] + 2) * tm.tmHeight;
	 y = (int) (double) ((double) y * 1.3) + tm.tmHeight;
	 cx = 10 * size.cx;
	 cy = (int) (double) ((double) tm.tmHeight * 1.3);
*/
	 x = Columns [0] + textWork->GetTextlen () + Length [0] + 3;
	 y = Rows [0];
	 cx = 15;
	 cy = 0;



 	 CFIELD **_fButton    = new CFIELD * [2];
/*
	 _fButton[0] = new CFIELD ("OK", "OK", cx, cy, x * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
	 _fButton[1] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x + 12 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
*/
	 _fButton[0] = new CFIELD ("OK", "OK", cx, cy, x, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 0, BS_PUSHBUTTON);				                                   
 
	 _fButton[1] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x, 
												  y + 1,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 0, BS_PUSHBUTTON);				    
	 CFORM *fButton     = new CFORM (2, _fButton);
//	 fButton->Setchpos (0, 2);


     _fButton[0]->SetFont (&buttonfont);
     _fButton[1]->SetFont (&buttonfont);


     CFIELD **_Cform    = new CFIELD * [2];
	 _Cform[0] = new CFIELD ("Cforms", Cforms, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 982, Font, 0, 0);
/*
	 _Cform[1] = new CFIELD ("Button", fButton, 0, 0, -1, 0, NULL, "", CFFORM,
				                                 981, Font, 0, 0);
*/
	 _Cform[1] = new CFIELD ("Button", fButton, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

     Cform = new CFORM (2, _Cform); 
     Cforms->SetFieldanz ();
	 Cform->GetRectText (&Wsize[0], &Wsize[1]);
	 Wsize[0] += 1;
	 Wsize[1] += 1;
	 if (Wsize[1] > 12)
	 {
		 Wsize [1] = 12;
	 }
     SetDimension (Wsize[0], Wsize[1]);
     SetDialog (Cform);
//	 Pack ();
     NewTabStops (Cforms);
//     fButton->GetCfield ("OK")->SetTabstop (FALSE); 
//     fButton->GetCfield ("Cancel")->SetTabstop (FALSE); 
     SetWinBackground (textWork->GetWinColor ());
}

TextDlg::~TextDlg ()
{
     int i;

     if (Cfields != NULL)
     {
         for (i = 0; Cfields[i] != NULL; i ++)
         {
             delete Cfields[i];
         }
         delete Cfields;
     }

     if (Cform != NULL)
     {
         delete Cform;
     }
}

void TextDlg::SetText (void)
{
      fWork->SetText ();
      SetTabStopPos (0);
}

BOOL TextDlg::OnKeyReturn (void)
{
      CFIELD *Cfield = GetCurrentCfield ();

      if (strcmp (Cfield->GetName (), "OK") == 0)
      {
          return OnKey12 ();
      }
      if (strcmp (Cfield->GetName (), "Cancel") == 0)
      {
          return OnKey5 ();
      }
      return FALSE;
}

BOOL TextDlg::OnKey5 (void)
{
	 syskey = KEY5;
     EnableWindows (hMainWindow, TRUE);
     fWork->GetText ();
     fWork->destroy ();    
     DestroyWindow ();
     return TRUE;
}

BOOL TextDlg::OnKeyEscape (void)
{
     return OnKey5 ();
}


BOOL TextDlg::OnKey12 (void)
{
	 syskey = KEY12;
     EnableWindows (hMainWindow, TRUE);
     fWork->GetText ();
     fWork->destroy ();    
     DestroyWindow ();
     return TRUE;
}


BOOL TextDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
		if (LOWORD (wParam) == BuCancel)
		{
			return OnKey5 ();
		}
		else if (LOWORD (wParam) == BuOk)
		{
			return OnKey12 ();
		}

        if (HIWORD (wParam) == EN_KILLFOCUS)
        {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
        }
        if (HIWORD (wParam) == EN_SETFOCUS)
        {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
        }
/*
        if (HIWORD (wParam) == WM_KILLFOCUS)
        {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
        }
        if (HIWORD (wParam) == WM_SETFOCUS)
        {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
        }
*/
        return DLG::OnCommand (hWnd, msg, wParam, lParam); 
}

BOOL TextDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            return OnKey5 ();
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}
           

    
     