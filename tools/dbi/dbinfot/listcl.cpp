#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "mo_gdruck.h"
#ifndef _NINFO
#include "inflib.h"
#endif

#define MAXLEN 80
#define MAXBEZ 500
#define MAXFIND 30


static mfont printfont   = {"Arial", 100, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

static mfont printfettfont   = {"Arial", 100, 0, 1, RGB (255, 0, 0),
                                         RGB (0, 255, 255),
                                         1,
                                         NULL};

extern "C"
{
extern long dasc_to_long (char *);
}

static BOOL NoPaint = FALSE;

static form RowUb;
static form RowData;
static form RowLine;


BOOL (*IsComboMessage) (MSG *) = NULL;

void SetComboMess (BOOL (*CbProg) (MSG *))
{
       IsComboMessage = CbProg;
}

mfont sfont = {"Courier New", 90, 100, 0,
                                       BLACKCOL,
                                       LTGRAYCOL,
                                       0,
                                       NULL};



ITEM iUbSatzNr ("", "S-Nr", "", 0);

static field _fUbSatzNr[] = {
&iUbSatzNr,          6, 1, 0, 0, 0, "", BUTTON, 0, 0, 0};

static form fUbSatzNr = {1, 0, 0, _fUbSatzNr, 0, 0, 0, 0, NULL};


static char SatzNr [10];
static ITEM iSatzNr ("", SatzNr, "", 0);

static field _fSatzNr[] = {
&iSatzNr,          6, 1, 0, 0, 0, "", EDIT, 0, 0, 0};

static form fSatzNr = {1, 0, 0, _fSatzNr, 0, 0, 0, 0, NULL};

BOOL NoRecNr = FALSE;
static char TAB[] = {9,0};
static char NL[]  = {10,0};


static field iButton = {NULL,  0, 1, 0, 0, 0, "", BUTTON, 0, 0, 0}; 
static field iEdit =   {NULL,  0, 0, 0, 0, 0, "", EDIT, 0, 0, 0}; 

static form IForm = {0, 0, 0, NULL, 0, 0, 0, 0, NULL};

static char *feldbztab [MAXBEZ];
static int fbanz = 0;

/* Daten fuer Switch  */


static char *DlgSaetze [1000];
static int DlgAnz = 0;

static FELDER DlgZiel [] = {
"Bezeichnung",     20, "C", 0, 1,
"Wert",        MAXLEN, "C", 0, 1,
};

static int Dlgzlen = 20 + MAXLEN;
static unsigned char DlgSatz [20 + MAXLEN + 2];

/* Daten fuer Fonteingabe           */

static char zh[] = {"Zeichenh�he  "};
static char zw[] = {"Zeichenbreite"};

HWND choise = NULL;

static int FAttr;
static ITEM vHeightT ("" , zh, "",  0);
static ITEM vWidthT  ("" , zw, "",  0);
static ITEM vHeight ("" , "      ", "",   0);
static ITEM vWidth  ("" , "      ", "", 0);
static ITEM vAttrC  ("",  "Fett",  "", 0);
static ITEM vOK     ("",  "OK",  "", 0);
static ITEM vCancel ("",  "Abbrechen", "", 0);

/*
static ITEM vHeight ("" , "      ", "Zeichenh�he  ",   0);
static ITEM vWidth  ("" , "      ","Zeichenbreite", 0);
static ITEM vAttrC  ("",  "Fett",  "", 0);
static ITEM vOK     ("",  "OK",  "", 0);
static ITEM vCancel ("",  "Abbrechen", "", 0);
*/

static int testcr (void);

/*
static field _ffont [] = {
&vHeight,   6, 0, 1, 1, 0, "%3d", EDIT | UPDOWN, 0, testcr, 0, 
&vWidth,    6, 0, 2, 1, 0, "%3d", EDIT | UPDOWN, 0, testcr, 0, 
&vAttrC,    7, 0, 3,15, 0, "", BUTTON | CHECKBUTTON, 0, testcr, KEY6,
&vOK,       4, 0, 5, 4, 0, "", BUTTON | DEFBUTTON, 0, testcr, KEY12,
&vCancel,  11, 0, 5, 9, 0, "", BUTTON, 0, testcr, KEY5, 
};

static form ffont = {5, 0, 0, _ffont, 0, 0, 0, 0, &sfont}; 
*/

static field _ffont [] = {
&vHeightT, 13, 0, 1, 1, 0, "", DISPLAYONLY, 0, 0, 0, 
&vWidthT,  13, 0, 2, 1, 0, "", DISPLAYONLY, 0, 0, 0, 
&vHeight,   6, 0, 1,15, 0, "%3d", EDIT | UPDOWN, 0, testcr, 0, 
&vWidth,    6, 0, 2,15, 0, "%3d", EDIT, 0, testcr, 0, 
&vAttrC,    7, 0, 3,15, 0, "", BUTTON | CHECKBUTTON, 0, testcr, KEY6,
&vOK,       4, 0, 5, 4, 0, "", BUTTON | DEFBUTTON, 0, testcr, KEY12,
&vCancel,  11, 0, 5, 9, 0, "", BUTTON, 0, testcr, KEY5, 
};

static form ffont = {7, 0, 0, _ffont, 0, 0, 0, 0, &sfont}; 

static struct UDTAB udtab[] = {500, 50, 90,
                               200,  0,100};
static int udanz = 2;

static char SearchBuff [256];

static ITEM vSearchBez ("","Suchen",         "", 0);   
static ITEM vSearch    ("", SearchBuff,       "", 0);
static int testcr (void);

static field _ffind [] = {
&vSearchBez,   6, 0,  1, 2,  0, "", DISPLAYONLY, 0, 0, 0,
&vSearch,     20,10,  1, 9,  0, "255", COMBOBOX | SCROLL, 0, 0, 0,     
&vOK,         11, 0,  3, 5,  0, "", BUTTON, 0, 0, KEY12,
&vCancel,     11, 0,  3,17,  0, "", BUTTON, 0, 0, KEY5, 
}
;
static char findauswahl [MAXFIND][256];
static int findpos = 0;

static combofield  cbfield[] = {1, 256, 0, (char *) findauswahl,
                                 0,  0, 0, NULL};


static form ffind = {4, 0, 0, _ffind, 0, 0, 0, cbfield, &sfont}; 

int eBreak (void)
{
       syskey = KEY5;
       break_enter ();
       return 0;
}

int eOk (void)
{
       syskey = KEY12;

       GetWindowText (ffont.mask [0].feldid,
                      ffont.mask [0].item->GetFeldPtr (),
                      ffont.mask [0].length);
       GetWindowText (ffont.mask [1].feldid,
                      ffont.mask [1].item->GetFeldPtr (),
                      ffont.mask [1].length);
       break_enter ();
       return 0;
}

int eAttr (void)
{
       syskey = KEY6;
       if (FAttr == 0)
       {
           FAttr = 1;
       }
       else
       {
           FAttr = 0;
       }
       SendMessage (ffont.mask[2].feldid, BM_SETCHECK,
                       FAttr, 0l);
       return 0;
}

int  ListClass::ColSort = 0;
form ListClass::DataForm;
form ListClass::PrintForm;
BOOL *ListClass::fSort = NULL;


int ListClass::pagelen = 66;
int ListClass::pagelen0 = 59;
int ListClass::ublen = 0;
int ListClass::PageStdCols;
int ListClass::PageStart = 0;
int ListClass::PageEnd = 0;
int ListClass::ReadItemLabel = FALSE;

void ListClass::ChoiseLines (HDC hdc)
{
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         RECT rect;
         int x, y;
         int cx, cy;

         GetClientRect (choise, &rect);
         x = tm.tmAveCharWidth / 2;
         y = tm.tmHeight / 2;

         cx = rect.right - tm.tmAveCharWidth / 2;
         cy = rect.bottom - tm.tmHeight / 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         SelectObject (hdc, hPenG);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

         SelectObject (hdc, hPenW);


         y ++;
         x ++;
         cx --;
         cy --;
         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         cx += 2;
         cy += 2;
         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

}

int testcr (void)
{
      HWND hWnd;
      
      if (syskey == KEY6)
      {
           eAttr ();
           return 1;
      }
      if (syskey == KEYCR)
      {
           hWnd = GetFocus ();
           if (hWnd == ffont.mask[2].feldid)
           {
               syskey = KEY6; 
               eAttr ();
               return 1;
           }
           else if (hWnd == ffont.mask[4].feldid)
           {
               syskey = KEY5;
           }
           break_enter ();
       }
       else if (syskey == KEY12)
       {
           break_enter ();
       }
       else if (syskey == KEY5)
       {
           break_enter ();
       }
       else if (syskey == KEYESC)
       {
           break_enter ();
       }
       return 0;
}

/*
void ListClass::ChoiseFont (mfont *lFont)
{
        HDC hdc; 
        RECT rect;

        spezfont (&sfont);

        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);
        set_fkt (eAttr, 6);

        udtab[0].Pos = lFont->FontHeight;
        udtab[1].Pos = lFont->FontWidth;
        sprintf (ffont.mask[0].item->GetFeldPtr (), "%3d", 
                 lFont->FontHeight);       

        sprintf (ffont.mask[1].item->GetFeldPtr (), "%3d", 
                 lFont->FontWidth);
        if (lFont->FontAttribute)
        {
                 ffont.mask[2].picture = "1";
        }
        else
        {
                 ffont.mask[2].picture = "";
        }

        no_break_end ();
        SetButtonTab (TRUE);
        DisablehWnd (hMainWindow);
        if (mamain2)
        {
            GetWindowRect (mamain2, &rect);
            rect.left += (tm.tmAveCharWidth * 7); 
        }
        else
        {
            GetWindowRect (hMainWindow, &rect);
            rect.top += (tm.tmHeight * 3);
            rect.left += (tm.tmAveCharWidth * 20); 
        }

        choise  = CreateWindow (
                                 "hStdWindow",
                                 "Font-Daten",
                                 WS_POPUP | 
                                 WS_VISIBLE | 
                                 WS_CAPTION | 
                                 WS_DLGFRAME, 
                                 rect.left, rect.top,
                                 200, 180,
                                 GetActiveWindow (),
                                 NULL,
                                 hMainInst,
                                 NULL);


        hdc = GetDC (choise);
        ChoiseLines (hdc);
        ReleaseDC (choise, hdc);
        SetUpDownTab (udtab , udanz);
        enter_form (choise, &ffont, 0, 0);
        DestroyWindow (choise);
        choise = NULL;
        SetButtonTab (FALSE);

        if (syskey == KEY5) return;
        if (syskey == KEYESC) return;

        lFont->FontHeight = atoi (ffont.mask[0].item->GetFeldPtr ());       
        lFont->FontWidth  = atoi (ffont.mask[1].item->GetFeldPtr ());       
        lFont->FontAttribute  = FAttr;       

		if (UbRows)
		{
                    UbHeight = UbRows * tm.tmHeight;
		}
		else
		{
                    UbHeight = (int) (double) ((double) tm.tmHeight * 1.5);

		}
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
}
*/

void ListClass::ChoiseFont (mfont *lFont)
/**
Font eingeben.
**/
{
        HDC hdc; 

        spezfont (&sfont);

        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);
        set_fkt (eAttr, 6);

        udtab[0].Pos = lFont->FontHeight;
        udtab[1].Pos = lFont->FontWidth;
        sprintf (ffont.mask[2].item->GetFeldPtr (), "%3d", 
                 lFont->FontHeight);       

        sprintf (ffont.mask[3].item->GetFeldPtr (), "%3d", 
                 lFont->FontWidth);
        strcpy (ffont.mask[0].item->GetFeldPtr (), zh);
        strcpy (ffont.mask[1].item->GetFeldPtr (), zw);

        if (lFont->FontAttribute)
        {
                 ffont.mask[4].picture = "1";
				 FAttr = 1;
        }
        else
        {
                 ffont.mask[4].picture = "";
				 FAttr = 0;
        }

        no_break_end ();
        SetButtonTab (TRUE);
        DisablehWnd (hMainWindow);

        choise  = CreateWindow (
                                 "hStdWindow",
                                 "Font-Daten",
                                 WS_POPUP | 
                                 WS_VISIBLE | 
                                 WS_CAPTION | 
                                 WS_DLGFRAME, 
                                 32, 40,
                                 200, 180,
                                 GetActiveWindow (),
                                 NULL,
                                 hMainInst,
                                 NULL);

        ShowWindow (choise, SW_SHOW);
        UpdateWindow (choise);

        hdc = GetDC (choise);
        ChoiseLines (hdc);
        ReleaseDC (choise, hdc);
        SetUpDownTab (udtab , udanz);
        DisplayAfterEnter (FALSE);
        enter_form (choise, &ffont, 0, 0);
        DestroyWindow (choise);
        DisplayAfterEnter (TRUE);
        choise = NULL;
        SetButtonTab (FALSE);
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);

        if (syskey == KEY5) return;
        if (syskey == KEYESC) return;

        lFont->FontHeight = atoi (ffont.mask[2].item->GetFeldPtr ());       
        lFont->FontWidth  = atoi (ffont.mask[3].item->GetFeldPtr ());       
        lFont->FontAttribute  = FAttr;       
        SetFont (lFont);
/*
		if (UbRows)
		{
                    UbHeight = UbRows * tm.tmHeight;
		}
		else
		{
                    UbHeight = (int) (double) ((double) tm.tmHeight * 1.5);

		}
	    RowHeightP = (int) (double) ((double) tm.tmHeight * RowHeight);
*/
}


void ListClass::SetColors (COLORREF Color, COLORREF BkColor)
{
     
            
        this->Color   = Color;
        this->BkColor = BkColor;
		if (mamain2)
		{
                   SetClassLong (mamain2,GCL_HBRBACKGROUND, 
                                      (long) CreateSolidBrush (BkColor));
                   InvalidateRect (mamain3, NULL, TRUE);
		}
}

void ListClass::FindNext (void)
/**
Naechste Uebereinstimmung suchen.
**/
{
        char leer [256];

        memset (leer, ' ', 256);
        leer[255] = (char) 0;
        if (strcmp (SearchBuff,leer) <= 0)
        {
            FindString ();
        }
        else
        {
            this->find (SearchBuff);
        }
}

BOOL ListClass::NewSearchString ()
{
        int i;

        for (i = 0; i < findpos; i ++)
        {
            if (strcmp (SearchBuff, findauswahl[i]) == 0)
            {
                return FALSE;
            }
        }
        return TRUE;
}

void ListClass::FindString (void)
/**
String suchen.
**/
{
        HWND hFind;

        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        break_end ();
        DisablehWnd (hMainWindow);
        SetActiveWindow (mamain3);
        SetAktivWindow (mamain3);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        hFind = OpenWindowChC (6, 40, 10, 20, hMainInst,
                "Suchen");
        enter_form (hFind, &ffind, 0, 0);
//        DestroyFrmWindow ();
        DestroyWindow (hFind);
        hFind = NULL;
        if (syskey == KEY5) return;

        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
        if (NewSearchString ())
        {
                 strcpy (findauswahl[findpos], SearchBuff);
                 if (findpos < MAXFIND -1) findpos ++;
                 cbfield[0].cbanz = findpos; 
                 fillcombobox (&ffind, 0);
        }
        this->find (SearchBuff);
        SetActiveWindow (mamain3);
        SetAktivWindow (mamain3);
}

BOOL ListClass::InRec (char *value, int *pos)
{
         int i;

         for (i = *pos; i < DataForm.fieldanz; i ++)
         {
             if (DataForm.mask[i].picture[0] == '%' && numeric (value))
             {
               if (ratod (value) == ratod (DataForm.mask[i].item->GetFeldPtr ()))
               {
                 *pos = i;
                 return TRUE;
               }
             }
             else
             {
//             if (strstr (DataForm.mask[i].item->GetFeldPtr (),
//             if (upstrstr (DataForm.mask[i].item->GetFeldPtr (),
               if (strupcmp (DataForm.mask[i].item->GetFeldPtr (),
                 value, strlen (value)) == 0)
               {
                 *pos = i;
                 return TRUE;
               }
             }
         }
         return FALSE;
}


void ListClass::find (char *value)
/**
Begriff suchen.
**/
{
         int i;
         int row;

         if (RowMode) return;

         clipped (value);
         row = AktColumn + 1;
         
         for (i = AktRow + 1; i < recanz; i ++)
         {
             memcpy (ausgabesatz, SwSaetze[i], zlen);
             if (InRec (value, &row)) 
             {
                 break;
             }
             row = 0;
         }
         if (i == recanz) 
         {
             disp_mess ("Keine �bereinstimmung gefunden", 2);
             return;
         }

         AktRow = i;
         AktColumn = row;
         FindRow = i;
         FindColumn = row;
		 if (RowMode == 0)
		 {
		           AktColumn = FirstColumn ();
		 }
		 else
		 {
		           AktColumn = FirstColumnR ();
		 }
//         SetPos (AktColumn);
         SetVPos (AktRow);
         InvalidateRect (mamain3, NULL, TRUE);
         UpdateWindow (mamain3);
}


HWND ListClass::InitListWindow (HWND hMainWindow)
/**
Listfenster oeffnen.
**/
{
       int x,y;
       int cx,cy;
       RECT rect;

       this->hMainWindow = hMainWindow;
       this->hMainInst = (HANDLE) GetWindowLong (hMainWindow, 
                                               GWL_HINSTANCE);
       y = LineRow + 5;
       x = 1;
       GetClientRect (hMainWindow, &rect);
       cy = rect.bottom - y - 5;
       cx = rect.right - 2;

       mamain2 = CreateWindowEx (
                                WS_EX_CLIENTEDGE, 
                                "hListWindow",
                                "",       
                                WS_CHILD | WS_VISIBLE,
                                x, y,
                                cx, cy,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);

        ShowWindow (mamain2, SW_SHOWNORMAL);
        UpdateWindow (mamain2);
        mamain3 = CreateWindowEx (
                                WS_EX_CLIENTEDGE, 
                                "hListWindow",
                                "",       
                                WS_CHILD | WS_VISIBLE,
                                0, 0,
                                0, 0,
                                mamain2,
                                NULL,
                                hMainInst,
                                NULL);
        return mamain2;
}

void ListClass::CopyFonts (mfont *lFont)
/**
Neuen Font fuer Liste setzen.
**/
{
        HFONT hFont, oldFont;
        HDC hdc;
        SIZE size;

        spezfont (lFont);
        memcpy (&UbFont,  lFont, sizeof (mfont));   
        memcpy (&UbSFont, lFont, sizeof (mfont));   
        memcpy (&SFont,   lFont, sizeof (mfont));   
        memcpy (&ListFont,lFont, sizeof (mfont));
        FAttr = lFont->FontAttribute;       
        EditFont = NULL;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
		ListhFont = hFont;
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (0, hdc);

        SetTextMetric (&tm);
}


void ListClass::SetFont (mfont *lFont)
/**
Neuen Font fuer Liste setzen.
**/
{
        HFONT hFont, oldFont;
        HDC hdc;
        SIZE size;

        SetNewStdFont (lFont); 
        AktFont = lFont;
        EditFont = NULL;
        DestroyFocusWindow ();
        CloseControls (&fUbSatzNr);
        CloseControls (&UbForm);

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (0, hdc);

        SetTextMetric (&tm);

        if (NoRecNr == FALSE)
        {
                  display_form (mamain3, &fUbSatzNr, 0, 0);
        }
		if (UbRows)
		{
                    UbHeight = UbRows * tm.tmHeight;
		}
		else
		{
                    UbHeight = (int) (double) ((double) tm.tmHeight * 1.5);

		}
	    RowHeightP = (int) (double) ((double) tm.tmHeight * RowHeight);
        display_form (mamain3, &UbForm, 0, 0);
        SendMessage (mamain2, WM_SIZE, NULL, NULL);
        InvalidateRect (mamain2, 0, TRUE);
        InvalidateRect (mamain3, 0, TRUE);
}


void ListClass::TestMamain3 (void)
{
       RECT rect;
//       HDC hdc;
       
       GetClientRect (mamain2, &rect);

       if (TrackNeeded ())
       {
           rect.bottom -= 16;
       }
       if (VTrackNeeded ())
       {
           rect.right -= 16;
       }

       MoveWindow (mamain3, 0, 0, rect.right, rect.bottom, TRUE);


       CloseControls (&UbForm);
       if (UbForm.mask)
       {
               display_form (mamain3, &UbForm, 0, 0);
       }
/*
       hdc = GetDC (mamain3);
       ShowDataForms (hdc);
       ReleaseDC (mamain3, hdc);
*/
}

void ListClass::TestTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
	   SCROLLINFO scinfo;

       if (TrackNeeded ())
       {
           CreateTrack ();
       }
       else
       {
           DestroyTrack ();
       }
       if (hTrack == NULL) return;

       GetClientRect (mamain2, &rect);
       x = 0;
       y = rect.bottom - 16;
       cy = 16;
       cx = rect.right;
       MoveWindow (hTrack, x, y, cx, cy, TRUE);
//       MoveWindow (hTrack, x, y, cx, cy, FALSE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE;
       scinfo.nPage  = 1;
       SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
}

void ListClass::TestVTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
	   SCROLLINFO scinfo;

       if (VTrackNeeded ())
       {
           CreateVTrack ();
       }
       else
       {
           DestroyVTrack ();
       }
       if (vTrack == NULL) return;

       GetClientRect (mamain2, &rect);

       x = rect.right - 16;
       cx = 16;
       y = 0;
       cy = rect.bottom;
       if (hTrack)
       {
                 cy -= 16;
       }
       MoveWindow (vTrack, x, y, cx, cy, TRUE);
//       MoveWindow (vTrack, x, y, cx, cy, FALSE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE;
       scinfo.nPage  = 1;
       SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
       SetScrollPos (vTrack, SB_CTL, scrollpos, TRUE);
}


void ListClass::MoveListWindow (void)
/**
Listfenster oeffnen.
**/
{
       int x,y;
       int cx,cy;
       RECT rect;

       if (mamain2 == NULL) return;

       GetPageLen ();
       y = LineRow + 5;
       x = 1;
       GetClientRect (hMainWindow, &rect);
       cy = rect.bottom - y - 5;
       cx = rect.right - 2;

       MoveWindow (mamain2, x, y, cx, cy, TRUE);

//       MoveWindow (mamain2, x, y, cx, cy, FALSE);

//       if (hwndTB == NULL) return;
       if (hwndTB != NULL)
       {
              GetClientRect (hwndTB, &rect);
              MoveWindow (hwndTB, 0, 0, cx + 2, rect.bottom, TRUE);
       }

       TestMamain3 ();
       TestTrack ();
       TestVTrack ();
}


BOOL ListClass::TrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT frmrect;
        RECT winrect;

		if (banz == 0) return FALSE;
        GetFrmRect (&UbForm, &frmrect);
        GetClientRect (mamain2, &winrect);
        if (frmrect.right > winrect.right)
        {
                    return TRUE;
        }
        return TRUE;
}

void ListClass::CreateTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
        RECT rect;
		SCROLLINFO scinfo;

        if (hTrack) return;

        GetClientRect (mamain2, &rect);

        x = 0;
        y = rect.bottom - 16;
        cy = 16;
        cx = rect.right;

        hTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_HORZ,
                               x, y, cx, cy,
                               mamain2,
                               (HMENU) 701,
                               hMainInst,
                               0);


		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = banz - 1;
		 scinfo.nPage  = 0;
         SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
}

void ListClass::DestroyTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (hTrack)
          {
              DestroyWindow (hTrack);
              hTrack = NULL;
          }
}

BOOL ListClass::VTrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT winrect;
        int cy;

        GetClientRect (mamain2, &winrect);

// recanz + 1 wegen Ueberschrift 

//        cy = (recanz + 1) * tm.tmHeight;
//        cy = (int) (double) ((double) UbHeight + recanz * tm.tmHeight * RowHeight);
        cy = UbHeight + recanz * RowHeightP;
        if (hTrack) 
        {

// untere Bildlaufleiste abziehen

            cy += 16; 
        }
        if (cy > winrect.bottom)
        {
                    return TRUE;
        }
        return FALSE;
}

void ListClass::CreateVTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
        RECT rect;
		SCROLLINFO scinfo;

        if (vTrack) return;
        GetClientRect (mamain2, &rect);

        x = rect.right - 16;
        cx = 16;
        y = 0;
        cy = rect.bottom;
        if (hTrack)
        {
                 cy -= 16;
        }

        vTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_VERT,
                               x, y, cx, cy,
                               mamain2,
                               (HMENU) 702,
                               hMainInst,
                               0);
		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = recanz - 1;
		 if (lPagelen)
		 {
		     scinfo.nPage  = 1;
		 }
		 else
		 {
			 scinfo.nPage = 0;
		 }
         SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
}

void ListClass::DestroyVTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (vTrack)
          {
              DestroyWindow (vTrack);
              vTrack = NULL;
          }
}

BOOL ListClass::IsInListArea (MSG *msg)
/**
MousePosition Pruefen unf Focus setzen.
**/
{
	    POINT mousepos;
		RECT rect;
		int x1, y1;
		int x2, y2;
		int i, j;
		int diff;

        if (msg->hwnd != mamain3) return FALSE;

        diff = 0;
        if (LineForm.frmstart)
		{
			if (NoRecNr == FALSE)
			{
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                           fSatzNr.mask[0].length;
			}
			else
			{
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
		}
        GetCursorPos (&mousepos);
		GetWindowRect (mamain3, &rect);
		rect.top += tm.tmHeight;

		if (mousepos.x < rect.left ||
			mousepos.x > rect.right)
		{
			        return FALSE;
		}
		if (mousepos.y < rect.top ||
			mousepos.y > rect.bottom)
		{
			        return FALSE;
		}

        ScreenToClient (mamain3, &mousepos);

		for (i = 0; i < recanz; i ++)
		{
			    if (i > lPagelen) break;
//			    y1 = (int) (double) ((double) UbHeight + 
//					        i * tm.tmHeight *  
//							RowHeight);
			    y1 = UbHeight +  i * RowHeightP;
				y2 = y1 + tm.tmHeight;
				if (mousepos.y < y1 ||
					mousepos.y > y2)
				{
					continue;
				}
				for (j = 0; j < banz; j ++)
				{
					x1 = (DataForm.mask[j].pos[1] - diff) *
						 tm.tmAveCharWidth;
					x2 = x1 + (DataForm.mask[j].length * tm.tmAveCharWidth);
 		    		if (mousepos.x > x1 &&
			    		mousepos.x < x2)
					{
					        break;
					}
				}
	    		if (mousepos.x > x1 &&
		    		mousepos.x < x2)
				{
					break;
				}
		}
   		if (mousepos.x < x1 ||
    		mousepos.x > x2)
		{
				return FALSE;
		}

		if (i + scrollpos >= recanz) return FALSE;
		if (j >= banz) return FALSE;
		if ((ListFocus > 2) && RowMode && DataRow->mask[i + scrollpos].attribut != EDIT)
		{		
			return FALSE;
		}
		AktRow = i + scrollpos;
        if (RowMode &&  j == 0)
        {
		        return TRUE;
        }
		if ((ListFocus > 2) && (RowMode == 0) && (DataForm.mask[j].attribut != EDIT)) 
		{		
			return FALSE;
		}


        AktColumn = j;
        InvalidateRect (mamain3, &FocusRect, TRUE);
	    UpdateWindow (mamain3);
		SetFeldFocus0 (AktRow, AktColumn);
        if (GetKeyState (VK_CONTROL) < 0)
        {
            SwitchMarked (AktRow);
            ShowAktRow ();
        }
        if (GetKeyState (VK_SHIFT) < 0)
        {
            SetToMark (AktRow);
            InvalidateRect (mamain3, NULL, FALSE);
            UpdateWindow (mamain3);
        }
		return TRUE;
}

void ListClass::DestroyFocusWindow ()
/**
Editfenster loeschen.
**/
{
	       if (FocusWindow)
		   {
			   DestroyWindow (FocusWindow);
			   FocusWindow = NULL;
		   }
}


void ListClass::SetFeldEdit0 (int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  int x,y ,cx, cy;
		  int diff;
		  HANDLE hInstance;
		  SIZE size;
		  HDC hdc;
          DWORD style = 0;

          if (banz == 0) return;

		  hdc = GetDC (mamain3);
 		  GetTextExtentPoint32 (hdc, "X", 1, &size);
		  ReleaseDC (mamain3, hdc);
          hInstance = (HANDLE) GetWindowLong (mamain3, 
                                               GWL_HINSTANCE);
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }

          char *p;
          if (RowMode == 0)
          {
                   p = strchr (ziel[spalte].feldname, '$');
                   if (p != NULL)
                   {
                         strcpy (AktItem, p + 1);
                   }
                   else
                   {
                         strcpy (AktItem, ziel[spalte].feldname);
                   }
          }
          else
          {
                   p = strchr (LstZiel[zeile].feldname, '$');
                   if (p != NULL)
                   {
                          strcpy (AktItem, p + 1);
                   }
                   else
                   {
                          strcpy (AktItem, LstZiel[zeile].feldname);
                   }

          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);

		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());


//		  rect.top = (int) (double) ((double) UbHeight + (
//			                zeile - scrollpos) * 
//                             tm.tmHeight * RowHeight - 2);
		  rect.top = UbHeight + (zeile - scrollpos) * RowHeightP - 2;
//          rect.top += (int) (double) ((double) 
//			             DataForm.mask[spalte].pos[0] * tm.tmHeight 
//						       * RowHeight);
          rect.top += DataForm.mask[spalte].pos[0] * RowHeightP;
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
//		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
//			          tm.tmAveCharWidth;
//		  rect.right = rect.left + DataForm.mask[spalte].length *
//			          tm.tmAveCharWidth;  
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          size.cx;
		  rect.right = rect.left + DataForm.mask[spalte].length *
			          size.cx;  
 		  memcpy (&FocusRect, &rect, sizeof (RECT));

//		  y  = (int) (double) ((double) UbHeight + 
//		  (zeile - scrollpos) * tm.tmHeight * RowHeight);
		  y  = UbHeight + (zeile - scrollpos) * RowHeightP;
//          y += (int) (double) ((double) 
//			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
          y += DataForm.mask[spalte].pos[0] * RowHeightP;
		  y = GetMidPos (y);
		  if (y < UbHeight) return;


		  cy = tm.tmHeight + 2;
//		  x  = (DataForm.mask[spalte].pos[1] - diff) *
//			   tm.tmAveCharWidth;

		  x  = (DataForm.mask[spalte].pos[1] - diff) *
			   size.cx;
          if (RowMode == 0)
          {
//		        cx = DataForm.mask[spalte].length *
//			              tm.tmAveCharWidth;  
		        cx = DataForm.mask[spalte].length *
			              size.cx;  
               if (strchr (DataForm.mask[spalte].picture, 'f') ||
                   (strchr (DataForm.mask[spalte].picture, 'd') &&
                        strchr (DataForm.mask[spalte].picture, 'y') == NULL))
               {
                        style |= ES_RIGHT;
               }
          }
          else
          {
//                cx = LstZiel[zeile].feldlen * tm.tmAveCharWidth;
                cx = LstZiel[zeile].feldlen * size.cx;
          }

          FocusWindow = CreateWindowEx (
			                        0,
                                    "Edit",
                                    "",
                                    WS_CHILD | ES_MULTILINE | style, 
									x, y, cx, cy,
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
  		  if (EditFont == NULL)
		  {
	           	stdfont ();
		        EditFont = SetWindowFont (mamain3);
		  }
          SendMessage (FocusWindow,  WM_SETFONT, (WPARAM) EditFont, 0);
		  ShowWindow (FocusWindow, SW_SHOWNORMAL);
		  UpdateWindow (FocusWindow);
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowWindowText (rect.left, rect.top);
          SendMessage (FocusWindow, EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
}


void ListClass::SetFocusFrame (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;
          int ubspalte;
		  int plus;
		  SIZE size;


		  if (ListFocus < 4) return;

 		  GetTextExtentPoint32 (hdc, "X", 1, &size);
          ubspalte = spalte;

          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (ubspalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }

          char *p;
          if (RowMode == 0)
          {
                   p = strchr (ziel[spalte].feldname, '$');
                   if (p != NULL)
                   {
                         strcpy (AktItem, p + 1);
                   }
                   else
                   {
                         strcpy (AktItem, ziel[spalte].feldname);
                   }
          }
          else
          {
                   p = strchr (LstZiel[zeile].feldname, '$');
                   if (p != NULL)
                   {
                          strcpy (AktItem, p + 1);
                   }
                   else
                   {
                          strcpy (AktItem, LstZiel[zeile].feldname);
                   }

          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());
//		  rect.top = (int) (double) ((double) UbHeight + 
//			         (zeile - scrollpos) * tm.tmHeight *  
//					  RowHeight);
		  rect.top = UbHeight + (zeile - scrollpos) * RowHeightP;
//          rect.top += (int) (double) ((double) 
//			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
          rect.top += DataForm.mask[spalte].pos[0] * RowHeightP;
//		  rect.bottom = (int) (double) ((double) rect.top + tm.tmHeight * RowHeight);
		  rect.bottom = rect.top + RowHeightP;
		  rect.left = (UbForm.mask[ubspalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
//		  rect.left = (DataForm.mask[ubspalte].pos[1] - diff) *
//			          size.cx;
          if (RowMode == 0)
          {
		        rect.right = rect.left + (UbForm.mask[ubspalte].length) *
			                 tm.tmAveCharWidth;  
//		        rect.right = rect.left + (UbForm.mask[ubspalte].length) *
//			                 size.cx;  
          }
          else
          {
//                rect.right = rect.left + (LstZiel[zeile].feldlen + 2) *
  //                           tm.tmAveCharWidth;
                rect.right = rect.left + (LstZiel[zeile].feldlen + 2) *
                           size.cx;
          }
		  if (RowHeight == (double) 1.0)
		  {
               rect.left -= 1;
               rect.right += 1;
               rect.top -= 1;
               rect.bottom += 1;
			   plus = 1;
		  }
		  else
		  {
               rect.left -= 1;
               rect.right += 1;
               rect.top -= 1;
               rect.bottom += 1;
  			   plus = 2;
		  }

          if (BkColor == BLUECOL)
          {
                 hBrush = CreateSolidBrush (WHITECOL);
          }
          else if (BkColor == BLACKCOL)
          {
                 hBrush = CreateSolidBrush (WHITECOL);
          }
          else
          {
                 hBrush = CreateSolidBrush (BLACKCOL);
          }
          SelectObject (hdc, GetStockObject (PS_DASH));
          FrameRect (hdc, &rect, hBrush);

          rect.top -= plus;
          rect.left -= plus;
          rect.bottom += plus;
          rect.right += plus;
          FrameRect (hdc, &rect, hBrush);
		  memcpy (&FocusRect, &rect, sizeof (RECT));

          DeleteObject (hBrush);
		  AktRow    = zeile;
          AktColumn = spalte;
}



void ListClass::SetFeldEdit (int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  int x,y ,cx, cy;
		  int diff;
		  HANDLE hInstance;
		  HDC hdc;
		  SIZE size;

          if (banz == 0) return;

		  hdc = GetDC (mamain3);
 		  GetTextExtentPoint32 (hdc, "X", 1, &size);
		  ReleaseDC (mamain3, hdc);
          hInstance = (HANDLE) GetWindowLong (mamain3, 
                                               GWL_HINSTANCE);
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }

          char *p;
          if (RowMode == 0)
          {
                   p = strchr (ziel[spalte].feldname, '$');
                   if (p != NULL)
                   {
                         strcpy (AktItem, p + 1);
                   }
                   else
                   {
                         strcpy (AktItem, ziel[spalte].feldname);
                   }
          }
          else
          {
                   p = strchr (LstZiel[zeile].feldname, '$');
                   if (p != NULL)
                   {
                          strcpy (AktItem, p + 1);
                   }
                   else
                   {
                          strcpy (AktItem, LstZiel[zeile].feldname);
                   }

          }
		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);

		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());


//		  rect.top = (int) (double) ((double) UbHeight + (
//			                zeile - scrollpos) * 
//                             tm.tmHeight * RowHeight - 2);
		  rect.top = UbHeight + (zeile - scrollpos) * RowHeightP - 2;
//          rect.top += (int) (double) ((double) 
//			             DataForm.mask[spalte].pos[0] * tm.tmHeight 
//						       * RowHeight);
          rect.top += DataForm.mask[spalte].pos[0] * RowHeightP;
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
//		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
//			          tm.tmAveCharWidth;
//		  rect.right = rect.left + DataForm.mask[spalte].length *
//			          tm.tmAveCharWidth;  
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          size.cx;
		  rect.right = rect.left + DataForm.mask[spalte].length *
			          size.cx;  
 		  memcpy (&FocusRect, &rect, sizeof (RECT));

//		  y  = (int) (double) ((double) UbHeight + 
//		  (zeile - scrollpos) * tm.tmHeight * RowHeight);
		  y  = UbHeight + (zeile - scrollpos) * RowHeightP;
//          y += (int) (double) ((double) 
//			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
          y += DataForm.mask[spalte].pos[0] * RowHeightP;
		  y = GetMidPos (y);
		  if (y < UbHeight) return;


		  cy = tm.tmHeight + 2;
//		  x  = (DataForm.mask[spalte].pos[1] - diff) *
//			   tm.tmAveCharWidth;
		  x  = (DataForm.mask[spalte].pos[1] - diff) *
			   size.cx;
          if (RowMode == 0)
          {
//		        cx = DataForm.mask[spalte].length *
//			              tm.tmAveCharWidth;  
		        cx = DataForm.mask[spalte].length *
			              size.cx;  
          }
          else
          {
//                cx = LstZiel[zeile].feldlen * tm.tmAveCharWidth;
                cx = LstZiel[zeile].feldlen * size.cx;
          }

          FocusWindow = CreateWindowEx (
			                        0,
                                    "Edit",
                                    "",
                                    WS_CHILD | WS_BORDER, 
									x, y, cx, cy,
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
  		  if (EditFont == NULL)
		  {
	           	stdfont ();
		        EditFont = SetWindowFont (mamain3);
		  }
          SendMessage (FocusWindow,  WM_SETFONT, (WPARAM) EditFont, 0);
		  ShowWindow (FocusWindow, SW_SHOWNORMAL);
		  UpdateWindow (FocusWindow);
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowWindowText (rect.left, rect.top);
          SendMessage (FocusWindow, EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
}


void ListClass::SetFeldEdit1 (int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  int x,y ,cx, cy;
		  int diff;
		  HANDLE hInstance;
		  SIZE size;
		  HDC hdc;

          if (banz == 0) return;

		  hdc = GetDC (mamain3);
 		  GetTextExtentPoint32 (hdc, "X", 1, &size);
		  ReleaseDC (mamain3, hdc);
          hInstance = (HANDLE) GetWindowLong (mamain3, 
                                               GWL_HINSTANCE);
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }

          char *p;
          if (RowMode == 0)
          {
                   p = strchr (ziel[spalte].feldname, '$');
                   if (p != NULL)
                   {
                         strcpy (AktItem, p + 1);
                   }
                   else
                   {
                         strcpy (AktItem, ziel[spalte].feldname);
                   }
          }
          else
          {
                   p = strchr (LstZiel[zeile].feldname, '$');
                   if (p != NULL)
                   {
                          strcpy (AktItem, p + 1);
                   }
                   else
                   {
                          strcpy (AktItem, LstZiel[zeile].feldname);
                   }

          }
		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);

		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());


		  rect.top = UbHeight + (zeile - scrollpos) * RowHeightP - 2;
          rect.top += DataForm.mask[spalte].pos[0] * RowHeightP;
		  rect.top = GetMidPos (rect.top);

		  rect.bottom = rect.top + tm.tmHeight;
//		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
//			          tm.tmAveCharWidth;
//		  rect.right = rect.left + DataForm.mask[spalte].length *
//			          tm.tmAveCharWidth;  
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          size.cx;
		  rect.right = rect.left + DataForm.mask[spalte].length *
			          size.cx;  
 		  memcpy (&FocusRect, &rect, sizeof (RECT));

		  y  = UbHeight + (zeile - scrollpos) * RowHeightP;
          y += DataForm.mask[spalte].pos[0] * RowHeightP;
		  y = GetMidPos (y);
		  if (y < UbHeight) return;


		  cy = tm.tmHeight + 5;
//		  x  = (DataForm.mask[spalte].pos[1] - diff) *
//			   tm.tmAveCharWidth;
		  x  = (DataForm.mask[spalte].pos[1] - diff) *
			   size.cx;
          if (RowMode == 0)
          {
//		        cx = DataForm.mask[spalte].length *
//			              tm.tmAveCharWidth;  
		        cx = DataForm.mask[spalte].length *
			              size.cx;  
          }
          else
          {
//                cx = LstZiel[zeile].feldlen * tm.tmAveCharWidth;
                cx = LstZiel[zeile].feldlen * size.cx;
          }


          FocusWindow = CreateWindowEx (
			                        WS_EX_CLIENTEDGE,
                                    "Edit",
                                    "",
                                    WS_CHILD | ES_MULTILINE, 
									x, y, cx, cy,
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
  		  if (EditFont == NULL)
		  {
	           	stdfont ();
		        EditFont = SetWindowFont (mamain3);
		  }
          SendMessage (FocusWindow,  WM_SETFONT, (WPARAM) EditFont, 0);
		  ShowWindow (FocusWindow, SW_SHOWNORMAL);
		  UpdateWindow (FocusWindow);
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowWindowText (rect.left, rect.top);
          SendMessage (FocusWindow, EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
}


void ListClass::SetFeldFrame (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;
		  SIZE size;
		  
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

 		  GetTextExtentPoint32 (hdc, "X", 1, &size);
          diff = 0;
          if (LineForm.frmstart)
		  {
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }

          char *p;
          if (RowMode == 0)
          {
                   p = strchr (ziel[spalte].feldname, '$');
                   if (p != NULL)
                   {
                         strcpy (AktItem, p + 1);
                   }
                   else
                   {
                         strcpy (AktItem, ziel[spalte].feldname);
                   }
          }
          else
          {
                   p = strchr (LstZiel[zeile].feldname, '$');
                   if (p != NULL)
                   {
                          strcpy (AktItem, p + 1);
                   }
                   else
                   {
                          strcpy (AktItem, LstZiel[zeile].feldname);
                   }

          }
		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());

		  rect.top = UbHeight + (zeile - scrollpos) * RowHeightP;
          rect.top += DataForm.mask[spalte].pos[0] * RowHeightP;
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
		  /*
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
		  */
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			           size.cx;
          if (RowMode == 0)
          {
//		        rect.right = rect.left + DataForm.mask[spalte].length *
//		                    tm.tmAveCharWidth;  
		        rect.right = rect.left + DataForm.mask[spalte].length *
		                    size.cx;  
          }
          else
          {
//                rect.right = rect.left + LstZiel[zeile].feldlen *
//                             tm.tmAveCharWidth;
                rect.right = rect.left + LstZiel[zeile].feldlen *
                             size.cx;
          }
		  memcpy (&FocusRect, &rect, sizeof (RECT));


          if (BkColor == BLUECOL)
          {
                 hBrush = CreateSolidBrush (WHITECOL);
          }
          else
          {
                 hBrush = CreateSolidBrush (BLUECOL);
          }

          SelectObject (hdc, hBrush);
          SelectObject (hdc, GetStockObject (NULL_PEN));
          Rectangle (hdc,rect.left, rect.top, rect.right, rect.bottom);
          DeleteObject (hBrush); 
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowFocusText (hdc, rect.left, rect.top);
}


void ListClass::SetFeldFocus (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;
		  SIZE size;


          if (RowMode) 
		  {
              if (SetKey9)
			  {
	                SetKey9 (LstZiel[zeile].feldname);
			  }
		  }
          else if (SetKey9)
		  {
                SetKey9 (ziel[spalte].feldname);
		  }
		  DestroyFocusWindow ();
		  if (ListFocus == 0) return;

          PrintUSlinesSatzNr (hdc);

		  if (ListFocus == 2)
		  {
		           SetFeldFrame (hdc, zeile, spalte);
                   PrintSlinesSatzNr (hdc);
		           return;
		  }

		  if (ListFocus == 3)
		  {
		           SetFeldEdit (zeile, spalte);
                   if (NoRecNr == FALSE)
                   {
                          PrintSlinesSatzNr (hdc);
                   }
		           return;
		  }

		  if (ListFocus == 4)
		  {
		           SetFeldEdit0 (zeile, spalte);
		           SetFocusFrame (hdc, zeile, spalte);
                   if (NoRecNr == FALSE)
                   {
                          PrintSlinesSatzNr (hdc);
                   }
		           return;
		  }
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

 		  GetTextExtentPoint32 (hdc, "X", 1, &size);
          diff = 0;
          if (LineForm.frmstart)
		  {
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }

          char *p;
          if (RowMode == 0)
          {
                   p = strchr (ziel[spalte].feldname, '$');
                   if (p != NULL)
                   {
                         strcpy (AktItem, p + 1);
                   }
                   else
                   {
                         strcpy (AktItem, ziel[spalte].feldname);
                   }
          }
          else
          {
                   p = strchr (LstZiel[zeile].feldname, '$');
                   if (p != NULL)
                   {
                          strcpy (AktItem, p + 1);
                   }
                   else
                   {
                          strcpy (AktItem, LstZiel[zeile].feldname);
                   }

          }
		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());
		  rect.top = UbHeight + (zeile - scrollpos) * RowHeightP;
          rect.top += DataForm.mask[spalte].pos[0] * RowHeightP;
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
//		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
//			          tm.tmAveCharWidth;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          size.cx;
          if (RowMode == 0)
          {
//		        rect.right = rect.left + DataForm.mask[spalte].length *
//			                 tm.tmAveCharWidth;  
		        rect.right = rect.left + DataForm.mask[spalte].length *
			                 size.cx;  
          }
          else
          {
//                rect.right = rect.left + LstZiel[zeile].feldlen *
//                            tm.tmAveCharWidth;
                rect.right = rect.left + LstZiel[zeile].feldlen *
                             size.cx;
          }
		  memcpy (&FocusRect, &rect, sizeof (RECT));


          if (BkColor == BLUECOL)
          {
                 hBrush = CreateSolidBrush (WHITECOL);
          }
          else if (BkColor == BLACKCOL)
          {
                 hBrush = CreateSolidBrush (WHITECOL);
          }
          else
          {
                 hBrush = CreateSolidBrush (BLACKCOL);
          }
          SelectObject (hdc, GetStockObject (PS_DASH));
          FrameRect (hdc, &rect, hBrush);

          rect.top --;
          rect.left --;
          rect.bottom ++;
          rect.right ++;
          FrameRect (hdc, &rect, hBrush);
		  memcpy (&FocusRect, &rect, sizeof (RECT));

          DeleteObject (hBrush);
		  AktRow    = zeile;
          AktColumn = spalte;
          PrintSlinesSatzNr (hdc);
}

int ListClass::DelRec (void)
/**
Satz aus Liste loeschen.
**/
{
          int i;
 		  SCROLLINFO scinfo;
          
          if (ListFocus == 0) return recanz;

          recanz --;
          for (i = AktRow; i < recanz; i ++)
          {
              SwSaetze[i] = SwSaetze[ i + 1];
          }
          
          if (mamain3)
          {
             SendMessage (mamain2, WM_SIZE, NULL, NULL);
             InvalidateRect (mamain3, 0, TRUE);
             TestVTrack ();
             if (vTrack)
             {
 		          scinfo.cbSize = sizeof (SCROLLINFO);
		          scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		          scinfo.nMin   = 0;
		          scinfo.nMax   = recanz - 1;
		          if (lPagelen)
                  {
		             scinfo.nPage  = 1;
                  }
		          else
                  {
			         scinfo.nPage = 0;
                  }
                  SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
             }
          }
          
          return recanz;
}
          
void ListClass::TestChoise (void)
/**
Testen, ob ein Auswahlfenster auf diesem Feld vorgesehen ist.
**/
{
          if (RowMode) 
          {
              if (DoChoise)
			  {
	                DoChoise (LstZiel[AktRow].feldname);
			  }
              return;
          }
          if (DoChoise)
		  {
                DoChoise (ziel[AktColumn].feldname);
		  }
}


void ListClass::FocusLeft (void)
/**
Focus nachh rechts setzen.
**/
{
          if (RowMode) 
          {
              if (DoAfter)
			  {
	                DoAfter (LstZiel[AktRow].feldname);
			  }
              PriorPageRow ();
              return;
          }

          if (DoAfter)
		  {
                DoAfter (ziel[AktColumn].feldname);
		  }
		  if (AktColumn <= 0) return;

	      AktColumn = PriorColumn ();
		  while (AktColumn < DataForm.frmstart) 
          {
              ScrollLeft ();
          }

		  if (AktColumn < DataForm.frmstart) 
          {
              ScrollLeft ();
          }
          else
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
		      UpdateWindow (mamain3);
          }
		  SetFeldFocus0 (AktRow, AktColumn);
}
		  

void ListClass::FocusRight (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;
          int x;
          int diff;
          int Scrolled;

          if (RowMode)
          {
              if (DoAfter)
			  {
                           DoAfter (LstZiel[AktRow].feldname);
			  }
              NextPageRow ();
              return;
          }


//  if (AktColumn >= banz - 1) return;

          if (DoAfter)
		  {
                DoAfter (ziel[AktColumn].feldname);
		  }
	      AktColumn = NextColumn ();
		  while (AktColumn < DataForm.frmstart) 
          {
              ScrollRight ();
          }

          GetWindowRect (mamain3, &rect);


          diff = Scrolled = 0;
          if (LineForm.frmstart)
          {
			       if (NoRecNr == FALSE)
				   {
                            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                                              fSatzNr.mask[0].length;
				   }
				   else
				   {
                            diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
				   }
          }
          x = (DataForm.mask[AktColumn].pos[1] - diff + 
               DataForm.mask[AktColumn].length) *
                   tm.tmAveCharWidth;
          if (x <= rect.right)
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
	  	      UpdateWindow (mamain3);
 		      SetFeldFocus0 (AktRow, AktColumn);
              return;
          }
          ScrollRight ();
		  if (NoRecNr == FALSE)
		  {
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                             fSatzNr.mask[0].length;
		  }
		  else
		  {
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
		  }
          x = (DataForm.mask[AktColumn].pos[1] - diff + 
               DataForm.mask[AktColumn].length) *
                   tm.tmAveCharWidth;
          while (x > rect.right)
          {
              ScrollRight ();
			  if (NoRecNr == FALSE)
			  {
                       diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                                  fSatzNr.mask[0].length;
			  }
			  else
			  {
                       diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			  }
               x = (DataForm.mask[AktColumn].pos[1] - diff + 
                       DataForm.mask[AktColumn].length) *
                       tm.tmAveCharWidth;
          }
	      SetFeldFocus0 (AktRow, AktColumn);
}

int ListClass::AktCol (void)
/**
N�chste Spalte suchen.
**/
{

          int i;


          i = AktColumn;
          while (TRUE)
          {

              if (DataForm.mask[i].attribut == EDIT || ListFocus < 3)
              {
                  break;
              }
              i ++;
              if (i == DataForm.fieldanz)
              {
                  return 0;
              }
          }
		  AktColumn = i;
          return i;
}


int ListClass::AktColR (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktRow;
          while (TRUE)
          {
              if (DataRow->mask[i].attribut == EDIT || ListFocus < 3)
              {
                  break;
              }
              i ++;
              if (i == DataRow->fieldanz)
              {
                  return 0;
              }
          }
		  AktRow = i;
          return i;
}

          

int ListClass::FirstColumn (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = 0;

          while (TRUE)
          {

              if (DataForm.mask[i].attribut == EDIT || ListFocus < 3)
              {
                  return i;
              }
              i ++;
              if (i == DataForm.fieldanz)
              {
                  return 0;
              }
          }
          return i;
}

int ListClass::FirstColumnR (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = 0;

          while (TRUE)
          {
              if (DataRow->mask[i].attribut == EDIT || ListFocus < 3)
              {
                  return i;
              }
              i ++;
              if (i == DataRow->fieldanz)
              {
                  return 0;
              }
          }
          return i;
}

          

int ListClass::PriorColumn (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktColumn;

          while (TRUE)
          {
              i --;
              if (i < 0)
              {
                  i = DataForm.fieldanz - 1;
              }
              if (DataForm.mask[i].attribut == EDIT || ListFocus < 3)
              {
                  return i;
              }
          }
          return i;
}

		  
int ListClass::PriorColumnR (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktRow;

          while (TRUE)
          {
              i --;
              if (i < 0)
              {
                  i = DataRow->fieldanz - 1;
              }
              if (DataRow->mask[i].attribut == EDIT || ListFocus < 3)
              {
                  return i;
              }
          }
          return i;
}


int ListClass::NextColumn (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktColumn;

          if (i == DataForm.fieldanz - 1 && UbForm.frmstart > 0)
          {
              return i;
          }
          while (TRUE)
          {
              i ++;
              if (i == DataForm.fieldanz)
              {
                  i = 0;
              }
              if (DataForm.mask[i].attribut == EDIT || ListFocus < 3)
              {
                  return i;
              }
          }
          return i;
}

int ListClass::NextColumnR (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktRow;

          while (TRUE)
          {
              i ++;
              if (i == DataRow->fieldanz)
              {
                  i = 0;

              }
              if (DataRow->mask[i].attribut == EDIT || ListFocus < 3)
              {
                  return i;
              }
          }
          return i;
}

	        
void ListClass::FocusUp (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;
		  int SRow;
          
		  if (AktRow <= 0) return;

		  if (InAppend)
		  {
			  DeleteLine ();
		  }
          GetClientRect (mamain3, &rect);
          rect.top += tm.tmHeight;
		  if (NoRecNr == FALSE)
		  {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		  }

          if (RowMode)
          {
				 SRow = AktRow;
	             if (DoAfter)
				 {
			                DoAfter (LstZiel[AktRow].feldname);
				 }
				 AktRow = SRow;
				 SelRow = AktRow;
                 AktRow = PriorColumnR ();
          }

          else
          {
	              if (DoAfter)
				  {
			                DoAfter (ziel[AktColumn].feldname);
				  }
                  AktRow --;
          }
		  if (AktRow < scrollpos)
          {
//                    InvalidateRect (mamain3, &rect, TRUE);
                    InvalidateRect (mamain3, &FocusRect, TRUE);
                    ScrollUp (); 
          }
          else
          {
                    InvalidateRect (mamain3, &FocusRect, TRUE);
                    UpdateWindow (mamain3);
          }
          InAppend = 0;
          NewRec = 0;
          SetInsAttr ();
		  SetFeldFocus0 (AktRow, AktColumn);
}


void ListClass::SetFieldAttr (CHATTR *cha)
{
         int i;
         char *feldname;

         for (i = 0; i < UbForm.fieldanz; i ++)
         {
             feldname = UbForm.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, cha->feldname) == 0)
             {
                   break;
             }
         }
         if (i == DataForm.fieldanz) return;

         if (NewRec || InAppend)
         {
                DataForm.mask[i].attribut = cha->InsAttr;
         }
         else
         {
                DataForm.mask[i].attribut = cha->UpdAttr;
         }
}
             

void ListClass::SetInsAttr (void)
/**
Feldattribute wechseln.
**/
{
         int i;

         if (ChAttr == NULL) return;

         for (i = 0; ChAttr[i].feldname; i ++)
         {
             SetFieldAttr (&ChAttr[i]);
         }
}


          
void ListClass::FormatField (char *buffer)
/**
**/
{
		char format [10];
		double dwert;

		if (RowMode == 0) return;

 	    if (LstZiel[AktRow].feldtyp[0] != 'N')
		{
		   return;
		}

		dwert = ratod (buffer);
        dnformat (format, &LstZiel[AktRow]);
		sprintf (buffer, format, dwert);
}

void ListClass::FocusDown (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;
		  int SRow;

          GetClientRect (mamain3, &rect);
          rect.top += tm.tmHeight;
		  if (NoRecNr == FALSE)
		  {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		  }
          if (RowMode)
          {
				 SRow = AktRow;
   		         if (DoAfter)
				 {
			                DoAfter (LstZiel[AktRow].feldname);
				 }
				 AktRow = SRow;
				 SelRow = AktRow;
                 AktRow = NextColumnR ();
          }
          else
          {
   		          if (DoAfter)
				  {
			                DoAfter (ziel[AktColumn].feldname);
				  }
 	              if (AktRow >= recanz - 1) 
				  {
                             SetFeldFocus0 (AktRow, AktColumn);
				             return;
				  }
                  AktRow ++;
          }

          if (lPagelen < recanz && 
			  AktRow - scrollpos >= lPagelen - 1)
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
              ScrollDown ();
          }
          else
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
	   	      UpdateWindow (mamain3);
          }


		  if (InAppend)
		  {
               InAppend = 0;
               NewRec = 0;
               SetInsAttr ();
               DestroyFocusWindow ();
		       InvalidateRect (mamain3, &FocusRect, TRUE);
               SetPos (AktRow, FirstColumn ());
		  }
	      SetFeldFocus0 (AktRow, AktColumn);
}
  
    

void ListClass::SetFeldFocus0 (int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	    HDC hdc;

        SetFocus (mamain3);
		hdc = GetDC (mamain3);
        SetFeldFocus (hdc, zeile, spalte);
        ReleaseDC (mamain3, hdc);
        SelRow = AktRow;

        if (RowMode) 
        {
/*
              if (DoBefore)
			  {
	                DoBefore (LstZiel[AktRow].feldname);
			  }
*/

              return;
         }

         if (DoBefore)
         {
                DoBefore (ziel[AktColumn].feldname);
        }

}


void ListClass::SetDataStart (void)
/**
frmstart bei DataForms erhoehen.
**/
{

        DataForm.frmstart = UbForm.frmstart;
        LineForm.frmstart = UbForm.frmstart;
}

BOOL ListClass::MustPaint (int zeile)
/**
Feststellen, ob der Bereich neu gezeichnet werden muss.
**/
{
         int py;
         PAINTSTRUCT *ps;
         int Painttop;
         int Paintbottom;

         ps = &aktpaint;

/* Update-Region ermitteln.                              */

         py =  zeile;

         Painttop    = ps->rcPaint.top    -  tm.tmHeight;
         Paintbottom = ps->rcPaint.bottom +  tm.tmHeight;

         if (py < Painttop) return FALSE;
         if (py > Paintbottom) return FALSE;
         return TRUE;
}

void ListClass::PrintUSlinesSatzNr (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int i;
        int x, y, y1;
		int RHeight;
		SIZE size;

		RHeight = (int) (double) ((double) tm.tmHeight * RowHeight + 0.5);

        if (recanz == 0) return;
        if (NoRecNr) return;
		GetTextExtentPoint32 (hdc, "X", 1, &size);
//        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;
        x = fSatzNr.mask[0].length * size.cx - 1;

        hPen  = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        i = SelRow - scrollpos;

        SelectObject (hdc, hPenW);
        y = UbHeight + i * RowHeightP;
        y1 = y;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPen);
        y ++;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);
 
        SelectObject (hdc, hPenG);
        y = UbHeight + (i + 1) * RowHeightP - 1;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPen);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPen);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);


        SelectObject (hdc, hPenG);
        MoveToEx (hdc, x - 1, y1, NULL);
        LineTo (hdc,   x - 1, y + 1);

        SelectObject (hdc, hPen);
        MoveToEx (hdc, x - 2, y1, NULL);
        LineTo (hdc,   x - 2, y + 1);

        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}

void ListClass::PrintSlinesSatzNr (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int i;
        int x, y, y1;
		SIZE size;

        if (recanz == 0) return;
        if (NoRecNr) return;
		GetTextExtentPoint32 (hdc, "X", 1, &size);
//        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;
        x = fSatzNr.mask[0].length * size.cx - 1;

        hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        i = AktRow - scrollpos;

        SelectObject (hdc, hPenW);
        y = UbHeight + i * RowHeightP;
        y1 = y;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        y ++;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);
 
        SelectObject (hdc, hPen);
        y = UbHeight + (i + 1) * RowHeightP - 1;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPenG);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);

        SelectObject (hdc, hPen);
        MoveToEx (hdc, x, y1, NULL);
        LineTo (hdc,   x, y + 1);

        SelectObject (hdc, hPen);
        MoveToEx (hdc, x - 1, y1, NULL);
        LineTo (hdc,   x - 1, y + 1);

        SelectObject (hdc, hPenG);
        MoveToEx (hdc, x - 2, y1, NULL);
        LineTo (hdc,   x - 2, y + 1);

         
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}


void ListClass::PrintVlineSatzNr (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
		RECT rect;
        long x, y;
		SIZE size;

		GetTextExtentPoint32 (hdc, "X", 1, &size);
		GetClientRect (mamain3, &rect);


        if (NoRecNr) return;
        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

//        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;
        x = fSatzNr.mask[0].length * size.cx - 1;
        y = UbHeight + recanz * RowHeightP;
   	    if (y > rect.bottom) y = rect.bottom;
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, GRAYCOL);
        oldPen = SelectObject (hdc, hPen);

//        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 2;
        x = fSatzNr.mask[0].length * size.cx - 2;
        y = UbHeight + recanz * RowHeightP;
	    if (y > rect.bottom) y = rect.bottom;
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}

void ListClass::SetListLines (int cnr)
{
        LineColor = cnr;
        if (mamain3)
        {
                 InvalidateRect (mamain3, NULL, TRUE);
                 UpdateWindow (mamain3);
        }
}
     

void ListClass::PrintVlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        long x, y;
		RECT rect;
        int diff;
		SIZE size;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintVlineSatzNr (hdc);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
        if (LineColor > 11) return;
        if (LineColor > 7) return;
        if (LineForm.mask == NULL) return;

    
		GetClientRect (mamain3, &rect);
		rect.bottom += 10;
        diff = 0;
        if (LineForm.frmstart)
        {
			if (NoRecNr == 0)
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                               fSatzNr.mask[0].length;
			}
			else
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
        }
        hPen = CreatePen (PS_SOLID, 0, lcol[LineColor]);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
//              x = (LineForm.mask[i].pos[1] - diff) * 
//                   tm.tmAveCharWidth;
              x = (LineForm.mask[i].pos[1] - diff) * 
                   size.cx;
              y = UbHeight + recanz * RowHeightP;
			  if (y > rect.bottom) y = rect.bottom;
              MoveToEx (hdc, x, tm.tmHeight, NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        PrintVlineSatzNr (hdc);
}

void ListClass::PrintPVlines (HDC hdc, RECT *rect)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        long x, y;
        int diff;
		SIZE size;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


//        PrintVlineSatzNr (hdc);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
        if (LineColor > 11) return;
        if (LineColor > 7) return;
        if (LineForm.mask == NULL) return;

    
    	rect->bottom += 10;
        diff = 0;
        if (LineForm.frmstart)
        {
			if (NoRecNr == 0)
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                               fSatzNr.mask[0].length;
			}
			else
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
        }
        hPen = CreatePen (PS_SOLID, 0, lcol[LineColor]);
        oldPen = SelectObject (hdc, hPen);

        for (i = 0; i < LineForm.fieldanz; i ++)
        {
//              x = (LineForm.mask[i].pos[1] - diff) * 
//                   tm.tmAveCharWidth;
              x = (LineForm.mask[i].pos[1] - diff) * 
                   size.cx;
              y = UbHeight + recanz * RowHeightP;
			  if (y > rect->bottom) y = rect->bottom;
              MoveToEx (hdc, x, tm.tmHeight, NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
//        PrintVlineSatzNr (hdc);
}


void ListClass::PrintVline (HDC hdc, int z)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;
		SIZE size;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintVlineSatzNr (hdc);
        if (LineColor > 11) return;
        if (LineColor > 7) return;
        if (LineForm.mask == NULL) return;

		GetTextExtentPoint32 (hdc, "X", 1, &size);
        diff = 0;
        if (LineForm.frmstart)
        {
			if (NoRecNr == 0)
			{
               diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                              fSatzNr.mask[0].length;
			}
			else
			{
               diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
        }
        hPen = CreatePen (PS_SOLID, 0, lcol[LineColor]);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
//              x = (LineForm.mask[i].pos[1] - diff) * 
//                   tm.tmAveCharWidth;
              x = (LineForm.mask[i].pos[1] - diff) * 
                   size.cx;
              y = UbHeight + z * RowHeightP;
              MoveToEx (hdc, x, y, NULL);
              y = UbHeight +  (z  + 1) * RowHeightP;
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        PrintVlineSatzNr (hdc);
}


void ListClass::PrintHlinesSatzNr (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int i;
        int x, y;
		SIZE size;


        if (NoRecNr) return;
		GetTextExtentPoint32 (hdc, "X", 1, &size);
//        x = fSatzNr.mask[0].length * tm.tmAveCharWidth;
        x = fSatzNr.mask[0].length * size.cx;

        hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i < lPagelen; i ++)
        {
              SelectObject (hdc, hPenW);
              y =  UbHeight + i * RowHeightP;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x - 1, y);
              }

              SelectObject (hdc, hPen);
              y = UbHeight + (i + 1) * RowHeightP;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }

              SelectObject (hdc, hPenG);
              y --;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x - 1, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}


void ListClass::PrintLastHline (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};

        GetClientRect (mamain3, &rect);
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);
        y = UbHeight + (recanz - scrollpos) * RowHeightP;
        if (MustPaint (y))
        {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}

void ListClass::PrintLastPHline (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};

        rect.top   = 0;
        rect.left  = 0;
        rect.bottom = GetDeviceCaps (hdc, VERTRES);
        rect.right  = GetDeviceCaps (hdc, HORZRES);

        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);
        y = UbHeight + (recanz - scrollpos) * RowHeightP;
        if (y < rect.bottom)
        {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}


void ListClass::PrintHlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int i;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintHlinesSatzNr (hdc);
        if (LineColor > 11) return;
        if (LineColor > 3 && LineColor < 8) 
		{
			PrintLastHline (hdc);
			return;
		}
        GetClientRect (mamain3, &rect);
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i <= lPagelen; i ++)
        {

              y = UbHeight +  i * RowHeightP;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        PrintHlinesSatzNr (hdc);
}

void ListClass::PrintPHlines (HDC hdc, RECT *rect)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


//        PrintHlinesSatzNr (hdc);
        if (LineColor > 11) return;

        if (LineColor > 3 && LineColor < 8) 
		{
			PrintLastPHline (hdc);
			return;
		}


        x = rect->right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i <= pagelen0; i ++)
        {
              if (i + printscrollpos > recanz) break;
              y = UbHeight +  i * RowHeightP;
              if (y < rect->bottom)
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
//        PrintHlinesSatzNr (hdc);
}

void ListClass::PrintHline (HDC hdc, int i)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        if (LineColor > 11) return;
        if (LineColor > 3 && LineColor < 8) return;
        GetClientRect (mamain3, &rect);
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);

        y = UbHeight + 	i * RowHeightP;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}

void ListClass::FillPrintUb (char *dest)
/**
String fuellen.
**/
{
        int i;
        char buffer [1024];
		form *frm;

        memset (dest, 0, 0x1000);
		frm = &UbForm;

        strcat (dest, " ");
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
                   ToFormat (buffer, &frm->mask[i]);
                   strcat (dest, buffer);           
        }
}

void ListClass::FillPrPrintUb (char *dest)
/**
String fuellen.
**/
{
        int i;
        char buffer [1024];
		form *frm;

        memset (dest, 0, 0x1000);
		frm = &PrintUbForm;

        strcat (dest, " ");
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
                   ToFormat (buffer, &frm->mask[i]);
                   strcat (dest, buffer);           
        }
}

void ListClass::FillPrintRow (char *dest)
/**
String fuellen.
**/
{
        int i;
        char buffer [1024];
		form *frm;

        memset (dest, 0, 0x1000);
		frm = &DataForm;

        strcat (dest, " ");

        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
//                   ToFormat (buffer, &frm->mask[i]);
                   ToFormatEx (buffer, &frm->mask[i], &UbForm.mask[i]);
                   strcat (dest, buffer);           
        }
}

void ListClass::FillPrPrintRow (char *dest)
/**
String fuellen.
**/
{
        int i;
        char buffer [1024];
		form *frm;

        memset (dest, 0, 0x1000);
		frm = &PrintForm;

//        strcat (dest, " ");
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
                   ToFormat (buffer, &frm->mask[i]);
//                   ToFormatEx (buffer, &frm->mask[i], &PrintUbForm.mask[i]);
                   strcat (dest, buffer);           
        }
}

int ListClass::GetUbFrmlen (void)
{
        int i;
        int len;

        len = 0;
        for (i = 0; i < UbForm.fieldanz; i ++)
        {
            len += UbForm.mask[i].length;
        }
        return len;
}


#ifdef _OLDLIST
void ListClass::FillFrmRow (char *dest, form *frm)
/**
String fuellen.
**/
{
        int i;
        char buffer [1024];

        memset (dest, 0, 0x1000);

        strcat (dest, " ");
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
                   ToFormat (buffer, &frm->mask[i]);
                   strcat (dest, buffer);           
        }
}
#else


BOOL ListClass::PrintPosText (HDC hdc, char *satz, int y , int ldiff, SIZE *size)
{
        int x = 0;
        int anz;
        int text;

        if (memcmp (satz, "$POS", 4) != 0)
        {
            return FALSE;
        }

        satz += 5;
        anz = wsplit (satz, ";");
        if (anz == 0) 
        {
            return FALSE;
        }

        if (anz > 1)
        {
            x = atoi (wort[0]);
            text = 1;
        }
        if (anz > 2)
        {
            y = atoi (wort[1]);
            y = UbHeight + y * RowHeightP - scrollpos * RowHeightP; 
  		    y = GetMidPos (y);
            text = 2;
            if (PosPrintMode == FALSE)
            {
                return TRUE;
            }
        }
        else if (PosPrintMode)
        {
            return TRUE;
        }

        if (PrintHdc)
        {
            if (y < PageStart ||
                y >= PageEnd)
            {
                        return TRUE;
            }
            else
            {
                        y -= PageStart;
            }
        }

        int tlen =  strlen ((char *) wort [text]);
        if (ldiff >= x + tlen)
        {
               return TRUE;
        }

        tlen  *= size->cx;
        ldiff *= size->cx;
        x     *= size->cx;           
        SetTextColor (hdc,Color);
        SetBkColor (hdc,BkColor);
        TextOut (hdc, x - ldiff, y, wort[text], strlen (wort [text]));
        StopPrint = FALSE;
        return TRUE;
}


BOOL ListClass::PrintLines (HDC hdc, char *satz, int y , int ldiff, SIZE *size)
{
        int x = 0;
        int tlen = 80;
        int anz;

        if (memcmp (satz, "$BLACKVLINE", 11) == 0);
        else if (memcmp (satz, "$LTGRAYVLINE", 11) == 0);
        else if (memcmp (satz, "$GRAYVLINE", 10) == 0);
        else if (memcmp (satz, "$WHITEVLINE", 11) == 0);
        else if (memcmp (satz, "$BLUEVLINE", 10) == 0);
        else if (memcmp (satz, "$RAISEDVLINE", 12) == 0);
        else if (memcmp (satz, "$BLACKLINE", 10) == 0);
        else if (memcmp (satz, "$LTGRAYLINE", 11) == 0);
        else if (memcmp (satz, "$GRAYLINE", 9) == 0);
        else if (memcmp (satz, "$WHITELINE", 10) == 0);
        else if (memcmp (satz, "$BLUELINE", 9) == 0);
        else if (memcmp (satz, "$RAISEDLINE", 11) == 0);
        else
        {
            return FALSE;
        }

        anz = wsplit (satz, ";");
        if (anz > 1)
        {
            tlen = atoi (wort[1]);
        }
        if (anz > 2)
        {
            x = atoi (wort[2]);
        }

        if (anz > 3)
        {
            y = atoi (wort[3]);
//            y *= size->cy;
            y = UbHeight + y * RowHeightP; 
//  		    y = GetMidPos (y);
            if (anz > 4)
            {
                y += atoi (wort[4]);
            }
            y -= scrollpos * RowHeightP;
            if (PosPrintMode == FALSE)
            {
                return TRUE;
            }
        }
        else if (PosPrintMode || ldiff + x >= tlen)
        {
            return TRUE;
        }



//        tlen  *= size->cx;
        ldiff *= size->cx;
        x     *= size->cx;           

        if (PrintHdc)
        {
            ldiff = 0;
            if (y >= PageEnd)
            {
                        return TRUE;
            }
            else if (memcmp (satz, "$BLACKVLINE", 11) == 0)
            {
                      tlen *= RowHeightP; 
                      if (y + tlen > PageStart)
                      {
                          y -= PageStart;
                          if ( y < 0)
                          {
                              tlen += y;
                              y = 0;
                          }
                      }
            }
            else if (memcmp (satz, "$LTGRAYVLINE", 12) == 0)
            {
                      tlen *= RowHeightP; 
                      if (y + tlen > PageStart)
                      {
                          y -= PageStart;
                          if ( y < 0)
                          {
                              tlen += y;
                              y = 0;
                          }
                      }
            }
            else if (memcmp (satz, "$GRAYVLINE", 10) == 0)
            {
                      tlen *= RowHeightP; 
                      if (y + tlen > PageStart)
                      {
                          y -= PageStart;
                          if ( y < 0)
                          {
                              tlen += y;
                              y = 0;
                          }
                      }
            }
            else if (memcmp (satz, "$WHITEVLINE", 11) == 0)
            {
                      tlen *= RowHeightP; 
                      if (y + tlen > PageStart)
                      {
                          y -= PageStart;
                          if ( y < 0)
                          {
                              tlen += y;
                              y = 0;
                          }
                      }
            }
            else if (memcmp (satz, "$BLUEVLINE", 10) == 0)
            {
                      tlen *= RowHeightP; 
                      if (y + tlen > PageStart)
                      {
                          y -= PageStart;
                          if ( y < 0)
                          {
                              tlen += y;
                              y = 0;
                          }
                      }
            }
            else if (memcmp (satz, "$RAISEDVLINE", 12) == 0)
            {
                      tlen *= RowHeightP; 
                      if (y + tlen > PageStart)
                      {
                          y -= PageStart;
                          if ( y < 0)
                          {
                              tlen += y;
                              y = 0;
                          }
                      }
                      else
                      {
                          return TRUE;
                      }
            }

            else
            {
                        y -= PageStart;
            }

            if (y >= PageEnd)
            {
                        return TRUE;
            }
        }


        if (memcmp (satz, "$BLACKLINE", 10) == 0)
        {
                   tlen  *= size->cx;
                   HPEN hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, tlen - ldiff, y);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$LTGRAYLINE", 11) == 0)
        {
                   tlen  *= size->cx;
                   HPEN hPen  = CreatePen (PS_SOLID, 0, LTGRAYCOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x + tlen - ldiff, y);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$GRAYLINE", 9) == 0)
        {
                   tlen  *= size->cx;
                   HPEN hPen  = CreatePen (PS_SOLID, 0, GRAYCOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x + tlen - ldiff, y);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$WHITELINE", 10) == 0)
        {
                   tlen  *= size->cx;
                   HPEN hPen  = CreatePen (PS_SOLID, 0, WHITECOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x + tlen - ldiff, y);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$BLUELINE", 9) == 0)
        {
                   tlen  *= size->cx;
                   HPEN hPen  = CreatePen (PS_SOLID, 0, BLUECOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x + tlen - ldiff, y);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$RAISEDLINE", 11) == 0)
        {
                   tlen  *= size->cx;
                   HPEN hPen  = CreatePen (PS_SOLID, 0, GRAYCOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x + tlen - ldiff, y);
                   DeleteObject (SelectObject (hdc, oldPen)); 

                   hPen  = CreatePen (PS_SOLID, 0, WHITECOL);

                   oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x, y + 1, NULL);
                   LineTo (hdc, x + tlen - ldiff, y + 1);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$BLACKVLINE", 11) == 0)
        {
                   if (PrintHdc == FALSE)
                   {
                      tlen *= RowHeightP; 
                   }
                   if (ldiff > x)
                   {
                     return TRUE;
                   }
                   HPEN hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x - ldiff, y + tlen);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$LTGRAYVLINE", 12) == 0)
        {
                   if (PrintHdc == FALSE)
                   {
                      tlen *= RowHeightP; 
                   }
                   if (ldiff > x)
                   {
                     return TRUE;
                   }
                   HPEN hPen  = CreatePen (PS_SOLID, 0, LTGRAYCOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x - ldiff, y + tlen);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$GRAYVLINE", 10) == 0)
        {
                   if (PrintHdc == FALSE)
                   {
                      tlen *= RowHeightP; 
                   }
                   if (ldiff > x)
                   {
                     return TRUE;
                   }
                   HPEN hPen  = CreatePen (PS_SOLID, 0, GRAYCOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x - ldiff, y + tlen);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$WHITEVLINE", 11) == 0)
        {
                   if (PrintHdc == FALSE)
                   {
                      tlen *= RowHeightP; 
                   }
                   if (ldiff > x)
                   {
                     return TRUE;
                   }
                   HPEN hPen  = CreatePen (PS_SOLID, 0, WHITECOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x - ldiff, y + tlen);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$BLUEVLINE", 10) == 0)
        {
                   if (PrintHdc == FALSE)
                   {
                      tlen *= RowHeightP; 
                   }
                   if (ldiff > x)
                   {
                     return TRUE;
                   }
                   HPEN hPen  = CreatePen (PS_SOLID, 0, BLUECOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x - ldiff, y + tlen);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }
        else if (memcmp (satz, "$RAISEDVLINE", 12) == 0)
        {
                   if (PrintHdc == FALSE)
                   {
                      tlen *= RowHeightP; 
                   }
                   if (ldiff > x)
                   {
                     return TRUE;
                   }
                   HPEN hPen  = CreatePen (PS_SOLID, 0, GRAYCOL);

                   HPEN oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff, y, NULL);
                   LineTo (hdc, x - ldiff, y + tlen);
                   DeleteObject (SelectObject (hdc, oldPen)); 

                   hPen  = CreatePen (PS_SOLID, 0, WHITECOL);

                   oldPen = SelectObject (hdc, hPen);
                   MoveToEx (hdc, x - ldiff + 1, y, NULL);
                   LineTo (hdc, x - ldiff + 1, y + tlen);
                   DeleteObject (SelectObject (hdc, oldPen)); 
                   return TRUE;
        }

        return FALSE;
}
                  

void ListClass::FillFrmPosRow (HDC hdc, char *dest, form *frm, int zeile)
{
        int ldiff;
        int szdiff;
        unsigned int frmlen;
		int y;
		int xplus;
		SIZE size;

		 GetTextExtentPoint32 (hdc, "X", 1, &size);
         y = UbHeight + zeile * RowHeightP; 
		 y = GetMidPos (y);
         if (NoRecNr == 0)
         {
//                  xplus = fSatzNr.mask[0].length * tm.tmAveCharWidth;
                  xplus = fSatzNr.mask[0].length * size.cx;
         }
         else
         {
                  xplus = 0;
         }
//        if (h) return;
        memset (dest, ' ', 0x1000);

        ldiff = 0;
        if (NoRecNr == 0 && PosPrintMode == FALSE)
        {
                   SetTextColor (hdc,BLACKCOL);
                   SetBkColor (hdc,LTGRAYCOL);
                   TextOut (hdc, 0, y, SatzNr, strlen (SatzNr));
        }
        if (LineForm.frmstart && PrintHdc == FALSE)
        {
            ldiff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            if (NoRecNr == 0)
            {
                    ldiff += fSatzNr.mask[0].length;
            }
		}
        else if (NoRecNr == 0)
        {
            ldiff = fSatzNr.mask[0].length;
        }
		else
        {
            ldiff = 0;
        }
        frmlen = 0;
        szdiff = 0;
        if (memcmp (ausgabesatz, "$DISP", 5) == 0)
        {
                   if (PrintLines (hdc, (char *) &ausgabesatz[5], y, ldiff, &size))
                   {
                       return;
                   }
                   if (PrintPosText (hdc, (char *) &ausgabesatz[5], y, ldiff, &size))
                   {
                       return;
                   }
                   return;
        }
}


void ListClass::FillFrmRow (HDC hdc, char *dest, form *frm, int zeile)
{
        int i;
        char buffer [1024];
        int ldiff;
        int szdiff;
        int pos;
        unsigned int frmlen;
		int x, y;
		int xplus;
		SIZE size;
		SIZE sizes;
        int startpos;

		 GetTextExtentPoint32 (hdc, "X", 1, &size);
         y = UbHeight + zeile * RowHeightP; 
		 y = GetMidPos (y);
         if (NoRecNr == 0)
         {
//                  xplus = fSatzNr.mask[0].length * tm.tmAveCharWidth;
                  xplus = fSatzNr.mask[0].length * size.cx;
         }
         else
         {
                  xplus = 0;
         }
//        if (h) return;
        memset (dest, ' ', 0x1000);

        ldiff = 0;
        if (NoRecNr == 0)
        {
                   SetTextColor (hdc,BLACKCOL);
                   SetBkColor (hdc,LTGRAYCOL);
                   TextOut (hdc, 0, y, SatzNr, strlen (SatzNr));
        }
        if (LineForm.frmstart && PrintHdc == FALSE)
        {
            ldiff = LineForm.mask[LineForm.frmstart - 1].pos[1];
/*
            if (NoRecNr == 0)
            {
                    ldiff += fSatzNr.mask[0].length;
            }
*/

		}
        else if (NoRecNr == 0)
        {
            ldiff = fSatzNr.mask[0].length;
        }
		else
        {
            ldiff = 0;
        }
        frmlen = 0;
        szdiff = 0;
        if (memcmp (ausgabesatz, "$DISP", 5) == 0)
        {
                   if (PrintLines (hdc, (char *) &ausgabesatz[5], y, ldiff, &size))
                   {
                       return;
                   }
                   if (PrintPosText (hdc, (char *) &ausgabesatz[5], y, ldiff, &size))
                   {
                       return;
                   }
                   int tlen =  strlen ((char *) ausgabesatz + 5);
                   if (ldiff >= tlen)
                   {
                             return;
                   }
                   x = 0;
                   if (MarkedRows && MarkedRows[scrollpos + zeile])
                   {
                            SetTextColor (hdc,MarkColor);
                            SetBkColor (hdc,MarkBgColor);
                   }
                   else
                   {
                            SetTextColor (hdc,Color);
                            SetBkColor (hdc,BkColor);
                   }
                   TextOut (hdc, x, y, (char *) ausgabesatz + 5 + ldiff, 
                                                strlen ((char *) ausgabesatz + 5 + ldiff));
                   StopPrint = FALSE;
                   return;
        }

        if (PrintHdc)
        {
            startpos = 0;
        }
        else
        {
            startpos = frm->frmstart;
        }

        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
                   ToFormat (buffer, &frm->mask[i]);
                   pos = frm->mask[i].pos[1];
                   if (strchr (frm->mask[i].picture, 'f') ||
                       (strchr (frm->mask[i].picture, 'd') &&
                        strchr (frm->mask[i].picture, 'y') == NULL))
                   {
                        clipped (buffer);
                   		GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
                        pos = (pos + frm->mask[i].length - 2) * size.cx;
                        pos -= sizes.cx; 
                        pos -= ldiff * size.cx;
                        if (pos < 0) continue;
				        x = xplus + pos;
                   } 
                   else
                   {
                        pos = pos - ldiff;
                        if (pos < 0) continue;
				        x = xplus + pos * size.cx;
                   }
                   pos -= (szdiff - size.cx);
                   if (MarkedRows && MarkedRows[scrollpos + zeile])
                   {
                            SetTextColor (hdc,MarkColor);
                            SetBkColor (hdc,MarkBgColor);
                   }
                   else
                   {
                            SetTextColor (hdc,Color);
                            SetBkColor (hdc,BkColor);
                   }
				   memset (buffer, ' ', 80);
				   buffer [79] = 0;
                   TextOut (hdc, x, y, buffer, strlen (buffer));
                   ToFormat (buffer, &frm->mask[i]);
                   TextOut (hdc, x, y, buffer, strlen (buffer));
                   StopPrint = FALSE;
        }
        dest[frmlen] = (char) 0;
}

void ListClass::FillUbPFrmRow (HDC hdc, char *dest, form *frm, int zeile)
{
        int i;
        char buffer [1024];
        int ldiff;
        int szdiff;
        int pos;
        unsigned int frmlen;
		int x, y;
		int xplus;
		SIZE size;
		SIZE sizes;

		 GetTextExtentPoint32 (hdc, "X", 1, &size);
         y = zeile * RowHeightP; 
//		 y = GetMidPos (y);
         if (NoRecNr == 0)
         {
                  xplus = fSatzNr.mask[0].length * size.cx;
         }
         else
         {
                  xplus = 0;
         }
        memset (dest, ' ', 0x1000);

        ldiff = 0;
        if (NoRecNr == 0)
        {
                   SetTextColor (hdc,BLACKCOL);
                   SetBkColor (hdc,LTGRAYCOL);
                   TextOut (hdc, 0, y, " S-Nr        ", strlen (" S-Nr        "));
        }
        if (NoRecNr == 0)
        {
            ldiff = fSatzNr.mask[0].length;
        }
		else
        {
            ldiff = 0;
        }
        frmlen = 0;
        szdiff = 0;
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
                   ToFormat (buffer, &frm->mask[i]);
                   pos = frm->mask[i].pos[1];
                   if (strchr (frm->mask[i].picture, 'f') ||
                       (strchr (frm->mask[i].picture, 'd') &&
                        strchr (frm->mask[i].picture, 'y') == NULL))
                   {
                        clipped (buffer);
                   		GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
                        pos = (pos + frm->mask[i].length - 2) * size.cx;
                        pos -= sizes.cx; 
                        pos -= ldiff * size.cx;
                        if (pos < 0) continue;
				        x = xplus + pos;
                   } 
                   else
                   {
                        pos = pos - ldiff;
                        if (pos < 0) continue;
				        x = xplus + pos * size.cx;
                   }
                   pos -= (szdiff - size.cx);
                   SetTextColor (hdc,Color);
                   SetBkColor (hdc,LTGRAYCOL);
				   memset (buffer, ' ', 80);
				   buffer [79] = 0;
                   x += size.cx;
                   TextOut (hdc, x, y, buffer, strlen (buffer));
                   ToFormat (buffer, &frm->mask[i]);
                   TextOut (hdc, x, y, buffer, strlen (buffer));
        }
        dest[frmlen] = (char) 0;
}

#endif

void ListClass::ShowSatzNr (HDC hdc, int zeile)
{
          RECT rect; 
          HBRUSH hBrush;
		  SIZE size;
	    

          if (NoRecNr) return;
 		  GetTextExtentPoint32 (hdc, "X", 1, &size);
//          rect.top = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
//			          RowHeight); 
          rect.top = UbHeight + zeile * RowHeightP; 
		  rect.bottom = rect.top + tm.tmHeight * RowHeightP;
		  rect.left = 0;
//          rect.right = fSatzNr.mask[0].length * tm.tmAveCharWidth;
          rect.right = fSatzNr.mask[0].length * size.cx;

          hBrush = CreateSolidBrush (LTGRAYCOL);

          SelectObject (hdc, hBrush);
          SelectObject (hdc, GetStockObject (NULL_PEN));
          Rectangle (hdc,rect.left, rect.top, rect.right, rect.bottom);
          DeleteObject (hBrush); 
}


int ListClass::GetMidPos (int y)
/**
Mittelposition in Ratserzeile ermitteln.
**/
{
	     int h;

	     if (RowHeight <= 1) return y;

//		 h = (int) (double) ((double) tm.tmHeight * RowHeight);
		 h = RowHeightP;
		 y = y + (h - tm.tmHeight) / 2;
		 return y;
}



void ListClass::ShowFrmRow (HDC hdc, int zeile)
/**
Zeile anzeigen.
**/
{
         int y;
         int x;

//         y = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
//			  RowHeight); 
         y = UbHeight + zeile * RowHeightP; 
		 y = GetMidPos (y);
         if (NoRecNr == 0)
         {
                  x = fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }
         else
         {
                  x = 0;
         }

         if (MustPaint (y))
         {
                 if (NoRecNr == 0)
                 {
                         SetTextColor (hdc,BLACKCOL);
                         SetBkColor (hdc,LTGRAYCOL);
                         TextOut (hdc, 0, y, SatzNr, strlen (SatzNr));
                 }

                 SetTextColor (hdc,Color);
                 SetBkColor (hdc,BkColor);
                 TextOut (hdc, x, y, frmrow, strlen (frmrow));
         }
}


int ListClass::GetPageRows (char *Printer, mfont *mFont)
/**
Seitenlaenge fuer gewaehlten Drucker holen.
**/
{
     HDC hdc;
     int RowHeight;
     int PageRows;
     int cxPage;
     int cyPage;
     SIZE size;
     HFONT oldfont;
     TEXTMETRIC tm;


     hdc = GetPrinterbyName (Printer);

     if (hdc == NULL) return 0;
     cxPage = GetDeviceCaps (hdc, HORZRES);
     cyPage = GetDeviceCaps (hdc, VERTRES);
	 HFONT hFont = SetDeviceFont (hdc, mFont, &tm);
     oldfont = SelectObject (hdc, hFont);
     GetTextExtentPoint32 (hdc, "X", 1, &size);
	 RowHeight = (int) (double) ((double) tm.tmHeight * 1.0); 
     PageRows = (int) (double) ((double) cyPage / RowHeight);
     SelectObject (hdc, (HFONT) DeleteObject (hFont));
/*
	 hFont = SetDeviceFont (hdc, &mFont, &tm);
     oldfont = SelectObject (hdc, hFont);
     GetTextExtentPoint32 (hdc, "X", 1, &size);
     SelectObject (hdc, oldfont);
     PageCols = (int) (double) ((double) cxPage / size.cx);
     SelectObject (hdc, (HFONT) DeleteObject (hFont));
*/
     return PageRows;
}


void ListClass::PrintList (void)
{
        TEXTMETRIC printtm;
        TEXTMETRIC scrtm;
        SIZE size;
        int savescroll;
        int scrRowHeightP;
        int scrUbHeight;
        double pagelend;
        RECT rect;
        COLORREF BkCol;
        DOCINFO di = {sizeof (DOCINFO), "dbinfo", NULL};
        
        if (QuerPrint)
        {
              SetQuerDefault (TRUE);
        }


        if (SelectPrinter () == FALSE)
        {
            return;
        }


/*
        if (::SetPage () == FALSE)
        {
            return;
        }
*/

        BkCol = BkColor;
        if (PrintBackground == FALSE)
        {
            BkColor = WHITECOL;
        }

        char *Printer = GetPrinterName ();
        if (Printer == NULL)
        {
            Printer = GetPrinterDevName ();
        }
        if (Printer == NULL)
        {
            return;
        }
        memcpy (&printfont, AktFont, sizeof (mfont));
        memcpy (&printfettfont, AktFont, sizeof (mfont));

        printfont.FontBkColor = WHITECOL;
        printfettfont.FontBkColor = WHITECOL;
        printfettfont.FontAttribute = 1;

        HDC hdc = GetPrinterbyName (Printer);
        if (hdc == NULL) return;
        PrintHdc = TRUE;
        if (StartDoc (hdc, &di) == 0) return; 
        rect.top = 0;
        rect.left = 0;
        rect.bottom = GetDeviceCaps (hdc, VERTRES);
        rect.right  = GetDeviceCaps (hdc, HORZRES);

        if (ScrPageLen == 0)
        {
               pagelen = GetPageRows (Printer, &printfont) - 2;
 
               pagelen0 = (int) (double) ((double) 
                    pagelen / RowHeight - ublen);
        }
        else
        {
               pagelen0 = ScrPageLen;
               pagelen = (int) (double) ((double) (pagelen0 + ublen) * RowHeight);
               pagelend = (double) (pagelen0 + ublen) * RowHeight;
        }

        PageStdCols = GetPageStdCols (Printer);
 	    HFONT hFont   = SetDeviceFont (hdc, &printfont, &printtm);
        HFONT oldfont = SelectObject (hdc, hFont);
	    GetTextExtentPoint32 (hdc, "X", 1, &size);
        printtm.tmAveCharWidth = size.cx;

        if (rect.bottom > (pagelen + 1) * printtm.tmHeight)
        {
            rect.bottom = (pagelen + 1) * printtm.tmHeight;
        }

        memcpy (&scrtm, &tm,   sizeof (TEXTMETRIC)); 
        memcpy (&tm, &printtm, sizeof (TEXTMETRIC)); 
        scrUbHeight = UbHeight;
        UbHeight *= tm.tmHeight / scrtm.tmHeight;

        scrRowHeightP = RowHeightP; 
		RowHeightP = (int) (double) ((double) tm.tmHeight * RowHeight);

        savescroll = scrollpos;
        scrollpos = 0;
        printscrollpos = 0;
        PageStart = 0;
        while (printscrollpos < recanz)
        {
              PageEnd = (int) (double) ((double) PageStart + pagelend * printtm.tmHeight + 0.5);
              if (StartPage (hdc) == 0) return;
              if (PrintBackground && (BkColor != WHITECOL))
              {
                    HBRUSH hBrush = CreateSolidBrush (LTGRAYCOL);
                    SelectObject (hdc, hBrush);
                    SelectObject (hdc, GetStockObject (NULL_PEN));
                    Rectangle (hdc,0, 0, GetDeviceCaps (hdc, HORZRES), 
                                         GetDeviceCaps (hdc, VERTRES));
                    DeleteObject (hBrush); 
              }
              if (PrintRowCaptions)
              {
                    FillUbPFrmRow (hdc, frmrow, &UbForm, 0);
              }
              ShowPrintForms (hdc);
              if (StopPrint)
              {
                  break;
              }
              PrintPVlines (hdc, &rect);
              PrintPHlines (hdc, &rect);
              EndPage (hdc);
              printscrollpos += pagelen0;
              PageStart = PageEnd;
        }
        DeleteObject (SelectObject (hdc, oldfont));
    	EndDoc (hdc);
        memcpy (&tm, &scrtm,   sizeof (TEXTMETRIC)); 
        RowHeightP = scrRowHeightP; 
        UbHeight   = scrUbHeight;
        PrintHdc = FALSE;
        BkColor = BkCol;
        scrollpos = savescroll;
}

 

void ListClass::ShowAktColumn (HDC hdc, int y, int i, int h)
/**
Ein Maskenfeld anzeigen.
**/
{
        int x;
        char buffer [1024];
        int diff;
        int pos;
        SIZE sizes;

        if (DataForm.mask[i].pos[0] != h) return;
        diff = 0;
        if (LineForm.frmstart)
        {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
        }
        if ((DataForm.mask[i].pos[1] - diff) <
             fSatzNr.mask[0].length)
        {
            return;
        }
        ToFormat (buffer, &DataForm.mask[i]);

        if (strchr (DataForm.mask[i].picture, 'f') ||
                   (strchr (DataForm.mask[i].picture, 'd') &&
                        strchr (DataForm.mask[i].picture, 'y') == NULL))
        {
                        pos = DataForm.mask[i].pos[1];
                        clipped (buffer);
                   		GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
                        pos = (pos + DataForm.mask[i].length - 2) * tm.tmAveCharWidth;
                        pos -= sizes.cx; 
                        pos -= diff * tm.tmAveCharWidth;
                        x = pos;
        } 
        else

        {
                        x = (DataForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;
        }

        if (MarkedRows && MarkedRows [AktRow])
        {
               SetTextColor (hdc,MarkColor);
               SetBkColor (hdc,MarkBgColor);
        }
        else
        {
               SetTextColor (hdc,Color);
               SetBkColor (hdc,BkColor);
        }
 	    memset (buffer, ' ', 80);
		buffer [79] = 0;
        TextOut (hdc, x, y, buffer, strlen (buffer));
        ToFormat (buffer, &DataForm.mask[i]);
        TextOut (hdc, x, y, buffer, strlen (buffer));
}

void ListClass::ShowAktRow (void)
/**
Aktuelle Zeile anzeigen.
**/
{
         HDC hdc;
         int y;
         int i;
         int z;

         if (RowMode) 
		 {

			 InvalidateRect (mamain3, NULL, TRUE);
			 UpdateWindow (mamain3);
			 return;
		 }

         z = AktRow - scrollpos;
         y = UbHeight + z * RowHeightP;
	     y = GetMidPos (y);
         hdc = GetDC (mamain3);
         for (i = UbForm.frmstart; i < DataForm.fieldanz; i ++)
         {
             ShowAktColumn (hdc, y, i, 0);
         }

         PrintHline (hdc, z);
         PrintVline (hdc, z);

         ReleaseDC (mamain3, hdc);
         SetFeldFocus0 (AktRow, AktColumn);
}
          

void ListClass::ShowDataForms (HDC hdc)
/**
Seite anzeigen
**/
{
        int i;
        int pos;

        if (DataForm.mask == NULL) return;

        PosPrintMode = FALSE;
        for (i = 0, pos = scrollpos; i < lPagelen + 1; i ++, pos ++)
        {
                  if (pos == recanz) break;
                  memcpy (ausgabesatz, SwSaetze [pos], zlen); 
                  sprintf (SatzNr, "%5ld ", pos + 1);
				  ShowSatzNr (hdc, i);
#ifdef _OLDLIST
                  FillFrmRow (frmrow, &DataForm);
                  ShowFrmRow (hdc, i);
#else
                  FillFrmRow (hdc, frmrow, &DataForm, i);
#endif
        }
#ifndef _OLDLIST

        if (PosPrint == FALSE) return; 

        PosPrintMode = TRUE;
        for (i = 0; i < recanz; i ++)
        {
                  memcpy (ausgabesatz, SwSaetze [i], zlen); 
                  FillFrmPosRow (hdc, frmrow, &DataForm, i);
        }

#endif
}



void ListClass::ShowPrintForms (HDC hdc)
/**
Seite anzeigen
**/
{
        int i;
        int pos;

        if (DataForm.mask == NULL) return;

        PrintHdc = TRUE;
        PosPrintMode = FALSE;
        StopPrint = FALSE;
        for (i = 0, pos = printscrollpos; i < pagelen0; i ++, pos ++)
        {
                  if (pos == recanz) break;
                  memcpy (ausgabesatz, SwSaetze [pos], zlen); 
                  sprintf (SatzNr, "%5ld ", pos + 1);
				  ShowSatzNr (hdc, i);
                  FillFrmRow (hdc, frmrow, &DataForm, i);
        }
        if (PosPrint == FALSE) return; 
        PosPrintMode = TRUE;
        StopPrint = TRUE;
        for (i = 0; i < recanz; i ++)
        {
                  memcpy (ausgabesatz, SwSaetze [i], zlen); 
                  FillFrmPosRow (hdc, frmrow, &DataForm, i);
        }
}


void ListClass::GetFocusText (void)
/**
Focus-Text aus Fenster holen.
**/
{
	    char buffer [1024];
		int i;
		char *adr;

		i = AktColumn;
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
        GetWindowText (FocusWindow, buffer, 1024);
		FormatField (buffer);
		adr = feldadr (ziel[i].feldname);

        mov (ziel[i].feldname, "%s", buffer);
	    strcpy (AktValue, 
			      DataForm.mask[AktColumn].item->GetFeldPtr ());

        memcpy (SwSaetze [AktRow], ausgabesatz, zlen); 
}
	 

void ListClass::ShowWindowText (int x, int y)
/**
Focus-Text anzeigen.
**/
{
	    char buffer [1024];
		char dest[1024];

		strcpy (dest, " "); 
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
//        strcpy (buffer, feldadr (ziel[AktColumn].feldname));
        ToFormat (buffer, &DataForm.mask[AktColumn]);
		strcat (dest, buffer);
        SetWindowText (FocusWindow, buffer);
		SetFocus (FocusWindow);
}

void ListClass::ZielFormat (char *buffer, FELDER *zfeld)
/**
Feld formatiert in buffer uebertragen.
**/
{
        int len;

        len = strlen (DataForm.mask[AktColumn].item->GetFeldPtr ());
        if (len > zfeld->feldlen)
        {
            len = zfeld->feldlen;
        }
        memcpy (buffer,DataForm.mask[AktColumn].item->GetFeldPtr (), 
                len);
        buffer [zfeld->feldlen] = (char) 0;
}

        

void ListClass::ShowFocusText (HDC hdc, int x, int y)
/**
Focus-Text anzeigen.
**/
{
	    char buffer [1024];
		char dest[1024];

		strcpy (dest, " "); 
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
        if (RowMode == 0)
        {
                  ToFormat (buffer, &DataForm.mask[AktColumn]);
        }
        else
        {
                  ZielFormat (buffer, &LstZiel[AktRow]);
        }
		strcat (dest, buffer);
        SetTextColor (hdc,WHITECOL);
        SetBkColor (hdc,BLUECOL);
        TextOut (hdc, x, y, dest, strlen (dest) - 1);
}



void ListClass::ScrollLeft (void)
/**
Ein Feld nach rechts scrollen.
**/
{


         RECT rect;

         GetClientRect (mamain3, &rect);
         if (NoRecNr == 0)
         {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }
         else
         {
                    rect.left = 0;
         }


         if(UbForm.frmstart > 0)
         {
                     UbForm.frmstart --;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}


void ListClass::ScrollRight (void)
/**
Ein Feld nach links scrollen.
**/
{


         RECT rect;

         GetClientRect (mamain3, &rect);
         if (NoRecNr == 0)
         {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }
         else
         {
                    rect.left = 0;
         }

         if(UbForm.frmstart < banz - 1)
         {
                     UbForm.frmstart ++;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}

void ListClass::ScrollPageUp (void)
/**
Eine Seite nach links scrollen.
**/
{

         RECT rect;

         GetClientRect (mamain3, &rect);
         if (NoRecNr == 0)
         {
                 rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }

         UbForm.frmstart -= 5;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         if(UbForm.frmstart < 0) 
         {
                     UbForm.frmstart = 0;
         }
         SetDataStart ();

         CloseControls (&UbForm);
         display_form (mamain3, &UbForm, 0, 0);
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                       TRUE);
}


void ListClass::ScrollPageDown (void)
/**
Eine Seite nach links scrollen.
**/
{

         RECT rect;

         GetClientRect (mamain3, &rect);
         if (NoRecNr == 0)
         {
                 rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }

         UbForm.frmstart += 5;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         if(UbForm.frmstart > banz - 1) 
         {
                     UbForm.frmstart = banz - 1;
         }
         SetDataStart ();

         CloseControls (&UbForm);
         display_form (mamain3, &UbForm, 0, 0);
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                       TRUE);
}

void ListClass::SetPos (int pos)
/**
Position setzen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         if (NoRecNr == 0)
         {
                 rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }

         if (pos >= 0 && pos <= banz - 1)
         {
                     UbForm.frmstart = pos;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
                     InvalidateRect (mamain3, &rect, TRUE);
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}
                      
void ListClass::SetTop ()
/**
Position setzen.
**/
{
          
          RECT rect;

          if (DoAfter && RowMode)
		  {
                DoAfter (LstZiel[AktRow].feldname);
		  }
          else if (DoAfter)
		  {
                 DoAfter (ziel[AktColumn].feldname);
		  }
          GetClientRect (mamain3, &rect);
          rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

          UbForm.frmstart = 0;
          SetDataStart ();
          CloseControls (&UbForm);
          display_form (mamain3, &UbForm, 0, 0);
		  if (FocusWindow)
		  {
					 DestroyFocusWindow ();
		  }
          InvalidateRect (mamain3, &rect, TRUE);
          SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
}

                      
void ListClass::SetBottom ()
/**
Position setzen.
**/
{
           
          RECT rect;

          if (DoAfter && RowMode)
		  {
                DoAfter (LstZiel[AktRow].feldname);
		  }
          else if (DoAfter)
		  {
                 DoAfter (ziel[AktColumn].feldname);
		  }
          GetClientRect (mamain3, &rect);

          UbForm.frmstart = banz - 1;;
          SetDataStart ();
          CloseControls (&UbForm);
          display_form (mamain3, &UbForm, 0, 0);
    	  if (FocusWindow)
		  {
					 DestroyFocusWindow ();
		  }
          InvalidateRect (mamain3, &rect, TRUE);
          SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
}
                      

void ListClass::HScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
         switch (LOWORD (wParam))
         {
             case SB_LINEDOWN :
             {    
                     ScrollRight ();
                     break;
             }
             case SB_LINEUP :
             {    
                     ScrollLeft ();
                     break;
             }
             case SB_PAGEDOWN :
             {    
                     ScrollPageDown ();
                     break;
             }
             case SB_PAGEUP :
             {    
                     ScrollPageUp ();
                     break;
             }
             case SB_THUMBPOSITION :
                     SetPos (HIWORD (wParam));
                     break;
             case SB_TOP :
                     SetTop ();
                     break;
             case SB_BOTTOM :
                     SetBottom ();
                     break;
         }
}


void ListClass::SetVPos (int pos)
/**
Position setzen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;

         if (pos >= 0 && pos <= recanz - 1)
         {
                     scrollpos = pos;
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}
                      
void ListClass::EditScroll (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;

          DestroyFocusWindow (); 
          GetClientRect (mamain3, &rect);
          rect.top += tm.tmHeight;
          rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

          InvalidateRect (mamain3, &rect, TRUE);
}

static RECT newrows;

void ListClass::ScrollWindow0 (HWND mamain3, int start, int height, RECT *rectv, RECT *rectb)
{
	     int i;
		 int plus;

		 if (height >= 0) 
		 {
			 plus = 2;
		 }
		 else
		 {
			 plus = -2;
			 height *= -1;
		 }

        
		 for (i = 0; i < height; i +=2)
		 {
                     ScrollWindow (mamain3, start, 
											plus,
                                            rectv, rectb);
//					 if (ScrollWait > 1) Sleep (ScrollWait - 1);
		 }
}
  

void ListClass::ScrollDown (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;
		 int bottom;

         GetClientRect (mamain3, &rect);
         GetClientRect (mamain3, &newrows);
         rect.top = UbHeight;
		 GetPageLen ();
		 rect.bottom -= 1;
		 bottom = (int) (double) ((double) UbHeight + lPagelen * 
			 tm.tmHeight * RowHeight + 2);
		 if (bottom < rect.bottom) rect.bottom = bottom; 

//         newrows.top = (int) (double) ((double) 
//			 newrows.bottom - 2 * UbHeight * RowHeight);

         newrows.top = (int) (double) ((double) 
			 newrows.bottom - tm.tmHeight * RowHeight);
         if(scrollpos < recanz - 1)
         {
                     scrollpos ++;

					 if (FocusWindow)
					 {
						 DestroyFocusWindow ();
					 }

                     if (ScrollWait)
					 {
                          ScrollWindow0 (mamain3, 0, 
                                            (int) (double) ((double) 
											-tm.tmHeight 
											* RowHeight),
                                            &rect, &rect);
                                    newrows.top = (int) (double) ((double) 
			                                newrows.bottom - 3 * tm.tmHeight * RowHeight);
					 }
					 else
					 {
                          ScrollWindow (mamain3, 0, 
                                            (int) (double) ((double) 
											-tm.tmHeight 
											* RowHeight),
                                            &rect, &rect);
                                    newrows.top = (int) (double) ((double) 
			                                newrows.bottom - tm.tmHeight * RowHeight - 2);
                          Sleep (20);
					 }
                     InvalidateRect (mamain3, &newrows, FALSE);
                     UpdateWindow (mamain3);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}
         
void ListClass::ScrollUp (void)
/**
Ein Feld nach unten scrollen.
**/
{

         RECT rect;
         RECT newrows;

         GetClientRect (mamain3, &rect);
         GetClientRect (mamain3, &newrows);


         rect.top = (int) (double) ((double) UbHeight + 1);
         newrows.top = UbHeight;

         if (recanz - scrollpos >= lPagelen)
         {
                  newrows.bottom = (int) (double) ((double) 
					  newrows.top + UbHeight * RowHeight);
         }


         if(scrollpos > 0)
         {
                     scrollpos --;
					 if (FocusWindow)
					 {
						 DestroyFocusWindow ();
					 }

                     if (ScrollWait)
					 {
                        ScrollWindow0 (mamain3, 0, 
						    (int) (double) ((double) 
						                    tm.tmHeight * RowHeight),
                                            &rect, &rect);
					 }
					 else
					 {
                        ScrollWindow (mamain3, 0, 
						    (int) (double) ((double) 
						                    tm.tmHeight * RowHeight),
                                            &rect, &rect);
                             Sleep (20);
					 }
                     InvalidateRect (mamain3, &newrows, TRUE);
                     UpdateWindow (mamain3);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}

void ListClass::ScrollVPageDown (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;

         if (DoAfter && RowMode)
		 {
                DoAfter (LstZiel[AktRow].feldname);
		 }
         else if (DoAfter)
		 {
                DoAfter (ziel[AktColumn].feldname);
		 }
         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;

         scrollpos += lPagelen - 1;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         if(scrollpos > recanz - 1)
         {
                     scrollpos = recanz - 1;
         }

         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
}

void ListClass::ScrollVPageUp (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;

         if (DoAfter && RowMode)
		 {
                DoAfter (LstZiel[AktRow].feldname);
		 }
         else if (DoAfter)
		 {
                DoAfter (ziel[AktColumn].feldname);
		 }
         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;

         scrollpos -= lPagelen - 1;

         if(scrollpos < 0)
         {
                     scrollpos = 0;
         }

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
}

void ListClass::VScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
         switch (LOWORD (wParam))
         {
             case SB_LINEDOWN :
             {    
                     ScrollDown ();
                     break;
             }
             case SB_LINEUP :
             {    
                     ScrollUp ();
                     break;
             }

             case SB_PAGEDOWN :
             {    
                     ScrollVPageDown ();
                     break;
             }
             case SB_PAGEUP :
             {    
                     ScrollVPageUp ();
                     break;
             }
             case SB_THUMBPOSITION :
                     SetVPos (HIWORD (wParam));
                     break;

             case SB_TOP :
                     SetTop ();
                     break;
             case SB_BOTTOM :
                     SetBottom ();
                     break;
         }
}

void ListClass::SetNoFocus (void)
{
	     DestroyFocusWindow ();
         ListFocus = 0;
         InvalidateRect (mamain3, &FocusRect, TRUE);
	     UpdateWindow (mamain3);
}

void ListClass::SetFrameFocus (void)
{
	     DestroyFocusWindow ();
         ListFocus = 1;
         InvalidateRect (mamain3, &FocusRect, TRUE);
	     UpdateWindow (mamain3);
}

void ListClass::SetRevFocus (void)
{
	     DestroyFocusWindow ();
         ListFocus = 2;
         InvalidateRect (mamain3, &FocusRect, TRUE);
	     UpdateWindow (mamain3);
}

void ListClass::SetEditFocus (void)
{
	     DestroyFocusWindow ();
         ListFocus = 3;
		 if (RowMode)
		 {
			 AktColR();
		 }
		 else
		 {
			 AktCol ();
		 }
 	     ActiveEdit = ListFocus;
         InvalidateRect (mamain3, &FocusRect, TRUE);
	     UpdateWindow (mamain3);
}

void ListClass::SetEdit3DFocus (void)
{
	     DestroyFocusWindow ();
         ListFocus = 4;
		 if (RowMode)
		 {
			 AktColR();
		 }
		 else
		 {
			 AktCol ();
		 }
 	     ActiveEdit = ListFocus;
         InvalidateRect (mamain3, &FocusRect, TRUE);
	     UpdateWindow (mamain3);
}

void ListClass::InsertLine (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         int i;

         if (RowMode) return;

         InAppend = 1; 
         SetInsAttr ();
         SetPos (AktRow, FirstColumn ());

         InvalidateRect (mamain3, &FocusRect, TRUE);
         DestroyFocusWindow ();
         for (i = recanz; i > AktRow; i --)
         {
                   memcpy (SwSaetze[i], SwSaetze[i - 1], zlen);
         }

         recanz ++;
         SwRecs = recanz;
         InitForm (&DataForm);
         memcpy (SwSaetze[i], ausgabesatz, zlen);
         GetPageLen ();
         MoveListWindow ();
		 DisplayList ();
/*
         InvalidateRect (mamain3, NULL, TRUE);
         UpdateWindow (mamain3);
*/
         NewRec = 1;
}

void ListClass::AppendLine (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         int i, j;
		 RECT rect;
		 char *adr;

         if (RowMode) return;

         if (TestAppend)
         {
            if ((*TestAppend) () == 0)
            {
                return;
            }
         }

         InvalidateRect (mamain3, &FocusRect, TRUE);
         InAppend = 1; 
         NewRec = 1; 
         SetInsAttr ();
         SetPos (AktRow, FirstColumn ());

         DestroyFocusWindow ();
         AktRow ++;
         SelRow = AktRow;
         SwSaetze [recanz] = (char *) GlobalAlloc (0, zlen + 2);
         for (i = recanz; i > AktRow; i --)
         {
                   memcpy (SwSaetze[i], SwSaetze[i - 1], zlen);
         }

         recanz ++;
         SwRecs = recanz;
         memcpy (ausgabesatz, SwSaetze[i], zlen);
         for (j = 0; ziel[j].feldname[0]; j ++)
		 {
			 adr = feldadr (ziel[j].feldname);
			 if (adr == NULL) continue;
			 memset (adr, ' ', ziel[j].feldlen);
             adr [ziel[j].feldlen - 1] = (char) 0;
		 }
//         InitForm (&DataForm);
         memcpy (SwSaetze[i], ausgabesatz, zlen);
         GetPageLen ();
         MoveListWindow ();

		 GetClientRect (mamain3, &rect);
		 rect.top += UbHeight * tm.tmHeight;
		 DisplayList ();
}

void ListClass::CopyLine (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         int i;
		 RECT rect;

         if (RowMode) return;

         if (TestAppend)
         {
            if ((*TestAppend) () == 0)
            {
                return;
            }
         }

         InvalidateRect (mamain3, &FocusRect, TRUE);
         InAppend = 1; 
         NewRec = 1; 
         SetInsAttr ();
         SetPos (AktRow, FirstColumn ());

         DestroyFocusWindow ();
         AktRow ++;
         SelRow = AktRow;
         SwSaetze [recanz] = (char *) GlobalAlloc (0, zlen + 2);
         for (i = recanz; i > AktRow; i --)
         {
                   memcpy (SwSaetze[i], SwSaetze[i - 1], zlen);
         }

         recanz ++;
         SwRecs = recanz;
         memcpy (SwSaetze[i], ausgabesatz, zlen);
         GetPageLen ();
         MoveListWindow ();

		 GetClientRect (mamain3, &rect);
		 rect.top += UbHeight * tm.tmHeight;
		 DisplayList ();
}

void ListClass::DeleteLine (void)
/**
Eine Zeile in Liste loeschen.
**/
{
         int i;
		 RECT rect;

         if (RowMode) return;

         if (recanz == 0) return;

         if (recanz <= 1)
         {
	                InvalidateFocusFrame = TRUE;
                    InitForm (&DataForm);
                    memcpy (SwSaetze[0], ausgabesatz, zlen);
                    InAppend = 1;
                    NewRec = 1;
                    SetInsAttr ();
                    SetPos (AktRow, FirstColumn ());
                    ShowAktRow ();
                    SetFeldFocus0 (AktRow, AktColumn);
					InvalidateFocusFrame = FALSE;
                    return;
         }

         InAppend = 0; 
         NewRec = 0;
         SetInsAttr ();
         SetPos (AktRow, FirstColumn ());
         DestroyFocusWindow ();
         for (i = AktRow; i < recanz - 1; i ++)
         {
                   memcpy (SwSaetze[i], SwSaetze[i + 1], zlen);
         }
         recanz --;
         GlobalFree (SwSaetze[recanz]);
         SwSaetze[recanz] = NULL;
         if (AktRow > recanz) AktRow --;
         GetPageLen ();
         MoveListWindow ();
		 GetClientRect (mamain3, &rect);
		 rect.top += UbHeight;
//		 DisplayList ();

         InvalidateRect (mamain3, &rect, TRUE);
         UpdateWindow (mamain3);


         if (AktRow >= recanz && syskey != KEYUP)
         {
             AktRow --;
             SetFeldFocus0 (AktRow, AktColumn); 
         }
	  	 InvalidateFocusFrame = FALSE;
}


void ListClass::FunkKeys (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
#ifndef _NINFO
 		 char where [256];
         char Item [81];
#endif

		 if (FocusWindow)
		 {
			 GetFocusText ();
		 }

         switch (LOWORD (wParam))
         {
             case VK_F2 :
#ifndef BIWAK
#ifndef _NINFO
  		            sprintf (where, "where %s = %s",
							  GetAktItem (), 
							  GetAktValue ()); 
					_CallInfoEx (hMainWindow, NULL,
							     GetAktItem (), 
								 where, 0l);
#endif
#endif
					 break; 
             case VK_F3 :
                     FindNext ();
					 break;
             case VK_F4 :
#ifndef _NINFO
                    strcpy (Item, GetAktItem ());
  		            sprintf (where, "where %s = %s",
							  Item, 
							  GetAktValue ()); 
                    GetInfInfo (where, Item);
					_CallInfoEx (hMainWindow, NULL,
							     Item, 
								 where, 0l);
#endif
					 break; 
             case VK_F6 :
                     syskey = KEY6; 
                     testkeys ();
    	             DestroyFocusWindow ();
                     if (ListFocus == 1)
					 {
				            ListFocus = ActiveEdit;
                    	    if (RowMode)
							{
			                         AktColR();
							}
		                    else
							{
			                         AktCol ();
							}
					 }
					 else
					 {
						    ListFocus = 1;
					 }
                     InvalidateRect (mamain3, &FocusRect, TRUE);
	                 UpdateWindow (mamain3);
					 break;
             case VK_F7 :
                     syskey = KEY7; 
                     testkeys ();
					 break;
             case VK_F8 :
                     syskey = KEY8; 
                     testkeys ();
					 break;
             case VK_F9 :
                     syskey = KEY9; 
                     testkeys ();
					 break;
             case VK_F10 :
                     syskey = KEY10; 
                     testkeys ();
					 break;
             case VK_F11 :
                     syskey = KEY11; 
                     TestChoise ();
//                     testkeys ();
                     break;
             case VK_F12 :
                     syskey = KEY12; 
                     testkeys ();
                     break;
             case VK_RIGHT :
             {    
				     if (ListFocus == 0)
					 {
                            ScrollRight ();
					 }
					 else
					 {
				            FocusRight ();
					 }
                     break;
             }
             case VK_LEFT :
             {    
				     if (ListFocus == 0)
					 {
                            ScrollLeft ();
					 }
					 else
					 {
				            FocusLeft ();
					 }
                     break;
             }
             case VK_DOWN :
             case VK_RETURN :
             {    
				     if (ListFocus == 0)
					 {
                            ScrollDown ();
					 }
					 else
					 {
				            FocusDown ();
					 }
                     break;
             }
             case VK_UP :
             {    
				     if (ListFocus == 0)
					 {
                            ScrollUp ();
					 }
					 else
					 {
				            FocusUp ();
					 }
                     break;
             }

             case VK_NEXT :
             {    
                     ScrollVPageDown ();
                     break;
             }
             case VK_PRIOR :
             {    
                     ScrollVPageUp ();
                     break;
             }

             case VK_HOME :
                     SetTop ();
                     break;
             case VK_END :
                     SetBottom ();
                     break;
             case VK_TAB :
                     if (GetKeyState (VK_SHIFT) < 0)
                     {
						      FocusLeft ();
                     }
					 else
					 {
 			                  FocusRight ();
					 }
					 break;

         }
}

void ListClass::FillUbForm (field *feld, char *feldname, int len, int spalte,
                 int BuId  )
/**
Maskenfeld fuer Ueberschruft fuellen.
**/
{
        memcpy (feld, &iButton, sizeof (field)); 
        clipped (feldname);
        feld->item       = new ITEM ("", feldname, "", 0);
        feld->length     = len + getFontlen (2);
        feld->rows       = UbRows;
        feld->pos[0]     = 0;
        feld->pos[1]     = spalte;
        feld->BuId       = BuId;
}


void ListClass::FreeDlgSaetze (void)
{
      int i;

      for (i = 0; i < DlgAnz; i ++)
      {
          if (DlgSaetze[i])
          {
              free (DlgSaetze[i]);
          }
      }
      DlgAnz = 0;
}
       
void ListClass::FromDlgRec (char *rec)
/**
Satz f�r Seitenansicht �bertragen.
**/
{
       int i;
       int len;
       char *adr;

       memcpy (ausgabesatz, rec, zlen);
       for (i = 0; i < banz; i ++)
       {
           adr = feldadr (ziel[i].feldname);
           if (adr == NULL) continue;

           len = strlen (adr);
           if (len > ziel[i].feldlen)
           {
               len = ziel[i].feldlen;
           }
           memcpy (adr, &DlgSaetze[i][20], len);
           adr [len] = (char) 0;
       }
       memcpy (rec, ausgabesatz, zlen);
}

void ListClass::ToDlgRec (char *rec)
/**
Satz f�r Seitenansicht �bertragen.
**/
{
       int i;
       int len;
       char *adr;
       char ibez [81];
       char *bez;
       char *name;
       char *p;

       FreeDlgSaetze ();
       memcpy (ausgabesatz, rec, zlen);
       for (i = 0; i < banz; i ++)
       {
           DlgSaetze [i] = (char *) malloc (Dlgzlen + 3);

           memset (DlgSaetze[i], ' ', Dlgzlen + 3);
           DlgSaetze[i][Dlgzlen] = (char) 0;
           name = ziel[i].feldname;
           clipped (name);
           bez = GetIfItem (name, ibez);
#ifndef _NINFO
           if (ReadItemLabel)
           {
             if (bez == NULL)
             {
                p = strchr (name, '$');
                if (p != NULL)
                {
                      bez = GetItemName (p + 1);
                }
                else
                {
                      bez = GetItemName (ziel[i].feldname);
                }
             }
           }
#endif
           if (bez) 
           {
               name = bez;
           }
        
           len = strlen (name);
           if (len > 19) len = 19;

           memcpy (DlgSaetze[i], name, len);
           DlgSaetze[i][19] = (char) 0;

           adr = feldadr (ziel[i].feldname);
           if (adr == NULL) continue;

           len = strlen (adr);
           if (len > MAXLEN) len = MAXLEN;
           memcpy (&DlgSaetze[i][20], adr, len);
           DlgSaetze[i][20 + len] = (char) 0;
       }
       DlgAnz = i;
}

void ListClass::SetPage (int pos)
/**
Satz in Seitenansicht.
**/
{

       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                banzS = banz;
                zlenS = zlen;
                LstZiel = ziel;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec (SwSaetze [pos]);
                SetPos (FirstColumnR (), 1);
                SelRow = 0;
                ziel = DlgZiel;
                ausgabesatz = DlgSatz;
                zlen = Dlgzlen;
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                SetSaetze (DlgSaetze);
       }
       else
       {
                AktRow = AktRowS;
                ziel = LstZiel;
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec (SwSaetze [pos]);
       }
}

void ListClass::SwitchRowMode (int mode)
/**
Bei RowMode originale Zielstruktur herstellen.
mode = 1 RowMode 0 herstellen.
mode = 0 RowMode 1 hestellen
**/
{
	    static int AktRow0;
	    static int AktColumn0;

	    if (RowMode == 0) return;

        SetPage (AktRowS);
		RowMode = 1;
        return;

        if (mode == 1)
		{
                ziel = DlgZiel;
                ausgabesatz = DlgSatz;
                zlen = Dlgzlen;

				AktColumn = AktColumn0;
				AktRow    = AktRow0;
                SetRecanz (banz);
                ToDlgRec (SwSaetze [AktRow]);
                SetSaetze (DlgSaetze);
		}
		else if (mode == 0)
		{
  			    SetPage (AktRow);
                ziel = LstZiel;
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);

				AktRow0    = AktRow;
				AktColumn0 = AktColumn;

				AktColumn  = AktRow;
				AktRow     = AktRowS;
                FromDlgRec (SwSaetze [AktRow]);
		}
}

void ListClass::SwitchPage (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       DestroyTrack ();
       DestroyVTrack ();
	   DestroyFocusWindow ();
       FreeUbForm ();
//       FreeDataForm ();
       FreeLineForm ();
       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                AktPage     = AktRow;
                scrollposS  = scrollpos;
                banzS = banz;
                zlenS = zlen;
                LstZiel = ziel;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec (SwSaetze [pos]);
                SelRow = 0;
                ziel = DlgZiel;
                ausgabesatz = DlgSatz;
                zlen = Dlgzlen;
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);

//              memcpy (&RowUb,  &UbForm,   sizeof (form));
                memcpy (&RowData,&DataForm, sizeof (form));
//                memcpy (&RowLine,&LineForm, sizeof (form));
//                UbRow   = &RowUb;
                DataRow = &RowData;
//                lineRow = &RowLine;
                SetPos (FirstColumnR (), 1);

                this->MakeUbForm ();
                SetSaetze (DlgSaetze);
                Initscrollpos ();
                MakeDataForm0 ();
       }
       else
       {
                FreeDataForm ();
                memcpy (&DataForm, &RowData,sizeof (form));
                FreeDataForm ();
                ziel = LstZiel;
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                this->MakeUbForm ();
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec (SwSaetze [pos]);
                Initscrollpos ();
                MakeDataForm0 ();
                Setscrollpos (scrollposS);
       }
}


void ListClass::NextPageRow (void)
/**
Naechsten Satz im PageView-Mode anzeigen.
**/
{
       if (PageView == 0) return; 
	   DestroyFocusWindow ();
       SetPage (AktRowS);
       if (AktRowS < SwRecs - 1)
       {
           AktRowS = AktRowS + 1;
           AktRow  = AktRowS;
       }
       SetPage (AktRowS);
       SendMessage (mamain2, WM_SIZE, NULL, NULL);
       InvalidateRect (mamain3, 0, TRUE);
}

void ListClass::PriorPageRow (void)
/**
Naechsten Satz im PageView-Mode anzeigen.
**/
{
       if (PageView == 0) return; 
	   DestroyFocusWindow ();
       SetPage (AktRowS);
       if (AktRowS > 0)
       {
           AktRow = AktRowS - 1;
           AktRowS = AktRow;
       }
       SetPage (AktRowS);
       SendMessage (mamain2, WM_SIZE, NULL, NULL);
       InvalidateRect (mamain3, 0, TRUE);
}

void ListClass::SetNoRecNr (void)
/**
FLag NoReNr setzen.
**/
{
       NoRecNr = TRUE;
}

int ListClass::GetItemAttr (char *feldname)
/**
Attribut fuer Maskenfeld holen.
**/
{
	     int i;

		 if (ItemAttr == NULL) return EDIT;

		 for (i = 0; ItemAttr[i].name[0]; i ++)
		 {
			 if (strcmp (feldname, ItemAttr[i].name) == 0)
			 {
				 return ItemAttr[i].attribut;
			 }
		 }
		 return EDIT;
}


void ListClass::FillDataForm (field *feld, char *feldname, int len, int spalte)
/**
Maskenfeld fuer Daten fuellen.
**/
{
        char *adr;

        memcpy (feld, &iEdit, sizeof (field)); 
        clipped (feldname);
        adr = feldadr (feldname);
        if (adr == NULL) return;
        feld->item       = new ITEM ("", adr, "", 0);
        feld->length     = len;
        feld->pos[0]     = 0;
        feld->pos[1]     = spalte;
		feld->attribut   = GetItemAttr (feldname);
}

void ListClass::FillLineForm (field *feld, int spalte)
/**
Maskenfeld fuer Daten fuellen.
**/
{
        memcpy (feld, &iEdit, sizeof (field)); 
        feld->pos[1]     = spalte;
}

/*
void ListClass::GetPageLen (void)
{
        RECT rect;
        HDC hdc;
        HFONT hFont;
        int y;

        stdfont ();
        hFont = SetWindowFont (mamain2);
        hdc = GetDC (mamain2);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (mamain2, hdc);

        GetClientRect (mamain2, &rect);
        y = rect.bottom - 19;
        lPagelen = (int) ((double) (y - UbHeight) / tm.tmHeight); 
        lPagelen = (int) (double) ((double) lPagelen / RowHeight);
        if (lPagelen > recanz)
        {
                     lPagelen = recanz;
        }
}
**/

void ListClass::GetPageLen (void)
/**
Seitenlaenge ermitteln.
**/
{
        RECT rect;
        int y;

        GetClientRect (mamain2, &rect);
        y = rect.bottom - 16;
        lPagelen = (int) (double) ((double) (y - UbHeight) / tm.tmHeight + 0.5); 
        lPagelen = (int) (double) ((double) lPagelen / RowHeight + 0.5);
        if (lPagelen > recanz)
        {
                     lPagelen = recanz;
        }
}


void ListClass::MakeLineForm (form *frm)
/**
Form fuer daten generieren.
**/
{
        int i, j;
        int spalte;
        int len;
        
        memcpy (frm, &IForm, sizeof (form));
        frm->fieldanz = banz;
        frm->mask = (field *) GlobalAlloc (0, (banz + 2) * sizeof (field));
        if (frm->mask == NULL)
        {
                 disp_mess ("Kein Speicherplatz", 2);
                 ExitProcess (1);
        }
        if (NoRecNr == FALSE)
        {
                  spalte = 6;
        }
        else
        {
                   spalte = 0;
        }

        for (i = 0, j = 0; i < banz; i ++)
        {
                 if (ziel[i].feldtyp[0] == 'L')
                 {
                     j ++;
                     continue;
                 }
                 if (ziel[i].feldtyp[0] == 'Z') continue;

                 len = ziel[i].feldlen + 3;

                 if (Rows != NULL && Rows->GetCols () != NULL &&  
                     Rows->GetCol (j) != NULL)
                 {
							  if (Rows->GetCol (i)->GetLength () != 0)
							  {
								  len = Rows->GetCol (i)->GetLength ();
							  }
                 }

//                 len = getFontlen (ziel[i].feldlen + 3);
                 if (len > MAXLEN) len = MAXLEN;
                 spalte += len + 2;
                 FillLineForm (&frm->mask[j], spalte);
                 j ++;
        }
        frm->fieldanz = j;
}


void ListClass::MakeDataForm (form *frm)
/**
Form fuer daten generieren.
**/
{
        int i, j;
        int spalte;
        int len;
        
        memcpy (frm, &IForm, sizeof (form));
        frm->fieldanz = banz;
        frm->mask = (field *) GlobalAlloc (0, (banz + 2) * sizeof (field));
        if (frm->mask == NULL)
        {
                 disp_mess ("Kein Speicherplatz", 2);
                 ExitProcess (1);
        }
        if (NoRecNr == FALSE)
        {
            spalte = 7;
        }
        else
        {
            spalte = 1;
        }

        if (fSort)
        {
            delete fSort;
            fSort = NULL;
        }
        fSort = new BOOL [banz];

        for (i = 0, j = 0; i < banz; i ++)
        {
                 if (fSort) fSort [i] = 1;
                 if (ziel[i].feldtyp[0] == 'L') 
                 {
                     j ++;
                     continue;
                 }
                 if (ziel[i].feldtyp[0] == 'Z') continue;
                 len = ziel[i].feldlen + 3;

                 if (Rows != NULL && Rows->GetCols () != NULL &&  
                     Rows->GetCol (j) != NULL)
                 {
							  if (Rows->GetCol (i)->GetLength () != 0)
							  {
								  len = Rows->GetCol (i)->GetLength ();
							  }
                 }

//                 len = getFontlen (ziel[i].feldlen + 3);
                 if (len > MAXLEN) len = MAXLEN;
                 FillDataForm (&frm->mask[j], ziel[i].feldname,
                                len, spalte);

                 if (Rows != NULL && Rows->GetCols () != NULL &&  
                     Rows->GetCol (j) != NULL)
                 {
                              char *p = Rows->GetCol (i)->GetPicture ();
							  clipped (p);
							  if (strcmp (p, " ") == 0) *p = 0;

                              if (strlen (p) > 0)
                              {
								  char *p1 = new char [strlen (p) + 1];
								  strcpy (p1, p);
                                  frm->mask[j].picture = p1;
                              }
                 }


                 if (strlen (frm->mask[j].picture) == 0 && 
                     ziel[i].feldtyp[0] == 'N')
                 {
                     frm->mask[i].picture = new char [10];
                     sprintf (frm->mask[j].picture, "%c%d.%df", '%', ziel[i].feldlen,
                                                                   ziel[i].nachkomma);
                 }
                 j ++;
                 spalte = spalte + len + 2;
        }
        frm->fieldanz = j;
}

void ListClass::MakePrintForm (form *frm)
/**
Form fuer daten generieren.
**/
{
        int i;
        int spalte;
        int len;
        
        memcpy (frm, &IForm, sizeof (form));
        frm->fieldanz = banz;
        frm->mask = (field *) GlobalAlloc (0, (banz + 2) * sizeof (field));
        if (frm->mask == NULL)
        {
                 disp_mess ("Kein Speicherplatz", 2);
                 ExitProcess (1);
        }
        if (NoRecNr == FALSE)
        {
            spalte = 7;
        }
        else
        {
            spalte = 1;
        }

        if (fSort)
        {
            delete fSort;
            fSort = NULL;
        }
        fSort = new BOOL [banz];

        for (i = 0; i < banz; i ++)
        {
                 if (fSort) fSort [i] = 1;
                 if (ziel[i].feldtyp[0] == 'L') continue;
                 len = ziel[i].feldlen;
//                 len = getFontlen (ziel[i].feldlen + 3);
                 if (len > MAXLEN) len = MAXLEN;
                 FillDataForm (&frm->mask[i], ziel[i].feldname,
                                len, spalte);
                 if (ziel[i].feldtyp[0] == 'N')
                 {
                     frm->mask[i].picture = new char [10];
                     sprintf (frm->mask[i].picture, "%c%d.%df", '%', ziel[i].feldlen,
                                                                   ziel[i].nachkomma);
                 }
                 spalte = spalte + len + sabs;
        }
}

void ListClass::MakeDataForm0 (void)
/**
Forms f�r Daten generieren.
**/
{

        GetPageLen ();
        MakeDataForm (&DataForm);
        MakePrintForm (&PrintForm);
        MakeLineForm (&LineForm);
}

int ListClass::getFontlen (int len)
/**
L�nge f�r Font ausrechnen.
**/
{
	    HDC hdc;
		SIZE size;

        return len;
		hdc = GetDC (mamain3);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
		ReleaseDC (mamain3, hdc);
		len *= size.cx; 
		len /= tm.tmAveCharWidth;
		return len;
}

int ListClass::getDataFontlen (int len)
/**
L�nge f�r Font ausrechnen.
**/
{
	    HDC hdc;
		SIZE size;

		hdc = GetDC (mamain3);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
		ReleaseDC (mamain3, hdc);
		len *= tm.tmAveCharWidth; 
		len /= size.cx;
		return len;
}


void ListClass::MakeUbForm (void)
/**
Form fuer Ueberschrift generieren.
**/
{
        int i, j;
        int spalte;
        int BuId;
        int len;

        memcpy (&UbForm, &IForm, sizeof (form));
        UbForm.fieldanz = banz;
        UbForm.mask = (field *) GlobalAlloc (0, (banz + 2) * sizeof (field));
        if (UbForm.mask == NULL)
        {
                 disp_mess ("Kein Speicherplatz", 2);
                 ExitProcess (1);
        }
        BuId = 600;
        if (NoRecNr == FALSE)
        {
                  spalte = getFontlen (6);
                  fSatzNr.mask[0].length = getFontlen (6);
        }
        else
        {
                  spalte = 0;
        }
        for (i = 0, j = 0; i < banz; i ++)
        {
                 if (ziel[i].feldtyp[0] == 'L') 
                 {
                     j ++;
                     continue;
                 }
                 if (ziel[i].feldtyp[0] == 'Z') continue;
                 len = getFontlen (ziel[i].feldlen + 3);
                 if (Rows != NULL && Rows->GetCols () != NULL &&  
                     Rows->GetCol (j) != NULL)
                 {
							  if (Rows->GetCol (i)->GetLength () != 0)
							  {
								  len = Rows->GetCol (i)->GetLength ();
							  }
                 }
                 if (len > MAXLEN) len = MAXLEN;
                 this->FillUbForm (&UbForm.mask[j], ziel[i].feldname,
                              len, spalte,
                              BuId);

                 if (RowCaptions != NULL && 
                     RowCaptions[j] != NULL)
                 {
                              UbForm.mask[j].item->SetFeldPtr (RowCaptions [j]);
                 }

                 if (Rows != NULL && Rows->GetCols () != NULL &&  
                     Rows->GetCol (j) != NULL)
                 {
                              char *p = Rows->GetCol (j)->GetCaption ();
                              clipped (p);
                              if (strcmp (p, " ") > 0)
                              {
                                  UbForm.mask[j].item->SetFeldPtr (Rows->GetCol (j)->GetCaption ());
                              }
                 }
                 spalte += getFontlen (len + 2);
                 BuId ++;
                 j ++;
        }
        UbForm.fieldanz = j;
        if (UbForm.mask[0].rows == 0)
        {
            fUbSatzNr.mask[0].rows = 0;
            UbHeight = tm.tmHeight + tm.tmHeight / 3; 
        }
        else 
        {
            fUbSatzNr.mask[0].rows = UbForm.mask[0].rows;
            UbHeight = tm.tmHeight * (UbForm.mask[0].rows); 
        }
		RowHeightP = (int) (double) ((double) tm.tmHeight * RowHeight);
        if (NoRecNr == FALSE)
        {
			     CloseControls (&fUbSatzNr);
                 display_form (mamain3, &fUbSatzNr, 0, 0);
        }
        display_form (mamain3, &UbForm, 0, 0);
        if (TrackNeeded ())
        {
                 CreateTrack ();
        }
}


void ListClass::MakePrintUbForm (void)
/**
Form fuer Ueberschrift generieren.
**/
{
        int i;
        int spalte;
        int BuId;
        int len;

        memcpy (&PrintUbForm, &IForm, sizeof (form));
        PrintUbForm.fieldanz = banz;
        PrintUbForm.mask = (field *) GlobalAlloc (0, (banz + 2) * sizeof (field));
        if (UbForm.mask == NULL)
        {
                 disp_mess ("Kein Speicherplatz", 2);
                 ExitProcess (1);
        }
        BuId = 600;
        if (NoRecNr == FALSE)
        {
                  spalte = getFontlen (6);
                  fSatzNr.mask[0].length = getFontlen (6);
        }
        else
        {
                  spalte = 0;
        }
        for (i = 0; i < banz; i ++)
        {
                 if (ziel[i].feldtyp[0] == 'L') continue;
                 if (ziel[i].feldtyp[0] == 'Z') continue;
                 len = getFontlen (ziel[i].feldlen);
                 if (len > MAXLEN) len = MAXLEN;
                 this->FillUbForm (&PrintUbForm.mask[i], ziel[i].feldname,
                              len, spalte,
                              BuId);
                 spalte += getFontlen (len);
                 BuId ++;
        }
}


void ListClass::FreeDataForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&DataForm);
        if (DataForm.mask)
        {   
             for (i = 0; i < DataForm.fieldanz; i ++)
             {
                  if (DataForm.mask[i].item)
                  {
                            delete DataForm.mask[i].item;   
                            DataForm.mask[i].item = NULL;
                  }
                  if (strcmp (DataForm.mask[i].picture, " ") > 0)
                  {
                            delete DataForm.mask[i].picture;
                            DataForm.mask[i].picture = NULL;
                  }

             }
             GlobalFree (DataForm.mask);
        }
        memcpy (&DataForm, &IForm, sizeof (form));
}

void ListClass::FreeLineForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&LineForm);
        if (LineForm.mask)
        {
               for (i = 0; i < LineForm.fieldanz; i ++)
               {
                  if (LineForm.mask[i].item)
                  {
                            delete LineForm.mask[i].item;                 
                            LineForm.mask[i].item = NULL;
                  }
               }
               GlobalFree (LineForm.mask);
        }
        memcpy (&LineForm, &IForm, sizeof (form));
}

void ListClass::FreeUbForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&UbForm);
        if (UbForm.mask)
        {
               for (i = 0; i < UbForm.fieldanz; i ++)
               {
                  if (UbForm.mask[i].item)
                  {
                            delete UbForm.mask[i].item;                 
                            UbForm.mask[i].item = NULL;
                  }
               }
               GlobalFree (UbForm.mask);
               
        }
        memcpy (&UbForm, &IForm, sizeof (form));
}

struct SRT
{
        BOOL MarkedRow;
        char *Row;
};

int ListClass::SortCol0 (const void *elem1, const void *elem2)
{
          struct SRT *sel1;
          struct SRT *sel2;
          char *el1;
          char *el2;
          char sfeld1 [512];
          char sfeld2 [512];

	      sel1 = (struct SRT *) elem1;
		  sel2 = (struct SRT *) elem2;
          el1 = sel1->Row;
          el2 = sel2->Row;
//	      el1 = (char *) elem1;
//		  el2 = (char *) elem2;
          memcpy (ausgabesatz, el1, zlen);
          strcpy (sfeld1, DataForm.mask[ColSort].item->GetFeldPtr ());
          memcpy (ausgabesatz, el2, zlen);
          strcpy (sfeld2, DataForm.mask[ColSort].item->GetFeldPtr ());
          if (fSort)
          {
                 if (ziel[ColSort].feldtyp[0] == 'd')
                 {
                     return ((dasc_to_long (sfeld1) - 
                              dasc_to_long (sfeld2)) * fSort[ColSort]);
                 }
	             return (strcmp (sfeld1,sfeld2) * fSort[ColSort]);
          }
          return (strcmp (sfeld1,sfeld2));
}


void ListClass::SortCol (int Col)
{
        if (RowMode) return;
        int i;
        struct SRT *array;
//        char *array;
        recanz;

        if (SortEnabled == FALSE) return;
        ColSort = Col;
        DestroyFocusWindow ();

        recanz = GetRecanz ();
//        array = new char [zlen * (recanz + 1)];
        array = new SRT [recanz + 1];
        if (array == NULL) return;
        for (i = 0; i < recanz; i ++)
        {
            if (MarkedRows != NULL)
            {
                array[i].MarkedRow = MarkedRows[i];
            }
            array[i].Row = new char [zlen];
            if (array[i].Row == NULL) 
            {
                disp_mess ("Speicherzuordnungsfehler", 2);
                ExitProcess (1);
            }
            memcpy (array[i].Row, SwSaetze[i], zlen);
        }

/*
   	    qsort (array, recanz, zlen,
			   SortCol0);
*/

   	    qsort (array, recanz, sizeof (struct SRT),
			   SortCol0);
        if (fSort)
        {
            fSort[ColSort] *= -1;
        }

        for (i = 0; i < recanz; i ++)
        {
//            memcpy ( SwSaetze[i], &array[i * zlen], zlen);

            if (MarkedRows != NULL)
            {
                MarkedRows[i] = array[i].MarkedRow;
            }
            memcpy ( SwSaetze[i], array[i].Row, zlen);
            delete array[i].Row;
        }
        delete array;
        InvalidateRect (mamain3, NULL, TRUE);
        UpdateWindow (mamain3); 
        memcpy (ausgabesatz, SwSaetze [AktRow],zlen); 
}


BOOL ListClass::TestKeySort (int taste)
{
        taste &= 0x0F; 
        if (taste == 0) 
        {
            taste = 10;
        }
        else
        {
            taste --;
        }

        if (taste >= DataForm.fieldanz) return FALSE; 
        SortCol (taste);
        return TRUE;
}

int ListClass::IsUbRow (MSG *msg)
/**
Test, ob der Mousecursor ueber der Feldueberschrift steht.
**/
{
        POINT mousepos;
        RECT rect;
        int i;

        if (UbForm.mask == NULL) return FALSE;



        for (i = 0; i < UbForm.fieldanz; i ++)
        {
            if (msg->hwnd == UbForm.mask[i].feldid)
            {
                if (msg->message != WM_LBUTTONDOWN)
                {
                         break;
                }
                if (aktcursor != arrow)
                {
                         SortCol (i);
                }
                break;
            }
        }
        if (i == UbForm.fieldanz) return FALSE;

        return TRUE;


        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);

        if (mousepos.y < 0 ||
            mousepos.y > tm.tmHeight)
        {
                   return FALSE;
        }
        if (mousepos.x > rect.left &&
            mousepos.x < rect.right)
        {
                   return TRUE;
        }
        return FALSE;
}


int ListClass::IsUbEnd (MSG *msg)
/**
Test, ob der Mousecursor ueber dem Ende einer Feldueberschrift steht.
**/
{
        POINT mousepos;
        RECT rect;
        int i;
        int diff;
        int x;

        if (LineForm.mask == NULL) return FALSE;

        for (i = 0; i < UbForm.fieldanz; i ++)
        {
            if (msg->hwnd == UbForm.mask[i].feldid)
            {
                break;
            }
        }
        if (i == UbForm.fieldanz) return FALSE;

        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);

        diff = 0;
        if (LineForm.frmstart)
        {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
        }
        if (mousepos.y < 0 ||
            mousepos.y > tm.tmHeight)
        {
                   return FALSE;
        }

        if (mousepos.x <= rect.left ||
            mousepos.x >= rect.right)
        {
                   return FALSE;
        }

        for (i = UbForm.frmstart; i < UbForm.fieldanz; i ++)
        {
                   x = (UbForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;
                   if ((x - mousepos.x) < 4 && 
                       (x - mousepos.x) >= 0) 
                   {
                       if (i == 0)
                       {
                            continue;
                       }
                       else
                       {
                            movfield = i - 1;
                            return TRUE;
                       }
                   }
                   else if ((x - mousepos.x) < 0 && 
                            (x - mousepos.x) > -4) 
                   {
                            movfield = i - 1;
                            return TRUE;
                   }
        }
        x = (UbForm.mask[i - 1].pos[1] + UbForm.mask[i - 1].length 
             - diff) * tm.tmAveCharWidth;
        if ((x - mousepos.x) < 4 && 
            (x - mousepos.x) >= 0) 
        {
                    movfield = i - 1;
                    return TRUE;
        }
        else if ((x - mousepos.x) < 0 && 
                 (x - mousepos.x) > -4) 
        {
                    movfield = i - 1;
                    return TRUE;
        }
        return FALSE;
}



void ListClass::ShowArrow (BOOL mode)
/**
Mousecursor als anzeigen.
**/
{

        if (mode == FALSE && IsArrow == FALSE)
        {
            return;
        }

        if (mode)
        {
                IsArrow = 1;
                if (arrow == 0)
                {
//                            arrow =  LoadCursor (NULL, IDC_SIZEWE);
                            arrow =  LoadCursor (hMainInst, 
                                                 "oarrow");
                }
                oldcursor = SetCursor (arrow);
                aktcursor = arrow;
        }
        else
        {
                IsArrow = 0;
                SetCursor (oldcursor);
                aktcursor = oldcursor;
        }
}


HWND mLine = 0;

void ListClass::StartMove (void)
{
        RECT rect;
        POINT mousepos;

        
        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        InMove = 1; 
        GetClientRect (mamain3, &rect);
        mLine = CreateWindow ("static",
                                 "",
                                WS_CHILD | 
                                WS_VISIBLE |
                                SS_BLACKRECT, 
                                mousepos.x, tm.tmHeight,
                                2, rect.bottom,
                                mamain3,
                                NULL,
                                hMainInst,
                                NULL);
}


void ListClass::MoveLine (void)
/**
Line bewegen.
**/
{

        RECT rect;
        POINT mousepos;
       
        if (mLine == NULL) return;

        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);

        MoveWindow (mLine, mousepos.x, tm.tmHeight, 
                           2, rect.bottom, TRUE);
}

void ListClass::DestroyField (int movfield)
/**
Ein Feld aus der Maske loeschen.
**/
{
        int i;
        int pos;
        RECT rect;

        if (UbForm.fieldanz <= 1) return;


        CloseControls (&UbForm);
        GetClientRect (mamain3, &rect);
        rect.top += tm.tmHeight;

        if (movfield > 0 && UbForm.mask[movfield].item)
        {
                  delete UbForm.mask[movfield].item;
                  UbForm.mask[movfield].item = NULL;
        }
        if (movfield > 0 && DataForm.mask[movfield].item)
        {
                  delete DataForm.mask[movfield].item;
                  DataForm.mask[movfield].item = NULL;
        }

        if (movfield > 1)
        {
                  if (LineForm.mask[movfield - 1].item)
                  {
                           delete LineForm.mask[movfield - 1].item;
                           LineForm.mask[movfield - 1].item = NULL;
                  }
        }

        pos = UbForm.mask[movfield].pos[1];
        for (i = movfield; i < UbForm.fieldanz - 1; i ++)
        {
/*
                   memcpy (&ziel[i], &ziel[i + 1],
                           sizeof (FELDER));
*/
                   memcpy (&UbForm.mask[i], &UbForm.mask[i + 1],
                           sizeof (field));
                   memcpy (&DataForm.mask[i], &DataForm.mask[i + 1],
                           sizeof (field));
                   if (i > 0)
                   {
                       memcpy (&LineForm.mask[i - 1], &LineForm.mask[i],
                               sizeof (field));
                   }

                   UbForm.mask[i].pos[1]   = pos;
                   DataForm.mask[i].pos[1] = pos;

                   if (i > 0)
                   {
                         LineForm.mask[i - 1].pos[1] = pos;
                   }

                   pos += UbForm.mask[i].length;
        }
        
        UbForm.fieldanz --;
        DataForm.fieldanz --;
        LineForm.fieldanz --;
        banz --;

        if (TrackNeeded ())
        {
           CreateTrack ();
           SetScrollRange (hTrack, SB_CTL, 0,
                                   banz - 1, TRUE);
        }
        else
        {
           DestroyTrack ();
        }
        if (hTrack == NULL) return;

        GetClientRect (mamain2, &rect);
        display_form (mamain3, &UbForm, 0, 0);
        InvalidateRect (mamain3, &rect, TRUE);
}


void ListClass::StopMove (void)
{
        POINT mousepos;
        int x;
        int lenu;
        int len;
//        int lend;
        int diff;
        RECT rect;
        int i;
		int datapos;
        static int diff0;
        static int movfield0;
		SIZE size;
		HDC hdc;

		hdc = GetDC (mamain3);
	    GetTextExtentPoint32 (hdc, "X", 1, &size);
		ReleaseDC (mamain3, hdc);
        InMove = 0;
        if (mLine)
        {
            DestroyWindow (mLine);
            mLine = NULL;
        }
        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);
        rect.top += tm.tmHeight;


        diff = 0;
        if (LineForm.frmstart)
        {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
        }

        if (movfield < UbForm.fieldanz - 1)
        {
                 x = (UbForm.mask[movfield + 1].pos[1] - getFontlen (diff)) * 
                                 tm.tmAveCharWidth;
//                 x = (UbForm.mask[movfield + 1].pos[1] - diff) * 
//                                 size.cx;
        }
        else
        {
                 x = (UbForm.mask[movfield].pos[1] + 
                      UbForm.mask[movfield].length - diff) *
                                 tm.tmAveCharWidth;
        }

        if ((x - mousepos.x) < 4 && 
            (x - mousepos.x) > -4) 
        {
                  return;
        }

        x = mousepos.x / tm.tmAveCharWidth + diff;

		for (i = movfield; i < DataForm.fieldanz; i ++)
		{
		    datapos = DataForm.mask[i].pos[1];
			if (datapos >= getDataFontlen (UbForm.mask[movfield].pos[1])) break;
		}

        if (movfield < UbForm.fieldanz - 1)
        {
                 diff = x - UbForm.mask[movfield + 1].pos[1]; 
        }
        else
        {
                 diff = x - (UbForm.mask[movfield].pos[1] + 
                             UbForm.mask[movfield].length); 
        }

/*        
        len = UbForm.mask[movfield].length + diff;
        lend = len - UbForm.mask[movfield].length;
		lend = UbForm.mask[movfield].length + lend;
*/
        if (len <= 0)
        {
/*
            if (PageView) return;
    		if (FocusWindow)
			{
			     DestroyFocusWindow ();
			}
            DestroyField (movfield);
*/
            return;
        }

		if (i == DataForm.fieldanz)
		{
	        datapos = DataForm.mask[i - 1].pos[1];
            len  = DataForm.mask[i - 1].length + getDataFontlen (diff);
		}
		else
		{
            len  = DataForm.mask[i].length + getDataFontlen (diff);
		}

        diff0 = diff;
        movfield0 = movfield;
        lenu = UbForm.mask[movfield].length + diff;

        if (len <= 0)
        {
            if (PageView) return;
    		if (FocusWindow)
			{
			     DestroyFocusWindow ();
			}
            DestroyField (movfield);
            return;
        }

/*
        if (movfield < UbForm.fieldanz - 1)
        {
             DataForm.mask[movfield + 1].pos[1] += x - UbForm.mask[movfield + 1].pos[1];
             UbForm.mask[movfield + 1].pos[1] = x;
             LineForm.mask[movfield].pos[1] = x;

             for ( i = movfield + 2; i < UbForm.fieldanz; i ++)
             {
                  UbForm.mask[i].pos[1]   += diff;
                  DataForm.mask[i].pos[1] += diff;
                  LineForm.mask[i - 1].pos[1] += diff;
             }
        }
*/

        if (movfield < UbForm.fieldanz - 1)
        {
             UbForm.mask[movfield + 1].pos[1] = x;
             LineForm.mask[movfield].pos[1] = getDataFontlen (x);

             for ( i = movfield + 2; i < UbForm.fieldanz; i ++)
             {
                   UbForm.mask[i].pos[1]   += diff;
                   LineForm.mask[i - 1].pos[1] += getDataFontlen (diff);
             }
        }

		for (i = 0; i < DataForm.fieldanz; i ++)
		{

			   if (DataForm.mask[i].pos[1] == datapos)
			   {
                         DataForm.mask[i].length = len;
			   }

			   if (DataForm.mask[i].pos[1] > datapos)
			   {
                  DataForm.mask[i].pos[1] += getDataFontlen (diff);
			   }
		}


        LineForm.mask[LineForm.fieldanz - 1].pos[1] += getDataFontlen (diff);
/*
        UbForm.mask[movfield].length = len;
        DataForm.mask[movfield].length = lend;
*/

        UbForm.mask[movfield].length = lenu;
		if (FocusWindow)
		{
			DestroyFocusWindow ();
		}

        CloseControls (&UbForm);
        display_form (mamain3, &UbForm, 0, 0);
        InvalidateRect (mamain3, &rect, TRUE);
}

/*  
void ListClass::StopMove (void)
{
        POINT mousepos;
        int x;
        int lenu;
        int len;
        int diff;
        RECT rect;
        int i;
		int datapos;
        static int diff0;
        static int movfield0;


        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);
        rect.top += tm.tmHeight;

        diff = 0;
        if (LineForm.frmstart)
        {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
        }

        if (movfield < UbForm.fieldanz - 1)
        {
                 x = (UbForm.mask[movfield + 1].pos[1] - diff) * 
                                 tm.tmAveCharWidth;
        }
        else
        {
                 x = (UbForm.mask[movfield].pos[1] + 
                       UbForm.mask[movfield].length - diff) *
                                 tm.tmAveCharWidth;
        }

        if ((x - mousepos.x) < 4 && 
            (x - mousepos.x) > -4) 
        {
                  return;
        }

        x = mousepos.x / tm.tmAveCharWidth + diff;
		for (i = movfield; i < DataForm.fieldanz; i ++)
		{
		    datapos = DataForm.mask[i].pos[1];
			if (datapos >= UbForm.mask[movfield].pos[1]) break;
		}
        if (movfield < DataForm.fieldanz - 1)
        {
                 diff = x - UbForm.mask[movfield + 1].pos[1]; 
        }
        else
        {
                 diff = x - (UbForm.mask[movfield].pos[1] + 
                               UbForm.mask[movfield].length); 
        }

		if (i == DataForm.fieldanz)
		{
	        datapos = DataForm.mask[i - 1].pos[1];
            len  = DataForm.mask[i - 1].length + diff;
		}
		else
		{
            len  = DataForm.mask[i].length + diff;
		}


        diff0 = diff;
        movfield0 = movfield;
        lenu = UbForm.mask[movfield].length + diff;

        if (len <= 0)
        {
            if (PageView) return;
    		if (FocusWindow)
			{
			     DestroyFocusWindow ();
			}
            DestroyField (movfield);
            return;
        }


        if (movfield < DataForm.fieldanz - 1)
        {

             UbForm.mask[movfield + 1].pos[1] = x;
             LineForm.mask[movfield].pos[1] = x;

             for ( i = movfield + 2; i < DataForm.fieldanz; i ++)
             {
                         UbForm.mask[i].pos[1]   += diff;
                         LineForm.mask[i - 1].pos[1] += diff;
             }
        }

		for (i = 0; i < DataForm.fieldanz; i ++)
		{

			   if (DataForm.mask[i].pos[1] == datapos)
			   {
                         DataForm.mask[i].length = len;
			   }

			   if (DataForm.mask[i].pos[1] > datapos)
			   {
                  DataForm.mask[i].pos[1] += diff;
			   }
		}

        LineForm.mask[LineForm.fieldanz - 1].pos[1] += diff;
        UbForm.mask[UbForm.fieldanz - 1].pos[1] += diff;

        UbForm.mask[movfield].length = lenu;
        DataForm.mask[movfield].length = len;

		if (FocusWindow)
		{
			DestroyFocusWindow ();
		}

        CloseControls (&UbForm);
        display_form (mamain3, &UbForm, 0, 0);
        InvalidateRect (mamain3, NULL, TRUE);
        UpdateWindow (mamain3);
}
*/


void ListClass::DisplayList (void)
/**
Liste anzeigen.
**/
{
        HDC hdc;
        
        paintallways = 1;
        hdc = GetDC (mamain3);
        ShowDataForms (hdc);
        PrintVlines (hdc);
        PrintHlines (hdc);
	    if (! FocusWindow)
        {
          	     SetFeldFocus (hdc, AktRow, AktColumn);
		}
        else
        {
	             SetFocusFrame (hdc, AktRow, AktColumn );
                 PrintSlinesSatzNr (hdc);
        }
        ReleaseDC (mamain3, hdc);
        paintallways = 0;
}


BOOL ListClass::IsMouseMessage (MSG *msg)
/**
Mouse-Meldungen pruefen.
**/
{
    /*
        if (IsComboMessage && (*IsComboMessage) (msg))
        {
              return FALSE;
        }

        if (opencombobox)
        {
              return FALSE;
        }
    */

        switch (msg->message)
        {
        
              case WM_MOUSEMOVE :
                      if (InMove)
                      {
                            SetCursor (aktcursor);
                            MoveLine ();
                            return TRUE;
                            return TRUE;
                      }
                      if (IsUbEnd (msg))
                      {
                              ShowArrow (TRUE);
                      }
                      else
                      {
                              ShowArrow (FALSE);
                      }
                      return FALSE;
              case WM_LBUTTONDOWN :
                      if (IsUbRow (msg) == FALSE)
//                      if (IsInUb (msg) == FALSE)
                      {
                              return IsInListArea (msg);
                      }
                      if (aktcursor == arrow)
                      {
                                SetCursor (aktcursor);
                                StartMove ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      return TRUE;
              case WM_LBUTTONDBLCLK :
                      if (IsUbRow (msg) == FALSE)
                      {
                              return FALSE;
                      }
                      if (aktcursor == arrow)
                      {
                                SetCursor (aktcursor);
                                StartMove ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      return TRUE;
              case WM_LBUTTONUP :
                      if (InMove)
                      {
                                StopMove ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      if (IsUbRow (msg))
                      {
                              return TRUE;
                      }
                      else
                      {
                              return FALSE;
                      }
        }
        return FALSE;
}

void ListClass::OnPaint (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         HDC hdc;


         if (hWnd == mamain2)
         {
                    MoveListWindow ();
         }
         else if (hWnd == mamain3)
         {
                    hdc = BeginPaint (mamain3, &aktpaint);
                    if (NoPaint == FALSE)
                    {
                         ShowDataForms (hdc);
                         PrintVlines (hdc);
                         PrintHlines (hdc);
					     if (! FocusWindow)
                         {
                	          SetFeldFocus (hdc, AktRow, AktColumn);
                         }
                         else
                         {
 		                       SetFocusFrame (hdc, AktRow, AktColumn);
                               PrintSlinesSatzNr (hdc);
                         }
                    }
                    EndPaint (mamain3,&aktpaint);
         }
         else if (hWnd == choise)
         {
                    hdc = BeginPaint (choise, &aktpaint);
                    ChoiseLines (hdc); 
                    EndPaint (choise, &aktpaint);
         }
}

void ListClass::OnHScroll (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         if ((HWND) lParam == hTrack)
         {
             HScroll (wParam, lParam);
         }
}

void ListClass::OnVScroll (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         if ((HWND) lParam == vTrack)
         {
             VScroll (wParam, lParam);
         }
}


void ListClass::OnSize (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         return;
}

void ListClass::BreakList (WPARAM wParam)
/**
Listenwerfassung beenden.
**/
{
	     list_break = TRUE;
		 PostMessage (mamain3, WM_USER, wParam, 0l);
}


int ListClass::ProcessMessages(BOOL ende_mode)
{
        MSG msg;
        int taste;
        char KeyState [256];
        PBYTE lpKeyState; 

        if (RowMode == 0)
        {
		        AktRow = 0;
		        AktColumn = 0;
        }
		list_break = FALSE;

		if (recanz > 1)
		{
			 if (RowMode)
			 {
		            SetFeldFocus0 (AktRow, FirstColumnR ());
			 }
			 else
			 {
		            SetFeldFocus0 (AktRow, FirstColumn ());
			 }
		}
        while (GetMessage (&msg, NULL, 0, 0))
        {
			 if (list_break) return msg.wParam;
			 
             if (msg.message == WM_KEYDOWN)
             {
                       if (GetKeyState (VK_CONTROL) < 0)
                       {
                              taste = (int) msg.wParam;
                              if (taste == (int) 'C')
                              {
                                  ToClipboard ();
                                  continue;
                              }
                              else if (taste == (int) 'V')
                              {
                                  FromClipboard ();
                                  continue;
                              }
                              else if (taste == VK_INSERT)
                              {
                                   SwitchMarked (AktRow);
                                   ShowAktRow ();
                                  continue;
                              }
                              else if (taste >= (int) '0' &&
                                       taste <= (int) '9')
                              {
                                  if (TestKeySort (taste))
                                  {
                                      continue;
                                  }
                              }
                       }
                       else if (GetKeyState (VK_SHIFT) < 0)
                       {
                              taste = (int) msg.wParam;
                              if (taste == VK_INSERT)
                              {
                                   SetToMark (AktRow);
                                   InvalidateRect (mamain3, NULL, FALSE);
                                   UpdateWindow (mamain3);
                                   continue;
                              }
                       }

                       switch (msg.wParam)
                       {
                              case VK_F5 :
								       syskey = KEY5; 
                                       return msg.wParam;
                              case VK_F3 :
                              case VK_F4 :
                              case VK_F6 :
                              case VK_F7 :
                              case VK_F8 :
                              case VK_F9 :
                              case VK_F10 :
                              case VK_F11 :
                              case VK_F12 :
                              case VK_RETURN:
								  if (ende_mode == TRUE)
								  {
								       syskey = KEY5; 
                                       return msg.wParam;
								  }
                              case VK_DOWN :
                              case VK_UP :
                              case VK_NEXT :
                              case VK_PRIOR :
                              case VK_HOME :
                              case VK_END :
                              case VK_TAB :
								       FunkKeys (msg.wParam,
										         msg.lParam);
									   continue;
                              case 'A' :
                                 if (GetKeyState (VK_CONTROL) < 0)
                                 {
                                       lpKeyState = (PBYTE) KeyState; 
                                       GetKeyboardState (lpKeyState);
                                       lpKeyState [VK_CONTROL] = (char) 0;
                                       SetKeyboardState (lpKeyState);
                                       msg.wParam = '0';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       msg.wParam = 'X';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       msg.wParam = 'A';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       continue;
                                 }
                       }
             }
             if (IsMouseMessage (&msg))
             {
                       continue;
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
        return msg.wParam;
}

void ListClass::MarkedToClipboard (void)
{
           HGLOBAL hb;
           LPVOID p;
           int row;
           int col;
           unsigned long len;
           char text [512];
           char *txt;
           HCURSOR oldcursor;
           BOOL First = TRUE;

           oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
           len = (unsigned long) recanz * (zlen + banz);
           hb = GlobalAlloc (GHND, len);
           if (hb == NULL) return;
           p = GlobalLock (hb);
           strcpy ((char *) p, "");
           for (row = 0; row < recanz; row ++)
           {
               if (MarkedRows [row] == FALSE) continue;
               if (row > 0 && First == FALSE)
               {
                   strcat ((char *) p, NL);
               }
               First = FALSE;
               memcpy (ausgabesatz, SwSaetze[row], zlen);
               for (col = 0; col < banz; col ++)
               {
                   if (col > 0)
                   {
                       strcat ((char *) p, TAB);
                   }
                   DataForm.mask[col].item->GetFeld (text);
                   ToFormat (text, &DataForm.mask[col]);
                   clipped (text);
                   txt = clippleft (text);
                   if (strlen (txt) > 0)
                   {
                      strcat ((char *) p, txt);
                   }
                   else
                   {
                      strcat ((char *) p, " ");
                   }
               }
           } 
           GlobalUnlock (hb);
           OpenClipboard (NULL);
           EmptyClipboard ();
           SetClipboardData (CF_TEXT, hb);
           CloseClipboard ();
           memcpy (ausgabesatz, SwSaetze[AktRow], zlen);
           SetCursor (oldcursor);
}


void ListClass::ToClipboard (void)
{
          HGLOBAL hb;
          LPVOID p;
          char *text;

          if (IsMarked ())
          {
              MarkedToClipboard ();
              return;
          }
          if (ListFocus == 0) return;
		  text = DataForm.mask[AktColumn].item->GetFeldPtr ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (NULL);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void ListClass::FromClipboard (void)
{
          HGLOBAL hb;
          LPVOID p;
          char *text;

          if (ListFocus == 0) return;
		  text = DataForm.mask[AktColumn].item->GetFeldPtr ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (NULL);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          if (strlen (text) > (unsigned int) DataForm.mask[AktColumn].length)
          {
              return;
          }
          memcpy (SwSaetze [AktRow], ausgabesatz, zlen); 
          ShowAktRow ();
}



// Ende ListClass


void ListClassDB::FreeBezTab (void)
/**
Speicher fuer feldbztab freigeben.
**/
{
        int i;

        for (i = 0; i < fbanz; i ++)
        {
            if (feldbztab[i])
            {
                GlobalFree (feldbztab[i]);
                feldbztab[i] = NULL;
            }
        }
        fbanz = 0;
}

void ListClassDB::FillUbForm (field *feld, char *feldname, int len, int spalte,
                 int BuId  )
/**
Maskenfeld fuer Ueberschruft fuellen.
**/
{
        char *bez;
        char *name;
        char ibez [81];
        char *p;

        name = feldname;
        memcpy (feld, &iButton, sizeof (field)); 
        bez = GetIfItem (name, ibez);
#ifndef _NINFO
        if (ReadItemLabel)
        {
          if (bez == NULL)
          {
            p = strchr (feldname, '$');
            if (p != NULL)
            {
                bez = GetItemName (p + 1);
            }
            else
            {
                bez = GetItemName (feldname);
            }
          }
        }
#endif
        if (bez)
        {
                   feldbztab[fbanz] = (char *) GlobalAlloc (0, 81);
                   if (feldbztab[fbanz])
                   {
                       strcpy (feldbztab[fbanz], bez);
                       name = feldbztab [fbanz];
                       if (fbanz < MAXBEZ - 1)
                       {
                           fbanz ++;
                       }
                   }
        }
        clipped (name);
        feld->item       = new ITEM (feldname, name, "", 0);
        feld->length     = getFontlen (len + 2);
        feld->rows       = UbRows;
        feld->pos[0]     = 0;
        feld->pos[1]     = spalte;
        feld->BuId       = BuId;
}

void ListClassDB::MakeUbForm (void)
/**
Form fuer Ueberschrift generieren.
**/
{
        int i, j;
        int spalte;
        int BuId;
        int len;

        memcpy (&UbForm, &IForm, sizeof (form));
        UbForm.fieldanz = banz;
        UbForm.mask = (field *) GlobalAlloc (0, (banz + 2) * sizeof (field));
        if (UbForm.mask == NULL)
        {
                 disp_mess ("Kein Speicherplatz", 2);
                 ExitProcess (1);
        }
        BuId = 600;
        if (NoRecNr == FALSE)
        {
                  spalte = getFontlen (6);
                  fUbSatzNr.mask[0].length = getFontlen (6);
        }
        else
        {
                  spalte = 0;
        }
        for (i = 0, j = 0; i < banz; i ++)
        {
//                 len = ziel[i].feldlen + 3;
                 if (ziel[i].feldtyp[0] == 'Z') continue;
                 len = ziel[i].feldlen + 3;
                 if (len > MAXLEN) len = MAXLEN;

                 if (Rows != NULL && Rows->GetCols () != NULL &&  
                     Rows->GetCol (j) != NULL)
                 {
							  if (Rows->GetCol (i)->GetLength () != 0)
							  {
								  len = Rows->GetCol (i)->GetLength ();
							  }
                 }

                 this->FillUbForm (&UbForm.mask[j], ziel[i].feldname,
                              len, spalte,
                              BuId);
                 if (RowCaptions != NULL && 
                     RowCaptions[j] != NULL)
                 {
                              UbForm.mask[j].item->SetFeldPtr (RowCaptions [j]);
                 }
                 if (Rows != NULL && Rows->GetCols () != NULL &&  
                     Rows->GetCol (j) != NULL)
                 {
                              char *p = Rows->GetCol (j)->GetCaption ();
                              clipped (p);
                              if (strcmp (p, " ") > 0)
                              {
                                  UbForm.mask[j].item->SetFeldPtr (Rows->GetCol (j)->GetCaption ());
                              }
                 }
                 spalte += getFontlen (len + 2);
                 BuId ++;
                 j ++;
        }

        UbForm.fieldanz = j;
        if (UbForm.mask[0].rows == 0)
        {
            fUbSatzNr.mask[0].rows = 0;
            UbHeight = tm.tmHeight + tm.tmHeight / 3; 
        }
        else 
        {
            fUbSatzNr.mask[0].rows = UbForm.mask[0].rows;
            UbHeight = tm.tmHeight * (UbForm.mask[0].rows); 
        }
        if (NoRecNr == FALSE)
        {
			     CloseControls (&fUbSatzNr);
                 display_form (mamain3, &fUbSatzNr, 0, 0);
        }
        display_form (mamain3, &UbForm, 0, 0);
        if (TrackNeeded ())
        {
                 CreateTrack ();
        }
}



void ListClassDB::SwitchPage (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       DestroyTrack ();
       DestroyVTrack ();
	   DestroyFocusWindow ();
       FreeUbForm ();
//       FreeDataForm ();
       FreeLineForm ();
       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                AktPage     = AktRow;
                scrollposS  = scrollpos;
                banzS = banz;
                zlenS = zlen;
                LstZiel = ziel;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec (SwSaetze [pos]);
                SelRow = 0;
                ziel = DlgZiel;
                ausgabesatz = DlgSatz;
                zlen = Dlgzlen;
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);

//              memcpy (&RowUb,  &UbForm,   sizeof (form));
                memcpy (&RowData,&DataForm, sizeof (form));
//                memcpy (&RowLine,&LineForm, sizeof (form));
//                UbRow   = &RowUb;
                DataRow = &RowData;
//                lineRow = &RowLine;
                SetPos (FirstColumnR (), 1);

                this->MakeUbForm ();
                SetSaetze (DlgSaetze);
                Initscrollpos ();
                MakeDataForm0 ();
       }
       else
       {
                FreeDataForm ();
                memcpy (&DataForm, &RowData,sizeof (form));
                FreeDataForm ();
                ziel = LstZiel;
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                this->MakeUbForm ();
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec (SwSaetze [pos]);
                Initscrollpos ();
                MakeDataForm0 ();
                Setscrollpos (scrollposS);
       }
}

