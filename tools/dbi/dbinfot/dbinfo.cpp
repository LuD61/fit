#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_wdebug.h"
#include "mo_wmess.h"
#include "mo_choise.h"

#ifdef BIWAK
#include "conf_env.h"
#include "mo_bicurs.h"
#else
#ifdef ODBC
#include "mo_odbc.h"
#else
#include "mo_curso.h"
#endif
#endif

inline void Closedbase (LPSTR DataBase) 
{
#ifdef ODBC
	closedbase (DataBase); 
#else
    closedbase ();
#endif
} 

#include "mo_txt.h"
#include "gprintl.h"
#include "mo_gdruck.h"
//#include "mo_enter.h"
#include "enterfunc.h"
#include "lbox.h"
#include "searchtables.h"
#include "searchcols.h"
/*
#ifdef BIWAK
#include "conf_env.h"
#include "mo_bicurs.h"
#else
#ifdef ODBC
#include "mo_odbc.h"
#else
#include "mo_curso.h"
#endif
#endif
*/
#include "RowCaptDlg.h"
#include "RowDlg.h"
#include "RowProps.h"
#include "textdlg.h"
#include "stringwork.h"
#include "DdeDial.h"
#include "Text.h"


#ifndef MAXEXEC
#define MAXEXEC 20
#endif

#define MAXLEN 40
#define MAXRECS 100000

#define IDM_FRAME       901
#define IDM_REVERSE     902
#define IDM_NOMARK      903
#define IDM_EDITMARK    904
#define IDM_PAGEVIEW    905
#define IDM_LIST        906
#define IDM_PAGE        907
#define IDM_FONT        908
#define IDM_FIND        909
#define IDM_EDITMARK3D  910
#define IDM_SQL         911
#define IDM_PAGELEN     912
#define IDM_SQL0        1913
#define IDM_DB          913
#define IDM_MARKALL     914
#define IDM_DELMARK     915
#define IDM_SEND_MAIL   916
#define IDM_LOAD_MACRO  917
#define IDM_SAVE_MACRO  918
#define IDM_ROWCAPT     919
#define IDM_DEL_ROWCAPT 920
#define IDM_PRINT_CAPT  921
#define IDM_PRINTMODE   922
#define IDM_QUERPRINT   923
#define IDM_ROWCAPTION  924
#define IDM_ITEMLABEL   925


static BOOL upd_mode = FALSE; 
static BOOL positions_mode = FALSE; 
static BOOL ende_mode = FALSE; 

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
static int ProcessMessages(void);
void     InitFirstInstance(HANDLE);
BOOL     InitNewInstance(HANDLE, int);
static   int WriteList (void);
static   int DeleteLine (void);
static   void InputSQL (void);
static   void InputSQLEx (void);
static   void close_cursor (void);
static   void ScrollMenue (PMENUE *, int);
static   int sql_error (void);
static void LoadMacro (char *);
static BOOL EnterAndPart (void);
static int RunDde (char *, char *, char *, char *, char *);
static void EnterDbase (void);
static BOOL ChangeEnvironment (char *String);
static void SaveRect(void);
static void GetRect(void);
 

HANDLE  hMainInst;
HWND    hMainWindow;

static char *TXT_WORK   = "Bearbeiten";
static char *TXT_SHOW   = "Anzeigen";
static char *TXT_DEL    = "L�schen";
static char *TXT_PRINT  = "Drucken";

static char *TXT_ACTIVE = TXT_WORK;


static int IDM_ACTIVE = IDM_WORK;

static double RowHeight = 1.5;
static int UbRows = 0;
static int AktFocus = 1;
static int EditMode = 0;
static char *DbServer = NULL;
static char *User = "informix";
static char *Passw = "t0pfit";
#ifdef ODBC
static char *DataBase = NULL;
#else
static char *DataBase = "bws";
#endif
static int aufruf = 0;

char progname[512] = {"dbinfot"};

HMENU hMenu;
WMESS Wmess;

CHOISE *Choise1 = NULL;
int PrintMode = 1;
static int pagelen = 66;
static int pagelen0 = 59;
static int ublen = 4;
static char *where_upd = NULL;
static int ListFocus = -1;
static int Rights = TRUE;
static int SortRow = 0;
static BOOL StartWithSort = FALSE;
static int ScrPrint = 0;
static unsigned int FETT = 0;

static int dbcursor = -1;
static int updcursor = -1;

static RowProps Rows (500);
static char *RowCaptions [500];
static int RowCaptAnz = 0;
static BOOL MacroCaption = FALSE;

static BOOL AddMenue = TRUE;
static BOOL AddToolbar = TRUE;
static BOOL WinSet = FALSE;
static int wx = -1; 
static int wy = -1;
static int wcx = -1;
static int wcy = -1;
static BOOL wmaximized = FALSE;

BOOL GrPrint = TRUE;

struct PMENUE dateimen[] = {
	                        "&1 �ffnen",      " ", NULL, IDM_WORK, 
	                        "&2 Speichern",   " ", NULL, KEY12, 
                            "&3 Tabelle",     " ", NULL, IDM_SHOW,
						    "&4 Query",       " ", NULL, IDM_DEL,
							"",               "S", NULL, 0, 
						    "&5 Drucken",     "G", NULL, IDM_PRINT,
						    "&6 E-Mail",      "G", NULL, IDM_SEND_MAIL,
//						    "&6 Druckerwahl", " ", NULL, IDM_CHPRINT,
						    "&7 Seite einrichten", " ", NULL, IDM_PAGELEN,
							"",             "S", NULL, 0, 
	                        "B&eenden",     " ", NULL, KEY5,

                            NULL, NULL, NULL, 0};
struct PMENUE bearbeiten[] = {
	                        "&Suchen",                     " ",  NULL, IDM_FIND, 
                             "kopieren",                   " ",  NULL, IDM_COPY,
                             "einf�gen",                   " ",  NULL, IDM_INS,
 							 "",                           "S",  NULL, 0, 
                             "alles markieren",            " ",  NULL, IDM_MARKALL,
                             "Markierung loeschen" ,       " ",  NULL, IDM_DELMARK,
 							 "",                           "S",  NULL, 0, 
                             "Zeile L�schen F7",           "  ", NULL, KEY7,
                             "Zeile anh�ngen F8",          "  ", NULL, KEY8,
							 "Ansehen F11",                " ",  NULL, KEY11,  
                             NULL, NULL, NULL, 0};


struct PMENUE ansicht[] = {
	                        "&Markierung mit Rahmen",    "C", NULL, IDM_FRAME, 
                            "&inverse Markierung",       " ", NULL, IDM_REVERSE,
							"&Editfenster",              " ", NULL, IDM_EDITMARK,
							"E&ditfenster 3D",           " ", NULL, IDM_EDITMARK3D,
							"&keine Markierung",         " ", NULL, IDM_NOMARK,
                            "&Seitenansicht",            "G", NULL, IDM_PAGEVIEW, 
                            "&Fonteinstellung",          " ", NULL, IDM_FONT,
                            "&ItemTexte",               " ",  NULL, IDM_ITEMLABEL,
 							 "",                        "S",  NULL, 0, 
                            "Druck vom &Bildschirm",    " ",  NULL, IDM_PRINTMODE,
                            "&Immer Quer drucken",      " ",  NULL, IDM_QUERPRINT,
                            "&Spalten�berschrift in Druck",
                                                        "C",  NULL, IDM_ROWCAPTION,
 							 "",                        "S",  NULL, 0, 
                            "S&palteneigenschaften",    " ",  NULL, IDM_ROWCAPT,
                            "Spalteneigenschaften &l�schen",
                                                         " ", NULL, IDM_DEL_ROWCAPT,
                            "�berschrift",               " ", NULL, IDM_PRINT_CAPT,
                             NULL, NULL, NULL, 0};

struct PMENUE extras[] = {
                             "S&Ql-Befehl eingeben",       " ",  NULL, IDM_SQL,
#ifndef BIWAK
                             "&Datenbank ausw�hlen",       " ",  NULL, IDM_DB,
#endif
                             NULL, NULL, NULL, 0};

struct PMENUE macros[] = {
                             "Selectparameter &laden",     " ",  NULL, IDM_LOAD_MACRO,
                             "&Selectparameter speichern", " ",  NULL, IDM_SAVE_MACRO,
                             NULL, NULL, NULL, 0};

struct PMENUE menuetab[] = {"&Datei",      "M", dateimen,   0, 
                            "&Bearbeiten", "M", bearbeiten, 0, 
                            "&Ansicht",    "M", ansicht,    0, 
                            "&Macros",     "M", macros,     0,
                            "&Sql",        "M", extras,     0, 
						     NULL, NULL, NULL, 0};

static int ActiveMark = IDM_FRAME;
static int ActiveEdit = IDM_EDITMARK;

static char InfoCaption [256] = {"\0"};
static char PrintUb[0x1000] = {"\0"};


extern HWND hWndToolTip;
HWND hwndTB;
HWND hwndCombo1;
HWND hwndCombo2;

static TBBUTTON tbb[] =
{
 0,               IDM_WORK,   TBSTATE_ENABLED, 
                               TBSTYLE_BUTTON, 
 0, 0, 0, 0,
 1,               IDM_SHOW,   TBSTATE_ENABLED, 
	                          TBSTYLE_BUTTON,
 0, 0, 0, 0,
 2,               IDM_DEL,    TBSTATE_ENABLED, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 3,               IDM_PRINT,  TBSTATE_INDETERMINATE, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 4,               IDM_SEND_MAIL,  TBSTATE_INDETERMINATE, 
                                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED,
                  TBSTYLE_SEP, 
 0, 0, 0, 0,
 5,               IDM_LIST,   TBSTATE_ENABLED | TBSTATE_CHECKED, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 6,               IDM_PAGE, TBSTATE_INDETERMINATE, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 7,               IDM_INFO,   TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 
 8,               KEYLEFT, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 9,               KEYRIGHT, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
10,               KEYDOWN, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
11 ,               KEYUP, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
};

static char *qInfo [] = {"Neue Datei �ffnen",
                         "Tablle �ffnen",
                         "Query zu Tabelle",
                         "Drucken",
                         "E-Mail",
                         "Funktion f�r aktives Eingabefeld",
                         "S�tze in Listenform anzeigen",
                         "Satz auf einer Seite anzeigen ",
                          0,
                          0, 
                          0};

static UINT qIdfrom [] = {IDM_WORK, IDM_SHOW, IDM_DEL,
                         IDM_PRINT, IDM_SEND_MAIL, IDM_INFO,
                         IDM_LIST, IDM_PAGE,0, 0, 0};
static char *qhWndInfo [] = {"Tabellen-Linien festlegen", 
                             "Farbe f�r Vordergrund und Hintergrund "
                             "festlegen",
                             0};
static HWND *qhWndFrom [] = {&hwndCombo1, 
                             &hwndCombo2,
                             NULL}; 

static char *Combo1 [] = {"wei�e Tabelle" ,
                          "hellgraue Tabelle",
                          "schwarze Tabelle",
                          "graue Tabelle",
                          "vertikal wei�",
                          "vertikal hellgrau",
                          "vertikal schwarz", 
                          "vertikal grau",
                          "horizontal wei�",
                          "horizontal hellgrau",
                          "horizontal schwarz", 
                          "horizontal grau",
                          "keine Linien",
                           NULL};

static char *lineflag [] = {"WT" ,
                           "HT",
                           "ST",
                           "GT",
                           "VW",
                           "VH",
                           "VS", 
                           "VG",
                           "HW",
                           "HH",
                           "HS", 
                           "HG",
                           "NO",
                            NULL};

static char *Combo2 [] = {"wei�er Hintergrund" ,
                          "blauer Hintergrund",
                          "schwarzer Hintergrund",
                          "grauer Hintergrund",
                          "hellgrauer Hintergrund",
						  "dunkelgr�ner Hintergrund",
                           NULL};

static char *bcolflag [] = {"WHITE" ,
                            "BLUE",
                            "BLACK",
                            "GRAY",
                            "LTGRAY",
							"DGREEN",
                            NULL};

static COLORREF Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL,
                            BLACKCOL,
                            BLACKCOL};  

static COLORREF BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL,
							LTGRAYCOL, 
							RGB (0, 128, 128)}; 

static char FontName[40] = {""};

mfont lFont = {
               FontName, 
               90, 
               100, 
               1, 
               RGB (-1, 0, 0), 
               RGB (-1, 0, 0),
               0, 0};

static int m3Size = 0;

char fname [512] = {"\0"};
static char and_part [1024];
static char *EnteredAndPart = NULL;
static char selfields [0x1000];
char *info_name;

static int MoveMain = FALSE;

static struct ItAttr ItAttr [200];
static int attranz = 0;

static struct ItAfter ItBefore [200];
static int beforeanz = 0;

static struct ItAfter ItAfter [200];
static int afteranz = 0;

static struct ItChoise ItChoise [200];
static int choiseanz = 0;

static struct CHATTR ChAttr [200] = {NULL,  0,           0};
static int chaanz = 0;

struct CHATTR ChAttrS [] = {"",    DISPLAYONLY, EDIT};


static ITEM ifname  ("", fname,    "Tabelle    ", 0);
static ITEM iquery  ("", and_part, "Query      ", 0);
static ITEM ifields ("", selfields,   "Felder     ", 0);
static ITEM icancel ("", " Abbrechen ", "", 0);
static ITEM iok     ("", "  Ok  ", "", 0);
static ITEM ichoise ("", "  Auswahl  ", "", 0);

static field _ffname[] = {
&ifname,       60, 0, 1, 1, 0, "80", EDIT | SCROLL, 0, 0, 0,
&iquery,       60, 0, 3, 1, 0, "512", EDIT | SCROLL, 0, 0, 0,
&ifields,      60, 0, 5, 1, 0, "512", EDIT | SCROLL, 0, 0, 0,
//&icancel,      12, 0, 5, 11, 0, "", BUTTON, 0, 0, KEYESC,
//&iok,           7, 0, 5, 26, 0, "", BUTTON, 0, 0, KEY12,    
&icancel,      12, 0, 8, 22, 0, "", BUTTON, 0, 0, KEYESC,
&iok,           7, 0, 8, 35, 0, "", BUTTON, 0, 0, KEY12,    
&ichoise,      12, 0, 8, 43, 0, "", BUTTON, 0, 0, KEY9,    
};

static form ffname = {6, 0, 0, _ffname, 0, 0, 0, 0, NULL};     

static int QueryEnter = 0;


// Buffer fuer alle SW-Saetze 

static PAINTSTRUCT aktpaint;

static char *SwSaetze[MAXRECS];
static BOOL MarkedRows [MAXRECS]; 

/*
static char *DlgSaetze [1000];

static FELDER DlgZiel [] = {
"Bezeichnung",     20, "C", 0, 1,
"Wert",        MAXLEN, "C", 0, 1,
};

static int Dlgzlen = 20 + MAXLEN;
*/
static int PageView = 0;
static unsigned char DlgSatz [20 + MAXLEN + 2];

static FELDER *LstZiel = NULL;
static unsigned char *LstSatz   = NULL;
static int Lstzlen;
static int Lstbanz = 0;

static int SwRecs = 0;
static int ListOK = 0;

static int AktRow = 0;
static int AktColumn = 0;
static int scrollpos = 0;


static TEXTMETRIC tm;

static char *BeforePrint = NULL;
static char *BeforePrintOrg = NULL;
static char *PrintFile = NULL;
static int CaptMode = 0;
static int PrintFont = 0;
static int RowPrintFont = 0;
static BOOL UsePrintForm = FALSE;
static BOOL QuerPrint = FALSE;
static char *DrkDeviceFile = NULL;

#define EXECNORMAL 0
#define EXECWAIT 1
#define EXECHIDE 2
#define CALL 3

static char *Execs [MAXEXEC];
static char ExecModes [MAXEXEC];
static int ExecPtr;
static int ExecOffs = 8000;
static int ExecEnd = 8000 + MAXEXEC;
static char MailParams [512] = {0};


ListClassDB eListe;
//ListClassDB eDialog;

void SetBackground (char *bk)
/**
**/
{
	int i;   
	
	for (i = 0; bcolflag[i]; i ++)
	{
		if (strcmp (bk, bcolflag[i]) == 0)
		{
                   eListe.SetColors (Colors[i], BkColors[i]);
                   SendMessage (hwndCombo2, CB_SELECTSTRING, 0,
                                           (LPARAM) Combo2 [i]);
		}
	}
}



void SetLine (char *line)
/**
**/
{
	int i;   
	
	for (i = 0; lineflag[i]; i ++)
	{
		if (strcmp (line, lineflag[i]) == 0)
		{
                   eListe.SetListLines (i);
                   SetWindowText (hwndCombo1,
                                  Combo1[i]);
                   SendMessage (hwndCombo1, CB_SELECTSTRING, 0,
                                           (LPARAM) Combo1 [i]);
		}
	}
}


void SetAktFocus (char *focus)
/**
**/
{
	
    AktFocus = min (4,atoi (focus));
    eListe.SetListFocus (AktFocus);
    CheckMenuItem (hMenu, ActiveMark, MF_UNCHECKED); 
	switch (AktFocus)
	{
	       case 0 :
  	              CheckMenuItem (hMenu, IDM_FRAME,
				 			            MF_CHECKED); 
				  ActiveMark = IDM_FRAME;
				  break;
	       case 1 :
  	              CheckMenuItem (hMenu, IDM_REVERSE,
				 			            MF_CHECKED); 
				  ActiveMark = IDM_REVERSE;
				  break;
	       case 2 :
  	              CheckMenuItem (hMenu, IDM_NOMARK,
				 			            MF_CHECKED); 
				  ActiveMark = IDM_NOMARK;
				  break;
	       case 3 :
  	              CheckMenuItem (hMenu, IDM_EDITMARK,
				 			            MF_CHECKED); 
				  ActiveMark = IDM_EDITMARK;
				  ActiveEdit = IDM_EDITMARK;
				  break;
	       case 4 :
  	              CheckMenuItem (hMenu, IDM_EDITMARK3D,
				 			            MF_CHECKED); 
				  ActiveMark = IDM_EDITMARK3D;
				  ActiveEdit = IDM_EDITMARK3D;
				  break;
	}
}

void InitExecs (void)
{
    int i;

    for (i = 0; i < MAXEXEC; i ++)
    {
        Execs[i] = NULL;
    }
}


void SetExec (char *ExecTxt, char *Exec, int Mode)
{
    if (ExecPtr >= MAXEXEC) return;

    Execs[ExecPtr] = new char [strlen (Exec) + 1];
    if (Execs[ExecPtr] == NULL) return;
    strcpy (Execs[ExecPtr], Exec);
    ExecModes[ExecPtr] = Mode;
//    InsertMenu (hMenu, IDM_DOCKLIST, MF_BYCOMMAND | MF_STRING, 
//                        ExecOffs + ExecPtr  , ExecTxt);
    if (ExecPtr == 0 && EditMode)
    {
            AppendMenu (GetSubMenu (hMenu, 1), MF_SEPARATOR, 
                        0  , NULL);
    }
    AppendMenu (GetSubMenu (hMenu, 1), MF_STRING, 
                        ExecOffs + ExecPtr  , ExecTxt);
    ExecPtr ++;
}

void SetBeforePrint (char *bf)
{

    BeforePrint = new char [1024];
    strcpy (BeforePrint, bf);
    ChangeEnvironment (BeforePrint);
    BeforePrintOrg = new char [1024];
    strcpy (BeforePrintOrg, BeforePrint);
}


void SetPrintFile (char *pf)
{

    PrintFile = new char [512];
    strcpy (PrintFile, pf);
    ChangeEnvironment (PrintFile);
}

void SetDeviceFile (char *pf)
{

    DrkDeviceFile = new char [512];
    strcpy (DrkDeviceFile, pf);
    ChangeEnvironment (DrkDeviceFile);
}


void FreeExecs (void)
{
    int i;

    for (i = 0; i < ExecPtr; i ++)
    {
        if (Execs[i] != NULL)
        {
            delete Execs [i];
            Execs [i] = NULL;
            DeleteMenu (hMenu, ExecOffs + i , MF_BYCOMMAND);
        }
    }
    ExecPtr = 0;
}

void SetAttr (void)
/**
Attribute aus swfont holen.
**/
{
	     FILE *fp;
		 char buffer [512];
		 char *etc;
		 int anz;
		 char *fname = "swfont";
#ifdef BIWAK
		 etc = getenv ("ETC");
#else
		 etc = getenv ("BWSETC");
#endif

         if (BeforePrint)
         {
             delete BeforePrint;
             BeforePrint = NULL;
         }

         if (BeforePrintOrg)
         {
             delete BeforePrintOrg;
             BeforePrintOrg = NULL;
         }

         if (PrintFile)
         {
             delete PrintFile;
             PrintFile = NULL;
         }

         if (DrkDeviceFile)
         {
             delete DrkDeviceFile;
             DrkDeviceFile = NULL;
         }


         CaptMode = 0;
         PrintFont = 0;
         RowPrintFont = 0;

         UsePrintForm = FALSE;
         QuerPrint = FALSE;

		 if (etc == NULL)
		 {
			 etc = "c:\\user\\fit\\etc";
		 }
		 if (strchr (fname, '\\'))
		 {
		          sprintf (buffer, "%s", fname);
		 }
		 else
		 {
		          sprintf (buffer, "%s\\%s", etc,fname);
		 }
		 fp = fopen (buffer, "r");
         if (fp == NULL) return;

		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$BACKGROUND", 11) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    SetBackground (wort[1]);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$LINES", 6) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    SetLine (wort[1]);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$FOCUS", 6) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    SetAktFocus (wort[1]);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$ROWHEIGHT", 10) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
					if (anz > 1)
					{
                            eListe.SetRowHeight (ratod (wort[1]));
					}
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$UBROWS", 7) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
					if (anz > 1)
					{
                            eListe.SetUbRows (atoi (wort[1]));
					}
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$SCROLLWAIT", 11) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
					if (anz > 1)
					{
                            eListe.SetScrollWait (atol (wort[1]));
					}
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$ITEMLABEL", 11) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
					if (anz > 1)
					{
                            ListClass::ReadItemLabel = atol (wort[1]);
					}
				    break;
			 }
		 }

         if (ListClass::ReadItemLabel)
         {
             CheckMenuItem (hMenu, IDM_ITEMLABEL, MF_CHECKED);
         }
         else
         {
             CheckMenuItem (hMenu, IDM_ITEMLABEL, MF_UNCHECKED);
         }
         fclose (fp);
}


void EnterFileName (void)
{
        char caption [1024];
//      char InitDir [512];
        static char *konvert;
        int anz;
        OPENFILENAME fnstruct;
        static LPCTSTR lpstrFilter = 
            "Info-Datei\0*.inf\0Alle Dateien\0*.*\0,\0";


        konvert = getenv ("KONVERT");
		selfields[0] = (char) 0;
        ZeroMemory (&fnstruct, sizeof (fnstruct));
        fnstruct.lStructSize = sizeof (fnstruct);
        fnstruct.hwndOwner   = AktivWindow;
        fnstruct.lpstrFile   = fname;
        fnstruct.nMaxFile    = 255;
        fnstruct.lpstrFilter = lpstrFilter;
        fnstruct.lpstrInitialDir  = konvert;

        syskey = 0;
        if (GetOpenFileName (&fnstruct) == 0)
        {
               UpdateWindow (hMainWindow);
               syskey = KEY5;
        }
        sprintf (caption, "Info-File %s",
                           fname);
        anz = wsplit (fname, "\\");
        if (anz > 1)
        {
                 strcpy (fname, wort[anz -1]);
        }
		sprintf (InfoCaption, "%s %s", fname, and_part);
        SetWindowText (hMainWindow, caption);
        eListe.InitRecanz ();
        InvalidateRect (eListe.Getmamain2 (), 0, TRUE);
        UpdateWindow (hMainWindow);
}

int StopEnterFile (void)
{
	     break_enter ();
		 return 0;
}

int GetItAttr (char *attr)
{
	int i;
	
	static char *attrstab[] = {"EDIT",
		                      "DISPLAYONLY",
							  "REMOVED",
							  NULL};
	static int attrntab [] = {EDIT,
		                      DISPLAYONLY,
							  REMOVED};

	clipped (attr);
    for (i = 0; attrstab[i]; i ++)
	{
		if (strcmp (attr, attrstab[i]) == 0) return attrntab [i];
	}
	return 0;
}

	
/*
void SetBackground (char *bk)
{
	int i;   
	
	for (i = 0; bcolflag[i]; i ++)
	{
		if (strcmp (bk, bcolflag[i]) == 0)
		{
                   eListe.SetColors (Colors[i], BkColors[i]);
                   SendMessage (hwndCombo2, CB_SELECTSTRING, 0,
                                           (LPARAM) Combo2 [i]);
		}
	}
}



void SetLine (char *line)
{
	int i;   
	
	for (i = 0; lineflag[i]; i ++)
	{
		if (strcmp (line, lineflag[i]) == 0)
		{
                   eListe.SetListLines (i);
                   SetWindowText (hwndCombo1,
                                  Combo1[i]);
                   SendMessage (hwndCombo1, CB_SELECTSTRING, 0,
                                           (LPARAM) Combo1 [i]);
		}
	}
}

void SetAktFocus (char *focus)
{
	
    eListe.SetListFocus (min (4,atoi (focus)));
    CheckMenuItem (hMenu, ActiveMark, MF_UNCHECKED); 
	switch (atoi (focus))
	{
	       case 0 :
  	              CheckMenuItem (hMenu, IDM_FRAME,
				 			            MF_CHECKED); 
				  ActiveMark = IDM_FRAME;
				  break;
	       case 1 :
  	              CheckMenuItem (hMenu, IDM_REVERSE,
				 			            MF_CHECKED); 
				  ActiveMark = IDM_REVERSE;
				  break;
	       case 2 :
  	              CheckMenuItem (hMenu, IDM_NOMARK,
				 			            MF_CHECKED); 
				  ActiveMark = IDM_NOMARK;
				  break;
	       case 3 :
  	              CheckMenuItem (hMenu, IDM_EDITMARK,
				 			            MF_CHECKED); 
				  ActiveMark = IDM_EDITMARK;
				  ActiveEdit = IDM_EDITMARK;
				  break;
	       case 4 :
  	              CheckMenuItem (hMenu, IDM_EDITMARK3D,
				 			            MF_CHECKED); 
				  ActiveMark = IDM_EDITMARK3D;
				  ActiveEdit = IDM_EDITMARK3D;
				  break;
	}
}
*/

void DeniRights (void)
{
        EnableMenuItem (hMenu, IDM_WORK, MF_GRAYED);
        ToolBar_SetState(hwndTB,IDM_WORK, TBSTATE_INDETERMINATE);
        EnableMenuItem (hMenu, IDM_SHOW, MF_GRAYED);
        ToolBar_SetState(hwndTB,IDM_SHOW, TBSTATE_INDETERMINATE);
        EnableMenuItem (hMenu, IDM_DEL, MF_GRAYED);
        ToolBar_SetState(hwndTB,IDM_DEL, TBSTATE_INDETERMINATE);
//        EnableMenuItem (hMenu, IDM_EDITMARK, MF_GRAYED);
//        EnableMenuItem (hMenu, IDM_EDITMARK3D, MF_GRAYED);
        EnableMenuItem (hMenu, IDM_FONT, MF_GRAYED);
        EnableMenuItem (hMenu, IDM_SQL,  MF_GRAYED);
#ifndef BIWAK
        EnableMenuItem (hMenu, IDM_DB,  MF_GRAYED);
#endif
}

void SetWinAttr (void)
/**
Attribute f�r Window aus Inf-Datei holen.
**/
{
	     FILE *fp;
		 char buffer [512];
		 char *konvert;
		 int anz;

		 konvert = getenv ("KONVERT");
		 if (konvert == NULL)
		 {
			 konvert = "c:\\user\\fit\\konvert";
		 }
		 if (strchr (fname, '\\'))
		 {
		          sprintf (buffer, "%s", fname);
		 }
		 else
		 {
		          sprintf (buffer, "%s\\%s", konvert,fname);
		 }
		 fp = fopen (buffer, "r");
         if (fp == NULL)
         {
                  strcpy (buffer, fname); 
            	  fp = fopen (buffer, "r");
         }
         if (fp == NULL) return;

		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$ADDMENUE", 9) == 0)
             {
                 if (anz = wsplit (buffer, " ") > 1)
                 {
                     AddMenue = atoi (wort[1]);
                 }
             }
             else if (memcmp (buffer, "$ADDTOOLBAR", 11) == 0)
             {
                 if (anz = wsplit (buffer, " ") > 1)
                 {
                     AddToolbar = atoi (wort[1]);
                 }
             }
             else if (memcmp (buffer, "$WINX", 5) == 0)
             {
                 if (anz = wsplit (buffer, " ") > 1)
                 {
                     wx = atoi (wort[1]);
                     WinSet = TRUE;
                 }
             }
             else if (memcmp (buffer, "$WINY", 5) == 0)
             {
                 if (anz = wsplit (buffer, " ") > 1)
                 {
                     wy = atoi (wort[1]);
                     WinSet = TRUE;
                 }
             }
             else if (memcmp (buffer, "$WINCX", 6) == 0)
             {
                 if (anz = wsplit (buffer, " ") > 1)
                 {
                     wcx = atoi (wort[1]);
                     WinSet = TRUE;
                 }
             }
             else if (memcmp (buffer, "$WINCY", 6) == 0)
             {
                 if (anz = wsplit (buffer, " ") > 1)
                 {
                     wcy = atoi (wort[1]);
                     WinSet = TRUE;
                 }
             }
             else if (memcmp (buffer, "$MAXIMIZED", 10) == 0)
             {
                     wmaximized = TRUE;
                     WinSet = TRUE;
             }
		 }
}


void SetItAttr (void)
/**
Attribute aus In-Datei holen.
**/
{
	     FILE *fp;
		 char buffer [512];
		 char *konvert;
		 int anz;

		 konvert = getenv ("KONVERT");
		 if (konvert == NULL)
		 {
			 konvert = "c:\\user\\fit\\konvert";
		 }
		 if (strchr (fname, '\\'))
		 {
		          sprintf (buffer, "%s", fname);
		 }
		 else
		 {
		          sprintf (buffer, "%s\\%s", konvert,fname);
		 }
		 fp = fopen (buffer, "r");
         if (fp == NULL)
         {
                  strcpy (buffer, fname); 
            	  fp = fopen (buffer, "r");
         }
         if (fp == NULL) return;

		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$ATTRIBUTE", 10) == 0) break;
		 }


		 if (memcmp (buffer, "$ATTRIBUTE", 10) == 0)
		 {
		     while (fgets (buffer, 255, fp))
			 {
                   if (buffer[0] == '#') continue;
			       if (buffer[0] == '$') break;

			       anz = wsplit (buffer, " ");
			       if (anz < 2) continue;
                   strcpy (ItAttr[attranz].name , wort[0]);
			       ItAttr[attranz].attribut = GetItAttr (wort[1]);
			       attranz ++;
			 }
		     ItAttr [attranz].name[0] = (char) 0;
		     eListe.SetItAttr (ItAttr);
		 }

		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$AFTER", 6) == 0) 
			 {
				    break;
			 }
		 }

		 if (memcmp (buffer, "$AFTER", 6) == 0) 
		 {
			 afteranz = 0;
 		     while (fgets (buffer, 255, fp))
			 {
                    if (buffer[0] == '#') continue;
			        if (buffer[0] == '$') break;

					cr_weg (buffer);
					clipped (buffer);
			        anz = wsplit (buffer, " ");
			        if (anz < 2) continue;
                    strcpy (ItAfter[afteranz].name , wort[0]);
			        strcpy (ItAfter[afteranz].proc, wort[1]);
			        afteranz ++;
			 }
		     ItAfter [afteranz].name[0] = (char) 0;
		 }

		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$BEFORE", 7) == 0) 
			 {
				    break;
			 }
		 }

		 if (memcmp (buffer, "$BEFORE", 7) == 0) 
		 {
			 beforeanz = 0;
 		     while (fgets (buffer, 255, fp))
			 {
                    if (buffer[0] == '#') continue;
			        if (buffer[0] == '$') break;

					cr_weg (buffer);
					clipped (buffer);
			        anz = wsplit (buffer, " ");
			        if (anz < 2) continue;
                    strcpy (ItBefore[beforeanz].name , wort[0]);
			        strcpy (ItBefore[beforeanz].proc, wort[1]);
			        beforeanz ++;
			 }
		     ItBefore [beforeanz].name[0] = (char) 0;
		 }

		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$CHATTR", 7) == 0) 
			 {
				    break;
			 }
		 }

		 if (memcmp (buffer, "$CHATTR", 7) == 0) 
		 {
			 chaanz = 0;
 		     while (fgets (buffer, 255, fp))
			 {
                    if (buffer[0] == '#') continue;
			        if (buffer[0] == '$') break;

					cr_weg (buffer); 
			        anz = wsplit (buffer, " ");
			        if (anz < 1) continue;
                    ChAttr[chaanz].feldname = new char [strlen (wort[0]) + 2];
					if (ChAttr[chaanz].feldname == NULL) continue;
                    strcpy (ChAttr[chaanz].feldname , wort[0]);
					ChAttr[chaanz].UpdAttr = ChAttrS[0].UpdAttr; 
					ChAttr[chaanz].InsAttr = ChAttrS[0].InsAttr; 
			        chaanz ++;
			 }
		     ChAttr [chaanz].feldname = NULL;
		     ChAttr [chaanz].UpdAttr  = 0;
		     ChAttr [chaanz].InsAttr  = 0;
		 }

		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$CHOISE", 7) == 0) 
			 {
				    break;
			 }
		 }

		 choiseanz = 0;
		 if (memcmp (buffer, "$CHOISE", 7) == 0) 
		 {
 		     while (fgets (buffer, 255, fp))
			 {
                    if (buffer[0] == '#') continue;
			        if (buffer[0] == '$') break;

			        anz = wsplit (buffer, " ");
			        if (anz < 5) continue;
                    strcpy (ItChoise[choiseanz].name , wort[0]);
			        strcpy (ItChoise[choiseanz].proc,  wort[1]);
					ItChoise[choiseanz].rows    = atoi (wort[2]);
					ItChoise[choiseanz].columns = atoi (wort[3]);
					ItChoise[choiseanz].key     = atoi (wort[4]);
					if (anz < 6)
					{
						strcpy (ItChoise[choiseanz].ActBuffer, "ActBuffer");
					}
					else
					{
						strcpy (ItChoise[choiseanz].ActBuffer, wort[5]);
					}
					if (anz < 7)
					{
						strcpy (ItChoise[choiseanz].ActCaption, "ActCaption");
					}
					else
					{
						strcpy (ItChoise[choiseanz].ActCaption, wort[6]);
					}
					if (anz < 8)
					{
						strcpy (ItChoise[choiseanz].ActVline, "ActVline");
					}
					else
					{
						strcpy (ItChoise[choiseanz].ActVline, wort[7]);
					}
					if (anz < 9)
					{
						strcpy (ItChoise[choiseanz].vline, "vlines");
					}
					else
					{
						strcpy (ItChoise[choiseanz].vline, wort[8]);
					}
					if (anz < 10)
					{
						strcpy (ItChoise[choiseanz].hline, "hlines");
					}
					else
					{
						strcpy (ItChoise[choiseanz].hline, wort[9]);
					}
					if (anz < 11)
					{
						strcpy (ItChoise[choiseanz].ListMode, "ListMode");
					}
					else
					{
						strcpy (ItChoise[choiseanz].ListMode, wort[10]);
					}
			        choiseanz ++;
			 }
		     ItChoise [choiseanz].name[0] = (char) 0;
		 }
         if (choiseanz == 0)
         {
             ScrollMenue (bearbeiten, KEY11);
         }

		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$BACKGROUND", 11) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    SetBackground (wort[1]);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$LINES", 6) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    SetLine (wort[1]);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$FOCUS", 6) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    SetAktFocus (wort[1]);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$ROWHEIGHT", 10) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
					if (anz > 1)
					{
                            eListe.SetRowHeight (ratod (wort[1]));
					}
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$UBROWS", 7) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
					if (anz > 1)
					{
                            eListe.SetUbRows (atoi (wort[1]));
					}
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$UPDATE", 7) == 0) 
			 {
				    upd_mode = TRUE;
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$FONTNAME", 9) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    strcpy (FontName,wort[1]);
                    eListe.SetFont (&lFont);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$FONTHEIGHT", 11) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    lFont.FontHeight = atoi (wort[1]);
                    eListe.SetFont (&lFont);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$FONTWIDTH", 9) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    lFont.FontWidth = atoi (wort[1]);
                    eListe.SetFont (&lFont);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$FONTATTRIBUTE", 13) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    lFont.FontAttribute = atoi (wort[1]);
                    eListe.SetFont (&lFont);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$PRINTMODE", 10) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    PrintMode = atoi (wort[1]);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$PAGELEN", 8) == 0) 
			 {
				    anz = wsplit (buffer, " "); 
				    pagelen = atoi (wort[1]);
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$BEFOREPRINT", 12) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              SetBeforePrint (wort[1]);
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$PRINTFILE", 10) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              SetPrintFile (wort[1]);
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$DEVICE", 7) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              SetDeviceFile (wort[1]);
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$CAPTMODE", 9) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              CaptMode = atoi (wort[1]);
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$CAPTION", 8) == 0) 
			 {
                    BOOL FirstUbRow = TRUE;
                    PrintUb[0] = 0;
   		            while (fgets (buffer, 255, fp))
                    {
                        if (buffer[0] == '$') break;
                        cr_weg (buffer);
                        if (FirstUbRow == FALSE)
                        {
                            strcat (PrintUb, "\n");
                        }
                        else
                        {
                            FirstUbRow = FALSE;
                        }
                        strcat (PrintUb, buffer);
                    }
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$SCRPAGELEN", 11) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              eListe.SetScrPageLen (atoi (wort[1]));
                    }
				    break;
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$PRINTFONT", 10) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              PrintFont = atoi (wort[1]);
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$ROWPRINTFONT", 13) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              RowPrintFont = atoi (wort[1]);
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$USEPRINTFORM", 10) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              UsePrintForm = atoi (wort[1]);
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$QUERPRINT", 10) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              QuerPrint = atoi (wort[1]);
                              eListe.SetQuerPrint (QuerPrint);
                              if (QuerPrint)
                              {
                                    CheckMenuItem (hMenu, IDM_QUERPRINT, MF_CHECKED);
                              }
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$PRINTBACKGROUND", 16) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              eListe.SetPrintBackground (atoi (wort[1]));
                              break;
                    }
			 }
         }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
             if (memcmp (buffer, "$SCRPRINT", 9) == 0) 
             {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              ScrPrint = atoi (wort[1]);
                              if (ScrPrint != 0)
                              {
                                     ScrPrint = 1;
                                     CheckMenuItem (hMenu, IDM_PRINTMODE, MF_CHECKED);
                              }
                              else
                              {
                                     CheckMenuItem (hMenu, IDM_PRINTMODE, MF_UNCHECKED);
                              }
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
             if (memcmp (buffer, "$SCRPAGELEN", 11) == 0) 
             {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              eListe.SetScrPageLen (atoi (wort[1]));
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
             if (memcmp (buffer, "$PRINTROWCAPTION", 16) == 0) 
             {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              eListe.SetPrintRowCaptions (atoi (wort[1]));
                              if (atoi (wort[1]) == 0)
                              {
                                   CheckMenuItem (hMenu, IDM_ROWCAPTION, MF_UNCHECKED);
                              }
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$RIGHTS", 7) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz > 1)
                    {
                              Rights = atoi (wort[1]);
                              if (Rights == FALSE) DeniRights ();
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$FETT", 5) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (strcmp (wort[0],"$FETT") == 0 &&  anz > 1)
                    {
                              FETT = atoi (wort[1]);
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$SORT", 5) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (strcmp (wort[0],"$SORT") == 0 &&  anz > 1)
                    {
                              eListe.EnableSort (atoi (wort[1]));
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$SORTROW", 8) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (strcmp (wort[0],"$SORTROW") == 0 && anz > 1)
                    {
                              SortRow = atoi (wort[1]);
                              StartWithSort = TRUE;
                              break;
                    }
			 }
		 }
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$EXEC", 5) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    if (anz == 3 )
                    {
                              SetExec (wort[1], wort [2], 0);
                    }
                    else if (anz > 3 )
                    {
                              SetExec (wort[1], wort [2], atoi (wort [3]));
                    }
			 }
		 }
         MailParams[0] = 0;
		 fseek (fp, 0l, 0);
		 while (fgets (buffer, 255, fp))
		 {
			 cr_weg (buffer);
			 if (memcmp (buffer, "$MAILPARAMS", 11) == 0) 
			 {
                    anz = wsplit (buffer, " ");
                    for  (int i = 1; i < anz; i ++)
                    {
                              if (i > 1)
                              {
                                  strcat (MailParams, " ");
                              }
                              if (memcmp (wort[i], "/Name=", 6) == 0)
                              {
                                  strcat (MailParams, "Name=");
                                  strcat (MailParams,"\"");
                                  strcat (MailParams, &wort[i][6]);
                                  strcat (MailParams,"\"");
                              }
                              else if (memcmp (wort[i], "/Subject=", 9) == 0)
                              {
                                  strcat (MailParams, "Subject=");
                                  strcat (MailParams,"\"");
                                  strcat (MailParams, &wort[i][9]);
                                  strcat (MailParams,"\"");
                              }
                              else if (memcmp (wort[i], "/NoteText=", 10) == 0)
                              {
                                  strcat (MailParams, "NoteText=");
                                  strcat (MailParams,"\"");
                                  strcat (MailParams, &wort[i][10]);
                                  strcat (MailParams,"\"");
                              }
                              else
                              {
                                  strcat (MailParams, wort [i]);
                              }
                    }
			 }
		 }
         fclose (fp);
}


SEARCHTABLES Tables;
SEARCHCOLS Columns;
HWND SearchhWnd = NULL;

int SearchTableTable (void)
{
        struct STABLE *stable;

        Tables.SetParams (hMainInst, hMainWindow);
        Tables.Setawin (hMainWindow);
        Tables.Search ();
        SetCurrentFocus (currentfield);
        if (syskey == KEY5)
        {
            return 0;
        }
        stable = Tables.GetSTable ();

        if (stable == NULL)
        {
            return 0;
        }
//        strcpy (fname, stable->tabname);
        clipped (fname);
        if (strcmp (fname, " ") == 0)
        {
            fname[0] = 0;
        }
        if (strlen (fname) > 0)
        {
            strcat (fname, ",");
        }
        strcat (fname, stable->tabname);
        display_form (SearchhWnd, &ffname, 0, 0);
        return 0;
}


int SearchTableColumn (char *selfields)
{
        struct SCOLUMN *scolumn;

        clipped (fname);
        if (strcmp (fname, " ") < 0) return 0;

//        Columns.SetParams (hMainInst, hMainWindow);
        Columns.SetParams (hMainInst, eListe.Getmamain2 ());
        Columns.SetTable (fname);
        Columns.Setawin (hMainWindow);
        Columns.Search ();
        SetCurrentFocus (currentfield);
        EnableWindow (eListe.Getmamain2 (), TRUE);
        if (syskey == KEY5)
        {
            return 0;
        }
        scolumn = Columns.GetSColumn ();

        if (scolumn == NULL)
        {
            return 0;
        }

        clipped (selfields);
        if (strcmp (selfields, " ") == 0)
        {
            selfields[0] = 0;
        }
        if (strlen (selfields) > 0)
        {
            strcat (selfields, ",");
        }
        strcat (selfields, scolumn->colname);
        display_form (SearchhWnd, &ffname, 0, 0);
        return 0;
}

int SearchWhereColumn (char *selfields)
{
        struct SCOLUMN *scolumn;

        clipped (fname);
        if (strcmp (fname, " ") < 0) return 0;

        Columns.SetParams (hMainInst, hMainWindow);
        Columns.SetTable (fname);
        Columns.Setawin (hMainWindow);
        Columns.Search ();
        SetCurrentFocus (currentfield);
        if (syskey == KEY5)
        {
            return 0;
        }
        scolumn = Columns.GetSColumn ();

        if (scolumn == NULL)
        {
            return 0;
        }

        clipped (selfields);
        if (strcmp (selfields, " ") == 0)
        {
            selfields[0] = 0;
        }
        if (strlen (selfields) > 0)
        {
            strcat (selfields, " ");
        }
        char *d = strchr (scolumn->colname, '$');
        if (d != NULL) *d = '.';
        strcat (selfields, scolumn->colname);
        display_form (SearchhWnd, &ffname, 0, 0);
        return 0;
}

int SearchTable (void)
{
#ifdef BIWAK
         return 0;
#endif

        if (currentfield == 0)
        {
            return SearchTableTable ();
        }
        else if (currentfield == 1)
        {
            return SearchWhereColumn (and_part);
        }
        else if (currentfield == 2)
        {
            return SearchTableColumn (selfields);
        }
        SetCurrentFocus (currentfield);
        return 0;
}

void EnterTable (void)
{
        HWND hWnd; 
		RECT rect;
		HFONT hFont;
		HDC hdc;
		TEXTMETRIC tm;
        SIZE size;
		int x,y, cx, cy;
        char caption [1024];


     	if (QueryEnter)
		{
		      ffname.mask[0].attribut = READONLY;
		}
		else
		{
		      ffname.mask[0].attribut = EDIT | SCROLL;
		}
		GetWindowRect (hMainWindow, &rect);

		stdfont ();
		hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);
		cx = 80 * tm.tmAveCharWidth;
		cy = (int) (double) (11 * tm.tmHeight * 1.5);

        x = (rect.right  - rect.left - cx) / 2 + rect.left; 
        y = (rect.bottom - rect.top  - cy) / 2 + rect.top;
		y = (int) (double) (y / 1.5);
		
		x /= tm.tmAveCharWidth;
		y /= tm.tmHeight;

         
        DisablehWnd (hMainWindow);
        SetBorder (WS_OVERLAPPED | WS_SYSMENU);
        hWnd = OpenWindowCh (11, 80, y, x, hMainInst);
        SearchhWnd = hWnd;
        break_end ();
		set_fkt (StopEnterFile, 12);
		set_fkt (SearchTable, 9);
        enter_form (hWnd, &ffname, 0, 0);
		set_fkt (NULL, 12);
		if (ffname.mask[0].feldid)
		{
               CloseControls (&ffname);
               DestroyWindow (hWnd);
		}
		if (syskey == KEY5 || syskey == KEYESC)

		{
               UpdateWindow (hMainWindow);
			   return;
        }
		if (strcmp (fname, "  ") < 0)
		{
			   syskey = KEYESC;
               UpdateWindow (hMainWindow);
			   return;
        }
        if (MacroCaption == FALSE)
        {
               sprintf (caption, "Database Info %s %s",
                           fname, and_part);
               SetWindowText (hMainWindow, caption);
        }
        eListe.InitRecanz ();
        InvalidateRect (eListe.Getmamain2 (), 0, TRUE);
        UpdateWindow (hMainWindow);
}


void FreeSwSaetze (void)
/**
SwSaetz freigeben.
**/
{
        int i;

        for ( i = 0; i < eListe.GetRecanz (); i ++)
        {
            if (SwSaetze[i])
            {
                GlobalFree (SwSaetze[i]);
                SwSaetze[i] = NULL;
            }
        }
        eListe.FreeBezTab ();
}


void FreeForms (void)
/**
Item fuer UbForm freigeben.
**/
{

        ziel = LstZiel;
        ausgabesatz = LstSatz;
        eListe.FreeUbForm ();
        eListe.FreeDataForm ();
        eListe.FreeLineForm ();
        if (eingabesatz) 
        {
               free (eingabesatz); 
               eingabesatz = NULL;
        }

        if (ausgabesatz) 
        {
               free (ausgabesatz); 
               ausgabesatz = NULL;
			   LstSatz = NULL;
        }
        if (quelle) 
        {
            free (quelle);
            quelle = NULL;
        }
        if (ziel) 
        {
            free (ziel);
            ziel = NULL;
			LstZiel = NULL;
        }

        FreeSwSaetze ();
}


int FromMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

//       memcpy (ausgabesatz, SwSaetze[pos], zlen);
       memcpy (ausgabesatz, eListe.GetSwSatz(pos), zlen);
       return 0;
}


int ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{
       if (constat > 0) return 0;
       SwSaetze [pos] = (char *) GlobalAlloc (0, zlen + 2);
       if (SwSaetze[pos] == NULL)
       {
           disp_mess ("Kein Speicherplatz", 2);
           close_cursor ();
		   FreeTable ();
           Closedbase (DataBase); 
           ExitProcess (1);
       }

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       MarkedRows[pos] = 0;
       eListe.SetRecanz (pos + 1);
       return 0;
}

int ToMemoryE (FILE *fp)
/**
SW-Satz in Ascii-Format Speichern.
**/
{
       int pos;

       pos = eListe.GetRecanz ();
       SwSaetze [pos] = (char *) GlobalAlloc (0, zlen + 2);
       if (SwSaetze[pos] == NULL)
       {
           disp_mess ("Kein Speicherplatz", 2);
           close_cursor ();
		   FreeTable ();
           Closedbase (DataBase); 
           ExitProcess (1);
       }

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       eListe.SetRecanz (pos + 1);
       return 0;
}

int ToMemoryD (char *satz, int len, FILE *fp)
/**
SW-Satz in Ascii-Format Speichern.
**/
{
       int pos;
       int slen;

       clipped (satz);
       slen = strlen (satz);
       if (slen < zlen)
       {
           slen = zlen;
       }

       pos = eListe.GetRecanz ();
       SwSaetze [pos] = (char *) GlobalAlloc (0, slen + 10);
       if (SwSaetze[pos] == NULL)
       {
           disp_mess ("Kein Speicherplatz", 2);
           close_cursor ();
		   FreeTable ();
           Closedbase (DataBase); 
           ExitProcess (1);
       }

       memcpy (SwSaetze[pos], "$DISP", 5);
       strcpy (&SwSaetze[pos][5], satz);
       eListe.SetPosPrint (TRUE);
       eListe.SetRecanz (pos + 1);
       return 0;
}

void SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{
       char *adr;

       adr = feldadr (feldname);
       if (adr == NULL) return;
       adr [len - 1] = (char) 0;
}

void SetCol (char *feldname, int len)
/**
Stringende Setzen.
**/
{
       char *adr;

       adr = feldadr (feldname);
       if (adr == NULL) return;
       memset (adr, ' ', len - 1);
       adr [len - 1] = (char) 0;
}

void uebertragen (void)
/**
Eingabesatz in ASCII-Striktur uebertragen.
**/
{
       int i;

       for (i = 0; ziel[i].feldname[0]; i ++)
       {
             if (fadresse (&insatz, ziel[i].feldname) == NULL)
             {
                  SetCol (ziel[i].feldname, ziel[i].feldlen );
                  continue;   
             }
                  
             uebertrage (&outsatz,
                  ziel[i].feldname,
                  &insatz,
                  ziel[i].feldname);
             SetStringEnd (ziel[i].feldname, ziel[i].feldlen);   
       }
}


void BeforeRow (char *fname)
/**
Procedure, die beim Verlassen einer Feldes aufgerufen wird.
**/
{
	    int i;
		int pos;

		char buffer [256];

        toprint = 0;
		for (i = 0; ItBefore[i].name[0]; i ++)
		{
			if (i == beforeanz) return;
			if (strcmp (fname, ItBefore[i].name) == 0) break;
		}
		if (ItBefore[i].name[0] == NULL) return;
		sprintf (buffer, "call %s", ItBefore[i].proc);
		eListe.SwitchRowMode (0);
		pos = eListe.GetAktRow ();
        memcpy (ausgabesatz, SwSaetze[pos], zlen);
		do_call (buffer);
        memcpy (SwSaetze[pos], ausgabesatz, zlen);
		eListe.SwitchRowMode (1);

		if (toprint)
		{
			eListe.ShowAktRow ();
            toprint = 0;
		}

}

void AfterRow (char *fname)
/**
Procedure, die beim Verlassen einer Feldes aufgerufen wird.
**/
{
	    int i;
		int pos;

		char buffer [256];

        toprint = 0;
		for (i = 0; ItAfter[i].name[0]; i ++)
		{
			if (i == afteranz) return;
			if (strcmp (fname, ItAfter[i].name) == 0) break;
		}
		if (ItAfter[i].name[0] == NULL) return;
		sprintf (buffer, "call %s", ItAfter[i].proc);
		eListe.SwitchRowMode (0);
		pos = eListe.GetAktRow ();
        memcpy (ausgabesatz, SwSaetze[pos], zlen);
		do_call (buffer);
        memcpy (SwSaetze[pos], ausgabesatz, zlen);
		eListe.SwitchRowMode (1);
		if (toprint)
		{
			eListe.ShowAktRow ();
            toprint = 0;
		}
}

void OpenChoiseWindow0 (int ItPos)
{
	  int cx, cy;
	  char *adr;
	  char buffer [512];
	  int hline;

	  cx = ItChoise[ItPos].columns;
	  cy = ItChoise[ItPos].rows;
	  if (Choise1 == NULL)
	  {
		  Choise1 = new CHOISE (cx, cy);
	  }
      Settchar ('|');
      Choise1->OpenWindow (hMainInst, eListe.Getmamain3());

	  hline = 0;
      sprintf (buffer, "data.%s", ItChoise[ItPos].hline); 
      adr = feldadr (buffer);
	  if (adr)
	  {
		  hline = (short) *adr;
	  }
      sprintf (buffer, "data.%s", ItChoise[ItPos].ActVline); 
      adr = feldadr (buffer);

	  if (adr)
	  {
		  Choise1->VLines (adr, hline);
	  }
	  else if (hline)
	  {
		  Choise1->VLines ("", hline);
	  }

      sprintf (buffer, "data.%s", ItChoise[ItPos].ActCaption); 
      adr = feldadr (buffer);
	  Choise1->InsertCaption (adr);
	  while (TRUE)
	  {
  		  sprintf (buffer, "call %s", ItChoise[ItPos].proc);
		  do_call (buffer);
          sprintf (buffer, "data.%s", ItChoise[ItPos].ActBuffer); 
          adr = feldadr (buffer);
		  if (strupcmp (adr, "$", 1) == 0) break;
	      Choise1->InsertRecord (adr);
	  }

	  Choise1->ProcessMessages ();
	  if (syskey != KEY5)
	  {
	            Choise1->GetText (AktMenuTxt);
	  }
      Choise1->DestroyWindow ();
	  delete Choise1;
	  Choise1 = NULL;
      return;

}

void OpenChoiseWindow (int ItPos)
{
	  RECT rectc, rectw; 
	  int cx, cy, x, y;
	  HDC hdc;
	  HFONT hFont, oldFont;
	  TEXTMETRIC tm;
	  int taste;
	  char *adr;
	  char buffer [512];

	  GetClientRect (eListe.Getmamain3 (), &rectc);
	  GetWindowRect (eListe.Getmamain3 (), &rectw);
	  cx = ItChoise[ItPos].columns;
	  cy = ItChoise[ItPos].rows;

	  hdc = GetDC (eListe.Getmamain3 ());
      spezfont (&lFont);
      hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
      oldFont = SelectObject (hdc,hFont);
      GetTextMetrics (hdc, &tm);
      ReleaseDC (eListe.Getmamain3 (), hdc);
	  cx *= tm.tmAveCharWidth;
//	  cy = (int) (double) ((double) cy * tm.tmHeight * 1.3);
	  cy *= tm.tmHeight;
	  x = max (0, (rectc.right - cx) / 2); 
	  y = max (0, (rectc.bottom - cy) / 2);
	  
	  x /= tm.tmAveCharWidth;
	  y /= tm.tmHeight;
//      y = (int) (double) ((double) y / tm.tmHeight / 1.3);
	  cx = ItChoise[ItPos].columns;
	  cy = ItChoise[ItPos].rows;
      OpenListWindowBu (eListe.Getmamain3 (), cy, cx, y, x, 1);

      sprintf (buffer, "data.%s", ItChoise[ItPos].ActCaption); 
      adr = feldadr (buffer);
	  InsertListUb (adr);
	  SetVLines (0);
	  SetHLines (0);
      sprintf (buffer, "data.vlines"); 
      adr = feldadr (buffer);
	  if (adr)
	  {
		  SetVLines ((short) *adr);
	  }
      sprintf (buffer, "data.hlines"); 
      adr = feldadr (buffer);
	  if (adr)
	  {
		  SetHLines ((short) *adr);
	  }
	  while (TRUE)
	  {
  		  sprintf (buffer, "call %s", ItChoise[ItPos].proc);
		  do_call (buffer);
          sprintf (buffer, "data.%s", ItChoise[ItPos].ActBuffer); 
          adr = feldadr (buffer);
		  if (strupcmp (adr, "$", 1) == 0) break;
	      InsertListRow (adr);
	  }
      taste = EnterQueryListBox ();
}

void TestKey9 (char *fname)
/**
Testen, ob Auswahl auf aktuellem Feld aktiv ist.
 **/
{
	    int i;

		for (i = 0; ItChoise[i].name[0]; i ++)
		{
			if (i == choiseanz) return;
			if (strcmp (fname, ItChoise[i].name) == 0) break;
		}
		if (ItChoise[i].name[0] == NULL)
		{
             EnableMenuItem (hMenu, KEY9, MF_GRAYED);
		}
		else
		{
             EnableMenuItem (hMenu, KEY9, MF_ENABLED);
		}
}

void PrintScrCaption (char *buffer)
{
        strcpy (buffer, "");
}

void dokey10 (char *fname)
/**
Procedure, die beim druecken von F10 aufgerufen wird.
**/
{
	    int i;
		int pos;
		int ListMode;
		char *adr;
		int anz;

		char buffer [256];

		for (i = 0; ItChoise[i].name[0]; i ++)
		{
			if (i == choiseanz) return;
			if (strcmp (fname, ItChoise[i].name) == 0) break;
		}
		if (ItChoise[i].name[0] == NULL) return;
		ListMode = 0;
        sprintf (buffer, "data.%s", ItChoise[i].ListMode); 
        adr = feldadr (buffer);
        if (adr)
		{
			ListMode = (short) *adr;
		}
		if (ListMode)
		{
		    OpenChoiseWindow (i);
		}
		else
		{
		    OpenChoiseWindow0 (i);
		}
		if (syskey == KEY5) 
		{
            eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                  eListe.GetAktColumn ());
			return;
		}

		anz = wsplit (AktMenuTxt, "|");
		if (anz <= 0)
		{
            eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                  eListe.GetAktColumn ());
			return;
		}
 	    strcpy (buffer, wort[0]);
        eListe.SwitchRowMode (0);
	    pos = eListe.GetAktRow ();
		mov (fname, "%s", buffer);
        memcpy (SwSaetze[pos],ausgabesatz,  zlen);
	    eListe.SwitchRowMode (1);
	    eListe.ShowAktRow ();
		eListe.SwitchPage (pos);
		eListe.SwitchPage (pos);
        eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                              eListe.GetAktColumn ());
        AfterRow (fname);
}


void ShowDB (void)
/**
Daten aus SW-Datei lesen und anzeigen.
**/
{
	    MSG msg;
        int i, j;
        HCURSOR oldcursor;
		char *adr;
        extern short do_exit;

        eListe.SetPosPrint (FALSE);
        if (Rights == FALSE) DeniRights ();

        if (where_upd)
        {
                setwhere_part_upd (where_upd);
                where_upd = NULL;
        }
        set_wrziel (ToMemoryE, NULL);
        set_wrsziel (ToMemoryD, NULL);
        eListe.SetRecanz (0);
		eListe.SetBefore (BeforeRow);
		eListe.SetAfter  (AfterRow);
		eListe.SetChoise (dokey10);
		eListe.SetSetKey9 (TestKey9);
        eListe.SetChAttr (ChAttr); 
        eListe.SetPrintScrCaption (PrintScrCaption);
        dbcursor = select_cursor ();
		if (dbcursor < 0)
		{
			print_mess (2, "SQL-Fehler %d", dbcursor);
			return;
		}

		updcursor = update_cursor ();

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
//		CreateMessage (eListe.Getmamain2 (), "Die Daten werden selektiert");
	    Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
			                                      "Die Daten werden selektiert.\n ");  
        ListOK = 0;
        if (ListFocus != -1)
        {
                eListe.SetListFocus (ListFocus);
                ListFocus = -1;
        }
                   
		SetItAttr ();
		if (upd_mode)
		{
                  set_fkt (DeleteLine, 7);
		          set_fkt (WriteList, 12);
                  EnableMenuItem (hMenu, KEY12,  MF_ENABLED);
		}
		else
		{
		          set_fkt (NULL, 7);
		          set_fkt (NULL, 12);
                  EnableMenuItem (hMenu, KEY12,  MF_GRAYED);
		}
		perform_begin ();
        if (do_exit == 0)
        {
             while (fetchsql (dbcursor) == 0)
             {
                     uebertragen ();
 			         perform_code ();
                     if (do_exit) break;
                     i = eListe.GetRecanz ();
                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXRECS) break; 

                     if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
					 {
                                TranslateMessage(&msg);
                                DispatchMessage(&msg);
					 }

			 }
        }
//		DestroyMessage ();
		Wmess.Destroy ();
 
        i = eListe.GetRecanz ();
		if (EditMode && i == 0)
		{
             SwSaetze [0] = (char *) GlobalAlloc (0, zlen + 2);
             for (j = 0; ziel[j].feldname[0]; j ++)
			 {
			         adr = feldadr (ziel[j].feldname);
			         if (adr == NULL) continue;
			         memset (adr, ' ', ziel[j].feldlen);
                     adr [ziel[j].feldlen - 1] = (char) 0;
			 }
             memcpy (SwSaetze[0], ausgabesatz, zlen);
             eListe.SetRecanz (1);
			 i = 1;
		}
//        SetCursor (oldcursor);

        if (i)
        {
                if (AddMenue)
                {
	                   EnableMenuItem (hMenu, IDM_PAGEVIEW, MF_ENABLED);
                       EnableMenuItem (hMenu, IDM_SEND_MAIL,  MF_ENABLED);
                       EnableMenuItem (hMenu, IDM_PRINT,      MF_ENABLED);
                }

                if (AddToolbar)
                {
                       ToolBar_SetState(hwndTB,IDM_PAGE, TBSTATE_ENABLED);
                       ToolBar_SetState(hwndTB,IDM_PRINT,     TBSTATE_ENABLED);
                       ToolBar_SetState(hwndTB,IDM_PRINT,     TBSTATE_ENABLED);
                       ToolBar_SetState(hwndTB,IDM_SEND_MAIL, TBSTATE_ENABLED);
                }
        }

        else
        {
                if (AddMenue)
                {
	                   EnableMenuItem (hMenu, IDM_PAGEVIEW, MF_GRAYED);
                       EnableMenuItem (hMenu, IDM_PRINT,   MF_GRAYED);
                       EnableMenuItem (hMenu, IDM_SEND_MAIL,   MF_GRAYED);
                }

                if (AddToolbar)
                {
                       ToolBar_SetState(hwndTB,IDM_PAGE, TBSTATE_INDETERMINATE);
                       ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_INDETERMINATE);
                       ToolBar_SetState(hwndTB,IDM_SEND_MAIL, TBSTATE_INDETERMINATE);
                }
        }


        eListe.SetRecanz (i);
//        eListe.MakeDataForm0 ();
        perform_ende ();
        StopDebug ();
        SwRecs = eListe.GetRecanz ();
        eListe.MakeUbForm ();
        eListe.MakeDataForm0 ();

        eListe.SetSaetze (SwSaetze);
        eListe.SetMarkedRows (MarkedRows);

/*
        if (EditMode == 0 && i < 2)
        {
               PageView = 0;
               SendMessage (hMainWindow, WM_COMMAND, IDM_PAGEVIEW, 0l);
        }
*/

        ListOK = 1;

        if (StartWithSort && i > 0)
        {
            eListe.SortCol (SortRow);
        }
        SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
        InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
}


int WriteList (void)
{
	    int i;
	    if (abfragejn (eListe.Getmamain3 () , 
			           "�nderungen speichern", "N"))
		{
           if (PageView)
		   {
	            CheckMenuItem (hMenu, IDM_PAGEVIEW,
	 			                      MF_UNCHECKED); 
                ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                ToolBar_SetState(hwndTB, IDM_PAGE, TBSTATE_ENABLED);
                PageView = 0;
                eListe.SwitchPage (eListe.GetAktRowS ());
		   }

		   Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
			                                      "Die Daten werden gespeichert.\n ");  
           SwRecs = eListe.GetRecanz ();
		   for (i = 0; i < SwRecs; i ++)
		   {
                   FromMemory (i);
				   if (to_db () == -1) break;;
		   }
		   Wmess.Destroy ();
		   eListe.BreakList ((WPARAM) 0);
		}
		if (syskey == KEY5)
		{
		   eListe.BreakList ((WPARAM) 0);
		}
		return 0;
}

int DeleteList (int i)
{
		{
           if (PageView)
		   {
/*
	            CheckMenuItem (hMenu, IDM_PAGEVIEW,
	 			                      MF_UNCHECKED); 
                ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                ToolBar_SetState(hwndTB, IDM_PAGE, TBSTATE_ENABLED);
                PageView = 0;
                eListe.SwitchPage (eListe.GetAktRowS ());
*/
                return 0;
		   }
           FromMemory (i);
		   delete_db ();
        }
		return 0;
}

int SwitchFocus (void)
/**
**/
{

	    CheckMenuItem (hMenu, ActiveMark,
					           MF_UNCHECKED); 
		if (ActiveMark == IDM_FRAME)
		{
			ActiveMark = ActiveEdit;
		}
		else
		{
			ActiveMark = IDM_FRAME;
		}
	    CheckMenuItem (hMenu, ActiveMark,  MF_CHECKED); 
		return 0;
}


void ReadDB (void)
/**
SW-Datei lesen.
**/
{
	    int ret;

        PageView = 0;
        eListe.SetPageView (0);
        if (AddMenue)
        {
               CheckMenuItem (hMenu, IDM_PAGEVIEW, MF_UNCHECKED); 
        }

        if (AddToolbar)
        {
               ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
               ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
        }

		setfields (NULL);
		if (selfields[0] && strcmp (selfields, "*"))
		{
			setfields (selfields);
		}

		info_name = (char *) fname;
        if (strstr (info_name, ".inf"))
        {
            ListClass::ReadItemLabel = TRUE;
            CheckMenuItem (hMenu, IDM_ITEMLABEL, MF_CHECKED);
        }
        ret = belege_ziel (info_name);
        belege_db (info_name);
        LstZiel = ziel;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;
        eListe.SetLstZiel (ziel);
        Lstbanz = feld_anz;
		CloseInf ();

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Initscrollpos ();
		if (upd_mode)
		{
		          set_fkt (WriteList, 12);
                  EnableMenuItem (hMenu, KEY12,  MF_ENABLED);
		}
        set_fkt (SwitchFocus, 6);
        ShowDB ();
}


void NewTable (BOOL Read)
/**
Neue Datei bearbeiten.
**/
{

        eListe.SetRowMode (0);
        EnableMenuItem (hMenu, IDM_PAGEVIEW, MF_GRAYED);
        ToolBar_SetState(hwndTB,IDM_PAGE, TBSTATE_INDETERMINATE);
        AktRow = 0;
        AktColumn = 0;
        scrollpos = 0;
        ziel = LstZiel;
        ausgabesatz = LstSatz;
        SetAktivWindow (hMainWindow);
        EnterTable ();
        SetAktivWindow (eListe.Getmamain3 ());
        if (Read == FALSE && (syskey == KEY5 || syskey == KEYESC))
        {
                     return;
        }


        close_cursor ();
        eListe.FreeUbForm ();
        eListe.FreeDataForm ();
        eListe.FreeLineForm ();
        FreeExecs ();

        if (eingabesatz) 
        {
               free (eingabesatz); 
               eingabesatz = NULL;
        }

        if (ausgabesatz) 
        {
               free (ausgabesatz); 
               ausgabesatz = NULL;
               LstSatz = NULL;
        }
        if (quelle) 
        {
            free (quelle);
            quelle = NULL;
        }
        if (ziel) 
        {
            free (ziel);
            ziel = NULL;
			LstZiel = NULL;
        }

        eListe.DestroyTrack ();
        eListe.DestroyVTrack ();
		eListe.DestroyFocusWindow ();
		if (strchr (and_part, '$') != NULL ||
			strchr (and_part, '#') != NULL ||
            strchr (and_part, '%') != NULL)
		{
			if (EnterAndPart () == FALSE)
			{
			      return;
			}
		}
		else
		{
	        setand_part (and_part);
		}
        if (MacroCaption == FALSE && EnteredAndPart != NULL)
        {
            strcpy (InfoCaption, EnteredAndPart);
            SetWindowText (hMainWindow, InfoCaption);
        }
        SetCursor (LoadCursor (NULL, IDC_WAIT));
        ReadDB ();
}

void ReRead ()
/**
Neue lesen.
**/
{

        eListe.SetRowMode (0);
        EnableMenuItem (hMenu, IDM_PAGEVIEW, MF_GRAYED);
        ToolBar_SetState(hwndTB,IDM_PAGE, TBSTATE_INDETERMINATE);
        AktRow = 0;
        AktColumn = 0;
        scrollpos = 0;
        ziel = LstZiel;
        ausgabesatz = LstSatz;
        SetAktivWindow (hMainWindow);
        SetAktivWindow (eListe.Getmamain3 ());

        close_cursor ();
        eListe.FreeUbForm ();
        eListe.FreeDataForm ();
        eListe.FreeLineForm ();
        FreeExecs ();

        if (eingabesatz) 
        {
               free (eingabesatz); 
               eingabesatz = NULL;
        }

        if (ausgabesatz) 
        {
               free (ausgabesatz); 
               ausgabesatz = NULL;
               LstSatz = NULL;
        }
        if (quelle) 
        {
            free (quelle);
            quelle = NULL;
        }
        if (ziel) 
        {
            free (ziel);
            ziel = NULL;
			LstZiel = NULL;
        }

        eListe.DestroyTrack ();
        eListe.DestroyVTrack ();
		eListe.DestroyFocusWindow ();
		if (strchr (and_part, '$') != NULL ||
			strchr (and_part, '#') != NULL ||
            strchr (and_part, '%') != NULL)
		{
			if (EnterAndPart () == FALSE)
			{
			      return;
			}
		}
		else
		{
	        setand_part (and_part);
		}
        if (MacroCaption == FALSE && EnteredAndPart != NULL)
        {
            strcpy (InfoCaption, EnteredAndPart);
            SetWindowText (hMainWindow, InfoCaption);
        }
        SetCursor (LoadCursor (NULL, IDC_WAIT));
        ReadDB ();
}


void NewFile ()
/**
Neue Datei bearbeiten.
**/
{
     
        EnteredAndPart = NULL;
        eListe.SetRowMode (0);
        EnableMenuItem (hMenu, IDM_PAGEVIEW, MF_GRAYED);
        ToolBar_SetState(hwndTB,IDM_PAGE, TBSTATE_INDETERMINATE);
        AktRow = 0;
        AktColumn = 0;
        scrollpos = 0;
        ziel = LstZiel;
        ausgabesatz = LstSatz;
        SetAktivWindow (eListe.Getmamain3 ());
        EnterFileName ();
		and_part[0] = (char) 0;
	    setand_part (NULL);
        SetAktivWindow (eListe.Getmamain3 ());
        if (syskey == KEY5 || syskey == KEYESC)
        {
                     return;
        }


        close_cursor ();
        eListe.FreeUbForm ();
        eListe.FreeDataForm ();
        eListe.FreeLineForm ();
        FreeExecs ();

        if (eingabesatz) 
        {
               free (eingabesatz); 
               eingabesatz = NULL;
        }

        if (ausgabesatz) 
        {
               free (ausgabesatz); 
               ausgabesatz = NULL;
        }
        if (quelle) 
        {
            free (quelle);
            quelle = NULL;
        }
        if (ziel) 
        {
            free (ziel);
            ziel = NULL;
        }

        eListe.DestroyTrack ();
        eListe.DestroyVTrack ();
		eListe.DestroyFocusWindow ();
        SetCursor (LoadCursor (NULL, IDC_WAIT));
        ReadDB ();
}

void GetTabLines (char *l)
/**
**/
{
        int i;
 
        for (i = 0; lineflag[i]; i ++) 
        {
            if (strcmp (l, lineflag[i]) == 0)
            {
                eListe.SetListLines (i);
                break;
            }
        }
}

void GetBackGround (char *bc)
/**
**/
{
        int i;
 
        for (i = 0; bcolflag[i]; i ++) 
        {
            if (strcmp (bc, bcolflag [i]) == 0)
            {
                eListe.SetColors (Colors[i], BkColors[i]);
                break;
            }
        }
}


void SetListFocus (char *f)
/**
**/
{
    static char *ftab[] = {"NO",
                           "FRAME",
                           "REVERSE",
                           "EDIT",
                           NULL};
        int i;
 
        for (i = 0; ftab[i]; i ++) 
        {
            if (strcmp (f, ftab[i]) == 0)
            {
                eListe.SetListFocus (i);
                break;
            }
        }
}


void hilfe (void)
{
	     char buffer [80];

         SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_VISIBLE);
         OpenSListWindow (10, 61, 10, 30, FALSE);
         InsertListUb ("Hilfe");
         sprintf (buffer, "dbinfot -a -wUpdate -uCaption -bBackground -fFocus -ne Inf-File");
         InsertListRow (buffer);
         sprintf (buffer, "        -u �berschrift");
         SetMenStart (1);
         InsertListRow (buffer);
         sprintf (buffer, "        -b Hintergrundfarbe. ");
         InsertListRow (buffer);
         sprintf (buffer, "        -f Focus. ");
         InsertListRow (buffer);
         sprintf (buffer, "             0 kein Focus. ");
         InsertListRow (buffer);
         sprintf (buffer, "             2 Rahmen. ");
         InsertListRow (buffer);
         sprintf (buffer, "             3 Edit-Focus. ");
         InsertListRow (buffer);
         sprintf (buffer, "             3 Edit-Focus. ");
         InsertListRow (buffer);
         sprintf (buffer, "             4 Edit-Focus mit Rahmen. ");
         InsertListRow (buffer);
         sprintf (buffer, "        -n Zeilennummern unterdr�cken. ");
         InsertListRow (buffer);
         sprintf (buffer, "        -e Einf�ge-modus. ");
         InsertListRow (buffer);
         sprintf (buffer, "        -A Fenstergr��e und Pos. aus etc\\dbinfo.rct holen und schreiben. ");
         InsertListRow (buffer);
         EnterQueryListBox ();
		 return;
}


void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                            case 'h' :
                                     hilfe ();
                                     ExitProcess (0);
                            case 'A' :
                                     positions_mode = TRUE;
									 GetRect (); 

                                     return;
                            case 'E' :
                                     ende_mode = TRUE;
									 GetRect (); 

                                     return;
                            case 'w' :
                                     upd_mode = TRUE;
                                     where_upd  = arg + 1;
                                     return;
                            case 'u' :
                                     if (memcmp (arg, "user=", 5) == 0)
                                     {
                                         User = arg + 5;
                                         return;
                                     }
                                     strcpy (InfoCaption, arg + 1);
                                     return;
                            case 'b' :
                                     GetBackGround (arg + 1);
                                     return;
                            case 'f' :
                                     ListFocus = atoi (arg + 1);
                                     return;
                            case 'n' :
                                     eListe.SetNoRecNr ();
                                     break;
                            case 'p' :
                                     if (memcmp (arg, "password=", 9) == 0)
                                     {
                                         Passw = arg + 9;
                                         return;
                                     }
								     if (memcmp (arg, "pagelen", 7) == 0)
									 {
										 pagelen = atoi (arg + 7);
										 return;
									 }
                                     PrintMode = atoi (arg + 1);;
                                     break;
                            case 'e' :
                                     EditMode = TRUE;
                                     break;
                            case 'r' :
                                     Rights = FALSE;  
                                     break;
                            case 'd' :
                                     if (memcmp (arg, "dbserver=", 9) == 0)
                                     {
                                         DbServer = arg + 9;
                                         return;
                                     }
   									 DataBase = (arg + 1); 
                                     return;
                            case 'a' :
                                     if (memcmp (arg, "aufruf=", 7) == 0)
                                     {
                                         aufruf = atoi (arg + 7);
                                         return;
                                     }
   									 DataBase = (arg + 1); 
                                     return;
                            case 'l' :
   									 SetLine (arg + 1); 
                                     return;
                 }
          }
          return;
}


BOOL IsComboMsg (MSG *msg)
/**
Test, ob die Meldung fuer die Combobox ist.
**/
{
      if (HIWORD (msg->lParam) == CBN_DROPDOWN)
      {
                 opencombobox = TRUE;
                 return TRUE;
      }
      if (HIWORD (msg->lParam) == CBN_CLOSEUP)
      {
                 opencombobox = FALSE;
                 return TRUE;
      }

      if (msg->hwnd == hwndCombo1)
      {
                 return TRUE;
      }
      if (msg->hwnd == hwndTB)
      {
                 return TRUE;
      }

      return FALSE;
}


void FillCombo1 (void)
{
      int i;

      for (i = 0; Combo1[i]; i ++)
      {
           SendMessage (hwndCombo1, CB_ADDSTRING, 0,
                                   (LPARAM) Combo1 [i]);
      }
      SendMessage (hwndCombo1, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo1 [3]);
//      eListe.SetLines (3);
      SetComboMess (IsComboMsg);
}

void FillCombo2 (void)
{
      int i;

      for (i = 0; Combo2[i]; i ++)
      {
           SendMessage (hwndCombo2, CB_ADDSTRING, 0,
                                   (LPARAM) Combo2 [i]);
      }
      SendMessage (hwndCombo2, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo2 [0]);
      SetComboMess (IsComboMsg);
}

int DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        if (eListe.GetRecanz () == 0)  return 0;

        if (updcursor != -1)
        {
            DeleteList (eListe.GetAktRow ());
        }
        eListe.DeleteLine ();
        return 0;
}


int AppendLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.AppendLine ();
        return 0;
}

int CopyLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        eListe.CopyLine ();
        return 0;
}

void ScrollMenue (PMENUE *menue, int ID)
{
    int i;

    for (i = 0; menue[i].text; i ++)
    {
        if (menue[i].menid == ID) break;
    }

    for (;menue[i].text; i ++)
    {
        memcpy (&menue[i], &menue[i + 1], sizeof (PMENUE));
    }
}


int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
       int anz; 
       int i;
       char args[20][512];
       char argtab[20][512];
       char *varargs[20];
	   char *mv;
#ifndef BIWAK
#ifndef ODBC
       int dsqlstatus;
       char buffer [512];
#endif
#endif

       LoadLibrary ("RICHED32.DLL");
       anz = wsplit (GetCommandLine (), " ");
       if (anz > 0)
       {
           strcpy (progname, wort[0]);
       }
//	   opendb ("bws");

       new_procedure ();
       SetRunDde (RunDde);
       anz = wsplit (lpszCmdLine, " ");
       for (i = 0; i < anz; i ++)
       {
            strcpy (argtab[i], wort[i]);
            varargs[i] = (char *) argtab[i];
       }
       argtst (&anz, varargs, tst_arg);
#ifndef BIWAK
#ifndef ODBC
       if (DbServer != NULL)
       {
             char *p = getenv ("INFORMIXSERVER");
             if (p != NULL)
             {
//                      print_mess (2, "INFORMIXSERVER = %s", p);
             }

             if (p == NULL || strcmp (p, DbServer) != 0)
             {
                      if (aufruf != 0)
                      {
                          print_mess (2, "Setzen von INFORMIXSERVER=%s misslungen", DbServer);
                          ExitProcess (1);
                      }
                      sprintf (buffer, "INFORMIXSERVER=%s", DbServer);
                      _putenv (buffer);
                      sprintf (buffer, "%s -aufruf=2 %s", progname, lpszCmdLine);
                      ProcExec (buffer, nCmdShow, -1, 0, -1, 0);
                      ExitProcess (0);
             }

             dsqlstatus = sqlconnect (DbServer, User, Passw);
//             dsqlstatus = sqlconnectdbase (DbServer, "informix", "t0pfit", DataBase);
	         opendb (DataBase);
         }
       else
       {
	         opendb (DataBase);
       }
       sql_mode = 3;
       set_sqlproc (sql_error);
#else
       if (DataBase == NULL)
       {
             EnterDbase ();
       }
	   else
	   {
	         opendb (DataBase);
       }
#endif
#endif
       if (EditMode == FALSE)
       {
           ScrollMenue (bearbeiten, KEY7);
           ScrollMenue (bearbeiten, KEY8);
           ScrollMenue (bearbeiten, KEY11);
       }
       InitExecs ();

       if (anz > 0)
       {
                     strcpy (fname, varargs[0]);
                     if (strchr (fname, '.') == NULL)
                     {
					            strcat (fname, ".inf");
                     }
                     SetWinAttr ();
       }

       SetStdProc (WndProc);
#ifdef BIWAK
	   mv = getenv ("MOVEMAIN");
#else
	   mv = getenv_default ("MOVEMAIN");
#endif
	   if (mv)
	   {
		   MoveMain = min (1, max (0, atoi (mv)));
	   }
       InitFirstInstance (hInstance);
       InitNewInstance (hInstance, nCmdShow);


       if (AddMenue)
       {
	           hMenu = MakeMenue (menuetab);
	           SetMenu (hMainWindow, hMenu);
               EnableMenuItem (hMenu, KEY12,  MF_GRAYED);
       }
       if (AddToolbar)
       {
	           hwndTB = MakeToolBarEx (hInstance, 
		                       hMainWindow,tbb, 53,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);
               hwndCombo1 = MakeToolBarCombo (hInstance,
                                      hMainWindow,
                                      hwndTB, 
                                      11, 
                                      13, 
                                      31);

               FillCombo1 ();
               ComboToolTip (hwndTB, hwndCombo1);

               hwndCombo2 = MakeToolBarCombo (hInstance,
                                      hMainWindow,
                                      hwndTB, 
                                      12, 
                                      32, 
                                      52);

                FillCombo2 ();
                ComboToolTip (hwndTB, hwndCombo2);
       }

       eListe.InitListWindow (hMainWindow);
       if (AddToolbar)
       {
                eListe.SethwndTB (hwndTB);
       }
       SetAktivWindow (eListe.Getmamain2 ());

       SetEnvFont ();
       eListe.SetListFont (&lFont);
	   eListe.SetRowHeight (RowHeight);
       eListe.SetListFocus (AktFocus);
       eListe.SetUbRows (UbRows);
	   SetAttr ();
       if (anz > 0)
       {
                     strcpy (fname, varargs[0]);
                     if (strchr (fname, '.') == NULL)
                     {
					            strcat (fname, ".inf");
                     }
       }
       BOOL EnterRet = TRUE;
       if (anz > 1)
       {
                     if (strcmp (varargs[1], "0"))
                     {
                           strcpy (and_part, varargs[1]);
     		               if (strchr (and_part, '$') != NULL ||
			                   strchr (and_part, '#') != NULL ||
                               strchr (and_part, '%') != NULL)
                           { 
			                      EnterRet = EnterAndPart ();
                                  if (MacroCaption == FALSE && EnteredAndPart != NULL)
                                  {
                                       strcpy (InfoCaption, EnteredAndPart);
                                       SetWindowText (hMainWindow, InfoCaption);
                                  }
                           }
 		                   else
                           {
	                               setand_part (and_part);
                           }
                     }         
       }

       arganz = 0;
       for (i = 2; i < anz; i ++)
       {
            strcpy (args[i - 2], varargs[i]);
            argsp [i - 2] = args [i - 2];
            arganz ++;
       }

       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       eListe.SetFont (&lFont);
       
       SetDebug ();
       SetDebugMain (hMainWindow);

	   if (anz > 0 && EnterRet)
	   {
                     if (strstr (fname, ".mcr"))
                     {
                         LoadMacro (fname);
                     }
                     else
                     {
		                 ReadDB ();
                     }
	   }

	   if (EditMode)
	   {
               set_fkt (DeleteLine, 7);
               set_fkt (AppendLine, 8);
               set_fkt (CopyLine, 9);
	   }
	   else
	   {
               set_fkt (NULL, 7);
               set_fkt (NULL, 8);
               set_fkt (NULL, 9);
	   }
       eListe.ProcessMessages (ende_mode);
	   if (syskey == KEY5 && upd_mode)
	   {
		   WriteList ();
	   }
       close_cursor ();
       FreeForms ();
	   FreeTable ();
	   Closedbase (DataBase);
	   SaveRect () ; //LAC-94
       return 0;
}


void InitFirstInstance(HANDLE hInstance)
{


		SetSWEnvFont ();

		strcpy (lFont.FontName, FontNameSW);
        lFont.FontHeight = FontHeightSW;
        lFont.FontWidth  = FontWidthSW;       
        lFont.FontAttribute  = FontAttributeSW;       


        WNDCLASS wc;
//        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
        wc.style         =  CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "MAINICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hStdWindow";

        RegisterClass(&wc);


        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hListWindow";
        RegisterClass(&wc);

        hMainInst = hInstance;
        return;
}

void MoveMamain (void)
/**
Koordinaten in $BWSETC lesen.
**/
{
        char *etc;
        char buffer [256];
        FILE *fp;
        int anz;
		static BOOL scrfOK = FALSE;
		RECT rect;
//	    int xfull, yfull;


	    if (MoveMain == FALSE) return;
/*
        xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
	    if (xfull < 900) return;
*/

#ifdef BIWAK
        etc = getenv ("ETC");
        if (etc == NULL)
        {
             return;
        }
        sprintf (buffer, "%s\\biwak.rct", etc);

        fp = fopen (buffer, "r");
        if (fp == NULL) return;

        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
/*
        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
*/
        fclose (fp);

        anz = wsplit (buffer, ";");
        if (anz < 4) 
        {
            return;
        }

        rect.left   = atoi (wort[0]);
        rect.top    = atoi (wort[1]);
        rect.right  = atoi (wort[2]);
        rect.bottom = atoi (wort[3]);
#else
        etc = getenv ("BWSETC");
        if (etc == NULL)
        {
             etc = "\\user\\fit\\etc";
        }
        sprintf (buffer, "%s\\fit.rct", etc);

        fp = fopen (buffer, "r");
        if (fp == NULL) return;

        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        fclose (fp);

        anz = wsplit (buffer, " ");
        if (anz < 4) 
        {
            return;
        }

        rect.left   = atoi (wort[0]);
        rect.top    = atoi (wort[1]);
        rect.right  = atoi (wort[2]);
        rect.bottom = atoi (wort[3]);
		rect.left ++; 
		rect.top ++; 
		rect.right  = rect.right  - rect.left - 2;
		rect.bottom = rect.bottom - rect.top - 2;
#endif
        MoveWindow (hMainWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);
        return;
}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        TEXTMETRIC tm;
        HDC hdc;
        HFONT hFont, oldFont;
        char *Caption;
        int x,y,cx,cy;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (0, hdc);
        eListe.SetTextMetric (&tm);
        if (InfoCaption[0])
        {
            Caption = InfoCaption;
        }
        else
        {
            Caption = "Database INFO";
        }

        x = CW_USEDEFAULT;
        y = CW_USEDEFAULT;
        cx = CW_USEDEFAULT;
        cy = CW_USEDEFAULT;
        if (wx != -1) x = wx;
        if (wy != -1) y = wy;
        if (wcx != -1) cx = wcx;
        if (wcy != -1) cy = wcy;


        hMainWindow = CreateWindow ("hStdWindow",
                                    Caption,
                                    WS_THICKFRAME | WS_CAPTION | 
                                    WS_SYSMENU | WS_MINIMIZEBOX |
                                    WS_MAXIMIZEBOX,
//                                    CW_USEDEFAULT, CW_USEDEFAULT,
//                                    CW_USEDEFAULT, CW_USEDEFAULT,
                                    x,y, cx, cy,

                                    NULL,
                                    NULL,
                                    hInstance,
                                    NULL);

		if (wmaximized) 
		{
	        ShowWindow (hMainWindow, SW_SHOWMAXIMIZED);
		}
		else
		{
			ShowWindow (hMainWindow, SW_SHOWNORMAL);
//			ShowWindow (hMainWindow, SW_SHOWNOACTIVATE);
		}
        UpdateWindow (hMainWindow);
        if (WinSet == FALSE)
        {
               MoveMamain ();
        }
        return 0;
}


static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                       return msg.wParam;
                       }
             }
             if (eListe.IsMouseMessage (&msg))
             {
                       continue;
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
        Closedbase (DataBase); 
        return msg.wParam;
}

void InvalidateLines (void)
{
         RECT rect;
         static TEXTMETRIC tm;
         HDC hdc;

         hdc = GetDC (hMainWindow);
         GetTextMetrics (hdc, &tm);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (hMainWindow, &rect);

         rect.top = 2 * tm.tmHeight;
         rect.top = rect.top + rect.top / 3;
         rect.top -= 14;
         rect.bottom = rect.top + 10;
         InvalidateRect (hMainWindow, &rect, TRUE);
}

#ifndef BIWAK
void EnterDbase (void)
/**
Laenge einer Druckseite festlegen.
**/
{
#ifndef ODBC
	   short sqls;
	   int cursor;
#endif

       if (DataBase == NULL)
       {
           DataBase = new char [128];
           strcpy (DataBase, "");
       }
	   EnterF Enter;
	   Enter.EnterText (hMainWindow, "Datenbank ", DataBase);
	   if (syskey == KEYCR)
	   {
		        Closedbase (DataBase); 
	            opendb (DataBase);
#ifndef ODBC
				sqls = sql_mode;
				sql_mode = 1;
				cursor =  prepare_sql ("select * from a_bas");
				fetch_sql (cursor);
				close_sql (cursor);
				sql_mode = sqls;
#endif
	   }

}
#endif


void InitRowCapt (void)
{
       for (int i = 0; i < 500; i ++)
       {
           if (RowCaptions[i] != NULL)
           {
               delete RowCaptions[i];
               RowCaptions[i] = NULL;
           }
       }
       eListe.SetRowCaptions (NULL);
}

void EnterRowCapt (void)
{
       RowCaptDlg *Dlg;
       COLORREF Background;

       Background = GetSysColor (COLOR_3DFACE);
       DLG::hInstance = hMainInst;
       Dlg = new RowCaptDlg (-1, -1, 40, 8, "Kopftext f�r Druck", 105, FALSE);
       Dlg->SetRowCaptions (RowCaptions, 500);
       Dlg->SetStyle (WS_VISIBLE | WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU);
//       Dlg->SetStyleEx (WS_EX_CLIENTEDGE);
       Dlg->SetWinBackground (Background);
       Dlg->OpenScrollWindow (DLG::hInstance, hMainWindow);
	   Dlg->ProcessMessages ();
       if (syskey != KEY5)
       {
           eListe.SetRowCaptions (RowCaptions);
       }
       delete Dlg;
}

void InitRows (void)
{
       ColProps * Row;
       for (int i = 0; i < 500; i ++)
       {
           Row = Rows.GetCol (i);
           if (Row != NULL)
           {
               delete Row;
               Rows.SetCol (NULL, i);
           }
       }
       eListe.SetRows (NULL);
}

void EnterRows (void)
{
       RowDlg *Dlg;
       COLORREF Background;

       Background = GetSysColor (COLOR_3DFACE);
       DLG::hInstance = hMainInst;
       Dlg = new RowDlg (-1, -1, 60, 12, "Spalteneigenschaften", 105, FALSE);
       Dlg->SetRows(&Rows);
       Dlg->SetStyle (WS_VISIBLE | WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU);
//       Dlg->SetStyleEx (WS_EX_CLIENTEDGE);
       Dlg->SetWinBackground (Background);
       Dlg->OpenScrollWindow (DLG::hInstance, hMainWindow);
	   Dlg->ProcessMessages ();
       if (syskey != KEY5)
       {
           eListe.SetRows (&Rows);
       }
       delete Dlg;
}

void PrintLines (HDC hdc)
/**
Linien am Bildschirm anzeigen.
**/
{
         static TEXTMETRIC tm;
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         RECT rect;
         int x, y;

         if (AddToolbar)
         {
             GetTextMetrics (hdc, &tm);
             y = 2 * tm.tmHeight;
             y = y + y / 3;
		     y -= 14;
             GetClientRect (hMainWindow, &rect);
             x = rect.right - 2;
             if (hPenG == NULL)
             {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
             }
             SelectObject (hdc, hPenG);
             MoveToEx (hdc, 1, y, NULL);
             LineTo (hdc, x, y);

             y ++;
             SelectObject (hdc, hPenW);
             MoveToEx (hdc, 0, y, NULL);
             LineTo (hdc, x, y);
             eListe.SetLineRow (y);
         }
         else
         {
             eListe.SetLineRow (0);
         }
}

void ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   eListe.SetListLines (i);
                   break;
              }
          }
}

void ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   eListe.SetColors (Colors[i], BkColors[i]);
                   break;
              }
          }
}

static int row;
static char *pline;

static unsigned char ESC = 27;
static unsigned char NORM   = 70;
static unsigned char FETT1  = 80;
static unsigned char FETT2  = 81;
static unsigned char ENG  = 0;
static int frmlen;
static int PageStdCols;
static int Page = 0;

void PrintCaption (FILE *fp, char *buffer)
/**
Ueberschrift drucken.
**/
{
        int pagepos; 
        char cbuffer0 [512]; 
        char pbuffer [80];
        char cbuffer [512]; 
	    int slen;
        char datum [12];
        int ubrows = 0;
        int anz = 0;
        int i;

        memset (cbuffer, ' ', 511);
        cbuffer[511] = 0;
        Page ++;
        sprintf (pbuffer, "Seite %d", Page);
        pagepos = PageStdCols - strlen (pbuffer);
        sysdate (datum);

        if (PrintUb[0] != 0)
        {
            char *NewUb = new char [0x1000];
            StringWork::ChangeSysdate (NewUb, PrintUb, 0x1000);
            anz = wsplit (NewUb, "\n");
            delete NewUb;
            if (anz >  0)
            {
                    strcpy (InfoCaption, wort [0]);
                    sprintf (cbuffer0, "%c%c%c%c%s  %c%c", ESC,NORM, ESC,FETT1,
                                                             InfoCaption, ESC,NORM); 
            }
        }
        if (anz == 0)
        {
             sprintf (cbuffer0, "%c%c%c%c%s  %s%c%c", ESC,NORM, ESC,FETT1,
                                                 InfoCaption,datum, ESC,NORM); 
        }
        if (CaptMode < 2)
        {
                   memcpy (cbuffer, cbuffer0, strlen (cbuffer0));
                   cbuffer [pagepos] = 0;
                   strcpy (&cbuffer[pagepos], pbuffer);
                   fprintf (fp, "%s\n\n", cbuffer);
        }
        else
        {
                   strcpy (cbuffer, cbuffer0);
                   fprintf (fp, "%s\n\n", cbuffer);
                   row = ublen;
                   return;
        }

        if (anz > 1)
        {
            fprintf (fp, "%c%c", ESC, NORM);
            for (i = 1; i < anz; i ++)
            {
                    fprintf (fp, "%s\n", wort[i]);
                    ubrows ++;
            }
        }

        if (UsePrintForm)
        {
		         eListe.FillPrPrintUb (buffer);
        }
        else
        {
		         eListe.FillPrintUb (buffer);
        }
        if (ENG > 0)
        {
            fprintf (fp, "%c%c", ESC, ENG);
        }
        if (FETT)
        {
            fprintf (fp, "%c%c", ESC, FETT1);
        }
        else
        {
            fprintf (fp, "%c%c", ESC, FETT2);
        }
		fprintf (fp, "%s%c%c\n", buffer,ESC,NORM);
		if (GrPrint)
		{
			slen = strlen (buffer);
			fprintf (fp, "#LINE%d\n", slen);
		}
		else
		{
            fprintf (fp, "%s\n",  pline); 
		}
	    row = ublen + ubrows;
}


void EnterPagelen (void)
/**
Laenge einer Druckseite festlegen.
**/
{
       char PageLen [10]; 
	   EnterF Enter;
	   sprintf (PageLen, "%d", pagelen);
	   Enter.EnterUpDown (hMainWindow, "Seitenl�nge ", PageLen);

	   if (syskey == KEYCR)
	   {
	            pagelen = atoi (PageLen);
	   }
}


static BOOL ChangeEnvironment (char *String)
{
    char buffer [1024];
    char CParam [512];
    char *p;
    char *pe;
    char *env;
    unsigned int len;

    p = strchr (String, '%');
    if (p == NULL) return FALSE;
    pe = strchr ((p + 1), '%');
    if (pe == NULL) return FALSE;

    len = p - String;

    memcpy (buffer, String, len);
    buffer[len] = 0;
    p ++;
    len = pe - p;
    memcpy (CParam, p, len);
    CParam[len] = 0;
    pe ++;
    env = getenv (CParam);
    if (env == NULL)
    {
        if (*pe == 0) return FALSE;
        return ChangeEnvironment (pe);
    }

    strcat (buffer, env);
    strcat (buffer, pe);
    strcpy (String, buffer);
    return ChangeEnvironment (String);
}


BOOL ChangeParam (char *String, char *Param, void *Value, int Type)
{
    char buffer [512];
    char CParam [80];
    char *p;
    unsigned int len;

    p = strstr (String, Param);
    if (p == NULL) return FALSE;

    len = p - String;

    memcpy (buffer, String, len);
    buffer[len] = 0;
    switch (Type)
    {
            case 0 :
                   strcpy (CParam, (char *) Value);
                   break;
            case 1 :
                   sprintf (CParam, "%hd", *(short *) Value);
                   break;
            case 2 :
                   sprintf (CParam, "%d", *(int *) Value);
                   break;
            case 3 :
                   sprintf (CParam, "%ld", *(long *) Value);
                   break;
            case 4 :
                   sprintf (CParam, "%lf", *(double *) Value);
                   break;
    }

    strcat (buffer, CParam);
    p += strlen (Param);
    strcat (buffer, p);
    strcpy (String, buffer);
    return ChangeParam (String, Param, Value, Type);
}


void ChangePrintParams (void)
/**
Parameter fuer spezielles Printprogramm einsaetzen.
**/
{

    static char *Params [] = {"$PROWS",
                              "$PCOLS",
                              "$DATAPROWS",
                              NULL,

    };

    static void *Values [] = {(int *) &pagelen,
                              (int *) &PageStdCols,
                              (int *) &pagelen0,
                              NULL,
    };

    static int Types [] = {2,
                           2,
                           2,
    };

    int i;

    strcpy (BeforePrint, BeforePrintOrg);
    for (i = 0; Params[i] != NULL; i ++)
    {
        ChangeParam (BeforePrint, Params[i], Values [i], Types[i]);
    }
}


void Print (void)
/**
Liste drucken.
**/
{

	    char *tmp;
		char buffer [0x1000];
		char listname [256];
		char command  [512];
		char *p;
		FILE *fp;
		int anz;
		int i;
		int len;
		gPrintl Gprint;
        DWORD ExitCode;

        if (ScrPrint)
        {
                eListe.PrintList ();
                return;
        }
        ublen = 5;
        if (CaptMode == 3)
        {
            ublen = 1;
        }
        else if (CaptMode == 2)
        {
            ublen = 3;
        }
        pagelen = 60;
        PageStdCols = 80;
        Page = 0;
        if (QuerPrint)
        {
              SetQuerDefault (TRUE);
        }
        if (DrkDeviceFile != NULL)
        {
             SetDrkDeviceFile (DrkDeviceFile);
        }
        else
        {
             SetDrkDeviceFile (NULL);
        }
        if (RowPrintFont != 0)
        {
            SetRowPrintFont (RowPrintFont);
        }
        if (SelectPrinter ())
        {
             pagelen = GetPageRows (NULL) - 2;
             pagelen0 = pagelen - ublen;
             PageStdCols = GetPageStdCols (NULL);
             if (BeforePrint != NULL)
             {
                 ChangePrintParams ();
             }
        }
        else
        {
             return;
        }

        len = eListe.GetUbFrmlen ();
        frmlen = len;
        ENG = GetRowWidthStz (len);
        if (PrintFont != 0)
        {
            ENG = PrintFont;
        }

		Token t;
		t.SetSep ("\\");
		t = fname;
  	    strcpy (listname, t.GetToken (t.GetAnzToken () - 1));
		t.SetSep ("/");
		t = listname;
  	    strcpy (listname, t.GetToken (t.GetAnzToken () - 1));
		p = strchr (listname, '.');
		if (p) *p = 0;

		tmp = getenv ("TMPPATH");
        if (tmp == NULL) 
		{
			sprintf (buffer, "%s.lst", listname);
		}
		else
		{
			sprintf (buffer, "%s\\%s.lst", tmp,listname);
		}

		fp = fopen (buffer, "w");
		if (fp == NULL)
		{
			print_mess (2, "Druckdatei %s kann nicht ge�ffnet werden", buffer);
			return;
		}

		if (InfoCaption[0] == 0)
		{
			sprintf (InfoCaption , "%s %s", fname, and_part);
		}
		eListe.FillPrintUb (buffer);
		len = strlen (buffer);
		pline = (char *) GlobalAlloc (GMEM_FIXED, len + 10);
		memset (pline, '-', len);
		pline [len] = 0;
        anz = eListe.GetRecanz ();
        if (CaptMode < 3) PrintCaption (fp, buffer);
        if (ENG > 0)
        {
                fprintf (fp, "%c%c", ESC, NORM);
        }

        for (i = 0; i < anz; i ++)
		{
			if (CaptMode == 0 && row >= pagelen)
			{
//				    fprintf (fp, "%c", (unsigned char) 0x0C);
                    fprintf (fp, "%c%c", ESC, NORM);
		            PrintCaption (fp, buffer);
			}
            FromMemory (i);
            if (ENG > 0)
            {
                fprintf (fp, "%c%c", ESC, ENG);
            }
            if (FETT)
            {
                fprintf (fp, "%c%c", ESC, FETT1);
            }
            if (UsePrintForm)
            {
			           eListe.FillPrPrintRow (buffer);
            }
            else if (memcmp (ausgabesatz, "$DISP", 5) == 0)
            {
                       strcpy (buffer, (char *) ausgabesatz + 5);
            }
            else
            {
			           eListe.FillPrintRow (buffer);
            }
			fprintf (fp, "%s\n", buffer);

            if (FETT)
            {
                fprintf (fp, "%c%c", ESC, FETT2);
            }

            if (ENG > 0)
            {
                fprintf (fp, "%c%c", ESC, NORM);
            }

			row ++;
		}
//	    fprintf (fp, "%c", (unsigned char) 0x0C);
		fclose (fp);
        if (BeforePrint)
        {
//            ProcWaitExec (BeforePrint, SW_HIDE, -1, 0, -1, 0);
            ExitCode = ProcWaitExec (BeforePrint, SW_NORMAL, -1, 0, -1, 0);
        }
		GlobalFree (pline);

        if (tmp == NULL) 
		{
			sprintf (buffer, "%s.lst", listname);
		}
		else
		{
			sprintf (buffer, "%s\\%s.lst", tmp,listname);
		}

        if (PrintFile != NULL)
        {
            strcpy (buffer, PrintFile);
        }
		if (GrPrint == FALSE)
		{
		    sprintf (command, "fitprint %s 0", buffer); 
	        ProcWaitExec (command, SW_SHOWNORMAL, -1, 0, -1, 0); 
		    disp_mess ("Liste wurde gedruckt", 2);
		}
		else
		{
		    sprintf (command, "-p%d %s", PrintMode, buffer); 
			Gprint.Print (command, SW_SHOWNORMAL);
		}
        SetQuerDefault (FALSE);
        SetRowPrintFont (0);
}


void PrintMailCaption (FILE *fp, char *buffer)
/**
Ueberschrift drucken.
**/
{
        int pagepos; 
        char cbuffer0 [512]; 
        char pbuffer [80];
        char cbuffer [512]; 
//	    int slen;
        char datum [12];
        int ubrows = 0;
        int anz = 0;
        int i;

        memset (cbuffer, ' ', 511);
        cbuffer[511] = 0;
        Page ++;
        sprintf (pbuffer, "Seite %d", Page);
        pagepos = PageStdCols - strlen (pbuffer);
        sysdate (datum);

        if (PrintUb[0] != 0)
        {
            anz = wsplit (PrintUb, "\n");
            if (anz >  0)
            {
                    strcpy (InfoCaption, wort [0]);
                    sprintf (cbuffer0, "%s", InfoCaption); 
            }
        }
        if (anz == 0)
        {
            sprintf (cbuffer0, "%s  %s", InfoCaption,datum); 
        }
        if (CaptMode < 2)
        {
                   memcpy (cbuffer, cbuffer0, strlen (cbuffer0));
                   cbuffer [pagepos] = 0;
                   strcpy (&cbuffer[pagepos], pbuffer);
                   fprintf (fp, "%s\n\n", cbuffer);
        }
        else
        {
                   strcpy (cbuffer, cbuffer0);
                   fprintf (fp, "%s\n\n", cbuffer);
                   row = ublen;
                   return;
        }

        if (anz > 1)
        {
            for (i = 1; i < anz; i ++)
            {
                    fprintf (fp, "%s\n", wort[i]);
                    ubrows ++;
            }
        }

        if (UsePrintForm)
        {
		         eListe.FillPrPrintUb (buffer);
        }
        else
        {
		         eListe.FillPrintUb (buffer);
        }
		fprintf (fp, "%s\n", buffer);
/*
		if (GrPrint)
		{
			slen = strlen (buffer);
			fprintf (fp, "#LINE%d\n", slen);
		}
		else
*/
		{
            fprintf (fp, "%s\n",  pline); 
		}
	    row = ublen + ubrows;
}


void SendMail (void)
/**
Liste drucken.
**/
{

	    char *tmp;
		char buffer [0x1000];
		char listname [256];
		char command  [512];
		char *p;
		FILE *fp;
		int anz;
		int i;
		int len;
		gPrintl Gprint;
        DWORD ExitCode;

        ublen = 5;
        if (CaptMode == 3)
        {
            ublen = 1;
        }
        else if (CaptMode == 2)
        {
            ublen = 3;
        }
        pagelen = 60;
        PageStdCols = 80;
        Page = 0;

        len = eListe.GetUbFrmlen ();
        frmlen = len;

  	    strcpy (listname, fname);
		p = strchr (listname, '.');
		if (p) *p = 0;
		p = strchr (listname, ',');
		if (p) *p = 0;

		tmp = getenv ("TMPPATH");
        if (tmp == NULL) 
		{
			sprintf (buffer, "%s.txt", listname);
		}
		else
		{
			sprintf (buffer, "%s\\%s.txt", tmp,listname);
		}

		fp = fopen (buffer, "w");
		if (fp == NULL)
		{
			print_mess (2, "Maildatei kann nicht ge�ffnet werden");
			return;
		}

		if (InfoCaption[0] == 0)
		{
			sprintf (InfoCaption , "%s %s", fname, and_part);
		}
		eListe.FillPrintUb (buffer);
		len = strlen (buffer);
		pline = (char *) GlobalAlloc (GMEM_FIXED, len + 10);
		memset (pline, '-', len);
		pline [len] = 0;
        anz = eListe.GetRecanz ();
        if (CaptMode < 3) PrintMailCaption (fp, buffer);

        for (i = 0; i < anz; i ++)
		{
/*
			if (CaptMode == 0 && row >= pagelen)
			{
		            PrintCaption (fp, buffer);
			}
*/
            FromMemory (i);
            if (UsePrintForm)
            {
			           eListe.FillPrPrintRow (buffer);
            }
            else if (memcmp (ausgabesatz, "$DISP", 5) == 0)
            {
                       strcpy (buffer, (char *) ausgabesatz + 5);
            }
            else
            {
			           eListe.FillPrintRow (buffer);
            }
			fprintf (fp, "%s\n", buffer);

			row ++;
		}
		fclose (fp);
        if (BeforePrint)
        {
//            ProcWaitExec (BeforePrint, SW_HIDE, -1, 0, -1, 0);
            ExitCode = ProcWaitExec (BeforePrint, SW_NORMAL, -1, 0, -1, 0);
        }
		GlobalFree (pline);

        if (tmp == NULL) 
		{
			sprintf (buffer, "%s.txt", listname);
		}
		else
		{
			sprintf (buffer, "%s\\%s.txt", tmp,listname);
		}

        if (PrintFile != NULL)
        {
            strcpy (buffer, PrintFile);
        }
        if (MailParams[0] == 0)
        {
	            sprintf (command, "sendexmail %s", buffer); 
        }
        else
        {
	            sprintf (command, "sendexmail %s %s", MailParams, buffer); 
        }
        if (ProcExec (command, SW_SHOWNORMAL, -1, 0, -1, 0) == 0)
        {
            disp_mess ("Fehler beim Starten von sendexmail", 2);
        }
}

BOOL TestExec (WPARAM wParam)
{
        int Id; 
		int pos;
        char buffer [256];

        if (ExecPtr == 0) return FALSE;

        Id = LOWORD (wParam);
        if (Id < ExecOffs ||
            Id >= ExecOffs + ExecPtr)
        {
            return FALSE;
        }

        Id -= ExecOffs;
        if (Execs[Id] == NULL) return FALSE;

        switch (ExecModes[Id])
        {
            case EXECNORMAL :
                   ProcExec (Execs [Id], SW_SHOWNORMAL, -1, 0, -1, 0);
                   break;
            case EXECWAIT :
                   ProcWaitExecEx (Execs [Id], SW_SHOWNORMAL, -1, 0, -1, 0);
                   break;
            case EXECHIDE :
                   ProcExec (Execs [Id], SW_HIDE, -1, 0, -1, 0);
                   break;
            case CALL :
                   if (eListe.GetRecanz () <= 0) return FALSE;

        		   sprintf (buffer, "call %s", Execs [Id]);
		           eListe.SwitchRowMode (0);
		           pos = eListe.GetAktRow ();
                   memcpy (ausgabesatz, SwSaetze[pos], zlen);
		           do_call (buffer);
                   memcpy (SwSaetze[pos], ausgabesatz, zlen);
		           eListe.SwitchRowMode (1);
		           if (toprint)
                   {
			                eListe.ShowAktRow ();
                            toprint = 0;
                   }
                   break;
        }
        return TRUE;
}




void ChoisePrinter (void)
/**
Drucker Auswaehlen.
**/
{
        SelectPrinter ();
}


void SaveMacro (void)
{
        char Path [1024];
        static char *konvert;
        FILE *fp;
        char FileName [512];
        OPENFILENAME fnstruct;
        static LPCTSTR lpstrFilter = 
            "Macro-Datei\0*.mcr\0Alle Dateien\0*.*\0,\0";


        clipped (fname);
        clipped (and_part);
        clipped (selfields);

        if (strcmp (fname, " ") == 0)
        {
            fname[0] = 0;
        }

        if (strlen (fname) == 0)
        {
            disp_mess ("Es wurde keine Selection eingegeben", 2);
            return;
        }

        FileName[0] = 0;   
        konvert = getenv ("KONVERT");
        sprintf (Path, "%s\\macros", konvert);
        ZeroMemory (&fnstruct, sizeof (fnstruct));
        fnstruct.lStructSize = sizeof (fnstruct);
        fnstruct.hwndOwner   = AktivWindow;
        fnstruct.lpstrFile   = FileName;
        fnstruct.nMaxFile    = 255;
        fnstruct.lpstrFilter = lpstrFilter;
        fnstruct.lpstrInitialDir  = Path;

        syskey = 0;
        if (GetSaveFileName (&fnstruct) == 0)
        {
               UpdateWindow (hMainWindow);
               syskey = KEY5;
               return;
        }
        if (strchr (FileName, '.') == NULL)
        {
            strcat (FileName, ".mcr");
        }
        fp = fopen (FileName, "w");
        if (fp == NULL)
        {
            print_mess (2, "%s kann nicht ge�ffnet werden");
            return;
        }
        fprintf (fp, "%s\n", fname);
        fprintf (fp, "%s\n", and_part);
        fprintf (fp, "%s\n", selfields);
/*
        if (RowCaptions != NULL)
        {
            for (int i = 0; i < 500; i ++)
            {
                if (RowCaptions[i] != NULL)
                {
                    fprintf (fp, "%d;%s\n", i, RowCaptions[i]);
                }
            }
        }
*/

        for (int i = 0; i < 500; i ++)
        {
                if (Rows.GetCol (i) != NULL)
                {
                    fprintf (fp, "%d;%s;%d;%s\n", i, Rows.GetCol (i)->GetCaption (),
                                                     Rows.GetCol (i)->GetLength (),
                                                     Rows.GetCol (i)->GetPicture ());
                }
        }

        if (PrintUb[0] != 0)
        {
            fprintf (fp,"[Caption]\n");
            int anz = wsplit (PrintUb, "\n");
            for (int i = 0; i < anz; i ++)
            {
                cr_weg (wort[i]);
                fprintf (fp, "%s\n", wort[i]);
            }
        }
        fclose (fp);
        InvalidateRect (eListe.Getmamain2 (), 0, TRUE);
        UpdateWindow (hMainWindow);
}


void LoadMacro (void)
{
        char Path [1024];
        static char *konvert;
        FILE *fp;
        char *p;
        char FileName [512];
        OPENFILENAME fnstruct;
        static LPCTSTR lpstrFilter = 
            "Macro-Datei\0*.mcr\0Alle Dateien\0*.*\0,\0";

        ListClass::ReadItemLabel = TRUE;
        CheckMenuItem (hMenu, IDM_ITEMLABEL, MF_CHECKED);
        EnteredAndPart = NULL;
        FileName[0] = 0;   
        konvert = getenv ("KONVERT");
        sprintf (Path, "%s\\macros", konvert);
        ZeroMemory (&fnstruct, sizeof (fnstruct));
        fnstruct.lStructSize = sizeof (fnstruct);
//        fnstruct.hwndOwner   = AktivWindow;
        fnstruct.hwndOwner   = NULL;
        fnstruct.lpstrFile   = FileName;
        fnstruct.nMaxFile    = 255;
        fnstruct.lpstrFilter = lpstrFilter;
        fnstruct.lpstrInitialDir  = Path;

        syskey = 0;
        if (GetOpenFileName (&fnstruct) == 0)
        {
               UpdateWindow (hMainWindow);
               syskey = KEY5;
               return;
        }
        fp = fopen (FileName, "r");
        if (fp == NULL)
        {
            print_mess (2, "%s kann nicht ge�ffnet werden", Path);
            return;
        }

        p = fgets (fname, 511, fp);
        if (p == NULL)
        {
            fclose (fp);
            return;
        }
        cr_weg (fname);
        clipped (fname);

        p = fgets (and_part, 1023, fp);
        if (p == NULL)
        {
            fclose (fp);
            return;
        }
        cr_weg (and_part);
        clipped (and_part);
        p = fgets (selfields, 1023, fp);
        if (p == NULL)
        {
            fclose (fp);
            return;
        }
        cr_weg (selfields);
        clipped (selfields);
/*
        InitRowCapt ();
        while (p = fgets (Path, 1023, fp))
        {
            if (Path[0] == '[') break;
            cr_weg (Path);
            int anz = wsplit (Path, ";");
            if (anz > 1)
            {
                int row = atoi (wort[0]);
                RowCaptions[row] = new char [256];
                if (RowCaptions[row] != NULL)
                {
                    strcpy (RowCaptions[row], wort[1]);
                    eListe.SetRowCaptions (RowCaptions);
                }
            }
        }
*/

        InitRows ();

        while (p = fgets (Path, 1023, fp))
        {
            if (Path[0] == '[') break;
            cr_weg (Path);
            int anz = wsplit (Path, ";");
            int row;
            if (anz > 0)
            {
                row = atoi (wort[0]);
                Rows.SetCol (new ColProps (), row);
            }
            if (anz > 1)
            {
                Rows.GetCol (row)->SetCaption (wort [1]);
            }
            if (anz > 2)
            {
                Rows.GetCol (row)->SetLength (atoi (wort[2]));
            }

            if (anz > 3)
            {
                Rows.GetCol (row)->SetPicture (wort[3]);
            }
            eListe.SetRows (&Rows);
        }

        if (Path[0] == '[')
        {
             PrintUb[0] = 0;
             while (p = fgets (Path, 1023, fp))
             {
                   strcat (PrintUb, Path);
                   char *Txt = new char [strlen (PrintUb) + 1];
                   if (Txt != NULL)
                   {
                       strcpy (Txt, PrintUb);
                       char *p = strtok (Txt, "\n");
                       if (p != NULL)
                       {
                           cr_weg (p);
                           strcpy (InfoCaption, p);
                           StringWork::ChangeSysdate (InfoCaption, p, 256);
                           SetWindowText (hMainWindow, InfoCaption);
                           delete p;
                       }
                   }
             }
             MacroCaption = TRUE; 
        }   
        fclose (fp);


//        InvalidateRect (eListe.Getmamain2 (), 0, TRUE);
//        UpdateWindow (hMainWindow);


        NewTable (TRUE);

}


void LoadMacro (char *mname)
{
        char Path [1024];
        static char *konvert;
        FILE *fp;
        char *p;

        EnteredAndPart = NULL;
        konvert = getenv ("KONVERT");
        sprintf (Path, "%s\\macros\\%s", konvert,mname);
        fp = fopen (Path, "r");
        if (fp == NULL)
        {
            print_mess (2, "%s kann nicht ge�ffnet werden", Path);
            return;
        }

        p = fgets (fname, 511, fp);
        if (p == NULL)
        {
            fclose (fp);
            return;
        }
        cr_weg (fname);
        clipped (fname);

        p = fgets (and_part, 1023, fp);
        if (p == NULL)
        {
            fclose (fp);
            return;
        }
        cr_weg (and_part);
        clipped (and_part);
        p = fgets (selfields, 1023, fp);
        if (p == NULL)
        {
            fclose (fp);
            return;
        }
        cr_weg (selfields);
        clipped (selfields);

        InitRows ();

        while (p = fgets (Path, 1023, fp))
        {
            if (Path[0] == '[') break;
            cr_weg (Path);
            int anz = wsplit (Path, ";");
            int row;
            if (anz > 0)
            {
                row = atoi (wort[0]);
                Rows.SetCol (new ColProps (), row);
            }
            if (anz > 1)
            {
                Rows.GetCol (row)->SetCaption (wort [1]);
            }
            if (anz > 2)
            {
                Rows.GetCol (row)->SetLength (atoi (wort[2]));
            }

            if (anz > 3)
            {
                Rows.GetCol (row)->SetPicture (wort[3]);
            }
            eListe.SetRows (&Rows);
        }

        if (Path[0] == '[')
        {
             PrintUb[0] = 0;
             while (p = fgets (Path, 1023, fp))
             {
                   strcat (PrintUb, Path);
                   char *Txt = new char [strlen (PrintUb) + 1];
                   if (Txt != NULL)
                   {
                       strcpy (Txt, PrintUb);
                       char *p = strtok (Txt, "\n");
                       if (p != NULL)
                       {
                           cr_weg (p);
//                           strcpy (InfoCaption, p);
                           StringWork::ChangeSysdate (InfoCaption, p, 256);
                           SetWindowText (hMainWindow, InfoCaption);
                           delete p;
                       }
                   }
             }
             MacroCaption = TRUE;
        }   
        fclose (fp);
		if (strchr (and_part, '$') != NULL ||
			strchr (and_part, '#') != NULL ||
            strchr (and_part, '%') != NULL)
		{
			if (EnterAndPart () == FALSE)
			{
			      return;
			}
		}
		else
		{
	        setand_part (and_part);
		}
        if (MacroCaption == FALSE && EnteredAndPart != NULL)
        {
            strcpy (InfoCaption, EnteredAndPart);
            SetWindowText (hMainWindow, InfoCaption);
        }
        ReadDB ();
}


static MTXT *CaptTxt;
static mfont pFont = {
                     "Courier New", 
                      100, 
                      0, 
                      0, 
                      BLACKCOL, 
                      LTGRAYCOL,
                      0, 0};


void InputPrinCaption (void)
{
  	  int cx, cy;
	  form *scurrent;
      char *EText;


	  scurrent = current_form;
      cx = 80;
      cy = 20;
      CaptTxt = new MTXT (cx, cy, "�berschrift ", PrintUb, NULL, &pFont);
      CaptTxt->OpenWindow (hMainInst, hMainWindow);
	  EnableWindow (hMainWindow, FALSE);

	  CaptTxt->ProcessMessages ();
      if (syskey != KEY5 &&
          syskey != KEYESC)
      {
        	   EText = CaptTxt->GetText ();
	           if (strlen (EText) > 0)
               {
                   strncpy (PrintUb, EText, 0x1000);
                   char *Txt = new char [strlen (PrintUb) + 1];
                   if (Txt != NULL)
                   {
                       strcpy (Txt, PrintUb);
                       char *p = strtok (Txt, "\n");
                       if (p != NULL)
                       {
                           cr_weg (p);
//                           strcpy (InfoCaption, p);
                           StringWork::ChangeSysdate (InfoCaption, p, 256);
                           SetWindowText (hMainWindow, InfoCaption);
                           delete p;
                       }
                   }
               }
	  }

	  SetActiveWindow (hMainWindow);
	  EnableWindow (hMainWindow, TRUE);
      CaptTxt->DestroyWindow ();
	  delete CaptTxt;
	  current_form = scurrent;
}


void ChangePrintMode (void)
{
      if (ScrPrint == 0)
      {
          ScrPrint = 1;
          CheckMenuItem (hMenu, IDM_PRINTMODE, MF_CHECKED);
      }
      else if (ScrPrint == 1)
      {
          ScrPrint = 0;
          CheckMenuItem (hMenu, IDM_PRINTMODE, MF_UNCHECKED);
      }
}


void ChangeQuerPrint (void)
{
      if (QuerPrint == 0)
      {
          QuerPrint = 1;
          eListe.SetQuerPrint (QuerPrint);
          CheckMenuItem (hMenu, IDM_QUERPRINT, MF_CHECKED);
      }
      else
      {
          QuerPrint = 0;
          eListe.SetQuerPrint (QuerPrint);
          CheckMenuItem (hMenu, IDM_QUERPRINT, MF_UNCHECKED);
      }
}

void ChangePrintRowCaption (void)
{
      if (eListe.GetPrintRowCaptions () == 0)
      {
          eListe.SetPrintRowCaptions (1);
          CheckMenuItem (hMenu, IDM_ROWCAPTION, MF_CHECKED);
      }
      else
      {
          eListe.SetPrintRowCaptions (0);
          CheckMenuItem (hMenu, IDM_ROWCAPTION, MF_UNCHECKED);
      }
}


LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
//		char where [256];

        switch(msg)
        {
              case WM_PAINT :
                      if (hWnd == hMainWindow)
                      {
                              hdc = BeginPaint (hMainWindow, &aktpaint);
                              PrintLines (hdc);
                              EndPaint (hMainWindow, &aktpaint);
                      }
                      else
                      {
                          if (ListOK)
                          {
                              eListe.OnPaint (hWnd, msg, wParam, lParam);
                          }
                      }
                      break;
              case WM_SIZE :
                      if (hWnd == hMainWindow)
                      {
                              InvalidateLines ();
                              eListe.MoveListWindow ();
                      }
                      break;

              case WM_HSCROLL :
                       eListe.OnHScroll (hWnd, msg,wParam, lParam);
                       break;
                      
              case WM_VSCROLL :
                       eListe.OnVScroll (hWnd, msg, wParam, lParam);
                       break;

              case WM_SYSCOMMAND:
                      if (wParam == SC_CLOSE)
                      {
                               if (upd_mode == FALSE)
							   {
                                          close_cursor ();
		                                  FreeTable ();
                                          Closedbase (DataBase); 
									      SaveRect () ; //LAC-94
						                  ExitProcess (0);
							   }
                        	   if (abfragejn (eListe.Getmamain3 () , 
			                       "Verarbeitung abbrechen", "N"))
							   {
                                          close_cursor ();
		                                  FreeTable ();
                                          Closedbase (DataBase); 
									      SaveRect () ; //LAC-94
						                  ExitProcess (0);
							   }
							   return 0;
					  }
					  break;

              case WM_NOTIFY :
                    {
                      LPNMHDR pnmh = (LPNMHDR) lParam;

                      if (pnmh->code == TTN_NEEDTEXT)
                      {
                           LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
                           if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                               == FALSE)
                           {
                                    QuickHwndCpy (lpttt);
                           }
                      }
                      break;
                    }

              case WM_DESTROY :
				      OnDestroy (hWnd); 
                      if (hWnd == hMainWindow)
                      {
                             PostQuitMessage (0);
                             return 0;
                      }
                      break;
              case WM_KEYDOWN :
                    eListe.FunkKeys (wParam, lParam);
                    break;
                  
              case WM_COMMAND :
                    if (TestExec (wParam)) 
                    {
                            return 0;
                    }
                    if (LOWORD (wParam) == KEYESC)
                    {
                            syskey = KEYESC;
                            SendKey (VK_ESCAPE);
                            break;
                    }
                    if (LOWORD (wParam) == KEY2)
                    {
                            syskey = KEY2;
                            SendKey (VK_F2);
                            break;
                    }
                    if (LOWORD (wParam) == KEY3)
                    {
                            syskey = KEY3;
                            SendKey (VK_F3);
                            break;
                    }
                    if (LOWORD (wParam) == KEY4)
                    {
                            syskey = KEY4;
                            SendKey (VK_F4);
                            break;
                    }
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYDOWN;
                            SendKey (VK_DOWN);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
                            SendKey (VK_UP);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYLEFT)
                    {
                            syskey = KEYLEFT;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYRIGHT)
                    {
                            syskey = KEYRIGHT;
                            SendKey (VK_RIGHT);
                            break;
                    }
					else if (LOWORD (wParam) == IDM_WORK)
                    {
                            NewFile ();
                            break;
                   }
				   else if (LOWORD (wParam) == IDM_FRAME)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_FRAME,
								           MF_CHECKED); 
							ActiveMark = IDM_FRAME;
							eListe.SetFrameFocus ();
                   }
				   else if (LOWORD (wParam) == IDM_REVERSE)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_REVERSE,
								           MF_CHECKED); 
							ActiveMark = IDM_REVERSE;
							eListe.SetRevFocus ();
                   }
				   else if (LOWORD (wParam) == IDM_NOMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_NOMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_NOMARK;
							eListe.SetNoFocus ();
                   }
				   else if (LOWORD (wParam) == IDM_EDITMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_EDITMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_EDITMARK;
							eListe.SetEditFocus ();
                   }
				   else if (LOWORD (wParam) == IDM_EDITMARK3D)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_EDITMARK3D,
								           MF_CHECKED); 
							ActiveMark = IDM_EDITMARK3D;
							eListe.SetEdit3DFocus ();
                   }
				   else if (LOWORD (wParam) == IDM_PAGEVIEW)
                   {
                            if (eListe.GetRecanz () < 1) break;
                            if (PageView)
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                                    ToolBar_SetState(hwndTB, IDM_PAGE, TBSTATE_ENABLED);
                                    PageView = 0;
                                    eListe.SwitchPage (eListe.GetAktRowS ());
                            }
                            else
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                                    ToolBar_SetState(hwndTB, IDM_LIST, TBSTATE_ENABLED);
                                    PageView = 1;
                                    eListe.SwitchPage (eListe.GetAktRow ());
                            }
                            SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_PAGE)
                   {
                            PageView = 1; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                            eListe.SwitchPage (eListe.GetAktRow ());
                            SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_LIST)
                   {
                            PageView = 0; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                            eListe.SwitchPage (eListe.GetAktRowS ());
                            SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
                   }
				   else if (LOWORD (wParam) == IDM_SHOW)
                   {
	  		                QueryEnter = 0;
                            EnteredAndPart = NULL;
						    NewTable (FALSE);
							break;
                    }
					else if (LOWORD (wParam) == IDM_DEL)
                    {
						    if (fname[0])
							{
						           QueryEnter = 1;
							}
							else
							{
						           QueryEnter = 0;
							}
						    NewTable (FALSE);
							break;
                    }
					else if (LOWORD (wParam) == IDM_PRINT)
                    {
						        Print ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_PRINTMODE)
                    {
						        ChangePrintMode ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_QUERPRINT)
                    {
						        ChangeQuerPrint ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_ROWCAPTION)
                    {
						        ChangePrintRowCaption ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_SEND_MAIL)
                    {
						        SendMail ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_SAVE_MACRO)
                    {
						        SaveMacro ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_LOAD_MACRO)
                    {
						        LoadMacro ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_CHPRINT)
                    {
						        ChoisePrinter ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_PAGELEN)
                    {
						        EnterPagelen ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_COPY)
                    {
						        eListe.ToClipboard ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_INS)
                    {
						        eListe.FromClipboard ();
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_MARKALL)
                    {
						        eListe.MarkAll ();
                                InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
								return 0;
                    }
					else if (LOWORD (wParam) == IDM_DELMARK)
                    {
						        eListe.InitMark ();
                                InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
								return 0;
                    }
                    if (HIWORD (wParam) == CBN_CLOSEUP)
                    {
                                if (lParam == (LPARAM) hwndCombo1)
                                {
                                   ChoiseCombo1 ();
                                }
                                if (lParam == (LPARAM) hwndCombo2)
                                {
                                   ChoiseCombo2 ();
                                }
                                SetFocus (eListe.Getmamain3 ());
                                return 0;
                    }
					else if (LOWORD (wParam) == IDM_FONT)
                    {
                                  eListe.ChoiseFont (&lFont);
                                  eListe.SetFont (&lFont);
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_ITEMLABEL)
                    {
                                  if (ListClass::ReadItemLabel)
                                  {
                                      ListClass::ReadItemLabel = FALSE;
                                      CheckMenuItem (hMenu, IDM_ITEMLABEL, MF_UNCHECKED);
                                  }
                                  else
                                  {
                                      ListClass::ReadItemLabel = TRUE;
                                      CheckMenuItem (hMenu, IDM_ITEMLABEL, MF_CHECKED);
                                  }
                                  ReRead ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_FIND)
                    {
                                  eListe.FindString ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_SQL)
                    {
                                  InputSQLEx ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_DB)
                    {
#ifndef BIWAK
                                  EnterDbase ();
#endif
                                  return 0;
                                   
                    }
                    else if (LOWORD (wParam) == IDM_ROWCAPT)
                    {
//                                 EnterRowCapt ();
                                 EnterRows ();
                                 return 0;
                    }
                    else if (LOWORD (wParam) == IDM_DEL_ROWCAPT)
                    {
//                                 InitRowCapt ();
                                 InitRows ();
                                 return 0;
                    }
                    else if (LOWORD (wParam) == IDM_PRINT_CAPT)
                    {
                                 InputPrinCaption ();
                                 return 0;
                    }
                    else if (LOWORD (wParam) == IDM_INFO)
                    {
                                  syskey = KEY4;
                                  SendKey (VK_F4);
                    }
                    else if (LOWORD (wParam) == IDM_TEXT)
                    {
                    }
                    else if (LOWORD (wParam) == IDM_ETI)
                    {
                    }
/*
                    else if (eListe.IsUbMessage (LOWORD (wParam)))
                    {
                                   return 0;
                    }
*/

        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


BOOL EnterAndPart (void)
{
       TextDlg *textDlg = NULL;
	   StringWork *textWork;
	   static char newAndPart [512];


       textWork = new StringWork (and_part); 

       textDlg = new TextDlg (textWork , -1, -1, 60, 10, "Auswahl", StdSz, FALSE);
       textDlg->SetStyle (WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU);
       textDlg->OpenScrollWindow (hMainInst, hMainWindow);
       EnableWindows (hMainWindow, FALSE);
       textDlg->ProcessMessages ();
	   if (syskey == KEY5)
	   {
           delete textDlg;
	       delete textWork;
		   return FALSE;
	   }
       if (textWork->setFelder (newAndPart, 511) == NULL)
	   {
           delete textDlg;
	       delete textWork;
		   return FALSE;
	   }

       delete textDlg;
	   delete textWork;
       setand_part (newAndPart);
       EnteredAndPart = newAndPart;
       return TRUE;
}

static MTXT *Mtxt;
static long TextNr;
char *sqltext;
Text SqlText; 

#ifndef BIWAK
#ifdef ODBC
void FillSqlValue (int i, char *val, int *vlen, int cursor)
#else
void FillSqlValue (int i, char *val, int *vlen)
#endif
{
	  int len;
	  int nkomma;
	  int typ;
	  char format [20];

#ifdef ODBC
      typ = get_coltype (i + 1, cursor);
      len = get_collength (i + 1, cursor);
#else
	  typ = sqltyp (i);
	  len = sqllen (i);
#endif
	  switch (typ)
	  {
		         case SQLCHAR :
		         case SQLVCHAR :
#ifdef ODBC
                   char buffer [256];
                   strcpy (buffer, "");
                   get_colvalue (i + 1, cursor, SQLCCHAR, (char *) buffer, len);
                   sprintf (val, " %s", buffer);
                   val[len] = 0;
#else
                   sprintf (val, " %s", csqlvalue (i));
#endif
				   *vlen = len;
			       break; 
		         case SQLSMINT :
					 len = 4;
					 *vlen = len;
					 strcpy (format, " %4hd");
#ifdef ODBC
                     short sfeld;
                     sfeld = 0;
                     get_colvalue (i + 1, cursor, SQLCSHORT, (short *) &sfeld, sizeof (short));
                     sprintf (val, format, sfeld);
#else
                     sprintf (val, format, ssqlvalue (i));
#endif
					 break;
#ifndef ODBC
		         case SQLSERIAL :
#endif
		         case SQLINT :
					 len = 8;
					 *vlen = len;
					 strcpy (format, "%8ld");
#ifdef ODBC
                     short lfeld;
                     lfeld = 0l;
                     get_colvalue (i + 1, cursor, SQLCLONG, (long *) &lfeld, sizeof (long));
                     sprintf (val, format, lfeld);
#else
                     sprintf (val, format, isqlvalue (i));
#endif
					 break;
		         case SQLDECIMAL :
		         case SQLFLOAT :
		            nkomma = len % 256;
			        len = len / 256 + 2; 
					 *vlen = len;
					sprintf (format, " %c%hd.%hdlf", '%', len, nkomma);
#ifdef ODBC
                     double dfeld;
                     dfeld = 0.0;
                     get_colvalue (i + 1, cursor, SQLCDOUBLE, (double *) &dfeld, sizeof (double));
                     sprintf (val, format, dfeld);
#else
                    sprintf (val, format, dsqlvalue (i));
#endif
					 break;
		         case SQLDATE :
					 len = 10;
					 *vlen = len;
#ifdef ODBC
                     DATE_STRUCT  dat;
                     get_colvalue (i + 1, cursor, SQLCDATE, (DATE_STRUCT *) &dat, sizeof (DATE_STRUCT));
                     sprintf (val, "%02hd.%02hd.%04hd", dat.day, dat.month, dat.year);
#else
					 dlong_to_asc (isqlvalue (i), val); 
#endif
					 break;
	  }
#endif
}


struct SHOWW
{
	  int Cursor;
	  char Caption [256];
};

static BOOL SqlActive = FALSE;

DWORD WINAPI ShowSqlListThread (LPVOID lpParameter)
{
	  CHOISE Choise (80, 20, FALSE);
	  int i;
	  int dbfields;
	  char feld[81];
	  int len;
	  int nkomma;
	  int typ;
	  int pos;
	  char val [512];
	  char feldbuff[512];
	  char vfeldbuff[512];
	  char buffer [4096];
	  char vbuffer [4096];
	  struct SHOWW *showw;

	  SqlActive = TRUE;
	  showw = (SHOWW *) lpParameter;

	  if (showw == NULL)
	  {
	      SqlActive = FALSE;
		  return 0;
	  }

	  int cursor = showw->Cursor;
	  close_sql (cursor);
	  cursor = prepare_sql (showw->Caption);
	  if (cursor == -1)
	  {
	      SqlActive = FALSE;
		  delete showw;
		  return 0;
	  }

	  Choise.SetMenSelect (FALSE);
	  Choise.Setstyle (WS_VISIBLE | WS_POPUP | 
		               WS_THICKFRAME | WS_CAPTION |
					   WS_SYSMENU | 
                       WS_MAXIMIZEBOX |
                       WS_MINIMIZEBOX);
	  Choise.SetCaption (showw->Caption);
//	  EnableWindow (Mtxt->GethWnd (), FALSE);
//      Choise.OpenWindow (hMainInst, hMainWindow);
      Choise.OpenWindow (hMainInst, NULL);
	  strcpy (buffer, "");
	  strcpy (vbuffer, "");
	  open_sql (cursor);
#ifdef ODBC
      dbfields = get_colanz (cursor);
#else
      dbfields = sqlfields ();
#endif
      for (i = 0; i < dbfields; i ++)
	  {
#ifdef ODBC
          typ = get_coltype (i + 1, cursor);
          len = get_collength (i + 1, cursor);
#else
		  typ = sqltyp (i);
		  len = sqllen (i);
#endif
		  switch (typ)
		  {
		         case SQLCHAR :
		         case SQLSMFLOAT :
			       break; 
		         case SQLSMINT :
					 len = 4;
					 break;
#ifndef ODBC
		         case SQLSERIAL :
#endif
		         case SQLINT :
					 len = 8;
					 break;
		         case SQLDECIMAL :
		         case SQLFLOAT :
		            nkomma = len % 256;
			        len = len / 256 + 2; 
					break;
		         case SQLDATE :
					 len = 10;
					 break;
#ifdef ODBC
		         case SQLTIME :
					 len = 8;
					 break;
		         case SQLTIMESTAMP :
					 len = 19;
					 break;
#endif
		  }
          len += 6; 
		  len = min (80, len);
#ifdef ODBC
          char colname [128];
          strcpy (feld, get_colname (i + 1,cursor,colname));
#else
          strcpy (feld, sqlname (i));
#endif
		  memset (feldbuff,' ', 512);
		  memset (vfeldbuff,' ', 512);
		  pos = max (0, ((int) len - (int) strlen (feld)) / 2);
		  memcpy (&feldbuff[pos],feld, strlen(feld));
		  feldbuff[len] = 0;
		  vfeldbuff[len - 1] = '1';
		  vfeldbuff[len] = 0;
		  if (i) 
		  {
			  strcat (buffer, " ");
			  strcat (vbuffer, " ");
		  }
		  strcat (buffer, feldbuff);
		  strcat (vbuffer, vfeldbuff);
	  }
	  Choise1->VLines (vbuffer, 3);
	  Choise.InsertCaption (buffer);

	  while (fetch_sql (cursor) == 0)
	  {
         strcpy (buffer, "");
         for (i = 0; i < dbfields; i ++)
		 {
#ifdef ODBC
		          FillSqlValue (i, val, &len, cursor);
#else
		          FillSqlValue (i, val, &len);
#endif
		          len += 6;
		          len = min (80, len);
		          memset (feldbuff,' ', 512);
		          pos = max (0, (len - strlen (val)) / 2);
		          memcpy (&feldbuff[pos],val, strlen(val));
		          feldbuff[len] = 0;
		          if (i) 
				  {
			                  strcat (buffer, " ");
				  }
		          strcat (buffer, feldbuff);
		 }
         Choise.InsertRecord (buffer);
	  }

	  Choise.ProcessMessages ();
	  Choise.DestroyWindow ();
	  EnableWindow (Mtxt->GethWnd (), TRUE);
	  close_sql (cursor);
	  delete showw;
      SqlActive = FALSE;
	  return 0;
}


void ShowSqlList (int cursor)
{
	  CHOISE Choise (80, 20, FALSE);
	  int i;
	  int dbfields;
	  char feld[81];
	  int len;
	  int nkomma;
	  int typ;
	  int pos;
	  char val [512];
	  char feldbuff[512];
	  char vfeldbuff[512];
	  char buffer [4096];
	  char vbuffer [4096];

	  Choise.SetMenSelect (FALSE);
	  Choise.Setstyle (WS_VISIBLE | WS_POPUP | 
		               WS_THICKFRAME | WS_CAPTION |
					   WS_SYSMENU | 
                       WS_MAXIMIZEBOX |
                       WS_MINIMIZEBOX);
	  Choise.SetCaption (sqltext);
//	  EnableWindow (Mtxt->GethWnd (), FALSE);
//      Choise.OpenWindow (hMainInst, hMainWindow);
      Choise.OpenWindow (hMainInst, Mtxt->GethWnd ());
	  strcpy (buffer, "");
	  strcpy (vbuffer, "");
	  open_sql (cursor);
#ifdef ODBC
      dbfields = get_colanz (cursor);
#else
      dbfields = sqlfields ();
#endif
      for (i = 0; i < dbfields; i ++)
	  {
#ifdef ODBC
          typ = get_coltype (i + 1, cursor);
          len = get_collength (i + 1, cursor);
#else
		  typ = sqltyp (i);
		  len = sqllen (i);
#endif
		  switch (typ)
		  {
		         case SQLCHAR :
		         case SQLSMFLOAT :
			       break; 
		         case SQLSMINT :
					 len = 4;
					 break;
#ifndef ODBC
		         case SQLSERIAL :
#endif
		         case SQLINT :
					 len = 8;
					 break;
		         case SQLDECIMAL :
		         case SQLFLOAT :
		            nkomma = len % 256;
			        len = len / 256 + 2; 
					break;
		         case SQLDATE :
					 len = 10;
					 break;
#ifdef ODBC
		         case SQLTIME :
					 len = 8;
					 break;
		         case SQLTIMESTAMP :
					 len = 19;
					 break;
#endif
		  }
          len += 6; 
		  len = min (80, len);
#ifdef ODBC
          char colname [128];
          strcpy (feld, get_colname (i + 1,cursor,colname));
#else
          strcpy (feld, sqlname (i));
#endif
		  memset (feldbuff,' ', 512);
		  memset (vfeldbuff,' ', 512);
		  pos = max (0, ((int) len - (int) strlen (feld)) / 2);
		  memcpy (&feldbuff[pos],feld, strlen(feld));
		  feldbuff[len] = 0;
		  vfeldbuff[len - 1] = '1';
		  vfeldbuff[len] = 0;
		  if (i) 
		  {
			  strcat (buffer, " ");
			  strcat (vbuffer, " ");
		  }
		  strcat (buffer, feldbuff);
		  strcat (vbuffer, vfeldbuff);
	  }
	  Choise1->VLines (vbuffer, 3);
	  Choise.InsertCaption (buffer);

	  while (fetch_sql (cursor) == 0)
	  {
         strcpy (buffer, "");
         for (i = 0; i < dbfields; i ++)
		 {
#ifdef ODBC
		          FillSqlValue (i, val, &len, cursor);
#else
		          FillSqlValue (i, val, &len);
#endif
		          len += 6;
		          len = min (80, len);
		          memset (feldbuff,' ', 512);
		          pos = max (0, (len - strlen (val)) / 2);
		          memcpy (&feldbuff[pos],val, strlen(val));
		          feldbuff[len] = 0;
		          if (i) 
				  {
			                  strcat (buffer, " ");
				  }
		          strcat (buffer, feldbuff);
		 }
         Choise.InsertRecord (buffer);
	  }

	  Choise.ProcessMessages ();
	  Choise.DestroyWindow ();
	  EnableWindow (Mtxt->GethWnd (), TRUE);
}


void ShowSqlListEx (int cursor)
{
#ifndef ODBC
	  ShowSqlList (cursor);
	  return;
#endif

      unsigned long threadId;
	  HANDLE hThread;
	  struct SHOWW *Cursor;

	  if (SqlActive)
	  {
		  close_sql (cursor);
		  return;
	  }
	  Cursor = new struct SHOWW;
	  Cursor->Cursor = cursor;
	  strcpy (Cursor->Caption, sqltext); 
      hThread = CreateThread(0, 0, ShowSqlListThread, Cursor, 0, &threadId);
}

void ExecuteSqlCursor (int cursor)
{
	  int dsqlstatus;
	  CHOISE Choise (80, 20, FALSE);
	  char buffer [4096];

	  Choise.SetMenSelect (FALSE);
	  Choise.Setstyle (WS_VISIBLE | WS_POPUP | 
		               WS_THICKFRAME | WS_CAPTION |
					   WS_SYSMENU | 
                       WS_MAXIMIZEBOX |
                       WS_MINIMIZEBOX);
	  Choise.SetCaption (sqltext);
	  EnableWindow (Mtxt->GethWnd (), FALSE);
      Choise.OpenWindow (hMainInst, hMainWindow);
	  strcpy (buffer, "");
      dsqlstatus = execute_curs (cursor);
	  Choise1->VLines ("", 0);
	  sprintf (buffer, " SQL Ausgabe");
	  Choise.InsertCaption (buffer);

	  if (dsqlstatus < 0)
	  {
		      sprintf (buffer, "  SQL-Fehler %d", dsqlstatus); 
              Choise.InsertRecord ("");
              Choise.InsertRecord (buffer);
	  }
	  else
	  {
#ifndef BIWAK
		      sprintf (buffer, "  Ge�nderte S�tze %d", sqlerror[2]);
#else
		      sprintf (buffer, "  Ge�nderte S�tze %d", dsqlstatus);
#endif
              Choise.InsertRecord ("");
              Choise.InsertRecord (buffer);
	  }

	  Choise.ProcessMessages ();
	  Choise.DestroyWindow ();
	  EnableWindow (Mtxt->GethWnd (), TRUE);
	  Choise.SetMenSelect (TRUE);
	  close_sql (cursor);
}


void RunSql (void)
/**
Sql.Kommand ausfuehren.
**/
{
//	   int dsqlstatus;
	   int cursor;
       int anz;

	   cursor = prepare_sql (sqltext);
	   if (cursor < 0)
	   {
		   print_mess (2, "Fehler %d bei %s", sqlstatus, sqltext);
		   return;
	   }

       anz = wsplit (sqltext, " ");
       if (anz < 1) return;
	   if (strstr (wort[0], "select") == 0)
	   {
           ExecuteSqlCursor (cursor);
 	       close_sql (cursor);
		   return;
	   }
#ifndef BIWAK
       ShowSqlListEx (cursor); 
#endif
//	   close_sql (cursor);
}

#ifndef BIWAK
extern "C" 
{
        extern short sql_mode;
}
#else
        short sql_mode;
#endif

void GetSql (void)
/**
Texte aus EditFeld holen.
**/
{
	   char *EText;
	   char *Text;
	   char *txt;
	   char *k;
	   static char seps [] = {13,10};
       short sql_save;

       sql_save = sql_mode;
	   EText = Mtxt->GetText ();
	   if (strlen (EText) == 0) return;
	   Text = new char [strlen (EText) + 2];
	   if (Text == NULL) 
       {
           return;
       }

       sql_mode = 2;
       strcpy (Text, EText);
	   SqlText = Text;
	   sqltext = new char [4096];
	   txt = strtok (Text, seps);
	   strcpy (sqltext, txt);
	   while (txt)
	   {
	       txt = strtok (NULL, seps);
		   if (txt == NULL) break;
		   strcat (sqltext, " ");
		   strcat (sqltext, txt);
		   clipped (sqltext);
           if (k = strchr (sqltext, ';'))
		   {
			   *k = 0;
			   RunSql ();
	           strcpy (sqltext, k + 1);
		   }
	   }
	   if (strlen (sqltext) > 0)
	   {
	        RunSql ();
	   }
	   delete (Text);
	   delete (sqltext);
       sql_mode = sql_save;
}


DWORD WINAPI InputSQLThread ( LPVOID lpParameter)
{
	   
  	  int cx, cy;
	  form *scurrent;
	  static BOOL EdOK = FALSE;

	  if (sqltext == NULL) sqltext = "";
	  scurrent = current_form;
      cx = 80;
      cy = 20;
      Mtxt = new MTXT (cx, cy, "SQL Befehl ", SqlText.GetBuffer (), NULL);
      Mtxt->OpenWindow (hMainInst, NULL);

	  while (Mtxt->ProcessMessages ())
	  {
		     GetSql ();
	  }

      Mtxt->DestroyWindow ();
	  delete Mtxt;
	  current_form = scurrent;
	  return 0;
}


void InputSQL (void)
{
  	  int cx, cy;
	  form *scurrent;
	  static BOOL EdOK = FALSE;

	  scurrent = current_form;
      cx = 80;
      cy = 20;
      Mtxt = new MTXT (cx, cy, "SQL Befehl ", SqlText.GetBuffer (), NULL);
      Mtxt->OpenWindow (hMainInst, hMainWindow);
	  EnableWindow (hMainWindow, FALSE);

	  while (Mtxt->ProcessMessages ())
	  {
		     GetSql ();
	  }

	  SetActiveWindow (hMainWindow);
	  EnableWindow (hMainWindow, TRUE);
      Mtxt->DestroyWindow ();
	  delete Mtxt;
	  current_form = scurrent;
}

void InputSQLEx (void)
{
#ifndef ODBC
	   InputSQL ();
	   return;
#endif

      unsigned long threadId;
	  HANDLE hThread;

      hThread = CreateThread(0, 0, InputSQLThread, 0, 0, &threadId);
}


void close_cursor (void)
{
      if (dbcursor > -1)
      {
          close_sql (dbcursor);
          dbcursor = -1;
      }
      if (updcursor > -1)
      {
          close_upd_cursor ();
          updcursor = -1;
      }
}

int sql_error (void)
{
      print_mess (2, "SQLfehler %d", sqlstatus);
      Closedbase (DataBase); 
      ExitProcess (1);
      return (0);
}

int RunDde (char *DdeDialProg,
            char *DdeDialService,
            char *DdeDialTopic,
            char *DdeDialItem,
            char *buffer)
{

          DdeDial *ddeDial = new DdeDial;
          if (DdeDialProg != NULL)
          {
              ddeDial->SetServer (DdeDialProg);
          }
          if (DdeDialService != NULL)
          {
              ddeDial->SetService (DdeDialService);
          }
          if (DdeDialTopic != NULL)
          {
              ddeDial->SetTopic (DdeDialTopic);
          }
          if (DdeDialItem != NULL)
          {
              ddeDial->SetItem (DdeDialItem);
          }
          ddeDial->Call (buffer);
          delete ddeDial;
          return 0;
}

void SaveRect (void) //LAC-94
{
	    char *etc;
		char rectname [512];
		RECT rect;
		FILE *fp;
		int xfull, yfull;

		if (positions_mode == FALSE) return;
		etc = getenv ("BWSETC");
		if (etc == NULL) return;

          xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
          yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
		GetWindowRect (hMainWindow, &rect);


		sprintf (rectname, "%s\\%s.rct", etc,"dbinfo");
		GetWindowRect (hMainWindow, &rect);
		if (rect.right < xfull)  
		{
				fp = fopen (rectname, "w");
				fprintf (fp, "left    %d\n", rect.left);
				fprintf (fp, "top     %d\n", rect.top);
				fprintf (fp, "right   %d\n", rect.right - rect.left);
				fprintf (fp, "bottom  %d\n", rect.bottom - rect.top);
				fclose (fp);
		}
}

void GetRect (void) //LAC-94
{
	    char *etc;
		char buffer [512];
		FILE *fp;
		int anz;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

			sprintf (buffer, "%s\\%s.rct", etc,"dbinfo");
			fp = fopen (buffer, "r");
			if (fp == NULL) return ;
			while (fgets (buffer, 511, fp))
			{
				cr_weg (buffer);
				anz = wsplit (buffer, " ");
				if (anz < 2) continue;
				if (strcmp (wort[0], "left") == 0)
				{
					wx = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "top") == 0)
				{
					wy = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "right") == 0)
				{
					wcx = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "bottom") == 0)
				{
					wcy = atoi (wort[1]);
				}
                WinSet = TRUE;
			}
			fclose (fp);
}
