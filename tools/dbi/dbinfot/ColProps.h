#ifndef _COLPROPS_DEF
#define _COLPROPS_DEF

class ColProps
{
   private :
       int  Colnr;
       char ColName [42];
       char Caption [81];
       int  Length;
       char Picture[12];
       char Macro [256];

   public :
       ColProps ()
       {
           Colnr = 0;
           strcpy (ColName, "");
           strcpy (Caption, "");
           Length = 0;
           strcpy (Picture, "");
           strcpy (Macro, "");
       }

       ColProps (int Colnr,
                 char * DbName,
                 char * Caption,
                 int Length,
                 char * Picture,
                 char * Macro)
       {
           this->Colnr       = Colnr;
           strncpy (this->ColName,  ColName, 41);
           this->ColName[41] = 0;
           strncpy (this->Caption, Caption, 80);
           this->Caption[80] = 0;
           this->Length      = Length;
           strncpy (this->Picture, Picture, 11);
           this->Picture[11] = 0;
           strncpy (this->Macro,  Macro, 255);
           this->Macro[255] = 0;
       }

       int GetColnr (void)
       {
           return Colnr;
       }

       void SetColnr (int Colnr)
       {
           this->Colnr       = Colnr;
       }

       char *GetColName (void)
       {
           clipped (ColName);
           return ColName;
       }

       void SetColName (char *ColName)
       {
           clipped (ColName);
           strncpy (this->ColName, ColName, 41);
           this->ColName[41] = 0;
       }

       char *GetCaption (void)
       {
           clipped (Caption);
           return Caption;
       }

       void SetCaption (char *Caption)
       {
           clipped (Caption);
           strncpy (this->Caption, Caption, 80);
           this->Caption[80] = 0;
       }

       int GetLength (void)
       {
           return Length;
       }

       void SetLength (int Length)
       {
           this->Length = Length;
       }

       char *GetPicture (void)
       {
           return Picture;
       }

       void SetPicture (char *Picture)
       {
           strncpy (this->Picture, Picture, 11);
           this->Picture[11] = 0;
       }
       char *GetMacro (void)
       {
           return Macro;
       }

       void SetMacro (char *Macro)
       {
           strncpy (this->Macro, Macro, 255);
           this->Macro[255] = 0;
       }
};

#endif