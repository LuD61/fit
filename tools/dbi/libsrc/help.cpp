#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "help.h" 
#ifdef BIWAK
#include "conf_env.h" 
#endif

static int StdSize = STDSIZE;

static mfont dlgfont = {
                         "Arial", 100, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};


static mfont dlgposfont = {
                         "Arial", 100, 0, 0,
                         WHITECOL,
                         GRAYCOL,
                         0,
                         NULL};


static mfont *Font = &dlgposfont;

COLORREF HELP::Background = DKYELLOWCOL;

HELP::HELP (char *Item, HINSTANCE hInstance, HWND hMainWindow) : 
           DLG (-1, -1, 40, 10, Item, 120, 0)
{
         this->hInstance   = hInstance;
         this->hMainWindow = hMainWindow;
         HelpName = "texte.cmd";
         Init (Item);
}

HELP::HELP (char *Item, HINSTANCE hInstance, HWND hMainWindow, BOOL Mode) : 
           DLG (-1, -1, 40, 10, Item, 120, 0)
{
         this->hInstance   = hInstance;
         this->hMainWindow = hMainWindow;
         HelpName = "texte.cmd";
         if (Mode == INITHELP)
         {
                Init (Item);
         }
         else if (Mode == INITCOMM)
         {
                InitComment (Item);
         }
}

HELP::HELP (char *HelpName, char *Item, HINSTANCE hInstance, HWND hMainWindow) : 
           DLG (-1, -1, 40, 10, Item, 120, 0)
{
         this->hInstance   = hInstance;
         this->hMainWindow = hMainWindow;
         this->HelpName = HelpName;
         Init (Item);
}

HELP::HELP (char *HelpName, char *Item, HINSTANCE hInstance, HWND hMainWindow, BOOL Mode) : 
           DLG (-1, -1, 40, 10, Item, 120, 0)
{
         this->hInstance   = hInstance;
         this->hMainWindow = hMainWindow;
         this->HelpName = HelpName;
         if (Mode == INITHELP)
         {
                Init (Item);
         }
         else if (Mode == INITCOMM)
         {
                InitComment (Item);
         }
}

HELP::~HELP ()
{
          int i; 

          if (_fWork != NULL)
          {
              for (i = 0; _fWork[i] != NULL; i ++)
              {
                  delete _fWork[i];
              }

              delete _fWork;
          }
          if (fWork != NULL)
          {
              delete fWork;
          }

          if (_fHelp != NULL)
          {
              for (i = 0; i < 1; i ++)
              {
                  delete _fHelp[i];
              }

              delete _fHelp;
          }
          if (fHelp != NULL)
          {
              delete fHelp;
          }

          if (head != NULL)
          {
              delete head;
          }

          if (itemdata != NULL)
          {
              delete itemdata;
          }

          if (Rows != NULL)
          {
              for (i = 0; Rows[i] != NULL; i ++)
              {
                  delete Rows[i];
              }
              delete Rows;
          }
}
          

char *HELP::GetItpos (char *Item)
{
          int i;
          char It [21];
          char *p;

          clipped (Item);
          p = head;
          for (i = 0; i < items; i ++, p += 28)
          {
              memcpy (It, p, 20);
              It[20] = 0;
              clipped (It);
              if (strcmp (It, Item) == 0)
              {
                  return p;
              }
          }
          return NULL;
}


BOOL HELP::MakeCform (int anz)
{
         int i;
         int len;
         int x;
         int y;

         cx = 0;
         cy = anz;
         for (i = 0; i < anz; i ++)
         {
             clipped (wort[i]);
             len = strlen (wort[i]);
             if (len > cx)
             {
                 cx = len;
             }
             Rows[i] = new char [len + 1];
             if (Rows[i] == NULL) return FALSE;
             strcpy (Rows[i], wort[i]);
         }
         Rows[i] = NULL;

         _fWork = new CFIELD *[anz + 1];
         if (_fWork == NULL) return FALSE;
         x = 1; 
         y = 0;

         for (i = 0; i < anz; i ++)
         {
             _fWork[i] = new CFIELD ("", Rows[i], 0, 0, x, y, NULL, "", CDISPLAYONLY,
                                                  500, &dlgposfont, 0, TRANSPARENT);
             if (_fWork[i] == NULL)
             {
                 return FALSE;
             }
             y ++;
         }
         _fWork[i] = NULL;
         fWork = new CFORM (anz, _fWork);

         if (fWork == NULL)
         {
             return FALSE;
         }

         _fHelp = new CFIELD *[1];
         if (_fHelp == NULL)
         {
             return FALSE;
         }
         _fHelp[0] = new CFIELD ("fWork", (CFORM *) fWork, 0, 0, -1, -1, NULL, "",
                     CFFORM, 500, &dlgposfont, 0, 0);
         if (_fHelp [0] == NULL)
         {
             return FALSE;
         }
  
         fHelp = new CFORM (1, _fHelp);
         if (fHelp == NULL)
         {
             return FALSE;
         }
         return TRUE;
}

char * HELP::GetItem (char *Item, char *text)
{
         if ((Rows == NULL) || (Rows[0] == NULL))
         {
             text[0] = 0;
         }
         else
         {
             strcpy (text, Rows[0]);
         }
         return text;
}


BOOL HELP::ReadItem (char *HelpName, char *Item)
{
         FILE *fp;
         char *etc;
         char fname [512];
         char *itpos;
         int anz;
         unsigned char t[2] = " ";

         t[0] = (unsigned char) 0xFF;
#ifdef BIWAK
         etc = getenv ("ETC");
#else
         etc = getenv ("BWSETC");
#endif
         if (etc == NULL) return NULL;
         fp = NULL;
         sprintf (fname, "%s\\%s", etc, HelpName);
         fp = fopen (fname, "rb");
         if (fp == NULL)
         {
             return FALSE;
         }
         hlen = 0;

         if (fread ((long *) &hlen, 1, sizeof (long), fp) <= 0)
         {
             fclose (fp);
             return FALSE;
         }
         head = new char [hlen];
         if (head == NULL)
         {
             fclose (fp);
             return FALSE;
         }

         hlen -= sizeof (long);
         items = hlen / 28;
         if (fread (head, 1, hlen, fp) < (unsigned int) hlen)
         {
             fclose (fp);
             return FALSE;
         }
         itpos = GetItpos (Item);
         if (itpos == NULL)
         {
             fclose (fp);
             return FALSE;
         }
         itpos += 20;
         memcpy (&offset, (long *) itpos, sizeof (long));
         itpos += sizeof (long);
         memcpy (&dlen, (long *) itpos, sizeof (long));
         fseek (fp, offset, 0);
         itemdata = new char [dlen + 1];
         if (itemdata == NULL)
         {
             fclose (fp);
             return FALSE;
         }

         if (fread (itemdata, 1, dlen, fp) == 0)
         if (itemdata == NULL)
         {
             fclose (fp);
             return FALSE;
         }
         fclose (fp);
         itemdata [dlen] = 0; 
         anz = wsplit (itemdata, (char *) t);
         if (anz <= 0)
         {
             return FALSE;
         }
         Rows = new char *[anz + 1];
         return MakeCform (anz);
}


void HELP::InitComment (char *Item)
{
         _fWork   = NULL; 
         fWork    = NULL; 
         _fHelp    = NULL; 
         fHelp    = NULL; 
         Rows     = NULL; 
         head     = NULL; 
         itemdata = NULL; 
         BOOL HelpOK;

         HelpOK = ReadItem (HelpName,Item);
         if ((HelpOK == FALSE) &&
              strcmp (HelpName, "texte.cmd"))
         {
             HelpOK = ReadItem ("texte.cmd", Item);
         }
         if (HelpOK == FALSE)
         {
             HelpOK = ReadItem (HelpName, "nocomm");
         }
         if (HelpOK == FALSE)
         {
             if (ReadItem ("texte.cmd", "nocomm") == FALSE)
             {
                    return;
             }
         }
}

void HELP::Init (char *Item)
{
         _fWork   = NULL; 
         fWork    = NULL; 
         _fHelp    = NULL; 
         fHelp    = NULL; 
         Rows     = NULL; 
         head     = NULL; 
         itemdata = NULL; 
         BOOL HelpOK;

         HelpOK = ReadItem (HelpName,Item);
         if ((HelpOK == FALSE) &&
              strcmp (HelpName, "texte.cmd"))
         {
             HelpOK = ReadItem ("texte.cmd", Item);
         }
         if (HelpOK == FALSE)
         {
             if (ReadItem (HelpName, "nohelp") == FALSE)
             {
                    return;
             }
         }
         SetStyle (WS_VISIBLE | WS_POPUP | WS_BORDER);
         SetWinBackground (Background);
         if (GetBValue (Background) > 128 && 
             GetRValue (Background) > 128 &&
             GetGValue (Background) > 128)
         {
              dlgposfont.FontColor = BLACKCOL;
         }
         SetDialog (fHelp);
         SetDimension (cx, cy);
         OpenWindow (hInstance, hMainWindow);
         ProcessMessages ();
         DestroyWindow ();
}

BOOL HELP::OnKey (int taste)
{
        DestroyWindow ();
        return TRUE;
}

BOOL HELP::OnKeyReturn ()
{
        DestroyWindow ();
        return TRUE;
}


BOOL HELP::OnKey3 ()
{
        DestroyWindow ();
        return TRUE;
}

BOOL HELP::OnLButtonDown (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (this->hWnd != hWnd)
        {
            DestroyWindow ();
            return TRUE;
        }
        return FALSE;
}

/*
BOOL HELP::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return FALSE;
}
*/


BOOL HELP::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {
              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (LOWORD (wParam) == OK_CTL)
              {
//                  return TestHELP ();
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL HELP::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == SC_CLOSE)
        {
             ExitProcess (0);
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL HELP::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        PostQuitMessage (0);
        return TRUE;
}

void HELP::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}

void HELP::SetBackground (COLORREF Col)
{
          Background = Col;
}



