#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "we_pos.h"

struct WE_POS we_pos, we_pos_null;

void WE_POS_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &we_pos.mdn, 1, 0);
            ins_quest ((char *) &we_pos.fil, 1, 0);
            ins_quest ((char *) &we_pos.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_pos.lief, 0, 17);
            ins_quest ((char *) &we_pos.a, 3, 0);
            ins_quest ((char *) &we_pos.blg_typ, 0, 2);
            ins_quest ((char *) &we_pos.int_pos, 2, 0);
    out_quest ((char *) &we_pos.a,3,0);
    out_quest ((char *) &we_pos.abw_lief_proz,3,0);
    out_quest ((char *) &we_pos.abw_rab_betr,3,0);
    out_quest ((char *) &we_pos.abw_rab_nat,3,0);
    out_quest ((char *) &we_pos.abw_rab_proz,3,0);
    out_quest ((char *) &we_pos.abw_zusch_proz,3,0);
    out_quest ((char *) &we_pos.akv,2,0);
    out_quest ((char *) &we_pos.alarm_prio,2,0);
    out_quest ((char *) &we_pos.anz_einh,3,0);
    out_quest ((char *) &we_pos.bearb,2,0);
    out_quest ((char *) &we_pos.best_blg,2,0);
    out_quest ((char *) &we_pos.best_me,3,0);
    out_quest ((char *) we_pos.best_zuord_kz,0,2);
    out_quest ((char *) we_pos.blg_typ,0,2);
    out_quest ((char *) we_pos.diff_kz,0,2);
    out_quest ((char *) &we_pos.fil,1,0);
    out_quest ((char *) &we_pos.hbk_dat,2,0);
    out_quest ((char *) &we_pos.inh,3,0);
    out_quest ((char *) &we_pos.int_pos,2,0);
    out_quest ((char *) we_pos.lief,0,17);
    out_quest ((char *) we_pos.lief_best,0,17);
    out_quest ((char *) we_pos.lief_rech_nr,0,17);
    out_quest ((char *) &we_pos.lief_s,2,0);
    out_quest ((char *) &we_pos.mdn,1,0);
    out_quest ((char *) &we_pos.me,3,0);
    out_quest ((char *) &we_pos.me_einh,1,0);
    out_quest ((char *) &we_pos.mwst,1,0);
    out_quest ((char *) &we_pos.p_num,1,0);
    out_quest ((char *) we_pos.pers_nam,0,9);
    out_quest ((char *) we_pos.pr_aend_kz,0,2);
    out_quest ((char *) &we_pos.pr_ek,3,0);
    out_quest ((char *) &we_pos.pr_ek_nto,3,0);
    out_quest ((char *) &we_pos.pr_vk,3,0);
    out_quest ((char *) we_pos.qua_kz,0,2);
    out_quest ((char *) &we_pos.rab_eff,3,0);
    out_quest ((char *) we_pos.rab_kz,0,2);
    out_quest ((char *) we_pos.sa_kz,0,2);
    out_quest ((char *) we_pos.skto_kz,0,2);
    out_quest ((char *) &we_pos.tara,3,0);
    out_quest ((char *) &we_pos.tara_proz,3,0);
    out_quest ((char *) &we_pos.we_txt,2,0);
    out_quest ((char *) we_pos.sti_pro_kz,0,2);
    out_quest ((char *) we_pos.me_kz,0,2);
    out_quest ((char *) &we_pos.a_krz,2,0);
    out_quest ((char *) &we_pos.we_kto,2,0);
    out_quest ((char *) &we_pos.buch_kz,1,0);
    out_quest ((char *) &we_pos.pr_fil_ek,3,0);
    out_quest ((char *) &we_pos.lager,2,0);
            cursor = prepare_sql ("select we_pos.a,  "
"we_pos.abw_lief_proz,  we_pos.abw_rab_betr,  we_pos.abw_rab_nat,  "
"we_pos.abw_rab_proz,  we_pos.abw_zusch_proz,  we_pos.akv,  "
"we_pos.alarm_prio,  we_pos.anz_einh,  we_pos.bearb,  we_pos.best_blg,  "
"we_pos.best_me,  we_pos.best_zuord_kz,  we_pos.blg_typ,  "
"we_pos.diff_kz,  we_pos.fil,  we_pos.hbk_dat,  we_pos.inh,  "
"we_pos.int_pos,  we_pos.lief,  we_pos.lief_best,  we_pos.lief_rech_nr,  "
"we_pos.lief_s,  we_pos.mdn,  we_pos.me,  we_pos.me_einh,  we_pos.mwst,  "
"we_pos.p_num,  we_pos.pers_nam,  we_pos.pr_aend_kz,  we_pos.pr_ek,  "
"we_pos.pr_ek_nto,  we_pos.pr_vk,  we_pos.qua_kz,  we_pos.rab_eff,  "
"we_pos.rab_kz,  we_pos.sa_kz,  we_pos.skto_kz,  we_pos.tara,  "
"we_pos.tara_proz,  we_pos.we_txt,  we_pos.sti_pro_kz,  we_pos.me_kz,  "
"we_pos.a_krz,  we_pos.we_kto,  we_pos.buch_kz,  we_pos.pr_fil_ek,  "
"we_pos.lager from we_pos "

#line 32 "we_pos.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   a = ? "
                                  "and   blg_typ = ? "
                                  "and   int_pos = ?");

    ins_quest ((char *) &we_pos.a,3,0);
    ins_quest ((char *) &we_pos.abw_lief_proz,3,0);
    ins_quest ((char *) &we_pos.abw_rab_betr,3,0);
    ins_quest ((char *) &we_pos.abw_rab_nat,3,0);
    ins_quest ((char *) &we_pos.abw_rab_proz,3,0);
    ins_quest ((char *) &we_pos.abw_zusch_proz,3,0);
    ins_quest ((char *) &we_pos.akv,2,0);
    ins_quest ((char *) &we_pos.alarm_prio,2,0);
    ins_quest ((char *) &we_pos.anz_einh,3,0);
    ins_quest ((char *) &we_pos.bearb,2,0);
    ins_quest ((char *) &we_pos.best_blg,2,0);
    ins_quest ((char *) &we_pos.best_me,3,0);
    ins_quest ((char *) we_pos.best_zuord_kz,0,2);
    ins_quest ((char *) we_pos.blg_typ,0,2);
    ins_quest ((char *) we_pos.diff_kz,0,2);
    ins_quest ((char *) &we_pos.fil,1,0);
    ins_quest ((char *) &we_pos.hbk_dat,2,0);
    ins_quest ((char *) &we_pos.inh,3,0);
    ins_quest ((char *) &we_pos.int_pos,2,0);
    ins_quest ((char *) we_pos.lief,0,17);
    ins_quest ((char *) we_pos.lief_best,0,17);
    ins_quest ((char *) we_pos.lief_rech_nr,0,17);
    ins_quest ((char *) &we_pos.lief_s,2,0);
    ins_quest ((char *) &we_pos.mdn,1,0);
    ins_quest ((char *) &we_pos.me,3,0);
    ins_quest ((char *) &we_pos.me_einh,1,0);
    ins_quest ((char *) &we_pos.mwst,1,0);
    ins_quest ((char *) &we_pos.p_num,1,0);
    ins_quest ((char *) we_pos.pers_nam,0,9);
    ins_quest ((char *) we_pos.pr_aend_kz,0,2);
    ins_quest ((char *) &we_pos.pr_ek,3,0);
    ins_quest ((char *) &we_pos.pr_ek_nto,3,0);
    ins_quest ((char *) &we_pos.pr_vk,3,0);
    ins_quest ((char *) we_pos.qua_kz,0,2);
    ins_quest ((char *) &we_pos.rab_eff,3,0);
    ins_quest ((char *) we_pos.rab_kz,0,2);
    ins_quest ((char *) we_pos.sa_kz,0,2);
    ins_quest ((char *) we_pos.skto_kz,0,2);
    ins_quest ((char *) &we_pos.tara,3,0);
    ins_quest ((char *) &we_pos.tara_proz,3,0);
    ins_quest ((char *) &we_pos.we_txt,2,0);
    ins_quest ((char *) we_pos.sti_pro_kz,0,2);
    ins_quest ((char *) we_pos.me_kz,0,2);
    ins_quest ((char *) &we_pos.a_krz,2,0);
    ins_quest ((char *) &we_pos.we_kto,2,0);
    ins_quest ((char *) &we_pos.buch_kz,1,0);
    ins_quest ((char *) &we_pos.pr_fil_ek,3,0);
    ins_quest ((char *) &we_pos.lager,2,0);
            sqltext = "update we_pos set we_pos.a = ?,  "
"we_pos.abw_lief_proz = ?,  we_pos.abw_rab_betr = ?,  "
"we_pos.abw_rab_nat = ?,  we_pos.abw_rab_proz = ?,  "
"we_pos.abw_zusch_proz = ?,  we_pos.akv = ?,  we_pos.alarm_prio = ?,  "
"we_pos.anz_einh = ?,  we_pos.bearb = ?,  we_pos.best_blg = ?,  "
"we_pos.best_me = ?,  we_pos.best_zuord_kz = ?,  we_pos.blg_typ = ?,  "
"we_pos.diff_kz = ?,  we_pos.fil = ?,  we_pos.hbk_dat = ?,  "
"we_pos.inh = ?,  we_pos.int_pos = ?,  we_pos.lief = ?,  "
"we_pos.lief_best = ?,  we_pos.lief_rech_nr = ?,  we_pos.lief_s = ?,  "
"we_pos.mdn = ?,  we_pos.me = ?,  we_pos.me_einh = ?,  we_pos.mwst = ?,  "
"we_pos.p_num = ?,  we_pos.pers_nam = ?,  we_pos.pr_aend_kz = ?,  "
"we_pos.pr_ek = ?,  we_pos.pr_ek_nto = ?,  we_pos.pr_vk = ?,  "
"we_pos.qua_kz = ?,  we_pos.rab_eff = ?,  we_pos.rab_kz = ?,  "
"we_pos.sa_kz = ?,  we_pos.skto_kz = ?,  we_pos.tara = ?,  "
"we_pos.tara_proz = ?,  we_pos.we_txt = ?,  we_pos.sti_pro_kz = ?,  "
"we_pos.me_kz = ?,  we_pos.a_krz = ?,  we_pos.we_kto = ?,  "
"we_pos.buch_kz = ?,  we_pos.pr_fil_ek = ?,  we_pos.lager = ? "

#line 41 "we_pos.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   a = ? "
                                  "and   blg_typ = ? "
                                  "and   int_pos = ?"; 


            ins_quest ((char *) &we_pos.mdn, 1, 0);
            ins_quest ((char *) &we_pos.fil, 1, 0);
            ins_quest ((char *) &we_pos.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_pos.lief, 0, 17);
            ins_quest ((char *) &we_pos.a, 2, 0);
            ins_quest ((char *) &we_pos.blg_typ, 0, 2);
            ins_quest ((char *) &we_pos.int_pos, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &we_pos.mdn, 1, 0);
            ins_quest ((char *) &we_pos.fil, 1, 0);
            ins_quest ((char *) &we_pos.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_pos.lief, 0, 17);
            ins_quest ((char *) &we_pos.a, 3, 0);
            ins_quest ((char *) &we_pos.blg_typ, 0, 2);
            ins_quest ((char *) &we_pos.int_pos, 2, 0);
            test_upd_cursor = prepare_sql ("select a from we_pos "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   a = ? "
                                  "and   blg_typ = ? "
                                  "and int_pos = ?");
            ins_quest ((char *) &we_pos.mdn, 1, 0);
            ins_quest ((char *) &we_pos.fil, 1, 0);
            ins_quest ((char *) &we_pos.lief_rech_nr, 0, 17);
            ins_quest ((char *) &we_pos.lief, 0, 17);
            ins_quest ((char *) &we_pos.a, 3, 0);
            ins_quest ((char *) &we_pos.blg_typ, 0, 2);
            ins_quest ((char *) &we_pos.int_pos, 2, 0);
            del_cursor = prepare_sql ("delete from we_pos "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief_rech_nr = ? "
                                  "and   lief = ? "
                                  "and   a = ? "
                                  "and   blg_typ = ? "
                                  "and   int_pos = ?");
    ins_quest ((char *) &we_pos.a,3,0);
    ins_quest ((char *) &we_pos.abw_lief_proz,3,0);
    ins_quest ((char *) &we_pos.abw_rab_betr,3,0);
    ins_quest ((char *) &we_pos.abw_rab_nat,3,0);
    ins_quest ((char *) &we_pos.abw_rab_proz,3,0);
    ins_quest ((char *) &we_pos.abw_zusch_proz,3,0);
    ins_quest ((char *) &we_pos.akv,2,0);
    ins_quest ((char *) &we_pos.alarm_prio,2,0);
    ins_quest ((char *) &we_pos.anz_einh,3,0);
    ins_quest ((char *) &we_pos.bearb,2,0);
    ins_quest ((char *) &we_pos.best_blg,2,0);
    ins_quest ((char *) &we_pos.best_me,3,0);
    ins_quest ((char *) we_pos.best_zuord_kz,0,2);
    ins_quest ((char *) we_pos.blg_typ,0,2);
    ins_quest ((char *) we_pos.diff_kz,0,2);
    ins_quest ((char *) &we_pos.fil,1,0);
    ins_quest ((char *) &we_pos.hbk_dat,2,0);
    ins_quest ((char *) &we_pos.inh,3,0);
    ins_quest ((char *) &we_pos.int_pos,2,0);
    ins_quest ((char *) we_pos.lief,0,17);
    ins_quest ((char *) we_pos.lief_best,0,17);
    ins_quest ((char *) we_pos.lief_rech_nr,0,17);
    ins_quest ((char *) &we_pos.lief_s,2,0);
    ins_quest ((char *) &we_pos.mdn,1,0);
    ins_quest ((char *) &we_pos.me,3,0);
    ins_quest ((char *) &we_pos.me_einh,1,0);
    ins_quest ((char *) &we_pos.mwst,1,0);
    ins_quest ((char *) &we_pos.p_num,1,0);
    ins_quest ((char *) we_pos.pers_nam,0,9);
    ins_quest ((char *) we_pos.pr_aend_kz,0,2);
    ins_quest ((char *) &we_pos.pr_ek,3,0);
    ins_quest ((char *) &we_pos.pr_ek_nto,3,0);
    ins_quest ((char *) &we_pos.pr_vk,3,0);
    ins_quest ((char *) we_pos.qua_kz,0,2);
    ins_quest ((char *) &we_pos.rab_eff,3,0);
    ins_quest ((char *) we_pos.rab_kz,0,2);
    ins_quest ((char *) we_pos.sa_kz,0,2);
    ins_quest ((char *) we_pos.skto_kz,0,2);
    ins_quest ((char *) &we_pos.tara,3,0);
    ins_quest ((char *) &we_pos.tara_proz,3,0);
    ins_quest ((char *) &we_pos.we_txt,2,0);
    ins_quest ((char *) we_pos.sti_pro_kz,0,2);
    ins_quest ((char *) we_pos.me_kz,0,2);
    ins_quest ((char *) &we_pos.a_krz,2,0);
    ins_quest ((char *) &we_pos.we_kto,2,0);
    ins_quest ((char *) &we_pos.buch_kz,1,0);
    ins_quest ((char *) &we_pos.pr_fil_ek,3,0);
    ins_quest ((char *) &we_pos.lager,2,0);
            ins_cursor = prepare_sql ("insert into we_pos (a,  "
"abw_lief_proz,  abw_rab_betr,  abw_rab_nat,  abw_rab_proz,  "
"abw_zusch_proz,  akv,  alarm_prio,  anz_einh,  bearb,  best_blg,  best_me,  "
"best_zuord_kz,  blg_typ,  diff_kz,  fil,  hbk_dat,  inh,  int_pos,  lief,  lief_best,  "
"lief_rech_nr,  lief_s,  mdn,  me,  me_einh,  mwst,  p_num,  pers_nam,  pr_aend_kz,  "
"pr_ek,  pr_ek_nto,  pr_vk,  qua_kz,  rab_eff,  rab_kz,  sa_kz,  skto_kz,  tara,  "
"tara_proz,  we_txt,  sti_pro_kz,  me_kz,  a_krz,  we_kto,  buch_kz,  pr_fil_ek,  "
"lager) "

#line 90 "we_pos.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?)"); 

#line 92 "we_pos.rpp"
}
int WE_POS_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

