#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "textwin.h" 
#include "conf_env.h" 

static int StdSize = STDSIZE;

static mfont dlgfont = {
                         "Arial", 100, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};


static mfont dlgposfont = {
                         "Arial", 100, 0, 0,
                         WHITECOL,
                         GRAYCOL,
                         0,
                         NULL};


static mfont *Font = &dlgposfont;

COLORREF TEXTWIN::Background = DKYELLOWCOL;

TEXTWIN::TEXTWIN (char *Text, HINSTANCE hInstance, HWND hMainWindow, int mx, int my) : 
           DLG (-1, -1, 40, 10, "", 120, 0)
{
         this->hInstance   = hInstance;
         this->hMainWindow = hMainWindow;
         this->mx = mx;
         this->my = my;
         Init (Text);
}

TEXTWIN::~TEXTWIN ()
{
          int i; 

          if (_fWork != NULL)
          {
              for (i = 0; _fWork[i] != NULL; i ++)
              {
                  delete _fWork[i];
              }

              delete _fWork;
          }
          if (fWork != NULL)
          {
              delete fWork;
          }

          if (_fText != NULL)
          {
              for (i = 0; i < 1; i ++)
              {
                  delete _fText[i];
              }

              delete _fText;
          }
          if (fText != NULL)
          {
              delete fText;
          }

          if (head != NULL)
          {
              delete head;
          }

          if (itemdata != NULL)
          {
              delete itemdata;
          }

          if (Rows != NULL)
          {
              for (i = 0; Rows[i] != NULL; i ++)
              {
                  delete Rows[i];
              }
              delete Rows;
          }
}
          


BOOL TEXTWIN::MakeCform (int anz)
{
         int i;
         int len;
         int x;
         int y;

         cx = 0;
         cy = anz;
         for (i = 0; i < anz; i ++)
         {
             clipped (wort[i]);
             len = strlen (wort[i]);
             if (len > cx)
             {
                 cx = len;
             }
             Rows[i] = new char [len + 1];
             if (Rows[i] == NULL) return FALSE;
             strcpy (Rows[i], wort[i]);
         }
         Rows[i] = NULL;

         _fWork = new CFIELD *[anz + 1];
         if (_fWork == NULL) return FALSE;
         x = 1; 
         y = 0;

         for (i = 0; i < anz; i ++)
         {
             _fWork[i] = new CFIELD ("", Rows[i], 0, 0, x, y, NULL, "", CDISPLAYONLY,
                                                  500, &dlgposfont, 0, TRANSPARENT);
             if (_fWork[i] == NULL)
             {
                 return FALSE;
             }
             y ++;
         }
         _fWork[i] = NULL;
         fWork = new CFORM (anz, _fWork);

         if (fWork == NULL)
         {
             return FALSE;
         }

         _fText = new CFIELD *[1];
         if (_fText == NULL)
         {
             return FALSE;
         }
         _fText[0] = new CFIELD ("fWork", (CFORM *) fWork, 0, 0, -1, -1, NULL, "",
                     CFFORM, 500, &dlgposfont, 0, 0);
         if (_fText [0] == NULL)
         {
             return FALSE;
         }
  
         fText = new CFORM (1, _fText);
         if (fText == NULL)
         {
             return FALSE;
         }
         return TRUE;
}


void TEXTWIN::Init (char *Text)
{
         int anz;
         RECT rect;

         _fWork   = NULL; 
         fWork    = NULL; 
         _fText    = NULL; 
         fText    = NULL; 
         Rows     = NULL; 
         head     = NULL; 
         itemdata = NULL; 

         anz = wsplit (Text, "\n");
         if (anz <= 0)
         {
             return;
         }

         Rows = new char *[anz + 1];
         if (MakeCform (anz) == FALSE) return;
         SetStyle (WS_VISIBLE | WS_POPUP | WS_BORDER);
         SetWinBackground (Background);
         if (GetBValue (Background) > 128 && 
             GetRValue (Background) > 128 &&
             GetGValue (Background) > 128)
         {
              dlgposfont.FontColor = BLACKCOL;
         }
         SetDialog (fText);
         this->x = mx;
         this->y = my;
         SetDimension (cx, cy);
         GetClientRect (hMainWindow, &rect);
         while (this->x + this->cx > rect.right)
         {
//             this->x -= this->cx + 2;
             this->x --;
         }
         OpenWindow (hInstance, hMainWindow);
}

void TEXTWIN::GetText (char *Text, int maxchars, int row)
{
         if (row >= fWork->GetFieldanz ()) return;

         strncpy (Text, Rows[row], maxchars);
         Text[maxchars] = 0;
}

BOOL TEXTWIN::OnRButtonUp (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (hWnd != NULL)
        {
            DestroyWindow ();
            return TRUE;
        }
        return FALSE;
}

BOOL TEXTWIN::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        return TRUE;
}

void TEXTWIN::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}

void TEXTWIN::SetBackground (COLORREF Col)
{
          Background = Col;
}



