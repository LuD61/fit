#include <windows.h>
#include <string.h>
#include "strfkt.h"
#include "colbut.h"

#define MAXWINCOLS 0x1000

LONG FAR PASCAL ColButtonProcEx(HWND,UINT,WPARAM,LPARAM);
static void RegisterColWinM (char *, COLORREF, WNDPROC);
static void BuColMinusA (ColButton *);
static void PrintColBorder (ColButton *, HWND);
static void PrintColBorderDown (ColButton *, HWND);

static BOOL MousePressed = 0;
static HWND MousehWnd = NULL;
static HWND MouseMovehWnd = NULL;
static int  TimerOn = 0;
static HWND TimehWnd = NULL;
static COLORREF wincolors [MAXWINCOLS];
static WNDPROC colorprocs [MAXWINCOLS];
static int wincolanz;
static HINSTANCE MainInst;
static BOOL ColBorder = FALSE;
static HWND loosefocus = NULL;
static HWND AktColFocus = NULL;

static int BorderColor = NULL;
static int BorderLtColor = NULL;

static BOOL ColDefEnabled = FALSE;

void EnableColDef (BOOL b)
{
    ColDefEnabled = b;
}

BOOL IsEnabledColDef (void)
{
     return ColDefEnabled;
}



void SetColBorderM (BOOL mode)
{
	     ColBorder = min (1, mode);
}

void RegisterColWinM (char *ColClass, COLORREF col, WNDPROC wproc)
/**
Farbwindow regostrieren.
**/
{
         int i;
         WNDCLASS wc;

         if (wproc == NULL)
         {
                     wproc = ColButtonProcEx;
         }
         for (i = 0; i < wincolanz; i ++)
         {
                     if ((COLORREF) col == (COLORREF) wincolors [i])
                     {
                               sprintf (ColClass, "winM%d", i);
                               if (wproc == colorprocs[i]) break;
                     }
         }
     //     i = wincolanz;
         sprintf (ColClass, "winM%d", i);
         wincolors[i]  = col;
         colorprocs[i] = wproc;
         if (i < wincolanz) return;

         if (i == wincolanz && wincolanz < MAXWINCOLS - 1) wincolanz ++;

         wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
         wc.lpfnWndProc   =  wproc;
         wc.cbClsExtra    =  0;
         wc.cbWndExtra    =  0;
         wc.hInstance     =  MainInst;
         wc.hIcon         =  LoadIcon (MainInst, "ROSIICON");
         wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
         wc.hbrBackground =  CreateSolidBrush (col);
         wc.lpszMenuName  =  "";
         wc.lpszClassName =  ColClass;

        RegisterClass(&wc);
}


struct CLB
{
	HWND hWnd;
	ColButton *CuB;
	HFONT hFont;
	DWORD BuId;
	BOOL Def;
};

typedef struct CLB COLB;

static COLB **colb = NULL;
static int colbanz = 0;

static COLORREF aktback;
static COLORREF aktvor;


static void BuColMinusA (ColButton *Cub)
/**
**/
{
         aktvor  = Cub->Color; 
         aktback = Cub->BkColor; 
         Cub->Color = BLACKCOL;
         Cub->BkColor = YELLOWCOL;
}

static void BuColPlusA (ColButton *Cub)
/**
**/
{
         Cub->Color = aktvor;
         Cub->BkColor = aktback;
}


static void BuColMinus (ColButton *Cub)
/**
**/
{
         static int minus = 120;
         int blue, green, red;

         blue = GetBValue (Cub->BkColor);
         red  = GetRValue (Cub->BkColor);
         green = GetGValue (Cub->BkColor);
         if (blue > minus) blue -= minus;
         if (red > minus) red -= minus;
         if (green > minus) green -= minus;
         Cub->BkColor = RGB (red, green, blue);
}

static void BuColPlus (ColButton *Cub)
/**
**/
{
         static int plus = 120;
         int blue, green, red;

         blue = GetBValue (Cub->BkColor);
         red  = GetRValue (Cub->BkColor);
         green = GetGValue (Cub->BkColor);
         if (blue > 0) blue += plus;
         if (red > 0) red += plus;
         if (green > 0) green += plus;
         Cub->BkColor = RGB (red, green, blue);
}

static void BuColMinusH (ColButton *Cub)
/**
**/
{
         static int minus = 50;
         int blue, green, red;

         blue = GetBValue (Cub->BkColor);
         red  = GetRValue (Cub->BkColor);
         green = GetGValue (Cub->BkColor);
         if (blue + minus < 255)  blue += minus;
         if (red + minus < 255)   red += minus;
         if (green + minus < 255) green += minus;
         Cub->BkColor = RGB (red, green, blue);
}


static void BuColPlusH (ColButton *Cub)
/**
**/
{
         static int plus = 50;
         int blue, green, red;

         blue = GetBValue (Cub->BkColor);
         red  = GetRValue (Cub->BkColor);
         green = GetGValue (Cub->BkColor);
         if (blue < 255) blue -= plus;
         if (red < 255) red -= plus;
         if (green < 255) green -= plus;
         Cub->BkColor = RGB (red, green, blue);
}


HWND GetColDef (void)
{
	int i;

	for (i = 0; i < colbanz; i ++)
	{
		if (colb[i]->Def) return colb[i]->hWnd;
	}
	return NULL;

}

void InitColDef (void)
{
	int i;

	for (i = 0; i < colbanz; i ++)
	{
		if (colb[i]->Def)
		{
		         colb[i]->Def = FALSE;
	             InvalidateRect (colb[i]->hWnd, NULL, TRUE);
	             UpdateWindow (colb[i]->hWnd);
		}
	}
}

void SetColDef (HWND hWnd)
{
	int i;

	for (i = 0; i < colbanz; i ++)
	{
		if (hWnd == colb[i]->hWnd) break;
	}
	InitColDef ();
	if (i == colbanz) return;
	colb[i]->Def = TRUE;
	InvalidateRect (colb[i]->hWnd, NULL, TRUE);
	UpdateWindow (colb[i]->hWnd);
}


HWND CreateColButton (HWND hMain, ColButton *CoB, int x, int y, int cx, int cy, HFONT hFont, 
					  HINSTANCE hMainInst, DWORD ID)
{
	      HWND hWnd;
		  int editstyle;
          char ColClass [20];

		  MainInst = hMainInst;
          if (CoB->aktivate == -1)
          {
                           RegisterColWinM (ColClass, LTGRAYCOL, ColButtonProcEx);
          }
          else
          {
                           RegisterColWinM (ColClass, CoB->BkColor, ColButtonProcEx);
          }
          editstyle = 0;
          if (CoB->icon)
          {
                         cx += 2;
                         cy += 2;
                         editstyle = WS_DLGFRAME
                         ;
          }

          hWnd = CreateWindow (   // "ColButton",
                                        ColClass,
                                        "",
                                        WS_CHILD | WS_VISIBLE |
                                        BS_OWNERDRAW | editstyle,
                                        // WS_DLGFRAME,
                                        x, y,
                                        cx, cy,
                                        hMain,
                                       (HMENU) ID,
                                        hMainInst,
                                        NULL);
/*
           if (CoB->aktivate < 0)
           {
                 EnableWindow (hWnd, FALSE);
           }
           SendMessage (hWnd,
                         WM_SETFONT, (WPARAM) hFont, 0);
*/
		   if (colb == NULL)
		   {
			   colb = new COLB *[0x1000];
			   if (colb == NULL) return hWnd;
		   }
		   colb[colbanz] = new COLB; 
		   if (colb[colbanz] == NULL) return hWnd;
		   colb[colbanz]->hWnd   = hWnd;
		   colb[colbanz]->CuB    = CoB;
		   colb[colbanz]->hFont  = hFont;
		   colb[colbanz]->BuId   = 0;
		   colb[colbanz]->Def    = 0;
		   if (colbanz < 0x1000 - 1) colbanz ++;
           return hWnd;
}

void DestroyColButton (HWND hWnd)
{
	int i;

	if (colb == NULL) return;
	for (i = 0; colbanz; i ++)
	{
		if (hWnd == colb[i]->hWnd) break;
	}
	if (i == colbanz) return;
  
	delete colb[i];
	colbanz --;
	for (; i < colbanz; i ++)
	{
		colb[i] = colb[i + 1];
	}
    colb[i] = NULL;
}

static void PressBorder (HDC hdc, RECT *rect, COLORREF BkColor)
/**
Rahmen um Fenster.
**/
{ 
         static HPEN hPenBlack = NULL;
         static HPEN hPenWhite;
         static HPEN hPenGray;

         static HPEN hPenLtBlue;
         static HPEN hPenBlue;

         static HPEN hPenLtRed;
         static HPEN hPenRed;
    
         static HPEN hPenLtGreen;
         static HPEN hPenGreen;

         static HPEN hPenLtGray;

         HPEN hBorderPen;
         HPEN hBorderLtPen;
         HPEN oldpen;

		 int blue, red, green;

		 if (ColBorder == FALSE)
		 {
			 BkColor = LTGRAYCOL;
		 }

         blue = GetBValue (BkColor);
         red  = GetRValue (BkColor);
 		 green = GetGValue (BkColor);

         if (hPenBlack == NULL)
         {
                       hPenBlack  = CreatePen (PS_SOLID, 1, BLACKCOL);
                       hPenWhite  = CreatePen (PS_SOLID, 1, WHITECOL);
                       hPenGray   = CreatePen (PS_SOLID, 1, GRAYCOL);

                       hPenLtBlue = CreatePen (PS_SOLID, 1, RGB (0, 120, 255));
                       hPenBlue   = CreatePen (PS_SOLID, 1, RGB ( 0, 0, 120));

                       hPenLtRed  = CreatePen (PS_SOLID, 1, RGB (255, 120, 0));
                       hPenRed    = CreatePen (PS_SOLID, 1, RGB ( 120, 0, 0));

                       hPenLtGreen  = CreatePen (PS_SOLID, 1, RGB (0, 255, 120));
                       hPenGreen    = CreatePen (PS_SOLID, 1, RGB ( 0, 120, 0));

                       hPenLtGray  = CreatePen (PS_SOLID, 1, RGB (150, 150, 150));
         }
		 if (BorderLtColor !=NULL)
         {
                       hBorderLtPen = CreatePen (PS_SOLID, 1, BorderLtColor);
                       hBorderPen   = CreatePen (PS_SOLID, 1, BorderColor);
         }

                        
         if (hPenGray == NULL)
         {
                       disp_mess ("Fehler bei CreatePen", 2);
                       ExitProcess (1);
		 }
         oldpen =  SelectObject (hdc, hPenGray);

		 if (BkColor == BLUECOL)
		 {
                       SelectObject (hdc, hPenBlue);
		 }
		 else if (BkColor == REDCOL)
		 {
                       SelectObject (hdc, hPenRed);
		 }
		 else if (BkColor == GREENCOL)
		 {
                       SelectObject (hdc, hPenGreen);
		 }
		 else if (BkColor == GRAYCOL)
		 {
                       SelectObject (hdc, hPenGray);
		 }
		 else if (BorderLtColor !=NULL)
		 {
                       SelectObject (hdc, hBorderPen);
		 }
		 else
		 {
                       SelectObject (hdc, hPenGray);
		 }

         MoveToEx (hdc, 0, 0, NULL);
         LineTo (hdc, rect->right - 1, 0);
         MoveToEx (hdc, 0, 0, NULL);
         LineTo (hdc, 0, rect->bottom);

		 if (BkColor == BLUECOL);
		 else if (BkColor == REDCOL);
		 else if (BkColor == GREENCOL);
		 else if (BorderLtColor !=NULL);
		 else
		 {
                       SelectObject (hdc, hPenBlack);
		 }

         MoveToEx (hdc, 1, 1, NULL);
         LineTo (hdc, rect->right - 1, 1);
         MoveToEx (hdc, 1, 1, NULL);
         LineTo (hdc, 1, rect->bottom - 1);

		 if (BkColor == BLUECOL)
		 {
                       SelectObject (hdc, hPenLtBlue);
		 }
		 else if (BkColor == REDCOL)
		 {
                       SelectObject (hdc, hPenLtRed);
		 }
		 else if (BkColor == GREENCOL)
		 {
                       SelectObject (hdc, hPenLtGreen);
		 }
/*
		 else if (BkColor == GRAYCOL)
		 {
                       SelectObject (hdc, hPenLtGray);
		 }
*/
		 else if (BorderLtColor !=NULL)
		 {
                       SelectObject (hdc, hBorderLtPen);
		 }
		 else
		 {
                       SelectObject (hdc, hPenWhite);
		 }
         MoveToEx (hdc, 1, rect->bottom - 1, NULL);
         LineTo (hdc, rect->right, rect->bottom - 1);
         MoveToEx (hdc, rect->right - 1, 1 , NULL);
         LineTo (hdc, rect->right - 1, rect->bottom - 1);

/*
         DeleteObject (hPenWhite);
         DeleteObject (hPenBlack);
         DeleteObject (hPenGray);
*/
         SelectObject (hdc, oldpen);
		 if (BorderLtColor !=NULL)
         {
             DeleteObject (hBorderPen);
             DeleteObject (hBorderLtPen);
         }
}


static void UnPressBorder (HDC hdc, RECT *rect, COLORREF BkColor)
/**
Rahmen um Fenster.
**/
{ 
         static HPEN hPenBlack = NULL;
         static HPEN hPenWhite;
         static HPEN hPenGray;

         static HPEN hPenLtBlue;
         static HPEN hPenBlue;

         static HPEN hPenLtRed;
         static HPEN hPenRed;

         static HPEN hPenLtGreen;
         static HPEN hPenGreen;

         static HPEN hPenLtGray;

         HPEN hBorderPen;
         HPEN hBorderLtPen;
         HPEN oldpen;

		 int blue, red, green;

		 if (ColBorder == FALSE)
		 {
			 BkColor = LTGRAYCOL;
		 }

         blue = GetBValue (BkColor);
         red  = GetRValue (BkColor);
 		 green = GetGValue (BkColor);

         if (hPenBlack == NULL)
         {
                     hPenBlack   = CreatePen (PS_SOLID, 1, BLACKCOL);
                     hPenWhite   = CreatePen (PS_SOLID, 1, WHITECOL);
                     hPenGray    = CreatePen (PS_SOLID, 1, GRAYCOL);

                     hPenLtBlue  = CreatePen (PS_SOLID, 1, RGB (0, 120, 255));
                     hPenBlue    = CreatePen (PS_SOLID, 1, RGB ( 0, 0, 120));

                     hPenLtRed  = CreatePen (PS_SOLID, 1, RGB (255, 120, 0));
                     hPenRed    = CreatePen (PS_SOLID, 1, RGB ( 120, 0, 0));

                     hPenLtGreen  = CreatePen (PS_SOLID, 1, RGB (120, 255, 0));
                     hPenGreen    = CreatePen (PS_SOLID, 1, RGB ( 0, 120, 0));

                     hPenLtGray  = CreatePen (PS_SOLID, 1, RGB (150, 150, 150));
         }

		 if (BorderLtColor !=NULL)
         {
                       hBorderLtPen = CreatePen (PS_SOLID, 1, BorderLtColor);
                       hBorderPen   = CreatePen (PS_SOLID, 1, BorderColor);
         }

         if (hPenGray == NULL)
         {
                          disp_mess ("Fehler bei CreatePen", 2);
                          ExitProcess (1);
         }

         oldpen =  SelectObject (hdc, hPenGray);

		 if (BkColor == BLUECOL)
		 {
                    SelectObject (hdc, hPenBlue);

                    MoveToEx (hdc, rect->right- 3, 2, NULL);
                    LineTo (hdc, rect->right - 3, rect->bottom - 3);

                    MoveToEx (hdc, rect->right- 2, 2, NULL);
                    LineTo (hdc, rect->right - 2, rect->bottom - 3);
                    MoveToEx (hdc, 2, rect->bottom - 3, NULL);
                    LineTo (hdc, rect->right, rect->bottom - 3);
		 }
		 else if (BkColor == REDCOL)
		 {
                    SelectObject (hdc, hPenRed);
                    MoveToEx (hdc, rect->right- 2, 2, NULL);
                    LineTo (hdc, rect->right - 2, rect->bottom - 3);
                    MoveToEx (hdc, 2, rect->bottom - 3, NULL);
                    LineTo (hdc, rect->right, rect->bottom - 3);
		 }
		 else if (BkColor == GREENCOL)
		 {
                    SelectObject (hdc, hPenGreen);
                    MoveToEx (hdc, rect->right- 2, 2, NULL);
                    LineTo (hdc, rect->right - 2, rect->bottom - 3);
                    MoveToEx (hdc, 2, rect->bottom - 3, NULL);
                    LineTo (hdc, rect->right, rect->bottom - 3);
		 }
/*
		 else if (BkColor == GRAYCOL)
		 {
                    SelectObject (hdc, hPenGray);
                    MoveToEx (hdc, rect->right- 2, 2, NULL);
                    LineTo (hdc, rect->right - 2, rect->bottom - 3);
                    MoveToEx (hdc, 2, rect->bottom - 3, NULL);
                    LineTo (hdc, rect->right, rect->bottom - 3);
		 }
*/
		 else if (BorderLtColor !=NULL)
		 {
                    SelectObject (hdc, hBorderPen);
                    MoveToEx (hdc, rect->right- 2, 2, NULL);
                    LineTo (hdc, rect->right - 2, rect->bottom - 3);
                    MoveToEx (hdc, 2, rect->bottom - 3, NULL);
                    LineTo (hdc, rect->right, rect->bottom - 3);
		 }
		 else
		 {
                    SelectObject (hdc, hPenGray);
		 }

         MoveToEx (hdc, rect->right- 2, 1, NULL);
         LineTo (hdc, rect->right - 2, rect->bottom - 2);
         MoveToEx (hdc, 1, rect->bottom -2, NULL);
         LineTo (hdc, rect->right, rect->bottom - 2);


		 if (BkColor == BLUECOL);
		 else if (BkColor == REDCOL);
		 else if (BkColor == GREENCOL);
		 else
		 {
                    SelectObject (hdc, hPenBlack);
		 }

         MoveToEx (hdc, rect->right - 1, 0, NULL);
         LineTo (hdc, rect->right - 1, rect->bottom);
         MoveToEx (hdc, 0, rect->bottom - 1, NULL);
         LineTo (hdc, rect->right, rect->bottom - 1);

		 if (BkColor == BLUECOL)
		 {
                   SelectObject (hdc, hPenLtBlue);
                   MoveToEx (hdc, 1, 0, NULL);
                   LineTo (hdc, 1, rect->bottom - 2);
                   MoveToEx (hdc, 2, 1, NULL);
                   LineTo (hdc, 2, rect->bottom - 3);
                   MoveToEx (hdc, 1, 1, NULL);
                   LineTo (hdc, rect->right - 3, 1);
		 }
		 else if (BkColor == REDCOL)
		 {
                   SelectObject (hdc, hPenLtRed);
                   MoveToEx (hdc, 1, 0, NULL);
                   LineTo (hdc, 1, rect->bottom - 2);
                   MoveToEx (hdc, 2, 1, NULL);
                   LineTo (hdc, 2, rect->bottom - 3);
		 }
		 else if (BkColor == GREENCOL)
		 {
                   SelectObject (hdc, hPenLtGreen);
                   MoveToEx (hdc, 1, 0, NULL);
                   LineTo (hdc, 1, rect->bottom - 2);
                   MoveToEx (hdc, 2, 1, NULL);
                   LineTo (hdc, 2, rect->bottom - 3);
		 }
/*
		 else if (BkColor == GRAYCOL)
		 {
                   SelectObject (hdc, hPenLtGray);
                   MoveToEx (hdc, 1, 0, NULL);
                   LineTo (hdc, 1, rect->bottom - 2);
                   MoveToEx (hdc, 2, 1, NULL);
                   LineTo (hdc, 2, rect->bottom - 3);
		 }
*/
		 else if (BorderLtColor !=NULL)
		 {
                   SelectObject (hdc, hBorderLtPen);
                   MoveToEx (hdc, 1, 0, NULL);
                   LineTo (hdc, 1, rect->bottom - 2);
                   MoveToEx (hdc, 2, 1, NULL);
                   LineTo (hdc, 2, rect->bottom - 3);
		 }
		 else
		 {
                   SelectObject (hdc, hPenWhite);
		 }

         MoveToEx (hdc, 0, 0, NULL);
         LineTo (hdc, 0, rect->bottom -1);
         MoveToEx (hdc, 0, 0, NULL);
         LineTo (hdc, rect->right - 1, 0);

/*
         DeleteObject (hPenWhite);
         DeleteObject (hPenBlack);
         DeleteObject (hPenGray);
*/
         SelectObject (hdc, oldpen);
		 if (BorderLtColor !=NULL)
         {
             DeleteObject (hBorderPen);
             DeleteObject (hBorderLtPen);
         }
}

static void UnPressBorderSm (HDC hdc, RECT *rect, COLORREF BkColor)
/**
Rahmen um Fenster.
**/
{ 
         static HPEN hPenBlack = NULL;
         static HPEN hPenWhite;
         static HPEN hPenGray;

         static HPEN hPenLtBlue;
         static HPEN hPenBlue;

         static HPEN hPenLtRed;
         static HPEN hPenRed;

         static HPEN hPenLtGreen;
         static HPEN hPenGreen;

         static HPEN hPenLtGray;

         HPEN hBorderPen;
         HPEN hBorderLtPen;
         HPEN oldpen;

		 int blue, red, green;

		 if (ColBorder == FALSE)
		 {
			 BkColor = LTGRAYCOL;
		 }

         blue = GetBValue (BkColor);
         red  = GetRValue (BkColor);
 		 green = GetGValue (BkColor);

         if (hPenBlack == NULL)
         {
                     hPenBlack   = CreatePen (PS_SOLID, 1, BLACKCOL);
                     hPenWhite   = CreatePen (PS_SOLID, 1, WHITECOL);
                     hPenGray    = CreatePen (PS_SOLID, 1, GRAYCOL);

                     hPenLtBlue  = CreatePen (PS_SOLID, 1, RGB (0, 120, 255));
                     hPenBlue    = CreatePen (PS_SOLID, 1, RGB ( 0, 0, 120));

                     hPenLtRed  = CreatePen (PS_SOLID, 1, RGB (255, 120, 0));
                     hPenRed    = CreatePen (PS_SOLID, 1, RGB ( 120, 0, 0));

                     hPenLtGreen  = CreatePen (PS_SOLID, 1, RGB (120, 255, 0));
                     hPenGreen    = CreatePen (PS_SOLID, 1, RGB ( 0, 120, 0));

                     hPenLtGray  = CreatePen (PS_SOLID, 1, RGB (150, 150, 150));
         }

		 if (BorderLtColor !=NULL)
         {
                       hBorderLtPen = CreatePen (PS_SOLID, 1, BorderLtColor);
                       hBorderPen   = CreatePen (PS_SOLID, 1, BorderColor);
         }

         if (hPenGray == NULL)
         {
                          disp_mess ("Fehler bei CreatePen", 2);
                          ExitProcess (1);
         }

         oldpen =  SelectObject (hdc, hPenGray);

/*
		 if (BkColor == BLUECOL);
		 else if (BkColor == REDCOL);
		 else if (BkColor == GREENCOL);
		 else
		 {
                    SelectObject (hdc, hPenBlack);
		 }
*/

         MoveToEx (hdc, rect->right - 1, 0, NULL);
         LineTo (hdc, rect->right - 1, rect->bottom);
         MoveToEx (hdc, 0, rect->bottom - 1, NULL);
         LineTo (hdc, rect->right, rect->bottom - 1);

		 if (BkColor == BLUECOL)
		 {
                   SelectObject (hdc, hPenLtBlue);
                   MoveToEx (hdc, 1, 0, NULL);
                   LineTo (hdc, 1, rect->bottom - 2);
		 }
		 else if (BkColor == REDCOL)
		 {
                   SelectObject (hdc, hPenLtRed);
                   MoveToEx (hdc, 1, 0, NULL);
                   LineTo (hdc, 1, rect->bottom - 2);
		 }
		 else if (BkColor == GREENCOL)
		 {
                   SelectObject (hdc, hPenLtGreen);
                   MoveToEx (hdc, 1, 0, NULL);
                   LineTo (hdc, 1, rect->bottom - 2);
		 }
		 else if (BorderLtColor !=NULL)
		 {
                   SelectObject (hdc, hBorderLtPen);
                   MoveToEx (hdc, 1, 0, NULL);
                   LineTo (hdc, 1, rect->bottom - 2);
		 }
		 else
		 {
                   SelectObject (hdc, hPenWhite);
		 }

         MoveToEx (hdc, 0, 0, NULL);
         LineTo (hdc, 0, rect->bottom -1);
         MoveToEx (hdc, 0, 0, NULL);
         LineTo (hdc, rect->right - 1, 0);

         SelectObject (hdc, oldpen);
		 if (BorderLtColor !=NULL)
         {
             DeleteObject (hBorderPen);
             DeleteObject (hBorderLtPen);
         }
}

static void PrintColTextUnakt (HDC hdc, int x, int y, char *text, int len)
/**
Text fuer inaktiven ColButton schreiben.
**/
{
         int i;
         char OneChar [2];
         SIZE size;
         
         SetBkMode (hdc, TRANSPARENT);
         SetBkColor (hdc, LTGRAYCOL);
         for (i = 0; i < len; i ++)
         {
                   if (text[i] == (char) 0) break;
                   if (text[i] == '&') continue;
                   OneChar [0] = text[i];
                   OneChar [1] = (char) 0;  
                   SetTextColor   (hdc, WHITECOL);
                   TextOut (hdc, x + 1, y + 1, OneChar, 1);
                   SetTextColor   (hdc, GRAYCOL);
                   TextOut (hdc, x, y, OneChar, 1);
                   GetTextExtentPoint32 (hdc, OneChar, 1, &size);
                   x += size.cx;
         }
         return;
}

static void PrintColIcon (HWND hWnd, HDC hdc,
                         HICON hIcon, int x, int y)
{
        RECT rect;

        GetClientRect (hWnd, &rect);
        if (x == -1)
/* Spalte zentrieren               */
        {
                  x = (rect.right - 32) / 2;
        }

        if (y == -1)
/* Zeile zentrieren               */
        {
                  y = (rect.bottom - 32) / 2;
        }
        DrawIcon (hdc, x, y, hIcon);
}

 
static void PrintBitmap (HWND hWnd, HDC hdc,
                         HBITMAP hbr, int x, int y, DWORD mode)
{
        BITMAP bm;
        HDC    hdcMemory;
        HBITMAP  hbmOld;
        RECT rect;

        GetClientRect (hWnd, &rect);
        GetObject (hbr, sizeof (BITMAP), &bm);

        if (x == -1)
/* Spalte zentrieren               */
        {
                  x = (rect.right - bm.bmWidth) / 2;
        }

        if (y == -1)
/* Zeile zentrieren               */
        {
                  y = (rect.bottom - bm.bmHeight) / 2;
        }

        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        BitBlt (hdc, x, y, bm.bmWidth, bm.bmHeight,
                         hdcMemory,0, 0, mode);
        
        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
}


static void SetSpezColor (COLORREF Color, COLORREF BkColor, HDC hdc)
/**
Spezialfarbe fuer Hot-Key-Buchstaben setzen.
**/
{
      int red;
      int blue;
      int green;
      int bkred;
      int bkblue;
      int bkgreen;

      blue = GetBValue (Color);
      red  = GetRValue (Color);
      green = GetGValue (Color);
      bkblue = GetBValue (BkColor);
      bkred  = GetRValue (BkColor);
      bkgreen = GetGValue (BkColor);

      if (red < 255 && bkred < 255)
      {
              SetTextColor   (hdc, REDCOL);
              return;
      }

      if (bkred < 255 && blue > 100 && green > 100)
      {
              SetTextColor   (hdc, REDCOL);
              return;
      }

      if (red == 0 && blue == 0 && green == 0)
      {
              SetTextColor (hdc, BLUECOL);
              return;
      }
                 
      SetTextColor   (hdc, BLACKCOL);
}
       

static void PrintColText (HWND hWnd, HDC hdc, char *text, int x, int y,
                          COLORREF color, COLORREF bkcolor, HFONT hFont,
                          int aktivate)
/**
Text auf Colbutton schreiben.
**/
{
        RECT rect;
        TEXTMETRIC tm ;
        int textlen;
        int textpilen;
        SIZE size;
        int anz;
        char OneChar [2];

        textlen = strlen (text);
        anz = zsplit (text, '&');
        if (anz > 1)
        {
                textlen --;
        }
        GetClientRect (hWnd, &rect);

        if (hFont)
        {
               SelectObject (hdc, hFont);
        }
        GetTextMetrics (hdc, &tm) ;
        GetTextExtentPoint (hdc, text, textlen, &size);
        GetClientRect (hWnd, &rect);
        rect.top += 2;
        rect.bottom -=2;
        textpilen = size.cx;
        if (x == -1)
/* Hotizontal zentrieren                             */
        {
                         
                x = max (2, (rect.right - textpilen) / 2);
        }
        else
        {
               x = max (2, x);
        }
        while ((x + size.cx) > (rect.right - 1))
        {
               textlen --;
               text [textlen] = (char) 0;
               if (textlen == 0) break;
               GetTextExtentPoint (hdc, text, textlen, &size);
        }
        if (y == -1)
/* Hotizontal zentrieren                             */
        {
              
                y = max (2, (rect.bottom - size.cy) / 2);
        }
        else
        {
                y *= size.cy;
                y = max (2, y);
        }

        SetBkMode (hdc, OPAQUE);
        SetTextColor   (hdc, color);
        SetBkColor (hdc, bkcolor);
        if (aktivate == -1 || (aktivate & 0x40) == NOCOLINACTIVE)
        {
                PrintColTextUnakt (hdc, x, y, text, strlen (text));
                return;
        }

        if (anz < 2 && text[0] != '&')
        {
                
                TextOut (hdc, x, y, text, strlen (text));
                return;
        }

        if (anz < 2)
        {
                SetSpezColor (color, bkcolor, hdc);
                OneChar[0] = zwort[1][0];
                OneChar[1] = (char) 0;
                TextOut (hdc, x, y, OneChar, 1);
                GetTextExtentPoint32 (hdc, OneChar, 1, &size);
                x += size.cx;
                SetTextColor   (hdc, color);
                TextOut (hdc, x, y, &zwort [1][1], strlen (zwort[1]) - 1);
                return;
        }
         
        TextOut (hdc, x, y, zwort [1], strlen (zwort[1]));
        GetTextExtentPoint32 (hdc, zwort[1], strlen (zwort[1]), &size);
        x += size.cx;
        SetSpezColor (color, bkcolor, hdc);
        OneChar[0] = zwort[2][0];
        OneChar[1] = (char) 0;
        TextOut (hdc, x, y, OneChar, 1);
        GetTextExtentPoint32 (hdc, OneChar, 1, &size);
        x += size.cx;
        SetTextColor   (hdc, color);
        TextOut (hdc, x, y, &zwort [2][1], strlen (zwort[2]) - 1);
}

static void PrintActivRect (HWND hWnd, HDC hdc, COLORREF color)
/**
Bei Colbuttom mit Focus Rechteck in Hintergrundfarbe anzeigen.
**/
{
         HBRUSH hBrush;
         RECT rect;

         GetClientRect (hWnd, &rect);
         rect.top    += 1;
         rect.left   += 1;
         rect.bottom -= 1;
         rect.right  -= 1;

         hBrush = CreateSolidBrush (color);

         SelectObject (hdc, hBrush);
         SelectObject (hdc, GetStockObject (NULL_PEN));
         Rectangle (hdc, rect.left, rect.top, rect.right, rect.bottom);
         DeleteObject (hBrush);
}



static void PrintFocusRect (HWND hWnd, HDC hdc, COLORREF color)
/**
Bei Colbutton mit Focus Rechteck in Hintergrundfarbe anzeigen.
**/
{
         HBRUSH hBrush;
         RECT rect;

         GetClientRect (hWnd, &rect);
         rect.top    += 5;
         rect.left   += 2;
         rect.bottom -= 5;
         rect.right  -= 2;

         hBrush = CreateSolidBrush (color);

         SelectObject (hdc, hBrush);
         SelectObject (hdc, GetStockObject (NULL_PEN));
         Rectangle (hdc, rect.left, rect.top, rect.right, rect.bottom);
         DeleteObject (hBrush);
}

static void PrintFocusFrame (HWND hWnd, HDC hdc, COLORREF color)
/**
Bei Colbutton mit Focus Rechteck in Hintergrundfarbe anzeigen.
**/
{
         HBRUSH hBrush;
         RECT rect;
         int blue;

         blue = GetBValue (color);
         GetClientRect (hWnd, &rect);
         rect.top    += 5;
         rect.left   += 5;
         rect.bottom -= 5;
         rect.right  -= 5;

         if (color == BLACKCOL)
         {
                   hBrush = CreateSolidBrush (WHITECOL);
         }
         else if (blue > 120)
         {
                   hBrush = CreateSolidBrush (WHITECOL);
         }
         else
         {
                   hBrush = CreateSolidBrush (BLACKCOL);
         }
         SelectObject (hdc, GetStockObject (PS_DASH));

         FrameRect (hdc, &rect, hBrush);

         DeleteObject (hBrush);
}

void print_colbuttonEx (ColButton *CuB, HWND hWnd, HFONT hFont, DWORD BuId)
/**
Inhalt von ColButton anzeigen.
**/
{
       HDC hdc;
       int HasFocus;
       HWND FocushWnd;
	   int IsDef;
       PAINTSTRUCT ps;
       RECT rect;
       static TEXTMETRIC tm;
       COLORREF Color, BkColor;

       if (CuB->BorderLtColor != NULL)
       {
           BorderLtColor = CuB->BorderLtColor;
           BorderColor   = CuB->BorderColor;
       }

       HasFocus = 0;
       FocushWnd = GetFocus ();

       if (FocushWnd == hWnd)
       {
                    HasFocus = 1;
       }

       IsDef = 0;
       if (GetColDef () == hWnd)
       {
                    IsDef = 1;
       }
       GetClientRect (hWnd, &rect);
	   BkColor = CuB->BkColor;
	   if (CuB->aktivate == -1)
	   {
		   BkColor = LTGRAYCOL;
	   }

       hdc = BeginPaint (hWnd, &ps);
/*
       if (BuId & 0x8000 || CuB->aktivate == 4 ||
           CuB->aktivate == 6)
       {
                   PressBorder (hdc, &rect, BkColor);
       }
       else if (BuId & 0x8000 && CuB->aktivate == 14)
	   {
                   PressBorder (hdc, &rect, BkColor);
				   Sleep (1000);
                   UnPressBorder (hdc, &rect, BkColor);
       }
       else if (BuId & 0x8000 && CuB->aktivate == 15)
	   {
                   PressBorder (hdc, &rect, BkColor);
				   Sleep (1000);
                   UnPressBorder (hdc, &rect, BkColor);
       }
       else if ((CuB->aktivate & 128) == 0)
       {
                   UnPressBorder (hdc, &rect, BkColor);
       }
*/

       if (CuB->aktivate == (ACTCOLPRESS | NOCOLBORDER))
       {
            if (MouseMovehWnd != NULL && hWnd == MouseMovehWnd)
            {
                   PrintColBorder (CuB, hWnd);
            }
       }

       if (HasFocus && CuB->aktivate == 1)
       {
                   Color   = CuB->BkColor;
                   BkColor = CuB->Color;
                   PrintFocusRect (hWnd, hdc, BkColor);
       }
       else if (HasFocus && CuB->aktivate == 2)
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
                   PrintFocusFrame (hWnd, hdc, BkColor);
       }
       else if (HasFocus && CuB->aktivate == 3)
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
                   PrintFocusFrame (hWnd, hdc, BkColor);
       }
       else if (HasFocus && CuB->aktivate == 4)
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
                   PrintFocusFrame (hWnd, hdc, BkColor);
       }
       else if (CuB->aktivate == 6 || CuB->aktivate == 7)
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
                   PrintActivRect (hWnd, hdc, BkColor);
       }
       else if (HasFocus && CuB->aktivate == 15)
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
                   PrintFocusFrame (hWnd, hdc, BkColor);
       }
       if (IsDef)
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
                   PrintFocusFrame (hWnd, hdc, BkColor);
       }
       else
       {
                   BkColor = CuB->BkColor;
                   Color   = CuB->Color;
       }

       if (CuB->text1)
       {
                    PrintColText (hWnd, hdc, CuB->text1,
                                  CuB->tx1, CuB->ty1,
                                  Color,
                                  BkColor,
                                  hFont,
                                  CuB->aktivate);
       }

       if (CuB->text2)
       {
                    PrintColText (hWnd, hdc, CuB->text2,
                                  CuB->tx2, CuB->ty2,
                                  Color,
                                  BkColor,
                                  hFont,
                                  CuB->aktivate);
       }

       if (CuB->text3)
       {
                    PrintColText (hWnd, hdc, CuB->text3,
                                  CuB->tx3, CuB->ty3,
                                  Color,
                                  BkColor,
                                  hFont,
                                  CuB->aktivate);
       }

       if (CuB->bmp)
       {
                    PrintBitmap (hWnd,
                                 hdc, CuB->bmp, CuB->bmpx,
                                 CuB->bmpy, SRCCOPY);
       }
                    
       if (CuB->icon)
       {
                    PrintColIcon (hWnd,
                                 hdc, CuB->icon, CuB->icox,
                                 CuB->icoy);
       }


       if (BuId & 0x8000 || CuB->aktivate == 4 ||
           CuB->aktivate == 6)
       {
                   PressBorder (hdc, &rect, BkColor);
       }
       else if (BuId & 0x8000 && CuB->aktivate == 14)
	   {
                   PressBorder (hdc, &rect, BkColor);
				   Sleep (1000);
                   UnPressBorder (hdc, &rect, BkColor);
       }
       else if (BuId & 0x8000 && CuB->aktivate == 15)
	   {
                   PressBorder (hdc, &rect, BkColor);
				   Sleep (1000);
                   UnPressBorder (hdc, &rect, BkColor);
       }
       else if ((CuB->aktivate & 128) == 0)
       {
                   UnPressBorder (hdc, &rect, BkColor);
       }
                    
       EndPaint (hWnd, &ps);
       BorderLtColor = NULL;
       BorderColor   = NULL;
}


void disp_colbuttonEx (HWND hWnd)
/**
Bei WM_PAINT-Meldung ColButton-Felder neu zeichnen.
**/
{
    int i;

	for (i = 0; i < colbanz; i ++)
	{
		if (hWnd == colb[i]->hWnd) break;
	}
	if (i >= colbanz) return;

    print_colbuttonEx (colb[i]->CuB, 
		               colb[i]->hWnd, 
		               colb[i]->hFont,
					   colb[i]->BuId);
}

ColButton *GetColButton (HWND hWnd)
{
        int i;

   	    for (i = 0; i < colbanz; i ++)
		{
		        if (hWnd == colb[i]->hWnd) 
                {
                    return colb[i]->CuB;
                }
		}
 	    return NULL;
}
        


void PressColButton (HWND hWnd, char *wclass)
/**
Kontrollelement Button gedrueckt erzeugen.
**/
{
        int i;
        ColButton *Cub;

   	    for (i = 0; i < colbanz; i ++)
		{
		        if (hWnd == colb[i]->hWnd) break;
				colb[i]->BuId = 0x3FF;
		}
 	    if (i >= colbanz) return;


        Cub =  colb[i]->CuB;
        if (Cub->aktivate == -1)
        {
                        return;
        }


        if (Cub->aktivate < 3 || Cub->aktivate == 10)
        {
                       colb[i]->BuId &= 0x03FF;
                       colb[i]->BuId |= 0x8000;
                        
					   SendMessage (GetParent (hWnd), WM_COMMAND, 
		                      MAKELONG ((WPARAM) GetDlgCtrlID (hWnd), BN_SETFOCUS), 
						                                         (LPARAM) hWnd);
        }
        else if (Cub->aktivate == 3)
        {
                       colb[i]->BuId &= 0x03FF;
                       colb[i]->BuId |= 0x8000;
                       Cub->aktivate = 4;
//                       DoIcon (feld);
					   SendMessage (GetParent (hWnd), WM_COMMAND, 
		                      MAKELONG ((WPARAM) GetDlgCtrlID (hWnd), WM_LBUTTONDOWN), 
						                                         (LPARAM) hWnd);
        }
        else if (Cub->aktivate == 5)
        {
                       colb[i]->BuId &= 0x03FF;
                       colb[i]->BuId |= 0x8000;
                       BuColMinusA (Cub);
                       Cub->aktivate = 6;
//                       DoIcon (feld);
					   SendMessage (GetParent (hWnd), WM_COMMAND, 
		                      MAKELONG ((WPARAM) GetDlgCtrlID (hWnd), WM_LBUTTONDOWN), 
						                                         (LPARAM) hWnd);
        }
        else if (Cub->aktivate == 7)
        {
                       colb[i]->BuId &= 0x03FF;
                       colb[i]->BuId |= 0x8000;
                       Cub->aktivate = 6;
//                       DoIcon (feld);
					   SendMessage (GetParent (hWnd), WM_COMMAND, 
		                      MAKELONG ((WPARAM) GetDlgCtrlID (hWnd), WM_LBUTTONDOWN), 
						                                         (LPARAM) hWnd);
        }
        else if (Cub->aktivate == 4)
        {
                       colb[i]->BuId &= 0x03FF;
                       colb[i]->BuId |= 0x8000;
                       Cub->aktivate = 3;
        }

        if (Cub->aktivate == 10 && TimerOn == 0)
        {
//                       DoIcon (feld);
					   SendMessage (GetParent (hWnd), WM_COMMAND, 
		                      MAKELONG ((WPARAM) GetDlgCtrlID (hWnd), WM_LBUTTONDOWN), 
						                                         (LPARAM) hWnd);
                       TimehWnd = hWnd;
                       SetTimer (TimehWnd, 1, 500, 0);  
        }
        else if (Cub->aktivate == 10)
        {
                       if (TimerOn == 1)
                       {
                                 KillTimer (TimehWnd, 1);  
                                 SetTimer (TimehWnd, 1, 100, 0);
                                 TimerOn = 2;
//                                 DoIcon (feld);
   					             SendMessage (GetParent (hWnd), WM_COMMAND, 
		                             MAKELONG ((WPARAM) GetDlgCtrlID (hWnd), WM_LBUTTONDOWN), 
						                                         (LPARAM) hWnd);
                       }
        }

        else if (Cub->aktivate == (NOCOLPRESS | NOCOLBORDER))
        {
 		               SendMessage (GetParent (hWnd), WM_COMMAND, 
		                             MAKELONG ((WPARAM) GetDlgCtrlID (hWnd), WM_LBUTTONDOWN), 
						                                         (LPARAM) hWnd);
        }

        if (TimerOn == 0)
        {
                  InvalidateRect (hWnd, NULL, TRUE);
                  UpdateWindow (hWnd);
        }
        if (Cub->aktivate == 10)
        {
                  TimerOn = 1;
        }

        if (Cub->aktivate == (ACTCOLPRESS | NOCOLBORDER))
        {
            if (MouseMovehWnd != NULL && hWnd == MouseMovehWnd)
            {
                   PrintColBorderDown (Cub, hWnd);
            }
        }
}

void UnPressColButton (HWND hWnd, char *wclass, int mode)
/**
Kontrollelement Button losgelassen erzeugen.
**/
{
        int i;
        ColButton *Cub;

   	    for (i = 0; i < colbanz; i ++)
		{
		        if (hWnd == colb[i]->hWnd) break;
				if (colb[i]->BuId & 0xC000) colb[i]->BuId = 0x3FF;
		}
 	    if (i >= colbanz) return;


        Cub = colb[i]->CuB;

/*
        if ((colb[i]->BuId & 0x8000) == 0)
        {
                        return;
        }
*/

        if (Cub->aktivate == 4);
        else if (Cub->aktivate == 6);
        else if (Cub->aktivate == (NOCOLPRESS | NOCOLBORDER));
//        else if (Cub->aktivate == (ACTCOLPRESS | NOCOLBORDER));
        else if (mode)
        {
               colb[i]->BuId &= 0x3FFF;
               if (Cub->aktivate != 10 && mode != 2)
               {
//                          DoIcon (feld);
					   SendMessage (GetParent (hWnd), WM_COMMAND, 
		                      MAKELONG ((WPARAM) GetDlgCtrlID (hWnd), WM_LBUTTONUP), 
						                                         (LPARAM) hWnd);
               }
               else if (TimerOn)
               {
                          KillTimer (TimehWnd, 1);
                          TimerOn = FALSE;
                          TimehWnd = NULL;
               }
        }
        else
        {
               colb[i]->BuId &= 0x3FFF;
               colb[i]->BuId |= 0x4000;
        }


        InvalidateRect (hWnd, NULL, TRUE);
        UpdateWindow (hWnd);

        if (Cub->aktivate == (ACTCOLPRESS | NOCOLBORDER))
        {
            if (MouseMovehWnd != NULL && hWnd == MouseMovehWnd)
            {
                   PrintColBorder (Cub, hWnd);
            }
        }
}

void KillColFocusEx (HWND hWnd)
/**
Focus fuer Kontrollelement setzen.
**/
{
        ColButton *CuB;  
        HDC hdc;
        int i;

   	    for (i = 0; i < colbanz; i ++)
		{
		        if (hWnd == colb[i]->hWnd) break;
				if (colb[i]->BuId & 0xC000) colb[i]->BuId = 0x3FF;
		}
 	    if (i >= colbanz) return;


        CuB = colb[i]->CuB;

        InvalidateRect (hWnd, NULL, TRUE);
        UpdateWindow (hWnd);
        if (GetColDef () == hWnd)
        {
                   hdc = GetDC (hWnd);
                   PrintFocusFrame (hWnd, hdc, CuB->BkColor);
                   ReleaseDC (hWnd, hdc);
        }
        return;
}

void SetColFocusEx (HWND hWnd)
/**
Focus fuer Kontrollelement setzen.
**/
{
        ColButton *CuB;  
        HWND Focus;
        int i;

   	    for (i = 0; i < colbanz; i ++)
		{
		        if (hWnd == colb[i]->hWnd) break;
				if (colb[i]->BuId & 0xC000) colb[i]->BuId = 0x3FF;
		}
 	    if (i >= colbanz) return;


        Focus = GetFocus ();
        CuB = colb[i]->CuB;
        if (CuB->aktivate == FALSE)
        {
                 return;
        }
        if (CuB->aktivate == -1)
        {
                 SetFocus (loosefocus);
                 return;
        }

        InvalidateRect (hWnd, NULL,  TRUE);
        UpdateWindow (hWnd);
        AktColFocus = hWnd;
        return;
}

void PrintNoColBorder (ColButton *ColBut, HWND hWnd)
{
        InvalidateRect (hWnd, NULL, TRUE);
        UpdateWindow (hWnd);
}

void PrintColBorder (ColButton *CuB, HWND hWnd)
{
       HDC hdc;
       COLORREF BkColor;
       RECT rect;

       if (CuB->BorderLtColor != NULL)
       {
           BorderLtColor = CuB->BorderLtColor;
           BorderColor   = CuB->BorderColor;
       }

	   BkColor = CuB->BkColor;
       GetClientRect (hWnd, &rect);
        
       hdc = GetDC (hWnd);
       UnPressBorderSm (hdc, &rect, BkColor);
       ReleaseDC (hWnd, hdc);
}
        
        
void PrintColBorderDown (ColButton *CuB, HWND hWnd)
{
       HDC hdc;
       COLORREF BkColor;
       RECT rect;

       if (CuB->BorderLtColor != NULL)
       {
           BorderLtColor = CuB->BorderLtColor;
           BorderColor   = CuB->BorderColor;
       }

	   BkColor = CuB->BkColor;
       GetClientRect (hWnd, &rect);
        
       hdc = GetDC (hWnd);
       PressBorder (hdc, &rect, BkColor);
       ReleaseDC (hWnd, hdc);
}

void TestColBorder (HWND hWnd)
{
        ColButton *ColBut;

        if (MouseMovehWnd != NULL)
        {
                ColBut = GetColButton (MouseMovehWnd);
                if (ColBut != NULL)
                {
                    PrintNoColBorder (ColBut, MouseMovehWnd);
                }
                MouseMovehWnd = NULL;
        }

        ColBut = GetColButton (hWnd);
        if (ColBut->aktivate != (ACTCOLPRESS | NOCOLBORDER))
        {
            return;
        }

        MouseMovehWnd = hWnd;
        if (ColBut != NULL)
        {
                    PrintColBorder (ColBut, hWnd);
                    SetTimer (hWnd, 3, 10, 0);
        }
}

void TestNoColBorder (HWND hWnd, POINT mousepos)
{
        ColButton *ColBut;

        if (!MouseinWindow (hWnd, &mousepos))
        {
                ColBut = GetColButton (MouseMovehWnd);
                if (ColBut != NULL)
                {
                    MouseMovehWnd = NULL;
                    PrintNoColBorder (ColBut, hWnd);
                    KillTimer (hWnd, 3);
                }
        }
}
       

LONG FAR PASCAL ColButtonProcEx (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
        static HFONT hFont = 0;
        POINT mousepos;
        static BOOL InSetFocus = FALSE;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_KEYDOWN :
                       PostMessage (GetParent (hWnd), msg, wParam, lParam);
                       return 0;
              case WM_PAINT :
                       disp_colbuttonEx (hWnd);
                       break;
              case WM_SETFONT :
                       hFont = (HFONT) wParam;
                       return 0;
              case WM_LBUTTONDOWN :
                       SetFocus (hWnd);
                       PressColButton (hWnd, "ColButton");
                       MousePressed = TRUE;
                       SetTimer (hWnd, 2, 10, 0);
                       return 0;
              case WM_LBUTTONUP :
                       UnPressColButton (hWnd,"ColButton", 1);
                       MousePressed = FALSE;
                       KillTimer (hWnd, 1);
                       KillTimer (hWnd, 2);
                       return 0;
              case WM_TIMER :
                       switch (wParam)
                       {
                                case 1:
                                   PressColButton (hWnd, "ColButton");
                                   if (!MouseinWindow (hWnd, &mousepos))
                                   {
                                          UnPressColButton (hWnd,"ColButton", 2);
                                          MousePressed = FALSE;
                                          MousehWnd = hWnd;
                                                   
                                   }
                                   if (GetKeyState (VK_LBUTTON) >= 0)
                                   {
                                          UnPressColButton (hWnd,"ColButton", 2);
                                          MousePressed = FALSE;
                                          KillTimer (hWnd, 1);
                                          KillTimer (hWnd, 2); 
                                          MousehWnd = NULL;
                                   }
                                   break;
                                case 2 :
                                   if (!MouseinWindow (hWnd, &mousepos))
                                   {
                                          UnPressColButton (hWnd,"ColButton", 2);
                                          MousePressed = FALSE;
                                          MousehWnd = hWnd;
                                                      
                                    }
                                    if (GetKeyState (VK_LBUTTON) >= 0)
                                    {
                                                   UnPressColButton (hWnd,"ColButton", 2);
                                                   MousePressed = FALSE;
                                                   KillTimer (hWnd, 1);
                                                   KillTimer (hWnd, 2); 
                                                   MousehWnd = NULL;
									}
                                case 3 :
                                    TestNoColBorder (hWnd, mousepos); 
                                    break;
                       }                     
                       return 0;
              case WM_MOUSEMOVE :
                       if (hWnd == MousehWnd)
                       {
                             if (GetKeyState (VK_LBUTTON) < 0)
                             {
                                    KillTimer (hWnd, 2); 
                                    MousehWnd = NULL;
                                    SendMessage (hWnd, WM_LBUTTONDOWN,
                                                 wParam, lParam);
                             }
                       }
                       if (MouseMovehWnd != hWnd)
                       {
                             TestColBorder (hWnd);
                       }
                       break;
              case WM_SETFOCUS :
//				       InitColDef ();
                       SetColFocusEx (hWnd);
                       if (IsEnabledColDef ())
                       {
				                SetColDef (hWnd);
                       }

//					   SendMessage (GetParent (hWnd), WM_COMMAND, 
//		                      MAKELONG ((WPARAM) GetDlgCtrlID (hWnd), BN_SETFOCUS), 
//						                                         (LPARAM) hWnd);
                       return 0;
              case WM_KILLFOCUS :
                       KillColFocusEx (hWnd);
//					   SendMessage (GetParent (hWnd), WM_COMMAND, 
//		                      MAKELONG ((WPARAM) GetDlgCtrlID (hWnd), BN_KILLFOCUS), 
//						                                         (LPARAM) hWnd);
                       return 0;
              case WM_DEF :
				       SetColDef (hWnd);
				       EnableColDef (TRUE);
					   break;
              case WM_DESTROY :

                       KillTimer (hWnd, 2);
                       if (TimerOn && TimehWnd == hWnd)
                       {
                                    KillTimer (TimehWnd, 1);
                                    TimerOn = 0;
                                    TimehWnd = NULL;
                       }
                       if (MousehWnd == hWnd)
                       {
                                    MousehWnd = NULL;
                       }
					   DestroyColButton (hWnd);
                       break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void ActivateCBNoBorder (ColButton *Cub, BOOL akt)
/**
Focus fuer Kontrollelement setzen.
**/
{
        if ((Cub->aktivate & NOCOLBORDER) == 0) return; 
        if (akt == FALSE)
        {
              Cub->aktivate |= NOCOLINACTIVE;
        }
        else
        {
              Cub->aktivate &= 0xFF ^ NOCOLINACTIVE;
        }
}
              
         
