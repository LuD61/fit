#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_enter.h"
#include "enterfunc.h"


int EnterF::BreakOk (void)
{
	   syskey = KEYCR;
       return 1;
}

int EnterF::BreakCancel (void)
{
	   syskey = KEY5;
       return 1;
}

int EnterF::BreakEnter (void)
{
	   if (syskey == KEYCR)
	   {
	            return 1;
	   }
	   return 0;
}


void EnterF::EnterText (HWND hMainWindow, char *label, char *text)
/**
Dialog mit einem Eingabefeld und 2 Buttons
**/
{
       HANDLE hMainInst;
       CFIELD C1 ("text1", "", 
 			                           0, 0, 0, 1, NULL, "",
                                       CDISPLAYONLY,
						               600, &textfont, 0, TRANSPARENT,
									   NULL,NULL);
       CFIELD C2 ("edit1", "",
						               20, 0, 12, 1, NULL, "",  
                                       CEDIT,
						               601, &textfont, 0, 0,
									   NULL,BreakEnter);
						
       CFIELD *_fWork0[] = {&C1,&C2};
       CFORM fWork0 (2, _fWork0);

	   CFIELD B1 ("BU1", "     OK    ", 
 			                           11, 0, 0, 4, NULL, "",
                                       CBUTTON,
						               603, &bufont, 0, 0,
									   NULL,BreakOk);
	   CFIELD B2 ("BU2", " Abbrechen ", 
 			                           11, 0, 12, 4, NULL, "",
                                       CBUTTON,
						               604, &bufont, 0, 0,
									   NULL,BreakCancel);
       CFIELD *_fWork1[] = {&B1,&B2};
       CFORM fWork1 (2, _fWork1);


       CFIELD C3 ("form0", &fWork0, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   602, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD C4 ("form1", &fWork1, 0, 0, -1, -2, NULL, "",
		                   CFFORM,
						   605, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD *_fWork[] = {&C3, &C4};
       CFORM fWork (2, _fWork);

       DWORD style = WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU;
 			         
       hMainInst = (HANDLE) GetClassLong (hMainWindow, GCL_HMODULE); 

       C1.SetFeld (label);
       C2.SetFeld (text);
       Enter = new fEnter (-1, -1, 46, 5, NULL);
       Enter->SetBkColor (GetSysColor (COLOR_3DFACE));
       Enter->SetMask (&fWork);
/*
       textfont.FontBkColor = LTGRAYCOL;
       Enter->SetBkColor (LTGRAYCOL);
*/
       Enter->OpenWindow (hMainInst, hMainWindow, style );
       Enter->ProcessMessages ();
       Enter->DestroyWindow ();
       delete Enter;
}

void EnterF::EnterText (HWND hMainWindow, char *label, char *text, int len, char *picture)
/**
Dialog mit einem Eingabefeld und 2 Buttons
**/
{
       HANDLE hMainInst;
       CFIELD C1 ("text1", "", 
 			                           0, 0, 0, 1, NULL, "",
                                       CDISPLAYONLY,
						               600, &textfont, 0, TRANSPARENT,
									   NULL,NULL);
       CFIELD C2 ("edit1", "",
						               len, 0, 12, 1, NULL, "",  
                                       CEDIT,
						               601, &textfont, 0, 0,
									   NULL,BreakEnter);
						
       CFIELD *_fWork0[] = {&C1,&C2};
       CFORM fWork0 (2, _fWork0);

	   CFIELD B1 ("BU1", "     OK    ", 
 			                           11, 2, 0, 4, NULL, "",
                                       CBUTTON,
						               603, &bufont, 0, 0,
									   NULL,BreakOk);
	   CFIELD B2 ("BU2", " Abbrechen ", 
 			                           11, 2, 12, 4, NULL, "",
                                       CBUTTON,
						               604, &bufont, 0, 0,
									   NULL,BreakCancel);
       CFIELD *_fWork1[] = {&B1,&B2};
       CFORM fWork1 (2, _fWork1);


       CFIELD C3 ("form0", &fWork0, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   602, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD C4 ("form1", &fWork1, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   605, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD *_fWork[] = {&C3, &C4};
       CFORM fWork (2, _fWork);

       if (picture && strlen (picture))
       {
           fWork.SetFieldPicture ("edit1", picture);
           if (strchr (picture, 'f'))
           {
               fWork.SetFieldBkMode ("edit1", ES_RIGHT);
           }
       }

       DWORD style = WS_POPUP | WS_DLGFRAME | WS_CAPTION;
 			         
       hMainInst = (HANDLE) GetClassLong (hMainWindow, GCL_HMODULE); 

       C1.SetFeld (label);
       C2.SetFeld (text);
       len = len + strlen (label) + 4;
       Enter = new fEnter (-1, -1, len, 5, NULL);
       Enter->SetMask (&fWork);

       textfont.FontBkColor = Color;
       Enter->SetBkColor (Color);

       Enter->OpenWindow (hMainInst, hMainWindow, style );
       Enter->ProcessMessages ();
       Enter->DestroyWindow ();
       delete Enter;
}

void EnterF::EnterUpDown (HWND hMainWindow, char *label, char *text)
/**
Dialog mit einem Eingabefeld und 2 Buttons
**/
{
       HANDLE hMainInst;
       CFIELD C1 ("text1", "", 
 			                           0, 0, 0, 1, NULL, "",
                                       CDISPLAYONLY,
						               600, &textfont, 0, TRANSPARENT,
									   NULL,NULL);
       CFIELD C2 ("edit1", "",
						               4, 0, 12, 1, NULL, "%3hd",  
                                       CEDIT,
						               601, &textfont, 0, 0,
									   NULL,BreakEnter);
						
       CFIELD *_fWork0[] = {&C1,&C2};
       CFORM fWork0 (2, _fWork0);

	   CFIELD B1 ("BU1", "     OK    ", 
 			                           11, 2, 0, 4, NULL, "",
                                       CBUTTON,
						               603, &bufont, 0, 0,
									   NULL,BreakOk);
	   CFIELD B2 ("BU2", " Abbrechen ", 
 			                           11, 2, 12, 4, NULL, "",
                                       CBUTTON,
						               604, &bufont, 0, 0,
									   NULL,BreakCancel);
       CFIELD *_fWork1[] = {&B1,&B2};
       CFORM fWork1 (2, _fWork1);


       CFIELD C3 ("form0", &fWork0, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   602, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD C4 ("form1", &fWork1, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   605, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD *_fWork[] = {&C3, &C4};
       CFORM fWork (2, _fWork);

       DWORD style = WS_BORDER | WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU;
 			         
       hMainInst = (HANDLE) GetClassLong (hMainWindow, GCL_HMODULE); 

       C1.SetFeld (label);
       C2.SetFeld (text);
       C2.SetX (strlen (label) + 1);
	   C2.SetUpDown (TRUE, 200, 0, atoi (text));
	   fWork0.SetLayout (XFLOW);
       Enter = new fEnter (-1, -1, 30, 5, "Numerische Eingabe");
       Enter->SetMask (&fWork);

       textfont.FontBkColor = LTGRAYCOL;
       Enter->SetBkColor (LTGRAYCOL);

       Enter->OpenWindow (hMainInst, hMainWindow, style );
	   EnableWindow (hMainWindow, FALSE);
       Enter->ProcessMessages ();
	   EnableWindow (hMainWindow, TRUE);
       Enter->DestroyWindow ();
       delete Enter;
}

