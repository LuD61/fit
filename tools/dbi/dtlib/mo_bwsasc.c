/***
--------------------------------------------------------------------------------
-
-       Modulname           :       mo_bwsasc.c
-
        Exports                 :
         
        Imports                 :

-       Interne Prozeduren      :  
-
-       Externe Prozeduren      :    
                                        
-
-       Autor                   :       WR    
-       Erstellungsdatum        :       18.01.93
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ESQL-C
-
-       Aenderungsjournal       :
-
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   BWS-Tabelle in Ascii-Datei uebertragen.
-
--------------------------------------------------------------------------------
***/

#include <stdio.h>
#include <stdarg.h>
#include <malloc.h>
#include <fcntl.h>
#include <string.h>
#include "strfkt.h"
#include "mo_intp.h"

#ifdef BIWAK
#include "mo_db.h"
#include "conf_env.h"
#else
#ifdef ODBC
#include "dbclass.h"
#else
#include "mo_curso.h"
#endif
#endif

int dbstatus = 0;

#define sqlstatus dbstatus

#define KONVERT "KONVERT"
#ifdef BIWAK
#define sqlstatus dbstatus
#endif


#define CHAR    1
#define SHORT   2
#define LONG    3
#define STRING  4
#define DOUBLE  5
#define HEX     6

#ifdef BIWAK
#define SQLCHAR    0
#define SQLSMINT   1
#define SQLINT     2
#define SQLSERIAL  2
#define SQLFLOAT   3
#define SQLDECIMAL 3
#define SQLMONEY   3
#endif


extern char *zwort[];
extern char *wort[];
extern char *clipped ();
extern double ratod ();
extern char * upstrstr ();
extern char * make_name ();

extern short DEBUG;
extern short FDEBUG;
extern char sql[];
extern short sql_mode;

/* Informationen aus SW-Kopf                                      */
extern short blklen;            /* SW-Blocklaenge                        */
extern short blkanz;            /* Anzahl SW-Bloecke                     */
extern short banz;              /* Anzahl SW-Felder pro Satz             */
extern short klen;              /* Anzahl SW-Felder pro Satz             */

extern short swq;                  /* Quelle ist eine SW-Datei              */ 
extern short swz;                  /* Ziel ist eine SW-Datei                */ 
short sw_rec = 0;
int Tables = 1;
char TableTab[20] [21];
int idtab[20];
char *AktTable;


/* Typ-Definitionen                               */

/* Struktur fuer Feldbeschreibung                 */

/*
typedef struct
       {   char  feldname [20];
           short feldlen;
           char  feldtyp [2]; 
           short nachkomma;
           short dimension;
       } FELDER;

typedef struct
       {   FELDER **felder;
           char   **buffer;
       } RECORD;


union FADR
      { char  *fchar;
        short *fshort;
        int   *fint;
        long  *flong;
        void  *fvoid;
        double *fdouble;
      };
*/

extern char *argsp [];      /* Speicher fuer Argumente                       */

static char dbtab [512];    /* Name der Datenbanktabelle                     */
static char *ascii_name;    /* Name der Ascii-Datei                          */
static char *info_name;     /* Name der Info-Datei                           */

static char *zwsatz = (char *) 0;
                            /* Zwischenbuffer fuer Ausgabe mit Trennzeichen  */
extern char *wertesatz;     /* Buffer mit Initialisierungswerten.            */
extern short zlen;          /* Laenge ausgabesatz                            */
extern char *daten;         /* Buffer fuer allgemeien Variablen              */
extern short dlen;          /* Anzahl allgemeien Daten                       */
char CR [] = {10,0};        /* Zeilenende                                    */ 

extern char where_part [];     
extern char where_part_upd [];     
                            /* Bufer fuer Where-Teil                         */

static int sel_db = -1;     /* Cursor                                        */
static int sel_db_upd = -1;     /* Cursor                                        */
static int ins_db = -1;
static int upd_db = -1;
static int del_db = -1;
static char *and_part = 0; /* Selektionskriterium fuer Tabelle               */
                           /* Wird als Argument mit -s uebergeben            */
                           /* z.B. -s"where ls = 1"                          */

static char *ename = 0;     /* Datei mit Bedingung fuer einzelne Saetze     */
static short rd_error = 0;  /* Kennzeichen fuer ename lesen.                */

static short anzeigen = 0;  /* Flag fuer Anzeigemodus                       */
static short sqldebug = 0;  /* Flag fuer SQL-Debugmode                      */
static short feldinh  = 0;  /* Feldinhalte Ziel                             */
static short test = 0;      /* Testmodus                                    */
static short binaer;        /* Dateimodus                                   */
extern short show_code;     /* Code anzeigen                                */
extern int arganz;          /* Anzahl Argumente                             */

static char trenner = 0;      /* Trennzeichen zwischen Felder                 */
static int hochkomma = 0;     /* Flag fuer Hochkomma bei Typ C                */
static int mit_clipp = 0;     /* Flag fuer clipped bei Typ C                  */
static char omodus[5] = {"a"};
static FILE *inf = NULL;
static char tmpname [512];
static int DBOK = 0;

int writelog (char * format, ...)
/**
Debugausgabe.
**/
{
          va_list args;
          FILE *logfile;
          static int log_ok = 0;
          char logtext [0x1000];

          if (log_ok == 0)
          {
                     log_ok = 1;
                     logfile = fopen ("\\user\\fit\\tmp\\dbinfo.log",
                                      "wb");
          }
          else
          {
                     logfile = fopen ("\\user\\fit\\tmp\\dbinfo.log",
                                      "ab");
          }

          if (logfile == (FILE *) 0)
          {
                     return 0;
          }

          va_start (args, format);
          vsprintf (logtext, format, args);
          va_end (args);

          fprintf (logfile, "%s", logtext);
          fclose (logfile);
          return 0;
}

void opendb (char *dbname)
/**
Datenbank oeffnen.
**/
{
        opendbase (dbname);
        DBOK = 1;
}


void setand_part (name)
char *name;
{
           and_part = name;
}

          
int fetchsql (dbcursor)
/**
Aus Datenbank lesen.
**/
short dbcursor;
{
	      static int first = 1;

          if (ename == 0)
          {
                    return (fetch_sql (dbcursor));
          }

		  if (! first) 
          {
			         fetch_sql (dbcursor);
					 if (sqlstatus == 0) return (0);
          }
		  else
          {
			         first = 0;
          }
          if (rd_error != 0) return (100);
          open_sql (dbcursor);
          fetch_sql (dbcursor);
          return (0);
}


void to_ausgabe ()
/**
Eingabesatz in Ausgabesatz konvertieren.
**/
{
          short i;

          for (i = 0; ziel[i].feldname[0]; i ++)  
          {
                   znullvalue (ziel[i].feldname);
                   uebertrage (&outsatz, ziel [i].feldname,
                               &insatz, ziel [i].feldname);
          }
}

void out_to_in ()
/**
Ausgabesatz in Eingabesatz uebertragn.
**/
{
          short i;

          for (i = 0; ziel[i].feldname[0]; i ++)  
          {
                   znullvalue (quelle[i].feldname);
                   uebertrage (&insatz, ziel [i].feldname,
                               &outsatz, ziel [i].feldname);
          }
}

int to_db ()
/**
Ausgabesatz in Eingabesatz konvertieren.
**/
{
          short zpos;
          int dsqlstatus;  
		  int dsqlerror;

          out_to_in ();
//          if (constat) return (0);
          if (where_part_upd[0])
          {
                   beginwork ();
                   perform_test_write ();
                   if (constat) 
				   {
					   commitwork ();
					   return (0);
				   }
                   dsqlstatus = open_sql (sel_db_upd);
                   if (dsqlstatus != 0) 
                   {
                                rollbackwork (); 
                                return (dsqlstatus);
                   }
                   dsqlstatus = fetch_sql (sel_db_upd);
                   perform_code_upd ();
                   if (constat) 
				   {
					   commitwork ();
					   return (0);
				   }
                   out_to_in ();
                   if (dsqlstatus == 0)
                   {
                                 execute_curs (upd_db);
#ifndef BIWAK
								 dsqlerror = sqlerror;
#endif
                   }
                   else if (dsqlstatus == 100)
                   {
                                 execute_curs (ins_db);
#ifndef BIWAK
								 dsqlerror = sqlerror;
#endif
                   }
                   commitwork ();
                   return (dsqlstatus);
          }
          return (0); 
}

int delete_db ()
/**
Ausgabesatz in Eingabesatz konvertieren.
**/
{
          short zpos;
          int dsqlstatus;  
		  int dsqlerror;

          out_to_in ();
          if (where_part_upd[0])
          {
                   beginwork ();
//                   perform_test_write ();
                   if (constat) 
				   {
					   commitwork ();
					   return (0);
				   }
                   dsqlstatus = open_sql (sel_db_upd);
                   if (dsqlstatus != 0) 
                   {
                                rollbackwork (); 
                                return (dsqlstatus);
                   }
                   dsqlstatus = fetch_sql (sel_db_upd);
                   perform_code_del ();
                   if (constat) 
				   {
					   commitwork ();
					   return (0);
				   }
                   out_to_in ();
                   if (dsqlstatus == 0)
                   {
                                 execute_curs (del_db);
#ifndef BIWAK
								 dsqlerror = sqlerror;
#endif
                   }

                   commitwork ();
                   return (dsqlstatus);
          }
          return (0); 
}


int nachkomma (dwert, nk1, nk2)
/**
Nachkommastellen auswerten.
**/
double *dwert;
short nk1, nk2;
{
           short i;
           short diff;
           double faktor;
           double rwert;
           long lwert;
           rwert = *dwert;
           if (double_null (rwert)) rwert = 0.0;
           faktor = 10.0;
           for (i = 0; i < nk1; i ++)
           {
                       rwert *= faktor;
           }
           *dwert = rwert;
           diff = nk1 - nk2;

           if (diff == 0) return 0;
           if (diff < 0)
           {
                      diff = 0 - diff;
           }
           else
           {
                      faktor = 0.1;
           }
           for (i = 0; i < diff; i ++)
           {
                       rwert *= faktor;
           }
           lwert = (long) rwert;
           rwert = (double) lwert;
           *dwert = rwert;
}

void date_to_str (dstring, feld)
/**
String in Datumformat konvertieren.
**/
char *dstring;
FELDER *feld;
{
          char datum [11];

          memcpy (datum,dstring,2);
          memcpy (&datum [2],&dstring[3],2);
          strcpy (&datum[4],&dstring[6]);
          if (strlen (datum) > (unsigned int) feld->feldlen) 
          {
              strcpy (&datum [4],&datum [6]);
          }
          strcpy (dstring,datum);
}
          
int memclipp (zpos, qpos, ziellen)
/**
qpos in zpos in der Laenge len kopieren.
Blanks am Ende von zpos entfernen.
**/
char *zpos;
char *qpos;
int ziellen;
{
         
         memcpy (zpos, qpos, ziellen);
         zpos [ziellen] = (char) 0;
         clipped (zpos);
         if (strcmp (zpos, " ") == 0) 
         {
                    *zpos = (char) 0;
                    return (0);
         }
         return (strlen (zpos));
}

int testblank (string, len)
/**
Testen, ob Character <> ' ' vorhanden sind.
**/
char *string;
int len;
{
         int i;

         if (trenner == (char) 0) return (1);
         if (mit_clipp < 2) return (1);
         for (i = 0; i < len; i ++)
         {
                   if (string[i] > ' ') return (1);
         }
         return (0);
}

int testformat (satz, len, ausgabefile)
/**
Ausgabeformat bestimmen und Satz formatieren.
**/
char *satz;
int len;
FILE *ausgabefile;
{
         int i;
         int flen;
         char *qpos;
         char *zpos;
         int setkomma;

         if (hochkomma == 0 && trenner == (char) 0) 
         {
                         return (0);
         }

         if (mit_clipp) len = 0;
         memset (zwsatz, (char) 0, sizeof (zwsatz));
         qpos = satz;
         zpos = zwsatz;
         for (i = 0; ziel[i].feldname[0]; i ++)
         {
                   switch (ziel[i].feldtyp[0])
                   {
                         case 'C' :
                         case 'd' :
                                  setkomma = 0;
                                  if (hochkomma && testblank (qpos,
                                                       ziel[i].feldlen)) 
                                  {
                                              *zpos = '\"';	
                                              zpos += 1;
                                              len ++;
                                              setkomma = 1;
                                  }
                                  if (mit_clipp)
                                  {
                                         flen = memclipp (zpos, qpos, 
                                                          ziel[i].feldlen);
                                         zpos += flen;
                                         len += flen;
                                  }
                                  else
                                  {
                                          memcpy (zpos, qpos, ziel[i].feldlen);
                                          zpos += ziel[i].feldlen;
                                  }
                                  qpos += ziel[i].feldlen;
                                  if (hochkomma && setkomma) 
                                  {
                                              *zpos = '\"';	
                                              zpos += 1;
                                              len ++;
                                  }
                                  break;
                         case  'A' :
                         case  'N' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                         case  'S' :
                         case  'I' :
                         case  'D' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
#ifdef ODBC
                         case  'G' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                         case  'J' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
                         case  'K' :
                                  memcpy (zpos, qpos, ziel[i].feldlen);
                                  zpos += ziel[i].feldlen;
                                  qpos += ziel[i].feldlen;
                                  if (mit_clipp) len += ziel[i].feldlen;
                                  break;
#endif
                 }
                 if (trenner != (char) 0 && ziel[i + 1].feldname [0])
                 {
                                  *zpos = trenner;
                                  zpos += 1;
                                  len ++;
                 }
      }
      fwrite (zwsatz, 1, len, ausgabefile);
      if (binaer == 0) fwrite (CR,1, strlen (CR), ausgabefile);
      return (1);
}

int TestSyntax (void)
/**
Syntax des SQl-Befehls testen.
**/
{
	   int dsqlstatus;
       short sql_s;

       sql_s = sql_mode;
       sql_mode = 2;
       sprintf (sql,"select * from %s ", dbtab);

       if (where_part [0]) strcat (sql,where_part);
       if (and_part) 
       {
                strcat (sql,and_part);
       }
	   dsqlstatus = prepare_sql (sql);
	   if (dsqlstatus < 0)
	   {
                 sql_mode = sql_s; 
		         return (dsqlstatus);
	   }
	   close_sql (dsqlstatus);
       sql_mode = sql_s; 
       return (0);
}
	    

void ChangeChar (char *String, char oldchar, char newchar)
{
    char *p;

    p = strchr (String, oldchar);
    if (p != NULL)
    {
        *p = newchar;
    }
}


int select_cursor (void)
/**
Cursor zum Lesen.
**/
{
          short i;
          short typ;
          short fpos;
          short spos;
          void *fvar;
          short wanz;
          extern char *wort [];
		  int dsqlstatus;


          dsqlstatus = TestSyntax ();
		  if (dsqlstatus)
		  {
			      return (dsqlstatus);
		  }

          sprintf (sql,"select ");
          for (i = 0; quelle [i].feldname [0]; i ++)
          {
                      typ = getdbtyp (&quelle [i]);
                      fpos = feldpos (quelle, quelle [i].feldname);
                      fvar = &eingabesatz [fpos];
                      if (typ == 0)
                      {
                                out_quest (fvar,typ, quelle[i].feldlen);
                      }
                      else
                      {
                                out_quest (fvar,typ, 0);
                      }
                      ChangeChar (quelle[i].feldname, '$', '.');
                      strcat (sql,quelle[i].feldname);
                      ChangeChar (quelle[i].feldname, '.', '$');
                      if (quelle [i + 1].feldname [0]) strcat (sql, ",");
           }

           wanz = split (where_part);
           for (i = 2; i <= wanz; i ++)
           {
                     spos = instruct (quelle, clipped (wort [i]));
                     if (spos != -1)
                     {
                               if (test_q (wanz, i) == -1) continue;
                               typ = getdbtyp (&quelle [spos]);
                               fpos = feldpos (quelle, wort [i]);
                               fvar = &eingabesatz [fpos];
                               if (typ == 0)
                               {
                                     ins_quest (fvar,typ, quelle[spos].feldlen);
                                     if (test) 
                                     printf 
                                         ("ins_quest (%s (%ld), %hd, %hd)\n",
                                             quelle[spos].feldname,
                                             fvar,
                                             typ,
                                             quelle[spos].feldlen);
                               }
                               else
                               {
                                      ins_quest (fvar,typ, 0);
                                      if (test) 
                                      printf 
                                         ("ins_quest (%s (%ld), %hd, %hd)\n",
                                             quelle[spos].feldname,
                                             fvar,
                                             typ,
                                             0);
                               }
                     }
           }

           strcat (sql," from ");
           strcat (sql,dbtab);

           strcat (sql," ") ;
           if (where_part [0]) strcat (sql,where_part);
           if (and_part) 
           {
                      strcat (sql,and_part);
           }
           strcat (sql," ") ;
           if (sqldebug)  printf ("%s\n", sql);
		   dsqlstatus = prepare_sql (sql);
		   if (dsqlstatus != 0)
		   {
		   }
           return (dsqlstatus);
}


int getdbname (char *dbname, char *string)
{
    int i;
    while (*string <= ' ') 
    {
        if (*string == 0) return -1;
        string += 1;
    }
    while (*string > ' ')
    {
        if (*string == 0) return -1;
        string += 1;
    }
    while (*string <= ' ') 
    {
        if (*string == 0) return -1;
        string += 1;
    }

    for (i = 0; *string > ' '; i ++, string += 1)
    {
            if (*string == '=') break;
            if (*string == '>') break;
            if (*string == '<') break;
            dbname[i] = *string;
    }
    dbname[i] = 0;
}


void ins_upd ()
/**
ins_quest fuer Update
**/
{
	       int i;
           short typ;
           short fpos;
           short spos;
           void *fvar;
		   int wanz;
           int ins_anz;
           static char updname [80];

           if (where_part_upd [0] == 0) return;

           ins_anz = zsplit (where_part_upd, '?');
            for (i = 0; i < ins_anz; i ++)
            {
                      zsplit (where_part_upd, '?');
                      if (getdbname (updname, zwort[i + 1]) == -1)
                      {
                          continue;
                      }
                      spos = instruct (quelle, updname);
                      if (spos != -1)
                      {
                               typ = getdbtyp (&quelle [spos]);
                               fpos = feldpos (quelle, updname);
                               fvar = &eingabesatz [fpos];
                               if (typ == 0)
                               {
                                     ins_quest (fvar,typ, quelle[spos].feldlen);
                               }
                               else
                               {
                                      ins_quest (fvar,typ, 0);
                               }
                     }
           }
}


int select_cursor_upd (void)
/**
Cursor zum Lesen fuer update.
**/
{
          short i;
          short typ;
          short fpos;
          short spos;
          void *fvar;
          short wanz;
          int Secondfield;
          extern char *wort [];
		  int dsqlstatus;


/*
		  dsqlstatus = TestSyntax ();
		  if (dsqlstatus)
		  {
			      return (dsqlstatus);
		  }
*/

          sprintf (sql,"select ");
          for (i = 0; quelle [i].feldname [0]; i ++)
          {
                      typ = getdbtyp (&quelle [i]);
                      fpos = feldpos (quelle, quelle [i].feldname);
                      fvar = &eingabesatz [fpos];
                      if (typ == 0)
                      {
                                out_quest (fvar,typ, quelle[i].feldlen);
                      }
                      else
                      {
                                out_quest (fvar,typ, 0);
                      }
                      strcat (sql,quelle[i].feldname);
                      if (quelle [i + 1].feldname [0]) strcat (sql, ",");
           }

		   ins_upd ();

           strcat (sql," from ");
           strcat (sql,dbtab);

           strcat (sql," ") ;
           if (where_part_upd [0]) strcat (sql,where_part_upd);
/*
           if (and_part) 
           {
                      strcat (sql,and_part);
           }
*/
           strcat (sql," ") ;
           if (sqldebug)  printf ("%s\n", sql);
		   dsqlstatus = prepare_sql (sql);
		   if (dsqlstatus != 0)
		   {
		   }
           return (dsqlstatus);
}


int upd_cursor ()
/**
Cursor fuer Update Index.
**/
{
           short i; 
           short pos;
           char *dbadr;
           static char updname [80];

           if (where_part_upd [0] == 0) return;
           pos = 0;
           for (i = 0; i < db_anz; i ++)
           {
                      dbadr = &eingabesatz [pos];
                      switch (quelle[i].feldtyp[0])
                      {
                            case 'C' :
                            case 'd' :
                                      ins_quest (dbadr, 0, quelle[i].feldlen);
                                      break;
                            case 'S' :
                                      ins_quest (dbadr, 1, 0);
                                      break;
                            case 'I' :
                                      ins_quest (dbadr, 2, 0);
                                      break;
                            case 'D' :
                                      ins_quest (dbadr, 3, 0);
                                      break;
                      }
                      pos += quelle[i].feldlen;
            }
            sprintf (sql,"update %s set (",dbtab);
            for (i = 0; i < db_anz - 1; i ++)
            {
                        strcat (sql,quelle[i].feldname);
                        strcat (sql,",");
            }
            strcat (sql,quelle[i].feldname);
            strcat (sql,") = (");
            for (i = 0; i < db_anz - 1; i ++)
            {
                        strcat (sql,"?,");
            }

            strcat (sql,"?)");

			ins_upd ();

            strcat (sql," ") ;
            if (where_part_upd [0]) strcat (sql,where_part_upd);

            if (sqldebug)  printf ("%s\n", sql);
            upd_db = prepare_sql (sql);
}


int del_cursor ()
/**
Cursor fuer Update Index.
**/
{
           short i; 
           short pos;
           char *dbadr;
           static char updname [80];

            if (where_part_upd [0] == 0) return;
            sprintf (sql,"delete from %s ",dbtab);

			ins_upd ();

            strcat (sql," ") ;
            if (where_part_upd [0]) strcat (sql,where_part_upd);

            if (sqldebug)  printf ("%s\n", sql);
            del_db = prepare_sql (sql);
}

int ins_cursor ()
/**
Insert-Cursor vorbereiten.
**/
{
           short i; 
           short pos;
           char *dbadr;
           double dwert;

           pos = 0;
           for (i = 0; i < db_anz; i ++)
           {
                      dbadr = &eingabesatz [pos];
                      switch (quelle[i].feldtyp[0])
                      {
                            case 'C' :
                            case 'd' :
                                      ins_quest (dbadr, 0, quelle[i].feldlen);
                                      break;
                            case 'S' :
                                      ins_quest (dbadr, 1, 0);
                                      break;
                            case 'I' :
                                      ins_quest (dbadr, 2, 0);
                                      break;
                            case 'D' :
                                      ins_quest (dbadr, 3, 0);
                                     break;
                      }
                      pos += quelle[i].feldlen;
            }
            sprintf (sql,"insert into %s (",dbtab);
            for (i = 0; i < db_anz - 1; i ++)
            {
                        strcat (sql,quelle[i].feldname);
                        strcat (sql,",");
            }
            strcat (sql,quelle[i].feldname);
            strcat (sql,") values (");
            for (i = 0; i < db_anz - 1; i ++)
            {
                        strcat (sql,"?,");
            }
            strcat (sql,"?)");
            if (sqldebug)  printf ("%s\n", sql);
            ins_db = prepare_sql (sql);
}


int update_cursor ()
/**
Cursor fuer Update und Insert vorbereiten.
**/
{
           if (where_part_upd [0] == (char) 0) return (-1);

           sel_db_upd = select_cursor_upd ();
		   if (sel_db_upd < 0) return (-1);
		   upd_cursor ();
		   if (upd_db < 0) 
           {
               close_sql (sel_db_upd);
               return (-1); 
           }
		   del_cursor ();
		   if (del_db < 0) 
           {
               close_sql (sel_db_upd);
               close_sql (upd_cursor);
               return (-1); 
           }
		   ins_cursor ();
		   if (ins_db < 0) 
           {
               close_sql (sel_db_upd);
               close_sql (upd_db);
               close_sql (del_db);
               return (-1); 
           }
           return (sel_db_upd);
}

int close_upd_cursor (void)
{
           if (sel_db_upd > -1) 
           {
               close_sql (sel_db_upd);
               sel_db_upd = -1;
           }

           if (upd_db > -1) 
           {
               close_sql (upd_db);
               upd_db = -1;
           }
           if (del_db > -1) 
           {
               close_sql (del_db);
               del_db = -1;
           }
           if (ins_db > -1) 
           {
               close_sql (ins_db);
               ins_db = -1;
           }
           return 0;
}

int test_q (wanz, pos)
/**
Test, ob das uebernaechste Zeichen ein ? ist.
**/
short wanz;
short pos;
{
           pos += 2;
           if (pos > wanz) return (-1);
           if (wort [pos] [0] == '?') return (0);
           return (-1);
}



/***********

Include-Datei einlesen   

*/


int includefile (incl, satz)
/**
Includedatei einlesen.
**/
FILE *incl;
char *satz;
{
          int anz;
          FILE *incfile;
          char iname [256];
          char *konv;

          cr_weg (satz);
          anz = split (satz);
          if (anz < 2) return (-1);

          konv = getenv ("KONVERT");
          if (konv)
          {
                 sprintf (iname, "%s/%s", konv, wort[2]);
          }
          else
          {
                 strcpy (iname, wort[2]);
          }

          incfile = fopen (iname, "r");
          if (incfile == (FILE *) 0)
          {
                 fprintf (stderr, "Includedatei %s nichht gefunden\n", iname);
                 exit (1);
          }

          while (fgets (satz, 511, incfile))
          {
                  fputs (satz, incl);
          }
          fclose (incfile);
          return (0);
 }

int leseinclude (satz)
/**
Includes lesen.
**/
char *satz;
{
          FILE *incl;

          incl = fopen ("infdat.tmp", "w");
          if (incl == (FILE *) 0)
          {
                     fprintf (stderr, "Fehler beim Oeffnen von temp. Datei\n");
                     exit (1);
          }

          while (fgets (satz, 511, inf))
          {
                    if (memcmp (satz, "$INCLUDE", 8) == 0)
                    {
                               includefile (incl, satz);
                    }
                    else
                    {
                               fputs (satz, incl);
                    }
          }
          fclose (incl);
          fclose (inf);
          inf = fopen ("infdat.tmp", "r");
          if (inf == (FILE *) 0)
          {
                     fprintf (stderr, "Fehler beim oeffnenn");
                     exit (1);
          }
          return (0);
}

int testinclude ()
/**
Includes suchen.
**/
{
          char satz [516];

          while (fgets (satz, 511, inf))
          {
                    if (memcmp (satz, "$INCLUDE", 8) == 0)
                    {
                               fseek (inf, 0l, 0);
                               leseinclude (satz);
                               break;
                    }
          }
          fseek (inf, 0l, 0);
          return (0);
}

/***********

Ende Include-Datei einlesen   

*/

void CloseInf (void)
/**
InfoDatei schliessen.
**/
{
           if (inf)
           {
                  fclose (inf);
                  inf = NULL;
           }
}

void FreeTable (void)
/**
Datei tmpname loeschen.
**/
{
          if (tmpname[0])
          {
                   unlink (tmpname);
          }
}

FILE *OpenTable (char *tname)
/**
Temoraere inf-Datei erstellen, wenn eine Tabelle tname existiert.
**/
{
          char *tmp;
          char *p;
          FILE *fp;
          char buffer [256];
          char sqltext [256];
		  char tn [80];

          if (p = strchr (tname, '.')) *p = 0;
          if (!DBOK)
          {
                 opendbase ("bws"); 
				 if (sqlstatus != 0) return ((FILE *) 0);
                 DBOK = 1;
          }


          FreeTable ();
#ifndef BIWAK
          p = strchr (tname,'.');
          if (p) *p = (char) 0;
          strcpy (buffer, tname);
          p = strchr (buffer,',');
          if (p) *p = (char) 0;

#ifndef ODBC
          sprintf (sqltext, 
            "select tabid, ncols from systables where tabname = \"%s\"",
            buffer); 
          execute_sql (sqltext); 
          if (sqlstatus != 0) return ((FILE *) 0);
#endif
#else
          strcpy (buffer, tname);
          p = strchr (buffer,',');
          if (p) *p = (char) 0;

#endif

          tmp = getenv ("TMPPATH");
          if (tmp)
          {
                       sprintf (tmpname, "%s\\%s.inf",tmp, buffer);
          }
          else
          {
                       sprintf (tmpname, "%s", buffer);
          } 

          fp = fopen (tmpname,"w");
		  if (fp == NULL)
		  {
			  return NULL;
		  }
          fprintf (fp, "$TABLE\n%s\n", tname);
          fclose (fp);
          fp = fopen (tmpname, "r");
          return (fp);
}


int belege_ziel (info_name)
/**
Satzaufbau fuer Quelldatei holen.
**/
char *info_name;
{
          char satz [256];
          char dname [256];
          short i;
          long feldseek;

          tmpname[0] = (char) 0; 
          if (inf) fclose (inf);

          if (getenv ("KONVERT"))
          {
                   sprintf (dname, "%s\\%s", getenv ("KONVERT"), info_name); 
                   sprintf (dname, "%s\\%s", getenv ("KONVERT"), info_name); 
       
          }
          else
          {
                   writelog ("KONVERT nicht gefunden\n");
                   sprintf (dname, "%s", info_name); 
          } 
          inf = fopen (dname, "r");
          if (inf == 0) inf = fopen (info_name,"r");
          if (inf == 0) inf = OpenTable (info_name);
          if (inf == 0)
          {
                        writelog ("%s kann nicht geoeffnet werden\n",
                                   info_name);
                        exit (1);
          }

          tabname (inf, satz);   /* Tabellenname holen                       */
          feldseek = ftell (inf);
          feldanz (inf, satz);   /* Anzahl Felder feststellen                */
          ziel = malloc ((feld_anz + 2) * sizeof (FELDER));
          if (ziel == 0)
          {
                        writelog ("Kein Speicherplatz ziel\n");
                        exit (1);
          }
          fseek (inf,feldseek,0);
          ins_ziel (inf, satz);
          zlen = satzlen (ziel) + 2;
          ausgabesatz = malloc (zlen + 2);
          if (ausgabesatz == 0)
          {
                        writelog ("Kein Speicherplatz ausgabesatz\n");
                        exit (1);
          }
          memset (ausgabesatz, ' ', zlen);
          ausgabesatz[zlen] = (char) 0; 
          init_ausgabesatz ();
          fseek (inf,feldseek,0);
          belege_where_part (inf, satz); 
          belege_code (inf, satz);
          belege_data (inf, satz);
          belege_structs (inf, satz);
}

int belege_where_part (inf, satz)
/**
Nach Suchkriterium im Info_file suchen.
**/
FILE *inf;
char *satz;
{
          short pos;

          fseek (inf, 0l, 0);
          memset (where_part,0, 1024);
          fseek (inf,0l,0);
          while (fgets (satz,1023,inf))
          {
                     if (strupcmp (satz,"$SELECT",7) == 0)
                     { 
                                   cr_weg (satz);
                                   pos = first_ze (satz, 7);
                                   if (pos == 0) return;
                                   strcpy (where_part, &satz[pos]);
                     }
          }
          belege_where_part_upd (inf, satz);
}

int belege_where_part_upd (inf, satz)
/**
Nach Suchkriterium im Info_file suchen.
**/
FILE *inf;
char *satz;
{
          short pos;

          fseek (inf, 0l, 0);
          memset (where_part_upd,0, 1024);
          fseek (inf,0l,0);
          while (fgets (satz,1023,inf))
          {
                     if (strupcmp (satz,"$UPDATE",7) == 0)
                     { 
                                   cr_weg (satz);
                                   pos = first_ze (satz, 7);
                                   if (pos == 0) return;
                                   strcpy (where_part_upd, &satz[pos]);
                     }
          }
}

void setwhere_part_upd (char *where_upd)
{
          if (where_part_upd[0] == 0)
          {
                     memset (where_part_upd,0, 1024);
                     strcpy (where_part_upd, where_upd);
          }
}
           
           

int tabname (inf, satz)
/**
Tabellenname holen.
**/
FILE *inf;
char *satz;
{


          while (fgets (satz,255,inf))
          {
                     if (strupcmp (satz,"$TABLE",6) == 0)
                     { 
                                fgets (satz, 255, inf);
                                cr_weg (satz); 
                                strncpy (dbtab, satz,511);
                                dbtab [511] = 0;
                                return (0);
                     }
          }
          fprintf (stderr,"Tabellenname nicht gefunden\n");
          exit (1);
}

int ins_ziel (inf, satz)
/**
Felder fuer Satzaufbau fuellen.
**/
FILE *inf;
char *satz;
{
         short i;
         short (anz);

         i = 0;
         while (fgets(satz,255,inf))
         {
                  cr_weg (satz);
                  if (strupcmp (satz,"$SELECT",7) == 0) break;
                  if (strupcmp (satz,"$CODE",7) == 0) break;
                  if (strupcmp (satz,"$DATA",7) == 0) break;
                  anz = zsplit (satz,','); 
                  if (anz < 5) continue;
                  strncpy (ziel [i].feldname,clipped (zwort [1]),41);
                  ziel [i].feldname [41] = 0;
                  ziel [i].feldlen = atoi (zwort [2]) + 1;
                  strncpy (ziel [i].feldtyp,clipped (zwort [3]),1);
                  ziel [i].feldtyp [1] = 0;
                  ziel [i].nachkomma = atoi (zwort [4]);
                  ziel [i].dimension = atoi (zwort [5]);
                  i ++;
                  if (i == feld_anz) break; 
      }
      ziel [i].feldname [0] = 0;
      ziel [i].feldlen = 0;
      strcpy (ziel [i].feldtyp,"C");
      ziel [i].nachkomma = 0;
      ziel [i].dimension = 0;
      show_aufbau (ziel);
}

int belege_db ()
/**
Satzaufbau fuer Datenbanktabelle belegen.
**/
{
      int tabid;
      char tabstring [512];
      char *tab;
      char tabname [128];
      short felder;
      int tabcurs;
      int colcurs;
      char colname [128];
      short coltype;
      short collength;
	  long precision;
      short scale; 
      short i, j;
      int cursor;
      char sqltext [256];
      int dsqlstatus;
	  int ret;
      int colanz;


#ifdef BIWAK
	  DBTABLE *akttab;
#endif

      // prepare_sql ("select tabid from systables");

#ifndef BIWAK
      if (!DBOK)
      {
             opendbase ("bws"); 
             DBOK = 1;
      }
      i = 0;

      strcpy (tabstring, dbtab);
      tab = strtok (tabstring, ", ");
      if (tab == NULL) return (0);

      strcpy (tabname, tab);
#ifdef ODBC
	  sprintf (sqltext, "select * from %s", clipped (tabname));
	  tabcurs = prepare_sql (sqltext);
      dsqlstatus = fetch_sql (tabcurs);
#else
      ins_quest ((char *) tabname, 0, 21);
      sprintf ( sqltext, "select tabid, ncols from systables where tabname = ?");
      tabcurs = prepare_sql (sqltext);
      dsqlstatus = fetch_sql (tabcurs);
      if (dsqlstatus != 0) return 0;
#endif
#ifdef ODBC

      colanz = get_colanz(tabcurs); 

/*
             ret = SQLColAttributes(tabcurs, 0, 
				                    fDescType, rgbDesc, cbDescMax, pcbDesc, pfDesc); 
*/
      strcpy (TableTab[i], tabname);
      db_anz += colanz;
#else
      tabid  = isqlvalue (0);
      idtab[i] = isqlvalue (0);
      strcpy (TableTab[i], tabname);
      db_anz = ssqlvalue (1);
#endif
      i ++;
      while (tab = strtok (NULL, ", "))
      {
             strcpy (tabname, tab);
#ifdef ODBC
             close_sql (tabcurs);
	         sprintf (sqltext, "select * from %s", clipped (tabname));
	         tabcurs = prepare_sql (sqltext);
#endif
             open_sql (tabcurs);
             dsqlstatus = fetch_sql (tabcurs);
             if (dsqlstatus != 0)
             {
                 break;
             }
#ifdef ODBC
             colanz = get_colanz(tabcurs); 
             db_anz += colanz;
             strcpy (TableTab[i], tabname);
#else
             idtab[i]  = isqlvalue (0);
             strcpy (TableTab[i], tabname);
             db_anz += ssqlvalue (1);
#endif
             i ++;
      }
      close_sql (tabcurs);
      Tables = i;
#else
       Tables = 1;
       open_table (dbtab);
	   if (dbstatus != 0)
	   {
		   return -1;
	   }
	   akttab = Getakttab ();
	   db_anz = akttab->tabfields;
#endif

      quelle = malloc ((db_anz + 2) * sizeof (FELDER));
      if (quelle == 0)
      {
                        fprintf (stderr,"Kein Speicherplatz\n");
                        exit (1);
      }

      i = j = 0;
#ifdef BIWAK
	   for (i = 0; i < akttab->tabfields; i ++)
	   {
		   memcpy (quelle[i].feldname, akttab->record[i].name, 41);
		   quelle[i].feldname[41] = 0;
		   clipped (quelle[i].feldname);
           quelle [i].feldtyp[0] = akttab->record[i].type;
		   if (quelle [i].feldtyp[0] == 'C')
		   {
                  quelle [i].feldlen = akttab->record[i].length + 1;
		   }
		   else
		   {
                  quelle [i].feldlen = akttab->record[i].length;
		   }
		   quelle[i].feldtyp[1] = 0;
           quelle [i].nachkomma = akttab->record[i].nkomma;
           quelle [i].dimension = 1;
	   }
#else
      for (i = 0; i < Tables; i ++)
      {
          AktTable = &TableTab[i];
#ifdef ODBC
	      colcurs = prepare_columns (clipped (AktTable), NULL);
		  bind_sqlcol (colcurs, 4, (char *) colname,   SQLCCHAR, 128);
		  bind_sqlcol (colcurs, 5, (short *) &coltype, SQLCSHORT, 0);
		  bind_sqlcol (colcurs, 7, (long *) &precision, SQLCLONG, 0);
		  bind_sqlcol (colcurs, 8, (short *) &collength, SQLCSHORT, 0);
		  bind_sqlcol (colcurs, 9, (short *) &scale, SQLCSHORT, 0);
#else
          out_quest (colname,0,41);
          out_quest (&coltype,1,0);
          out_quest (&collength,1,0);
          ins_quest (&idtab[i],2,0);
          colcurs = prepare_sql ("select colname, coltype,collength from "
                                 "syscolumns where tabid = ?");
#endif
          dsqlstatus = fetch_sql (colcurs);
          while (dsqlstatus == 0)
          {
#ifdef ODBC
			     if (coltype == SQLDECIMAL)
				 {
					 collength = (precision << 8) | scale;
				 }
			     else if (coltype == SQLCHAR)
				 {
					 collength ++;
				 }
#endif
                 belege_quelle (&quelle [j],colname, coltype, collength);
                 j ++;
                 if (j == db_anz) break;
                 dsqlstatus = fetch_sql (colcurs);
          }
          close_sql (colcurs);
      }
      i = j; 
#endif
      quelle [i].feldname [0] = 0;
      quelle [i].feldlen = 0;
      strcpy (quelle [i].feldtyp,"C");
      quelle [i].nachkomma = 0;
      quelle [i].dimension = 0;
      show_aufbau (quelle); 
      qlen = satzlen (quelle);
      eingabesatz = malloc (qlen + 2);
      if (eingabesatz == 0)
      {
                    fprintf (stderr,"Kein Speicherplatz\n");
                    exit (1);
      }
      if (feld_anz == 0)
      {
                    feld_anz = db_anz;
                    feld_anz = ascii_typ (&ziel, quelle, feld_anz);
                    show_aufbau (ziel); 
                    zlen = satzlen (ziel) + 2;
                    ausgabesatz = malloc (zlen + 2);
                    if (ausgabesatz == 0)
                    {
                                  fprintf (stderr,"Kein Speicherplatz\n");
                                  exit (1);
                    }
      }
	  return 0;
}


#ifndef BIWAK
int belege_quelle (quelle, colname, coltype, collength)
/**
Satzstruktur fuer ein Datenbankfeld belegen.
**/
FELDER *quelle;
char *colname;
short coltype;
short collength;
{

      if (Tables > 1)
      {
               sprintf (quelle->feldname, "%s$%s", clipped (AktTable), clipped (colname));
      }
      else
      {
               strcpy (quelle->feldname, clipped (colname));
      }
	  coltype &= 0xFF;
      switch (coltype)
      { 
                case SQLVCHAR :
			 	          collength &= 0xFF;
                case SQLCHAR :
                          quelle->feldlen = collength + 1;
                          strcpy (quelle->feldtyp,"C");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                case SQLSMINT :
                          quelle->feldlen = sizeof (short);
                          strcpy (quelle->feldtyp,"S");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                case SQLINT :
#ifndef ODBC
                case SQLSERIAL :
#endif
                          quelle->feldlen = sizeof (long);
                          strcpy (quelle->feldtyp,"I");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                case SQLDECIMAL :
#ifndef ODBC
                case SQLMONEY :
#endif
                          quelle->feldlen = sizeof (double);
                          strcpy (quelle->feldtyp,"D");
                          quelle->nachkomma = collength;
                          quelle->dimension = 1;
                          break;
                case SQLFLOAT :
                          quelle->feldlen = sizeof (double);
                          strcpy (quelle->feldtyp,"D");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
#ifndef ODBC
                case SQLDATE :
                          quelle->feldlen = 11;
                          strcpy (quelle->feldtyp,"d");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
#else
                case SQLDATE :
					      quelle->feldlen = sizeof (DATE_STRUCT);
                          strcpy (quelle->feldtyp,"G");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                case SQLTIME :
					      quelle->feldlen = sizeof (TIME_STRUCT);
                          strcpy (quelle->feldtyp,"J");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
                case SQLTIMESTAMP :
					      quelle->feldlen = sizeof (TIMESTAMP_STRUCT);
                          strcpy (quelle->feldtyp,"K");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
#endif
                default :
                          quelle->feldlen = collength + 1;
                          strcpy (quelle->feldtyp,"C");
                          quelle->nachkomma = 0;
                          quelle->dimension = 1;
                          break;
           }
}
#endif

static char bezkurz [11];
static char name [42];

char *GetItemName (char *feldname)
/**
Bezeichnung aus Tabelle Item zu Feldname holen.
**/
{
#ifdef BIWAK
	       return NULL;
#else
#ifdef ODBC
	       return NULL;
#else
           static int icursor = -1;

           if (icursor == -1)
           {
                        out_quest ((char *) bezkurz, 0, 11);
                        ins_quest ((char *) name, 0, 41);
                        icursor = prepare_sql ("select bezkurz from item where name = ?");
           }
           strcpy (name, feldname);
           open_sql (icursor);
           fetch_sql (icursor);
           if (sqlstatus == 0 && strcmp (bezkurz, " ") > 0)
           {
                        return (bezkurz);
           }
           return (NULL);
#endif
#endif
}

            

           


