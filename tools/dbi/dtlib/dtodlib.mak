# Makefile "maketest": test Testprogramm
# BIZERBA Balingen VI-WW-B

.SUFFIXES: .obj .cpp

#CC   = bcc -ml
CC = cl
#LINK = bcc
LIB = lib

CFLAGS = -c -W3 /Zi -D_SWU 
PROGRAMM = dtod.lib 
RCVARS=-r -DWIN32

OBJ =  mo_intp.obj \
        strfkt.obj \
        mo_sw.obj \
        mo_swkop.obj \
        mo_proct.obj

$(PROGRAMM): $(OBJ)
         $(LIB) /out:$(PROGRAMM) $(OBJ)
.cpp.obj :
	$(CC) $(CFLAGS) $*.cpp
.c.obj :
        $(CC) $(CFLAGS) $*.c
