/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Geisental 12, 4630 Bochum 1, Tel: 0234/5065-250
-
-----------------------------------------------------------------------------

/* Eingabestruktur fuer preis_hol          */

typedef struct 
         {short mdn;
          short fil;
          short kun_fil;
          int kun;
          double a;
          char datum [11];
         } PREISIN;

/* Ausgabestruktur fuer Preishol          */

typedef struct
         {short sa;
          double pr_ek;
          double pr_vk;
         } PREISOUT;

/* Funktionen zum Preisholen

int preis_hol (preisin, preisout)
PREISIN  *preisin;
PREISOUT *preisout;

int preise_holen (dmdn, dfil, dkun_fil, dkun, da, ddatum, sa, pr_ek, pr_vk)
short  dmdn;
short  dfil;
short  dkun_fil;
int    dkun;
double da;
char *ddatum;
short *sa;
double *pr_ek;
double *pr_vk;
*/
