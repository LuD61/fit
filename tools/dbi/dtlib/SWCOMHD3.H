/* Struktur fuer Headersatz SW-Waage  Version 3.0              */


/* Struktur fuer die Informationen ueber ein Feld             */

typedef struct
	{       char     att_symb [4];
		ushort   stellen;
		TYint64  minWert;       /* Version 3.00 */
		TYint64  maxWert;       /* Version 3.00 */
		uchar    typ;
		uchar    key;
		ushort   bits1;         /* um 2 By laenger seit 28.09.92 */
	} TYdbmATT3;                    /* #1628 Version 3.00 */

/* Headersatz des Scnittstellenfiles                    */
/* Version 3.00                                         */

struct HD3
{
     ushort        klen;
     char          dname[14];
     ushort        vz;
     ushort        rz;
     long          dttm;
     char          prnr [12];
     char          kun [80];
     char          sprname[40]; 
     ushort        vSW;
     ushort        rSW;
     long          dttmSW;
     char          prnrSW [12];
     uchar         pafeld [80];
     char          reserve [80];
     ushort        nrziel;
     ushort        feldanz;
     TYdbmATT3     feldbez [100];
     ushort        blklen;
     unsigned long blkanz;
};

short bpos [MAXFELDER];                 /* Feldpositionen im Satz   */
short blen [MAXFELDER];                 /* Feldlaenge               */
short btyp [MAXFELDER];                 /* Feldtyp                  */
extern short banz;                      /* Anzahl Felder im Satz    */
