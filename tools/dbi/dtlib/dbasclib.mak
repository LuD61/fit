# Makefile "maketest": test Testprogramm
# BIZERBA Balingen VI-WW-B

.SUFFIXES: .obj .cpp

#CC   = bcc -ml
CC = cl
#LINK = bcc
LIB = lib

CFLAGS = -c -W3 /Zi -D_DBASE
PROGRAMM = dbasc.lib 
RCVARS=-r -DWIN32

OBJ =  mo_intp.obj \
        datum.obj \
        strfkt.obj \
        mo_sw.obj \
        mo_swkop.obj \
        mo_bwsasc.obj \
        sysdate.obj \
        mo_curso.obj \
        mo_proct.obj

$(PROGRAMM): $(OBJ)
         $(LIB) /out:$(PROGRAMM) $(OBJ)
.cpp.obj :
	$(CC) $(CFLAGS) $*.cpp
.c.obj :
        $(CC) $(CFLAGS) $*.c
