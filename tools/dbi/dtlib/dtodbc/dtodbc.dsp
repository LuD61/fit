# Microsoft Developer Studio Project File - Name="dtodbc" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=dtodbc - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "dtodbc.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "dtodbc.mak" CFG="dtodbc - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "dtodbc - Win32 Release" (basierend auf  "Win32 (x86) Static Library")
!MESSAGE "dtodbc - Win32 Debug" (basierend auf  "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe

!IF  "$(CFG)" == "dtodbc - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_DBASE" /YX /FD /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "dtodbc - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /w /W0 /GX /Z7 /Od /D "_DEBUG" /D "ODBC" /D "WIN32" /D "_WINDOWS" /D "_DBASE" /YX /FD /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\..\dbinfot\dtodbc.lib"

!ENDIF 

# Begin Target

# Name "dtodbc - Win32 Release"
# Name "dtodbc - Win32 Debug"
# Begin Source File

SOURCE=..\a_pr.c
# End Source File
# Begin Source File

SOURCE=..\akt_krz.c
# End Source File
# Begin Source File

SOURCE=..\aktion.c
# End Source File
# Begin Source File

SOURCE=..\DATUM.C
# End Source File
# Begin Source File

SOURCE=..\dbclass.c
# End Source File
# Begin Source File

SOURCE=..\MO_A_PR.C
# End Source File
# Begin Source File

SOURCE=..\mo_bwsasc.c
# End Source File
# Begin Source File

SOURCE=..\MO_INTP.C
# End Source File
# Begin Source File

SOURCE=..\MO_PREIS.C
# End Source File
# Begin Source File

SOURCE=..\MO_SPAWN.C
# End Source File
# Begin Source File

SOURCE=..\MO_SW.C
# End Source File
# Begin Source File

SOURCE=..\MO_SWKOP.C
# End Source File
# Begin Source File

SOURCE=..\mo_wprocs.c
# End Source File
# Begin Source File

SOURCE=..\STRFKT.C
# End Source File
# Begin Source File

SOURCE=..\SYSDATE.C
# End Source File
# End Target
# End Project
