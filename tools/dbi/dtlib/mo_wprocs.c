/*
  Dummy-Modul fuer mit Definition der Tabelle Spezialfunktionen fuer
  Standard asc_bws, bws_asc, dtod
*/ 

/* Structur fuer Proceduren und Funktionen    */
#include <windows.h>
#include "strfkt.h"

int cmessageOK (char *[]);
int ctoclipboard (char *[]);
int cfromclipboard (char *[]);
int cdde (char *[]);

int (*RunDde) (char *, char *, char *, char *, char *) = NULL;

struct PROC
      {
        char *procname;
        int (*procadr) ();
      };

struct PROC proctab [] = { "OKBox",         cmessageOK,
                           "ToClipboard",   ctoclipboard,
                           "FromClipboard", cfromclipboard,
                           "Dde",           cdde,
                            NULL,     NULL,  
};


void SetRunDde (int (*p) (char *, char *, char *, char *, char *))
{
    RunDde = p;
}

int cmessageOK (char *params[])
{
    if (params[0] == NULL)
    {
        return 0;
    }

    if (params[1] == NULL)
    {
        MessageBox (NULL, params[0], "", MB_OK);
    }
    else
    {
        MessageBox (NULL, params[0], params[1], MB_OK);
    }
    return 0;
}


int ctoclipboard (char *params[])
{
          HGLOBAL hb;
          LPVOID p;
          char *text;
          int tlen;
          int i;

          for (i = 0, tlen = 0; params[i] != NULL; i ++)
          {
              tlen += strlen (params[i]) + 1;
          }
          text = (char *) malloc (tlen + 1);
          strcpy (text, "");
          for (i = 0, tlen = 0; params[i] != NULL; i ++)
          {
              if (i > 0)
              {
                  strcat (text, "\n");
              }
              strcat (text, params[i]);
          }

          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (NULL);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
          free (text);
          return (0);
}

int cfromclipboard (char *params[])
{
          HGLOBAL hb;
          LPVOID p;
          char *text;

          if (params[0] == NULL) return 0;

		  text = feldadr (params[0]);
          if (text == NULL) return 0;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (NULL);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          return 0;
}

int cdde (char *params[])
{
    int i;
    char *Server = NULL;

    for (i = 0; i < 5; i ++)
    {
        if (params[i] == NULL)
        {
            MessageBox (NULL, "Ung�ltige Anzahl Argumente f�r Dde\n"
                              "Aufruf : Dde (Server, Service, Topic, Item, Wert)", "", 
                               MB_OK ); 
            return 0;
        }
    }

    if (RunDde == NULL)
    {
        MessageBox (NULL, "RunDde nicht gefunden", "", MB_OK);
    }

    if (strcmp (params[0], "NULL") != 0)
    {
        Server = params[0];
    }

    (*RunDde) (Server, params[1], params[2], params[3], params[4]);
    return 0;
}
