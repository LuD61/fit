#ifndef _DBCLASS_DEF
#define  _DBCLASS_DEF
#ifdef WIN32
#include <windows.h>
#include <odbcinst.h>
#endif
#include <sql.h>
#include <sqlext.h>

#ifndef MAXCURS
#define MAXCURS 500
#endif

#ifndef MAXVARS
#define MAXVARS 0x1000
#endif

#define SQLCHAR SQL_CHAR
#define SQLSMINT SQL_SMALLINT
#define SQLINT SQL_INTEGER
#define SQLSERIAL SQL_INTEGER
#define SQLDECIMAL SQL_DECIMAL
#define SQLFLOAT SQL_DOUBLE
#define SQLSMFLOAT SQL_FLOAT
#define SQLVCHAR SQL_VARCHAR
// #define SQLDATE SQL_DATETIME
#define SQLDATE SQL_DATE
#define SQLTIME SQL_TIME
#define SQLTIMESTAMP SQL_TIMESTAMP

#define SQLCCHAR   0
#define SQLCSHORT  1
#define SQLCLONG   2
#define SQLCDOUBLE 3
#define SQLCDATE 4
#define SQLCTIME 5
#define SQLCTIMESTAMP 6
#define SQLCVCHAR 7

#define NEXT 1
#define PRIOR 2
#define PREVIOUS 2
#define FIRST 3
#define LAST 4
#define CURRENT 5
#define RELATIV 7
#define DBABSOLUTE 6

typedef struct
{
           void *var;
           int typ;
           int len;
} SQLVAR;


#ifdef __cplusplus
extern "C" {
#endif

extern int sqlstatus;
extern int sqlerror[];
extern short sql_mode;
extern char sql[];
extern char SqlErrorText[];

extern BOOL short_null (short);
extern BOOL long_null (long);
extern BOOL double_null (double);

int FromOdbcDate (DATE_STRUCT *, char *);
int FromGerDate (DATE_STRUCT *, char *);
int ToGerDate (DATE_STRUCT *, char *);

int FromOdbcTime (TIME_STRUCT *, char *);
int FromGerTime (TIME_STRUCT *, char *);
int ToGerTime (TIME_STRUCT *, char *);

int FromOdbcTimestamp (TIMESTAMP_STRUCT *, char *);
int FromGerTimestamp (TIMESTAMP_STRUCT *, char *);
int ToGerTimestamp (TIMESTAMP_STRUCT *, char *);

void SetSqlErrorProc (BOOL (*ErrProc) (SDWORD, char *));
void set_sqlproc (int (*) (void));
int opendbase (char *);
int closedbase (char *);
int select_db (char *);
int sqlconnectdbase (char *, char *, char *, char *);
int sqlconnect (char *, char *, char *);
int beginwork ();
int commitwork ();
int rollbackwork ();
int execute_sql (char *);
int execute_db (char *, char *);
int prepare_tables (char *);
int prepare_columns (char *, char *);
int prepare_sql (char *);
int prepare_db (char *,char *);
int bind_sqlcol (int, int, void *, int, int);
int open_sql (int);
int fetch_sql (int);
int execute_curs (int);
int close_sql (int);
void ins_quest (void *, int , int);
void out_quest  (void *, int , int);
long _sysdate ();
long dasc_to_long (char *);
int dlong_to_asc (long, char *);
int dlong_to_asc_p (long,char *);
int get_colanz (int);
int get_collength (int, int);
int get_coltype (int, int);
char *get_colname (int, int, char *);
int get_colvalue (int, int, int, void *, int);
void GetError (HSTMT);
#ifdef __cplusplus
}
#endif
#endif
