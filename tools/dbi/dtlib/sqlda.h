/***************************************************************************
 *
 *			   INFORMIX SOFTWARE, INC.
 *
 *			      PROPRIETARY DATA
 *
 *	THIS DOCUMENT CONTAINS TRADE SECRET DATA WHICH IS THE PROPERTY OF 
 *	INFORMIX SOFTWARE, INC.  THIS DOCUMENT IS SUBMITTED TO RECIPIENT IN
 *	CONFIDENCE.  INFORMATION CONTAINED HEREIN MAY NOT BE USED, COPIED OR 
 *	DISCLOSED IN WHOLE OR IN PART EXCEPT AS PERMITTED BY WRITTEN AGREEMENT 
 *	SIGNED BY AN OFFICER OF INFORMIX SOFTWARE, INC.
 *
 *	THIS MATERIAL IS ALSO COPYRIGHTED AS AN UNPUBLISHED WORK UNDER
 *	SECTIONS 104 AND 408 OF TITLE 17 OF THE UNITED STATES CODE. 
 *	UNAUTHORIZED USE, COPYING OR OTHER REPRODUCTION IS PROHIBITED BY LAW.
 *
 *
 *  Title:	sqlda.h
 *  Sccsid:	@(#)sqlda.h	9.2	1/12/92  12:10:21
 *  Description:
 *		SQL Data Description Area
 *
 ***************************************************************************
 */


#ifndef _SQLDA
#define _SQLDA

#ifdef __BORLANDC__
#pragma option -a8
#else
#pragma pack (8)
#endif

#ifdef __cplusplus
extern "C" {
#endif

struct sqlvar_struct
    {
    short sqltype;		/* variable type		*/
    short sqllen;		/* length in bytes		*/
    char *sqldata;		/* pointer to data		*/
    short *sqlind;		/* pointer to indicator		*/
    char  *sqlname;		/* variable name		*/
    char  *sqlformat;		/* reserved for future use 	*/
    short sqlitype;		/* ind variable type		*/
    short sqlilen;		/* ind length in bytes		*/
    char *sqlidata;		/* ind data pointer		*/
    };

struct sqlda
    {
    short sqld;
    struct sqlvar_struct *sqlvar;
    char desc_name[19];		/* descriptor name 		*/
    short desc_occ;		/* size of sqlda structure 	*/
    struct sqlda *desc_next;	/* pointer to next sqlda struct */
    };

#ifdef __cplusplus
}
#endif

#ifdef __BORLANDC__
#pragma option -a-
#else
#pragma pack ()
#endif

#endif /* _SQLDA */
