#include <stdio.h>
#include <windows.h>
#include "wmaskc.h"



int wcode_debug (iff *ifs, short mit_debug)
{
 char anweisung [80];

 if (mit_debug == 0)
   return (0);

 if (ifs->bedingung)
   printf ("%s ", ifs->bedingung);
 if (ifs->anweisung)
   printf ("%s ", ifs->anweisung);
 printf ("%hd\n", ifs->bflag);
 gets (anweisung);
 if (memcmp (anweisung, "dis", 3) == 0)
 {
  show_feld (anweisung);
 }
 if (memcmp (anweisung, "con", 3) == 0)
 {
  mit_debug = 0;
 }
 return (mit_debug);
}

