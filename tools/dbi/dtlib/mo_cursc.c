/***

--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       mo_cursor.ec
-
-       Autor                   :       Wille
-       Erstellungsdatum        :       09.02.93
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ESQL-C
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Aktionen mit SQL - Informix.
-                                   Modul fuer Informix 5.01
-
--------------------------------------------------------------------------------
***/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <malloc.h> 
#include <sqlhdr.h>
#include <sqlproto.h>
#include <login.h>
#include "tcursor.h"
#include "mo_curso.h"
#include "strfkt.h"

#define PREPARE  "prepare"
#define DECLARE  "declare"
#define DESCRIBE "describe"
#define OPEN     "open"
#define FETCH    "fetch"
#define EXECUTE  "execute"
#define INSERT   "insert"

#define SQLFEHLER(b, s, l) if (sqlca.sqlcode b) _SQLF(s,l);

extern _SQCURSOR *_iqnprep();

void opendbase ();
void closedbase ();

#define MAXCURS 500

#ifndef UCHAR
#define UCHAR unsigned char
#endif

char SHORTNULL []  = {(UCHAR) 0x00,(UCHAR) 0x80};
char LONGNULL []   = {(UCHAR) 0x00, (UCHAR) 0x00, (UCHAR) 0x00, (UCHAR) 0x80};
char DOUBLENULL [] = {(UCHAR) 0xff, (UCHAR) 0xff, (UCHAR) 0xff,
                      (UCHAR)  0xff,(UCHAR) 0xff, (UCHAR) 0xff,
                      (UCHAR) 0xff, (UCHAR) 0xff};


char *modtext [] = {"SCROLL", "HOLD", 0}; 
int cumods []    = {0x20, 0x1000};
       
short sql_mode = 0;              /* Flag fuer Vorgehensweise bei SQL-Fehlern */
                                 /* 0 = Abbruch                              */
                                 /* 1 = Continue                             */
                                 /* 2 = Fehlermedlung in SQL-Protokoll       */
FILE *sqlprot = (FILE *) 0;      /* Stream fuer SQLfehler-Protokoll          */

int (*sqlerrprog) () = 0;        /* Zeiger auf Procedure fuer Sqlfehler      */

static short erster_aufruf = 1;  /* Kennzeichen fuer 1. Auftruf              */
       
typedef union 
    {  
         char   *fchar; 
         short  *fshort; 
         int    *fint;
         double *fdouble;
         void   *fvoid;
     } TYADR;

static TYADR feld;

static _SQCURSOR *sqcursor [MAXCURS];
static _SQCURSOR *sqprepcurs [MAXCURS];
static CURSOR rcurs[MAXCURS];
static char *curssql[MAXCURS];
static struct sqlvar_struct *cursivars [MAXCURS];
static struct sqlvar_struct *cursovars [MAXCURS];
static short cursnivars [MAXCURS];
static short cursnovars [MAXCURS];
static OP_CURS quest_curs[MAXCURS]; 
static short cusize [MAXCURS];
struct sqlda *q_desc = 0;
char sql [4096];
char sqlt [4096];
char sqlt1 [4096];

int size_malloc;
static char *cu_malloc (int);

static short cu_modus = 0;        /* Cursor Modus Scroll oder Normal */
static short in_work = 0;         /* Merker fuer Transaktion         */
static char  tabname [18];        /* Tabellenname                    */
static int   tabid;               /* Tabid einer Tabelle             */
static char  colname [18];        /* Feldname eines Tabellenfeldes   */
static short coltype;             /* Feldtyp eines Tabellenfeldes    */
static short collength;           /* Feldlaenge eines Tabellenfeldes */

static char old_tab [20];          /* letzter Tabellenname            */
static char old_col [20];          /* letzter Feldname                */
static short coldec = 0;

static short tab_id;              /* Cursor fuer Systables           */
static short cols;                /* Cursor fuer Syscolumns          */

static short varptr = 0;       /* Zeiger auf Position in var_buf          */
static short outptr = 0;       /* Zeiger auf Position in out_buf          */

static SVAR varbuff [MAX_VARS];   /* Buffer fuer SQL-Eingabevariable      */
static SVAR outvar  [MAX_VARS];   /* Buffer fuer SQL-Ausgabevariable      */

static int prepcurs (char *, ...);/* Cursor vorbereiten                   */
static int opencurs (short);      /* Cursor oeffnen                       */
static int fetchcurs (short, int, ...);
                                  /* mit Cursor lesen                     */
static int executecurs (short);   /* Cursor ausfeuhern                    */
static int insertcurs (short);    /* mit Cursor einfuegen                 */
static int closecurs (short);     /* Cursor schliessen                    */

static int test_quest (char *, short);
                                  /* ? in sqltext suchen fuer Eingabe      */
static int test_out (short);
                                  /* Adressen fuer Ausgabevariablen suchen  */
static void quest_free (int);     /* Speicher freigeben                     */

static int upd_cursor (char *);   /* Test auf update-Cursor                 */
static int ins_cursor (char *, short);  /* Test auf insert-Cursor                 */
static char *doubleform (char *, short);
                                  /* Format fuer Decimal-werte erstellen    */

static void begin_work (void);
static void commit_work (void);
static void rollbwack_work (void);

/* Cursor zum initialisieren                */
static CURSOR  icurs  = {0,0,0,0,0,0,prepcurs,
                                     opencurs,
                                     fetchcurs,
                                     closecurs ,
                                     executecurs};
static short cursinit  = 0;
static char cunamen [MAXCURS][13];   /* Cursor-Namen                        */
static char prepnamen [MAXCURS][13];  /* Cursor-Namen                        */

static short cursor_stack[MAXCURS];   /* Cursor-Stack                       */
static short cstptr = MAXCURS - 1;    /* Zeiger fuer Cursor-Stack           */
static short cu_nr;                   /* Cursor-Nr                          */

static short formate;
static FORMAT forms[256];
short sqlfehler;
short DEBUG = 0;
short SQLDEBUG = 0;
static FILE *sqldebug = stderr;
static short jahrh = 1900;
static int Transaction = 1;

void SetTransaction (int t)
{
    Transaction = t;
}

int GetTransaction ()
{
    return (Transaction);
}


static short spez_typ [] = {0,1,2,3,3,3,2,7,3,9,10};
                         /* Tabelle fuer Feldtypkonvertierung       */

static char *typ_string [] = {"char",
                              "smallint",
                              "integer",
                              "float",
                              "smallfloat",
                              "decimal",
                              "serial",
                              "date",
                              "money",
                              (char *) 0}; 
                       
void _SQLF (char *, short);
static void init_sql (void);
static void init_curs (void);
static int cursfree (int);
static int declare_col (void);
static int lese_col (char *, char *);
static int from_sqlchar (char *, short, void *);
static int from_sqlshort (short, short, void *);
static int from_sqlint (long, short, void *);
static int from_sqldouble (double, short, void *, int);
static void floatkomma (char *);
static void decimalkomma (char *);
static int var_type (short);
static void init_sqlout (void);
static void init_sqlin (void);
void set_sqlproc (int (*) ());
static int showivars (short);
static int showovars (short);
static int showvar (char *, short, struct sqlvar_struct *);

int execute_sql (char *sqltext, ...)
/**
Cursor vorbereiten ausfuehren und schliessen.
**/
{
          va_list args;
          static short exe_curs = -1;

          if ((exe_curs > -1) && 
              (rcurs[exe_curs].preped))
          {
                     close_sql (exe_curs);
          }

          va_start (args, sqltext);
          vsprintf (sqlt1,sqltext,args);
          va_end (args);
          exe_curs  = prepare_sql (sqlt1);
          if (sqlstatus) return (sqlstatus);
          cu_modus = 0;
          if (rcurs [exe_curs].cutyp == 'U')
          {
                     return (execute_curs (exe_curs));
          }
          if (rcurs [exe_curs].cutyp == 'I')
          {
                     return (execute_curs (exe_curs));
          }
          return (fetch_sql (exe_curs));
}

void cursor_modus (int start, ...)
/**
Cursor - Modus Scroll oder Hold setzen.
**/
{
          va_list args;
          char *modus;
          short j;

          cu_modus = 0;
          va_start (args, start);
          modus = va_arg (args, char *);
          while (modus)
          {
                   upstr (modus); 
                   for (j = 0; modtext [j]; j ++)
                   {
                           if (strcmp (modus, modtext[j]) == 0)
                           {
                                       cu_modus |= cumods [j];
                                       break;
                           }
                   }
                   modus = va_arg (args, char *);
         }
}
                               
 

int prepare_spez ( int modus, char *sqltext, ...)
/**
Scroll Cursor vorbereiten und Cursor-Nummer zurueckgeben.
**/
{
          va_list args;
          short scroll_curs;

          va_start (args, sqltext);
          vsprintf (sqlt,sqltext,args);
          va_end (args);
          cu_modus = modus;
          scroll_curs = prepare_sql (sqlt);
          cu_modus = 0;
          if (sqlstatus) return (sqlstatus);
          return (scroll_curs);
}

int prepare_scroll (char *sqltext, ...)
/**
Scroll Cursor vorbereiten und Cursor-Nummer zurueckgeben.
**/
{
          va_list args;
          short scroll_curs;

          va_start (args, sqltext);
          vsprintf (sqlt,sqltext,args);
          va_end (args);
          cu_modus = 0x20;
          scroll_curs = prepare_sql (sqlt);
          cu_modus = 0;
          if (sqlstatus) return (sqlstatus);
          return (scroll_curs);
}

int prepare_sql (char *sqltext, ...)
/**
Cursor vorbereiten und Cursor-Nummer zurueckgeben.
**/
{
       va_list args;
       short i;
       int status;

       if (cursinit == 0)
       {
                  init_curs ();
       }
       for (i = 0; i < 100; i ++)
       {
                  if (rcurs[i].preped == 0)
                  {
                                 break;
                  }
       }

       if (i == 100)
       {
                 return (-1);
       }
       cu_nr = i;
       va_start (args, sqltext);
       vsprintf (sqlt,sqltext, args);
       va_end (args);
       status = (*rcurs[i].p_curs) (sqlt);
       if (sqlstatus) return (sqlstatus);
       cursor_stack [cstptr] = cu_nr;
       cstptr --;
       return (status);
}

int open_sql (short nr)
/**
Cursor lesen.
**/
{
       int status;

       if (rcurs[nr].preped == 0)
       {
                 return (-1);
       }
 
       cu_nr = nr;
       status = (*rcurs[nr].o_curs) (nr);
                     
       return (status);
}


int fetch_sql (short nr)
/**
Cursor lesen.
**/
{
 
       int status;

       if (rcurs[nr].preped == 0)
       {
                 return (-1);
       }
 
       cu_nr = nr;
       status = (*rcurs[nr].f_curs) (nr, 1);
       return (status);
}

int fetch_scroll (short nr, int modus, ...)
/**
Cursor lesen.
**/
{
 
       int status;
       va_list args;
       int pos;

       if (rcurs[nr].preped == 0)
       {
                 return (-1);
       }
 
       cu_nr = nr;
       if (modus == RELATIV | modus == DBABSOLUTE)
       {
                va_start (args, modus); 
                pos = va_arg (args, int);
                va_end (args);
                status = (*rcurs[nr].f_curs) (nr, modus, pos);
       }
       else
       {
                status = (*rcurs[nr].f_curs) (nr, modus);
       }
       return (status);
}


int execute_curs (short nr)
/**
Cursor ausfuehren.
**/
{
 
       int status;

       if (rcurs[nr].preped == 0)
       {
                 return (-1);
       }
 
       cu_nr = nr;
       status = (*rcurs[nr].e_curs) (nr);
       return (status);
}


int close_sql (short nr)
/**
Cursor schliessen.
**/
{
     
       int status;

       if ((nr < 0) || (nr >= MAXCURS))
       {
                 return (-1);
       }

       if (rcurs[nr].preped == 0)
       {
                 return (-1);
       }
       select_curs (nr);
       if (cstptr < MAXCURS - 1)
       {
                 cstptr ++;
       }
       status = (*rcurs[nr].c_curs) (nr);
       if (cstptr < MAXCURS)
       {
                  cu_nr = cursor_stack [cstptr + 1];
       }
       return 0;
}


int close_all_cursor ()
{
	   int i;

	   for (i = 0; i < MAXCURS; i ++)
	   {
		   close_sql (i);
	   }
	   return 0;
}

void createdbase (char *dbname)
/**
Datenbank anlegen.
**/
{
  _iqdbase(dbname, 0);
  SQLFEHLER (!= 0, "Fehler beim Anlegen der Datenbank",__LINE__)
}

void dropdbase (char *dbname)
/**
Datenbank loeschen.
**/
{
  static char dropstring [40]; 
  static char *sqlcmdtxt[2];
  static _SQSTMT _SQ0 = {0};

  sprintf (dropstring,"drop database %s", dbname);
  sqlcmdtxt [0] = (char *) dropstring;  
  sqlcmdtxt [1] = (char *) 0;  
  _iqstmnt(&_SQ0, (const char **)
                  &sqlcmdtxt[0], 0, (struct sqlvar_struct *) 0,
                  (struct value *) 0);
  SQLFEHLER (!= 0, "Fehler beim Loeschen der Datenbank",__LINE__)
}

int prepcurs (char *sqltext, ...)
/**
Cursor vorbereiten.
**/
{
/*
        short i;
        short nr;
        short quest = 0;
        va_list args;

        va_start (args, sqltext);
        vsprintf (sql, sqltext, args);
        va_end (args);
        if (erster_aufruf) init_sql ();
        nr = cu_nr;
        cusize [nr] = 0;
        if (rcurs[nr].preped)
        {
                    close_sql (nr);
        }
        debug_sql (sql);
        test_quest (sql,nr);
        sqprepcurs [nr] = _iqnprep(prepnamen [nr], sql);

        sql_prot (nr, PREPARE);
        SQLFEHLER (!= 0, "Fehler beim Prepare",__LINE__)
        if (sqlstatus) return (sqlstatus);
        if (DEBUG) showcurs (nr, 1);
        save_sql (sql, nr); 
        if (upd_cursor (sql))
        {
                     rcurs[nr].sqlwerte = 0;
                     rcurs[nr].vind = 0;
                     rcurs[nr].vformat = 0;
                     rcurs [nr].cutyp = 'U';
                     rcurs[nr].preped = 1;
                     return (nr);
        }
        if (ins_cursor (sql,nr) == 0)
        {

                     if (!sqprepcurs [nr]) 
                                  sqprepcurs [nr] = 
                                     _iqlocate_cursor(prepnamen [nr], 1);
                      _iqdescribe(sqprepcurs [nr], &q_desc, (char *) 0);

                     sql_prot (nr, DESCRIBE);
                     SQLFEHLER (!= 0, "Fehler beim describe",__LINE__)
                     if (sqlstatus) return (sqlstatus);
                     rcurs[nr].q_desc = q_desc;
                     if ((rcurs[nr].sqlwerte = sqlmalloc (nr)) == 0)
                     {
                                 return (-1);
                     }

                     cu_nr = nr;
                     for (i = 0; i < cfields; i ++)
                     { 
                                 cfieldinf[i].sqldata = 
                                       &rcurs[nr].sqlwerte[csqlind];
                     }

                     test_out (nr); 
       }
       if (!sqcursor [nr]) 
              sqcursor [nr] = _iqlocate_cursor(cunamen [nr], 0);
       if (!sqprepcurs [nr]) 
              sqprepcurs [nr] = _iqlocate_cursor(prepnamen [nr], 1);
       _iqcddcl(sqcursor [nr], cunamen [nr] , sqprepcurs [nr], cu_modus);
        sql_prot (nr, DECLARE);
        SQLFEHLER (!= 0, "Fehler beim Prepare",__LINE__)
        if (sqlstatus) 
		{
			// close_sql (nr);
			return (sqlstatus);
		}
        rcurs[nr].preped = 1;
        return (nr);
*/
        short i;
        short nr;
        short quest = 0;
        va_list args;
        int dsqlstatus;

        va_start (args, sqltext);
        vsprintf (sql, sqltext, args);
        va_end (args);

        if (erster_aufruf) init_sql ();
       
        nr = cu_nr;
        cusize [nr] = 0;
        if (rcurs[nr].preped)
        {
                    close_sql (nr);
        }
        debug_sql (sql);
        test_quest (sql,nr);
        sqprepcurs [nr] = _iqnprep((char *) prepnamen [nr], sql);
  
        dsqlstatus = sqlstatus;
        sql_prot (nr, PREPARE);
        SQLFEHLER (!= 0, "Fehler beim Prepare",__LINE__)
        if (sqlstatus) return (sqlstatus);
        if (DEBUG) showcurs (nr, 1);
        save_sql (sql, nr); 
        if (upd_cursor (sql))
        {
                     rcurs[nr].sqlwerte = 0;
                     rcurs[nr].vind = 0;
                     rcurs[nr].vformat = 0;
                     rcurs [nr].cutyp = 'U';
                     rcurs[nr].preped = 1;
                     return (nr);
        }

        rcurs[nr].sqlwerte = 0;
        rcurs[nr].vind = 0;
        rcurs[nr].vformat = 0;

        if (ins_cursor (sql,nr) == 0)
        {
                     _iqdescribe(_iqlocate_cursor(prepnamen [nr], 101), &q_desc, (char *) 0);
                     if (sqlstatus) return (sqlstatus);
                     rcurs[nr].q_desc = q_desc;

                     if ((rcurs[nr].sqlwerte = sqlmalloc (nr)) == 0)
                     {
                                 return (-1);
                     }


                     cu_nr = nr;

                     for (i = 0; i < cfields; i ++)
                     { 
                                 cfieldinf[i].sqldata = 
                                       &rcurs[nr].sqlwerte[csqlind];
                     }


                     test_out (nr); 
       }

       _iqcddcl(_iqlocate_cursor(cunamen [nr], 0), cunamen [nr] , 
                 _iqlocate_cursor(prepnamen [nr], 1), cu_modus);

        sql_prot (nr, DECLARE);
        SQLFEHLER (!= 0, "Fehler beim Prepare",__LINE__)
        if (sqlstatus) return (sqlstatus);
        rcurs[nr].preped = 1;
        return (nr);
}

int opencurs (short nr)
/**
Cursor oeffnen.
**/
{
/*
        struct sqlda *quest_desc;

        quest_desc = quest_curs [nr].quest_desc;
        if ((quest_curs [nr].quest) && (rcurs [nr].cutyp != 'I'))
        {
               if (!sqcursor [nr]) sqcursor [nr]  = 
                           _iqlocate_cursor(cunamen [nr], 0);
               _iqdcopen(sqcursor [nr], quest_desc, 
                         (char *)0, (struct value *)0, 1, 0);
        }
        else
        {
                if (!sqcursor [nr]) sqcursor [nr] = 
                            _iqlocate_cursor(cunamen [nr], 0);
                _iqdcopen
                   (sqcursor [nr], (struct sqlda *)0, 
                                 (char *)0, (struct value *)0, 0, 0);

        }
        sql_prot (nr, OPEN);
        SQLFEHLER (< 0,"Fehler beim open ",__LINE__)
        if (DEBUG) showcurs (nr, 1);
        rcurs [nr].preped = 2;
        return (0);
*/
        struct sqlda *quest_desc;

        quest_desc = quest_curs [nr].quest_desc;
        if ((quest_curs [nr].quest) && (rcurs [nr].cutyp != 'I'))
        {
               if (!sqcursor [nr]) sqcursor [nr]  = 
                           _iqlocate_cursor(cunamen [nr], 0);
               _iqdcopen(_iqlocate_cursor(cunamen [nr], 0), quest_desc, 
                         (char *)0, (struct value *)0, 1, 0);
        }
        else
        {
                _iqdcopen
                   (_iqlocate_cursor(cunamen [nr], 0), (struct sqlda *)0, 
                                 (char *)0, (struct value *)0, 0, 0);

        }
        sql_prot (nr, OPEN);
        SQLFEHLER (< 0,"Fehler beim open ",__LINE__)
        if (DEBUG) showcurs (nr, 1);
        rcurs [nr].preped = 2;
        return (0);
}


int fetchcurs (short nr, int modus, ...)
/**
ScrollCursor lesen.
**/
{
/*
        static _FetchSpec _FS0 = { 0, 1, 0 };
        va_list args;
        int row_num;

        if (modus == RELATIV | modus == DBABSOLUTE)
        {
                    va_start (args, modus);  
                    row_num = va_arg (args, int);
                    va_end (args);
                    _FS0.fval = row_num;
        }
        else
        {
                    _FS0.fval = 0l;
        }

        _FS0.fdir = modus;
        if (rcurs [nr].preped == 1)
        {
                    opencurs (nr);
        }

        q_desc = rcurs[nr].q_desc;

        if (!sqcursor [nr]) 
             sqcursor [nr] = _iqlocate_cursor(cunamen [nr], 0);
        _iqcftch(sqcursor [nr], (struct sqlda *)0, q_desc, (char *)0, &_FS0);

        sql_prot (nr, FETCH);
        SQLFEHLER (< 0, "Fehler beim select",__LINE__)
        if (DEBUG) showcurs (nr, 2);
        return (sqlstatus);
*/
        static _FetchSpec _FS0 = { 0, 1, 0 };
        va_list args;
        int row_num;
        int dsqlstatus;

        if (modus == RELATIV | modus == DBABSOLUTE)
        {
                    va_start (args, modus);  
                    row_num = va_arg (args, int);
                    va_end (args);
                    _FS0.fval = row_num;
        }
        else
        {
                    _FS0.fval = 0l;
        }

        _FS0.fdir = modus;
        if (rcurs [nr].preped == 1)
        {
                    opencurs (nr);
        }

        q_desc = rcurs[nr].q_desc;

        _iqcftch(_iqlocate_cursor(cunamen [nr], 100), (struct sqlda *)0, q_desc, (char *)0, &_FS0);
        dsqlstatus = sqlstatus;

        sql_prot (nr, FETCH);
        SQLFEHLER (< 0, "Fehler beim select",__LINE__)
        if (DEBUG) showcurs (nr, 2);
        return (sqlstatus);
}

int closecurs (short nr)
/**
Speicherbereiche fuer sqlerte freigeben.
**/
{
/*
        int ret;


        if (rcurs [nr].cutyp == 'U')
        {
                   sqprepcurs [nr] = _iqlocate_cursor(prepnamen [nr], 100);
                   if (sqprepcurs [nr]) _iqfree(sqprepcurs[nr]);
        }
        else
        {
                    sqcursor [nr] = _iqlocate_cursor(cunamen [nr], 102);
                    if (sqcursor [nr]) _iqclose(sqcursor[nr]);
                    sqprepcurs [nr] = _iqlocate_cursor(prepnamen [nr], 100);
                    if (sqprepcurs [nr]) _iqfree(sqprepcurs[nr]);
        }


        if (curssql [nr]) free (curssql [nr]);  

        if (cursnovars [nr]) free (cursovars [nr]);
        if (cursnivars [nr]) free (cursivars [nr]);

        curssql [nr] = 0;
        cursivars [nr] = 0;
        cursovars [nr] = 0;
        cursnivars [nr] = 0;
        cursnovars [nr] = 0;

        quest_free (nr);
        ret =  cursfree (nr);
        return (ret);
*/
        int ret;


        if (rcurs [nr].cutyp == 'U')
        {
                   _iqfree(_iqlocate_cursor(cunamen [nr], 102));

                    _iqfree(_iqlocate_cursor(prepnamen [nr], 102));
        }
        else
        {
                    _iqclose(_iqlocate_cursor(cunamen [nr], 100));
                    _iqfree(_iqlocate_cursor(cunamen [nr], 102));
                    _iqfree(_iqlocate_cursor(prepnamen [nr], 102));
        }


        if (curssql [nr]) free (curssql [nr]);  

        if (cursnovars [nr]) free (cursovars [nr]);
        if (cursnivars [nr]) free (cursivars [nr]);

        curssql [nr] = 0;
        cursivars [nr] = 0;
        cursovars [nr] = 0;
        cursnivars [nr] = 0;
        cursnovars [nr] = 0;

        quest_free (nr);
        ret =  cursfree (nr);
        return (ret);
}


int executecurs (short nr)
/**
Cursor oeffnen.
**/
{
/*
        struct sqlda *quest_desc;

        if (rcurs [nr].cutyp == 0)
        {
                     return (-1);
        }

        if (rcurs [nr].cutyp == 'I')
        {
                     return (insertcurs (nr));
        }
 
        if (quest_curs [nr].quest)
        {
                    quest_desc = quest_curs [nr].quest_desc;
                    if (!sqprepcurs [nr]) 
                       sqprepcurs [nr] = 
                     _iqlocate_cursor(prepnamen [nr], 1);
                    _iqexecute
                       (sqprepcurs [nr], quest_desc, (char *)0,
                          (struct value *) 0,
                          (struct sqlda *) 0, (char *) 0,
                          (struct value *) 0, 0);
        }
        else
        {
                     if (!sqprepcurs [nr]) sqprepcurs [nr] = 
                        _iqlocate_cursor(prepnamen [nr], 1);
                     _iqexecute
                         (sqprepcurs [nr], (struct sqlda *) 0, (char *)0,
                          (struct value *) 0,
                          (struct sqlda *) 0, (char *) 0,
                          (struct value *) 0, 0);


        }
        sql_prot (nr, EXECUTE);
        SQLFEHLER (< 0,"Fehler beim execute ",__LINE__)
        if (DEBUG) showcurs (nr, 3);
        return (0);
*/

        struct sqlda *quest_desc;

        if (rcurs [nr].cutyp == 0)
        {
                     return (-1);
        }

        if (rcurs [nr].cutyp == 'I')
        {
                     return (insertcurs (nr));
        }
 
        if (quest_curs [nr].quest)
        {
                    quest_desc = quest_curs [nr].quest_desc;
                    _iqexecute
                       (_iqlocate_cursor(prepnamen [nr], 1), quest_desc, (char *)0,
                          (struct value *) 0,
                          (struct sqlda *) 0, (char *) 0,
                          (struct value *) 0, 0);
        }
        else
        {
                     _iqexecute
                         (_iqlocate_cursor(prepnamen [nr], 1), (struct sqlda *) 0, (char *)0,
                          (struct value *) 0,
                          (struct sqlda *) 0, (char *) 0,
                          (struct value *) 0, 0);
		}
        sql_prot (nr, EXECUTE);
        SQLFEHLER (< 0,"Fehler beim execute ",__LINE__)
        if (DEBUG) showcurs (nr, 3);
        return (0);

}

int insertcurs (short nr)
/**
Insert mit Cursor.
**/
{
/*
        struct sqlda *quest_desc;
		int dsqlstatus;

        if (!sqcursor [nr]) 
             sqcursor [nr] = _iqlocate_cursor(cunamen [nr], 0);
        _iqdcopen(sqcursor [nr], (struct sqlda *)0, (char *)0,
            (struct value *)0, 0,0);

        dsqlstatus = sqlstatus;

        SQLFEHLER (< 0," Fehler beim open",__LINE__)
        if (quest_curs [nr].quest)
        {
                 quest_desc = quest_curs [nr].quest_desc;
                 if (!sqcursor [nr]) 
                       sqcursor [nr] = _iqlocate_cursor(cunamen [nr], 0);
                 _iqcput(sqcursor [nr], quest_desc, (char *)0);
                 dsqlstatus = sqlstatus;
        }
        else
        {
                 if (!sqcursor [nr]) 
                       sqcursor [nr] = _iqlocate_cursor(cunamen [nr], 0);
                 _iqcput(sqcursor [nr], (struct sqlda *)0, (char *)0);

        }
        sql_prot (nr, INSERT);
        SQLFEHLER (< 0," Fehler beim put",__LINE__)
        _iqflush(sqcursor[nr]);
        if (!sqcursor [nr]) 
                       sqcursor [nr] = _iqlocate_cursor(cunamen [nr], 0);
        _iqflush(sqcursor [nr]);

        SQLFEHLER (< 0," Fehler beim flush",__LINE__)
        if (DEBUG) showcurs (nr, 3);
        return (sqlstatus);
*/
        struct sqlda *quest_desc;
		int dsqlstatus;

        _iqdcopen(_iqlocate_cursor(cunamen [nr], 0), (struct sqlda *)0, (char *)0,
            (struct value *)0, 0,0);
        SQLFEHLER (< 0," Fehler beim open",__LINE__)
        if (quest_curs [nr].quest)
        {
                 quest_desc = quest_curs [nr].quest_desc;
                 _iqcput(_iqlocate_cursor(cunamen [nr], 0), quest_desc, (char *)0);

        }
        else
        {
                 _iqcput(_iqlocate_cursor(cunamen [nr], 0), (struct sqlda *)0, (char *)0);

        }
        sql_prot (nr, INSERT);
        SQLFEHLER (< 0," Fehler beim put",__LINE__)
        _iqflush(_iqlocate_cursor(cunamen [nr], 0));

        SQLFEHLER (< 0," Fehler beim flush",__LINE__)
        if (DEBUG) showcurs (nr, 3);
        return (sqlstatus);

}
        
void init_curs (void)
/**
Cursorarray inittialisieren.
**/
{
        short i;

        for (i = 0; i < 100; i ++)
        {
                     rcurs [i] = icurs;
        }
        cursinit = 1;
}

char *sqlmalloc (int nr)
/**
Speichergroesse fuer SQLwerte ermitteln und Speicherplatz
zuordnen.
**/
{
        short i;
        int sqlsize;
        struct sqlda *qdesc;

        cu_nr = nr;
        rcurs[cu_nr].vind = (short *) 
                      cu_malloc ((rcurs[nr].fields + 1) * sizeof (short));
        if (rcurs[cu_nr].vind == 0)
        {
                    return (0);
        }
        rcurs[cu_nr].vformat = (char *) cu_malloc (rcurs[cu_nr].fields * 4);
        if (rcurs[cu_nr].vformat == 0)
        {
                    return (0);
        }
        
        sqlsize = 0;
        for (i = 0; i < rcurs[cu_nr].fields; i ++)
        { 
                    csqlind = sqlsize;
//                    qdesc = rcurs[cu_nr].q_desc;
//                    switch (qdesc->sqlvar->sqltype)
                    switch ((int) cfieldinf[i].sqltype)
                    {
                           case SQLCHAR :
                                     cfieldinf[i].sqltype = SQLCHAR;
                                     sqlsize += cfieldinf[i].sqllen + 2;
                                     cfieldinf[i].sqllen ++;
                                     strcpy (&rcurs[cu_nr].vformat [i * 4],
                                             "%s"); 
                                     break;
                           case SQLVCHAR :
                                     cfieldinf[i].sqltype = SQLVCHAR;
                                     cfieldinf[i].sqllen &= 255;
                                     sqlsize += cfieldinf[i].sqllen + 2;
                                     cfieldinf[i].sqllen ++;
                                     strcpy (&rcurs[cu_nr].vformat [i * 4],
                                             "%s"); 
                                     break;
                            case SQLSMINT :
                                     sqlsize += sizeof(short);
                                     strcpy (&rcurs[cu_nr].vformat [i * 4],
                                             "%hd"); 
   
                                     break;
                            case SQLINT :
                            case SQLSERIAL :
                                     sqlsize += sizeof(int);
                                     strcpy (&rcurs[cu_nr].vformat [i * 4],
                                             "%d"); 
                                     break;
                            case SQLDECIMAL :
                                     cfieldinf[i].sqltype = SQLFLOAT;
                                     sqlsize += sizeof (double);
                                     strcpy (&rcurs[cu_nr].vformat [i * 4],
                                             "%lf"); 
                                     break;
                            case SQLDATE :
                                     cfieldinf[i].sqltype = SQLCHAR;
                                     cfieldinf[i].sqllen = 12;
                                     sqlsize += 12;
                                     strcpy (&rcurs[cu_nr].vformat [i * 4],
                                             "%s"); 
                   }
        }
        if (sqlsize == 0)
        {
                   return ((char *) 1);
        }
        cusize [cu_nr] = sqlsize;
        return (cu_malloc (sqlsize));
}

int cursfree (int nr)
/**
Speicherbereiche fuer cursornr freigeben.
**/
{
        if (rcurs[nr].sqlwerte > (char *) 1)
        {
                       free (rcurs[nr].sqlwerte);
        }
        if (rcurs[nr].vind > 0)
        {
                       free (rcurs[nr].vind);
        }
        if (rcurs[nr].vformat > 0)
        {
                       free (rcurs[nr].vformat);
        }
/*
        if (rcurs[nr].q_desc)
        {
                       free (rcurs[nr].q_desc);
        }
*/
        rcurs[nr].preped = 0;
        return (0);
}


int select_curs (short nr)
/**
Cursor auswahlen.
**/
{
       
        short i,j;

        if (rcurs[nr].preped == 0)
        {
                       return (-1);
        }
        cu_nr = nr;

        for (i = MAXCURS - 1; i > cstptr; i --)
        {
                       if (cursor_stack[i] == cu_nr)
                       {
                                   break;
                       }
        }
        if (i == cstptr)
        {
                       return (0);
        }

        for (j = i; j > (cstptr + 1); j --)
        {
                      cursor_stack[j] = cursor_stack[j - 1];
        }

        cursor_stack [j] = cu_nr; 
        return (0);
}

void *dbfeldadr (short nr)
/**
Adresse eines Feldes zurueckgeben.
**/
{
        short i;

        i = nr;
        return (&rcurs[cu_nr].sqlwerte[csqlind]);
}
char debugbuff [512];

char *GetDebug ()
{
	return debugbuff;
}

static char *cretwert;

void sqlvalue (void *val, int nr)
/**
Wert einer Sqlvariablen holen.
**/
{
	    struct sqlda *Sqld;
		struct sqlvar_struct *Sqlvar;
		short swert;
		long lwert;
		double dwert;
		
		Sqld = rcurs[cu_nr].q_desc;
        Sqlvar = Sqld->sqlvar;
		switch (Sqlvar[nr].sqltype)
		{
		    case SQLCHAR  :
		    case SQLVCHAR  :
                cretwert = (char *) Sqlvar[nr].sqldata;
//				memcpy ((char *) val, &cwert[0], sizeof (char *));
                break; 
		    case SQLSMINT :
				memcpy ((short *) &swert, (short *) Sqlvar[nr].sqldata, sizeof (short));
				memcpy ((short *) val,    (short *) Sqlvar[nr].sqldata, sizeof (short));
				break;
		    case SQLINT :
			case SQLSERIAL :
				memcpy ((long *) &lwert, (long *) Sqlvar[nr].sqldata, sizeof (long));
				memcpy ((long *) val,    (long *) Sqlvar[nr].sqldata, sizeof (long));
				break;
		    case SQLFLOAT :
				memcpy ((double *) &dwert, (double *) Sqlvar[nr].sqldata, sizeof (double));
				memcpy ((double *) val,    (double *) Sqlvar[nr].sqldata, sizeof (double));
				break;
		}
}

int sqlwert (void *field, short nr)
/**
sqlwert in variable uebertragen.
**/
{
        short i;
        TYADR var;
		int cs;
		short *val;
		char hex[5];

		sqlvalue (field, nr);
		return 0;

        var.fvoid = field;
        i = nr;
        if (i >= cfields)
        {
                      return (-1);
        }

		val = rcurs[cu_nr].sqlwerte;
		debugbuff[0] = 0;
		for (i = 0; i < 40; i ++)
		{
			sprintf (hex, "%02X ", (unsigned char) val[i]);
			strcat (debugbuff, hex);
		}
		cs = csqlind;
        feld.fchar = &rcurs[cu_nr].sqlwerte[csqlind];
//        feld.fchar = &rcurs[cu_nr].sqlwerte[i];
        switch (cfieldinf[i].sqltype)
        {
                       case SQLCHAR :
                       case SQLVCHAR :
                                  strcpy (var.fchar,feld.fchar);
                                  break;
                       case SQLSMINT :
                                  *var.fshort =  *feld.fshort;
                                  break;
                       case SQLINT :
                       case SQLSERIAL :
                                  *var.fint = *feld.fint;
                                  break;
                       case SQLFLOAT   :
                                  *var.fdouble = *feld.fdouble;
                                  break;
                       case SQLDATE :
                                  strcpy (var.fchar,feld.fchar);
                                  break;
        }
        return (0);
}

int charsql (char *field, short nr)
/**
Char in SQL-Variable uebertragen.
**/
{
        short i;

        i = nr;
        if (i >= cfields)
        {
                      return (-1);
        }

        feld.fchar = &rcurs[cu_nr].sqlwerte[csqlind];
        switch (cfieldinf[i].sqltype)
        {
                       case SQLCHAR :
                       case SQLVCHAR :
                                  strcpy (feld.fchar,field);
                                  break;
                       case SQLSMINT :
                                  sscanf (field,"%hd",feld.fshort);
                                  break;
                       case SQLINT :
                       case SQLSERIAL :
                                  sscanf (field,"%d",feld.fint);
                                  break;
                       case SQLFLOAT   :
                                  sscanf (field,"%lf",feld.fdouble);
                                  break;
                       case SQLDATE :
                                  strcpy (feld.fchar,field);
                                  break;
        }
        return (0);
}

int wertsql (void *field, short nr)
/**
sqlwert in variable uebertragen.
**/
{
        short i;
        TYADR var;

        var.fvoid = field;
        i = nr;
        if (i >= cfields)
        {
                      return (-1);
        }

        feld.fchar = &rcurs[cu_nr].sqlwerte[csqlind];
        switch (cfieldinf[i].sqltype)
        {
                       case SQLCHAR :
                       case SQLVCHAR :
                                  strcpy (feld.fchar,var.fchar);
                                  break;
                       case SQLSMINT :
                                  *feld.fshort =  *var.fshort;
                                  break;
                       case SQLINT :
                       case SQLSERIAL :
                                  *feld.fint = *var.fint;
                                  break;
                       case SQLFLOAT   :
                                  *feld.fdouble = *var.fdouble;
                                  break;
                       case SQLDATE :
                                  strcpy (feld.fchar,var.fchar);
                                  break;
        }
        return (0);
}

char *csqlvalue (short nr)
/**
Wert aus Selectanweisung zurueckgeben.
**/
{
	    char *retwert;
		
        if (sqlwert (&retwert, nr) == 0) return (cretwert);
        return ((char *) 0);
}

char *csqlnvalue (char *name)
/**
Char-Wert aus Selectanweisung zurueckgeben.
**/
{
        short nr;
        char *retwert;

        nr = feld_nr (name);
        if (nr == -1)
        {
                   return (0);
        }
        if (sqlwert (&retwert, nr) == 0) return (retwert);
        return ((char *) 0);
}

short ssqlvalue (short nr)
/**
Short-Wert aus Selectanweisung zurueckgeben.
**/
{
        short retwert;

        if (sqlwert (&retwert, nr) == 0) return (retwert);
        return (0);
}

short ssqlnvalue (char *name)
/**
Wert aus Selectanweisung zurueckgeben.
**/
{
        short retwert;
        short nr;

        nr = feld_nr (name);
        if (nr == -1)
        {
                   return (-1);
        }
        if (sqlwert (&retwert, nr) == 0) return (retwert);
        return (-1);
}

long isqlvalue (short nr)
/**
Wert aus Selectanweisung zurueckgeben.
**/
{
        long retwert;

        if (sqlwert (&retwert, nr) == 0) return (retwert);
        return (0);
}

long isqlnvalue (char *name)
/**
Wert aus Selectanweisung zurueckgeben.
**/
{
        long retwert;
        short nr;

        nr = feld_nr (name);
        if (nr == -1)
        {
                   return (-1);
        }
        if (sqlwert (&retwert, nr) == 0) return (retwert);
        return (-1);
}

double dsqlvalue (short nr)
/**
Wert aus Selectanweisung zurueckgeben.
**/
{
        double retwert;

        if (sqlwert (&retwert, nr) == 0) return (retwert);
        return ((double) 0.0);
}

double dsqlnvalue (char *name)
/**
Wert aus Selectanweisung zurueckgeben.
**/
{
        double retwert;
        short nr;

        nr = feld_nr (name);
        if (nr == -1)
        {
                   return ((double) -1);
        }
        if (sqlwert (&retwert, nr) == 0) return (retwert);
        return ((double) 0.0);
}


int chqltyp (short nr,short typ, void *newfeld)
/**
Konvertierung eines SQL-Feldes in typ
**/
{
        short i;
        struct sqlvar_struct *sqlvar;

        i = nr;
        if (i >= cfields)
        {
                      return (-1);
        }

        sqlvar = rcurs [cu_nr].q_desc->sqlvar;
        feld.fchar = sqlvar[i].sqldata;
        switch (cfieldinf[i].sqltype)
        {
                       case SQLCHAR :
                       case SQLVCHAR :
                            return (from_sqlchar (feld.fchar, typ, newfeld));
                       case SQLSMINT :
                            return (from_sqlshort (*feld.fshort,typ, newfeld));
                       case SQLINT :
                       case SQLSERIAL :
                            return (from_sqlint (*feld.fint,typ, newfeld));
                       case SQLFLOAT :
                            return (from_sqldouble (*feld.fdouble,typ, newfeld, nr));
                       case SQLDATE :
                             return (from_sqlchar (feld.fchar,typ, newfeld));
        }
        return (-1);
}


int from_sqlchar (char *quelle, short typ, void *ziel)
/**
Sql-Feld in Typ konvertieren.
**/
{
        short swert;
        long iwert;
        double dwert;

        switch (typ)
        {
                case SQLCHAR :
                case SQLVCHAR :
                       strcpy ((char *) ziel, quelle);
                       return (0);
                case SQLSMINT :
                       if (!numeric (quelle)) return (-1);
                       swert = atoi (quelle);
                       memcpy ((char *) ziel, (char *) &swert, sizeof (short));
                       return (0);
                case SQLINT :
                case SQLSERIAL :
                       if (!numeric (quelle)) return (-1);
                       iwert = atoi (quelle);
                       memcpy ((char *) ziel, (char *) &iwert, sizeof (long));
                       return (0);
                case SQLFLOAT   :
                       if (!numeric (quelle)) return (-1);
                       dwert = ratod (quelle);
                       memcpy ((char *) ziel, (char *) &dwert, sizeof (double));
                       return (0);
                case SQLDATE :
                       strcpy ((char *) ziel, quelle);
                       return (0);
        }
        return (-1);
}

int from_sqlshort (short quelle, short typ, void *ziel)
/**
Sql-Feld in Typ konvertieren.
**/
{
        long iwert;
        double dwert;

        switch (typ)
        {
                case SQLCHAR :
                case SQLVCHAR :
                       sprintf ((char *) ziel,"%hd",quelle);
                       return (0);
                case SQLSMINT :
                       memcpy ((char *) ziel, (char *) &quelle,
                                sizeof (short));
                       return (0);
                case SQLINT :
                case SQLSERIAL :
                       iwert = (long) quelle;
                       memcpy ((char *) ziel, (char *) &iwert,
                                sizeof (long));
                       return (0);
                case SQLFLOAT   :
                       dwert = (double) quelle;
                       memcpy ((char *) ziel, (char *) &dwert,
                                sizeof (double));
                       return (0);
                case SQLDATE :
                       return (-1);
        }
        return (-1);
}

int from_sqlint (long quelle, short typ, void *ziel)
/**
Sql-Feld in Typ konvertieren.
**/
{
        double dwert;

        switch (typ)
        {
                case SQLCHAR :
                case SQLVCHAR :
                       sprintf ((char *) ziel,"%ld",quelle);
                       return (0);
                case SQLSMINT :
                       return (0);
                case SQLINT :
                case SQLSERIAL :
                       memcpy ((char *) ziel, (char *) &quelle,
                                sizeof (long));
                       return (0);
                case SQLFLOAT :
                       dwert = (double) quelle;
                       memcpy ((char *) ziel, (char *) &dwert,
                                sizeof (double));
                       return (0);
                case SQLDATE :
                       dlong_to_asc (quelle,(char *) ziel);
                       return (0);
        }
        return (-1);
}

int from_sqldouble (double quelle, short typ, void *ziel, int nr)
/**
Sql-Feld in Typ konvertieren.
**/
{
        char sql_form [20];
        long iwert;

        switch (typ)
        {
                case SQLCHAR :
                case SQLVCHAR :
                       doubleform ((char *) sql_form, (short) nr);
                       sprintf ((char *) ziel, sql_form, quelle);
                       return (0);
                case SQLSMINT :
                       return (0);
                case SQLINT :
                case SQLSERIAL :
                       iwert = (long) quelle;
                       return (0);
                case SQLFLOAT   :
                       memcpy ((char *) ziel, (char *) &quelle,
                                sizeof (double));
                       return (0);
                case SQLDATE :
                       return (-1);
        }
        return (-1);
}

short sqltyp (short nr)
/**
SQL-Typ holen.
**/
{
        return (cfieldinf[nr].sqltype);
}

short sqllen (short nr)
/**
SQL-Typ holen.
**/
{
        return (cfieldinf[nr].sqllen);
}

void set_sqlolen (short nr,short len)
/**
SQL-Typ holen.
**/
{
        struct sqlvar_struct *sqlvar;

        sqlvar = quest_curs [cu_nr].quest_desc->sqlvar;
        sqlvar[nr].sqllen  = len;
}


char * sqlname (short nr)
/**
SQL-Typ holen.
**/
{
        return (cfieldinf[nr].sqlname);
}

int   sqlfields (void)
/**
 Anzahl SQL-Felder             
**/
{
        return (cfields);
}

int sqlsize (void)
/**
Holt die Groesse des Buffers bei Selectanweisungen.
Kann nur direkt nach prepare benutzt werden.
**/

{
         return (cusize [cu_nr]);
}

char *  sqlformat (short nr)          
/** Format fuer SQL-Feld          
**/
{
        return (&rcurs[cu_nr].vformat [nr * 4]);
}

char *usformat (char *fname)
/**
Vom User definiertes Format zu Feldnamen holen.
**/
{
        short i;

        for (i = 0; i < formate; i ++)
        { 
                       if (strcmp (fname,forms[i].name) == 0)
                       {
                                      break;
                       }
        }
        if (i == formate)
        {
                       return (0);
        }
        return (forms[i].format);
}

int insformat (char *name, char *format)
/**
Format fuer Feld in forms einfuegen.
**/
{
        if (formate == 256)
        {
                      return (-1);
        }

        if (strlen (name) > 19)
        {
                      return (-1);
        }
        
        if (strlen (format) > 9)
        {
                      return (-1);
        }
        
        strcpy(forms[formate].name,name);
        strcpy(forms[formate].format,format);
        formate ++;
        return 0;
}

void delformate (void)
/**
forms loeschen.
**/
{
        formate == 0;
}
        
void floatkomma (char *wert)
/**
Komma durch Punkt in Decimalwerten ersetzen.
**/
{

         for (;*wert; wert += 1)
         {
                  if (*wert == ',') 
                  {
                           *wert = '.';
                           break;
                  }
         }
}
                     
void printfield (short nr)
/**
SQL-Feld anzeigen.
**/
{
       short i;
       char *format;
       char sql_form [20];
       
       i = nr;
       format = usformat (sqlname (nr));
       if (format == 0)
       {  
             switch (sqltyp (nr))
             {
                   case SQLCHAR :
                   case SQLVCHAR :
                            printf ("%s",csqlvalue (nr));
                            break;
                   case SQLSMINT :
                            printf ("%hd",ssqlvalue (nr));
                            break;
                   case SQLINT :
                   case SQLSERIAL :
                            printf ("%d",isqlvalue (nr));
                            break;
                   case SQLFLOAT :
                            printf (doubleform (sql_form, nr),dsqlvalue (nr));
                            break;
                   case SQLDATE :
                            printf ("%s",csqlvalue (nr));
                            break;
             }
       }
       else
       {
             switch (sqltyp (nr))
             {
                   case SQLCHAR :
                   case SQLVCHAR :
                            printf (format,csqlvalue (nr));
                            break;
                   case SQLSMINT :
                            printf (format,ssqlvalue (nr));
                            break;
                   case SQLINT :
                   case SQLSERIAL :
                            printf (format,isqlvalue (nr));
                            break;
                   case SQLFLOAT :
                            printf (format,dsqlvalue (nr));
                            break;
                   case SQLDATE :
                            printf (format,csqlvalue (nr));
                            break;
             }
        }
}
        
void sprintfield (char *wert, short nr)
/**
SQL-Feld anzeigen.
**/
{
       short i;
       char *format;
       char sql_form [20];
       
       i = nr;
       format = usformat (sqlname (nr));
       if (format == 0)
       {  
             switch (sqltyp (nr))
             {
                   case SQLCHAR :
                   case SQLVCHAR :
                            sprintf (wert,"%s",csqlvalue (nr));
                            break;
                   case SQLSMINT :
                            sprintf (wert,"%hd",ssqlvalue (nr));
                            break;
                   case SQLINT :
                   case SQLSERIAL :
                            sprintf (wert,"%d",isqlvalue (nr));
                            break;
                   case SQLFLOAT :
                            sprintf (wert,doubleform (sql_form, nr) ,
                                          dsqlvalue (nr));
                            decimalkomma (wert);
                            break;
                   case SQLDATE :
                            sprintf (wert,"%s",csqlvalue (nr));
                            break;
             }
       }
       else
       {
             switch (sqltyp (nr))
             {
                   case SQLCHAR :
                   case SQLVCHAR :
                            sprintf (wert,format,csqlvalue (nr));
                            break;
                   case SQLSMINT :
                            sprintf (wert,format,ssqlvalue (nr));
                            break;
                   case SQLINT :
                   case SQLSERIAL :
                            sprintf (wert,format,isqlvalue (nr));
                            break;
                   case SQLFLOAT :
                            sprintf (wert,format,dsqlvalue (nr));
                            decimalkomma (wert);
                            break;
                   case SQLDATE :
                            sprintf (wert,format,csqlvalue (nr));
                            break;
             }
        }
}

void decimalkomma (char *string)
/**
Wenn ',' dann '.'.
**/
{
       char *komma;

       komma = strchr (string, ',');
       if (komma) *komma = '.';
}


short feld_nr (char *name)
/**
Feldnummer zu Feldnamen aus aktuellen Cursor holen.
**/
{
       short i;

       for (i =0; i < cfields; i ++)
       {
                   if (strcmp (name,cfieldinf[i].sqlname) == 0)
                               return (i);
       }
       return (-1);
}

char *cursor_name (short cursor_nr)
/**
Cursor-Name zu Cursor-Nr zurueckgeben.
**/
{
        return ((char *) cunamen [cursor_nr]);
}

int test_out (short nr)
/**
Testen, ob Variable fuer den Select-Befehl zugewiesen wurden.
**/
{
        struct sqlvar_struct *sqlvar;
        short i;


        cursnovars [nr] = 0;
        cursovars [nr] = 0;
/* Test, ob qanz groesser als die zugeordneten Adressen sind       */

        if (outptr <= 0)
        {
                     return (0);
        }

        sqlvar = rcurs [nr].q_desc->sqlvar;
        for (i = 0; i < outptr; i ++)
        {
                    sqlvar[i].sqltype = outvar [i].typ;
                    sqlvar[i].sqllen  = outvar [i].len;
                    sqlvar[i].sqldata = outvar [i].var;
        }

        cursovars [nr] = (struct sqlvar_struct *)
                  cu_malloc (sizeof (struct sqlvar_struct) * (outptr * 2));
        if (cursovars [nr] == 0)  
        {
                    outptr = 0;
                    return (0);
        }
        cursnovars [nr] = outptr;

        sqlvar = cursovars [nr];
        for (i = 0; i < outptr; i ++)
        {
                    sqlvar[i].sqltype = outvar [i].typ;
                    sqlvar[i].sqllen  = outvar [i].len;
                    sqlvar[i].sqldata = outvar [i].var;
        }
        outptr = 0; 
        return (0);
}

int test_quest (char *text, short nr)
/**
Testen, ob Fragezeichen im where_teil vorhanden sind.
**/
{
        short qanz;
        char *qpos;
        struct sqlvar_struct *sqlvar;
        short i;

        cursnivars [nr] = 0;
        cursivars [nr] = 0;

        quest_curs [nr].quest = 0;
        qpos = text;

        for (qanz = 0;qpos > 0; qanz ++, qpos += 1)
        {
                    qpos = strchr (qpos,'?');
                    if (qpos == 0)
                    {
                                break;
                    }
        } 
        if (qanz == 0)
        {
                     return (0);
        }

/* Test, ob qanz groesser als die zugeordneten Adressen sind       */

        if (qanz > varptr)
        {
                     qanz = varptr;
        }
        if (qanz <= 0)
        {
                     return (-1);
        }

        varptr = 0; 
        quest_curs [nr].quest_desc = (struct sqlda *) cu_malloc (128);
        if (quest_curs[nr].quest_desc == 0)
        {
                    return (-1);
        }
        quest_curs [nr].quest_desc->sqlvar =
                       (struct sqlvar_struct *)cu_malloc
                          (sizeof (struct sqlvar_struct) * (qanz * 2));

        if (quest_curs[nr].quest_desc->sqlvar == 0)
        {
                    return (-1);
        }

        quest_curs [nr].quest = 1;

        quest_curs [nr].quest_desc->sqld = qanz;
        quest_curs [nr].quest_desc->desc_occ = qanz;
        quest_curs [nr].quest_desc->desc_next = 0;
        sqlvar = quest_curs [nr].quest_desc->sqlvar;
        for (i = 0; i < qanz; i ++)
        {
                    sqlvar[i].sqlname = (char *) 0;
                    sqlvar[i].sqltype = var_type (varbuff [i].typ);
                    sqlvar[i].sqllen  = varbuff [i].len;
                    sqlvar[i].sqldata = varbuff [i].var;
                    sqlvar[i].sqlind = 0;
                    sqlvar [i].sqlformat  = 0;
                    sqlvar [i].sqlidata  = 0;
                    sqlvar [i].sqlitype  = 0;
                    sqlvar [i].sqlilen   = 0;

        }
        if (qanz == 0) return (0);

        cursivars [nr] = (struct sqlvar_struct *)
                cu_malloc (sizeof (struct sqlvar_struct) * (qanz * 2));
        if (cursivars [nr] == 0)  return (0);

        cursnivars [nr] = qanz;
        sqlvar = cursivars [nr];
        for (i = 0; i < qanz; i ++)
        {
                    sqlvar[i].sqlname = (char *) 0;
                    sqlvar[i].sqltype = varbuff [i].typ;
                    sqlvar[i].sqllen  = varbuff [i].len;
                    sqlvar[i].sqldata = varbuff [i].var;
                    sqlvar[i].sqlind = 0;
        }
        return (0);
}

int var_type (short type)
/**
SQL-Typ in C-Typ.
**/ 
{

        static short ttab [] = {CCHARTYPE, 
                                CSHORTTYPE,
                                CLONGTYPE,
                                CDOUBLETYPE,
                               -1};

/* Date-Type 7 in Long-Type 2 umwandeln   */
        if (type == 7) type = 2;

        return (ttab [type]);
}

int ins_quest (char *var, short typ,short len)
/**
Variablenadresse in var_buf schreiben.
**/  
{
       if (varptr == MAX_VARS)
       {
                     return (-1);
       }
       varbuff [varptr].var = var;
       varbuff [varptr].typ = typ;
       varbuff [varptr].len = len;
       varptr ++;
       return (0);
}

void init_sqlout (void)
/**
Ausgabereiche initialisieren.
**/
{
       outptr = 0;
}

void init_sqlin (void)
/**
Eingabebereich initialisieren.
**/
{
       varptr = 0;
}

int out_quest (char *var, short typ, short len)
/**
Variablenadresse in var_buf schreiben.
**/  
{
       if (outptr == MAX_VARS)
       {
                     return (-1);
       }
       outvar [outptr].var = var;
       outvar [outptr].typ = typ;
       outvar [outptr].len = len;
       outptr ++;
       return (0);
}

void quest_free (int nr)
/**
Speicher bereich fuer quest_desc freigeben.
**/
{
        if (quest_curs [nr].quest)
        {
                      free (quest_curs [nr].quest_desc->sqlvar);
                      free (quest_curs [nr].quest_desc);
        }
}


int upd_cursor (char *sqltext)
/**
Testen, ob ein Update-Cursor vorbereitet werden soll.
**/
{
        short i;
        char *into;
    
        for (i = 0; sqltext [i]; i ++)
        {
                     if (sqltext [i] > ' ')
                     {
                                    break;
                     }
        }
        if (memcmp (&sqltext [i], "update", strlen ("update")) == 0)
        {
                      return (1);
        }
        if (memcmp (&sqltext [i], "UPDATE", strlen ("update")) == 0)
        {
                      return (1);
        }
        if (memcmp (&sqltext [i], "delete", strlen ("update")) == 0)
        {
                      return (1);
        }
        if (memcmp (&sqltext [i], "DELETE", strlen ("update")) == 0)
        {
                      return (1);
        }
        into = strstr (&sqltext[i],"into");
        if ((into) && (strstr (into,"temp")))
        {
                     return (1);
        }
        if (memcmp (&sqltext [i],"select",strlen ("select")) == 0)
        {
                      return (0);
        }
        if (memcmp (&sqltext [i],"SELECT",strlen ("select")) == 0)
        {
                      return (0);
        }
        if (memcmp (&sqltext [i],"insert",strlen ("insert")) == 0)
        {
                      return (0);
        }
        if (memcmp (&sqltext [i],"INSERT",strlen ("insert")) == 0)
        {
                      return (0);
        }
        return (1);
}
       
int ins_cursor (char *sqltext, short nr)
/**
Testen, ob ein Insert-Cursor vorbereitet werden soll.
**/
{
        short i;
    
        rcurs [nr].cutyp = 0;
        for (i = 0; sqltext [i]; i ++)
        {
                     if (sqltext [i] > ' ')
                     {
                                    break;
                     }
        }
        if (memcmp (&sqltext [i], "insert", strlen ("insert")) == 0)
        {
                      rcurs [nr].cutyp = 'I';
                      return (1);
        }
        if (memcmp (&sqltext [i], "INSERT", strlen ("insert")) == 0)
        {
                      rcurs [nr].cutyp = 'I';
                      return (1);
        }
        return (0);
}

void _SQLF (char *s, short l)
/**
Abbruch bei Sqlfehler.
**/
{
            FILE *sqlerr;

            if (sql_mode == 0)
            {
                      printf ("fehler : %d bei : %s Zeile : %d\n",
                            sqlca.sqlcode,
                            s,
                            l); 
                      rollbackwork(); 
                      return; 
            }
            if (sql_mode == 2)
            {
                      if (sqlprot)
                      {
                            sqlerr = sqlprot;
                            fprintf(sqlerr,"fehler : %d bei : %s Zeile : %d\n",
                                            sqlca.sqlcode,
                                            s,
                                            l); 
                      }
                      else
                      {
                             printf("fehler : %d bei : %s Zeile : %d\n",
                                           sqlca.sqlcode,
                                           s,
                                           l);
                      }
           }
           if (sql_mode == 3)
           {
               if (sqlerrprog != (int (*)()) 0)
               {
                      (*sqlerrprog) ();
               }
           }
           return;
}

int sql_prot (short nr, char *typ)
/**
Sqlprotokoll ausgaben.
**/
{
        short i;
        TYADR sql_var;

        double dwert;
        static short env_ok = 0;
        struct sqlvar_struct *sqlvari;
        struct sqlvar_struct *sqlvaro;

        if (env_ok == 0)
        {
                if (getenv ("SQLDEBUG")) SQLDEBUG = atoi (getenv ("SQLDEBUG"));
                env_ok = 1;
        }
               
        if (SQLDEBUG == 0) return (0);
        fprintf (sqldebug,"%s Cursor %s\n", typ, cursor_name (nr));
        fprintf (sqldebug,"SQLSTAUS %d\n", sqlstatus);
        for (i = 0; sqprepcurs [nr]->_SQCcommand [i]; i ++)
        {
            fprintf (sqldebug,"%s\n", sqprepcurs [nr]->_SQCcommand [i]);
        }
        if (strcmp (typ,PREPARE) == 0)
        {
                     if (SQLDEBUG == 2) SQLDEBUG = 0;
                     return (0);
        }
        if (strcmp (typ,DECLARE) == 0)
        {
                     if (SQLDEBUG == 2) SQLDEBUG = 0;
                     return (0);
        }
        if (strcmp (typ,DESCRIBE) == 0)
        {
                     if (SQLDEBUG == 2) SQLDEBUG = 0;
                     return (0);
        }
        sqlvaro = rcurs [nr].q_desc->sqlvar;
        sqlvari = quest_curs [nr].quest_desc->sqlvar;
        if (strcmp (typ,"fetch") == 0)
        {
             for (i = 0; i < rcurs [nr].q_desc->sqld; i ++)
             {
                       sql_var.fchar = sqlvaro[i].sqldata;
                       fprintf (sqldebug,"Ausgabevariable%hd ", i + 1);
                       switch (sqlvaro[i].sqltype)
                       {
                               case 0 :
                                      fprintf (sqldebug,"%s\n",
                                               sql_var.fchar);
                                      break;
                               case 1 :
                                      fprintf (sqldebug,"%hd\n",
                                               *sql_var.fshort);
                                      break;
                               case 2 :
                                      fprintf (sqldebug,"%d\n",
                                               *sql_var.fint);
                                      break;
                               case 3 :
                                      memcpy (&dwert, sqlvaro [i].sqldata,
                                              sizeof (double));
                                      fprintf (sqldebug,"%lf\n", 
                                                         *sql_var.fdouble);
                                      break;
                               default :
                                      fprintf (sqldebug,"\n");
                        }
             }
        }                      

        if (quest_curs [nr].quest == 0)
        {
                     if (SQLDEBUG == 2) SQLDEBUG = 0;
                     return (0);
        }
        for (i = 0; i < quest_curs[nr].quest_desc->sqld; i ++)
        {
                  sql_var.fchar = sqlvari[i].sqldata;
                  fprintf (sqldebug,"Eingabevariable%hd ", i + 1);
                  switch (sqlvari[i].sqltype)
                  {
                          case CCHARTYPE :
                                 fprintf (sqldebug,"%s\n",
                                          sql_var.fchar);
                                 break;
                          case CSHORTTYPE :
                                 fprintf (sqldebug,"%hd\n",
                                          *sql_var.fshort);
                                 break;
                          case CLONGTYPE :
                                 fprintf (sqldebug,"%d\n",
                                          *sql_var.fint);
                                 break;
                          case CDOUBLETYPE :
                                 fprintf (sqldebug,"%lf\n",
                                          *sql_var.fdouble);
                                 break;
                                 fprintf (sqldebug,"\n");
                   }
        }
        printf ("\n");
        if (SQLDEBUG == 2) SQLDEBUG = 0;
        return (0);
}

          

void set_sqlproc (int (*procedure) ())
/**
Procedureadresee fuer Sqlerrorausgabe setzen.
**/
{
          sqlerrprog = procedure;
}


void sqlstatement (char *statement)
{
	   static _SQSTMT _SQ0 = {0};
	   static char *sqlcmdtxt[] = {0, 0};
	   
	   sqlcmdtxt[0] = statement;
       _iqstmnt(&_SQ0,(char **) sqlcmdtxt, 0, (struct sqlvar_struct *) 0, (struct value *) 0);
}
	 

int sqlconnect (char *server, char *user, char *passw)
{
     char buffer [256];
     char *p;

     sprintf (buffer, "INFORMIXSERVER=%s", server);
     _putenv (buffer);
     strcpy(InetLogin.InfxServer, server);
     strcpy(InetLogin.Host, "fit2000");
     strcpy(InetLogin.User, user);
     strcpy(InetLogin.Pass, passw);
     _iqconnect(0, server, (char *) 0, user, passw, 0);
     return (sqlstatus);
}


int sqlconnectdbase (char *server, char *user, char *passw, char *dbase)
{
     char buffer [256];
     char *p;

     sprintf (buffer, "INFORMIXSERVER=%s", server);
     _putenv (buffer);
     p = getenv ("INFORMIXSERVER");
     strcpy(InetLogin.InfxServer, server);
     strcpy(InetLogin.Host, "fit2000");
     strcpy(InetLogin.User, user);
     strcpy(InetLogin.Pass, passw);
     _iqdbase(dbase, 0);
     return sqlstatus;
}

  

void opendbase (char *dbase)
/**
Datenbank selektieren.
**/
{
       short i;


       _iqdbase(dbase, 0);
       sqlstatement ("set isolation to dirty read");  

/*       execute_sql ("set isolation to dirty read");  */


/*
       for (i = 0; i < MAXCURS; i ++)
       {
                             cursnivars [i] = 0;
                             cursnovars [i] = 0;
       }
       sprintf (sql,"database %s",dbase);
       execute_sql (sql);
*/
}


void closedbase (void)
/**
Datenbank schliessen.
**/

{
        execute_sql ("close database");
}

void debug_sql (char *sqltext)
/**
Text anzeigen wenn Parameter DEBUG gesetzt ist;
**/
{
       if (DEBUG)
       {
                  printf ("%s\n",sqltext);
       }
       if (DEBUG == 2) DEBUG = 0;
}

void begin_work (void)
{
       if (Transaction == 0)
       {
           return;
       }
       execute_sql ("begin work");
       in_work = 1;
}

void commit_work (void)
{
       if (Transaction == 0)
       {
           return;
       }
       execute_sql ("commit work");
       in_work = 0;
}

void rollback_work (void)
{
       if (Transaction == 0)
       {
           return;
       }
       execute_sql ("rollback work");
       in_work = 0;
}


void beginwork (void)
{
        int sql_s;

        sql_s = sql_mode;
        sql_mode = 1;
        commitwork ();
        begin_work ();
        sql_mode = sql_s;
}

void commitwork (void)
{
        int sql_s;

        sql_s = sql_mode;
        sql_mode = 1;
        if (in_work)
        {
                  commit_work ();
        }
        sql_mode = sql_s;
}

void rollbackwork (void)
{
        int sql_s;

        sql_s = sql_mode;
        sql_mode = 1;
        if (in_work)
        {
                  rollback_work ();
        }
        sql_mode = sql_s;
}

char *get_colstring (char *string)
/**
Typ eines Feldes als Charakter in String zurueckgeben.
Beispliel a_bz1 : char(24).
**/
{
           short len;
           short vorkomma;
           short nachkomma;
 
           if (coltype > 8)
           {
                           return (0);
           }
           switch (coltype)
           {
                   case SQLCHAR :
                   case SQLVCHAR :
                           sprintf (string,"%s(%hd)",typ_string[coltype],
                                                     collength);
                           break;
                   case SQLDECIMAL :
                           len = dec_len (collength,&vorkomma,&nachkomma);
                           sprintf (string,"%s(%hd,%hd)",typ_string[coltype],
                                                         len,
                                                         nachkomma);
                           break;
                   default :
                           strcpy (string,typ_string[coltype]);
                           break;
           }
           return (string);
}

short get_spez_typd (short typ)
/**
Typ eines Datenbankfeldes konvertieren. Date nicht konvertieren. 
**/
{
          return (spez_typ [typ]);
}


short get_spez_typ (short typ)
/**
Typ eines Datenbankfeldes konvertieren.
**/
{
          if (typ == SQLDATE)
          {
                        return(SQLCHAR);
          }
          return (spez_typ [typ]);
}
      
int get_coltyp_spez (char *tabelle, char *feldname)
/**
Typ eines Tabellenfeldes aus syscolumns holen und konvertiren.
**/
{
            short typ;

            typ = get_coltyp (tabelle,feldname);
            if (typ < 11)
            {
                        return (spez_typ [typ]);
            }
            return (typ);
}
       

int get_coltyp (char *tabelle, char *feldname)
/**
Typ eines Tabellenfeldes aus syscolumns holen.
**/
{

            if (coldec == 0)
            {
                         declare_col ();
            }
            lese_col (tabelle, feldname);
            return (coltype);
}

int get_collen (char *tabelle, char *feldname)
/**
Typ eines Tabellenfeldes aus syscolumns holen.
**/
{

            if (coldec == 0)
            {
                         declare_col ();
            }
            lese_col (tabelle, feldname);
            return (collength);
}

short dec_len (short len, short *vorkomma, short *nachkomma)
/**
Laenge eines Double - oder Decimalfeldes  in Vor - und Nachkomma
zerlegen.
**/
{
       *nachkomma = len & 0xff;
       len >>= 8;
       *vorkomma  = len - *nachkomma;
       return (len);
}

TyLen get_feldlen (char *typ)
/**
Feldlaenge aus typ ermitteln.
**/
{
       short i;
       char *tpos;
       TyLen tynull;

       for (i = 0; typ_string [i] ; i ++)
       {
                  if ((tpos = strstr (typ,typ_string [i])))
                  {
                         return (len_aus_typ (tpos, i)); 
                  }
       }
       return (tynull);
}

TyLen len_aus_typ (char *tstring, short typ)
/**
Feldlaenge aus tstring ermitteln.
**/
{

       TyLen typinf;

       tstring += strlen (typ_string [typ]) + 1;
       if (typ < 10)
       {
                  typinf.typ = spez_typ [typ];
                  typ = spez_typ [typ];
       }
       else
       {
                  typinf.typ = typ;
       }

       switch (typ)
       {
                   case SQLSMINT :
                           typinf.len = sizeof (short);
                           break;
                   case SQLINT :
                           typinf.len = sizeof (int);
                           break;
                   case SQLFLOAT :
                           typinf.len = sizeof (double);
                           break;
                   case SQLDATE :
                           typinf.len = 12;
                           break;
                   default :
                           typinf.len = 0;
        }
                       

        if (typ == SQLCHAR)
        {
                   sscanf (tstring,"%hd",&typinf.len);
                   typinf.len ++;
        }
        return (typinf);  
}

int short_null (short swert)
/**
short auf NULLVALUE testen.
**/
{
       char *sstring;

       sstring = (char *) &swert;
       if (memcmp (sstring,SHORTNULL,sizeof (short)) == 0)
       {
                         return (1);
       }
       return (0);
}

int long_null (long lwert)
/**
long auf NULLVALUE testen.
**/
{
       char *lstring;

       lstring = (char *) &lwert;
       if (memcmp (lstring,LONGNULL,sizeof (long)) == 0)
       {
                         return (1);
       }
       return (0);
}

int double_null (double dwert)
/**
double auf NULLVALUE testen.
**/
{
       char *dstring;

       dstring = (char *) &dwert;
       if (memcmp (dstring,DOUBLENULL,sizeof (double)) == 0)
       {
                         return (1);
       }
       return (0);
}

int declare_col (void)
/**
Cursor fuer get_coltyp und get_collen declarieren.
**/
{
            out_quest ((char *) &tabid,1,0);
            ins_quest (tabname,0,18);
            tab_id = prepare_sql ("select tabid from systables "
                                  "where tabname = ?");

            out_quest ((char *) &coltype,1,0);
            out_quest ((char *) &collength,1,0);
            ins_quest ((char *) &tabid,1,0);
            ins_quest (colname,0,18);
            cols = prepare_sql ("select coltype, collength from syscolumns "
                                "where tabid   = ? "
                                "and   colname = ?");
            coldec = 1;
            return (0);
}

int lese_col (char *tabelle, char *feldname)
/**
In syscolumns und systables lesen.
**/
{

            coltype = -1;
            collength = -1;
            sqlstatus = 0;

            strcpy (tabname,tabelle); 
            strcpy (colname,feldname); 

            open_sql (tab_id);
            fetch_sql (tab_id);
            if (sqlstatus)
            {
                      return (sqlstatus);
            }
            open_sql (cols);
            fetch_sql (cols);
            return (sqlstatus);
}

char *doubleform (char *format, short nr)
/**
Format fuer ein Decimalfeld festlegen.
**/
{

          short len;
          short vork;
          short nachk;

          len = sqllen (nr);

          if (len == sizeof (double))
          { 
                     strcpy (format,"%.4lf");
                     return (format);
          }

          vork  = (len >> 8) & 0xff;
          nachk = len & 0xff;
          sprintf (format,"%c%hd.%hdlf",'%',vork,nachk);
          return (format);
}

void init_sql (void)
/**
Initialisierung bei 1. Aufruf.
**/
{       
          short i;

          erster_aufruf = 0;
          for (i = 0; i < MAXCURS; i ++) 
          {
                       sqcursor [i] = (_SQCURSOR *) 0;
                       sprintf (cunamen [i],"sdec_fetch%02hd", i);
                       sprintf (prepnamen [i],"sprp_fetch%02hd", i);
          }
}

int save_sql (char *sqltext, short nr)
/**
Aktuelles SQL-Commando sichern.
**/
{
          curssql [nr] = cu_malloc (strlen (sqltext) + 2); 
          if (curssql [nr] == 0) return (-1);
          strcpy ( curssql [nr], sqltext);
          return (0);
}

int showcurs (short nr, short modus)
/**
Informationen ueber Cursor anzeigen.
**/
{
       static char *sqlfunc [] = {"prepare",
                                  "open",
                                  "fetch", 
                                  "execute",
                                  "close",
                                  0};

       if (rcurs[nr].preped == 0)
       {
                 return (-1);
       }
       
       printf ("SQLSTATUS %hd\n", sqlstatus);

       if (sqcursor [nr] && sqcursor [nr]->_SQCname)
       {
                     printf ("%s cursor %s\n", sqlfunc [modus],
                                 sqcursor [nr]->_SQCname);
       }
       if (curssql [nr]) printf ("%s\n", curssql [nr]);
       
       if (modus == 1 && cursnivars [nr]) showivars (nr);
       else if (modus == 3 && cursnivars [nr]) showivars (nr);
       else if (modus == 2 && cursnovars [nr]) showovars (nr);
       printf ("\n");
       return (0);
}
                  
int showivars (short nr)
/**
Eingabevariablen anzeigen.
**/
{ 
       struct sqlvar_struct *ivars;
       short varanz;
       short i;

       ivars = cursivars [nr];
       varanz = cursnivars [nr];
       for (i = 0; i < varanz; i ++) showvar ("Eingabevariable", i, &ivars [i]);
       return (0);
}

int showovars (short nr)
/**
Eingabevariablen anzeigen.
**/
{ 
       struct sqlvar_struct *ovars;
       short varanz;
       short i;

       if (sqlstatus != 0) return (0);
       ovars = cursovars [nr];
       varanz = cursnovars [nr];
       for (i = 0; i < varanz; i ++) showvar ("Ausgabevariable", i, &ovars [i]);
       return (0);
}

int showvar (char *text, short nr, struct sqlvar_struct *var)
/**
SQLvariable anzeigen.
**/
{
        short swert;
        long  lwert;
        double dwert;
        char ausgabe0[256];
        char ausgabe [80];


        sprintf (ausgabe0, "%s %hd  ", text, nr);
        switch (var->sqltype)
        {
                 case SQLCHAR :
                 case SQLVCHAR :
                            sprintf (ausgabe, "%s", var->sqldata);
                            break;
                 case SQLSMINT :
                            memcpy 
                             ((char *) &swert, var->sqldata, sizeof (short));
                            sprintf (ausgabe, "%hd", swert);
                            break;
                 case SQLINT :
                 case SQLDATE :
                            memcpy 
                             ((char *) &lwert, var->sqldata, sizeof (long));
                            sprintf (ausgabe, "%hd", lwert);
                            break;
                 case SQLFLOAT :
                            memcpy 
                             ((char *) &dwert, var->sqldata, sizeof (double));
                            sprintf (ausgabe, "%lf", dwert);
                            break;
        }
        strcat (ausgabe0, ausgabe);
        printf (ausgabe0);
        printf ("\n");
        return (0);
}

int make_msize (int size)
/**
Groesse fuer cu_malloc auf 128-iger Vielfaches aufrunden.
**/
{
        int mul;

        mul = size / 128;
        // size_malloc = (mul + 1) * 128;
        size_malloc = size;
        return (0);
}

char *cu_malloc (int size)
/**
Malloc aufrufen.
**/
{
        make_msize (size);
        return ((char *) malloc (size_malloc));
}
       
