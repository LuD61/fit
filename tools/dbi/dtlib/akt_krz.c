#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#ifdef ODBC
#include "dbclass.h"
#else
#include "mo_curso.h"
#endif
#include "strfkt.h"
#include "a_pr.h"
#include "akt_krz.h"

struct AKT_KRZ _akt_krz, _akt_krz_null;


static short test_upd_a_pr;
static short cursor_akt_krz  = -1;
static short cursor_akt_krz_s = -1;
static short cursor_akt_krz_d = -1;
static short cursor_neu_pr   = -1;
static short test_upd_cursor = -1;
static short test_neu_pr     = -1;
static short upd_cursor      = -1;
static short upd_cursor1     = -1;
static short del_cursor1     = -1;
static short ins_cursor      = -1;
static short del_cursor      = -1;
static short aktiv_cursor     = -1;
static short upd_a_pr;
static short ins_a_pr;

int lese_akt_krzf (double a)
/**
Tabelle akt_krz fuer Artikel a lesen.
**/
{
         if (cursor_akt_krz == -1)
         {
                ins_quest ((char *) &_akt_krz.a, 3, 0);
                ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                ins_quest ((char *) &_akt_krz.fil, 1, 0);
                out_quest ((char *) &_akt_krz.pr_ek_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_ek_sa_euro, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk_sa_euro, 3, 0);
                out_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                out_quest ((char *) &_akt_krz.lad_akt_bis, 2, 0);
                out_quest ((char *) &_akt_krz.lad_akv_sa, 0, 1);
                cursor_akt_krz =
                    prepare_sql ("select pr_ek_sa, pr_ek_sa_euro,"
                                 "pr_vk_sa, pr_vk_sa_euro, "
                                 "lad_akt_von, lad_akt_bis, "
                                 "lad_akv_sa "
                                 "from akt_krz "
                                 "where a = ? "
                                 "and mdn_gr = ? "
                                 "and mdn = ? "
                                 "and fil_gr = ? "
                                 "and fil = ? "
                                 "and (lad_akv_sa = \"0\" or "
                                 "lad_akv_sa = \"1\")");
         }
         _akt_krz.a = a;
         open_sql (cursor_akt_krz);
         fetch_sql (cursor_akt_krz);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int lese_akt_krz (void)
/**
Naechsten Satz aus Tabelle a_pr lesen.
**/
{
         fetch_sql (cursor_akt_krz);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int lese_akt_krz_sf (short mdn_gr, short mdn,
                                   short fil_gr, short fil, double a)
/**
Tabelle akt_krz fuer aktive Aktion lesen.
**/
{
         if (cursor_akt_krz_s == -1)
         {
                ins_quest ((char *) &_akt_krz.a, 3, 0);
                ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                ins_quest ((char *) &_akt_krz.fil, 1, 0);
                out_quest ((char *) &_akt_krz.pr_ek_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_ek_sa_euro, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk_sa_euro, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk1_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk2_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk3_sa, 3, 0);
                out_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                out_quest ((char *) &_akt_krz.lad_akt_bis, 2, 0);
                out_quest ((char *) &_akt_krz.lad_akv_sa, 0, 1);
                cursor_akt_krz_s =
                    prepare_sql ("select pr_ek_sa, pr_ek_sa_euro, pr_vk_sa, pr_vk_sa_euro, "
                                 "pr_vk1_sa, pr_vk2_sa, pr_vk3_sa,"
                                 "lad_akt_von, lad_akt_bis, "
                                 "lad_akv_sa "
                                 "from akt_krz "
                                 "where a = ? "
                                 "and mdn_gr = ? "
                                 "and mdn = ? "
                                 "and fil_gr = ? "
                                 "and fil = ? "
                                 "and lad_akv_sa = \"1\"");
         }
         _akt_krz.mdn_gr = mdn_gr;
         _akt_krz.mdn    = mdn;
         _akt_krz.fil_gr = fil_gr;
         _akt_krz.fil    = fil;
         _akt_krz.a      = a;
         open_sql (cursor_akt_krz_s);
         fetch_sql (cursor_akt_krz_s);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int lese_akt_krz_s (void)
/**
Naechsten Satz aus Tabelle akt_krz lesen.
**/
{
         fetch_sql (cursor_akt_krz_s);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int lese_akt_krz_df (short mdn_gr, short mdn,
                                   short fil_gr, short fil,
                                   double a, char *datum)
/**
Tabelle akt_krz fuer aktive Aktion lesen.
**/
{
         static long ldat;

         if (cursor_akt_krz_d == -1)
         {
                ins_quest ((char *) &_akt_krz.a, 3, 0);
                ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                ins_quest ((char *) &_akt_krz.fil, 1, 0);
                ins_quest ((char *) &ldat, 2, 0);
                ins_quest ((char *) &ldat, 2, 0);
                out_quest ((char *) &_akt_krz.pr_ek_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_ek_sa_euro, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk_sa_euro, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk1_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk2_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk3_sa, 3, 0);
                out_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                out_quest ((char *) &_akt_krz.lad_akt_bis, 2, 0);
                out_quest ((char *) &_akt_krz.lad_akv_sa, 0, 1);
                cursor_akt_krz_d =
                    prepare_sql ("select pr_ek_sa, pr_ek_sa_euro, pr_vk_sa, pr_vk_sa_euro, "
                                 "pr_vk1_sa, pr_vk2_sa, pr_vk3_sa,"
                                 "lad_akt_von, lad_akt_bis, "
                                 "lad_akv_sa "
                                 "from akt_krz "
                                 "where a = ? "
                                 "and mdn_gr = ? "
                                 "and mdn = ? "
                                 "and fil_gr = ? "
                                 "and fil = ? "
                                 "and lad_akv_sa >= \"0\""
                                 "and lad_akv_sa <= \"3\""
                                 "and lad_akt_von <= ? "
                                 "and lad_akt_bis >= ? ");
         }
         _akt_krz.mdn_gr = mdn_gr;
         _akt_krz.mdn    = mdn;
         _akt_krz.fil_gr = fil_gr;
         _akt_krz.fil    = fil;
         _akt_krz.a      = a;
         ldat = dasc_to_long (datum);
         open_sql (cursor_akt_krz_d);
         fetch_sql (cursor_akt_krz_d);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int lese_akt_krz_d (void)
/**
Naechsten Satz aus Tabelle akt_krz lesen.
**/
{
         fetch_sql (cursor_akt_krz_d);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}


int lese_neu_prf (double a)
/**
Tabelle akt_krz fuer Artikel a lesen.
**/
{
         if (cursor_neu_pr == -1)
         {
                ins_quest ((char *) &_akt_krz.a, 3, 0);
                ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                ins_quest ((char *) &_akt_krz.fil, 1, 0);
                out_quest ((char *) &_akt_krz.pr_ek_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_ek_sa_euro, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk_sa, 3, 0);
                out_quest ((char *) &_akt_krz.pr_vk_sa_euro, 3, 0);
                out_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                out_quest ((char *) &_akt_krz.lad_akt_bis, 2, 0);
                out_quest ((char *) &_akt_krz.lad_akv_sa, 0, 1);
                cursor_neu_pr =
                    prepare_sql ("select pr_ek_sa, pr_ek_sa_euro, pr_vk_sa, pr_vk_sa_euro, "
                                 "lad_akt_von, lad_akt_bis, "
                                 "lad_akv_sa "
                                 "from akt_krz "
                                 "where a = ? "
                                 "and mdn_gr = ? "
                                 "and mdn = ? "
                                 "and fil_gr = ? "
                                 "and fil = ? "
                                 "and lad_akv_sa = \"9\"");
         }
         _akt_krz.a = a;
         open_sql (cursor_neu_pr);
         fetch_sql (cursor_neu_pr);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int lese_neu_pr (void)
/**
Naechsten Satz aus Tabelle akt_krz fuer neue Preise lesen.
**/
{
         fetch_sql (cursor_neu_pr);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}


int update_akt_krz (double a, short mdn_gr, short mdn,
                    short fil_gr, short fil,
                    char *lad_akt_von, char *lad_akv_sa)
/**
Tabelle akt_krz aendern.
**/
{
         if (test_upd_cursor == -1)
         {
                  ins_quest ((char *) &_akt_krz.a, 3, 0);
                  ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil, 1, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                  test_upd_cursor = prepare_sql
                    ("select a from akt_krz where a = ? "
                                        "and mdn_gr = ? "
                                        "and mdn = ? " 
                                        "and fil_gr = ? " 
                                        "and fil = ? "
                                        "and lad_akt_von = ? for update");
                  ins_quest ((char *) &_akt_krz.pr_ek_sa, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_ek_sa_euro, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_vk_sa, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_vk_sa_euro, 3, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_bis, 2, 0);
                  ins_quest ((char *) _akt_krz.lad_akv_sa, 0, 1);
                  ins_quest ((char *) _akt_krz.lief_akv_sa, 0, 1);

                  ins_quest ((char *) &_akt_krz.a, 3, 0);
                  ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil, 1, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);

                  upd_cursor = prepare_sql
                    ("update akt_krz set pr_ek_sa = ?, pr_ek_sa_euro = ?, "
                     "pr_vk_sa = ?, pr_vk_sa_euro = ?, "
                     "lad_akt_von = ?, lad_akt_bis = ?, "
                     "lad_akv_sa = ?, lief_akv_sa = ? "
                     "where a = ? ",
                     "and mdn_gr = ? "
                     "and mdn = ? " 
                     "and fil_gr = ? " 
                     "and fil = ? "
                     "and lad_akt_von = ?");

         }
         if (ins_cursor == -1)
         {
                  ins_quest ((char *) &_akt_krz.a, 3, 0);
                  ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil, 1, 0);
                  ins_quest ((char *) &_akt_krz.pr_ek_sa, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_ek_sa_euro, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_vk_sa, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_vk_sa_euro, 3, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_bis, 2, 0);
                  ins_quest ((char *) &_akt_krz.bel_akt_von, 2, 0);
                  ins_quest ((char *) &_akt_krz.bel_akt_bis, 2, 0);
                  ins_quest ((char *) _akt_krz.lad_akv_sa, 0, 1);
                  ins_quest ((char *) _akt_krz.lief_akv_sa, 0, 1);
                  ins_cursor = prepare_sql
                    ("insert into akt_krz "
                     "(a, mdn_gr, mdn, fil_gr, fil, pr_ek_sa, pr_ek_sa_euro, "
                     "pr_vk_sa, pr_vk_sa_euro, "
                     "lad_akt_von, lad_akt_bis, bel_akt_von, bel_akt_bis, "
                     "lad_akv_sa, lief_akv_sa) "
                     "values "
                     "(?,?,?,?,?,?,?,?,?,?,?,?,?)");
         }
         _akt_krz.a      = a;
         _akt_krz.mdn_gr = mdn_gr;
         _akt_krz.mdn    = mdn;
         _akt_krz.fil_gr = fil_gr;
         _akt_krz.fil    = fil;
#ifdef ODBC
		 FromGerDate (&_akt_krz.lad_akt_von, lad_akt_von);
#else
         _akt_krz.lad_akt_von = dasc_to_long (lad_akt_von);
#endif
         open_sql (test_upd_cursor);
         fetch_sql (test_upd_cursor);
         strcpy (_akt_krz.lad_akv_sa, lad_akv_sa);
         strcpy (_akt_krz.lief_akv_sa, lad_akv_sa);
         if (sqlstatus == 0)
         {
                     execute_curs (upd_cursor);
         }
         else
         {
                     execute_curs (ins_cursor);
         }
         return (0);
}

int delete_akt_krz (double a, short mdn_gr, short mdn,
                    short fil_gr, short fil,
                    char *lad_akt_von)
/**
Tabelle akt_krz loeschen.
**/
{
         if (del_cursor == -1)
         {
                  ins_quest ((char *) &_akt_krz.a, 3, 0);
                  ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil, 1, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                  del_cursor = prepare_sql
                    ("delete from akt_krz where a = ? "
                                        "and mdn_gr = ? "
                                        "and mdn = ? " 
                                         "and fil_gr = ? " 
                                         "and fil = ? "
                                         "and lad_akt_von = ? "
                                         "and lad_akv_sa <> \"9\"");
         }
         _akt_krz.a      = a;
         _akt_krz.mdn_gr = mdn_gr;
         _akt_krz.mdn    = mdn;
         _akt_krz.fil_gr = fil_gr;
         _akt_krz.fil    = fil;
#ifdef ODBC
 		 FromGerDate (&_akt_krz.lad_akt_von, lad_akt_von);
#else
         _akt_krz.lad_akt_von = dasc_to_long (lad_akt_von);
#endif
         execute_curs (del_cursor);
         return (0);
}

int aktion_aktiv (double a, short mdn_gr, short mdn,
                    short fil_gr, short fil)
/**
Aktion testen.
**/
{
         if (aktiv_cursor == -1)
         {
                  ins_quest ((char *) &_akt_krz.a, 3, 0);
                  ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil, 1, 0);
                  aktiv_cursor = prepare_sql
                    ("select a from akt_krz where a = ? "
                                        "and mdn_gr = ? "
                                        "and mdn = ? " 
                                        "and fil_gr = ? " 
                                        "and fil = ? "
                                        "and lad_akv_sa < \"2\"");
         }
         _akt_krz.a      = a;
         _akt_krz.mdn_gr = mdn_gr;
         _akt_krz.mdn    = mdn;
         _akt_krz.fil_gr = fil_gr;
         _akt_krz.fil    = fil;
         open_sql (aktiv_cursor);
         fetch_sql (aktiv_cursor);
         if (sqlstatus == 0)
         {
                     return 1;
         }
         return (0);
}


int update_neu_pr (double a, short mdn_gr, short mdn,
                    short fil_gr, short fil,
                    char *lad_akt_von, char *lad_akv_sa)
/**
Tabelle akt_krz aendern.
**/
{
         if (test_neu_pr == -1)
         {
                  ins_quest ((char *) &_akt_krz.a, 3, 0);
                  ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil, 1, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                  test_neu_pr = prepare_sql
                    ("select a from akt_krz where a = ? "
                                        "and mdn_gr = ? "
                                        "and mdn = ? " 
                                        "and fil_gr = ? " 
                                        "and fil = ? "
                                        "and lad_akt_von = ? "
                                        "and lad_akv_sa = \"9\" for update");
                  ins_quest ((char *) &_akt_krz.pr_ek_sa, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_ek_sa_euro, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_vk_sa, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_vk_sa_euro, 3, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_von, 4, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_bis, 4, 0);
                  ins_quest ((char *) _akt_krz.lad_akv_sa, 0, 1);
                  ins_quest ((char *) _akt_krz.lief_akv_sa, 0, 1);

                  ins_quest ((char *) &_akt_krz.a, 3, 0);
                  ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil, 1, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                  upd_cursor1 = prepare_sql
                    ("update akt_krz set pr_ek_sa = ?, pr_ek_sa_euro = ?, "
                     "pr_vk_sa = ?, pr_vk_sa_euro = ?, "
                     "lad_akt_von = ?, lad_akt_bis = ?, "
                     "lad_akv_sa = ?, lief_akv_sa = ? "
                     "where a = ?",
                     "and mdn_gr = ? "
                     "and mdn = ? " 
                     "and fil_gr = ? " 
                     "and fil = ? "
                     "and lad_akt_von = ? "
                     "and lad_akv_sa = \"9\"");

         }
         if (upd_cursor == -1)
         {
                  ins_quest ((char *) &_akt_krz.a, 3, 0);
                  ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil, 1, 0);
                  ins_quest ((char *) &_akt_krz.pr_ek_sa, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_ek_sa_euro, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_vk_sa, 3, 0);
                  ins_quest ((char *) &_akt_krz.pr_vk_sa_euro, 3, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_von, 4, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_bis, 4, 0);
                  ins_quest ((char *) &_akt_krz.bel_akt_von, 4, 0);
                  ins_quest ((char *) &_akt_krz.bel_akt_bis, 4, 0);
                  ins_quest ((char *) _akt_krz.lad_akv_sa, 0, 1);
                  ins_quest ((char *) _akt_krz.lief_akv_sa, 0, 1);
                  ins_cursor = prepare_sql
                    ("insert into akt_krz "
                     "(a, mdn_gr, mdn, fil_gr, fil, pr_ek_sa, pr_ek_sa_euro, "
                     "pr_vk_sa, pr_vk_sa_euro, "
                     "lad_akt_von, lad_akt_bis, bel_akt_von, bel_akt_bis, "
                     "lad_akv_sa, lief_akv_sa) "
                     "values "
                     "(?,?,?,?,?,?,?,?,?,?,?,?,?)");
         }
         _akt_krz.a      = a;
         _akt_krz.mdn_gr = mdn_gr;
         _akt_krz.mdn    = mdn;
         _akt_krz.fil_gr = fil_gr;
         _akt_krz.fil    = fil;
#ifdef ODBC
		 FromGerDate (&_akt_krz.lad_akt_von, lad_akt_von);
#else
         _akt_krz.lad_akt_von = dasc_to_long (lad_akt_von);
#endif
         open_sql (test_neu_pr);
         fetch_sql (test_neu_pr);
         strcpy (_akt_krz.lad_akv_sa, lad_akv_sa);
         strcpy (_akt_krz.lief_akv_sa, lad_akv_sa);
         if (sqlstatus == 0)
         {
                     execute_curs (upd_cursor1);
         }
         else
         {
                     execute_curs (ins_cursor);
         }
         return (0);
}


int delete_neu_pr (double a, short mdn_gr, short mdn,
                    short fil_gr, short fil,
                    char *lad_akt_von)
/**
Tabelle akt_krz loeschen.
**/
{
         if (del_cursor1 == -1)
         {
                  ins_quest ((char *) &_akt_krz.a, 3, 0);
                  ins_quest ((char *) &_akt_krz.mdn_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.mdn, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil_gr, 1, 0);
                  ins_quest ((char *) &_akt_krz.fil, 1, 0);
                  ins_quest ((char *) &_akt_krz.lad_akt_von, 2, 0);
                  del_cursor1 = prepare_sql
                    ("delete from akt_krz where a = ? "
                                        "and mdn_gr = ? "
                                        "and mdn = ? " 
                                         "and fil_gr = ? " 
                                         "and fil = ? "
                                         "and lad_akt_von = ? "
                                         "and lad_akv_sa = \"9\"");
         }
         _akt_krz.a      = a;
         _akt_krz.mdn_gr = mdn_gr;
         _akt_krz.mdn    = mdn;
         _akt_krz.fil_gr = fil_gr;
         _akt_krz.fil    = fil;
#ifdef ODBC
		 FromGerDate (&_akt_krz.lad_akt_von, lad_akt_von);
#else
         _akt_krz.lad_akt_von = dasc_to_long (lad_akt_von);
#endif
         execute_curs (del_cursor1);
         return (0);
}

static void prepare  (void)
{
          ins_quest ((char *) &_a_pr.a, 3, 0);
          ins_quest ((char *) &_a_pr.mdn_gr, 1, 0);
          ins_quest ((char *) &_a_pr.mdn, 1, 0);
          ins_quest ((char *) &_a_pr.fil_gr, 1, 0);
          ins_quest ((char *) &_a_pr.fil, 1, 0);
          test_upd_a_pr = prepare_sql
                    ("select a from a_pr where a = ? "
                                        "and mdn_gr = ? "
                                        "and mdn = ? " 
                                        "and fil_gr = ? " 
                                        "and fil = ? for update");
          ins_quest ((char *) &_a_pr.pr_ek, 3, 0);
          ins_quest ((char *) &_a_pr.pr_ek_euro, 3, 0);
          ins_quest ((char *) &_a_pr.pr_vk, 3, 0);
          ins_quest ((char *) &_a_pr.pr_vk_euro, 3, 0);
          ins_quest ((char *) &_a_pr.akt, 1, 0);
          ins_quest ((char *) &_a_pr.lad_akv, 0, 1);
          ins_quest ((char *) &_a_pr.lief_akv, 0, 1);
          upd_a_pr = prepare_sql
                    ("update a_pr set pr_ek = ?, pr_ek_euro = ?, "
                     "pr_vk = ?, pr_vk_euro = ?, "
                     "akt = ?, lad_akv = ?, lief_akv = ? "
                     "where current of %s",
                      (char *) cursor_name (test_upd_a_pr));

          ins_quest ((char *) &_a_pr.a, 3, 0);
          ins_quest ((char *) &_a_pr.mdn_gr, 1, 0);
          ins_quest ((char *) &_a_pr.mdn, 1, 0);
          ins_quest ((char *) &_a_pr.fil_gr, 1, 0);
          ins_quest ((char *) &_a_pr.fil, 1, 0);
          ins_quest ((char *) &_a_pr.pr_ek, 3, 0);
          ins_quest ((char *) &_a_pr.pr_ek_euro, 3, 0);
          ins_quest ((char *) &_a_pr.pr_vk, 3, 0);
          ins_quest ((char *) &_a_pr.pr_vk_euro, 3, 0);
          ins_quest ((char *) &_a_pr.akt, 1, 0);
          ins_quest ((char *) &_a_pr.key_typ_dec13, 3, 0);
          ins_quest ((char *) &_a_pr.lad_akv, 0, 1);
          ins_quest ((char *) &_a_pr.lief_akv, 0, 1);
          ins_quest ((char *) &_a_pr.bearb, 2, 0);
          ins_quest (_a_pr.pers_nam, 0, 8);
          ins_a_pr = prepare_sql
                    ("insert into a_pr "
                     "(a, mdn_gr, mdn, fil_gr, fil, pr_ek, pr_ek_euro, "
                     "pr_vk, pr_vk_euro, "
                     "delstatus,akt, key_typ_dec13, lad_akv, lief_akv, "
                     "modif, bearb, pers_nam, pr_vk1, pr_vk2, pr_vk3) "
                     "values "
                     "(?,?,?,?,?,?,?, 0, ?, ?, ?, ?, \"B\", "
                     " ?, ?, 0, 0, 0)");
}

int update_a_pr_krz (double a, short mdn_gr, short mdn,
                          short fil_gr, short fil)
/**
Tabelle a_pr aendern.
**/
{
         char datum [12];

         _a_pr.a      = a;
         _a_pr.mdn_gr = mdn_gr;
         _a_pr.mdn    = mdn;
         _a_pr.fil_gr = fil_gr;
         _a_pr.fil    = fil;
         _a_pr.key_typ_dec13  = a;
         sysdate (datum);
         _a_pr.bearb = dasc_to_long (datum);
         strcpy (_a_pr.pers_nam, "TEST");
         open_sql (test_upd_a_pr);
         fetch_sql (test_upd_a_pr);
         if (sqlstatus == 0)
         {
                     execute_curs (upd_a_pr);
         }
         else
         {
                     execute_curs (ins_a_pr);
         }
         return (0);
}

void close_akt_krz (void)
/**
Cursor akt_krz loeschen.
**/
{
        if (cursor_akt_krz  == -1) return;

        close_sql (cursor_akt_krz);
        cursor_akt_krz = -1;
}
                  
void close_akt_krz_s (void)
/**
Cursor akt_krz loeschen.
**/
{
        if (cursor_akt_krz_s  == -1) return;

        close_sql (cursor_akt_krz_s);
        cursor_akt_krz_s = -1;
}

void close_akt_krz_d (void)
/**
Cursor akt_krz loeschen.
**/
{
        if (cursor_akt_krz_d  == -1) return;

        close_sql (cursor_akt_krz_d);
        cursor_akt_krz_d = -1;
}


void close_neu_pr (void)
/**
Cursor neu_pr loeschen.
**/
{
        if (cursor_neu_pr  == -1) return;

        close_sql (cursor_neu_pr);
        cursor_neu_pr   = -1;
}


void close_upd (void)
/**
Cursor fuer Update loeschen.
**/
{
        if (test_upd_cursor  == -1) return;

        close_sql (test_upd_cursor);
        close_sql (upd_cursor);
        close_sql (ins_cursor);
        test_upd_cursor = -1;
        upd_cursor      = -1;
        ins_cursor      = -1;
}

void close_del (void)
/**
Cursor fuer Delete loeschen.
**/
{
        if (del_cursor  == -1) return;

        close_sql (del_cursor);
        del_cursor      = -1;
}


void close_aktiv (void)
/**
Cursor fuer Aktive Aktion loeschen.
**/
{
        if (aktiv_cursor  == -1) return;

        close_sql (aktiv_cursor);
        aktiv_cursor      = -1;
}

void close_del_n (void)
/**
Cursor fuer Delete (Neue Preise loeschen.
**/
{
        if (del_cursor1  == -1) return;

        close_sql (del_cursor1);
        del_cursor1      = -1;
}


void close_upd_n (void)
/**
Cursor fuer Update (Neue Preise) loeschen.
**/
{
        if (test_neu_pr  == -1) return;

        close_sql (test_neu_pr);
        close_sql (upd_cursor1);
        test_neu_pr  = -1;
        upd_cursor1  = -1;
}

void close_ins (void)
/**
Cursor fuer Insert loeschen.
**/
{
        if (ins_cursor  == -1) return;

        close_sql (ins_cursor);
        ins_cursor      = -1;
}
