#ifdef  IDENT   /* rev. 3.51 */
static char ident[] = "@(#) swcos.h  VLUGLOTSA   18.01.96 by Dorn"
#endif         /* rev. 3.51 */
/***************************************************************************/
/* Programmname :  SWCOS.H                                                 */
/*-------------------------------------------------------------------------*/
/* Funktion :  OS-specific defines                                         */
/*-------------------------------------------------------------------------*/
/* Revision : 4.60     Datum : 18.01.96   erstellt von : M. Dorn           */
/*-------------------------------------------------------------------------*/
/* Aenderungsstand :                                                       */
/* Revision : 3.00  Datum : 17.09.93  geaendert von :  M. Dorn             */
/* Aenderung: F_SEP file separator                                         */
/* Revision : 3.08  Datum : 05.10.93  geaendert von :  M. Dorn             */
/* Aenderung: Union of IOSB structures                                     */
/* Revision : 3.20  Datum : 24.03.94  geaendert von :  M. Dorn             */
/* Aenderung: Modifications for IBM OS/2 and VMS                           */
/* Revision : 4.11  Datum : 16.06.95  geaendert von :  M. Dorn             */
/* Aenderung: Modifications for IBM OS4680                                 */
/* Revision : 4.20  Datum : 23.06.95  geaendert von :  M. Dorn             */
/* Aenderung: Modifications for IBM AS/400                                 */
/* Revision : 4.60 Datum : 16.01.96  geaendert von : M. Dorn               */
/* Aenderung: Modifications for WIN95/NT                                   */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/

#ifdef OS4680

char *config_environment(const char *);
#define   getenv  config_environment

#endif

/* binary XOR operation rev. 4.20 */
#ifdef AS400
#define  XORB    �
#else
#define  XORB    ^
#endif

/* fopen mode rev. 4.60 */
#if defined DOS || defined IBMOS2 || defined OS4680 || defined AS400 || defined WIN
#ifdef AS400
#define  APPEND_BIN         "ab,lrecl=1"
#define  READ_BIN           "rb,lrecl=1"
#define  READ_PLUS_BIN      "r+b,lrecl=1"
#define  WRITE_BIN          "wb,lrecl=1"
#define  WRITE_PLUS_BIN     "w+b,lrecl=1"
#else
#define  APPEND_BIN         "ab"
#define  READ_BIN           "rb"
#define  READ_PLUS_BIN      "r+b"
#define  WRITE_BIN          "wb"
#define  WRITE_PLUS_BIN     "w+b"
#endif
#else
#define  APPEND_BIN         "a"
#define  READ_BIN           "r"
#define  READ_PLUS_BIN      "r+"
#define  WRITE_BIN          "w"
#define  WRITE_PLUS_BIN     "w+"
#endif

#if defined DOS || defined WIN
#define APPEND_TEXT         "at"
#define APPEND_PLUS_TEXT    "a+t"
#define READ_TEXT           "rt"
#define READ_PLUS_TEXT      "r+t"
#define WRITE_TEXT          "wt"
#define WRITE_PLUS_TEXT     "w+t"
#else
#define APPEND_TEXT         "a"
#define APPEND_PLUS_TEXT    "a+"
#define READ_TEXT           "r"
#define READ_PLUS_TEXT      "r+"
#define WRITE_TEXT          "w"
#define WRITE_PLUS_TEXT     "w+"
#endif

#ifdef VMS /* rev. 2.21 md */
#define ASSIGN_ERR      -1
#endif  /* rev. 2.21 md */

#ifdef TOS4
#define pid_t int
extern char * getenv();
#endif

/* file status rev. 4.20 */
#if defined OS4680 || defined AS400
typedef short             dev_t;      /* <old device number> type */
typedef unsigned short    ino_t;      /* <old device number> type */
typedef long              off_t;      /* ?<offset> type */
typedef long              key_t;      /* IPC key type */
#ifndef AS400
typedef unsigned int      size_t;     /* len param for string funcs */
#endif
/*
 * stat structure, used by stat(2) and fstat(2)
 */

struct stat {
   dev_t st_dev;
   ino_t st_ino;
   unsigned short st_mode;
   short st_nlink;
   short st_uid;
   short st_gid;
   dev_t st_rdev;
   off_t st_size;
   time_t st_atime;
   time_t st_mtime;
   time_t st_ctime;
   };


#endif

/* Returncodes for System functions rev. 2.12 */

#ifndef OS4680

typedef int TYfh;
typedef int TYirr;
typedef int TYrwlen;

#else
typedef long TYfh;
typedef long TYirr;
typedef long TYrwlen;

#endif

/* file separator rev. 3.20 */

#if defined DOS || defined IBMOS2 || defined VMS || defined WIN || defined OS4680

#ifdef VMS
#define  F_SEP           ""
#else
#define  F_SEP           "\\"
#endif
#define  PAR_SEP  (char) 0x2f       /* Parameterkennung rev. 2.21                */

#else

#define  F_SEP           "/"
#define  PAR_SEP  (char) '-'        /* Parameterkennung rev. 4.32                */

#endif

#ifdef VMS     /* rev. 3.08 md */

typedef char TYenv[80]; /* environment value */

typedef struct iosb_struct
{
        short   int     stat,
                        len;
        int             dev_data;
} TY_IOSB;

typedef struct iosb_read
{
 u_short stat;
 u_short off_term;
 u_short term;
 u_short term_size;
} TY_IOSB_R;

typedef struct iosb_write
{
 u_short stat;
 u_short count;
 u_short aux1;
 u_short aux2;
} TY_IOSB_W;

typedef struct iosb_set
{
 u_short stat;
 u_char  trn_speed;
 u_char  rcv_speed;
 u_char  cr_fill_cnt;
 u_char  lf_fill_cnt;
 u_char  parity;
 u_char  aux;
} TY_IOSB_S;

typedef union
{
 TY_IOSB     IOSB;      /* io_status_block (no specific) */
 TY_IOSB_R   IOSB_R;    /* io_status_block (Read Function) */
 TY_IOSB_W   IOSB_W;    /* io_status_block (Write Fuction) */
 TY_IOSB_S   IOSB_S;    /* io_status_block (Set/Sense Fuction) */
} TYunion_IOSB;

#endif          /* rev. 3.08 md  */

#ifdef IBMOS2

/* device control rev. 3.20 */

typedef struct GetLineCtrl
{
 char DataBits;        /* character length 5-8 bits            */
 char Parity;          /* 0=N, 1=O, 2=E, 3=Mark, 4=Space       */
 char StopBits;        /* 0=1., 1=1.5, 2=2 stop bits           */
 char TransBreak;      /* 0 = not currently transmitting break */
                       /* 1 = currently transmitting break     */
} TYGetLineCtrl;

typedef struct SetLineCtrl
{
 char DataBits;        /* character length 5-8 bits            */
 char Parity;          /* 0=N, 1=O, 2=E, 3=Mark, 4=Space       */
 char StopBits;        /* 0=1., 1=1.5, 2=2 stop bits           */
} TYSetLineCtrl;

typedef struct DCBInfo
{
 WORD WriTimOut;       /* Write Timeout processing             */
 WORD ReadTimOut;      /* Read Timeout processing              */
 BYTE Flags1;          /* Control lines                        */
 BYTE Flags2;          /* Control flow                         */
 BYTE Flags3;          /* Timeout processing control           */
 BYTE ErrRepl;         /* Error Replacement character          */
 BYTE BrkRepl;         /* Break Replacement character          */
 BYTE XONChar;         /* XON Character                        */
 BYTE XOFFChar;        /* XOFF Character                       */
} TYDCBInfo;


#endif

/*-------------------------------------------------------------------------*/
/* --ENDE Include SWCOS.H--                                                */
