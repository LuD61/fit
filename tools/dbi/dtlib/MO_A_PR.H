#ifndef MO_A_PR_OK
#define MO_A_PR_OK 1

void mitfilbelosa (void);
void ohnefilbelosa ();
int fetch_a_pr (short, short, short, short, double);
int wr_a_pr (void);
int fetch_preis_lad (short,short,short,short,double,
                     double *, double *);
int fetch_preis_tag (short,short,short,short,double,
                     char *,
                     double *, double *);
#endif
