#ifndef _ENTERFUNC_DEF
#define _ENTERFUNC_DEF
#include "mo_enter.h"

class EnterF {
          private :
              fEnter *Enter;
              COLORREF Color;
          public :
              EnterF ()
              {
                  Color = GRAYCOL;
              }

              void SetColor (COLORREF Color)
              {
                  this->Color = Color;
              }

              ~EnterF ()
              {
              } 
              static int BreakOk (void);
              static int BreakCancel (void);
              static int BreakEnter (void);

              void SetPicture (char *); 
              void SetBkMode (int); 
              void EnterText (HWND, char *, char *);
              void EnterText (HWND, char *, char *, int, char *);
              void EnterUpDown (HWND, char *, char *);
};
#endif
