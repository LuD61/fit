#ifndef _DLG_DEF
#define _DLG_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_meld.h"
#include "formfield.h"

#ifndef STDSIZE
#define STDSIZE 120 
#endif
#define MAXCOLS 100
#define MAXINSTANCES 100

struct COLW
{
     char colname[21];
     COLORREF col;
};

typedef struct BI
{
	      HBITMAP Image;
		  HBITMAP Inactive;
} BitImage;


class DLG
{
         private : 
            void AddInstance (void);
            void DelInstance (void);
          protected :
            static DLG *ActiveDlg;
            static DLG *Instances [];
            static int instanz;
            static struct COLW ColWindows [];
            static int colwanz;
            static BOOL ScChar;
			static DWORD AktId;

            BOOL TrackAllways;
            HWND hTrack;
            HWND vTrack;
            int vScroll;
            int hScroll;
			int hScrollRange;
			int vScrollRange;
			int hScrollPage;
			int vScrollPage;
			int hScWidth;
			int vScWidth;
			int ScSize;
			int ScVSize;

            HWND tbMain;
            TBBUTTON *tbb;
            int tblen;  
            char **qInfo;
            UINT *qIdfrom;
            char **qhWndInfo;
            HWND **qhWndFrom;

            DLG *OldDlg;
  	        HWND hMainWindow;
     	    HWND hWnd;
     	    HWND hWndMain;
            mfont *Font;
            mfont *TmFont;
 	        DWORD currentfield;
            static int cupos;
            static double scrfx;
            static double scrfy;
            int x,y, cx, cy;
            int xorg,yorg, cxorg, cyorg;
            BOOL Pixel;
 	        char *Caption;
            int CharHeight;
            int CharWidth;
            HMENU hMenu;
            HWND hwndTB;
            HWND NextWindow;
            CFIELD *CurrentCfield;
            CFORM *fWork;
            struct PMENUE *menuetab;
            BOOL (*TestMenue) (HWND,UINT,WPARAM,LPARAM);
            void (*TimerProc) (void);
  		    DWORD style;
  		    DWORD StyleEx;
            COLORREF ClassBackground;
            COLORREF WinBackground;
            BOOL FieldSet;
            DWORD ShowModus;
            HBRUSH hbrBackground;
            HBITMAP  hBitmap;
            HBITMAP  hBitmapOrg;
            BMAP bMap;
            int Bitmapmode;
            void (*DockProg) ();
			BOOL WithTabStops;
			HWND *TabStops;
			int TabStoplen;
			int TabStopPos;
			BOOL Dialog;
            static HWND MessWindow;
            static HWND MessMain;
            static char MessText [];
            static BOOL MessEnabled;
			BOOL NoMain;

         protected :
            TEXTMETRIC DlgTm;                

          public :
	        static HINSTANCE hInstance;
            static BOOL MousePressed;
 	        DLG (int, int, int, int, char *, int, BOOL);
	        ~DLG ();

			void SetNoMain (BOOL b)
			{
				NoMain = b;
			}

			BOOL IsDialog (void)
			{
				return Dialog;
			}

            void SetNextTopWindow (HWND NextWindow)
            {
                this->NextWindow = NextWindow;
            }

            HWND GetNextTopWindow (void)
            {
                return NextWindow;
            }

            void EnableMess (BOOL b)
            {
                MessEnabled = b;
            }

            int GetCX (void)
            {
                return cx;
            }

            int GetCY (void)
            {
                return cy;
            }

            int GetX (void)
            {
                return x;
            }

            int GetY (void)
            {
                return y;
            }

            void SetMessWindow (HWND MessWindow)
            {
                this->MessWindow = MessWindow;
            }

            HWND GetMessWindow (void)
            {
                return MessWindow;
            }

            int GetCharWidth (void)
            {
                return CharWidth;
            }

            int GetCharHeight (void)
            {
                return CharHeight;
            }

			void SetWithTabstops  (BOOL b)
			{
				WithTabStops = b;
			}

            DLG *GetOldDlg (void)
            {
                return OldDlg;
            }

			BOOL GetWithTabStops (void)
			{
				return WithTabStops;
			}

			void SetTabStopPos (int s)
			{
				TabStopPos = s;
			}

			int GetTabStopPos (void)
			{
				return TabStopPos;
			}

            void SetDockProg (void (*dp) ())
            {
                DockProg = dp;
            }

            void SetShowModus (DWORD ShowModus)
            {
                this->ShowModus = ShowModus;
            }

            DWORD GetShowModus (void)
            {
                return ShowModus;
            }

            void SetTrackAllways (BOOL b)
            {
                TrackAllways = b;
            }

            void SettbMain (HWND hWnd)
            {
                tbMain = hWnd;
            }

            HWND GettbMain (void)
            {
                return tbMain;
            }

            HWND GethMainWindow (void)
            {
                return hMainWindow;
            }

            HWND GethWnd (void)
            {
                return hWnd;
            }

            HMENU GethMenu (void)
            {
                return hMenu;
            }

            void SethMenu (HMENU hMenu)
            {
                this->hMenu = hMenu;
            }

            HWND GethwndTB (void)
            {
                return hwndTB;
            }

            void SetCurrentCfield (CFIELD *Cfield)
            {
                CurrentCfield = Cfield;
            }

            CFIELD *GetCurrentCfield (void)
            {
                return CurrentCfield;
            }

            void SetCurrentFocus (void)
            {
                CFIELD *Cfield;

                Cfield = GetCurrentCfield ();
                if (Cfield != NULL)
                {
                    Cfield->SetFocus ();
                }
            }

            void SetClassBackground (COLORREF Col)
            {
                ClassBackground = Col;
            }

            void SetDialog (CFORM *fWork)
            {
                this->fWork = fWork;
            }

            CFORM *GetDialog (void)
            {
                return fWork;
            }

            void Validate (void)
            {
                InvalidateRect (hWnd, NULL, FALSE);
                UpdateWindow (hWnd);
            }

            void InValidate (void)
            {
                InvalidateRect (hWnd, NULL, TRUE);
                UpdateWindow (hWnd);
            }

            void SetBitmapmode (int Bitmapmode)
            {
                this->Bitmapmode = Bitmapmode;
            }

            int Getbitmapmode (void)
            {
                return Bitmapmode;
            }

            COLORREF GetWindBackgrond (void)
            {
                return WinBackground;
            }

            void SetWindow (HANDLE, HWND, HWND);
            void SetTmFont (mfont *);
            void SetWinBackground (COLORREF);
            void ScreenParam (double, double);
            void SetSize (int);
            void SetLocation (int, int);
            void Pack ();
            void Pack (int, int);
            void SetDimension (int, int);
            void SetCurrentID (DWORD);
            void SetCurrentName (char *);
            void SetMenue (struct PMENUE *, BOOL (*) (HWND,UINT,WPARAM,LPARAM));
            void SetToolbar (TBBUTTON *, int, char **, UINT *,
                              char **, HWND **); 
            HWND SetToolCombo (int, int, int);
            void SetComboTxt (HWND, char **, int);
            void SetStyle (DWORD);
            void SetStyleEx (DWORD);
            void TestScroll (void);
            void SetTabFocus (void);
            void FocusUp ();
            void FocusDown ();
 	        void ProcessMessages (void);
            void ShowDlg (HDC, form *);
            BOOL GetWindowClass (COLORREF, char **);
            HWND OpenWindow (HANDLE, HWND);
            void MoveWindow (int, int ,int ,int ,int);
            HWND OpenScrollWindow (HANDLE, HWND);
            void DestroyWindow (void);
            void ToClipboard (void);
            void FromClipboard (void);

            virtual BOOL OnKey1 (void);
            virtual BOOL OnKey2 (void);
            virtual BOOL OnKey3 (void);
            virtual BOOL OnKey4 (void);
            virtual BOOL OnKey5 (void);
            virtual BOOL OnKey6 (void);
            virtual BOOL OnKey7 (void);
            virtual BOOL OnKey8 (void);
            virtual BOOL OnKey9 (void);
            virtual BOOL OnKey10 (void);
            virtual BOOL OnKey11 (void);
            virtual BOOL OnKey12 (void);
            virtual BOOL OnKeyPrior (void);
            virtual BOOL OnKeyDown (void);
            virtual BOOL OnKeyUp (void);
            virtual BOOL OnKeyNext (void);
            virtual BOOL OnKeyHome (void);
            virtual BOOL OnKeyEnd (void);
            virtual BOOL OnKeyReturn (void);
            virtual BOOL OnKeyTab (void);
            virtual BOOL OnKeySTab (void);
            virtual BOOL OnKeyEscape (void);
            virtual BOOL OnKeyDelete (void);
            virtual BOOL OnKeyInsert (void);
            virtual BOOL OnKey (int);
            virtual BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            virtual BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnNotify  (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnMove (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnSetFocus (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnKillFocus (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnChildSetFocus (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnChildKillFocus (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnLButtonDown (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnRButtonDown (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnLButtonUp (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnRButtonUp (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnMouseMove (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnTimer (HWND,UINT,WPARAM,LPARAM);
            virtual BOOL OnUser (HWND,UINT,WPARAM,LPARAM);
            virtual void PrintComment (char *);
            static void Enable (CFORM *, char **, BOOL);
            static void ToForm (FORMFIELD **);
            static void FromForm (FORMFIELD **);
            static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
            BOOL     TrackNeeded (void);
            void     CreateTrack (void);
            void     DestroyTrack (void); 
            void     TestTrack (void);
            BOOL     VTrackNeeded (void);
            void     CreateVTrack (void);
            void     DestroyVTrack (void);
            void     TestVTrack (void);
            void     OnVScroll (WPARAM, LPARAM);
            void     OnHScroll (WPARAM, LPARAM);
            void     ReadBmp (HWND, char *, int, int);
            void     StrechBitmapMem (HDC, int, int);
            void     SethWndTabStop (HWND);
            void     UpdateTabstop (HWND);
			int      GetTabStoplen (CFORM *, int);
			void     AddTabStops (CFORM *);
			void     SetTabStops (CFORM *);
			void     DestroyTabStops (void);
			void     NewTabStops (CFORM *);
            void     SetTimer (UINT, UINT, TIMERPROC);
            void     SetTimerProc (void (*tm) (void));
            static   int      OpenMess (HWND);
            static   void     PrintMess (char *);
            static   int      MoveMess (void);
            static   void     CloseMess (void);
            static   COLORREF GetColor (char *);
            void     CallInfo (void);
};
#endif

