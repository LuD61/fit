#ifndef _RDONLY_DEF
#define _RDONLY_DEF
#include "wmaskc.h"


void ROSetBkColor (COLORREF);
void ROSetAktBack (COLORREF);
void ROSetAktVor (COLORREF);

HWND CreateRdOnly (HWND hMain, char *text, int x, int y, int cx, int cy, HFONT hFont, 
		       HINSTANCE hMainInst, DWORD ID);
HWND CreateRdOnlyEx (HWND hMain, char *text, int x, int y, int cx, int cy, HFONT hFont, 
		       HINSTANCE hMainInst, DWORD, DWORD, DWORD);
#endif
