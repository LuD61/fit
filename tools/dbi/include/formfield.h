#ifndef _FORMFIELD_DEF
#define _FORMFIELD_DEF
#include "strfkt.h"

#define FCHAR 0
#define FSHORT 1
#define FLONG 2
#define FDOUBLE 3



class FORMFIELD
{
  private :
      char *Item;
      char *formvalue;
      void *memvalue;
      int memtype;  
      char *picture;
  public :
      FORMFIELD (char *Item, char *formvalue, void *memvalue, int memtype, char *picture)
      {
          this->Item      = Item;
          this->formvalue = formvalue;
          this->memvalue  = memvalue;
          this->memtype   = memtype;
          this->picture   = picture;
      }

      void SetItem (char *Item)
      {
          if (this == NULL) return;
          this->Item = Item;
      }

      void Setformvalue (char *formvalue)
      {
          if (this == NULL) return;
          this->formvalue = formvalue;
      }

      void Setmemvalue (char *memvalue)
      {
          if (this == NULL) return;
          this->memvalue = memvalue;
      }

      void Setmemtype (int memtype)
      {
          if (this == NULL) return;
          this->memtype   = memtype;
      }

      void Setpicture (char *picture)
      {
          if (this == NULL) return;
          this->picture = picture;
      }

      char *GetItem (void)
      {
          if (this == NULL) NULL;
          return Item;
      }

      char *Getformvalue (void)
      {
          if (this == NULL) NULL;
          return formvalue;
      }

      void *Getmemvalue (void)
      {
          if (this == NULL) NULL;
          return memvalue;
      }

      int Getmemtype (void)
      {
          if (this == NULL) NULL;
          return memtype;
      }
      char *Getpicture (void)
      {
          if (this == NULL) NULL;
          return picture;
      }

      void ToForm (void)
      {
          if (this == NULL) NULL;
          switch (memtype)
          {
                 case FCHAR :
                        clipped ((char *) memvalue);
                        strcpy (formvalue, (char *) memvalue);
                        break;
                 case FSHORT :
                        if (picture == NULL) picture = "%hd"; 
                        sprintf (formvalue, picture, *(short *)memvalue);
                        break;
                 case FLONG :
                        if (picture == NULL) picture = "%ld"; 
                        sprintf (formvalue, picture, *(long *)memvalue);
                        break;
                 case FDOUBLE :
                        if (picture == NULL) picture = "%lf"; 
                        sprintf (formvalue, picture, *(double *)memvalue);
                        break;
          }
       } 

       void FromForm (void)
       {
          if (this == NULL) NULL;
          switch (memtype)
          {
                 case FCHAR :
                        clipped (formvalue);
                        strcpy ((char *) memvalue, formvalue);
                        break;
                 case FSHORT :
                        *(short *) memvalue = atoi (formvalue);
                        break;
                 case FLONG :
                        *(long *) memvalue = atoi (formvalue);
                        break;
                 case FDOUBLE :
                        *(double *) memvalue = ratod (formvalue);
                        break;
          }
       }
};

#endif
