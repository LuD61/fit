#ifndef _ITFONT_DEF
#define _ITFONT_DEF
#include "cmask.h"

class ItFont
{
  private :
      char *Item;
      mfont *mFont;
  public :
      ItFont (char *Item, mfont *mFont)
      {
          this->Item  = Item;
          this->mFont = mFont;
      }

      void SetItem (char *Item)
      {
          if (this == NULL) return;
          this->Item = Item;
      }

      void SetmFont (mfont *mFont)
      {
          if (this == NULL) return;
          this->mFont = mFont;
      }

      char *GetItem (void)
      {
          if (this == NULL) NULL;
          return Item;
      }

      mfont *GetFont (void)
      {
          if (this == NULL) return NULL;
          return mFont;
      }


      void SetFont (CFORM *cForm)
      {
          if (this == NULL) return;
          if (mFont != NULL && Item != NULL)
          {
                cForm->GetCfield (Item)->SetFont (mFont);
          }
      }
};
#endif
