#ifndef _BWS_ASC_DEF
#define _BWS_ASC_DEF

extern "C"
{
int setand_part (char*);
int setascii_name (char *);
int setinfo_name (char *);
int setename (char *);
int setanzeigen (int);
int setsqldebug (int);
int setfeldinh (int);
int settest (int);
int setbinaer (int);
int do_bwsasc (void);
}
#endif
