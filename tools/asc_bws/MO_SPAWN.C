#ifndef SINIX
#ident  "@(#) mo_spawn.c	2.000d	12.04.95	by RO"
#endif
/***
--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
-       Modulname               :       mo_spawn.c
-
        Exports                 :
         
        Imports                 :

-       Interne Prozeduren      :  
-
-       Externe Prozeduren      :    
                                        
-
-       Autor                   :       WR    
-       Erstellungsdatum        :       28.02.95
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       C
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Liest externe Daten von einem Ascii-File und
                                    bucht diese in die BWS-Datenbank.

        Module                  :   mo_spawn.c
-
--------------------------------------------------------------------------------
***/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <varargs.h>
#include <windows.h>
#include <errno.h>
#include "mo_spawn.h"

extern char *wort [];
static short anzeigen = 0;     /* Anzeigemodus                   */
static FILE *fpbat = NULL;
static char batname [256];

#include "split.h"

int stanz (mode)
/**
Flag fuer anzeigen setzen.
**/
short mode;
{
         anzeigen = mode;
}

int starte (char *format, ...)
/**
Programm starten.
**/
{
      short anz;
      int status;
      int pid;
      char programm [512];
      char prog [128];
      short i;
      char *args [30];
      va_list ap;

      set_env (format, format);
      va_start (ap, format);
      vsprintf (programm, format, ap);
      va_end (ap);
	  /*
      anz = split_s (programm);
      if (anz == 0) return (0);

      strcpy (prog, wort [1]);
      args [0]  = wort [1];
      for (i = 2; i <= anz; i ++) args [i - 1] = wort [i];
      args [i - 1] = 0;

      if (anzeigen)
      {
                   for (i = 0; args [i]; i ++) printf ("%s ", args [i]);
                   printf ("\n");
                   fflush (stdout);
      }

      spawnvp (prog, args); 
	  */
	  ProcWaitExec (programm, SW_SHOW, -1, 0, -1, 0);
      return (0);
}

int set_env (ziel, quelle)
/**
Environmentvariablen aufloesen.
**/
char *ziel;
char *quelle;
{
        char envstr [512];
       
        strcpy (envstr, quelle);
        while (environment (envstr, envstr) == 0);
        strcpy (ziel, envstr);
}

int environment (zstr, quelle) 
/**
quelle in ziel uebertragen, dabei Environmentvariablen aufloesen.
**/
char *zstr;
char *quelle;
{
         char *ps;
         short i;
         char env [80];
         char ziel [512];
         extern char *getenv ();

         ziel [0] = 0;
         ps = strchr (quelle,'$');
         if (ps == 0)
         {
                        strcpy (ziel,quelle);
                        return (-1);
         }

         if (ps > quelle) 
         {
                        memcpy (ziel, quelle, ps - quelle);        
                        ziel [ps - quelle] = 0;
         }
         ps += 1;
         for (i = 0; ps [i]; i ++) 
         {
                        if (ps [i] <= ' ') break;
                        if (ps [i] == '$') break;
                        if (ps [i] == '/') break;
                        if (ps [i] == '\\') break;
                        if (i == 79) break; 
                        env [i] = ps [i];
         }
         env [i] = 0;
		 if (getenv (env) == NULL)
		 {
                        strcpy (ziel,quelle);
                        return (0);
         }

         strcat (ziel,getenv (env));
         strcat (ziel, &ps [i]);
         strcpy (zstr, ziel);
         return (0);
}
      

int ProcExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        if (SHOW_MODE == 0)
        {
               SHOW_MODE = SW_SHOW;
        }
        set_env (prog, prog);
        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  CloseHandle (pi.hProcess);
        }
        return ret;
}

DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        set_env (prog, prog);
        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}

int CreateBatch (char *format, ...)
/**
Programm starten.
**/
{
      char programm [512];
      va_list ap;
      char *bws;

      bws = getenv ("BWS");
      if (bws == NULL)
      {
               strcpy (batname, "fitcmd.bat");
      }
      else
      {
               sprintf (batname, "%s\\bin\\fitcmd.bat", bws);
      }

      fpbat = fopen (batname, "w");
      if (fpbat == NULL) return (1);

      set_env (format, format);
      va_start (ap, format);
      vsprintf (programm, format, ap);
      va_end (ap);
//      fprintf (fpbat, "echo off\n");
      fprintf (fpbat, "%s\n", programm);
      return (0);
}

int AppendBatch (char *format, ...)
/**
Programm starten.
**/
{
      char programm [512];
      va_list ap;
      char *bws;

      if (fpbat == NULL) return (1);

      set_env (format, format);
      va_start (ap, format);
      vsprintf (programm, format, ap);
      va_end (ap);
      fprintf (fpbat, "%s\n", programm);
      return (0);
}
int RunBatch (char *format, ...)
/**
Programm starten.
**/
{
      char programm [512];
      va_list ap;
      int ex;
      char *bws;

      if (fpbat == NULL) return (1);

      set_env (format, format);
      va_start (ap, format);
      vsprintf (programm, format, ap);
      va_end (ap);
      fprintf (fpbat, "%s\n", programm);
      fclose (fpbat);
      ex = starte (batname);
      if (ex == 0)
      {
          unlink (batname);
      }
      return (ex);
}


int RunBatchPause (char *format, ...)
/**
Programm starten.
**/
{
      char programm [512];
      va_list ap;
      int ex;
      char *bws;

      if (fpbat == NULL) return (1);

      set_env (format, format);
      va_start (ap, format);
      vsprintf (programm, format, ap);
      va_end (ap);
      fprintf (fpbat, "%s\n", programm);
      fprintf (fpbat, "pause\n");
      fclose (fpbat);
      ex = starte (batname);
      if (ex == 0)
      {
          unlink (batname);
      }
      return (ex);
}

