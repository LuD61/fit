#ifndef SINIX
#ident  "@(#) mo_proctab.c	VLUGLOTSA	23.10.96	by RO"
#endif
/*
  Dummy-Modul fuer mit Definition der Tabelle Spezialfunktionen fuer
  Standard asc_bws, bws_asc, dtod
*/ 

/* Structur fuer Proceduren und Funktionen    */

struct PROC
      {
        char *procname;
        int (*procadr) ();
      };

struct PROC proctab [] = { 0,  0};
