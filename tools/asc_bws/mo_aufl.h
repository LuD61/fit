#ifndef _MO_AUFL_DEF
#define _MO_AUFL_DEF

#define MAXLEN 40

class AUFPLIST
{
            private :
                int mamainmax;
                int mamainmin;
                HWND    hwndTB;
                char *eingabesatz;
                unsigned char *ausgabesatz;
                int zlen;
                int feld_anz;
                int banz;
                PAINTSTRUCT aktpaint;
                char *SwSaetze[30000];
                int PageView;
                unsigned char DlgSatz [20 + MAXLEN + 2];
                FELDER *LstZiel;
                unsigned char *LstSatz;
                int Lstzlen;
                int Lstbanz;
                int SwRecs;
                int AktRow;
                int AktColumn;
                int scrollpos;
                TEXTMETRIC tm;
                char InfoCaption [80];
                int WithMenue;
                int WithToolBar;
                BOOL ListAktiv;
                HWND hMainWindow;

             public :
                static void SetMax0 (int);
                static void SetMin0 (int);
                AUFPLIST ();
                void SetMenue (int with)
                {
                    WithMenue = with;
                }

                void SetToolMenue (int with)
                {
                    WithToolBar = with;
                }

                void SetMax (void)
                {
                    mamainmax = 1;
                    SetMax0 (mamainmax);
                }

                void SetMin (void)
                {
                    mamainmin = 1;
                    SetMin0 (mamainmin);
                }

                void InitMax (void)
                {
                    mamainmax = 0;
                    SetMax0 (mamainmax);
                }

                void InitMin (void)
                {
                    mamainmin = 0;
                    SetMin0 (mamainmin);
                }

                BOOL IsListAktiv (void)
                {
                    return ListAktiv;
                }

                void SethMainWindow (HWND hMainWindow)
                {
                    this->hMainWindow = hMainWindow;
                }

                HWND GetMamain1 (void);
                static int TestAppend (void);
                static int DeleteLine (void);
                static int InsertLine (void);
                static int AppendLine (void);
                static int SearchA (void);
                static void ReadPr (void);
                static void ReadMeEinh (void);

                static void rechne_aufme0 (void);
                static void rechne_aufme1 (void);
                static void rechne_aufme2 (void);
                static void rechne_aufme3 (void);

                static int testme (void);
                static int testpr (void);
                static int fetcha (void);
                static int fetcha_kun (void);
                static int ChMeEinh (void);
                static int Texte (void);
                static int Querya (void);
                static int setkey9me (void);
                static int Savea (void);
                static double GetAufMeVgl (void);
                static void UpdateBsd (void);
                static void DeleteBsd (void);
                static void WritePos (int);
                static int WriteAllPos (void);
                static int dokey5 (void);
                static int WriteRow (void);
                static int TestRow (void);
                static void GenNewPosi (void);
                static int PosiEnd (long);
                static void SaveAuf (void);
                static void SetAuf (void);
                static void RestoreAuf (void);
                static int Schirm (void);
                static int SetRowItem (void);
                static int  Getib (void);

                void InitSwSaetze (void);
                int ToMemory (int pos);
                void SetStringEnd (char *, int);
                void uebertragen (void);
                void ShowDB (short, short, long);
                void ReadDB (short, short, long);
                void SetSchirm (void);
                void EnterAufp (short, short, long,
                                short, long,  char *);
                void ShowAufp (short, short, long,
                                short, long,  char *);
                void DestroyWindows (void);
                void WorkAufp (void);
                void DestroyMainWindow (void);
                HWND CreateMainWindow (void);
                void MoveMamain1 ();
                void MaximizeMamain1 ();

                void SetChAttr (int);
                void SetFieldAttr (char *, int);
                void Geta_bz2_par (void);
                void Getauf_me_pr0 (void);
                void SetRecHeight (void);

                void SethwndTB (HWND);
                void SetTextMetric (TEXTMETRIC *);
                void SetLineRow (int);
                void SetListLines (int); 
                void OnPaint (HWND, UINT, WPARAM, LPARAM);
                void MoveListWindow (void);
                void BreakList (void);
                void OnHScroll (HWND, UINT, WPARAM, LPARAM);
                void OnVScroll (HWND, UINT, WPARAM, LPARAM);
                void OnSize (HWND, UINT, WPARAM, LPARAM);
                void FunkKeys (WPARAM, LPARAM);
                int  GetRecanz (void);
                void SwitchPage0 (int);
                HWND Getmamain2 (void);
                HWND Getmamain3 (void);
                void SetFont (mfont *); 
                void SetListFont (mfont *);
                void ChoiseFont (mfont *);
                void FindString (void);
                void SetLines (int);
                int GetAktRow (void);
                int GetAktRowS (void);
                void SetColors (COLORREF, COLORREF);
                void SetListFocus (void);
 
};
#endif