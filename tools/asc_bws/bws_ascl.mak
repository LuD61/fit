# Makefile "maketest": test Testprogramm
# BIZERBA Balingen VI-WW-B

.SUFFIXES: .obj .cpp

CC = cl
LINK = link
LIB = lib

CFLAGS = -c -W3 /Zi -D_NOCONSOLE
PROGRAMM = bws_asc.lib 
GUILIBS= user32.lib gdi32.lib winmm.lib comdlg32.lib comctl32.lib isqlt07c.lib
RCVARS=-r -DWIN32

OBJ =   mo_bwsas.obj \
        mo_arg.obj \
        mo_intp.obj \
        mo_spawn.obj \
        mo_sw.obj \
        mo_swkop.obj \
        mo_ean0.obj \
        datum.obj \
        strfkt.obj \
        sysdate.obj \
        mo_a_pr.obj \
        mo_preis.obj \
        mo_numme.obj \
        mo_curso.obj \
        mo_proct.obj


$(PROGRAMM): $(OBJ)
         $(LIB) /out:$(PROGRAMM) $(OBJ)
.c.obj :
        $(CC) $(CFLAGS) $*.c
