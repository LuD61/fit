#ifndef _BEST_BDF_DEF
#define _BEST_BDF_DEF

#include "dbclass.h"

struct BEST_BDF {
   double    a;
   short     bdf_status;
   long      bdf_term;
   short     fil;
   short     mdn;
   double    me;
   short     me_einh;
   long      melde_term;
};
extern struct BEST_BDF best_bdf, best_bdf_null;

#line 7 "best_bdf.rh"

class BEST_BDF_CLASS : public DB_CLASS 
{
       private :
               int del_cursor_a;
               void prepare (void);
       public :
               BEST_BDF_CLASS () : DB_CLASS (), del_cursor_a (-1)
               {
               }
               int dbreadfirst (void);
               int delete_a (void);
};
#endif
