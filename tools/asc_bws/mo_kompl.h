#ifndef _MO_KOMPL_DEF
#define _MO_KOMPL_DEF


class KompAuf
{
        private :
            int auf_a_sort; 
        public :
            KompAuf () : auf_a_sort (0)
            {
            }
            void BearbKomplett (void);
            int  SetKomplett (HWND);
            long GenLsNr (short, short);
            void AufktoLsk (void);
            void aufpttolspt (void);
            void AufptoLsp (long);
            double GetAufMeVgl(void);
            void TeilSmttoLs (void);
            void AuftoLs (void);
            void Getauf_a_sort (void);
            void AuftoPid (void);
            void InitPid (void);
            void K_Liste ();
};
#endif
