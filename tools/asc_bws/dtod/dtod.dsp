# Microsoft Developer Studio Project File - Name="dtod" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=dtod - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "dtod.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "dtod.mak" CFG="dtod - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "dtod - Win32 Release" (basierend auf\
  "Win32 (x86) Console Application")
!MESSAGE "dtod - Win32 Debug" (basierend auf\
  "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "dtod - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /w /W0 /GX /O2 /I "d:\informix\incl\esql" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "CONSOLE" /YX /FD /c
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib isqlt07c.lib /nologo /subsystem:console /machine:I386 /out:"d:\user\fit\bin\dtod.exe" /libpath:"d:\informix\lib"

!ELSEIF  "$(CFG)" == "dtod - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /w /W0 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "CONSOLE" /YX /FD /c
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib isqlt07c.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept /libpath:"d:\informix\lib"

!ENDIF 

# Begin Target

# Name "dtod - Win32 Release"
# Name "dtod - Win32 Debug"
# Begin Source File

SOURCE=..\DATUM.C
# End Source File
# Begin Source File

SOURCE=..\DTOD.C
# End Source File
# Begin Source File

SOURCE=..\MO_A_PR.C
# End Source File
# Begin Source File

SOURCE=..\MO_ARG.C
# End Source File
# Begin Source File

SOURCE=..\MO_CURSO.C
# End Source File
# Begin Source File

SOURCE=..\MO_DTOD.C
# End Source File
# Begin Source File

SOURCE=..\MO_EAN0.C
# End Source File
# Begin Source File

SOURCE=..\MO_EAN_G.C
# End Source File
# Begin Source File

SOURCE=..\MO_INTP.C
# End Source File
# Begin Source File

SOURCE=..\MO_NUMME.C
# End Source File
# Begin Source File

SOURCE=..\MO_PREIS.C
# End Source File
# Begin Source File

SOURCE=..\MO_SPAWN.C
# End Source File
# Begin Source File

SOURCE=..\MO_SW.C
# End Source File
# Begin Source File

SOURCE=..\MO_SWKOP.C
# End Source File
# Begin Source File

SOURCE=..\sleep.c
# End Source File
# Begin Source File

SOURCE=..\STRFKT.C
# End Source File
# Begin Source File

SOURCE=..\SYSDATE.C
# End Source File
# End Target
# End Project
