#ifndef SINIX
#ident  "@(#) dtod.c	VLUGLOTSA	28.02.97	by RO"
#endif
/**
*       Main-Programm fuer mo_dtod
*
**/

#include <stdio.h>

#define KONVERT "KONVERT"
#define GESPERRT 250

extern short ecode;               /* Exit-Code                      */
extern short FDEBUG;
extern char *make_name ();

extern char *eingabe;             /* Name der Ascii-Datei            */
extern char *ausgabe;             /* Name der Ascii-Datei            */

extern char *progname;            /* Aktueller Programmname          */
extern char *info_name;           /* Name der Info-Datei             */

extern char *argsp [];            /* Speicher fuer Argumente         */

extern short mit_default; /* Kennzeichen, ob ein Defaultsatz gelesen wird */
extern short anzeigen;   /* Flag fuer Anzeigemodus                        */
extern short sqldebug;   /* Flag fuer SQL-Debugmode                       */
extern short feldinh;    /* Feldinhalte Ziel                              */
extern short quick;      /* Flag fuer schnelles einbuchen                 */
extern short test;       /* Testmodus                                     */
extern short binaer;     /* Dateimodus                                    */
extern short append;     /* Flag fuer Append-Modus                        */
extern char *wr_modus;   /* Dateimodus fuer Aisgabedatei                  */
extern short show_code;  /* Programmzeilen anzeigen                       */
extern short sw_rec;     /* Flag fuer SW-Satzaufbau                       */
extern int arganz;       /* Anzahl Argumente                              */
extern int DebRun;       /* Debug-Kennzeichen                             */
extern int chainanz;
extern int dochain;
extern char *chainarg[];
extern char *charg[];

static short mehrere = 0; /* Kennzeichen fuer mehere Infodateien          */


int tst_arg (arg)
char *arg;
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                            case 'a' :
                                     anzeigen = 1;
                                     break;
                            case 'b' :
                                     binaer = 1;
                                     break;
                            case 'c' :
                                     show_code = 1;
                                     break;
                            case 'd' :
                                     DebRun = 1;
                                     break;
                            case 't' :
								     if (arg[1] == 'i' && arg[2])
									 {
                                             settrenneri (arg[2]);
                                     }
                                     else if (arg[1])
                                     {
                                             settrenner (arg[1]);
                                     }
                                     return;
                            case 'k' :
                                     sethochkomma ();
                                     break;
                            case 'l' :
                                     setclipped (atoi (arg + 1));
                                     break;
                            case 'm' : 
                                     mehrere = 1;
                                     break;
                            case 'f' :
                                     feldinh = 1;
                                     break;
                            case 'r' :
                                     append = 1;
									 if (binaer)
									 {
										wr_modus = "ab";
									 }
									 else
									 {
										wr_modus = "a";
									 }

                                     break;
                            case 's' :
                                     FDEBUG = 1;
                                     break;
                            case 'i' :
                                     eingabe = arg + 1;
                                     if (anzeigen)
                                          printf ("eingabe = %s\n", eingabe);
                                     return;
                            case 'o' :
                                     ausgabe = arg + 1;
                                     if (anzeigen)
                                          printf ("ausgabe = %s\n", ausgabe);
                                     return;
                            case 'w' :
                                     sw_rec = 1;
                                     break;
                            case 'h' :
                                     hilfe ();
                                     break;
                 }
          }
          return;
}

main (anz, arg)
int anz;
char *arg [];
{
          short i;
          int ret;


          progname = arg [0];

/* Flags mit - testen                           */

          argtst (&anz, arg, tst_arg);


          for (i = 0; i < 20; i ++)
          {
                     if (anz < i + 3) break;
                     argsp [i] = arg[i + 2];
          }
          arganz = i;

          info_name  = RifToIf (arg [1]);
          if (mehrere)
          {
                     ret = uebergebe_inf ();
          }
          else
          {
                     ret = do_dtod ();
          }
          if (ret == GESPERRT) 
          {
                     fprintf (stderr, "Ausgabedatei gesperrt\n");
                     ecode = ret;
          }
		  if (dochain)
		  {
			  dochain = 0;
			  chainmain (chainanz, charg);
		  }
          exit (ecode);
}

int uebergebe_inf ()
/**
Es werden mehrere Info-Dateien bearbeitet.
**/
{
          FILE *inf;
          static char infon [80];
          int anz;
          extern char *wort [];
          int ret;
          
          inf = fopen (make_name (KONVERT, info_name), "r");
          if (inf == 0) inf = fopen (info_name,"r");
          if (inf == 0)
          {
			            opendbase ("bws");
                        fprintf (stderr,"%s kann nicht geoeffnet werden\n",
                                         make_name (KONVERT,info_name));
                        exit (1);
          }

          while (fgets (infon,79,inf))
          {
                        cr_weg (infon);
                        if (anzeigen) printf ("%s\n", infon);
                        anz = split (infon);
                        anz ++;
                        info_name = infon; 
                        argtst (&anz, wort, tst_arg);
                        info_name  = RifToIf (wort[1]);
                        ret = do_dtod ();
 		                while (chainanz)
						{
			                     chainmain (chainanz, charg);
						}
                        if (ecode) break;
          }
          fclose (inf);
          return (0);
}


chainmain (anz, arg)
int anz;
char *arg [];
{

          short i;

/* Flags mit - testen                           */

          progname = arg [0];

/* Flags mit - testen                           */

          argtst (&anz, arg, tst_arg);


          for (i = 0; i < 20; i ++)
          {
                     if (anz < i + 3) break;
                     argsp [i] = arg[i + 2];
          }
          arganz = i;

          info_name  = RifToIf (arg [1]);

          do_dtod ();
		  if (dochain)
		  {
			  dochain = 0;
			  chainmain (chainanz, charg);
		  }
		  for (i = 0; i < chainanz; i ++) 
		  {
			  if (chainarg[i]) free (chainarg[i]);
			  chainarg[i] = (char *) 0;
		  }
		  chainanz = 0;
}


int hilfe ()
{
        opendbase ("bws");
        printf ("%s -acdfkmqrsw -lModus -tTrennzeichen -tiTrennzeichen "
                "-iEingabedatei -oAusgabedatei Infodatei\n", 
                 progname);
        printf ("  -a   Anzeigemodus\n");
        printf ("  -b   Binaerdatei\n");
        printf ("  -c   Programm-Teil anzeigen\n");
        printf ("  -d   Breakpoint zulassen\n");
        printf ("  -f   Feldinhalte von Datenbankbereich anzeigen\n");
        printf ("  -k   Datentyp C steht in Anfuehrungszeichen\n");
        printf ("  -m   Mehrere Info-Dateien werden bearbeitet\n");
        printf ("       In der Info_datei stehen die Namen der zu bearbeitenden"
                       " Infodateien\n");
        printf ("  -r   Ausgabe-Datei im Append-Modus oeffnen\n");
        printf ("  -s   Satzaufbau anzeigen\n");
        printf ("  -tz   Felder der Ausgabedatei sind durch Zeichen z getrennt\n");
        printf ("  -tiz  Felder der Eingabedatie sind durch Zeichen z getrennt\n");
        printf ("  -lm  In Verbindung mit k werden Blanks am Schluss "
                        "entfernt.\n");
        printf ("       m = 1   leerer String = \"\"\n");
        printf ("       m = 2   Wenn mit Trennzeichen leerer String "
                                "ohne \"\"\n" );
        printf ("  -w   Bei SW-Datei wird nur der Satzaufbau gelesen\n");
        printf ("       dtod -sw If-Datei : SW-Satzaufbau anzeigen\n");
        exit (0);
}
        
