# Microsoft Developer Studio Generated NMAKE File, Based on asc_bws.dsp
!IF "$(CFG)" == ""
CFG=asc_bws - Win32 Debug
!MESSAGE No configuration specified. Defaulting to asc_bws - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "asc_bws - Win32 Release" && "$(CFG)" !=\
 "asc_bws - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "asc_bws.mak" CFG="asc_bws - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "asc_bws - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "asc_bws - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "asc_bws - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

!IF "$(RECURSE)" == "0" 

ALL : "C:\USER\FIT\BIN\asc_bws.exe"

!ELSE 

ALL : "C:\USER\FIT\BIN\asc_bws.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\ASC_BWS.OBJ"
	-@erase "$(INTDIR)\DATUM.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\MO_ARG.OBJ"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_EAN0.OBJ"
	-@erase "$(INTDIR)\MO_INTP.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\MO_PROCT.OBJ"
	-@erase "$(INTDIR)\MO_SPAWN.OBJ"
	-@erase "$(INTDIR)\MO_SW.OBJ"
	-@erase "$(INTDIR)\MO_SWKOP.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\SYSDATE.OBJ"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "C:\USER\FIT\BIN\asc_bws.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /ML /w /W0 /GX /I "E:\INFORMIX\INCL\ESQL" /D "NDEBUG" /D\
 "WIN32" /D "_CONSOLE" /D "_MBCS" /D "CONSOLE" /Fp"$(INTDIR)\asc_bws.pch" /YX\
 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\asc_bws.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib isqlt07c.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)\asc_bws.pdb" /machine:I386 /out:"C:\USER\FIT\BIN\asc_bws.exe"\
 /libpath:"D:\INFORMIX\LIB" 
LINK32_OBJS= \
	"$(INTDIR)\ASC_BWS.OBJ" \
	"$(INTDIR)\DATUM.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\MO_ARG.OBJ" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_EAN0.OBJ" \
	"$(INTDIR)\MO_INTP.OBJ" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\MO_PROCT.OBJ" \
	"$(INTDIR)\MO_SPAWN.OBJ" \
	"$(INTDIR)\MO_SW.OBJ" \
	"$(INTDIR)\MO_SWKOP.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\SYSDATE.OBJ"

"C:\USER\FIT\BIN\asc_bws.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "asc_bws - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\asc_bws.exe"

!ELSE 

ALL : "$(OUTDIR)\asc_bws.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\ASC_BWS.OBJ"
	-@erase "$(INTDIR)\DATUM.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\MO_ARG.OBJ"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_EAN0.OBJ"
	-@erase "$(INTDIR)\MO_INTP.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\MO_PROCT.OBJ"
	-@erase "$(INTDIR)\MO_SPAWN.OBJ"
	-@erase "$(INTDIR)\MO_SW.OBJ"
	-@erase "$(INTDIR)\MO_SWKOP.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\SYSDATE.OBJ"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(OUTDIR)\asc_bws.exe"
	-@erase "$(OUTDIR)\asc_bws.ilk"
	-@erase "$(OUTDIR)\asc_bws.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MLd /w /W0 /Gm /GX /Zi /Od /I "E:\INFORMIX\INCL\ESQL" /D\
 "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "CONSOLE"\
 /Fp"$(INTDIR)\asc_bws.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\asc_bws.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib isqlt07c.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)\asc_bws.pdb" /debug /machine:I386 /out:"$(OUTDIR)\asc_bws.exe"\
 /pdbtype:sept /libpath:"E:\INFORMIX\LIB" 
LINK32_OBJS= \
	"$(INTDIR)\ASC_BWS.OBJ" \
	"$(INTDIR)\DATUM.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\MO_ARG.OBJ" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_EAN0.OBJ" \
	"$(INTDIR)\MO_INTP.OBJ" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\MO_PROCT.OBJ" \
	"$(INTDIR)\MO_SPAWN.OBJ" \
	"$(INTDIR)\MO_SW.OBJ" \
	"$(INTDIR)\MO_SWKOP.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\SYSDATE.OBJ"

"$(OUTDIR)\asc_bws.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(CFG)" == "asc_bws - Win32 Release" || "$(CFG)" ==\
 "asc_bws - Win32 Debug"
SOURCE=.\ASC_BWS.C

!IF  "$(CFG)" == "asc_bws - Win32 Release"

DEP_CPP_ASC_B=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	

"$(INTDIR)\ASC_BWS.OBJ" : $(SOURCE) $(DEP_CPP_ASC_B) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "asc_bws - Win32 Debug"

DEP_CPP_ASC_B=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	

"$(INTDIR)\ASC_BWS.OBJ" : $(SOURCE) $(DEP_CPP_ASC_B) "$(INTDIR)"


!ENDIF 

SOURCE=.\DATUM.C

"$(INTDIR)\DATUM.OBJ" : $(SOURCE) "$(INTDIR)"


SOURCE=.\MO_A_PR.C

!IF  "$(CFG)" == "asc_bws - Win32 Release"

DEP_CPP_MO_A_=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\allgemei.h"\
	".\mo_a_pr.h"\
	".\mo_curso.h"\
	".\sqlam.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "asc_bws - Win32 Debug"

DEP_CPP_MO_A_=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\allgemei.h"\
	".\mo_a_pr.h"\
	".\mo_curso.h"\
	".\sqlam.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_ARG.C

"$(INTDIR)\MO_ARG.OBJ" : $(SOURCE) "$(INTDIR)"


SOURCE=.\MO_CURSO.C

!IF  "$(CFG)" == "asc_bws - Win32 Release"

DEP_CPP_MO_CU=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	".\tcursor.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "asc_bws - Win32 Debug"

DEP_CPP_MO_CU=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	".\tcursor.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_EAN0.C

!IF  "$(CFG)" == "asc_bws - Win32 Release"

DEP_CPP_MO_EA=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	

"$(INTDIR)\MO_EAN0.OBJ" : $(SOURCE) $(DEP_CPP_MO_EA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "asc_bws - Win32 Debug"

DEP_CPP_MO_EA=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	

"$(INTDIR)\MO_EAN0.OBJ" : $(SOURCE) $(DEP_CPP_MO_EA) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_INTP.C

!IF  "$(CFG)" == "asc_bws - Win32 Release"

DEP_CPP_MO_IN=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	

"$(INTDIR)\MO_INTP.OBJ" : $(SOURCE) $(DEP_CPP_MO_IN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "asc_bws - Win32 Debug"

DEP_CPP_MO_IN=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	

"$(INTDIR)\MO_INTP.OBJ" : $(SOURCE) $(DEP_CPP_MO_IN) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_NUMME.C

!IF  "$(CFG)" == "asc_bws - Win32 Release"

DEP_CPP_MO_NU=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	".\sqlam.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "asc_bws - Win32 Debug"

DEP_CPP_MO_NU=\
	".\sqlam.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_PREIS.C

!IF  "$(CFG)" == "asc_bws - Win32 Release"

DEP_CPP_MO_PR=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\mo_preis.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "asc_bws - Win32 Debug"

DEP_CPP_MO_PR=\
	"..\..\informix\incl\esql\sqlca.h"\
	"..\..\informix\incl\esql\sqlda.h"\
	"..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\mo_preis.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_PROCT.C

"$(INTDIR)\MO_PROCT.OBJ" : $(SOURCE) "$(INTDIR)"


SOURCE=.\MO_SPAWN.C
DEP_CPP_MO_SP=\
	".\mo_spawn.h"\
	".\split.h"\
	

"$(INTDIR)\MO_SPAWN.OBJ" : $(SOURCE) $(DEP_CPP_MO_SP) "$(INTDIR)"


SOURCE=.\MO_SW.C

"$(INTDIR)\MO_SW.OBJ" : $(SOURCE) "$(INTDIR)"


SOURCE=.\MO_SWKOP.C
DEP_CPP_MO_SW=\
	".\swcomhd.h"\
	".\swcomhd3.h"\
	

"$(INTDIR)\MO_SWKOP.OBJ" : $(SOURCE) $(DEP_CPP_MO_SW) "$(INTDIR)"


SOURCE=.\STRFKT.C

"$(INTDIR)\STRFKT.OBJ" : $(SOURCE) "$(INTDIR)"


SOURCE=.\SYSDATE.C

"$(INTDIR)\SYSDATE.OBJ" : $(SOURCE) "$(INTDIR)"



!ENDIF 

