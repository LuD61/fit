# Makefile "maketest": test Testprogramm
# BIZERBA Balingen VI-WW-B

.SUFFIXES: .obj .cpp

CC = cl
LINK = link

CFLAGS = -c -W1 
PROGRAMM = dtod.exe 
GUILIBS= user32.lib gdi32.lib winmm.lib comdlg32.lib comctl32.lib isqlt07c.lib
RCVARS=-r -DWIN32

OBJ = dtod.obj \
        mo_dtod.obj \
        mo_arg.obj \
        mo_intp.obj \
        mo_spawn.obj \
        mo_sw.obj \
        mo_swkop.obj \
        datum.obj \
        strfkt.obj \
        sysdate.obj \
        mo_a_pr.obj \
        mo_preis.obj \
        mo_numme.obj \
        mo_curso.obj \
        mo_proct.obj


$(PROGRAMM): $(OBJ)
         $(CC) -Fedtod.exe $(OBJ) $(GUILIBS)
         copy dtod.exe C:\user\fit\bin\dtod.exe
.c.obj :
        $(CC) $(CFLAGS) $*.c
