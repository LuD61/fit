#ifndef SINIX
#ident  "@(#) mo_sw.c	2.001g	23.10.96	by RO"
#endif
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>

#define GESPERRT 250

extern char * swname ();

typedef struct
       {   char  feldname [20];
           short feldlen;
           char  feldtyp [2]; 
           short nachkomma;
           short dimension;
       } FELDER;

/* Informationen aus SW-Kopf                                      */
extern short blklen;            /* SW-Blocklaenge                        */
extern long blkanz;             /* Anzahl SW-Bloecke                     */
extern short banz;              /* Anzahl SW-Felder pro Satz             */
extern short klen;              /* Anzahl SW-Felder pro Satz             */
extern long blkanz_p;           /* Position im Kopf fuer Anzahl Saetze    */

static short swslen;            /* Laege des aktuellen SW-Satzes         */


int lese_sw ();                 /* SW-Datei bearbeiten                */
static void belege_swnamen (); /* Tabelle swfelder mit Namen aus Kopf fuellen */

static short kenlen = 2;                       /* Laenge der Kennung im Satz */
static int swfile = 0;      /* ID fuer SW-Datei         */

/*  Tabellen fuer SW-Saetze                            */    

static char swfelder [100][5];           /* Namen der SW-Felder      */
static short swpos [100];                /* Feldpositionen im Satz   */
static short swlen [100];                /* Feldlaenge               */
static short swtyp [100];                /* Feldtyp                  */
static short swanz;                      /* Anzahl Felder            */
static short sw_start = 0;               /* Flag fuer Lesen in SW-Datei */
static long swsanz = 0;                  /* Anzahl SW-Saezte         */  

int init_sw_start ()
/**
sw_start auf 0 setzen.
**/
{
          sw_start = 0;
}

int lese_sw (dname)
/**
SW-Datei lesen und anzeigen.
**/
char *dname;
{

/* SW-Datei neu oeffnen zum Kopflesen             */
/* notwendig, da read_k mit Datei - ID liest      */

          swfile = open (dname,O_RDONLY | O_BINARY);
          if (swfile < 0)
          {
 //                   if (errno == 13) return (GESPERRT);
                      schreibelog ("%s kann nicht geoeffnet werden",dname);
                      exit (1);
          }
          sw_file (swfile);            /* File-ID setzen            */
          if (read_k () == -1) 
          {
                     fprintf (stderr, "SW-Kopf konnte nicht gelesen werden\n");
                     return; /* SW-Kopf lesen             */
          }
          belege_swnamen ();
          close (swfile);
          return (0);
}

void belege_swnamen ()
/**
Tabelle swfelder mit Feldbezeichner aus Kopfsatz fuellen.
**/
{
          short i;

          swanz = banz;

          for (i = 0; i < swanz; i ++)
          {
                     memcpy (swfelder [i],swname (i),4);
                     swfelder [i][4] = 0;
          }
          make_satz (swfelder, swpos, swlen, swtyp, swanz);
      /*    sw_info ();  */
}
          
int sw_info (satzinfo)
/**
Satzaufbau aus SW-Kopf uebertragen.
**/
FELDER *satzinfo;
{
      short i;

      for (i = 0; i < swanz; i ++)
      {
                  memset (satzinfo[i].feldname, ' ', 19);
                  strncpy (satzinfo [i].feldname,swfelder[i],4);
                  satzinfo [i].feldname [4] = 0;
                  satzinfo [i].feldlen = swlen [i];
                  swtcpy (satzinfo [i].feldtyp,swtyp [i],swlen [i], i);
                  satzinfo [i].nachkomma = 0;
                  satzinfo [i].dimension = 1;

       }
       satzinfo [i].feldname [0] = 0;
       satzinfo [i].feldlen = 0;
       strcpy (satzinfo [i].feldtyp,"C");
       satzinfo [i].nachkomma = 0;
       satzinfo [i].dimension = 0;
}

int swtcpy (styp, swtyp, swlen,i)
/**
Typ aus SW uebertragen.
**/
char *styp;
short swtyp;
short swlen;
short i;
{
       styp [1] = 0;
       if (swtyp == 2 && i == swanz - 1)
       {
               styp[0] = 'C';
               return;
       }

       switch (swtyp)
       {
               case 1 :
               case 2 :
               case 9 :
               case 11 :
                        styp[0] = 'C'; 
                        break;
	       case 5 :
	       case 6 :
                        if (swlen == 2)
                        {
                               styp [0] = 'S';
                        }
                        else
                        if (swlen == 4)
                        {
                               styp [0] = 'I';
                        }
                        else if (swlen == 8)
                        {
                         styp [0] = 'T';
                        }
                        break;
                        break;

	       case 7 :
	       case 8 :
                        styp[0] = 'S'; 
                        break;
	       case 3 :
                        styp[0] = 'I'; 
                        break;
	       case 10 :
			            if (swlen == 2)
						{
                                styp[0] = 'u'; 
						}
						else
						{
                                styp[0] = 'U'; 
						}
                        break;
       }
}

int readsw (satz, swfile)
/**
Datensatz von SW-Datei lesen.
**/
char *satz;
FILE *swfile;
{
       if (sw_start == 0)
       {
                    sw_start = 1;
                    return (readsw_first (satz, swfile));
       }
       else
       {
                    return (readsw_next (satz, swfile));
       }
       return (-1);
}

static int rec_nr = 1;

int readsw_first (satz, swfile)
/**
1. SW-Datensatz lesen.
**/
char *satz;
FILE *swfile;
{
        short swlen;
        int bytes;

        fseek (swfile, klen, 0);
        bytes = fread (&swlen, 1, sizeof (swslen), swfile);
        if (bytes <= 0) return (0);
		rec_nr = 1;
        return (fread (satz, 1, swlen - sizeof (swlen), swfile));
}

int readsw_next (satz, swfile)
/**
Naechsten SW-Datensatz lesen.
**/
char *satz;
FILE *swfile;
{
        short swlen;
        int bytes;

        bytes = fread (&swlen, 1, sizeof (swslen), swfile);
        if (bytes <= 0) 
		{
			return (0);
		}
		rec_nr ++;
        return (fread (satz, 1, swlen - sizeof (swlen), swfile));
}

int writesw_first (satz, bytes, swfile)
/**
1. SW-Datensatz lesen.
**/
char *satz;
short bytes;
FILE *swfile;
{
        swsanz = 1;
        fseek (swfile, 0l, 2);
        bytes = getswlen (satz, bytes);
        bytes += sizeof (bytes);
        fwrite (&bytes, 1, sizeof (bytes), swfile);
        return (fwrite (satz, 1, bytes - sizeof (bytes), swfile));
}

int writesw_next (satz, bytes, swfile)
/**
Naechsten SW-Datensatz lesen.
**/
char *satz;
short bytes;
FILE *swfile;
{
 
        swsanz ++;
        bytes = getswlen (satz, bytes);
        bytes += sizeof (bytes);
        fwrite (&bytes, 1, sizeof (bytes), swfile);
        return (fwrite (satz, 1, bytes - sizeof (bytes), swfile));
}

int writesw (satz, bytes, swfile)
/**
Naechsten SW-Datensatz lesen.
**/
char *satz;
short bytes;
FILE *swfile;
{
        if (sw_start == 0)
        {
                   sw_start = 1;
                   return (writesw_first (satz, bytes, swfile));
       }
       else
       {
                    return (writesw_next (satz, bytes, swfile));
       }
       return (-1);
}

int writesw_anz (swfile)
/**
Anzahl Saetze in SW-Kopf schreiben.
**/ 
FILE *swfile;
{
        long sw_saetze;
        int pos;
        int bytes;

        sw_saetze = blkanz + swsanz; 
        pos = fseek (swfile, blkanz_p, 0);
        bytes = fwrite (&sw_saetze, 1, sizeof (sw_saetze), swfile);
        blkanz = sw_saetze;
        swsanz = 0;
        return (bytes);
}

int getswlen (satz, bytes)
/**
Laenge des SW-Satzes ermittlen.
**/
unsigned char *satz;
int bytes;
{
        short i;

        if (varsatz () == 0) return (bytes);

        for (i = bytes - 1; i > 0; i --)
        {
                     if (satz [i] > (unsigned char) 0) return (i + 2);
        }
        return (bytes);
}
