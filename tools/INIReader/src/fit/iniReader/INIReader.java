package fit.iniReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


public class INIReader {		
		
	public static Properties load(final String iniFile) throws FileNotFoundException, IOException {
		
		final Properties properties = new Properties();
		final FileInputStream in = new FileInputStream(iniFile);
		
		properties.load(in);		
		in.close();
		
		return properties;	
	}	

}
