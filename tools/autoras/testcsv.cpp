#include <windows.h>
#include <ras.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_fit.h"
#include "mo_arg.h"

#define MAXCONN 20




FitFit fit_fit ((long) 10000, "\15\12");

      char *frmenv;
static char import_files [80] [64];
static char export_file [80];
static long maxwait = 61;
static long DateiAnzahl = 0;
static long BreakSecs = 0;

static char ExeProc [512];
static char Environment [10];
static char Trennzeichen [20];
static char Debugmodus [3];
static char Projekt [40];
static char IfProcA [40];
static char IfProc1 [40];
static char IfProc2 [40];
static char IfProc3 [40];
static int Execute = 0;
static BOOL view = FALSE; 
static int ExeMode = 0;

static char messdat[256];
static char logname [256];
static long logzeilen = 10000l;
static char crnl[] = {13,10,0};

static char datum [12];
static char zeit [12];

static char WorkDirectory[512];
static char WorkDirectoryExport[512];


static int writelog (char * format, ...)
/**
Debugausgabe.
**/
{
          static int log_ok = 0;
          va_list args;
          FILE *logfile;
          char logtext [245];
          char logsatz [256];
          char datum [12];
          char zeit[12];
          long lpos;
          char *tmp;

          if (log_ok == 0)
          {
                   tmp = getenv ("TMPPATH");
                   if (tmp == (char *) 0)
                   {
                                tmp = "C:\\temp";
                   }
                   sprintf (logname, "%s\\testcsv.log", tmp);
                   tmp = getenv ("LOGZEILEN");
                   if (tmp)
                   {
                                logzeilen = atol (tmp + 10);
                   }
                   log_ok = 1;
          }

          logfile = fopen (logname, "rb+");
          if (logfile == (FILE *) 0)
          {
                     logfile = fopen (logname, "wb");
                     if (logfile == (FILE *) 0) return 0;
                     lpos = 0l;
                     sprintf (logsatz, "%8ld", lpos + 1);
                     fprintf (logfile, "%-78.78s%s", logsatz, crnl);
                     fclose (logfile);
                     logfile = fopen (logname, "rb+");
                     if (logfile == (FILE *) 0) return 0;
          }

          fgets (logsatz, 20, logfile);
          lpos = atol (logsatz);
          lpos -= (long) 1;

          if (lpos >= logzeilen) lpos = 0l;

          va_start (args, format);
          vsprintf (logtext, format, args);
          va_end (args);

          sysdate (datum);
          systime (zeit);
          sprintf (logsatz, "%-10.10s %-5.5s  %-60.60s", datum, zeit,logtext);

          lpos += 1;
          fseek (logfile, lpos * (long) 80, 0);
          cr_weg (logsatz);
          fprintf (logfile, "%s%s", logsatz, crnl);

          fseek (logfile, 0l, 0);
          sprintf (logsatz, "%8ld", lpos + 1);
          fprintf (logfile, "%-78.78s%s", logsatz, crnl);
          fclose (logfile);
          return 0;
}


void StartProg (void)
/**
Programm auf starten, wenn Message-Datei gefunden.
**/
{
	  int i ;
      char ziel [256];
      char quelle [256];
	    DWORD Ecode;


		if (Execute)
		{
		  for (i = 0;i < DateiAnzahl;i++)
		  {
			sprintf(ExeProc , "asc_bws %s -t\"%s%\" %s\\%s %s %s",Debugmodus,Trennzeichen,WorkDirectory,import_files[i],IfProc1, import_files[i]); 
 	        if (view)
			{
			          printf ("%s wird gestartet\n", ExeProc);
			}
            writelog ("%s wird gestartet", ExeProc);
		    if (ExeMode == 0)
			{
		              Ecode = ProcWaitExec (ExeProc, SW_HIDE, -1, 0, -1, 0);
			}
		    else
			{
		              Ecode = ProcWaitExec (ExeProc, SW_SHOW, -1, 0, -1, 0);
			}
 	        if (view)
			{
			          printf ("%s mit Status %ld beendet\n", ExeProc, Ecode);
			}
				writelog ("%s Status %ld beendet", ExeProc, Ecode);
// ==========   Aufruf des 2. If-Files ==================
			if (strlen(IfProc2) > 2)
			{
				sprintf(ExeProc , "asc_bws %s -t\"%s%\" %s\\%s %s",Debugmodus,Trennzeichen,WorkDirectory,import_files[i],IfProc2); 
 				if (view)
				{
						printf ("%s wird gestartet\n", ExeProc);
				}
				writelog ("%s wird gestartet", ExeProc);
				if (ExeMode == 0)
				{
						Ecode = ProcWaitExec (ExeProc, SW_HIDE, -1, 0, -1, 0);
				}
				else
				{
						Ecode = ProcWaitExec (ExeProc, SW_SHOW, -1, 0, -1, 0);
				}
 				if (view)
				{
						printf ("%s mit Status %ld beendet\n", ExeProc, Ecode);
				}
				writelog ("%s Status %ld beendet", ExeProc, Ecode);
			}
// ==========   Aufruf des 3. If-Files ==================
			if (strlen(IfProc3) > 2)
			{
				sprintf(ExeProc , "asc_bws %s -t\"%s%\" %s\\%s %s",Debugmodus,Trennzeichen,WorkDirectory,import_files[i],IfProc3); 
 				if (view)
				{
						printf ("%s wird gestartet\n", ExeProc);
				}
				writelog ("%s wird gestartet", ExeProc);
				if (ExeMode == 0)
				{
						Ecode = ProcWaitExec (ExeProc, SW_HIDE, -1, 0, -1, 0);
				}
				else
				{
						Ecode = ProcWaitExec (ExeProc, SW_SHOW, -1, 0, -1, 0);
				}
 				if (view)
				{
						printf ("%s mit Status %ld beendet\n", ExeProc, Ecode);
				}
				writelog ("%s Status %ld beendet", ExeProc, Ecode);
			}

		      sprintf (quelle, "%s\\%s", WorkDirectory,import_files[i]);
		      sprintf (ziel, "%s\\save\\%s", WorkDirectory,import_files[i]);
	         CopyFile (quelle,ziel, FALSE);
			 unlink(quelle);
		  }
		}
}

void StartProgExport (void)
/**
Programm auf starten, wenn Message-Datei gefunden.
**/
{
	    DWORD Ecode;
	  char datum [13];
	  char zeit [9];

      char frmdir [256];
	  char filenam [256];
      char *frmenv;
      WIN32_FIND_DATA lpffd;
      HANDLE hFile;
      int i;


		if (strlen(IfProcA) > 2)
		{

			sysdateYYYYMMDD (datum);
			systimeHHMM (zeit);
			sprintf(export_file,"Kunde_%s_%s.csv",datum,zeit);
			//=========== Export==================
			sprintf(ExeProc , "bws_asc %s -t\"%s%\" %s\\%s %s",Debugmodus,Trennzeichen,WorkDirectoryExport,export_file,IfProcA); 

				if (ExeMode == 0)
				{
						Ecode = ProcWaitExec (ExeProc, SW_HIDE, -1, 0, -1, 0);
				}
				else
				{
						Ecode = ProcWaitExec (ExeProc, SW_SHOW, -1, 0, -1, 0);
				}
 				if (view)
				{
						printf ("%s mit Status %ld beendet\n", ExeProc, Ecode);
				}
				writelog ("%s Status %ld beendet", ExeProc, Ecode);
		}



// Dateien die Leer sind l�schen
	  if (strlen(Environment) < 2) strcpy(Environment,"BWS");
      frmenv = getenv (Environment);
      if (frmenv == NULL) return ;

      sprintf (frmdir, "%s\\AdressenExport\\*%s", frmenv,messdat);

      hFile = FindFirstFile (frmdir, &lpffd);

      if (hFile == INVALID_HANDLE_VALUE)
      {
      }
	  else
	  {
 	   if (lpffd.nFileSizeLow < 2)
	   {
	      sprintf (filenam, "%s\\AdressenExport\\%s", frmenv,lpffd.cFileName);
		   unlink (filenam);
	   }
		while (FindNextFile (hFile, &lpffd))
		{
			   if (lpffd.nFileSizeLow < 2)
			   {
			      sprintf (filenam, "%s\\AdressenExport\\%s", frmenv,lpffd.cFileName);
				   unlink (filenam);
			   }

		}
	  }
      FindClose (hFile);

      DateiAnzahl = i;








}



long TestFile (void)
/**
Test, ob messdat existiert.
**/
{
      char frmdir [256];
 //     char ziel [256];
 //     char quelle [256];
      char *frmenv;
      WIN32_FIND_DATA lpffd;
      HANDLE hFile;
 //     char *p;
      int i;

	  if (strlen(Environment) < 2) strcpy(Environment,"BWS");
      frmenv = getenv (Environment);
      if (frmenv == NULL) return FALSE;

      sprintf (frmdir, "%s\\AdressenImport\\*%s", frmenv,messdat);
	  if (strstr (Projekt,"ImportPreise") != NULL )  sprintf (frmdir, "%s\\ImportPreise\\*%s", frmenv,messdat);
 
      hFile = FindFirstFile (frmdir, &lpffd);

      if (hFile == INVALID_HANDLE_VALUE)
      {
                      return FALSE;
      }

      i = 0;
//      p = strchr (lpffd.cFileName, '.');
//      if (p) *p = (char) 0;

  //    sprintf (quelle, "%s\\%s.cfg", getenv ("FRMPATH"), lpffd.cFileName);
//      sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
      strcpy (import_files[i], lpffd.cFileName);
			   if (view)
			   {
				  printf ("Datei: %s gefunden\n", lpffd.cFileName);
			   }
			   writelog ("Datei: %s gefunden\n", lpffd.cFileName);
      i ++;

      while (FindNextFile (hFile, &lpffd))
      {
//               p = strchr (lpffd.cFileName, '.');
//               if (p) *p = (char) 0;
               strcpy (import_files[i], lpffd.cFileName);
               i ++;
			   if (view)
			   {
				  printf ("Datei: %s gefunden\n", lpffd.cFileName);
			   }
			   writelog ("Datei: %s gefunden\n", lpffd.cFileName);

      }
      FindClose (hFile);

      DateiAnzahl = i;


	  return DateiAnzahl; 




	/***

        HANDLE fp;
        DWORD fsize;
		char buffer [256];
		DWORD bytes;
	    
        fp = CreateFile (messdat, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
			             NULL, 
                         OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (fp == INVALID_HANDLE_VALUE)
        {
                         return FALSE;
        }

        fsize = GetFileSize (fp, NULL);
        if (fsize == (DWORD) 0 ||
            fsize == 0xFFFFFFFF)
        {     
                         CloseHandle (fp);
                         return TRUE;
        }

        if (ReadFile (fp, buffer, 255, &bytes, NULL) == FALSE)
		{
            CloseHandle (fp);
			return TRUE;
		}
        CloseHandle (fp);
		if (buffer[0] == 'E' || buffer[0] == 'e')
		{
			if (view)
			{
				printf ("Testras �ber Message-Datei beendet\n");
			}
			writelog ("Testras �ber Message-Datei beendet");
		    unlink (messdat);
			exit (0);
		}
        return TRUE;
		**********/
}


void starttest (void)
/**
Start der Bearbeitungsschleife.
**/
{
	      long count;
		  if (strstr (Projekt,"M3B") != NULL ) 
		  {
			  strcpy (Trennzeichen,";");
			  strcpy (messdat,".csv");
			  strcpy (IfProc1,"m3b_import_kun.if");
			  strcpy (IfProc2,"m3b_import_adr.if");
			  strcpy (IfProc3,"m3b_import_ladr.if");
			  strcpy (IfProcA,"m3b_fromkun.if");
		  }
		  if (strstr (Projekt,"ImportPreise") != NULL ) 
		  {
			  strcpy (Trennzeichen,";");
			  strcpy (messdat,".csv");
			  strcpy (IfProc1,"ImportPreise.if");
			  strcpy (IfProc2,"");
			  strcpy (IfProc3,"");
			  strcpy (IfProcA,"");
		  }
	      if (view)
		  {
			  printf ("Testcsv gestartet\n");
			  printf ("Workdirectory %s\n", WorkDirectory);
			  printf ("Dateien = %s\n", messdat);
		  }
		  writelog ("Testcsv gestartet");
		  writelog ("Workdirectory %s", WorkDirectory);
		  writelog ("Dateien = %s", messdat);

		  count = 0;
		  while (TRUE)
		  {
			  DateiAnzahl = 0;
			  if (TestFile ())
			  {
//				  unlink (messdat);
				  StartProg ();
			  }
			  StartProgExport ();
			  Sleep (maxwait);
			  if (BreakSecs > 0l)
			  {
				  count ++;
				  if ((count * maxwait) >= BreakSecs)
				  {
					  exit (1);
				  }
			  }
		  }
}

void tst_arg (char *arg)    
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
				           case 'v' :
							         view = TRUE;
									 break;
				           case 'd' :
							         strcpy (Debugmodus," -d ");
									 break;
				           case 'p' :
							         strcpy (Projekt, arg + 1);
							         Execute = 1; 
							         return;

				           case 'x' :
							         strcpy (IfProc1, arg + 1);
							         Execute = 1; 
							         return;
				           case 'y' :
							         strcpy (IfProc2, arg + 1);
							         return;
				           case 'z' :
							         strcpy (IfProc3, arg + 1);
							         return;
				           case 't' :
							         strcpy (Trennzeichen, arg + 1);
		 							 return;
                           case 's' :
							         ExeMode = atoi (arg + 1);
		 							 return;
                           case 'e' :
							         strcpy (Environment, arg + 1);
		 							 return;
                           case 'w' :
							         maxwait = atoi (arg + 1);
									 return;
                           case 'b' : 
							         BreakSecs = atol (arg + 1) * 1000;
									 return;
                 }
          }
          return;
}


void SetWorkDirectory (LPTSTR lpCmdLine)
{
	  if (strlen(Environment) < 2) strcpy(Environment,"BWS");
      frmenv = getenv (Environment);
	  if (strstr (Projekt,"ImportPreise"))
	  {
		  sprintf (WorkDirectory,"%s\\%s",frmenv,"ImportPreise");
		  sprintf (WorkDirectoryExport,"");
	  }
	  else
	  {
		sprintf (WorkDirectory,"%s\\%s",frmenv,"AdressenImport");
		sprintf (WorkDirectoryExport,"%s\\%s",frmenv,"AdressenExport");
	  }
}
   
int main(int anz, char *varargs[])
{

	strcpy (Debugmodus," ");
    argtst (&anz, varargs, tst_arg);
	SetWorkDirectory (varargs[0]);

	if (anz < 2 && strlen (Projekt) < 2)  
	{
		      printf ("testcsv -vw -bSec -xIf-file1 -yIf-file2 -zIf-file3 -sShowModus Datei\n");
			  printf ("    v           Anzeigemodus\n");
			  printf ("    -d          -d = Debugmodus \n");
			  printf ("    w           Wartezeit in Sekunden. Default = 10\n");
			  printf ("    b           Abbruch nach Sec Sekunden\n");
			  printf ("    -e          Umgebungsvariable, zeigt auf das Directory\n");
			  printf ("                     in dem das Import-Verz. steht (default BWS)\n");
			  printf ("    ShowModus   0 Programm versteckt starten (Default)\n");
			  printf ("                1 Programm im Fenster starten\n");
			  printf ("    -p          Projekt (bisher M3B) wenn -p angegeben wird,\n");
			  printf ("		                 koennen die folgenden Angaben entfallen\n");
			  printf ("    -x          1.if-File  \n");
			  printf ("    -y          2.if-File (optional) \n");
			  printf ("    -z          3.if-File (optional) \n");
			  printf ("    -t          Trennzeichen z.B ; oder | \n");
			  printf ("    Datei      sucht im import-Verzeichnis nach *Datei z.b. *csv\n");
			  return (1);
    }

	maxwait *= (long) 1000;
	if (anz == 2) strcpy (messdat, varargs[1]);
    strcpy (ExeProc, "asc_bws");
	
	starttest ();
	return 0;
}



/*
    HANDLE Dir = FindFirstFileEx( 
		         filename,   // Eingabe mit Wildcards
                 FindExInfoStandard, 
                 &fileinfo,  // Ausgabe, Struktur mit Dateiinformationen 
                 FindExSearchNameMatch, 
                 NULL, 
                 0 );

	if (Dir == NULL || Dir == INVALID_HANDLE_VALUE)
	{
		DWORD err = GetLastError ();
	    ShowMessage = TRUE;
		return 0;
	}

	if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
	{
	}
	while (FindNextFile (Dir, &fileinfo))
	{

*/