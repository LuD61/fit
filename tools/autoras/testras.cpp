#include <windows.h>
#include <ras.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_fit.h"
#include "mo_arg.h"

#define MAXCONN 20




FitFit fit_fit ((long) 10000, "\15\12");

static long maxwait = 10;
static long BreakSecs = 0;

static char ExeProc [512];
static int Execute = 0;
static BOOL view = FALSE; 
static int ExeMode = 0;

static char messdat[256];
static char logname [256];
static long logzeilen = 10000l;
static char crnl[] = {13,10,0};

static char datum [12];
static char zeit [12];

static char WorkDirectory[512];


static int writelog (char * format, ...)
/**
Debugausgabe.
**/
{
          static int log_ok = 0;
          va_list args;
          FILE *logfile;
          char logtext [245];
          char logsatz [256];
          char datum [12];
          char zeit[12];
          long lpos;
          char *tmp;

          if (log_ok == 0)
          {
                   tmp = getenv ("TMPPATH");
                   if (tmp == (char *) 0)
                   {
                                tmp = "C:\\temp";
                   }
                   sprintf (logname, "%s\\testras.log", tmp);
                   tmp = getenv ("LOGZEILEN");
                   if (tmp)
                   {
                                logzeilen = atol (tmp + 10);
                   }
                   log_ok = 1;
          }

          logfile = fopen (logname, "rb+");
          if (logfile == (FILE *) 0)
          {
                     logfile = fopen (logname, "wb");
                     if (logfile == (FILE *) 0) return 0;
                     lpos = 0l;
                     sprintf (logsatz, "%8ld", lpos + 1);
                     fprintf (logfile, "%-78.78s%s", logsatz, crnl);
                     fclose (logfile);
                     logfile = fopen (logname, "rb+");
                     if (logfile == (FILE *) 0) return 0;
          }

          fgets (logsatz, 20, logfile);
          lpos = atol (logsatz);
          lpos -= (long) 1;

          if (lpos >= logzeilen) lpos = 0l;

          va_start (args, format);
          vsprintf (logtext, format, args);
          va_end (args);

          sysdate (datum);
          systime (zeit);
          sprintf (logsatz, "%-10.10s %-5.5s  %-60.60s", datum, zeit,logtext);

          lpos += 1;
          fseek (logfile, lpos * (long) 80, 0);
          cr_weg (logsatz);
          fprintf (logfile, "%s%s", logsatz, crnl);

          fseek (logfile, 0l, 0);
          sprintf (logsatz, "%8ld", lpos + 1);
          fprintf (logfile, "%-78.78s%s", logsatz, crnl);
          fclose (logfile);
          return 0;
}


void StartProg (void)
/**
Programm auf starten, wenn Message-Datei gefunden.
**/
{
	    DWORD Ecode;

	    if (view)
		{
			printf ("Message-Datei gefunden\n");
		}
		writelog ("Message-Datei gefunden");

		if (Execute)
		{
 	        if (view)
			{
			          printf ("%s wird gestartet\n", ExeProc);
			}
            writelog ("%s wird gestartet", ExeProc);
		    if (ExeMode == 0)
			{
		              Ecode = ProcWaitExec (ExeProc, SW_HIDE, -1, 0, -1, 0);
			}
		    else
			{
		              Ecode = ProcWaitExec (ExeProc, SW_SHOW, -1, 0, -1, 0);
			}
 	        if (view)
			{
			          printf ("%s mit Status %ld beendet\n", ExeProc, Ecode);
			}
            writelog ("%s mit Status %ld beendet", ExeProc, Ecode);
		}
}


BOOL TestFile (void)
/**
Test, ob messdat existiert.
**/
{
        HANDLE fp;
        DWORD fsize;
		char buffer [256];
		DWORD bytes;
	    
        fp = CreateFile (messdat, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
			             NULL, 
                         OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (fp == INVALID_HANDLE_VALUE)
        {
                         return FALSE;
        }

        fsize = GetFileSize (fp, NULL);
        if (fsize == (DWORD) 0 ||
            fsize == 0xFFFFFFFF)
        {     
                         CloseHandle (fp);
                         return TRUE;
        }

        if (ReadFile (fp, buffer, 255, &bytes, NULL) == FALSE)
		{
            CloseHandle (fp);
			return TRUE;
		}
        CloseHandle (fp);
		if (buffer[0] == 'E' || buffer[0] == 'e')
		{
			if (view)
			{
				printf ("Testras �ber Message-Datei beendet\n");
			}
			writelog ("Testras �ber Message-Datei beendet");
		    unlink (messdat);
			exit (0);
		}
        return TRUE;
}


void starttest (void)
/**
Start der Bearbeitungsschleife.
**/
{
	      long count;
	      if (view)
		  {
			  printf ("Testras gestartet\n");
			  printf ("Workdirectory %s", WorkDirectory);
			  printf ("Message-Datei = %s\n", messdat);
		  }
		  writelog ("Testras gestartet");
		  writelog ("Workdirectory %s", WorkDirectory);
		  writelog ("Message-Datei = %s", messdat);

		  count = 0;
		  while (TRUE)
		  {
			  if (TestFile ())
			  {
				  unlink (messdat);
				  StartProg ();
			  }
			  Sleep (maxwait);
			  if (BreakSecs > 0l)
			  {
				  count ++;
				  if ((count * maxwait) >= BreakSecs)
				  {
					  exit (1);
				  }
			  }
		  }
}

void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
				           case 'v' :
							         view = TRUE;
									 break;
				           case 'x' :
							         strcpy (ExeProc, arg + 1);
							         Execute = 1; 
							         return;
                           case 's' :
							         ExeMode = atoi (arg + 1);
									 return;
                           case 'w' :
							         maxwait = atoi (arg + 1);
									 return;
                           case 'b' : 
							         BreakSecs = atol (arg + 1) * 1000;
									 return;
                 }
          }
          return;
}


void SetWorkDirectory (LPTSTR lpCmdLine)
{
	TCHAR cmd[512];
	LPTSTR p;

	strcpy (cmd, lpCmdLine);
	p = strtok (cmd, " ");
	strcpy (cmd, p);


	strcpy (cmd, lpCmdLine);
	int i = strlen (cmd) - 1;
	for (;i >= 0; i --)
	{
		if (cmd[i] == '/' || cmd[i] == '\\')
		{
			cmd[i] = 0;
			break;
		}
	}
    strcpy (WorkDirectory, cmd);
	SetCurrentDirectory (WorkDirectory);
}
   
int main(int anz, char *varargs[])
{

    argtst (&anz, varargs, tst_arg);
	SetWorkDirectory (varargs[0]);

	if (anz < 2)
	{
		      printf ("testras -vw -bSec -xProgramm -sShowModus Message-Datei\n");
			  printf ("    v           Anzeigemodus\n");
			  printf ("    w           Wartezeit in Sekunden. Default = 10\n");
			  printf ("    b           Abbruch nach Sec Sekunden\n");
			  printf ("    ShowModus   0 Programm versteckt starten (Default)\n");
			  printf ("                1 Programm im Fenster starten\n");
			  return (1);
    }

	maxwait *= (long) 1000;
	strcpy (messdat, varargs[1]);
	starttest ();
	return 0;
}



/*
    HANDLE Dir = FindFirstFileEx( 
		         filename,   // Eingabe mit Wildcards
                 FindExInfoStandard, 
                 &fileinfo,  // Ausgabe, Struktur mit Dateiinformationen 
                 FindExSearchNameMatch, 
                 NULL, 
                 0 );

	if (Dir == NULL || Dir == INVALID_HANDLE_VALUE)
	{
		DWORD err = GetLastError ();
	    ShowMessage = TRUE;
		return 0;
	}

	if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
	{
	}
	while (FindNextFile (Dir, &fileinfo))
	{

*/