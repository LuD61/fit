#include <stdio.h>
#include "strfkt.h"
#ifdef WIN32
#include <process.h>
#include <windows.h>
#else
#include <sys/types.h>
#include <unistd.h>
#include "windtype.h"
#endif
#include "stdfkt.h"
#include "mo_flog.h"

void EventProt::WriteFirstLine (char *FileText)
/**
Satz in erste Zeile einer Datei schreiben.
**/
{
	   FILE *in;
       FILE *out; 
	   int pid;
	   int z;
	   char fname [256];
	   char buffer [256];

	   pid = getpid ();
       sprintf (fname, "touchwrite.%d", pid);
	   CopyFile (FileName, fname, FALSE);
       out = fopen (FileName, "w");
	   if (out == NULL) return;
	   fprintf (out, "%s\n", FileText);
	   in = fopen (fname, "r");
	   if (in == NULL) 
	   {
		   fclose (out);
		   return;
	   }
	   z = 0;
	   while (fgets (buffer, 255, in))
	   {
		   fputs (buffer, out);
		   if (z >= MAXLINES) break;
		   z ++;
	   }
	   fclose (in);
	   fclose (out);
	   unlink (fname);
}

void EventProt::WriteDTlog (int stat, long sys, char *Text)
/**
Text mit Datum, Uhrzeit, Fehlerstatus und Geraetenummer schreiben
**/
{
          char date [12];
          char time [10];
          char FileText [512];  
          
          sysdate (date);
          systime (time);

          sprintf (FileText, "%s %s %4d %8ld %s", date, time, stat, sys, Text);
          WriteFirstLine (FileText);
}
       

