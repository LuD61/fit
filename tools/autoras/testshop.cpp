#include <windows.h>
#include <ras.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_fit.h"
#include "mo_arg.h"

#define MAXCONN 20




FitFit fit_fit ((long) 10000, "\15\12");

      char *frmenv;
static char import_files [80] [64];
static char export_file [80];
static long maxwait = 3;  //alle 3 sec 
static long SchleifeDtodProg = 10;  //nach 10 * dtodProg restliche bearbeitung 
static long DateiAnzahl = 0;
static long BreakSecs = 0;

static char ExeProc [512];
static char Environment [10];
static char Trennzeichen [20];
static char Debugmodus [3];
static char Projekt [40];
static char IfProcA [40];
static char IfProc1 [40];
static char IfProc2 [40];
static char IfProc3 [40];
static int Execute = 0;
static BOOL view = FALSE; 
static int ExeMode = 0;

static char dtodProg[256];
static char logname [256];
static long logzeilen = 10000l;
static char crnl[] = {13,10,0};

static char datum [12];
static char zeit [12];

static char WorkDirectory[512];


static int writelog (char * format, ...)
/**
Debugausgabe.
**/
{
          static int log_ok = 0;
          va_list args;
          FILE *logfile;
          char logtext [245];
          char logsatz [256];
          char datum [12];
          char zeit[12];
          long lpos;
          char *tmp;

          if (log_ok == 0)
          {
                   tmp = getenv ("TMPPATH");
                   if (tmp == (char *) 0)
                   {
                                tmp = "C:\\temp";
                   }
                   sprintf (logname, "%s\\testcsv.log", tmp);
                   tmp = getenv ("LOGZEILEN");
                   if (tmp)
                   {
                                logzeilen = atol (tmp + 10);
                   }
                   log_ok = 1;
          }

          logfile = fopen (logname, "rb+");
          if (logfile == (FILE *) 0)
          {
                     logfile = fopen (logname, "wb");
                     if (logfile == (FILE *) 0) return 0;
                     lpos = 0l;
                     sprintf (logsatz, "%8ld", lpos + 1);
                     fprintf (logfile, "%-78.78s%s", logsatz, crnl);
                     fclose (logfile);
                     logfile = fopen (logname, "rb+");
                     if (logfile == (FILE *) 0) return 0;
          }

          fgets (logsatz, 20, logfile);
          lpos = atol (logsatz);
          lpos -= (long) 1;

          if (lpos >= logzeilen) lpos = 0l;

          va_start (args, format);
          vsprintf (logtext, format, args);
          va_end (args);

          sysdate (datum);
          systime (zeit);
          sprintf (logsatz, "%-10.10s %-5.5s  %-60.60s", datum, zeit,logtext);

          lpos += 1;
          fseek (logfile, lpos * (long) 80, 0);
          cr_weg (logsatz);
          fprintf (logfile, "%s%s", logsatz, crnl);

          fseek (logfile, 0l, 0);
          sprintf (logsatz, "%8ld", lpos + 1);
          fprintf (logfile, "%-78.78s%s", logsatz, crnl);
          fclose (logfile);
          return 0;
}


void StartProg (void)
/**
Programm auf starten, wenn Message-Datei gefunden.
**/
{
		    FILE *File;
		    DWORD Ecode;
	      char filenam [256];
	      char buffer [256];

			 sprintf (filenam, "%s\\auftraege",WorkDirectory);
			 unlink (filenam);


			sprintf(ExeProc , "dtod -d -o%s\\auftraege %s",WorkDirectory,"fromshopaufp.if"); 
             Ecode = ProcWaitExec (ExeProc, SW_HIDE, -1, 0, -1, 0);

		     File = fopen (filenam, "r");
			 int z = 0;
			if (fgets (buffer, 255, File))
			{
			   z ++;
			}
		    if (File != NULL) fclose (File);
		     if (z) 
			 {
				fclose (File);
				sprintf(ExeProc , "asc_bws  -d %s\\auftraege %s",WorkDirectory,"shopaufk.if 1 0"); 
				Ecode = ProcWaitExec (ExeProc, SW_HIDE, -1, 0, -1, 0);
	
				sprintf(ExeProc , "asc_bws -d  %s\\auftraege %s",WorkDirectory,"shopaufp.if 1 0"); 
				Ecode = ProcWaitExec (ExeProc, SW_HIDE, -1, 0, -1, 0);
			 }


}

void StartdtodProg (void)
/**
Tabelle Tour_dat aktualisieren 
**/
{
		    DWORD Ecode;


			if ( strlen (dtodProg) > 3)
			{
				sprintf(ExeProc , "dtod %s", dtodProg); 
				Ecode = ProcWaitExec (ExeProc, SW_HIDE, -1, 0, -1, 0);
			}
}


void starttest (void)
/**
Start der Bearbeitungsschleife.
**/
{
	      long count;
	      long scount;
		  count = 0;
		  scount = 1;
		  while (TRUE)
		  {
			  DateiAnzahl = 0;
			  scount --;
			  StartdtodProg ();
			  if (scount == 0)
			  {
				  scount = SchleifeDtodProg;
				  StartProg ();
			  }
			  Sleep (maxwait);
			  if (BreakSecs > 0l)
			  {
				  count ++;
				  if ((count * maxwait) >= BreakSecs)
				  {
					  exit (1);
				  }
			  }
		  }
}


void SetWorkDirectory (LPTSTR lpCmdLine)
{
	  if (strlen(Environment) < 2) strcpy(Environment,"BWS");
      frmenv = getenv (Environment);
	  sprintf (WorkDirectory,"%s\\%s",frmenv,"tmp");
}
   
int main(int anz, char *varargs[])
{

	strcpy (Debugmodus," ");
	SetWorkDirectory (varargs[0]);


	if (anz >= 2) 
	{
		strcpy (dtodProg, varargs[1]);
	}
	if (anz == 3) maxwait = atoi (varargs[2]);
	if (anz == 4) SchleifeDtodProg = atoi (varargs[2]);
	maxwait *= (long) 1000;
    strcpy (ExeProc, "asc_bws");
	
	starttest ();
	return 0;
}



/*
    HANDLE Dir = FindFirstFileEx( 
		         filename,   // Eingabe mit Wildcards
                 FindExInfoStandard, 
                 &fileinfo,  // Ausgabe, Struktur mit Dateiinformationen 
                 FindExSearchNameMatch, 
                 NULL, 
                 0 );

	if (Dir == NULL || Dir == INVALID_HANDLE_VALUE)
	{
		DWORD err = GetLastError ();
	    ShowMessage = TRUE;
		return 0;
	}

	if ((fileinfo.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
	{
	}
	while (FindNextFile (Dir, &fileinfo))
	{

*/