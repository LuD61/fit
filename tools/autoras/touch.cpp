#include <stdio.h>
#include <process.h>
#include <windows.h>
#include "mo_arg.h"

static int MAXLINES = 1000;
static BOOL FileWrite = FALSE;
static BOOL FileWriteB = FALSE;
static BOOL FileExist = FALSE;
static BOOL FileSize  = FALSE;
static BOOL FCreate  = FALSE;

static char FileText[512];


BOOL ExistFile (char *filename)
/**
Test, ob messdat existiert.
**/
{
        HANDLE fp;
		DWORD fsize;
	    
        fp = CreateFile (filename, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, 
			             NULL, 
                         OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (fp == INVALID_HANDLE_VALUE)
        {
                         return TRUE;
        }
		if (FileSize)
		{
                   fsize = GetFileSize (fp, NULL);
                   if (fsize == (DWORD) 0 ||
                             fsize == 0xFFFFFFFF)
				   {     
                         CloseHandle (fp);
                         return TRUE;
				   }
		}
        CloseHandle (fp);
		return FALSE;
}

void WriteFirstLine (char *filename)
/**
Satz in erste Zeile einer Datei schreiben.
**/
{
	   FILE *in;
       FILE *out; 
	   int pid;
	   int z;
	   char fname [256];
	   char buffer [256];

	   pid = _getpid ();
       sprintf (fname, "touchwrite.%d", pid);
	   CopyFile (filename, fname, FALSE);
       out = fopen (filename, "w");
	   if (out == NULL) return;
	   fprintf (out, "%s\n", FileText);
	   in = fopen (fname, "r");
	   if (in == NULL) 
	   {
		   fclose (out);
		   return;
	   }
	   z = 0;
	   while (fgets (buffer, 255, in))
	   {
		   fputs (buffer, out);
		   if (z >= MAXLINES) break;
		   z ++;
	   }
	   fclose (in);
	   fclose (out);
	   unlink (fname);
}



void TouchFile (char *filename)
/**
Test, ob messdat existiert.
**/
{
        FILE *fp;
	    
        fp = fopen (filename, "a");
        if (fp == NULL)
        {
                         return;
        }
		if (FileWrite)
		{
			fprintf (fp, "%s\n", FileText);
		}
		else if (FileWriteB)
		{
			fclose (fp);
			WriteFirstLine (filename);
		}
		fclose (fp);
}


void FileCreate (char *filename)
/**
Test, ob messdat existiert.
**/
{
        HANDLE fp;
	    
        fp = CreateFile (filename, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, 
			             NULL, 
                         CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        if (fp == INVALID_HANDLE_VALUE)
        {
			             printf ("Datei %s kann nicht erzeugt werden\n");  
                         return;
        }
        CloseHandle (fp);
}

   
void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                           case 's' :
							         FileSize = TRUE;
									 break;
                           case 'e' :
                                     FileExist = TRUE;
                                     break;
                           case 'w' :
                                     sprintf (FileText, "%s", arg + 1);
                                     FileWrite = TRUE;
                                     return;
                           case 'b' :
                                     sprintf (FileText, "%s", arg + 1);
                                     FileWriteB = TRUE;
                                     return;
						   case 'c' :			  
							         FCreate = TRUE;
									 break;
						   case 'm' :			  
							         MAXLINES = atoi (arg + 1);
									 break;
                    }
          }
          return;
}

int main(int anz, char *varargs[])
{

    argtst (&anz, varargs, tst_arg);

	if (anz < 2)
	{
		      printf ("touch -csewText -bText -mMAXLINES Datei-Name\n");
			  printf ("c      Datei auf Groesse 0 setzen\n");
			  printf ("s      Test, ob die Datei existiert und groesser als 0 Byte ist\n");
			  printf ("e      Test, ob die Datei existiert\n");
			  printf ("w      Schreibt Text in die Datei\n");
			  printf ("b      Schreibt Text in die 1. Zeile einer Datei\n");
			  printf ("m      maximale Anzahl Zeilen f�r Modus 'b' (DEFAULT 1000) \n");
			  return (1);
    }

	if (FCreate)
	{
 	          FileCreate (varargs[1]);
			  return 0;
	}

    if (FileExist)
	{
	          return ExistFile (varargs[1]);
	}

    TouchFile (varargs[1]);
	return 0;
}

