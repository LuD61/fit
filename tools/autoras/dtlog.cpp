#include <stdio.h>
#ifdef WIN32
#include <windows.h>
#endif
#include <stdarg.h>
#include "mo_flog.h"

EventProt Eventprot (100, "dfue.log");

int main (int varanz, char *varargs [])
{
        int i;
        int stat;
        long sys;
        char text [512];

        if (varanz < 4)
        {
                printf ("Usage dtlog status sys text\n");
                return 1;
        }
        stat = atoi (varargs[1]);  
        sys  = atol (varargs[2]);  
        strcpy (text, varargs[3]);
        for (i = 4; i < varanz; i ++)
        {
                strcat (text, " ") ;
                strcat (text, varargs[i]) ;
        }

        Eventprot.WriteDTlog (stat, sys, text);
        return 0;
} 
