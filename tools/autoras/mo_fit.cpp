#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <time.h>
#include <windows.h>
#include "mo_fit.h"

#define MAXTRIES 10000

static jrhstart = 70;
static jrh1 = 1900;
static jrh2 = 2000;

/**
Systemdatum holen.
**/
int FitFit::sysdate (char *datum)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 if (ltime->tm_year < 100)
 {
             if (ltime->tm_year < jrhstart)
             {
                  ltime->tm_year += jrh2;
             }
             else
             {
                  ltime->tm_year += jrh1;
             }
 }

 sprintf (datum, "%02d.%02d.%04d", ltime->tm_mday,
                                   ltime->tm_mon + 1,
                                   ltime->tm_year);
 return (0);
}

/**
Systemzeit holen.
**/
int FitFit::systime (char *zeit)
{
 time_t timer;
 struct tm *ltime;
 extern int daylight;

 time (&timer);
 daylight = 0;
 ltime = localtime (&timer);
 sprintf (zeit, "%02d:%02d:%02d", ltime->tm_hour,
                                  ltime->tm_min,
                                  ltime->tm_sec);
 return (0);
}

int FitFit::writelog (char * format, ...)
/**
Debugausgabe.
**/
{
          static int log_ok = 0;
          va_list args;
          FILE *logfile;
          char logtext [245];
          char logsatz [256];
          char datum [12];
          char zeit[12];
          long lpos;
          char *tmp;

          if (log_ok == 0)
          {
                   tmp = getenv ("TMPPATH");
                   if (tmp == (char *) 0)
                   {
                                return 0;
                   }
                   sprintf (logname, "%s\\bildfit.log", tmp);
                   tmp = getenv ("LOGZEILEN");
                   if (tmp)
                   {
                                logzeilen = atol (tmp + 10);
                   }
                   log_ok = 1;
          }

          logfile = fopen (logname, "rb+");
          if (logfile == (FILE *) 0)
          {
                     logfile = fopen (logname, "wb");
                     if (logfile == (FILE *) 0) return 0;
                     lpos = 0l;
                     sprintf (logsatz, "%8ld", lpos + 1);
                     fprintf (logfile, "%-78.78s%s", logsatz, crnl);
                     fclose (logfile);
                     logfile = fopen (logname, "rb+");
                     if (logfile == (FILE *) 0) return 0;
          }

          fgets (logsatz, 20, logfile);
          lpos = atol (logsatz);
          lpos -= (long) 1;

          if (lpos >= logzeilen) lpos = 0l;

          va_start (args, format);
          vsprintf (logtext, format, args);
          va_end (args);

          sysdate (datum);
          systime (zeit);
          sprintf (logsatz, "%-10.10s %-5.5s  %-60.60s", datum, zeit,logtext);

          lpos += 1;
          fseek (logfile, lpos * (long) 80, 0);
          fprintf (logfile, "%s%s", logsatz, crnl);

          fseek (logfile, 0l, 0);
          sprintf (logsatz, "%8ld", lpos + 1);
          fprintf (logfile, "%-78.78s%s", logsatz, crnl);
          fclose (logfile);
          return 0;
}


         
void FitFit::write_fit (char *command)
/**
Kommando in Warteschreife bearbeiten.
**/
{
          static int statname_ok = 0;
          char status [40];
          FILE *stat;
          char *tmp;
          int count;


 
          if (statname_ok == 0)
          {
                   tmp = getenv ("SERVFIT");
                   if (tmp == (char *) 0)
                   {
                                return;
                   }
                   sprintf (statname, "%s\\messfit.dat", tmp);
                    
                   statname_ok = 1;
          }
          count = 0;
          while (TRUE)
          {
                   stat = NULL;
                   stat = fopen (statname, "r");
                   if (stat)
                   {
                           status [0] = (char) 0;
                           fgets (status, 39, stat);
                           if (status[0] != '*')
                           {
                                  fclose (stat);
                           }
                           else
                           {
                                  break;
                           }
                   }
                   else
                   {
                           stat = fopen (statname, "w");
                           if (stat) break;
                   }
                   count ++;
                   if (MAXTRIES && count >= MAXTRIES) return;
                   Sleep (100);
          }

          if (stat) fclose (stat);
          stat = fopen (statname, "w");
          if (stat == (FILE *) 0)
          {
                   return;
          }

          fprintf (stat, "%s\n", command);
          fclose (stat);
          writelog ("%s in %s geschrieben", command, statname);
          return;
}

