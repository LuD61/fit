#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <windows.h>

void SaveFile (char *path, char *file)
/**
Aenderungsdatei sichern.
**/
{
      time_t timer;
      struct tm *ltime;
	  char buffer [256];
	  char quelle [256];
	  char ziel [256];
	  static char *days [] = {"Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", NULL};


      time (&timer);
      ltime = localtime (&timer);

      sprintf (buffer, "%s\\%s", path, days [ltime->tm_wday]);
      _mkdir (buffer);

	  sprintf (quelle, "%s\\%s", path, file);
	  sprintf (ziel, "%s\\%s", buffer, file);

	  if (CopyFile (quelle, ziel, FALSE)) unlink (quelle);
}

void GetPath (char *fpath, char *path, char *file)
/**
Verzeichnis aus Dateinamen holen.
**/
{
	   char *bs;

	   bs = fpath + strlen (fpath);

	   for (;bs > fpath; bs -= 1)
       {
		       if (*bs == '\\')
			   {
				         break;
			   }
	   }

	   if (bs == fpath)
       {
		        fpath[0] = (char) 0x0;
	   }

	   *bs = (char) 0x0; 
	   strcpy (path, fpath);
	   bs += 1;
	   strcpy (file, bs);
}


int main (int varanz, char *vararg[])
/**
Datei unter Wochentag sichern.
**/
{
	   char path [256];
	   char file [256];

	   if (varanz < 2)
	   {
		          return (1);
	   }

	   GetPath (vararg[1], path, file);
       SaveFile (path, file);
	   return (0);
}
