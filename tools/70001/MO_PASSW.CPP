/* *********************************************************************** */
/*                                                                         */
/*                        Eingabe der Benutzerkennung                      */
/*                                                                         */
/*                        Wilhelm Roth 27.04.1998                          */
/*                                                                         */
/* *********************************************************************** */


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include <math.h>
#include "strfkt.h"
#include "mo_menu.h"
#include "mo_curso.h"
#include "wmask.h"

LONG FAR PASCAL PassProc(HWND,UINT,WPARAM,LPARAM);

static char pers_nam [9];
static char pers [13];

static int DestroyOk = 0;

static int lastfield (void); 

static HWND PassWindow;

int FHS = 0;

static mfont fitfont = {"Courier New", 150, 0, 1,
                                       RGB (0, 255, 255),
                                       BLUECOL,
                                       0};

static mfont fhsfont = {"Courier New", 150, 0, 1,
                                       BLACKCOL,
                                       LTGRAYCOL,
                                       0};

static mfont passfont;

static field _benutzer [] = {
"Benutzer      ",  14, 0, 1, 2, 0,  "", DISPLAYONLY, 0, 0, 0,
"Passwort      ",  14, 0, 3, 2, 0,  "", DISPLAYONLY, 0, 0, 0,
pers_nam,          12, 0, 1, 17, 0, "", NORMAL, 0, testkeys, 0,
pers,              16, 0, 3, 17, 0, "", PASSWORD, 0, lastfield, 0,
" OK ",             4, 2, 7,10,  0, "", BUTTON, 0, 0, KEY12,
" Abbrechen ",     11, 2, 7,17,  0, "", BUTTON, 0, 0, KEY5};

static form benutzer = {6, 0, 0, _benutzer, 0,0,0,0, &passfont};

static class MENUE_CLASS menue_class;

char *GetPers (char *benutzer)
/**
Benutzername holen.
**/
{
            if (benutzer == NULL)
            {
                      return (pers);
            }
            strcpy (benutzer, pers);
            return (benutzer);
}

static int test_benutzer (void)
/**
Eingegebenen Benutzer pruefen.
**/
{
              
            menue_class.Lesesys_ben (pers_nam, pers);
            if (sqlstatus == 0)
            {
                          return TRUE;
            }
            return FALSE;
}

static int dokey5 ()
/**
Aktion bei Taste F5
**/
{
            break_enter ();
            return TRUE;
}

int lastfield ()
/**
Letztes Eingabefeld bearbeiten.
**/
{
           if (testkeys ()) return 0;

           GetWindowText (benutzer.mask [2].feldid,
                          benutzer.mask [2].feld,
                          benutzer.mask [2].length);
           GetWindowText (benutzer.mask [3].feldid,
                          benutzer.mask [3].feld,
                          benutzer.mask [3].length);
           if (test_benutzer () == FALSE)
           {
                        disp_mess ("Benutzer oder Passwort falsch", 0);
                        SetCurrentField (0);
                        return 0;
           }
           syskey = KEY12;
           break_enter ();
           return 1;
}


static HWND OpenPassWindow (int rows, int columns, HANDLE hInst, HWND hWnd)
/**
Fenster fuer Benutzereingabe oeffnen.
**/
{
           static int registered = 0;
           WNDCLASS wc;
           int x, y, cx, cy;
           int scx, scy;
           HWND Window;
           HWND AktivWindow;
           HDC hdc;
           TEXTMETRIC tm;
           HFONT hFont, oldFont;


           if (registered == 0)
           {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
                   wc.lpfnWndProc   =  PassProc;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hInst;
                   wc.hIcon         =  LoadIcon (hInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   if (FHS)
                   {
                        memcpy (&passfont, &fhsfont, sizeof (mfont));
                        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   }
                   else
                   {
                        memcpy (&passfont, &fitfont, sizeof (mfont));
                        wc.hbrBackground =  CreateSolidBrush (BLUECOL);
                   }
                   wc.lpszMenuName  =  "";
                   wc.lpszClassName =  "Benutzer";

                   RegisterClass(&wc);
                   registered = 1;
           }

           AktivWindow = GetActiveWindow ();
           hdc = GetDC (AktivWindow);
           if (benutzer.font)
           {
                spezfont (benutzer.font);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
           }

           GetTextMetrics (hdc, &tm);
           ReleaseDC (AktivWindow, hdc);
           cy = rows * (tm.tmHeight + tm.tmHeight / 2);
           cx = columns * tm.tmAveCharWidth;
           scx = GetSystemMetrics (SM_CXSCREEN);
           scy = GetSystemMetrics (SM_CYSCREEN);
           x = (scx - cx) / 2;
           y = (scy - cy) / 2;
           SelectObject (hdc, oldFont);

           Window = CreateWindow ("Benutzer",
                                 "Benutzeranmeldung",
                                 WS_DLGFRAME | WS_CAPTION | WS_VISIBLE |
                                 WS_MINIMIZEBOX,
                                 x, y,
                                 cx, cy,
                                 hWnd,
                                 NULL,
                                 hInst,
                                 NULL);

           ShowWindow (Window, SW_SHOW);
           UpdateWindow (Window);
           return (Window);
}


BOOL EnterPassw (HANDLE hInst, HWND hWnd)
/**
Benutzername und Passwort eingeben.
**/
{
            DestroyOk = 0; 
            set_fkt (dokey5, 5);
            pers_nam[0] = (char) 0;
            pers[0] = (char) 0;
            PassWindow = OpenPassWindow (8, 37, hInst, hWnd);
            SetMouseLock (TRUE);
            enter_form (PassWindow, &benutzer, 0, 0);
            SetMouseLock (FALSE);

//            if (! DestroyOk)
            {
                        DestroyWindow (PassWindow);
            }
            if (syskey == KEY5)
            {
                        return FALSE;
            }
            menue.mdn  = sys_ben.mdn;
            menue.fil  = sys_ben.fil;
            strcpy (menue.pers,sys_ben.pers);
            return TRUE;
}

LONG FAR PASCAL PassProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
//        HWND enchild;
        static int SendChild = 0;

        GetCursorPos (&mousepos);

        switch(msg)
        {
/*
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    if (IsModalDlg () == FALSE) break;
                    enchild = IsDlgChild (&mousepos);
                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;

              case WM_ACTIVATE :
                    if (IsModalDlg () == FALSE) break;
                    switch (wParam)
                    {
                         case WA_ACTIVE :
                         case WA_CLICKACTIVE :
                              SetCapture (PassWindow);
                              break;
                    }
*/
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY12)
                    {
                            if (lastfield () == FALSE)
                            {
                                     SetCurrentFocus (2);
                                     return 0;
                            }
                            return 0;
                    }
                    else if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            break_enter ();
                            return 0;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}
