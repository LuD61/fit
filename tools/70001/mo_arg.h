#ifndef _MO_ARG
#define _MO_ARG

int scroll_arg (char **, short, int);
void argtst (int *, char **, void (*) (char *));
#endif