import javax.mail.MessagingException;




public class Start {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		final String htmlContent = args[0];
		final String subject = args[1];
		final String to = args[2];
		final String filename = args[3];
		
		String cc = "";
		
		if ( args.length > 4 ) {
			cc = args[4];	
		}		
		
		final SendJavaMail sendJavaMail = new SendJavaMail();
		
		sendJavaMail.sendEmail(htmlContent, subject, to);
		
		System.exit(0);
	}
}
