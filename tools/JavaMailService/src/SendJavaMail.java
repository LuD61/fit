

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;

import fit.iniReader.INIReader;


public class SendJavaMail {
	
	
	private String host = "setec-systeme.de";
	private String from = "servicemail@setec-systeme.de";
	private String port = "587";
	private String user = "web1178p1";
	private String password = "t0pfit";
	
	public SendJavaMail() {
		
		String iniFilePath = System.getenv("BWS");
		iniFilePath = iniFilePath + "\\etc\\smtpMail.ini";
		
		Properties properties = null;
		try {
			properties = INIReader.load(iniFilePath);
			host = properties.getProperty("host");
			from = properties.getProperty("from");		
			port = properties.getProperty("port");
			user = properties.getProperty("user");
			password = properties.getProperty("password");
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}		
	}
	
	
	
	public void sendEmail(final String htmlContent, final String subject, final String to) {		

		Properties props = new Properties();
		
	    props.put("mail.smtp.host", host);
	    props.put("mail.from", from);
	    props.put("mail.protocol.port", port);
	    props.put("mail.protocol.user", user);	    
	    props.put("mail.smtp.auth", "true"); 

	    SMTPAuth auth = new SMTPAuth();
	    auth.setUsername(user);
	    auth.setPassword(password);
	    Session session = Session.getDefaultInstance(props, auth); 

	    try {    	
	    	
	        MimeMultipart content = new MimeMultipart( "alternative" );
	        MimeBodyPart html = new MimeBodyPart();
	        html.setContent(htmlContent, "text/html");
	        content.addBodyPart( html );	        
	        
	        MimeMessage msg = new MimeMessage(session);
	        msg.setSubject(subject);
	        msg.setFrom();
	        msg.setRecipients(Message.RecipientType.TO, to);
	        msg.setRecipients(Message.RecipientType.CC, "daniel.heil@setec-systeme.de");
	        msg.setContent( content );
	        msg.setHeader( "MIME-Version" , "1.0" );
	        msg.setHeader( "Content-Type" , content.getContentType() );
	        msg.setHeader( "X-Mailer", "Java-Mailer V 1.60217733" );
	        msg.setSentDate( new Date() );
	                 

	        Transport.send(msg);
	        
	    } catch (MessagingException mex) {
	    	
	        System.out.println("send failed, exception: " + mex);
	    }
	    
	}
}