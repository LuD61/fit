#if ! defined( REMOTE_ACCESS_SERVICE_HANDLER )

#include <ras.h>
#include <raserror.h>

#if ! defined( ARRAYSIZE )
#define ARRAYSIZE( argument ) ( sizeof( argument ) / sizeof( *( argument ) ) )
#endif

#define REMOTE_ACCESS_SERVICE_HANDLER

typedef struct DllThunkRAS_s	{
   HINSTANCE dll_instance_handle;
   TCHAR     dll_name[ MAX_PATH ];
   DWORD (APIENTRY * RasDialA)( LPRASDIALEXTENSIONS, LPSTR, LPRASDIALPARAMSA, DWORD, LPVOID, LPHRASCONN );
   DWORD (APIENTRY * RasDialW)( LPRASDIALEXTENSIONS, LPWSTR, LPRASDIALPARAMSW, DWORD, LPVOID, LPHRASCONN );
   DWORD (APIENTRY * RasEnumConnectionsA)( LPRASCONNA, LPDWORD, LPDWORD );
   DWORD (APIENTRY * RasEnumConnectionsW)( LPRASCONNW, LPDWORD, LPDWORD );
   DWORD (APIENTRY * RasEnumEntriesA)( LPSTR, LPSTR, LPRASENTRYNAMEA, LPDWORD, LPDWORD );
   DWORD (APIENTRY * RasEnumEntriesW)( LPWSTR, LPWSTR, LPRASENTRYNAMEW, LPDWORD, LPDWORD );
   DWORD (APIENTRY * RasGetConnectStatusA)( HRASCONN, LPRASCONNSTATUSA );
   DWORD (APIENTRY * RasGetConnectStatusW)( HRASCONN, LPRASCONNSTATUSW );
   DWORD (APIENTRY * RasGetErrorStringA)( UINT, LPSTR, DWORD );
   DWORD (APIENTRY * RasGetErrorStringW)( UINT, LPWSTR, DWORD );
   DWORD (APIENTRY * RasHangUpA)( HRASCONN );
   DWORD (APIENTRY * RasHangUpW)( HRASCONN );
   DWORD (APIENTRY * RasGetProjectionInfoA)( HRASCONN, RASPROJECTION, LPVOID, LPDWORD );
   DWORD (APIENTRY * RasGetProjectionInfoW)( HRASCONN, RASPROJECTION, LPVOID, LPDWORD );
   DWORD (APIENTRY * RasCreatePhonebookEntryA)( HWND, LPSTR );
   DWORD (APIENTRY * RasCreatePhonebookEntryW)( HWND, LPWSTR );
   DWORD (APIENTRY * RasEditPhonebookEntryA)( HWND, LPSTR, LPSTR );
   DWORD (APIENTRY * RasEditPhonebookEntryW)( HWND, LPWSTR, LPWSTR );
   DWORD (APIENTRY * RasSetEntryDialParamsA)( LPSTR, LPRASDIALPARAMSA, BOOL );
   DWORD (APIENTRY * RasSetEntryDialParamsW)( LPWSTR, LPRASDIALPARAMSW, BOOL );
   DWORD (APIENTRY * RasGetEntryDialParamsA)( LPSTR, LPRASDIALPARAMSA, LPBOOL );
   DWORD (APIENTRY * RasGetEntryDialParamsW)( LPWSTR, LPRASDIALPARAMSW, LPBOOL );
   DWORD (APIENTRY * RasEnumDevicesA)( LPRASDEVINFOA, LPDWORD, LPDWORD );
   DWORD (APIENTRY * RasEnumDevicesW)( LPRASDEVINFOW, LPDWORD, LPDWORD );
   DWORD (APIENTRY * RasGetCountryInfoA)( LPRASCTRYINFOA, LPDWORD );
   DWORD (APIENTRY * RasGetCountryInfoW)( LPRASCTRYINFOW, LPDWORD );
   DWORD (APIENTRY * RasGetEntryPropertiesA)( LPSTR, LPSTR, LPRASENTRYA, LPDWORD, LPBYTE, LPDWORD );
   DWORD (APIENTRY * RasGetEntryPropertiesW)( LPWSTR, LPWSTR, LPRASENTRYW, LPDWORD, LPBYTE, LPDWORD );
   DWORD (APIENTRY * RasSetEntryPropertiesA)( LPSTR, LPSTR, LPRASENTRYA, DWORD, LPBYTE, DWORD );
   DWORD (APIENTRY * RasSetEntryPropertiesW)( LPWSTR, LPWSTR, LPRASENTRYW, DWORD, LPBYTE, DWORD );
   DWORD (APIENTRY * RasRenameEntryA)( LPSTR, LPSTR, LPSTR );
   DWORD (APIENTRY * RasRenameEntryW)( LPWSTR, LPWSTR, LPWSTR );
   DWORD (APIENTRY * RasDeleteEntryA)( LPSTR, LPSTR );
   DWORD (APIENTRY * RasDeleteEntryW)( LPWSTR, LPWSTR );
   DWORD (APIENTRY * RasValidateEntryNameA)( LPSTR, LPSTR );
   DWORD (APIENTRY * RasValidateEntryNameW)( LPWSTR, LPWSTR );
   DWORD (APIENTRY * RasGetSubEntryHandleA)( HRASCONN, DWORD, LPHRASCONN );
   DWORD (APIENTRY * RasGetSubEntryHandleW)( HRASCONN, DWORD, LPHRASCONN );
/*
   DWORD (APIENTRY * RasGetCredentialsA)( LPSTR, LPSTR, LPRASCREDENTIALSA);
   DWORD (APIENTRY * RasGetCredentialsW)( LPWSTR, LPWSTR, LPRASCREDENTIALSW );
   DWORD (APIENTRY * RasSetCredentialsA)( LPSTR, LPSTR, LPRASCREDENTIALSA, BOOL );
   DWORD (APIENTRY * RasSetCredentialsW)( LPWSTR, LPWSTR, LPRASCREDENTIALSW, BOOL );
   DWORD (APIENTRY * RasConnectionNotificationA)( HRASCONN, HANDLE, DWORD );
   DWORD (APIENTRY * RasConnectionNotificationW)( HRASCONN, HANDLE, DWORD );
   DWORD (APIENTRY * RasGetSubEntryPropertiesA)( LPSTR, LPSTR, DWORD, LPRASSUBENTRYA, LPDWORD, LPBYTE, LPDWORD );
   DWORD (APIENTRY * RasGetSubEntryPropertiesW)( LPWSTR, LPWSTR, DWORD, LPRASSUBENTRYW, LPDWORD, LPBYTE, LPDWORD );
   DWORD (APIENTRY * RasSetSubEntryPropertiesA)( LPSTR, LPSTR, DWORD, LPRASSUBENTRYA, DWORD, LPBYTE, DWORD );
   DWORD (APIENTRY * RasSetSubEntryPropertiesW)( LPWSTR, LPWSTR, DWORD, LPRASSUBENTRYW, DWORD, LPBYTE, DWORD );
   DWORD (APIENTRY * RasGetAutodialAddressA)( LPSTR, LPDWORD, LPRASAUTODIALENTRYA, LPDWORD, LPDWORD );
   DWORD (APIENTRY * RasGetAutodialAddressW)( LPWSTR, LPDWORD, LPRASAUTODIALENTRYW, LPDWORD, LPDWORD);
   DWORD (APIENTRY * RasSetAutodialAddressA)( LPSTR, DWORD, LPRASAUTODIALENTRYA, DWORD, DWORD );
   DWORD (APIENTRY * RasSetAutodialAddressW)( LPWSTR, DWORD, LPRASAUTODIALENTRYW, DWORD, DWORD );
   DWORD (APIENTRY * RasEnumAutodialAddressesA)( LPSTR *, LPDWORD, LPDWORD );
   DWORD (APIENTRY * RasEnumAutodialAddressesW)( LPWSTR *, LPDWORD, LPDWORD );
   DWORD (APIENTRY * RasGetAutodialEnableA)( DWORD, LPBOOL );
   DWORD (APIENTRY * RasGetAutodialEnableW)( DWORD, LPBOOL );
   DWORD (APIENTRY * RasSetAutodialEnableA)( DWORD, BOOL );
   DWORD (APIENTRY * RasSetAutodialEnableW)( DWORD, BOOL );
   DWORD (APIENTRY * RasGetAutodialParamA)( DWORD, LPVOID, LPDWORD );
   DWORD (APIENTRY * RasGetAutodialParamW)( DWORD, LPVOID, LPDWORD );
   DWORD (APIENTRY * RasSetAutodialParamA)( DWORD, LPVOID, DWORD );
   DWORD (APIENTRY * RasSetAutodialParamW)( DWORD, LPVOID, DWORD );
*/
}DllThunkRAS_struct, *DllThunkRAS_struct_p;

class RemoteAccessServiceHandler : public CObject
{
   public:
      RemoteAccessServiceHandler();
      RemoteAccessServiceHandler( DWORD input_buffer_size );
      RemoteAccessServiceHandler( DWORD input_buffer_size, DWORD output_buffer_size );
      virtual ~RemoteAccessServiceHandler();
      virtual BOOL		Dial( LPCTSTR destination );
      virtual HRASCONN	GetConnection( LPCTSTR name_of_connection );
      virtual BOOL		GetConnections( CStringArray& connections );
      virtual BOOL		GetConnectionStatus( void );
      virtual LONG		GetErrorCode( void ) const;
      virtual BOOL		GetErrorString( CString& error_string );
      virtual void		GetName( CString& name ) const;
      virtual BOOL		GetPhoneBookEntries( CStringArray& phone_book_entries );
      virtual BOOL		HangUp( LPCTSTR name_of_connection = NULL );
      virtual BOOL		IsConnected( void );
      virtual BOOL		SetKill( BOOL do_kill ); // Returns replaces setting
      virtual LPVOID	SetNotifyHandler( LPVOID RasDialFunc1_function_pointer );
      virtual HWND		SetNotifyHandlerWindow( HWND window_handle );
      virtual DWORD		SetDialOptions( DWORD dial_options );
      virtual BOOL		Connect( LPCTSTR who_to_call );
      virtual BOOL		Disconnect( LPCTSTR name_of_connection = NULL ); // returns when RAS really hangs up
      static BOOL		EnableLogging( BOOL enable_logging, LPCTSTR machine_name = NULL ); // Creates DEVICE.LOG
      static BOOL		GetKeepConnectionsAfterLogoff( LPCTSTR machine_name = NULL );
      static BOOL		SetKeepConnectionsAfterLogoff( BOOL keep_connections = TRUE, LPCTSTR machine_name = NULL );
	  static VOID PASCAL RemoteAccessServiceHandler::RasMon( HRASCONN connection_handle, 
			 UINT message, RASCONNSTATE connection_state,DWORD error_code, DWORD extended_error_code );
      enum Protocols      {
         protocolAuthenticationMessageBlock = RASP_Amb,
         protocolNetBEUIFramer              = RASP_PppNbf,
         protocolInternetworkPacketExchange = RASP_PppIpx,
         protocolInternetProtocol           = RASP_PppIp
      };
      enum DialOptions      {
         dialUsePrefixAndSuffix        = RDEOPT_UsePrefixSuffix,
         dialAcceptPausedStates        = RDEOPT_PausedStates,
         dialIgnoreModemSpeaker        = RDEOPT_IgnoreModemSpeaker,
         dialSetModemSpeaker           = RDEOPT_SetModemSpeaker,
         dialIgnoreSoftwareCompression = RDEOPT_IgnoreSoftwareCompression,
         dialSetSoftwareCompression    = RDEOPT_SetSoftwareCompression
      };
   protected:
      HRASCONN m_ConnectionHandle;
      RASCONNSTATUS m_ConnectionStatus;
      LPVOID m_ConnectionCallbackFunctionPointer;
      RASDIALEXTENSIONS m_DialExtensions;
      CString m_Name;
      DllThunkRAS_struct_p m_Thunk_p;
      BOOL m_endKill;
      LONG m_ErrorCode;
      DWORD m_NotifierType;
   private:
      void m_Initialize( void );
      void m_InitializeThunk( void );
      void BuildConnectionList( LPRASCONN& return_value, DWORD& number_of_entries );
      RemoteAccessServiceHandler( const RemoteAccessServiceHandler& ) {};
      RemoteAccessServiceHandler& operator=( const RemoteAccessServiceHandler& ) { return( *this ); };

};

#endif // REMOTE_ACCESS_SERVICE_HANDLER
