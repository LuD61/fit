#include <afx.h>
#include "rashand.h"
#pragma hdrstop

#define DLL_LOAD_THUNK( dll_instance_handle, structure_pointer, function_name ) \
{\
   *( (FARPROC *) &structure_pointer->function_name ) = GetProcAddress( dll_instance_handle, #function_name );\
   if ( structure_pointer->function_name == NULL ) \
 {\
   TRACE( TEXT( "Thunk %s nicht implementiert \n" ), CString( #function_name ) ); \
   *( (FARPROC *) &structure_pointer->function_name ) = (FARPROC) ThunkNotImplemented;\
 }\
}

#if defined( _DEBUG )
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif // _DEBUG

#if defined( _DEBUG )

static LPCTSTR GetRASStateString( DWORD ras_state )
{
	switch( ras_state ) {
      case RASCS_OpenPort:
         return( TEXT( "RASCS_OpenPort - RAS Port wird ge�ffnet !" ) );
      case RASCS_PortOpened:
         return( TEXT( "RASCS_PortOpened - RAS Port erfolgreich ge�ffnet !" ) );
      case RASCS_ConnectDevice:
         return( TEXT( "RASCS_ConnectDevice - Verbindung wird aufgebaut !" ) );
      case RASCS_DeviceConnected:
         return( TEXT( "RASCS_DeviceConnected - Ger�t erfolgreich verbunden !" ) );
      case RASCS_AllDevicesConnected:
         return( TEXT( "RASCS_AllDevicesConnected - Alle Ger�te wurden erfolgreich verbunden !" ) );
      case RASCS_Authenticate:
         return( TEXT( "RASCS_Authenticate - Netzwerk-Berechtigungen werden �berpr�ft !" ) );
      case RASCS_AuthNotify:
         return( TEXT( "RASCS_AuthNotify - Ameldung fehlgeschlagen !" ) );
      case RASCS_AuthRetry:
         return( TEXT( "RASCS_AuthRetry - Netzwerk-Anmeldung wird wiederholt !" ) );
      case RASCS_AuthCallback:
         return( TEXT( "RASCS_AuthCallback - R�ckrufnummer wird angefordert !" ) );
      case RASCS_AuthChangePassword:
         return( TEXT( "RASCS_AuthChangePassword - Das Passwort mu� ge�ndert werden" ) );
      case RASCS_AuthProject:
         return( TEXT( "RASCS_AuthProject - Die Projektionsphase wird gestartet !" ) );
      case RASCS_AuthLinkSpeed:
         return( TEXT( "RASCS_AuthLinkSpeed - Verbindungsparameter werden ermittelt !" ) );
      case RASCS_AuthAck:
         return( TEXT( "RASCS_AuthAck - Authentisierung erfolgreich !" ) );
      case RASCS_ReAuthenticate:
         return( TEXT( "RASCS_ReAuthenticate - R�ckrufparameter werden gepr�ft !" ) );
      case RASCS_Authenticated:
         return( TEXT( "RASCS_Authenticated - Authentisierung erfolgreich !" ) );
      case RASCS_PrepareForCallback:
         return( TEXT( "RASCS_PrepareForCallback - R�ckruf wird eingeleitet !" ) );
      case RASCS_WaitForModemReset:
         return( TEXT( "RASCS_WaitForModemReset - Modem wird f�r R�ckruf vorbereitet !" ) );
      case RASCS_WaitForCallback:
         return( TEXT( "RASCS_WaitForCallback - RAS wartet auf R�ckruf !" ) );
      case RASCS_Projected:
         return( TEXT( "RASCS_Projected -  RasGetProjectionInfo wird ermittelt !" ) );
      case RASCS_StartAuthentication:
         return( TEXT( "RASCS_StartAuthentication - Authentisiering wird ermittelt (Windows 95) !" ) );
      case RASCS_CallbackComplete:
         return( TEXT( "RASCS_CallbackComplete - Authentisiering wird ermittelt (Windows 95) !" ) );
      case RASCS_LogonNetwork:
         return( TEXT( "RASCS_LogonNetwork - Rechner wird mit dem Netzwerk verbunden (Windows 95) !" ) );
      case RASCS_SubEntryConnected:
         return( TEXT( "RASCS_SubEntryConnected - Sub-Eintrag erfolgreich verbunden !" ) );
      case RASCS_SubEntryDisconnected:
         return( TEXT( "RASCS_SubEntryDisconnected - Sub-Eintrag getrennt !" ) );
      case RASCS_Interactive:
         return( TEXT( "RASCS_Interactive - Das RAS-Terminalfenster ist aktiv !" ) );
      case RASCS_RetryAuthentication:
         return( TEXT( "RASCS_RetryAuthentication - Authentifierung wird versucht !" ) );
      case RASCS_CallbackSetByCaller:
         return( TEXT( "RASCS_CallbackSetByCaller - Benutzerdefinierter R�ckruf wird erwartet !" ) );
      case RASCS_PasswordExpired:
         return( TEXT( "RASCS_PasswordExpired - Das Passwort ist abgelaufen !" ) );
      case RASCS_Connected:
         return( TEXT( "RASCS_Connected - Verbindungsaufbau erfolgreich !" ) );
      case RASCS_Disconnected:
         return( TEXT( "RASCS_Disconnected - Verbindungsaufbau nicht erfolgreich !" ) );
      default:
         return( TEXT( "Unbekannter Zustand !" ) );
   }
}
static LPCTSTR GetRASExtendedError( DWORD ext_error )
{
	switch( ext_error ) {
      case ERROR_SERVER_NOT_RESPONDING:
         return ( TEXT( "Extended Error: Server antwortet nicht !" ) );
      case ERROR_NETBIOS_ERROR:
         return ( TEXT( "Extended Error: NetBIOS Fehler!" ) );
      case ERROR_AUTH_INTERNAL:
         return ( TEXT( "Extended Error: Interner Diagnose Code !" ) );
      case ERROR_CANNOT_GET_LANA:
         return ( TEXT( "Extended Error: Routing Fehler !" ) );
	}
	return ( TEXT( "Unbekannter Fehlercode !" ) );
}
#endif // _DEBUG

RemoteAccessServiceHandler::RemoteAccessServiceHandler()
{
   m_Thunk_p = NULL;
   m_Initialize();
   m_InitializeThunk();
   m_endKill = FALSE;
}

RemoteAccessServiceHandler::~RemoteAccessServiceHandler()
{
   if ( m_endKill != FALSE )
      Disconnect();
   m_Initialize();
   if ( m_Thunk_p != NULL )   {
      if ( m_Thunk_p->dll_instance_handle != NULL )    
         ::FreeLibrary( m_Thunk_p->dll_instance_handle );
      ZeroMemory( m_Thunk_p, sizeof( DllThunkRAS_struct ) );
      delete m_Thunk_p;
      m_Thunk_p = NULL;
   }
}

BOOL RemoteAccessServiceHandler::Disconnect( LPCTSTR name_of_connection )
{

   CString connection_name;

#if defined( _DEBUG )
   DWORD number_of_tries = 0;
#endif
   try   {
      if ( !name_of_connection )       {
         connection_name = m_Name;
         m_ErrorCode = m_Thunk_p->RasHangUp( m_ConnectionHandle );
         if ( !m_ErrorCode )          {
            while( m_Thunk_p->RasGetConnectStatus( m_ConnectionHandle, &m_ConnectionStatus ) != ERROR_INVALID_HANDLE )  {
#if defined( _DEBUG )
               number_of_tries++;
#endif
               ::Sleep( 250 );
            }
            TRACE( TEXT( "Verbindung getrennt nach  %d Versuchen\n" ), number_of_tries );
            m_ConnectionHandle = NULL;
         }
      }
      else      {
         HRASCONN temp_handle = GetConnection( name_of_connection );
         connection_name = name_of_connection;
         m_ErrorCode = m_Thunk_p->RasHangUp( temp_handle );
         if ( !m_ErrorCode  )  {
            TRACE( TEXT( "Warte bei %s auf Abbruch der Verbindung\n" ), CString( name_of_connection ) );
            while( m_Thunk_p->RasGetConnectStatus( temp_handle, &m_ConnectionStatus ) != ERROR_INVALID_HANDLE )  {
#if defined( _DEBUG )
               number_of_tries++;
#endif
               ::Sleep( 250 );
            }
            TRACE( TEXT( "Verbindung getrennt nach  %d Versuchen\n" ), number_of_tries );
         }
      }
      HRASCONN temp_handle = NULL;
      do        {
         temp_handle = GetConnection( connection_name );
         if ( temp_handle != NULL )         {
            TRACE( TEXT( "Verbindung besteht noch...\n" ) );
            m_Thunk_p->RasHangUp( temp_handle );
            ::Sleep( 1000 );
         }
      }
      while( temp_handle != NULL );

      if ( m_ErrorCode == 0 )      
         return( TRUE );
      else
         return( FALSE );
   }
   catch( ... )   {
      m_ErrorCode = ERROR_EXCEPTION_IN_SERVICE;
      return( FALSE );
   }
}

BOOL RemoteAccessServiceHandler::Dial( LPCTSTR destination )
{
   if ( destination == NULL )   {
      m_ErrorCode = ERROR_INVALID_PARAMETER;
      return( FALSE );
   }
   HangUp();

#if ( WINVER >= 0x401 )
   RASCREDENTIALS credentials;
   ::ZeroMemory( &credentials, sizeof( credentials ) );
   credentials.dwSize = sizeof( credentials );
   credentials.dwMask = RASCM_UserName | RASCM_Password | RASCM_Domain;
   m_Thunk_p->RasGetCredentials( NULL, (TCHAR *) destination, &credentials );
#endif 

   RASDIALPARAMS dialing_parameters;
   ::ZeroMemory( &dialing_parameters, sizeof( dialing_parameters ) );
   dialing_parameters.dwSize = sizeof( dialing_parameters );
   try   {
      _tcscpy( dialing_parameters.szEntryName, destination );
#if ( WINVER >= 0x401 )
      _tcscpy( dialing_parameters.szUserName, credentials.szUserName );
      _tcscpy( dialing_parameters.szPassword, credentials.szPassword );
      _tcscpy( dialing_parameters.szDomain,   credentials.szDomain   );
#endif 
      m_ConnectionHandle = NULL;
      m_ErrorCode = m_Thunk_p->RasDial( &m_DialExtensions,NULL,&dialing_parameters,
		  m_NotifierType,m_ConnectionCallbackFunctionPointer,&m_ConnectionHandle );
      if ( m_ErrorCode == 0 )      {
         m_Name = destination;
         return( TRUE );
      }
      else      {
         m_Name.Empty();
         return( FALSE );
      }
   }
   catch( ... )   {
      m_ErrorCode = ERROR_EXCEPTION_IN_SERVICE;
      return( FALSE );
   }
}

HRASCONN RemoteAccessServiceHandler::GetConnection( LPCTSTR name_of_connection )
{
   if ( name_of_connection == NULL )   {
      m_ErrorCode = ERROR_INVALID_PARAMETER;
      return( NULL );
   }
   try   {
      LPRASCONN connections       = NULL;
      DWORD     number_of_entries = 0;
      BuildConnectionList( connections, number_of_entries );
      HRASCONN return_value = NULL;
      if ( m_ErrorCode == 0 )      {
         DWORD index = 0;
         while( index < number_of_entries )         {
            if ( _tcsicmp( name_of_connection, connections[ index ].szEntryName ) == 0 ) {
               return_value = connections[ index ].hrasconn;
               index = number_of_entries;
            }
            index++;
         }
      }
      delete [] connections;
      return( return_value );
   }
   catch( ... )
   {
      m_ErrorCode = ERROR_EXCEPTION_IN_SERVICE;
      return( FALSE );
   }
}

BOOL RemoteAccessServiceHandler::GetConnections( CStringArray& connection_names )
{
   LPRASCONN connections       = NULL;
   DWORD     number_of_entries = 0;
   connection_names.RemoveAll();
   BuildConnectionList( connections, number_of_entries );
   BOOL return_value = FALSE;
   if ( m_ErrorCode == 0 )   {
      return_value = TRUE;
      DWORD index = 0;
      while( index < number_of_entries )      {
         connection_names.Add( connections[ index ].szEntryName );
         index++;
      }
   }
   delete [] connections;
   return( return_value );
}

BOOL RemoteAccessServiceHandler::GetConnectionStatus( void )
{
   m_ErrorCode = m_Thunk_p->RasGetConnectStatus( m_ConnectionHandle, &m_ConnectionStatus );
   if ( !m_ErrorCode )
      return( TRUE );
   else
      return( FALSE );
}

LONG RemoteAccessServiceHandler::GetErrorCode( void ) const
{
   return( m_ErrorCode );
}

BOOL RemoteAccessServiceHandler::GetErrorString( CString& return_string )
{
   TCHAR string[ 4096 ];
   ::ZeroMemory( string, sizeof( string ) );
   if ( ! m_Thunk_p->RasGetErrorString( m_ErrorCode, string, ARRAYSIZE( string ) ) ) {
      return_string = string;
      return( TRUE );
   }
   else {
      return_string.Empty();
      return( FALSE );
   }
}
void RemoteAccessServiceHandler::GetName( CString& name ) const
{
   name = m_Name;
}

BOOL RemoteAccessServiceHandler::GetPhoneBookEntries( CStringArray& phone_book_entries )
{
   DWORD size_of_buffer         = 64 * sizeof( RASENTRYNAME );
   DWORD number_of_bytes_needed = size_of_buffer;
   DWORD number_of_entries      = 0;
   LPRASENTRYNAME memory_buffer = (LPRASENTRYNAME) new BYTE[ size_of_buffer ];
   phone_book_entries.RemoveAll();
   if ( !memory_buffer ) {
      m_ErrorCode = ERROR_NOT_ENOUGH_MEMORY;
      return( FALSE );
   }
   ::ZeroMemory( memory_buffer, size_of_buffer );
   memory_buffer->dwSize = sizeof( RASENTRYNAME );
   m_ErrorCode = m_Thunk_p->RasEnumEntries( NULL, NULL, memory_buffer, &number_of_bytes_needed, &number_of_entries );
   if ( m_ErrorCode  ) {
	   if ( m_ErrorCode == ERROR_BUFFER_TOO_SMALL || m_ErrorCode == ERROR_NOT_ENOUGH_MEMORY ) {
         delete [] memory_buffer;
         memory_buffer = (LPRASENTRYNAME) new BYTE[ number_of_bytes_needed ];
         if ( memory_buffer == NULL ) {
            m_ErrorCode = ERROR_NOT_ENOUGH_MEMORY;
            return( FALSE );
         }
         ::ZeroMemory( memory_buffer, number_of_bytes_needed );
         memory_buffer->dwSize = sizeof( RASENTRYNAME );
         m_ErrorCode = m_Thunk_p->RasEnumEntries( NULL, NULL, memory_buffer, &number_of_bytes_needed, &number_of_entries );
      }
   }
   BOOL return_value = FALSE;
   if ( m_ErrorCode ) {
      DWORD index = 0;
	  return_value = TRUE;
      while( index < number_of_entries ) {
         phone_book_entries.Add( memory_buffer[ index ].szEntryName );
         index++;
      }
   }
   delete [] memory_buffer;
   return( return_value );
}

BOOL RemoteAccessServiceHandler::HangUp( LPCTSTR name_of_connection )
{
   try   {
      if ( !name_of_connection )
         m_ErrorCode = m_Thunk_p->RasHangUp( m_ConnectionHandle );
      else      {
         HRASCONN temp_handle = GetConnection( name_of_connection );
         m_ErrorCode = m_Thunk_p->RasHangUp( temp_handle );
      }
      if ( !m_ErrorCode )
         return( TRUE );
      else
         return( FALSE );
   }
   catch( ... )    {
      m_ErrorCode = ERROR_EXCEPTION_IN_SERVICE;
      return( FALSE );
   }
}

BOOL RemoteAccessServiceHandler::IsConnected( void )
{
   GetConnectionStatus();
   if ( m_ConnectionStatus.rasconnstate == RASCS_Connected )
      return( TRUE );
   else   {
      TRACE( TEXT( "Zustand %s\n" ), CString(GetRASStateString( m_ConnectionStatus.rasconnstate ) ) );
      return( FALSE );
   }
}
                                                              
void RemoteAccessServiceHandler::BuildConnectionList( LPRASCONN& connections, DWORD& number_of_connections )
{
   DWORD size_of_buffer         = 64 * sizeof( RASCONN );
   DWORD number_of_bytes_needed = size_of_buffer;
   DWORD number_of_entries      = 0;
   LPRASCONN memory_buffer = (LPRASCONN) new BYTE[ size_of_buffer ]; // Get room for 64 entries (to begin with)

   if ( memory_buffer == NULL )   {
      m_ErrorCode = ERROR_NOT_ENOUGH_MEMORY;
      return;
   }
   ::ZeroMemory( memory_buffer, size_of_buffer );
   memory_buffer->dwSize = sizeof( RASCONN );
   m_ErrorCode = m_Thunk_p->RasEnumConnections( memory_buffer, &number_of_bytes_needed, &number_of_entries );
   if ( m_ErrorCode != 0 )   {
      if ( m_ErrorCode == ERROR_BUFFER_TOO_SMALL || m_ErrorCode == ERROR_NOT_ENOUGH_MEMORY )      {
         delete [] memory_buffer;
         memory_buffer = (LPRASCONN) new BYTE[ number_of_bytes_needed ];
         if ( memory_buffer == NULL )         {
            m_ErrorCode = ERROR_NOT_ENOUGH_MEMORY;
            return;
         }
         ::ZeroMemory( memory_buffer, number_of_bytes_needed );
         memory_buffer->dwSize = sizeof( RASCONN );
         m_ErrorCode = m_Thunk_p->RasEnumConnections( memory_buffer, &number_of_bytes_needed, &number_of_entries );
      }
   }
   connections           = memory_buffer;
   number_of_connections = number_of_entries;
}

void RemoteAccessServiceHandler::m_Initialize( void )
{
   ::ZeroMemory( &m_ConnectionStatus, sizeof( m_ConnectionStatus ) );
   m_ConnectionStatus.dwSize = sizeof( m_ConnectionStatus );
   ::ZeroMemory( &m_DialExtensions, sizeof( m_DialExtensions ) );
   m_DialExtensions.dwSize = sizeof( m_DialExtensions );
   m_ErrorCode                         = 0;
   m_ConnectionHandle                  = NULL;
   m_ConnectionCallbackFunctionPointer = NULL;
   m_NotifierType                      = 0;
   m_Name.Empty();
}

static DWORD APIENTRY ThunkNotImplemented( void )
{
   return( ERROR_CALL_NOT_IMPLEMENTED );
}


void RemoteAccessServiceHandler::m_InitializeThunk( void )
{
   if ( m_Thunk_p != NULL )   {
      TRACE( TEXT( "Thunk bereits installiert\n" ) );
      return;
   }
   try   {
      m_Thunk_p = new DllThunkRAS_struct;
   }
   catch( ... )   {
      m_Thunk_p = NULL;
   }
   if ( m_Thunk_p == NULL )   {
      TRACE( TEXT( "Speicherfehler\n" ) );
      return;
   }
   ZeroMemory( m_Thunk_p, sizeof( DllThunkRAS_struct ) );
   _tcsncpy( m_Thunk_p->dll_name, TEXT( "RASAPI32.DLL" ), ARRAYSIZE( m_Thunk_p->dll_name ) );
   m_Thunk_p->dll_instance_handle = ::LoadLibrary( m_Thunk_p->dll_name );
   if ( m_Thunk_p->dll_instance_handle == NULL )   {
      TRACE( TEXT( "%s kann nicht geladen werden\n" ), CString( m_Thunk_p->dll_name ) );
   }
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasDialA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasDialW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasEnumConnectionsA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasEnumConnectionsW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasEnumEntriesA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasEnumEntriesW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetConnectStatusA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetConnectStatusW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetErrorStringA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetErrorStringW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasHangUpA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasHangUpW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetProjectionInfoA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetProjectionInfoW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasCreatePhonebookEntryA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasCreatePhonebookEntryW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasEditPhonebookEntryA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasEditPhonebookEntryW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetEntryDialParamsA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetEntryDialParamsW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetEntryDialParamsA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetEntryDialParamsW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasEnumDevicesA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasEnumDevicesW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetCountryInfoA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetCountryInfoW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetEntryPropertiesA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetEntryPropertiesW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetEntryPropertiesA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetEntryPropertiesW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasRenameEntryA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasRenameEntryW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasDeleteEntryA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasDeleteEntryW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasValidateEntryNameA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasValidateEntryNameW );

#if ( WINVER >= 0x401 )

   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetSubEntryHandleA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetSubEntryHandleW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetCredentialsA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetCredentialsW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetCredentialsA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetCredentialsW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasConnectionNotificationA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasConnectionNotificationW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetSubEntryPropertiesA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetSubEntryPropertiesW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetSubEntryPropertiesA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetSubEntryPropertiesW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetAutodialAddressA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetAutodialAddressW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetAutodialAddressA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetAutodialAddressW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasEnumAutodialAddressesA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasEnumAutodialAddressesW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetAutodialEnableA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetAutodialEnableW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetAutodialEnableA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetAutodialEnableW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetAutodialParamA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasGetAutodialParamW );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetAutodialParamA );
   DLL_LOAD_THUNK( m_Thunk_p->dll_instance_handle, m_Thunk_p, RasSetAutodialParamW );

#endif
}

BOOL RemoteAccessServiceHandler::Connect( LPCTSTR destination )
{
   if ( destination == NULL )   {
      m_ErrorCode = ERROR_INVALID_PARAMETER;
      return( FALSE );
   }
   try   {
#if ( WINVER >= 0x401 )
      RASCREDENTIALS credentials;
      ::ZeroMemory( &credentials, sizeof( credentials ) );
      credentials.dwSize = sizeof( credentials );
      credentials.dwMask = RASCM_UserName | RASCM_Password | RASCM_Domain;
      m_Thunk_p->RasGetCredentials( NULL, (TCHAR *) destination, &credentials );

#endif
      RASDIALPARAMS dialing_parameters;
      ::ZeroMemory( &dialing_parameters, sizeof( dialing_parameters ) );
      dialing_parameters.dwSize = sizeof( dialing_parameters );
      _tcscpy( dialing_parameters.szEntryName, destination );
#if ( WINVER >= 0x401 )
      _tcscpy( dialing_parameters.szUserName, credentials.szUserName );
      _tcscpy( dialing_parameters.szPassword, credentials.szPassword );
      _tcscpy( dialing_parameters.szDomain,   credentials.szDomain   );
#endif 
      m_ConnectionHandle = NULL;
      m_Name.Empty();
      m_ErrorCode = m_Thunk_p->RasDial( &m_DialExtensions,NULL,&dialing_parameters,
		m_NotifierType,m_ConnectionCallbackFunctionPointer,&m_ConnectionHandle );
      if ( !m_ErrorCode )      {
         m_Name = destination;
         ::Sleep( 6000 );
		 DWORD timeout = GetTickCount() + 60 * 1000;
		 while( TRUE ) {
 		   if(timeout < GetTickCount())
			   return FALSE;
           if ( GetConnectionStatus() != FALSE )            {
               if ( m_ConnectionStatus.rasconnstate == RASCS_Connected )       {
                  TRACE( TEXT( "Verbunden !\n" ) );
                  return( TRUE );
               }
               if ( m_ConnectionStatus.rasconnstate == RASCS_Disconnected )    {
                  TRACE( TEXT( "Verbindung getrennt\n" ) );
                  return( FALSE );
               }
               ::Sleep( 250 );
            }
            else  {
               TRACE( TEXT( "Verbindungsfehler: %d\n" ), m_ErrorCode );
               TRACE( TEXT( "Verbindungsstatus: %d\n" ), m_ConnectionStatus.dwError );
               TRACE( TEXT( "Rasconnstate: %s\n" ), CString( GetRASStateString( m_ConnectionStatus.rasconnstate ) ) );
               return( FALSE );
            }
         }
      }
      else
        return( FALSE );
   }
   catch( ... )   {
      m_ErrorCode = ERROR_EXCEPTION_IN_SERVICE;
      return( FALSE );
   }
}

BOOL RemoteAccessServiceHandler::SetKill( BOOL do_kill )
{
   BOOL return_value = m_endKill;
   m_endKill = do_kill;
   return( return_value );
}

LPVOID RemoteAccessServiceHandler::SetNotifyHandler( LPVOID function_pointer )
{
   LPVOID return_value = NULL;
   if ( m_NotifierType == 1 )   
      return_value = m_ConnectionCallbackFunctionPointer;
   m_ConnectionCallbackFunctionPointer = function_pointer;
   m_NotifierType                      = 1;
   return( return_value );
}

HWND RemoteAccessServiceHandler::SetNotifyHandlerWindow( HWND window_handle )
{
   HWND return_value = NULL;
   if ( m_NotifierType == 0xFFFFFFFF )   {
      return_value = (HWND) m_ConnectionCallbackFunctionPointer;
   }
   m_ConnectionCallbackFunctionPointer = (LPVOID) window_handle;
   m_NotifierType = 0xFFFFFFFF;
   return( return_value );
}

DWORD RemoteAccessServiceHandler::SetDialOptions( DWORD dial_options )
{
   DWORD return_value = m_DialExtensions.dwfOptions;
   m_DialExtensions.dwfOptions = dial_options;
   return( return_value );
}

VOID PASCAL RemoteAccessServiceHandler::RasMon( HRASCONN connection_handle, 
       UINT message, RASCONNSTATE connection_state,DWORD error_code, DWORD extended_error_code )
{
    TRACE (GetRASStateString( connection_state ));
    TRACE ( TEXT ("\n") );
	if ( extended_error_code )  {
		TRACE ( GetRASExtendedError( extended_error_code ));
		TRACE ( TEXT ("\n") );
	}

}

