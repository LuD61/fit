
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;


import javax.swing.LookAndFeel;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.plaf.PanelUI;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LayeredBarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.jdbc.JDBCCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.TextAnchor;
import org.jfree.util.SortOrder;


public class BarChart extends ApplicationFrame {

    private static final long serialVersionUID = 1L;
    private String stmt = "";
    private JDBCCategoryDataset jdbcCategoryDataset = null;

    {
        ChartFactory.setChartTheme(new StandardChartTheme("JFree/Shadow",
                true));
    }

    /**
     * Creates a new bar instance.
     *
     * @param title  the frame title.
     */
    public BarChart(final String title, final int px, final int py, final String sql, final Connection connection,
    		final String chartTitle, final String domainAxisTitle, final String rangeAxisTitle) {
        
    	super(title);
        this.stmt = sql;   	
    	this.jdbcCategoryDataset = new JDBCCategoryDataset(connection);    	
        this.executeDataSet();
        
        JFreeChart chart = createChart(this.jdbcCategoryDataset, chartTitle, domainAxisTitle, rangeAxisTitle);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(px, py));
        chartPanel.setFillZoomRectangle(true);
        chartPanel.setMouseWheelEnabled(true);
        chartPanel.setDisplayToolTips(true);
        setContentPane(chartPanel);
    }
    
    
    
    /**
     * Returns a sample dataset.
     *
     * @return The dataset.
     */
    private void executeDataSet() {  	
    	
    	try {
			this.jdbcCategoryDataset.executeQuery(this.stmt);
			System.out.println("Query executed.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    

    /**
     * Creates a sample chart.
     *
     * @param dataset  the dataset.
     *
     * @return The chart.
     */
    private static JFreeChart createChart(CategoryDataset dataset, final String chartTitle, final String domainAxisTitle, final String rangeAxisTitle) {

        // create the chart...
        JFreeChart chart = ChartFactory.createBarChart(
            chartTitle,	       					// chart title
            domainAxisTitle,               		// domain axis label
            rangeAxisTitle,           			// range axis label
            dataset,                  			// data
            PlotOrientation.VERTICAL, 			// orientation
            true,                     			// include legend
            true,                     			// tooltips?
            false                     			// URLs?
        );


        // set the background color for the chart...
        chart.setBackgroundPaint(Color.white);

        // get a reference to the plot for further customisation...
        CategoryPlot plot = (CategoryPlot) chart.getPlot();

        // set the range axis to display integers only...
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        // disable bar outlines...
        LayeredBarRenderer renderer = new LayeredBarRenderer();
        renderer.setDrawBarOutline(false);
        
        StandardCategoryToolTipGenerator generator = new StandardCategoryToolTipGenerator("{1}{2}", NumberFormat.getInstance());        
        renderer.setBaseToolTipGenerator(generator);        
        
        plot.setRowRenderingOrder(SortOrder.DESCENDING); 
        plot.setRenderer(renderer);
        

        // set up gradient paints for series...
        GradientPaint gp0 = new GradientPaint(0.0f, 0.0f, Color.blue,
                0.0f, 0.0f, new Color(0, 0, 64));
        GradientPaint gp1 = new GradientPaint(0.0f, 0.0f, Color.green,
                0.0f, 0.0f, new Color(0, 64, 0));
        renderer.setSeriesPaint(0, gp1);
        renderer.setSeriesPaint(1, gp0);
        
        CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(
                        Math.PI / 6.0));

        return chart;

    }
    
    
    /**
    * The data generator.
    */
    class DataGenerator extends Timer implements ActionListener {
    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;


	/**
    * Constructor.
    *
    * @param interval the interval (in milliseconds)
    */
	    DataGenerator(int interval) {
	    	super(interval, null);
	    	addActionListener(this);	    	
	    }
    

		@Override
		public void actionPerformed(ActionEvent e) {
			executeDataSet();			
		}

    }

}
