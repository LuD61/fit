import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;

import fit.informixconnector.InformixConnector;




public class Chart {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
    	
    	final int x = Integer.valueOf(args[0]);
    	final int y = Integer.valueOf(args[1]);
    	final boolean focus = Boolean.valueOf(args[2]);
    	final int px = Integer.valueOf(args[3]);
    	final int py = Integer.valueOf(args[4]);
    	final String sql = args[5];
    	final int refresh = Integer.valueOf(args[6]);
    	final boolean alwaysOnTop = Boolean.valueOf(args[7]);
    	final String chartTitle = args[8];
    	final String domainAxisTitle = args[9];
    	final String rangeAxisTitle = args[10];
    	
    	final InformixConnector informixConnector = new InformixConnector();
    	final Connection connection = informixConnector.createConnection();    	
    	
        final BarChart barChart = new BarChart(chartTitle, px, py, sql, connection, chartTitle, domainAxisTitle, rangeAxisTitle);
        barChart.pack();
        barChart.setFocusable(focus);
        barChart.setFocusableWindowState(focus);
        barChart.setLocation(x, y);
        barChart.setPreferredSize(new Dimension(px, py));
        barChart.new DataGenerator(refresh).start();        
        barChart.setAlwaysOnTop(alwaysOnTop);
        barChart.setVisible(true);
        
        barChart.addWindowListener(new WindowAdapter() {
        	public void windowClosing(WindowEvent e) {
        		informixConnector.closeConnection();       		
        	}
		});        
        
	}
}