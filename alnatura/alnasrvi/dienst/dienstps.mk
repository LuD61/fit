
dienstps.dll: dlldata.obj dienst_p.obj dienst_i.obj
	link /dll /out:dienstps.dll /def:dienstps.def /entry:DllMain dlldata.obj dienst_p.obj dienst_i.obj kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib 

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL $<

clean:
	@del dienstps.dll
	@del dienstps.lib
	@del dienstps.exp
	@del dlldata.obj
	@del dienst_p.obj
	@del dienst_i.obj
