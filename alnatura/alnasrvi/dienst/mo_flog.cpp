#include "stdafx.h"
#include <stdio.h>
#include <process.h>
//#include <windows.h>
#include <time.h>

#define MAXLINES 1000 

static char FileName[] = "c:\\bizerba\\tmp\\server.log" ; 
static jrhstart = 70;
static jrh1 = 1900;
static jrh2 = 2000;

void dat_zeit_log(char * datzeit)
{

 time_t timer;
 struct tm *ltime;

   time (&timer);
 // timer += ioffset * 86400L ;
 
   ltime = localtime (&timer);
   if (ltime->tm_year < 100)
   {
             if (ltime->tm_year < jrhstart)
             {
                  ltime->tm_year += jrh2;
             }
             else
             {
                  ltime->tm_year += jrh1;
             }
	}
	else
	{
             ltime->tm_year += jrh1;
     
	} 

	ltime->tm_mon += 1 ;
 
	sprintf( datzeit, "%02d.%02d.%04d %02d:%02d:%02d",
		ltime->tm_mday, ltime->tm_mon, ltime->tm_year, 
		ltime->tm_hour, ltime->tm_min, ltime->tm_sec);
}

void WriteFirstLine (char *FileText)
/**
Satz in erste Zeile einer Datei schreiben.
**/
{
	   FILE *in;
       FILE *out; 
	   int pid;
	   int z;
	   char fname [512];
	   char buffer [5002];

	   pid = getpid ();
       sprintf (fname, "touchwrite.%d", pid);
	   CopyFile (FileName, fname, FALSE);
       out = fopen (FileName, "w");
	   if (out == NULL) return;
	   fprintf (out, "%s\n", FileText);
	   in = fopen (fname, "r");
	   if (in == NULL) 
	   {
		   fclose (out);
		   return;
	   }
	   z = 0;
	   while (fgets (buffer, 5000, in))
	   {
		   fputs (buffer, out);
		   if (z >= MAXLINES) break;
		   z ++;
	   }
	   fclose (in);
	   fclose (out);
	   unlink (fname);
}

void WriteLog (char *Text)
/**
Text mit Datum, Uhrzeit, Fehlerstatus und Geraetenummer schreiben
**/
{
          char datzeit [32];
	      char FileText [512];

          dat_zeit_log ( datzeit );

          sprintf (FileText, "%s %s", datzeit, Text);
          WriteFirstLine (FileText);
}
       
