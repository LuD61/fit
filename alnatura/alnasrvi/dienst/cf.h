/*******************************************************************************
*   C/FRONT Database C-API Library Version W1 3.60 / 5.01
*
*       (c) 1987-2002 Navision a/s. + V5.01 GrJ 2009
*******************************************************************************/
#ifndef _DBL_TYPE_H__
#define _DBL_TYPE_H__

// 111109 : so war das bisher : #define DBL_VERSION                  32




#   ifdef DBL_VERSION_DEFENED_IN_PC_CL_ENV_VAR
// To build release binaries by VS you have to set environment variable CL=/DDBL_VERSION_DEFENED_IN_PC_CL_ENV_VAR#32.
#     define DBL_VERSION  DBL_VERSION_DEFENED_IN_PC_CL_ENV_VAR 
#   else
#   define DBL_VERSION                  ( ((unsigned char)(5)<<24) | ((unsigned char)(0)<<16) | ((unsigned char)(00)<<8) )

#endif



#ifdef _MSC_VER
#define DBL_CDECL                    __cdecl
#else
#define DBL_CDECL                    _cdecl
#endif

#define DBL_EXPORT(type_)            type_ DBL_CDECL
#define DBL_EXPORTVA(type_)          type_ DBL_CDECL

#define DBL_MaxRecSize               4000
#define DBL_MaxCompanyNameLen        30
#define DBL_MaxTableNameLen          30
#define DBL_MaxFieldNameLen          30
#define DBL_MaxUserIDLen             20		// 111109 10->20
#define DBL_MaxNoOfKeys              40
#define DBL_MaxFieldsPerKey          20
#define DBL_MaxSumFieldsPerKey       20
#define DBL_MaxPassWordLen           20        	// 111109 10->20

#define DBL_MaxRecordIDSize          224		// 111109 neu dazu


#define Module_DB                    19
#define DEFINE_DBERR(xErrNo)         ((((DBL_S32)(Module_DB) * 0x10000L + (DBL_S32)(xErrNo)) | 0x1000000L) | 0x4000000L)

#define DBL_Err_TableNotFound        DEFINE_DBERR(1008)
#define DBL_Err_RecordNotFound       DEFINE_DBERR(1008)
#define DBL_Err_RecordExists         DEFINE_DBERR(1009)
#define DBL_Err_KeyNotFound          DEFINE_DBERR(1007)

typedef unsigned char                DBL_U8;
typedef unsigned short int           DBL_U16;
typedef signed   short int           DBL_S16;
typedef unsigned long int            DBL_U32;
typedef signed long int              DBL_S32;
typedef signed long int              DBL_O32;
typedef double                       DBL_DOUBLE;

typedef DBL_U32                      DBL_BOOL;
typedef DBL_U32                      DBL_DATE;
typedef DBL_U32                      DBL_TIME;

typedef struct
{
  DBL_U8  Exp;
  DBL_U8  Mant[9];
  DBL_U8  Slack[2];
} DBL_BCD;

typedef struct
{
  DBL_U32 LowPart;
  DBL_S32 HighPart;
} DBL_S64;

typedef DBL_S64                     DBL_Duration;
typedef DBL_S64                     DBL_Datetime;

#define DateCalc_Size 32	// 111109 neu

typedef struct
{
  DBL_U32 Data1;
  DBL_U16 Data2;
  DBL_U16 Data3;
  DBL_U8  Data4[8];
} DBL_GUID;


// A 111109 :neu dazu

typedef struct
{
  DBL_S32 TableNo;
  DBL_U8  KeyData[DBL_MaxRecordIDSize-sizeof(DBL_S32)];
} DBL_RECORDID;


#define DBL_TABLEFILTER_Size 252

typedef struct
{
  DBL_U8 m_Expression[4];
} DBL_FIELDFILTER2;

typedef struct
{
  DBL_S32			m_FieldNo;
  DBL_U16			m_FieldType;
  DBL_FIELDFILTER2	m_FieldFilter2;
} DBL_FIELDFILTER;

typedef struct
{
  DBL_S32			m_TableNo;
  DBL_S32			m_Size;
  DBL_FIELDFILTER	m_FirstFieldFilter;
  DBL_U8			m_Fill[DBL_TABLEFILTER_Size - sizeof(DBL_FIELDFILTER) - sizeof(DBL_S32) - sizeof(DBL_S32)];
} DBL_TABLEFILTER;

typedef enum
{
	DBL_Undefined,
	DBL_ODBCDriver,
	DBL_DemandPlanner,
	DBL_BusinessAnalytics
} DBL_Consumer;



// E 111109 :neu dazu


#define DBL_BCD_Up               1L
#define DBL_BCD_Near             0L
#define DBL_BCD_Down            -1L

#define DBL_Make_Bcd_0           0
#define DBL_Make_Bcd_1           1
#define DBL_Make_Bcd_2           2
#define DBL_Make_Bcd_10          3
#define DBL_Make_Bcd_100         4
#define DBL_Make_Bcd_1024        5
#define DBL_Make_Bcd_MIN         6
#define DBL_Make_Bcd_MAX         7

typedef DBL_S32              DBL_CREATE_TABLE;
typedef DBL_CREATE_TABLE    *DBL_HCREATE_TABLE;
typedef struct DBL_REC      *DBL_HREC;
typedef struct DBL_TABLE    *DBL_HTABLE;

typedef void    (DBL_CDECL *DBL_pFuncExceptionHandler)(DBL_S32 ErrorCode, DBL_BOOL IsFatal);
typedef void    (DBL_CDECL *DBL_pFuncMessageShowHandler)(DBL_U8* Message, DBL_U32 MessageType, DBL_S32 ErrorCode);

#define DBL_Class_Normal            0
#define DBL_Class_FlowField         1
#define DBL_Class_FlowFilter        2

#define DBL_LockWait                0
#define DBL_LockNoWait              1

// A 111109 :neu dazu
#define DBL_ImportThrowError        0
#define DBL_ImportOverwrite         1
#define DBL_ImportSkip              2

#define DBL_TableDescToken           ((U8*)"TA")
#define DBL_FormToken                ((U8*)"FO")
#define DBL_ReportToken              ((U8*)"RE")
#define DBL_DataportToken            ((U8*)"DA")
#define DBL_CodeunitToken            ((U8*)"CO")
#define DBL_XMLPortToken             ((U8*)"XP")
#define DBL_MenuSuiteToken           ((U8*)"MS")
#define DBL_SeparatorToken           ((U8*)",")

#ifdef SUBLANG_SYS_DEFAULT
#define DBL_LANGUNDEFINED           ((((WORD)(SUBLANG_SYS_DEFAULT)) << 10) | (WORD)(LANG_NEUTRAL))
#else
#define DBL_LANGUNDEFINED           0x0800
#endif


// E 111109 :neu dazu

#define C_DBL_DirTableNo            2000000001
#define   F_DBL_DR_ObjType          1
#define     C_DBL_OT_TableData      0
#define     C_DBL_OT_TableDesc      1
#define     C_DBL_OT_Form           2
#define     C_DBL_OT_Report         3
#define     C_DBL_OT_Dataport       4
#define     C_DBL_OT_Codeunit       5
#define   F_DBL_DR_CompanyName      2
#define   F_DBL_DR_ObjNo            3
#define   F_DBL_DR_ObjName          4
#define   F_DBL_DR_Modified         5
#define   F_DBL_DR_Compiled         6
#define   F_DBL_DR_Blob             7
#define   F_DBL_DR_BlobSize         8
#define   F_DBL_DR_DBMTableNo       9
#define   F_DBL_DR_StampDate        10
#define   F_DBL_DR_StampTime        11
#define   F_DBL_DR_VersionList      12
#define   F_DBL_DR_ObjCaption       20		// 111109 : neu

#define C_DBL_UserTableNo           2000000002
#define   F_DBL_UR_UserID           1
#define   F_DBL_UR_Password         2
#define   F_DBL_UR_Name             3
#define   F_DBL_UR_ValidUntil       4

#define C_DBL_MemberOfTableNo       2000000003
#define   F_DBL_MR_UserID           1
#define   F_DBL_MR_UserName         2
#define   F_DBL_MR_GroupID          3
#define   F_DBL_MR_GroupName        4
#define   F_DBL_MR_CompanyName      5

#define C_DBL_UserGroupTableNo      2000000004
#define F_DBL_UG_GroupID            1
#define F_DBL_UG_Name               2

#define C_DBL_PermissionTableNo     2000000005
#define   F_DBL_PR_GroupID          1
#define   F_DBL_PR_GroupName        2
#define   F_DBL_PR_ObjType          3
#define     C_DBL_OT_TableData      0
#define     C_DBL_OT_TableDesc      1
#define     C_DBL_OT_Form           2
#define     C_DBL_OT_Report         3
#define     C_DBL_OT_Dataport       4
#define     C_DBL_OT_Codeunit       5
#define     C_DBL_OT_System         10
#define   F_DBL_PR_ObjNo            4
#define   F_DBL_PR_ObjName          5
#define   F_DBL_PR_Read             6
#define   F_DBL_PR_Insert           7
#define   F_DBL_PR_Modify           8
#define   F_DBL_PR_Delete           9
#define   F_DBL_PR_Execute          10

#define C_DBL_CompanyTableNo        2000000006
#define   F_DBL_CR_Name             1

#define C_DBL_DATETableNo           2000000007
#define   F_DBL_DT_PeriodType       1
#define     C_DBL_DT_Date           0
#define     C_DBL_DT_Week           1
#define     C_DBL_DT_Month          2
#define     C_DBL_DT_Quarter        3
#define     C_DBL_DT_Year           4
#define   F_DBL_DT_PeriodStart      2
#define   F_DBL_DT_PeriodEnd        3

#define C_DBL_ConnTableNo           2000000009
#define   F_DBL_CR_ConnectionID     1
#define   F_DBL_CR_UserID           2
#define   F_DBL_CR_MySession        3
#define   F_DBL_CR_UserName         4		// 111109 : gibbet nicht mehr ?!
#define   F_DBL_CR_LoginTime        5
#define   F_DBL_CR_LoginDate        6
#define   F_DBL_CR_CacheReads       7
#define   F_DBL_CR_DiskReads        8
#define   F_DBL_CR_DiskWrites       9
#define   F_DBL_CR_RecsFound        10
#define   F_DBL_CR_RecsScanned      11
#define   F_DBL_CR_RecsInserted     12
#define   F_DBL_CR_RecsDeleted      13
#define   F_DBL_CR_RecsModified     14
#define   F_DBL_CR_SumIntervals     15
// 111109 A
#define   F_DBL_CR_DatabaseName         16
#define   F_DBL_CR_ApplicationName      17
#define   F_DBL_CR_UserType             18
#define     C_DBL_CR_UT_Database        0
#define     C_DBL_CR_UT_Windows         1
#define   F_DBL_CR_HostName             19
#define   F_DBL_CR_CPU                  20
#define   F_DBL_CR_Memory               21
#define   F_DBL_CR_PhysicalIO           22
#define   F_DBL_CR_Blocked              23
#define   F_DBL_CR_WaitTime             24
#define   F_DBL_CR_BlockingConnectionID 25
#define   F_DBL_CR_BlockingUserID       26
#define   F_DBL_CR_BlockingHostName     27
#define   F_DBL_CR_BlockingObjectName   28
#define   F_DBL_CR_IdleTime             29
#define   F_DBL_CR_NetType              30
#define   F_DBL_CR_ServerName           31
#define   F_DBL_CR_ServerHostName       32
// 111109 E


#define C_DBL_DatabaseFilesTableNo  2000000010
#define   F_DBL_DF_No               1
#define   F_DBL_DF_OSFileName       2
#define   F_DBL_DF_SizeInKb         3
#define   F_DBL_DF_TotalReads       4
#define   F_DBL_DF_MeanReadTimeMS   5
#define   F_DBL_DF_ReadsInQueue     6
#define   F_DBL_DF_TotalWrites      7
#define   F_DBL_DF_MeanWriteTimeMS  8
#define   F_DBL_DF_WritesInQueue    9
#define   F_DBL_DF_DiskLoadInPct    10

#define C_DBL_DRIVETableNo          2000000020
#define   F_DBL_DRT_Drive           1
#define   F_DBL_DRT_Removable       2
#define   F_DBL_DRT_SizeInKb        3
#define   F_DBL_DRT_FreeInKb        4

#define C_DBL_FILETableNo           2000000022
#define   F_DBL_FT_Path             1
#define   F_DBL_FT_IsFile           2
#define   F_DBL_FT_Name             3
#define   F_DBL_FT_Size             4
#define   F_DBL_FT_Date             5
#define   F_DBL_FT_Time             6
#define   F_DBL_FT_Data             7

#define C_DBL_MoniTableNo            2000000024
#define   F_DBL_MT_CallNo            1
#define   F_DBL_MT_FuncType          2
#define   F_DBL_MT_ParameterSeqNo    3
#define   F_DBL_MT_Parameter         4
#define     C_DBL_TableParmNo          1
#define     C_DBL_SearchRecParmNo      2
#define     C_DBL_SearchIndexParmNo    3
#define     C_DBL_FilterParmNo         4
#define     C_DBL_SearchStrParmNo      5
#define     C_DBL_SearchResultParmNo   6
#define     C_DBL_FoundRecParmNo       7
#define     C_DBL_RealsParmNo          8
#define     C_DBL_TimeUsedParmNo       9
#define     C_DBL_RecsScannedParmNo    10
#define     C_DBL_SumIntervalsParmNo   11
#define     C_DBL_RecsDeletedParmNo    12
#define     C_DBL_DiskReadParmNo       13
#define     C_DBL_DiskWriteParmNo      14
#define     C_DBL_RecordParmNo         15
#define     C_DBL_WaitParmNo           16
#define     C_DBL_SumFieldsParmNo      17
#define     C_DBL_BlobFieldParmNo      18
#define     C_DBL_CommitParmNo         19
#define     C_DBL_UserIDParmNo         20
#define     C_DBL_FileNameParmNo       21
#define   F_DBL_MT_Number            5
#define   F_DBL_MT_Data              6

#define C_DBL_S32TableNo             2000000026
#define   F_DBL_DT_S32               1

#define C_DBL_TITableNo              2000000028
#define   F_DBL_TI_CompanyName       1
#define   F_DBL_TI_ObjNo             2
#define   F_DBL_TI_ObjName           3
#define   F_DBL_TI_NoOfRecs          4
#define   F_DBL_TI_RecSize           5
#define   F_DBL_TI_TableSizeInKb     6
#define   F_DBL_TI_Utilization       7

#define C_DBL_SysObjTableNo          2000000029
#define   F_DBL_SYO_ObjType          F_DBL_DR_ObjType
#define     C_DBL_OT_TableData         0
#define     C_DBL_OT_TableDesc         1
#define     C_DBL_OT_Form              2
#define     C_DBL_OT_Report            3
#define     C_DBL_OT_Dataport          4
#define     C_DBL_OT_Codeunit          5
#define     C_DBL_OT_System            10
#define   F_DBL_SYO_ObjNo            F_DBL_DR_ObjNo
#define   F_DBL_SYO_ObjName          F_DBL_DR_ObjName

#define C_DBL_PERFTableNo            2000000037
#define   F_DBL_PERF_No              1
#define   F_DBL_PERF_Name            2
#define   F_DBL_PERF_Value           3
#define   F_DBL_PERF_Unit            4

#define C_DBL_AllObjTableNo          2000000038
#define   F_DBL_ALLO_ObjType         F_DBL_DR_ObjType
#define   F_DBL_ALLO_ObjNo           F_DBL_DR_ObjNo
#define   F_DBL_ALLO_ObjName         F_DBL_DR_ObjName

#define C_DBL_PRINTERTableNo         2000000039
#define   F_DBL_PDR_ID               1
#define   F_DBL_PDR_Name             2
#define   F_DBL_PDR_Driver           3
#define   F_DBL_PDR_Device           4

#define C_DBL_LicenseTableNo         2000000040
#define   F_DBL_LIC_LineNo           1
#define   F_DBL_LIC_Text             2

#define C_DBL_AFIELDTableNo          2000000041
#define   F_DBL_AFIELDT_TableNo      1
#define   F_DBL_AFIELDT_No           2
#define   F_DBL_AFIELDT_TableName    3
#define   F_DBL_AFIELDT_FieldName    4
#define   F_DBL_AFIELDT_Type         5
#define   F_DBL_AFIELDT_Len          6
#define   F_DBL_AFIELDT_Class        7
#define   F_DBL_AFIELDT_Enabled      8
#define   F_DBL_AFIELDT_TypeName     9
// 111109 A
#define   F_DBL_AFIELDT_Caption         20
#define   F_DBL_AFIELDT_RelationTableNo 21
#define   F_DBL_AFIELDT_RelationFieldNo 22
#define   F_DBL_AFIELDT_SQLDataType     23

#define C_DBL_LangTableNo             2000000045
#define F_DBL_Lang_LanguageID         1
#define F_DBL_Lang_PrimaryLanguageID  2
#define F_DBL_Lang_Name               10
#define F_DBL_Lang_ShortName          11
#define F_DBL_Lang_Enabled            20
#define F_DBL_Lang_EnabledGlobal      21
#define F_DBL_Lang_EnabledForm        22
#define F_DBL_Lang_EnabledReport      23
#define F_DBL_Lang_EnabledDataport    24
#define F_DBL_Lang_EnabledXMLPort     25
#define F_DBL_Lang_PrimaryCodePage    30
#define F_DBL_Lang_STXExists          31
#define F_DBL_Lang_ETXExists          32
#define F_DBL_Lang_HelpExists         33

#define C_DBL_AllObjWithCaptionTableNo     2000000058
#define   F_DBL_ALLOWITHCAPTION_ObjType    F_DBL_DR_ObjType
#define   F_DBL_ALLOWITHCAPTION_ObjNo      F_DBL_DR_ObjNo
#define   F_DBL_ALLOWITHCAPTION_ObjName    F_DBL_DR_ObjName
#define   F_DBL_ALLOWITHCAPTION_ObjCaption F_DBL_DR_ObjCaption

#define C_DBL_BreakpointTableNo       2000000059
#define   F_DBL_Breakpoint_ObjNo      1
#define   F_DBL_Breakpoint_ObjType    2
#define   F_DBL_Breakpoint_TrLineNo   3
#define   F_DBL_Breakpoint_CodeLineNo 4
#define   F_DBL_Breakpoint_TrName     5
#define   F_DBL_Breakpoint_ObjName    6
#define   F_DBL_Breakpoint_Enabled    7

#define C_DBL_UserMenuTableNo         2000000061
#define   F_DBL_UM_UserID             1
#define   F_DBL_UM_UserType           2
#define     C_DBL_UT_Database         1
#define     C_DBL_UT_Windows          2
#define   F_DBL_UM_Level              3
#define     C_DBL_OL_Restriction      10
#define     C_DBL_OL_Change           20
#define   F_DBL_UM_Object             4
#define   F_DBL_UM_Modified           5

#define C_DBL_KeyTableNo              2000000063
#define   F_DBL_Key_Table             1
#define   F_DBL_Key_KeyNo             2
#define   F_DBL_Key_TableName         3
#define   F_DBL_Key_KeyFields         4
#define   F_DBL_Key_SumFields         5
#define   F_DBL_Key_SQLIndex          6
#define   F_DBL_Key_Enabled           7
#define   F_DBL_Key_MaintainSQLIndex  8
#define   F_DBL_Key_MaintainSIFTIndex 9
#define   F_DBL_Key_ClusteredIndex    10


// 111109 E

#define DBL_Type_STR                 (45  * 0x100)
#define DBL_Type_DATE                (46  * 0x100)
#define DBL_Type_TIME                (46  * 0x100 + 1)
#define DBL_Type_BINARY              (132 * 0x100)	// 111109 neu
#define DBL_Type_BLOB                (132 * 0x100 + 2)
#define DBL_Type_BOOL                (133 * 0x100)
#define DBL_Type_S16                 (134 * 0x100)
#define DBL_Type_S32                 (135 * 0x100)
#define DBL_Type_U8                  (136 * 0x100)
#define DBL_Type_ALPHA               (137 * 0x100)
#define DBL_Type_O32                 (139 * 0x100)
#define DBL_Type_BCD                 (50  * 0x100)
#define DBL_Type_DATEFORMULA         (46  * 0x100 +22)
#define DBL_Type_S64                 (141 * 0x100)
#define DBL_Type_Duration            (144 * 0x100)
#define DBL_Type_Datetime            (146 * 0x100)
#define DBL_Type_GUID                (145 * 0x100)


#define DBL_Msg_Critical             0x002010
#define DBL_Msg_Warning              0x002030

#endif

#ifndef _DBL_TD_H__
#define _DBL_TD_H__

void    DBL_Exit(void);
DBL_S32 DBL_Init(void);
// 111109 :void    SessionInit(DBL_U8 *DLLNAME);
int  SessionInit(DBL_U8 *DLLNAME);

#ifdef NDBC 
#define DLLNAME                        ((DBL_U8*)"cfrontsql")
#else
#define DLLNAME                        ((DBL_U8*)"cfront")
#endif

#define ID_DBL_ALLOW                               1
#define ID_DBL_GETVERSION                          2
#define ID_DBL_INIT                                3
#define ID_DBL_EXIT                                4
#define ID_DBL_CONNECTSERVER                       5
#define ID_DBL_DISCONNECTSERVER                    6
#define ID_DBL_OPENDATABASE                        7
#define ID_DBL_CLOSEDATABASE                       8
#define ID_DBL_OPENCOMPANY                         9
#define ID_DBL_CLOSECOMPANY                       10
#define ID_DBL_NEXTTABLE                          11
#define ID_DBL_OPENTABLE                          12
#define ID_DBL_TABLENAME                          13
#define ID_DBL_CLOSETABLE                         14
#define ID_DBL_LOCKTABLE                          15
#define ID_DBL_FINDREC                            16
#define ID_DBL_NEXTREC                            17
#define ID_DBL_INSERTREC                          18
#define ID_DBL_DELETEREC                          19
#define ID_DBL_MODIFYREC                          20
#define ID_DBL_RECCOUNT                           21
#define ID_DBL_INITREC                            22
#define ID_DBL_CALCFIELDS                         23
#define ID_DBL_CALCSUMS                           24
#define ID_DBL_KEYCOUNT                           25
#define ID_DBL_NEXTKEY                            26
#define ID_DBL_KEYSUMFIELDS                       27
#define ID_DBL_SETCURRENTKEY                      28
#define ID_DBL_GETCURRENTKEY                      29
#define ID_DBL_SETFILTER                          30
#define ID_DBL_GETFILTER                          31
#define ID_DBL_SETRANGE                           32
#define ID_DBL_GETRANGE                           33
#define ID_DBL_FIELDCOUNT                         34
#define ID_DBL_NEXTFIELD                          35
#define ID_DBL_FIELDTYPE                          36
#define ID_DBL_FIELDSIZE                          37
#define ID_DBL_FIELDNAME                          38
#define ID_DBL_FIELDOPTIONSTR                     39
#define ID_DBL_FIELDCLASS                         40
#define ID_DBL_ASSIGNFIELD                        41
#define ID_DBL_GETFIELDDATAADDR                   42
#define ID_DBL_GETFIELDDATASIZE                   43
#define ID_DBL_GETFIELDDATA                       44
#define ID_DBL_SELECTLATESTVERSION                45
#define ID_DBL_BWT                                46
#define ID_DBL_EWT                                47
#define ID_DBL_AWT                                48
#define ID_DBL_USERID                             49
#define ID_DBL_COMPANYNAME                        50
#define ID_DBL_USERCOUNT                          51
#define ID_DBL_LOGIN                              52
#define ID_DBL_FIELD_2_STR                        53
#define ID_DBL_BCD_2_STR                          54
#define ID_DBL_STR_2_BCD                          55
#define ID_DBL_YMD_2_DATE                         56
#define ID_DBL_DATE_2_YMD                         57
#define ID_DBL_HMST_2_TIME                        58
#define ID_DBL_TIME_2_HMST                        59
#define ID_DBL_ALPHA_2_STR                        60
#define ID_DBL_STR_2_ALPHA                        61
#define ID_DBL_NEXTCOMPANY                        62
#define ID_DBL_TABLENO                            63
#define ID_DBL_FIELDNO                            64
#define ID_DBL_ALLOCREC                           65
#define ID_DBL_FREEREC                            66
#define ID_DBL_SETEXCEPTIONHANDLER                67
#define ID_DBL_GETDATABASENAME                    68
#define ID_DBL_COPYREC                            69
#define ID_DBL_CMPREC                             70
#define ID_DBL_SETMESSAGESHOWHANDLER              71
#define ID_DBL_GETLASTERRORCODE                   72
#define ID_DBL_CHECKLICENSEFILE                   73
#define ID_DBL_FIELDDATAOFFSET                    74
#define ID_DBL_STR_2_DATE                         75
#define ID_DBL_DATE_2_STR                         76
#define ID_DBL_STR_2_TIME                         77
#define ID_DBL_TIME_2_STR                         78
#define ID_DBL_RELEASEALLOBJECTS                  79
#define ID_DBL_OPENTEMPORARYTABLE                 80
#define ID_DBL_FIELDLEN                           81
#define ID_DBL_CREATETABLEBEGIN                   82
#define ID_DBL_ADDTABLEFIELD                      83
#define ID_DBL_ADDKEY                             84
#define ID_DBL_CREATETABLE                        85
#define ID_DBL_CREATETABLEEND                     86
#define ID_DBL_DELETETABLE                        87
#define ID_DBL_SETNAVISIONPATH                    88
#define ID_DBL_LOADLICENSEFILE                    89
#define ID_DBL_CRYPTPASSWORD                      90
#define ID_DBL_DELETERECS                         91
#define ID_DBL_BCD_2_DOUBLE                       92 
#define ID_DBL_DOUBLE_2_BCD                       93
#define ID_DBL_BCD_2_S32                          94
#define ID_DBL_S32_2_BCD                          95
#define ID_DBL_BCD_ISZERO                         96
#define ID_DBL_BCD_ISNEGATIVE                     97
#define ID_DBL_BCD_ISPOSITIVE                     98
#define ID_DBL_BCD_DIV                            99
#define ID_DBL_BCD_MUL                           100
#define ID_DBL_BCD_ADD                           101
#define ID_DBL_BCD_SUB                           102
#define ID_DBL_BCD_ABS                           103
#define ID_DBL_BCD_NEG                           104
#define ID_DBL_BCD_POWER                         105
#define ID_DBL_BCD_SGN                           106
#define ID_DBL_BCD_CMP                           107
#define ID_DBL_BCD_TRUNC                         108
#define ID_DBL_BCD_ROUND                         109
#define ID_DBL_BCD_ROUNDUNIT                     110
#define ID_DBL_BCD_MAKE                          111
#define ID_DBL_DBL_CONNECTSERVERANDOPENDATABASE  112
#define ID_DBL_OEM2ANSIBUFF                      113
#define ID_DBL_ANSI2OEMBUFF                      114
#define ID_DBL_USECODEUNITSPERMISSIONS           115
#define ID_DBL_TABLEISSAME                       116
#define ID_DBL_TABLEDUP                          117
#define ID_DBL_DUPREC                            118
#define ID_DBL_SETVIEW                           119
#define ID_DBL_GETVIEW                           120
#define ID_DBL_SETTRANSFER                       121
#define ID_DBL_DATEFORMULA_2_STR                 122
#define ID_DBL_STR_2_DATEFORMULA                 123
#define ID_DBL_S64_2_STR                         124
#define ID_DBL_STR_2_S64                         125
#define ID_DBL_S64_2_S32                         126
#define ID_DBL_S32_2_S64                         127
#define ID_DBL_DURATION_2_STR                    128
#define ID_DBL_STR_2_DURATION                    129
#define ID_DBL_DATETIME_2_STR                    130
#define ID_DBL_STR_2_DATETIME                    131
#define ID_DBL_DATETIME_2_YMDHMST                132
#define ID_DBL_YMDHMST_2_DATETIME                133
#define ID_DBL_GUID_2_STR                        134
#define ID_DBL_STR_2_GUID                        135

// 111109 A
#define ID_DBL_RECORDID_2_STR                    136
#define ID_DBL_STR_2_RECORDID                    137
#define ID_DBL_TABLECAPTION                      138
#define ID_DBL_FIELDCAPTION                      139
#define ID_DBL_FIELDOPTIONCAPTION                140
#define ID_DBL_GETLANGUAGE                       141
#define ID_DBL_SETLANGUAGE                       142
#define ID_DBL_RENAMEREC                         143
#define ID_DBL_TABLEFILTER_2_STR                 144
#define ID_DBL_STR_2_TABLEFILTER                 145
#define ID_DBL_STR_COMPARE_DATABASE              146
#define ID_DBL_GETCHALLENGE                      147
#define ID_DBL_INITLOGIN                         148
#define ID_DBL_DATETIME_2_STR_EX                 149
#define ID_DBL_STR_2_DATETIME_EX                 150
#define ID_DBL_DATETIME_2_YMDHMST_EX             151
#define ID_DBL_YMDHMST_2_DATETIME_EX             152
#define ID_DBL_TESTSYSPERMISSION                 153
#define ID_DBL_STR_COMPARE_DATABASE_EXT          154
#define ID_DBL_FINDTOPREC                        155
#define ID_DBL_FINDSET                           156
#define ID_DBL_KEYSQLINDEXFIELDS                 157
#define ID_DBL_HIDEMESSAGESHOWHANDLER            158
#define ID_DBL_GETLASTERRORMESSAGE               159
#define ID_DBL_STR_2_FIELD                       160
#define ID_DBL_USERCOUNT_EXT                     161
#define ID_DBL_REMOVEFILTER                      162
#define ID_DBL_IMPORTFOB                         163
#define ID_DBL_EXPORTFOB                         164

typedef DBL_S32         (DBL_CDECL *tDBL_Init)(void);
typedef void            (DBL_CDECL *tDBL_Exit)(void);


// 111109 E

typedef void      (DBL_CDECL *tDBL_Allow)(DBL_S32 ErrorCode);
typedef DBL_S32   (DBL_CDECL *tDBL_GetVersion)(void);
typedef void      (DBL_CDECL *tDBL_ConnectServer)(const DBL_U8 *ServerName,const DBL_U8 *NetType);
typedef void      (DBL_CDECL *tDBL_DisconnectServer)(void);
typedef void      (DBL_CDECL *tDBL_OpenDatabase)(const DBL_U8 *DatabaseName, DBL_S32 CacheSize, DBL_BOOL UseCommitCache);
typedef void      (DBL_CDECL *tDBL_CloseDatabase)(void);
typedef DBL_U8*   (DBL_CDECL *tDBL_NextCompany)(DBL_U8 *CompanyName);
typedef void      (DBL_CDECL *tDBL_OpenCompany)(const DBL_U8 *CompanyName);
typedef void      (DBL_CDECL *tDBL_CloseCompany)(void);
typedef DBL_S32   (DBL_CDECL *tDBL_NextTable)(DBL_S32 TableNo);
typedef DBL_BOOL  (DBL_CDECL *tDBL_OpenTable)(DBL_HTABLE *hTablePtr, DBL_S32 TableNo);
typedef DBL_U8*   (DBL_CDECL *tDBL_TableName)(DBL_HTABLE hTable);
typedef DBL_S32   (DBL_CDECL *tDBL_TableNo)(const DBL_U8 *TableName);
typedef void      (DBL_CDECL *tDBL_CloseTable)(DBL_HTABLE hTable);
typedef void      (DBL_CDECL *tDBL_LockTable)(DBL_HTABLE hTable, DBL_U32 Mode);
typedef DBL_HREC  (DBL_CDECL *tDBL_AllocRec)(DBL_HTABLE hTable);
typedef void      (DBL_CDECL *tDBL_FreeRec)(DBL_HREC Rec);
typedef DBL_BOOL  (DBL_CDECL *tDBL_FindRec)(DBL_HTABLE hTable, DBL_HREC Rec, const DBL_U8 *SearchMethod);
typedef DBL_S16   (DBL_CDECL *tDBL_NextRec)(DBL_HTABLE hTable, DBL_HREC Rec, DBL_S16 Step);
typedef DBL_BOOL  (DBL_CDECL *tDBL_InsertRec)(DBL_HTABLE hTable, DBL_HREC Rec);
typedef DBL_BOOL  (DBL_CDECL *tDBL_DeleteRec)(DBL_HTABLE hTable, DBL_HREC Rec);
typedef DBL_BOOL  (DBL_CDECL *tDBL_ModifyRec)(DBL_HTABLE hTable, DBL_HREC Rec);

typedef DBL_BOOL        (DBL_CDECL *tDBL_RenameRec)(DBL_HTABLE hTable, DBL_HREC NewRec, DBL_HREC OldRec);	// 111109 neu

typedef DBL_S32   (DBL_CDECL *tDBL_RecCount)(DBL_HTABLE hTable);
typedef void      (DBL_CDECL *tDBL_InitRec)(DBL_HTABLE hTable, DBL_HREC Rec);
typedef void      (DBL_CDECL *tDBL_CalcFields)(DBL_HTABLE hTable, DBL_HREC Rec, DBL_S32 *FieldList);
typedef void      (DBL_CDECL *tDBL_CalcSums)(DBL_HTABLE hTable, DBL_HREC Rec, DBL_S32 *FieldList);
typedef DBL_S16   (DBL_CDECL *tDBL_KeyCount)(DBL_HTABLE hTable);
typedef DBL_S32*  (DBL_CDECL *tDBL_NextKey)(DBL_HTABLE hTable, DBL_S32 *Key);
typedef DBL_S32*  (DBL_CDECL *tDBL_KeySumFields)(DBL_HTABLE hTable, DBL_S32 *Key);
typedef DBL_BOOL  (DBL_CDECL *tDBL_SetCurrentKey)(DBL_HTABLE hTable, DBL_S32 *Key);
typedef DBL_S32*  (DBL_CDECL *tDBL_GetCurrentKey)(DBL_HTABLE hTable);
typedef void      (DBL_CDECL *tDBL_SetFilter)(DBL_HTABLE hTable, DBL_S32 FieldNo, const DBL_U8 *FilterStr, void *ValuePtr1, ...);

typedef void            (DBL_CDECL *tDBL_RemoveFilter)(DBL_HTABLE hTable);	// 111105 neu

typedef DBL_S32   (DBL_CDECL *tDBL_GetFilter)(DBL_HTABLE hTable, DBL_S32 FieldNo, DBL_U8 *FilterStr, DBL_S16 FilterStrSize);
typedef void      (DBL_CDECL *tDBL_SetRange)(DBL_HTABLE hTable, DBL_S32 FieldNo, void *MinValue,void *MaxValue);
typedef void      (DBL_CDECL *tDBL_GetRange)(DBL_HTABLE hTable, DBL_S32 FieldNo, void *MinValue,void *MaxValue);
typedef DBL_S16   (DBL_CDECL *tDBL_FieldCount)(DBL_HTABLE hTable);
typedef DBL_S32   (DBL_CDECL *tDBL_NextField)(DBL_HTABLE hTable, DBL_S32 FieldNo);
typedef DBL_U16   (DBL_CDECL *tDBL_FieldType)(DBL_HTABLE hTable, DBL_S32 FieldNo);
typedef DBL_S16   (DBL_CDECL *tDBL_FieldSize)(DBL_HTABLE hTable, DBL_S32 FieldNo);
typedef DBL_S32   (DBL_CDECL *tDBL_FieldNo)(DBL_HTABLE hTable, const DBL_U8 *FieldName);
typedef DBL_U8*   (DBL_CDECL *tDBL_FieldName)(DBL_HTABLE hTable, DBL_S32 FieldNo);
typedef DBL_U8*   (DBL_CDECL *tDBL_FieldOptionStr)(DBL_HTABLE hTable, DBL_S32 FieldNo);
typedef DBL_S16   (DBL_CDECL *tDBL_FieldClass)(DBL_HTABLE hTable, DBL_S32 FieldNo);
typedef void      (DBL_CDECL *tDBL_AssignField)(DBL_HTABLE hTable, DBL_HREC hRec, DBL_S32 FieldNo, DBL_U16 Type, void *Data, DBL_S32 DataSize);
typedef void*     (DBL_CDECL *tDBL_GetFieldDataAddr)(DBL_HTABLE hTable, DBL_HREC hRec, DBL_S32 FieldNo);
typedef DBL_S32   (DBL_CDECL *tDBL_GetFieldDataSize)(DBL_HTABLE hTable, DBL_S32 FieldNo, const void *FieldVal);
typedef DBL_S32   (DBL_CDECL *tDBL_GetFieldData)(void *Dst, DBL_S32 DstSize, DBL_HTABLE hTable, DBL_HREC hRec, DBL_S32 FieldNo);
typedef void      (DBL_CDECL *tDBL_SelectLatestVersion)(void);
typedef void      (DBL_CDECL *tDBL_BWT)(void);
typedef void      (DBL_CDECL *tDBL_EWT)(void);
typedef void      (DBL_CDECL *tDBL_AWT)(void);
typedef DBL_U8*   (DBL_CDECL *tDBL_UserID)(void);
typedef DBL_U8*   (DBL_CDECL *tDBL_CompanyName)(void);

// 111109 A
typedef const DBL_U8*   (DBL_CDECL *tDBL_ImportFOB)(const DBL_U8 *FileName, DBL_U32 Mode);
typedef void            (DBL_CDECL *tDBL_ExportFOB)(const DBL_U8 *FileName, const DBL_U8 *Filter);

enum USER_ACCOUNT_FLG
{
  DB_USER   = 1, // DB users
  WIN_USER  = 2  // Windows users
};
// The functionality of this function like the tDBL_UserCount, but
// it can returns a number of registered Windows user as well as DB users or both.
typedef DBL_S32         (DBL_CDECL *tDBL_UserCountExt)( enum USER_ACCOUNT_FLG userKind );



// 111109 E

typedef DBL_S32   (DBL_CDECL *tDBL_UserCount)(void);


typedef DBL_BOOL  (DBL_CDECL *tDBL_Login)(const DBL_U8 *UserID, const DBL_U8 *PassWord);
typedef void      (DBL_CDECL *tDBL_Field_2_Str)(DBL_U8 *Str, DBL_S16 StrSize, DBL_HTABLE hTable, DBL_HREC Rec, DBL_S32 FieldNo);
typedef void       (DBL_CDECL *tDBL_Str_2_Field)(DBL_HTABLE hTable, DBL_HREC Rec, DBL_S32 FieldNo,DBL_U8 *Str);	// neu 111109
typedef void      (DBL_CDECL *tDBL_BCD_2_Str)(DBL_U8 *Str, DBL_S16 StrSize, DBL_BCD *Bcd);
typedef void      (DBL_CDECL *tDBL_Str_2_BCD)(DBL_BCD *Bcd, DBL_U8 *Str);
typedef void      (DBL_CDECL *tDBL_YMD_2_Date)(DBL_DATE *Date, DBL_S32 y, DBL_S32 m, DBL_S32 d, DBL_BOOL Closing);
typedef void      (DBL_CDECL *tDBL_Date_2_YMD)(DBL_S32 *y, DBL_S32 *m, DBL_S32 *d, DBL_BOOL *Closing, DBL_DATE Date);
typedef void      (DBL_CDECL *tDBL_HMST_2_Time)(DBL_TIME *Time, DBL_S32 h, DBL_S32 m, DBL_S32 s, DBL_S32 t);
typedef void      (DBL_CDECL *tDBL_Time_2_HMST)(DBL_S32 *h, DBL_S32 *m, DBL_S32 *s, DBL_S32 *t, DBL_TIME Time);
typedef void      (DBL_CDECL *tDBL_Alpha_2_Str)(DBL_U8 *Str, DBL_S16 StrSize, DBL_U8 *Alpha);
typedef void      (DBL_CDECL *tDBL_Str_2_Alpha)(DBL_U8 *Alpha, DBL_S16 AlphaSize, DBL_U8 *Str);
typedef void*     (DBL_CDECL *tDBL_SetExceptionHandler)(DBL_pFuncExceptionHandler ExceptionHandler);
typedef DBL_BOOL  (DBL_CDECL *tDBL_GetDatabaseName)(DBL_U8 *DatabaseName);
typedef void      (DBL_CDECL *tDBL_CopyRec)(DBL_HTABLE hTable, DBL_HREC hDstRec, const DBL_HREC hSrcRec);
typedef DBL_BOOL  (DBL_CDECL *tDBL_CmpRec)(DBL_HTABLE hTable, const DBL_HREC hDstRec, const DBL_HREC  hSrcRec);
typedef void*     (DBL_CDECL *tDBL_SetMessageShowHandler)(DBL_pFuncMessageShowHandler ShowFunc);
typedef void            (DBL_CDECL *tDBL_HideMessageShowHandler)(DBL_BOOL Hide); // 111109 neu
typedef DBL_S32   (DBL_CDECL *tDBL_GetLastErrorCode)();
typedef void            (DBL_CDECL *tDBL_GetLastErrorMessage)(DBL_U8 *Message, DBL_S32 MessageLength);	// 111109 neu
typedef void      (DBL_CDECL *tDBL_CheckLicenseFile)(DBL_S32 ObjectNo);
typedef DBL_S16   (DBL_CDECL *tDBL_FieldDataOffset)(DBL_HTABLE hTable, DBL_S32 FieldNo);
typedef void      (DBL_CDECL *tDBL_Str_2_Date)(DBL_DATE *Date, DBL_U8 *Str);
typedef void      (DBL_CDECL *tDBL_Date_2_Str)(DBL_U8 *Str, DBL_S16 StrSize, DBL_DATE Date);
typedef void      (DBL_CDECL *tDBL_Str_2_Time)(DBL_TIME *Time, DBL_U8 *Str);
typedef void      (DBL_CDECL *tDBL_Time_2_Str)(DBL_U8 *Str, DBL_S16 StrSize, DBL_TIME Time);
typedef void      (DBL_CDECL *tDBL_ReleaseAllObjects)(void);
typedef DBL_BOOL  (DBL_CDECL *tDBL_OpenTemporaryTable)(DBL_HTABLE *hTablePtr, DBL_S32 TableNo);
typedef DBL_S16   (DBL_CDECL *tDBL_FieldLen)(DBL_HTABLE hTable, DBL_S32 FieldNo);
typedef void      (DBL_CDECL *tDBL_CreateTableBegin)(DBL_HCREATE_TABLE *phCT, DBL_S32 TableNo, DBL_U8 *TableName,DBL_BOOL DataPerCompany);
typedef void      (DBL_CDECL *tDBL_AddTableField)(DBL_HCREATE_TABLE hCT, DBL_S32 FieldNo, DBL_U8 *FieldName, DBL_U16 FieldType, DBL_S16 FieldLen, DBL_U8 *OptionStr, DBL_S16 FieldClass);
typedef void      (DBL_CDECL *tDBL_AddKey)(DBL_HCREATE_TABLE  hCT, DBL_S32 *Key, DBL_S32 *SumIndexFields);
typedef DBL_BOOL  (DBL_CDECL *tDBL_CreateTable)(DBL_HCREATE_TABLE hCT);
typedef void      (DBL_CDECL *tDBL_CreateTableEnd)(DBL_HCREATE_TABLE hCT);
typedef DBL_BOOL  (DBL_CDECL *tDBL_DeleteTable)(DBL_S32 TableNo);
typedef void      (DBL_CDECL *tDBL_SetNavisionPath)(DBL_U8 *Path);
typedef void      (DBL_CDECL *tDBL_LoadLicenseFile)(const DBL_U8 *FileName);                  
typedef void      (DBL_CDECL *tDBL_CryptPassword)(const DBL_U8 *UserID, DBL_U8 *PassWord);
typedef void      (DBL_CDECL *tDBL_DeleteRecs)(DBL_HTABLE hTable);
typedef void      (DBL_CDECL *tDBL_BCD_2_Double)(DBL_DOUBLE *Dest,const DBL_BCD *Source);
typedef void      (DBL_CDECL *tDBL_Double_2_BCD)(DBL_BCD *Dest,DBL_DOUBLE Source);
typedef DBL_S32   (DBL_CDECL *tDBL_BCD_2_S32)(const DBL_BCD *Source);
typedef void      (DBL_CDECL *tDBL_S32_2_BCD)(DBL_BCD *Dest, DBL_S32 Source);
typedef DBL_BOOL  (DBL_CDECL *tDBL_BCD_IsZero)(const DBL_BCD *Source);
typedef DBL_BOOL  (DBL_CDECL *tDBL_BCD_IsNegative)(const DBL_BCD *Source);
typedef DBL_BOOL  (DBL_CDECL *tDBL_BCD_IsPositive)(const DBL_BCD *Source);
typedef DBL_BCD*  (DBL_CDECL *tDBL_BCD_Div)(DBL_BCD *Dest,const DBL_BCD *Source);
typedef DBL_BCD*  (DBL_CDECL *tDBL_BCD_Mul)(DBL_BCD *Dest,const DBL_BCD *Source);
typedef DBL_BCD*  (DBL_CDECL *tDBL_BCD_Add)(DBL_BCD *Dest,const DBL_BCD *Source);
typedef DBL_BCD*  (DBL_CDECL *tDBL_BCD_Sub)(DBL_BCD *Dest,const DBL_BCD *Source);
typedef void      (DBL_CDECL *tDBL_BCD_Abs)(DBL_BCD *Dest);
typedef void      (DBL_CDECL *tDBL_BCD_Neg)(DBL_BCD *Dest);
typedef DBL_BCD*  (DBL_CDECL *tDBL_BCD_Power)(DBL_BCD *Dest,const DBL_BCD *Power);
typedef DBL_S32   (DBL_CDECL *tDBL_BCD_Sgn)(const DBL_BCD *Source);
typedef DBL_S32   (DBL_CDECL *tDBL_BCD_Cmp)(const DBL_BCD *Left, const DBL_BCD *Right);
typedef void      (DBL_CDECL *tDBL_BCD_Trunc)(DBL_BCD *Dest, DBL_S32 Cnt);
typedef void      (DBL_CDECL *tDBL_BCD_Round)(DBL_BCD *Dest, DBL_S32 Cnt);
typedef void      (DBL_CDECL *tDBL_BCD_RoundUnit)(DBL_BCD *Dest,const DBL_BCD *Unit, DBL_S32 How);
typedef DBL_BCD*  (DBL_CDECL *tDBL_BCD_Make)(DBL_BCD *Dest, DBL_S32 Kind);
typedef void      (DBL_CDECL *tDBL_ConnectServerAndOpenDatabase)(const DBL_U8 *NDBCDriverName,
                                                                 const DBL_U8 *ServerName,
                                                                 const DBL_U8 *NetType,
                                                                 const DBL_U8 *DatabaseName,
                                                                 DBL_S32       CacheSize,
                                                                 DBL_BOOL      UseCommitCache,
                                                                 DBL_BOOL      UseNTAuthentication,
                                                                 const DBL_U8 *UserID,
                                                                 const DBL_U8 *PassWord);
typedef void      (DBL_CDECL *tDBL_Oem2AnsiBuff)(const DBL_U8 *Src,DBL_U8 *Dst,DBL_S32 DstSize);

typedef void      (DBL_CDECL *tDBL_Ansi2OemBuff)(const DBL_U8 *Src,DBL_U8 *Dst,DBL_S32 DstSize);

typedef void      (DBL_CDECL *tDBL_UseCodeUnitsPermissions)(DBL_S32 CodeUnitID);

typedef DBL_BOOL  (DBL_CDECL *tDBL_TableIsSame)(const DBL_HTABLE hTable1, const DBL_HTABLE hTable2);
typedef DBL_HTABLE(DBL_CDECL *tDBL_TableDup)(const DBL_HTABLE hSrcTable);
typedef DBL_HREC  (DBL_CDECL *tDBL_DupRec)(const DBL_HTABLE hTable, const DBL_HREC hSrcRec);
typedef void      (DBL_CDECL *tDBL_SetView)(DBL_HTABLE hTable, const DBL_U8 *ViewStr);
typedef void      (DBL_CDECL *tDBL_GetView)(const DBL_HTABLE hTable, DBL_U8 *ViewStrBuffer, DBL_S32 ViewStrBufferSize, DBL_BOOL UseFieldNames);
typedef DBL_BOOL  (DBL_CDECL *tDBL_SetTransfer )(DBL_HTABLE *hInnerTable, DBL_HTABLE *hOuterTable, DBL_HREC *hInnerRec, DBL_HREC *hOuterRec, const DBL_U8 *TFormula);

typedef void      (DBL_CDECL *tDBL_DateFormula_2_Str)(DBL_U8 *Str,DBL_S16 StrSize,DBL_U8 *DateFormula);
typedef void      (DBL_CDECL *tDBL_Str_2_DateFormula)(DBL_U8 *DateFormula,DBL_S16 DateFormulaSize,DBL_U8 *Str);
typedef void      (DBL_CDECL *tDBL_S64_2_Str)(DBL_U8  *Str,DBL_S16  StrSize,DBL_S64 *S64Src);
typedef void      (DBL_CDECL *tDBL_Str_2_S64)(DBL_S64 *S64Dest,DBL_U8  *Str);
typedef void      (DBL_CDECL *tDBL_S64_2_S32)(DBL_S32 *S32,DBL_S64 *S64Src);
typedef void      (DBL_CDECL *tDBL_S32_2_S64)(DBL_S64 *S64Dest,DBL_S32  S32);
typedef void      (DBL_CDECL *tDBL_Duration_2_Str)(DBL_U8 *Str,DBL_S16 StrSize,DBL_Duration *DurationSrc);
typedef void      (DBL_CDECL *tDBL_Str_2_Duration)(DBL_Duration *DurationDest,DBL_U8 *Str);
typedef void      (DBL_CDECL *tDBL_Datetime_2_Str)(DBL_U8 *Str,DBL_S16 StrSize,DBL_Datetime *DatetimeSrc);
typedef void      (DBL_CDECL *tDBL_Str_2_Datetime)(DBL_Datetime *DatetimeDest,DBL_U8 *Str);
typedef void      (DBL_CDECL *tDBL_Datetime_2_YMDHMST)(DBL_S32      *Year,
                                                       DBL_S32      *Month,
                                                       DBL_S32      *Day,
                                                       DBL_S32      *Hour,
                                                       DBL_S32      *Minute,
                                                       DBL_S32      *Second,
                                                       DBL_S32      *Fraction,
                                                       DBL_Datetime *DatetimeSrc);
typedef void      (DBL_CDECL *tDBL_YMDHMST_2_Datetime)(DBL_Datetime *DatetimeDest,
                                                       DBL_S32       Year,
                                                       DBL_S32       Month,
                                                       DBL_S32       Day,
                                                       DBL_S32       Hour,
                                                       DBL_S32       Minute,
                                                       DBL_S32       Second,
                                                       DBL_S32       Fraction);
typedef void      (DBL_CDECL *tDBL_GUID_2_Str)(DBL_U8 *Str,DBL_S16 StrSize,DBL_GUID *GuidSrc);
typedef void      (DBL_CDECL *tDBL_Str_2_GUID)(DBL_GUID *GuidDest,DBL_U8 *Str);
// 111109 A

typedef void            (DBL_CDECL *tDBL_RECORDID_2_Str)(DBL_U8 *Str,DBL_S16 StrSize,DBL_RECORDID *RecordIDSrc);
typedef void            (DBL_CDECL *tDBL_Str_2_RECORDID)(DBL_RECORDID *RecordIDDest,DBL_U8 *Str);
typedef const DBL_U8*   (DBL_CDECL *tDBL_TableCaption)(DBL_HTABLE hTable);
typedef const DBL_U8*   (DBL_CDECL *tDBL_FieldCaption)(DBL_HTABLE hTable, DBL_S32 FieldNo);
typedef DBL_U8*         (DBL_CDECL *tDBL_FieldOptionCaption)(DBL_HTABLE hTable, DBL_S32 FieldNo);
typedef DBL_S32         (DBL_CDECL *tDBL_GetLanguage)(void);
typedef void            (DBL_CDECL *tDBL_SetLanguage)(DBL_S32 LanguageID);
typedef void            (DBL_CDECL *tDBL_TableFilter_2_Str)(DBL_U8 *Str,DBL_S16 StrSize,DBL_TABLEFILTER *TableFilterSrc);
typedef void            (DBL_CDECL *tDBL_Str_2_TableFilter)(DBL_TABLEFILTER *TableFilterDest,DBL_U8 *Str);
typedef DBL_S32         (DBL_CDECL *tDBL_Str_Compare_Database)(const DBL_U8 *LeftStr, const DBL_U8 *RightStr);

// NB: The values of STR_CMP_MODE should be always syncronized with constants
// C_Str_CompCase, C_Str_CompNoCase, C_Str_CompNoAccent from the "../modules/rt/STR_T.H" file.
enum STR_CMP_MODE
{
  CompCase = 1,     // case sensitive comparison
  CompNoCase = 3,   // case insensitive comparison
  CompNoAccent = 5, // comparison without accent
};
// The functionality of this function the same with tDBL_Str_Compare_Database, but
// comparison is controlled by the "strCmpMode" parameter.
typedef DBL_S32     (DBL_CDECL *tDBL_Str_Compare_DatabaseExt)(const DBL_U8 *LeftStr, const DBL_U8 *RightStr, enum STR_CMP_MODE strCmpMode );

typedef DBL_U32         (DBL_CDECL *tDBL_GetChallenge)(DBL_Consumer Consumer, DBL_U8 **pChallenge);
typedef void            (DBL_CDECL *tDBL_InitLogin)(const DBL_U8 *Response, DBL_U32 DataSize);
typedef void            (DBL_CDECL *tDBL_Datetime_2_Str_Ex)(DBL_U8 *Str,DBL_S16 StrSize,DBL_Datetime *DatetimeSrc, DBL_BOOL UTC_Time);
typedef void            (DBL_CDECL *tDBL_Str_2_Datetime_Ex)(DBL_Datetime *DatetimeDest,DBL_U8 *Str, DBL_BOOL UTC_Time);
typedef void            (DBL_CDECL *tDBL_Datetime_2_YMDHMST_Ex)(
                                                             DBL_S32      *Year,
                                                             DBL_S32      *Month,
                                                             DBL_S32      *Day,
                                                             DBL_S32      *Hour,
                                                             DBL_S32      *Minute,
                                                             DBL_S32      *Second,
                                                             DBL_S32      *Fraction,
                                                             DBL_Datetime *DatetimeSrc,
                                                             DBL_BOOL      UTC_Time);
typedef void            (DBL_CDECL *tDBL_YMDHMST_2_Datetime_Ex)(
                                                             DBL_Datetime *DatetimeDest,
                                                             DBL_S32       Year,
                                                             DBL_S32       Month,
                                                             DBL_S32       Day,
                                                             DBL_S32       Hour,
                                                             DBL_S32       Minute,
                                                             DBL_S32       Second,
                                                             DBL_S32       Fraction,
                                                             DBL_BOOL      UTC_Time);
typedef DBL_BOOL        (DBL_CDECL *tDBL_TestSysPermission)(DBL_S32 ObjectNo);
typedef DBL_BOOL        (DBL_CDECL *tDBL_FindTopRec)(DBL_HTABLE hTable, DBL_HREC Rec, DBL_BOOL First);
typedef DBL_BOOL        (DBL_CDECL *tDBL_FindSet)(DBL_HTABLE hTable, DBL_HREC Rec, DBL_BOOL ForUpdate, DBL_BOOL UpdateKey);
typedef DBL_S32*        (DBL_CDECL *tDBL_KeySQLIndexFields)(DBL_HTABLE hTable, DBL_S32 *Key);


// 111109 E

extern tDBL_Allow                        DBL_Allow                        ;
extern tDBL_GetVersion                   DBL_GetVersion                   ;
extern tDBL_ConnectServer                DBL_ConnectServer                ;
extern tDBL_DisconnectServer             DBL_DisconnectServer             ;
extern tDBL_OpenDatabase                 DBL_OpenDatabase                 ;
extern tDBL_CloseDatabase                DBL_CloseDatabase                ;
extern tDBL_NextCompany                  DBL_NextCompany                  ;
extern tDBL_OpenCompany                  DBL_OpenCompany                  ;
extern tDBL_CloseCompany                 DBL_CloseCompany                 ;
extern tDBL_NextTable                    DBL_NextTable                    ;
extern tDBL_OpenTable                    DBL_OpenTable                    ;
extern tDBL_TableName                    DBL_TableName                    ;
extern tDBL_TableNo                      DBL_TableNo                      ;
extern tDBL_CloseTable                   DBL_CloseTable                   ;
extern tDBL_LockTable                    DBL_LockTable                    ;
extern tDBL_AllocRec                     DBL_AllocRec                     ;
extern tDBL_FreeRec                      DBL_FreeRec                      ;
extern tDBL_FindRec                      DBL_FindRec                      ;
extern tDBL_NextRec                      DBL_NextRec                      ;
extern tDBL_InsertRec                    DBL_InsertRec                    ;
extern tDBL_DeleteRec                    DBL_DeleteRec                    ;
extern tDBL_ModifyRec                    DBL_ModifyRec                    ;
extern tDBL_RenameRec                    DBL_RenameRec                    ;	// 111109 neu
extern tDBL_RecCount                     DBL_RecCount                     ;
extern tDBL_InitRec                      DBL_InitRec                      ;
extern tDBL_CalcFields                   DBL_CalcFields                   ;
extern tDBL_CalcSums                     DBL_CalcSums                     ;
extern tDBL_KeyCount                     DBL_KeyCount                     ;
extern tDBL_NextKey                      DBL_NextKey                      ;
extern tDBL_KeySumFields                 DBL_KeySumFields                 ;
extern tDBL_SetCurrentKey                DBL_SetCurrentKey                ;
extern tDBL_GetCurrentKey                DBL_GetCurrentKey                ;
extern tDBL_SetFilter                    DBL_SetFilter                    ;
extern tDBL_RemoveFilter                 DBL_RemoveFilter                 ;	// 111109 neu
extern tDBL_GetFilter                    DBL_GetFilter                    ;
extern tDBL_SetRange                     DBL_SetRange                     ;
extern tDBL_GetRange                     DBL_GetRange                     ;
extern tDBL_FieldCount                   DBL_FieldCount                   ;
extern tDBL_NextField                    DBL_NextField                    ;
extern tDBL_FieldType                    DBL_FieldType                    ;
extern tDBL_FieldSize                    DBL_FieldSize                    ;
extern tDBL_FieldNo                      DBL_FieldNo                      ;
extern tDBL_FieldName                    DBL_FieldName                    ;
extern tDBL_FieldOptionStr               DBL_FieldOptionStr               ;
extern tDBL_FieldClass                   DBL_FieldClass                   ;
extern tDBL_AssignField                  DBL_AssignField                  ;
extern tDBL_GetFieldDataAddr             DBL_GetFieldDataAddr             ;
extern tDBL_GetFieldDataSize             DBL_GetFieldDataSize             ;
extern tDBL_GetFieldData                 DBL_GetFieldData                 ;
extern tDBL_SelectLatestVersion          DBL_SelectLatestVersion          ;
extern tDBL_BWT                          DBL_BWT                          ;
extern tDBL_EWT                          DBL_EWT                          ;
extern tDBL_AWT                          DBL_AWT                          ;
extern tDBL_UserID                       DBL_UserID                       ;
extern tDBL_CompanyName                  DBL_CompanyName                  ;
extern tDBL_UserCount                    DBL_UserCount                    ;
extern tDBL_UserCountExt                 DBL_UserCountExt                 ;		// 111109 neu
extern tDBL_Login                        DBL_Login                        ;
extern tDBL_Field_2_Str                  DBL_Field_2_Str                  ;
extern tDBL_Str_2_Field                  DBL_Str_2_Field                  ;		// 111109 neu
extern tDBL_BCD_2_Str                    DBL_BCD_2_Str                    ;
extern tDBL_Str_2_BCD                    DBL_Str_2_BCD                    ;
extern tDBL_YMD_2_Date                   DBL_YMD_2_Date                   ;
extern tDBL_Date_2_YMD                   DBL_Date_2_YMD                   ;
extern tDBL_HMST_2_Time                  DBL_HMST_2_Time                  ;
extern tDBL_Time_2_HMST                  DBL_Time_2_HMST                  ;
extern tDBL_Alpha_2_Str                  DBL_Alpha_2_Str                  ;
extern tDBL_Str_2_Alpha                  DBL_Str_2_Alpha                  ;
extern tDBL_SetExceptionHandler          DBL_SetExceptionHandler          ;
extern tDBL_GetDatabaseName              DBL_GetDatabaseName              ;
extern tDBL_CopyRec                      DBL_CopyRec                      ;
extern tDBL_CmpRec                       DBL_CmpRec                       ;
extern tDBL_SetMessageShowHandler        DBL_SetMessageShowHandler        ;
extern tDBL_HideMessageShowHandler       DBL_HideMessageShowHandler       ;		// 111109 neu
extern tDBL_GetLastErrorCode             DBL_GetLastErrorCode             ;
extern tDBL_GetLastErrorMessage          DBL_GetLastErrorMessage          ;		// 111109 neu
extern tDBL_CheckLicenseFile             DBL_CheckLicenseFile             ;
extern tDBL_FieldDataOffset              DBL_FieldDataOffset              ;
extern tDBL_Str_2_Date                   DBL_Str_2_Date                   ;
extern tDBL_Date_2_Str                   DBL_Date_2_Str                   ;
extern tDBL_Str_2_Time                   DBL_Str_2_Time                   ;
extern tDBL_Time_2_Str                   DBL_Time_2_Str                   ;
extern tDBL_ReleaseAllObjects            DBL_ReleaseAllObjects            ;
extern tDBL_OpenTemporaryTable           DBL_OpenTemporaryTable           ;
extern tDBL_FieldLen                     DBL_FieldLen                     ;
extern tDBL_CreateTableBegin             DBL_CreateTableBegin             ;
extern tDBL_AddTableField                DBL_AddTableField                ;
extern tDBL_AddKey                       DBL_AddKey                       ;
extern tDBL_CreateTable                  DBL_CreateTable                  ;
extern tDBL_CreateTableEnd               DBL_CreateTableEnd               ;
extern tDBL_DeleteTable                  DBL_DeleteTable                  ;
extern tDBL_SetNavisionPath              DBL_SetNavisionPath              ;
extern tDBL_LoadLicenseFile              DBL_LoadLicenseFile              ;
extern tDBL_CryptPassword                DBL_CryptPassword                ;
extern tDBL_DeleteRecs                   DBL_DeleteRecs                   ;
extern tDBL_BCD_2_Double                 DBL_BCD_2_Double                 ;
extern tDBL_Double_2_BCD                 DBL_Double_2_BCD                 ;
extern tDBL_BCD_2_S32                    DBL_BCD_2_S32                    ;
extern tDBL_S32_2_BCD                    DBL_S32_2_BCD                    ;
extern tDBL_BCD_IsZero                   DBL_BCD_IsZero                   ;
extern tDBL_BCD_IsNegative               DBL_BCD_IsNegative               ;
extern tDBL_BCD_IsPositive               DBL_BCD_IsPositive               ;
extern tDBL_BCD_Div                      DBL_BCD_Div                      ;
extern tDBL_BCD_Mul                      DBL_BCD_Mul                      ;
extern tDBL_BCD_Add                      DBL_BCD_Add                      ;
extern tDBL_BCD_Sub                      DBL_BCD_Sub                      ;
extern tDBL_BCD_Abs                      DBL_BCD_Abs                      ;
extern tDBL_BCD_Neg                      DBL_BCD_Neg                      ;
extern tDBL_BCD_Power                    DBL_BCD_Power                    ;
extern tDBL_BCD_Sgn                      DBL_BCD_Sgn                      ;
extern tDBL_BCD_Cmp                      DBL_BCD_Cmp                      ;
extern tDBL_BCD_Trunc                    DBL_BCD_Trunc                    ;
extern tDBL_BCD_Round                    DBL_BCD_Round                    ;
extern tDBL_BCD_RoundUnit                DBL_BCD_RoundUnit                ;
extern tDBL_BCD_Make                     DBL_BCD_Make                     ;
extern tDBL_ConnectServerAndOpenDatabase DBL_ConnectServerAndOpenDatabase ;
extern tDBL_Oem2AnsiBuff                 DBL_Oem2AnsiBuff                 ;
extern tDBL_Ansi2OemBuff                 DBL_Ansi2OemBuff                 ;
extern tDBL_UseCodeUnitsPermissions      DBL_UseCodeUnitsPermissions      ;
extern tDBL_TableIsSame                  DBL_TableIsSame                  ;
extern tDBL_TableDup                     DBL_TableDup                     ;
extern tDBL_DupRec                       DBL_DupRec                       ;
extern tDBL_SetView                      DBL_SetView                      ;
extern tDBL_GetView                      DBL_GetView                      ;
extern tDBL_SetTransfer                  DBL_SetTransfer                  ;
extern tDBL_DateFormula_2_Str            DBL_DateFormula_2_Str            ;
extern tDBL_Str_2_DateFormula            DBL_Str_2_DateFormula            ;
extern tDBL_S64_2_Str                    DBL_S64_2_Str                    ;
extern tDBL_Str_2_S64                    DBL_Str_2_S64                    ;
extern tDBL_S64_2_S32                    DBL_S64_2_S32                    ;
extern tDBL_S32_2_S64                    DBL_S32_2_S64                    ;
extern tDBL_Duration_2_Str               DBL_Duration_2_Str               ;
extern tDBL_Str_2_Duration               DBL_Str_2_Duration               ;
extern tDBL_Datetime_2_Str               DBL_Datetime_2_Str               ;
extern tDBL_Str_2_Datetime               DBL_Str_2_Datetime               ;
extern tDBL_Datetime_2_YMDHMST           DBL_Datetime_2_YMDHMST           ;
extern tDBL_YMDHMST_2_Datetime           DBL_YMDHMST_2_Datetime           ;
extern tDBL_GUID_2_Str                   DBL_GUID_2_Str                   ;
extern tDBL_Str_2_GUID                   DBL_Str_2_GUID                   ;
// 111109 A

extern tDBL_RECORDID_2_Str               DBL_RECORDID_2_Str               ;
extern tDBL_Str_2_RECORDID               DBL_Str_2_RECORDID               ;
extern tDBL_TableCaption                 DBL_TableCaption                 ;
extern tDBL_FieldCaption                 DBL_FieldCaption                 ;
extern tDBL_FieldOptionStr               DBL_FieldOptionCaption           ;
extern tDBL_GetLanguage                  DBL_GetLanguage                  ;
extern tDBL_SetLanguage                  DBL_SetLanguage                  ;
extern tDBL_TableFilter_2_Str            DBL_TableFilter_2_Str            ;
extern tDBL_Str_2_TableFilter            DBL_Str_2_TableFilter            ;
extern tDBL_Str_Compare_Database         DBL_Str_Compare_Database         ;
extern tDBL_Str_Compare_DatabaseExt      DBL_Str_Compare_DatabaseExt      ;
extern tDBL_GetChallenge                 DBL_GetChallenge                 ;
extern tDBL_InitLogin                    DBL_InitLogin                    ;
extern tDBL_Datetime_2_Str_Ex            DBL_Datetime_2_Str_Ex            ;
extern tDBL_Str_2_Datetime_Ex            DBL_Str_2_Datetime_Ex            ;
extern tDBL_Datetime_2_YMDHMST_Ex        DBL_Datetime_2_YMDHMST_Ex        ;
extern tDBL_YMDHMST_2_Datetime_Ex        DBL_YMDHMST_2_Datetime_Ex        ;
extern tDBL_TestSysPermission            DBL_TestSysPermission            ;
extern tDBL_FindTopRec                   DBL_FindTopRec                   ;
extern tDBL_FindSet                      DBL_FindSet                      ;
extern tDBL_KeySQLIndexFields            DBL_KeySQLIndexFields            ;
extern tDBL_ImportFOB                    DBL_ImportFOB                    ;
extern tDBL_ExportFOB                    DBL_ExportFOB                    ;

// 111109 E
#endif
