#include "StdAfx.h"
/*******************************************************************************
*                C/FRONT Database C-API Version W1 3.60
*
*                   (c) 1987-2002 Navision a/s.
*					(c) CRJG 2003-2009
*******************************************************************************/
//Same layout as cf.h
// 170707 #include </alnatura/dienst/cf.h>
#include "cf.h"
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>


// 200803 A
extern void WriteLog(char*) ;
static char debugtext[256];
extern DBL_U8 testextraextra ;	// 111109
int logschreib =0 ;

int nureinmal = 0 ;	// 121109
// 200803 E

tDBL_Allow                  DBL_Allow                  = NULL;
tDBL_GetVersion             DBL_GetVersion             = NULL;
tDBL_ConnectServer          DBL_ConnectServer          = NULL;
tDBL_DisconnectServer       DBL_DisconnectServer       = NULL;
tDBL_OpenDatabase           DBL_OpenDatabase           = NULL;
tDBL_CloseDatabase          DBL_CloseDatabase          = NULL;
tDBL_NextCompany            DBL_NextCompany            = NULL;
tDBL_OpenCompany            DBL_OpenCompany            = NULL;
tDBL_CloseCompany           DBL_CloseCompany           = NULL;
tDBL_NextTable              DBL_NextTable              = NULL;
tDBL_OpenTable              DBL_OpenTable              = NULL;
tDBL_TableName              DBL_TableName              = NULL;
tDBL_TableNo                DBL_TableNo                = NULL;
tDBL_CloseTable             DBL_CloseTable             = NULL;
tDBL_LockTable              DBL_LockTable              = NULL;
tDBL_AllocRec               DBL_AllocRec               = NULL;
tDBL_FreeRec                DBL_FreeRec                = NULL;
tDBL_FindRec                DBL_FindRec                = NULL;
tDBL_NextRec                DBL_NextRec                = NULL;
tDBL_InsertRec              DBL_InsertRec              = NULL;
tDBL_DeleteRec              DBL_DeleteRec              = NULL;
tDBL_ModifyRec              DBL_ModifyRec              = NULL;
tDBL_RecCount               DBL_RecCount               = NULL;
tDBL_InitRec                DBL_InitRec                = NULL;
tDBL_CalcFields             DBL_CalcFields             = NULL;
tDBL_CalcSums               DBL_CalcSums               = NULL;
tDBL_KeyCount               DBL_KeyCount               = NULL;
tDBL_NextKey                DBL_NextKey                = NULL;
tDBL_KeySumFields           DBL_KeySumFields           = NULL;
tDBL_SetCurrentKey          DBL_SetCurrentKey          = NULL;
tDBL_GetCurrentKey          DBL_GetCurrentKey          = NULL;
tDBL_SetFilter              DBL_SetFilter              = NULL;
tDBL_GetFilter              DBL_GetFilter              = NULL;
tDBL_SetRange               DBL_SetRange               = NULL;
tDBL_GetRange               DBL_GetRange               = NULL;
tDBL_FieldCount             DBL_FieldCount             = NULL;
tDBL_NextField              DBL_NextField              = NULL;
tDBL_FieldType              DBL_FieldType              = NULL;
tDBL_FieldSize              DBL_FieldSize              = NULL;
tDBL_FieldNo                DBL_FieldNo                = NULL;
tDBL_FieldName              DBL_FieldName              = NULL;
tDBL_FieldOptionStr         DBL_FieldOptionStr         = NULL;
tDBL_FieldClass             DBL_FieldClass             = NULL;
tDBL_AssignField            DBL_AssignField            = NULL;
tDBL_GetFieldDataAddr       DBL_GetFieldDataAddr       = NULL;
tDBL_GetFieldDataSize       DBL_GetFieldDataSize       = NULL;
tDBL_GetFieldData           DBL_GetFieldData           = NULL;
tDBL_SelectLatestVersion    DBL_SelectLatestVersion    = NULL;
tDBL_BWT                    DBL_BWT                    = NULL;
tDBL_EWT                    DBL_EWT                    = NULL;
tDBL_AWT                    DBL_AWT                    = NULL;
tDBL_UserID                 DBL_UserID                 = NULL;
tDBL_CompanyName            DBL_CompanyName            = NULL;
tDBL_UserCount              DBL_UserCount              = NULL;
tDBL_Login                  DBL_Login                  = NULL;
tDBL_Field_2_Str            DBL_Field_2_Str            = NULL;
tDBL_BCD_2_Str              DBL_BCD_2_Str              = NULL;
tDBL_Str_2_BCD              DBL_Str_2_BCD              = NULL;
tDBL_YMD_2_Date             DBL_YMD_2_Date             = NULL;
tDBL_Date_2_YMD             DBL_Date_2_YMD             = NULL;
tDBL_HMST_2_Time            DBL_HMST_2_Time            = NULL;
tDBL_Time_2_HMST            DBL_Time_2_HMST            = NULL;
tDBL_Alpha_2_Str            DBL_Alpha_2_Str            = NULL;
tDBL_Str_2_Alpha            DBL_Str_2_Alpha            = NULL;
tDBL_SetExceptionHandler    DBL_SetExceptionHandler    = NULL;
tDBL_GetDatabaseName        DBL_GetDatabaseName        = NULL;
tDBL_CopyRec                DBL_CopyRec                = NULL;
tDBL_CmpRec                 DBL_CmpRec                 = NULL;
tDBL_SetMessageShowHandler  DBL_SetMessageShowHandler  = NULL;
tDBL_GetLastErrorCode       DBL_GetLastErrorCode       = NULL;
tDBL_CheckLicenseFile       DBL_CheckLicenseFile       = NULL;
tDBL_FieldDataOffset        DBL_FieldDataOffset        = NULL;
tDBL_Str_2_Date             DBL_Str_2_Date             = NULL;
tDBL_Date_2_Str             DBL_Date_2_Str             = NULL;
tDBL_Str_2_Time             DBL_Str_2_Time             = NULL;
tDBL_Time_2_Str             DBL_Time_2_Str             = NULL;
tDBL_ReleaseAllObjects      DBL_ReleaseAllObjects      = NULL;
tDBL_OpenTemporaryTable     DBL_OpenTemporaryTable     = NULL;
tDBL_FieldLen               DBL_FieldLen               = NULL;
tDBL_CreateTableBegin       DBL_CreateTableBegin       = NULL;
tDBL_AddTableField          DBL_AddTableField          = NULL;
tDBL_AddKey                 DBL_AddKey                 = NULL;
tDBL_CreateTable            DBL_CreateTable            = NULL;
tDBL_CreateTableEnd         DBL_CreateTableEnd         = NULL;
tDBL_DeleteTable            DBL_DeleteTable            = NULL;
tDBL_SetNavisionPath        DBL_SetNavisionPath        = NULL;
tDBL_LoadLicenseFile        DBL_LoadLicenseFile        = NULL;
tDBL_CryptPassword          DBL_CryptPassword          = NULL;
tDBL_DeleteRecs             DBL_DeleteRecs             = NULL;
tDBL_BCD_2_Double           DBL_BCD_2_Double           = NULL;
tDBL_Double_2_BCD           DBL_Double_2_BCD           = NULL;
tDBL_BCD_2_S32              DBL_BCD_2_S32              = NULL;
tDBL_S32_2_BCD              DBL_S32_2_BCD              = NULL;
tDBL_BCD_IsZero             DBL_BCD_IsZero             = NULL;
tDBL_BCD_IsNegative         DBL_BCD_IsNegative         = NULL;
tDBL_BCD_IsPositive         DBL_BCD_IsPositive         = NULL;
tDBL_BCD_Div                DBL_BCD_Div                = NULL;
tDBL_BCD_Mul                DBL_BCD_Mul                = NULL;
tDBL_BCD_Add                DBL_BCD_Add                = NULL;
tDBL_BCD_Sub                DBL_BCD_Sub                = NULL;
tDBL_BCD_Abs                DBL_BCD_Abs                = NULL;
tDBL_BCD_Neg                DBL_BCD_Neg                = NULL;
tDBL_BCD_Power              DBL_BCD_Power              = NULL;
tDBL_BCD_Sgn                DBL_BCD_Sgn                = NULL;
tDBL_BCD_Cmp                DBL_BCD_Cmp                = NULL;
tDBL_BCD_Trunc              DBL_BCD_Trunc              = NULL;
tDBL_BCD_Round              DBL_BCD_Round              = NULL;
tDBL_BCD_RoundUnit          DBL_BCD_RoundUnit          = NULL;
tDBL_BCD_Make               DBL_BCD_Make               = NULL;
tDBL_ConnectServerAndOpenDatabase DBL_ConnectServerAndOpenDatabase = NULL;
tDBL_Oem2AnsiBuff                 DBL_Oem2AnsiBuff                 = NULL;
tDBL_Ansi2OemBuff                 DBL_Ansi2OemBuff                 = NULL;
tDBL_UseCodeUnitsPermissions      DBL_UseCodeUnitsPermissions      = NULL;
tDBL_TableIsSame                  DBL_TableIsSame            = NULL;
tDBL_TableDup                     DBL_TableDup               = NULL;
tDBL_DupRec                       DBL_DupRec                 = NULL;
tDBL_SetView                      DBL_SetView                = NULL;
tDBL_GetView                      DBL_GetView                = NULL;
tDBL_SetTransfer                  DBL_SetTransfer            = NULL;
tDBL_DateFormula_2_Str            DBL_DateFormula_2_Str      = NULL;
tDBL_Str_2_DateFormula            DBL_Str_2_DateFormula      = NULL;
tDBL_S64_2_Str                    DBL_S64_2_Str              = NULL;
tDBL_Str_2_S64                    DBL_Str_2_S64              = NULL;
tDBL_S64_2_S32                    DBL_S64_2_S32              = NULL;
tDBL_S32_2_S64                    DBL_S32_2_S64              = NULL;
tDBL_Duration_2_Str               DBL_Duration_2_Str         = NULL;
tDBL_Str_2_Duration               DBL_Str_2_Duration         = NULL;
tDBL_Datetime_2_Str               DBL_Datetime_2_Str         = NULL;
tDBL_Str_2_Datetime               DBL_Str_2_Datetime         = NULL;
tDBL_Datetime_2_YMDHMST           DBL_Datetime_2_YMDHMST     = NULL;
tDBL_YMDHMST_2_Datetime           DBL_YMDHMST_2_Datetime     = NULL;
tDBL_GUID_2_Str                   DBL_GUID_2_Str             = NULL;
tDBL_Str_2_GUID                   DBL_Str_2_GUID             = NULL;

typedef DBL_U32             DBL_DLLHandleType;

typedef struct DLL
{
  char               DLLName[20];
  DBL_S16            DLLLoaded;
  DBL_DLLHandleType* DLLHandle;
} DLL;

typedef struct DLL* HDLLSESSION;

static HDLLSESSION hGlobalSession = NULL;

/* -----> 111109
void SessionInit(DBL_U8* DLLName)
{
  hGlobalSession = (HDLLSESSION)malloc(sizeof(DLL));
  strcpy(hGlobalSession->DLLName,(char *) DLLName);
  hGlobalSession->DLLLoaded = 0;
  DBL_Init();
}
 
< ---------- */


int SessionInit(DBL_U8* DLLName)
{
	// 111109 Um  den CAST-Operator ergaenz 
  hGlobalSession = (HDLLSESSION) malloc(sizeof(DLL));
  if (!hGlobalSession)
  {
	  if ( testextraextra )
	  {
			sprintf ( debugtext, "malloc ist schiefgegangen \n" ) ;
			WriteLog ( debugtext );


	  }
    return (-1);
  }
 // 111109 : gibbet noch nicht StringCbCopy(hGlobalSession->DLLName,sizeof(hGlobalSession->DLLName), DLLName);

	strcpy(hGlobalSession->DLLName,(char *) DLLName);


  hGlobalSession->DLLLoaded = 0;
  if ( testextraextra )
  {
	sprintf ( debugtext, "es folgt DBL_Init \n" ) ;
	WriteLog ( debugtext );
  }
  
  DBL_S32 retcos = DBL_Init();
	if ( testextraextra )
	{
		sprintf ( debugtext, "es war DBL_Init \n" ) ;
		WriteLog ( debugtext );
	}
  
  return (0);
}

static DBL_S32 DBL_FreeModule()
{
  HINSTANCE Handle = (HINSTANCE)(hGlobalSession->DLLHandle);

  if (!hGlobalSession->DLLLoaded)
    return 0;
  if (FreeLibrary(Handle))
  {
    hGlobalSession->DLLLoaded = 0;
    return 0;
  }
  
  return GetLastError();
}

static DBL_S32 DBL_LoadModule()
{
  if (hGlobalSession->DLLLoaded)
    DBL_FreeModule();
  
  hGlobalSession->DLLHandle = (DBL_DLLHandleType*)LoadLibrary((char const*)hGlobalSession->DLLName);
  
  if (hGlobalSession->DLLHandle)
  {
    hGlobalSession->DLLLoaded = 1;
    return 0;
  }  
  
  return GetLastError();
}


static DBL_S32 DBL_GetProcAddr(DBL_S32 ProcNo,
                               FARPROC *ProcAddr)
{
  HINSTANCE  Handle = (HINSTANCE)(hGlobalSession->DLLHandle);
  FARPROC    Addr   = GetProcAddress(Handle, (LPCSTR)ProcNo);

  if (Addr)
  {
    *ProcAddr = Addr;
    return 0;
  }
  return GetLastError();
}

static FARPROC DBL_GetFuncAddr(DBL_S16 ProgNo)
{
  DBL_S32   err;
  FARPROC   func;

  if ( !hGlobalSession->DLLLoaded ) 
    return(NULL);

  err = DBL_GetProcAddr(ProgNo, &func);
  if (err == 0)
    return(func);

  return(NULL);
}


static DBL_S32 LoadFunctionPtr()
{
  #define LOADFUNCPTR(a,b,c) if (NULL == ( b = ((a) DBL_GetFuncAddr(c)))) return(-1)

  LOADFUNCPTR(tDBL_Allow                     ,DBL_Allow                     ,ID_DBL_ALLOW);
  LOADFUNCPTR(tDBL_GetVersion                ,DBL_GetVersion                ,ID_DBL_GETVERSION);

  if (DBL_VERSION != DBL_GetVersion())
  {

	sprintf (debugtext ,"falsche Version ist : %d , erwartet : %d \n ", DBL_GetVersion(), DBL_VERSION  ) ;
	if ( testextraextra )
		WriteLog ( debugtext) ;

	sprintf (debugtext ,"Connect mit Version < 5.01" ) ;
	if ( ! nureinmal )
		WriteLog ( debugtext) ;
	nureinmal = 1 ;

//     return(-1);	// 121109 : dann geht es auch mit den alten Versionen durch ?!
  }

  LOADFUNCPTR(tDBL_ConnectServer             ,DBL_ConnectServer             ,ID_DBL_CONNECTSERVER);
  LOADFUNCPTR(tDBL_DisconnectServer          ,DBL_DisconnectServer          ,ID_DBL_DISCONNECTSERVER);
  LOADFUNCPTR(tDBL_OpenDatabase              ,DBL_OpenDatabase              ,ID_DBL_OPENDATABASE);
  LOADFUNCPTR(tDBL_CloseDatabase             ,DBL_CloseDatabase             ,ID_DBL_CLOSEDATABASE);
  LOADFUNCPTR(tDBL_OpenCompany               ,DBL_OpenCompany               ,ID_DBL_OPENCOMPANY);
  LOADFUNCPTR(tDBL_CloseCompany              ,DBL_CloseCompany              ,ID_DBL_CLOSECOMPANY);
  LOADFUNCPTR(tDBL_NextTable                 ,DBL_NextTable                 ,ID_DBL_NEXTTABLE);
  LOADFUNCPTR(tDBL_OpenTable                 ,DBL_OpenTable                 ,ID_DBL_OPENTABLE);
  LOADFUNCPTR(tDBL_TableName                 ,DBL_TableName                 ,ID_DBL_TABLENAME);
  LOADFUNCPTR(tDBL_CloseTable                ,DBL_CloseTable                ,ID_DBL_CLOSETABLE);
  LOADFUNCPTR(tDBL_LockTable                 ,DBL_LockTable                 ,ID_DBL_LOCKTABLE);
  LOADFUNCPTR(tDBL_FindRec                   ,DBL_FindRec                   ,ID_DBL_FINDREC);
  LOADFUNCPTR(tDBL_NextRec                   ,DBL_NextRec                   ,ID_DBL_NEXTREC);
  LOADFUNCPTR(tDBL_InsertRec                 ,DBL_InsertRec                 ,ID_DBL_INSERTREC);
  LOADFUNCPTR(tDBL_DeleteRec                 ,DBL_DeleteRec                 ,ID_DBL_DELETEREC);
  LOADFUNCPTR(tDBL_ModifyRec                 ,DBL_ModifyRec                 ,ID_DBL_MODIFYREC);
  LOADFUNCPTR(tDBL_RecCount                  ,DBL_RecCount                  ,ID_DBL_RECCOUNT);
  LOADFUNCPTR(tDBL_InitRec                   ,DBL_InitRec                   ,ID_DBL_INITREC);
  LOADFUNCPTR(tDBL_CalcFields                ,DBL_CalcFields                ,ID_DBL_CALCFIELDS);
  LOADFUNCPTR(tDBL_CalcSums                  ,DBL_CalcSums                  ,ID_DBL_CALCSUMS);
  LOADFUNCPTR(tDBL_KeyCount                  ,DBL_KeyCount                  ,ID_DBL_KEYCOUNT);
  LOADFUNCPTR(tDBL_NextKey                   ,DBL_NextKey                   ,ID_DBL_NEXTKEY);
  LOADFUNCPTR(tDBL_KeySumFields              ,DBL_KeySumFields              ,ID_DBL_KEYSUMFIELDS);
  LOADFUNCPTR(tDBL_SetCurrentKey             ,DBL_SetCurrentKey             ,ID_DBL_SETCURRENTKEY);
  LOADFUNCPTR(tDBL_GetCurrentKey             ,DBL_GetCurrentKey             ,ID_DBL_GETCURRENTKEY);
  LOADFUNCPTR(tDBL_SetFilter                 ,DBL_SetFilter                 ,ID_DBL_SETFILTER);
  LOADFUNCPTR(tDBL_GetFilter                 ,DBL_GetFilter                 ,ID_DBL_GETFILTER);
  LOADFUNCPTR(tDBL_SetRange                  ,DBL_SetRange                  ,ID_DBL_SETRANGE);
  LOADFUNCPTR(tDBL_GetRange                  ,DBL_GetRange                  ,ID_DBL_GETRANGE);
  LOADFUNCPTR(tDBL_FieldCount                ,DBL_FieldCount                ,ID_DBL_FIELDCOUNT);
  LOADFUNCPTR(tDBL_NextField                 ,DBL_NextField                 ,ID_DBL_NEXTFIELD);
  LOADFUNCPTR(tDBL_FieldType                 ,DBL_FieldType                 ,ID_DBL_FIELDTYPE);
  LOADFUNCPTR(tDBL_FieldSize                 ,DBL_FieldSize                 ,ID_DBL_FIELDSIZE);
  LOADFUNCPTR(tDBL_FieldName                 ,DBL_FieldName                 ,ID_DBL_FIELDNAME);
  LOADFUNCPTR(tDBL_FieldOptionStr            ,DBL_FieldOptionStr            ,ID_DBL_FIELDOPTIONSTR);
  LOADFUNCPTR(tDBL_FieldClass                ,DBL_FieldClass                ,ID_DBL_FIELDCLASS);
  LOADFUNCPTR(tDBL_AssignField               ,DBL_AssignField               ,ID_DBL_ASSIGNFIELD);
  LOADFUNCPTR(tDBL_GetFieldDataAddr          ,DBL_GetFieldDataAddr          ,ID_DBL_GETFIELDDATAADDR);
  LOADFUNCPTR(tDBL_GetFieldDataSize          ,DBL_GetFieldDataSize          ,ID_DBL_GETFIELDDATASIZE);
  LOADFUNCPTR(tDBL_GetFieldData              ,DBL_GetFieldData              ,ID_DBL_GETFIELDDATA);
  LOADFUNCPTR(tDBL_SelectLatestVersion       ,DBL_SelectLatestVersion       ,ID_DBL_SELECTLATESTVERSION);
  LOADFUNCPTR(tDBL_BWT                       ,DBL_BWT                       ,ID_DBL_BWT);
  LOADFUNCPTR(tDBL_EWT                       ,DBL_EWT                       ,ID_DBL_EWT);
  LOADFUNCPTR(tDBL_AWT                       ,DBL_AWT                       ,ID_DBL_AWT);
  LOADFUNCPTR(tDBL_UserID                    ,DBL_UserID                    ,ID_DBL_USERID);
  LOADFUNCPTR(tDBL_CompanyName               ,DBL_CompanyName               ,ID_DBL_COMPANYNAME);
  LOADFUNCPTR(tDBL_UserCount                 ,DBL_UserCount                 ,ID_DBL_USERCOUNT);
  LOADFUNCPTR(tDBL_Login                     ,DBL_Login                     ,ID_DBL_LOGIN);
  LOADFUNCPTR(tDBL_Field_2_Str               ,DBL_Field_2_Str               ,ID_DBL_FIELD_2_STR);
  LOADFUNCPTR(tDBL_BCD_2_Str                 ,DBL_BCD_2_Str                 ,ID_DBL_BCD_2_STR);
  LOADFUNCPTR(tDBL_Str_2_BCD                 ,DBL_Str_2_BCD                 ,ID_DBL_STR_2_BCD);
  LOADFUNCPTR(tDBL_YMD_2_Date                ,DBL_YMD_2_Date                ,ID_DBL_YMD_2_DATE);
  LOADFUNCPTR(tDBL_Date_2_YMD                ,DBL_Date_2_YMD                ,ID_DBL_DATE_2_YMD);
  LOADFUNCPTR(tDBL_HMST_2_Time               ,DBL_HMST_2_Time               ,ID_DBL_HMST_2_TIME);
  LOADFUNCPTR(tDBL_Time_2_HMST               ,DBL_Time_2_HMST               ,ID_DBL_TIME_2_HMST);
  LOADFUNCPTR(tDBL_Alpha_2_Str               ,DBL_Alpha_2_Str               ,ID_DBL_ALPHA_2_STR);
  LOADFUNCPTR(tDBL_Str_2_Alpha               ,DBL_Str_2_Alpha               ,ID_DBL_STR_2_ALPHA);
  LOADFUNCPTR(tDBL_NextCompany               ,DBL_NextCompany               ,ID_DBL_NEXTCOMPANY);
  LOADFUNCPTR(tDBL_TableNo                   ,DBL_TableNo                   ,ID_DBL_TABLENO);
  LOADFUNCPTR(tDBL_FieldNo                   ,DBL_FieldNo                   ,ID_DBL_FIELDNO);
  LOADFUNCPTR(tDBL_AllocRec                  ,DBL_AllocRec                  ,ID_DBL_ALLOCREC);
  LOADFUNCPTR(tDBL_FreeRec                   ,DBL_FreeRec                   ,ID_DBL_FREEREC);
  LOADFUNCPTR(tDBL_SetExceptionHandler       ,DBL_SetExceptionHandler       ,ID_DBL_SETEXCEPTIONHANDLER);
  LOADFUNCPTR(tDBL_GetDatabaseName           ,DBL_GetDatabaseName           ,ID_DBL_GETDATABASENAME);
  LOADFUNCPTR(tDBL_CopyRec                   ,DBL_CopyRec                   ,ID_DBL_COPYREC);
  LOADFUNCPTR(tDBL_CmpRec                    ,DBL_CmpRec                    ,ID_DBL_CMPREC);
  LOADFUNCPTR(tDBL_SetMessageShowHandler     ,DBL_SetMessageShowHandler     ,ID_DBL_SETMESSAGESHOWHANDLER);
  LOADFUNCPTR(tDBL_GetLastErrorCode          ,DBL_GetLastErrorCode          ,ID_DBL_GETLASTERRORCODE);
  LOADFUNCPTR(tDBL_CheckLicenseFile          ,DBL_CheckLicenseFile          ,ID_DBL_CHECKLICENSEFILE);
  LOADFUNCPTR(tDBL_FieldDataOffset           ,DBL_FieldDataOffset           ,ID_DBL_FIELDDATAOFFSET);
  LOADFUNCPTR(tDBL_Str_2_Date                ,DBL_Str_2_Date                ,ID_DBL_STR_2_DATE);
  LOADFUNCPTR(tDBL_Date_2_Str                ,DBL_Date_2_Str                ,ID_DBL_DATE_2_STR);
  LOADFUNCPTR(tDBL_Str_2_Time                ,DBL_Str_2_Time                ,ID_DBL_STR_2_TIME);
  LOADFUNCPTR(tDBL_Time_2_Str                ,DBL_Time_2_Str                ,ID_DBL_TIME_2_STR);
  LOADFUNCPTR(tDBL_ReleaseAllObjects         ,DBL_ReleaseAllObjects         ,ID_DBL_RELEASEALLOBJECTS);
  LOADFUNCPTR(tDBL_OpenTemporaryTable        ,DBL_OpenTemporaryTable        ,ID_DBL_OPENTEMPORARYTABLE);
  LOADFUNCPTR(tDBL_FieldLen                  ,DBL_FieldLen                  ,ID_DBL_FIELDLEN);
  LOADFUNCPTR(tDBL_CreateTableBegin          ,DBL_CreateTableBegin          ,ID_DBL_CREATETABLEBEGIN);
  LOADFUNCPTR(tDBL_AddTableField             ,DBL_AddTableField             ,ID_DBL_ADDTABLEFIELD);
  LOADFUNCPTR(tDBL_AddKey                    ,DBL_AddKey                    ,ID_DBL_ADDKEY);
  LOADFUNCPTR(tDBL_CreateTable               ,DBL_CreateTable               ,ID_DBL_CREATETABLE);
  LOADFUNCPTR(tDBL_CreateTableEnd            ,DBL_CreateTableEnd            ,ID_DBL_CREATETABLEEND);
  LOADFUNCPTR(tDBL_DeleteTable               ,DBL_DeleteTable               ,ID_DBL_DELETETABLE);
  LOADFUNCPTR(tDBL_SetNavisionPath           ,DBL_SetNavisionPath           ,ID_DBL_SETNAVISIONPATH);
  LOADFUNCPTR(tDBL_LoadLicenseFile           ,DBL_LoadLicenseFile           ,ID_DBL_LOADLICENSEFILE);
  LOADFUNCPTR(tDBL_CryptPassword             ,DBL_CryptPassword             ,ID_DBL_CRYPTPASSWORD);
  LOADFUNCPTR(tDBL_DeleteRecs                ,DBL_DeleteRecs                ,ID_DBL_DELETERECS);
  LOADFUNCPTR(tDBL_BCD_2_Double              ,DBL_BCD_2_Double              ,ID_DBL_BCD_2_DOUBLE);
  LOADFUNCPTR(tDBL_Double_2_BCD              ,DBL_Double_2_BCD              ,ID_DBL_DOUBLE_2_BCD);
  LOADFUNCPTR(tDBL_BCD_2_S32                 ,DBL_BCD_2_S32                 ,ID_DBL_BCD_2_S32);
  LOADFUNCPTR(tDBL_S32_2_BCD                 ,DBL_S32_2_BCD                 ,ID_DBL_S32_2_BCD);
  LOADFUNCPTR(tDBL_BCD_IsZero                ,DBL_BCD_IsZero                ,ID_DBL_BCD_ISZERO);
  LOADFUNCPTR(tDBL_BCD_IsNegative            ,DBL_BCD_IsNegative            ,ID_DBL_BCD_ISNEGATIVE);
  LOADFUNCPTR(tDBL_BCD_IsPositive            ,DBL_BCD_IsPositive            ,ID_DBL_BCD_ISPOSITIVE);
  LOADFUNCPTR(tDBL_BCD_Div                   ,DBL_BCD_Div                   ,ID_DBL_BCD_DIV);
  LOADFUNCPTR(tDBL_BCD_Mul                   ,DBL_BCD_Mul                   ,ID_DBL_BCD_MUL);
  LOADFUNCPTR(tDBL_BCD_Add                   ,DBL_BCD_Add                   ,ID_DBL_BCD_ADD);
  LOADFUNCPTR(tDBL_BCD_Sub                   ,DBL_BCD_Sub                   ,ID_DBL_BCD_SUB);
  LOADFUNCPTR(tDBL_BCD_Abs                   ,DBL_BCD_Abs                   ,ID_DBL_BCD_ABS);
  LOADFUNCPTR(tDBL_BCD_Neg                   ,DBL_BCD_Neg                   ,ID_DBL_BCD_NEG);
  LOADFUNCPTR(tDBL_BCD_Power                 ,DBL_BCD_Power                 ,ID_DBL_BCD_POWER);
  LOADFUNCPTR(tDBL_BCD_Sgn                   ,DBL_BCD_Sgn                   ,ID_DBL_BCD_SGN);
  LOADFUNCPTR(tDBL_BCD_Cmp                   ,DBL_BCD_Cmp                   ,ID_DBL_BCD_CMP);
  LOADFUNCPTR(tDBL_BCD_Trunc                 ,DBL_BCD_Trunc                 ,ID_DBL_BCD_TRUNC);
  LOADFUNCPTR(tDBL_BCD_Round                 ,DBL_BCD_Round                 ,ID_DBL_BCD_ROUND);
  LOADFUNCPTR(tDBL_BCD_RoundUnit             ,DBL_BCD_RoundUnit             ,ID_DBL_BCD_ROUNDUNIT);
  LOADFUNCPTR(tDBL_BCD_Make                  ,DBL_BCD_Make                  ,ID_DBL_BCD_MAKE);
  LOADFUNCPTR(tDBL_ConnectServerAndOpenDatabase,DBL_ConnectServerAndOpenDatabase,ID_DBL_DBL_CONNECTSERVERANDOPENDATABASE);
  LOADFUNCPTR(tDBL_Oem2AnsiBuff              ,DBL_Oem2AnsiBuff              ,ID_DBL_OEM2ANSIBUFF);
  LOADFUNCPTR(tDBL_Ansi2OemBuff              ,DBL_Ansi2OemBuff              ,ID_DBL_ANSI2OEMBUFF);
  LOADFUNCPTR(tDBL_UseCodeUnitsPermissions   ,DBL_UseCodeUnitsPermissions   ,ID_DBL_USECODEUNITSPERMISSIONS);
  LOADFUNCPTR(tDBL_TableIsSame               ,DBL_TableIsSame               ,ID_DBL_TABLEISSAME);
  LOADFUNCPTR(tDBL_TableDup                  ,DBL_TableDup                  ,ID_DBL_TABLEDUP);
  LOADFUNCPTR(tDBL_DupRec                    ,DBL_DupRec                    ,ID_DBL_DUPREC);
  LOADFUNCPTR(tDBL_SetView                   ,DBL_SetView                   ,ID_DBL_SETVIEW);
  LOADFUNCPTR(tDBL_GetView                   ,DBL_GetView                   ,ID_DBL_GETVIEW);
  LOADFUNCPTR(tDBL_SetTransfer               ,DBL_SetTransfer               ,ID_DBL_SETTRANSFER);
  LOADFUNCPTR(tDBL_DateFormula_2_Str         ,DBL_DateFormula_2_Str         ,ID_DBL_DATEFORMULA_2_STR);
  LOADFUNCPTR(tDBL_Str_2_DateFormula         ,DBL_Str_2_DateFormula         ,ID_DBL_STR_2_DATEFORMULA);
  LOADFUNCPTR(tDBL_S64_2_Str                 ,DBL_S64_2_Str                 ,ID_DBL_S64_2_STR);
  LOADFUNCPTR(tDBL_Str_2_S64                 ,DBL_Str_2_S64                 ,ID_DBL_STR_2_S64);
  LOADFUNCPTR(tDBL_S64_2_S32                 ,DBL_S64_2_S32                 ,ID_DBL_S64_2_S32);
  LOADFUNCPTR(tDBL_S32_2_S64                 ,DBL_S32_2_S64                 ,ID_DBL_S32_2_S64);
  LOADFUNCPTR(tDBL_Duration_2_Str            ,DBL_Duration_2_Str            ,ID_DBL_DURATION_2_STR);
  LOADFUNCPTR(tDBL_Str_2_Duration            ,DBL_Str_2_Duration            ,ID_DBL_STR_2_DURATION);
  LOADFUNCPTR(tDBL_Datetime_2_Str            ,DBL_Datetime_2_Str            ,ID_DBL_DATETIME_2_STR);
  LOADFUNCPTR(tDBL_Str_2_Datetime            ,DBL_Str_2_Datetime            ,ID_DBL_STR_2_DATETIME);
  LOADFUNCPTR(tDBL_Datetime_2_YMDHMST        ,DBL_Datetime_2_YMDHMST        ,ID_DBL_DATETIME_2_YMDHMST);
  LOADFUNCPTR(tDBL_YMDHMST_2_Datetime        ,DBL_YMDHMST_2_Datetime        ,ID_DBL_YMDHMST_2_DATETIME);
  LOADFUNCPTR(tDBL_GUID_2_Str                ,DBL_GUID_2_Str                ,ID_DBL_GUID_2_STR);
  LOADFUNCPTR(tDBL_Str_2_GUID                ,DBL_Str_2_GUID                ,ID_DBL_STR_2_GUID);

return(0);
}

static DBL_U8* memchr_bckwrd(const DBL_U8*  Dst,
                             DBL_S32        Size,
                             DBL_U8         Chr)
{
  DBL_U8 *pS = (DBL_U8*)Dst+Size;

  while(--pS >= Dst)
    if (*pS == Chr)
      return(pS);

  return(NULL);
}

void DBL_CDECL My_ExceptionHandler(DBL_S32  ErrorCode,
                                   DBL_BOOL IsFatal)
{
  char *Fatal = (IsFatal) ? " Fatal" : "";
  char *dbError = (19 == (ErrorCode/0x10000L)) ? "Database " : ""; /* Module No in high word */

  ErrorCode &= 0xffffL; /* Error Code in low word */

// 200803  printf ("Exception Handler called with%s %sError: %d.\n", Fatal, dbError, ErrorCode);
	sprintf (debugtext ,"Exception Handler called with%s %sError: %d. ", Fatal, dbError, ErrorCode);
	if ( logschreib || testextraextra )
	WriteLog ( debugtext) ;

  if(IsFatal)
  {
   
  	sprintf (debugtext ,"Beende Programm irregulaer");
	WriteLog ( debugtext) ;
	exit(ErrorCode);
  }	
}

void DBL_CDECL My_MessageShowHandler(DBL_U8 *Message,
                                     DBL_U32 MessageType,
                                     DBL_S32 ErrorCode)
{
  DBL_S32 LineWidth = 70;

	sprintf (debugtext ,"bin imshowhandler gelandet");
 	if ( testextraextra )
	WriteLog ( debugtext) ;

  ErrorCode = ErrorCode;
  MessageType = MessageType;

  do {
    DBL_U8 *Ret = (DBL_U8*)memchr(Message,'\r',strlen((char*)Message));

    if (Ret > Message+LineWidth) {
      DBL_U8 *Space = (DBL_U8*)memchr_bckwrd(Message,LineWidth,' ');
      if (Space)
        Ret = Space;
    }

    if (Ret)
      *Ret++ = 0;
// 200803 : Ausgabe in log-file
//     printf("%s\n",Message);
	sprintf ( debugtext , "%s", Message);
	if ( logschreib || testextraextra )
	WriteLog (debugtext);
    Message = Ret;
  } while(Message && *Message != 0);

//  fflush(stdout);

}

typedef void (DBL_CDECL *pFunc)(void);

void DBL_Exit(void)
{
  pFunc func;

  if (NULL != (func = (pFunc)DBL_GetFuncAddr(ID_DBL_EXIT)))
    (*func)();

  DBL_FreeModule();
  
}

DBL_S32 DBL_Init(void)
{
  {
    DBL_S32 err = DBL_LoadModule();
    if (err != 0)
      return(err);
  }
  
  {
    pFunc func;

    if (NULL != (func = (pFunc)DBL_GetFuncAddr(ID_DBL_INIT)))
    {
		if ( testextraextra )
		{
		sprintf (debugtext ,"dbl_init durchführen\n");
		WriteLog ( debugtext) ;
		}
		  (*func)();
	  }
    else
      return(-1);
    
		if ( testextraextra )
		{
			sprintf (debugtext ,"es folgt ladepointers\n");
			WriteLog ( debugtext) ;
		}

    DBL_S32 err2 = LoadFunctionPtr();

		if ( testextraextra )
		{
			sprintf (debugtext ," .%d. es folgt ladeexceptionhandlers \n", err2);
			WriteLog ( debugtext) ;
		}

    
	DBL_SetExceptionHandler(&My_ExceptionHandler);
		DBL_SetMessageShowHandler(&My_MessageShowHandler);
  }

  return 0;
}


