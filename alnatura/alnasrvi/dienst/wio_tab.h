/******************************************************************
*         C/FRONT Tabellenstrukturen fuer Projekt AlnaturA
*
*           (c) 2003 SETEC-GMBH fuer AlnaturA
*           (c) 2009 SETEC-GMBH fuer AlnaturA
*
*******************************************************************/
//  200803 : Integration Wio-BonText

#define OKAESE 0
#define OTONNE 1
#define OOG	   2

#define OLOEALL 0
#define OSENDPR 1
#define OSENDTA 2
#define OSENDUH 3

#define BASDIR  "c:\\bizerba\\"
#define TMPDIR  "c:\\bizerba\\tmp\\"	
#define KONVDIR "c:\\bizerba\\konv\\"
#define BINDIR  "c:\\bizerba\\bin\\"

#define SWCWIN  "swcwin.exe"
#define SXASCII "sxascii.exe"
#define DTOD    "dtod.exe"

#define PLTElen		511
// 300803

// max 511 Zeichen + stringende fuer PTElen moeglich -> Zielfeldgroessen beachten
// wir schicken hier aber nur den 1. text ( max 30 Stellen in der Daba )
// #define PLTElen		201
// wir legen mal max. 55 Zeichen je Zeile fest ->  Zielfeldgroessen beachten
#define ZTEXTlen	56
// Anzahl neuer Felder in der Daba, bevor Automatik abstoppt
#define ZUFELDCOUNT	8		// 120612 von 5 auf 8 erweitert


/* TESTEXTRA -->

#define SWCWIN  "nixmachwin.exe"
#define SXASCII  "nixmachascii.exe"
< ----- */

/* DER Prototyp aus "sample.c" 
FieldDefinitionType fieldsstruct[] = {
  { 10,  (DBL_U8*)"F1",  DBL_Type_S32,         4,  4  ,  NULL, 0 },                    // Integer            **
  { 20,  (DBL_U8*)"F2",  DBL_Type_STR,         30, 30+1, NULL, 0 },                    // Text30             **
  { 30,  (DBL_U8*)"F3",  DBL_Type_ALPHA,       10, 10+2, NULL, 0 },                    // Code10             **
  { 40,  (DBL_U8*)"F4",  DBL_Type_BCD,         12, 12,   NULL, 0 },                    // Decimal            **
  { 50,  (DBL_U8*)"F5",  DBL_Type_O32,         4,  4,    (DBL_U8*)"a,b,c,d,e", 0},     // Option (a,b,c,d,e) **
  { 60,  (DBL_U8*)"F6",  DBL_Type_BOOL,        4,  4,    NULL, 0 },                    // Boolean            **
  { 70,  (DBL_U8*)"F7",  DBL_Type_DATE,        4,  4,    NULL, 0 },                    // Date               **
  { 80,  (DBL_U8*)"F8",  DBL_Type_TIME,        4,  4,    NULL, 0 },                    // Time               **
  { 90,  (DBL_U8*)"F9",  DBL_Type_BLOB,        8,  8,    NULL, 0 },                    // BLOB               **
  { 100, (DBL_U8*)"F10", DBL_Type_DATEFORMULA, 32, 32,   NULL, 0 },                    // DateFormula        **
  { 110, (DBL_U8*)"F11", DBL_Type_S64,         8,  8,    NULL, 0 },                    // S64                **
  { 120, (DBL_U8*)"F12", DBL_Type_Duration,    8,  8,    NULL, 0 },                    // Duration           **
  { 130, (DBL_U8*)"F13", DBL_Type_Datetime,    8,  8,    NULL, 0 },                    // Datetime           **
  { 140, (DBL_U8*)"F14", DBL_Type_GUID,        16, 16,   NULL, 0 }                     // GUID               **
};

< ----- */

DBL_HTABLE  hMainTable;


// #### 50019 #### Tabelle WIO Einrichtung ##################################

#define WIO_EINRICHTUNG_TABNO 50019
#define WIO_EINRICHTUNG_TABNAM "WIO Einrichtung"
static  DBL_HTABLE hwio_einrichtung;
static  DBL_HREC  hRwio_einrichtung;

FieldDefinitionType fs_wio_einrichtung[] = {
  { 01,  (DBL_U8*)"Prim�rschl�ssel"			,DBL_Type_ALPHA,  10, 10+2, NULL, 0 },                    /* Code12             */
  { 05,  (DBL_U8*)"BarcodeStartSequenz"		,DBL_Type_ALPHA,   2,  2+2, NULL, 0 },                    /* Code2             */
  { 10,  (DBL_U8*)"Ziffer vor BAN"			,DBL_Type_ALPHA,   1,  1+2, NULL, 0 },                    /* Code1             */
  { 15,  (DBL_U8*)"OG-Filial-Preis Startnummer",DBL_Type_ALPHA,4,  4+2, NULL, 0 },					  /* Code4             */
  { 16,  (DBL_U8*)"OG-Filial-Preis Endnummer"  ,DBL_Type_ALPHA,4,  4+2, NULL, 0 },					  /* Code4             */
  { 20,  (DBL_U8*)"OG-Filial-Bez Startnummer"  ,DBL_Type_ALPHA,4,  4+2, NULL, 0 },					  /* Code4             */
  { 21,  (DBL_U8*)"OG-Filial-Bez Ednummer"     ,DBL_Type_ALPHA,4,  4+2, NULL, 0 },					  /* Code4             */
// Fuer die Rechtschreibefehler bin ich nicht zustaendig !!! -> Namenstest spaeter rausnehmen
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 }	  /* dummy  */

};




// #### 50020 #### Tabelle WIO Devices ##################################

#define WIO_DEVICES_TABNO 50020
#define WIO_DEVICES_TABNAM "WIO Devices"
static  DBL_HTABLE hwio_devices;
static  DBL_HREC  hRwio_devices;

FieldDefinitionType fs_wio_devices[] = {
  { 05,  (DBL_U8*)"Hostname"				,DBL_Type_ALPHA,  12, 12+2, NULL, 0 },                    /* Code12             */
  { 10,  (DBL_U8*)"Filiale"					,DBL_Type_ALPHA,  10, 10+2, NULL, 0 },                    /* Code10             */
  { 15,  (DBL_U8*)"Filialname"				,DBL_Type_STR,    30, 30+1, NULL, 0 },                    /* Text30             */
  { 20,  (DBL_U8*)"Waagentyp"				,DBL_Type_O32,     4,  4,    (DBL_U8*)"K�se,Tonne,OG", 0},  /* Optionen (a,b,c,d,e) */
  { 25,  (DBL_U8*)"Aktiv"					,DBL_Type_BOOL,    4,  4,    NULL, 0 },						/* Boolean            */
  { 30,  (DBL_U8*)"Letzte �bertragung OK"	,DBL_Type_BOOL,    4,  4,    NULL, 0 },						/* Boolean            */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 }	  /* dummy  */

};

// #### 50021 #### Tabelle WIO Tastenzuordnung ############################

#define WIO_TASTENZUORDNUNG_TABNO 50021
#define WIO_TASTENZUORDNUNG_TABNAM "WIO Tastenzuordnung"

static DBL_HTABLE hwio_tastenzuordnung;
static DBL_HREC  hRwio_tastenzuordnung;

FieldDefinitionType fs_wio_tastenzuordnung[] = {
  { 05,  (DBL_U8*)"BAN"				,DBL_Type_ALPHA,  3, 3+2, NULL, 0 },      /* Code3             */
  { 10,  (DBL_U8*)"Waagentaste"		,DBL_Type_ALPHA,  5, 5+2, NULL, 0 },       /* Code5             */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 }	  /* dummy  */

};


// #### 50022 #### Tabelle WIO Artikelpreisliste ############################

#define WIO_ARTPREISLISTE_TABNO 50022
#define WIO_ARTPREISLISTE_TABNAM "WIO Artikelpreisliste"

static DBL_HTABLE hwio_artpreisliste;
static DBL_HREC  hRwio_artpreisliste;

FieldDefinitionType fs_wio_artpreisliste[] = { 
  { 05,  (DBL_U8*)"BAN"						,DBL_Type_ALPHA,  4, 4+2, NULL, 0 },      /* Code4             */
  { 10,  (DBL_U8*)"Preistyp"				,DBL_Type_O32,    4,  4,    (DBL_U8*)"Normalpreis, Aktionspreis", 0},  /* Optionen (a,b,c,d,e) */
  { 15,  (DBL_U8*)"Startdatum"				,DBL_Type_DATE,   4,  4,    NULL, 0 },    /* Date               */
  { 20,  (DBL_U8*)"AAN"						,DBL_Type_ALPHA,  10, 10+2, NULL, 0 },    /* Code10             */
  { 25,  (DBL_U8*)"AA-Artikelbezeichnung"	,DBL_Type_STR,    30, 30+1, NULL, 1 },    /* Text30 ;FlowField  */
  { 30,  (DBL_U8*)"Artikelbez. Waage"		,DBL_Type_STR,    30, 30+1, NULL, 0 },    /* Text30             */
  { 35,  (DBL_U8*)"Grundpreis pro KG"		,DBL_Type_BCD,    12, 12,   NULL, 0 },    /* Decimal            */
  { 40,  (DBL_U8*)"Auslistung zum"			,DBL_Type_DATE,   4,  4,    NULL, 0 },    /* Date               */
  { 42,  (DBL_U8*)"Aktionscode"				,DBL_Type_ALPHA, 10, 10+2,  NULL, 0 },    /* Code10             */
  { 43,  (DBL_U8*)"Aktionsbezeichnung"		,DBL_Type_STR,   30, 30+1,  NULL, 1 },    /* Text30             */
  { 45,  (DBL_U8*)"Aktionsende am"			,DBL_Type_DATE,   4,  4,    NULL, 0 },    /* Date               */
  { 50,  (DBL_U8*)"Tara"					,DBL_Type_BCD,   12, 12,    NULL, 0 },    /* Decimal            */
  { 55,  (DBL_U8*)"HBZ in Tagen"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 60,  (DBL_U8*)"Kleinstmenge"			,DBL_Type_BOOL,   4,  4,    NULL, 0 },    /* Boolean            */
  { 90,  (DBL_U8*)"Letzte Korrektur am"		,DBL_Type_DATE,   4,  4,    NULL, 0 },    /* Date               */
  { 91,  (DBL_U8*)"Letzte Korrektur um"		,DBL_Type_TIME,   4,  4,    NULL, 0 },	  /* Time               */
  { 95,  (DBL_U8*)"BIZ holte am"			,DBL_Type_DATE,   4,  4,    NULL, 0 },    /* Date               */
  { 96,  (DBL_U8*)"BIZ holte um"			,DBL_Type_TIME,   4,  4,    NULL, 0 },	  /* Time               */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 }	  /* dummy  */
};


// ##### 50023 ##### Tabelle WIO Aufgaben ##################################

#define WIO_AUFGABEN_TABNO 50023
#define WIO_AUFGABEN_TABNAM "WIO Aufgaben"

static DBL_HTABLE hwio_aufgaben;
static DBL_HREC  hRwio_aufgaben;

FieldDefinitionType fs_wio_aufgaben[] = { 
	{ 05,  (DBL_U8*)"Zeilennr.",  DBL_Type_S32,         4,  4  ,  NULL, 0 },                    /* Integer            */
  { 10,  (DBL_U8*)"Tasktyp"				,DBL_Type_O32,     4,  4,    (DBL_U8*)"L�scheAlles,SendePreise,SendeTasten,SendeUhr", 0},  /* Optionen (a,b,c,d,e) */
  { 15,  (DBL_U8*)"Anwenden auf Waage"	,DBL_Type_ALPHA,  12, 12+2, NULL, 0 },                  /* Code12             */
  { 20,  (DBL_U8*)"Aufgabe aktiv"		,DBL_Type_BOOL,        4,  4,    NULL, 0 },             /* Boolean            */
  { 21,  (DBL_U8*)"Aufgabe aktiv am"	,DBL_Type_DATE,        4,  4,    NULL, 0 },             /* Date               */
  { 22,  (DBL_U8*)"Aufgabe aktiv um"	,DBL_Type_TIME,        4,  4,    NULL, 0 },				/* Time               */
	{ 30,  (DBL_U8*)"Aufgabe erledigt"	,DBL_Type_BOOL,        4,  4,    NULL, 0 },                    /* Boolean            */
  { 31,  (DBL_U8*)"Aufgabe erledigt am"	,DBL_Type_DATE,        4,  4,    NULL, 0 },                    /* Date               */
  { 32,  (DBL_U8*)"Aufgabe erledigt um"	,DBL_Type_TIME,        4,  4,    NULL, 0 },                    /* Time               */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 }	  /* dummy  */

}; 

// 200803
// ##### 50024 ##### Tabelle WIO BonText ##################################

// #define WIO_BONTEXT_TABNO 50018
// #define WIO_BONTEXT_TABNAM "WIO-BonText"

#define WIO_BONTEXT_TABNO 50024
#define WIO_BONTEXT_TABNAM "WIO BonText"

static DBL_HTABLE hwio_bontext;
static DBL_HREC  hRwio_bontext;

FieldDefinitionType fs_wio_bontext[] = { 
  { 01,  (DBL_U8*)"AAN"						,DBL_Type_ALPHA,  10, 10+2, NULL, 0 },    /* Code10             */
  { 101,  (DBL_U8*)"Zeile 1"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 102,  (DBL_U8*)"Zeile 2"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 103,  (DBL_U8*)"Zeile 3"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 104,  (DBL_U8*)"Zeile 4"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 105,  (DBL_U8*)"Zeile 5"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 106,  (DBL_U8*)"Zeile 6"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 107,  (DBL_U8*)"Zeile 7"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 108,  (DBL_U8*)"Zeile 8"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 109,  (DBL_U8*)"Zeile 9"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 110,  (DBL_U8*)"Zeile 10"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 111,  (DBL_U8*)"Zeile 11"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 112,  (DBL_U8*)"Zeile 12"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 113,  (DBL_U8*)"Zeile 13"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 114,  (DBL_U8*)"Zeile 14"	,DBL_Type_STR,   55, 55+1, NULL, 0 },   /* Text55   */
  { 115,  (DBL_U8*)"Zeile 15"	,DBL_Type_STR,   55, 55+1, NULL, 0 },	/* Text55   */

  // 300803
  { 200,  (DBL_U8*)"SG 0"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 201,  (DBL_U8*)"SG 1"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 202,  (DBL_U8*)"SG 2"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 203,  (DBL_U8*)"SG 3"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 204,  (DBL_U8*)"SG 4"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 205,  (DBL_U8*)"SG 5"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 206,  (DBL_U8*)"SG 6"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 207,  (DBL_U8*)"SG 7"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 208,  (DBL_U8*)"SG 8"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 209,  (DBL_U8*)"SG 9"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 210,  (DBL_U8*)"SG 10"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 211,  (DBL_U8*)"SG 11"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 212,  (DBL_U8*)"SG 12"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 213,  (DBL_U8*)"SG 13"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 214,  (DBL_U8*)"SG 14"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */
  { 215,  (DBL_U8*)"SG 15"			,DBL_Type_S32,    4,  4  ,  NULL, 0 },    /* Integer            */


  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 },	  /* dummy  */
  { 0x0, (DBL_U8*) NULL         ,0x0         ,  0x0,0x0  , NULL, 0x0 }	  /* dummy  */

}; 