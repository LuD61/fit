// dienst.cpp : Implementierung von WinMain


// Hinweis: Proxy/Stub-Information
//		Um eine eigene Proxy/Stub-DLL zu erstellen, 
//		f�hren Sie nmake -f dienstps.mk im Projektverzeichnis aus.

#include "stdafx.h"
#include "resource.h"
#include "initguid.h"
#include "dienst.h"

#include "dienst_i.c"


#include <stdio.h>


extern void alnaman(void) ;
extern void allesschliessen(void) ;
extern void WriteLog(char *) ;

FILE * pFile;
char buff[80];


void schreibeddienst( char * xbuf)
{

 	pFile = fopen ("c:\\bizerba\\tmp\\ddienst.tst","a+");
	fseek(pFile,(long)0,SEEK_END);
	fwrite (xbuf, 1, strlen(xbuf),pFile);
	fflush(pFile);
	fclose(pFile);
}


CServiceModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
END_OBJECT_MAP()


LPCTSTR FindOneOf(LPCTSTR p1, LPCTSTR p2)
{
	while (*p1 != NULL)
	{
		LPCTSTR p = p2;
		while (*p != NULL)
		{
			if (*p1 == *p++)
				return p1+1;
		}
		p1++;
	}
	return NULL;
}

// Obwohl einige dieser Funktionen umfangreich sind, wurden sie als Inline deklariert, da sie nur einmal benutzt werden

inline HRESULT CServiceModule::RegisterServer(BOOL bRegTypeLib, BOOL bService)
{
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr))
		return hr;

	// Entfernen Sie alle vorhergehenden Dienste, da diese auf die  
	// falsche Datei verweisen k�nnten
	Uninstall();

	// Serviceeintr�ge hinzuf�gen

	UpdateRegistryFromResource(IDR_Dienst, TRUE);

	// ID der Anwendung (AppID) f�r lokalen Server oder Dienst anpassen|
	CRegKey keyAppID;
	LONG lRes = keyAppID.Open(HKEY_CLASSES_ROOT, _T("AppID"));

	sprintf (buff, "Zugrifsrecht=%ld,  ERROR_SUCCESS=%ld\n",
		lRes,ERROR_SUCCESS);
	schreibeddienst ( buff );

	lRes = ERROR_SUCCESS; /* FF keine Zugrifsrechte pr�fen */

	if (lRes != ERROR_SUCCESS)
		return lRes;

	sprintf (buff, "vor key.Open:Zugrifsrecht=%ld,  ERROR_SUCCESS=%ld\n",
		lRes,ERROR_SUCCESS);
	schreibeddienst ( buff);


	CRegKey key;
	lRes = key.Open(keyAppID, _T("{752CD745-EAFE-11D5-B97D-0090272760B3}"));

	sprintf (buff, "2:Zugrifsrecht=%ld\n",lRes);
	schreibeddienst ( buff );

	lRes = ERROR_SUCCESS; /* FF keine Zugrifsrechte pr�fen */
	
	if (lRes != ERROR_SUCCESS)
		return lRes;
	key.DeleteValue(_T("LocalService"));
	
	if (bService)
	{
		key.SetValue(_T("dienst"), _T("LocalService"));
		key.SetValue(_T("-Service"), _T("ServiceParameters"));
		// Dienst erstellen
		Install();
	}

	// Objekteintr�ge hinzuf�gen
	hr = CComModule::RegisterServer(bRegTypeLib);

	CoUninitialize();
	return hr;
}

inline HRESULT CServiceModule::UnregisterServer()
{
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr))
		return hr;

	// Service-Eintr�ge entfernen
	UpdateRegistryFromResource(IDR_Dienst, FALSE);
	// Dienst entfernen
	Uninstall();
	// Objekteintr�ge entfernen
	CComModule::UnregisterServer();

	CoUninitialize();
	return S_OK;
}

inline void CServiceModule::Init(_ATL_OBJMAP_ENTRY* p, HINSTANCE h, UINT nServiceNameID)
{
	CComModule::Init(p, h);

	m_bService = TRUE;

	LoadString(h, nServiceNameID, m_szServiceName, sizeof(m_szServiceName) / sizeof(TCHAR));

    // Richten Sie den anf�nglichen Dienste-Status ein 
    m_hServiceStatus = NULL;
    m_status.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    m_status.dwCurrentState = SERVICE_STOPPED;
    m_status.dwControlsAccepted = SERVICE_ACCEPT_STOP;
    m_status.dwWin32ExitCode = 0;
    m_status.dwServiceSpecificExitCode = 0;
    m_status.dwCheckPoint = 0;
    m_status.dwWaitHint = 0;
}

LONG CServiceModule::Unlock()
{
	LONG l = CComModule::Unlock();
	if (l == 0 && !m_bService)
		PostThreadMessage(dwThreadID, WM_QUIT, 0, 0);
	return l;
}

BOOL CServiceModule::IsInstalled()
{
    BOOL bResult = FALSE;

    SC_HANDLE hSCM = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

    if (hSCM != NULL)
	{
        SC_HANDLE hService = ::OpenService(hSCM, m_szServiceName, SERVICE_QUERY_CONFIG);
        if (hService != NULL)
		{
            bResult = TRUE;
            ::CloseServiceHandle(hService);
        }
        ::CloseServiceHandle(hSCM);
    }
    return bResult;
}

inline BOOL CServiceModule::Install()
{
	if (IsInstalled())
		return TRUE;

    SC_HANDLE hSCM = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (hSCM == NULL)
	{
	schreibeddienst ("�ffnen des Dienst-Managers nicht m�glich\n" );
		MessageBox(NULL, _T("�ffnen des Dienst-Managers nicht m�glich"), m_szServiceName, MB_OK);
        return FALSE;
	}

    // Pfad der ausf�hrbaren Datei ermitteln
    TCHAR szFilePath[_MAX_PATH];
    ::GetModuleFileName(NULL, szFilePath, _MAX_PATH);

    SC_HANDLE hService = ::CreateService(
		hSCM, m_szServiceName, m_szServiceName,
		SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS,
		SERVICE_DEMAND_START, SERVICE_ERROR_NORMAL,
        szFilePath, NULL, NULL, _T("RPCSS\0"), NULL, NULL);

    if (hService == NULL)
	{
        ::CloseServiceHandle(hSCM);
	schreibeddienst ("�ffnen des Dienstes nicht m�glich\n" );

		MessageBox(NULL, _T("�ffnen des Dienstes nicht m�glich"), m_szServiceName, MB_OK);
		return FALSE;
    }

    ::CloseServiceHandle(hService);
    ::CloseServiceHandle(hSCM);
	return TRUE;
}

inline BOOL CServiceModule::Uninstall()
{
	if (!IsInstalled())
		return TRUE;

	SC_HANDLE hSCM = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

	if (hSCM == NULL)
	{
		schreibeddienst ("Couldn't open service manager\n" );
		MessageBox(NULL, _T("Couldn't open service manager"), m_szServiceName, MB_OK);
		return FALSE;
	}

	SC_HANDLE hService = ::OpenService(hSCM, m_szServiceName, SERVICE_STOP | DELETE);

	if (hService == NULL)
	{
		::CloseServiceHandle(hSCM);
		schreibeddienst ("Couldn't open service\n" );
		MessageBox(NULL, _T("Couldn't open service"), m_szServiceName, MB_OK);
		return FALSE;
	}
	SERVICE_STATUS status;
	::ControlService(hService, SERVICE_CONTROL_STOP, &status);

	BOOL bDelete = ::DeleteService(hService);
	::CloseServiceHandle(hService);
	::CloseServiceHandle(hSCM);

	if (bDelete)
		return TRUE;
	schreibeddienst ("Dienst konnte nicht gel�scht werden\n" );

	MessageBox(NULL, _T("Dienst konnte nicht gel�scht werden"), m_szServiceName, MB_OK);
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////
// Protokollfunktionen
void CServiceModule::LogEvent(LPCTSTR pFormat, ...)
{
    TCHAR    chMsg[256];
    HANDLE  hEventSource;
    LPTSTR  lpszStrings[1];
	va_list	pArg;

	va_start(pArg, pFormat);
	_vstprintf(chMsg, pFormat, pArg);
	va_end(pArg);

    lpszStrings[0] = chMsg;

	if (m_bService)
	{
	    /* Zugriffsnummer f�r die Verwendung mit ReportEvent() bereitstellen. */
		hEventSource = RegisterEventSource(NULL, m_szServiceName);
	    if (hEventSource != NULL)
	    {
	        /* In Ereignisprotokoll schreiben. */
	        ReportEvent(hEventSource, EVENTLOG_INFORMATION_TYPE, 0, 0, NULL, 1, 0, (LPCTSTR*) &lpszStrings[0], NULL);
	        DeregisterEventSource(hEventSource);
	    }
	}
	else
	{
		// Da wir nicht als Service ausgef�hrt werden, k�nnen wir den Fehler direkt an die Konsole ausgeben.
		_putts(chMsg);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Service-Start und Registrierung
inline void CServiceModule::Start()
{
    SERVICE_TABLE_ENTRY st[] =
	{
        { m_szServiceName, _ServiceMain },
        { NULL, NULL }
    };
//  if (!::StartServiceCtrlDispatcher(st))
    if (m_bService && !::StartServiceCtrlDispatcher(st))
	{
		schreibeddienst ("kein service-entry gefunden \n");
		m_bService = FALSE;
	}
	if (m_bService == FALSE)
		Run();
}

inline void CServiceModule::ServiceMain(DWORD /* dwArgc */, LPTSTR* /* lpszArgv */)
{
    // Registriern des Control-Request-Handlers
    m_status.dwCurrentState = SERVICE_START_PENDING;
    m_hServiceStatus = RegisterServiceCtrlHandler(m_szServiceName, _Handler);
    if (m_hServiceStatus == NULL)
	{
		schreibeddienst ("Behandlungsroutine ist nicht installiert\n");
        LogEvent(_T("Behandlungsroutine ist nicht installiert"));
        return;
    }
    SetServiceStatus(SERVICE_START_PENDING);

    m_status.dwWin32ExitCode = S_OK;
    m_status.dwCheckPoint = 0;
    m_status.dwWaitHint = 0;

    // Der Dienst ist beendet, sobald die Funktion Run ausgef�hrt ist.
    Run();
	allesschliessen () ;
	WriteLog ( "Dienst gestoppt" );
    SetServiceStatus(SERVICE_STOPPED);
	LogEvent(_T("Dienst haltet"));
}

inline void CServiceModule::Handler(DWORD dwOpcode)
{
		schreibeddienst ( "es folgt ne dienstanforderung \n" );
	switch (dwOpcode)
	{
	case SERVICE_CONTROL_STOP:
		SetServiceStatus(SERVICE_STOP_PENDING);
		PostThreadMessage(dwThreadID, WM_QUIT, 0, 0);
		break;
	case SERVICE_CONTROL_PAUSE:
		break;
	case SERVICE_CONTROL_CONTINUE:
		break;
	case SERVICE_CONTROL_INTERROGATE:
		break;
	case SERVICE_CONTROL_SHUTDOWN:
		break;
	default:
		LogEvent(_T("Fehlerhafte Dienstanforderung"));
		schreibeddienst ( "fehlerhafte Dienstanforderung \n" );
	}
}

void WINAPI CServiceModule::_ServiceMain(DWORD dwArgc, LPTSTR* lpszArgv)
{
	_Module.ServiceMain(dwArgc, lpszArgv);
}
void WINAPI CServiceModule::_Handler(DWORD dwOpcode)
{
	_Module.Handler(dwOpcode); 
}

void CServiceModule::SetServiceStatus(DWORD dwState)
{
	m_status.dwCurrentState = dwState;
	::SetServiceStatus(m_hServiceStatus, &m_status);
}

DWORD WINAPI myFunc(LPVOID lpParameter)
{

/* das war DER dummy
	FILE *pFile;
    int  In;
    char buff[64];
	In = 0;
    pFile = fopen ("c:\\bizerba\\tmp\\ddienst.xxx", "w+");
	fseek(pFile,(long)0,SEEK_END);

	while (In++ < 200)
	{
		sprintf (buff," Zeile %d\n", In);
		fwrite (buff, 1, strlen(buff), pFile);
	    fflush(pFile);
		Sleep(1000);
	}
	fflush(pFile);
	fclose(pFile);

	exit (0);
< ---- */

	FILE *pFile;
    char buff[64];
    pFile = fopen ("c:\\bizerba\\tmp\\ddienst.tst", "a");
	fseek(pFile,(long)0,SEEK_END);

	sprintf ( buff, "\n starte alna-ablaeufe \n");
	fwrite (buff, 1, strlen(buff), pFile);
    fflush(pFile);
	fclose(pFile);


	alnaman() ;

	
	return 0;
}

void CServiceModule::Run()
{
	HRESULT hr;

	_Module.dwThreadID = GetCurrentThreadId();

	HRESULT hRes = CoInitialize(NULL);
//  Bei der Verwendung von NT 4.0 k�nnen Sie folgenden Aufruf verwenden
//		anstatt freie Threads innerhalb der EXE zuzulassen.
//  Dies bedeutet, dass Aufrufe �ber einen zuf�lligen RPC-Thread eingehen
//	HRESULT hRes = CoInitializeEx(NULL, COINIT_MULTITHREADED);

	_ASSERTE(SUCCEEDED(hr));

	// Hier wird ein NULL DACL zur Verf�gung gestellt, damit jeder Zugriff erhalten kann.
	CSecurityDescriptor sd;
	sd.InitializeFromThreadToken();
	hr = CoInitializeSecurity(sd, -1, NULL, NULL,
		RPC_C_AUTHN_LEVEL_PKT, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, NULL);
	_ASSERTE(SUCCEEDED(hr));

	hr = _Module.RegisterClassObjects(CLSCTX_LOCAL_SERVER | CLSCTX_REMOTE_SERVER, REGCLS_MULTIPLEUSE);
	_ASSERTE(SUCCEEDED(hr));

	LogEvent(_T("Service started"));
    SetServiceStatus(SERVICE_RUNNING);

    schreibeddienst ( "vor CreateThread\n");

	unsigned long threadId;
	HANDLE hTread;

	hTread = CreateThread(0, 0, myFunc, 0, 0, &threadId);
	ResumeThread(hTread);

	schreibeddienst ( "vor GetMessage\n");
	fwrite (buff, 1, strlen(buff),pFile);
	fflush(pFile);
	MSG msg;
	while (GetMessage(&msg, 0, 0, 0))
	{
    	sprintf (buff, "Message=%d erhalten\n",msg.message);
		schreibeddienst ( buff );
		DispatchMessage(&msg);
	}
	_Module.RevokeClassObjects();

	CoUninitialize();
}


/////////////////////////////////////////////////////////////////////////////
//
extern "C" int WINAPI _tWinMain(HINSTANCE hInstance, 
	HINSTANCE /*hPrevInstance*/, LPTSTR lpCmdLine, int /*nShowCmd*/)
{

	
	// jedesmal neu starten 

	pFile = fopen ("c:\\bizerba\\tmp\\ddienst.tst","w+");
	fclose ( pFile ) ;
	schreibeddienst ( "Start winmain\n");

	
	lpCmdLine = GetCommandLine(); //dieser Linie f�r_ATL_MIN_CRT notwendig
	_Module.Init(ObjectMap, hInstance, IDS_SERVICENAME);
	_Module.m_bService = TRUE;

	TCHAR szTokens[] = _T("-/");

	LPCTSTR lpszToken = FindOneOf(lpCmdLine, szTokens);
	while (lpszToken != NULL)
	{
		schreibeddienst ( (char *)lpszToken );
		schreibeddienst ( "\n" );
		if (lstrcmpi(lpszToken, _T("UnregServer"))==0)
			return _Module.UnregisterServer();

		// Lokaler Server
		if (lstrcmpi(lpszToken, _T("RegServer"))==0)
			return _Module.RegisterServer(TRUE, FALSE);
		
		// Dienst
		if (lstrcmpi(lpszToken, _T("Service"))==0)
			return _Module.RegisterServer(TRUE, TRUE);
		
		lpszToken = FindOneOf(lpszToken, szTokens);
	}

	// Arbeiten wir als Dienst oder als lokaler Server
	CRegKey keyAppID;
	LONG lRes = keyAppID.Open(HKEY_CLASSES_ROOT, _T("AppID"));
	if (lRes != ERROR_SUCCESS)
	{
		schreibeddienst ( "HKEY_CLASSES_ROOT : AppID nicht gefunden \n");

		return lRes;
	}

	CRegKey key;
	lRes = key.Open(keyAppID, _T("{752CD745-EAFE-11D5-B97D-0090272760B3}"));
	if (lRes != ERROR_SUCCESS)
	{
		schreibeddienst ( "{752CD745-EAFE-11D5-B97D-0090272760B3} nicht gefunden \n");

		return lRes;
	}

	TCHAR szValue[_MAX_PATH];
	DWORD dwLen = _MAX_PATH;
	lRes = key.QueryValue(szValue, _T("LocalService"), &dwLen);

	_Module.m_bService = FALSE;
	if (lRes == ERROR_SUCCESS)
		_Module.m_bService = TRUE;

	_Module.Start();

    // An dieser Stelle des Ablaufs wurde der Dienst bereits beendet
    return _Module.m_status.dwWin32ExitCode;
}

