#include <share.h>
#include "properties.h"

CProperties::CProperties(void)
{
	FileName = "";
	SectionName = "";
}

CProperties::~CProperties(void)
{
}

CString CProperties::GetValue (CString Name)
{
	FirstPosition ();
	CPropertyItem *p;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Name.CompareNoCase (Name) == 0)
		{
			return p->Value;
		}
	}
	return Name;
}

CString CProperties::GetValues (CString Name)
{
	CPropertyItem *p;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		CString pName = p->Name.Left (Name.GetLength ()); 
		if (pName.CompareNoCase (Name) == 0)
		{
			return p->Value;
		}
	}
	return "";
}

CString CProperties::GetName (CString Value)
{
	FirstPosition ();
	CPropertyItem *p;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Value.CompareNoCase (Value) == 0)
		{
			return p->Name;
		}
	}
	return Value;
}

int CProperties::Find (CString Name)
{
	FirstPosition ();
	CPropertyItem *p;
	int i = 0;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Name.CompareNoCase (Name) == 0)
		{
			return i;
		}
		i ++;
	}
	return -1;
}

int CProperties::FindValue (CString Value)
{
	FirstPosition ();
	CPropertyItem *p;
	int i = 0;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Value.CompareNoCase (Value) == 0)
		{
			return i;
		}
		i ++;
	}
	return -1;
}

void CProperties::Set (CString Name, CString Value, int idx)
{
	CPropertyItem *p;
	p = (CPropertyItem *) Get (idx);
	if (p != NULL)
	{
		p->Name = Name;
		p->Value = Value;
	}
}

void CProperties::Set (CString& Record)
{
	CToken t;
	t.SetSep ("=");
	t = Record;
	if (t.GetAnzToken () < 1) return;

	CString Name = t.GetToken (0);
    CString Value = "";
	if (t.GetAnzToken () > 1)
	{
		Value = t.GetToken (1);
	}
	Name.Trim ();
	Value.Trim ();

	CPropertyItem *p = new CPropertyItem (Name, Value);
    Add (p); 
}

void CProperties::SetWithSep (CString& Record, LPTSTR Sep)
{
	CToken t;
	t.SetSep (Sep);
	t = Record;
	if (t.GetAnzToken () < 1) return;

	CString Name = t.GetToken (0);
    CString Value = "";
	if (t.GetAnzToken () > 1)
	{
		Value = t.GetToken (1);
	}
	Name.Trim ();
	Value.Trim ();

	CPropertyItem *p = new CPropertyItem (Name, Value);
    Add (p); 
}

void CProperties::SetWithSep (CString& Record, LPTSTR Sep1, LPTSTR Sep2)
{
	CToken t;
	t.SetSep (Sep1);
	t = Record;
	for (int i = 0; i < t.GetAnzToken (); i ++)
	{
		CString Record2 = t.GetToken (i);
		SetWithSep (Record2, Sep2);
	}
}

BOOL CProperties::FindSection (FILE *fp)
{
	char buffer [512];

	while (fgets (buffer, 511, fp))
	{
		CString Record = buffer;
		Record.Trim ();
		if (Record.Left (1) != "[") continue;
		int len = Record.Find ("]");
		if (len < 0) continue;
		CString Name = Record.Mid (1, len - 1);
		if (Name.Trim () == SectionName) 
		{
			return TRUE;
		}
	}
    return FALSE;
}

void CProperties::Load ()
{
	char buffer [512];
	if (FileName == "") return;

	FILE *fp = _fsopen (FileName.GetBuffer (), "r", _SH_DENYNO);
	if (fp == NULL) return;


	BOOL TestSection = FALSE;
	if (SectionName != "")
	{
		TestSection = TRUE;
		if (!FindSection (fp)) return;
	}

	DestroyAll ();
	while (fgets (buffer, 511, fp))
	{
		CString Record = buffer;
		Record.Trim ();
		if (TestSection && (Record.Left (1) == "["))
		{
			break;
		}

		Set (Record);
	}

	fclose (fp);
}