// #include "stdafx.h"
#include <stdio.h>
#include <process.h>
#include <windows.h>
#include <time.h>

#define MAXLINES 2000 

static char FileName[130] ; 
static int jrhstart = 70;
static int jrh1 = 1900;
static int jrh2 = 2000;

void dat_zeit_log(char * datzeit)
{

 time_t timer;
 struct tm *ltime;

   time (&timer);
 // timer += ioffset * 86400L ;
 
   ltime = localtime (&timer);
   if (ltime->tm_year < 100)
   {
             if (ltime->tm_year < jrhstart)
             {
                  ltime->tm_year += jrh2;
             }
             else
             {
                  ltime->tm_year += jrh1;
             }
	}
	else
	{
             ltime->tm_year += jrh1;
     
	} 

	ltime->tm_mon += 1 ;
 
	sprintf( datzeit, "%02d.%02d.%04d %02d:%02d:%02d",
		ltime->tm_mday, ltime->tm_mon, ltime->tm_year, 
		ltime->tm_hour, ltime->tm_min, ltime->tm_sec);
}

void WriteFirstLine (char *FileText)
/**
Satz in erste Zeile einer Datei schreiben.
**/
{
	   FILE *in;
       FILE *out; 
	   int pid;
	   int z;
	   char fname [256];
	   char buffer [256];

	   pid = getpid ();
       sprintf (fname, "touchwrite.%d", pid);
	   CopyFile (FileName, fname, FALSE);
       out = fopen (FileName, "w");
	   if (out == NULL) return;
	   fprintf (out, "%s\n", FileText);
	   in = fopen (fname, "r");
	   if (in == NULL) 
	   {
		   fclose (out);
		   return;
	   }
	   z = 0;
	   while (fgets (buffer, 255, in))
	   {
		   fputs (buffer, out);
		   if (z >= MAXLINES) break;
		   z ++;
	   }
	   fclose (in);
	   fclose (out);
	   unlink (fname);
}

void WriteLog (char *iBASDIR,char *Text)
/**
Text mit Datum, Uhrzeit, Fehlerstatus und Geraetenummer schreiben
**/
{
		sprintf ( FileName ,"%s\\alnasrvmaster.log" , iBASDIR ) ;
          char datzeit [32];
	      char FileText [1032];

          dat_zeit_log ( datzeit );

          sprintf (FileText, "%s %s", datzeit, Text);
          WriteFirstLine (FileText);
}
       
