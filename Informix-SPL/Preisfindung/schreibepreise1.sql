
drop table kun_art_bezug;
create table kun_art_bezug
  (
    mdn integer,
    fil integer,
    kun_fil integer,
    kun integer,
    a integer,
    lieferdat char(10),
    sa smallint,
    pr_ek decimal (8,4),
    pr_vk decimal (8,4)
  ) in fit_dat
;
Insert into kun_art_bezug (mdn, fil, kun_fil, kun, a,lieferdat,sa,pr_ek,pr_vk)
    select  ipr.mdn,
            0,
            0,
            kun.kun,
            ipr.a,
            date(sysdate),
            0,0,0
    from ipr, kun
            where ipr.mdn=kun.mdn and
                kun.pr_stu = ipr.pr_gr_stuf and
                kun.pr_lst = ipr.kun_pr and
                kun.kun = ipr.kun
;



--drop function schreibepreise1;
CREATE PROCEDURE schreibepreise1 ( )
		RETURNING  smallint as flg_ok;
	DEFINE GLOBAL dmdn      integer  default 0;
	DEFINE GLOBAL dfil      integer  default 0;
	DEFINE GLOBAL dkun_fil      smallint  default 0;
	DEFINE GLOBAL dkun      integer  default 0;
	DEFINE GLOBAL da      REAL  default 0;
	DEFINE GLOBAL cdatum      char(10) default "";
	DEFINE GLOBAL flg_ok      smallint  default 0;     -- 1 wenn alles ok
	DEFINE GLOBAL sqlerrd2      integer  default 0;
	DEFINE GLOBAL dsa          smallint default 0;    -- Sonderangebotskennzeichen
	DEFINE GLOBAL dpr_ek       real     default 0;     -- Ergebnis EK
	DEFINE GLOBAL dpr_vk       real     default 0;     -- Ergebnis VK

	IF dkun_fil = 1 THEN
		let flg_ok = -1;
--		RETURN flg_ok,dsa,pr_ek,pr_vk;
	END IF;  -- kun_fil = 1 noch nicht integriert !!!

	let flg_ok = 0;
	let dsa = 0;
	let dpr_ek = 0;
	let dpr_vk = 0;

	set isolation to dirty read;

	-- ======Preise holen ==================
FOREACH cursor FOR
      SELECT mdn,fil,kun_fil,kun,a,lieferdat INTO dmdn,dfil,dkun_fil,dkun,da,cdatum FROM kun_art_bezug
         WHERE 1 = 1
	CALL wapreise (dmdn,dfil,dkun_fil,dkun,da,cdatum )
                RETURNING flg_ok,dsa,dpr_ek, dpr_vk;
  update kun_art_bezug set  sa = dsa,
          pr_ek = dpr_ek,
          pr_vk = dpr_vk
          where mdn = dmdn and
          fil = dfil and kun_fil = dkun_fil and
          kun = dkun and
          a = da and lieferdat = cdatum ;
  END FOREACH;

		RETURN flg_ok ;
END PROCEDURE
;
execute function schreibepreise1 () ;		
