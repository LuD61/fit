DROP PROCEDURE schreibepreise ;
CREATE PROCEDURE schreibepreise ( dpid integer,
                         dmdn integer,
			 dfil integer,
			 dkun_fil smallint,
			 dkun integer,
			 da REAL,
			 cdatum char(10)) 
		RETURNING  real as pr_vk;
	DEFINE GLOBAL flg_ok      smallint  default 0;     -- 1 wenn alles ok
	DEFINE GLOBAL sqlerrd2      integer  default 0;    
	DEFINE GLOBAL sa          smallint default 0;    -- Sonderangebotskennzeichen
	DEFINE GLOBAL pr_ek       real     default 0;     -- Ergebnis EK
	DEFINE GLOBAL pr_vk       real     default 0;     -- Ergebnis VK
	
	DEFINE  dvk_pr_i like ipr.vk_pr_i;
	DEFINE  dld_pr like ipr.ld_pr; 
	DEFINE GLOBAL ipr_vk_pr_i       real     default 0;     -- Standardpreis 
	DEFINE GLOBAL ipr_ld_pr       real     default 0;     -- Standardpreis
	DEFINE GLOBAL iv_pr_vk_pr_i       real     default 0;     -- Standardpreis Vortabelle 
	DEFINE GLOBAL iv_pr_ld_pr       real     default 0;     -- Standardpreis Vortabelle

	
	DEFINE GLOBAL kun_pr_stu  integer  default 0;     -- kun.Preisgruppenstufe
	DEFINE GLOBAL kun_pr_lst  integer  default 0;     -- kun.Preisliste
	DEFINE GLOBAL kun_pr_hier smallint  default 0;    -- kun.PreisHirachie  0= alles(bis Preisgruppe 0), 1, nur bis PreisGruppe,  2= bis Preisliste       
	DEFINE GLOBAL kun_kun integer  default 0;      

	DEFINE GLOBAL akiprgrstp_vk_pr_i       real     default 0;     -- Aktion Preisgruppe 
	DEFINE GLOBAL akiprgrstp_ld_pr       real     default 0;     -- Aktion Preisgruppe
	DEFINE GLOBAL akikunlsp_vk_pr_i       real     default 0;     -- Aktion Kundenpreisliste 
	DEFINE GLOBAL akikunlsp_ld_pr       real     default 0;     -- Aktion Kundenpreisliste
	DEFINE GLOBAL akikunprp_vk_pr_i       real     default 0;     -- Aktion Kunde direkt 
	DEFINE GLOBAL akikunprp_ld_pr       real     default 0;     -- Aktion Kunde direkt	
	IF dkun_fil = 1 THEN
		let flg_ok = -1;
--		RETURN flg_ok,sa,pr_ek,pr_vk;  
	END IF;  -- kun_fil = 1 noch nicht integriert !!! 
	
	let flg_ok = 0;
	let sa = 0;
	let pr_ek = 0;
	let pr_vk = 0;
	let kun_pr_stu = 0;
	let kun_pr_lst = 0;
	let kun_kun = 0;
        let ipr_vk_pr_i = 0;
	let ipr_ld_pr = 0;
	let iv_pr_vk_pr_i = 0;
	let iv_pr_ld_pr = 0;
	let dvk_pr_i = 0;
	let dld_pr = 0;
	
	set isolation to dirty read;

	-- ======Preise holen ==================
	CALL wapreise (dmdn,dfil,dkun_fil,dkun,da,cdatum ) RETURNING flg_ok,sa,ipr_ld_pr,ipr_vk_pr_i;

	let pr_vk = ipr_vk_pr_i;
	let pr_ek = ipr_ld_pr;

	delete from preise_spl  where pid = dpid and a = da ;
  insert into preise_spl (pid, kun, lieferdat, a, flg_ok, sa, pr_ek, pr_vk) values (dpid, dkun, cdatum, da, flg_ok, sa, pr_ek, pr_vk) ;

		RETURN pr_vk ;
END PROCEDURE
;		
execute PROCEDURE wapreise (1,0,0,1010,12,"28.08.2013")  ;
		