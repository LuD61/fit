DROP FUNCTION preise_iv_pr;
CREATE FUNCTION preise_iv_pr (dmdn integer,
			 dfil smallint,
			 dkun_fil smallint,
			 dkun integer,
			 da REAL,
			 cdatum char(10))
		RETURNING smallint as flg_ok, smallint as sa, real as pr_ek, real as pr_vk;
	DEFINE GLOBAL flg_ok      smallint  default 0;     -- 1 wenn alles ok
	DEFINE GLOBAL sqlerrd2      integer  default 0;    
	DEFINE GLOBAL sa          smallint default 0;    -- Sonderangebotskennzeichen
	DEFINE GLOBAL pr_ek       real     default 0;     -- Ergebnis EK
	DEFINE GLOBAL pr_vk       real     default 0;     -- Ergebnis VK
	
	DEFINE  dvk_pr_i like ipr.vk_pr_i;
	DEFINE  dld_pr like ipr.ld_pr; 
	DEFINE GLOBAL ipr_vk_pr_i       real     default 0;     -- Standardpreis 
	DEFINE GLOBAL ipr_ld_pr       real     default 0;     -- Standardpreis
	DEFINE GLOBAL iv_pr_vk_pr_i       real     default 0;     -- Standardpreis Vortabelle 
	DEFINE GLOBAL iv_pr_ld_pr       real     default 0;     -- Standardpreis Vortabelle

	
	DEFINE GLOBAL kun_pr_stu  integer  default 0;     -- kun.Preisgruppenstufe
	DEFINE GLOBAL kun_pr_lst  integer  default 0;     -- kun.Preisliste
	DEFINE GLOBAL kun_pr_hier smallint  default 0;    -- kun.PreisHirachie  0= alles(bis Preisgruppe 0), 1, nur bis PreisGruppe,  2= bis Preisliste       
	DEFINE GLOBAL kun_kun integer  default 0;      


	
	-- =======Gruppen holen: normalerweise in der aufrufenden SPL schon pasiert, dann ist kun_kun > 0==================
	set isolation to dirty read;
	IF kun_kun = 0 THEN
		let flg_ok = -1;
		let kun_kun, kun_pr_hier, kun_pr_stu, kun_pr_lst  = (select kun.kun, kun.pr_hier, kun.pr_stu, kun.pr_lst from kun where mdn = dmdn and fil = dfil and kun = dkun);
		IF kun_kun != dkun THEN RETURN flg_ok,sa,pr_ek,pr_vk;  END IF;
		IF kun_pr_lst = -9 THEN RETURN flg_ok,sa,pr_ek,pr_vk;  END IF;  -- Sonderablauf mit Preisliste = -9 noch nicht integriert !!! 
		IF kun_pr_hier > 2 THEN RETURN flg_ok,sa,pr_ek,pr_vk;  END IF;  -- Preishierachie >2  noch nicht integriert !!! 
	END IF	
	
	-- =======Standardpreise holen ==================
	--     ==== Kunde direkt ====
	IF kun_pr_hier < 3 THEN
		let dvk_pr_i, dld_pr  = (select vk_pr_i, ld_pr from iv_pr where mdn = dmdn and pr_gr_stuf = kun_pr_stu and kun_pr = kun_pr_lst and kun = dkun and a = da and gue_ab <= cdatum );
		-- sqlca.sqlerrd2: Returns the number of rows processed by selects, inserts, deletes, updates, EXECUTE PROCEDURE statements, and EXECUTE FUNCTION statements
		let sqlerrd2 = DBINFO ('sqlca.sqlerrd2');
		IF sqlerrd2 > 0 THEN
			let ipr_vk_pr_i = dvk_pr_i;
			let ipr_ld_pr = dld_pr;
			let flg_ok = 14;
		END IF	
	END IF	
		
	--     ==== KundenPreisliste ====
	IF flg_ok < 4 THEN  -- flg_ok = 4 : es existiert schon ein Kundenspez. Standardpreis
		IF kun_pr_hier < 3 THEN
				let dvk_pr_i, dld_pr  = (select vk_pr_i, ld_pr from iv_pr where mdn = dmdn and pr_gr_stuf = kun_pr_stu and kun_pr = kun_pr_lst and kun = 0 and a = da and gue_ab <= cdatum  );
				IF DBINFO ('sqlca.sqlerrd2') > 0 THEN
					let ipr_vk_pr_i = dvk_pr_i;
					let ipr_ld_pr = dld_pr;
					let flg_ok = 13;
				END IF	
		END IF	
	END IF	
	--     ==== KundenPreisgruppe ====
	IF flg_ok < 3 THEN -- flg_ok = 3 : es existiert schon ein Kundenlistenspez. Standardpreis
		IF kun_pr_hier < 2 THEN
				let dvk_pr_i, dld_pr  = (select vk_pr_i, ld_pr from iv_pr where mdn = dmdn and pr_gr_stuf = kun_pr_stu and kun_pr = 0 and kun = 0 and a = da and gue_ab <= cdatum  );
				IF DBINFO ('sqlca.sqlerrd2') > 0 THEN
					let ipr_vk_pr_i = dvk_pr_i;
					let ipr_ld_pr = dld_pr;
					let flg_ok = 12;
				END IF	
		END IF	
	END IF	
	--     ==== Preisgruppe 0 ====
	IF flg_ok < 2 THEN -- flg_ok = 2 : es existiert schon ein KundenPreisGruppen. Standardpreis
		IF kun_pr_hier = 0 THEN
				let dvk_pr_i, dld_pr  = (select vk_pr_i, ld_pr from iv_pr where mdn = dmdn and pr_gr_stuf = 0 and kun_pr = 0 and kun = 0 and a = da and gue_ab <= cdatum  );
				IF DBINFO ('sqlca.sqlerrd2') > 0 THEN
					let ipr_vk_pr_i = dvk_pr_i;
					let ipr_ld_pr = dld_pr;
					let flg_ok = 11;
				END IF	
		END IF	
	END IF	
	let pr_vk = ipr_vk_pr_i;
	let pr_ek = ipr_ld_pr;


		RETURN flg_ok,sa, pr_ek, pr_vk ;
END FUNCTION
;		
execute FUNCTION preise_iv_pr (1,0,0,1010,12,"28.08.2013") ;
		
		