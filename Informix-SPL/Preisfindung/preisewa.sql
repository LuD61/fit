DROP FUNCTION wapreise;
CREATE FUNCTION wapreise (dmdn integer,
			 dfil integer,
			 dkun_fil smallint,
			 dkun integer,
			 da REAL,
			 cdatum char(10))
		RETURNING smallint as flg_ok, smallint as sa, real as pr_ek, real as pr_vk;
	DEFINE GLOBAL flg_ok      smallint  default 0;     -- 1 wenn alles ok
	DEFINE GLOBAL sqlerrd2      integer  default 0;    
	DEFINE GLOBAL sa          smallint default 0;    -- Sonderangebotskennzeichen
	DEFINE GLOBAL pr_ek       real     default 0;     -- Ergebnis EK
	DEFINE GLOBAL pr_vk       real     default 0;     -- Ergebnis VK
	
	DEFINE  dvk_pr_i like ipr.vk_pr_i;
	DEFINE  dld_pr like ipr.ld_pr; 
	DEFINE GLOBAL ipr_vk_pr_i       real     default 0;     -- Standardpreis 
	DEFINE GLOBAL ipr_ld_pr       real     default 0;     -- Standardpreis
	DEFINE GLOBAL iv_pr_vk_pr_i       real     default 0;     -- Standardpreis Vortabelle 
	DEFINE GLOBAL iv_pr_ld_pr       real     default 0;     -- Standardpreis Vortabelle

	
	DEFINE GLOBAL kun_pr_stu  integer  default 0;     -- kun.Preisgruppenstufe
	DEFINE GLOBAL kun_pr_lst  integer  default 0;     -- kun.Preisliste
	DEFINE GLOBAL kun_pr_hier smallint  default 0;    -- kun.PreisHirachie  0= alles(bis Preisgruppe 0), 1, nur bis PreisGruppe,  2= bis Preisliste       
	DEFINE GLOBAL kun_kun integer  default 0;      

	DEFINE GLOBAL akiprgrstp_vk_pr_i       real     default 0;     -- Aktion Preisgruppe 
	DEFINE GLOBAL akiprgrstp_ld_pr       real     default 0;     -- Aktion Preisgruppe
	DEFINE GLOBAL akikunlsp_vk_pr_i       real     default 0;     -- Aktion Kundenpreisliste 
	DEFINE GLOBAL akikunlsp_ld_pr       real     default 0;     -- Aktion Kundenpreisliste
	DEFINE GLOBAL akikunprp_vk_pr_i       real     default 0;     -- Aktion Kunde direkt 
	DEFINE GLOBAL akikunprp_ld_pr       real     default 0;     -- Aktion Kunde direkt	
	IF dkun_fil = 1 THEN
		let flg_ok = -1;
		RETURN flg_ok,sa,pr_ek,pr_vk;  
	END IF;  -- kun_fil = 1 noch nicht integriert !!! 
	
	let flg_ok = 0;
	let sa = 0;
	let pr_ek = 0;
	let pr_vk = 0;
	let kun_pr_stu = 0;
	let kun_pr_lst = 0;
	let kun_kun = 0;
    let ipr_vk_pr_i = 0;
	let ipr_ld_pr = 0;
	let iv_pr_vk_pr_i = 0;
	let iv_pr_ld_pr = 0;
	let dvk_pr_i = 0;
	let dld_pr = 0;
	
	set isolation to dirty read;
	-- =======Gruppen holen ==================
	let kun_kun, kun_pr_hier, kun_pr_stu, kun_pr_lst  = (select kun.kun, kun.pr_hier, kun.pr_stu, kun.pr_lst from kun where mdn = dmdn and fil = dfil and kun = dkun);
	IF kun_kun != dkun THEN RETURN -11,sa,pr_ek,pr_vk;  END IF;
	IF kun_pr_lst = -9 THEN RETURN -9,sa,pr_ek,pr_vk;  END IF;  -- Sonderablauf mit Preisliste = -9 noch nicht integriert !!! 
	IF kun_pr_hier > 2 THEN RETURN -2,sa,pr_ek,pr_vk;  END IF;  -- Preishierachie >2  noch nicht integriert !!! 

	-- ======Standardpreise holen ==================
		CALL preise_ipr (dmdn,dfil,dkun_fil,dkun,da,cdatum ) RETURNING flg_ok,sa,ipr_ld_pr,ipr_vk_pr_i;
		
	-- ======Standardpreise aus Vortabelle  holen ==================
		CALL preise_iv_pr (dmdn,dfil,dkun_fil,dkun,da,cdatum ) RETURNING flg_ok,sa,ipr_ld_pr,ipr_vk_pr_i;
	
	-- ====== Preisgruppenaktionen  holen ==================
	--	CALL aktion_akiprgrstp (dmdn,dfil,dkun_fil,dkun,da,cdatum ) RETURNING flg_ok,sa,akiprgrstp_ld_pr,akiprgrstp_vk_pr_i;

	
	let pr_vk = ipr_vk_pr_i;
	let pr_ek = ipr_ld_pr;


		RETURN flg_ok,sa, pr_ek, pr_vk ;
END FUNCTION
;		
execute FUNCTION wapreise (1,0,0,1010,12,"28.08.2013")  ;

oder innerhalb eines Views :  

drop view vi_dummy;

create view vi_dummy
(
     flg_ok,
    sa,
    pr_ek,
    pr_vk
)

as select  flg_ok,sa,pr_ek,pr_vk from table
(function wapreise(1,0,0,9999 ,1,"15.10.2014"))
vtab1 (flg_ok,sa,pr_ek,pr_vk) , kun
;
select * from vi_dummy where kun = 1
