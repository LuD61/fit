create table "fit".preise_spl 
  (
    pid integer,
    kun integer,
    lieferdat date,
    a decimal(13,0),
    flg_ok smallint,
    sa smallint,
    pr_ek decimal (12,4),
    pr_vk decimal (12,4)
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".a_mat from "public";

create unique index "fit".i01preise_spl on "fit".preise_spl (pid,a);




