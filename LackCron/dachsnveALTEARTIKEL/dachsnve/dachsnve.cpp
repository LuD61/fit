// dachsnve.cpp : Definiert das Klassenverhalten f�r die Anwendung.
//

#include "stdafx.h"
#include "dachsnve.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "dachsnveDoc.h"
// #include "pakdruView.h"
#include "dachsnveDlg.h"
#include "DruckDialog.h"
#include "dbClass.h"
#include "token.h"	// 030913

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

DB_CLASS dbClass ;
long globrepair ;	// 030913

// CdachsnveApp

BEGIN_MESSAGE_MAP(CdachsnveApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CdachsnveApp::OnAppAbout)
	ON_COMMAND(ID_DATEI_DRUCKEREINRICHTUNG, &CdachsnveApp::OnDruckWahl)
	// Dateibasierte Standarddokumentbefehle
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
	// Standarddruckbefehl "Seite einrichten"
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()


// CdachsnveApp-Erstellung

CdachsnveApp::CdachsnveApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance positionieren
}


// Das einzige CdachsnveApp-Objekt

CdachsnveApp theApp;


// CdachsnveApp-Initialisierung

BOOL CdachsnveApp::InitInstance()
{
	// InitCommonControlsEx() ist f�r Windows XP erforderlich, wenn ein Anwendungsmanifest
	// die Verwendung von ComCtl32.dll Version 6 oder h�her zum Aktivieren
	// von visuellen Stilen angibt. Ansonsten treten beim Erstellen von Fenstern Fehler auf.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Legen Sie dies fest, um alle allgemeinen Steuerelementklassen einzubeziehen,
	// die Sie in Ihrer Anwendung verwenden m�chten.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// OLE-Bibliotheken initialisieren
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standardinitialisierung
	// Wenn Sie diese Features nicht verwenden und die Gr��e
	// der ausf�hrbaren Datei verringern m�chten, entfernen Sie
	// die nicht erforderlichen Initialisierungsroutinen.
	// �ndern Sie den Registrierungsschl�ssel, unter dem Ihre Einstellungen gespeichert sind.
	// TODO: �ndern Sie diese Zeichenfolge entsprechend,
	// z.B. zum Namen Ihrer Firma oder Organisation.
	SetRegistryKey(_T("Vom lokalen Anwendungs-Assistenten generierte Anwendungen"));
	LoadStdProfileSettings(4);  // Standard INI-Dateioptionen laden (einschlie�lich MRU)
	// Dokumentvorlagen der Anwendung registrieren. Dokumentvorlagen
	//  dienen als Verbindung zwischen Dokumenten, Rahmenfenstern und Ansichten.
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_DACHSNVETYPE,
		RUNTIME_CLASS(CdachsnveDoc),
		RUNTIME_CLASS(CChildFrame), // Benutzerspezifischer MDI-Child-Rahmen
//		RUNTIME_CLASS(CdachsnveView));
		RUNTIME_CLASS(CdachsnveDlg));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	// Haupt-MDI-Rahmenfenster erstellen
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
	{
		delete pMainFrame;
		return FALSE;
	}
	m_pMainWnd = pMainFrame;
	// Rufen Sie DragAcceptFiles nur auf, wenn eine Suffix vorhanden ist.
	//  In einer MDI-Anwendung ist dies unmittelbar nach dem Festlegen von m_pMainWnd erforderlich


	// Befehlszeile auf Standardumgebungsbefehle �berpr�fen, DDE, Datei �ffnen
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

// 030913 : mit Parameter "repair"

    char *px;

	LPSTR CommandLine = GetCommandLine ();

	CToken Token (CommandLine, " ");

	globrepair = 0 ;
	char hilfrepair [199] ;	// das muss reichen

    px = Token.NextToken ();

	if ((px = Token.NextToken ()) != NULL)
		if ( px[0] != '\0' && px[0] != ' ' )
				sprintf ( hilfrepair , "%s" , px ) ;


	if ( ! strncmp (hilfrepair, "repair" , 6 ) || ! strncmp (hilfrepair, "REPAIR" , 6 ) )
		globrepair = 1 ;


	// Verteilung der in der Befehlszeile angegebenen Befehle. Gibt FALSE zur�ck, wenn
	// die Anwendung mit /RegServer, /Register, /Unregserver oder /Unregister gestartet wurde.
/* ----> GrJ : weg damit 
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
< ---- */
		// Das Hauptfenster ist initialisiert und kann jetzt angezeigt und aktualisiert werden.
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	CWinApp::OnFileNew();	// GrJ jetzt geht es los

	return TRUE;
}



// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// Anwendungsbefehl zum Ausf�hren des Dialogfelds
void CdachsnveApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

void CdachsnveApp::OnDruckWahl()
{
	CDruckDialog DruckerWahl ;
	DruckerWahl.DruckWahl();
}
// CdachsnveApp-Meldungshandler

