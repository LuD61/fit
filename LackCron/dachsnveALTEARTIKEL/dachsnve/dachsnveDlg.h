#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "BelListCtrl.h"



// CdachsnveDlg-Formularansicht

class CdachsnveDlg : public CFormView
{
	DECLARE_DYNCREATE(CdachsnveDlg)

protected:
	CdachsnveDlg();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CdachsnveDlg();

public:
	enum { IDD = IDD_DACHSNVEDLG };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedAllesdruck();
// public:
 	afx_msg BOOL PreTranslateMessage(LPMSG) ;
//	afx_msg BOOL NoListCtrl (CWnd *) ;

public:
	CEdit m_mandant;
public:
	CString v_mandant;
public:
	CEdit m_mdnname;
public:
	CString v_mdnname;
public:
	CEdit m_datum;
public:
	CString v_datum;
public:
	afx_msg void OnEnKillfocusMandant();
public:
	afx_msg void OnEnKillfocusDatum();
public:
	CBelListCtrl m_list1;
//	CListCtrl m_list1;
public:
	void ReadMdn(void) ;
	virtual BOOL Read(void);
	virtual BOOL Write(void);
	virtual BOOL ReadList(void) ;

	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);
	virtual void DestroyRows(CVector &Rows);
	virtual BOOL InList (LSK_CLASS &lsk_class) ;	// Input nur dummy ?!
	virtual void StapelDruck (void) ;

	CVector DbRows;
	CVector ListRows;

	CFillList FillList ;

public:
	afx_msg void OnListBegintrack(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnListEnddrag(NMHDR *pNMHDR, LRESULT *pResult);
public:
	CEdit m_beleg;
public:
	long v_beleg;
public:
	afx_msg void OnEnKillfocusBeleg();
	afx_msg void OnBnClickedRepbutton();
	CButton m_repbutton;
};





