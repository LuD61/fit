// dachsnve.h : Hauptheaderdatei f�r die dachsnve-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"       // Hauptsymbole


// CdachsnveApp:
// Siehe dachsnve.cpp f�r die Implementierung dieser Klasse
//

class CdachsnveApp : public CWinApp
{
public:
	CdachsnveApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	afx_msg void OnDruckWahl();
	DECLARE_MESSAGE_MAP()
};

extern CdachsnveApp theApp;

extern long globrepair;		// 030913