// pakdruView.h : Schnittstelle der Klasse CpakdruView
//


#pragma once


class CpakdruView : public CView
{
protected: // Nur aus Serialisierung erstellen
	CpakdruView();
	DECLARE_DYNCREATE(CpakdruView)

// Attribute
public:
	CpakdruDoc* GetDocument() const;

// Vorgänge
public:

// Überschreibungen
public:
	virtual void OnDraw(CDC* pDC);  // Überschrieben, um diese Ansicht darzustellen
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~CpakdruView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // Debugversion in pakdruView.cpp
inline CpakdruDoc* CpakdruView::GetDocument() const
   { return reinterpret_cast<CpakdruDoc*>(m_pDocument); }
#endif

