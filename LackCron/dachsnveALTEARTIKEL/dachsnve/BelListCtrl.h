#pragma once
#include <vector>
#include "editlistctrl.h"
// #include "schlaklkk.h"
// #include "schlaklkp.h"
#include "dbClass.h"
#include "ls.h"
// #include "ChoiceZuBasis.h"

#define MAXLISTROWS 30

class CBelListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

/* --->
	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
	};
< ---- */
	enum LISTPOS
	{
		POSKUNU		= 1,
		POSBEZ1		= 2,
		POSRECH		= 3,
		POSLS		= 4,
		POSART		= 5,
		POSNVE      = 6,
//		POSAKTIV	= 5,
	};

    int PosKunu;
    int PosBez1;
	int PosRech;
	int PosLs;
	int PosArt;
	int PosNve;
//	int PosAktiv;

    int *Position[7];


// void SetWerte ( long , double, long ) ;

// void Zuschlagsetzen ( void ) ;

//	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	BOOL ActiveListRow;
	short m_mdn;
	short mdn;

//	int oldsel;
	std::vector<BOOL> vSelect;
//	CVector ZuBasisCombo ;
//	CVector PrGrStufCombo;
//	CVector KunPrCombo;
//	CChoiceKun *ChoiceKun;
//	BOOL ModalChoiceKun;
//	BOOL KunChoiceStat;
//	CChoiceIKunPr *ChoiceIKunPr;
//	BOOL ModalChoiceIKunPr;
//	BOOL IKunPrChoiceStat;
//	CChoiceZuBasis *ChoiceZuBasis;
//	BOOL ModalChoiceZuBasis;
//	BOOL ZuBasisChoiceStat;
	CVector ListRows;

//	KUN_CLASS Kun;
//	ADR_CLASS KunAdr;
//	I_KUN_PRK_CLASS I_kun_prk;
//	IPRGRSTUFK_CLASS Iprgrstufk;

	CBelListCtrl(void);
	~CBelListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
//	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
//	void FillZuBasisCombo( CVector&);
//	void FillPrGrStufCombo (CVector&);
//	void FillKunPrCombo (CVector&);
	void OnChoice ();
//	void OnKunChoice (CString &);
//    void OnIKunPrChoice (CString& Search);
//    void OnPrGrStufChoice (CString& Search);
//	void OnKey9 ();
//    void ReadKunName ();
//    void ReadKunPr ();
//    void ReadPrGrStuf ();
    void GetColValue (int row, int col, CString& Text);
//    void TestIprIndex ();
	void ScrollPositions (int pos);
	BOOL LastCol ();
};
