// dachsnveDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "dachsnve.h"
#include "dachsnveDlg.h"
#include "strfuncs.h"
#include "dbClass.h"
#include "mdn.h"
#include "adr.h"
#include "ls.h"
#include "fracht.h"
#include "trapo.h"
#include "RepDialog.h"
#include "sys_par.h"	// 120914

#include "FillList.h"

extern DB_CLASS dbClass ;
extern MDN_CLASS mdn_class ;
extern ADR_CLASS adr_class ;
extern LSK_CLASS lsk_class ;
// extern FRACHT_CLASS fracht_class ;
extern TRAPOPSPM_CLASS trapopspm_class ;
extern void DruckeEtiketten(char * nvenve, long ils ) ;

char bufh [256] ;

// CdachsnveDlg

IMPLEMENT_DYNCREATE(CdachsnveDlg, CFormView)

CdachsnveDlg::CdachsnveDlg()
	: CFormView(CdachsnveDlg::IDD)
	, v_mandant(_T(""))
	, v_mdnname(_T(""))
	, v_datum(_T(""))
	, v_beleg(0)
{

}

CdachsnveDlg::~CdachsnveDlg()
{
}

void CdachsnveDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MANDANT, m_mandant);
	DDX_Text(pDX, IDC_MANDANT, v_mandant);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_DATUM, m_datum);
	DDX_Text(pDX, IDC_DATUM, v_datum);
	DDX_Control(pDX, IDC_LIST1, m_list1);
	DDX_Control(pDX, IDC_BELEG, m_beleg);
	DDX_Text(pDX, IDC_BELEG, v_beleg);
	DDV_MinMaxLong(pDX, v_beleg, 0, 99999999);
	DDX_Control(pDX, IDC_REPBUTTON, m_repbutton);
}

BEGIN_MESSAGE_MAP(CdachsnveDlg, CFormView)
	ON_BN_CLICKED(IDCANCEL, &CdachsnveDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CdachsnveDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_ALLESDRUCK, &CdachsnveDlg::OnBnClickedAllesdruck)
	ON_EN_KILLFOCUS(IDC_MANDANT, &CdachsnveDlg::OnEnKillfocusMandant)
	ON_EN_KILLFOCUS(IDC_DATUM, &CdachsnveDlg::OnEnKillfocusDatum)
	ON_NOTIFY(HDN_BEGINTRACK, 0, &CdachsnveDlg::OnListBegintrack)
	ON_NOTIFY(HDN_ENDDRAG, 0, &CdachsnveDlg::OnListEnddrag)
	ON_EN_KILLFOCUS(IDC_BELEG, &CdachsnveDlg::OnEnKillfocusBeleg)
	ON_BN_CLICKED(IDC_REPBUTTON, &CdachsnveDlg::OnBnClickedRepbutton)
END_MESSAGE_MAP()


// CdachsnveDlg-Diagnose

#ifdef _DEBUG
void CdachsnveDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CdachsnveDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void CdachsnveDlg::ReadMdn (void) 
{
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//	gibbet nich		PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich 		PrevDlgCtrl();
	}
		UpdateData (FALSE) ;
}

// CdachsnveDlg-Meldungshandler


void CdachsnveDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.


	dbClass.opendbase (_T("bws"));

// 120914
	spdachsnosmt2 = atoi ( sys_par_class.sys_par_holen ( "dachsnosmt2" ))  ;

	spdachsneu = atoi ( sys_par_class.sys_par_holen ( "dachsneu" ))  ;


	// Cursoren createn ....

	/* --->
	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_list1.SetImageList (&image, LVSIL_SMALL);   
	}
< ---- */

// 	m_list1.SetImageList (&image, LVSIL_SMALL);   
	FillList = m_list1;
	FillList.SetStyle (LVS_REPORT);
	if (m_list1.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Kun-Nr"), 1, 60, LVCFMT_RIGHT);
	FillList.SetCol (_T("K-Name"), 2, 150, LVCFMT_LEFT);
	FillList.SetCol (_T("Rech-Nr"), 3, 60, LVCFMT_RIGHT);
	FillList.SetCol (_T("LS-Nr"), 4, 60, LVCFMT_RIGHT);
	FillList.SetCol (_T("Artikel"), 5, 50, LVCFMT_RIGHT);
	FillList.SetCol (_T("NVE"), 6, 150, LVCFMT_LEFT);
//	m_list1.ColType.Add (new CColType (5, m_list1.CheckBox)) ;


	/* --->
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);
	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
//	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
//	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
//	MdnGrid.Add (c_MdnChoice);

	SchlklknrGrid.Create (this, 1, 2);
    SchlklknrGrid.SetBorder (0, 0);
    SchlklknrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Schlklknr = new CCtrlInfo (&m_schlklknr, 0, 0, 1, 1);
	SchlklknrGrid.Add (c_Schlklknr);
< ----- */
/* --->
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);
< ----- */

	/* --->
	ButtonGrid.Create (this, 5, 5);
    ButtonGrid.SetBorder (0, 0);
    ButtonGrid.SetCellHeight (20);
    ButtonGrid.SetGridSpace (0, 10);
< -- */

/* --->
	CCtrlInfo *c_Cancel = new CCtrlInfo (&m_Cancel, 0, 0, 1, 1);
	ButtonGrid.Add (c_Cancel);
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 1, 1, 1);
	ButtonGrid.Add (c_Save);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ButtonGrid.Add (c_Delete);
	CCtrlInfo *c_Insert = new CCtrlInfo (&m_Insert, 0, 3, 1, 1);
	ButtonGrid.Add (c_Insert);
< --- */

/* --->
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_GrundPreis = new CCtrlInfo (&GrundPreis, DOCKRIGHT, 0, 5, 1); 
	CtrlGrid.Add (c_GrundPreis);

	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_LA_bz1  = new CCtrlInfo (&m_LA_bz1, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LA_bz1);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 1, 2, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 1, 3, 3, 1); 
	CtrlGrid.Add (c_A_bz2);
	CCtrlInfo *c_ButtonGrid = new CCtrlInfo (&ButtonGrid, DOCKRIGHT, 4, 1, 3); 
	CtrlGrid.Add (c_ButtonGrid);
	CCtrlInfo *c_List  = new CCtrlInfo (&m_List, 0, 4, DOCKRIGHT, DOCKBOTTOM); 
//	c_List->rightspace = 125;
	c_List->rightspace = RightListSpace;
	CtrlGrid.Add (c_List);
< ----- */

/* --->
	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();
< ---- */
/* --->
	memcpy (&Schlaklkk.schlaklkk, &schlaklkk_null, sizeof (schlaklkk));

	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
< --- */
	mdn.mdn = 1 ;
	v_mandant = "1" ;
/* ---->
	if (PersName.Trim () != "")
	{
		_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
		if (Sys_ben.dbreadfirstpers_nam () == 0)
		{
			if (Sys_ben.sys_ben.mdn != 0)
			{
				Mdn.mdn.mdn = Sys_ben.sys_ben.mdn;
				m_Mdn.SetReadOnly ();
				m_Mdn.ModifyStyle (WS_TABSTOP, 0);
			}
		}
	}
< ---- */

// 	kalkartfeldersetzen () ;
// 	Form.Show ();

	
	CStrFuncs::SysDate (v_datum) ;
	
	
	ReadMdn ();

/* --->
	if (RemoveKun)
	{
		m_list1.DeleteColumn (m_List.PosKun);
		m_list1.ScrollPositions (2);
		m_list1.DeleteColumn (m_List.PosKunName);
		m_list1.ScrollPositions (3);
	}
< ----- */
//	EnableHeadControls (TRUE);

// return TRUE;  // so mag es oninitdialog sein .... Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten


	if ( globrepair != 1 )	// 030913
		m_repbutton.ShowWindow(SW_HIDE); 



}

void CdachsnveDlg::OnBnClickedCancel()
{
	GetParent ()->DestroyWindow ();
}

void CdachsnveDlg::OnBnClickedOk()
{
	// Speichern und ende
	Write () ;
	GetParent ()->DestroyWindow ();
}

void CdachsnveDlg::OnBnClickedAllesdruck()
{
	Write () ;
	StapelDruck () ;
	// Speichern und Stapeldruck
}

void CdachsnveDlg::OnEnKillfocusMandant()
{
	UpdateData (TRUE) ;
	int i = m_mandant.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;

}

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	int k = 0 ;	// 261007 : Zusatzchecks
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		// 261007 : Zusatzchecks wegen vista, nur noch bedingtes return
		if ( i < 6 ) k = -1 ;
		if ( k == 0 )
		{
			// die ersten beiden Zeichen MUESSEN ZAHLEN sein
			if ( bufi[0] > '9' || bufi[0] < '0' ||bufi[1] > '9' || bufi[1] < '0' )
				k = - 1 ;
		}
		if ( k == 0 )
		{
			if ( bufi[2] > '9' || bufi[2] < '0' )
				k = 2 ;		// Typ 8 oder typ 10 moeglich

		}
		if ( k == 0 )
		{
			// nur noch Typ 6 erlaubt 
			if (    bufi[3] > '9' || bufi[3] < '0'
				 || bufi[4] > '9' || bufi[4] < '0'
				 || bufi[5] > '9' || bufi[5] < '0' )
			{
				k = - 1 ;
			}
			else
			{	// Gueltiger Typ 6  ......
				k = 1 ;
				i = 6 ;
				bufi[6] = '\0' ;
			}
		}
		if ( k == 2 )
		{
			if ( i < 8 )
			{	
				k = -1 ;
			}
			else
			{	// 4.,5.,7.,8. Zeichen MUESSEN ZAHLEN sein, 6.Zeichen MUSS Nicht-Zahl sein
				if (   bufi[3] > '9' || bufi[3] < '0' 
					 ||bufi[4] > '9' || bufi[4] < '0'
				     ||bufi[6] > '9' || bufi[6] < '0'
				     ||bufi[7] > '9' || bufi[7] < '0'
				     || !( bufi[5] >'9' || bufi[5]< '0')
				    )
				k = - 1 ;
			}
		}
		if ( k == 2 )
		{
			if ( bufi[8] > '9' || bufi[8] < '0' )
			{	// gueltiger Typ 8 
				k = 1 ;
				i = 8 ;
				bufi[8] = '\0' ;
			}
		
		}

		if ( k == 2 )
		{
			if ( i < 10 )
			{	
				k = -1 ;
			}
			else
			{	// 9.,10. Zeichen MUESSEN ZAHLEN sein
				if (   bufi[8] > '9' || bufi[8] < '0' 
					 ||bufi[9] > '9' || bufi[9] < '0'
			       )
				{
					k = -1 ;
				}
				else
				{
					k = 1 ;
					i = 10 ;
					bufi[10] = '\0' ;
				}
			}
		}
		if ( k < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}

void CdachsnveDlg::OnEnKillfocusDatum()
{

	UpdateData(TRUE) ;
	int	i = m_datum.GetLine(0,bufh,500) ;
	if (i)
	{
		bufh[i] = '\0' ;
		if (datumcheck(bufh))
		{
			v_datum.Format("%s",_T(bufh));
		}
	}
	Read () ;	// Saetze einlesen 
	UpdateData (FALSE) ;
	return ;
}

BOOL CdachsnveDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_list1.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_list1 &&
					GetFocus ()->GetParent () != &m_list1 )
				{

					break;
			    }
				m_list1.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				m_list1.OnKeyD (VK_DOWN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				m_list1.OnKeyD (VK_UP);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
// ???				OnCancel () ;
				return TRUE;
			}
/* -----> Loeschen ist niemals .....
 			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
< ------ */
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

/* ---->
			else if (pMsg->wParam == VK_F8)
			{
				** ---->
				if (Choice != NULL)
				{	if (Choice->IsWindowVisible ())
					{	Choice->ShowWindow (SW_HIDE);
					}
					else
					{	Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{	OnChoice ();
				}
				< ----- **
				OnChoice();	// alternative zur echten selektion 
			}
			else if (pMsg->wParam == VK_F9)
			{	if (GetFocus () == &m_mdn)
				{	OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_schlklknr)
				{	OnChoice ();
					return TRUE;
				}

				m_List.OnKey9 ();
				return TRUE;
			}
< ----- */
	}

//return CDbPropertyPage::PreTranslateMessage(pMsg);
return CFormView::PreTranslateMessage(pMsg);
//	return FALSE ;
}

BOOL CdachsnveDlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	/* --->
	if (Control == &m_mdn)
	{	if (!ReadMdn ())
		{	m_mdn.SetFocus ();
			return FALSE;
		}
	}
	< ---- */
	/* ----->
	if (Control == &m_schlklknr)
	{	if (!Read ())
		{	m_schlklknr.SetFocus ();
			return FALSE;
		}
	}
	< ------ */
	/* ----->
	if (Control == &m_ckalkart)
	{	kalkartfeldernachsetzen ();
		rechnenach (); 
	}
	< -------- */

	/* ---> 
	if (Control == &m_preisek ||
		Control == &m_anzahl ||
		Control == &m_lebendgew ||
		Control == &m_schlachtgewicht ||
		Control == &m_kaltgew ||
		Control == &m_ausbeute ||
		Control == &m_verlust )
	{
		rechnenach () ;
	}
	< ---- */


	if (Control != &m_list1 &&
		Control->GetParent ()!= &m_list1)
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CdachsnveDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_list1 &&
		Control->GetParent ()!= &m_list1 )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_PrGrList.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}




/* ---> erst mal wech

BOOL CdachsnveDlg::PreTranslateMessage(LPMSG lpMsg)
{

	return PreTranslateMessage(lpMsg);

	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
			 {
		        case VK_RETURN :
					cWnd = GetFocus ();
			           if(cWnd == GetDlgItem (IDOK))
							return PreTranslateMessage(lpMsg);

** ---->
			           if(cWnd == GetDlgItem (IDCANCEL))
					   {
							OnCancel ();
 							return TRUE;
					   }
< ---- **

** --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- **
// gibbet nich			   NextDlgCtrl();
						return TRUE;

** --->
			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
< ---- **

 ** ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- **

** ---->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ----- **


 		    case VK_DOWN :
                     if (NoListCtrl (GetFocus ()))
                     {
// gibbet nich						NextDlgCtrl();
						return TRUE;
					
                     }
				     break;
 		    case VK_UP :
                     if (NoListCtrl (GetFocus ()))
                     {
// gibbet nich						PrevDlgCtrl();
	 					cWnd = GetFocus ();
					 	GetNextDlgTabItem(cWnd ,FALSE) ;
						return TRUE;
                     }
				     break;
		 }
	 }

return PreTranslateMessage(lpMsg);

}
< ------- */
/* ------>
BOOL CdachsnveDlg::NoListCtrl (CWnd *cWnd)
{
    if (((CComboBox *) cWnd->GetParent ())->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CComboBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CListBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    return TRUE;
}
< ---- */

void CdachsnveDlg::OnListBegintrack(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);

	m_list1.StartPauseEnter ();

	*pResult = 0;
}

void CdachsnveDlg::OnListEnddrag(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	
	m_list1.EndPauseEnter ();
	*pResult = 0;
}

BOOL CdachsnveDlg::Read ()
{
	ReadList ();
	return TRUE;
}

/* --->
// Der Inputstring hat zwingend das Format "dd.mm.yyyyy" 
char * sqldatdstger(TIMESTAMP_STRUCT *dstakt, char *outpstr)
{
	sprintf ( outpstr, "%02d.%02d.%04d", dstakt->day, dstakt->month,dstakt->year ) ;
	if ( dstakt->year < 1901 )	// NULL bekaempfen
		sprintf ( outpstr, "  .  .    " ) ;
	return ( outpstr ) ;
}
< ----- */

void setdatum (  TIMESTAMP_STRUCT * ziel , CString quelle )
{
// Input zwingend "dd.mm.yyyy" oder blank

    char tag[] = {"01"};
    char mon[]  = {"01"};
    char jahr[]  = {"2000"};
   	int len = quelle.GetLength ();
	 if (len >=2)
	 {
		 memcpy (tag, quelle.Mid (0, 2), 2);
	 }
	 if (len >=5)
	 {
			 memcpy (mon, quelle.Mid (3, 2), 2);
	 }
	 if (len >=10)
	 {
		 memcpy (jahr, quelle.Mid (6, 4), 4);
	 }
	 else
	 {
		if (len >=10)
		{
			memcpy (jahr + 2 , quelle.Mid (6, 2), 2);
		}
	 }
	 ziel->year = atoi ( jahr ) ; if ( ziel->year > 2099 || ziel->year < 1900 ) ziel->year = 1999 ;
	 ziel->day = atoi ( tag ) ;   if ( ziel->day > 31 || ziel->day < 1 ) ziel->day = 1 ;
	 ziel->month = atoi ( mon ) ; if ( ziel->month > 12 || ziel->month < 1 ) ziel->month = 1 ;
}

BOOL CdachsnveDlg::ReadList ()
{


	m_list1.DeleteAllItems ();
	m_list1.vSelect.clear ();
	int i = 0;
	memcpy (&trapopspm, &trapopspm_null, sizeof (struct TRAPOPSPM));
 	setdatum ( &lsk.lieferdat , v_datum ) ;
	lsk.tou_nr  = v_beleg ;
	int sqlret = 100 ;

//		v_datum.Format( "%02d.%02d.%04d" , lsk.lieferdat.day , lsk.lieferdat.month, lsk.lieferdat.year ) ;
	trapopspm_class.openalltrapo () ;
	sqlret =  trapopspm_class.lesealltrapo() ;	

	while ( ! sqlret )
	{
		FillList.InsertItem (i, 0);

/* -->
		CString pOSI;
		pOSI.Format (_T("%d"), i + 1 );
		FillList.SetItemText (pOSI.GetBuffer (), i, m_List.PosPosi);
		CString kOST_BEZ;
		kOST_BEZ = Schlaklkp.schlaklkp.kost_bez;
		FillList.SetItemText (kOST_BEZ.GetBuffer (), i, m_List.PosKost_bez);

		CString pTWERT ;
		TCHAR ptbezk [37] ; 
		pTWERT.Format (_T("%d"), Schlaklkp.schlaklkp.zubasis);
    	HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk);
	
		CString zUBASIS;
		zUBASIS.Format (_T("%d  %s"),Schlaklkp.schlaklkp.zubasis, ptbezk);
		FillList.SetItemText (zUBASIS.GetBuffer (), i, m_List.PosZubasis);

		CString zUSCHLAG;
		m_List.DoubleToString (Schlaklkp.schlaklkp.zuschlag, zUSCHLAG, 2);
		FillList.SetItemText (zUSCHLAG.GetBuffer (), i, m_List.PosZuschlag);

		double diposwert, diposwertkg ;
		switch ( Schlaklkp.schlaklkp.zubasis )
		{
//		case 0 :	// je kg
		case 1 :	// je Stck.
			diposwert = Schlaklkp.schlaklkp.zuschlag * Schlaklkk.schlaklkk.anzahl ;
			if ( Schlaklkk.schlaklkk.kaltgew != 0.0 )
			{
			    diposwertkg = (Schlaklkp.schlaklkp.zuschlag * 
					Schlaklkk.schlaklkk.anzahl) / Schlaklkk.schlaklkk.kaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;
		case 2 :	// pauschal
			diposwert = Schlaklkp.schlaklkp.zuschlag ;
			if ( Schlaklkk.schlaklkk.kaltgew != 0.0 )
			{
			    diposwertkg = Schlaklkp.schlaklkp.zuschlag / Schlaklkk.schlaklkk.kaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;

		default :	// auffang-Linie : je kg
		    diposwert = Schlaklkp.schlaklkp.zuschlag * Schlaklkk.schlaklkk.kaltgew ;
			diposwertkg = Schlaklkp.schlaklkp.zuschlag ;
			break ;
		}

		CString wERT;
		m_List.DoubleToString (diposwert, wERT, 2);
		Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
		FillList.SetItemText (wERT.GetBuffer (), i, m_List.PosWert);

		CString wERTKG;
		m_List.DoubleToString (diposwertkg, wERTKG, 4);
		Schlaklkp.schlaklkp.wertkg = CStrFuncs::StrToDouble (wERTKG);
		dizuabkg += Schlaklkp.schlaklkp.wertkg ;
		FillList.SetItemText (wERTKG.GetBuffer (), i, m_List.PosWertkg);
		sqlret = Schlaklkp.dbread() ;			
		i ++;
	}

	Schlaklkk.schlaklkk.zuabkg = dizuabkg;
< ---- */

		CString pKUNU;
		pKUNU.Format (_T("%d"), lsk.kun );
		FillList.SetItemText (pKUNU.GetBuffer (), i, m_list1.PosKunu);


		CString pRECH;
		pRECH.Format (_T("%d"), trapopspm.rech );
		FillList.SetItemText (pRECH.GetBuffer (), i, m_list1.PosRech);

		CString pLS;
		pLS.Format (_T("%d"), trapopspm.ls );
		FillList.SetItemText (pLS.GetBuffer (), i, m_list1.PosLs);

		CString pBEZ1;
		pBEZ1 = adr.adr_krz;
		FillList.SetItemText (pBEZ1.GetBuffer (), i, m_list1.PosBez1);

		CString pA;
		pA.Format (_T("%1.0f"), trapopspm.pm );
		FillList.SetItemText (pA.GetBuffer (), i, m_list1.PosArt);

		CString pNVE;
		pNVE = traponve.nve;
		FillList.SetItemText (pNVE.GetBuffer (), i, m_list1.PosNve);

		CString pART;
		pART.Format (_T("%1.0f"), trapopspm.pm );
		FillList.SetItemText (pART.GetBuffer (), i, m_list1.PosArt);

/* ->
		CString pANZ;
		pANZ.Format (_T("%d"), fracht.anz );
		FillList.SetItemText (pANZ.GetBuffer (), i, m_list1.PosAnz);

		CString pAKTIV  = _T(" ")  ;
		if ( fracht.stat )
			pAKTIV = _T("X") ;
		FillList.SetItemText (pAKTIV.GetBuffer (), i, m_list1.PosAktiv);
< ---- */
		sqlret =  trapopspm_class.lesealltrapo() ;
		i ++ ;
	}
	return TRUE;
}

BOOL CdachsnveDlg::InList (LSK_CLASS & lsk_class)
{
	ListRows.FirstPosition ();
/* --->
	CAkiPreise *aki_pr;
	while ((aki_pr = (CAkiPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Akiprgrstp.akiprgrstp.aki_nr == aki_pr->akiprgrstp.aki_nr &&
			Akiprgrstp.akiprgrstp.a == aki_pr->akiprgrstp.a) return TRUE;
	}
< ---- */

   return FALSE;
}
/* --->
void CdachsnveDlg::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CSchlklkpos *pr;
	while ((pr = (CSchlklkpos *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Schlaklkp.schlaklkp, &pr->schlaklkp, sizeof (SCHLAKLKP));
		if (!InList (Schlaklkp))
		{
			Schlaklkp.dbdelete ();
		}
	}
}
< ----- */


void CdachsnveDlg::OnEnKillfocusBeleg()
{
	UpdateData(TRUE) ;
	Read () ;	// Saetze einlesen 
	UpdateData (FALSE) ;
}

BOOL CdachsnveDlg::Write ()
{
	return TRUE ;
}
/* hier wird nie nixx geschrieben
BOOL CdachsnveDlg::Write ()
{
//	UpdateData(TRUE ) ; erzeugt Probeleme ?!

//	Schlaklkp.beginwork ();
	m_list1.StopEnter ();
	int count = m_list1.GetItemCount ();

	for (int i = 0; i < count; i ++)
	{

		memcpy (&fracht, &fracht_null, sizeof (struct FRACHT));

       CString Text;
		 Text = m_list1.GetItemText (i, m_list1.PosLs);
		 fracht.ls = atol ( Text ) ;			// CStrFuncs::StrToDouble (Text);
		 fracht.mdn = mdn.mdn ;
		 fracht_class.openfracht() ;
		 int sqls = fracht_class.lesefracht() ;
		 Text = m_list1.GetItemText (i, m_list1.PosAnz);
		 fracht.anz = atoi ( Text ) ;	// CStrFuncs::StrToDouble (Text);

		 Text = m_list1.GetItemText (i, m_list1.PosAktiv);
		 if ( Text !="X" )
			 fracht.stat = 0 ;
		 else
			 fracht.stat = 1 ;


		 if ( !sqls )
		 {
			 fracht_class.updatefracht () ;
		 }
		 else
		 {
			Text = m_list1.GetItemText (i, m_list1.PosLs);
			fracht.ls = atol (Text ) ;	// CStrFuncs::StrToDouble (Text);
			fracht.mdn = mdn.mdn ;
			fracht.fil = 0 ;
			memcpy ( &fracht.lief_term, &lsk.lieferdat, sizeof (  TIMESTAMP_STRUCT )) ;
			memcpy ( &fracht.dat       , &lsk.lieferdat, sizeof ( TIMESTAMP_STRUCT )) ;

			fracht_class.insertfracht () ;
		 }
	}

	return TRUE;
}
< ---- */

void CdachsnveDlg::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	Rows.Init ();
}

DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}


void CdachsnveDlg::StapelDruck ( )
{

	// gesamte Liste abklappern ....

	int count = m_list1.GetItemCount ();

	for (int i = 0; i < count; i ++)
	{

		CString pTNVE ;
		pTNVE =  FillList.GetItemText (i, m_list1.PosNve );
		char inve[ 40 ] ;
		sprintf ( inve, "%s", pTNVE.GetBuffer()) ;

		CString Value;
		Value =  FillList.GetItemText (i, m_list1.PosLs );
		long els = atol (_T(Value.GetBuffer()));

		DruckeEtiketten ( inve, els );
	}
	
}


void CdachsnveDlg::OnBnClickedRepbutton()
{
	// genau eine Rechnung : Rech-Nr Abfragen, Sicherheitsabfragen und dann : 
	// alle Etiketten l�schen und anschliessend neu erstellen und einmal drucken
	// ist nicht mehr sinnvoll nach Versenden der Datei

	RepDialog repdial;
	repdial.DoModal();


}
