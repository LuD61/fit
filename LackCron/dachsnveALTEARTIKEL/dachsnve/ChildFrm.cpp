// ChildFrm.cpp : Implementierung der Klasse CChildFrame
//
#include "stdafx.h"
#include "dachsnve.h"

#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
END_MESSAGE_MAP()


// CChildFrame-Erstellung/Zerst�rung

CChildFrame::CChildFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
}

CChildFrame::~CChildFrame()
{
}


BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie die Fensterklasse oder die Stile hier, indem Sie CREATESTRUCT �ndern
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}


// CChildFrame-Diagnose

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

// CChildFrame-Meldungshandler

BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	BOOL ret = CMDIChildWnd::OnCreateClient(lpcs,   pContext);	

	CStringA ClassName = pContext->m_pNewViewClass->m_lpszClassName;
	/* -->
	if (ClassName == "CArt1")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->ArtWnd == NULL)
		{
			MainFrm->ArtWnd = this;
		}
		else
		{
			ArtWnd.Add (this);
		}
	}
	if (ClassName == "CArt2")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->MatWnd == NULL)
		{
			MainFrm->MatWnd = this;
		}
		else
		{
			MatWnd.Add (this);
		}
	}
	else if (ClassName == "CApr1")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->APrWnd == NULL)
		{
			MainFrm->APrWnd = this;
		}
		else
		{
			APrWnd.Add (this);
		}
	}
	else if (ClassName == "CIpr1")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->StdPrWnd == NULL)
		{
			MainFrm->StdPrWnd = this;
		}
		else
		{
			StdPrWnd.Add (this);
		}
	}
< --- */
	MDIMaximize ();
	
	return ret;
}


