#ifndef _LIEFRET_DEF
#define _LIEFRET_DEF

struct LIEF_RETBA {
short	delstatus;
long	nr;
char	blg_typ[2];
short	mdn;
short	fil;
long	kun;
short	kun_fil;
TIMESTAMP_STRUCT	dat;
char	pers[13];
long	vertr;
char	err_txt[17];
short	zutyp;
};
extern struct LIEF_RETBA lief_retba, lief_retba_null;


class LIEF_RETBA_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int schreibe_liefret (void);
               LIEF_RETBA_CLASS () : DB_CLASS ()
               {
               }
};
#endif

