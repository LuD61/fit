#ifndef _KUN_DEF
#define _KUN_DEF

struct KUN {

short mdn;
short fil;
long kun;
long adr1;
long adr2;  
long adr3; 
char kun_seit[12];
long txt_nr1;
char frei_txt1[65];
char kun_krz1[17];
char kun_bran[2];
char kun_krz2[17];
char kun_krz3[17];
short kun_typ;
long bbn;
short pr_stu;
long pr_lst;
char vereinb[6];
long inka_nr;
long vertr1;
long vertr2;
char statk_period[2];
char a_period[2];
short sprache;
long txt_nr2;
char frei_txt2[65];
char freifeld1[9];
char freifeld2[9];
long tou;
char vers_art[3];
short lief_art;
char fra_ko_ber[3];
short rue_schei;
char form_typ1[3];
short auflage1;
char freifeld3[9];
char freifeld4[9];
short zahl_art;
short zahl_ziel;
char form_typ2[3];
short auflage2;
long txt_nr3;
char frei_txt3[65];
char nr_bei_rech[17];
char rech_st[3];
short sam_rech;
short einz_ausw;
short gut;
char rab_schl[9];
double bonus1;
double bonus2;
double tdm_grenz1;
double tdm_grenz2;
double jr_plan_ums;
char deb_kto[9];
double kred_lim;
short inka_zaehl;
char bank_kun[37];
long blz;
char kto_nr[17];
short hausbank;
short kun_of_po;
short kun_of_lf;
short kun_of_best;
short delstatus;
char kun_bran2[3];
long rech_fuss_txt;
long ls_fuss_txt;
char ust_id[12];
long rech_kopf_txt;
long ls_kopf_txt;
char gn_pkt_kz[2];
char sw_rab[2];
char bbs[9];
long inka_nr2;
short sw_fil_gr;
short sw_fil;
char ueb_kz[2];
char modif[2];
short kun_leer_kz;
char ust_id16[17];
char iln[17];
short waehrung;
short pr_ausw;
char pr_hier[2];
short pr_ausw_ls;
short pr_ausw_re;
short kun_gr1;
short kun_gr2;
short eg_kz;
short bonitaet;
short kred_vers;
long kst;
char edi_typ[2];
long sedas_dta;
char sedas_kz[3];
short sedas_umf;
short sedas_abr;
short sedas_gesch;
short sedas_satz;
short sedas_med;
char sedas_nam[11];
short sedas_abk1;
short sedas_abk2;
short sedas_abk3;
char sedas_nr1[9];
char sedas_nr2[9];
char sedas_nr3[9];
short sedas_vb1;
short sedas_vb2;
short sedas_vb3;
char sedas_iln[17];
long kond_kun;
short kun_schema;
char plattform[17];
char be_log[4];
long stat_kun;
char ust_nummer[25];
long cmr ;
long pal_bew ;
long trakober ;

};

extern struct KUN kun, kun_save, kun_null;

class KUN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesekun (void);
               int openkun (void);
			   int closeall(void) ;	// 280210 
               KUN_CLASS () : DB_CLASS ()
               {
               }
};

struct TOU {
 long tou ;
 char tou_bz[49] ;
 char fz_kla[3] ;
 char fz[13] ; 
 char srt_zeit[6] ;
 char dau[6] ;
 long lng ;
 char fah_1[13];
 char fah_2[13];
 short delstatus;
 long lgr;
 long leitw_typ;
 long htou;
 long adr;
 short eigentour;
 long lgkonto;
 short auto_typ;
 
};

extern struct TOU tou, tou_save, tou_null;

class TOU_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesetou (void);
               int opentou (void);
               int lesealltou (void);
               int openalltou (void);
               TOU_CLASS () : DB_CLASS ()
               {
               }
};


struct A_HNDW {
double a ;
double tara ;
} ;

extern struct A_HNDW a_hndw, a_hndw_save, a_hndw_null;

class A_HNDW_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_hndw (void);
//               int opena_hndw (void);
               A_HNDW_CLASS () : DB_CLASS ()
               {
               }
};


struct A_EIG {
double a ;
double tara ;
} ;

extern struct A_EIG a_eig, a_eig_save, a_eig_null;

class A_EIG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_eig (void);
//               int opena_eig (void);
               A_EIG_CLASS () : DB_CLASS ()
               {
               }
};

// 151009 

struct A_CRON {
double a ;
long lmhd ;
} ;

extern struct A_CRON a_cron, a_cron_save, a_cron_null;

class A_CRON_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_cron (void);
//               int opena_eig (void);
               A_CRON_CLASS () : DB_CLASS ()
               {
               }
};

struct A_BAS {

double a ;
/* ---
mdn                  smallint                                yes
fil                  smallint                                yes
< ---- */

char a_bz1[25] ;
char a_bz2[25] ;
double a_gew ;
short a_typ ;
short a_typ2 ;
short abt ;
long ag ;

/* ----->
best_auto            char(1)                                 yes
bsd_kz               char(1)                                 yes
cp_aufschl           char(1)                                 yes
delstatus            smallint                                yes
dr_folge             smallint                                yes
erl_kto              integer                                 yes
hbk_kz               char(1)                                 yes
hbk_ztr              smallint                                yes
< ---*/
char hnd_gew  [2] ;
/* --->
hwg                  smallint                                yes
kost_kz              char(2)                                 yes
< --- */
short me_einh ;
/* --->
modif                char(1)                                 yes
mwst                 smallint                                yes
plak_div             smallint                                yes
stk_lst_kz           char(1)                                 yes
< ---- */
double sw ;
short teil_smt ;
/* --->
we_kto               integer                                 yes
wg                   smallint                                yes
zu_stoff             smallint                                yes
akv                  date                                    yes
bearb                date                                    yes
pers_nam             char(8)                                 yes
prod_zeit            decimal(6,3)                            yes
pers_rab_kz          char(1)                                 yes
gn_pkt_gbr           decimal(8,4)                            yes
kost_st              integer                                 yes
sw_pr_kz             char(1)                                 yes
kost_tr              integer                                 yes
< --- */
double a_grund ;
char smt[2] ;		// 021210
/* --->
kost_st2             integer                                 yes
we_kto2              integer                                 yes
charg_hand           integer                                 yes
intra_stat           integer                                 yes
qual_kng             char(4)                                 yes
a_bz3                char(24)                                yes
lief_einh            smallint                                yes
< ---- */
double inh_lief ;
/* --->
erl_kto_1            integer                                 yes
erl_kto_2            integer                                 yes
erl_kto_3            integer                                 yes
we_kto_1             integer                                 yes
we_kto_2             integer                                 yes
we_kto_3             integer                                 yes
skto_f               char(1)                                 yes
sk_vollk             decimal(8,4)                            yes
a_ersatz             decimal(13,0)                           yes
a_ers_kz             smallint                                yes
me_einh_abverk       smallint                                yes
< ---- */

double inh_abverk ;

/* ----> 
hnd_gew_abverk       char(1)                                 yes
inh_ek               decimal(11,3)                           yes
< ---- */

};

extern struct A_BAS a_bas, a_bas_save, a_bas_null;

class A_BAS_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//               int dbcount (void);
               int lesea_bas (void);
               int opena_bas (void);
               int lesealla_bas (void);
               int openalla_bas (void);
			   int closeall ( void ) ;	// 280210
               A_BAS_CLASS () : DB_CLASS ()
               {
               }
};

/* ---->
struct FIL {

char abr_period[2] ;
long adr;
long adr_lief;
short afl;
char auf_typ[2];
char best_kz[2];
char bli_kz[2];
char dat_ero[12];
short daten_mnp;
short delstatus;
short fil;
char fil_kla[2];
short fil_gr;
double fl_lad;
double fl_nto;
double fl_vk_ges;
short frm;
char iakv[12];
char inv_rht[2];
long kun;
char lief[17];
char lief_rht[2];
long lief_s;
char ls_abgr[2];
char ls_kz[2];
short ls_sum;
short mdn;
char pers[13];
short pers_anz;
char pos_kum[2];
char pr_ausw[2];
char pr_bel_entl[2];
char pr_fil_kz[2];
long pr_lst;
char pr_vk_kz[2];
double reg_bed_theke_lng;
double reg_kt_lng;
double reg_kue_lng;
double reg_lng;
double reg_tks_lng;
double reg_tkt_lng;
char ret_entl[2];
char smt_kz[2];
short sonst_einh;
short sprache;
char sw_kz[2];
long tou;
char umlgr[2];
char verk_st_kz[2];
short vrs_typ;
char inv_akv[2];
double planumsatz;

};

extern struct FIL fil, fil_save, fil_null;

class FIL_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesefil (void);
               int openfil (void);
               FIL_CLASS () : DB_CLASS ()
               {
               }
};

struct LEER_LSDR {

short mdn;
short fil;
long ls;
char blg_typ[2];

double a ;
long me_stk_zu ;
long me_stk_abn ;
long stk ;
short stat ;
};

extern struct LEER_LSDR leer_lsdr, leer_lsdr_save, leer_lsdr_null;

class LEER_LSDR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leselsdr (void);
               int openlsdr (void);
               LEER_LSDR_CLASS () : DB_CLASS ()
               {
               }
};

struct LEER_LSH {

short mdn;
short fil;
long ls;
char blg_typ[2];

double a1 ;
char a_bez1[5] ;
long stk_zu1 ;
long stk_ab1 ;
long stk1 ;
double vk_pr1 ;

double a2 ;
char a_bez2[5] ;
long stk_zu2 ;
long stk_ab2 ;
long stk2 ;
double vk_pr2 ;

double a3 ;
char a_bez3[5] ;
long stk_zu3 ;
long stk_ab3 ;
long stk3 ;
double vk_pr3 ;

double a4 ;
char a_bez4[5] ;
long stk_zu4 ;
long stk_ab4 ;
long stk4 ;
double vk_pr4 ;

double a5 ;
char a_bez5[5] ;
long stk_zu5 ;
long stk_ab5 ;
long stk5 ;
double vk_pr5 ;

double a6 ;
char a_bez6[5] ;
long stk_zu6 ;
long stk_ab6 ;
long stk6 ;
double vk_pr6 ;

double a7 ;
char a_bez7[5] ;
long stk_zu7 ;
long stk_ab7 ;
long stk7 ;
double vk_pr7 ;


double a8 ;
char a_bez8[5] ;
long stk_zu8 ;
long stk_ab8 ;
long stk8 ;
double vk_pr8 ;

};
 
extern struct LEER_LSH leer_lsh, leer_lsh_save, leer_lsh_null;

< --- */


struct KUNBRTXTZU {

char kun_bran2[3] ;
long kopf_txt;
long fuss_txt;

};

extern struct KUNBRTXTZU kunbrtxtzu, kunbrtxtzu_save, kunbrtxtzu_null;

class KUNBRTXTZU_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesebrantext (void);
               KUNBRTXTZU_CLASS () : DB_CLASS ()
               {
               }
};



#endif

