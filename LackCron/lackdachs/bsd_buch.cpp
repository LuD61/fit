#include <Windows.h>
#include "DbClass.h"
#include "bsd_buch.h"

struct BSD_BUCH bsd_buch,  bsd_buch_null;
extern DB_CLASS dbClass;
extern UINT ProtMode  ;	// 201209

BSD_BUCH_CLASS bsd_buch_class ;

int BSD_BUCH_CLASS::insertbsd_buch (void)
{
	if ( upd_cursor < 0 ) prepare () ;	// 020210 : einfach sauber machen read_ -> upd_
// 201209 : LS nicht mehr bearbeiten :   dbClass.sqlexecute (ins_cursor);
	return  dbClass.sqlexecute (upd_cursor);	
}

int BSD_BUCH_CLASS::loeschebsd_buch (void)
{

	if ( del_cursor < 0 ) prepare () ;	// 020210 : einfach sauber machen read_ -> ins_
         return dbClass.sqlexecute (del_cursor);
}

void BSD_BUCH_CLASS::prepare (void)
{

/* das war das original, wir machen es aber ganz anders 
	dbClass.sqlin ((long *) &bsd_buch.nr, SQLLONG, 0 ) ;
	dbClass.sqlin ((char *) &bsd_buch.blg_typ, SQLCHAR, 3 ) ;
	dbClass.sqlin ((short *) &bsd_buch.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &bsd_buch.fil, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &bsd_buch.kun_fil, SQLSHORT, 0 ) ;
	
	dbClass.sqlin ((double *) &bsd_buch.a, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd_buch.dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char *) &bsd_buch.zeit, SQLCHAR,  9 ) ;
	dbClass.sqlin ((char *) &bsd_buch.pers, SQLCHAR, 13 ) ;
	dbClass.sqlin ((char *) &bsd_buch.bsd_lgr_ort, SQLCHAR, 13 ) ; 
	
	dbClass.sqlin ((short *) &bsd_buch.qua_status, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &bsd_buch.me, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &bsd_buch.bsd_ek_vk, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((char *) &bsd_buch.chargennr, SQLCHAR, 31 ) ;
	dbClass.sqlin ((char *) &bsd_buch.ident_nr, SQLCHAR, 21 ) ;
	
	dbClass.sqlin ((char *) &bsd_buch.herk_nachw, SQLCHAR, 4 ) ;
	dbClass.sqlin ((char *) &bsd_buch.lief, SQLCHAR, 17 ) ;
	dbClass.sqlin ((long *) &bsd_buch.auf, SQLLONG, 0 ) ;
	dbClass.sqlin ((char *) &bsd_buch.verfall, SQLCHAR, 2 ) ;
//	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd_buch.verf_dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((short *) &bsd_buch.delstatus, SQLSHORT, 0 ) ;
	
	dbClass.sqlin ((char *) &bsd_buch.err_txt, SQLCHAR, 17 ) ;
	dbClass.sqlin ((double *) &bsd_buch.me2, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &bsd_buch.me_einh2, SQLSHORT, 0 ) ;
//	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd_buch.lieferdat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((double *) &bsd_buch.a_grund, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short *) &bsd_buch.me_einh, SQLSHORT, 0 ) ;

	ins_cursor = (short) dbClass.sqlcursor ("insert into bsd_buch (  "
	 "  nr, blg_typ, mdn, fil, kun_fil " 
	" , a, dat, zeit, pers, bsd_lgr_ort "
	" ,	qua_status, me, bsd_ek_vk, chargennr, ident_nr "
	" , herk_nachw, lief, auf, verfall, delstatus "
	" , err_txt, me2, me_einh2, a_grund, me_einh "
	
	" ) values ( "
	 "  ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" , ?, ?, ?, ?, ? " 
	" ) " );
< ------- */

// Funktioniert nur fuer Mandant 1 so richtig korrekt, nur minimal-Bestueckung der Felder
// organisiert
	upd_cursor = (short) dbClass.sqlcursor ( 
	" insert into bsd_buch ( mdn,fil,kun_fil,blg_typ,dat,a,me )"
	" select 1,0,0,'A',today,a,sum(aufp.lief_me) from aufk,aufp "
	" where aufk.lieferdat between today and today + 20 and auf_stat < 5 "
	" and aufk.mdn = aufp.mdn and aufk.auf = aufp.auf group by aufp.a "
		) ;
	ins_cursor = (short) dbClass.sqlcursor (
	" insert into bsd_buch ( mdn,fil,kun_fil,blg_typ,dat,a,me ) "
    " select 1,0,0,'WA',today,a,sum(lsp.auf_me_vgl ) from  lsk,lsp "
	" where  lsk.lieferdat between today and today + 20 and  ls_stat in(2,22) "
    " and  lsk.mdn = lsp.mdn and lsk.ls  = lsp.ls  group by  lsp.a "
	) ;

	del_cursor = (short) dbClass.sqlcursor ("delete from bsd_buch " );

}

LGR_CLASS lgr_class ;
struct LGR lgr,  lgr_null;
struct A_LGR a_lgr,  a_lgr_null;

int LGR_CLASS::lese_a_lager (void)
{

	if ( readcursor < 0 ) prepare () ;
	dbClass.sqlopen(readcursor) ;
	return dbClass.sqlfetch (readcursor);
}

void LGR_CLASS::prepare (void)
{
	dbClass.sqlin ((double *) &a_lgr.a, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((short *) &lgr.lgr_gr, SQLSHORT, 0 ) ;
	dbClass.sqlin ((short *) &lgr.mdn, SQLSHORT, 0 ) ;

	dbClass.sqlout ((long *) &a_lgr.lgr, SQLLONG, 0 ) ;	// beachte : short lgr.lgr ; long a_lgr.lgr 

	readcursor = (short) dbClass.sqlcursor ("select a_lgr.lgr  from a_lgr  "
	" where  a_lgr.a = ?  and a_lgr.lgr in " 
	" ( select lgr.lgr from lgr where  lgr.lgr_gr = ? and  (  lgr.mdn = ? or lgr.mdn = 0 )) "  );
}

// 180809 A

BSD_CLASS bsd_class ;
struct BSD bsd,  bsd_null;

int BSD_CLASS::schreibebsd (void)
{
	if ( test_upd_cursor < 0 )
		prepare () ;
	else
		dbClass.sqlopen( readcursor ) ;

	int di = dbClass.sqlfetch ( readcursor ) ;
	if ( di )
	{
		bsd.delstatus = 0 ;
		di = dbClass.sqlexecute ( ins_cursor ) ;

	}
	else
	{
		bsd.delstatus = 0 ;
		di = dbClass.sqlexecute ( upd_cursor ) ;
	}
	return ( di ) ;
}


int BSD_CLASS::setzebsd_stat (void)
{
// erst mal alles als alt markieren
	if ( test_upd_cursor < 0 ) prepare () ;
    return dbClass.sqlexecute (test_upd_cursor);
}

int BSD_CLASS::loeschebsd_stat (void)
{

// loeschen, was NICHT aktualisiert wurde 
	if ( del_cursor < 0 ) prepare () ;
    return dbClass.sqlexecute (del_cursor);
}

void BSD_CLASS::prepare (void)
{

// 211209 : Schluessel fuer update : artikel und bsd_lgr_ort
// readcursor : Probelesen
	dbClass.sqlin ((short *) &bsd.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &bsd.a, SQLDOUBLE, 0 ) ;
// 201209 	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd.verf_dat, SQLTIMESTAMP, 26 ) ;
 	dbClass.sqlin ((char *) bsd.bsd_lgr_ort, SQLCHAR, 13 ) ;

	dbClass.sqlout ((short *) &bsd.delstatus, SQLSHORT, 0 ) ;

// der erste duplikate Index geht ueber mdn,fil,a , Kriterium ist ausserdem noch bsd_lgr_ort
	readcursor = (short) dbClass.sqlcursor (" select delstatus from bsd where "
		" mdn = ? and fil = 0 and a = ? and bsd_lgr_ort = ? " ) ;

// ins_cursor : es gab noch keinen bsd-Satz zu dem artikel/bsd_lgr_ort

	dbClass.sqlin ((short *) &bsd.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &bsd.a, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((double *) &bsd.bsd_gew, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd.verf_dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char *)   bsd.bsd_lgr_ort, SQLCHAR, 13 ) ;

	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd.bsd_update, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char *)   bsd.bsd_upzeit, SQLCHAR, 13 ) ;

// wichtig : Index ausfuellen

	ins_cursor = (short) dbClass.sqlcursor (
	" insert into bsd ( "
	" mdn,fil, a ,posi "	
	" ,bsd_gew, verf_dat , delstatus, bsd_lgr_ort , bsd_kost_st,bsd_update, bsd_upzeit "
	" ) values ( "
	" ? , 0 , ? , 0 "
	" ,? , ? , 0 , ? , 0, ? , ? ) " ) ;

// upd_cursor : vorhandenen satz aktualisieren 

	dbClass.sqlin ((double *) &bsd.bsd_gew, SQLDOUBLE, 0 ) ;

	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd.bsd_update, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char *) bsd.bsd_upzeit, SQLCHAR, 13 ) ;

	dbClass.sqlin ((short *) &bsd.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlin ((double *) &bsd.a, SQLDOUBLE, 0 ) ;


	dbClass.sqlin ((TIMESTAMP_STRUCT *) &bsd.verf_dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char *) bsd.bsd_lgr_ort, SQLCHAR, 13 ) ;


	upd_cursor = (short) dbClass.sqlcursor (" update bsd set "
		" bsd_gew = ? , delstatus = 0 , bsd_update= ?, bsd_upzeit = ? where "
		" mdn = ? and fil = 0 and a = ?  and verf_dat = ? and bsd_lgr_ort = ? " );
		
	del_cursor = (short) dbClass.sqlcursor ("delete from bsd where delstatus = 1 " ) ;
	test_upd_cursor = (short) dbClass.sqlcursor ("update bsd set delstatus = 1 " ) ;
}


BEST_RES_CLASS best_res_class ;
struct BEST_RES best_res,  best_res_null;

int BEST_RES_CLASS::loeschebest_res (void)
{

	if ( del_cursor < 0 ) prepare () ; // 020210 : einfach sauber machen read_ -> del_
         return dbClass.sqlexecute (del_cursor);
}

void BEST_RES_CLASS::prepare (void)
{
	del_cursor = (short) dbClass.sqlcursor ("delete from best_res  " ) ;
}

// 201209 A

struct CRON_BEST cron_best, cron_best_null;

// 280210 
int CRON_BEST_CLASS::closeall ( void )
{
	if ( ins_cursor > -1 )    dbClass.sqlclose(ins_cursor) ;
	if ( del_cursor > -1 )    dbClass.sqlclose(del_cursor) ;
	if ( upd_cursor > -1 )    dbClass.sqlclose(upd_cursor) ;
	return 0 ;
}



int CRON_BEST_CLASS::loeschecron_best (void)
{

//	if ( readcursor < 0 ) prepare () ;	020210 : siehe 280110
	if ( ins_cursor < 0 ) prepare () ;
         return dbClass.sqlexecute (del_cursor);
}

int CRON_BEST_CLASS::schreibecron_best (void)
{

	if ( ins_cursor < 0 )	// 280110 : readcursor gibbet gar nicht
		prepare () ;

	dbClass.sql_mode = 2 ;	//  exit + Protokoll Abschalten
	int saveProtMode = ProtMode ;
	ProtMode = 0 ;
//	char hilfe [33] ;
//	sprintf ( hilfe, "%1.3f" , cron_best.me ) ;
//	cron_best.me = atof ( hilfe ) ;
//	cron_best.me = 1.123 ;
	int di = dbClass.sqlexecute (ins_cursor) ;

	if ( di )
        di = dbClass.sqlexecute (upd_cursor) ;

	dbClass.sql_mode = 0 ;	//  exit anschalten
	ProtMode = saveProtMode ;
	return  di  ;
}

void CRON_BEST_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &cron_best.a, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short  *) &cron_best.pr_kz, SQLSHORT, 0 ) ;
	dbClass.sqlin ((long   *) &cron_best.bsd_stk, SQLLONG, 0 ) ;
	dbClass.sqlin ((double *) &cron_best.me, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cron_best.dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char   *)  cron_best.zeit, SQLCHAR, 11 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cron_best.mhd, SQLTIMESTAMP, 26 ) ;	// 080312

	ins_cursor = (short) dbClass.sqlcursor (
	" insert into cron_best ( "
	"  a ,pr_kz, bsd_stk, me ,dat, zeit , mhd "	
	" ) values ( "
	" ? , ? , ? , ? , ? , ? , ? ) " ) ;

// upd_cursor : vorhandenen satz aktualisieren 

//	dbClass.sqlin ((double *) &cron_best.a, SQLDOUBLE, 0 ) ;
//	dbClass.sqlin ((short  *) &cron_best.pr_kz, SQLSHORT, 0 ) ;
	dbClass.sqlin ((long   *) &cron_best.bsd_stk, SQLLONG, 0 ) ;
	dbClass.sqlin ((double *) &cron_best.me, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cron_best.dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char   *)  cron_best.zeit, SQLCHAR, 11 ) ;


	dbClass.sqlin ((double *) &cron_best.a, SQLDOUBLE, 0 ) ;
	dbClass.sqlin ((short  *) &cron_best.pr_kz, SQLSHORT, 0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cron_best.mhd, SQLTIMESTAMP, 26 ) ;	// 080312


	upd_cursor = (short) dbClass.sqlcursor (" update cron_best set "
		" bsd_stk = ? , me = ? , dat = ? , zeit = ?  where "
		" a = ? and pr_kz = ? and mhd = ? " );
		
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cron_best.dat, SQLTIMESTAMP, 26 ) ;
	dbClass.sqlin ((char   *)  cron_best.zeit, SQLCHAR, 11 ) ;

	del_cursor = (short) dbClass.sqlcursor ("delete from cron_best where dat <> ? or zeit <> ? " ) ;
}

// 2011209 E