#ifndef _TRAPO_DEF
#define _TRAPO_DEF

struct TRAPOPSPM {
  short mdn ;
  short fil ;
  long ls ;
  char blg_typ [2] ;
  long lfd ;
  double pm ;
  double pmzahl ;
  double ps1 ;
  double ps1zahl ;
  double ps2 ;
  double ps2zahl ;
  double ps3 ;
  double ps3zahl ;
  double ps4 ;
  double ps4zahl ;
  long rech ;
};
extern struct TRAPOPSPM trapopspm, trapopspm_null;


class TRAPOPSPM_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int opentrapopspm (void);
               int lesetrapopspm (void);
               int inserttrapopspm (void);
               int updatetrapopspm (void);

			   TRAPOPSPM_CLASS () : DB_CLASS ()
               {
               }
};

struct TRAPONVE {
  short mdn ;
  short fil ;
  long ls ;
  char blg_typ[2] ;
  long lfd ;
  char nve [21] ;
  double a ;
  short delstatus ;
};
extern struct TRAPONVE traponve, traponve_null;

class TRAPONVE_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int opentraponve (void);
               int lesetraponve (void);
               int inserttraponve (void);
               int updatetraponve (void);

			   TRAPONVE_CLASS () : DB_CLASS ()
               {
               }
};


struct TRAPOTAG {
  short mdn ;
  long tou ;
  TIMESTAMP_STRUCT lieferdat ;
  long status ;	// further usage ?!
};
extern struct TRAPOTAG trapotag, trapotag_null;

class TRAPOTAG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int opentrapotag (void);
               int lesetrapotag (void);
               int inserttrapotag (void);
               int updatetrapotag (void);
               int deletetrapotag (void);

			   TRAPOTAG_CLASS () : DB_CLASS ()
               {
               }
};
extern TRAPOTAG_CLASS Trapotag ;

struct TRAPOKO {
short mdn ;
long leitw_typ ;
long typ ;
long plz_v ;
long plz_b;
char plz_von[9] ;
char plz_bis[9] ;

double von1 ;
double bis1 ;
double wert1 ;

double von2 ;
double bis2 ;
double wert2 ;

double von3 ;
double bis3 ;
double wert3 ;

double von4 ;
double bis4 ;
double wert4 ;

double von5 ;
double bis5 ;
double wert5 ;

double von6 ;
double bis6 ;
double wert6 ;

double von7 ;
double bis7 ;
double wert7 ;

double von8 ;
double bis8 ;
double wert8 ;

double von9 ;
double bis9 ;
double wert9 ;

double von10 ;
double bis10 ;
double wert10 ;

short staat ;	// 130513

};

extern struct TRAPOKO trapoko, trapoko_null;


class TRAPOKO_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesealltrapoko (void);
               int openalltrapoko (void);
               int lesetrapoko (void);
               int opentrapoko (void);
			   void schliessen (void);
			   TRAPOKO_CLASS () : DB_CLASS ()
               {
               }
};

extern TRAPOKO_CLASS Trapoko ;


struct LACKDNUM {
  char numstart[21] ;
  char numakt[21] ;
  char numend[21] ;
};
extern struct LACKDNUM lackdnum, lackdnum_null;

class LACKDNUM_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int openlackdnum (void);
               int leselackdnum (void);
               int writelackdnum (void);

			   LACKDNUM_CLASS () : DB_CLASS ()
               {
               }
};
extern LACKDNUM_CLASS Lackdnum ;


#endif

