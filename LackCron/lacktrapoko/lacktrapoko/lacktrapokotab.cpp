#include "stdafx.h"
#include "DbClass.h"
#include "lacktrapokotab.h"

#include "kun.h"	// 260713
#include "adr.h"	// 260713

extern DB_CLASS dbClass;

struct TRAPOKO trapoko,  trapoko_null;

struct B_LEIT b_leit ;

static int anzzfelder ;


TRAPOKO_CLASS trapoko_class ;


long TRAPOKO_CLASS::firstleseallkun (int allesuchen )
{
	if ( readcbeding < 0 ) prepare ();
	if ( allesuchen )
		sprintf (adr.adr_plz_bis , "zzzzzzzzz" ) ;
	else
		sprintf (adr.adr_plz_bis,  "%s", adr.plz ) ;

	int di = dbClass.sqlopen (readcbeding);
	if (di) return 0;
	di = dbClass.sqlfetch( readcbeding);
	if ( di ) return 0 ;
	return kun.kun ;
}
long TRAPOKO_CLASS::nextleseallkun (void)
{
	return ( dbClass.sqlfetch( readcbeding)) ;
}

int TRAPOKO_CLASS::deletelacktrapoko (void)
{
	if ( del_cursor < 0 ) prepare ();
	int di ;

    trapoko.staat = atoi ( tkopfstaat ) ;

	if ( tkopfstaat[0] == ' ' || tkopfstaat[0] == '\0' )
		di = dbClass.sqlexecute (del_cursor);
	else
		if ( tkopfplz[0] == ' ' || tkopfplz[0] == '\0' )
			di = dbClass.sqlexecute (upd_cursor);
		else
			di = dbClass.sqlexecute (readbbeding);
	return di;
}


int TRAPOKO_CLASS::insertlacktrapoko (void)
{
	if ( ins_cursor < 0 ) prepare ();	
	int di = dbClass.sqlexecute (ins_cursor);
	return di;
}

int TRAPOKO_CLASS::lesealllacktrapoko (void)
{


	int di ;
	 if ( tkopfstaat[0] == ' ' || tkopfstaat[0] == '\0' )
		di = dbClass.sqlfetch (readcursor);
	 else
		if ( tkopfplz[0] == ' ' || tkopfplz[0] == '\0' )
			di = dbClass.sqlfetch (test_upd_cursor);
		else
			di = dbClass.sqlfetch (readabeding);

	  return di;
}


int TRAPOKO_CLASS::openalllacktrapoko (char * ekopfstaat, char * ekopfplz )
{

	sprintf ( tkopfplz, "%s", ekopfplz) ;
	sprintf ( tkopfstaat ,"%s" , ekopfstaat ) ;	// 230713
	trapoko.staat = atoi ( tkopfstaat ) ;

		if ( readcursor < 0 ) prepare ();
		if ( tkopfstaat[0] == ' ' || tkopfstaat[0] == '\0' )
		{
			tnurland = 0 ;	// 230713
			tnurplz = 0 ;
			return dbClass.sqlopen (readcursor);
		}
		else
		{
			if ( tkopfplz[0] == ' ' || tkopfplz[0] == '\0' )
			{
				tnurland = 1 ;	// 230713
				tnurplz = 0 ;
				return dbClass.sqlopen (test_upd_cursor);
			}
			else
			{
				tnurland = 1 ;	// 230713
				tnurplz = 1 ;
				sprintf ( trapoko.plz_von ,"%s", tkopfplz);
				return dbClass.sqlopen (readabeding);
			}
		}
}

void TRAPOKO_CLASS::schliessen (void)
{
	if ( readcursor > -1 )
	{	// geht ca. 200 mal gut, dann knallt es wohl 
		dbClass.sqlclose(readcursor ) ;
		readcursor = -1 ;
	}
}


void TRAPOKO_CLASS::prepare (void)
{

// delcursor .....

	if ( del_cursor < 0 )
	{

		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.typ, SQLLONG, 0 ) ;

		del_cursor = (short) dbClass.sqlcursor ("delete from trapoko  "
		" where mdn = ? and leitw_typ = ? and typ = ? " );
	}

// Lese alle Saetze eines trapoko-Typs

	if ( readcursor < 0 )
	{

		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long *) &trapoko.typ, SQLLONG, 0 ) ;

		dbClass.sqlout (( long *) &trapoko.plz_v, SQLLONG, 0 ) ;
		dbClass.sqlout (( long *) &trapoko.plz_b, SQLLONG, 0 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_von,  SQLCHAR, 9 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_bis,  SQLCHAR, 9 ) ;

		dbClass.sqlout (( double *) &trapoko.von1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert1,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert2,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert3,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert4,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert5,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert6,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert7,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert8,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert9,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert10,SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( short *)  &trapoko.staat, SQLSHORT, 0 ) ;	// 060513 

			readcursor = (short) dbClass.sqlcursor ("select "
			" plz_v, plz_b , plz_von , plz_bis "
			" , von1, bis1 , wert1 "
			" , von2, bis2 , wert2 "
			" , von3, bis3 , wert3 "
			" , von4, bis4 , wert4 "
			" , von5, bis5 , wert5 "
			" , von6, bis6 , wert6 "
			" , von7, bis7 , wert7 "
			" , von8, bis8 , wert8 "
			" , von9, bis9 , wert9 "
			" , von10,bis10, wert10,staat "
	
			" from trapoko where mdn = ? and leitw_typ = ?  and typ = ?" 
			" order by staat, plz_von, plz_bis desc " ) ;
	}

// Lese alle Saetze eines trapoko-Typs

	if ( test_upd_cursor < 0 )
	{

		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long *) &trapoko.typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( short *)  &trapoko.staat, SQLSHORT, 0 ) ;

		dbClass.sqlout (( long *) &trapoko.plz_v, SQLLONG, 0 ) ;
		dbClass.sqlout (( long *) &trapoko.plz_b, SQLLONG, 0 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_von,  SQLCHAR, 9 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_bis,  SQLCHAR, 9 ) ;

		dbClass.sqlout (( double *) &trapoko.von1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert1,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert2,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert3,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert4,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert5,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert6,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert7,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert8,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert9,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert10,SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( short *)  &trapoko.staat, SQLSHORT, 0 ) ;	// 060513 

			test_upd_cursor = (short) dbClass.sqlcursor ("select "
			" plz_v, plz_b , plz_von , plz_bis "
			" , von1, bis1 , wert1 "
			" , von2, bis2 , wert2 "
			" , von3, bis3 , wert3 "
			" , von4, bis4 , wert4 "
			" , von5, bis5 , wert5 "
			" , von6, bis6 , wert6 "
			" , von7, bis7 , wert7 "
			" , von8, bis8 , wert8 "
			" , von9, bis9 , wert9 "
			" , von10,bis10, wert10,staat "

			" from trapoko where mdn = ? and leitw_typ = ?  and typ = ? and staat = ? " 
			" order by staat, plz_von, plz_bis desc " ) ;
	}


// 230713 : Lese genau einen satz  

	if ( readabeding < 0 )
	{

		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long *) &trapoko.typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( short *)  &trapoko.staat, SQLSHORT, 0 ) ;
		dbClass.sqlin (( char *)  trapoko.plz_von, SQLCHAR, 9 ) ;

		dbClass.sqlout (( long *) &trapoko.plz_v, SQLLONG, 0 ) ;
		dbClass.sqlout (( long *) &trapoko.plz_b, SQLLONG, 0 ) ;
//		dbClass.sqlout (( char *)  trapoko.plz_von,  SQLCHAR, 9 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_bis,  SQLCHAR, 9 ) ;

		dbClass.sqlout (( double *) &trapoko.von1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert1,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert2,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert3,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert4,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert5,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert6,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert7,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert8,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert9,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert10,SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( short *)  &trapoko.staat, SQLSHORT, 0 ) ;	// 060513 

			readabeding = (short) dbClass.sqlcursor ("select "
			" plz_v, plz_b , plz_bis "
			" , von1, bis1 , wert1 "
			" , von2, bis2 , wert2 "
			" , von3, bis3 , wert3 "
			" , von4, bis4 , wert4 "
			" , von5, bis5 , wert5 "
			" , von6, bis6 , wert6 "
			" , von7, bis7 , wert7 "
			" , von8, bis8 , wert8 "
			" , von9, bis9 , wert9 "
			" , von10,bis10, wert10,staat "

			" from trapoko where mdn = ? and leitw_typ = ?  and typ = ? and staat = ? " 
			" and plz_von = ? order by staat, plz_von, plz_bis desc " ) ;
	}




// insert ....

	if ( ins_cursor < 0 )
	{
		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long *) &trapoko.typ, SQLLONG, 0 ) ;

		dbClass.sqlin (( long *) &trapoko.plz_v, SQLLONG, 0 ) ;
		dbClass.sqlin (( long *) &trapoko.plz_b, SQLLONG, 0 ) ;
		dbClass.sqlin (( char *)  trapoko.plz_von,  SQLCHAR, 9 ) ;
		dbClass.sqlin (( char *)  trapoko.plz_bis,  SQLCHAR, 9 ) ;

		dbClass.sqlin (( double *) &trapoko.von1, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.bis1, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.wert1,SQLDOUBLE, 0 ) ;

		dbClass.sqlin (( double *) &trapoko.von2, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.bis2, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.wert2,SQLDOUBLE, 0 ) ;

		dbClass.sqlin (( double *) &trapoko.von3, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.bis3, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.wert3,SQLDOUBLE, 0 ) ;

		dbClass.sqlin (( double *) &trapoko.von4, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.bis4, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.wert4,SQLDOUBLE, 0 ) ;

		dbClass.sqlin (( double *) &trapoko.von5, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.bis5, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.wert5,SQLDOUBLE, 0 ) ;

		dbClass.sqlin (( double *) &trapoko.von6, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.bis6, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.wert6,SQLDOUBLE, 0 ) ;

		dbClass.sqlin (( double *) &trapoko.von7, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.bis7, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.wert7,SQLDOUBLE, 0 ) ;

		dbClass.sqlin (( double *) &trapoko.von8, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.bis8, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.wert8,SQLDOUBLE, 0 ) ;

		dbClass.sqlin (( double *) &trapoko.von9, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.bis9, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.wert9,SQLDOUBLE, 0 ) ;

		dbClass.sqlin (( double *) &trapoko.von10, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.bis10, SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( double *) &trapoko.wert10,SQLDOUBLE, 0 ) ;
		dbClass.sqlin (( short *)&trapoko.staat, SQLSHORT, 0 ) ;	// 060513

		ins_cursor = (short) dbClass.sqlcursor ("insert into trapoko  "
		" ( mdn, leitw_typ, typ, plz_v, plz_b, plz_von, plz_bis "
			" , von1, bis1 , wert1 "
			" , von2, bis2 , wert2 "
			" , von3, bis3 , wert3 "
			" , von4, bis4 , wert4 "
			" , von5, bis5 , wert5 "
			" , von6, bis6 , wert6 "
			" , von7, bis7 , wert7 "
			" , von8, bis8 , wert8 "
			" , von9, bis9 , wert9 "
			" , von10,bis10, wert10, staat "
		" ) values ( "
		"    ? ,? ,? ,? ,? ,? ,? "
		"   ,? ,? ,? "		// 1		
		"   ,? ,? ,? "		
		"   ,? ,? ,? "		// 3	
		"   ,? ,? ,? "		
		"   ,? ,? ,? "		// 5
		"   ,? ,? ,? "		
		"   ,? ,? ,? "		// 7	
		"   ,? ,? ,? "		
		"   ,? ,? ,? "		// 9
		"   ,? ,? ,? ,? "		
		") "
		) ;
	}

// upd_cursor ..... : angepasster del_cursor

	if ( upd_cursor < 0 )
	{

		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( short *)&trapoko.staat, SQLSHORT, 0 ) ;

		upd_cursor = (short) dbClass.sqlcursor ("delete from trapoko  "
		" where mdn = ? and leitw_typ = ? and typ = ? and staat = ? " );
	}

// readbbeding ..... : angepasster del_cursor

	if ( readbbeding < 0 )
	{

		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( short *)&trapoko.staat, SQLSHORT, 0 ) ;
		dbClass.sqlin (( char *) trapoko.plz_von, SQLCHAR, 9 ) ;

		readbbeding = (short) dbClass.sqlcursor ("delete from trapoko  "
		" where mdn = ? and leitw_typ = ? and typ = ? and staat = ? and plz_von = ? " );
	}

	if ( readcbeding < 0 )
	{

		dbClass.sqlin (( short *)&adr.staat, SQLSHORT, 0 ) ;
		dbClass.sqlin (( char *) adr.plz , SQLCHAR, 9 ) ;
		dbClass.sqlin (( char *) adr.adr_plz_bis , SQLCHAR, 9 ) ;

		dbClass.sqlout (( long *) &kun.kun , SQLLONG, 0 ) ;
		dbClass.sqlout (( char *) kun.kun_krz1 , SQLCHAR, 17 ) ;


		readcbeding = (short) dbClass.sqlcursor ("select kun.kun, kun.kun_krz1 from adr, kun "
		" where adr.staat = ? and adr.plz between ? and ?"
		" and kun.mdn = 1 and kun.fil = 0 and kun.adr2 = adr.adr and kun.kun > 0 "
		"order by kun.kun"
		 );
	}


}

