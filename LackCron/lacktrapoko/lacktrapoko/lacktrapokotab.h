#ifndef _TRAPOKO_DEF
#define _TRAPOKO_DEF

#define DEFAULTMANDANT 1

struct TRAPOKO {
short mdn ;
long leitw_typ ;
long typ ;
long plz_v ;
long plz_b;
char plz_von[9] ;
char plz_bis[9] ;

double von1 ;
double bis1 ;
double wert1 ;

double von2 ;
double bis2 ;
double wert2 ;

double von3 ;
double bis3 ;
double wert3 ;

double von4 ;
double bis4 ;
double wert4 ;

double von5 ;
double bis5 ;
double wert5 ;

double von6 ;
double bis6 ;
double wert6 ;

double von7 ;
double bis7 ;
double wert7 ;

double von8 ;
double bis8 ;
double wert8 ;

double von9 ;
double bis9 ;
double wert9 ;

double von10 ;
double bis10 ;
double wert10 ;
short staat ;	// 060513

};

extern struct TRAPOKO trapoko, trapoko_null;

struct B_LEIT {
char inleitw [4099] ;
long vonwert ;
long biswert ;
};

extern struct B_LEIT b_leit ;


class TRAPOKO_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesealllacktrapoko (void);
               int openalllacktrapoko (char *, char *);
			   int deletelacktrapoko (void) ;
			   int insertlacktrapoko (void);
			   long firstleseallkun ( int);	// 260713
			   long nextleseallkun(void);		// 260713
			   void schliessen (void);
			   BOOL basissort ;
			   BOOL gespeichert ;
			   char tkopfstaat[90] ;
			   char tkopfkun[90];	// 230713
			   char tkopfplz[90];
			   int tnurland;
			   int tnurplz;
			   TRAPOKO_CLASS () : DB_CLASS ()
               {
				   sprintf ( tkopfstaat, " " ) ;
				   sprintf ( tkopfkun,  " " );	// 230713
				   sprintf ( tkopfplz,"") ;
				   tnurland = 0;	// 230713 
				   tnurplz = 0;	// 230713 
				   basissort = FALSE ;
				   gespeichert = TRUE ;
               }
};

extern TRAPOKO_CLASS trapoko_class ;


#endif

