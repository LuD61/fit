// lacktrapokoDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "lacktrapoko.h"
#include "lacktrapokoDlg.h"
// #include "KunWahl.h"

#include "strfuncs.h"
#include "dbClass.h"
#include "mdn.h"
#include "adr.h"
#include "kun.h"
#include "lacktrapokotab.h"
#include "ptabn.h"

#include "FillList.h"

extern DB_CLASS dbClass ;

extern char * clipped( char *) ;

static char bufh[516] ;	// allgemeine Hilfsvariable
static CString bufx ;	// allgemeine Hilfsvariable 


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMytrapokoDlg-Dialogfeld
static int geradeaufland ;

CMytrapokoDlg::CMytrapokoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMytrapokoDlg::IDD, pParent)
//	, v_mdnname(_T(""))
//	, v_mdnnr(0)
	, v_comboleitwtyp(_T(""))
//	, v_kundnr(0)
//	, v_kundname(_T(""))
, v_check1(FALSE)
, v_message(_T(""))
, v_combostaat(_T(""))
, v_kunnr(_T(""))
, v_kstaat(_T(""))
, v_kplz(_T(""))
, v_kort(_T(""))
, v_kname(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	trapoko_class.basissort = FALSE ;
	trapoko_class.gespeichert = TRUE ;
}

void CMytrapokoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_list1);
	DDX_Control(pDX, IDC_COMBOLEITWTYP, m_comboleitwtyp);
	DDX_CBString(pDX, IDC_COMBOLEITWTYP, v_comboleitwtyp);
	DDX_Control(pDX, IDC_BUTTKEY6, m_buttkey6);
	DDX_Control(pDX, IDC_BUTTKEY7, m_buttkey7);
	DDX_Control(pDX, IDC_BUTTKEY8, m_buttkey8);
	DDX_Control(pDX, IDC_BUTTKEY9, m_buttkey9);
	DDX_Control(pDX, IDC_BUTTKEY10, m_buttkey10);
	DDX_Control(pDX, IDC_BUTTKEY11, m_buttkey11);
	DDX_Control(pDX, IDC_CHECK1, m_check1);
	DDX_Check(pDX, IDC_CHECK1, v_check1);
	DDX_Control(pDX, IDC_MESSAGE, m_message);
	DDX_Text(pDX, IDC_MESSAGE, v_message);
	DDX_Control(pDX, IDC_COMBOSTAAT, m_combostaat);
	DDX_CBString(pDX, IDC_COMBOSTAAT, v_combostaat);
	DDX_Control(pDX, IDC_KUNNR, m_kunnr);
	DDX_Text(pDX, IDC_KUNNR, v_kunnr);
	DDX_Control(pDX, IDC_KSTAAT, m_kstaat);
	DDX_Text(pDX, IDC_KSTAAT, v_kstaat);
	DDX_Control(pDX, IDC_KPLZ, m_kplz);
	DDX_Text(pDX, IDC_KPLZ, v_kplz);
	DDX_Control(pDX, IDC_KORT, m_kort);
	DDX_Text(pDX, IDC_KORT, v_kort);
	DDX_Control(pDX, IDC_KNAME, m_kname);
	DDX_Text(pDX, IDC_KNAME, v_kname);
}

BEGIN_MESSAGE_MAP(CMytrapokoDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_NOTIFY(HDN_BEGINTRACK, 0, &CMytrapokoDlg::OnHdnBegintrackList1)
	ON_NOTIFY(HDN_ENDTRACK, 0, &CMytrapokoDlg::OnHdnEndtrackList1)
	ON_BN_CLICKED(IDOK, &CMytrapokoDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMytrapokoDlg::OnBnClickedCancel)
	ON_CBN_KILLFOCUS(IDC_COMBOLEITWTYP, &CMytrapokoDlg::OnCbnKillfocusComboleitwtyp)
	ON_CBN_SELCHANGE(IDC_COMBOLEITWTYP, &CMytrapokoDlg::OnCbnSelchangeComboleitwtyp)
	ON_CBN_SETFOCUS(IDC_COMBOLEITWTYP, &CMytrapokoDlg::OnCbnSetfocusComboleitwtyp)
	ON_BN_CLICKED(IDC_BDRUCK, &CMytrapokoDlg::OnBnClickedBdruck)
	ON_BN_CLICKED(IDC_BUTTKEY7, &CMytrapokoDlg::OnBnClickedButtkey7)
	ON_BN_CLICKED(IDC_BUTTKEY6, &CMytrapokoDlg::OnBnClickedButtkey6)
	ON_BN_CLICKED(IDC_CHECK1, &CMytrapokoDlg::OnBnClickedCheck1)
	ON_CBN_KILLFOCUS(IDC_COMBOSTAAT, &CMytrapokoDlg::OnCbnKillfocusCombostaat)
	ON_CBN_SETFOCUS(IDC_COMBOSTAAT, &CMytrapokoDlg::OnCbnSetfocusCombostaat)
	ON_CBN_SELCHANGE(IDC_COMBOSTAAT, &CMytrapokoDlg::OnCbnSelchangeCombostaat)
	ON_EN_KILLFOCUS(IDC_KUNNR, &CMytrapokoDlg::OnEnKillfocusKunnr)
END_MESSAGE_MAP()

/* -----
bool CMytrapokoDlg::ReadMdn (void) 
{
	bool retcode = FALSE ;
	Mdn.openmdn();
	if (! Mdn.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
			retcode = TRUE ;
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			retcode = FALSE ;
// hier nicht notwendig :	PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		retcode = FALSE ;
// hier nicht notwendig : 		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;
	return retcode ;
}
< ---- */
 
void CMytrapokoDlg::FillStaatCombo ()
{
	CVector Values;
	Values.Init ();

	int i = ptabn_class.openallptabn ("staat") ;

//	Ptabn.sqlopen (IStaatCursor);
// 	while (Ptabn.sqlfetch (IStaatCursor) == 0)
	while ( ! ptabn_class.leseallptabn() )
	{
//		Ptabn.dbreadfirst ();
		CString *Value = new CString ();
		// ptabn.ptwert ist hier immer 3 Stellig + linksbuendig
		Value->Format ("%s  %s", ptabn_class.ptabn.ptwert, ptabn_class.ptabn.ptwer2);
		Values.Add (Value);
	}
	m_list1.FillStaatCombo (Values);
}


void CMytrapokoDlg::FillKundeCombo ()
{
	CVector Values;
	Values.Init ();

	adr.staat = 0 ;
	sprintf (adr.plz ,"");
	long i = trapoko_class.firstleseallkun (0 ) ;
	if (i) i= 0 ;

	while ( ! i )
	{
		CString *Value = new CString ();
		Value->Format ("%08d  %s", kun.kun, kun.kun_krz1);
		Values.Add (Value);
		i = trapoko_class.nextleseallkun() ;
	}
	m_list1.FillKundeCombo (Values);
}


BOOL CMytrapokoDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				keyset(TRUE);
				m_list1.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_list1 &&
					GetFocus ()->GetParent () != &m_list1 )
				{

					break;
			    }
				keyset(TRUE);
				m_list1.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
// AAA OOOO					return FALSE ;
				}
				keyset(TRUE);
				m_list1.OnKeyD (VK_DOWN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				keyset(TRUE);
				m_list1.OnKeyD (VK_UP);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				OnCancel () ;
				return TRUE;
			}

 			else if (pMsg->wParam == VK_F6)
			{

//				OnInsert ();
//				return TRUE;
				CWnd *Control = GetFocus ();
				if (Control == &m_list1 ||
					Control->GetParent ()== &m_list1 )
				{
					m_list1.OnKeyD (VK_F6);
					return TRUE;
				}

			}

 			else if (pMsg->wParam == VK_F7)
			{
				CWnd *Control = GetFocus ();
				if (Control == &m_list1 ||
					Control->GetParent ()== &m_list1 )
				{
					m_list1.OnKeyD (VK_F7);
					return TRUE;
				}
//				OnDelete ();
//				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				Read  ();
				v_message = "Daten wurden gespeichert" ;
				i_message = 1 ;
				return TRUE;
			}

/* ---->
			else if (pMsg->wParam == VK_F8)
			{
				** ---->
				if (Choice != NULL)
				{	if (Choice->IsWindowVisible ())
					{	Choice->ShowWindow (SW_HIDE);
					}
					else
					{	Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{	OnChoice ();
				}
				< ----- **
				OnChoice();	// alternative zur echten selektion 
			}
			else if (pMsg->wParam == VK_F9)
			{	if (GetFocus () == &m_mdn)
				{	OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_schlklknr)
				{	OnChoice ();
					return TRUE;
				}

				m_List.OnKey9 ();
				return TRUE;
			}
< ----- */
	}
//return CDbPropertyPage::PreTranslateMessage(pMsg);
//return CFormView::PreTranslateMessage(pMsg);
//	return FALSE ;
return CDialog::PreTranslateMessage(pMsg);
}


// CMytrapokoDlg-Meldungshandler

void CMytrapokoDlg::OnFillList(void)
{



	FillList = m_list1;


    CList1Ctrl *ListFilli  = (CList1Ctrl *) GetDlgItem (IDC_LIST1);

	if (ListFilli != NULL)
	{
		ListFilli->DeleteAllItems ();

		CHeaderCtrl *Header = ListFilli->GetHeaderCtrl ();
		if (Header != NULL)
		{
			int iCount = Header->GetItemCount ();
			for (int i = 0; i < iCount; i ++)
			{
				ListFilli->DeleteColumn (0);
			}
		}
	}
	FillList.SetStyle (LVS_REPORT);

	if (m_list1.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}

/* ---> das braucht gar keiner 
	Ptabn.sqlout ((long *)  &Ptabn.ptabn.ptlfnr, SQLLONG, 0);
	Ptabn.sqlout ((char *)  Ptabn.ptabn.ptwert, SQLCHAR, 4);
	Ptabn.sqlout ((char *)  Ptabn.ptabn.ptwer2, SQLCHAR, 9) ;

	IMeEinhCursor = Ptabn.sqlcursor ( "select ptlfnr"
							" , ptwert, ptwer2 "
							" from ptabn "
							"where ptitem = 'staat'  order by ptlfnr " 
						) ;
< ----- */
	FillStaatCombo ();

	FillKundeCombo();	// 260713

//	m_buttkey6.EnableWindow (FALSE) ;
//	m_buttkey7.EnableWindow (FALSE) ;
//	m_buttkey8.EnableWindow (FALSE) ;
//	m_buttkey9.EnableWindow (FALSE) ;
//	m_buttkey10.EnableWindow(FALSE) ;
//	m_buttkey11.EnableWindow(FALSE) ;


// 060513 : Staat dazu : alles eins runter .... 

	FillList.SetCol (_T(""), 0, 0);
//	FillList.SetCol (_T("lacktrapoko"), 1, 80, LVCFMT_RIGHT);

	FillList.SetCol (_T("Staat"), 1, 65, LVCFMT_RIGHT);

	FillList.SetCol (_T("PLZ"), 2, 65, LVCFMT_RIGHT);
// 230713
	FillList.SetCol (_T("Kunde"), 3, 65, LVCFMT_RIGHT);
// 230713 : die folgenden Indizes um 1 erhoeht und FixWert ergaenzt


	if ( trapoko_class.basissort == FALSE )	// FALSE = Palette TRUE = Gewicht
	{
		FillList.SetCol (_T("bis Pal.1"), 4, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 1"), 5, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.2"), 6, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 2"), 7, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.3"), 8, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 3"), 9, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.4"), 10, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 4"), 11, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.5"), 12, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 5"), 13, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.6"), 14, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 6"), 15, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.7"), 16, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 7"), 17, 65, LVCFMT_RIGHT);
	}
	else
	{
		FillList.SetCol (_T("bis kg.1"), 4, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 1"), 5, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.2"), 6, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 2"), 7, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.3"), 8, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 3"), 9, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.4"), 10, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 4"), 11, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.5"), 12, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 5"), 13, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.6"), 14, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 6"), 15, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.7"), 16, 65, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 7"), 17, 65, LVCFMT_RIGHT);
	}
	FillList.SetCol (_T("FixBetr"), 18, 65, LVCFMT_RIGHT);


//	m_list1.ColType.Add (new CColType (5, m_list1.CheckBox)) ;

}


BOOL CMytrapokoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	branrekursion = 0 ;
	i_message = 0 ;
	geradeaufland = 0 ;
	
	dbClass.opendbase (_T("bws"));


	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	// grundeinstellung
//	m_comboleitwtyp.EnableWindow (TRUE) ;
//	m_comboleitwtyp.ModifyStyle (0, WS_TABSTOP,0) ;


	OnFillList() ;
/* ----->
	FillList = m_list1;
	FillList.SetStyle (LVS_REPORT);
	if (m_list1.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}

	** --->
	Ptabn.sqlout ((long *)  &Ptabn.ptabn.ptlfnr, SQLLONG, 0);
	Ptabn.sqlout ((char *)  Ptabn.ptabn.ptwert, SQLCHAR, 4);
	Ptabn.sqlout ((char *)  Ptabn.ptabn.ptbezk, SQLCHAR, 9) ;

	IMeEinhCursor = Ptabn.sqlcursor ( "select ptlfnr"
							" , ptwert, ptbezk "
							" from ptabn "
							"where ptitem = 'me_einh'  order by ptlfnr " 
						) ;
< ------ ** */
	FillStaatCombo ();
	FillKundeCombo () ;	// 260713
/* --->
//	m_buttkey6.EnableWindow (FALSE) ;
//	m_buttkey7.EnableWindow (FALSE) ;
//	m_buttkey8.EnableWindow (FALSE) ;
//	m_buttkey9.EnableWindow (FALSE) ;
//	m_buttkey10.EnableWindow(FALSE) ;
//	m_buttkey11.EnableWindow(FALSE) ;


	FillList.SetCol (_T(""), 0, 0);
//	FillList.SetCol (_T("lacktrapoko"), 1, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("von PLZ"), 1, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("bis PLZ"), 2, 80, LVCFMT_RIGHT);

	if ( trapoko_class.basissort == FALSE )	// FALSE = Palette TRUE = Gewicht
	{
		FillList.SetCol (_T("bis Pal.1"), 3, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 1"), 4, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.2"), 5, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 2"), 6, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.3"), 7, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 3"), 8, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.4"), 9, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 4"), 10, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.5"), 11, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 5"), 12, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.6"), 13, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 6"), 14, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis Pal.7"), 15, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 7"), 16, 80, LVCFMT_RIGHT);
	}
	else
	{
		FillList.SetCol (_T("bis kg.1"), 3, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 1"), 4, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.2"), 5, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 2"), 6, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.3"), 7, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 3"), 8, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.4"), 9, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 4"), 10, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.5"), 11, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 5"), 12, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.6"), 13, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 6"), 14, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("bis kg.7"), 15, 80, LVCFMT_RIGHT);
		FillList.SetCol (_T("Wert 7"), 16, 80, LVCFMT_RIGHT);
	}


//	m_list1.ColType.Add (new CColType (5, m_list1.CheckBox)) ;
< ---- */

//	mdn.mdn = 1 ;	// gibbet hoffentlich immer ?!
//	v_mdnnr = 1 ;

	sprintf ( leitw_typ , " " ) ;	// undefiniert laden .....
	sprintf ( kopfstaat , " " ) ;	// undefiniert laden .....
	sprintf ( kopfkun, " " ) ;	// 230713 undef. laden	
	sprintf ( kopfplz, " " ) ;	// 230713 undef. laden	

//	kun.kun = 0 ;

//	ReadMdn() ;

// Laden der Combo-Box leitwtyp

	CString szptabn ;
//	szptabn.Format("3" ) ;	// default : Dachser ?!
//	((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->AddString(szptabn.GetBuffer(0));
//	((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->SetCurSel(0);


	int sqlstat = ptabn_class.openallptabn ("leitw_typ");
	sqlstat = ptabn_class.leseallptabn();
	while(!sqlstat)
	{
		szptabn.Format("%s  %s",ptabn_class.ptabn.ptwert, ptabn_class.ptabn.ptbez);
		// hier haben wir jetzt die Namen und k�nnen sie in die ComboBox einf�gen

		((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->AddString(szptabn.GetBuffer(0));
		((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->SetCurSel(0);
	
		sqlstat = ptabn_class.leseallptabn () ;
	}

	setzeleitw_typ3() ;


	sqlstat = ptabn_class.openallptabn ("staat");
	sqlstat = ptabn_class.leseallptabn();
	szptabn.Format( "   ");	// Leerzeile 
	((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->AddString(szptabn.GetBuffer(0));
	((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->SetCurSel(0);


	while(!sqlstat)
	{
		szptabn.Format("%s %s %s",ptabn_class.ptabn.ptwert, ptabn_class.ptabn.ptwer2, ptabn_class.ptabn.ptbez);
		// hier haben wir jetzt die Namen und k�nnen sie in die ComboBox einf�gen

		((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->AddString(szptabn.GetBuffer(0));
		((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->SetCurSel(0);
	
		sqlstat = ptabn_class.leseallptabn () ;
	}

	setzestaat_leer() ;

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CMytrapokoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CMytrapokoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CMytrapokoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMytrapokoDlg::OnHdnBegintrackList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	m_list1.StartPauseEnter ();

	*pResult = 0;
}

void CMytrapokoDlg::OnHdnEndtrackList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	m_list1.EndPauseEnter ();

	*pResult = 0;
}

BOOL CMytrapokoDlg::Read ()
{
	ReadList ();
	return TRUE;
}

BOOL CMytrapokoDlg::ReadList ()
{


	m_list1.DeleteAllItems ();
	m_list1.vSelect.clear ();
	int i = 0;
	memcpy (&trapoko, &trapoko_null, sizeof (struct TRAPOKO));
	trapoko.leitw_typ = atol ( leitw_typ ) ;
	trapoko.mdn = DEFAULTMANDANT ;
	trapoko.typ = 0 ;
	if ( trapoko_class.basissort == TRUE )
		trapoko.typ = 1 ;
 
	int sqlret = 100 ;

	trapoko_class.gespeichert = FALSE ;

 // 	Text = m_list1.GetItemText (i, m_list1.PosPlz_von);
//		sprintf ( hilflacktrapoko ,"%s" , Text.Trim().Left(8).GetBuffer()) ;

	trapoko_class.openalllacktrapoko (kopfstaat, v_kplz.Trim().Left(8).GetBuffer()) ;
	sqlret =  trapoko_class.lesealllacktrapoko() ;	
	if ( !sqlret )
	{

		CChoiceStaat HoleStaat ;

		while ( ! sqlret )
		{
	

			FillList.InsertItem (i, 0);

			CString pTWERT ;
			TCHAR ptwer2 [9] ; 
			pTWERT.Format (_T("%d"), trapoko.staat);
			HoleStaat.GetPtWer2 ( pTWERT.GetBuffer(), ptwer2 );
	
			CString zUBASIS;
// das Problem : Formatierung numerischer Wert mit 2 Folgeblanks 
			if ( trapoko.staat > -1 && trapoko.staat < 10 )
			{
				zUBASIS.Format (_T("%d    %s"),trapoko.staat, ptwer2);
			}
			else
			{
				if ( trapoko.staat > 9 && trapoko.staat  < 100 )
				{
					zUBASIS.Format (_T("%d   %s"),trapoko.staat, ptwer2);
				}
				else	// Notbremse
					zUBASIS.Format (_T("%d   %s"),trapoko.staat, ptwer2);
			}
			FillList.SetItemText (zUBASIS.GetBuffer (), i, m_list1.PosStaat);




			CString wERT;
//			m_list1.DoubleToString ( trapoko.trapoko, wERT, 0);
//			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Poslacktrapoko);


			CString pPLZ_VON;
			pPLZ_VON = trapoko.plz_von;
			FillList.SetItemText (pPLZ_VON.GetBuffer (), i, m_list1.PosPlz_von);

		m_list1.DoubleToString ( trapoko.plz_v, wERT, 0);				// 260713
		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosKun);	// 260713

//		m_list1.DoubleToString ( trapoko.plz_b, wERT, 0);
//		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosPlz_b);

			if ( trapoko_class.basissort == TRUE )
	
				m_list1.DoubleToString ( trapoko.bis1, wERT, 3);
			else
				m_list1.DoubleToString ( trapoko.bis1, wERT, 0);

			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Posbis1);
			m_list1.DoubleToString ( trapoko.wert1, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Poswert1);

			if ( trapoko_class.basissort == TRUE )
				m_list1.DoubleToString ( trapoko.bis2, wERT, 3);
			else
				m_list1.DoubleToString ( trapoko.bis2, wERT, 0);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Posbis2);
			m_list1.DoubleToString ( trapoko.wert2, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Poswert2);

			if ( trapoko_class.basissort == TRUE )
				m_list1.DoubleToString ( trapoko.bis3, wERT, 3);
			else
				m_list1.DoubleToString ( trapoko.bis3, wERT, 0);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Posbis3);
			m_list1.DoubleToString ( trapoko.wert3, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Poswert3);

			if ( trapoko_class.basissort == TRUE )
				m_list1.DoubleToString ( trapoko.bis4, wERT, 3);
			else
				m_list1.DoubleToString ( trapoko.bis4, wERT, 0);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Posbis4);
			m_list1.DoubleToString ( trapoko.wert4, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Poswert4);

			if ( trapoko_class.basissort == TRUE )
				m_list1.DoubleToString ( trapoko.bis5, wERT, 3);
			else
				m_list1.DoubleToString ( trapoko.bis5, wERT, 0);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Posbis5);
			m_list1.DoubleToString ( trapoko.wert5, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Poswert5);
		
			if ( trapoko_class.basissort == TRUE )
				m_list1.DoubleToString ( trapoko.bis6, wERT, 3);
			else
				m_list1.DoubleToString ( trapoko.bis6, wERT, 0);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Posbis6);
			m_list1.DoubleToString ( trapoko.wert6, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Poswert6);

			if ( trapoko_class.basissort == TRUE )
				m_list1.DoubleToString ( trapoko.bis7, wERT, 3);
			else
				m_list1.DoubleToString ( trapoko.bis7, wERT, 0);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Posbis7);
			m_list1.DoubleToString ( trapoko.wert7, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Poswert7);
/* --->
		trapoko.bis7  = 0 ;
		trapoko.wert7  = 0 ;
< ---- */
			trapoko.bis8  = 0 ;
			trapoko.wert8  = 0 ;
			trapoko.bis9  = 0 ;
			trapoko.wert9  = 0 ;
			trapoko.bis10 = 0 ;

// 230713		trapoko.wert10 = 0 ;
			m_list1.DoubleToString ( trapoko.wert10, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, m_list1.Poswert10);

			sqlret =  trapoko_class.lesealllacktrapoko() ;	
			i ++ ;
		}
	}
	else
	{	// prima neuanlage
		if ( trapoko_class.tnurland && trapoko_class.tnurplz )
		{
			FillList.SetItemText ( trapoko_class.tkopfstaat, 0, m_list1.PosStaat);
			FillList.SetItemText ( trapoko_class.tkopfplz , 0, m_list1.PosPlz_von);
			FillList.SetItemText ( v_kunnr.Trim().Left(8).GetBuffer()  , 0, m_list1.PosKun);	// 260713

		}
	}
	return TRUE;
}

BOOL CMytrapokoDlg::InList (TRAPOKO_CLASS & trapoko_class)
{
	
	m_buttkey6.EnableWindow (TRUE) ;
	m_buttkey7.EnableWindow (TRUE) ;
	m_buttkey8.EnableWindow (TRUE) ;
	m_buttkey9.EnableWindow (TRUE) ;
	m_buttkey10.EnableWindow(TRUE) ;
	m_buttkey11.EnableWindow(TRUE) ;

	ListRows.FirstPosition ();
/* --->
	CAkiPreise *aki_pr;
	while ((aki_pr = (CAkiPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Akiprgrstp.akiprgrstp.aki_nr == aki_pr->akiprgrstp.aki_nr &&
			Akiprgrstp.akiprgrstp.a == aki_pr->akiprgrstp.a) return TRUE;
	}
< ---- */

   return FALSE;
}
/* --->
void CpakdruDlg::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CSchlklkpos *pr;
	while ((pr = (CSchlklkpos *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Schlaklkp.schlaklkp, &pr->schlaklkp, sizeof (SCHLAKLKP));
		if (!InList (Schlaklkp))
		{
			Schlaklkp.dbdelete ();
		}
	}
}
< ----- */


BOOL CMytrapokoDlg::Write ()
{
//	UpdateData(TRUE ) ; erzeugt Probleme ?!

	trapoko_class.gespeichert = TRUE ;

// leere oder doppelte Saetze ignoriere ich beim Speichern

	m_list1.StopEnter ();

	int count = m_list1.GetItemCount ();

	int j = 0 ;
	int k =  0 ;

// 230713 : doppelt-weg	count = m_list1.GetItemCount ();

	long hlwtyp = atol ( leitw_typ) ;

	trapoko.leitw_typ = hlwtyp ;

	dbClass.beginwork ();

	trapoko.mdn  = DEFAULTMANDANT ;
	trapoko.leitw_typ = hlwtyp ;
	trapoko.typ = 0 ;
	if ( trapoko_class.basissort == TRUE )
		trapoko.typ = 1 ;
	trapoko_class.deletelacktrapoko() ;	// erst mal ALLES loeschen

	char dopplplz[50] ;
	short dopplstaat;	// 230713
	dopplstaat = -1;	// 230713 : dopplstaat mit auswerten
	sprintf ( dopplplz,"") ;


	for (int i = 0; i < count; i ++)
	{

		CString Text;
		char hilflacktrapoko[99] ;



		Text = m_list1.GetItemText (i, m_list1.PosPlz_von);
		sprintf ( hilflacktrapoko ,"%s" , Text.Trim().Left(8).GetBuffer()) ;
//		sprintf ( hilflacktrapoko "%s", clipped( hilflacktrapoko) );

		if (( strlen (hilflacktrapoko)) > 0 )
		{

			memcpy (&trapoko, &trapoko_null, sizeof (struct TRAPOKO));

			trapoko.mdn  = DEFAULTMANDANT ;
			trapoko.leitw_typ = hlwtyp ;
			trapoko.typ = 0 ;
			if ( trapoko_class.basissort == TRUE )
				trapoko.typ = 1 ;

			sprintf ( trapoko.plz_von ,"%s", hilflacktrapoko ) ;
			sprintf ( trapoko.plz_bis , "" ) ;

//	260713		trapoko.plz_v = trapoko.plz_b = 0 ;	// 101212

// zumindest beim einlesen wurde sortiert .....

// 230713 : Staat mit auswerten 
			Text = m_list1.GetItemText (i, m_list1.PosStaat);
			trapoko.staat = atoi (Text.GetBuffer());

// 260713 : Kunden-Nummer erg�nzen
			trapoko.plz_b = 0 ;
			Text = m_list1.GetItemText (i, m_list1.PosKun);
			trapoko.plz_v = (long) CStrFuncs::StrToDouble (Text);
			
			if (trapoko.plz_v < 1 )
			{
				sprintf ( adr.plz,"%s", trapoko.plz_von );
				adr.staat = trapoko.staat;
				trapoko.plz_v = trapoko_class.firstleseallkun(0);
			}

//	230714 : dopplstaat dazu		if (strcmp ( trapoko.plz_von , dopplplz )) 
			if ((strcmp ( trapoko.plz_von , dopplplz )) || (trapoko.staat != dopplstaat)) 
			{
				sprintf ( dopplplz ,"%s", trapoko.plz_von ) ;
				dopplstaat = trapoko.staat ;	// 230713

				Text = m_list1.GetItemText (i, m_list1.Posbis1);
				if ( trapoko.typ == 1 )
					trapoko.bis1 = CStrFuncs::StrToDouble (Text);
				else
					trapoko.bis1 = (long) CStrFuncs::StrToDouble (Text);
				if ( trapoko.bis1 < 0 ) trapoko.bis1 = 0 ;
				if ( trapoko.bis1 > 9999999 ) trapoko.bis1 = 9999999 ;
				Text = m_list1.GetItemText (i, m_list1.Poswert1);
				trapoko.wert1 = CStrFuncs::StrToDouble (Text);
				if ( trapoko.wert1 < -9999999 ) trapoko.wert1 = -9999999 ;
				if ( trapoko.wert1 >  9999999 ) trapoko.wert1 =  9999999 ;

				Text = m_list1.GetItemText (i, m_list1.Posbis2);
				if ( trapoko.typ == 1 )
					trapoko.bis2 = CStrFuncs::StrToDouble (Text);
				else
					trapoko.bis2 = (long) CStrFuncs::StrToDouble (Text);
				if ( trapoko.bis2 < 0 ) trapoko.bis2 = 0 ;
				if ( trapoko.bis2 > 9999999 ) trapoko.bis2 = 9999999 ;
				Text = m_list1.GetItemText (i, m_list1.Poswert2);
				trapoko.wert2 = CStrFuncs::StrToDouble (Text);
				if ( trapoko.wert2 < -9999999 ) trapoko.wert2 = -9999999 ;
				if ( trapoko.wert2 >  9999999 ) trapoko.wert2 =  9999999 ;

				Text = m_list1.GetItemText (i, m_list1.Posbis3);
				if ( trapoko.typ == 1 )
					trapoko.bis3 = CStrFuncs::StrToDouble (Text);
				else	
					trapoko.bis3 = (long) CStrFuncs::StrToDouble (Text);
				if ( trapoko.bis3 < 0 ) trapoko.bis3 = 0 ;
				if ( trapoko.bis3 > 9999999 ) trapoko.bis3 = 9999999 ;
				Text = m_list1.GetItemText (i, m_list1.Poswert3);
				trapoko.wert3 = CStrFuncs::StrToDouble (Text);
				if ( trapoko.wert3 < -9999999 ) trapoko.wert3 = -9999999 ;
				if ( trapoko.wert3 >  9999999 ) trapoko.wert3 =  9999999 ;

				Text = m_list1.GetItemText (i, m_list1.Posbis4);
				if ( trapoko.typ == 1 )
					trapoko.bis4 = CStrFuncs::StrToDouble (Text);
				else
					trapoko.bis4 = (long) CStrFuncs::StrToDouble (Text);
				if ( trapoko.bis4 < 0 ) trapoko.bis4 = 0 ;
				if ( trapoko.bis4 > 9999999 ) trapoko.bis4 = 9999999 ;
				Text = m_list1.GetItemText (i, m_list1.Poswert4);
				trapoko.wert4 = CStrFuncs::StrToDouble (Text);
				if ( trapoko.wert4 < -9999999 ) trapoko.wert4 = -9999999 ;
				if ( trapoko.wert4 >  9999999 ) trapoko.wert4 =  9999999 ;

				Text = m_list1.GetItemText (i, m_list1.Posbis5);
				if ( trapoko.typ == 1 )
					trapoko.bis5 = CStrFuncs::StrToDouble (Text);
				else
					trapoko.bis5 = (long) CStrFuncs::StrToDouble (Text);
				if ( trapoko.bis5 < 0 ) trapoko.bis5 = 0 ;
				if ( trapoko.bis5 > 9999999 ) trapoko.bis5 = 9999999 ;
				Text = m_list1.GetItemText (i, m_list1.Poswert5);
				trapoko.wert5 = CStrFuncs::StrToDouble (Text);
				if ( trapoko.wert5 < -9999999 ) trapoko.wert5 = -9999999 ;
				if ( trapoko.wert5 >  9999999 ) trapoko.wert5 =  9999999 ;

				Text = m_list1.GetItemText (i, m_list1.Posbis6);
				if ( trapoko.typ == 1 )
					trapoko.bis6 = CStrFuncs::StrToDouble (Text);
				else
					trapoko.bis6 = (long) CStrFuncs::StrToDouble (Text);
				if ( trapoko.bis6 < 0 ) trapoko.bis6 = 0 ;
				if ( trapoko.bis6 > 9999999 ) trapoko.bis6 = 9999999 ;
				Text = m_list1.GetItemText (i, m_list1.Poswert6);
				trapoko.wert6 = CStrFuncs::StrToDouble (Text);
				if ( trapoko.wert6 < -9999999 ) trapoko.wert6 = -9999999 ;
				if ( trapoko.wert6 >  9999999 ) trapoko.wert6 =  9999999 ;

				Text = m_list1.GetItemText (i, m_list1.Posbis7);
				if ( trapoko.typ == 1 )
					trapoko.bis7 = CStrFuncs::StrToDouble (Text);
				else
					trapoko.bis7 = (long) CStrFuncs::StrToDouble (Text);
				if ( trapoko.bis7 < 0 ) trapoko.bis7 = 0 ;
				if ( trapoko.bis7 > 9999999 ) trapoko.bis7 = 9999999 ;
				Text = m_list1.GetItemText (i, m_list1.Poswert7);
				trapoko.wert7 = CStrFuncs::StrToDouble (Text);
				if ( trapoko.wert7 < -9999999 ) trapoko.wert7 = -9999999 ;
				if ( trapoko.wert7 >  9999999 ) trapoko.wert7 =  9999999 ;

				trapoko.bis8  = 0 ;
				trapoko.wert8  = 0 ;
				trapoko.bis9  = 0 ;
				trapoko.wert9  = 0 ;
				trapoko.bis10 = 0 ;
// 230713				trapoko.wert10 = 0 ;
				Text = m_list1.GetItemText (i, m_list1.Poswert10);
				trapoko.wert10 = CStrFuncs::StrToDouble (Text);
				if ( trapoko.wert10 < -9999999 ) trapoko.wert10 = -9999999 ;
				if ( trapoko.wert10 >  9999999 ) trapoko.wert10 =  9999999 ;

// 230713 : muss nach oben wegen Index ?!	Text = m_list1.GetItemText (i, m_list1.PosStaat);

				trapoko_class.insertlacktrapoko();
			}
		}
	}
	dbClass.commitwork ();
	m_comboleitwtyp.SetFocus() ;

	return TRUE;
}



/* + * + * + * ---->
BOOL CMylacktrapokoDlg::Write ()
{
//	UpdateData(TRUE ) ; erzeugt Probleme ?!

//	DbClass.beginwork ();

	m_list1.StopEnter ();

	int count = m_list1.GetItemCount ();

	int j = 0 ;
	int k =  0 ;
	char hilfepuffer[3099] ;	
	
// 141009 : gr�ssere Anzahl rows korrekt handeln , bei altem Ablauf droht sonst Datenverlust

	double startwert = 0.0 ;
	double endwert   = 0.0 ;
// Der Mechanismus funktioniert nur korrekt bei aufsteigend sortierter Liste


	int allesleer = 0 ;	// 020211

	int i = 0 ;
	while ( i < count )
	{

		j = 0 ;
		k =  0 ;
		hilfepuffer[0] = '\0' ;

		for ( ; i < count; i ++)
		{
			CString Text;
			Text = m_list1.GetItemText (i, m_list1.PosArtNr);
			double  hilfart = CStrFuncs::StrToDouble (Text);
			if ( hilfart > 0 )
			{
				if ( k > 0 )
				{
					sprintf ( hilfepuffer + k ,",%1.0f", hilfart ) ;
				}
				else	// erster Eintrag ohne komma 
				{
					sprintf ( hilfepuffer + k ,"%1.0f", hilfart ) ;
				}
				j++ ;
				k = (int) strlen ( hilfepuffer ) ;
				endwert = hilfart ;

			}
			else	// 020211 
			{
				// das folgende ist doppelmoppel aber ansonsten ok.
				if ( hilfart == 0.0 && count == 1 )
				allesleer = 1 ;
			}
				
			if ( k > 3080 )
			{	
				i ++ ;
				break ;	// Notbremse, falls mehr als mindestens 2080 /14 = 148 
									// oder entsprechend mehr bei Artikelnummern mit weniger als 13 stellen
			}
		}	// string aufbauen
		if ( j )
		{
			// redundante Saetze loeschen 
			sprintf ( b_kun.inakun,"%s",hilfepuffer ) ;
			a_kun.mdn = mdn.mdn ;
			a_kun.kun = kun.kun ;
			sprintf ( a_kun.kun_bran2 ,"%s", kunbran2 ) ;
			b_kun.vonwert = startwert ;
			b_kun.biswert = endwert ;
			startwert = endwert + 1 ;
			a_kun_class.deletea_kun() ;
		}
		else
		{
			if ( allesleer )
			{
				allesleer = 0 ;
				sprintf ( b_kun.inakun,"-2" ) ;
				a_kun.mdn = mdn.mdn ;
				a_kun.kun = kun.kun ;
				sprintf ( a_kun.kun_bran2 ,"%s", kunbran2 ) ;
				b_kun.vonwert = 0 ;
				b_kun.biswert = 999999999999 ;
				startwert = endwert + 1 ;
				a_kun_class.deletea_kun() ;
			}

		}
	}	// bis alle Posten erledigt sind 

	count = m_list1.GetItemCount ();

	for (int i = 0; i < count; i ++)
	{

		CString Text;
		Text = m_list1.GetItemText (i, m_list1.PosArtNr);
		double  hilfart = CStrFuncs::StrToDouble (Text);
		if ( hilfart > 0 )
		{

			memcpy (&a_kun, &a_kun_null, sizeof (struct A_KUN));
			a_kun.mdn = mdn.mdn ;
			a_kun.kun = kun.kun ;
			sprintf ( a_kun.kun_bran2 ,"%s", kunbran2 ) ;
			a_kun.a = hilfart ;
			int j = a_kun_class.opena_kun() ;
			j = a_kun_class.lesea_kun() ;

			Text = m_list1.GetItemText (i, m_list1.PosA_bz1);
			sprintf ( a_kun.a_bz1 , "%s" , Text.Trim().Left(24).GetBuffer());

			Text = m_list1.GetItemText (i, m_list1.PosA_bz2);
			sprintf ( a_kun.a_bz2 , "%s" , Text.Trim().Left(24).GetBuffer());

			Text = m_list1.GetItemText (i, m_list1.PosA_kun);
			sprintf ( a_kun.a_kun , "%s" , Text.Trim().Left(16).GetBuffer());

			Text = m_list1.GetItemText (i, m_list1.PosKun_inh);
			a_kun.inh =CStrFuncs::StrToDouble (Text);
			if ( a_kun.inh < -9999 ) a_kun.inh = -9999 ;
			if ( a_kun.inh > 99999 ) a_kun.inh = 99999 ;

			Text = m_list1.GetItemText (i, m_list1.PosKun_me_einh);
			a_kun.me_einh_kun =atoi (Text.GetBuffer());

			Text = m_list1.GetItemText (i, m_list1.PosGeb_fakt);
			a_kun.geb_fakt =CStrFuncs::StrToDouble (Text);
			if ( a_kun.geb_fakt < -9999999 ) a_kun.geb_fakt = -9999999 ;
			if ( a_kun.geb_fakt > 99999999 ) a_kun.geb_fakt = 99999999 ;

			Text = m_list1.GetItemText (i, m_list1.PosEan);
			a_kun.ean =CStrFuncs::StrToDouble (Text);
			if ( a_kun.ean < -999999999999) a_kun.ean = -999999999999 ;
			if ( a_kun.ean > 9999999999999) a_kun.ean = 9999999999999 ;

			Text = m_list1.GetItemText (i, m_list1.PosEan_VK);
			a_kun.ean_vk =CStrFuncs::StrToDouble (Text);
			if ( a_kun.ean_vk < -999999999999) a_kun.ean_vk = -999999999999 ;
			if ( a_kun.ean_vk > 9999999999999) a_kun.ean_vk = 9999999999999 ;

			Text = m_list1.GetItemText (i, m_list1.PosLi_a);
			sprintf ( a_kun.li_a , "%s" , Text.Trim().Left(12).GetBuffer());

			if ( !j )
			{
				a_kun_class.upda_kun () ;
			}
			else
			{
				a_kun.a = hilfart ;
				a_kun.mdn = mdn.mdn ;
				a_kun.kun = kun.kun ;
				sprintf ( a_kun.kun_bran2 ,"%s", kunbran2 ) ;
				a_kun_class.inserta_kun ();
			}
		}
	}
			
//			memcpy ( &fracht.lief_term, &lsk.lieferdat, sizeof (  TIMESTAMP_STRUCT )) ;
//			memcpy ( &fracht.dat       , &lsk.lieferdat, sizeof ( TIMESTAMP_STRUCT )) ;

	if ( kun.kun > 0 )
		m_kundnr.SetFocus() ;
	else
		m_mdnnr.SetFocus() ;

	return TRUE;
}
< -* + * + * + * + ----- */




void CMytrapokoDlg::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	Rows.Init ();
}

DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}

BOOL CMytrapokoDlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

/* ---> wird beim killfocusmdnnr organisiert , dort ist dann auch der input aktuell ...
	if (Control == &m_mdnnr)
	{	
		if (!ReadMdn ())
		{	m_mdnnr.SetFocus ();
			return FALSE;
		}
	}
< ---- */

	/* ----->
	if (Control == &m_schlklknr)
	{	if (!Read ())
		{	m_schlklknr.SetFocus ();
			return FALSE;
		}
	}
	< ------ */
	/* ----->
	if (Control == &m_ckalkart)
	{	kalkartfeldernachsetzen ();
		rechnenach (); 
	}
	< -------- */

	/* ---> 
	if (Control == &m_preisek ||
		Control == &m_anzahl ||
		Control == &m_lebendgew ||
		Control == &m_schlachtgewicht ||
		Control == &m_kaltgew ||
		Control == &m_ausbeute ||
		Control == &m_verlust )
	{
		rechnenach () ;
	}
	< ---- */

	if (Control != &m_list1 &&
		Control->GetParent ()!= &m_list1)
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CMytrapokoDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_list1 &&
		Control->GetParent ()!= &m_list1 )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_PrGrList.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}

void CMytrapokoDlg::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Write () ;
	Read  () ;
	v_message = "Daten wurden gespeichert" ;
	i_message = 0 ;
	geradeaufland = 0 ;	// 230713

	UpdateData(FALSE) ;

	//	-> verlassen nur mit Abbrechen !! OnOK();
}

void CMytrapokoDlg::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

	OnCancel();
}
/* brauchet mer nitte
void CMytrapokoDlg::OnEnKillfocusMdnnr()
{
	UpdateData (TRUE) ;

	int i = m_mdnnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 191110
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{


		v_mdnname.Format("              ");

		if ( mdn.mdn < 0 || mdn.mdn > 9999 )	// 030211
		{
			v_mdnnr = 1 ;
			UpdateData (FALSE) ;
		}
		else
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);

		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;
}
< ----- */

void CMytrapokoDlg::keyset(BOOL schalter)
{
	schalter = TRUE ;	// immer anschalten, der Rest organsiert sich duch m_list1
	m_buttkey6.EnableWindow (schalter) ;
	m_buttkey7.EnableWindow (schalter) ;
	m_buttkey8.EnableWindow (schalter) ;
	m_buttkey9.EnableWindow (schalter) ;
	m_buttkey10.EnableWindow(schalter) ;
	m_buttkey11.EnableWindow(schalter) ;
}

/* ---> brauchet mer nicht
void CMytrapokoDlg::enablekunbran(void) 
{
	// Kundenbranche enablen, Kunde disablen 
		m_combobran.EnableWindow (TRUE) ;
		m_combobran.ModifyStyle (0, WS_TABSTOP,0) ;
		m_kundnr.EnableWindow (FALSE) ;
		m_kundnr.ModifyStyle (WS_TABSTOP,0,0) ;
		m_buttonkun.EnableWindow (FALSE) ;
//		m_buttonkun.ModifyStyle (WS_TABSTOP,0,0) ;
}

void CMytrapokoDlg::disablekunbran(void) 
{

	// Kundenbranche disablen, Kunde enablen 
		m_combobran.EnableWindow (FALSE) ;
		m_combobran.ModifyStyle (WS_TABSTOP,0,0) ;
		m_kundnr.EnableWindow (TRUE) ;
		m_kundnr.ModifyStyle (0,WS_TABSTOP,0) ;
		m_buttonkun.EnableWindow (TRUE) ;
//		m_buttonkun.ModifyStyle (0,WS_TABSTOP,0) ;
}
void CMytrapokoDlg::setzekunbran0(void) 
{

	sprintf ( bufh, "0" );
	int nCurSel = -1 ;
//	v_kundnr = 0 ;
//	kun.kun = 0 ;
//	v_kundname.Format("Kundenbranche") ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOBRAN))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_combobran = bufx ;
		UpdateData(FALSE) ;
	}
	if ( ! branrekursion )
	{
		branrekursion = 1 ;
		CMytrapokoDlg::OnCbnSelchangeCombobran();
		branrekursion = 0 ;
	}
}
< ------- */

void CMytrapokoDlg::setzeleitw_typ3(void) 
{

	sprintf ( bufh, "3" );
	int nCurSel = -1 ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_comboleitwtyp = bufx ;
		sprintf ( leitw_typ , "3" ) ;
		UpdateData(FALSE) ;
	}
	if ( ! branrekursion )
	{
		branrekursion = 1 ;
		CMytrapokoDlg::OnCbnSelchangeComboleitwtyp();
		branrekursion = 0 ;
	}
}
void CMytrapokoDlg::setzestaat_leer(void) 
{

	sprintf ( bufh, " " );
	int nCurSel = -1 ;
	nCurSel = 0 ;	// erster Satz

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_combostaat = bufx ;
		sprintf ( kopfstaat , " " ) ;
		UpdateData(FALSE) ;
	}
}



void CMytrapokoDlg::OnCbnKillfocusComboleitwtyp()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE) ;

	geradeaufland = 0 ;	// 230713
	if ( i_message > 0 )
	{
		i_message -- ;
		v_message = "Daten wurden gespeichert" ;
		UpdateData(FALSE) ;
	}
	else
		v_message = " " ;	// 060513


	sprintf ( bufh, "%s", v_comboleitwtyp );
	int nCurSel = -1 ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_comboleitwtyp = bufx ;
		UpdateData(FALSE) ;
		if ( ! branrekursion )
		{
			branrekursion = 1 ;
			CMytrapokoDlg::OnCbnSelchangeComboleitwtyp();
			branrekursion = 0 ;
		}
		Read() ;
	}
	else
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		sprintf ( leitw_typ ,"-99" )	;	// unm�glicher wert, leitw_typ ist nur 2 zeichen gross ....

		PrevDlgCtrl();
	}
	UpdateData(FALSE) ;
}

void CMytrapokoDlg::OnCbnSelchangeComboleitwtyp()
{

	int nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->GetCurSel();
	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOLEITWTYP))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_comboleitwtyp = bufx ;
		int i = (int) strlen ( bufh );
		int j,k ;
		j = k = 0 ;
		if ( i )
		{
//			int j, k ; 
			j = k = 0 ;
			for (j=k=0  ;j < i;j++ )
			{
				if ( bufh[j] == ' ') 
				{
					if ( k)
					{
						bufh[j] = '\0' ;
						break ;
					}
				}
				else
				{
					k ++ ;	// nicht-Space
				}
			}
		}
		bufh[2] = '\0' ;	// jedenfalls kappen
		sprintf ( leitw_typ, "%s", bufh ) ;
/* ----->
		if ( ( k == 0)  || ( k == 1 && ! strncmp ( bufh,"0",1)))
		{
			sprintf ( leitw_typ, "0" ) ;
			setzekunbran0() ;
			disablekunbran() ;
		}
		else
			enablekunbran() ;
< ---- */	

	}
	else
	{
// das folgende wird bei killfocus besser abgearbeitet .....
		sprintf ( leitw_typ ,"-99" )	;	// unm�glicher wert, kun_bran2 ist nur 2 zeichen gross ....
//		sprintf ( kunbran2, "0" ) ;
//		setzekunbran0() ;
//		disablekunbran() ;
	}

	UpdateData(FALSE ) ;


}


void CMytrapokoDlg::OnCbnSetfocusComboleitwtyp()
{
//	UpdateData(TRUE);
	keyset(FALSE) ;
//	UpdateData(FALSE);
}


void CMytrapokoDlg::OnBnClickedBdruck()
{

// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Write () ;	// Ich nehme mal an, hier wollte einer vorher speichern ?!
	v_message = "Daten wurden gespeichert" ;

// Starten der druckerei , 70001.exe verzweigt eigenst�ndig auf alt oder neu .......
// Leider haben wir nur noch neu ......

	char s1[256] ;
	sprintf ( s1, "dr70001.exe  -name trapoko" );
	int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
}

void CMytrapokoDlg::OnBnClickedButtkey7()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	// erst mal gar nix, weil ich ja nie weiss, ob ich in der Listenbearbeitung bin oder doch nicht......
}
void CMytrapokoDlg::OnBnClickedButtkey6()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	// erst mal gar nix, weil ich ja nie weiss, ob ich in der Listenbearbeitung bin oder doch nicht......
}

void CMytrapokoDlg::OnBnClickedCheck1()
{

	UpdateData(TRUE);
	if ( v_check1 == TRUE )
	{
		if ( trapoko_class.basissort == FALSE )
		{
//			trapoko_class.schliessen () ;
			if ( ! trapoko_class.gespeichert )
			{
				if ( MessageBox("�nderungen speichern ?? ", " ", MB_YESNO ) == IDYES )
				{
					Write () ;
					v_message = "Daten wurden gespeichert" ;
//					m_message.SetDlgItemTextA( IDC_MESSAGE ,"Daten wurden gespeichert" ) ;
					i_message = 1 ;

				}
			}
			trapoko_class.basissort = TRUE ;
			OnFillList() ;
			Read() ;
			UpdateData(FALSE);
			m_comboleitwtyp.SetFocus() ;
			return ;
		}
	}
	else
	{
		if ( trapoko_class.basissort == TRUE )
		{
//			trapoko_class.schliessen () ;

			if ( ! trapoko_class.gespeichert )
			{
				if ( MessageBox("�nderungen speichern ?? ", " ", MB_YESNO ) == IDYES )
				{
					Write () ;
					v_message = "Daten wurden gespeichert" ;
//					m_message.SetDlgItemTextA( IDC_MESSAGE, "Daten wurden gespeichert") ;

					i_message = 1 ;
				}

			}
			trapoko_class.basissort = FALSE ;
			OnFillList() ;
			Read() ;
			UpdateData(FALSE);
			m_comboleitwtyp.SetFocus() ;
			return ;
		}
	}
	UpdateData(FALSE);
}

void CMytrapokoDlg::OnCbnKillfocusCombostaat()
{
	UpdateData(TRUE) ;

	if ( i_message > 0 )
	{
		i_message -- ;
		v_message = "Daten wurden gespeichert" ;
		UpdateData(FALSE) ;
	}
	else
		v_message = " " ;

	geradeaufland = 1;	// 230713

	sprintf ( bufh, "%s", v_combostaat );
	int nCurSel = -1 ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_combostaat = bufx ;
		sprintf ( kopfstaat , "%s" , v_combostaat.GetBuffer(5) );
		v_kunnr = " " ;
		v_kplz = " " ;
		UpdateData(FALSE) ;
		Read() ;
		m_list1.SetFocus ();
		return ;

	}
	else
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		sprintf ( kopfstaat ," " )	;	// unm�glicher wert, kopfstaat ist nur 2 zeichen gross ....

		PrevDlgCtrl();
	}
	UpdateData(FALSE) ;
}

void CMytrapokoDlg::OnCbnSetfocusCombostaat()
{
	keyset(FALSE) ;
}

void CMytrapokoDlg::OnCbnSelchangeCombostaat()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CMytrapokoDlg::OnEnKillfocusKunnr()
{

	UpdateData (TRUE) ;

	int i = m_kunnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	kun.kun = atol ( bufh );
	else kun.kun = -2 ;

	if ( kun.kun == 0)	// kunde == 0 gibt neuerdings auch manchmal .......
		kun.kun = -2 ;
	kun.mdn = 1;	// immer == 1 f�er Lackmann
//	kun_class.openkun(0);
	if (! kun_class.lesedefkun())
	{
		adr.adr = kun.adr2 ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_kplz.Format("%s",_T(adr.plz));

				CString pTWERT;
				TCHAR ptwer2 [9] ; 
				CChoiceStaat HoleStaat ;

				pTWERT.Format (_T("%d"), adr.staat);
				HoleStaat.GetPtWer2 ( pTWERT.GetBuffer(), ptwer2 );
			v_kstaat.Format("%d %s",adr.staat,_T(ptwer2 ));	

			v_kort.Format("%s",_T(adr.ort1));
			v_kname.Format("%s%s",_T(adr.adr_nam1), _T(adr.adr_nam2));

			sprintf ( bufh , "%d", adr.staat );

			int nCurSel = -1 ;
			nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->FindString(nCurSel, bufh);
			if (nCurSel != CB_ERR)
			{
				((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->SetCurSel(nCurSel) ;
				((CComboBox *)GetDlgItem(IDC_COMBOSTAAT))->GetLBText(nCurSel, bufx);
				sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
				v_combostaat = bufx ;
				sprintf ( kopfstaat , "%s" , v_combostaat.GetBuffer(5) );

				UpdateData(FALSE) ;
				Read() ;
			}
			else
				sprintf ( kopfstaat ," " )	;
		}
		else
		{

			v_kunnr.Format(" " );
			v_kplz.Format(" ");
			v_kort.Format(" ");
			v_kname.Format(" ");

			if ( geradeaufland == 0 )
			{
				sprintf ( kopfstaat , "  " );
				v_combostaat = "" ;
				v_kstaat.Format(" ");
			}

			geradeaufland = 0 ;
			UpdateData(FALSE) ;	// 230713
			Read() ;
//			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
			v_kunnr.Format(" " );
			v_kplz.Format(" ");
			v_kort.Format(" ");
			v_kname.Format(" ");
			if ( geradeaufland == 0 )
			{
				v_combostaat = " "  ;
				sprintf ( kopfstaat , " " );
				v_kstaat.Format(" ");
			}
			geradeaufland = 0 ;
			UpdateData(FALSE) ;	// 230713
			Read() ;

	}

	m_list1.SetFocus ();
	UpdateData (FALSE) ;
}
