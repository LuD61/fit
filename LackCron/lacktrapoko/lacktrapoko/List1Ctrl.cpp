#include "StdAfx.h"
#include "List1Ctrl.h"
#include "StrFuncs.h"
#include "resource.h"
#include "lacktrapokoDlg.h"
#include "lacktrapokotab.h"
#include "lacktrapokotab.h"
#include "adr.h"	// 260713
#include "kun.h"	// 260713

CList1Ctrl::CList1Ctrl(void)
{
//	m_mdn = 1;
//	Poslacktrapoko		= POSlacktrapoko;
//	PosPlz_v		= POSPLZ_V;
//	PosPlz_b		= POSPLZ_B;

	PosStaat		= POSStaat;	// 060513

	PosPlz_von		= POSPLZ_von;

	PosKun		= POSKun;	// 230713

	Posbis1			= POSbis1;
	Poswert1		= POSwert1;
	Posbis2			= POSbis2;
	Poswert2		= POSwert2;
	Posbis3			= POSbis3;
	Poswert3		= POSwert3;
	Posbis4			= POSbis4;
	Poswert4		= POSwert4;
	Posbis5			= POSbis5;
	Poswert5		= POSwert5;
	Posbis6			= POSbis6;
	Poswert6		= POSwert6;
	Posbis7			= POSbis7;
	Poswert7		= POSwert7;
	Poswert10		= POSwert10;	// 230713



//	Position[0] = &Poslacktrapoko;
	Position[0] = &PosStaat;
	Position[1] = &PosPlz_von;
// 101212 	Position[1] = &PosPlz_b; + alles eins hoch
	// 230713 : Poskun und poswert10 ergaenzt

	Position[2] = &PosKun;
	Position[3] = &Posbis1;
	Position[4] = &Poswert1;
	Position[5] = &Posbis2;
	Position[6] = &Poswert2;
	Position[7] = &Posbis3;
	Position[8] = &Poswert3;
	Position[9] = &Posbis4;
	Position[10] = &Poswert4;
	Position[11] = &Posbis5;
	Position[12] = &Poswert5;
	Position[13] = &Posbis6;
	Position[14] = &Poswert6;
	Position[15] = &Posbis7;
	Position[16] = &Poswert7;
	Position[17] = &Poswert10;

	Position[18] = NULL;

	MaxComboEntries = 50;

	ListRows.Init ();
}

CList1Ctrl::~CList1Ctrl(void)
{

	CString *c;
    StaatCombo.FirstPosition ();
	while ((c = (CString *) StaatCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	StaatCombo.Init ();


    KundeCombo.FirstPosition ();
	while ((c = (CString *) KundeCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	KundeCombo.Init ();

}

BEGIN_MESSAGE_MAP(CList1Ctrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


/* ---->
void CList1Ctrl::SetWerte ( long anzahl, double kaltgew, long schlklknr )
{
	if (( lianzahl != anzahl ) || ( dikaltgew != kaltgew )|| ( lischlklknr != schlklknr ))
	{
		CString wERTKG ;
		CString wERT;
		CString zUBASIS ;
		CString zUSCHLAG ;

		double dizuschlag ;
		int dibasis ;
		double diwert ;
		double diwertkg ;

		dizuabkg = 0.0 ; 

		int i = GetItemCount () ;
		if ( i > 0 )
		{
			lischlklknr = schlklknr ;
		};
		lianzahl = anzahl ;	
		dikaltgew = kaltgew ;
		
		while  ( i > 0 )
		{
			i -- ;

 			zUSCHLAG =  FillList.GetItemText (i, PosZuschlag);
			dizuschlag = CStrFuncs::StrToDouble (zUSCHLAG);
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (i, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	 		switch ( dibasis )
			{
//			case 0 :	// je kg
			case 1 :	// je Stck.
				diwert = dizuschlag * lianzahl ;
				if ( dikaltgew != 0.0 )
				{
					diwertkg = (dizuschlag *	lianzahl ) / dikaltgew ;
				}
				else
				{
					diwertkg = 0.0 ;
				}
			break ;
			case 2 :	// pauschal
				diwert = dizuschlag ;
				if ( dikaltgew != 0.0 )
				{
					diwertkg = dizuschlag / dikaltgew ;
				}
				else
				{	
					diwertkg = 0.0 ;
				}
			break ;

			default :	// auffang-Linie : je kg
				diwert = dizuschlag * dikaltgew ;
				diwertkg = dizuschlag ;
			break ;
			}

			DoubleToString (diwert, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, PosWert);

			DoubleToString (diwertkg, wERTKG, 4);
			FillList.SetItemText (wERTKG.GetBuffer (), i, PosWertkg);
			dizuabkg += diwertkg ;

		}
		Zuschlagsetzen  ( ) ;
	}
	else
	{
		lianzahl = anzahl ;
		dikaltgew = kaltgew ;
		lischlklknr = schlklknr ;
	}
}
< ---- */
/* ----->
void CKstArtListCtrl::Zuschlagsetzen (void) 
{
 CSchlakaDlg * CElFenst ;
CElFenst = ( CSchlakaDlg *) GetParent () ;
CElFenst->SetzeEkzerleg (dizuabkg ) ;
}
< ---- */

void CList1Ctrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (PosPlz_von, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
//		StartEnter (Poslacktrapoko, 0);
		StartEnter (PosPlz_von, 0);
	}
}

void CList1Ctrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
//	if (col == PosArtBez) return;
/* ---> es gibt keine Kriterien mehr ..........
	if ( col == PosArtNr )
	{
		CString cArtNr = GetItemText (row, PosArtNr);
		if (StrToDouble (cArtNr) > 0  )	// Artikel gefunden/vorhanden 
		{
			return ;
		}
		CEditListCtrl::StartEnter (col, row);

	}


/* --->
	if (col == PosAktiv)
	{
		CEditListCtrl::StartEnterCheckBox (col, row);
		return ;
	}
< ---- */

	CEditListCtrl::StartEnter (col, row);


	if (col == PosStaat)
	{

		if (IsWindow (ListComboBox.m_hWnd))
		{
			StopEnter ();
		}


//		CString zUBASIS ;
//		zUBASIS =  FillList.GetItemText (row, PosStaat);	// == col //
		
//		int nCurSel = ListComboBox.FindString( 0,zUBASIS.GetBuffer(0));

		CEditListCtrl::StartEnterCombo (col, row, &StaatCombo);

//		if (nCurSel > 0)
//			ListComboBox.SetCurSel( nCurSel ) ;
//		else
//			ListComboBox.SetCurSel( 0 ) ;
//		int oldsel = ListComboBox.GetCurSel ();

		return ;
	}

// 260713 A
	if (col == PosKun)
	{

		if (IsWindow (ListComboBox.m_hWnd))
		{
			StopEnter ();
		}


// 260713
		CVector Values;
		Values.Init ();

		int allesuchen = 0 ;
		CString zXXX ;
		zXXX =  FillList.GetItemText (row, PosStaat);
		adr.staat = atoi ( zXXX.GetBuffer() ) ;
		zXXX =  FillList.GetItemText (row, PosPlz_von);
		sprintf (adr.plz ,"%s", zXXX.GetBuffer() );
		if ( adr.plz[0] == '\0' )
		{
			allesuchen = 1 ;
			allesuchen = 0; // zunächst deaktivieren
		}

		long i = trapoko_class.firstleseallkun ( allesuchen ) ;

		if (i)
			i= 0; 
		else
			i = 100 ;

		while ( ! i )
		{
			CString *Value = new CString ();
			Value->Format ("%d  %s", kun.kun, kun.kun_krz1);
			Values.Add (Value);
			i = trapoko_class.nextleseallkun() ;
		}
		FillKundeCombo (Values);

// 260713 E

		CEditListCtrl::StartEnterCombo (col, row, &KundeCombo);
		
		if ( allesuchen ) 
		{
			// hier kann später eine "rumschnipsung noch hinein
		}
		return ;
	}

// 260713 E


/* ---->
	if (col == PosZuschlag || col == PosKost_bez)
	{
		CEditListCtrl::StartEnter (col, row);
	}
< ----- */

}

void CList1Ctrl::StopEnter ()
{

	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = _T("");
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
/* --->
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		int check = ListCheckBox.GetCheck ();
        if (check == BST_CHECKED)
		{
			FillList.SetItemText (_T("X"), EditRow, EditCol);
		}
		else
		{
			FillList.SetItemText (_T(" "), EditRow, EditCol);
		}
		ListCheckBox.DestroyWindow ();
	}
< ---- */

	/* ---->
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	< ----- */
}

void CList1Ctrl::SetSel (CString& Text)
{

 //  if (EditCol == Poslacktrapoko )   // EditCol == PosArtNr
   if (EditCol == PosPlz_von )   // EditCol == PosArtNr
   {
		ListEdit.SetSel (0, -1);
   }
/* ----->
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
//		ListEdit.SetSel (cpos, cpos);
   }
< ------ */
   else
   {
	   
	   
	   if (EditCol != PosStaat && EditCol != PosKun )	// 260713 : poskun dazu
		{
 					// alles numerische Felder
					ListEdit.SetSel (0, -1);
		}
   }
}

void CList1Ctrl::FormatText (CString& Text)
{

	if (EditCol == PosPlz_von)
	{
		Text.Trim().Left(8).GetBuffer();
	}

/* ----
 if (EditCol == Poslacktrapoko)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
 < ------ */

/* ---->
  if (EditCol == PosPlz_v)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
  if (EditCol == PosPlz_b)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
< ---- */

  if (EditCol == Poswert1 
		|| EditCol == Poswert2 
		|| EditCol == Poswert3 
		|| EditCol == Poswert4 
		|| EditCol == Poswert5 
		|| EditCol == Poswert6 
		|| EditCol == Poswert7
		|| EditCol == Poswert10	// 230713
	  )
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
  if (EditCol == Posbis1 
		|| EditCol == Posbis2 
		|| EditCol == Posbis3 
		|| EditCol == Posbis4 
		|| EditCol == Posbis5 
		|| EditCol == Posbis6 
		|| EditCol == Posbis7 
	  )
	{
		if (trapoko_class.basissort == TRUE )
			DoubleToString (StrToDouble (Text), Text, 3);
		else
			DoubleToString (StrToDouble (Text), Text, 0);
	}
 
  /* ---->
  if (EditCol == PosKun_inh)
	{
		DoubleToString (StrToDouble (Text), Text, 3);
	}

< ----- */

/* --->
   if (EditCol == PosZuschlag || EditCol == PosZubasis )
   {
	   double dizuschlag ;
	   short dibasis ;
	   if ( EditCol == PosZuschlag )
	   {
			dizuschlag = StrToDouble (Text) ;
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (EditRow, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	   }
	   else
	   {

 			CString zUSCHLAG =  FillList.GetItemText (EditRow, PosZuschlag);
			dizuschlag = CStrFuncs::StrToDouble (zUSCHLAG);
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (EditRow, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	   }
		double diposwert, diposwertkg ;
		switch ( dibasis )
		{
//		case 0 :	// je kg
		case 1 :	// je Stck.
			diposwert = dizuschlag * lianzahl ;
			if ( dikaltgew != 0.0 )
			{
			    diposwertkg = (dizuschlag *	lianzahl ) / dikaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;
		case 2 :	// pauschal
			diposwert = dizuschlag ;
			if ( dikaltgew != 0.0 )
			{
			    diposwertkg = dizuschlag / dikaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;

		default :	// auffang-Linie : je kg
		    diposwert = dizuschlag * dikaltgew ;
			diposwertkg = dizuschlag ;
			break ;
		}

		CString wERT;
		DoubleToString (diposwert, wERT, 2);
//		Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
// 		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosWert);
		FillList.SetItemText (wERT.GetBuffer (), EditRow, PosWert);

		CString wERTKG;
		DoubleToString (diposwertkg, wERTKG, 4);
//		Schlaklkp.schlaklkp.wertkg = CStrFuncs::StrToDouble (wERTKG);
//		FillList.SetItemText (wERTKG.GetBuffer (), i, m_list1.PosWertkg);
		FillList.SetItemText (wERTKG.GetBuffer (), EditRow, PosWertkg);
	}

< ---- */

	/* --->
   CString weRTKG ;
   dizuabkg = 0.0 ; 
 	for (int i = GetItemCount()  ; i > 0 ; )
	{
		i -- ;
		weRTKG =  FillList.GetItemText (i, PosWertkg);
		dizuabkg += CStrFuncs::StrToDouble (weRTKG);
	}

	Zuschlagsetzen  ( ) ;

< ----- */

/*
    else if (EditCol == 4)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
*/

}

void CList1Ctrl::NextRow ()
{

	int count = GetItemCount ();

	BOOL ob = TRUE ;

	BOOL buggy = FALSE ;

/* -----> keine Kriterien 
	if (EditCol == PosArtNr)
	{
		ob = ReadArtikel ();
	}
< ----- */
/* ->
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< --- */


    CString Text;
	ListEdit.GetWindowText (Text);
/* -------> Leitweg z.Z. nicht aktuell
	if ( EditCol == Poslacktrapoko )
	{
		if ( atol(Text) < 1 )
		{
			ob = FALSE ;
		}
		else
		{
			CString PLZV = GetItemText (EditRow, PosPlz_v);
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			if (atol (Text.GetBuffer()) < 1  ||
				     StrToDouble ( PLZV ) < 1 || 
					 StrToDouble ( PLZB ) < 1 )
			{
				if ( EditRow >= ( count - 1 ) && StrToDouble( PLZB ) < 1 && StrToDouble ( PLZV ) < 1 )
				{
					// dann ist es erlaubt aber unmögliche Situation
				}
				else
					buggy = TRUE ;
			}
			else  
			{
				if ( StrToDouble( PLZB ) <  StrToDouble ( PLZV ) && StrToDouble ( PLZV ) > 0  )
				{
					MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
					buggy = TRUE ;
				}
				else
				{
					long lappweg = lapptest ( Text , PLZV , PLZB ) ;
					if ( lappweg )
					{
						char text[95] ;
						sprintf ( text , "Es gibt eine Überlappung mit lacktrapoko %d ", lappweg ) ;
						MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
						buggy = TRUE ;
					}
				}
			}
		}
	}
< ----- */
/* --->
	if ( EditCol == PosPlz_v )
	{

		if ( atol(Text) < 1  )
		{
			ob = FALSE ;
		}
		else
		{
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			CString LW = "1" ;	// dummy rein GetItemText (EditRow, Poslacktrapoko);
			if ( StrToDouble( PLZB ) < 1 || StrToDouble ( Text ) < 1  || StrToDouble ( LW ) < 1   )
			{
				if ( EditRow >= ( count - 1 ) && StrToDouble( PLZB ) < 1 && StrToDouble ( Text ) < 1 )
				{
					// dann ist es erlaubt
				}
				else
					buggy = TRUE ;
			}
			else if ( StrToDouble( PLZB ) < StrToDouble ( Text ) && StrToDouble ( Text ) > 0  )
			{
				MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
				buggy = TRUE ;
			}
			else 
			{
			}	
		}
	}

	if ( EditCol == PosPlz_b )
	{
		if (  atol(Text) < 1 )
		{
			ob = FALSE ;
		}
		else
		{
			CString PLZV = GetItemText (EditRow, PosPlz_v);
			CString LW = "1" ;	// dummy anstelle von GetItemText (EditRow, Poslacktrapoko);
			if ( StrToDouble( Text ) < 1 || StrToDouble ( PLZV ) < 1  || StrToDouble ( LW ) < 1  )
			{
				if ( EditRow >= ( count - 1 ) && StrToDouble( Text ) < 1 && StrToDouble ( PLZV ) < 1 )
				{
					// dann ist es erlaubt
				}
				else
					buggy = TRUE ;
			}
			else if ( StrToDouble( Text ) < StrToDouble ( PLZV ) && StrToDouble ( PLZV ) > 0  )
			{
				MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
				buggy = TRUE ;
			}
			else
			{

			}
		}
	}
< ----- */

	SetEditText ();

	count = GetItemCount ();


	if ( ob == TRUE && buggy == FALSE )
	{
		if (EditRow >= count - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
			StopEnter ();
			EditRow ++;
			EditCol = 0;

		}
		else
		{
			StopEnter ();
			EditRow ++;
		}
	}
	else
	{	// Auf Schluessel stehen bleiben ?!
//		EditCol = Poslacktrapoko ;
		EditCol = PosPlz_von ;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);

	if (GetKeyState (VK_SHIFT) < 0)
	{
		BOOL &b = vSelect[EditRow - 1];
		b = TRUE;
		FillList.SetItemImage (EditRow - 1, 1);
	}
	else if (GetKeyState (VK_CONTROL) == 0)
	{
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
	}
}

void CList1Ctrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();

	BOOL ob = TRUE ;

	BOOL buggy = FALSE ;

	if (IsWindow (ListEdit.m_hWnd))
	{
/* ----->
		if (EditCol == Poslacktrapoko)
		{
			ob = FALSE ;
			CString Text;
			ListEdit.GetWindowText (Text);
			CString PLZV = GetItemText (EditRow, PosPlz_v);
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			if (atol (Text.GetBuffer()) < 1  ||
				     StrToDouble ( PLZV ) < 1 || 
					 StrToDouble ( PLZB ) < 1 )
			{
//				DeleteRow() ;
				if ( EditRow >= ( count - 1 ) && StrToDouble( PLZB ) < 1 && StrToDouble ( PLZV ) < 1 )
				{
					// dann ist es erlaubt
				}
				else
					buggy = TRUE ;
			}
			else  
			{
				if ( StrToDouble( PLZB ) <  StrToDouble ( PLZV ) && StrToDouble ( PLZV ) > 0  )
				{
					MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
					buggy = TRUE ;
				}
				else
				{
					long lappweg = lapptest ( Text , PLZV , PLZB ) ;
					if ( lappweg )
					{
						char text[95] ;
						sprintf ( text , "Es gibt eine Überlappung mit lacktrapoko %d ", lappweg ) ;
						MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
						buggy = TRUE ;
					}
				}
			}
		}
< ----- */
	
/* ---->
		if (EditCol == PosPlz_v)
		{
			CString PLZV ;
			ListEdit.GetWindowText (PLZV);
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			CString LW = "1" ;	// dummy anstelle von GetItemText (EditRow, Poslacktrapoko);

			if ( StrToDouble( PLZB ) < 1 || StrToDouble ( PLZV ) < 1  || StrToDouble ( LW ) < 1   )
			{
				if ( EditRow >= ( count - 1 ) && StrToDouble( PLZB ) < 1 && StrToDouble ( PLZV ) < 1 )
				{
					// dann ist es erlaubt
				}
				else
					buggy = TRUE ;
			}
			else if ( StrToDouble( PLZB ) < StrToDouble ( PLZV ) && StrToDouble ( PLZV ) > 0  )
			{
				MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
				buggy = TRUE ;
			}
			else 
			{
** ---->
				long lappweg = lapptest ( LW , PLZV , PLZB ) ;
				if ( lappweg )
				{
					char text[95] ;
					sprintf ( text , "Es gibt eine Überlappung mit lacktrapoko %d ", lappweg ) ;
					MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
					buggy = TRUE ;
				}
< ---- **
			}
		}

		if (EditCol == PosPlz_b)
		{
			CString PLZB ;
			ListEdit.GetWindowText (PLZB);
			CString PLZV = GetItemText (EditRow, PosPlz_v);
			CString LW = "1" ;	// dummy anstelle von GetItemText (EditRow, Poslacktrapoko);
			if ( StrToDouble( PLZB ) < 1 || StrToDouble ( PLZV ) < 1  || StrToDouble ( LW ) < 1  )
			{
				if ( EditRow >= ( count - 1 ) && StrToDouble( PLZB ) < 1 && StrToDouble ( PLZV ) < 1 )
				{
					// dann ist es erlaubt
				}
				else
					buggy = TRUE ;
			}
			else if ( StrToDouble( PLZB ) < StrToDouble ( PLZV ) && StrToDouble ( PLZV ) > 0  )
			{
				MessageBox (_T("PLZ bis ist kleiner als PLZ von"), NULL, MB_OK | MB_ICONERROR); 
				buggy = TRUE ;
			}
			else
			{
** -------> 
				long lappweg = lapptest ( LW , PLZV , PLZB ) ;
				if ( lappweg )
				{
					char text[95] ;
					sprintf ( text , "Es gibt eine Überlappung mit lacktrapoko %d ", lappweg ) ;
					MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
					buggy = TRUE ;
				}
< ------ **
			}

		}
< ----- */
	}
	else
//		EditCol = Poslacktrapoko ;
		EditCol = PosPlz_von ;

	if (EditRow == count - 1)
	{
/* --->
		CString Zuschlag = GetItemText (EditRow, PosZuschlag);
		CString Kost_bez = GetItemText (EditRow, PosKost_bez);
		if ((StrToDouble (Zuschlag) == 0.0)
			&& (_tcslen (Kost_bez.Trim()) == 0 ))
		{
	        DeleteItem (EditRow);
		}
< ---- */
	}
	else
	{
		if (EditRow <= 0)
		{
			return;
		}
/* ---->
		if (EditCol == PosKun)
		{
			ReadKunName ();
		}
		else if (EditCol == PosKunPr)
		{
			ReadKunPr ();
		}
		else if (EditCol == PosPrGrStuf)
		{
			ReadPrGrStuf ();
		}
		TestIprIndex ();
< ---- */
	}
	StopEnter ();

/* --->
	if ( buggy == TRUE )
	{
		EditRow ++ ;
		if ( EditCol == PosPlz_b )
			EditCol -- ;
	}
< ---- */
	EditRow --;
	EnsureVisible (EditRow, FALSE);
	if ( ob == FALSE && buggy == FALSE )
		EditCol = PosPlz_von ; // Poslacktrapoko ;	// PosA_kun

	StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CList1Ctrl::LastCol ()
{

//	if (EditCol < PosPlz_b) return FALSE;
//	230713 : if (EditCol < Poswert7) return FALSE;
	if (EditCol < Poswert10) return FALSE;

	/* --->
	if (Mode == TERMIN && EditCol >= PosLdPr)
	{
		return TRUE;
	}
	if (Aufschlag == LIST || Aufschlag == ALL)
	{
		if (EditCol < PosEkProz) return FALSE;
	}
< ---- */

	return TRUE;
}

void CList1Ctrl::OnReturn ()
{

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();

	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{	// Letzte Stelle der neusten Zeile

/* ----->
		if ((StrToDouble (lacktrapoko) < 0.9 ) 
			&&	(StrToDouble (PLZV) < 0.9 )
			&&	(StrToDouble (PLZB) < 0.9 ))
		{
			EditCol --;
			return;
		}

< ----- */
	}

/* --->
	if (EditCol == PosArtNr)
	{
		ReadArtikel ();
	}
< ---- */

/* ---->
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< ---- */

	if (LastCol ())
	{
//		TestIprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}


		}

		
		// letzte Spalte extra handeln 
		CString LW = GetItemText (EditRow, PosStaat);	// 230713 : land reaktivieren und alles wird gut


		CString PLZV = GetItemText (EditRow, PosPlz_von);
		BOOL ob = FALSE ;
		

		if ( trapoko_class.tnurland && ( trapoko.staat != atoi (LW.GetBuffer()) )) 
		{	char text[95] ;
			sprintf ( text , "Nur Staat %d  erlaubt ", trapoko.staat  ) ;
			MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR);

// leere PLZ : Satz wird nicht gebucht
			FillList.SetItemText ( trapoko_class.tkopfstaat, EditRow, PosStaat);
			FillList.SetItemText ( "" , EditRow, PosPlz_von);

			ob = TRUE ;
		}
		if ( ob == FALSE)
		{
			if ( trapoko_class.tnurplz && ( strcmp (PLZV.GetBuffer(), trapoko_class.tkopfplz ))) 
			{	char text[95] ;
				sprintf ( text , "Nur PLZ %s  erlaubt ", trapoko_class.tkopfplz  ) ;
				MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 

// leere PLZ : Satz wird nicht gebucht
			FillList.SetItemText ( trapoko_class.tkopfstaat, EditRow, PosStaat);
			FillList.SetItemText ( "" , EditRow, PosPlz_von);

				ob = TRUE ;
			}
		}


		if ( ob == FALSE )
		{
			long lappweg = lapptest ( LW , PLZV  ) ;
			if ( lappweg )
			{	char text[95] ;
				sprintf ( text , "Ein Eintrag ist bereits vorhanden " ) ;
				MessageBox (_T(text), NULL, MB_OK | MB_ICONERROR); 
// leere PLZ : Satz wird nicht gebucht
			FillList.SetItemText ( trapoko_class.tkopfstaat, EditRow, PosStaat);
			FillList.SetItemText ( "" , EditRow, PosPlz_von);

				ob = TRUE ;
			}
		}
		StopEnter ();
		if ( ob == TRUE )
		{
//			EditRow -- ;
			EditCol = 0;
		}
		else
		{
			EditRow ++;
			EditCol = 0;
		
			if (EditRow == rowCount)
			{
				EditCol = PosStaat ;	// Poslacktrapoko ; // PosArtNr // PosPlz_von
			}
			else
			{
				EditCol = PosStaat ;	// Poslacktrapoko ;	// PosA_kun // PosPlz_von
			}
		}
	}
	else
	{
	    StopEnter ();

/* ------>
		if ( EditCol == Poslacktrapoko )
		{
			CString lacktrapoko = GetItemText (EditRow, Poslacktrapoko);
			if ( StrToDouble( lacktrapoko ) < 1 )
				EditCol -- ;
		}
		else
< ----- */
/* --->
		if( EditCol == PosPlz_v )
		{
			CString PLZV = GetItemText (EditRow, PosPlz_v);

			if ( StrToDouble( PLZV ) < 1 )
				EditCol -- ;
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			if ( StrToDouble( PLZB ) < 1  )
			{ // default laden 
				FillList.SetItemText (PLZV.GetBuffer (0), EditRow, PosPlz_b);
			}


		}
		else if ( EditCol == PosPlz_b )
		{
			CString PLZV = GetItemText (EditRow, PosPlz_v);
			CString PLZB = GetItemText (EditRow, PosPlz_b);
			if ( StrToDouble( PLZB ) < 1  )
				EditCol -- ;
			else if ( StrToDouble( PLZB ) < StrToDouble ( PLZV )  )
			{
				EditCol -- ;
				EditCol -- ;
			}
		}
< ---- */

		EditCol ++;
/* ---->
		if (EditCol == PosArtBez)
		{
			EditCol ++;
		}
		if (EditCol == PosKante)
		{
			EditCol ++;
		}
< ----- */
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CList1Ctrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}

/* ---->
	if (EditCol == PosArtNr)
	{
		ReadArtikel ();
	}
< ----- */
/* --- >
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< ---- */

	StopEnter ();
	EditCol ++;
/* --->
	if (EditCol == PosKunName)
	{
		EditCol ++;
	}
< --- */
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CList1Ctrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
/* ---->
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< -- */

	StopEnter ();
	EditCol --;
/* ---->
	if (EditCol == PosKunName)
	{
		EditCol --;
	}
< ---- */
    StartEnter (EditCol, EditRow);
}

BOOL CList1Ctrl::InsertRow ()
{
//	return FALSE ;
	CString cLW = "1" ; // dummy GetItemText (EditRow, Poslacktrapoko);
/* --->
	CString cPV = GetItemText (EditRow, PosPlz_v);
	CString cPB = GetItemText (EditRow, PosPlz_b);
	if (   (StrToDouble (cLW ) > 0 )
		&& (StrToDouble (cPV ) < 1 )
		&& (StrToDouble (cPB ) < 1 ) )
	{
		return FALSE;
	}
< ---- */

	StopEnter ();
	// editrow - 1 = ???

	// CString Altlacktrapoko = GetItemText (EditRow, Poslacktrapoko);

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);

//	FillList.SetItemText (Altlacktrapoko.GetBuffer(), EditRow, Poslacktrapoko);
	FillList.SetItemText ("", EditRow, PosPlz_von);	// Blank editiert sich besser
//	FillList.SetItemText (" ", EditRow, PosPlz_b);	// Balnk editiert sich besser

	/* ----->
		FillList.SetItemText (" ", EditRow, PosArtNr);
		FillList.SetItemText (" ", EditRow, PosArtBez);

		CString pKANTE;
		pKANTE = _T("|") ;
		FillList.SetItemText (pKANTE.GetBuffer (), EditRow, PosKante);

		FillList.SetItemText (" " , EditRow, Poslacktrapoko );
		FillList.SetItemText (" " , EditRow, PosA_bz1);
		FillList.SetItemText (" " , EditRow, PosA_bz2);

		CString pTWERT ;
		TCHAR ptbezk [37] ; 
		pTWERT.Format (_T("2"));	// Default : Gewichtsartikel ?! 
		CChoiceMeEinh HoleBezk ;
		HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk );
		CString zUBASIS;
// das Problem : Formatierung numerischer Wert mit 2 Folgeblanks 
		zUBASIS.Format (_T("2    %s"), ptbezk);
		FillList.SetItemText (zUBASIS.GetBuffer (), EditRow, PosKun_me_einh);

		FillList.SetItemText (" ",EditRow, PosKun_inh);	// blank  editiert sich besser
		FillList.SetItemText (" " , EditRow, PosGeb_fakt);	// blank editiert sich besser
		FillList.SetItemText (" ", EditRow, PosEan);
		FillList.SetItemText (" ", EditRow, PosEan_VK);
		FillList.SetItemText (" ", EditRow, PosLi_a);
< ---- */

	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CList1Ctrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
//	return FALSE ;
	return CEditListCtrl::DeleteRow ();
}

BOOL CList1Ctrl::AppendEmpty ()
{
	int rowCount = GetItemCount ();

	if (rowCount > 0)
	{
/* --->
		CString clacktrapoko = GetItemText (EditRow + 1 , Poslacktrapoko);
		double hilfe = StrToDouble (clacktrapoko) ;
		if (hilfe > 0 )	// Bereits neue Zeile angelegt
		{
			return FALSE;
		}
< ------ */

		CString cPLZV = GetItemText (EditRow  , PosPlz_von);
		if ( strlen ( cPLZV.Trim().GetBuffer()) == 0 )
			return FALSE;
/* --->
		CString cPLZB = GetItemText (EditRow  , PosPlz_b);
		double vhilfe = StrToDouble (cPLZV) ;
		double bhilfe = StrToDouble (cPLZB) ;
		if (vhilfe < 1 && bhilfe < 1 )	// Die Zeile ist leer bis auf den lacktrapoko
		{
			return FALSE;
		}
< --- */
	}

//	CString Altlacktrapoko = GetItemText (EditRow, Poslacktrapoko);

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);

//	FillList.SetItemText (Altlacktrapoko.GetBuffer(), rowCount, Poslacktrapoko);
	FillList.SetItemText ("", rowCount, PosPlz_von);	// Blank editiert sich besser
//	FillList.SetItemText (" ", rowCount, PosPlz_b);	// Balnk editiert sich besser

/* --->
	CString pOSI ;
	pOSI.Format (_T("%d"), rowCount + 1 );
	FillList.SetItemText (_T(pOSI.GetBuffer()), rowCount, PosPosi);

	FillList.SetItemText (_T(""), rowCount, PosKost_bez);
	FillList.SetItemText (_T("0,00"), rowCount, PosZuschlag);
	FillList.SetItemText (_T(""), rowCount, PosZubasis);
	FillList.SetItemText (_T("0,00"), rowCount, PosWert);
	FillList.SetItemText (_T("0,00"), rowCount, PosWertkg);
< ---- */

	/* ----->
		FillList.SetItemText (" ", rowCount, PosArtNr);
		FillList.SetItemText (" ", rowCount, PosArtBez);

		CString pKANTE;
		pKANTE = _T("|") ;
		FillList.SetItemText (pKANTE.GetBuffer (), rowCount, PosKante);

		FillList.SetItemText (" " , rowCount, PosA_kun);
		FillList.SetItemText (" " , rowCount, PosA_bz1);
		FillList.SetItemText (" " , rowCount, PosA_bz2);

		CString pTWERT ;
		TCHAR ptbezk [37] ; 
		pTWERT.Format (_T("2"));	// Default : Gewichtsartikel ?! 
		CChoiceMeEinh HoleBezk ;
		HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk );
		CString zUBASIS;
// das Problem : Formatierung numerischer Wert mit 2 Folgeblanks 
		zUBASIS.Format (_T("2    %s"), ptbezk);
		FillList.SetItemText (zUBASIS.GetBuffer (), rowCount, PosKun_me_einh);

		FillList.SetItemText (" ",rowCount, PosKun_inh); // blank editiert sdich besser
		FillList.SetItemText (" " , rowCount, PosGeb_fakt);	// blank editiert sich besser 
		FillList.SetItemText (" ", rowCount, PosEan);
		FillList.SetItemText (" ", rowCount, PosEan_VK);
		FillList.SetItemText (" ", rowCount, PosLi_a);
	< ---- */	
		
	rowCount = GetItemCount ();
	return TRUE;
}

void CList1Ctrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/

}

/* --->
void CKstArtListCtrl::FillPrGrStufCombo (CVector& Values)
{
	CString *c;
    PrGrStufCombo.FirstPosition ();
	while ((c = (CString *) PrGrStufCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	PrGrStufCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		PrGrStufCombo.Add (c);
	}
}
< ---- */


void CList1Ctrl::FillStaatCombo (CVector& Values)
{
	CString *c;
    StaatCombo.FirstPosition ();
	while ((c = (CString *) StaatCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	StaatCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		StaatCombo.Add (c);
	}
}

void CList1Ctrl::FillKundeCombo (CVector& Values)
{
	CString *c;
    KundeCombo.FirstPosition ();
	while ((c = (CString *) KundeCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	KundeCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		KundeCombo.Add (c);
	}
}


/* ----->
void CKstArtListCtrl::RunItemClicked (int Item)
{
**
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
**
}
< ----- */


void CList1Ctrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CList1Ctrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CList1Ctrl::OnChoice ()
{
	/* -> 
	if (EditCol == PosKun)
	{
		OnKunChoice (CString (_T("")));
	}
	else if (EditCol == PosKunPr)
	{
		OnIKunPrChoice (CString (_T("")));
	}
	else if (EditCol == PosPrGrStuf)
	{
		OnPrGrStufChoice (CString (_T("")));
	}
	< ----- */
}


/* --->
void CList1Ctrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}
< ---- */


// Classe::ReadKunNmae () 
// bool : FALSE : lesen ging schief  

/* brauchet mer z.Z nicht 
BOOL CList1Ctrl::ReadArtikel ()
{

	if (EditCol != PosArtNr) return TRUE ;
    memcpy (&a_bas, &a_bas_null, sizeof (A_BAS));
//	memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
** ---->
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		return;
	}
< ---- **

    CString Text;
	ListEdit.GetWindowText (Text);
** ---->
	if (!CStrFuncs::IsDecimal (Text))
	{
		OnKunChoice (Text);
	    SearchListCtrl.Edit.GetWindowText (Text);
		Text.Format (_T("%ld"), atol (Text.GetBuffer ()));
	    SearchListCtrl.Edit.SetWindowText (Text);
		if (!KunChoiceStat)
		{
			EditCol --;
			return;
		}
	}
< ---- **
	if (atol (Text) == 0l)
	{
		MessageBox (_T("eine Eingabe ist erforderlich"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return FALSE ;
	}
    a_bas.a = atol (Text ) ;
	int i = a_bas_class.opena_bas () ;
	i = a_bas_class.lesea_bas () ;
	a_kun.a = atol (Text);
	if (!i)
    {
//		  KunAdr.adr.adr = Kun.kun.adr1;
//		  KunAdr.dbreadfirst ();
		CString hilfart ;
		double dart ;
		for (int rowco = GetItemCount()  ; rowco > 0 ; )
		{
			rowco -- ;
			hilfart =  FillList.GetItemText (rowco, PosArtNr);
			dart = CStrFuncs::StrToDouble (hilfart) ;  
			if ( ( dart == a_kun.a ) && ( rowco != EditRow ) && dart > 0 )
			{
				MessageBox (_T("Artikel bereits in der Liste"), NULL, MB_OK | MB_ICONERROR); 
				EditCol --;
				return FALSE ;
			}
		}

	}
	else // if (a_bas.a != 0l)
	{
		MessageBox (_T("Artikel nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return FALSE ;
	}
    CString cBez;
	cBez.Format (_T("%s %s"), a_bas.a_bz1, a_bas.a_bz2 );
	FillList.SetItemText (cBez.GetBuffer (), EditRow, PosArtBez);
// 020211 : bei Neuanlage zunächst default vorschlagen 
	cBez.Format (_T("%s"), a_bas.a_bz1 );
	FillList.SetItemText (cBez.GetBuffer (), EditRow, PosA_bz1);
	return TRUE ;

}
< ----- */
void CList1Ctrl::GetColValue (int row, int col, CString& Text)
{
	/* ---->
    CString cText = GetItemText (row, col);
	int pos = 0;
	if (col == PosPrGrStuf)
	{
		Text = cText.Tokenize (_T(" "), pos);
	}
	else if (col == PosKunPr)
	{
		Text = cText.Tokenize (_T(" "), pos);
	}
	else
	{
		Text = cText.Trim ();
	}
	< ---- */
}

/* ---->
void CList1Ctrl::TestIprIndex ()
{
** ---->
	int Items = GetItemCount ();
	if (Items <= 1) return;
	CString Value;
	GetColValue (EditRow, PosPrGrStuf, Value);
	long rPrGrStuf = atol (_T(Value));
	GetColValue (EditRow, PosKunPr, _T(Value));
	long rKunPr = atol (_T(Value));
	GetColValue (EditRow, PosKun, _T(Value));
	long rKun = atol (_T(Value));
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosPrGrStuf, Value);
		long lPrGrStuf = atol (_T(Value));
		GetColValue (i, PosKunPr, Value);
		long lKunPr = atol (_T(Value));
		GetColValue (i, PosKun, Value);
		long lKun = atol (Value);
		if (lKun == rKun && rKun != 0)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
		else if (lKunPr == rKunPr && rKunPr != 0)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
		else if (lPrGrStuf == rPrGrStuf && lKunPr == rKunPr
				&& lKun == rKun)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
	}
	< --- **
}
< ---- */

void CList1Ctrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}


long CList1Ctrl::lapptest ( CString LW , CString PLZV  )
{

// alles neu und anders am 230713

// Jede Kombination lpz/Land darf es nur einmal geben

	CString cLW;
	CString cPLZV;
	for (int i = GetItemCount()  ; i > 0 ; )
	{
		i -- ;
		cLW =  FillList.GetItemText (i, PosStaat);

		cPLZV =  FillList.GetItemText (i, PosPlz_von);

		if ( EditRow == i )	// Eigentest ist wohl Quatsch
			continue ;

		if ( strcmp ( cPLZV,PLZV ) )
		{
			continue ;
		}
		if ( strcmp ( cLW,LW ) )
		{
			continue ;
		}
		return 1 ;	// Bitte meckern : gleicher Bereich mit 2 lacktrapokoen!!!!
	}	
	return 0 ;
}
