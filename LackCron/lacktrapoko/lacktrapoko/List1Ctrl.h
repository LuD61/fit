#pragma once
#include <vector>
#include "editlistctrl.h"
#include "dbClass.h"
#include "lacktrapokotab.h"
#include "ChoiceMeEinh.h"

#define MAXLISTROWS 30

class CList1Ctrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum LISTPOS
	{
//		POSleitweg	= 1,
		POSStaat    = 1,	// 060513 : Rest um eine Stelle verschieben
		POSPLZ_von	= 2,
// 230713 : Die Posten Kunde und FixBetrag( wert 10 ) eingebaut
		POSKun	    = 3,

		POSbis1		= 4,
		POSwert1	= 5,

		POSbis2		= 6,
		POSwert2	= 7,

		POSbis3		= 8,
		POSwert3	= 9,
		POSbis4		= 10,
		POSwert4	= 11,
		POSbis5		= 12,
		POSwert5	= 13,
		POSbis6		= 14,
		POSwert6	= 15,
		POSbis7		= 16,
		POSwert7	= 17,
		POSwert10   = 18,
	};

//	int Poslacktrapoko;
 
	int PosStaat;	// 060513
	int PosPlz_von;
	int PosKun;		// 230713
	int Posbis1;
	int	Poswert1;

	int	Posbis2;
	int	Poswert2;

	int	Posbis3;
	int	Poswert3;

	int	Posbis4;
	int	Poswert4;

	int	Posbis5;
	int	Poswert5;

	int	Posbis6;
	int	Poswert6;

	int	Posbis7;
	int	Poswert7;

	int Poswert10;	// 230713
	int *Position[20];


// void SetWerte ( long , double, long ) ;

// void Zuschlagsetzen ( void ) ;

//	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	BOOL ActiveListRow;
	short m_mdn;
	short mdn;

	std::vector<BOOL> vSelect;
//X	CVector MeEinhCombo ;
CVector StaatCombo ;
CVector KundeCombo;
CChoiceStaat *ChoiceStaat;
BOOL ModalChoiceStaat;
BOOL StaatChoiceStat;


//X	CChoiceMeEinh *ChoiceMeEinh;
//X	BOOL ModalChoiceMeEinh;
//X	BOOL MeEinhChoiceStat;

	CVector ListRows;

	CList1Ctrl(void);
	~CList1Ctrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
//	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
//X	void FillMeEinhCombo( CVector&);
	void FillStaatCombo( CVector&);
	void FillKundeCombo( CVector&);	// 260713

	void OnChoice ();

	//	void OnKey9 ();
    BOOL ReadArtikel ();

	void GetColValue (int row, int col, CString& Text);
	void ScrollPositions (int pos);
	BOOL LastCol ();
	long lapptest ( CString LW , CString PLZV ) ;
};
