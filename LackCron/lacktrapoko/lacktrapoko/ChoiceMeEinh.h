#ifndef _CHOICEMEEINH_DEF
#define _CHOICEMEEINH_DEF

#include "ChoiceX.h"
#include "MeEinhList.h"
#include "Vector.h"
#include <vector>
// 060513 : an div. Stellen meeinh durhc staat ersetzt
class CChoiceStaat : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
		long m_mdn;
		TCHAR ptwert[5] ;
		TCHAR ptwer2[8] ;
		TCHAR ptbezk[36] ;
		CVector *Rows;
	    std::vector<CStaatList *> StaatList;
      	CChoiceStaat(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceStaat(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchStaat (CListCtrl *,  LPTSTR);
        void SearchPtWer2 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CStaatList *GetSelectedText ();
        int GetPtBezk (LPTSTR, LPTSTR);
        int GetPtWer2 (LPTSTR, LPTSTR);
		void DestroyList ();
};

class CChoiceKunde : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
		long m_mdn;
		TCHAR ptwert[5] ;
		TCHAR ptwer2[8] ;
		TCHAR ptbezk[36] ;
		CVector *Rows;
	    std::vector<CKundeList *> KundeList;
      	CChoiceKunde(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceKunde(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchKunde (CListCtrl *,  LPTSTR);
        void SearchPtWer2 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CKundeList *GetSelectedText ();
        int GetPtBezk (LPTSTR, LPTSTR);
        int GetPtWer2 (LPTSTR, LPTSTR);
		void DestroyList ();
};


#endif
