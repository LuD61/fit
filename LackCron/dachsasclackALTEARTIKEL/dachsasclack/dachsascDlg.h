// dachsascDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"

extern int globaktion ;
extern char globmdn[199] ;
extern char globdatum[199] ;
extern char globtour[199] ;

// CdachsascDlg-Dialogfeld
class CdachsascDlg : public CDialog
{
// Konstruktion
public:
	CdachsascDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_DACHSASC_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_mdnnr;
	CString v_mdnnr;
	CEdit m_mdnname;
	CString v_mdnname;
	CEdit m_tour;
	CString v_tour;
	CEdit m_vdat;
	CString v_vdat;
	CEdit m_bdat;
	CString v_bdat;
	afx_msg void OnEnKillfocusMdnnr();
	afx_msg void OnEnKillfocusTour();
	afx_msg void OnEnKillfocusVdat();
	afx_msg void OnEnKillfocusBdat();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
 
	void ReadMdn (void) ; 
	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);

	int CdachsascDlg::Dateitesten ( int modus , char * dateiname ) ;
	int CdachsascDlg::opendatei ( void ) ;
	int CdachsascDlg::schreibevorsatz ( void ) ;
	int CdachsascDlg::schreibekopfsatz ( long ) ;
	int CdachsascDlg::schreibepspm (  void ) ;
	int CdachsascDlg::schreibenve ( void ) ;
	int CdachsascDlg::schreibekutx ( int ) ;	// 270813
	int CdachsascDlg::schreibekpap ( void ) ;
	int CdachsascDlg::schreibekref ( void ) ;	// 270313
	int CdachsascDlg::schreibeschlusssatz (void) ;
	int CdachsascDlg::Dateierstellung (void) ;
	int CdachsascDlg::holedefaults (void) ;

	CEdit m_melde;
	CString v_melde;
};
