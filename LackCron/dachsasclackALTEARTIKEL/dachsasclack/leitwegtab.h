#ifndef _LEITWEG_DEF
#define _LEITWEG_DEF


struct LEITWEG {
long leitweg ;
long leitw_typ ;
long plz_v ;
long plz_b;
char plz_von[9] ;
char plz_bis[9] ;
};

extern struct LEITWEG leitweg, leitweg_null;

struct B_LEIT {
char inleitw [3099] ;
long vonwert ;
long biswert ;
};

extern struct B_LEIT b_leit ;


class LEITWEG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
			   void preparedel(void);
			   long such_plz ;
       public :
               int leseallleitweg (void);
               int openallleitweg (void);
			   int deleteleitweg (void) ;
			   int insertleitweg (void);
			   void schliessen (void);
			   long holeplz (long leitw_typ,long plz);
			   BOOL basissort ;
               LEITWEG_CLASS () : DB_CLASS ()
               {
				   basissort = FALSE ;
               }
};

extern LEITWEG_CLASS leitweg_class ;


#endif

