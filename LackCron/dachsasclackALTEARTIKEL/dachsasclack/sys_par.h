#ifndef _SYS_PAR_DEF
#define _SYS_PAR_DEF

extern int spdachsnosmt2;	// 050914
extern int spdachsneu;	// 091114


struct SYS_PAR {
   char      sys_par_nam[19];
   char      sys_par_wrt[2];
   char      sys_par_besch[33];
   long      zei;
   short     delstatus;
};
extern struct SYS_PAR sys_par, sys_par_null;

class SYS_PAR_CLASS {
              private :
                     int cursor;
              public :
              SYS_PAR_CLASS () : cursor (-1)
              {
              }
              void prepare (void);
              int readfirst (void);
              int read (void);
			  char * sys_par_holen ( char *) ;
};
extern SYS_PAR_CLASS sys_par_class;	// 050914
#endif

