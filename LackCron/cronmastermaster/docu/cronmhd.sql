create table cronmhd
(
mdn smallint    ,
ls  integer ,
posi integer ,
a decimal(13,0) ,
mmhd01 decimal ( 9,3 ),
dmhd01 date ,
mmhd02 decimal ( 9,3 ),
dmhd02 date ,
mmhd03 decimal ( 9,3 ),
dmhd03 date ,
mmhd04 decimal ( 9,3 ),
dmhd04 date ,
mmhd05 decimal ( 9,3 ),
dmhd05 date ,
mmhd06 decimal ( 9,3 ),
dmhd06 date ,
mmhd07 decimal ( 9,3 ),
dmhd07 date ,
mmhd08 decimal ( 9,3 ),
dmhd08 date ,
mmhd09 decimal ( 9,3 ),
dmhd09 date ,
mmhd10 decimal ( 9,3 ),
dmhd10 date 
) in fit_dat extent size 32 next size 256 lock mode row ;

create index i01cronmhd on cronmhd ( ls, mdn, posi ) ;