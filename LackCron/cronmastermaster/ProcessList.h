#pragma once
#include <windows.h>
#include <Tlhelp32.h>
#include <vector>

class CProcessList
{
public:
    enum
	{
		Exe = 1,
		Path = 2,
	};

    PROCESSENTRY32 pe32; 
    MODULEENTRY32 me32; 
	std::vector<MODULEENTRY32 *> Entries;

	CProcessList(void);
	~CProcessList(void);
    BOOL PrintProcessModule (DWORD dwPID, LPMODULEENTRY32 lpMe32); 
    BOOL PrintList ();
    BOOL FillProcessModule (DWORD dwPID, LPMODULEENTRY32 lpMe32); 
    BOOL Fill ();
    BOOL GetProcessModule (DWORD dwPID, LPMODULEENTRY32 lpMe32, LPTSTR Name, int Mode); 
	BOOL GetExe (LPTSTR ExeName, DWORD Id = 0);
	BOOL GetPath (LPTSTR Path, DWORD Id = 0);
};
