#include <atlstr.h>
#include "processlist.h"


CProcessList::CProcessList(void)
{
	Entries.clear ();
}

CProcessList::~CProcessList(void)
{
	for (std::vector<MODULEENTRY32 *>::iterator pabl = Entries.begin (); pabl != Entries.end (); ++pabl)
	{
		MODULEENTRY32 *abl = *pabl;
		delete abl;
	}
    Entries.clear ();
}


BOOL CProcessList::PrintProcessModule (DWORD dwPID, LPMODULEENTRY32 lpMe32) 
{ 
    BOOL bRet = FALSE; 
    BOOL bFound = FALSE; 
    HANDLE hModuleSnap = NULL; 
    MODULEENTRY32 me32 = {0}; 


    hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwPID); 
    if (hModuleSnap == (HANDLE)-1) 
	{ 
		DWORD dwErr = GetLastError(); 
		return (FALSE); 
	}	 

    me32.dwSize = sizeof(MODULEENTRY32); 

    if (Module32First(hModuleSnap, &me32)) 
    { 
        do 
        { 
			printf ("%d %s\n%s\n\n", me32.th32ProcessID, me32.szModule, me32.szExePath); 
        } 
        while (!bFound && Module32Next(hModuleSnap, &me32)); 
        bRet = bFound;
    } 
    else 
	{
        bRet = FALSE; 
	}

    CloseHandle (hModuleSnap); 

    return (bRet); 
} 

BOOL CProcessList::PrintList ()
{ 
    HANDLE hProcessSnap = NULL; 
    BOOL bRet = FALSE; 
    memset (&pe32, 0, sizeof (pe32)); 

    hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0); 

    if (hProcessSnap == (HANDLE)-1) 
        return (FALSE); 

    pe32.dwSize = sizeof(PROCESSENTRY32); 


    if (Process32First(hProcessSnap, &pe32)) 
    { 
        BOOL bGotModule = FALSE; 
	    memset (&me32, 0, sizeof (me32)); 

        do 
        { 
			printf ("%d %s\n", pe32.th32ProcessID, pe32.szExeFile); 

            PrintProcessModule(pe32.th32ProcessID, &me32);
        } 
        while (Process32Next(hProcessSnap, &pe32)); 
        bRet = TRUE; 
    } 
    else 
	{
        bRet = FALSE;
	}

    CloseHandle (hProcessSnap); 
    return (bRet); 
} 


BOOL CProcessList::FillProcessModule (DWORD dwPID, LPMODULEENTRY32 lpMe32)
{
    BOOL bRet = FALSE; 
    BOOL bFound = FALSE; 
    HANDLE hModuleSnap = NULL; 
    MODULEENTRY32 me32 = {0}; 


    hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwPID); 
    if (hModuleSnap == (HANDLE)-1) 
	{ 
		DWORD dwErr = GetLastError(); 
		return (FALSE); 
	}	 

    me32.dwSize = sizeof(MODULEENTRY32); 

    if (Module32First(hModuleSnap, &me32)) 
    { 
        do 
        { 
			MODULEENTRY32 *Entry = new MODULEENTRY32;
            CopyMemory (Entry, &me32, sizeof(MODULEENTRY32)); 
			Entries.push_back (Entry);
        } 
        while (!bFound && Module32Next(hModuleSnap, &me32)); 
        bRet = bFound;
    } 
    else 
	{
        bRet = FALSE; 
	}

    CloseHandle (hModuleSnap); 

    return (bRet); 
}	


BOOL CProcessList::Fill ()
{ 
    HANDLE hProcessSnap = NULL; 
    BOOL bRet = FALSE; 
    memset (&pe32, 0, sizeof (pe32)); 

	Entries.clear ();
    hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0); 

    if (hProcessSnap == (HANDLE)-1) 
        return (FALSE); 

    pe32.dwSize = sizeof(PROCESSENTRY32); 


    if (Process32First(hProcessSnap, &pe32)) 
    { 
        BOOL bGotModule = FALSE; 
	    memset (&me32, 0, sizeof (me32)); 

        do 
        { 
            FillProcessModule(pe32.th32ProcessID, &me32);
        } 
        while (Process32Next(hProcessSnap, &pe32)); 
        bRet = TRUE; 
    } 
    else 
	{
        bRet = FALSE;
	}

    CloseHandle (hProcessSnap); 
    return (bRet); 
} 

BOOL CProcessList::GetProcessModule (DWORD dwPID, LPMODULEENTRY32 lpMe32, LPTSTR Name, int Mode) 
{ 
    BOOL bRet = FALSE; 
    BOOL bFound = FALSE; 
    HANDLE hModuleSnap = NULL; 
    MODULEENTRY32 me32 = {0}; 

    hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwPID); 
    if (hModuleSnap == (HANDLE)-1) 
	{ 
		DWORD dwErr = GetLastError(); 
		return (FALSE); 
	}	 

    me32.dwSize = sizeof(MODULEENTRY32); 

    if (Module32First(hModuleSnap, &me32)) 
    { 
        do 
        { 
			if (Mode == Exe)
			{
//				if (_tcscmp (me32.szModule, Name) == 0) 

				CString ModulName = me32.szModule;
				CString ExeName = Name;
				ModulName.Trim ().MakeUpper ();
				ExeName.Trim ().MakeUpper ();
				if (ExeName == ModulName)
				{ 
	                CopyMemory (lpMe32, &me32, sizeof(MODULEENTRY32)); 
		            return TRUE; 
				}
            } 
			else if (Mode == Path)
			{
//				if (_tcscmp (me32.szExePath, Name) == 0) 
				CString ModulName = me32.szExePath;
				CString ExeName = Name;
				ModulName.Trim ().MakeUpper ();
				ExeName.Trim ().MakeUpper ();
				if (ExeName == ModulName)
				{ 
	                CopyMemory (lpMe32, &me32, sizeof(MODULEENTRY32)); 
		            return TRUE; 
				}
            } 
        } 
        while (!bFound && Module32Next(hModuleSnap, &me32)); 
        bRet = bFound;
    } 
    else 
	{
        bRet = FALSE; 
	}

    CloseHandle (hModuleSnap); 

    return (bRet); 
} 

BOOL CProcessList::GetExe (LPTSTR ExeName, DWORD Id)
{
    HANDLE hProcessSnap = NULL; 
    BOOL bRet = FALSE; 
    memset (&pe32, 0, sizeof (pe32)); 

    hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0); 

    if (hProcessSnap == (HANDLE)-1) 
        return (FALSE); 

    pe32.dwSize = sizeof(PROCESSENTRY32); 


    if (Process32First(hProcessSnap, &pe32)) 
    { 
        BOOL bGotModule = FALSE; 
	    memset (&me32, 0, sizeof (me32)); 


        do 
        { 
			if (pe32.th32ProcessID == 0) continue;
			if (Id == pe32.th32ProcessID) continue;
            if (GetProcessModule(pe32.th32ProcessID, &me32, ExeName, Exe))
			{
				bRet = TRUE; 
				break;
			}
        } 
        while (Process32Next(hProcessSnap, &pe32)); 
    } 
    else 
	{
        bRet = FALSE;
	}

    CloseHandle (hProcessSnap); 
    return (bRet); 
} 

BOOL CProcessList::GetPath (LPTSTR Path, DWORD Id)
{
    HANDLE hProcessSnap = NULL; 
    BOOL bRet = FALSE; 
    memset (&pe32, 0, sizeof (pe32)); 

    hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0); 

    if (hProcessSnap == (HANDLE)-1) 
        return (FALSE); 

    pe32.dwSize = sizeof(PROCESSENTRY32); 


    if (Process32First(hProcessSnap, &pe32)) 
    { 
        BOOL bGotModule = FALSE; 
	    memset (&me32, 0, sizeof (me32)); 

        do 
        { 
			if (Id == pe32.th32ProcessID) continue;
            if (GetProcessModule(pe32.th32ProcessID, &me32, Path, this->Path))
			{
				bRet = TRUE; 
				break;
			}
        } 
        while (Process32Next(hProcessSnap, &pe32)); 
    } 
    else 
	{
        bRet = FALSE;
	}

    CloseHandle (hProcessSnap); 
    return (bRet); 
}

