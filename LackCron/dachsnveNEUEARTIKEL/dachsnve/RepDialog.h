#pragma once
#include "afxwin.h"


// RepDialog-Dialogfeld

class RepDialog : public CPropertyPage
{
	DECLARE_DYNAMIC(RepDialog)

public:
	RepDialog();
	virtual ~RepDialog();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_rech_nr;
	long v_rech_nr;
	afx_msg void OnEnKillfocusRechNr();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
