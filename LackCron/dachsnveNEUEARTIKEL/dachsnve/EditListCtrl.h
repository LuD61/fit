#pragma once
#include "afxcmn.h"
#include <atlimage.h>
#include "FillList.h"
#include "ListEdit.h"
// #include "ListComboBox.h"
#include "ListCheckBox.h"
// #include "SearchListCtrl.h"
#include "ListDropEx.h"
#include "Vector.h"
#include "ListChangeHandler.h"
#include <vector>

class CColType
{
public:
	int Pos;
	int Type;

	CColType::CColType (int Pos, int Type);
};

class CEditListCtrl :
	public CListCtrl
{

public:
	CEditListCtrl(void);
	~CEditListCtrl(void);
protected :
	DECLARE_MESSAGE_MAP()
	virtual void DrawItem (LPDRAWITEMSTRUCT);
public :
	CListDropEx dropTarget;
    enum
	{
		EDIT = 1990,
		COMBO = 1991,
        SEARCHEDIT = 1992, 
		SEARCHBUTTON = 1993,
		CHECKBOX = 1994,
	};

	std::vector<CListChangeHandler *> LsChHandlers;
	int Edit;
	int Combo;
	int SearchEdit;
	int SearchButton;
	int CheckBox;
	CImage check;
	CImage uncheck;
	UINT LimitText;
	BOOL VLines;
	BOOL HLines;
	BOOL GridLines;
	CFillList FillList;
	BOOL SetEditFocus;
	int EditRow;
	int EditCol;
	CListEdit ListEdit;
//	CListComboBox ListComboBox;
	CListCheckBox ListCheckBox;
//	CSearchListCtrl SearchListCtrl;
	BOOL MustStart;
	int EditNumber;
	BOOL FirstScroll;
	CVector ColType;

	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
//	virtual void StartEnterCombo (int, int, CVector *);
	virtual void StartEnterCheckBox (int, int);
//	virtual void StartSearchListCtrl (int, int);
	virtual void StopEnter ();
	virtual void GetEnter ();
	virtual void SetEditText ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual BOOL OnKeyD (WPARAM);
	virtual BOOL OnLBuDown (CPoint&);
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
//	virtual void SetSearchSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	virtual void ClearSelect ();
    void EnsureColVisible (int);

    static int jrhstart;
    static int jrh1;
    static int jrh2;
    static int sjr;
    static double StrToDouble (LPTSTR);
    static double StrToDouble (CString&);
    static void CEditListCtrl::DoubleToString (double dValue, CString& Value, int scale, CString Point = _T(","));
    static void DatFormat (CString &, LPTSTR);
    static BOOL IsMon31 (int);
    static BOOL IsMon30 (int);
    static BOOL IsMon29 (int, int);
    static BOOL IsMon28 (int, int);
	void StartPauseEnter ();
	void EndPauseEnter ();

	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar); 
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar); 
    afx_msg void OnKeyDown(UINT nTCHAR, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSetFocus (CWnd *);
	afx_msg void OnKillFocus (CWnd *);
	afx_msg void OnEnKillFocusEdit();
	afx_msg void OnLButtonDown (UINT, CPoint);
	afx_msg void OnMouseMove (UINT, CPoint);
    afx_msg void OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnPaint( );
    int GetColType (int Pos);
    void GrPaint (CDC *cDC);
	void AddListChangeHandler (CListChangeHandler *Handler);
	void PerformListChangeHandler ();
};
