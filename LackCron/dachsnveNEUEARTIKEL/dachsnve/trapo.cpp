#include "stdafx.h"
#include "DbClass.h"
#include "trapo.h"
#include "adr.h" 
#include "ls.h"

struct TRAPOPSPM trapopspm,  trapopspm_null;
struct TRAPONVE traponve,  traponve_null;
struct TRAPOKO trapoko,  trapoko_null;
struct LACKDNUM lackdnum,  lackdnum_null;


extern DB_CLASS dbClass;

TRAPOPSPM_CLASS trapopspm_class;
TRAPONVE_CLASS traponve_class;
TRAPOKO_CLASS trapoko_class;
LACKDNUM_CLASS lackdnum_class;


int LACKDNUM_CLASS::openlackdnum (void)
{
	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (readcursor);
}

int LACKDNUM_CLASS::leselackdnum (void)
{
	if (readcursor < 0 )
	{
		prepare ();
		dbClass.sqlopen (readcursor);
	}
	int di = dbClass.sqlfetch (readcursor);
	return di;   
}
int LACKDNUM_CLASS::writelackdnum (void)
{
	if (readcursor < 0 )
	{
		prepare ();
	}
	int di = dbClass.sqlexecute (upd_cursor);
	return di;
}

void LACKDNUM_CLASS::prepare (void)
{

// fuer readcursor t  
	
	dbClass.sqlout ((char *) lackdnum.numstart ,SQLCHAR, 21) ;
	dbClass.sqlout ((char *) lackdnum.numakt ,  SQLCHAR, 21);
	dbClass.sqlout ((char *) lackdnum.numend ,  SQLCHAR, 21);

    readcursor = (short)dbClass.sqlcursor ("select "
		" numstart, numakt, numend "
		" from lackdnum "
	) ;

// fuer upd_cursor 
	
	dbClass.sqlin ((char  *) lackdnum.numakt ,SQLCHAR, 21) ;

	 
    upd_cursor = (short)dbClass.sqlcursor ("update lackdnum "
		" set numakt = ? "
	) ;

}


int TRAPOPSPM_CLASS::opentrapopspm (void)
{	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (readcursor);
}
int TRAPOPSPM_CLASS::openalltrapo (void)
{	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (test_upd_cursor);
}

int TRAPONVE_CLASS::opentraponve (void)
{	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (readcursor);
}

int TRAPOPSPM_CLASS::lesetrapopspm (void)
{	if (readcursor < 0 )
	{	prepare ();
		dbClass.sqlopen (readcursor);
	}
	int di = dbClass.sqlfetch (readcursor);
	return di;   
}
int TRAPOPSPM_CLASS::lesealltrapo (void)
{	if (readcursor < 0 )
	{	prepare ();
		dbClass.sqlopen (test_upd_cursor);
	}
	int di = dbClass.sqlfetch (test_upd_cursor);
	return di;   
}

int TRAPONVE_CLASS::lesetraponve (void)
{	if (readcursor < 0 )
    {	prepare ();
		dbClass.sqlopen (readcursor);
	}
	int di = dbClass.sqlfetch (readcursor);
	return di;   
}

int TRAPOPSPM_CLASS::inserttrapopspm (void)
{	if (readcursor < 0 )
		prepare ();
	int di = dbClass.sqlexecute (ins_cursor);
	return di;
}

int TRAPONVE_CLASS::inserttraponve (void)
{	if (readcursor < 0 )
			prepare ();
	int di = dbClass.sqlexecute (ins_cursor);
	return di;
}

int TRAPOPSPM_CLASS::updatetrapopspm (void)
{	if (readcursor < 0 )
		prepare ();
	int di = dbClass.sqlexecute (upd_cursor);
	return di;
}

int TRAPOPSPM_CLASS::del_pspm (void)
{	if (del_cursor < 0 )
		prepare ();
	int di = dbClass.sqlexecute (del_cursor);
	return di;
}



int TRAPONVE_CLASS::updatetraponve (void)
{	if (readcursor < 0 )
		prepare ();
	int di = dbClass.sqlexecute (upd_cursor);
	return di;
}

int TRAPONVE_CLASS::del_nve (void)
{	if (del_cursor < 0 )
		prepare ();
	int di = dbClass.sqlexecute (del_cursor);
	return di;
}

void TRAPOPSPM_CLASS::prepare (void)
{

// fuer ins_cursor 

	dbClass.sqlin ((short  *) &trapopspm.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((short  *) &trapopspm.fil ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &trapopspm.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  trapopspm.blg_typ ,SQLCHAR, 2) ;
	dbClass.sqlin ((long   *) &trapopspm.lfd ,SQLLONG, 0) ;

	dbClass.sqlin ((double *) &trapopspm.pm ,SQLDOUBLE, 0) ;
	dbClass.sqlin ((double *) &trapopspm.pmzahl ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps1 ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps1zahl ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps2 ,SQLDOUBLE, 0);

	dbClass.sqlin ((double *) &trapopspm.ps2zahl ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps3 ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps3zahl ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps4 ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps4zahl ,SQLDOUBLE, 0);

	dbClass.sqlin ((long   *) &trapopspm.rech ,SQLLONG, 0);

 
    ins_cursor = (short)dbClass.sqlcursor ("insert into trapopspm ( "
		" mdn ,fil ,ls ,blg_typ ,lfd "
		",pm ,pmzahl ,ps1 ,ps1zahl ,ps2 "
		",ps2zahl ,ps3 ,ps3zahl ,ps4 ,ps4zahl "
		",rech "
		" ) values ( "
		" ? ,? ,? ,? ,? "
		",? ,? ,? ,? ,? "
		",? ,? ,? ,? ,? "
		",?" 
		" ) " ) ;

// fuer readcursor - wird hier nicht wirklich benoetigt 

	dbClass.sqlin ((short  *) &trapopspm.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &trapopspm.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  trapopspm.blg_typ ,SQLCHAR, 2) ;

	dbClass.sqlout ((short  *) &trapopspm.fil ,SQLSHORT, 0) ;
	dbClass.sqlout ((long   *) &trapopspm.lfd ,SQLLONG, 0) ;

	dbClass.sqlout ((double *) &trapopspm.pm ,SQLDOUBLE, 0) ;
	dbClass.sqlout ((double *) &trapopspm.pmzahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps1 ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps1zahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps2 ,SQLDOUBLE, 0);

	dbClass.sqlout ((double *) &trapopspm.ps2zahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps3 ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps3zahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps4 ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps4zahl ,SQLDOUBLE, 0);

	dbClass.sqlout ((long   *) &trapopspm.rech ,SQLLONG, 0);

 
    readcursor = (short)dbClass.sqlcursor ("select "
		" fil ,lfd "
		",pm ,pmzahl ,ps1 ,ps1zahl ,ps2 "
		",ps2zahl ,ps3 ,ps3zahl ,ps4 ,ps4zahl "
		",rech "
		" from trapopspm "
		" where "
		" mdn = ? and ls = ? and blg_typ = ? "
		" order by lfd " ) ;


// fuer upd_cursor 

	dbClass.sqlin ((long   *) &trapopspm.rech ,SQLLONG, 0);

	dbClass.sqlin ((short  *) &trapopspm.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &trapopspm.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  trapopspm.blg_typ ,SQLCHAR, 2) ;

	 
    upd_cursor = (short)dbClass.sqlcursor ("update trapopspm "
		" set rech = ? "
		" where "
		" mdn = ? and ls = ? and blg_typ = ? " ) ;

// fuer test_upd_cursor - der einzig wesentliche cursor  

	dbClass.sqlin ((TIMESTAMP_STRUCT *) &lsk.lieferdat ,SQLTIMESTAMP, 26) ;
	dbClass.sqlin ((long   *) &lsk.tou_nr ,SQLLONG, 0);

	dbClass.sqlout ((long   *) &lsk.ls ,SQLLONG, 0);
	dbClass.sqlout ((long   *) &lsk.kun ,SQLLONG, 0);
	dbClass.sqlout ((long   *) &trapopspm.ls ,SQLLONG, 0);
	dbClass.sqlout ((char   *)  adr.adr_krz ,SQLCHAR, 17) ;
	dbClass.sqlout ((char   *)  traponve.nve ,SQLCHAR, 21) ;

	dbClass.sqlout ((long   *) &trapopspm.lfd ,SQLLONG, 0) ;

	dbClass.sqlout ((double *) &trapopspm.pm ,SQLDOUBLE, 0) ;
	dbClass.sqlout ((double *) &trapopspm.pmzahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps1 ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps1zahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps2 ,SQLDOUBLE, 0);

	dbClass.sqlout ((double *) &trapopspm.ps2zahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps3 ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps3zahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps4 ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps4zahl ,SQLDOUBLE, 0);

	dbClass.sqlout ((long   *) &trapopspm.rech ,SQLLONG, 0);

// Das ist der Global-Cursor : 
//	select * from lsk,trapopspm, traponve, adr 
//	where lsk.mdn = 1 
//	and lsk.lieferdat = ?
//	and lsk.tou_nr = ? 
//	and adr.adr = lsk.adr
//	and trapopspm.mdn = lsk.mdn
//	and trapopspm.ls = lsk.ls
//	and trapopspm.blg_typ = "L"
//  and traponve.mdn = trapopspm.mdn 
//  and traponve.ls  = trapopspm.ls
//  and traponve.blg_typ = trapopspm.blg_typ
//  and traponve.lfd     = trapopspm.lfd
// order by   trapopspm.rech, trapopspm.ls, trapopspm.lfd , traponve.nve 



    test_upd_cursor = (short)dbClass.sqlcursor ("select "

		" lsk.ls, lsk.kun, trapopspm.ls, adr.adr_krz , traponve.nve "
		" ,trapopspm.lfd "
		",trapopspm.pm ,trapopspm.pmzahl ,trapopspm.ps1 ,trapopspm.ps1zahl ,trapopspm.ps2 "
		",trapopspm.ps2zahl ,trapopspm.ps3 ,trapopspm.ps3zahl ,trapopspm.ps4 ,trapopspm.ps4zahl "
		",trapopspm.rech "
		" from lsk,adr, trapopspm, traponve "
		" where lsk.mdn = 1 "
		" and lsk.lieferdat = ? "
		" and lsk.tou_nr = ? " 
		" and adr.adr = lsk.adr "
		" and trapopspm.mdn = lsk.mdn "
		" and trapopspm.ls = lsk.ls "
		" and trapopspm.blg_typ = \"L\" "
		" and traponve.mdn = trapopspm.mdn " 
		" and traponve.ls  = trapopspm.ls "
		" and traponve.blg_typ = trapopspm.blg_typ "
		" and traponve.lfd     = trapopspm.lfd "
		" order by trapopspm.rech, trapopspm.ls, trapopspm.lfd , traponve.nve " 
		) ;


	dbClass.sqlin ((long   *) &trapopspm.rech ,SQLLONG, 0);
	del_cursor = (short)dbClass.sqlcursor ("delete from trapopspm  "
	   "where mdn = 1 and rech = ? " ) ;


	

}

void TRAPONVE_CLASS::prepare (void)
{

// fuer ins_cursor 

	dbClass.sqlin ((short  *) &traponve.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((short  *) &traponve.fil ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &traponve.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  traponve.blg_typ ,SQLCHAR, 2) ;
	dbClass.sqlin ((long   *) &traponve.lfd ,SQLLONG, 0) ;

	dbClass.sqlin ((char   *)  traponve.nve ,SQLCHAR, 21) ;
	dbClass.sqlin ((double *) &traponve.a ,SQLDOUBLE, 0) ;
	dbClass.sqlin ((short  *) &traponve.delstatus ,SQLSHORT, 0) ;

 
    ins_cursor = (short)dbClass.sqlcursor ("insert into traponve ( "
		" mdn ,fil ,ls ,blg_typ ,lfd "
		",nve ,a ,delstatus "
		" ) values ( "
		" ? ,? ,? ,? ,? "
		",? ,? ,? "
		" ) " ) ;

// fuer readcursor - wird hier nicht wirklich benoetigt 

	dbClass.sqlin ((short  *) &traponve.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &traponve.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  traponve.blg_typ ,SQLCHAR, 2) ;
	dbClass.sqlout ((long   *) &traponve.lfd ,SQLLONG, 0) ;

	dbClass.sqlout ((short  *) &traponve.fil ,SQLSHORT, 0) ;


	dbClass.sqlout ((char   *)  traponve.nve ,SQLCHAR, 21) ;
	dbClass.sqlout ((double *) &traponve.a ,SQLDOUBLE, 0) ;
	dbClass.sqlout ((short  *) &traponve.delstatus ,SQLSHORT, 0) ;
 
    readcursor = (short)dbClass.sqlcursor ("select "
		" fil "
		",nve ,a ,delstatus "
		" from traponve "
		" where "
		" mdn = ? and ls = ? and blg_typ = ? and lfd = ? " ) ;


// fuer upd_cursor wird hier nicht wirklich benoetigt

	dbClass.sqlin ((char   *)  traponve.nve ,SQLCHAR, 21);
	dbClass.sqlin ((double *) &traponve.a ,SQLDOUBLE, 0 );

	dbClass.sqlin ((short  *) &traponve.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &traponve.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  traponve.blg_typ ,SQLCHAR, 2) ;
	dbClass.sqlin ((long   *) &traponve.lfd ,SQLLONG, 0);
	 
    upd_cursor = (short)dbClass.sqlcursor ("update traponve "
		" set nve = ? , a = ? "
		" where "
		" mdn = ? and ls = ? and blg_typ = ?  and lfd = ? " ) ;

dbClass.sqlin ((long   *) &traponve.ls ,SQLLONG, 0);
    del_cursor = (short)dbClass.sqlcursor ("delete from traponve "
		" where mdn = 1 and ls = ?  and blg_typ ='L' " ) ;


}


int TRAPOKO_CLASS::lesealltrapoko (void)
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}
int TRAPOKO_CLASS::lesetrapoko (void)
{
      int di = dbClass.sqlfetch (test_upd_cursor);
	  return di;
}


int TRAPOKO_CLASS::openalltrapoko (void)
{
		if ( readcursor < 0 ) prepare ();	
         return dbClass.sqlopen (readcursor);
}

int TRAPOKO_CLASS::opentrapoko (void)
{
		if ( readcursor < 0 ) prepare ();	
		return dbClass.sqlopen (test_upd_cursor);
}


void TRAPOKO_CLASS::schliessen (void)
{
	if ( readcursor > -1 )
	{	// geht ca. 200 mal gut, dann knallt es wohl 
		dbClass.sqlclose(readcursor ) ;
		readcursor = -1 ;
	}
	if ( test_upd_cursor > -1 )
	{	// geht ca. 200 mal gut, dann knallt es wohl 
		dbClass.sqlclose(test_upd_cursor ) ;
		test_upd_cursor = -1 ;
	}
}


void TRAPOKO_CLASS::prepare (void)
{

// Lese alle Saetze eines trapoko-Typs

// evtl. spaeter mal optimierung der Suche ?!

		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long *) &trapoko.typ, SQLLONG, 0 ) ;

		dbClass.sqlout (( long *) &trapoko.plz_v, SQLLONG, 0 ) ;
		dbClass.sqlout (( long *) &trapoko.plz_b, SQLLONG, 0 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_von,  SQLCHAR, 9 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_bis,  SQLCHAR, 9 ) ;

		dbClass.sqlout (( double *) &trapoko.von1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert1,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert2,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert3,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert4,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert5,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert6,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert7,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert8,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert9,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert10,SQLDOUBLE, 0 ) ;

			readcursor = (short) dbClass.sqlcursor ("select "
			" plz_v, plz_b , plz_von , plz_bis "
			" , von1, bis1 , wert1 "
			" , von2, bis2 , wert2 "
			" , von3, bis3 , wert3 "
			" , von4, bis4 , wert4 "
			" , von5, bis5 , wert5 "
			" , von6, bis6 , wert6 "
			" , von7, bis7 , wert7 "
			" , von8, bis8 , wert8 "
			" , von9, bis9 , wert9 "
			" , von10,bis10, wert10 "
	
			" from trapoko where mdn = ? and leitw_typ = ?  and typ = ?" 
			" order by plz_v, plz_b desc " ) ;

// fuer konkreten Satz nach aktueller Spezi

		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long *) &trapoko.plz_v, SQLLONG, 0 ) ;

		dbClass.sqlout (( long *) &trapoko.typ, SQLLONG, 0 ) ;
		dbClass.sqlout (( long *) &trapoko.plz_b, SQLLONG, 0 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_von,  SQLCHAR, 9 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_bis,  SQLCHAR, 9 ) ;

		dbClass.sqlout (( double *) &trapoko.von1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert1,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert2,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert3,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert4,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert5,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert6,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert7,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert8,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert9,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert10,SQLDOUBLE, 0 ) ;

			readcursor = (short) dbClass.sqlcursor ("select "
			" typ, plz_b , plz_von , plz_bis "
			" , von1, bis1 , wert1 "
			" , von2, bis2 , wert2 "
			" , von3, bis3 , wert3 "
			" , von4, bis4 , wert4 "
			" , von5, bis5 , wert5 "
			" , von6, bis6 , wert6 "
			" , von7, bis7 , wert7 "
			" , von8, bis8 , wert8 "
			" , von9, bis9 , wert9 "
			" , von10,bis10, wert10 "
	
			" from trapoko where mdn = ? and leitw_typ = ?  and plz_v = ?" 
			" order by typ , plz_b desc " ) ;

}