#pragma once

class CListChangeHandler
{
public:
	CListChangeHandler(void);
	~CListChangeHandler(void);
	virtual void RowChanged (int NewRow) = 0;
};
