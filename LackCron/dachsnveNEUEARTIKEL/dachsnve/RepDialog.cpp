// RepDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "dachsnve.h"
#include "RepDialog.h"
#include "dbClass.h"
#include "ls.h"
#include "trapo.h"
#include "mdn.h"



extern LSK_CLASS lsk_class ;
extern LSP_CLASS lsp_class ;
extern TRAPOPSPM_CLASS trapopspm_class ;
extern TRAPONVE_CLASS traponve_class ;
extern LACKDNUM_CLASS lackdnum_class ;

char okstring2[99] ;
char okstring[99] ;

char * generierenve ( char * innve )
{

int point1 ;
int  wicht ;
int platz ;
char testchars[2] ; 
int summe ;

  sprintf ( okstring , "%s" , innve ) ;
 
  point1 = 0 ;
  summe  = 0 ;
  wicht  = 3 ;

  // Beachte : das funzt nur, wenn die ersten 2 Stellen der insgesamt 20 stelligen NVE konstant auf "00" stehen
  // oder zumindest in der Pruefsumme einen Summand von n * 10 ergeben  und daher neutral sind 
  // erlaubte Werte : "00" , "17" , "24" , "31" , "48" , "55" , "62" , "79" , "86" , "93" 
  // dier ersten 18 Stellen werden verknotet
  // falls der input-string kuerzer ist, wird er mit 000 aufgefuellt,
  // falls nicht-nummer-Zeichen drin sind, wird ebenfalls mit "0" aufgefuellt
  testchars[1] = '\0' ;
  while ( point1 < 17 )
  {
      testchars[0] = okstring[point1] ;
	  if ( testchars[0] < '0' || testchars[0] > '9' )
	  {
		  if ( testchars[0] == '\0' )
		  {
			  okstring[point1 + 1 ] = '\0' ;	// stringende fortplanzen
		  }
		  okstring[point1 ] = '0'		;	// Nicht-alpha durch "0" ersetzen bzw. string verl�ngern
		  testchars[0] = '0'			;
	  }
      platz    = atoi ( testchars ) ;
      summe += ( platz * wicht ) ;
      if ( wicht == 1 ) 
		wicht = 3 ;
      else
		wicht = 1 ;
      
      point1 ++ ; 
  }
  platz =  summe % 10 ;            /* Mod 10  */
  if ( platz == 0 )
 	  sprintf ( testchars, "0" ) ;
  else
	  sprintf ( testchars , "%d" , 10 - platz ) ; 

  sprintf ( okstring + 17,  "%s" , testchars ) ;

   return ( okstring ) ;

}


char * Generierenve ( long lfdwert )
{
// Struktur der NVE :  "3" + mdn.iln[0->6] + lfd[9stellig]
// die "3" ist "frei verf�gbar, aber in der Regel IMMER == "3" ....
// das die Mandanten-ILN korrekt und aktuell ist, sollte vorher klar sein , bei Leerstring gehts in die hose
	sprintf ( okstring2, "3%s", mdn.iln );
//	sprintf ( okstring2, "34312345" );	// Genau Lackmann-GLN
	long hilfls ;
	hilfls = lsk.ls ;

	while ( hilfls > 9999999 )
	{ 
		hilfls -=  10000000 ;
	}
		while ( hilfls > 999999 )
	{ 
		hilfls -=  1000000 ;
	}

	sprintf ( okstring2 + 8 , "%06ld" , hilfls ) ;
	sprintf ( okstring2 + 14 , "%03ld" , lfdwert ) ;

// A 100113 : alles anders wegen NVE aus Dachser-Nummernkreis :
// Struktur : numakt : hat naechste freie Nummer 
// 01234567 890123456
// 34032685 001280000	// Startwert
	int iii = lackdnum_class.leselackdnum() ;

	long hiwerta ;
	long hiwerte ;
	hiwerta = atol ( lackdnum.numakt + 8 );
	hiwerte = atol ( lackdnum.numend + 8 );
	if ( hiwerte > hiwerta )	// erst mal einfach Notbremse bei endwert
	{
		hiwerta++ ;
	}
// 151213 : weil der Index in traponve auch ls integriert, kann man einfach wieder von vorne anfangen ?!
	else
	{
		hiwerta = atol ( lackdnum.numstart + 8 );
		hiwerta++ ;
	}


	
	sprintf ( okstring2,"%s", lackdnum.numakt ) ;
	sprintf ( lackdnum.numakt + 8, "%09ld",hiwerta ) ;
	lackdnum_class.writelackdnum() ;

// E 100113 : alles anders wegen NVE aus Dachser-Nummernkreis :

	sprintf ( okstring2 , "%s" , generierenve(okstring2 )) ;
	return okstring2 ;
}


// der erste LSK-Satz wurde jedenfalls erfolgreich gefunden und ist aktuell
int  NVE_generieren ( int aktion )
{
#define MAXNVE 100


	int iret = 0 ;		// erster Satz ist vorgelesen
	int maxnve = MAXNVE ;
	char nvepuffer[MAXNVE + 1 ][22];	// 100 NVE zu max. 21 Zeichen
	double nvetyppuf[MAXNVE + 1 ];		// Artikel

	int nvepoint = 0 ;

// testen ob bed�rftige Position existiert und bei Notwendigkeit : Etikett ausl�sen und in trapopspm und traponve 
// im ls-Modus schreiben

	int di ;

//	tou.tou = cron_komm.tou_nr ;
//	tou.leitw_typ = -1 ;
//	di = Tou.opentou() ;
//	di = Tou.lesetou() ;	// Tour komplettieren
// ist bereits und immer aktuell 


/* ---> wird evtl. bei Druck ben�tigt, alle Entscheidungen hat der Mensch bereits vorher getroffen 
	if ( tou.leitw_typ != LEITDACHS )
		return ( aktion ) ;

	lsk.ls = cron_komm.ls ;
	lsk.fil = 0 ;
	lsk.mdn = cron_komm.mdn ;
	di = Lsk.openlsk();
	di = Lsk.leselsk();
	if ( di || ( lsk.ls_stat > 4 ))	// da ist was faul !!!!!
		return (aktion) ;
	kun.kun = cron_komm.kun ;
	kun.fil = 0 ;
	kun.mdn = cron_komm.mdn ;
	di = Kun.openkun() ;
	di = Kun.lesekun() ;	// Kunde komplettieren


	mdn.mdn = DEFAULTMANDANT ;
	di = mdn_class.openmdn();
	di = mdn_class.lesemdn();
< -------- */


	trapopspm.mdn = DEFAULTMANDANT ;
	trapopspm.fil = 0 ;
	trapopspm.rech = lsk.rech ;

	trapopspm_class.del_pspm() ;

	while ( ! iret )
	{


		nvepoint = 0 ;
		int artik9996 = 0 ;	// DD-Palette
		int artik9997 = 0 ;	// Euro-Palette
		int artik9995 = 0 ;	// Kiste allgemein
		int artik9998 = 0 ;	// E2-Kiste

// 271114 : weitere Artikel 

		int artik9987 = 0 ;	// EURO EW Packmittel
		int artik9988 = 0 ;	// DD   EW Packmittel
		int artik9989 = 0 ;	// EURO    Packmittel
		int artik9990 = 0 ;	// DD      Packmittel
		int artik9991 = 0 ;	// DD   EW
		int artik9992 = 0 ;	// EURO EW


		lsp.ls = lsk.ls ;
		lsp.mdn = lsk.mdn ;
		lsp.fil = 0 ;
/* ----> 271114 : counter ganz anders 
		lsp.a = 9996.0 ;
		double anzi = lsp_class.dbcount() ;
        artik9996 = anzi ;
		lsp.a = 9997.0 ;
		anzi = lsp_class.dbcount() ;
		artik9997 = anzi ;
		lsp.a = 9995.0 ;
		anzi = lsp_class.dbcount() ;
		artik9995 = anzi ;
		lsp.a = 9998.0 ;
		anzi = lsp_class.dbcount() ;
		artik9998 = anzi ;
< ----- */

// 271114
		double anzi = lsp_class.dbcount() ;
		artik9996 = lsp_class.come9996 ;
		artik9997 = lsp_class.come9997 ;
		artik9995 = lsp_class.come9995 ;
		artik9998 = lsp_class.come9998 ;

		artik9987 = lsp_class.come9987 ;
		artik9988 = lsp_class.come9988 ;
		artik9989 = lsp_class.come9989 ;
		artik9990 = lsp_class.come9990 ;



		trapopspm.mdn = DEFAULTMANDANT ;
		trapopspm.fil = 0 ;
		trapopspm.ls = lsk.ls ;
		sprintf ( trapopspm.blg_typ, "L" ) ;
		trapopspm.lfd = 1 ;
		trapopspm.pm = 0.0 ;
		trapopspm.pmzahl = 0.0 ;
		trapopspm.ps1 = 0.0 ;
		trapopspm.ps1zahl = 0.0 ;
		trapopspm.ps2 = 0.0 ;
		trapopspm.ps2zahl = 0.0 ;
		trapopspm.ps3 = 0.0 ;
		trapopspm.ps3zahl = 0.0 ;
		trapopspm.ps4 = 0.0 ;
		trapopspm.ps4zahl = 0.0 ;
		trapopspm.rech = lsk.rech ;

		traponve.ls = lsk.ls ;	// mdn=1 and blg_typ ="L"
		traponve_class.del_nve() ;

		long lfdinls = 0 ;	// 271114 : nach hier oben verschoben 

		int psart = 0 ;	// Nur maximal 4 Packst�ckarten zulaessig ( sollte eigentlich ausreichen )
		int pmart = 0 ;	// Packmittel-Counter - falls auf einem LS noch gar keine Packmittel mitgeteilt wurden !!!!


// Nur Palettenhandling realisiert, keine Etiketten f�r Einzelkisten ( waere z.B. f�r mehrere Lieferungen auf einer Palette notwendig )

//		if ( artik9996 > 0.01  || artik9997 > 0.01 )	// es gibt explizit Packmittel
//		{

		if ( artik9991 > 0 )	// DD  EW
		{
			if ( psart > 3 )
				psart ++ ;
			if ( psart == 3 )
			{
				psart ++;
				trapopspm.ps4 = 9991 ; trapopspm.ps4zahl = artik9991;
			}
			if ( psart == 2 )
			{
				psart ++;
				trapopspm.ps3 = 9991 ; trapopspm.ps3zahl = artik9991;
			}
			if ( psart == 1 )
			{
				psart ++;
				trapopspm.ps2 = 9991 ; trapopspm.ps2zahl = artik9991;
			}
			if ( psart == 0 )
			{
				psart ++;
				trapopspm.ps1 = 9991 ; trapopspm.ps1zahl = artik9991;
			}
		}
		if ( artik9992 > 0 )	// EUR EW
		{
			if ( psart > 3 )
					psart ++ ;
			if ( psart == 3 )
			{
				psart ++;
				trapopspm.ps4 = 9992 ; trapopspm.ps4zahl = artik9992;
			}
			if ( psart == 2 )
			{
				psart ++;
				trapopspm.ps3 = 9992 ; trapopspm.ps3zahl = artik9992;
			}
			if ( psart == 1 )
			{
				psart ++;
				trapopspm.ps2 = 9992 ; trapopspm.ps2zahl = artik9992;
			}
			if ( psart == 0 )
			{
				psart ++;
				trapopspm.ps1 = 9992 ; trapopspm.ps1zahl = artik9992;
			}
		}
		if ( artik9996 > 0 )	// DD  -Palette
		{
			if ( psart > 3 )
					psart ++ ;
			if (psart == 3 )
			{
				psart ++;
				trapopspm.ps4 = 9996 ; trapopspm.ps4zahl = artik9996;
			}
			if ( psart == 2 )
			{
				psart ++;
				trapopspm.ps3 = 9996 ; trapopspm.ps3zahl = artik9996;
			}
			if ( psart == 1 )
			{
				psart ++;
				trapopspm.ps2 = 9996 ; trapopspm.ps2zahl = artik9996;
			}
			if ( psart == 0 )
			{
				psart ++;
				trapopspm.ps1 = 9996 ; trapopspm.ps1zahl = artik9996;
			}
		}
		if ( artik9997 > 0 )	// EUR 
		{
			if ( psart > 3 )
					psart ++ ;
			if ( psart == 3 )
			{
				psart ++;
				trapopspm.ps4 = 9997 ; trapopspm.ps4zahl = artik9997;
			}
			if ( psart == 2 )
			{
				psart ++;
				trapopspm.ps3 = 9997 ; trapopspm.ps3zahl = artik9997;
			}
			if ( psart == 1 )
			{
				psart ++;
				trapopspm.ps2 = 9997 ; trapopspm.ps2zahl = artik9997;
			}
			if ( psart == 0 )
			{
				psart ++;
				trapopspm.ps1 = 9997 ; trapopspm.ps1zahl = artik9997;
			}
		}
		if ( artik9995 > 0 )	// Kiste
		{
			if ( psart > 3 )
					psart ++ ;
			if ( psart == 3 )
			{
				psart ++;
				trapopspm.ps4 = 9995 ; trapopspm.ps4zahl = artik9995;
			}
			if ( psart == 2 )
			{
				psart ++;
				trapopspm.ps3 = 9995 ; trapopspm.ps3zahl = artik9995;
			}
			if ( psart == 1 )
			{
				psart ++;
				trapopspm.ps2 = 9995 ; trapopspm.ps2zahl = artik9995;
			}
			if ( psart == 0 )
			{
				psart ++;
				trapopspm.ps1 = 9995 ; trapopspm.ps1zahl = artik9995;
			}

		}
		if ( artik9998 > 0 )	// E2
		{
			if ( psart > 3 )
					psart ++ ;
			if ( psart == 3 )
			{
				psart ++;
				trapopspm.ps4 = 9998 ; trapopspm.ps4zahl = artik9998;
			}
			if ( psart == 2 )
			{
				psart ++;
				trapopspm.ps3 = 9998 ; trapopspm.ps3zahl = artik9998;
			}
			if ( psart == 1 )
			{
				psart ++;
				trapopspm.ps2 = 9998 ; trapopspm.ps2zahl = artik9998;
			}
			if ( psart == 0 )
			{
				psart ++;
				trapopspm.ps1 = 9998 ; trapopspm.ps1zahl = artik9998;
			}
		}


/* ---> 271114 : alles ganz anders  es sind ja bis zu 4 Packst�ckarten unterst�tzt 
			if ( artik9995 > 0.01 )	// Kisten
			{
				trapopspm.ps1 = 9995.0 ;
				trapopspm.ps1zahl = artik9995 ;
				if ( artik9998 > 0.01 )	// Kisten + E2 
				{
					trapopspm.ps2 = 9998.0 ;
					trapopspm.ps2zahl = artik9998 ;
				}
// red.		else
//				{
//					trapopspm.ps2 = 0.0 ;
//					trapopspm.ps2zahl = 0.0 ;
//				}
			}
			else
			{
				if ( artik9998 > 0.01 )	// nur E2
				{
					trapopspm.ps1 = 9998.0 ;
					trapopspm.ps1zahl = artik9998 ;
				}
// red.			else	// nix
//				{
//					trapopspm.ps1 = 0.0 ;
//					trapopspm.ps1zahl = 0.0 ;
//					trapopspm.ps2 = 0.0 ;
//					trapopspm.ps2zahl = 0.0 ;
//				}
			}
		}

		trapopspm.ps3 = 0.0 ;
		trapopspm.ps3zahl = 0.0 ;
		trapopspm.ps4 = 0.0 ;
		trapopspm.ps4zahl = 0.0 ;
< ------- */

		trapopspm.rech = lsk.rech  ;

		traponve.mdn = trapopspm.mdn ;
		traponve.fil= trapopspm.fil ;
		traponve.ls = trapopspm.ls ;
		sprintf ( traponve.blg_typ, "L" ) ;
//    traponve.lfd = trapopspm.lfd ;
//    traponve.nve char(20),
//    traponve.a decimal(13,0),
		traponve.delstatus = 0 ;

// 271114 : nach oben		long lfdinls = 0 ;

		if ( artik9987 > 0 )	// EUR EW
		{
			pmart++;
			trapopspm.pm   = 9987 ;
			trapopspm.pmzahl = artik9987 ;
			di = trapopspm_class.inserttrapopspm() ;
			trapopspm.ps1 = 0.0 ;	// Notbremse : falls Mehrfach-PM, dann immer alles auf das erste PM zuordnen
			trapopspm.ps1zahl = 0.0 ;
			trapopspm.ps2 = 0.0 ;
			trapopspm.ps2zahl = 0.0 ;
			trapopspm.ps3 = 0.0 ;
			trapopspm.ps3zahl = 0.0 ;
			trapopspm.ps4 = 0.0 ;
			trapopspm.ps4zahl = 0.0 ;

			di = 0 ;
			while ( di < artik9987 && nvepoint < MAXNVE )
			{
				traponve.lfd = trapopspm.lfd ;
				lfdinls ++ ;
				Generierenve ( lfdinls ) ;
				sprintf ( traponve.nve ,"%s", okstring2 ) ;
				traponve.a = 9987.0 ;
// alles generieren und bef�llen
				sprintf ( nvepuffer[nvepoint], ("%s"), traponve.nve );
				nvetyppuf[nvepoint] = 9987.0 ;
				nvepoint ++ ;
				nvetyppuf[nvepoint] = 0.0 ;
				traponve_class.inserttraponve () ;
				di ++ ;
			}
			trapopspm.lfd ++ ;
		}

		if ( artik9988 > 0 )		// DD EW
		{
			pmart++;
			trapopspm.pm = 9988.0 ;
			trapopspm.pmzahl = artik9988 ;

			di = trapopspm_class.inserttrapopspm() ;

			trapopspm.ps1 = 0.0 ;	// Notbremse : falls Mehrfach-PM, dann immer alles auf das erste PM zuordnen
			trapopspm.ps1zahl = 0.0 ;
			trapopspm.ps2 = 0.0 ;
			trapopspm.ps2zahl = 0.0 ;
			trapopspm.ps3 = 0.0 ;
			trapopspm.ps3zahl = 0.0 ;
			trapopspm.ps4 = 0.0 ;
			trapopspm.ps4zahl = 0.0 ;
			di = 0 ;
			while ( di < artik9988 && nvepoint < MAXNVE )
			{
				traponve.lfd = trapopspm.lfd ;
				lfdinls ++ ;
				Generierenve ( lfdinls ) ;
				sprintf ( traponve.nve ,"%s", okstring2 ) ;
				traponve.a = 9988.0 ;
// alles generieren und bef�llen
				sprintf ( nvepuffer[nvepoint], ("%s"), traponve.nve );
				nvetyppuf[nvepoint] = 9988.0 ;
				nvepoint ++ ;
				nvetyppuf[nvepoint] = 0.0 ;
				traponve_class.inserttraponve () ;
				di ++ ;
			}
			trapopspm.lfd ++ ;
		}

		if ( artik9989 > 0 )		// EUR 
		{
			pmart++;
			trapopspm.pm = 9989.0 ;
			trapopspm.pmzahl = artik9989 ;
			di = trapopspm_class.inserttrapopspm() ;
			trapopspm.ps1 = 0.0 ;	// Notbremse : falls Mehrfach-PM, dann immer alles auf das erste PM zuordnen
			trapopspm.ps1zahl = 0.0 ;
			trapopspm.ps2 = 0.0 ;
			trapopspm.ps2zahl = 0.0 ;
			trapopspm.ps3 = 0.0 ;
			trapopspm.ps3zahl = 0.0 ;
			trapopspm.ps4 = 0.0 ;
			trapopspm.ps4zahl = 0.0 ;
			di = 0 ;
			while ( di < artik9989 && nvepoint < MAXNVE )
			{
				traponve.lfd = trapopspm.lfd ;
				lfdinls ++ ;
				Generierenve ( lfdinls ) ;
				sprintf ( traponve.nve ,"%s", okstring2 ) ;
				traponve.a = 9989.0 ;
// alles generieren und bef�llen
				sprintf ( nvepuffer[nvepoint], ("%s"), traponve.nve );
				nvetyppuf[nvepoint] = 9989.0 ;
				nvepoint ++ ;
				nvetyppuf[nvepoint] = 0.0 ;
				traponve_class.inserttraponve () ;
				di ++ ;
			}
			trapopspm.lfd ++ ;
		}

		if ( artik9990 > 0 )		// DD 
		{
			pmart++;
			trapopspm.pm = 9990.0 ;
			trapopspm.pmzahl = artik9990 ;
			di = trapopspm_class.inserttrapopspm() ;

			trapopspm.ps1 = 0.0 ;	// Notbremse : falls Mehrfach-PM, dann immer alles auf das erste PM zuordnen
			trapopspm.ps1zahl = 0.0 ;
			trapopspm.ps2 = 0.0 ;
			trapopspm.ps2zahl = 0.0 ;
			trapopspm.ps3 = 0.0 ;
			trapopspm.ps3zahl = 0.0 ;
			trapopspm.ps4 = 0.0 ;
			trapopspm.ps4zahl = 0.0 ;
			di = 0 ;
			while ( di < artik9990 && nvepoint < MAXNVE )
			{
				traponve.lfd = trapopspm.lfd ;
				lfdinls ++ ;
				Generierenve ( lfdinls ) ;
				sprintf ( traponve.nve ,"%s", okstring2 ) ;
				traponve.a = 9990.0 ;
// alles generieren und bef�llen
				sprintf ( nvepuffer[nvepoint], ("%s"), traponve.nve );
				nvetyppuf[nvepoint] = 9990.0 ;
				nvepoint ++ ;
				nvetyppuf[nvepoint] = 0.0 ;
				traponve_class.inserttraponve () ;
				di ++ ;
			}
			trapopspm.lfd ++ ;
		}

		if ( pmart == 0 &&
			(artik9998 > 0 || artik9997 > 0 || artik9996 > 0 || artik9995 > 0 || artik9991 > 0 || artik9992 > 0 ) )	// nur Packst�cke ohne Packmittel -> dann ist das Packmittel halt auf einem anderen LS mit drauf ?! 
		{
			trapopspm.pm = 0.0 ;
			trapopspm.pmzahl = 0.0 ;
			di = trapopspm_class.inserttrapopspm() ;

			trapopspm.ps1 = 0.0 ;	
			trapopspm.ps1zahl = 0.0 ;
			trapopspm.ps2 = 0.0 ;
			trapopspm.ps2zahl = 0.0 ;
			trapopspm.ps3 = 0.0 ;
			trapopspm.ps3zahl = 0.0 ;
			trapopspm.ps4 = 0.0 ;
			trapopspm.ps4zahl = 0.0 ;
			trapopspm.lfd ++ ;

		}


/* 271114 : alles anders ....

		if ( artik9996 > 0 )	// DD-Palette
		{
			trapopspm.pm   = 9996 ;
			trapopspm.pmzahl = artik9996 ;
	
			di = trapopspm_class.inserttrapopspm() ;

			trapopspm.ps1 = 0.0 ;	// Notbremse : falls Mehrfach-PM, dann immer auf das erste PM zuordnen
			trapopspm.ps1zahl = 0.0 ;
			trapopspm.ps2 = 0.0 ;
			trapopspm.ps2zahl = 0.0 ;

			di = 0 ;
			while ( di < artik9996 && nvepoint < MAXNVE )
			{
				traponve.lfd = trapopspm.lfd ;
				lfdinls ++ ;
				Generierenve ( lfdinls ) ;
				sprintf ( traponve.nve ,"%s", okstring2 ) ;
				traponve.a = 9996.0 ;	// irrelevant
// alles generieren und bef�llen
				sprintf ( nvepuffer[nvepoint], ("%s"), traponve.nve );
				nvetyppuf[nvepoint] = 9996.0 ;
				nvepoint ++ ;
				nvetyppuf[nvepoint] = 0.0 ;
				traponve_class.inserttraponve () ;
				di ++ ;
			}
			trapopspm.lfd ++ ;
		}

		if ( artik9997 > 0 )
		{
			trapopspm.pm = 9997.0 ;
			trapopspm.pmzahl = artik9997 ;

			di = trapopspm_class.inserttrapopspm() ;

			di = 0 ;
			while ( di < artik9997 && nvepoint < MAXNVE )
			{
				traponve.lfd = trapopspm.lfd ;
				lfdinls ++ ;
				Generierenve ( lfdinls ) ;
				sprintf ( traponve.nve ,"%s", okstring2 ) ;
				traponve.a = 9997.0 ;	// irrelevant
// alles generieren und bef�llen
				sprintf ( nvepuffer[nvepoint], ("%s"), traponve.nve );
				nvetyppuf[nvepoint] = 9997.0 ;
				nvepoint ++ ;
				nvetyppuf[nvepoint] = 0.0 ;
				traponve_class.inserttraponve () ;
				di ++ ;
			}
			trapopspm.lfd ++ ;
		}
271114 	<------- */

		if ( lfdinls > 0 )
		{
			for ( int ii= 0 ; ii <  lfdinls  ; ii ++ )
			{
// evtl. sp�ter mal : 			DruckeEtiketten(nvepuffer[ii]) ;
			}
			aktion ++ ;
		}
		iret = lsk_class.leserechlsk() ;
	}
	return (aktion) ;

}




// RepDialog-Dialogfeld

IMPLEMENT_DYNAMIC(RepDialog, CPropertyPage)

RepDialog::RepDialog()
	: CPropertyPage(RepDialog::IDD)
	, v_rech_nr(0)
{

}

RepDialog::~RepDialog()
{
}

void RepDialog::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RECH_NR, m_rech_nr);
	DDX_Text(pDX, IDC_RECH_NR, v_rech_nr);
	DDV_MinMaxLong(pDX, v_rech_nr, 0, 99999999);
}


BEGIN_MESSAGE_MAP(RepDialog, CPropertyPage)
	ON_EN_KILLFOCUS(IDC_RECH_NR, &RepDialog::OnEnKillfocusRechNr)
	ON_BN_CLICKED(IDOK, &RepDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &RepDialog::OnBnClickedCancel)
END_MESSAGE_MAP()


// RepDialog-Meldungshandler

void RepDialog::OnEnKillfocusRechNr()
{
		UpdateData(TRUE) ;
		UpdateData(FALSE) ;

}

void RepDialog::OnBnClickedOk()
{

	UpdateData(TRUE) ;

	lsk.mdn = DEFAULTMANDANT ;
	lsk.rech = v_rech_nr ;
	int iret = lsk_class.openrechlsk() ;
	iret = lsk_class.leserechlsk() ;
	CString XA ;
	XA.Format( "Rech-Nr.: %d" ,lsk.rech ) ;

	if ( iret )
	{
		MessageBox("Ung�ltige Rechnungsnummer", XA , MB_OK|MB_ICONSTOP);
	}
	else
	{

		if ( MessageBox("NVE wirklich l�schen und neu aufbauen ?? ", XA , MB_YESNO ) == IDYES )
		{


			int obaktion = NVE_generieren(0) ;

			if ( obaktion )
				MessageBox("NVE neu aufgebaut", XA , MB_OK|MB_ICONSTOP);
			else
				MessageBox("keine Leergutinformationen gefunden", XA , MB_OK|MB_ICONSTOP);

		}
	}

	CDialog::OnOK();
}

void RepDialog::OnBnClickedCancel()
{
		CDialog::OnCancel();

}
