#ifndef _MDN_DEF
#define _MDN_DEF

#define DEFAULTMANDANT 1

struct MDN {
    short mdn ;
	long adr ;
    char iln[17] ;
    char ust_id[12] ;
};

extern struct MDN mdn, mdn_null;

class MDN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesemdn (void);
               int openmdn (void);
               MDN_CLASS () : DB_CLASS ()
               {
               }
};
#endif

