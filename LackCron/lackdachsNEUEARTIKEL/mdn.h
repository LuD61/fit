#ifndef _MDN_DEF
#define _MDN_DEF

struct MDN {
    short mdn ;
	long adr ;
    char iln[17] ;
    char ust_id[12] ;
};
extern struct MDN mdn, mdn_null;

extern class MDN_CLASS mdn_class ;


class MDN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesemdn (void);
               int openmdn (void);
               MDN_CLASS () : DB_CLASS ()
               {
               }
};
#endif

