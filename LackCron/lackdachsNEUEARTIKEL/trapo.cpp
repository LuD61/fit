#include <Windows.h>
#include "DbClass.h"
#include "trapo.h"

struct TRAPOPSPM trapopspm,  prapopspm_null;
struct TRAPONVE traponve,  traponve_null;
struct TRAPOTAG trapotag,  trapotag_null;
struct TRAPOKO trapoko,  trapoko_null;
struct LACKDNUM lackdnum,  lackdnum_null;
struct SYS_PAR sys_par,  sys_par_null;	// 261114

extern DB_CLASS dbClass;

int LACKDNUM_CLASS::openlackdnum (void)
{
	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (readcursor);
}

int TRAPOPSPM_CLASS::opentrapopspm (void)
{
	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (readcursor);
}

int TRAPONVE_CLASS::opentraponve (void)
{
	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (readcursor);
}

int TRAPOTAG_CLASS::opentrapotag (void)
{
	if ( readcursor < 0 ) prepare ();	
    return dbClass.sqlopen (readcursor);
}


int LACKDNUM_CLASS::leselackdnum (void)
{
	if (readcursor < 0 )
	{
		prepare ();
		dbClass.sqlopen (readcursor);
	}
	int di = dbClass.sqlfetch (readcursor);
	return di;   
}


int TRAPOPSPM_CLASS::lesetrapopspm (void)
{
	if (readcursor < 0 )
	{
		prepare ();
		dbClass.sqlopen (readcursor);
	}
	int di = dbClass.sqlfetch (readcursor);
	return di;   
}
int TRAPONVE_CLASS::lesetraponve (void)
{
	if (readcursor < 0 )
    {
		prepare ();
		dbClass.sqlopen (readcursor);
	}
	int di = dbClass.sqlfetch (readcursor);
	return di;   
}
int TRAPOTAG_CLASS::lesetrapotag (void)
{
	if (readcursor < 0 )
    {
		prepare ();
		dbClass.sqlopen (readcursor);
	}
	int di = dbClass.sqlfetch (readcursor);
	return di;   
}

int TRAPOPSPM_CLASS::inserttrapopspm (void)
{
	if (readcursor < 0 )
	{
		prepare ();
	}
	int di = dbClass.sqlexecute (ins_cursor);
	return di;
}

int TRAPONVE_CLASS::inserttraponve (void)
{
	if (readcursor < 0 )
	{
		prepare ();
	}
	int di = dbClass.sqlexecute (ins_cursor);
	return di;
}
int TRAPOTAG_CLASS::inserttrapotag (void)
{
	if (readcursor < 0 )
	{
		prepare ();
	}
	int di = dbClass.sqlexecute (ins_cursor);
	return di;
}

int LACKDNUM_CLASS::writelackdnum (void)
{
	if (readcursor < 0 )
	{
		prepare ();
	}
	int di = dbClass.sqlexecute (upd_cursor);
	return di;
}


int TRAPOPSPM_CLASS::updatetrapopspm (void)
{
	if (readcursor < 0 )
	{
		prepare ();
	}
	int di = dbClass.sqlexecute (upd_cursor);
	return di;
}

int TRAPONVE_CLASS::updatetraponve (void)
{
	if (readcursor < 0 )
	{
		prepare ();
	}
	int di = dbClass.sqlexecute (upd_cursor);
	return di;
}
int TRAPOTAG_CLASS::updatetrapotag (void)
{
	if (readcursor < 0 )
	{
		prepare ();
	}
	int di = dbClass.sqlexecute (upd_cursor);
	return di;
}
int TRAPOTAG_CLASS::deletetrapotag (void)
{
	if (readcursor < 0 )
	{
		prepare ();
	}
	int di = dbClass.sqlexecute (del_cursor);
	return di;
}

void TRAPOPSPM_CLASS::prepare (void)
{

// fuer ins_cursor 

	dbClass.sqlin ((short  *) &trapopspm.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((short  *) &trapopspm.fil ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &trapopspm.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  trapopspm.blg_typ ,SQLCHAR, 2) ;
	dbClass.sqlin ((long   *) &trapopspm.lfd ,SQLLONG, 0) ;

	dbClass.sqlin ((double *) &trapopspm.pm ,SQLDOUBLE, 0) ;
	dbClass.sqlin ((double *) &trapopspm.pmzahl ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps1 ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps1zahl ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps2 ,SQLDOUBLE, 0);

	dbClass.sqlin ((double *) &trapopspm.ps2zahl ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps3 ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps3zahl ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps4 ,SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &trapopspm.ps4zahl ,SQLDOUBLE, 0);

	dbClass.sqlin ((long   *) &trapopspm.rech ,SQLLONG, 0);

 
    ins_cursor = (short)dbClass.sqlcursor ("insert into trapopspm ( "
		" mdn ,fil ,ls ,blg_typ ,lfd "
		",pm ,pmzahl ,ps1 ,ps1zahl ,ps2 "
		",ps2zahl ,ps3 ,ps3zahl ,ps4 ,ps4zahl "
		",rech "
		" ) values ( "
		" ? ,? ,? ,? ,? "
		",? ,? ,? ,? ,? "
		",? ,? ,? ,? ,? "
		",?" 
		" ) " ) ;

// fuer readcursor - wird hier nicht wirklich benoetigt 

	dbClass.sqlin ((short  *) &trapopspm.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &trapopspm.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  trapopspm.blg_typ ,SQLCHAR, 2) ;

	dbClass.sqlout ((short  *) &trapopspm.fil ,SQLSHORT, 0) ;
	dbClass.sqlout ((long   *) &trapopspm.lfd ,SQLLONG, 0) ;

	dbClass.sqlout ((double *) &trapopspm.pm ,SQLDOUBLE, 0) ;
	dbClass.sqlout ((double *) &trapopspm.pmzahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps1 ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps1zahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps2 ,SQLDOUBLE, 0);

	dbClass.sqlout ((double *) &trapopspm.ps2zahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps3 ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps3zahl ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps4 ,SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &trapopspm.ps4zahl ,SQLDOUBLE, 0);

	dbClass.sqlout ((long   *) &trapopspm.rech ,SQLLONG, 0);

 
    readcursor = (short)dbClass.sqlcursor ("select "
		" fil ,lfd "
		",pm ,pmzahl ,ps1 ,ps1zahl ,ps2 "
		",ps2zahl ,ps3 ,ps3zahl ,ps4 ,ps4zahl "
		",rech "
		" from trapopspm "
		" where "
		" mdn = ? and ls = ? and blg_typ = ? "
		" order by lfd " ) ;


// fuer upd_cursor 

	dbClass.sqlin ((long   *) &trapopspm.rech ,SQLLONG, 0);

	dbClass.sqlin ((short  *) &trapopspm.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &trapopspm.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  trapopspm.blg_typ ,SQLCHAR, 2) ;

	 
    upd_cursor = (short)dbClass.sqlcursor ("update trapopspm "
		" set rech = ? "
		" where "
		" mdn = ? and ls = ? and blg_typ = ? " ) ;
}

void TRAPONVE_CLASS::prepare (void)
{

// fuer ins_cursor 

	dbClass.sqlin ((short  *) &traponve.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((short  *) &traponve.fil ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &traponve.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  traponve.blg_typ ,SQLCHAR, 2) ;
	dbClass.sqlin ((long   *) &traponve.lfd ,SQLLONG, 0) ;

	dbClass.sqlin ((char   *)  traponve.nve ,SQLCHAR, 21) ;
	dbClass.sqlin ((double *) &traponve.a ,SQLDOUBLE, 0) ;
	dbClass.sqlin ((short  *) &traponve.delstatus ,SQLSHORT, 0) ;

 
    ins_cursor = (short)dbClass.sqlcursor ("insert into traponve ( "
		" mdn ,fil ,ls ,blg_typ ,lfd "
		",nve ,a ,delstatus "
		" ) values ( "
		" ? ,? ,? ,? ,? "
		",? ,? ,? "
		" ) " ) ;

// fuer readcursor - wird hier nicht wirklich benoetigt 

	dbClass.sqlin ((short  *) &traponve.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &traponve.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  traponve.blg_typ ,SQLCHAR, 2) ;
	dbClass.sqlout ((long   *) &traponve.lfd ,SQLLONG, 0) ;

	dbClass.sqlout ((short  *) &traponve.fil ,SQLSHORT, 0) ;


	dbClass.sqlout ((char   *)  traponve.nve ,SQLCHAR, 21) ;
	dbClass.sqlout ((double *) &traponve.a ,SQLDOUBLE, 0) ;
	dbClass.sqlout ((short  *) &traponve.delstatus ,SQLSHORT, 0) ;
 
    readcursor = (short)dbClass.sqlcursor ("select "
		" fil "
		",nve ,a ,delstatus "
		" from traponve "
		" where "
		" mdn = ? and ls = ? and blg_typ = ? and lfd = ? " ) ;


// fuer upd_cursor wird hier nicht wirklich benoetigt

	dbClass.sqlin ((char   *)  traponve.nve ,SQLCHAR, 21);
	dbClass.sqlin ((double *) &traponve.a ,SQLDOUBLE, 0 );

	dbClass.sqlin ((short  *) &traponve.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((long   *) &traponve.ls ,SQLLONG, 0);
	dbClass.sqlin ((char   *)  traponve.blg_typ ,SQLCHAR, 2) ;
	dbClass.sqlin ((long   *) &traponve.lfd ,SQLLONG, 0);
	 
    upd_cursor = (short)dbClass.sqlcursor ("update traponve "
		" set nve = ? , a = ? "
		" where "
		" mdn = ? and ls = ? and blg_typ = ?  and lfd = ? " ) ;
}

void TRAPOTAG_CLASS::prepare (void)
{

// fuer ins_cursor 

	dbClass.sqlin ((short  *) &trapotag.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((short  *) &trapotag.tou ,SQLLONG, 0) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &trapotag.lieferdat ,SQLTIMESTAMP, 26);
	dbClass.sqlin ((long   *)  &trapotag.status ,SQLLONG, 0) ;
 
    ins_cursor = (short)dbClass.sqlcursor ("insert into trapotag( "
		" mdn ,tou ,lieferdat ,status "
		" ) values ( "
		" ? ,? ,? ,?  "
		" ) " ) ;

// fuer readcursor -i.W. existenz-Test  
	
	dbClass.sqlin ((short  *) &trapotag.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &trapotag.lieferdat ,SQLTIMESTAMP, 26);
	dbClass.sqlin ((long   *) &trapotag.tou ,SQLLONG, 0);

	dbClass.sqlout ((long   *) &trapotag.status ,SQLLONG, 0) ;

    readcursor = (short)dbClass.sqlcursor ("select "
		" status "
		" from trapotag "
		" where "
		" mdn = ? and lieferdat = ? and tou = ? " ) ;

// fuer upd_cursor wird hier nicht wirklich benoetigt

	dbClass.sqlin ((long   *) &trapotag.status ,SQLLONG, 0) ;

	dbClass.sqlin ((short  *) &trapotag.mdn ,SQLSHORT, 0) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &trapotag.lieferdat ,SQLTIMESTAMP, 26);
	dbClass.sqlin ((long   *) &trapotag.tou ,SQLLONG, 0);

	 
    upd_cursor = (short)dbClass.sqlcursor ("update trapotag "
		" set status = ? "
		" where "
		" mdn = ? and lieferdat = ? and tou = ? " ) ;


// fuer del_cursor : altes Zeug wegwerfen

//	dbClass.sqlin ((short  *) &trapotag.mdn ,SQLSHORT, 0) ;
//	dbClass.sqlin ((long   *) &trapotag.tou ,SQLLONG, 0);
 
    del_cursor = (short)dbClass.sqlcursor ("delete from trapotag "
		" where "
		" mdn = 1 and lieferdat < TODAY -15 " ) ;

}

int TRAPOKO_CLASS::lesealltrapoko (void)
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}
int TRAPOKO_CLASS::lesetrapoko (void)
{
      int di = dbClass.sqlfetch (test_upd_cursor);
	  return di;
}


int TRAPOKO_CLASS::openalltrapoko (void)
{
		if ( readcursor < 0 ) prepare ();	
         return dbClass.sqlopen (readcursor);
}

int TRAPOKO_CLASS::opentrapoko (void)
{
		if ( readcursor < 0 ) prepare ();	
		return dbClass.sqlopen (test_upd_cursor);
}


void TRAPOKO_CLASS::schliessen (void)
{
	if ( readcursor > -1 )
	{	// geht ca. 200 mal gut, dann knallt es wohl 
		dbClass.sqlclose(readcursor ) ;
		readcursor = -1 ;
	}
	if ( test_upd_cursor > -1 )
	{	// geht ca. 200 mal gut, dann knallt es wohl 
		dbClass.sqlclose(test_upd_cursor ) ;
		test_upd_cursor = -1 ;
	}
}


void TRAPOKO_CLASS::prepare (void)
{

// Lese alle Saetze eines trapoko-Typs

// evtl. spaeter mal optimierung der Suche ?!

		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( long *) &trapoko.typ, SQLLONG, 0 ) ;

		dbClass.sqlout (( long *) &trapoko.plz_v, SQLLONG, 0 ) ;
		dbClass.sqlout (( long *) &trapoko.plz_b, SQLLONG, 0 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_von,  SQLCHAR, 9 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_bis,  SQLCHAR, 9 ) ;

		dbClass.sqlout (( double *) &trapoko.von1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert1,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert2,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert3,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert4,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert5,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert6,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert7,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert8,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert9,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert10,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( short *) &trapoko.staat,SQLSHORT, 0 ) ;	// 130513

			readcursor = (short) dbClass.sqlcursor ("select "
			" plz_v, plz_b , plz_von , plz_bis "
			" , von1, bis1 , wert1 "
			" , von2, bis2 , wert2 "
			" , von3, bis3 , wert3 "
			" , von4, bis4 , wert4 "
			" , von5, bis5 , wert5 "
			" , von6, bis6 , wert6 "
			" , von7, bis7 , wert7 "
			" , von8, bis8 , wert8 "
			" , von9, bis9 , wert9 "
			" , von10,bis10, wert10 , staat "
	
			" from trapoko where mdn = ? and leitw_typ = ?  and typ = ?" 
			" order by plz_von, plz_bis desc " ) ;

// fuer konkreten Satz nach aktueller Spezi test_upd_cursor

// 130513 : genau hier muss der staat noch reingeschraubt werden - aktiviert am 170713
// wenn die PLZ mehrdeutig werden bisher aus Kompatibilitätsgrunden nicht aktiv
//		dbClass.sqlin (( short *)&trapoko.staat, SQLSHORT, 0 ) ;
//		" from trapoko where mdn = ? and leitw_typ = ?  and plz_von = ? and staat = ? " 

		dbClass.sqlin (( short *)&trapoko.mdn, SQLSHORT, 0 ) ;
		dbClass.sqlin (( long  *)&trapoko.leitw_typ, SQLLONG, 0 ) ;
		dbClass.sqlin (( char *) trapoko.plz_von, SQLCHAR, 9 ) ;
		dbClass.sqlin (( short *)&trapoko.staat, SQLSHORT, 0 ) ;

		dbClass.sqlout (( long *) &trapoko.typ, SQLLONG, 0 ) ;
		dbClass.sqlout (( long *) &trapoko.plz_b, SQLLONG, 0 ) ;
		dbClass.sqlout (( long *) &trapoko.plz_v,  SQLLONG, 0 ) ;
		dbClass.sqlout (( char *)  trapoko.plz_bis,  SQLCHAR, 9 ) ;

		dbClass.sqlout (( double *) &trapoko.von1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis1, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert1,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis2, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert2,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis3, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert3,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis4, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert4,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis5, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert5,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis6, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert6,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis7, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert7,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis8, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert8,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis9, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert9,SQLDOUBLE, 0 ) ;

		dbClass.sqlout (( double *) &trapoko.von10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.bis10, SQLDOUBLE, 0 ) ;
		dbClass.sqlout (( double *) &trapoko.wert10,SQLDOUBLE, 0 ) ;

			test_upd_cursor = (short) dbClass.sqlcursor ("select "
			" typ, plz_b , plz_v , plz_bis "
			" , von1, bis1 , wert1 "
			" , von2, bis2 , wert2 "
			" , von3, bis3 , wert3 "
			" , von4, bis4 , wert4 "
			" , von5, bis5 , wert5 "
			" , von6, bis6 , wert6 "
			" , von7, bis7 , wert7 "
			" , von8, bis8 , wert8 "
			" , von9, bis9 , wert9 "
			" , von10,bis10, wert10 "
	
//			" from trapoko where mdn = ? and leitw_typ = ?  and plz_von = ?" 
		" from trapoko where mdn = ? and leitw_typ = ?  and plz_von = ? and staat = ? " 
		" order by typ , plz_bis desc " ) ;
}


void LACKDNUM_CLASS::prepare (void)
{

// fuer readcursor t  
	
	dbClass.sqlout ((char *) lackdnum.numstart ,SQLCHAR, 21) ;
	dbClass.sqlout ((char *) lackdnum.numakt ,  SQLCHAR, 21);
	dbClass.sqlout ((char *) lackdnum.numend ,  SQLCHAR, 21);

    readcursor = (short)dbClass.sqlcursor ("select "
		" numstart, numakt, numend "
		" from lackdnum "
	) ;

// fuer upd_cursor 
	
	dbClass.sqlin ((char  *) lackdnum.numakt ,SQLCHAR, 21) ;

	 
    upd_cursor = (short)dbClass.sqlcursor ("update lackdnum "
		" set numakt = ? "
	) ;

}
// 261114 FF

int SYS_PAR_CLASS::lesesys_par ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int SYS_PAR_CLASS::opensys_par (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

void SYS_PAR_CLASS::prepare (void)
{

	dbClass.sqlin ((char *) sys_par.sys_par_nam, SQLCHAR, 18);

	dbClass.sqlout ((char *) sys_par.sys_par_wrt,SQLCHAR, 2);
	dbClass.sqlout ((char *) sys_par.sys_par_besch,SQLCHAR, 33);
	dbClass.sqlout ((long *) &sys_par.zei, SQLLONG, 0);
	dbClass.sqlout ((short *) &sys_par.delstatus, SQLSHORT, 0);

		readcursor = (short) dbClass.sqlcursor ("select "

	" sys_par_wrt, sys_par_besch, zei, delstatus " 
	" from sys_par where sys_par_nam = ? " ) ;

}
