#ifndef _DBCLASS_DEF
#define  _DBCLASS_DEF
#include <odbcinst.h>
#include <atlstr.h>
#include <sqlext.h>

#ifndef MAXCURS
#define MAXCURS 500
#endif

#ifndef MAXVARS
#define MAXVARS 0x1000
#endif

#define SQLCHAR   0
#define SQLSHORT  1
#define SQLLONG   2
#define SQLDOUBLE 3
#define SQLDATE 4
#define SQLTIMESTAMP 5

#define NEXT 1
#define PRIOR 2
#define PREVIOUS 2
#define FIRST 3
#define LAST 4
#define CURRENT 5
#define RELATIV 7
#define DBABSOLUTE 6


class SQLVAR
{
       public :
           void *var;
           int typ;
           int len;
       SQLVAR ()
       {
           var = NULL;
           typ = 0;
           len = 0;
       }

       void SetVar (void *var)
       {
           this->var = var;
       }

       void *GetVar (void)
       {
           return var;
       }

       void SetTyp (int typ)
       {
           this->typ = typ;
       }

       int GetTyp (void)
       {
           return typ;
       }

       void SetLen (int len)
       {
           this->len = len;
       }

       int GetLen (void)
       {
           return len;
       }
};

class DataBase
{
   public :
        HENV henv;
        HDBC hdbc;
        CString Name;

        DataBase ()
        {
            henv = NULL;
            hdbc = NULL;
        }

        DataBase (HENV henv, HDBC hdbc)
        {
            this->henv = henv;
            this->hdbc = hdbc;
        }

        ~DataBase ()
        {
        }

        HENV GetHenv ()
        {
            return henv;
        }

        HDBC GetHdbc ()
        {
            return hdbc;
        }

        CString GetName ()
        {
            return Name;
        }

        HENV GetHenv (CString Name)
        {
            if (this->Name == Name)
            {
                 return henv;
            }
            return NULL;
        }

        HDBC GetHdbc (CString Name)
        {
            if (this->Name == Name)
            {
                 return hdbc;
            }
            return NULL;
        }

        void SetDbase (CString Name, HENV henv, HDBC hdbc) 
        {
            this->Name = Name;
            this->henv = henv;
            this->hdbc = hdbc;
        }
/*
        const Database& Database::operator=(Database& Database)
        {
                SetBuffer (Txt.GetBuffer ());  
                return *this;
        }
*/
};


class DB_CLASS
{
       public :
               HENV    henv;
               HDBC    hdbc;
               CString name;
               DataBase DBase;
               HSTMT   hstmDirect;
               HSTMT   hstm;
               int     CursTab[MAXCURS];  
               HSTMT   HstmtTab[MAXCURS];  
               SQLVAR  OutVars[MAXVARS];
               SQLVAR  InVars[MAXVARS];
               int     OutAnz;
               int     InAnz;
               int     sqlstatus;
               BOOL    InWork ;
               SDWORD cbLen ;
               int sql_mode ;
			   int nurfetch ;	// 191005
               BOOL (*SqlErrorProc) (SDWORD, CString&);

               short cursor;
	           short readcursor;
	           short readallcursor;
	           short readkcursor;	
               short test_upd_cursor;
               short upd_cursor;
               short ins_cursor;
               short del_cursor;
               short cursor_ausw;
			   short count_cursor;	
               int   scrollpos;
			   BOOL OwnConnect;
               static short ShortNull;
               static long LongNull;
               static double DoubleNull;
               static HENV    mainhenv;
               static HDBC    mainhdbc;
               static CString mainname;



               DB_CLASS ()
               {
                         hstmDirect = NULL;
                         memset ((char *) CursTab,  0, MAXCURS * sizeof (int));
                         memset ((char *) HstmtTab, 0, MAXCURS * sizeof (HSTMT));
                         OutAnz = 0;
                         InAnz  = 0;
                         cbLen = SQL_NTS;
                         sql_mode = 0;	// 0 bedeutet Abbruch , 1 oder 2 geht es mit warnung weiter
                         SqlErrorProc = ErrProc;
                         InWork = FALSE;
						 henv = NULL;
						 hdbc = NULL;
                         cursor          = -1;
                         readcursor 	 = -1;
						 readallcursor	 = -1;
						 readkcursor 	 = -1;
						 test_upd_cursor = -1;
                         upd_cursor      = -1;
                         ins_cursor      = -1;
                         del_cursor      = -1;
                         count_cursor    = -1;
                         scrollpos       = 1;
						 nurfetch        = 0 ;	// 191005
 					     OwnConnect = FALSE;
                         for (int i = 0; i < MAXCURS; i ++)
                         {
                             CursTab[i] = 0;
                         }
                         henv = hdbc = NULL;
               }

               ~DB_CLASS ()
               {
  /*  ----> erzeugt Dr. Watson
                         for (int i = 0; i < MAXCURS; i ++)
                         {
                             if (CursTab[i] != 0)
                             {
                                 sqlclose (CursTab[i]);
                                 CursTab[i] = 0;
                             }
                         }
< ----- */
						 closedbase (name.GetBuffer (80));
               }

               void SetSqlErrorProc (BOOL (*ErrProc) (SDWORD, CString&))
               {
                   this->SqlErrorProc = ErrProc;
               }

               BOOL opendbase (char *, char*, char*);
               BOOL closedbase (char *);
			   static BOOL closedbase ();
               int sqlconnectdbase (char *, char *, char *, char *);
               int sqlconnect (char *, char *, char *);

               void SetDatabase (DataBase& DBase)
               {
                   this->DBase.henv = DBase.GetHenv ();
                   this->DBase.hdbc = DBase.GetHdbc ();
                   this->DBase.Name = DBase.GetName ();
                   henv = DBase.GetHenv ();
                   hdbc = DBase.GetHdbc ();
                   name = DBase.GetName ();
			       OwnConnect = FALSE;
               }

               DataBase &GetDatabase (void)
               {
                   return DBase;
               }


               int beginwork ();
               int commitwork ();
               int rollbackwork ();
               BOOL sqlcomm (char *);
               int sqlcursor (char *);
               int sqlopen (int);
               int sqlfetch (int);
               int sqlexecute (int);
               int sqlclose (int);
			   int closeall ();
               void sqlout (void *, int , int);
               void sqlin  (void *, int , int);
               int TestOut (HSTMT);
               int TestIn  (HSTMT);
               void GetError (HSTMT);
               static BOOL ErrProc (SDWORD, CString&);

               virtual void prepare (void) 
               {
               }
               int dbreadfirst (void);
               int dbread (void);
               int dbupdate (void);
               int dblock (void);
               int dbdelete (void);
               void dbclose (void);

               int dbmove (int);
               int dbmove (int, int);
               int dbcanmove (int);
               int dbcanmove (int, int);
};
#endif