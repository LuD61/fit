#ifndef TOKEN_DEF
#define TOKEN_DEF
#include <string.h> 
#include <stdarg.h> 

class CToken
{
  private :
     CString Buffer;
     CString **Tokens;
     CString sep;
     int AnzToken;
     int AktToken;
  public :
     CToken ();
     CToken (char *, char *);
     CToken (CString&, char *);
     ~CToken ();
     const CToken& operator=(char *);
     const CToken& operator=(CString&);
     void GetTokens (char *);
     void SetSep (char *);
     char * NextToken (void);
     char * GetToken (int);
     int GetAnzToken (void);
};
#endif
