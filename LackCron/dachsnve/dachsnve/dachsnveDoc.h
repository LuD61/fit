// dachsnveDoc.h : Schnittstelle der Klasse CpakdruDoc
//


#pragma once


class CdachsnveDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CdachsnveDoc();
	DECLARE_DYNCREATE(CdachsnveDoc)

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CdachsnveDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


