#include "stdafx.h"
#include "DbClass.h"
#include "fracht.h"

struct FRACHT fracht,  fracht_null;
extern DB_CLASS dbClass;

FRACHT_CLASS fracht_class ;


int FRACHT_CLASS::lesefracht (void)
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int FRACHT_CLASS::insertfracht (void)
{
	if ( readcursor < 0 ) prepare ();	
    int di = dbClass.sqlexecute (ins_cursor);
	return di;
}

int FRACHT_CLASS::updatefracht (void)
{
	if ( readcursor < 0 ) prepare ();	
    int di = dbClass.sqlexecute (test_upd_cursor);
	return di;
}

int FRACHT_CLASS::openfracht (void)
{
	if ( readcursor < 0 ) prepare ();	
	
    return dbClass.sqlopen (readcursor);
}


void FRACHT_CLASS::prepare (void)
{

	// evtl. muesste auf die Tabelle fracht noch ein index mdn/ls gelegt werden ?! 

// lesen 


	dbClass.sqlin ((long *) &fracht.ls, SQLLONG, 0);
	dbClass.sqlin ((short *) &fracht.mdn, SQLSHORT, 0);

 dbClass.sqlout ((short  *) &fracht.mdn ,SQLSHORT , 0 ) ;
 dbClass.sqlout ((short  *) &fracht.fil ,SQLSHORT , 0 ) ;
 dbClass.sqlout ((long   *) &fracht.kun ,SQLLONG , 0 ) ;
 dbClass.sqlout ((char   *)  fracht.adr_nam1, SQLCHAR, 37) ;
 dbClass.sqlout ((char   *)  fracht.adr_nam2, SQLCHAR, 37) ;

 dbClass.sqlout ((short  *) &fracht.anz ,SQLSHORT , 0 )  ;
 dbClass.sqlout ((char   *)  fracht.str, SQLCHAR, 37) ;
 dbClass.sqlout ((char   *)  fracht.plz, SQLCHAR,  9) ;
 dbClass.sqlout ((char   *)  fracht.ort1, SQLCHAR, 37) ;
 dbClass.sqlout ((TIMESTAMP_STRUCT *) &fracht.lief_term ,SQLTIMESTAMP , 26 )  ;

 dbClass.sqlout ((char   *)  fracht.krz_txt, SQLCHAR, 17) ;
 dbClass.sqlout ((char   *)  fracht.tel, SQLCHAR, 17) ;
 dbClass.sqlout ((char   *)  fracht.wrt, SQLCHAR, 21) ;
 dbClass.sqlout ((TIMESTAMP_STRUCT *) &fracht.dat ,SQLTIMESTAMP , 26 ) ;
 dbClass.sqlout ((char   *)  fracht.frei_txt1, SQLCHAR, 65) ;

 dbClass.sqlout ((char   *)  fracht.frei_txt2, SQLCHAR, 65) ;
 dbClass.sqlout ((char   *)  fracht.frei_txt3, SQLCHAR, 65) ;
 dbClass.sqlout ((char   *)  fracht.frei_txt4, SQLCHAR, 65) ;
 dbClass.sqlout ((char   *)  fracht.frei_txt5, SQLCHAR, 65) ;
 dbClass.sqlout ((char   *)  fracht.frei_txt6, SQLCHAR, 65) ;

 dbClass.sqlout ((long   *) &fracht.ls ,SQLLONG , 0 ) ;
 dbClass.sqlout ((short  *) &fracht.stat ,SQLSHORT , 0 ) ;
 dbClass.sqlout ((double *) &fracht.lief_gew ,SQLDOUBLE , 0 )  ;


	readcursor = dbClass.sqlcursor ("select "
	" mdn ,fil ,kun ,adr_nam1 ,adr_nam2 "
	" ,anz ,str ,plz ,ort1 ,lief_term "
	" ,krz_txt ,tel ,wrt ,dat ,frei_txt1 "
	" ,frei_txt2 ,frei_txt3 ,frei_txt4 ,frei_txt5 ,frei_txt6 "
	" ,ls ,stat ,lief_gew "

	" from fracht where ls = ?  and mdn = ?" ) ;

// Update 


 dbClass.sqlin ((short  *) &fracht.mdn ,SQLSHORT , 0 ) ;
 dbClass.sqlin ((short  *) &fracht.fil ,SQLSHORT , 0 ) ;
 dbClass.sqlin ((long   *) &fracht.kun ,SQLLONG , 0 ) ;
 dbClass.sqlin ((char   *)  fracht.adr_nam1, SQLCHAR, 37) ;
 dbClass.sqlin ((char   *)  fracht.adr_nam2, SQLCHAR, 37) ;

 dbClass.sqlin ((short  *) &fracht.anz ,SQLSHORT , 0 )  ;
 dbClass.sqlin ((char   *)  fracht.str, SQLCHAR, 37) ;
 dbClass.sqlin ((char   *)  fracht.plz, SQLCHAR,  9) ;
 dbClass.sqlin ((char   *)  fracht.ort1, SQLCHAR, 37) ;
 dbClass.sqlin ((TIMESTAMP_STRUCT *) &fracht.lief_term ,SQLTIMESTAMP , 26 )  ;

 dbClass.sqlin ((char   *)  fracht.krz_txt, SQLCHAR, 17) ;
 dbClass.sqlin ((char   *)  fracht.tel, SQLCHAR, 17) ;
 dbClass.sqlin ((char   *)  fracht.wrt, SQLCHAR, 21) ;
 dbClass.sqlin ((TIMESTAMP_STRUCT *) &fracht.dat ,SQLTIMESTAMP , 26 ) ;
 dbClass.sqlin ((char   *)  fracht.frei_txt1, SQLCHAR, 65) ;

 dbClass.sqlin ((char   *)  fracht.frei_txt2, SQLCHAR, 65) ;
 dbClass.sqlin ((char   *)  fracht.frei_txt3, SQLCHAR, 65) ;
 dbClass.sqlin ((char   *)  fracht.frei_txt4, SQLCHAR, 65) ;
 dbClass.sqlin ((char   *)  fracht.frei_txt5, SQLCHAR, 65) ;
 dbClass.sqlin ((char   *)  fracht.frei_txt6, SQLCHAR, 65) ;

 dbClass.sqlin ((long   *) &fracht.ls ,SQLLONG , 0 ) ;
 dbClass.sqlin ((short  *) &fracht.stat ,SQLSHORT , 0 ) ;
 dbClass.sqlin ((double *) &fracht.lief_gew ,SQLDOUBLE , 0 )  ;


 	dbClass.sqlin ((long *) &fracht.ls, SQLLONG, 0);
	dbClass.sqlin ((short *) &fracht.mdn, SQLSHORT, 0);

 test_upd_cursor = dbClass.sqlcursor ("update fracht set "
	" mdn = ? ,fil = ? ,kun = ?  ,adr_nam1 = ? ,adr_nam2 = ? "
	" ,anz = ? ,str = ? ,plz = ? ,ort1 = ?,lief_term = ? "
	" ,krz_txt = ? ,tel = ? ,wrt = ? ,dat = ? ,frei_txt1 = ? "
	" ,frei_txt2 = ? ,frei_txt3 = ? ,frei_txt4 = ?,frei_txt5 = ? ,frei_txt6 = ? "
	" ,ls = ? ,stat = ? , lief_gew = ? "

	" where ls = ?  and mdn = ? " ) ;

// insert 
 dbClass.sqlin ((short  *) &fracht.mdn ,SQLSHORT , 0 ) ;
 dbClass.sqlin ((short  *) &fracht.fil ,SQLSHORT , 0 ) ;
 dbClass.sqlin ((long   *) &fracht.kun ,SQLLONG , 0 ) ;
 dbClass.sqlin ((char   *)  fracht.adr_nam1, SQLCHAR, 37) ;
 dbClass.sqlin ((char   *)  fracht.adr_nam2, SQLCHAR, 37) ;

 dbClass.sqlin ((short  *) &fracht.anz ,SQLSHORT , 0 )  ;
 dbClass.sqlin ((char   *)  fracht.str, SQLCHAR, 37) ;
 dbClass.sqlin ((char   *)  fracht.plz, SQLCHAR,  9) ;
 dbClass.sqlin ((char   *)  fracht.ort1, SQLCHAR, 37) ;
 dbClass.sqlin ((TIMESTAMP_STRUCT *) &fracht.lief_term ,SQLTIMESTAMP , 26 )  ;

 dbClass.sqlin ((char   *)  fracht.krz_txt, SQLCHAR, 17) ;
 dbClass.sqlin ((char   *)  fracht.tel, SQLCHAR, 17) ;
 dbClass.sqlin ((char   *)  fracht.wrt, SQLCHAR, 21) ;
 dbClass.sqlin ((TIMESTAMP_STRUCT *) &fracht.dat ,SQLTIMESTAMP , 26 ) ;
 dbClass.sqlin ((char   *)  fracht.frei_txt1, SQLCHAR, 65) ;

 dbClass.sqlin ((char   *)  fracht.frei_txt2, SQLCHAR, 65) ;
 dbClass.sqlin ((char   *)  fracht.frei_txt3, SQLCHAR, 65) ;
 dbClass.sqlin ((char   *)  fracht.frei_txt4, SQLCHAR, 65) ;
 dbClass.sqlin ((char   *)  fracht.frei_txt5, SQLCHAR, 65) ;
 dbClass.sqlin ((char   *)  fracht.frei_txt6, SQLCHAR, 65) ;

 dbClass.sqlin ((long   *) &fracht.ls ,SQLLONG , 0 ) ;
 dbClass.sqlin ((short  *) &fracht.stat ,SQLSHORT , 0 ) ;
 dbClass.sqlin ((double *) &fracht.lief_gew ,SQLDOUBLE , 0 )  ;


	ins_cursor = dbClass.sqlcursor ("insert into fracht  "
	" ( mdn ,fil ,kun ,adr_nam1 ,adr_nam2 "
	" ,anz ,str ,plz ,ort1 ,lief_term "
	" ,krz_txt ,tel ,wrt ,dat ,frei_txt1 "
	" ,frei_txt2 ,frei_txt3 ,frei_txt4 ,frei_txt5 ,frei_txt6 "
	" ,ls ,stat ,lief_gew ) "
	" values "
	" (?,?,?,?,? "
	" ,?,?,?,?,? "
	" ,?,?,?,?,? "
	" ,?,?,?,?,? "
	" ,?,?,? ) " ) ;

}

