#include "stdafx.h"
#include "dbClass.h"
#include "sys_par.h"

struct SYS_PAR sys_par, sys_par_null;

extern DB_CLASS dbClass;

SYS_PAR_CLASS sys_par_class ;


int spdachsnosmt2;	// 050914

void SYS_PAR_CLASS::prepare (void)
{
      dbClass.sqlin ((char *) sys_par.sys_par_nam, 0, 19);

    dbClass.sqlout ((char *) sys_par.sys_par_nam,SQLCHAR,19);
    dbClass.sqlout ((char *) sys_par.sys_par_wrt,SQLCHAR,2);
    dbClass.sqlout ((char *) sys_par.sys_par_besch,SQLCHAR,33);
    dbClass.sqlout ((long *) &sys_par.zei,SQLLONG,0);
    dbClass.sqlout ((char *) &sys_par.delstatus,SQLSHORT,0);

      cursor = dbClass.sqlcursor ("select sys_par.sys_par_nam,  "
"sys_par.sys_par_wrt,  sys_par.sys_par_besch,  sys_par.zei,  "
"sys_par.delstatus from sys_par "

                            "where sys_par_nam = ?");
}

int SYS_PAR_CLASS::readfirst (void)
{
      if (cursor == -1)
      {
                 prepare ();
      }
      int i = dbClass.sqlopen (cursor);
      if (i) return i;
	  return (dbClass.sqlfetch (cursor));
}

int SYS_PAR_CLASS::read (void)
{
      return (dbClass.sqlfetch (cursor));
}

char * SYS_PAR_CLASS::sys_par_holen ( char * esys_par_nam ) 
{
	sprintf ( sys_par.sys_par_nam ,"%s", esys_par_nam ) ;

	int i = readfirst() ;
	if ( i )
		return "0" ;
	return sys_par.sys_par_wrt ;
}
