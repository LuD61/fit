#ifndef _RECH_DEF
#define _RECH_DEF


struct RECH {

    short delstatus ;
    short fil ;
    long kun ;
    short mdn ;
    long rech_nr ;
    TIMESTAMP_STRUCT ts_rech_dat ;
    char rech_dat[11] ;
    long rech_fuss_txt ;
    long rech_kopf_txt ;
    double rech_sum_bto ;
    double rech_sum_nto ;
    double rech_end_rab ;
    double rech_bez ;
    char rech_stat [2];
    char blg_typ [2];
    char krz_txt [17];
    double skto ;
    short zahl_art ;
    char zahl_dat [11];
    short waehrung ;
    double rech_bto_eur ;
    double rech_nto_eur ;
    double rech_rab_eur ;
    double rech_bez_eur ;
    double rech_bto_fremd ;
    double rech_nto_fremd ;
    double rech_rab_fremd ;
    double rech_bez_fremd ;
    char ueb_kz [2] ;
    long geld_kto ;
    long teil_smt ;
    long abkommen ;
    double rerabproz ;
    double rech_rab ;
    double rech_rab1 ;
    double rech_rab2 ;
    double netto ;
    double netto1 ;
    double netto2 ;
    double mwst1 ;
    double mwst2 ;
    short sam_rech ;
	long rech_vbd ;
	TIMESTAMP_STRUCT ts_rvdat ;
	char rvdat[11] ;

};

extern struct RECH rech, rech_null;
extern class RECH_CLASS rech_class ;

struct RECH_P {
    short waehrung ;
    short mdn ;
    short fil ;
	long rech_nr ;   
	char blg_typ[2] ;
	long ls ;
	char lblg[2] ;
	long lkun ;
	long posi ;
//	char ls_charge[21] ;
	char ls_charge[51] ;	// boese Falle 110308
	char a_bz1[25] ;
	char a_bz2[25] ;
	double a ;
	char a_kun[14] ;
	double lad_pr ;
	double vk_pr ;
	double netto ;
	double bonus_pr ;
	double bonus_wrt ;
	double rab_pr ;
	double rab_wrt ;
	double rech_rab1 ;
	double rech_rabwrt ;
	short mwst ;
	double mwst_wert ;
	TIMESTAMP_STRUCT ts_lieferdat ;
	char lieferdat[11] ;
	char auf_ext[31] ;	// 270313 17->31
	double lief_me ;
	long lsp_txt ;	// 140910 
};
extern struct RECH_P rech_p, rech_p_null;

struct RECH_VBD {

	char krz_txt[17] ;
	short mdn ;
	short fil ;
	long inka_nr ;
	long kun ;
	short teil_smt ;
	long rech_vbd ;
	char blg_typ[2] ;
	long rech_nr ;
	long ls ;
	double rech_sum_nto ;
	double rech_sum_nto1 ;
	double rech_sum_nto2 ;
	double mwst_betr1 ;
	double mwst_betr2 ;
	short waehrung ;
	double rech_nto_eur ;
	double rech_nto1_eur ;
	double rech_nto2_eur ;
	double rech_mwst1_eur ;
	double rech_mwst2_eur ;
	
};
extern struct RECH_VBD rech_vbd, rech_vbd_null;

class RECH_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
			   long longdta ;
               int statusupdate (void);
			   int updvbd(long);
			   int leserech (void);
               int openrech (void);
               RECH_CLASS () : DB_CLASS ()
               {
               }
};

class RECH_P_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leserech_p (void);
               int openrech_p (void);
               RECH_P_CLASS () : DB_CLASS ()
               {
               }
};

class RECH_VBD_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leserech_vbd (void);
               int openrech_vbd (void);
               RECH_VBD_CLASS () : DB_CLASS ()
               {
               }
};


#endif

