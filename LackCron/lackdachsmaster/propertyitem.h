#pragma once
#include "Text.h"

#define CString Text

class CPropertyItem
{
public:
	CString Name;
	CString Value;
	CPropertyItem(void);
	CPropertyItem(CString Name, CString Value);
	~CPropertyItem(void);
};
