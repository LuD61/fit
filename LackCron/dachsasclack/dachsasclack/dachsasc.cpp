// dachsasc.cpp : Definiert das Klassenverhalten f�r die Anwendung.
//

#include "stdafx.h"
#include "dachsasc.h"
#include "dachsascDlg.h"
#include "token.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CdachsascApp

BEGIN_MESSAGE_MAP(CdachsascApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CdachsascApp-Erstellung

CdachsascApp::CdachsascApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance positionieren
}


// Das einzige CdachsascApp-Objekt

CdachsascApp theApp;


// CdachsascApp-Initialisierung

BOOL CdachsascApp::InitInstance()
{
	// InitCommonControlsEx() ist f�r Windows XP erforderlich, wenn ein Anwendungsmanifest
	// die Verwendung von ComCtl32.dll Version 6 oder h�her zum Aktivieren
	// von visuellen Stilen angibt. Ansonsten treten beim Erstellen von Fenstern Fehler auf.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Legen Sie dies fest, um alle allgemeinen Steuerelementklassen einzubeziehen,
	// die Sie in Ihrer Anwendung verwenden m�chten.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// Standardinitialisierung
	// Wenn Sie diese Features nicht verwenden und die Gr��e
	// der ausf�hrbaren Datei verringern m�chten, entfernen Sie
	// die nicht erforderlichen Initialisierungsroutinen.
	// �ndern Sie den Registrierungsschl�ssel, unter dem Ihre Einstellungen gespeichert sind.
	// TODO: �ndern Sie diese Zeichenfolge entsprechend,
	// z.B. zum Namen Ihrer Firma oder Organisation.
	SetRegistryKey(_T("Vom lokalen Anwendungs-Assistenten generierte Anwendungen"));

//  Aufrufreihenfolge : mdn datum Tour
//	Beispiel : 1 12.12.2012 39



	CCommandLineInfo cmdInfo;

    char *px;

	LPSTR CommandLine = GetCommandLine ();

	CToken Token (CommandLine, " ");

	sprintf ( globmdn, "-2" );
	sprintf ( globdatum , "01.01.1990" );
	sprintf ( globtour , "-3" ); 
	globaktion = 0 ;

    px = Token.NextToken ();

	if ((px = Token.NextToken ()) != NULL)
		if ( px[0] != '\0' && px[0] != ' ' )
				sprintf ( globmdn, "%s" , px ) ;

	if ((px = Token.NextToken ()) != NULL)
		if ( px[0] != '\0' && px[0] != ' ' )
				sprintf ( globdatum, "%s" , px ) ;

	if ((px = Token.NextToken ()) != NULL)
		if ( px[0] != '\0' && px[0] != ' ' )
				sprintf ( globtour, "%s" , px ) ;

	if ((atoi(globmdn) > 0 ) && ( atoi ( globtour ) > 0 ))	// eigentlich muesste man auch noch das Datum auf Gueltigkeit pruefen
		globaktion = 1 ;

	CdachsascDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: F�gen Sie hier Code ein, um das Schlie�en des
		//  Dialogfelds �ber "OK" zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: F�gen Sie hier Code ein, um das Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
	}

	// Da das Dialogfeld geschlossen wurde, FALSE zur�ckliefern, sodass wir die
	//  Anwendung verlassen, anstatt das Nachrichtensystem der Anwendung zu starten.
	return FALSE;
}
