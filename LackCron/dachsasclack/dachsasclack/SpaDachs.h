#ifndef _SPADACHS
#define _SPADACHS

// das ist die mit ptabn korrespondierende Definition der Spedition Dachser  
#define DACHSER 1
// das ist die mit ptabn korrespondierende Definition der Spedition Dachser  


int testmode = 0 ;	// 240414

// Struktur Speditionsauftrag  Version 3.2.1 (2010-06-22)
// Vorsatz KVOR_
/*  000 */	char kvor_satza[6] ;	// immer "KVOR_"
/*  005 */	char kvor_gsber[3] ;	// immer "01"
/*  007 */	char kvor_kvkda[9] ;	// Absender-Nummer
/*  015 */	char kvor_dtkda[9] ;	// Dachser-NL
/*  023 */	char kvor_kvkvn[6] ;	// Vorsatznummer

/*  028 */	char kvor_d1kvd[3] ;	// Crea-Datum dd
/*  030 */	char kvor_d2kvd[3] ;	// Crea-Datum mm
/*  032 */	char kvor_d3kvd[3] ;	// Crea-Datum yy
/*  034 */	char kvor_d4kvd[3] ;	// Crea-Datum jhd
/*  036 */	char kvor_z1kvd[3] ;	// Crea-Datum hh
/*  038 */	char kvor_z2kvd[3] ;	// Crea-Datum min

/*  040 */	char kvor_kvtnk[11] ;	// "o" Trapo-Nummer
/*  050 */	char kvor_kvauf[6] ;	// "o" Anzahl auftr.
/*  055 */	char kvor_kvdst[2] ;	// "o" Reservefeld
/*  056 -> crlf */

// Auftragskopfsatz KSTA_

/*  000 */	char ksta_satza[6] ;	// immer "KSTA_"
/*  005 */	char ksta_gsber[3] ;	// immer "01"
/*  007 */	char ksta_kvkda[9] ;	// Absender-Nummer
/*  015 */	char ksta_dtkda[9] ;	// Dachser-NL
/*  023 */	char ksta_kvkvn[6] ;	// Vorsatznummer

/*  028 */	char ksta_sspba[6] ;	// lfd im Vorsatz
/*  033 */	char ksta_sskda[9] ;	// Kunu Absender
/*  041 */	char ksta_ssik1[2] ;	// "o" Auf-gruppe
/*  042 */	char ksta_sssan[2] ;	// "o" Reserve-Feld
/*  043 */	char ksta_ssskd[18] ;	// Sendungsid
/*  060 */	char ksta_ssn1e[31] ;	// Name1
/*  090 */	char ksta_ssn2e[31] ;	// "o" Name2
/*  120 */	char ksta_ssn3e[31] ;	// "o" Name3
/*  150 */	char ksta_sssre[31] ;	// Str+Hausnummer
/*  180 */	char ksta_sslde[4] ;	// Nation
/*  183 */	char ksta_ssple[7] ;	// PLZ
/*  189 */	char ksta_ssore[27] ;	// Ort
/*  215 */	char ksta_sszuh[31] ;	// "o" Adresserg.

/*  245 */	char ksta_ssanr[4] ;	// "o" Unterrel.
/*  248 */	char ksta_ssrev[5] ;	// "m"/"o" Verladerel.
/*  252 */	char ksta_ssldb[4] ;	// "o" Postcode
/*  255 */	char ksta_ssplb[7] ;	// "o" PLZ zu ssldb
/*  261 */	char ksta_ssorb[27] ;	// "o" Ort
/*  287 */	char ksta_sslag[4] ;	// "o" Ausland-Gr.
/*  290 */	char ksta_sspag[7] ;	// "o" Ausland-pz-Gr.
/*  296 */	char ksta_ssoag[27] ;	// "o" Ausland-ort-Gr.
/*  322 */	char ksta_sstdo[2] ;	// Reserve blank
/*  323 */	char ksta_sszgu[2] ;	// Zollgut J/N 
/*  324 */	char ksta_sssah[2] ;	// Selbstabholer == "N"
/*  325 */	char ksta_ssheb[2] ;	// Hebebuehne == "N"
/*  326 */	char ksta_sstr1[2] ;	// Sparte == "F"

/*  327 */	char ksta_sstr2[2] ;	// 
/*  328 */	char ksta_sstr3[2] ;	// 
/*  329 */	char ksta_ssfkt[4] ;	// Frankatur
/*  332 */	char ksta_sssvv[2] ;	// Reservefeld
/*  333 */	char ksta_sswwe[10] ;	// "m"/"o" Warenwert
/*  342 */	char ksta_sswhs[4] ;	// Waehrung
/*  345 */	char ksta_ssfwb[14] ;	// Reserve
/*  358 */	char ksta_ssiws[10] ;	// Reserve
/*  367 */	char ksta_smreb[10] ;	// Nachnahme
/*  376 */	char ksta_smwhs[4] ;	// Waehrung
/*  379 */	char ksta_smfwb[14] ;	// Reserve
/*  392 */	char ksta_sstvk[2] ;	// Reserve

/*  393 */	char ksta_d1ssf[3] ;	// Fixtermin dd
/*  395 */	char ksta_d2ssf[3] ;	// Fixtermin mm
/*  397 */	char ksta_d3ssf[3] ;	// Fixtermin yy
/*  399 */	char ksta_d4ssf[3] ;	// Fixtermin jhd

/*  401 */	char ksta_d1ssv[3] ;	// Zustellv dd
/*  403 */	char ksta_d2ssv[3] ;	// Zustellv mm
/*  405 */	char ksta_d3ssv[3] ;	// Zustellv yy
/*  407 */	char ksta_d4ssv[3] ;	// Zustellv jhd
/*  409 */	char ksta_z1ssv[3] ;	// Zustellv hh
/*  411 */	char ksta_z2ssv[3] ;	// Zustellv min
/*  413 */	char ksta_d1ssb[3] ;	// Zustellb dd
/*  415 */	char ksta_d2ssb[3] ;	// Zustellb mm
/*  417 */	char ksta_d3ssb[3] ;	// Zustellb yy
/*  419 */	char ksta_d4ssb[3] ;	// Zustellb jhd
/*  421 */	char ksta_z1ssb[3] ;	// Zustellb hh
/*  423 */	char ksta_z2ssb[3] ;	// Zustellb min
/*  425 */	char ksta_swwco[3] ;	// Nachnahme-Code
/*  427 */	char ksta_ssiln[14] ;	// Empf-ILN
/*  440 */	char ksta_ssaua[3] ;	// Reserve blank
/*  442 -> crlf */
// Auftragszeile KZEI_

/*  000 */	char kzei_satza[6] ;	// immer "KZEI_"
/*  005 */	char kzei_gsber[3] ;	// immer "01"
/*  007 */	char kzei_kvkda[9] ;	// Absender-Nummer
/*  015 */	char kzei_dtkda[9] ;	// Dachser-NL
/*  023 */	char kzei_kvkvn[6] ;	// Vorsatznummer
/*  028 */	char kzei_sspba[6] ;	// lfd im Vorsatz

/*  033 */	char kzei_szazl[4] ;	// lfd. sendepos
/*  036 */	char kzei_szaps[6] ;	// Anz. Verp.
/*  041 */	char kzei_szvpa[4] ;	// Verp.-Art
/*  044 */	char kzei_szinh[21] ;	// Beschr. Packst�ck
/*  064 */	char kzei_szgew[6] ;	// bruttogew. 
/*  069 */	char kzei_sznet[6] ;	// nettogew. 
/*  074 */	char kzei_szcbm[8] ;	// Kubmeter. 
/*  081 */	char kzei_szldm[4] ;	// Lademeter.
/*  084 */	char kzei_szlkl[8] ;	// Reserve blank
/*  091 */	char kzei_szgua[5] ;	// Guterart
/*  095 */	char kzei_szame[6] ;	// Anz. ME WG
/*  100 */	char kzei_szwgp[3] ;	// WG
/*  102 */	char kzei_szkul[2] ;	// Res blank
/*  103 */	char kzei_sztpv[4] ;	// Res blank
/*  106 */	char kzei_sztpb[4] ;	// Res blank
/*  109 */	char kzei_szzei[18] ;	// Markierung
/*  126 */	char kzei_szlng[6] ;	// laenge in Meter
/*  131 */	char kzei_szbre[6] ;	// breite in Meter
/*  136 */	char kzei_szhoh[6] ;	// hoehe in Meter
/*  141 */	char kzei_szpos[4] ;	// Packmittelpos.
/*  144 */	char kzei_szwnr[10] ;	// Zoll-W-Nr.
/*  153 -> crlf */


// Gefahrgutzeile ( KGEF_)
// kommt spaeter, da nur fakultativ

// Packstueck-Ident ( KPPA_ )

/*  000 */	char kppa_satza[6] ;	// immer "KPPA_"
/*  005 */	char kppa_gsber[3] ;	// immer "01"
/*  007 */	char kppa_kvkda[9] ;	// Absender-Nummer
/*  015 */	char kppa_dtkda[9] ;	// Dachser-NL
/*  023 */	char kppa_kvkvn[6] ;	// Vorsatznummer

/*  028 */	char kppa_ssbpa[6] ;	// lfd-Nr
/*  033 */	char kppa_kplfd[6] ;	// lfd-Nr Packst-ID
/*  038 */	char kppa_kppik[36] ;	// Packst-ID(NVE) 20 stellen ( 18 + 2 fuehr-Nullen)
/*  073 */	char kppa_kpcak[4] ;	// Codeart
/*  076 */	char kppa_kpzei[4] ;	// auf-zeile
/*  079 -> crlf */

// Zusatztextzeile ( KUTX_)		// 27.08.2013

/*  000 */	char kutx_satza[6] ;	// immer "KUTX_"
/*  005 */	char kutx_gsber[3] ;	// immer "01"
/*  007 */	char kutx_kvkda[9] ;	// Absender-Nummer
/*  015 */	char kutx_dtkda[9] ;	// Dachser-NL
/*  023 */	char kutx_kvkvn[6] ;	// Vorsatznummer aus kfor
/*  028 */	char kutx_sspba[6] ;	// lfd im Vorsatz aus ksta

/*  033 */	char kutx_hutnr[4] ;	// lfd.Nr Textz. je Sendung
/*  036 */	char kutx_hutxa[3] ;	// Textart -> "SI"
/*  038 */	char kutx_hutxt[61] ;	// Der Text
/*  098 -> crlf */

// Zusatzadressen ( KTVA_) kommt sp�ter ( oder nie )

// Packmittelzeile ( KPAP_)

/*  000 */	char kpap_satza[6] ;	// immer "KPAP_"
/*  005 */	char kpap_gsber[3] ;	// immer "01"
/*  007 */	char kpap_kvkda[9] ;	// Absender-Nummer
/*  015 */	char kpap_dtkda[9] ;	// Dachser-NL
/*  023 */	char kpap_kvkvn[6] ;	// Vorsatznummer

/*  028 */	char kpap_ssbpa[6] ;	// lfd-Nr
/*  033 */	char kpap_sppos[4] ;	// lfd-Nr Packmittel
/*  036 */	char kpap_spanp[6] ;	// Anz. Packmittel
/*  041 */	char kpap_spvpp[4] ;	// Packmittel-Art
/*  044 -> crlf */


// Referenz-Satz ( KREF_) kommt sp�ter
// 270313 : KREF : heute ist "sp�ter"

/*  000 */	char kref_satza[6] ;	// immer "KREF_"
/*  005 */	char kref_gsber[3] ;	// immer "01"
/*  007 */	char kref_kvkda[9] ;	// Absender-Nummer
/*  015 */	char kref_dtkda[9] ;	// Dachser-NL
/*  023 */	char kref_kvkvn[6] ;	// Vorsatznummer

/*  028 */	char kref_ssbpa[6] ;	// lfd-Nr

/*  033 */	char kref_krras[4] ;	// Referenztyp (immer "003")
/*  036 */	char kref_krpro[4] ;	// Partnerrolle ( immer "   ")
/*  039 */	char kref_krref[36] ;	// Referenz ( z.B, Lieferschein oder Bestellnummer - Lack-Auftrag !? )
/*  074 */	char kref_krerf[9] ;	// Reservefeld "00000000"
/*  082 */	char kref_krgen[9] ;	// Reservefeld "00000000"
/*  090 */	char kref_d8ref[9] ;	// Reservefeld "00000000"
/*  098 */	char kref_z4ref[5] ;	// Reservefeld "0000"
/*  102 */	char kref_krobj[4] ;	// Reservefeld "   "

/*  105 -> crlf */




// 270313 : KREF : heute ist "sp�ter"

// Dateiende ( K999_)

/*  000 */	char k999_satza[6] ;	// immer "K999_"
/*  005 */	char k999_daawi[7] ;	// immer "601113"
/*  011 */	char k999_d1dav[3] ;	// Erstell-Dat dd
/*  013 */	char k999_d2dav[3] ;	// Erstell-Dat mm
/*  015 */	char k999_d3dav[3] ;	// Erstell-Dat yy
/*  017 */	char k999_d4dav[3] ;	// Erstell-Dat jhd
/*  019 */	char k999_z1dav[3] ;	// Erstell-Dat hh
/*  021 */	char k999_z2dav[3] ;	// Erstell-Dat min
/*  023 */	char k999_dastp[3] ;	// immer "V "
/*  025 */	char k999_dakew[11] ;	// immer "3.1      "
/*  35 -> crlf */


static int anwender ;	// ich lade hier erst mal fest den Dietz vor ..., spaeter evtl paramtrierbar ...
#define ANWDIETZ 1 
#define ANWBOESI 2
#define ANWLACK  3

#define NACHNAHMEBAR 2
#define NACHNAHME 5

static long vorsatznummer ;	// ( d.h unendlich laufende Nummer )

static char abkda[9] ;	// Kundennumer des Auftraggebers bei Dachser ( d.h. Dietz usw. )
static char dtkda[9] ;	// beauftragte Dachser-Niederlassung
static int  satzzahl ;	// satzzahl(=Auftrag ) je Vorsatz


int poskpap ;	// lfd. in kpap (NVE) je Auftrag	// aus  traponve
int posinzei ;	//	lfd. Zei je Auftrag	// synchron aus trapopspm
int poskppa ;	// lfd. kppa je Auftrag	// synchron aus trapopspm

#endif
