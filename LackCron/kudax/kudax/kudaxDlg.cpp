// kudaxDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "kudax.h"
#include "kudaxDlg.h"
#include "KunWahl.h"
#include "DbClass.h"
#include "mdn.h"
#include "adr.h"
#include "kun.h"
#include "ptabn.h"
#include "zudax.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static long savkun ;	// setfocus kommt meistens for killfocus ;-)

static char bufh[516] ;	// allgemeine Hilfsvariable
static CString bufx ;	// allgemeine Hilfsvariable 

DB_CLASS dbClass ;	// Hier ist die Heimat der dbClass, alle Tabellen sind in ihrem jeweiligen Modul daheim



DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CkudaxDlg-Dialogfeld




CkudaxDlg::CkudaxDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CkudaxDlg::IDD, pParent)
	, v_kundnr(0)
	, v_mdnnr(0)
	, v_combospedit(_T(""))
	, v_kundname(_T(""))
	, v_combologistik(_T(""))
	, v_comboabfert(_T(""))
	, v_combofranka(_T(""))
	, v_combofrachtg(_T(""))
	, v_mdnname(_T(""))
	, v_speich(_T(""))
	, v_comboaufgruppe(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CkudaxDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_KUNDNR, m_kundnr);
	DDX_Text(pDX, IDC_KUNDNR, v_kundnr);
	DDV_MinMaxLong(pDX, v_kundnr, 0, 99999999);
	DDX_Control(pDX, IDC_BUTTON1, m_button1);
	DDX_Control(pDX, IDC_MDNNR, m_mdnnr);
	DDX_Text(pDX, IDC_MDNNR, v_mdnnr);
	DDX_Control(pDX, IDC_COMBOSPEDIT, m_combospedit);
	DDX_CBString(pDX, IDC_COMBOSPEDIT, v_combospedit);
	DDX_Control(pDX, IDC_KUNDNAME, m_kundname);
	DDX_Text(pDX, IDC_KUNDNAME, v_kundname);
	DDX_Control(pDX, IDC_COMBOLOGISTIK, m_combologistik);
	DDX_CBString(pDX, IDC_COMBOLOGISTIK, v_combologistik);
	DDX_Control(pDX, IDC_COMBOABFERT, m_comboabfert);
	DDX_CBString(pDX, IDC_COMBOABFERT, v_comboabfert);
	DDX_Control(pDX, IDC_COMBOFRANKA, m_combofranka);
	DDX_CBString(pDX, IDC_COMBOFRANKA, v_combofranka);
	DDX_Control(pDX, IDC_COMBOFRACHTG, m_combofrachtg);
	DDX_CBString(pDX, IDC_COMBOFRACHTG, v_combofrachtg);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_SPEICH, m_speich);
	DDX_Text(pDX, IDC_SPEICH, v_speich);
	DDX_Control(pDX, IDC_COMBOAUFGRUPPE, m_comboaufgruppe);
	DDX_CBString(pDX, IDC_COMBOAUFGRUPPE, v_comboaufgruppe);
}

BEGIN_MESSAGE_MAP(CkudaxDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CkudaxDlg::OnBnClickedButton1)
	ON_EN_KILLFOCUS(IDC_MDNNR, &CkudaxDlg::OnEnKillfocusMdnnr)
	ON_CBN_KILLFOCUS(IDC_COMBOSPEDIT, &CkudaxDlg::OnCbnKillfocusCombospedit)
	ON_EN_KILLFOCUS(IDC_KUNDNR, &CkudaxDlg::OnEnKillfocusKundnr)
	ON_CBN_KILLFOCUS(IDC_COMBOLOGISTIK, &CkudaxDlg::OnCbnKillfocusCombologistik)
	ON_CBN_KILLFOCUS(IDC_COMBOABFERT, &CkudaxDlg::OnCbnKillfocusComboabfert)
	ON_CBN_KILLFOCUS(IDC_COMBOFRANKA, &CkudaxDlg::OnCbnKillfocusCombofranka)
	ON_CBN_KILLFOCUS(IDC_COMBOFRACHTG, &CkudaxDlg::OnCbnKillfocusCombofrachtg)
	ON_BN_CLICKED(IDOK, &CkudaxDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CkudaxDlg::OnBnClickedCancel)
	ON_CBN_SETFOCUS(IDC_COMBOSPEDIT, &CkudaxDlg::OnCbnSetfocusCombospedit)
	ON_EN_SETFOCUS(IDC_KUNDNR, &CkudaxDlg::OnEnSetfocusKundnr)
	ON_CBN_SETFOCUS(IDC_COMBOLOGISTIK, &CkudaxDlg::OnCbnSetfocusCombologistik)
	ON_CBN_SETFOCUS(IDC_COMBOABFERT, &CkudaxDlg::OnCbnSetfocusComboabfert)
	ON_CBN_SETFOCUS(IDC_COMBOFRANKA, &CkudaxDlg::OnCbnSetfocusCombofranka)
	ON_CBN_SETFOCUS(IDC_COMBOFRACHTG, &CkudaxDlg::OnCbnSetfocusCombofrachtg)
	ON_BN_CLICKED(IDC_BUTTDEL, &CkudaxDlg::OnBnClickedButtdel)
	ON_BN_CLICKED(IDC_BUTTDRUCK, &CkudaxDlg::OnBnClickedButtdruck)
	ON_CBN_KILLFOCUS(IDC_COMBOAUFGRUPPE, &CkudaxDlg::OnCbnKillfocusComboaufgruppe)
	ON_CBN_SETFOCUS(IDC_COMBOAUFGRUPPE, &CkudaxDlg::OnCbnSetfocusComboaufgruppe)
END_MESSAGE_MAP()


// CkudaxDlg-Meldungshandler

BOOL CkudaxDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	dbClass.opendbase (_T("bws"));



	mdn.mdn = 1 ;
	v_mdnnr = 1 ;

 	ReadMdn ();

	v_speich.Format ( _T("               "));

	ladecombobox( "sped" , IDC_COMBOSPEDIT ) ;
	ladecombobox( "be_logd" , IDC_COMBOLOGISTIK ) ;
	ladecombobox( "abfert" , IDC_COMBOABFERT ) ;
	ladecombobox( "dfrank" , IDC_COMBOFRANKA ) ;
	ladecombobox( "lief_art" , IDC_COMBOFRACHTG ) ;
	ladecombobox( "aufgruppe" , IDC_COMBOAUFGRUPPE ) ;	// 220311
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

// Laden der Combo-Box 

void CkudaxDlg::ladecombobox ( char * ptname , int IDX )
{

	CString szptabn ;
	szptabn.Format("         " ) ;	// Leerstring ( d.h. KEIN DEFAULT )
	((CComboBox *)GetDlgItem(IDX))->AddString(szptabn.GetBuffer(0));
	((CComboBox *)GetDlgItem(IDX))->SetCurSel(0);

	int sqlstat = ptabn_class.openallptabn (ptname);
	sqlstat = ptabn_class.leseallptabn();
	while(!sqlstat)
	{
		szptabn.Format("%s  %s",ptabn.ptwert, ptabn.ptbez);
		// hier haben wir jetzt die Namen und k�nnen sie in die ComboBox einf�gen

			((CComboBox *)GetDlgItem(IDX))->AddString(szptabn.GetBuffer(0));
			((CComboBox *)GetDlgItem(IDX))->SetCurSel(0);
	
		sqlstat = ptabn_class.leseallptabn () ;
	}

}


void CkudaxDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CkudaxDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CkudaxDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CkudaxDlg::OnBnClickedButton1()
{

	savkun = kun.kun ;

	CKunWahl DialKun ;
	DialKun.DoModal() ;

	v_speich.Format ( _T("               "));

	if ( kun.kun > -1 )	// Kunde
	{
		v_kundnr = kun.kun ;
		if ( kun.kun == 0 )
			sprintf ( kun.kun_krz1,"Defaultkunde") ;

		v_kundname.Format ("%s", kun.kun_krz1 );
	}
	else	// Fehlermeldung
	{
		v_kundname.Format ("%s", "                     " );
	}
	if ( savkun != kun.kun )
		Read() ;
	UpdateData(FALSE) ;
}

bool CkudaxDlg::ReadMdn (void) 
{
	bool retcode = FALSE ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn()&& mdn.mdn > 0 )
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
			retcode = TRUE ;
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			retcode = FALSE ;
// hier nicht notwendig :	PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		retcode = FALSE ;
// hier nicht notwendig : 		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;
	return retcode ;
}


void CkudaxDlg::Write (void)
{
	if ( (mdn.mdn > 0 ) && (zudax.sped > 0 ) && ( kun.kun > -1 ))
	{	//Nur, falls gueltiger schl�ssel aktiv ist... 

		zudax.mdn = mdn.mdn ;
		zudax.kun = kun.kun ;
		// zudax.sped steht schon drin 

// -------------
		sprintf(bufh,"%s",v_combologistik.GetBuffer(2)) ;
		bufh[2] = '\0' ;
		if ( bufh[1] == ' ' )
				bufh[1] = '\0' ;
		sprintf ( zudax.be_logd ,"%s", bufh ) ;
// -------------
		sprintf(bufh,"%s",v_comboabfert.GetBuffer(1)) ;
		bufh[1] = '\0' ;
		sprintf ( zudax.abfert , "%s" , bufh );
// -------------
		sprintf(bufh,"%s",v_combofrachtg.GetBuffer(0)) ;
		zudax.lief_art = (short) atoi ( bufh ) ;
// -------------
		sprintf(bufh,"%s",v_combofranka.GetBuffer(3)) ;
		bufh[3] = '\0' ;
		sprintf ( zudax.dfrank ,"%s" ,bufh );

// ------------- 220311
		sprintf(bufh,"%s",v_comboaufgruppe.GetBuffer(3)) ;
		bufh[3] = '\0' ;
		sprintf ( zudax.aufgruppe ,"%s" ,bufh );

		int dret = zudax_class.writezudax() ;

		v_speich.Format ( _T("Satz wurde gespeichert"));
		UpdateData(FALSE) ;
	}
}
void CkudaxDlg::Delete (void)
{

	v_speich.Format( "              " ) ;

	if ( (mdn.mdn > 0 ) && (zudax.sped > 0 ) && ( kun.kun > -1 ))
	{	//Nur, falls gueltiger schl�ssel aktiv ist... 
		if ( MessageBox("Wirklich l�schen ?? ", " ", MB_YESNO ) == IDYES )
		{

			zudax.mdn = mdn.mdn ;
			zudax.kun = kun.kun ;
			// zudax.sped steht schon drin 
			int dret = zudax_class.deletezudax () ;
			v_speich.Format ( _T("Satz wurde gel�schtt"));
			// Display : Satz steht noch in der Anzeige
		}
	}
	UpdateData(FALSE) ;
}


void CkudaxDlg::Read (void) 
{	// Lese gewuenschten Satz 

	v_speich.Format( "              " ) ;

	if ( (mdn.mdn > 0 ) && (zudax.sped > 0 ) && ( kun.kun > -1 ))
	{	//Nur, falls gueltiger schl�ssel aktiv ist... 
		zudax.mdn = mdn.mdn ;
		zudax.kun = kun.kun ;
		// zudax.sped steht schon drin 
		int dret = zudax_class.openzudax () ;
		dret = zudax_class.lesezudax () ;
		if ( dret )
		{
			v_speich.Format( "Neuanlage" ) ;
			if ( kun.kun > 0 )
			{
				zudax.kun = 0 ;
				dret = zudax_class.openzudax () ;
				dret = zudax_class.lesezudax () ;
				zudax.kun = kun.kun ;
			}
		}
//		if ( dret ) sprintf ( zudax.be_logd , "05");	// allgemein - boesinger
		if ( dret ) sprintf ( zudax.be_logd , "02");	// 220311 : Rest - Dietz
		if ( dret ) sprintf ( zudax.aufgruppe , "02");	// 220311 : Rest - Dietz
		if ( dret ) zudax.lief_art = 0 ;
		if ( dret ) sprintf ( zudax.abfert ,  "N" );
//		if ( dret ) sprintf ( zudax.dfrank, "999" );	// allgemein - boesinger
		if ( dret ) sprintf ( zudax.dfrank, "031" );	// frei haus - Dietz


		int nCurSel ;

// Beschaffungslogistik(WG)( ALDI, NORMA,METRO ... )
		sprintf ( bufh, "%s", zudax.be_logd );
		nCurSel = -1 ;
		nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOLOGISTIK))->FindString(nCurSel, bufh);

		if (nCurSel != CB_ERR)
		{
			((CComboBox *)GetDlgItem(IDC_COMBOLOGISTIK))->SetCurSel(nCurSel) ;
			((CComboBox *)GetDlgItem(IDC_COMBOLOGISTIK))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			v_combologistik = bufx ;
		}
		else	// notbremse
		{
			v_combologistik.Format( bufh) ;
		}


// 220311 Auftragsgruppe ( 01 : EDEKA , 02 = Rest )

		sprintf ( bufh, "%s", zudax.aufgruppe );
		nCurSel = -1 ;
		nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOAUFGRUPPE))->FindString(nCurSel, bufh);
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)GetDlgItem(IDC_COMBOAUFGRUPPE))->SetCurSel(nCurSel) ;
			((CComboBox *)GetDlgItem(IDC_COMBOAUFGRUPPE))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			v_comboaufgruppe = bufx ;
		}
		else
		{
			v_comboaufgruppe.Format(bufh )   ;
		}
		
// lief_art ( Frachtgewichts-Berechnung )
		sprintf ( bufh, "%01d", zudax.lief_art );
		nCurSel = -1 ;
		nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOFRACHTG))->FindString(nCurSel, bufh);
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)GetDlgItem(IDC_COMBOFRACHTG))->SetCurSel(nCurSel) ;
			((CComboBox *)GetDlgItem(IDC_COMBOFRACHTG))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			v_combofrachtg = bufx ;
		}
		else	// notbremse
		{
			v_combofrachtg.Format (bufh) ;
		}

// Abfertigung ( normal, Fixtermin ......
		sprintf ( bufh, "%s", zudax.abfert );
		nCurSel = -1 ;
		nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOABFERT))->FindString(nCurSel, bufh);
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)GetDlgItem(IDC_COMBOABFERT))->SetCurSel(nCurSel) ;
			((CComboBox *)GetDlgItem(IDC_COMBOABFERT))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			v_comboabfert = bufx ;
		}
		else
		{
			v_comboabfert.Format(bufh )   ;
		}
// Frankatur ( unfrei, nachnahme usw. )

		sprintf ( bufh, "%s", zudax.dfrank );
		nCurSel = -1 ;
		nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOFRANKA))->FindString(nCurSel, bufh);
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)GetDlgItem(IDC_COMBOFRANKA))->SetCurSel(nCurSel) ;
			((CComboBox *)GetDlgItem(IDC_COMBOFRANKA))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			v_combofranka = bufx ;
		}
		else
		{
			v_combofranka.Format(bufh )   ;
		}
	}

	UpdateData (FALSE) ;
}



void CkudaxDlg::OnEnKillfocusMdnnr()
{

	short savmdn = mdn.mdn ;
	UpdateData (TRUE) ;

	v_speich.Format ( _T("               "));

	int i = m_mdnnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn() && mdn.mdn > 0 )
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		PrevDlgCtrl();
	}
	if ( savmdn != mdn.mdn )
		Read() ;
	UpdateData (FALSE) ;
}
BOOL CkudaxDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				CWnd *xWnd ;
				xWnd = GetFocus ();
			           if(xWnd == GetDlgItem (IDOK)) break ;			// ok - eigentlich nichts machen
// auf den ok-Knopf verlegt ....			           if(xWnd == GetDlgItem (IDC_AUSDRUCK)) break ;	// Ausduck halt
			           if(xWnd == GetDlgItem (IDCANCEL))
					   {
							GetParent ()->DestroyWindow ();
							return TRUE ;	// Abbruch halt ( genau wie ButtonClickedCancel) 
					   }
	
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (OnKeyup ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				OnCancel () ;	// 220311 
//				GetParent ()->DestroyWindow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write () ;
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				Delete () ;
				return TRUE;
			}
	}

	return CDialog::PreTranslateMessage(pMsg); 
}

BOOL CkudaxDlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);

	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CkudaxDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}


void CkudaxDlg::OnCbnKillfocusCombospedit()
{

	short savsped = zudax.sped ;

	UpdateData(TRUE) ;

	v_speich.Format ( _T("               "));

	sprintf ( bufh, "%s", v_combospedit );
	int nCurSel = -1 ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOSPEDIT))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOSPEDIT))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOSPEDIT))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_combospedit = bufx ;

		zudax.sped = (short) atoi ( bufh );

		UpdateData(FALSE) ;
	}
	else
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//		sprintf ( kunbran2 ,"-99" )	;	// unm�glicher wert, kun_bran2 ist nur 2 zeichen gross ....

		PrevDlgCtrl();
	}

	if ( savsped != zudax.sped )
		Read() ;

	UpdateData(FALSE) ;
}

void CkudaxDlg::OnEnKillfocusKundnr()
{
	savkun = kun.kun ;

	UpdateData (TRUE) ;

	v_speich.Format ( _T("               "));

	kun.kun = v_kundnr ;
	kun.mdn = mdn.mdn ;
	if ( kun.kun == 0 )
	{
		v_kundnr = kun.kun ;
		v_kundname.Format ( "Defaultkunde" ) ;
		sprintf ( kun.kun_krz1 ,"Defaultkunde" );
	}
	else	// kun.kun < 0 wird in der Eingabe abgewiesen 
	{

		int i = kun_class.openkun();
		i = kun_class.lesekun();
		if ( i )
		{
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			PrevDlgCtrl();
		}
		else
		{
			v_kundnr = kun.kun ;
			v_kundname.Format ("%s", kun.kun_krz1 );
		}
	}

	if ( savkun != kun.kun )
		Read() ;

	UpdateData(FALSE) ;

}

void CkudaxDlg::OnCbnKillfocusCombologistik()
{
	UpdateData(TRUE) ;

	v_speich.Format ( _T("               "));

	sprintf ( bufh, "%s", v_combologistik );
	int nCurSel = -1 ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOLOGISTIK))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOLOGISTIK))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOLOGISTIK))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_combologistik = bufx ;
		UpdateData(FALSE) ;
	}
	else
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//		sprintf ( kunbran2 ,"-99" )	;	// unm�glicher wert, kun_bran2 ist nur 2 zeichen gross ....
		PrevDlgCtrl();
	}
	UpdateData(FALSE) ;

}
// 220311
void CkudaxDlg::OnCbnKillfocusComboaufgruppe()
{
	UpdateData(TRUE) ;

	v_speich.Format ( _T("               "));

	sprintf ( bufh, "%s", v_comboaufgruppe );
	int nCurSel = -1 ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOAUFGRUPPE))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOAUFGRUPPE))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOAUFGRUPPE))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_comboaufgruppe = bufx ;
		UpdateData(FALSE) ;
	}
	else
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//		sprintf ( kunbran2 ,"-99" )	;	// unm�glicher wert, kun_bran2 ist nur 2 zeichen gross ....

		PrevDlgCtrl();
	}
	UpdateData(FALSE) ;
}


void CkudaxDlg::OnCbnKillfocusComboabfert()
{
	UpdateData(TRUE) ;

	v_speich.Format ( _T("               "));

	sprintf ( bufh, "%s", v_comboabfert );
	int nCurSel = -1 ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOABFERT))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOABFERT))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOABFERT))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_comboabfert = bufx ;
		UpdateData(FALSE) ;
	}
	else
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//		sprintf ( kunbran2 ,"-99" )	;	// unm�glicher wert, kun_bran2 ist nur 2 zeichen gross ....

		PrevDlgCtrl();
	}
	UpdateData(FALSE) ;
}

void CkudaxDlg::OnCbnKillfocusCombofranka()
{
	UpdateData(TRUE) ;

	v_speich.Format ( _T("               "));

	sprintf ( bufh, "%s", v_combofranka );
	int nCurSel = -1 ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOFRANKA))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOFRANKA))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOFRANKA))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_combofranka = bufx ;
		UpdateData(FALSE) ;
	}
	else
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//		sprintf ( kunbran2 ,"-99" )	;	// unm�glicher wert, kun_bran2 ist nur 2 zeichen gross ....

		PrevDlgCtrl();
	}
	UpdateData(FALSE) ;
}

void CkudaxDlg::OnCbnKillfocusCombofrachtg()
{
	UpdateData(TRUE) ;

	v_speich.Format ( _T("               "));

	sprintf ( bufh, "%s", v_combofrachtg );
	int nCurSel = -1 ;
	nCurSel = ((CComboBox *)GetDlgItem(IDC_COMBOFRACHTG))->FindString(nCurSel, bufh);

	if (nCurSel != CB_ERR)
	{
		((CComboBox *)GetDlgItem(IDC_COMBOFRACHTG))->SetCurSel(nCurSel) ;
		((CComboBox *)GetDlgItem(IDC_COMBOFRACHTG))->GetLBText(nCurSel, bufx);
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		v_combofrachtg = bufx ;
		UpdateData(FALSE) ;
	}
	else
	{
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//		sprintf ( kunbran2 ,"-99" )	;	// unm�glicher wert, kun_bran2 ist nur 2 zeichen gross ....

		PrevDlgCtrl();
	}
	UpdateData(FALSE) ;
}

void CkudaxDlg::OnBnClickedOk()
{

	Write () ;

	UpdateData(FALSE) ;

//	OnOK();

}

void CkudaxDlg::OnBnClickedCancel()
{
	OnCancel();
}

void CkudaxDlg::OnCbnSetfocusCombospedit()
{
	v_speich.Format ( _T("               "));
	UpdateData(FALSE) ;
}

void CkudaxDlg::OnEnSetfocusKundnr()
{
	savkun = kun.kun ;
	v_speich.Format ( _T("               "));

	UpdateData(FALSE);
}

void CkudaxDlg::OnCbnSetfocusCombologistik()
{
	v_speich.Format ( _T("               "));
	UpdateData(FALSE) ;
}

void CkudaxDlg::OnCbnSetfocusComboaufgruppe()
{
	v_speich.Format ( _T("               "));
	UpdateData(FALSE) ;
}

void CkudaxDlg::OnCbnSetfocusComboabfert()
{
	v_speich.Format ( _T("               "));
	UpdateData(FALSE) ;
}

void CkudaxDlg::OnCbnSetfocusCombofranka()
{
	v_speich.Format ( _T("               "));
	UpdateData(FALSE) ;
}

void CkudaxDlg::OnCbnSetfocusCombofrachtg()
{
	v_speich.Format ( _T("               "));
	UpdateData(FALSE) ;
}


void CkudaxDlg::OnBnClickedButtdel()
{
	Delete () ;

	UpdateData(FALSE) ;
}

void CkudaxDlg::OnBnClickedButtdruck()
{
	char s1[200] ;
	sprintf ( s1, "dr70001.exe -name KUDAX" );
	int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );

}


