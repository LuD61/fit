#pragma once
#include "afxwin.h"


// CKunWahl-Dialogfeld

class CKunWahl : public CDialog
{
	DECLARE_DYNAMIC(CKunWahl)

public:
	CKunWahl(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CKunWahl();

// Dialogfelddaten
	enum { IDD = IDD_KUNWAHL };

protected:

	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_kunwahl;
	CString v_kunwahl;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
