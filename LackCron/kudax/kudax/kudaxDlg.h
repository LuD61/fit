// kudaxDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CkudaxDlg-Dialogfeld
class CkudaxDlg : public CDialog
{
// Konstruktion
public:
	CkudaxDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_KUDAX_DIALOG };

	protected:

	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	bool CkudaxDlg::ReadMdn (void) ;
	void CkudaxDlg::Read(void);	  	
	void CkudaxDlg::Write(void);
	void CkudaxDlg::Delete(void);
	void CkudaxDlg::ladecombobox ( char * ptname , int IDX );

	afx_msg void OnBnClickedButton1();
	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);


	CEdit m_kundnr;
	long v_kundnr;
	CButton m_button1;
	CEdit m_mdnnr;
	long v_mdnnr;
	CComboBox m_combospedit;
	CString v_combospedit;
	CEdit m_kundname;
	CString v_kundname;
	CComboBox m_combologistik;
	CString v_combologistik;
	CComboBox m_comboabfert;
	CString v_comboabfert;
	CComboBox m_combofranka;
	CString v_combofranka;
	CComboBox m_combofrachtg;
	CString v_combofrachtg;
	afx_msg void OnEnKillfocusMdnnr();
	CEdit m_mdnname;
	CString v_mdnname;
	afx_msg void OnCbnKillfocusCombospedit();
	afx_msg void OnEnKillfocusKundnr();
	afx_msg void OnCbnKillfocusCombologistik();
	afx_msg void OnCbnKillfocusComboabfert();
	afx_msg void OnCbnKillfocusCombofranka();
	afx_msg void OnCbnKillfocusCombofrachtg();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CEdit m_speich;
	CString v_speich;
	afx_msg void OnCbnSetfocusCombospedit();
	afx_msg void OnEnSetfocusKundnr();
	afx_msg void OnCbnSetfocusCombologistik();
	afx_msg void OnCbnSetfocusComboabfert();
	afx_msg void OnCbnSetfocusCombofranka();
	afx_msg void OnCbnSetfocusCombofrachtg();
	afx_msg void OnBnClickedButtdel();
	afx_msg void OnBnClickedButtdruck();
	afx_msg void OnCbnKillfocusComboaufgruppe();
	afx_msg void OnCbnSetfocusComboaufgruppe();
	CComboBox m_comboaufgruppe;
	CString v_comboaufgruppe;
};
