create table a_cron
  (
    mdn smallint,
    fil smallint,
    kun integer,
    kun_bran2 char(2),
    a decimal(13,0),
    lmhd integer
  ) in fit_dat extent size 32 next size 256 lock mode row ;

create index i01a_cron on a_cron (a) ;
