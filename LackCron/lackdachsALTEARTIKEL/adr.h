#ifndef _ADR_DEF
#define _ADR_DEF

struct ADR {
long adr;
char adr_krz[17];
char adr_nam1[37];
char adr_nam2[37];
char adr_nam3[37];
short adr_typ;
char adr_verkt[17];
short anr;
short delstatus;
char fax[21];
short fil;
char geb_dat[12];
short land;
short mdn;
char merkm_1[3];
char merkm_2[3];
char merkm_3[3];
char merkm_4[3];
char merkm_5[3];
char modem[21];
char ort1[37];
char ort2[37];
char partner[37];
char pf[17];
char plz[9];
short staat;
char str[37];
char tel[21];
char telex[21];
long txt_nr;
char plz_postf[9];
char plz_pf[9];
char iln[33];
char email[37];
char swift[25];

};
extern struct ADR adr, adr_null;
extern struct ADR adr2, adr3;


class ADR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leseadr (void);
               int openadr (void);
               ADR_CLASS () : DB_CLASS ()
               {
               }
};

/*--->
struct LEER_LSDR {

short mdn ;
short fil ;
long ls ; 
char blg_typ[2] ;
double a  ;
long me_stk_zu ;
long me_stk_abn ;
long stk ;
short stat ;
};

extern struct LEER_LSDR leer_lsdr, leer_lsdr_null;

class LEER_LSDR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leseleer_lsdr (void);
               int openleer_lsdr (void);
               LEER_LSDR_CLASS () : DB_CLASS ()
               {
               }
};
< --- */


#endif

