
-------- Original-Nachricht --------
> Datum: Fri, 18 Dec 2009 08:03:24 +0100
> Von: "Juergen Greiner" <Grj@setec-gmbh.de>
> An: juergen-greiner-berlin@gmx.de
> Betreff: WG: Thank you for downloading the /n software\'s EDI AS2 Connector

>  
> 
> -----Urspr�ngliche Nachricht-----
> Von: /n software - Sales Dept. [mailto:sales@nsoftware.com] 
> Gesendet: Donnerstag, 17. Dezember 2009 21:37
> An: Juergen Greiner
> Betreff: Thank you for downloading the /n software's EDI AS2 Connector
> 
> Hi , 
> 
> I noticed that you downloaded the /n software EDI AS2 Connector
> recently. 
> 
> I wanted to follow up with you to see how your evaluation of the the
> product is coming along.  If you need assistance with your evaluation
> please let us know.  Our sales and support teams are always here to
> assist you.
> 
> You can reach our support team in the following ways: 
> 
> Phone: 1-800-225-4190 or Intl: 919-544-7070 and select the support
> option
> Email: support@nsoftware.com
> Web: http://www.nsoftware.com/support/submit.aspx 
> 
> The Free AS2 Connector will always be free for a single trading partner.
> We also offer several upgrade options if you need to support more
> partners. The paid versions include a host of additional features listed
> here: http://www.freeas2.com/overview/upgrade.aspx
> 
> Thank you for your interest in our products. If you have any questions,
> please let me know.
> 
> Regards, 
> 
> -- Tony Hito
> /n software - The Net Tools Company
> (US)    800.225.4190
> (int'l) 919.544.7070  
> 
> 
> 


  
> create table cron_best
> (
> a decimal ( 13,0),
> pr_kz smallint,
> bsd_stk integer,
> me decimal ( 12,3)
> ) ;
> create unique index i01cron_best on cron_best( a, pr_kz) ;
>



Bestand:
> Die ASCII-Datei wird von Cron in das gleiche Verzeichnis wie die
> Auftragsdateien geschrieben.
> 
> Datei:	"cbest.dat"
> 
> L�nge	Art	Bezeichnung
> 1	A	Satzart (B)
> 8	N	Artikelnummer
> 1	N	Kennzeichen mit/ohne Preis (1/0)
> 12.3	N	Bestand in kg
> 8	N	Bestand in Stck
> 