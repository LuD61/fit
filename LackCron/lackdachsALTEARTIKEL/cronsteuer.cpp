#include <windows.h>
#include "DbClass.h"
#include "cronsteuer.h"
#include <time.h>

extern DB_CLASS dbClass;

extern CRONSTEUER_CLASS Cronsteuer ;

struct CRONSTEUER cronsteuer,  cronsteuer_null;

extern CRONMHD_CLASS Cronmhd ;

struct CRONMHD cronmhd,  cronmhd_null;

extern CRON_KOMM_CLASS Cron_komm ;

struct CRON_KOMM cron_komm,  cron_komm_null;


int CRONSTEUER_CLASS::closeall ( void )
{
	if ( readallcursor > -1 ) dbClass.sqlclose(readallcursor) ;
	if ( readcursor > -1 )    dbClass.sqlclose(readcursor) ;
	return 0 ;
}

int CRONSTEUER_CLASS::lesecronsteuer (	)
{
      memcpy ( &cronsteuer,&cronsteuer_null, sizeof(struct CRONSTEUER));
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int CRONSTEUER_CLASS::opencronsteuer (void)
{
		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

void CRONSTEUER_CLASS::prepare (void)
{

	dbClass.sqlout ((short *) &cronsteuer.mdn     , SQLSHORT, 0 ) ;
	dbClass.sqlout ((long  *) &cronsteuer.senden  , SQLLONG,  0 ) ;
	dbClass.sqlout ((long  *) &cronsteuer.empfang , SQLLONG,  0 ) ;
	dbClass.sqlout ((long  *) &cronsteuer.aktiv   , SQLLONG,  0 ) ;

	readcursor =
		(short)dbClass.sqlcursor ("select "
		" MDN ,SENDEN ,EMPFANG ,AKTIV  "
		" from cronsteuer " ) ;
} 

int CRONMHD_CLASS::closeall ( void )
{
	if ( ins_cursor > -1 ) dbClass.sqlclose(ins_cursor) ;
	return 0 ;
}

int CRONMHD_CLASS::schreibesatz ( void )
{

		if ( ins_cursor < 0 ) prepare ();
        return dbClass.sqlexecute (ins_cursor);
}


void CRONMHD_CLASS::prepare (void)
{

	dbClass.sqlin ((short *) &cronmhd.mdn   , SQLSHORT, 0 ) ;
	dbClass.sqlin ((long  *) &cronmhd.ls	, SQLLONG,  0 ) ;
	dbClass.sqlin ((long  *) &cronmhd.posi	, SQLLONG,  0 ) ;
	dbClass.sqlin ((double  *) &cronmhd.a   , SQLDOUBLE,  0 ) ;

	dbClass.sqlin ((double  *) &cronmhd.mmhd01	, SQLDOUBLE,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cronmhd.dmhd01,SQLTIMESTAMP,26);
	dbClass.sqlin ((double  *) &cronmhd.mmhd02	, SQLDOUBLE,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cronmhd.dmhd02,SQLTIMESTAMP,26);
	dbClass.sqlin ((double  *) &cronmhd.mmhd03	, SQLDOUBLE,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cronmhd.dmhd03,SQLTIMESTAMP,26);
	dbClass.sqlin ((double  *) &cronmhd.mmhd04	, SQLDOUBLE,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cronmhd.dmhd04,SQLTIMESTAMP,26);
	dbClass.sqlin ((double  *) &cronmhd.mmhd05	, SQLDOUBLE,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cronmhd.dmhd05,SQLTIMESTAMP,26);
	dbClass.sqlin ((double  *) &cronmhd.mmhd06	, SQLDOUBLE,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cronmhd.dmhd06,SQLTIMESTAMP,26);
	dbClass.sqlin ((double  *) &cronmhd.mmhd07	, SQLDOUBLE,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cronmhd.dmhd07,SQLTIMESTAMP,26);
	dbClass.sqlin ((double  *) &cronmhd.mmhd08	, SQLDOUBLE,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cronmhd.dmhd08,SQLTIMESTAMP,26);
	dbClass.sqlin ((double  *) &cronmhd.mmhd09	, SQLDOUBLE,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cronmhd.dmhd09,SQLTIMESTAMP,26);
	dbClass.sqlin ((double  *) &cronmhd.mmhd10	, SQLDOUBLE,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cronmhd.dmhd10,SQLTIMESTAMP,26);


	ins_cursor =
		(short)dbClass.sqlcursor ("insert into cronmhd "
		" ( MDN ,LS ,POSI ,A ,MMHD01 , DMHD01, MMHD02 ,DMHD02"
		",MMHD03 ,DMHD03 ,MMHD04 , DMHD04 "
		" ,MMHD05 ,DMHD05 ,MMHD06 ,DMHD06 ,MMHD07 ,DMHD07 ,MMHD08, DMHD08 ,MMHD09 ,DMHD09 "
		" ,MMHD10 ,DMHD10 "
		" ) values ( " 
//		" ?,?,?,?, ?,?,?,? ) " ) ;
		" ?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,? ) " ) ;
} 


int CRON_KOMM_CLASS::closeall ( void )
{
	if ( ins_cursor > -1 ) dbClass.sqlclose(ins_cursor) ;
	if ( del_cursor > -1 ) dbClass.sqlclose(del_cursor) ;
	if ( readcursor > -1 ) dbClass.sqlclose(readcursor) ;
	return 0 ;
}

int CRON_KOMM_CLASS::schreibecron_komm ( void )
{

		if ( ins_cursor < 0 ) prepare ();
        return dbClass.sqlexecute (ins_cursor);
}

int CRON_KOMM_CLASS::deletecron_komm ( void )
{

		if ( del_cursor < 0 ) prepare ();
        return dbClass.sqlexecute (del_cursor);
}

int CRON_KOMM_CLASS::lesecron_komm ( void )
{

      memcpy ( &cron_komm,&cron_komm_null, sizeof(struct CRON_KOMM));
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}


int CRON_KOMM_CLASS::opencron_komm (void)
{
		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}


void CRON_KOMM_CLASS::prepare (void)
{

	dbClass.sqlin ((short *) &cron_komm.mdn   , SQLSHORT, 0 ) ;
	dbClass.sqlin ((long  *) &cron_komm.ls	, SQLLONG,  0 ) ;
	dbClass.sqlin ((long  *) &cron_komm.kun	, SQLLONG,  0 ) ;
	dbClass.sqlin ((long  *) &cron_komm.tou_nr   , SQLLONG,  0 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &cron_komm.lieferdat,SQLTIMESTAMP,26);

	ins_cursor =
		(short)dbClass.sqlcursor ("insert into cron_komm "
		" ( MDN ,LS ,KUN ,TOU_NR ,AKV, LIEFERDAT "
		" ) values ( " 
		" ?,?,?,?,TODAY , ? ) " ) ;


	dbClass.sqlout ((short *) &cron_komm.mdn   , SQLSHORT, 0 ) ;
	dbClass.sqlout ((long  *) &cron_komm.ls	, SQLLONG,  0 ) ;
	dbClass.sqlout ((long  *) &cron_komm.kun	, SQLLONG,  0 ) ;
	dbClass.sqlout ((long  *) &cron_komm.tou_nr   , SQLLONG,  0 ) ;
	dbClass.sqlout ((TIMESTAMP_STRUCT *) &cron_komm.lieferdat,SQLTIMESTAMP,26);

	readcursor =
		(short)dbClass.sqlcursor ("select "
		"  MDN ,LS ,KUN ,TOU_NR , LIEFERDAT  "
		" from  cron_komm "
		" where mdn = 1 order by ls " ) ;

	dbClass.sqlin ((short *) &cron_komm.mdn   , SQLSHORT, 0 ) ;
	dbClass.sqlin ((long  *) &cron_komm.ls	, SQLLONG,  0 ) ;

	del_cursor =
		(short)dbClass.sqlcursor ("delete from  cron_komm "
		"  where MDN = ? and ls = ?" ) ;
	
}



