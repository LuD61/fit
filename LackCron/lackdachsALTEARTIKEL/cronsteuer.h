#ifndef _CRONSTEUER_DEF
#define _CRONSTEUER_DEF



#define BASDIR  "c:\\user\\fit\\cron\\"
#define TMPDIR  "c:\\user\\fit\\tmp\\"	

// ######################  CRONSTEUER ############################
struct CRONSTEUER
{
short mdn ;
long senden ;	
long empfang ;
long aktiv ;	

} ;

extern struct CRONSTEUER cronsteuer, cronsteuer_null;

struct CRONMHD 
{
short		mdn ;
long		ls ;
double		a ;
long		posi ;
double		mmhd01 ;
TIMESTAMP_STRUCT dmhd01 ; 
double		mmhd02 ;
TIMESTAMP_STRUCT dmhd02 ; 
double		mmhd03 ;
TIMESTAMP_STRUCT dmhd03 ; 
double		mmhd04 ;
TIMESTAMP_STRUCT dmhd04 ; 
double		mmhd05 ;
TIMESTAMP_STRUCT dmhd05 ; 
double		mmhd06 ;
TIMESTAMP_STRUCT dmhd06 ; 
double		mmhd07 ;
TIMESTAMP_STRUCT dmhd07 ; 
double		mmhd08 ;
TIMESTAMP_STRUCT dmhd08 ; 
double		mmhd09 ;
TIMESTAMP_STRUCT dmhd09 ; 
double		mmhd10 ;
TIMESTAMP_STRUCT dmhd10 ; 
};

extern struct CRONMHD cronmhd, cronmhd_null;

// 2-130912
struct CRON_KOMM
{
short mdn ;
long ls ;	
long kun ;
long tou_nr ;	
TIMESTAMP_STRUCT akv ; 
TIMESTAMP_STRUCT lieferdat ; 

} ;

extern struct CRON_KOMM cron_komm, cron_komm_null;


class CRONSTEUER_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesecronsteuer (void);
               int opencronsteuer (void);
               int closeall (void);
               CRONSTEUER_CLASS () : DB_CLASS ()
               {
//				   readcursor = -1 ;
               }
};

class CRONMHD_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int schreibesatz (void);
               int closeall (void);
               CRONMHD_CLASS () : DB_CLASS ()
               {
//				   readcursor = -1 ;
               }
};


class CRON_KOMM_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int schreibecron_komm (void);
               int deletecron_komm (void);
               int lesecron_komm (void);
               int opencron_komm (void);
				   

               int closeall (void);
               CRON_KOMM_CLASS () : DB_CLASS ()
               {
//				   readcursor = -1 ;
               }
};

#endif
