#pragma once
#include <stdio.h>
#include "vector.h"
#include "propertyitem.h"

class CProperties :
	public CVector
{
public:
	CString FileName;
	CString SectionName;
	CProperties(void);
	~CProperties(void);
    CString GetValue (CString Name);
	CString GetValues (CString Name);
    CString GetName (CString Value);
	int Find (CString Name);
	int FindValue (CString Value);
	void Set (CString Name, CString Value, int idx);
	void Set (CString& Record);
	virtual void SetWithSep (CString& Record, LPTSTR Sep);
	void SetWithSep (CString& Record, LPTSTR Sep1, LPTSTR Sep2);
	BOOL FindSection (FILE *fp);
	void Load ();
};
