#ifndef TEXT_DEF
#define TEXT_DEF
#include <string.h> 
#include <stdarg.h> 
#include <windows.h> 

#define Mid SubString

class Text
{
     char *Buffer;
     int Len;
     char *Sub;
  public :
     int GetLen (void)
     {
         return Len;
     }

	 int GetLength ()
	 {
		 return Len;
	 }

     Text ();
     Text (char *);
     Text (Text&);
     ~Text ();
     char *GetBuffer (void);
     void SetBuffer (char *);
     const Text& operator=(char *);
     const Text& operator=(int);
     const Text& operator=(Text&);

     Text& operator+ (char *);
     Text& operator+ (Text&);
     Text& operator+= (char *);
     Text& operator+= (Text&);
     BOOL operator== (Text&);
     BOOL operator== (char *);
     BOOL operator!= (Text&);
     BOOL operator!= (char *);
     BOOL operator> (Text&);
     BOOL operator> (char *);
     BOOL operator< (Text&);
     BOOL operator< (char *);
     char operator[] (int);
     Text& TrimLeft (void);
     Text& TrimRight (void);
     Text& Trim (void);
     char *SubString (int, int);
     void Format (char *format, ...);
     static BOOL matchcomp (LPSTR, LPSTR);
     BOOL CompareMatch (Text &);
     void MakeLines (int);
     void MakeUpper ();
     int Find (LPCSTR);
     int CompareNoCase (Text& text);
	 Text& Left (int len);
     static char *TrimRight (char *);
};

class CToken
{
  private :
     Text Buffer;
     Text **Tokens;
     Text sep;
     int AnzToken;
     int AktToken;
  public :
     CToken ();
     CToken (char *, char *);
     CToken (Text&, char *);
     ~CToken ();
     const CToken& operator=(char *);
     const CToken& operator=(Text&);
     void GetTokens (char *);
     void SetSep (char *);
     char * NextToken (void);
     char * GetToken (int);
     int GetAnzToken (void);
};
#endif
