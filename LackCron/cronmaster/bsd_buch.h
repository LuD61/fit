#ifndef _BSD_BUCH_DEF
#define _BSD_BUCH_DEF

struct BSD_BUCH {
long nr ;
char blg_typ[3] ;
short mdn ;
short fil ;
short kun_fil ;
double a ;
TIMESTAMP_STRUCT dat ;
char zeit [ 9] ;
char pers [13] ;
char bsd_lgr_ort [13] ; 
short qua_status ;
double me ;
double bsd_ek_vk ;
char chargennr [31] ;
char ident_nr [21] ;
char herk_nachw[4] ;
char lief [17];
long auf ;
char verfall [2] ;
TIMESTAMP_STRUCT verf_dat ;
short delstatus ;
char err_txt [17] ;
double me2 ;
short me_einh2 ;
TIMESTAMP_STRUCT lieferdat ;
double a_grund ;
short me_einh ;
};

extern struct BSD_BUCH bsd_buch, bsd_buch_null;

class BSD_BUCH_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int insertbsd_buch (void);
               int loeschebsd_buch (void);	// 180809
			
			   BSD_BUCH_CLASS () : DB_CLASS ()
               {
               }
};

struct LGR{
short lgr ;
short mdn ;
short fil ;
char lgr_smt_kz [4] ;
char lgr_bz [2] ;
short lgr_kz ;
short lgr_gr ;
char lgr_kla [2] ;
char abr_period [2] ;
long adr ;
short afl ;
char bli_kz [2] ;
TIMESTAMP_STRUCT dat_ero ;
double fl_lad ;
double fl_nto ;
double fl_vk_ges ;
short frm ;
TIMESTAMP_STRUCT iakv ;
char inv_rht [2] ;
char ls_abgr [2] ;
char ls_kz  [2] ;
short ls_sum ;
char pers [13] ;
short pers_anz ;
char pos_kum [2] ;
char pr_ausw [2] ;
char pr_bel_entl [2] ;
char pr_lgr_kz [2] ;
long pr_lst ;
char pr_vk_kz[2] ;
double reg_bed_theke_lng ;
double reg_kt_lng ;
double reg_kue_lng ;
double reg_lng ;
double reg_tks_lng ;
double reg_tkt_lng ;
char smt_kz[2] ; 
short sonst_einh ;
short sprache ;
char sw_kz[2] ;
long tou ;
short vrs_typ ;
char inv_akv[2] ;
short delstatus ;
};

struct A_LGR{
double a ;
double a_lgr_kap ;
short delstatus ;
short fil ;
long lgr ;
short lgr_pla_kz ;
short lgr_typ ;
short mdn ;
char haupt_lgr [2] ;
char lgr_platz [11] ;
double min_bestand ;
double meld_bestand ;
double hoechst_bestand ;
long inh_wanne ;
};

extern struct LGR lgr, lgr_null;

extern struct A_LGR a_lgr, a_lgr_null;

class LGR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lese_a_lager (void);
               LGR_CLASS () : DB_CLASS ()
               {
               }
};

// 180809 A

struct BSD{
short mdn ;
short fil ;
double a ;
long bsd_kost_st ;
char bsd_lgr_ort[13] ;
long lief_s ;
long lief_stk ; 
double lief_gew ;
short me_einh ;
double ek_pr ;
TIMESTAMP_STRUCT lief_dat ;
char lief_zeit_char[9] ;
long bsd_stk ;
double bsd_gew ;
TIMESTAMP_STRUCT verf_dat ;
char qua_status[3] ; 
char inv_flag[2] ;
long inv_stk ;
double inv_gew ;
TIMESTAMP_STRUCT inv_dat ;
TIMESTAMP_STRUCT bsd_update ;
char bsd_upzeit[9] ;
short delstatus ;
char lief[17] ;
char lief_best[17] ;
double pr_vk ;
long kun ;
short bsd_red_grnd ;
char pers[13] ;
long posi ;
double wrt ;
char chargennr[31] ;
char ident_nr[14] ;
char herk_nachw[14] ;
};

extern struct BSD bsd, bsd_null;

class BSD_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int setzebsd_stat   (void);
               int schreibebsd     (void);
               int loeschebsd_stat (void);
               BSD_CLASS () : DB_CLASS ()
               {
               }
};

struct BEST_RES{
double a ;
long auf ;
short fil ;
long kun ;
TIMESTAMP_STRUCT lief_term ;
short mdn ;
double me ;
TIMESTAMP_STRUCT melde_term ;
short stat ;
short kun_fil ;
char chargennr[31] ;
};

extern struct BEST_RES best_res, best_res_null;

class BEST_RES_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int loeschebest_res (void);
               BEST_RES_CLASS () : DB_CLASS ()
               {
               }
};
// 201209 A
struct CRON_BEST{
 double a ;
 short pr_kz ;
 long bsd_stk ;	// z.Z. IMMER == 0
 double me ;
 TIMESTAMP_STRUCT dat ;
 char zeit[11] ;
 TIMESTAMP_STRUCT mhd ;	// 080312
};

extern struct CRON_BEST cron_best, cron_best_null;

class CRON_BEST_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int loeschecron_best (void);
               int schreibecron_best (void);
			   int closeall(void) ;		// 280210
               CRON_BEST_CLASS () : DB_CLASS ()
               {
               }
};

// 201209 E


#endif

