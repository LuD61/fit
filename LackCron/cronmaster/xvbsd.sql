So kann man einen view anlegen, der anschliessend prima im LuL verwendbar ist 

create view vxbsd
(
a,
me,
bsd_gew)
as

 select bsd.a ,(select sum(me) from bsd_buch where bsd_buch.a = bsd.a)   ,
bsd.bsd_gew  from bsd
;
