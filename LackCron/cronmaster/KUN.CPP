//#include "stdafx.h"
#include <Windows.h>
#include "DbClass.h"
#include "kun.h"

struct KUN kun, kun_save, kun_null;
struct TOU tou, tou_save, tou_null;

struct A_BAS a_bas, a_bas_save, a_bas_null;
struct A_PFA_K a_pfa_k, a_pfa_k_save, a_pfa_k_null;	// 100913

struct A_HNDW a_hndw, a_hndw_save, a_hndw_null;
struct A_EIG  a_eig , a_eig_save , a_eig_null;

struct A_CRON  a_cron , a_cron_save , a_cron_null;	// 151009

//struct LEER_LSDR leer_lsdr, leer_lsdr_save, leer_lsdr_null;
//struct FIL fil, fil_save, fil_null;
extern DB_CLASS dbClass;


static int anzzfelder ;

int KUN_CLASS::dbcount (void)
/**
Tabelle kun lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}


int A_PFA_K_CLASS::closeall ( void )
{
	if ( readcursor > -1 )    dbClass.sqlclose(readcursor) ;
	return 0 ;
}

int A_PFA_K_CLASS::lesea_pfa_k (void)
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int A_PFA_K_CLASS::opena_pfa_k (void)
{
		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

// 280210
int A_BAS_CLASS::closeall ( void )
{
	if ( readallcursor > -1 ) dbClass.sqlclose(readallcursor) ;
	if ( readcursor > -1 )    dbClass.sqlclose(readcursor) ;
	return 0 ;
}

int A_BAS_CLASS::lesea_bas (void)
{
	sprintf ( a_bas.smt , "" ) ;	// 021210 : initialisieren 
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_BAS_CLASS::lesealla_bas (void)
{

	memcpy ( &a_bas, &a_bas_null, sizeof ( struct A_BAS ));
	int di = dbClass.sqlfetch (readallcursor);

	  return di;
}


int A_HNDW_CLASS::lesea_hndw ()
{
	if ( readcursor < 0 ) prepare ();
	int di = dbClass.sqlopen (readcursor);
	if ( !di)
	{
		memcpy ( &a_hndw, &a_hndw_null, sizeof ( struct A_HNDW ));	
		di = dbClass.sqlfetch (readcursor);
	}
	  return di;
}
int A_EIG_CLASS::lesea_eig ()
{
	if ( readcursor < 0 ) prepare ();
	int di = dbClass.sqlopen (readcursor);
	if ( !di)
	{
		memcpy ( &a_eig, &a_eig_null, sizeof ( struct A_EIG ));	
		di = dbClass.sqlfetch (readcursor);
	}
	  return di;
}

// 151009
int A_CRON_CLASS::lesea_cron ()
{
	if ( readcursor < 0 ) prepare ();
	int di = dbClass.sqlopen (readcursor);
	if ( !di)
	{
		memcpy ( &a_cron, &a_cron_null, sizeof ( struct A_CRON ));	
		di = dbClass.sqlfetch (readcursor);
	}
	  return di;
}

int KUN_CLASS::lesekun ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int TOU_CLASS::lesetou ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_BAS_CLASS::opena_bas (void)
{

		if ( readcursor < 0 ) prepare ();

         return dbClass.sqlopen (readcursor);
}

int A_BAS_CLASS::openalla_bas (void)
{

		if ( readallcursor < 0 ) prepare ();

         return dbClass.sqlopen (readallcursor);
}

int KUN_CLASS::openkun (void)
{

		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

int TOU_CLASS::opentou (void)
{

		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}


/* --->
int LEER_LSDR_CLASS::dbcount (void)
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}


int LEER_LSDR_CLASS::leselsdr ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}


int LEER_LSDR_CLASS::openlsdr (void)
{

	if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

int FIL_CLASS::dbcount (void)
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}

int FIL_CLASS::lesefil ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int FIL_CLASS::openfil (void)
{

		if ( readcursor < 0 ) prepare ();	

         return dbClass.sqlopen (readcursor);
}
< ---- */

void A_PFA_K_CLASS::prepare (void)
{

test_upd_cursor = 1;

	dbClass.sqlin  ((double *) &a_pfa_k.a, SQLDOUBLE, 0);

	dbClass.sqlout ((long   *) &a_pfa_k.posi, SQLLONG,  0);
	dbClass.sqlout ((double *) &a_pfa_k.a_k,  SQLDOUBLE,0);
	dbClass.sqlout ((long   *) &a_pfa_k.inh,  SQLLONG,  0);
	dbClass.sqlout ((char   *)  a_pfa_k.verkt,SQLCHAR,  2);
	dbClass.sqlout ((double *) &a_pfa_k.zinh, SQLDOUBLE,0);


	readcursor = (short)dbClass.sqlcursor ("select "

	" posi, a_k, inh, verkt, zinh  "
	" from a_pfa_k where a = ?   order by posi" ) ;
	
 }

void TOU_CLASS::prepare (void)
{

test_upd_cursor = 1;

	dbClass.sqlin ((long *) &tou.tou, SQLLONG, 0);	// hier stand bis 100913 "(double *)"

	dbClass.sqlout (( char *) tou.tou_bz,SQLCHAR, 49 ) ;
/* ->
	dbClass.sqlout (( char *)  tou.fz_kla,SQLCHAR,3) ;
	dbClass.sqlout (( char *)  tou.fz,SQLCHAR,13) ; 
	dbClass.sqlout (( char *)  tou.srt_zeit,SQLCHAR,6 ) ;
	dbClass.sqlout (( char *)  tou.dau,SQLCHAR,6 ) ;
	dbClass.sqlout (( long *) &tou.lng,SQLLONG,0) ;
	dbClass.sqlout (( char *)  tou.fah_1[13];
	dbClass.sqlout (( char *)  tou.fah_2[13];
	dbClass.sqlout (( short *)&tou.delstatus;
	dbClass.sqlout (( long *) &tou.lgr;
	dbClass.sqlout (( long *) &tou.leitw_typ;
	dbClass.sqlout (( long *) &tou.htou;
	dbClass.sqlout (( long *) &tou.adr;
	dbClass.sqlout (( short *)&tou.eigentour;
	dbClass.sqlout (( long *) &tou.lgkonto;

< ----- */


	readcursor = (short)dbClass.sqlcursor ("select "

	" tou_bz  "

	" from tou where tou = ?  " ) ;
	
 }

void A_HNDW_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &a_hndw.a, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_hndw.tara, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_hndw.a_pfa, SQLDOUBLE, 0);

	readcursor = (short) dbClass.sqlcursor ("select "

	" tara , a_pfa "

	" from a_hndw where a = ?  " ) ;
}

void A_EIG_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &a_eig.a, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_eig.tara, SQLDOUBLE, 0);

	readcursor = (short) dbClass.sqlcursor ("select "

	" tara  "

	" from a_eig where a = ?  " ) ;
}

// 151009
void A_CRON_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &a_cron.a, SQLDOUBLE, 0);
	dbClass.sqlout ((long *) &a_cron.lmhd, SQLLONG, 0);	// 020210 : bisher stand hier ein double-Pointer ?! -ist aber nur fuer Compiler wichtig ?!

	readcursor = (short) dbClass.sqlcursor ("select "

	" lmhd  "

	" from a_cron where a = ?  " ) ;
}


void A_BAS_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &a_bas.a, SQLDOUBLE, 0);

/* ----
	dbClass.sqlout (( double *)	&a_bas.a		decimal(13,0)                           yes
	dbClass.sqlout ((short *)	&a_bas.mdn      smallint                                yes
	dbClass.sqlout ((short *)	&a_bas.fil      smallint
---	*/
	dbClass.sqlout ((char *)	a_bas.a_bz1 ,SQLCHAR , 25 );
	dbClass.sqlout ((char *)	a_bas.a_bz2 ,SQLCHAR , 25 ) ;
/* ----
	dbClass.sqlout (( *)	&a_bas.a_gew                decimal(8,3)                            yes
---- */
	dbClass.sqlout (( short *)	&a_bas.a_typ, SQLSHORT, 0 ) ;	// 100913
/* ---
	dbClass.sqlout (( *)	&a_bas.a_typ2               smallint                                yes
	dbClass.sqlout (( *)	&a_bas.abt                  smallint                                yes
	dbClass.sqlout (( *)	&a_bas.ag                   integer                                 yes
	dbClass.sqlout (( *)	&a_bas.best_auto            char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.bsd_kz               char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.cp_aufschl           char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.delstatus            smallint                                yes
	dbClass.sqlout (( *)	&a_bas.dr_folge             smallint                                yes
	dbClass.sqlout (( *)	&a_bas.erl_kto              integer                                 yes
	dbClass.sqlout (( *)	&a_bas.hbk_kz               char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.hbk_ztr              smallint   
< ---- */
	dbClass.sqlout ((char *)	a_bas.hnd_gew ,SQLCHAR , 3 ) ;
/* --->
	dbClass.sqlout (( *)	&a_bas.hwg                  smallint                                yes
	dbClass.sqlout (( *)	&a_bas.kost_kz              char(2)                                 yes
< --- */
	dbClass.sqlout ((short *)	&a_bas.me_einh , SQLSHORT , 0 ) ;
/* --->
	dbClass.sqlout (( *)	&a_bas.modif                char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.mwst                 smallint                                yes
	dbClass.sqlout (( *)	&a_bas.plak_div             smallint                                yes
	dbClass.sqlout (( *)	&a_bas.stk_lst_kz           char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.sw                   decimal(3,1)
< --- */
	dbClass.sqlout ((short *)	&a_bas.teil_smt, SQLSHORT, 0 ) ;
/* --->
	dbClass.sqlout (( *)	&a_bas.we_kto               integer                                 yes
	dbClass.sqlout (( *)	&a_bas.wg                   smallint                                yes
	dbClass.sqlout (( *)	&a_bas.zu_stoff             smallint                                yes
	dbClass.sqlout (( *)	&a_bas.akv                  date                                    yes
	dbClass.sqlout (( *)	&a_bas.bearb                date                                    yes
	dbClass.sqlout (( *)	&a_bas.pers_nam             char(8)                                 yes
	dbClass.sqlout (( *)	&a_bas.prod_zeit            decimal(6,3)                            yes
	dbClass.sqlout (( *)	&a_bas.pers_rab_kz          char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.gn_pkt_gbr           decimal(8,4)                            yes
	dbClass.sqlout (( *)	&a_bas.kost_st              integer                                 yes
	dbClass.sqlout (( *)	&a_bas.sw_pr_kz             char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.kost_tr              integer                                 yes
< ---- */
	dbClass.sqlout ((double *)	&a_bas.a_grund, SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((char *)	 a_bas.smt, SQLCHAR , 2 ) ;		// 021210
/* --->
	dbClass.sqlout (( *)	&a_bas.kost_st2             integer                                 yes
	dbClass.sqlout (( *)	&a_bas.we_kto2              integer                                 yes
	dbClass.sqlout (( *)	&a_bas.charg_hand           integer                                 yes
	dbClass.sqlout (( *)	&a_bas.intra_stat           integer                                 yes
	dbClass.sqlout (( *)	&a_bas.qual_kng             char(4)                                 yes
	dbClass.sqlout (( *)	&a_bas.a_bz3                char(24)                                yes
	dbClass.sqlout (( *)	&a_bas.lief_einh            smallint                                yes
	dbClass.sqlout (( *)	&a_bas.inh_lief             decimal(11,3)                           yes
	dbClass.sqlout (( *)	&a_bas.erl_kto_1            integer                                 yes
	dbClass.sqlout (( *)	&a_bas.erl_kto_2            integer                                 yes
	dbClass.sqlout (( *)	&a_bas.erl_kto_3            integer                                 yes
	dbClass.sqlout (( *)	&a_bas.we_kto_1             integer                                 yes
	dbClass.sqlout (( *)	&a_bas.we_kto_2             integer                                 yes
	dbClass.sqlout (( *)	&a_bas.we_kto_3             integer                                 yes
	dbClass.sqlout (( *)	&a_bas.skto_f               char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.sk_vollk             decimal(8,4)                            yes
	dbClass.sqlout (( *)	&a_bas.a_ersatz             decimal(13,0)                           yes
	dbClass.sqlout (( *)	&a_bas.a_ers_kz             smallint                                yes
	dbClass.sqlout (( *)	&a_bas.me_einh_abverk       smallint                                yes
	dbClass.sqlout (( *)	&a_bas.inh_abverk           decimal(11,3)                           yes
	dbClass.sqlout (( *)	&a_bas.hnd_gew_abverk       char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.inh_ek               decimal(11,3)                           yes
< ----- */

// 021210 : smt zugefuegt
// 100913 : a_typ zugefuegt
	readcursor = (short) dbClass.sqlcursor ("select "

	" a_bz1, a_bz2, a_typ, hnd_gew, me_einh, teil_smt, a_grund ,smt "

	" from a_bas where a = ?  " ) ;


// fuer readallcursor .....

	dbClass.sqlout ((double *) &a_bas.a, SQLDOUBLE, 0);

/* ----
	dbClass.sqlout ((short *)	&a_bas.mdn      smallint                                yes
	dbClass.sqlout ((short *)	&a_bas.fil      smallint
---	*/
	dbClass.sqlout ((char *)	a_bas.a_bz1 ,SQLCHAR , 25 );
	dbClass.sqlout ((char *)	a_bas.a_bz2 ,SQLCHAR , 25 ) ;

	dbClass.sqlout ((double *)	&a_bas.a_gew ,SQLDOUBLE , 0 ) ;
	dbClass.sqlout ((short *)	&a_bas.a_typ , SQLSHORT ,0 );
	dbClass.sqlout ((short *)	&a_bas.a_typ2 , SQLSHORT ,0);
	dbClass.sqlout ((short *)	&a_bas.abt , SQLSHORT , 0 ) ;
	dbClass.sqlout ((long *)	&a_bas.ag ,SQLLONG ,0 ) ;
/* --->
	dbClass.sqlout (( *)	&a_bas.best_auto            char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.bsd_kz               char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.cp_aufschl           char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.delstatus            smallint                                yes
	dbClass.sqlout (( *)	&a_bas.dr_folge             smallint                                yes
	dbClass.sqlout (( *)	&a_bas.erl_kto              integer                                 yes
	dbClass.sqlout (( *)	&a_bas.hbk_kz               char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.hbk_ztr              smallint   
< ---- */
	dbClass.sqlout ((char *)	a_bas.hnd_gew ,SQLCHAR , 3 ) ;
/* --->
	dbClass.sqlout (( *)	&a_bas.hwg                  smallint                                yes
	dbClass.sqlout (( *)	&a_bas.kost_kz              char(2)                                 yes
< --- */
	dbClass.sqlout ((short *)	&a_bas.me_einh , SQLSHORT , 0 ) ;
/* --->
	dbClass.sqlout (( *)	&a_bas.modif                char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.mwst                 smallint                                yes
	dbClass.sqlout (( *)	&a_bas.plak_div             smallint                                yes
	dbClass.sqlout (( *)	&a_bas.stk_lst_kz           char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.sw                   decimal(3,1)
< --- */
	dbClass.sqlout ((short *)	&a_bas.teil_smt, SQLSHORT, 0 ) ;
/* --->
	dbClass.sqlout (( *)	&a_bas.we_kto               integer                                 yes
	dbClass.sqlout (( *)	&a_bas.wg                   smallint                                yes
	dbClass.sqlout (( *)	&a_bas.zu_stoff             smallint                                yes
	dbClass.sqlout (( *)	&a_bas.akv                  date                                    yes
	dbClass.sqlout (( *)	&a_bas.bearb                date                                    yes
	dbClass.sqlout (( *)	&a_bas.pers_nam             char(8)                                 yes
	dbClass.sqlout (( *)	&a_bas.prod_zeit            decimal(6,3)                            yes
	dbClass.sqlout (( *)	&a_bas.pers_rab_kz          char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.gn_pkt_gbr           decimal(8,4)                            yes
	dbClass.sqlout (( *)	&a_bas.kost_st              integer                                 yes
	dbClass.sqlout (( *)	&a_bas.sw_pr_kz             char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.kost_tr              integer                                 yes
< ---- */
	dbClass.sqlout ((double *)	&a_bas.a_grund, SQLDOUBLE , 0 ) ;
/* --->
	dbClass.sqlout (( *)	&a_bas.kost_st2             integer                                 yes
	dbClass.sqlout (( *)	&a_bas.we_kto2              integer                                 yes
	dbClass.sqlout (( *)	&a_bas.charg_hand           integer                                 yes
	dbClass.sqlout (( *)	&a_bas.intra_stat           integer                                 yes
	dbClass.sqlout (( *)	&a_bas.qual_kng             char(4)                                 yes
	dbClass.sqlout (( *)	&a_bas.a_bz3                char(24)                                yes
	dbClass.sqlout (( *)	&a_bas.lief_einh            smallint
< -- */

	dbClass.sqlout ((double *) &a_bas.inh_lief ,SQLDOUBLE,0 ) ;

/* --->
	dbClass.sqlout (( *)	&a_bas.erl_kto_1            integer                                 yes
	dbClass.sqlout (( *)	&a_bas.erl_kto_2            integer                                 yes
	dbClass.sqlout (( *)	&a_bas.erl_kto_3            integer                                 yes
	dbClass.sqlout (( *)	&a_bas.we_kto_1             integer                                 yes
	dbClass.sqlout (( *)	&a_bas.we_kto_2             integer                                 yes
	dbClass.sqlout (( *)	&a_bas.we_kto_3             integer                                 yes
	dbClass.sqlout (( *)	&a_bas.skto_f               char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.sk_vollk             decimal(8,4)                            yes
	dbClass.sqlout (( *)	&a_bas.a_ersatz             decimal(13,0)                           yes
	dbClass.sqlout (( *)	&a_bas.a_ers_kz             smallint                                yes
	dbClass.sqlout (( *)	&a_bas.me_einh_abverk       smallint
< ---- */
	dbClass.sqlout (( double *)	&a_bas.inh_abverk , SQLDOUBLE, 0 ) ;
	dbClass.sqlout ((char *)	a_bas.smt, SQLCHAR , 2 ) ;	// 021210
/* --->
	dbClass.sqlout (( *)	&a_bas.hnd_gew_abverk       char(1)                                 yes
	dbClass.sqlout (( *)	&a_bas.inh_ek               decimal(11,3)                           yes
< ----- */


	// 021210 : smt zugefuegt 

	readallcursor = (short) dbClass.sqlcursor ("select "

	" a, a_bz1, a_bz2,a_gew, a_typ, a_typ2, abt, ag, hnd_gew, me_einh, teil_smt "
	" ,a_grund, inh_lief , inh_abverk , smt  "
	" from a_bas where a >0 order by a  " ) ;



}


// 280210
int KUN_CLASS::closeall ( void )
{
	if ( readcursor > -1 )    dbClass.sqlclose(readcursor) ;
	if ( count_cursor > -1 )    dbClass.sqlclose(count_cursor) ;
	return 0 ;
}


void KUN_CLASS::prepare (void)
{

	dbClass.sqlin ((long *) &kun.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = (short)dbClass.sqlcursor ("select count(*) from kun "
										"where kun.kun = ? and kun.mdn = ? ");
										
	test_upd_cursor = 1;


	dbClass.sqlin ((long *) &kun.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &kun.mdn, SQLSHORT, 0);


dbClass.sqlout ((short *) &kun.mdn, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.fil, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.kun, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.adr1, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.adr2, SQLLONG, 0);  
dbClass.sqlout ((long *) &kun.adr3, SQLLONG, 0); 
dbClass.sqlout ((char *) kun.kun_seit, SQLCHAR, 12);
dbClass.sqlout ((long *) &kun.txt_nr1, SQLLONG, 0);
dbClass.sqlout ((char *) kun.frei_txt1, SQLCHAR, 65);

dbClass.sqlout ((char *) kun.kun_krz1, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.kun_bran, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.kun_krz2, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.kun_krz3, SQLCHAR, 17);
dbClass.sqlout ((short *) &kun.kun_typ, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.bbn, SQLLONG, 0);
dbClass.sqlout ((short *) &kun.pr_stu, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.pr_lst, SQLLONG, 0);
dbClass.sqlout ((char *) kun.vereinb, SQLCHAR, 6);
dbClass.sqlout ((long *) &kun.inka_nr, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.vertr1, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.vertr2, SQLLONG, 0);
dbClass.sqlout ((char *) kun.statk_period, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.a_period, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.sprache, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.txt_nr2, SQLLONG, 0);
dbClass.sqlout ((char *) kun.frei_txt2, SQLCHAR, 65);
dbClass.sqlout ((char *) kun.freifeld1, SQLCHAR, 9);
dbClass.sqlout ((char *) kun.freifeld2, SQLCHAR, 9);
dbClass.sqlout ((long *) &kun.tou, SQLLONG, 0);
dbClass.sqlout ((char *) kun.vers_art, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.lief_art, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.fra_ko_ber, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.rue_schei, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.form_typ1, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.auflage1, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.freifeld3, SQLCHAR, 9);
dbClass.sqlout ((char *) kun.freifeld4, SQLCHAR, 9);
dbClass.sqlout ((short *) &kun.zahl_art, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.zahl_ziel, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.form_typ2, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.auflage2, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.txt_nr3, SQLLONG, 0);
dbClass.sqlout ((char *) kun.frei_txt3, SQLCHAR, 65);
dbClass.sqlout ((char *) kun.nr_bei_rech, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.rech_st, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.sam_rech, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.einz_ausw, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.gut, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.rab_schl, SQLCHAR, 9);

dbClass.sqlout ((double *) &kun.bonus1, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.bonus2, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.tdm_grenz1, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.tdm_grenz2, SQLDOUBLE, 0);
dbClass.sqlout ((double *) &kun.jr_plan_ums, SQLDOUBLE, 0);
dbClass.sqlout ((char *) kun.deb_kto, SQLCHAR, 9);
dbClass.sqlout ((double *) &kun.kred_lim, SQLDOUBLE, 0);
dbClass.sqlout ((short *) &kun.inka_zaehl, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.bank_kun, SQLCHAR, 37);
dbClass.sqlout ((long *) &kun.blz, SQLLONG, 0);
dbClass.sqlout ((char *) kun.kto_nr, SQLCHAR, 17);
dbClass.sqlout ((short *) &kun.hausbank, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_of_po, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_of_lf, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_of_best, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.delstatus, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.kun_bran2, SQLCHAR, 3);
dbClass.sqlout ((long *) &kun.rech_fuss_txt, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.ls_fuss_txt, SQLLONG, 0);
dbClass.sqlout ((char *) kun.ust_id, SQLCHAR, 12);
dbClass.sqlout ((long *) &kun.rech_kopf_txt, SQLLONG, 0);
dbClass.sqlout ((long *) &kun.ls_kopf_txt, SQLLONG, 0);
dbClass.sqlout ((char *) kun.gn_pkt_kz, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.sw_rab, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.bbs, SQLCHAR, 9);
dbClass.sqlout ((long *) &kun.inka_nr2, SQLLONG, 0);
dbClass.sqlout ((short *) &kun.sw_fil_gr, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sw_fil, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.ueb_kz, SQLCHAR, 2);
dbClass.sqlout ((char *) kun.modif, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.kun_leer_kz, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.ust_id16, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.iln, SQLCHAR, 17);
dbClass.sqlout ((short *) &kun.waehrung, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.pr_ausw, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.pr_hier, SQLCHAR, 2);
dbClass.sqlout ((short *) &kun.pr_ausw_ls, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.pr_ausw_re, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_gr1, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kun_gr2, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.eg_kz, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.bonitaet, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.kred_vers, SQLSHORT, 0);
dbClass.sqlout ((long *) &kun.kst, SQLLONG, 0);
dbClass.sqlout ((char *) kun.edi_typ, SQLCHAR, 2);
dbClass.sqlout ((long *) &kun.sedas_dta, SQLLONG, 0);
dbClass.sqlout ((char *) kun.sedas_kz, SQLCHAR, 3);
dbClass.sqlout ((short *) &kun.sedas_umf, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_abr, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_gesch, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_satz, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_med, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.sedas_nam, SQLCHAR, 11);
dbClass.sqlout ((short *) &kun.sedas_abk1, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_abk2, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_abk3, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.sedas_nr1, SQLCHAR, 9);
dbClass.sqlout ((char *) kun.sedas_nr2, SQLCHAR, 9);
dbClass.sqlout ((char *) kun.sedas_nr3, SQLCHAR, 9);
dbClass.sqlout ((short *) &kun.sedas_vb1, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_vb2, SQLSHORT, 0);
dbClass.sqlout ((short *) &kun.sedas_vb3, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.sedas_iln, SQLCHAR, 17);
dbClass.sqlout ((long *) &kun.kond_kun, SQLLONG, 0);
dbClass.sqlout ((short *) &kun.kun_schema, SQLSHORT, 0);
dbClass.sqlout ((char *) kun.plattform, SQLCHAR, 17);
dbClass.sqlout ((char *) kun.be_log, SQLCHAR, 4);
dbClass.sqlout ((long *) &kun.stat_kun, SQLLONG, 0);
dbClass.sqlout ((char *) kun.ust_nummer, SQLCHAR, 25);
dbClass.sqlout ((long *) &kun.cmr, SQLLONG, 0);	// 121212
dbClass.sqlout ((long *) &kun.pal_bew, SQLLONG, 0);	// 121212
dbClass.sqlout ((long *) &kun.trakober, SQLLONG, 0);	// 121212
dbClass.sqlout ((short*) &kun.pfand_ber, SQLSHORT, 0);	// 190913


	
	readcursor = (short) dbClass.sqlcursor ("select "

	" mdn, fil, kun, adr1, adr2, adr3, kun_seit, txt_nr1, frei_txt1, "
	" kun_krz1, kun_bran, kun_krz2, kun_krz3, kun_typ, bbn, pr_stu, "
	" pr_lst, vereinb, inka_nr, vertr1, vertr2, statk_period, a_period, "
	" sprache, txt_nr2, frei_txt2, freifeld1, freifeld2, tou, vers_art, "
	" lief_art, fra_ko_ber, rue_schei, form_typ1, auflage1, freifeld3, "
	" freifeld4, zahl_art, zahl_ziel, form_typ2, auflage2, txt_nr3, "
	" frei_txt3, nr_bei_rech, rech_st, sam_rech, einz_ausw, gut, rab_schl, "
	" bonus1, bonus2, tdm_grenz1, tdm_grenz2, jr_plan_ums, deb_kto, "
	" kred_lim, inka_zaehl, bank_kun, blz, kto_nr, hausbank, kun_of_po, "
	" kun_of_lf, kun_of_best, delstatus, kun_bran2, rech_fuss_txt, "
	" ls_fuss_txt, ust_id, rech_kopf_txt, ls_kopf_txt, gn_pkt_kz, sw_rab, "
	" bbs, inka_nr2, sw_fil_gr, sw_fil, ueb_kz, modif, kun_leer_kz,"
	" ust_id16, iln, waehrung, pr_ausw, pr_hier, pr_ausw_ls, "
	" pr_ausw_re, kun_gr1, kun_gr2, eg_kz, bonitaet, kred_vers, kst, "
	" edi_typ, sedas_dta, sedas_kz, sedas_umf, sedas_abr, sedas_gesch,"
	" sedas_satz, sedas_med, sedas_nam, sedas_abk1, sedas_abk2, sedas_abk3, "
	" sedas_nr1, sedas_nr2, sedas_nr3, sedas_vb1, sedas_vb2, sedas_vb3, "
	" sedas_iln, kond_kun, kun_schema, plattform, be_log, stat_kun, "
	" ust_nummer , cmr, pal_bew, trakober, pfand_ber "

	" from kun where kun = ? and mdn = ? " ) ;
	
 }

/* ---->

void LEER_LSDR_CLASS::prepare (void)
{


	dbClass.sqlin ((short *) &leer_lsdr.mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &leer_lsdr.fil, SQLSHORT, 0);
    dbClass.sqlin ((long *)  &leer_lsdr.ls, SQLLONG, 0);
	dbClass.sqlin ((char *)  leer_lsdr.blg_typ, SQLCHAR, 2);

test_upd_cursor = 1;


dbClass.sqlout ((double *) &leer_lsdr.a, SQLDOUBLE, 0);
dbClass.sqlout ((long *) &leer_lsdr.me_stk_zu, SQLLONG, 0);
dbClass.sqlout ((long *) &leer_lsdr.me_stk_abn, SQLLONG, 0);
dbClass.sqlout ((long *) &leer_lsdr.stk, SQLLONG, 0);
dbClass.sqlout ((long *) &leer_lsdr.stat, SQLSHORT, 0);
	
	readcursor = dbClass.sqlcursor ("select "

	" a, me_stk_zu, me_stk_abn, stk, stat "

	" from leer_lsdr where mdn = ? and fil = ?  and ls = ? and blg_typ = ? order by a " ) ;
	
 }

void FIL_CLASS::prepare (void)
{

	test_upd_cursor = 1;

	dbClass.sqlin ((short *) &fil.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &fil.mdn, SQLSHORT, 0);

	dbClass.sqlout ((char  *)  fil.abr_period,SQLCHAR,2) ;
	dbClass.sqlout ((long  *) &fil.adr,SQLLONG,0);
	dbClass.sqlout ((long  *) &fil.adr_lief,SQLLONG,0);
	dbClass.sqlout ((short *) &fil.afl,SQLSHORT,0);
	dbClass.sqlout ((char  *)  fil.auf_typ,SQLCHAR,2);
	dbClass.sqlout ((char  *)  fil.best_kz,SQLCHAR,2);
	dbClass.sqlout ((char  *)  fil.bli_kz,SQLCHAR,2);
	dbClass.sqlout ((char  *)  fil.dat_ero,SQLCHAR,12);
	dbClass.sqlout ((short *) &fil.daten_mnp,SQLSHORT,0);
	dbClass.sqlout ((short *) &fil.delstatus,SQLSHORT,0);
	dbClass.sqlout ((short *) &fil.fil,SQLSHORT,0);
	dbClass.sqlout ((char  *)  fil.fil_kla,SQLCHAR,2);
	dbClass.sqlout ((short *) &fil.fil_gr,SQLSHORT,0);
	dbClass.sqlout ((double*) &fil.fl_lad,SQLDOUBLE,0);
	dbClass.sqlout ((double*) &fil.fl_nto,SQLDOUBLE,0);
	dbClass.sqlout ((double*) &fil.fl_vk_ges,SQLDOUBLE,0);
	dbClass.sqlout ((short *) &fil.frm,SQLSHORT,0);
	dbClass.sqlout ((char  *)  fil.iakv,SQLCHAR,12);
	dbClass.sqlout ((char  *)  fil.inv_rht,SQLCHAR,2);
	dbClass.sqlout ((long  *) &fil.kun,SQLLONG,0);
	dbClass.sqlout ((char  *)  fil.lief,SQLCHAR,17);
	dbClass.sqlout ((char  *)  fil.lief_rht,SQLCHAR,2);
	dbClass.sqlout ((long  *) &fil.lief_s,SQLLONG,0);
	dbClass.sqlout ((char  *)  fil.ls_abgr,SQLCHAR,2);
	dbClass.sqlout ((char  *)  fil.ls_kz,SQLCHAR,2);
	dbClass.sqlout ((short *) &fil.ls_sum,SQLSHORT,0);
	dbClass.sqlout ((short *) &fil.mdn,SQLSHORT,0);
	dbClass.sqlout ((char  *)  fil.pers,SQLCHAR,13);
	dbClass.sqlout ((short *) &fil.pers_anz,SQLSHORT,0);
	dbClass.sqlout ((char  *)  fil.pos_kum,SQLCHAR,2);
	dbClass.sqlout ((char  *)  fil.pr_ausw,SQLCHAR,2);
	dbClass.sqlout ((char  *)  fil.pr_bel_entl,SQLCHAR,2);
	dbClass.sqlout ((char  *)  fil.pr_fil_kz,SQLCHAR,2);
	dbClass.sqlout ((long  *) &fil.pr_lst,SQLLONG,0);
	dbClass.sqlout ((char  *)  fil.pr_vk_kz,SQLCHAR,2);
	dbClass.sqlout ((double*) &fil.reg_bed_theke_lng,SQLDOUBLE,0);
	dbClass.sqlout ((double*) &fil.reg_kt_lng,SQLDOUBLE,0);
	dbClass.sqlout ((double*) &fil.reg_kue_lng,SQLDOUBLE,0);
	dbClass.sqlout ((double*) &fil.reg_lng,SQLDOUBLE,0);
	dbClass.sqlout ((double*) &fil.reg_tks_lng,SQLDOUBLE,0);
	dbClass.sqlout ((double*) &fil.reg_tkt_lng,SQLDOUBLE,0);
	dbClass.sqlout ((char  *)  fil.ret_entl,SQLCHAR,2);
	dbClass.sqlout ((char  *)  fil.smt_kz,SQLCHAR,2);
	dbClass.sqlout ((short *) &fil.sonst_einh,SQLSHORT,0);
	dbClass.sqlout ((short *) &fil.sprache,SQLSHORT,0);
	dbClass.sqlout ((char  *)  fil.sw_kz,SQLCHAR,2);
	dbClass.sqlout ((long  *) &fil.tou,SQLLONG,0);
	dbClass.sqlout ((char  *)  fil.umlgr,SQLCHAR,2);
	dbClass.sqlout ((char  *)  fil.verk_st_kz,SQLCHAR,2);
	dbClass.sqlout ((short *) &fil.vrs_typ,SQLSHORT,0);
	dbClass.sqlout ((char  *)  fil.inv_akv,SQLCHAR,2);
	dbClass.sqlout ((double*) &fil.planumsatz,SQLDOUBLE,0);

	
	readcursor = dbClass.sqlcursor ("select "

	" abr_period, adr, adr_lief, afl, auf_typ, best_kz "
	" ,bli_kz, dat_ero, daten_mnp, delstatus, fil "
	" ,fil_kla, fil_gr, fl_lad, fl_nto, fl_vk_ges, frm "
	" ,iakv, inv_rht, kun, lief, lief_rht, lief_s "
	" ,ls_abgr, ls_kz, ls_sum, mdn, pers, pers_anz, pos_kum "
    " ,pr_ausw, pr_bel_entl, pr_fil_kz, pr_lst, pr_vk_kz "
	" ,reg_bed_theke_lng, reg_kt_lng, reg_kue_lng, reg_lng "
	" ,reg_tks_lng, reg_tkt_lng, ret_entl, smt_kz, sonst_einh "
	" ,sprache, sw_kz, tou, umlgr, verk_st_kz, vrs_typ, inv_akv "
	" ,planumsatz "


	" from fil where fil = ? and mdn = ? " ) ;
	
}
< ---- */
