// 561lackDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "561lack.h"
#include "561lackDlg.h"
#include "DbClass.h"
#include "mdn.h"
#include "adr.h"
#include "kun.h"
#include "ls.h"
#include "sys_par.h"
#include "liefret.h"
#include "tou.h"	// 161211

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MAXTOUNR 999999999	// 010612

DB_CLASS dbClass ;	// Hier ist die Heimat der dbClass, alle Tabellen sind in ihrem jeweiligen Modul daheim
extern MDN_CLASS mdn_class ;
extern ADR_CLASS adr_class ;
extern LSK_CLASS lsk_class ;
extern LSP_CLASS lsp_class ;
extern KUN_CLASS kun_class ;
extern FIL_CLASS fil_class ;
extern SYS_PAR_CLASS sys_par_class ;
extern KUNBRTXTZU_CLASS kunbrtxtzu_class ;
extern LIEF_RETBA_CLASS lief_retba_class ;
extern TOU_CLASS tou_class ;	// 161211


#define IMAXDIM 1450 
long schlussmat [IMAXDIM + 2 ];	// Array LS-Nummern
long schluss2mat[IMAXDIM + 2 ];	// Array Kunden-Nummern zum LS
int eigmat[IMAXDIM + 2 ] ;		// Array LS-Status-Infos
// Status : == 0	// da war was, aber bitte nichts mehr machen 
// Status : == 3	// es war komplett 
// Status : == 4	// es war gedruckt
// Status : == -1	// leer 
// Status : == -2	// es gibt probleme , am besten ignorieren
long kungutmat[IMAXDIM + 2 ];	// zu fakturierende Kunden
long kunbadmat[IMAXDIM + 2 ];	// nicht zu fakturierende Kunden

// 280812 
CString scandatum ;
long scantour ;
BOOL scansequenz ;
BOOL scanactiv ;
BOOL scankaputt ;

int spnutzdruck;	// 261114
int dnachkpreis ;
char bufh[502];



long TourenFaktor ;	// 170513 -> Detlef will Chaos erzeugen  


// ALLES NUR WEGEN BWS_DEFA -ANFANG 

char *wort[25] ; // Bereich fuer Worte eines Strings
static char buffer[1000] ;
static char DefWert[256] ;


void cr_weg (char *string)

{
 for (; *string; string += 1)
 {
  if (*string == (char) 13)
    break;
  if (*string == (char) 10)
    break;
  if (*string == '\0')
    return ;
 }
 *string = 0;
 return;
}

int next_char_ci (char *string, char tzeichen, int i)
// Naechstes Zeichen != Trennzeichen suchen.

{
       for (;string [i]; i ++)
       {
               if (string[i] != tzeichen)
               {
                                   return (i);
               }
       }
       return (i);
}


short split (char *string)
{
 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
//  if (buffer == (char *) 0) buffer = (char *) malloc (0x1000);
	wz = 0;
	j = 0;
	len = (int)strlen (string);
	wz = 1;
	i = next_char_ci (string, zeichen, 0);
	if (i >= len) return (0);
	wort [wz] = buffer;
	wz ++;
	for (; i < len; i ++, j ++)
	{
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
	}
	buffer [j] = (char) 0;
	return (wz - 1);
}

int strupcmp (char *str1, char *str2, int len)
{
 short i;
 unsigned char upstr1;
 unsigned char upstr2;


	for (i = 0; i < len; i ++, str1 ++, str2 ++)
	{
		if (*str1 == 0)
			return (-1);
		if (*str2 == 0)
			return (1);

		upstr1 = (unsigned char) toupper((int) *str1);
		switch (upstr1)
		{
		case (unsigned char) '�' :
            upstr1 = '�';
			break;
		case (unsigned char) '�' :
            upstr1 = '�';
			break;
		case (unsigned char) '�' :
            upstr1 = '�';
			break;
		}

		upstr2 = (unsigned char) toupper((int) *str2);
		switch (upstr2)
		{
		case (unsigned char) '�' :
            upstr2 = '�';
			break;
		case (unsigned char) '�' :
            upstr2 = '�';
			break;
		case (unsigned char) '�' :
            upstr2 = '�';
			break;
		}
		if (upstr1 < upstr2)
		{
			return(-1);
		}
		else if (upstr1 > upstr2)
		{
			return (1);
		}
	}
	return (0);
}


char *bws_default (char *env)
/**
Wert aus Bws-default holen.
**/
{         
	char *etc;
    int anz;
    char buffer [512];
    FILE *fp;

    etc = getenv ("BWSETC");
    if (etc == (char *) 0)
    {
		etc = "C:\\USER\\FIT\\ETC";
    }
    sprintf (buffer, "%s\\bws_defa", etc);
	fp = fopen (buffer, "r");
    if (fp == NULL) return NULL;

     while (fgets (buffer, 511, fp))
     {
		cr_weg (buffer);
        anz = split (buffer);
        if (anz < 2) continue;
        if (strupcmp (wort[1], env, (int) strlen (env)) == 0)
        {
			strcpy (DefWert, wort [2]);
            fclose (fp);
			return (char *) DefWert;
        }
	}
    fclose (fp);
    return NULL;
}
// ALLES NUR WEGEN BWS_DEFA - ENDE 

double ROUND ( double inwert , int prezi )
{

	if ( inwert == 0.0 ) return 0.0 ;
	double outwert ;
	double dmult ;
	double dfaktor ;
	double ddiv ;
	double ddummy ;

    dfaktor = 1 ;
	dmult = 1 ;

	for ( int i = 0 ; i < prezi ; i ++ ) dmult *= 10.0 ;

    ddiv = dmult * 2.0 ;
	dfaktor /= ddiv ;

	if ( inwert > 0.0 )
		ddummy = inwert + dfaktor ;
	else
		ddummy = inwert - dfaktor ;


	inwert = ( long )(ddummy * dmult ) ;
	outwert = inwert / dmult ;
	return  outwert ;
}


BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	int k = 0 ;	
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		if ( i < 6 ) k = -1 ;
		if ( k == 0 )
		{
			// die ersten beiden Zeichen MUESSEN ZAHLEN sein
			if ( bufi[0] > '9' || bufi[0] < '0' ||bufi[1] > '9' || bufi[1] < '0' )
				k = - 1 ;
		}
		if ( k == 0 )
		{
			if ( bufi[2] > '9' || bufi[2] < '0' )
				k = 2 ;		// Typ 8 oder typ 10 moeglich

		}
		if ( k == 0 )
		{
			// nur noch Typ 6 erlaubt 
			if (    bufi[3] > '9' || bufi[3] < '0'
				 || bufi[4] > '9' || bufi[4] < '0'
				 || bufi[5] > '9' || bufi[5] < '0' )
			{
				k = - 1 ;
			}
			else
			{	// Gueltiger Typ 6  ......
				k = 1 ;
				i = 6 ;
				bufi[6] = '\0' ;
			}
		}
		if ( k == 2 )
		{
			if ( i < 8 )
			{	
				k = -1 ;
			}
			else
			{	// 4.,5.,7.,8. Zeichen MUESSEN ZAHLEN sein, 6.Zeichen MUSS Nicht-Zahl sein
				if (   bufi[3] > '9' || bufi[3] < '0' 
					 ||bufi[4] > '9' || bufi[4] < '0'
				     ||bufi[6] > '9' || bufi[6] < '0'
				     ||bufi[7] > '9' || bufi[7] < '0'
				     || !( bufi[5] >'9' || bufi[5]< '0')
				    )
				k = - 1 ;
			}
		}
		if ( k == 2 )
		{
			if ( bufi[8] > '9' || bufi[8] < '0' )
			{	// gueltiger Typ 8 
				k = 1 ;
				i = 8 ;
				bufi[8] = '\0' ;
			}
		
		}

		if ( k == 2 )
		{
			if ( i < 10 )
			{	
				k = -1 ;
			}
			else
			{	// 9.,10. Zeichen MUESSEN ZAHLEN sein
				if (   bufi[8] > '9' || bufi[8] < '0' 
					 ||bufi[9] > '9' || bufi[9] < '0'
			       )
				{
					k = -1 ;
				}
				else
				{
					k = 1 ;
					i = 10 ;
					bufi[10] = '\0' ;
				}
			}
		}
		if ( k < 1 )
		{
			bufi[0] = '\0' ;
			return FALSE ;
		}
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return FALSE ;
		}

		j = TRUE ;

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return FALSE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return FALSE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return FALSE ;
			}
			break ;
		case 2 :
			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return FALSE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return FALSE ;
				};
			}
			break ;

		}
	}
	return j ;

}
// 280812 A

// genau fuer die scansequenz (13)yymmdd(90)12345678 realisiert -> 18 Stellen 
BOOL CMy561lackDlg::scancheck(char * bufi )
{
	int i = (int) strlen(bufi) ;

	if ( strlen ( bufi ) < 17 )
		return FALSE ;

	if (   bufi[0] != '1'
		  || bufi[1] != '3'
		  || bufi[8] != '9'
		  || bufi[9] != '0'
	   )
	   return FALSE ;

	char hilf15 [15] ;

	hilf15[0] = bufi[6] ;
	hilf15[1] = bufi[7] ;
	hilf15[2] = bufi[4] ;
	hilf15[3] = bufi[5] ;
	hilf15[4] = bufi[2] ;
	hilf15[5] = bufi[3] ;
	hilf15[6] = '\0' ;

	if (! datumcheck( hilf15 ))
	{
		return FALSE ;
	}
	scandatum.Format("%s",_T(hilf15 )) ;

	hilf15[0] = bufi[10] ;
	hilf15[1] = bufi[11] ;
	hilf15[2] = bufi[12] ;
	hilf15[3] = bufi[13] ;
	hilf15[4] = bufi[14] ;
	hilf15[5] = bufi[15] ;
	hilf15[6] = bufi[16] ;
	hilf15[7] = bufi[17] ;
	hilf15[8] = '\0' ;

	scantour = atol ( hilf15 ) ;
	scantour = scantour / TourenFaktor ;	// 040912->170513

	scansequenz = TRUE ;

	v_rechdat = scandatum ;
	v_abgrdat = v_rechdat ;
	v_kunvon = "" ;
	v_kunbis = "" ;
	v_tourvon.Format("%d", scantour ) ;

	UpdateData(FALSE) ;
	scanactiv = TRUE ;
	scankaputt = FALSE ;

	return TRUE ;
}
// 280812 E


DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMy561lackDlg-Dialogfeld


CMy561lackDlg::CMy561lackDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMy561lackDlg::IDD, pParent)
    , m_bShowGripper(true)	// 111211 
    , m_szMinimum(0, 0)		// 111211
    , m_hGripper(NULL)		// 111211

	, v_mdnname(_T(""))
	, v_mandant(_T(""))
	, v_rechdat(_T(""))
	, v_abgrdat(_T(""))
	, v_kunvon(_T(""))
	, v_kunbis(_T(""))
	, v_tourvon(_T(""))
	, v_tourbis(_T(""))
	, v_info(_T(""))
	, v_kunvkrz(_T(""))
	, v_kunbkrz(_T(""))
	, v_checkr(FALSE)
	, v_checkl(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy561lackDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_MANDANT, m_mandant);
	DDX_Text(pDX, IDC_MANDANT, v_mandant);
	DDX_Control(pDX, IDC_RECHDAT, m_rechdat);
	DDX_Text(pDX, IDC_RECHDAT, v_rechdat);
	DDX_Control(pDX, IDC_ABGRDAT, m_abgrdat);
	DDX_Text(pDX, IDC_ABGRDAT, v_abgrdat);
	DDX_Control(pDX, IDC_KUNVON, m_kunvon);
	DDX_Control(pDX, IDC_KUNBIS, m_kunbis);
	DDX_Text(pDX, IDC_KUNVON, v_kunvon);
	DDX_Text(pDX, IDC_KUNBIS, v_kunbis);
	DDX_Control(pDX, IDC_TOURVON, m_tourvon);
	//	DDX_Text(pDX, IDC_TOURVON, v_tourvon);
	DDX_CBString(pDX, IDC_TOURVON, v_tourvon);


	DDX_Control(pDX, IDC_TOURBIS, m_tourbis);
	//	DDX_Text(pDX, IDC_TOURBIS, v_tourbis);
	DDX_CBString(pDX, IDC_TOURBIS, v_tourbis);
	DDX_Control(pDX, IDC_INFO, m_info);
	DDX_Text(pDX, IDC_INFO, v_info);
	DDX_Control(pDX, IDC_KUNVKRZ, m_kunvkrz);
	DDX_Text(pDX, IDC_KUNVKRZ, v_kunvkrz);
	DDX_Control(pDX, IDC_KUNBKRZ, m_kunbkrz);
	DDX_Text(pDX, IDC_KUNBKRZ, v_kunbkrz);
	DDX_Control(pDX, IDC_CHECKR, m_checkr);
	DDX_Check(pDX, IDC_CHECKR, v_checkr);
	DDX_Control(pDX, IDC_CHECKL, m_checkl);
	DDX_Check(pDX, IDC_CHECKL, v_checkl);
}

BEGIN_MESSAGE_MAP(CMy561lackDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_GETMINMAXINFO()		// 111211 
    ON_WM_SIZE()				// 111211
  	ON_BN_CLICKED(IDCANCEL, &CMy561lackDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CMy561lackDlg::OnBnClickedOk)
	ON_EN_KILLFOCUS(IDC_MANDANT, &CMy561lackDlg::OnEnKillfocusMandant)
	ON_EN_KILLFOCUS(IDC_RECHDAT, &CMy561lackDlg::OnEnKillfocusRechdat)
	ON_EN_KILLFOCUS(IDC_ABGRDAT, &CMy561lackDlg::OnEnKillfocusAbgrdat)
	ON_EN_KILLFOCUS(IDC_KUNVON, &CMy561lackDlg::OnEnKillfocusKunvon)
	ON_EN_KILLFOCUS(IDC_KUNBIS, &CMy561lackDlg::OnEnKillfocusKunbis)
	ON_CBN_SELCHANGE(IDC_TOURVON, OnCbnSelchangeTourvon)
	ON_CBN_SELCHANGE(IDC_TOURBIS, OnCbnSelchangeTourbis)
	ON_CBN_KILLFOCUS(IDC_TOURVON, &CMy561lackDlg::OnCbnKillfocusTourvon)
	ON_CBN_KILLFOCUS(IDC_TOURBIS, &CMy561lackDlg::OnCbnKillfocusTourbis)
	ON_BN_CLICKED(IDC_DRWAHL, &CMy561lackDlg::OnBnClickedDrwahl)
	ON_BN_CLICKED(IDC_CHECKR, &CMy561lackDlg::OnBnClickedCheckr)
	ON_BN_CLICKED(IDC_LDRWAHL, &CMy561lackDlg::OnBnClickedLdrwahl)
END_MESSAGE_MAP()


void CMy561lackDlg::AutoMove(int iID, double dXMovePct, double dYMovePct, double dXSizePct, double dYSizePct)	// 111211 
{ 
    ASSERT((dXMovePct + dXSizePct) <= 100.0);   // can't use more than 100% of the resize for the child 
    ASSERT((dYMovePct + dYSizePct) <= 100.0);   // can't use more than 100% of the resize for the child 
    SMovingChild s; 
    GetDlgItem(iID, &s.m_hWnd); 
    ASSERT(s.m_hWnd != NULL); 
    s.m_dXMoveFrac = dXMovePct / 100.0; 
    s.m_dYMoveFrac = dYMovePct / 100.0; 
    s.m_dXSizeFrac = dXSizePct / 100.0; 
    s.m_dYSizeFrac = dYSizePct / 100.0; 
    ::GetWindowRect(s.m_hWnd, &s.m_rcInitial); 
    ScreenToClient(s.m_rcInitial); 
    m_MovingChildren.push_back(s); 
} 
 



// CMy561lackDlg-Meldungshandler


void setdatum (  TIMESTAMP_STRUCT * ziel , CString quelle, short offset )
{
// Input zwingend "dd.mm.yyyy" oder blankmit dem offset gibt es nur stress wegen unsigned short-Werten
    char tag[] = {"01"};
    char mon[]  = {"01"};
    char jahr[]  = {"2000"};
   	int len = quelle.GetLength ();
	 if (len >=2)
	 {
		 memcpy (tag, quelle.Mid (0, 2), 2);
	 }
	 if (len >=5)
	 {
			 memcpy (mon, quelle.Mid (3, 2), 2);
	 }
	 if (len >=10)
	 {
		 memcpy (jahr, quelle.Mid (6, 4), 4);
	 }
	 else
	 {
		if (len >=10)
		{
			memcpy (jahr + 2 , quelle.Mid (6, 2), 2);
		}
	 }
	 ziel->year = atoi ( jahr ) ; if ( ziel->year > 2099 || ziel->year < 1900 ) ziel->year = 1999 ;
	 ziel->day = atoi ( tag ) ;   if ( ziel->day > 31 || ziel->day < 1 ) ziel->day = 1 ;
	 ziel->month = atoi ( mon ) ; if ( ziel->month > 12 || ziel->month < 1 ) ziel->month = 1 ;

	if ( offset != 0 )
	{	// offset abarbeiten

		ziel->day += offset ;
		if ( offset < 0 )
		{
			while ( ziel->day < 1 || ziel->day > 10000 )
			{
				ziel->month -= 1 ;

				if ( ziel->month == 2 )
				{
					if ( ziel->year == 2008 || ziel->year == 2012 || ziel->year == 2016 || ziel->year == 2020 )
						ziel->day += 29 ;
					else
						ziel->day += 28 ;
				}
				if ( ziel->month == 0 )
				{
					ziel->month = 12 ;
					ziel->year -= 1 ;
				}
				if ( ziel->month == 1 || ziel->month == 3 || ziel->month == 5 || ziel->month == 7 
					|| ziel->month == 8 || ziel->month == 10 || ziel->month == 12 )
				{
					ziel->day += 31 ;
				}

				if ( ziel->month == 4 || ziel->month == 6 || ziel->month == 9 || ziel->month == 11 )
				{
					ziel->day += 30 ;
				}
			}
		}
		if ( offset > 0 )
		{
			int unfertig = 1 ;
			while ( unfertig )
			{
				unfertig = 0 ;
// ################# Februar
				if ( ziel->month == 2 )
				{
					if ( ziel->year == 2008 || ziel->year == 2012 || ziel->year == 2016 || ziel->year == 2020 )
					{
						if ( ziel->day > 29 )
						{
							ziel->month = 3 ; ziel->day -= 29 ;
							unfertig = 1 ;
						}
					}
					else
					{
						if (ziel->day > 28 )
						{
							ziel->month = 3 ; ziel->day -= 28 ;
							unfertig = 1 ;
						}
					}
					continue ;
				}
// ################ Jahreswechsel
				if ( ziel->month == 12 )
				{
					if (ziel->day > 31 )
					{
						ziel->month = 1 ; ziel->year += 1 ; ziel->day -= 31 ;
							unfertig = 1 ;
					}
					continue ;
				}
// ############### grosse Monate
				if ( ziel->month == 1 || ziel->month == 3 || ziel->month == 5 || ziel->month == 7 
					|| ziel->month == 8 || ziel->month == 10  )
				{
					if (ziel->day > 31 )
					{
						ziel->month += 1 ; ziel->day -= 31 ;
						unfertig = 1 ;
					}
					continue ;
				}
// ############### kleine Monate	
				if ( ziel->month == 4 || ziel->month == 6 || ziel->month == 9 || ziel->month == 11 )
				{
					if ( ziel->day > 30 )
					{
						ziel->month += 1 ; ziel->day -= 30 ;
						unfertig = 1 ;
					}
					continue ;
				}
			}	// while unfertig
		}		// offset > 0 

	}			// offset != 0 

}

void CMy561lackDlg::systemdatum ( char *  ziel )
{
// Fuer Platz im "Ziel" ist jeder selber zust�ndig

time_t timer;
struct tm *ltime;

int jrhstart = 70 ;
int jrh1 = 1900 ;
int jrh2 = 2000 ;

 time (&timer);
 ltime = localtime (&timer);

 if (ltime->tm_year > 99 && ltime->tm_year < 200)
 {
	ltime->tm_year -=  100 ;
 }

 if (ltime->tm_year < 100)
 {
             if (ltime->tm_year < jrhstart)
             {
                  ltime->tm_year += jrh2;
             }
             else
             {
                  ltime->tm_year += jrh1;
             }
 }
 sprintf (ziel, "%02d.%02d.%04d", ltime->tm_mday,
                                   ltime->tm_mon + 1,
                                   ltime->tm_year);
}

BOOL CMy561lackDlg::OnInitDialog()
{
	CDialog::OnInitDialog();


	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

// A 111211
   // use the initial dialog size as the default minimum 
    if ((m_szMinimum.cx == 0) && (m_szMinimum.cy == 0)) 
    { 
        CRect rcWindow; 
        GetWindowRect(rcWindow); 
        m_szMinimum = rcWindow.Size(); 
    } 
 
    // keep the initial size of the client area as a baseline for moving/sizing controls 
    CRect rcClient; 
    GetClientRect(rcClient); 
    m_szInitial = rcClient.Size(); 

	double right ;
	double bottom ;
	double left ;
	double top ;

	POINT pointini ;
	pointini = rcClient.BottomRight();
 
    // create a gripper in the bottom-right corner 
    if (m_bShowGripper && ((GetStyle() & WS_THICKFRAME) != 0)) 
    { 
        SMovingChild s; 

		s.m_szInitial = rcClient.Size();	// 111211 111211 


        s.m_rcInitial.SetRect(-GetSystemMetrics(SM_CXVSCROLL), -GetSystemMetrics(SM_CYHSCROLL), 0, 0); 
        s.m_rcInitial.OffsetRect(rcClient.BottomRight());

		right = (double)s.m_rcInitial.right ;	// 111211
		bottom = (double)s.m_rcInitial.bottom ;	// 111211
		left = (double)s.m_rcInitial.left ;	// 111211
		top = (double)s.m_rcInitial.top ;	// 111211

        m_hGripper = CreateWindow(_T("Scrollbar"), _T("size"), WS_CHILD | WS_VISIBLE | SBS_SIZEGRIP, 
                                  s.m_rcInitial.left, s.m_rcInitial.top, s.m_rcInitial.Width(), s.m_rcInitial.Height(), 
                                  m_hWnd, NULL, AfxGetInstanceHandle(), NULL); 
        ASSERT(m_hGripper != NULL); 
        if (m_hGripper != NULL) 
        { 
            s.m_hWnd = m_hGripper; 
            s.m_dXMoveFrac = 1.0; 
            s.m_dYMoveFrac = 1.0; 
            s.m_dXSizeFrac = 0.0; 
            s.m_dYSizeFrac = 0.0; 
            m_MovingChildren.push_back(s); 
 
            // put the gripper first in the z-order so it paints first and doesn't obscure other controls 
            ::SetWindowPos(m_hGripper, HWND_TOP, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_SHOWWINDOW); 
        } 


/* ----> funzt irgendwie noch nicht
// AAAAAA Mehrfachnutzung der Struktur
// m_mandant
		m_mandant.GetRect ( rcClient );
		s.m_rcInitial.SetRect(rcClient.left, rcClient.top, rcClient.right,rcClient.bottom ); 

		s.m_rcInitial.OffsetRect (rcClient.BottomRight());

        s.m_dXMoveFrac = 0.0 ;	// (double)s.m_rcInitial.top / top ;
        s.m_dYMoveFrac = 0.0 ; // (double)s.m_rcInitial.left / left ;
        s.m_dXSizeFrac = 1.0 ;
        s.m_dYSizeFrac = 1.0 ; 
		s.m_hWnd = m_mandant; 
        m_MovingChildren.push_back(s); 
  // EEEEEE
< ------ */
    } 
 

// E 111211


	dbClass.opendbase (_T("bws"));

	char hilfe[20] ;
	systemdatum ( hilfe) ; 
	v_rechdat = _T(hilfe);
	v_abgrdat = _T(hilfe);

	mdn.mdn = 1 ;
	v_mandant = "1" ;

 	int i = ReadMdn ();



//	spnutzdruck = atoi ( sys_par_class.sys_par_holen ( "nutzdruck" ))  ;	// 261114
//	if ( spnutzdruck )
//	{

		sprintf ( Nutzer, "%s", globnutzer) ;
//	}
//	else
//		sprintf ( Nutzer,"");


	dnachkpreis = atoi ( sys_par_class.sys_par_holen ( "nachkpreis" ))  ;
	if ( dnachkpreis > 4 || dnachkpreis < 2 ) dnachkpreis = 2 ;

// 170513 
	TourenFaktor = atol ( sys_par_class.sys_par_holen ( "tourlang" ))  ;
	if ( TourenFaktor < 4L || TourenFaktor > 5L )
		TourenFaktor = 1000L;

	if ( TourenFaktor == 4L )
		TourenFaktor = 100L;

	if ( TourenFaktor == 5L )
		TourenFaktor = 1000L;





#ifdef NEU561
// 010612
	m_mandant.ShowWindow(SW_HIDE); m_mdnname.ShowWindow(SW_HIDE);m_abgrdat.ShowWindow(SW_HIDE);
	m_tourbis.ShowWindow(SW_HIDE);

	CWnd *Control1 = GetDlgItem (IDC_STATIC1);
	if (Control1 != NULL)
			Control1->ShowWindow( SW_HIDE);

	CWnd *Control3 = GetDlgItem (IDC_STATIC3);
	if (Control3 != NULL)
			Control3->ShowWindow( SW_HIDE);

	CWnd *Control7 = GetDlgItem (IDC_STATIC7);
	if (Control7 != NULL)
			Control7->ShowWindow( SW_HIDE);

// 150612
	CWnd *Control2 = GetDlgItem (IDC_STATIC2);
	Control2->SetWindowTextA( "Lieferdatum :") ;

	CWnd *Control6 = GetDlgItem (IDC_STATIC6);
	Control6->SetWindowTextA( "Tour     :") ;

	CDialog::SetWindowTextA("561Lack + 535A0") ;

#else


	CWnd *ControlL = GetDlgItem (IDC_CHECKL);
	if (ControlL != NULL)
			ControlL->ShowWindow( SW_HIDE);
	CWnd *ControlR = GetDlgItem (IDC_CHECKR);
	if (ControlR != NULL)
			ControlR->ShowWindow( SW_HIDE);

	CWnd *ControlDW = GetDlgItem (IDC_LDRWAHL);
	if (ControlDW != NULL)
			ControlDW->ShowWindow( SW_HIDE);

#endif
	


	v_checkl = TRUE ;	// 010612
	v_checkr = TRUE ;	// 010612

// 161211 A


	CString sztour ;

	int sqlstat = tou_class.openalltou ();
	sqlstat = tou_class.lesealltou();
	while(!sqlstat)
	{
	
			sztour .Format("%4.0d  %s                   ",tou.tou,tou.tou_bz);	// 280812 : 20 zeichen mehr 
				// hier haben wir jetzt die tour und k�nnen sir in die ListBox einf�gen
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURVON))->AddString(sztour.GetBuffer(0));
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURVON))->SetCurSel(0);
	
		sqlstat = tou_class.lesealltou () ;
	}

	sqlstat = tou_class.openalltou ();
	sqlstat = tou_class.lesealltou();
	while(!sqlstat)
	{
		sztour .Format("%4.0d  %s                    ",tou.tou,tou.tou_bz); // 280812 : 20 zeichen mehr 
			// hier haben wir jetzt die tour und k�nnen sie in die ListBox einf�gen
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURBIS))->AddString(sztour.GetBuffer(0));
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURBIS))->SetCurSel(0);
		sqlstat = tou_class.lesealltou () ;
	}

// 161211 E


	scansequenz = FALSE ;	// 280812 : lt. A.Lackmann : beim Start alles erlaubt, danachn NUR scan erlaubt
	scanactiv = FALSE ;		// 280812
	scankaputt = FALSE ;

	UpdateData(FALSE);

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten

}


// 111211

void CMy561lackDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)  
{ 
    CDialog::OnGetMinMaxInfo(lpMMI); 
 
    if (lpMMI->ptMinTrackSize.x < m_szMinimum.cx) 
        lpMMI->ptMinTrackSize.x = m_szMinimum.cx; 
    if (lpMMI->ptMinTrackSize.y < m_szMinimum.cy) 
        lpMMI->ptMinTrackSize.y = m_szMinimum.cy; 
} 
 
void CMy561lackDlg::OnSize(UINT nType, int cx, int cy)  
{ 
    CDialog::OnSize(nType, cx, cy); 
 
// 111211    int iXDelta = cx - m_szInitial.cx; 
// 111211    int iYDelta = cy - m_szInitial.cy; 
    HDWP hDefer = NULL; 
    for (MovingChildren::iterator p = m_MovingChildren.begin();  p != m_MovingChildren.end();  ++p) 
    { 
        if (p->m_hWnd != NULL) 
        { 
            CRect rcNew(p->m_rcInitial); 
			int iXDelta = cx - p->m_szInitial.cx;		// 111211 
			int iYDelta = cy - p->m_szInitial.cy;		// 111211

			rcNew.OffsetRect(int(iXDelta * p->m_dXMoveFrac), int(iYDelta * p->m_dYMoveFrac)); 
            rcNew.right += int(iXDelta * p->m_dXSizeFrac); 
            rcNew.bottom += int(iYDelta * p->m_dYSizeFrac); 
            if (hDefer == NULL) 
                hDefer = BeginDeferWindowPos(m_MovingChildren.size()); 
            UINT uFlags = SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER; 
            if ((p->m_dXSizeFrac != 0.0) || (p->m_dYSizeFrac != 0.0)) 
                uFlags |= SWP_NOCOPYBITS; 
            DeferWindowPos(hDefer, p->m_hWnd, NULL, rcNew.left, rcNew.top, rcNew.Width(), rcNew.Height(), uFlags); 
        } 
    } 
    if (hDefer != NULL) 
        EndDeferWindowPos(hDefer); 
 
    if (m_hGripper != NULL) 
        ::ShowWindow(m_hGripper, (nType == SIZE_MAXIMIZED) ? SW_HIDE : SW_SHOW); 
} 

// E 111211


int CMy561lackDlg::ReadMdn (void) 
{
    int retcode = 0 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
			retcode = 1 ;
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
			retcode = 0 ;
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		retcode = 0 ;
	}
//		UpdateData (FALSE) ;
		return retcode ;
}

void CMy561lackDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CMy561lackDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CMy561lackDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMy561lackDlg::OnBnClickedCancel()
{
	OnCancel();
}

void CMy561lackDlg::OnBnClickedOk()
{

scanactiv = FALSE ;	// 280812 : neue Eingabe erlaubt
if ( scankaputt )	// Nur durch korrekten scan reaktivierbar
	return ;
int eingabeok = 1 ;

	UpdateData (TRUE) ;

// Mandant ok ?
	int i = m_mandant.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	v_mandant.Format("%d",mdn.mdn);
	if ( ! ReadMdn())
	{
		eingabeok = 0;

		CWnd *Control = GetDlgItem (IDC_MANDANT);
		if (Control != NULL)
		{
			Control->SetFocus ();
			return;
		}
	}

// Rechdat ok ?
	i = m_rechdat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_rechdat.Format("%s",_T(bufh));
		}
		else
			i = 0 ;
	}
	if ( !i)
	{
		eingabeok = 0;
		CWnd *Control = GetDlgItem (IDC_RECHDAT);
		if (Control != NULL)
		{
			Control->SetFocus ();
			return;
		}

	}
#ifdef NEU561
	v_abgrdat = v_rechdat ;	// immer identisch !?
#else
// Abgrenzdatum ok ?
	i = m_abgrdat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_abgrdat.Format("%s",_T(bufh));
		}
		else
			i = 0 ;
	}
	if ( !i)
	{
		eingabeok = 0;
		CWnd *Control = GetDlgItem (IDC_ABGRDAT);
		if (Control != NULL)
		{
			Control->SetFocus ();
			return;
		}
	}
#endif
// Kunden- und Tour-Bedingungen laden

	i = m_kunvon.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	if (i)
		lsk.v_kun = atol ( bufh );
	else
		lsk.v_kun = 0;


	i = m_kunbis.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	if (i)
		lsk.bis_kun = atol ( bufh );
	else
		lsk.bis_kun = 0;

	if ( lsk.bis_kun == 0 && lsk.v_kun == 0)
	{
		lsk.v_kun = 1 ;
		lsk.bis_kun = 99999999 ;
	}
	else
	{
		if ( lsk.bis_kun == 0 )
			lsk.bis_kun = lsk.v_kun;
	}
	
/* -----> 161211 
	i = m_tourvon.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	if (i)
		lsk.v_tou = atol ( bufh );
	else
		lsk.v_tou = 0;


	i = m_tourbis.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;
	if (i)
		lsk.bis_tou = atol ( bufh );
	else
		lsk.bis_tou = 0;

	if ( lsk.bis_tou == 0 && lsk.v_tou == 0)
	{
		lsk.v_tou = 0 ;
		lsk.bis_tou = MAXTOUNR ;
	}
	else
	{
		if ( lsk.bis_tou == 0 )
			lsk.bis_tou = lsk.v_tou;
	}
< ---- */


// 161211 A


 	sprintf(bufh,"%s",v_tourvon.TrimRight ());
	i = (int) strlen ( bufh );
	// fixe formatierung : 4 stellen und 2 blanks , danach folgt ein text
	if ( i > 0  )
	{
		if ( i < 5 )
		{
			bufh[i ] = '\0' ;
		}
		else
		{
			bufh[4] = '\0' ;
		}
	}
	else
		i = 0 ;
		
	if (i)
		lsk.v_tou = atol ( bufh );
	else
		lsk.v_tou = 0;

 	sprintf(bufh,"%s",v_tourbis.TrimRight() ) ;
	i = (int) strlen ( bufh );
	// fixe formatierung : 4 stellen und 2 blanks , danach folgt ein text
	if ( i > 0  )
	{
		if ( i < 5 )
		{
			bufh[i ] = '\0' ;
		}
		else
		{
			bufh[4] = '\0' ;
		}
	}
	else
		i = 0 ;

	if (i)
		lsk.bis_tou = atol ( bufh );
	else
		lsk.bis_tou = 0;


	if ( lsk.bis_tou == 0 && lsk.v_tou == 0)
	{
		lsk.v_tou = 0 ;
		lsk.bis_tou = MAXTOUNR ;
	}
	else
	{
		if ( lsk.bis_tou == 0 )
			lsk.bis_tou = lsk.v_tou;
	}

// 161211 E

#ifdef NEU561
// 010612
	if ( v_checkl == TRUE )	
	{
		ListenDruck () ;
	}

	if ( v_checkr == TRUE )	
	{
		StapelDruck () ;
	}

#else

	StapelDruck () ;
#endif

	//	OnOK();
}

void CMy561lackDlg::OnEnKillfocusMandant()
{
	UpdateData (TRUE) ;


// 280812 :scancheck nicht integriert, da bei neu-Ablauf inaktiv

	int i = m_mandant.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;

	v_mandant.Format("%d",mdn.mdn);

	if ( ! ReadMdn())
	{
		UpdateData (FALSE) ;
		CWnd *Control = GetFocus ();
		Control = GetNextDlgTabItem (Control, TRUE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
	}
	else
		UpdateData (FALSE) ;
}

void CMy561lackDlg::OnEnKillfocusRechdat()
{

	if ( scanactiv )
	{	scanactiv = FALSE ;
		return ;
	}
	CString hilfedat ;
	hilfedat = v_rechdat ;

	UpdateData(TRUE) ;
	int	i = m_rechdat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;

// 280812 A


	if (i) 
	{
		if ( scancheck(bufh) )
		{

			CWnd *Control = GetDlgItem (IDOK);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}

			return ;
		}

	}
	if ( scansequenz )
	{

		if ( v_rechdat != hilfedat )
		{
			MessageBox("Ung�ltige Eingabe", " ", MB_OK|MB_ICONSTOP);
			scankaputt = TRUE ;
		}
		CWnd *Control = GetFocus ();
		Control = GetNextDlgTabItem (Control, TRUE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
		return ;
	}
// 280812 E

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_rechdat.Format("%s",_T(bufh));
		}
		else
			i = 0 ;
	}
	if ( !i)
	{

		MessageBox("Ung�ltiges Datum!", " ", MB_OK|MB_ICONSTOP);

		CWnd *Control = GetFocus ();
		Control = GetNextDlgTabItem (Control, TRUE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}

	}
	UpdateData (FALSE) ;
}

void CMy561lackDlg::OnEnKillfocusAbgrdat()
{
	if ( scanactiv )
	{
		scanactiv = FALSE ;
		return ;
	}
	UpdateData(TRUE) ;
	int	i = m_abgrdat.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;

// 280812 A

	if (i) 
	{
		if ( scancheck(bufh) )
		{
			UpdateData(FALSE) ;
			return ;
		}

	}
	if ( scansequenz )
	{
		MessageBox("Ung�ltige Eingabe", " ", MB_OK|MB_ICONSTOP);
		scankaputt = TRUE ;
		CWnd *Control = GetFocus ();
		Control = GetNextDlgTabItem (Control, TRUE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
		return ;
	}
// 280812 E

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_abgrdat.Format("%s",_T(bufh));
		}
		else
			i = 0 ;
	}
	if ( !i )	
	{
		MessageBox("Ung�ltiges Datum!", " ", MB_OK|MB_ICONSTOP);
		CWnd *Control = GetFocus ();
		Control = GetNextDlgTabItem (Control, TRUE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
	}
	UpdateData (FALSE) ;
}

void CMy561lackDlg::OnEnKillfocusKunvon()
{

	if ( scanactiv )
	{
		scanactiv = FALSE ;
		return ;
	}
	CString hilfekun ;
	hilfekun = v_kunvon ;
	UpdateData (TRUE) ;

	int i = m_kunvon.GetLine(0,bufh,500);
	bufh[i] = '\0' ;

// 280812 A

	if (i) 
	{
		if ( scancheck(bufh) )
		{
			UpdateData(FALSE) ;
			return ;
		}

	}
	if ( scansequenz )
	{
		if ( hilfekun != v_kunvon )
		{
			MessageBox("Ung�ltige Eingabe", " ", MB_OK|MB_ICONSTOP);
			scankaputt = TRUE ;
		}
		CWnd *Control = GetFocus ();
		Control = GetNextDlgTabItem (Control, TRUE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
		return ;
	}
// 280812 E

	if (i)	kun.kun = atol ( bufh );
	else kun.kun = -33 ;

	if ( kun.kun > 0 )
	{
		v_kunvon.Format("%d",kun.kun);
		kun.mdn = mdn.mdn ;
		kun_class.openkun();
		if (! kun_class.lesekun())
			v_kunvkrz.Format("%s",_T(kun.kun_krz1));
		else
			v_kunvkrz = "" ;
	}
	else
	{
			v_kunvkrz = "" ;
	}

	UpdateData (FALSE) ;
}


void CMy561lackDlg::OnEnKillfocusKunbis()
{

	if ( scanactiv )
	{
		scanactiv = FALSE ;
		return ;
	}
	CString hilfekun ;
	hilfekun = v_kunbis ;

	UpdateData (TRUE) ;

	int i = m_kunbis.GetLine(0,bufh,500);
	bufh[i] = '\0' ;

// 280812 A


	if (i) 
	{
		if ( scancheck(bufh) )
		{
			UpdateData(FALSE) ;
			return ;
		}

	}
	if ( scansequenz )
	{
		if ( hilfekun != v_kunbis )
		{
			MessageBox("Ung�ltige Eingabe", " ", MB_OK|MB_ICONSTOP);
			scankaputt = TRUE ;
		}
		CWnd *Control = GetFocus ();
		Control = GetNextDlgTabItem (Control, TRUE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
		return ;
	}
// 280812 E


	if (i)	kun.kun = atol ( bufh );
	else kun.kun = -33 ;

	if ( kun.kun > 0 )
	{
		v_kunbis.Format("%d",kun.kun);
		kun.mdn = mdn.mdn ;
		kun_class.openkun();
		if (! kun_class.lesekun())
			v_kunbkrz.Format("%s",_T(kun.kun_krz1));
		else
			v_kunbkrz = "" ;
	}
	else
	{
			v_kunbkrz = "" ;
	}

	UpdateData (FALSE) ;
}

void CMy561lackDlg::OnBnClickedDrwahl()
{
	char s1[199] ;
//	char s2[199] ;

	if ( strlen( Nutzer ) > 1 )	// 261114
		sprintf ( s1, "dr561.exe -NUTZER %s -WAHL 1", Nutzer );
	else
		sprintf ( s1, "dr561.exe -WAHL 1" );

	int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );

//	sprintf ( s1, "%s\\format\\ll\\T53100.lsp", getenv("BWS") ) ;
//    sprintf ( s2, "%s\\format\\ll\\T53101.lsp", getenv("BWS") ) ;
//	CopyFile ( s1, s2, FALSE ) ;	// immer ueberschreiben 
}




BOOL CMy561lackDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
					CWnd *xWnd ;
					xWnd = GetFocus ();
					if(xWnd == GetDlgItem (IDOK)) {OnBnClickedOk() ; return TRUE ;};
					if(xWnd == GetDlgItem (IDCANCEL)) {	OnBnClickedCancel ();	return TRUE ;};
					if(xWnd == GetDlgItem (IDC_DRWAHL)){OnBnClickedDrwahl() ; return TRUE ;};


				if (OnReturn ())
				{
					return TRUE;
				}
			}
/*-----
			else if (pMsg->wParam == VK_TAB)
			{

				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (OnKeyup ())
				{
					return TRUE;
				}
			}
< ---- */

// 161211 A
			else if (pMsg->wParam == VK_DOWN)
                  if (NoListCtrl (GetFocus ()))
                   {
					NextDlgCtrl();
					return TRUE;
                  }
			else if (pMsg->wParam == VK_UP)
                     if (NoListCtrl (GetFocus ()))
                     {
						PrevDlgCtrl();
	 					cWnd = GetFocus ();
					 	GetNextDlgTabItem(cWnd ,FALSE) ;
						return TRUE;
                     }
// 161211 E

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				OnCancel () ;
				return TRUE;
			}
	}

//return CDbPropertyPage::PreTranslateMessage(pMsg);
//  return CFormView::PreTranslateMessage(pMsg);
//	return FALSE ;
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CMy561lackDlg::NoListCtrl (CWnd *cWnd)
{
    if (((CComboBox *) cWnd->GetParent ())->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CComboBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CListBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    return TRUE;
}


BOOL CMy561lackDlg::OnReturn()
{
	CWnd *Control = GetFocus ();


	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	return TRUE;
	}
	return FALSE;
}

BOOL CMy561lackDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();
		Control = GetNextDlgTabItem (Control, TRUE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		return TRUE;
		}
	return FALSE;
}

// 161211 A

void CMy561lackDlg::OnCbnSelchangeTourvon()
{

	char ibufh[400] ;

	if ( scanactiv )
	{
		scanactiv = FALSE ;
		return ;
	}
	CString hilfetour ;
	hilfetour = v_tourvon ;

	UpdateData(TRUE) ;


// 280812 A 
		sprintf ( bufh, "%s", v_tourvon.TrimRight() );
		int i = (int) strlen ( bufh );
		if (i) 
		{
			if ( scancheck(bufh) )
			{
				UpdateData(FALSE) ;
				return ;
			}

		}
		if ( scansequenz )
		{
			if ( hilfetour != v_tourvon )
			{
				MessageBox("Ung�ltige Eingabe", " ", MB_OK|MB_ICONSTOP);
				scankaputt = TRUE ;
			}
			CWnd *Control = GetFocus ();
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return ;
		}
// 280812 E


	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURVON))->GetCurSel();
	CString bufc;
	((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURVON))->GetLBText(nCurSel, bufc);

	sprintf(ibufh,"%s",bufc.TrimRight()) ;

	// fixe formatierung : 4 stellen und 2 blanks , danch folgt ein text
/*	int 280812 */ i = (int)strlen ( ibufh) ;
	if ( i )
	{
		if ( i < 5 )
		{
			ibufh[i] = '\0' ;
		}
		else
		{
			ibufh[4] = '\0' ;
		}
		tou.tou = atol ( ibufh ) ;
		i = tou_class.opentou();
		i = tou_class.lesetou();
	}
	
}

void CMy561lackDlg::OnCbnKillfocusTourvon()
{

// 280812 A

	if ( scanactiv )
	{
		scanactiv = FALSE ;
		return ;
	}
	CString hilfetour ;
	hilfetour = v_tourvon ;

	UpdateData(TRUE) ;

// 280812 A
		sprintf ( bufh, "%s", v_tourvon.TrimRight() );
		int i = (int) strlen ( bufh );
		if (i) 
		{
			if ( scancheck(bufh) )
			{
				UpdateData(FALSE) ;
				return ;
			}

		}
		if ( scansequenz )
		{
			if ( hilfetour != v_tourvon )
			{
				MessageBox("Ung�ltige Eingabe", " ", MB_OK|MB_ICONSTOP);
				scankaputt = TRUE ;
			}
			CWnd *Control = GetFocus ();
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return ;
		}
// 280812 E

	bufh[0] = '\0' ;
	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURVON))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURVON))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.TrimRight()) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : 4 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 5 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[4] = '\0' ;
			}
			tou.tou = atol ( bufh ) ;
			i = tou_class.opentou();
			i = tou_class.lesetou();
			if ( i )
			{
//				v_tourvon.Format("                   ");
			}
			else
			{
			}

		}
		else
		{
//				v_tourvon.Format("                   ");
		}
	}
	else
	{
// 150612
		if ( nCurSel == -1 )
		{
			long touhilf  = 0 ;
			sprintf ( bufh, "%s", v_tourvon.TrimRight() );
			int i = (int) strlen ( bufh );
			if (i)	touhilf = atol ( bufh );
			else touhilf = -33 ;
			if ( touhilf > TourenFaktor )	// 170513 1000 -> TourenFaktor
			{
				touhilf = touhilf / TourenFaktor ;
				v_tourvon.Format("%d", touhilf);
			}

		}

		sprintf ( bufh, "%s", v_tourvon.TrimRight() );
		int i = (int) strlen ( bufh );
		// fixe formatierung : 4 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 5 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[4] = '\0' ;
			}
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%4.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURVON))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURVON))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURVON))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.TrimRight()) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 4 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 5 )
				{
					bufh[i] = '\0' ;
				}
			}
			else
			{
				bufh[4] = '\0' ;
			}
			tou.tou = atol ( bufh ) ;
			i = tou_class.opentou();
			i = tou_class.lesetou();
			if ( i )
			{
//				v_tourvon.Format("                   ");
			}
			else
			{
				v_tourvon.Format(bufx.GetBuffer(0));
			}

		}
		else
		{
//			v_tourvon.Format("                   ");
		}
	}
	UpdateData(FALSE);

}

void CMy561lackDlg::OnCbnSelchangeTourbis()
{

	char ibufh[400] ;

	if ( scanactiv )
	{
		scanactiv = FALSE ;
		return ;
	}


	UpdateData(TRUE) ;
//	int i = m_Combo1.GetLine(0,bufh,9) ;


	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURBIS))->GetCurSel();
	CString bufc;
	((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURBIS))->GetLBText(nCurSel, bufc);

	sprintf(ibufh,"%s",bufc.TrimRight()) ;

	// fixe formatierung : 4 stellen und 2 blanks , danch folgt ein text
	int i = (int)strlen ( ibufh) ;
	if ( i )
	{
		if ( i < 5 )
		{
			ibufh[i] = '\0' ;
		}
		else
		{
			ibufh[4] = '\0' ;
		}
		tou.tou = atol ( ibufh ) ;
		i = tou_class.opentou();
		i = tou_class.lesetou();
	}
	
}

void CMy561lackDlg::OnCbnKillfocusTourbis()
{
// deaktiviert fuer Neu-Ablauf

	if ( scanactiv )
	{
		scanactiv = FALSE ;
		return ;
	}


	UpdateData(TRUE) ;

// 280812 A
		sprintf ( bufh, "%s", v_tourbis.TrimRight() );
		int i = (int) strlen ( bufh );
		if (i) 
		{
			if ( scancheck(bufh) )
			{
				UpdateData(FALSE) ;
				return ;
			}

		}
		if ( scansequenz )
		{
			MessageBox("Ung�ltige Eingabe", " ", MB_OK|MB_ICONSTOP);
			scankaputt = TRUE ;
			CWnd *Control = GetFocus ();
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return ;
		}
// 280812 E

	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURBIS))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURBIS))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.TrimRight());
		int i = (int) strlen ( bufh );
		// fixe formatierung : 4 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 5 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[4] = '\0' ;
			}
			tou.tou = atol ( bufh ) ;
			i = tou_class.opentou();
			i = tou_class.lesetou();
			if ( i )
			{
//				v_tourbis.Format("                   ");
			}
			else
			{
			}

		}
		else
		{
//				v_tourbis.Format("                   ");
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_tourbis.TrimRight() ) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : 4 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 5 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[4] = '\0' ;
			}
		}
		long hilfe = atol ( bufh ) ;
		nCurSel = -1 ;
		sprintf ( bufh, "%4.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURBIS))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURBIS))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_TOURBIS))->GetLBText(nCurSel, bufx);
			sprintf(bufh,"%s",bufx.TrimRight()) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 4 stellen und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 5 )
				{
					bufh[i] = '\0' ;
				}
			}
			else
			{
				bufh[4] = '\0' ;
			}
			tou.tou = atol ( bufh ) ;
			i = tou_class.opentou();
			i = tou_class.lesetou();
			if ( i )
			{
//				v_tourbis.Format("                   ");
			}
			else
			{
				v_tourbis.Format(bufx.GetBuffer(0));
			}

		}
		else
		{
//			v_tourbis.Format("                   ");
		}
	}
	UpdateData(FALSE);

}

// 161211 E

// 0106512 A
void CMy561lackDlg::ListenDruck ()
{

 char s1[200] ;
 char s2[200] ;
  sprintf ( s1 , "%s\\535A0.prm", getenv ( "TMPPATH" ) ) ;
  FILE * fp2 ;

	fp2 = fopen ( s1 , "w" ) ;
	if ( fp2 == NULL )
	{
		return  ;
	}

	sprintf ( s2, "NAME 535A0\n" ) ;
	fputs ( s2 , fp2 );
	fputs ( "LABEL 0\n" , fp2 ) ;

	fputs ( "ANZAHL 1\n" , fp2 ) ;
	
	
//	int ix = 0 ;
//	char *p = bws_default ("535A0_druck") ;
//	if ( p != NULL ) ix = atoi(p) ;
//	sprintf ( s2, "DRUCK %01d\n" , ix  ) ;
//	fputs ( s2 , fp2 ) ;

	sprintf ( s2, "DRUCK 0\n" ) ;	// kein Druckerdialog
	fputs ( s2 , fp2 ) ;


// 150812

	if ( strlen ( globnutzer ) > 1 ) 
	{
		sprintf ( s2, "NUTZER %s\n",  globnutzer   ) ;
		fputs ( s2 , fp2) ;
	}


//	sprintf ( s2, "mdn %01d %01d\n", imdn , imdn   ) ;
	sprintf ( s2, "mdn 1 1\n" ) ;
	fputs ( s2 , fp2 ) ;

	char crechdat[22] ;
	int	i = m_rechdat.GetLine(0,crechdat,19) ;
	crechdat[i] = '\0' ;
	sprintf ( s2, "lieferdat %s %s\n", crechdat , crechdat   ) ;
	fputs ( s2 , fp2) ;


	long vhitou ;
	long bhitou ;
	if ( lsk.bis_tou == MAXTOUNR && lsk.v_tou == 0 )
	{
		vhitou = lsk.v_tou ;
		bhitou = lsk.bis_tou ;
	}
	else
	{
		vhitou = lsk.v_tou * TourenFaktor ;	// 170513 1000 -> TourenFaktor
		bhitou = (lsk.bis_tou * TourenFaktor)  + ( TourenFaktor - 1L ) ;
	}
	sprintf ( s2, "tou %d %d\n", vhitou , bhitou   ) ;
	fputs ( s2 , fp2) ;

	fclose ( fp2) ;

	sprintf ( s2 , "dr70001.exe -datei \"%s\" " , s1 );
	int ex_code = ProcWaitExec( (char *)s2, SW_SHOW, -1, 0, -1, 0 );


//	sprintf ( s1, "%s\\format\\ll\\T53100.lsp", getenv("BWS") ) ;
//    sprintf ( s2, "%s\\format\\ll\\T53101.lsp", getenv("BWS") ) ;
//	CopyFile ( s1, s2, FALSE ) ;	// immer ueberschreiben 

}
// 010612 E

void CMy561lackDlg::StapelDruck ()
{

	int gutpoint = 0 ;	// Zeiger auf gute Kunden
	int badpoint = 0 ;	// Zeiger auf schlechte Kunden 
	int maxlspoint = 0 ; // Ende der ls-matrix

	double iof_po ;
	double iof_ek ;
	double iof_po_euro ;
	double iof_ek_euro ;
	double igew ;
	double ibrutto ;
	double hime ;

	int i ;


	for ( i = 0 ; i < IMAXDIM ; i++ )
		kungutmat[i] = -1 ;

	for ( i = 0 ; i < IMAXDIM ; i++ )
		kunbadmat[i] = -1 ;

	lsk.mdn = mdn.mdn ;
 	setdatum ( &lsk.lieferdat , v_abgrdat , 0 ) ;

	int istatus  ;
// Es werden nur Kunden-LS bearbeitet

	lsk.kun_fil = 0 ;	// Kunden-Stapel

//		kun.tou = (long) stapnu ;
	setdatum ( & lsk.vondate , v_abgrdat ,  -10  ) ;
	setdatum ( & lsk.bisdate , v_abgrdat , 0 ) ;

	istatus = 0 ;
	if ( lsk.v_kun > lsk.bis_kun || lsk.v_tou > lsk.bis_tou )
		istatus = -1 ;	// sinnlose Eingaben ergeben sinnlose Ergebnisse

	if ( !istatus )istatus = lsk_class.openkbeding() ;
	if ( !istatus ) istatus = lsk_class.lesekbeding () ;

	if ( istatus )
	{
		   MessageBox (_T("Datensatz nicht vorhanden"), NULL, MB_OK | MB_ICONERROR); 
		   return ;
	}

//	1. Runde : lesen der LS-Saetze 
// Bewertungsbasis : nur LS, da ja ein Auftrag "gleichzeitig" gesplittet wird
	i = 0 ;
	while ( ( ! istatus ) && ( i < IMAXDIM ) )
	{

		schlussmat[i] = lsk.ls ;
		schluss2mat[i] = lsk.kun ;
		eigmat[i]      = lsk.ls_stat ;
		istatus = lsk_class.lesekbeding () ;
		i ++ ;
		eigmat[i]      = -1 ;	// nachtr�gliche Initialisierung
		schluss2mat[i] = -1 ;	// nachtr�gliche Initialisierung
		schlussmat[i]  = -1 ;	// nachtr�gliche Initialisierung
	}

// 2. Runde : LS-Saetze bei Bedarf updaten 
	int kunstart = 0 ;			// startpos Kunde im Array
    int kunende = 0 ;			// Endpos   Kunde im Array
	long altkunde = -2 ;		// Gruppierungskunde
	int kunstat = 0 ;	// 0 = neutral ; 1 = zu tun ; 2 = sperren

	maxlspoint = i ;

	v_info.Format( " %d Belege bearbeiten " , maxlspoint ) ;

	UpdateData(FALSE);

	i = 0 ;
	altkunde = schluss2mat[0] ;
	while (  i < ( maxlspoint + 1 ) )
	{
//		if ( eigmat[i] == -1 )	break ;

		if ( altkunde == schluss2mat[i] )
		{		// innerhalb eines Kunden
			if ( eigmat[i] == 6 )	// neutral
				{ i++ ; continue ;};
			if ( eigmat[i] == 2|| eigmat[i] == 22 )	// sperren
				{ i++ ; kunstat = 2 ; continue ;};
			if ( eigmat[i] == 3|| eigmat[i] == 4 || eigmat[i] == 5 )	// da gibts was zu tun
				{ i++ ; if ( kunstat !=2 )kunstat = 1 ; continue ;};
			i++;	// den letzten hochschalten
		}
		else
		{		// Kundenwechsel
			kunende = i ;
			if ( kunstat == 2 )
			{
				kunbadmat[badpoint] = altkunde ;	// in Sperrliste eintragen
				badpoint ++ ;
			}
			if ( kunstat == 1 )	// es gibt was zu tun ....
			{
				for ( i = kunstart ; i < kunende ; i ++ )
				{
					if ( eigmat[i] == 3 || eigmat[i] == 4 )
					{
							
// ===============================================================================================
// LS aktualisieren + updaten, lief_retba erstellen
						lsk.ls = schlussmat[i] ;
						lsk.mdn = mdn.mdn ;
						lsk.fil =  0 ;
						dbClass.beginwork() ;
						istatus = lsk_class.openlsk() ;
						if ( ! istatus )
						{
							istatus = lsk_class.leselsk () ;
						}
						if ( istatus )
						{
							dbClass.rollbackwork() ;
							kunbadmat[badpoint] = altkunde ;	// in Sperrliste eintragen
							badpoint ++ ;
							break ;	// der Kunde wird abgebrochen

//							eigmat[i] = -2 ;	// Marker : Problem
//							i ++ ;
//							continue ;
						}
						if ( lsk.ls_stat == 5 || lsk.ls_stat == 6 )
						{							// da schafft jemand parallel !!!, ist aber zun�chst unkritisch
							dbClass.rollbackwork() ;
							continue ;
						}
						if ( lsk.ls_stat != 3 && lsk.ls_stat != 4 )	// Hier darf eigentlich nur noch 3 und 4 ankommen
						{
							dbClass.rollbackwork() ;
							kunbadmat[badpoint] = altkunde ;	// in Sperrliste eintragen
							badpoint ++ ;
							break ;	// der Kunde wird abgebrochen

						}
						if ( ! istatus )
						{
							lsp.ls = schlussmat[i] ;
							lsp.mdn = mdn.mdn ;
							lsp.fil =  0 ;
							istatus = lsp_class.openlsp () ;
						}
						if ( ! istatus ) 
						{
							istatus = lsp_class.leselsp () ;
						}
						if ( !istatus ) 
						{
							iof_po = iof_ek = iof_po_euro = iof_ek_euro = igew = ibrutto = 0.0 ;
							kun.kun = lsk.kun ;
							kun.mdn = mdn.mdn ;
							istatus = kun_class.openkun() ;
							if ( ! istatus )
								istatus = kun_class.lesekun () ;
							while ( ! istatus )
							{
								if ( a_bas.me_einh == 2 )
									hime = lsp.lief_me ;
								else
									hime = lsp.lief_me * a_bas.a_gew ;

								if (( kun.sw_rab[0] == 'J' ) && ( a_bas.sw > 0.001 ))
									hime = hime * ((100 - a_bas.sw) / 100.0 ) ;

								iof_po += ROUND ( hime * lsp.ls_vk_pr , dnachkpreis ) ;
									iof_ek += ROUND ( hime * lsp.ls_lad_pr , dnachkpreis ) ;

								iof_po_euro += ROUND ( hime * lsp.ls_vk_euro , dnachkpreis ) ;
								iof_ek_euro += ROUND ( hime * lsp.ls_lad_euro , dnachkpreis ) ;
								if ( ! strncmp( lsp.lief_me_bz,"kg",2 ) 
										|| ! strncmp( lsp.lief_me_bz,"KG",2)
										|| ! strncmp( lsp.lief_me_bz, "Kg",2))
									igew += lsp.lief_me ;	// das ist wohl noch ein FES-Ablauf ?!

								ibrutto +=  ROUND ( hime , 3 ) ;

								istatus = lsp_class.leselsp () ;
								// summieren und updaten
							}
							lsk.brutto = ibrutto ;
							lsk.gew = igew ;
							lsk.of_po = iof_po ;
							lsk.of_ek = iof_ek ;
							lsk.of_po_euro = iof_po_euro ;
							lsk.of_ek_euro = iof_ek_euro ;
//  komplettieren
							lsk.inka_nr = kun.kun ;
							if ( kun.kun_typ == 4 || kun.kun_typ == 5 || kun.kun_typ == 7 )
							if ( kun.inka_nr > 0 )
								lsk.inka_nr = kun.inka_nr ;
							if ( lsk.kopf_txt > 0 )
							{
								// dann nichts mehr tun ......
							}
							else
							{
								if ( kun.ls_kopf_txt > 0 )
									lsk.kopf_txt = kun.ls_kopf_txt ;
							}
							if ( lsk.fuss_txt > 0 )
							{
									// dann nichts mehr tun ......
							}
							else
							{
								if ( kun.ls_fuss_txt > 0 )
									lsk.fuss_txt = kun.ls_fuss_txt ;
							}
							if ( lsk.fuss_txt == 0 || lsk.kopf_txt == 0 )
							{
								sprintf ( kunbrtxtzu.kun_bran2 ,"%s", kun.kun_bran2 ) ;
								int i = kunbrtxtzu_class.lesebrantext() ;
								if ( !i)
								{
									if ( lsk.kopf_txt == 0 && kunbrtxtzu.kopf_txt > 0 )
										lsk.kopf_txt = kunbrtxtzu.kopf_txt ;

									if ( lsk.fuss_txt == 0 && kunbrtxtzu.fuss_txt > 0 )
										lsk.fuss_txt = kunbrtxtzu.fuss_txt ;
								}
							}
							sprintf ( lsk.blg_typ ,"R" );
							istatus = 0;
							if ( lsk.ls_stat == 3 || lsk.ls_stat == 4 )	// wurde eigentlich bereits etwas weiter oben abgefangen ...
							{
								lsk.ls_stat = 5 ;

								lief_retba.delstatus = 0;
								lief_retba.nr = lsk.ls ;
								sprintf ( lief_retba.blg_typ, "L" );
								lief_retba.mdn = mdn.mdn  ;
								lief_retba.fil = 0;
								lief_retba.kun = altkunde;
								lief_retba.kun_fil = 0;
								memcpy ( &lief_retba.dat, &lsk.lieferdat, sizeof( TIMESTAMP_STRUCT));
								sprintf ( lief_retba.pers, "redru" );
								lief_retba.vertr = 0;
								sprintf( lief_retba.err_txt, "");
								lief_retba.zutyp = 0;

								int save_sql_mode = dbClass.sql_mode ;
								dbClass.sql_mode = 7 ;
								istatus = lief_retba_class.schreibe_liefret() ;
								dbClass.sql_mode = save_sql_mode ;
							}
							int save_sql_mode = dbClass.sql_mode ;
							dbClass.sql_mode = 7 ;
							if ( !istatus ) istatus = lsk_class.schreibelsk() ;
							dbClass.sql_mode = save_sql_mode ;
							if ( !istatus )
							{
								dbClass.commitwork() ;
								eigmat[i] = 5 ;
							}
							else
							{
								kunbadmat[i] = altkunde ;
								badpoint ++ ;
								dbClass.rollbackwork() ;
								break ;
//								eigmat[i] = -2 ; // marker : Problem
//								i ++ ;
//								continue ;
							}
						}	// Daten vorhanden
					}		// status 3 oder 4
				}			// im Kunde
// ===============================================================================================
				if ( kunstat == 1 )
				{
					kungutmat[gutpoint] = altkunde ;
					gutpoint ++ ;
				}
			}	// kunstat war == 1

			i = kunende ;
			kunstart = i ;
			altkunde = schluss2mat[i] ;
			kunstat = 0 ;
		}	// Kundenwechsel
	}	// gesamten stapel durchwandern

	if ( gutpoint > 0 )
	{

		char rosipath[200];
		sprintf ( rosipath,"%s", getenv( "RSDIR"));

		char cvondat[20];
		char cbisdat[20];
		char crechdat[20];
		sqldatdstger( &lsk.vondate, cvondat) ;
		sqldatdstger( &lsk.bisdate, cbisdat) ;
		int	i = m_rechdat.GetLine(0,crechdat,19) ;
		crechdat[i] = '\0' ;



		v_info.Format( " %d Rechnungen erstellen " , gutpoint ) ;
		UpdateData(FALSE);

		for ( i = 0 ; i < gutpoint ; i ++ )
		{


//Musteraufruf 
// keine Leerzeichen, delimiter beachten usw. Kommando "F" bedeutet : generiere aus freigegebnen Rechnungen
//			rem rswsdb -q quark -d rechbearb F mdn=1;dat_von=01.01.2004;dat_bis=31.12.2004;kun_von=1010;kun_bis=1010;bran_von=0;bran_bis=99;kugru_von=0;kugru_bis=9999999;fil=0;rech_dat=10.10.2005;


			if ( globnutzer[0] != '\0' )
				sprintf ( bufh , "%s\\bin\\rswrun rechbearb F mdn=%d;dat_von=%s;dat_bis=%s;kun_von=%d;kun_bis=%d;bran_von=0;bran_bis=99;kugru_von=0;kugru_bis=9999999;fil=0;rech_dat=%s;nutzer=%s;tou_nr=%d;" 
				,rosipath , mdn.mdn , cvondat, cbisdat , kungutmat[i], kungutmat[i] ,crechdat,globnutzer, lsk.v_tou) ;
// 170513 : globnutzer aktiv 050613 : tou_nr aktiv 050613 deaktiveren			sprintf ( bufh , "%s\\bin\\rswrun rechbearb F mdn=%d;dat_von=%s;dat_bis=%s;kun_von=%d;kun_bis=%d;bran_von=0;bran_bis=99;kugru_von=0;kugru_bis=9999999;fil=0;rech_dat=%s;" 
// 170513 / 560613 deaktivieren				 ,rosipath , mdn.mdn , cvondat, cbisdat , kungutmat[i], kungutmat[i] ,crechdat) ;
			else
			sprintf ( bufh , "%s\\bin\\rswrun rechbearb F mdn=%d;dat_von=%s;dat_bis=%s;kun_von=%d;kun_bis=%d;bran_von=0;bran_bis=99;kugru_von=0;kugru_bis=9999999;fil=0;rech_dat=%s;tou_nr=%d;" 
			,rosipath , mdn.mdn , cvondat, cbisdat , kungutmat[i], kungutmat[i] ,crechdat , lsk.v_tou );
			int ex_code = ProcWaitExec( (char *) bufh , SW_SHOW, -1, 0, -1, 0 );
		}
	}
	if ( badpoint > 0 )
	{

		char hilfkun[30] ;
		sprintf ( bufh, "offene Kunden : ");
//		sprintf ( bufh, "Langtest 12345678901234567890123456789012345678901234567890 12345678901234567890123456789012345678901234567890 12345678901234567890123456789012345678901234567890 offene Kunden : ");
//		sprintf ( hilfkun, "\r\n") ;
//				strcat ( bufh, hilfkun);
//		sprintf ( hilfkun, "2.Zeile") ;
//				strcat ( bufh, hilfkun);

//		sprintf ( hilfkun, "\r\n") ;
//				strcat ( bufh, hilfkun);
//		sprintf ( hilfkun, "3.Zeile") ;
//				strcat ( bufh, hilfkun);

		int schoncr = 0 ;
		int strlang ;
		for ( i = 0 ; i < badpoint ; i ++ )
		{
			sprintf ( hilfkun, " %d" ,kunbadmat[i]) ;
			strcat ( bufh, hilfkun);
			strlang = strlen ( bufh ) ;
			if ( ( strlang > 130 && strlang < 150) || (strlang > 240 && strlang < 260)|| (strlang > 350 && strlang < 370) ) schoncr = 0 ;

			if ( (( strlang > 110 && strlang < 120) || (strlang > 220 && strlang < 230)|| (strlang > 330 && strlang < 340))
				&& schoncr == 0)
			{
				schoncr = 1 ;
				sprintf ( hilfkun,"\r\n") ;
				strcat ( bufh, hilfkun);
			}


			if ( strlang > 480 )
				break;
		}
		v_info.Format ("%s", bufh ) ;
		UpdateData(FALSE);

	}
}	

void CMy561lackDlg::OnBnClickedCheckl()
{
//		UpdateData(TRUE);
//		if ( v_checkl == TRUE )
//			v_checkl = FALSE ;
//		else
//			v_checkl = TRUE ;
//
//		UpdateData(FALSE);
}


void CMy561lackDlg::OnBnClickedCheckr()
{
//		UpdateData(TRUE);
// das passiert automatisch 
//		if ( v_checkr == TRUE )
//			v_checkr = FALSE ;
//		else
//			v_checkr = TRUE ;
//
//		UpdateData(FALSE);
}

void CMy561lackDlg::OnBnClickedLdrwahl()
{
	char s1[199] ;

	if ( strlen ( Nutzer ) > 1 )	// 261114 falls aus fit-menue heraus ( 1. Parameter fit-user-name)
		sprintf ( s1, "dr70001.exe -name 535A0 -NUTZER %s -WAHL 1", Nutzer );
	else
		sprintf ( s1, "dr70001.exe -name 535A0 -WAHL 1" );

	int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );

}

