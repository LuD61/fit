//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 561lack.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MY561LACK_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_DRWAHL                      1000
#define IDC_MANDANT                     1001
#define IDC_MDNNAME                     1002
#define IDC_RECHDAT                     1003
#define IDC_ABGRDAT                     1004
#define IDC_KUNVON                      1005
#define IDC_KUNBIS                      1006
#define IDC_TOURVON                     1007
#define IDC_TOURBIS                     1008
#define IDC_KUNVKRZ                     1009
#define IDC_EDIT8                       1010
#define IDC_KUNBKRZ                     1010
#define IDC_EDIT1                       1011
#define IDC_INFO                        1011
#define IDC_PROGRESS1                   1012
#define IDC_STATIC1                     1013
#define IDC_STATIC2                     1014
#define IDC_STATIC3                     1015
#define IDC_STATIC4                     1016
#define IDC_STATIC5                     1017
#define IDC_STATIC6                     1018
#define IDC_STATIC7                     1019
#define IDC_STATIC8                     1020
#define IDC_STATIC9                     1021
#define IDC_CHECKR                      1022
#define IDC_CHECKL                      1023
#define IDC_LDRWAHL                     1024

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
