#ifndef _LS_DEF
#define _LS_DEF

struct LSK
{
long	ls;
long	adr;
short	mdn ;
long	auf;
short   kun_fil;
long    kun ;
short   fil ;
char    feld_bz1[20] ;
TIMESTAMP_STRUCT lieferdat;
char    lieferzeit [6] ;
char    hinweis [49] ;
short   ls_stat;
char    kun_krz1 [17] ;
double  auf_sum ;
char    feld_bz2 [12] ;
double  lim_er ;
char    partner [37] ;
long	pr_lst ;
char    feld_bz3 [8] ;
short   pr_stu ;
long    vertr ;
long    tou ;
char    adr_nam1 [37] ;
char    adr_nam2 [37] ;
char    pf [17] ;
char    str [37] ;
char    plz [9] ;
char    ort1 [37] ;
double  of_po ;
short	delstatus ;
long    rech ;
char    blg_typ [2] ;
double	zeit_dec ;
long    kopf_txt ;
long    fuss_txt ;
long    inka_nr ;
char    auf_ext[20] ;
long    teil_smt ;
char    pers_nam [9];
double    brutto ;
TIMESTAMP_STRUCT komm_dat ;
double  of_ek ;
double	of_po_euro ;
double  of_po_fremd ;
double  of_ek_euro ;
double  of_ek_fremd ;
short	waehrung ;
char    ueb_kz [3];
double  gew ;
short   auf_art ;
short   fak_typ ;
short   ccmarkt ;
long    gruppe ;
long    tou_nr ;
short   wieg_kompl ;
TIMESTAMP_STRUCT best_dat ;
char    hinweis2 [32] ;
char    hinweis3 [32] ;
TIMESTAMP_STRUCT fix_dat ;
char    komm_name [13] ;
short   psteuer_kz ;

TIMESTAMP_STRUCT vondate ;
TIMESTAMP_STRUCT bisdate ;
long v_tou;
long bis_tou;
long v_kun;
long bis_kun;

};

struct LSP 
{
short		mdn ;
short		fil ;
long		ls ;
double		a ;
double		auf_me ;
char		auf_me_bz [7];
double		lief_me ;
char		lief_me_bz [7] ;
double		ls_vk_pr ;
double		ls_lad_pr ;
short		delstatus ;
double		tara ;
long		posi ;
long		lsp_txt ;
short		pos_stat ;
short		sa_kz_sint ;
char		erf_kz [2];
double		prov_satz ;
short		leer_pos ;
TIMESTAMP_STRUCT hbk_date ; 
char		ls_charge [31] ;
double		auf_me_vgl ;
double		ls_vk_euro ;
double		ls_vk_fremd ;
double		ls_lad_euro ;
double		ls_lad_fremd ;
double		rab_satz ;
short		me_einh_kun  ;
short		me_einh  ;
short		me_einh_kun1  ;
double		auf_me1 ;
double		inh1 ;
short		me_einh_kun2 ;
double		auf_me2 ;
double		inh2 ;
short		me_einh_kun3 ;
double		auf_me3 ;
double		inh3 ;
char		lief_me_bz_ist[12]; 
double		inh_ist ;
short		me_einh_ist ;
double		me_ist ; 
short		lager ;
double		lief_me1 ;
double		lief_me2 ;
double		lief_me3 ;
short		ls_pos_kz ;
double		a_grund ;
char		kond_art [5] ;
long		posi_ext ;
long		kun  ;
short		na_lief_kz  ;
long		nve_posi ;
short		teil_smt ;
short		aufschlag ;
double		aufschlag_wert ;
char		ls_ident [32] ;
};

extern struct LSK lsk, lsk_null;

extern struct LSP lsp, lsp_null;

class LSK_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesesbbeding (void);	// 280409
               int opensbbeding (void);	// 280409

			   int leseefbeding (void);	// 280409
               int openefbeding (void);	// 280409
               int leseekbeding (void);	// 280409
               int openekbeding (void);	// 280409

			   int lesefbeding (void);
               int openfbeding (void);
               int lesekbeding (void);
               int openkbeding (void);
               int leselsk (void);
               int openlsk (void);
               int leseeinzellsk (void);	// 090611
               int openeinzellsk (void);	// 090611
               int closeall (void);
			   int schreibelsk(void);
               LSK_CLASS () : DB_CLASS ()
               {
//				   readcursor = -1 ;
               }
};

class LSP_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leselsp (void);
               int openlsp (void);
			   int closeall (void);
               LSP_CLASS () : DB_CLASS ()
               {
//				   readcursor = -1 ;
               }
};

#endif
