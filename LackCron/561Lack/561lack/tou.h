#ifndef _TOU_DEF
#define _TOU_DEF
struct TOU {

    long tou ;
    char tou_bz[49] ;
};

extern struct TOU tou, tou_null;


class TOU_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesetou (void);
               int lesealltou (void);
               int opentou (void);
               int openalltou (void);
               TOU_CLASS () : DB_CLASS ()
               {
               }
};
#endif

