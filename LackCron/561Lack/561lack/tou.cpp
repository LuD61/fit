#include "stdafx.h"
#include "DbClass.h"
#include "tou.h"

struct TOU tou,  tou_null;
extern DB_CLASS dbClass;
TOU_CLASS tou_class;

static int anzzfelder ;

int TOU_CLASS::dbcount (void)
/**
Tabelle dta lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;  
}

int TOU_CLASS::lesetou (void)
{
	long savetou = tou.tou ;	// diese bloeden null-values .......
	memcpy ( &tou,&tou_null, sizeof(struct TOU));

	int di = dbClass.sqlfetch (readcursor);
	tou.tou = savetou ;
	return di;
}

int TOU_CLASS::lesealltou (void)
{
	  memcpy ( &tou,&tou_null, sizeof(struct TOU));
      int di = dbClass.sqlfetch (readallcursor);

	  return di;
}

int TOU_CLASS::opentou (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

int TOU_CLASS::openalltou (void)
{

		if ( readallcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readallcursor);
}

void TOU_CLASS::prepare (void)
{

							
	test_upd_cursor = 1;

	dbClass.sqlin ((long *) &tou.tou, SQLLONG, 0);

    dbClass.sqlout ((char *)  tou.tou_bz,	SQLCHAR, 49 ) ;

		readcursor = (short) dbClass.sqlcursor ("select "

    " tou_bz "
	" from tou where tou = ? ") ;

	dbClass.sqlout ((long *) &tou.tou, SQLLONG, 0);
	dbClass.sqlout ((char *)  tou.tou_bz,	SQLCHAR, 49 ) ;
	

		readallcursor = (short) dbClass.sqlcursor ("select "

    " tou, tou_bz "
	" from tou order by tou ") ;
}

