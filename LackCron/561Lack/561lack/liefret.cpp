#include "stdafx.h"
#include "DbClass.h"
#include "liefret.h"

struct LIEF_RETBA lief_retba, lief_retba_null;
extern DB_CLASS dbClass;
LIEF_RETBA_CLASS lief_retba_class ;


int LIEF_RETBA_CLASS::schreibe_liefret (void)
/**
insert into lief_retba
**/
{

         if (ins_cursor < 0 )	prepare ();
 	
         return dbClass.sqlexecute (ins_cursor);
}


void LIEF_RETBA_CLASS::prepare (void)
{

	dbClass.sqlin ((short *) &lief_retba.delstatus, SQLSHORT, 0);
	dbClass.sqlin ((long  *) &lief_retba.nr, SQLLONG, 0);
	dbClass.sqlin ((char  *)  lief_retba.blg_typ, SQLCHAR, 2);
	dbClass.sqlin ((short *) &lief_retba.mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &lief_retba.fil, SQLSHORT, 0);
	dbClass.sqlin ((long  *) &lief_retba.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &lief_retba.kun_fil, SQLSHORT, 0);
	dbClass.sqlin ((TIMESTAMP_STRUCT	*) &lief_retba.dat, SQLTIMESTAMP, 26);
	dbClass.sqlin ((char  *)  lief_retba.pers, SQLCHAR, 13);
	dbClass.sqlin ((long  *) &lief_retba.vertr, SQLLONG, 0);
	dbClass.sqlin ((char  *)  lief_retba.err_txt, SQLCHAR, 17);
	dbClass.sqlin ((short *) &lief_retba.zutyp, SQLSHORT, 0);

	ins_cursor = (short) dbClass.sqlcursor ( "insert into lief_retba "
	" ( delstatus, nr, blg_typ, mdn, fil, kun, kun_fil, dat, pers, vertr, err_txt, zutyp ) "
	" values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) " ) ;
	
}

