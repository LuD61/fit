// 561lackDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"
#include <vector>	// 111211

// CMy561lackDlg-Dialogfeld
class CMy561lackDlg : public CDialog
{
// Konstruktion
public:
	CMy561lackDlg(CWnd* pParent = NULL);	// Standardkonstruktor
   void AutoMove(int iID, double dXMovePct, double dYMovePct, double dXSizePct, double dYSizePct);	// 111211 


// Dialogfelddaten
	enum { IDD = IDD_MY561LACK_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);	// 111211 
    afx_msg void OnSize(UINT nType, int cx, int cy);		// 111211

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

// A111211
public: 
    bool            m_bShowGripper;         // ignored if not WS_THICKFRAME("Rahmen : Groesse aendern ....") 
 
private: 
    struct SMovingChild 
    { 
        HWND        m_hWnd; 
        double      m_dXMoveFrac; 
        double      m_dYMoveFrac; 
        double      m_dXSizeFrac; 
        double      m_dYSizeFrac; 
        CRect       m_rcInitial; 
        CSize       m_szInitial;	// 111211 111211

    }; 
//    typedef std::vector<SMovingChild>   MovingChildren; 
	typedef std::vector<SMovingChild>   MovingChildren; 
 
    MovingChildren  m_MovingChildren; 
    CSize           m_szInitial; 
    CSize           m_szMinimum; 
    HWND            m_hGripper; 

// E 111211


public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
	CEdit m_mdnname;
	CString v_mdnname;
	CEdit m_mandant;
	CString v_mandant;
	afx_msg void OnEnKillfocusMandant();
	afx_msg void OnEnKillfocusRechdat();
	afx_msg void OnEnKillfocusAbgrdat();
	afx_msg void OnEnKillfocusKunvon();
	afx_msg void OnEnKillfocusKunbis();

// 161211
	afx_msg void OnCbnSelchangeTourvon();
	CComboBox m_tourvon;
	CString v_tourvon;
	afx_msg void OnCbnKillfocusTourvon();

	afx_msg void OnCbnSelchangeTourbis();
	CComboBox m_tourbis;
	CString v_tourbis;
	afx_msg void OnCbnKillfocusTourbis();

//	161211 afx_msg void OnEnKillfocusTourvon();
//	161211 afx_msg void OnEnKillfocusTourbis();

	afx_msg void OnBnClickedDrwahl();
	CEdit m_rechdat;
	CString v_rechdat;
	CEdit m_abgrdat;
	CString v_abgrdat;
	CEdit m_kunvon;
	CEdit m_kunbis;
	CString v_kunvon;
	CString v_kunbis;

//  161211	CEdit m_tourvon;
//	161211 CString v_tourvon;
//	161211 CEdit m_tourbis;
//	161211 CString v_tourbis;

	CEdit m_info;
	CString v_info;
	CEdit m_kunvkrz;
	CString v_kunvkrz;
	CEdit m_kunbkrz;
	CString v_kunbkrz;

	int ReadMdn (void);	// (0)false/(1)true 
	void systemdatum (char *  ziel);
	BOOL PreTranslateMessage(MSG* pMsg);
	BOOL OnReturn(void);
	BOOL NoListCtrl(CWnd *cWnd);	// 161211
	BOOL OnKeyup(void);
	void StapelDruck (void);
	void ListenDruck (void);	// 010612
	BOOL scancheck(char * );	// 280812
	CButton m_checkr;
	BOOL v_checkr;
	CButton m_checkl;
	BOOL v_checkl;
	afx_msg void OnBnClickedCheckr();
	afx_msg void OnBnClickedCheckl();
	afx_msg void OnBnClickedLdrwahl();
};
