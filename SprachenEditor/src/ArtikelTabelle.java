import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;


public class ArtikelTabelle implements TableModel {
	
	private DataGenerator dataGenerator;
	private ArrayList<Artikel> artikelListe;
	
	public ArtikelTabelle (final DataGenerator dataGenerator) {		

		try {
			this.dataGenerator = dataGenerator;
			this.artikelListe = this.dataGenerator.getValues();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		// TODO Auto-generated method stub
		return Object.class;
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public String getColumnName(int columnIndex) {
		// TODO Auto-generated method stub
		
		switch(columnIndex) {
		case 0:
			return "Artikel";
		case 1:
			return "Bezeichnung 1";
		case 2:
			return "Bezeichnung 2";
		}
		return "";
	}

	@Override
	public int getRowCount() {		
		return this.artikelListe.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {		
		return this.artikelListe.get(rowIndex).getValueAt(columnIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {		
		// TODO Auto-generated method stub
	}
	
	
	public ArrayList<Artikel> getArtikelListe() {
		return this.artikelListe;
	}

}
