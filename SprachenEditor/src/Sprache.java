
public class Sprache {
	
	
	private short sprache;
	private String a_bz1;
	private String a_bz2;
	private String produkt_info;
	
	
	public Sprache(final short sprache, final String a_bz1, final String a_bz2, final String produkt_info) {
		
		this.sprache = sprache;
		this.a_bz1 = a_bz1;
		this.a_bz2 = a_bz2;
		this.produkt_info = produkt_info;
		
	}


	/**
	 * @return the sprache
	 */
	public short getSprache() {
		return sprache;
	}
	
	


	/**
	 * @return the a_bz1
	 */
	public String getA_bz1() {
		return a_bz1;
	}
	
	
	public void setA_bz1(final String a_bz1) {
		this.a_bz1 = a_bz1;
	}


	/**
	 * @return the produkt_info
	 */
	public String getProdukt_info() {
		return produkt_info;
	}
	
	
	public void setProdukt_info(final String produkt_info) {
		this.produkt_info = produkt_info;
	}


	/**
	 * @return the a_bz2
	 */
	public String getA_bz2() {
		return a_bz2;
	}
	
	
	public void setA_bz2(final String a_bz2) {
		this.a_bz2 = a_bz2;
	}
	



}
