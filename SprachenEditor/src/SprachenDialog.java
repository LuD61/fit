import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.JScrollPane;
import javax.swing.JLayeredPane;
import javax.swing.JButton;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import fit.informixconnector.InformixConnector;

import javax.swing.JTextField;
import java.util.Iterator;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.UnsupportedEncodingException;

import javax.swing.JEditorPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;



public class SprachenDialog extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2321838842904587176L;
	private JLayeredPane contentPane;
	private static InformixConnector informixConnector;
	private ArtikelTabelle tableModel;
	private JTable table;
	private JTextField deBz1;
	private JTextField deBz2;
	private JTextField enBz1;
	private JTextField ruBz1;
	private JEditorPane enProduktInfo;
	private JEditorPane ruProduktInfo;
	private JEditorPane enBz2;
	private JEditorPane ruBz2;
	private JEditorPane dePPABZ1;
	private Artikel sel_artikel = null;
	private boolean en_vorhanden = false;
	private boolean ru_vorhanden = false;
	private DataGenerator dataGenerator;

	/**
	 * Launch the application.
	 */
	
	
	public static void main(String[] args) {		
		
		try {
			
			informixConnector = new InformixConnector();			
			informixConnector.createConnection();
			
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			SprachenDialog frame = new SprachenDialog();					
			frame.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	

	/**
	 * Create the frame.
	 */
	public SprachenDialog() {		
				
		dataGenerator = new DataGenerator(informixConnector);
		
		setTitle("Spracheneditor");	
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1100, 771);
		contentPane = new JLayeredPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		tableModel = new ArtikelTabelle(dataGenerator);
		
		JLabel lblEmailRechnungsversand = new JLabel("fit Spracheneditor");
		lblEmailRechnungsversand.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblEmailRechnungsversand.setBounds(22, 11, 985, 50);
		contentPane.add(lblEmailRechnungsversand);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				
				if ( arg0.getKeyCode() == KeyEvent.VK_DOWN || arg0.getKeyCode() == KeyEvent.VK_UP ) {
					
					System.out.println(table.getSelectedRow());
					sel_artikel = tableModel.getArtikelListe().get(table.getSelectedRow());
					
					deBz1.setText(sel_artikel.getA_bz1());
					deBz2.setText(sel_artikel.getA_bz2());
					dePPABZ1.setText(sel_artikel.getPrp_abz());
					en_vorhanden = false;
					ru_vorhanden = false;
					enBz1.setText("");
					enBz2.setText("");
					enProduktInfo.setText("");
					ruBz1.setText("");
					ruBz2.setText("");
					ruProduktInfo.setText("");				
					
					Iterator<Sprache> it_sprachen = sel_artikel.getSprachenList().iterator();						
					while (it_sprachen.hasNext()) {
						Sprache sprache = (Sprache) it_sprachen.next();
						final short spracheId = sprache.getSprache();
						
						if ( spracheId == 16 ) {
							enBz1.setText(sprache.getA_bz1());
							enBz2.setText(sprache.getA_bz2());
							enProduktInfo.setText(sprache.getProdukt_info());
							en_vorhanden = true;
						} else if ( spracheId == 17 ){
							ruBz1.setText(sprache.getA_bz1());
							ruBz2.setText(sprache.getA_bz2());
							ruProduktInfo.setText(sprache.getProdukt_info());
							ru_vorhanden = true;
						} else {
							continue;
						}					
					}
				}
			}
		});
		scrollPane.setBounds(22, 90, 462, 617);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(tableModel);		
		table.setRowSelectionAllowed(true);
        table.setColumnSelectionAllowed(false);
        
        JLabel lblNewLabel = new JLabel("Deutsche \u00DCbersetzung");
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
        lblNewLabel.setBounds(505, 47, 170, 30);
        contentPane.add(lblNewLabel);
        
        deBz1 = new JTextField();
        deBz1.setBackground(Color.WHITE);
        deBz1.setEditable(false);
        deBz1.setBounds(656, 88, 404, 20);
        contentPane.add(deBz1);
        deBz1.setColumns(10);
        
        deBz2 = new JTextField();
        deBz2.setBackground(Color.WHITE);
        deBz2.setEditable(false);
        deBz2.setColumns(10);
        deBz2.setBounds(656, 115, 404, 20);
        contentPane.add(deBz2);
        
        JLabel lblEnglischebersetzung = new JLabel("Englische \u00DCbersetzung");
        lblEnglischebersetzung.setFont(new Font("Tahoma", Font.PLAIN, 14));
        lblEnglischebersetzung.setBounds(505, 207, 199, 30);
        contentPane.add(lblEnglischebersetzung);
        
        enBz1 = new JTextField();
        enBz1.setColumns(10);
        enBz1.setBounds(656, 248, 404, 20);
        contentPane.add(enBz1);
        
        enProduktInfo = new JEditorPane();
        enProduktInfo.setBounds(656, 346, 404, 103);
        contentPane.add(enProduktInfo);
        
        JLabel lblBezeichnung = new JLabel("Artikelbezeichnung 1");
        lblBezeichnung.setBounds(525, 251, 121, 14);
        contentPane.add(lblBezeichnung);
        
        JLabel lblBezeichnung_1 = new JLabel("Verkehrsbezeichnung");
        lblBezeichnung_1.setBounds(525, 281, 121, 54);
        contentPane.add(lblBezeichnung_1);
        
        JLabel lblProduktInfo = new JLabel("Produkt Beschreibung");
        lblProduktInfo.setBounds(525, 346, 121, 14);
        contentPane.add(lblProduktInfo);
        
        JLabel lblRussischebersetzung = new JLabel("Russische \u00DCbersetzung");
        lblRussischebersetzung.setFont(new Font("Tahoma", Font.PLAIN, 14));
        lblRussischebersetzung.setBounds(505, 452, 199, 30);
        contentPane.add(lblRussischebersetzung);
        
        JLabel lblArtikelbezeichnung = new JLabel("Artikelbezeichnung 1");
        lblArtikelbezeichnung.setBounds(525, 493, 121, 14);
        contentPane.add(lblArtikelbezeichnung);
        
        JLabel lblVerkehrsbezeichnung = new JLabel("Verkehrsbezeichnung");
        lblVerkehrsbezeichnung.setBounds(525, 534, 121, 14);
        contentPane.add(lblVerkehrsbezeichnung);
        
        JLabel lblProduktBeschreibung = new JLabel("Produkt Beschreibung");
        lblProduktBeschreibung.setBounds(525, 596, 121, 14);
        contentPane.add(lblProduktBeschreibung);
        
        ruProduktInfo = new JEditorPane();
        ruProduktInfo.setBounds(656, 590, 404, 89);
        contentPane.add(ruProduktInfo);
        
        ruBz1 = new JTextField();
        ruBz1.setColumns(10);
        ruBz1.setBounds(656, 490, 404, 20);
        contentPane.add(ruBz1);
        
        JButton btnNewButton = new JButton("Beenden");
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		System.exit(0);
        	}
        });
        btnNewButton.setBounds(881, 699, 179, 23);
        contentPane.add(btnNewButton);
        
        JButton button = new JButton("Eingabe speichern");
        button.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		final String enB1 = enBz1.getText();
        		final String enB2 = enBz2.getText();
        		final String enPI = enProduktInfo.getText();
				
        		final String ruB1 = ruBz1.getText();
        		final String ruB2 = ruBz2.getText();
        		final String ruPI = ruProduktInfo.getText();
        		
        		if (!enB1.isEmpty()) {        			
        			if ( en_vorhanden ) {
        				final Sprache sprache = getSprache((short) 16);        				
        				if ( sprache != null ) {
        					sprache.setA_bz1(enB1);
            				sprache.setA_bz2(enB2);
            				sprache.setProdukt_info(enPI);
        					try {
								dataGenerator.updateValues(sprache, sel_artikel.getA(), sel_artikel.getMdn());
							} catch (UnsupportedEncodingException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
        				}
        			} else {
        				final short spracheId = 16;
        				final Sprache sprache = new Sprache(spracheId, enB1, enB2, enPI);
        				sel_artikel.addSprache(sprache);
        				try {
							dataGenerator.insertValues(sprache, sel_artikel.getA(), sel_artikel.getMdn());
						} catch (UnsupportedEncodingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
        			}        			
        		}
        		
        		if (!ruB1.isEmpty()) {
        			if ( ru_vorhanden ) {
        				final Sprache sprache = getSprache((short) 17);
        				if ( sprache != null ) {
        					sprache.setA_bz1(ruB1);
            				sprache.setA_bz2(ruB2);
            				sprache.setProdukt_info(ruPI);
        					try {
								dataGenerator.updateValues(sprache, sel_artikel.getA(), sel_artikel.getMdn());
							} catch (UnsupportedEncodingException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
        				}       				
        			} else {
        				final short spracheId = 17;
        				final Sprache sprache = new Sprache(spracheId, ruB1, ruB2, ruPI);
        				sel_artikel.addSprache(sprache);
        				try {
							dataGenerator.insertValues(sprache, sel_artikel.getA(), sel_artikel.getMdn());
						} catch (UnsupportedEncodingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
        			}        			
        		}       		
        	}
        });
        button.setBounds(622, 699, 179, 23);
        contentPane.add(button);
        
        JLabel label = new JLabel("Artikelbezeichnung 1");
        label.setBounds(525, 89, 121, 14);
        contentPane.add(label);
        
        JLabel lblArtikelbezeichnung_1 = new JLabel("Artikelbezeichnung 2");
        lblArtikelbezeichnung_1.setBounds(525, 118, 121, 14);
        contentPane.add(lblArtikelbezeichnung_1);
        
        JLabel lblVerkehrsbezeichnung_1 = new JLabel("Verkehrsbezeichnung");
        lblVerkehrsbezeichnung_1.setBounds(525, 149, 121, 14);
        contentPane.add(lblVerkehrsbezeichnung_1);
        
        enBz2 = new JEditorPane();
        contentPane.setLayer(enBz2, 2);
        enBz2.setBounds(656, 279, 404, 56);
        contentPane.add(enBz2);
        
        ruBz2 = new JEditorPane();
        ruBz2.setBounds(656, 521, 404, 56);
        contentPane.add(ruBz2);
        
        dePPABZ1 = new JEditorPane();
        dePPABZ1.setBackground(Color.WHITE);
        dePPABZ1.setForeground(Color.BLACK);
        dePPABZ1.setEnabled(false);
        dePPABZ1.setBounds(656, 146, 404, 56);
        contentPane.add(dePPABZ1);
		table.getTableHeader().setResizingAllowed(true);
		
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				System.out.println(table.getSelectedRow());
				sel_artikel = tableModel.getArtikelListe().get(table.getSelectedRow());
				
				deBz1.setText(sel_artikel.getA_bz1());
				deBz2.setText(sel_artikel.getA_bz2());
				dePPABZ1.setText(sel_artikel.getPrp_abz());
				en_vorhanden = false;
				ru_vorhanden = false;
				enBz1.setText("");
				enBz2.setText("");
				enProduktInfo.setText("");
				ruBz1.setText("");
				ruBz2.setText("");
				ruProduktInfo.setText("");				
				
				Iterator<Sprache> it_sprachen = sel_artikel.getSprachenList().iterator();						
				while (it_sprachen.hasNext()) {
					Sprache sprache = (Sprache) it_sprachen.next();
					final short spracheId = sprache.getSprache();
					
					if ( spracheId == 16 ) {
						enBz1.setText(sprache.getA_bz1());
						enBz2.setText(sprache.getA_bz2());
						enProduktInfo.setText(sprache.getProdukt_info());
						en_vorhanden = true;
					} else if ( spracheId == 17 ){
						ruBz1.setText(sprache.getA_bz1());
						ruBz2.setText(sprache.getA_bz2());
						ruProduktInfo.setText(sprache.getProdukt_info());
						ru_vorhanden = true;
					} else {
						continue;
					}					
				}
				
				
				
			}
		});

	
	}
	
	private Sprache getSprache(final short spracheId) {
		
		Iterator<Sprache> sprachen = sel_artikel.getSprachenList().iterator();
		while (sprachen.hasNext()) {
			Sprache sprache = (Sprache) sprachen.next();
			if ( sprache.getSprache() == spracheId ) {
				return sprache;				
			}
		}
		return null;		
	}
}
