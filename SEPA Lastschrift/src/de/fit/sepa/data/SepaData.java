package de.fit.sepa.data;

import java.sql.Date;

public class SepaData {
	
		
	private Date rech_dat;
	private int rech_nr;
	private double rech_bto_eur;
	private double skto;
	private double lastbetrag;
	


	public SepaData(Date rech_dat, int rech_nr, double rech_bto_eur,
			double skto, double lastbetrag) {
		this.rech_dat = rech_dat;
		this.rech_nr = rech_nr;
		this.rech_bto_eur = rech_bto_eur;
		this.skto = skto;
		this.lastbetrag = lastbetrag;
	}


	public Date getRech_dat() {
		return rech_dat;
	}


	public void setRech_dat(Date rech_dat) {
		this.rech_dat = rech_dat;
	}


	public int getRech_nr() {
		return rech_nr;
	}


	public void setRech_nr(int rech_nr) {
		this.rech_nr = rech_nr;
	}


	public double getRech_bto_eur() {
		return rech_bto_eur;
	}


	public void setRech_bto_eur(double rech_bto_eur) {
		this.rech_bto_eur = rech_bto_eur;
	}


	public double getSkto() {
		return skto;
	}


	public void setSkto(double skto) {
		this.skto = skto;
	}


	public double getLastbetrag() {
		return lastbetrag;
	}


	public void setLastbetrag(double lastbetrag) {
		this.lastbetrag = lastbetrag;
	}

}
