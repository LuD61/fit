package de.fit.sepa.data;

public class Creditor {

	private String ci;
	private String name;
	private String nam_krz;
	
	public Creditor(String ci, String name, String nam_krz) {
		
		this.ci = (ci != null) ? ci.trim():"";
		this.name = (name != null) ? name.trim():"";
		this.nam_krz = (nam_krz != null) ? nam_krz.trim():"";
	}


	public String getCi() {
		return ci;
	}


	public void setCi(String ci) {
		this.ci = ci;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNam_krz() {
		return nam_krz;
	}


	public void setNam_krz(String nam_krz) {
		this.nam_krz = nam_krz;
	}		

}
