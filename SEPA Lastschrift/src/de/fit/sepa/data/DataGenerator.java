package de.fit.sepa.data;


import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import fit.informixconnector.InformixConnector;


public class DataGenerator {
	
	private InformixConnector informixConnector;


	public DataGenerator (final InformixConnector informixConnector) {
		this.informixConnector = informixConnector;		
	}


	public ArrayList<Kunde> getValues(String beginDateSQL, String endDateSQL, int rechnr_von, int rechnr_bis, int kun_von, int kun_bis, int sepaart, int zahl_art) throws SQLException, IOException {
		
		ArrayList<Kunde> kunden = new ArrayList<>();
		Kunde kunde = null;		
		int previous_kun = -1;
		
		final String stmt = "select rech.kun kun, rech_dat, rech_nr, rech_bto_eur, skto," +
							"(rech_bto_eur * ((100 - skto) / 100)) lastbetrag," +
							"kun_krz1, bank_kun, kun.blz, kun.kto_nr, adr.iban aiban, adr.swift aswift, sepaart, tagesepa," +
							"mandatref, haus_bank.iban hiban, haus_bank.swift hswift, haus_bank.bank_nam hbank_nam, mandseit, sambuch" +
							" from rech, kun, adr, mdn, haus_bank, kun_erw" +
							" where rech.kun = kun.kun" +
							" and kun.kun = kun_erw.kun" +
							" and kun.adr3 = adr.adr" +
							" and rech.zahl_art = 0" +
							" and blg_typ = 'R'" +
							" and rech_stat = 1" +
							" and kun.zahl_art = " + zahl_art + 
							" and rech_bto_eur > 0" +
							" and hausbank = haus_bank.bank" +
							" and rech_dat between '" + beginDateSQL + "' and '" + endDateSQL + "'" +
							" and rech.kun between " + kun_von + " and " + kun_bis +
							" and rech_nr between " + rechnr_von + " and " + rechnr_bis +
							" and sepaart = " + sepaart +
							" and mdn.mdn = 1" +
							" and mdn.mdn = kun.mdn" +
							" order by rech.kun, rech_dat";
							
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);		
		
		while(resultSet.next()) {
			boolean btchbook = true;
			final int kun = resultSet.getInt("kun");
			final Date rech_dat = resultSet.getDate("rech_dat");
			final int rech_nr = resultSet.getInt("rech_nr");
			final double rech_bto_eur = resultSet.getDouble("rech_bto_eur");
			final double skto = resultSet.getDouble("skto");
			final double lastbetrag = resultSet.getDouble("lastbetrag");
			final String kun_krz1 = resultSet.getString("kun_krz1");
			final int blz = resultSet.getInt("blz");
			final String kto_nr = resultSet.getString("kto_nr");
			final String iban = resultSet.getString("aiban");
			final String swift = resultSet.getString("aswift");
			final short sepaart2 = resultSet.getShort("sepaart");
			final short tagesepa = resultSet.getShort("tagesepa");
			final String mandatref = resultSet.getString("mandatref");
			final String hausiban = resultSet.getString("hiban");
			final String hausswift = resultSet.getString("hswift");
			final String hausbank = resultSet.getString("hbank_nam");
			final Date mandseit = resultSet.getDate("mandseit");
			final String sambuch = resultSet.getString("sambuch");
			
			if ( sambuch != null ) {
				if ( sambuch.equals("N") ) {
					btchbook = false;
				} else {
					btchbook = true;
				}
			}
			
			if ( kunde == null ) {
				kunde = new Kunde(kun, kun_krz1, blz, kto_nr, iban, swift, sepaart2, tagesepa, mandatref, hausiban, hausswift, hausbank, mandseit, btchbook);
				kunde.addSepaData(new SepaData(rech_dat, rech_nr, rech_bto_eur, skto, lastbetrag));
				kunden.add(kunde);
			} else if ( kun != previous_kun ) {				
				kunde = new Kunde(kun, kun_krz1, blz, kto_nr, iban, swift, sepaart2, tagesepa, mandatref, hausiban, hausswift, hausbank, mandseit, btchbook);
				kunde.addSepaData(new SepaData(rech_dat, rech_nr, rech_bto_eur, skto, lastbetrag));
				kunden.add(kunde);
			} else {
				kunde.addSepaData(new SepaData(rech_dat, rech_nr, rech_bto_eur, skto, lastbetrag));
			}
			
			previous_kun = kun;		
		}		
		return kunden;
	}
	
	
	
	public Creditor getCreditorInformations() throws SQLException, IOException {
		
		Creditor creditor = null;
		
		final String stmt = "select ci, adr_nam1, adr_krz" +
							" from mdn, adr" +
							" where mdn.adr = adr.adr" +
							" and mdn.mdn = 1";							
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		while(resultSet.next()) {
			
			final String ci = resultSet.getString("ci");
			final String name = resultSet.getString("adr_nam1");
			final String nam_krz = resultSet.getString("adr_krz");
			
			creditor = new Creditor(ci, name, nam_krz);
		}		
		return creditor;
	}
	
	
	
	public void updateRechStat(final ArrayList<Kunde> kunden) throws SQLException, IOException {
						
		Iterator<Kunde> kunde_it = kunden.iterator();
		while (kunde_it.hasNext()) {
			Kunde kunde = (Kunde) kunde_it.next();
			Iterator<SepaData> sepas = kunde.getSepaDataIterator();
			while (sepas.hasNext()) {
				SepaData sepaData = (SepaData) sepas.next();				
				final String stmt = "update rech set rech_stat = 3 where rech_nr = " + sepaData.getRech_nr() + "; ";
				informixConnector.executeStatement(stmt);
			}			
		}				
	}
	
	
	public void loggingActionToDB(final ArrayList<Kunde> kunden) throws SQLException, IOException {
		
		java.sql.Date export = new Date(new java.util.Date().getTime());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
		String dateExportString = simpleDateFormat.format(export);
		
		
		Iterator<Kunde> kunde_it = kunden.iterator();
		while (kunde_it.hasNext()) {
			Kunde kunde = (Kunde) kunde_it.next();
			Iterator<SepaData> sepas = kunde.getSepaDataIterator();
			String dateFaelligString = simpleDateFormat.format(kunde.getFaellig());
			while (sepas.hasNext()) {
				SepaData sepaData = (SepaData) sepas.next();
				
				String insertstmt = 
						"INSERT INTO sepa_log" +
						"(rech," +
						"kun," +
						"lastbetrag," +
						"export," +
						"faellig," +
						"typ" +
						")	VALUES (" +
						sepaData.getRech_nr() + "," +
						kunde.getKun() + "," +
						sepaData.getLastbetrag() + ",'" +
						dateExportString + "','" +
						dateFaelligString + "','L'" + 
						"); ";
				
				this.informixConnector.executeStatement(insertstmt);
			}			
		}				
	}
	
}
