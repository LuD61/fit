package de.fit.sepa.data;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import de.fit.sepa.AccountIdentificationSEPA;
import de.fit.sepa.ActiveOrHistoricCurrencyAndAmountSEPA;
import de.fit.sepa.ActiveOrHistoricCurrencyCodeEUR;
import de.fit.sepa.BranchAndFinancialInstitutionIdentificationSEPA3;
import de.fit.sepa.CashAccountSEPA1;
import de.fit.sepa.CashAccountSEPA2;
import de.fit.sepa.ChargeBearerTypeSEPACode;
import de.fit.sepa.CustomerDirectDebitInitiationV02;
import de.fit.sepa.DirectDebitTransactionInformationSDD;
import de.fit.sepa.DirectDebitTransactionSDD;
import de.fit.sepa.Document;
import de.fit.sepa.FinancialInstitutionIdentificationSEPA3;
import de.fit.sepa.GroupHeaderSDD;
import de.fit.sepa.IdentificationSchemeNameSEPA;
import de.fit.sepa.LocalInstrumentSEPA;
import de.fit.sepa.MandateRelatedInformationSDD;
import de.fit.sepa.PartyIdentificationSEPA1;
import de.fit.sepa.PartyIdentificationSEPA2;
import de.fit.sepa.PartyIdentificationSEPA3;
import de.fit.sepa.PartyIdentificationSEPA5;
import de.fit.sepa.PartySEPA2;
import de.fit.sepa.PaymentIdentificationSEPA;
import de.fit.sepa.PaymentInstructionInformationSDD;
import de.fit.sepa.PaymentMethod2Code;
import de.fit.sepa.PaymentTypeInformationSDD;
import de.fit.sepa.PersonIdentificationSEPA2;
import de.fit.sepa.RemittanceInformationSEPA1Choice;
import de.fit.sepa.RestrictedPersonIdentificationSEPA;
import de.fit.sepa.RestrictedPersonIdentificationSchemeNameSEPA;
import de.fit.sepa.SequenceType1Code;
import de.fit.sepa.ServiceLevelSEPA;

public class SepaConverter {
	
	private ArrayList<Kunde> kunden;
	private Creditor creditor;
	private int counter = 0;
	

	public SepaConverter(final ArrayList<Kunde> kunden, final Creditor creditor) {
		this.kunden = kunden;
		this.creditor = creditor;
	}
	
	
	public Document getDocument(SequenceType1Code seqCode) throws DatatypeConfigurationException {
		
		GregorianCalendar gcal = new GregorianCalendar();
	    XMLGregorianCalendar xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
		
		String nowAsISO = javax.xml.bind.DatatypeConverter.printDateTime(Calendar.getInstance(TimeZone.getTimeZone("UTC")));
		System.out.println(nowAsISO); 
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final String msgId = "SEPA" + df.format(new Date());
		
		Document document = new Document();		
		CustomerDirectDebitInitiationV02 customerDirectDebitInitiationV02 = new CustomerDirectDebitInitiationV02();
		
		PartyIdentificationSEPA1 partyIdentificationSEPA1 = new PartyIdentificationSEPA1();
		partyIdentificationSEPA1.setNm(this.creditor.getName());
		
		double ctrlSum = this.getGlobalCtrlSum();		
		BigDecimal ctrlSumBD = round(ctrlSum, 2, BigDecimal.ROUND_HALF_UP);
		GroupHeaderSDD groupHeaderSDD = new GroupHeaderSDD(msgId, xgcal, String.valueOf(counter), ctrlSumBD, partyIdentificationSEPA1);
		
		customerDirectDebitInitiationV02.setGrpHdr(groupHeaderSDD);
		this.getPaymentInformations(customerDirectDebitInitiationV02, seqCode);		
		
		document.setCstmrDrctDbtInitn(customerDirectDebitInitiationV02);
		
		return document;
	}
	
	
	private double getGlobalCtrlSum() {
		
		double temp = 0;		
		
		Iterator<Kunde> kunde_it = this.kunden.iterator();
		while (kunde_it.hasNext()) {
			Kunde kunde = (Kunde) kunde_it.next();
			Iterator<SepaData> sepas = kunde.getSepaDataIterator();
			while (sepas.hasNext()) {
				SepaData sepaData = (SepaData) sepas.next();				
				temp += sepaData.getLastbetrag();
				this.counter++;
			}			
		}		
		return temp;
	}
	
	
	private double getKunCtrlSum(final Kunde kunde) {
		
		double temp = 0;
		
		Iterator<SepaData> sepas = kunde.getSepaDataIterator();
		while (sepas.hasNext()) {
			SepaData sepaData = (SepaData) sepas.next();				
			temp += sepaData.getLastbetrag();
		}		
		return temp;
	}
	
	
	public static BigDecimal round(double unrounded, int precision, int roundingMode)
	{
	    BigDecimal bd = new BigDecimal(unrounded);
	    BigDecimal rounded = bd.setScale(precision, roundingMode);
	    return rounded;
	}
	
	
	private void getPaymentInformations ( CustomerDirectDebitInitiationV02 customerDirectDebitInitiationV02, SequenceType1Code seqCode ) {
		
		PaymentInstructionInformationSDD paymentInstructionInformationSDD = null;
		DirectDebitTransactionInformationSDD directDebitTransactionInformationSDD = null;
		
		Iterator<Kunde> kun_it = kunden.iterator();		
		
		while (kun_it.hasNext()) {
			Kunde kunde = (Kunde) kun_it.next();			
			try {
				paymentInstructionInformationSDD = this.getNewPaymentInstructionInformationSDD(kunde, seqCode);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
			
			Iterator<SepaData> it_sepaData = kunde.getSepaDataIterator();
			
			while (it_sepaData.hasNext()) {
				SepaData sepaData = (SepaData) it_sepaData.next();
				directDebitTransactionInformationSDD = this.getNewDirectDebitTransactionInformationSDD(kunde, sepaData);
				paymentInstructionInformationSDD.getDrctDbtTxInf().add(directDebitTransactionInformationSDD);				
			}
			
			customerDirectDebitInitiationV02.getPmtInf().add(paymentInstructionInformationSDD);			
		}
	}
	
	
	private PaymentInstructionInformationSDD getNewPaymentInstructionInformationSDD(final Kunde kunde, SequenceType1Code seqCode) throws DatatypeConfigurationException {
		
		PaymentInstructionInformationSDD pi = new PaymentInstructionInformationSDD();
		
		pi.setPmtInfId(kunde.getMandatref());
		
		pi.setPmtMtd(PaymentMethod2Code.DD);
		
		pi.setBtchBookg(kunde.isBtchbook());
		
		pi.setNbOfTxs(String.valueOf(kunde.getSepaData().size()));
		
		double ctrlSum = this.getKunCtrlSum(kunde);		
		BigDecimal ctrlSumBD = round(ctrlSum, 2, BigDecimal.ROUND_HALF_UP);
		pi.setCtrlSum(ctrlSumBD);
		
		PaymentTypeInformationSDD paymentTypeInformationSDD = new PaymentTypeInformationSDD();
		ServiceLevelSEPA serviceLevelSEPA = new ServiceLevelSEPA();
		serviceLevelSEPA.setCd("SEPA");
		paymentTypeInformationSDD.setSvcLvl(serviceLevelSEPA);
		LocalInstrumentSEPA localInstrumentSEPA = new LocalInstrumentSEPA();
		
		if ( kunde.getSeparart() == 0 ) {
			localInstrumentSEPA.setCd("CORE");	
		} else {
			localInstrumentSEPA.setCd("B2B");		
		}		
		
		paymentTypeInformationSDD.setLclInstrm(localInstrumentSEPA);
		paymentTypeInformationSDD.setSeqTp(seqCode);		
		pi.setPmtTpInf(paymentTypeInformationSDD);
		
		Date now = new Date();
		GregorianCalendar gcal = new GregorianCalendar();
		gcal.setTime(now);
		gcal.setFirstDayOfWeek(Calendar.MONDAY);
		gcal.add(Calendar.DAY_OF_MONTH, kunde.getTagesepa());
		int dow = gcal.get(Calendar.DAY_OF_WEEK);
		if (dow == Calendar.SATURDAY) {
			gcal.add(Calendar.DAY_OF_MONTH, 2);
		} else if (dow == Calendar.SUNDAY) {
			gcal.add(Calendar.DAY_OF_MONTH, 1);
		}
	    
		java.sql.Date faellig = new java.sql.Date(gcal.getTime().getTime());		
		kunde.setFaellig(faellig);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");	    
		pi.setReqdColltnDt(df.format(gcal.getTime()));
		
		PartyIdentificationSEPA5 partyIdentificationSEPA5 = new PartyIdentificationSEPA5();
		partyIdentificationSEPA5.setNm(this.creditor.getName());
		pi.setCdtr(partyIdentificationSEPA5);
		
		CashAccountSEPA1 cashAccountSEPA1 = new CashAccountSEPA1();
		cashAccountSEPA1.setCcy("EUR");
		AccountIdentificationSEPA accountIdentificationSEPA = new AccountIdentificationSEPA();
		accountIdentificationSEPA.setIBAN(kunde.getHausiban());
		cashAccountSEPA1.setId(accountIdentificationSEPA);
		pi.setCdtrAcct(cashAccountSEPA1);
		
		BranchAndFinancialInstitutionIdentificationSEPA3 b3 = new BranchAndFinancialInstitutionIdentificationSEPA3();
		FinancialInstitutionIdentificationSEPA3 fi3 = new FinancialInstitutionIdentificationSEPA3();
		fi3.setBIC(kunde.getHausswift());
		b3.setFinInstnId(fi3);
		pi.setCdtrAgt(b3);
		
		pi.setChrgBr(ChargeBearerTypeSEPACode.SLEV);
		
		PartyIdentificationSEPA3 partyIdentificationSEPA3 = new PartyIdentificationSEPA3();		
		PartySEPA2 partySepa2 = new PartySEPA2();		
		PersonIdentificationSEPA2 personIdentificationSEPA2 = new PersonIdentificationSEPA2();		
		RestrictedPersonIdentificationSEPA restrictedPersonIdentificationSEPA = new RestrictedPersonIdentificationSEPA();
		restrictedPersonIdentificationSEPA.setId(this.creditor.getCi());		
		RestrictedPersonIdentificationSchemeNameSEPA restrictedPersonIdentificationSchemeNameSEPA = new RestrictedPersonIdentificationSchemeNameSEPA();
		restrictedPersonIdentificationSchemeNameSEPA.setPrtry(IdentificationSchemeNameSEPA.SEPA);		
		restrictedPersonIdentificationSEPA.setSchmeNm(restrictedPersonIdentificationSchemeNameSEPA);
		personIdentificationSEPA2.setOthr(restrictedPersonIdentificationSEPA);
		partySepa2.setPrvtId(personIdentificationSEPA2);
		partyIdentificationSEPA3.setId(partySepa2);		
		pi.setCdtrSchmeId(partyIdentificationSEPA3);			
		
		return pi;
	}
	
	
	
	private DirectDebitTransactionInformationSDD getNewDirectDebitTransactionInformationSDD(final Kunde kunde, final SepaData sepaData) {
		
		DirectDebitTransactionInformationSDD dti = new DirectDebitTransactionInformationSDD();
		
		PaymentIdentificationSEPA paymentIdentificationSEPA = new PaymentIdentificationSEPA();
		paymentIdentificationSEPA.setInstrId(String.valueOf(sepaData.getRech_nr()));
		paymentIdentificationSEPA.setEndToEndId(String.valueOf("R" + sepaData.getRech_nr() + " K" + String.valueOf(kunde.getKun())));
		dti.setPmtId(paymentIdentificationSEPA);
		
		ActiveOrHistoricCurrencyAndAmountSEPA activeOrHistoricCurrencyAndAmountSEPA = new ActiveOrHistoricCurrencyAndAmountSEPA();
		activeOrHistoricCurrencyAndAmountSEPA.setCcy(ActiveOrHistoricCurrencyCodeEUR.EUR);
		BigDecimal amount = round(sepaData.getLastbetrag(), 2, BigDecimal.ROUND_HALF_UP);
		activeOrHistoricCurrencyAndAmountSEPA.setValue(amount);
		dti.setInstdAmt(activeOrHistoricCurrencyAndAmountSEPA);
		
		DirectDebitTransactionSDD directDebitTransactionSDD = new DirectDebitTransactionSDD();
		MandateRelatedInformationSDD mandateRelatedInformationSDD = new MandateRelatedInformationSDD();
		mandateRelatedInformationSDD.setMndtId(kunde.getMandatref());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat df2 = new SimpleDateFormat("dd.MM.yyyy");
		mandateRelatedInformationSDD.setDtOfSgntr(df.format(kunde.getMandseit()));
		directDebitTransactionSDD.setMndtRltdInf(mandateRelatedInformationSDD);		
		dti.setDrctDbtTx(directDebitTransactionSDD);
		
		
		BranchAndFinancialInstitutionIdentificationSEPA3 br = new BranchAndFinancialInstitutionIdentificationSEPA3();
		FinancialInstitutionIdentificationSEPA3 fi = new FinancialInstitutionIdentificationSEPA3();
		fi.setBIC(kunde.getSwift());
		br.setFinInstnId(fi);
		dti.setDbtrAgt(br);
		
		PartyIdentificationSEPA2 partyIdentificationSEPA2 = new PartyIdentificationSEPA2();
		partyIdentificationSEPA2.setNm(kunde.getKun_krz1());
		dti.setDbtr(partyIdentificationSEPA2);
		
		CashAccountSEPA2 cashAccountSEPA2 = new CashAccountSEPA2();
		AccountIdentificationSEPA accountIdentificationSEPA = new AccountIdentificationSEPA();
		accountIdentificationSEPA.setIBAN(kunde.getIban());
		cashAccountSEPA2.setId(accountIdentificationSEPA);
		dti.setDbtrAcct(cashAccountSEPA2);
		
		RemittanceInformationSEPA1Choice remittanceInformationSEPA1Choice = new RemittanceInformationSEPA1Choice();
		remittanceInformationSEPA1Choice.setUstrd("Rechnung " + sepaData.getRech_nr() + " Kunde " + String.valueOf(kunde.getKun()) + " Rech.Datum " + df2.format(sepaData.getRech_dat()));
		dti.setRmtInf(remittanceInformationSEPA1Choice);		
		
		return dti;
		
	}
	
	

}
