package de.fit.sepa.data;


import java.util.ArrayList;
import java.sql.Date;
import java.util.Iterator;

public class Kunde {
	
	private int kun;
	private String kun_krz1;	
	private int blz;
	private String kto_nr;
	private String iban;
	private String swift;
	private short separart;
	private short tagesepa;
	private String mandatref;
	private String hausiban;
	private String hausswift;
	private String hausbank;
	private boolean btchbook;
	private Date mandseit;
	private Date faellig;
	private ArrayList<SepaData> sepas;

	
	
    public Kunde(int kun, String kun_krz1, int blz, String kto_nr,
			String iban, String swift, short separart, short tagesepa,
			String mandatref, String hausiban, String hausswift, String hausbank, Date mandseit2, boolean btchbook) {
		this.kun = kun;
		this.kun_krz1 = (kun_krz1 != null) ? kun_krz1.trim():"";
		this.blz = blz;
		this.kto_nr = (kto_nr != null) ? kto_nr.trim():"";
		this.iban = (iban != null) ? iban.trim():"";
		this.swift = (swift != null) ? swift.trim():"";
		this.separart = separart;
		this.tagesepa = tagesepa;
		this.mandatref = (mandatref != null) ? mandatref.trim():"";
		this.hausiban = (hausiban != null) ? hausiban.trim():"";
		this.hausswift = (hausswift != null) ? hausswift.trim():"";
		this.hausbank = (hausbank != null) ? hausbank.trim():"";
		this.mandseit = mandseit2;
		this.setBtchbook(btchbook);
		this.sepas = new ArrayList<SepaData>();
	}
    

	public ArrayList<SepaData> getSepaData() {
        return this.sepas;
    }
	
	
	public void addSepaData(final SepaData sepaData) {
		this.sepas.add(sepaData);
	}


	public int getKun() {
		return kun;
	}


	public void setKun(int kun) {
		this.kun = kun;
	}


	public String getKun_krz1() {
		return kun_krz1;
	}


	public void setKun_krz1(String kun_krz1) {
		this.kun_krz1 = kun_krz1;
	}


	public int getBlz() {
		return blz;
	}


	public void setBlz(int blz) {
		this.blz = blz;
	}


	public String getKto_nr() {
		return kto_nr;
	}


	public void setKto_nr(String kto_nr) {
		this.kto_nr = kto_nr;
	}


	public String getIban() {
		return iban;
	}


	public void setIban(String iban) {
		this.iban = iban;
	}


	public String getSwift() {
		return swift;
	}


	public void setSwift(String swift) {
		this.swift = swift;
	}


	public short getSeparart() {
		return separart;
	}


	public void setSeparart(short separart) {
		this.separart = separart;
	}


	public short getTagesepa() {
		return tagesepa;
	}


	public void setTagesepa(short tagesepa) {
		this.tagesepa = tagesepa;
	}


	public String getMandatref() {
		return mandatref;
	}


	public void setMandatref(String mandatref) {
		this.mandatref = mandatref;
	}


	public String getHausiban() {
		return hausiban;
	}


	public void setHausiban(String hausiban) {
		this.hausiban = hausiban;
	}


	public String getHausswift() {
		return hausswift;
	}


	public void setHausswift(String hausswift) {
		this.hausswift = hausswift;
	}


	public String getHausbank() {
		return hausbank;
	}


	public void setHausbank(String hausbank) {
		this.hausbank = hausbank;
	}


	public ArrayList<SepaData> getSepas() {
		return sepas;
	}


	public void setSepas(ArrayList<SepaData> sepas) {
		this.sepas = sepas;
	}
	
	
	public Iterator<SepaData> getSepaDataIterator() {
		return this.sepas.iterator();
	}


	/**
	 * @return the mandseit
	 */
	public Date getMandseit() {
		
		if ( mandseit != null ) {
			return mandseit;	
		} else {
			java.util.Date now = new java.util.Date();
			return new Date(now.getTime());
		}
		
	}


	/**
	 * @param mandseit the mandseit to set
	 */
	public void setMandseit(Date mandseit) {
		this.mandseit = mandseit;
	}


	/**
	 * @return the btchbook
	 */
	public boolean isBtchbook() {
		return btchbook;
	}


	/**
	 * @param btchbook the btchbook to set
	 */
	public void setBtchbook(boolean btchbook) {
		this.btchbook = btchbook;
	}


	/**
	 * @return the faellig
	 */
	public Date getFaellig() {
		return faellig;
	}


	/**
	 * @param faellig the faellig to set
	 */
	public void setFaellig(Date faellig) {
		this.faellig = faellig;
	}

}
