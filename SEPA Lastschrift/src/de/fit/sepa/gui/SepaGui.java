package de.fit.sepa.gui;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.BatchUpdateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.swing.JFormattedTextField;

import de.fit.sepa.SequenceType1Code;
import de.fit.sepa.data.Kunde;
import de.fit.sepa.data.SepaData;
import de.fit.sepa.main.XMLCreator;
import javax.swing.JToggleButton;
import javax.swing.ButtonGroup;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import sun.text.normalizer.Trie.DataManipulate;
import javax.swing.JCheckBox;
import javax.xml.bind.JAXBException;

import org.jdesktop.swingx.JXDatePicker;
import org.xml.sax.SAXException;

public class SepaGui extends JFrame {

	private static JPanel contentPane;
	private int sepaart = 0;
	private static int zahl_art = 3;
	private boolean simulation = false;
	private JLabel lblNewLabel;
	private JTextField kunde_von;
	private JTextField kunde_bis;
	private JTextField rechnr_bis;
	private JTextField rechnr_von;
	private JXDatePicker rechdat_bis;
	private JXDatePicker rechdat_von;
	private JToggleButton tglbtnNewToggleButton;
	private JToggleButton tglbtnFirmenLastschrift;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private SequenceType1Code seqCode = SequenceType1Code.FRST;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		if (args.length == 1) {
			zahl_art = Integer.valueOf(args[0]);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SepaGui frame = new SepaGui(zahl_art);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		Toolkit.getDefaultToolkit().getSystemEventQueue()
				.push(new EventQueue() {
					protected void dispatchEvent(AWTEvent event) {
						if (event instanceof KeyEvent) {
							KeyEvent keyEvent = (KeyEvent) event;
							if (keyEvent.getID() == KeyEvent.KEY_PRESSED
									&& (keyEvent).getKeyCode() == KeyEvent.VK_F5) {
								int msg = JOptionPane
										.showConfirmDialog(
												contentPane,
												"Wollen Sie die Applikation jetzt beenden?",
												"Beenden",
												JOptionPane.CANCEL_OPTION,
												JOptionPane.INFORMATION_MESSAGE);
								if (msg == JOptionPane.OK_OPTION) {
									System.exit(0);
								}
							}
						}
						super.dispatchEvent(event);
					}
				});
	}

	/**
	 * Create the frame.
	 * 
	 * @param zahl_art
	 */
	public SepaGui(final int zahl_art) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy");
		setTitle("SEPA Lastschriften Export");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 461, 369);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblNewLabel = new JLabel("SEPA Lastschriften exportieren");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(26, 11, 240, 53);
		contentPane.add(lblNewLabel);

		JLabel lblKunde = new JLabel("Kunde");
		lblKunde.setBounds(26, 97, 99, 14);
		contentPane.add(lblKunde);

		JLabel lblRechnungsnummer = new JLabel("Rechnungsnummer");
		lblRechnungsnummer.setBounds(26, 144, 119, 14);
		contentPane.add(lblRechnungsnummer);

		JLabel lblRechnungsdatum = new JLabel("Rechnungsdatum");
		lblRechnungsdatum.setBounds(26, 191, 119, 14);
		contentPane.add(lblRechnungsdatum);

		kunde_von = new JTextField();
		kunde_von.setHorizontalAlignment(SwingConstants.RIGHT);
		kunde_von.setText("1");
		kunde_von.setBounds(155, 94, 126, 20);
		contentPane.add(kunde_von);
		kunde_von.setColumns(10);

		kunde_bis = new JTextField();
		kunde_bis.setText("999999999");
		kunde_bis.setHorizontalAlignment(SwingConstants.RIGHT);
		kunde_bis.setColumns(10);
		kunde_bis.setBounds(298, 94, 126, 20);
		contentPane.add(kunde_bis);

		rechnr_bis = new JTextField();
		rechnr_bis.setText("999999999");
		rechnr_bis.setHorizontalAlignment(SwingConstants.RIGHT);
		rechnr_bis.setColumns(10);
		rechnr_bis.setBounds(298, 141, 126, 20);
		contentPane.add(rechnr_bis);

		rechnr_von = new JTextField();
		rechnr_von.setText("1");
		rechnr_von.setHorizontalAlignment(SwingConstants.RIGHT);
		rechnr_von.setColumns(10);
		rechnr_von.setBounds(155, 138, 126, 20);
		contentPane.add(rechnr_von);

		// Create a date spinner
		rechdat_von = new JXDatePicker(new Date());
		rechdat_von.setBounds(155, 185, 126, 20);
		contentPane.add(rechdat_von);

		rechdat_bis = new JXDatePicker(new Date());
		rechdat_bis.setBounds(298, 185, 126, 20);
		contentPane.add(rechdat_bis);

		JButton btnNewButton = new JButton("Export starten");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (kunde_von.getText().isEmpty()
						|| kunde_bis.getText().isEmpty()
						|| rechnr_von.getText().isEmpty()
						|| rechnr_bis.getText().isEmpty()) {
					JOptionPane
							.showMessageDialog(
									contentPane,
									"Mindestens ein Range Feld beinhaltet nicht g�ltige Werte!",
									"Vorgang nicht m�glich",
									JOptionPane.ERROR_MESSAGE);
				} else {
					XMLCreator xmlCreator = new XMLCreator();
					final Date beginDate = rechdat_von.getDate();
					final Date endDate = rechdat_bis.getDate();
					SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat(
							"dd.MM.yyyy");
					String beginDateSQL = simpleDateTimeFormat.format(beginDate
							.getTime());
					String endDateSQL = simpleDateTimeFormat.format(endDate
							.getTime());

					ArrayList<Kunde> kunden;
					try {
						kunden = xmlCreator.start(beginDateSQL, endDateSQL,
								Integer.valueOf(rechnr_von.getText()),
								Integer.valueOf(rechnr_bis.getText()),
								Integer.valueOf(kunde_von.getText()),
								Integer.valueOf(kunde_bis.getText()), sepaart,
								simulation, zahl_art, seqCode);

						int size = getGlobalSum(kunden);
						JOptionPane.showMessageDialog(contentPane,
								"Es wurden f�r " + kunden.size() + " Kunden "
										+ size + " Lastschriften exportiert",
								"Erfolgreich exportiert",
								JOptionPane.INFORMATION_MESSAGE);

					} catch (NumberFormatException e1) {

						JOptionPane.showMessageDialog(contentPane,
								e1.getMessage(), "Fehler beim SEPA Export",
								JOptionPane.ERROR_MESSAGE);

					} catch (FileNotFoundException e1) {

						JOptionPane.showMessageDialog(contentPane,
								e1.getMessage(), "Fehler beim SEPA Export",
								JOptionPane.ERROR_MESSAGE);

					} catch (JAXBException e1) {

						JOptionPane.showMessageDialog(contentPane,
								e1.getMessage(), "Fehler beim SEPA Export",
								JOptionPane.ERROR_MESSAGE);

					} catch (SAXException e1) {

						JOptionPane.showMessageDialog(contentPane,
								e1.getMessage(), "Fehler beim SEPA Export",
								JOptionPane.ERROR_MESSAGE);

					} catch (IOException e1) {

						JOptionPane.showMessageDialog(contentPane,
								e1.getMessage(), "Fehler beim SEPA Export",
								JOptionPane.ERROR_MESSAGE);

					}

				}

			}
		});
		btnNewButton.setBounds(155, 290, 269, 23);
		contentPane.add(btnNewButton);

		tglbtnNewToggleButton = new JToggleButton("Basis Lastschrift");
		tglbtnNewToggleButton.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if (tglbtnNewToggleButton.isSelected()) {
					sepaart = 0;
				}
			}
		});
		tglbtnNewToggleButton.setSelected(true);
		buttonGroup.add(tglbtnNewToggleButton);
		tglbtnNewToggleButton.setBounds(155, 256, 126, 23);
		contentPane.add(tglbtnNewToggleButton);

		tglbtnFirmenLastschrift = new JToggleButton("Firmen Lastschrift");
		tglbtnFirmenLastschrift.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if (tglbtnFirmenLastschrift.isSelected()) {
					sepaart = 1;
				}
			}
		});
		buttonGroup.add(tglbtnFirmenLastschrift);
		tglbtnFirmenLastschrift.setBounds(298, 256, 126, 23);
		contentPane.add(tglbtnFirmenLastschrift);

		final JCheckBox chckbxNewCheckBox = new JCheckBox("Simulation");
		chckbxNewCheckBox.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if (chckbxNewCheckBox.isSelected()) {
					simulation = true;
				} else {
					simulation = false;
				}
			}
		});
		chckbxNewCheckBox
				.setToolTipText("Lastschriften werden exportiert. Datens\u00E4tze werden in der Datenbank aber nicht ver\u00E4ndert.");
		chckbxNewCheckBox.setHorizontalAlignment(SwingConstants.LEFT);
		chckbxNewCheckBox.setBounds(26, 256, 99, 23);
		contentPane.add(chckbxNewCheckBox);

		final JToggleButton tErstLastSchrift = new JToggleButton(
				"Erstlastschrift");
		tErstLastSchrift.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if (tErstLastSchrift.isSelected()) {
					seqCode = SequenceType1Code.FRST;
				}
			}
		});
		buttonGroup_1.add(tErstLastSchrift);
		tErstLastSchrift.setSelected(true);
		tErstLastSchrift.setBounds(155, 222, 126, 23);
		contentPane.add(tErstLastSchrift);

		final JToggleButton tglbtnFolgelastschrift = new JToggleButton(
				"Folgelastschrift");
		tglbtnFolgelastschrift.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if (tglbtnFolgelastschrift.isSelected()) {
					seqCode = SequenceType1Code.RCUR;
				}
			}
		});
		buttonGroup_1.add(tglbtnFolgelastschrift);
		tglbtnFolgelastschrift.setBounds(298, 222, 126, 23);
		contentPane.add(tglbtnFolgelastschrift);

	}

	private int getGlobalSum(final ArrayList<Kunde> kunden) {

		int temp = 0;

		for (int i = 0; i < kunden.size(); i++) {
			temp += kunden.get(i).getSepaData().size();
		}

		return temp;
	}
}
