package de.fit.sepa.main;

import java.io.*;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;

import de.fit.sepa.*;
import de.fit.sepa.data.Creditor;
import de.fit.sepa.data.DataGenerator;
import de.fit.sepa.data.Kunde;
import de.fit.sepa.data.SepaConverter;
import fit.informixconnector.InformixConnector;

public class XMLCreator {

	private static final String SEPA_XSD = "xsd/pain.008.003.02.xsd";
	private static final Logger logger = Logger.getLogger(XMLCreator.class
			.getName());
	Handler fh = null;

	public ArrayList<Kunde> start(final String beginDateSQL,
			final String endDateSQL, final int rechnr_von,
			final int rechnr_bis, final int kun_von, final int kun_bis,
			final int sepaart, boolean simulation, int zahl_art,
			SequenceType1Code seqCode) throws JAXBException, FileNotFoundException, SAXException, IOException {
		
		String outFilePath = System.getenv("BWSSEPA");
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		String now = df.format(new Date());
		String fileName = "SEPA" + now + ".xml";
		outFilePath = outFilePath + "\\lastschrift\\";
		String outFileName = outFilePath + fileName;
		String outFilePathLogger = outFilePath + "logger\\";
		String charEnc = "UTF-8";

		try {
			fh = new FileHandler(outFilePathLogger + "SEPA_Lastschrift " + now
					+ ".log");
			logger.addHandler(fh);
			fh.setFormatter(new SimpleFormatter());
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		logger.info("Lastschrift SEPA XML wird erzeugt " + outFileName);
		logger.info("Lastschrift SEPA XML Parameter Rechnungsdatum von "
				+ beginDateSQL + " bis " + endDateSQL);
		logger.info("Lastschrift SEPA XML Parameter Rechnungsnummer von "
				+ rechnr_von + " bis " + rechnr_bis);
		logger.info("Lastschrift SEPA XML Parameter Kunde von " + kun_von
				+ " bis " + kun_bis);
		logger.info("Lastschrift SEPA XML Parameter Simulation " + simulation);
		logger.info("Lastschrift SEPA XML Parameter SEPA Art " + sepaart);
		
		final ArrayList<Kunde> kunden = erzeugeXML(outFileName, charEnc,
				beginDateSQL, endDateSQL, rechnr_von, rechnr_bis, kun_von,
				kun_bis, sepaart, simulation, zahl_art, seqCode);

		return kunden;
	}

	private ArrayList<Kunde> erzeugeXML(String outFile, String charEnc,
			String beginDateSQL, String endDateSQL, int rechnr_von,
			int rechnr_bis, int kun_von, int kun_bis, int sepaart,
			boolean simulation, int zahl_art, SequenceType1Code seqCode)
			throws JAXBException, SAXException {

		Document document = null;
		ArrayList<Kunde> kunden = null;
		Creditor creditor = null;

		final InformixConnector informixConnector = new InformixConnector();
		informixConnector.createConnection();
		DataGenerator dataGenerator = new DataGenerator(informixConnector);

		try {
			kunden = dataGenerator
					.getValues(beginDateSQL, endDateSQL, rechnr_von,
							rechnr_bis, kun_von, kun_bis, sepaart, zahl_art);
			creditor = dataGenerator.getCreditorInformations();

			if (kunden != null && !kunden.isEmpty() && creditor != null) {

				logger.info("Anzahl Kunden " + kunden.size());
				SepaConverter sepaConverter = new SepaConverter(kunden,
						creditor);
				logger.info("SEPA Konvertierung erfolgreich");

				try {
					document = sepaConverter.getDocument(seqCode);
				} catch (DatatypeConfigurationException e) {
					e.printStackTrace();
				}

				// create JAXB context and instantiate marshaller
				JAXBContext context = JAXBContext.newInstance(Document.class);

				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				m.setProperty(Marshaller.JAXB_ENCODING, charEnc);

				// Write to System.out
				m.marshal(document, System.out);

				// Write to File
				m.marshal(document, new File(outFile));
				logger.info("XML erfolgreich erstellt");				

				if (kunden != null && !kunden.isEmpty()) {
					
					XMLCreator.validate(SEPA_XSD, new InputStreamReader(
							new FileInputStream(outFile)));
					logger.info("Lastschrift SEPA XML Validierung erfolgreich");
					
					if (!simulation) {
						dataGenerator.updateRechStat(kunden);
						logger.info("RechnungsStatus auf 'Bezahlt' gesetzt.");
						dataGenerator.loggingActionToDB(kunden);
						logger.info("Export in die Datenbank protokolliert.");
					}
					
				}
			}

		} catch (SQLException | IOException e) {
			e.printStackTrace();

		} finally {
			informixConnector.closeConnection();
		}
		
		return kunden;
	}

	/**
	 * Validierung des XML-Readers gegen ein XSD-Schema.<br>
	 * <b>Achtung:</b> Zum XML-Reader wird kein close() aufgerufen.
	 * 
	 * @param xsdSchema
	 *            XSD-Schema
	 * @param xml
	 *            XML-Reader
	 */
	public static void validate(String xsdSchema, Reader xml)
			throws SAXException, IOException {
		SchemaFactory schemaFactory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(new File(xsdSchema));
		Validator validator = schema.newValidator();
		validator.validate(new StreamSource(xml));
	}

}
