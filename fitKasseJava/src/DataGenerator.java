

import java.text.SimpleDateFormat;
import java.util.Date;

import fit.informixconnector.InformixConnector;


public class DataGenerator {
	
	private InformixConnector informixConnector;


	public DataGenerator (final InformixConnector informixConnector) {
		this.informixConnector = informixConnector;		
	}
	
	
	public void saveValues(final Werte werte) {

		Date sqlDate = new java.sql.Date(werte.getRechDate().getTime());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
		String rechDate = simpleDateFormat.format(sqlDate);
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("HHmmss");
		String rechTime = simpleDateFormat2.format(sqlDate);
		
		String insertstmt = 
				"INSERT INTO kase_fit" +
				"(mdn," +
				"fil," +
				"kasse," +
				"bon," +
				"betrag_bto," +
				"gegeben," +
				"zurueck," +
				"gut_sum," +
				"essengut_sum," +
				"gut_anzahl," +
				"essengut_anzahl," +
				"zahlart," +
				"bon_date," +
				"pers_nam," +
				"bon_time" +
				")	VALUES (" +
				werte.getMdn() + "," +
				werte.getFil() + "," +
				werte.getKasse() + "," +
				werte.getRechNr() + "," +
				werte.getBetrag() + "," +
				werte.getGegeben() + "," +
				werte.getZurueck() + "," +
				werte.getBetragGutscheinGesamt() + "," +
				werte.getBetragEssensgutscheinGesamt() + "," +
				werte.getAnzahlGutschein() + "," +
				werte.getAnzahlEssensgutschein() + "," +
				werte.getZahlArt() + ",'" +
				rechDate + "','" +
				werte.getBenutzer() + "','" +
				rechTime + "'" +
				"); ";
		
		this.informixConnector.executeStatement(insertstmt);
			
	}	
}
