/*
 * @(#)SimpleWrite.java	1.12 98/06/25 SMI
 *
 * Copyright (c) 1998 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Sun grants you ("Licensee") a non-exclusive, royalty free, license 
 * to use, modify and redistribute this software in source and binary
 * code form, provided that i) this copyright notice and license appear
 * on all copies of the software; and ii) Licensee does not utilize the
 * software in a manner which is disparaging to Sun.
 *
 * This software is provided "AS IS," without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND
 * ITS LICENSORS SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY
 * LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THE
 * SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS
 * BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING
 * OUT OF THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * This software is not designed or intended for use in on-line control
 * of aircraft, air traffic, aircraft navigation or aircraft
 * communications; or in the design, construction, operation or
 * maintenance of any nuclear facility. Licensee represents and
 * warrants that it will not use or redistribute the Software for such
 * purposes.
 */

import java.io.*;
import java.util.*;
import javax.comm.*;

public class SimpleWrite {
    
	private Enumeration portList;
	private CommPortIdentifier portId;
	private String messageString = "Hello, world! Test";
	private String messageString2 = "Hello, world2! Test2";
	private SerialPort serialPort;
	private OutputStream outputStream;
	private String comPort;
	
	
	public SimpleWrite(final String comPort) {
		
		this.comPort = comPort;
	}
	

    public void send(final String line1, final String line2) throws IOException, UnsupportedCommOperationException, PortInUseException {
    	
    	messageString = line1;
    	messageString2 = line2;
    	
        portList = CommPortIdentifier.getPortIdentifiers();

        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier) portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (portId.getName().equals(this.comPort)) {
                //if (portId.getName().equals("/dev/term/a")) {
                	if ( !portId.isCurrentlyOwned() ) {
                		
                		serialPort = (SerialPort) portId.open("SimpleWriteApp", 0);
                        outputStream = serialPort.getOutputStream();                
                        serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
                    	
                        char mychar = 12;
                    	byte c = (byte) mychar;
                    	outputStream.write(c);
                        outputStream.write(messageString.getBytes());
                        mychar = 10;
                    	c = (byte) mychar;
                    	outputStream.write(c);
                    	mychar = 13;
                    	c = (byte) mychar;
                    	outputStream.write(c);
                    	outputStream.write(messageString2.getBytes());
                    	serialPort.close();
                     
                	}
                	   
                }
            }
        }
    }
    
}
