import java.util.Date;


public class Werte {
	
	
	private int mdn;
	private int fil;
	private int kasse;
	private int rechNr;	
	private String blgTyp;
	private String benutzer;
	private Date rechDate;
	private double betrag;
	
	private double gegeben;
	private double zurueck;
	
	private double betragGutscheinGesamt = 0;
	private int anzahlEssensgutschein = 0;
	private int anzahlGutschein = 0;
	private double betragEssensgutscheinGesamt = 0;
	private int zahlArt = 2;	
	
	
	
	public Werte(final int mdn, final int fil, final int kasse, final int rechNr, final String benutzer,			
			final double betrag) {
		
		this.setMdn(mdn);
		this.setFil(fil);
		this.setKasse(kasse);
		this.setRechNr(rechNr);
		this.setBenutzer(benutzer);
		this.setBetrag(betrag);
		
		this.setBlgTyp("K");
		this.setRechDate(new Date());
	}



	public int getMdn() {
		return mdn;
	}



	private void setMdn(int mdn) {
		this.mdn = mdn;
	}



	public int getFil() {
		return fil;
	}



	private void setFil(int fil) {
		this.fil = fil;
	}



	public int getKasse() {
		return kasse;
	}



	private void setKasse(int kasse) {
		this.kasse = kasse;
	}



	public int getRechNr() {
		return rechNr;
	}



	private void setRechNr(int rechNr) {
		this.rechNr = rechNr;
	}



	public String getBlgTyp() {
		return blgTyp;
	}



	private void setBlgTyp(String blgTyp) {
		this.blgTyp = blgTyp;
	}



	public String getBenutzer() {
		return benutzer;
	}



	private void setBenutzer(String benutzer) {
		this.benutzer = benutzer;
	}



	public Date getRechDate() {
		return rechDate;
	}



	private void setRechDate(Date rechDate) {
		this.rechDate = rechDate;
	}



	public double getBetrag() {
		return betrag;
	}



	private void setBetrag(double betrag) {
		this.betrag = betrag;
	}



	public double getGegeben() {
		return gegeben;
	}



	public void setGegeben(double gegeben) {
		this.gegeben = gegeben;
	}



	public double getZurueck() {
		return zurueck;
	}



	public void setZurueck(double zurueck) {
		this.zurueck = zurueck;
	}



	public double getBetragGutscheinGesamt() {
		return betragGutscheinGesamt;
	}



	public void setBetragGutschein(double betragGutschein) {
		if ( betragGutschein > 0 ) {
			this.anzahlGutschein++;	
		}
		this.betragGutscheinGesamt = betragGutschein;		
	}



	public int getAnzahlEssensgutschein() {
		return anzahlEssensgutschein;
	}



	public int getAnzahlGutschein() {
		return anzahlGutschein;
	}



	public double getBetragEssensgutscheinGesamt() {
		return betragEssensgutscheinGesamt;
	}



	public void addBetragEssensgutschein(double betragEssensGutschein, int anzahl) {
		this.betragEssensgutscheinGesamt = this.betragEssensgutscheinGesamt + betragEssensGutschein;
		this.anzahlEssensgutschein = this.anzahlEssensgutschein + anzahl;
	}



	public int getZahlArt() {
		return zahlArt;
	}



	public void setZahlArt(int zahlArt) {
		this.zahlArt = zahlArt;
	}
	
}
