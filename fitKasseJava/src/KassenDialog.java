import javax.comm.PortInUseException;
import javax.comm.UnsupportedCommOperationException;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Window;

import javax.swing.AbstractButton;
import javax.swing.JLayeredPane;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Enumeration;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

import fit.informixconnector.InformixConnector;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class KassenDialog extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3453606934760337110L;
	/**
	 * 
	 */
	
	boolean gutscheinAktiviert = false;
	boolean essenGutscheinAktiviert = false;
	private double gegebenGesamt = 0;
	private double gegebenScheine = 0;
	private double gegebenBetrag = 0;
	private double gegebenGutscheinScheine = 0;
	private double gegebenGutscheinBetrag = 0;
	private double zurueckBetrag = 0;
	private double offenerBetrag = 0;
	private double gegebenEssensgutscheine = 0;
	private String gegebenBetragGutscheinString = "";
	private String rechBetragEssenGutscheinString = "";
	private String multiBetragEssenGutscheinString = "";
	private boolean malAktiviert = false;
	private double multiplyBetragEssenGutschein = 0;
	private double rechBetragEssenGutschein = 0;
	private String gegebenBetragString = "";
	private boolean gedruckt = false;
	private String belderPath;

	/**
	 * Launch the application.
	 */
	
	
	public static void main(final String[] args) {		
		
		try {
			final int mdn = Integer.valueOf(args[0]);
			final int fil = Integer.valueOf(args[1]);
			final int kasse = Integer.valueOf(args[2]);
			final int rechNr = Integer.valueOf(args[3]);
			final double brutto  = Double.valueOf(args[4]);
			final String benutzer = args[5];
			
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                try {
	                	UIManager.setLookAndFeel( "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel" );
	                	
	                	String comPortString = "";
	                	
	        			if ( args.length == 7 ) {
	        				comPortString = args[6];	        				
	        			}
	                	
	        			KassenDialog frame = new KassenDialog(mdn, fil, kasse, rechNr, brutto, benutzer, comPortString);	
	        			frame.setExtendedState(MAXIMIZED_BOTH);
	        			frame.setIgnoreRepaint(true);
	        			frame.setVisible(true);	        			
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
	            }
	        });				
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	

	/**
	 * Create the frame.
	 */
	public KassenDialog(final int mdn, final int fil, final int kasse, final int rechNr, 
			final double brutto, final String benutzer, final String comPortString) {
		setType(Type.UTILITY);
		
		final JLayeredPane contentPane;
		final DecimalFormat decimalFormat = new DecimalFormat("####0.00");
		final Werte werte;
		final ButtonGroup buttonGroup_1 = new ButtonGroup();
		
		final SimpleWrite simpleWrite = new SimpleWrite(comPortString);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setAlwaysOnTop(true);				
		
		final Rech rech = new Rech(mdn, fil, kasse, rechNr, brutto, benutzer);
		werte = new Werte(mdn, fil, kasse, rechNr, benutzer, brutto);	
		
		setTitle("fit Kassenmodul");
		String fitPath = System.getenv("BWS");
		setBounds(100, 100, 1024, 768);
		contentPane = new JLayeredPane();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEmailRechnungsversand = new JLabel("Kassenmodul");
		lblEmailRechnungsversand.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblEmailRechnungsversand.setBounds(74, 11, 730, 50);
		contentPane.add(lblEmailRechnungsversand);
		
		JTextField RechNr = new JTextField();
		RechNr.setHorizontalAlignment(SwingConstants.RIGHT);
		RechNr.setEditable(false);
		RechNr.setFont(new Font("Tahoma", Font.PLAIN, 14));
		RechNr.setBounds(124, 74, 148, 31);
		RechNr.setText(String.valueOf(rech.getRechNr()));
		contentPane.add(RechNr);
		RechNr.setColumns(10);
		
		JLabel LRechNr = new JLabel("Bon Nummer");
		LRechNr.setFont(new Font("Tahoma", Font.PLAIN, 14));
		LRechNr.setBounds(10, 74, 115, 29);
		contentPane.add(LRechNr);
		
		JLabel LKassierer = new JLabel("Kassierer");
		LKassierer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		LKassierer.setBounds(10, 111, 115, 29);
		contentPane.add(LKassierer);
		
		JTextField Kassierer = new JTextField();
		Kassierer.setHorizontalAlignment(SwingConstants.RIGHT);
		Kassierer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		Kassierer.setEditable(false);
		Kassierer.setColumns(10);
		Kassierer.setBounds(124, 111, 148, 29);
		Kassierer.setText(rech.getBenutzer());
		contentPane.add(Kassierer);
		
		JLabel LBetrag = new JLabel("Betrag");
		LBetrag.setFont(new Font("Tahoma", Font.PLAIN, 24));
		LBetrag.setBounds(10, 201, 115, 53);
		contentPane.add(LBetrag);
		
		JTextField Betrag = new JTextField();
		Betrag.setHorizontalAlignment(SwingConstants.RIGHT);
		Betrag.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Betrag.setEditable(false);
		Betrag.setColumns(10);
		Betrag.setBounds(124, 200, 148, 53);
		Betrag.setText(decimalFormat.format(rech.getBrutto()));
		contentPane.add(Betrag);
		
		JLabel LGegeben = new JLabel("Gegeben");
		LGegeben.setFont(new Font("Tahoma", Font.PLAIN, 24));
		LGegeben.setBounds(10, 296, 115, 53);
		contentPane.add(LGegeben);
		
		final JTextField Gegeben = new JTextField();
		Gegeben.setHorizontalAlignment(SwingConstants.RIGHT);
		Gegeben.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Gegeben.setEditable(false);
		Gegeben.setColumns(10);
		Gegeben.setBounds(124, 295, 148, 53);
		Gegeben.setText(decimalFormat.format(gegebenBetrag));
		contentPane.add(Gegeben);
		
		final JTextField Offen = new JTextField();
		Offen.setText("0.0");
		Offen.setHorizontalAlignment(SwingConstants.RIGHT);
		Offen.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Offen.setEditable(false);
		Offen.setColumns(10);
		Offen.setBounds(124, 480, 148, 57);
		Offen.setText(String.valueOf(decimalFormat.format(rech.getBrutto())));	
		contentPane.add(Offen);
		
		final JButton btnxfc = new JButton("BON");
		
		JLabel LZurueck = new JLabel("Zur\u00FCck");
		LZurueck.setFont(new Font("Tahoma", Font.PLAIN, 24));
		LZurueck.setBounds(10, 385, 115, 53);
		contentPane.add(LZurueck);
		
		final JTextField Zurueck = new JTextField();
		Zurueck.setHorizontalAlignment(SwingConstants.RIGHT);
		Zurueck.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Zurueck.setEditable(false);
		Zurueck.setColumns(10);
		Zurueck.setBounds(124, 385, 148, 53);
		Zurueck.setText(String.valueOf(decimalFormat.format(zurueckBetrag)));		
		contentPane.add(Zurueck);
		
		final JButton Euro5 = new JButton("");
		Euro5.setSelectedIcon(null);
		Euro5.setIcon(new ImageIcon(fitPath + "\\bilder\\EUR_5.jpg"));
		Euro5.setBackground(Color.GRAY);
		Euro5.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Euro5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				berechneScheine(5.00);
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		Euro5.setBounds(295, 73, 231, 85);
		contentPane.add(Euro5);
		
		final JButton Euro20 = new JButton("");
		Euro20.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				berechneScheine(20.00);
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		Euro20.setIcon(new ImageIcon(fitPath + "\\bilder\\EUR_20.jpg"));
		Euro20.setBackground(new Color(51, 153, 204));
		Euro20.setForeground(new Color(0, 0, 0));
		Euro20.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Euro20.setBounds(777, 72, 231, 85);
		contentPane.add(Euro20);
		
		final JButton Euro50 = new JButton("");
		Euro50.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				berechneScheine(50.00);
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		Euro50.setIcon(new ImageIcon(fitPath + "\\bilder\\EUR_50.jpg"));
		Euro50.setBackground(new Color(255, 153, 0));
		Euro50.setForeground(new Color(0, 0, 0));
		Euro50.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Euro50.setBounds(295, 169, 231, 85);
		contentPane.add(Euro50);
		
		
		final JButton Euro100 = new JButton("");
		Euro100.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				berechneScheine(100.00);
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		Euro100.setForeground(Color.BLACK);
		Euro100.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Euro100.setBackground(new Color(51, 204, 102));
		Euro100.setBounds(536, 169, 231, 85);
		Euro100.setIcon(new ImageIcon(fitPath + "\\bilder\\EUR_100.jpg"));
		contentPane.add(Euro100);
		
		final JButton Euro200 = new JButton("");
		Euro200.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				berechneScheine(200.00);
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		Euro200.setForeground(Color.BLACK);
		Euro200.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Euro200.setBackground(new Color(255, 255, 153));
		Euro200.setIcon(new ImageIcon(fitPath + "\\bilder\\EUR_200.jpg"));
		Euro200.setBounds(777, 166, 231, 85);
		contentPane.add(Euro200);
		
		
		final JButton Euro500 = new JButton("");
		Euro500.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				berechneScheine(500.00);
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		Euro500.setForeground(Color.BLACK);
		Euro500.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Euro500.setBackground(new Color(255, 204, 255));
		Euro500.setIcon(new ImageIcon(fitPath + "\\bilder\\EUR_500.jpg"));
		Euro500.setBounds(536, 264, 231, 85);
		contentPane.add(Euro500);
		
		
		
		final JButton Euro10 = new JButton("");
		Euro10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				berechneScheine(10.00);
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		Euro10.setIcon(new ImageIcon(fitPath + "\\bilder\\EUR_10.jpg"));
		Euro10.setForeground(new Color(0, 0, 0));
		Euro10.setBackground(new Color(255, 153, 153));
		Euro10.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Euro10.setBounds(536, 73, 231, 85);
		contentPane.add(Euro10);
		
		final JButton Gutschein = new JButton("Gutschein");
		Gutschein.setBackground(Color.LIGHT_GRAY);
		
		final JButton Essensgutschein = new JButton("Essensgutschein");
		Essensgutschein.setBackground(Color.LIGHT_GRAY);
		Essensgutschein.setToolTipText("Bitte Betrag * Menge eingeben");
		Essensgutschein.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				boolean berechne = neuerEssensGutschein(Gutschein, Essensgutschein, werte);
				if ( berechne ) {
					berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
				}
				
			}
		});
		Essensgutschein.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Essensgutschein.setBounds(777, 380, 221, 67);
		contentPane.add(Essensgutschein);
		
		
		Gutschein.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if ( gutscheinAktiviert ) {
					gutscheinAktiviert = false;
					Gutschein.setBackground(Color.LIGHT_GRAY);
				} else {
					gutscheinAktiviert = true;
					Gutschein.setBackground(Color.GREEN);
					essenGutscheinAktiviert = false;
					Essensgutschein.setBackground(Color.LIGHT_GRAY);					
				}				
			}
		});
		Gutschein.setFont(new Font("Tahoma", Font.PLAIN, 24));
		Gutschein.setBounds(777, 458, 221, 67);
		contentPane.add(Gutschein);
		
		final JButton BSieben = new JButton("7");
		BSieben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numberFieldChange("7");
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		contentPane.setLayer(BSieben, 0);
		BSieben.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BSieben.setBackground(Color.GRAY);
		BSieben.setBounds(295, 380, 96, 67);
		contentPane.add(BSieben);
		
		final JButton BAcht = new JButton("8");
		BAcht.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numberFieldChange("8");
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		BAcht.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BAcht.setBackground(Color.GRAY);
		BAcht.setBounds(401, 380, 96, 67);
		contentPane.add(BAcht);
		
		final JButton BNeun = new JButton("9");
		BNeun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numberFieldChange("9");
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		BNeun.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BNeun.setBackground(Color.GRAY);
		BNeun.setBounds(507, 380, 96, 67);
		contentPane.add(BNeun);
		
		final JButton BVier = new JButton("4");
		BVier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numberFieldChange("4");
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		BVier.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BVier.setBackground(Color.GRAY);
		BVier.setBounds(295, 458, 96, 67);
		contentPane.add(BVier);
		
		final JButton BFuenf = new JButton("5");
		BFuenf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numberFieldChange("5");
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		BFuenf.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BFuenf.setBackground(Color.GRAY);
		BFuenf.setBounds(401, 458, 96, 67);
		contentPane.add(BFuenf);
		
		final JButton BSechs = new JButton("6");
		BSechs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numberFieldChange("6");
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		BSechs.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BSechs.setBackground(Color.GRAY);
		BSechs.setBounds(507, 458, 96, 67);
		contentPane.add(BSechs);
		
		final JButton BEins = new JButton("1");
		BEins.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numberFieldChange("1");
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		BEins.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BEins.setBackground(Color.GRAY);
		BEins.setBounds(295, 536, 96, 67);
		contentPane.add(BEins);
		
		final JButton BZwei = new JButton("2");
		BZwei.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numberFieldChange("2");
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		BZwei.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BZwei.setBackground(Color.GRAY);
		BZwei.setBounds(401, 536, 96, 67);
		contentPane.add(BZwei);
		
		final JButton BDrei = new JButton("3");
		BDrei.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numberFieldChange("3");
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		BDrei.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BDrei.setBackground(Color.GRAY);
		BDrei.setBounds(507, 536, 96, 67);
		contentPane.add(BDrei);
		
		final JButton BNull = new JButton("0");
		BNull.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numberFieldChange("0");				
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		BNull.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BNull.setBackground(Color.GRAY);
		BNull.setBounds(295, 614, 96, 67);
		contentPane.add(BNull);
		
		final JButton BCancel = new JButton("C");
		BCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				init(rech, Gegeben, Zurueck, Offen, decimalFormat, btnxfc);
			}
		});
		BCancel.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BCancel.setBackground(Color.LIGHT_GRAY);
		BCancel.setBounds(401, 614, 96, 67);
		contentPane.add(BCancel);
		
		final JButton btnHauptseite = new JButton("Hauptseite");
		btnHauptseite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);								
			}
		});
		btnHauptseite.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnHauptseite.setBackground(Color.RED);
		btnHauptseite.setBounds(777, 536, 221, 67);
		contentPane.add(btnHauptseite);
		
		
		
		final JButton BNachdruck = new JButton("BON Nachdruck");
		BNachdruck.setEnabled(false);
		BNachdruck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				callBelder(werte);				
			}
		});
		BNachdruck.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BNachdruck.setBounds(777, 614, 221, 67);
		contentPane.add(BNachdruck);
		
		final JButton BMal = new JButton("*");
		BMal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				malAktiviert=true;
			}
		});
		BMal.setFont(new Font("Tahoma", Font.PLAIN, 24));
		BMal.setBackground(Color.LIGHT_GRAY);
		BMal.setBounds(507, 614, 96, 67);
		contentPane.add(BMal);
		
		
		final JButton btnPassendGegeben = new JButton("Passend gegeben");
		btnPassendGegeben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				berechneScheine(rech.getBrutto());
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		btnPassendGegeben.setForeground(UIManager.getColor("Button.foreground"));
		btnPassendGegeben.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnPassendGegeben.setBackground(Color.GRAY);
		btnPassendGegeben.setBounds(295, 264, 231, 85);
		contentPane.add(btnPassendGegeben);
		
		final JRadioButton rdbtnBarzahlung = new JRadioButton("Barzahlung");
		rdbtnBarzahlung.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				init(rech, Gegeben, Zurueck, Offen, decimalFormat, btnxfc);
			}
		});
		rdbtnBarzahlung.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				init(rech, Gegeben, Zurueck, Offen, decimalFormat, btnxfc);
			}
			public void mousePressed(MouseEvent arg0) {
				init(rech, Gegeben, Zurueck, Offen, decimalFormat, btnxfc);
			}
		});
		rdbtnBarzahlung.setHorizontalAlignment(SwingConstants.LEFT);
		buttonGroup_1.add(rdbtnBarzahlung);
		rdbtnBarzahlung.setSelected(true);
		rdbtnBarzahlung.setFont(new Font("Tahoma", Font.PLAIN, 26));
		rdbtnBarzahlung.setBounds(124, 577, 157, 50);
		contentPane.add(rdbtnBarzahlung);
		
		final JRadioButton rdbtnEcCash = new JRadioButton("EC- Cash");
		rdbtnEcCash.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				init(rech, Gegeben, Zurueck, Offen, decimalFormat, btnxfc);
				berechneScheine(rech.getBrutto());
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		rdbtnEcCash.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				init(rech, Gegeben, Zurueck, Offen, decimalFormat, btnxfc);
				berechneScheine(rech.getBrutto());
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
			@Override
			public void mousePressed(MouseEvent arg0) {
				init(rech, Gegeben, Zurueck, Offen, decimalFormat, btnxfc);
				berechneScheine(rech.getBrutto());
				berechne(decimalFormat, rech, Gegeben, Zurueck, Offen, btnxfc);
			}
		});
		rdbtnEcCash.setHorizontalAlignment(SwingConstants.LEFT);
		buttonGroup_1.add(rdbtnEcCash);
		rdbtnEcCash.setFont(new Font("Tahoma", Font.PLAIN, 26));
		rdbtnEcCash.setBounds(124, 630, 157, 50);
		contentPane.add(rdbtnEcCash);		
		
		btnxfc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				double fehlbetrag = rech.getBrutto() - gegebenGesamt;				
								
				if ( fehlbetrag <= 0 ) {
					
					if ( !gedruckt  ) {
						
						final String computername = System.getenv("COMPUTERNAME");
					    OutputStream out = null;
						try {
							
							out = new FileOutputStream( "\\\\" + computername + "\\TMT70" );
							PrintWriter printWriter = new PrintWriter(out);
							char[] p = new char[5];
							p[0] = 27;
							p[1] = 112;
							p[2] = 0;
							p[3] = 100;
							p[4] = 100;
														
							printWriter.println(p);
							printWriter.close();
							
						} catch (FileNotFoundException e2) {
							e2.printStackTrace();
						}
							        
						
						callBelder(werte);
						btnxfc.setEnabled(false);
						schreibeWerteinErgebnisObjekt(werte, gegebenGesamt, zurueckBetrag, gegebenGutscheinScheine, gegebenGutscheinBetrag, buttonGroup_1);											
												
						BNull.setEnabled(false);
						BEins.setEnabled(false);
						BZwei.setEnabled(false);
						BDrei.setEnabled(false);
						BVier.setEnabled(false);
						BFuenf.setEnabled(false);
						BSechs.setEnabled(false);
						BSieben.setEnabled(false);
						BAcht.setEnabled(false);
						BNeun.setEnabled(false);
						BCancel.setEnabled(false);
						btnHauptseite.setEnabled(false);
						Euro5.setEnabled(false);
						Euro10.setEnabled(false);
						Euro20.setEnabled(false);
						Euro50.setEnabled(false);
						Euro100.setEnabled(false);
						Euro200.setEnabled(false);
						Euro500.setEnabled(false);
						Essensgutschein.setEnabled(false);
						Gutschein.setEnabled(false);
						BNachdruck.setEnabled(true);
						BMal.setEnabled(false);
						rdbtnEcCash.setEnabled(false);
						rdbtnBarzahlung.setEnabled(false);
						btnPassendGegeben.setEnabled(false);
						gedruckt = true;
						
						
						String line1 = "B:" + decimalFormat.format(rech.getBrutto()) + " G: " + decimalFormat.format(gegebenGesamt);
						String line2 = "O:" + decimalFormat.format(offenerBetrag) + " Z: " + decimalFormat.format(zurueckBetrag);
						try {
							simpleWrite.send(line1, line2);
						} catch (IOException
								| UnsupportedCommOperationException
								| PortInUseException e1) {
							e1.printStackTrace();
						}
						
						InformixConnector informixConnector = new InformixConnector();
						informixConnector.createConnection();
						DataGenerator dataGenerator = new DataGenerator(informixConnector);						
						dataGenerator.saveValues(werte);
						informixConnector.closeConnection();
						
						btnxfc.setEnabled(true);
						btnxfc.setText("ENDE");
						btnxfc.setBackground(Color.RED);
						
					} else {						
						System.exit(0);						
					}								
				}
			}
		});
		btnxfc.setBackground(Color.RED);
		btnxfc.setIcon(null);
		btnxfc.setFont(new Font("Tahmoma", Font.PLAIN, 30));
		btnxfc.setBounds(635, 378, 132, 303);
		contentPane.add(btnxfc);
		
		JLabel lblFit = new JLabel("fit");
		lblFit.setForeground(new Color(255, 0, 0));
		lblFit.setFont(new Font("Comic Sans MS", Font.PLAIN, 36));
		lblFit.setBounds(10, 11, 60, 50);
		contentPane.add(lblFit);	
		
		JLabel LOffen = new JLabel("Offen");
		LOffen.setFont(new Font("Tahoma", Font.PLAIN, 24));
		LOffen.setBounds(10, 480, 115, 57);
		contentPane.add(LOffen);
		
		JLabel lblZahlart = new JLabel("Zahlart");
		lblZahlart.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblZahlart.setBounds(10, 588, 115, 93);
		contentPane.add(lblZahlart);
		
		String line1 = "Betrag: " + decimalFormat.format(rech.getBrutto());
		try {			
			simpleWrite.send(line1, "");
		} catch (IOException | UnsupportedCommOperationException
				| PortInUseException e1) {
			e1.printStackTrace();
		}
	}

	
	
	private void berechneScheine(final double temp_gegeben) {
		
		if ( gutscheinAktiviert ) {
			gegebenGutscheinScheine = gegebenGutscheinScheine + temp_gegeben;
		} else {
			gegebenScheine = gegebenScheine + temp_gegeben;	
		}
		
	}

	
	
	private void init(final Rech rech, final JTextField Gegeben, final JTextField Zurueck, final JTextField Offen,
			final DecimalFormat decimalFormat, final JButton btnxfc) {		
		gegebenBetragString = "";
		gegebenBetragGutscheinString = "";
		gegebenBetrag = 0;
		gegebenScheine = 0;
		gegebenGutscheinScheine = 0;
		gegebenGutscheinBetrag = 0;
		gegebenEssensgutscheine = 0;
		gegebenGesamt = 0;
		zurueckBetrag = 0;
		offenerBetrag = rech.getBrutto();
		rechBetragEssenGutschein = 0;
		rechBetragEssenGutscheinString = "";
		multiplyBetragEssenGutschein = 0;
		multiBetragEssenGutscheinString = "";
		gutscheinAktiviert = false;
		essenGutscheinAktiviert = false;
		malAktiviert = false;
		
		Gegeben.setText(decimalFormat.format(gegebenGesamt));
		Zurueck.setText(decimalFormat.format(zurueckBetrag));
		Offen.setText(decimalFormat.format(offenerBetrag ));
		
		btnxfc.setBackground(Color.RED);
	}
	
	
	private void numberFieldChange(final String ziffer) {
		
		if ( gutscheinAktiviert ) {
			gegebenBetragGutscheinString = gegebenBetragGutscheinString + ziffer;
			double temp = Double.valueOf(gegebenBetragGutscheinString);
			gegebenGutscheinBetrag = temp / 100;
		} else if ( essenGutscheinAktiviert && !malAktiviert) {
			rechBetragEssenGutscheinString = rechBetragEssenGutscheinString + ziffer;
			double temp = Double.valueOf(rechBetragEssenGutscheinString);
			rechBetragEssenGutschein = temp / 100;
		} else if ( essenGutscheinAktiviert && malAktiviert) {
			multiBetragEssenGutscheinString = multiBetragEssenGutscheinString + ziffer;
			multiplyBetragEssenGutschein = Double.valueOf(multiBetragEssenGutscheinString);
		} else {
			gegebenBetragString = gegebenBetragString + ziffer;
			double temp = Double.valueOf(gegebenBetragString);
			gegebenBetrag = temp / 100;			
		}	
		
	}
	
	
	private void berechne(final DecimalFormat decimalFormat, final Rech rech, final JTextField gegeben,
			final JTextField zurueck, final JTextField offen, final JButton btnxfc) {
		
		gegebenGesamt = gegebenScheine + gegebenBetrag + gegebenGutscheinScheine + gegebenGutscheinBetrag + gegebenEssensgutscheine;
		offenerBetrag = rech.getBrutto() - gegebenGesamt;
		
		if ( rech.getBrutto() - gegebenGesamt >= 0 ) {
			zurueckBetrag = 0;
		} else {
			zurueckBetrag = rech.getBrutto() - gegebenGesamt;
			zurueckBetrag = zurueckBetrag * -1;
			offenerBetrag = 0;
		}
		
		gegeben.setText(decimalFormat.format(gegebenGesamt));
		zurueck.setText(decimalFormat.format(zurueckBetrag));
		offen.setText(decimalFormat.format(offenerBetrag ));
		
		double fehlbetrag = rech.getBrutto() - gegebenGesamt;
		
		if ( fehlbetrag > 0 ) {
			btnxfc.setBackground(Color.RED);	
		} else {
			btnxfc.setBackground(Color.GREEN);
		}
		
	}
	
	
	
	private void schreibeWerteinErgebnisObjekt(final Werte werte, final double gegebenGesamt, final double zurueckBetrag, 
			final double gegebenGutscheinScheine, final double gegebenGutscheinBetrag, final ButtonGroup buttonGroup_1) {
		
		werte.setGegeben(gegebenGesamt);
		werte.setZurueck(zurueckBetrag);
		
		if (getSelectedButtonText(buttonGroup_1).equals("Barzahlung")) {
			werte.setZahlArt(1);
		} else {
			werte.setZahlArt(2);
		}
		werte.setBetragGutschein(gegebenGutscheinScheine + gegebenGutscheinBetrag);		
	}
	
	
	private boolean neuerEssensGutschein(final JButton Gutschein, final JButton Essensgutschein, 
			final Werte werte) {
		
		gutscheinAktiviert = false;
		Gutschein.setBackground(Color.LIGHT_GRAY);
		
		if ( essenGutscheinAktiviert ) {
			essenGutscheinAktiviert = false;
			Essensgutschein.setBackground(Color.LIGHT_GRAY);
			double temp = rechBetragEssenGutschein * multiplyBetragEssenGutschein;
			int anzahl = (int) multiplyBetragEssenGutschein;
			werte.addBetragEssensgutschein(temp, anzahl);
			gegebenEssensgutscheine = gegebenEssensgutscheine + temp;
			return true;
		} else {
			essenGutscheinAktiviert = true;
			Essensgutschein.setBackground(Color.GREEN);
			multiBetragEssenGutscheinString = "";
			multiplyBetragEssenGutschein = 0;
			rechBetragEssenGutscheinString = "";
			rechBetragEssenGutschein = 0;
			malAktiviert = false;
			return false;
		}
		
	}
	
	
	private void callBelder(final Werte werte) {
		
		String rsPath = System.getenv("BWS");
		belderPath = "rswrun.exe " + rsPath + "\\bin\\BELDR.RSR R " + werte.getMdn() + " " + werte.getFil() + " " + werte.getRechNr() + " " + werte.getBenutzer() + " K";
		
		String commands[] = new String[6];
		commands[0] = "R";
		commands[1] = String.valueOf(werte.getMdn());
		commands[2] = String.valueOf(werte.getFil());
		commands[3] = String.valueOf(werte.getRechNr());
		commands[4] = String.valueOf(werte.getBenutzer());
		commands[5] = "K";
		
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Runtime.getRuntime().exec("cmd /c start /MIN " + belderPath);
				} catch (IOException e) {
					e.printStackTrace();
				}				
			}
		});
		thread.start();
	}
	
	
	public String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }
}
