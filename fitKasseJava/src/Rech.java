import java.util.Date;


public class Rech {
	
	
	private int mdn;
	private int fil;
	private int kasse;
	private int rechNr;	
	private String blgTyp;
	private String benutzer;
	private Date rechDate;
	private double brutto;
	
	
	public Rech(final int mdn, final int fil, final int kasse, final int rechNr, 
			final double brutto, final String benutzer) {
		
		this.setMdn(mdn);
		this.setFil(fil);
		this.setKasse(kasse);		
		this.setRechNr(rechNr);
		this.setBlgTyp("K");
		this.setRechDate(new Date());
		this.setBrutto(brutto);
		this.setBenutzer(benutzer);		
	}


	public int getMdn() {
		return mdn;
	}


	public void setMdn(int mdn) {
		this.mdn = mdn;
	}


	public int getKasse() {
		return this.kasse;
	}


	public void setKasse(int kasse) {
		this.kasse = kasse;
	}


	public int getRechNr() {
		return rechNr;
	}


	public void setRechNr(int rechNr) {
		this.rechNr = rechNr;
	}


	public String getBlgTyp() {
		return blgTyp;
	}


	public void setBlgTyp(String blgTyp) {
		this.blgTyp = blgTyp;
	}


	public Date getRechDate() {
		return rechDate;
	}


	public void setRechDate(Date rechDate) {
		this.rechDate = rechDate;
	}


	public double getBrutto() {
		return brutto;
	}


	public void setBrutto(double brutto) {
		this.brutto = brutto;
	}
	

	public int getFil() {
		return fil;
	}
	

	public void setFil(int fil) {
		this.fil = fil;
	}


	public String getBenutzer() {
		return benutzer;
	}


	public void setBenutzer(String benutzer) {
		this.benutzer = benutzer;
	}	

	
}
