#ifndef _SYSTABLES_DEF
#define _SYSTABLES_DEF

struct SYSTABLES {
   char     tabname[MAXNAME];
   long tabid ;	
}
 ;

extern struct SYSTABLES systables;

struct SYSCOLUMNS {
   char     colname[MAXNAME];
   short coltype ;
   long tabid ;	
}
 ;

extern struct SYSCOLUMNS syscolumns;

#endif