// OrderWhere.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "catalog2.h"
#include "OrderWhere.h"


// COrderWhere-Dialogfeld

IMPLEMENT_DYNAMIC(COrderWhere, CPropertyPage)
COrderWhere::COrderWhere()
	: CPropertyPage(COrderWhere::IDD)
	, m_vinptext(_T(""))
{
}



BOOL COrderWhere::OnInitDialog()
{
			SetWindowText(ifeldnam);
			UpdateData(FALSE) ;
	return TRUE ;
}


COrderWhere::~COrderWhere()
{
}

void COrderWhere::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_inptext);
	DDX_Text(pDX, IDC_EDIT1, m_vinptext);
	DDV_MaxChars(pDX, m_vinptext, 2050);
}


BEGIN_MESSAGE_MAP(COrderWhere, CPropertyPage)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
END_MESSAGE_MAP()




void COrderWhere::putcapture(char * efeldnam)
{
	sprintf ( ifeldnam,"%s", efeldnam );
}

static char savekrztxt[2058] ;

char * COrderWhere::rkrztxt()
{

	char *S= m_vinptext.GetBuffer(2058) ; 
	return  S ;
}

void COrderWhere::putkrztxt(char * ekrztxt)
{

	sprintf ( savekrztxt,"%s", ekrztxt );
	 m_vinptext = _T( ekrztxt) ;
}
char * COrderWhere::getkrztxt(char * ekrztxt)
{

	sprintf ( ekrztxt, "%s", rkrztxt() );
	return ekrztxt ;
}


// COrderWhere-Meldungshandler

void COrderWhere::OnBnClickedOk()
{
		CDialog::OnOK();
 
}

void COrderWhere::OnBnClickedCancel()
{
	m_vinptext = _T( savekrztxt );
	CDialog::OnCancel();
}
