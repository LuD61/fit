#pragma once
#include "afxwin.h"


// TabEigenschaften-Dialogfeld

class TabEigenschaften : public CPropertyPage
{
	DECLARE_DYNAMIC(TabEigenschaften)

public:
	TabEigenschaften();
	virtual ~TabEigenschaften();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG4 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	int iput_aktion ;

	DECLARE_MESSAGE_MAP()
public:
	void put_aktion ( int );
	int get_aktion ( void );
	CButton m_outer;
	BOOL v_outer;
	CButton m_dele;
	BOOL v_dele;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
