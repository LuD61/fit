// catalog2View.cpp : implementation of the CCatalog2View class
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.


#include "stdafx.h"
#include "catalog2.h"

#ifdef LL19
#include "cmbtll19.h"		// 011014
#else
#include "cmbtll11.h"		// 071205
#endif


#include "catsets.h"
#include "cat2Doc.h"
#include "cat2View.h"
// GrJ
#include "neuFormat.h"
#include "FeldEigenschaften.h"
#include "ExtFeldEigenschaften.h"
#include "TabEigenschaften.h"
#include "DbClass.h"
#include "nform_tab.h"
#include "nform_feld.h"
#include "systables.h"
#include "mo_db.h"
#include "OrderWhere.h"
#include "form_ow.h"
//#include "cmbtl9.h"
#include "mainfrm.h"
#include "Kommentar.h"
#include "dialvirtuell.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// GrJ

const int LUL_LANGUAGE = CMBTLANG_GERMAN;

void holeexttext(int vorhanden) ;	// 220207

char myform_nr [11] ;

static char klausel[2056] ;	// 021204 : von 1005 auf 2056 erweitert
/////////////////////////////////////////////////////////////////////////////
// CCatalog2View

IMPLEMENT_DYNCREATE(CCatalog2View, CListView)


BEGIN_MESSAGE_MAP(CCatalog2View, CListView)
	//{{AFX_MSG_MAP(CCatalog2View)
	ON_COMMAND(ID_VIEW_FORM_NR, OnViewNonelevel)
	ON_COMMAND(ID_VIEW_TABLES, OnViewTablelevel)
	ON_COMMAND(ID_VIEW_COLUMNINFO, OnViewColumnlevel)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_UPDATE_COMMAND_UI(ID_VIEW_COLUMNINFO, OnUpdateViewColumnlevel)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TABLES, OnUpdateViewTablelevel)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FORM_NR, OnUpdateViewNonelevel)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_NEW133, OnFileNew133)
	ON_COMMAND(ID_LUL_LISTENDESIGN, OnLulListendesign)
	ON_COMMAND(ID_LUL_WHERE, OnLulWhere)
	ON_COMMAND(ID_LUL_ORDER, OnLulOrder)
	ON_COMMAND(ID_LUL_VIRTUELL, OnLulVirtuell)
// Intergation in "normale" where -Klausel  	ON_COMMAND(ID_LUL_ZUKLAUSEL, OnLulZuklausel)
	ON_COMMAND(ID_FILE_DELETE, OnFileDelete)

END_MESSAGE_MAP()



//150509 A

// Nach den dingens ein blank und dann ein "%" , jedoch nur bis zur maximalen Laenge
//  d.h stringlaenge inclusive 0 am Ende 
// dann psst ORACLe hoffentlich 
void likeoracle ( int maxleng , char * dingens)
{

	if ( odbdabatyp != FITORACLE )
		return ;

	int j = -1 ;
	for ( int i = 0 ; i < maxleng ; i++)
	{
		if ( dingens[i] == ' ' ) 
		{
			if ( j == -1 )
				continue ;	// vorblank bleiben stehen
			if ( i < (maxleng - 2) )
			{
				dingens[i+1] = '%' ;
				dingens[i+2] = '\0' ;
				break ;
			}

			break ;	// string ist lang genug
		}
		else
		{
			if ( dingens[i] == '\0' )
			{
				// stringende innerhalb der erlaubten Laenge
				if ( j == -1 )
					break ;	// vorblank bleiben stehen
				if ( i < (maxleng - 2) )
				{
					dingens[i  ] = ' ' ;
					dingens[i+1] = '%' ;
					dingens[i+2] = '\0' ;
					break ;
				}
				break ;	// string ist lang genug
			}
			else
				j = 0 ;	// String erkannt
		}
	}
		

}
// 150509 E
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       clipped
-
-       In              :       string
-
-       Out             :
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Blanks am Ende eines Strings entfernen.
-
--------------------------------------------------------------------------------
**/
#ifdef OLD_DCL
char *clipped (string)
char *string;
#else
char *clipped (char *string)
#endif
{
 char *clstring;
 short i,len;

 len = (short) strlen (string);

 if (len == 0) return (string);
 len --;
 clstring = string;
 for (i = len; i >= 0; i --)	// 071110 : nachgebaut : Null-Laenge
 {
  if ((unsigned char) clstring[i] > 0x20)
  {
   break;
  }
 }
 clstring [i + 1] = 0;

 clstring = string;
/* anfangsclipped muss fuer uns weg ....
 len = (short) strlen (clstring);
 for (i = 0; i < len; i ++, clstring +=1)
 {
  if ((unsigned char) *clstring > (unsigned char) 0X20)
  {
   break;
  }
 }
 < ----- */
 return (clstring);
}

#ifdef OLD_DCL
char *uPPER (string)
char *string;
#else
char *uPPER (char *string)
#endif
{

   for( char *p = string; p < string + strlen( string ); p++ )
   {
      if( islower( *p ) )
			( p[0] = _toupper( *p ) );
     }
   return string ;
}



#ifdef OLABEL
static int olabelfcount ;
static char olafeldname[OLASTU][51] ;
// static char olafeldwert[OLASTU][251] ;
static int  olafeldtyp[OLASTU] ;


// Modus == 0 -> nur struktur lesen 
// Modus > 0 -> auch noch Daten lesen
// Returncode : Anzahl gelesener Saetze

int olaopendaten ( int imodus )
{
 int incount, i, o ;
 int gescount ;
 int ianz ;
 char ibuf [120] ;
// gibbet kaum char suchwert [110] ;
// gibbet kaum char suchname[31] ;
 char dateiname[256] ;
 char * etc ;
 FILE * fp_struct ;
 
	sprintf (dateiname, "%s\\format\\LuLabel\\%s.stu", odbbasisverz , myform_nr);

 fp_struct = fopen ( dateiname, "r" ) ;
	 if ( fp_struct == NULL )	return 0 ;
 incount = 0 ;
 while ( ( ( incount + 1 ) < OLASTU )&&  ( fgets( ibuf, 100, fp_struct)!= NULL ) )
 {
	 ianz = (int)strlen ( ibuf ) ;
	 if ( ianz < 3 ) continue ;
	 if (ibuf[0] == '$' && ibuf[1] == '$' )
	 {
		i = 2 ;
		while ( (i < ianz) && (ibuf[i] != ';') )
		{
			olafeldname[incount][i - 2] = ibuf[i] ;
			i ++ ;
		}
		olafeldname[incount][i - 2] = '\0' ;	
// gibbet nicht ...		olafeldwert[incount][0] = '\0' ;	// Wert initen 
		i ++ ;
		olafeldtyp[incount] = 0 ;
		if ( i < ianz )
		{
			if ( ibuf[i] == 'N' || ibuf[i] == 'n' )
				olafeldtyp[incount] = 1 ;
		}

		 incount ++ ;
	}
}
gescount = incount ;
fclose ( fp_struct ) ;
if ( ! imodus ) return gescount ;
/* -> gibbet hier ja niemals ......
if ( ! datenausdatei ) return 0 ;	// Keine Daten da
if ( imodus )
{
	sprintf (dateiname, "%s", datenausdateiname);
	fp_struct = fopen ( dateiname, "r" ) ;
	if ( fp_struct == NULL )	return 0 ;
	while ( ( fgets( ibuf, 110, fp_struct)) != NULL )
	{

		ianz = strlen ( ibuf );
		if ( ianz < 3 ) continue ;
		if (ibuf[0] == '$' && ibuf[1] == '$' )
		{
			i = 0 ;
			while ( (i < ianz) && (ibuf[i] != ';') )
			{
				suchname[i] = ibuf[i] ;
				i ++ ;
			}
			suchname[i] = '\0' ;
			i ++ ;
			o = 0 ;
			suchwert[o] = '\0' ;
			while ( i < ianz && ibuf[i] != 0x0a && ibuf[i] != 0x0d )
			{
				suchwert[o++] = ibuf[i++] ;
			}
			suchwert[o] = '\0' ;
		}

		for ( incount = 0 ; incount < gescount ; incount ++ )
		{
			if ( ! strncmp ( suchname, olafeldname[incount] , strlen ( suchname)) )
			{
				sprintf ( olafeldwert[incount],"%s", suchwert ) ;
				break ;
			}
		}
	}
};
< ---- */

return gescount ;

}

#endif	OLABEL





/////////////////////////////////////////////////////////////////////////////
// CCatalog2View construction/destruction

CCatalog2View::CCatalog2View()
{
	m_strTableName = _T("");
}

CCatalog2View::~CCatalog2View()
{
}

BOOL CCatalog2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// set list view control to report, single selection
	cs.style &= ~(LVS_LIST | LVS_ICON | LVS_SMALLICON);
	cs.style |= LVS_REPORT;
	cs.style |= LVS_SINGLESEL;

	return CListView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCatalog2View drawing

void CCatalog2View::OnDraw(CDC* /*pDC*/)
{
	CCatalog2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CCatalog2View diagnostics

#ifdef _DEBUG
void CCatalog2View::AssertValid() const
{
	CListView::AssertValid();
}

void CCatalog2View::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}

CCatalog2Doc* CCatalog2View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCatalog2Doc)));
	return (CCatalog2Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCatalog2View message handlers

void CCatalog2View::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	CCatalog2Doc*   pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// delete all items and columns
	CListCtrl& control = GetListCtrl();
	control.DeleteAllItems();


	pDoc->OnOpenBWS();

	while (control.DeleteColumn(0));

	// set up view based on the document's level
	switch (pDoc->m_nLevel)
	{
		case CCatalog2Doc::levelNone:
		{
/* ---->
			// set the document title
			pDoc->SetTitle(pDoc->GetDSN());
			break;
			
< ----- */
// GrJ
			// add columns to display
			control.InsertColumn(0,_T("Name"),LVCFMT_LEFT,100,-1);
			control.InsertColumn(1,_T("Beschreibung"),LVCFMT_LEFT,400,4); // 301104
			control.InsertColumn(2,_T("distinct"),LVCFMT_LEFT,50,3);	// 030211
			
			int item = 0;

			sprintf ( nform_tab.form_nr, "%%" );	// 061205
			nform_tab.lila = GlobalLila ;
			DbClass.sqlopen (form_nr_curs);
			while (!DbClass.sqlfetch(form_nr_curs))	// gefunden
			{

				control.InsertItem(item, nform_tab.form_nr);

// 301104 : Zusaetzlich Beschreibung ausgeben Anfang
				sprintf ( nform_hea.form_nr, "%s", clipped ( nform_tab.form_nr) ); // 150409 :clipped dazu
				likeoracle( NR_NAME ,nform_hea.form_nr ) ;
				nform_hea.lila = GlobalLila ;
				DbClass.sqlopen (form_h_curs);	
				if (!DbClass.sqlfetch(form_h_curs))	// gefunden
				{
					control.SetItem(item,1,LVIF_TEXT,_T(nform_hea.form_ube),0,0,0,0);
					if ( nform_hea.delstatus == 1 )	// 030211
						control.SetItem(item,2,LVIF_TEXT,_T("*"),0,0,0,0);
					else
						control.SetItem(item,2,LVIF_TEXT,_T(""),0,0,0,0);
				}
				else
				{
					control.SetItem(item,1,LVIF_TEXT,_T(" "),0,0,0,0);
					control.SetItem(item,2,LVIF_TEXT,_T(""),0,0,0,0);	// 030211
				}
				
				
// 301104 : Zusaetzlich Beschreibung ausgeben Ende				

				item++ ;
			}

			char titelstring[20];
			if ( !GlobalLila )
				sprintf ( titelstring, "Listen" );
			else
				sprintf ( titelstring, "Labels" );

#ifdef OLABEL
				sprintf ( titelstring, "%s", myform_nr );
#endif

			pDoc->SetTitle(titelstring);


			break ;
		}
		case CCatalog2Doc::levelTable:
		{

			// set the document title
			CString strDataSource = pDoc->GetDSN();
			strDataSource += _T(" [Tables]");
//			pDoc->SetTitle(strDataSource);
			
			// GrJ
// 301104 : Beschreibungskram dazu ...
//	char titelstring[50] ;

			char titelstring[150] ;

			if ( strlen ( clipped ( nform_hea.form_ube )) > 0 )
			{
				if ( !GlobalLila )
					sprintf ( titelstring, "Listen - Format : %s Tabellen %s", myform_nr,
						clipped ( nform_hea.form_ube));
				else
					sprintf ( titelstring, "Labels - Format : %s Tabellen %s", myform_nr,
						clipped(nform_hea.form_ube ));
			}
			else
			{
				if ( !GlobalLila )
					sprintf ( titelstring, "Listen - Format : %s Tabellen", myform_nr );
				else
					sprintf ( titelstring, "Labels - Format : %s Tabellen", myform_nr );

			};

			pDoc->SetTitle(titelstring);


		// add columns to display
			control.InsertColumn(0,_T("Name"),LVCFMT_LEFT,100,-1);
/* --->
			control.InsertColumn(1,_T("Type"),LVCFMT_LEFT,100,1);
			control.InsertColumn(2,_T("Qualifier"),LVCFMT_LEFT,100,2);
			control.InsertColumn(3,_T("Owner"),LVCFMT_LEFT,100,3);
			control.InsertColumn(4,_T("Select"),LVCFMT_LEFT,100,4);// GrJ "Remarks"
< ----- */
			control.InsertColumn(1,_T("Select"),LVCFMT_LEFT,100,1);
		// traverse the table recordset
		// displaying the table information
			int item = 0;
			
#ifdef FITODBC
			pDoc->m_pTableset->MoveFirst();
			while (!pDoc->m_pTableset->IsEOF())
#endif
#ifndef FITODBC
			DbClass.sqlopen (tabellen_curs);
			while (!DbClass.sqlfetch(tabellen_curs))	// gefunden
#endif
			{

#ifdef FITODBC
			/* ---> 230610
			if ( odbdabatyp == FITORACLE )
			{
				if ( strncmp ( "BWSO" , pDoc->m_pTableset->m_strTableOwner,4 ))
				{	// Nur bws-Tabellen anzeigen 
					pDoc->m_pTableset->MoveNext();
					continue ;
				}
			}
			< ----- */
			
#endif



#ifndef FITODBC
				control.InsertItem(item,systables.tabname);
#endif
#ifdef FITODBC
				control.InsertItem(item,pDoc->m_pTableset->m_strTableName);
#endif
/* ----> wollen wir alles nicht 
				control.SetItem(item,1,LVIF_TEXT,
					pDoc->m_pTableset->m_strTableType,0,0,0,0);
				control.SetItem(item,2,LVIF_TEXT,
					pDoc->m_pTableset->m_strTableQualifier,0,0,0,0);
				control.SetItem(item,3,LVIF_TEXT,
					pDoc->m_pTableset->m_strTableOwner,0,0,0,0);
< ---- */


/* ----->
				control.SetItem(item,4,LVIF_TEXT,
					pDoc->m_pTableset->m_strRemarks,0,0,0,0);
< ---- */
// GrJ

				nform_tab.lila = GlobalLila ;

				sprintf ( nform_tab.form_nr,  "%s" , myform_nr );
#ifndef FITODBC
				sprintf ( nform_tab.tab_nam, "%s" ,systables.tabname);
#endif
#ifdef FITODBC
				sprintf ( nform_tab.tab_nam,pDoc->m_pTableset->m_strTableName );
				likeoracle(  TAFELNAME ,nform_tab.tab_nam ) ;

#endif
				DbClass.sqlopen (formtab_curs);	// destroyed den Cursor, sqlopen refreshed
				if (!DbClass.sqlfetch(formtab_curs))	// gefunden
				{
//					control.SetItem(item,4,LVIF_TEXT,_T("*"),0,0,0,0);
// 121104			control.SetItem(item,1,LVIF_TEXT,_T("*"),0,0,0,0);
					if ( nform_tab.delstatus == 1 )
					{
						control.SetItem(item,1,LVIF_TEXT,_T("O"),0,0,0,0);	// 121104
					}
					else
					{
						control.SetItem(item,1,LVIF_TEXT,_T("*"),0,0,0,0);
					}
				}
				else
				{
//					control.SetItem(item,4,LVIF_TEXT,_T(" "),0,0,0,0);
					control.SetItem(item,1,LVIF_TEXT,_T(" "),0,0,0,0);
				}

				item++;

#ifdef FITODBC
				pDoc->m_pTableset->MoveNext();
#endif
			}

#ifdef FITODBC
// Alles sortieren 
//			int gescount = 0 ;	// testcounter
//			int tauschcount = 0 ;

// hiermal bissel optimized mit Sprungweiten :
// bei einer-Schritten zuerst ca 83000 Runden mit ca 40400 Tauschoperationen
// jetzt                 noch ca 50500 Runden mit ca  9600 Tauschoperationen
// in meiner DABA mit ca 770 Tabellen 





			int itemmax = item - 5 ;
			char u1[36] ; char o1 [36] ; 
			char u2[4]  ; char o2 [4]  ; 


			for ( item = 0 ; item < itemmax ; item ++ )
			{
//				gescount ++ ;
				sprintf ( u1, control.GetItemText(item,0));
				sprintf ( o1, control.GetItemText(item + 5 ,0));
				if ( strcmp ( u1, o1 ) > 0 ) // Bitte tauschen
				{
//					tauschcount ++ ;
					sprintf ( u2, control.GetItemText(item,1));
					sprintf ( o2, control.GetItemText(item  + 5,1));

					control.SetItem(item     ,0,LVIF_TEXT,_T(o1),0,0,0,0);
					control.SetItem(item + 5 ,0,LVIF_TEXT,_T(u1),0,0,0,0);

					control.SetItem(item     ,1,LVIF_TEXT,_T(o2),0,0,0,0);
					control.SetItem(item + 5 ,1,LVIF_TEXT,_T(u2),0,0,0,0);
					item -= 5 ;  if ( item > -1 ) item -- ; else item = -1 ;
				}
			}


			itemmax += 4 ;
			for ( item = 0 ; item < itemmax ; item ++ )
			{
//				gescount ++ ;
				sprintf ( u1, control.GetItemText(item,0));
				sprintf ( o1, control.GetItemText(item + 1 ,0));
				if ( strcmp ( u1, o1 ) > 0 ) // Bitte tauschen
				{
//					tauschcount ++ ;
					sprintf ( u2, control.GetItemText(item,1));
					sprintf ( o2, control.GetItemText(item  + 1,1));

					control.SetItem(item     ,0,LVIF_TEXT,_T(o1),0,0,0,0);
					control.SetItem(item + 1 ,0,LVIF_TEXT,_T(u1),0,0,0,0);

					control.SetItem(item     ,1,LVIF_TEXT,_T(o2),0,0,0,0);
					control.SetItem(item + 1 ,1,LVIF_TEXT,_T(u2),0,0,0,0);
					item -- ; if ( item > -1 ) item -- ;
				}

			}
//			gescount ++ ;
//			tauschcount ++ ;
#endif

			break;
		}

		case CCatalog2Doc::levelColumn:
		{
			int column;
			
		// set the document title
/* so sah das frueher aus ...
			CString strDataSource = pDoc->GetDSN();
			strDataSource += _T(" - ");
			strDataSource += m_strTableName;
			strDataSource += _T(" [Column Info]");
			pDoc->SetTitle(strDataSource);
< ---- */
		
			CString strDataSource;
			if ( !GlobalLila )
				strDataSource = "Listen - ";
			else		
				strDataSource = "Labels - ";

			strDataSource += _T("Format : ");
			strDataSource += _T(myform_nr);
			strDataSource += _T(" Tabelle : ");
			strDataSource += m_strTableName;
			pDoc->SetTitle(strDataSource);


			// add columns to display
			// respect the column info settings values
			column = 0;
			control.InsertColumn(column++,_T("Name"),LVCFMT_LEFT,100,-1);
			control.InsertColumn(column,_T("Type"),LVCFMT_LEFT,100,column++);
			if (pDoc->m_bLength)
				control.InsertColumn(column,_T("Length"),LVCFMT_LEFT,80,column++);
			if (pDoc->m_bPrecision)
			{
				control.InsertColumn(column,_T("Precision"),LVCFMT_LEFT,80,column++);
				control.InsertColumn(column,_T("Scale"),LVCFMT_LEFT,50,column++);
				control.InsertColumn(column,_T("Radix"),LVCFMT_LEFT,50,column++);
			}
// GrJ			if (pDoc->m_bNullability)
				control.InsertColumn(column,_T("Select"),LVCFMT_LEFT,50,column++);
				control.InsertColumn(column,_T("Range"),LVCFMT_LEFT,50,column++);
				control.InsertColumn(column,_T("Referenz"),LVCFMT_LEFT,100,column++);
// 010204 : Referenz dazu


			// traverse the column info recordset
			// respect the column info settings values
			int item = 0;
			pDoc->m_pColumnset->MoveFirst();
			while (!pDoc->m_pColumnset->IsEOF())
			{
				CString strValue;

				// always insert the column name
				control.InsertItem(item,
					pDoc->m_pColumnset->m_strColumnName);

				// always insert the column type
				column = 1;
				control.SetItem(item,column++,LVIF_TEXT,
					pDoc->m_pColumnset->m_strTypeName,0,0,0,0);

				// only show type if requested
				if (pDoc->m_bLength)
				{
					strValue.Format(_T("%d"),pDoc->m_pColumnset->m_nLength);
					control.SetItem(item,column++,LVIF_TEXT,strValue,0,0,0,0);
				}

				// only show precision,scale,radix if requested
				if (pDoc->m_bPrecision)
				{
					// precision
					strValue.Format(_T("%d"),pDoc->m_pColumnset->m_nPrecision);
					control.SetItem(item,column++,LVIF_TEXT,strValue,0,0,0,0);

					// scale
					if (!pDoc->m_pColumnset->IsFieldNull(
						&(pDoc->m_pColumnset->m_nScale)))
					{
						strValue.Format(_T("%d"),pDoc->m_pColumnset->m_nScale);
						control.SetItem(item,column++,LVIF_TEXT,strValue,0,0,0,0);
					}
					else
						control.SetItem(item,column++,LVIF_TEXT,_T("<na>"),0,0,0,0);

					// radix
					if (!pDoc->m_pColumnset->IsFieldNull(
						&(pDoc->m_pColumnset->m_nRadix)))
					{
						strValue.Format(_T("%d"),pDoc->m_pColumnset->m_nRadix);
						control.SetItem(item,column++,LVIF_TEXT,strValue,0,0,0,0);
					}
					else
						control.SetItem(item,column++,LVIF_TEXT,_T("<na>"),0,0,0,0);
				}

				// only show nullability if requested
/* ->>>
				if (pDoc->m_bNullability)
				{
					if (pDoc->m_pColumnset->m_fNullable == SQL_NO_NULLS)
						control.SetItem(item,column++,LVIF_TEXT,_T("No"),0,0,0,0);
					else if (pDoc->m_pColumnset->m_fNullable == SQL_NULLABLE)
						control.SetItem(item,column++,LVIF_TEXT,_T("Yes"),0,0,0,0);
					else
						control.SetItem(item,column++,LVIF_TEXT,_T("Unknown"),0,0,0,0);
				}

<<<< - */

// GrJ : selekiert  oder nicht

				nform_feld.lila = GlobalLila ;
				sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
				sprintf ( nform_feld.tab_nam, "%s" , m_strTableName );
				char *p = clipped ( nform_feld.tab_nam ) ;
				sprintf ( nform_feld.feld_nam, "%s" , pDoc->m_pColumnset->m_strColumnName );
			    likeoracle( TAFELNAME ,nform_feld.feld_nam ) ;

				DbClass.sqlopen (formfeld_curs);
				if (!DbClass.sqlfetch(formfeld_curs))	// gefunden
				{
						control.SetItem(item,column++,LVIF_TEXT,_T("*"),0,0,0,0);	// Selected
						if ( nform_feld.range )
						{
//							control.SetItem(item,column++,LVIF_TEXT,_T("*"),0,0,0,0);
							char test[2] ;
							if ((nform_feld.range < 0) ||( nform_feld.range > 9 ))
								nform_feld.range = 9 ;
							sprintf ( test, "%d", nform_feld.range ) ;
							control.SetItem(item,column++,LVIF_TEXT,_T(test),0,0,0,0);
						}
						else
							control.SetItem(item,column++,LVIF_TEXT,_T(" "),0,0,0,0);

						switch(nform_feld.feld_typ)
						{

						case FT_PTABN : control.SetItem(item,1,LVIF_TEXT,_T("*Prueftab"),0,0,0,0);
//								control.SetItem(item,column++,LVIF_TEXT,_T(" "),0,0,0,0);
								control.SetItem(item,column++,LVIF_TEXT,_T(nform_feld.krz_txt),0,0,0,0);	// 091209
							break; 
						case FT_TEXTBAUST : control.SetItem(item,1,LVIF_TEXT,_T("*Text"),0,0,0,0);
								control.SetItem(item,column++,LVIF_TEXT,_T(nform_feld.krz_txt),0,0,0,0);

							break;

						case FT_ADRESSE : control.SetItem(item,1,LVIF_TEXT,_T("*Adresse"),0,0,0,0);
								 control.SetItem(item,column++,LVIF_TEXT,_T(" "),0,0,0,0);
							break;
						case FT_TEXTEXT : control.SetItem(item,1,LVIF_TEXT,_T("*TextExt"),0,0,0,0);	// 220207
								control.SetItem(item,column++,LVIF_TEXT,_T(nform_feld.krz_txt),0,0,0,0);
							break ;
						case FT_FELDEXT : control.SetItem(item,1,LVIF_TEXT,_T("*FeldExt"),0,0,0,0);	// 071110
								control.SetItem(item,column++,LVIF_TEXT,_T(nform_feld.krz_txt),0,0,0,0);

							break;

						}						

				}
				else
				{
						control.SetItem(item,column++,LVIF_TEXT,_T(" "),0,0,0,0);
						control.SetItem(item,column++,LVIF_TEXT,_T(" "),0,0,0,0);
						control.SetItem(item,column++,LVIF_TEXT,_T(" "),0,0,0,0);
				}

				item++;
				pDoc->m_pColumnset->MoveNext();
			}
			break;
		}
	}
}

void CCatalog2View::OnViewTablelevel()
{
	CCatalog2Doc*   pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	pDoc->SetLevel(CCatalog2Doc::levelTable);
}


// GrJ
void CCatalog2View::OnViewNonelevel()
{
	CCatalog2Doc*   pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	pDoc->SetLevel(CCatalog2Doc::levelTable);
}


void CCatalog2View::OnViewColumnlevel()
{
	CCatalog2Doc*   pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// determine list control selection
	CListCtrl&  control = GetListCtrl();
	int nCount = control.GetItemCount();
	int i ;	// 010409
	for ( i = 0; i < nCount; i++)
	{
		if (control.GetItemState(i,LVIS_SELECTED))
			break;
	}
	if (i < nCount)
	{
		// pull table name to send to document
		m_strTableName = control.GetItemText(i,0);

#ifndef _UNICODE
		LPCSTR lpszName;
		lpszName = m_strTableName;
#else
		LPSTR lpszName;
		char rgchTableName[257];
		lpszName = rgchTableName;
		int nSize;
		nSize = ::WideCharToMultiByte(CP_ACP,0,m_strTableName,
			-1, lpszName, 257, NULL, NULL);
		// Notify on failure
		ASSERT(nSize);
#endif  // _UNICODE

		pDoc->FetchColumnInfo(lpszName,NULL);	// 071205 : NULL dazu 
		pDoc->SetLevel(CCatalog2Doc::levelColumn);
	}
}

void CCatalog2View::OnFileNew()
{
// GrJ
	CCatalog2Doc*   pDoc = GetDocument( );
	pDoc->OnOpenBWS() ;

	if ((strlen (myform_nr)) == 0 )
	{
		pDoc->SetLevel(CCatalog2Doc::levelNone);
		ASSERT_VALID(pDoc);

		return ;
	}

	ASSERT_VALID(pDoc);
	if (pDoc->OnOpenDocument())
		pDoc->SetLevel(CCatalog2Doc::levelTable);
	else
		pDoc->SetLevel(CCatalog2Doc::levelNone);
}

// 301104 A
void CCatalog2View::OnKommentari()
{
	CKommentar komment;

	char kommsave[257];
	char kommneu[257];
	short distinctsave ;	// 030211
	short distinctneu ;		// 030211


	sprintf ( nform_hea.form_nr, "%s", myform_nr );
	likeoracle(NR_NAME, nform_hea.form_nr ) ;
	nform_hea.lila = GlobalLila ;
	DbClass.sqlopen (form_h_curs);	
	int di = DbClass.sqlfetch(form_h_curs);
		if ( di )								// nix gefunden
		{
			sprintf ( nform_hea.form_ube, " " );
			nform_hea.delstatus = 0 ;	// 050314 - mea culpa
		}

	sprintf ( kommsave, "%s", nform_hea.form_ube);
	komment.putkommentar ( nform_hea.form_ube ) ;

	distinctsave = nform_hea.delstatus ;	// 030211
	komment.putdistinct ( nform_hea.delstatus ) ;	// 030211

	komment.puttitel( nform_hea.form_nr );
	if ( komment.DoModal() == IDOK )
	{	// gegbnenfalls updaten oder inserten
		sprintf ( kommneu, komment.getkommentar()) ;
		distinctneu = komment.getdistinct() ;	// 030211

		if ( (strlen ( clipped(kommsave))) != (strlen(clipped(kommneu)))
			|| (strcmp ( kommsave,kommneu )) 
			|| (distinctneu != distinctsave) )
			// 170706 : Hier wurde bisher auf !strcmp getestet, was zur Folge hatte,
			// das bei gleichlangem neuen oder altem Kommentar der neue NICHT
			// abgespeichert wurde
			// 030211 : distinct mit auswerten  ....
		{
//			sprintf ( nform_hea.form_nr, "%s", myform_nr );
//			nform_hea.lila = GlobalLila ;
//			DbClass.sqlopen (form_h_curs);

			sprintf ( nform_hea.form_ube , "%s", kommneu) ;
			nform_hea.delstatus = distinctneu ;	// 030211

//			if (DbClass.sqlfetch(form_h_curs))	// nix gefunden
			if ( di )
			{
				sprintf ( nform_hea.form_nr, "%s", myform_nr );
				nform_hea.lila = GlobalLila ;
// 030211 : variabel				nform_hea.delstatus = 0 ;
				DbClass.sqlexecute ( form_h_i_curs );
			}
			else
			{
				sprintf ( nform_hea.form_nr, "%s", myform_nr );
				nform_hea.lila = GlobalLila ;
				nform_hea.delstatus = distinctneu ;	// 030211
				DbClass.sqlexecute(form_h_u_curs);	// Satz updaten
			}

			CCatalog2Doc*   pDoc = GetDocument( );
			pDoc->OnOpenBWS() ;
			pDoc->SetLevel(CCatalog2Doc::levelNone); // Kommentar refreshen
		}
	}
}

// 301104 E

void CCatalog2View::OnFileOpeni()
{
// GrJ
	CneuFormat newdial;
#ifndef OLABEL
	if ( strlen (myform_nr ) == 0 )
	{
		newdial.DoModal();
		nform_tab.lila = GlobalLila ;

		sprintf ( myform_nr ,"%s" ,clipped (newdial.neuformat())) ;
	}
	CCatalog2Doc*   pDoc = GetDocument( );
	pDoc->OnOpenBWS() ;
	if ((strlen (myform_nr))  < 5 )
	{
		sprintf ( myform_nr ,"" );
		pDoc->SetLevel(CCatalog2Doc::levelNone);
		ASSERT_VALID(pDoc);

		return ;
	}

	nform_tab.lila = GlobalLila ;
	sprintf ( nform_tab.form_nr,  "%s" , myform_nr );
	likeoracle( NR_NAME ,nform_tab.form_nr ) ;

	DbClass.sqlopen (form_nr_curs);
	sprintf ( nform_hea.form_ube , " ") ;	// 301104
	
	if (!DbClass.sqlfetch(form_nr_curs))

	{

		ASSERT_VALID(pDoc);
		if (pDoc->OnOpenDocument())
		{
			
	
		// 301104 : Kommentare lesen A
			sprintf ( nform_hea.form_nr, "%s", nform_tab.form_nr );
			nform_hea.lila = GlobalLila ;
			likeoracle( NR_NAME ,nform_hea.form_nr );

			DbClass.sqlopen (form_h_curs);	
			if (DbClass.sqlfetch(form_h_curs))	// nix gefunden
			{
				sprintf ( nform_hea.form_ube , " ") ;
				nform_hea.delstatus = 0 ;	// 030211
			}
		// 301104 : Kommentare lesen E
			pDoc->SetLevel(CCatalog2Doc::levelTable);
		}
		else
			pDoc->SetLevel(CCatalog2Doc::levelNone);
	}
	else
		pDoc->SetLevel(CCatalog2Doc::levelNone);
#endif		//OLABEL
}

void CCatalog2View::OnFileOpen()
{
#ifndef OLABEL
	sprintf ( myform_nr , "" );
	OnFileOpeni();
#endif
}


// GrJ
void CCatalog2View::OnUpdateViewNonelevel(CCmdUI* pCmdUI)
{
	CCatalog2Doc*   pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	if (pDoc->m_nLevel == CCatalog2Doc::levelTable &&
		GetListCtrl().GetSelectedCount())
	{
		pCmdUI->Enable();
	}
	else
		pCmdUI->Enable(FALSE);
}

void CCatalog2View::OnUpdateViewColumnlevel(CCmdUI* pCmdUI)
{
	CCatalog2Doc*   pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	if (pDoc->m_nLevel == CCatalog2Doc::levelTable &&
		GetListCtrl().GetSelectedCount())
	{
		pCmdUI->Enable();
	}
	else
		pCmdUI->Enable(FALSE);
}

void CCatalog2View::OnUpdateViewTablelevel(CCmdUI* pCmdUI)
{
	CCatalog2Doc*   pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	if (pDoc->m_nLevel == CCatalog2Doc::levelColumn)
		pCmdUI->Enable();
	else
		pCmdUI->Enable(FALSE);
}



BOOL CCatalog2View::OnChildNotify(UINT message, WPARAM wParam,
 LPARAM lParam, LRESULT* pLResult)
{
	CCatalog2Doc*   pDoc = GetDocument();
	ASSERT_VALID(pDoc);

// #### DOUBLE CLICK AT TABLE LEVEL

	if (pDoc->m_nLevel == CCatalog2Doc::levelTable)
	{
		if (message == WM_NOTIFY &&
			((NMHDR*)lParam)->code == NM_DBLCLK)
		{

// Gegebnenfalls inserten A .....
			CListCtrl&  control = GetListCtrl();
			int nCount = control.GetItemCount();
			int i ;
			for ( i = 0; i < nCount; i++)
			{
				if (control.GetItemState(i,LVIS_SELECTED))
				break;
			}
			if (i < nCount)
			{
				nform_tab.lila = GlobalLila ;
				sprintf ( nform_tab.form_nr,  "%s" , myform_nr );
				sprintf (nform_tab.tab_nam, control.GetItemText(i,0));
				likeoracle( TAFELNAME ,nform_tab.tab_nam ) ;

				DbClass.sqlopen (formtab_curs);
				if (!DbClass.sqlfetch(formtab_curs))
					
				{
					// gefunden
				}
				else
				{
// loesche "leeren" Satz
					nform_tab.lila = GlobalLila ;
					sprintf ( nform_tab.form_nr,  "%s" , myform_nr );
					sprintf (nform_tab.tab_nam, "");
					DbClass.sqlexecute ( deltab_curs );
					sprintf (nform_tab.tab_nam, control.GetItemText(i,0));
					nform_tab.delstatus = 0 ;
					DbClass.sqlexecute ( instab_curs );
				}

// Gegebnenfalls inserten E .....
			}
			OnViewColumnlevel();
			return 0;
		}	// doppelclick auf table-level 


// #### DOUBLE RIGHT-CLICK AT TABLE LEVEL

		if (message == WM_NOTIFY &&
			((NMHDR*)lParam)->code == NM_RDBLCLK)
		{

		//	xxxxxxxxxxxxxxxxxxxxxxxxxxxx
		//	xxxxxxxxxxxxxxxxxxxxxxxxxxx
		//	xxxxxxxxxxxxxxxxxxxxxxxxxxxx
		//	xxxxxxxxxxxxxxxxxxxxxxxxxxx

			CListCtrl&  control = GetListCtrl();
			int nCount = control.GetItemCount();
			int i ;
			for ( i = 0; i < nCount; i++)
			{
				if (control.GetItemState(i,LVIS_SELECTED))
				break;
			}

			if (i < nCount)
			{

				nform_tab.lila = GlobalLila ;
				sprintf ( nform_tab.form_nr,  "%s" , myform_nr );
				sprintf (nform_tab.tab_nam, control.GetItemText(i,0));
				likeoracle( TAFELNAME ,nform_tab.tab_nam ) ;

				DbClass.sqlopen (formtab_curs);
				if (!DbClass.sqlfetch(formtab_curs))
				{	// nur was machen, wenn bereits vorhanden 

					TabEigenschaften tabdial;
					tabdial.put_aktion( (int) nform_tab.delstatus) ;
					tabdial.DoModal();
					int xmodus = tabdial.get_aktion() ;
					if ( xmodus == 1 )	// loeschen
					{
// nur, falls gefunden-> alles loeschen
						DbClass.sqlexecute ( deltab_curs );
						nform_feld.lila = GlobalLila ;
						sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
						sprintf (nform_feld.tab_nam, control.GetItemText(i,0));

						sprintf ( nform_feld.feld_nam,  "%%" );	// 061205
						DbClass.sqlexecute ( delfeld_curs );

						// 220207 
						form_t.lila = GlobalLila ;
						sprintf ( form_t.form_nr,  "%s" , myform_nr );
						sprintf ( form_t.tab_nam, control.GetItemText(i,0));
						
						sprintf ( form_t.feld_nam,  "%%" );
						DbClass.sqlexecute ( delform_t_curs );

						form_te.lila = GlobalLila ;
						sprintf ( form_te.form_nr,  "%s" , myform_nr );
						sprintf ( form_te.tab_nam, control.GetItemText(i,0));
						sprintf ( form_te.feld_nam,  "%%" );
						DbClass.sqlexecute ( delform_te_curs );


						pDoc->SetLevel(CCatalog2Doc::levelTable);	// 130104
						return 0 ;
					}
					if ( xmodus == 2 ||xmodus == 3 )	// outer-status setzen 
					{
						if ( xmodus == 2 ) nform_tab.delstatus = 1 ;
						if ( xmodus == 3 ) nform_tab.delstatus = 0 ;
						DbClass.sqlexecute ( updtab_curs );
						pDoc->SetLevel(CCatalog2Doc::levelTable);	// 130104

					}

					return 0 ;
				}
			}
		}
		return 0 ;
	}	// levelTable zu ende


// #### DOUBLE CLICK AT NONE LEVEL( Format selektieren )

	if (pDoc->m_nLevel == CCatalog2Doc::levelNone)
	{
		if (message == WM_NOTIFY &&
			((NMHDR*)lParam)->code == NM_DBLCLK)
		{

// determine list control selection
			CListCtrl&  control = GetListCtrl();
			int nCount = control.GetItemCount();
			int i ;
			for ( i = 0; i < nCount; i++)
			{
				if (control.GetItemState(i,LVIS_SELECTED))
				break;
			}
			if (i < nCount)
			{
			sprintf ( myform_nr, control.GetItemText(i,0));
			char *p = clipped(myform_nr);	// 140509

/* --->
#ifndef _UNICODE
		LPCSTR lpszName;
		lpszName = m_strTableName;
#else
		LPSTR lpszName;
		char rgchTableName[257];
		lpszName = rgchTableName;
		int nSize;
		nSize = ::WideCharToMultiByte(CP_ACP,0,m_strTableName,
			-1, lpszName, 257, NULL, NULL);
		// Notify on failure
		ASSERT(nSize);
#endif  // _UNICODE
< ---- */
				if ((strlen ( myform_nr )) > 0 )
				{
// 					pDoc->SetLevel(CCatalog2Doc::levelTable);
					OnFileOpeni();
				}
			}
			return 0;
		}

		// 301104 A : Kommentar schreiben
		// DOUBLE - RIGHT-CLICK at none-Level : Kommentar schreiben
		if (message == WM_NOTIFY &&
			((NMHDR*)lParam)->code == NM_RDBLCLK)
		
		{
			CListCtrl&  control = GetListCtrl();
			int nCount = control.GetItemCount();
			int i ;
			for ( i = 0; i < nCount; i++)
			{
				if (control.GetItemState(i,LVIS_SELECTED))
				break;
			}
			if (i < nCount)
			{
				sprintf ( myform_nr, control.GetItemText(i,0));
				char *p = clipped(myform_nr);	// 140509


				if ((strlen ( myform_nr )) > 0 )
				{
// 					pDoc->SetLevel(CCatalog2Doc::levelTable);
					OnKommentari();
				}
				return 0;
			}
		}
		// 301104 E : Kommentar schreiben
		return 0 ;

	}	// levelNone zu ende

// #### DOUBLE CLICK AT COLUMN LEVEL
	if (pDoc->m_nLevel == CCatalog2Doc::levelColumn)
	{
		if (message == WM_NOTIFY &&
			((NMHDR*)lParam)->code == NM_DBLCLK)
		{

// Gegebnenfalls inserten A .....
			CListCtrl&  control = GetListCtrl();
			int nCount = control.GetItemCount();
			int i ;
			for ( i = 0; i < nCount; i++)
			{
				if (control.GetItemState(i,LVIS_SELECTED))
				break;
			}
			if (i < nCount)
			{
				nform_feld.lila = GlobalLila ;
				sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
				sprintf ( nform_feld.tab_nam,  "%s" , nform_tab.tab_nam );
				sprintf ( nform_feld.feld_nam, control.GetItemText(i,0));
				likeoracle( TAFELNAME ,nform_feld.feld_nam ) ;
				DbClass.sqlopen (formfeld_curs);
				if (!DbClass.sqlfetch(formfeld_curs))
				{
					// gefunden
				}
				else
				{
					sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
				    sprintf ( nform_feld.tab_nam,  "%s" , nform_tab.tab_nam );
					sprintf (nform_feld.feld_nam, control.GetItemText(i,0));
				    char *p = clipped (nform_feld.feld_nam ) ;	// 150509 : clipped dazu
					nform_feld.delstatus = 0 ;
					nform_feld.feld_typ = FT_STANDARD ;
					nform_feld.feld_id = 0 ;
					nform_feld.sort_nr = 0 ;
					sprintf ( nform_feld.krz_txt,  " " );
					nform_feld.range = 0 ;
					nform_feld.lila = GlobalLila ;

					DbClass.sqlexecute ( insfeld_curs );
				}

			}

			pDoc->SetLevel(CCatalog2Doc::levelColumn);	// 130104
// Gegebnenfalls inserten E .....
		}

// #### DOUBLE RIGHT-CLICK AT COLUMN LEVEL

		if (message == WM_NOTIFY &&
			((NMHDR*)lParam)->code == NM_RDBLCLK)
		{

			CListCtrl&  control = GetListCtrl();
			int nCount = control.GetItemCount();
			int i ;
			for ( i = 0; i < nCount; i++)
			{
				if (control.GetItemState(i,LVIS_SELECTED))
				break;
			}
			if (i < nCount)
			{
				nform_feld.lila = GlobalLila ;
				sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
				sprintf ( nform_feld.tab_nam,  "%s" , nform_tab.tab_nam );
				sprintf ( nform_feld.feld_nam, control.GetItemText(i,0));

				likeoracle( TAFELNAME,nform_feld.feld_nam ) ;
				
				DbClass.sqlopen (formfeld_curs);
				if (!DbClass.sqlfetch(formfeld_curs))
				{
					nform_feld.lila = GlobalLila ;
					sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
				    sprintf ( nform_feld.tab_nam,  "%s" , nform_tab.tab_nam );
					sprintf (nform_feld.feld_nam, control.GetItemText(i,0));
					likeoracle ( TAFELNAME,nform_feld.feld_nam ) ;
					
					DbClass.sqlexecute ( delfeld_curs );
					pDoc->SetLevel(CCatalog2Doc::levelColumn);	// 130104
				}
				else
				{
					// ignorieren
				}
			}
		}

// #### SINGLE RIGHT-CLICK AT COLUMN LEVEL
// gegbnenfalls inserten , Contextmenue aktivieren
		if (message == WM_NOTIFY &&
			((NMHDR*)lParam)->code == NM_RCLICK)
		{
			CListCtrl&  control = GetListCtrl();
			int nCount = control.GetItemCount();
			int i ;
			for ( i = 0; i < nCount; i++)
			{
				if (control.GetItemState(i,LVIS_SELECTED))
				break;
			}

			if (i < nCount)
			{
				nform_feld.lila = GlobalLila ;
				sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
				sprintf ( nform_feld.tab_nam,  "%s" , nform_tab.tab_nam );
				sprintf ( nform_feld.feld_nam, control.GetItemText(i,0));
				likeoracle ( TAFELNAME,nform_feld.feld_nam ) ;
				sprintf ( nform_feld.krz_txt,  " " );
				nform_feld.range = 0 ;

				DbClass.sqlopen (formfeld_curs);
				int vorhanden = 0 ;
				if (!DbClass.sqlfetch(formfeld_curs))
				{
					vorhanden = 1 ;
				}
				else
				{
					nform_feld.feld_typ = FT_STANDARD;
					vorhanden = 0 ;
					sprintf ( nform_feld.krz_txt,  " " );
					nform_feld.range = 0 ;
					nform_feld.lila = GlobalLila ;
				}

				CFeldEigenschaften felddial;
				felddial.putfeldtyp(nform_feld.feld_typ) ;
				felddial.putrange(nform_feld.range) ;
				felddial.putkrztxt(clipped ( nform_feld.krz_txt)) ;
				felddial.putfeldnam(nform_feld.feld_nam) ;
				felddial.DoModal();
				short savefeld_typ = nform_feld.feld_typ ;	// 130207 
				nform_feld.feld_typ = (short) felddial.getfeldtyp() ;
				nform_feld.range = (short) felddial.getrange() ;
				felddial.getkrztxt(nform_feld.krz_txt) ;	// Text-Tabellenname
				if ( nform_feld.feld_typ == FT_TEXTEXT || nform_feld.feld_typ == FT_FELDEXT )	// 071110
				{
					if ( ! vorhanden )
					{	// Neuanlage usw.
						holeexttext( vorhanden );
					}
					else
					{	// update usw.
						holeexttext( vorhanden );
					}
//					nform_feld.feld_typ = savefeld_typ ;	// NOTBREMSE, spaeter
															// weg von hier ...
				}
				if (( ! vorhanden ) && ( nform_feld.feld_typ != FT_LOESCH ))	// 220207
					// zwar doppelt, aber erlaubt : noch nicht da und gleich wieder weg ..
				{
					// Normal : NeuAnlage
					nform_feld.lila = GlobalLila ;
					sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
					sprintf ( nform_feld.tab_nam,  "%s" , nform_tab.tab_nam );
					sprintf (nform_feld.feld_nam, control.GetItemText(i,0));
					char *p = clipped (nform_feld.feld_nam ) ;	// 150509 : clipped dazu

					nform_feld.delstatus = 0 ;
					nform_feld.feld_id = 0 ;
					nform_feld.sort_nr = 0 ;
//					sprintf ( nform_feld.krz_txt, " " );
					DbClass.sqlexecute ( insfeld_curs );
					pDoc->SetLevel(CCatalog2Doc::levelColumn);	// 130104
				}
				else
				{
					if ( nform_feld.feld_typ == FT_LOESCH )
					{
						CListCtrl&  control = GetListCtrl();
						int nCount = control.GetItemCount();
						int i ;
						for ( i = 0; i < nCount; i++)
						{
							if (control.GetItemState(i,LVIS_SELECTED))
							break;
						}
						if (i < nCount)
						{
							nform_feld.lila = GlobalLila ;
							sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
							sprintf ( nform_feld.tab_nam,  "%s" , nform_tab.tab_nam );
							sprintf ( nform_feld.feld_nam, control.GetItemText(i,0));
							likeoracle (TAFELNAME, nform_feld.feld_nam ) ;	

							DbClass.sqlopen (formfeld_curs);
							if (!DbClass.sqlfetch(formfeld_curs))
							{
								nform_feld.lila = GlobalLila ;
								sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
								sprintf ( nform_feld.tab_nam,  "%s" , nform_tab.tab_nam );
								sprintf (nform_feld.feld_nam, control.GetItemText(i,0));
								char *p = clipped (nform_feld.feld_nam ) ;	// 150509 : clipped dazu

								DbClass.sqlexecute ( delfeld_curs );
								if ( savefeld_typ == FT_TEXTEXT || savefeld_typ == FT_FELDEXT)
								{
									form_t.lila = GlobalLila ;
									sprintf ( form_t.form_nr,  "%s" , myform_nr );
									sprintf ( form_t.tab_nam,  "%s" , nform_tab.tab_nam );
									sprintf ( form_t.feld_nam, control.GetItemText(i,0));

									form_te.lila = GlobalLila ;
									sprintf ( form_te.form_nr,  "%s" , myform_nr );
									sprintf ( form_te.tab_nam,  "%s" , nform_tab.tab_nam );
									sprintf ( form_te.feld_nam, control.GetItemText(i,0));
									char *p = clipped (form_te.feld_nam ) ;	// 150509 : clipped dazu

									DbClass.sqlexecute ( delform_t_curs );
									DbClass.sqlexecute ( delform_te_curs );
								}
								pDoc->SetLevel(CCatalog2Doc::levelColumn);	// 130104
							}
							else
							{
								// ignorieren
							}
						}
					}
			
					else
					{
						//				updaten mit werten ;
						DbClass.sqlexecute ( updfeld_curs );
						pDoc->SetLevel(CCatalog2Doc::levelColumn);						
					}
				}	// Vorhanden
			}	// i < count

		}		// RDCLICK

	}			// Columnlevel

	return CListView::OnChildNotify(message,wParam,lParam,pLResult);
}

void CCatalog2View::OnFileNew133()
{
#ifndef OLABEL
	CneuFormat newdial;

	 newdial.puttitel("Name-Dialog") ;
	 newdial.DoModal();
	 sprintf ( myform_nr ,"%s" ,clipped(newdial.neuformat())) ;
	 if ( strlen (myform_nr) > 4 )
	 {
		nform_tab.lila = GlobalLila ;
	    sprintf ( nform_tab.form_nr,  "%s" , myform_nr );
		DbClass.sqlopen (form_nr_curs);
		if (!DbClass.sqlfetch(form_nr_curs))
			
		{
				// gefunden
		}
		else
		{
			nform_tab.lila = GlobalLila ;
			sprintf ( nform_tab.form_nr,  "%s" , myform_nr );
		    sprintf ( nform_tab.tab_nam,  "" );
			nform_tab.delstatus = 0 ;
// Anlage dummy-Satz 
			DbClass.sqlexecute ( instab_curs );
		}
	 }
	 else
	 {}
#endif		// OLABEL
		CCatalog2Doc*   pDoc = GetDocument( );
		pDoc->SetLevel(CCatalog2Doc::levelNone);	// 130104
}


// 110506
void CCatalog2View::ptabschreiben ( HJOB hJob, char * varfeld , int imodus , char * tab , char * feld ) 
{
	// feld imodus lediglich aus Kompatibilitaetsgruenden noch vorhanden 
	char hilfsfeld [256] ;
	char hilfsfeld2[256] ;
	int schleife ;
	schleife = 0 ;

	while ( schleife  < 5 )
	{
// ptbezk
		if ( schleife == 0 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptbezk", tab, feld );
				sprintf ( hilfsfeld2, "%s.ptbezk",  feld );
		};
// ptbez
		if ( schleife == 1 ) 
		{
			sprintf ( hilfsfeld, "%s.%s.ptbez",tab, feld);
				sprintf ( hilfsfeld2, "%s.ptbez", feld );
		}
// ptwert
		if ( schleife == 2 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwert",tab, feld );
				sprintf ( hilfsfeld2, "%s.ptwert", feld);
		}

// ptwer1
		if ( schleife == 3 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwer1", tab, feld ) ;
				sprintf ( hilfsfeld2, "%s.ptwer1", feld ) ;
		}

// ptwer2
		if ( schleife == 4 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwer2", tab, feld ) ;
				sprintf ( hilfsfeld2, "%s.ptwer2", feld ) ;
		}

		if ( varfeld[0] == 'V' )
		{	
			if ( !imodus )
				LlDefineVariableExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
			else
				if ( LlPrintIsVariableUsed(hJob, hilfsfeld))
					LlDefineVariableExt(hJob, clipped( hilfsfeld),
								clipped(hilfsfeld2), LL_TEXT, NULL);
		}
		else
		{
			if ( !imodus )
				LlDefineFieldExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
			else
				if ( LlPrintIsFieldUsed(hJob, hilfsfeld))
					LlDefineFieldExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
		}
		schleife ++ ;
	} ;	// end while 

}

// 071110 
#ifdef FITODBC
void erzeugeextfelder( HJOB hJob , CCatalog2Doc* pDoc,  char * hilfsfeld , char * hilfsfeld2 , char typ[5] ) 
#endif
#ifndef FITODBC
void erzeugeextfelder( HJOB hJob , char * hilfsfeld , char * hilfsfeld2 , char typ[5] ) 
#endif
{
char feldname[33] ;

// 1. lese form_t-Satz 
	form_t.lila = GlobalLila ;
	sprintf ( form_t.form_nr, "%s", myform_nr );
	sprintf ( form_t.tab_nam, "%s", clipped( nform_feld.tab_nam) );
	sprintf ( form_t.feld_nam, "%s", clipped( nform_feld.feld_nam) ) ;

	DbClass.sqlopen (form_t_curs);

// Falls gefunden, alle Felder anzeigen ....
	if (!DbClass.sqlfetch(form_t_curs))
	{

#ifndef FITODBC
		sprintf ( systables.tabname , "%s" , clipped( form_t.ttab_nam));
		systables.tabid = 0 ;
		DbClass.sqlopen (tabid_curs);
		DbClass.sqlfetch(tabid_curs);
#endif
		for ( int feldnum = 0 ; feldnum < 5 ; feldnum ++ )
		{
			switch ( feldnum )
			{
				case 0 : sprintf ( feldname , "%s" , form_t.sfeld1 ) ; break ;
				case 1 : sprintf ( feldname , "%s" , form_t.sfeld2 ) ; break ;
				case 2 : sprintf ( feldname , "%s" , form_t.sfeld3 ) ; break ;
				case 3 : sprintf ( feldname , "%s" , form_t.sfeld4 ) ; break ;
				case 4 : sprintf ( feldname , "%s" , form_t.sfeld5 ) ; break ;

			}
			if ( strlen ( clipped (feldname )) == 0 )
				continue ;	// nix zu tun .....

			syscolumns.coltype  = iDBCHAR ;	// Notbremse

#ifndef FITODBC
			if ( !systables.tabid )	// Error
			{
				syscolumns.coltype  = iDBCHAR ;	// Notbremse
			}
			else
			{
				sprintf ( syscolumns.colname , "%s" , clipped( feldname));  
				syscolumns.coltype = iDBCHAR ;
				syscolumns.tabid = systables.tabid ;
				DbClass.sqlopen (colid_curs);
				DbClass.sqlfetch(colid_curs);
			}
#endif
					
#ifdef FITODBC
			pDoc->FetchColumnInfo(clipped(form_t.ttab_nam)
		                          ,clipped(feldname) ) ;
			pDoc->m_pColumnset->MoveFirst();
			if (!pDoc->m_pColumnset->IsEOF())
			{	// gefunden -> einigermassen unkrititsche Zuordnung
				syscolumns.coltype = (short)pDoc->m_pColumnset->m_nDataType ;
				switch (syscolumns.coltype)
				{
					case SQL_UNKNOWN_TYPE :
					case SQL_NUMERIC :
					case SQL_CHAR :
					case iSQL_VARCHAR :	// 280612 -> 290612
							syscolumns.coltype = iDBCHAR ;
						break ;
					case SQL_INTEGER :
					case iDBSERIAL :	// 280612
							syscolumns.coltype = iDBINTEGER ;
						break ;

					case SQL_SMALLINT : 
							syscolumns.coltype = iDBSMALLINT ;
						break ;

					case SQL_DECIMAL :
					case SQL_FLOAT :
					case SQL_REAL :
					case SQL_DOUBLE :
							syscolumns.coltype = iDBDECIMAL ;
						break ;

					case SQL_DATE :
							syscolumns.coltype = iDBDATUM ;
						break ;

					case SQL_TIMESTAMP :
							syscolumns.coltype = iDBTIMESTAMP ;
						break ;

				}
			}
			else
			{	// negativer Notnagel 
					syscolumns.coltype = iDBCHAR ;
			}
		
#endif

#ifndef FITODBC		// 140214 : da war noch was ...
					if ( syscolumns.coltype == SQL_TIME || syscolumns.coltype == SQL_TIMESTAMP )
							syscolumns.coltype = iDBTIMESTAMP;
#endif

			sprintf ( hilfsfeld , "%s.%s.%s.%s",
				clipped(nform_feld.tab_nam) ,clipped(nform_feld.feld_nam),
				    clipped(form_t.ttab_nam) , clipped(feldname) ) ;
			sprintf ( hilfsfeld2 , "%s" ,feldname ) ;

			if ( syscolumns.coltype == iDBCHAR
			  || syscolumns.coltype == iDBVARCHAR		// 280612 
			  || syscolumns.coltype == iDBDATUM 
			  || syscolumns.coltype == iDBTIMESTAMP )
			{	//  Datum-Default sinnvoll belegen 
				if ( syscolumns.coltype == iDBDATUM 
					|| syscolumns.coltype == iDBTIMESTAMP )
						sprintf ( hilfsfeld2, "20.12.2013");

				if ( typ[0] == 'V' )					
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,	clipped(hilfsfeld2), LL_TEXT, NULL);
				else
					LlDefineFieldExt(hJob, clipped( hilfsfeld) , clipped(hilfsfeld2), LL_TEXT, NULL);
			}
			else
			{
				if ( typ[0] == 'V' )					
					LlDefineVariableExt(hJob, clipped (hilfsfeld) ,	"1.0000", LL_NUMERIC, NULL);
				else
					LlDefineFieldExt(hJob, clipped (hilfsfeld) ,	"1.0000", LL_NUMERIC, NULL);
			}

		}	// schleife ueber felder
	}		// ueberhaupt was gefunden

}

void CCatalog2View::Variablendefinition (HJOB hJob)
{

	char hilfsfeld[256] ;

	char hilfsfeld2[128] ;

#ifdef FITODBC
	CCatalog2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
#endif

	sprintf ( systables.tabname, " " );	// initialisieren
	systables.tabid = 0 ;				// initialisieren

	nform_tab.lila = GlobalLila ;
	sprintf (nform_tab.form_nr ,"%s", myform_nr );
	sprintf (nform_tab.tab_nam ,"%%" );		// 061205

#ifndef OLABEL

	DbClass.sqlopen (formtab_curs);
	while (!DbClass.sqlfetch(formtab_curs))	// gefunden
	{

		nform_feld.lila = GlobalLila ;
		sprintf (nform_feld.form_nr ,"%s", myform_nr );
		sprintf (nform_feld.tab_nam ,"%s", nform_tab.tab_nam );
		sprintf (nform_feld.feld_nam ,"%%" );		// 061205

		DbClass.sqlopen (formfeld_curs);
		while (!DbClass.sqlfetch(formfeld_curs))	// gefunden
		{
			switch ( nform_feld.feld_typ )
			{

				case FT_PTABN : // ptabn
					ptabschreiben ( hJob, "V" , 0 ,
						clipped (nform_feld.tab_nam), clipped( nform_feld.feld_nam)) ;
					if ( nform_feld.range > 0 )			// 220813
						goto WIEDEFAULTV ;
					break ;

				case FT_TEXTBAUST : // Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-Textbaustein", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					break ;

				case FT_ADRESSE : // adresse
//					anr
					sprintf ( hilfsfeld, "%s.%s.anr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.anr", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_krz
					sprintf ( hilfsfeld, "%s.%s.adr_krz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_krz", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam1
					sprintf ( hilfsfeld, "%s.%s.adr_nam1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam1", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam2
					sprintf ( hilfsfeld, "%s.%s.adr_nam2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam2", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam3	090905
					sprintf ( hilfsfeld, "%s.%s.adr_nam3", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam3", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					ort1
					sprintf ( hilfsfeld, "%s.%s.ort1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort1", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 060307 A
//					ort2
					sprintf ( hilfsfeld, "%s.%s.ort2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort2", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					tel
					sprintf ( hilfsfeld, "%s.%s.tel", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.tel", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					fax
					sprintf ( hilfsfeld, "%s.%s.fax", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.fax", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//	291111				mobil
					sprintf ( hilfsfeld, "%s.%s.mobil", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.mobil", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//	300813			iban /	swift
					sprintf ( hilfsfeld, "%s.%s.swift", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.swift", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					sprintf ( hilfsfeld, "%s.%s.iban", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iban", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
		
					
//					partner
					sprintf ( hilfsfeld, "%s.%s.partner", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.partner", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 060307 E
//					plz
					sprintf ( hilfsfeld, "%s.%s.plz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					postfach
					sprintf ( hilfsfeld, "%s.%s.plz_pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz_pf", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

					sprintf ( hilfsfeld, "%s.%s.pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.pf", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					
//					str
					sprintf ( hilfsfeld, "%s.%s.str", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.str", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					iln	301104
					sprintf ( hilfsfeld, "%s.%s.iln", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iln", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr	021204
					sprintf ( hilfsfeld, "%s.%s.adr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"08150", LL_NUMERIC, NULL);

// 110506 : alles neu dazu 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					ptabschreiben ( hJob, "V" , 0 , hilfsfeld , "staat" ) ;

					break ;
				
				case FT_FELDEXT :

#ifdef FITODBC
					erzeugeextfelder ( hJob ,  pDoc , hilfsfeld , hilfsfeld2 , "V" ) ;
#else
					erzeugeextfelder ( hJob ,  hilfsfeld , hilfsfeld2 , "V" ) ;

#endif
					break ;

				case FT_TEXTEXT : // Ext-Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-Ext-Textbaustein", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// hier explizit weiter : wegen "_intern"-Variable 
//					break ;

				default :
WIEDEFAULTV:
					if ( nform_feld.feld_typ == FT_TEXTEXT )
						sprintf ( hilfsfeld, "%s.%s_intern", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					else	// 220207 : so war das bisher 
						sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

					    sprintf ( hilfsfeld2, "%s", clipped ( nform_feld.feld_nam) );
#ifndef FITODBC
					if ( strcmp ( clipped ( systables.tabname)  , clipped ( nform_feld.tab_nam)))
					{
						sprintf ( systables.tabname , "%s" , clipped( nform_feld.tab_nam));
						systables.tabid = 0 ;
						DbClass.sqlopen (tabid_curs);
						DbClass.sqlfetch(tabid_curs);

					}
					if ( !systables.tabid )	// Error
					{
						syscolumns.coltype  = iDBCHAR ;	// Notbremse
					}
					else
					{
						sprintf ( syscolumns.colname , "%s" , clipped( nform_feld.feld_nam));  
						syscolumns.coltype = iDBCHAR ;
						syscolumns.tabid = systables.tabid ;
						DbClass.sqlopen (colid_curs);
						DbClass.sqlfetch(colid_curs);
					}
#endif
					
					// 071205 : odbc-Ablauf dazu
#ifdef FITODBC
					pDoc->FetchColumnInfo(clipped(nform_feld.tab_nam)
		                          ,clipped(nform_feld.feld_nam) ) ;
					pDoc->m_pColumnset->MoveFirst();
					if (!pDoc->m_pColumnset->IsEOF())
					{
						// gefunden -> einigermassen unkrititsche Zuordnung

						syscolumns.coltype = (short)pDoc->m_pColumnset->m_nDataType ;
						switch (syscolumns.coltype)
						{
						case SQL_UNKNOWN_TYPE :
						case SQL_NUMERIC :
						case SQL_CHAR :
						case iSQL_VARCHAR :	// 280612 - 290612
							syscolumns.coltype = iDBCHAR ;
							break ;
						case iDBSERIAL :	// 280612
						case SQL_INTEGER :
							syscolumns.coltype = iDBINTEGER ;
							break ;

						case SQL_SMALLINT : 
							syscolumns.coltype = iDBSMALLINT ;
							break ;

						case SQL_DECIMAL :
						case SQL_FLOAT :
						case SQL_REAL :
						case SQL_DOUBLE :
								syscolumns.coltype = iDBDECIMAL ;
								break ;

						case SQL_DATE :
								syscolumns.coltype = iDBDATUM ;
								break ;
// 111205
						case SQL_TIMESTAMP :
								syscolumns.coltype = iDBTIMESTAMP ;
								break ;

						}
					
					
					}
					else
					{	// negativer Notnagel 
						syscolumns.coltype = iDBCHAR ;
					}
	
			

#endif

#ifndef FITODBC		// 140214 : da war noch was ...
					if ( syscolumns.coltype == SQL_TIME || syscolumns.coltype == SQL_TIMESTAMP )
							syscolumns.coltype = iDBTIMESTAMP;
#endif

					if ( syscolumns.coltype == iDBCHAR
					  || syscolumns.coltype == iDBVARCHAR		// 280612 
					  || syscolumns.coltype == iDBDATUM 
					  || syscolumns.coltype == iDBTIMESTAMP )
					{

// 200105 : Datum-Default sinnvoll belegen 
						if ( syscolumns.coltype == iDBDATUM 
						  || syscolumns.coltype == iDBTIMESTAMP )
							sprintf ( hilfsfeld2, "12.12.2013");
							if ( nform_feld.feld_typ != FT_PTABN )	// 220813
								LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
									clipped(hilfsfeld2), LL_TEXT, NULL);
					}
					else
					{
						if ( nform_feld.feld_typ != FT_PTABN )	// 220813
							LlDefineVariableExt(hJob, clipped (hilfsfeld) , 
								"1.0000", LL_NUMERIC, NULL);
					}

// 060307 A
					if (! ( strncmp ( clipped( nform_feld.feld_nam), "tou", 3 )))
					{
						if ( (strlen ( nform_feld.feld_nam )) == 3 ||
							 ! (strncmp(nform_feld.feld_nam ,"tou_nr",6)))
						{
							sprintf ( hilfsfeld, "%s.%s_bez", clipped (nform_feld.tab_nam)
							, clipped( nform_feld.feld_nam ));
								LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
					}
// 060307 E


// 200105 A
// 011205					if ( nform_feld.range == 1 )
					if ( nform_feld.range > 0 )
					{
						sprintf ( hilfsfeld, "rangt.%s.v%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

						if ( syscolumns.coltype == iDBCHAR
							|| syscolumns.coltype == iDBVARCHAR	// 280612
							|| syscolumns.coltype == iDBDATUM
							|| syscolumns.coltype == iDBTIMESTAMP )
						{
							LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
						else
						{
							LlDefineVariableExt(hJob, clipped (hilfsfeld) ,
								"1.0000", LL_NUMERIC, NULL);
						}

						sprintf ( hilfsfeld, "rangt.%s.b%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

						if ( syscolumns.coltype == iDBCHAR 
							||	syscolumns.coltype == iDBVARCHAR	// 280612
							||	syscolumns.coltype == iDBDATUM
							|| syscolumns.coltype == iDBTIMESTAMP )
						{
							LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
						else
						{
							LlDefineVariableExt(hJob, clipped (hilfsfeld) ,
								"2.0000", LL_NUMERIC, NULL);
						}
// 200909 : range als Bedingungsstring .....
						sprintf ( hilfsfeld, "rangt.%s.r%s", clipped (nform_feld.tab_nam)
							, clipped( nform_feld.feld_nam ));
						sprintf ( hilfsfeld2,"Wertebereich" ) ;

						LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

					}


// 200105 E
			}
		}
	}


#ifndef OLABEL	// 201213

	form_v.lila = GlobalLila ;
	sprintf ( form_v.form_nr,"%s", myform_nr );
	DbClass.sqlopen (form_v_curs);
	while (!DbClass.sqlfetch(form_v_curs))	// gefunden
	{
		sprintf ( hilfsfeld, "ali.%s", clipped (form_v.klar_txt));

		if ( form_v.typ == SQLSHORT ||form_v.typ == SQLLONG ||form_v.typ == iDBDECIMAL )	// 310114 SQLDOUBLE-> iDBDECIMAL
		{
			LlDefineVariableExt(hJob,hilfsfeld , "1234", LL_NUMERIC, NULL);
		}
		else
//		if ( form_v.typ == SQLCHAR ||form_v.typ == SQLDATE  )
		{
			LlDefineVariableExt(hJob,hilfsfeld , "ali.ABC", LL_TEXT, NULL);
		}
	}
#endif
	
	
	
// 150305
	LlDefineVariableExt(hJob,"rangt.nachkpreis" ,
								"2.0000", LL_NUMERIC, NULL);
// 270307
	LlDefineVariableExt(hJob,"rangt.sortnr" ,
								"0", LL_NUMERIC, NULL);

// 030408 
	LlDefineVariableExt(hJob,"rangt.anzvon" ,
								"1", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob,"rangt.anzges" ,
								"2", LL_NUMERIC, NULL);

// 220107
	LlDefineVariableExt(hJob,"rangt.Heute" ,
								"12.12.2013", LL_TEXT, NULL);
	LlDefineVariableExt(hJob,"rangt.Zeit" ,	"11:55:00", LL_TEXT, NULL);
	LlDefineVariableExt(hJob,"rangt.Nutzer" ,
								"Nu-Name", LL_TEXT, NULL);
// 110309
	LlDefineVariableExt(hJob,"rangt.variante" ,	"0", LL_NUMERIC, NULL);

#endif	// ifndef OLABEL

#ifdef OLABEL
	
	int schleife = olaopendaten(0) ;
	while ( schleife > 0 )
	{
		schleife -- ;
		sprintf ( hilfsfeld, "fitet.%s", olafeldname[schleife] );
		if ( olafeldtyp[schleife] == 1 )	// Numerisch
		{

			LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"2.00" , LL_NUMERIC, NULL);
		}
		else								// rest alphanum.
		{
				LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								olafeldname[schleife], LL_TEXT, NULL);
		}
	}

// 251011 A
			sprintf ( hilfsfeld , "rangt.anzges" ) ;
			LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"2.00" , LL_NUMERIC, NULL);
			sprintf ( hilfsfeld , "rangt.anzvon" ) ;
			LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"1.00" , LL_NUMERIC, NULL);
// 251011 E


#endif
}


void CCatalog2View::Felderdefinition (HJOB hJob)
{

	char hilfsfeld[256] ;
	char hilfsfeld2[128] ;



#ifdef FITODBC
	CCatalog2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
#endif


	sprintf ( systables.tabname, " " );	// initialisieren
	systables.tabid = 0 ;				// initialisieren

	nform_tab.lila = GlobalLila ;
	sprintf (nform_tab.form_nr ,"%s", myform_nr );
	sprintf (nform_tab.tab_nam ,"%%" );		// 061205

#ifndef OLABEL

	DbClass.sqlopen (formtab_curs);
	while (!DbClass.sqlfetch(formtab_curs))	// gefunden
	{

		nform_feld.lila = GlobalLila ;
		sprintf (nform_feld.form_nr ,"%s", myform_nr );
		sprintf (nform_feld.tab_nam ,"%s", nform_tab.tab_nam );
		sprintf (nform_feld.feld_nam ,"%%" );		// 061205

		DbClass.sqlopen (formfeld_curs);
		while (!DbClass.sqlfetch(formfeld_curs))	// gefunden
		{
			switch ( nform_feld.feld_typ )
			{

				case FT_PTABN : // ptabn

					ptabschreiben ( hJob, "F" , 0 ,
					clipped (nform_feld.tab_nam), clipped( nform_feld.feld_nam) ) ;	

				break ;

				case FT_TEXTBAUST : // Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-Textbaustein", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					break ;

				case FT_ADRESSE : // adresse
//					anr
					sprintf ( hilfsfeld, "%s.%s.anr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.anr", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_krz
					sprintf ( hilfsfeld, "%s.%s.adr_krz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_krz", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam1
					sprintf ( hilfsfeld, "%s.%s.adr_nam1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam1", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam2
					sprintf ( hilfsfeld, "%s.%s.adr_nam2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam2", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_nam3	090905
					sprintf ( hilfsfeld, "%s.%s.adr_nam3", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam3", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ort1
					sprintf ( hilfsfeld, "%s.%s.ort1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort1", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 060307 A 
//					ort2
					sprintf ( hilfsfeld, "%s.%s.ort2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort2", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					tel
					sprintf ( hilfsfeld, "%s.%s.tel", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.tel", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					fax
					sprintf ( hilfsfeld, "%s.%s.fax", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.fax", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//	291111			mobil
					sprintf ( hilfsfeld, "%s.%s.mobil", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.mobil", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//	300813			iban / swift
					sprintf ( hilfsfeld, "%s.%s.iban", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iban", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					sprintf ( hilfsfeld, "%s.%s.swift", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.swift", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					partner
					sprintf ( hilfsfeld, "%s.%s.partner", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.partner", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 060307 E
//					plz
					sprintf ( hilfsfeld, "%s.%s.plz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					postfach
					sprintf ( hilfsfeld, "%s.%s.plz_pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz_pf", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					sprintf ( hilfsfeld, "%s.%s.pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.pf", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					str
					sprintf ( hilfsfeld, "%s.%s.str", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.str", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					iln		301104
					sprintf ( hilfsfeld, "%s.%s.iln", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iln", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr		021204
					sprintf ( hilfsfeld, "%s.%s.adr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								"08150", LL_NUMERIC, NULL);

// 110506 : alles neu dazu 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					ptabschreiben ( hJob, "F" , 0 , hilfsfeld , "staat" ) ;


					break ;

				case FT_FELDEXT :
#ifdef FITODBC
					erzeugeextfelder ( hJob, pDoc , hilfsfeld , hilfsfeld2 , "F") ;
#else
					erzeugeextfelder ( hJob, hilfsfeld , hilfsfeld2 , "F") ;
#endif
					break ;

				case FT_TEXTEXT : // Ext-Textbaustein 220207
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-Ext-Textbaustein", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
				// hier kein break, sondern fortlaufend UND zusaetzlich "int-Variable 
				//	break ;


				default :
					if ( nform_feld.feld_typ == FT_TEXTEXT )
						sprintf ( hilfsfeld, "%s.%s_intern", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					else	// 220207 : alle anderen ....
						sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					
					sprintf ( hilfsfeld2, "%s", clipped ( nform_feld.feld_nam) );
					
#ifndef FITODBC
					if ( strcmp ( systables.tabname , clipped ( nform_feld.tab_nam)))
					{
						sprintf ( systables.tabname , "%s" , clipped( nform_feld.tab_nam));
						systables.tabid = 0 ;
						DbClass.sqlopen (tabid_curs);
						DbClass.sqlfetch(tabid_curs);
					}
					if ( !systables.tabid )	// Error
					{
						syscolumns.coltype  = iDBCHAR ;	// Notbremse
					}
					else
					{
						sprintf ( syscolumns.colname , "%s" , clipped( nform_feld.feld_nam));  
						syscolumns.coltype = iDBCHAR ;
						syscolumns.tabid = systables.tabid ;
						DbClass.sqlopen (colid_curs);
						DbClass.sqlfetch(colid_curs);
					}
#endif
										// 071205 : odbc-Ablauf dazu
#ifdef FITODBC
					pDoc->FetchColumnInfo(clipped(nform_feld.tab_nam)
		                          ,clipped(nform_feld.feld_nam) ) ;
					pDoc->m_pColumnset->MoveFirst();
					if (!pDoc->m_pColumnset->IsEOF())
					{
						// gefunden -> einigermassen unkrititsche Zuordnung

						syscolumns.coltype = (short)pDoc->m_pColumnset->m_nDataType ;
						switch (syscolumns.coltype)
						{
						case SQL_UNKNOWN_TYPE :
						case SQL_NUMERIC :
						case SQL_CHAR :
						case iSQL_VARCHAR :	// 280612 - 290612
							syscolumns.coltype = iDBCHAR ;
							break ;
						case SQL_INTEGER :
						case iDBSERIAL :	// 280612
							syscolumns.coltype = iDBINTEGER ;
							break ;

						case SQL_SMALLINT : 
							syscolumns.coltype = iDBSMALLINT ;
							break ;

						case SQL_DECIMAL :
						case SQL_FLOAT :
						case SQL_REAL :
						case SQL_DOUBLE :
								syscolumns.coltype = iDBDECIMAL ;
								break ;

						case SQL_DATE :
								syscolumns.coltype = iDBDATUM ;
								break ;
// 111205
						case SQL_TIMESTAMP :
								syscolumns.coltype = iDBTIMESTAMP ;
								break ;
						}
					
					
					}
					else
					{	// negativer Notnagel 
						syscolumns.coltype = iDBCHAR ;
					}
	
#endif

#ifndef FITODBC		// 140214 : da war noch was ...
					if ( syscolumns.coltype == SQL_TIME || syscolumns.coltype == SQL_TIMESTAMP )
							syscolumns.coltype = iDBTIMESTAMP;
#endif
											// 111205 :TIMESTAMP
					if ( syscolumns.coltype == iDBCHAR
						|| syscolumns.coltype == iDBVARCHAR		// 280612 
						|| syscolumns.coltype == iDBDATUM 
						|| syscolumns.coltype == iDBTIMESTAMP )
						LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					else
						LlDefineFieldExt(hJob, clipped (hilfsfeld) ,
								"1.0000", LL_NUMERIC, NULL);

// 060307 A
					if (! ( strncmp ( clipped( nform_feld.feld_nam), "tou", 3 )))
					{
						if ( (strlen ( nform_feld.feld_nam )) == 3 ||
							 ! (strncmp(nform_feld.feld_nam ,"tou_nr",6)))
						{
							sprintf ( hilfsfeld, "%s.%s_bez", clipped (nform_feld.tab_nam)
							, clipped( nform_feld.feld_nam ));
								LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
					}
// 060307 E


// 150508 A
					if ( nform_feld.range > 0 )
					{
						sprintf ( hilfsfeld, "rangt.%s.v%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

						if ( syscolumns.coltype == iDBCHAR
							|| syscolumns.coltype == iDBVARCHAR		// 280612
							|| syscolumns.coltype == iDBDATUM
							|| syscolumns.coltype == iDBTIMESTAMP )
						{
							LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
						else
						{
							LlDefineFieldExt(hJob, clipped (hilfsfeld) ,
								"1.0000", LL_NUMERIC, NULL);
						}

						sprintf ( hilfsfeld, "rangt.%s.b%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

						if ( syscolumns.coltype == iDBCHAR 
							||	syscolumns.coltype == iDBVARCHAR	// 280612
							||	syscolumns.coltype == iDBDATUM
							|| syscolumns.coltype == iDBTIMESTAMP )
						{
							LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
						else
						{
							LlDefineFieldExt(hJob, clipped (hilfsfeld) ,
								"2.0000", LL_NUMERIC, NULL);
						}
// 200909 : range als Bedingungsstring .....
						sprintf ( hilfsfeld, "rangt.%s.r%s", clipped (nform_feld.tab_nam)
							, clipped( nform_feld.feld_nam ));
						sprintf ( hilfsfeld2,"Wertebereich" ) ;

						LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

					}

// 150508 E

			}
		}
	}
// 270307

#ifndef OLABEL	// 201213

	form_v.lila = GlobalLila ;
	sprintf ( form_v.form_nr,"%s", myform_nr );
	DbClass.sqlopen (form_v_curs);
	while (!DbClass.sqlfetch(form_v_curs))	// gefunden
	{
		sprintf ( hilfsfeld, "ali.%s", clipped (form_v.klar_txt));
		if ( form_v.typ == SQLSHORT ||form_v.typ == SQLLONG ||form_v.typ == iDBDECIMAL )	// 310114 SQLDOUBLE -> iDBDECIMAL
		{
			LlDefineFieldExt(hJob,hilfsfeld , "1234", LL_NUMERIC, NULL);
		}
		else
//		if ( form_v.typ == SQLCHAR ||form_v.typ == SQLDATE  )
		{
			LlDefineFieldExt(hJob,hilfsfeld , "ali.ABC", LL_TEXT, NULL);
		}
	}
#endif


	LlDefineFieldExt(hJob,"rangt.sortnr" ,
								"1", LL_NUMERIC, NULL);
// 150305
	LlDefineFieldExt(hJob,"rangt.nachkpreis" ,
								"2.0000", LL_NUMERIC, NULL);
// 220107
	LlDefineFieldExt(hJob,"rangt.Heute" ,
								"12.12.2013", LL_TEXT, NULL);
	LlDefineFieldExt(hJob,"rangt.Zeit" , "11:55:00", LL_TEXT, NULL);
	LlDefineFieldExt(hJob,"rangt.Nutzer" ,
								"Nu-Name", LL_TEXT, NULL);
// 110309
	LlDefineFieldExt(hJob,"rangt.variante" ,	"0", LL_NUMERIC, NULL);

#endif	// ifndef OLABEL	-> Felder gibt es beim Etikett nicht
}



// Das ist dann schon der ganze designer .......
void CCatalog2View::OnLulListendesign()
{


	if (strlen ( myform_nr ) == 0 )
	{
		return ;
	}

	char envchar[512] ;

	HWND hWnd = m_hWnd;
	HJOB hJob;
	char filename[512];


	if (! GlobalLila )
      sprintf (filename, "%s\\format\\LL\\%s.lst", odbbasisverz, clipped (myform_nr));
	else
#ifdef OLABEL
      sprintf (filename, "%s\\format\\LuLabel\\%s.lbl", odbbasisverz, clipped (myform_nr));
#else
      sprintf (filename, "%s\\format\\LL\\%s.lbl", odbbasisverz, clipped (myform_nr));
#endif
//	FILENAME_DEFAULT = filename;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

	
// Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden (Siehe redist.txt)
// CELL11 : das war lizenz fuer LuL09  :LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");


#ifdef LL19
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "V2XSEQ");
#else

	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");
#endif

	LlSetDefaultProjectParameter(hJob,"LL.MAIL.To","EMAIL",0);

    char * penvchar ;
#ifdef OLABEL
	sprintf ( envchar ,"%s\\format\\LuLabel\\" , odbbasisverz);
#else
	sprintf ( envchar ,"%s\\format\\LL\\" , odbbasisverz);
#endif

	LlSetPrinterDefaultsDir(hJob, envchar) ;	// GrJ Printer-Datei

	penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// GrJ Vorschau-Datei


	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	LlSetOption(hJob, LL_OPTION_NOMAILVARS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

 	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
/* ---->
	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
		{
	        LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}
	< ---- */

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    // GR: Zur�cksetzen der internen Feld-Puffer
    LlDefineFieldStart(hJob);

	Variablendefinition (hJob);

	if ( ! GlobalLila )			// Labels haben nur Variablen .......
		Felderdefinition (hJob);

/* --->
	if (Listenauswahl != 1)  
    {
	   	sprintf ( szFilename, FILENAME_DEFAULT );
	}
< ----- */
	// szFilename -> filename 

	if ( !GlobalLila )
	{
		if (LlDefineLayout(hJob, hWnd, "Listendesign", LL_PROJECT_LIST, filename) < 0)
		{
			MessageBox("Error by calling LlDefineLayout", "Listendesign", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}
	else
	{
		if (LlDefineLayout(hJob, hWnd, "Labeldesign", LL_PROJECT_LABEL, filename) < 0)
		{
			MessageBox("Error by calling LlDefineLayout", "Labeldesign", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
	CCatalog2Doc*   pDoc = GetDocument( );
	pDoc->SetLevel(CCatalog2Doc::levelNone);	
	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
}

void subrueck ( char * text )
{
	int geslen, ipo ;
	unsigned char uchar ;
//	geslen = (int) strlen ( clipped( text)) ;
	geslen = (int) strlen ( text) ;	// 010507 
	for ( ipo = 0 ; ipo < geslen ; ipo ++ )
	{
		uchar = text[ipo] ;
		if (uchar > 0x80 && uchar < 0xa0 ) text[ipo] = uchar - 0x80 ;
	}

}
void subhin ( char * text )
{
	int geslen, ipo ;
//	geslen = (int) strlen ( clipped( text)) ;
	geslen = (int) strlen (  text) ;	// 010507 
	for ( ipo = 0 ; ipo < geslen ; ipo ++ )
	{
		if (text[ipo] < 0x20 ) text[ipo] = text[ipo] + 0x80 ;
	}

}


#define FORM_OWLEN 50

void CCatalog2View::OnLulWhere()
{

	COrderWhere dialwhere;

#ifndef OLABEL

	int i,j,k ;
	if ( strlen(clipped(myform_nr)) > 4 )
	{

		form_w.lila = GlobalLila ;
		sprintf ( form_w.form_nr, "%s", myform_nr );
		DbClass.sqlopen (form_w_curs);
		int vorhanden = 0 ;
		// 010507 : nicht mehr : Wichtig : Spaces gegbnenfalls am Anfang des jeweiligen Feldes !! 
		if (!DbClass.sqlfetch(form_w_curs))
		{
			subrueck ( form_w.krz_txt ) ;	// 021204 
//			sprintf ( klausel , "%s", clipped (form_w.krz_txt)) ;
			sprintf ( klausel , "%s", form_w.krz_txt) ;	// 010507
			vorhanden = 1 ;
			while (!DbClass.sqlfetch(form_w_curs))
			{
//				j = (int)strlen ( clipped( klausel)) ;
				j = (int)strlen ( klausel) ;	// 010507
				subrueck ( form_w.krz_txt ) ;	// 021204 
//				sprintf ( klausel + j , "%s" , clipped ( form_w.krz_txt));
				sprintf ( klausel + j , "%s" , form_w.krz_txt);	// 010507 
			}
		}
		else
		{
			sprintf ( klausel , " " );
			vorhanden = 0 ;
		}

		dialwhere.putkrztxt(klausel) ;
		dialwhere.putcapture("where" ) ;
		dialwhere.DoModal();
		dialwhere.getkrztxt(klausel) ;
		i = (int) strlen ( clipped(klausel));
		j = k = 0 ;

		if ( vorhanden)
		{
			DbClass.sqlexecute ( delform_w_curs );
		}

		while ( i > FORM_OWLEN )
		{
			form_w.zei = k ++ ;
			memcpy( form_w.krz_txt,klausel + j , FORM_OWLEN );
			form_w.krz_txt[FORM_OWLEN]= '\0' ;
			i -= FORM_OWLEN ; j += FORM_OWLEN ;
			subhin ( form_w.krz_txt ) ;	// 021204  
			DbClass.sqlexecute ( insform_w_curs );
			if ( form_w.krz_txt[FORM_OWLEN - 1 ] == ' ')	// Endblank transferieren
			{
				i++ ;
				j-- ;
			}
		}

		if ( i )
		{
			form_w.zei = k ++ ;
			sprintf( form_w.krz_txt,"%s", clipped ( klausel + j ));
			subhin ( form_w.krz_txt ) ;	// 021204  
			DbClass.sqlexecute ( insform_w_curs );
		}
	}
#endif
}

void CCatalog2View::OnLulOrder()
{

	COrderWhere dialorder;


#ifndef OLABEL

	int i,j,k ;

	if ( strlen(clipped(myform_nr)) > 4 )
	{

		
		form_o.lila = GlobalLila ;
		sprintf ( form_o.form_nr, "%s", myform_nr );
		DbClass.sqlopen (form_o_curs);
		int vorhanden = 0 ;

		if (!DbClass.sqlfetch(form_o_curs))
		{
			subrueck ( form_o.krz_txt );	// 021204 
//			sprintf ( klausel , "%s", clipped (form_o.krz_txt)) ;
			sprintf ( klausel , "%s", form_o.krz_txt) ;	// 010507
			vorhanden = 1 ;
			while (!DbClass.sqlfetch(form_o_curs))
			{
//				j = (int)strlen ( clipped( klausel)) ;
				j = (int)strlen (  klausel) ;	// 010507
				subrueck ( form_o.krz_txt );	// 021204 
//				sprintf ( klausel + j , "%s" , clipped ( form_o.krz_txt));
				sprintf ( klausel + j , "%s" , form_o.krz_txt);
			}
		}
		else
		{
			sprintf ( klausel , " " );
			vorhanden = 0 ;
		}

		dialorder.putkrztxt(clipped ( klausel)) ;
		dialorder.putcapture("order by" ) ;
		dialorder.DoModal();
		dialorder.getkrztxt(klausel) ;

		i = (int) strlen ( clipped(klausel));
		j = k = 0 ;

		if ( vorhanden)
		{
			DbClass.sqlexecute ( delform_o_curs );
		}

		while ( i > FORM_OWLEN )
		{
			form_o.zei = k ++ ;
			memcpy( form_o.krz_txt,klausel + j , FORM_OWLEN );
			form_o.krz_txt[FORM_OWLEN]= '\0' ;
			i -= FORM_OWLEN ; j += FORM_OWLEN ;
			subhin ( form_o.krz_txt );	// 021204 
			DbClass.sqlexecute ( insform_o_curs );
			if ( form_o.krz_txt[FORM_OWLEN - 1 ] == ' ')	// Endblank transferieren
			{
				i++ ;
				j-- ;
			}
		}

		if ( i )
		{
			form_o.zei = k ++ ;
			sprintf( form_o.krz_txt,"%s", clipped ( klausel + j ));
			subhin ( form_o.krz_txt );	// 021204 
			DbClass.sqlexecute ( insform_o_curs );
		}
	}
#endif
}


void CCatalog2View::OnLulVirtuell()
{


	DIALVIRTUELL dialvirtuell;


#ifndef OLABEL


	if ( strlen(clipped(myform_nr)) > 4 )
	{

		
		dialvirtuell.getLila (GlobalLila) ;
		dialvirtuell.getListe ( myform_nr );
//		dialorder.putcapture("order by" ) ;
		dialvirtuell.DoModal();
	}
#endif
}


void CCatalog2View::OnFileDelete()
{

	CneuFormat loedial;
#ifndef OLABEL
	 loedial.puttitel("Delete-Dialog") ;
	 loedial.DoModal();

	 
	 sprintf ( myform_nr ,"%s" ,clipped(loedial.neuformat())) ;
	 if ( strlen (myform_nr) > 4 )
	 {
		nform_tab.lila = GlobalLila ;
		sprintf ( nform_tab.form_nr,  "%s" , myform_nr );
		sprintf ( nform_tab.tab_nam,  "%%"  );		// 061205
		DbClass.sqlexecute ( deltab_curs );

// 301104 A
		nform_hea.lila = GlobalLila ;
		sprintf ( nform_hea.form_nr,  "%s" , myform_nr );
		likeoracle ( NR_NAME, nform_hea.form_nr) ;
		DbClass.sqlexecute ( form_h_d_curs );
// 301104 E

		nform_feld.lila = GlobalLila ;
		sprintf ( nform_feld.form_nr,  "%s" , myform_nr );
		sprintf (nform_feld.tab_nam, "%%");		// 061205
		sprintf ( nform_feld.feld_nam,  "%%" );	// 061205
		DbClass.sqlexecute ( delfeld_curs );

		form_o.lila = GlobalLila ;
		sprintf ( form_o.form_nr,  "%s" , myform_nr );
		DbClass.sqlexecute ( delform_o_curs );
			
		form_w.lila = GlobalLila ;
		sprintf ( form_w.form_nr,  "%s" , myform_nr );
		DbClass.sqlexecute ( delform_w_curs );

		// 220207 
		form_t.lila = GlobalLila ;
		sprintf ( form_t.form_nr,  "%s" , myform_nr );
		sprintf ( form_t.tab_nam, "%%");
		sprintf ( form_t.feld_nam,  "%%" );
		DbClass.sqlexecute ( delform_t_curs );

		form_te.lila = GlobalLila ;
		sprintf ( form_te.form_nr,  "%s" , myform_nr );
		sprintf ( form_te.tab_nam, "%%");
		sprintf ( form_te.feld_nam,  "%%" );
		DbClass.sqlexecute ( delform_te_curs );

		char filename[256];

	
		if ( !GlobalLila )
		{
		sprintf (filename, "%s\\format\\LL\\%s.lst",
				odbbasisverz , myform_nr);
				unlink( filename ); 
		sprintf (filename, "%s\\format\\LL\\%s.lsp",
				odbbasisverz , myform_nr);
				unlink( filename );
		}
		else
		{
		sprintf (filename, "%s\\format\\LL\\%s.lbl",
				 odbbasisverz , myform_nr);
				unlink( filename ); 
		sprintf (filename, "%s\\format\\LL\\%s.lbp",
				odbbasisverz , myform_nr);
				unlink( filename );
		}

	 }
	 else
	 {}
	CCatalog2Doc*   pDoc = GetDocument( );
	pDoc->SetLevel(CCatalog2Doc::levelNone);	// 130104
#endif	// OLABEL
}



void holeexttext(int vorhanden)
{

	// vorhanden ist die externe Info zur totalen Neuanlage ( oder eben nicht )
	CExtFeldEigenschaften extfelddial;
	
#ifndef OLABEL
	int i,j,k ;

	if ( strlen(clipped(myform_nr)) > 4 )
	{
		form_t.lila = GlobalLila ;
// Pruefen, ob feldname passt !! 
		sprintf ( form_t.form_nr, "%s", myform_nr );
		sprintf ( form_t.tab_nam, "%s", clipped( nform_feld.tab_nam ) );
		sprintf ( form_t.feld_nam, "%s", clipped( nform_feld.feld_nam) ) ;

		DbClass.sqlopen (form_t_curs);
		int vorhandent = 0 ;
		if (!DbClass.sqlfetch(form_t_curs))
		{
			vorhandent = 1 ;
			extfelddial.putttab_nam(form_t.ttab_nam);

			extfelddial.putsfeld1(form_t.sfeld1);
			extfelddial.putsfeld2(form_t.sfeld2);
			extfelddial.putsfeld3(form_t.sfeld3);
			extfelddial.putsfeld4(form_t.sfeld4);
			extfelddial.putsfeld5(form_t.sfeld5);

			extfelddial.putwtab1(form_t.wtab1);
			extfelddial.putwtab2(form_t.wtab2);
			extfelddial.putwtab3(form_t.wtab3);
			extfelddial.putwtab4(form_t.wtab4);
			extfelddial.putwtab5(form_t.wtab5);

			extfelddial.putwfeld1(form_t.wfeld1);
			extfelddial.putwfeld2(form_t.wfeld2);
			extfelddial.putwfeld3(form_t.wfeld3);
			extfelddial.putwfeld4(form_t.wfeld4);
			extfelddial.putwfeld5(form_t.wfeld5);

			extfelddial.putofeld1(form_t.ofeld1);
			extfelddial.putofeld2(form_t.ofeld2);
			extfelddial.putofeld3(form_t.ofeld3);
			extfelddial.putofeld4(form_t.ofeld4);
			extfelddial.putofeld5(form_t.ofeld5);

			extfelddial.putodesc1(form_t.odesc1);
			extfelddial.putodesc2(form_t.odesc2);
			extfelddial.putodesc3(form_t.odesc3);
			extfelddial.putodesc4(form_t.odesc4);
			extfelddial.putodesc5(form_t.odesc5);
		}
		else
		{
			vorhandent = 0 ;
			extfelddial.putttab_nam("");
			extfelddial.putsfeld1("");
			extfelddial.putsfeld2("");
			extfelddial.putsfeld3("");
			extfelddial.putsfeld4("");
			extfelddial.putsfeld5("");

			extfelddial.putwtab1("");
			extfelddial.putwtab2("");
			extfelddial.putwtab3("");
			extfelddial.putwtab4("");
			extfelddial.putwtab5("");

			extfelddial.putwfeld1("");
			extfelddial.putwfeld2("");
			extfelddial.putwfeld3("");
			extfelddial.putwfeld4("");
			extfelddial.putwfeld5("");

			extfelddial.putofeld1("");
			extfelddial.putofeld2("");
			extfelddial.putofeld3("");
			extfelddial.putofeld4("");
			extfelddial.putofeld5("");

			extfelddial.putodesc1(0);
			extfelddial.putodesc2(0);
			extfelddial.putodesc3(0);
			extfelddial.putodesc4(0);
			extfelddial.putodesc5(0);
		}
// pruefen, ob feldname korrekt ist ;-)
		form_te.lila = GlobalLila ;
		sprintf ( form_te.form_nr, "%s", myform_nr );
		sprintf ( form_te.tab_nam, "%s", clipped( nform_feld.tab_nam) );
		sprintf ( form_te.feld_nam, "%s", clipped( nform_feld.feld_nam) ) ;

		DbClass.sqlopen (form_te_curs);
		int vorhandente = 0 ;
		// Wichtig : Spaces gegbnenfalls am Anfang des jeweiligen Feldes !! 
		if (!DbClass.sqlfetch(form_te_curs))
		{
			subrueck ( form_te.krz_txt ) ;
//			sprintf ( klausel , "%s", clipped (form_te.krz_txt)) ;
			sprintf ( klausel , "%s", form_te.krz_txt) ;	// 010507
			vorhandente = 1 ;
			while (!DbClass.sqlfetch(form_te_curs))
			{
//				j = (int)strlen ( clipped( klausel)) ;
				j = (int)strlen ( klausel) ;	// 010507
				subrueck ( form_te.krz_txt ) ;
//				sprintf ( klausel + j , "%s" , clipped ( form_te.krz_txt));
				sprintf ( klausel + j , "%s" , form_te.krz_txt);	// 010507
			}
		}
		else
		{
			sprintf ( klausel , " " );
			vorhandente = 0 ;
		}

		extfelddial.putkrztxt(klausel) ;
//		extfelddial.putcapture("where" ) ;

		extfelddial.DoModal();
		int retstat = extfelddial.getretstat() ;
		if ( ! retstat )
		{
			// Verwurf der aktuellen Eingaben -> Achtung, was passiert
			//  bei Neuanlage und fehlenden Daten ?? 
			// es passiert halt NICHTS
		}
		else	// aktualisieren der Daten 
		{
			extfelddial.getkrztxt(klausel) ;
			i = (int) strlen ( clipped(klausel));
			j = k = 0 ;

			if ( vorhandente)
			{
				DbClass.sqlexecute ( delform_te_curs );
			}

			while ( i > FORM_OWLEN )
			{
				form_te.zei = k ++ ;
				memcpy( form_te.krz_txt,klausel + j , FORM_OWLEN );
				form_te.krz_txt[FORM_OWLEN]= '\0' ;
				i -= FORM_OWLEN ; j += FORM_OWLEN ;
				subhin ( form_te.krz_txt ) ;
				DbClass.sqlexecute ( insform_te_curs );
				if ( form_te.krz_txt[FORM_OWLEN - 1 ] == ' ')	// Endblank transferieren
				{
					i++ ;
					j-- ;
				}
			}

			if ( i )
			{
				form_te.zei = k ++ ;
				sprintf( form_te.krz_txt,"%s", clipped ( klausel + j ));
				subhin ( form_te.krz_txt ) ;
				DbClass.sqlexecute ( insform_te_curs );
			}

			extfelddial.getttab_nam(form_t.ttab_nam);
			extfelddial.getsfeld1(form_t.sfeld1);
			extfelddial.getsfeld2(form_t.sfeld2);
			extfelddial.getsfeld3(form_t.sfeld3);
			extfelddial.getsfeld4(form_t.sfeld4);
			extfelddial.getsfeld5(form_t.sfeld5);

			extfelddial.getwtab1(form_t.wtab1);
			extfelddial.getwtab2(form_t.wtab2);
			extfelddial.getwtab3(form_t.wtab3);
			extfelddial.getwtab4(form_t.wtab4);
			extfelddial.getwtab5(form_t.wtab5);

			extfelddial.getwfeld1(form_t.wfeld1);
			extfelddial.getwfeld2(form_t.wfeld2);
			extfelddial.getwfeld3(form_t.wfeld3);
			extfelddial.getwfeld4(form_t.wfeld4);
			extfelddial.getwfeld5(form_t.wfeld5);

			extfelddial.getofeld1(form_t.ofeld1);
			extfelddial.getofeld2(form_t.ofeld2);
			extfelddial.getofeld3(form_t.ofeld3);
			extfelddial.getofeld4(form_t.ofeld4);
			extfelddial.getofeld5(form_t.ofeld5);

			form_t.odesc1 = extfelddial.getodesc1() ;
			form_t.odesc2 = extfelddial.getodesc2() ;
			form_t.odesc3 = extfelddial.getodesc3() ;
			form_t.odesc4 = extfelddial.getodesc4() ;
			form_t.odesc5 =	extfelddial.getodesc5() ;
			if ( vorhandent)
			{
				DbClass.sqlexecute ( delform_t_curs );
			}
			if ( form_t.odesc5 == 1 ) nform_feld.feld_typ = FT_FELDEXT ;
			if ( form_t.odesc5 == 0 ) nform_feld.feld_typ = FT_TEXTEXT ;
			DbClass.sqlexecute ( insform_t_curs );

		}
	}
#endif
}

