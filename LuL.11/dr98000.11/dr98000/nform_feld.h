#ifndef _NFORM_FELD_DEF
#define _NFORM_FELD_DEF

struct NFORM_FELD {
   short    delstatus;
   char     form_nr[11];
   char     feld_nam[MAXNAME];
   char     tab_nam[MAXNAME];
   short    feld_typ;
   short    feld_id;
   short    sort_nr;
   char		krz_txt[25];
   short	range ;
   short    lila ;
 
};

extern struct NFORM_FELD nform_feld;

#endif