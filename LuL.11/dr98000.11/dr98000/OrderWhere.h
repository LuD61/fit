#pragma once
#include "afxwin.h"


// COrderWhere-Dialogfeld

class COrderWhere : public CPropertyPage
{
	DECLARE_DYNAMIC(COrderWhere)

public:
	COrderWhere();
	virtual ~COrderWhere();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	char ifeldnam[128] ;
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_inptext;
	CString m_vinptext;
	afx_msg BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	void putcapture(char*);
	void putkrztxt(char*);
	char * getkrztxt(char*);
	char * rkrztxt();

};
