#pragma once
#include "afxwin.h"


// CExtFeldEigenschaften-Dialogfeld

class CExtFeldEigenschaften : public CPropertyPage
{
	DECLARE_DYNAMIC(CExtFeldEigenschaften)

public:
	CExtFeldEigenschaften();
	virtual ~CExtFeldEigenschaften();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG6 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	int iretstat ;


	DECLARE_MESSAGE_MAP()
public:
	CEdit m_tab;
	CString v_ctab;

	CString stat_txt_desc1;

	CEdit m_sfeld1;
	CString v_csfeld1;
	CEdit m_wtab1;
	CString v_cwtab1;
	CEdit m_wfeld1;
	CString v_cwfeld1;

	CEdit m_sfeld2;
	CString v_csfeld2;
	CEdit m_wtab2;
	CString v_cwtab2;
	CEdit m_wfeld2;
	CString v_cwfeld2;

	CEdit m_sfeld3;
	CString v_csfeld3;
	CEdit m_wtab3;
	CString v_cwtab3;
	CEdit m_wfeld3;
	CString v_cwfeld3;

	CEdit m_sfeld4;
	CString v_csfeld4;
	CEdit m_wtab4;
	CString v_cwtab4;
	CEdit m_wfeld4;
	CString v_cwfeld4;

	CEdit m_sfeld5;
	CString v_csfeld5;
	CEdit m_wtab5;
	CString v_cwtab5;
	CEdit m_wfeld5;
	CString v_cwfeld5;

	CEdit m_ofeld1;
	CString v_cofeld1;
	CEdit m_ofeld2;
	CString v_cofeld2;
	CEdit m_ofeld3;
	CString v_cofeld3;
	CEdit m_ofeld4;
	CString v_cofeld4;


	char * rkrztxt(void);

	int  getretstat(void);
	char * getkrztxt(char *);
	char * getttab_nam(char *);

	char * getsfeld1(char *);
	char * getsfeld2(char *);
	char * getsfeld3(char *);
	char * getsfeld4(char *);
	char * getsfeld5(char *);

	char * getwtab1(char *);
	char * getwtab2(char *);
	char * getwtab3(char *);
	char * getwtab4(char *);
	char * getwtab5(char *);

	char * getwfeld1(char *);
	char * getwfeld2(char *);
	char * getwfeld3(char *);
	char * getwfeld4(char *);
	char * getwfeld5(char *);

	char * getofeld1(char *);
	char * getofeld2(char *);
	char * getofeld3(char *);
	char * getofeld4(char *);
	char * getofeld5(char *);

	short getodesc1(void) ;
	short getodesc2(void) ;
	short getodesc3(void) ;
	short getodesc4(void) ;
	short getodesc5(void) ;

	void putkrztxt( char *);
	void putttab_nam(char *);

	void putsfeld1(char *);
	void putsfeld2(char *);
	void putsfeld3(char *);
	void putsfeld4(char *);
	void putsfeld5(char *);

	void putwtab1(char *);
	void putwtab2(char *);
	void putwtab3(char *);
	void putwtab4(char *);
	void putwtab5(char *);

	void putwfeld1(char *);
	void putwfeld2(char *);
	void putwfeld3(char *);
	void putwfeld4(char *);
	void putwfeld5(char *);

	void putofeld1(char *);
	void putofeld2(char *);
	void putofeld3(char *);
	void putofeld4(char *);
	void putofeld5(char *);

	void putodesc1(short) ;
	void putodesc2(short) ;
	void putodesc3(short) ;
	void putodesc4(short) ;
	void putodesc5(short) ;

	CButton m_desc1;
	BOOL m_bdesc1;
	CButton m_desc2;
	BOOL m_bdesc2;
	CButton m_desc3;
	BOOL m_bdesc3;
	CButton m_desc4;
	BOOL m_bdesc4;
	CButton m_desc5;
	BOOL m_bdesc5;



	afx_msg void OnBnClickedCheck4();
	afx_msg void OnBnClickedCheck3();
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnBnClickedCheck5();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	CEdit m_inptxt;
	CString m_vinptxt;

};
