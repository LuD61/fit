#ifndef _FORM_OW_DEF
#define _FORM_OW_DEF

struct FORM_V {
   char     form_nr[11];
   long		zei;
   char     krz_txt[101];
   short    lila;
   long		typ;
   long		vk;
   long		nk;
   char		klar_txt[31]; 	
};

extern struct FORM_V form_v;

struct FORM_W {
   char     form_nr[11];
   long		zei;
   char     krz_txt[101];
   short    lila;
 
};

extern struct FORM_W form_w;


struct FORM_O {
   char     form_nr[11];
   long		zei;
   char     krz_txt[51];
   short lila ;
 
};

extern struct FORM_O form_o;

// 220207 : es folgen form_te und form_t

struct FORM_TE {
   char     form_nr[11];
   short    lila;
   char     tab_nam[MAXNAME];
   char     feld_nam[MAXNAME];
   long		zei;
   char     krz_txt[51];
 
};

extern struct FORM_TE form_te;

struct FORM_T {
 short		delstatus ;
 char		form_nr[11];
 short		lila;
 char		tab_nam [MAXNAME];
 char		feld_nam[MAXNAME];
 char		ttab_nam[MAXNAME] ;
 char		sfeld1[19];
 char		sfeld2[19];
 char		sfeld3[19];
 char		sfeld4[19];
 char		sfeld5[19];

 char		wtab1[19];
 char		wtab2[19];
 char		wtab3[19];
 char		wtab4[19];
 char		wtab5[19];

 char		wfeld1[19];
 char		wfeld2[19];
 char		wfeld3[19];
 char		wfeld4[19];
 char		wfeld5[19];

 char		ofeld1[19];
 char		ofeld2[19];
 char		ofeld3[19];
 char		ofeld4[19];
 char		ofeld5[19];

 short		odesc1 ;
 short		odesc2 ;
 short		odesc3 ;
 short		odesc4 ;
 short		odesc5 ;
 
};

extern struct FORM_T form_t;

#endif