#pragma once
#include "afxwin.h"


// neuFormat-Dialogfeld

class CneuFormat : public CPropertyPage
{
	DECLARE_DYNAMIC(CneuFormat)

public:
	CneuFormat();
	virtual ~CneuFormat();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	char * neuformat();
	void puttitel(char *);
	CEdit m_neuformat;
	CString v_neuformat;
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnBnClickedCheck2();
	CButton m_liste;
	BOOL m_bliste;
	CButton m_label;
	BOOL m_blabel;
};
