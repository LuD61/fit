#pragma once
#include "afxwin.h"


// CKommentar-Dialogfeld

class CKommentar : public CPropertyPage
{
	DECLARE_DYNAMIC(CKommentar)

public:
	CKommentar();
	virtual ~CKommentar();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG5 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	char * getkommentar();
	void putkommentar(char *);

	void putdistinct( short );	// 030211 
	short getdistinct( void );	// 030211

	void puttitel(char *);

	CEdit m_kommentar;
	CString v_ckommentar;
	CButton m_checkdistinct;
	BOOL b_checkdistinct;
	afx_msg void OnBnClickedCheckdistinct();
};
