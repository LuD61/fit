// ExtFeldEigenschaften.cpp : Implementierungsdatei
// 071110 :odesc5 wird zukuenftig als Umschalter Typ4/Typ5 interpretiert

#include "stdafx.h"
#include "catalog2.h"
#include "ExtFeldEigenschaften.h"


// CExtFeldEigenschaften-Dialogfeld

IMPLEMENT_DYNAMIC(CExtFeldEigenschaften, CPropertyPage)
CExtFeldEigenschaften::CExtFeldEigenschaften()
	: CPropertyPage(CExtFeldEigenschaften::IDD)

	, v_ctab(_T(""))

	, v_csfeld1(_T(""))
	, v_cwtab1(_T(""))
	, v_cwfeld1(_T(""))

	, v_csfeld2(_T(""))
	, v_cwtab2(_T(""))
	, v_cwfeld2(_T(""))

	, v_csfeld3(_T(""))
	, v_cwtab3(_T(""))
	, v_cwfeld3(_T(""))

	, v_csfeld4(_T(""))
	, v_cwtab4(_T(""))
	, v_cwfeld4(_T(""))

	, v_csfeld5(_T(""))
	, v_cwtab5(_T(""))
	, v_cwfeld5(_T(""))

	, v_cofeld1(_T(""))
	, v_cofeld2(_T(""))
	, v_cofeld3(_T(""))
	, v_cofeld4(_T(""))

	, m_bdesc1(FALSE)
	, m_bdesc2(FALSE)
	, m_bdesc3(FALSE)
	, m_bdesc4(FALSE)
	, m_bdesc5(FALSE)
	, m_vinptxt(_T(""))
{
}

CExtFeldEigenschaften::~CExtFeldEigenschaften()
{
}

void CExtFeldEigenschaften::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_EDIT22, m_tab);
	DDX_Text(pDX, IDC_EDIT22, v_ctab);
	DDV_MaxChars(pDX, v_ctab, 18);
	// 1
	DDX_Control(pDX, IDC_EDIT3, m_sfeld1);
	DDX_Text(pDX, IDC_EDIT3, v_csfeld1);
	DDV_MaxChars(pDX, v_csfeld1, 18);

	DDX_Control(pDX, IDC_EDIT8, m_wtab1);
	DDX_Text(pDX, IDC_EDIT8, v_cwtab1);
	DDV_MaxChars(pDX, v_cwtab1, 18);

	DDX_Control(pDX, IDC_EDIT13, m_wfeld1);
	DDX_Text(pDX, IDC_EDIT13, v_cwfeld1);
	DDV_MaxChars(pDX, v_cwfeld1, 18);
	// 2
	DDX_Control(pDX, IDC_EDIT4, m_sfeld2);
	DDX_Text(pDX, IDC_EDIT4, v_csfeld2);
	DDV_MaxChars(pDX, v_csfeld2, 18);

	DDX_Control(pDX, IDC_EDIT9, m_wtab2);
	DDX_Text(pDX, IDC_EDIT9, v_cwtab2);
	DDV_MaxChars(pDX, v_cwtab2, 18);

	DDX_Control(pDX, IDC_EDIT14, m_wfeld2);
	DDX_Text(pDX, IDC_EDIT14, v_cwfeld2);
	DDV_MaxChars(pDX, v_cwfeld2, 18);
	// 3
	DDX_Control(pDX, IDC_EDIT5, m_sfeld3);
	DDX_Text(pDX, IDC_EDIT5, v_csfeld3);
	DDV_MaxChars(pDX, v_csfeld3, 18);

	DDX_Control(pDX, IDC_EDIT10, m_wtab3);
	DDX_Text(pDX, IDC_EDIT10, v_cwtab3);
	DDV_MaxChars(pDX, v_cwtab3, 18);

	DDX_Control(pDX, IDC_EDIT15, m_wfeld3);
	DDX_Text(pDX, IDC_EDIT15, v_cwfeld3);
	DDV_MaxChars(pDX, v_cwfeld3, 18);
	// 4
	DDX_Control(pDX, IDC_EDIT6, m_sfeld4);
	DDX_Text(pDX, IDC_EDIT6, v_csfeld4);
	DDV_MaxChars(pDX, v_csfeld4, 18);

	DDX_Control(pDX, IDC_EDIT11, m_wtab4);
	DDX_Text(pDX, IDC_EDIT11, v_cwtab4);
	DDV_MaxChars(pDX, v_cwtab4, 18);

	DDX_Control(pDX, IDC_EDIT16, m_wfeld4);
	DDX_Text(pDX, IDC_EDIT16, v_cwfeld4);
	DDV_MaxChars(pDX, v_cwfeld4, 18);
	// 5
	DDX_Control(pDX, IDC_EDIT7, m_sfeld5);
	DDX_Text(pDX, IDC_EDIT7, v_csfeld5);
	DDV_MaxChars(pDX, v_csfeld5, 18);

	DDX_Control(pDX, IDC_EDIT12, m_wtab5);
	DDX_Text(pDX, IDC_EDIT12, v_cwtab5);
	DDV_MaxChars(pDX, v_cwtab5, 18);

	DDX_Control(pDX, IDC_EDIT17, m_wfeld5);
	DDX_Text(pDX, IDC_EDIT17, v_cwfeld5);
	DDV_MaxChars(pDX, v_cwfeld5, 18);

	// orders
	DDX_Control(pDX, IDC_EDIT18, m_ofeld1);
	DDX_Text(pDX, IDC_EDIT18, v_cofeld1);
	DDV_MaxChars(pDX, v_cofeld1, 18);

	DDX_Control(pDX, IDC_EDIT19, m_ofeld2);
	DDX_Text(pDX, IDC_EDIT19, v_cofeld2);
	DDV_MaxChars(pDX, v_cofeld2, 18);

	DDX_Control(pDX, IDC_EDIT20, m_ofeld3);
	DDX_Text(pDX, IDC_EDIT20, v_cofeld3);
	DDV_MaxChars(pDX, v_cofeld3, 18);

	DDX_Control(pDX, IDC_EDIT21, m_ofeld4);
	DDX_Text(pDX, IDC_EDIT21, v_cofeld4);
	DDV_MaxChars(pDX, v_cofeld4, 18);

	DDX_Control(pDX, IDC_CHECK1, m_desc1);
	DDX_Check(pDX, IDC_CHECK1, m_bdesc1);
	DDX_Control(pDX, IDC_CHECK2, m_desc2);
	DDX_Check(pDX, IDC_CHECK2, m_bdesc2);
	DDX_Control(pDX, IDC_CHECK3, m_desc3);
	DDX_Check(pDX, IDC_CHECK3, m_bdesc3);
	DDX_Control(pDX, IDC_CHECK4, m_desc4);
	DDX_Check(pDX, IDC_CHECK4, m_bdesc4);
	DDX_Control(pDX, IDC_EDIT23, m_inptxt);
	DDX_Text(pDX, IDC_EDIT23, m_vinptxt);
	DDV_MaxChars(pDX, m_vinptxt, 2050);
	DDX_Control(pDX, IDC_CHECK5, m_desc5);
	DDX_Check(pDX, IDC_CHECK5, m_bdesc5);
}

BEGIN_MESSAGE_MAP(CExtFeldEigenschaften, CPropertyPage)
	ON_BN_CLICKED(IDC_CHECK4, OnBnClickedCheck4)
	ON_BN_CLICKED(IDC_CHECK3, OnBnClickedCheck3)
	ON_BN_CLICKED(IDC_CHECK2, OnBnClickedCheck2)
	ON_BN_CLICKED(IDC_CHECK1, OnBnClickedCheck1)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK5, &CExtFeldEigenschaften::OnBnClickedCheck5)
END_MESSAGE_MAP()

// CExtFeldEigenschaften-Meldungshandler

void CExtFeldEigenschaften::OnBnClickedCheck1()
{
	if ( m_bdesc1 )
		m_bdesc1 = FALSE ;
	else
		m_bdesc1 = TRUE ;

	UpdateData(TRUE);
	UpdateData(FALSE);
}

void CExtFeldEigenschaften::OnBnClickedCheck2()
{
	if ( m_bdesc2 )
		m_bdesc2 = FALSE ;
	else
		m_bdesc2 = TRUE ;

	UpdateData(TRUE);
	UpdateData(FALSE);
}

void CExtFeldEigenschaften::OnBnClickedCheck3()
{
	if ( m_bdesc3 )
		m_bdesc3 = FALSE ;
	else
		m_bdesc3 = TRUE ;

	UpdateData(TRUE);
	UpdateData(FALSE);
}

void CExtFeldEigenschaften::OnBnClickedCheck4()
{
	if ( m_bdesc4 )
		m_bdesc4 = FALSE ;
	else
		m_bdesc4 = TRUE ;

	UpdateData(TRUE);
	UpdateData(FALSE);
}

void CExtFeldEigenschaften::OnBnClickedCheck5()
{
		if ( m_bdesc5 )
		m_bdesc5 = FALSE ;
	else
		m_bdesc5 = TRUE ;

	UpdateData(TRUE);
	UpdateData(FALSE);
}

void CExtFeldEigenschaften::OnBnClickedOk()
{
	iretstat = 1 ;
	CDialog::OnOK();
}

void CExtFeldEigenschaften::OnBnClickedCancel()
{
	iretstat = 0 ;
	CDialog::OnCancel();
}

int    CExtFeldEigenschaften::getretstat(void)
{
	return iretstat;
}

static char savekrztxt[2058] ;

char * CExtFeldEigenschaften::rkrztxt(void)
{
	char *S= m_vinptxt.GetBuffer(2058) ; 
	return  S ;
}

char * CExtFeldEigenschaften::getkrztxt(char * ekrztxt)
{
	sprintf ( ekrztxt, "%s", rkrztxt() );
	return ekrztxt ;
}

void CExtFeldEigenschaften::putkrztxt(char * ekrztxt)
{	sprintf ( savekrztxt,"%s", ekrztxt );
	 m_vinptxt = _T( ekrztxt) ;
}

char * CExtFeldEigenschaften::getttab_nam(char * etab_nam)
{
	char *S= v_ctab.GetBuffer(30) ; 
	sprintf ( etab_nam, "%s", S );
	return etab_nam ;
}
char * CExtFeldEigenschaften::getsfeld1(char * esfeld1 )
{
	char *S= v_csfeld1.GetBuffer(30) ; 
	sprintf ( esfeld1, "%s", S );
	return esfeld1 ;
}
char * CExtFeldEigenschaften::getsfeld2(char * esfeld2 )
{
	char *S= v_csfeld2.GetBuffer(30) ; 
	sprintf ( esfeld2, "%s", S );
	return esfeld2 ;
}
char * CExtFeldEigenschaften::getsfeld3(char * esfeld3 )
{
	char *S= v_csfeld3.GetBuffer(30) ; 
	sprintf ( esfeld3, "%s", S );
	return esfeld3 ;

}
char * CExtFeldEigenschaften::getsfeld4(char * esfeld4 )
{
	char *S= v_csfeld4.GetBuffer(30) ; 
	sprintf ( esfeld4, "%s", S );
	return esfeld4 ;
}
char * CExtFeldEigenschaften::getsfeld5(char * esfeld5 )
{
	char *S= v_csfeld5.GetBuffer(30) ; 
	sprintf ( esfeld5, "%s", S );
	return esfeld5 ;
}

char * CExtFeldEigenschaften::getwtab1(char * ewtab1 )
{
	char *S= v_cwtab1.GetBuffer(30) ; 
	sprintf ( ewtab1, "%s", S );
	return ewtab1 ;
}
char * CExtFeldEigenschaften::getwtab2(char * ewtab2 )
{
	char *S= v_cwtab2.GetBuffer(30) ; 
	sprintf ( ewtab2, "%s", S );
	return ewtab2 ;
}
char * CExtFeldEigenschaften::getwtab3(char * ewtab3 )
{
	char *S= v_cwtab3.GetBuffer(30) ; 
	sprintf ( ewtab3, "%s", S );
	return ewtab3 ;
}
char * CExtFeldEigenschaften::getwtab4(char * ewtab4 )
{
	char *S= v_cwtab4.GetBuffer(30) ; 
	sprintf ( ewtab4, "%s", S );
	return ewtab4 ;
}
char * CExtFeldEigenschaften::getwtab5(char * ewtab5 )
{
	char *S= v_cwtab5.GetBuffer(30) ; 
	sprintf ( ewtab5, "%s", S );
	return ewtab5 ;
}

char * CExtFeldEigenschaften::getwfeld1(char * ewfeld1 )
{
	char *S= v_cwfeld1.GetBuffer(30) ; 
	sprintf ( ewfeld1, "%s", S );
	return ewfeld1 ;
}
char * CExtFeldEigenschaften::getwfeld2(char * ewfeld2 )
{
	char *S= v_cwfeld2.GetBuffer(30) ; 
	sprintf ( ewfeld2, "%s", S );
	return ewfeld2 ;
}
char * CExtFeldEigenschaften::getwfeld3(char * ewfeld3 )
{
	char *S= v_cwfeld3.GetBuffer(30) ; 
	sprintf ( ewfeld3, "%s", S );
	return ewfeld3 ;
}
char * CExtFeldEigenschaften::getwfeld4(char * ewfeld4 )
{
	char *S= v_cwfeld4.GetBuffer(30) ; 
	sprintf ( ewfeld4, "%s", S );
	return ewfeld4 ;
}
char * CExtFeldEigenschaften::getwfeld5(char * ewfeld5 )
{
	char *S= v_cwfeld5.GetBuffer(30) ; 
	sprintf ( ewfeld5, "%s", S );
	return ewfeld5 ;
}

char * CExtFeldEigenschaften::getofeld1(char * eofeld1 )
{
	char *S= v_cofeld1.GetBuffer(30) ; 
	sprintf ( eofeld1, "%s", S );
	return eofeld1 ;
}
char * CExtFeldEigenschaften::getofeld2(char * eofeld2 )
{
	char *S= v_cofeld2.GetBuffer(30) ; 
	sprintf ( eofeld2, "%s", S );
	return eofeld2 ;
}
char * CExtFeldEigenschaften::getofeld3(char * eofeld3 )
{
	char *S= v_cofeld3.GetBuffer(30) ; 
	sprintf ( eofeld3, "%s", S );
	return eofeld3 ;
}
char * CExtFeldEigenschaften::getofeld4(char * eofeld4 )
{
	char *S= v_cofeld4.GetBuffer(30) ; 
	sprintf ( eofeld4, "%s", S );
	return eofeld4 ;
}
char * CExtFeldEigenschaften::getofeld5(char * eofeld5 )
{
//	char *S= v_cofeld5.GetBuffer(30) ; 
//	sprintf ( eofeld5, "%s", S );
	return eofeld5 ;
}

short CExtFeldEigenschaften::getodesc1(void)
{
	if ( m_bdesc1 ) return 1 ; else return 0 ;
}
short CExtFeldEigenschaften::getodesc2(void)
{
	if ( m_bdesc2 ) return 1 ; else return 0 ;
}
short CExtFeldEigenschaften::getodesc3(void)
{
	if ( m_bdesc3 ) return 1 ; else return 0 ;
}
short CExtFeldEigenschaften::getodesc4(void)
{
	if ( m_bdesc4 ) return 1 ; else return 0 ;
}
// 071110 : mit Leben erfuellt
short CExtFeldEigenschaften::getodesc5(void)
{
	if ( m_bdesc5 ) return 1 ; else return 0 ;
}

void CExtFeldEigenschaften::putttab_nam(char * etab_nam)
{
	 v_ctab = _T( etab_nam) ;
}
void CExtFeldEigenschaften::putsfeld1(char * esfeld1 )
{
	 v_csfeld1 = _T( esfeld1) ;
}
void CExtFeldEigenschaften::putsfeld2(char * esfeld2 )
{
	 v_csfeld2 = _T( esfeld2) ;
}
void CExtFeldEigenschaften::putsfeld3(char * esfeld3 )
{
	 v_csfeld3 = _T( esfeld3) ;
}
void CExtFeldEigenschaften::putsfeld4(char * esfeld4 )
{
		 v_csfeld4 = _T( esfeld4) ;
}
void CExtFeldEigenschaften::putsfeld5(char * esfeld5 )
{
	 v_csfeld5 = _T( esfeld5) ;
}

void CExtFeldEigenschaften::putwtab1(char * ewtab1 )
{
	 v_cwtab1 = _T( ewtab1) ;
}
void CExtFeldEigenschaften::putwtab2(char * ewtab2 )
{
	 v_cwtab2 = _T( ewtab2) ;
}
void CExtFeldEigenschaften::putwtab3(char * ewtab3 )
{
	 v_cwtab3 = _T( ewtab3) ;
}
void CExtFeldEigenschaften::putwtab4(char * ewtab4 )
{
	 v_cwtab4 = _T( ewtab4) ;
}
void CExtFeldEigenschaften::putwtab5(char * ewtab5 )
{
	 v_cwtab5= _T( ewtab5) ;
}

void CExtFeldEigenschaften::putwfeld1(char * ewfeld1 )
{
	 v_cwfeld1 = _T( ewfeld1) ;
}
void CExtFeldEigenschaften::putwfeld2(char * ewfeld2 )
{
	 v_cwfeld2 = _T( ewfeld2) ;
}
void CExtFeldEigenschaften::putwfeld3(char * ewfeld3 )
{
	 v_cwfeld3 = _T( ewfeld3) ;
}
void CExtFeldEigenschaften::putwfeld4(char * ewfeld4 )
{
	 v_cwfeld4 = _T( ewfeld4) ;
}
void CExtFeldEigenschaften::putwfeld5(char * ewfeld5 )
{
	 v_cwfeld5 = _T( ewfeld5) ;
}

void CExtFeldEigenschaften::putofeld1(char * eofeld1 )
{
	 v_cofeld1 = _T( eofeld1) ;
}
void CExtFeldEigenschaften::putofeld2(char * eofeld2 )
{
	 v_cofeld2 = _T( eofeld2) ;
}
void CExtFeldEigenschaften::putofeld3(char * eofeld3 )
{
	 v_cofeld3 = _T( eofeld3) ;
}
void CExtFeldEigenschaften::putofeld4(char * eofeld4 )
{
	 v_cofeld4 = _T( eofeld4) ;
}
void CExtFeldEigenschaften::putofeld5(char * eofeld5 )
{
// z.Z noch nicht realisiert : 	 v_cofeld5 = _T( eofeld5) ;
}

void CExtFeldEigenschaften::putodesc1(short eodesc1 )
{
	if ( eodesc1 ) m_bdesc1 = TRUE ; else m_bdesc1 = FALSE ;
}
void CExtFeldEigenschaften::putodesc2(short eodesc2 )
{
	if ( eodesc2 ) m_bdesc2 = TRUE ; else m_bdesc2 = FALSE ;
}
void CExtFeldEigenschaften::putodesc3(short eodesc3 )
{
	if ( eodesc3 ) m_bdesc3 = TRUE ; else m_bdesc3 = FALSE ;
}
void CExtFeldEigenschaften::putodesc4(short eodesc4 )
{
	if ( eodesc4 ) m_bdesc4 = TRUE ; else m_bdesc4 = FALSE ;
}
void CExtFeldEigenschaften::putodesc5(short eodesc5 )
{	// 071110 mit Leben erfuellt
	if ( eodesc5 ) m_bdesc5 = TRUE ; else m_bdesc5 = FALSE ;
}

