//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by catalog2.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_CATALOTYPE                  129
#define IDD_COLUMNPAGE                  130
#define ID_FILE_NEW131                  131
#define ID_Menu                         132
#define IDD_TABLEPAGE                   133
#define ID_FILE_NEW133                  133
#define IDD_DIALOG1                     136
#define IDD_DIALOG2                     139
#define ID_LUL_LISTENDESIGN             140
#define ID_LUL_WHERE                    141
#define ID_LUL_ORDER                    142
#define IDD_DIALOG3                     143
#define ID_FILE_DELETE                  144
#define IDD_DIALOG4                     145
#define IDD_DIALOG5                     146
#define IDD_DIALOG6                     147
#define ID_LUL_VIRTUELL                 148
#define IDD_DIALVIRTUELL                150
#define IDC_SYSTABLES                   1000
#define IDC_ALIAI                       1001
#define IDC_VIEWS                       1002
#define IDC_LENGTH                      1003
#define IDC_PRECISION                   1004
#define IDC_NULLABILITY                 1005
#define IDC_EDIT1                       1008
#define IDC_CHECK1                      1015
#define IDC_CHECK2                      1016
#define IDC_CHECK3                      1017
#define IDC_CHECK4                      1018
#define IDC_CHECK5                      1019
#define IDC_CHECK6                      1020
#define IDC_EDIT2                       1021
#define IDC_EDIT4                       1022
#define IDC_EDIT3                       1023
#define IDC_EDIT5                       1025
#define IDC_EDIT6                       1026
#define IDC_EDIT7                       1027
#define IDC_EDIT8                       1028
#define IDC_EDIT9                       1029
#define IDC_EDIT10                      1030
#define IDC_EDIT11                      1031
#define IDC_EDIT12                      1032
#define IDC_EDIT13                      1033
#define IDC_EDIT14                      1034
#define IDC_EDIT15                      1035
#define IDC_EDIT16                      1036
#define IDC_EDIT17                      1037
#define IDC_EDIT18                      1038
#define IDC_EDIT19                      1039
#define IDC_EDIT20                      1042
#define IDC_EDIT21                      1043
#define IDC_EDIT22                      1044
#define IDC_EDIT23                      1045
#define IDC_CHECKDISTINCT               1048
#define IDC_SMALLINT                    1049
#define IDC_INTEGER                     1050
#define IDC_DECIMAL                     1051
#define IDC_DATE                        1052
#define IDC_CHARA                       1053
#define IDC_CHARLANG                    1054
#define IDC_DECVK                       1055
#define IDC_DECNK                       1056
#define IDC_BDOWN                       1057
#define IDC_BUP                         1058
#define IDC_BPLUS                       1059
#define IDC_BUTTON2                     1060
#define IDC_BMINUS                      1060
#define IDC_ZEI                         1061
#define ID_VIEW_TABLES                  32771
#define ID_VIEW_COLUMNINFO              32772
#define ID_VIEW_TABLELEVEL              32773
#define ID_VIEW_SETTINGS                32774
#define ID_VIEW_FORM_NR                 32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        151
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1062
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
