// MainFrm.cpp : implementation of the CMainFrame class
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include "stdafx.h"
#include "catalog2.h"

#ifdef LL19
#include "cmbtll19.h"		// 011014
#else
#include "cmbtll11.h"		// 071205
#endif

#include "token.h"
#include "MainFrm.h"

#include "PropertieTable.h"	// 230610

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern short GlobalLila ;

int TEST = 0 ;	// 290610

extern char * uPPER (char *) ;
extern char *clipped (char *string) ;
extern char myform_nr[11] ;

int  odbdabatyp = FITINFORMIX ;	// 0 = INFORMIX
								// 1 = MYSQL
								// 2 = ORACLE
char odbbasisverz[256] = "" ;
char odb_daba [256] = "" ;
char odb_user [256] = "" ;
char odb_pass [256] = "" ;
char odb_isomode[26] = "1" ;	// kann was gesteuert werden 
 								// z.Z. ( 01.04.09 )
								// 1 == setze dirty read 
								// 0 == kein dirty read setzen ( mysql,oracle ..... )


/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
};

// A 230610 A
void LoadDbIni ()
{

	CString St ;
	PropExtension::CProperties DbIni;  
	char *etc = getenv ("BWSETC");
	if (etc != NULL)
	{
if ( TEST )
		sprintf(etc, "e:\\user\\mios\\etc") ;	// 290610

		DbIni.FileName.Format ("%s\\Database.ini", etc);
	}
	else
	{
		DbIni.FileName = "Database.ini";;
	}
	DbIni.SectionName = "Database";
	DbIni.Load ();
// 230610 
	CString tMode = DbIni.GetValue (CString ("MODE"));
	if (tMode != "MODE")
	{
		St = tMode.GetBuffer() ;
		St.MakeUpper ();
		if (St.Find ("ORACLE") > -1)
		{
			odbdabatyp = FITORACLE ;
		}

		if (St.Find ("INFORMIX") > -1)
		{
			odbdabatyp = FITINFORMIX ;
		}

		if (St.Find ("MYSQL") > -1)
		{
			odbdabatyp = FITMYSQL ;
		}
	}

	CString tName = DbIni.GetValue (CString ("NAME"));
	if (tName != "NAME")
	{
// 		DbName = tName;
		 St = tName.GetBuffer() ;
		 sprintf ( odb_daba ,"%s" ,St) ;
	}
	CString tUser = DbIni.GetValue (CString ("USER"));
	if (tUser != "USER")
	{
//		User = tUser;
		 St = tUser.GetBuffer() ;
		 sprintf ( odb_user ,"%s" ,St) ;
	}
	CString tPassw = DbIni.GetValue (CString ("PASSW"));
	if (tPassw != "PASSW")
	{
//		Passw = tPassw;
		St = tPassw.GetBuffer() ;
		sprintf ( odb_pass ,"%s" ,St) ;
	}
}



/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here

	GlobalLila = 0 ;	// Start mit Liste

	char * etc ;
	odbdabatyp = FITINFORMIX ;	// default : informix z.B. fuer standard-fit

	LoadDbIni () ;	// 230610 : evtl. auch vorher noch den gesamten DABATYP �berladen 


#ifdef FITODBC
#ifdef FITSMART
//	ich subsummiere fitodbc hier mal immer mit .....

	odbdabatyp = FITMYSQL ;	// MYSQL 
	etc = getenv ( "FITROOT" ) ;
	if ( etc != NULL )
		sprintf ( odbbasisverz, "%s", etc ) ;
	sprintf ( odb_daba ,"fsm" ) ;
//	char odb_user [256] = "" ;
//	char odb_pass [256] = "" ;
	printf ( odb_isomode, "0" ) ;	// kein dirty-read 
#else
//	alle ODBC-Ablaeufe ausser fitsmart bws mit odbc oder oracle oder , oder ....

	InterpretCommandLine() ;	// Achtung : setzt z.Z. ueber den dabatyp auch details

	if ( odbdabatyp == FITINFORMIX )	// nix oder INFORMIX -> daher vermute ich standard-Fit
	{
		etc = getenv ( "BWS" ) ;
		if ( etc != NULL )
			sprintf ( odbbasisverz, "%s", etc ) ;
		sprintf ( odb_daba ,"bws" ) ;
//		char odb_user [256] = "" ;
//		char odb_pass [256] = "" ;
		sprintf ( odb_isomode, "1" ) ;	// dirty-read aktiv
	}

	if ( odbdabatyp == FITORACLE )	// ORACLE -> daher vermute ich bg-Projekt
	{
		// 230610 : von bwso -> bws gewandelt und Database.ini auswerten
		etc = getenv ( "BWS" ) ;
		if ( TEST )	// 290610
		sprintf ( etc, "e:\\user\\mios" ) ;

		if ( etc != NULL )
			sprintf ( odbbasisverz, "%s", etc ) ;
		sprintf ( odb_daba ,"bws" ) ;
		char odb_user [256] = "SYSTEM" ;
		char odb_pass [256] = "t0pfit" ;
		sprintf ( odb_isomode, "0" ) ;	// kein dirty-read
		LoadDbIni () ;	// 230610 : evtl. auch vorher noch den gesamten DABATYP �berladen 

	}
#endif
#else
// Standard-bws mit systables oder auch fuer olabel 
	etc = getenv ( "bws" ) ;
	if ( etc != NULL )
		sprintf ( odbbasisverz, "%s", etc ) ;
	sprintf ( odb_daba ,"bws" ) ;
	// odb_user ;
	// odb_pass ;
	sprintf ( odb_isomode, "1" );	// informix : dirty read aktiv 
#endif

#ifdef OLABEL
	GlobalLila = 1 ;	// Fix nur Labels erlaubt
	InterpretCommandLine() ;
#endif

   }
CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.Create(this) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	// TODO: Remove this if you don't want tool tips
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFrameWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
#ifdef OLABEL
// Die Commandline hat (020409) folgendes fixes Format wegen der fixen Funktion
// dr98000o.exe -name ETIKETTNAME
// LABEL=1  wird fix belegt
//=============================================================================
void CMainFrame::InterpretCommandLine()
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");
	char *px;
	char p[299] ;
	GlobalLila = 1 ;

	Token.NextToken ();
	while ((px = Token.NextToken ()) != NULL)
	{
		sprintf ( p ,"%s", uPPER(px) );

		if (strcmp (p, "-NAME") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
  			sprintf ( myform_nr ,"%s", clipped ( uPPER (px)));
		}
	}
}
#endif
#ifndef OLABEL
// Die Commandline kann (020409) als Parameter den DABA-Typ mitbekommen 
// dr98000o.exe [OCRACLE | INFORMIX | MYSQL]
// z.Z. nur ORACLE,INFORMIX,MYSQL ausgewertet,
// default : INFORMIX ; falls FITSMART aktiv : default "MYSQL"
// 
//=============================================================================
void CMainFrame::InterpretCommandLine()
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");

	char *px;
	char p[299] ;

	Token.NextToken ();
	while ((px = Token.NextToken ()) != NULL)
	{
		sprintf ( p ,"%s", uPPER(px) );

		if (strcmp (p, "INFORMIX" ) == 0)
		{
			odbdabatyp = FITINFORMIX ;
			break ;
		}
		if (strcmp (p, "MYSQL" ) == 0)
		{
			odbdabatyp = FITMYSQL ;
			break ;
		}

		if (strcmp (p, "ORACLE" ) == 0)
		{
			odbdabatyp = FITORACLE ;
			break ;
		}

	}
}
#endif	// ifndef-OLABEL

