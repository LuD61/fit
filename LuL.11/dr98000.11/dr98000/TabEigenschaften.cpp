// TabEigenschaften.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "catalog2.h"
#include "TabEigenschaften.h"


// TabEigenschaften-Dialogfeld

IMPLEMENT_DYNAMIC(TabEigenschaften, CPropertyPage)
TabEigenschaften::TabEigenschaften()
	: CPropertyPage(TabEigenschaften::IDD)
	, v_outer(FALSE)
	, v_dele(FALSE)
{
}

TabEigenschaften::~TabEigenschaften()
{
}

void TabEigenschaften::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK1, m_outer);
	DDX_Check(pDX, IDC_CHECK1, v_outer);
	DDX_Control(pDX, IDC_CHECK2, m_dele);
	DDX_Check(pDX, IDC_CHECK2, v_dele);
}


BEGIN_MESSAGE_MAP(TabEigenschaften, CPropertyPage)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
END_MESSAGE_MAP()


void TabEigenschaften::put_aktion(int eput_aktion)
{

	iput_aktion = eput_aktion ;
	if ( ! eput_aktion )	// Standard-Tabelle
	{
		v_outer = FALSE ;
	}
	else
	{ 
		v_outer = TRUE ;
	}
}

int TabEigenschaften::get_aktion(void)
{
	int ausgabe = 0 ;
	if ( v_outer == TRUE && ! iput_aktion ) ausgabe = 2 ;	// Setze outer
	if ( v_outer == FALSE && iput_aktion )  ausgabe = 3 ;	// Setze outer zurueck
	if ( v_dele  == TRUE )	   		        ausgabe = 1 ;	// bitte loeschen
	return ausgabe ;
}


// TabEigenschaften-Meldungshandler

void TabEigenschaften::OnBnClickedOk()	// eingaben gelten
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CDialog::OnOK();

}

void TabEigenschaften::OnBnClickedCancel()	// verwerfen
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	v_outer = iput_aktion ;
	v_dele  = FALSE ;
	CDialog::OnCancel();

}
