#ifndef _NFORM_TAB_DEF
#define _NFORM_TAB_DEF

struct NFORM_TAB {
   short    delstatus;
   char     form_nr[11];

   char     tab_nam[MAXNAME];
   short    lila ;
}
 ;

extern struct NFORM_TAB nform_tab;


struct NFORM_HEA {
   short    delstatus;
   char     form_nr[11];
   short    lila ;
   char		form_ube[129] ;
}
 ;

extern struct NFORM_HEA nform_hea;


#define iDBCHAR 0
#define iDBSMALLINT 1
#define iDBINTEGER  2
#define iDBDECIMAL  5
#define iDBDATUM    7
#define iDBTIMESTAMP   12
#define iDBVARCHAR	   13		// 280612 aus syscolumns heraus
#define iSQL_VARCHAR   12		// 290612 aus informix-odbc heraus	
#define iDBSERIAL		262		// 280612

#endif