#pragma once
#include "afxwin.h"

// DIALVIRTUELL-Dialogfeld

class DIALVIRTUELL : public CDialog
{
	DECLARE_DYNAMIC(DIALVIRTUELL)

public:
	DIALVIRTUELL(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~DIALVIRTUELL();

// Dialogfelddaten
	enum { IDD = IDD_DIALVIRTUELL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnInitDialog();
	afx_msg void lademaske(int iakt_zei);
	afx_msg int  plautest( int imodus);
	afx_msg void ladearray( int iakt_zei);

	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnEnKillfocusEdit1();
	afx_msg void OnEnKillfocusEdit2();
	afx_msg void OnBnClickedSmallint();
	afx_msg void OnBnClickedInteger();
	afx_msg void OnBnClickedDate();
	afx_msg void OnBnClickedDecimal();
	afx_msg void OnBnClickedChara();
	afx_msg void OnEnKillfocusDecvk();
	afx_msg void OnEnKillfocusDecnk();
	afx_msg void OnEnKillfocusCharlang();
	afx_msg void OnBnClickedBdown();
	afx_msg void OnBnClickedBup();

	afx_msg void getListe( char *);
	afx_msg void getLila( short);

	afx_msg void OnBnClickedBplus();
	afx_msg void OnBnClickedBminus();
	CEdit m_edit1feldname;
	CString v_edit1feldname;
	CEdit m_edit2feldwert;
	CString v_edit2feldwert;
	CEdit m_editvk;
	CString v_editvk;
	CEdit m_editnk;
	CString v_editnk;
	CEdit m_charlang;
	CString v_charlang;
	CButton m_smallint;
	BOOL v_smallint;
	CButton m_integer;
	BOOL v_integer;
	CButton m_date;
	BOOL v_date;
	CButton m_decimal;
	CButton m_chara;
	BOOL v_chara;
	BOOL v_decimal;
	CEdit m_zei;
	CString v_zei;
};
