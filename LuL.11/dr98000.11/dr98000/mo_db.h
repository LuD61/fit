extern int formtab_curs;
extern int formfeld_curs;
extern int form_nr_curs;

extern int form_h_curs;		// 301104
extern int form_h_d_curs;	// 301104
extern int form_h_i_curs;	// 301104
extern int form_h_u_curs;	// 301104

extern int instab_curs;
extern int updtab_curs;
extern int deltab_curs;

extern int insfeld_curs;
extern int updfeld_curs;
extern int delfeld_curs;

extern int tabellen_curs;
extern int tabid_curs ;
extern int colid_curs ;

extern int form_o_curs;
extern int form_w_curs ;
extern int form_v_curs ;
extern int insform_o_curs ;
extern int insform_w_curs ;
extern int insform_v_curs ;
extern int delform_o_curs ;
extern int delform_w_curs ;
extern int delform_v_curs ;

extern int form_t_curs;	// 220207
extern int form_te_curs ;
extern int insform_t_curs ;
extern int insform_te_curs ;
extern int delform_t_curs ;
extern int delform_te_curs ;

extern DB_CLASS DbClass;
