// Kommentar.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "catalog2.h"
#include "Kommentar.h"


// CKommentar-Dialogfeld

IMPLEMENT_DYNAMIC(CKommentar, CPropertyPage)
CKommentar::CKommentar()
	: CPropertyPage(CKommentar::IDD)
	, v_ckommentar(_T(""))
	, b_checkdistinct(FALSE)
{
}

CKommentar::~CKommentar()
{
}

void CKommentar::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_kommentar);
	DDX_Text(pDX, IDC_EDIT1, v_ckommentar);
	DDX_Control(pDX, IDC_CHECKDISTINCT, m_checkdistinct);
	DDX_Check(pDX, IDC_CHECKDISTINCT, b_checkdistinct);
}


BEGIN_MESSAGE_MAP(CKommentar, CPropertyPage)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECKDISTINCT, OnBnClickedCheckdistinct)
END_MESSAGE_MAP()


// CKommentar-Meldungshandler

static char xkommentar[156] ;
static char xtitel[56] ;

void CKommentar::putkommentar(char * ekommentar)
{
	sprintf ( xkommentar,"%s", ekommentar );
}

void CKommentar::putdistinct(short booldist )
{	// 030211
	if ( booldist  )
		b_checkdistinct = TRUE ;
	else
		b_checkdistinct = FALSE ;
}


void CKommentar::puttitel(char * etitel)
{
	sprintf ( xtitel,"%s", etitel );
}

char * CKommentar::getkommentar()
{

	char *S= v_ckommentar.GetBuffer(512) ; 

	return  S ;
}

short CKommentar::getdistinct( void )
{	// 030211
		if ( b_checkdistinct )
			return 1 ;
	else
		return 0  ;
}


BOOL CKommentar::OnInitDialog()
{

	SetWindowText(xtitel);

	v_ckommentar = xkommentar ;
	UpdateData(FALSE) ;
	return TRUE ;
}

void CKommentar::OnBnClickedOk()
{
	CDialog::OnOK();
}

void CKommentar::OnBnClickedCancel()
{
	CDialog::OnCancel();
}

void CKommentar::OnBnClickedCheckdistinct()
{

	if ( b_checkdistinct )
		b_checkdistinct = FALSE ;
	else
		b_checkdistinct = TRUE ;

	UpdateData(TRUE);
	UpdateData(FALSE);


	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}
