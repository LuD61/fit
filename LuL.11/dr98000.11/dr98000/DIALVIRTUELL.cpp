// DIALVIRTUELL.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "catalog2.h"
#include "DIALVIRTUELL.h"
#include "DbClass.h"
#include "form_ow.h"
#include "mo_db.h"
#include "nform_tab.h"	// wegen datentypen

struct FORM_V * form_v_a ;

char vmyform_nr[30];
short vlila;
int vanz_zei;
int vakt_zei;

// DIALVIRTUELL-Dialogfeld

IMPLEMENT_DYNAMIC(DIALVIRTUELL, CDialog)

DIALVIRTUELL::DIALVIRTUELL(CWnd* pParent /*=NULL*/)
	: CDialog(DIALVIRTUELL::IDD, pParent)
	, v_edit1feldname(_T(""))
	, v_edit2feldwert(_T(""))
	, v_editvk(_T(""))
	, v_editnk(_T(""))
	, v_charlang(_T(""))
	, v_smallint(FALSE)
	, v_integer(FALSE)
	, v_date(FALSE)
	, v_chara(FALSE)
	, v_decimal(FALSE)
	, v_zei(_T(""))
{

}

void DIALVIRTUELL::lademaske( int ivakt_zei )
{


	if ( ivakt_zei < 1 )
	{
		v_edit1feldname=_T("");
		v_edit2feldwert=_T("");
		v_editvk=_T("");
		v_editnk=_T("");
		v_charlang=_T("");
		v_smallint=FALSE;
		v_integer=FALSE;
		v_date=FALSE;
		v_chara=FALSE;
		v_decimal=FALSE;
		if ( ivakt_zei < 1 )
			v_zei =_T("-1");
		else
			v_zei.Format("%d", ivakt_zei);
		return;
	}
	else
	{
		if ( form_v_a[ivakt_zei - 1].zei == 0 )
		{
			v_edit1feldname=_T("");
			v_edit2feldwert=_T("");
			v_editvk=_T("");
			v_editnk=_T("");
			v_charlang=_T("");
			v_smallint=FALSE;
			v_integer=FALSE;
			v_date=FALSE;
			v_chara=FALSE;
			v_decimal=FALSE;
			if ( ivakt_zei < 1 )
				v_zei =_T("-1");
			else
				v_zei.Format("%d", ivakt_zei);
			return;
		}

	}

	v_edit1feldname.Format("%s",form_v_a[ ivakt_zei - 1].klar_txt) ;
	v_edit2feldwert.Format("%s",form_v_a[ ivakt_zei - 1].krz_txt) ;
	v_editvk.Format("%d",form_v_a[ ivakt_zei - 1].vk);
	v_editnk.Format("%d",form_v_a[ ivakt_zei - 1].nk);
	v_charlang.Format("%d",form_v_a[ ivakt_zei - 1].vk);

	v_smallint=FALSE;
	v_integer=FALSE;
	v_date=FALSE;
	v_chara=FALSE;
	v_decimal=FALSE;

	switch( form_v_a[ ivakt_zei - 1].typ)
	{
	   case iDBSMALLINT:
		v_smallint=TRUE;
		m_editvk.EnableWindow(FALSE);
		m_editnk.EnableWindow(FALSE);
		m_charlang.EnableWindow(FALSE);
		break;
	   case iDBINTEGER:
		v_integer=TRUE;
		m_editvk.EnableWindow(FALSE);
		m_editnk.EnableWindow(FALSE);
		m_charlang.EnableWindow(FALSE);
		break;
	   case iDBDATUM:
		v_date=TRUE;
		m_editvk.EnableWindow(FALSE);
		m_editnk.EnableWindow(FALSE);
		m_charlang.EnableWindow(FALSE);

		break;
	   case iDBDECIMAL:
		v_decimal=TRUE;
		m_editvk.EnableWindow(TRUE);
		m_editnk.EnableWindow(TRUE);
		m_charlang.EnableWindow(FALSE);
		break;
	   case iDBCHAR:
		v_chara=TRUE;
		m_editvk.EnableWindow(FALSE);
		m_editnk.EnableWindow(FALSE);
		m_charlang.EnableWindow(FALSE);

		break;
	}
	if ( form_v_a[ ivakt_zei - 1].zei == 0 )
		v_zei.Format("%d", ivakt_zei);
	else
		v_zei.Format("%d", form_v_a[ ivakt_zei - 1].zei);

}
int DIALVIRTUELL::plautest( int imodus )
{
	// UpdateData(FALSE) muss �bergeordnet realisiert sein 

	int problem = 0;
	if ( strlen( v_edit1feldname.GetBuffer()) > 0 )
	{
	}
	else
		problem = 1;
	if ( strlen ( v_edit2feldwert.GetBuffer()) > 0 )
	{
	}
	else
		problem = 1;
	if 	( v_smallint==FALSE && v_integer==FALSE &&
			v_date==FALSE && v_chara==FALSE && v_decimal==FALSE)
			problem = 1;

	if ( v_chara == TRUE && atol ( v_charlang.GetBuffer()) < 1 )
			problem = 1;
	if ( v_decimal == TRUE && 
		 (  atol ( v_editvk.GetBuffer() ) < 1 
		    || 
		     ( atol ( v_editvk.GetBuffer()) <= atol ( v_editnk.GetBuffer()))
		  )
	   )
	   problem = 1;

	if (problem)
	{
		if ( imodus == 1 )
			MessageBox("Inkonsistente Eingaben", MB_OK);
		return 0;
	}
	if ( atol (v_zei.GetBuffer()) == -1 )
		v_zei.Format("1");
	return 1 ;
}


void DIALVIRTUELL::ladearray( int ivakt_zei )
{
	if ( ivakt_zei < 1 )
			return;
	ivakt_zei -- ;

	sprintf ( form_v.klar_txt, "%s", v_edit1feldname.GetBuffer());
	sprintf (form_v.krz_txt,"%s", v_edit2feldwert.GetBuffer());
	form_v.vk = atol( v_editvk.GetBuffer());
	if (v_chara==TRUE) 
		form_v.vk =	atol ( v_charlang.GetBuffer());
	form_v.nk = atol( v_editnk.GetBuffer());

	if (v_smallint==TRUE)
		form_v.typ = iDBSMALLINT;
	if (v_integer==TRUE)
		form_v.typ = iDBINTEGER;
		;
	if (v_date==TRUE)
		form_v.typ = iDBDATUM;
	if (v_chara==TRUE)
		form_v.typ=iDBCHAR;
	if (v_decimal==TRUE)
		form_v.typ =iDBDECIMAL;
	form_v.zei = atol (v_zei.GetBuffer());
	if ( form_v.zei == -1 )
	{
				form_v.zei = 1;
				v_zei.Format("1");
	}
	memcpy ( form_v_a + ivakt_zei ,&form_v, sizeof ( struct FORM_V ));
}


BOOL DIALVIRTUELL::OnInitDialog()
{

	form_v.lila = vlila ;
	sprintf ( form_v.form_nr, "%s", vmyform_nr );
	vanz_zei = 0;
	vakt_zei = 0;

	form_v_a = NULL ;
	form_v_a = (struct FORM_V *)calloc(15 , sizeof(struct FORM_V ));

	DbClass.sqlopen (form_v_curs);
	form_v_a[0].zei = 0;

	while (!DbClass.sqlfetch(form_v_curs) && vanz_zei < 13 )
	{
		memcpy ( form_v_a + vanz_zei  , &form_v, sizeof ( struct FORM_V ));
		vanz_zei ++ ;
		form_v_a[vanz_zei].zei = 0;
		vakt_zei = 1;
	}


	lademaske(vakt_zei);
	if (vakt_zei == 0 )
		vakt_zei = 1;
	
	UpdateData(FALSE) ;
	return TRUE ;
}


DIALVIRTUELL::~DIALVIRTUELL()
{
}

void DIALVIRTUELL::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit1feldname);
	DDX_Text(pDX, IDC_EDIT1, v_edit1feldname);
	DDX_Control(pDX, IDC_EDIT2, m_edit2feldwert);
	DDX_Text(pDX, IDC_EDIT2, v_edit2feldwert);
	DDX_Control(pDX, IDC_DECVK, m_editvk);
	DDX_Text(pDX, IDC_DECVK, v_editvk);
	DDX_Control(pDX, IDC_DECNK, m_editnk);
	DDX_Text(pDX, IDC_DECNK, v_editnk);
	DDX_Control(pDX, IDC_CHARLANG, m_charlang);
	DDX_Text(pDX, IDC_CHARLANG, v_charlang);
	DDX_Control(pDX, IDC_SMALLINT, m_smallint);
	DDX_Check(pDX, IDC_SMALLINT, v_smallint);
	DDX_Control(pDX, IDC_INTEGER, m_integer);
	DDX_Check(pDX, IDC_INTEGER, v_integer);
	DDX_Control(pDX, IDC_DATE, m_date);
	DDX_Check(pDX, IDC_DATE, v_date);
	DDX_Control(pDX, IDC_DECIMAL, m_decimal);
	DDX_Control(pDX, IDC_CHARA, m_chara);
	DDX_Check(pDX, IDC_CHARA, v_chara);
	DDX_Check(pDX, IDC_DECIMAL, v_decimal);
	DDX_Control(pDX, IDC_ZEI, m_zei);
	DDX_Text(pDX, IDC_ZEI, v_zei);
}

BEGIN_MESSAGE_MAP(DIALVIRTUELL, CDialog)
	ON_BN_CLICKED(IDOK, &DIALVIRTUELL::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &DIALVIRTUELL::OnBnClickedCancel)
	ON_EN_KILLFOCUS(IDC_EDIT1, &DIALVIRTUELL::OnEnKillfocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT2, &DIALVIRTUELL::OnEnKillfocusEdit2)
	ON_BN_CLICKED(IDC_SMALLINT, &DIALVIRTUELL::OnBnClickedSmallint)
	ON_BN_CLICKED(IDC_INTEGER, &DIALVIRTUELL::OnBnClickedInteger)
	ON_BN_CLICKED(IDC_DATE, &DIALVIRTUELL::OnBnClickedDate)
	ON_BN_CLICKED(IDC_DECIMAL, &DIALVIRTUELL::OnBnClickedDecimal)
	ON_BN_CLICKED(IDC_CHARA, &DIALVIRTUELL::OnBnClickedChara)
	ON_EN_CHANGE(IDC_DECVK, &DIALVIRTUELL::OnEnKillfocusDecvk)
	ON_EN_CHANGE(IDC_DECNK, &DIALVIRTUELL::OnEnKillfocusDecnk)
	ON_EN_KILLFOCUS(IDC_CHARLANG, &DIALVIRTUELL::OnEnKillfocusCharlang)

	ON_BN_CLICKED(IDC_BDOWN, &DIALVIRTUELL::OnBnClickedBdown)
	ON_BN_CLICKED(IDC_BUP, &DIALVIRTUELL::OnBnClickedBup)
	ON_BN_CLICKED(IDC_BPLUS, &DIALVIRTUELL::OnBnClickedBplus)
	ON_BN_CLICKED(IDC_BMINUS, &DIALVIRTUELL::OnBnClickedBminus)
END_MESSAGE_MAP()


// DIALVIRTUELL-Meldungshandler

void DIALVIRTUELL::getListe(char * emyform_nr)
{
	sprintf ( vmyform_nr,"%s", emyform_nr);
}

void DIALVIRTUELL::getLila(short elila)
{
	vlila = elila;
}

void DIALVIRTUELL::OnBnClickedOk()
{

	UpdateData(TRUE);

	if ( plautest(1) )
	{
			ladearray ( vakt_zei ) ;
			if (vanz_zei == 0 )
				vanz_zei = 1 ;
	}
	else
// Fehlerhafte Eingaben
	   if ( vakt_zei == 1 && form_v_a[0].zei < 1 )
	   {
		   // weiter geht es
	   }
	   else
		return;

	int i = 0 ;
	sprintf ( form_v.form_nr, "%s", vmyform_nr );
	form_v.lila = vlila;

	DbClass.sqlexecute ( delform_v_curs );

	while ( i < vanz_zei )
	{
		memcpy ( &form_v, form_v_a + i , sizeof ( struct FORM_V));
		i ++;
		sprintf ( form_v.form_nr, "%s", vmyform_nr );
		form_v.lila = vlila;
		if ( form_v.zei > 0 )
			DbClass.sqlexecute ( insform_v_curs );
	}
//	free ( form_v_a );
	OnOK();
}

void DIALVIRTUELL::OnBnClickedCancel()
{
	free ( form_v_a );
	OnCancel();
}

void DIALVIRTUELL::OnEnKillfocusEdit1()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void DIALVIRTUELL::OnEnKillfocusEdit2()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void DIALVIRTUELL::OnBnClickedSmallint()
{

	UpdateData(TRUE);
	m_editvk.EnableWindow(FALSE);
	m_editnk.EnableWindow(FALSE);
	m_charlang.EnableWindow(FALSE);
	v_smallint=TRUE;
	v_integer=FALSE;
	v_date=FALSE;
	v_chara=FALSE;
	v_decimal=FALSE;
	UpdateData(FALSE);
}

void DIALVIRTUELL::OnBnClickedInteger()
{
	UpdateData(TRUE);
	m_editvk.EnableWindow(FALSE);
	m_editnk.EnableWindow(FALSE);
	m_charlang.EnableWindow(FALSE);
	v_smallint=FALSE;
	v_integer=TRUE;
	v_date=FALSE;
	v_chara=FALSE;
	v_decimal=FALSE;
	UpdateData(FALSE);
}

void DIALVIRTUELL::OnBnClickedDate()
{
	UpdateData(TRUE);
	m_editvk.EnableWindow(FALSE);
	m_editnk.EnableWindow(FALSE);
	m_charlang.EnableWindow(FALSE);
	v_smallint=FALSE;
	v_integer=FALSE;
	v_date=TRUE;
	v_chara=FALSE;
	v_decimal=FALSE;
	UpdateData(FALSE);
}

void DIALVIRTUELL::OnBnClickedDecimal()
{
	UpdateData(TRUE);
	m_editvk.EnableWindow(TRUE);
	m_editnk.EnableWindow(TRUE);
	m_charlang.EnableWindow(FALSE);
	v_smallint=FALSE;
	v_integer=FALSE;
	v_date=FALSE;
	v_chara=FALSE;
	v_decimal=TRUE;
	UpdateData(FALSE);
}

void DIALVIRTUELL::OnBnClickedChara()
{
	UpdateData(TRUE);
	m_editvk.EnableWindow(FALSE);
	m_editnk.EnableWindow(FALSE);
	m_charlang.EnableWindow(TRUE);
	v_smallint=FALSE;
	v_integer=FALSE;
	v_date=FALSE;
	v_chara=TRUE;
	v_decimal=FALSE;
	UpdateData(FALSE);
}

void DIALVIRTUELL::OnEnKillfocusDecvk()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CListView::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.
}

void DIALVIRTUELL::OnEnKillfocusDecnk()
{

}

void DIALVIRTUELL::OnEnKillfocusCharlang()
{
}

void DIALVIRTUELL::OnBnClickedBdown()
{

	UpdateData(TRUE);

	if ( plautest(1) )
		ladearray ( vakt_zei ) ;
	else
	{
		if ( vakt_zei < 2 )
			return;	// einfach mal nix

		if ( form_v_a[vakt_zei - 1 ].zei == 0 )
		{	// Leere Zeile verwerfen 
			vakt_zei -- ;
			lademaske ( vakt_zei ) ;
			UpdateData(FALSE);
			return;
		}
		return;	// Fehlerhaftes Zeugs
	}
	if ( vakt_zei < 2 )
		return;	// einfach mal nix
	vakt_zei -- ;
	lademaske ( vakt_zei ) ;
	UpdateData(FALSE);

}

void DIALVIRTUELL::OnBnClickedBup()
{
	UpdateData(TRUE);

	if ( plautest(1) )
			ladearray ( vakt_zei ) ;
	else
// Fehlerhafte Eingaben
		return;

	if ( vakt_zei > vanz_zei )
		vanz_zei = vakt_zei ;

	if ( vakt_zei > 14  ||	form_v_a[vakt_zei].zei == 0 || vakt_zei > vanz_zei )
		return;	// einfach mal nix


	vakt_zei ++ ;
	lademaske ( vakt_zei ) ;
	UpdateData(FALSE);
}

void DIALVIRTUELL::OnBnClickedBplus()
{
	UpdateData(TRUE);

	if ( plautest(1) )
			ladearray ( vakt_zei ) ;

	if ( vakt_zei > 14  ||	form_v_a[vakt_zei - 1 ].zei < 1 )
		return;	// einfach mal nix
	if ( form_v_a[vakt_zei ].zei > 0 )
	{
		OnBnClickedBup();	// einfach nur weiter bl�ttern
		return;
	}
	form_v_a[vakt_zei ].zei = 0;
	vakt_zei ++ ;
	lademaske ( vakt_zei ) ;
	if ( vakt_zei > vanz_zei )
		vanz_zei = vakt_zei ;
	UpdateData(FALSE);
}

void DIALVIRTUELL::OnBnClickedBminus()
{
	// Zeile rauswerfen und alles eins runter
	UpdateData(TRUE);

	if ( vakt_zei < 1 ) 
		return;	// einfach mal nix
	if ( form_v_a[vakt_zei - 1 ].zei < 1 )
		return;

	int iakt_zei ;

	iakt_zei = vakt_zei;

	while ( iakt_zei < vanz_zei )
	{
		memcpy ( form_v_a + (iakt_zei - 1 ), &form_v_a[iakt_zei ] ,sizeof(struct FORM_V));
		form_v_a[iakt_zei - 1 ].zei = iakt_zei ;
		iakt_zei ++ ;
	}

	vanz_zei -- ;
	if ( vanz_zei < 0 )
		vanz_zei = 0;

	if ( vakt_zei > vanz_zei  && vanz_zei > 0 )  
		vakt_zei = vanz_zei ;

	if ( vanz_zei >= 0 )
		form_v_a[vanz_zei ].zei = 0 ;

	if ( vakt_zei < 2 && vanz_zei == 0 )
	{
		lademaske(0);
	}
	else
		lademaske(vakt_zei);

	
	UpdateData(FALSE);

}
