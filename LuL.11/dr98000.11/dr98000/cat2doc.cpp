
// catalog2Doc.cpp : implementation of the CCatalog2Doc class
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.
#include "stdafx.h"
#include "mainfrm.h"
#include "catalog2.h"
#ifdef LL19
#include "cmbtll19.h"		// 011014
#else
#include "cmbtll11.h"		// 071205
#endif

#include "catsets.h"
#include "cat2Doc.h"
#include "TabPage.h"
#include "ColPage.h"
// GrJ

#include "DbClass.h"
#include "nform_tab.h"
#include "nform_feld.h"
#include "systables.h"
#include "mo_db.h"
#include "form_ow.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



short GlobalLila ;

DB_CLASS DbClass;
NFORM_TAB nform_tab;
NFORM_HEA nform_hea;
NFORM_FELD nform_feld; 
SYSTABLES systables; 
SYSCOLUMNS syscolumns;
FORM_O form_o;
FORM_W form_w;
FORM_V form_v;

FORM_TE form_te;	// 220207
FORM_T form_t;		// 220207

/////////////////////////////////////////////////////////////////////////////
// CCatalog2Doc

IMPLEMENT_DYNCREATE(CCatalog2Doc, CDocument)

BEGIN_MESSAGE_MAP(CCatalog2Doc, CDocument)
	//{{AFX_MSG_MAP(CCatalog2Doc)
	ON_COMMAND(ID_VIEW_SETTINGS, OnViewSettings)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCatalog2Doc construction/destruction

CCatalog2Doc::CCatalog2Doc()
{
	// initialize member recordset pointers
	m_pTableset = 0;
	m_pColumnset = 0;
}

CCatalog2Doc::~CCatalog2Doc()
{
}

BOOL CCatalog2Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// initialize current view level
	m_nLevel = levelNone;

	// initialize table settings
	m_bSystemTables = GetProfileValue(_T("TableSettings"),_T("SystemTables"));
	m_bViews        = GetProfileValue(_T("TableSettings"),_T("Views"));
	m_bSynonyms     = GetProfileValue(_T("TableSettings"),_T("SystemTables"));

	// initialize column info settings
	m_bLength       = GetProfileValue(_T("ColumnInfoSettings"),_T("Length"));
	m_bPrecision    = GetProfileValue(_T("ColumnInfoSettings"),_T("Precision"));
	m_bNullability  = GetProfileValue(_T("ColumnInfoSettings"),_T("Nullability"));

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CCatalog2Doc serialization

void CCatalog2Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCatalog2Doc diagnostics

#ifdef _DEBUG
void CCatalog2Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCatalog2Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCatalog2Doc commands

int CCatalog2Doc::GetProfileValue(LPCTSTR lpszSection,LPCTSTR lpszItem)
{
	int nValue = AfxGetApp()->GetProfileInt(lpszSection,lpszItem,-1);
	if (nValue == -1)
	{
		nValue = 0;
		AfxGetApp()->WriteProfileInt(lpszSection,lpszItem,nValue);
	}
	return nValue;
}

void CCatalog2Doc::SetLevel(Level nLevel)
{
	m_nLevel = nLevel;
	UpdateAllViews(NULL);
}

CString CCatalog2Doc::GetDSN()
{
	if (!m_Database.IsOpen())
		return _T("[No Data Source Selected]");

	// pull DSN from database connect string
	CString string = m_Database.GetConnect();
	string = string.Right(string.GetLength() - (string.Find(_T("DSN=")) + 4));
	string = string.Left(string.Find(_T(";")));
	return string;
}
// GrJ - alle cursoren nur einmalig generieren
void CCatalog2Doc::OnOpenBWS()
{
#ifndef OLABEL
    if (DbClass.DBase.GetHdbc () == NULL)
	{
#ifdef FITSMART
		DbClass.opendbase ("fsm");
#endif

#ifndef FITSMART
#ifdef FITODBC
		if ( strlen( odb_daba) > 0 )
		{
			DbClass.opendbase ( odb_daba );
		}
		else
			DbClass.opendbase ("bws");

#endif
#ifndef FITODBC
		// ohne odbc : immer bws
		DbClass.opendbase ("bws");
#endif
#endif

#ifndef FITODBC
// tabellen_curs : Lesen der Tabellen aus systables > 100
 
		DbClass.sqlout ((char *) systables.tabname, SQLCHAR,MAXNAME);
		tabellen_curs = DbClass.sqlcursor ("select tabname from systables where tabid > 99 order by tabname");


// tabid_curs : Lesen der Tabellen-ID
 
		DbClass.sqlin ((char *) systables.tabname, SQLCHAR,MAXNAME);

		DbClass.sqlout ((char *) systables.tabname, SQLCHAR,MAXNAME);
		DbClass.sqlout ((long *) &systables.tabid, SQLLONG,0);
		tabid_curs = DbClass.sqlcursor ("select tabname,tabid from systables where tabname = ? ");

// colid_curs : Lesen der Columns-Eigenschaften
 
		DbClass.sqlin ((char *) syscolumns.colname, SQLCHAR,MAXNAME);
		DbClass.sqlin ((long *) &syscolumns.tabid, SQLLONG,0);

		DbClass.sqlout ((char *) syscolumns.colname, SQLCHAR,MAXNAME);
		DbClass.sqlout ((long *) &syscolumns.tabid, SQLLONG,0);
		DbClass.sqlout ((short *) &syscolumns.coltype, SQLSHORT,0);
		colid_curs = DbClass.sqlcursor ("select colname,tabid,coltype from syscolumns where colname = ? and tabid = ? ");
#endif		// FITODBC

		
		
// formtab_curs : Test, ob tabelle fuer diese Format existiert
		DbClass.sqlin ((char *)   nform_tab.form_nr,   SQLCHAR,  11);
		DbClass.sqlin ((char *)   nform_tab.tab_nam,  SQLCHAR, MAXNAME);
		DbClass.sqlin ((short *)  &nform_tab.lila, SQLSHORT,0); 

		DbClass.sqlout ((short *) &nform_tab.delstatus, SQLSHORT,0);
		DbClass.sqlout ((char *) nform_tab.tab_nam, SQLCHAR,MAXNAME);

		formtab_curs = DbClass.sqlcursor ("select delstatus,tab_nam from nform_tab "
			" where form_nr = ? and tab_nam like ? and lila = ? "
			" order by tab_nam ");
	

// formfeld_curs : lese ob feld dieser Tabelle existiert ....
		DbClass.sqlin ((char *)   nform_feld.form_nr,   SQLCHAR, 11);
		DbClass.sqlin ((char *)   nform_feld.tab_nam,   SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *)   nform_feld.feld_nam,  SQLCHAR,MAXNAME);
		DbClass.sqlin ((short *)  &nform_feld.lila, SQLSHORT,0); 

		DbClass.sqlout ((short *) &nform_feld.delstatus, SQLSHORT,0);
		DbClass.sqlout ((char *)   nform_feld.form_nr,   SQLCHAR, 11);
		DbClass.sqlout ((char *)   nform_feld.feld_nam,  SQLCHAR,MAXNAME);

		DbClass.sqlout ((char *)   nform_feld.tab_nam,   SQLCHAR,MAXNAME);
		DbClass.sqlout ((short *) &nform_feld.feld_typ,  SQLSHORT,0);
		DbClass.sqlout ((short *) &nform_feld.feld_id,   SQLSHORT,0);
		DbClass.sqlout ((short *) &nform_feld.sort_nr,   SQLSHORT,0);
		DbClass.sqlout ((char *)   nform_feld.krz_txt,   SQLCHAR,25);
		DbClass.sqlout ((short *) &nform_feld.range,  SQLSHORT,0);
		
		formfeld_curs = DbClass.sqlcursor
			("select delstatus, form_nr,feld_nam,tab_nam,feld_typ,"
			 "feld_id,sort_nr,krz_txt,range from nform_feld where "
			 " form_nr = ? and tab_nam = ? and feld_nam like ? "
			 " and lila = ? order by feld_nam " );

// form_nr_curs : Lesen, ob Format vorhanden ist 
		DbClass.sqlin ((char *) nform_tab.form_nr, SQLCHAR ,11);
		DbClass.sqlin ((short *) &nform_tab.lila, SQLSHORT, 0 );

		DbClass.sqlout ((char *) nform_tab.form_nr, SQLCHAR ,11);
 		form_nr_curs = DbClass.sqlcursor
			("select distinct form_nr from nform_tab where form_nr"
			 " like ? and lila = ? order by form_nr");

// form_h_curs : Lesen von Zusatz-Infos Kopfebene
		DbClass.sqlin ((char *) nform_hea.form_nr, SQLCHAR ,11);
		DbClass.sqlin ((short *) &nform_hea.lila, SQLSHORT, 0 );

		DbClass.sqlout ((char *) nform_hea.form_ube, SQLCHAR ,129);
		DbClass.sqlout ((short *) &nform_hea.delstatus, SQLSHORT ,0);

 		form_h_curs = DbClass.sqlcursor
			("select form_ube, delstatus from nform_hea where form_nr "
			 " like ? and lila = ? " );

// form_h_d_curs : L�schen von Zusatz-Infos Kopfebene
		DbClass.sqlin ((char *) nform_hea.form_nr, SQLCHAR ,11);
		DbClass.sqlin ((short *) &nform_hea.lila, SQLSHORT, 0 );

 		form_h_d_curs = DbClass.sqlcursor
			("delete from nform_hea where form_nr"
			 " like ? and lila = ? " );


// form_h_i_curs : inserten
		DbClass.sqlin ((char *) nform_hea.form_nr, SQLCHAR ,11);
		DbClass.sqlin ((short *) &nform_hea.lila, SQLSHORT, 0 );
		DbClass.sqlin ((char *) nform_hea.form_ube, SQLCHAR ,129);
		DbClass.sqlin ((short *) &nform_hea.delstatus, SQLSHORT, 0 );

 		form_h_i_curs = DbClass.sqlcursor
			("insert into nform_hea ( form_nr,lila,form_ube,delstatus ) "
			 " values ( ?,?,?,?)" );

// form_h_u_curs : Updaten Tabelle fuer Format

		DbClass.sqlin ((char *) nform_hea.form_nr, SQLCHAR ,11);
		DbClass.sqlin ((short *) &nform_hea.lila, SQLSHORT, 0 );
		DbClass.sqlin ((char *) nform_hea.form_ube, SQLCHAR ,129);
		DbClass.sqlin ((short *) &nform_hea.delstatus, SQLSHORT, 0 );

		DbClass.sqlin ((char *) nform_hea.form_nr, SQLCHAR ,11);
		DbClass.sqlin ((short *) &nform_hea.lila, SQLSHORT, 0 );

		form_h_u_curs = DbClass.sqlcursor
			("update nform_hea set form_nr=? ,lila=?, "
			 " form_ube=?, delstatus =? where form_nr= ? and "
			 " lila = ? " );

		// deltab_curs : Loeschen des Tabelleneintrages

		DbClass.sqlin ((char *) nform_tab.form_nr,SQLCHAR,11);
		DbClass.sqlin ((char *) nform_tab.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((short *) &nform_tab.lila, SQLSHORT, 0 );

		deltab_curs = DbClass.sqlcursor
			("delete from nform_tab where form_nr = ? and "
			 " ( tab_nam like ?  or tab_nam IS NULL ) and lila = ? " );

// das is-null-Dingens dazu wegen doof-oracle 

// delfeld_curs : Loeschen des Feldeintrages je Tabelle,Format und Feld

		DbClass.sqlin ((char *) nform_feld.form_nr,SQLCHAR,11);
		DbClass.sqlin ((char *) nform_feld.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *) nform_feld.feld_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((short *) &nform_feld.lila, SQLSHORT, 0 );

		delfeld_curs = DbClass.sqlcursor
			("delete from nform_feld where form_nr = ? and "
			 " tab_nam like ? and feld_nam like ? and lila = ?" );



	// updtab_curs : Updaten Tabelle fuer Format

		DbClass.sqlin ((char *) nform_tab.form_nr,SQLCHAR,11);
		DbClass.sqlin ((char *) nform_tab.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((short *) &nform_tab.delstatus, SQLSHORT,0);
		DbClass.sqlin ((short *) &nform_tab.lila, SQLSHORT, 0 );


		DbClass.sqlin ((char *) nform_tab.form_nr,SQLCHAR,11);
		DbClass.sqlin ((char *) nform_tab.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((short *) &nform_tab.lila, SQLSHORT, 0 );

		updtab_curs = DbClass.sqlcursor
			("update nform_tab set form_nr=? ,tab_nam=?, "
			 " delstatus=?, lila =? where form_nr= ? and tab_nam = ? and "
			 " lila = ? " );


// updfeld_curs : Updaten Feld fuer Tabelle und Format

		DbClass.sqlin ((char *)   nform_feld.form_nr,SQLCHAR,11);
		DbClass.sqlin ((char *)   nform_feld.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *)   nform_feld.feld_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((short *) &nform_feld.delstatus,SQLSHORT,0);
		DbClass.sqlin ((short *) &nform_feld.feld_typ,SQLSHORT,0);
		DbClass.sqlin ((short *) &nform_feld.feld_id,SQLSHORT,0);
		DbClass.sqlin ((short *) &nform_feld.sort_nr,SQLSHORT,0);
		DbClass.sqlin ((char *)   nform_feld.krz_txt,SQLCHAR,25);
		DbClass.sqlin ((short *) &nform_feld.range,SQLSHORT,0);
		DbClass.sqlin ((short *) &nform_feld.lila, SQLSHORT, 0 );


		DbClass.sqlin ((char *) nform_feld.form_nr,SQLCHAR,11);
		DbClass.sqlin ((char *) nform_feld.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *) nform_feld.feld_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((short *) &nform_feld.lila, SQLSHORT, 0 );

		updfeld_curs = DbClass.sqlcursor
			("update nform_feld set form_nr=? ,tab_nam=?,feld_nam=?,"
			 " delstatus=?,feld_typ=?,feld_id=?,sort_nr=?,krz_txt =?,"
			 " range=?, lila =? where form_nr=? and tab_nam =? and "
			 " feld_nam=? and lila = ? "   );


// instab_curs : Inserten Tabelle fuer Format

		DbClass.sqlin ((char *) nform_tab.form_nr,SQLCHAR,11);
		DbClass.sqlin ((char *) nform_tab.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((short *) &nform_tab.delstatus, SQLSHORT,0);
		DbClass.sqlin ((short *) &nform_tab.lila, SQLSHORT, 0 );

		instab_curs = DbClass.sqlcursor ("insert into nform_tab ( form_nr,tab_nam,delstatus,lila ) values( ?, ?, ?, ? ) "  );

// insfeld_curs : Inserten Feld fuer Tabelle und Format

		DbClass.sqlin ((char *)  nform_feld.form_nr,SQLCHAR,11);
		DbClass.sqlin ((char *)  nform_feld.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *)  nform_feld.feld_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((short *) &nform_feld.delstatus,SQLSHORT,0);
		DbClass.sqlin ((short *) &nform_feld.feld_typ,SQLSHORT,0);
		DbClass.sqlin ((short *) &nform_feld.feld_id,SQLSHORT,0);
		DbClass.sqlin ((short *) &nform_feld.sort_nr,SQLSHORT,0);
		DbClass.sqlin ((char *)  nform_feld.krz_txt,SQLCHAR,25);
		DbClass.sqlin ((short *) &nform_feld.range,SQLSHORT,0);
		DbClass.sqlin ((short *) &nform_feld.lila, SQLSHORT, 0 );

		insfeld_curs = DbClass.sqlcursor
			("insert into nform_feld ( form_nr,tab_nam,feld_nam,delstatus,"
			 " feld_typ,feld_id,sort_nr,krz_txt,range,lila ) "
			 " values( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) "  );

// form_w_curs : Lese where-bedingungen

		DbClass.sqlin ((char *)  form_w.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_w.lila, SQLSHORT, 0 );

		DbClass.sqlout((char *)  form_w.krz_txt,SQLCHAR,51);
		DbClass.sqlout((long *)  &form_w.zei,SQLLONG,0);

		form_w_curs =
			DbClass.sqlcursor("select krz_txt,zei from form_w"
			" where form_nr = ? and lila = ? order by zei ");

// form_v_curs : Lese virtuelle Felder

		DbClass.sqlin ((char *)  form_v.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_v.lila, SQLSHORT, 0 );

		DbClass.sqlout((char *)  form_v.krz_txt,SQLCHAR,101);
		DbClass.sqlout((long *)  &form_v.zei,SQLLONG,0);
		DbClass.sqlout((long *)  &form_v.typ,SQLLONG,0);
		DbClass.sqlout((long *)  &form_v.vk,SQLLONG,0);
		DbClass.sqlout((long *)  &form_v.nk,SQLLONG,0);
		DbClass.sqlout((char *)  form_v.klar_txt,SQLCHAR,31);

		form_v_curs =
			DbClass.sqlcursor("select krz_txt,zei,typ , vk, nk , klar_txt from form_v"
			" where form_nr = ? and lila = ? order by zei ");

// form_o_curs : Lese order-bedingungen
		DbClass.sqlin ((char *)  form_o.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_o.lila, SQLSHORT, 0 );

		DbClass.sqlout((char *)  form_o.krz_txt,SQLCHAR,51);
		DbClass.sqlout((long *)  &form_o.zei,SQLLONG,0);

		form_o_curs =
			DbClass.sqlcursor("select krz_txt,zei from form_o"
			 " where form_nr = ? and lila = ? order by zei ");

// insform_w_curs : Schreibe where-bedingungen
		DbClass.sqlin ((char *)  form_w.form_nr,SQLCHAR,11);
		DbClass.sqlin ((long *)  &form_w.zei,SQLLONG,0);
		DbClass.sqlin ((char *)  form_w.krz_txt,SQLCHAR,51);
		DbClass.sqlin ((short *) &form_w.lila, SQLSHORT, 0 );

		insform_w_curs = 
			DbClass.sqlcursor("insert into form_w ( form_nr,"
			 " zei, krz_txt,lila) values ( ?, ?, ?, ?)");

// insform_v_curs : Schreibe virtuelle Felder
		DbClass.sqlin ((char *)  form_v.form_nr,SQLCHAR,11);
		DbClass.sqlin ((long *)  &form_v.zei,SQLLONG,0);
		DbClass.sqlin ((char *)  form_v.krz_txt,SQLCHAR,101);
		DbClass.sqlin ((short *) &form_v.lila, SQLSHORT, 0 );
		DbClass.sqlin ((long *)  &form_v.typ,SQLLONG,0);
		DbClass.sqlin ((long *)  &form_v.vk,SQLLONG,0);
		DbClass.sqlin ((long *)  &form_v.nk,SQLLONG,0);
		DbClass.sqlin ((char *)  form_v.klar_txt,SQLCHAR,31);

		insform_v_curs = 
			DbClass.sqlcursor("insert into form_v ( form_nr,"
			 " zei, krz_txt,lila,typ, vk, nk, klar_txt ) values ( ?, ?, ?, ?, ?, ?, ?, ?)");

// insform_o_curs : Schreibe order-bedingungen
		DbClass.sqlin ((char *)  form_o.form_nr,SQLCHAR,11);
		DbClass.sqlin ((long *)  &form_o.zei,SQLLONG,0);
		DbClass.sqlin ((char *)  form_o.krz_txt,SQLCHAR,51);
		DbClass.sqlin ((short *) &form_o.lila, SQLSHORT, 0 );

		insform_o_curs = 
			DbClass.sqlcursor("insert into form_o ( form_nr,"
			" zei, krz_txt, lila ) values ( ?, ?, ?, ?)");

// delform_w_curs : loesche where-bedingungen

		DbClass.sqlin ((char *)  form_w.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_w.lila,SQLSHORT, 0);
		delform_w_curs =
			DbClass.sqlcursor("delete from form_w where form_nr = ? and lila = ? " );

// delform_v_curs : loesche viruelle Felder

		DbClass.sqlin ((char *)  form_v.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_v.lila,SQLSHORT, 0);
		delform_v_curs =
			DbClass.sqlcursor("delete from form_v where form_nr = ? and lila = ? " );

// delform_o_curs : loesche order-bedingungen
		DbClass.sqlin ((char *)  form_o.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_o.lila, SQLSHORT, 0 );

		delform_o_curs =
			DbClass.sqlcursor("delete from form_o where form_nr = ? and lila = ? " );

//	220207 : diverse Cursoren

// form_te_curs : Lese where-bedingungen fuer te

		DbClass.sqlin ((char *)  form_te.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_te.lila, SQLSHORT, 0 );
		DbClass.sqlin ((char *)  form_te.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *) &form_te.feld_nam, SQLCHAR, MAXNAME );

		DbClass.sqlout((char *)  form_te.krz_txt,SQLCHAR,51);
		DbClass.sqlout((long *)  &form_te.zei,SQLLONG,0);

		form_te_curs =
			DbClass.sqlcursor("select krz_txt,zei from form_te"
			" where form_nr = ? and lila = ? and tab_nam = ? "
			" and feld_nam = ? order by zei ");

// form_t_curs : Lese allerlei-bedingungen fuer form_t

		DbClass.sqlin ((char *)  form_t.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_t.lila, SQLSHORT, 0 );
		DbClass.sqlin ((char *)  form_t.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *)  form_t.feld_nam, SQLCHAR, MAXNAME );

		DbClass.sqlout((char *)  form_t.ttab_nam ,SQLCHAR,MAXNAME);

		DbClass.sqlout((char *)  form_t.sfeld1 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.sfeld2 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.sfeld3 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.sfeld4 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.sfeld5 ,SQLCHAR,19);

		DbClass.sqlout((char *)  form_t.wtab1 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.wtab2 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.wtab3 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.wtab4 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.wtab5 ,SQLCHAR,19);

		DbClass.sqlout((char *)  form_t.wfeld1 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.wfeld2 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.wfeld3 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.wfeld4 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.wfeld5 ,SQLCHAR,19);

		DbClass.sqlout((char *)  form_t.ofeld1 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.ofeld2 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.ofeld3 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.ofeld4 ,SQLCHAR,19);
		DbClass.sqlout((char *)  form_t.ofeld5 ,SQLCHAR,19);

		DbClass.sqlout((short *) &form_t.odesc1,SQLSHORT,0);
		DbClass.sqlout((short *) &form_t.odesc2,SQLSHORT,0);
		DbClass.sqlout((short *) &form_t.odesc3,SQLSHORT,0);
		DbClass.sqlout((short *) &form_t.odesc4,SQLSHORT,0);
		DbClass.sqlout((short *) &form_t.odesc5,SQLSHORT,0);

		form_t_curs =
			DbClass.sqlcursor("select ttab_nam "
			" ,sfeld1, sfeld2, sfeld3, sfeld4, sfeld5 "
			" ,wtab1, wtab2, wtab3, wtab4, wtab5 "
			" ,wfeld1, wfeld2, wfeld3, wfeld4, wfeld5 "
			" ,ofeld1, ofeld2, ofeld3, ofeld4, ofeld5 "
			" ,odesc1, odesc2, odesc3, odesc4, odesc5 "
			" from form_t"
			" where form_nr = ? and lila = ? and tab_nam = ? "
			" and feld_nam = ? ");

// insform_te_curs : Schreibe where-bedingungen fuer textext

		DbClass.sqlin ((char *)  form_te.form_nr,SQLCHAR,11);
		DbClass.sqlin ((char *)  form_te.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *)  form_te.feld_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((short *) &form_te.lila, SQLSHORT, 0 );
		DbClass.sqlin ((long *)  &form_te.zei,SQLLONG,0);
		DbClass.sqlin ((char *)  form_te.krz_txt,SQLCHAR,51);


		insform_te_curs = 
			DbClass.sqlcursor("insert into form_te ( form_nr , "
			" tab_nam, feld_nam, lila, " 
			" zei, krz_txt ) values ( ?, ?, ?, ?, ?, ? )");

// insform_t_curs : Schreibe allerlei-bedingungen fuer form_t

		DbClass.sqlin ((char *)  form_t.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_t.lila, SQLSHORT, 0 );
		DbClass.sqlin ((char *)  form_t.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *)  form_t.feld_nam, SQLCHAR, MAXNAME );

		DbClass.sqlin((char *)  form_t.ttab_nam ,SQLCHAR,MAXNAME);

		DbClass.sqlin((char *)  form_t.sfeld1 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.sfeld2 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.sfeld3 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.sfeld4 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.sfeld5 ,SQLCHAR,19);

		DbClass.sqlin((char *)  form_t.wtab1 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.wtab2 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.wtab3 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.wtab4 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.wtab5 ,SQLCHAR,19);

		DbClass.sqlin((char *)  form_t.wfeld1 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.wfeld2 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.wfeld3 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.wfeld4 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.wfeld5 ,SQLCHAR,19);

		DbClass.sqlin((char *)  form_t.ofeld1 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.ofeld2 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.ofeld3 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.ofeld4 ,SQLCHAR,19);
		DbClass.sqlin((char *)  form_t.ofeld5 ,SQLCHAR,19);

		DbClass.sqlin((short *) &form_t.odesc1,SQLSHORT,0);
		DbClass.sqlin((short *) &form_t.odesc2,SQLSHORT,0);
		DbClass.sqlin((short *) &form_t.odesc3,SQLSHORT,0);
		DbClass.sqlin((short *) &form_t.odesc4,SQLSHORT,0);
		DbClass.sqlin((short *) &form_t.odesc5,SQLSHORT,0);

		insform_t_curs =
			DbClass.sqlcursor("insert into form_t ( "
			" form_nr, lila, tab_nam, feld_nam "
			" ,ttab_nam "
			" ,sfeld1, sfeld2, sfeld3, sfeld4, sfeld5 "
			" ,wtab1, wtab2, wtab3, wtab4, wtab5 "
			" ,wfeld1, wfeld2, wfeld3, wfeld4, wfeld5 "
			" ,ofeld1, ofeld2, ofeld3, ofeld4, ofeld5 "
			" ,odesc1, odesc2, odesc3, odesc4, odesc5 "
			" ) values ( "
			" ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, "
			" ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,? ) " 
			 );
		
// delform_t_curs : loesche exttext-bedingungen
		DbClass.sqlin ((char *)  form_t.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_t.lila, SQLSHORT, 0 );
		DbClass.sqlin ((char *)  form_t.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *)  form_t.feld_nam,SQLCHAR,MAXNAME);
	
		delform_t_curs =
			DbClass.sqlcursor("delete from form_t where form_nr = ? and lila = ? "
			" and tab_nam like ? and feld_nam like ? " );

// delform_te_curs : loesche exttext-bedingungen
		DbClass.sqlin ((char *)  form_te.form_nr,SQLCHAR,11);
		DbClass.sqlin ((short *) &form_te.lila, SQLSHORT, 0 );
		DbClass.sqlin ((char *)  form_te.tab_nam,SQLCHAR,MAXNAME);
		DbClass.sqlin ((char *)  form_te.feld_nam,SQLCHAR,MAXNAME);

		delform_te_curs =
			DbClass.sqlcursor("delete from form_te where form_nr = ? and lila = ? "
			" and tab_nam like ? and feld_nam like ? " );

	}

#endif	// OLABEL
}

BOOL CCatalog2Doc::OnOpenDocument()
{
	// close and delete any open recordsets
	if (m_pTableset)
	{
		if (m_pTableset->IsOpen())
			m_pTableset->Close();
		delete m_pTableset;
		m_pTableset = 0;
	}
	if (m_pColumnset)
	{
		if (m_pColumnset->IsOpen())
			m_pColumnset->Close();
		delete m_pColumnset;
		m_pColumnset = 0;
	}

	// close the database
	if (m_Database.IsOpen())
		m_Database.Close();

	// open the database
/* --->
	if (m_Database.Open(NULL,FALSE,TRUE))
< ---- */


	OnOpenBWS ();
BOOL bstat = FALSE ;
#ifdef FITSMART
	bstat = m_Database.Open(_T( "fsm" ),FALSE,TRUE) ;
#else
#ifdef FITODBC
		if ( strlen( odb_daba) > 0 )
		{
			if ( odbdabatyp == FITORACLE )
			{
// 230610			bstat = m_Database.OpenEx(_T("DSN=bwso;UID=bwso;PWD=t0pfit" ), 0 ) ;
			char paramst [256] ;
			sprintf ( paramst , ";DSN=%s;UID=%s;PWD=%s;", odb_daba, odb_user, odb_pass ) ;
			bstat = m_Database.OpenEx(_T(paramst), 6 ) ;

			}
			else
			bstat = m_Database.Open(_T( odb_daba ),FALSE,TRUE) ;
		}
		else
			bstat = m_Database.Open(_T( "bws" ),FALSE,TRUE) ;
#else
		// ohne odbc : immer bws
	bstat = m_Database.Open(_T( "bws" ),FALSE,TRUE) ;
#endif
#endif

	if ( bstat )
	{
		if (FetchTableInfo())
			return TRUE;
	}
	return FALSE;

}

void CCatalog2Doc::OnCloseDocument()
{
	if (m_pTableset)
	{
		if (m_pTableset->IsOpen())
			m_pTableset->Close();
		delete m_pTableset;
		m_pTableset = 0;
	}
	if (m_pColumnset)
	{
		if (m_pColumnset->IsOpen())
			m_pColumnset->Close();
		delete m_pColumnset;
		m_pColumnset = 0;
	}

	if (m_Database.IsOpen())
		m_Database.Close();

	CDocument::OnCloseDocument();
}

// 071205 void CCatalog2Doc::FetchColumnInfo(LPCSTR lpszName)
void CCatalog2Doc::FetchColumnInfo(LPCSTR lpszName, LPCSTR lpszColName)
{
	if (m_pColumnset)
	{
		if (m_pColumnset->IsOpen())
			m_pColumnset->Close();
		delete m_pColumnset;
		m_pColumnset = 0;
	}
	m_pColumnset = new CColumns(&m_Database);
// 071205 	m_pColumnset->Open(NULL,NULL,lpszName,NULL,CRecordset::snapshot);
	m_pColumnset->Open(NULL,NULL,lpszName,lpszColName,CRecordset::snapshot);
}

BOOL CCatalog2Doc::FetchTableInfo()
{
	m_pTableset = new CTables(&m_Database);

	// Must use char array for ODBC interface
	// (can simply hard code max size)
	char lpszType[64];

	strcpy(lpszType, "'TABLE'");
	if (m_bViews)
		strcat(lpszType, ",'VIEW'");
	if (m_bSystemTables)
		strcat(lpszType, ",'SYSTEM TABLE'");
	if (m_bSynonyms)
		strcat(lpszType, ",'ALIAS','SYNONYM'");

	if (!m_pTableset->Open(NULL,NULL,NULL,lpszType,CRecordset::snapshot))
	{
		delete m_pTableset;
		m_pTableset = NULL;
		m_Database.Close();
		return FALSE;
	}
	return TRUE;
}

void CCatalog2Doc::OnViewSettings()
{
	CPropertySheet  sheet(_T("Settings"));
	CTablePage      pageTable;
	CColumnPage     pageColumn;

	// initialize and add table settings page
	sheet.AddPage(&pageTable);
	pageTable.m_bSystemTables = m_bSystemTables;
	pageTable.m_bViews = m_bViews;
	pageTable.m_bSynonyms = m_bSynonyms;

	// initialize and add column info settings page
	sheet.AddPage(&pageColumn);
	pageColumn.m_bLength = m_bLength;
	pageColumn.m_bPrecision = m_bPrecision;
	pageColumn.m_bNullability = m_bNullability;

	// execte property sheet and update settings
	if (sheet.DoModal() == IDOK) {
		BOOL    bTableModified = FALSE;
		BOOL    bColumnModified = FALSE;

		if (m_bSystemTables != pageTable.m_bSystemTables)
		{
			m_bSystemTables = pageTable.m_bSystemTables;
			AfxGetApp()->WriteProfileInt(_T("TableSettings"),
				_T("SystemTables"),m_bSystemTables);
			bTableModified = TRUE;
		}
		if (m_bViews != pageTable.m_bViews)
		{
			m_bViews = pageTable.m_bViews;
			AfxGetApp()->WriteProfileInt(_T("TableSettings"),
				_T("Views"),m_bViews);
			bTableModified = TRUE;
		}
		if (m_bSynonyms != pageTable.m_bSynonyms)
		{
			m_bSynonyms = pageTable.m_bSynonyms;
			AfxGetApp()->WriteProfileInt(_T("TableSettings"),
				_T("Synonyms"),m_bSynonyms);
			bTableModified = TRUE;
		}
		if (m_bLength != pageColumn.m_bLength)
		{
			m_bLength = pageColumn.m_bLength;
			AfxGetApp()->WriteProfileInt(_T("ColumnInfoSettings"),
				_T("Length"),m_bLength);
			bColumnModified = TRUE;
		}
		if (m_bPrecision != pageColumn.m_bPrecision)
		{
			m_bPrecision = pageColumn.m_bPrecision;
			AfxGetApp()->WriteProfileInt(_T("ColumnInfoSettings"),
				_T("Precision"),m_bPrecision);
			bColumnModified = TRUE;
		}
		if (m_bNullability != pageColumn.m_bNullability)
		{
			m_bNullability = pageColumn.m_bNullability;
			AfxGetApp()->WriteProfileInt(_T("ColumnInfoSettings"),
				_T("Nullability"),m_bNullability);
			bColumnModified = TRUE;
		}

		// check for table modification first
		if (bTableModified)
		{
			// close and delete any open recordsets
			if (m_pTableset)
			{
				if (m_pTableset->IsOpen())
					m_pTableset->Close();
				delete m_pTableset;
				m_pTableset = 0;
			}
			if (m_pColumnset)
			{
				if (m_pColumnset->IsOpen())
					m_pColumnset->Close();
				delete m_pColumnset;
				m_pColumnset = 0;
			}

			// refresh table data
			FetchTableInfo();
			SetLevel(levelTable);
			UpdateAllViews(NULL);
		}

		// if table settings not modified, check column info
		else if (bColumnModified)
			UpdateAllViews(NULL);
	}
}
