// CneuFormat.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "catalog2.h"
// 130104
#include "neuFormat.h"
// #include "catsets.h"
// #include "cat2doc.h"
// #include "cat2view.h"
// #include "DbClass.h"
// #include "nform_tab.h"
// #include "nform_feld.h"
// #include "mo_db.h"
// CneuFormat-Dialogfeld

//extern DB_CLASS DbClass;
//extern NFORM_TAB nform_tab;
// extern NFORM_FELD nform_feld; 
// CCatalog2Doc*   pDoc;
extern char * uPPER (char *) ;

extern short GlobalLila ;

IMPLEMENT_DYNAMIC(CneuFormat, CPropertyPage)
CneuFormat::CneuFormat()
	: CPropertyPage(CneuFormat::IDD)
	, v_neuformat(_T(""))
	, m_bliste(FALSE)
	, m_blabel(FALSE)
{
}

CneuFormat::~CneuFormat()
{
}

void CneuFormat::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_neuformat);
	DDX_Text(pDX, IDC_EDIT1, v_neuformat);
	DDV_MaxChars(pDX, v_neuformat, 10);
	DDX_Control(pDX, IDC_CHECK1, m_liste);
	DDX_Check(pDX, IDC_CHECK1, m_bliste);
	DDX_Control(pDX, IDC_CHECK2, m_label);
	DDX_Check(pDX, IDC_CHECK2, m_blabel);
}


BEGIN_MESSAGE_MAP(CneuFormat, CPropertyPage)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK1, OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_CHECK2, OnBnClickedCheck2)
END_MESSAGE_MAP()

static char xfeldnam[128] ;
static short xGlobalLila ;

void CneuFormat::puttitel(char * efeldnam)
{
	sprintf ( xfeldnam,"%s", efeldnam );
}

BOOL CneuFormat::OnInitDialog()
{
			SetWindowText(xfeldnam);
			xGlobalLila = GlobalLila ;
			if (! GlobalLila)
			{
				m_bliste = TRUE ;
				m_blabel = FALSE ;
			}
			else
			{
				m_bliste = FALSE ;
				m_blabel = TRUE ;
			}

			UpdateData(FALSE) ;
	return TRUE ;
}

// neuFormat-Meldungshandler

void CneuFormat::OnBnClickedOk()
{
	CDialog::OnOK();

	if (strlen (v_neuformat) < 5 ||strlen (v_neuformat) > 10  )
	{
		v_neuformat = "" ;


	}

	GlobalLila = 0 ;
	if (m_blabel == TRUE)
	{
		GlobalLila = 1 ;
	}

}
char * CneuFormat::neuformat()
{

	char *S= v_neuformat.GetBuffer(512) ; 

	uPPER(S) ;
	return  S ;
}
void CneuFormat::OnBnClickedCancel()
{
	CDialog::OnCancel();
	v_neuformat = "" ;
}

// Check-Box Liste
void CneuFormat::OnBnClickedCheck1()
{
	m_bliste = TRUE ;
	m_blabel = FALSE;
	UpdateData(FALSE) ;
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

// Check-Box Label
void CneuFormat::OnBnClickedCheck2()
{
	m_bliste = FALSE;
	m_blabel = TRUE ;
	UpdateData(FALSE);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}
