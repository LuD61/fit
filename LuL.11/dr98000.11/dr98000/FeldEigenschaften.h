#pragma once
#include "afxwin.h"

#define FT_LOESCH -1
#define FT_STANDARD 0
#define FT_PTABN 1
#define FT_TEXTBAUST 2
#define FT_ADRESSE 3
#define FT_TEXTEXT 4
#define FT_FELDEXT 5

// CFeldEigenschaften-Dialogfeld

class CFeldEigenschaften : public CPropertyPage
{
	DECLARE_DYNAMIC(CFeldEigenschaften)

public:
	CFeldEigenschaften();
	virtual ~CFeldEigenschaften();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	char ifeldnam[128] ;
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnBnClickedCheck3();
	void putrange(int) ;
	int getrange() ;
	void putfeldtyp(int) ;
	int getfeldtyp() ;
	void putfeldnam(char *);
	void putkrztxt(char*);
	char * getkrztxt(char*);
	char * rkrztxt();
	CButton m_ptabn;
	CButton m_textbaust;
	CButton m_adresse;
	BOOL m_bptabn;
	BOOL m_btextbaust;
	BOOL m_badresse;
	BOOL m_bloesch;
	CButton m_loesch;
	afx_msg void OnBnClickedCheck4();
	afx_msg void OnBnClickedCheck5();
	CButton m_standard;
	BOOL m_bstandard;
	CEdit m_ckrz_txt;
	CString v_krz_txt;
	CEdit m_numrange;
	short v_numrange;
	afx_msg void OnBnClickedCheck6();
	CButton m_textext;
	BOOL m_btextext;
};
