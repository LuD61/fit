// FeldEigenschaften.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "catalog2.h"
#include "FeldEigenschaften.h"

// CFeldEigenschaften-Dialogfeld

IMPLEMENT_DYNAMIC(CFeldEigenschaften, CPropertyPage)
CFeldEigenschaften::CFeldEigenschaften()
	: CPropertyPage(CFeldEigenschaften::IDD)
	, m_bptabn(FALSE)
	, m_btextbaust(FALSE)
	, m_badresse(FALSE)
	, m_bloesch(FALSE)
	, m_bstandard(FALSE)
	, m_btextext(FALSE)
	, v_krz_txt(_T(""))
//	, m_brange(FALSE)
	, v_numrange(0)
{
}

BOOL CFeldEigenschaften::OnInitDialog()
{
		SetWindowText(ifeldnam);
		UpdateData(FALSE) ;
	return TRUE ;
}

CFeldEigenschaften::~CFeldEigenschaften()
{
}


void CFeldEigenschaften::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK1, m_ptabn);
	DDX_Control(pDX, IDC_CHECK2, m_textbaust);
	DDX_Control(pDX, IDC_CHECK3, m_adresse);
	DDX_Control(pDX, IDC_CHECK4, m_loesch);
	DDX_Control(pDX, IDC_CHECK5, m_standard);
	DDX_Control(pDX, IDC_CHECK6, m_textext);
	DDX_Check(pDX, IDC_CHECK1, m_bptabn);
	DDX_Check(pDX, IDC_CHECK2, m_btextbaust);
	DDX_Check(pDX, IDC_CHECK3, m_badresse);
	DDX_Check(pDX, IDC_CHECK4, m_bloesch);
	DDX_Check(pDX, IDC_CHECK5, m_bstandard);
	DDX_Check(pDX, IDC_CHECK6, m_btextext);

	DDX_Control(pDX, IDC_EDIT1, m_ckrz_txt);
	DDX_Text(pDX, IDC_EDIT1, v_krz_txt);
	DDV_MaxChars(pDX, v_krz_txt, 18);
	DDX_Control(pDX, IDC_EDIT2, m_numrange);
	DDX_Text(pDX, IDC_EDIT2, v_numrange);
}


BEGIN_MESSAGE_MAP(CFeldEigenschaften, CPropertyPage)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK1, OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_CHECK2, OnBnClickedCheck2)
	ON_BN_CLICKED(IDC_CHECK3, OnBnClickedCheck3)
	ON_BN_CLICKED(IDC_CHECK4, OnBnClickedCheck4)
	ON_BN_CLICKED(IDC_CHECK5, OnBnClickedCheck5)
	ON_BN_CLICKED(IDC_CHECK6, OnBnClickedCheck6)
END_MESSAGE_MAP()

void CFeldEigenschaften::putfeldnam(char * efeldnam)
{
	sprintf ( ifeldnam,"%s", efeldnam );
}


static int saverange ;

void CFeldEigenschaften::putrange(int v_range)
{


//	if ( v_range ) m_brange	= TRUE ;
//	else m_brange = FALSE ;

	saverange		= v_range ;
	v_numrange  = ( short ) v_range ;	// 011205 
}
int CFeldEigenschaften::getrange()
{
	int v_range = 0 ;

//	if ( m_brange == TRUE ) v_range = 1 ;

	v_range = v_numrange ;	// 011205
	return v_range ;
}

static int savefeldtyp ;

void CFeldEigenschaften::putfeldtyp(int feldtyp)
{

	m_bptabn		= FALSE ;
	m_btextbaust	= FALSE ;
	m_badresse		= FALSE ;
	m_bloesch		= FALSE ;
	savefeldtyp     =feldtyp ;
	switch ( feldtyp )
	{

	case FT_PTABN : m_bptabn = TRUE ; break ;
	case FT_TEXTBAUST: m_btextbaust = TRUE ; break ;
	case FT_ADRESSE: m_badresse = TRUE ; break ;
	case FT_TEXTEXT: m_btextext = TRUE ; break ;
	case FT_FELDEXT: m_btextext = TRUE ; break ;	// 071110
	case FT_STANDARD: m_bstandard = TRUE ; break ;

	}

}
int CFeldEigenschaften::getfeldtyp()
{
	int feldtyp = FT_STANDARD ;

	if ( m_bstandard == TRUE ) feldtyp = FT_STANDARD ;
	if ( m_bptabn == TRUE ) feldtyp = FT_PTABN ;
	if ( m_btextbaust == TRUE ) feldtyp = FT_TEXTBAUST ;
	if ( m_badresse == TRUE ) feldtyp = FT_ADRESSE ;
	if ( m_btextext == TRUE ) feldtyp = FT_TEXTEXT ;
	if ( m_bloesch == TRUE ) feldtyp = FT_LOESCH ;
	return feldtyp ;
}

static char savekrztxt[129] ;

char * CFeldEigenschaften::rkrztxt()
{

	char *S= v_krz_txt.GetBuffer(512) ; 
	return  S ;
}

void CFeldEigenschaften::putkrztxt(char * ekrztxt)
{

	sprintf ( savekrztxt,"%s", ekrztxt );
	v_krz_txt = _T( ekrztxt) ;
}
char * CFeldEigenschaften::getkrztxt(char * ekrztxt)
{

	if ( m_btextbaust == TRUE || m_btextext == TRUE  || m_bptabn == TRUE )	// 220207 -091209
	{
		sprintf ( ekrztxt, "%s", rkrztxt() );
	}
	else sprintf ( ekrztxt ," " );
	return ekrztxt ;
}


// CFeldEigenschaften-Meldungshandler

void CFeldEigenschaften::OnBnClickedOk()
{
	// if ( m_btextext == FALSE &&  savefeldtyp == FT_TEXTEXT )
	// loeschen des Datenbank-Eintrages im aufrufenden Programm
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CDialog::OnOK();
}

void CFeldEigenschaften::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.


	m_bstandard		= FALSE ;	// 240406 : erst mal ALLE reseten
	m_bptabn		= FALSE ;
	m_btextbaust	= FALSE ;
	m_btextext	    = FALSE ;
	m_badresse		= FALSE ;
	m_bloesch		= FALSE ;

	switch ( savefeldtyp )
	{
	case FT_STANDARD: m_bstandard  = TRUE ; break ;
	case FT_PTABN: m_bptabn     = TRUE ; break ;
	case FT_TEXTBAUST: m_btextbaust = TRUE ; break ;
	case FT_ADRESSE: m_badresse   = TRUE ; break ;
	case FT_TEXTEXT: m_btextext   = TRUE ; break ;
	case FT_FELDEXT: m_btextext   = TRUE ; break ;	// 071110
	}
	v_krz_txt = _T( savekrztxt );

//	if ( saverange ) m_brange	= TRUE ;
//	else m_brange = FALSE ;
	v_numrange = (short) saverange ;	// 011205
	CDialog::OnCancel();
}

void CFeldEigenschaften::OnBnClickedCheck1()
{

	m_bstandard		= FALSE ;
	m_bptabn		= TRUE ;
	m_btextbaust	= FALSE ;
	m_badresse		= FALSE ;
	m_bloesch		= FALSE ;
	m_btextext		= FALSE ;
	v_krz_txt = _T( "" );
	UpdateData(FALSE);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CFeldEigenschaften::OnBnClickedCheck2()
{
	m_bstandard		= FALSE ;
	m_bptabn		= FALSE ;
	m_btextbaust	= TRUE ;
	m_badresse		= FALSE ;
	m_bloesch		= FALSE ;
	m_btextext		= FALSE ;
	UpdateData(FALSE);
}

void CFeldEigenschaften::OnBnClickedCheck3()
{
	m_bstandard		= FALSE ;
	m_bptabn		= FALSE ;
	m_btextbaust	= FALSE ;
	m_badresse		= TRUE ;
	m_bloesch		= FALSE ;
	m_btextext		= FALSE ;
	v_krz_txt = _T( "" );
	UpdateData(FALSE);
}

void CFeldEigenschaften::OnBnClickedCheck4()
{
	m_bstandard		= FALSE ;
	m_bptabn		= FALSE ;
	m_btextbaust	= FALSE ;
	m_badresse		= FALSE ;
	m_bloesch		= TRUE ;
	m_btextext		= FALSE ;
	UpdateData(FALSE);
}

void CFeldEigenschaften::OnBnClickedCheck5()
{
	m_bstandard		= TRUE ;
	m_bptabn		= FALSE ;
	m_btextbaust	= FALSE ;
	m_badresse		= FALSE ;
	m_bloesch		= FALSE ;
	m_btextext		= FALSE ;
	v_krz_txt = _T( "" );
	UpdateData(FALSE);
}

// 130207 : m_btextext an diversen Stellen zugefuegt
void CFeldEigenschaften::OnBnClickedCheck6()
{
	m_bstandard		= FALSE ;
	m_bptabn		= FALSE ;
	m_btextbaust	= FALSE ;
	m_badresse		= FALSE ;
	m_bloesch		= FALSE ;
	m_btextext		= TRUE ;
	UpdateData(FALSE);
}
