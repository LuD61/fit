* Ziel-Tabelle

Zieltab

* select-fields : komplette schluessel-felder aus der Zieltabelle
 ( sicher meist genau wie where-fields
$s1 : sfield1
$s2 : sfield2
$s3 : sfield3 
$s4 : sfield4
$s5 : sfield5

* where-fields :primaere Selektionskrit. ( muessen vorhanden sein)

$w1 : wtab1.wfield1
$w2 : wtab2.wfield2
$w3 : wtab3.wfield3
$w4 : wtab4.wfield4

* order-fields : werden spaeter selektiert ( datentyp) und geordert
                        sowie als sekundaere sel-Krits verwendet

$o1 : ofeld1 desc
$o2 : ofeld2 desc
$o3 : ofeld3 desc
$o4 : ofeld4 desc

Zielstatement :   ( die durch "%" geklammerten Werte werden aktuell
               zugewiesen und muessen im Hauptstatement selektiert sein )

select a,mdn,kun,kun_bran2 from mempos where a = %lsp.a%
 and ( mdn = %lsk.mdn% or mdn = 0 )  
 and ( kun = %lsk.kun% or kun = 0 )
 and ( kun_bran2 = %kun.kun_bran2% or  kun_bran2 = "0")
  order by mdn desc, kun desc, kun_bran2 desc

Daraus wird dann :
bitte erfassen
Zieltab : mempos

$s1 : a
Ss2 : mdn
$s3 : kun
$s4 : kun_bran2

$w1 : lsp.a
$w2 : lsk.mdn
$w3 : lsk.kun
$w4 : kun.kun_bran2

$o1 : mdn       desc
$o2 : kun       desc
$o3 : kun_bran2 desc

where-Statement :
  a = $w1 and ( mdn = $w2  or mdn = 0 )
  and ( kun = $w3  or kun = 0 )
  and ( kun_bran2 = "$w4" or kun_bran2 = "0" )

*************************************************************

Eine Musterliste zu obigm Beispiel ist die Liste "Affenzahn"
************************************************************

Es wird zuerst das Hauptstatement ausgefuehrt,
anschliessend wird ein folge-Statement mit dem ersten Dataset
ausgefuehrt :

select zei,txt from  mempos
  where a= %ww% and kun = %xx% and mdn =%yy% and kun_bran2 = %zz%
  order by zei


Aktuelle Restriktionen  :


Alle Felder, die als where-Felder angegeben werden, muessen auch selektiert sein, sonst 
bringt dr70001 entsprechende fehlermeldungen
Tabellen- und Feldnamen sind casesensitiv, d.h muessen genau so wie in der Datenbank,
im Allgemeinen wohl komplett klein geschrieben werden

Datentypen in der where-Klausel muessen zusammen passen 