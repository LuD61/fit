create table form_t
(
 delstatus smallint,
 form_nr  char(10),
 lila     smallint,
 tab_nam  char(18),
 feld_nam char(18),

 ttab_nam char(18),

 sfeld1   char(18),
 sfeld2   char(18),
 sfeld3   char(18),
 sfeld4   char(18),
 sfeld5   char(18),

 wtab1    char(18),
 wtab2    char(18),
 wtab3    char(18),
 wtab4    char(18),
 wtab5    char(18),

 wfeld1   char(18),
 wfeld2   char(18),
 wfeld3   char(18),
 wfeld4   char(18),
 wfeld5   char(18),

 ofeld1   char(18),
 ofeld2   char(18),
 ofeld3   char(18),
 ofeld4   char(18),
 ofeld5   char(18),
 odesc1 smallint,
 odesc2 smallint,
 odesc3 smallint,
 odesc4 smallint,
 odesc5 smallint
 )  in fit_dat extent size 32 next size 256 lock mode row ;
 create index i01form_t on form_t ( form_nr,lila,tab_nam,feld_nam );

 create table form_te
 (
 form_nr  char(10),
 lila     smallint,
 tab_nam  char(18),
 feld_nam char(18),
 zei integer,
 krz_txt char(50)
 )  in fit_dat extent size 32 next size 256 lock mode row ;

 create index i01form_te on form_te ( form_nr,lila,tab_nam,feld_nam );
