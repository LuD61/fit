#include "stdafx.h"
#include "DbClass.h"

short DB_CLASS::ShortNull = (short) 0x8000;
long DB_CLASS::LongNull = (long) 0x80000000;
double DB_CLASS::DoubleNull = (double) 0xffffffffffffffff;
CString DB_CLASS::mainname;
HENV DB_CLASS::mainhenv = NULL;
HDBC DB_CLASS::mainhdbc = NULL;


BOOL DB_CLASS::opendbase (char *dbase, int mitmysql )
{
     name = dbase;

     int retcode = SQLAllocEnv(&henv);
     if (retcode == SQL_SUCCESS); 
	 else
	 {
             GetError (NULL);
             return FALSE;
	 }
     DBase.Name = name;
     DBase.henv = henv;

     retcode = SQLAllocConnect(henv, &hdbc); /* Connection handle */

     if (retcode == SQL_SUCCESS); 
	 else
	 {
             GetError (NULL);
             return FALSE;
	 }
        /* Connect to data source */

     retcode = SQLConnect(hdbc, (UCHAR *) dbase, SQL_NTS, 
		                        (UCHAR *) "", SQL_NTS, 
								(UCHAR *) "", SQL_NTS);
     DBase.hdbc = hdbc;
	 mainname = name;
	 mainhenv = henv;
	 mainhdbc = hdbc;
	 OwnConnect = TRUE;
     if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO);
     else
     {
             GetError (NULL);
             return FALSE;
     }
     sqlstatus = 0;
	 if ( mitmysql == 0 )	// 190814 
		sqlcomm ("set isolation to dirty read");  
     return TRUE;
}


BOOL DB_CLASS::closedbase (char *dbase)
/**
Datenbank schliessen.
**/

{
	 if (OwnConnect == FALSE)
	 {
		 return FALSE;
	 }

	 if (hdbc == mainhdbc)
	 {
		 closedbase ();
	 }
	 else
	 {
        if (hdbc != NULL)
		{
              SQLDisconnect(hdbc);
              SQLFreeConnect(hdbc);
		}
        if (hdbc != NULL)
		{
              SQLFreeEnv(henv);
		}
	 }
     hdbc = NULL;
     henv = NULL;
     return TRUE;
}


BOOL DB_CLASS::closedbase ()
/**
Datenbank schliessen.
**/

{
	 if (mainhdbc != NULL)
	 {
           SQLDisconnect(mainhdbc);
           SQLFreeConnect(mainhdbc);
	 }
	 if (mainhenv != NULL)
	 {
           SQLFreeEnv(mainhenv);
	 }
     mainhdbc = NULL;
     mainhenv = NULL;
     return TRUE;
}

int DB_CLASS::sqlconnect (char *server, char *user, char *passw)
{

     int retcode = SQLAllocEnv(&henv);
     if (retcode == SQL_SUCCESS); 
	 else
	 {
		     printf ("Fehler bei SQLAllocEnv\n");
		     exit (1);
	 }
     retcode = SQLAllocConnect(henv, &hdbc); /* Connection handle */

     if (retcode == SQL_SUCCESS); 
	 else
	 {
		     printf ("Fehler bei SQLAllocConnect\n");
		     exit (1);
	 }
     return (0);
}

// 190814 : mitmysql dazu
int DB_CLASS::sqlconnectdbase (char *server, char *user, char *passw, char *dbase, int mitmysql)
{
     opendbase (dbase ,mitmysql);
     return sqlstatus;
}

int DB_CLASS::beginwork (void)
{
    if (InWork)
    {
        commitwork ();
    }
    sqlcomm ("begin work");
    InWork = TRUE;
    return 0;
}

int DB_CLASS::commitwork (void)
{
    if (InWork)
    {
        sqlcomm ("commit work");
//        SQLTransact (henv, hdbc, SQL_COMMIT);
        InWork = FALSE;
    }
    return 0;
}

int DB_CLASS::rollbackwork (void)
{
    if (InWork)
    {
        sqlcomm ("rollback work");
//        SQLTransact (henv, hdbc, SQL_ROLLBACK);
        InWork = FALSE;
    }
    return 0;
}

int DB_CLASS::sqlcomm (char *statement)
{
	 int retcode = SQLAllocStmt (hdbc, &hstmDirect);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
		     return -1;
	 }

     retcode = TestOut (hstmDirect);
     retcode = TestIn (hstmDirect);

     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
		     return -1;
	 }
     retcode = SQLExecDirect((HSTMT) hstmDirect, 
		                  (unsigned char *) statement,
						  (SDWORD) strlen ((char *) statement));
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
     {
             GetError (hstmDirect);
             return -1;
     }

     CString St = statement;
     St.MakeUpper ();
     if (St.Find ("SELECT") > -1)
     {
             retcode = SQLFetch(hstmDirect); 
             if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) 
             {
	                 sqlstatus = 0; 
             }
             else if (retcode == SQL_NO_DATA)
             {
                     sqlstatus = 100;
             }
             else
             { 
                    GetError (hstmDirect);
                    return -1;  
             }
     }
     return sqlstatus;
}

int DB_CLASS::sqlcursor (char *statement)
{
    int cursor;

    for (cursor  = 0; cursor < MAXCURS; cursor ++)
    {
        if (CursTab[cursor] == 0)
        {
            CursTab[cursor] = 1;
            break;
        }
    }

	 int retcode = SQLAllocStmt (hdbc, &HstmtTab[cursor]);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (NULL);
             CursTab[cursor] = 0;
		     return -1;
	 }

     HSTMT Cursor = HstmtTab[cursor];
     retcode = SQLPrepare(Cursor, 
		                  (UCHAR *)statement,
						  (SDWORD) strlen ((char *) statement));
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (Cursor);
             CursTab[cursor] = 0;
		     return -1;
	 }

     retcode = TestIn (Cursor);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (Cursor);
             CursTab[cursor] = 0;
		     return -1;
	 }

     retcode = TestOut (Cursor);

     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (Cursor);
             CursTab[cursor] = 0;
		     return -1;
	 }
     return cursor;
}



int DB_CLASS::sqlopen (int cursor)
{
     if (cursor < 0)
     {
         return -1;
     }

     if (CursTab[cursor] == 0)
     {
         return -1;
     }

     HSTMT Cursor = HstmtTab[cursor];


  	 int retcode = SQLFreeStmt (Cursor, SQL_CLOSE);
     if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
     {
                 sqlstatus = 0;
     } 
     else
     {
                 GetError (Cursor);
 		         sqlstatus = 0 - retcode;
                 return -1;
     }


     retcode = SQLExecute(Cursor); 
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
     {
                 GetError (Cursor);
  		         sqlstatus = 0 - retcode;
                 return -1;
     } 

     CursTab[cursor] = 2;
     return sqlstatus;
}


int DB_CLASS::sqlfetch (int cursor)
{
 
        int dsqlstatus;
        int retcode;

        if (cursor < 0)
        {
             return -1;
        }

        if (CursTab[cursor] == 0)
        { 
             return -1;
        }

        if (CursTab [cursor] == 1)
        {
                    sqlopen (cursor);
        }
        HSTMT Cursor = HstmtTab[cursor];

        retcode = SQLFetch(Cursor); 
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) 
        {
	           sqlstatus = 0; 
        }
        else if (retcode == SQL_NO_DATA)
        {
                 sqlstatus = 100;
        }
        else
        {
                 GetError (Cursor);
                 return -1;  
        }
        dsqlstatus = sqlstatus;

        return sqlstatus;
}


int DB_CLASS::sqlexecute (int cursor)
/**
Cursor oeffnen.
**/
{
        if (cursor < 0)
        {
             return -1;
        }

        if (CursTab[cursor] == 0)
        { 
             return -1;
        }

        HSTMT Cursor = HstmtTab[cursor];

  	    int retcode = SQLFreeStmt (Cursor, SQL_CLOSE);
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
		{
                 sqlstatus = 0;
		} 
        else
		{
                 GetError (Cursor);
 		         sqlstatus = 0 - retcode;
                 return -1;
		}
        retcode = SQLExecute(Cursor); 
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
        {
                 sqlstatus = 0;
        } 
	    else
        {
                 GetError (Cursor);
                 return -1;
        }
        return sqlstatus;
}


int DB_CLASS::sqlclose (int cursor)
/**
Speicherbereiche fuer sqlerte freigeben.
**/
{
     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
         return -1;
     }

     if (CursTab[cursor] == 0 ) return 0 ;

     HSTMT Cursor = HstmtTab[cursor];
     SQLFreeStmt(Cursor, SQL_DROP);
	 CursTab[cursor] = 0;
     return (0);
}



void DB_CLASS::sqlout (void *var, int typ, int len)
{
     if (OutAnz == MAXVARS)
     {
         return;
     }

     OutVars[OutAnz].var = var;
     OutVars[OutAnz].len = len;
     OutVars[OutAnz].typ = typ;
     OutAnz ++;
}


void DB_CLASS::sqlin (void *var, int typ, int len)
{
     if (InAnz == MAXVARS)
     {
         return;
     }

     InVars[InAnz].var = var;
     InVars[InAnz].len = len;
     InVars[InAnz].typ = typ;
     InAnz ++;
}


static SDWORD pcbValue;

int DB_CLASS::TestOut (HSTMT Cursor)
{
     int retcode = 0;

     for (int i = 0; i < OutAnz; i ++)
     {
         switch (OutVars[i].typ)
         {
                 case SQLCHAR:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_CHAR,  
                                         (char *) OutVars[i].var, 
                                                  OutVars[i].len, 
                                                  &pcbValue); 
                        break;
                 case SQLSHORT:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_SHORT,  
                                         (short *) OutVars[i].var, 
                                                   sizeof (short), 
                                                   &pcbValue); 
                        break;
                 case SQLLONG:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_LONG,  
                                         (long *) OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 
                        break;
                 case SQLDOUBLE:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_DOUBLE,  
                                         (double *)  OutVars[i].var, 
                                                     sizeof (double), 
                                                     &pcbValue); 
                        break;
                 case SQLDATE:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_DATE,  
                                         (long *)  OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 
                        break;
				 case SQLTIMESTAMP:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_TIMESTAMP,  
                                         (long *)  OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 

                        break;


         }
     }
     OutAnz = 0;
     return retcode;
}


int DB_CLASS::TestIn (HSTMT Cursor)
{
     int retcode = 0;

     for (int i = 0; i < InAnz; i ++)
     {
         switch (InVars[i].typ)
         {
                 case SQLCHAR:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_CHAR, SQL_CHAR, InVars[i].len, 0,  
                                         (char *) InVars[i].var, 
                                                  0, 
                                                  &cbLen); 
                        break;
                 case SQLSHORT:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_SSHORT, SQL_SMALLINT, 0, 0,  
                                         (short *) InVars[i].var, 
                                                   sizeof (short), 
                                                   &cbLen); 
                        break;
                 case SQLLONG:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_SLONG, SQL_INTEGER, 0, 0,  
                                         (long *) InVars[i].var, 
                                                   0, 
                                                   &cbLen); 
                        break;
                 case SQLDOUBLE:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_DOUBLE, SQL_DOUBLE, 0, 0,   
                                         (double *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 
                        break;
                 case SQLDATE:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_DATE, SQL_DATE, 0, 0,   
                                         (DATE_STRUCT *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 
                        break;
                 case SQLTIMESTAMP:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_TIMESTAMP, SQL_TIMESTAMP, 0, 0,   
                                         (TIMESTAMP_STRUCT *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 

         }
     }
     InAnz = 0;
     return retcode;
}


void DB_CLASS::GetError	(HSTMT Cursor)
{ 
     UCHAR szSqlState[512];
     SDWORD pfNativeError;
     UCHAR szErrorMsg [512]; 

     int retcode = SQLError(henv, 
                             hdbc, 
                             Cursor, 
                             szSqlState, &pfNativeError,
                             szErrorMsg, 512, NULL);
     CString ErrText;
     ErrText.Format ("Fehler %ld\n%s", pfNativeError, szErrorMsg);
 
     if (sql_mode == 0)
     {
            MessageBox (NULL, ErrText, "", MB_ICONERROR);
            ErrText = szErrorMsg;
            (*SqlErrorProc) (pfNativeError, ErrText);
     }
}

BOOL DB_CLASS::ErrProc	(SDWORD ErrStatus, CString& ErrText)
{
    ExitProcess (ErrStatus);
    return TRUE;
}

int DB_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
             prepare ();
         }
         sqlopen (cursor);
         sqlfetch (cursor);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int DB_CLASS::dbread (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         sqlfetch (cursor);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int DB_CLASS::dbupdate (void)
/**
Tabelle eti Updaten.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         sqlopen (test_upd_cursor);
         sqlfetch (test_upd_cursor);
         if (sqlstatus == 100)
         {
                   sqlexecute (ins_cursor);
         }  
         else if (sqlstatus == 0)
         {
                   sqlexecute (upd_cursor);
         }  
          
         return sqlstatus;
} 

int DB_CLASS::dblock (void)
/**
Tabelle eti Updaten.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         sqlopen (test_upd_cursor);
         sqlfetch (test_upd_cursor);
          
         return sqlstatus;
} 

int DB_CLASS::dbdelete (void)
/**
Tabelle eti lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         sqlopen (test_upd_cursor);
         sqlfetch (test_upd_cursor);
         if (sqlstatus == 0)
         {
                      sqlexecute (del_cursor);
         }
         return sqlstatus;
}

void DB_CLASS::dbclose (void)
/**
Cursor fuer eti schliessen.
**/
{
         if (cursor == -1) return;

         sqlclose (cursor); 
         sqlclose (upd_cursor); 
         sqlclose (ins_cursor); 
         sqlclose (del_cursor); 
         sqlclose (test_upd_cursor);

         cursor = -1;
         upd_cursor = -1;
         ins_cursor = -1;
         del_cursor = -1;
         test_upd_cursor = -1;
         cursor_ausw = -1;
}

int DB_CLASS::dbmove (int mode)
/**
Scroll-Cursor lesen.
**/
{
         int scrollakt;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }

         scrollakt = scrollpos;
         switch (mode)
         {
             case FIRST :
                         scrollpos = 1;
                         break;
             case NEXT :     
                         scrollpos ++;
                         break;
             case PRIOR :
                         if (scrollpos > 1)
                         {
                                 scrollpos --;
                         }
                         break;
//             case LAST :      
             case CURRENT :
                         break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode);
         if (sqlstatus != 0)
         {
             scrollpos = scrollakt;
         }
         return (sqlstatus);
}

int DB_CLASS::dbmove (int mode, int pos)
/**
Scroll-Cursor lesen.
**/
{
         int scrollakt;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }
         scrollakt = scrollpos;
         switch (mode)
         {
             case DBABSOLUTE :      
             case RELATIVE :      
                    break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode, pos);
         if (sqlstatus != 0)
         {
             scrollpos = scrollakt;
         }
         return (sqlstatus);
}

int DB_CLASS::dbcanmove (int mode)
/**
Scroll-Cursor testen.
**/
{
         int status; 

         if (cursor_ausw == -1)
         {
                    return (-1);
         }

         switch (mode)
         {
             case FIRST :
                         scrollpos = 1;
                         break;
             case NEXT :     
                         scrollpos ++;
                         break;
             case PRIOR :
                         if (scrollpos > 1)
                         {
                                 scrollpos --;
                         }
                         break;
//             case LAST :      
             case CURRENT :
                         break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode);
         if (sqlstatus != 0)
         {
             status = FALSE;
         }
         else
         {
             status = TRUE;
         }
         dbmove (DBABSOLUTE, scrollpos);
         return (status);
}

int DB_CLASS::dbcanmove (int mode, int pos)
/**
Scroll-Cursor lesen.
**/
{
         int status;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }
         switch (mode)
         {
             case DBABSOLUTE :      
             case RELATIVE :      
                    break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode, pos);
         if (sqlstatus != 0)
         {
             status = FALSE;
         }
         else
         {
             status = TRUE;
         }
         dbmove (DBABSOLUTE, scrollpos);
         return (status);
}

