#include "stdafx.h"
#include "DbClass.h"
#include "reporech.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif
struct REPORZU reporzu, reporzu_null, reporzu1, reporzu2,reporzu3,reporzu4,reporzu5,reporzu6 ;	// 090709 260612 +4+5+6

struct REPORECH reporech, reporech_null;
extern DB_CLASS dbClass;
static long anzzfelder = -1;

int mdn = 1;
int rechnr;
char blgtyp[2] ;



int REPORECH_CLASS::dbcount (void)
/**
Tabelle reprech lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;   //050803
}

int REPORZU_CLASS::lesereporzu ()
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}


int REPORECH_CLASS::lesereporech (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int REPORZU_CLASS::openreporzu (void)
{

	if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

int REPORECH_CLASS::openreporech (void)
{
         return dbClass.sqlopen (readcursor);
}

void REPORECH_CLASS::prepare (void)
{


	dbClass.sqlin ((long *) &rechnr, SQLLONG, 0);
	dbClass.sqlin ((char *) blgtyp, SQLCHAR, 2);
	dbClass.sqlin ((long *) &mdn, SQLLONG, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = dbClass.sqlcursor ("select count(*) from reporech "
										"where reporech.rech_nr = ? and reporech.blg_typ = ? "
										"and reporech.mdn = ? ");
										
	test_upd_cursor = 1;


	dbClass.sqlin ((long *) &rechnr, SQLLONG, 0);
	dbClass.sqlin ((char *) blgtyp, SQLCHAR, 2);
	dbClass.sqlin ((long *) &mdn, SQLLONG, 0);

// 150605 : lsnr dazu gebaut


    dbClass.sqlout ((double *) &reporech.a,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reporech.a_bz1,SQLCHAR,25);
    dbClass.sqlout ((char *) reporech.a_bz2,SQLCHAR,25);
    dbClass.sqlout ((long *) &reporech.lpos_txt,SQLLONG,0);
    dbClass.sqlout ((long *) &reporech.kopf_txt,SQLLONG,0);
    dbClass.sqlout ((long *) &reporech.fuss_txt,SQLLONG,0);
    dbClass.sqlout ((char *) reporech.a_kun,SQLCHAR,14);
    dbClass.sqlout ((char *) reporech.zu_stoff,SQLCHAR,3);
    dbClass.sqlout ((double *) &reporech.gn_pkt_gbr,SQLDOUBLE,0);
    dbClass.sqlout ((long *) &reporech.adr2,SQLLONG,0);
    dbClass.sqlout ((long *) &reporech.adr3,SQLLONG,0);
    dbClass.sqlout ((double *) &reporech.snetto1,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.snetto2,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.snetto3,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.smwst1,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.smwst2,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.smwst3,SQLDOUBLE,0);
    dbClass.sqlout ((char *) reporech.smwst1p,SQLCHAR,7);
	dbClass.sqlout ((char *) reporech.smwst2p,SQLCHAR,7);
	dbClass.sqlout ((char *) reporech.smwst3p,SQLCHAR,7);
	dbClass.sqlout ((short *) &reporech.smwst1s,SQLSHORT,0);
	dbClass.sqlout ((short *) &reporech.smwst2s,SQLSHORT,0);
	dbClass.sqlout ((short *) &reporech.smwst3s,SQLSHORT,0);
    dbClass.sqlout ((double *) &reporech.rech_summ,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.end_rab,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.zahl_betr,SQLDOUBLE,0);
    dbClass.sqlout ((long *) &reporech.fil,SQLLONG,0);
	dbClass.sqlout ((char *) reporech.belk_txt,SQLCHAR,81);	// 61->81 am 300106
	dbClass.sqlout ((char *) reporech.belf_txt,SQLCHAR,81);	// 61->81 am 300106
	dbClass.sqlout ((char *) reporech.zako_txt,SQLCHAR,81);	// 61->81 am 300106
	dbClass.sqlout ((char *) reporech.pmwsts,SQLCHAR,3);
    dbClass.sqlout ((long *) &reporech.kun,SQLLONG,0);
    dbClass.sqlout ((double *) &reporech.ges_rabp,SQLDOUBLE,0);
    dbClass.sqlout ((long *) &reporech.mdn,SQLLONG,0);
	dbClass.sqlout ((char *) reporech.ust_id,SQLCHAR,17);
	dbClass.sqlout ((char *) reporech.nr_bei_rech,SQLCHAR,17);
    dbClass.sqlout ((double *) &reporech.alt_pr,SQLDOUBLE,0);
	dbClass.sqlout ((char *) reporech.lsret,SQLCHAR,2);
	dbClass.sqlout ((char *) reporech.auf_ext,SQLCHAR,31);	// 270212 17->31
	dbClass.sqlout ((short *) &reporech.a_typ,SQLSHORT,0);
	dbClass.sqlout ((char *) reporech.kun_nam,SQLCHAR,46);
	dbClass.sqlout ((char *) reporech.bbn,SQLCHAR,17);
    dbClass.sqlout ((long *) &reporech.mdnadr,SQLLONG,0);
	dbClass.sqlout ((char *) reporech.rech_dat,SQLCHAR,12);
    dbClass.sqlout ((short *) &reporech.order3,SQLSHORT,0);
    dbClass.sqlout ((long *) &reporech.rech_nr,SQLLONG,0);
	dbClass.sqlout ((char *) reporech.erf_kz,SQLCHAR,2);
    dbClass.sqlout ((double *) &reporech.lief_me,SQLDOUBLE,0);
	dbClass.sqlout ((char *) reporech.lief_me_bz,SQLCHAR,7);
    dbClass.sqlout ((double *) &reporech.vk_pr,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.auf_me,SQLDOUBLE,0);
	dbClass.sqlout ((char *) reporech.sa_kz,SQLCHAR,2);
	dbClass.sqlout ((short *) &reporech.order2,SQLSHORT,0);
    dbClass.sqlout ((double *) &reporech.einz_rabp,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.zeil_sum,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporech.prab_wert,SQLDOUBLE,0);
    dbClass.sqlout ((long *) &reporech.rpos_txt,SQLLONG,0);
    dbClass.sqlout ((long *) &reporech.ktx_jebel,SQLLONG,0);
    dbClass.sqlout ((long *) &reporech.ftx_jebel,SQLLONG,0);
	dbClass.sqlout ((short *) &reporech.lief_art,SQLSHORT,0);
	dbClass.sqlout ((char *)  reporech.tel,SQLCHAR,21);
	dbClass.sqlout ((char *)  reporech.fax,SQLCHAR,21);
	dbClass.sqlout ((char *)  reporech.auf_me_bz,SQLCHAR,7);
    dbClass.sqlout ((double *) &reporech.lief_me1,SQLDOUBLE,0);
	dbClass.sqlout ((char *)  reporech.blg_typ,SQLCHAR,2);
	dbClass.sqlout ((char *)  reporech.frm_name,SQLCHAR,6);
	dbClass.sqlout ((char *)  reporech.akv,SQLCHAR,11);
	dbClass.sqlout ((char *)  reporech.ls_charge,SQLCHAR,31);
	dbClass.sqlout ((long *)  &reporech.lsnr,SQLLONG,0 );
	dbClass.sqlout ((long *)  &reporech.txt_rech,SQLLONG,0 );	// 101014

            readcursor = dbClass.sqlcursor ("select "


		
	" a, a_bz1, a_bz2, lpos_txt, kopf_txt, fuss_txt, a_kun, zu_stoff, gn_pkt_gbr "
	" ,adr2, adr3, snetto1, snetto2, snetto3, smwst1, smwst2, smwst3 "

    " ,reporech.smwst1p, reporech.smwst2p, reporech.smwst3p, reporech.smwst1s "
	" ,reporech.smwst2s, reporech.smwst3s, reporech.rech_summ, reporech.end_rab "
	" ,reporech.zahl_betr, reporech.fil, reporech.belk_txt, reporech.belf_txt "
	" ,reporech.zako_txt, reporech.pmwsts, reporech.kun, reporech.ges_rabp "
	" ,reporech.mdn, reporech.ust_id, reporech.nr_bei_rech, reporech.alt_pr "
    
	" ,lsret, auf_ext, a_typ, kun_nam, bbn, mdnadr, rech_dat, order3 "

	" ,reporech.rech_nr, reporech.erf_kz, reporech.lief_me, reporech.lief_me_bz "
    " ,reporech.vk_pr, reporech.auf_me, reporech.sa_kz, reporech.order2 "
	" ,reporech.einz_rabp, reporech.zeil_sum, reporech.prab_wert, reporech.rpos_txt "
	" ,reporech.ktx_jebel, reporech.ftx_jebel, reporech.lief_art, reporech.tel "
	" ,reporech.fax, reporech.auf_me_bz, reporech.lief_me1, reporech.blg_typ "
	" ,reporech.frm_name, reporech.akv, reporech.ls_charge, reporech.lsnr "
	" ,reporech.txt_rech "
	" from reporech where "
	" reporech.rech_nr = ? and reporech.blg_typ = ? and reporech.mdn = ? "
	" order by order3 " ) ;


}

void REPORZU_CLASS::prepare (void)
{


	dbClass.sqlin ((long *) &rechnr, SQLLONG, 0);
	dbClass.sqlin ((char *) blgtyp, SQLCHAR, 2);
	dbClass.sqlin ((long *) &mdn, SQLLONG, 0);


	dbClass.sqlout ((short *) &reporzu.order2,SQLSHORT,0);
    dbClass.sqlout ((char *)   reporzu.typ,SQLCHAR, 3 );
    dbClass.sqlout ((char *)   reporzu.gruppe,SQLCHAR, 7 );
    dbClass.sqlout ((char *)   reporzu.wertart,SQLCHAR, 2 );

    dbClass.sqlout ((double *) &reporzu.me,SQLDOUBLE,0);
    dbClass.sqlout ((double *) &reporzu.faktor,SQLDOUBLE,0);
	dbClass.sqlout ((char *)   reporzu.rab_bz,SQLCHAR,37);
    dbClass.sqlout ((double *) &reporzu.geswert,SQLDOUBLE,0);

	readcursor = dbClass.sqlcursor ("select "
		
	" order2, typ, gruppe, wertart "
	" , me, faktor, rab_bz, geswert "
	" from reporzu where "
	" reporzu.rech_nr = ? and reporzu.blg_typ = ? and reporzu.mdn = ? "
	" order by order2 " ) ;

}
