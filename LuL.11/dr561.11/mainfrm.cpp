//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
// einige Infos zu der Adressiererei :
// Adressen werden entsprechend Eintrag in 94800 bestueckt ( d.h. 0,1,2 oder 3 Adressen )
// zu unterdrueckende Adressen werden mit -2 bestueckt
// d.h.: falls adr2==adr3 steht in adr2 nur noch eine -2 drin 

// rtab.adr => +adr1	// Mandantenadresse, nur eingetragen, falls obige Bedingungen erfuellt sind
// rtab.adr2 => +adr2	// nur eingetragen, falls obige Bedingungen erfuellt sind
// rtab.adr3 => +adr3	// sollte ( fast ) immer bestueckt sein, ausser bei Adressanzahl == 0  

// Adresse 3 : das ist die rechungsadresse 
// Adresse 2 : das ist die Lieferadresse
// Adresse1 : das ist die Mandantenadresse

//  Module:  LLMFC - List & Label MFC Sample Application
//
// Diese Applikation z.Z. nur f�r Rechnungsdruck
//	- - - - >>> (Tabelle reporech wird ausgelesen und komplettiert)
// Erstellung : 20.10.2003  GrJ

// folgende Belegtypen gibt es neuerdings :
// "R" ,"G" ,"B"
// Zusaetzlich fuer die Angebote : "L" : Ursprung war ein LS 
//									"N" : aNgebot als Quelle ( angk/angp ..... )
// Pos-Texte werden aus angpt / angp_txt geholt
//									"U" : aUftrag als Quelle ( aufk/aufp ..... )
// Pos-Texte werden aus aufpt / aufp_txt geholt

// eine m�gliche Aufrufsequenz ist z.B. : dr561 -mdn 1 -formtyp 5610000 -blgtyp R
// der letzte Parameter darf ohne Wert sein, dann wird defualt genutzt,
// ansonsten MUSS die Reihenfolge Parameter Werte Parameter Wert ... eingehalten werden,
// sonst sind LZF im Programmverlauf auf Grund sinnarmer Paramterkombis m�glich  

// Liste alle Parameter 

// -datei 
// ->> nach diesem Parameter wird der Inhalt der Parameterdatei mit entsprecheden Eintr�gen genutzt

// -mdn
// -rechnr
// -blgtyp
// -formtyp 561000X

// -nutzer
// -wahl
// -silent
// duplikat
// -medium

//=============================================================================

// mainfrm.cpp : implementation of the CMainFrame class

#include "stdafx.h"
#include "llmfc.h"
#include "mainfrm.h"
// 110506 #include "wmask.h"
#include "DbClass.h"
#include "reporech.h"
#include "adr.h"
#include "txt.h"
#include "kun.h"
#include "zustab.h"			// 221104 
#include "Token.h"
#include "a_bas.h"			// 070208
#include "rech.h"	// 291010
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GR: Waehlen Sie hier die List & Label Sprache f�r den Designer und die
//     Dialoge aus :
//    (Die entsprechenden Sprachkonstanten entnehmen Sie der Datei cmbtl9.h)

// US: choose your LuL-Language for all designer, dialogs, etc... here:
//    (see cmbtl9.h for other language-constants)

const int LUL_LANGUAGE = CMBTLANG_GERMAN;
const int CTEXTMAX = 512;	// Das entspricht mindestens 7 (7*70=490) ,meist aber viel mehr Zeilen 
const int LEERMATDIM = 21 ;	// 041206 : 20 Felder

static char FILENAME_DEFAULT[] = "c:\\user\\fit\\format\\LL\\561000.lst ........................................................" ; 

// Nettorechnung ohne Rabatt
bool cfgOK;

static char Nutzer[65];
static char LNutzer[65];
static char Listenname[512];
char Internformat[10] ;		// 081204
char erstfolge[5] ;			// 081204
char ohnemitrab[5] ;		// 081204
char partymdn[5] ;			// 041206
char szTempt[CTEXTMAX + 5];

// 300608 : Optimizer A
char szkTempt[CTEXTMAX + 5];	// Rechungskopftext
long szknummer = -1 ; 
char szfTempt[CTEXTMAX + 5];	// Rechnungsfusstext
long szfnummer = -1 ; 
char szLTempt[CTEXTMAX + 5];	// kun.ls_fuss_txt
long szLnummer = -1 ; 
char szRTempt[CTEXTMAX + 5];	// kun.rech_fuss_txt
long szRnummer = -1 ; 
// 300608 : Optimizer E


// 041206 : Dimension von 10 auf 20 erweitert

static double leer_mat_a  [LEERMATDIM];
static long   leer_mat_sz [LEERMATDIM];
static long   leer_mat_sa [LEERMATDIM];
static long   leer_mat_s  [LEERMATDIM];
static double leer_mat_pr [LEERMATDIM];
static char * leer_mat_bz [LEERMATDIM];

static char leer_mat_bz1 [37] ;
static char leer_mat_bz2 [37] ;
static char leer_mat_bz3 [37] ;
static char leer_mat_bz4 [37] ;
static char leer_mat_bz5 [37] ;
static char leer_mat_bz6 [37] ;
static char leer_mat_bz7 [37] ;
static char leer_mat_bz8 [37] ;
static char leer_mat_bz9 [37] ;
static char leer_mat_bz10[37] ;

static char leer_mat_bz11 [37] ;
static char leer_mat_bz12 [37] ;
static char leer_mat_bz13 [37] ;
static char leer_mat_bz14 [37] ;
static char leer_mat_bz15 [37] ;
static char leer_mat_bz16 [37] ;
static char leer_mat_bz17 [37] ;
static char leer_mat_bz18 [37] ;
static char leer_mat_bz19 [37] ;
static char leer_mat_bz20 [37] ;


int TESTTEST = 0 ;

int Listenauswahl = 0;   // wenn 1 : Auswahl mehrerer Reports
int Hauptmenue = 0;      // wenn 1 : Auswahl edit oder print �ber Menue
int DrMitMenue = 0;      // wenn 1 : Ausdruck mit diversen Dialogen
int NutzerIgnorieren = 0;	// wenn 1 , dann kein fehler bei fehlendem sub-Dir
int danzahlerlaubt = 0 ;	// 131109 : Falls auf dem Platz anzahlerlaubt, dann Anzahl korrekt auswerten
int danzahl = 1 ;			// 131109 : abwaerts kompatibel = ?!
int EntwurfsModus = 0;   // wenn 1 : Entwurfsmodus: Aufruf ohne Parameter
int WahlModus = 0;		 // wenn 1 : nur Druckerwahl
int wahlmod   = 1;		// 1 = Erstdruck, 2 = Folgedruck; ab 041009 : mehrwertig
int illrefo   = 0;		// 041009
int da_zustxt_wa = 0 ;	// 221104 Systemparameter
int dnachkpreis = 2 ;	// 160905 Systemparameter

int eanausakungx = 0;	// Sondersuperwunsch der Fa. Lackmann 110913			// rtab.a_bas.ean
int eanausakun = 0;		// Schaltbarkeit gleich mit erledigt :  110913			// rtab.ean



// 190814 A
int mitmysql = 0 ;
char myuid[99];
char mypass[99];
char mydsn[99];
// 190814 A

int cfga_kun_txt = 0;	// 200214
int cfga_bas_erw = 0;	// 200214

int bar_kreis_par = 0 ;	// 250713
int leerbest2 = 0;		//250713
int dietzosep = 0;	// 231213 

int spessiggrund = 0;	// 031114


static int dparty_mdn = 0 ;	// 041206 
static int party_mdn_aktiv = 0 ;	// 041206
static int dsilent = 0 ;	// 021204 

static int dduplikat = 0 ;	// 240205


extern int mdn;
extern int rechnr;
extern char blgtyp[4] ;
extern int iadr ;


static long wechselblg ;		// 150605 
static char wechseltyp[2]  ;	// 150605

//extern char formtyp[8] ;


int holedatenausdatei(void) ;	
char *get_bws_defa (char *) ;	// 041009
void a_kun_gx_komplett(void);	// 110913


static int datenausdatei ;		// Lesen aus Datei angefordert
static int alternat_ausgabe ;	// Alternativ-Ausgabe ist aktiv
static char datenausdateiname[299] ;

static char alternat_file[299] ;
static char alternat_dir[299] ;
static char alternat_type[99] ;
static char alternat_buchst[19] ;


int irez = 0;

  DB_CLASS dbClass;

  DB_CLASS mydbClass;	// 190814

  REPORECH_CLASS RepoRech;
  REPORZU_CLASS Reporzu ;	// 090709
  ADR_CLASS Adr;

  LSPT_CLASS Lspt;
  AUFPT_CLASS Aufpt;	// 130707
  ANGPT_CLASS Angpt;	// 130707
  RETPT_CLASS Retpt;
  ATEXTE_CLASS Atexte;	// 101014
  AUFPT_VERKFRAGE_CLASS Aufpt_verkfrage;	// 151014

  LS_TXT_CLASS Ls_txt;
  KUN_CLASS Kun;
  LEER_LSDR_CLASS Leer_lsdr;

  KASE_FIT_CLASS Kase_fit;	// 130413 
  TOU_CLASS Tou;	// 301107

  SYS_PAR_CLASS Sys_par;			// 221104
  A_ZUS_TXT_CLASS A_zus_txt;		// 221104
  LSK_CLASS Lsk;
  ANGK_CLASS Angk;					// 130707
  AUFK_CLASS Aufk;					// 130707
  RETK_CLASS Retk;					// 200706
  A_BAS_CLASS A_bas;				// 070208

  PTABN_CLASS Ptabn ;				// 110506

  A_KUN_CLASS a_kun_class;			// 290910
  A_KUN_GX_CLASS a_kun_gx_class;	// 110913

  A_HNDW_CLASS a_hndw_class;		// 110608
  A_EIG_CLASS a_eig_class;			// 110608
  A_EIG_DIV_CLASS a_eig_div_class;	// 110608


  A_KUN_TXT_CLASS A_kun_txt;		// 200214
  A_BAS_ERW_CLASS A_bas_erw;		// 200214

  ASPRACHE_CLASS Asprache;		// 200214

  RECH_CLASS Rech;					// 291010	

struct PTABN ptabn, ptabn_null ;

HWND hMainWindow;

int form_feld_curs, tabid_curs, coltyp_curs,  dynacurs ; 
/////////////////////////////////////////////////////////////////////////////
// CMainFrame

// GR: Registrieren eines Nachrichtenkanals f�r List & Label Callbacks
// US: register LuL MessageBase:
UINT CMainFrame::m_uLuLMessageBase = RegisterWindowMessage("cmbtLLMessage");

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_START_DEBUG, OnStartDebug)
	ON_COMMAND(ID_FILE_STOP_DEBUG, OnStopDebug)
	ON_COMMAND(ID_EDIT_LABEL, OnEditLabel)
	ON_COMMAND(ID_EDIT_LIST, OnEditList)
	ON_UPDATE_COMMAND_UI(ID_FILE_START_DEBUG, OnUpdateStartDebug)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP_DEBUG, OnUpdateStopDebug)
	ON_COMMAND(ID_PRINT_LABEL, OnPrintLabel)
	ON_COMMAND(ID_PRINT_REPORT, OnPrintReport)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(m_uLuLMessageBase,OnLulMessage)
END_MESSAGE_MAP()

char eanstring[20] ;	// 110913


char *clippedi (char *string)
{
  char *clstring;
  short i,len;

  len = (short) strlen (string);

  if (len == 0) return (string);
  len --;
  clstring = string;
 // 201010 damit werden auch Leerstrings korekt gekappt 
  for (i = len; i >= 0; i --)
  {
    if ((unsigned char) clstring[i] > 0x20)
    {
       break;
    }
  }
  clstring [i + 1] = 0;

  clstring = string;
  len = (short)strlen (clstring);

  for (i = 0; i < len; i ++, clstring +=1)
  {
     if ((unsigned char) *clstring > (unsigned char) 0X20)
     {
         break;
     }
  }
  return (clstring);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{


	m_bDebug = FALSE;
//    ProgCfg = new PROG_CFG ("dr561");


}

CMainFrame::~CMainFrame()
{
/* ---->
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
< ------ */

}


/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	/* ----->
	LlAddCtlSupport(m_hWnd,
					LL_CTL_ADDTOSYSMENU | LL_CTL_CONVERTCONTROLS | LL_CTL_ALSOCHILDREN,
					"combit.ini");
< ----- */

	GetCfgValues();

	InterpretCommandLine();
	if ( EntwurfsModus)
	{   DrMitMenue = 1;
		Hauptmenue = 1 ;
		Listenauswahl = 1 ;
	}

	if ( WahlModus )
	{
		DrMitMenue = 1 ;
		Hauptmenue = 0 ;
		Listenauswahl = 0 ;
	}


	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_PRINT_REPORT,0l);
	}

return 0;
}

void CMainFrame::OnStartDebug()
{
	MessageBox("Make sure that DEBWIN2 had been started before this demo application. "
				"If this doesn't happen you won't see any debug outputs now!",
				"List & Label Fakdruck App", MB_OK | MB_ICONINFORMATION);
//	LlSetDebug(LL_DEBUG_CMBTLL);
	m_bDebug = TRUE;

}

void CMainFrame::OnStopDebug()
{
	LlSetDebug(FALSE);
	m_bDebug = FALSE;
}

void CMainFrame::OnUpdateStopDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bDebug);
}

void CMainFrame::OnUpdateStartDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bDebug);
}


//=============================================================================
void CMainFrame::OnEditLabel()
//=============================================================================
{
	CHAR szFilename[128+1] = "unendlich lange ...........................                                                                  *.lbl";
	HWND hWnd = m_hWnd;
	HJOB hJob;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	// US: initialize Job
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

// OnEditLabel

	// GR: Setzen der List & Label Optionen
	// US: Setting the List & Label options
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	//GR: Exporter aktivieren
	//US: Enable exporter
#ifdef LL19
// am besten alles weg ...	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll19ex.llx");
	int v = LlSetFileExtensions(hJob, LL_PROJECT_LIST, "lst", "lsp", "lsv");
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll12ex.llx");
#else
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");
#endif
#endif
	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
/* ---> GrJ GrJ
	if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
    {
        LlJobClose(hJob);
        return;
    }
< ---- */

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

// OnEditLabel

	// GR: Aufruf des Designers


	

   	sprintf ( szFilename, FILENAME_DEFAULT );

 	if (LlDefineLayout(hJob, hWnd, "Designer", LL_PROJECT_LABEL, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
// OnEditLabel
}


int GetRecCount()  //#LuD
{
	return RepoRech.dbcount ();
}

void init_leer_mat(void)
{
	int ipo = 0 ;	// Portierung 120210 
	for ( ipo = 0 ; ipo < LEERMATDIM ; ipo ++ )
	{
		leer_mat_a[ipo]  = 0.0;
		leer_mat_sz[ipo] = 0L ;
		leer_mat_sa[ipo] = 0L ;
		leer_mat_s[ipo]  = 0L ;
		leer_mat_pr[ipo] = 0.0;
	}

	leer_mat_bz[0] =  leer_mat_bz1  ;
	leer_mat_bz[1] =  leer_mat_bz2  ;
	leer_mat_bz[2] =  leer_mat_bz3  ;
	leer_mat_bz[3] =  leer_mat_bz4  ;
	leer_mat_bz[4] =  leer_mat_bz5  ;
	leer_mat_bz[5] =  leer_mat_bz6  ;
	leer_mat_bz[6] =  leer_mat_bz7  ;
	leer_mat_bz[7] =  leer_mat_bz8  ;
	leer_mat_bz[8] =  leer_mat_bz9  ;
	leer_mat_bz[9] =  leer_mat_bz10 ;

	leer_mat_bz[10] =  leer_mat_bz11  ;
	leer_mat_bz[11] =  leer_mat_bz12  ;
	leer_mat_bz[12] =  leer_mat_bz13  ;
	leer_mat_bz[13] =  leer_mat_bz14  ;
	leer_mat_bz[14] =  leer_mat_bz15  ;
	leer_mat_bz[15] =  leer_mat_bz16  ;
	leer_mat_bz[16] =  leer_mat_bz17  ;
	leer_mat_bz[17] =  leer_mat_bz18  ;
	leer_mat_bz[18] =  leer_mat_bz19  ;
	leer_mat_bz[19] =  leer_mat_bz20  ;

	leer_mat_bz1[0] =  '\0'  ;
	leer_mat_bz2[0] =  '\0'  ;
	leer_mat_bz3[0] =  '\0'  ;
	leer_mat_bz4[0] =  '\0'  ;
	leer_mat_bz5[0] =  '\0'  ;
	leer_mat_bz6[0] =  '\0'  ;
	leer_mat_bz7[0] =  '\0'  ;
	leer_mat_bz8[0] =  '\0'  ;
	leer_mat_bz9[0] =  '\0'  ;
	leer_mat_bz10[0]=  '\0'  ;

	leer_mat_bz11[0] =  '\0'  ;
	leer_mat_bz12[0] =  '\0'  ;
	leer_mat_bz13[0] =  '\0'  ;
	leer_mat_bz14[0] =  '\0'  ;
	leer_mat_bz15[0] =  '\0'  ;
	leer_mat_bz16[0] =  '\0'  ;
	leer_mat_bz17[0] =  '\0'  ;
	leer_mat_bz18[0] =  '\0'  ;
	leer_mat_bz19[0] =  '\0'  ;
	leer_mat_bz20[0] =  '\0'  ;

// 140307
	if ((kun.kun_leer_kz > 2) && ( kun.sam_rech == 1 ))
	{

// 250713 : Werner-Problem dazu : Parameter leerbest2 soll aktiv sein und der Kunde ist
		// auf sam_rech = 1 eingestellt, es wird jedoch absichtlich ein barverk. erzeugt
		// oder der Kunde steht ausserdem noch auf kun_typ = 6 , dann kollidieren die Nummernkreise
		leer_lsdr.mdn = (short) reporech.mdn ;

		if ( reporech.blg_typ[0] == 'B' && ! bar_kreis_par && leerbest2 )
			sprintf ( leer_lsdr.blg_typ ,"f") ;
		else
			sprintf ( leer_lsdr.blg_typ ,"F") ;

		leer_lsdr.ls = reporech.rech_nr ;
		Leer_lsdr.openleer_lsdr() ;

		ipo = 0 ;
		while ( ! Leer_lsdr.leseleer_lsdr()&& ipo < LEERMATDIM - 1 )
		{
			leer_mat_a[ipo] = leer_lsdr.a ;
			leer_mat_s[ipo] = leer_lsdr.stk ;
			ipo ++ ;
		}
	}
}

void laderabaz( void)
{
	int nummer ;
	nummer = 0 ;
	memcpy ( &reporzu1, &reporzu_null, sizeof(struct REPORZU )) ;
	memcpy ( &reporzu2, &reporzu_null, sizeof(struct REPORZU )) ;
	memcpy ( &reporzu3, &reporzu_null, sizeof(struct REPORZU )) ;
	memcpy ( &reporzu4, &reporzu_null, sizeof(struct REPORZU )) ;	// 260612
	memcpy ( &reporzu5, &reporzu_null, sizeof(struct REPORZU )) ;	// 260612
	memcpy ( &reporzu6, &reporzu_null, sizeof(struct REPORZU )) ;	// 260612

	int di = Reporzu.openreporzu() ;
	
	while ( !di  && nummer < 6 )	// 260612 3->6
	{
		di = Reporzu.lesereporzu() ;
		if ( di ) break ;
		if ( nummer == 0 )	// 260612  dieser case fehlte einfach .....
			memcpy ( &reporzu1, &reporzu, sizeof(struct REPORZU )) ;
		if ( nummer == 1 )
			memcpy ( &reporzu2, &reporzu, sizeof(struct REPORZU )) ;
		if ( nummer == 2 )
			memcpy ( &reporzu3, &reporzu, sizeof(struct REPORZU )) ;
		if ( nummer == 3 )	// 260612 dazu bis zur 6.
			memcpy ( &reporzu4, &reporzu, sizeof(struct REPORZU )) ;
		if ( nummer == 4 )
			memcpy ( &reporzu5, &reporzu, sizeof(struct REPORZU )) ;
		if ( nummer == 5 )
			memcpy ( &reporzu6, &reporzu, sizeof(struct REPORZU )) ;
		nummer ++ ;
	}
}

void lade_leer_mat(void)
{

	int ipo ;

	short nreferenz ;	// 041206 
	double save_a, save_pr;
	long save_sz, save_sa, save_s;
	char save_bz[37] ;

	ipo = 0 ;

	nreferenz = 11 ;	// Leergut
	if ( party_mdn_aktiv )	// Leihartikel 
		nreferenz = 12 ;

	if (reporech.a_typ == nreferenz )
	{
		for ( ipo = 0 ; ipo < LEERMATDIM ; ipo ++ )
		{
			if ( leer_mat_a[ipo]  == 0.0 ) break ;
			if ( leer_mat_a[ipo]  == reporech.a ) break ;
		}
		if ( ipo > LEERMATDIM - 1 ) return ;	// Matrix voll-Pech gehabt

		if ( leer_mat_a[ipo]  == reporech.a)
		{

			if ( reporech.lief_me > 0 )
			{
				if ( reporech.lsret[0] == 'L' )
					leer_mat_sz[ipo] += (long)reporech.lief_me ;
				else
					leer_mat_sa[ipo] += (long)reporech.lief_me ;

			}
			else
			{
				if ( reporech.lsret[0] == 'L' )
					leer_mat_sa[ipo] -= (long)reporech.lief_me ;
				else
					leer_mat_sz[ipo] -= (long)reporech.lief_me ;
			}

			// 140307 : Namen nachladen

			if (strlen(clippedi(leer_mat_bz[ipo])) < 2 )
			{
				if (ipo == 0 )sprintf ( leer_mat_bz1,"%s", reporech.a_bz1)  ;
				if (ipo == 1 )sprintf ( leer_mat_bz2,"%s", reporech.a_bz1)  ;
				if (ipo == 2 )sprintf ( leer_mat_bz3,"%s", reporech.a_bz1)  ;
				if (ipo == 3 )sprintf ( leer_mat_bz4,"%s", reporech.a_bz1)  ;
				if (ipo == 4 )sprintf ( leer_mat_bz5,"%s", reporech.a_bz1)  ;
				if (ipo == 5 )sprintf ( leer_mat_bz6,"%s", reporech.a_bz1)  ;
				if ( ipo < 6 ) return ;
				if (ipo == 6 )sprintf ( leer_mat_bz7,"%s", reporech.a_bz1)  ;
				if (ipo == 7 )sprintf ( leer_mat_bz8,"%s", reporech.a_bz1)  ;
				if (ipo == 8 )sprintf ( leer_mat_bz9,"%s", reporech.a_bz1)  ;
				if (ipo == 9 )sprintf ( leer_mat_bz10,"%s", reporech.a_bz1) ;
				if ( ipo < 10 ) return ;
				if (ipo == 10 )sprintf ( leer_mat_bz11,"%s", reporech.a_bz1)  ;
				if (ipo == 11 )sprintf ( leer_mat_bz12,"%s", reporech.a_bz1)  ;
				if (ipo == 12 )sprintf ( leer_mat_bz13,"%s", reporech.a_bz1)  ;
				if (ipo == 13 )sprintf ( leer_mat_bz14,"%s", reporech.a_bz1)  ;
				if (ipo == 14 )sprintf ( leer_mat_bz15,"%s", reporech.a_bz1)  ;
				if (ipo == 15 )sprintf ( leer_mat_bz16,"%s", reporech.a_bz1)  ;
				if (ipo == 16 )sprintf ( leer_mat_bz17,"%s", reporech.a_bz1)  ;
				if (ipo == 17 )sprintf ( leer_mat_bz18,"%s", reporech.a_bz1)  ;
				if (ipo == 18 )sprintf ( leer_mat_bz19,"%s", reporech.a_bz1)  ;
				if (ipo == 19 )sprintf ( leer_mat_bz20,"%s", reporech.a_bz1)  ;
			}
			return ;
		}

		leer_mat_a[ipo] = reporech.a ;
		leer_mat_pr[ipo] = reporech.vk_pr ;

		if ( reporech.lief_me > 0 )
		{
			if ( reporech.lsret[0] == 'L' )
				leer_mat_sz[ipo] = (long)reporech.lief_me ;
			else
				leer_mat_sa[ipo] = (long)reporech.lief_me ;
		}

		else
		{
			if ( reporech.lsret[0] == 'L' )
				leer_mat_sa[ipo] = 0L - (long)reporech.lief_me ;
			else
				leer_mat_sz[ipo] = 0L - (long)reporech.lief_me ;
		}
		
		if (ipo == 0 )sprintf ( leer_mat_bz1,"%s", reporech.a_bz1)  ;
		if (ipo == 1 )sprintf ( leer_mat_bz2,"%s", reporech.a_bz1)  ;
		if (ipo == 2 )sprintf ( leer_mat_bz3,"%s", reporech.a_bz1)  ;
		if (ipo == 3 )sprintf ( leer_mat_bz4,"%s", reporech.a_bz1)  ;
		if (ipo == 4 )sprintf ( leer_mat_bz5,"%s", reporech.a_bz1)  ;
		if (ipo == 5 )sprintf ( leer_mat_bz6,"%s", reporech.a_bz1)  ;
		if (ipo == 6 )sprintf ( leer_mat_bz7,"%s", reporech.a_bz1)  ;
		if (ipo == 7 )sprintf ( leer_mat_bz8,"%s", reporech.a_bz1)  ;
		if (ipo == 8 )sprintf ( leer_mat_bz9,"%s", reporech.a_bz1)  ;
		if (ipo == 9 )sprintf ( leer_mat_bz10,"%s", reporech.a_bz1) ;
// 041206 
		if (ipo == 10 )sprintf ( leer_mat_bz11,"%s", reporech.a_bz1)  ;
		if (ipo == 11 )sprintf ( leer_mat_bz12,"%s", reporech.a_bz1)  ;
		if (ipo == 12 )sprintf ( leer_mat_bz13,"%s", reporech.a_bz1)  ;
		if (ipo == 13 )sprintf ( leer_mat_bz14,"%s", reporech.a_bz1)  ;
		if (ipo == 14 )sprintf ( leer_mat_bz15,"%s", reporech.a_bz1)  ;
		if (ipo == 15 )sprintf ( leer_mat_bz16,"%s", reporech.a_bz1)  ;
		if (ipo == 16 )sprintf ( leer_mat_bz17,"%s", reporech.a_bz1)  ;
		if (ipo == 17 )sprintf ( leer_mat_bz18,"%s", reporech.a_bz1)  ;
		if (ipo == 18 )sprintf ( leer_mat_bz19,"%s", reporech.a_bz1)  ;
		if (ipo == 19 )sprintf ( leer_mat_bz20,"%s", reporech.a_bz1)  ;

	// Es folgt die Sortierei, Matrix ist immer sortiert ausser evtl. dem neuen Element

		while ( ipo > 0 )
		{
			if ( leer_mat_a[ipo] > leer_mat_a[ipo - 1] )  break ;

			save_a  = leer_mat_a[ ipo - 1] ;
			save_sz = leer_mat_sz[ipo - 1] ;
			save_sa = leer_mat_sa[ipo - 1] ;
			save_s  = leer_mat_s[ ipo - 1] ;
			save_pr = leer_mat_pr[ipo - 1] ;
			sprintf ( save_bz, "%s", leer_mat_bz[ipo - 1] ) ; 


			leer_mat_a[ ipo - 1] = leer_mat_a[ ipo] ;
			leer_mat_sz[ipo - 1] = leer_mat_sz[ipo] ;
			leer_mat_sa[ipo - 1] = leer_mat_sa[ipo] ;
			leer_mat_s[ ipo - 1] = leer_mat_s[ ipo] ;
			leer_mat_pr[ipo - 1] = leer_mat_pr[ipo] ;;
			sprintf ( leer_mat_bz[ipo - 1], "%s", leer_mat_bz[ipo] ) ; 


			leer_mat_a[ ipo] = save_a    ;
			leer_mat_sz[ipo] = save_sz   ;
			leer_mat_sa[ipo] = save_sa ;
			leer_mat_s[ ipo] = save_s  ;
			leer_mat_pr[ipo] = save_pr ;
			sprintf ( leer_mat_bz[ipo], "%s", save_bz ) ; 

			ipo-- ;
		}
	}
}

void Felderdefinition (HJOB hJob)
{

// 040805 : Leergut-Felder einbauen
	char szTemp3[30] ;
    int ipo ;
	// 041206 : 10 -> LEERMATDIM
	for ( ipo = 1 ; ipo < LEERMATDIM ; ipo ++ )
	{
	    sprintf(szTemp3, "leer_rh.a%.0d", ipo );
		LlDefineFieldExt(hJob, szTemp3     , "4711", LL_NUMERIC, NULL);

		sprintf(szTemp3, "leer_rh.a_bz%.0d", ipo );
		LlDefineFieldExt(hJob, szTemp3  , szTemp3, LL_TEXT, NULL);

		sprintf(szTemp3, "leer_rh.stk_zu%.0d", ipo );
		LlDefineFieldExt(hJob, szTemp3, "10", LL_NUMERIC, NULL);

		sprintf(szTemp3, "leer_rh.stk_ab%.0d", ipo );
		LlDefineFieldExt(hJob, szTemp3, "11", LL_NUMERIC, NULL);

		sprintf(szTemp3, "leer_rh.stk%.0d", ipo );
		LlDefineFieldExt(hJob, szTemp3   , "12", LL_NUMERIC, NULL);

		sprintf(szTemp3, "leer_rh.pr_vk%.0d", ipo );
		LlDefineFieldExt(hJob, szTemp3 , "13,14", LL_NUMERIC, NULL);

    }

	LlDefineFieldExt(hJob, "leer_rh.kun_leer_kz", "2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "leer_rh.leih_aktiv", "3", LL_NUMERIC, NULL);	// 041206


	LlDefineFieldExt(hJob, "adr1.plz_pf","PF-12345", LL_TEXT, NULL);	//	080709
    LlDefineFieldExt(hJob, "adr1.pf","adr1.pf", LL_TEXT, NULL);			//	080709
	LlDefineFieldExt(hJob, "kun.adr1.plz_pf","PF-12345", LL_TEXT, NULL);	//	011014
    LlDefineFieldExt(hJob, "kun.adr1.pf","adr1.pf", LL_TEXT, NULL);			//	011014

// 120413 A
	LlDefineFieldExt(hJob, "kase_fit.kasse","123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "kase_fit.bon","76543", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "kase_fit.betrag_bto","123.22", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "kase_fit.gegeben","76543.34", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "kase_fit.zurueck","123.22", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "kase_fit.gut_sum","76543.11", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "kase_fit.essengut_sum","123,22", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "kase_fit.gut_anzahl","76543", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "kase_fit.essengut_anzahl","76543", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "kase_fit.zahlart","1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "kase_fit.bon_date","12.12.2013", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kase_fit.pers_nam","Krause", LL_TEXT, NULL);


// 120413 E

// 200214 A
	if ( cfga_kun_txt )
	{
		LlDefineFieldExt(hJob, "a_kun_txt.txt1","txt1", LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_kun_txt.txt2","txt2", LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_kun_txt.txt3","txt3", LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_kun_txt.txt4","txt4", LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_kun_txt.txt5","txt5", LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_kun_txt.sort","1", LL_NUMERIC, NULL);
	}

// 190814
	if ( mitmysql )
	{
		LlDefineFieldExt(hJob, "asprache.sprache","0", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "asprache.a_bz2","a_bz2",     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "asprache.a_bz1","a_bz1",     LL_TEXT, NULL);
// 251114
		LlDefineFieldExt(hJob, "asprache2.sprache","0", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "asprache2.a_bz2","a_bz2",     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "asprache2.a_bz1","a_bz1",     LL_TEXT, NULL);

	}
	if ( cfga_bas_erw )
	{
		LlDefineFieldExt(hJob, "a_bas_erw.pp_a_bz1","pp_a_bz1", LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.pp_a_bz2","pp_a_bz2",     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.lgr_tmpr","lgr_tmpr",     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.lupine","lupine",       LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.schutzgas","schutzgas",    LL_TEXT, NULL);
 
		LlDefineFieldExt(hJob, "a_bas_erw.huelle","1",       LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.shop_wg1","1",     LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.shop_wg2","2",     LL_NUMERIC, NULL);	//  smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.shop_wg3","3",     LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.shop_wg4","4",     LL_NUMERIC, NULL);	// smallint,

		LlDefineFieldExt(hJob, "a_bas_erw.shop_wg5","5",     LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.tara2","1.23",        LL_NUMERIC, NULL);	// decimal(8,3),
		LlDefineFieldExt(hJob, "a_bas_erw.a_tara2","2",      LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.tara3","1.23",        LL_NUMERIC, NULL);	// decimal(8,3),
		LlDefineFieldExt(hJob, "a_bas_erw.a_tara3","3",      LL_NUMERIC, NULL);	// smallint,

		LlDefineFieldExt(hJob, "a_bas_erw.tara4","2.34",        LL_NUMERIC, NULL);	// decimal(8,3),
		LlDefineFieldExt(hJob, "a_bas_erw.a_tara4","4",      LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.salz","4.34",         LL_NUMERIC, NULL);	// decimal(8,3),
		LlDefineFieldExt(hJob, "a_bas_erw.davonfett","1.23",    LL_NUMERIC, NULL);	// decimal(8,3),
		LlDefineFieldExt(hJob, "a_bas_erw.davonzucker","1.234",  LL_NUMERIC, NULL);	// decimal(8,3),

		LlDefineFieldExt(hJob, "a_bas_erw.ballaststoffe","1.234",LL_NUMERIC, NULL);	// decimal(8,3),
		LlDefineFieldExt(hJob, "a_bas_erw.shop_aktion","J",  LL_TEXT, NULL);	// char(1),
		LlDefineFieldExt(hJob, "a_bas_erw.shop_neu","N",     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.shop_tv","N",      LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.shop_agew","3.234" ,   LL_NUMERIC, NULL);	// decimal(8,3),

		LlDefineFieldExt(hJob, "a_bas_erw.a_bez","a_bez",        LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.zutat","zutat",        LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.userdef1","1",     LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.userdef2","2",     LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.userdef3","3",     LL_NUMERIC, NULL);	// smallint,

		LlDefineFieldExt(hJob, "a_bas_erw.minstaffgr","123",   LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.l_pack","1000",       LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.b_pack","2000",       LL_NUMERIC, NULL);	// smallint,
		LlDefineFieldExt(hJob, "a_bas_erw.h_pack","3000",       LL_NUMERIC, NULL);	// smallint

	}

// 200214 E

	
// 071108 A

	LlDefineFieldExt(hJob, "adr1.plz","X-12345", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr1.anr","adr1.anr", LL_TEXT, NULL);	// 091208
    LlDefineFieldExt(hJob, "adr1.adr_nam1","adr1.adr_nam1", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr1.adr_nam2","adr1.adr_nam2", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr1.adr_nam3","adr1.adr_nam3", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr1.iln","adr1.iln", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr1.ort1","adr1.ort1", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr1.str","adr1.str", LL_TEXT, NULL);

	LlDefineFieldExt(hJob, "adr1.ort2","adr1.ort2", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr1.tel","adr1.tel", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr1.fax","adr1.fax", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr1.mobil","adr1.mobil", LL_TEXT, NULL);	// 301111
	LlDefineFieldExt(hJob, "adr1.iban","adr1.iban", LL_TEXT, NULL);	// 300813
	LlDefineFieldExt(hJob, "adr1.swift","adr1.swift", LL_TEXT, NULL);	// 300813
    LlDefineFieldExt(hJob, "adr1.partner","adr1.partner", LL_TEXT, NULL);
	
	LlDefineFieldExt(hJob, "adr1.staattyp","A", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr1.staatkz","NL", LL_TEXT, NULL);

// 031114 A
	LlDefineFieldExt(hJob, "aufpt_verkfrage.def1","1", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.def2","2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.def3","3", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.def4","4", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.def5","5", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.def6","6", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.def7","7", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.def8","8", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.def9","9", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.def10","10", LL_NUMERIC, NULL);

// 151014 A
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls1_t","txt_ls1_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls2_t","txt_ls2_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls3_t","txt_ls3_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls4_t","txt_ls4_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls5_t","txt_ls5_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls6_t","txt_ls6_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls7_t","txt_ls7_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls8_t","txt_ls8_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls9_t","txt_ls9_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls10_t","txt_ls10_t", LL_TEXT, NULL);

	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech1_t","txt_rech1_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech2_t","txt_rech2_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech3_t","txt_rech3_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech4_t","txt_rech4_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech5_t","txt_rech5_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech6_t","txt_rech6_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech7_t","txt_rech7_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech8_t","txt_rech8_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech9_t","txt_rech9_t", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech10_t","txt_rech10_t", LL_TEXT, NULL);

// 151014 E

// 011014
	LlDefineFieldExt(hJob, "kun.adr1.plz","X-12345", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.adr1.anr","adr1.anr", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.adr1.adr_nam1","adr1.adr_nam1", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.adr1.adr_nam2","adr1.adr_nam2", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.adr1.adr_nam3","adr1.adr_nam3", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.adr1.iln","adr1.iln", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.adr1.ort1","adr1.ort1", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.adr1.str","adr1.str", LL_TEXT, NULL);

	LlDefineFieldExt(hJob, "kun.adr1.ort2","adr1.ort2", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.adr1.tel","adr1.tel", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "kun.adr1.fax","adr1.fax", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "kun.adr1.mobil","adr1.mobil", LL_TEXT, NULL);	// 301111
	LlDefineFieldExt(hJob, "kun.adr1.iban","adr1.iban", LL_TEXT, NULL);	// 300813
	LlDefineFieldExt(hJob, "kun.adr1.swift","adr1.swift", LL_TEXT, NULL);	// 300813
    LlDefineFieldExt(hJob, "kun.adr1.partner","adr1.partner", LL_TEXT, NULL);
	
	LlDefineFieldExt(hJob, "kun.adr1.staattyp","A", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.adr1.staatkz","NL", LL_TEXT, NULL);

	LlDefineFieldExt(hJob, "adr2.plz_pf","PF-12345", LL_TEXT, NULL);	//	080709
    LlDefineFieldExt(hJob, "adr2.pf","adr2.pf", LL_TEXT, NULL);			//	080709

    LlDefineFieldExt(hJob, "adr2.plz","X-12345", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr2.anr","adr2.anr", LL_TEXT, NULL);	// 091208
    LlDefineFieldExt(hJob, "adr2.adr_nam1","adr2.adr_nam1", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr2.adr_nam2","adr2.adr_nam2", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr2.adr_nam3","adr2.adr_nam3", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr2.iln","adr2.iln", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr2.ort1","adr2.ort1", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr2.str","adr2.str", LL_TEXT, NULL);

	LlDefineFieldExt(hJob, "adr2.ort2","adr2.ort2", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr2.tel","adr2.tel", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr2.fax","adr2.fax", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr2.mobil","adr2.mobil", LL_TEXT, NULL);	// 301111
	LlDefineFieldExt(hJob, "adr2.iban","adr2.iban", LL_TEXT, NULL);	// 300813
	LlDefineFieldExt(hJob, "adr2.swift","adr2.swift", LL_TEXT, NULL);	// 300813

    LlDefineFieldExt(hJob, "adr2.partner","adr2.partner", LL_TEXT, NULL);
 
	LlDefineFieldExt(hJob, "adr2.staattyp","A", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr2.staatkz","NL", LL_TEXT, NULL);
	
	LlDefineFieldExt(hJob, "adr3.plz_pf","PF-12345", LL_TEXT, NULL);	//	080709
    LlDefineFieldExt(hJob, "adr3.pf","adr3.pf", LL_TEXT, NULL);			//	080709

    LlDefineFieldExt(hJob, "adr3.plz","X-12345", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr3.anr","adr3.anr", LL_TEXT, NULL);	// 091208
    LlDefineFieldExt(hJob, "adr3.adr_nam1","adr3.adr_nam1", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr3.adr_nam2","adr3.adr_nam2", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr3.adr_nam3","adr3.adr_nam3", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr3.iln","adr3.iln", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr3.ort1","adr3.ort1", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr3.str","adr3.str", LL_TEXT, NULL);

	LlDefineFieldExt(hJob, "adr3.ort2","adr3.ort2", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr3.tel","adr3.tel", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr3.fax","adr3.fax", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr3.mobil","adr3.mobil", LL_TEXT, NULL);	// 301111
	LlDefineFieldExt(hJob, "adr3.iban","adr3.iban", LL_TEXT, NULL);	// 300813
	LlDefineFieldExt(hJob, "adr3.swift","adr3.swift", LL_TEXT, NULL);	// 300813

    LlDefineFieldExt(hJob, "adr3.partner","adr3.partner", LL_TEXT, NULL);

	LlDefineFieldExt(hJob, "adr3.staattyp","A", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "adr3.staatkz","NL", LL_TEXT, NULL);

// 071108 E

	LlDefineFieldExt(hJob, "rtab.a_bas.a_gew", "1.123", LL_NUMERIC, NULL);	// 070208
	LlDefineFieldExt(hJob, "rtab.a_bas.me_einh", "1", LL_NUMERIC, NULL);	// 070208


	LlDefineFieldExt(hJob, "rtab.a_bas.ag", "12", LL_NUMERIC, NULL);	// 190310
	LlDefineFieldExt(hJob, "rtab.a_bas.wg", "13", LL_NUMERIC, NULL);	// 190310
	LlDefineFieldExt(hJob, "rtab.a_bas.hwg", "14", LL_NUMERIC, NULL);	// 190310
	LlDefineFieldExt(hJob, "rtab.a_bas.teil_smt", "15", LL_NUMERIC, NULL);	// 190310



	LlDefineFieldExt(hJob, "rtab.a", "47114711", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.Nutzer","rtab.Nutzer", LL_TEXT, NULL);	// 060905
    LlDefineFieldExt(hJob, "rtab.nachkpreis","2", LL_NUMERIC, NULL);	// 160905
    LlDefineFieldExt(hJob, "rtab.a_bz1","rtab.a_bz1", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.a_bz2","rtab.a_bz2", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.lpos_txt", "11", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.kopf_txt", "12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.fuss_txt", "13", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.txt_rech","14", LL_NUMERIC, NULL);	// 101014


    LlDefineFieldExt(hJob, "rtab.txt_rech_t", "atexte-Postext", LL_TEXT, NULL);	// 101014
    LlDefineFieldExt(hJob, "rtab.lpos_txt_t", "Postext", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.kopf_txt_t", "Kopftext", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.fuss_txt_t", "Fusstext", LL_TEXT, NULL);

// 120504
    LlDefineFieldExt(hJob, "rtab.ktx_jebel_t", "Kopftext je Beleg", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.ftx_jebel_t", "Fusstext je Beleg", LL_TEXT, NULL);

//	300608 
	LlDefineFieldExt(hJob, "kun.ls_fuss_txt_t", "Kunden-LS-fusstext", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "kun.rech_fuss_txt_t", "Kunden-RECH-fusstext", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.ls_fuss_txt", "12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "kun.rech_fuss_txt", "13", LL_NUMERIC, NULL);

// 251113
	LlDefineFieldExt(hJob, "kun.zahl_ziel", "365", LL_NUMERIC, NULL);

// 231213 
	if ( !dietzosep )
	{
		LlDefineFieldExt(hJob, "kun.tagesepa", "366", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "kun.mandatref", "mandat_ref_nr", LL_TEXT, NULL);
	}

//	030912 
	LlDefineFieldExt(hJob, "kun.zahl_art", "3", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "kun.bank_kun", "Kundenmustersparbank", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.blz", "12345678", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "kun.kto_nr", "0034512345", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "kun.sprache", "0", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "kun.sprache2", "0", LL_NUMERIC, NULL);	// 251114

	LlDefineFieldExt(hJob, "kun.kun_gr1", "1", LL_NUMERIC, NULL );	// 051210
	LlDefineFieldExt(hJob, "kun.kun_gr2", "2", LL_NUMERIC, NULL );	// 051210
	LlDefineFieldExt(hJob, "kun.kun_krz1", "kun_krz1", LL_TEXT, NULL );	// 211010
	LlDefineFieldExt(hJob, "kun.kun_bran2", "13", LL_TEXT, NULL );	// 220910
	LlDefineFieldExt(hJob, "rtab.a_bas.ean", "4012345678901", LL_TEXT, NULL );	// 290910

	LlDefineFieldExt(hJob, "rtab.teil_smt","0", LL_NUMERIC, NULL);	// 291010

// 050213
	LlDefineFieldExt(hJob, "rtab.auf","123", LL_NUMERIC, NULL);
// 201207 
	LlDefineFieldExt(hJob, "rtab.tour","123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.tour_bez","Tour-Bez", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.lpers_nam","LS-Pers-nam", LL_TEXT, NULL);	// 131113
    LlDefineFieldExt(hJob, "rtab.apers_nam","Auf-Pers-nam", LL_TEXT, NULL);	// 281113
    
	LlDefineFieldExt(hJob, "rtab.a_kun","rtab.a_kun", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rtab.ean","4012345678901", LL_NUMERIC, NULL);	// 110913


    LlDefineFieldExt(hJob, "rtab.zu_stoff","ZS", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.gn_pkt_gbr", "0.04", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.adr2", "4711", LL_NUMERIC, NULL);		// Lieferadresse oder -2 
    LlDefineFieldExt(hJob, "rtab.adr3", "4712", LL_NUMERIC, NULL);		// Rechnungsadresse
    LlDefineFieldExt(hJob, "rtab.snetto1", "10.00", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.snetto2", "20.00", LL_NUMERIC, NULL);;
    LlDefineFieldExt(hJob, "rtab.snetto3", "30.00", LL_NUMERIC, NULL);;
    LlDefineFieldExt(hJob, "rtab.smwst1", "0.70", LL_NUMERIC, NULL);;
    LlDefineFieldExt(hJob, "rtab.smwst2", "3.20", LL_NUMERIC, NULL);;
    LlDefineFieldExt(hJob, "rtab.smwst3", "3.90", LL_NUMERIC, NULL);;
    LlDefineFieldExt(hJob, "rtab.smwst1p","7,0 %", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.smwst2p","16,00 %", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.smwst3p","13,00 %", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.smwst1s", "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.smwst2s", "2", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.smwst3s", "3", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.rech_summ", "40.00", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.end_rab", "8.00", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.zahl_betr", "43.00", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.fil", "128", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.belk_txt","rtab_belk_txt", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.belf_txt","rtab_belf_txt", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.zako_txt","rtab_zako_txt", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.pmwsts","A", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.kun", "1010", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.ges_rabp", "14.00", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.mdn", "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.ust_id","Eigen-Ust_ID", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.nr_bei_rech","rtab_nr_bei_rech", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.alt_pr", "12.99", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "rtab.lsret", "L", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rtab.lsnr", "1234", LL_NUMERIC, NULL);	// 290609
	LlDefineFieldExt(hJob, "rtab.auf_ext","rtab_auf_ext", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.a_typ", "2.0", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.kun_nam","rtab_kun_nam", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.bbn","rtab_bbn", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.mdnadr", "1", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.rech_dat","21.07.2003", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.order3", "3", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.rech_nr", "123456", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.erf_kz","H", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.lief_me", "12.345", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.lief_me_bz","kg", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.vk_pr", "12.345", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.auf_me", "45.321", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.sa_kz","*", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.order2", "2", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.einz_rabp", "3.3", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.zeil_sum", "23.45", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.prab_wert", "1.23", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.rpos_txt","99", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.ktx_jebel", "123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.ftx_jebel", "321", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.lief_art", "5", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.tel","rtab.tel", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.fax","rtab.fax", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.auf_me_bz","Stck", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.lief_me1", "23.456", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "rtab.red_inh", "2.0", LL_NUMERIC, NULL);	// 110508
    LlDefineFieldExt(hJob, "rtab.ls_charge","Chargen-Nr.", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.kunsteunum","rtab_kunsteunum", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "rtab.kunfak_kz","J", LL_TEXT, NULL);	// 250806
    LlDefineFieldExt(hJob, "rtab.kunfak_nr","12345678", LL_NUMERIC, NULL);	// 250806

    LlDefineFieldExt(hJob, "rtab.silent_pr","0", LL_NUMERIC, NULL);	// 021204
    LlDefineFieldExt(hJob, "rtab.format", "56100", LL_NUMERIC, NULL);	// 081204
    LlDefineFieldExt(hJob, "rtab.blg_typ", "R", LL_TEXT, NULL);	// 010507
    LlDefineFieldExt(hJob, "rtab.ohnemitrab", "0", LL_NUMERIC, NULL);	// 081204
    LlDefineFieldExt(hJob, "rtab.duplikat", "0", LL_NUMERIC, NULL);	// 240205
    LlDefineFieldExt(hJob, "rtab.hinweis", "Hinweis", LL_TEXT, NULL);	// 150605
	LlDefineFieldExt(hJob, "rtab.lieferzeit", "11:55", LL_TEXT, NULL);	// 201207

// 100709 : Rabatt-Zeilen
	LlDefineFieldExt(hJob, "rabz1.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineFieldExt(hJob, "rabz1.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineFieldExt(hJob, "rabz1.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineFieldExt(hJob, "rabz1.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineFieldExt(hJob, "rabz1.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineFieldExt(hJob, "rabz1.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rabz1.geswert", "22.33", LL_NUMERIC, NULL);

	LlDefineFieldExt(hJob, "rabz2.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineFieldExt(hJob, "rabz2.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineFieldExt(hJob, "rabz2.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineFieldExt(hJob, "rabz2.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineFieldExt(hJob, "rabz2.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineFieldExt(hJob, "rabz2.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rabz2.geswert", "22.33", LL_NUMERIC, NULL);

	LlDefineFieldExt(hJob, "rabz3.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineFieldExt(hJob, "rabz3.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineFieldExt(hJob, "rabz3.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineFieldExt(hJob, "rabz3.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineFieldExt(hJob, "rabz3.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineFieldExt(hJob, "rabz3.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rabz3.geswert", "22.33", LL_NUMERIC, NULL);

// 260612 : mehr Rabatt-Zeilen
	LlDefineFieldExt(hJob, "rabz4.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineFieldExt(hJob, "rabz4.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineFieldExt(hJob, "rabz4.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineFieldExt(hJob, "rabz4.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineFieldExt(hJob, "rabz4.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineFieldExt(hJob, "rabz4.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rabz4.geswert", "22.33", LL_NUMERIC, NULL);

	LlDefineFieldExt(hJob, "rabz5.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineFieldExt(hJob, "rabz5.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineFieldExt(hJob, "rabz5.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineFieldExt(hJob, "rabz5.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineFieldExt(hJob, "rabz5.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineFieldExt(hJob, "rabz5.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rabz5.geswert", "22.33", LL_NUMERIC, NULL);

	LlDefineFieldExt(hJob, "rabz6.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineFieldExt(hJob, "rabz6.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineFieldExt(hJob, "rabz6.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineFieldExt(hJob, "rabz6.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineFieldExt(hJob, "rabz6.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineFieldExt(hJob, "rabz6.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rabz6.geswert", "22.33", LL_NUMERIC, NULL);


}


void Variablendefinition (HJOB hJob)
{

// 040805 : Leergut-Felder einbauen
	char szTemp3[30] ;
	int ipo ;

	// 041206 : 10 -> LEERMATDIM

	for (  ipo = 1 ; ipo < LEERMATDIM ; ipo ++ )
	{
	    sprintf(szTemp3, "leer_rh.a%.0d", ipo );
		LlDefineVariableExt(hJob, szTemp3     , "4711", LL_NUMERIC, NULL);

		sprintf(szTemp3, "leer_rh.a_bz%.0d", ipo );
		LlDefineVariableExt(hJob, szTemp3  , szTemp3, LL_TEXT, NULL);

		sprintf(szTemp3, "leer_rh.stk_zu%.0d", ipo );
		LlDefineVariableExt(hJob, szTemp3, "10", LL_NUMERIC, NULL);

		sprintf(szTemp3, "leer_rh.stk_ab%.0d", ipo );
		LlDefineVariableExt(hJob, szTemp3, "11", LL_NUMERIC, NULL);

		sprintf(szTemp3, "leer_rh.stk%.0d", ipo );
		LlDefineVariableExt(hJob, szTemp3   , "12", LL_NUMERIC, NULL);

		sprintf(szTemp3, "leer_rh.pr_vk%.0d", ipo );
		LlDefineVariableExt(hJob, szTemp3 , "13,14", LL_NUMERIC, NULL);

    }
	LlDefineVariableExt(hJob, "leer_rh.kun_leer_kz", "2", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "leer_rh.leih_aktiv", "3", LL_NUMERIC, NULL);	// 041206

	LlDefineVariableExt(hJob, "adr1.plz_pf","PF-12345", LL_TEXT, NULL);	//	080709
    LlDefineVariableExt(hJob, "adr1.pf","adr1.pf", LL_TEXT, NULL);			//	080709
	LlDefineVariableExt(hJob, "kun.adr1.plz_pf","PF-12345", LL_TEXT, NULL);	//	011014
    LlDefineVariableExt(hJob, "kun.adr1.pf","adr1.pf", LL_TEXT, NULL);			//	011014

// 031114 A
	LlDefineVariableExt(hJob, "aufpt_verkfrage.def1","1", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.def2","2", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.def3","3", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.def4","4", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.def5","5", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.def6","6", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.def7","7", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.def8","8", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.def9","9", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.def10","10", LL_NUMERIC, NULL);

// 151014 A
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls1_t","txt_ls1_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls2_t","txt_ls2_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls3_t","txt_ls3_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls4_t","txt_ls4_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls5_t","txt_ls5_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls6_t","txt_ls6_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls7_t","txt_ls7_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls8_t","txt_ls8_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls9_t","txt_ls9_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls10_t","txt_ls10_t", LL_TEXT, NULL);

	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech1_t","txt_rech1_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech2_t","txt_rech2_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech3_t","txt_rech3_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech4_t","txt_rech4_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech5_t","txt_rech5_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech6_t","txt_rech6_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech7_t","txt_rech7_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech8_t","txt_rech8_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech9_t","txt_rech9_t", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech10_t","txt_rech10_t", LL_TEXT, NULL);

// 151014 E



// 120413 A
	LlDefineVariableExt(hJob, "kase_fit.kasse","123", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "kase_fit.bon","76543", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "kase_fit.betrag_bto","123.22", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "kase_fit.gegeben","76543.34", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "kase_fit.zurueck","123.22", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "kase_fit.gut_sum","76543.11", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "kase_fit.essengut_sum","123,22", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "kase_fit.gut_anzahl","76543", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "kase_fit.essengut_anzahl","76543", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "kase_fit.zahlart","1", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "kase_fit.bon_date","12.12.2013", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kase_fit.pers_nam","Krause", LL_TEXT, NULL);

// 120413 E
    LlDefineVariableExt(hJob, "adr1.plz","X-12345", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr1.anr","adr1.anr", LL_TEXT, NULL);	// 091208
    LlDefineVariableExt(hJob, "adr1.adr_nam1","adr1.adr_nam1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr1.adr_nam2","adr1.adr_nam2", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr1.adr_nam3","adr1.adr_nam3", LL_TEXT, NULL);	// 090804
    LlDefineVariableExt(hJob, "adr1.iln","adr1.iln", LL_TEXT, NULL);	// 150905
    LlDefineVariableExt(hJob, "adr1.ort1","adr1.ort1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr1.str","adr1.str", LL_TEXT, NULL);

// 250708 
	LlDefineVariableExt(hJob, "adr1.ort2","adr1.ort2", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr1.tel","adr1.tel", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "adr1.fax","adr1.fax", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "adr1.mobil","adr1.mobil", LL_TEXT, NULL);	// 301111
	LlDefineVariableExt(hJob, "adr1.iban","adr1.iban", LL_TEXT, NULL);	// 300813
	LlDefineVariableExt(hJob, "adr1.swift","adr1.swift", LL_TEXT, NULL);	// 300813

    LlDefineVariableExt(hJob, "adr1.partner","adr1.partner", LL_TEXT, NULL);
	
// 110506 
	LlDefineVariableExt(hJob, "adr1.staattyp","A", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr1.staatkz","NL", LL_TEXT, NULL);

// 011014
    LlDefineVariableExt(hJob, "kun.adr1.plz","X-12345", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.adr1.anr","adr1.anr", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.adr1.adr_nam1","adr1.adr_nam1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.adr1.adr_nam2","adr1.adr_nam2", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.adr1.adr_nam3","adr1.adr_nam3", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.adr1.iln","adr1.iln", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.adr1.ort1","adr1.ort1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.adr1.str","adr1.str", LL_TEXT, NULL);

	LlDefineVariableExt(hJob, "kun.adr1.ort2","adr1.ort2", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.adr1.tel","adr1.tel", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "kun.adr1.fax","adr1.fax", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "kun.adr1.mobil","adr1.mobil", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "kun.adr1.iban","adr1.iban", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "kun.adr1.swift","adr1.swift", LL_TEXT, NULL);

    LlDefineVariableExt(hJob, "kun.adr1.partner","adr1.partner", LL_TEXT, NULL);
	
	LlDefineVariableExt(hJob, "kun.adr1.staattyp","A", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.adr1.staatkz","NL", LL_TEXT, NULL);

	LlDefineVariableExt(hJob, "adr2.plz_pf","PF-12345", LL_TEXT, NULL);	//	080709
    LlDefineVariableExt(hJob, "adr2.pf","adr2.pf", LL_TEXT, NULL);			//	080709

	LlDefineVariableExt(hJob, "adr2.plz","X-12345", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr2.anr","adr2.anr", LL_TEXT, NULL);	// 091208
    LlDefineVariableExt(hJob, "adr2.adr_nam1","adr2.adr_nam1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr2.adr_nam2","adr2.adr_nam2", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "adr2.adr_nam3","adr2.adr_nam3", LL_TEXT, NULL);	// 090804
    LlDefineVariableExt(hJob, "adr2.iln","adr2.iln", LL_TEXT, NULL);	// 150905
    LlDefineVariableExt(hJob, "adr2.ort1","adr2.ort1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr2.str","adr2.str", LL_TEXT, NULL);
// 250708 A
	LlDefineVariableExt(hJob, "adr2.ort2","adr2.ort2", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr2.tel","adr2.tel", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "adr2.fax","adr2.fax", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "adr2.mobil","adr2.mobil", LL_TEXT, NULL);	// 301111
	LlDefineVariableExt(hJob, "adr2.iban","adr2.iban", LL_TEXT, NULL);	// 300813
	LlDefineVariableExt(hJob, "adr2.swift","adr2.swift", LL_TEXT, NULL);	// 300813

    LlDefineVariableExt(hJob, "adr2.partner","adr2.partner", LL_TEXT, NULL);
// 110506 
	LlDefineVariableExt(hJob, "adr2.staattyp","A", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr2.staatkz","NL", LL_TEXT, NULL);
	
	LlDefineVariableExt(hJob, "adr3.plz_pf","PF-12345", LL_TEXT, NULL);	//	080709
    LlDefineVariableExt(hJob, "adr3.pf","adr3.pf", LL_TEXT, NULL);			//	080709

    LlDefineVariableExt(hJob, "adr3.plz","X-12345", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr3.anr","adr3.anr", LL_TEXT, NULL);	// 091208
    LlDefineVariableExt(hJob, "adr3.adr_nam1","adr3.adr_nam1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr3.adr_nam2","adr3.adr_nam2", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr3.adr_nam3","adr3.adr_nam3", LL_TEXT, NULL);	// 090804
    LlDefineVariableExt(hJob, "adr3.iln","adr3.iln", LL_TEXT, NULL);	// 150905
    LlDefineVariableExt(hJob, "adr3.ort1","adr3.ort1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr3.str","adr3.str", LL_TEXT, NULL);
// 250708 A
	LlDefineVariableExt(hJob, "adr3.ort2","adr3.ort2", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr3.tel","adr3.tel", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "adr3.fax","adr3.fax", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "adr3.mobil","adr3.mobil", LL_TEXT, NULL);	// 301111
	LlDefineVariableExt(hJob, "adr3.iban","adr3.iban", LL_TEXT, NULL);	// 300813
	LlDefineVariableExt(hJob, "adr3.swift","adr3.swift", LL_TEXT, NULL);	// 300813

    LlDefineVariableExt(hJob, "adr3.partner","adr3.partner", LL_TEXT, NULL);

// 110506 
	LlDefineVariableExt(hJob, "adr3.staattyp","A", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "adr3.staatkz","NL", LL_TEXT, NULL);


	LlDefineVariableExt(hJob, "rtab.a_bas.a_gew", "1.123", LL_NUMERIC, NULL);	// 070208
	LlDefineVariableExt(hJob, "rtab.a_bas.me_einh", "2", LL_NUMERIC, NULL);	// 070208

	LlDefineVariableExt(hJob, "rtab.a_bas.ag", "11", LL_NUMERIC, NULL);	// 190310
	LlDefineVariableExt(hJob, "rtab.a_bas.wg", "12", LL_NUMERIC, NULL);	// 190310
	LlDefineVariableExt(hJob, "rtab.a_bas.hwg", "13", LL_NUMERIC, NULL);	// 190310
	LlDefineVariableExt(hJob, "rtab.a_bas.teil_smt", "14", LL_NUMERIC, NULL);	// 190310

	LlDefineVariableExt(hJob, "rtab.a", "47114711", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.Nutzer","rtab.Nutzer", LL_TEXT, NULL);	// 060905
    LlDefineVariableExt(hJob, "rtab.nachkpreis","2", LL_NUMERIC, NULL);	// 160905
    LlDefineVariableExt(hJob, "rtab.a_bz1","rtab.a_bz1", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.a_bz2","rtab.a_bz2", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.lpos_txt", "11", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.kopf_txt", "12", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.fuss_txt", "13", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.txt_rech","14", LL_NUMERIC, NULL);	// 101014


    LlDefineVariableExt(hJob, "rtab.txt_rech_t", "atexte-Postext", LL_TEXT, NULL);	// 101014
    LlDefineVariableExt(hJob, "rtab.lpos_txt_t", "Postext", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.kopf_txt_t", "Kopftext", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.fuss_txt_t", "Fusstext", LL_TEXT, NULL);

	// 120504
    LlDefineVariableExt(hJob, "rtab.ktx_jebel_t", "Kopftext je Beleg", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.ftx_jebel_t", "Fusstext je Beleg", LL_TEXT, NULL);

//	270608 
	LlDefineVariableExt(hJob, "kun.ls_fuss_txt_t", "Kunden-LS-fusstext", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "kun.rech_fuss_txt_t", "Kunden-RECH-fusstext", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.ls_fuss_txt", "12", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "kun.rech_fuss_txt", "13", LL_NUMERIC, NULL);

// 251113
	LlDefineVariableExt(hJob, "kun.zahl_ziel", "365", LL_NUMERIC, NULL);
// 231213 
	if ( !dietzosep )
	{
		LlDefineVariableExt(hJob, "kun.tagesepa", "366", LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "kun.mandatref", "mandat_ref_nr", LL_TEXT, NULL);
	}
//	030912 
	LlDefineVariableExt(hJob, "kun.zahl_art", "3", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "kun.bank_kun", "Kundenmustersparbank", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.blz", "12345678", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "kun.kto_nr", "0034512345", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "kun.sprache", "0", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "kun.sprache2", "0", LL_NUMERIC, NULL);	// 251114

    LlDefineVariableExt(hJob, "kun.kun_gr1", "1", LL_NUMERIC, NULL );	// 051210
    LlDefineVariableExt(hJob, "kun.kun_gr2", "2", LL_NUMERIC, NULL );	// 051210
    LlDefineVariableExt(hJob, "kun.kun_krz1", "kun_krz1", LL_TEXT, NULL );	// 211010
    LlDefineVariableExt(hJob, "kun.kun_bran2", "13", LL_TEXT, NULL );	// 220910
// Quatsch :  LlDefineVariableExt(hJob, "rtab.a_bas.ean", "4012345678901", LL_TEXT, NULL );	// 290910

	LlDefineVariableExt(hJob, "rtab.teil_smt","0", LL_NUMERIC, NULL);	// 291010
//050213
	LlDefineVariableExt(hJob, "rtab.auf","123", LL_NUMERIC, NULL);
// 201207 
	LlDefineVariableExt(hJob, "rtab.tour","123", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.tour_bez","Tour-Bez", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.lpers_nam","LS-Pers-Nam", LL_TEXT, NULL);

    LlDefineVariableExt(hJob, "rtab.a_kun","rtab.a_kun", LL_TEXT, NULL);
// Quatsch : LlDefineVariableExt(hJob, "rtab.ean","4012345678901", LL_NUMERIC, NULL);	// 110913

    LlDefineVariableExt(hJob, "rtab.zu_stoff","ZS", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.gn_pkt_gbr", "0.04", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.adr2", "4711", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.adr3", "4712", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.snetto1", "10.00", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.snetto2", "20.00", LL_NUMERIC, NULL);;
    LlDefineVariableExt(hJob, "rtab.snetto3", "30.00", LL_NUMERIC, NULL);;
    LlDefineVariableExt(hJob, "rtab.smwst1", "0.70", LL_NUMERIC, NULL);;
    LlDefineVariableExt(hJob, "rtab.smwst2", "3.20", LL_NUMERIC, NULL);;
    LlDefineVariableExt(hJob, "rtab.smwst3", "3.90", LL_NUMERIC, NULL);;
    LlDefineVariableExt(hJob, "rtab.smwst1p","7,0 %", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.smwst2p","16,00 %", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.smwst3p","13,00 %", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.smwst1s", "1", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.smwst2s", "2", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.smwst3s", "3", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.rech_summ", "40.00", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.end_rab", "8.00", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.zahl_betr", "43.00", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.fil", "128", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.belk_txt","rtab_belk_txt", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.belf_txt","rtab_belf_txt", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.zako_txt","rtab_zako_txt", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.pmwsts","A", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.kun", "1010", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.ges_rabp", "14.00", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.mdn", "1", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.ust_id","Eigen-Ust_ID", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.nr_bei_rech","rtab_nr_bei_rech", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.alt_pr", "12.99", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.lsret", "L", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.lsnr", "L", LL_NUMERIC, NULL);	// 290609
    LlDefineVariableExt(hJob, "rtab.auf_ext","rtab_auf_ext", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.a_typ", "2.0", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.kun_nam","rtab_kun_nam", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.bbn","rtab_bbn", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.mdnadr", "1", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.rech_dat","21.07.2003", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.order3", "3", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.rech_nr", "123456", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.erf_kz","H", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.lief_me", "12.345", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.lief_me_bz","kg", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.vk_pr", "12.345", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.auf_me", "45.321", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.sa_kz","*", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.order2", "2", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.einz_rabp", "3.3", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.zeil_sum", "23.45", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.prab_wert", "1.23", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.rpos_txt","99", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.ktx_jebel", "123", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.ftx_jebel", "321", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.lief_art", "5", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.tel","rtab.tel", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.fax","rtab.fax", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.auf_me_bz","Stck", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.lief_me1", "23.456", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.red_inh", "2.0", LL_NUMERIC, NULL);	// 110608
    LlDefineVariableExt(hJob, "rtab.ls_charge","Chargen-Nr.", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.kunsteunum","rtab_kunsteunum", LL_TEXT, NULL);
    LlDefineVariableExt(hJob, "rtab.kunfak_kz","J", LL_TEXT, NULL);	// 250806
    LlDefineVariableExt(hJob, "rtab.kunfak_nr","12345678", LL_NUMERIC, NULL);	// 250806
	LlDefineVariableExt(hJob, "rtab.silent_pr","0", LL_NUMERIC, NULL);
    LlDefineVariableExt(hJob, "rtab.format", "56100", LL_NUMERIC, NULL);	// 081204
    LlDefineVariableExt(hJob, "rtab.blg_typ", "R", LL_TEXT, NULL);	// 010507
    LlDefineVariableExt(hJob, "rtab.ohnemitrab", "0", LL_NUMERIC, NULL);	// 081204
    LlDefineVariableExt(hJob, "rtab.duplikat", "0", LL_NUMERIC, NULL);	// 240205
    LlDefineVariableExt(hJob, "rtab.hinweis", "Hinweis", LL_TEXT, NULL);	// 150605
	LlDefineVariableExt(hJob, "rtab.lieferzeit", "11:55", LL_TEXT, NULL);	// 201207

// 100709 : Rabatt-Zeilen
	LlDefineVariableExt(hJob, "rabz1.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineVariableExt(hJob, "rabz1.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineVariableExt(hJob, "rabz1.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineVariableExt(hJob, "rabz1.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineVariableExt(hJob, "rabz1.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineVariableExt(hJob, "rabz1.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rabz1.geswert", "22.33", LL_NUMERIC, NULL);

	LlDefineVariableExt(hJob, "rabz2.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineVariableExt(hJob, "rabz2.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineVariableExt(hJob, "rabz2.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineVariableExt(hJob, "rabz2.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineVariableExt(hJob, "rabz2.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineVariableExt(hJob, "rabz2.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rabz2.geswert", "22.33", LL_NUMERIC, NULL);

	LlDefineVariableExt(hJob, "rabz3.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineVariableExt(hJob, "rabz3.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineVariableExt(hJob, "rabz3.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineVariableExt(hJob, "rabz3.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineVariableExt(hJob, "rabz3.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineVariableExt(hJob, "rabz3.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rabz3.geswert", "22.33", LL_NUMERIC, NULL);

// 260612 : mehr Rabatt-Zeilen
	LlDefineVariableExt(hJob, "rabz4.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineVariableExt(hJob, "rabz4.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineVariableExt(hJob, "rabz4.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineVariableExt(hJob, "rabz4.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineVariableExt(hJob, "rabz4.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineVariableExt(hJob, "rabz4.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rabz4.geswert", "22.33", LL_NUMERIC, NULL);

	LlDefineVariableExt(hJob, "rabz5.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineVariableExt(hJob, "rabz5.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineVariableExt(hJob, "rabz5.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineVariableExt(hJob, "rabz5.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineVariableExt(hJob, "rabz5.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineVariableExt(hJob, "rabz5.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rabz5.geswert", "22.33", LL_NUMERIC, NULL);

	LlDefineVariableExt(hJob, "rabz6.typ", "T", LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	LlDefineVariableExt(hJob, "rabz6.gruppe", "kg", LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	LlDefineVariableExt(hJob, "rabz6.wertart", "W", LL_TEXT, NULL);	// "W"ert oder "P"rozent
	LlDefineVariableExt(hJob, "rabz6.me", "11.55", LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	LlDefineVariableExt(hJob, "rabz6.faktor", "11.55", LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	LlDefineVariableExt(hJob, "rabz6.rab_bz", "Rabattart", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rabz6.geswert", "22.33", LL_NUMERIC, NULL);

}

/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sqldatamger
-
-       In              :       string-amerikanisch "YYYY-MM-DD"
-
-       Out             :       string-germanisch   "DD.MM.JJJJ"
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Umformatieren eines Datumsstrings
-
--------------------------------------------------------------------------------
**/
#ifdef OLD_DCL
char * sqldatamger(     inpstr,        outpstr)
char * inpstr;
char * outpstr;
#else
char * sqldatamger(char *inpstr, char *outpstr)
#endif
{
 /* --->  270907 : so einfach macht es uns das odbc nicht mehr .....
	outpstr[ 0]= inpstr[8];
	outpstr[ 1]= inpstr[9];
	outpstr[ 2]= '.';
	outpstr[ 3]= inpstr[5];
	outpstr[ 4]= inpstr[6];
	outpstr[ 5]= '.';
	outpstr[ 6]= inpstr[0];
	outpstr[ 7]= inpstr[1];
	outpstr[ 8]= inpstr[2];
	outpstr[ 9]= inpstr[3];
	outpstr[10]= '\0';
	return outpstr ;
< ---- */

	if (( inpstr[0] == ' ') || ( inpstr[0] == '\0') ||(inpstr[1] == '\0' ))
	{	// Nullstring handeln ...
		sprintf ( outpstr, "  .  .    " ) ;
		return outpstr  ;
	}
	else
	{
		if ( inpstr[4] == '-' && inpstr[7] == '-' )	// passt fuer y4md- und y4dm-
		{											// wir nehmen halt jetzt mal y4md- an
			// default fit-informix
				outpstr[ 0]= inpstr[8];
				outpstr[ 1]= inpstr[9];
				outpstr[ 3]= inpstr[5];
				outpstr[ 4]= inpstr[6];
/* ---->
		else	// eher hypothetischer Fall : "y4dm-"
			{
				outpstr[ 0]= inpstr[5];
				outpstr[ 1]= inpstr[6];
				outpstr[ 3]= inpstr[8];
				outpstr[ 4]= inpstr[9];
			}
< ---- */
			outpstr[ 2]= '.';
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[0];
			outpstr[ 7]= inpstr[1];
			outpstr[ 8]= inpstr[2];
			outpstr[ 9]= inpstr[3];
			outpstr[10]= '\0';
			return outpstr ;
		} ;

		if ( inpstr[2] == '.' && inpstr[5] == '.' )	// passt fuer dmy4.
		{

			outpstr[ 0]= inpstr[0];
			outpstr[ 1]= inpstr[1];
			outpstr[ 2]= '.';
			outpstr[ 3]= inpstr[3];
			outpstr[ 4]= inpstr[4];
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[6];
			outpstr[ 7]= inpstr[7];
			outpstr[ 8]= inpstr[8];
			outpstr[ 9]= inpstr[9];
			outpstr[10]= '\0';
			return outpstr ;
		}	// "dmy4."

	// ab jetzt geht sowieso alles schief ....

		outpstr[ 0]= inpstr[8];
		outpstr[ 1]= inpstr[9];
		outpstr[ 2]= '.';
		outpstr[ 3]= inpstr[5];
		outpstr[ 4]= inpstr[6];
		outpstr[ 5]= '.';
		outpstr[ 6]= inpstr[0];
		outpstr[ 7]= inpstr[1];
		outpstr[ 8]= inpstr[2];
		outpstr[ 9]= inpstr[3];
		outpstr[10]= '\0';
		return outpstr ;

	}
	return inpstr ;	// Error-return dummy
}


/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sqldatgeram
-
-
-       In              :       string-germanisch   "DD.MM.JJJJ"
-       Out             :       string-amerikanisch "YYYY-MM-DD"
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Umformatieren eines Datumsstrings
-
--------------------------------------------------------------------------------
**/
#ifdef OLD_DCL
char * sqldatgeram(     inpstr,   outpstr)
char * inpstr;
char * outpstr;
#else
char * sqldatgeram(char *inpstr, char * outpstr)
#endif
{
 
	outpstr[ 0]= inpstr[6];
	outpstr[ 1]= inpstr[7];
	outpstr[ 2]= inpstr[8];
	outpstr[ 3]= inpstr[9];
	outpstr[ 4]= '-';
	outpstr[ 5]= inpstr[3];
	outpstr[ 6]= inpstr[4];
	outpstr[ 7]= '-';
	outpstr[ 8]= inpstr[0];
	outpstr[ 9]= inpstr[1];
	outpstr[10]= '\0';
	return  outpstr ;
	
}


#ifdef OLD_DCL
char * datplusgerman( inpstr, outpstr, doffset)
char * inpstr;
char * outpstr;
long doffset;
#else
char * datplusgerman(char *inpstr, char * outpstr, long doffset)
#endif

{

 
	struct tm akttm;
	time_t dstart;

	char idd[3];
	char imm[3];
	char iyy[5];

	idd[0] = inpstr[0] ;
	idd[1] = inpstr[1] ;
	idd[2] = '\0' ;

	imm[0] = inpstr[3] ;
	imm[1] = inpstr[4] ;
	imm[2] = '\0' ;

	iyy[0] = inpstr[6] ;
	iyy[1] = inpstr[7] ;
	iyy[2] = inpstr[8] ;
	iyy[3] = inpstr[9] ;
	iyy[4] = '\0' ;
	
	
	time( &dstart );
	akttm = *localtime( &dstart );
 
	// in tm_year steckt dan so was wie "103" fuer 2003" usw.

	long hijahr = atol ( iyy );
	if ( hijahr < 60L) hijahr += 100L ;	//2 stelliges Jahr 2000 bis 2059
	if ( hijahr <= 60L && hijahr < 100L ) {} ;	
											// 2 stelliges Jahr 1960 ..1999
	if ( hijahr > 1900L && hijahr < 2099L )
			hijahr -= 1900L ;
	// Der Rest fuehrt zu Fehlern ....
	akttm.tm_year = hijahr ; 
    akttm.tm_mday = atol ( idd ); 
    akttm.tm_mon = atol ( imm ) - 1L ; 
         
  if( (dstart = mktime( &akttm )) != (time_t)-1 )
  {

	dstart += ( 86400L * doffset ) ;
	akttm = *localtime( &dstart );

    sprintf ( outpstr, "%02d.%02d.%04d",	// 311213 : Format versch�nert
		akttm.tm_mday, akttm.tm_mon + 1,
		akttm.tm_year + 1900L) ; 
 
  
  }
  else	// mktime failed
  {
	  sprintf ( outpstr, "%s", inpstr );
  }
	return  outpstr ;
 
}



void zahlweisen ( HJOB hJob, char * text, char * typ)
{

char kond1[4], kond2[4], kond3[4];
char hilfdatum1[11], hilfdatum2[11], hilfdatum3[11] ;
char hilfwert1[20], hilfwert2[20] ;	// 130804 : von 11 auf 20 erweitert
char hilfproz1[11], hilfproz2[11] ;


double hp, hw;

double hw1;	// 090506 : zusaetzliche Info Zahlbetrag

int i ;

	szTempt[0] = '\0' ;

	if ((( typ[0] == 'V') && (LlPrintIsVariableUsed(hJob, "rtab.zako_txt")))
		|| 
	   (( typ[0] == 'F') && (LlPrintIsFieldUsed(hJob, "rtab.zako_txt"))))
	{
		char add='@' ;

		if ( text[0] == add )
		{

//			1. Zeichen "@"	=> Kennung "vorformatiert"
//
//		                11111111112222222222333333333344444444445555555555
//            012345678901234567890123456789012345678901234567890123456789
//
//           "@Tage--& Skto -&.&& , Tage--& Skto -&.&& , Tage--& netto "
//
//			fixe string-Formate zum Aufloesen, nichtvorhandene Kondis fallen weg ...			
//

//                      1111111111222222222			
//            01234567890123456789012345678
//           "@Bankeinzug : Skonto -&.&& % "

//		221113 : neues Format mit Tagen und gegebnenfalls einem skto
//                      111111111122222222223333333			
//            0123456789012345678901234567890123456
//           "@Bankeinzug : Tage--& Skonto -&.&& % "



			if (text[1] == 'B')
			{
				sprintf ( szTempt, "%s", text + 1 ) ;	// "@" kappen

// so war das bisher ......130804 E

// so wird es zukuenftig ......130804 A
				i = (int)strlen ( clippedi(text));

				if ( i > 20 && text[14] !='T' )	// 221113 : bisheriger Ablauf 
				{
					if ( i > 25 )
					{
						sprintf (hilfwert1 ,"00,00");
						sprintf (hilfproz1 ,"00,00");

						hilfproz1[0] = text[21] ;
						hilfproz1[1] = text[22] ;
						hilfproz1[2] = text[23] ;
						hilfproz1[3] = text[24] ;
						hilfproz1[4] = text[25] ;
						hilfproz1[5] = '\0' ;

						if ( spessiggrund )	// 031114 lt, Frau Kaiser nie nicht Nachkomma
							hilfproz1[2] = '\0';

						sqldatamger ( reporech.rech_dat, hilfdatum1 );	// 131213 Rechnungsdatum als Basis

						hp = atof( hilfproz1 );
						hw = reporech.zahl_betr * ((100 - hp) / 100.00) ;
						sprintf ( hilfwert1, "%1.2f EUR", hw);
						sprintf ( hilfwert2, "%1.2f EUR", reporech.zahl_betr - hw);
						if ( dietzosep )	// 231213 : nur f�r Dietz
						{
								sprintf ( szTempt, "Den Betrag werden wir am %s oder dem darauf folgenden Bankarbeitstag Ihrem uns bekannten Konto belasten.",
								hilfdatum1) ;
						}
						else				// 231213 : f�r alle normalen Kunden
						{
							sprintf ( szTempt, "%s = %s ; Den Einzugsbetrag von %s werden wir am %s oder dem darauf folgenden Bankarbeitstag Ihrem uns bekannten Konto belasten.",
								clippedi(text + 1),hilfwert2, hilfwert1 ,hilfdatum1) ;
						}
					}	// bisheriger skto-Ablauf mit Einzugsbetrag ohne zahlziel
// so wird es zukuenftig ......130804 E
				}
				else
				{	// 221113 : neuer Ablauf f�r Dietz-SL
					if ( i > 20 && text[14] =='T' )	// 221113 : neuer Ablauf mit "Tage" 
					{
						kond1[0] = text[18] ;
						kond1[1] = text[19] ;
						kond1[2] = text[20] ;
						kond1[3] = '\0' ;

						sqldatamger ( reporech.rech_dat, hilfdatum1 );
						datplusgerman( hilfdatum1 , hilfdatum1, atol( kond1));
// das w�re immer f�r Dietz-SL
							sprintf ( szTempt,
								"Den Betrag werden wir am %s oder dem darauf folgenden Bankarbeitstag Ihrem uns bekannten Konto belasten.", hilfdatum1 );
// 051213 : Skonto auch noch generieren .......
						if ( i > 34 && text[22] =='S' )
						{
							sprintf (hilfwert1 ,"00,00");
							sprintf (hilfproz1 ,"00,00");

							hilfproz1[0] = text[29] ;
							hilfproz1[1] = text[30] ;
							hilfproz1[2] = text[31] ;
							hilfproz1[3] = text[32] ;
							hilfproz1[4] = text[33] ;
							hilfproz1[5] = '\0' ;

							if ( spessiggrund )	// 031114 lt, Frau Kaiser nie nicht Nachkomma
								hilfproz1[2] = '\0';

							hp = atof( hilfproz1 );
							hw = reporech.zahl_betr * ((100 - hp) / 100.00) ;
							sprintf ( hilfwert1, "%1.2f EUR", hw);
							sprintf ( hilfwert2, "%1.2f EUR", reporech.zahl_betr - hw);
							if ( dietzosep )	// 231213 : nur f�r Dietz
							{
								sprintf ( szTempt, "Den Betrag werden wir am %s oder dem darauf folgenden Bankarbeitstag Ihrem uns bekannten Konto belasten." ,hilfdatum1 ) ;
							}
							else	// 231213 : f�r alle normalen Kunden
							{
								sprintf ( szTempt, "%s = %s ; Den Einzugsbetrag von %s werden wir am %s oder dem darauf folgenden Bankarbeitstag Ihrem uns bekannten Konto belasten."
									,clippedi(text + 1),hilfwert2, hilfwert1 , hilfdatum1 ) ;
							}
						}	// Tage UND skonto
				
					}	// mindestens Tage

				}	// alter Ablauf : nur skto 
			}// Aufloesung "@Bankeinzug"
			else	// ohne Bankeinzug
			{

				i = (int)strlen (clippedi (text));
				sprintf (hilfdatum1 ,"12.12.02");
				sprintf (hilfdatum2 ,"12.12.02");
				sprintf (hilfdatum3 ,"12.12.02");
				sprintf (hilfwert1 ,"00,00");
				sprintf (hilfwert2 ,"00,00");
				sprintf (hilfproz1 ,"00,00");
				sprintf (hilfproz2 ,"00,00");

				if (i > 9)		// Notbremse ....
				{
	
					kond1[0] = text[5] ;
					kond1[1] = text[6] ;
					kond1[2] = text[7] ;
					kond1[3] = '\0' ;

					sqldatamger ( reporech.rech_dat, hilfdatum1 );
					datplusgerman( hilfdatum1 , hilfdatum1, atol( kond1));

// Erzeugen :	hilfdatum = rech_dat + kond1 

					if ( text[9] != 'S' )	// netto-Ablauf
					{
						sprintf ( szTempt, "Zahlbar bis zum %s rein netto.", hilfdatum1 );
					}
					else
					{
						if ( i > 30 )	// Notbremse
						{
							kond2[0] = text[26] ;
							kond2[1] = text[27] ;
							kond2[2] = text[28] ;
							kond2[3] = '\0' ;


							hilfproz1[0] = text[14] ;
							hilfproz1[1] = text[15] ;
							hilfproz1[2] = text[16] ;
							hilfproz1[3] = text[17] ;
							hilfproz1[4] = text[18] ;
							hilfproz1[5] = '\0' ;

							if ( spessiggrund )	// 031114 lt, Frau Kaiser nie nicht Nachkomma
								hilfproz1[2] = '\0';

							sqldatamger ( reporech.rech_dat, hilfdatum2 );
							datplusgerman( hilfdatum2 , hilfdatum2, atol( kond2));

							
							if ( text[30] != 'S' )	// netto-Ablauf
							{

								 hp = atof( hilfproz1 );
								 hw = reporech.zahl_betr * (hp /100.00) ;


								 sprintf ( hilfwert1, "%1.2f", hw);

								 
								 hw1 = atof( hilfwert1 ) ;	// 090506 : erst runden, dann rechnen
								 hw1 = reporech.zahl_betr - hw1 ;	// 090506

// 090506 								sprintf ( szTempt, "Zahlbar bis zum %s abz. %s Euro %s%% Skto, bis zum %s rein netto",
// 090506									hilfdatum1, hilfwert1,hilfproz1, hilfdatum2 );
								if ( spessiggrund )
 									sprintf ( szTempt, "Zahlbar bis zum %s abz�glich %s Euro %s%% Skonto (=%1.2f), bis zum %s rein netto .",
									hilfdatum1, hilfwert1,hilfproz1, hw1, hilfdatum2 );
								else	// 031114 : ohne Essig-Extras
 									sprintf ( szTempt, "Zahlbar bis zum %s abz. %s Euro %s%% Skto (=%1.2f), bis zum %s rein netto .",
									hilfdatum1, hilfwert1,hilfproz1, hw1, hilfdatum2 );
							}
							else
							{
								if ( i > 50 )	// Notbremse
								{
									kond3[0] = text[26] ;
									kond3[1] = text[27] ;
									kond3[2] = text[28] ;
									kond3[3] = '\0' ;


									sqldatamger ( reporech.rech_dat, hilfdatum3 );
									datplusgerman( hilfdatum3 , hilfdatum3, atol( kond3));


									hilfproz2[0] = text[14] ;
									hilfproz2[1] = text[15] ;
									hilfproz2[2] = text[16] ;
									hilfproz2[3] = text[17] ;
									hilfproz2[4] = text[18] ;
									hilfproz2[5] = '\0' ;

									if ( spessiggrund )	// 031114 lt, Frau Kaiser nie nicht Nachkomma
									hilfproz2[2] = '\0';

									hp = atof( hilfproz1 );
									hw = reporech.zahl_betr * (hp /100.00) ;
										sprintf ( hilfwert1, "%1.2f", hw);

									hp = atof( hilfproz2 );
									hw = reporech.zahl_betr * (hp /100.00) ;

									sprintf ( hilfwert2, "%1.2f", hw);

									hw1 = atof( hilfwert1 ) ;	// 090506 : erst runden, dann rechnen
									hw1 = reporech.zahl_betr - hw1 ;	// 090506

/* ----> 090506
									sprintf ( szTempt, "Zahlbar bis zum %s abz. %s Euro %s%% Skto,%cbis zum %s abz. %s Euro %s%% Skto, bis zum %s rein netto",
									hilfdatum1, hilfwert1,hilfproz1,
									LL_CHAR_NEWLINE,
									hilfdatum2, hilfwert2, hilfproz2,
									hilfdatum3 );
< --- */

									if (spessiggrund )	// 031114 
										sprintf ( szTempt, "Zahlbar bis zum %s abz�glich %s Euro %s%% Skonto (=%1.2f),%cbis zum %s abz�glich %s Euro %s%% Skonto, bis zum %s rein netto .",
										hilfdatum1, hilfwert1,hilfproz1,hw1,
										LL_CHAR_NEWLINE,
										hilfdatum2, hilfwert2, hilfproz2,
										hilfdatum3 );
									else
										sprintf ( szTempt, "Zahlbar bis zum %s abz. %s Euro %s%% Skto (=%1.2f),%cbis zum %s abz. %s Euro %s%% Skto, bis zum %s rein netto.",
										hilfdatum1, hilfwert1,hilfproz1,hw1,
										LL_CHAR_NEWLINE,
										hilfdatum2, hilfwert2, hilfproz2,
										hilfdatum3 );

								}
							}
						}
						else	// nur skto-Info 
						{
							if (i > 18 )
							{
							    hilfproz1[0] = text[14] ;
							    hilfproz1[1] = text[15] ;
							    hilfproz1[2] = text[16] ;
								hilfproz1[3] = text[17] ;
								hilfproz1[4] = text[18] ;
								hilfproz1[5] = '\0' ;

								if ( spessiggrund )	// 031114 lt, Frau Kaiser nie nicht Nachkomma
									hilfproz1[2] = '\0';

								
								hp = atof( hilfproz1 );
								hw = reporech.zahl_betr * (hp /100) ;

								sprintf ( hilfwert1, "%1.2f", hw);

								hw1 = atof( hilfwert1 ) ;	// 090506 : erst runden, dann rechnen
								hw1 = reporech.zahl_betr - hw1 ;	// 090506

// 090506								sprintf ( szTempt, "Zahlbar bis zum %s abz. %s Euro %s%% Skto",
// 090506								hilfdatum1, hilfwert1,hilfproz1 );
								if ( spessiggrund )	// 031114 
									sprintf ( szTempt, "Zahlbar bis zum %s abz�glich %s Euro %s%% Skonto (=%1.2f) .",
									hilfdatum1, hilfwert1,hilfproz1, hw1 );
								else
									sprintf ( szTempt, "Zahlbar bis zum %s abz. %s Euro %s%% Skto (=%1.2f) .",
									hilfdatum1, hilfwert1,hilfproz1, hw1 );
							}

						}	// i > 30
					}
				}			// i > 9 


			}	// Aufloesungen "@"
		
		
		}
// 131213 A
		if ( ! strncmp ( szTempt, "per Bankeinzug", 14 )||! strncmp ( reporech.zako_txt, "per Bankeinzug", 14 ) )	// Notnagel, falls keinerlei Kondis drin stehen
		{
			sqldatamger ( reporech.rech_dat, hilfdatum1 );
			sprintf ( szTempt,
					"Den Betrag werden wir am %s oder dem darauf folgenden Bankarbeitstag Ihrem uns bekannten Konto belasten.", hilfdatum1 );

		}


// 131213 E


	}	// ueberhaupt was zu tun .....



	if ( (strlen( szTempt)) >0 )
	{
		if ( typ[0] == 'V')	LlDefineVariableExt(hJob, "rtab.zako_txt", szTempt, LL_TEXT, NULL);
		if ( typ[0] == 'F')	LlDefineFieldExt(hJob, "rtab.zako_txt", szTempt, LL_TEXT, NULL);
	}
}


// l -> lpos_txt(lspt oder aufpt oder angpt )	// 130707
// k/f -> kopf/fuss (ls_txt)
// K/F -> Kopf/Fuss je Beleg	120504
// r -> rpos_txt ( retpt)
// L -> LS-Fusstext je Kunde
// R -> Rech-Fusstext je Kunde

// 101014 a -> txt_rech (aus atexte)
// 151014 x -> 20 fr�hliche Texte lesen
void textsetzen ( HJOB hJob, char * tab, char * typ)
{

char feldnam[45] ;
int k ;

	if ( tab[0] == 'a' )
	{	// 101014 : txt_rech aus atexte lesen

		sprintf ( atexte.disp_txt,"");
		sprintf ( atexte.txt,"");
		atexte.txt_nr = reporech.txt_rech;
		if ( typ[0] == 'V' )
		{
			if ( LlPrintIsVariableUsed(hJob, "rtab.txt_rech_t"))
			{
				if ( atexte.txt_nr > 0 )
					Atexte.leseatexte(atexte.txt_nr);
				LlDefineVariableExt(hJob, "rtab.txt_rech_t", atexte.disp_txt, LL_TEXT, NULL);
			}
		}
		if ( typ[0] == 'F' )
		{
			if ( LlPrintIsFieldUsed(hJob, "rtab.txt_rech_t"))
			{
				if ( atexte.txt_nr > 0 )
					Atexte.leseatexte(atexte.txt_nr);
				LlDefineFieldExt(hJob, "rtab.txt_rech_t", atexte.disp_txt, LL_TEXT, NULL);
			}
		}
		return;
	}

// 151014 A

	if ( tab[0] == 'x' )
	{	// 151014 : alle Texte laden 

		double fhilfe ;

		// falls reporech.a == reporech.a_kun, dann wird reporech.a vom uebergeordneten Programm = 0 gestetzt
		if ( reporech.a == 0.0 )
			fhilfe = atof ( reporech.a_kun ) ;
		else
			fhilfe = reporech.a ;
		if (reporech.lsret[0] == 'L')
			Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,fhilfe);
		else
			Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,-777,fhilfe);	// marker f�r "bitte l�schen" wegen z.B. retouren
		if ( typ[0] == 'F') 
		{
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls1_t",Aufpt_verkfrage.ctxt_ls1, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls2_t",Aufpt_verkfrage.ctxt_ls2, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls3_t",Aufpt_verkfrage.ctxt_ls3, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls4_t",Aufpt_verkfrage.ctxt_ls4, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls5_t",Aufpt_verkfrage.ctxt_ls5, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls6_t",Aufpt_verkfrage.ctxt_ls6, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls7_t",Aufpt_verkfrage.ctxt_ls7, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls8_t",Aufpt_verkfrage.ctxt_ls8, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls9_t",Aufpt_verkfrage.ctxt_ls9, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls10_t",Aufpt_verkfrage.ctxt_ls10, LL_TEXT, NULL);

			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech1_t",Aufpt_verkfrage.ctxt_rech1, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech2_t",Aufpt_verkfrage.ctxt_rech2, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech3_t",Aufpt_verkfrage.ctxt_rech3, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech4_t",Aufpt_verkfrage.ctxt_rech4, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech5_t",Aufpt_verkfrage.ctxt_rech5, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech6_t",Aufpt_verkfrage.ctxt_rech6, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech7_t",Aufpt_verkfrage.ctxt_rech7, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech8_t",Aufpt_verkfrage.ctxt_rech8, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech9_t",Aufpt_verkfrage.ctxt_rech9, LL_TEXT, NULL);
			LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech10_t",Aufpt_verkfrage.ctxt_rech10, LL_TEXT, NULL);
		}
		if ( typ[0] == 'V' )
		{
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls1_t",Aufpt_verkfrage.ctxt_ls1, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls2_t",Aufpt_verkfrage.ctxt_ls2, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls3_t",Aufpt_verkfrage.ctxt_ls3, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls4_t",Aufpt_verkfrage.ctxt_ls4, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls5_t",Aufpt_verkfrage.ctxt_ls5, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls6_t",Aufpt_verkfrage.ctxt_ls6, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls7_t",Aufpt_verkfrage.ctxt_ls7, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls8_t",Aufpt_verkfrage.ctxt_ls8, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls9_t",Aufpt_verkfrage.ctxt_ls9, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls10_t",Aufpt_verkfrage.ctxt_ls10, LL_TEXT, NULL);

			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech1_t",Aufpt_verkfrage.ctxt_rech1, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech2_t",Aufpt_verkfrage.ctxt_rech2, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech3_t",Aufpt_verkfrage.ctxt_rech3, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech4_t",Aufpt_verkfrage.ctxt_rech4, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech5_t",Aufpt_verkfrage.ctxt_rech5, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech6_t",Aufpt_verkfrage.ctxt_rech6, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech7_t",Aufpt_verkfrage.ctxt_rech7, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech8_t",Aufpt_verkfrage.ctxt_rech8, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech9_t",Aufpt_verkfrage.ctxt_rech9, LL_TEXT, NULL);
			LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech10_t",Aufpt_verkfrage.ctxt_rech10, LL_TEXT, NULL);
		}
		return;
	}

// 151014 E



	// lpos_txt und Variable
	if ( tab[0] == 'l' && typ[0] == 'V')
	{
		if (LlPrintIsVariableUsed(hJob, "rtab.lpos_txt_t"))
		{
			szTempt[0] = '\0' ;
			if ( reporech.lpos_txt > 0 )
			{
				angpt.nr = reporech.lpos_txt;
				aufpt.nr = reporech.lpos_txt;
				lspt.nr = reporech.lpos_txt;
				if ( blgtyp[0] == 'N' )    { k = Angpt.openangpt(); }
				else
				{ if ( blgtyp[0] == 'U' )	{ k = Aufpt.openaufpt(); }
					else	// 130707 : so war es bisher ....
					{ k = Lspt.openlspt(); }
				}
				while (! k )
				{
					if ( blgtyp[0] == 'N' ) { k = Angpt.leseangpt(0) ; }
					else
					{ if ( blgtyp[0] == 'U' ) { k = Aufpt.leseaufpt(0) ;}
						else	// 130707 : so war es bisher ....
						{ k = Lspt.leselspt(0) ; }
					}
					if (! k )
					{
						if (strlen(szTempt))
						{
							sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
						}
					
						if (( strlen(szTempt)) + ( strlen(clippedi(lspt.txt))) >= CTEXTMAX  )
							break ;

						sprintf(  szTempt + strlen(szTempt) , "%s", clippedi(lspt.txt) );
					}
				}
				if ( strlen(szTempt))
				{	
					LlDefineVariableExt(hJob, "rtab.lpos_txt_t", szTempt, LL_TEXT, NULL);
				}
				else
				{
					LlDefineVariableExt(hJob, "rtab.lpos_txt_t", " ", LL_TEXT, NULL);
				}
			}
			else
			{
				LlDefineVariableExt(hJob, "rtab.lpos_txt_t", " ", LL_TEXT, NULL);
			}
		}
	}


	// lpos_txt und Feld
	if ( tab[0] == 'l' && typ[0] == 'F')
	{
		if (LlPrintIsFieldUsed(hJob, "rtab.lpos_txt_t"))
		{

// 221104 A
			szTempt[0] = '\0' ;

			if ( da_zustxt_wa )
			{
				a_zus_txt.a = reporech.a ;

				k = A_zus_txt.opena_zus_txt();
				while (! k )
				{

					k = A_zus_txt.lesea_zus_txt(0) ;
					if (! k )
					{
						if (strlen(szTempt))
						{
							sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
						}
						if (( strlen(szTempt)) + ( strlen(clippedi(a_zus_txt.txt))) >= CTEXTMAX  )
							break ;
						sprintf(  szTempt + strlen(szTempt) , "%s", clippedi(a_zus_txt.txt) );
					}
				}
			}

			
			if ( reporech.lpos_txt > 0 )
			{
// 221104				szTempt[0] = '\0' ;
				angpt.nr = reporech.lpos_txt;
				aufpt.nr = reporech.lpos_txt;
				lspt.nr = reporech.lpos_txt;

				if ( blgtyp[0] == 'N' )    { k = Angpt.openangpt(); }
				else
				{ if ( blgtyp[0] == 'U' )	{ k = Aufpt.openaufpt(); }
					else	// 130707 : so war es bisher ....
					{ k = Lspt.openlspt(); }
				}
				while (! k )
				{
					if ( blgtyp[0] == 'N' ) { k = Angpt.leseangpt(0) ; }
					else
					{ if ( blgtyp[0] == 'U' ) { k = Aufpt.leseaufpt(0) ;}
						else	// 130707 : so war es bisher ....
						{ k = Lspt.leselspt(0) ; }
					}

					if (! k )
					{
						if (strlen(szTempt))
						{
							sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
						}
					
						if (( strlen(szTempt)) + ( strlen(clippedi(lspt.txt))) >= CTEXTMAX  )
							break ;

						sprintf(  szTempt + strlen(szTempt) , "%s", clippedi(lspt.txt) );
					}
				}
			}
			if ( strlen(szTempt))
			{	
				LlDefineFieldExt(hJob, "rtab.lpos_txt_t", szTempt, LL_TEXT, NULL);
			}
			else
			{
				LlDefineFieldExt(hJob, "rtab.lpos_txt_t", " ", LL_TEXT, NULL);
			}

		}
	}


	// rpos_txt und Variable -> der text wird in "lpos_txt_t" eingestellt !!!!
	// 200504 bis heute stand hier : rpos_txt_t 
	if ( tab[0] == 'r' && typ[0] == 'V')
	{
		if (LlPrintIsVariableUsed(hJob, "rtab.lpos_txt_t"))
		{
			if ( reporech.rpos_txt > 0 )
			{
				retpt.nr = reporech.rpos_txt;
				szTempt[0] = '\0' ;
				k = Retpt.openretpt();
				while (! k )
				{

					k = Retpt.leseretpt(0) ;
					if (! k )
					{
						if (strlen(szTempt))
						{
							sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
						}
	
						if (( strlen(szTempt)) + ( strlen(clippedi(retpt.txt))) >= CTEXTMAX  )
							break ;

						sprintf( szTempt + strlen(szTempt) , "%s", clippedi(retpt.txt) );
					}

				}
				if ( strlen(szTempt))
				{	
					LlDefineVariableExt(hJob, "rtab.lpos_txt_t", szTempt, LL_TEXT, NULL);
				}
				else
				{
					LlDefineVariableExt(hJob, "rtab.lpos_txt_t", " ", LL_TEXT, NULL);
				}
			}
			else
			{
				LlDefineVariableExt(hJob, "rtab.lpos_txt_t", " ", LL_TEXT, NULL);
			}
		}
	}


	// rpos_txt und Feld
	if ( tab[0] == 'r' && typ[0] == 'F')
	{
		if (LlPrintIsFieldUsed(hJob, "rtab.lpos_txt_t"))
		{
			if ( reporech.rpos_txt > 0 )
			{
				retpt.nr = reporech.rpos_txt;
				szTempt[0] = '\0' ;
				k = Retpt.openretpt();
				while (! k )
				{

					k = Retpt.leseretpt(0) ;
					if (! k )
					{
						if (strlen(szTempt))
						{
							sprintf( & szTempt[strlen(szTempt)], "%c", LL_CHAR_NEWLINE );
						}

						if (( strlen(szTempt)) + ( strlen(clippedi(retpt.txt))) >= CTEXTMAX  )
							break ;

						sprintf( szTempt + strlen(szTempt) , "%s", clippedi(retpt.txt) );
					}

				}
				if ( strlen(szTempt))
				{	
					LlDefineFieldExt(hJob, "rtab.lpos_txt_t", szTempt, LL_TEXT, NULL);
				}
				else
				{
					LlDefineFieldExt(hJob, "rtab.lpos_txt_t", " ", LL_TEXT, NULL);
				}
			}
			else
			{
				LlDefineFieldExt(hJob, "rtab.lpos_txt_t", " ", LL_TEXT, NULL);
			}
		}
	}

	
	// kopf-/fuss_txt und Variable : 300608 : auch fuer Feld : Codesparen  
	// 30608 : einbau Optimizer : diese texte muss man ja eigentlich nur einmal je Rechnung lesen 
	if ( ( tab[0] == 'k' || tab[0] == 'f' 
		|| tab[0] == 'K' || tab[0] == 'F'
		|| tab[0] == 'L' || tab[0] == 'R') 
		&& ( typ[0] == 'V' || typ[0] == 'F' ) )
	{
		int gefunden = 0 ;	// 300608

		ls_txt.nr = 0 ;
		if ( tab[0] == 'k' )
		{
			sprintf ( feldnam , "rtab.kopf_txt_t" ) ;
			ls_txt.nr = reporech.kopf_txt;

			if ( ls_txt.nr == szknummer )
			{
				sprintf ( szTempt , "%s" , szkTempt ) ;
				gefunden = 1 ;
			}
		}

		if ( tab[0] == 'f' )
		{
			sprintf ( feldnam , "rtab.fuss_txt_t" ) ;
			ls_txt.nr = reporech.fuss_txt;

			if ( ls_txt.nr == szfnummer )
			{
				sprintf ( szTempt , "%s" , szfTempt ) ;
				gefunden = 1 ;
			}
		}

// 120504 	
		if ( tab[0] == 'K' )
		{
			sprintf ( feldnam , "rtab.ktx_jebel_t" ) ;
			ls_txt.nr = reporech.ktx_jebel;
		}

		if ( tab[0] == 'F' )
		{
			sprintf ( feldnam , "rtab.ftx_jebel_t" ) ;
			ls_txt.nr = reporech.ftx_jebel;
		}

// 270608 	
		if ( tab[0] == 'L' )
		{
			sprintf ( feldnam , "kun.ls_fuss_txt_t" ) ;
			ls_txt.nr = kun.ls_fuss_txt ;
			if ( ls_txt.nr == szLnummer )
			{
				sprintf ( szTempt , "%s" , szLTempt ) ;
				gefunden = 1 ;
			}
		}

		if ( tab[0] == 'R' )
		{
			sprintf ( feldnam , "kun.rech_fuss_txt_t" ) ;
			ls_txt.nr = kun.rech_fuss_txt;
			if ( ls_txt.nr == szRnummer )
			{
				sprintf ( szTempt , "%s" , szRTempt ) ;
				gefunden = 1 ;
			}
		}

		if ( gefunden == 1 )	// 300608 : Optimized 
		{
			if ( typ[0] == 'V' )
			{
				LlDefineVariableExt(hJob, feldnam , szTempt, LL_TEXT, NULL);
			}
			else
			{
				LlDefineFieldExt(hJob, feldnam , szTempt, LL_TEXT, NULL);
			}
		}
		else	// 300608 : so �hnlich war es bisher ....
		{
			if ((LlPrintIsVariableUsed(hJob, feldnam) && typ[0] == 'V' ) 
			|| (LlPrintIsFieldUsed(hJob, feldnam) && typ[0] == 'F' )) 
			{
				if ( ls_txt.nr > 0 )
				{
					szTempt[0] = '\0' ;
					k = Ls_txt.openls_txt();
					while (! k )
					{
						k = Ls_txt.lesels_txt(0) ;
						if (! k )
						{
							if (strlen(szTempt))
							{
								sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
							}
							if (( strlen(szTempt)) + ( strlen(clippedi(ls_txt.txt))) >= CTEXTMAX  )
							break ;
							sprintf( szTempt + strlen(szTempt), "%s", clippedi(ls_txt.txt) );
						}
					}
					if ( strlen(szTempt))
					{	
						if ( typ[0] == 'V') LlDefineVariableExt(hJob, feldnam , szTempt, LL_TEXT, NULL);
						if ( typ[0] == 'F') LlDefineFieldExt(hJob, feldnam , szTempt, LL_TEXT, NULL);
						if ( tab[0] == 'k' ) { sprintf ( szkTempt, "%s" , szTempt ) ; szknummer = reporech.kopf_txt ; } ;
						if ( tab[0] == 'f' ) { sprintf ( szfTempt, "%s" , szTempt ) ; szfnummer = reporech.fuss_txt ; } ;
						if ( tab[0] == 'L' ) { sprintf ( szLTempt, "%s" , szTempt ) ; szLnummer = kun.ls_fuss_txt ; } ;
						if ( tab[0] == 'R' ) { sprintf ( szRTempt, "%s" , szTempt ) ; szRnummer = kun.rech_fuss_txt ; } ;

					}
					else
					{
						if ( typ[0] == 'V' ) LlDefineVariableExt(hJob, feldnam , " ", LL_TEXT, NULL);
						if ( typ[0] == 'F' ) LlDefineFieldExt(hJob, feldnam , " ", LL_TEXT, NULL);
						if ( tab[0] == 'k' ) { sprintf ( szkTempt, " " ) ; szknummer = reporech.kopf_txt ; } ;
						if ( tab[0] == 'f' ) { sprintf ( szfTempt, " " ) ; szfnummer = reporech.fuss_txt ; } ;
						if ( tab[0] == 'L' ) { sprintf ( szLTempt, " " ) ; szLnummer = kun.ls_fuss_txt ; } ; 
						if ( tab[0] == 'R' ) { sprintf ( szRTempt, " " ) ; szRnummer = kun.rech_fuss_txt ; } ;

					}
				}
				else
				{
					if ( typ[0] == 'V' ) LlDefineVariableExt(hJob, feldnam , " ", LL_TEXT, NULL);
					if ( typ[0] == 'F' ) LlDefineFieldExt(hJob, feldnam , " ", LL_TEXT, NULL);
					if ( tab[0] == 'k' ) { sprintf ( szkTempt, " " ) ; szknummer = reporech.kopf_txt ; } ;
					if ( tab[0] == 'f' ) { sprintf ( szfTempt, " " ) ; szfnummer = reporech.fuss_txt ; } ;
					if ( tab[0] == 'L' ) { sprintf ( szLTempt, " " ) ; szLnummer = kun.ls_fuss_txt  ; } ;
					if ( tab[0] == 'R' ) { sprintf ( szRTempt, " " ) ; szRnummer = kun.rech_fuss_txt ; } ;

				}
			}
		}
	}

}


void VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{


	// 040805
	int ipo ;
	char szTemp3[30] ;
	// 041206  10 -> LEERMATDIM
	for ( ipo = 1 ; ipo < LEERMATDIM ; ipo ++ )
	{


	    sprintf(szTemp2, "leer_rh.a%.0d", ipo );
		if (LlPrintIsVariableUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0lf" , leer_mat_a[ipo - 1] ) ;
			LlDefineVariableExt(hJob, szTemp2 , szTemp3 , LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_rh.a_bz%.0d", ipo );
		if (LlPrintIsVariableUsed(hJob, szTemp2))
		{
			LlDefineVariableExt(hJob, szTemp2  , leer_mat_bz[ipo - 1], LL_TEXT, NULL);
		}

		sprintf(szTemp2, "leer_rh.stk_zu%.0d", ipo );
		if (LlPrintIsVariableUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0lf", leer_mat_sz[ipo - 1] ) ;
			LlDefineVariableExt(hJob, szTemp2, szTemp3, LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_rh.stk_ab%.0lf", ipo );
		if (LlPrintIsVariableUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0d", leer_mat_sa[ipo - 1] ) ;
			LlDefineVariableExt(hJob, szTemp2, szTemp3, LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_rh.stk%.0d", ipo );
		if (LlPrintIsVariableUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0d", leer_mat_s[ipo - 1] ) ;
			LlDefineVariableExt(hJob, szTemp2 , szTemp3, LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_rh.pr_vk%.0d", ipo );
		if (LlPrintIsVariableUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%1.2lf", leer_mat_pr[ipo - 1] ) ;
			LlDefineVariableExt(hJob, szTemp2 , szTemp3, LL_NUMERIC, NULL);
		}
	}
	if (LlPrintIsVariableUsed(hJob, "leer_rh.kun_leer_kz"))
	{
		sprintf ( szTemp3, "%1.0d", kun.kun_leer_kz ) ;
		LlDefineVariableExt(hJob, "leer_rh.kun_leer_kz" , szTemp3, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "leer_rh.leih_aktiv"))	// 0411206
	{
		sprintf ( szTemp3, "%1.0d", party_mdn_aktiv ) ;
		LlDefineVariableExt(hJob, "leer_rh.leih_aktiv" , szTemp3, LL_NUMERIC, NULL);
	}
	
// 090709
// rabz1 #############
	if (LlPrintIsVariableUsed(hJob, "rabz1.typ"))
		LlDefineVariableExt(hJob, "rabz1.typ", reporzu1.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsVariableUsed(hJob, "rabz1.gruppe"))
		LlDefineVariableExt(hJob, "rabz1.gruppe", reporzu1.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsVariableUsed(hJob, "rabz1.wertart"))
		LlDefineVariableExt(hJob, "rabz1.wertart", reporzu1.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsVariableUsed(hJob, "rabz1.rab_bz"))
		LlDefineVariableExt(hJob, "rabz1.rab_bz", reporzu1.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsVariableUsed(hJob, "rabz1.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu1.me);
		LlDefineVariableExt(hJob, "rabz1.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsVariableUsed(hJob, "rabz1.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu1.faktor);
		LlDefineVariableExt(hJob, "rabz1.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsVariableUsed(hJob, "rabz1.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu1.geswert);
		LlDefineVariableExt(hJob, "rabz1.geswert", szTemp2, LL_NUMERIC, NULL);
	}

// rabz2 #############
	if (LlPrintIsVariableUsed(hJob, "rabz2.typ"))
		LlDefineVariableExt(hJob, "rabz2.typ", reporzu2.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsVariableUsed(hJob, "rabz2.gruppe"))
		LlDefineVariableExt(hJob, "rabz2.gruppe", reporzu2.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsVariableUsed(hJob, "rabz2.wertart"))
		LlDefineVariableExt(hJob, "rabz2.wertart", reporzu2.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsVariableUsed(hJob, "rabz2.rab_bz"))
		LlDefineVariableExt(hJob, "rabz2.rab_bz", reporzu2.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsVariableUsed(hJob, "rabz2.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu2.me);
		LlDefineVariableExt(hJob, "rabz2.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsVariableUsed(hJob, "rabz2.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu2.faktor);
		LlDefineVariableExt(hJob, "rabz2.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsVariableUsed(hJob, "rabz2.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu2.geswert);
		LlDefineVariableExt(hJob, "rabz2.geswert", szTemp2, LL_NUMERIC, NULL);
	}

// rabz3 #############
	if (LlPrintIsVariableUsed(hJob, "rabz3.typ"))
		LlDefineVariableExt(hJob, "rabz3.typ", reporzu3.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsVariableUsed(hJob, "rabz3.gruppe"))
		LlDefineVariableExt(hJob, "rabz3.gruppe", reporzu3.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsVariableUsed(hJob, "rabz3.wertart"))
		LlDefineVariableExt(hJob, "rabz3.wertart", reporzu3.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsVariableUsed(hJob, "rabz3.rab_bz"))
		LlDefineVariableExt(hJob, "rabz3.rab_bz", reporzu3.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsVariableUsed(hJob, "rabz3.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu3.me);
		LlDefineVariableExt(hJob, "rabz3.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsVariableUsed(hJob, "rabz3.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu3.faktor);
		LlDefineVariableExt(hJob, "rabz3.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsVariableUsed(hJob, "rabz3.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu3.geswert);
		LlDefineVariableExt(hJob, "rabz3.geswert", szTemp2, LL_NUMERIC, NULL);
	}

// 260612
// rabz4 #############
	if (LlPrintIsVariableUsed(hJob, "rabz4.typ"))
		LlDefineVariableExt(hJob, "rabz4.typ", reporzu4.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsVariableUsed(hJob, "rabz4.gruppe"))
		LlDefineVariableExt(hJob, "rabz4.gruppe", reporzu4.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsVariableUsed(hJob, "rabz4.wertart"))
		LlDefineVariableExt(hJob, "rabz4.wertart", reporzu4.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsVariableUsed(hJob, "rabz4.rab_bz"))
		LlDefineVariableExt(hJob, "rabz4.rab_bz", reporzu4.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsVariableUsed(hJob, "rabz4.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu4.me);
		LlDefineVariableExt(hJob, "rabz4.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsVariableUsed(hJob, "rabz4.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu4.faktor);
		LlDefineVariableExt(hJob, "rabz4.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsVariableUsed(hJob, "rabz4.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu4.geswert);
		LlDefineVariableExt(hJob, "rabz4.geswert", szTemp2, LL_NUMERIC, NULL);
	}

// rabz5 #############
	if (LlPrintIsVariableUsed(hJob, "rabz5.typ"))
		LlDefineVariableExt(hJob, "rabz5.typ", reporzu5.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsVariableUsed(hJob, "rabz5.gruppe"))
		LlDefineVariableExt(hJob, "rabz5.gruppe", reporzu5.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsVariableUsed(hJob, "rabz5.wertart"))
		LlDefineVariableExt(hJob, "rabz5.wertart", reporzu5.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsVariableUsed(hJob, "rabz5.rab_bz"))
		LlDefineVariableExt(hJob, "rabz5.rab_bz", reporzu5.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsVariableUsed(hJob, "rabz5.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu5.me);
		LlDefineVariableExt(hJob, "rabz5.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsVariableUsed(hJob, "rabz5.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu5.faktor);
		LlDefineVariableExt(hJob, "rabz5.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsVariableUsed(hJob, "rabz5.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu5.geswert);
		LlDefineVariableExt(hJob, "rabz5.geswert", szTemp2, LL_NUMERIC, NULL);
	}

// rabz6 #############
	if (LlPrintIsVariableUsed(hJob, "rabz6.typ"))
		LlDefineVariableExt(hJob, "rabz6.typ", reporzu6.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsVariableUsed(hJob, "rabz6.gruppe"))
		LlDefineVariableExt(hJob, "rabz6.gruppe", reporzu6.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsVariableUsed(hJob, "rabz6.wertart"))
		LlDefineVariableExt(hJob, "rabz6.wertart", reporzu6.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsVariableUsed(hJob, "rabz6.rab_bz"))
		LlDefineVariableExt(hJob, "rabz6.rab_bz", reporzu6.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsVariableUsed(hJob, "rabz6.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu6.me);
		LlDefineVariableExt(hJob, "rabz6.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsVariableUsed(hJob, "rabz6.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu6.faktor);
		LlDefineVariableExt(hJob, "rabz6.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsVariableUsed(hJob, "rabz6.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu6.geswert);
		LlDefineVariableExt(hJob, "rabz6.geswert", szTemp2, LL_NUMERIC, NULL);
	}

	
// 170304 : immer laden, gebnenfalls ergibt sich dann empty .....

// 250708 A

//	if ( reporech.mdnadr > 0 )	// Mandantenadresse
//	{
		if (LlPrintIsVariableUsed(hJob, "adr1.anr"))	// 091208
		{
			LlDefineVariableExt(hJob, "adr1.anr",adr1.anrtxt, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr1.adr_nam1"))	
		{
			LlDefineVariableExt(hJob, "adr1.adr_nam1",adr1.adr_nam1, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr1.adr_nam2"))
		{
			LlDefineVariableExt(hJob, "adr1.adr_nam2",adr1.adr_nam2, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr1.adr_nam3"))	
		{
			LlDefineVariableExt(hJob, "adr1.adr_nam3",adr1.adr_nam3, LL_TEXT, NULL);
		}

		if (LlPrintIsVariableUsed(hJob, "adr1.iln"))	// 150905
		{
			LlDefineVariableExt(hJob, "adr1.iln",adr1.iln, LL_TEXT, NULL);
		}

		if (LlPrintIsVariableUsed(hJob, "adr1.ort1"))
		{
			LlDefineVariableExt(hJob, "adr1.ort1",adr1.ort1, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr1.plz"))
		{
			LlDefineVariableExt(hJob, "adr1.plz",adr1.plz, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr1.str"))
		{
			LlDefineVariableExt(hJob, "adr1.str",adr1.str, LL_TEXT, NULL);
		}

// 080709
		if (LlPrintIsVariableUsed(hJob, "adr1.plz_pf"))
			LlDefineVariableExt(hJob, "adr1.plz_pf",adr1.plz_pf, LL_TEXT, NULL);
		if (LlPrintIsVariableUsed(hJob, "adr1.pf"))
			LlDefineVariableExt(hJob, "adr1.pf",adr1.pf, LL_TEXT, NULL);

		if ( LlPrintIsVariableUsed(hJob, "adr1.ort2"))
			LlDefineVariableExt(hJob, "adr1.ort2",adr1.ort2 , LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr1.tel"))
			LlDefineVariableExt(hJob, "adr1.tel",adr1.tel, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr1.fax"))
			LlDefineVariableExt(hJob, "adr1.fax",adr1.fax, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr1.mobil"))		// 301111
			LlDefineVariableExt(hJob, "adr1.mobil",adr1.mobil, LL_TEXT, NULL);

		if ( LlPrintIsVariableUsed(hJob, "adr1.iban"))		// 300813
			LlDefineVariableExt(hJob, "adr1.iban",adr1.iban, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr1.swift"))		// 300813
			LlDefineVariableExt(hJob, "adr1.swift",adr1.swift, LL_TEXT, NULL);

		if ( LlPrintIsVariableUsed(hJob, "adr1.partner"))
			LlDefineVariableExt(hJob, "adr1.partner",adr1.partner, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr1.staattyp")
			|| LlPrintIsVariableUsed(hJob, "adr1.staatkz"))
		{
			sprintf ( ptabn.ptitem, "staat" ) ;
			sprintf ( ptabn.ptwert,"%d",adr1.staat );
			Ptabn.openptabn() ;
			if ( !Ptabn.leseptabn() )
			{
				if (LlPrintIsVariableUsed(hJob, "adr1.staattyp"))
				{
	
					LlDefineVariableExt(hJob, "adr1.staattyp",
						clippedi( ptabn.ptwer1), LL_TEXT, NULL);
				}
				if (LlPrintIsVariableUsed(hJob, "adr1.staatkz"))
				{
					LlDefineVariableExt(hJob, "adr1.staatkz",
						clippedi( ptabn.ptwer2), LL_TEXT, NULL);
				}
			}
		}
//	}

// 250708 E

// 011014 A
		if (LlPrintIsVariableUsed(hJob, "kun.adr1.anr"))
		{
			LlDefineVariableExt(hJob, "kun.adr1.anr",kunadr1.anrtxt, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "kun.adr1.adr_nam1"))	
		{
			LlDefineVariableExt(hJob, "kun.adr1.adr_nam1",kunadr1.adr_nam1, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "kun.adr1.adr_nam2"))
		{
			LlDefineVariableExt(hJob, "kun.adr1.adr_nam2",kunadr1.adr_nam2, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "kun.adr1.adr_nam3"))	
		{
			LlDefineVariableExt(hJob, "kun.adr1.adr_nam3",kunadr1.adr_nam3, LL_TEXT, NULL);
		}

		if (LlPrintIsVariableUsed(hJob, "kun.adr1.iln"))	// 150905
		{
			LlDefineVariableExt(hJob, "kun.adr1.iln",kunadr1.iln, LL_TEXT, NULL);
		}

		if (LlPrintIsVariableUsed(hJob, "kun.adr1.ort1"))
		{
			LlDefineVariableExt(hJob, "kun.adr1.ort1",kunadr1.ort1, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "kun.adr1.plz"))
		{
			LlDefineVariableExt(hJob, "kun.adr1.plz",kunadr1.plz, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "kun.adr1.str"))
		{
			LlDefineVariableExt(hJob, "kun.adr1.str",kunadr1.str, LL_TEXT, NULL);
		}

		if (LlPrintIsVariableUsed(hJob, "kun.adr1.plz_pf"))
			LlDefineVariableExt(hJob, "kun.adr1.plz_pf",kunadr1.plz_pf, LL_TEXT, NULL);
		if (LlPrintIsVariableUsed(hJob, "kun.adr1.pf"))
			LlDefineVariableExt(hJob, "kun.adr1.pf",kunadr1.pf, LL_TEXT, NULL);

		if ( LlPrintIsVariableUsed(hJob, "kun.adr1.ort2"))
			LlDefineVariableExt(hJob, "kun.adr1.ort2",kunadr1.ort2 , LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "kun.adr1.tel"))
			LlDefineVariableExt(hJob, "kun.adr1.tel",kunadr1.tel, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "kun.adr1.fax"))
			LlDefineVariableExt(hJob, "kun.adr1.fax",kunadr1.fax, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "kun.adr1.mobil"))		// 301111
			LlDefineVariableExt(hJob, "kun.adr1.mobil",kunadr1.mobil, LL_TEXT, NULL);

		if ( LlPrintIsVariableUsed(hJob, "kun.adr1.iban"))		// 300813
			LlDefineVariableExt(hJob, "kun.adr1.iban",kunadr1.iban, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "kun.adr1.swift"))		// 300813
			LlDefineVariableExt(hJob, "kun.adr1.swift",kunadr1.swift, LL_TEXT, NULL);

		if ( LlPrintIsVariableUsed(hJob, "kun.adr1.partner"))
			LlDefineVariableExt(hJob, "kun.adr1.partner",kunadr1.partner, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "kun.adr1.staattyp")
			|| LlPrintIsVariableUsed(hJob, "kun.adr1.staatkz"))
		{
			sprintf ( ptabn.ptitem, "staat" ) ;
			sprintf ( ptabn.ptwert,"%d",kunadr1.staat );
			Ptabn.openptabn() ;
			if ( !Ptabn.leseptabn() )
			{
				if (LlPrintIsVariableUsed(hJob, "kun.adr1.staattyp"))
				{
	
					LlDefineVariableExt(hJob, "kun.adr1.staattyp",
						clippedi( ptabn.ptwer1), LL_TEXT, NULL);
				}
				if (LlPrintIsVariableUsed(hJob, "kun.adr1.staatkz"))
				{
					LlDefineVariableExt(hJob, "kun.adr1.staatkz",
						clippedi( ptabn.ptwer2), LL_TEXT, NULL);
				}
			}
		}


// 011014 E

//	if ( reporech.adr2 > 0 )
//	{
		if (LlPrintIsVariableUsed(hJob, "adr2.anr"))	// 091208
		{
			LlDefineVariableExt(hJob, "adr2.anr",adr2.anrtxt, LL_TEXT, NULL);
		}

		if (LlPrintIsVariableUsed(hJob, "adr2.adr_nam1"))
		{
			LlDefineVariableExt(hJob, "adr2.adr_nam1",adr2.adr_nam1, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr2.adr_nam2"))
		{
			LlDefineVariableExt(hJob, "adr2.adr_nam2",adr2.adr_nam2, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr2.adr_nam3"))	// 090804
		{
			LlDefineVariableExt(hJob, "adr2.adr_nam3",adr2.adr_nam3, LL_TEXT, NULL);
		}

		if (LlPrintIsVariableUsed(hJob, "adr2.iln"))	// 150905
		{
			LlDefineVariableExt(hJob, "adr2.iln",adr2.iln, LL_TEXT, NULL);
		}

		if (LlPrintIsVariableUsed(hJob, "adr2.ort1"))
		{
			LlDefineVariableExt(hJob, "adr2.ort1",adr2.ort1, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr2.plz"))
		{
			LlDefineVariableExt(hJob, "adr2.plz",adr2.plz, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr2.str"))
		{
			LlDefineVariableExt(hJob, "adr2.str",adr2.str, LL_TEXT, NULL);
		}

// 080709
		if (LlPrintIsVariableUsed(hJob, "adr2.plz_pf"))
			LlDefineVariableExt(hJob, "adr2.plz_pf",adr2.plz_pf, LL_TEXT, NULL);
		if (LlPrintIsVariableUsed(hJob, "adr2.pf"))
			LlDefineVariableExt(hJob, "adr2.pf",adr2.pf, LL_TEXT, NULL);

// 250708 A
		if ( LlPrintIsVariableUsed(hJob, "adr2.ort2"))
			LlDefineVariableExt(hJob, "adr2.ort2",adr2.ort2 , LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr2.tel"))
			LlDefineVariableExt(hJob, "adr2.tel",adr2.tel, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr2.fax"))
			LlDefineVariableExt(hJob, "adr2.fax",adr2.fax, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr2.mobil"))		// 301111
			LlDefineVariableExt(hJob, "adr2.mobil",adr2.mobil, LL_TEXT, NULL);
			
		if ( LlPrintIsVariableUsed(hJob, "adr2.iban"))		// 300813
			LlDefineVariableExt(hJob, "adr2.iban",adr2.iban, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr2.swift"))		// 300813
			LlDefineVariableExt(hJob, "adr2.swift",adr2.swift, LL_TEXT, NULL);

		if ( LlPrintIsVariableUsed(hJob, "adr2.partner"))
			LlDefineVariableExt(hJob, "adr2.partner",adr2.partner, LL_TEXT, NULL);

// 110506 
		if ( LlPrintIsVariableUsed(hJob, "adr2.staattyp")
			|| LlPrintIsVariableUsed(hJob, "adr2.staatkz"))
		{
			sprintf ( ptabn.ptitem, "staat" ) ;
			sprintf ( ptabn.ptwert,"%d",adr2.staat );
			Ptabn.openptabn() ;
			if ( !Ptabn.leseptabn() )
			{
				if (LlPrintIsVariableUsed(hJob, "adr2.staattyp"))
				{
	
					LlDefineVariableExt(hJob, "adr2.staattyp",
						clippedi( ptabn.ptwer1), LL_TEXT, NULL);
				}
				if (LlPrintIsVariableUsed(hJob, "adr2.staatkz"))
				{
					LlDefineVariableExt(hJob, "adr2.staatkz",
						clippedi( ptabn.ptwer2), LL_TEXT, NULL);
				}
			}
		}
		
		
//	}

    
//	if ( reporech.adr3 > 0 )
//	{
		if (LlPrintIsVariableUsed(hJob, "adr3.anr"))	// 091208
		{
			LlDefineVariableExt(hJob, "adr3.anr",adr3.anrtxt, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr3.adr_nam1"))
		{
			LlDefineVariableExt(hJob, "adr3.adr_nam1",adr3.adr_nam1, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr3.adr_nam2"))
		{
			LlDefineVariableExt(hJob, "adr3.adr_nam2",adr3.adr_nam2, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr3.adr_nam3"))	// 090804
		{
			LlDefineVariableExt(hJob, "adr3.adr_nam3",adr3.adr_nam3, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr3.iln"))	// 150905
		{
			LlDefineVariableExt(hJob, "adr3.iln",adr3.iln, LL_TEXT, NULL);
		}

		if (LlPrintIsVariableUsed(hJob, "adr3.ort1"))
		{
			LlDefineVariableExt(hJob, "adr3.ort1",adr3.ort1, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr3.plz"))
		{
			LlDefineVariableExt(hJob, "adr3.plz",adr3.plz, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "adr3.str"))
		{
			LlDefineVariableExt(hJob, "adr3.str",adr3.str, LL_TEXT, NULL);
		}


// 080709
		if (LlPrintIsVariableUsed(hJob, "adr3.plz_pf"))
			LlDefineVariableExt(hJob, "adr3.plz_pf",adr3.plz_pf, LL_TEXT, NULL);
		if (LlPrintIsVariableUsed(hJob, "adr3.pf"))
			LlDefineVariableExt(hJob, "adr3.pf",adr3.pf, LL_TEXT, NULL);


// 250708 A
		if ( LlPrintIsVariableUsed(hJob, "adr3.ort2"))
			LlDefineVariableExt(hJob, "adr3.ort2",adr3.ort2 , LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr3.tel"))
			LlDefineVariableExt(hJob, "adr3.tel",adr3.tel, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr3.fax"))
			LlDefineVariableExt(hJob, "adr3.fax",adr3.fax, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr3.mobil"))		// 301111
			LlDefineVariableExt(hJob, "adr3.mobil",adr3.mobil, LL_TEXT, NULL);
			
		if ( LlPrintIsVariableUsed(hJob, "adr3.iban"))		// 300813
			LlDefineVariableExt(hJob, "adr3.iban",adr3.iban, LL_TEXT, NULL);
		if ( LlPrintIsVariableUsed(hJob, "adr3.swift"))		// 300813
			LlDefineVariableExt(hJob, "adr3.swift",adr3.swift, LL_TEXT, NULL);

		if ( LlPrintIsVariableUsed(hJob, "adr3.partner"))
			LlDefineVariableExt(hJob, "adr3.partner",adr3.partner, LL_TEXT, NULL);

// 110506 
		if (LlPrintIsVariableUsed(hJob, "adr3.staattyp")
			|| LlPrintIsVariableUsed(hJob, "adr3.staatkz"))
		{
			sprintf ( ptabn.ptitem, "staat" ) ;
			sprintf ( ptabn.ptwert,"%d",adr3.staat );
			Ptabn.openptabn() ;
			if ( !Ptabn.leseptabn() )
			{
				if (LlPrintIsVariableUsed(hJob, "adr3.staattyp"))
				{
	
					LlDefineVariableExt(hJob, "adr3.staattyp",
						clippedi( ptabn.ptwer1), LL_TEXT, NULL);
				}
				if (LlPrintIsVariableUsed(hJob, "adr3.staatkz"))
				{
					LlDefineVariableExt(hJob, "adr3.staatkz",
						clippedi( ptabn.ptwer2), LL_TEXT, NULL);
				}
			}
		}

//	}



// 120413 A
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.kasse" ))
	{
		sprintf(szTemp2, "%d", kase_fit.kasse );
		LlDefineVariableExt(hJob, "kase_fit.kasse",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.bon" ))
	{
		sprintf(szTemp2, "%d", kase_fit.bon );
		LlDefineVariableExt(hJob, "kase_fit.bon",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.betrag_bto" ))
	{
		sprintf(szTemp2, "%1.3lf", kase_fit.betrag_bto );
		LlDefineVariableExt(hJob, "kase_fit.betrag_bto",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.gegeben" ))
	{
		sprintf(szTemp2, "%1.3lf", kase_fit.gegeben );
		LlDefineVariableExt(hJob, "kase_fit.gegeben",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.zurueck" ))
	{
		sprintf(szTemp2, "%1.3lf", kase_fit.zurueck );
		LlDefineVariableExt(hJob, "kase_fit.zurueck",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.gut_sum" ))
	{
		sprintf(szTemp2, "%1.3lf", kase_fit.gut_sum );
		LlDefineVariableExt(hJob, "kase_fit.gut_sum",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.essengut_sum" ))
	{
		sprintf(szTemp2, "%1.3lf", kase_fit.essengut_sum );
		LlDefineVariableExt(hJob, "kase_fit.essengut_sum",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.gut_anzahl" ))
	{
		sprintf(szTemp2, "%d", kase_fit.gut_anzahl );

		LlDefineVariableExt(hJob, "kase_fit.gut_anzahl",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.essengut_anzahl" ))
	{
		sprintf(szTemp2, "%d", kase_fit.essengut_anzahl );
		LlDefineVariableExt(hJob, "kase_fit.essengut_anzahl",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.zahlart" ))
	{
		sprintf(szTemp2, "%d", kase_fit.zahlart );
		LlDefineVariableExt(hJob, "kase_fit.zahlart","1", LL_NUMERIC, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.bon_date" ))
	{
		LlDefineVariableExt(hJob, "kase_fit.bon_date", sqldatamger (kase_fit.bon_datec,szTemp2), LL_TEXT, NULL);
	}
	if ( LlPrintIsVariableUsed(hJob, "kase_fit.pers_nam" ))
	{
		LlDefineVariableExt(hJob, "kase_fit.pers_nam",kase_fit.pers_nam, LL_TEXT, NULL);
	}
// 120413 E



// 070208 A
	double fhilfe ;
	if (LlPrintIsVariableUsed(hJob, "rtab.a_bas.a_gew"))
	{
		if ( reporech.a == 0.0 )
		{	// falls reporech.a == reporech.a_kun, dann wird reporech.a vom uebergeordneten Programm = 0 gestetzt
			fhilfe = atof ( reporech.a_kun ) ;
		}
		else
		{
			fhilfe = reporech.a ;
		} 
		if ( fhilfe == a_bas.a )
		{	/* dummy-Aktion */ } 
		else
		{
		    memcpy ( &a_bas, &a_bas_null , sizeof ( struct A_BAS )) ;
			a_bas.a = fhilfe ;
			A_bas.opena_bas() ;
			A_bas.lesea_bas() ;
		}
		sprintf(szTemp2, "%1.3lf", a_bas.a_gew );	// Nachkommastellen korrigiert von 0 auf 3 030709
		LlDefineVariableExt(hJob, "rtab.a_bas.a_gew", szTemp2, LL_NUMERIC, NULL);
	
	}


	if (LlPrintIsVariableUsed(hJob, "rtab.a_bas.me_einh"))
	{
		if ( reporech.a == 0.0 )
		{	// falls reporech.a == reporech.a_kun dann wird reporech.a vom uebergeordneten Programm = 0 gestetzt
			fhilfe = atof ( reporech.a_kun ) ;
		}
		else
		{
			fhilfe = reporech.a ;
		} 
		if ( fhilfe == a_bas.a )
		{	/** dummy-Aktion */ }
		else
		{
		    memcpy ( &a_bas, &a_bas_null , sizeof ( struct A_BAS )) ;
			a_bas.a = fhilfe ;
			A_bas.opena_bas() ;
			A_bas.lesea_bas() ;
		}
		sprintf(szTemp2, "%d", a_bas.me_einh );
		LlDefineVariableExt(hJob, "rtab.a_bas.me_einh", szTemp2, LL_NUMERIC, NULL);
	}
// 070208 E

// 190310 A
// AG
	if (LlPrintIsVariableUsed(hJob, "rtab.a_bas.ag"))
	{
		if ( reporech.a == 0.0 )
			fhilfe = atof ( reporech.a_kun ) ;
		else
			fhilfe = reporech.a ;

		if ( fhilfe == a_bas.a )
		{	/* dummy-Aktion */ } 
		else
		{
		    memcpy ( &a_bas, &a_bas_null , sizeof ( struct A_BAS )) ;
			a_bas.a = fhilfe ;
			A_bas.opena_bas() ;
			A_bas.lesea_bas() ;
		}
		sprintf(szTemp2, "%d", a_bas.ag );
		LlDefineVariableExt(hJob, "rtab.a_bas.ag", szTemp2, LL_NUMERIC, NULL);
	}
// WG
	if (LlPrintIsVariableUsed(hJob, "rtab.a_bas.wg"))
	{
		if ( reporech.a == 0.0 )
			fhilfe = atof ( reporech.a_kun ) ;
		else
			fhilfe = reporech.a ;

		if ( fhilfe == a_bas.a )
		{	/* dummy-Aktion */ } 
		else
		{
		    memcpy ( &a_bas, &a_bas_null , sizeof ( struct A_BAS )) ;
			a_bas.a = fhilfe ;
			A_bas.opena_bas() ;
			A_bas.lesea_bas() ;
		}
		sprintf(szTemp2, "%d", a_bas.wg );
		LlDefineVariableExt(hJob, "rtab.a_bas.wg", szTemp2, LL_NUMERIC, NULL);
	}
// HWG
	if (LlPrintIsVariableUsed(hJob, "rtab.a_bas.hwg"))
	{
		if ( reporech.a == 0.0 )
			fhilfe = atof ( reporech.a_kun ) ;
		else
			fhilfe = reporech.a ;

		if ( fhilfe == a_bas.a )
		{	/* dummy-Aktion */ } 
		else
		{
		    memcpy ( &a_bas, &a_bas_null , sizeof ( struct A_BAS )) ;
			a_bas.a = fhilfe ;
			A_bas.opena_bas() ;
			A_bas.lesea_bas() ;
		}
		sprintf(szTemp2, "%d", a_bas.hwg );
		LlDefineVariableExt(hJob, "rtab.a_bas.hwg", szTemp2, LL_NUMERIC, NULL);
	}
// TEIL_SMT
	if (LlPrintIsVariableUsed(hJob, "rtab.a_bas.teil_smt"))
	{
		if ( reporech.a == 0.0 )
			fhilfe = atof ( reporech.a_kun ) ;
		else
			fhilfe = reporech.a ;

		if ( fhilfe == a_bas.a )
		{	/* dummy-Aktion */ } 
		else
		{
		    memcpy ( &a_bas, &a_bas_null , sizeof ( struct A_BAS )) ;
			a_bas.a = fhilfe ;
			A_bas.opena_bas() ;
			A_bas.lesea_bas() ;
		}
		sprintf(szTemp2, "%d", a_bas.teil_smt );
		LlDefineVariableExt(hJob, "rtab.a_bas.teil_smt", szTemp2, LL_NUMERIC, NULL);
	}

// 190310 E

	if (LlPrintIsVariableUsed(hJob, "rtab.a"))
	{
		sprintf(szTemp2, "%.0lf", reporech.a );
		LlDefineVariableExt(hJob, "rtab.a", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.Nutzer"))	// 060905
	{
		LlDefineVariableExt(hJob, "rtab.Nutzer",LNutzer, LL_TEXT, NULL);
	}


	if (LlPrintIsVariableUsed(hJob, "rtab.nachkpreis"))	// 160905
	{
		sprintf(szTemp2, "%d", dnachkpreis );
		LlDefineVariableExt(hJob, "rtab.nachkpreis",szTemp2, LL_NUMERIC, NULL);
	}

	if (LlPrintIsVariableUsed(hJob, "rtab.a_bz1"))
	{
		LlDefineVariableExt(hJob, "rtab.a_bz1",reporech.a_bz1, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.a_bz2"))
	{
		LlDefineVariableExt(hJob, "rtab.a_bz2",reporech.a_bz2, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.lpos_txt"))
	{
		sprintf(szTemp2, "%.0ld", reporech.lpos_txt );
		LlDefineVariableExt(hJob, "rtab.lpos_txt", szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen (hJob, "lpos", "V");



// 151014 A 
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_rech1_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_rech2_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_rech3_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_rech4_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_rech5_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_rech6_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_rech7_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_rech8_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_rech9_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_rech10_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");

	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_ls1_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_ls2_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_ls3_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_ls4_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_ls5_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_ls6_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_ls7_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_ls8_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_ls9_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.txt_ls10_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "V");

// 151014 E

// 031114 A 
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.def1"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def1 );
		LlDefineVariableExt(hJob, "aufpt_verkfrage.def1", szTemp2, LL_NUMERIC, NULL);
	}

	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.def2"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def2 );
		LlDefineVariableExt(hJob, "aufpt_verkfrage.def2", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.def3"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def3 );
		LlDefineVariableExt(hJob, "aufpt_verkfrage.def3", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.def4"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def4 );
		LlDefineVariableExt(hJob, "aufpt_verkfrage.def4", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.def5"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def5 );
		LlDefineVariableExt(hJob, "aufpt_verkfrage.def5", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.def6"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def6 );
		LlDefineVariableExt(hJob, "aufpt_verkfrage.def6", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.def7"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def7 );
		LlDefineVariableExt(hJob, "aufpt_verkfrage.def7", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.def8"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def8 );
		LlDefineVariableExt(hJob, "aufpt_verkfrage.def8", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.def9"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def9 );
		LlDefineVariableExt(hJob, "aufpt_verkfrage.def9", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "aufpt_verkfrage.def10"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def10 );
		LlDefineVariableExt(hJob, "aufpt_verkfrage.def10", szTemp2, LL_NUMERIC, NULL);
	}

// 101014
	if (LlPrintIsVariableUsed(hJob, "rtab.txt_rech"))
	{
		sprintf(szTemp2, "%.0ld", reporech.txt_rech );
		LlDefineVariableExt(hJob, "rtab.txt_rech", szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen (hJob, "atexte", "V");

	if (LlPrintIsVariableUsed(hJob, "rtab.kopf_txt"))
	{
		sprintf(szTemp2, "%.0ld", reporech.kopf_txt );
		LlDefineVariableExt(hJob, "rtab.kopf_txt", szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen (hJob, "kopf" , "V" );

	if (LlPrintIsVariableUsed(hJob, "rtab.fuss_txt"))
	{
		sprintf(szTemp2, "%.0ld", reporech.fuss_txt );
		LlDefineVariableExt(hJob, "rtab.fuss_txt", szTemp2, LL_NUMERIC, NULL);

	}
	textsetzen (hJob, "fuss" , "V" );

	if (LlPrintIsVariableUsed(hJob, "rtab.a_kun"))
	{
	    LlDefineVariableExt(hJob, "rtab.a_kun",reporech.a_kun, LL_TEXT, NULL);
	}

/* Quatsch als Variable .....
	if (LlPrintIsVariableUsed(hJob, "rtab.ean"))	// 110913
	{
		a_kun_gx_komplett();
	    LlDefineVariableExt(hJob, "rtab.ean",eanstring , LL_NUMERIC, NULL);
	}
< ----- */

	if (LlPrintIsVariableUsed(hJob, "rtab.zu_stoff"))
	{
		LlDefineVariableExt(hJob, "rtab.zu_stoff",reporech.zu_stoff, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.gn_pkt_gbr"))
	{
	
		sprintf(szTemp2, "%1.2lf", reporech.gn_pkt_gbr );
		LlDefineVariableExt(hJob, "rtab.gn_pkt_gbr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.adr2"))
	{
	   	sprintf(szTemp2, "%1.0lf", reporech.adr2 );
		LlDefineVariableExt(hJob, "rtab.adr2", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.adr3"))
	{
		sprintf(szTemp2, "%1.0lf", reporech.adr3 );
		LlDefineVariableExt(hJob, "rtab.adr3", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.snetto1"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.snetto1 );
		LlDefineVariableExt(hJob, "rtab.snetto1", szTemp2, LL_NUMERIC, NULL);
	}	
	if (LlPrintIsVariableUsed(hJob, "rtab.snetto2"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.snetto2 );
		LlDefineVariableExt(hJob, "rtab.snetto2", szTemp2, LL_NUMERIC, NULL);;
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.snetto3"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.snetto3 );
		LlDefineVariableExt(hJob, "rtab.snetto3", szTemp2, LL_NUMERIC, NULL);;
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.smwst1"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.smwst1 );
		LlDefineVariableExt(hJob, "rtab.smwst1", szTemp2, LL_NUMERIC, NULL);;
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.smwst2"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.smwst2 );
		LlDefineVariableExt(hJob, "rtab.smwst2", szTemp2, LL_NUMERIC, NULL);;
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.smwst3"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.smwst3 );
		LlDefineVariableExt(hJob, "rtab.smwst3", szTemp2, LL_NUMERIC, NULL);;
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.smwst1p"))
	{
	    LlDefineVariableExt(hJob, "rtab.smwst1p",reporech.smwst1p, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.smwst2p"))
	{
	    LlDefineVariableExt(hJob, "rtab.smwst2p",reporech.smwst2p, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.smwst3p"))
	{
	    LlDefineVariableExt(hJob, "rtab.smwst3p",reporech.smwst3p, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.smwst1s"))
	{
		sprintf(szTemp2, "%1d", reporech.smwst1s );
		LlDefineVariableExt(hJob, "rtab.smwst1s", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.smwst2s"))
	{
		sprintf(szTemp2, "%1d", reporech.smwst2s );
		LlDefineVariableExt(hJob, "rtab.smwst2s", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.smwst3s"))
	{
		sprintf(szTemp2, "%1d", reporech.smwst3s );
		LlDefineVariableExt(hJob, "rtab.smwst3s", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.rech_summ"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.rech_summ );
		LlDefineVariableExt(hJob, "rtab.rech_summ", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.end_rab"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.end_rab );
		LlDefineVariableExt(hJob, "rtab.end_rab", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.zahl_betr"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.zahl_betr );
		LlDefineVariableExt(hJob, "rtab.zahl_betr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.fil"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.fil );
		LlDefineVariableExt(hJob, "rtab.fil", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.belk_txt"))
	{
		LlDefineVariableExt(hJob, "rtab.belk_txt",reporech.belk_txt, LL_TEXT, NULL);
    }
	if (LlPrintIsVariableUsed(hJob, "rtab.belf_txt"))
	{
		LlDefineVariableExt(hJob, "rtab.belf_txt",reporech.belf_txt, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.zako_txt"))
	{
		LlDefineVariableExt(hJob, "rtab.zako_txt",reporech.zako_txt, LL_TEXT, NULL);
	}
	zahlweisen( hJob, reporech.zako_txt, "V" ) ;

	if (LlPrintIsVariableUsed(hJob, "rtab.pmwsts"))
	{
	   LlDefineVariableExt(hJob, "rtab.pmwsts",reporech.pmwsts, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.kun"))
	{
	
		sprintf(szTemp2, "%1.0ld", reporech.kun );
		LlDefineVariableExt(hJob, "rtab.kun", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.ges_rabp"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.ges_rabp );
		LlDefineVariableExt(hJob, "rtab.ges_rabp", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.mdn"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.mdn );
		LlDefineVariableExt(hJob, "rtab.mdn", szTemp2, LL_NUMERIC, NULL);
  	}
	if (LlPrintIsVariableUsed(hJob, "rtab.ust_id"))
	{
		LlDefineVariableExt(hJob, "rtab.ust_id",reporech.ust_id, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.nr_bei_rech"))
	{
		LlDefineVariableExt(hJob, "rtab.nr_bei_rech",reporech.nr_bei_rech, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.alt_pr"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.alt_pr );
		LlDefineVariableExt(hJob, "rtab.alt_pr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.lsret"))
	{
	    LlDefineVariableExt(hJob, "rtab.lsret", reporech.lsret, LL_TEXT, NULL);
	}

// 290609
	if (LlPrintIsVariableUsed(hJob, "rtab.lsnr"))
	{
	
		sprintf(szTemp2, "%1.0ld", reporech.lsnr );
		LlDefineVariableExt(hJob, "rtab.lsnr", szTemp2, LL_NUMERIC, NULL);
	}

	if (LlPrintIsVariableUsed(hJob, "rtab.auf_ext"))
	{
		LlDefineVariableExt(hJob, "rtab.auf_ext",reporech.auf_ext, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.a_typ"))
	{
		sprintf(szTemp2, "%d", reporech.a_typ );	// 230813 : hier stand bisher : 1.0lf
		LlDefineVariableExt(hJob, "rtab.a_typ", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.kun_nam"))
	{
		LlDefineVariableExt(hJob, "rtab.kun_nam",reporech.kun_nam, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.bbn"))
	{
			LlDefineVariableExt(hJob, "rtab.bbn",reporech.bbn, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.mdnadr"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.mdnadr );
		LlDefineVariableExt(hJob, "rtab.mdnadr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.rech_dat"))
	{
	
		LlDefineVariableExt(hJob, "rtab.rech_dat", sqldatamger (reporech.rech_dat,szTemp2), LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.order3"))
	{
	
		sprintf(szTemp2, "%1d", reporech.order3 );
		LlDefineVariableExt(hJob, "rtab.order3", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.rech_nr"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.rech_nr );
		LlDefineVariableExt(hJob, "rtab.rech_nr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.erf_kz"))
	{
	    LlDefineVariableExt(hJob, "rtab.erf_kz",reporech.erf_kz, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.lief_me"))
	{
		sprintf(szTemp2, "%1.3lf", reporech.lief_me );
		LlDefineVariableExt(hJob, "rtab.lief_me", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.lief_me_bz"))
	{
	    LlDefineVariableExt(hJob, "rtab.lief_me_bz",reporech.lief_me_bz, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.vk_pr"))
	{
		if ( dnachkpreis == 4 )	// 160905 
		{
			sprintf(szTemp2, "%1.4lf", reporech.vk_pr );
		}
		else
		{
			if ( dnachkpreis == 3 )
			{
				sprintf(szTemp2, "%1.3lf", reporech.vk_pr );
			}
			else
			{
				sprintf(szTemp2, "%1.2lf", reporech.vk_pr );
			}
		}
		LlDefineVariableExt(hJob, "rtab.vk_pr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.auf_me"))
	{
		sprintf(szTemp2, "%1.3lf", reporech.auf_me );
		LlDefineVariableExt(hJob, "rtab.auf_me", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.sa_kz"))
	{
	  LlDefineVariableExt(hJob, "rtab.sa_kz",reporech.sa_kz, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.order2"))
	{
		sprintf(szTemp2, "%1d", reporech.order2 );
	    LlDefineVariableExt(hJob, "rtab.order2", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.einz_rabp"))
	{
	
		sprintf(szTemp2, "%1.2lf", reporech.einz_rabp );
		LlDefineVariableExt(hJob, "rtab.einz_rabp", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.zeil_sum"))
	{
	
                sprintf(szTemp2, "%1.2lf", reporech.zeil_sum );
		LlDefineVariableExt(hJob, "rtab.zeil_sum", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.prab_wert"))
	{
	
		sprintf(szTemp2, "%1.2lf", reporech.prab_wert );
		LlDefineVariableExt(hJob, "rtab.prab_wert", szTemp2, LL_NUMERIC, NULL);
	}


// 251113
	if (LlPrintIsVariableUsed(hJob, "kun.zahl_ziel"))
	{
		sprintf(szTemp2, "%1d", kun.zahl_ziel );
		LlDefineVariableExt(hJob, "kun.zahl_ziel", szTemp2, LL_NUMERIC, NULL);
	}
// 231213 A
	if ( !dietzosep )
	{
		if (LlPrintIsVariableUsed(hJob, "kun.tagesepa"))
		{
			sprintf(szTemp2, "%1d", kun.tagesepa );
			LlDefineVariableExt(hJob, "kun.tagesepa", szTemp2, LL_NUMERIC, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "kun.mandatref"))
		{
			sprintf(szTemp2, "%s", kun.mandatref );
			LlDefineVariableExt(hJob, "kun.mandatref", szTemp2, LL_TEXT, NULL);
		}
	}
// 231213 E



// 300608 A

	if (LlPrintIsVariableUsed(hJob, "kun.ls_fuss_txt"))
	{
		sprintf(szTemp2, "%1.0ld", kun.ls_fuss_txt );
		LlDefineVariableExt(hJob, "kun.ls_fuss_txt",szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "kun.rech_fuss_txt"))
	{
		sprintf(szTemp2, "%1.0ld", kun.rech_fuss_txt );
		LlDefineVariableExt(hJob, "kun.rech_fuss_txt",szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen ( hJob,"LFX", "V" );	
	textsetzen ( hJob,"RFX", "V" );	

// 300608 E

//	030912 
	if (LlPrintIsVariableUsed(hJob, "kun.zahl_art"))
	{
		sprintf(szTemp2, "%01.0d", kun.zahl_art );
		LlDefineVariableExt(hJob, "kun.zahl_art",szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "kun.bank_kun"))
	{
		LlDefineVariableExt(hJob, "kun.bank_kun",kun.bank_kun, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "kun.blz"))
	{
		sprintf(szTemp2, "%01.0d", kun.blz );
		LlDefineVariableExt(hJob, "kun.blz",szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "kun.kto_nr"))
	{
		LlDefineVariableExt(hJob, "kun.kto_nr",kun.kto_nr, LL_TEXT, NULL);
	}

	if (LlPrintIsVariableUsed(hJob, "kun.sprache"))
	{
		sprintf(szTemp2, "%01.0d", kun.sprache );
		LlDefineVariableExt(hJob, "kun.sprache",szTemp2, LL_NUMERIC, NULL);
	}
// 251114
	if (LlPrintIsVariableUsed(hJob, "kun.sprache2"))
	{
		sprintf(szTemp2, "%01.0d", kun.sprache2 );
		LlDefineVariableExt(hJob, "kun.sprache2",szTemp2, LL_NUMERIC, NULL);
	}

// 211010
	if (LlPrintIsVariableUsed(hJob, "kun.kun_gr1"))
	{
		sprintf(szTemp2, "%01.0d", kun.kun_gr1 );
		LlDefineVariableExt(hJob, "kun.kun_gr1",szTemp2, LL_NUMERIC, NULL);
	}

// 051210
	if (LlPrintIsVariableUsed(hJob, "kun.kun_gr2"))
	{
		sprintf(szTemp2, "%01.0d", kun.kun_gr2 );
		LlDefineVariableExt(hJob, "kun.kun_gr2",szTemp2, LL_NUMERIC, NULL);
	}

	
// 211010
	if (LlPrintIsVariableUsed(hJob, "kun.kun_krz1"))
	{
		LlDefineVariableExt(hJob, "kun.kun_krz1",kun.kun_krz1, LL_TEXT, NULL);
	}


	// 220910
	if (LlPrintIsVariableUsed(hJob, "kun.kun_bran2"))
	{
		LlDefineVariableExt(hJob, "kun.kun_bran2",kun.kun_bran2, LL_TEXT, NULL);
	}


	if (LlPrintIsVariableUsed(hJob, "rtab.rpos_txt"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.rpos_txt );
		LlDefineVariableExt(hJob, "rtab.rpos_txt",szTemp2, LL_NUMERIC, NULL);
	}
	//220504 : retourentext nur behandeln, wenn er groesser als 0 ist 
	if ( reporech.rpos_txt > 0 ) textsetzen ( hJob,"rpos", "V" );

	if (LlPrintIsVariableUsed(hJob, "rtab.ktx_jebel"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.ktx_jebel );
		LlDefineVariableExt(hJob, "rtab.ktx_jebel", szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen ( hJob,"KTX", "V" );	//120504
	if (LlPrintIsVariableUsed(hJob, "rtab.ftx_jebel"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.ftx_jebel );
		LlDefineVariableExt(hJob, "rtab.ftx_jebel", szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen ( hJob,"FTX", "V" );	// 120504

	if (LlPrintIsVariableUsed(hJob, "rtab.lief_art"))
	{
		sprintf(szTemp2, "%1.0lf", reporech.lief_art );
		LlDefineVariableExt(hJob, "rtab.lief_art", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.tel"))
	{
		LlDefineVariableExt(hJob, "rtab.tel",reporech.tel, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.fax"))
	{
	    LlDefineVariableExt(hJob, "rtab.fax",reporech.fax, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.auf_me_bz"))
	{
	   LlDefineVariableExt(hJob, "rtab.auf_me_bz",reporech.auf_me_bz, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.lief_me1"))
	{
		sprintf(szTemp2, "%1.3lf", reporech.lief_me1 );
		LlDefineVariableExt(hJob, "rtab.lief_me1", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.red_inh"))	// 110608 
	{

		double red_inh = 0.0 ;
		int  istat = 0 ;
		if ( reporech.a_typ == 1 )
		{
			a_hndw.a = reporech.a ;
			istat = a_hndw_class.opena_hndw () ;
			if ( !istat ) istat = a_hndw_class.lesea_hndw () ;
			if ( !istat ) red_inh = a_hndw.inh ;
		}
		if ( reporech.a_typ == 2 )
		{
			a_eig.a = reporech.a ;
			istat = a_eig_class.opena_eig () ;
			if ( !istat ) istat = a_eig_class.lesea_eig () ;
			if ( !istat ) red_inh = a_eig.inh ;
		}
		if ( reporech.a_typ == 3 )
		{
			a_eig_div.a = reporech.a ;
			istat = a_eig_div_class.opena_eig_div () ;
			if ( !istat ) istat = a_eig_div_class.lesea_eig_div () ;
			if ( !istat ) red_inh = a_eig_div.inh ;
		}

		sprintf(szTemp2, "%1.3lf", red_inh );
		LlDefineVariableExt(hJob, "rtab.red_inh", szTemp2, LL_NUMERIC, NULL);
	}


	if (LlPrintIsVariableUsed(hJob, "rtab.ls_charge"))
	{
		LlDefineVariableExt(hJob, "rtab.ls_charge",reporech.ls_charge, LL_TEXT, NULL);
	}

	if (LlPrintIsVariableUsed(hJob, "rtab.kunsteunum"))
	{
		LlDefineVariableExt(hJob, "rtab.kunsteunum",kun.ust_nummer, LL_TEXT, NULL);
	}

// 250806 A
	if (LlPrintIsVariableUsed(hJob, "rtab.kunfak_kz"))
	{
		LlDefineVariableExt(hJob, "rtab.kunfak_kz",kun.fak_kz, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rtab.kunfak_nr"))
	{
		sprintf(szTemp2, "%1.0ld", kun.fak_nr );
		LlDefineVariableExt(hJob, "rtab.kunfak_nr", szTemp2, LL_NUMERIC, NULL);
	}
// 250806 E

// 021204  
	if (LlPrintIsVariableUsed(hJob, "rtab.silent_pr"))
	{
		sprintf(szTemp2, "%d", dsilent );
		LlDefineVariableExt(hJob, "rtab.silent_pr",szTemp2, LL_NUMERIC, NULL);
	}

// 081204  
	if (LlPrintIsVariableUsed(hJob, "rtab.format"))
	{
		LlDefineVariableExt(hJob, "rtab.format",Internformat, LL_NUMERIC, NULL);
	}
// 010507  
	if (LlPrintIsVariableUsed(hJob, "rtab.blg_typ"))
	{
		LlDefineVariableExt(hJob, "rtab.blg_typ",blgtyp, LL_TEXT, NULL);
	}

// 081204  
	if (LlPrintIsVariableUsed(hJob, "rtab.ohnemitrab"))
	{
		LlDefineVariableExt(hJob, "rtab.ohnemitrab",ohnemitrab, LL_NUMERIC, NULL);
	}

// 240205  
	if (LlPrintIsVariableUsed(hJob, "rtab.duplikat"))
	{
		sprintf(szTemp2, "%d", dduplikat );
		LlDefineVariableExt(hJob, "rtab.duplikat",szTemp2, LL_NUMERIC, NULL);
	}

// 291010
	if (LlPrintIsVariableUsed(hJob, "rtab.teil_smt"))
		if ( rech.teil_smt == 0 )
			sprintf(szTemp2, "0");
		else
			sprintf(szTemp2, "%d", rech.teil_smt );
		LlDefineVariableExt(hJob, "rtab.teil_smt",szTemp2, LL_NUMERIC, NULL);

// 150605  A , 201207 : toruenkram dazu 
// 050213 : auf dazu 
		// 131113 / 281113 : pers_nam dazu
	if (LlPrintIsVariableUsed(hJob, "rtab.hinweis")
		|| LlPrintIsVariableUsed(hJob, "rtab.lieferzeit") 
		|| LlPrintIsVariableUsed(hJob, "rtab.tour") 
		|| LlPrintIsVariableUsed(hJob, "rtab.auf")
		|| LlPrintIsVariableUsed(hJob, "rtab.lpers_nam")
		|| LlPrintIsVariableUsed(hJob, "rtab.apers_nam")
		|| LlPrintIsVariableUsed(hJob, "rtab.tour_bez"))
	{
		if ( blgtyp[0] == 'N' || blgtyp[0] == 'U' )
		{	// 130707 Hinweis erst mal platt machen 
// 071108 sprintf(lsk.hinweis," ");sprintf(lsk.lieferzeit,"");sprintf(tou.tou_bz,"");tou.tou = 0;
			if ((wechselblg != reporech.lsnr) || ( wechseltyp[0] != reporech.lsret[0] ))
			{
				sprintf ( lsk.hinweis , " " );	// 071108 : loeschen nur noch einmalig am Anfang eines neuen Beleges
				sprintf ( lsk.lieferzeit , " " );
				lsk.auf = 0 ;	// 050213
				sprintf ( tou.tou_bz ," " ) ;
				tou.tou = 0 ;
				sprintf ( lsk.pers_nam,"");	// 131113
				sprintf ( aufk.pers_nam,"");	// 281113

				wechselblg = reporech.lsnr ;
				wechseltyp[0] = reporech.lsret[0] ;
				if ( blgtyp[0] == 'N' )
				{	// funktioniert nur bei Einzelbeleg !!!!
					angk.mdn = (short) reporech.mdn ;
					angk.fil = 0 ;
 					angk.ang  = reporech.rech_nr ;
					Angk.openangk () ;
					int di = Angk.leseangk (0);
					if (!di )
					{
// 201207 : Tour lesen 
						tou.tou = angk.tou ;
						if (  LlPrintIsVariableUsed(hJob, "rtab.tour") 
								|| LlPrintIsVariableUsed(hJob, "rtab.tour_bez"))
						{
							Tou.opentou() ;
							di = Tou.lesetou(0) ;
							if ( di )
							{
								tou.tou = 0 ;
								sprintf ( tou.tou_bz ," " ) ;
							}
						}
					}
					else
					{
						memcpy ( &lsk,&lsk_null, sizeof(struct LSK));
						sprintf ( lsk.hinweis , " " );
						sprintf ( lsk.lieferzeit , " " ) ;	// 201207
					}
				}
				if ( blgtyp[0] == 'U' )
				{	// funktioniert nur bei Einzelbeleg !!!!
					aufk.mdn = (short) reporech.mdn ;
					aufk.fil = 0 ;
 					aufk.auf  = reporech.rech_nr ;
					Aufk.openaufk () ;
					int di = Aufk.leseaufk (0);
					if (!di )
					{
// 201207 : Tour lesen 
						tou.tou = aufk.tou ;
						if (  LlPrintIsVariableUsed(hJob, "rtab.tour") 
								|| LlPrintIsVariableUsed(hJob, "rtab.tour_bez"))
						{
							Tou.opentou() ;
							di = Tou.lesetou(0) ;
							if ( di )
							{
								tou.tou = 0 ;
								sprintf ( tou.tou_bz ," " ) ;
							}
						}
					}
					else
					{
						memcpy ( &lsk,&lsk_null, sizeof(struct LSK));
						sprintf ( lsk.hinweis , " " );
						sprintf ( lsk.lieferzeit , " " );	// 201207
					}
				}
			}
		}
		else	// bisheriger Standard-Ablauf 
		{
			if ((wechselblg != reporech.lsnr) || ( wechseltyp[0] != reporech.lsret[0] ))
			{
				// 201207 : erst mal platt machen 
				sprintf ( tou.tou_bz ," " );
				tou.tou = 0 ;
				sprintf ( lsk.hinweis,"");		// 050213 
				sprintf ( lsk.lieferzeit,"");	// 050213
				lsk.auf = 0 ;					// 050213
				sprintf ( lsk.pers_nam,"");		// 131113
				sprintf ( aufk.pers_nam,"");		// 281113
				wechselblg = reporech.lsnr ;
				wechseltyp[0] = reporech.lsret[0] ;	

				if ( wechseltyp[0] == 'L' )
				{
					lsk.mdn = (short) reporech.mdn ;
 					lsk.ls  = reporech.lsnr ;
					Lsk.openlsk () ;
			
					int di = Lsk.leselsk (0);

					if (!di )
					{
						tou.tou = lsk.tou ;
						if (  LlPrintIsVariableUsed(hJob, "rtab.tour") 
								|| LlPrintIsVariableUsed(hJob, "rtab.tour_bez"))
						{
							Tou.opentou() ;
							di = Tou.lesetou(0) ;
							if ( di )
							{
								tou.tou = 0 ;
								sprintf ( tou.tou_bz ," " ) ;
							}
						}
// 281113 A
						if (  LlPrintIsVariableUsed(hJob, "rtab.apers_nam")	&& ( lsk.auf > 0 ))
						{
							aufk.auf = lsk.auf;
							aufk.mdn = reporech.mdn;
							aufk.fil = 0;
							di = Aufk.openaufk();
							di = Aufk.leseaufk(0);
						}
// 281113 E

					}
					else
					{
						memcpy ( &lsk,&lsk_null, sizeof(struct LSK));
						sprintf ( lsk.hinweis , " " );
						sprintf ( lsk.lieferzeit , " " );
					}
				}
				else
				{
// 200706 
					retk.mdn = (short) reporech.mdn ;
 					retk.ret  = reporech.lsnr ;
					Retk.openretk () ;
			
					int di = Retk.leseretk (0);

					if (!di )
					{
						sprintf ( lsk.hinweis, "%s", retk.hinweis )	;
						sprintf ( lsk.lieferzeit , " " ) ;	// 201207
					}
					else
					{
						memcpy ( &lsk,&lsk_null, sizeof(struct LSK));
						sprintf ( lsk.hinweis , " " );
						sprintf ( lsk.lieferzeit , " " ) ;
					}
				
				}
			}
		}

		if (LlPrintIsVariableUsed(hJob, "rtab.hinweis"))
		{
			sprintf(szTemp2, "%s", lsk.hinweis );
			LlDefineVariableExt(hJob, "rtab.hinweis",szTemp2, LL_TEXT, NULL);
		}
		// 201207 A 
		if (LlPrintIsVariableUsed(hJob, "rtab.lieferzeit"))
		{
			sprintf(szTemp2, "%s", lsk.lieferzeit );
			LlDefineVariableExt(hJob, "rtab.lieferzeit",szTemp2, LL_TEXT, NULL);
		}
		if (LlPrintIsVariableUsed(hJob, "rtab.tour"))
		{
			sprintf ( szTemp2, "%d" , tou.tou ) ;
			LlDefineVariableExt(hJob, "rtab.tour",szTemp2, LL_NUMERIC, NULL);
		}

		if (LlPrintIsVariableUsed(hJob, "rtab.tour_bez"))
		{
			LlDefineVariableExt(hJob, "rtab.tour_bez",tou.tou_bz, LL_TEXT, NULL);
		}
// 201207 E
// 131113 A
		if (LlPrintIsVariableUsed(hJob, "rtab.lpers_nam"))
		{
				sprintf(szTemp2, "%s", lsk.pers_nam );

			LlDefineVariableExt(hJob, "rtab.lpers_nam",szTemp2 , LL_TEXT, NULL);
		}
// 131113 E
// 281113 A
		if (LlPrintIsVariableUsed(hJob, "rtab.apers_nam"))
		{
				sprintf(szTemp2, "%s", aufk.pers_nam );

			LlDefineVariableExt(hJob, "rtab.apers_nam",szTemp2 , LL_TEXT, NULL);
		}
// 281113 E

		if (LlPrintIsVariableUsed(hJob, "rtab.auf"))	// 050213
		{
			sprintf ( szTemp2, "%d" , lsk.auf ) ;
			LlDefineVariableExt(hJob, "rtab.auf",szTemp2, LL_NUMERIC, NULL);
		}
	}

// 150605  E 

}

static char eanhilfe[20] ;

char * eange ( char * eingabe)
{

int point1 ;
int wicht  ; 
int platz  ;
char testchar;
int summe  ;

  sprintf ( eanhilfe, "%s",  eingabe );
  point1 = 0;
  summe  = 0;
  wicht  = 1;
  while ( point1 < 12 )
  {

	testchar = eingabe[point1] ;
//  platz    = testchar ;

	switch (testchar)
	{
		case '0' : platz = 0 ; break ;
	    case '1' : platz = 1 ; break ;
	    case '2' : platz = 2 ; break ;
	    case '3' : platz = 3 ; break ;
	    case '4' : platz = 4 ; break ;
	    case '5' : platz = 5 ; break ;
	    case '6' : platz = 6 ; break ;
	    case '7' : platz = 7 ; break ;
	    case '8' : platz = 8 ; break ;
	    case '9' : platz = 9 ; break ;
		default  : platz = 0 ; break ;
	};

      summe = summe + ( platz * wicht );
      if ( wicht == 1 )
	  {
		wicht = 3 ;
	  }
      else
	  {
		wicht = 1 ;
	  }
      point1 ++ ;
  }

  wicht = summe / 10  ;            // Quotient 
  platz = summe - ( wicht * 10 );  // modul 10 
  if ( platz == 0 )
  {
      eanhilfe [12] = '0' ;
      eanhilfe [13] = '\0' ;
  }
  else
  {
      sprintf ( eanhilfe + 12, "%d", ( 10 - platz ));
  }
  return  eanhilfe ;
}

char * ean_machen ( void )
{
char in_feld [ 20 ] ;

	if ( eanausakun == 0 )	// 110913
	{
		sprintf ( eanhilfe, "" );
		return eanhilfe ;
	}

	// 8-stell ean direkt eingetragen 
	if ( a_kun.ean_vk  > 9999999.0L && a_kun.ean_vk < 100000000.0L)
	{
		sprintf ( eanhilfe, "%1.0f", a_kun.ean_vk );
		return eanhilfe ;
	}
	//  es steht 12 stellig was drin ...
    if ( a_kun.ean_vk  > 99999999999.0L && a_kun.ean_vk < 1000000000000.0L)
	{
		sprintf ( in_feld, "%1.0f", a_kun.ean_vk );
		sprintf ( eanhilfe, "%s", eange (in_feld));
		return eanhilfe  ;
	}
	// es steht mind. 13 stellig was drin 
    if ( a_kun.ean_vk  > 999999999999.0L )
	{
		sprintf ( in_feld, "%1.0f", a_kun.ean_vk );
		in_feld[12] = '\0' ;
		sprintf ( eanhilfe, "%s", eange (in_feld));
		return  eanhilfe ;
	}

// dann a_kun.ean testen 
	// 8-stell ean direkt eingetragen 
	if ( a_kun.ean  > 9999999.0L && a_kun.ean < 100000000.0L)
	{
		sprintf ( eanhilfe, "%1.0f", a_kun.ean );
		return eanhilfe ;
	}
	//  es steht 12 stellig was drin ...
    if ( a_kun.ean  > 99999999999.0L && a_kun.ean < 1000000000000.0L)
	{
		sprintf ( in_feld, "%1.0f", a_kun.ean );
		sprintf ( eanhilfe, "%s", eange (in_feld));
		return  eanhilfe  ;
	}
	// es steht mind. 13 stellig was drin 
    if ( a_kun.ean  > 999999999999.0L )
	{
		sprintf ( in_feld, "%1.0f", a_kun.ean );
		in_feld[12] = '\0' ;
		sprintf ( eanhilfe, "%s", eange (in_feld));
		return  eanhilfe  ;
	}
	return eanhilfe ;	// war mit blank vorgeladen 
}

char * eanholen(void)
{

double fhilfe ;

	if ( eanausakun == 0 )	// 110913
	{
		sprintf ( eanhilfe, "" ) ;
		return eanhilfe;
	}

	if ( reporech.a == 0.0 )
	{	// falls reporech.a == reporech.a_kun, dann wird reporech.a vom uebergeordneten Programm = 0 gestetzt
		fhilfe = atof ( reporech.a_kun ) ;
	}
	else
	{
		fhilfe = reporech.a ;
	} 

	if ( a_kun.a == fhilfe )
	{
		// es bereits alles passiert
		return eanhilfe;
	}
	else
	{

		sprintf ( eanhilfe, "" ) ;	// 110913 : Initialisierung neu strukturiert
		memcpy ( &a_kun, &a_kun_null, sizeof(struct A_KUN )) ;
		a_kun.kun = reporech.kun ;
		a_kun.mdn = (short) reporech.mdn ;
		a_kun.a   = fhilfe ;
		sprintf ( a_kun.kun_bran2 ,"0" ) ;
		int di = a_kun_class.opena_kun () ;
		di = a_kun_class.lesea_kun() ;
		if ( di )
		{
			a_kun.kun = 0 ;
			a_kun.mdn = (short ) reporech.mdn ;
			a_kun.a   = fhilfe ;
			sprintf ( a_kun.kun_bran2 , "%s" , kun.kun_bran2 ) ;
			di = a_kun_class.opena_kun () ;
			di = a_kun_class.lesea_kun() ;
		}
		if ( di )
		{
			a_kun.ean = 0 ;
			a_kun.ean_vk = 0 ;
		}
	}

	if ( a_kun.ean == 0 )
		return eanhilfe ;	//  Leerstring

	return ean_machen () ;

}


void a_kun_gx_komplett ( void )	// 110913
{

	if ( eanausakungx == 0 )
	{
		sprintf ( eanstring,"");
		return ;	// Leerstring
	}
	int retakun = 100;
	char ean_nr_hilfe[33] ;


double fhilfe ;

	if ( reporech.a == 0.0 )
	{	// falls reporech.a == reporech.a_kun, dann wird reporech.a vom uebergeordneten Programm = 0 gestetzt
		fhilfe = atof ( reporech.a_kun ) ;
	}
	else
	{
		fhilfe = reporech.a ;
	} 

	if ( a_kun_gx.a == fhilfe )
	{
		// es bereits alles passiert
		return;
	}

	memcpy ( &a_kun_gx, &a_kun_gx_null, sizeof(struct A_KUN_GX )) ;
	sprintf ( eanstring,"");

	a_kun_gx.mdn = reporech.mdn;
	a_kun_gx.a = fhilfe;
	a_kun_gx.kun = reporech.kun ;
	sprintf ( a_kun_gx.kun_bran2 , "0" ) ;
	retakun = a_kun_gx_class.opena_kun_gx() ;
	retakun = a_kun_gx_class.lesea_kun_gx() ;
	while ( ! retakun ) 
	{
		if (a_kun_gx.ean  > 99999999999 && a_kun_gx.ean < 10000000000000 )
			break ;
		if (a_kun_gx.ean1  > 99999999999 && a_kun_gx.ean1 < 10000000000000 )
		{
			a_kun_gx.ean = a_kun_gx.ean1 ;
			break ;
		}
		retakun = a_kun_gx_class.lesea_kun_gx() ;
	}
	if ( retakun ) 
	{
		a_kun_gx.mdn = reporech.mdn ;
		a_kun_gx.a = fhilfe ;
		a_kun_gx.kun = 0 ;
		sprintf ( a_kun_gx.kun_bran2 , kun.kun_bran2 ) ;
		retakun = a_kun_gx_class.opena_kun_gx() ;
		retakun = a_kun_gx_class.lesea_kun_gx() ;
		while ( ! retakun ) 
		{
			if (a_kun_gx.ean  > 99999999999 && a_kun_gx.ean < 10000000000000 )
			{
				break ;
			}
			if (a_kun_gx.ean1  > 99999999999 && a_kun_gx.ean1 < 10000000000000 )
			{
				a_kun_gx.ean = a_kun_gx.ean1 ;
				break ;
			}
			retakun = a_kun_gx_class.lesea_kun_gx() ;
		}
	}
	if ( retakun )        // totaler, eigentlich unzulaessiger Noptnagel, jedoch der "Standard" bei der Spezialfirma Lackmann
	{ 
//			return ( ean_machen ( interna)) ;
		a_kun_gx.mdn = reporech.mdn ;
		a_kun_gx.a = fhilfe ;
		a_kun_gx.kun = 0 ;
		sprintf ( a_kun_gx.kun_bran2 ,"%s" , kun.kun_bran2 ) ;
		retakun = a_kun_gx_class.opena_kun_gx() ;
		retakun = a_kun_gx_class.lesea_kun_gx() ;
		while ( ! retakun ) 
		{
			if (a_kun_gx.ean  > 99999999999 && a_kun_gx.ean < 10000000000000 )
			{
				break ;
			}
			if (a_kun_gx.ean1  > 99999999999 && a_kun_gx.ean1 < 10000000000000 )
			{
				a_kun_gx.ean = a_kun_gx.ean1 ;
				break ;
			}
			retakun = a_kun_gx_class.lesea_kun_gx() ;
		}
	}
	if ( retakun )
		return ;	// negativ-return mit Leerstring

	sprintf ( ean_nr_hilfe, "%1.0f", a_kun_gx.ean  ) ;
	if ( ean_nr_hilfe[0] == '2' && ean_nr_hilfe[1] == '8' )				// "28"
	{
// 020804 : prinzipiell Pruefziffer mit "0" ersetzen lt. Wunsch fuer 28er
		ean_nr_hilfe[12] = '0' ;
		ean_nr_hilfe[13] = '\0' ;
        sprintf ( eanstring, "%s", ean_nr_hilfe );
		return;
	}
	sprintf ( eanstring, "%s", eange( ean_nr_hilfe)) ;
}

// 201014

double texteladen(double ehilfe)
{	// Hier kommt man vorbei, wenn a_bas noch nicht aktuell ist und das bedeutet : dieser Artikel 
	// muss initialisiert werden, ichgehe davon aus, das der Artiekl �berhaupt existiert .... 

	memcpy ( &a_bas, &a_bas_null , sizeof ( struct A_BAS )) ;
	a_bas.a = ehilfe ;
	A_bas.opena_bas() ;
	A_bas.lesea_bas() ;
	if ( cfga_kun_txt )		// 200214 
	{
		memcpy ( &a_kun_txt, &a_kun_txt_null , sizeof ( struct A_KUN_TXT ) );
		a_kun_txt.a = ehilfe ;
		a_kun_txt.kun=0;
		A_kun_txt.opena_kun_txt() ;
		A_kun_txt.lesea_kun_txt() ;
	}
	if ( cfga_bas_erw ) 
	{
		memcpy ( &a_bas_erw, &a_bas_erw_null , sizeof ( struct A_BAS_ERW ) );
		a_bas_erw.a = ehilfe ;
		A_bas_erw.opena_bas_erw() ;
		A_bas_erw.lesea_bas_erw() ;
	}
	if ( mitmysql )		// 171014
	{

// 251114
		memcpy ( &asprache, &asprache_null , sizeof ( struct ASPRACHE ) );
		asprache.a = ehilfe ;
		asprache.sprache = kun.sprache2 ;
		Asprache.openasprache() ;
		Asprache.leseasprache() ;
		memcpy ( &asprache2, &asprache , sizeof ( struct ASPRACHE ) );
// 251114
		memcpy ( &asprache, &asprache_null , sizeof ( struct ASPRACHE ) );
		asprache.a = ehilfe ;
		asprache.sprache = kun.sprache ;
		Asprache.openasprache() ;
		Asprache.leseasprache() ;
	}
	return ehilfe ;
}
// 201014 E 

void FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{

	char szTemp3[30] ;
	int ipo ;


	for (  ipo = 1 ; ipo < LEERMATDIM ; ipo ++ )// 041206 9 -> LEERMATDIM
	{


	    sprintf(szTemp2, "leer_rh.a%.0d", ipo );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0lf" , leer_mat_a[ipo - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2, szTemp3, LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_rh.a_bz%.0d", ipo );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			LlDefineFieldExt(hJob, szTemp2  , leer_mat_bz[ipo - 1], LL_TEXT, NULL);
		}

		sprintf(szTemp2, "leer_rh.stk_zu%.0d", ipo );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%1.0ld", leer_mat_sz[ipo - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2, szTemp3, LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_rh.stk_ab%.0d", ipo );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0ld", leer_mat_sa[ipo - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2, szTemp3, LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_rh.stk%.0d", ipo );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0ld", leer_mat_s[ipo - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2 , szTemp3, LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_rh.pr_vk%.0d", ipo );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%1.2lf", leer_mat_pr[ipo - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2 , szTemp3, LL_NUMERIC, NULL);
		}
	}
	if (LlPrintIsFieldUsed(hJob, "leer_rh.kun_leer_kz"))
	{
		sprintf ( szTemp3, "%1.0d", kun.kun_leer_kz ) ;
		LlDefineFieldExt(hJob, "leer_rh.kun_leer_kz" , szTemp3, LL_NUMERIC, NULL);
	}

	if (LlPrintIsFieldUsed(hJob, "leer_rh.leer_aktiv"))	// 041206
	{
		sprintf ( szTemp3, "%1.0d", party_mdn_aktiv ) ;
		LlDefineFieldExt(hJob, "leer_rh.leih_aktiv" , szTemp3, LL_NUMERIC, NULL);
	}

// 090709
// rabz1 #############
	if (LlPrintIsFieldUsed(hJob, "rabz1.typ"))
		LlDefineFieldExt(hJob, "rabz1.typ", reporzu1.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsFieldUsed(hJob, "rabz1.gruppe"))
		LlDefineFieldExt(hJob, "rabz1.gruppe", reporzu1.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsFieldUsed(hJob, "rabz1.wertart"))
		LlDefineFieldExt(hJob, "rabz1.wertart", reporzu1.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsFieldUsed(hJob, "rabz1.rab_bz"))
		LlDefineFieldExt(hJob, "rabz1.rab_bz", reporzu1.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsFieldUsed(hJob, "rabz1.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu1.me);
		LlDefineFieldExt(hJob, "rabz1.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsFieldUsed(hJob, "rabz1.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu1.faktor);
		LlDefineFieldExt(hJob, "rabz1.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsFieldUsed(hJob, "rabz1.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu1.geswert);
		LlDefineFieldExt(hJob, "rabz1.geswert", szTemp2, LL_NUMERIC, NULL);
	}


// rabz2 #############
	if (LlPrintIsFieldUsed(hJob, "rabz2.typ"))
		LlDefineFieldExt(hJob, "rabz2.typ", reporzu2.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsFieldUsed(hJob, "rabz2.gruppe"))
		LlDefineFieldExt(hJob, "rabz2.gruppe", reporzu2.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsFieldUsed(hJob, "rabz2.wertart"))
		LlDefineFieldExt(hJob, "rabz2.wertart", reporzu2.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsFieldUsed(hJob, "rabz2.rab_bz"))
		LlDefineFieldExt(hJob, "rabz2.rab_bz", reporzu2.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsFieldUsed(hJob, "rabz2.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu2.me);
		LlDefineFieldExt(hJob, "rabz2.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsFieldUsed(hJob, "rabz2.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu2.faktor);
		LlDefineFieldExt(hJob, "rabz2.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsFieldUsed(hJob, "rabz2.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu2.geswert);
		LlDefineFieldExt(hJob, "rabz2.geswert", szTemp2, LL_NUMERIC, NULL);
	}

// rabz3 #############
	if (LlPrintIsFieldUsed(hJob, "rabz3.typ"))
		LlDefineFieldExt(hJob, "rabz3.typ", reporzu3.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsFieldUsed(hJob, "rabz3.gruppe"))
		LlDefineFieldExt(hJob, "rabz3.gruppe", reporzu3.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsFieldUsed(hJob, "rabz3.wertart"))
		LlDefineFieldExt(hJob, "rabz3.wertart", reporzu3.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsFieldUsed(hJob, "rabz3.rab_bz"))
		LlDefineFieldExt(hJob, "rabz3.rab_bz", reporzu3.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsFieldUsed(hJob, "rabz3.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu3.me);
		LlDefineFieldExt(hJob, "rabz3.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsFieldUsed(hJob, "rabz3.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu3.faktor);
		LlDefineFieldExt(hJob, "rabz3.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsFieldUsed(hJob, "rabz3.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu3.geswert);
		LlDefineFieldExt(hJob, "rabz3.geswert", szTemp2, LL_NUMERIC, NULL);
	}

// 260612
// rabz4 #############
	if (LlPrintIsFieldUsed(hJob, "rabz4.typ"))
		LlDefineFieldExt(hJob, "rabz4.typ", reporzu4.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsFieldUsed(hJob, "rabz4.gruppe"))
		LlDefineFieldExt(hJob, "rabz4.gruppe", reporzu4.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsFieldUsed(hJob, "rabz4.wertart"))
		LlDefineFieldExt(hJob, "rabz4.wertart", reporzu4.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsFieldUsed(hJob, "rabz4.rab_bz"))
		LlDefineFieldExt(hJob, "rabz4.rab_bz", reporzu4.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsFieldUsed(hJob, "rabz4.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu4.me);
		LlDefineFieldExt(hJob, "rabz4.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsFieldUsed(hJob, "rabz4.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu4.faktor);
		LlDefineFieldExt(hJob, "rabz4.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsFieldUsed(hJob, "rabz4.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu4.geswert);
		LlDefineFieldExt(hJob, "rabz4.geswert", szTemp2, LL_NUMERIC, NULL);
	}


// rabz5 #############
	if (LlPrintIsFieldUsed(hJob, "rabz5.typ"))
		LlDefineFieldExt(hJob, "rabz5.typ", reporzu5.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsFieldUsed(hJob, "rabz5.gruppe"))
		LlDefineFieldExt(hJob, "rabz5.gruppe", reporzu5.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsFieldUsed(hJob, "rabz5.wertart"))
		LlDefineFieldExt(hJob, "rabz5.wertart", reporzu5.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsFieldUsed(hJob, "rabz5.rab_bz"))
		LlDefineFieldExt(hJob, "rabz5.rab_bz", reporzu5.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsFieldUsed(hJob, "rabz5.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu5.me);
		LlDefineFieldExt(hJob, "rabz5.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsFieldUsed(hJob, "rabz5.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu5.faktor);
		LlDefineFieldExt(hJob, "rabz5.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsFieldUsed(hJob, "rabz5.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu5.geswert);
		LlDefineFieldExt(hJob, "rabz5.geswert", szTemp2, LL_NUMERIC, NULL);
	}

// rabz6 #############
	if (LlPrintIsFieldUsed(hJob, "rabz6.typ"))
		LlDefineFieldExt(hJob, "rabz6.typ", reporzu6.typ , LL_TEXT, NULL);	// "T","M","F","K",Teilsortiment,Mengeneinheit,Frakozu,Kette
	if (LlPrintIsFieldUsed(hJob, "rabz6.gruppe"))
		LlDefineFieldExt(hJob, "rabz6.gruppe", reporzu6.gruppe , LL_TEXT, NULL);	// Sortimentsnummer oder Mengeneinheit
	if (LlPrintIsFieldUsed(hJob, "rabz6.wertart"))
		LlDefineFieldExt(hJob, "rabz6.wertart", reporzu6.wertart, LL_TEXT, NULL);	// "W"ert oder "P"rozent
	if (LlPrintIsFieldUsed(hJob, "rabz6.rab_bz"))
		LlDefineFieldExt(hJob, "rabz6.rab_bz", reporzu6.rab_bz, LL_TEXT, NULL);
	if (LlPrintIsFieldUsed(hJob, "rabz6.me"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu6.me);
		LlDefineFieldExt(hJob, "rabz6.me", szTemp2, LL_NUMERIC, NULL);	// Bewertungsbasis(Anzahl /Warenwert)
	}
	if (LlPrintIsFieldUsed(hJob, "rabz6.faktor"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu6.faktor);
		LlDefineFieldExt(hJob, "rabz6.faktor", szTemp2, LL_NUMERIC, NULL);	// Faktor ( Wert je einheit /Prozent )
	}
	if (LlPrintIsFieldUsed(hJob, "rabz6.geswert"))
	{
		sprintf(szTemp2, "%1.4lf", reporzu6.geswert);
		LlDefineFieldExt(hJob, "rabz6.geswert", szTemp2, LL_NUMERIC, NULL);
	}

// 071108 A
// einfach immer uebergeben, evtl. wird dann halt ein empty-feld gesetzt
	if (LlPrintIsFieldUsed(hJob, "adr1.anr"))	// 091208
	{
		LlDefineFieldExt(hJob, "adr1.anr",adr1.anrtxt, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr1.adr_nam1"))
	{
		LlDefineFieldExt(hJob, "adr1.adr_nam1",adr1.adr_nam1, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr1.adr_nam2"))
	{
		LlDefineFieldExt(hJob, "adr1.adr_nam2",adr1.adr_nam2, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr1.adr_nam3"))	
	{
		LlDefineFieldExt(hJob, "adr1.adr_nam3",adr1.adr_nam3, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr1.iln"))
	{
		LlDefineFieldExt(hJob, "adr1.iln",adr1.iln, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr1.ort1"))
	{
		LlDefineFieldExt(hJob, "adr1.ort1",adr1.ort1, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr1.plz"))
	{
		LlDefineFieldExt(hJob, "adr1.plz",adr1.plz, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr1.str"))
	{
		LlDefineFieldExt(hJob, "adr1.str",adr1.str, LL_TEXT, NULL);
	}

// 080709
	if (LlPrintIsFieldUsed(hJob, "adr1.plz_pf"))
		LlDefineFieldExt(hJob, "adr1.plz_pf",adr1.plz_pf, LL_TEXT, NULL);
	if (LlPrintIsFieldUsed(hJob, "adr1.pf"))
		LlDefineFieldExt(hJob, "adr1.pf",adr1.pf, LL_TEXT, NULL);

	if ( LlPrintIsFieldUsed(hJob, "adr1.ort2"))
		LlDefineFieldExt(hJob, "adr1.ort2",adr1.ort2 , LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr1.tel"))
		LlDefineFieldExt(hJob, "adr1.tel",adr1.tel, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr1.fax"))
		LlDefineFieldExt(hJob, "adr1.fax",adr1.fax, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr1.mobil"))	// 301111
		LlDefineFieldExt(hJob, "adr1.mobil",adr1.mobil, LL_TEXT, NULL);
	
	if ( LlPrintIsFieldUsed(hJob, "adr1.iban"))		// 300813
		LlDefineFieldExt(hJob, "adr1.iban",adr1.iban, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr1.swift"))		// 300813
		LlDefineFieldExt(hJob, "adr1.swift",adr1.swift, LL_TEXT, NULL);

	if ( LlPrintIsFieldUsed(hJob, "adr1.partner"))
		LlDefineFieldExt(hJob, "adr1.partner",adr1.partner, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr1.staattyp")
		|| LlPrintIsFieldUsed(hJob, "adr1.staatkz"))
	{
		sprintf ( ptabn.ptitem, "staat" ) ;
		sprintf ( ptabn.ptwert,"%d",adr1.staat );
		Ptabn.openptabn() ;
		if ( !Ptabn.leseptabn() )
		{
			if (LlPrintIsFieldUsed(hJob, "adr1.staattyp"))
			{

				LlDefineFieldExt(hJob, "adr1.staattyp",
					clippedi( ptabn.ptwer1), LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "adr1.staatkz"))
			{
				LlDefineFieldExt(hJob, "adr1.staatkz",
					clippedi( ptabn.ptwer2), LL_TEXT, NULL);
			}
		}
	}
// 011014 A
	if (LlPrintIsFieldUsed(hJob, "kun.adr1.anr"))
	{
		LlDefineFieldExt(hJob, "kun.adr1.anr",kunadr1.anrtxt, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.adr1.adr_nam1"))
	{
		LlDefineFieldExt(hJob, "kun.adr1.adr_nam1",kunadr1.adr_nam1, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.adr1.adr_nam2"))
	{
		LlDefineFieldExt(hJob, "kun.adr1.adr_nam2",kunadr1.adr_nam2, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.adr1.adr_nam3"))	
	{
		LlDefineFieldExt(hJob, "kun.adr1.adr_nam3",kunadr1.adr_nam3, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.adr1.iln"))
	{
		LlDefineFieldExt(hJob, "kun.adr1.iln",kunadr1.iln, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.adr1.ort1"))
	{
		LlDefineFieldExt(hJob, "kun.adr1.ort1",kunadr1.ort1, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.adr1.plz"))
	{
		LlDefineFieldExt(hJob, "kun.adr1.plz",kunadr1.plz, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.adr1.str"))
	{
		LlDefineFieldExt(hJob, "kun.adr1.str",kunadr1.str, LL_TEXT, NULL);
	}

	if (LlPrintIsFieldUsed(hJob, "kun.adr1.plz_pf"))
		LlDefineFieldExt(hJob, "kun.adr1.plz_pf",kunadr1.plz_pf, LL_TEXT, NULL);
	if (LlPrintIsFieldUsed(hJob, "kun.adr1.pf"))
		LlDefineFieldExt(hJob, "kun.adr1.pf",kunadr1.pf, LL_TEXT, NULL);

	if ( LlPrintIsFieldUsed(hJob, "kun.adr1.ort2"))
		LlDefineFieldExt(hJob, "kun.adr1.ort2",kunadr1.ort2 , LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "kun.adr1.tel"))
		LlDefineFieldExt(hJob, "kun.adr1.tel",kunadr1.tel, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "kun.adr1.fax"))
		LlDefineFieldExt(hJob, "kun.adr1.fax",kunadr1.fax, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "kun.adr1.mobil"))
		LlDefineFieldExt(hJob, "kun.adr1.mobil",kunadr1.mobil, LL_TEXT, NULL);
	
	if ( LlPrintIsFieldUsed(hJob, "kun.adr1.iban"))
		LlDefineFieldExt(hJob, "kun.adr1.iban",kunadr1.iban, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "kun.adr1.swift"))
		LlDefineFieldExt(hJob, "kun.adr1.swift",kunadr1.swift, LL_TEXT, NULL);

	if ( LlPrintIsFieldUsed(hJob, "kun.adr1.partner"))
		LlDefineFieldExt(hJob, "kun.adr1.partner",kunadr1.partner, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "kun.adr1.staattyp")
		|| LlPrintIsFieldUsed(hJob, "kun.adr1.staatkz"))
	{
		sprintf ( ptabn.ptitem, "staat" ) ;
		sprintf ( ptabn.ptwert,"%d",kunadr1.staat );
		Ptabn.openptabn() ;
		if ( !Ptabn.leseptabn() )
		{
			if (LlPrintIsFieldUsed(hJob, "kun.adr1.staattyp"))
			{

				LlDefineFieldExt(hJob, "kun.adr1.staattyp",
					clippedi( ptabn.ptwer1), LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "kun.adr1.staatkz"))
			{
				LlDefineFieldExt(hJob, "kun.adr1.staatkz",
					clippedi( ptabn.ptwer2), LL_TEXT, NULL);
			}
		}
	}

// 011014 E

	if (LlPrintIsFieldUsed(hJob, "adr2.anr"))	// 091208
	{
		LlDefineFieldExt(hJob, "adr2.anr",adr2.anrtxt, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr2.adr_nam1"))
	{
		LlDefineFieldExt(hJob, "adr2.adr_nam1",adr2.adr_nam1, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr2.adr_nam2"))
	{
		LlDefineFieldExt(hJob, "adr2.adr_nam2",adr2.adr_nam2, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr2.adr_nam3"))			
	{
		LlDefineFieldExt(hJob, "adr2.adr_nam3",adr2.adr_nam3, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr2.iln"))
	{
		LlDefineFieldExt(hJob, "adr2.iln",adr2.iln, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr2.ort1"))
	{
		LlDefineFieldExt(hJob, "adr2.ort1",adr2.ort1, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr2.plz"))
	{
		LlDefineFieldExt(hJob, "adr2.plz",adr2.plz, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr2.str"))
	{
		LlDefineFieldExt(hJob, "adr2.str",adr2.str, LL_TEXT, NULL);
	}

// 080709
	if (LlPrintIsFieldUsed(hJob, "adr2.plz_pf"))
		LlDefineFieldExt(hJob, "adr2.plz_pf",adr2.plz_pf, LL_TEXT, NULL);
	if (LlPrintIsFieldUsed(hJob, "adr2.pf"))
		LlDefineFieldExt(hJob, "adr2.pf",adr2.pf, LL_TEXT, NULL);

	if ( LlPrintIsFieldUsed(hJob, "adr2.ort2"))
		LlDefineFieldExt(hJob, "adr2.ort2",adr2.ort2 , LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr2.tel"))
		LlDefineFieldExt(hJob, "adr2.tel",adr2.tel, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr2.fax"))
		LlDefineFieldExt(hJob, "adr2.fax",adr2.fax, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr2.mobil"))	// 301111
		LlDefineFieldExt(hJob, "adr2.mobil",adr2.mobil, LL_TEXT, NULL);

	if ( LlPrintIsFieldUsed(hJob, "adr2.iban"))		// 300813
		LlDefineFieldExt(hJob, "adr2.iban",adr2.iban, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr2.swift"))		// 300813
		LlDefineFieldExt(hJob, "adr2.swift",adr2.swift, LL_TEXT, NULL);

	if ( LlPrintIsFieldUsed(hJob, "adr2.partner"))
		LlDefineFieldExt(hJob, "adr2.partner",adr2.partner, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr2.staattyp")
		|| LlPrintIsFieldUsed(hJob, "adr2.staatkz"))
	{
		sprintf ( ptabn.ptitem, "staat" ) ;
		sprintf ( ptabn.ptwert,"%d",adr2.staat );
		Ptabn.openptabn() ;
		if ( !Ptabn.leseptabn() )
		{
			if (LlPrintIsFieldUsed(hJob, "adr2.staattyp"))
			{
				LlDefineFieldExt(hJob, "adr2.staattyp",
					clippedi( ptabn.ptwer1), LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "adr2.staatkz"))
			{
				LlDefineFieldExt(hJob, "adr2.staatkz",
					clippedi( ptabn.ptwer2), LL_TEXT, NULL);
			}
		}
	}

	if (LlPrintIsFieldUsed(hJob, "adr3.anr"))	// 091208
	{
		LlDefineFieldExt(hJob, "adr3.anr",adr3.anrtxt, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr3.adr_nam1"))
	{
		LlDefineFieldExt(hJob, "adr3.adr_nam1",adr3.adr_nam1, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr3.adr_nam2"))
	{
		LlDefineFieldExt(hJob, "adr3.adr_nam2",adr3.adr_nam2, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr3.adr_nam3"))
	{
		LlDefineFieldExt(hJob, "adr3.adr_nam3",adr3.adr_nam3, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr3.iln"))
	{
		LlDefineFieldExt(hJob, "adr3.iln",adr3.iln, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr3.ort1"))
	{
		LlDefineFieldExt(hJob, "adr3.ort1",adr3.ort1, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr3.plz"))
	{
		LlDefineFieldExt(hJob, "adr3.plz",adr3.plz, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "adr3.str"))
	{
		LlDefineFieldExt(hJob, "adr3.str",adr3.str, LL_TEXT, NULL);
	}

// 080709
		if (LlPrintIsFieldUsed(hJob, "adr3.plz_pf"))
			LlDefineFieldExt(hJob, "adr3.plz_pf",adr3.plz_pf, LL_TEXT, NULL);
		if (LlPrintIsFieldUsed(hJob, "adr3.pf"))
			LlDefineFieldExt(hJob, "adr3.pf",adr3.pf, LL_TEXT, NULL);


	if ( LlPrintIsFieldUsed(hJob, "adr3.ort2"))
		LlDefineFieldExt(hJob, "adr3.ort2",adr3.ort2 , LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr3.tel"))
		LlDefineFieldExt(hJob, "adr3.tel",adr3.tel, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr3.fax"))
		LlDefineFieldExt(hJob, "adr3.fax",adr3.fax, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr3.mobil"))	// 301111
		LlDefineFieldExt(hJob, "adr3.mobil",adr3.mobil, LL_TEXT, NULL);
	
	if ( LlPrintIsFieldUsed(hJob, "adr3.iban"))		// 300813
		LlDefineFieldExt(hJob, "adr3.iban",adr3.iban, LL_TEXT, NULL);
	if ( LlPrintIsFieldUsed(hJob, "adr3.swift"))		// 300813
		LlDefineFieldExt(hJob, "adr3.swift",adr3.swift, LL_TEXT, NULL);


	if ( LlPrintIsFieldUsed(hJob, "adr3.partner"))
		LlDefineFieldExt(hJob, "adr3.partner",adr3.partner, LL_TEXT, NULL);
	if (LlPrintIsFieldUsed(hJob, "adr3.staattyp")
		|| LlPrintIsFieldUsed(hJob, "adr3.staatkz"))
	{
		sprintf ( ptabn.ptitem, "staat" ) ;
		sprintf ( ptabn.ptwert,"%d",adr3.staat );
		Ptabn.openptabn() ;
		if ( !Ptabn.leseptabn() )
		{
			if (LlPrintIsFieldUsed(hJob, "adr3.staattyp"))
			{
				LlDefineFieldExt(hJob, "adr3.staattyp",
					clippedi( ptabn.ptwer1), LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "adr3.staatkz"))
			{
				LlDefineFieldExt(hJob, "adr3.staatkz",
					clippedi( ptabn.ptwer2), LL_TEXT, NULL);
			}
		}
	}


// 071108 E

// 120413 A
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.kasse" ))
	{
		sprintf(szTemp2, "%d", kase_fit.kasse );
		LlDefineFieldExt(hJob, "kase_fit.kasse",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.bon" ))
	{
		sprintf(szTemp2, "%d", kase_fit.bon );
		LlDefineFieldExt(hJob, "kase_fit.bon",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.betrag_bto" ))
	{
		sprintf(szTemp2, "%1.3lf", kase_fit.betrag_bto );
		LlDefineFieldExt(hJob, "kase_fit.betrag_bto",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.gegeben" ))
	{
		sprintf(szTemp2, "%1.3lf", kase_fit.gegeben );
		LlDefineFieldExt(hJob, "kase_fit.gegeben",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.zurueck" ))
	{
		sprintf(szTemp2, "%1.3lf", kase_fit.zurueck );
		LlDefineFieldExt(hJob, "kase_fit.zurueck",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.gut_sum" ))
	{
		sprintf(szTemp2, "%1.3lf", kase_fit.gut_sum );
		LlDefineFieldExt(hJob, "kase_fit.gut_sum",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.essengut_sum" ))
	{
		sprintf(szTemp2, "%1.3lf", kase_fit.essengut_sum );
		LlDefineFieldExt(hJob, "kase_fit.essengut_sum",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.gut_anzahl" ))
	{
		sprintf(szTemp2, "%d", kase_fit.gut_anzahl );

		LlDefineFieldExt(hJob, "kase_fit.gut_anzahl",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.essengut_anzahl" ))
	{
		sprintf(szTemp2, "%d", kase_fit.essengut_anzahl );
		LlDefineFieldExt(hJob, "kase_fit.essengut_anzahl",szTemp2, LL_NUMERIC, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.zahlart" ))
	{
		sprintf(szTemp2, "%d", kase_fit.zahlart );
		LlDefineFieldExt(hJob, "kase_fit.zahlart","1", LL_NUMERIC, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.bon_date" ))
	{
		LlDefineFieldExt(hJob, "kase_fit.bon_date", sqldatamger (kase_fit.bon_datec,szTemp2), LL_TEXT, NULL);
	}
	if ( LlPrintIsFieldUsed(hJob, "kase_fit.pers_nam" ))
	{
		LlDefineFieldExt(hJob, "kase_fit.pers_nam",kase_fit.pers_nam, LL_TEXT, NULL);
	}
// 120413 E

// 070208 A
	double fhilfe = 0  ;	// 201014 : explizit initialisieren

	if (LlPrintIsFieldUsed(hJob, "rtab.a_bas.a_gew"))
	{
		if ( reporech.a == 0.0 )
		{	// falls reporech.a == reporech.a_kun,
			// dann wird reporech.a vom uebergeordneten Programm = 0 gestetzt
			fhilfe = atof ( reporech.a_kun ) ;
		}
		else
		{
			fhilfe = reporech.a ;
		}
		if ( fhilfe == a_bas.a )
		{ /* dummy-Aktion */ }
		else
		{

			fhilfe = texteladen ( fhilfe ) ;	// 201014 
		}
		sprintf(szTemp2, "%1.3lf", a_bas.a_gew );// Nachkommastellen korrigiert von 0 auf 3 030709

		LlDefineFieldExt(hJob, "rtab.a_bas.a_gew", szTemp2, LL_NUMERIC, NULL);
	
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.a_bas.me_einh"))
	{
		if ( reporech.a == 0.0 )
		{	// falls reporech.a == reporech.a_kun,
			// dann wird reporech.a vom uebergeordneten Programm = 0 gestetzt
			fhilfe = atof ( reporech.a_kun ) ;
		}
		else
		{
			fhilfe = reporech.a ;
		}

		if ( fhilfe == a_bas.a )
		{ /* dummy-Aktion */ }
		else
		{
			fhilfe = texteladen ( fhilfe ) ;	// 201014 
		}
		sprintf(szTemp2, "%d", a_bas.me_einh );
		LlDefineFieldExt(hJob, "rtab.a_bas.me_einh", szTemp2, LL_NUMERIC, NULL);
	}
// 070208 E
// 190310 A
// AG
	if (LlPrintIsFieldUsed(hJob, "rtab.a_bas.ag"))
	{
		if ( reporech.a == 0.0 )
			fhilfe = atof ( reporech.a_kun ) ;
		else
			fhilfe = reporech.a ;

		if ( fhilfe == a_bas.a )
		{	/* dummy-Aktion */ } 
		else
		{
			fhilfe = texteladen ( fhilfe ) ;	// 201014 
		}
		sprintf(szTemp2, "%d", a_bas.ag );
		LlDefineFieldExt(hJob, "rtab.a_bas.ag", szTemp2, LL_NUMERIC, NULL);
	}
// WG
	if (LlPrintIsFieldUsed(hJob, "rtab.a_bas.wg"))
	{
		if ( reporech.a == 0.0 )
			fhilfe = atof ( reporech.a_kun ) ;
		else
			fhilfe = reporech.a ;

		if ( fhilfe == a_bas.a )
		{	/* dummy-Aktion */ } 
		else
		{
			fhilfe = texteladen ( fhilfe ) ;	// 201014 
		}
		sprintf(szTemp2, "%d", a_bas.wg );
		LlDefineFieldExt(hJob, "rtab.a_bas.wg", szTemp2, LL_NUMERIC, NULL);
	}
// HWG
	if (LlPrintIsFieldUsed(hJob, "rtab.a_bas.hwg"))
	{
		if ( reporech.a == 0.0 )
			fhilfe = atof ( reporech.a_kun ) ;
		else
			fhilfe = reporech.a ;

		if ( fhilfe == a_bas.a )
		{	/* dummy-Aktion */ } 
		else
		{
			fhilfe = texteladen ( fhilfe ) ;	// 201014 
		}
		sprintf(szTemp2, "%d", a_bas.hwg );
		LlDefineFieldExt(hJob, "rtab.a_bas.hwg", szTemp2, LL_NUMERIC, NULL);
	}
// TEIL_SMT
	if (LlPrintIsFieldUsed(hJob, "rtab.a_bas.teil_smt"))
	{
		if ( reporech.a == 0.0 )
			fhilfe = atof ( reporech.a_kun ) ;
		else
			fhilfe = reporech.a ;

		if ( fhilfe == a_bas.a )
		{	/* dummy-Aktion */ } 
		else
		{
			fhilfe = texteladen ( fhilfe ) ;	// 201014 
		}
		sprintf(szTemp2, "%d", a_bas.teil_smt );
		LlDefineFieldExt(hJob, "rtab.a_bas.teil_smt", szTemp2, LL_NUMERIC, NULL);
	}
// 190310 E
// 200214 A
	if ( reporech.a == 0.0 )
		fhilfe = atof ( reporech.a_kun ) ;
	else
		fhilfe = reporech.a ;

	if ( cfga_kun_txt || cfga_bas_erw || mitmysql )		// 200214 -190814
	{
		if ( fhilfe == a_bas.a )
		{	/* dummy-Aktion */ 
		} 
		else
		{
			fhilfe = texteladen ( fhilfe ) ;	// 201014 
		}
	}

// 200214 A
	if ( cfga_kun_txt )
	{

		if (LlPrintIsFieldUsed(hJob, "a_kun_txt.txt1"))
		{
			LlDefineFieldExt(hJob, "a_kun_txt.txt1",a_kun_txt.txt1, LL_TEXT, NULL);
		}
		if (LlPrintIsFieldUsed(hJob, "a_kun_txt.txt2"))
		{
			LlDefineFieldExt(hJob, "a_kun_txt.txt2",a_kun_txt.txt2, LL_TEXT, NULL);
		}
		if (LlPrintIsFieldUsed(hJob, "a_kun_txt.txt3"))
		{
			LlDefineFieldExt(hJob, "a_kun_txt.txt3",a_kun_txt.txt3, LL_TEXT, NULL);
		}
		if (LlPrintIsFieldUsed(hJob, "a_kun_txt.txt4"))
		{
			LlDefineFieldExt(hJob, "a_kun_txt.txt4",a_kun_txt.txt4, LL_TEXT, NULL);
		}
		if (LlPrintIsFieldUsed(hJob, "a_kun_txt.txt5"))
		{
			LlDefineFieldExt(hJob, "a_kun_txt.txt5",a_kun_txt.txt5, LL_TEXT, NULL);	// 290914 hier stand bisher txt3 ....
		}
		if (LlPrintIsFieldUsed(hJob, "a_kun_txt.sort"))
		{
			sprintf(szTemp2, "%d", a_kun_txt.sort );
			LlDefineFieldExt(hJob, "a_kun_txt.sort",szTemp2, LL_NUMERIC, NULL);
		}
	}

// 190814
	if ( mitmysql )
	{
		sprintf(szTemp2, "%d", asprache.sprache );
		LlDefineFieldExt(hJob, "asprache.sprache",szTemp2, LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "asprache.a_bz1",asprache.a_bz1,     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "asprache.a_bz2",asprache.a_bz2,     LL_TEXT, NULL);
// 251114
		sprintf(szTemp2, "%d", asprache2.sprache );
		LlDefineFieldExt(hJob, "asprache2.sprache",szTemp2, LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "asprache2.a_bz1",asprache2.a_bz1,     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "asprache2.a_bz2",asprache2.a_bz2,     LL_TEXT, NULL);

	}

	if ( cfga_bas_erw )
	{
		LlDefineFieldExt(hJob, "a_bas_erw.pp_a_bz1", a_bas_erw.pp_a_bz1,     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.pp_a_bz2", a_bas_erw.pp_a_bz2,     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.lgr_tmpr",a_bas_erw.lgr_tmpr,     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.lupine",a_bas_erw.lupine,       LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.schutzgas",a_bas_erw.schutzgas,    LL_TEXT, NULL);
 
		sprintf(szTemp2, "%d", a_bas_erw.huelle );
		LlDefineFieldExt(hJob, "a_bas_erw.huelle",szTemp2,       LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%d", a_bas_erw.shop_wg1 );
		LlDefineFieldExt(hJob, "a_bas_erw.shop_wg1",szTemp2,     LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%d", a_bas_erw.shop_wg2 );
		LlDefineFieldExt(hJob, "a_bas_erw.shop_wg2",szTemp2,     LL_NUMERIC, NULL);	//  smallint,
		sprintf(szTemp2, "%d", a_bas_erw.shop_wg3 );
		LlDefineFieldExt(hJob, "a_bas_erw.shop_wg3",szTemp2,     LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%d", a_bas_erw.shop_wg4 );
		LlDefineFieldExt(hJob, "a_bas_erw.shop_wg4",szTemp2,     LL_NUMERIC, NULL);	// smallint,

		sprintf(szTemp2, "%d", a_bas_erw.shop_wg5 );
		LlDefineFieldExt(hJob, "a_bas_erw.shop_wg5",szTemp2,     LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%1.4f", a_bas_erw.tara2 );
		LlDefineFieldExt(hJob, "a_bas_erw.tara2",szTemp2,        LL_NUMERIC, NULL);	// decimal(8,3),
		sprintf(szTemp2, "%d", a_bas_erw.a_tara2 );
		LlDefineFieldExt(hJob, "a_bas_erw.a_tara2",szTemp2,      LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%1.4f", a_bas_erw.tara3 );
		LlDefineFieldExt(hJob, "a_bas_erw.tara3",szTemp2,        LL_NUMERIC, NULL);	// decimal(8,3),
		sprintf(szTemp2, "%d", a_bas_erw.a_tara3 );
		LlDefineFieldExt(hJob, "a_bas_erw.a_tara3",szTemp2,      LL_NUMERIC, NULL);	// smallint,

		sprintf(szTemp2, "%1.4f", a_bas_erw.tara4 );
		LlDefineFieldExt(hJob, "a_bas_erw.tara4",szTemp2,        LL_NUMERIC, NULL);	// decimal(8,3),
		sprintf(szTemp2, "%d", a_bas_erw.a_tara4 );
		LlDefineFieldExt(hJob, "a_bas_erw.a_tara4",szTemp2,      LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%1.4f", a_bas_erw.salz );
		LlDefineFieldExt(hJob, "a_bas_erw.salz",szTemp2,         LL_NUMERIC, NULL);	// decimal(8,3),
		sprintf(szTemp2, "%1.4f", a_bas_erw.davonfett );
		LlDefineFieldExt(hJob, "a_bas_erw.davonfett",szTemp2,    LL_NUMERIC, NULL);	// decimal(8,3),
		sprintf(szTemp2, "%1.4f", a_bas_erw.davonzucker );
		LlDefineFieldExt(hJob, "a_bas_erw.davonzucker",szTemp2,  LL_NUMERIC, NULL);	// decimal(8,3),

		sprintf(szTemp2, "%1.4f", a_bas_erw.ballaststoffe );
		LlDefineFieldExt(hJob, "a_bas_erw.ballaststoffe",szTemp2,LL_NUMERIC, NULL);	// decimal(8,3),
		LlDefineFieldExt(hJob, "a_bas_erw.shop_aktion",a_bas_erw.shop_aktion,  LL_TEXT, NULL);	// char(1),
		LlDefineFieldExt(hJob, "a_bas_erw.shop_neu",a_bas_erw.shop_neu,     LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.shop_tv",a_bas_erw.shop_tv,      LL_TEXT, NULL);
		sprintf(szTemp2, "%1.4f", a_bas_erw.shop_agew );
		LlDefineFieldExt(hJob, "a_bas_erw.shop_agew",szTemp2,    LL_NUMERIC, NULL);	// decimal(8,3),

		LlDefineFieldExt(hJob, "a_bas_erw.a_bez",a_bas_erw.a_bez,        LL_TEXT, NULL);
		LlDefineFieldExt(hJob, "a_bas_erw.zutat",a_bas_erw.zutat,        LL_TEXT, NULL);
		sprintf(szTemp2, "%d", a_bas_erw.userdef1 );
		LlDefineFieldExt(hJob, "a_bas_erw.userdef1",szTemp2,     LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%d", a_bas_erw.userdef2 );
		LlDefineFieldExt(hJob, "a_bas_erw.userdef2",szTemp2,     LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%d", a_bas_erw.userdef3 );
		LlDefineFieldExt(hJob, "a_bas_erw.userdef3",szTemp2,     LL_NUMERIC, NULL);	// smallint,

		sprintf(szTemp2, "%d", a_bas_erw.minstaffgr );
		LlDefineFieldExt(hJob, "a_bas_erw.minstaffgr",szTemp2,   LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%d", a_bas_erw.l_pack );
		LlDefineFieldExt(hJob, "a_bas_erw.l_pack",szTemp2,       LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%d", a_bas_erw.b_pack );
		LlDefineFieldExt(hJob, "a_bas_erw.b_pack",szTemp2,       LL_NUMERIC, NULL);	// smallint,
		sprintf(szTemp2, "%d", a_bas_erw.h_pack );
		LlDefineFieldExt(hJob, "a_bas_erw.h_pack",szTemp2,       LL_NUMERIC, NULL);	// smallint

	}

// 200214 E

	if (LlPrintIsFieldUsed(hJob, "rtab.a"))
	{
		sprintf(szTemp2, "%.0lf", reporech.a );
		LlDefineFieldExt(hJob, "rtab.a", szTemp2, LL_NUMERIC, NULL);
 	}
	if (LlPrintIsFieldUsed(hJob, "rtab.Nutzer"))	// 060905
	{
		LlDefineFieldExt(hJob, "rtab.Nutzer",LNutzer, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.nachkpreis"))	// 160905
	{
		sprintf(szTemp2, "%d", dnachkpreis );
		LlDefineFieldExt(hJob, "rtab.nachkpreis",szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.a_bz1"))
	{
		LlDefineFieldExt(hJob, "rtab.a_bz1",reporech.a_bz1, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.abz2"))
	{
		LlDefineFieldExt(hJob, "rtab.a_bz2",reporech.a_bz2, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.lpos_txt"))
	{
		sprintf(szTemp2, "%.0ld", reporech.lpos_txt );
		LlDefineFieldExt(hJob, "rtab.lpos_txt", szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen (hJob, "lpos", "F");

// 151014 A
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_ls1_t"))
	{
		textsetzen (hJob, "xaufpt_verkfrage", "F");
//		sprintf(szTemp2, "%.0ld", reporech.txt_rech );
//		LlDefineFieldExt(hJob, "txt_rech", szTemp2, LL_NUMERIC, NULL);
	}
//	textsetzen (hJob, "atexte", "F");

	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_ls2_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_ls3_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_ls4_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_ls5_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_ls6_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_ls7_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_ls8_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_ls9_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_ls10_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");

	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_rech1_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_rech2_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_rech3_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_rech4_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_rech5_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_rech6_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_rech7_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_rech8_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_rech9_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.txt_rech10_t"))
		textsetzen (hJob, "xaufpt_verkfrage", "F");

// 151014 E

// 031114 

	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.def1"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def1 );
		LlDefineFieldExt(hJob, "aufpt_verkfrage.def1", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.def2"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def2 );
		LlDefineFieldExt(hJob, "aufpt_verkfrage.def2", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.def3"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def3 );
		LlDefineFieldExt(hJob, "aufpt_verkfrage.def3", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.def4"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def4 );
		LlDefineFieldExt(hJob, "aufpt_verkfrage.def4", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.def5"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def5 );
		LlDefineFieldExt(hJob, "aufpt_verkfrage.def5", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.def6"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def6 );
		LlDefineFieldExt(hJob, "aufpt_verkfrage.def6", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.def7"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def7 );
		LlDefineFieldExt(hJob, "aufpt_verkfrage.def7", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.def8"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def8 );
		LlDefineFieldExt(hJob, "aufpt_verkfrage.def8", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.def9"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def9 );
		LlDefineFieldExt(hJob, "aufpt_verkfrage.def9", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "aufpt_verkfrage.def10"))
	{		// a_bas.a ist hier immer bereits aktuell ;-)
		if (reporech.lsret[0] == 'L')	Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn,reporech.lsnr,a_bas.a);
		else							Aufpt_verkfrage.leseaufpt_verkfrage(reporech.mdn, -777 ,a_bas.a);

		sprintf(szTemp2, "%1d", aufpt_verkfrage.def10 );
		LlDefineFieldExt(hJob, "aufpt_verkfrage.def10", szTemp2, LL_NUMERIC, NULL);
	}
	
// 101014
	if (LlPrintIsFieldUsed(hJob, "rtab.txt_rech"))
	{
		sprintf(szTemp2, "%.0ld", reporech.txt_rech );
		LlDefineFieldExt(hJob, "txt_rech", szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen (hJob, "atexte", "F");





	if (LlPrintIsFieldUsed(hJob, "rtab.kopf_txt"))
	{
		sprintf(szTemp2, "%.0lf", reporech.kopf_txt );
		LlDefineFieldExt(hJob, "rtab.kopf_txt", szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen (hJob,"kopf", "F");

	if (LlPrintIsFieldUsed(hJob, "rtab.fuss_txt"))
	{
		sprintf(szTemp2, "%.0ld", reporech.fuss_txt );
		LlDefineFieldExt(hJob, "rtab.fuss_txt", szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen (hJob, "fuss", "F");

	if (LlPrintIsFieldUsed(hJob, "rtab.a_kun"))
	{
		LlDefineFieldExt(hJob, "rtab.a_kun",reporech.a_kun, LL_TEXT, NULL);
	}

	if (LlPrintIsFieldUsed(hJob, "rtab.ean"))	// 110913
	{
		a_kun_gx_komplett();
		LlDefineFieldExt(hJob, "rtab.ean",eanstring , LL_NUMERIC, NULL);
	}

	if (LlPrintIsFieldUsed(hJob, "rtab.zu_stoff"))
	{
		LlDefineFieldExt(hJob, "rtab.zu_stoff",reporech.zu_stoff, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.gn_pkt_gbr"))
	{

		sprintf(szTemp2, "%1.2lf", reporech.gn_pkt_gbr );
		LlDefineFieldExt(hJob, "rtab.gn_pkt_gbr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.adr2"))
	{

		sprintf(szTemp2, "%1.0lf", reporech.adr2 );
		LlDefineFieldExt(hJob, "rtab.adr2", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.adr3"))
	{
		sprintf(szTemp2, "%1.0lf", reporech.adr3 );
		LlDefineFieldExt(hJob, "rtab.adr3", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.snetto1"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.snetto1 );
		LlDefineFieldExt(hJob, "rtab.snetto1", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.snetto2"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.snetto2 );
		LlDefineFieldExt(hJob, "rtab.snetto2", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.snetto3"))
	{

		sprintf(szTemp2, "%1.2lf", reporech.snetto3 );
		LlDefineFieldExt(hJob, "rtab.snetto3", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.smwst1"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.smwst1 );
		LlDefineFieldExt(hJob, "rtab.smwst1", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.smwst2"))
	{

		sprintf(szTemp2, "%1.2lf", reporech.smwst2 );
		LlDefineFieldExt(hJob, "rtab.smwst2", szTemp2, LL_NUMERIC, NULL);;
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.smwst3"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.smwst3 );
		LlDefineFieldExt(hJob, "rtab.smwst3", szTemp2, LL_NUMERIC, NULL);;
  	}
	if (LlPrintIsFieldUsed(hJob, "rtab.smwst1p"))
	{
		LlDefineFieldExt(hJob, "rtab.smwst1p",reporech.smwst1p, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.smwst2p"))
	{
		LlDefineFieldExt(hJob, "rtab.smwst2p",reporech.smwst2p, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.smwst3p"))
	{
		LlDefineFieldExt(hJob, "rtab.smwst3p",reporech.smwst3p, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.smwst1s"))
	{
		sprintf(szTemp2, "%1d", reporech.smwst1s );
		LlDefineFieldExt(hJob, "rtab.smwst1s", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.smwst2s"))
	{
		sprintf(szTemp2, "%1d", reporech.smwst2s );
		LlDefineFieldExt(hJob, "rtab.smwst2s", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.smwst3s"))
	{
		sprintf(szTemp2, "%1d", reporech.smwst3s );
		LlDefineFieldExt(hJob, "rtab.smwst3s", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.rech_summ"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.rech_summ );
		LlDefineFieldExt(hJob, "rtab.rech_summ", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.end_rab"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.end_rab );
		LlDefineFieldExt(hJob, "rtab.end_rab", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.zahl_betr"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.zahl_betr );
		LlDefineFieldExt(hJob, "rtab.zahl_betr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.fil"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.fil );
		LlDefineFieldExt(hJob, "rtab.fil", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.belk_txt"))
	{
		LlDefineFieldExt(hJob, "rtab.belk_txt",reporech.belk_txt, LL_TEXT, NULL);
 	}
	if (LlPrintIsFieldUsed(hJob, "rtab.belf_txt"))
	{
		LlDefineFieldExt(hJob, "rtab.belf_txt",reporech.belf_txt, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.zako_txt"))
	{
		LlDefineFieldExt(hJob, "rtab.zako_txt",reporech.zako_txt, LL_TEXT, NULL);
		zahlweisen( hJob, reporech.zako_txt, "F" ) ;
	}

	if (LlPrintIsFieldUsed(hJob, "rtab.pmwsts"))
	{
		LlDefineFieldExt(hJob, "rtab.pmwsts",reporech.pmwsts, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.kun"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.kun );
		LlDefineFieldExt(hJob, "rtab.kun", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.ges_rabp"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.ges_rabp );
		LlDefineFieldExt(hJob, "rtab.ges_rabp", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.mdn"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.mdn );
		LlDefineFieldExt(hJob, "rtab.mdn", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.ust_id"))
	{
		LlDefineFieldExt(hJob, "rtab.ust_id",reporech.ust_id, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.nr_bei_rech"))
	{
		LlDefineFieldExt(hJob, "rtab.nr_bei_rech",reporech.nr_bei_rech, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.alt_pr"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.alt_pr );
		LlDefineFieldExt(hJob, "rtab.alt_pr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.lsret"))
	{
		LlDefineFieldExt(hJob, "rtab.lsret", reporech.lsret, LL_TEXT, NULL);
	}

// 290609
	if (LlPrintIsFieldUsed(hJob, "rtab.lsnr"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.lsnr );
		LlDefineFieldExt(hJob, "rtab.lsnr", szTemp2, LL_NUMERIC, NULL);
	}

	if (LlPrintIsFieldUsed(hJob, "rtab.auf_ext"))
	{
		LlDefineFieldExt(hJob, "rtab.auf_ext",reporech.auf_ext, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.a_typ"))
	{
		sprintf(szTemp2, "%d", reporech.a_typ );	// 230813 : hier stand bisher : 1.0lf
		LlDefineFieldExt(hJob, "rtab.a_typ", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.kun_nam"))
	{

		LlDefineFieldExt(hJob, "rtab.kun_nam",reporech.kun_nam, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.bbn"))
	{
		LlDefineFieldExt(hJob, "rtab.bbn",reporech.bbn, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.mdnadr"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.mdnadr );
		LlDefineFieldExt(hJob, "rtab.mdnadr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.rech_dat"))
	{

		LlDefineFieldExt(hJob, "rtab.rech_dat",sqldatamger(reporech.rech_dat,szTemp2), LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.order3"))
	{
		sprintf(szTemp2, "%1d", reporech.order3 );
		LlDefineFieldExt(hJob, "rtab.order3", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.rech_nr"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.rech_nr );
		LlDefineFieldExt(hJob, "rtab.rech_nr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.erf_kz"))
	{
		LlDefineFieldExt(hJob, "rtab.erf_kz",reporech.erf_kz, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.lief_me"))
	{
		sprintf(szTemp2, "%1.3lf", reporech.lief_me );
		LlDefineFieldExt(hJob, "rtab.lief_me", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.lief_me_bz"))
	{
		LlDefineFieldExt(hJob, "rtab.lief_me_bz",reporech.lief_me_bz, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.vk_pr"))
	{
		if ( dnachkpreis == 4 )	// 160905 
		{
			sprintf(szTemp2, "%1.4lf", reporech.vk_pr );
		}
		else
		{
			if ( dnachkpreis == 3 )
			{
				sprintf(szTemp2, "%1.3lf", reporech.vk_pr );
			}
			else
			{
				sprintf(szTemp2, "%1.2lf", reporech.vk_pr );
			}
		}
		LlDefineFieldExt(hJob, "rtab.vk_pr", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.auf_me"))
	{
		sprintf(szTemp2, "%1.3lf", reporech.auf_me );
		LlDefineFieldExt(hJob, "rtab.auf_me", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.sa_kz"))
	{
		LlDefineFieldExt(hJob, "rtab.sa_kz",reporech.sa_kz, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.order2"))
	{
		sprintf(szTemp2, "%1d", reporech.order2 );
		LlDefineFieldExt(hJob, "rtab.order2", szTemp2, LL_NUMERIC, NULL);
	
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.einz_rabp"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.einz_rabp );
		LlDefineFieldExt(hJob, "rtab.einz_rabp", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.zeil_sum"))
	{
                sprintf(szTemp2, "%1.2lf", reporech.zeil_sum );
		LlDefineFieldExt(hJob, "rtab.zeil_sum", szTemp2, LL_NUMERIC, NULL);

	}
	if (LlPrintIsFieldUsed(hJob, "rtab.prab_wert"))
	{
		sprintf(szTemp2, "%1.2lf", reporech.prab_wert );
		LlDefineFieldExt(hJob, "rtab.prab_wert", szTemp2, LL_NUMERIC, NULL);
	}

// 300608 A

	if (LlPrintIsFieldUsed(hJob, "kun.ls_fuss_txt"))
	{
		sprintf(szTemp2, "%1.0ld", kun.ls_fuss_txt );
		LlDefineFieldExt(hJob, "kun.ls_fuss_txt",szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.rech_fuss_txt"))
	{
		sprintf(szTemp2, "%1.0ld", kun.rech_fuss_txt );
		LlDefineFieldExt(hJob, "kun.rech_fuss_txt",szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen ( hJob,"LFX", "F" );	
	textsetzen ( hJob,"RFX", "F" );	

// 300608 E
	// 251113
	if (LlPrintIsFieldUsed(hJob, "kun.zahl_ziel"))
	{
		sprintf(szTemp2, "%1d", kun.zahl_ziel );
		LlDefineFieldExt(hJob, "kun.zahl_ziel", szTemp2, LL_NUMERIC, NULL);
	}
// 231213 
	if ( !dietzosep )
	{
		if (LlPrintIsFieldUsed(hJob, "kun.tagesepa"))
		{
			sprintf(szTemp2, "%1d", kun.tagesepa );
			LlDefineFieldExt(hJob, "kun.tagesepa", szTemp2, LL_NUMERIC, NULL);
		}
		if (LlPrintIsFieldUsed(hJob, "kun.mandatref"))
		{
			sprintf(szTemp2, "%s", kun.mandatref );
			LlDefineFieldExt(hJob, "kun.mandatref", szTemp2, LL_TEXT, NULL);
		}
	}

	//	030912 
	if (LlPrintIsFieldUsed(hJob, "kun.zahl_art"))
	{
		sprintf(szTemp2, "%01.0d", kun.zahl_art );
		LlDefineFieldExt(hJob, "kun.zahl_art",szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.bank_kun"))
	{
		LlDefineFieldExt(hJob, "kun.bank_kun",kun.bank_kun, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.blz"))
	{
		sprintf(szTemp2, "%01.0d", kun.blz );
		LlDefineFieldExt(hJob, "kun.blz",szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.kto_nr"))
	{
		LlDefineFieldExt(hJob, "kun.kto_nr",kun.kto_nr, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.sprache"))
	{
		sprintf(szTemp2, "%01.0d", kun.sprache );
		LlDefineFieldExt(hJob, "kun.sprache",szTemp2, LL_NUMERIC, NULL);
	}
// 251114
	if (LlPrintIsFieldUsed(hJob, "kun.sprache2"))
	{
		sprintf(szTemp2, "%01.0d", kun.sprache2 );
		LlDefineFieldExt(hJob, "kun.sprache",szTemp2, LL_NUMERIC, NULL);
	}

// 051210
	if (LlPrintIsFieldUsed(hJob, "kun.kun_gr1"))
	{
		sprintf(szTemp2, "%01.0d", kun.kun_gr1 );
		LlDefineFieldExt(hJob, "kun.kun_gr1",szTemp2, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "kun.kun_gr2"))
	{
		sprintf(szTemp2, "%01.0d", kun.kun_gr2 );
		LlDefineFieldExt(hJob, "kun.kun_gr2",szTemp2, LL_TEXT, NULL);
	}

// 211010
	if (LlPrintIsFieldUsed(hJob, "kun.kun_krz1"))
	{
		LlDefineFieldExt(hJob, "kun.kun_krz1",kun.kun_krz1, LL_TEXT, NULL);
	}

// 220910
	if (LlPrintIsFieldUsed(hJob, "kun.kun_bran2"))
	{
		LlDefineFieldExt(hJob, "kun.kun_bran2",kun.kun_bran2, LL_TEXT, NULL);
	}

// 290910
	if (LlPrintIsFieldUsed(hJob, "rtab.a_bas.ean"))
	{
		sprintf ( szTemp2, "%s" ,eanholen( )) ;

		LlDefineFieldExt(hJob, "rtab.a_bas.ean",szTemp2, LL_TEXT, NULL);
	}


	if (LlPrintIsFieldUsed(hJob, "rtab.rpos_txt"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.rpos_txt );
		LlDefineFieldExt(hJob, "rtab.rpos_txt",szTemp2, LL_NUMERIC, NULL);
	}
	//220504 : retourentext nur behandeln, wenn er groesser als 0 ist 
	if ( reporech.rpos_txt > 0 ) textsetzen ( hJob,"rpos", "F" );

	if (LlPrintIsFieldUsed(hJob, "rtab.ktx_jebel"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.ktx_jebel );
		LlDefineFieldExt(hJob, "rtab.ktx_jebel", szTemp2, LL_NUMERIC, NULL);
	}
		textsetzen ( hJob,"KTX", "F" );

	if (LlPrintIsFieldUsed(hJob, "rtab.ftx_jebel"))
	{
		sprintf(szTemp2, "%1.0ld", reporech.ftx_jebel );
		LlDefineFieldExt(hJob, "rtab.ftx_jebel", szTemp2, LL_NUMERIC, NULL);
	}
	textsetzen ( hJob,"FTX", "F" );

	if (LlPrintIsFieldUsed(hJob, "rtab.lief_art"))
	{
		sprintf(szTemp2, "%1.0lf", reporech.lief_art );
		LlDefineFieldExt(hJob, "rtab.lief_art", szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.tel"))
	{
		LlDefineFieldExt(hJob, "rtab.tel",reporech.tel, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.fax"))
	{
		LlDefineFieldExt(hJob, "rtab.fax",reporech.fax, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.auf_me_bz"))
	{
		LlDefineFieldExt(hJob, "rtab.auf_me_bz",reporech.auf_me_bz, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.lief_me1"))
	{
		sprintf(szTemp2, "%1.3lf", reporech.lief_me1 );
		LlDefineFieldExt(hJob, "rtab.lief_me1", szTemp2, LL_NUMERIC, NULL);
 	}
	if (LlPrintIsFieldUsed(hJob, "rtab.red_inh"))	// 110608
	{
		double red_inh = 0.0 ;
		int  istat = 0 ;
		if ( reporech.a_typ == 1 )
		{
			a_hndw.a = reporech.a ;
			istat = a_hndw_class.opena_hndw () ;
			if ( !istat ) istat = a_hndw_class.lesea_hndw () ;
			if ( !istat ) red_inh = a_hndw.inh ;
		}
		if ( reporech.a_typ == 2 )
		{
			a_eig.a = reporech.a ;
			istat = a_eig_class.opena_eig () ;
			if ( !istat ) istat = a_eig_class.lesea_eig () ;
			if ( !istat ) red_inh = a_eig.inh ;
		}
		if ( reporech.a_typ == 3 )
		{
			a_eig_div.a = reporech.a ;
			istat = a_eig_div_class.opena_eig_div () ;
			if ( !istat ) istat = a_eig_div_class.lesea_eig_div () ;
			if ( !istat ) red_inh = a_eig_div.inh ;
		}

		sprintf(szTemp2, "%1.3lf", red_inh );
		LlDefineFieldExt(hJob, "rtab.red_inh", szTemp2, LL_NUMERIC, NULL);
 	}
	if (LlPrintIsFieldUsed(hJob, "rtab.ls_charge"))
	{
		LlDefineFieldExt(hJob, "rtab.ls_charge",reporech.ls_charge, LL_TEXT, NULL);
	}
 
	if (LlPrintIsFieldUsed(hJob, "rtab.kunsteunum"))
	{
		LlDefineFieldExt(hJob, "rtab.kunsteunum",kun.ust_nummer, LL_TEXT, NULL);
	}
	// 250806 A
	if (LlPrintIsFieldUsed(hJob, "rtab.kunfak_kz"))
	{
		LlDefineFieldExt(hJob, "rtab.kunfak_kz",kun.fak_kz, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rtab.kunfak_nr"))
	{
		sprintf(szTemp2, "%1.0ld", kun.fak_nr );
		LlDefineFieldExt(hJob, "rtab.kunfak_nr", szTemp2, LL_NUMERIC, NULL);
	}
// 250806 E


// 021204 
	if (LlPrintIsFieldUsed(hJob, "rtab.silent_pr"))
	{
		sprintf(szTemp2, "%d", dsilent );
		LlDefineFieldExt(hJob, "rtab.silent_pr", szTemp2, LL_NUMERIC, NULL);
	}
// 081204 
	if (LlPrintIsFieldUsed(hJob, "rtab.format"))
	{
		LlDefineFieldExt(hJob, "rtab.format", Internformat, LL_NUMERIC, NULL);
	}
// 010507 
	if (LlPrintIsFieldUsed(hJob, "rtab.blg_typ"))
	{
		LlDefineFieldExt(hJob, "rtab.blg_typ", blgtyp, LL_TEXT, NULL);
	}

// 081204 
	if (LlPrintIsFieldUsed(hJob, "rtab.ohnemitrab"))
	{
		LlDefineFieldExt(hJob, "rtab.ohnemitrab", ohnemitrab, LL_NUMERIC, NULL);
	}
// 240205 
	if (LlPrintIsFieldUsed(hJob, "rtab.duplikat"))
	{
		sprintf(szTemp2, "%d", dduplikat );
		LlDefineFieldExt(hJob, "rtab.duplikat", szTemp2, LL_NUMERIC, NULL);
	}

// 291010
	if (LlPrintIsFieldUsed(hJob, "rtab.teil_smt"))
		if ( rech.teil_smt == 0 )
			sprintf(szTemp2, "0");
		else
			sprintf(szTemp2, "%d", rech.teil_smt );
		LlDefineFieldExt(hJob, "rtab.teil_smt",szTemp2, LL_NUMERIC, NULL);

// 150605 A  
// 050213 lsk.auf dazu 
			// 131113 / 281113 : pers_nam dazu

	if (LlPrintIsFieldUsed(hJob, "rtab.hinweis")
			|| LlPrintIsFieldUsed(hJob, "rtab.lieferzeit") 
			|| LlPrintIsFieldUsed(hJob, "rtab.tour") 
			|| LlPrintIsFieldUsed(hJob, "rtab.auf") 
			|| LlPrintIsFieldUsed(hJob, "rtab.lpers_nam") 
			|| LlPrintIsFieldUsed(hJob, "rtab.apers_nam") 
			|| LlPrintIsFieldUsed(hJob, "rtab.tour_bez"))
	{
		if ( blgtyp[0] == 'N' || blgtyp[0] == 'U' )
		{	// 130707 Hinweis erst mal platt machen 
// 071108 sprintf(lsk.hinweis,""); sprintf(lsk.lieferzeit,""); sprintf(tou.tou_bz,""); tou.tou = 0 ;
			if ((wechselblg != reporech.lsnr) || ( wechseltyp[0] != reporech.lsret[0] ))
			{
				// 071108 : nur einmalig je Beleg plaetten und ggf. lesen 
				sprintf ( lsk.hinweis , " " );
				sprintf ( lsk.lieferzeit , " " ) ;
				sprintf ( tou.tou_bz," " ) ;
				sprintf (lsk.pers_nam,"");	// 131113
				sprintf (aufk.pers_nam,"");	// 281113
				tou.tou = 0 ;
				lsk.auf = 0 ;	// 050213

				wechselblg = reporech.lsnr ;
				wechseltyp[0] = reporech.lsret[0] ;
				if ( blgtyp[0] == 'N' )
				{	// funktioniert nur bei Einzelbeleg !!!!
					angk.mdn = (short) reporech.mdn ;
					angk.fil = 0 ;
 					angk.ang  = reporech.rech_nr ;
					Angk.openangk () ;
					int di = Angk.leseangk (0);
					if (!di )
					{
// 201207 : Tour lesen 
						tou.tou = angk.tou ;
						if (  LlPrintIsFieldUsed(hJob, "rtab.tour") 
								|| LlPrintIsFieldUsed(hJob, "rtab.tour_bez"))
						{
							Tou.opentou() ;
							di = Tou.lesetou(0) ;
							if ( di )
							{
								tou.tou = 0 ;
								sprintf ( tou.tou_bz ," " ) ;
							}
						}

					}
					else
					{
						memcpy ( &lsk,&lsk_null, sizeof(struct LSK));
						sprintf ( lsk.hinweis , " " );
						sprintf ( lsk.lieferzeit , " " ) ;
					}
				}
				if ( blgtyp[0] == 'U' )
				{	// funktioniert nur bei Einzelbeleg !!!!
					aufk.mdn = (short) reporech.mdn ;
					aufk.fil = 0 ;
 					aufk.auf  = reporech.rech_nr ;
					Aufk.openaufk () ;
					int di = Aufk.leseaufk (0);
					if (!di )
					{
// 201207 : Tour lesen 
						tou.tou = aufk.tou ;
						if (  LlPrintIsFieldUsed(hJob, "rtab.tour") 
								|| LlPrintIsFieldUsed(hJob, "rtab.tour_bez"))
						{
							Tou.opentou() ;
							di = Tou.lesetou(0) ;
							if ( di )
							{
								tou.tou = 0 ;
								sprintf ( tou.tou_bz ," " ) ;
							}
						}
					}
					else
					{
						memcpy ( &lsk,&lsk_null, sizeof(struct LSK));
						sprintf ( lsk.hinweis , " " );
						sprintf ( lsk.lieferzeit , " " ) ;
					}
				}

			};

		}
		else	// bisheriger Standard-Ablauf 
		{
			if ((wechselblg != reporech.lsnr) || ( wechseltyp[0] != reporech.lsret[0] ))
			{
// 201207 : erst mal platt machen 
				sprintf ( tou.tou_bz ," " );
				tou.tou = 0 ;
				lsk.auf = 0 ;	// 050213

				wechselblg = reporech.lsnr ;
				wechseltyp[0] = reporech.lsret[0] ;

				if ( wechseltyp[0] == 'L' )
				{
					lsk.mdn = (short) reporech.mdn ;
 					lsk.ls  = reporech.lsnr ;
					Lsk.openlsk () ;
			
					int di = Lsk.leselsk (0);

					if (!di )
					{
// 201207 : tour lesen
						tou.tou = lsk.tou ;
						if (  LlPrintIsFieldUsed(hJob, "rtab.tour") 
								|| LlPrintIsFieldUsed(hJob, "rtab.tour_bez"))
						{
							Tou.opentou() ;
							di = Tou.lesetou(0) ;
							if ( di )
							{
								tou.tou = 0 ;
								sprintf ( tou.tou_bz ," " ) ;
							}
						}
					}
					else
					{
						memcpy ( &lsk,&lsk_null, sizeof(struct LSK));
						sprintf ( lsk.hinweis , " " );
						sprintf ( lsk.lieferzeit , " " ) ;
					}
// 281113 A
						if (  LlPrintIsFieldUsed(hJob, "rtab.apers_nam")	&& ( lsk.auf > 0 ))
						{
							aufk.auf = lsk.auf;
							aufk.mdn = reporech.mdn;
							aufk.fil = 0;
							di = Aufk.openaufk();
							di = Aufk.leseaufk(0);
						}
// 281113 E

			
				}

				else
				{

// 200706 
					retk.mdn = (short) reporech.mdn ;
 					retk.ret  = reporech.lsnr ;
					Retk.openretk () ;
			
					int di = Retk.leseretk (0);

					if (!di )
					{
						sprintf ( lsk.hinweis, "%s", retk.hinweis )	;
						sprintf ( lsk.lieferzeit , " " ) ;
					}
					else
					{
						memcpy ( &lsk,&lsk_null, sizeof(struct LSK));
						sprintf ( lsk.hinweis , " " );
						sprintf ( lsk.lieferzeit , " " ) ;
					}
				
				}
			}
		}

		if (LlPrintIsFieldUsed(hJob, "rtab.hinweis"))
		{
			sprintf(szTemp2, "%s", lsk.hinweis );
			LlDefineFieldExt(hJob, "rtab.hinweis",szTemp2, LL_TEXT, NULL);
		}
// 131113
		if (LlPrintIsFieldUsed(hJob, "rtab.lpers_nam"))
		{
			sprintf(szTemp2, "%s", lsk.pers_nam );
			LlDefineFieldExt(hJob, "rtab.lpers_nam",szTemp2, LL_TEXT, NULL);
		}
// 281113
		if (LlPrintIsFieldUsed(hJob, "rtab.apers_nam"))
		{
			sprintf(szTemp2, "%s", aufk.pers_nam );
			LlDefineFieldExt(hJob, "rtab.apers_nam",szTemp2, LL_TEXT, NULL);
		}

// 201207 A
		if (LlPrintIsFieldUsed(hJob, "rtab.lieferzeit"))
		{
			sprintf(szTemp2, "%s", lsk.lieferzeit );
			LlDefineFieldExt(hJob, "rtab.lieferzeit",szTemp2, LL_TEXT, NULL);
		}
		if (LlPrintIsFieldUsed(hJob, "rtab.tour"))
		{
			sprintf ( szTemp2 , "%d", tou.tou ) ;
			LlDefineFieldExt(hJob, "rtab.tour",szTemp2, LL_NUMERIC, NULL);
		}

		if (LlPrintIsFieldUsed(hJob, "rtab.tour_bez"))
		{
			LlDefineFieldExt(hJob, "rtab.tour_bez",tou.tou_bz, LL_TEXT, NULL);
		}
// 201207 E
		if (LlPrintIsFieldUsed(hJob, "rtab.auf"))	// 050213
		{
			sprintf ( szTemp2 , "%d", lsk.auf ) ;
			LlDefineFieldExt(hJob, "rtab.auf",szTemp2, LL_NUMERIC, NULL);
		}
	}
// 150605  E 
}


//=============================================================================
void CMainFrame::OnEditList()
//=============================================================================
{


	char envchar[512] ;

	
	char szFilename[128+1] = "unendlich lang ..............................................................................              *.lst";	// 031014 : unendlich  
	HWND hWnd = m_hWnd;
	HJOB hJob;
    char *etc;
	char filename[512];	
// 240304 A
	FILE *fp ;
	
	etc = getenv ("BWS");
	if ( Nutzer[0] == '\0' )
		sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	else
        sprintf (filename, "%s\\format\\LL\\%s\\%s.lst", etc, Nutzer, Listenname);

	if ( TESTTEST )
	{
		if ( Nutzer[0] == '\0' )
			sprintf (filename, "d:\\test\\LL\\%s.lst",  Listenname);
		else
			sprintf (filename, "d:\\test\\LL\\bon\\%s.lst", Listenname);
	}

	sprintf ( LNutzer , "%s", Nutzer ) ;	// 060905
	fp = fopen ( filename, "r" ); 
	if ( fp== NULL )
	{
		Nutzer[0] = '\0' ;
		sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	}
	else fclose ( fp) ;

// 240304 E

	sprintf ( FILENAME_DEFAULT , filename) ;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}


	
	
	// Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden (Siehe redist.txt)
// 300506 	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");
#ifdef LL19
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "V2XSEQ");
	int v = LlSetFileExtensions(hJob, LL_PROJECT_LIST, "lst", "lsp", "lsv");
#else

#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "ht8gOw");
#else				// LL11 als default bis 120911
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");
#endif
#endif



	char * penvchar = getenv ("BWS");


	if ( Nutzer[0] == '\0' )	// 240304 
		sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);
	else
		sprintf ( envchar ,"%s\\format\\LL\\%s\\" , penvchar, Nutzer);

	if ( TESTTEST )
	{
	if ( Nutzer[0] == '\0' )
		sprintf ( envchar ,"d:\\test\\ll\\" ) ;
	else
		sprintf ( envchar ,"d:\\test\\ll\\bon" )  ;
	}

	LlSetPrinterDefaultsDir(hJob, envchar) ;	// GrJ Printer-Datei
	penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// GrJ Vorschau-Datei


	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	//GR: Exporter aktivieren
#ifdef LL19
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll19ex.llx");
// am besten nichts, da die defualts reichen sollten ....
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll12ex.llx");
#else
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");
#endif
#endif

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
		{
	        LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    // GR: Zur�cksetzen der internen Feld-Puffer
 

//	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

	if (Listenauswahl != 1)  
    {
	   	sprintf ( szFilename, FILENAME_DEFAULT );
	}
	// GR: Aufruf des Designers
    if (LlDefineLayout(hJob, hWnd, "Design Rechnung", LL_PROJECT_LIST, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "Design Rechnung", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);

	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);

}


//=============================================================================
void CMainFrame::OnPrintLabel()
//=============================================================================
{
	DoLabelPrint();
}



#ifdef OLD_DCL
char *uPPER (string)
char *string;
#else
char *uPPER (char *string)
#endif
{

   for( char *p = string; p < string + strlen( string ); p++ )
   {
      if( islower( *p ) )
			( p[0] = _toupper( *p ) );
     }
   return string ;
}


//=============================================================================
void CMainFrame::InterpretCommandLine()
//=============================================================================
{

    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");


	SetWindowText ( "Rech-Druck") ;

// Test : 	MessageBox (CommandLine, "", MB_ICONERROR);

	Nutzer[0] = '\0' ;
	LNutzer[0] = '\0' ;	// 060905
	dsilent = 0 ;
	dduplikat = 0 ;	// 240205
	party_mdn_aktiv = 0 ;

	// erweiterter Ablauf : zusaetzlich fit-Nutzer : getrennte Bereiche
    char *px ;
	char p[300] ;
	EntwurfsModus = 1 ;
	Listenname[0] = '\0' ;	// 260304

	Internformat[0] = '\0';	// 081204
	erstfolge[0] = '\0';	// 081204
	ohnemitrab[0] = '\0';	// 081204
	partymdn[0]  = '\0' ;	// 041206

	datenausdatei = 0 ;	// 080805
 
	Token.NextToken ();
	while ((px = Token.NextToken ()) != NULL)
	{

		sprintf ( p ,"%s", uPPER(px) );
		if (strcmp (p, "-H") == 0)
		{
              MessageBox ("Aufruf : dr561 -mdn Mandant -rechnr RechNr -formtyp 561000 -blgtyp BlgTyp -nutzer Name[ -wahl 1|2] [-silent 0|1] [-medium 1]",
                                "",
                                MB_OK);
			  // fuer Wahl "reicht dr561 -wahl 1|2 -nutzer Name -formtyp 5610000|5610001

			EntwurfsModus = 0 ;
              return;
		}
		if (strcmp (p, "-MDN") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            mdn = (atoi (px));
			EntwurfsModus = 0 ;
			continue ;	// 131109
		}
		if (strcmp (p, "-RECHNR") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            rechnr = (atoi (px));
			EntwurfsModus = 0 ;
			continue ;	// 131109
		}
		if (strcmp (p, "-BLGTYP") == 0)
		{
			sprintf ( blgtyp, "R" );
			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( blgtyp ,"%s", px);
			EntwurfsModus = 0 ;
			continue ;	// 131109
		}

// 230304
		if (strcmp (p, "-NUTZER") == 0)
		{

			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( Nutzer ,"%s", px);
			continue ;	// 131109
		}

// 151105 : medium-Dialog dazu .....
		if (strcmp (p, "-MEDIUM") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
			if ( ! DrMitMenue )	// 291105   sonst Zwangsdeaktivierung fuer Erstdruck
			{
            DrMitMenue = (atoi (px));
			EntwurfsModus = 0 ;
			continue ;	// 131109
#ifdef PROHNE
			DrMitMenue = 0 ;	// 080113 : Projekt temporaer auf dr561s.exe umbauen
#endif


			}
		}

// 131109
		if (strcmp (p, "-ANZAHL") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;

			danzahl = (atoi (px));
			if ( danzahl < 1 ) danzahl = 1 ;
			continue ;
		}

		if (strcmp (p, "-WAHL") == 0)
		{

	        WahlModus = 1 ;
			px = Token.NextToken ();
			if (px == NULL) break;
            wahlmod = (atoi (px));
			continue ;	// 131109
		}

// 021204

		if (strcmp (p, "-SILENT") == 0)
		{

	        dsilent = 0 ;
			px = Token.NextToken ();
			if (px == NULL) break;
            dsilent = (atoi (px));
			if ( dsilent ) dsilent = 1 ;
			continue ;	// 131109
		}

// 240205

		if (strcmp (p, "-DUPLIKAT") == 0)
		{

	        dduplikat = 0 ;	// hier stand bisher dsilent // 300106
			px = Token.NextToken ();
			if (px == NULL) break;
            dduplikat = (atoi (px));
			if ( dduplikat ) dduplikat = 1 ;
			continue ;	// 131109
		}


		if (strcmp (p, "-FORMTYP") == 0)
		{

			sprintf ( Listenname, "5610000" );
			sprintf (Internformat,"56100" );
			sprintf ( erstfolge,  "0" );
			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( Listenname ,"%s", px);
// 081204 A
			Internformat[0] = Listenname[0] ;
			Internformat[1] = Listenname[1] ;
			Internformat[2] = Listenname[2] ;
			Internformat[3] = Listenname[3] ;
			Internformat[4] = Listenname[4] ;
			Internformat[5] = '\0' ;
			ohnemitrab[0] = Listenname[5] ;
			ohnemitrab[1] = '\0' ;
			erstfolge[0] = Listenname[6] ;
			erstfolge[1] = '\0' ;
			if ( Listenname[7] == '\0' ||Listenname[6] == '\0' )	// 041206
			{
				sprintf ( partymdn,  "" );	
				
			}
			else
			{
				partymdn[0] = Listenname[7] ;
				partymdn[1] = '\0' ;
				party_mdn_aktiv = 1 ;
			}

			sprintf ( Listenname, "56100%s%s", erstfolge,partymdn);
// 081204 E

			EntwurfsModus = 0 ;
			continue ;	// 131109
		}

		if (strcmp (p, "-DATEI") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( datenausdateiname ,"%s", clippedi(uPPER(px)));
			if ( strlen ( datenausdateiname ) > 1 )
			{
				EntwurfsModus = 0 ;
				// Achtung : klappt nicht bei Namen mit ner Luecke ......	

				if ( holedatenausdatei())
				{
					datenausdatei = 1 ;
					// break ;			// evtl. spaeter alles von hier
				}
			}
		}

// 261109 : mea culpa, hier kommt man fast nie vorbei .....		char *cllrefo =  get_bws_defa ( "llrefo" )	;// 041009
//		if ( cllrefo == NULL )
//			illrefo = 0 ;
//		else
//		illrefo = atoi ( cllrefo ) ;
	}
		char *cllrefo =  get_bws_defa ( "llrefo" )	;// 041009
		if ( cllrefo == NULL )
			illrefo = 0 ;
		else
		illrefo = atoi ( cllrefo ) ;

/* ---> 151105 nach holedatenausdatei verschoben, da gehoert es ja eigentlich hin
// 260304

    if ( datenausdatei == 1 ) DrMitMenue = 0 ;
	if ( datenausdatei == 1 ) Hauptmenue = 0 ;
	if ( datenausdatei == 1 ) Listenauswahl = 0 ;
< ----- */
	
	if ( wahlmod < 1 || wahlmod > 2 ) wahlmod = 1 ;
	
	if ( WahlModus && wahlmod == 1 )
	{
		if ( party_mdn_aktiv )	// 041206
			SetWindowText ( "Party-Erstdruck") ;
		else
			SetWindowText ( "Erstdruck") ;

	    if ( Listenname[0] == '\0' )
//	081204		sprintf ( Listenname, "5610000" );
			sprintf ( Listenname, "561000" );
	}

	if ( WahlModus && wahlmod == 2 )
	{
		if ( party_mdn_aktiv )	// 041206
			SetWindowText ( "Party-FolgeExemplare") ;
		else
			SetWindowText ( "FolgeExemplare") ;

		if ( Listenname[0] == '\0' )
//	081204		sprintf ( Listenname, "5610001" );
			sprintf ( Listenname, "561001" );
	}

	if ( Listenname[0] == '\0' )
//	081204		sprintf ( Listenname, "5610000" );
			sprintf ( Listenname, "561000" );

	if ( alternat_buchst[0] != '\0' && datenausdatei )
		Listenname[3] = alternat_buchst[0] ;	// z.B. "P" fuer pdf

}


//=============================================================================
void CMainFrame::OnPrintReport()
//=============================================================================
{
    
	InterpretCommandLine() ;

	DoListPrint();
}

//=============================================================================
void CMainFrame::DoLabelPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lbl", szTemp[30], szTemp2[40], szBoxText[200];
	HJOB hJob;
	int  nRet = 0;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

	//GR: Exporter aktivieren
#ifdef LL19
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll19ex.llx");
	int v = LlSetFileExtensions(hJob, LL_PROJECT_LABEL, "lbl", "lbp", "lbv");
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll12ex.llx");
#else
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");
#endif
#endif

	// US: Choose new expression mode with formula support
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);
	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
// Labelprint
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
   		{
	    	if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	int i = 1 ;	// Portierung 120210 
	
	for ( i=1; i<10; i++)
	{
		sprintf(szTemp, "Field%d", i);
    	LlDefineVariable(hJob, szTemp, szTemp);
   	}

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

    // GR: Druck starten
// Labelprint
	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_BRIDGEMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "dr561.exe", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

	// GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog


	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	LlPrintSetOption(hJob, LL_PRNOPT_OFFSET, 0);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
        return;
    }
// Labelprint

	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

	char szPrinter[80], szPort[20];
	int  nRecCount = 30, nErrorValue = 0, nRecno, nLastPage;
// Labelprint
	LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

    nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

	// GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	while ( (( nRecno < nRecCount) || ( nRecno == 1 && nRecCount == 1))
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& nErrorValue == 0)
	{
		for (i=1; i<10; i++)
		{
			sprintf(szTemp, "Field%d", i);
			if (LlPrintIsVariableUsed(hJob, szTemp))
			{
				sprintf(szTemp2, "contents of Field%d, record %d", i, nRecno);
    			LlDefineVariable(hJob, szTemp, szTemp2);
			}
    	}
// Labelprint
    	// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    	sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    	nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        if (nErrorValue == LL_ERR_USER_ABORTED)
    	{
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
    		return;
    	}

    	// GR: Variablen drucken
    	nErrorValue = LlPrint(hJob);

		// GR: gehe zum naechsten Datensatz
    	nRecno++;
	}

	//GR: Druck beenden
	LlPrintEnd(hJob,0);

// Labelprint
	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) l�schen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }

	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
}



int iRepoRech_lesereporech (int nPrintFehler)
{ 
		int retco = RepoRech.lesereporech (nPrintFehler) ;
		
		if ( ! retco && ! nPrintFehler )	// nur beim ersten erfolgreichen Leseversuch
		{
			lade_leer_mat ();
		}
		return retco ;
}

//=============================================================================
void CMainFrame::DoListPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    char szFilename[128+1] = "*.lst", szTemp[50], szTemp2[105], szBoxText[200];	
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;
	int retc2 ;
	strcpy(szTemp, " ");
	char *etc;
	char filename[512];	
	CString sMedia;

	char envchar[512];
	etc = getenv ("BWS");


// 240304 A
	FILE *fp ;

	if (Nutzer[0] == '\0' )
		sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	else
		sprintf (filename, "%s\\format\\LL\\%s\\%s.lst", etc, Nutzer, Listenname);

	if ( TESTTEST )
	{
		if (Nutzer[0] == '\0' )
			sprintf (filename, "d:\\test\\LL\\%s.lst", Listenname);
		else
			sprintf (filename, "d:\\test\\LL\\bon\\%s.lst", Listenname);
	}

	sprintf ( LNutzer, "%s", Nutzer ) ;	// 060905
	fp = fopen ( filename, "r" ); 
	if ( fp== NULL )
	{

		if ( ! NutzerIgnorieren )	// 090109 
		{
			if (WahlModus)
			{
				MessageBox("Nutzer-Dir nicht ok", "dr561.exe", MB_OK|MB_ICONSTOP);
				return;
			}
		}
		Nutzer[0] = '\0' ;
		sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	}
	else fclose ( fp) ;

// 240304 E


	if ( Nutzer[0] == '\0' )	// 240304
		sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	else
		sprintf (filename, "%s\\format\\LL\\%s\\%s.lst", etc, Nutzer, Listenname);

	if ( TESTTEST )
	{
		if ( Nutzer[0] == '\0' )
			sprintf (filename, "d:\\test\\LL\\%s.lst", Listenname);
		else
			sprintf (filename, "d:\\test\\LL\\bon\\%s.lst", Listenname);
	}

	sprintf ( FILENAME_DEFAULT , filename);


	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet


	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "dr561.exe", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

//	LlSetOption(hJob, LL_OPTION_CODEPAGE, CP_UTF8);

// Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
// 110506	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");

#ifdef LL19
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "V2XSEQ");
	int v = LlSetFileExtensions(hJob, LL_PROJECT_LIST, "lst", "lsp", "lsv");
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "ht8gOw");
#else				// LL11 als default bis 120911
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");
#endif
#endif

	char * penvchar = getenv ("BWS");

	if ( Nutzer[0] == '\0' )
		sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);
	else
		sprintf ( envchar ,"%s\\format\\LL\\%s\\" , penvchar, Nutzer);

	if (  TESTTEST )
	{
		if ( Nutzer[0] == '\0' )
			sprintf ( envchar ,"d:\\test\\LL\\" );
		else
			sprintf ( envchar ,"d:\\test\\LL\\bon\\" );
	}

	LlSetPrinterDefaultsDir(hJob,envchar) ;	// GrJ Printer-Datei
	penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// GrJ Vorschau-Datei
	
	
	//GR: Exporter aktivieren
#ifdef LL19
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll19ex.llx");
// am besten gar nichts, da die defaults reichen solltn .......
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll12ex.llx");
#else
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");
#endif
#endif

	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );

/* ----> nicht sinnvoll unterst�tzt an dieser Stelle
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
	   	{
    		if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}
<---- */
    
	wechseltyp[0] = 'o' ;	// was anderes halt  
	wechselblg    = 0L ;	// was anderes halt und Stringende !!!!

	if ( mitmysql )
		mydbClass.opendbase (mydsn, mitmysql);


	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	dbClass.opendbase ("bws", 0 );
    Variablendefinition (hJob);
    Felderdefinition (hJob);

    // GR: Druck starten

    if ( DrMitMenue == 1 && WahlModus == 0  )	// Wahlmodus == 0 dazu am 051014	
    {
		
		if (LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...") < 0)
		{
			MessageBox("Error While Printing", "List & Label Fak-Druck App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			 dbClass.closedbase ("bws");
			return;
		}
	}
	else	// Druck pur immer auf den Drucker
	{
// 080805 : alternat_ausgabe-Auswertung dazugebaut
		if ( alternat_ausgabe )
		{
			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.Path",alternat_dir);
			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.File",alternat_file);
			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);

			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
 
			retc2 = LlPrintStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							(int) NULL );
		}
		else
		{
			if ( ! WahlModus )	// 051014 : WahlModus auswerten ...
			{
				retc2 = LlPrintStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_NORMAL,
							(int) NULL /* dummy */) ;
			}
			else
				retc2 = 0 ;
		}

		if (retc2 < 0 )
		{
			MessageBox("Error While Printing", "dr561", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			dbClass.closedbase ("bws");
			return;
		}
   } 

// 131109 A
	if ( danzahl < 1 ) danzahl = 1 ;	// Notbremse
	if ( (!alternat_ausgabe && danzahlerlaubt && ! DrMitMenue) || ( danzahl > 1 ) )
	{
		int i = LlPrintSetOption(hJob, LL_PRNOPT_COPIES, danzahl);
		i = LlPrintGetOption(hJob, LL_PRNOPT_COPIES_SUPPORTED);
	}

	if ( alternat_ausgabe )
	{
		LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
	}

// 131109 E

	LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);

	if ( DrMitMenue == 1  )	// 300306 : immer blind bei alternat-Ausgabe
    {
		char histrix[56] ;
	    sprintf ( histrix , "Drucker-Einrichtung" );
//		if ( WahlModus && wahlmod == 1 ) sprintf ( histrix , "Drucker-Einrichtung Erstdruck" );
//		if ( WahlModus && wahlmod == 2 ) sprintf ( histrix , "Drucker-Einrichtung Folgedruck" );

		if ( WahlModus )
		{
// 041009 : alter Ablauf : llrefo == 0 || llrefo == 2 || DrMitMenue 
// 041009 : neuer Ablauf : WahlModus & llrefo > 1 bei WahlModus == 1 : komplette Schleife; Wahlmodus=2 : gar nix machen   

			if ( illrefo == 0 )
			{
//				if (LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) < 0)
				int testi = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename);
				if ( testi < 0 )
				{
					LlPrintEnd(hJob,0);
					LlJobClose(hJob);
					dbClass.closedbase ("bws");
					if (Hauptmenue == 0) 
					{
						PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
					}
					return;
				}
			}
			else	// 041009 : ab hier alles neu ......
			{

				if ( wahlmod == 1 )
				{

					wahlmod -- ;
					while ( wahlmod < illrefo )
					{
						if ( party_mdn_aktiv )
						{
							if ( wahlmod == 0 ) 
								sprintf ( Listenname , "561000%s",partymdn ) ;
							else
								sprintf ( Listenname , "56100%1.0d%s",wahlmod,partymdn ) ;
						}
						else
						{
							if ( wahlmod == 0 ) 
								sprintf ( Listenname , "561000" ) ;
							else
								sprintf ( Listenname , "56100%1.0d",wahlmod ) ;
						}

						wahlmod ++ ;

						if ( party_mdn_aktiv )
						{
							if ( wahlmod == 1 )
								SetWindowText ( "Party-Erstdruck") ;
							else
							{
								if ( wahlmod == illrefo )
								{
									SetWindowText ( "Party-FolgeExemplare") ;
								}
								else
								{
									char hilfex [33] ;
									sprintf ( hilfex , " Party-Exemplar %1.0d " , wahlmod ) ;
									SetWindowText ( hilfex) ;
								}

							}
						}
						else
						{
							if ( wahlmod == 1 )
								SetWindowText ( "Erstdruck") ;
							else
							{
								if ( wahlmod == illrefo )
								{
									SetWindowText ( "FolgeExemplare") ;
								}
								else
								{
									char hilfex [33] ;
									sprintf ( hilfex , "Exemplar %1.0d " , wahlmod ) ;
									SetWindowText ( hilfex) ;
								}

							}
						}

						etc = getenv ( "BWS" ) ;
						if (Nutzer[0] == '\0' )
							sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
						else
							sprintf (filename, "%s\\format\\LL\\%s\\%s.lst", etc, Nutzer, Listenname);
						fp = fopen ( filename, "r" ); 

						if ( fp== NULL )
						{
							if ( ! NutzerIgnorieren )	// 090109 
							{
								MessageBox("Nutzer-Dir nicht ok", "dr561.exe", MB_OK|MB_ICONSTOP);
								return;
							}
							else
							{
								Nutzer[0] = '\0' ;
								sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
							}
						}
						else fclose ( fp) ;

						sprintf ( szFilename , "%s" , filename ) ;
		//				if (LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) < 0)
						int testi = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename);
						if ( testi < 0 )
						{
							LlPrintEnd(hJob,0);
							LlJobClose(hJob);
							dbClass.closedbase ("bws");
							if (Hauptmenue == 0) 
							{
								PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
							}
							return;
						}
					}
				}

			}
		}
		else
		{
		
			if (LlPrintOptionsDialog(hJob, hWnd, histrix) < 0)
			{
				LlPrintEnd(hJob,0);
				LlJobClose(hJob);
				dbClass.closedbase ("bws");
				if (Hauptmenue == 0)
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
				return;
			}

		}

		if ( WahlModus )
		{

			// wenn man bis hierher kommt, bekommt man einen LZF, ich weiss aber nicht, warum
			// das exit(0) unterdr�ckt zumindest die Fehlermeldung, ist aber ansonsten recht brutal 
			LlPrintEnd(hJob,0);
			LlJobClose(hJob);	// 051014 : reaktiviert ...
			dbClass.closedbase ("bws");
// 031014			exit (0) ; -> eins runter ?!
// 031014			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			exit (0) ;
	        return;
		}
		//GR: Druckziel abfragen
		LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		sMedia.ReleaseBuffer();
    }
	else
	{
		//GR: Druckziel abfragen
// 080805 : Alternative Ausgabe eingebaut
		if ( alternat_ausgabe )
		{
			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);

			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Path", alternat_dir);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.File", alternat_file);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Quiet", "1");
//			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_PRINTDST_FILENAME, alternat_file);


			if ( ! strcmp ( alternat_type , "PDF") )
			{
// 080905 : Mit diesen Parametern ergibt sich je nach Vorlage ein Reduktionsfaktor 5 .. 10 gegenueber
// der default-Parametrierung beim Einsatz von Logos und RTF-Texten 				
// Performance-Fresser bleibt RTF-Text, Platz-Fresser bleiben bitmaps und Bilder
				                                                                                             // min defa max
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}


		}
		else
		{
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		}
		sMedia.ReleaseBuffer();
	}

//	LlPrintCopyPrinterConfiguration (hJob, szneupfilename, , LL_PRINTERCONFIG_SAVE );
//	LlPrintCopyPrinterConfiguration (hJob, szpfilename, , LL_PRINTERCONFIG_RESTORE );

	//GR: Druckziel abfragen
//	CString sMedia;
//	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
//	sMedia.ReleaseBuffer();

    int  nRecCount = 1, nErrorValue = 0, nPrintFehler = 0, nLastPage, nRecno;
	char szPrinter[80], szPort[20];

    LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

// 221104 A 
	sprintf ( sys_par.sys_par_nam,"a_zustxt_wa" );

	sys_par.sys_par_wrt[0] = '\0' ;

	Sys_par.opensys_par();
	da_zustxt_wa = Sys_par.lesesys_par(0);

	if ( da_zustxt_wa || sys_par.sys_par_wrt[0] == '\0')
	{
		da_zustxt_wa = 0 ;
	}
	else
	{
		da_zustxt_wa = 0;
		da_zustxt_wa = atoi ( sys_par.sys_par_wrt ) ;
	}
	
// 221104 E






// 041206 A 
	sprintf ( sys_par.sys_par_nam,"party_mdn" );

	sys_par.sys_par_wrt[0] = '\0' ;
// -> wird in der kdo-Zeile akrtiviert	party_mdn_aktiv = 0 ;

	Sys_par.opensys_par();
	dparty_mdn = Sys_par.lesesys_par(0);

	if ( dparty_mdn || sys_par.sys_par_wrt[0] == '\0')
	{
		dparty_mdn = 0 ;
		party_mdn_aktiv = 0 ;
	}
	else
	{
		dparty_mdn = 0;
		dparty_mdn = atoi ( sys_par.sys_par_wrt ) ;
		// bissel doppelt gemoppelt : mandant muss identisch sein und es muss
		// eine alternative Layoutliste existieren
// reporech ist noch nicht aktiv		if ( dparty_mdn == reporech.mdn && party_mdn_aktiv )
//			party_mdn_aktiv = dparty_mdn ;
	}
	
// 041206 E

// 160905 A 
	sprintf ( sys_par.sys_par_nam,"nachkpreis" );

	sys_par.sys_par_wrt[0] = '\0' ;

	Sys_par.opensys_par();
	dnachkpreis = Sys_par.lesesys_par(0);

	if ( dnachkpreis || sys_par.sys_par_wrt[0] == '\0')
	{
		dnachkpreis = 2 ;
	}
	else
	{
		dnachkpreis = 2;
		dnachkpreis = atoi ( sys_par.sys_par_wrt ) ;
		if ( dnachkpreis > 4 || dnachkpreis < 2 ) dnachkpreis = 2 ;
	}

//	231213 A
	sprintf ( sys_par.sys_par_nam,"dietzosep" );

	sys_par.sys_par_wrt[0] = '\0' ;

	Sys_par.opensys_par();
	dietzosep = Sys_par.lesesys_par(0);

	if ( dietzosep || sys_par.sys_par_wrt[0] == '\0')
	{
		dietzosep = 0  ;
	}
	else
	{
		dietzosep = atoi ( sys_par.sys_par_wrt ) ;
	}

//	031114 A
	sprintf ( sys_par.sys_par_nam,"essiggrund" );

	sys_par.sys_par_wrt[0] = '\0' ;

	Sys_par.opensys_par();
	spessiggrund = Sys_par.lesesys_par(0);

	if ( spessiggrund || sys_par.sys_par_wrt[0] == '\0')
	{
		spessiggrund = 0  ;
	}
	else
	{
		spessiggrund = atoi ( sys_par.sys_par_wrt ) ;
	}


//	250713 A
	sprintf ( sys_par.sys_par_nam,"bar_kreis_par" );

	sys_par.sys_par_wrt[0] = '\0' ;

	Sys_par.opensys_par();
	bar_kreis_par = Sys_par.lesesys_par(0);

	if ( bar_kreis_par || sys_par.sys_par_wrt[0] == '\0')
	{
		bar_kreis_par = 0  ;
	}
	else
	{
		bar_kreis_par = atoi ( sys_par.sys_par_wrt ) ;
	}

	sprintf ( sys_par.sys_par_nam,"leerbest2" );

	sys_par.sys_par_wrt[0] = '\0' ;

	Sys_par.opensys_par();
	leerbest2 = Sys_par.lesesys_par(0);

	if ( leerbest2 || sys_par.sys_par_wrt[0] == '\0')
	{
		leerbest2 = 0  ;
	}
	else
	{
		leerbest2 = atoi ( sys_par.sys_par_wrt ) ;
	}

//	250713 E

	nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	nRecCount = GetRecCount () ;
//	sprintf (dmess, "nRecCount = %hd",nRecCount);  //050803
//	MessageBox(dmess, "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
	int di = RepoRech.lesereporech (0);   //       1.DATENSATZ lesen f�r Variablen�bergabe
	// nErrorValue als Paramter ist hier nicht gut
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				dbClass.closedbase ("bws");
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
	// 041206
 	if ( dparty_mdn == reporech.mdn && party_mdn_aktiv )
		party_mdn_aktiv = dparty_mdn ;

//	Adr.SetDatabase (dbClass.GetDatabase());
//	Adr.openadr (); 

// 250708 A  : reporech.mdnadr wird erst seit heute korrekt bestueckt 
	if ( reporech.mdnadr > 0 )
	{
		adr.adr = reporech.mdnadr ;
		Adr.openadr (); 
		di = Adr.leseadr (); 
		if (!di )
		{
			memcpy ( &adr1,&adr, sizeof(struct ADR));
			if ( adr1.anr > 0 )	// 091208 
			{
				sprintf ( ptabn.ptitem, "anr" ) ;
				sprintf ( ptabn.ptwert,"%d",adr1.anr );
				Ptabn.openptabn() ;
				if ( !Ptabn.leseptabn() )
				{
					sprintf ( adr1.anrtxt ,"%s", clippedi( ptabn.ptbez));
				}
			}
		}
		else
		{
			memcpy ( &adr1,&adr_null, sizeof(struct ADR));
		}
	 }
	 else
			memcpy ( &adr1,&adr_null, sizeof(struct ADR));

// 250708 E

	 if ( reporech.adr2 > 0 )
	 {
		adr.adr = reporech.adr2 ;
		Adr.openadr (); 
		di = Adr.leseadr (); 
		if (!di )
		{
			memcpy ( &adr2,&adr, sizeof(struct ADR));
			if ( adr2.anr > 0 )	// 091208 
			{
				sprintf ( ptabn.ptitem, "anr" ) ;
				sprintf ( ptabn.ptwert,"%d",adr2.anr );
				Ptabn.openptabn() ;
				if ( !Ptabn.leseptabn() )
				{
					sprintf ( adr2.anrtxt ,"%s", clippedi( ptabn.ptbez));
				}
			}
		}
		else
		{
			memcpy ( &adr2,&adr_null, sizeof(struct ADR));
		}
	 }
	 else
			memcpy ( &adr2,&adr_null, sizeof(struct ADR));

	 if ( reporech.adr3 > 0 )
	 {
		adr.adr = reporech.adr3 ;
		Adr.openadr (); 
		di = Adr.leseadr ();
		if (!di )
		{
			memcpy ( &adr3,&adr, sizeof(struct ADR));
			if ( adr3.anr > 0 )	// 091208 
			{
				sprintf ( ptabn.ptitem, "anr" ) ;
				sprintf ( ptabn.ptwert,"%d",adr3.anr );
				Ptabn.openptabn() ;
				if ( !Ptabn.leseptabn() )
				{
					sprintf ( adr3.anrtxt ,"%s", clippedi( ptabn.ptbez));
				}
			}
		}
		else
		{
			memcpy ( &adr3,&adr_null, sizeof(struct ADR));
		}
	 }
	 else
			memcpy ( &adr3,&adr_null, sizeof(struct ADR));

	kun.kun = reporech.kun ;
	kun.mdn = (short) reporech.mdn ;
	Kun.openkun (); 
	di = Kun.lesekun ();

	if (!di )
	{
//				memcpy ( &kunx,&kun, sizeof(struct KUN));
// 011014 A
		if ( kun.adr1 > 0 )
		{
			adr.adr = kun.adr1 ;
			Adr.openadr (); 
			di = Adr.leseadr (); 
			if (!di )
			{
				memcpy ( &kunadr1,&adr, sizeof(struct ADR));
				if ( kunadr1.anr > 0 )
				{
					sprintf ( ptabn.ptitem, "anr" ) ;
					sprintf ( ptabn.ptwert,"%d",kunadr1.anr );
					Ptabn.openptabn() ;
					if ( !Ptabn.leseptabn() )
					{
						sprintf ( kunadr1.anrtxt ,"%s", clippedi( ptabn.ptbez));
					}
				}
			}
			else
			{
				memcpy ( &kunadr1,&adr_null, sizeof(struct ADR));
			}
		 }
		 else
			memcpy ( &kunadr1,&adr_null, sizeof(struct ADR));
	
	}
	else
	{
		memcpy ( &kun,&kun_null, sizeof(struct KUN));
	}


// 130413
	memcpy ( &kase_fit, &kase_fit_null, sizeof ( struct KASE_FIT )) ;

	if ( reporech.blg_typ[0] == 'K' )
	{
		kase_fit.mdn = reporech.mdn ;
		kase_fit.fil = reporech.fil ;
		kase_fit.bon = reporech.rech_nr ;
		di = Kase_fit.openkase_fit () ;
		if ( !di )
			di = Kase_fit.lesekase_fit () ;
		if ( di )
			memcpy ( &kase_fit, &kase_fit_null, sizeof ( struct KASE_FIT )) ;
	}

// 291010 : lese die ganze Rechnung nochmal ?!
	int x291010 = 1 ;
	if ( x291010 == 1 )
	{
	rech.mdn = (short) reporech.mdn ;
	rech.rech_nr = reporech.rech_nr ;
	sprintf( rech.blg_typ , "%s",  reporech.blg_typ ) ;
	Rech.openrech ('x') ;
	di = Rech.leserech('x') ;
	if (!di )
	{
//				memcpy ( &rechx,&rech, sizeof(struct RECH));
	}
	else
	{
		memcpy ( &rech,&rech_null, sizeof(struct RECH));
	}
	}

	init_leer_mat();	// 040805

	laderabaz();		// 090709



	a_kun.a = -133 ;	// 290910


	RepoRech.openreporech ();   
//	while (nRecno < nRecCount
	while ( (( nRecno < nRecCount) || ( nRecno == 1 && nRecCount == 1))
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA))
   	{
	    VariablenUebergabe ( hJob, szTemp2, nRecno );

		nErrorValue = LlPrint(hJob);
		while (nRecno <= nRecCount 
			&& (nErrorValue == 0) 
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (iRepoRech_lesereporech (nPrintFehler) == 0))  //       DATENSATZ lesen

		{

		    FelderUebergabe ( hJob, szTemp2, nRecno );
    		// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    		sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    		nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        	if (nErrorValue == LL_ERR_USER_ABORTED)
   			{
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				dbClass.closedbase ("bws");
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}

				return;
   			}

    		// GR: Drucken der aktuellen Tabellenzeile
    		nErrorValue = LlPrintFields(hJob);
			nPrintFehler = nErrorValue;
//050803
			if (nErrorValue == LL_WRN_REPEAT_DATA)
			{
					// GR: Aktualisieren der seitenabhaengigen Variablen
			    VariablenUebergabe ( hJob, szTemp2, nRecno );
				nErrorValue = LlPrint(hJob);
//			    FelderUebergabe ( hJob, szTemp2, nRecno );
//	    		nErrorValue = LlPrintFields(hJob);		
			}
			else	// 090609
			{
				if (nErrorValue == LL_ERR_NO_TABLEOBJECT)
				{
					VariablenUebergabe ( hJob, szTemp2, nRecno );
					nErrorValue = LlPrint(hJob);
					FelderUebergabe ( hJob, szTemp2, nRecno );	
	    			nErrorValue = LlPrintFields(hJob);	
					nPrintFehler = nErrorValue;

				}
			}


			if (nErrorValue == 0)	// US: everything fine, record could have been printed...
	    	{
				// US: ...but probably the user defined a filter condition!
				//     So before updating time dependent variables we have to check if the record really has been printed:
				if (LlPrintDidMatchFilter(hJob))
				{
					// GR: Aktualisieren der zeitabhaengigen Variable 'FixedVariable2'
					// US: update the time dependent variable 'FixedVariable2'
					//     coming after printig the list(s)
		    		sprintf(szTemp2, "FixedVariable2, record %d", nRecno);
		    		LlDefineVariable(hJob, "FixedVariable2", szTemp2);
		    	}

	    		if (nPrintFehler == 0) nRecno++;	// GR: gehe zum naechsten Datensatz
			}
		}
  	}

	// US: all records have been printed, now flush the table
	//     If footer doesn't fit to this page try again for the next page:

	VariablenUebergabe ( hJob, szTemp2, nRecno );

	nErrorValue = LlPrintFieldsEnd(hJob);
	if (nErrorValue == LL_WRN_REPEAT_DATA)
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	    VariablenUebergabe ( hJob, szTemp2, nRecno );
	    FelderUebergabe ( hJob, szTemp2, nRecno );

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);
	}
// 190914 : unter druck : 2 mal wiederholen 
	if (nErrorValue == LL_WRN_REPEAT_DATA)
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	    VariablenUebergabe ( hJob, szTemp2, nRecno );
	    FelderUebergabe ( hJob, szTemp2, nRecno );

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);
	}

	if (nErrorValue == LL_WRN_REPEAT_DATA)	// US: footer still doesn't fit!
		MessageBox("Error because table is too small for footer!", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);

	//GR: Druck beenden
	LlPrintEnd(hJob,0);
	
	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) loeschen
        LlPreviewDisplay(hJob, szFilename, penvchar, hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, penvchar);
    }
	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
	dbClass.closedbase ("bws");
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}

}

//=============================================================================
LRESULT CMainFrame::OnLulMessage(WPARAM wParam, LPARAM lParam)
//=============================================================================
{
	// GR: Dies ist die List & Label Callback Funktion.
	//     Saemtliche Rueckmeldungen bzw. Events werden dieser Funktion
	//     gemeldet.
	// US: This is the List & Label Callback function.
	//     Is is called for requests an notifications.


	PSCLLCALLBACK	pscCallback = (PSCLLCALLBACK) lParam;
	LRESULT			lRes = TRUE;
	CString			sVariableDescr;

	static CHAR szHelpText[256];

	ASSERT(pscCallback->_nSize == sizeof(scLlCallback));	// GR: Die groesse der Callback Struktur muss stimmen!
															// US: sizes of callback structs must match!

	switch(wParam)
	{
		case LL_CMND_VARHELPTEXT:	// GR: Es wird ein Beschreibungstext f�r eine Variable erfragt.
									// US: Helptext needed for selected variable within designer

				// GR: ( pscCallback->_lParam enth�lt einen LPCSTR des Beschreibungstextes )
				// US: ( pscCallback->_lParam contains a LPCSTR to the name of the selected variable )

				sVariableDescr = (LPCSTR)pscCallback->_lParam;

				if (!sVariableDescr.IsEmpty())
					sprintf(szHelpText,
							"This is the sample field / variable '%s'.",
							(LPCSTR)sVariableDescr);
				else
					strcpy(szHelpText, "No variable or field selected.");

				pscCallback->_lReply = (LPARAM)szHelpText;
				break;

		default:
				pscCallback->_lReply = lRes = FALSE; // GR: Die Nachricht wurde nicht bearbeitet.
													 // US: indicate that message hasn't been processed
	}

	return(lRes);
}




char *wort[256];

int next_char_ci (char *string, char tzeichen, int i)
// Naechstes Zeichen != Trennzeichen suchen.

{
       for (;string [i]; i ++)
       {
               if (string[i] != tzeichen)
               {
                                   return (i);
               }
       }
       return (i);
}


static char buffer [0x1000] ;	// 310306
static char DefWert [256] ;

// 300306 : der 2. Parameter ist dann ein endloser String bis zum #-Trenner oder CR/LF

short splite (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 wz = j = 0;
 len = (int)strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] =  buffer ;	
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
					zeichen = '#' ;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}



short split (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 wz = j = 0;
 len = (int)strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] =  buffer ;	
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}

int strupcmpi (char *str1, char *str2, int len)
{
 short i;
 char upstr1;
 char upstr2;


 for (i = 0; i < len; i ++, str1 ++, str2 ++)
 {
  if (*str1 == 0)
    return (-1);
  if (*str2 == 0)
    return (1);
  upstr1 = toupper((int) *str1);
  upstr2 = toupper((int) *str2);
  if (upstr1 < upstr2)
  {
   return(-1);
  }
  else if (upstr1 > upstr2)
  {
   return (1);
  }
 }
 return (0);
}


/**
CR am Stringende etfernen.
**/
#ifdef OLD_DCL
void cr_weg (string)
char *string;
#else
void cr_weg (char *string)
#endif
{
 for (; *string; string += 1)
 {
  if (*string == (char) 13)
    break;
  if (*string == (char) 10)
    break;
 }
 *string = 0;
 return;
}



int holedatenausdatei(void)
{
int anz;
char puffer [512];
char token[64] ;
int gefunden ;
gefunden = 0 ;
FILE *fp;
    alternat_ausgabe = 0 ;	
    alternat_buchst[0] = '\0' ;



	fp = fopen (datenausdateiname, "r");
    if (fp == (FILE *)NULL) return FALSE;
    while (fgets (puffer, 511, fp))
    {
		cr_weg (puffer);
        anz = split (puffer);
        if (anz < 2) continue;

		sprintf ( token, "ALTERNATETYP" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token)) == 0)
        {
			strcpy (alternat_type, clippedi (uPPER (wort [2])));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}

		sprintf ( token, "ALTERNATEBUCHST" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token)) == 0)
        {
			strcpy (alternat_buchst, clippedi (uPPER (wort [2])));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}

		// funktioniert vorerst NICHT mit space-Namen
		sprintf ( token, "ALTERNATENAME" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token)) == 0)
        {
			anz = splite ( puffer ) ;
			strcpy (alternat_file, clippedi (wort [2]));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}

		// funktioniert vorerst NICHT mit space-Namen
		sprintf ( token, "ALTERNATEDIR" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token)) == 0)
        {
			anz = splite ( puffer ) ;	// 040406
			strcpy (alternat_dir, clippedi (wort [2]));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}
// 131109
		sprintf ( token, "ANZAHL" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token)) == 0)
        {

			danzahl = atoi (wort[2]);
			if ( danzahl < 1 ) danzahl = 1 ;
			gefunden ++ ;
			continue ;
        }


	}
	fclose (fp);

	// 151105 : damit kann man Parameters danach noch aktualisieren ( z.B. medium-Dialog )
//    if ( datenausdatei == 1 ) DrMitMenue = 0 ;
//	if ( datenausdatei == 1 ) Hauptmenue = 0 ;
//	if ( datenausdatei == 1 ) Listenauswahl = 0 ;

    if ( alternat_ausgabe == 1 ) DrMitMenue = 0 ;
	if ( alternat_ausgabe == 1 ) Hauptmenue = 0 ;
	if ( alternat_ausgabe == 1 ) Listenauswahl = 0 ;

	if ( gefunden ) return TRUE;

	return FALSE ;
}


char *get_default (char *env)
{         
        int anz;
        char puffer [512];
        FILE *fp;
        sprintf (puffer, "%s\\dr561.cfg",getenv ("BWSETC"));
		fp = fopen (puffer, "r");
        if (fp == (FILE *)NULL) return NULL;

//110506        clippedi (env);

		while (fgets (puffer, 511, fp))
        {
                     cr_weg (puffer);
                     anz = split (puffer);
                     if (anz < 2) continue;
                     if (strupcmpi (wort[1], env, (int) strlen (env)) == 0)
                     {
                                 strcpy (DefWert, clippedi (wort [2]));
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}

// 041009
char *get_bws_defa (char *env)
{         
        int anz;
        char puffer [512];
        FILE *fp;
        sprintf (puffer, "%s\\bws_defa",getenv ("BWSETC"));
		fp = fopen (puffer, "r");
        if (fp == (FILE *)NULL) return NULL;

		while (fgets (puffer, 511, fp))
        {
                     cr_weg (puffer);
                     anz = split (puffer);
                     if (anz < 2) continue;
                     if (strupcmpi (wort[1], env, (int) strlen (env)) == 0)
                     {
                                 strcpy (DefWert, clippedi (wort [2]));
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}


void CMainFrame::GetCfgValues (void)
{
//       char cfg_v [1024];
//       if (ProgCfg == NULL) return;


	char hilfstr[256] ;
	char *p ;

	
	mitmysql = 0;	
	sprintf ( myuid ,"root");
	sprintf ( mypass,"t0pfit_1");
	sprintf ( mydsn, "xt_shop" );
	
	p =  get_default("MySql");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		mitmysql = atoi ( hilfstr ) ;
		if ( mitmysql == 1 )
			mitmysql = 1 ;
		else
			mitmysql = 0 ;
	} ;
	if ( mitmysql == 1 )
	{
		p =  get_default("MyUid");
		if ( p != NULL )
		{
			sprintf ( myuid ,"%s", p );
		} ;
		p =  get_default("MyPass");
		if ( p != NULL )
		{
			sprintf ( mypass ,"%s", p );
		} ;
		p =  get_default("MyDsn");
		if ( p != NULL )
		{
			sprintf ( mydsn ,"%s", p );
		} ;
	}
	
	
	cfgOK = TRUE;
		
// 090109
	NutzerIgnorieren = 0 ;
	p =  get_default("NutzerIgnorieren");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		NutzerIgnorieren = atoi ( hilfstr ) ;
	} ;
	

// 131109

	danzahlerlaubt = 0 ;
	p =  get_default("anzahlerlaubt");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		danzahlerlaubt = atoi ( hilfstr ) ;
	} ;



	Hauptmenue = 0 ;
	p =  get_default("Hauptmenue");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		Hauptmenue = atoi ( hilfstr ) ;
	} ;
	

	Listenauswahl = 0 ;
	p =  get_default("Listenauswahl");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		Listenauswahl = atoi ( hilfstr ) ;
	} ;

	DrMitMenue = 0 ;
	p =  get_default("DrMitMenue");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		DrMitMenue = atoi ( hilfstr ) ;
	}
#ifdef PROHNE
		DrMitMenue = 0 ;	// 080113 : Projekt temporaer umbauen auf dr561s.exe ........
#endif

// 110913 
	eanausakungx = 0 ;
	p =  get_default("eanausakungx");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		eanausakungx = atoi ( hilfstr ) ;
	} ;

	eanausakun = 0 ;
	p =  get_default("eanausakun");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		eanausakun = atoi ( hilfstr ) ;
	} ;

// 200414 
	cfga_kun_txt = 0 ;
	p =  get_default("a_kun_txt");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		cfga_kun_txt = atoi ( hilfstr ) ;
	} ;

	cfga_bas_erw = 0 ;
	p =  get_default("a_bas_erw");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		cfga_bas_erw = atoi ( hilfstr ) ;
	} ;


/* --->
		Listenauswahl =0 ;
		if (ProgCfg->GetCfgValue ("Listenauswahl", cfg_v) == TRUE)
       {
           Listenauswahl = atoi (cfg_v);
	   }
< -*/
/* -->
		Hauptmenue = 0;
		if (ProgCfg->GetCfgValue ("Hauptmenue", cfg_v) == TRUE)
       {
           Hauptmenue = atoi (cfg_v);
	   }

	   exit(0) ;
< ---- */

/* --->
		DrMitMenue = 1 ;
       if (ProgCfg->GetCfgValue ("DrMitMenue", cfg_v) == TRUE)
       {
           DrMitMenue = atoi (cfg_v);
	   }
< ---- */

}


    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt
	/************
	LlDefineFieldExt(hJob, "Barcode_EAN13", "44|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P2", "44|44444|44444|44", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P5", "44|44444|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN128", "EAN128ean128", LL_BARCODE_EAN128, NULL);
	LlDefineFieldExt(hJob, "Barcode_CODE128", "Code 128", LL_BARCODE_CODE128, NULL);
	LlDefineFieldExt(hJob, "Barcode_Codabar", "A123456A", LL_BARCODE_CODABAR, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCA", "44|44444", LL_BARCODE_EAN8, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCE", "4|44444|44444", LL_BARCODE_UPCA, NULL);
	LlDefineFieldExt(hJob, "Barcode_3OF9", "*TEST*", LL_BARCODE_3OF9, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IND", "44444", LL_BARCODE_25INDUSTRIAL, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IL", "444444", LL_BARCODE_25INTERLEAVED, NULL);
	LlDefineFieldExt(hJob, "Barcode_25MAT", "44444", LL_BARCODE_25MATRIX, NULL);
	LlDefineFieldExt(hJob, "Barcode_25DL", "44444", LL_BARCODE_25DATALOGIC, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET5", "44444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET10", "44444-4444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET12", "44444-444444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_FIM", "A", LL_BARCODE_FIM, NULL);
	**********/
void fitapldefines (HJOB hJob)
{

}


