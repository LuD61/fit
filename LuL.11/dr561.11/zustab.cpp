#include "stdafx.h"
#include "DbClass.h"
#include "zustab.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif
struct SYS_PAR sys_par,  sys_par_null;
struct A_ZUS_TXT a_zus_txt, a_zus_txt_null;

struct A_BAS_ERW a_bas_erw, a_bas_erw_null;	// 200214
struct A_KUN_TXT a_kun_txt, a_kun_txt_null;	// 200214


extern struct PTABN ptabn, ptabn_null ;		// 110506 

extern DB_CLASS dbClass;


int SYS_PAR_CLASS::lesesys_par (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int SYS_PAR_CLASS::opensys_par (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

void SYS_PAR_CLASS::prepare (void)
{


	dbClass.sqlin ((char *) sys_par.sys_par_nam, SQLCHAR, 18);

	dbClass.sqlout ((char *) sys_par.sys_par_wrt,SQLCHAR, 2);
	dbClass.sqlout ((char *) sys_par.sys_par_besch,SQLCHAR, 33);
	dbClass.sqlout ((long *) &sys_par.zei, SQLLONG, 0);
	dbClass.sqlout ((short *) &sys_par.delstatus, SQLSHORT, 0);

		readcursor = dbClass.sqlcursor ("select "

	" sys_par_wrt, sys_par_besch, zei, delstatus " 
	" from sys_par where sys_par_nam = ? " ) ;
	

}

int A_ZUS_TXT_CLASS::lesea_zus_txt (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
	  	memcpy ( &a_zus_txt, &a_zus_txt_null , sizeof ( struct A_ZUS_TXT )) ;	// 290506

      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_ZUS_TXT_CLASS::opena_zus_txt (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

void A_ZUS_TXT_CLASS::prepare (void)
{


	dbClass.sqlin ((double *) &a_zus_txt.a, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) a_zus_txt.txt,SQLCHAR, 61);
	dbClass.sqlout ((short *) &a_zus_txt.sg1,SQLSHORT, 0);
	dbClass.sqlout ((long *) &a_zus_txt.zei, SQLLONG, 0);

		readcursor = dbClass.sqlcursor ("select "

	" txt, sg1, zei " 
	" from a_zus_txt where a = ?  and zei > 0 order by zei" ) ;

}

// 110506 
int PTABN_CLASS::leseptabn (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int PTABN_CLASS::openptabn (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

void PTABN_CLASS::prepare (void)
{

		test_upd_cursor = 1;

	dbClass.sqlin ((char *) ptabn.ptwert, SQLCHAR, 4);
	dbClass.sqlin ((char *) ptabn.ptitem, SQLCHAR, 19);

	dbClass.sqlout ((long *) &ptabn.ptlfnr, SQLLONG, 0);
	dbClass.sqlout ((char *) ptabn.ptbez,  SQLCHAR,33);
	dbClass.sqlout ((char *) ptabn.ptbezk, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer1, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer2, SQLCHAR, 9);
	
			
	readcursor = dbClass.sqlcursor ("select "

	" ptlfnr, ptbez, ptbezk, ptwer1, ptwer2  "

	" from ptabn where ptwert = ? and ptitem = ? " ) ;

}


// 200214 A


int A_KUN_TXT_CLASS::lesea_kun_txt ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_KUN_TXT_CLASS::opena_kun_txt (void)
{
		if ( readcursor < 0 ) prepare ();
		
		return dbClass.sqlopen (readcursor);
}

void A_KUN_TXT_CLASS::prepare (void)
{

	dbClass.sqlin (( long	*) &a_kun_txt.a,        SQLLONG, 0);
	dbClass.sqlin (( long	*) &a_kun_txt.kun,      SQLLONG, 0);

//	dbClass.sqlin (( char	*)  a_kun_gx.kun_bran2,SQLCHAR,  3);
//	dbClass.sqlin (( short	*) &a_kun_gx.mdn,      SQLSHORT, 0);

//	dbClass.sqlout (( short  *) &a_kun_txt.kun,		SQLLONG,	 0 ) ;

	dbClass.sqlout (( char	 *)  a_kun_txt.txt1,	SQLCHAR,	129 );
	dbClass.sqlout (( char	 *)  a_kun_txt.txt2,	SQLCHAR,	129 );
	dbClass.sqlout (( char	 *)  a_kun_txt.txt3,	SQLCHAR,	129 );
	dbClass.sqlout (( char	 *)  a_kun_txt.txt4,	SQLCHAR,	129 );
	dbClass.sqlout (( char	 *)  a_kun_txt.txt5,	SQLCHAR,	129 );
	
	dbClass.sqlout (( short	 *) &a_kun_txt.sort,	SQLSHORT, 0 );

		readcursor = dbClass.sqlcursor ("select "

	" txt1, txt2, txt3, txt4, txt5, sort "

//	" from a_kun_txt where a = ? and ( kun = ? or kun = 0 ) order by kun desc " ) ;
	" from a_kun_txt where a = ? and  kun = 0 " ) ;	// 060214 Anruf von Daniel

}


// 190214 A


int A_BAS_ERW_CLASS::lesea_bas_erw ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_BAS_ERW_CLASS::opena_bas_erw (void)
{
		if ( readcursor < 0 ) prepare ();
		
		return dbClass.sqlopen (readcursor);
}

void A_BAS_ERW_CLASS::prepare (void)
{

	dbClass.sqlin (( double	*) &a_bas_erw.a,SQLDOUBLE, 0);

    dbClass.sqlout ((char   *)  a_bas_erw.pp_a_bz1,     SQLCHAR,100);
    dbClass.sqlout ((char   *)  a_bas_erw.pp_a_bz2,     SQLCHAR,100);
    dbClass.sqlout ((char   *)  a_bas_erw.lgr_tmpr,     SQLCHAR,100);
	dbClass.sqlout ((char   *)  a_bas_erw.lupine,       SQLCHAR,3);
	dbClass.sqlout ((char   *)  a_bas_erw.schutzgas,    SQLCHAR,3);
 
	dbClass.sqlout ((short  *) &a_bas_erw.huelle,       SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.shop_wg1,     SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.shop_wg2,     SQLSHORT,0);	//  smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.shop_wg3,     SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.shop_wg4,     SQLSHORT,0);	// smallint,

	dbClass.sqlout ((short  *) &a_bas_erw.shop_wg5,     SQLSHORT,0);	// smallint,
    dbClass.sqlout ((double *) &a_bas_erw.tara2,        SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((short  *) &a_bas_erw.a_tara2,      SQLSHORT,0);	// smallint,
    dbClass.sqlout ((double *) &a_bas_erw.tara3,        SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((short  *) &a_bas_erw.a_tara3,      SQLSHORT,0);	// smallint,

	dbClass.sqlout ((double *) &a_bas_erw.tara4,        SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((short  *) &a_bas_erw.a_tara4,      SQLSHORT,0);	// smallint,
    dbClass.sqlout ((double *) &a_bas_erw.salz,         SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((double *) &a_bas_erw.davonfett,    SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((double *) &a_bas_erw.davonzucker,  SQLDOUBLE,0);	// decimal(8,3),

	dbClass.sqlout ((double *) &a_bas_erw.ballaststoffe,SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((char   *)  a_bas_erw.shop_aktion,  SQLCHAR,3);	// char(1),
    dbClass.sqlout ((char   *)  a_bas_erw.shop_neu,     SQLCHAR,3);
    dbClass.sqlout ((char   *)  a_bas_erw.shop_tv,      SQLCHAR,3);
	dbClass.sqlout ((double *) &a_bas_erw.shop_agew,    SQLDOUBLE,0);	// decimal(8,3),

	dbClass.sqlout ((char   *)  a_bas_erw.a_bez,        SQLCHAR,65);
	dbClass.sqlout ((char   *)  a_bas_erw.zutat,        SQLCHAR,2002);
    dbClass.sqlout ((short  *) &a_bas_erw.userdef1,     SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.userdef2,     SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.userdef3,     SQLSHORT,0);	// smallint,

	dbClass.sqlout ((short  *) &a_bas_erw.minstaffgr,   SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.l_pack,       SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.b_pack,       SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.h_pack,       SQLSHORT,0);	// smallint

		readcursor = dbClass.sqlcursor ("select "
	"  pp_a_bz1, pp_a_bz2, lgr_tmpr, lupine, schutzgas "
	" ,huelle, shop_wg1, shop_wg2, shop_wg3, shop_wg4 "
	" ,shop_wg5, tara2, a_tara2, tara3, a_tara3 "
	" ,tara4,  a_tara4, salz, davonfett,  davonzucker "
	" ,ballaststoffe, shop_aktion, shop_neu, shop_tv, shop_agew "
	" ,a_bez, zutat, userdef1, userdef2, userdef3 "
	" ,minstaffgr, l_pack, b_pack, h_pack "
	
	" from a_bas_erw where a = ? " ) ;

}

// 190214 E




