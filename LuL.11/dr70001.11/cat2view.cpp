// catalog2View.cpp : implementation of the CCatalog2View class
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.


#include "stdafx.h"
#include "catalog2.h"
#include "cmbtl9.h"		// 071205 : hier hoch

#include "catsets.h"
#include "cat2Doc.h"
#include "cat2View.h"
// GrJ
//#include "neuFormat.h"
//#include "FeldEigenschaften.h"
//#include "TabEigenschaften.h"
// #include "DbClass.h"
//#include "nform_tab.h"
//#include "nform_feld.h"
//#include "systables.h"
//#include "mo_db.h"
//#include "OrderWhere.h"
//#include "form_ow.h"
//#include "cmbtl9.h"
#include "mainfrm.h"
//#include "Kommentar.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// GrJ

const int LUL_LANGUAGE = CMBTLANG_GERMAN;

/////////////////////////////////////////////////////////////////////////////
// CCatalog2View

IMPLEMENT_DYNCREATE(CCatalog2View, CListView)


BEGIN_MESSAGE_MAP(CCatalog2View, CListView)
	//{{AFX_MSG_MAP(CCatalog2View)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()




/////////////////////////////////////////////////////////////////////////////
// CCatalog2View construction/destruction

CCatalog2View::CCatalog2View()
{
	m_strTableName = _T("");
}

CCatalog2View::~CCatalog2View()
{
}

BOOL CCatalog2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// set list view control to report, single selection
	cs.style &= ~(LVS_LIST | LVS_ICON | LVS_SMALLICON);
	cs.style |= LVS_REPORT;
	cs.style |= LVS_SINGLESEL;

	return CListView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCatalog2View drawing

void CCatalog2View::OnDraw(CDC* /*pDC*/)
{
	CCatalog2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CCatalog2View diagnostics

#ifdef _DEBUG
void CCatalog2View::AssertValid() const
{
	CListView::AssertValid();
}

void CCatalog2View::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}

CCatalog2Doc* CCatalog2View::GetDocument() // non-debug version is inline
{
//	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCatalog2Doc)));

	return (CCatalog2Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCatalog2View message handlers

