#include "stdafx.h"
#include "DbClass.h"
#include "adr.h"

#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

struct TOU tou,  tou_null;	// 060307 

struct ADR adr,  adr_null;
struct ADR adr2, adr3;
extern DB_CLASS dbClass;

int iadr ;
static int anzzfelder ;

int ADR_CLASS::dbcount (void)
/**
Tabelle adr lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;   //050803
}

int ADR_CLASS::leseadr (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int TOU_CLASS::lesetou (void)
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int TOU_CLASS::opentou (void)
{
		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?
         return dbClass.sqlopen (readcursor);
}

int ADR_CLASS::openadr (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}

void TOU_CLASS::prepare (void)
{

test_upd_cursor = 1;

	dbClass.sqlin ((long *) &tou.tou, SQLLONG, 0);


	dbClass.sqlout (( char *) tou.tou_bz,SQLCHAR, 49 ) ;
/* ->
	dbClass.sqlout (( char *)  tou.fz_kla,SQLCHAR,3) ;
	dbClass.sqlout (( char *)  tou.fz,SQLCHAR,13) ; 
	dbClass.sqlout (( char *)  tou.srt_zeit,SQLCHAR,6 ) ;
	dbClass.sqlout (( char *)  tou.dau,SQLCHAR,6 ) ;
	dbClass.sqlout (( long *) &tou.lng,SQLLONG,0) ;
	dbClass.sqlout (( char *)  tou.fah_1[13];
	dbClass.sqlout (( char *)  tou.fah_2[13];
	dbClass.sqlout (( short *)&tou.delstatus;
	dbClass.sqlout (( long *) &tou.lgr;
	dbClass.sqlout (( long *) &tou.leitw_typ;
	dbClass.sqlout (( long *) &tou.htou;
	dbClass.sqlout (( long *) &tou.adr;
	dbClass.sqlout (( short *)&tou.eigentour;
	dbClass.sqlout (( long *) &tou.lgkonto;

< ----- */

	readcursor = (short) dbClass.sqlcursor ("select "
	" tou_bz  "
	" from tou where tou = ?  " ) ;
	
 }


void ADR_CLASS::prepare (void)
{


	dbClass.sqlin ((long *) &adr.adr, SQLLONG, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = (short)dbClass.sqlcursor ("select count(*) from adr "
										"where adr.adr = ? ");
										
	test_upd_cursor = 1;

	dbClass.sqlin ((long *) &adr.adr, SQLLONG, 0);

	dbClass.sqlout ((long *) &adr.adr,SQLLONG, 0);
	dbClass.sqlout ((char *) adr.adr_krz,SQLCHAR, 17);
	dbClass.sqlout ((char *) adr.adr_nam1, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.adr_nam2, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.adr_nam3, SQLCHAR, 37);
	dbClass.sqlout ((short *) &adr.adr_typ, SQLSHORT, 0);
	dbClass.sqlout ((char *) adr.adr_verkt, SQLCHAR, 17);
	dbClass.sqlout ((short *) &adr.anr, SQLSHORT, 0);

	dbClass.sqlout ((short *) &adr.delstatus, SQLSHORT, 0);
	dbClass.sqlout ((char *) adr.fax, SQLCHAR, 21);
	dbClass.sqlout ((short *) &adr.fil, SQLSHORT,0);
// 130710	dbClass.sqlout ((char *) adr.geb_dat, SQLCHAR, 12);	// datentyp-mismatch ( oracle ist doof ...... )
	dbClass.sqlout ((short *) &adr.land, SQLSHORT, 0);
	dbClass.sqlout ((short *) &adr.mdn, SQLSHORT,  0);
	dbClass.sqlout ((char *) adr.merkm_1, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.merkm_2, SQLCHAR, 3);

	dbClass.sqlout ((char *) adr.merkm_3, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.merkm_4, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.merkm_5, SQLCHAR, 3);
	dbClass.sqlout ((char *) adr.modem, SQLCHAR, 21);
	dbClass.sqlout ((char *) adr.ort1, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.ort2, SQLCHAR, 37);
// korekte Variante
	dbClass.sqlout ((char *) adr.partner, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.pf, SQLCHAR, 17);
	dbClass.sqlout ((char *) adr.plz, SQLCHAR, 9);
	dbClass.sqlout ((short *) &adr.staat, SQLSHORT, 0);
	dbClass.sqlout ((char *) adr.str,SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.tel, SQLCHAR, 21);
	dbClass.sqlout ((char *) adr.telex, SQLCHAR, 21);
	dbClass.sqlout ((long *) &adr.txt_nr, SQLLONG, 0);
	dbClass.sqlout ((char *) adr.plz_postf, SQLCHAR,9);

	dbClass.sqlout ((char *) adr.plz_pf,SQLCHAR, 9);
	dbClass.sqlout ((char *) adr.iln, SQLCHAR, 33 );
	dbClass.sqlout ((char *) adr.email, SQLCHAR, 37);
	dbClass.sqlout ((char *) adr.swift, SQLCHAR, 25);
	dbClass.sqlout ((char *) adr.mobil, SQLCHAR, 21);	// 301111
	dbClass.sqlout ((char *) adr.iban, SQLCHAR, 25);	// 300813


		readcursor = (short) dbClass.sqlcursor ("select "

	" adr, adr_krz, adr_nam1, adr_nam2, adr_nam3,adr_typ, adr_verkt, anr, " 
	" delstatus, fax, fil, land, mdn, merkm_1, merkm_2, "
	" merkm_3, merkm_4, merkm_5, modem, ort1, ort2, "
	" partner, pf, plz, staat, str, tel, telex, txt_nr, plz_postf, "
	" plz_pf, iln, email, swift, mobil ,iban "
	" from adr where adr = ? " ) ;
	

/* ---> testbereich, das funktioniert dann jedenfalls schon mal
	dbClass.sqlin ((long *) &adr.adr, SQLLONG, 0);
	

// red.	01. dbClass.sqlout ((long *) &adr.adr,SQLLONG, 0);

	dbClass.sqlout ((char *) adr.adr_krz,SQLCHAR, 17);			// genutzt
	dbClass.sqlout ((char *) adr.adr_nam1, SQLCHAR, 37);		// genutzt
	dbClass.sqlout ((char *) adr.adr_nam2, SQLCHAR, 37);		// genutzt
	dbClass.sqlout ((char *) adr.adr_nam3, SQLCHAR, 37);		// genutzt
// red. 02.	dbClass.sqlout ((short *) &adr.adr_typ, SQLSHORT, 0);
// red. 01.	dbClass.sqlout ((char *) adr.adr_verkt, SQLCHAR, 17);

	dbClass.sqlout ((short *) &adr.anr, SQLSHORT, 0);			// genutzt
// red.	 01. dbClass.sqlout ((short *) &adr.delstatus, SQLSHORT, 0);
	dbClass.sqlout ((char *) adr.fax, SQLCHAR, 21);				// genutzt
// red. 01.	dbClass.sqlout ((short *) &adr.fil, SQLSHORT,0);
//	dbClass.sqlout ((char *) adr.geb_dat, SQLCHAR, 12);

// red. 02.	dbClass.sqlout ((short *) &adr.land, SQLSHORT, 0);
// red. 01.	dbClass.sqlout ((short *) &adr.mdn, SQLSHORT,  0);
// red. 02.	dbClass.sqlout ((char *) adr.merkm_1, SQLCHAR, 3);
// red. 02.	dbClass.sqlout ((char *) adr.merkm_2, SQLCHAR, 3);
// red. 02.	dbClass.sqlout ((char *) adr.merkm_3, SQLCHAR, 3);

// red. 02.	dbClass.sqlout ((char *) adr.merkm_4, SQLCHAR, 3);
// red. 02.	dbClass.sqlout ((char *) adr.merkm_5, SQLCHAR, 3);
// red. 02.	dbClass.sqlout ((char *) adr.modem, SQLCHAR, 21);
	dbClass.sqlout ((char *) adr.ort1, SQLCHAR, 37);		// genutzt
	dbClass.sqlout ((char *) adr.ort2, SQLCHAR, 37);		// genutzt

	dbClass.sqlout ((char *) adr.partner, SQLCHAR, 37);		// genutzt
	dbClass.sqlout ((char *) adr.pf, SQLCHAR, 17);			// genutzt
	dbClass.sqlout ((char *) adr.plz, SQLCHAR, 9);			// genutzt
	dbClass.sqlout ((short *) &adr.staat, SQLSHORT, 0);		// genutzt
	dbClass.sqlout ((char *) adr.str,SQLCHAR, 37);			// genutzt

	dbClass.sqlout ((char *) adr.tel, SQLCHAR, 21);			// genutzt
// red. 02.	dbClass.sqlout ((char *) adr.telex, SQLCHAR, 21);	
// red. 02.	dbClass.sqlout ((long *) &adr.txt_nr, SQLLONG, 0);
// tippfehler !!	dbClass.sqlout ((char *) adr.plz_postf, SQLLONG,9);
// red. 02.	dbClass.sqlout ((char *) adr.plz_postf, SQLCHAR ,9);
	dbClass.sqlout ((char *) adr.plz_pf,SQLCHAR, 9);		// genutzt

	dbClass.sqlout ((char *) adr.iln, SQLCHAR, 33 );		// genutzt
// red. 02.	dbClass.sqlout ((char *) adr.email, SQLCHAR, 37);
// red. 02.	dbClass.sqlout ((char *) adr.swift, SQLCHAR, 25);


            readcursor = (short) dbClass.sqlcursor ("select "

// red 01.	" adr "
// red01.	" , adr_krz, adr_nam1, adr_nam2, adr_nam3, adr_typ, adr_verkt "
	" adr_krz, adr_nam1, adr_nam2, adr_nam3 "
// red. 01.	" , anr , delstatus, fax, fil, geb_dat "
	" , anr , fax "
// red. 01.	" , land, mdn, merkm_1, merkm_2,  merkm_3 "
// red.02.	" , merkm_4, merkm_5, modem, ort1, ort2 " 
	" , ort1, ort2 " 
	" , partner, pf, plz, staat, str "
// red. 02.	" , tel, telex, txt_nr, plz_postf, plz_pf "
	" , tel, plz_pf "
	" , iln, email, swift " 
	" from adr where adr = ? " ) ;
ende testbereich < ------- */

}

