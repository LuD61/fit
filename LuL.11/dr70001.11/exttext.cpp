#include "stdafx.h"

#ifndef OLABEL
#include "DbClass.h"
#endif

#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif


#ifdef FITODBC
#include "MainFrm.h"
#include "catsets.h"
#include "llsmpdoc.h"
#include "llsmpvw.h"
#endif

#include "temptab.h"
#include "systables.h"
#include "syscolumns.h"
#include "form_ow.h"
#include "txt.h"
#include "range.h"
#include "item.h"
#include "exttext.h"

extern DB_CLASS dbClass;

SYSTABLES_CLASS Systables_class ;
SYSCOLUMNS_CLASS Syscolumns_class ;
FORM_T_CLASS Form_t ;
FORM_TE_CLASS Form_te ;

extern short GlobalLila ;

// extern char myform_nr[20] ;

#ifdef FITODBC
extern  CLlSampleDoc* pDoc ;	// am 150509 : extern dazu
#endif 


struct EXT_TEXT ext_text,  ext_text_null;

extern FORM_T form_t, form_t_null ;

extern DB_CLASS dbClass;

char klausel[2500] ;	// evtl. noch groesser notwendig ?!

char	et_tabnam [MAXETEXTE][19] ;	// tabname
char	et_feldnam[MAXETEXTE][19] ;	// feldname
int		et_cursors[MAXETEXTE] ;		// preparierte Cursoren Schluesselsuche
int		et_cursorl[MAXETEXTE] ;		// preparierte Cursoren Textleserei

int		et_typ[MAXETEXTE] ;			// 071110 
char	et_ttabnam [MAXETEXTE][19] ;	// 071110 : extern-tabname
char	et_tfeldnam[MAXETEXTE][5][19] ;	// 071110 : extern-feldname


// Die Dimension 15 ergibt sich vorerst aus folgender Ueberlegung 
// 5 select's				(0....4)
// 5 order's (im select)	(5....9)
// 5 where's				(10..14)

int    et_datetyp[MAXETEXTE][15];	// Datentyp in der Daba
int    et_datelen[MAXETEXTE][15];	// Datelength in der Daba
void * et_sqlval [MAXETEXTE][15];	// pointer auf Feldinhalte(string,dates) 
long    et_sqllong[MAXETEXTE][15];	// Matrix fuer longs und shorts 
double  et_sqldouble[MAXETEXTE][15];	// Matrix fuer double 

int et_pwfeld[MAXETEXTE][5] ;	// MAXETEXTE Pointers ins Array je wfeld 
// es seien mal max. 10 substitue je whereklausel erlaubt 
int et_wpoint[MAXETEXTE][5][10] ;	// Where-Positionen im Statement


int EXT_TEXT_CLASS::Leseext_text (	)
{

      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

void EXT_TEXT_CLASS::satzinit( int tint )
{


	if ( et_typ[tint] == 1 )	// nur Felder-Modus notwendig
	{

		for ( int numfeld = 0 ; numfeld < 5 ; numfeld ++ )
		{
			if ( strlen(clipped(et_tfeldnam[tint][numfeld])) == 0 )
				continue ;

			switch ( et_datetyp[tint][numfeld] )
			{
			case(iDBCHAR):
			case(iDBVARCHAR):	// 280612
//				sprintf ( (char*)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				sprintf ( (char*)et_sqlval[tint][numfeld] ,"" ) ;
				break ;
			case(iDBSMALLINT):
				et_sqllong[tint][numfeld] = 0 ;
				break ;

			case(iDBSERIAL):
			case(iDBINTEGER):
				et_sqllong[tint][numfeld] = 0 ;
				break ;
			case(iDBDECIMAL):
				et_sqldouble[tint][numfeld] = 0.0 ;
				break ;

			case (iDBDATETIME) :
//				memcpy ( tt_sqlval[i] ,savtt_sqlval[i], sizeof ( TIMESTAMP_STRUCT)) ;
				sprintf ( (char *)et_sqlval[tint][numfeld]," ")  ;
				break ;		

			case(iDBDATUM):
//				sprintf ( (char*)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				sprintf ( (char *)et_sqlval[tint][numfeld]," ")  ;
				break ;

			default :	// schlechte Notbremse
				sprintf ( (char*)et_sqlval[tint][numfeld] ,"") ;
				break ;
			} ;
		}		// while
	}

}

int EXT_TEXT_CLASS::Openext_text (int tint)
{
	int retcode ;
	// Zuerst aktuellen Such-Cursor suchen
	readcursor = (short)et_cursors[tint];
	if ( readcursor < 0 )
		return -1 ;
	// dann aktuellen Such-Cursor oeffnen
	satzinit(tint) ;	// 071110
	retcode = dbClass.sqlopen (readcursor);
	if ( retcode )
		 return retcode ;
	// dann aktuellen Such-Cursor lesen
	 retcode = Leseext_text() ;

			// 071110 : if et_typ == 1 : bereits alles gelesen  .....
	 if ( retcode  || et_typ[tint] == 1 )
		 return retcode ;
	// aktuellen Lese-Cursor suchen
	readcursor = (short)et_cursorl[tint];
	if ( readcursor < 0 )
		return -1 ;
	// dann aktuellen Lese-Cursor oeffnen
    retcode = dbClass.sqlopen (readcursor);
	return retcode ;
}

void EXT_TEXT_CLASS::setzedetails (int platz ,int iint, int tint)
{
	// platz : welches feld
	// iint : global-Pointer
	// tint : Pointer in den Texten

			// vorerst keinerlei Fehlerbehandlung 
			// 5 select's				(0....4) Nur diese werden hier rein geschickt ....
			// 5 order's (im select)	(5....9)
			// 5 where's				(10..14)



//			et_datetyp[tint][platz]= syscolumns.coltype ;	// Datentyp in der Daba
//			et_datelen[tint][platz] = syscolumns.collength;	// Datelength in der Daba
	switch ( et_datetyp[tint][platz] )
	{
		case(iDBCHAR):
		case(iDBVARCHAR):	// 280612
			dbClass.sqlin ((char *)  et_sqlval[tint][platz], SQLCHAR , et_datelen[tint][platz] + 1 );
		break ;
		case(iDBSMALLINT):	// smallint
			dbClass.sqlin ((long *)  &et_sqllong[tint][platz], SQLLONG, 0);
		break ;

		case(iDBSERIAL):	// 201209
		case(iDBINTEGER):	// integer
			dbClass.sqlin ((long *)  &et_sqllong[tint][platz], SQLLONG, 0);
		break ;
		case(iDBDECIMAL):	// decimal
			dbClass.sqlin ((double *)  &et_sqldouble[tint][platz], SQLDOUBLE, 0);
		break ;
		case(iDBDATUM):	// Datumsfeld
			dbClass.sqlin ((char *)  et_sqlval[tint][platz], SQLCHAR , 11 );
		break ;
		case(iDBDATETIME):
			dbClass.sqlout (et_sqlval[tint][platz], SQLTIMESTAMP , 26 );
		break ;

		default :
		break ;
	}
}

void EXT_TEXT_CLASS::holedetails (int platz ,int iint, int tint)
{
	// platz : welches feld
	// iint : global-Pointer
	// tint : Pointer in den Texten

// HIER FEHLT IRGENDWIE NOCH rewinden des Documentes #ifdef FITODBC 
// ASSERT valid(pDoc )

		if ( ! strcmp(clipped (systables.tabname), clipped (form_t.ttab_nam ))
			&& ((strlen( clipped( systables.tabname)))
			      == (strlen( clipped(form_t.ttab_nam)))) )
		{	// evtl. passt ja alles schon ?
			;
		}
		else
		{
			sprintf ( systables.tabname, "%s", form_t.ttab_nam );
#ifndef FITODBC 
			Systables_class.opensystables ();
			if ( ! Systables_class.lesesystables ())	// Test,ob es die Tabelle gibt 
			{
#endif
#ifdef FITODBC
			if ( TRUE )
			{
#endif
			}
			else
			{
				MessageBox(NULL,"Error bei ext-Texten", "", MB_OK|MB_ICONEXCLAMATION);
			}
		}
			// vorerst keinerlei Fehlerbehandlung 
			// 5 select's				(0....4)
			// 5 order's (im select)	(5....9)
			// 5 where's				(10..14)

		switch ( platz)
		{
		case 0 : sprintf ( syscolumns.colname ,"%s", form_t.sfeld1) ; break ;
		case 1 : sprintf ( syscolumns.colname ,"%s", form_t.sfeld2) ; break ;
		case 2 : sprintf ( syscolumns.colname ,"%s", form_t.sfeld3) ; break ;
		case 3 : sprintf ( syscolumns.colname ,"%s", form_t.sfeld4) ; break ;
		case 4 : sprintf ( syscolumns.colname ,"%s", form_t.sfeld5) ; break ;
		case 5 : sprintf ( syscolumns.colname ,"%s", form_t.ofeld1) ; break ;
		case 6 : sprintf ( syscolumns.colname ,"%s", form_t.ofeld2) ; break ;
		case 7 : sprintf ( syscolumns.colname ,"%s", form_t.ofeld3) ; break ;
		case 8 : sprintf ( syscolumns.colname ,"%s", form_t.ofeld4) ; break ;
		case 9 : sprintf ( syscolumns.colname ,"%s", form_t.ofeld5) ; break ;
		}
		syscolumns.tabid = systables.tabid ;
#ifndef FITODBC
		Syscolumns_class.opensyscolumns ();
		if (! Syscolumns_class.lesesyscolumns()) 
		{
#endif
#ifdef FITODBC
		 pDoc->FetchColumnInfo(clipped(form_t.ttab_nam)
		                         ,clipped(syscolumns.colname) ) ;
		pDoc->m_pColumnset->MoveFirst();
		if (!pDoc->m_pColumnset->IsEOF())
		{
					// gefunden -> einigermassen unkrititsche Zuordnung

			syscolumns.coltype = (short)pDoc->m_pColumnset->m_nDataType ;
			syscolumns.collength = (short)pDoc->m_pColumnset->m_nLength ;
			switch (syscolumns.coltype)
			{
				case SQL_UNKNOWN_TYPE :
				case SQL_NUMERIC :
				case SQL_CHAR :
				case iDBVARCHAR :	// 280612 : bissel doof

						syscolumns.coltype = iDBCHAR ;
					break ;

				case iDBSERIAL :		// 201209 zum lesen als integer behandeln 
				case SQL_INTEGER :
						syscolumns.coltype = iDBINTEGER ;
					break ;

				case SQL_SMALLINT : 
						syscolumns.coltype = iDBSMALLINT ;
					break ;

				case SQL_DECIMAL :
				case SQL_FLOAT :
				case SQL_REAL :
				case SQL_DOUBLE :
						syscolumns.coltype = iDBDECIMAL ;
					break ;

				case SQL_DATE :
						syscolumns.coltype = iDBDATUM ;
					break ;

				case SQL_TIMESTAMP :
				case SQL_TIME :			// 140214

						syscolumns.coltype = iDBDATETIME ;
					break ;
			}
		}
		else
		{	// negativer Notnagel 
				syscolumns.coltype = iDBCHAR ;
				syscolumns. collength = 100L ;
		}
		if (TRUE)	// ANNAHME : ES WURDE korrekt gearbeitet 
		{
#endif

	//	Matritzen  laden ;


			et_datetyp[tint][platz]= syscolumns.coltype ;	// Datentyp in der Daba
			et_datelen[tint][platz] = syscolumns.collength;	// Datelength in der Daba
	}
	switch ( syscolumns.coltype )
	{
		case(iDBCHAR):
		case(iDBVARCHAR):	// 280612
			if (et_sqlval[tint][platz] != (char *) 0)
			{
				free ( et_sqlval[tint][platz]) ;
				et_sqlval[tint][platz] = (char *) 0;
			}
			if (et_sqlval[tint][platz] == (char *) 0) et_sqlval[tint][platz] = (char *) malloc (et_datelen[tint][platz] + 2);

			sprintf ( (char*) et_sqlval[tint][platz] ,"" ) ;

			dbClass.sqlout ((char *)  et_sqlval[tint][platz], SQLCHAR , et_datelen[tint][platz] + 1 );
		break ;
		case(iDBSMALLINT):	// smallint
			dbClass.sqlout ((long *)  &et_sqllong[tint][platz], SQLLONG, 0);
		break ;

		case(iDBSERIAL):	// 201209
		case(iDBINTEGER):	// integer
			dbClass.sqlout ((long *)  &et_sqllong[tint][platz], SQLLONG, 0);
		break ;
		case(iDBDECIMAL):	// decimal
			dbClass.sqlout ((double *)  &et_sqldouble[tint][platz], SQLDOUBLE, 0);
		break ;
		case(iDBDATUM):	// Datumsfeld
			if (et_sqlval[tint][platz] != (char *) 0)
			{
				free ( et_sqlval[tint][platz]) ;
				et_sqlval[tint][platz] = (char *) 0;
			}
			if (et_sqlval[tint][platz] == (char *) 0) et_sqlval[tint][platz] = (char *) malloc (12);
			sprintf ( (char*) et_sqlval[tint][platz] ,"" ) ; 
			dbClass.sqlout ((char *)  et_sqlval[tint][platz], SQLCHAR , 11 );
		break ;
		case(iDBDATETIME):

			if (et_sqlval[tint][platz] != NULL )
			{
				free ( et_sqlval[tint][platz]) ;
				et_sqlval[tint][platz] = NULL ;
			}
			if (et_sqlval[tint][platz] == NULL) 
				 et_sqlval[tint][platz] =  malloc (sizeof(TIMESTAMP_STRUCT));
			sprintf ( (char*) et_sqlval[tint][platz] ,"" ) ;
//			dbClass.sqlout (et_sqlval[tint][platz], SQLTIMESTAMP , 0 );
			dbClass.sqlout (et_sqlval[tint][platz], SQLTIMESTAMP , 26 );	// 140214
		break ;

		default :
		break ;

	}

}


void EXT_TEXT_CLASS::holewdetails (int tint, int platz, int pos ,char * tabname, char * feldname)
{
	int aktupoint,itablen,ifeldlen ;
	char itabnam[19] ;
	char ifeldnam[19] ;

	// tint : Pointer in den Texten
	// platz : welches feld
	// pos : pointer im where

	// Problem bleibt zunaechst Datentyp-Konsistenz

	// Zuerst : Suche Referenz
	sprintf ( itabnam , "%s" , clipped( tabname)) ;
	sprintf ( ifeldnam, "%s" , clipped(feldname)) ;
	itablen  = (int) strlen( itabnam) ;
	ifeldlen = (int) strlen(ifeldnam) ;

	for ( aktupoint = 0 ; aktupoint < 249 ; aktupoint ++ )
	{
		if (!(strncmp((clipped(tt_tabnam[aktupoint])),itabnam, itablen )))
		{
			if (!(strncmp((clipped(tt_feldnam[aktupoint])),ifeldnam, ifeldlen )))
			{
				if ( strlen(clipped(tt_tabnam[aktupoint])) == itablen 
						  && strlen(clipped(tt_feldnam[aktupoint])) == ifeldlen )
				{	// wohl erfolgreich gefunden ?!
// 5 where's				(10..14)
					et_datetyp[tint][platz] = tt_datetyp[aktupoint] ;	// Datentyp in der Daba
					et_datelen[tint][platz] = tt_datelen[aktupoint] ;	// Datelength in der Daba
//					void * et_sqlval [tint][platz]= ??? ;	// pointer auf Feldinhalte(string,dates) 
//					long   et_sqllong[tint][platz]= ??? ;	// Matrix fuer longs und shorts 
//					double  et_sqldouble[tint][platz = ??? ];	// Matrix fuer double 

					et_pwfeld[tint][platz] = aktupoint ;	// Pointer ins Array je wfeld 
					et_wpoint[tint][platz - 10][pos] ;		// Beachte : offset "-10" Where-Positionen im Statement

					switch ( tt_datetyp[aktupoint] )
					{
						case(iDBCHAR):
						case(iDBVARCHAR):	// 280612
						
							dbClass.sqlin ((char *)  tt_sqlval[aktupoint], SQLCHAR ,
							tt_datelen[aktupoint] + 2 );
						  break ;
						case(iDBSMALLINT):	// smallint-> short-Problemchen !!!!!
							dbClass.sqlin ((long *) &tt_sqllong[aktupoint], SQLLONG ,0);
						  break ;

					    case(iDBSERIAL):	// 201209
						case(iDBINTEGER):	// integer
							dbClass.sqlin ((long *) &tt_sqllong[aktupoint], SQLLONG ,0);
						  break ;
						case(iDBDECIMAL):	// decimal
						    dbClass.sqlin ((double *)  &tt_sqldouble[aktupoint], SQLDOUBLE ,0);
						  break ;

						case(iDBDATUM):	// Datumsfeld
							dbClass.sqlin ((char *)  tt_sqlval[aktupoint], SQLCHAR ,12);
						  break ;
						case (iDBDATETIME) :	
							dbClass.sqlin ( ( TIMESTAMP_STRUCT *) &tt_sqlval[aktupoint], SQLTIMESTAMP,
									sizeof(TIMESTAMP_STRUCT));
						  break ;
					}
					break ;	// fertig und weg 
				}			// laengen identisch
			}				// feldnamen identisch
		}					// tabnamen identisch
	}			// for-schleife
	if ( aktupoint > 248 )
	{
		char Errormeldung[50] ;
		sprintf ( Errormeldung, "Ext-Feld fehlt %s.%s ", itabnam, ifeldnam );
		MessageBox(NULL, Errormeldung , "", MB_OK|MB_ICONEXCLAMATION);
	}
}


char * EXT_TEXT_CLASS::machewhere (char * myform_nr, int iint,int tint, char * klausel)
{
int i,j,k ;
// iint :  Global-Struktur( und Name )
// tint : der wievielte texte

	sprintf ( form_te.tab_nam ,"%s", clipped(tt_tabnam[iint])) ;
    sprintf ( form_te.feld_nam ,"%s", clipped(tt_feldnam[iint])) ;
	form_te.lila = GlobalLila ;
	sprintf ( form_te.form_nr, "%s" , myform_nr );
	k = Form_te.openform_te() ;
	klausel[0] = '\0' ;
	if (!k) 
		k = Form_te.leseform_te () ;
	if ( k )
	{
		return 0 ;	// Error-Return
	}
//	sprintf ( klausel,"%s", clipped ( form_te.krz_txt )) ;
	sprintf ( klausel,"%s",  form_te.krz_txt ) ;	// 010507
	while ( ! Form_te.leseform_te() )
	{
		k = (int) strlen ( klausel) ;
//		sprintf ( klausel + k , "%s",clipped ( form_te.krz_txt ));
		sprintf ( klausel + k , "%s", form_te.krz_txt );	// 010507
	}

	i = 0 ;	// maximal 10 substitute sind erlaubt

	k = (int) strlen ( klausel );

	for ( j= 0 ; (j < k)  && (i < 10) ; j++ )
	{
		if ( klausel[j] == '$')
		{
			if ( klausel[j+1 ] == 'w')
			{
				switch ( klausel[j+2] )
				{
				case '1' :
					holewdetails ( tint,10,i, form_t.wtab1, form_t.wfeld1 ) ;
					break ;
				case '2' :
					holewdetails ( tint,11,i, form_t.wtab2, form_t.wfeld2 ) ;
					break ;
				case '3' :
					holewdetails ( tint,12,i, form_t.wtab3, form_t.wfeld3 ) ;
					break ;
				case '4' :
					holewdetails ( tint,13,i, form_t.wtab4, form_t.wfeld4 ) ;
					break ;
				case '5' :
					holewdetails ( tint,14,i, form_t.wtab5, form_t.wfeld5 ) ;
					break ;
				}
				i++ ;
				klausel[j] = ' ' ;
				j++ ;
				klausel[j] = '?' ;
				j++ ;
				klausel[j] = ' ' ;

			}
		}
	}

	return klausel ;
}

int EXT_TEXT_CLASS::Prepare (char * myform_nr, int iint ,int tint)
{
	// iint : Pointer auf Gesamtstruktur
	// tint : Pointer auf et_strukturen 

	char qstatement[2500] ;
	char zstatement[2500] ;
	char anhang[20] ;
	int k,komma ;

	sprintf (et_tabnam[tint] ,"%s", clipped ( tt_tabnam[iint])); 
	sprintf (et_feldnam[tint] ,"%s", clipped ( tt_feldnam[iint])); 
	// zuerst fertig initialisieren
	for (k=0 ; k < 5 ; k++)
	{
		et_pwfeld[tint][k] = -1 ;
		for ( komma = 0 ; komma < 10 ; komma++)
		{
			et_wpoint[tint][k][komma] = -1 ;
		}
	}

	sprintf ( form_t.tab_nam ,"%s", clipped(tt_tabnam[iint])) ;
    sprintf ( form_t.feld_nam ,"%s", clipped(tt_feldnam[iint])) ;
	form_t.lila = GlobalLila ;
	sprintf ( form_t.form_nr, "%s" , myform_nr );
	k = Form_t.openform_t() ;
	if (!k) 
		k = Form_t.leseform_t () ;
	if ( k ) return 0 ;	// Error-Return

	machewhere(myform_nr,iint,tint, klausel) ;

	// 071110 A
	sprintf ( et_ttabnam[tint] ,"%s" , clipped ( form_t.ttab_nam )) ;

	sprintf ( et_tfeldnam[tint][0] ,"%s" , clipped ( form_t.sfeld1 )) ;
	sprintf ( et_tfeldnam[tint][1] ,"%s" , clipped ( form_t.sfeld2 )) ;
	sprintf ( et_tfeldnam[tint][2] ,"%s" , clipped ( form_t.sfeld3 )) ;
	sprintf ( et_tfeldnam[tint][3] ,"%s" , clipped ( form_t.sfeld4 )) ;
	sprintf ( et_tfeldnam[tint][4] ,"%s" , clipped ( form_t.sfeld5 )) ;

// Zusammenschieben
	if ( strlen ( clipped ( et_tfeldnam[tint][0]) ) == 0 &&
		 strlen ( clipped ( et_tfeldnam[tint][1]) ) == 0 &&
		 strlen ( clipped ( et_tfeldnam[tint][2]) ) == 0 &&
		 strlen ( clipped ( et_tfeldnam[tint][3]) ) == 0 &&
		 strlen ( clipped ( et_tfeldnam[tint][4] ) ) == 0 )
		 return (0) ;	// Notbremse gegen deadlock, clipped gleichzeitig die Felder vor

/* nicht machen ....
	while ( strlen (et_tfeldnam[tint][0]) == 0 )
	{
		if ( strlen (et_tfeldnam[tint][1]) > 0 || strlen (et_tfeldnam[tint][2]) > 0 
		  || strlen (et_tfeldnam[tint][3]) > 0 || strlen (et_tfeldnam[tint][4]) > 0 )
		{
			sprintf ( et_tfeldnam[tint][0] , "%s" , et_tfeldnam[tint][1] ) ;
			sprintf ( et_tfeldnam[tint][1] , "%s" , et_tfeldnam[tint][2] ) ;
			sprintf ( et_tfeldnam[tint][2] , "%s" , et_tfeldnam[tint][3] ) ;
			sprintf ( et_tfeldnam[tint][3] , "%s" , et_tfeldnam[tint][4] ) ;
			sprintf ( et_tfeldnam[tint][4] , "" ) ;
		}
		else
			break ;
	}
	while ( strlen (et_tfeldnam[tint][1]) == 0 )
	{
		if ( strlen (et_tfeldnam[tint][2]) > 0 || strlen (et_tfeldnam[tint][3]) > 0 
		  || strlen (et_tfeldnam[tint][4]) > 0 )
		{
			sprintf ( et_tfeldnam[tint][1] , "%s" , et_tfeldnam[tint][2] ) ;
			sprintf ( et_tfeldnam[tint][2] , "%s" , et_tfeldnam[tint][3] ) ;
			sprintf ( et_tfeldnam[tint][3] , "%s" , et_tfeldnam[tint][4] ) ;
			sprintf ( et_tfeldnam[tint][4] , "" ) ;
		}
		else
			break ;
	}

	while ( strlen (et_tfeldnam[tint][2]) == 0 )
	{
		if ( strlen (et_tfeldnam[tint][3]) > 0 || strlen (et_tfeldnam[tint][4]) > 0 ) 
		{
			sprintf ( et_tfeldnam[tint][2] , "%s" , et_tfeldnam[tint][3] ) ;
			sprintf ( et_tfeldnam[tint][3] , "%s" , et_tfeldnam[tint][4] ) ;
			sprintf ( et_tfeldnam[tint][4] , "" ) ;
		}
		else
			break ;
	}

	while ( strlen (et_tfeldnam[tint][3]) == 0 )
	{
		if ( strlen (et_tfeldnam[tint][4]) > 0 ) 
		{
			sprintf ( et_tfeldnam[tint][3] , "%s" , et_tfeldnam[tint][4] ) ;
			sprintf ( et_tfeldnam[tint][4] , "" ) ;
		}
		else
			break ;
	}
< ------ */

	// 071110 E
	sprintf ( qstatement, "select " ) ;
	komma = 0 ;
	if ( (strlen ( clipped (form_t.sfeld1 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s , %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld1));
		else
			sprintf ( zstatement , "%s %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld1));

		holedetails ( 0 ,iint, tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped (form_t.sfeld2 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s , %s.%s " , qstatement,clipped ( form_t.ttab_nam ), clipped(form_t.sfeld2));
		else
			sprintf ( zstatement , "%s %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld2));

		holedetails ( 1 ,iint,tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped ( form_t.sfeld3 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s , %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld3));
		else
			sprintf ( zstatement , "%s %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld3));

		holedetails ( 2 ,iint,tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped ( form_t.sfeld4 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s , %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld4));
		else
			sprintf ( zstatement , "%s %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld4));

		holedetails ( 3 ,iint,tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped (form_t.sfeld5 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s , %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld5));
		else
			sprintf ( zstatement , "%s %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld5));

		holedetails ( 4 ,iint, tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}

// order-felder einfach noch dazu 
	if ( (strlen ( clipped (form_t.ofeld1 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s , %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld1));
		else
			sprintf ( zstatement , "%s %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld1));

		holedetails ( 5 ,iint,tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped ( form_t.ofeld2 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s , %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld2));
		else
			sprintf ( zstatement , "%s %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld2));

		holedetails ( 6 ,iint, tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped ( form_t.ofeld3 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s , %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld3));
		else
			sprintf ( zstatement , "%s %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld3));

		holedetails ( 7 ,iint ,tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped ( form_t.ofeld4 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s , %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld4));
		else
			sprintf ( zstatement , "%s %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld4));

		holedetails ( 8 ,iint ,tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped ( form_t.ofeld5 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s , %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld5));
		else
			sprintf ( zstatement , "%s %s.%s " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld5));

		holedetails ( 9 ,iint, tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}

// Hier muss die where-Klausel mit erzeugt werden

	sprintf ( zstatement , "%s from %s where %s", qstatement, clipped ( form_t.ttab_nam ),  klausel );

// ab hier gehts mit order weiter
	sprintf ( qstatement , "%s", zstatement );

	komma = 0 ;
	if ( (strlen ( clipped ( form_t.ofeld1 ))) > 0 )
	{
		if ( form_t.odesc1 ) sprintf ( anhang, " DESC "); else anhang[0] = '\0' ;
		if (komma)
			sprintf ( zstatement , "%s , %s.%s%s" , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld1),anhang);
		else
			sprintf ( zstatement , "%s ORDER BY %s.%s%s" , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld1),anhang);

		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped ( form_t.ofeld2 ))) > 0 )
	{
		if ( form_t.odesc2 ) sprintf ( anhang, " DESC "); else anhang[0] = '\0' ;
		if (komma)
			sprintf ( zstatement , "%s , %s.%s%s" , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld2),anhang);
		else
			sprintf ( zstatement , "%s ORDER BY %s.%s%s" , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld2),anhang);

		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped ( form_t.ofeld3 ))) > 0 )
	{

		if ( form_t.odesc3 ) sprintf ( anhang, " DESC "); else anhang[0] = '\0' ;
		if (komma)
			sprintf ( zstatement , "%s , %s.%s%s" , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld3),anhang);
		else
			sprintf ( zstatement , "%s ORDER BY %s.%s%s" , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld3),anhang);

		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	if ( (strlen ( clipped ( form_t.ofeld4 ))) > 0 )
	{
		if ( form_t.odesc4 ) sprintf ( anhang, " DESC "); else anhang[0] = '\0' ;
		if (komma)
			sprintf ( zstatement , "%s , %s.%s%s" , clipped ( form_t.ttab_nam ), clipped(form_t.ofeld4),anhang);
		else
			sprintf ( zstatement , "%s ORDER BY %s.%s%s" , clipped ( form_t.ttab_nam ), clipped(form_t.ofeld4),anhang);

		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}

/* ---> 071110 : Umwidmung des 5. Felderpaares : odesc = Text ODER Felder
	if ( (strlen ( clipped ( form_t.ofeld5 ))) > 0 )
	{
		if ( form_t.odesc5 ) sprintf ( anhang, " DESC "); else anhang[0] = '\0' ;
		if (komma)
			sprintf ( zstatement , "%s , %s.%s%s" , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld5),anhang);
		else
			sprintf ( zstatement , "%s ORDER BY %s.%s%s" , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.ofeld5),anhang);
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
< ------ */


// 260907 : falls Testmode aktiv, dann das statement auf Platte ablegen 
	char * penvchar = getenv ("testmode");
	if ( penvchar == NULL )	penvchar = getenv ( "TESTMODE" ) ;
	if ( penvchar == NULL )
	{}
	else
	{ int testwert = atoi ( penvchar) ;
	  if ( testwert > 0 )
	  {
		  char dateiname[ 256 ] ;
		  penvchar = getenv ( "TMPPATH" ) ;
		  if ( penvchar == NULL )
			  sprintf ( dateiname , "drstatement.sql" ) ;
		  else
			  sprintf ( dateiname ,"%s\\drstatement.sql", penvchar) ;

		FILE * fp_struct ;
		fp_struct = fopen ( dateiname, "ab" ) ;
			if ( fp_struct == NULL )
			{
				// es hat halt NICHT funktioniert
			}

			int nwritten= (int) fwrite(qstatement, strlen(qstatement), 1, fp_struct );
			fclose ( fp_struct ); 
	  }
	}
// 260907 : falls Testmode aktiv, dann das statement auf Platte ablegen 

	int ireadcursor = dbClass.sqlcursor ( qstatement );
	et_cursors [tint] = ireadcursor ;
	if ( form_t.odesc5 == 1 )	// 071110
	{
		et_typ[tint] = 1 ;	// dann gibbet keinen Textlesecursor
		return 0 ; 
	}

// es folgt der schreibcursor fuer ext_txt

	dbClass.sqlout ((long *)  &ext_text.zei, SQLLONG, 0);
	dbClass.sqlout ((char *)   ext_text.txt, SQLCHAR, 260);

	sprintf ( qstatement ," select zei ,txt from %s where ", clipped (form_t.ttab_nam)) ;
	komma = 0 ;

	if ( (strlen ( clipped (form_t.sfeld1 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s and %s.%s = ? " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld1));
		else
			sprintf ( zstatement , "%s  %s.%s = ? " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld1));
		setzedetails ( 0 ,iint, tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}

	if ( (strlen ( clipped (form_t.sfeld2 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s and %s.%s = ? " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld2));
		else
			sprintf ( zstatement , "%s  %s.%s = ? " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld2));
		setzedetails ( 1 ,iint, tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}

	if ( (strlen ( clipped (form_t.sfeld3 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s and %s.%s = ? " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld3));
		else
			sprintf ( zstatement , "%s  %s.%s = ? " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld3));
		setzedetails ( 2 ,iint, tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}

	if ( (strlen ( clipped (form_t.sfeld4 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s and %s.%s = ? " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld4));
		else
			sprintf ( zstatement , "%s  %s.%s = ? " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld4));
		setzedetails ( 3 ,iint, tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}

	if ( (strlen ( clipped (form_t.sfeld5 ))) > 0 )
	{
		if (komma)
			sprintf ( zstatement , "%s and %s.%s = ? " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld5));
		else
			sprintf ( zstatement , "%s  %s.%s = ? " , qstatement, clipped ( form_t.ttab_nam ), clipped(form_t.sfeld5));

		setzedetails ( 4 ,iint, tint ) ;
		komma = 1 ;
		sprintf ( qstatement, "%s", zstatement );
	}
	sprintf ( zstatement , " %s order by zei ", qstatement ) ;

	ireadcursor = dbClass.sqlcursor ( zstatement );

	et_cursorl [tint] = ireadcursor ;
	return 0 ;
}


