#include "stdafx.h"
#include "DbClass.h"
#include "ptabn.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

#include "MainFrm.h"
struct PTABN ptabn,  ptabn_null;
extern DB_CLASS dbClass;

// int itxt_nr ;

static int anzzfelder ;

int PTABN_CLASS::dbcount (void)
/**
Tabelle ptabn lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}




int PTABN_CLASS::leseptabn (							)
{

      int di = dbClass.sqlfetch (readcursor);

	  return di;
}


int PTABN_CLASS::openptabn (void)
{

		if ( readcursor < 0 ) prepare ();
		sprintf ( ptabn.hilfe, "i" ) ;
		if ( odbdabatyp == FITORACLE )
		{
// 290610			likeoracle ( TAFELNAME , ptabn.ptitem ) ;
			uPPER ( ptabn.ptitem ) ;	// 290610

		}

         return dbClass.sqlopen (readcursor);
}


void PTABN_CLASS::prepare (void)
{

 	dbClass.sqlin ((char *) ptabn.ptwert, SQLCHAR, 4);
	dbClass.sqlin ((char *) ptabn.ptitem, SQLCHAR, 19);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);

    count_cursor = (short) dbClass.sqlcursor ("select count(*) from ptabn "
										"where ptabn.ptwert = ? and ptabn.ptitem = ? ");
										
	test_upd_cursor = 1;


	dbClass.sqlin ((char *) ptabn.ptwert, SQLCHAR, 4);
	dbClass.sqlin ((char *) ptabn.ptitem, SQLCHAR, 19);

if ( odbdabatyp == FITORACLE )
{
// 290610	dbClass.sqlin ((char *) ptabn.hilfe, SQLCHAR, 2);	// // caseinsensitive ist selber wieder sensitive .......
}
	dbClass.sqlout ((long *) &ptabn.ptlfnr, SQLLONG, 0);
	dbClass.sqlout ((char *) ptabn.ptbez,  SQLCHAR,33);
	dbClass.sqlout ((char *) ptabn.ptbezk, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer1, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer2, SQLCHAR, 9);
	
			
if ( odbdabatyp == FITORACLE )
	readcursor = (short) dbClass.sqlcursor ("select "
	" ptlfnr, ptbez, ptbezk, ptwer1, ptwer2  "
// 290610	" from ptabn where ptwert = ? and REGEXP_LIKE ( ptitem , ? , ? ) " ) ;
	" from ptabn where ptwert = ? and Upper ( ptitem ) = ? " ) ;
else
	readcursor = (short) dbClass.sqlcursor ("select "
	" ptlfnr, ptbez, ptbezk, ptwer1, ptwer2  "
	" from ptabn where ptwert = ? and ptitem = ? " ) ;

	
  }
