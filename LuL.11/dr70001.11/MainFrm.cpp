//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
// Diese Applikation soll irgendwann die Listen ausgeben ....
// Erstellung : 01.02.2004  GrJ

//=============================================================================

// mainfrm.cpp : implementation of the CMainFrame class

#include "stdafx.h"
#include "llmfc.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif
#ifdef FITODBC
#include "catsets.h"
#include "llsmpdoc.h"
#include "llsmpvw.h"
#endif 

#include "PropertieTable.h"	// 220610

#include "mainfrm.h"
// #include "wmask.h"
// #include "strfkt.h"
// #include "form_feld.h"
#ifndef OLABEL
#include "DbClass.h"
#endif	// OLABEL

#include "txt.h"

#ifndef OLABEL
#include "temptab.h" 
#include "adr.h"
#include "ptabn.h"
#include "systables.h"
#include "syscolumns.h"
#include "exttext.h"
#include "syspar.h"
#include "form_ow.h"	// 071110

extern struct FORM_T form_t,  form_t_null;	// 071110
extern struct FORM_V form_v,  form_v_null;	// 201213
extern 	FORM_V_CLASS Form_v_class ;

#ifdef FITODBC
extern CLlSampleDoc* pDoc ;
#endif

#endif


#include "sys/timeb.h"
#include "Token.h"

// #include "cmbtl9.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GR: Waehlen Sie hier die List & Label Sprache f�r den Designer und die
//     Dialoge aus :
//    (Die entsprechenden Sprachkonstanten entnehmen Sie der Datei cmbtl9.h)

// US: choose your LuL-Language for all designer, dialogs, etc... here:
//    (see cmbtl9.h for other language-constants)


// CELL11 :const int LUL_LANGUAGE = CMBTLANG_DEFAULT;

const int LUL_LANGUAGE = CMBTLANG_GERMAN;

short GlobalLila ;

int zeittest = 0 ;	// 171109


int TEST = 0 ;	// 290610

// 150509 A : erweiterungen fuer odbc usw.
int  odbdabatyp = FITINFORMIX ;	// 0 = INFORMIX
								// 1 = MYSQL
								// 2 = ORACLE
char odbbasisverz[256] = "" ;
char odb_daba [256] = "" ;
char odb_user [256] = "" ;
char odb_pass [256] = "" ;
char odb_isomode[26] = "1" ;	// kann was gesteuert werden 
 								// z.Z. ( 01.04.09 )
								// 1 == setze dirty read 
								// 0 == kein dirty read setzen ( mysql,oracle ..... )

// 150509 E




const int CTEXTMAX = 512;

// irgendein dummy halt 
const char *FILENAME_DEFAULT = "c:\\user\\fit\\format\\LL\\11200.lst und immer laenger wird der string ........";

bool cfgOK;
char Listenname[512];
char szTempt[CTEXTMAX + 50];	// 141209 : von 5 auf 50 

char User[128] ;	// 121104
static char LUser[128] ;	// 220106
int danzahl ;		// 121104   
int dialoggelaufen ;	// 240507 
char myform_nr[99] ;	// eigentlich max 10 Zeichen + Zeichenkettenende
int Listenauswahl = 0;   // wenn 1 : Auswahl mehrerer Reports
int Hauptmenue = 0;   // wenn 1 : Auswahl edit oder print �ber Menue
#ifdef OLABEL
int DrMitMenue = 0;   // erst mal ausschalten
int xxx = 0 ;			// 190608 -Kopie-Modus-modifizieren
#else
int xxx = 0 ;			// Normaler Modus fuer normales dr70001 
int DrMitMenue = 1;   // wenn 1 : Ausdruck mit diversen Dialogen,sollte der Standard sein 
#endif
int EntwurfsModus = 0;   // wenn 1 : Entwurfsmodus: Aufruf ohne Parameter

static int alternat_ausgabe = 0 ;	// Alternativ-Ausgabe auf irgendein export-Modul
	// nach jetziger Erkenntnis gibt es : HTML, MHTML, RTF, PDF, XML, XLS, TXT, JPEG, EMF, TIFF, BMP
	// und dazu jeweils diverse Optionen
	// die unendlichen M�glichkeiten und Funktionen je nach Bedarf realisieren

static char alternat_dir  [ 513] ;
static char alternat_file [ 257] ;
static char alternat_type   [25] ;
static char alternat_sepachar[25] ;		// 140508
static char alternat_framechar[25] ;	// 140508
static char alternat_igl[25] ;	// 260509
static char alternat_show[25] ;	// 160310
static char alternat_linewrap[25] ;	// 160310
static char alternat_quiet[25] ;	// 160310
static char alternat_fsp [25] ;	// 250510

char odformat[29] ;		// 121205
char idformat[29] ;		// 121205

static int dnachkpreis = 2 ;	// Dummy fuer spaeteren sys_par( like drllls )

int WahlModus = 0 ;	// Wahlmodus : nur Druckerwahl ; Name muss mitgegeben werden, sonst Fehler
int datenausdatei = 0;	// entscheidung, ob Ranges per Dialog
						// oder aus einer Parameterdatei kommen sollen
int mitrange = 0; // oder nur Vorbelegung und dann doch noch Dialog

int dvariante = 0 ;	// 110309 

int externe_sortnr = -1 ;	// 280307 :
							// -1 : keine externe Vorgabe
							// > 0 : entsprechende Vorgabe

static char datenausdateiname[513] ;	// Dateiname ......

char *wort[100];	// mehr als 100 Worte braucht es nicht ?!

char *get_default (char *env) ;	// 151009 : Prototyping 

#ifndef OLABEL
 DB_CLASS dbClass;

 TOU_CLASS Tou;
 ADR_CLASS Adr;
 PTXT_CLASS Ptxt;
 NFORM_TAB_CLASS Nform_tab;
 NFORM_FELD_CLASS Nform_feld;

 SYSCOLUMNS_CLASS Syscolumns;
 SYSTABLES_CLASS Systables;
 TEMPTAB_CLASS Temptab;
 PTABN_CLASS Ptabn;
 SYS_PAR_CLASS Sys_par;



#ifndef FITSMART
 EXT_TEXT_CLASS Ext_text ;
extern SYSTABLES_CLASS Systables_class ;	// 071110
extern SYSCOLUMNS_CLASS Syscolumns_class ;	// 071110
extern FORM_T_CLASS Form_t ;				// 071110
#endif
#endif


#ifdef OLABEL
static int olabelanzahl ;
static int olabelfcount ;
static char olafeldname[OLASTU][51] ;
static char olafeldwert[OLASTU][2505] ;	// 160713/150813 : 201->251->2505, bei groesserem Text kann ein Laufzeitfehler
static int  olafeldtyp[OLASTU] ;


// Modus == 0 -> nur struktur lesen 
// Modus > 0 -> auch noch Daten lesen
// Returncode : Anzahl gelesener Saetze

int olaopendaten ( int imodus )
{
 int incount, i, o ;
 int gescount ;
 int ianz ;
// 150813 char ibuf [120] ;
// 150813 char suchwert [110] ;
// 150813 char suchname[31] ;
char ibuf [2600] ;
char suchwert [2505] ;
char suchname[51] ;
char dateiname[256] ;
// char * etc ;
 FILE * fp_struct ;
 

 sprintf (dateiname, "%s\\format\\LuLabel\\%s.stu", odbbasisverz, myform_nr);

 fp_struct = fopen ( dateiname, "r" ) ;
	 if ( fp_struct == NULL )	return 0 ;
 incount = 0 ;	
 while ( ( (incount + 1) < OLASTU   )&&  ( fgets( ibuf, 2595 , fp_struct)!= NULL ) )	// 160713 : 100-> OLASTU
 {																						// 150813 : 110 -> 2595
	 ianz = (int) strlen ( ibuf ) ;
	 if ( ianz < 3 ) continue ;
	 if (ibuf[0] == '$' && ibuf[1] == '$' )
	 {
		i = 2 ;
		while ( (i < ianz) && (ibuf[i] != ';') && i < 50 )	// 150813 : i < 50 dazu als Notbremse
		{
			olafeldname[incount][i - 2] = ibuf[i] ;
			i ++ ;
		}
		olafeldname[incount][i - 2 ] = '\0' ;	
		olafeldwert[incount][0] = '\0' ;	// Wert initen 
		i ++ ;
		olafeldtyp[incount] = 0 ;
		if ( i < ianz )
		{
			if ( ibuf[i] == 'N' || ibuf[i] == 'n' )
				olafeldtyp[incount] = 1 ;
		}

		 incount ++ ;
	}
}
gescount = incount ;
fclose ( fp_struct ) ;
if ( ! imodus ) return gescount ;
if ( ! datenausdatei ) return 0 ;	// Keine Daten da
if ( imodus )
{
	sprintf (dateiname, "%s", datenausdateiname);
	fp_struct = fopen ( dateiname, "r" ) ;
	if ( fp_struct == NULL )	return 0 ;
	while ( ( fgets( ibuf, 2595, fp_struct)) != NULL )	// 150813 110->2595
	{

		ianz = (int) strlen ( ibuf );
		if ( ianz < 3 ) continue ;
		if (ibuf[0] == '$' && ibuf[1] == '$' )
		{
			i = 2 ;
			while ( (i < ianz) && (ibuf[i] != ';') && (i < 50 ) )	// 150813 i <51 dazu
			{
				suchname[i - 2 ] = ibuf[i] ;
				i ++ ;
			}
			suchname[i - 2 ] = '\0' ;
			i ++ ;
			o = 0 ;
			suchwert[o] = '\0' ;
			while ( i < ianz && ibuf[i] != 0x0a && ibuf[i] != 0x0d )
			{
				//		 230114 : Portabel-Falle		suchwert[o++] = ibuf[i++] ;
				suchwert[o] = ibuf[i] ;
				o++;
				i++;
			}
			suchwert[o] = '\0' ;
		}

		int xxyy = strlen(suchname);	// 230114
		for ( incount = 0 ; incount < gescount ; incount ++ )
		{
			// 230114 : disjunkte Namen sind eine Einschr�nkung	 :		if ( ! strncmp ( suchname, olafeldname[incount] , strlen ( suchname)) )
			if ( (! strncmp ( suchname, olafeldname[incount] , xxyy)) && olafeldname[incount ][ xxyy ] =='\0' )
			{
				sprintf ( olafeldwert[incount],"%s", suchwert ) ;
				break ;
			}
		}
	}
};
return gescount ;
}

#endif	OLABEL


HWND hMainWindow;

// int form_feld_curs, tabid_curs, coltyp_curs,  dynacurs ; 

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

// GR: Registrieren eines Nachrichtenkanals f�r List & Label Callbacks
// US: register LuL MessageBase:
UINT CMainFrame::m_uLuLMessageBase = RegisterWindowMessage("cmbtLLMessage");

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_START_DEBUG, OnStartDebug)
	ON_COMMAND(ID_FILE_STOP_DEBUG, OnStopDebug)
	ON_COMMAND(ID_EDIT_LABEL, OnEditLabel)
	ON_COMMAND(ID_EDIT_LIST, OnEditList)
	ON_UPDATE_COMMAND_UI(ID_FILE_START_DEBUG, OnUpdateStartDebug)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP_DEBUG, OnUpdateStopDebug)
	ON_COMMAND(ID_PRINT_LABEL, OnPrintLabel)
	ON_COMMAND(ID_PRINT_REPORT, OnPrintReport)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(m_uLuLMessageBase,OnLulMessage)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{


	m_bDebug = FALSE;

}

CMainFrame::~CMainFrame()
{
/* ---->
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
< ------ */

}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

void schreibezeit ( char * text )
{
FILE *fperr ;
char writepuffer[199] ;
char erfnam [199] ;
char timestr[100] ;

struct _timeb tstruct;


    _ftime( &tstruct ); 
	sprintf (writepuffer , "%s.%u : %s\n" , _strtime(timestr), tstruct.millitm, text ) ; 

	sprintf ( erfnam , "%s\\timelog.dr7", getenv("TMPPATH") ) ;
	if ( ( fperr = fopen(erfnam, "at" )) == NULL ) return ;   // APPEND_TEXT 
	fprintf ( fperr , writepuffer ) ;
	fclose ( fperr ) ;
}

// A 230610 A
void LoadDbIni ()
{

	CString St ;
	PropExtension::CProperties DbIni;  
	char *etc = getenv ("BWSETC");
	if (etc != NULL)
	{
if ( TEST )
		sprintf ( etc, "e:\\user\\mios\\etc" );	
		DbIni.FileName.Format ("%s\\Database.ini", etc);
	}
	else
	{
		DbIni.FileName = "Database.ini";;
	}
	DbIni.SectionName = "Database";
	DbIni.Load ();
// 230610 
	CString tMode = DbIni.GetValue (CString ("MODE"));
	if (tMode != "MODE")
	{
		St = tMode.GetBuffer() ;
		St.MakeUpper ();
		if (St.Find ("ORACLE") > -1)
		{
			odbdabatyp = FITORACLE ;
		}

		if (St.Find ("INFORMIX") > -1)
		{
			odbdabatyp = FITINFORMIX ;
		}

		if (St.Find ("MYSQL") > -1)
		{
			odbdabatyp = FITMYSQL ;
		}
	}

	CString tName = DbIni.GetValue (CString ("NAME"));
	if (tName != "NAME")
	{
// 		DbName = tName;
		 St = tName.GetBuffer() ;
		 sprintf ( odb_daba ,"%s" ,St) ;
	}
	CString tUser = DbIni.GetValue (CString ("USER"));
	if (tUser != "USER")
	{
//		User = tUser;
		 St = tUser.GetBuffer() ;
		 sprintf ( odb_user ,"%s" ,St) ;
	}
	CString tPassw = DbIni.GetValue (CString ("PASSW"));
	if (tPassw != "PASSW")
	{
//		Passw = tPassw;
		St = tPassw.GetBuffer() ;
		sprintf ( odb_pass ,"%s" ,St) ;
	}
}





/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

/* ---> #ifndef CELL11
	LlAddCtlSupport(m_hWnd,
					LL_CTL_ADDTOSYSMENU | LL_CTL_CONVERTCONTROLS | LL_CTL_ALSOCHILDREN,
					"combit.ini");
  #endif  < ------ */
	GetCfgValues();

	char * etc ;
	
	odbdabatyp = FITINFORMIX ;	// default : informix z.B. fuer standard-fit

	LoadDbIni () ;	// 230610 : evtl. auch vorher noch den gesamten DABATYP �berladen 
	
	InterpretCommandLine() ;	// Achtung : setzt auch entsprechende Details zum dabatyp 
	if ( zeittest ) schreibezeit("Start nach Kommandline \n" ) ;

// 200509 A 

#ifdef FITODBC

#ifdef FITSMART
//	ich subsummiere fitodbc hier mal immer mit .....

	odbdabatyp = FITMYSQL ;	// MYSQL 
    etc = getenv ( "FITROOT" ) ;
	if ( etc != NULL )
		sprintf ( odbbasisverz, "%s", etc ) ;
	sprintf ( odb_daba ,"fsm" ) ;
//	char odb_user [256] = "" ;
//	char odb_pass [256] = "" ;
	printf ( odb_isomode, "0" ) ;	// kein dirty-read 

#else
//	alle ODBC-Ablaeufe ausser fitsmart bws mit odbc oder oracle oder , oder ....
	if ( odbdabatyp == FITINFORMIX )	// nix oder INFORMIX -> daher vermute ich standard-Fit
	{
		etc = getenv ( "BWS" ) ;
		if ( etc != NULL )
			sprintf ( odbbasisverz, "%s", etc ) ;
		sprintf ( odb_daba ,"bws" ) ;
//		char odb_user [256] = "" ;
//		char odb_pass [256] = "" ;
		sprintf ( odb_isomode, "1" ) ;	// dirty-read aktiv
	}

	if ( odbdabatyp == FITORACLE )	// ORACLE -> daher vermute ich bg-Projekt
	{
/* -> bis 230610 
		etc = getenv ( "BWSO" ) ;
		if ( etc != NULL )
			sprintf ( odbbasisverz, "%s", etc ) ;
		sprintf ( odb_daba ,"bwso" ) ;
//		char odb_user [256] = "" ;
//		char odb_pass [256] = "" ;
		sprintf ( odb_isomode, "0" ) ;	// kein dirty-read
< ----- */
		etc = getenv ( "BWS" ) ;
if ( TEST )
		sprintf ( etc, "e:\\user\\mios" ) ;
		if ( etc != NULL )
			sprintf ( odbbasisverz, "%s", etc ) ;
		sprintf ( odb_daba ,"bws" ) ;
		sprintf ( odb_user , "SYSTEM" ) ;
		sprintf ( odb_pass , "t0pfit" ) ;
		sprintf ( odb_isomode, "0" ) ;	// kein dirty-read
		LoadDbIni() ;	// mit aktuellen werten ueberladen 
	}
#endif
#else
// Standard-bws mit systables oder auch fuer olabel 
	etc = getenv ( "bws" ) ;
	if ( etc != NULL )
		sprintf ( odbbasisverz, "%s", etc ) ;
	sprintf ( odb_daba ,"bws" ) ;
	// odb_user ;
	// odb_pass ;
	sprintf ( odb_isomode, "1" );	// informix : dirty read aktiv 
#endif

// 200509 E

	if ( EntwurfsModus)
	{   DrMitMenue = 1;
		Hauptmenue = 1 ;
		Listenauswahl = 1 ;
	}

	if (Hauptmenue == 0)
	{
		PostMessage(WM_COMMAND,ID_PRINT_REPORT,0l);
	}


	SetWindowText ( myform_nr) ;	// 240608 

return 0;
}

void CMainFrame::OnStartDebug()
{
	MessageBox("Make sure that DEBWIN2 had been started before this demo application. "
				"If this doesn't happen you won't see any debug outputs now!",
				"List & Label Sample App", MB_OK | MB_ICONINFORMATION);
	LlSetDebug(LL_DEBUG_CMBTLL);	// am 02102014
	m_bDebug = TRUE;

}

void CMainFrame::OnStopDebug()
{
	LlSetDebug(FALSE);
	m_bDebug = FALSE;
}

void CMainFrame::OnUpdateStopDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bDebug);
}

void CMainFrame::OnUpdateStartDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bDebug);
}

//=============================================================================
void CMainFrame::OnEditLabel()
//=============================================================================
{

}

int GetRecCount()  
{
//	return RepoRech.dbcount ();
	return 1 ;
}


// 110506
#ifndef OLABEL
void CMainFrame::ptabschreiben( HJOB hJob, char * varfeld , int imodus , char * tab , char * feld ) 
{
	char hilfsfeld [256] ;
	char hilfsfeld2[256] ;
	int schleife ;
	schleife = 0 ;

	while ( schleife  < 5 )
	{
// ptbezk
		if ( schleife == 0 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptbezk", tab, feld );
			if  ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptbezk",  feld );
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptbezk ) ;
		};
// ptbez
		if ( schleife == 1 ) 
		{
			sprintf ( hilfsfeld, "%s.%s.ptbez",tab, feld);
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptbez", feld );
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptbez ) ;
		}
// ptwert
		if ( schleife == 2 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwert",tab, feld );
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptwert", feld);
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptwert ) ;
		}

// ptwer1
		if ( schleife == 3 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwer1", tab, feld ) ;
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptwer1", feld ) ;
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptwer1 ) ;
		}

// ptwer2
		if ( schleife == 4 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwer2", tab, feld ) ;
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptwer2", feld ) ;
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptwer2 ) ;
		}

		if ( varfeld[0] == 'V' )
		{	
			if ( !imodus )
				LlDefineVariableExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
			else
				if ( LlPrintIsVariableUsed(hJob, hilfsfeld))
					LlDefineVariableExt(hJob, clipped( hilfsfeld),
								clipped(hilfsfeld2), LL_TEXT, NULL);
		}
		else
		{
			if ( !imodus )
				LlDefineFieldExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
			else
				if ( LlPrintIsFieldUsed(hJob, hilfsfeld))
					LlDefineFieldExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
		}
		schleife ++ ;
	} ;	// end while 

}
//  071110 nach unten verschieben : #endif	// ifndef OLABEL
// nicht aktiv fuer fitsmart
#ifndef FITSMART
// 071110 
#ifdef FITODBC
void erzeugeextfelder( HJOB hJob , CLlSampleDoc* pDoc,  char * hilfsfeld , char * hilfsfeld2 , char typ[5] ) 
#endif
#ifndef FITODBC
void erzeugeextfelder( HJOB hJob , char * hilfsfeld , char * hilfsfeld2 , char typ[5] ) 
#endif
{
char feldname[33] ;

// 1. lese form_t-Satz 
	form_t.lila = GlobalLila ;
	sprintf ( form_t.form_nr, "%s", myform_nr );
	sprintf ( form_t.tab_nam, "%s", clipped( nform_feld.tab_nam) );
	sprintf ( form_t.feld_nam, "%s", clipped( nform_feld.feld_nam) ) ;

	int di ;
	di =  Form_t.openform_t() ;

// Falls gefunden, alle Felder anzeigen ....
	if (!Form_t.leseform_t())
	{
#ifndef FITODBC
		sprintf ( systables.tabname , "%s" , clipped( form_t.ttab_nam));
		systables.tabid = 0 ;
		di = Systables.opensystables()  ;
		Systables.lesesystables() ;
#endif
		for ( int feldnum = 0 ; feldnum < 5 ; feldnum ++ )
		{
			switch ( feldnum )
			{
				case 0 : sprintf ( feldname , "%s" , form_t.sfeld1 ) ; break ;
				case 1 : sprintf ( feldname , "%s" , form_t.sfeld2 ) ; break ;
				case 2 : sprintf ( feldname , "%s" , form_t.sfeld3 ) ; break ;
				case 3 : sprintf ( feldname , "%s" , form_t.sfeld4 ) ; break ;
				case 4 : sprintf ( feldname , "%s" , form_t.sfeld5 ) ; break ;

			}
			if ( strlen ( clipped (feldname )) == 0 )
				continue ;	// nix zu tun .....

			syscolumns.coltype  = iDBCHAR ;	// Notbremse

#ifndef FITODBC
			if ( !systables.tabid )	// Error
			{
				syscolumns.coltype  = iDBCHAR ;	// Notbremse
			}
			else
			{
				sprintf ( syscolumns.colname , "%s" , clipped( feldname));  
				syscolumns.coltype = iDBCHAR ;
				syscolumns.tabid = systables.tabid ;
				di = Syscolumns.opensyscolumns();
				di = Syscolumns.lesesyscolumns();
			}
#endif
					
#ifdef FITODBC
			pDoc->FetchColumnInfo(clipped(form_t.ttab_nam)
		                          ,clipped(feldname) ) ;
			pDoc->m_pColumnset->MoveFirst();
			if (!pDoc->m_pColumnset->IsEOF())
			{	// gefunden -> einigermassen unkrititsche Zuordnung
				syscolumns.coltype = (short)pDoc->m_pColumnset->m_nDataType ;
				switch (syscolumns.coltype)
				{
					case SQL_UNKNOWN_TYPE :
					case SQL_NUMERIC :
					case SQL_CHAR :
							syscolumns.coltype = iDBCHAR ;
						break ;
						
					case iDBSERIAL :		// 150214 :  zum lesen als integer behandeln
//					case iDBSERIAL - 256 :	// offset wegnehmen 150214
					case SQL_INTEGER :
							syscolumns.coltype = iDBINTEGER ;
						break ;

					case SQL_SMALLINT : 
							syscolumns.coltype = iDBSMALLINT ;
						break ;

					case SQL_DECIMAL :
					case SQL_FLOAT :
					case SQL_REAL :
					case SQL_DOUBLE :
							syscolumns.coltype = iDBDECIMAL ;
						break ;

					case SQL_DATE :
							syscolumns.coltype = iDBDATUM ;
						break ;

					case SQL_TIMESTAMP :
					case SQL_TIME :			// 140214

							syscolumns.coltype = iDBDATETIME ;
						break ;

				}
			}
			else
			{	// negativer Notnagel 
					syscolumns.coltype = iDBCHAR ;
			}
		
#endif

			sprintf ( hilfsfeld , "%s.%s.%s.%s",
				clipped(nform_feld.tab_nam) ,clipped(nform_feld.feld_nam),
				    clipped(form_t.ttab_nam) , clipped(feldname) ) ;
			sprintf ( hilfsfeld2 , "%s" ,feldname ) ;

			if ( syscolumns.coltype == iDBCHAR
			  || syscolumns.coltype == iDBDATUM 
			  || syscolumns.coltype == iDBDATETIME )
			{	//  Datum-Default sinnvoll belegen 
				if ( syscolumns.coltype == iDBDATUM 
					|| syscolumns.coltype == iDBDATETIME )
						sprintf ( hilfsfeld2, "14.12.2012");
				else
					sprintf ( hilfsfeld2, "123");	// 140214 : Initialisierung immer sinnvoll

				if ( typ[0] == 'V' )					
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,	clipped(hilfsfeld2), LL_TEXT, NULL);
				else
					LlDefineFieldExt(hJob, clipped( hilfsfeld) , clipped(hilfsfeld2), LL_TEXT, NULL);
			}
			else
			{
				if ( typ[0] == 'V' )					
					LlDefineVariableExt(hJob, clipped (hilfsfeld) ,	"1.0000", LL_NUMERIC, NULL);
				else
					LlDefineFieldExt(hJob, clipped (hilfsfeld) ,	"1.0000", LL_NUMERIC, NULL);
			}

		}	// schleife ueber felder
	}		// ueberhaupt was gefunden

}

#endif	// nicht bei fitsmart
#endif	// ifndef OLABEL


void CMainFrame::Variablendefinition (HJOB hJob)
{
	char hilfsfeld[256] ;
	char hilfsfeld2[128] ;
	sprintf ( hilfsfeld2,"");	// 140214 : immer initilaisieren

#ifndef OLABEL		// aus der Datenbank halt ....
#ifdef FITODBC
	CLlSampleView *pView = (CLlSampleView *) GetActiveView();
//	CLlSampleDoc* pDoc = pView->GetDocument();
	pDoc = pView->GetDocument();
	ASSERT_VALID(pDoc);
	pDoc->OnOpenDocument() ;
#endif

	sprintf ( systables.tabname, " " );	// initialisieren
	systables.tabid = 0 ;				// initialisieren

	sprintf (nform_tab.form_nr ,"%s", myform_nr );
	nform_tab.lila = GlobalLila ;

	Nform_tab.opennformtab ();
	while (!Nform_tab.lesenformtab())	// gefunden
	{

		sprintf (nform_feld.form_nr ,"%s", myform_nr );
		sprintf (nform_feld.tab_nam ,"%s", nform_tab.tab_nam );
		nform_feld.lila = GlobalLila ;

		Nform_feld.opennform_feld ();
		while (!Nform_feld.lesenform_feld())	// gefunden
		{
			switch ( nform_feld.feld_typ )
			{
				case FT_PTABN : // ptabn
// 220207 
				ptabschreiben ( hJob, "V" , 0 ,
					clipped (nform_feld.tab_nam), clipped( nform_feld.feld_nam) ) ;	
					if ( nform_feld.range > 0 )			// 220813
						goto WIEDEFAULTV ;

					break ;

				case FT_TEXTBAUST : // Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-Textbaustein", clipped ( nform_feld.krz_txt));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					break ;

				case FT_ADRESSE : // adresse
//					anr
					sprintf ( hilfsfeld, "%s.%s.anr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.anr", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_krz
					sprintf ( hilfsfeld, "%s.%s.adr_krz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_krz", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam1
					sprintf ( hilfsfeld, "%s.%s.adr_nam1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam1", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam2
					sprintf ( hilfsfeld, "%s.%s.adr_nam2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam2", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam3			090905
					sprintf ( hilfsfeld, "%s.%s.adr_nam3", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam3", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					ort1
					sprintf ( hilfsfeld, "%s.%s.ort1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort1", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 060307 A
//					ort2
					sprintf ( hilfsfeld, "%s.%s.ort2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort2", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					tel
					sprintf ( hilfsfeld, "%s.%s.tel", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.tel", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					fax
					sprintf ( hilfsfeld, "%s.%s.fax", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.fax", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 301111				mobil
					sprintf ( hilfsfeld, "%s.%s.mobil", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.mobil", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 300813			swift
					sprintf ( hilfsfeld, "%s.%s.swift", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.swift", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 300813			iban
					sprintf ( hilfsfeld, "%s.%s.iban", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iban", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					partner
					sprintf ( hilfsfeld, "%s.%s.partner", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.partner", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 060307 E
//					plz
					sprintf ( hilfsfeld, "%s.%s.plz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 080709
//					postfach
					sprintf ( hilfsfeld, "%s.%s.pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.pf", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

					sprintf ( hilfsfeld, "%s.%s.plz_pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz_pf", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					
//					str
					sprintf ( hilfsfeld, "%s.%s.str", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.str", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					iln		301104
					sprintf ( hilfsfeld, "%s.%s.iln", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iln", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr	// 021204
					sprintf ( hilfsfeld, "%s.%s.adr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"08150", LL_NUMERIC, NULL);

// staat	110506
					sprintf ( hilfsfeld, "%s.%s",clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					ptabschreiben ( hJob, "V" , 0 ,hilfsfeld, "staat" ) ;


				break ;

// 071110 : fitsmart macht da gar nix ....
#ifndef FITSMART
				case FT_FELDEXT :	// extended-feld 071110
#ifdef FITODBC
					erzeugeextfelder ( hJob ,  pDoc , hilfsfeld , hilfsfeld2 , "V" ) ;
#else
					erzeugeextfelder ( hJob ,  hilfsfeld , hilfsfeld2 , "V" ) ;

#endif
					break ;

#endif


				case FT_TEXTEXT : // extended-Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-E-Textbaustein", clipped ( nform_feld.krz_txt));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 220207 : explizit weiter ...					break ;

				default :
WIEDEFAULTV:
					if ( nform_feld.feld_typ == FT_TEXTEXT )
						sprintf ( hilfsfeld, "%s.%s_intern", clipped (nform_feld.tab_nam)
							, clipped( nform_feld.feld_nam ));
					else
						sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
							, clipped( nform_feld.feld_nam ));

					sprintf ( hilfsfeld2, "%s", clipped ( nform_feld.feld_nam) );


#ifndef FITODBC

					if ( strcmp ( clipped ( systables.tabname)  , clipped ( nform_feld.tab_nam)))
					{
						sprintf ( systables.tabname , "%s" , clipped( nform_feld.tab_nam));
						systables.tabid = 0 ;
						Systables.opensystables ();
						Systables.lesesystables();

					}
					if ( !systables.tabid )	// Error
					{
						syscolumns.coltype  = iDBCHAR ;	// Notbremse
					}
					else
					{
						sprintf ( syscolumns.colname , "%s" , clipped( nform_feld.feld_nam));  
						syscolumns.coltype = iDBCHAR ;
						syscolumns.tabid = systables.tabid ;
						Syscolumns.opensyscolumns ();
						Syscolumns.lesesyscolumns();
					}

#endif	// ifndef FITODBC

#ifdef FITODBC
					pDoc->FetchColumnInfo(clipped(nform_feld.tab_nam)
		                          ,clipped(nform_feld.feld_nam) ) ;
					pDoc->m_pColumnset->MoveFirst();
					if (!pDoc->m_pColumnset->IsEOF())
					{
						// gefunden -> einigermassen unkrititsche Zuordnung

						syscolumns.coltype = (short)pDoc->m_pColumnset->m_nDataType ;
						switch (syscolumns.coltype)
						{
						case SQL_UNKNOWN_TYPE :
						case SQL_NUMERIC :
						case SQL_CHAR :
						case iSQL_VARCHAR :	// 280612 bissel doof halt .........
							syscolumns.coltype = iDBCHAR ;
							break ;

						case iDBSERIAL :		// 201209 zum lesen als integer behandeln
//						case iDBSERIAL - 256 :	// offset wegnehmen 150214
						case SQL_INTEGER :
							syscolumns.coltype = iDBINTEGER ;
							break ;

						case SQL_SMALLINT : 
							syscolumns.coltype = iDBSMALLINT ;
							break ;

						case SQL_DECIMAL :
						case SQL_FLOAT :
						case SQL_REAL :
						case SQL_DOUBLE :
								syscolumns.coltype = iDBDECIMAL ;
								break ;

						case SQL_DATE :
								syscolumns.coltype = iDBDATUM ;
								break ;
// 111205
						case SQL_TIMESTAMP :
						case SQL_TIME :			// 140214
								syscolumns.coltype = iDBDATETIME ;
								break ;

						}
					
					
					}
					else
					{	// negativer Notnagel 
						syscolumns.coltype = iDBCHAR ;
					}
	
			

#endif		// ifdef FITODBC

#ifndef FITODBC		// 140214 : da war noch was ...
					if ( syscolumns.coltype == SQL_TIME || syscolumns.coltype == SQL_TIMESTAMP )
							syscolumns.coltype = iDBDATETIME;
#endif

					if ( syscolumns.coltype == iDBCHAR 
						|| syscolumns.coltype == iDBVARCHAR		// 280612 -erst mal nur fuer Informix
						|| syscolumns.coltype == iDBDATUM 
						|| syscolumns.coltype == iDBDATETIME )
					{

// 200105 : Datum-Default sinnvoll belegen 
						if ( syscolumns.coltype == iDBDATUM 
							|| syscolumns.coltype == iDBDATETIME )
							sprintf ( hilfsfeld2, "08.08.2008");
						else
							sprintf ( hilfsfeld2, "123");	// immer initialisieren 140214
							if ( nform_feld.feld_typ != FT_PTABN )	// 220813
								LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
									clipped(hilfsfeld2), LL_TEXT, NULL);
					}
					else
					{
							if ( nform_feld.feld_typ != FT_PTABN )	// 220813
								LlDefineVariableExt(hJob, clipped (hilfsfeld) ,
									"1.0000", LL_NUMERIC, NULL);
					}

// 060307 A
					if (! ( strncmp ( clipped( nform_feld.feld_nam), "tou", 3 )))
					{
						if ( (strlen ( nform_feld.feld_nam )) == 3 ||
							 ! (strncmp(nform_feld.feld_nam ,"tou_nr",6)))
						{
							sprintf ( hilfsfeld, "%s.%s_bez", clipped (nform_feld.tab_nam)
							, clipped( nform_feld.feld_nam ));
								LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
					}
// 060307 E

// 200105 A
// 080206 : Detlef hat es zuerst gemerkt	if ( nform_feld.range == 1 )
					if ( nform_feld.range > 0 )
					{
						sprintf ( hilfsfeld, "rangt.%s.v%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

						if ( syscolumns.coltype == iDBCHAR
							|| syscolumns.coltype == iDBDATUM
							|| syscolumns.coltype == iDBDATETIME )
						{
							LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
						else
						{
							LlDefineVariableExt(hJob, clipped (hilfsfeld) ,
								"1.0000", LL_NUMERIC, NULL);
						}

						sprintf ( hilfsfeld, "rangt.%s.b%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

						if ( syscolumns.coltype == iDBCHAR 
							|| syscolumns.coltype == iDBDATUM 
							|| syscolumns.coltype == iDBDATETIME )
						{
							LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
						else
						{
							LlDefineVariableExt(hJob, clipped (hilfsfeld) ,
								"2.0000", LL_NUMERIC, NULL);
						}
// 220909 
						sprintf ( hilfsfeld, "rangt.%s.r%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
						LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"Wertebereich", LL_TEXT, NULL);
						
					}
// 200105 E
				
			}
		}
	}

#ifndef OLABEL	// 201213

	form_v.lila = GlobalLila ;
	sprintf ( form_v.form_nr,"%s", myform_nr );
	Form_v_class.openform_v ();
	while (!Form_v_class.leseform_v())	// gefunden
	{
		sprintf ( hilfsfeld, "ali.%s", clipped (form_v.klar_txt));

		if ( form_v.typ == iDBSMALLINT ||form_v.typ == iDBINTEGER ||form_v.typ == iDBDECIMAL )
		{
			LlDefineVariableExt(hJob,hilfsfeld , "1234", LL_NUMERIC, NULL);
		}
		else
//		if ( form_v.typ == iDBCHAR ||form_v.typ == iDBDATUM  )
		{
			LlDefineVariableExt(hJob,hilfsfeld , "ali.ABC", LL_TEXT, NULL);
		}
	}
#endif


// 160305 
	LlDefineVariableExt(hJob, "rangt.nachkpreis" , "2.0000", LL_NUMERIC, NULL);
	LlDefineVariableExt(hJob, "rangt.sortnr" , "0", LL_NUMERIC, NULL);	// 270306
	LlDefineVariableExt(hJob, "rangt.anzvon" , "1.0000", LL_NUMERIC, NULL);	// 30408
	LlDefineVariableExt(hJob, "rangt.anzges" , "2.0000", LL_NUMERIC, NULL);	//030408

// 220107 
	LlDefineVariableExt(hJob, "rangt.Heute" , "01.01.2012", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rangt.Zeit" , "11:55:00", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rangt.Nutzer" , "Nu-Name", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rangt.variante" , "0", LL_NUMERIC, NULL);	// 110309




#endif		// ifndef OLABEL d.h. Definitiones kommen aus der datenbank
#ifdef OLABEL

	int schleife = olaopendaten(0) ;
	while ( schleife > 0 )
	{
		schleife -- ;
		sprintf ( hilfsfeld, "fitet.%s", olafeldname[schleife] );
		if ( olafeldtyp[schleife] == 1 )	// Numerisch
		{

			LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"2.00" , LL_NUMERIC, NULL);
		}
		else								// rest alphanum.
		{
				LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								olafeldname[schleife], LL_TEXT, NULL);
		}


	}
// 251011 A
	sprintf ( hilfsfeld , "rangt.anzvon" ) ;
	LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"1.00" , LL_NUMERIC, NULL);

	sprintf ( hilfsfeld , "rangt.anzges" ) ;
	LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"2.00" , LL_NUMERIC, NULL);
// 251011 E	
#endif

}


void CMainFrame::Felderdefinition (HJOB hJob)
{

	char hilfsfeld[256] ;
	char hilfsfeld2[128] ;
	sprintf ( hilfsfeld2,"");	// 140214 : immer initialisieren

#ifdef FITODBC
	CLlSampleView *pView = (CLlSampleView *) GetActiveView();
//	CLlSampleDoc* pDoc = pView->GetDocument();
	pDoc = pView->GetDocument();

	ASSERT_VALID(pDoc);
	pDoc->OnOpenDocument() ;
#endif

#ifndef OLABEL		// aus der Datenbank halt ....

	sprintf ( systables.tabname, " " );	// initialisieren
	systables.tabid = 0 ;				// initialisieren


	sprintf (nform_tab.form_nr ,"%s", myform_nr );
	nform_tab.lila = GlobalLila ;

	Nform_tab.opennformtab ();
	while (!Nform_tab.lesenformtab())	// gefunden
	{

		sprintf (nform_feld.form_nr ,"%s", myform_nr );
		sprintf (nform_feld.tab_nam ,"%s", nform_tab.tab_nam );
		nform_feld.lila = GlobalLila ;


		Nform_feld.opennform_feld ();
		while (!Nform_feld.lesenform_feld())	// gefunden
		{
			switch ( nform_feld.feld_typ )
			{
				case FT_PTABN : // ptabn

					ptabschreiben ( hJob, "F" , 0 ,
					clipped (nform_feld.tab_nam), clipped( nform_feld.feld_nam) ) ;	

					break ;

				case FT_TEXTBAUST : // Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-Textbaustein", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					break ;


				case FT_ADRESSE : // adresse
//					anr
					sprintf ( hilfsfeld, "%s.%s.anr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.anr", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_krz
					sprintf ( hilfsfeld, "%s.%s.adr_krz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_krz", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam1
					sprintf ( hilfsfeld, "%s.%s.adr_nam1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam1", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam2
					sprintf ( hilfsfeld, "%s.%s.adr_nam2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam2", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam3	090905
					sprintf ( hilfsfeld, "%s.%s.adr_nam3", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam3", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					ort1
					sprintf ( hilfsfeld, "%s.%s.ort1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort1", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 060307 A
//					ort2
					sprintf ( hilfsfeld, "%s.%s.ort2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort2", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					tel
					sprintf ( hilfsfeld, "%s.%s.tel", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.tel", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					fax
					sprintf ( hilfsfeld, "%s.%s.fax", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.fax", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//	301111				mobil
					sprintf ( hilfsfeld, "%s.%s.mobil", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.mobil", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//	300813			swift
					sprintf ( hilfsfeld, "%s.%s.swift", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.swift", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//	301111			iban
					sprintf ( hilfsfeld, "%s.%s.iban", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iban", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					partner
					sprintf ( hilfsfeld, "%s.%s.partner", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.partner", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

// 060307 E

//					plz
					sprintf ( hilfsfeld, "%s.%s.plz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

// 080709
//					postfach
					sprintf ( hilfsfeld, "%s.%s.pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.pf", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

					sprintf ( hilfsfeld, "%s.%s.plz_pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz_pf", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);



//					str
					sprintf ( hilfsfeld, "%s.%s.str", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.str", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					iln		301104
					sprintf ( hilfsfeld, "%s.%s.iln", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iln", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr		021204
					sprintf ( hilfsfeld, "%s.%s.adr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								"08150", LL_TEXT, NULL);

// staat	110506
					sprintf ( hilfsfeld, "%s.%s",clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					ptabschreiben ( hJob, "F" , 0 ,hilfsfeld, "staat" ) ;

					break ;
				case FT_FELDEXT :	// 071110 

#ifndef FITSMART
#ifdef FITODBC
					erzeugeextfelder ( hJob ,  pDoc , hilfsfeld , hilfsfeld2 , "F" ) ;
#else
					erzeugeextfelder ( hJob ,  hilfsfeld , hilfsfeld2 , "F" ) ;

#endif
					break ;

#endif

				case FT_TEXTEXT : // extended-Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-E-Textbaustein", clipped ( nform_feld.krz_txt));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 220207 : absichtlich weiter :					break ;

				default :
					if ( nform_feld.feld_typ == FT_TEXTEXT )
						sprintf ( hilfsfeld, "%s.%s_intern", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					else	// 220207 : echtes default 
						sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

					sprintf ( hilfsfeld2, "%s", clipped ( nform_feld.feld_nam) );

#ifndef FITODBC
					if ( strcmp ( clipped ( systables.tabname)  , clipped ( nform_feld.tab_nam)))
					{
						sprintf ( systables.tabname , "%s" , clipped( nform_feld.tab_nam));
						systables.tabid = 0 ;
						Systables.opensystables ();
						Systables.lesesystables();

					}
					if ( !systables.tabid )	// Error
					{
						syscolumns.coltype  = iDBCHAR ;	// Notbremse
					}
					else
					{
						sprintf ( syscolumns.colname , "%s" , clipped( nform_feld.feld_nam));  
						syscolumns.coltype = iDBCHAR ;
						syscolumns.tabid = systables.tabid ;
						Syscolumns.opensyscolumns ();
						Syscolumns.lesesyscolumns();
					}
#endif

#ifdef FITODBC
					pDoc->FetchColumnInfo(clipped(nform_feld.tab_nam)
		                          ,clipped(nform_feld.feld_nam) ) ;
					pDoc->m_pColumnset->MoveFirst();
					if (!pDoc->m_pColumnset->IsEOF())
					{
						// gefunden -> einigermassen unkrititsche Zuordnung

						syscolumns.coltype = (short)pDoc->m_pColumnset->m_nDataType ;
						switch (syscolumns.coltype)
						{
						case SQL_UNKNOWN_TYPE :
						case SQL_NUMERIC :
						case SQL_CHAR :
						case iSQL_VARCHAR :		// 280612 
							syscolumns.coltype = iDBCHAR ;
							break ;

//						case iDBSERIAL - 256 :	// offset wegnehmen 150214
						case iDBSERIAL :		// 201209 zum lesen als integer behandeln 
						case SQL_INTEGER :
							syscolumns.coltype = iDBINTEGER ;
							break ;

						case SQL_SMALLINT : 
							syscolumns.coltype = iDBSMALLINT ;
							break ;

						case SQL_DECIMAL :
						case SQL_FLOAT :
						case SQL_REAL :
						case SQL_DOUBLE :
								syscolumns.coltype = iDBDECIMAL ;
								break ;

						case SQL_DATE :
								syscolumns.coltype = iDBDATUM ;
								break ;
// 111205
						case SQL_TIMESTAMP :
						case SQL_TIME :			// 140214

								syscolumns.coltype = iDBDATETIME ;
								break ;

						}
					
					
					}
					else
					{	// negativer Notnagel 
						syscolumns.coltype = iDBCHAR ;
					}
	
			

#endif

#ifndef FITODBC		// 140214 : da war noch was ...
					if ( syscolumns.coltype == SQL_TIME || syscolumns.coltype == SQL_TIMESTAMP )
							syscolumns.coltype = iDBDATETIME;
#endif


					if ( syscolumns.coltype == iDBCHAR
						|| syscolumns.coltype == iDBVARCHAR		// 280612 -erst mal nur fuer Informix
						|| syscolumns.coltype == iDBDATUM
						|| syscolumns.coltype == iDBDATETIME )
					{

						if (  syscolumns.coltype == iDBDATUM
						|| syscolumns.coltype == iDBDATETIME )
							sprintf( hilfsfeld2, "01.01.2015");	// 140214
						else
							sprintf ( hilfsfeld2,"12345");	// 140214
						LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					}
					else
						LlDefineFieldExt(hJob, clipped (hilfsfeld) ,
								"1.0000", LL_NUMERIC, NULL);

// 060307 A
					if (! ( strncmp ( clipped( nform_feld.feld_nam), "tou", 3 )))
					{
						if ( (strlen ( nform_feld.feld_nam )) == 3 ||
							 ! (strncmp(nform_feld.feld_nam ,"tou_nr",6)))
						{
							sprintf ( hilfsfeld, "%s.%s_bez", clipped (nform_feld.tab_nam)
							, clipped( nform_feld.feld_nam ));
								LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
					}
// 060307 E
// 150508 A
					if ( nform_feld.range > 0 )
					{
						sprintf ( hilfsfeld, "rangt.%s.v%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

						if ( syscolumns.coltype == iDBCHAR
							|| syscolumns.coltype == iDBDATUM
							|| syscolumns.coltype == iDBDATETIME )
						{
							LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
						else
						{
							LlDefineFieldExt(hJob, clipped (hilfsfeld) ,
								"1.0000", LL_NUMERIC, NULL);
						}

						sprintf ( hilfsfeld, "rangt.%s.b%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

						if ( syscolumns.coltype == iDBCHAR 
							|| syscolumns.coltype == iDBDATUM 
							|| syscolumns.coltype == iDBDATETIME )
						{
							LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
						else
						{
							LlDefineFieldExt(hJob, clipped (hilfsfeld) ,
								"2.0000", LL_NUMERIC, NULL);
						}
// 220909 
						sprintf ( hilfsfeld, "rangt.%s.r%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
						LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								"Wertebereich", LL_TEXT, NULL);

					}
// 150508 E

			}
		}
	}

#ifndef OLABEL	// 201213

	form_v.lila = GlobalLila ;
	sprintf ( form_v.form_nr,"%s", myform_nr );
	Form_v_class.openform_v ();
	while (!Form_v_class.leseform_v())	// gefunden
	{
		sprintf ( hilfsfeld, "ali.%s", clipped (form_v.klar_txt));

		if ( form_v.typ == iDBSMALLINT ||form_v.typ == iDBINTEGER ||form_v.typ == iDBDECIMAL )
		{
			LlDefineFieldExt(hJob,hilfsfeld , "1234", LL_NUMERIC, NULL);
		}
		else
//		if ( form_v.typ == iDBCHAR ||form_v.typ == iDBDATUM  )
		{
			LlDefineFieldExt(hJob,hilfsfeld , "ali.ABC", LL_TEXT, NULL);
		}
	}
#endif

// 160305 
	LlDefineFieldExt(hJob, "rangt.nachkpreis" , "2.0000", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "rangt.sortnr" , "0", LL_NUMERIC, NULL);	// 270307
// 220107 
	LlDefineFieldExt(hJob, "rangt.Heute" , "01.01.2012", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rangt.Zeit" , "11:55:00", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rangt.Nutzer" , "Nu-Name", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rangt.variante" , "0" , LL_NUMERIC, NULL);	// 110309

#endif	// ifndef OLABEL

#ifdef OLABEL

	int schleife = olaopendaten(0) ;
	while ( schleife > 0 )
	{
		schleife -- ;
		sprintf ( hilfsfeld, "fitet.%s", olafeldname[schleife] );
		if ( olafeldtyp[schleife] == 1 )	// Numerisch
		{

			LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								"2.00" , LL_NUMERIC, NULL);
		}
		else								// rest alphanum.
		{
				LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								olafeldname[schleife], LL_TEXT, NULL);
		}

	}
	// 251011 A
		sprintf ( hilfsfeld , "rangt.anzvon" ) ;
		LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								"1.00" , LL_NUMERIC, NULL);

		sprintf ( hilfsfeld , "rangt.anzges" ) ;
		LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								"2.00" , LL_NUMERIC, NULL);
// 251011 E

	
#endif

}

/* ---> folgende Formate sind vorgesehen  :
intern immer : dd.mm.yyyy

Aus der Daba :
oddmy4.		// dd.mm.yyyy
ody4md-		// yyyy-mm-dd default (Informix/mysql)
ody4dm-		// yyyy-dd-mm

In die Daba

iddmy4.		// dd.mm.yyyy default (Informix/mysql)
idymd4-		// yyyy-mm-dd 
idydm4-		// yyyy-dd-mm

< --- */

/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sqldatset
-
-       In              :       Request zum setzen der Format-Strings
-
-       Out             :       aktuelle Format-Strings
-
-       Errorcodes      :       -
-
-
--------------------------------------------------------------------------------
**/
void sqldatset(void)
{
//	sprintf ( odformat, "dmy4." ) ;	// eher selten
	sprintf ( odformat, "y4md-" ) ;	// default
//	sprintf ( odformat, "y4dm-" ) ;	// irgendwas halbdeutsches (MSDE)

	sprintf ( idformat, "dmy4." ) ;	// funktioniert hier fuer fit-informix 
//	sprintf ( idformat, "y4md-" ) ;	// ????
//	sprintf ( idformat, "y4dm-" ) ;	// ????

}


/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sqldatamger
-
-       In              :       string-amerikanisch "YYYY-MM-DD"
-
-       Out             :       string-germanisch   "DD.MM.JJJJ"
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Umformatieren eines Datumsstrings
-
--------------------------------------------------------------------------------
**/

/* -> das doofe odbc versucht sich an Eigenintelligenz ...
-> alles neu und dynamischer testen ..... ab 270907
char * sqldatamger(char *inpstr, char *outpstr)

{

	if ( ! strcmp ( odformat, "y4md-" ))		// default fit-informix
	{
		if (( inpstr[0] == ' ') || ( inpstr[0] == '\0') ||(inpstr[1] == '\0' ))
		{	// 080205 : Null-String handeln
			sprintf ( outpstr, "  .  .    " ) ;
		}
		else
		{
			outpstr[ 0]= inpstr[8];
			outpstr[ 1]= inpstr[9];
			outpstr[ 2]= '.';
			outpstr[ 3]= inpstr[5];
			outpstr[ 4]= inpstr[6];
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[0];
			outpstr[ 7]= inpstr[1];
			outpstr[ 8]= inpstr[2];
			outpstr[ 9]= inpstr[3];
			outpstr[10]= '\0';
		} ;
		return outpstr ;
	}	// "y4md-"

	if ( ! strcmp ( odformat, "y4dm-" ))
	{
		if (( inpstr[0] == ' ') || ( inpstr[0] == '\0') ||(inpstr[1] == '\0' ))
		{	// 080205 : Null-String handeln
			sprintf ( outpstr, "  .  .    " ) ;
		}
		else
		{
			outpstr[ 0]= inpstr[5];
			outpstr[ 1]= inpstr[6];
			outpstr[ 2]= '.';
			outpstr[ 3]= inpstr[8];
			outpstr[ 4]= inpstr[9];
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[0];
			outpstr[ 7]= inpstr[1];
			outpstr[ 8]= inpstr[2];
			outpstr[ 9]= inpstr[3];
			outpstr[10]= '\0';
		} ;
		return outpstr ;
	}	// "y4dm-"

	if ( ! strcmp ( odformat, "dmy4." ))	// sicher eher selten 	
	{
		if (( inpstr[0] == ' ') || ( inpstr[0] == '\0') ||(inpstr[1] == '\0' ))
		{
			sprintf ( outpstr, "  .  .    " ) ;
		}
		else
		{
			outpstr[ 0]= inpstr[0];
			outpstr[ 1]= inpstr[1];
			outpstr[ 2]= '.';
			outpstr[ 3]= inpstr[3];
			outpstr[ 4]= inpstr[4];
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[6];
			outpstr[ 7]= inpstr[7];
			outpstr[ 8]= inpstr[8];
			outpstr[ 9]= inpstr[9];
			outpstr[10]= '\0';
		} ;
		return outpstr ;
	}	// "dmy4."
	return inpstr ;	// Error-return
}
< ----- */

// Output ist immer ein "deutscher" Datumsstring
// Zuerst reale Struktur testen, erst danach default versuchen 
char * sqldatamger(char *inpstr, char *outpstr)
{
	if (( inpstr[0] == ' ') || ( inpstr[0] == '\0') ||(inpstr[1] == '\0' ))
	{
		sprintf ( outpstr, "  .  .    " ) ;
		return outpstr ;
	}
	else
	{
		if ( inpstr[4] == '-' && inpstr[7] == '-' )	// passt fuer y4md- und y4dm-
		{
			if ( ! strcmp ( odformat, "y4md-" ))		// default fit-informix
			{
				outpstr[ 0]= inpstr[8];
				outpstr[ 1]= inpstr[9];
				outpstr[ 3]= inpstr[5];
				outpstr[ 4]= inpstr[6];
			}
			else	// eher hypothetischer Fall : "y4dm-"
			{
				outpstr[ 0]= inpstr[5];
				outpstr[ 1]= inpstr[6];
				outpstr[ 3]= inpstr[8];
				outpstr[ 4]= inpstr[9];
			}
			outpstr[ 2]= '.';
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[0];
			outpstr[ 7]= inpstr[1];
			outpstr[ 8]= inpstr[2];
			outpstr[ 9]= inpstr[3];
			outpstr[10]= '\0';
			return outpstr ;
		} ;

		if ( inpstr[2] == '.' && inpstr[5] == '.' )	// passt fuer dmy4.
		{

			outpstr[ 0]= inpstr[0];
			outpstr[ 1]= inpstr[1];
			outpstr[ 2]= '.';
			outpstr[ 3]= inpstr[3];
			outpstr[ 4]= inpstr[4];
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[6];
			outpstr[ 7]= inpstr[7];
			outpstr[ 8]= inpstr[8];
			outpstr[ 9]= inpstr[9];
			outpstr[10]= '\0';
			return outpstr ;
		}	// "dmy4."
// ab jetzt geht sowieso alles schief ....

		if ( ! strcmp ( odformat, "y4md-" ))		// default fit-informix
		{
			outpstr[ 0]= inpstr[8];
			outpstr[ 1]= inpstr[9];
			outpstr[ 2]= '.';
			outpstr[ 3]= inpstr[5];
			outpstr[ 4]= inpstr[6];
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[0];
			outpstr[ 7]= inpstr[1];
			outpstr[ 8]= inpstr[2];
			outpstr[ 9]= inpstr[3];
			outpstr[10]= '\0';
			return outpstr ;
		}	// "y4md-"

		if ( ! strcmp ( odformat, "y4dm-" ))
		{
			outpstr[ 0]= inpstr[5];
			outpstr[ 1]= inpstr[6];
			outpstr[ 2]= '.';
			outpstr[ 3]= inpstr[8];
			outpstr[ 4]= inpstr[9];
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[0];
			outpstr[ 7]= inpstr[1];
			outpstr[ 8]= inpstr[2];
			outpstr[ 9]= inpstr[3];
			outpstr[10]= '\0';
			return outpstr ;
		}	// "y4dm-"

		if ( ! strcmp ( odformat, "dmy4." ))	// irgendwie germanisch 	
		{
			outpstr[ 0]= inpstr[0];
			outpstr[ 1]= inpstr[1];
			outpstr[ 2]= '.';
			outpstr[ 3]= inpstr[3];
			outpstr[ 4]= inpstr[4];
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[6];
			outpstr[ 7]= inpstr[7];
			outpstr[ 8]= inpstr[8];
			outpstr[ 9]= inpstr[9];
			outpstr[10]= '\0';
			return outpstr ;
		}	// "dmy4."
	}
	return inpstr ;	// Error-return
}


// ausschnitt aus datetime.h
// time units for datetime qualifier

#define TU_YEAR 0
#define TU_MONTH 2
#define TU_DAY 4
#define TU_HOUR 6
#define TU_MINUTE 8
#define TU_SECOND 10
#define TU_FRAC 12
#define TU_F1 11
#define TU_F2 12
#define TU_F3 13
#define TU_F4 14
#define TU_F5 15



char * sqldatdsttime(TIMESTAMP_STRUCT *dstakt, char *outpstr, int datelen )
{


	int quals;
	int quale;
	quale = quals = datelen;
	quals = (quals>>4) & 0xf;
	quale = quale & 0xf;

	sprintf ( outpstr,"");

	if ( quale < TU_HOUR )	// Nur datum oder sonstwas
	{
		return outpstr;
	}
// Ab hier nehme ich mal immer als Startpunkt einfach mindestens hour an .......
	if ( quale < TU_MINUTE )
	{
		sprintf ( outpstr,"%02d", dstakt->hour);
		return outpstr;
	}
	if ( quale < TU_SECOND )
	{
		sprintf ( outpstr,"%02d:%02d", dstakt->hour, dstakt->minute);
		return outpstr;
	}
	if ( quale < TU_F1 )
	{
		sprintf ( outpstr,"%02d:%02d:%02d", dstakt->hour, dstakt->minute,dstakt->second);
		return outpstr;
	}

	if ( quale == TU_F1 )
	{
		sprintf ( outpstr,"%02d:%02d:%02d:%01d", dstakt->hour, dstakt->minute,dstakt->second,dstakt->fraction);
	}
	if ( quale == TU_F2 )
	{
		sprintf ( outpstr,"%02d:%02d:%02d:%02d", dstakt->hour, dstakt->minute,dstakt->second,dstakt->fraction);
	}
	if ( quale == TU_F3 )
	{
		sprintf ( outpstr,"%02d:%02d:%02d:%03d", dstakt->hour, dstakt->minute,dstakt->second,dstakt->fraction);
	}
	if ( quale == TU_F4 )
	{
		sprintf ( outpstr,"%02d:%02d:%02d:%04d", dstakt->hour, dstakt->minute,dstakt->second,dstakt->fraction);
	}
	if ( quale == TU_F5 )
	{
		sprintf ( outpstr,"%02d:%02d:%02d:%05d", dstakt->hour, dstakt->minute,dstakt->second,dstakt->fraction);
	}

	return ( outpstr ) ;
}


char * sqldatdstger(TIMESTAMP_STRUCT *dstakt, char *outpstr, int datelen )
{
	sprintf ( outpstr, "%02d.%02d.%04d", dstakt->day, dstakt->month,dstakt->year ) ;
	if ( dstakt->year < 1901 )	// NULL bekaempfen
		sprintf ( outpstr, "  .  .    " ) ;
	return ( outpstr ) ;
}

// Der Inputstring hat zwingend das Format "dd.mm.yyyyy" 

void sqldatdst( TIMESTAMP_STRUCT * dstakt , char * inpstr )
{
			dstakt->year   = (SQLSMALLINT) atoi ( inpstr + 6 );
			dstakt->month  = (SQLUSMALLINT) atoi ( inpstr + 3 ) ;
			dstakt->day    = (SQLUSMALLINT) atoi ( inpstr ) ;
			dstakt->minute = 0;
			dstakt->second = 0;
			dstakt->fraction = 0;
}

/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sqldatgeram
-
-
-       In              :       string-germanisch   "DD.MM.JJJJ"
-       Out             :       string-amerikanisch "YYYY-MM-DD"
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Umformatieren eines Datumsstrings
-
--------------------------------------------------------------------------------
**/
char * sqldatgeram(char *inpstr, char * outpstr)
{
	if ( ! strcmp ( idformat, "y4md-" ))		// funktionierte z.B. bei msde
	{
 		outpstr[ 0]= inpstr[6];
		outpstr[ 1]= inpstr[7];
		outpstr[ 2]= inpstr[8];
		outpstr[ 3]= inpstr[9];
		outpstr[ 4]= '-';
		outpstr[ 5]= inpstr[3];
		outpstr[ 6]= inpstr[4];
		outpstr[ 7]= '-';
		outpstr[ 8]= inpstr[0];
		outpstr[ 9]= inpstr[1];
		outpstr[10]= '\0';
		return  outpstr ;
	}	// "y4md-"
	if ( ! strcmp ( idformat, "dmy4." ))		// funktionierte z.B. bei 
	{												// fit-informix + fitsmart-mysql
 		outpstr[ 0]= inpstr[0];
		outpstr[ 1]= inpstr[1];
		outpstr[ 2]=  '.';
		outpstr[ 3]= inpstr[3];
		outpstr[ 4]= inpstr[4];
		outpstr[ 5]= '.';
		outpstr[ 6]= inpstr[6];
		outpstr[ 7]= inpstr[7];
		outpstr[ 8]= inpstr[8];
		outpstr[ 9]= inpstr[9];
		outpstr[10]= '\0';
		return  outpstr ;
	}	// "dmy4."
	if ( ! strcmp ( idformat, "y4dm-" ))		//	???? 
	{
 		outpstr[ 0]= inpstr[6];
		outpstr[ 1]= inpstr[7];
		outpstr[ 2]= inpstr[8];
		outpstr[ 3]= inpstr[9];
		outpstr[ 4]= '-';
		outpstr[ 5]= inpstr[0];
		outpstr[ 6]= inpstr[1];
		outpstr[ 7]= '-';
		outpstr[ 8]= inpstr[3];
		outpstr[ 9]= inpstr[4];
		outpstr[10]= '\0';
		return  outpstr ;
	}	// "y4dm-"
	return inpstr ;
}

/* ----> 140214 : toter Code
char * datplusgerman(char *inpstr, char * outpstr, long doffset)

{

 
	struct tm akttm;
	time_t dstart;

	char idd[3];
	char imm[3];
	char iyy[5];

	idd[0] = inpstr[0] ;
	idd[1] = inpstr[1] ;
	idd[2] = '\0' ;

	imm[0] = inpstr[3] ;
	imm[1] = inpstr[4] ;
	imm[2] = '\0' ;

	iyy[0] = inpstr[6] ;
	iyy[1] = inpstr[7] ;
	iyy[2] = inpstr[8] ;
	iyy[3] = inpstr[9] ;
	iyy[4] = '\0' ;
	
	
	time( &dstart );
	akttm = *localtime( &dstart );
 
	// in tm_year steckt dan so was wie "103" fuer 2003" usw.

	long hijahr = atol ( iyy );
	if ( hijahr < 60L) hijahr += 100L ;	//2 stelliges Jahr 2000 bis 2059
	if ( hijahr <= 60L && hijahr < 100L ) hijahr = hijahr ;	// 2 stelliges Jahr 1960 ..1999 syntax-dummy
	if ( hijahr > 1900L && hijahr < 2099L ) hijahr -= 1900L ;  // Der Rest fuehrt zu Fehlern ....
	akttm.tm_year = hijahr ; 
    akttm.tm_mday = atol ( idd ); 
    akttm.tm_mon = atol ( imm ) - 1L ; 
         
  if( (dstart = mktime( &akttm )) != (time_t)-1 )
  {

	dstart += ( 86400L * doffset ) ;
	akttm = *localtime( &dstart );

    sprintf ( outpstr, "%02d.%02d.%04d",	// 311213 : Format versch�nert
		akttm.tm_mday, akttm.tm_mon + 1,
		akttm.tm_year + 1900L) ; 
 
  
  }
  else	// mktime failed
  {
	  sprintf ( outpstr, "%s", inpstr );
  }
	return  outpstr ;
 
}
< ---- 140214 : toter code ---- */
#ifndef OLABEL

void textsetzen ( HJOB hJob, int iint, char * hilfsfeld ,  char * ctyp)
{

int k ;

// Nur was tun, wenn used und vorhanden usw. -iDBDECIMAL eigentlich nur zum Test
// 200509 Zwischenvariable einfuehren, weil im oracle immer nur decimal-Felders existieren

  long ptxtnr = 0 ;

//	201209 : serial dazu if (( tt_datetyp[iint] == iDBSMALLINT || tt_datetyp[iint] == iDBINTEGER )
	if (( tt_datetyp[iint] == iDBSMALLINT || tt_datetyp[iint] == iDBINTEGER || tt_datetyp[iint] == iDBSERIAL )
		&& tt_sqllong[iint] > 0L  && tt_referenz[iint] > 0L )
	{
		ptxtnr = tt_sqllong[iint] ;
	}

	if ( tt_datetyp[iint] == iDBDECIMAL 
		&& tt_sqldouble[iint] > 0.01  && tt_referenz[iint] > 0L )
	{
		ptxtnr = (long) tt_sqldouble[ iint] ;
	}


	if ( ptxtnr > 0 )
	{

// 200509		ptxt.nr = tt_sqllong[iint] ;
		ptxt.nr = ptxtnr ;
		szTempt[0] = '\0' ;
		k = Ptxt.openptxt(clipped ( tt_creferenz[ tt_referenz[iint]]));
		while (! k )
		{
			k = Ptxt.leseptxt() ;
			if (! k )
			{
				if (strlen(szTempt))
				{
					sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
				}
			
				if (( strlen(szTempt)) + ( strlen(clipped(ptxt.txt))) >= CTEXTMAX  )
				break ;
				sprintf(  szTempt + strlen(szTempt) , "%s", clipped(ptxt.txt) );
			}
		}
		if ( strlen(szTempt))
		{	
			if ( ctyp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld, szTempt, LL_TEXT, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld, szTempt, LL_TEXT, NULL);
		}
		else
		{
			if ( ctyp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
		}
	}
	else		// Feld definiert leer setzen
	{
			if ( ctyp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
		
	}
}


#ifndef FITSMART	// 240507
// 220207 

void exttextsetzen ( HJOB hJob, int iint, char * hilfsfeld ,  char * ctyp)
{
  int i,j,k ;
  j = 0 ;

	for ( i = 0 ; i < MAXETEXTE ; i ++ )
	{
		if ( et_cursors[i] <  0 )
			break ;	// Ende, bzw. noch nichts gefunden

		if ( ! strncmp ( et_feldnam[i],tt_feldnam[iint], strlen(clipped (et_feldnam[i]))))
		{
		  if ( strlen(clipped(et_feldnam[i])) == strlen(clipped(tt_feldnam[iint])))
		  {
		    if ( ! strncmp ( et_tabnam[i],tt_tabnam[iint], strlen( clipped(et_tabnam[i]))))
			{
			  if ( strlen (clipped(et_tabnam[i]))== strlen(clipped(tt_tabnam[iint])))
			  {	// jetzt passt wohl alles ?! 
			     j = 1 ;
				 break ;
			   }
			}
		 }
	  }
	}

	if ( i == MAXETEXTE )	// Overflow 
	{
		if ( ctyp[0] == 'V' )
			LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
		else
			LlDefineFieldExt(hJob, hilfsfeld,  " " , LL_TEXT, NULL);
		return ;
	}

	if ( ! j)	// Erst-Abfertigung 
	{
		Ext_text.Prepare(myform_nr,iint, i );
	}
	if ( et_cursors[i] <  1 )
	{	// schlechtes Ende 
		if ( ctyp[0] == 'V' )
			LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
		else
			LlDefineFieldExt(hJob, hilfsfeld,  " " , LL_TEXT, NULL);
		return ;
	}

	szTempt[0] = '\0' ;
	k = Ext_text.Openext_text(i);
	while (! k )
	{
		k = Ext_text.Leseext_text() ;
		if (! k )
		{
			if (strlen(szTempt))
			{
				sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
			}
			if (( strlen(szTempt)) + ( strlen(clipped(ext_text.txt))) >= CTEXTMAX  )
			break ;
			sprintf(  szTempt + strlen(szTempt) , "%s", clipped(ext_text.txt) );
		}
	}
	if ( strlen(szTempt))
	{	
		if ( ctyp[0] == 'V' )
			LlDefineVariableExt(hJob, hilfsfeld, szTempt, LL_TEXT, NULL);
		else
			LlDefineFieldExt(hJob, hilfsfeld, szTempt, LL_TEXT, NULL);
	}
	else
	{
		if ( ctyp[0] == 'V' )
			LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
		else
			LlDefineFieldExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
	}
}


// 071110 Variablen-use ignorieren 
// zuerst index finden, dann Basissatz fetchen, dann alles ausgeben
void extfeldsetzen ( HJOB hJob,  int iint, char * hilfsfeld, char * hilfsfeld2, char * ctyp )
{


	char hilfsfeld3[55];	// 140214
	char hilfsfeld4[55];	// 140214
  int i,j,k ;
  j = 0 ;

	for ( i = 0 ; i < MAXETEXTE ; i ++ )
	{
		if ( et_cursors[i] <  0 )
			break ;	// Ende, bzw. noch nichts gefunden

		if ( ! strncmp ( et_feldnam[i],tt_feldnam[iint], strlen(clipped (et_feldnam[i]))))
		{
		  if ( strlen(clipped(et_feldnam[i])) == strlen(clipped(tt_feldnam[iint])))
		  {
		    if ( ! strncmp ( et_tabnam[i],tt_tabnam[iint], strlen( clipped(et_tabnam[i]))))
			{
			  if ( strlen (clipped(et_tabnam[i]))== strlen(clipped(tt_tabnam[iint])))
			  {	// jetzt passt wohl alles ?! 
			     j = 1 ;
				 break ;
			   }
			}
		 }
	  }
	}

	if ( i == MAXETEXTE )	// Overflow 
	{
		return ; 
	}

	if ( ! j)	// Erst-Abfertigung 
	{
		Ext_text.Prepare(myform_nr,iint, i );
	}
	if ( et_cursors[i] <  1 )
	{	// schlechtes Ende - hier kommt schon der Laufzeitfehler 
	}



	k = Ext_text.Openext_text(i);	// k ist jetzt nur noch Marker : was gefunden oder eben nicht 


	char feldname[33] ;

	for ( int feldnum = 0 ; feldnum < 5 ; feldnum ++ )
	{
		switch ( feldnum )
		{
			case 0 : sprintf ( feldname , "%s" , clipped ( et_tfeldnam[i][0] )) ; break ;
			case 1 : sprintf ( feldname , "%s" , clipped ( et_tfeldnam[i][1] ) ) ; break ;
			case 2 : sprintf ( feldname , "%s" , clipped ( et_tfeldnam[i][2] ) ) ; break ;
			case 3 : sprintf ( feldname , "%s" , clipped ( et_tfeldnam[i][3] ) ) ; break ;
			case 4 : sprintf ( feldname , "%s" , clipped ( et_tfeldnam[i][4] ) ) ; break ;

		}
		if ( strlen ( clipped (feldname )) == 0 )
			continue ;	// nix zu tun .....

		sprintf ( hilfsfeld , "%s.%s.%s.%s",
			clipped(tt_tabnam[iint]) ,clipped(tt_feldnam[iint]),
			    clipped(et_ttabnam[i]) , feldname ) ;

		/* ---->
		if ( typ[0] == 'V' )					
			LlDefineVariableExt(hJob, clipped( hilfsfeld) ,	clipped(hilfsfeld2), LL_TEXT, NULL);
		else
			LlDefineFieldExt(hJob, clipped( hilfsfeld) , clipped(hilfsfeld2), LL_TEXT, NULL);
		< ----- */

		switch ( et_datetyp[i][feldnum] )
		{
		case(iDBCHAR):
		case (iDBVARCHAR):		// 280612
			if ( ! k )
				sprintf ( hilfsfeld2 ,"%s",clipped ((char *)et_sqlval[i][feldnum]));
			else
				sprintf ( hilfsfeld2, " " ) ;

			if ( ctyp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld , hilfsfeld2, LL_TEXT, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld , hilfsfeld2, LL_TEXT, NULL);
			break ;

		case(iDBSMALLINT):
			if ( ! k )
				sprintf ( hilfsfeld2, "%01d", et_sqllong[i][feldnum] );
			else
				sprintf ( hilfsfeld2 , "0" ) ;
			if ( ctyp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld , hilfsfeld2, LL_NUMERIC, NULL);
			break ;

		case(iDBSERIAL):
		case(iDBINTEGER):
			if ( ! k )
				sprintf ( hilfsfeld2, "%01ld", et_sqllong[i][feldnum] );
			else
				sprintf ( hilfsfeld2 , "0" ) ;
			if ( ctyp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld , hilfsfeld2, LL_NUMERIC, NULL);
			break ;
		case(iDBDECIMAL):
			if ( !k)
				sprintf ( hilfsfeld2, "%01.4lf", et_sqldouble[i][feldnum] );
			else
				sprintf ( hilfsfeld2 , "0" ) ;
			if ( ctyp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld , hilfsfeld2, LL_NUMERIC, NULL);
			break ;
		case(iDBDATUM):
			if ( ctyp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld , sqldatamger ((char *)et_sqlval[i][feldnum], hilfsfeld2) , LL_TEXT, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld , sqldatamger ((char *)et_sqlval[i][feldnum], hilfsfeld2) , LL_TEXT, NULL);
			break ;
		case(iDBDATETIME):
			// 150214hier lassen wir die Formatierung mal noch offen ....
			sqldatdstger ((TIMESTAMP_STRUCT *)et_sqlval[i][feldnum], hilfsfeld3,0x0b);		// 150214 : gegebnenfalls nachformatieren
			sqldatdsttime ((TIMESTAMP_STRUCT *)et_sqlval[i][feldnum], hilfsfeld4, 0x0b);	// 140214	; 150214 : gegebnenfalls nachformatieren	
			sprintf( hilfsfeld2,"%s %s", hilfsfeld3, hilfsfeld4);	// 140214		
			if ( ctyp[0] == 'V' )
			{

// 140214		LlDefineVariableExt(hJob, hilfsfeld , sqldatdstger ((TIMESTAMP_STRUCT *)et_sqlval[i][feldnum], hilfsfeld2) , LL_TEXT, NULL);
				LlDefineVariableExt(hJob, hilfsfeld , hilfsfeld2 , LL_TEXT, NULL);
			}
			else
			{
// 140214				LlDefineFieldExt(hJob, hilfsfeld , sqldatdstger ((TIMESTAMP_STRUCT *)et_sqlval[i][feldnum], hilfsfeld2) , LL_TEXT, NULL);
						LlDefineFieldExt(hJob, hilfsfeld , hilfsfeld2 , LL_TEXT, NULL);
			}
			break ;		
		default :	// schlechte Notbremse
			if ( !k)
				sprintf ( hilfsfeld2 ,"%s",et_sqlval[i][feldnum]);
			else
				sprintf ( hilfsfeld2 , "0" ) ;
			if ( ctyp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld , hilfsfeld2 , LL_TEXT, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld , hilfsfeld2 , LL_TEXT, NULL);
			break ;
		}

	}	// schleife ueber felder

}



#endif	// ! FITSMART
#endif	// ! OLABEL


void CMainFrame::VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno , int dox , int ganz )
{																// 300408 : dox und ganz dazu

	char hilfsfeld[256] ;
	char hilfsfeld2[256] ;
	char hilfsfeld3[55];	// 140214
	char hilfsfeld4[55];	// 140214

	dox ++ ;	// 030408 

	int i ;
	i=0;
#ifndef OLABEL
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;

		sprintf ( hilfsfeld, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);

		switch ( tt_feldtyp[i] )
		{
		case FT_PTABN : // ptabn
			sprintf ( hilfsfeld2, "%s.ptbezk" , hilfsfeld );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
				;
			else
			{
				sprintf ( hilfsfeld2, "%s.ptbez" , hilfsfeld );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
					;
				else
				{
					sprintf ( hilfsfeld2, "%s.ptwer1" , hilfsfeld );
					if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
						;
					else
					{
						sprintf ( hilfsfeld2, "%s.ptwer2" , hilfsfeld );
						if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
							;
						else
						{
							// 021204
							sprintf ( hilfsfeld2, "%s.ptwert" , hilfsfeld );
							if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
							;
							else
							{
								break ;
							} // ptwert
						}	  // ptwer2
					}		  // ptwer1
				}			  // ptbez
			}				  // ptbezk
		
			switch ( tt_datetyp[i] )
			{
			case(iDBVARCHAR):	// 280612
			case(iDBCHAR):
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			case(iDBSMALLINT):
				sprintf ( ptabn.ptwert, "%d", tt_sqllong[i] );
				break ;
			case(iDBSERIAL):	// 201209
			case(iDBINTEGER):
				sprintf ( ptabn.ptwert, "%ld", tt_sqllong[i] );
				break ;
			case(iDBDECIMAL):
				sprintf ( ptabn.ptwert, "%0.0f", tt_sqldouble[i] );	// 200509 : bisher fehlerhafterweise :"%0.0d"
				break ;
			default :	// schlechte Notbremse
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			}
			
			if ( strlen ( clipped ( tt_creferenz[ tt_referenz[i]])) > 0 )
			{
				sprintf ( ptabn.ptitem ,"%s" , clipped ( tt_creferenz[ tt_referenz[i]])) ;
			}
			else
				sprintf ( ptabn.ptitem ,"%s",tt_feldnam[i]);
	
			Ptabn.openptabn() ; 

			if ( Ptabn.leseptabn())
			{	// 091209 : anders fuellen und immer schreiben 
				sprintf ( ptabn.ptwer1 ,"%s nix", ptabn.ptwert );
				sprintf ( ptabn.ptwer2 ,"%s nix", ptabn.ptwert );
				sprintf ( ptabn.ptbez  ,"%s %s nix", ptabn.ptitem, ptabn.ptwert );
				sprintf ( ptabn.ptbezk ,"%s", ptabn.ptitem );
				ptabn.ptlfnr = 0 ;

			}
				ptabschreiben ( hJob, "V" , 1 ,tt_tabnam[i], tt_feldnam[i] ) ;

//			else
//				memcpy ( &ptabn,&ptabn_null, sizeof(struct PTABN));

			break ;

		case FT_TEXTBAUST : // Textbaustein 
			
			    sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{
					textsetzen (hJob, i, hilfsfeld,"V");
				}
			break ;

		case FT_ADRESSE : // adresse
// 200509 A : bei oracle gibbet nur numerisch (d.h. alles in decimal-Feldern versteckt)
// gleich als allgemeiner turnaround : hauptsache numerisch ohne NK-Stellen )
			char charadresse[25] ;
			long numadresse ;
			sprintf ( charadresse, "0" );
			switch ( tt_datetyp[i] )
			{
			 case(iDBVARCHAR):	// 280612
			 case(iDBCHAR):
				sprintf ( charadresse ,"%s",tt_sqlval[i]);
				break ;
			 case(iDBSMALLINT):
				sprintf ( charadresse, "%d", tt_sqllong[i] );
				break ;

 			 case(iDBSERIAL):	// 201209
			 case(iDBINTEGER):
				sprintf ( charadresse, "%ld", tt_sqllong[i] );
				break ;
			 case(iDBDECIMAL):
				sprintf ( charadresse, "%0.0f", tt_sqldouble[i] );
				break ;
			 default :	// schlechte Notbremse
				sprintf ( charadresse ,"%s",tt_sqlval[i]);

			}

			numadresse = atol ( charadresse ) ;
			
// 200509			if (adr.adr == tt_sqllong[i] )
			if (adr.adr == numadresse )
			{
				adr.adr = adr.adr ;	// testbreakpoint
			}
			else
			{
// 200509				adr.adr = tt_sqllong[i];
				adr.adr = numadresse ;
	
				Adr.openadr() ; 
				if ( Adr.leseadr(0))	// immer lesen ODER leeren
				{
					memcpy ( &adr,&adr_null, sizeof(struct ADR));
// 200509					adr.adr = tt_sqllong[i];
					adr.adr = numadresse;

				}
			}
//			anr als string
			sprintf ( hilfsfeld, "%s.%s.anr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				sprintf ( ptabn.ptwert, "%d", adr.anr );
				sprintf ( ptabn.ptitem ,"anr");
				Ptabn.openptabn() ; 
				if ( ! Ptabn.leseptabn())
//	280911				LlDefineVariableExt(hJob, hilfsfeld, clipped(ptabn.ptbezk), LL_TEXT, NULL);
					LlDefineVariableExt(hJob, hilfsfeld, clipped(ptabn.ptbez), LL_TEXT, NULL);
				else			
					LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
			}

//			adr_krz
			sprintf ( hilfsfeld, "%s.%s.adr_krz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
			    LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.adr_krz), LL_TEXT, NULL);
			}
//			adr_nam1
			sprintf ( hilfsfeld, "%s.%s.adr_nam1",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.adr_nam1), LL_TEXT, NULL);
			}
//			adr_nam2
			sprintf ( hilfsfeld, "%s.%s.adr_nam2",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.adr_nam2), LL_TEXT, NULL);
			}
//			adr_nam3			090905
			sprintf ( hilfsfeld, "%s.%s.adr_nam3",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.adr_nam3), LL_TEXT, NULL);
			}
//			ort1
			sprintf ( hilfsfeld, "%s.%s.ort1", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.ort1), LL_TEXT, NULL);
			}
// 060307 A
//			ort2
			sprintf ( hilfsfeld, "%s.%s.ort2", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.ort2), LL_TEXT, NULL);
			}
//			tel
			sprintf ( hilfsfeld, "%s.%s.tel", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.tel), LL_TEXT, NULL);
			}
//			fax
			sprintf ( hilfsfeld, "%s.%s.fax", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.fax), LL_TEXT, NULL);
			}
//	301111		mobil
			sprintf ( hilfsfeld, "%s.%s.mobil", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.mobil), LL_TEXT, NULL);
			}
//	300813	swift
			sprintf ( hilfsfeld, "%s.%s.swift", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.swift), LL_TEXT, NULL);
			}
//	300813	iban
			sprintf ( hilfsfeld, "%s.%s.iban", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.iban), LL_TEXT, NULL);
			}
//			partner
			sprintf ( hilfsfeld, "%s.%s.partner", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.partner), LL_TEXT, NULL);
			}

// 060307 E
//			plz
			sprintf ( hilfsfeld, "%s.%s.plz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.plz), LL_TEXT, NULL);
			}

// 080709
//			post_fach
			sprintf ( hilfsfeld, "%s.%s.plz_pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.plz_pf), LL_TEXT, NULL);
			}				
			sprintf ( hilfsfeld, "%s.%s.pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.pf), LL_TEXT, NULL);
			}				
			
//			str
			sprintf ( hilfsfeld, "%s.%s.str",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld , clipped(adr.str), LL_TEXT, NULL);
			}
//			iln
			sprintf ( hilfsfeld, "%s.%s.iln",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld , clipped(adr.iln), LL_TEXT, NULL);
			}
//			adr 021204
			sprintf ( hilfsfeld, "%s.%s.adr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
				LlDefineVariableExt(hJob, hilfsfeld ,szTemp2 , LL_TEXT, NULL);
			}

// staat	110506
			sprintf ( ptabn.ptwert, "%d", adr.staat );
			sprintf ( ptabn.ptitem ,"staat");
			Ptabn.openptabn() ; 
			if ( ! Ptabn.leseptabn())
			{
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				ptabschreiben ( hJob, "V" , 1 ,hilfsfeld, "staat" ) ;
			}


			break ;


#ifndef FITSMART	// 240507
		// 071110
		case FT_FELDEXT : // Ext-Feldbausteine 
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
					extfeldsetzen (hJob, i, hilfsfeld, hilfsfeld2, "V");
			break ;
		

		case FT_TEXTEXT : // Ext-Textbaustein	220207 
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{
					exttextsetzen (hJob, i, hilfsfeld,"V");
				}
// 220207 : absichtlich weiter :			break ;
#endif		// 240507
		default :	// Standard-Felder
			if ( tt_feldtyp[i] == FT_TEXTEXT )
			{
				sprintf ( hilfsfeld, "%s.%s_intern",tt_tabnam[i], tt_feldnam[i] );
			}

			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				switch ( tt_datetyp[i] )
				{
				case(iDBVARCHAR):	// 280612
				case(iDBCHAR):
					sprintf ( szTemp2 ,"%s",clipped ((char *)tt_sqlval[i]));
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_TEXT, NULL);
					break ;

				case(iDBSMALLINT):
					sprintf ( szTemp2, "%01d", tt_sqllong[i] );
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBSERIAL):	// 201209
				case(iDBINTEGER):
					sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDECIMAL):
					sprintf ( szTemp2, "%01.4lf", tt_sqldouble[i] );
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDATUM):
					LlDefineVariableExt(hJob, hilfsfeld , sqldatamger ((char *)tt_sqlval[i], szTemp2) , LL_TEXT, NULL);
					break ;
				case(iDBDATETIME):	// 111205

					sqldatdstger ((TIMESTAMP_STRUCT *)tt_sqlval[i], hilfsfeld3,tt_datelen[i]);	// 150214
					sqldatdsttime ((TIMESTAMP_STRUCT *)tt_sqlval[i], hilfsfeld4,tt_datelen[i]);	// 140214		
					sprintf( szTemp2,"%s %s", hilfsfeld3, hilfsfeld4);	// 140214		
	
// 140214					LlDefineVariableExt(hJob, hilfsfeld , sqldatdstger ((TIMESTAMP_STRUCT *)tt_sqlval[i], szTemp2) , LL_TEXT, NULL);
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2 , LL_TEXT, NULL);
					break ;		
				default :	// schlechte Notbremse
					sprintf ( szTemp2 ,"%s",tt_sqlval[i]);
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_TEXT, NULL);
				break ;
				}

			}
// 060307 A
			if (! ( strncmp ( clipped( tt_feldnam[i]), "tou", 3 )))
			{
				if ( (strlen ( tt_feldnam[i] )) == 3 ||
							 ! (strncmp(tt_feldnam[i] ,"tou_nr",6)))
				{
					sprintf ( hilfsfeld, "%s.%s_bez", clipped (tt_tabnam[i])
								, clipped( tt_feldnam[i] ));
					if (LlPrintIsVariableUsed(hJob, hilfsfeld))
					{
						switch ( tt_datetyp[i] )
						{
						case(iDBVARCHAR):	// 280612
						case(iDBCHAR):
								tou.tou = atol( clipped ((char *) tt_sqlval[i]));
						break ;

						case(iDBSERIAL):	// 201209
						case(iDBINTEGER):
						case(iDBSMALLINT):
								tou.tou = tt_sqllong[i] ;
						break ;
						case(iDBDECIMAL):
								tou.tou = (long) tt_sqldouble[i] ;
						break ;
						case(iDBDATUM):
						default :	// schlechte Notbremse
							tou.tou = 0L ;
						break ;
						}
						if ( tou.tou > 0L)
						{
							Tou.opentou() ;
							if ( !Tou.lesetou())
								LlDefineVariableExt(hJob, clipped( hilfsfeld), clipped(tou.tou_bz), LL_TEXT, NULL);
							else
								LlDefineVariableExt(hJob, clipped( hilfsfeld), " " , LL_TEXT, NULL);

						}
						else
								LlDefineVariableExt(hJob, clipped( hilfsfeld), " " , LL_TEXT, NULL);
					}
				}
			}
// 060307 E

			break ;
		} ;	// switch je Feldtyp/Generator
	
// 200105 A

		if ( tt_range[i] )
		{
			switch ( tt_datetyp[i] )
			{
			case(iDBDATETIME):
			case(iDBDATUM):
			case(iDBCHAR):
			case(iDBVARCHAR):	// 280612
// 220909 A : rangin ausgeben
				sprintf ( hilfsfeld, "rangt.%s.r%s", tt_tabnam[i], tt_feldnam[i] );
				if 	(tt_rangin[i]!= NULL )
				{
					LlDefineVariableExt(hJob, clipped( hilfsfeld),	tt_rangin[i], LL_TEXT, NULL);
				}
				else
				{
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,	" ", LL_TEXT, NULL);
				}
// 220909 E
				if 	( tt_cvrange[tt_range[i]][0] == -1 ) // 220909 || (tt_rangin[i]!= NULL ))	// 280508 : bei rangin unterdruecken
				{
					sprintf ( hilfsfeld, "rangt.%s.v%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								" ", LL_TEXT, NULL);
					sprintf ( hilfsfeld, "rangt.%s.b%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								" ", LL_TEXT, NULL);

					break ;
				}
				if 	( tt_cbrange[tt_range[i]][0] == -1)	// 220909  || ( tt_rangin[i]!= NULL) )	// 280508 : bei rangin unterdruecken
				{
					sprintf ( hilfsfeld, "rangt.%s.v%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								tt_cvrange[tt_range[i]], LL_TEXT, NULL);

					sprintf ( hilfsfeld, "rangt.%s.b%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								tt_cvrange[tt_range[i]], LL_TEXT, NULL);
				}
				else
				{
					sprintf ( hilfsfeld, "rangt.%s.v%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								tt_cvrange[tt_range[i]], LL_TEXT, NULL);

					sprintf ( hilfsfeld, "rangt.%s.b%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								tt_cbrange[tt_range[i]], LL_TEXT, NULL);
					}
				break ;
				;
			case (iDBSMALLINT) :

// 220909 A : rangin immer mit String
				sprintf ( hilfsfeld, "rangt.%s.r%s", tt_tabnam[i], tt_feldnam[i] );
				if 	(tt_rangin[i]!= NULL )
				{
					LlDefineVariableExt(hJob, clipped( hilfsfeld), tt_rangin[i], LL_TEXT, NULL);
				}
				else
				{
					if (tt_lvrange[tt_range[i]] == tt_lbrange[tt_range[i]])
					{
						sprintf ( szTemp2, "%01d", tt_lvrange[tt_range[i]] );
					}
					else
					{
						if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
							sprintf ( szTemp2," " ) ;
						else
							sprintf ( szTemp2, "%01d-%01d", tt_lvrange[tt_range[i]],tt_lbrange[tt_range[i]] );
					}
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,	szTemp2, LL_TEXT, NULL);
				}
// 220909 E 
				sprintf ( hilfsfeld, "rangt.%s.v%s",tt_tabnam[i], tt_feldnam[i] );
				if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
				// 220909  || ( tt_rangin[i] != NULL ) )	// 280508 : rangin unterdruecken
					sprintf ( szTemp2 , "0" );
				else
				{
					sprintf ( szTemp2, "%01d", tt_lvrange[tt_range[i]] );
				}

				LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);

				sprintf ( hilfsfeld, "rangt.%s.b%s",tt_tabnam[i], tt_feldnam[i] );
				if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
				// 220909 || ( tt_rangin[i] != NULL ))	// 280508 : rangin unterdruecken
					   sprintf ( szTemp2 , "0" );
				else			
					sprintf ( szTemp2, "%01d", tt_lbrange[tt_range[i]] );

				LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;

			case(iDBSERIAL):	// 201209
			case(iDBINTEGER):
// 220909 A : rangin immer mit String
				sprintf ( hilfsfeld, "rangt.%s.r%s", tt_tabnam[i], tt_feldnam[i] );
				if 	(tt_rangin[i]!= NULL )
				{
					LlDefineVariableExt(hJob, clipped( hilfsfeld),	tt_rangin[i], LL_TEXT, NULL);
				}
				else
				{
					if ( tt_lvrange[tt_range[i]] == tt_lbrange[tt_range[i]] )
					{
						sprintf ( szTemp2, "%01ld", tt_lbrange[tt_range[i]] );
					}
					else
					{
						if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
							sprintf ( szTemp2 , " " );
						else			
							sprintf ( szTemp2, "%01ld-%01ld",tt_lvrange[tt_range[i]], tt_lbrange[tt_range[i]] );
					}
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,	szTemp2, LL_TEXT, NULL);
				}

				sprintf ( hilfsfeld, "rangt.%s.v%s", tt_tabnam[i], tt_feldnam[i] );
				if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
				// 220909 || ( tt_rangin[i] != NULL ) )	// 280508 : rangin unterdruecken
					sprintf ( szTemp2 , "0" );
				else			
					sprintf ( szTemp2, "%01ld", tt_lvrange[tt_range[i]] );
				LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);

				sprintf ( hilfsfeld, "rangt.%s.b%s",tt_tabnam[i], tt_feldnam[i] );
				if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
				// 220909 || ( tt_rangin[i] != NULL ) )	// 280508 : rangin unterdruecken
					   sprintf ( szTemp2 , "0" );
				else			
					sprintf ( szTemp2, "%01ld", tt_lbrange[tt_range[i]] );
				LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;

			case(iDBDECIMAL):
// 220909 A : rangin immer mit String, 
				sprintf ( hilfsfeld, "rangt.%s.r%s", tt_tabnam[i], tt_feldnam[i] );
				if 	(tt_rangin[i]!= NULL )
				{
					LlDefineVariableExt(hJob, clipped( hilfsfeld),	tt_rangin[i], LL_TEXT, NULL);
				}
				else
				{
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,	" ", LL_TEXT, NULL);
				}
// 220909 E 
				
				// 100506 : bisher stand hier tt_lvrange <> 11 usw...
				sprintf ( hilfsfeld, "rangt.%s.v%s", tt_tabnam[i], tt_feldnam[i] );
				if ((tt_dvrange[tt_range[i]] == 11.0 ) && ( tt_dbrange[tt_range[i]] == -11.0 ))
				// 220909 || ( tt_rangin[i] != NULL ) )	// 280508 : rangin unterdruecken
					sprintf ( szTemp2 , "0.00" );
				else			
					sprintf ( szTemp2, "%01.4f", tt_dvrange[tt_range[i]] );
				LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);

				sprintf ( hilfsfeld, "rangt.%s.b%s", tt_tabnam[i], tt_feldnam[i] );
				if ((tt_dvrange[tt_range[i]] == 11.0 ) && ( tt_dbrange[tt_range[i]] == -11.0 ))
					sprintf ( szTemp2 , "0.00" );
				else			
					sprintf ( szTemp2, "%01.4f", tt_dbrange[tt_range[i]] );
				LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
			}
		}
		// 200105 E
		i ++ ;
	
	}		// while
#endif	// ifndef OLABEL
#ifdef OLABEL

	int schleife = olaopendaten(1) ;
	while ( schleife > 0 )
	{
		schleife -- ;
		sprintf ( hilfsfeld, "fitet.%s", olafeldname[schleife] );
		if ( olafeldtyp[schleife] == 1 )	// Numerisch
		{
			sprintf ( szTemp2, "%01.4f", atof(olafeldwert[schleife]) );

			LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								szTemp2, LL_NUMERIC, NULL);
		}
		else								// rest alphanum.
		{
				LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								olafeldwert[schleife], LL_TEXT, NULL);
		}
	}


	// 251011 A
		sprintf ( hilfsfeld , "rangt.anzvon" ) ;
		sprintf ( szTemp2, "%d", dox );
		LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								szTemp2 , LL_NUMERIC, NULL);

		sprintf ( hilfsfeld , "rangt.anzges" ) ;
		sprintf ( szTemp2, "%d", ganz );
		LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								szTemp2 , LL_NUMERIC, NULL);
// 251011 E
	
#endif
	if (LlPrintIsVariableUsed(hJob, "rangt.nachkpreis"))
	{
		sprintf ( szTemp2, "%d", dnachkpreis );
		LlDefineVariableExt(hJob, "rangt.nachkpreis" , szTemp2, LL_NUMERIC, NULL);
	}

// 110309
	if (LlPrintIsVariableUsed(hJob, "rangt.variante"))
	{
		sprintf ( szTemp2, "%d", dvariante );
		LlDefineVariableExt(hJob, "rangt.variante" , szTemp2, LL_NUMERIC, NULL);
	}

	if (LlPrintIsVariableUsed(hJob, "rangt.anzvon"))
	{
		sprintf ( szTemp2, "%d", dox );
		LlDefineVariableExt(hJob, "rangt.anzvon" , szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rangt.anzges"))
	{
		sprintf ( szTemp2, "%d", ganz );
		LlDefineVariableExt(hJob, "rangt.anzges" , szTemp2, LL_NUMERIC, NULL);
	}

#ifndef OLABEL
// 270307
	if (LlPrintIsVariableUsed(hJob, "rangt.sortnr"))
	{
		sprintf ( szTemp2, "%d", externe_sortnr );
		LlDefineVariableExt(hJob, "rangt.sortnr" , szTemp2, LL_NUMERIC, NULL);
	}
#endif

// 220107
	if (LlPrintIsVariableUsed(hJob, "rangt.Nutzer"))
	{
		sprintf ( szTemp2, "%s", LUser );
		LlDefineVariableExt(hJob, "rangt.Nutzer" , szTemp2, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rangt.Heute")||LlPrintIsVariableUsed(hJob, "rangt.Zeit"))
	{
		time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);
		sprintf ( szTemp2 , "%02d.%02d.%04d",
			ltime->tm_mday ,ltime->tm_mon + 1 ,ltime->tm_year + 1900 );

		LlDefineVariableExt(hJob, "rangt.Heute" , szTemp2, LL_TEXT, NULL);

		sprintf ( szTemp2 , "%02d:%02d:%02d",
			ltime->tm_hour, ltime->tm_min, ltime->tm_sec );
		LlDefineVariableExt(hJob, "rangt.Zeit" , szTemp2, LL_TEXT, NULL);
	}
}

void CMainFrame::FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{

	char hilfsfeld[256] ;
	char hilfsfeld2[256] ;
	char hilfsfeld3[55];	// 140214
	char hilfsfeld4[55];	// 140214


	int i ;
	i=0;
#ifndef OLABEL
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;

		sprintf ( hilfsfeld, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);

		switch ( tt_feldtyp[i] )
		{
		case FT_PTABN : // ptabn
			sprintf ( hilfsfeld2, "%s.ptbezk" , hilfsfeld );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
				;
			else
			{
				sprintf ( hilfsfeld2, "%s.ptbez" , hilfsfeld );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
					;
				else
				{
					sprintf ( hilfsfeld2, "%s.ptwer1" , hilfsfeld );
					if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
						;
					else
					{
						sprintf ( hilfsfeld2, "%s.ptwer2" , hilfsfeld );
						if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
							;
						else
						{
							// 021204
							sprintf ( hilfsfeld2, "%s.ptwert" , hilfsfeld );
							if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
								;
							else
							{
								break ;
							} // ptwert
						}	  // ptwer2
					}		  // ptwer1
				}			  // ptbez
			}				  // ptbezk
		
			switch ( tt_datetyp[i] )
			{
			case(iDBVARCHAR):	// 280612
			case(iDBCHAR):
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			case(iDBSMALLINT):
				sprintf ( ptabn.ptwert, "%d", tt_sqllong[i] );
				break ;

			case(iDBSERIAL):	// 201209
			case(iDBINTEGER):
				sprintf ( ptabn.ptwert, "%ld", tt_sqllong[i] );
				break ;
			case(iDBDECIMAL):
				sprintf ( ptabn.ptwert, "%0.0f", tt_sqldouble[i] );// 200509 : bisher fehlerhafterweise :"%0.0d"

				break ;
			default :	// schlechte Notbremse
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			}
				
			if ( strlen ( clipped ( tt_creferenz[ tt_referenz[i]])) > 0 )	// 091209
			{
				sprintf ( ptabn.ptitem ,"%s" , clipped ( tt_creferenz[ tt_referenz[i]])) ;
			}
			else
				sprintf ( ptabn.ptitem ,"%s",tt_feldnam[i]);


			Ptabn.openptabn() ; 

			if ( Ptabn.leseptabn())
			{	// 091209 : anders fuellen und immer schreiben
				sprintf ( ptabn.ptwer1 ,"%s nix", ptabn.ptwert );
				sprintf ( ptabn.ptwer2 ,"%s nix", ptabn.ptwert );
				sprintf ( ptabn.ptbez  ,"%s %s nix", ptabn.ptitem, ptabn.ptwert );
				sprintf ( ptabn.ptbezk ,"%s", ptabn.ptitem );
				ptabn.ptlfnr = 0 ;

			}

			ptabschreiben ( hJob, "F" , 1 ,tt_tabnam[i], tt_feldnam[i] ) ;

			break ;

		case FT_TEXTBAUST : // Textbaustein 
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					textsetzen (hJob, i, hilfsfeld, "F");
				}
			break ;

		case FT_ADRESSE : // adresse
// 200509 A : bei oracle gibbet nur numerisch (d.h. alles in decimal-Feldern versteckt)
// gleich als allgemeiner turnaround : hauptsache numerisch ohne NK-Stellen )
			char charadresse[25] ;
			long numadresse ;
			sprintf ( charadresse, "0" );
			switch ( tt_datetyp[i] )
			{
			 case(iDBVARCHAR):	// 280612
			 case(iDBCHAR):
				sprintf ( charadresse ,"%s",tt_sqlval[i]);
				break ;
			 case(iDBSMALLINT):
				sprintf ( charadresse, "%d", tt_sqllong[i] );
				break ;

			 case(iDBSERIAL):	// 201209
			 case(iDBINTEGER):
				sprintf ( charadresse, "%ld", tt_sqllong[i] );
				break ;
			 case(iDBDECIMAL):
				sprintf ( charadresse, "%0.0f", tt_sqldouble[i] );
				break ;
			 default :	// schlechte Notbremse
				sprintf ( charadresse ,"%s",tt_sqlval[i]);

			}

			numadresse = atol ( charadresse ) ;
			
// 200509			if (adr.adr == tt_sqllong[i] )

			if ( adr.adr == numadresse ) 
			{
				adr.adr = adr.adr ;	// testbreakpoint
			}
			else
			{
//				adr.adr = tt_sqllong[i];
				adr.adr = numadresse;
	
				Adr.openadr() ; 
				if ( Adr.leseadr(0))	// immer lesen ODER leeren
				{
					memcpy ( &adr,&adr_null, sizeof(struct ADR));
					adr.adr = tt_sqllong[i];

				}
			}

//			anr als string
			sprintf ( hilfsfeld, "%s.%s.anr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				sprintf ( ptabn.ptwert, "%d", adr.anr );
				sprintf ( ptabn.ptitem ,"anr");
				Ptabn.openptabn() ; 
				if ( ! Ptabn.leseptabn())
// 280911					LlDefineFieldExt(hJob, hilfsfeld, clipped(ptabn.ptbezk), LL_TEXT, NULL);
					LlDefineFieldExt(hJob, hilfsfeld, clipped(ptabn.ptbez), LL_TEXT, NULL);
				else			
					LlDefineFieldExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
			}

//			adr_krz
			sprintf ( hilfsfeld, "%s.%s.adr_krz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.adr_krz), LL_TEXT, NULL);
			}
//			adr_nam1
			sprintf ( hilfsfeld, "%s.%s.adr_nam1",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.adr_nam1), LL_TEXT, NULL);
			}
//			adr_nam2
			sprintf ( hilfsfeld, "%s.%s.adr_nam2",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.adr_nam2), LL_TEXT, NULL);
			}
//			adr_nam3		090905
			sprintf ( hilfsfeld, "%s.%s.adr_nam3",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.adr_nam3), LL_TEXT, NULL);
			}
//			ort1
			sprintf ( hilfsfeld, "%s.%s.ort1", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.ort1), LL_TEXT, NULL);
			}
// 060307 A
//			ort2
			sprintf ( hilfsfeld, "%s.%s.ort2", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.ort2), LL_TEXT, NULL);
			}
//			tel
			sprintf ( hilfsfeld, "%s.%s.tel", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.tel), LL_TEXT, NULL);
			}
//			fax
			sprintf ( hilfsfeld, "%s.%s.fax", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.fax), LL_TEXT, NULL);
			}
//	301111		mobil
			sprintf ( hilfsfeld, "%s.%s.mobil", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.mobil), LL_TEXT, NULL);
			}
//	300813	swift
			sprintf ( hilfsfeld, "%s.%s.swift", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.swift), LL_TEXT, NULL);
			}
//	300813	iban
			sprintf ( hilfsfeld, "%s.%s.iban", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.iban), LL_TEXT, NULL);
			}

//			partner
			sprintf ( hilfsfeld, "%s.%s.partner", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.partner), LL_TEXT, NULL);
			}
// 060307 E
//			plz
			sprintf ( hilfsfeld, "%s.%s.plz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.plz), LL_TEXT, NULL);
			}

// 080709 
//			postfach
			sprintf ( hilfsfeld, "%s.%s.plz_pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.plz_pf), LL_TEXT, NULL);
			}
			sprintf ( hilfsfeld, "%s.%s.pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.pf), LL_TEXT, NULL);
			}

//			str
			sprintf ( hilfsfeld, "%s.%s.str",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld , clipped(adr.str), LL_TEXT, NULL);
			}
//			iln
			sprintf ( hilfsfeld, "%s.%s.iln",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld , clipped(adr.iln), LL_TEXT, NULL);
			}
//			adr 021204
			sprintf ( hilfsfeld, "%s.%s.adr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
				LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_TEXT, NULL);
			}
	// staat	110506
			sprintf ( ptabn.ptwert, "%d", adr.staat );
			sprintf ( ptabn.ptitem ,"staat");
			Ptabn.openptabn() ; 
			if ( ! Ptabn.leseptabn())
			{
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				ptabschreiben ( hJob, "F" , 1 ,hilfsfeld, "staat" ) ;
			}
			break ;
#ifndef FITSMART	// 240507
// 071110
		case FT_FELDEXT : // Ext-Feldbausteine 
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
					extfeldsetzen (hJob, i, hilfsfeld, hilfsfeld2, "F");
			break ;
		

		case FT_TEXTEXT : // Ext-Textbaustein 
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					exttextsetzen (hJob, i, hilfsfeld, "F");
				}
// 220207 : absichtlich mit dem default weiter			break ;
#endif		// 240507
		default :	// Standard-Felder

			if ( tt_feldtyp[i] == FT_TEXTEXT )
			{
				sprintf ( hilfsfeld, "%s.%s_intern",tt_tabnam[i], tt_feldnam[i] );
			}
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				switch ( tt_datetyp[i] )
				{
				case(iDBCHAR):
				case(iDBVARCHAR):	// 280612
					sprintf ( szTemp2 ,"%s",clipped ((char *) tt_sqlval[i]));
//					LlDefineFieldExt(hJob, "a_bas.a_bz1" , "Test", LL_TEXT, NULL);
//					LlDefineFieldExt(hJob, "a_bas.a_bz2" , "Test2", LL_TEXT, NULL);

					LlDefineFieldExt(hJob, hilfsfeld , (LPCSTR)tt_sqlval[i], LL_TEXT, NULL);
					break ;
				case(iDBSMALLINT):
					sprintf ( szTemp2, "%01d", tt_sqllong[i] );
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;

				case(iDBSERIAL):	// 201209
				case(iDBINTEGER):
					sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDECIMAL):
					sprintf ( szTemp2, "%01.4lf", tt_sqldouble[i] );
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDATUM):
					LlDefineFieldExt(hJob, hilfsfeld , sqldatamger ((char *)tt_sqlval[i], szTemp2) , LL_TEXT, NULL);
					break ;
				case(iDBDATETIME):	// 111205
					sqldatdstger ((TIMESTAMP_STRUCT *)tt_sqlval[i], hilfsfeld3,tt_datelen[i]);
					sqldatdsttime ((TIMESTAMP_STRUCT *)tt_sqlval[i], hilfsfeld4,tt_datelen[i]);	// 140214		
					sprintf( szTemp2,"%s %s", hilfsfeld3, hilfsfeld4);	// 140214		
	
// 140214					LlDefineFieldExt(hJob, hilfsfeld , sqldatdstger ((TIMESTAMP_STRUCT *)tt_sqlval[i], szTemp2) , LL_TEXT, NULL);
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2 , LL_TEXT, NULL);
					break ;		

				default :	// schlechte Notbremse
					sprintf ( szTemp2 ,"%s",tt_sqlval[i]);
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_TEXT, NULL);
				break ;
				}
			}	

// 060307 A
			if (! ( strncmp ( clipped( tt_feldnam[i]), "tou", 3 )))
			{
				if ( (strlen ( tt_feldnam[i] )) == 3 ||
							 ! (strncmp(tt_feldnam[i] ,"tou_nr",6)))
				{
					sprintf ( hilfsfeld, "%s.%s_bez", clipped (tt_tabnam[i])
								, clipped( tt_feldnam[i] ));
					if (LlPrintIsFieldUsed(hJob, hilfsfeld))
					{
						switch ( tt_datetyp[i] )
						{
						case(iDBVARCHAR):	// 280612
						case(iDBCHAR):
								tou.tou = atol( clipped ((char *) tt_sqlval[i]));
						break ;

					    case(iDBSERIAL):	// 201209
						case(iDBINTEGER):
						case(iDBSMALLINT):
								tou.tou = tt_sqllong[i] ;
						break ;
						case(iDBDECIMAL):
								tou.tou = (long) tt_sqldouble[i] ;
						break ;
						case(iDBDATUM):
						default :	// schlechte Notbremse
							tou.tou = 0L ;
						break ;
						}
						if ( tou.tou > 0L)
						{
							Tou.opentou() ;
							if ( !Tou.lesetou())
								LlDefineFieldExt(hJob, clipped( hilfsfeld),	clipped(tou.tou_bz), LL_TEXT, NULL);
							else
								LlDefineFieldExt(hJob, clipped( hilfsfeld),	" " , LL_TEXT, NULL);
						}
						else
								LlDefineFieldExt(hJob, clipped( hilfsfeld),	" " , LL_TEXT, NULL);
					}
				}
			}
// 060307 E

			break ;
		} ;	// switch je Feldtyp/Generator

// 150508 A

		if ( tt_range[i] )
		{
			switch ( tt_datetyp[i] )
			{
			case(iDBDATETIME):
			case(iDBDATUM):
			case(iDBCHAR):
			case(iDBVARCHAR):	// 280612
// 220909 A : rangin immer mit String
				sprintf ( hilfsfeld, "rangt.%s.r%s", tt_tabnam[i], tt_feldnam[i] );
				if 	(tt_rangin[i]!= NULL )
				{
					LlDefineFieldExt(hJob, clipped( hilfsfeld),	tt_rangin[i], LL_TEXT, NULL);
				}
				else
				{
					LlDefineFieldExt(hJob, clipped( hilfsfeld) , " ", LL_TEXT, NULL);
				}

				if ( tt_cvrange[tt_range[i]][0] == -1 )	// 220909 :|| ( tt_rangin[i] != NULL) )	// 280508 : rangin unterdruecken
				{
					sprintf ( hilfsfeld, "rangt.%s.v%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								" ", LL_TEXT, NULL);
					sprintf ( hilfsfeld, "rangt.%s.b%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								" ", LL_TEXT, NULL);

					break ;
				}
				if 	( tt_cbrange[tt_range[i]][0] == -1)	// 220909 || ( tt_rangin[i] != NULL) )	// 280508 : rangin unterdruecken
				{
					sprintf ( hilfsfeld, "rangt.%s.v%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								tt_cvrange[tt_range[i]], LL_TEXT, NULL);

					sprintf ( hilfsfeld, "rangt.%s.b%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								tt_cvrange[tt_range[i]], LL_TEXT, NULL);
				}
				else
				{
					sprintf ( hilfsfeld, "rangt.%s.v%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								tt_cvrange[tt_range[i]], LL_TEXT, NULL);

					sprintf ( hilfsfeld, "rangt.%s.b%s", tt_tabnam[i], tt_feldnam[i] );
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								tt_cbrange[tt_range[i]], LL_TEXT, NULL);
					}
				break ;
				;
			case (iDBSMALLINT) :
// 220909 A : rangin immer mit String
				sprintf ( hilfsfeld, "rangt.%s.r%s", tt_tabnam[i], tt_feldnam[i] );
				if 	(tt_rangin[i]!= NULL )
				{
					LlDefineFieldExt(hJob, clipped( hilfsfeld),	tt_rangin[i], LL_TEXT, NULL);
				}
				else
				{
					if (tt_lvrange[tt_range[i]] == tt_lbrange[tt_range[i]])
					{
						sprintf ( szTemp2, "%01d", tt_lbrange[tt_range[i]] );
					}
					else
					{
						if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
							sprintf ( szTemp2 , " " );
						else			
							sprintf ( szTemp2, "%01d-%01d", tt_lvrange[tt_range[i]],tt_lbrange[tt_range[i]] );
					}
					LlDefineFieldExt(hJob, clipped( hilfsfeld) , szTemp2 , LL_TEXT, NULL);
				}
// 220909 E 
				sprintf ( hilfsfeld, "rangt.%s.v%s",tt_tabnam[i], tt_feldnam[i] );
				if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
				// 220909 || ( tt_rangin[i] != NULL ))	// 280508 : rangin unterdruecken
					sprintf ( szTemp2 , "0" );
				else			
					sprintf ( szTemp2, "%01d", tt_lvrange[tt_range[i]] );

				LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);

				sprintf ( hilfsfeld, "rangt.%s.b%s",tt_tabnam[i], tt_feldnam[i] );
				if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
					sprintf ( szTemp2 , "0" );
				else			
					sprintf ( szTemp2, "%01d", tt_lbrange[tt_range[i]] );

				LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;

			case(iDBSERIAL):	// 201209
			case(iDBINTEGER):
// 220909 A : rangin immer mit String
				sprintf ( hilfsfeld, "rangt.%s.r%s", tt_tabnam[i], tt_feldnam[i] );
				if 	(tt_rangin[i]!= NULL )
				{
					LlDefineFieldExt(hJob, clipped( hilfsfeld),	tt_rangin[i], LL_TEXT, NULL);
				}
				else
				{
					if ( tt_lvrange[tt_range[i]] ==  tt_lbrange[tt_range[i]] )
					{
						sprintf ( szTemp2, "%01ld", tt_lvrange[tt_range[i]] );
					}
					else
					{
						if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
							sprintf ( szTemp2 , " " );
					else			
						sprintf ( szTemp2, "%01ld-%01ld", tt_lvrange[tt_range[i]],tt_lbrange[tt_range[i]] );
					}
					LlDefineFieldExt(hJob, clipped( hilfsfeld) , szTemp2, LL_TEXT, NULL);
				}
// 220909 E 
				sprintf ( hilfsfeld, "rangt.%s.v%s", tt_tabnam[i], tt_feldnam[i] );
				if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
				// 220909 || ( tt_rangin[i] != NULL ))	// 280508 : rangin unterdruecken
					sprintf ( szTemp2 , "0" );
				else			
					sprintf ( szTemp2, "%01ld", tt_lvrange[tt_range[i]] );
				LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);

				sprintf ( hilfsfeld, "rangt.%s.b%s",tt_tabnam[i], tt_feldnam[i] );
				if ((tt_lvrange[tt_range[i]] == 11 ) && ( tt_lbrange[tt_range[i]] == -11 ))
					sprintf ( szTemp2 , "0" );
				else			
					sprintf ( szTemp2, "%01ld", tt_lbrange[tt_range[i]] );
				LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;

			case(iDBDECIMAL):
// 220909 A : rangin immer mit String
				sprintf ( hilfsfeld, "rangt.%s.r%s", tt_tabnam[i], tt_feldnam[i] );
				if 	(tt_rangin[i]!= NULL )
				{
					LlDefineFieldExt(hJob, clipped( hilfsfeld),	tt_rangin[i], LL_TEXT, NULL);
				}
				else
				{
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,	" ", LL_TEXT, NULL);
				}
// 220909 E 
				sprintf ( hilfsfeld, "rangt.%s.v%s", tt_tabnam[i], tt_feldnam[i] );
				if ((tt_dvrange[tt_range[i]] == 11.0 ) && ( tt_dbrange[tt_range[i]] == -11.0 ))
				// 220909 || ( tt_rangin[i] != NULL ))	// 280508 : rangin unterdruecken
					sprintf ( szTemp2 , "0.00" );
				else			
					sprintf ( szTemp2, "%01.4f", tt_dvrange[tt_range[i]] );
				LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);

				sprintf ( hilfsfeld, "rangt.%s.b%s", tt_tabnam[i], tt_feldnam[i] );
				if ((tt_dvrange[tt_range[i]] == 11.0 ) && ( tt_dbrange[tt_range[i]] == -11.0 ))
					sprintf ( szTemp2 , "0.00" );
				else			
					sprintf ( szTemp2, "%01.4f", tt_dbrange[tt_range[i]] );
				LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
			}
		}
		// 150508 E

		i ++ ;
	}		// while
#endif	// ifndef OLABEL
#ifdef OLABEL

	int schleife = olaopendaten(1) ;
	while ( schleife > 0 )
	{
		schleife -- ;
		sprintf ( hilfsfeld, "fitet.%s", olafeldname[schleife] );

		if (LlPrintIsFieldUsed(hJob, clipped ( hilfsfeld)))
		{


			if ( olafeldtyp[schleife] == 1 )	// Numerisch
			{
				sprintf ( szTemp2, "%01.4f", atof(olafeldwert[schleife]) );
				LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								szTemp2, LL_NUMERIC, NULL);
			}
			else								// rest alphanum.
			{
				LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								olafeldname[schleife], LL_TEXT, NULL);
			}
		}
	}

#endif
	if (LlPrintIsFieldUsed(hJob, "rangt.nachkpreis"))
	{
		sprintf ( szTemp2, "%d", dnachkpreis );
		LlDefineFieldExt(hJob, "rangt.nachkpreis" , szTemp2, LL_NUMERIC, NULL);
	}

if (LlPrintIsFieldUsed(hJob, "rangt.variante"))
	{
		sprintf ( szTemp2, "%d", dvariante );
		LlDefineFieldExt(hJob, "rangt.variante" , szTemp2, LL_NUMERIC, NULL);
	}

// 270307 
	if (LlPrintIsFieldUsed(hJob, "rangt.sortnr"))
	{
		sprintf ( szTemp2, "%d", externe_sortnr );
		LlDefineFieldExt(hJob, "rangt.sortnr" , szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rangt.Nutzer"))
	{
		sprintf ( szTemp2, "%s", LUser );
		LlDefineFieldExt(hJob, "rangt.Nutzer" , szTemp2, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rangt.Heute")||LlPrintIsFieldUsed(hJob, "rangt.Zeit"))
	{
		time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);
		sprintf ( szTemp2 , "%02d.%02d.%04d",
			ltime->tm_mday ,ltime->tm_mon + 1 ,ltime->tm_year + 1900 );
		LlDefineFieldExt(hJob, "rangt.Heute" , szTemp2, LL_TEXT, NULL);

		sprintf ( szTemp2 , "%02d:%02d:%02d",
			ltime->tm_hour, ltime->tm_min, ltime->tm_sec );
		LlDefineFieldExt(hJob, "rangt.Zeit" , szTemp2, LL_TEXT, NULL);
	}

}



//=============================================================================
void CMainFrame::OnEditList()
//=============================================================================
{
	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
}


//=============================================================================
void CMainFrame::OnPrintLabel()
//=============================================================================
{
	DoLabelPrint();
}


#ifdef OLD_DCL
char *uPPER (string)
char *string;
#else
char *uPPER (char *string)
#endif
{

   for( char *p = string; p < string + strlen( string ); p++ )
   {
      if( islower( *p ) )
			( p[0] = _toupper( *p ) );
     }
   return string ;
}


// CR am Stringende etfernen.

void cr_weg (char *string)
{
 for (; *string; string += 1)
 {
  if (*string == (char) 13)
    break;
  if (*string == (char) 10)
    break;
 }
 *string = 0;
 return;
}
// 301105 : Stringlaenge mit auswerten 
int strupcmpi (char *str1, char *str2, int len, int len2)
{
 short i;
 char upstr1;
 char upstr2;


 if ( len < len2 ) return -1 ;	// 301105
 if ( len > len2 ) return  1 ;	// 301105
 for (i = 0; i < len; i ++, str1 ++, str2 ++)
 {
  if (*str1 == 0)
    return (-1);
  if (*str2 == 0)
    return (1);
  upstr1 = toupper((int) *str1);
  upstr2 = toupper((int) *str2);
  if (upstr1 < upstr2)
  {
   return(-1);
  }
  else if (upstr1 > upstr2)
  {
   return (1);
  }
 }
 return (0);
}

// static char * buffer ;

static char buffer[1024] ;

int next_char_ci (char *string, char tzeichen, int i)
// Naechstes Zeichen != Trennzeichen suchen.

{
       for (;string [i]; i ++)
       {
               if (string[i] != tzeichen)
               {
                                   return (i);
               }
       }
       return (i);
}

short split (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 // if (buffer == (char *) 0) buffer = (char *) malloc (0x1000);
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}



int holedatenausdatei(void)
{
int anz;
char buffer [512];
char token[99] ;	// 160310 : mal von 64 uf 99 erweitert
int gefunden ;
gefunden = 0 ;

// es gibt einen kleinen Haken : falls jemand der Meinung ist,
// er muesse ein rangebehaftestes
// Feld "NAME", "USER", "ANZAHL", "DRUCK" ,"VARIANTE" oder "LABEL" nennen ;-)
// weitere reservierte Worte : "INFORMIX" , "ORACLE" , "MYSQL" 
// 130405 : ALTERNATIVE dazu gebaut
FILE *fp;

    alternat_ausgabe = 0 ;	// 130405

// 140508
	alternat_dir  [0]   =   '\0' ;
	alternat_file [0]   =   '\0' ;
	alternat_type [0]   =   '\0' ;
	alternat_sepachar [0] = '\0' ;
	alternat_framechar[0] = '\0' ;
	alternat_igl[0] = '\0' ;	// 260509
	alternat_show[0] = '\0' ;	// 160310
	alternat_linewrap[0] = '\0' ;	// 160310
	alternat_quiet[0] = '\0' ;	// 160310
	alternat_fsp[0] = '\0' ;	// 250510

	fp = fopen (datenausdateiname, "r");
    if (fp == (FILE *)NULL) return FALSE;
    while (fgets (buffer, 511, fp))
    {
		cr_weg (buffer);
        anz = split (buffer);
        if (anz < 2) continue;


// 130405 
		sprintf ( token, "ALTERNATETYP" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (alternat_type, clipped (uPPER (wort [2])));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}
		
		
// 140508  
		sprintf ( token, "ALTERNATE_FRAMECHAR" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
// Achtung : ohne "uPPER" - 1:1 durchgereicht 
			strcpy (alternat_framechar, clipped (wort [2]));
			gefunden ++ ;
			continue ;
		}

// 140508  
		sprintf ( token, "ALTERNATE_SEPACHAR" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
// Achtung : ohne "uPPER" - 1:1 durchgereicht
			strcpy (alternat_sepachar, clipped (wort [2]));
			gefunden ++ ;
			continue ;
		}



// 260509  
		sprintf ( token, "ALTERNATE_IGL" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (alternat_igl, clipped (wort [2]));
			gefunden ++ ;
			continue ;
		}
// 250510  
		sprintf ( token, "ALTERNATE_FSP" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (alternat_fsp, clipped (wort [2]));
			gefunden ++ ;
			continue ;
		}
// 160310  
		sprintf ( token, "ALTERNATESHOW" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (alternat_show, clipped (wort [2]));
			gefunden ++ ;
			continue ;
		}

// 160310  
		sprintf ( token, "ALTERNATELINEWRAP" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (alternat_linewrap, clipped (wort [2]));
			gefunden ++ ;
			continue ;
		}

// 160310  
		sprintf ( token, "ALTERNATEQUIET" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (alternat_quiet, clipped (wort [2]));
			gefunden ++ ;
			continue ;
		}


// funktioniert vorerst NICHT mit space-Namen
		sprintf ( token, "ALTERNATENAME" ); 
// 301105         if (strupcmpi (wort[1], token, (int)strlen (token)) == 0)
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
       {
			strcpy (alternat_file, clipped (wort [2]));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}

		// funktioniert vorerst NICHT mit space-Namen
		sprintf ( token, "ALTERNATEDIR" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (alternat_dir, clipped (wort [2]));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}

		sprintf ( token, "NAME" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (myform_nr, clipped (uPPER (wort [2])));
			gefunden ++ ;
			continue ;
		}


// 200509
		sprintf ( token, "ORACLE" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			odbdabatyp = FITORACLE ;
//			gefunden ++ ;
			continue ;
		}

// 200509
		sprintf ( token, "INFORMIX" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			odbdabatyp = FITINFORMIX ;
//			gefunden ++ ;
			continue ;
		}
// 200509
		sprintf ( token, "MYSQL" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			odbdabatyp = FITMYSQL ;
//			gefunden ++ ;
			continue ;
		}


	   
// 301105
		sprintf ( token, "MITRANGE" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			mitrange = atoi( wort[2]) ;
//			gefunden ++ ;
			continue ;
		}

// 110309 
		sprintf ( token, "MITVARIANTE" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			dvariante = atoi( wort[2]) ;
			continue ;
		}

// 280307
		sprintf ( token, "MITSORTNR" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			externe_sortnr = atoi( wort[2]) ;
//			gefunden ++ ;
			continue ;
		}


// 121104
		sprintf ( token, "USER" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (User, clipped (uPPER (wort [2])));
			strcpy (LUser, clipped (wort [2]));	// 220107

			gefunden ++ ;
			continue ;
		}

		sprintf ( token, "ANZAHL" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
       {

			danzahl = atoi (wort[2]);
			gefunden ++ ;
			continue ;
        }

		sprintf ( token, "LABEL" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
       {
			GlobalLila = atoi( wort[2]) ;
			gefunden ++ ;
			continue ;
		}
		sprintf ( token, "DRUCK" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
       {

			DrMitMenue = atoi (wort[2]);
			gefunden ++ ;
			continue ;
        }

	}
	fclose (fp);
	if ( gefunden ) return TRUE;

	return FALSE ;
}

// Nicht besonders elegant, aber es funktioniert ....
int holerangesausdatei(char * token, char * wert1, char * wert2)
{
int anz;
char buffer [1040];	// 280508 : bisher 599
FILE *fp;

	wert1[0] = '\0' ;
	wert2[0] = '\0' ;
	fp = fopen (datenausdateiname, "r");
    if (fp == (FILE *)NULL) return FALSE;
    while (fgets (buffer, 1035, fp))	// 280508 : 299-> 1035
    {
		cr_weg (buffer);
        anz = split (buffer);
        if (anz < 2) continue;

        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
       {
			strcpy (wert1, clipped (wort [2]));
			if ( anz == 2 )	// 200406 : sonst knallts bei bugs : duplizieren
				strcpy (wert2, clipped (wort [2]));
			else			// NormalZustand entsprechend bisheriger source
			strcpy (wert2, clipped (wort [3]));
			return TRUE ;
		}
	}
	fclose (fp);
	return FALSE ;
}

char * anfuehrweg ( char * quellstr )
{
	// hier  duerfen nur gueltige und zugriffige Strings herein kommen

	int k = 0 ;
//	int l = 0 ;
	int i = (int) strlen ( quellstr ) ;

	for ( int j = 0 ; ( j + k ) <= i ; j++ )
	{
		if   ( quellstr[j + k ] == '"' )
		{
			k ++ ;
			while ( quellstr[ j + k ] == '"')
			{
				k ++ ;
			}
		}
		quellstr[j] = quellstr[ j + k ] ;

	}
	return quellstr ;
}
// Die Commandline hat (020506) folgendes fixes Format wegen der fixen Funktionen :
// dr70001o.exe -name ETIKETTNAME -datei PARADATEINAME -anzahl ANZAHL -dr 1|0  -TYP 51|52
// Achtung : 51 : Schleife, 52 : Alle auf einmal 
// LABEL=1  und gegbnenfalls Anzahl = 1 werden fix belegt
//=============================================================================
void CMainFrame::InterpretCommandLine()
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");

    char *px;
	char p[299] ;
//	EntwurfsModus = 1 ;


	sqldatset () ;	// 121205

	EntwurfsModus = 0 ;
	GlobalLila = 0 ;
#ifdef OLABEL
	GlobalLila = 1 ;
#endif

	datenausdatei = 0 ;		// 300904
	mitrange      = 0 ;		// 301105

	danzahl = 1 ;			// 121104
	User[0] = '\0' ;		// 121104
	LUser[0] = '\0' ;		// 220107

#ifdef OLABEL
	// 220711 
		char *  puserchar = getenv ( "OLAUSER" ) ;
		if (puserchar != NULL)
			sprintf ( User,"%s",puserchar ) ;

#endif
	
	Token.NextToken ();
	while ((px = Token.NextToken ()) != NULL)
	{
		sprintf ( p ,"%s", uPPER(px) );
		if (strcmp (p, "-H") == 0)
		{
#ifdef OLABEL
		MessageBox ("080506 : usage dr70001o -name xxxxXXXX -datei DATEINAMEabsolut [-anzahl X] [TYP 51|52] ",
				"oder : dr70001o -name xxxxXXXX -datei DATEINAMEabsolut ",
                                MB_OK);
#else

			MessageBox ("260106 : usage dr70001 -name xxxxXXXX [-dr 0|1][-l][-wahl] ",
				"oder : dr70001 -datei DATEINAMEabsolut ",
                                MB_OK);
#endif	// OLABEL

			EntwurfsModus = 0 ;
              return;
		}

// einfach "ZEITTEST" in die Kommandozeile eintragen

		if (strcmp (p, "ZEITTEST") == 0)
		{
			zeittest = 1  ;
			continue ;
		}



		if (strcmp (p, "ORACLE") == 0)
		{
			odbdabatyp = FITORACLE ;
			continue ;
		}

		if (strcmp (p, "INFORMIX") == 0)
		{
			odbdabatyp = FITINFORMIX ;
			continue ;
		}

		if (strcmp (p, "MYSQL") == 0)
		{
			odbdabatyp = FITMYSQL ;
			continue ;
		}

	// 300904
		if (strcmp (p, "-DATEI") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( datenausdateiname ,"%s", clipped(uPPER(px)));
			if ( strlen ( datenausdateiname ) > 1 )
			{
				EntwurfsModus = 0 ;
				// Achtung : klappt nicht bei Namen mit ner Luecke ......
				sprintf ( datenausdateiname ,"%s", anfuehrweg( datenausdateiname )) ;
#ifdef OLABEL
				datenausdatei = 1 ;
#else
				if ( holedatenausdatei())
				{
					datenausdatei = 1 ;
					break ;
				}
#endif
			}
		}

// 220107
		// Achtung : wirkt meist nur korrekt, falls als erster Parameter gesendet
		// wird evtl. durch Daten aus Datei ueberschrieben
		if (strcmp (p, "-NUTZER") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( User, "%s" ,clipped(uPPER (px))) ;
			sprintf ( LUser, "%s" ,clipped( px )) ;
			continue ;	// so ist es besser 200509
		}

// 220107


		if (strcmp (p, "-NAME") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
  			sprintf ( myform_nr ,"%s", clipped ( uPPER (px)));
			EntwurfsModus = 0 ;
//			return ;
			continue ;	// so ist es besser 200509
		}

// 170105 A
// Nur Druckerwahl, Name ist ausserdem Pflicht
		if (strcmp (p, "-WAHL") == 0)
		{
			WahlModus = 1 ;
			continue ;	// so ist es besser 200509 
		}
// 170105 E

		if (strcmp (p, "-DR") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            DrMitMenue = (atoi (px));
			EntwurfsModus = 0 ;
			continue ;	// 200509 : so ist es besser
		}
// Label verwenden
		if (strcmp (p, "-L") == 0)
		{
			GlobalLila = 1 ;
			continue ;	// 200509 : so ist es besser
		}

// 190608 : bissel brutal
		if (strcmp (p, "-TYP") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            xxx = (atoi (px));
			if ( xxx == 52 )
			xxx = 1 ;
			else xxx = 0 ;
			continue ;	// so ist es besser
		}


		if (strcmp (p, "-ANZAHL") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            danzahl = (atoi (px));
// gar nix ist gar nix 151009
			if ( danzahl == 0 )
				danzahl = 1 ;
#ifdef OLABEL
			olabelanzahl = danzahl ;
#endif
			continue ;	// so ist es besser 200509 
		}

	}

	SetWindowText ( myform_nr) ;	// 051007 


}


//=============================================================================
void CMainFrame::OnPrintReport()
//=============================================================================
{
    
	InterpretCommandLine() ;

	if ( !GlobalLila )
	    DoListPrint();
	else
		DoLabelPrint();
}

//=============================================================================
void CMainFrame::DoLabelPrint()
//=============================================================================
{
// 150813 : szTemp2 von 505 auf 2505 erweitert 
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lbl", szTemp[50], szTemp2[2505] /*, szBoxText[200] */;
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;

	strcpy(szTemp, " ");
//	char *etc;
	char filename[512];

	CString sMedia;

	char envchar[512];

#ifdef OLABEL
	// 220711 : Alternativen eingebaut zwecks user-Steuerung
	if ( User[0] == '\0' )
		sprintf (filename, "%s\\format\\LuLabel\\%s.lbl", odbbasisverz, myform_nr);	// So war es bisher
	else
		sprintf (filename, "%s\\format\\LuLabel\\%s\\%s.lbl", odbbasisverz,User, myform_nr);
#else
	if ( User[0] == '\0' )
		sprintf (filename, "%s\\format\\LL\\%s.lbl", odbbasisverz, myform_nr);
	else
		sprintf (filename, "%s\\format\\LL\\%s\\%s.lbl", odbbasisverz,User, myform_nr);
#endif	// OLABEL

    FILE *fp ;
	fp = fopen ( filename, "r" ); 
	if ( fp== NULL )
	{
		User[0] = '\0' ;

#ifdef OLABEL
		sprintf (filename, "%s\\format\\LuLabel\\%s.lbl", odbbasisverz, myform_nr);
#else
		sprintf (filename, "%s\\format\\LL\\%s.lbl", odbbasisverz, myform_nr);
#endif
	}
	else fclose ( fp) ;

// 121104 E

	FILENAME_DEFAULT = clipped (filename);

	if ( strlen(myform_nr) < 4 || strlen( myform_nr) > 10 )
	{
#ifdef OLABEL
		MessageBox("Ung�ltiger Labelname!", myform_nr,
										MB_OK|MB_ICONSTOP);
#else
		MessageBox("Ung�ltiger Labelname!", myform_nr,
										MB_OK|MB_ICONSTOP);
#endif
		return;
    }

// QQQQQQQ Labelprint

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
#ifdef OLABEL
		MessageBox("Job can't be initialized!", "dr70001-Exception 010", MB_OK|MB_ICONSTOP);
#else
		MessageBox("Job can't be initialized!", "dr70001-Exception 010", MB_OK|MB_ICONSTOP);
#endif	// OLABEL

		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{

#ifdef OLABEL
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"dr70001-Exception 020",
					MB_OK|MB_ICONSTOP);
#else
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"dr70001-Exception 020",
					MB_OK|MB_ICONSTOP);
#endif	// OLABEL

		return;
    }

	// Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
	if ( zeittest ) schreibezeit("Vor Lizenz \n" ) ;

#ifdef LL19
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "V2XSEQ");
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "ht8gOw");
#else				// LL11 als default bis 120911
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");
#endif
#endif
	if ( zeittest ) schreibezeit("nach Lizenz \n" ) ;

	// CELL 11 : das war lizenz fuer LuL09  : 	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");

// QQQQQQQ Labelprint

#ifdef OLABEL
	sprintf ( envchar ,"%s\\format\\LuLabel\\" , odbbasisverz);
	if ( User[0] == '\0' )	// 250711 mea culpa
		sprintf ( envchar ,"%s\\format\\LuLabel\\" , odbbasisverz);
	else
		sprintf ( envchar ,"%s\\format\\LuLabel\\%s\\" , odbbasisverz, User);

#else
	sprintf ( envchar ,"%s\\format\\LL\\" , odbbasisverz);
	if ( User[0] == '\0' )	// 121104
		sprintf ( envchar ,"%s\\format\\LL\\" , odbbasisverz);
	else
		sprintf ( envchar ,"%s\\format\\LL\\%s\\" , odbbasisverz, User);
#endif

	LlSetPrinterDefaultsDir(hJob,envchar) ;	// Printer-Datei

	char * penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// Vorschau-Datei

	//GR: Exporter aktivieren

#ifdef LL19
// am besten alles weg 	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll19ex.llx");
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll12ex.llx");
#else
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");
#endif
#endif
	// US: Choose new expression mode with formula support
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);
	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);
	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog

// QQQQQQQ Labelprint
   	
	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
   		{
	    	if (nRet != LL_ERR_USER_ABORTED)
#ifdef OLABEL
				MessageBox("Error While Printing", "dr70001o-Exception 030",
				MB_OK|MB_ICONEXCLAMATION);
#else
				MessageBox("Error While Printing", "dr70001-Exception 030",
				MB_OK|MB_ICONEXCLAMATION);
#endif
			LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	
			return;
		}
	}

// QQQQQQQ Labelprint

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);
#ifndef OLABEL
#ifdef FITSMART
	dbClass.opendbase ("fsm");
#else
#ifdef FITODBC
		if ( strlen( odb_daba) > 0 )
		{
			dbClass.opendbase ( odb_daba );
		}
		else
			dbClass.opendbase ("bws");

#else
	dbClass.opendbase ("bws");
#endif
#endif

#ifdef FITODBC
	CLlSampleView *pView = (CLlSampleView *) GetActiveView();
//	CLlSampleDoc* pDoc = pView->GetDocument();
	pDoc = pView->GetDocument();
	ASSERT_VALID(pDoc);
	pDoc->OnOpenDocument() ;
#endif


	Temptab.structprep(myform_nr) ;
#endif	// ifndef OLABEL

    Variablendefinition (hJob);
//    Felderdefinition (hJob);

    // GR: Druck starten

// QQQQQQQ Labelprint

// 180105 A

	if ( WahlModus )	// 171204
	{
		int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LABEL, szFilename) ;	// 140408 : ab heute funzt das korrekt
		LlPrintEnd(hJob,0);
		LlJobClose(hJob);
#ifndef OLABEL
#ifdef FITSMART
		dbClass.closedbase ("fsm");
#else
		dbClass.closedbase ("bws");
#endif
#endif // ifndef OLABEL

		if (Hauptmenue == 0)  
		{
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		}
		if ( i < 0 ) return;
		return ;
	}

    if ( DrMitMenue > 0 )	// 220413 : Mehrwertig auswerten
    {
		int retc1x = 0 ;
		if ( DrMitMenue == 2 )
		{


				retc1x = LlPrintWithBoxStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...");
					LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, "PRV");
	
		}
		else
		{
			retc1x = LlPrintWithBoxStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...");
		}
		if ( retc1x < 0 )
		{
			sprintf ( dmess, " dr70001-Exception : %d", retc1x );
			MessageBox("Error While Printing", dmess , MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
#ifndef OLABEL
			Temptab.drfree();
#ifdef FITSMART
		dbClass.closedbase ("fsm");
#else
			dbClass.closedbase ("bws");
#endif
#endif	// ifndef OLABEL

			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

// QQQQQQQ Labelprint

	else	// Druck pur immer auf den Drucker
	{
		int retc2 = LlPrintStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_NORMAL,
							(int) NULL /* dummy */);

/* --->
	if (LlPrintStart(hJob, LL_PROJECT_LABEL, szFilename,LL_PRINT_NORMAL,(int) NULL ) < 0)
< ---- */
		if ( retc2 < 0 )
		{
			sprintf ( dmess, " dr70001-Exception : %d", retc2 );
	
			MessageBox("Error While Printing", dmess, MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);

#ifndef OLABEL
			Temptab.drfree() ;
#ifdef FITSMART
		dbClass.closedbase ("fsm");
#else

			dbClass.closedbase ("bws");
#endif
#endif	// ifndef OLABEL

			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
   } 

// QQQQQQQ Labelprint

	// GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	if ( ! xxx )		// 190608
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);

// QQQQQQQ Labelprint


//	if ( DrMitMenue == 1)	220413
	if ( DrMitMenue > 0 )
    {
		if (LlPrintOptionsDialog(hJob, hWnd, "Drucker-Einrichtung") < 0)
		{
			LlPrintEnd(hJob,0);
			LlJobClose(hJob);


#ifndef OLABEL
			Temptab.drfree() ;
#ifdef FITSMART
		dbClass.closedbase ("fsm");
#else
		    dbClass.closedbase ("bws");
#endif
#endif	// i�fndef OLABEL

			if (Hauptmenue == 0)
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
	        return;
		}
		//GR: Druckziel abfragen
		LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		sMedia.ReleaseBuffer();
    }
	else
	{
		//GR: Druckziel abfragen
		LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		sMedia.ReleaseBuffer();
	}

// QQQQQQQ Labelprint

//	LlPrintCopyPrinterConfiguration (hJob, szneupfilename, , LL_PRINTERCONFIG_SAVE );
//	LlPrintCopyPrinterConfiguration (hJob, szpfilename, , LL_PRINTERCONFIG_RESTORE );


	char szPrinter[80], szPort[20];
	int  nRecCount = 30, nErrorValue = 0, nRecno, nLastPage;

// QQQQQQQ Labelprint

	LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

    nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;


	// GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.

#ifdef OLABEL
	int abbruch = 0 ;	// einigermassen dummy

#else	// OLABEL

	int abbruch = Temptab.opentemptab();
#endif	// OLABEL

	if ( abbruch == -3 ) 
    {
//		sprintf (dmess, "User-Abbruch ");
//		MessageBox (dmess, "", MB_ICONERROR);
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
#ifndef OLABEL
			Temptab.drfree () ;
#ifdef FITSMART
			dbClass.closedbase ("fsm");
#else
			dbClass.closedbase ("bws");
#endif
#endif		// ifndef OLABEL

			if (Hauptmenue == 0)  //#LuD
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
    		return;

	}

#ifdef OLABEL
	int anzahl = olabelanzahl ;
	int di     = 0 ;
#else

	int anzahl = Temptab.getanzahl() ;
	if ( ! dialoggelaufen )
		anzahl = danzahl ;
// QQQQQQQ Labelprint

	int di = Temptab.lesetemptab (0);   //       1.DATENSATZ lesen f�r Variablen�bergabe
											// nErrorValue als Paramter ist hier nicht gut
#endif
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);

#ifndef OLABEL
				Temptab.drfree () ;
#ifdef FITSMART
				dbClass.closedbase ("fsm");
#else
				dbClass.closedbase ("bws");
#endif
#endif
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}

// QQQQQQQ Labelprint


	if ( xxx )	// 190608 - das muss dann der Anwender schon selbst entscheiden 
	{
		int i = LlPrintSetOption(hJob, LL_PRNOPT_COPIES, anzahl);
		i = LlPrintGetOption(hJob, LL_PRNOPT_COPIES_SUPPORTED);

	}

	while ( ( !di )
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA))
   	{

		if (xxx )	// 190608 : Variante neu dazu 
		{
				nErrorValue = LL_WRN_REPEAT_DATA ;	// 280708 : so geht die schleife korrekt ueber den seiteumbruch hinaus 
													// ( sollte es aber hier nicht geben, sondern nur die paar Zeilen weiter unten ?!) 
								// 200409 : Zuweisung korrekt
				while ( nErrorValue == LL_WRN_REPEAT_DATA )
				{
					VariablenUebergabe ( hJob, szTemp2, nRecno , anzahl, anzahl );	
					nErrorValue = LlPrint(hJob);
				}
		}
		else
		{
			for ( int dox = 0 ; dox < anzahl ; dox ++ )
			{											// 030408 dox und anzahl dazu ...

				nErrorValue = LL_WRN_REPEAT_DATA ;	// 280708 : so geht die schleife korrekt ueber den seiteumbruch hinaus 
								// 200409 : Zuweisung korrekt( alt : "==" neu : "=" )

				while ( nErrorValue == LL_WRN_REPEAT_DATA )
				{
					VariablenUebergabe ( hJob, szTemp2, nRecno , dox, anzahl );	// Variablen vor derListe .....
/* ---->
						// Labelprint : Prozentbalken in der Fortschritts-Anzeige setzen
    						sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    						nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
							if (nErrorValue == LL_ERR_USER_ABORTED)
    						{
    							LlPrintEnd(hJob,0);
    							LlJobClose(hJob);
    							return;
    						}
< ---- */

					nErrorValue = LlPrint(hJob);
				}
			}
		}
#ifdef OLABEL
		di = 100 ;
#else
		di = Temptab.lesetemptab (0) ;	//       DATENSATZ lesen
#endif		// OLABEL
	}

// QQQQQQQ Labelprint
	//GR: Druck beenden
	LlPrintEnd(hJob,0);
	if ( xxx )
	{
		LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 1);
		LlPrintGetOption(hJob, LL_PRNOPT_COPIES_SUPPORTED);
	}

	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) l�schen
        LlPreviewDisplay(hJob, szFilename, penvchar, hWnd);

        LlPreviewDeleteFiles(hJob, szFilename,penvchar);
    }

	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);

#ifndef OLABEL
	Temptab.drfree();
#ifdef FITSMART
	dbClass.closedbase ("fsm");
#else
	dbClass.closedbase ("bws");
#endif
#endif		// ifndef OLABEL

	if (Hauptmenue == 0)  
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}
}

//=============================================================================
void CMainFrame::DoListPrint()
//=============================================================================
{
	// 150813 : szTemp2 von 505 auf 2505
    HWND hWnd = m_hWnd;
    char szFilename[128+1] = "*.lst", szTemp[50], szTemp2[2505]/*, szBoxText[200]*/ ;
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;
	strcpy(szTemp, " ");
//	char *etc;
	char filename[512];

	CString sMedia;

	char envchar[512];	


	if ( User[0] == '\0' )
        sprintf (filename, "%s\\format\\LL\\%s.lst", odbbasisverz, myform_nr);
	else
        sprintf (filename, "%s\\format\\LL\\%s\\%s.lst", odbbasisverz,User, myform_nr);

    FILE *fp ;
	fp = fopen ( filename, "r" ); 
	if ( fp== NULL )
	{
		User[0] = '\0' ;
		sprintf (filename, "%s\\format\\LL\\%s.lst", odbbasisverz, myform_nr);
	}
	else fclose ( fp) ;

// 121104 E


	FILENAME_DEFAULT = clipped (filename);
	if ( strlen(myform_nr) < 4 || strlen( myform_nr) > 10 )
	{
		MessageBox("Ung�ltiger Listenname!", myform_nr, MB_OK|MB_ICONSTOP);
		return;
    }

	// GR: Initialisieren von List & Label.

	if ( zeittest ) schreibezeit("Vor JobOpen \n" ) ;
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "dr70001-Exception 010", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"dr70001-Exception 020",
					MB_OK|MB_ICONSTOP);
		return;
    }


 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
// CELL11 : fuer LuL.90 :	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw")
	if ( zeittest ) schreibezeit("Vor Lizenz \n" ) ;

#ifdef LL19
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "V2XSEQ");
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "ht8gOw");
#else		// LL11 als default bis 120911

	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");
#endif
#endif

	if ( zeittest ) schreibezeit("Nach Lizenz \n" ) ;

	sprintf ( envchar ,"%s\\format\\LL\\" , odbbasisverz);

	if ( User[0] == '\0' )	// 121104
		sprintf ( envchar ,"%s\\format\\LL\\" , odbbasisverz);
	else
		sprintf ( envchar ,"%s\\format\\LL\\%s\\" , odbbasisverz, User);

	LlSetPrinterDefaultsDir(hJob,envchar) ;	// Printer-Datei
	
	char * penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// Vorschau-Datei

	
		//GR: Exporter aktivieren

#ifdef LL19
// am besten alles weg .....	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll19ex.llx");
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll12ex.llx");
#else
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");
#endif
#endif
	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);


	LlSetOption(hJob, LL_OPTION_NOMAILVARS, TRUE);	// 250313



	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );


	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
	   	{
    		if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "dr70001-Exception 030", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	
			return;
		}
	}

	if ( zeittest ) schreibezeit("jetzt geht es los -init vars \n" ) ;

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	if ( zeittest ) schreibezeit("vor dabastart \n" ) ;

#ifndef OLABEL
#ifdef FITSMART
	dbClass.opendbase ("fsm");
#else
#ifdef FITODBC
		if ( strlen( odb_daba) > 0 )
		{
			dbClass.opendbase ( odb_daba );
		}
		else
			dbClass.opendbase ("bws");

#else
	dbClass.opendbase ("bws");
#endif
#endif

#ifdef FITODBC
	CLlSampleView *pView = (CLlSampleView *) GetActiveView();
//	CLlSampleDoc* pDoc = pView->GetDocument();
	pDoc = pView->GetDocument();
	ASSERT_VALID(pDoc);
	pDoc->OnOpenDocument() ;
#endif

	if ( zeittest ) schreibezeit("nach dabastart \n" ) ;

	Temptab.structprep(myform_nr) ;

	if ( zeittest ) schreibezeit("nach strukturaufbau \n" ) ;

// 200308 
#ifndef FITSMART
    if ( odbdabatyp != FITORACLE )	// ORACLE hat sogar mit dem Name "sys_par" ein Problem ....
	{
		// fitsmart kennt diesen Systemparameter nicht ....
		sprintf ( sys_par.sys_par_nam,"nachkpreis" );
		sys_par.sys_par_wrt[0] = '\0' ;
		Sys_par.opensys_par();
		dnachkpreis = Sys_par.lesesys_par();
		if ( dnachkpreis || sys_par.sys_par_wrt[0] == '\0')
		{
			dnachkpreis = 0 ;
		}
		else
		{
			dnachkpreis = 0;
			dnachkpreis = atoi ( sys_par.sys_par_wrt ) ;
		}
	}
#endif
	if ( dnachkpreis < 2 || dnachkpreis > 4 ) dnachkpreis = 2 ;

// 200308 E




#endif	// ifndef OLABEL

    Variablendefinition (hJob);
    Felderdefinition (hJob);

	if ( zeittest ) schreibezeit("nach V+F-Definitionen \n" ) ;


    // GR: Druck starten


// 180105 A
	if ( WahlModus )	// 171204
	{
		int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
		LlPrintEnd(hJob,0);
		LlJobClose(hJob);
#ifndef OLABEL
#ifdef FITSMART
		dbClass.closedbase ("fsm");
#else
		dbClass.closedbase ("bws");
#endif
#endif	// ifndef OLABEL
		if (Hauptmenue == 0)  
		{
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		}
		if ( i < 0 ) return;
		return ;
	}

// 180105 E

// 151009
	int anzahlerlaubt = 0 ;
	char * p =  get_default("anzahlerlaubt");
	char hilfstr [199] ;
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		anzahlerlaubt = atoi ( hilfstr ) ;
	} ;

	if ( ! DrMitMenue )
	{
		char * p =  get_default("DrMitMenue");
		char hilfstr [199] ;
		if ( p != NULL )
		{
			sprintf ( hilfstr ,"%s", p );
			DrMitMenue = atoi ( hilfstr ) ;
		}
		if ( DrMitMenue != 0 && DrMitMenue != 1 && DrMitMenue != 2 )	// 220413 != 2 dazu
			DrMitMenue = 0 ;
	} ;

    if ( DrMitMenue > 0 )

    {
/* ->
		if (LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...") < 0)
< --- */

		if ( zeittest ) schreibezeit("vor boxstart \n" ) ;
		int retc1x  = 0 ;

		if ( DrMitMenue == 2 )	// 220413
		{
			retc1x = LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...");
				LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, "PRV");

		}
		else
		{
			retc1x = LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...");
		}
		if ( retc1x < 0 )
		{	// Errormeldung schoener 240608 
			sprintf ( dmess, " dr70001-Exception %s : %d", myform_nr ,retc1x );
			MessageBox( dmess, "Error While Printing", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
#ifndef OLABEL
			Temptab.drfree();
#ifdef FITSMART
			dbClass.closedbase ("fsm");
#else
			dbClass.closedbase ("bws");
#endif
#endif	// ifndef OLABEL

			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	// GrJ 040204

			return;
		}

		if ( zeittest ) schreibezeit("nach boxstart \n" ) ;

	}
	else	// Druck pur immer auf den Drucker
	{
		int retc2 = -1 ;


		// TESTESTEST
		if ( alternat_ausgabe )
		{


			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.Path",alternat_dir);
			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.File",alternat_file);
			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);

			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
 

			if ( ! strcmp ( alternat_type , "PDF") )
			{
// 080905 : Mit diesen Parametern ergibt sich je nach Vorlage ein Reduktionsfaktor 5 .. 10 gegenueber
// der default-Parametrierung beim Einsatz von Logos und RTF-Texten 				
// Performance-Fresser bleibt RTF-Text, Platz-Fresser bleiben bitmaps und Bilder
				                                                                                             // min defa max
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}
// 140508 
			if ( ! strcmp ( alternat_type , "TXT") )
			{
				if ((strlen ( alternat_framechar)) > 0 )
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "TXT.FrameChar", alternat_framechar );		// default : '"' ; NONE,''','"'		
				if ((strlen ( alternat_sepachar)) > 0 )
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "TXT.SeparatorChar", alternat_sepachar );	// default : ',' ; NONE,TAB.BLANK,',',';'

// 260509
				if ((strlen ( alternat_igl)) > 0 )
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "TXT.IgnoreGroupLines", alternat_igl );	// default : '1' grouplines weren ignoriert 
			}

// 160310 A
			if ( ! strcmp ( alternat_type , "XLS") )
			{
				if ((strlen( alternat_linewrap)) > 0 )	// default ist "1" d.h. ignorieren, wirkt aber nur bei Export.OnlyTabledata
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "XLS.IgnoreLinewrapForDataOnlyExport", alternat_linewrap );
				if ((strlen( alternat_show )) > 0 )		// default ist "0" d.h. nicht anstarten
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.ShowResult", alternat_show );
				if ((strlen ( alternat_igl)) > 0 )	// default ist "1" d.h. ignorieren , wirkt aber nur bei Export.OnlyTabledata
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "XLS.IgnoreGroupLines", alternat_igl );	// default : '1' grouplines weren ignoriert
// 250510
				if ((strlen ( alternat_fsp)) > 0 )	// default ist "89" Schriftgroesse wird entsprechend geschrumpelt
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "XLS.FontScalingPercentage", alternat_fsp );	// default : '89' 


			}

// 160310 E 

			retc2 = LlPrintStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							(int) NULL );

		}
		else
		{
		if ( zeittest ) schreibezeit("vor printstart \n" ) ;
			retc2 = LlPrintStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_NORMAL,
							(int) NULL /* dummy */);
		if ( zeittest ) schreibezeit("nach printstart \n" ) ;

		}
		if ( retc2 < 0 )
		{	// 240608 : Fehlermeldung schoener
			sprintf ( dmess, " dr70001-Exception %s : %d", myform_nr, retc2 );
			MessageBox(dmess, "Error While Printing", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);

#ifndef OLABEL
			Temptab.drfree() ;
#ifdef FITSMART
			dbClass.closedbase ("fsm");
#else
			dbClass.closedbase ("bws");
#endif
#endif	// ifndef OLABEL


			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	// GrJ 040204
			return;
		
		}
   } 
	

    // GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog


	if ( danzahl < 1 ) danzahl = 1 ;	// 301109 : Notbremse

	if ( ( !alternat_ausgabe && anzahlerlaubt && ! DrMitMenue ) || (danzahl > 1 ))	// 151009 
	{
		int i = LlPrintSetOption(hJob, LL_PRNOPT_COPIES, danzahl);
		i = LlPrintGetOption(hJob, LL_PRNOPT_COPIES_SUPPORTED);
	}

//	if ( alternat_ausgabe || ! anzahlerlaubt )	// 151009 
	if ( alternat_ausgabe )	// 031109 : eigentlich immer erlaubt ( im Dialog ) 
	{
		LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
	}

	LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);


// 220413	if ( DrMitMenue == 1)
 	if ( DrMitMenue > 0 )
    {
	
		if (LlPrintOptionsDialog(hJob, hWnd, "Drucker-Einrichtung") < 0)
		{
			LlPrintEnd(hJob,0);
			LlJobClose(hJob);

#ifndef OLABEL
			Temptab.drfree() ;
#ifdef FITSMART
			dbClass.closedbase ("fsm");
#else
			dbClass.closedbase ("bws");
#endif
#endif		// ifndef OLABEL


			if (Hauptmenue == 0)
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
	        return;
		}
		//GR: Druckziel abfragen
		LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		sMedia.ReleaseBuffer();
    }
	else
	{
		//GR: Druckziel abfragen

		// TESTESTEST
//		int ierrc ;
		if ( alternat_ausgabe )
		{
			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);

			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Path", alternat_dir);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.File", alternat_file);

			if (( strlen ( alternat_quiet )) > 0 )	// 160310 : ab heute parametrierbar
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Quiet", alternat_quiet);
			else
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Quiet", "1");

//			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_PRINTDST_FILENAME, alternat_file);
			if ( ! strcmp ( alternat_type , "PDF") )
			{
// 080905 : Mit diesen Parametern ergibt sich je nach Vorlage ein Reduktionsfaktor 5 .. 10 gegenueber
// der default-Parametrierung beim Einsatz von Logos und RTF-Texten 				
// Performance-Fresser bleibt RTF-Text, Platz-Fresser bleiben bitmaps und Bilder
				                                                                                             // min defa max
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}
// 140508 
			if ( ! strcmp ( alternat_type , "TXT") )
			{
				if ((strlen ( alternat_framechar)) > 0 )
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "TXT.FrameChar", alternat_framechar );		// default : '"' ; NONE,''','"'		
				if ((strlen ( alternat_sepachar)) > 0 )
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "TXT.SeparatorChar", alternat_sepachar );	// default : ',' ; NONE,TAB.BLANK,',',';'
// 260509
				if ((strlen ( alternat_igl)) > 0 )
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "TXT.IgnoreGroupLines", alternat_igl );	// default : '1' grouplines weren ignoriert 

			}
// 160310 A
			if ( ! strcmp ( alternat_type , "XLS") )
			{
				if ((strlen( alternat_linewrap)) > 0 )	// default ist "1" d.h. ignorieren, wirkt aber nur bei Export.OnlyTabledata
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "XLS.IgnoreLinewrapForDataOnlyExport", alternat_linewrap );
				if ((strlen( alternat_show )) > 0 )		// default ist "0" d.h. nicht anstarten
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.ShowResult", alternat_show );
				if ((strlen ( alternat_igl)) > 0 )	// default ist "1" d.h. ignorieren , wirkt aber nur bei Export.OnlyTabledata
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "XLS.IgnoreGroupLines", alternat_igl );	// default : '1' grouplines weren ignoriert 
// 250510
				if ((strlen ( alternat_fsp)) > 0 )	// default ist "89" Schriftgroesse wird entsprechend geschrumpelt
					LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "XLS.FontScalingPercentage", alternat_fsp );	// default : '89' 


			}

// 160310 E 

		}
		else
		{
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		}
		sMedia.ReleaseBuffer();
	}

//	LlPrintCopyPrinterConfiguration (hJob, szneupfilename, , LL_PRINTERCONFIG_SAVE );
//	LlPrintCopyPrinterConfiguration (hJob, szpfilename, , LL_PRINTERCONFIG_RESTORE );


	//GR: Druckziel abfragen
//	CString sMedia;
//	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
//	sMedia.ReleaseBuffer();

    int  nRecCount = 1, nErrorValue = 0, nPrintFehler = 0, nLastPage, nRecno;
	char szPrinter[80], szPort[20];

	if ( zeittest ) schreibezeit("vor  printerinfo \n" ) ;

    LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

	if ( zeittest ) schreibezeit("nach  printerinfo \n" ) ;

	nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);

	nRecno = 1;

    // GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.

//  spaeter evtl .aml : zu aufwendig 	nRecCount = GetRecCount () ;

//	sprintf (dmess, "nRecCount = %hd",nRecCount);  //050803
//	MessageBox(dmess, "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
#ifdef OLABEL
	int abbruch = 3 ;	// eigentlich nur Syntax-Dummy
#else

	if ( zeittest ) schreibezeit("vor  erster fetch \n" ) ;

	int abbruch = Temptab.opentemptab();
	if ( zeittest ) schreibezeit("nach  erster fetch \n" ) ;

#endif	// OLABEL
	if ( abbruch == -3 ) 
    {
//		sprintf (dmess, "User-Abbruch ");
//		MessageBox (dmess, "", MB_ICONERROR);
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
#ifndef OLABEL
			Temptab.drfree () ;
#ifdef FITSMART
			dbClass.closedbase ("fsm");
#else
			dbClass.closedbase ("bws");
#endif
#endif		// OLABEL
			if (Hauptmenue == 0)  //#LuD
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
    		return;

	}

#ifdef OLABEL
	int di = 100 ;	// z.Z. nur ein Syntax-Dummy 
#else
		int di = Temptab.lesetemptab (0);   //       1.DATENSATZ lesen f�r Variablen�bergabe
#endif										// nErrorValue als Paramter ist hier nicht gut
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);

#ifndef OLABEL
				Temptab.drfree () ;
#ifdef FITSMART
			dbClass.closedbase ("fsm");
#else
			dbClass.closedbase ("bws");
#endif
#endif	// ifndef OLABEL


				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;
	}



	while  ( !di )	// Pseudo-Schleife
// 151009			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
// 151009			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA))
   	{					// 030408 : dox und anzah ",1,1" ldazu .....
	    VariablenUebergabe ( hJob, szTemp2, nRecno , 0 , 1);	// Variablen vor derListe .....
		nErrorValue = LlPrint(hJob);
		while ((!di)&& (nErrorValue == 0))
// 151009 		&& (LlPrintGetCurrentPage(hJob) <= nLastPage))
		{
		    FelderUebergabe ( hJob, szTemp2, nRecno );
    		nErrorValue = LlPrintFields(hJob);
// 151009			nPrintFehler = nErrorValue;	//Nix doppelt lesen ....
			if (nErrorValue == LL_WRN_REPEAT_DATA)
			{
				while ( nErrorValue == LL_WRN_REPEAT_DATA )
				{
					VariablenUebergabe ( hJob, szTemp2, nRecno , 0 , 1 );
					nErrorValue = LlPrint(hJob);
//					FelderUebergabe ( hJob, szTemp2, nRecno );
	    			nErrorValue = LlPrintFields(hJob);	// 151009 : mehrmals ausgeben aktiviert
				}
			}
			else	// 090609 : Deckblatt-Funktion einbauen
			{
				if (nErrorValue == LL_ERR_NO_TABLEOBJECT)	// den ganzen Kram nochmals ausgeben fuer die "2.Seite"
				{
					VariablenUebergabe ( hJob, szTemp2, nRecno ,0 ,1 );
					nErrorValue = LlPrint(hJob);
					FelderUebergabe ( hJob, szTemp2, nRecno );	
	    			nErrorValue = LlPrintFields(hJob);	
//					nPrintFehler = nErrorValue;

				}

			}

			if (nErrorValue == 0)	// US: everything fine, record could have been printed...
	    	{
				// US: ...but probably the user defined a filter condition!
				//     So before updating time dependent variables we have to check if the record really has been printed:
				if (LlPrintDidMatchFilter(hJob))
				{
		    		sprintf(szTemp2, "FixedVariable2, record %d", nRecno);
		    		LlDefineVariable(hJob, "FixedVariable2", szTemp2);
		    	}

			}
#ifdef OLABEL
			di = 100 ;
#else
			di = Temptab.lesetemptab (nPrintFehler) ;	//       DATENSATZ lesen
#endif	// ifdef OLABEL

			if ( di )
			{
				di = di ;	// Testbreakpoint
				VariablenUebergabe ( hJob, szTemp2, nRecno ,0 , 1  );	// letzten Satz nachladen
			}

		}	// eigentliche schleife 
  	}		// pseudo-schleife

	// US: all records have been printed, now flush the table
	//     If footer doesn't fit to this page try again for the next page:

	VariablenUebergabe ( hJob, szTemp2, nRecno , 0, 1 );

	nErrorValue = LlPrintFieldsEnd(hJob);
	while (nErrorValue == LL_WRN_REPEAT_DATA)	// aus "if" ein "while" gemacht 151009
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	// 30408 : dox und anzahl dazu .....
		VariablenUebergabe ( hJob, szTemp2, nRecno,0, 1 );
	    FelderUebergabe ( hJob, szTemp2, nRecno );

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);
	}

	if (nErrorValue == LL_WRN_REPEAT_DATA)	// US: footer still doesn't fit!
	{
		char hilfe50 [50];	// 240608 : Name etwas komfortabler 
		sprintf ( hilfe50 , " dr70001 - %s " , myform_nr ) ;
#ifdef OLABEL
		sprintf ( hilfe50 , " dr70001 - %s " , myform_nr ) ;
#endif
#ifdef FITSMART
		sprintf ( hilfe50 , " dr70001 - %s " , myform_nr ) ;
#endif
		MessageBox("Error because table is too small for footer!", hilfe50, MB_OK|MB_ICONEXCLAMATION);
	}
	//GR: Druck beenden

	if ( zeittest ) schreibezeit("vor printende \n" ) ;

	LlPrintEnd(hJob,0);
	
	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) loeschen
        LlPreviewDisplay(hJob, szFilename, penvchar, hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, penvchar);
    }
	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
#ifndef OLABEL
	Temptab.drfree();
#ifdef FITSMART
			dbClass.closedbase ("fsm");
#else
			dbClass.closedbase ("bws");
#endif

#endif		// ifndef OLABEL


	if ( zeittest ) schreibezeit("STOP-ENDE \n" ) ;
			
	if (Hauptmenue == 0)  
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}

}

//=============================================================================
LRESULT CMainFrame::OnLulMessage(WPARAM wParam, LPARAM lParam)
//=============================================================================
{
	// GR: Dies ist die List & Label Callback Funktion.
	//     Saemtliche Rueckmeldungen bzw. Events werden dieser Funktion
	//     gemeldet.
	// US: This is the List & Label Callback function.
	//     Is is called for requests an notifications.


	PSCLLCALLBACK	pscCallback = (PSCLLCALLBACK) lParam;
	LRESULT			lRes = TRUE;
	CString			sVariableDescr;

	static CHAR szHelpText[256];

	ASSERT(pscCallback->_nSize == sizeof(scLlCallback));	// GR: Die groesse der Callback Struktur muss stimmen!
															// US: sizes of callback structs must match!

	switch(wParam)
	{
		case LL_CMND_VARHELPTEXT:	// GR: Es wird ein Beschreibungstext f�r eine Variable erfragt.
									// US: Helptext needed for selected variable within designer

				// GR: ( pscCallback->_lParam enth�lt einen LPCSTR des Beschreibungstextes )
				// US: ( pscCallback->_lParam contains a LPCSTR to the name of the selected variable )

				sVariableDescr = (LPCSTR)pscCallback->_lParam;

				if (!sVariableDescr.IsEmpty())
					sprintf(szHelpText,
							"This is the sample field / variable '%s'.",
							(LPCSTR)sVariableDescr);
				else
					strcpy(szHelpText, "No variable or field selected.");

				pscCallback->_lReply = (LPARAM)szHelpText;
				break;

		default:
				pscCallback->_lReply = lRes = FALSE; // GR: Die Nachricht wurde nicht bearbeitet.
													 // US: indicate that message hasn't been processed
	}

	return(lRes);
}




static char DefWert [256] ;

// 151009 : dr70001.cfg aktiviert : Zwangs-drmitmenue ; Anzahl auswertbar
char *get_default (char *env)
{         
        int anz;
        char buffer [512];
        FILE *fp;
        sprintf (buffer, "%s\\dr70001.cfg",getenv ("BWSETC"));
		fp = fopen (buffer, "r");
        if (fp == (FILE *)NULL) return NULL;

        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmpi (wort[1], env, (int)strlen (env),
							(int)strlen(clipped(wort[1]))) == 0)
                     {
                                 strcpy (DefWert, clipped (wort [2]));
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}

void CMainFrame::GetCfgValues (void)
{
//       char cfg_v [1024];
//       if (ProgCfg == NULL) return;

//		char hilfstr[256] ;
//		char *p ;


	cfgOK = TRUE;

	Hauptmenue =0 ;
	Listenauswahl = 0 ;
	DrMitMenue = 1 ;

	if (! zeittest )	// 180110
	{
		char * p =  get_default("zeittest");
		char hilfstr [199] ;
		if ( p != NULL )
		{
			sprintf ( hilfstr ,"%s", p );
			zeittest = atoi ( hilfstr ) ;
			if ( zeittest ) schreibezeit("Zeittest via dr70001.cfg aktiviert \n" ) ;
		}

	} ;



	return ;
/* ----> evtl. fuer spaeter
	Hauptmenue = 0 ;
	p =  get_default("Hauptmenue");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		Hauptmenue = atoi ( hilfstr ) ;
	} ;
	

	Listenauswahl = 0 ;
	p =  get_default("Listenauswahl");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		Listenauswahl = atoi ( hilfstr ) ;
	} ;

	DrMitMenue = 0 ;
	p =  get_default("DrMitMenue");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		DrMitMenue = atoi ( hilfstr ) ;
	} ;

	if ( buffer !=  (char *) 0) 
			free ( buffer );
< ---- */

/* --->
		Listenauswahl =0 ;
		if (ProgCfg->GetCfgValue ("Listenauswahl", cfg_v) == TRUE)
       {
           Listenauswahl = atoi (cfg_v);
	   }
< -*/
/* -->
		Hauptmenue = 0;
		if (ProgCfg->GetCfgValue ("Hauptmenue", cfg_v) == TRUE)
       {
           Hauptmenue = atoi (cfg_v);
	   }

	   exit(0) ;
< ---- */

/* --->
		DrMitMenue = 1 ;
       if (ProgCfg->GetCfgValue ("DrMitMenue", cfg_v) == TRUE)
       {
           DrMitMenue = atoi (cfg_v);
	   }
< ---- */

}


    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt
	/************
	LlDefineFieldExt(hJob, "Barcode_EAN13", "44|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P2", "44|44444|44444|44", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P5", "44|44444|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN128", "EAN128ean128", LL_BARCODE_EAN128, NULL);
	LlDefineFieldExt(hJob, "Barcode_CODE128", "Code 128", LL_BARCODE_CODE128, NULL);
	LlDefineFieldExt(hJob, "Barcode_Codabar", "A123456A", LL_BARCODE_CODABAR, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCA", "44|44444", LL_BARCODE_EAN8, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCE", "4|44444|44444", LL_BARCODE_UPCA, NULL);
	LlDefineFieldExt(hJob, "Barcode_3OF9", "*TEST*", LL_BARCODE_3OF9, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IND", "44444", LL_BARCODE_25INDUSTRIAL, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IL", "444444", LL_BARCODE_25INTERLEAVED, NULL);
	LlDefineFieldExt(hJob, "Barcode_25MAT", "44444", LL_BARCODE_25MATRIX, NULL);
	LlDefineFieldExt(hJob, "Barcode_25DL", "44444", LL_BARCODE_25DATALOGIC, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET5", "44444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET10", "44444-4444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET12", "44444-444444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_FIM", "A", LL_BARCODE_FIM, NULL);
	**********/


void fitapldefines (HJOB hJob)
{

/* -->
	sprintf (form_feld.form_nr, "test1");

	dbClass.sqlopen (form_feld_curs);

	while (dbClass.sqlfetch (form_feld_curs) == 0)
	{
		ll_typ = holetyp(form_feld.tab_nam , form_feld.feld_nam );

		form_feld.tab_nam[0] = toupper(form_feld.tab_nam[0]) ;
		form_feld.feld_nam[0] = toupper(form_feld.feld_nam[0]) ;
		CString s = form_feld.tab_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.tab_nam, s ) ;
		s = form_feld.feld_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.feld_nam, s ) ;
		sprintf( tabdotfeld , "%s.%s", form_feld.tab_nam, form_feld.feld_nam);
		sprintf ( defabeleg, "%s", form_feld.feld_nam);
		if (ll_typ == LL_NUMERIC )
		{
			sprintf ( defabeleg, "12345678");
		};
	
			LlDefineFieldExt(hJob, tabdotfeld, defabeleg,ll_typ,NULL);

	}
*********/
}


