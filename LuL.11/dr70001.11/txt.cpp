#include "stdafx.h"


#ifndef OLABEL
#include "DbClass.h"
#endif	//	OLABEL

#include "txt.h"

#ifndef OLABEL

#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

struct PTXT ptxt,  ptxt_null;
extern DB_CLASS dbClass;

#endif	// ifndef OLABEL

// int itxt_nr ;
// static int anzzfelder ;

/**
--------------------------------------------------------------------------------
-
-       Procedure       :       clipped
-
-       In              :       string
-
-       Out             :
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Blanks am Ende eines Strings entfernen.
-
-    ich baue mal so um, das nur noch  rechts geclipped wird
--------------------------------------------------------------------------------
**/
#ifdef OLD_DCL
char *clipped (string)
char *string;
#else
char *clipped (char *string)
#endif
{
 char *clstring;
 short i,len;

 if ( string == NULL ) return string ;	// 200406
 len = (short) strlen (string);

 if (len == 0) return (string);
 len --;
 clstring = string;
 // 280207 : >0 -> >= 0 , damit werden auch leerstrings korrekt gekappt 
 for (i = len; i >= 0; i --)
 {
  if ((unsigned char) clstring[i] > 0x20)
  {
   break;
  }
 }
 clstring [i + 1] = 0;

 clstring = string;
 /* -----> links-clippen lasse ich mal weg 
 len = (short) strlen (clstring);

 for (i = 0; i < len; i ++, clstring +=1)
 {
  if ((unsigned char) *clstring > (unsigned char) 0X20)
  {
   break;
  }
 }
 < ----- */
 return (clstring);
}

#ifndef OLABEL

int PTXT_CLASS::leseptxt ()
{

	memcpy ( &ptxt, &ptxt_null , sizeof ( struct PTXT )) ;	// 290506
	int di = dbClass.sqlfetch (readcursor);

	  return di;
}

// 111110
int PTXT_CLASS::suchereadcursor (char *ctabnam)
{
	for ( int i = 0 ; i < MYMAXRANGE ; i ++ )
	{
		if ( ctabimatr[i] < 0 )
		{
			// Eintragsende erreicht : readcursor zeigt auf naechsten freien Platz
			readcursor = i ;
			return -1 ;
		}
		if ( ! strncmp ( ctabnmatr[i],clipped (ctabnam) , strlen(clipped (ctabnam))))
		{
			// erfolgreich gefunden
			readcursor = ctabimatr[i] ;
			return 0 ;
		}
	}
	// Notbremse ?!
	readcursor = 0 ;
	return -1  ;
}


int PTXT_CLASS::openptxt (char *ctabnam)
{
//		if ( suchereadcursor( ctabnam ) < 0 )	// 111110 : bedingt suchen oder oeffnen
			prepare ( ctabnam );

        return dbClass.sqlopen (readcursor);
}


void PTXT_CLASS::prepare (char * ctabnam)
{

	// 111110
	int ihilfe = suchereadcursor( ctabnam ) ;
	if ( ! ihilfe )
	{
		// alles ist gut, dencursor gibbet schon ....
		// readcursor zeigt auf atuelle selektion
		return ;
	}
	ihilfe = readcursor ;	// ihilfe zeigt auf naechsten freien Platz

	test_upd_cursor = 1;
// ich gehe mal davon aus, das eine Zeile nicht groesser als 100 Zeichen ist ...

	dbClass.sqlin ((long *)   &ptxt.nr, SQLLONG, 0);

	dbClass.sqlout ((long *)  &ptxt.zei,SQLLONG, 0);
	dbClass.sqlout ((char *)  ptxt.txt,SQLCHAR, 101);



//		readcursor = dbClass.sqlcursor ("select "
//	" zei, txt "
//	" from " clipped (ctabnam) " where nr = ? order by zei" ) ;
	char buffer[256] ;
	sprintf ( buffer , "select zei,txt from %s where nr = ? order by zei ", 
		clipped (ctabnam) );
	readcursor = (short) dbClass.sqlcursor (buffer);

	if ( readcursor > -1 )	// 111110 : neuer Eintrag
	{
		if ( ihilfe < MYMAXRANGE )
		{
			ctabimatr[ihilfe] = readcursor ;
			sprintf ( ctabnmatr[ihilfe] , "%s" , ctabnam ) ;
		}
	}

}
#endif	// ifndef OLABEL
