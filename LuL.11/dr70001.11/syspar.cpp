#include "stdafx.h"
#include "DbClass.h"
#include "syspar.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

struct SYS_PAR sys_par,  sys_par_null;
extern DB_CLASS dbClass;


int SYS_PAR_CLASS::lesesys_par ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int SYS_PAR_CLASS::opensys_par (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

void SYS_PAR_CLASS::prepare (void)
{


	dbClass.sqlin ((char *) sys_par.sys_par_nam, SQLCHAR, 18);

	dbClass.sqlout ((char *) sys_par.sys_par_wrt,SQLCHAR, 2);
	dbClass.sqlout ((char *) sys_par.sys_par_besch,SQLCHAR, 33);
	dbClass.sqlout ((long *) &sys_par.zei, SQLLONG, 0);
	dbClass.sqlout ((short *) &sys_par.delstatus, SQLSHORT, 0);

		readcursor = (short) dbClass.sqlcursor ("select "

	" sys_par_wrt, sys_par_besch, zei, delstatus " 
	" from sys_par where sys_par_nam = ? " ) ;
	

}

