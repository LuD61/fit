#include "stdafx.h"
#include "DbClass.h"
#include "syscolumns.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif


struct SYSCOLUMNS syscolumns, syscolumns_null;
struct NFORM_FELD nform_feld, nform_feld_null;

extern DB_CLASS dbClass;

int coltyp_curs, nformfeld_curs ;

int SYSCOLUMNS_CLASS::lesesyscolumns (void)
{
      int di = dbClass.sqlfetch (coltyp_curs);
	  return di;
}


int NFORM_FELD_CLASS::lesenform_feld (void)
{

      int di = dbClass.sqlfetch (nformfeld_curs);
	  return di;
}


int SYSCOLUMNS_CLASS::opensyscolumns (void)
{
	 if ( coltyp_curs < 0 ) prepare ();
//	prepare ();
	return dbClass.sqlopen (coltyp_curs);
}

int NFORM_FELD_CLASS::opennform_feld (void)
{
    if ( nformfeld_curs < 0 ) prepare ();
//	prepare () ;
         return dbClass.sqlopen (nformfeld_curs);
}

void SYSCOLUMNS_CLASS::prepare (void)
{

#ifndef FITODBC

		dbClass.sqlin ((long *) &syscolumns.tabid, SQLLONG, 0);
		dbClass.sqlin ((char *)  syscolumns.colname, SQLCHAR, 19);

		dbClass.sqlout ((short *) &syscolumns.coltype, SQLSHORT,0);
		dbClass.sqlout ((short *) &syscolumns.collength, SQLSHORT,0);
    
		coltyp_curs = dbClass.sqlcursor ("select coltype,collength from syscolumns where tabid = ? and colname = ?");
#endif 	    									
		test_upd_cursor = 1;
	
}


void NFORM_FELD_CLASS::prepare (void)
{		
	// nformfeld_curs : lese Felder und deren Eigenschaften je Tabelle
		dbClass.sqlin ((char *)   nform_feld.form_nr,   SQLCHAR, 11);
		dbClass.sqlin ((char *)   nform_feld.tab_nam,   SQLCHAR,19);
		dbClass.sqlin ((short *)  &nform_feld.lila,     SQLSHORT,0);

		dbClass.sqlout ((short *) &nform_feld.delstatus, SQLSHORT,0);
		dbClass.sqlout ((char *)   nform_feld.form_nr,   SQLCHAR, 11);
		dbClass.sqlout ((char *)   nform_feld.feld_nam,  SQLCHAR,19);
		dbClass.sqlout ((char *)   nform_feld.tab_nam,   SQLCHAR,19);
		dbClass.sqlout ((short *) &nform_feld.feld_typ,  SQLSHORT,0);
		dbClass.sqlout ((short *) &nform_feld.feld_id,   SQLSHORT,0);
		dbClass.sqlout ((short *) &nform_feld.sort_nr,   SQLSHORT,0);
		dbClass.sqlout ((char *)   nform_feld.krz_txt,   SQLCHAR,25);
		dbClass.sqlout ((short *) &nform_feld.range,  SQLSHORT,0);
		


		nformfeld_curs = dbClass.sqlcursor ("select delstatus, form_nr,"
			" feld_nam,tab_nam,feld_typ,feld_id,sort_nr,krz_txt,range "
			" from nform_feld where form_nr = ? and tab_nam = ? and lila = ? "
			" order by feld_nam " );


}
