//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
//=============================================================================

// mainfrm.h : interface of the CMainFrame class
// #include "mo_progcfg.h"

extern short GlobalLila ;

extern char* uPPER( char * ) ;	// 290610

class CMainFrame : public CFrameWnd
{
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// GrJ 161203     PROG_CFG *ProgCfg;
// Attributes
	BOOL m_bDebug;

// Operations
public:

// Implementation
public:
	virtual ~CMainFrame();
	void Variablendefinition ( HJOB hJob );
	void VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno ,int dox, int ganz) ;

	void ptabschreiben ( HJOB , char * , int,  char * , char *  ) ; 
	void Felderdefinition  (HJOB hJob ) ;
	void FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno ) ;
#ifdef _DEBUG
	virtual	void AssertValid() const;
	virtual	void Dump(CDumpContext& dc) const;
#endif

	static UINT m_uLuLMessageBase;	// message base for LuL messages


// Generated message map functions
protected:

	void DoListPrint();
	void DoLabelPrint();
    void GetCfgValues();
	void InterpretCommandLine();
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnStartDebug();
	afx_msg void OnStopDebug();
	afx_msg void OnEditLabel();
	afx_msg void OnEditList();
	afx_msg void OnPrintLabel();
	afx_msg void OnPrintReport();
	afx_msg void OnUpdateStartDebug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStopDebug(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg LRESULT OnLulMessage(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
#define FITINFORMIX 0 
#define FITMYSQL 1
#define FITORACLE 2




#define OLASTU 251		// 160713

extern int odbdabatyp ;
extern char odbbasisverz[256] ;
extern char odb_daba [256] ;
extern char odb_user [256] ;
extern char odb_pass [256] ;
extern char odb_isomode[26] ;

