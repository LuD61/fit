#include "stdafx.h"
#include "DbClass.h"
#include "item.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

#include "Mainfrm.h"	// 200509

struct ITEM item,  item_null;
extern DB_CLASS dbClass;

int ITEM_CLASS::leseitem (	)
{

      int di = dbClass.sqlfetch (readcursor);

	  return di;
}


int ITEM_CLASS::openitem (void)
{

		if ( readcursor < 0 ) prepare ();

		sprintf ( item.hilfe ,"i" ) ;
		if ( odbdabatyp == FITORACLE )
		{
// 290610			likeoracle ( TAFELNAME, item.name ) ;
			uPPER ( item.name ) ;	// 290610
		}
        return dbClass.sqlopen (readcursor);
}


void ITEM_CLASS::prepare (void)
{

	dbClass.sqlin  ((char *) item.name, SQLCHAR, 19);

if ( odbdabatyp == FITORACLE )
{	// hier MUSS dann wieder kleines i als Parameter fuer "caseinsensitive" mitgegeben werden ... 
	sprintf ( item.hilfe ,"i" ) ;
// 290610	dbClass.sqlin  ((char *) item.hilfe, SQLCHAR, 2);

}
	dbClass.sqlout ((char *) item.bezkurz, SQLCHAR, 11);
    dbClass.sqlout ((char *) item.bezlang, SQLCHAR, 19);

 	test_upd_cursor = 1;
if ( odbdabatyp == FITORACLE )
	readcursor =
//	290610 (short)	dbClass.sqlcursor ("select bezkurz, bezlang from item where REGEXP_LIKE ( name , ? , ? )" ) ;
	(short)	dbClass.sqlcursor ("select bezkurz, bezlang from item where Upper(name) = ? ") ; 
else
	readcursor =
	(short)	dbClass.sqlcursor ("select bezkurz, bezlang from item where name = ? " ) ;
	
  }
