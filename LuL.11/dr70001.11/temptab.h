#ifndef _TEMPTAB_DEF
#define _TEMPTAB_DEF

#define MAXANZ 250
#define MAXRANGE 20
#define MAXRANGEI 8
// Mehr als 250 felder sollten nur sehr selten vorkommen 
// MAXRANGE ist nur zur Sicherheit wegen Ueberlauf, mehr als MAXRANGEI Bereiche werte ich z.Z. nicht aus 
extern  char  tt_tabnam [MAXANZ][29];	// 220909 : 19-> 29 wegen langer Feldnamen
extern  char  tt_feldnam[MAXANZ][29]; 	// 220909 : 19-> 29 wegen langer Feldnamen
extern  int    tt_datetyp[MAXANZ];	// Datentyp in der Daba
extern  int    tt_datelen[MAXANZ];	// Stringlaenge in der Daba 280207 : bisher nur lokal ?!
extern  int    tt_feldtyp[MAXANZ];	// Datentyp fuer Generator
extern  int    tt_referenz  [MAXANZ];	// nur Pointer auf referenz-Liste
extern  int    tt_range     [MAXANZ];	// ob Feld fuer range herangezogen wird
extern  char * tt_rangin    [MAXANZ];	// 280508 : Pointer auf diesen String	// Input-String 
extern  int    tt_rangini    [MAXANZ];	// 220909 : rangin-Typ	// 0 = NIX ,1 = "in ()" 2 = freier Typ 

extern  void * tt_sqlval [MAXANZ];	// pointer auf Feldinhalte 
extern long  tt_sqllong    [MAXANZ];	// Matrix fuer longs und shorts 
extern double tt_sqldouble [MAXANZ];	// Matrix fuer double 

 // mehr als 50 verschiedene Textbausteine sollten sehr selten sein
 extern char  tt_creferenz[100][25];	// 141209 aus 50 wird 100 


extern char  tt_cvrange[MAXRANGE][50];	// maximale stringlaenge  49
extern char  tt_cbrange[MAXRANGE][50];
extern char * tt_rangtyp2[MAXRANGE] ;	// 230909

extern TIMESTAMP_STRUCT  tt_tvrange[MAXRANGE];
extern TIMESTAMP_STRUCT  tt_tbrange[MAXRANGE];
extern long  tt_lvrange[MAXRANGE];
extern long  tt_lbrange[MAXRANGE];
extern double  tt_dvrange[MAXRANGE];
extern double  tt_dbrange[MAXRANGE];


#ifdef FITODBC
	extern	CLlSampleDoc* pDoc ;
#endif

extern struct TEMPTAB temptab, temptab_null;


class TEMPTAB_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
			   int ddistinct ;	// 030211
       public :
               int structprep (char *);
               int lesetemptab (int);
               void inittemptab (void);
               void reloadtemptab (void);	// 300506
               int opentemptab (void);
			   int getanzahl (void);
			   void drfree(void);
               TEMPTAB_CLASS () : DB_CLASS ()
               {
				   ddistinct = 0 ;
               }
};
#endif

