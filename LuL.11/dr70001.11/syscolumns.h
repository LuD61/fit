#ifndef _SYSCOLUMNS_DEF
#define _SYSCOLUMNS_DEF

struct SYSCOLUMNS {
   char     colname[19];
   long     tabid;
   short    coltype;
   short  collength;
}
 ;

extern int coltyp_curs ;

extern struct SYSCOLUMNS syscolumns;

struct NFORM_FELD {
   short    delstatus;
   char     form_nr[11];
   char     feld_nam[19];
   char     tab_nam[19];
   short    feld_typ;
   short    feld_id;
   short    sort_nr;
   char		krz_txt[25];
   short	range ;
   short    lila ;
}
 ;

extern int nformfeld_curs ;

extern struct NFORM_FELD nform_feld;





/*-

https://groups.google.com/forum/#!topic/comp.databases.informix/DIhrEkUtZVY

1. Problem : coltype ist > 255 :
IF p_coltype>255 
   THEN LET p_coltype=mod(p_coltype,256); 
END IF; 

2. So kann man das alles komplett abarbeiten :

IF p_coltype= 0 THEN return p_collength;	// charakter 
ELIF p_coltype= 1 THEN return p_collength;	// smallint
ELIF p_coltype= 2 THEN return p_collength;	// integer
ELIF p_coltype= 3 THEN return p_collength;	// float
ELIF p_coltype= 4 THEN return p_collength;	// smallfloat
ELIF p_coltype= 5 THEN						// decimal
     let i=trunc(p_collength/256); 
     IF mod(i,2)=0 
     THEN 
        let j=(i+4)/2; 
     ELSE 
        let j=(i+3)/2; 
     END IF; 
     IF mod(j,2)=1 
     THEN 
        LET j=j-1; 
     END IF; 
     IF j>17 THEN   
        let j=17; 
     END IF; 
     RETURN j; 
ELIF p_coltype= 6 THEN return p_collength;	// serail
ELIF p_coltype= 7 THEN return p_collength;	// date
ELIF p_coltype= 8 THEN						// money
     let i=trunc(p_collength/256); 
     IF mod(i,2)=0 
     THEN 
        let j=(i+4)/2; 
     ELSE 
        let j=(i+3)/2; 
     END IF; 
     IF mod(j,2)=1 
     THEN 
        LET j=j-1; 
     END IF; 
     IF j>17 THEN   
        let j=17; 
     END IF; 
     RETURN j; 
ELIF p_coltype= 9 THEN return p_collength;	// null 
ELIF p_coltype= 10 THEN RETURN MOD(p_collength,256);	// datetime 
ELIF p_coltype= 11 THEN return p_collength;				// byte
ELIF p_coltype= 12 THEN return p_collength;				// text
ELIF p_coltype= 13 THEN return mod(p_collength,256);	// varchar
ELIF p_coltype= 14 THEN RETURN MOD(p_collength,256); 
ELIF p_coltype= 15 THEN return p_collength; 
ELIF p_coltype= 16 THEN return mod(p_collength,256); 
ELIF p_coltype= 17 THEN return p_collength; 
ELIF p_coltype= 18 THEN return p_collength; 
ELSE RETURN 0; 
END IF; 

END 

3. collength f�r datetime : ( length * 256 ) + ( first_qual * 16 ) + last_qual 
   bsp : year to fraction(3) (17*256 )  + ( 0 * 16 ) + 13  := 4365 dez.

0   year
2   mon
4   day
6   hour
8   min
10  sec
12  frac

datetime.h


direkt von ibm aus dem internet

coltype

0 = CHAR 
1 = SMALLINT 
2 = INTEGER 
3 = FLOAT 
4 = SMALLFLOAT 
5 = DECIMAL 
6 = SERIAL * 
7 = DATE 
8 = MONEY 
9 = NULL 
10 = DATETIME 
11 = BYTE 
12 = TEXT
13 = VARCHAR 
14 = INTERVAL 
15 = NCHAR 
16 = NVARCHAR 
17 = INT8 
18 = SERIAL8 * 
19 = SET 
20 = MULTISET 
21 = LIST 
22 = Unnamed ROW 
40 = Variable-length 
opaque type 
4118 = Named ROW 

// ausschnitt aus datetime.h
** time units for datetime qualifier **

#define TU_YEAR 0
#define TU_MONTH 2
#define TU_DAY 4
#define TU_HOUR 6
#define TU_MINUTE 8
#define TU_SECOND 10
#define TU_FRAC 12
#define TU_F1 11
#define TU_F2 12
#define TU_F3 13
#define TU_F4 14
#define TU_F5 15

** TU_END - end time unit in datetime qualifier **
** TU_START - start time unit in datetime qualifier **
** TU_LEN - length in  digits of datetime qualifier **

#define TU_END(qual) (qual & 0xf)
#define TU_START(qual) ((qual>>4) & 0xf)
#define TU_LEN(qual) ((qual>>8) & 0xff)

** TU_ENCODE - encode length, start and end time unit to form qualifier **
** TU_DTENCODE - encode datetime qual **
** TU_IENCODE - encode interval qual **

#define TU_ENCODE(len,s,e) (((len)<<8) | ((s)<<4) | (e))
#define TU_DTENCODE(s,e) TU_ENCODE(((e)-(s)+((s)==TU_YEAR?4:2)), s, e) 
#define TU_IENCODE(len,s,e) TU_ENCODE(((e)-(s)+(len)),s,e)
#define TU_FLEN(len) (TU_LEN(len)-(TU_END(len)-TU_START(len)))

** TU_CURRQUAL - default qualifier used by current **
#define TU_CURRQUAL TU_ENCODE(17,TU_YEAR,TU_F3)

** TU_CURRQUAL - default qualifier used by sysdate **
#define TU_SYSDQUAL TU_DTENCODE(TU_YEAR,TU_F5)

<------ */

// Typ Datenbank ( syscolumns - Informix)
#define iDBCHAR		0
#define iDBSMALLINT 1
#define iDBINTEGER  2
#define iDBDECIMAL  5
#define iDBDATUM    7
#define iDBDATETIME 12
#define iDBVARCHAR	13		// 280612 : varchar aus syscolumns direkt anschliessend als char weiterhandeln ?!
#define iSQL_VARCHAR	12	// 290612 : varchar aus odbc32 , anschliessend als char weiterhandeln ?!
#define iDBSERIAL 262		// 201209 : wir lesen das ding ja bloss -als integer weiterhandeln -> ist letzendlich offset 256


// Typ LuL 
#define iTYPPTAB	1
#define iTYPPTXT	2
#define iTYPADR		3


class SYSCOLUMNS_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//               int dbcount (void);
               int lesesyscolumns (void);
               int opensyscolumns (void);
               SYSCOLUMNS_CLASS () : DB_CLASS ()
               {
				  coltyp_curs = -1 ;
               }
};

class NFORM_FELD_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//               int dbcount (void);
               int lesenform_feld (void);
               int opennform_feld (void);
               NFORM_FELD_CLASS () : DB_CLASS ()
               {
				   nformfeld_curs = -1 ;
               }
};
#endif
