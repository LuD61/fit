#ifndef _PTABN_DEF
#define _PTABN_DEF

struct PTABN {

char ptwert[4];
char ptitem[19];
long ptlfnr ;
char ptbez[33];
char ptbezk[9];
char ptwer1[9];
char ptwer2[9];
char hilfe[2];	// 200509
};

extern struct PTABN ptabn, ptabn_null;


class PTABN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leseptabn (void);
               int openptabn (void);
               PTABN_CLASS () : DB_CLASS ()
               {
               }
};


#endif

