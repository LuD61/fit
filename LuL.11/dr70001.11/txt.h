#ifndef _TXT_DEF
#define _TXT_DEF

extern char * clipped( char *) ;

#define MYMAXRANGE 20

#ifndef OLABEL

struct PTXT {
long nr;
long zei;
char txt[501];	// ich gehe mal davon aus, das eine Ziele nie groesser als 100 Zeichen ist

};
extern struct PTXT ptxt, ptxt_null;


class PTXT_CLASS : public DB_CLASS
{
       private :
               void prepare (char *);
       public :
//               int dbcount (void);
               int leseptxt (void);
               int openptxt (char *);
			   char  ctabnmatr[MYMAXRANGE][25] ;			// 111110 mal so lang wie "nform_feld.krz_txt"
			   short ctabimatr[MYMAXRANGE] ;				// 111110
			   int suchereadcursor (char *ctabnam) ;	// 111110
			   
			   PTXT_CLASS () : DB_CLASS ()
               {

					// 111110 
					for ( int i = 0 ; i < MYMAXRANGE ; i ++ )
					{
						ctabnmatr[i][0] = '\0' ;
						ctabimatr[i]    =  -1  ;
					}
               }
};
#endif	// ifndef OLABEL
#endif
