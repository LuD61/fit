#include "stdafx.h"
#include "DbClass.h"
#include "form_ow.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif


extern char * clipped(char*) ;	// 021204


char ordersel [1032] ;
//hier drin stehen spaeter mal evtl. 0...n durch '\0' getrennte Namensstrings 
struct FORM_W form_w,  form_w_null;
struct FORM_O form_o,  form_o_null;
struct FORM_V form_v,  form_v_null;

struct FORM_T form_t,  form_t_null;	// 220207
struct FORM_TE form_te,  form_te_null;	// 220207

extern DB_CLASS dbClass;

void subrueck ( char * text )
{
	int geslen, ipo ;
	unsigned char uchar ;
//	geslen = (int) strlen ( clipped( text)) ;
	geslen = (int) strlen ( text) ;	// 010507
	for ( ipo = 0 ; ipo < geslen ; ipo ++ )
	{
		uchar = text[ipo] ;
		if (uchar > 0x80 && uchar < 0xa0 ) text[ipo] = uchar - 0x80 ;
	}

}

// 220207
int FORM_TE_CLASS::leseform_te ( void )
{

      int di = dbClass.sqlfetch (readcursor);
	  if ( !di) subrueck(form_te.krz_txt );
	  return di;
}

int FORM_T_CLASS::leseform_t ( void )
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int FORM_V_CLASS::leseform_v (							)
{

      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int FORM_W_CLASS::leseform_w (							)
{

      int di = dbClass.sqlfetch (readcursor);
	  if ( !di) subrueck(form_w.krz_txt );	// 021204
	  return di;
}

int FORM_TE_CLASS::openform_te (void)
{
	if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

int FORM_T_CLASS::openform_t (void)
{
	if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

int FORM_V_CLASS::openform_v (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

int FORM_W_CLASS::openform_w (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

void FORM_TE_CLASS::prepare (void)
{

	dbClass.sqlin ((char *) form_te.form_nr, SQLCHAR, 11);
	dbClass.sqlin ((short *) &form_te.lila, SQLSHORT, 0);
	dbClass.sqlin ((char *) form_te.tab_nam, SQLCHAR, 19);
	dbClass.sqlin ((char *) form_te.feld_nam, SQLCHAR, 19);

	dbClass.sqlout ((long *) &form_te.zei, SQLLONG,0 );
	dbClass.sqlout ((char *) form_te.krz_txt, SQLCHAR, 51);

    readcursor = (short) dbClass.sqlcursor ("select zei, krz_txt from form_te "
					"where form_te.form_nr = ? and form_te.lila = ? "
					" and form_te.tab_nam = ? and form_te.feld_nam = ? "
					"order by zei ");
										
	test_upd_cursor = 1;

}

void FORM_T_CLASS::prepare (void)
{

		dbClass.sqlin ((char *)  form_t.form_nr,SQLCHAR,11);
		dbClass.sqlin ((short *) &form_t.lila, SQLSHORT, 0 );
		dbClass.sqlin ((char *)  form_t.tab_nam,SQLCHAR,19);
		dbClass.sqlin ((char *)  form_t.feld_nam, SQLCHAR, 19 );

		dbClass.sqlout((char *)  form_t.ttab_nam ,SQLCHAR,19);

		dbClass.sqlout((char *)  form_t.sfeld1 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.sfeld2 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.sfeld3 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.sfeld4 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.sfeld5 ,SQLCHAR,19);

		dbClass.sqlout((char *)  form_t.wtab1 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.wtab2 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.wtab3 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.wtab4 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.wtab5 ,SQLCHAR,19);

		dbClass.sqlout((char *)  form_t.wfeld1 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.wfeld2 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.wfeld3 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.wfeld4 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.wfeld5 ,SQLCHAR,19);

		dbClass.sqlout((char *)  form_t.ofeld1 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.ofeld2 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.ofeld3 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.ofeld4 ,SQLCHAR,19);
		dbClass.sqlout((char *)  form_t.ofeld5 ,SQLCHAR,19);

		dbClass.sqlout((short *) &form_t.odesc1,SQLSHORT,0);
		dbClass.sqlout((short *) &form_t.odesc2,SQLSHORT,0);
		dbClass.sqlout((short *) &form_t.odesc3,SQLSHORT,0);
		dbClass.sqlout((short *) &form_t.odesc4,SQLSHORT,0);
		dbClass.sqlout((short *) &form_t.odesc5,SQLSHORT,0);

    readcursor = (short)
		dbClass.sqlcursor("select ttab_nam "
			" ,sfeld1, sfeld2, sfeld3, sfeld4, sfeld5 "
			" ,wtab1, wtab2, wtab3, wtab4, wtab5 "
			" ,wfeld1, wfeld2, wfeld3, wfeld4, wfeld5 "
			" ,ofeld1, ofeld2, ofeld3, ofeld4, ofeld5 "
			" ,odesc1, odesc2, odesc3, odesc4, odesc5 "
			" from form_t"
			" where form_nr = ? and lila = ? and tab_nam = ? "
			" and feld_nam = ? ");

	test_upd_cursor = 1;

}

void FORM_V_CLASS::prepare (void)
{

		dbClass.sqlin ((char *)  form_v.form_nr,SQLCHAR,11);
		dbClass.sqlin ((short *) &form_v.lila, SQLSHORT, 0 );

		dbClass.sqlout((char *)  form_v.krz_txt,SQLCHAR,101);
		dbClass.sqlout((long *)  &form_v.zei,SQLLONG,0);
		dbClass.sqlout((long *)  &form_v.typ,SQLLONG,0);
		dbClass.sqlout((long *)  &form_v.vk,SQLLONG,0);
		dbClass.sqlout((long *)  &form_v.nk,SQLLONG,0);
		dbClass.sqlout((char *)  form_v.klar_txt,SQLCHAR,31);

		readcursor =
			dbClass.sqlcursor("select krz_txt,zei,typ , vk, nk , klar_txt from form_v"
			" where form_nr = ? and lila = ? order by zei ");

										
	test_upd_cursor = 1;

}


void FORM_W_CLASS::prepare (void)
{


	dbClass.sqlin ((char *) form_w.form_nr, SQLCHAR, 11);
	dbClass.sqlin ((short *) &form_w.lila, SQLSHORT, 0);

	dbClass.sqlout ((long *) &form_w.zei, SQLLONG,0 );
	dbClass.sqlout ((char *) form_w.krz_txt, SQLCHAR, 51);

    readcursor = (short) dbClass.sqlcursor ("select zei, krz_txt from form_w "
					"where form_w.form_nr = ? and form_w.lila = ? "
					"order by zei ");
										
	test_upd_cursor = 1;

}

int FORM_O_CLASS::getmehrfach_o ( void	)
{
	return imehrfach ;
}
void FORM_O_CLASS::putmehrfach_o ( int emehrfach	)
{
	imehrfach = emehrfach ;
}

static int imodus  ; // 0 := vor Muster  1 := inMuster
static int imod2 ;	// 0 := aktiver String im Lesen

int FORM_O_CLASS::leseform_o ( void	)
{

	char text[ 60] ;
	unsigned char uchar ;
	int di, i, ziel ;
	if ( imehrfach == 0 )	// konventioneller Ablauf
	{
      di = dbClass.sqlfetch (readcursor);
	  if ( !di) subrueck(form_o.krz_txt );
	  return di;
	}

    di = dbClass.sqlfetch (readcursor);
	i = ziel = 0 ;
	while (! di && ( imod2 <= imehrfach ) )
	{
		for (  ; form_o.krz_txt[i] != '\0' ;i++)
		{
			if ( imod2 < imehrfach )
			{	// Suche, wo das Statement beginnt 
				uchar = form_o.krz_txt[i] ;
				if ( imodus == 0 )
				{
					if ( uchar == '='  )
					{
						imodus = 1 ;
					}
				}
				else	// imodus == 1
				{
					if ( uchar == '=' || 
						( uchar > 0x80 && uchar < 0xa0 )) 
					{
						imodus = 0 ; imod2 ++ ;
					}
				}
			}
			if ( imod2 == imehrfach )
			{
				uchar = form_o.krz_txt[i] ;
				if ( imodus == 0 )
				{
					if ( uchar == '='  )
					{
						imodus = 1 ;
						text[ziel] = '\0' ;
						ziel ++ ;
						imod2 ++ ;	
					}
					else
					if ( uchar > 0x80 && uchar < 0xa0 )
						uchar -= 0x80 ;
						text[ziel] = uchar ;
						ziel ++ ;
				}
				else	// imodus == 1
				{
					// Nix schreiben
				}
			}
		}
		if ( ziel > 0 )
		{
			text[ziel] = '\0' ;
			ziel ++ ;
			sprintf ( form_o.krz_txt,"%s",text );
			return 0 ;
		}
		di = dbClass.sqlfetch (readcursor);
		i = ziel = 0 ;
	}

	return 100 ;
}

int FORM_O_CLASS::openform_o (void)
{

	int ziel ;		// zeilpointer
	unsigned char uchar ;
	if ( readcursor < 0 ) prepare ();
	imodus = 0 ;
	imod2 = 1 ;
	ziel = 0 ;
	ordersel[ziel] = '\0' ;

	int di = dbClass.sqlopen (readcursor);
// 260307 : doppelt lesen 
	if (di )
	{
		imehrfach = -1 ;	// Nix vorhanden
		return di ;
	}


	imehrfach = 0 ;	// Mindestens konventioneller Ablauf
	imodus = 0 ;
    di = dbClass.sqlfetch (readcursor);
	while (! di && (ziel < 1030) )
	{
		for ( int i = 0 ; form_o.krz_txt[i] != '\0';i++)
		{
			uchar = form_o.krz_txt[i] ;
			if ( imodus == 0 )
			{
				if ( uchar == '='  )
				{
					imodus = 1 ; imehrfach ++ ;
				}
			}
			else	// imodus == 1
			{
				if ( uchar == '=' || 
						( uchar > 0x80 && uchar < 0xa0 )) 
				{
					imodus = 0 ;
					ordersel [ziel ] = '\0' ;
					ziel ++ ;
				}
				else
				{
					ordersel [ziel] = form_o.krz_txt[i] ; 
					ziel ++ ;
				}
			}

		}
		di = dbClass.sqlfetch (readcursor);
	}
	if ( imehrfach > 0 )
	{
		ordersel [ziel] = '\0' ;
		ziel ++ ;
	}

	imodus = 0 ;
	imod2 = 1 ;
	di = dbClass.sqlopen (readcursor);
	return di ;
}

void FORM_O_CLASS::prepare (void)
{

	dbClass.sqlin ((char *) form_o.form_nr, SQLCHAR, 11);
	dbClass.sqlin ((short *) &form_o.lila, SQLSHORT, 0);

	dbClass.sqlout ((long *) &form_o.zei, SQLLONG,0 );
	dbClass.sqlout ((char *) form_o.krz_txt, SQLCHAR, 51);


    readcursor = (short)dbClass.sqlcursor ("select zei,krz_txt from form_o "
					"where form_o.form_nr = ? and form_o.lila = ? "
					" order by zei ");
										
	test_upd_cursor = 1;

}

