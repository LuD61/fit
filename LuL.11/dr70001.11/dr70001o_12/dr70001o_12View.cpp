// dr70001o_12View.cpp : Implementierung der Klasse Cdr70001o_12View
//

#include "stdafx.h"
#include "dr70001o_12.h"

#include "dr70001o_12Doc.h"
#include "dr70001o_12View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cdr70001o_12View

IMPLEMENT_DYNCREATE(Cdr70001o_12View, CView)

BEGIN_MESSAGE_MAP(Cdr70001o_12View, CView)
	// Standarddruckbefehle
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// Cdr70001o_12View-Erstellung/Zerst�rung

Cdr70001o_12View::Cdr70001o_12View()
{
	// TODO: Hier Code zur Konstruktion einf�gen

}

Cdr70001o_12View::~Cdr70001o_12View()
{
}

BOOL Cdr70001o_12View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CView::PreCreateWindow(cs);
}

// Cdr70001o_12View-Zeichnung

void Cdr70001o_12View::OnDraw(CDC* /*pDC*/)
{
	Cdr70001o_12Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: Code zum Zeichnen der systemeigenen Daten hinzuf�gen
}


// Cdr70001o_12View drucken

BOOL Cdr70001o_12View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void Cdr70001o_12View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void Cdr70001o_12View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// Cdr70001o_12View-Diagnose

#ifdef _DEBUG
void Cdr70001o_12View::AssertValid() const
{
	CView::AssertValid();
}

void Cdr70001o_12View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

Cdr70001o_12Doc* Cdr70001o_12View::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(Cdr70001o_12Doc)));
	return (Cdr70001o_12Doc*)m_pDocument;
}
#endif //_DEBUG


// Cdr70001o_12View-Meldungshandler
