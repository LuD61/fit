// dr70001o_12Doc.cpp : Implementierung der Klasse Cdr70001o_12Doc
//

#include "stdafx.h"
#include "dr70001o_12.h"

#include "dr70001o_12Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cdr70001o_12Doc

IMPLEMENT_DYNCREATE(Cdr70001o_12Doc, CDocument)

BEGIN_MESSAGE_MAP(Cdr70001o_12Doc, CDocument)
END_MESSAGE_MAP()


// Cdr70001o_12Doc-Erstellung/Zerst�rung

Cdr70001o_12Doc::Cdr70001o_12Doc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

Cdr70001o_12Doc::~Cdr70001o_12Doc()
{
}

BOOL Cdr70001o_12Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// Cdr70001o_12Doc-Serialisierung

void Cdr70001o_12Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// Cdr70001o_12Doc-Diagnose

#ifdef _DEBUG
void Cdr70001o_12Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void Cdr70001o_12Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// Cdr70001o_12Doc-Befehle
