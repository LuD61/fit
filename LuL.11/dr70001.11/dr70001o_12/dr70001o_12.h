// dr70001o_12.h : Hauptheaderdatei f�r die dr70001o_12-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"       // Hauptsymbole


// Cdr70001o_12App:
// Siehe dr70001o_12.cpp f�r die Implementierung dieser Klasse
//

class Cdr70001o_12App : public CWinApp
{
public:
	Cdr70001o_12App();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern Cdr70001o_12App theApp;