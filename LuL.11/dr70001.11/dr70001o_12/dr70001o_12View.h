// dr70001o_12View.h : Schnittstelle der Klasse Cdr70001o_12View
//


#pragma once


class Cdr70001o_12View : public CView
{
protected: // Nur aus Serialisierung erstellen
	Cdr70001o_12View();
	DECLARE_DYNCREATE(Cdr70001o_12View)

// Attribute
public:
	Cdr70001o_12Doc* GetDocument() const;

// Vorgänge
public:

// Überschreibungen
public:
	virtual void OnDraw(CDC* pDC);  // Überschrieben, um diese Ansicht darzustellen
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~Cdr70001o_12View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // Debugversion in dr70001o_12View.cpp
inline Cdr70001o_12Doc* Cdr70001o_12View::GetDocument() const
   { return reinterpret_cast<Cdr70001o_12Doc*>(m_pDocument); }
#endif

