// dr70001o_12Doc.h : Schnittstelle der Klasse Cdr70001o_12Doc
//


#pragma once


class Cdr70001o_12Doc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	Cdr70001o_12Doc();
	DECLARE_DYNCREATE(Cdr70001o_12Doc)

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~Cdr70001o_12Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


