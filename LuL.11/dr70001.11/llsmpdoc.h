//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
//=============================================================================

// llsmpdoc.h : interface of the CLlSampleDoc class

class CLlSampleDoc : public CDocument
{
private:
	using CDocument::OnOpenDocument;

protected: // create from serialization only
	CLlSampleDoc();
	DECLARE_DYNCREATE(CLlSampleDoc)

// Attributes
public:

// Operations
public:

#ifdef FITODBC
	// Attributes
public:

	// current database
	CDatabase   m_Database;

	// table stuff
	CTables*    m_pTableset;
	BOOL        m_bSystemTables;
	BOOL        m_bViews;
	BOOL        m_bSynonyms;

	// column stuff
	CColumns*   m_pColumnset;
	BOOL        m_bLength;
	BOOL        m_bPrecision;
	BOOL        m_bNullability;

	// level possibilities
	enum Level
	{
		levelNone,
		levelTable,
		levelColumn
	};

	Level       m_nLevel;

// Operations
public:

//	void    SetLevel(Level nLevel);
	CString GetDSN();
	void    FetchColumnInfo(LPCSTR lpszName, LPCSTR lpszColName);	// 071205 : ColName
	BOOL    FetchTableInfo();
	void    OnOpenBWS();	// GrJ

protected:
	int     GetProfileValue(LPCTSTR lpszSection,LPCTSTR lpszItem);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCatalog2Doc)
	public:
//	virtual BOOL OnNewDocument();
//	virtual void Serialize(CArchive& ar);
	virtual void OnCloseDocument();
	//}}AFX_VIRTUAL
	virtual BOOL OnOpenDocument();

#endif

	// Implementation
public:
	virtual ~CLlSampleDoc();
	virtual void Serialize(CArchive& ar);	// overridden for document i/o
#ifdef _DEBUG
	virtual	void AssertValid() const;
	virtual	void Dump(CDumpContext& dc) const;
#endif
protected:
	virtual	BOOL	OnNewDocument();

// Generated message map functions
protected:
	//{{AFX_MSG(CLlSampleDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	afx_msg void OnViewSettings();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
