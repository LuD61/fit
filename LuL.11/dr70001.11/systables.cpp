#include "stdafx.h"
#include "DbClass.h"
#include "systables.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

struct SYSTABLES systables, systables_null;
extern DB_CLASS dbClass;
static long anzzfelder = -1;
struct NFORM_TAB nform_tab, nform_tab_null;
struct NFORM_HEA nform_hea, nform_hea_null;

int nformtab_curs , tabid_curs , nformhea_curs ;	// 030211 : nformhea dazu 

int SYSTABLES_CLASS::lesesystables ()
{

      int di = dbClass.sqlfetch (tabid_curs);

	  return di;
}

int NFORM_TAB_CLASS::lesenformtab (void)
{

      int di = dbClass.sqlfetch (nformtab_curs);

	  return di;
}

int NFORM_HEA_CLASS::lesenformhea (void)
{	// 030211

      int di = dbClass.sqlfetch (nformhea_curs);

	  return di;
}


int SYSTABLES_CLASS::opensystables (void)
{
        
 if ( tabid_curs < 0 ) prepare ();
//	prepare ();
	return dbClass.sqlopen (tabid_curs);
}

int NFORM_TAB_CLASS::opennformtab (void)
{
   
 if ( nformtab_curs < 0 ) prepare ();
//	prepare () ;
	return dbClass.sqlopen (nformtab_curs);
}

int NFORM_HEA_CLASS::opennformhea (void)
{	// 030211
   
 if ( nformhea_curs < 0 ) prepare ();
	return dbClass.sqlopen (nformhea_curs);
}


void SYSTABLES_CLASS::prepare (void)
{

#ifndef FITODBC
		dbClass.sqlin ((char *) systables.tabname, SQLCHAR, 19);

		dbClass.sqlout ((long *)  &systables.tabid, SQLLONG, 0);

		tabid_curs = dbClass.sqlcursor ("select tabid from systables where tabname = ?");
#endif
		test_upd_cursor = 1;



}

void NFORM_TAB_CLASS::prepare (void)
{

// formtab_curs : Welche tabelle fuer diese Format existieren
		dbClass.sqlin ((char *)   nform_tab.form_nr,   SQLCHAR,  11);
		dbClass.sqlin ((short *) &nform_tab.lila, SQLSHORT,0);

		dbClass.sqlout ((short *) &nform_tab.delstatus, SQLSHORT,0);
		dbClass.sqlout ((char *) nform_tab.tab_nam, SQLCHAR,19);
 
		nformtab_curs = dbClass.sqlcursor ("select delstatus,tab_nam from nform_tab"
			   " where form_nr = ? and lila = ? order by tab_nam");

}



void NFORM_HEA_CLASS::prepare (void)
{

// formhea_curs : KOpfsatz lesen ( wegen distinct ?! )
		dbClass.sqlin ((char *)   nform_hea.form_nr,   SQLCHAR,  11);
		dbClass.sqlin ((short *) &nform_hea.lila, SQLSHORT,0);

		dbClass.sqlout ((short *) &nform_hea.delstatus, SQLSHORT,0);
		dbClass.sqlout ((char *) nform_hea.form_ube, SQLCHAR,129);
 
		nformhea_curs = dbClass.sqlcursor ("select delstatus,form_ube from nform_hea"
			   " where form_nr = ? and lila = ? ");
}