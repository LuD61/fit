#pragma once
#include "afxwin.h"
#include "resource.h"
// Crange-Dialogfeld

// 140504 class Crange : public CPropertyPage
class Crange : public CDialog
{
	DECLARE_DYNAMIC(Crange)

public:
	Crange();
	virtual ~Crange();
	int getabbruch();
	int getanzahl();
	int getmehrfach_o() ;
	void putmehrfach_o(int);

// Dialogfelddaten
	enum { IDD = IDD_DIALOG1 };

protected:

	int mehrfach_o ;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnInitDialog();
	afx_msg BOOL PreTranslateMessage(LPMSG) ;	// 080408
	afx_msg BOOL NoListCtrl (CWnd *) ;			// 080408

	afx_msg void OnEnKillFocusEdit1();
	afx_msg void OnEnKillFocusEdit2();
//	Mustereintrag : afx_msg void OnEnChangeEdit3();
	afx_msg void OnEnKillFocusEdit3();
	afx_msg void OnEnKillFocusEdit4();
	afx_msg void OnEnKillFocusEdit5();
	afx_msg void OnEnKillFocusEdit6();
	afx_msg void OnEnKillFocusEdit7();
	afx_msg void OnEnKillFocusEdit8();
	afx_msg void OnEnKillFocusEdit9();
	afx_msg void OnEnKillFocusEdit10();
	afx_msg void OnEnKillFocusEdit11();
	afx_msg void OnEnKillFocusEdit12();
	afx_msg void OnEnKillFocusEdit13();
	afx_msg void OnEnKillFocusEdit14();
	afx_msg void OnEnKillFocusEdit15();
	afx_msg void OnEnKillFocusEdit16();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CString m_vcedit1;
	CString m_vcedit2;
	CString m_vcedit3;
	CString m_vcedit4;
	CString m_vcedit5;
	CString m_vcedit6;
	CString m_vcedit7;
	CString m_vcedit8;
	CString m_vcedit9;
	CString m_vcedit10;
	CString m_vcedit11;
	CString m_vcedit12;
	CString m_vcedit13;
	CString m_vcedit14;
	CString m_vcedit15;
	CString m_vcedit16;
	CString m_vcedit17;
	CString m_vcedit18;
	CString m_vcedit19;
	CString m_vcedit20;
	CString m_vcedit21;
	CString m_vcedit22;
	CString m_vcedit23;
	CString m_vcedit24;
	CEdit m_cedit1;
	CEdit m_cedit2;
	CEdit m_cedit3;
	CEdit m_cedit4;
	CEdit m_cedit5;
	CEdit m_cedit6;
	CEdit m_cedit7;
	CEdit m_cedit8;
	CEdit m_cedit9;
	CEdit m_cedit10;
	CEdit m_cedit11;
	CEdit m_cedit12;
	CEdit m_cedit13;
	CEdit m_cedit14;
	CEdit m_cedit15;
	CEdit m_cedit16;
	CEdit m_cedit17;
	CEdit m_cedit18;
	CEdit m_cedit19;
	CEdit m_cedit20;
	CEdit m_cedit21;
	CEdit m_cedit22;
	CEdit m_cedit23;
	CEdit m_cedit24;
	CEdit m_cedit25;
	CString m_vcedit25;
	CEdit m_cedit26;
	int m_vcedit26;
	afx_msg void OnCbnSelchangeCombo1();	// 270307
	afx_msg void OnCbnKillfocusCombo1();	// 270307
	CComboBox m_Combo1;
//	CString v_Combo1;

	char m_combo1;
};
