//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by llmfc.rc
//
#define IDR_MAINFRAME                   2
#define IDD_ABOUTBOX                    100
#define IDD_DIALOG1                     101
#define IDC_EDIT1                       101
#define IDC_EDIT2                       102
#define IDC_EDIT3                       103
#define IDC_EDIT4                       104
#define IDC_EDIT5                       105
#define IDC_EDIT6                       106
#define IDC_EDIT7                       107
#define IDC_EDIT8                       108
#define IDC_EDIT9                       109
#define IDC_EDIT10                      110
#define IDC_EDIT11                      111
#define IDC_EDIT12                      112
#define IDC_EDIT13                      113
#define IDC_EDIT14                      114
#define IDC_EDIT15                      115
#define IDC_EDIT16                      116
#define IDC_EDIT17                      117
#define IDC_EDIT18                      118
#define IDC_EDIT19                      119
#define IDC_EDIT20                      120
#define IDC_EDIT21                      121
#define IDC_EDIT22                      122
#define IDC_EDIT23                      123
#define IDC_EDIT24                      124
#define IDC_EDIT25                      125
#define IDC_EDIT26                      126
#define IDD_COLUMNPAGE                  130
#define IDD_TABLEPAGE                   133
#define IDC_SYSTABLES                   1000
#define IDC_ALIAI                       1001
#define IDC_VIEWS                       1002
#define IDC_LENGTH                      1003
#define IDC_PRECISION                   1004
#define IDC_NULLABILITY                 1005
#define IDC_COMBO1                      1022
#define IDC_COMBO2                      1023
#define ID_PRINT_LIST_PRINTER           32770
#define ID_PRINT_LIST_PREVIEW           32771
#define ID_PRINT_LABEL_PRINTER          32772
#define ID_PRINT_LABEL_PREVIEW          32773
#define ID_EDIT_LIST                    32774
#define ID_EDIT_LABEL                   32775
#define ID_FILE_DEBUG                   32776
#define ID_FILE_STOP_DEBUG              32778
#define ID_FILE_START_DEBUG             32779
#define ID_PRINT_REPORT                 32780
#define ID_PRINT_LABEL                  32781
#define ID_VIEW_TABLES                  32782
#define ID_VIEW_COLUMNINFO              32783
#define ID_VIEW_TABLELEVEL              32784
#define ID_VIEW_SETTINGS                32785
#define ID_VIEW_FORM_NR                 32786

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         32787
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
