//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
//=============================================================================

// llsmpdoc.cpp : implementation of the CLlSampleDoc class

#include "stdafx.h"


#include "catsets.h"
#ifndef OLABEL
#include "dbclass.h"
#endif
#include "llmfc.h"
#include "llsmpdoc.h"
#include "TabPage.h"
#include "ColPage.h"

#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

#include "mainFrm.h"	// 200509

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


#ifdef FITODBC
extern DB_CLASS dbClass ;
#endif
/////////////////////////////////////////////////////////////////////////////
// CLlSampleDoc

IMPLEMENT_DYNCREATE(CLlSampleDoc, CDocument)

BEGIN_MESSAGE_MAP(CLlSampleDoc, CDocument)
	//{{AFX_MSG_MAP(CLlSampleDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLlSampleDoc construction/destruction

CLlSampleDoc::CLlSampleDoc()
{
	// TODO: add one-time construction code here
		// initialize member recordset pointers
#ifdef FITODBC
	m_pTableset = 0;
	m_pColumnset = 0;
#endif
}

CLlSampleDoc::~CLlSampleDoc()
{
}

BOOL CLlSampleDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
		// initialize table settings
#ifdef FITODBC
	m_bSystemTables = GetProfileValue(_T("TableSettings"),_T("SystemTables"));
	m_bViews        = GetProfileValue(_T("TableSettings"),_T("Views"));
	m_bSynonyms     = GetProfileValue(_T("TableSettings"),_T("SystemTables"));

	// initialize column info settings
	m_bLength       = GetProfileValue(_T("ColumnInfoSettings"),_T("Length"));
	m_bPrecision    = GetProfileValue(_T("ColumnInfoSettings"),_T("Precision"));
	m_bNullability  = GetProfileValue(_T("ColumnInfoSettings"),_T("Nullability"));
#endif

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CLlSampleDoc serialization

void CLlSampleDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


/////////////////////////////////////////////////////////////////////////////
// CLlSampleDoc diagnostics

#ifdef _DEBUG
void CLlSampleDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLlSampleDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLlSampleDoc commands

#ifdef FITODBC

int CLlSampleDoc::GetProfileValue(LPCTSTR lpszSection,LPCTSTR lpszItem)
{
	int nValue = AfxGetApp()->GetProfileInt(lpszSection,lpszItem,-1);
	if (nValue == -1)
	{
		nValue = 0;
		AfxGetApp()->WriteProfileInt(lpszSection,lpszItem,nValue);
	}
	return nValue;
}

/* ---->
void CLlSampleDoc::SetLevel(Level nLevel)
{
	m_nLevel = nLevel;
	UpdateAllViews(NULL);
}
< ----- */

CString CLlSampleDoc::GetDSN()
{
	if (!m_Database.IsOpen())
		return _T("[No Data Source Selected]");

	// pull DSN from database connect string
	CString string = m_Database.GetConnect();
	string = string.Right(string.GetLength() - (string.Find(_T("DSN=")) + 4));
	string = string.Left(string.Find(_T(";")));
	return string;
}
// GrJ - alle cursoren nur einmalig generieren

void CLlSampleDoc::OnOpenBWS()
{
    if (dbClass.DBase.GetHdbc () == NULL)
	{
#ifdef FITSMART
		dbClass.opendbase ("fsm");

#endif

#ifndef FITSMART
#ifdef FITODBC
		if ( strlen( odb_daba) > 0 )
		{
			dbClass.opendbase ( odb_daba );
		}
		else
			dbClass.opendbase ("bws");

#endif
#ifndef FITODBC
		// ohne odbc : immer bws
		dbClass.opendbase ("bws");
#endif
#endif
	}

}

BOOL CLlSampleDoc::OnOpenDocument()
{
	// close and delete any open recordsets
	if (m_pTableset)
	{
		if (m_pTableset->IsOpen())
			m_pTableset->Close();
		delete m_pTableset;
		m_pTableset = 0;
	}
	if (m_pColumnset)
	{
		if (m_pColumnset->IsOpen())
			m_pColumnset->Close();
		delete m_pColumnset;
		m_pColumnset = 0;
	}

	// close the database
	if (m_Database.IsOpen())
		m_Database.Close();

	// open the database
/* --->
	if (m_Database.Open(NULL,FALSE,TRUE))
< ---- */


	OnOpenBWS ();	// GrJ
// 200509 A
	BOOL bstat = FALSE ;
#ifdef FITSMART
	bstat = m_Database.Open(_T( "fsm" ),FALSE,TRUE) ;
#else
#ifdef FITODBC
		if ( strlen( odb_daba) > 0 )
		{
			if ( odbdabatyp == FITORACLE )
			{
// 			bstat = m_Database.OpenEx(_T(";DSN=bwso;UID=bwso;PWD=t0pfit;" ), 6 ) ;
			char paramst [256] ;
			sprintf ( paramst , ";DSN=%s;UID=%s;PWD=%s;", odb_daba, odb_user, odb_pass ) ;
			bstat = m_Database.OpenEx(_T(paramst), 6 ) ;
			}
			else
			bstat = m_Database.Open(_T( odb_daba ),FALSE,TRUE) ;
		}
		else
			bstat = m_Database.Open(_T( "bws" ),FALSE,TRUE) ;
#else
		// ohne odbc : immer bws
	bstat = m_Database.Open(_T( "bws" ),FALSE,TRUE) ;
#endif
#endif



	if ( bstat )
	{
		if (FetchTableInfo())
			return TRUE;
	}
	return FALSE;
}

void CLlSampleDoc::OnCloseDocument()
{
	if (m_pTableset)
	{
		if (m_pTableset->IsOpen())
			m_pTableset->Close();
		delete m_pTableset;
		m_pTableset = 0;
	}
	if (m_pColumnset)
	{
		if (m_pColumnset->IsOpen())
			m_pColumnset->Close();
		delete m_pColumnset;
		m_pColumnset = 0;
	}

	if (m_Database.IsOpen())
		m_Database.Close();

	CDocument::OnCloseDocument();
}

void CLlSampleDoc::FetchColumnInfo(LPCSTR lpszName, LPCSTR lpszColName)
{
	if (m_pColumnset)
	{
		if (m_pColumnset->IsOpen())
			m_pColumnset->Close();
		delete m_pColumnset;
		m_pColumnset = 0;
	}
	m_pColumnset = new CColumns(&m_Database);
	m_pColumnset->Open(NULL,NULL,lpszName,lpszColName,CRecordset::snapshot);
}

BOOL CLlSampleDoc::FetchTableInfo()
{
	m_pTableset = new CTables(&m_Database);

	// Must use char array for ODBC interface
	// (can simply hard code max size)
	char lpszType[64];

	strcpy(lpszType, "'TABLE'");
	if (m_bViews)
		strcat(lpszType, ",'VIEW'");
	if (m_bSystemTables)
		strcat(lpszType, ",'SYSTEM TABLE'");
	if (m_bSynonyms)
		strcat(lpszType, ",'ALIAS','SYNONYM'");

	if (!m_pTableset->Open(NULL,NULL,NULL,lpszType,CRecordset::snapshot))
	{
		delete m_pTableset;
		m_pTableset = NULL;
		m_Database.Close();
		return FALSE;
	}
	return TRUE;
}

void CLlSampleDoc::OnViewSettings()
{
	CPropertySheet  sheet(_T("Settings"));
	CTablePage      pageTable;
	CColumnPage     pageColumn;

	// initialize and add table settings page
	sheet.AddPage(&pageTable);
	pageTable.m_bSystemTables = m_bSystemTables;
	pageTable.m_bViews = m_bViews;
	pageTable.m_bSynonyms = m_bSynonyms;

	// initialize and add column info settings page
	sheet.AddPage(&pageColumn);
	pageColumn.m_bLength = m_bLength;
	pageColumn.m_bPrecision = m_bPrecision;
	pageColumn.m_bNullability = m_bNullability;

	// execte property sheet and update settings
	if (sheet.DoModal() == IDOK) {
		BOOL    bTableModified = FALSE;
		BOOL    bColumnModified = FALSE;

		if (m_bSystemTables != pageTable.m_bSystemTables)
		{
			m_bSystemTables = pageTable.m_bSystemTables;
			AfxGetApp()->WriteProfileInt(_T("TableSettings"),
				_T("SystemTables"),m_bSystemTables);
			bTableModified = TRUE;
		}
		if (m_bViews != pageTable.m_bViews)
		{
			m_bViews = pageTable.m_bViews;
			AfxGetApp()->WriteProfileInt(_T("TableSettings"),
				_T("Views"),m_bViews);
			bTableModified = TRUE;
		}
		if (m_bSynonyms != pageTable.m_bSynonyms)
		{
			m_bSynonyms = pageTable.m_bSynonyms;
			AfxGetApp()->WriteProfileInt(_T("TableSettings"),
				_T("Synonyms"),m_bSynonyms);
			bTableModified = TRUE;
		}
		if (m_bLength != pageColumn.m_bLength)
		{
			m_bLength = pageColumn.m_bLength;
			AfxGetApp()->WriteProfileInt(_T("ColumnInfoSettings"),
				_T("Length"),m_bLength);
			bColumnModified = TRUE;
		}
		if (m_bPrecision != pageColumn.m_bPrecision)
		{
			m_bPrecision = pageColumn.m_bPrecision;
			AfxGetApp()->WriteProfileInt(_T("ColumnInfoSettings"),
				_T("Precision"),m_bPrecision);
			bColumnModified = TRUE;
		}
		if (m_bNullability != pageColumn.m_bNullability)
		{
			m_bNullability = pageColumn.m_bNullability;
			AfxGetApp()->WriteProfileInt(_T("ColumnInfoSettings"),
				_T("Nullability"),m_bNullability);
			bColumnModified = TRUE;
		}

		// check for table modification first
		if (bTableModified)
		{
			// close and delete any open recordsets
			if (m_pTableset)
			{
				if (m_pTableset->IsOpen())
					m_pTableset->Close();
				delete m_pTableset;
				m_pTableset = 0;
			}
			if (m_pColumnset)
			{
				if (m_pColumnset->IsOpen())
					m_pColumnset->Close();
				delete m_pColumnset;
				m_pColumnset = 0;
			}

			// refresh table data
			FetchTableInfo();
//			SetLevel(levelTable);
			UpdateAllViews(NULL);
		}

		// if table settings not modified, check column info
		else if (bColumnModified)
			UpdateAllViews(NULL);
	}
}

#endif