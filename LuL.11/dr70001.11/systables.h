#ifndef _SYSTABLES_DEF
#define _SYSTABLES_DEF

struct SYSTABLES {
  char     tabname[19];
  long     tabid;
};

extern int tabid_curs ;
extern struct SYSTABLES systables;

struct NFORM_TAB {
	short	delstatus;
	char    tab_nam[19];
	char    form_nr[11];
	short   lila;
};

extern int nformtab_curs ;
extern struct NFORM_TAB nform_tab;

// 030211 
struct NFORM_HEA {
	short	delstatus;
	char    form_ube[129];
	char    form_nr[11];
	short   lila;
};

extern int nformhea_curs ;
extern struct NFORM_HEA nform_hea;


class SYSTABLES_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//               int dbcount (void);
               int lesesystables (void);
               int opensystables (void);
               SYSTABLES_CLASS () : DB_CLASS ()
               {
				   tabid_curs = -1 ;
               }
};

class NFORM_TAB_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//               int dbcount (void);
               int lesenformtab (void);
               int opennformtab (void);
               NFORM_TAB_CLASS () : DB_CLASS ()
               {
				   nformtab_curs = -1 ;
               }
};

// 030211

class NFORM_HEA_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesenformhea (void);
               int opennformhea (void);
               NFORM_HEA_CLASS () : DB_CLASS ()
               {
				   nformhea_curs = -1 ;
               }
};


#endif