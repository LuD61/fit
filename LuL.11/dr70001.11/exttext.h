#ifndef _EXTTEXT_DEF
#define _EXTTEXT_DEF

#define MAXETEXTE 20

struct EXT_TEXT {
   long		zei[2];
   char     txt[260];
};

extern struct EXT_TEXT ext_text;

class EXT_TEXT_CLASS : public DB_CLASS
{
       private :
		   void setzedetails (int ,int , int ) ;	// sqlin setzen fuer select-felder
		   void holedetails (int ,int , int ) ;		// sqlout fuer select-felder und order-felder
		   void holewdetails( int, int, int, char*,char*) ;	// sqlin fuer where-felder
		   char * machewhere(char*,int,int, char *) ;		// string-substitution
       public :
			   int Prepare ( char *, int, int );
			   int Leseext_text (void);
               int Openext_text (int);
			   void satzinit(int);	// 071110 
               EXT_TEXT_CLASS () : DB_CLASS ()
               {
               }
};


// Mehr als 20 verschiedene Ext-Texte verbiete ich erst mal ( Performance )?! 

extern  char	et_tabnam [MAXETEXTE][19] ;	// tabname
extern  char	et_feldnam[MAXETEXTE][19] ;	// feldname
extern	int		et_cursors[MAXETEXTE] ;		// preparierte Cursoren Schluesselsuche
extern	int		et_cursorl[MAXETEXTE] ;		// preparierte Cursoren Textleserei

extern int		et_typ[MAXETEXTE] ;			// 071110 
extern  char	et_ttabnam [MAXETEXTE][19] ;	// 071110 : extern-tabname
extern char		et_tfeldnam[MAXETEXTE][5][19] ;	// 071110 : extern-feldname


// Die Dimension 15 ergibt sich vorerst aus folgender Ueberlegung 
// 5 select's				(0....4)
// 5 order's (im select)	(5....9)
// 5 where's				(10..14)

extern  int    et_datetyp[MAXETEXTE][15];	// Datentyp in der Daba
extern  int    et_datelen[MAXETEXTE][15];	// Datelength in der Daba
extern  void * et_sqlval [MAXETEXTE][15];	// pointer auf Feldinhalte(string,dates) 
extern long    et_sqllong[MAXETEXTE][15];	// Matrix fuer longs und shorts 
extern double  et_sqldouble[MAXETEXTE][15];	// Matrix fuer double 

extern int et_pwfeld[MAXETEXTE][5] ;	// MAXETEXTE Pointers ins Array je wfeld 
// es seien mal max. 10 substitue je whereklausel erlaubt 
extern int et_wpoint[MAXETEXTE][5][10] ;	// Where-Positionen im Statement


#endif

