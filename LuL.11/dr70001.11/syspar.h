#ifndef _SYSPAR_DEF
#define _SYSPAR_DEF

struct SYS_PAR {

char sys_par_nam[21];	// Texte lieber zu lang als zu kurz beim lesen
char sys_par_wrt[3];
char sys_par_besch[37];

long zei;
short delstatus;
};
extern struct SYS_PAR sys_par, sys_par_null;


class SYS_PAR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//              int dbcount (void);
               int lesesys_par (void);
               int opensys_par (void);
               SYS_PAR_CLASS () : DB_CLASS ()
               {
               }
};

#endif

