#include "stdafx.h"

#ifndef OLABEL
#include "DbClass.h"
#endif

#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif


#ifdef FITODBC
#include "MainFrm.h"
#include "catsets.h"
#include "llsmpdoc.h"
#include "llsmpvw.h"
#endif

#include "temptab.h"
#include "systables.h"
#include "syscolumns.h"
#include "form_ow.h"
#include "txt.h"
#include "range.h"
#include "item.h"
#include "exttext.h"

extern DB_CLASS dbClass;
extern int datenausdatei ;	// wird in MainFrm.cpp gesetzt
extern int mitrange ;		// 301105 : wird in MainFrm.cpp gesetzt
extern int externe_sortnr ;	// 280307 
extern int dialoggelaufen ;	// 240507
extern short GlobalLila ;	// wird in MainFrm.cpp gesetzt
extern int macherange ( int , char * , char * ) ;	// 300904 aus range.cpp
extern int holerangesausdatei(char *, char * , char *) ;	// 300904 aus MainFrm.cpp

// Mehr als 250 felder sollten nur sehr selten vorkommen 
char  tt_tabnam [MAXANZ][29];	// 220909 : 19-> 29 wegen langer Namen
int   tt_tabout [MAXANZ];
char  tt_feldnam[MAXANZ][29]; 	// 220909 : 19-> 29 wegen langer Namen
int    tt_datetyp[MAXANZ];		// Datentyp in der Daba
int    tt_datelen[MAXANZ];		// Stringlaenge in der Daba
int    tt_feldtyp[MAXANZ];	    // Datentyp fuer Generator
int    tt_referenz  [MAXANZ];	// nur Index auf referenz-Liste
int    tt_range     [MAXANZ];	// nur Index auf range-Liste 
char * tt_rangin	[MAXANZ];	// referenz like tt_range 	 
int    tt_rangini	[MAXANZ];	// 220909 : typ von rangin ; 0 = nix , 1 ="in()" , 2 = freie Syntax 	 
void * tt_sqlval    [MAXANZ];	// pointer auf (char-)Feldinhalte 
long	tt_sqllong    [MAXANZ];	// Matrix fuer longs und shorts 
double  tt_sqldouble [MAXANZ];	// Matrix fuer double 

void * savtt_sqlval    [MAXANZ];	// 300506 : pointer auf (char-)Feldinhalte 
long	savtt_sqllong    [MAXANZ];	// 300506 : Matrix fuer longs und shorts 
double  savtt_sqldouble [MAXANZ];	// 300506 : Matrix fuer double 

 // mehr als 50 verschiedene Textbausteine sollten sehr selten sein
char  tt_creferenz[100][25];	// 141209 : aus 50 wird 100 

// mehr als 20 verschiedene Bedingungen sind nicht erlaubt

TIMESTAMP_STRUCT  tt_tvrange[MAXRANGE]; 
TIMESTAMP_STRUCT  tt_tbrange[MAXRANGE];

char  tt_cvrange[MAXRANGE][50];	// maximale stringlaenge  49
char  tt_cbrange[MAXRANGE][50];
long  tt_lvrange[MAXRANGE];
long  tt_lbrange[MAXRANGE];
double  tt_dvrange[MAXRANGE];
double  tt_dbrange[MAXRANGE];
char * tt_rangtyp2[MAXRANGE] ;	// 230909

FORM_V_CLASS Form_v_class;	// 201213
char virtuellklausel[3076] ; // 201213 




#ifdef FITODBC
CLlSampleDoc* pDoc ;
#endif

static int aktupoint ;
static int v_abbruch ;
static int v_anzahl ;
int TEMPTAB_CLASS::structprep (char * myform_nr)
{

	int refdi = 0 ;	// Pointer zu Referenztexten
	int rangdi = 0 ;	// Pointer zu Ranges
	aktupoint = 0 ;

	NFORM_TAB_CLASS Nform_tab_class ;
	NFORM_HEA_CLASS Nform_hea_class ;	// 030211
	SYSTABLES_CLASS Systables_class ;
	NFORM_FELD_CLASS Nform_feld_class ;
	SYSCOLUMNS_CLASS Syscolumns_class ;

#ifdef FITODBC

//	CLlSampleView *pView = (CLlSampleView *) GetActiveView();
//	CLlSampleDoc* pDoc = pView->GetDocument();

//	CLlSampleDoc* pDoc = CLlSampleView::GetDocument();
	ASSERT_VALID(pDoc);
	pDoc->OnOpenDocument() ;
#endif


// 030211 : ddistinct setzen A

	ddistinct = 0 ;
	sprintf (nform_hea.form_nr ,"%s", myform_nr );
	nform_hea.lila = GlobalLila ;
	Nform_hea_class.opennformhea ();
	if (!Nform_hea_class.lesenformhea())	// gefunden
		ddistinct = nform_hea.delstatus ;

// 030211 : ddistinct setzen E

	tt_tabnam [0][0] = '\0';

	sprintf ( nform_tab.form_nr , "%s", myform_nr );
	nform_tab.lila = GlobalLila ;


	Nform_tab_class.opennformtab();
	while (!Nform_tab_class.lesenformtab () && aktupoint < 249 )	//       DATENSATZ lesen
	{
		sprintf ( systables.tabname, "%s", nform_tab.tab_nam );
//		SYSTABLES_CLASS Systables_class ;
#ifndef FITODBC 
		Systables_class.opensystables ();
		if ( ! Systables_class.lesesystables ())	// Test,ob es die Tabelle gibt 
		{
#endif
#ifdef FITODBC
			// ANNAHME : ES WURDE Ordentlich gearbeitet 
       if ( TRUE )
	   {
#endif
			sprintf ( nform_feld.tab_nam ,"%s", nform_tab.tab_nam) ;
			sprintf ( nform_feld.form_nr ,"%s", nform_tab.form_nr) ;
			nform_feld.lila = GlobalLila ;

//			NFORM_FELD_CLASS Nform_feld_class ;
			Nform_feld_class.opennform_feld() ;
			while ( ! Nform_feld_class.lesenform_feld ())	// Alle aktiven Felder dieser Tabelle
			{
				syscolumns.tabid = systables.tabid ;
				sprintf ( syscolumns.colname ,"%s", nform_feld.feld_nam) ;
//				SYSCOLUMNS_CLASS Syscolumns_class ;
#ifndef FITODBC
				Syscolumns_class.opensyscolumns ();
				if (! Syscolumns_class.lesesyscolumns()) 
				{
#endif
#ifdef FITODBC
				 pDoc->FetchColumnInfo(clipped(nform_feld.tab_nam)
		                          ,clipped(nform_feld.feld_nam) ) ;
					pDoc->m_pColumnset->MoveFirst();
					if (!pDoc->m_pColumnset->IsEOF())
					{
						// gefunden -> einigermassen unkrititsche Zuordnung

						syscolumns.coltype = (short)pDoc->m_pColumnset->m_nDataType ;
						syscolumns.collength = (short)pDoc->m_pColumnset->m_nLength ;
						switch (syscolumns.coltype)
						{
						case SQL_UNKNOWN_TYPE :
						case SQL_NUMERIC :
						case SQL_CHAR :
						case iSQL_VARCHAR  : //	280612 SQL_VARCHAR gibbet noch nicht gescheit  ?!
							syscolumns.coltype = iDBCHAR ;
							break ;

						case iDBSERIAL :		// 201209 zum lesen als integer behandeln 
						case SQL_INTEGER :
							syscolumns.coltype = iDBINTEGER ;
							break ;

						case SQL_SMALLINT : 
							syscolumns.coltype = iDBSMALLINT ;
							break ;

						case SQL_DECIMAL :
						case SQL_FLOAT :
						case SQL_REAL :
						case SQL_DOUBLE :
								syscolumns.coltype = iDBDECIMAL ;
								break ;

						case SQL_DATE :
								syscolumns.coltype = iDBDATUM ;
								break ;
// 111205
						case SQL_TIMESTAMP :
						case SQL_TIME :			// 140214

								syscolumns.collength = 0x0f ;
								syscolumns.collength = (short)pDoc->m_pColumnset->m_nPrecision ;
								// hier nur per Experiment :
								// 10 == year,monat+Tag
								// 19 == Datum + Zeit bis sekunde
								// 21 == Datum + Zeit + frac(1)
								// 25 == Datum+Zeit+Frac(5)

								if ( syscolumns.collength == 25 )
									syscolumns.collength = 0x0f;
								else
								if ( syscolumns.collength == 24 )
									syscolumns.collength = 0x0e;
								else
								if ( syscolumns.collength == 23 )
									syscolumns.collength = 0x0d;
								else
								if ( syscolumns.collength == 22 )
									syscolumns.collength = 0x0c;
								else
								if ( syscolumns.collength == 21 )
									syscolumns.collength = 0x0b;
								else

								if ( syscolumns.collength < 11 )	// Datum pur
									syscolumns.collength = 4;
								else
								if ( syscolumns.collength < 20 )	// Datum + Zeit
										syscolumns.collength = 10;


								syscolumns.coltype = iDBDATETIME ;
								break ;

						}
					
					
					}
					else
					{	// negativer Notnagel 
						syscolumns.coltype = iDBCHAR ;
						syscolumns. collength = 100L ;
					}
				if (TRUE)	// ANNAHME : ES WURDE korrekt gearbeitet 
				{
#endif
//	Matritzen  laden ;

					sprintf ( tt_tabnam [aktupoint],"%s",clipped( nform_feld.tab_nam));
					tt_tabout [aktupoint] = (int) nform_tab.delstatus ;
					sprintf ( tt_feldnam[aktupoint],"%s",clipped( nform_feld.feld_nam)); 	
#ifndef FITODBC		// 140214 : da war noch was ...
					if ( syscolumns.coltype == SQL_TIME || syscolumns.coltype == SQL_TIMESTAMP )
							syscolumns.coltype = iDBDATETIME;
#endif

					tt_datetyp[aktupoint] = syscolumns.coltype ;	// Datentyp in der Daba
					tt_datelen[aktupoint] = syscolumns.collength ;	// Datenlaenge in der Daba
					tt_feldtyp[aktupoint] = nform_feld.feld_typ ;	// Datentyp fuer Generator
																	// 260207 : nur noch fuer Textfeld // 091209 auch fuer ptabn 
					if ( strlen ( clipped( nform_feld.krz_txt)) > 0 && ( nform_feld.feld_typ == 2 || nform_feld.feld_typ == 1 ))
					{
						refdi ++ ;	// Achtung : index geht erst mit Feld 1 los ... 
						sprintf ( tt_creferenz[refdi], "%s", clipped(nform_feld.krz_txt));
						tt_referenz [aktupoint] = refdi ;
					}
					else
						tt_referenz [aktupoint] = 0 ;

					if ( nform_feld.range )
					{
						rangdi ++ ;	// Achtung : index geht erst mit Feld 1 los ... 
						tt_referenz [aktupoint] = rangdi ;
}
					else
					{
					}
					tt_range[aktupoint] = nform_feld.range ;	
					aktupoint ++ ;
					tt_tabnam[aktupoint][0] = '\0' ;
				}
			}	// fetch nformfeld_curs
		}		// if tabid_curs
	}			// nformtab_curs 
// 201213 A

	form_v.lila = GlobalLila ;
	sprintf ( form_v.form_nr,"%s", myform_nr );
	Form_v_class.openform_v() ;
	virtuellklausel[0] = '\0'; //201213
	while ( ! Form_v_class.leseform_v ())
	{
		if (aktupoint < 249 )	// Überlauf ....
		{
//	Matritzen  laden ;
			sprintf ( tt_tabnam [aktupoint],"ali");
			tt_tabout [aktupoint] = -9 ;
			sprintf ( tt_feldnam[aktupoint],"%s",clipped( form_v.klar_txt)); 	
			tt_datetyp[aktupoint] = form_v.typ ;	// Datentyp in der Daba
			tt_datelen[aktupoint] = form_v.vk ;		// Datenlaenge in der Daba
			tt_feldtyp[aktupoint] = 0 ;				// Datentyp fuer Generator(dummy)
			tt_referenz [aktupoint] = 0 ;

			tt_range[aktupoint] = 0 ;	
			aktupoint ++ ;
			tt_tabnam[aktupoint][0] = '\0' ;
			int jj;
			jj = (int) strlen( virtuellklausel);
//			if ( form_v.typ == iDBDATETIME )	bisher NICHT unterstützt

			if ( form_v.typ == iDBCHAR )
				sprintf ( virtuellklausel + jj , ",cast(%s as char(%d)) %s ", clipped(form_v.krz_txt), form_v.vk, form_v.klar_txt);
			if ( form_v.typ == iDBDATUM )
				sprintf ( virtuellklausel + jj , ",cast(%s as date) %s ", clipped(form_v.krz_txt),form_v.klar_txt);
			if ( form_v.typ == iDBSMALLINT )
				sprintf ( virtuellklausel + jj , ",cast(%s as smallint) %s ", clipped(form_v.krz_txt), form_v.klar_txt);
			if ( form_v.typ == iDBINTEGER )
				sprintf ( virtuellklausel + jj , ",cast(%s as integer) %s ", clipped(form_v.krz_txt), form_v.klar_txt);
			if ( form_v.typ == iDBDECIMAL )
				sprintf ( virtuellklausel + jj , ",cast(%s as decimal(%d,%d)) %s " , clipped(form_v.krz_txt), form_v.vk, form_v.nk, form_v.klar_txt);
		}
	}			// leseform_v 
// 201213 E


	return aktupoint ;
}

int rangeklausel ( int k , char * p )
{
	p = NULL ;

	//  tt_tabnam[k], tt_feldnam[k] , clipped ( tt_rangin[k]) tt_datetyp[k] ;
// die Klausel sollte dann so aussehen  :
// >>(( name between w1 und w2 ) or ( name > wx ) or ( name < wx ) or (name = w1) or (name = w2) or (...) )<<	

	return 0 ;
}

int TEMPTAB_CLASS::getanzahl (void)
{
	return v_anzahl ;
}


void TEMPTAB_CLASS::reloadtemptab ()
{
	int i ;
	i=0;
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;
		switch ( tt_feldtyp[i] )
		{
		case FT_PTABN : // ptabn
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
			case(iDBVARCHAR):	// 280612
				sprintf ( (char*)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			case(iDBSMALLINT):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;

			case(iDBSERIAL):	// 201209
			case(iDBINTEGER):
				tt_sqllong[i] = savtt_sqllong[i] ;	
				break ;
			case(iDBDECIMAL):
				tt_sqldouble[i] = savtt_sqldouble[i] ;
				break ;
			default :	// schlechte Notbremse
				sprintf ( (char*)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			}
			break ;

		case FT_TEXTBAUST : // Textbaustein 
// 200509 : nicht mehr fix long sondern variabeler
			switch ( tt_datetyp[i] )
			{
			 case(iDBVARCHAR):	// 280612
			 case(iDBCHAR):
				sprintf ( (char*)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			 case(iDBSMALLINT):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;

			 case(iDBSERIAL):	// 201209
			 case(iDBINTEGER):
				tt_sqllong[i] = savtt_sqllong[i] ;	
				break ;
			 case(iDBDECIMAL):
				tt_sqldouble[i] = savtt_sqldouble[i] ;
				break ;
			default :	// so war es bisher immer
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			}
			break ;

		case FT_TEXTEXT : // 220207 : Ext-Textbaustein 

// 200509 : nicht mehr fix long sondern variabeler
			switch ( tt_datetyp[i] )
			{
			 case(iDBCHAR):
			 case(iDBVARCHAR):	// 280612
				sprintf ( (char*)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			 case(iDBSMALLINT):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;

			 case(iDBSERIAL):	// 201209
			 case(iDBINTEGER):
				tt_sqllong[i] = savtt_sqllong[i] ;	
				break ;
			 case(iDBDECIMAL):
				tt_sqldouble[i] = savtt_sqldouble[i] ;
				break ;
			default :	// so war es bisher immer
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			}
			break ;
			
		case 3 : // adresse
// 200509 : nicht mehr fix long sondern variabeler
			switch ( tt_datetyp[i] )
			{
			 case(iDBCHAR):
			 case(iDBVARCHAR):	// 280612
				sprintf ( (char*)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			 case(iDBSMALLINT):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;

			 case(iDBSERIAL):	// 201209
			 case(iDBINTEGER):
				tt_sqllong[i] = savtt_sqllong[i] ;	
				break ;
			 case(iDBDECIMAL):
				tt_sqldouble[i] = savtt_sqldouble[i] ;
				break ;
			default :	// so war es bisher immer
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			}
			break ;

		default :	// Standard-Felder 
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
			case(iDBVARCHAR):	// 280612
				sprintf ( (char*)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			case(iDBSMALLINT):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;

			case(iDBSERIAL):	// 201209
			case(iDBINTEGER):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			case(iDBDECIMAL):
				tt_sqldouble[i] = savtt_sqldouble[i] ;
				break ;

			case (iDBDATETIME) :
				// 310506 : ab jetzt getrennt handeln 
				memcpy ( tt_sqlval[i] ,savtt_sqlval[i], sizeof ( TIMESTAMP_STRUCT)) ;	// 310506
				break ;		

			case(iDBDATUM):
				sprintf ( (char*)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;

			default :	// schlechte Notbremse
				sprintf ( (char*)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
					break ;
			
			}
			break ;
		} ;
		i ++ ;
	}		// while	

}


// 121104 A
// 300506 : save des vorherigen Wertes dazu  
void TEMPTAB_CLASS::inittemptab ()
{
	int i ;
	i=0;
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;
		switch ( tt_feldtyp[i] )
		{
		case 1 : // ptabn
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
			case(iDBVARCHAR):	// 280612
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
// 300506 : so sah das bisher aus :				tt_sqlval[i], (char *)NULL ;
 				sprintf ( (char *)tt_sqlval[i] ," " ) ;	// 310506

				break ;
			case(iDBSMALLINT):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 300506
				tt_sqllong[i] = 0 ;
				break ;

			case(iDBSERIAL):	// 201209
			case(iDBINTEGER):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 300506
				tt_sqllong[i] = 0 ;
				break ;
			case(iDBDECIMAL):
				savtt_sqldouble[i] = tt_sqldouble[i] ;	// 300506
				tt_sqldouble[i] =0.0L ;
				break ;
			default :	// schlechte Notbremse
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
 				sprintf ( (char *)tt_sqlval[i] ," " ) ;
						break ;
			}
//			memcpy ( &ptabn,&ptabn_null, sizeof(struct PTABN));

			break ;

/* -----> komplett mit rein in die default-felder
		case 2 : // Textbaustein 
				savtt_sqllong[i] = tt_sqllong[i] ;	// 300506
				tt_sqllong[i] = 0 ;
			break ;

		case 3 : // adresse
			savtt_sqllong[i] = tt_sqllong[i] ;	// 300506
			tt_sqllong[i] = 0 ;
//				memcpy ( &adr,&adr_null, sizeof(struct ADR));
			break ;
< ----- */

		case 4 : // Ext-Textbaustein soll mit in die default-Felder reinlaufen 

		default :	// Standard-Felder 
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
			case(iDBVARCHAR):	// 280612
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
						break ;
			case(iDBSMALLINT):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 300506
				tt_sqllong[i] = 0L ;
				break ;

			case(iDBSERIAL):	// 201209
			case(iDBINTEGER):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 300506
				tt_sqllong[i] = 0L ;
				break ;
			case(iDBDECIMAL):
				savtt_sqldouble[i] = tt_sqldouble[i] ;	// 300506
				tt_sqldouble[i]= 0.0L ;
				break ;

			case (iDBDATETIME) :		// 111205
				// 310506 : ab jetzt getrennt handeln 
				memcpy ( savtt_sqlval[i] ,tt_sqlval[i], sizeof (  TIMESTAMP_STRUCT)) ;	// 310506
				sprintf ( (char *)tt_sqlval[i]," ")  ;
				break ;		
			case(iDBDATUM):
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i]," ")  ;
				break ;		
			default :	// schlechte Notbremse
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i], " ");
				break ;
			}
			break ;
		} ;
		i ++ ;
	}		// while	

}


int TEMPTAB_CLASS::lesetemptab (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;   
	  }

// weg von hier 220506	  inittemptab ();	//121104
// wieder hier hin 300506
	  inittemptab();
	  int di = dbClass.sqlfetch (readcursor);
		if  (di )
			reloadtemptab() ;	// 300506 
	  return di;
}

int TEMPTAB_CLASS::opentemptab (void)
{
	if ( readcursor < 0 )
	{
	}
	else
	{
		dbClass.sqlclose(readcursor) ;
	}
		 prepare () ;
		 if ( v_abbruch ) return ( -3) ;

// wieder weg 300506		inittemptab ();	//220506 : nur einmalig 

         return dbClass.sqlopen (readcursor);
}


// Schreiben der Werte like rangedialog
void schreiberangesausdatei(void)
{
int retco ;
int l = 0 ;
char token[125],wert1[299],wert2[1030] ;	// 280508 : bisher nur 128 Zeichen lang

	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( ! tt_range[i]) continue ;

		sprintf ( token, "%s",tt_feldnam[i]);
		int j = holerangesausdatei( token, wert1,  wert2);

		l ++ ;
		tt_range[i] = l ;

/* ----> 220909 : nur noch zentral abarbeiten 
=		if (! strncmp ( wert1, "INRANGE" ,7 ))	// 280508
=		{
=			char * p = (char *) malloc (strlen ( wert2 )+ 3 ) ;
=			sprintf ( p , "%s", wert2 ) ;
=			tt_rangin[i] = p ;
=			tt_rangini[i] = 1 ;	// 220909
=		}
=		else	// 280508 : so war es bisher
< ----- */

			retco = macherange( i, wert1, wert2 ) ;

	}
}


// hier komplette Struktur UND Cursor neu aufbauen
void TEMPTAB_CLASS::prepare (void)
{

int i,j,k ;
int mehrfach_o ;		// nach openform_o :
						// -1 keine Order-Klausel
						// 0 genau eine Klausel(alter Ablauf
						// 1....n : n Klauseln vorhanden 
						// Nach Dialog : -1 oder 0 oder 1 ... n : die jeweilige Klausel

char statementpuffer[5101];	// informix kann wohl im Standard 4096 2012 : von 4101 auf 5101 
char tabellenpuffer[1025];	// sollte fuer > 50 Tabellennamen reichen
char grupptab[30] ;			// 20->30	
char wherefeld[15] ;
char whereklausel[3076] ;	// 021204 von 1005 auf 2076 + 1000
char groupklausel[2005] ;	// jetzt nicht mehr : nur dummy 1005-> 2005
char orderklausel[3076] ;	// 021204 von 1005 auf 2076 + 1000
char klausel[3076] ;		// 021204 von 1005 auf 2076 + 1000
// Syntax der virtuell-Klausel : 
//    fffffffffvvvvvvvvvvvvvvvvvvvvvvvvvvfffffvvvvvvvvvvvvvffvvvvvvvvv
//    , cast ( sum(ls_vk_euro * lief_me )  as decimal(10,2)) klarname
// ff = fixe Komponenten, vv = variable Komponenten


extern ITEM_CLASS Item ;
Item.openitem();

// falls Feldueberlauf kann es schon mal knallen ..

for (i = 0 ; i < MAXANZ ; i++)	// 191006 
{
	tt_sqlval[i]	= ( char * )0 ;
	savtt_sqlval[i] = ( char * )0 ;
	tt_rangin [i]   = NULL ;
	tt_rangini[i]   = 0 ;	// 220909
}

#ifndef FITSMART					// 240507
for (i = 0 ; i < MAXETEXTE ; i++)	// 280207 
{

	// Anfangsinit, muss beim jeweils ersten Zugriff komplettiert werden 

	et_cursors[i]    = -1 ;
	et_datetyp[i][0] = -1 ;
	et_typ[i] = 0 ;	// 071110 

	et_feldnam[i][0] = '\0' ;
	et_tabnam [i][0] = '\0' ;

	et_ttabnam [i][0] = '\0' ;	// 071110 : extern-tabname
	for ( int iii = 0 ; iii < 5  ; iii ++ )
	{
		et_tfeldnam[i][iii][0] = '\0' ;	// 071110 : extern-feldname
	}

	for ( j = 0 ; j < 15 ; j++ )
	{
		et_sqlval[i][j]	= ( char * )0 ;
	}
}
#endif	// !FITSMART

i=j=k= 0 ;


// memset ( statementpuffer, '\0', size_t(statementpuffer));

sprintf ( wherefeld, " where ");	// Anfangsinit
if ( ddistinct )	// 030211   12345678901234567
	sprintf ( statementpuffer, " select distinct ");
else
	sprintf ( statementpuffer, " select ");
// 121104
if ( ! tt_tabout[0] )
    sprintf ( tabellenpuffer, "%s", clipped ( tt_tabnam[0] ));
else
{
	if ( tt_tabout[0] != -9 )	// 201213 
	{
		sprintf ( tabellenpuffer, " outer %s", clipped ( tt_tabnam[0] ));
	}
}
sprintf ( grupptab, "%s", clipped ( tt_tabnam[0] ));
whereklausel[0] = '\0' ;
groupklausel[0] = '\0' ;
orderklausel[0] = '\0' ;


	sprintf ( form_w.form_nr,"%s" , nform_feld.form_nr );
	sprintf ( form_o.form_nr,"%s" , nform_feld.form_nr );

	form_w.lila = GlobalLila ;
	form_o.lila = GlobalLila ;

	FORM_O_CLASS Form_o_class ;
	Form_o_class.openform_o ();
	mehrfach_o = Form_o_class.getmehrfach_o();
//	Form_o_class.leseform_o(); 

	FORM_W_CLASS Form_w_class ;
	Form_w_class.openform_w ();
//	Form_w_class.leseform_w(); 



	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;

		j = (int) strlen( statementpuffer ) ;
		if ( tt_tabout[i] == -9 )
		{
			// 201213 : die ali-Felder werden nicht an dieser Stelle in die klausel eingetragen
		}
		else
		{
			if ( ( j < 10 && ! ddistinct ) || ( j < 19 &&  ddistinct ) )	// erstes element + 030211
				sprintf ( statementpuffer + j, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);
			else
				sprintf ( statementpuffer + j, ",%s.%s", tt_tabnam[i], tt_feldnam[i]);
		}

		switch ( tt_datetyp[i] )
		{
			case(iDBCHAR):
			case(iDBVARCHAR):	// 280612
	
				if (tt_sqlval[i] != (char *) 0)
				{
					free ( tt_sqlval[i]) ;
					tt_sqlval[i] = (char *) 0;
				}
				if (tt_sqlval[i] == (char *) 0) tt_sqlval[i] = (char *) malloc (tt_datelen[i]+2);

				if (savtt_sqlval[i] != (char *) 0)
				{
					free ( savtt_sqlval[i]) ;
					savtt_sqlval[i] = (char *) 0;
				}
				if (savtt_sqlval[i] == (char *) 0) savtt_sqlval[i] = (char *) malloc (tt_datelen[i]+2);	// 310506
				sprintf ( (char*) tt_sqlval[i] ,"" ) ; sprintf ( (char*) savtt_sqlval[i],"") ;	// 191006
				dbClass.sqlout ((char *)  tt_sqlval[i], SQLCHAR , tt_datelen[i]+1 );
				break ;
			case(iDBSMALLINT):	// smallint
//				dbClass.sqlout ((short *)  &tt_sqllong[i], SQLSHORT, 0);
				dbClass.sqlout ((long *)  &tt_sqllong[i], SQLLONG, 0);
				break ;

			case(iDBSERIAL):	// 201209
			case(iDBINTEGER):	// integer
				dbClass.sqlout ((long *)  &tt_sqllong[i], SQLLONG, 0);
				break ;
			case(iDBDECIMAL):	// decimal
				dbClass.sqlout ((double *)  &tt_sqldouble[i], SQLDOUBLE, 0);
				break ;
			case(iDBDATUM):	// Datumsfeld
				if (tt_sqlval[i] != (char *) 0)
				{
					free ( tt_sqlval[i]) ;
					tt_sqlval[i] = (char *) 0;
				}
				if (tt_sqlval[i] == (char *) 0) tt_sqlval[i] = (char *) malloc (12);
// 310506 : Platz allocieren
				if (savtt_sqlval[i] != (char *) 0)
				{
					free ( savtt_sqlval[i]) ;
					savtt_sqlval[i] = (char *) 0;
				}
				if (savtt_sqlval[i] == (char *) 0) savtt_sqlval[i] = (char *) malloc (12);
				sprintf ( (char*) tt_sqlval[i] ,"" ) ; sprintf ( (char*) savtt_sqlval[i],"") ;	// 191006
				dbClass.sqlout ((char *)  tt_sqlval[i], SQLCHAR , 11 );
				break ;

/* ----->
			case(iDBDATETIME):	// 111205

				if (tt_sqlval[i] != (char *) 0)
				{
					free ( tt_sqlval[i]) ;
					tt_sqlval[i] = (char *) 0;
				}
				if (tt_sqlval[i] == (char *) 0) tt_sqlval[i] = (char *) malloc (28);

				dbClass.sqlout ((char *)  tt_sqlval[i], SQLCHAR , 27 );
				break ;
< ------ */
			case(iDBDATETIME):	// 111205

				if (tt_sqlval[i] != NULL )
				{
					free ( tt_sqlval[i]) ;
					tt_sqlval[i] = NULL ;
				}
				if (tt_sqlval[i] == NULL) 
					 tt_sqlval[i] =  malloc (sizeof(TIMESTAMP_STRUCT));

// 310506 : Platz allocieren
				if (savtt_sqlval[i] != NULL )
				{
					free ( savtt_sqlval[i]) ;
					savtt_sqlval[i] = NULL ;
				}
				if (savtt_sqlval[i] == NULL) 
					 savtt_sqlval[i] =  malloc (sizeof(TIMESTAMP_STRUCT));
				sprintf ( (char*) tt_sqlval[i] ,"" ) ; sprintf ( (char*) savtt_sqlval[i],"") ;	// 191006
				// 191006 : dann steht wenigstens nur eine  0x0 in der Struktur ;-)
//				dbClass.sqlout (tt_sqlval[i], SQLTIMESTAMP , 0 );
				dbClass.sqlout ( tt_sqlval[i], SQLTIMESTAMP , 26 );	// 140214
				break ;

			default :
				break ;

		} ;	// switch

		if ( tt_range[i] )	k++ ;
		

		if (( ! strncmp ( grupptab , tt_tabnam[i], strlen( grupptab)))
			&& strlen ( grupptab)== strlen(tt_tabnam[i] ))
		{
		}
		else
		{
			j = (int) strlen( tabellenpuffer );
			// 121104 
			if ( ! tt_tabout[i] )
				sprintf ( tabellenpuffer + j,",%s" , tt_tabnam[i]);
			else
			{
				if ( tt_tabout[i] != -9 )	// 201213 erweiterte Bedingung
				{
					sprintf ( tabellenpuffer + j, " , outer %s",  tt_tabnam[i]);
				}
			}

			sprintf ( grupptab , "%s" , tt_tabnam[i]);

		}

		i ++ ;
	}		// while	

	if ( i )
	{

// 1. Testphase 		sprintf ( whereklausel , " where a_bas.a = a_kun.a ");	// zu testzwecken

// 2. Testphase
//		sprintf ( wherefeld, " where ");

	dialoggelaufen = 0 ;	// 240507 
	if ( k )
	{
			Crange rangedial;
				// 301105 : mitrange dazu 
			if (! datenausdatei || mitrange )
			{
			  dialoggelaufen = 1 ;
			  rangedial.putmehrfach_o(mehrfach_o) ;
			  rangedial.DoModal();
			  mehrfach_o = rangedial.getmehrfach_o();
			  v_abbruch = rangedial.getabbruch();
			  v_anzahl = rangedial.getanzahl();
			}
			else
			{
				v_abbruch = 0 ;
				schreiberangesausdatei() ;
				//  280307 : keine Vorgabe, mehrere Selektionsvarianten aber kein
				// Dialog -> also 1. Selektion 
				if ( externe_sortnr < 1 )
					if ( mehrfach_o > 0 )
						externe_sortnr = mehrfach_o = 1 ;
			}

			// 280307 : gleichziehen Vorgabe und Aktionswert
			if ( externe_sortnr > 0 )
				if ( externe_sortnr <= mehrfach_o )
					mehrfach_o = externe_sortnr ;

		for ( k= 0 ; tt_tabnam[k][0] != '\0' ;k ++ )
		{
			if ( ! tt_range[k] ) continue ;

			j = (int) strlen( whereklausel ) ;

// 280508 A : "in" - Klausel
			if (  tt_rangin [k] != NULL )
			{
				if ( tt_rangini[k] == 1 )	// 220909  : ab heute mit unterschiedlichen Typen ...
				{
// beachte : Syntax muss absolut stimmen ( keine Blanks, Trenner mit Komma usw.)
					sprintf ( whereklausel + j, " %s %s.%s  in ( %s ) ",
						wherefeld , tt_tabnam[k], tt_feldnam[k] , clipped ( tt_rangin[k]));
						sprintf ( wherefeld, " and") ;
						continue ;
				}
				if ( tt_rangini[k] == 2 )	// 220909 : die eigentliche Arbeit incl. Check wird in rangeklausel gemacht 				{ 
				{
						sprintf ( whereklausel + j, " %s %s ", wherefeld , tt_rangtyp2[tt_range[k]] );
						sprintf ( wherefeld, " and") ;
						continue ;
				}
			}
			
// 280508 E
			// 220909 : Im Zweifelsfall folgt jetzt noch die standard-Routine 
			switch ( tt_datetyp[k] )
			{
			case(iDBCHAR):
			case(iDBVARCHAR):	// 280612
// nothing, nur 1 String, 2 Verschiedene Strings
				if (( tt_cvrange[tt_range[k]][0] == -1 ) && 
					( tt_cbrange[tt_range[k]][0] == -1 ) )
					break ;
				else
				{
					if ( tt_cbrange[tt_range[k]][0] == -1 )
					{
						dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,
							(int)strlen ( clipped(tt_cvrange[tt_range[k]]))+1 );
						sprintf ( whereklausel + j, " %s %s.%s  like ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
// default-Zweig
					dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,
							(int)strlen ( clipped(tt_cvrange[tt_range[k]]))+1 );
					dbClass.sqlin ((char *)  tt_cbrange[tt_range[k]], SQLCHAR ,
							(int)strlen ( clipped(tt_cbrange[tt_range[k]]))+1 );
					sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
					sprintf ( wherefeld, " and") ;
				
					break ;
				}
				break ;				
			case(iDBSMALLINT):	// smallint
				if (( tt_lvrange[tt_range[k]] ==   11 ) && 
					( tt_lbrange[tt_range[k]] ==  -11 ) )	// keine Eingabe
					break ;
				else
				{
					if ( tt_lvrange[tt_range[k]] ==   tt_lbrange[tt_range[k]])
					{
						dbClass.sqlin ((short *) &tt_lvrange[tt_range[k]], SQLSHORT ,0);
						sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
// default-Zweig
					dbClass.sqlin ((short *) &tt_lvrange[tt_range[k]], SQLSHORT, 0);
					dbClass.sqlin ((short *) &tt_lbrange[tt_range[k]], SQLSHORT, 0);
					sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
					sprintf ( wherefeld, " and") ;
					break ;
				}
				break ;

			case(iDBSERIAL):	// 201209
			case(iDBINTEGER):	// integer
				if (( tt_lvrange[tt_range[k]] ==   11 ) && 
					( tt_lbrange[tt_range[k]] ==  -11 ) )	// keine Eingabe
					break ;
				else
				{
					if ( tt_lvrange[tt_range[k]] ==   tt_lbrange[tt_range[k]])
					{
						dbClass.sqlin ((long *) &tt_lvrange[tt_range[k]], SQLLONG ,0);
						sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
// default-Zweig
					dbClass.sqlin ((long *) &tt_lvrange[tt_range[k]], SQLLONG, 0);
					dbClass.sqlin ((long *) &tt_lbrange[tt_range[k]], SQLLONG, 0);
					sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
					sprintf ( wherefeld, " and") ;
					break ;
				}
				break ;

			case(iDBDECIMAL):	// decimal
				if (( tt_dvrange[tt_range[k]] ==   11 ) && 
					( tt_dbrange[tt_range[k]] ==  -11 ) )	// keine Eingabe
					break ;
				else
				{
					if ( tt_dvrange[tt_range[k]] ==  tt_dbrange[tt_range[k]])
					{
						dbClass.sqlin ((double *)  &tt_dvrange[tt_range[k]], SQLDOUBLE ,0);
						sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
// default-Zweig
					dbClass.sqlin ((double *)  &tt_dvrange[tt_range[k]], SQLDOUBLE, 0);
					dbClass.sqlin ((double *)  &tt_dbrange[tt_range[k]], SQLDOUBLE, 0);
					sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
					sprintf ( wherefeld, " and") ;
					break ;
				}
				break ;

			case(iDBDATUM):	// Datumsfeld
// nothing, nur 1 String, 2 Verschiedene Strings
				if (( tt_cvrange[tt_range[k]][0] == -1 ) && 
					( tt_cbrange[tt_range[k]][0] == -1 ) )
					break ;
				else
				{
					if ( tt_cbrange[tt_range[k]][0] == -1 )
					{
						dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,11);
						sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
// default-Zweig
					dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,11);
					dbClass.sqlin ((char *)  tt_cbrange[tt_range[k]], SQLCHAR ,11);
					sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
					sprintf ( wherefeld, " and") ;
				
					break ;
				}
				break ;
			case (iDBDATETIME) :	// 111205

// nothing, nur 1 String, 2 Verschiedene Strings
				if (( tt_cvrange[tt_range[k]][0] == -1 ) && 
					( tt_cbrange[tt_range[k]][0] == -1 ) )
					break ;
				else
				{
					if ( tt_cbrange[tt_range[k]][0] == -1 )
					{

//						dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,11);
						dbClass.sqlin ( ( TIMESTAMP_STRUCT *) &tt_tvrange[tt_range[k]], SQLTIMESTAMP, sizeof(TIMESTAMP_STRUCT));
						sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
// default-Zweig
//					dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,11);
//					dbClass.sqlin ((char *)  tt_cbrange[tt_range[k]], SQLCHAR ,11);
					dbClass.sqlin ( (TIMESTAMP_STRUCT *) & tt_tvrange[ tt_range[k]], SQLTIMESTAMP, sizeof(TIMESTAMP_STRUCT));
					dbClass.sqlin ( (TIMESTAMP_STRUCT *) & tt_tbrange[ tt_range[k]], SQLTIMESTAMP, sizeof(TIMESTAMP_STRUCT));
					sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
					sprintf ( wherefeld, " and") ;
				
					break ;
				}
				break ;
			default :
				break ;

			}
		}
	}


// ##### WHERE - KLAUSEL


		sprintf ( klausel, " " );

		if (! Form_w_class.leseform_w())
		{
//			sprintf ( klausel , "%s", clipped (form_w.krz_txt)) ;
			sprintf ( klausel , "%s", form_w.krz_txt ) ;
			while (!Form_w_class.leseform_w())
			{
//				j = (int)strlen ( clipped( klausel)) ;
// 				sprintf ( klausel + j , "%s" , clipped ( form_w.krz_txt));
				j = (int)strlen (  klausel) ;	// 010507
 				sprintf ( klausel + j , "%s" , form_w.krz_txt);	// 010507
			}
		}

// 20122013 A
		int ikl =  (int) strlen (clipped(klausel));
		int jkl, kkl, lkl ;
		jkl = kkl = lkl = 0;
		while ( jkl < ikl )
		{
			if ( lkl < 2 )
			{
				if ( klausel[jkl] != '$' )
				{
					lkl = 0 ;
					jkl ++ ;
					continue ;
				}
			}
			lkl ++;
			if ( lkl == 1 )
			{	// erstes '$' gefunden
				jkl ++;
				continue;
			}
			if ( lkl == 2 )
			{
				klausel[jkl] = '\0';
				klausel[jkl - 1 ] = '\0';
				jkl ++ ;
				continue;
			}
			groupklausel[kkl]= klausel[jkl] ;
			kkl ++ ; jkl ++;
		}
		groupklausel [kkl] = '\0'; 

// 20122013 E

		if ( wherefeld[1] == 'w' )	// noch nix reingeschrieben ausser " where "
		{
			if ( strlen ( clipped ( klausel)) > 2 )
				sprintf ( whereklausel , " where %s", clipped (klausel) );
			else
				sprintf ( whereklausel , " " );
		}
		else
		{
			if ( strlen ( clipped ( klausel)) > 2 )
			{
				j = (int)strlen ( clipped (whereklausel ));
				sprintf ( whereklausel + j , " and %s", clipped (klausel));
			}

		}


// ##### ORDER - KLAUSEL

		sprintf ( klausel, " " );

		Form_o_class.putmehrfach_o(mehrfach_o) ;

		if (! Form_o_class.leseform_o())
		{
//			sprintf ( klausel , "%s", clipped (form_o.krz_txt)) ;
			sprintf ( klausel , "%s", form_o.krz_txt) ;	// 010507
			while (!Form_o_class.leseform_o())
			{
//				j = (int)strlen ( clipped( klausel)) ;
//				sprintf ( klausel + j , "%s" , clipped ( form_o.krz_txt));
				j = (int)strlen ( klausel) ;	// 010507
				sprintf ( klausel + j , "%s" ,  form_o.krz_txt);	// 010507
			}
		}

		if ( strlen ( clipped ( klausel)) > 1 )
			sprintf ( orderklausel , " order by %s", clipped (klausel) );
		else
			sprintf ( orderklausel , " " );
	

		j = (int) strlen ( statementpuffer ) ;
		sprintf ( statementpuffer + j, " % s from %s"
			, clipped ( virtuellklausel )	// 201213 : virtuell-Klausel dazu
			, tabellenpuffer );

		j = (int) strlen ( statementpuffer ) ;
		sprintf ( statementpuffer + j, " %s %s %s"
				,clipped ( whereklausel )
				,clipped ( groupklausel )
				,clipped ( orderklausel ) );
	}	// if i ( daher nur, wenn was da ist ....
// 010507 : falls Testmode aktiv, dann das statement auf Platte ablegen 
	char * penvchar = getenv ("testmode");
	if ( penvchar == NULL )	penvchar = getenv ( "TESTMODE" ) ;
	if ( penvchar == NULL )
	{}
	else
	{ int testwert = atoi ( penvchar) ;
	  if ( testwert > 0 )
	  {
		  char dateiname[ 256 ] ;
		  penvchar = getenv ( "TMPPATH" ) ;
		  if ( penvchar == NULL )
			  sprintf ( dateiname , "drstatement.sql" ) ;
		  else
			  sprintf ( dateiname ,"%s\\drstatement.sql", penvchar) ;

		FILE * fp_struct ;
		fp_struct = fopen ( dateiname, "ab" ) ;
			if ( fp_struct == NULL )
			{
				// es hat halt NICHT funktioniert
			}

			int nwritten= (int) fwrite(statementpuffer, (int) strlen(statementpuffer), 1, fp_struct );
			fclose ( fp_struct ); 

	  }
	}
// 010507 : falls Testmode aktiv, dann das statement auf Platte ablegen 

	readcursor = (short)dbClass.sqlcursor ( statementpuffer );
	
}

void TEMPTAB_CLASS::drfree (void) 
{

	for (int i = 0 ; i < MAXANZ; i++ )
	{
		if (tt_sqlval[i] != (char *) 0)
		{
			free ( tt_sqlval[i]) ;
					tt_sqlval[i] = (char *) 0;
		} ;
// 310506 
		if (savtt_sqlval[i] != (char *) 0)
		{
			free ( savtt_sqlval[i]) ;
					savtt_sqlval[i] = (char *) 0;
		}

// 280508 
		if (tt_rangin [i] != NULL)
		{
			free ( tt_rangin[i]) ;
			tt_rangin[i] = NULL;
		}

	}
#ifndef FITSMART	// 240507 
// 280207 
	for (int i = 0 ; i < MAXETEXTE; i++ )
	{
		for ( int j = 0 ; j < 15 ; j++ )
		{
			if (et_sqlval[i][j] != (char *) 0)
			{
				free ( et_sqlval[i][j]) ;
					et_sqlval[i][j] = (char *) 0;
			}
		} 
	}
#endif	// 240507

}