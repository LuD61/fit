// range.cpp : Implementierungsdatei
//

#include "stdafx.h"
// #include "dr70001.h"
#include "range.h"
#ifndef OLABEL
#include "DbClass.h"
#endif
#ifdef FITODBC
#include "catsets.h"
#include "llsmpdoc.h"
#include "llsmpvw.h"
#endif
#include "temptab.h"
#include "item.h"
#include "syscolumns.h"

#include "form_ow.h"	// 270307 

extern char * clipped(char*) ;
extern char * sqldatgeram( char *, char *) ;
extern void sqldatdst( TIMESTAMP_STRUCT *, char *) ;
extern short GlobalLila ;
extern int mitrange ;	// aus MainFrm.cpp
extern int externe_sortnr ;	// 280307
extern int danzahl ;	// aus Mainfrm.cpp 011205
extern int holerangesausdatei(char *, char * , char *) ;	// 301105 aus MainFrm.cpp

extern char myform_nr[] ;	// 051009

static int v_abbruch ;

static int globmp ;	// 100506 : Anzahl aktive Ranges

// Crange-Dialogfeld

IMPLEMENT_DYNAMIC(Crange,CDialog) // 140504 : CDialog<-CPropertyPage)
Crange::Crange()
// 140504	: CPropertyPage(Crange::IDD)
	: CDialog(Crange::IDD)
	, m_vcedit1(_T(""))
	, m_vcedit2(_T(""))
	, m_vcedit3(_T(""))
	, m_vcedit4(_T(""))
	, m_vcedit5(_T(""))
	, m_vcedit6(_T(""))
	, m_vcedit7(_T(""))
	, m_vcedit8(_T(""))
	, m_vcedit9(_T(""))
	, m_vcedit10(_T(""))
	, m_vcedit11(_T(""))
	, m_vcedit12(_T(""))
	, m_vcedit13(_T(""))
	, m_vcedit14(_T(""))
	, m_vcedit15(_T(""))
	, m_vcedit16(_T(""))
	, m_vcedit17(_T(""))
	, m_vcedit18(_T(""))
	, m_vcedit19(_T(""))
	, m_vcedit20(_T(""))
	, m_vcedit21(_T(""))
	, m_vcedit22(_T(""))
	, m_vcedit23(_T(""))
	, m_vcedit24(_T(""))
	, m_vcedit25(_T(""))
	, m_vcedit26(danzahl)	// 011205 1-> danzahl
	, m_combo1(0)
{
}

ITEM_CLASS Item ;


BOOL Crange::OnInitDialog()
 
{

char token[125],wert1[125],wert2[125] ;	// 301105
int l,m,n;
CDialog::OnInitDialog();
l=m=n=0;


	SetWindowText ( myform_nr) ;	// 051007 


	// A 011205 tt_range[i] : sortierung pruefen, gegebnenfalls korrigieren

int m_rangep[MAXRANGEI] ;	// welches "i" ist rangebehaftet
int m_rangeq[MAXRANGEI] ;	// Quell-Eintrag aus der daba

int mp,mq,mz ;
mp = mq = mz = 0 ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i ++ )
	{
		if ( ! tt_range[i] ) continue ;
		if ( mp < MAXRANGEI )	// mehr als 8 Ranges werden ignoriert
		{

			sprintf ( item.name, "%s",tt_feldnam[i]);
			Item.openitem();
			if ( Item.leseitem())
			{
			   tt_range[i] = 0 ; // Item fehlt und wird ausgetragen
			   tt_rangin[i] = NULL ;	// 280508
			   tt_rangini[i] = 0 ;			// 220909
			}
			else
			{
				m_rangep[mp] = i ;
				m_rangeq[mp] = tt_range[i] ;
				mp ++ ;
			}
		}
	}	// mp beinhaltet jetzt Anzahl Range-Felder
	
	for ( int i = 0 ; i < (mp - 1); )
	{
		if ( m_rangeq[i + 1] < m_rangeq[i])
		{
			mq = m_rangep[i] ;
			mz = m_rangeq[i] ;
			m_rangep[i] = m_rangep[i + 1] ;
			m_rangeq[i] = m_rangeq[i + 1] ;
			m_rangep[i + 1] = mq ;
			m_rangeq[i + 1] = mz ;


			i = 0 ;
			continue ;
		}
		i ++ ;
	}	// jetzt nach Range-Nummerierung sortiert

	for ( int i = 0 ; i < mp ;i++ )
	{
		tt_range[m_rangep[i]] = i +  1 ;
	}	// in tt_range steht jetzt die sortierte feldnummer
	// E 011205 tt_range[i] : sortierung pruefen, gegebnenfalls korrigieren 

	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i ++ )
	{
		if ( ! tt_range[i] ) continue ;

		sprintf ( item.name, "%s",tt_feldnam[i]);
		Item.openitem();

		if ( Item.leseitem()) continue ; // Item fehlt .......
	// hier fehlt jetzt nie mehr was ... 011205

		l = tt_range[i] - 1 ;	// 011205 : ist ja jetzt bereits zugeordnet

		switch (l)
		{
		case 0 :
				m_vcedit17 = _T (item.bezlang);
				if ( mitrange )
				{
					sprintf ( token, "%s",tt_feldnam[i]);
					if (  holerangesausdatei( token, wert1,  wert2))
					{
						m_vcedit1 = _T (wert1);
						m_vcedit9 = _T (wert2);
					}
				} ;
			break ;
		case 1 :
				if ( mp < 5 )	// 100506 
				{
					m_vcedit19 = _T (item.bezlang);
					if ( mitrange )
					{
						sprintf ( token, "%s",tt_feldnam[i]);
						if (  holerangesausdatei( token, wert1,  wert2))
						{
							m_vcedit3 = _T (wert1);
							m_vcedit11 = _T (wert2);
						}
					}
				}
				else
				{
					m_vcedit18 = _T (item.bezlang);
					if ( mitrange )
					{
						sprintf ( token, "%s",tt_feldnam[i]);
						if (  holerangesausdatei( token, wert1,  wert2))
						{
							m_vcedit2 = _T (wert1);
							m_vcedit10 = _T (wert2);
						}
					}
				} ;
			break ;
		case 2 :
				if ( mp < 5 )	// 100506 
				{
					m_vcedit21 = _T (item.bezlang);
					if ( mitrange )
					{
						sprintf ( token, "%s",tt_feldnam[i]);
						if (  holerangesausdatei( token, wert1,  wert2))
						{
							m_vcedit5 = _T (wert1);
							m_vcedit13 = _T (wert2);
						}
					}
				}
				else
				{
					m_vcedit19 = _T (item.bezlang);
					if ( mitrange )
					{
						sprintf ( token, "%s",tt_feldnam[i]);
						if (  holerangesausdatei( token, wert1,  wert2))
						{
							m_vcedit3 = _T (wert1);
							m_vcedit11 = _T (wert2);
						}
					}
				} ;
			break ;
		case 3 :
				if ( mp < 5 )	// 100506 
				{
					m_vcedit23 = _T (item.bezlang);
					if ( mitrange )
					{
						sprintf ( token, "%s",tt_feldnam[i]);
						if (  holerangesausdatei( token, wert1,  wert2))
						{
							m_vcedit7 = _T (wert1);
							m_vcedit15 = _T (wert2);
						}
					}
				}
				else
				{
					m_vcedit20 = _T (item.bezlang);
					if ( mitrange )
					{
						sprintf ( token, "%s",tt_feldnam[i]);
						if (  holerangesausdatei( token, wert1,  wert2))
						{
							m_vcedit4 = _T (wert1);
							m_vcedit12 = _T (wert2);
						}
					}
				} ;
			break ;
		case 4 :
				m_vcedit21 = _T (item.bezlang);
				if ( mitrange )
				{
					sprintf ( token, "%s",tt_feldnam[i]);
					if (  holerangesausdatei( token, wert1,  wert2))
					{
						m_vcedit5 = _T (wert1);
						m_vcedit13 = _T (wert2);
					}
				} ;
			break ;
		case 5 :
				m_vcedit22 = _T (item.bezlang);
				if ( mitrange )
				{
					sprintf ( token, "%s",tt_feldnam[i]);
					if (  holerangesausdatei( token, wert1,  wert2))
					{
						m_vcedit6 = _T (wert1);
						m_vcedit14 = _T (wert2);
					}
				} ;
			break ;
		case 6 :
				m_vcedit23 = _T (item.bezlang);
				if ( mitrange )
				{
					sprintf ( token, "%s",tt_feldnam[i]);
					if (  holerangesausdatei( token, wert1,  wert2))
					{
						m_vcedit7 = _T (wert1);
						m_vcedit15 = _T (wert2);
					}
				} ;
			break ;
		case 7 :
				m_vcedit24 = _T (item.bezlang);
				if ( mitrange )
				{
					sprintf ( token, "%s",tt_feldnam[i]);
					if (  holerangesausdatei( token, wert1,  wert2))
					{
						m_vcedit8 = _T (wert1);
						m_vcedit16 = _T (wert2);
					}
				} ;
			break ;
		}
		l ++ ;
		tt_range[i] = l ;	// 011205 : redundant

	}
    l = mp ;		// Rest platt machen
	globmp = mp	;	// 100506


	if ( l == 0 )
	{
				m_cedit1.ShowWindow(SW_HIDE); m_cedit9.ShowWindow(SW_HIDE);m_cedit17.ShowWindow(SW_HIDE);
				m_cedit2.ShowWindow(SW_HIDE); m_cedit10.ShowWindow(SW_HIDE);m_cedit18.ShowWindow(SW_HIDE);
				m_cedit3.ShowWindow(SW_HIDE); m_cedit11.ShowWindow(SW_HIDE);m_cedit19.ShowWindow(SW_HIDE);
				m_cedit4.ShowWindow(SW_HIDE); m_cedit12.ShowWindow(SW_HIDE);m_cedit20.ShowWindow(SW_HIDE);
				m_cedit5.ShowWindow(SW_HIDE); m_cedit13.ShowWindow(SW_HIDE);m_cedit21.ShowWindow(SW_HIDE);
				m_cedit6.ShowWindow(SW_HIDE); m_cedit14.ShowWindow(SW_HIDE);m_cedit22.ShowWindow(SW_HIDE);
				m_cedit7.ShowWindow(SW_HIDE); m_cedit15.ShowWindow(SW_HIDE);m_cedit23.ShowWindow(SW_HIDE);
				m_cedit8.ShowWindow(SW_HIDE); m_cedit16.ShowWindow(SW_HIDE);m_cedit24.ShowWindow(SW_HIDE);
	}
	if ( l == 1 )
	{
				m_cedit2.ShowWindow(SW_HIDE); m_cedit10.ShowWindow(SW_HIDE);m_cedit18.ShowWindow(SW_HIDE);
				m_cedit3.ShowWindow(SW_HIDE); m_cedit11.ShowWindow(SW_HIDE);m_cedit19.ShowWindow(SW_HIDE);
				m_cedit4.ShowWindow(SW_HIDE); m_cedit12.ShowWindow(SW_HIDE);m_cedit20.ShowWindow(SW_HIDE);
				m_cedit5.ShowWindow(SW_HIDE); m_cedit13.ShowWindow(SW_HIDE);m_cedit21.ShowWindow(SW_HIDE);
				m_cedit6.ShowWindow(SW_HIDE); m_cedit14.ShowWindow(SW_HIDE);m_cedit22.ShowWindow(SW_HIDE);
				m_cedit7.ShowWindow(SW_HIDE); m_cedit15.ShowWindow(SW_HIDE);m_cedit23.ShowWindow(SW_HIDE);
				m_cedit8.ShowWindow(SW_HIDE); m_cedit16.ShowWindow(SW_HIDE);m_cedit24.ShowWindow(SW_HIDE);
	}
	if ( l == 2 )
	{
				m_cedit2.ShowWindow(SW_HIDE); m_cedit10.ShowWindow(SW_HIDE);m_cedit18.ShowWindow(SW_HIDE);
				m_cedit4.ShowWindow(SW_HIDE); m_cedit12.ShowWindow(SW_HIDE);m_cedit20.ShowWindow(SW_HIDE);
				m_cedit5.ShowWindow(SW_HIDE); m_cedit13.ShowWindow(SW_HIDE);m_cedit21.ShowWindow(SW_HIDE);
				m_cedit6.ShowWindow(SW_HIDE); m_cedit14.ShowWindow(SW_HIDE);m_cedit22.ShowWindow(SW_HIDE);
				m_cedit7.ShowWindow(SW_HIDE); m_cedit15.ShowWindow(SW_HIDE);m_cedit23.ShowWindow(SW_HIDE);
				m_cedit8.ShowWindow(SW_HIDE); m_cedit16.ShowWindow(SW_HIDE);m_cedit24.ShowWindow(SW_HIDE);
	}
	if ( l == 3 )
	{
				m_cedit2.ShowWindow(SW_HIDE); m_cedit10.ShowWindow(SW_HIDE);m_cedit18.ShowWindow(SW_HIDE);
				m_cedit4.ShowWindow(SW_HIDE); m_cedit12.ShowWindow(SW_HIDE);m_cedit20.ShowWindow(SW_HIDE);
				m_cedit6.ShowWindow(SW_HIDE); m_cedit14.ShowWindow(SW_HIDE);m_cedit22.ShowWindow(SW_HIDE);
				m_cedit7.ShowWindow(SW_HIDE); m_cedit15.ShowWindow(SW_HIDE);m_cedit23.ShowWindow(SW_HIDE);
				m_cedit8.ShowWindow(SW_HIDE); m_cedit16.ShowWindow(SW_HIDE);m_cedit24.ShowWindow(SW_HIDE);
	}

	if ( l == 4 )
	{
				m_cedit2.ShowWindow(SW_HIDE); m_cedit10.ShowWindow(SW_HIDE);m_cedit18.ShowWindow(SW_HIDE);
				m_cedit4.ShowWindow(SW_HIDE); m_cedit12.ShowWindow(SW_HIDE);m_cedit20.ShowWindow(SW_HIDE);
				m_cedit6.ShowWindow(SW_HIDE); m_cedit14.ShowWindow(SW_HIDE);m_cedit22.ShowWindow(SW_HIDE);
				m_cedit8.ShowWindow(SW_HIDE); m_cedit16.ShowWindow(SW_HIDE);m_cedit24.ShowWindow(SW_HIDE);
	}
	if ( l == 5 )
	{
				m_cedit6.ShowWindow(SW_HIDE); m_cedit14.ShowWindow(SW_HIDE);m_cedit22.ShowWindow(SW_HIDE);
				m_cedit7.ShowWindow(SW_HIDE); m_cedit15.ShowWindow(SW_HIDE);m_cedit23.ShowWindow(SW_HIDE);
				m_cedit8.ShowWindow(SW_HIDE); m_cedit16.ShowWindow(SW_HIDE);m_cedit24.ShowWindow(SW_HIDE);
	}
	if ( l == 6 )
	{
				m_cedit7.ShowWindow(SW_HIDE); m_cedit15.ShowWindow(SW_HIDE);m_cedit23.ShowWindow(SW_HIDE);
				m_cedit8.ShowWindow(SW_HIDE); m_cedit16.ShowWindow(SW_HIDE);m_cedit24.ShowWindow(SW_HIDE);
	}
	if ( l == 7 )
	{
				m_cedit8.ShowWindow(SW_HIDE); m_cedit16.ShowWindow(SW_HIDE);m_cedit24.ShowWindow(SW_HIDE);
	}


	if ( ! GlobalLila)
	{
		m_cedit25.ShowWindow(SW_HIDE); m_cedit26.ShowWindow(SW_HIDE);
			}
	else
	{
				m_vcedit25 = _T ("Anzahl");
	}


	if ( mehrfach_o > 0 )
	{

//  270307

//		int    nIndex;
		int aktuell = 0 ;
		int ziel = 0 ;
		char einzelpuffer[125] ;
		CString szdta ;

		einzelpuffer[0] = '\0' ;
		for ( int di = 0; di < 1030 && aktuell < mehrfach_o && ziel < 124 ; )
		{
			einzelpuffer[ziel] = ordersel[di] ;
			if ( ordersel[di ] == '\0' )
			{

    // Listbox mit Eintraegen fuellen
				szdta.Format("%s", einzelpuffer);
				m_Combo1.InsertString(aktuell, szdta.GetBuffer(0));
				m_Combo1.SetCurSel(0);
//				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->AddString(szdta.GetBuffer(0));
//				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(0);
				aktuell ++ ; ziel = -1 ; einzelpuffer[0] = '\0' ;
			}
			ziel ++ ; di ++ ;
		}

		// 280307 : Voreinstellung voreinstellen :-)

		if ( externe_sortnr > 0 )
		{
			if ( externe_sortnr <= mehrfach_o )
				m_Combo1.SetCurSel( externe_sortnr - 1 ) ;
		};

	}
	else
		m_Combo1.ShowWindow(SW_HIDE);

	UpdateData(FALSE) ;

	return TRUE ;
}

Crange::~Crange()
{
}

void Crange::DoDataExchange(CDataExchange* pDX)
{
// 140504 	CPropertyPage::DoDataExchange(pDX);
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_vcedit1);
	DDX_Text(pDX, IDC_EDIT2, m_vcedit2);
	DDX_Text(pDX, IDC_EDIT3, m_vcedit3);
	DDX_Text(pDX, IDC_EDIT4, m_vcedit4);
	DDX_Text(pDX, IDC_EDIT5, m_vcedit5);
	DDX_Text(pDX, IDC_EDIT6, m_vcedit6);
	DDX_Text(pDX, IDC_EDIT7, m_vcedit7);
	DDX_Text(pDX, IDC_EDIT8, m_vcedit8);
	DDX_Text(pDX, IDC_EDIT9, m_vcedit9);
	DDX_Text(pDX, IDC_EDIT10, m_vcedit10);
	DDX_Text(pDX, IDC_EDIT11, m_vcedit11);
	DDX_Text(pDX, IDC_EDIT12, m_vcedit12);
	DDX_Text(pDX, IDC_EDIT13, m_vcedit13);
	DDX_Text(pDX, IDC_EDIT14, m_vcedit14);
	DDX_Text(pDX, IDC_EDIT15, m_vcedit15);
	DDX_Text(pDX, IDC_EDIT16, m_vcedit16);
	DDX_Text(pDX, IDC_EDIT17, m_vcedit17);
	DDX_Text(pDX, IDC_EDIT18, m_vcedit18);
	DDX_Text(pDX, IDC_EDIT19, m_vcedit19);
	DDX_Text(pDX, IDC_EDIT20, m_vcedit20);
	DDX_Text(pDX, IDC_EDIT21, m_vcedit21);
	DDX_Text(pDX, IDC_EDIT22, m_vcedit22);
	DDX_Text(pDX, IDC_EDIT23, m_vcedit23);
	DDX_Text(pDX, IDC_EDIT24, m_vcedit24);

	DDX_Control(pDX, IDC_EDIT1, m_cedit1);
	DDX_Control(pDX, IDC_EDIT2, m_cedit2);
	DDX_Control(pDX, IDC_EDIT3, m_cedit3);
	DDX_Control(pDX, IDC_EDIT4, m_cedit4);
	DDX_Control(pDX, IDC_EDIT5, m_cedit5);
	DDX_Control(pDX, IDC_EDIT6, m_cedit6);
	DDX_Control(pDX, IDC_EDIT7, m_cedit7);
	DDX_Control(pDX, IDC_EDIT8, m_cedit8);
	DDX_Control(pDX, IDC_EDIT9, m_cedit9);
	DDX_Control(pDX, IDC_EDIT10, m_cedit10);
	DDX_Control(pDX, IDC_EDIT11, m_cedit11);
	DDX_Control(pDX, IDC_EDIT12, m_cedit12);
	DDX_Control(pDX, IDC_EDIT13, m_cedit13);
	DDX_Control(pDX, IDC_EDIT14, m_cedit14);
	DDX_Control(pDX, IDC_EDIT15, m_cedit15);
	DDX_Control(pDX, IDC_EDIT16, m_cedit16);
	DDX_Control(pDX, IDC_EDIT17, m_cedit17);
	DDX_Control(pDX, IDC_EDIT18, m_cedit18);
	DDX_Control(pDX, IDC_EDIT19, m_cedit19);
	DDX_Control(pDX, IDC_EDIT20, m_cedit20);
	DDX_Control(pDX, IDC_EDIT21, m_cedit21);
	DDX_Control(pDX, IDC_EDIT22, m_cedit22);
	DDX_Control(pDX, IDC_EDIT23, m_cedit23);
	DDX_Control(pDX, IDC_EDIT24, m_cedit24);

	DDX_Control(pDX, IDC_EDIT25, m_cedit25);
	DDX_Text(pDX, IDC_EDIT25, m_vcedit25);

	DDX_Control(pDX, IDC_EDIT26, m_cedit26);
	DDX_Text(pDX, IDC_EDIT26, m_vcedit26);
	DDV_MinMaxInt(pDX, m_vcedit26, 1, 9999);

	DDX_Control(pDX, IDC_COMBO1, m_Combo1);	// 270307
//	DDX_CBString(pDX, IDC_COMBO1, v_Combo1);

}


// 140504 BEGIN_MESSAGE_MAP(Crange, CPropertyPage)
BEGIN_MESSAGE_MAP(Crange, CDialog)


	ON_EN_KILLFOCUS(IDC_EDIT1, OnEnKillFocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT2, OnEnKillFocusEdit2)
//	Mustereintrag ON_EN_CHANGE(IDC_EDIT3, OnEnChangeEdit3)
	ON_EN_KILLFOCUS(IDC_EDIT3, OnEnKillFocusEdit3)
	ON_EN_KILLFOCUS(IDC_EDIT4, OnEnKillFocusEdit4)
	ON_EN_KILLFOCUS(IDC_EDIT5, OnEnKillFocusEdit5)
	ON_EN_KILLFOCUS(IDC_EDIT6, OnEnKillFocusEdit6)
	ON_EN_KILLFOCUS(IDC_EDIT7, OnEnKillFocusEdit7)
	ON_EN_KILLFOCUS(IDC_EDIT8, OnEnKillFocusEdit8)
	ON_EN_KILLFOCUS(IDC_EDIT9, OnEnKillFocusEdit9)
	ON_EN_KILLFOCUS(IDC_EDIT10, OnEnKillFocusEdit10)
	ON_EN_KILLFOCUS(IDC_EDIT11, OnEnKillFocusEdit11)
	ON_EN_KILLFOCUS(IDC_EDIT12, OnEnKillFocusEdit12)
	ON_EN_KILLFOCUS(IDC_EDIT13, OnEnKillFocusEdit13)
	ON_EN_KILLFOCUS(IDC_EDIT14, OnEnKillFocusEdit14)
	ON_EN_KILLFOCUS(IDC_EDIT15, OnEnKillFocusEdit15)
	ON_EN_KILLFOCUS(IDC_EDIT16, OnEnKillFocusEdit16)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnCbnSelchangeCombo1)	// 270307
	ON_CBN_KILLFOCUS(IDC_COMBO1, OnCbnKillfocusCombo1)	// 270307

END_MESSAGE_MAP()

/* ---->

char * rtxt(CEdit feld)
{	char *S ;
	 int len = feld.GetLine(0,S) ;
	 if ( len )	return  S ;
	 return ( char *) 0 ;
}
< ---- */

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	int k = 0 ;	// 261007 : Zusatzchecks
	BOOL j = FALSE ;

	int testi = 1 ;	// 250810
	if ( testi )
	{

				char hilfi1[25] ;
				char hilfi2[40] ;
				sprintf ( hilfi1,"%d" ,i ) ;
				sprintf ( hilfi2,"%s",bufi ) ;
//				MessageBox(NULL,hilfi2,hilfi1,MB_OKCANCEL);
	}

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		// 261007 : Zusatzchecks wegen vista, nur noch bedingtes return
		if ( i < 6 ) k = -1 ;
		if ( k == 0 )
		{
			// die ersten beiden Zeichen MUESSEN ZAHLEN sein
			if ( bufi[0] > '9' || bufi[0] < '0' ||bufi[1] > '9' || bufi[1] < '0' )
				k = - 1 ;
		}
		if ( k == 0 )
		{
			if ( bufi[2] > '9' || bufi[2] < '0' )
				k = 2 ;		// Typ 8 oder typ 10 moeglich

		}
		if ( k == 0 )
		{
			// nur noch Typ 6 erlaubt 
			if (    bufi[3] > '9' || bufi[3] < '0'
				 || bufi[4] > '9' || bufi[4] < '0'
				 || bufi[5] > '9' || bufi[5] < '0' )
			{
				k = - 1 ;
			}
			else
			{	// Gueltiger Typ 6  ......
				k = 1 ;
				i = 6 ;
				bufi[6] = '\0' ;
			}
		}
		if ( k == 2 )
		{
			if ( i < 8 )
			{	
				k = -1 ;
			}
			else
			{	// 4.,5.,7.,8. Zeichen MUESSEN ZAHLEN sein, 6.Zeichen MUSS Nicht-Zahl sein
				if (   bufi[3] > '9' || bufi[3] < '0' 
					 ||bufi[4] > '9' || bufi[4] < '0'
				     ||bufi[6] > '9' || bufi[6] < '0'
				     ||bufi[7] > '9' || bufi[7] < '0'
				     || !( bufi[5] >'9' || bufi[5]< '0')
				    )
				k = - 1 ;
			}
		}
		if ( k == 2 )
		{
			if ( bufi[8] > '9' || bufi[8] < '0' )
			{	// gueltiger Typ 8 
				k = 1 ;
				i = 8 ;
				bufi[8] = '\0' ;
			}
		
		}

		if ( k == 2 )
		{
			if ( i < 10 )
			{	
				k = -1 ;
			}
			else
			{	// 9.,10. Zeichen MUESSEN ZAHLEN sein
				if (   bufi[8] > '9' || bufi[8] < '0' 
					 ||bufi[9] > '9' || bufi[9] < '0'
			       )
				{
					k = -1 ;
				}
				else
				{
					k = 1 ;
					i = 10 ;
					bufi[10] = '\0' ;
				}
			}
		}
		if ( k < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil2 < 1 )	// 260110 hil1->hil2
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )	// 150308 hil2->hil1 mea culpa
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}

int Crange::getmehrfach_o(void)
{
	return mehrfach_o ;
}

void Crange::putmehrfach_o(int emehrfach)
{
	mehrfach_o = emehrfach ;
}


// Crange-Meldungshandler

// 080408 A
BOOL Crange::NoListCtrl (CWnd *cWnd)
{
    if (((CComboBox *) cWnd->GetParent ())->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CComboBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CListBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    return TRUE;
}
BOOL Crange::PreTranslateMessage(LPMSG lpMsg)
{
	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
			 {
		 
		        case VK_RETURN :
					cWnd = GetFocus ();
			           if(cWnd == GetDlgItem (IDOK))
						   return CDialog::PreTranslateMessage(lpMsg);

			           if(cWnd == GetDlgItem (IDCANCEL))
					   {
			 				v_abbruch = 1 ;
							CDialog::OnCancel();
					   }

/* --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- */
			   NextDlgCtrl();

						return TRUE;

			 case VK_F5 :
			 	v_abbruch = 1 ;
				CDialog::OnCancel();
//                     OnCancel ();
 			         return TRUE;

/* ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- */

/* -------->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ------ */

 		    case VK_DOWN :
                     if (NoListCtrl (GetFocus ()))
                     {
						NextDlgCtrl();
						return TRUE;
                     }
				     break;
 		    case VK_UP :
                    if (NoListCtrl (GetFocus ()))
                    {
						PrevDlgCtrl();
	 					cWnd = GetFocus ();
					 	GetNextDlgTabItem(cWnd ,FALSE) ;
						return TRUE;
                     }
				     break;
		 }
	 }

return CDialog::PreTranslateMessage(lpMsg);

}

// 080408 E




static char bufh[512] ;

void Crange::OnEnKillFocusEdit1()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
		if ( tt_range[i] == 1 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :	// 111205 : vorerst nur datum
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit1.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit1.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

/* ---> Mustereintrag

void Crange::OnEnChangeEdit2()
{
}
< ---- */

void Crange::OnEnKillFocusEdit2()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
		if ( tt_range[i] == 2 )	//  Nur bei globmp >  4 ueberhaupt aktiv .....
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit2.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit2.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit3()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
//		if ( tt_range[i] == 3 )	100506
		if ( ( tt_range[i] == 3 && globmp > 4 ) || ( tt_range[i] == 2 && globmp < 5) )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE);
				i = m_cedit3.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit3.Format("%s",_T(bufh));
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}

	}
	return ;

}

void Crange::OnEnKillFocusEdit4()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
		if ( tt_range[i] == 4 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit4.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit4.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}


void Crange::OnEnKillFocusEdit5()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
//		if ( tt_range[i] == 5 )	100506 
		if ( ( tt_range[i] == 5 && globmp > 4 ) || ( tt_range[i] == 3 && globmp < 5) )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit5.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit5.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit6()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
		if ( tt_range[i] == 6 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit6.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit6.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit7()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
// 100506 		if ( tt_range[i] == 7 )
		if ( ( tt_range[i] == 7 && globmp > 4 ) || ( tt_range[i] == 4 && globmp < 5) )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit7.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit7.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit8()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
		if ( tt_range[i] == 8 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit8.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit8.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit9()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
		if ( tt_range[i] == 1 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit9.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit9.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit10()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
		if ( tt_range[i] == 2)	// bur bei globmp > 4 aktiv ....
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit10.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit10.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit11()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
// 100506		if ( tt_range[i] == 3 )
		if ( ( tt_range[i] == 3 && globmp > 4 ) || ( tt_range[i] == 2 && globmp < 5) )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit11.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit11.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit12()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
		if ( tt_range[i] == 4 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit12.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit12.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit13()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
// 100506		if ( tt_range[i] == 5 )
		if ( ( tt_range[i] == 5 && globmp > 4 ) || ( tt_range[i] == 3 && globmp < 5) )

		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit13.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit13.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit14()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
		if ( tt_range[i] == 6 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit14.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit14.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit15()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
// 100506		if ( tt_range[i] == 7 )
		if ( ( tt_range[i] == 7 && globmp > 4 ) || ( tt_range[i] == 4 && globmp < 5) )
		{

			switch (tt_datetyp[i])
			{	
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit15.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit15.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit16()
{
	// 101110 MAXRANGE -> MAXANZ 
	for (int i = 0 ; i < MAXANZ ; i++ )
	{
		if ( tt_range[i] == 8 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATETIME :		// 111205
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit16.GetLine(0,bufh,500) ;
				bufh[i] ='\0' ;		// 250810
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit16.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

int isnumeric ( int datetyp, char * rangex )
{
// Bei Datentyp iDBINTEGER und iDBSMALLINT wird nach Komma gekappt !!
	int retco = 1 ;
	int inwert = 0;	// 261007
	int vorzeichen = 0 ;
	int j = (int) strlen ( rangex ) ;
	for ( int i = 0 ;i<j; i++ )
// Leerstring ist zunaechst mal gueltig, wird aber anders als String "0" gehandelt
	{
		if ( rangex[i] > '9' || rangex[i] < '0' )
		{
			if ( rangex[i] == ',' )	rangex [i] = '.' ;
			else
			{
				// 100506 : Vorzeichen handeln 
				if (( rangex[i] == '+' ) ||( rangex[i] == '-' ))
				{
					if ( vorzeichen ) 
					{
						retco = 0 ;
						break ;
					}
					vorzeichen = 1 ;
				}
				else
				{
					if ( inwert == 1 )
					{	// 271007 : irgendwie am Ende
						retco = 1 ;
						break ;
					}
					if ( rangex[i] != ' ' )
					{
						retco = 0 ;
						break ;
					}
				}
			}
		}
		else
		{	// 261007
			inwert = 1 ;	// irgednwie erste Zahl gefunden 
		}
	}
	long wertx ;
	if ( retco )
	{
		if (datetyp == iDBSMALLINT) 
		{
			wertx = atol(rangex) ;
			if (wertx > 32000L  ||wertx < -32000L  )
				retco = 0 ;	// 261007 : Datentyp "L" dazugeschrieben
		}
		if (datetyp == iDBINTEGER || datetyp == iDBSERIAL )	// 201209 
		{
			wertx = atol(rangex) ;
			if (wertx > 99999999L || wertx < -99999999L  )
				retco = 0 ;
		}
	}
	return retco ;
}
// 230909 A												161110
int extendrange( int i, char * range1, char * range2 , int sonderrange)
{
// 230909 extended Ranges handeln
// Aufruf nur durch ganzahlige positive felder mit definierter Syntax 
// Einschraenkungen :
// nur positive ganze Zahlen ( d.h. smallint,integer,long )
// erlaubte Syntax : 
// Trenner "," ";"
// Operatoren : "-","<" , ">" , "<>"
// vor- und folgeblanks ignorieren
// Syntax-Fehler f�hrt zu Ignorieren des Segments bis zum n�chsten Trenner

// 161110 : erweiterte Syntax : falls der Datentyp ( numerisch ) stimmt und als erstes Zeichen "#" steht,
//	dann wird ebenfalls extended-range-Ablauf aktiviert ( z.B. f�r fit-Artikelnummern )
// beachte : immer ganzzahlig unabhaenig vom Datentyp


long klals = 0 ;	// es kann nur einen geben
int klalsp = 0 ;

long grals = 0 ;	// es kann nur einen geben
int gralsp = 0 ;

long win[256] ;
int winp = 0 ;

long nin[256] ;
int ninp = 0 ;

long von[256] ;	// korrespondiert immer mit bis
long bis[256] ;	// korresponidett immer mit von
long vobp = 0 ;	 


char zahl1[99] ;
int pin1 = 0 ;
char zahl2[99] ;
int pin2 = 0 ;
int m1inza = 0 ;
int m2inza = 0 ;
int minop = 0 ;
int opgrals = 0 ;	// groesser als
int opklals = 0 ;	// kleiner als
int opbetwe = 0 ;	// between
int opnin   = 0 ;	// not in
int merror = 0 ;	// ungueltiges segment


int kurz ;	// 161110 : nach hier oben 
char * p ;	// 161110 : nach hier oben

	if (! sonderrange )	// 161110 : bisheriger "intelligenter" Ablauf
	{
		int kurz1 = (int) strlen ( clipped (range1)) ;
		int kurz2 = (int) strlen ( clipped (range2)) ;
		if ( kurz1 ==  kurz2  )
		{
			if ( ! kurz1 )
			return 0 ;	// Nix ist auch normal
	
			if ( strncmp (range1 , range2, kurz1 ))
				return 0 ;	// Normale Eingaben
		}
		else	// Verschieden lange Eingaben
		{
			if ( kurz1 > 0 && kurz2 > 0 )
				return 0 ;	// Unterschiedliches Zeugs in beiden feldern
		
		}
		// Hier ist nur noch feld1 ODER feld2 oder 2 gleiche Felder �brig 
		if ( kurz1 > 0 )
		{
			kurz = kurz1 ;
			p = range1 ;
		}
		else
		{
			kurz = kurz2 ;
			p = range2 ;
		} ;
	}
	else	// 161110 : sonderrange handeln
	{
		range1 ++ ;	// "#" ueberlesen 
		kurz = (int) strlen ( clipped (range1)) ;
		if ( ! kurz )
			return 0 ;	// Nix ist auch normal
		p = range1 ;
	}

	m1inza = 0 ;
	m2inza = 0 ;
	minop = 0 ;
	opgrals = 0 ;	// groesser als
	opklals = 0 ;	// kleiner als
	opbetwe = 0 ;	// between
	opnin = 0 ;	// not in
	merror = 0 ;	// ungueltiges segment
	pin1 = 0 ;
	pin2 = 0 ;
	kurz ++ ;	// Stringende jetzt ist mit im search 

	for ( int ii = 0 ;  ii < kurz ; ii ++ )
	{ 
		if ( p[ii] == '\0' || p[ii] == ',' || p[ii] == ';' )
		{
			if ( !merror && ! minop )
			{
				int gefunden = 0 ;

// ######   Kleiner als 
				if ( opklals && pin2 > 0 && ! pin1 )
				{
					zahl2[pin2] = '\0' ;
					long hilfe = atol (zahl2) ;
					if ( ! klalsp || hilfe < klals )
						klals = hilfe ;
					klalsp = 1 ;
					gefunden = 1 ;

				}
// ######   Groesser als 
				if ( opgrals && pin2 > 0 && ! pin1 )
				{
					zahl2[pin2] = '\0' ;
					long hilfe = atol (zahl2) ;
					if ( ! gralsp || hilfe > grals )
						grals = hilfe ;
					gralsp = 1 ;
					gefunden = 1 ;
				}
// ######   between 
				if ( opbetwe && pin2 > 0 && pin1 > 0 )
				{
					zahl1[pin1] = '\0' ;
					zahl2[pin2] = '\0' ;
					von[vobp] = atol (zahl1) ;
					bis[vobp] = atol (zahl2) ;
					if ( bis[vobp] < von[vobp] )
					{
						// leerer Bereich
					}
					else
					{
						vobp ++ ;
						gefunden = 1 ;
					}
				}
// ######   not in ( bzw. ungleich ) 
				if ( opnin && pin2 > 0 && ! pin1 )
				{
					zahl2[pin2] = '\0' ;
					nin[ninp] = atol (zahl2) ;
					ninp ++ ;
					gefunden = 1 ;
				}
// #####   Listenelement
				if ( pin1 > 0 && ! opgrals && ! opklals  && ! opbetwe && !opnin && ! pin2 )
				{	// die letzte Option : in Liste
					zahl1[pin1] = '\0' ;
					win[winp] = atol (zahl1) ;
					winp ++ ;
					gefunden = 1 ;
				}
			}
			m1inza = 0 ;
			m2inza = 0 ;
			minop = 0 ;
			opgrals = 0 ;
			opklals = 0 ;
			opbetwe = 0 ;
			opnin = 0 ;
			merror = 0 ;
			pin1 = 0 ;
			pin2 = 0 ;
			if ( p[ii] == '\0' )
				break ;
			continue ;
		}	// Segmentende
		if ( merror )	// problemsegmente ueberlesen 
			continue ;

		if ( p[ii] > '9' || p[ii] < '0' )
		{
			if ( p[ii] == '-' )
			{
				m1inza = 0 ;
				m2inza = 0 ;
				if ( pin1 > 0 && ! minop && ! opgrals && ! opklals && ! opbetwe && ! opnin && ! pin2 )
				{
					opbetwe = 1 ;
				}
				else
					merror = 1 ;
				continue ;
			}

			if ( p[ii] == '<' )
			{
				m1inza = 0 ;
				m2inza = 0 ;
				if ( ! pin1 && ! minop && ! opgrals && ! opklals && ! opbetwe && ! opnin && ! pin2 )
				{
					minop = 1 ;
					opklals = 1 ;
				}
				else
					merror = 1 ;
				continue ;
			}

			if ( p[ii] == '>' )
			{
				m1inza = 0 ;
				m2inza = 0 ;
				if ( minop && opklals )
				{
					opklals = 0 ;
					minop = 0 ;
					opnin = 1 ;
				}
				else
				{
					if ( ! pin1 && ! minop && ! opklals && ! opgrals && ! opbetwe && ! opnin && ! pin2 )
					{
						opgrals = 1 ;
					}
					else
					{
						merror = 1 ;
					}
				}
				minop = 0 ;
				continue ;
			}
			if ( p[ii] == ' ' || p[ii] == '	'  )
			{
				minop = 0 ;
				m1inza = 0 ;
				m2inza = 0 ;
				continue ;
			}
			merror = 1 ;	// alles andere ist Schrott 
			continue ;
		}
		else
		{	// Zahlen abhandeln
			minop = 0 ;
			if ( ! pin1 && ! minop && ! opklals && ! opgrals && ! opbetwe && ! opnin && ! pin2 )
				m1inza = 1 ;

//			if ( pin1 > 0 && ! m1inza )
//			{	// Loch in der 1. Zahl
//				merror = 1 ;
//				continue ;
//			}

			if ( m1inza )
			{
				zahl1[pin1] = p[ii] ;
				pin1 ++ ;
				continue ;
			}

			if ( m2inza )
			{
				zahl2[pin2] = p[ii] ;
				pin2 ++ ;
				continue ;
			}

			if ( opgrals || opklals || opnin && ! pin1 )
			{
				if ( pin2 > 0 && ! m2inza )
				{	// Loch in der 2. Zahl
					merror = 1 ;
					continue ;
				}
				m2inza = 1 ;
				zahl2[pin2] = p[ii] ;
				pin2 ++ ;
				continue ;
			}
			if ( opbetwe &&  pin1 )
			{
				if ( pin2 > 0 && ! m2inza )
				{	// Loch in der 2. Zahl
					merror = 1 ;
					continue ;
				}

				m2inza = 1 ;
				zahl2[pin2] = p[ii] ;
				pin2 ++ ;
				continue ;
			}
		}
		continue ;	// Schleifenende ohne alles 
	}

// Falls genau ein wert oder gar nix, dann "konventioneller" Zustand 161110 : sonderrange ist extra
	if ( ! sonderrange && ! klalsp && ! gralsp && ( winp == 1 || winp == 0 ) && ! ninp && ! vobp )
		return 0 ;

// Eingabe "Typ2" merken
	char * po = (char *) malloc (strlen ( p )+ 3 ) ;

	sprintf ( po , "%s", p ) ;
	tt_rangin[i] = po ;
	tt_rangini[i] = 2 ;

// baue range-string auf : (incl kleiner optimizer , da waere noch allerlei m�glich )
		if ( klalsp && gralsp )
		{
			if ( klals == grals )	// ergibt genau ein "not in "
			{
				nin[ninp] = klals ;
				klalsp = 0 ; gralsp = 0 ;
			}
			if ( klals > grals )	// ergibt genau ein "between"
			{
				von [vobp] = grals ;
				bis [vobp] = klals ;
				vobp ++ ;
				klalsp = 0 ; gralsp = 0 ;
			}

		}

	po = (char *) malloc ( 2000 ) ;

	char feldname [99] ;
	char orfeld[10] ;
	sprintf ( orfeld, "" ) ;
	sprintf ( feldname, "%s.%s" , tt_tabnam[i] ,tt_feldnam[i] ) ;
// Klammer auf
	sprintf ( po, "( " ) ;
	if ( klalsp ) 
	{
		sprintf ( zahl1, " %s %s < %01ld" ,orfeld, feldname, klals ) ;
		strcat ( po , zahl1 ) ;
		sprintf ( orfeld , "or" ) ;
	}
	if ( gralsp ) 
	{
		sprintf ( zahl1, " %s %s > %01ld" ,orfeld, feldname, grals ) ;
		strcat ( po , zahl1 ) ;
		sprintf ( orfeld , "or" ) ;
	}
	if ( vobp )
	{
		for ( int i2 = 0 ; i2 < vobp ; i2 ++ )
		{
			sprintf ( zahl1, " %s %s between %01ld and %01ld" ,orfeld, feldname, von[i2] , bis[i2]  ) ;
			strcat ( po , zahl1 ) ;
			sprintf ( orfeld , "or" ) ;
			if ( i2 > 10 )
			{	// Notbremse irgendwie
				if ( strlen ( po ) > 1920 )
					break ;
			}
		}
	}

	if ( winp )
	{
		sprintf ( zahl1 , " %s %s in (" , orfeld, feldname ) ;
		strcat ( po , zahl1 ) ;
		sprintf ( orfeld , "" ) ;
		for ( int i2 = 0 ; i2 < winp ; i2 ++ )
		{
			sprintf ( zahl1, " %s%01ld" ,orfeld, win[i2] ) ;
			strcat ( po , zahl1 ) ;
			sprintf ( orfeld , "," ) ;
			if ( i2 > 10 )
			{	// Notbremse irgendwie
				if ( strlen ( po ) > 1920 )
					break ;
			}
		}
		sprintf ( orfeld, ")" ) ;
		strcat ( po , orfeld ) ;
		sprintf ( orfeld, "or" ) ;
	}

	if ( ninp )
	{

		if ( strlen( orfeld) > 1  )
		{
			sprintf ( orfeld ," and " ) ;
		}
		sprintf ( zahl1 , " %s %s not in (" , orfeld, feldname ) ;
		strcat ( po , zahl1 ) ;
		sprintf ( orfeld , "" ) ;
		for ( int i2 = 0 ; i2 < ninp ; i2 ++ )
		{
			sprintf ( zahl1, " %s%01ld" ,orfeld, nin[i2] ) ;
			strcat ( po , zahl1 ) ;
			sprintf ( orfeld , "," ) ;
			if ( i2 > 10 )
			{	// Notbremse irgendwie
				if ( strlen ( po ) > 1980 )
					break ;
			}
		}
		sprintf ( orfeld, ")" ) ;
		strcat ( po , orfeld ) ;
		sprintf ( orfeld, "or" ) ;
	}

// Klammer zu 
	sprintf ( orfeld, ")" ) ;
	strcat ( po , orfeld ) ;
	
	tt_rangtyp2[tt_range[i]] = po ; 
	tt_rangini[i] = 2 ;

	return 1 ;

}	// 230909 E

int macherange ( int i, char* range1, char * range2 )
{

	int retco = 0 ;

// 220909 : INRANGE zentral auswerten, von/bis-Werte loeschen usw.
	if (! strncmp ( range1, "INRANGE" ,7 ))
	{
		char * p = (char *) malloc (strlen ( range2 )+ 3 ) ;
		sprintf ( p , "%s", range2 ) ;
		tt_rangin[i] = p ;
		tt_rangini[i] = 1 ;	// 220909

		switch ( tt_datetyp[i])
		{
			case (iDBCHAR) :
			case (iDBVARCHAR) :	// 280612
					tt_cvrange[tt_range[i]][0] = -1 ; 
					tt_cbrange[tt_range[i]][0] = -1 ;
				break ;

			case(iDBSERIAL):	// 201209
			case (iDBSMALLINT) :
			case (iDBINTEGER) :
					tt_lvrange[tt_range[i]] =  11 ;
					tt_lbrange[tt_range[i]] = -11 ;
				break ;
			case (iDBDECIMAL) :
					tt_dvrange[tt_range[i]] =  11.0 ;
					tt_dbrange[tt_range[i]] = -11.0 ;
				break ;

			case (iDBDATETIME) :
			case (iDBDATUM) :
					tt_cvrange[tt_range[i]][0] = -1 ; 
					tt_cbrange[tt_range[i]][0] = -1 ;
				break ;
		};
		return retco ;
	}

// 220909 E : INRANGE zentral auswerten

// 230909 extended Ranges handeln
// 230909 Einschraenkungen :
// nur positive ganze Zahlen ( d.h. smallint,integer,long )
// erlaubte Syntax : 
//	Trenner "," ";"
// Operatoren : "-","<" , ">" , "<>"
// vor- und folgeblanks ignorieren
// Syntax-Fehler f�hrt zu Ignorieren des Segments bis zum n�chsten Trenner


	int sonderrange = 0 ;	// 161110 

	if (   ( range1[0] == '#' ) 
		&& (( tt_datetyp[i]  == iDBSERIAL)
		|| ( tt_datetyp[i] == iDBSMALLINT)
		|| ( tt_datetyp[i] == iDBINTEGER)
		|| ( tt_datetyp[i] == iDBDECIMAL)))
	{
		sonderrange = 1 ;
	}



	int rangext = 0 ;	// 230909

	switch ( tt_datetyp[i])
	{
	case (iDBCHAR) :
	case (iDBVARCHAR):	// 280612
		if ( strlen(range1) > 49 )	range1[49] = '\0' ;
		if ( strlen(range2) > 49 )	range2[49] = '\0' ;

// Beachte immer den Unterschied zw. leerem String und string mit Laenge 0 
		if ( strlen(range1) == 0 || strlen(range2) == 0 )
		{
			if ( strlen(range1) > 0 ) sprintf ( range2, "%s", range1 );
			else
			{	
				if ( strlen(range2) > 0 ) sprintf ( range1, "%s", range2 );
				else
				{ 
					tt_cvrange[tt_range[i]][0] = -1 ; 
					tt_cbrange[tt_range[i]][0] = -1 ;
					break ;
				}
			}
		};
		
		if (! strcmp(clipped( range1),clipped(range2)))
		{
			sprintf ( tt_cvrange[tt_range[i]], "%s", clipped(range1));
			tt_cbrange[tt_range[i]][0] = -1 ;
			break ;
		}

		sprintf ( tt_cvrange[tt_range[i]], "%s", clipped(range1));
		sprintf ( tt_cbrange[tt_range[i]], "%s", clipped(range2));
		break ;

	case(iDBSERIAL):	// 201209
	case (iDBSMALLINT) :
	case (iDBINTEGER) :
		rangext = extendrange( i, range1, range2 , sonderrange ) ;	// 230909 -161110
		if ( ! rangext )
		{	// 230909 : so war es bisher .....

			if ( strlen(range1) > 49 )	range1[49] = '\0' ;
			if ( strlen(range2) > 49 )	range2[49] = '\0' ;

			if ( !isnumeric( tt_datetyp[i], range1 )||
				 !isnumeric( tt_datetyp[i], range2 ) )
			{
				range1[0] = '\0' ;
				range2[0] = '\0' ;
			}

		// leerer Wert ist ungueltiger Wert
			if ( strlen(range1) == 0 || strlen(range2) == 0 )
			{	
				if ( strlen(range1) > 0 ) sprintf ( range2, "%s", range1 );
				else
				{	
					if ( strlen(range2) > 0 ) sprintf ( range1, "%s", range2 );
					else
					{	 
						tt_lvrange[tt_range[i]] =  11 ;
						tt_lbrange[tt_range[i]] = -11 ;
						break ;
					}
				}
			}

			tt_lvrange[tt_range[i]]= atol (range1);
			tt_lbrange[tt_range[i]]= atol (range2);
			break ;
		}
		else	// rangeext erfolgreich
		{
			tt_lvrange[tt_range[i]] =  11 ;
			tt_lbrange[tt_range[i]] = -11 ;
		}
		break ;

	case (iDBDECIMAL) :
		if ( sonderrange )		// 161110 
		rangext = extendrange( i, range1, range2 , sonderrange ) ;	// 161110
		if ( ! rangext )	// sowar es bishher
		{
			if ( strlen(range1) > 49 )	range1[49] = '\0' ;
			if ( strlen(range2) > 49 )	range2[49] = '\0' ;

			if ( !isnumeric( tt_datetyp[i], range1 )||
				!isnumeric( tt_datetyp[i], range2 ) )
			{
				range1[0] = '\0' ;
				range2[0] = '\0' ;
			}

			// leerer Wert ist ungueltiger Wert
			if ( strlen(range1) == 0 || strlen(range2) == 0 )
			{	
				if ( strlen(range1) > 0 ) sprintf ( range2, "%s", range1 );
				else
				{	
					if ( strlen(range2) > 0 ) sprintf ( range1, "%s", range2 );
					else
					{ 
						tt_dvrange[tt_range[i]] =  11.0 ;
						tt_dbrange[tt_range[i]] = -11.0 ;
						break ;
					}
				}
			}

			tt_dvrange[tt_range[i]]= atof (range1);
			tt_dbrange[tt_range[i]]= atof (range2);
			break ;

		}
		else	// 161110 erfolgreich rangext 
		{
				tt_dvrange[tt_range[i]] =  11.0 ;
				tt_dbrange[tt_range[i]] = -11.0 ;
		}
	case (iDBDATETIME) :		// 111205
	case (iDBDATUM) :	// hier sollten nur noch gueltige Dati ankommen ....
		if ( strlen(range1) > 49 )	range1[49] = '\0' ;
		if ( strlen(range2) > 49 )	range2[49] = '\0' ;

		if ( strlen(range1) == 0 || strlen(range2) == 0 )
		{
			if ( strlen(range1) > 0 ) sprintf ( range2, "%s", range1 );
			else
			{	
				if ( strlen(range2) > 0 ) sprintf ( range1, "%s", range2 );
				else
				{ 
					tt_cvrange[tt_range[i]][0] = -1 ; 
					tt_cbrange[tt_range[i]][0] = -1 ;
					break ;
				}
			}
		};


		if (! strcmp(clipped( range1),clipped(range2)))
		{
			sprintf ( tt_cvrange[tt_range[i]], "%s",  clipped(range1)) ;
			tt_cbrange[tt_range[i]][0] = -1 ;
			
			sqldatdst( & (tt_tvrange[tt_range[i]]) , range1 ) ;

			break ;
		}

		sprintf ( tt_cvrange[tt_range[i]], "%s", clipped(range1));
		sqldatdst( & (tt_tvrange[tt_range[i]]) , range1 ) ;

		sprintf (  tt_cbrange[tt_range[i]], "%s", clipped(range2));
		sqldatdst(  & ( tt_tbrange[tt_range[i]]) , range2 ) ;
		break ;
	}

	return retco ;
}

void Crange::OnBnClickedOk()
{

	int retco ;
	int x ;	// 250810
	char buf1[512] ;
	char buf2[512] ;
	UpdateData(TRUE) ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i ++ )
	{
		if ( ! tt_range[i] ) continue ;

		switch (tt_range[i])
		{
		case 1 :
			x = m_cedit1.GetLine(0,buf1,500);
			buf1[x] ='\0' ;		// 250810
			x = m_cedit9.GetLine(0,buf2,500);
			buf2[x] ='\0' ;		// 250810

			retco = macherange( i,buf1,buf2 );
			break ;
		case 2 :
			if ( globmp > 4 )
			{
				x = m_cedit2.GetLine(0,buf1,500);
				buf1[x] ='\0' ;		// 250810
				x = m_cedit10.GetLine(0,buf2,500);
				buf2[x] ='\0' ;		// 250810
			}
			else
			{
				x = m_cedit3.GetLine(0,buf1,500);
				buf1[x] ='\0' ;		// 250810
				x = m_cedit11.GetLine(0,buf2,500);
				buf2[x] ='\0' ;		// 250810
			}

			retco = macherange( i,buf1,buf2 );

			break ;
		case 3 :
			if ( globmp > 4 )
			{
				x = m_cedit3.GetLine(0,buf1,500);
				buf1[x] ='\0' ;		// 250810
				x = m_cedit11.GetLine(0,buf2,500);
				buf2[x] ='\0' ;		// 250810
			}
			else
			{
				x = m_cedit5.GetLine(0,buf1,500);
				buf1[x] ='\0' ;		// 250810
				x = m_cedit13.GetLine(0,buf2,500);
				buf2[x] ='\0' ;		// 250810
			}

			retco = macherange( i,buf1,buf2 );
			break ;
		case 4 :
			if ( globmp > 4 )
			{
				x = m_cedit4.GetLine(0,buf1,500);
				buf1[x] ='\0' ;		// 250810
				x = m_cedit12.GetLine(0,buf2,500);
				buf2[x] ='\0' ;		// 250810
			}
			else
			{
				x = m_cedit7.GetLine(0,buf1,500);
				buf1[x] ='\0' ;		// 250810
				x = m_cedit15.GetLine(0,buf2,500);
				buf2[x] ='\0' ;		// 250810
			}
			retco = macherange( i,buf1,buf2 );
			break ;
		case 5 :
			x = m_cedit5.GetLine(0,buf1,500);
			buf1[x] ='\0' ;		// 250810
			x = m_cedit13.GetLine(0,buf2,500);
			buf2[x] ='\0' ;		// 250810
			retco = macherange( i,buf1,buf2 );
			break ;
		case 6 :
			x = m_cedit6.GetLine(0,buf1,500);
			buf1[x] ='\0' ;		// 250810
			x = m_cedit14.GetLine(0,buf2,500);
			buf2[x] ='\0' ;		// 250810
			retco = macherange( i,buf1,buf2 );
			break ;
		case 7 :
			x = m_cedit7.GetLine(0,buf1,500);
			buf1[x] ='\0' ;		// 250810
			x = m_cedit15.GetLine(0,buf2,500);
			buf2[x] ='\0' ;		// 250810
			retco = macherange( i,buf1,buf2 );
			break ;
		case 8 :
			x = m_cedit8.GetLine(0,buf1,500);
			buf1[x] ='\0' ;		// 250810
			x = m_cedit16.GetLine(0,buf2,500);
			buf2[x] ='\0' ;		// 250810
			retco = macherange( i,buf1,buf2 );
			break ;
		}
	}

// 270307

	if ( mehrfach_o > 0 )
	{
		int num_auswahl; 
			num_auswahl=m_Combo1.GetCurSel();
			if ( num_auswahl > -1 )
				externe_sortnr = mehrfach_o = num_auswahl + 1 ;
					// 280307 : externe ... dazu
	}


	v_abbruch = 0 ;
	CDialog::OnOK();
}

void Crange::OnBnClickedCancel()
{
	v_abbruch = 1 ;
		CDialog::OnCancel();
}

int Crange::getabbruch () 
{
	return v_abbruch ;
}
int Crange::getanzahl () 
{
	return m_vcedit26 ;
}

void Crange::OnCbnSelchangeCombo1()
{
	UpdateData(TRUE) ;
	int num_auswahl; 
		num_auswahl=m_Combo1.GetCurSel();
		if ( num_auswahl > -1 )
			mehrfach_o = num_auswahl + 1 ;
	UpdateData(FALSE) ;

}

void Crange::OnCbnKillfocusCombo1()
{
	UpdateData(TRUE) ;
	int num_auswahl; 
		num_auswahl=m_Combo1.GetCurSel();
		if ( num_auswahl > -1 )
			mehrfach_o = num_auswahl + 1 ;
	UpdateData(FALSE) ;

	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}
