#include "stdafx.h"
#include "DbClass.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

#include "temptab.h"
#include "systables.h"
#include "syscolumns.h"
#include "form_ow.h"
#include "txt.h"
#include "range.h"
#include "item.h"
#include "kun.h"
#include "zustab.h"

extern DB_CLASS dbClass;
int verboteneFelder  ;	// 160113
char cverboteneFelder[120] ;	// 160113
static int badfeld = 0 ;	// 160113 : ungueltiges Feld-Marker

FIL_CLASS Fil ;
KUN_CLASS Kun ;
A_KUN_CLASS A_Kun ;


extern int datenausdatei ;	// wird in MainFrm.cpp gesetzt
extern short GlobalLila ;	// wird in MainFrm.cpp gesetzt
extern int macherange ( int , char * , char * ) ;	// 300904 aus range.cpp
int holerangesausdatei(char *, char * , char *) ;	// 300904 aus MainFrm.cpp

#define PRIV_RETK           "retk"  
#define PRIV_RETP           "retp"  
#define PRIV_A_BAS         "a_bas"  
#define NEBEN           1  
#define HAUPT           0  

// Mehr als 250 felder sollten nur sehr selten vorkommen 
char  tt_tabnam [MAXANZ][19];
int   tt_tabout [MAXANZ];
char  tt_feldnam[MAXANZ][19]; 	
int    tt_datetyp[MAXANZ];		// Datentyp in der Daba
int    tt_datelen[MAXANZ];		// Stringlaenge in der Daba
int    tt_feldtyp[MAXANZ];	    // Datentyp fuer Generator
int    tt_referenz  [MAXANZ];	// nur Index auf referenz-Liste
int    tt_range     [MAXANZ];	// nur Index auf range-Liste 
void * tt_sqlval    [MAXANZ];	// pointer auf (char-)Feldinhalte 
long	tt_sqllong    [MAXANZ];	// Matrix fuer longs und shorts 
double  tt_sqldouble [MAXANZ];	// Matrix fuer double 

void * savtt_sqlval    [MAXANZ];	// 310506 : pointer auf (char-)Feldinhalte 
long	savtt_sqllong    [MAXANZ];	// 310506 : Matrix fuer longs und shorts 
double  savtt_sqldouble [MAXANZ];	// 310506 : Matrix fuer double 

int     tt_hauneb [MAXANZ] ;	// Haupt- oder Nebencursor
								// um Entscheidung nur einmalig treffen zu muessen

 // mehr als 50 verschiedene Textbausteine sollten sehr selten sein
char  tt_creferenz[50][25];

// mehr als 20 verschiedene Bedingungen sind nicht erlaubt
char  tt_cvrange[MAXRANGE][50];	// maximale stringlaenge  49
char  tt_cbrange[MAXRANGE][50];
long  tt_lvrange[MAXRANGE];
long  tt_lbrange[MAXRANGE];
double  tt_dvrange[MAXRANGE];
double  tt_dbrange[MAXRANGE];
char  tt_qrange[MAXRANGE][100];	// Maximale Query-Laenge 99


static int aktupoint ;
static int v_abbruch ;
static int v_anzahl ;
static char tmyform_nr[99] ;

// 160305 -> zeiger auf diese felder
static int pa_bas_a ;
static int pa_bas_a_typ ;
static int pa_bas_a_bz1 ;

static int pretp_ret_vk_euro ;
static int pretp_ret_me ;



int TEMPTAB_CLASS::structprep (char * myform_nr)
{

	int refdi = 0 ;	// Pointer zu Referenztexten
	int rangdi = 0 ;	// Pointer zu Ranges
	aktupoint = 0 ;
	sprintf ( tmyform_nr, "%s", myform_nr ) ;

	NFORM_TAB_CLASS Nform_tab_class ;
	SYSTABLES_CLASS Systables_class ;
	NFORM_FELD_CLASS Nform_feld_class ;
	SYSCOLUMNS_CLASS Syscolumns_class ;

	tt_tabnam [0][0] = '\0';

	sprintf ( nform_tab.form_nr , "%s", myform_nr );
	nform_tab.lila = GlobalLila ;

	pa_bas_a = pa_bas_a_typ = pa_bas_a_bz1 = -1 ;
	pretp_ret_vk_euro = pretp_ret_me = -1 ;	

//	NFORM_TAB_CLASS Nform_tab_class ;
	Nform_tab_class.opennformtab();
	while (!Nform_tab_class.lesenformtab () && aktupoint < 249 )	//       DATENSATZ lesen
	{


		sprintf ( systables.tabname, "%s", nform_tab.tab_nam );
//		SYSTABLES_CLASS Systables_class ;
		Systables_class.opensystables ();
		if ( ! Systables_class.lesesystables ())	// Test,ob es die Tabelle gibt 
		{
			sprintf ( nform_feld.tab_nam ,"%s", nform_tab.tab_nam) ;
			sprintf ( nform_feld.form_nr ,"%s", nform_tab.form_nr) ;
			nform_feld.lila = GlobalLila ;

//			NFORM_FELD_CLASS Nform_feld_class ;
			Nform_feld_class.opennform_feld() ;
			while ( ! Nform_feld_class.lesenform_feld ())	// Alle aktiven Felder dieser Tabelle
			{


				syscolumns.tabid = systables.tabid ;
				sprintf ( syscolumns.colname ,"%s", nform_feld.feld_nam) ;
//				SYSCOLUMNS_CLASS Syscolumns_class ;
				Syscolumns_class.opensyscolumns ();
				if (! Syscolumns_class.lesesyscolumns()) 
				{

//	Matritzen  laden ;
					sprintf ( tt_tabnam [aktupoint],"%s",clipped( nform_feld.tab_nam));

					if (  (!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_RETK))
					||(!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_RETP))
					||(!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_A_BAS))
					   )
					{
						tt_hauneb [aktupoint] = HAUPT ;

// 160305 A
						if (!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_RETP))
						{
							if ( ! strcmp ( clipped( nform_feld.feld_nam),"ret_vk_euro"))
								pretp_ret_vk_euro = aktupoint ;
							else
								if( ! strcmp ( clipped( nform_feld.feld_nam),"ret_me")) 	
									pretp_ret_me = aktupoint ;
								else	// Artikeleintrag evtl. doppelt
									if( ! strcmp ( clipped( nform_feld.feld_nam),"a")) 	
									pa_bas_a = aktupoint ;
						}
						if (!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_A_BAS))
						{
							if ( ! strcmp ( clipped( nform_feld.feld_nam),"a_typ"))
								pa_bas_a_typ = aktupoint ;
							else
								if( ! strcmp ( clipped( nform_feld.feld_nam),"a_bz1")) 	
									pa_bas_a_bz1 = aktupoint ;
								else	// Artikel-Eintrag evtl. doppelt
									if( ! strcmp ( clipped( nform_feld.feld_nam),"a")) 	
									pa_bas_a = aktupoint ;
						}
						
// 160305 E

					}
					else
					{
						tt_hauneb [aktupoint] = NEBEN ;
					}

					tt_tabout [aktupoint] = (int) nform_tab.delstatus ;
// OUTER ist irrelevant
					tt_tabout [aktupoint] = 0 ;

					sprintf ( tt_feldnam[aktupoint],"%s",clipped( nform_feld.feld_nam)); 	
					tt_datetyp[aktupoint] = syscolumns.coltype ;	// Datentyp in der Daba
					tt_datelen[aktupoint] = syscolumns.collength ;	// Datenlaenge in der Daba
					tt_feldtyp[aktupoint] = nform_feld.feld_typ ;	// Datentyp fuer Generator
					if ( strlen ( clipped( nform_feld.krz_txt)) > 0 && nform_feld.feld_typ)
					{
						refdi ++ ;	// Achtung : index geht erst mit Feld 1 los ... 
						sprintf ( tt_creferenz[refdi], "%s", clipped(nform_feld.krz_txt));
						tt_referenz [aktupoint] = refdi ;
					}
					else
						tt_referenz [aktupoint] = 0 ;

					if ( nform_feld.range )
					{
						rangdi ++ ;	// Achtung : index geht erst mit Feld 1 los ... 
						tt_referenz [aktupoint] = rangdi ;
}
					else
					{
					}
					tt_range[aktupoint] = nform_feld.range ;	
					aktupoint ++ ;
					tt_tabnam[aktupoint][0] = '\0' ;
				}
			}	// fetch nformfeld_curs
		}		// if tabid_curs
	}			// nformtab_curs 

	return aktupoint ;
}
int TEMPTAB_CLASS::getanzahl (void)
{
	return v_anzahl ;
}

// 121104 A
void TEMPTAB_CLASS::inittemptab ()
{
	int i ;
	i=0;
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;
		if ( tt_range[i] ) { i++ ; continue ; }	// Range-felder NICHT initialisieren
		switch ( tt_feldtyp[i] )
		{
		case 1 : // ptabn
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
			sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
// 310506 : so sah das bisher aus :			tt_sqlval[i], (char *)NULL ;
 				sprintf ( (char *)tt_sqlval[i] ," " ) ;	// 310506
				break ;
			case(iDBSMALLINT):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
				tt_sqllong[i] = 0 ;
				break ;
			case(iDBINTEGER):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
				tt_sqllong[i] = 0 ;
				break ;
			case(iDBDECIMAL):
				savtt_sqldouble[i] = tt_sqldouble[i] ;	// 310506
				tt_sqldouble[i] =0.0L ;
				break ;
			default :	// schlechte Notbremse
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
				break ;
			}
//			memcpy ( &ptabn,&ptabn_null, sizeof(struct PTABN));

			break ;

		case 2 : // Textbaustein 
				savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
				tt_sqllong[i] = 0 ;
			break ;
		case 3 : // adresse
			savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
			tt_sqllong[i] = 0 ;
//				memcpy ( &adr,&adr_null, sizeof(struct ADR));
			break ;
		default :	// Standard-Felder 
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
				break ;
			case(iDBSMALLINT):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
				tt_sqllong[i] = 0L ;
				break ;
			case(iDBINTEGER):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
				tt_sqllong[i] = 0L ;
				break ;
			case(iDBDECIMAL):
				savtt_sqldouble[i] = tt_sqldouble[i] ;	// 310506
				tt_sqldouble[i]= 0.0L ;
				break ;
			case(iDBDATUM):
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i]," ")  ;
				break ;		
			default :	// schlechte Notbremse
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i], " ");
				break ;
			
			}
			break ;
		} ;
		i ++ ;
	}		// while	

}

// 310506 A
void TEMPTAB_CLASS::reloadtemptab ()
{
	int i ;
	i=0;
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;
		if ( tt_range[i] ) { i++ ; continue ; }	// Range-felder NICHT initialisieren
		switch ( tt_feldtyp[i] )
		{
		case 1 : // ptabn
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
			sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			case(iDBSMALLINT):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			case(iDBINTEGER):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			case(iDBDECIMAL):
				tt_sqldouble[i] = savtt_sqldouble[i] ;
				break ;
			default :	// schlechte Notbremse
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			}
			break ;

		case 2 : // Textbaustein 
				tt_sqllong[i] = savtt_sqllong[i] ;
			break ;
		case 3 : // adresse
			tt_sqllong[i] = savtt_sqllong[i] ;
			break ;
		default :	// Standard-Felder 
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;	
				break ;
			case(iDBSMALLINT):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			case(iDBINTEGER):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			case(iDBDECIMAL):
				tt_sqldouble[i] = savtt_sqldouble[i] ;
				break ;
			case(iDBDATUM):
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;		
			default :	// schlechte Notbremse
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			}
			break ;
		} ;
		i ++ ;
	}		// while	

}

double TEMPTAB_CLASS::a_bas_a(void)
{
		if ( pa_bas_a == -1 ) return 0 ;
		return (tt_sqldouble[pa_bas_a] ) ;
}

short TEMPTAB_CLASS::a_bas_a_typ(void)
{

	if ( pa_bas_a_typ == -1 ) return 0 ;
	return ( (short) tt_sqllong[pa_bas_a_typ] ) ;
}

char * TEMPTAB_CLASS::a_bas_a_bz1(void)
{
		if ( pa_bas_a_bz1 == -1 ) return NULL ;
		return ( (char *) tt_sqlval[pa_bas_a_bz1] ) ;
}
double TEMPTAB_CLASS::retp_ret_me(void)
{
		if ( pretp_ret_me == -1 ) return 0 ;
		return (tt_sqldouble[pretp_ret_me] ) ;
}
double TEMPTAB_CLASS::retp_ret_vk_euro(void)
{
	int di =  0 ;
	di = di ;
	if ( pretp_ret_vk_euro == -1 ) return 0 ;
		return (tt_sqldouble[pretp_ret_vk_euro] ) ;
}

/* ---->
double TEMPTAB_CLASS::get_lsp_a(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ), PRIV_LSP))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ) , "a" )) return ( tt_sqldouble[i] );
		}
	}
	return 0.0L ;
};
< ----- */

long get_retk_kun(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_RETK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ), "kun" )) return ( tt_sqllong[i] );
		}
	}
	return 0 ;
}


short get_retk_kun_fil(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_RETK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "kun_fil" )) return ( (short)tt_sqllong[i] );
		}
	}
	return 0 ;
}

short TEMPTAB_CLASS::get_retk_mdn(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_RETK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "mdn" )) return ( (short)tt_sqllong[i] );
		}
	}

// Hilfsweise auch noch in retp suchen 
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ), PRIV_RETP))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ) , "mdn" )) return ( (short)tt_sqllong[i] );
		}
	}

	return 0 ;
};

short TEMPTAB_CLASS::get_retk_fil(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_RETK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "fil" )) return ( (short)tt_sqllong[i] );
		}
	}

// Hilfsweise auch noch in retp suchen 
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ), PRIV_RETP))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ) , "fil" )) return ( (short)tt_sqllong[i] );
		}
	}

	return 0 ;
};

long TEMPTAB_CLASS::get_retk_ret(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_RETK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "ret" )) return ( (long)tt_sqllong[i] );
		}
	}

// Hilfsweise auch noch in retp suchen 
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ), PRIV_RETP))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ) , "ret" )) return ( (long)tt_sqllong[i] );
		}
	}

	return 0L ;
};

long TEMPTAB_CLASS::get_retp_posi(void)
{
		
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ), PRIV_RETP))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ) , "posi" )) return ( (long)tt_sqllong[i] );
		}
	}

	return 0L  ;
};

void ladetyp( int i, void * pointi )
{

	if ( i > 33 )
	{
		 i  = i;
	}
	
		                
	switch ( tt_feldtyp[i] )
	{
	case 1 : // ptabn
		switch ( tt_datetyp[i] )
		{
		case(iDBCHAR):
			if (  (char *) pointi == NULL )
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
			else
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *) pointi ) ;
				break ;
		case(iDBSMALLINT):
//			tt_sqllong[i] = *((long *) pointi) ;	 * 091209
			tt_sqllong[i] = (long)*((short *) pointi) ;

				break ;
		case(iDBINTEGER):
			tt_sqllong[i] = *((long *)pointi) ;
				break ;
		case(iDBDECIMAL):
			tt_sqldouble[i] = *((double*)pointi) ;
				break ;
		default :	// schlechte Notbremse
//	160113 lieber gar nix machen : 		sprintf ( (char *)tt_sqlval[i] ," " ) ;
				badfeld ++ ;	// 160113
				break ;
		}

		break ;

	case 2 : // Textbaustein 
				tt_sqllong[i] = *((long *)pointi) ;
		break ;
	case 3 : // adresse
		tt_sqllong[i] = *((long *)pointi) ;
		break ;
	default :	// Standard-Felder 
		switch ( tt_datetyp[i] )
		{
		case(iDBCHAR):
			if ( (char *)pointi == NULL )
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
			else
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)pointi ) ;
				break ;
		case(iDBSMALLINT):

//			tt_sqllong[i] = *((long *) pointi) ;	 * 190105
			tt_sqllong[i] = (long)*((short *) pointi) ;
				break ;
		case(iDBINTEGER):
			tt_sqllong[i] = *((long *)pointi) ;
				break ;
		case(iDBDECIMAL):
			tt_sqldouble[i]= *((double *) pointi) ;
				break ;
		case(iDBDATUM):
			if ( (char *) pointi == NULL )
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
			else
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)pointi ) ;
				break ;
		default :	// schlechte Notbremse
			badfeld ++ ;	// 160113
// 160113			if ( (char *)pointi == NULL )
// 160113				sprintf ( (char *)tt_sqlval[i] ," " ) ;
// 160113			else
// 160113				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)pointi ) ;
				break ;
			
		}
		break ;
	} ;
}


bool komplettierefil ( void )
{

	badfeld = 0 ;	// 160113

	int oi = 0 ;
	int ei = 0 ;	// 160113
	char hilfech[26] ;
	int colint ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "fil"))
		{
			oi = 1 ;
			ei = 0 ;	// 160113
			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
			colint = (int)strlen (hilfech);
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "abr_period" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.abr_period );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.adr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr_lief" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.adr_lief );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "afl" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.afl );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "auf_typ" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.auf_typ );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "best_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.best_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bli_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.bli_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "dat_ero" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.dat_ero );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "daten_mnp" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.daten_mnp );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "delstatus" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.delstatus );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fil" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.fil );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fil_kla" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.fil_kla );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fil_gr" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.fil_gr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fl_lad" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.fl_lad );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fl_nto" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.fl_nto );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fl_vk_ges" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.fl_vk_ges );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "frm" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.frm );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "iakv" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.iakv );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "inv_rht" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.inv_rht );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.kun );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lief" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.lief );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lief_rht" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.lief_rht );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lief_s" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.lief_s );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ls_abgr" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.ls_abgr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ls_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.ls_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ls_sum" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.ls_sum );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "mdn" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.mdn );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pers" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.pers );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pers_anz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.pers_anz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pos_kum" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.pos_kum );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_ausw" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.pr_ausw );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_bel_entl" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.pr_bel_entl );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_fil_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.pr_fil_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_lst" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.pr_lst );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_vk_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.pr_vk_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_bed_theke_lng" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.reg_bed_theke_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_kt_lng" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.reg_kt_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_kue_lng" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.reg_kue_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_lng" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.reg_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_tks_lng" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.reg_tks_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_tkt_lng" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.reg_tkt_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ret_entl" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.ret_entl );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "smt_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.smt_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sonst_einh" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.sonst_einh );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sprache" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.sprache );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sw_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.sw_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tou" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.tou );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "umlgr" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.umlgr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "verk_st_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.verk_st_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vrs_typ" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.vrs_typ );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "inv_akv" )) 
			{
				ei ++ ; ladetyp( i, (void *)fil.inv_akv );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "planumsatz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&fil.planumsatz );
				continue ;
			};
			if ( !ei )	// 160113 : unbekanntes Feld
					break ;

		continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// while 
// 160113 : ret-code 
	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE ; 
}


bool komplettierekun ( void )
{

	int oi = 0 ;
	int ei = 0 ;	// 160113
	badfeld = 0 ;	// 160113
	char hilfech[26] ;
	int colint ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "kun"))
		{
			oi = 1 ;
			ei = 0 ;	// 160113
			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
			colint = (int)strlen (hilfech);
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "mdn" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.mdn );
				continue ;
			};

// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fil" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.fil );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.adr1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.adr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr3" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.adr3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_seit" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_seit );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt_nr1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.txt_nr1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "frei_txt1" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.frei_txt1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_krz1" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_krz1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_bran" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_bran );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_krz2" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_krz2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_krz3" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_krz3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_typ" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_typ );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bbn" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.bbn );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_stu" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_stu );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_lst" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_lst );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vereinb" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.vereinb );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "inka_nr" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.inka_nr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vertr1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.vertr1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vertr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.vertr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "statk_period" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.statk_period );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "a_period" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.a_period );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sprache" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sprache );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt_nr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.txt_nr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "frei_txt2" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.frei_txt2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "freifeld1" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.freifeld1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "freifeld2" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.freifeld2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tou" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.tou );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vers_art" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.vers_art );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lief_art" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.lief_art );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fra_ko_ber" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.fra_ko_ber );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "rue_schei" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.rue_schei );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "form_typ1" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.form_typ1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "auflage1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.auflage1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "freifeld3" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.freifeld3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "freifeld4" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.freifeld4 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "zahl_art" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.zahl_art );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "zahl_ziel" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.zahl_ziel );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "form_typ2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.form_typ2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "auflage2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.auflage2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt_nr3" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.txt_nr3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "frei_txt3" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.frei_txt3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "nr_bei_rech" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.nr_bei_rech );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "rech_st" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.rech_st );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sam_rech" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sam_rech );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "einz_ausw" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.einz_ausw );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "gut" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.gut );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "rab_schl" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.rab_schl );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bonus1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.bonus1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bonus2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.bonus2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tdm_grenz1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.tdm_grenz1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tdm_grenz2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.tdm_grenz2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "jr_plan_ums" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.jr_plan_ums );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "deb_kto" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.deb_kto );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kred_lim" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kred_lim );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "inka_zaehl" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.inka_zaehl );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bank_kun" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.bank_kun );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "blz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.blz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kto_nr" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kto_nr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "hausbank" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.hausbank );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_of_po" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_of_po );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_of_lf" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_of_lf );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_of_best" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_of_best );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "delstatus" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.delstatus );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_bran2" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_bran2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "rech_fuss_txt" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.rech_fuss_txt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ls_fuss_txt" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.ls_fuss_txt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ust_id" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.ust_id );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "rech_kopf_txt" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.rech_kopf_txt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ls_kopf_txt" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.ls_kopf_txt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "gn_pkt_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.gn_pkt_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sw_rab" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sw_rab );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bbs" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.bbs );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "inka_nr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.inka_nr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sw_fil_gr" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sw_fil_gr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sw_fil" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sw_fil );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ueb_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.ueb_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "modif" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.modif );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_leer_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_leer_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ust_id16" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.ust_id16 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "iln" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.iln );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "waehrung" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.waehrung );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_ausw" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_ausw );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_hier" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_hier );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_ausw_ls" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_ausw_ls );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_ausw_re" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_ausw_re );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_gr1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_gr1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_gr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_gr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "eg_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.eg_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bonitaet" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.bonitaet );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kred_vers" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kred_vers );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kst" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kst );
				continue ;
			};

// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "edi_typ" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.edi_typ );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_dta" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_dta );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_umf" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_umf );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_abr" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_abr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_gesch" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_gesch );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_satz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_satz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_med" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_med );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_nam" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sedas_nam );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_abk1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_abk1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_abk2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_abk2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_abk3" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_abk3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_nr1" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sedas_nr1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_nr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sedas_nr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_nr3" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sedas_nr3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_vb1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_vb1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_vb2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_vb2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_vb3" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_vb3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_iln" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sedas_iln );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kond_kun" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kond_kun );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_schema" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_schema );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "plattform" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.plattform );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "be_log" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.be_log );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "stat_kun" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.stat_kun );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ust_nummer" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.ust_nummer );
				continue ;
			};

			if ( !ei )	// 160113
				break ;

		continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// while 
// 160113 : ret-code 
	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE ; 
}


// 230506 void komplettierea_kun ( void )
bool komplettierea_kun ( int istatus )
{

	// 230506 
	if ( istatus )
		memcpy ( &a_kun, &a_kun_null, sizeof ( struct A_KUN ) ) ;

	int oi = 0 ;
	int ei = 0 ;	// 160113
	badfeld = 0 ;
	char hilfech[26] ;
	int colint ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "a_kun"))
		{
			oi = 1 ;
			ei = 0 ;
			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
			colint = (int)strlen (hilfech);
// ####################
			if ( !strcmp ( hilfech, "mdn" )) 
			{
				ei ++ ; ladetyp( i, (void *)&a_kun.mdn );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "fil" )) 
			{
				ei ++ ; ladetyp( i,  (void *) &a_kun.fil );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "kun" )) 
			{
				ei ++ ; ladetyp( i,  (void *) &a_kun.kun );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a" )) 
			{
				ei ++ ; ladetyp( i,  (void *) &a_kun.a );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a_kun" )) 
			{
				ei ++ ; ladetyp( i,  (void *) a_kun.a_kun );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a_bz1" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.a_bz1 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "me_einh_kun" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.me_einh_kun );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "inh" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.inh );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "kun_bran2" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.kun_bran2 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "tara" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.tara );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "ean" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.ean );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a_bz2" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.a_bz2 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "hbk_ztr" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.hbk_ztr );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "kopf_text" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.kopf_text );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pr_rech_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.pr_rech_kz );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "modif" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.modif );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "text_nr" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.text_nr );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "devise" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.devise );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "geb_eti" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.geb_eti );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "geb_fill" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.geb_fill );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "geb_anz" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.geb_anz );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pal_eti" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.pal_eti );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pal_fill" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.pal_fill );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pal_anz" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.pal_anz );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pos_eti" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.pos_eti );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "sg1" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.sg1 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "sg2" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.sg2 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pos_fill" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.pos_fill );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "ausz_art" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.ausz_art );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "text_nr2" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.text_nr2 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "cab" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.cab );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a_bz3" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.a_bz3 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a_bz4" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.a_bz4 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "li_a" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.li_a );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "geb_fakt" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.geb_fakt );
				continue ;
			};
			if ( !ei )	// 160113
				break ;
		continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// while 

	// 160113 : ret-code 
	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE ; 
}



void TEMPTAB_CLASS::komplettiereSatz(void)
{
	int di ;

	verboteneFelder = 0 ;	// 160113
	sprintf ( cverboteneFelder,"" ) ;

	kun.mdn = get_retk_mdn() ;
	kun.kun = get_retk_kun() ;

	if ( (! strcmp( tmyform_nr, "54101" ))|| get_retk_kun_fil())
	{
		fil.mdn = kun.mdn ;
		fil.fil = (short) kun.kun ;
		if ( fil.fil == fil_save.fil )
		{
			memcpy ( &fil, &fil_save, sizeof(struct FIL)); ;
		}
		else
		{
			di = Fil.openfil() ;
			di = Fil.lesefil (0) ;
			if ( di )	return ;	// Filialstamm-Fehler
			memcpy ( &fil_save,&fil, sizeof(struct FIL)); ;
		}
		if ( ! komplettierefil ())
		{
		//160113
			verboteneFelder ++ ;	
			sprintf ( cverboteneFelder,"Tabelle fil mit verbotenen Feldern designet");
			MessageBox(NULL, "Tabelle fil mit verbotenen Feldern designet", "drllret.exe", MB_OK|MB_ICONSTOP);
		}
	}
	else
	{
		if ( kun.kun == kun_save.kun )
		{
			memcpy ( &kun, &kun_save, sizeof(struct KUN)); ;
		}
		else
		{
			di = Kun.openkun() ;
			di = Kun.lesekun (0) ;
			if ( di )	return ;	// Kundenstamm-Fehler
			memcpy ( &kun_save,&kun, sizeof(struct KUN)); ;
		}
		if ( ! komplettierekun () )
		{
		//160113
			verboteneFelder ++ ;	
			sprintf ( cverboteneFelder,"Tabelle kun mit verbotenen Feldern designet");
			MessageBox(NULL, "Tabelle kun mit verbotenen Feldern designet", "drllret.exe", MB_OK|MB_ICONSTOP);
		}

	

		a_kun.mdn = kun.mdn ;
		a_kun.a = a_bas_a ();				// get_lsp_a() ;
		a_kun.kun = kun.kun ;
		sprintf ( a_kun.kun_bran2 , "0" );
		di = A_Kun.opena_kun() ;
		if ( A_Kun.lesea_kun(0))
		{
			sprintf ( a_kun.kun_bran2 , kun.kun_bran2 );
			a_kun.kun = 0L ;
			di = A_Kun.opena_kun() ;
			di = A_Kun.lesea_kun(0) ;
		}
//	230506 	if ( !di ) komplettierea_kun() ;
		if ( ! komplettierea_kun( di) )
		{
		//160113
			verboteneFelder ++ ;	
			sprintf ( cverboteneFelder,"Tabelle a_kun mit verbotenen Feldern designet");
			MessageBox(NULL, "Tabelle a_kun mit verbotenen Feldern designet", "drllret.exe", MB_OK|MB_ICONSTOP);
		}

	}
}

int TEMPTAB_CLASS::lesetemptab (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;   
	  }

 		inittemptab ();	// 310506 : reaktiviert
		int di = dbClass.sqlfetch (readcursor);
		if ( di )
			reloadtemptab () ;	// 310506

		if ( !di )
			komplettiereSatz();
		return di;
}

int TEMPTAB_CLASS::opentemptab (void)
{

	kun.kun = 0;
	a_kun.a = 0.0L ;
	a_zus_txt.a = 0.0L ;

	if ( readcursor < 0 )
	{
	}
	else
	{
		dbClass.sqlclose(readcursor) ;
		if ( del_cursor > -1 )	dbClass.sqlclose(del_cursor) ;
	}
		prepare () ;
		if ( v_abbruch ) return ( -3) ;

		if ( del_cursor > -1 )
				dbClass.sqlopen(del_cursor) ;

// 310506 : wieder weg ...		inittemptab ();	// 220506 : nur einmalig

         return dbClass.sqlopen (readcursor);
}


// Schreiben der Werte like rangedialog
void schreiberangesausdatei(void)
{
int retco ;
int l = 0 ;
char token[125],wert1[125],wert2[125] ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( ! tt_range[i]) continue ;

		sprintf ( token, "%s",tt_feldnam[i]);
		int j = holerangesausdatei( token, wert1,  wert2);

		l ++ ;
		tt_range[i] = l ;

		retco = macherange( i, wert1, wert2 ) ;

	}
}

// hier komplette Struktur UND Cursor neu aufbauen
void TEMPTAB_CLASS::prepare (void)
{

int i,j,k ;
int hauptrunde ;
char statementpuffer[4101];	// informix kann wohl im Standard 4096 
char tabellenpuffer[1025];	// sollte fuer > 50 Tabellennamen reichen
char grupptab[20] ;	
char wherefeld[15] ;
char whereklausel[1005] ;
char groupklausel[1005] ;
char orderklausel[1005] ;
char klausel[1005] ;

// Alles nochmals fuer Nebencursor fuer Definition der Variablen

char nstatementpuffer[4101];	// informix kann wohl im Standard 4096 
char ntabellenpuffer[1025];	// sollte fuer > 50 Tabellennamen reichen

extern ITEM_CLASS Item ;
Item.openitem();

// falls Feldueberlauf kann es schon mal knallen ..

for (i = 0 ; i < MAXANZ ; i++)	// 191006 
{
	tt_sqlval[i]	= ( char * )0 ;
	savtt_sqlval[i] = ( char * )0 ;
}

hauptrunde = HAUPT ;	// z.T. bissel redundant, einfach 2 Runden und dadurch 2 Cursoren 
 while ( TRUE )
 {

	i=j=k= 0 ;


	// memset ( statementpuffer, '\0', size_t(statementpuffer));

	sprintf ( wherefeld, " where ");	// Anfangsinit
	sprintf ( statementpuffer, " select ");
	sprintf ( nstatementpuffer, " select ");

	tabellenpuffer [0] = '\0' ;
	ntabellenpuffer [0] = '\0' ;
	if (  tt_hauneb[0] == HAUPT )
	{
		if ( ! tt_tabout[0] )
			sprintf ( tabellenpuffer, "%s", clipped ( tt_tabnam[0] ));
		else
			sprintf ( tabellenpuffer, " outer %s", clipped ( tt_tabnam[0] ));

	}
	else
	{
		if ( ! tt_tabout[0] )
			sprintf ( ntabellenpuffer, "%s", clipped ( tt_tabnam[0] ));
		else
//			sprintf ( ntabellenpuffer, " outer %s", clipped ( tt_tabnam[0] ));
// OUTER ist hier NICHT RELEVANT
			sprintf ( ntabellenpuffer, " %s", clipped ( tt_tabnam[0] ));

	}
	sprintf ( grupptab, "%s", clipped ( tt_tabnam[0] ));

	whereklausel[0] = '\0' ;
	groupklausel[0] = '\0' ;
	orderklausel[0] = '\0' ;


	sprintf ( form_w.form_nr,"%s" , nform_feld.form_nr );
	sprintf ( form_o.form_nr,"%s" , nform_feld.form_nr );

	form_w.lila = GlobalLila ;
	form_o.lila = GlobalLila ;

	FORM_O_CLASS Form_o_class ;
	Form_o_class.openform_o ();

	FORM_W_CLASS Form_w_class ;
	Form_w_class.openform_w ();


	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;


		if (  tt_hauneb[i] == HAUPT )
		{

			j = (int) strlen( statementpuffer ) ;
			if ( j < 10 )	// erstes element
				sprintf ( statementpuffer + j, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);
			else
				sprintf ( statementpuffer + j, ",%s.%s", tt_tabnam[i], tt_feldnam[i]);
		}
		else
		{
			j = (int) strlen( nstatementpuffer ) ;
			if ( j < 10 )	// erstes element
				sprintf ( nstatementpuffer + j, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);
			else
				sprintf ( nstatementpuffer + j, ",%s.%s", tt_tabnam[i], tt_feldnam[i]);
		}

// In der Hauptrunde Hauptcursor zuweisen, in der Nebenrunde Nebencursor zuweisen ...
		if (  (tt_hauneb[i] == HAUPT && hauptrunde == HAUPT)  
			|| (tt_hauneb[i] == NEBEN && hauptrunde == NEBEN ))
		{

			switch ( tt_datetyp[i] )
			{
				case(iDBCHAR):
					if (tt_sqlval[i] != (char *) 0)
					{
						free ( tt_sqlval[i]) ;
						tt_sqlval[i] = (char *) 0;
					}
					if (tt_sqlval[i] == (char *) 0) tt_sqlval[i] = (char *) malloc (tt_datelen[i]+2);
// 310506 
					if (savtt_sqlval[i] != (char *) 0)
					{
						free ( savtt_sqlval[i]) ;
						savtt_sqlval[i] = (char *) 0;
					}
					if (savtt_sqlval[i] == (char *) 0) savtt_sqlval[i] = (char *) malloc (tt_datelen[i]+2);
					sprintf ( (char*) tt_sqlval[i] ,"" ) ; sprintf ( (char*) savtt_sqlval[i],"") ;	// 191006
					dbClass.sqlout ((char *)  tt_sqlval[i], SQLCHAR , tt_datelen[i]+1 );
					break ;
				case(iDBSMALLINT):	// smallint
//					dbClass.sqlout ((short *)  &tt_sqllong[i], SQLSHORT, 0);
					dbClass.sqlout ((long *)  &tt_sqllong[i], SQLLONG, 0);
					break ;
				case(iDBINTEGER):	// integer
					dbClass.sqlout ((long *)  &tt_sqllong[i], SQLLONG, 0);
					break ;
				case(iDBDECIMAL):	// decimal
					dbClass.sqlout ((double *)  &tt_sqldouble[i], SQLDOUBLE, 0);
					break ;
				case(iDBDATUM):	// Datumsfeld
					if (tt_sqlval[i] != (char *) 0)
					{
						free ( tt_sqlval[i]) ;
						tt_sqlval[i] = (char *) 0;
					}
					if (tt_sqlval[i] == (char *) 0) tt_sqlval[i] = (char *) malloc (12);
// 310506 
					if (savtt_sqlval[i] != (char *) 0)
					{
						free ( savtt_sqlval[i]) ;
						savtt_sqlval[i] = (char *) 0;
					}
					if (savtt_sqlval[i] == (char *) 0) savtt_sqlval[i] = (char *) malloc (12);
					sprintf ( (char*) tt_sqlval[i] ,"" ) ; sprintf ( (char*) savtt_sqlval[i],"") ;	// 191006
					dbClass.sqlout ((char *)  tt_sqlval[i], SQLCHAR , 11 );
					break ;
				default :
					break ;

			} ;	// switch
		}
		if ( tt_range[i] )	k++ ;	// Darf nur im Hauptcursor sein ....

		if (( ! strncmp ( grupptab , tt_tabnam[i], strlen( grupptab)))
				&& strlen ( grupptab)== strlen(tt_tabnam[i] ))
		{
		}
		else
		{
			if ( tt_hauneb[i] == HAUPT )
			{
				j = (int) strlen( tabellenpuffer );
				if ( j > 1 )	// Folge oder erst-Eintrag
				{
					if ( ! tt_tabout[i] )
						sprintf ( tabellenpuffer + j,",%s" , tt_tabnam[i]);
					else
						sprintf ( tabellenpuffer + j,", outer %s" , tt_tabnam[i]);
				}
				else
				{
					if ( ! tt_tabout[i] )
						sprintf ( tabellenpuffer + j,"%s" , tt_tabnam[i]);
					else
						sprintf ( tabellenpuffer + j," outer %s" , tt_tabnam[i]);
				}
			}
			else
			{
				j = (int) strlen( ntabellenpuffer );
				if ( j > 1 )	// Folge oder erst-Eintrag
				{
					if ( ! tt_tabout[i] )
						sprintf ( ntabellenpuffer + j,",%s" , tt_tabnam[i]);
					else
	//					sprintf ( ntabellenpuffer + j,", outer %s" , tt_tabnam[i]);
	// OUTER HIER NICHT RELEVANT
						sprintf ( ntabellenpuffer + j,", %s" , tt_tabnam[i]);
				}
				else
				{
					if ( ! tt_tabout[i] )
						sprintf ( ntabellenpuffer + j,"%s" , tt_tabnam[i]);
					else
						sprintf ( ntabellenpuffer + j," outer %s" , tt_tabnam[i]);
				}
			}
			sprintf ( grupptab , "%s" , tt_tabnam[i]);

		}
		i ++ ;
	}		// while	

	if ( i )
	{

		if ( k  && ( hauptrunde == HAUPT ) )
		{
			Crange rangedial;

			if (! datenausdatei )
			{
				rangedial.DoModal();

				v_abbruch = rangedial.getabbruch();
				v_anzahl = rangedial.getanzahl();
			}
			else
			{
				v_abbruch = 0 ;
				schreiberangesausdatei() ;
			}

			for ( k= 0 ; tt_tabnam[k][0] != '\0' ;k ++ )
			{
				if ( ! tt_range[k] )
					continue ;
				if (  tt_hauneb[k] == NEBEN )
					continue ;	// Kann nur fehlerhaftes Design des Formulars sein

				j = (int) strlen( whereklausel ) ;

				switch ( tt_datetyp[k] )
				{
				case(iDBCHAR):
// nothing, nur 1 String, 2 Verschiedene Strings
					if (( tt_cvrange[tt_range[k]][0] == -1 ) && 
						( tt_cbrange[tt_range[k]][0] == -1 ) )
						break ;
					else
					{
						if ( tt_cbrange[tt_range[k]][0] == -1 )
						{
							dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,
							(int)strlen ( clipped(tt_cvrange[tt_range[k]]))+1 );
							sprintf ( whereklausel + j, " %s %s.%s  matches ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
							sprintf ( wherefeld, " and") ;
							break ;
						}
// default-Zweig
						dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,
							(int)strlen ( clipped(tt_cvrange[tt_range[k]]))+1 );
						dbClass.sqlin ((char *)  tt_cbrange[tt_range[k]], SQLCHAR ,
							(int)strlen ( clipped(tt_cbrange[tt_range[k]]))+1 );
						sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
				
						break ;
					}
					break ;				
				case(iDBSMALLINT):	// smallint
					if (( tt_lvrange[tt_range[k]] ==   11 ) && 
						( tt_lbrange[tt_range[k]] ==  -11 ) )	// keine Eingabe
						break ;
					else
					{
						if ( tt_lvrange[tt_range[k]] ==   tt_lbrange[tt_range[k]])
						{
							dbClass.sqlin ((short *) &tt_lvrange[tt_range[k]], SQLSHORT ,0);
							sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
							sprintf ( wherefeld, " and") ;
							break ;
						}
// default-Zweig
						dbClass.sqlin ((short *) &tt_lvrange[tt_range[k]], SQLSHORT, 0);
						dbClass.sqlin ((short *) &tt_lbrange[tt_range[k]], SQLSHORT, 0);
						sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
					break ;

				case(iDBINTEGER):	// integer
					if (( tt_lvrange[tt_range[k]] ==   11 ) && 
					( tt_lbrange[tt_range[k]] ==  -11 ) )	// keine Eingabe
						break ;
					else
					{
						if ( tt_lvrange[tt_range[k]] ==   tt_lbrange[tt_range[k]])
						{
							dbClass.sqlin ((long *) &tt_lvrange[tt_range[k]], SQLLONG ,0);
							sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
							sprintf ( wherefeld, " and") ;
							break ;
						}
// default-Zweig
						dbClass.sqlin ((long *) &tt_lvrange[tt_range[k]], SQLLONG, 0);
						dbClass.sqlin ((long *) &tt_lbrange[tt_range[k]], SQLLONG, 0);
						sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
					break ;

				case(iDBDECIMAL):	// decimal
					if (( tt_dvrange[tt_range[k]] ==   11 ) && 
						( tt_dbrange[tt_range[k]] ==  -11 ) )	// keine Eingabe
						break ;
					else
					{
						if ( tt_dvrange[tt_range[k]] ==  tt_dbrange[tt_range[k]])
						{
							dbClass.sqlin ((double *)  &tt_dvrange[tt_range[k]], SQLDOUBLE ,0);
							sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
							sprintf ( wherefeld, " and") ;
							break ;
						}
// default-Zweig
						dbClass.sqlin ((double *)  &tt_dvrange[tt_range[k]], SQLDOUBLE, 0);
						dbClass.sqlin ((double *)  &tt_dbrange[tt_range[k]], SQLDOUBLE, 0);
						sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
					break ;
				case(iDBDATUM):	// Datumsfeld
// nothing, nur 1 String, 2 Verschiedene Strings
					if (( tt_cvrange[tt_range[k]][0] == -1 ) && 
						( tt_cbrange[tt_range[k]][0] == -1 ) )
						break ;
					else
					{
						if ( tt_cbrange[tt_range[k]][0] == -1 )
						{
							dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,11);
							sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
							sprintf ( wherefeld, " and") ;
							break ;
						}
// default-Zweig
						dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,11);
						dbClass.sqlin ((char *)  tt_cbrange[tt_range[k]], SQLCHAR ,11);
						sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
				
						break ;
					}
					break ;				

				default :
					break ;

				}		// 	switch ( tt_datetyp[k] ) 
			}			// 	for ( k= 0 ; tt_tabnam[k][0] != '\0' ;k ++ )
		}				// if ( k  &&( hauptrunde == HAUPT )) -> das war ein range


// ##### WHERE - KLAUSEL

		sprintf ( klausel, " " );

		if (! Form_w_class.leseform_w())
		{
			sprintf ( klausel , "%s", clipped (form_w.krz_txt)) ;
			while (!Form_w_class.leseform_w())
			{
				j = (int)strlen ( clipped( klausel)) ;
				sprintf ( klausel + j , "%s" , clipped ( form_w.krz_txt));
			}
		}


		if ( wherefeld[1] == 'w' )	// noch nix reingeschrieben ausser " where "
		{
			if ( strlen ( clipped ( klausel)) > 2 )
				sprintf ( whereklausel , " where %s", clipped (klausel) );
			else
				sprintf ( whereklausel , " " );
		}
		else
		{
			if ( strlen ( clipped ( klausel)) > 2 )
			{
				j = (int)strlen ( clipped (whereklausel ));
				sprintf ( whereklausel + j , " and %s", clipped (klausel));
			}

		}


// ##### ORDER - KLAUSEL

		sprintf ( klausel, " " );

		if (! Form_o_class.leseform_o())
		{
			sprintf ( klausel , "%s", clipped (form_o.krz_txt)) ;
			while (!Form_o_class.leseform_o())
			{
				j = (int)strlen ( clipped( klausel)) ;
				sprintf ( klausel + j , "%s" , clipped ( form_o.krz_txt));
			}
		}

		if ( strlen ( clipped ( klausel)) > 1 )
			sprintf ( orderklausel , " order by %s", clipped (klausel) );
		else
			sprintf ( orderklausel , " " );
	

		j = (int) strlen ( statementpuffer ) ;
		sprintf ( statementpuffer + j, " from %s", tabellenpuffer );

		j = (int) strlen ( statementpuffer ) ;
		sprintf ( statementpuffer + j, " %s %s %s"
				,clipped ( whereklausel )
				,clipped ( groupklausel )
				,clipped ( orderklausel ) );
	}	// if i ( daher nur, wenn was da ist ....

	if ( hauptrunde == HAUPT )
	{
	    readcursor = dbClass.sqlcursor ( statementpuffer );
	}
	if ( i && ( hauptrunde == NEBEN ))
	{
		j = (int) strlen ( nstatementpuffer ) ;
		if ( j )
		{
			sprintf ( nstatementpuffer + j, " from %s", ntabellenpuffer );
			del_cursor = dbClass.sqlcursor ( nstatementpuffer );
			//del_cursor zweckentfremdet nutzen ..... 
										
		}
	}
	if ( hauptrunde == NEBEN ) break ; // fertig
	if ( hauptrunde == HAUPT ) hauptrunde = NEBEN ;	// jetzt kommt die Nebenrunde
 }		// hauptrunden-while
}

short TEMPTAB_CLASS::kun_leer_kz(void)
{
	if ( ! strcmp( tmyform_nr , "54101")) return 1 ;	// Filiale immer kleine Tabelle 
	return kun.kun_leer_kz ;
}

void TEMPTAB_CLASS::drfree (void) 
{

	for (int i = 0 ; i < MAXANZ; i++ )
	{
		if (tt_sqlval[i] != (char *) 0)
		{
			free ( tt_sqlval[i]) ;
					tt_sqlval[i] = (char *) 0;
		};
// 310506 
		if (savtt_sqlval[i] != (char *) 0)
		{
			free ( savtt_sqlval[i]) ;
					savtt_sqlval[i] = (char *) 0;
		}
	}
}