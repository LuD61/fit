//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//

// drllret : Druckausgabe fuer RET : angelehnt an dr70001, ist 
// letztendlich jedoch ein Vorselektionstool
// Es wird eigentlich nur ret-nr und mandant ausgwertet,
// restliche Tabellen aus dr89000 dienen i.w. nur Layout-Gestaltung
// bzw. als Parameter fuer irgendwas


//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
// Diese Applikation soll irgendwann die Listen ausgeben ....
// Erstellung : 24.10.2004  GrJ

//=============================================================================

// mainfrm.cpp : implementation of the CMainFrame class

#include "stdafx.h"
#include "llmfc.h"
#include "mainfrm.h"
// #include "wmask.h"
#include "DbClass.h"
#include "temptab.h"
#include "adr.h"
#include "txt.h"
#include "ptabn.h"
// #include "strfkt.h"

#include "systables.h"
#include "syscolumns.h"
#include "zustab.h"
#include "kun.h"		// 160305

#include "sys/timeb.h"	// 120210

#include "Token.h"

#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GR: Waehlen Sie hier die List & Label Sprache f�r den Designer und die
//     Dialoge aus :
//    (Die entsprechenden Sprachkonstanten entnehmen Sie der Datei cmbtl9.h)

// US: choose your LuL-Language for all designer, dialogs, etc... here:
//    (see cmbtl9.h for other language-constants)


extern int verboteneFelder ;	//160113
extern char cverboteneFelder[120] ;	// 160113


short GlobalLila ;

const int LUL_LANGUAGE = CMBTLANG_GERMAN;
const int CTEXTMAX = 512;
const int LG_MAX = 10 ;


// irgendein dummy halt 
const char *FILENAME_DEFAULT = "c:\\user\\fit\\format\\LL\\54100.lst und immer laenger wird der string ........";

bool cfgOK;
char Listenname[512];
char szTempt[CTEXTMAX + 5];

char User[128] ;
char LUser[128] ;		// 220207

int danzahl ;		   
char myform_nr[99] ;	// eigentlich max 10 Zeichen + Zeichenkettenende
int Listenauswahl = 0;   // wenn 1 : Auswahl mehrerer Reports
int Hauptmenue = 0;   // wenn 1 : Auswahl edit oder print �ber Menue
int DrMitMenue = 1;   // wenn 1 : Ausdruck mit diversen Dialogen,sollte der Standard sein 
int NutzerIgnorieren = 0;	// 090109 :wenn 1 , dann kein fehler bei fehlendem sub-Dir	
int danzahlerlaubt = 0 ;	// 031109 : Falls auf dem Platz anzahlerlaubt, dann Anzahl korrekt auswerten
int EntwurfsModus = 0;   // wenn 1 : Entwurfsmodus: Aufruf ohne Parameter
int WahlModus = 0 ;   // wenn 1 : Druckerwahl-Dialog als getrennte Aktion
int dvariante = 0 ;	// 110309 : variante ( z.B. Teil-LS ...... )

int da_zustxt_wa = 0 ;	// Systemparameter
int dnachkpreis  = 0 ;	// Systemparameter
int dwa_pos_txt  = 0 ;	// Systemparameter

int dkun_leer    = 0 ;	// Systemparameter	160305
// 160305
int leerg_st[10] ;
int leerg_zu[10] ;
int leerg_ab[10] ;
double leerg_a[10] ;
double leerg_p[10] ;
char leerg_bz[10][25] ;

int datenausdatei = 0;	// entscheidung, ob Ranges per Dialog
						// oder aus einer Parameterdatei kommen sollen
static char datenausdateiname[513] ;	// Dateiname ......

char *wort[100];	// mehr als 100 Worte braucht es nicht ?!

int zeittest = 0 ;	// ACHTUNG !!! TESTMODUS

// 300306 A
static int  alternat_ausgabe = 0 ;

static char alternat_type[ 50] ;
static char alternat_file[150] ;
static char alternat_dir [150] ;
// 300306 E




DB_CLASS dbClass;

ADR_CLASS Adr;
PTXT_CLASS Ptxt;
NTXT_CLASS Ntxt;	// 110205
NFORM_TAB_CLASS Nform_tab;
NFORM_FELD_CLASS Nform_feld;
SYSCOLUMNS_CLASS Syscolumns;
SYSTABLES_CLASS Systables;
TEMPTAB_CLASS Temptab;
PTABN_CLASS Ptabn;
SYS_PAR_CLASS Sys_par;
A_ZUS_TXT_CLASS A_zus_txt;	
LEER_LSDR_CLASS Leer_lsdr;	// 160305	
A_BAS_CLASS A_bas;			// 160305	

HWND hMainWindow;

// int form_feld_curs, tabid_curs, coltyp_curs,  dynacurs ; 

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

// GR: Registrieren eines Nachrichtenkanals f�r List & Label Callbacks
// US: register LuL MessageBase:
UINT CMainFrame::m_uLuLMessageBase = RegisterWindowMessage("cmbtLLMessage");

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_START_DEBUG, OnStartDebug)
	ON_COMMAND(ID_FILE_STOP_DEBUG, OnStopDebug)
	ON_COMMAND(ID_EDIT_LABEL, OnEditLabel)
	ON_COMMAND(ID_EDIT_LIST, OnEditList)
	ON_UPDATE_COMMAND_UI(ID_FILE_START_DEBUG, OnUpdateStartDebug)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP_DEBUG, OnUpdateStopDebug)
	ON_COMMAND(ID_PRINT_LABEL, OnPrintLabel)
	ON_COMMAND(ID_PRINT_REPORT, OnPrintReport)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(m_uLuLMessageBase,OnLulMessage)
END_MESSAGE_MAP()


void schreibezeit ( char * text )
{
FILE *fperr ;
char writepuffer[199] ;
char erfnam [199] ;
char timestr[100] ;

struct _timeb tstruct;


    _ftime( &tstruct ); 
	sprintf (writepuffer , "%s.%u : %s\n" , _strtime(timestr), tstruct.millitm, text ) ; 

	sprintf ( erfnam , "%s\\timelog.lls", getenv("TMPPATH") ) ;
	if ( ( fperr = fopen(erfnam, "at" )) == NULL ) return ;   // APPEND_TEXT 
	fprintf ( fperr , writepuffer ) ;
	fclose ( fperr ) ;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{


	m_bDebug = FALSE;

}

CMainFrame::~CMainFrame()
{
/* ---->
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
< ------ */

}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	/* ----> CELL11 weg damit .....
	LlAddCtlSupport(m_hWnd,
					LL_CTL_ADDTOSYSMENU | LL_CTL_CONVERTCONTROLS | LL_CTL_ALSOCHILDREN,
					"combit.ini");
<----- */


	if ( zeittest ) schreibezeit("Start \n" ) ;

	GetCfgValues();

	InterpretCommandLine();

	if ( zeittest ) schreibezeit("Nach Commandline \n" ) ;

	if ( EntwurfsModus)
	{   DrMitMenue = 1;
		Hauptmenue = 1 ;
		Listenauswahl = 1 ;
	}

	if (Hauptmenue == 0)
	{
		PostMessage(WM_COMMAND,ID_PRINT_REPORT,0l);
	}

return 0;
}

void CMainFrame::OnStartDebug()
{
	MessageBox("Make sure that DEBWIN2 had been started before this demo application. "
				"If this doesn't happen you won't see any debug outputs now!",
				"List & Label App", MB_OK | MB_ICONINFORMATION);
//	LlSetDebug(LL_DEBUG_CMBTLL);
	m_bDebug = TRUE;

}

void CMainFrame::OnStopDebug()
{
	LlSetDebug(FALSE);
	m_bDebug = FALSE;
}

void CMainFrame::OnUpdateStopDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bDebug);
}

void CMainFrame::OnUpdateStartDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bDebug);
}

//=============================================================================
void CMainFrame::OnEditLabel()
//=============================================================================
{
	/* ---->
	CHAR szFilename[128+1] = "*.lbl";
	HWND hWnd = m_hWnd;
	HJOB hJob;
	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	// US: initialize Job
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}
// OnEditLabel
	// GR: Setzen der List & Label Optionen
	// US: Setting the List & Label options
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	//GR: Exporter aktivieren
	//US: Enable exporter
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
** ---> GrJ GrJ
	if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
    {
        LlJobClose(hJob);
        return;
    }
< ---- **
	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);
// OnEditLabel
	// GR: Aufruf des Designers
   	sprintf ( szFilename, FILENAME_DEFAULT );
 	if (LlDefineLayout(hJob, hWnd, "Designer", LL_PROJECT_LABEL, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
// OnEditLabel
< ----- */
}

int GetRecCount()  
{
//	return RepoRech.dbcount ();
	return 1 ;
}

// 110506
void ptabschreiben ( HJOB hJob, char * varfeld , int imodus , char * tab , char * feld ) 
{
	char hilfsfeld [256] ;
	char hilfsfeld2[256] ;
	int schleife ;
	schleife = 0 ;

	while ( schleife  < 5 )
	{
// ptbezk
		if ( schleife == 0 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptbezk", tab, feld );
			if  ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptbezk",  feld );
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptbezk ) ;
		};
// ptbez
		if ( schleife == 1 ) 
		{
			sprintf ( hilfsfeld, "%s.%s.ptbez",tab, feld);
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptbez", feld );
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptbez ) ;
		}
// ptwert
		if ( schleife == 2 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwert",tab, feld );
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptwert", feld);
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptwert ) ;
		}

// ptwer1
		if ( schleife == 3 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwer1", tab, feld ) ;
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptwer1", feld ) ;
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptwer1 ) ;
		}

// ptwer2
		if ( schleife == 4 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwer2", tab, feld ) ;
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptwer2", feld ) ;
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptwer2 ) ;
		}

		if ( varfeld[0] == 'V' )
		{	
			if ( !imodus )
				LlDefineVariableExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
			else
				if ( LlPrintIsVariableUsed(hJob, hilfsfeld))
					LlDefineVariableExt(hJob, clipped( hilfsfeld),
								clipped(hilfsfeld2), LL_TEXT, NULL);
		}
		else
		{
			if ( !imodus )
				LlDefineFieldExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
			else
				if ( LlPrintIsFieldUsed(hJob, hilfsfeld))
					LlDefineFieldExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
		}
		schleife ++ ;
	} ;	// end while 

}



void Variablendefinition (HJOB hJob)
{
	char hilfsfeld[256] ;
	char hilfsfeld2[128] ;


	sprintf ( systables.tabname, " " );	// initialisieren
	systables.tabid = 0 ;				// initialisieren


	sprintf (nform_tab.form_nr ,"%s", myform_nr );
	nform_tab.lila = GlobalLila ;

	Nform_tab.opennformtab ();
	while (!Nform_tab.lesenformtab())	// gefunden
	{

		sprintf (nform_feld.form_nr ,"%s", myform_nr );
		sprintf (nform_feld.tab_nam ,"%s", nform_tab.tab_nam );
		nform_feld.lila = GlobalLila ;

		Nform_feld.opennform_feld ();
		while (!Nform_feld.lesenform_feld())	// gefunden
		{


			switch ( nform_feld.feld_typ )
			{
				case 1 : // ptabn

					ptabschreiben ( hJob, "V" , 0 ,
						clipped (nform_feld.tab_nam), clipped( nform_feld.feld_nam)) ;

/* ----> auslagern 110506					
					//					ptbezk
					sprintf ( hilfsfeld, "%s.%s.ptbezk", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptbezk", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptbez
					sprintf ( hilfsfeld, "%s.%s.ptbez", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptbez", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwert
					sprintf ( hilfsfeld, "%s.%s.ptwert", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwert", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwer1
					sprintf ( hilfsfeld, "%s.%s.ptwer1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwer1", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwer2
					sprintf ( hilfsfeld, "%s.%s.ptwer2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwer2", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
< ------ */
			break ;

				case 2 : // Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-Textbaustein", clipped ( nform_feld.krz_txt));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					break ;

				case 3 : // adresse
//					anr
					sprintf ( hilfsfeld, "%s.%s.anr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.anr", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_krz
					sprintf ( hilfsfeld, "%s.%s.adr_krz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_krz", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam1
					sprintf ( hilfsfeld, "%s.%s.adr_nam1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam1", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam2
					sprintf ( hilfsfeld, "%s.%s.adr_nam2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam2", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_nam3	090905
					sprintf ( hilfsfeld, "%s.%s.adr_nam3", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam3", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					ort1
					sprintf ( hilfsfeld, "%s.%s.ort1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort1", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					plz
					sprintf ( hilfsfeld, "%s.%s.plz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					postfach	080709
					sprintf ( hilfsfeld, "%s.%s.plz_pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz_pf", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					sprintf ( hilfsfeld, "%s.%s.pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.pf", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					str
					sprintf ( hilfsfeld, "%s.%s.str", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.str", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					iln
					sprintf ( hilfsfeld, "%s.%s.iln", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iln", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr
					sprintf ( hilfsfeld, "%s.%s.adr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"08150", LL_NUMERIC, NULL);

// 310813 A : partner, ort2,tel,fax,mobil,iban,swift dazu

//					ort2
					sprintf ( hilfsfeld, "%s.%s.ort2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort2", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					partner
					sprintf ( hilfsfeld, "%s.%s.partner", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.partner", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					tel
					sprintf ( hilfsfeld, "%s.%s.tel", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.tel", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					fax
					sprintf ( hilfsfeld, "%s.%s.fax", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.fax", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					mobil
					sprintf ( hilfsfeld, "%s.%s.mobil", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.mobil", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					iban
					sprintf ( hilfsfeld, "%s.%s.iban", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iban", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					swift
					sprintf ( hilfsfeld, "%s.%s.swift", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.swift", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 310813 E : partner, ort2,tel,fax,mobil,iban,swift dazu

// 110506 : alles neu dazu 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					ptabschreiben ( hJob, "V" , 0 , hilfsfeld , "staat" ) ;

					break ;


				default :

					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

					sprintf ( hilfsfeld2, "%s", clipped ( nform_feld.feld_nam) );

					if ( strcmp ( clipped ( systables.tabname)  , clipped ( nform_feld.tab_nam)))
					{
						sprintf ( systables.tabname , "%s" , clipped( nform_feld.tab_nam));
						systables.tabid = 0 ;
						Systables.opensystables ();
						Systables.lesesystables();

					}
					if ( !systables.tabid )	// Error
					{
						syscolumns.coltype  = iDBCHAR ;	// Notbremse
					}
					else
					{
						sprintf ( syscolumns.colname , "%s" , clipped( nform_feld.feld_nam));  
						syscolumns.coltype = iDBCHAR ;
						syscolumns.tabid = systables.tabid ;
						Syscolumns.opensyscolumns ();
						Syscolumns.lesesyscolumns();
					}
					if ( syscolumns.coltype == iDBCHAR || syscolumns.coltype == iDBDATUM )
					{
// 200105 : Datum-Default sinnvoll belegen 
						if ( syscolumns.coltype == iDBDATUM )
							sprintf ( hilfsfeld2, "08.08.2007");


						LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					}
					else
					{
						LlDefineVariableExt(hJob, clipped (hilfsfeld) ,
								"1.0000", LL_NUMERIC, NULL);
					}

			}
		}
	}
//			nachkpreis
	LlDefineVariableExt(hJob, "rangt.nachkpreis" , "2", LL_NUMERIC, NULL);
// 110309
	LlDefineVariableExt(hJob, "rangt.variante" , "0", LL_NUMERIC, NULL);
// 310812
	LlDefineVariableExt(hJob, "rangt.Heute" , "01.01.2012", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rangt.Zeit" , "11:55:00", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rangt.Nutzer" , "Nu-Name", LL_TEXT, NULL);


}


void Felderdefinition (HJOB hJob)
{

	char hilfsfeld[256] ;
	char hilfsfeld2[128] ;

	sprintf ( systables.tabname, " " );	// initialisieren
	systables.tabid = 0 ;				// initialisieren


	sprintf (nform_tab.form_nr ,"%s", myform_nr );
	nform_tab.lila = GlobalLila ;

	Nform_tab.opennformtab ();
	while (!Nform_tab.lesenformtab())	// gefunden
	{

		sprintf (nform_feld.form_nr ,"%s", myform_nr );
		sprintf (nform_feld.tab_nam ,"%s", nform_tab.tab_nam );
		nform_feld.lila = GlobalLila ;


	
		Nform_feld.opennform_feld ();
		while (!Nform_feld.lesenform_feld())	// gefunden
		{

			switch ( nform_feld.feld_typ )
			{
				case 1 : // ptabn
					ptabschreiben ( hJob, "F" , 0 ,
					clipped (nform_feld.tab_nam), clipped( nform_feld.feld_nam) ) ;	

/* 110506 --------> alles auslagern
					//					ptbezk
					sprintf ( hilfsfeld, "%s.%s.ptbezk", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptbezk", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptbez
					sprintf ( hilfsfeld, "%s.%s.ptbez", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptbez", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwert
					sprintf ( hilfsfeld, "%s.%s.ptwert", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwert", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwer1
					sprintf ( hilfsfeld, "%s.%s.ptwer1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwer1", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwer2
					sprintf ( hilfsfeld, "%s.%s.ptwer2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwer2", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
< ------ */

					break ;

				case 2 : // Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-Textbaustein", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					break ;

				case 3 : // adresse
//					anr
					sprintf ( hilfsfeld, "%s.%s.anr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.anr", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_krz
					sprintf ( hilfsfeld, "%s.%s.adr_krz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_krz", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam1
					sprintf ( hilfsfeld, "%s.%s.adr_nam1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam1", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam2
					sprintf ( hilfsfeld, "%s.%s.adr_nam2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam2", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam3	090905
					sprintf ( hilfsfeld, "%s.%s.adr_nam3", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam3", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					ort1
					sprintf ( hilfsfeld, "%s.%s.ort1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort1", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					plz
					sprintf ( hilfsfeld, "%s.%s.plz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					postfach	080790
					sprintf ( hilfsfeld, "%s.%s.plz_pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz_pf", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					sprintf ( hilfsfeld, "%s.%s.pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.pf", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					str
					sprintf ( hilfsfeld, "%s.%s.str", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.str", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					iln
					sprintf ( hilfsfeld, "%s.%s.iln", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iln", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr
					sprintf ( hilfsfeld, "%s.%s.adr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								"08150", LL_TEXT, NULL);
// 310813 A : partner, ort2,tel,fax,mobil,iban,swift dazu
//					partner
					sprintf ( hilfsfeld, "%s.%s.partner", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.partner", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ort2
					sprintf ( hilfsfeld, "%s.%s.ort2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort2", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					tel
					sprintf ( hilfsfeld, "%s.%s.tel", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.tel", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					fax
					sprintf ( hilfsfeld, "%s.%s.fax", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.fax", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					mobil
					sprintf ( hilfsfeld, "%s.%s.mobil", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.mobil", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					iban
					sprintf ( hilfsfeld, "%s.%s.iban", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iban", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					swift
					sprintf ( hilfsfeld, "%s.%s.swift", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.swift", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

// 310813 E : partner, ort2,tel,fax,mobil,iban,swift dazu

// 110506 : alles neu dazu 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					ptabschreiben ( hJob, "F" , 0 , hilfsfeld , "staat" ) ;

					break ;


				default :
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

					sprintf ( hilfsfeld2, "%s", clipped ( nform_feld.feld_nam) );

					if ( strcmp ( clipped ( systables.tabname)  , clipped ( nform_feld.tab_nam)))
					{
						sprintf ( systables.tabname , "%s" , clipped( nform_feld.tab_nam));
						systables.tabid = 0 ;
						Systables.opensystables ();
						Systables.lesesystables();

					}
					if ( !systables.tabid )	// Error
					{
						syscolumns.coltype  = iDBCHAR ;	// Notbremse
					}
					else
					{
						sprintf ( syscolumns.colname , "%s" , clipped( nform_feld.feld_nam));  
						syscolumns.coltype = iDBCHAR ;
						syscolumns.tabid = systables.tabid ;
						Syscolumns.opensyscolumns ();
						Syscolumns.lesesyscolumns();
					}
					if ( syscolumns.coltype == iDBCHAR || syscolumns.coltype == iDBDATUM )
						LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					else
						LlDefineFieldExt(hJob, clipped (hilfsfeld) ,
								"1.0000", LL_NUMERIC, NULL);

			}
		}
	}
//			nachkpreis
	LlDefineFieldExt(hJob, "rangt.nachkpreis" ,
								"2", LL_NUMERIC, NULL);
// 110309
	LlDefineFieldExt(hJob, "rangt.variante" ,
								"0", LL_NUMERIC, NULL);
// 310812 
	LlDefineFieldExt(hJob, "rangt.Heute" , "01.01.2012", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rangt.Zeit" , "11:55:00", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rangt.Nutzer" , "Nu-Name", LL_TEXT, NULL);
}

/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sqldatamger
-
-       In              :       string-amerikanisch "YYYY-MM-DD"
-
-       Out             :       string-germanisch   "DD.MM.JJJJ"
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Umformatieren eines Datumsstrings
-
--------------------------------------------------------------------------------
**/
#ifdef OLD_DCL
char * sqldatamger(     inpstr,        outpstr)
char * inpstr;
char * outpstr;
#else
char * sqldatamger(char *inpstr, char *outpstr)
#endif
{
 
	/* ---> 270907 : so einfach ist es nicht mehr 
	outpstr[ 0]= inpstr[8];
	outpstr[ 1]= inpstr[9];
	outpstr[ 2]= '.';
	outpstr[ 3]= inpstr[5];
	outpstr[ 4]= inpstr[6];
	outpstr[ 5]= '.';
	outpstr[ 6]= inpstr[0];
	outpstr[ 7]= inpstr[1];
	outpstr[ 8]= inpstr[2];
	outpstr[ 9]= inpstr[3];
	outpstr[10]= '\0';
	return outpstr ;	
< ---- */
	if (( inpstr[0] == ' ') || ( inpstr[0] == '\0') ||(inpstr[1] == '\0' ))
	{	// Nullstring handeln ...
		sprintf ( outpstr, "  .  .    " ) ;
		return outpstr  ;
	}
	else
	{
		if ( inpstr[4] == '-' && inpstr[7] == '-' )	// passt fuer y4md- und y4dm-
		{											// wir nehmen halt jetzt mal y4md- an
			// default fit-informix
				outpstr[ 0]= inpstr[8];
				outpstr[ 1]= inpstr[9];
				outpstr[ 3]= inpstr[5];
				outpstr[ 4]= inpstr[6];
/* ---->
		else	// eher hypothetischer Fall : "y4dm-"
			{
				outpstr[ 0]= inpstr[5];
				outpstr[ 1]= inpstr[6];
				outpstr[ 3]= inpstr[8];
				outpstr[ 4]= inpstr[9];
			}
< ---- */
			outpstr[ 2]= '.';
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[0];
			outpstr[ 7]= inpstr[1];
			outpstr[ 8]= inpstr[2];
			outpstr[ 9]= inpstr[3];
			outpstr[10]= '\0';
			return outpstr ;
		} ;

		if ( inpstr[2] == '.' && inpstr[5] == '.' )	// passt fuer dmy4.
		{

			outpstr[ 0]= inpstr[0];
			outpstr[ 1]= inpstr[1];
			outpstr[ 2]= '.';
			outpstr[ 3]= inpstr[3];
			outpstr[ 4]= inpstr[4];
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[6];
			outpstr[ 7]= inpstr[7];
			outpstr[ 8]= inpstr[8];
			outpstr[ 9]= inpstr[9];
			outpstr[10]= '\0';
			return outpstr ;
		}	// "dmy4."

	// ab jetzt geht sowieso alles schief ....

		outpstr[ 0]= inpstr[8];
		outpstr[ 1]= inpstr[9];
		outpstr[ 2]= '.';
		outpstr[ 3]= inpstr[5];
		outpstr[ 4]= inpstr[6];
		outpstr[ 5]= '.';
		outpstr[ 6]= inpstr[0];
		outpstr[ 7]= inpstr[1];
		outpstr[ 8]= inpstr[2];
		outpstr[ 9]= inpstr[3];
		outpstr[10]= '\0';
		return outpstr ;

	}

}


/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sqldatgeram
-
-
-       In              :       string-germanisch   "DD.MM.JJJJ"
-       Out             :       string-amerikanisch "YYYY-MM-DD"
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Umformatieren eines Datumsstrings
-
--------------------------------------------------------------------------------
**/
#ifdef OLD_DCL
char * sqldatgeram(     inpstr,   outpstr)
char * inpstr;
char * outpstr;
#else
char * sqldatgeram(char *inpstr, char * outpstr)
#endif
{
 
	outpstr[ 0]= inpstr[6];
	outpstr[ 1]= inpstr[7];
	outpstr[ 2]= inpstr[8];
	outpstr[ 3]= inpstr[9];
	outpstr[ 4]= '-';
	outpstr[ 5]= inpstr[3];
	outpstr[ 6]= inpstr[4];	// little bug : Ziel 7 -> 6 ab 300904
	outpstr[ 7]= '-';
	outpstr[ 8]= inpstr[0];
	outpstr[ 9]= inpstr[1];
	outpstr[10]= '\0';
	return  outpstr ;
	
}


#ifdef OLD_DCL
char * datplusgerman( inpstr, outpstr, doffset)
char * inpstr;
char * outpstr;
long doffset;
#else
char * datplusgerman(char *inpstr, char * outpstr, long doffset)
#endif

{

 
	struct tm akttm;
	time_t dstart;

	char idd[3];
	char imm[3];
	char iyy[5];

	idd[0] = inpstr[0] ;
	idd[1] = inpstr[1] ;
	idd[2] = '\0' ;

	imm[0] = inpstr[3] ;
	imm[1] = inpstr[4] ;
	imm[2] = '\0' ;

	iyy[0] = inpstr[6] ;
	iyy[1] = inpstr[7] ;
	iyy[2] = inpstr[8] ;
	iyy[3] = inpstr[9] ;
	iyy[4] = '\0' ;
	
	
	time( &dstart );
	akttm = *localtime( &dstart );
 
	// in tm_year steckt dan so was wie "103" fuer 2003" usw.

	long hijahr = atol ( iyy );
	if ( hijahr < 60L) hijahr += 100L ;	//2 stelliges Jahr 2000 bis 2059
	if ( hijahr <= 60L && hijahr < 100L ) hijahr = hijahr ;	// 2 stelliges Jahr 1960 ..1999 syntax-dummy
	if ( hijahr > 1900L && hijahr < 2099L ) hijahr -= 1900L ;  // Der Rest fuehrt zu Fehlern ....
	akttm.tm_year = hijahr ; 
    akttm.tm_mday = atol ( idd ); 
    akttm.tm_mon = atol ( imm ) - 1L ; 
         
  if( (dstart = mktime( &akttm )) != (time_t)-1 )
  {

	dstart += ( 86400L * doffset ) ;
	akttm = *localtime( &dstart );

    sprintf ( outpstr, "%02.0d.%02.0d.%04.0d",
		akttm.tm_mday, akttm.tm_mon + 1,
		akttm.tm_year + 1900L) ; 
 
  
  }
  else	// mktime failed
  {
	  sprintf ( outpstr, "%s", inpstr );
  }
	return  outpstr ;
 
}

void textsetzen ( HJOB hJob, int iint, char * hilfsfeld ,  char * ctyp)
{

	// ctyp == "V" : Variablen-Textbaustein
	// ctyp == "f" : retp.retp_txt mit diversen Sonderbehandlungen
	// ctyp == "F" : irgendein Pos-TextBaustein

int k ;
char strpuf[257] ;	// 110205

	szTempt[0] = '\0' ;

	if ( ctyp[0] == 'f' )

// 081210 : macht LZF : ctyp[0] = 'F' ;		// wa_pos_txt_par bei retouren z.Z. ( ROSI-Welt)
											// nicht realisiert

	da_zustxt_wa = 0 ;	// 081210 : hart ausschalten 

	if ( ctyp[0] == 'f' )	// Nur fuer Feld retp.retp_txt sonderbehandlungen
	{
		if ( da_zustxt_wa )
		{
			a_zus_txt.a = Temptab.a_bas_a ();	// Temptab.get_retp_a() ;
			k = A_zus_txt.opena_zus_txt();
			while (! k )
			{
				k = A_zus_txt.lesea_zus_txt(0) ;
				if (! k )
				{
					if (strlen(szTempt))
					{
						sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
					}
					if (( strlen(szTempt)) + ( strlen(clipped(a_zus_txt.txt))) >= CTEXTMAX  )
						break ;
					sprintf(  szTempt + strlen(szTempt) , "%s", clipped(a_zus_txt.txt) );
				}
			}
		}
	};
	
	// Nur was tun, wenn used und vorhanden usw. -iDBDECIMAL eigentlich nur zum Test
	if  ( ( tt_datetyp[iint] == iDBSMALLINT || tt_datetyp[iint] == iDBINTEGER )
		       && tt_sqllong[iint] > 0L  && tt_referenz[iint] > 0L )
	{
		ptxt.nr = tt_sqllong[iint] ;
//		szTempt[0] = '\0' ;
// 110205 : gewisse Redundanz,da wa_pos_txt UND eintrag "lsp_txt" passen muessen
		if (( dwa_pos_txt ) && ( ctyp[0] == 'f' ))
		{

			ntxt.mdn = Temptab.get_retk_mdn() ;
			ntxt.fil = Temptab.get_retk_fil() ;
			ntxt.ls = Temptab.get_retk_ret() ;
			ntxt.posi = Temptab.get_retp_posi() ;

			k = Ntxt.openntxt(clipped ( tt_creferenz[ tt_referenz[iint]]));
		}
		else
			k = Ptxt.openptxt(clipped ( tt_creferenz[ tt_referenz[iint]]));

		while (! k )
		{
			if (( dwa_pos_txt ) && ( ctyp[0] == 'f' ))
				k = Ntxt.lesentxt() ;
			else
				k = Ptxt.leseptxt() ;

			if (! k )
			{
				if (( dwa_pos_txt ) && ( ctyp[0] == 'f' ))
					sprintf ( strpuf, "%s", ntxt.txt ) ;
				else
					sprintf ( strpuf, "%s", ptxt.txt );

				if (strlen(szTempt))
				{
					sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
				}
			
				if (( strlen(szTempt)) + ( strlen(clipped(strpuf))) >= CTEXTMAX  )
				break ;
				sprintf(  szTempt + strlen(szTempt) , "%s", clipped(strpuf) );
			}
		}
	}
	if ( strlen(szTempt))
	{	
		if ( ctyp[0] == 'V' )
			LlDefineVariableExt(hJob, hilfsfeld, szTempt, LL_TEXT, NULL);
		else
			LlDefineFieldExt(hJob, hilfsfeld, szTempt, LL_TEXT, NULL);
	}
	else
	{
		if ( ctyp[0] == 'V' )
			LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
		else
			LlDefineFieldExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
	}
}


void VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{

	char hilfsfeld[256] ;
	char hilfsfeld2[256] ;

	verboteneFelder = 0 ;	// 160113
	sprintf ( cverboteneFelder,"");	// 160113


	int i ;
	i=0;
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;

		sprintf ( hilfsfeld, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);

		switch ( tt_feldtyp[i] )
		{
		case 1 : // ptabn
			sprintf ( hilfsfeld2, "%s.ptbezk" , hilfsfeld );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
				;
			else
			{
				sprintf ( hilfsfeld2, "%s.ptbez" , hilfsfeld );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
					;
				else
				{
					sprintf ( hilfsfeld2, "%s.ptwer1" , hilfsfeld );
					if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
						;
					else
					{
						sprintf ( hilfsfeld2, "%s.ptwer2" , hilfsfeld );
						if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
							;
						else
						{
							sprintf ( hilfsfeld2, "%s.ptwert" , hilfsfeld );
							if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
								;
							else
							{
								break ;
							} // ptwert
						}	  // ptwer2
					}		  // ptwer1
				}			  // ptbez
			}				  // ptbezk
		
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			case(iDBSMALLINT):
				sprintf ( ptabn.ptwert, "%d", tt_sqllong[i] );
				break ;
			case(iDBINTEGER):
				sprintf ( ptabn.ptwert, "%ld", tt_sqllong[i] );
				break ;
			case(iDBDECIMAL):
				sprintf ( ptabn.ptwert, "%0.0d", tt_sqldouble[i] );
				break ;
			default :	// schlechte Notbremse
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			}
				
			if ( strlen ( clipped ( tt_creferenz[ tt_referenz[i]])) > 0 )	// 091209
			{
				sprintf ( ptabn.ptitem ,"%s" , clipped ( tt_creferenz[ tt_referenz[i]])) ;
			}
			else
				sprintf ( ptabn.ptitem ,"%s",tt_feldnam[i]);
	
			Ptabn.openptabn() ; 
			if ( ! Ptabn.leseptabn())
			{
				ptabschreiben ( hJob, "V" , 1 ,tt_tabnam[i], tt_feldnam[i] ) ;

/* ------> 110506 : auslagern
			
//				ptbezk
				sprintf ( hilfsfeld, "%s.%s.ptbezk",tt_tabnam[i], tt_feldnam[i] );

				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{
					LlDefineVariableExt(hJob, hilfsfeld , clipped(ptabn.ptbezk), LL_TEXT, NULL);
				}

//				ptbez
				sprintf ( hilfsfeld, "%s.%s.ptbez",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{
					LlDefineVariableExt(hJob, hilfsfeld , clipped(ptabn.ptbez), LL_TEXT, NULL);
				}

//				ptwert
				sprintf ( hilfsfeld, "%s.%s.ptwert",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{
					LlDefineVariableExt(hJob, hilfsfeld , clipped(ptabn.ptwert), LL_TEXT, NULL);
				}
//				ptwer1
				sprintf ( hilfsfeld, "%s.%s.ptwer1",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{
					LlDefineVariableExt(hJob, hilfsfeld , clipped(ptabn.ptwer1), LL_TEXT, NULL);
				}
//				ptwer2
				sprintf ( hilfsfeld, "%s.%s.ptwer2",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{
					LlDefineVariableExt(hJob, hilfsfeld, clipped(ptabn.ptwer2), LL_TEXT, NULL);
				}
< ----- */

			}
			else
				memcpy ( &ptabn,&ptabn_null, sizeof(struct PTABN));

			break ;

		case 2 : // Textbaustein 
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{
					textsetzen (hJob, i, hilfsfeld,"V");
				}
			break ;
		case 3 : // adresse
			
			if (adr.adr == tt_sqllong[i] )
			{
				adr.adr = adr.adr ;	// testbreakpoint
			}
			else
			{
				adr.adr = tt_sqllong[i];
	
				Adr.openadr() ; 
				if ( Adr.leseadr(0))	// immer lesen ODER leeren
				{
					memcpy ( &adr,&adr_null, sizeof(struct ADR));
					adr.adr = tt_sqllong[i];

				}
			}
//			anr als string
			sprintf ( hilfsfeld, "%s.%s.anr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				sprintf ( ptabn.ptwert, "%d", adr.anr );
				sprintf ( ptabn.ptitem ,"anr");
				Ptabn.openptabn() ; 
				if ( ! Ptabn.leseptabn())
// 280911					LlDefineVariableExt(hJob, hilfsfeld, clipped(ptabn.ptbezk), LL_TEXT, NULL);
					LlDefineVariableExt(hJob, hilfsfeld, clipped(ptabn.ptbez), LL_TEXT, NULL);
				else			
					LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
			}

//			adr_krz
			sprintf ( hilfsfeld, "%s.%s.adr_krz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
			    LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.adr_krz), LL_TEXT, NULL);
			}
//			adr_nam1
			sprintf ( hilfsfeld, "%s.%s.adr_nam1",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.adr_nam1), LL_TEXT, NULL);
			}
//			adr_nam2
			sprintf ( hilfsfeld, "%s.%s.adr_nam2",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.adr_nam2), LL_TEXT, NULL);
			}

//			adr_nam3	090905
			sprintf ( hilfsfeld, "%s.%s.adr_nam3",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.adr_nam3), LL_TEXT, NULL);
			}

//			ort1
			sprintf ( hilfsfeld, "%s.%s.ort1", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.ort1), LL_TEXT, NULL);
			}
//			plz
			sprintf ( hilfsfeld, "%s.%s.plz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.plz), LL_TEXT, NULL);
			}

//			postfach	080709
			sprintf ( hilfsfeld, "%s.%s.plz_pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.plz_pf), LL_TEXT, NULL);
			sprintf ( hilfsfeld, "%s.%s.pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.pf), LL_TEXT, NULL);

//			str
			sprintf ( hilfsfeld, "%s.%s.str",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld , clipped(adr.str), LL_TEXT, NULL);
			}
//			iln
			sprintf ( hilfsfeld, "%s.%s.iln",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld , clipped(adr.iln), LL_TEXT, NULL);
			}
//			adr
			sprintf ( hilfsfeld, "%s.%s.adr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
				LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
			}

// 310813 : partner,ort2,tel,fax,mobil,iban,swift dazu
//			partner
			sprintf ( hilfsfeld, "%s.%s.partner", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.partner), LL_TEXT, NULL);
			}

//			ort2
			sprintf ( hilfsfeld, "%s.%s.ort2", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.ort2), LL_TEXT, NULL);
			}

//			tel
			sprintf ( hilfsfeld, "%s.%s.tel", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.tel), LL_TEXT, NULL);
			}
//			fax
			sprintf ( hilfsfeld, "%s.%s.fax", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.fax), LL_TEXT, NULL);
			}
//			mobil
			sprintf ( hilfsfeld, "%s.%s.mobil", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.mobil), LL_TEXT, NULL);
			}
//			iban
			sprintf ( hilfsfeld, "%s.%s.iban", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.iban), LL_TEXT, NULL);
			}
//			swift
			sprintf ( hilfsfeld, "%s.%s.swift", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.swift), LL_TEXT, NULL);
			}

// 310813 E 


// staat	110506
			sprintf ( ptabn.ptwert, "%d", adr.staat );
			sprintf ( ptabn.ptitem ,"staat");
			Ptabn.openptabn() ; 
			if ( ! Ptabn.leseptabn())
			{
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				ptabschreiben ( hJob, "V" , 1 ,hilfsfeld, "staat" ) ;
			}

			break ;
		default :	// Standard-Felder 
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				switch ( tt_datetyp[i] )
				{
				case(iDBCHAR):
					sprintf ( szTemp2 ,"%s",clipped ((char *)tt_sqlval[i]));
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_TEXT, NULL);
					break ;
				case(iDBSMALLINT):
					sprintf ( szTemp2, "%01d", tt_sqllong[i] );
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBINTEGER):
					sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDECIMAL):
					sprintf ( szTemp2, "%01.4lf", tt_sqldouble[i] );
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDATUM):
					LlDefineVariableExt(hJob, hilfsfeld , sqldatamger ((char *)tt_sqlval[i], szTemp2) , LL_TEXT, NULL);

					break ;		
				default :	// schlechte Notbremse
					sprintf ( szTemp2 ,"%s",tt_sqlval[i]);
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_TEXT, NULL);
				break ;
				}
			}	// switch je datentyp/daba

			break ;
		} ;	// switch je Feldtyp/Generator



		i ++ ;
	}		// while	

	if (LlPrintIsVariableUsed(hJob, "rangt.nachkpreis"))
	{
		sprintf ( szTemp2 ,"%d",dnachkpreis) ;
		LlDefineVariableExt(hJob, "rangt.nachkpreis" , szTemp2, LL_NUMERIC, NULL);
	}

// 110309
	if (LlPrintIsVariableUsed(hJob, "rangt.variante"))
	{
		sprintf ( szTemp2 ,"%d",dvariante) ;
		LlDefineVariableExt(hJob, "rangt.variante" , szTemp2, LL_NUMERIC, NULL);
	}
// 310812
	if (LlPrintIsVariableUsed(hJob, "rangt.Nutzer"))
	{
		sprintf ( szTemp2, "%s", LUser );
		LlDefineVariableExt(hJob, "rangt.Nutzer" , szTemp2, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rangt.Heute")||LlPrintIsVariableUsed(hJob, "rangt.Zeit"))
	{
		time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);
		sprintf ( szTemp2 , "%02d.%02d.%04d",
			ltime->tm_mday ,ltime->tm_mon + 1 ,ltime->tm_year + 1900 );

		LlDefineVariableExt(hJob, "rangt.Heute" , szTemp2, LL_TEXT, NULL);

		sprintf ( szTemp2 , "%02d:%02d:%02d",
			ltime->tm_hour, ltime->tm_min, ltime->tm_sec );
		LlDefineVariableExt(hJob, "rangt.Zeit" , szTemp2, LL_TEXT, NULL);
	}
}

void FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{

	char hilfsfeld[256] ;
	char hilfsfeld2[256] ;


	int i ;
	i=0;
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;

		sprintf ( hilfsfeld, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);

		switch ( tt_feldtyp[i] )
		{
		case 1 : // ptabn
			sprintf ( hilfsfeld2, "%s.ptbezk" , hilfsfeld );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
				;
			else
			{
				sprintf ( hilfsfeld2, "%s.ptbez" , hilfsfeld );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
					;
				else
				{
					sprintf ( hilfsfeld2, "%s.ptwer1" , hilfsfeld );
					if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
						;
					else
					{
						sprintf ( hilfsfeld2, "%s.ptwer2" , hilfsfeld );
						if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
							;
						else
						{
							sprintf ( hilfsfeld2, "%s.ptwert" , hilfsfeld );
							if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
								;
							else
							{

								break ;
							} //ptwert
						}	  // ptwer2
					}		  // ptwer1
				}			  // ptbez
			}				  // ptbezk
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			case(iDBSMALLINT):
				sprintf ( ptabn.ptwert, "%d", tt_sqllong[i] );
				break ;
			case(iDBINTEGER):
				sprintf ( ptabn.ptwert, "%ld", tt_sqllong[i] );
				break ;
			case(iDBDECIMAL):
				sprintf ( ptabn.ptwert, "%0.0d", tt_sqldouble[i] );
				break ;
			default :	// schlechte Notbremse
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			}
				

			if ( strlen ( clipped ( tt_creferenz[ tt_referenz[i]])) > 0 )	// 091209
			{
				sprintf ( ptabn.ptitem ,"%s" , clipped ( tt_creferenz[ tt_referenz[i]])) ;
			}
			else
				sprintf ( ptabn.ptitem ,"%s",tt_feldnam[i]);
	
			Ptabn.openptabn() ; 
			if ( ! Ptabn.leseptabn())
			{
				
				ptabschreiben ( hJob, "F" , 1 ,	tt_tabnam[i], tt_feldnam[i] ) ;
/* ----> 110506 auslagern 
//				ptbezk
				sprintf ( hilfsfeld, "%s.%s.ptbezk",tt_tabnam[i], tt_feldnam[i] );

				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					LlDefineFieldExt(hJob, hilfsfeld , clipped(ptabn.ptbezk), LL_TEXT, NULL);
				}

//				ptbez
				sprintf ( hilfsfeld, "%s.%s.ptbez",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					LlDefineFieldExt(hJob, hilfsfeld , clipped(ptabn.ptbez), LL_TEXT, NULL);
				}

//				ptwert
				sprintf ( hilfsfeld, "%s.%s.ptwert",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					LlDefineFieldExt(hJob, hilfsfeld , clipped(ptabn.ptwert), LL_TEXT, NULL);
				}
//				ptwer1
				sprintf ( hilfsfeld, "%s.%s.ptwer1",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					LlDefineFieldExt(hJob, hilfsfeld , clipped(ptabn.ptwer1), LL_TEXT, NULL);
				}
//				ptwer2
				sprintf ( hilfsfeld, "%s.%s.ptwer2",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					LlDefineFieldExt(hJob, hilfsfeld, clipped(ptabn.ptwer2), LL_TEXT, NULL);
				}
< ---- */
			}
			break ;

		case 2 : // Textbaustein 
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
							// 081210 lsp_txt -> retp_txt .........
					if ( ! strcmp( clipped( tt_tabnam[i]) , "retp") && 
						 ! strcmp( clipped( tt_feldnam[i]), "retp_txt"))
						textsetzen (hJob, i, hilfsfeld, "f");	// genau bei diesem Feld Sonderbehandlung
					else
						textsetzen (hJob, i, hilfsfeld, "F");
				}
			break ;
		case 3 : // adresse

			if ( adr.adr == tt_sqllong[i] ) 
			{
				adr.adr = adr.adr ;	// testbreakpoint
			}
			else
			{
				adr.adr = tt_sqllong[i];
	
				Adr.openadr() ; 
				if ( Adr.leseadr(0))	// immer lesen ODER leeren
				{
					memcpy ( &adr,&adr_null, sizeof(struct ADR));
					adr.adr = tt_sqllong[i];

				}
			}

//			anr als string
			sprintf ( hilfsfeld, "%s.%s.anr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				sprintf ( ptabn.ptwert, "%d", adr.anr );
				sprintf ( ptabn.ptitem ,"anr");
				Ptabn.openptabn() ; 
				if ( ! Ptabn.leseptabn())
// 280911					LlDefineFieldExt(hJob, hilfsfeld, clipped(ptabn.ptbezk), LL_TEXT, NULL);
					LlDefineFieldExt(hJob, hilfsfeld, clipped(ptabn.ptbez), LL_TEXT, NULL);
				else			
					LlDefineFieldExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
			}

//			adr_krz
			sprintf ( hilfsfeld, "%s.%s.adr_krz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.adr_krz), LL_TEXT, NULL);
			}
//			adr_nam1
			sprintf ( hilfsfeld, "%s.%s.adr_nam1",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.adr_nam1), LL_TEXT, NULL);
			}
//			adr_nam2
			sprintf ( hilfsfeld, "%s.%s.adr_nam2",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.adr_nam2), LL_TEXT, NULL);
			}
//			adr_nam3			090905
			sprintf ( hilfsfeld, "%s.%s.adr_nam3",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.adr_nam3), LL_TEXT, NULL);
			}

//			ort1
			sprintf ( hilfsfeld, "%s.%s.ort1", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.ort1), LL_TEXT, NULL);
			}
//			plz
			sprintf ( hilfsfeld, "%s.%s.plz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.plz), LL_TEXT, NULL);
			}

//			postfach	080709
			sprintf ( hilfsfeld, "%s.%s.plz_pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.plz_pf), LL_TEXT, NULL);
			sprintf ( hilfsfeld, "%s.%s.pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.pf), LL_TEXT, NULL);

//			str
			sprintf ( hilfsfeld, "%s.%s.str",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld , clipped(adr.str), LL_TEXT, NULL);
			}
//			iln
			sprintf ( hilfsfeld, "%s.%s.iln",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld , clipped(adr.iln), LL_TEXT, NULL);
			}
//			adr
			sprintf ( hilfsfeld, "%s.%s.adr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
				LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
			}

// 310813 A: partner, ort2,tel,fax,mobil,iban,swift neu dazu
//			partner
			sprintf ( hilfsfeld, "%s.%s.partner", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.partner), LL_TEXT, NULL);
			}
//			ort2
			sprintf ( hilfsfeld, "%s.%s.ort2", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.ort2), LL_TEXT, NULL);
			}
//			tel
			sprintf ( hilfsfeld, "%s.%s.tel", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.tel), LL_TEXT, NULL);
			}
//			fax
			sprintf ( hilfsfeld, "%s.%s.fax", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.fax), LL_TEXT, NULL);
			}
//			mobil
			sprintf ( hilfsfeld, "%s.%s.mobil", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.mobil), LL_TEXT, NULL);
			}
//			iban
			sprintf ( hilfsfeld, "%s.%s.iban", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.iban), LL_TEXT, NULL);
			}
//			swift
			sprintf ( hilfsfeld, "%s.%s.swift", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.swift), LL_TEXT, NULL);
			}

// 310813 E : partner, ort2,tel,fax,mobil,iban,swift neu dazu

// staat	110506
			sprintf ( ptabn.ptwert, "%d", adr.staat );
			sprintf ( ptabn.ptitem ,"staat");
			Ptabn.openptabn() ; 
			if ( ! Ptabn.leseptabn())
			{
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				ptabschreiben ( hJob, "F" , 1 ,hilfsfeld, "staat" ) ;
			}


			break ;
		default :	// Standard-Felder 
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				switch ( tt_datetyp[i] )
				{
				case(iDBCHAR):
					sprintf ( szTemp2 ,"%s",clipped ((char *) tt_sqlval[i]));

					LlDefineFieldExt(hJob, hilfsfeld , (LPCSTR)tt_sqlval[i], LL_TEXT, NULL);
					break ;
				case(iDBSMALLINT):
					sprintf ( szTemp2, "%01d", tt_sqllong[i] );
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBINTEGER):
					sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDECIMAL):
					sprintf ( szTemp2, "%01.4lf", tt_sqldouble[i] );
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDATUM):
					LlDefineFieldExt(hJob, hilfsfeld , sqldatamger ((char *)tt_sqlval[i], szTemp2) , LL_TEXT, NULL);

					break ;		
				default :	// schlechte Notbremse
					sprintf ( szTemp2 ,"%s",tt_sqlval[i]);
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_TEXT, NULL);
				break ;
				}
			}	// switch je datentyp/daba

			break ;
		} ;	// switch je Feldtyp/Generator



		i ++ ;
	}		// while
	if (LlPrintIsFieldUsed(hJob, "rangt.nachkpreis"))
	{
		sprintf ( szTemp2 ,"%d",dnachkpreis);
		LlDefineFieldExt(hJob, "rangt.nachkpreis" , szTemp2, LL_NUMERIC, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rangt.variante"))
	{
		sprintf ( szTemp2 ,"%d",dvariante);
		LlDefineFieldExt(hJob, "rangt.variante" , szTemp2, LL_NUMERIC, NULL);
	}

// 310812 
	if (LlPrintIsFieldUsed(hJob, "rangt.Nutzer"))
	{
		sprintf ( szTemp2, "%s", LUser );
		LlDefineFieldExt(hJob, "rangt.Nutzer" , szTemp2, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rangt.Heute")||LlPrintIsFieldUsed(hJob, "rangt.Zeit"))
	{
		time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);
		sprintf ( szTemp2 , "%02d.%02d.%04d",
			ltime->tm_mday ,ltime->tm_mon + 1 ,ltime->tm_year + 1900 );
		LlDefineFieldExt(hJob, "rangt.Heute" , szTemp2, LL_TEXT, NULL);

		sprintf ( szTemp2 , "%02d:%02d:%02d",
			ltime->tm_hour, ltime->tm_min, ltime->tm_sec );
		LlDefineFieldExt(hJob, "rangt.Zeit" , szTemp2, LL_TEXT, NULL);
	}



}



//=============================================================================
void CMainFrame::OnEditList()
//=============================================================================
{


	/* --->
	char envchar[512] ;

	
	CHAR szFilename[128+1] = "*.lst";
	HWND hWnd = m_hWnd;
	HJOB hJob;
    char *etc;
	char filename[512];
	etc = getenv ("BWS");
    sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	FILENAME_DEFAULT = filename;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}
	
	// Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden (Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");



	char * penvchar = getenv ("BWS");
	sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);

	LlSetPrinterDefaultsDir(hJob, envchar) ;	// GrJ Printer-Datei
	penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// GrJ Vorschau-Datei


	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");


	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
		{
	        LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    // GR: Zur�cksetzen der internen Feld-Puffer
    LlDefineFieldStart(hJob);

//	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

	if (Listenauswahl != 1)  
    {
	   	sprintf ( szFilename, FILENAME_DEFAULT );
	}
	// GR: Aufruf des Designers
    if (LlDefineLayout(hJob, hWnd, "Design Rechnung", LL_PROJECT_LIST, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "Design Rechnung", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
< ----- */
	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);

}


//=============================================================================
void CMainFrame::OnPrintLabel()
//=============================================================================
{
	DoLabelPrint();
}


#ifdef OLD_DCL
char *uPPER (string)
char *string;
#else
char *uPPER (char *string)
#endif
{

   for( char *p = string; p < string + strlen( string ); p++ )
   {
      if( islower( *p ) )
			( p[0] = _toupper( *p ) );
   }
   return string ;
}


// CR am Stringende etfernen.

void cr_weg (char *string)
{
 for (; *string; string += 1)
 {
  if (*string == (char) 13)
    break;
  if (*string == (char) 10)
    break;
 }
 *string = 0;
 return;
}

// 300306 : Stringlaenge mit auswerten 
int strupcmpi (char *str1, char *str2, int len, int len2)
// int strupcmpi (char *str1, char *str2, int len)
{
 short i;
 char upstr1;
 char upstr2;

 if ( len < len2 ) return -1 ;	// 300306
 if ( len > len2 ) return  1 ;	// 300306


 for (i = 0; i < len; i ++, str1 ++, str2 ++)
 {
  if (*str1 == 0)
    return (-1);
  if (*str2 == 0)
    return (1);
  upstr1 = toupper((int) *str1);
  upstr2 = toupper((int) *str2);
  if (upstr1 < upstr2)
  {
   return(-1);
  }
  else if (upstr1 > upstr2)
  {
   return (1);
  }
 }
 return (0);
}

// static char * buffer ;

static char buffer[1024] ;

static char buffere[1024] ;

int next_char_ci (char *string, char tzeichen, int i)
// Naechstes Zeichen != Trennzeichen suchen.

{
       for (;string [i]; i ++)
       {
               if (string[i] != tzeichen)
               {
                                   return (i);
               }
       }
       return (i);
}

// 300306 : der 2. Parameter ist dann ein endloser String bis zum #-Trenner oder CR/LF
short splite (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
 					zeichen ='#' ;
       }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}

short split (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 // if (buffer == (char *) 0) buffer = (char *) malloc (0x1000);
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}

// 031109
short splitdoll (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = '$';
 // if (buffer == (char *) 0) buffer = (char *) malloc (0x1000);
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}


int holedatenausdatei(void)
{
int anz;
char buffer [512];
char token[64] ;
int gefunden ;
gefunden = 0 ;

FILE *fp;
	fp = fopen (datenausdateiname, "r");
    if (fp == (FILE *)NULL) return FALSE;
    while (fgets (buffer, 511, fp))
    {
		cr_weg (buffer);
        anz = split (buffer);
        if (anz < 2) continue;

// 300306  
		sprintf ( token, "ALTERNATETYP" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
//			anz = splite(buffer) ;
			strcpy (alternat_type, clipped (uPPER (wort [2])));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}

		// funktioniert vorerst NICHT mit space-Namen
		sprintf ( token, "ALTERNATENAME" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
		{
			anz = splite(buffer) ;
			strcpy (alternat_file, clipped (wort [2]));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}

		// funktioniert vorerst NICHT mit space-Namen
		sprintf ( token, "ALTERNATEDIR" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			anz = splite(buffer) ;
			strcpy (alternat_dir, clipped (wort [2]));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}





		sprintf ( token, "NAME" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
										(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (myform_nr, clipped (uPPER (wort [2])));
			gefunden ++ ;
			continue ;
		}
// 121104
		sprintf ( token, "USER" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)

		{
			strcpy (User, clipped (uPPER (wort [2])));
			gefunden ++ ;
			continue ;
		}

		sprintf ( token, "ANZAHL" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {

			danzahl = atoi (wort[2]);
			gefunden ++ ;
			continue ;
        }

// 250505
		sprintf ( token, "DEBUG" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {

			ddebug = atoi (wort[2]);
			continue ;
        }


		sprintf ( token, "LABEL" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
								(int)strlen(clipped(wort[1]))) == 0)
        {
			GlobalLila = atoi( wort[2]) ;
			gefunden ++ ;
			continue ;
		}
		sprintf ( token, "DRUCK" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token) ,
							(int)strlen(clipped(wort[1]))) == 0)
        {
			DrMitMenue = atoi (wort[2]);
			gefunden ++ ;
			continue ;
        }
// 110309
		sprintf ( token, "MITVARIANTE" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token) ,
							(int)strlen(clipped(wort[1]))) == 0)
        {
			dvariante = atoi (wort[2]);
			gefunden ++ ;
			continue ;
        }

	}
	fclose (fp);
	if ( gefunden ) return TRUE;

	return FALSE ;
}

// Nicht besonders elegant, aber es funktioniert ....
int holerangesausdatei(char * token, char * wert1, char * wert2)
{
int anz;
char buffer [512];
FILE *fp;

	wert1[0] = '\0' ;
	wert2[0] = '\0' ;
	fp = fopen (datenausdateiname, "r");
    if (fp == (FILE *)NULL) return FALSE;
    while (fgets (buffer, 511, fp))
    {
		cr_weg (buffer);
        anz = split (buffer);
        if (anz < 2) continue;

        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)

        {
			strcpy (wert1, clipped (wort [2]));
			strcpy (wert2, clipped (wort [3]));
			return TRUE ;
		}
	}
	fclose (fp);
	return FALSE ;
}


//=============================================================================
void CMainFrame::InterpretCommandLine()
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");

    char *px;
	char p[299];
//	EntwurfsModus = 1 ;


	SetWindowText ( "Retouren-Druck") ;


	EntwurfsModus = 0 ;
	WahlModus = 0;	// 171204
	GlobalLila = 0 ;

	ddebug = 0 ;	// 250505
	debugfp = (FILE *) NULL ;	// 250505

	datenausdatei = 0 ;		// 300904

	alternat_ausgabe = 0 ;	// 300306 
	danzahl = 1 ;			// 121104
	User[0] = '\0' ;		// 121104
	LUser[0] = '\0' ;		// 310812
    Token.NextToken ();
	while ((px = Token.NextToken ()) != NULL)
	{
		sprintf ( p ,"%s", clipped(uPPER(px)));

		if (strcmp (p, "-H") == 0)
		{
			MessageBox ("161104 : Aufruf : drllls -datei DATEINAMEabsolut ",
				"oder drllls -wahl oder drllls -name xxxxxXXXXX [-dr 0|1][-l] ",
	                            MB_OK);

			EntwurfsModus = 0 ;
              return;
		}

	// 300904
		if (strcmp (p, "-DATEI") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( datenausdateiname ,"%s", clipped(uPPER(px)));
			if ( strlen ( datenausdateiname ) > 1 )
			{
				EntwurfsModus = 0 ;
				// Achtung : klappt nicht bei Namen mit ner Luecke ......	

				if ( holedatenausdatei())
				{
					datenausdatei = 1 ;
					break ;
				}
			}
		}

		if (strcmp (p, "-NAME") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( myform_nr ,"%s", clipped ( uPPER (px)));
			EntwurfsModus = 0 ;
//			return ;
			continue ;	// 031109
		}

		if (strcmp (p, "-DR") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            DrMitMenue = (atoi (px));
			EntwurfsModus = 0 ;
			continue ;	// 031109
		}

//  171204
		if (strcmp (p, "-WAHL") == 0)
		{
			WahlModus = 1 ;
			sprintf ( myform_nr , "54100" ) ;	

			SetWindowText ( "Druckerwahl-Dialog Retouren") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemsichte Parametrierung
		}


//  090206
		if (strcmp (p, "-WAHL2") == 0)
		{
			WahlModus = 2 ;
			sprintf ( myform_nr , "54100" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r Retouren") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemsichte Parametrierung
		}

//  090206
		if (strcmp (p, "-WAHL3") == 0)
		{
			WahlModus = 3 ;
			sprintf ( myform_nr , "54100" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r Retouren") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemsichte Parametrierung
		}

//  090206
		if (strcmp (p, "-WAHL4") == 0)
		{
			WahlModus = 4 ;
			sprintf ( myform_nr , "54100" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r Retouren") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemsichte Parametrierung
		}
// 220207
		if (strcmp (p, "-NUTZER") == 0)
		{
			// damit kann man es so regeln, das als LETZTER Token auch leerer Nutzer korrekt funktioniert
			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( User ,"%s", px);
            sprintf ( LUser ,"%s", px);
			continue ;	// 031109
		}


// Label verwenden
		if (strcmp (p, "-L") == 0)
		{
			GlobalLila = 1 ;
			continue ;	// 031109
		}

// 031109
		if (strcmp (p, "-ANZAHL") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;

			danzahl = (atoi (px));
			if ( danzahl < 1 ) danzahl = 1 ;

			continue ;	// 031109
		}

	}

}


//=============================================================================
void CMainFrame::OnPrintReport()
//=============================================================================
{
    
	InterpretCommandLine() ;

	if ( !GlobalLila )
	    DoListPrint();
	else
		DoLabelPrint();
}

//=============================================================================
void CMainFrame::DoLabelPrint()
//=============================================================================
{

	/* -----> 090506  : redundant ----->
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lbl", szTemp[50], szTemp2[505] **, szBoxText[200] ** ;
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;

// QQQQQQQ Labelprint

	strcpy(szTemp, " ");
	char *etc;
	char filename[512];

	CString sMedia;

	char envchar[512];
	etc = getenv ("BWS");
	if ( User[0] == '\0' )
		sprintf (filename, "%s\\format\\LL\\%s.lbl", etc, myform_nr);
	else
		sprintf (filename, "%s\\format\\LL\\%s\\%s.lbl", etc,User, myform_nr);

// QQQQQQQ Labelprint

	FILE *fp ;
	fp = fopen ( filename, "r" ); 
	if ( fp== NULL )
	{
		User[0] = '\0' ;
		sprintf (filename, "%s\\format\\LL\\%s.lbl", etc, myform_nr);
	}
	else fclose ( fp) ;

// QQQQQQQ Labelprint


	FILENAME_DEFAULT = clipped (filename);

	if ( strlen(myform_nr) < 4 || strlen( myform_nr) > 10 )
	{
		MessageBox("Ung�ltiger Labelname!", "Usage : drllls {-name xxxxxXXXXX [-dr 0|1][-l]| -datei Datei}",
										MB_OK|MB_ICONSTOP);
		return;
    }

// QQQQQQQ Labelprint

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "drllls-Exception 010", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"drllls-Exception 020",
					MB_OK|MB_ICONSTOP);
		return;
    }

	// Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");

// QQQQQQQ Labelprint

	char * penvchar = getenv ("BWS");
	sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);
	if ( User[0] == '\0' )	// 121104
		sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);
	else
		sprintf ( envchar ,"%s\\format\\LL\\%s\\" , penvchar, User);

	LlSetPrinterDefaultsDir(hJob,envchar) ;	// Printer-Datei
	penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// Vorschau-Datei

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// US: Choose new expression mode with formula support
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);
	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);
	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog

// QQQQQQQ Labelprint
   	
	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
   		{
	    	if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "drllls-Exception 030",
				MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	
			return;
		}
	}

// QQQQQQQ Labelprint

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	dbClass.opendbase ("bws");

	Temptab.structprep(myform_nr) ;

    Variablendefinition (hJob);
//    Felderdefinition (hJob);

    // GR: Druck starten

// QQQQQQQ Labelprint

    if ( DrMitMenue == 1)
    {
		int retc1 = LlPrintWithBoxStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...");
		if ( retc1 < 0 )
		{
			sprintf ( dmess, " drllls-Exception : %d", retc1 );
			MessageBox("Error While Printing", dmess , MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			Temptab.drfree();
			dbClass.closedbase ("bws");
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

// QQQQQQQ Labelprint

	else	// Druck pur immer auf den Drucker
	{
		int retc2 ;
		if ( alternat_ausgabe )	// 300306 
		{

			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
 
			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.Path",alternat_dir);
			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.File",alternat_file);

			if ( ! strcmp ( alternat_type , "PDF") )
			{
				                                                                                             // min defa max
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}

			retc2 = LlPrintStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							(int) NULL );

		}
		else
		{

			retc2 = LlPrintStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_NORMAL,
							(int) NULL ** dummy ** );

		}
** --->
	if (LlPrintStart(hJob, LL_PROJECT_LABEL, szFilename,LL_PRINT_NORMAL,(int) NULL ) < 0)
< ---- **
		if ( retc2 < 0 )
		{
			sprintf ( dmess, " drllls-Exception : %d", retc2 );
	
			MessageBox("Error While Printing", dmess, MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			Temptab.drfree() ;
			dbClass.closedbase ("bws");
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
   } 

// QQQQQQQ Labelprint

	// GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);

// QQQQQQQ Labelprint

	if ( DrMitMenue == 1)
    {
		if (LlPrintOptionsDialog(hJob, hWnd, "Drucker-Einrichtung") < 0)
		{
			LlPrintEnd(hJob,0);
			LlJobClose(hJob);
			Temptab.drfree() ;
		    dbClass.closedbase ("bws");
			if (Hauptmenue == 0)
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
	        return;
		}
		//GR: Druckziel abfragen
		LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		sMedia.ReleaseBuffer();
    }
	else
	{
		// 300306
		if ( alternat_ausgabe )
		{
			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);

			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Path", alternat_dir);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.File", alternat_file);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Quiet", "1");
//			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_PRINTDST_FILENAME, alternat_file);

			if ( ! strcmp ( alternat_type , "PDF") )
			{
// 080905 : Mit diesen Parametern ergibt sich je nach Vorlage ein Reduktionsfaktor 5 .. 10 gegenueber
// der default-Parametrierung beim Einsatz von Logos und RTF-Texten 				
// Performance-Fresser bleibt RTF-Text, Platz-Fresser bleiben bitmaps und Bilder
				                                                                                             // min defa max
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}

		}
		else
		{
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		}
		sMedia.ReleaseBuffer();

	}

// QQQQQQQ Labelprint

//	LlPrintCopyPrinterConfiguration (hJob, szneupfilename, , LL_PRINTERCONFIG_SAVE );
//	LlPrintCopyPrinterConfiguration (hJob, szpfilename, , LL_PRINTERCONFIG_RESTORE );


	char szPrinter[80], szPort[20];
	int  nRecCount = 30, nErrorValue = 0, nRecno, nLastPage;

// QQQQQQQ Labelprint

	LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

    nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;


	// GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	int abbruch = Temptab.opentemptab();
	if ( abbruch == -3 ) 
    {
//		sprintf (dmess, "User-Abbruch ");
//		MessageBox (dmess, "", MB_ICONERROR);
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
			Temptab.drfree () ;
			dbClass.closedbase ("bws");
			if (Hauptmenue == 0)  //#LuD
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
    		return;

	}

	int anzahl = Temptab.getanzahl(); 

// QQQQQQQ Labelprint

	int di = Temptab.lesetemptab (0);   //       1.DATENSATZ lesen f�r Variablen�bergabe
											// nErrorValue als Paramter ist hier nicht gut
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				Temptab.drfree () ;
				dbClass.closedbase ("bws");
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}

// QQQQQQQ Labelprint

	while ( ( !di )
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA))
   	{
		for ( int dox = 0 ; dox < anzahl ; dox ++ )
		{
			VariablenUebergabe ( hJob, szTemp2, nRecno );	// Variablen vor derListe .....
** ---->
		// Labelprint
    	// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    	sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    	nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        if (nErrorValue == LL_ERR_USER_ABORTED)
    	{
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
    		return;
    	}
< ---- **

			nErrorValue = LlPrint(hJob);
		}
		di = Temptab.lesetemptab (0) ;	//       DATENSATZ lesen
	}

// QQQQQQQ Labelprint

	//GR: Druck beenden
	LlPrintEnd(hJob,0);

	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) l�schen
        LlPreviewDisplay(hJob, szFilename, penvchar, hWnd);

        LlPreviewDeleteFiles(hJob, szFilename,penvchar);
    }

	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
	Temptab.drfree();
	dbClass.closedbase ("bws");
	if (Hauptmenue == 0)  
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}
	< ------ */
}

// 160305 : vorsortiert sollten die Artikel ohnehin sein 
void lade_leerg( double ia , int istk, double ipr, char * ibez )
{
	int ipoint ;
	if ( ia == 0.0 && istk == 0 )
	{
		for ( ipoint=0 ;ipoint < LG_MAX ; ipoint++ )
		{
			leerg_st[ipoint] = 0;
			leerg_zu[ipoint] = 0;
			leerg_ab[ipoint] = 0;
			leerg_a [ipoint] = 0.0 ;
			leerg_p [ipoint] = 0.0 ;
		}
		return ;
	}

	for ( ipoint=0 ;ipoint < LG_MAX ; ipoint++ )
	{
		if ( leerg_a [ipoint] == 0.0)
		{
			leerg_a[ipoint] = ia ;
			leerg_p[ipoint] = ipr ; 
			sprintf( leerg_bz[ipoint],"%s",ibez );
			if (istk > 0 )
				leerg_zu[ipoint] = istk ;
			else
				leerg_ab[ipoint] = 0 - istk ;
			return ;
		}

		if ( leerg_a [ipoint] == ia)
		{
			if (istk > 0 )
				leerg_zu[ipoint] += istk ;
			else
				leerg_ab[ipoint] += (0 - istk) ;
			return ;
		}
	}
}

void SchreibeLeergut(HJOB hJob, char * szTemp2)
{

	int ipoint ;
	// hier ist kun.kun_leer_kz bzw. Filial-LS bereits integriert
	if ( ! dkun_leer ) return ;

	leer_lsdr.ls = Temptab.get_retk_ret();
	sprintf ( leer_lsdr.blg_typ, "R" ) ;
	leer_lsdr.fil = 0 ;
	leer_lsdr.mdn = Temptab.get_retk_mdn() ;

	Leer_lsdr.openlsdr();

	while ( ! Leer_lsdr.leselsdr(0) )
	{
		for ( ipoint = 0 ; ipoint < LG_MAX ; ipoint ++ )
		{
			if ( leer_lsdr.a == leerg_a[ipoint])  
			{
				leerg_st[ipoint] = leer_lsdr.stk ;
				break ;
			};

			if ( leerg_a[ipoint] == 0.0 )
			{
				leerg_a [ipoint] = leer_lsdr.a ;
				leerg_st[ipoint] = leer_lsdr.stk ;
				a_bas.a = leer_lsdr.a ;
				A_bas.opena_bas() ;
				if ( ! A_bas.lesea_bas(0))
					sprintf ( leerg_bz[ipoint],"%s", a_bas.a_bz1 );
				break ;
			};
		}
			
	}
	for ( ipoint = 0 ; ipoint < LG_MAX ; ipoint ++ )
	{
		if ( leerg_a[ipoint] == 0.0 ) break ;	// es ist vollbracht

// ###### ipoint == 0
		if ( ipoint == 0 )
		{
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a1"))
			{	sprintf ( szTemp2 ,"%0.0f",leerg_a[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a1" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a_bez1"))
			{	sprintf ( szTemp2 ,"%s",leerg_bz[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a_bez1" , szTemp2, LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_zu1"))
			{	sprintf ( szTemp2 ,"%d",leerg_zu[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_zu1" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk1"))
			{	sprintf ( szTemp2 ,"%d",leerg_st[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk1" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_ab1"))
			{	sprintf ( szTemp2 ,"%d",leerg_ab[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_ab1" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.pr_vk1"))
			{	sprintf ( szTemp2 ,"%4.4f",leerg_p[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.pr_vk1" , szTemp2, LL_NUMERIC, NULL);
			}
			continue ;
		}

// ###### ipoint == 1
		if ( ipoint == 1 )
		{
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a2"))
			{	sprintf ( szTemp2 ,"%0.0f",leerg_a[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a2" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a_bez2"))
			{	sprintf ( szTemp2 ,"%s",leerg_bz[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a_bez2" , szTemp2, LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_zu2"))
			{	sprintf ( szTemp2 ,"%d",leerg_zu[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_zu2" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk2"))
			{	sprintf ( szTemp2 ,"%d",leerg_st[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk2" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_ab2"))
			{	sprintf ( szTemp2 ,"%d",leerg_ab[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_ab2" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.pr_vk2"))
			{	sprintf ( szTemp2 ,"%4.4f",leerg_p[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.pr_vk2" , szTemp2, LL_NUMERIC, NULL);
			}
			continue ;
		}

// ###### ipoint == 2
		if ( ipoint == 2 )
		{
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a3"))
			{	sprintf ( szTemp2 ,"%0.0f",leerg_a[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a3" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a_bez3"))
			{	sprintf ( szTemp2 ,"%s",leerg_bz[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a_bez3" , szTemp2, LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk3"))
			{	sprintf ( szTemp2 ,"%d",leerg_st[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk3" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_zu3"))
			{	sprintf ( szTemp2 ,"%d",leerg_zu[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_zu3" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_ab3"))
			{	sprintf ( szTemp2 ,"%d",leerg_ab[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_ab3" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.pr_vk3"))
			{	sprintf ( szTemp2 ,"%4.4f",leerg_p[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.pr_vk3" , szTemp2, LL_NUMERIC, NULL);
			}
			continue ;
		}

// ###### ipoint == 3
		if ( ipoint == 3 )
		{
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a4"))
			{	sprintf ( szTemp2 ,"%0.0f",leerg_a[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a4" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a_bez4"))
			{	sprintf ( szTemp2 ,"%s",leerg_bz[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a_bez4" , szTemp2, LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_zu4"))
			{	sprintf ( szTemp2 ,"%d",leerg_zu[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_zu4" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk4"))
			{	sprintf ( szTemp2 ,"%d",leerg_st[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk4" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_ab4"))
			{	sprintf ( szTemp2 ,"%d",leerg_ab[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_ab4" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.pr_vk4"))
			{	sprintf ( szTemp2 ,"%4.4f",leerg_p[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.pr_vk4" , szTemp2, LL_NUMERIC, NULL);
			}
			continue ;
		}
// ###### ipoint == 4
		if ( ipoint == 4 )
		{
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a5"))
			{	sprintf ( szTemp2 ,"%0.0f",leerg_a[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a5" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a_bez5"))
			{	sprintf ( szTemp2 ,"%s",leerg_bz[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a_bez5" , szTemp2, LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_zu5"))
			{	sprintf ( szTemp2 ,"%d",leerg_zu[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_zu5" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk5"))
			{	sprintf ( szTemp2 ,"%d",leerg_st[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk5" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_ab5"))
			{	sprintf ( szTemp2 ,"%d",leerg_ab[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_ab5" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.pr_vk5"))
			{	sprintf ( szTemp2 ,"%0.4f",leerg_p[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.pr_vk5" , szTemp2, LL_NUMERIC, NULL);
			}
			continue ;
		}


// ###### ipoint == 5
		if ( ipoint == 5 )
		{
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a6"))
			{	sprintf ( szTemp2 ,"%0.0f",leerg_a[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a6" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a_bez6"))
			{	sprintf ( szTemp2 ,"%s",leerg_bz[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a_bez6" , szTemp2, LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_zu6"))
			{	sprintf ( szTemp2 ,"%d",leerg_zu[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_zu6" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk6"))
			{	sprintf ( szTemp2 ,"%d",leerg_st[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk6" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_ab6"))
			{	sprintf ( szTemp2 ,"%d",leerg_ab[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_ab6" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.pr_vk6"))
			{	sprintf ( szTemp2 ,"%0.4f",leerg_p[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.pr_vk6" , szTemp2, LL_NUMERIC, NULL);
			}
			continue ;
		}

// ###### ipoint == 6
		if ( ipoint == 6 )
		{
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a7"))
			{	sprintf ( szTemp2 ,"%0.0f",leerg_a[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a7" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a_bez7"))
			{	sprintf ( szTemp2 ,"%s",leerg_bz[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a_bez7" , szTemp2, LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_zu7"))
			{	sprintf ( szTemp2 ,"%d",leerg_zu[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_zu7" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk7"))
			{	sprintf ( szTemp2 ,"%d",leerg_st[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk7" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_ab7"))
			{	sprintf ( szTemp2 ,"%d",leerg_ab[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_ab7" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.pr_vk7"))
			{	sprintf ( szTemp2 ,"%0.4f",leerg_p[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.pr_vk7" , szTemp2, LL_NUMERIC, NULL);
			}
			continue ;
		}

// ###### ipoint == 7
		if ( ipoint == 7 )
		{
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a8"))
			{	sprintf ( szTemp2 ,"%0.0f",leerg_a[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a8" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a_bez8"))
			{	sprintf ( szTemp2 ,"%s",leerg_bz[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a_bez8" , szTemp2, LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_zu8"))
			{	sprintf ( szTemp2 ,"%d",leerg_zu[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_zu8" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk8"))
			{	sprintf ( szTemp2 ,"%d",leerg_st[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk8" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_ab8"))
			{	sprintf ( szTemp2 ,"%d",leerg_ab[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_ab8" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.pr_vk8"))
			{	sprintf ( szTemp2 ,"%0.4f",leerg_p[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.pr_vk8" , szTemp2, LL_NUMERIC, NULL);
			}
			continue ;
		}

/* ---->
// ###### ipoint == 8
		if ( ipoint == 8 )
		{
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a9"))
			{	sprintf ( szTemp2 ,"%0.0f",leerg_a[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a9" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.a_bez9"))
			{	sprintf ( szTemp2 ,"%s",leerg_bz[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.a_bez9" , szTemp2, LL_TEXT, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_zu9"))
			{	sprintf ( szTemp2 ,"%d",leerg_zu[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_zu9" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk9"))
			{	sprintf ( szTemp2 ,"%d",leerg_st[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk9" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.stk_ab9"))
			{	sprintf ( szTemp2 ,"%d",leerg_ab[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.stk_ab9" , szTemp2, LL_NUMERIC, NULL);
			}
			if (LlPrintIsFieldUsed(hJob, "leer_lsh.pr_vk9"))
			{	sprintf ( szTemp2 ,"%0.4f",leerg_p[ipoint]);
				LlDefineFieldExt(hJob, "leer_lsh.pr_vk9" , szTemp2, LL_NUMERIC, NULL);
			}
			continue ;
		}
< -------------- */
	}

}
// komplettieren der Leergutausgabe






//=============================================================================
void CMainFrame::DoListPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lst", szTemp[50], szTemp2[505]/*, szBoxText[200]*/ ;
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;
	strcpy(szTemp, " ");
	char *etc;
	char filename[512];

	CString sMedia;

	char envchar[512];
	etc = getenv ("BWS");

    FILE *fp ;

	if ( User[0] == '\0' )
        sprintf (filename, "%s\\format\\LL\\%s.lst", etc, myform_nr);
	else
        sprintf (filename, "%s\\format\\LL\\%s\\%s.lst", etc,User, myform_nr);

	sprintf ( LUser, "%s", User ) ;	// 220207

	fp = fopen ( filename, "r" ); 
	if ( fp== NULL )
	{
		User[0] = '\0' ;
		sprintf (filename, "%s\\format\\LL\\%s.lst", etc, myform_nr);
	}
	else fclose ( fp) ;

// 121104 E



	FILENAME_DEFAULT = clipped (filename);
	if ( strlen(myform_nr) < 4 || strlen( myform_nr) > 10 )
	{
		MessageBox("Ung�ltiger Listenname!", "Usage :  drllls {-name xxxxxXXXXX [-dr 0|1][-l]| -datei Datei}", MB_OK|MB_ICONSTOP);
		return;
    }


	// GR: Initialisieren von List & Label.

	if ( zeittest ) schreibezeit("Vor Job-Open \n" ) ;

	hJob = LlJobOpen(LUL_LANGUAGE);

	if ( zeittest ) schreibezeit("Nach Job-Open \n" ) ;

	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "drllls-Exception 010", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"drllls-Exception 020",
					MB_OK|MB_ICONSTOP);
		return;
    }


 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
// CELL11 : fuer LuL.90 : 	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");
#ifdef LL19
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "V2XSEQ");
#else

#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "ht8gOw");
#else				// LL11 als default bis 120911
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");
#endif
#endif


	if ( zeittest ) schreibezeit("Nach Lizenz \n" ) ;


	char * penvchar = getenv ("BWS");
	sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);

	if ( User[0] == '\0' )	// 121104
		sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);
	else
		sprintf ( envchar ,"%s\\format\\LL\\%s\\" , penvchar, User);

	LlSetPrinterDefaultsDir(hJob,envchar) ;	// Printer-Datei
	penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// Vorschau-Datei

	
		//GR: Exporter aktivieren
#ifdef LL19
	// gar nix, default ist reichlich
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll12ex.llx");
#else
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");
#endif
#endif

	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );


	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
	   	{
    		if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "drllls-Exception 030", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	
			return;
		}
	}

	if ( zeittest ) schreibezeit("Vor V+F-Variablen \n" ) ;

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);
    LlDefineFieldStart(hJob);

	if ( zeittest ) schreibezeit("Vor daba-Open \n" ) ;

	dbClass.opendbase ("bws");

	if ( zeittest ) schreibezeit("Nach daba-Open \n" ) ;

	Temptab.structprep(myform_nr) ;

	if ( zeittest ) schreibezeit("Nach temptabstruct \n" ) ;


// 160305
	sprintf ( sys_par.sys_par_nam,"kun_leer" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	dkun_leer = Sys_par.lesesys_par(0);
	if ( dkun_leer || sys_par.sys_par_wrt[0] == '\0')
	{
		dkun_leer = 0 ;
	}
	else
	{
		dkun_leer = 0;
		dkun_leer = atoi ( sys_par.sys_par_wrt ) ;
	}
	lade_leerg( 0.0,0,0.0,"")	;	// Initialisierung

	sprintf ( sys_par.sys_par_nam,"a_zustxt_wa" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	da_zustxt_wa = Sys_par.lesesys_par(0);
	if ( da_zustxt_wa || sys_par.sys_par_wrt[0] == '\0')
	{
		da_zustxt_wa = 0 ;
	}
	else
	{
		da_zustxt_wa = 0;
		da_zustxt_wa = atoi ( sys_par.sys_par_wrt ) ;
	}

	da_zustxt_wa = 0 ;	// bei Retouren nicht aktiv .....

	sprintf ( sys_par.sys_par_nam,"nachkpreis" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	dnachkpreis = Sys_par.lesesys_par(0);
	if ( dnachkpreis || sys_par.sys_par_wrt[0] == '\0')
	{
		dnachkpreis = 0 ;
	}
	else
	{
		dnachkpreis = 0;
		dnachkpreis = atoi ( sys_par.sys_par_wrt ) ;
	}
	if ( dnachkpreis < 2 || dnachkpreis > 4 ) dnachkpreis = 2 ;

// 110205
	// folgendes ist zu gewaehrleisten :
	// 1. wa_pos_txt == ON
	// 2. lsp.posi anwaehlen
	// 3. als Referenztabelle lsp_txt eintragen ( anstelle von lspt, falls wa_pos_txt == OFF) 

	sprintf ( sys_par.sys_par_nam,"wa_pos_txt" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	dwa_pos_txt = Sys_par.lesesys_par(0);
	if ( dwa_pos_txt || sys_par.sys_par_wrt[0] == '\0')
	{
		dwa_pos_txt = 0 ;
	}
	else
	{
		dwa_pos_txt = 0;
		dwa_pos_txt = atoi ( sys_par.sys_par_wrt ) ;
	}

	if ( zeittest ) schreibezeit("Vor V+F-Definition \n" ) ;

    Variablendefinition (hJob);
    Felderdefinition (hJob);

    // GR: Druck starten

    if ( DrMitMenue == 1 && WahlModus == 0 )	// 071014

    {
/* ->
		if (LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...") < 0)
< --- */
		int retc1 = LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...");
		if ( retc1 < 0 )
		{
			sprintf ( dmess, " drllret-Exception : %d", retc1 );
			MessageBox("Error While Printing", dmess , MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			Temptab.drfree();
			dbClass.closedbase ("bws");
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	// GrJ 040204

			return;
		}
	}
	else	// Druck pur immer auf den Drucker
	{
		int retc2 ;
		if ( alternat_ausgabe )	// 300306 
		{

			if ( zeittest ) schreibezeit("Vor setzealternat \n" ) ;


			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);

			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.Path",alternat_dir);
			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.File",alternat_file);
 			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Quiet", "1");


			if ( ! strcmp ( alternat_type , "PDF") )
			{
				                                                                                             // min defa max
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}

			if ( zeittest ) schreibezeit("Vor Printstart \n" ) ;

			retc2 = LlPrintStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							(int) NULL );

			if ( zeittest ) schreibezeit("Nach Printstart \n" ) ;
		}
		else
		{

			if ( ! WahlModus )	// 071014 : wahlmodus auswerten
			{
				if ( zeittest ) schreibezeit("Vor Printstart \n" ) ;
				retc2 = LlPrintStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_NORMAL,
							(int) NULL /* dummy */);
				if ( zeittest ) schreibezeit("Nach Printstart \n" ) ;
			}
			else	// 071014 :wegen default
				retc2 = 0;
		}

/* --->
		if (LlPrintStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_NORMAL,
							(int) NULL ) < 0)
< ---- */
		if ( retc2 < 0 )
		{
			sprintf ( dmess, " drllret-Exception : %d", retc2 );
	
			MessageBox("Error While Printing", dmess, MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			Temptab.drfree() ;
			dbClass.closedbase ("bws");
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	// GrJ 040204
			return;
		}
   } 

// 031109 A
	if ( danzahl < 1 ) danzahl = 1 ;	// Notbremse
	if ( (!alternat_ausgabe && danzahlerlaubt && ! DrMitMenue) || ( danzahl > 1 ) )
	{
		int i = LlPrintSetOption(hJob, LL_PRNOPT_COPIES, danzahl);
		i = LlPrintGetOption(hJob, LL_PRNOPT_COPIES_SUPPORTED);
	}

	if ( alternat_ausgabe )
	{
		LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
	}

// 031109 E


    // GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
// 031109 :	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);


	if ( WahlModus )	// 171204 - 140909 : hier stand bisher "Lieferschein" anstatt  "Retoure"
	{

		sprintf ( szFilename, "%s\\format\\ll\\54100.lst",getenv ("BWS"));
		char szFilenam1[299] ;
		sprintf ( szFilenam1, "%s\\format\\ll\\54101.lst",getenv ("BWS"));
		int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;

		sprintf ( szFilename, "%s\\format\\ll\\54100.lsp",getenv ("BWS"));
		sprintf ( szFilenam1, "%s\\format\\ll\\54101.lsp",getenv ("BWS"));
		i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben
		if ( WahlModus > 1 )
		{
			SetWindowText ( "Druckerwahl Retouren - 2.Exemplar") ;

			sprintf ( szFilename, "%s\\format\\ll\\541002.lst",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\541012.lst",getenv ("BWS"));
			int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
			sprintf ( szFilename, "%s\\format\\ll\\541002.lsp",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\541012.lsp",getenv ("BWS"));
		    i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben
		}
		if ( WahlModus > 2 )
		{
		SetWindowText ( "Druckerwahl Retouren - 3.Exemplar") ;
		sprintf ( szFilename, "%s\\format\\ll\\541003.lst",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\541013.lst",getenv ("BWS"));
			int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
			sprintf ( szFilename, "%s\\format\\ll\\541003.lsp",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\541013.lsp",getenv ("BWS"));
		    i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben
		}
		if ( WahlModus > 3 )
		{
			SetWindowText ( "Druckerwahl Retouren - 4.Exemplar") ;
			sprintf ( szFilename, "%s\\format\\ll\\541004.lst",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\541014.lst",getenv ("BWS"));
			int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
			sprintf ( szFilename, "%s\\format\\ll\\541004.lsp",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\541014.lsp",getenv ("BWS"));
		    i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben
		}
		LlPrintEnd(hJob,0);
		LlJobClose(hJob);
		dbClass.closedbase ("bws");
		if (Hauptmenue == 0)  
		{
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		}
		if ( i < 0 ) return;

// 090206  i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben

		return ;
	}

	if ( DrMitMenue == 1)
    {
	
		if (LlPrintOptionsDialog(hJob, hWnd, "Drucker-Einrichtung") < 0)
		{
			LlPrintEnd(hJob,0);
			LlJobClose(hJob);
			Temptab.drfree() ;
		    dbClass.closedbase ("bws");
			if (Hauptmenue == 0)
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
	        return;
		}
		//GR: Druckziel abfragen
		LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		sMedia.ReleaseBuffer();
    }
	else
	{

		if ( alternat_ausgabe )
		{
			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);

			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Path", alternat_dir);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.File", alternat_file);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Quiet", "1");
//			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_PRINTDST_FILENAME, alternat_file);

			if ( ! strcmp ( alternat_type , "PDF") )
			{
				                                                                                             // min defa max
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}


		}
		else
		{
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		}
		sMedia.ReleaseBuffer();

	}


    int  nRecCount = 1, nErrorValue = 0, nPrintFehler = 0, nLastPage, nRecno;
	char szPrinter[80], szPort[20];

    LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

	if ( zeittest ) schreibezeit("Nach GetPrinterinfo \n" ) ;

	nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);

	nRecno = 1;

    // GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.

//  spaeter evtl .aml : zu aufwendig 	nRecCount = GetRecCount () ;

//	sprintf (dmess, "nRecCount = %hd",nRecCount);  //050803
//	MessageBox(dmess, "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);


	if ( zeittest ) schreibezeit("vor erster Fetch \n" ) ;

	int abbruch = Temptab.opentemptab();

	if ( zeittest ) schreibezeit("Nach erster Fetch \n" ) ;

	if ( abbruch == -3 ) 
    {
//		sprintf (dmess, "User-Abbruch ");
//		MessageBox (dmess, "", MB_ICONERROR);
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
			Temptab.drfree () ;
			dbClass.closedbase ("bws");
			if (Hauptmenue == 0)  //#LuD
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
    		return;

	}

		int di = Temptab.lesetemptab (0);   //       1.DATENSATZ lesen f�r Variablen�bergabe
											// nErrorValue als Paramter ist hier nicht gut

	if (di != 0 || verboteneFelder ) 
    {
		if ( verboteneFelder )	// 160113
			sprintf (dmess, "%s",cverboteneFelder);
		else	// kann nur noch di als Ursache sein ........
			sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);

		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				Temptab.drfree () ;
				dbClass.closedbase ("bws");
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
	if ( ! Temptab.kun_leer_kz()) dkun_leer = 0  ;	// Ausgabe des LG als total normale Artikel
		// Filial-LS retourniert hier immer "1"


//	while ( (( nRecno < nRecCount) || ( nRecno == 1 && nRecCount == 1))
	while ( ( !di )
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA))
   	{
	    VariablenUebergabe ( hJob, szTemp2, nRecno );	// Variablen vor derListe .....

		nErrorValue = LlPrint(hJob);

		while ((!di) && (nErrorValue == 0) 
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage))

		{

			if ( Temptab.a_bas_a_typ()  == 11 && dkun_leer )
			{

				lade_leerg( Temptab.a_bas_a() ,(int) Temptab.retp_ret_me(),
				            Temptab.retp_ret_vk_euro(), Temptab.a_bas_a_bz1() );
			    FelderUebergabe ( hJob, szTemp2, nRecno );	// 081210 : trotzdem lesen ,aber nicht schreiben ( z.B. wegen Texten oder anderen Infos

				di = Temptab.lesetemptab (0) ;	//       DATENSATZ lesen
				if ( di )
				{
					di = di ;	// Testbreakpoint
					// 210506 
					VariablenUebergabe ( hJob, szTemp2, nRecno );
					SchreibeLeergut (hJob,szTemp2) ;	
				}

				continue ;
			}

		    FelderUebergabe ( hJob, szTemp2, nRecno );
    		// GR: Drucken der aktuellen Tabellenzeile
    		nErrorValue = LlPrintFields(hJob);
			nPrintFehler = nErrorValue;

			if (nErrorValue == LL_WRN_REPEAT_DATA)
			{
			    VariablenUebergabe ( hJob, szTemp2, nRecno );
				nErrorValue = LlPrint(hJob);
//			    FelderUebergabe ( hJob, szTemp2, nRecno );
//	    		nErrorValue = LlPrintFields(hJob);
			}


			if (nErrorValue == 0)	// US: everything fine, record could have been printed...
	    	{
				// US: ...but probably the user defined a filter condition!
				//     So before updating time dependent variables we have to check if the record really has been printed:
				if (LlPrintDidMatchFilter(hJob))
				{
					// GR: Aktualisieren der zeitabhaengigen Variable 'FixedVariable2'
					// US: update the time dependent variable 'FixedVariable2'
					//     coming after printig the list(s)
		    		sprintf(szTemp2, "FixedVariable2, record %d", nRecno);
		    		LlDefineVariable(hJob, "FixedVariable2", szTemp2);
		    	}

//	    		if (nPrintFehler == 0) nRecno++;	// GR: gehe zum naechsten Datensatz
			}
			di = Temptab.lesetemptab (nPrintFehler) ;	//       DATENSATZ lesen
			if ( di )
			{
				di = di ;	// Testbreakpoint
			// 210506 	
				VariablenUebergabe ( hJob, szTemp2, nRecno );
				SchreibeLeergut (hJob,szTemp2) ;	

			}


		}
  	}

	// US: all records have been printed, now flush the table
	//     If footer doesn't fit to this page try again for the next page:

	VariablenUebergabe ( hJob, szTemp2, nRecno );

	SchreibeLeergut (hJob,szTemp2) ;	// 160305
	nErrorValue = LlPrintFieldsEnd(hJob);

	if (nErrorValue == LL_WRN_REPEAT_DATA)
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	    VariablenUebergabe ( hJob, szTemp2, nRecno );
	    FelderUebergabe ( hJob, szTemp2, nRecno );
		SchreibeLeergut (hJob,szTemp2) ;	// 160305

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);
	}

	if (nErrorValue == LL_WRN_REPEAT_DATA)	// US: footer still doesn't fit!
		MessageBox("Error because table is too small for footer!", "List & Label App", MB_OK|MB_ICONEXCLAMATION);

	//GR: Druck beenden
	LlPrintEnd(hJob,0);
	
	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) loeschen
        LlPreviewDisplay(hJob, szFilename, penvchar, hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, penvchar);
    }
	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
	Temptab.drfree();
	dbClass.closedbase ("bws");

	if ( zeittest ) schreibezeit("alles vorbei \n" ) ;

	if (Hauptmenue == 0)  
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}

}

//=============================================================================
LRESULT CMainFrame::OnLulMessage(WPARAM wParam, LPARAM lParam)
//=============================================================================
{
	// GR: Dies ist die List & Label Callback Funktion.
	//     Saemtliche Rueckmeldungen bzw. Events werden dieser Funktion
	//     gemeldet.
	// US: This is the List & Label Callback function.
	//     Is is called for requests an notifications.


	PSCLLCALLBACK	pscCallback = (PSCLLCALLBACK) lParam;
	LRESULT			lRes = TRUE;
	CString			sVariableDescr;

	static CHAR szHelpText[256];

	ASSERT(pscCallback->_nSize == sizeof(scLlCallback));	// GR: Die groesse der Callback Struktur muss stimmen!
															// US: sizes of callback structs must match!

	switch(wParam)
	{
		case LL_CMND_VARHELPTEXT:	// GR: Es wird ein Beschreibungstext f�r eine Variable erfragt.
									// US: Helptext needed for selected variable within designer

				// GR: ( pscCallback->_lParam enth�lt einen LPCSTR des Beschreibungstextes )
				// US: ( pscCallback->_lParam contains a LPCSTR to the name of the selected variable )

				sVariableDescr = (LPCSTR)pscCallback->_lParam;

				if (!sVariableDescr.IsEmpty())
					sprintf(szHelpText,
							"This is the sample field / variable '%s'.",
							(LPCSTR)sVariableDescr);
				else
					strcpy(szHelpText, "No variable or field selected.");

				pscCallback->_lReply = (LPARAM)szHelpText;
				break;

		default:
				pscCallback->_lReply = lRes = FALSE; // GR: Die Nachricht wurde nicht bearbeitet.
													 // US: indicate that message hasn't been processed
	}

	return(lRes);
}




static char DefWert [256] ;



char *get_default (char *env)
{         
        int anz;
        char buffer [512];
        FILE *fp;
        sprintf (buffer, "%s\\bws_defa",getenv ("BWSETC"));	// 090109 : bisher nur dummy , jetzt aus bws_defa was lesen
		fp = fopen (buffer, "r");
        if (fp == (FILE *)NULL) return NULL;

// redundant        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmpi (wort[1], env, (int)strlen (env),
									(int)strlen(clipped(wort[1]))) == 0)
    
					 {
                                 strcpy (DefWert, clipped (wort [2]));
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}

// 031109 
char *get_cfg (char *env)
{         
        int anz;
        char buffer [512];
        FILE *fp;
        sprintf (buffer, "%s\\drllls.cfg",getenv ("BWSETC"));
		fp = fopen (buffer, "r");
        if (fp == (FILE *)NULL) return NULL;

        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = splitdoll (buffer);
                     if (anz < 2) continue;
                     if (strupcmpi (wort[1], env, (int)strlen (env),
							(int)strlen(clipped(wort[1]))) == 0)
                     {
                                 strcpy (DefWert, clipped (wort [2]));
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}

void CMainFrame::GetCfgValues (void)
{
//       char cfg_v [1024];
//       if (ProgCfg == NULL) return;

		char hilfstr[256] ;
		char *p ;


	cfgOK = TRUE;

	Hauptmenue =0 ;
	Listenauswahl = 0 ;
	DrMitMenue = 1 ;

	if ( ! zeittest )
	{
		p =  get_cfg("zeittest");
		if ( p != NULL )
		{
			sprintf ( hilfstr ,"%s", p );
			zeittest = atoi ( hilfstr ) ;
		} ;
		if ( zeittest ) schreibezeit("Start via drllls.cfg \n" ) ;
	}



// 090109
	NutzerIgnorieren = 0 ;
	p =  get_default("NutzerIgnorieren");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		NutzerIgnorieren = atoi ( hilfstr ) ;
	} ;

// 031109

	danzahlerlaubt = 0 ;
	p =  get_cfg("anzahlerlaubt");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		danzahlerlaubt = atoi ( hilfstr ) ;
	} ;

	return ;
/* ---> der rest ist evtl. fuer spaeter
	Hauptmenue = 0 ;
	p =  get_default("Hauptmenue");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		Hauptmenue = atoi ( hilfstr ) ;
	} ;
	

	Listenauswahl = 0 ;
	p =  get_default("Listenauswahl");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		Listenauswahl = atoi ( hilfstr ) ;
	} ;

	DrMitMenue = 0 ;
	p =  get_default("DrMitMenue");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		DrMitMenue = atoi ( hilfstr ) ;
	} ;

	if ( buffer !=  (char *) 0) 
			free ( buffer );
< ---- */

/* --->
		Listenauswahl =0 ;
		if (ProgCfg->GetCfgValue ("Listenauswahl", cfg_v) == TRUE)
       {
           Listenauswahl = atoi (cfg_v);
	   }
< -*/
/* -->
		Hauptmenue = 0;
		if (ProgCfg->GetCfgValue ("Hauptmenue", cfg_v) == TRUE)
       {
           Hauptmenue = atoi (cfg_v);
	   }

	   exit(0) ;
< ---- */

/* --->
		DrMitMenue = 1 ;
       if (ProgCfg->GetCfgValue ("DrMitMenue", cfg_v) == TRUE)
       {
           DrMitMenue = atoi (cfg_v);
	   }
< ---- */

}


    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt
	/************
	LlDefineFieldExt(hJob, "Barcode_EAN13", "44|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P2", "44|44444|44444|44", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P5", "44|44444|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN128", "EAN128ean128", LL_BARCODE_EAN128, NULL);
	LlDefineFieldExt(hJob, "Barcode_CODE128", "Code 128", LL_BARCODE_CODE128, NULL);
	LlDefineFieldExt(hJob, "Barcode_Codabar", "A123456A", LL_BARCODE_CODABAR, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCA", "44|44444", LL_BARCODE_EAN8, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCE", "4|44444|44444", LL_BARCODE_UPCA, NULL);
	LlDefineFieldExt(hJob, "Barcode_3OF9", "*TEST*", LL_BARCODE_3OF9, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IND", "44444", LL_BARCODE_25INDUSTRIAL, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IL", "444444", LL_BARCODE_25INTERLEAVED, NULL);
	LlDefineFieldExt(hJob, "Barcode_25MAT", "44444", LL_BARCODE_25MATRIX, NULL);
	LlDefineFieldExt(hJob, "Barcode_25DL", "44444", LL_BARCODE_25DATALOGIC, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET5", "44444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET10", "44444-4444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET12", "44444-444444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_FIM", "A", LL_BARCODE_FIM, NULL);
	**********/


void fitapldefines (HJOB hJob)
{

/* -->
	sprintf (form_feld.form_nr, "test1");

	dbClass.sqlopen (form_feld_curs);

	while (dbClass.sqlfetch (form_feld_curs) == 0)
	{
		ll_typ = holetyp(form_feld.tab_nam , form_feld.feld_nam );

		form_feld.tab_nam[0] = toupper(form_feld.tab_nam[0]) ;
		form_feld.feld_nam[0] = toupper(form_feld.feld_nam[0]) ;
		CString s = form_feld.tab_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.tab_nam, s ) ;
		s = form_feld.feld_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.feld_nam, s ) ;
		sprintf( tabdotfeld , "%s.%s", form_feld.tab_nam, form_feld.feld_nam);
		sprintf ( defabeleg, "%s", form_feld.feld_nam);
		if (ll_typ == LL_NUMERIC )
		{
			sprintf ( defabeleg, "12345678");
		};
	
			LlDefineFieldExt(hJob, tabdotfeld, defabeleg,ll_typ,NULL);

	}
*********/
}


