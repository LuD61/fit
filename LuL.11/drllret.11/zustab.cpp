#include "stdafx.h"
#include "DbClass.h"
#include "zustab.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif


struct SYS_PAR sys_par,  sys_par_null;
struct A_ZUS_TXT a_zus_txt, a_zus_txt_null;
struct A_KUN a_kun, a_kun_null;

extern DB_CLASS dbClass;


int SYS_PAR_CLASS::lesesys_par (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int SYS_PAR_CLASS::opensys_par (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

void SYS_PAR_CLASS::prepare (void)
{


	dbClass.sqlin ((char *) sys_par.sys_par_nam, SQLCHAR, 18);

	dbClass.sqlout ((char *) sys_par.sys_par_wrt,SQLCHAR, 2);
	dbClass.sqlout ((char *) sys_par.sys_par_besch,SQLCHAR, 33);
	dbClass.sqlout ((long *) &sys_par.zei, SQLLONG, 0);
	dbClass.sqlout ((short *) &sys_par.delstatus, SQLSHORT, 0);

		readcursor = dbClass.sqlcursor ("select "

	" sys_par_wrt, sys_par_besch, zei, delstatus " 
	" from sys_par where sys_par_nam = ? " ) ;
	

}

int A_KUN_CLASS::lesea_kun (int fehlercode)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_KUN_CLASS::opena_kun (void)
{
		if ( readcursor < 0 ) prepare ();
		
		return dbClass.sqlopen (readcursor);
}

void A_KUN_CLASS::prepare (void)
{

	dbClass.sqlin (( double	*) &a_kun.a,        SQLDOUBLE,0);
	dbClass.sqlin (( long	*) &a_kun.kun,      SQLLONG,  0);
	dbClass.sqlin (( char	*)   a_kun.kun_bran2,SQLCHAR,  3);
	dbClass.sqlin (( short	*) &a_kun.mdn,      SQLSHORT, 0);

	dbClass.sqlout (( short  *) &a_kun.mdn,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.fil,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( long	 *) &a_kun.kun,		SQLLONG,	 0 ) ;
	dbClass.sqlout (( double *) &a_kun.a,		SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.a_kun,	SQLCHAR,	14 ) ;
	dbClass.sqlout (( char	 *)  a_kun.a_bz1,	SQLCHAR,	25 ) ;
	dbClass.sqlout (( short	 *) &a_kun.me_einh_kun,SQLSHORT, 0 ) ;
	dbClass.sqlout (( double *) &a_kun.inh,		SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.kun_bran2,SQLCHAR,	 3 ) ;
	dbClass.sqlout (( double *) &a_kun.tara,	SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( double *) &a_kun.ean,		SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.a_bz2,	SQLCHAR,	25 ) ;
	dbClass.sqlout (( short	 *) &a_kun.hbk_ztr,	SQLSHORT,	 0 ) ;
	dbClass.sqlout (( long	 *) &a_kun.kopf_text,SQLLONG,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.pr_rech_kz,SQLCHAR,   2 ) ;
	dbClass.sqlout (( char	 *)  a_kun.modif,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( long	 *) &a_kun.text_nr,	SQLLONG,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.devise,	SQLSHORT,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.geb_eti,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( char	 *)  a_kun.geb_fill,SQLCHAR,	 2 ) ;
	dbClass.sqlout (( long	 *) &a_kun.geb_anz,	SQLLONG,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.pal_eti,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( char	 *)  a_kun.pal_fill,SQLCHAR,	 2 ) ;
	dbClass.sqlout (( short	 *) &a_kun.pal_anz,	SQLSHORT,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.pos_eti,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( short	 *) &a_kun.sg1,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.sg2,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.pos_fill,SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.ausz_art,SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.text_nr2,SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.cab,		SQLSHORT,    0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.a_bz3,	SQLCHAR,	25 ) ;
	dbClass.sqlout (( char	 *)  a_kun.a_bz4,	SQLCHAR,	25 ) ;
	dbClass.sqlout (( char	 *)  a_kun.li_a,	SQLCHAR,	13 ) ;
	dbClass.sqlout (( double *) &a_kun.geb_fakt,SQLDOUBLE,	 0 ) ;


		readcursor = dbClass.sqlcursor ("select "

	" mdn, fil, kun, a, a_kun, a_bz1, me_einh_kun, inh, kun_bran2, "
	" tara, ean, a_bz2, hbk_ztr, kopf_text, pr_rech_kz, modif, text_nr, "
	" devise, geb_eti, geb_fill, geb_anz, pal_eti, pal_fill, pal_anz, "
	" pos_eti, sg1,	sg2, pos_fill, ausz_art, text_nr2, cab,	a_bz3, a_bz4, "
	" li_a,	geb_fakt "

	" from a_kun where a = ? and kun = ? and kun_bran2 = ? and mdn = ? and fil = 0 " ) ;
	// fil == 0 ergaenzt 1302010 

}


int A_ZUS_TXT_CLASS::lesea_zus_txt (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }
	  memcpy ( &a_zus_txt, &a_zus_txt_null, sizeof ( struct A_ZUS_TXT )) ;	// 290506
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_ZUS_TXT_CLASS::opena_zus_txt (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

void A_ZUS_TXT_CLASS::prepare (void)
{


	dbClass.sqlin ((double *) &a_zus_txt.a, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) a_zus_txt.txt,SQLCHAR, 61);
	dbClass.sqlout ((short *) &a_zus_txt.sg1,SQLSHORT, 0);
	dbClass.sqlout ((long *) &a_zus_txt.zei, SQLLONG, 0);

		readcursor = dbClass.sqlcursor ("select "

	" txt, sg1, zei " 
	" from a_zus_txt where a = ?  and zei > 0 order by zei" ) ;

}

