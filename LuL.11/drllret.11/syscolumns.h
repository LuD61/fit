#ifndef _SYSCOLUMNS_DEF
#define _SYSCOLUMNS_DEF

struct SYSCOLUMNS {
   char     colname[19];
   long     tabid;
   short    coltype;
   short  collength;
}
 ;

extern int coltyp_curs ;

extern struct SYSCOLUMNS syscolumns;

struct NFORM_FELD {
   short    delstatus;
   char     form_nr[11];
   char     feld_nam[19];
   char     tab_nam[19];
   short    feld_typ;
   short    feld_id;
   short    sort_nr;
   char		krz_txt[25];
   short	range ;
   short    lila ;
}
 ;

extern int nformfeld_curs ;

extern struct NFORM_FELD nform_feld;

// Typ Datenbank ( syscolumns - Informix)
#define iDBCHAR		0
#define iDBSMALLINT 1
#define iDBINTEGER  2
#define iDBDECIMAL  5
#define iDBDATUM    7

#define iDBVARCHAR	13		// 160113 : varchar aus syscolumns direkt anschliessend als char weiterhandeln ?!
#define iSQL_VARCHAR	12	// 160113 : varchar aus odbc32 , anschliessend als char weiterhandeln ?!
#define iDBSERIAL 262		// 160113


// Typ LuL 
#define iTYPPTAB	1
#define iTYPPTXT	2
#define iTYPADR		3


class SYSCOLUMNS_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//               int dbcount (void);
               int lesesyscolumns (void);
               int opensyscolumns (void);
               SYSCOLUMNS_CLASS () : DB_CLASS ()
               {
				  coltyp_curs = -1 ;
               }
};

class NFORM_FELD_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//               int dbcount (void);
               int lesenform_feld (void);
               int opennform_feld (void);
               NFORM_FELD_CLASS () : DB_CLASS ()
               {
				   nformfeld_curs = -1 ;
               }
};
#endif
