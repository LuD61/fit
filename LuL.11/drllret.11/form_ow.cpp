#include "stdafx.h"
#include "DbClass.h"
#include "form_ow.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif


struct FORM_W form_w,  form_w_null;
struct FORM_O form_o,  form_o_null;

extern DB_CLASS dbClass;
extern char * clipped( char *) ;

void subrueck ( char * text )
{
	int geslen, ipo ;
	unsigned char uchar ;
	geslen = (int) strlen ( clipped( text)) ;
	for ( ipo = 0 ; ipo < geslen ; ipo ++ )
	{
		uchar = text[ipo] ;
		if (uchar > 0x80 && uchar < 0xa0 ) text[ipo] = uchar - 0x80 ;
	}
}

int FORM_W_CLASS::leseform_w (							)
{

      int di = dbClass.sqlfetch (readcursor);
	  if ( !di) subrueck( form_w.krz_txt);

	  return di;
}


int FORM_W_CLASS::openform_w (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}



void FORM_W_CLASS::prepare (void)
{


	dbClass.sqlin ((char *) form_w.form_nr, SQLCHAR, 11);
	dbClass.sqlin ((short *) &form_w.lila, SQLSHORT, 0);

	dbClass.sqlout ((long *) &form_w.zei, SQLLONG,0 );
	dbClass.sqlout ((char *) form_w.krz_txt, SQLCHAR, 51);

    readcursor = dbClass.sqlcursor ("select zei, krz_txt from form_w "
					"where form_w.form_nr = ? and form_w.lila = ? "
					"order by zei ");
										
	test_upd_cursor = 1;

}

int FORM_O_CLASS::leseform_o (							)
{

      int di = dbClass.sqlfetch (readcursor);
	  if ( !di) subrueck( form_o.krz_txt);
	  return di;
}


int FORM_O_CLASS::openform_o (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}



void FORM_O_CLASS::prepare (void)
{


	dbClass.sqlin ((char *) form_o.form_nr, SQLCHAR, 11);
	dbClass.sqlin ((short *) &form_o.lila, SQLSHORT, 0);

	dbClass.sqlout ((long *) &form_o.zei, SQLLONG,0 );
	dbClass.sqlout ((char *) form_o.krz_txt, SQLCHAR, 51);


    readcursor = dbClass.sqlcursor ("select zei,krz_txt from form_o "
					"where form_o.form_nr = ? and form_o.lila = ? "
					" order by zei ");
										
	test_upd_cursor = 1;

}

