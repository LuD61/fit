// range.cpp : Implementierungsdatei
//

#include "stdafx.h"
// #include "dr70001.h"
#include "range.h"
#include "DbClass.h"
#include "temptab.h"
#include "item.h"
#include "syscolumns.h"

extern char * clipped(char*) ;
extern short GlobalLila ;

static int v_abbruch ;

// Crange-Dialogfeld

IMPLEMENT_DYNAMIC(Crange,CDialog) // 140504 : CDialog<-CPropertyPage)
Crange::Crange()
// 140504	: CPropertyPage(Crange::IDD)
	: CDialog(Crange::IDD)
	, m_vcedit1(_T(""))
	, m_vcedit2(_T(""))
	, m_vcedit3(_T(""))
	, m_vcedit4(_T(""))
	, m_vcedit5(_T(""))
	, m_vcedit6(_T(""))
	, m_vcedit7(_T(""))
	, m_vcedit8(_T(""))
	, m_vcedit9(_T(""))
	, m_vcedit10(_T(""))
	, m_vcedit11(_T(""))
	, m_vcedit12(_T(""))
	, m_vcedit13(_T(""))
	, m_vcedit14(_T(""))
	, m_vcedit15(_T(""))
	, m_vcedit16(_T(""))
	, m_vcedit17(_T(""))
	, m_vcedit18(_T(""))
	, m_vcedit19(_T(""))
	, m_vcedit20(_T(""))
	, m_vcedit21(_T(""))
	, m_vcedit22(_T(""))
	, m_vcedit23(_T(""))
	, m_vcedit24(_T(""))
	, m_vcedit25(_T(""))
	, m_vcedit26(1)
{
}

ITEM_CLASS Item ;


BOOL Crange::OnInitDialog()
 
{

int l,m,n;
CDialog::OnInitDialog();
l=m=n=0;

	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i ++ )
	{
		if ( ! tt_range[i] ) continue ;

		sprintf ( item.name, "%s",tt_feldnam[i]);
		Item.openitem();
		if ( Item.leseitem()) continue ;	// Item fehlt .......

		switch (l)
		{
		case 0 :
				m_vcedit17 = _T (item.bezlang);
			break ;
		case 1 :
				m_vcedit18 = _T (item.bezlang);
			break ;
		case 2 :
				m_vcedit19 = _T (item.bezlang);
			break ;
		case 3 :
				m_vcedit20 = _T (item.bezlang);
			break ;
		case 4 :
				m_vcedit21 = _T (item.bezlang);
			break ;
		case 5 :
				m_vcedit22 = _T (item.bezlang);
			break ;
		case 6 :
				m_vcedit23 = _T (item.bezlang);
			break ;
		case 7 :
				m_vcedit24 = _T (item.bezlang);
			break ;
		}
		l ++ ;
		tt_range[i] = l ;

	}

	while ( l < 8 )
	{
		switch ( l)
		{
		case 0 :
				m_cedit1.ShowWindow(SW_HIDE); m_cedit9.ShowWindow(SW_HIDE);m_cedit17.ShowWindow(SW_HIDE);
			break ;
		case 1 :
				m_cedit2.ShowWindow(SW_HIDE); m_cedit10.ShowWindow(SW_HIDE);m_cedit18.ShowWindow(SW_HIDE);
			break ;
		case 2 :
				m_cedit3.ShowWindow(SW_HIDE); m_cedit11.ShowWindow(SW_HIDE);m_cedit19.ShowWindow(SW_HIDE);
			break ;
		case 3 :
				m_cedit4.ShowWindow(SW_HIDE); m_cedit12.ShowWindow(SW_HIDE);m_cedit20.ShowWindow(SW_HIDE);
			break ;
		case 4 :
				m_cedit5.ShowWindow(SW_HIDE); m_cedit13.ShowWindow(SW_HIDE);m_cedit21.ShowWindow(SW_HIDE);
			break ;
		case 5 :
				m_cedit6.ShowWindow(SW_HIDE); m_cedit14.ShowWindow(SW_HIDE);m_cedit22.ShowWindow(SW_HIDE);
			break ;
		case 6 :
				m_cedit7.ShowWindow(SW_HIDE); m_cedit15.ShowWindow(SW_HIDE);m_cedit23.ShowWindow(SW_HIDE);
			break ;
		case 7 :
				m_cedit8.ShowWindow(SW_HIDE); m_cedit16.ShowWindow(SW_HIDE);m_cedit24.ShowWindow(SW_HIDE);
			break ;
		}
		l ++ ;
	}


	if ( ! GlobalLila)
	{
		m_cedit25.ShowWindow(SW_HIDE); m_cedit26.ShowWindow(SW_HIDE);
			}
	else
	{
				m_vcedit25 = _T ("Anzahl");
	}
	UpdateData(FALSE) ;

	return TRUE ;
}


Crange::~Crange()
{
}

void Crange::DoDataExchange(CDataExchange* pDX)
{
// 140504 	CPropertyPage::DoDataExchange(pDX);
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_vcedit1);
	DDX_Text(pDX, IDC_EDIT2, m_vcedit2);
	DDX_Text(pDX, IDC_EDIT3, m_vcedit3);
	DDX_Text(pDX, IDC_EDIT4, m_vcedit4);
	DDX_Text(pDX, IDC_EDIT5, m_vcedit5);
	DDX_Text(pDX, IDC_EDIT6, m_vcedit6);
	DDX_Text(pDX, IDC_EDIT7, m_vcedit7);
	DDX_Text(pDX, IDC_EDIT8, m_vcedit8);
	DDX_Text(pDX, IDC_EDIT9, m_vcedit9);
	DDX_Text(pDX, IDC_EDIT10, m_vcedit10);
	DDX_Text(pDX, IDC_EDIT11, m_vcedit11);
	DDX_Text(pDX, IDC_EDIT12, m_vcedit12);
	DDX_Text(pDX, IDC_EDIT13, m_vcedit13);
	DDX_Text(pDX, IDC_EDIT14, m_vcedit14);
	DDX_Text(pDX, IDC_EDIT15, m_vcedit15);
	DDX_Text(pDX, IDC_EDIT16, m_vcedit16);
	DDX_Text(pDX, IDC_EDIT17, m_vcedit17);
	DDX_Text(pDX, IDC_EDIT18, m_vcedit18);
	DDX_Text(pDX, IDC_EDIT19, m_vcedit19);
	DDX_Text(pDX, IDC_EDIT20, m_vcedit20);
	DDX_Text(pDX, IDC_EDIT21, m_vcedit21);
	DDX_Text(pDX, IDC_EDIT22, m_vcedit22);
	DDX_Text(pDX, IDC_EDIT23, m_vcedit23);
	DDX_Text(pDX, IDC_EDIT24, m_vcedit24);

	DDX_Control(pDX, IDC_EDIT1, m_cedit1);
	DDX_Control(pDX, IDC_EDIT2, m_cedit2);
	DDX_Control(pDX, IDC_EDIT3, m_cedit3);
	DDX_Control(pDX, IDC_EDIT4, m_cedit4);
	DDX_Control(pDX, IDC_EDIT5, m_cedit5);
	DDX_Control(pDX, IDC_EDIT6, m_cedit6);
	DDX_Control(pDX, IDC_EDIT7, m_cedit7);
	DDX_Control(pDX, IDC_EDIT8, m_cedit8);
	DDX_Control(pDX, IDC_EDIT9, m_cedit9);
	DDX_Control(pDX, IDC_EDIT10, m_cedit10);
	DDX_Control(pDX, IDC_EDIT11, m_cedit11);
	DDX_Control(pDX, IDC_EDIT12, m_cedit12);
	DDX_Control(pDX, IDC_EDIT13, m_cedit13);
	DDX_Control(pDX, IDC_EDIT14, m_cedit14);
	DDX_Control(pDX, IDC_EDIT15, m_cedit15);
	DDX_Control(pDX, IDC_EDIT16, m_cedit16);
	DDX_Control(pDX, IDC_EDIT17, m_cedit17);
	DDX_Control(pDX, IDC_EDIT18, m_cedit18);
	DDX_Control(pDX, IDC_EDIT19, m_cedit19);
	DDX_Control(pDX, IDC_EDIT20, m_cedit20);
	DDX_Control(pDX, IDC_EDIT21, m_cedit21);
	DDX_Control(pDX, IDC_EDIT22, m_cedit22);
	DDX_Control(pDX, IDC_EDIT23, m_cedit23);
	DDX_Control(pDX, IDC_EDIT24, m_cedit24);

	DDX_Control(pDX, IDC_EDIT25, m_cedit25);
	DDX_Text(pDX, IDC_EDIT25, m_vcedit25);

	DDX_Control(pDX, IDC_EDIT26, m_cedit26);
	DDX_Text(pDX, IDC_EDIT26, m_vcedit26);
	DDV_MinMaxInt(pDX, m_vcedit26, 1, 9999);
}


// 140504 BEGIN_MESSAGE_MAP(Crange, CPropertyPage)
BEGIN_MESSAGE_MAP(Crange, CDialog)


	ON_EN_KILLFOCUS(IDC_EDIT1, OnEnKillFocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT2, OnEnKillFocusEdit2)
//	Mustereintrag ON_EN_CHANGE(IDC_EDIT3, OnEnChangeEdit3)
	ON_EN_KILLFOCUS(IDC_EDIT3, OnEnKillFocusEdit3)
	ON_EN_KILLFOCUS(IDC_EDIT4, OnEnKillFocusEdit4)
	ON_EN_KILLFOCUS(IDC_EDIT5, OnEnKillFocusEdit5)
	ON_EN_KILLFOCUS(IDC_EDIT6, OnEnKillFocusEdit6)
	ON_EN_KILLFOCUS(IDC_EDIT7, OnEnKillFocusEdit7)
	ON_EN_KILLFOCUS(IDC_EDIT8, OnEnKillFocusEdit8)
	ON_EN_KILLFOCUS(IDC_EDIT9, OnEnKillFocusEdit9)
	ON_EN_KILLFOCUS(IDC_EDIT10, OnEnKillFocusEdit10)
	ON_EN_KILLFOCUS(IDC_EDIT11, OnEnKillFocusEdit11)
	ON_EN_KILLFOCUS(IDC_EDIT12, OnEnKillFocusEdit12)
	ON_EN_KILLFOCUS(IDC_EDIT13, OnEnKillFocusEdit13)
	ON_EN_KILLFOCUS(IDC_EDIT14, OnEnKillFocusEdit14)
	ON_EN_KILLFOCUS(IDC_EDIT15, OnEnKillFocusEdit15)
	ON_EN_KILLFOCUS(IDC_EDIT16, OnEnKillFocusEdit16)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
END_MESSAGE_MAP()

/* ---->

char * rtxt(CEdit feld)
{	char *S ;
	 int len = feld.GetLine(0,S) ;
	 if ( len )	return  S ;
	 return ( char *) 0 ;
}
< ---- */

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		bufi[0] = '\0' ;
		return TRUE ;
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil2 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}

// Crange-Meldungshandler
static char bufh[512] ;

void Crange::OnEnKillFocusEdit1()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 1 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit1.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit1.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

/* ---> Mustereintrag

void Crange::OnEnChangeEdit2()
{
}
< ---- */

void Crange::OnEnKillFocusEdit2()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 2 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit2.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit2.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit3()
{

	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 3 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE);
				i = m_cedit3.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit3.Format("%s",_T(bufh));
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}

	}
	return ;

}

void Crange::OnEnKillFocusEdit4()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 4 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit4.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit4.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}


void Crange::OnEnKillFocusEdit5()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 5 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit5.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit5.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit6()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 6 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit6.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit6.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit7()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 7 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit7.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit7.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit8()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 8 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit8.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit8.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit9()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 1 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit9.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit9.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit10()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 2)
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit10.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit10.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit11()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 3 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit11.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit11.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit12()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 4 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit12.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit12.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit13()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 5 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit13.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit13.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit14()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 6 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit14.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit14.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit15()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 7 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit15.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit15.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

void Crange::OnEnKillFocusEdit16()
{
	for (int i = 0 ; i < MAXRANGE ; i++ )
	{
		if ( tt_range[i] == 8 )
		{

			switch (tt_datetyp[i])
			{
			case iDBDATUM :
				UpdateData(TRUE) ;
				i = m_cedit16.GetLine(0,bufh,500) ;
				if (i)
				{
					if (datumcheck(bufh))
					{
						m_vcedit16.Format("%s",_T(bufh));
						
						UpdateData (FALSE) ;
					}
				}
				break ;
			}
			return ;
		}
	}
}

int isnumeric ( int datetyp, char * rangex )
{
// Bei Datentyp iDBINTEGER und iDBSMALLINT wird nach Komma gekappt !!
	int retco = 1 ;
	int j = (int) strlen ( rangex ) ;
	for ( int i = 0 ;i<j; i++ )
// Leerstring ist zunaechst mal gueltig, wird aber anders als String "0" gehandelt
	{
		if ( rangex[i] > '9' || rangex[i] < '0' )
		{
			if ( rangex[i] == ',' )	rangex [i] = '.' ;
			else
			{
				if ( rangex[i] != ' ' )
				{
					retco = 0 ;
					break ;
				}
			}

		}
	}
	long wertx ;
	if ( retco )
	{			// 110205 : bis heute Syntaxfehler ...
		if (datetyp == iDBSMALLINT) 
		{
			wertx = atol(rangex) ;
			if (wertx > 32000  ||wertx < -32000  )
				retco = 0 ;
		}
		if (datetyp == iDBINTEGER)
		{
			wertx = atol(rangex) ;
			if (wertx > 99999999L || wertx < -99999999L  )
				retco = 0 ;
		}
	}
	return retco ;
}

int macherange ( int i, char* range1, char * range2 )
{

	int retco = 0 ;
	switch ( tt_datetyp[i])
	{
	case (iDBCHAR) :
		if ( strlen(range1) > 49 )	range1[49] = '\0' ;
		if ( strlen(range2) > 49 )	range2[49] = '\0' ;

// Beachte immer den Unterschied zw. leerem String und string mit Laenge 0 
		if ( strlen(range1) == 0 || strlen(range2) == 0 )
		{
			if ( strlen(range1) > 0 ) sprintf ( range2, "%s", range1 );
			else
			{	
				if ( strlen(range2) > 0 ) sprintf ( range1, "%s", range2 );
				else
				{ 
					tt_cvrange[tt_range[i]][0] = -1 ; 
					tt_cbrange[tt_range[i]][0] = -1 ;
					break ;
				}
			}
		};
		
		if (! strcmp(clipped( range1),clipped(range2)))
		{
			sprintf ( tt_cvrange[tt_range[i]], "%s", clipped(range1));
			tt_cbrange[tt_range[i]][0] = -1 ;
			break ;
		}

		sprintf ( tt_cvrange[tt_range[i]], "%s", clipped(range1));
		sprintf ( tt_cbrange[tt_range[i]], "%s", clipped(range2));
		break ;
	case (iDBSMALLINT) :
	case (iDBINTEGER) :
		if ( strlen(range1) > 49 )	range1[49] = '\0' ;
		if ( strlen(range2) > 49 )	range2[49] = '\0' ;

		if ( !isnumeric( tt_datetyp[i], range1 )||
			 !isnumeric( tt_datetyp[i], range2 ) )
		{
			range1[0] = '\0' ;
			range2[0] = '\0' ;
		}

		// leerer Wert ist ungueltiger Wert
		if ( strlen(range1) == 0 || strlen(range2) == 0 )
		{	
			if ( strlen(range1) > 0 ) sprintf ( range2, "%s", range1 );
			else
			{	
				if ( strlen(range2) > 0 ) sprintf ( range1, "%s", range2 );
				else
				{ 
					tt_lvrange[tt_range[i]] =  11 ;
					tt_lbrange[tt_range[i]] = -11 ;
					break ;
				}
			}
		}

		tt_lvrange[tt_range[i]]= atol (range1);
		tt_lbrange[tt_range[i]]= atol (range2);
		break ;


	case (iDBDECIMAL) :
		if ( strlen(range1) > 49 )	range1[49] = '\0' ;
		if ( strlen(range2) > 49 )	range2[49] = '\0' ;

		if ( !isnumeric( tt_datetyp[i], range1 )||
			 !isnumeric( tt_datetyp[i], range2 ) )
		{
			range1[0] = '\0' ;
			range2[0] = '\0' ;
		}

		// leerer Wert ist ungueltiger Wert
		if ( strlen(range1) == 0 || strlen(range2) == 0 )
		{	
			if ( strlen(range1) > 0 ) sprintf ( range2, "%s", range1 );
			else
			{	
				if ( strlen(range2) > 0 ) sprintf ( range1, "%s", range2 );
				else
				{ 
					tt_dvrange[tt_range[i]] =  11 ;
					tt_dbrange[tt_range[i]] = -11 ;
					break ;
				}
			}
		}

		tt_dvrange[tt_range[i]]= atof (range1);
		tt_dbrange[tt_range[i]]= atof (range2);
		break ;

	case (iDBDATUM) :	// hier sollten nur noch gueltige Dati ankommen ....
		if ( strlen(range1) > 49 )	range1[49] = '\0' ;
		if ( strlen(range2) > 49 )	range2[49] = '\0' ;

		if ( strlen(range1) == 0 || strlen(range2) == 0 )
		{
			if ( strlen(range1) > 0 ) sprintf ( range2, "%s", range1 );
			else
			{	
				if ( strlen(range2) > 0 ) sprintf ( range1, "%s", range2 );
				else
				{ 
					tt_cvrange[tt_range[i]][0] = -1 ; 
					tt_cbrange[tt_range[i]][0] = -1 ;
					break ;
				}
			}
		};



		if (! strcmp(clipped( range1),clipped(range2)))
		{
			sprintf ( tt_cvrange[tt_range[i]], "%s", clipped(range1));
			tt_cbrange[tt_range[i]][0] = -1 ;
			break ;
		}

		sprintf ( tt_cvrange[tt_range[i]], "%s", clipped(range1));
		sprintf ( tt_cbrange[tt_range[i]], "%s", clipped(range2));
		break ;


	}

	return retco ;
}

void Crange::OnBnClickedOk()
{

	int retco ;
	char buf1[512] ;
	char buf2[512] ;
	UpdateData(TRUE) ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i ++ )
	{
		if ( ! tt_range[i] ) continue ;

		switch (tt_range[i])
		{
		case 1 :
			m_cedit1.GetLine(0,buf1,500);
			m_cedit9.GetLine(0,buf2,500);
			retco = macherange( i,buf1,buf2 );
			break ;
		case 2 :
			m_cedit2.GetLine(0,buf1,500);
			m_cedit10.GetLine(0,buf2,500);
			retco = macherange( i,buf1,buf2 );

			break ;
		case 3 :
			m_cedit3.GetLine(0,buf1,500);
			m_cedit11.GetLine(0,buf2,500);
			retco = macherange( i,buf1,buf2 );
			break ;
		case 4 :
			m_cedit4.GetLine(0,buf1,500);
			m_cedit12.GetLine(0,buf2,500);
			retco = macherange( i,buf1,buf2 );
			break ;
		case 5 :
			m_cedit5.GetLine(0,buf1,500);
			m_cedit13.GetLine(0,buf2,500);
			retco = macherange( i,buf1,buf2 );
			break ;
		case 6 :
			m_cedit6.GetLine(0,buf1,500);
			m_cedit14.GetLine(0,buf2,500);
			retco = macherange( i,buf1,buf2 );
			break ;
		case 7 :
			m_cedit7.GetLine(0,buf1,500);
			m_cedit15.GetLine(0,buf2,500);
			retco = macherange( i,buf1,buf2 );
			break ;
		case 8 :
			m_cedit8.GetLine(0,buf1,500);
			m_cedit16.GetLine(0,buf2,500);
			retco = macherange( i,buf1,buf2 );
			break ;
		}

	}

	v_abbruch = 0 ;
	CDialog::OnOK();
}

void Crange::OnBnClickedCancel()
{
	v_abbruch = 1 ;
		CDialog::OnCancel();
}

int Crange::getabbruch () 
{
	return v_abbruch ;
}
int Crange::getanzahl () 
{
	return m_vcedit26 ;
}
