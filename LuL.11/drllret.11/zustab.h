#ifndef _ZUSTAB_DEF
#define _ZUSTAB_DEF

struct SYS_PAR {

char sys_par_nam[21];	// Texte lieber zu lang als zu kurz beim lesen
char sys_par_wrt[3];
char sys_par_besch[37];

long zei;
short delstatus;
};
extern struct SYS_PAR sys_par, sys_par_null;


class SYS_PAR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//              int dbcount (void);
               int lesesys_par (int);
               int opensys_par (void);
               SYS_PAR_CLASS () : DB_CLASS ()
               {
               }
};

struct A_ZUS_TXT {

double a;
long zei;
short sg1;
char txt[61];
};
extern struct A_ZUS_TXT a_zus_txt, a_zus_txt_null;


class A_ZUS_TXT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//              int dbcount (void);
               int lesea_zus_txt (int);
               int opena_zus_txt (void);
               A_ZUS_TXT_CLASS () : DB_CLASS ()
               {
               }
};

struct A_KUN {

short	mdn ;
short	fil ;
long	kun ;
double	a ;
char	a_kun[14] ;
char	a_bz1[25] ;
short	me_einh_kun ;
double	inh ;
char	kun_bran2[3] ;
double	tara ;
double	ean ;
char	a_bz2[25] ;
short	hbk_ztr ;
long	kopf_text ;
char	pr_rech_kz[2] ;
char	modif[2] ;
long	text_nr ;
short	devise ;
char	geb_eti[2] ;
char	geb_fill[2] ;
long	geb_anz ;
char	pal_eti[2] ;
char	pal_fill[2]  ;
short	pal_anz ;
char	pos_eti[2] ;
short	sg1 ;
short	sg2 ;
short	pos_fill ;
short	ausz_art ;
short	text_nr2 ;
short	cab ;
char	a_bz3[25] ;
char	a_bz4[25] ;
char	li_a[13] ;
double	geb_fakt ;

};
extern struct A_KUN a_kun, a_kun_null;


class A_KUN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_kun (int);
               int opena_kun (void);
               A_KUN_CLASS () : DB_CLASS ()
               {
               }
};

#endif

