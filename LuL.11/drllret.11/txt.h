#ifndef _TXT_DEF
#define _TXT_DEF

#define MYMAXRANGE 10

struct PTXT {
long nr;
long zei;
char txt[101];	// ich gehe mal davon aus, das eine Ziele nie groesser als 100 Zeichen ist

};
extern struct PTXT ptxt, ptxt_null;

struct NTXT {
short mdn;
short fil;
long ls;
long posi;
long zei;
char txt[101];	// ich gehe mal davon aus, das eine Zeile nie groesser als 100 Zeichen ist

};
extern struct NTXT ntxt, ntxt_null;

extern char * clipped( char *) ;

class PTXT_CLASS : public DB_CLASS
{
       private :
               void prepare (char *);
       public :
//               int dbcount (void);
               int leseptxt (void);
               int openptxt (char *);
			   char  ctabnmatr[MYMAXRANGE][25] ;			// 111110 mal so lang wie "nform_feld.krz_txt"
			   short ctabimatr[MYMAXRANGE] ;				// 111110
			   int suchereadcursor (char *ctabnam) ;	// 111110
			   PTXT_CLASS () : DB_CLASS ()
               {
					// 111110  
					for ( int i = 0 ; i < MYMAXRANGE ; i ++ )
					{
						ctabnmatr[i][0] = '\0' ;
						ctabimatr[i]    =  -1  ;
					}
               }
};

class NTXT_CLASS : public DB_CLASS
{
       private :
               void prepare (char *);
       public :
               int lesentxt (void);
               int openntxt (char *);
			   char  ctabnmatr[MYMAXRANGE][25] ;			// 111110 mal so lang wie "nform_feld.krz_txt"
			   short ctabimatr[MYMAXRANGE] ;				// 111110
			   int suchereadcursor (char *ctabnam) ;	// 111110
               NTXT_CLASS () : DB_CLASS ()
               {
					// 111110  
					for ( int i = 0 ; i < MYMAXRANGE ; i ++ )
					{
						ctabnmatr[i][0] = '\0' ;
						ctabimatr[i]    =  -1  ;
					}
               }
};


#endif
