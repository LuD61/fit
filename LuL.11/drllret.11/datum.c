
#include "stdafx.h"

/***
--------------------------------------------------------------------------------
-
-       SE.TEC
-       Unternehmensberatung fuer systemische Entwicklung GmbH
-       Zeppelinstr 4
-
-       72189 V�hringen
-
-       Tel.: 07454/9667-0     Fax: 07454/9667-200
-       E-Mail info@setec-gmbh.de
-
--------------------------------------------------------------------------------

 Fuer dieses Programm behalten wir uns alle Rechte, auch fuer den Fall der
 Patenterteilung und der Eintragung eines anderen gewerblichen Schutz-
 rechtes vor. Missbraeuchliche Verwendung, wie insbesondere Vervielfael-
 tigung und Weitergabe an Dritte ist nicht gestattet; sie kann zivil- und
 strafrechtlich geahndet werden.

--------------------------------------------------------------------------------
-
-       Modulname               :       datum.c
-
-       Autor                   :       F.Folmer
-       Erstellungsdatum        :       05.10.93
-       Modifikationsdatum      :       21.01.97
-
-       Projekt                 :       BIWAK
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :
-       Betriebssystem          :       SCO-UNIX/OS4680/DOS
-       Sprache                 :       C
-
-       Modulbeschreibung       :	HILFSFUNKTIONEN FUER DATUM.
-				
        11.01.2000                FORMAT: TTMMJJJJ in der Funktion dlongtochar()
--------------------------------------------------------------------------------

short get_jahr(char * datum)             Funktion liefert jahr (short)
short get_monat(char * datum)            Funktion liefert monat(short)
short get_tag(char * datum)              Funktion liefert tag (short)
short get_lauf_tag(char * datum)         Funktion liefert laufenden Tag im Jahr
short get_schalt_jahr(short jahr)        Funktion liefert 1/0 wenn Schalt-Jahr
short get_wochentag(char * datum)        Funktion liefert Wochentag
short get_woche(char * datum)            Funktion liefert Kalenderwoche im Jahr
long tages_datum_zeit(zeit,par,ausgabe)  Funktion liefert Tagesdatum
long get_dat_long(datum,len)
char * get_dat_char(string,dat_long,len)
char * get_zeit_char(string,wert,len)
long get_zeit_long(zeit, len)                      siehe  unten
long dchartolong(datum)
char * dlongtochar(string,format,dat_long)
long zchartolong(zeit)
char * zlongtochar(string,format,wert)

--------------------------------------------------------------------------------
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       1.00    04.09.91	A.Folmer  Grundmodul
--------------------------------------------------------------------------------
***/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* prototyp */
short get_jahr(char *);
short get_monat(char *);
short get_woche(char *);
short get_tag(char *);
short get_lauf_tag(char *);
short get_wochentag(char *);
short get_schalt_jahr(short);
double zeit_yo_dec(char *);
static short woche_korr(short,short);
static short woche_pre(char *);

#define   NEU   (short)1
#define   ALT   (short)2

short mon_tab[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};/* in get_lauf_tag()*/


/************* Funktionen **************/

/***************************************/
/*    Funkton liefert Jahr (short)     */
/*                  Jahr nur 2 Stellen */
/*-------------------------------------*/
/*    Format des Datums: TTMMJJJJ      */
/*                       TT.MM.JJ      */
/*                       TT.MM.JJJJ    */
/***************************************/
short get_jahr(datum)
char * datum;
{
char  dat[16];
strcpy(dat,datum);
if(strlen(dat) == 8)
      return((short)(atoi(&dat[6])));
else
      return((short)(atoi(&dat[8])));
}

/***************************************/
/*    Funkton liefert Monat(short)     */
/*-------------------------------------*/
/*    Format des Datums: TT.MM.JJ      */
/*                       TT.MM.JJJJ    */
/***************************************/
short get_monat(datum)
char * datum;
{
char  dat[16];
strcpy(dat,datum);
dat[5]='\0';
return((short)(atoi(&dat[3])));
}



/***************************************/
/*    Funkton liefert tag (short)      */
/*-------------------------------------*/
/*    Format des Datums: TT.MM.JJ      */
/*                       TTMMJJJJ      */
/*                       TT.MM.JJJJ    */
/***************************************/
short get_tag(datum)
char * datum;
{
char  dat[16];
strcpy(dat,datum);
dat[2]='\0';
return((short)(atoi(dat)));
}

/********************************************/
/*    Funkton liefert laufenden Tag im Jahr */
/*------------------------------------------*/
/*    Format des Datums: TT.MM.JJ           */
/*                       TT.MM.JJJJ         */
/********************************************/
short get_lauf_tag(datum)
char * datum;
{
short i,l_tag=0,tag,mon;
mon=get_monat(datum);
tag=get_tag(datum);
for(i=1;i<mon;i++)
    {
    l_tag += mon_tab[i];
    }
if(mon > 2)
     l_tag += tag + get_schalt_jahr(get_jahr(datum));
else
     l_tag += tag;

return(l_tag);
}

/********************************************/
/*    Funkton liefert 1/0 wenn Schalt-Jahr  */
/********************************************/
short get_schalt_jahr(short jahr)   /* jahr 2-Stellig */
{
short i;
i=jahr%4;
if(i == 0)
    return(1);
else
    return(0);
}

/********************************************/
/*    Funkton liefert Wochentag             */
/*          Basis  01.01.1990               */
/*    1 = Montag .. 7 = Sonntag             */
/*------------------------------------------*/
/*    Format des Datums: TT.MM.JJ           */
/*                       TT.MM.JJJJ         */
/********************************************/
short get_wochentag(datum)
char * datum;
{
short jahr;
short l_tag;
short wochentag;
short i;
int   rest=0;
int   tage=0;

jahr = get_jahr(datum);
if(jahr < 90 )
	jahr += 100;
for(i=90;i<jahr;i++)
	{
	rest = (int)(i % 4);
	if(rest != 0)
		tage += 365;
	else
		tage += 366;
	}
tage += (int)get_lauf_tag(datum);

wochentag = (short)(tage % 7);
if(wochentag == 0)
	return(7);
return(wochentag);
}/*end of get_wochentag() */
/**********************************************/
/*    Funkton liefert Kalenderwoche im Jahr   */
/*           52(53)     vom Vorjahr           */
/*           1 ..52(53) im Jahr               */
/*           1          fuer Neujahr(im Dez)  */
/*------------------------------------------*/
/*    Format des Datums: TT.MM.JJ           */
/*                       TT.MM.JJJJ         */
/**********************************************/
short get_woche(datum)
char * datum;
{
short woche;
short jahr;
char  buff[16];

woche = woche_pre(datum);
if(woche == 0) /* Korrektur bei Anfang des Jahres */
	{
	jahr = get_jahr(datum);
	if(jahr == 0)
		jahr = 99;
	else
		jahr--;
	sprintf(buff,"31.12.%02d",jahr);
        woche = woche_pre(buff);
	}
return(woche);
}/* end of get_woche() */

/****static Funktionen fuer get_woche********/
/****static get_woche_pre********************/
static short woche_pre(datum)
char * datum;
{
short woche,jahr,rest;
short korr_neu;
short l_tag;

l_tag = get_lauf_tag(datum);
jahr = get_jahr(datum);
korr_neu = woche_korr(jahr,NEU);
woche = (short)(l_tag / 7);
rest  = (short)(l_tag % 7);

if(rest == 0)
	{
	woche--;
	rest = 7;
	}
if(korr_neu <= 4)
	woche++;
if(korr_neu > 1)
	if(rest >= ((short)9 - korr_neu))
		woche++;
if(woche == 53)
	{
	if(woche_korr(jahr,ALT) <= 3)
		woche = 1;
	}
if(woche == 0)
	{
	}
return(woche);
} /* end of static woche_pre() */

/***********static woche_korr***************/
/*          Basis = 01.01.90               */

static short woche_korr(short jahr_ein,short par)
{
short jahr;
short l_tag;
short wochentag;
short i;
int   rest=0;
int   tage=0;

jahr = jahr_ein;
if(par == ALT) /* Ende des Jahres */
	jahr++;
if(jahr < 90 )
	jahr += 100;

for(i=90;i<jahr;i++)
	{
	rest = (int)(i % 4);
	if(rest != 0)
		tage += 365;
	else
		tage += 366;
	}
if(par == NEU) /* Anfang des Jahres */
	tage++;

wochentag = (short)(tage % 7);
if(wochentag == 0)
	return(7);

return(wochentag);
}/* end of static woche_korr() */

/***************** zeit_to_dec ********************/
/*    min in decimal Darstellung                  */

double zeit_to_dec(zeit)
char * zeit;
{
char zwi[8];
double arb_double;

strcpy(zwi,zeit);

zwi[2] = '\0';
arb_double = (double)((double)atol(&zwi[3]) / 60.0) + (double)atol(zwi);
return (arb_double);
} /* end of zeit_to_dec */


/**************************************************************************/
/*              int tages_datum_zeit(long zeit,short par,char * ausgabe)  */
/*------------------------------------------------------------------------*/
/*  Prozedure rechnet Datum und Uhrzeit aus der Anzahl Sekunden           */
/*  seit 01.01.1970, die in der Variable zeit �bergeben sind.          */
/*          wenn par=0  liefert Datum: tt.mm.yyyy  im ausgabe             */
/*          wenn par=1  liefert Datum: tt.mm.yy    im ausgabe             */
/*          wenn par=2  liefert Zeit : hh:mm:ss    im ausgabe             */
/*          wenn par=3  liefert Zeit : hh:mm       im ausgabe             */
/*          wenn par=4  liefert letzte Tages-Ziffer im return und ausgabe */
/*          wenn par=5  liefert Zeit : hhmmss      im ausgabe             */
/*          wenn par=6  liefert Datum: ttmmyyyy    im ausgabe             */
/*          wenn par=7  liefert Datum: ttmmyy      im ausgabe             */
/**************************************************************************/

long tages_datum_zeit(zeit,par,ausgabe)
long zeit;
short par;
char * ausgabe;
{
long int arb;
int  i,jahr,mon,schal,tag,std,min,sek;

arb = (long int)(zeit - 725846400);
for(i=1;;i++)
   {
   if((i % 4) == 0)
      {
      if(arb < 31622400)
        {
        schal = 1;
        break;
        }
      arb = arb - 31622400;
      continue;
      }
   schal = 0;
   if(arb < 31536000)
        break;
   arb = arb - 31536000;
   }
jahr = 1992 + i;
for(i=1;i<=12;i++)
   {
   if(i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)
      {
      if(arb < 2678400)
        break;
      arb = arb - 2678400;
      continue;
      }
   if(i == 2)
      {
      if(arb < (2419200 + schal * 86400))
        break;
      arb = arb - (2419200 + schal * 86400);
      continue;
      }
   if(arb < 2592000)
        break;
   arb = arb - 2592000;
   }
mon = i;
for(i=1;;i++)
   {
   if(arb < 86400)
        break;
   arb = arb - 86400;
   }
tag = i;

/****** return letzte Ziffer tages_datum *****/
if (par == 4)
   {
   sprintf(ausgabe,"%1d",tag % 10);
   return (tag % 10);
   }
/****** tages_datum *****/
if (par == 0)
   {
   sprintf(ausgabe,"%02d.%02d.%4d",tag,mon,jahr);
   return (0);
   }
if (par == 1)
   {
   sprintf(ausgabe,"%02d.%02d.%2d",tag,mon,jahr%100);
   return (0);
   }
if (par == 6)
   {
   sprintf(ausgabe,"%02d%02d%4d",tag,mon,jahr);
   return (0);
   }
if (par == 7)
   {
   sprintf(ausgabe,"%02d%02d%2d",tag,mon,jahr%100);
   return (0);
   }

for(i=0;;i++)
   {
   if(arb < 3600)
        break;
   arb = arb - 3600;
   }
std = i;
for(i=0;;i++)
   {
   if(arb < 60)
        break;
   arb = arb - 60;
   }
min = i;
sek = (int)arb;
if(par == 2)
   {
   sprintf(ausgabe,"%02d:%02d:%02d",std,min,sek);
   return (0);
   }
if(par == 3)
   {
   sprintf(ausgabe,"%02d:%02d",std,min);
   return (0);
   }
if(par == 5)
   {
   sprintf(ausgabe,"%02d%02d%02d",std,min,sek);
   return (0);
   }
return (-1);
} /* end of teges_datum_zeit() */

/**********************************************************/
/*               get_dat_long()                           */
/*--------------------------------------------------------*/
/*  Funktion liefert long als Anzahl Tage seit 01.01.1970 */
/*--------------------------------------------------------*/
/*  Bereich fuer Datum:  01.01.1970 bis 31.12.2069        */
/*--------------------------------------------------------*/
/*  Parameter:     char * datum         Datum als String  */
/*                 int  laenge          Laenge des Datums */
/*--------------------------------------------------------*/
/*  Eingabe-Format fuer Datum:    TTMMJJ        Laenge 6  */
/*                                TTMMJJJJ      Laenge 8  */
/*                                TT.MM.JJ      Laenge 8  */
/*                                TT.MM.JJJJ    Laenge 10 */
/*                                today +-XX    Laenge 10 */
/*--------------------------------------------------------*/
/*  Return:        long (-1)            falsche Eingabe   */
/**********************************************************/
long get_dat_long(datum,laenge)
char * datum;
int  laenge;
{
int  len;
long  wert=0;
long  today=0;
short jahr=0;
short monat=0;
short tag=0;
int   i;
char dat[12];

len=laenge;
memset(dat,0x00,12);
memcpy(dat,datum,len);

if(strncmp(dat,"today",5) == 0)
    {
    today=atol(&dat[5]);
    sysdate(dat);
    len=10;
    }

switch(len)
   {
   case 6:
   case 8:
	  if(dat[2] != '.' || len == 6)
	     {
	     dat[2]='.';
	     memcpy(&dat[3],(datum+2),2);
	     dat[5]='.';
	     memcpy(&dat[6],(datum+4),(len-4));
	     }
          break;
   case 10:
          break;
   default:
	  return (-1);
          break;
   }

jahr = get_jahr(dat);

/* Kontrolle des Datums A */
if(strlen(dat) == 10)
   {
   i = atoi(&dat[6]);
   if(i > 2069 || i < 1970)
          return(-1);
   }
monat = get_monat(dat);
if(monat < 1 || monat > 12)
    return(-1);
tag = get_tag(dat);
if(monat == 2)
    {
    if(tag > (short)(mon_tab[2] + get_schalt_jahr(jahr)))
        return(-1);
    }
else
    {
    if(tag > (short)mon_tab[(int)monat])
        return(-1);
    }
/* Kontrolle des Datums E */

if(jahr < 70)
     jahr += 2000;
else
     jahr += 1900;

wert = (long) get_lauf_tag(dat);

for(i=(int)(jahr - 1); i >= 1970; i--)
     {
     wert += (long)(365 + get_schalt_jahr((short) i));
     }
wert += today;
return wert;
} /* end of get_dat_long() */



/**********************************************************/
/*               get_dat_char()                           */
/*--------------------------------------------------------*/
/*  Funktion liefert Datum als String                     */
/*--------------------------------------------------------*/
/*  Bereich fuer Datum:  01.01.1970 bis 31.12.2069        */
/*--------------------------------------------------------*/
/*  Parameter:  char * string   Ausgabe fuer Datum        */
/*              long dat_long   Anz. Tage seit 01.01.1970 */
/*              int  len        Laenge des Datums         */
/*--------------------------------------------------------*/
/*  Ausgabe-Format fuer Datum:  TTMMJJ     fuer Laenge 6  */
/*                              TT.MM.JJ   fuer Laenge 8  */
/*                              TT.MM.JJJJ fuer Laenge 10 */
/*--------------------------------------------------------*/
/*  Return:     "-1"            falsche Eingabe           */
/**********************************************************/
char * get_dat_char(string,dat_long,len)
char * string;
long dat_long;
int  len;
{
long wert;
int  jahr;
long schalt;
int  i;

wert = dat_long;

if(wert == (-1))
	{
	sprintf(string,"-1");
        return string;
	}

for(i=1970; ;i++)
   {
   schalt = (long) get_schalt_jahr((short) i);
   if(wert <= (long)(365 + schalt))
        {
	jahr = i;
	break;
	}
   wert = wert - (long)365 - schalt;
   }

for(i=1; i<12 ;i++)
   {
   if(i != 2)
       {
       if(wert <= (long) mon_tab[i])
            break;
       }
   else
       {
       schalt = (long) get_schalt_jahr((short) jahr);
       if(wert <= (long) (mon_tab[i] + schalt))
	    break;
       wert -= schalt;
       }
   wert -= (long) mon_tab[i];
   }

switch(len)
   {
   case 6:
 	  sprintf(string,"%02d%02d%02d",(int)wert,i,(jahr%100));
	  break;
   case 8:
	  sprintf(string,"%02d.%02d.%02d",(int)wert,i,(jahr%100));
	  break;
   case 10:
	  sprintf(string,"%02d.%02d.%4d",(int)wert,i,jahr);
	  break;
   default:
	  sprintf(string,"-1");
	  break;
   }
return string;
} /* end of get_dat_char() */


/**********************************************************/
/*               get_zeit_long()                          */
/*--------------------------------------------------------*/
/*  Funktion liefert long als Anzahl Minuten              */
/*                                                        */
/*--------------------------------------------------------*/
/*  Parameter:     char * zeit          Zeit als String   */
/*                 int  len             Laenge des Strings*/
/*--------------------------------------------------------*/
/*  Eingabe-Format fuer Zeit:  HHMM      Laenge 4         */
/*                             HH:MM     Laenge 5         */
/*                             HH.MM     Laenge 5         */
/*                             HHMMSS    Laenge 6         */
/*                             HH:MM:SS  Laenge 8         */
/*                             HH.MM.SS  Laenge 8         */
/*--------------------------------------------------------*/
/*  Return:        long (-1)            falsche Eingabe   */
/**********************************************************/
long get_zeit_long(zeit, len)
char * zeit ;
int  len;
{
long minute=0;
long stunde=0;
long sekunde=0;
char dat[12];

memcpy(dat,zeit,len);

switch(len)
     {
     case 4:
            dat[4] = '\0';
	    sekunde = 0;
	    break;
     case 5:
            dat[2] = 0x30;
            dat[5] = '\0';
	    sekunde = 0;
	    break;
     case 6:
            dat[6] = '\0';
	    sekunde = (short)atoi(&dat[4]);
            dat[4] = '\0';
	    break;
     case 8:
	    if(dat[2]>='0' && dat[2]<='9' || dat[5]>='0' && dat[5]<='9')
                      return (-1);
            dat[8] = '\0';
	    sekunde = (short)atoi(&dat[6]);
            dat[2] = 0x30;
            dat[5] = '\0';
	    break;
     default:
            return (-1);
	    break;
     }
if(sekunde > 60)
     return (-1);

minute = atol(&dat[2]);
if(minute > 60)
     return (-1);

dat[2] = '\0';
stunde = atol(dat);
if(stunde > 24)
     return (-1);
return (long)(stunde * 3600 + minute * 60 + sekunde);
} /* end of get_zeit_long() */


/**********************************************************/
/*               get_zeit_char()                          */
/*--------------------------------------------------------*/
/*  Funktion liefert Zeit  als String                     */
/*                                                        */
/*--------------------------------------------------------*/
/*  Parameter:  char * string   Ausgabe fuer Zeit         */
/*              long wert       Anzahl Minuten im Tag     */
/*              int  len        Laenge des Strings        */
/*--------------------------------------------------------*/
/*  Ausgabe-Format fuer Zeit:   HHMM       fuer  Laenge 4 */
/*                              HH:MM      fuer  Laenge 5 */
/*                              HHMMSS     fuer  Laenge 6 */
/*                              HH:MM:SS   fuer  Laenge 8 */
/*--------------------------------------------------------*/
/*  Return:     "-1"            falsche Eingabe           */
/**********************************************************/
char * get_zeit_char(string,wert,len)
char * string;
long wert;
int  len;
{
long stunde;
long minute;
long sekunde;

if(wert > (long)86400 || wert == (-1))
    {
    sprintf(string,"-1");
    return string;
    }
stunde = wert/3600;
minute = (wert - stunde*3600)/60;
sekunde = wert - stunde*3600 - minute * 60;
switch(len)
    {
    case 4:
        sprintf(string,"%02ld%02ld",stunde,minute);
	break;
    case 5:
        sprintf(string,"%02ld:%02ld",stunde,minute);
	break;
    case 6:
        sprintf(string,"%02ld%02ld%02ld",stunde,minute,sekunde);
	break;
    case 8:
        sprintf(string,"%02ld:%02ld:%02ld",stunde,minute,sekunde);
	break;
    default:
	sprintf(string,"-1");
	break;
    }
return string;
} /* end of get_zeit_char() */

/**********************************************************/
/*               dchartolong()                           */
/*--------------------------------------------------------*/
/*  Funktion liefert long als Anzahl Tage seit 01.01.1970 */
/*--------------------------------------------------------*/
/*  Bereich fuer Datum:  01.01.1970 bis 31.12.2069        */
/*--------------------------------------------------------*/
/*  Parameter:     char * datum         Datum als String  */
/*                        today +-XX    moeglich          */
/*--------------------------------------------------------*/
/*  Eingabe-Format fuer Datum:    TTMMJJ                  */
/*                                TTMMJJJJ                */
/*                                TT.MM.JJ                */
/*                                TT.MM.JJJJ              */
/*                       (.) - belibige Trennzeichnen     */
/*--------------------------------------------------------*/
/*  Return:        long (-1)            falsche Eingabe   */
/**********************************************************/
long dchartolong(datum)
char * datum;
{
int  len;
long  wert=0;
long  today=0;
short jahr=0;
short monat=0;
short tag=0;
int   i;
char dat[12];

strcpy(dat,datum);
len=strlen(dat);

if(strncmp(dat,"today",5) == 0)
    {
    today=atol(&dat[5]);
    sysdate(dat);
    len=10;
    }
switch(len)
   {
   case 6:
   case 8:
	  if((dat[2] >= '0' && dat[2] <= '9') || len == 6)
	  if(dat[2] != '.' || len == 6)
	     {
	     dat[2]='.';
	     memcpy(&dat[3],(datum+2),2);
	     dat[5]='.';
	     memcpy(&dat[6],(datum+4),(len-4));
	     dat[(int)(len + 2)] = '\0';
	     }
          break;
   case 10:
          break;
   default:
	  return (-1);
          break;
   }

jahr = get_jahr(dat);
/* Kontrolle des Datums A */
if(strlen(dat) == 10)
   {
   i = atoi(&dat[6]);
   if(i > 2069 || i < 1970)
          return(-1);
   }
monat = get_monat(dat);
if(monat < 1 || monat > 12)
    return(-1);
tag = get_tag(dat);
if(monat == 2)
    {
    if(tag > (short)(mon_tab[2] + get_schalt_jahr(jahr)))
        return(-1);
    }
else
    {
    if(tag > (short)mon_tab[(int)monat])
        return(-1);
    }
/* Kontrolle des Datums E */

if(jahr < 70)
     jahr += 2000;
else
     jahr += 1900;

wert = (long) get_lauf_tag(dat);

for(i=(int)(jahr - 1); i >= 1970; i--)
     {
     wert += (long)(365 + get_schalt_jahr((short) i));
     }
wert += today;
return wert;
} /* end of dchartolong() */


/**********************************************************/
/*               dlongtochar()                           */
/*--------------------------------------------------------*/
/*  Funktion liefert Datum als String                     */
/*--------------------------------------------------------*/
/*  Bereich fuer Datum:  01.01.1970 bis 31.12.2069        */
/*--------------------------------------------------------*/
/*  Parameter:  char * string   Ausgabe fuer Datum        */
/*              char * format   Format fuer Ausgabe       */
/*              long dat_long   Anz. Tage seit 01.01.1970 */
/*--------------------------------------------------------*/
/*  Ausgabe-Format fuer Datum:  TTMMJJ                    */
/*                              TT.MM.JJ                  */
/*                              TTMMJJJJ                  */
/*                              TT.MM.JJJJ                */
/*                       (.) - belibige Trennzeichnen     */
/*--------------------------------------------------------*/
/*  Return:     "-1"            falsche Eingabe           */
/**********************************************************/
char * dlongtochar(string,format,dat_long)
char * string;
char * format;
long dat_long;
{
long wert;
int  jahr;
long schalt;
int  i;
int  len;

len = strlen(format);
wert = dat_long;

for(;;)
     {
     if(format[len-1] == ' ' || format[len-1] == '\n')
	  {
          format[len-1]='\0';
	  len = strlen(format);
          if(len <= 0)
	        break;
	  continue;
	  }
     else
	  break;
     }


if(wert == (-1))
	{
	sprintf(string,"-1");
        return string;
	}

for(i=1970; ;i++)
   {
   schalt = (long) get_schalt_jahr((short) i);
   if(wert <= (long)(365 + schalt))
        {
	jahr = i;
	break;
	}
   wert = wert - (long)365 - schalt;
   }

for(i=1; i<12 ;i++)
   {
   if(i != 2)
       {
       if(wert <= (long) mon_tab[i])
            break;
       }
   else
       {
       schalt = (long) get_schalt_jahr((short) jahr);
       if(wert <= (long) (mon_tab[i] + schalt))
	    break;
       wert -= schalt;
       }
   wert -= (long) mon_tab[i];
   }

switch(len)
   {
   case 6:
 	  sprintf(string,"%02d%02d%02d",(int)wert,i,(jahr%100));
	  break;
   case 8:
          /* Format: 01011999 TTMMJJJJ ttmmjjjj  11.01.2000 FF */
	  if(format[2] >= '0' && format[2] <= '9' ||
	     format[2] == 'M' || format[2] == 'm' )
	        sprintf(string,"%02d%02d%04d",(int)wert,i,jahr);
	  else
	        sprintf(string,"%02d%c%02d%c%02d",
	        (int)wert,format[2],i,format[5],(jahr%100));
	  break;
   case 10:
	  sprintf(string,"%02d%c%02d%c%4d",
	  (int)wert,format[2],i,format[5],jahr);
	  break;
   default:
	  sprintf(string,"-1");
	  break;
   }
return string;
} /* end of dlongtochar() */


/**********************************************************/
/*               zchartolong()                            */
/*--------------------------------------------------------*/
/*  Funktion liefert long als Anzahl Sekunden             */
/*                                                        */
/*--------------------------------------------------------*/
/*  Parameter:     char * zeit          Zeit als String   */
/*--------------------------------------------------------*/
/*  Eingabe-Format fuer Zeit:  HHMM                       */
/*                             HH:MM                      */
/*                             HHMMSS                     */
/*                             HH:MM:SS                   */
/*                       (:) - belibige Trennzeichnen     */
/*--------------------------------------------------------*/
/*  Return:        long (-1)            falsche Eingabe   */
/**********************************************************/
long zchartolong(zeit)
char * zeit ;
{
int len;
short minute=0;
short stunde=0;
short sekunde=0;
char dat[12];

strcpy(dat,zeit);
len = strlen(dat);
switch(len)
     {
     case 4:
	    sekunde = 0;
	    break;
     case 5:
            dat[2] = 0x30;
	    sekunde = 0;
	    break;
     case 6:
	    sekunde = (short)atoi(&dat[4]);
            dat[4] = '\0';
	    break;
     case 8:
	    if(dat[2]>='0' && dat[2]<='9' || dat[5]>='0' && dat[5]<='9')
                      return (-1);
	    sekunde = (short)atoi(&dat[6]);
            dat[2] = 0x30;
            dat[5] = '\0';
	    break;
     default:
            return (-1);
	    break;
     }
if(sekunde > 60)
     return (-1);

minute = (short)atoi(&dat[2]);
if(minute > 60)
     return (-1);

dat[2] = '\0';
stunde = (short)atoi(dat);
if(stunde > 24)
     return (-1);

return (long)(stunde * 3600 + minute * 60 + sekunde);
} /* end of zchartolong() */


/**********************************************************/
/*               zlongtochar()                          */
/*--------------------------------------------------------*/
/*  Funktion liefert Zeit  als String                     */
/*                                                        */
/*--------------------------------------------------------*/
/*  Parameter:  char * string   Ausgabe fuer Zeit         */
/*              char * format   Format fuer Ausgabe       */
/*              long wert       Anzahl Minuten im Tag     */
/*--------------------------------------------------------*/
/*  Ausgabe-Format fuer Zeit:   HHMM                      */
/*                              HH:MM                     */
/*                              HHMMSS                    */
/*                              HH:MM:SS                  */
/*                       (:) - belibige Trennzeichnen     */
/*--------------------------------------------------------*/
/*  Return:     "-1"            falsche Eingabe           */
/**********************************************************/
char * zlongtochar(string,format,wert)
char * string;
char * format;
long wert;
{
int  len;
long  stunde;
long  minute;
long  sekunde;

len = strlen(format);
if(wert > (long)86400 || wert == (-1))
    {
    sprintf(string,"-1");
    return string;
    }
stunde = wert/3600;
minute = (wert - stunde*3600)/60;
sekunde = wert - stunde*3600 - minute * 60;
switch(len)
    {
    case 4:
        sprintf(string,"%02ld%02ld",stunde,minute);
	break;
    case 5:
        sprintf(string,"%02ld%c%02ld",stunde,format[2],minute);
	break;
    case 6:
        sprintf(string,"%02ld%02ld%02ld",stunde,minute,sekunde);
	break;
    case 8:
        sprintf(string,"%02ld%c%02ld%c%02ld",
	stunde,format[2],minute,format[5],sekunde);
	break;
    default:
	sprintf(string,"-1");
	break;
    }
return string;
} /* end of zlongtochar() */
/************************Ende******************************/
