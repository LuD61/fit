#include "stdafx.h"
#include "DbClass.h"
#include "zustab.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

#include "temptab.h"	// 201213

struct SYS_PAR sys_par,  sys_par_null;
struct A_ZUS_TXT a_zus_txt, a_zus_txt_null;
struct A_KUN a_kun, a_kun_null;
struct A_KUN_GX a_kun_gx, a_kun_gx_null;	// 230609
struct A_KUN_TXT a_kun_txt, a_kun_txt_null;	// 201213

struct A_BAS_ERW a_bas_erw, a_bas_erw_null;	// 190214

extern DB_CLASS dbClass;

extern TEMPTAB_CLASS Temptab;

extern long get_lsk_kun(void);	// 201213


int SYS_PAR_CLASS::lesesys_par (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int SYS_PAR_CLASS::opensys_par (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

void SYS_PAR_CLASS::prepare (void)
{

	dbClass.sqlin ((char *) sys_par.sys_par_nam, SQLCHAR, 18);

	dbClass.sqlout ((char *) sys_par.sys_par_wrt,SQLCHAR, 2);
	dbClass.sqlout ((char *) sys_par.sys_par_besch,SQLCHAR, 33);
	dbClass.sqlout ((long *) &sys_par.zei, SQLLONG, 0);
	dbClass.sqlout ((short *) &sys_par.delstatus, SQLSHORT, 0);

		readcursor = dbClass.sqlcursor ("select "

	" sys_par_wrt, sys_par_besch, zei, delstatus " 
	" from sys_par where sys_par_nam = ? " ) ;
	

}

int A_KUN_CLASS::lesea_kun (int fehlercode)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_KUN_CLASS::opena_kun (void)
{
		if ( readcursor < 0 ) prepare ();
		
		return dbClass.sqlopen (readcursor);
}

void A_KUN_CLASS::prepare (void)
{

	dbClass.sqlin (( double	*) &a_kun.a,        SQLDOUBLE,0);
	dbClass.sqlin (( long	*) &a_kun.kun,      SQLLONG,  0);
	dbClass.sqlin (( char	*)   a_kun.kun_bran2,SQLCHAR,  3);
	dbClass.sqlin (( short	*) &a_kun.mdn,      SQLSHORT, 0);

	dbClass.sqlout (( short  *) &a_kun.mdn,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.fil,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( long	 *) &a_kun.kun,		SQLLONG,	 0 ) ;
	dbClass.sqlout (( double *) &a_kun.a,		SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.a_kun,	SQLCHAR,	14 ) ;
	dbClass.sqlout (( char	 *)  a_kun.a_bz1,	SQLCHAR,	25 ) ;
	dbClass.sqlout (( short	 *) &a_kun.me_einh_kun,SQLSHORT, 0 ) ;
	dbClass.sqlout (( double *) &a_kun.inh,		SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.kun_bran2,SQLCHAR,	 3 ) ;
	dbClass.sqlout (( double *) &a_kun.tara,	SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( double *) &a_kun.ean,		SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.a_bz2,	SQLCHAR,	25 ) ;
	dbClass.sqlout (( short	 *) &a_kun.hbk_ztr,	SQLSHORT,	 0 ) ;
	dbClass.sqlout (( long	 *) &a_kun.kopf_text,SQLLONG,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.pr_rech_kz,SQLCHAR,   2 ) ;
	dbClass.sqlout (( char	 *)  a_kun.modif,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( long	 *) &a_kun.text_nr,	SQLLONG,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.devise,	SQLSHORT,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.geb_eti,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( char	 *)  a_kun.geb_fill,SQLCHAR,	 2 ) ;
	dbClass.sqlout (( long	 *) &a_kun.geb_anz,	SQLLONG,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.pal_eti,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( char	 *)  a_kun.pal_fill,SQLCHAR,	 2 ) ;
	dbClass.sqlout (( short	 *) &a_kun.pal_anz,	SQLSHORT,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.pos_eti,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( short	 *) &a_kun.sg1,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.sg2,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.pos_fill,SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.ausz_art,SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.text_nr2,SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun.cab,		SQLSHORT,    0 ) ;
	dbClass.sqlout (( char	 *)  a_kun.a_bz3,	SQLCHAR,	25 ) ;
	dbClass.sqlout (( char	 *)  a_kun.a_bz4,	SQLCHAR,	25 ) ;
	dbClass.sqlout (( char	 *)  a_kun.li_a,	SQLCHAR,	13 ) ;
	dbClass.sqlout (( double *) &a_kun.geb_fakt,SQLDOUBLE,	 0 ) ;


		readcursor = dbClass.sqlcursor ("select "

	" mdn, fil, kun, a, a_kun, a_bz1, me_einh_kun, inh, kun_bran2, "
	" tara, ean, a_bz2, hbk_ztr, kopf_text, pr_rech_kz, modif, text_nr, "
	" devise, geb_eti, geb_fill, geb_anz, pal_eti, pal_fill, pal_anz, "
	" pos_eti, sg1,	sg2, pos_fill, ausz_art, text_nr2, cab,	a_bz3, a_bz4, "
	" li_a,	geb_fakt "

	" from a_kun where a = ? and kun = ? and kun_bran2 = ? and mdn = ? and fil = 0  " ) ;
		// 141009 : fil = 0  dazu 

}

// 230609 A
int A_KUN_GX_CLASS::lesea_kun_gx ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_KUN_GX_CLASS::opena_kun_gx (void)
{
		if ( readcursor < 0 ) prepare ();
		
		return dbClass.sqlopen (readcursor);
}

void A_KUN_GX_CLASS::prepare (void)
{

	dbClass.sqlin (( double	*) &a_kun_gx.a,        SQLDOUBLE,0);
	dbClass.sqlin (( long	*) &a_kun_gx.kun,      SQLLONG,  0);
	dbClass.sqlin (( char	*)  a_kun_gx.kun_bran2,SQLCHAR,  3);
	dbClass.sqlin (( short	*) &a_kun_gx.mdn,      SQLSHORT, 0);

	dbClass.sqlout (( short  *) &a_kun_gx.mdn,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun_gx.fil,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( long	 *) &a_kun_gx.kun,		SQLLONG,	 0 ) ;
	dbClass.sqlout (( double *) &a_kun_gx.a,		SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun_gx.a_kun,	SQLCHAR,	26 ) ;
	
	dbClass.sqlout (( char	 *)  a_kun_gx.a_bz1,	SQLCHAR,	25 ) ;
	dbClass.sqlout (( short	 *) &a_kun_gx.me_einh_kun,SQLSHORT, 0 ) ;
	dbClass.sqlout (( double *) &a_kun_gx.inh,		SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun_gx.kun_bran2,SQLCHAR,	 3 ) ;
	dbClass.sqlout (( double *) &a_kun_gx.tara,	SQLDOUBLE,	 0 ) ;

	dbClass.sqlout (( double *) &a_kun_gx.ean,		SQLDOUBLE,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun_gx.a_bz2,	SQLCHAR,	25 ) ;
	dbClass.sqlout (( short	 *) &a_kun_gx.hbk_ztr,	SQLSHORT,	 0 ) ;
	dbClass.sqlout (( long	 *) &a_kun_gx.kopf_text,SQLLONG,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun_gx.pr_rech_kz,SQLCHAR,   2 ) ;

	dbClass.sqlout (( char	 *)  a_kun_gx.modif,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( long	 *) &a_kun_gx.text_nr,	SQLLONG,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun_gx.devise,	SQLSHORT,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun_gx.geb_eti,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( short	 *) &a_kun_gx.geb_fill, SQLSHORT,	 0 ) ;

	dbClass.sqlout (( long	 *) &a_kun_gx.geb_anz,	SQLLONG,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun_gx.pal_eti,	SQLCHAR,	 2 ) ;
	dbClass.sqlout (( short	 *) &a_kun_gx.pal_fill, SQLSHORT,    0 ) ;
	dbClass.sqlout (( long	 *) &a_kun_gx.pal_anz,	SQLLONG,	 0 ) ;
	dbClass.sqlout (( char	 *)  a_kun_gx.pos_eti,	SQLCHAR,	 2 ) ;

	dbClass.sqlout (( short	 *) &a_kun_gx.sg1,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun_gx.sg2,		SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun_gx.pos_fill,SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun_gx.ausz_art,SQLSHORT,	 0 ) ;
	dbClass.sqlout (( short	 *) &a_kun_gx.text_nr2,SQLSHORT,	 0 ) ;

	dbClass.sqlout (( short	 *) &a_kun_gx.cab,		SQLSHORT,    0 ) ;


		readcursor = dbClass.sqlcursor ("select "

	"   mdn, fil, kun, a, a_kun "
	" , a_bz1, me_einh_kun, inh, kun_bran2, tara "
	" , ean, a_bz2, hbk_ztr, kopf_text, pr_rech_kz "
	" , modif, text_nr, devise, geb_eti, geb_fill "
	" , geb_anz, pal_eti, pal_fill, pal_anz, pos_eti "
	" , sg1, sg2, pos_fill, ausz_art, text_nr2 "
	" , cab "

	" from a_kun_gx where a = ? and kun = ? and kun_bran2 = ? and mdn = ? and fil = 0 " ) ;
	// 141009 : fil = 0 dazu 
}

// 230609 E

int A_ZUS_TXT_CLASS::lesea_zus_txt (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }
	  memcpy ( &a_zus_txt, &a_zus_txt_null , sizeof ( struct A_ZUS_TXT )) ;	// 290506
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_ZUS_TXT_CLASS::opena_zus_txt (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

void A_ZUS_TXT_CLASS::prepare (void)
{


	dbClass.sqlin ((double *) &a_zus_txt.a, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) a_zus_txt.txt,SQLCHAR, 61);
	dbClass.sqlout ((short *) &a_zus_txt.sg1,SQLSHORT, 0);
	dbClass.sqlout ((long *) &a_zus_txt.zei, SQLLONG, 0);

		readcursor = dbClass.sqlcursor ("select "

	" txt, sg1, zei " 
	" from a_zus_txt where a = ?  and zei > 0 order by zei" ) ;

}


// 201213 A

int A_KUN_TXT_CLASS::leseakuntxt ()
{
	a_kun_txt.a = (long)Temptab.a_bas_a();

	a_kun_txt.kun = get_lsk_kun() ;
	opena_kun_txt();
    int di = lesea_kun_txt() ;
	if ( di )
		memcpy (&a_kun_txt,&a_kun_txt_null, sizeof(struct A_KUN_TXT ));
	return di;
}


int A_KUN_TXT_CLASS::lesea_kun_txt ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_KUN_TXT_CLASS::opena_kun_txt (void)
{
		if ( readcursor < 0 ) prepare ();
		
		return dbClass.sqlopen (readcursor);
}

void A_KUN_TXT_CLASS::prepare (void)
{

	dbClass.sqlin (( long	*) &a_kun_txt.a,        SQLLONG, 0);
	dbClass.sqlin (( long	*) &a_kun_txt.kun,      SQLLONG, 0);

//	dbClass.sqlin (( char	*)  a_kun_gx.kun_bran2,SQLCHAR,  3);
//	dbClass.sqlin (( short	*) &a_kun_gx.mdn,      SQLSHORT, 0);

//	dbClass.sqlout (( short  *) &a_kun_txt.kun,		SQLLONG,	 0 ) ;

	dbClass.sqlout (( char	 *)  a_kun_txt.txt1,	SQLCHAR,	129 );
	dbClass.sqlout (( char	 *)  a_kun_txt.txt2,	SQLCHAR,	129 );
	dbClass.sqlout (( char	 *)  a_kun_txt.txt3,	SQLCHAR,	129 );
	dbClass.sqlout (( char	 *)  a_kun_txt.txt4,	SQLCHAR,	129 );
	dbClass.sqlout (( char	 *)  a_kun_txt.txt5,	SQLCHAR,	129 );
	
	dbClass.sqlout (( short	 *) &a_kun_txt.sort,	SQLSHORT, 0 );

		readcursor = dbClass.sqlcursor ("select "

	" txt1, txt2, txt3, txt4, txt5, sort "

//	" from a_kun_txt where a = ? and ( kun = ? or kun = 0 ) order by kun desc " ) ;
	" from a_kun_txt where a = ? and  kun = 0 " ) ;	// 060214 Anruf von Daniel

}

// 201213 E

// 190214 A

int A_BAS_ERW_CLASS::leseabaserw ()
{
	a_bas_erw.a = Temptab.a_bas_a();

//	a_kun_txt.kun = get_lsk_kun() ;

	opena_bas_erw();
    int di = lesea_bas_erw() ;
	if ( di )
		memcpy (&a_bas_erw,&a_bas_erw_null, sizeof(struct A_BAS_ERW ));
	return di;
}


int A_BAS_ERW_CLASS::lesea_bas_erw ()
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_BAS_ERW_CLASS::opena_bas_erw (void)
{
		if ( readcursor < 0 ) prepare ();
		
		return dbClass.sqlopen (readcursor);
}

void A_BAS_ERW_CLASS::prepare (void)
{

	dbClass.sqlin (( double	*) &a_bas_erw.a,SQLDOUBLE, 0);

    dbClass.sqlout ((char   *)  a_bas_erw.pp_a_bz1,     SQLCHAR,100);
    dbClass.sqlout ((char   *)  a_bas_erw.pp_a_bz2,     SQLCHAR,100);
    dbClass.sqlout ((char   *)  a_bas_erw.lgr_tmpr,     SQLCHAR,100);
	dbClass.sqlout ((char   *)  a_bas_erw.lupine,       SQLCHAR,3);
	dbClass.sqlout ((char   *)  a_bas_erw.schutzgas,    SQLCHAR,3);
 
	dbClass.sqlout ((short  *) &a_bas_erw.huelle,       SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.shop_wg1,     SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.shop_wg2,     SQLSHORT,0);	//  smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.shop_wg3,     SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.shop_wg4,     SQLSHORT,0);	// smallint,

	dbClass.sqlout ((short  *) &a_bas_erw.shop_wg5,     SQLSHORT,0);	// smallint,
    dbClass.sqlout ((double *) &a_bas_erw.tara2,        SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((short  *) &a_bas_erw.a_tara2,      SQLSHORT,0);	// smallint,
    dbClass.sqlout ((double *) &a_bas_erw.tara3,        SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((short  *) &a_bas_erw.a_tara3,      SQLSHORT,0);	// smallint,

	dbClass.sqlout ((double *) &a_bas_erw.tara4,        SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((short  *) &a_bas_erw.a_tara4,      SQLSHORT,0);	// smallint,
    dbClass.sqlout ((double *) &a_bas_erw.salz,         SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((double *) &a_bas_erw.davonfett,    SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((double *) &a_bas_erw.davonzucker,  SQLDOUBLE,0);	// decimal(8,3),

	dbClass.sqlout ((double *) &a_bas_erw.ballaststoffe,SQLDOUBLE,0);	// decimal(8,3),
    dbClass.sqlout ((char   *)  a_bas_erw.shop_aktion,  SQLCHAR,3);	// char(1),
    dbClass.sqlout ((char   *)  a_bas_erw.shop_neu,     SQLCHAR,3);
    dbClass.sqlout ((char   *)  a_bas_erw.shop_tv,      SQLCHAR,3);
	dbClass.sqlout ((double *) &a_bas_erw.shop_agew,    SQLDOUBLE,0);	// decimal(8,3),

	dbClass.sqlout ((char   *)  a_bas_erw.a_bez,        SQLCHAR,65);
	dbClass.sqlout ((char   *)  a_bas_erw.zutat,        SQLCHAR,2002);
    dbClass.sqlout ((short  *) &a_bas_erw.userdef1,     SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.userdef2,     SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.userdef3,     SQLSHORT,0);	// smallint,

	dbClass.sqlout ((short  *) &a_bas_erw.minstaffgr,   SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.l_pack,       SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.b_pack,       SQLSHORT,0);	// smallint,
    dbClass.sqlout ((short  *) &a_bas_erw.h_pack,       SQLSHORT,0);	// smallint
	dbClass.sqlout ((char   *)  a_bas_erw.zolltarifnr,  SQLCHAR,73);	// 271114

		readcursor = dbClass.sqlcursor ("select "
	"  pp_a_bz1, pp_a_bz2, lgr_tmpr, lupine, schutzgas "
	" ,huelle, shop_wg1, shop_wg2, shop_wg3, shop_wg4 "
	" ,shop_wg5, tara2, a_tara2, tara3, a_tara3 "
	" ,tara4,  a_tara4, salz, davonfett,  davonzucker "
	" ,ballaststoffe, shop_aktion, shop_neu, shop_tv, shop_agew "
	" ,a_bez, zutat, userdef1, userdef2, userdef3 "
	" ,minstaffgr, l_pack, b_pack, h_pack, zolltarifnr "
	
	" from a_bas_erw where a = ? " ) ;

}

// 190214 E




