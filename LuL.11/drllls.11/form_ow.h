#ifndef _FORM_OW_DEF
#define _FORM_OW_DEF

struct FORM_W {
   char     form_nr[11];
   long	    zei ;
   char     krz_txt[51];
   short	lila ; 
 
};

extern struct FORM_W form_w;

class FORM_W_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leseform_w (void);
               int openform_w (void);
			   short readcursor;
               FORM_W_CLASS () : DB_CLASS ()
               {
				   readcursor = -1 ;
               }
};

struct FORM_O {
   char     form_nr[11];
   long		zei;
   char     krz_txt[51];
   short	lila ;
 
};

extern struct FORM_O form_o;

class FORM_O_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leseform_o (void);
               int openform_o (void);
			   short readcursor;
               FORM_O_CLASS () : DB_CLASS ()
               {
				   readcursor = -1 ;
               }
};

#endif