#ifndef _ZUSTAB_DEF
#define _ZUSTAB_DEF

struct SYS_PAR {

char sys_par_nam[21];	// Texte lieber zu lang als zu kurz beim lesen
char sys_par_wrt[3];
char sys_par_besch[37];

long zei;
short delstatus;
};
extern struct SYS_PAR sys_par, sys_par_null;


class SYS_PAR_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//              int dbcount (void);
               int lesesys_par (int);
               int opensys_par (void);
               SYS_PAR_CLASS () : DB_CLASS ()
               {
               }
};

struct A_ZUS_TXT {

double a;
long zei;
short sg1;
char txt[61];
};
extern struct A_ZUS_TXT a_zus_txt, a_zus_txt_null;


class A_ZUS_TXT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//              int dbcount (void);
               int lesea_zus_txt (int);
               int opena_zus_txt (void);
               A_ZUS_TXT_CLASS () : DB_CLASS ()
               {
               }
};

struct A_KUN {

short	mdn ;
short	fil ;
long	kun ;
double	a ;
char	a_kun[14] ;
char	a_bz1[25] ;
short	me_einh_kun ;
double	inh ;
char	kun_bran2[3] ;
double	tara ;
double	ean ;
char	a_bz2[25] ;
short	hbk_ztr ;
long	kopf_text ;
char	pr_rech_kz[2] ;
char	modif[2] ;
long	text_nr ;
short	devise ;
char	geb_eti[2] ;
char	geb_fill[2] ;
long	geb_anz ;
char	pal_eti[2] ;
char	pal_fill[2]  ;
short	pal_anz ;
char	pos_eti[2] ;
short	sg1 ;
short	sg2 ;
short	pos_fill ;
short	ausz_art ;
short	text_nr2 ;
short	cab ;
char	a_bz3[25] ;
char	a_bz4[25] ;
char	li_a[13] ;
double	geb_fakt ;

};
extern struct A_KUN a_kun, a_kun_null;


class A_KUN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_kun (int);
               int opena_kun (void);
               A_KUN_CLASS () : DB_CLASS ()
               {
               }
};

// 230609 : a_kun_gx zugefügt 
struct A_KUN_GX {
short	mdn ;
short	fil ;
long	kun ;
double	a ;
char	a_kun[26] ;

char	a_bz1[25] ;
short	me_einh_kun ;
double	inh ;
char	kun_bran2[3] ;
double	tara ;

double	ean ;
char	a_bz2[25] ;
short	hbk_ztr ;
long	kopf_text ;
char	pr_rech_kz[2] ;

char	modif[2] ;
long	text_nr ;
short	devise ;
char	geb_eti[2] ;
short	geb_fill ;

long	geb_anz ;
char	pal_eti[2] ;
short	pal_fill  ;
long	pal_anz ;
char	pos_eti[2] ;

short	sg1 ;
short	sg2 ;
short	pos_fill ;
short	ausz_art ;
short	text_nr2 ;

short	cab ;
// hier geht es unendlich weiter ......
};
extern struct A_KUN_GX a_kun_gx, a_kun_gx_null;


class A_KUN_GX_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesea_kun_gx (void);
               int opena_kun_gx (void);
               A_KUN_GX_CLASS () : DB_CLASS ()
               {
               }
};

// 201213 : a_kun_txt zugefügt
// Diese Tabelle durchbricht mal wieder alle Konventionen :
// kein Mandant drin ,aber dafür der Index unique auf kun und a
// so werden je Satz unendlich Ressourcen verbraten,
// es gibt jedoch weder Flexibilität bezüglich der erfoderlichen Datenmenge je Eintrag noch der Mandantenstruktur

// Rekursion ist zwar drin, aber halt nur bis auf Kundenebene - Gruppierungen müssten dann "superindividuell" gepflegt werden
// Da kommen dann sofort wieder riesige Datenmengen, exzessive Kopierfunktionen und Unübersichtlichkeiten rein
// ausserdem ist z.B das Feld Artikel mit einem fehlerhfaten Datentyp angelegt, mal sehen,
// was dann wieder an Unsinn zur Laufzeit passiert
// desweiteren wird nicht die Rekursion, sondern nur der Kunde=="0" gewünscht ...
struct A_KUN_TXT {
long	kun ;
long	a ;	// ACHTUNG ACHTUNG
char	txt1[129] ;
char	txt2[129] ;
char	txt3[129] ;
char	txt4[129] ;
char	txt5[129] ;
short sort ;

};
extern struct A_KUN_TXT a_kun_txt, a_kun_txt_null;

class A_KUN_TXT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
			   int leseakuntxt ( void);
               int lesea_kun_txt (void);
               int opena_kun_txt (void);
               A_KUN_TXT_CLASS () : DB_CLASS ()
               {
               }
};

// 190214 : a_bas_erw zugefügt
struct A_BAS_ERW {
	double a;	// decimal(13,0),
    char pp_a_bz1[100];	// char(99),
    char pp_a_bz2[100];	// char(99),
    char lgr_tmpr[100];	// char(99),
    char lupine[3];	// char(1),
    char schutzgas[3];	// char(1),
    short huelle;	// smallint,
    short shop_wg1;	// smallint,
    short shop_wg2;	//  smallint,
    short shop_wg3;	// smallint,
    short shop_wg4;	// smallint,
    short shop_wg5;	// smallint,
    double tara2;	// decimal(8,3),
    short a_tara2;	// smallint,
    double tara3;	// decimal(8,3),
    short a_tara3;	// smallint,
    double tara4;	// decimal(8,3),
    short a_tara4;	// smallint,
    double salz;	// decimal(8,3),
    double davonfett;	// decimal(8,3),
    double davonzucker;	// decimal(8,3),
    double ballaststoffe;	// decimal(8,3),
    char shop_aktion[3];	// char(1),
	char shop_neu[3];	// char(1),
    char shop_tv[3];	// char(1),
    double shop_agew;	// decimal(8,3),
    char a_bez[65];	// char(62),
    char zutat[2002];	// char(2000),
    short userdef1;	// smallint,
    short userdef2;	// smallint,
	short userdef3;	// smallint,
    short minstaffgr;	// smallint,
    short l_pack;	// smallint,
    short b_pack;	// smallint,
    short h_pack;	// smallint

	char zolltarifnr[73];	// 271114 - orig-feld : nur 18 lang ...
};
extern struct A_BAS_ERW a_bas_erw, a_bas_erw_null;

class A_BAS_ERW_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
			   int leseabaserw ( void);
               int lesea_bas_erw (void);
               int opena_bas_erw (void);
               A_BAS_ERW_CLASS () : DB_CLASS ()
               {
               }
};

#endif
