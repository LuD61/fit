#ifndef _TXT_DEF
#define _TXT_DEF


#define MYMAXRANGE 10 

struct PTXT {
long nr;
long zei;
char txt[101];	// ich gehe mal davon aus, das eine Ziele nie groesser als 100 Zeichen ist

};
extern struct PTXT ptxt, ptxt_null;


struct NTXT {
short mdn;
short fil;
long ls;
long posi;
long zei;
char txt[101];	// ich gehe mal davon aus, das eine Zeile nie groesser als 100 Zeichen ist

};
extern struct NTXT ntxt, ntxt_null;

// 161014 A
struct AUFPT_VERKFRAGE {
// vermutlich gibt es da noch viel mehr mehr oder weniger Gammel-Felder in dieser tabelle. 
// die werden dann auf Anforderung unterstützt, da das hier alles relativ unausgegoren wirkt ....
long txt_ls1;
long txt_rech1;
long txt_ls2;
long txt_rech2;
long txt_ls3;
long txt_rech3;
long txt_ls4;
long txt_rech4;
long txt_ls5;
long txt_rech5;
long txt_ls6;
long txt_rech6;
long txt_ls7;
long txt_rech7;
long txt_ls8;
long txt_rech8;
long txt_ls9;
long txt_rech9;
long txt_ls10;
long txt_rech10;
long ls;
short mdn;
double a;
// 031114 dazu :
short def1;
short def2;
short def3;
short def4;
short def5;
short def6;
short def7;
short def8;
short def9;
short def10;
};
extern struct AUFPT_VERKFRAGE aufpt_verkfrage, aufpt_verkfrage_null;

class AUFPT_VERKFRAGE_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
		   ~AUFPT_VERKFRAGE_CLASS();
            int leseaufpt_verkfrage (short emdn , long els, double ea );
			char *ctxt_ls1;
			char *ctxt_rech1;
			char *ctxt_ls2;
			char *ctxt_rech2;
			char *ctxt_ls3;
			char *ctxt_rech3;
			char *ctxt_ls4;
			char *ctxt_rech4;
			char *ctxt_ls5;
			char *ctxt_rech5;
			char *ctxt_ls6;
			char *ctxt_rech6;
			char *ctxt_ls7;
			char *ctxt_rech7;
			char *ctxt_ls8;
			char *ctxt_rech8;
			char *ctxt_ls9;
			char *ctxt_rech9;
			char *ctxt_ls10;
			char *ctxt_rech10;

               AUFPT_VERKFRAGE_CLASS () : DB_CLASS ()
               {
			   }
} ;
// 161014 E

// 101014 A
struct ATEXTE {
    long sys;
    short waa;
    long txt_nr;
    char txt [1025];
    char disp_txt[1025];
    short alignment;
    short send_ctrl;
    short txt_typ;
    short txt_platz;
    double a;

};
extern struct ATEXTE atexte, atexte_null;

class ATEXTE_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leseatexte (long);

               ATEXTE_CLASS () : DB_CLASS ()
               {
			   }
} ;

// 101014 E

extern char * clipped( char *) ;

class PTXT_CLASS : public DB_CLASS
{
       private :
               void prepare (char *);
       public :
//               int dbcount (void);
               int leseptxt (void);
               int openptxt (char *);
   			   char  ctabnmatr[MYMAXRANGE][25] ;			// 111110 mal so lang wie "nform_feld.krz_txt"
			   short ctabimatr[MYMAXRANGE] ;				// 111110
			   int suchereadcursor (char *ctabnam) ;	// 111110

               PTXT_CLASS () : DB_CLASS ()
               {
					// 111110  
					for ( int i = 0 ; i < MYMAXRANGE ; i ++ )
					{
						ctabnmatr[i][0] = '\0' ;
						ctabimatr[i]    =  -1  ;
					}

               }
};

class NTXT_CLASS : public DB_CLASS
{
       private :
               void prepare (char *);
       public :
               int lesentxt (void);
               int openntxt (char *);
			   char  ctabnmatr[MYMAXRANGE][25] ;			// 111110 mal so lang wie "nform_feld.krz_txt"
			   short ctabimatr[MYMAXRANGE] ;				// 111110
			   int suchereadcursor (char *ctabnam) ;	// 111110
			   NTXT_CLASS () : DB_CLASS ()
               {
				   // 111110  
					for ( int i = 0 ; i < MYMAXRANGE ; i ++ )
					{
						ctabnmatr[i][0] = '\0' ;
						ctabimatr[i]    =  -1  ;
					}

               }
};


#endif
