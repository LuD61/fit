//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//

// drllls : Druckausgabe fuer LS : angelehnt an dr70001, ist 
// letztendlich jedoch ein Vorselektionstool
// Es wird eigentlich nur ls-nr und mandant ausgwertet,
// restliche Tabellen aus dr89000 dienen i.w. nur Layout-Gestaltung
// bzw. als Parameter fuer irgendwas
// cronmhd einbauen fuer Lackmann

// -> Performance-Probleme :
//
// falls z.B bei drucker+faxgeraete ein printer \\DC3\hplaserjet existiert und der auch noch auf diesen den Anschlussnamen hoert
// und dieser Anschluss vom Typ "lokaler Anschluss" ist, d.h dieser remote drucker lokal gespoolt wird, dann gibt es Stress ,falls dieser
// Drucker nicht zugriffig ist(timeout von ca. 10 sek je Drucker), falls er zugriffig ist, ist es trotzdem nicht besonders fix 
// korrekt waere ein sauber installierter windows-remote-Drucker , der dann gar keine lokale Anschluss-Zuordnung hat
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
// Diese Applikation soll irgendwann die Listen ausgeben ....
// Erstellung : 24.10.2004  GrJ

//=============================================================================

// mainfrm.cpp : implementation of the CMainFrame class

#include "stdafx.h"
#include "llmfc.h"
#include "mainfrm.h"
// #include "wmask.h"
#include "DbClass.h"
#include "temptab.h"
#include "adr.h"
#include "txt.h"
#include "ptabn.h"
#include "item.h"	// 230108
// #include "strfkt.h"

#include "systables.h"
#include "syscolumns.h"
#include "zustab.h"
#include "kun.h"		// 160305

#include "sys/timeb.h"	// 171109

#include "Token.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GR: Waehlen Sie hier die List & Label Sprache f�r den Designer und die
//     Dialoge aus :
//    (Die entsprechenden Sprachkonstanten entnehmen Sie der Datei cmbtl9.h)

// US: choose your LuL-Language for all designer, dialogs, etc... here:
//    (see cmbtl9.h for other language-constants)


short GlobalLila ;

const int LUL_LANGUAGE = CMBTLANG_GERMAN;
const int CTEXTMAX = 512;
const int LG_MAX = 21 ;	// 041206 10 -> 21

#define PRIV_CRONMHD	"cronmhd" 
#define PRIV_ZERLDATEN	"zerldaten"	// 091209 
#define PRIV_A_KUN_TXT	"a_kun_txt"	// 201213 
#define PRIV_A_BAS_ERW	"a_bas_erw"	// 190214 

#define PRIV_AUFK	"aufk"			// 230914 

#define PRIV_AUFPT_VERKFRAGE "aufpt_verkfrage"	// 031114



// irgendein dummy halt 
const char *FILENAME_DEFAULT = "c:\\user\\fit\\format\\LL\\53100.lst und immer laenger wird der string ........";

bool cfgOK;
char Listenname[512];
char szTempt[CTEXTMAX + 5];

char User[128] ;
char LUser[128] ;	// 170107
int danzahl ;		   
char myform_nr[99] ;	// eigentlich max 10 Zeichen + Zeichenkettenende
int Listenauswahl = 0;   // wenn 1 : Auswahl mehrerer Reports
int Hauptmenue = 0;   // wenn 1 : Auswahl edit oder print �ber Menue
int DrMitMenue = 1;   // wenn 1 : Ausdruck mit diversen Dialogen,sollte der Standard sein 
int NutzerIgnorieren = 0;	// 090109 :wenn 1 , dann kein fehler bei fehlendem sub-Dir	
int danzahlerlaubt = 0 ;	// 031109 : Falls auf dem Platz anzahlerlaubt, dann Anzahl korrekt auswerten
int EntwurfsModus = 0;   // wenn 1 : Entwurfsmodus: Aufruf ohne Parameter
int WahlModus = 0 ;   // wenn 1 : Druckerwahl-Dialog als getrennte Aktion
int PMWahlModus = 0 ;   // 041206 : Party-Mandant
int dzerllfd = 0 ;		// 080113 : parametrierbarer Schluessel

int dvariante = 0 ;	// 110309 : Varianten-Steuerung ( z.B. Teil-LS : pos_stat > 2 + variante = 1) 

static int da_zustxt_wa = 0 ;		// Systemparameter
static int dnachkpreis  = 0 ;		// Systemparameter
static int dwa_pos_txt  = 0 ;		// Systemparameter
static int party_mdn_aktiv = 0 ;	// Systemparameter 041206 
static int lackmann_aktiv = 0 ;		// Systemparameter  230108
extern int redeckerpar ;			// Systemparameter 250808 : steht in temptab.cpp drin
//  Falls redeckerpar aktiv ist, wird das feld a_kun.geb_fakt alternativ mit redecker-Infos gefuellt 

extern int conradi ;			// falls conradi aktiv ist, dann werden conradi-Sonderw�nsche ausgef�hrt

extern int  pa_kun_txt0 ;

extern int sonschill2 ;			// Systemparameter : 230609 : steht in temptab.cpp drin 
// Falls sonschill2 aktiv ist, wird a_kun.geb_anz durch a_kun_gx.geb_anz �berladen - muss aber angekreuzt sein 

static int dkun_leer    = 0 ;		// Systemparameter	160305
// 160305 041206 : auf 20 Stellen erweitert 
static int leerg_st[LG_MAX] ;
static int leerg_zu[LG_MAX] ;
static int leerg_ab[LG_MAX] ;
static double leerg_a[LG_MAX] ;
static double leerg_p[LG_MAX] ;
static double leerg_g[LG_MAX] ;	// 230609 : Arikelgewicht auch noch, sollte aber in der Selektion angekreuzt werden 
static char leerg_bz[LG_MAX][25] ;


extern int verboteneFelder ;		// 160113
extern char cverboteneFelder[120] ;	// 160113

extern int spmetro1_par;			// 171114
extern int spboes_par;				// 171114
extern int  intboes;				// 171114
extern char strboes_extramgr[257];	// 171114

int datenausdatei = 0;	// entscheidung, ob Ranges per Dialog
						// oder aus einer Parameterdatei kommen sollen
static char datenausdateiname[513] ;	// Dateiname ......

char *wort[100];	// mehr als 100 Worte braucht es nicht ?!

int zeittest = 0 ;	// ACHTUNG !!! TESTMODUS

// 300306 A
static int  alternat_ausgabe = 0 ;

static char alternat_type[ 50] ;
static char alternat_file[150] ;
static char alternat_dir [150] ;
// 300306 E


extern bool komplettierezerldaten ( void) ;
extern bool komplettiereakuntxt ( void) ;
extern bool komplettiereabaserw ( void) ;	// 190214

DB_CLASS dbClass;

TOU_CLASS Tou;

ADR_CLASS Adr;
PTXT_CLASS Ptxt;
NTXT_CLASS Ntxt;	// 110205
ATEXTE_CLASS Atexte;	// 101014
AUFPT_VERKFRAGE_CLASS Aufpt_verkfrage;	// 161014

NFORM_TAB_CLASS Nform_tab;
NFORM_FELD_CLASS Nform_feld;
SYSCOLUMNS_CLASS Syscolumns;
SYSTABLES_CLASS Systables;
TEMPTAB_CLASS Temptab;
PTABN_CLASS Ptabn;
SYS_PAR_CLASS Sys_par;
A_ZUS_TXT_CLASS A_zus_txt;	
LEER_LSDR_CLASS Leer_lsdr;	// 160305	
A_BAS_CLASS A_bas;			// 160305
CRONMHD_CLASS Cronmhd ;	// 230108 
ZERLDATEN_CLASS Zerldaten ;	// 091209
A_KUN_TXT_CLASS A_kun_txt ;	// 201213
A_BAS_ERW_CLASS A_bas_erw ;	// 190214

HWND hMainWindow;

// int form_feld_curs, tabid_curs, coltyp_curs,  dynacurs ; 

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

// GR: Registrieren eines Nachrichtenkanals f�r List & Label Callbacks
// US: register LuL MessageBase:
UINT CMainFrame::m_uLuLMessageBase = RegisterWindowMessage("cmbtLLMessage");

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_START_DEBUG, OnStartDebug)
	ON_COMMAND(ID_FILE_STOP_DEBUG, OnStopDebug)
	ON_COMMAND(ID_EDIT_LABEL, OnEditLabel)
	ON_COMMAND(ID_EDIT_LIST, OnEditList)
	ON_UPDATE_COMMAND_UI(ID_FILE_START_DEBUG, OnUpdateStartDebug)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP_DEBUG, OnUpdateStopDebug)
	ON_COMMAND(ID_PRINT_LABEL, OnPrintLabel)
	ON_COMMAND(ID_PRINT_REPORT, OnPrintReport)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(m_uLuLMessageBase,OnLulMessage)
END_MESSAGE_MAP()



void schreibezeit ( char * text )
{
FILE *fperr ;
char writepuffer[199] ;
char erfnam [199] ;
char timestr[100] ;

struct _timeb tstruct;


    _ftime( &tstruct ); 
	sprintf (writepuffer , "%s.%u : %s\n" , _strtime(timestr), tstruct.millitm, text ) ; 

	sprintf ( erfnam , "%s\\timelog.lls", getenv("TMPPATH") ) ;
	if ( ( fperr = fopen(erfnam, "at" )) == NULL ) return ;   // APPEND_TEXT 
	fprintf ( fperr , writepuffer ) ;
	fclose ( fperr ) ;
}


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction


CMainFrame::CMainFrame()
{


	m_bDebug = FALSE;

}

CMainFrame::~CMainFrame()
{
/* ---->
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }
< ------ */

}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{


	if ( zeittest ) schreibezeit("Start \n" ) ;

	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

/* ---> CELL11  : redundant 	
	LlAddCtlSupport(m_hWnd,
					LL_CTL_ADDTOSYSMENU | LL_CTL_CONVERTCONTROLS | LL_CTL_ALSOCHILDREN,
					"combit.ini");
< --- */

	GetCfgValues();

	InterpretCommandLine();

	if ( zeittest ) schreibezeit("Nach Commandline \n" ) ;

	if ( EntwurfsModus)
	{   DrMitMenue = 1;
		Hauptmenue = 1 ;
		Listenauswahl = 1 ;
	}

	if (Hauptmenue == 0)
	{
		PostMessage(WM_COMMAND,ID_PRINT_REPORT,0l);
	}

return 0;
}

void CMainFrame::OnStartDebug()
{
// 	komplett deaktiviert -> zu testzwecken aktivieren und debwin2.exe starten vor LuL-Start
//	MessageBox("Make sure that DEBWIN2 had been started before this demo application. "
//				"If this doesn't happen you won't see any debug outputs now!",
//				"List & Label Sample App", MB_OK | MB_ICONINFORMATION);
//	LlSetDebug(LL_DEBUG_CMBTLL);
//	m_bDebug = TRUE;

}

void CMainFrame::OnStopDebug()
{
	LlSetDebug(FALSE);
	m_bDebug = FALSE;
}

void CMainFrame::OnUpdateStopDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bDebug);
}

void CMainFrame::OnUpdateStartDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bDebug);
}

//=============================================================================
void CMainFrame::OnEditLabel()
//=============================================================================
{
	/* ---->
	CHAR szFilename[128+1] = "*.lbl";
	HWND hWnd = m_hWnd;
	HJOB hJob;
	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	// US: initialize Job
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}
// OnEditLabel
	// GR: Setzen der List & Label Optionen
	// US: Setting the List & Label options
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	//GR: Exporter aktivieren
	//US: Enable exporter
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
** ---> GrJ GrJ
	if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
    {
        LlJobClose(hJob);
        return;
    }
< ---- **
	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);
// OnEditLabel
	// GR: Aufruf des Designers
   	sprintf ( szFilename, FILENAME_DEFAULT );
 	if (LlDefineLayout(hJob, hWnd, "Designer", LL_PROJECT_LABEL, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
// OnEditLabel
< ----- */
}

int GetRecCount()  
{
//	return RepoRech.dbcount ();
	return 1 ;
}

// 110506
void ptabschreiben ( HJOB hJob, char * varfeld , int imodus , char * tab , char * feld ) 
{
	char hilfsfeld [256] ;
	char hilfsfeld2[256] ;
	int schleife ;
	schleife = 0 ;

	while ( schleife  < 5 )
	{
// ptbezk
		if ( schleife == 0 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptbezk", tab, feld );
			if  ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptbezk",  feld );
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptbezk ) ;
		};
// ptbez
		if ( schleife == 1 ) 
		{
			sprintf ( hilfsfeld, "%s.%s.ptbez",tab, feld);
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptbez", feld );
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptbez ) ;
		}
// ptwert
		if ( schleife == 2 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwert",tab, feld );
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptwert", feld);
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptwert ) ;
		}

// ptwer1
		if ( schleife == 3 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwer1", tab, feld ) ;
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptwer1", feld ) ;
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptwer1 ) ;
		}

// ptwer2
		if ( schleife == 4 )
		{
			sprintf ( hilfsfeld, "%s.%s.ptwer2", tab, feld ) ;
			if ( ! imodus )
				sprintf ( hilfsfeld2, "%s.ptwer2", feld ) ;
			else
				sprintf ( hilfsfeld2, "%s", ptabn.ptwer2 ) ;
		}

		if ( varfeld[0] == 'V' )
		{	
			if ( !imodus )
				LlDefineVariableExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
			else
				if ( LlPrintIsVariableUsed(hJob, hilfsfeld))
					LlDefineVariableExt(hJob, clipped( hilfsfeld),
								clipped(hilfsfeld2), LL_TEXT, NULL);
		}
		else
		{
			if ( !imodus )
				LlDefineFieldExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
			else
				if ( LlPrintIsFieldUsed(hJob, hilfsfeld))
					LlDefineFieldExt(hJob, clipped( hilfsfeld),
							clipped(hilfsfeld2), LL_TEXT, NULL);
		}
		schleife ++ ;
	} ;	// end while 

}

void Variablendefinition (HJOB hJob)
{
	char hilfsfeld[256] ;
	char hilfsfeld2[128] ;


	sprintf ( systables.tabname, " " );	// initialisieren
	systables.tabid = 0 ;				// initialisieren


	sprintf (nform_tab.form_nr ,"%s", myform_nr );
	nform_tab.lila = GlobalLila ;

	Nform_tab.opennformtab ();
	while (!Nform_tab.lesenformtab())	// gefunden
	{

		sprintf (nform_feld.form_nr ,"%s", myform_nr );
		sprintf (nform_feld.tab_nam ,"%s", nform_tab.tab_nam );
		nform_feld.lila = GlobalLila ;

		Nform_feld.opennform_feld ();
		while (!Nform_feld.lesenform_feld())	// gefunden
		{


			switch ( nform_feld.feld_typ )
			{
				case 1 : // ptabn
					ptabschreiben ( hJob, "V" , 0 ,
						clipped (nform_feld.tab_nam), clipped( nform_feld.feld_nam)) ;

/* ---> 110506 : auslagern wegen rekursiv
//					ptbezk
					sprintf ( hilfsfeld, "%s.%s.ptbezk", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptbezk", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptbez
					sprintf ( hilfsfeld, "%s.%s.ptbez", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptbez", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwert
					sprintf ( hilfsfeld, "%s.%s.ptwert", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwert", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwer1
					sprintf ( hilfsfeld, "%s.%s.ptwer1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwer1", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwer2
					sprintf ( hilfsfeld, "%s.%s.ptwer2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwer2", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
110506				< ----- */
					break ;

				case 2 : // Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-Textbaustein", clipped ( nform_feld.krz_txt));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					break ;

				case 3 : // adresse
//					anr
					sprintf ( hilfsfeld, "%s.%s.anr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.anr", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_krz
					sprintf ( hilfsfeld, "%s.%s.adr_krz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_krz", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam1
					sprintf ( hilfsfeld, "%s.%s.adr_nam1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam1", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam2
					sprintf ( hilfsfeld, "%s.%s.adr_nam2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam2", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_nam3	090905
					sprintf ( hilfsfeld, "%s.%s.adr_nam3", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam3", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					ort1
					sprintf ( hilfsfeld, "%s.%s.ort1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort1", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

// 060307 A
//					ort2
					sprintf ( hilfsfeld, "%s.%s.ort2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort2", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					tel
					sprintf ( hilfsfeld, "%s.%s.tel", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.tel", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					fax
					sprintf ( hilfsfeld, "%s.%s.fax", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.fax", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 301111			mobil
					sprintf ( hilfsfeld, "%s.%s.mobil", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.mobil", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

// 300813			iban
					sprintf ( hilfsfeld, "%s.%s.iban", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iban", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 300813			swift
					sprintf ( hilfsfeld, "%s.%s.swift", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.swift", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					partner
					sprintf ( hilfsfeld, "%s.%s.partner", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.partner", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 060307 E					
					
//					plz
					sprintf ( hilfsfeld, "%s.%s.plz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					postfach	080709
					sprintf ( hilfsfeld, "%s.%s.plz_pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz_pf", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					sprintf ( hilfsfeld, "%s.%s.pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.pf", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					
//					str
					sprintf ( hilfsfeld, "%s.%s.str", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.str", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					iln
					sprintf ( hilfsfeld, "%s.%s.iln", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iln", clipped ( nform_feld.feld_nam));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr
					sprintf ( hilfsfeld, "%s.%s.adr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								"08150", LL_NUMERIC, NULL);

// 110506 : alles neu dazu 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					ptabschreiben ( hJob, "V" , 0 , hilfsfeld , "staat" ) ;

					break ;


				default :

					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

					sprintf ( hilfsfeld2, "%s", clipped ( nform_feld.feld_nam) );

					if ( strcmp ( clipped ( systables.tabname)  , clipped ( nform_feld.tab_nam)))
					{
						sprintf ( systables.tabname , "%s" , clipped( nform_feld.tab_nam));
						systables.tabid = 0 ;
						Systables.opensystables ();
						Systables.lesesystables();

					}
					if ( !systables.tabid )	// Error
					{
						syscolumns.coltype  = iDBCHAR ;	// Notbremse
					}
					else
					{
						sprintf ( syscolumns.colname , "%s" , clipped( nform_feld.feld_nam));  
						syscolumns.coltype = iDBCHAR ;
						syscolumns.tabid = systables.tabid ;
						Syscolumns.opensyscolumns ();
						Syscolumns.lesesyscolumns();
					}
					if ( syscolumns.coltype == iDBCHAR || syscolumns.coltype == iDBDATUM )
					{
// 200105 : Datum-Default sinnvoll belegen  230914 : datum dazu .......
						if ( syscolumns.coltype == iDBDATUM ||syscolumns.coltype == iDBDATETIME )
							sprintf ( hilfsfeld2, "08.08.2007");


						LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					}
					else
					{
						LlDefineVariableExt(hJob, clipped (hilfsfeld) ,
								"1.0000", LL_NUMERIC, NULL);
					}
// 060307 A
					if (! ( strncmp ( clipped( nform_feld.feld_nam), "tou", 3 )))
					{
						if ( (strlen ( nform_feld.feld_nam )) == 3 ||
							 ! (strncmp(nform_feld.feld_nam ,"tou_nr",6)))
						{
							sprintf ( hilfsfeld, "%s.%s_bez", clipped (nform_feld.tab_nam)
							, clipped( nform_feld.feld_nam ));
								LlDefineVariableExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
					}
// 060307 E

			}
		}
	}
//			nachkpreis
	LlDefineVariableExt(hJob, "rangt.nachkpreis" , "2", LL_NUMERIC, NULL);
//	110309 
	LlDefineVariableExt(hJob, "rangt.variante" , "0", LL_NUMERIC, NULL);
// 310812
	LlDefineVariableExt(hJob, "rangt.Heute" , "08.01.2013", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rangt.Zeit" , "11:55:00", LL_TEXT, NULL);
	LlDefineVariableExt(hJob, "rangt.Nutzer" , "Nu-Name", LL_TEXT, NULL);


}


void Felderdefinition (HJOB hJob)
{

	char hilfsfeld[256] ;
	char hilfsfeld2[128] ;

	sprintf ( systables.tabname, " " );	// initialisieren
	systables.tabid = 0 ;				// initialisieren


	sprintf (nform_tab.form_nr ,"%s", myform_nr );
	nform_tab.lila = GlobalLila ;

	Nform_tab.opennformtab ();
	while (!Nform_tab.lesenformtab())	// gefunden
	{

		sprintf (nform_feld.form_nr ,"%s", myform_nr );
		sprintf (nform_feld.tab_nam ,"%s", nform_tab.tab_nam );
		nform_feld.lila = GlobalLila ;


	
		Nform_feld.opennform_feld ();
		while (!Nform_feld.lesenform_feld())	// gefunden
		{

			switch ( nform_feld.feld_typ )
			{
				case 1 : // ptabn
					ptabschreiben ( hJob, "F" , 0 ,
					clipped (nform_feld.tab_nam), clipped( nform_feld.feld_nam) ) ;	

/* 110506 -----> auslagern 
//					ptbezk
					sprintf ( hilfsfeld, "%s.%s.ptbezk", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptbezk", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptbez
					sprintf ( hilfsfeld, "%s.%s.ptbez", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptbez", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwert
					sprintf ( hilfsfeld, "%s.%s.ptwert", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwert", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwer1
					sprintf ( hilfsfeld, "%s.%s.ptwer1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwer1", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					ptwer2
					sprintf ( hilfsfeld, "%s.%s.ptwer2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ptwer2", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
< ----- */

					break ;

				case 2 : // Textbaustein 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s-Textbaustein", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					break ;

				case 3 : // adresse
//					anr
					sprintf ( hilfsfeld, "%s.%s.anr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.anr", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					adr_krz
					sprintf ( hilfsfeld, "%s.%s.adr_krz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_krz", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam1
					sprintf ( hilfsfeld, "%s.%s.adr_nam1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam1", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam2
					sprintf ( hilfsfeld, "%s.%s.adr_nam2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam2", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr_nam3	090905
					sprintf ( hilfsfeld, "%s.%s.adr_nam3", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.adr_nam3", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					ort1
					sprintf ( hilfsfeld, "%s.%s.ort1", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort1", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 060307 A
//					ort2
					sprintf ( hilfsfeld, "%s.%s.ort2", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.ort2", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					tel
					sprintf ( hilfsfeld, "%s.%s.tel", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.tel", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					fax
					sprintf ( hilfsfeld, "%s.%s.fax", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.fax", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 301111			mobil
					sprintf ( hilfsfeld, "%s.%s.mobil", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.mobil", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 300813			iban
					sprintf ( hilfsfeld, "%s.%s.iban", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iban", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
// 300813			swift
					sprintf ( hilfsfeld, "%s.%s.swift", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.swift", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					partner
					sprintf ( hilfsfeld, "%s.%s.partner", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.partner", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

// 060307 E
//					plz
					sprintf ( hilfsfeld, "%s.%s.plz", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);

//					postfach	090708
					sprintf ( hilfsfeld, "%s.%s.plz_pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.plz_pf", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					sprintf ( hilfsfeld, "%s.%s.pf", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.pf", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);


//					str
					sprintf ( hilfsfeld, "%s.%s.str", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.str", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					iln
					sprintf ( hilfsfeld, "%s.%s.iln", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					sprintf ( hilfsfeld2, "%s.iln", clipped ( nform_feld.feld_nam));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
//					adr
					sprintf ( hilfsfeld, "%s.%s.adr", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								"08150", LL_TEXT, NULL);

// 110506 : alles neu dazu 
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));
					ptabschreiben ( hJob, "F" , 0 , hilfsfeld , "staat" ) ;
					break ;


				default :
					sprintf ( hilfsfeld, "%s.%s", clipped (nform_feld.tab_nam)
						, clipped( nform_feld.feld_nam ));

					sprintf ( hilfsfeld2, "%s", clipped ( nform_feld.feld_nam) );

					if ( strcmp ( clipped ( systables.tabname)  , clipped ( nform_feld.tab_nam)))
					{
						sprintf ( systables.tabname , "%s" , clipped( nform_feld.tab_nam));
						systables.tabid = 0 ;
						Systables.opensystables ();
						Systables.lesesystables();

					}
					if ( !systables.tabid )	// Error
					{
						syscolumns.coltype  = iDBCHAR ;	// Notbremse
					}
					else
					{
						sprintf ( syscolumns.colname , "%s" , clipped( nform_feld.feld_nam));  
						syscolumns.coltype = iDBCHAR ;
						syscolumns.tabid = systables.tabid ;
						Syscolumns.opensyscolumns ();
						Syscolumns.lesesyscolumns();
					}
					if ( syscolumns.coltype == iDBCHAR || syscolumns.coltype == iDBDATUM )
						LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
					else
						LlDefineFieldExt(hJob, clipped (hilfsfeld) ,
								"1.0000", LL_NUMERIC, NULL);

// 060307 A
					if (! ( strncmp ( clipped( nform_feld.feld_nam), "tou", 3 )))
					{
						if ( (strlen ( nform_feld.feld_nam )) == 3 ||
							 ! (strncmp(nform_feld.feld_nam ,"tou_nr",6)))
						{
							sprintf ( hilfsfeld, "%s.%s_bez", clipped (nform_feld.tab_nam)
							, clipped( nform_feld.feld_nam ));
								LlDefineFieldExt(hJob, clipped( hilfsfeld) ,
								clipped(hilfsfeld2), LL_TEXT, NULL);
						}
					}
// 060307 E


			}
		}
	}
//			nachkpreis
					LlDefineFieldExt(hJob, "rangt.nachkpreis" ,
								"2", LL_NUMERIC, NULL);
//	110309 
	LlDefineFieldExt(hJob, "rangt.variante" , "0", LL_NUMERIC, NULL);
// 310812
	LlDefineFieldExt(hJob, "rangt.Heute" , "01.01.2012", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rangt.Zeit" , "11:55:00", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "rangt.Nutzer" , "Nu-Name", LL_TEXT, NULL);


}

/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sqldatamger
-
-       In              :       string-amerikanisch "YYYY-MM-DD"
-
-       Out             :       string-germanisch   "DD.MM.JJJJ"
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Umformatieren eines Datumsstrings
-
--------------------------------------------------------------------------------
**/
#ifdef OLD_DCL
char * sqldatamger(     inpstr,        outpstr)
char * inpstr;
char * outpstr;
#else
char * sqldatamger(char *inpstr, char *outpstr)
#endif
{
	/* so simpel geht es nicht mehr 270907 
 
	outpstr[ 0]= inpstr[8];
	outpstr[ 1]= inpstr[9];
	outpstr[ 2]= '.';
	outpstr[ 3]= inpstr[5];
	outpstr[ 4]= inpstr[6];
	outpstr[ 5]= '.';
	outpstr[ 6]= inpstr[0];
	outpstr[ 7]= inpstr[1];
	outpstr[ 8]= inpstr[2];
	outpstr[ 9]= inpstr[3];
	outpstr[10]= '\0';
	return outpstr ;	
	< ----- */
	if (( inpstr[0] == ' ') || ( inpstr[0] == '\0') ||(inpstr[1] == '\0' ))
	{	// Nullstring handeln ...
		if ( lackmann_aktiv )	// 230108
			outpstr[0] = '\0' ;
		else
			sprintf ( outpstr, "  .  .    " ) ;
		return outpstr  ;
	}
	else
	{
		if ( inpstr[4] == '-' && inpstr[7] == '-' )	// passt fuer y4md- und y4dm-
		{											// wir nehmen halt jetzt mal y4md- an
			// default fit-informix
				outpstr[ 0]= inpstr[8];
				outpstr[ 1]= inpstr[9];
				outpstr[ 3]= inpstr[5];
				outpstr[ 4]= inpstr[6];
/* ---->
		else	// eher hypothetischer Fall : "y4dm-"
			{
				outpstr[ 0]= inpstr[5];
				outpstr[ 1]= inpstr[6];
				outpstr[ 3]= inpstr[8];
				outpstr[ 4]= inpstr[9];
			}
< ---- */
			outpstr[ 2]= '.';
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[0];
			outpstr[ 7]= inpstr[1];
			outpstr[ 8]= inpstr[2];
			outpstr[ 9]= inpstr[3];
			outpstr[10]= '\0';
			if ( lackmann_aktiv )	// 230108
			{
				if ( ! strcmp ( outpstr, "31.12.1899")) outpstr[0] = '\0' ;
			}
			return outpstr ;
		} ;

		if ( inpstr[2] == '.' && inpstr[5] == '.' )	// passt fuer dmy4.
		{

			outpstr[ 0]= inpstr[0];
			outpstr[ 1]= inpstr[1];
			outpstr[ 2]= '.';
			outpstr[ 3]= inpstr[3];
			outpstr[ 4]= inpstr[4];
			outpstr[ 5]= '.';
			outpstr[ 6]= inpstr[6];
			outpstr[ 7]= inpstr[7];
			outpstr[ 8]= inpstr[8];
			outpstr[ 9]= inpstr[9];
			outpstr[10]= '\0';
			if ( lackmann_aktiv )	// 230108
			{
				if ( ! strcmp ( outpstr, "31.12.1899")) outpstr[0] = '\0' ;
			}
			return outpstr ;
		}	// "dmy4."

	// ab jetzt geht sowieso alles schief ....

		outpstr[ 0]= inpstr[8];
		outpstr[ 1]= inpstr[9];
		outpstr[ 2]= '.';
		outpstr[ 3]= inpstr[5];
		outpstr[ 4]= inpstr[6];
		outpstr[ 5]= '.';
		outpstr[ 6]= inpstr[0];
		outpstr[ 7]= inpstr[1];
		outpstr[ 8]= inpstr[2];
		outpstr[ 9]= inpstr[3];
		outpstr[10]= '\0';
		if ( lackmann_aktiv )	// 230108
		{
			if ( ! strcmp ( outpstr, "31.12.1899")) outpstr[0] = '\0' ;
		}
		return outpstr ;

	}

}


/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sqldatgeram
-
-
-       In              :       string-germanisch   "DD.MM.JJJJ"
-       Out             :       string-amerikanisch "YYYY-MM-DD"
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Umformatieren eines Datumsstrings
-
--------------------------------------------------------------------------------
**/
#ifdef OLD_DCL
char * sqldatgeram(     inpstr,   outpstr)
char * inpstr;
char * outpstr;
#else
char * sqldatgeram(char *inpstr, char * outpstr)
#endif
{
 
	outpstr[ 0]= inpstr[6];
	outpstr[ 1]= inpstr[7];
	outpstr[ 2]= inpstr[8];
	outpstr[ 3]= inpstr[9];
	outpstr[ 4]= '-';
	outpstr[ 5]= inpstr[3];
	outpstr[ 6]= inpstr[4];	// little bug : Ziel 7 -> 6 ab 300904
	outpstr[ 7]= '-';
	outpstr[ 8]= inpstr[0];
	outpstr[ 9]= inpstr[1];
	outpstr[10]= '\0';
	return  outpstr ;
	
}


#ifdef OLD_DCL
char * datplusgerman( inpstr, outpstr, doffset)
char * inpstr;
char * outpstr;
long doffset;
#else
char * datplusgerman(char *inpstr, char * outpstr, long doffset)
#endif

{

 
	struct tm akttm;
	time_t dstart;

	char idd[3];
	char imm[3];
	char iyy[5];

	idd[0] = inpstr[0] ;
	idd[1] = inpstr[1] ;
	idd[2] = '\0' ;

	imm[0] = inpstr[3] ;
	imm[1] = inpstr[4] ;
	imm[2] = '\0' ;

	iyy[0] = inpstr[6] ;
	iyy[1] = inpstr[7] ;
	iyy[2] = inpstr[8] ;
	iyy[3] = inpstr[9] ;
	iyy[4] = '\0' ;
	
	
	time( &dstart );
	akttm = *localtime( &dstart );
 
	// in tm_year steckt dan so was wie "103" fuer 2003" usw.

	long hijahr = atol ( iyy );
	if ( hijahr < 60L) hijahr += 100L ;	//2 stelliges Jahr 2000 bis 2059
	if ( hijahr <= 60L && hijahr < 100L ) hijahr = hijahr ;	// 2 stelliges Jahr 1960 ..1999 syntax-dummy
	if ( hijahr > 1900L && hijahr < 2099L ) hijahr -= 1900L ;  // Der Rest fuehrt zu Fehlern ....
	akttm.tm_year = hijahr ; 
    akttm.tm_mday = atol ( idd ); 
    akttm.tm_mon = atol ( imm ) - 1L ; 
         
  if( (dstart = mktime( &akttm )) != (time_t)-1 )
  {

	dstart += ( 86400L * doffset ) ;
	akttm = *localtime( &dstart );

    sprintf ( outpstr, "%02.0d.%02.0d.%04.0d",
		akttm.tm_mday, akttm.tm_mon + 1,
		akttm.tm_year + 1900L) ; 
 
  
  }
  else	// mktime failed
  {
	  sprintf ( outpstr, "%s", inpstr );
  }
	return  outpstr ;
 
}

void zahlweisen ( HJOB hJob, char * text, char * typ)
{
/* ---> evtl. spaeter wieder aktiv ?
-> ist so nicht mehr aktuell, dieser Ramsch kann irgendwann ganz weg ....
char kond1[4], kond2[4], kond3[4];
char hilfdatum1[11], hilfdatum2[11], hilfdatum3[11] ;
char hilfwert1[11], hilfwert2[11] ;
char hilfproz1[11], hilfproz2[11] ;


double hp, hw;
int i ;

	szTempt[0] = '\0' ;

	if ((( typ[0] == 'V') && (LlPrintIsVariableUsed(hJob, "rtab.zako_txt")))
		|| 
	   (( typ[0] == 'F') && (LlPrintIsFieldUsed(hJob, "rtab.zako_txt"))))
	{
		char add='@' ;

		if ( text[0] == add )
		{

//			1. Zeichen "@"	=> Kennung "vorformatiert"
//
//		                11111111112222222222333333333344444444445555555555
//            012345678901234567890123456789012345678901234567890123456789
//
//           "@Tage--& Skto -&.&& , Tage--& Skto -&.&& , Tage--& netto "
//
//			fixe string-Formate zum Aufloesen, nichtvorhandene Kondis fallen weg ...			
//
//                      1111111111222222222			
//            01234567890123456789012345678
//           "@Bankeinzug : Skonto -&.&& % "
			

			if (text[1] == 'B')
			{
				sprintf ( szTempt, "%s", text + 1 ) ;	// "@" kappen
			}
			else
			{

				i = strlen (clipped (text));
				sprintf (hilfdatum1 ,"12.12.02");
				sprintf (hilfdatum2 ,"12.12.02");
				sprintf (hilfdatum3 ,"12.12.02");
				sprintf (hilfwert1 ,"00,00");
				sprintf (hilfwert2 ,"00,00");
				sprintf (hilfproz1 ,"00,00");
				sprintf (hilfproz2 ,"00,00");

				if (i > 9)		// Notbremse ....
				{
	
					kond1[0] = text[5] ;
					kond1[1] = text[6] ;
					kond1[2] = text[7] ;
					kond1[3] = '\0' ;

					sqldatamger ( reporech.rech_dat, hilfdatum1 );
					datplusgerman( hilfdatum1 , hilfdatum1, atol( kond1));

// Erzeugen :	hilfdatum = rech_dat + kond1 

					if ( text[9] != 'S' )	// netto-Ablauf
					{
						sprintf ( szTempt, "Zahlbar bis zum %s rein netto", hilfdatum1 );
					}
					else
					{
						if ( i > 30 )	// Notbremse
						{
							kond2[0] = text[26] ;
							kond2[1] = text[27] ;
							kond2[2] = text[28] ;
							kond2[3] = '\0' ;


							hilfproz1[0] = text[14] ;
							hilfproz1[1] = text[15] ;
							hilfproz1[2] = text[16] ;
							hilfproz1[3] = text[17] ;
							hilfproz1[4] = text[18] ;
							hilfproz1[5] = '\0' ;

							sqldatamger ( reporech.rech_dat, hilfdatum2 );
							datplusgerman( hilfdatum2 , hilfdatum2, atol( kond2));

							
							if ( text[30] != 'S' )	// netto-Ablauf
							{

								 hp = atof( hilfproz1 );
								 hw = reporech.zahl_betr * (hp /100.00) ;

								 sprintf ( hilfwert1, "%1.2f", hw);
								 

								sprintf ( szTempt, "Zahlbar bis zum %s abz. %s Euro %s%% Skto, bis zum %s rein netto",
									hilfdatum1, hilfwert1,hilfproz1, hilfdatum2 );
							}
							else
							{
								if ( i > 50 )	// Notbremse
								{
									kond3[0] = text[26] ;
									kond3[1] = text[27] ;
									kond3[2] = text[28] ;
									kond3[3] = '\0' ;


									sqldatamger ( reporech.rech_dat, hilfdatum3 );
									datplusgerman( hilfdatum3 , hilfdatum3, atol( kond3));


									hilfproz2[0] = text[14] ;
									hilfproz2[1] = text[15] ;
									hilfproz2[2] = text[16] ;
									hilfproz2[3] = text[17] ;
									hilfproz2[4] = text[18] ;
									hilfproz2[5] = '\0' ;

									hp = atof( hilfproz1 );
									hw = reporech.zahl_betr * (hp /100.00) ;

									sprintf ( hilfwert1, "%1.2f", hw);

									hp = atof( hilfproz2 );
									hw = reporech.zahl_betr * (hp /100.00) ;

									sprintf ( hilfwert2, "%1.2f", hw);

									sprintf ( szTempt, "Zahlbar bis zum %s abz. %s Euro %s%% Skto,%cbis zum %s abz. %s Euro %s%% Skto, bis zum %s rein netto",
									hilfdatum1, hilfwert1,hilfproz1,
									LL_CHAR_NEWLINE,
									hilfdatum2, hilfwert2, hilfproz2,
									hilfdatum3 );

								}
							}
						}
						else	// nur skto-Info 
						{
							if (i > 18 )
							{
							    hilfproz1[0] = text[14] ;
							    hilfproz1[1] = text[15] ;
							    hilfproz1[2] = text[16] ;
								hilfproz1[3] = text[17] ;
								hilfproz1[4] = text[18] ;
								hilfproz1[5] = '\0' ;

								
								hp = atof( hilfproz1 );
								hw = reporech.zahl_betr * (hp /100) ;

								sprintf ( hilfwert1, "%1.2f", hw);

								sprintf ( szTempt, "Zahlbar bis zum %s abz. %s Euro %s%% Skto",
									hilfdatum1, hilfwert1,hilfproz1 );
							}

						}	// i > 30
					}
				}			// i > 9 


			}
		
		
		}
	}	// ueberhaupt was zu tun .....

	if ( (strlen( szTempt)) >0 )
	{
		if ( typ[0] == 'V')	LlDefineVariableExt(hJob, "rtab.zako_txt", szTempt, LL_TEXT, NULL);
		if ( typ[0] == 'F')	LlDefineFieldExt(hJob, "rtab.zako_txt", szTempt, LL_TEXT, NULL);
	}
	< ---- */
}


void textsetzen ( HJOB hJob, int iint, char * hilfsfeld ,  char * ctyp)
{

	// ctyp == "V" : Variablen-Textbaustein
	// ctyp == "f" : lsp.lsp_txt mit diversen Sonderbehandlungen je nach wa_pos_txt_par-Einstellung
	// ctyp == "F" : irgendein Pos-TextBaustein

	// 101014 : ab heute wird zuerst die referenztabelle "atexte" ausgewertet
	// 101014 : Der Walser-Sonderablauf schl�gt genau dann zu wenn als referenztabelle "atexte" drin steht 
	// 161014 : Referenz==atexte UND Tabelle aufpt_verkfrage als Schalter

int k ;
char strpuf[257] ;	// 110205

	szTempt[0] = '\0' ;

	if (! strcmp (clipped ( tt_creferenz[ tt_referenz[iint]] ),"atexte" ))
	{	// 101014 -alles wieder anders am 161014 ......
		if ( strcmp( clipped( tt_tabnam[iint]) , "aufpt_verkfrage"))	 
		{	// 101014 Komplett Sonder-Text f�r Walser - ALLGEMEIN -funktioniert auch im Kopf !!!
			sprintf ( atexte.disp_txt,"");
			sprintf ( atexte.txt,"");
			if  ( ( tt_datetyp[iint] == iDBSMALLINT || tt_datetyp[iint] == iDBINTEGER )
		       && tt_sqllong[iint] > 0L  && tt_referenz[iint] > 0L )
			{
				atexte.txt_nr = tt_sqllong[iint] ;
				if ( atexte.txt_nr > 0 )
					Atexte.leseatexte(atexte.txt_nr);
				}

			if ( strlen(clipped(atexte.disp_txt)))
			{
				if ( ctyp[0] == 'f' || ctyp[0] == 'F') 
					LlDefineFieldExt(hJob, hilfsfeld, atexte.disp_txt, LL_TEXT, NULL);
				if ( ctyp[0] == 'V' ) 
					LlDefineVariableExt(hJob, hilfsfeld, atexte.disp_txt, LL_TEXT, NULL);
			}
			else
			{
				if ( ctyp[0] == 'f' || ctyp[0] == 'F') 
					LlDefineFieldExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
				if ( ctyp[0] == 'V' ) 
					LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
			}
			return;
		}
		else	// 140114 : Supersonder genau aus aus aufpt_verkfrage -ist wohl nur auf Pos-Ebene sinnvoll ?!
		{
			Aufpt_verkfrage.leseaufpt_verkfrage(Temptab.get_lsk_mdn(),Temptab.get_lsk_ls(),Temptab.a_bas_a ());
			if ( ctyp[0] == 'f' || ctyp[0] == 'F') 
			{
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls1",Aufpt_verkfrage.ctxt_ls1, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls2",Aufpt_verkfrage.ctxt_ls2, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls3",Aufpt_verkfrage.ctxt_ls3, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls4",Aufpt_verkfrage.ctxt_ls4, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls5",Aufpt_verkfrage.ctxt_ls5, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls6",Aufpt_verkfrage.ctxt_ls6, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls7",Aufpt_verkfrage.ctxt_ls7, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls8",Aufpt_verkfrage.ctxt_ls8, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls9",Aufpt_verkfrage.ctxt_ls9, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_ls10",Aufpt_verkfrage.ctxt_ls10, LL_TEXT, NULL);

				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech1",Aufpt_verkfrage.ctxt_rech1, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech2",Aufpt_verkfrage.ctxt_rech2, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech3",Aufpt_verkfrage.ctxt_rech3, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech4",Aufpt_verkfrage.ctxt_rech4, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech5",Aufpt_verkfrage.ctxt_rech5, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech6",Aufpt_verkfrage.ctxt_rech6, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech7",Aufpt_verkfrage.ctxt_rech7, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech8",Aufpt_verkfrage.ctxt_rech8, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech9",Aufpt_verkfrage.ctxt_rech9, LL_TEXT, NULL);
				LlDefineFieldExt(hJob, "aufpt_verkfrage.txt_rech10",Aufpt_verkfrage.ctxt_rech10, LL_TEXT, NULL);
			}
			if ( ctyp[0] == 'V' )
			{
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls1",Aufpt_verkfrage.ctxt_ls1, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls2",Aufpt_verkfrage.ctxt_ls2, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls3",Aufpt_verkfrage.ctxt_ls3, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls4",Aufpt_verkfrage.ctxt_ls4, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls5",Aufpt_verkfrage.ctxt_ls5, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls6",Aufpt_verkfrage.ctxt_ls6, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls7",Aufpt_verkfrage.ctxt_ls7, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls8",Aufpt_verkfrage.ctxt_ls8, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls9",Aufpt_verkfrage.ctxt_ls9, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_ls10",Aufpt_verkfrage.ctxt_ls10, LL_TEXT, NULL);

				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech1",Aufpt_verkfrage.ctxt_rech1, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech2",Aufpt_verkfrage.ctxt_rech2, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech3",Aufpt_verkfrage.ctxt_rech3, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech4",Aufpt_verkfrage.ctxt_rech4, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech5",Aufpt_verkfrage.ctxt_rech5, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech6",Aufpt_verkfrage.ctxt_rech6, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech7",Aufpt_verkfrage.ctxt_rech7, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech8",Aufpt_verkfrage.ctxt_rech8, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech9",Aufpt_verkfrage.ctxt_rech9, LL_TEXT, NULL);
				LlDefineVariableExt(hJob, "aufpt_verkfrage.txt_rech10",Aufpt_verkfrage.ctxt_rech10, LL_TEXT, NULL);
			}
			return;
		}

	}

	if ( ctyp[0] == 'f' )	// Nur fuer Feld lsp.lsp_txt sonderbehandlungen
	{
		if ( da_zustxt_wa )
		{
			a_zus_txt.a = Temptab.a_bas_a ();	// Temptab.get_lsp_a() ;
			k = A_zus_txt.opena_zus_txt();
			while (! k )
			{
				k = A_zus_txt.lesea_zus_txt(0) ;
				if (! k )
				{
					if (strlen(szTempt))
					{
						sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
					}
					if (( strlen(szTempt)) + ( strlen(clipped(a_zus_txt.txt))) >= CTEXTMAX  )
						break ;
					sprintf(  szTempt + strlen(szTempt) , "%s", clipped(a_zus_txt.txt) );
				}
			}
		}
	};
	

	// 171114 : Immer pr�fen und (vor-)erg�nzen , falls spboes_par und lsk.kopf_txt)

	if ( intboes )
	{
		if ( ! strcmp ( tt_creferenz[ tt_referenz[iint]] ,"ls_txt" ))
		{
			if ( ! strcmp( clipped( tt_tabnam[iint]) , "lsk"))
			{
				if ( ! strcmp( clipped( tt_feldnam[iint]) , "kopf_txt"))
				{		// strboes_extramgr ist immer bereits geklipped ....... 
					if (( strlen(szTempt)) + ( strlen(strboes_extramgr)) <= CTEXTMAX  )	// eiegtnlich redundant ....
						sprintf(  szTempt + strlen(szTempt) , "%s", strboes_extramgr );
				}
			}
		}
	}


	// Nur was tun, wenn used und vorhanden usw. -iDBDECIMAL eigentlich nur zum Test
	if  ( ( tt_datetyp[iint] == iDBSMALLINT || tt_datetyp[iint] == iDBINTEGER )
		       && tt_sqllong[iint] > 0L  && tt_referenz[iint] > 0L )
	{
		ptxt.nr = tt_sqllong[iint] ;
//		szTempt[0] = '\0' ;
// 110205 : gewisse Redundanz,da wa_pos_txt UND eintrag "lsp_txt" passen muessen
		if (( dwa_pos_txt ) && ( ctyp[0] == 'f' ))
		{

			ntxt.mdn = Temptab.get_lsk_mdn() ;
			ntxt.fil = Temptab.get_lsk_fil() ;
			ntxt.ls = Temptab.get_lsk_ls() ;
			ntxt.posi = Temptab.get_lsp_posi() ;

			k = Ntxt.openntxt(clipped ( tt_creferenz[ tt_referenz[iint]]));
		}
		else
			k = Ptxt.openptxt(clipped ( tt_creferenz[ tt_referenz[iint]]));

		while (! k )
		{
			if (( dwa_pos_txt ) && ( ctyp[0] == 'f' ))
				k = Ntxt.lesentxt() ;
			else
				k = Ptxt.leseptxt() ;

			if (! k )
			{
				if (( dwa_pos_txt ) && ( ctyp[0] == 'f' ))	// 110205
					sprintf ( strpuf, "%s", ntxt.txt ) ;
				else
					sprintf ( strpuf, "%s", ptxt.txt );

				if (strlen(szTempt))
				{
					sprintf( szTempt + strlen(szTempt) , "%c", LL_CHAR_NEWLINE );
				}
			
				if (( strlen(szTempt)) + ( strlen(clipped(strpuf))) >= CTEXTMAX  )
				break ;
				sprintf(  szTempt + strlen(szTempt) , "%s", clipped(strpuf) );
			}
		}
	}
	if ( strlen(szTempt))
	{	
		if ( ctyp[0] == 'V' )
			LlDefineVariableExt(hJob, hilfsfeld, szTempt, LL_TEXT, NULL);
		else
			LlDefineFieldExt(hJob, hilfsfeld, szTempt, LL_TEXT, NULL);
	}
	else
	{
		if ( ctyp[0] == 'V' )
			LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
		else
			LlDefineFieldExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
	}
}


int lack_tab[ 22 ] ;	// 230108 : gut Platz fuer 20 Felder, Info ablegen for further use 

void cronmhd_schreiben( HJOB hJob , char styp [] , int croncount ) 
{

	char hilfsfeld1 [50] ;
	char hilfsfeld2 [50] ;

	// falls man ueberhaupt hier vorbeikommt, dann einfach immer alle felder fuellen
	// es steht ja jedenfalls was drin 
	int suche = 0 ;
	memcpy ( &cronmhd, &cronmhd_null, sizeof(struct CRONMHD)) ;
	cronmhd.mdn = Temptab.get_lsk_mdn() ;
	cronmhd.ls  = Temptab.get_lsk_ls() ;
	cronmhd.posi = Temptab.get_lsp_posi();
	if ( Cronmhd.lesemhd())
	{
		// Nichts gefunden : initialisieren
		cronmhd.mmhd01 = cronmhd.mmhd02 = cronmhd.mmhd03 = cronmhd.mmhd04 = cronmhd.mmhd05 = 0 ;
		cronmhd.mmhd06 = cronmhd.mmhd07 = cronmhd.mmhd08 = cronmhd.mmhd09 = cronmhd.mmhd10 = 0 ;
		cronmhd.dmhd01[0] = cronmhd.dmhd02[0] = cronmhd.dmhd03[0] = cronmhd.dmhd04[0] = cronmhd.dmhd05[0] = '\0' ;
		cronmhd.dmhd06[0] = cronmhd.dmhd07[0] = cronmhd.dmhd08[0] = cronmhd.dmhd09[0] = cronmhd.dmhd10[0] = '\0' ;
	}

/* 01 */	sprintf ( hilfsfeld1, "cronmhd.mmhd01" );
			sprintf ( hilfsfeld2, "%01.4lf", cronmhd.mmhd01 );
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			sprintf ( hilfsfeld1, "cronmhd.dmhd01" );
			sqldatamger (cronmhd.dmhd01, (char *) hilfsfeld2 ) ;
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			else			
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			
/* 02 */	sprintf ( hilfsfeld1, "cronmhd.mmhd02" );
			sprintf ( hilfsfeld2, "%01.4lf", cronmhd.mmhd02 );
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			sprintf ( hilfsfeld1, "cronmhd.dmhd02" );
			sqldatamger (cronmhd.dmhd02, (char *) hilfsfeld2 ) ;
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			else			
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);

/* 03 */	sprintf ( hilfsfeld1, "cronmhd.mmhd03" );
			sprintf ( hilfsfeld2, "%01.4lf", cronmhd.mmhd03 );
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			sprintf ( hilfsfeld1, "cronmhd.dmhd03" );
			sqldatamger (cronmhd.dmhd03, (char *)hilfsfeld2 ) ;
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			else			
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);

/* 04 */	sprintf ( hilfsfeld1, "cronmhd.mmhd04" );
			sprintf ( hilfsfeld2, "%01.4lf", cronmhd.mmhd04 );
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			sprintf ( hilfsfeld1, "cronmhd.dmhd04" );
			sqldatamger (cronmhd.dmhd04, (char *)hilfsfeld2 ) ;
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			else			
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);

/* 05 */	sprintf ( hilfsfeld1, "cronmhd.mmhd05" );
			sprintf ( hilfsfeld2, "%01.4lf", cronmhd.mmhd05 );
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			sprintf ( hilfsfeld1, "cronmhd.dmhd05" );
			sqldatamger (cronmhd.dmhd05, (char*) hilfsfeld2 ) ;
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			else			
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);

/* 06 */	sprintf ( hilfsfeld1, "cronmhd.mmhd06" );
			sprintf ( hilfsfeld2, "%01.4lf", cronmhd.mmhd06 );
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			sprintf ( hilfsfeld1, "cronmhd.dmhd06" );
			sqldatamger (cronmhd.dmhd06, (char *)hilfsfeld2 ) ;
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			else			
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);

/* 07 */	sprintf ( hilfsfeld1, "cronmhd.mmhd07" );
			sprintf ( hilfsfeld2, "%01.4lf", cronmhd.mmhd07 );
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			sprintf ( hilfsfeld1, "cronmhd.dmhd07" );
			sqldatamger (cronmhd.dmhd07, (char *)hilfsfeld2 ) ;
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			else			
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);

/* 08 */	sprintf ( hilfsfeld1, "cronmhd.mmhd08" );
			sprintf ( hilfsfeld2, "%01.4lf", cronmhd.mmhd08 );
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			sprintf ( hilfsfeld1, "cronmhd.dmhd08" );
			sqldatamger (cronmhd.dmhd08, (char *) hilfsfeld2 ) ;
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			else			
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);

/* 09 */	sprintf ( hilfsfeld1, "cronmhd.mmhd09" );
			sprintf ( hilfsfeld2, "%01.4lf", cronmhd.mmhd09 );
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			sprintf ( hilfsfeld1, "cronmhd.dmhd09" );
			sqldatamger (cronmhd.dmhd09, (char *)hilfsfeld2 ) ;
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			else			
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);

/* 10 */	sprintf ( hilfsfeld1, "cronmhd.mmhd10" );
			sprintf ( hilfsfeld2, "%01.4lf", cronmhd.mmhd10 );
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			else
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2, LL_NUMERIC, NULL);
			sprintf ( hilfsfeld1, "cronmhd.dmhd10" );
			sqldatamger (cronmhd.dmhd10, (char *) hilfsfeld2 ) ;
			if ( styp[0] == 'V' )
				LlDefineVariableExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
			else			
				LlDefineFieldExt(hJob, hilfsfeld1 , hilfsfeld2 , LL_TEXT, NULL);
}

void VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{

	char hilfsfeld[256] ;
	char hilfsfeld2[256] ;

	verboteneFelder = 0 ;	// 160113
	sprintf ( cverboteneFelder,"");	// 160113

	int croncount = 0 ;	// 230108

// 091209 : es folgt die Bearbeitung der zerldaten 
// das alles funktioniert nur, falls a_bas.charg_hand und lsp.ls_ident gew�hlt wurden 
	memcpy ( &zerldaten, &zerldaten_null , sizeof ( struct ZERLDATEN) ) ;
	int zerldatenstat = -1 ;
	int a_kun_txt_stat = -1;	// 201213
	int a_erw_txt_stat = -1;	// 190214
	long lls_ident = 0 ;
	if (  Temptab.a_bas_charg_hand() == 9 )	// nur genau dann ...
	{
		lls_ident = Temptab.lsp_ls_ident() ;
		if ( lls_ident < 1 )
		zerldatenstat = 100 ;
	}
	else
		zerldatenstat = 100 ;

	// Jetzt ist entweder alles vorbei oder es besteht Erlaubnis zum Suchen
	 


	int i ;
	i=0;
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;

		if ( zerldatenstat == -1 )	// 091209 
		{
			if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_ZERLDATEN))
			{
				zerldaten.a = Temptab.a_bas_a () ;
				zerldaten.lfd_i = lls_ident ;
				int iii = Zerldaten.lesezerldaten () ;
				if ( iii )
					zerldatenstat = 100 ;
				else
									{
					zerldatenstat = 0 ;
					if ( ! komplettierezerldaten() )	// 160113
					{
						verboteneFelder ++ ;
						sprintf ( cverboteneFelder, "Tabelle zerldaten mit verbotenen Feldern designet" ) ;
						MessageBox( NULL, "Tabelle zerldaten mit verbotenen Feldern designet", "drllls.exe", MB_OK|MB_ICONSTOP);
					}
				}

			}
			// das war es dann :  jedenfalls wurde mit entsprechenden Werten gefuellt 
		}


		sprintf ( hilfsfeld, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);

		if ( ! strcmp( hilfsfeld, "lsk.hinweis" ))
			i = i ;	// sollbreakpoint

		switch ( tt_feldtyp[i] )
		{
		case 1 : // ptabn
			sprintf ( hilfsfeld2, "%s.ptbezk" , hilfsfeld );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
				;
			else
			{
				sprintf ( hilfsfeld2, "%s.ptbez" , hilfsfeld );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
					;
				else
				{
					sprintf ( hilfsfeld2, "%s.ptwer1" , hilfsfeld );
					if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
						;
					else
					{
						sprintf ( hilfsfeld2, "%s.ptwer2" , hilfsfeld );
						if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
							;
						else
						{
							sprintf ( hilfsfeld2, "%s.ptwert" , hilfsfeld );
							if (LlPrintIsVariableUsed(hJob, hilfsfeld2))
								;
							else
							{
								break ;
							} // ptwert
						}	  // ptwer2
					}		  // ptwer1
				}			  // ptbez
			}				  // ptbezk
		
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			case(iDBSMALLINT):
				sprintf ( ptabn.ptwert, "%d", tt_sqllong[i] );
				break ;
			case(iDBINTEGER):
				sprintf ( ptabn.ptwert, "%ld", tt_sqllong[i] );
				break ;
			case(iDBDECIMAL):
				sprintf ( ptabn.ptwert, "%0.0d", tt_sqldouble[i] );
				break ;
			default :	// schlechte Notbremse
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			}
				

			if ( strlen ( clipped ( tt_creferenz[ tt_referenz[i]])) > 0 )	// 091209
			{
				sprintf ( ptabn.ptitem ,"%s" , clipped ( tt_creferenz[ tt_referenz[i]])) ;
			}
			else
				sprintf ( ptabn.ptitem ,"%s",tt_feldnam[i]);
	
			Ptabn.openptabn() ; 
			if ( ! Ptabn.leseptabn())
			{

			ptabschreiben ( hJob, "V" , 1 ,tt_tabnam[i], tt_feldnam[i] ) ;

/* ------> 110506 : auslagern
//				ptbezk
				sprintf ( hilfsfeld, "%s.%s.ptbezk",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{	LlDefineVariableExt(hJob, hilfsfeld , clipped(ptabn.ptbezk), LL_TEXT, NULL);
				}
//				ptbez
				sprintf ( hilfsfeld, "%s.%s.ptbez",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{	LlDefineVariableExt(hJob, hilfsfeld , clipped(ptabn.ptbez), LL_TEXT, NULL);
				}
//				ptwert
				sprintf ( hilfsfeld, "%s.%s.ptwert",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{	LlDefineVariableExt(hJob, hilfsfeld , clipped(ptabn.ptwert), LL_TEXT, NULL);
				}
//				ptwer1
				sprintf ( hilfsfeld, "%s.%s.ptwer1",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{	LlDefineVariableExt(hJob, hilfsfeld , clipped(ptabn.ptwer1), LL_TEXT, NULL);
				}
//				ptwer2
				sprintf ( hilfsfeld, "%s.%s.ptwer2",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{	LlDefineVariableExt(hJob, hilfsfeld, clipped(ptabn.ptwer2), LL_TEXT, NULL);
				}
110506 < ------ */
			}

			else
				memcpy ( &ptabn,&ptabn_null, sizeof(struct PTABN));
			break ;

		case 2 : // Textbaustein // 101014 + 161014 Der ablauf aus aufpt_verkfrage ist eigentlich nur auf Pos-Ebene sinnvoll !!
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				{
						textsetzen (hJob, i, hilfsfeld,"V");
				}
			break ;
		case 3 : // adresse
			
			if (adr.adr == tt_sqllong[i] )
			{
				adr.adr = adr.adr ;	// testbreakpoint
			}
			else
			{
				adr.adr = tt_sqllong[i];
	
				Adr.openadr() ; 
				if ( Adr.leseadr(0))	// immer lesen ODER leeren
				{
					memcpy ( &adr,&adr_null, sizeof(struct ADR));
					adr.adr = tt_sqllong[i];

				}
			}
//			anr als string
			sprintf ( hilfsfeld, "%s.%s.anr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				sprintf ( ptabn.ptwert, "%d", adr.anr );
				sprintf ( ptabn.ptitem ,"anr");
				Ptabn.openptabn() ; 
				if ( ! Ptabn.leseptabn())
// 280911					LlDefineVariableExt(hJob, hilfsfeld, clipped(ptabn.ptbezk), LL_TEXT, NULL);
					LlDefineVariableExt(hJob, hilfsfeld, clipped(ptabn.ptbez), LL_TEXT, NULL);
				else			
					LlDefineVariableExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
			}

//			adr_krz
			sprintf ( hilfsfeld, "%s.%s.adr_krz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
			    LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.adr_krz), LL_TEXT, NULL);
			}
//			adr_nam1
			sprintf ( hilfsfeld, "%s.%s.adr_nam1",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.adr_nam1), LL_TEXT, NULL);
			}
//			adr_nam2
			sprintf ( hilfsfeld, "%s.%s.adr_nam2",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.adr_nam2), LL_TEXT, NULL);
			}

//			adr_nam3	090905
			sprintf ( hilfsfeld, "%s.%s.adr_nam3",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.adr_nam3), LL_TEXT, NULL);
			}

//			ort1
			sprintf ( hilfsfeld, "%s.%s.ort1", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.ort1), LL_TEXT, NULL);
			}
// 060307 A
//			ort2
			sprintf ( hilfsfeld, "%s.%s.ort2", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.ort2), LL_TEXT, NULL);
			}
//			tel
			sprintf ( hilfsfeld, "%s.%s.tel", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.tel), LL_TEXT, NULL);
			}

//			fax
			sprintf ( hilfsfeld, "%s.%s.fax", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.fax), LL_TEXT, NULL);
			}
// 301111		mobil
			sprintf ( hilfsfeld, "%s.%s.mobil", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.mobil), LL_TEXT, NULL);
			}
// 300813	iban
			sprintf ( hilfsfeld, "%s.%s.iban", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.iban), LL_TEXT, NULL);
			}
// 300813	swift
			sprintf ( hilfsfeld, "%s.%s.swift", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.swift), LL_TEXT, NULL);
			}

//			partner
			sprintf ( hilfsfeld, "%s.%s.partner", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob,hilfsfeld, clipped(adr.partner), LL_TEXT, NULL);
			}

// 060307 E
//			plz
			sprintf ( hilfsfeld, "%s.%s.plz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.plz), LL_TEXT, NULL);
			}

//			postfach	080709
			sprintf ( hilfsfeld, "%s.%s.plz_pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				LlDefineVariableExt(hJob, hilfsfeld, clipped(adr.plz_pf), LL_TEXT, NULL);
			sprintf ( hilfsfeld, "%s.%s.pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
				LlDefineVariableExt(hJob, hilfsfeld , clipped(adr.pf), LL_TEXT, NULL);
//			str
			sprintf ( hilfsfeld, "%s.%s.str",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld , clipped(adr.str), LL_TEXT, NULL);
			}
//			iln
			sprintf ( hilfsfeld, "%s.%s.iln",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				LlDefineVariableExt(hJob, hilfsfeld , clipped(adr.iln), LL_TEXT, NULL);
			}
//			adr
			sprintf ( hilfsfeld, "%s.%s.adr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
				LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
			}

// staat	110506
			sprintf ( ptabn.ptwert, "%d", adr.staat );
			sprintf ( ptabn.ptitem ,"staat");
			Ptabn.openptabn() ; 
			if ( ! Ptabn.leseptabn())
			{
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				ptabschreiben ( hJob, "V" , 1 ,hilfsfeld, "staat" ) ;
			}

			break ;

		default :	// Standard-Felder

			if (LlPrintIsVariableUsed(hJob, hilfsfeld))
			{
				if ( lackmann_aktiv )	// 230108 
				{
					if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_CRONMHD))
					{
						lack_tab[croncount] = i ;
						croncount ++ ;
						break ;
					}
				}

				switch ( tt_datetyp[i] )
				{
				case(iDBCHAR):
					sprintf ( szTemp2 ,"%s",clipped ((char *)tt_sqlval[i]));
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_TEXT, NULL);
					break ;
				case(iDBSMALLINT):
					sprintf ( szTemp2, "%01d", tt_sqllong[i] );
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBINTEGER):
					sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDECIMAL):
					sprintf ( szTemp2, "%01.4lf", tt_sqldouble[i] );
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDATUM):
					LlDefineVariableExt(hJob, hilfsfeld , sqldatamger ((char *)tt_sqlval[i], szTemp2) , LL_TEXT, NULL);

					break ;	

				default :	// schlechte Notbremse
					sprintf ( szTemp2 ,"%s",tt_sqlval[i]);
					LlDefineVariableExt(hJob, hilfsfeld , szTemp2, LL_TEXT, NULL);
				break ;
				}
			}
// 060307 A
			if (! ( strncmp ( clipped( tt_feldnam[i]), "tou", 3 )))
			{
				if ( (strlen ( tt_feldnam[i] )) == 3 ||
							 ! (strncmp(tt_feldnam[i] ,"tou_nr",6)))
				{
					sprintf ( hilfsfeld, "%s.%s_bez", clipped (tt_tabnam[i])
								, clipped( tt_feldnam[i] ));
					if (LlPrintIsVariableUsed(hJob, hilfsfeld))
					{
						switch ( tt_datetyp[i] )
						{
						case(iDBCHAR):
								tou.tou = atol( clipped ((char *) tt_sqlval[i]));
						break ;
						case(iDBINTEGER):
						case(iDBSMALLINT):
								tou.tou = tt_sqllong[i] ;
						break ;
						case(iDBDECIMAL):
								tou.tou = (long) tt_sqldouble[i] ;
						break ;
						case(iDBDATUM):
						default :	// schlechte Notbremse
							tou.tou = 0L ;
						break ;
						}
	
						if ( tou.tou > 0L)
						{
							Tou.opentou() ;
							if ( !Tou.lesetou(0))
								LlDefineVariableExt(hJob, clipped( hilfsfeld), clipped(tou.tou_bz), LL_TEXT, NULL);
							else
								LlDefineVariableExt(hJob, clipped( hilfsfeld), " " , LL_TEXT, NULL);
					
						}
						else
								LlDefineVariableExt(hJob, clipped( hilfsfeld), " " , LL_TEXT, NULL);
					}
				}
			}
// 060307 E

			break ;
		} ;	// switch je Feldtyp/Generator

		i ++ ;
	}		// while	

	if (LlPrintIsVariableUsed(hJob, "rangt.nachkpreis"))
	{
		sprintf ( szTemp2 ,"%d",dnachkpreis) ;
		LlDefineVariableExt(hJob, "rangt.nachkpreis" , szTemp2, LL_NUMERIC, NULL);
	}

// 110309
	if (LlPrintIsVariableUsed(hJob, "rangt.variante"))
	{
		sprintf ( szTemp2 ,"%d",dvariante) ;
		LlDefineVariableExt(hJob, "rangt.variante" , szTemp2, LL_NUMERIC, NULL);
	}
// 310812
	if (LlPrintIsVariableUsed(hJob, "rangt.Nutzer"))
	{
		sprintf ( szTemp2, "%s", LUser );
		LlDefineVariableExt(hJob, "rangt.Nutzer" , szTemp2, LL_TEXT, NULL);
	}
	if (LlPrintIsVariableUsed(hJob, "rangt.Heute")||LlPrintIsVariableUsed(hJob, "rangt.Zeit"))
	{
		time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);
		sprintf ( szTemp2 , "%02d.%02d.%04d",
			ltime->tm_mday ,ltime->tm_mon + 1 ,ltime->tm_year + 1900 );

		LlDefineVariableExt(hJob, "rangt.Heute" , szTemp2, LL_TEXT, NULL);

		sprintf ( szTemp2 , "%02d:%02d:%02d",
			ltime->tm_hour, ltime->tm_min, ltime->tm_sec );
		LlDefineVariableExt(hJob, "rangt.Zeit" , szTemp2, LL_TEXT, NULL);
	}




	if (LlPrintIsVariableUsed(hJob, "leer_lsh.party_mdn"))	// 041206
	{
		if ( party_mdn_aktiv == Temptab.get_lsk_mdn())
			sprintf ( szTemp2 ,"%d",party_mdn_aktiv) ;
		else
			sprintf ( szTemp2,"0" ) ;
		LlDefineVariableExt(hJob, "leer_lsh.party_mdn" , szTemp2, LL_NUMERIC, NULL);
	}
// 230108
	if ( lackmann_aktiv && croncount > 0 )
	{
		cronmhd_schreiben ( hJob,"V", croncount ) ;
	}
}


void FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{

	char hilfsfeld[256] ;
	char hilfsfeld2[256] ;

	int croncount = 0 ;	// 230108


// 091209 : es folgt die Bearbeitung der zerldaten 
// das alles funktioniert nur, falls a_bas.charg_hand und lsp.ls_ident gew�hlt wurden 
	memcpy ( &zerldaten, &zerldaten_null , sizeof ( struct ZERLDATEN) ) ;
	int zerldatenstat = -1 ;

	memcpy ( &a_bas_erw, &a_bas_erw_null , sizeof ( struct A_BAS_ERW) ) ;
	int a_bas_erw_stat = -1;	// 190214

	memcpy ( &a_kun_txt, &a_kun_txt_null , sizeof ( struct A_KUN_TXT) ) ;
	int a_kun_txt_stat = -1;	// 201213
	if ( pa_kun_txt0 == -1 )
		a_kun_txt_stat = 100 ;	// Tabelle wird nicht genutzt


	long lls_ident = 0 ;
	if (  Temptab.a_bas_charg_hand() == 9 )	// nur genau dann ...
	{
		lls_ident = Temptab.lsp_ls_ident() ;
		if ( lls_ident < 1 )
		zerldatenstat = 100 ;
	}
	else
		zerldatenstat = 100 ;

	// Jetzt ist entweder alles vorbei oder es besteht Erlaubnis zum Suchen

	int i ;
	i=0;
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;

		if ( zerldatenstat == -1 )	// 091209 
		{
			if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_ZERLDATEN))
			{
				zerldaten.a = Temptab.a_bas_a () ;
				zerldaten.lfd_i = lls_ident ;
				int iii = Zerldaten.lesezerldaten () ;
				if ( iii )
					zerldatenstat = 100 ;
				else
				{
					zerldatenstat = 0 ;
					if ( ! komplettierezerldaten())	// 160113
					{
						verboteneFelder ++ ;
						sprintf ( cverboteneFelder, "Tabelle zerldaten mit verbotenen Feldern designet" ) ;
						MessageBox( NULL, "Tabelle zerldaten mit verbotenen Feldern designet", "drllls.exe", MB_OK|MB_ICONSTOP);
					}
				}

			}
			// das war es dann :  jedenfalls wurde mit entsprechenden Werten gefuellt 
		}


// 201213
		if ( a_kun_txt_stat == -1 )	// nur einmalig je Schleife
		{
			if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_A_KUN_TXT))
			{
				int iii = A_kun_txt.leseakuntxt () ;
				if ( iii )
					a_kun_txt_stat = 100 ;
				else
				{
					a_kun_txt_stat = 0 ;
					if ( ! komplettiereakuntxt())	// 160113
					{
						verboteneFelder ++ ;
						sprintf ( cverboteneFelder, "Tabelle a_kun_txtn mit verbotenen Feldern designet" ) ;
						MessageBox( NULL, "Tabelle a_kun_txt mit verbotenen Feldern designet", "drllls.exe", MB_OK|MB_ICONSTOP);
					}
				}

			}
			// das war es dann :  jedenfalls wurde mit entsprechenden Werten gefuellt 
		}

// 201213

// 190214
		if ( a_bas_erw_stat == -1 )	// nur einmalig je Schleife
		{
			if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_A_BAS_ERW))
			{
				int iii = A_bas_erw.leseabaserw () ;
				if ( iii )
					a_bas_erw_stat = 100 ;
				else
				{
					a_bas_erw_stat = 0 ;
					if ( ! komplettiereabaserw())
					{
						verboteneFelder ++ ;
						sprintf ( cverboteneFelder, "Tabelle a_bas_erw mit verbotenen Feldern designet" ) ;
						MessageBox( NULL, "Tabelle a_bas_erw mit verbotenen Feldern designet", "drllls.exe", MB_OK|MB_ICONSTOP);
					}
				}
			}
			// das war es dann :  jedenfalls wurde mit entsprechenden Werten gefuellt 
		}

// 190214

		sprintf ( hilfsfeld, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);

		switch ( tt_feldtyp[i] )
		{
		case 1 : // ptabn
			sprintf ( hilfsfeld2, "%s.ptbezk" , hilfsfeld );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
				;
			else
			{
				sprintf ( hilfsfeld2, "%s.ptbez" , hilfsfeld );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
					;
				else
				{
					sprintf ( hilfsfeld2, "%s.ptwer1" , hilfsfeld );
					if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
						;
					else
					{
						sprintf ( hilfsfeld2, "%s.ptwer2" , hilfsfeld );
						if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
							;
						else
						{
							sprintf ( hilfsfeld2, "%s.ptwert" , hilfsfeld );
							if (LlPrintIsFieldUsed(hJob, hilfsfeld2))
								;
							else
							{

								break ;
							} //ptwert
						}	  // ptwer2
					}		  // ptwer1
				}			  // ptbez
			}				  // ptbezk
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			case(iDBSMALLINT):
				sprintf ( ptabn.ptwert, "%d", tt_sqllong[i] );
				break ;
			case(iDBINTEGER):
				sprintf ( ptabn.ptwert, "%ld", tt_sqllong[i] );
				break ;
			case(iDBDECIMAL):
				sprintf ( ptabn.ptwert, "%0.0d", tt_sqldouble[i] );
				break ;
			default :	// schlechte Notbremse
				sprintf ( ptabn.ptwert ,"%s",tt_sqlval[i]);
				break ;
			}
				
			sprintf ( ptabn.ptitem ,"%s",tt_feldnam[i]);
	

			if ( strlen ( clipped ( tt_creferenz[ tt_referenz[i]])) > 0 )	// 091209
			{
				sprintf ( ptabn.ptitem ,"%s" , clipped ( tt_creferenz[ tt_referenz[i]])) ;
			}
			else
				sprintf ( ptabn.ptitem ,"%s",tt_feldnam[i]);

			Ptabn.openptabn() ; 
			if ( ! Ptabn.leseptabn())
			{
				ptabschreiben ( hJob, "F" , 1 ,	tt_tabnam[i], tt_feldnam[i] ) ;
		
/* ------> 110506 : auslagern
				
//				ptbezk
				sprintf ( hilfsfeld, "%s.%s.ptbezk",tt_tabnam[i], tt_feldnam[i] );

				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					LlDefineFieldExt(hJob, hilfsfeld , clipped(ptabn.ptbezk), LL_TEXT, NULL);
				}

//				ptbez
				sprintf ( hilfsfeld, "%s.%s.ptbez",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					LlDefineFieldExt(hJob, hilfsfeld , clipped(ptabn.ptbez), LL_TEXT, NULL);
				}

//				ptwert
				sprintf ( hilfsfeld, "%s.%s.ptwert",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					LlDefineFieldExt(hJob, hilfsfeld , clipped(ptabn.ptwert), LL_TEXT, NULL);
				}
//				ptwer1
				sprintf ( hilfsfeld, "%s.%s.ptwer1",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					LlDefineFieldExt(hJob, hilfsfeld , clipped(ptabn.ptwer1), LL_TEXT, NULL);
				}
//				ptwer2
				sprintf ( hilfsfeld, "%s.%s.ptwer2",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					LlDefineFieldExt(hJob, hilfsfeld, clipped(ptabn.ptwer2), LL_TEXT, NULL);
				}
 110506 < ---- */
			}
			break ;

		case 2 : // Textbaustein 
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				{
					if ( ! strcmp( clipped( tt_tabnam[i]) , "lsp") && 
						 ! strcmp( clipped( tt_feldnam[i]), "lsp_txt"))
						textsetzen (hJob, i, hilfsfeld, "f");	// genau bei diesem Feld Sonderbehandlung
					else
					{
						/* ----> 101014 : zu speziell, kann besser gel�st werden ...
						// 101014 : das ist der neueste Walser-Ablauf 
						if ( ! strcmp( clipped( tt_tabnam[i]) , "lsp") && (
							     ! strcmp( clipped( tt_feldnam[i]), "txt_ls") 
							 ||  ! strcmp( clipped( tt_feldnam[i]), "txt_rech")
						 ))
						{
							textsetzen (hJob, i, hilfsfeld, "a");
						}
						else	// 101014 Das war der bisherige "normale" else-Zweig
						<<< ------ */
							textsetzen (hJob, i, hilfsfeld, "F");
					}
				}
			break ;
		case 3 : // adresse

			if ( adr.adr == tt_sqllong[i] ) 
			{
				adr.adr = adr.adr ;	// testbreakpoint
			}
			else
			{
				adr.adr = tt_sqllong[i];
	
				Adr.openadr() ; 
				if ( Adr.leseadr(0))	// immer lesen ODER leeren
				{
					memcpy ( &adr,&adr_null, sizeof(struct ADR));
					adr.adr = tt_sqllong[i];

				}
			}

//			anr als string
			sprintf ( hilfsfeld, "%s.%s.anr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				sprintf ( ptabn.ptwert, "%d", adr.anr );
				sprintf ( ptabn.ptitem ,"anr");
				Ptabn.openptabn() ; 
				if ( ! Ptabn.leseptabn())
//	280911				LlDefineFieldExt(hJob, hilfsfeld, clipped(ptabn.ptbezk), LL_TEXT, NULL);
					LlDefineFieldExt(hJob, hilfsfeld, clipped(ptabn.ptbez), LL_TEXT, NULL);
				else			
					LlDefineFieldExt(hJob, hilfsfeld, " ", LL_TEXT, NULL);
			}

//			adr_krz
			sprintf ( hilfsfeld, "%s.%s.adr_krz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.adr_krz), LL_TEXT, NULL);
			}
//			adr_nam1
			sprintf ( hilfsfeld, "%s.%s.adr_nam1",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.adr_nam1), LL_TEXT, NULL);
			}
//			adr_nam2
			sprintf ( hilfsfeld, "%s.%s.adr_nam2",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.adr_nam2), LL_TEXT, NULL);
			}
//			adr_nam3			090905
			sprintf ( hilfsfeld, "%s.%s.adr_nam3",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.adr_nam3), LL_TEXT, NULL);
			}

//			ort1
			sprintf ( hilfsfeld, "%s.%s.ort1", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.ort1), LL_TEXT, NULL);
			}

// 060307 A
//			ort2
			sprintf ( hilfsfeld, "%s.%s.ort2", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.ort2), LL_TEXT, NULL);
			}
//			tel
			sprintf ( hilfsfeld, "%s.%s.tel", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.tel), LL_TEXT, NULL);
			}
//			fax
			sprintf ( hilfsfeld, "%s.%s.fax", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.fax), LL_TEXT, NULL);
			}
// 301111	    mobil
			sprintf ( hilfsfeld, "%s.%s.mobil", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.mobil), LL_TEXT, NULL);
			}
// 300813	iban
			sprintf ( hilfsfeld, "%s.%s.iban", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.iban), LL_TEXT, NULL);
			}
// 300813	swift
			sprintf ( hilfsfeld, "%s.%s.swift", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.swift), LL_TEXT, NULL);
			}
//			partner
			sprintf ( hilfsfeld, "%s.%s.partner", tt_tabnam[i], tt_feldnam[i] ) ;
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob,hilfsfeld, clipped(adr.partner), LL_TEXT, NULL);
			}

// 060307 E
			
//			plz
			sprintf ( hilfsfeld, "%s.%s.plz",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.plz), LL_TEXT, NULL);
			}

//			postfach 080709
			sprintf ( hilfsfeld, "%s.%s.plz_pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.plz_pf), LL_TEXT, NULL);
			sprintf ( hilfsfeld, "%s.%s.pf",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
				LlDefineFieldExt(hJob, hilfsfeld, clipped(adr.pf), LL_TEXT, NULL);

//			str
			sprintf ( hilfsfeld, "%s.%s.str",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld , clipped(adr.str), LL_TEXT, NULL);
			}
//			iln
			sprintf ( hilfsfeld, "%s.%s.iln",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				LlDefineFieldExt(hJob, hilfsfeld , clipped(adr.iln), LL_TEXT, NULL);
			}
//			adr
			sprintf ( hilfsfeld, "%s.%s.adr",tt_tabnam[i], tt_feldnam[i] );
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{
				sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
				LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
			}

// staat	110506

			sprintf ( ptabn.ptwert, "%d", adr.staat );
			sprintf ( ptabn.ptitem ,"staat");
			Ptabn.openptabn() ; 
			if ( ! Ptabn.leseptabn())
			{
				sprintf ( hilfsfeld, "%s.%s",tt_tabnam[i], tt_feldnam[i] );
				ptabschreiben ( hJob, "F" , 1 ,hilfsfeld, "staat" ) ;
			}

			break ;
		default :	// Standard-Felder 
			if (LlPrintIsFieldUsed(hJob, hilfsfeld))
			{

				if ( lackmann_aktiv )	// 230108 
				{

					if ( ! strcmp (tt_tabnam[i] , PRIV_CRONMHD) )
					{
						lack_tab[croncount] = i ;
						croncount ++ ;
						break ;
					}
				}

				switch ( tt_datetyp[i] )
				{
				case(iDBCHAR):
					sprintf ( szTemp2 ,"%s",clipped ((char *) tt_sqlval[i]));

					LlDefineFieldExt(hJob, hilfsfeld , (LPCSTR)tt_sqlval[i], LL_TEXT, NULL);
					break ;
				case(iDBSMALLINT):
					sprintf ( szTemp2, "%01d", tt_sqllong[i] );
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBINTEGER):
					sprintf ( szTemp2, "%01ld", tt_sqllong[i] );
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDECIMAL):
					sprintf ( szTemp2, "%01.4lf", tt_sqldouble[i] );
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_NUMERIC, NULL);
					break ;
				case(iDBDATUM):
					LlDefineFieldExt(hJob, hilfsfeld , sqldatamger ((char *)tt_sqlval[i], szTemp2) , LL_TEXT, NULL);

					break ;		
				default :	// schlechte Notbremse
					sprintf ( szTemp2 ,"%s",tt_sqlval[i]);
					LlDefineFieldExt(hJob, hilfsfeld , szTemp2, LL_TEXT, NULL);
				break ;
				}
			}
// 060307 A
			if (! ( strncmp ( clipped( tt_feldnam[i]), "tou", 3 )))
			{
				if ( (strlen ( tt_feldnam[i] )) == 3 ||
							 ! (strncmp(tt_feldnam[i] ,"tou_nr",6)))
				{
					sprintf ( hilfsfeld, "%s.%s_bez", clipped (tt_tabnam[i])
								, clipped( tt_feldnam[i] ));
					if (LlPrintIsFieldUsed(hJob, hilfsfeld))
					{
						switch ( tt_datetyp[i] )
						{
						case(iDBCHAR):
								tou.tou = atol( clipped ((char *) tt_sqlval[i]));
						break ;
						case(iDBINTEGER):
						case(iDBSMALLINT):
								tou.tou = tt_sqllong[i] ;
						break ;
						case(iDBDECIMAL):
								tou.tou = (long) tt_sqldouble[i] ;
						break ;
						case(iDBDATUM):

						default :	// schlechte Notbremse
							tou.tou = 0L ;
						break ;
						}

						if ( tou.tou > 0L ) 
						{
							Tou.opentou() ;
							if ( !Tou.lesetou(0))
								LlDefineFieldExt(hJob, clipped( hilfsfeld),	clipped(tou.tou_bz), LL_TEXT, NULL);
							else
								LlDefineFieldExt(hJob, clipped( hilfsfeld),	" " , LL_TEXT, NULL);
						}			
						else
							LlDefineFieldExt(hJob, clipped( hilfsfeld), " " , LL_TEXT, NULL);

					}
				}
			}
// 060307 E
			break ;
		} ;	// switch je Feldtyp/Generator



		i ++ ;
	}		// while
	if (LlPrintIsFieldUsed(hJob, "rangt.nachkpreis"))
	{
		sprintf ( szTemp2 ,"%d",dnachkpreis);
		LlDefineFieldExt(hJob, "rangt.nachkpreis" , szTemp2, LL_NUMERIC, NULL);
	}

// 310812
	if (LlPrintIsFieldUsed(hJob, "rangt.Nutzer"))
	{
		sprintf ( szTemp2, "%s", LUser );
		LlDefineFieldExt(hJob, "rangt.Nutzer" , szTemp2, LL_TEXT, NULL);
	}
	if (LlPrintIsFieldUsed(hJob, "rangt.Heute")||LlPrintIsFieldUsed(hJob, "rangt.Zeit"))
	{
		time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);
		sprintf ( szTemp2 , "%02d.%02d.%04d",
			ltime->tm_mday ,ltime->tm_mon + 1 ,ltime->tm_year + 1900 );
		LlDefineFieldExt(hJob, "rangt.Heute" , szTemp2, LL_TEXT, NULL);

		sprintf ( szTemp2 , "%02d:%02d:%02d",
			ltime->tm_hour, ltime->tm_min, ltime->tm_sec );
		LlDefineFieldExt(hJob, "rangt.Zeit" , szTemp2, LL_TEXT, NULL);
	}

// 110309
	if (LlPrintIsFieldUsed(hJob, "rangt.variante"))
	{
		sprintf ( szTemp2 ,"%d",dvariante) ;
		LlDefineFieldExt(hJob, "rangt.variante" , szTemp2, LL_NUMERIC, NULL);
	}

	if (LlPrintIsFieldUsed(hJob, "leer_lsh.party_mdn"))	// 041206
	{
		if ( party_mdn_aktiv == Temptab.get_lsk_mdn())
			sprintf ( szTemp2 ,"%d",party_mdn_aktiv);
		else
			sprintf ( szTemp2,"0" ) ;
		LlDefineFieldExt(hJob, "leer_lsh.party_mdn" , szTemp2, LL_NUMERIC, NULL);
	}

	// 230108
	if ( lackmann_aktiv  && croncount > 0 )
	{
		cronmhd_schreiben ( hJob,"F", croncount ) ;
	}

}



//=============================================================================
void CMainFrame::OnEditList()
//=============================================================================
{


	/* --->
	char envchar[512] ;

	
	CHAR szFilename[128+1] = "*.lst";
	HWND hWnd = m_hWnd;
	HJOB hJob;
    char *etc;
	char filename[512];
	etc = getenv ("BWS");
    sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	FILENAME_DEFAULT = filename;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}


	
	
	// Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden (Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");



	char * penvchar = getenv ("BWS");
	sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);

	LlSetPrinterDefaultsDir(hJob, envchar) ;	// GrJ Printer-Datei
	penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// GrJ Vorschau-Datei


	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");


	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
		{
	        LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    // GR: Zur�cksetzen der internen Feld-Puffer
    LlDefineFieldStart(hJob);

//	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

	if (Listenauswahl != 1)  
    {
	   	sprintf ( szFilename, FILENAME_DEFAULT );
	}
	// GR: Aufruf des Designers
    if (LlDefineLayout(hJob, hWnd, "Design Rechnung", LL_PROJECT_LIST, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "Design Rechnung", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
< ----- */
	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);

}


//=============================================================================
void CMainFrame::OnPrintLabel()
//=============================================================================
{
	DoLabelPrint();
}


#ifdef OLD_DCL
char *uPPER (string)
char *string;
#else
char *uPPER (char *string)
#endif
{

   for( char *p = string; p < string + strlen( string ); p++ )
   {
      if( islower( *p ) )
			( p[0] = _toupper( *p ) );
   }
   return string ;
}


// CR am Stringende etfernen.

void cr_weg (char *string)
{
 for (; *string; string += 1)
 {
  if (*string == (char) 13)
    break;
  if (*string == (char) 10)
    break;
 }
 *string = 0;
 return;
}

// 300306 : Stringlaenge mit auswerten 
int strupcmpi (char *str1, char *str2, int len, int len2)
// int strupcmpi (char *str1, char *str2, int len)
{
 short i;
 char upstr1;
 char upstr2;

 if ( len < len2 ) return -1 ;	// 300306
 if ( len > len2 ) return  1 ;	// 300306


 for (i = 0; i < len; i ++, str1 ++, str2 ++)
 {
  if (*str1 == 0)
    return (-1);
  if (*str2 == 0)
    return (1);
  upstr1 = toupper((int) *str1);
  upstr2 = toupper((int) *str2);
  if (upstr1 < upstr2)
  {
   return(-1);
  }
  else if (upstr1 > upstr2)
  {
   return (1);
  }
 }
 return (0);
}

// static char * buffer ;

static char buffer[1024] ;

static char buffere[1024] ;

int next_char_ci (char *string, char tzeichen, int i)
// Naechstes Zeichen != Trennzeichen suchen.

{
       for (;string [i]; i ++)
       {
               if (string[i] != tzeichen)
               {
                                   return (i);
               }
       }
       return (i);
}

// 300306 : der 2. Parameter ist dann ein endloser String bis zum #-Trenner oder CR/LF
short splite (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
 					zeichen ='#' ;
       }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}


short split (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
 // if (buffer == (char *) 0) buffer = (char *) malloc (0x1000);
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}

// 031109
short splitdoll (char *string)
{

 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = '$';
 // if (buffer == (char *) 0) buffer = (char *) malloc (0x1000);
 wz = j = 0;
 len = (int) strlen (string);
 wz = 1;
 i = next_char_ci (string, zeichen, 0);
 if (i >= len) return (0);
 wort [wz] = buffer;
 wz ++;
 for (; i < len; i ++, j ++)
 {
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
  }
  buffer [j] = (char) 0;
  return (wz - 1);
}


int holedatenausdatei(void)
{
int anz;
char buffer [512];
char token[64] ;
int gefunden ;
gefunden = 0 ;

FILE *fp;
	fp = fopen (datenausdateiname, "r");
    if (fp == (FILE *)NULL) return FALSE;
    while (fgets (buffer, 511, fp))
    {
		cr_weg (buffer);
        anz = split (buffer);
        if (anz < 2) continue;

// 300306  
		sprintf ( token, "ALTERNATETYP" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
//			anz = splite(buffer) ;
			strcpy (alternat_type, clipped (uPPER (wort [2])));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}

		// funktioniert vorerst NICHT mit space-Namen
		sprintf ( token, "ALTERNATENAME" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
		{
			anz = splite(buffer) ;
			strcpy (alternat_file, clipped (wort [2]));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}

		// funktioniert vorerst NICHT mit space-Namen
		sprintf ( token, "ALTERNATEDIR" ); 
       if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {
			anz = splite(buffer) ;
			strcpy (alternat_dir, clipped (wort [2]));
			gefunden ++ ; alternat_ausgabe = 1 ;
			continue ;
		}


		sprintf ( token, "NAME" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
										(int)strlen(clipped(wort[1]))) == 0)
        {
			strcpy (myform_nr, clipped (uPPER (wort [2])));
			gefunden ++ ;
			continue ;
		}
// 121104
		sprintf ( token, "USER" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)

		{
			strcpy (User, clipped (uPPER (wort [2])));
			gefunden ++ ;
			continue ;
		}

		sprintf ( token, "ANZAHL" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {

			danzahl = atoi (wort[2]);
			if ( danzahl < 1 ) danzahl = 1 ;	// 031109
			gefunden ++ ;
			continue ;
        }

// 250505
		sprintf ( token, "DEBUG" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)
        {

			ddebug = atoi (wort[2]);
			continue ;
        }


		sprintf ( token, "LABEL" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token),
								(int)strlen(clipped(wort[1]))) == 0)
        {
			GlobalLila = atoi( wort[2]) ;
			gefunden ++ ;
			continue ;
		}
		sprintf ( token, "DRUCK" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token) ,
							(int)strlen(clipped(wort[1]))) == 0)
        {

			DrMitMenue = atoi (wort[2]);

			gefunden ++ ;
			continue ;
        }
// 110309 
		sprintf ( token, "MITVARIANTE" ); 
        if (strupcmpi (wort[1], token, (int)strlen (token) ,
							(int)strlen(clipped(wort[1]))) == 0)
        {

			dvariante = atoi (wort[2]);
			gefunden ++ ;
			continue ;
        }



	}
	fclose (fp);
	if ( gefunden ) return TRUE;

	return FALSE ;
}

// Nicht besonders elegant, aber es funktioniert ....
int holerangesausdatei(char * token, char * wert1, char * wert2)
{
int anz;
char buffer [512];
FILE *fp;

	wert1[0] = '\0' ;
	wert2[0] = '\0' ;
	fp = fopen (datenausdateiname, "r");
    if (fp == (FILE *)NULL) return FALSE;
    while (fgets (buffer, 511, fp))
    {
		cr_weg (buffer);
        anz = split (buffer);
        if (anz < 2) continue;

        if (strupcmpi (wort[1], token, (int)strlen (token),
							(int)strlen(clipped(wort[1]))) == 0)

        {
			strcpy (wert1, clipped (wort [2]));
			strcpy (wert2, clipped (wort [3]));
			return TRUE ;
		}
	}
	fclose (fp);
	return FALSE ;
}


//=============================================================================
void CMainFrame::InterpretCommandLine()
//=============================================================================
{

// 170107 : falls als fuehrender Parameter "-Nutzer Nutzername" kommt, kann man die 
//	Druckerwahl nutzerspezifisch fuehren, ansonsten beenden die Paramter
// "-DATEI" bzw. die Druckkommandos die Interpretation der Kommandozeile

    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");

    char *px;
	char p[299];
//	EntwurfsModus = 1 ;

	SetWindowText( "LS-Druck" ) ;	// 120210

	EntwurfsModus = 0 ;
	WahlModus = 0;	// 171204
	PMWahlModus = 0 ;	// 041206
	GlobalLila = 0 ;

	ddebug = 0 ;	// 250505
	debugfp = (FILE *) NULL ;	// 250505

	datenausdatei = 0 ;		// 300904

	alternat_ausgabe = 0 ;	// 300306 
	danzahl = 1 ;			// 121104
	User[0] = '\0' ;		// 121104
	LUser[0] = '\0'	;		// 310812
    Token.NextToken ();
	while ((px = Token.NextToken ()) != NULL)
	{
		sprintf ( p ,"%s", clipped(uPPER(px)));

		if (strcmp (p, "-H") == 0)
		{
			MessageBox ("161104 : Aufruf : drllls -datei DATEINAMEabsolut ",
				"oder drllls -wahl oder drllls -name xxxxxXXXXX [-dr 0|1][-l] ",
	                            MB_OK);

			EntwurfsModus = 0 ;
              return;
		}

	// 300904
		if (strcmp (p, "-DATEI") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( datenausdateiname ,"%s", clipped(uPPER(px)));
			if ( strlen ( datenausdateiname ) > 1 )
			{
				EntwurfsModus = 0 ;
				// Achtung : klappt nicht bei Namen mit ner Luecke ......	

				if ( holedatenausdatei())
				{
					datenausdatei = 1 ;
					break ;
				}
			}
		}

		if (strcmp (p, "-NAME") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( myform_nr ,"%s", clipped ( uPPER (px)));
			EntwurfsModus = 0 ;
//			return ;
			continue ;	// 031109
		}

		if (strcmp (p, "-DR") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;
            DrMitMenue = (atoi (px));
			EntwurfsModus = 0 ;
			continue ;	// 031109
		}

//  171204
		if (strcmp (p, "-WAHL") == 0)
		{
			WahlModus = 1 ;
			sprintf ( myform_nr , "53100" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r Lieferscheine") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemischte Parametrierung
		}
// 041206
		if (strcmp (p, "-WAHLPM") == 0)
		{
			PMWahlModus = 1 ;
			sprintf ( myform_nr , "531000" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r Party-Lieferscheine") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemischte Parametrierung
		}

//  090206
		if (strcmp (p, "-WAHL2") == 0)
		{
			WahlModus = 2 ;
			sprintf ( myform_nr , "53100" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r Lieferscheine") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemischte Parametrierung
		}

//  041206
		if (strcmp (p, "-WAHLPM2") == 0)
		{
			PMWahlModus = 2 ;
			sprintf ( myform_nr , "531000" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r Party-Lieferscheine") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemischte Parametrierung
		}

//  090206
		if (strcmp (p, "-WAHL3") == 0)
		{
			WahlModus = 3 ;
			sprintf ( myform_nr , "53100" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r Lieferscheine") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemischte Parametrierung
		}

//  041206
		if (strcmp (p, "-WAHLPM3") == 0)
		{
			PMWahlModus = 3 ;
			sprintf ( myform_nr , "531000" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r Party-Lieferscheine") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemischte Parametrierung
		}
//  090206
		if (strcmp (p, "-WAHL4") == 0)
		{
			WahlModus = 4 ;
			sprintf ( myform_nr , "53100" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r Lieferscheine") ;

			continue ;	// 031109
//			break ;	// moeglichst keine gemischte Parametrierung
		}
//  041206
		if (strcmp (p, "-WAHLPM4") == 0)
		{
			PMWahlModus = 4 ;
			sprintf ( myform_nr , "531000" ) ;	

			SetWindowText ( "Druckerwahl-Dialog f�r PArty-Lieferscheine") ;

			continue ;	// 031109
			break ;	// moeglichst keine gemischte Parametrierung
		}

// 170107
		if (strcmp (p, "-NUTZER") == 0)
		{	// Damit kann man es so regeln, dass als LETZTER token auch leerer Nutzer funktioniert

			px = Token.NextToken ();
			if (px == NULL) break;
            sprintf ( User ,"%s", px);
			sprintf ( LUser, "%s" ,clipped( px )) ;	// 310812

			continue ;	// 031109

		}

// Label verwenden
		if (strcmp (p, "-L") == 0)
		{
			GlobalLila = 1 ;
			continue ;	// 031109
		}

// 031109
		if (strcmp (p, "-ANZAHL") == 0)
		{
			px = Token.NextToken ();
			if (px == NULL) break;

			danzahl = (atoi (px));
			if ( danzahl < 1 ) danzahl = 1 ;

			continue ;	// 031109
		}

	}

}


//=============================================================================
void CMainFrame::OnPrintReport()
//=============================================================================
{
    
	InterpretCommandLine() ;

	if ( !GlobalLila )
	    DoListPrint();
	else
		DoLabelPrint();
}

//=============================================================================
void CMainFrame::DoLabelPrint()
//=============================================================================
{
	/* -> das ist alles redundant ----> 090506

    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lbl", szTemp[50], szTemp2[505] **, szBoxText[200] ** ;
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;

// QQQQQQQ Labelprint

	strcpy(szTemp, " ");
	char *etc;
	char filename[512];

	CString sMedia;

	char envchar[512];
	etc = getenv ("BWS");
	if ( User[0] == '\0' )
		sprintf (filename, "%s\\format\\LL\\%s.lbl", etc, myform_nr);
	else
		sprintf (filename, "%s\\format\\LL\\%s\\%s.lbl", etc,User, myform_nr);

// QQQQQQQ Labelprint

	FILE *fp ;
	fp = fopen ( filename, "r" ); 
	if ( fp== NULL )
	{
		User[0] = '\0' ;
		sprintf (filename, "%s\\format\\LL\\%s.lbl", etc, myform_nr);
	}
	else fclose ( fp) ;

// QQQQQQQ Labelprint


	FILENAME_DEFAULT = clipped (filename);

	if ( strlen(myform_nr) < 4 || strlen( myform_nr) > 10 )
	{
		MessageBox("Ung�ltiger Labelname!", "Usage : drllls {-name xxxxxXXXXX [-dr 0|1][-l]| -datei Datei}",
										MB_OK|MB_ICONSTOP);
		return;
    }

// QQQQQQQ Labelprint

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "drllls-Exception 010", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"drllls-Exception 020",
					MB_OK|MB_ICONSTOP);
		return;
    }

	// Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");

// QQQQQQQ Labelprint

	char * penvchar = getenv ("BWS");
	sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);
	if ( User[0] == '\0' )	// 121104
		sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);
	else
		sprintf ( envchar ,"%s\\format\\LL\\%s\\" , penvchar, User);

	LlSetPrinterDefaultsDir(hJob,envchar) ;	// Printer-Datei
	penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// Vorschau-Datei

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// US: Choose new expression mode with formula support
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);
	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);
	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog

// QQQQQQQ Labelprint
   	
	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
   		{
	    	if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "drllls-Exception 030",
				MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	
			return;
		}
	}

// QQQQQQQ Labelprint

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	dbClass.opendbase ("bws");

	Temptab.structprep(myform_nr) ;

    Variablendefinition (hJob);
//    Felderdefinition (hJob);

    // GR: Druck starten

// QQQQQQQ Labelprint

    if ( DrMitMenue == 1)
    {
		int retc1 = LlPrintWithBoxStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...");
		if ( retc1 < 0 )
		{
			sprintf ( dmess, " drllls-Exception : %d", retc1 );
			MessageBox("Error While Printing", dmess , MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			Temptab.drfree();
			dbClass.closedbase ("bws");
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

// QQQQQQQ Labelprint

	else	// Druck pur immer auf den Drucker
	{
		int retc2 ;
		if ( alternat_ausgabe )	// 300306 
		{

			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
 
			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.Path",alternat_dir);
			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.File",alternat_file);

			if ( ! strcmp ( alternat_type , "PDF") )
			{
				                                                                                             // min defa max
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}

			retc2 = LlPrintStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							(int) NULL );

		}
		else
		{

			retc2 = LlPrintStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_NORMAL,
							(int) NULL ** dummy ** );

		}

** --->
	if (LlPrintStart(hJob, LL_PROJECT_LABEL, szFilename,LL_PRINT_NORMAL,(int) NULL ) < 0)
< ---- **

		if ( retc2 < 0 )
		{
			sprintf ( dmess, " drllls-Exception : %d", retc2 );
	
			MessageBox("Error While Printing", dmess, MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			Temptab.drfree() ;
			dbClass.closedbase ("bws");
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
   } 

// QQQQQQQ Labelprint

	// GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);

// QQQQQQQ Labelprint

	if ( DrMitMenue == 1)
    {
		if (LlPrintOptionsDialog(hJob, hWnd, "Drucker-Einrichtung") < 0)
		{
			LlPrintEnd(hJob,0);
			LlJobClose(hJob);
			Temptab.drfree() ;
		    dbClass.closedbase ("bws");
			if (Hauptmenue == 0)
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
	        return;
		}
		//GR: Druckziel abfragen
		LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		sMedia.ReleaseBuffer();
    }
	else
	{
		// 300306
		if ( alternat_ausgabe )
		{
			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);

			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Path", alternat_dir);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.File", alternat_file);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Quiet", "1");
//			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_PRINTDST_FILENAME, alternat_file);

			if ( ! strcmp ( alternat_type , "PDF") )
			{
// 080905 : Mit diesen Parametern ergibt sich je nach Vorlage ein Reduktionsfaktor 5 .. 10 gegenueber
// der default-Parametrierung beim Einsatz von Logos und RTF-Texten 				
// Performance-Fresser bleibt RTF-Text, Platz-Fresser bleiben bitmaps und Bilder
				                                                                                             // min defa max
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}

		}
		else
		{
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		}
		sMedia.ReleaseBuffer();

	}

// QQQQQQQ Labelprint

//	LlPrintCopyPrinterConfiguration (hJob, szneupfilename, , LL_PRINTERCONFIG_SAVE );
//	LlPrintCopyPrinterConfiguration (hJob, szpfilename, , LL_PRINTERCONFIG_RESTORE );


	char szPrinter[80], szPort[20];
	int  nRecCount = 30, nErrorValue = 0, nRecno, nLastPage;

// QQQQQQQ Labelprint

	LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

    nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;


	// GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	int abbruch = Temptab.opentemptab();
	if ( abbruch == -3 ) 
    {
//		sprintf (dmess, "User-Abbruch ");
//		MessageBox (dmess, "", MB_ICONERROR);
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
			Temptab.drfree () ;
			dbClass.closedbase ("bws");
			if (Hauptmenue == 0)  //#LuD
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
    		return;

	}

	int anzahl = Temptab.getanzahl(); 

// QQQQQQQ Labelprint

	int di = Temptab.lesetemptab (0);   //       1.DATENSATZ lesen f�r Variablen�bergabe
											// nErrorValue als Paramter ist hier nicht gut
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				Temptab.drfree () ;
				dbClass.closedbase ("bws");
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}

// QQQQQQQ Labelprint

	while ( ( !di )
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA))
   	{
		for ( int dox = 0 ; dox < anzahl ; dox ++ )
		{
			VariablenUebergabe ( hJob, szTemp2, nRecno );	// Variablen vor derListe .....
** ---->
		// Labelprint
    	// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    	sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    	nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        if (nErrorValue == LL_ERR_USER_ABORTED)
    	{
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
    		return;
    	}
< ---- **

			nErrorValue = LlPrint(hJob);
		}
		di = Temptab.lesetemptab (0) ;	//       DATENSATZ lesen
	}

// QQQQQQQ Labelprint

	//GR: Druck beenden
	LlPrintEnd(hJob,0);

	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) l�schen
        LlPreviewDisplay(hJob, szFilename, penvchar, hWnd);

        LlPreviewDeleteFiles(hJob, szFilename,penvchar);
    }

	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
	Temptab.drfree();
	dbClass.closedbase ("bws");
	if (Hauptmenue == 0)  
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}
	090506 : labelprint ist hier redundant < ----- */
}

// 160305 : vorsortiert sollten die Artikel ohnehin sein
// 041206 : bei Leihartikeln erst mal nicht sortiert, ist aber hoffentlich egal ..
void lade_leerg( double ia , int istk, double ipr, char * ibez, double ia_gew )
{
	int ipoint ;
	if ( ia == 0.0 && istk == 0 )
	{
		for ( ipoint=0 ;ipoint < LG_MAX ; ipoint++ )
		{
			leerg_st[ipoint] = 0;
			leerg_zu[ipoint] = 0;
			leerg_ab[ipoint] = 0;
			leerg_a [ipoint] = 0.0 ;
			leerg_p [ipoint] = 0.0 ;
			leerg_g [ipoint] = 0.0 ;	// 230609 
			leerg_bz [ipoint][0] = '\0' ;	// 221209 
		}
		return ;
	}

	for ( ipoint=0 ;ipoint < LG_MAX ; ipoint++ )
	{
		if ( leerg_a [ipoint] == 0.0)
		{
			leerg_a[ipoint] = ia ;
			leerg_p[ipoint] = ipr ; 
			leerg_g[ipoint] = ia_gew ;	// 230609
			sprintf( leerg_bz[ipoint],"%s",ibez );
			if (istk > 0 )
				leerg_zu[ipoint] = istk ;
			else
				leerg_ab[ipoint] = 0 - istk ;
			return ;
		}

		if ( leerg_a [ipoint] == ia)
		{
			if (istk > 0 )
				leerg_zu[ipoint] += istk ;
			else
				leerg_ab[ipoint] += (0 - istk) ;
			return ;
		}
	}
}

void SchreibeLeergut(HJOB hJob, char * szTemp2)
{

	int ipoint ;
	char szTemp3[30] ;

	// 041206 : Kein Bestand fuer Leihartikel
	if ( party_mdn_aktiv != Temptab.get_lsk_mdn())
	{


		// hier sind kun.kun_leer_kz bzw. Filial-LS bereits integriert
		if ( ! dkun_leer ) return ;

		leer_lsdr.ls = Temptab.get_lsk_ls();
		sprintf ( leer_lsdr.blg_typ, "L" ) ;
		leer_lsdr.fil = 0 ;
		leer_lsdr.mdn = Temptab.get_lsk_mdn() ;

		Leer_lsdr.openlsdr();

		while ( ! Leer_lsdr.leselsdr(0) )
		{
			for ( ipoint = 0 ; ipoint < LG_MAX ; ipoint ++ )
			{
				if ( leer_lsdr.a == leerg_a[ipoint])  
				{
					leerg_st[ipoint] = leer_lsdr.stk ;
					break ;
				};

				if ( leerg_a[ipoint] == 0.0 )
				{
					leerg_a [ipoint] = leer_lsdr.a ;
					leerg_st[ipoint] = leer_lsdr.stk ;
					a_bas.a = leer_lsdr.a ;
					A_bas.opena_bas() ;
					if ( ! A_bas.lesea_bas(0))
						sprintf ( leerg_bz[ipoint],"%s", a_bas.a_bz1 );
					break ;
				};
			}
			
		}
	}

/* ----> 041206 : Altzustand 
	for ( ipoint = 0 ; ipoint < LG_MAX ; ipoint ++ )
	{
		if ( leerg_a[ipoint] == 0.0 ) break ;	// es ist vollbracht
< ---- */

	if ( conradi )	// 221209 : total nach a sortieren 
	{
		for ( ipoint = 1 ; ipoint < LG_MAX ; ipoint ++ )
		{
			if ( leerg_a[ipoint] < 0.1 ) break ;	// 0.0 included

			if ( leerg_a[ipoint] < leerg_a[ipoint - 1] )
			{
				
				double ta = leerg_a[ipoint -1] ;
				leerg_a[ipoint -1] = leerg_a[ipoint] ;
				leerg_a[ipoint] = ta ;

				int tst = leerg_st[ipoint -1] ;
				leerg_st[ipoint -1] = leerg_st[ipoint] ;
				leerg_st[ipoint] = tst ;

				int tzu = leerg_zu[ipoint -1] ;
				leerg_zu[ipoint -1] = leerg_zu[ipoint] ;
				leerg_zu[ipoint] = tzu ;

				int tab = leerg_ab[ipoint -1] ;
				leerg_ab[ipoint -1] = leerg_ab[ipoint] ;
				leerg_ab[ipoint] = tab ;

				double tp = leerg_p[ipoint -1] ;
				leerg_p[ipoint -1] = leerg_p[ipoint] ;
				leerg_p[ipoint] = tp ;

				double tg = leerg_g[ipoint -1] ;
				leerg_g[ipoint -1] = leerg_g[ipoint] ;
				leerg_g[ipoint] = tg ;

				char tbz[30] ;
				sprintf ( tbz ,"%s" ,leerg_bz[ipoint -1]) ;
				sprintf ( leerg_bz[ipoint -1], "%s", leerg_bz[ipoint]) ;
				sprintf ( leerg_bz[ipoint] ,"%s" , tbz ) ;

				ipoint = 0 ;	// faengt dann wieder bei 1 an .....

			}
		}
	}




	for ( ipoint = 1 ; ipoint < LG_MAX ; ipoint ++ )
	{
	    sprintf(szTemp2, "leer_lsh.a%.0d", ipoint );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0lf" , leerg_a[ipoint - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2 , szTemp3 , LL_NUMERIC, NULL);
		}
		sprintf(szTemp2, "leer_lsh.a_bez%.0d", ipoint );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			LlDefineFieldExt(hJob, szTemp2  , leerg_bz[ipoint - 1], LL_TEXT, NULL);
		}

		sprintf(szTemp2, "leer_lsh.stk_zu%.0d", ipoint );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0d", leerg_zu[ipoint - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2, szTemp3, LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_lsh.stk_ab%.0d", ipoint );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0d", leerg_ab[ipoint - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2, szTemp3, LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_lsh.stk%.0d", ipoint );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%.0d", leerg_st[ipoint - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2 , szTemp3, LL_NUMERIC, NULL);
		}

		sprintf(szTemp2, "leer_lsh.pr_vk%.0d", ipoint );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%1.2lf", leerg_p[ipoint - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2 , szTemp3, LL_NUMERIC, NULL);
		}

// 230609 
		sprintf(szTemp2, "leer_lsh.a_gew%.0d", ipoint );
		if (LlPrintIsFieldUsed(hJob, szTemp2))
		{
			sprintf ( szTemp3, "%1.2lf", leerg_g[ipoint - 1] ) ;
			LlDefineFieldExt(hJob, szTemp2 , szTemp3, LL_NUMERIC, NULL);
		}

	}

}
// komplettieren der Leergutausgabe






//=============================================================================
void CMainFrame::DoListPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lst", szTemp[50], szTemp2[505]/*, szBoxText[200]*/ ;
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;
	strcpy(szTemp, " ");
	char *etc;
	char filename[512];

	CString sMedia;

	char envchar[512];
	etc = getenv ("BWS");


	FILE *fp ;

	if ( User[0] == '\0' )
        sprintf (filename, "%s\\format\\LL\\%s.lst", etc, myform_nr);
	else
        sprintf (filename, "%s\\format\\LL\\%s\\%s.lst", etc,User, myform_nr);

	sprintf ( LUser, "%s", User ) ;	// 170107
 
	fp = fopen ( filename, "r" ); 
	if ( fp== NULL )
	{
		if ( ! NutzerIgnorieren )	// 090109
		{
			if (WahlModus || PMWahlModus )
			{
				MessageBox("Nutzer-Dir nicht ok", "drllls.exe", MB_OK|MB_ICONSTOP);
				return;
			}
		}

		User[0] = '\0' ;
		sprintf (filename, "%s\\format\\LL\\%s.lst", etc, myform_nr);
	}
	else fclose ( fp) ;

// 121104 E


	FILENAME_DEFAULT = clipped (filename);
	if ( strlen(myform_nr) < 4 || strlen( myform_nr) > 10 )
	{
		MessageBox("Ung�ltiger Listenname!", "Usage :  drllls {-name xxxxxXXXXX [-dr 0|1][-l]| -datei Datei}", MB_OK|MB_ICONSTOP);
		return;
    }


	// GR: Initialisieren von List & Label.

	if ( zeittest ) schreibezeit("Vor Job-Open \n" ) ;

	hJob = LlJobOpen(LUL_LANGUAGE);

	if ( zeittest ) schreibezeit("Nach Job-Open \n" ) ;

	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "drllls-Exception 010", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"drllls-Exception 020",
					MB_OK|MB_ICONSTOP);
		return;
    }


 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
	// C�ELL11 : fuer LuL.90 : LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");

	if ( zeittest ) schreibezeit("vor Lizenz \n" ) ;
#ifdef LL19
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "V2XSEQ");
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "ht8gOw");
#else				// LL11 als default bis 120911
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");
#endif
#endif
	if ( zeittest ) schreibezeit("Nach Lizenz \n" ) ;

	char * penvchar = getenv ("BWS");
	sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);

	if ( User[0] == '\0' )	// 121104
		sprintf ( envchar ,"%s\\format\\LL\\" , penvchar);
	else
		sprintf ( envchar ,"%s\\format\\LL\\%s\\" , penvchar, User);

	LlSetPrinterDefaultsDir(hJob,envchar) ;	// Printer-Datei
	penvchar = getenv ("TMPPATH");

	LlPreviewSetTempPath(hJob,penvchar) ;	// Vorschau-Datei

	
		//GR: Exporter aktivieren
#ifdef LL19
// gar nix, default ist ausreichend
#else
#ifdef LL12
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll12ex.llx");
#else
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");
#endif
#endif

	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );


	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
	   	{
    		if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "drllls-Exception 030", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	
			return;
		}
	}

	// GR: Zur�cksetzen der internen Variablen-Puffer

	if ( zeittest ) schreibezeit("Vor V+F-Variablen \n" ) ;

	LlDefineVariableStart(hJob);
    LlDefineFieldStart(hJob);

	if ( zeittest ) schreibezeit("Vor daba-Open \n" ) ;

	dbClass.opendbase ("bws");
	if ( zeittest ) schreibezeit("nach daba-Open \n" ) ;

// 230609 
	sprintf ( sys_par.sys_par_nam,"sonschill2" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	sonschill2 = Sys_par.lesesys_par(0);	// 221209 : hier stand bisher redeckerpar anstatt sonschill2
	if ( sonschill2 || sys_par.sys_par_wrt[0] == '\0')
	{
		sonschill2 = 0 ;
	}
	else
	{
		sonschill2 = 0;
		sonschill2 = atoi ( sys_par.sys_par_wrt ) ;
	}

// 221209 
	sprintf ( sys_par.sys_par_nam,"conradi" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	conradi = Sys_par.lesesys_par(0);
	if ( conradi || sys_par.sys_par_wrt[0] == '\0')
	{
		conradi = 0 ;
	}
	else
	{
		conradi = 0;
		conradi = atoi ( sys_par.sys_par_wrt ) ;
	}


// 171114 - Schalter f�r alle extra-Boesinger-Abl�ufe .....

//  Leitweg und mgr-nr werden zuerst in lsk.kopf_txt reingeballert, danach folgt eventuell der "echte" Kopftext

	sprintf ( sys_par.sys_par_nam,"lboes_par" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	spboes_par = Sys_par.lesesys_par(0);
	if ( spboes_par || sys_par.sys_par_wrt[0] == '\0')
	{
		spboes_par = 0 ;
	}
	else
	{
		spboes_par = 0;
		spboes_par = atoi ( sys_par.sys_par_wrt ) ;
	}

	sprintf ( sys_par.sys_par_nam,"metro1_par" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	spmetro1_par = Sys_par.lesesys_par(0);
	if ( spmetro1_par || sys_par.sys_par_wrt[0] == '\0')
	{
		spmetro1_par = 0 ;
	}
	else
	{
		spmetro1_par = 0;
		spmetro1_par = atoi ( sys_par.sys_par_wrt ) ;
	}

// 250808 
	sprintf ( sys_par.sys_par_nam,"redecker_par" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	redeckerpar = Sys_par.lesesys_par(0);
	if ( redeckerpar || sys_par.sys_par_wrt[0] == '\0')
	{
		redeckerpar = 0 ;
	}
	else
	{
		redeckerpar = 0;
		redeckerpar = atoi ( sys_par.sys_par_wrt ) ;
	}

// 230108 
	sprintf ( sys_par.sys_par_nam,"lackmann" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	lackmann_aktiv = Sys_par.lesesys_par(0);
	if ( lackmann_aktiv || sys_par.sys_par_wrt[0] == '\0')
	{
		lackmann_aktiv = 0 ;
	}
	else
	{
		lackmann_aktiv = 0;
		lackmann_aktiv = atoi ( sys_par.sys_par_wrt ) ;
	}

// 041206
	sprintf ( sys_par.sys_par_nam,"party_mdn" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	party_mdn_aktiv = Sys_par.lesesys_par(0);
	if ( party_mdn_aktiv || sys_par.sys_par_wrt[0] == '\0')
	{
		party_mdn_aktiv = 0 ;
	}
	else
	{
		party_mdn_aktiv = 0;
		party_mdn_aktiv = atoi ( sys_par.sys_par_wrt ) ;
	}


	if ( zeittest ) schreibezeit("Vor temptabstruct \n" ) ;

	Temptab.structprep(myform_nr) ;

	if ( zeittest ) schreibezeit("Nach temptabstruct \n" ) ;

// 160305
	sprintf ( sys_par.sys_par_nam,"kun_leer" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	dkun_leer = Sys_par.lesesys_par(0);
	if ( dkun_leer || sys_par.sys_par_wrt[0] == '\0')
	{
		dkun_leer = 0 ;
	}
	else
	{
		dkun_leer = 0;
		dkun_leer = atoi ( sys_par.sys_par_wrt ) ;
	}


	lade_leerg( 0.0,0,0.0,"",0.0)	;	// Initialisierung  230609 : parametersatz erweitert

	sprintf ( sys_par.sys_par_nam,"a_zustxt_wa" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	da_zustxt_wa = Sys_par.lesesys_par(0);
	if ( da_zustxt_wa || sys_par.sys_par_wrt[0] == '\0')
	{
		da_zustxt_wa = 0 ;
	}
	else
	{
		da_zustxt_wa = 0;
		da_zustxt_wa = atoi ( sys_par.sys_par_wrt ) ;
	}

	sprintf ( sys_par.sys_par_nam,"nachkpreis" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	dnachkpreis = Sys_par.lesesys_par(0);
	if ( dnachkpreis || sys_par.sys_par_wrt[0] == '\0')
	{
		dnachkpreis = 0 ;
	}
	else
	{
		dnachkpreis = 0;
		dnachkpreis = atoi ( sys_par.sys_par_wrt ) ;
	}
	if ( dnachkpreis < 2 || dnachkpreis > 4 ) dnachkpreis = 2 ;

// 110205
	// folgendes ist zu gewaehrleisten :
	// 1. wa_pos_txt == ON
	// 2. lsp.posi anwaehlen
	// 3. als Referenztabelle lsp_txt eintragen ( anstelle von lspt, falls wa_pos_txt == OFF) 

	sprintf ( sys_par.sys_par_nam,"wa_pos_txt" );
	sys_par.sys_par_wrt[0] = '\0' ;
	Sys_par.opensys_par();
	dwa_pos_txt = Sys_par.lesesys_par(0);
	if ( dwa_pos_txt || sys_par.sys_par_wrt[0] == '\0')
	{
		dwa_pos_txt = 0 ;
	}
	else
	{
		dwa_pos_txt = 0;
		dwa_pos_txt = atoi ( sys_par.sys_par_wrt ) ;
	}

	if ( zeittest ) schreibezeit("Vor V+F-Definition \n" ) ;

    Variablendefinition (hJob);
    Felderdefinition (hJob);

    // GR: Druck starten

    if ( DrMitMenue == 1 && (PMWahlModus == 0 && WahlModus == 0) )	// 071014 : wahlmodus dazu 

    {
/* ->
		if (LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...") < 0)
< --- */
		int retc1 = LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...");
		if ( retc1 < 0 )
		{
			sprintf ( dmess, " drllls-Exception : %d", retc1 );
			MessageBox("Error While Printing", dmess , MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			Temptab.drfree();
			dbClass.closedbase ("bws");
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	// GrJ 040204

			return;
		}
	}
	else	// Druck pur immer auf den Drucker
	{
		int retc2 ;
		if ( alternat_ausgabe )	// 300306 
		{

			if ( zeittest ) schreibezeit("Vor setzealternat \n" ) ;

			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);

			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.Path",alternat_dir);
			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT,alternat_type,"Export.File",alternat_file);
 			retc2 = LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Quiet", "1");


			if ( ! strcmp ( alternat_type , "PDF") )
			{
				                                                                                             // min defa max
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}

			if ( zeittest ) schreibezeit("Vor Printstart \n" ) ;
			retc2 = LlPrintStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							(int) NULL );

			if ( zeittest ) schreibezeit("Nach Printstart  \n" ) ;
		
		}
		else
		{

			if ( (! WahlModus) && ( !PMWahlModus) )	// 071014 : WahlModus auswerten ...
			{
				if ( zeittest ) schreibezeit("Vor Printstart \n" ) ;
				retc2 = LlPrintStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_NORMAL,
							(int) NULL /* dummy */);
				if ( zeittest ) schreibezeit("Nach Printstart \n" ) ;
			}
			else	// 071014
					retc2 = 0 ;
		}
/* --->
		if (LlPrintStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_NORMAL,
							(int) NULL ) < 0)
< ---- */
		if ( retc2 < 0 )
		{
			sprintf ( dmess, " drllls-Exception : %d", retc2 );
	
			MessageBox("Error While Printing", dmess, MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			Temptab.drfree() ;
			dbClass.closedbase ("bws");
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);	// GrJ 040204
			return;
		}
   } 
	
// 031109 A
	if ( danzahl < 1 ) danzahl = 1 ;	// Notbremse
	if ( (!alternat_ausgabe && danzahlerlaubt && ! DrMitMenue) || ( danzahl > 1 ) )
	{
		int i = LlPrintSetOption(hJob, LL_PRNOPT_COPIES, danzahl);
		i = LlPrintGetOption(hJob, LL_PRNOPT_COPIES_SUPPORTED);
	}

//	if ( alternat_ausgabe || ! danzahlerlaubt ) 031109 : eigentlich immer erlaubt ( z.B. im Dialog )
	if ( alternat_ausgabe )
	{
		LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
	}

// 031109 E

	// GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
// 031109 	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);

	LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);


	if ( WahlModus )	// 171204
	{

		sprintf ( szFilename, "%s\\format\\ll\\53100.lst",getenv ("BWS"));
		char szFilenam1[299] ;
		sprintf ( szFilenam1, "%s\\format\\ll\\53101.lst",getenv ("BWS"));
		int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
// 090206 
		sprintf ( szFilename, "%s\\format\\ll\\53100.lsp",getenv ("BWS"));
		sprintf ( szFilenam1, "%s\\format\\ll\\53101.lsp",getenv ("BWS"));
		i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben
		if ( WahlModus > 1 )
		{
			SetWindowText ( "Druckerwahl Lieferscheine - 2.Exemplar") ;

			sprintf ( szFilename, "%s\\format\\ll\\531002.lst",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\531012.lst",getenv ("BWS"));
			int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
			sprintf ( szFilename, "%s\\format\\ll\\531002.lsp",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\531012.lsp",getenv ("BWS"));
		    i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben
		}
		if ( WahlModus > 2 )
		{
		SetWindowText ( "Druckerwahl Lieferscheine - 3.Exemplar") ;
		sprintf ( szFilename, "%s\\format\\ll\\531003.lst",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\531013.lst",getenv ("BWS"));
			int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
			sprintf ( szFilename, "%s\\format\\ll\\531003.lsp",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\531013.lsp",getenv ("BWS"));
		    i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben
		}
		if ( WahlModus > 3 )
		{
			SetWindowText ( "Druckerwahl Lieferscheine - 4.Exemplar") ;
			sprintf ( szFilename, "%s\\format\\ll\\531004.lst",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\531014.lst",getenv ("BWS"));
			int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
			sprintf ( szFilename, "%s\\format\\ll\\531004.lsp",getenv ("BWS"));
			sprintf ( szFilenam1, "%s\\format\\ll\\531014.lsp",getenv ("BWS"));
		    i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben
		}
		LlPrintEnd(hJob,0);
		LlJobClose(hJob);
		dbClass.closedbase ("bws");
		if (Hauptmenue == 0)  
		{
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		}
		if ( i < 0 ) return;

// 090206  i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben

		return ;
	}
	if ( PMWahlModus )	// 041206 es darf normalerweise nur entweder oder geben
	{

		sprintf ( szFilename, "%s\\format\\ll\\531000%01.0d.lst",getenv ("BWS"),party_mdn_aktiv);
		int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
		if ( PMWahlModus > 1 )
		{
			SetWindowText ( "Druckerwahl Party-Lieferscheine - 2.Exemplar") ;

			sprintf ( szFilename, "%s\\format\\ll\\531002%01.0d.lst",getenv ("BWS"),party_mdn_aktiv);
			int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
		}
		if ( PMWahlModus > 2 )
		{
		SetWindowText ( "Druckerwahl Party-Lieferscheine - 3.Exemplar") ;
		sprintf ( szFilename, "%s\\format\\ll\\531003%01.0d.lst",getenv ("BWS"),party_mdn_aktiv);
		int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
		}
		if ( PMWahlModus > 3 )
		{
			SetWindowText ( "Druckerwahl Party-Lieferscheine - 4.Exemplar") ;
			sprintf ( szFilename, "%s\\format\\ll\\531004%01.0d.lst",getenv ("BWS"),party_mdn_aktiv);
			int i = LlPrinterSetup(hJob, hWnd, LL_PROJECT_LIST, szFilename) ;
		}
		LlPrintEnd(hJob,0);
		LlJobClose(hJob);
		dbClass.closedbase ("bws");
		if (Hauptmenue == 0)  
		{
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		}
		if ( i < 0 ) return;

// 090206  i = CopyFile ( szFilename, szFilenam1 ,0) ;	// Bitte mit ueberschreiben

		return ;
	}

	if ( DrMitMenue == 1)
    {
	
		if (LlPrintOptionsDialog(hJob, hWnd, "Drucker-Einrichtung") < 0)
		{
			LlPrintEnd(hJob,0);
			LlJobClose(hJob);
			Temptab.drfree() ;
		    dbClass.closedbase ("bws");
			if (Hauptmenue == 0)
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
	        return;
		}
		//GR: Druckziel abfragen
		LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		sMedia.ReleaseBuffer();
    }
	else
	{

		if ( alternat_ausgabe )
		{
			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_EXPORT, alternat_type);
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);

			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Path", alternat_dir);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.File", alternat_file);
			LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Export.Quiet", "1");
//			LlPrintSetOptionString(hJob, LL_PRNOPTSTR_PRINTDST_FILENAME, alternat_file);

			if ( ! strcmp ( alternat_type , "PDF") )
			{
				                                                                                             // min defa max
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.BitsPerPixel", "8");	// 1    8    24		Farbtiefe - kein einfluss
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "Picture.JPEGQuality", "30");	// 0    100  100	JPEG-Qualit�t - Raster wird gr�ber
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.MaxOutlineDepth", "0");
				LlXSetParameter(hJob, LL_LLX_EXTENSIONTYPE_EXPORT, alternat_type, "PDF.AddOutline", "0");
			}


		}
		else
		{
			LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
		}
		sMedia.ReleaseBuffer();

	}


    int  nRecCount = 1, nErrorValue = 0, nPrintFehler = 0, nLastPage, nRecno;
	char szPrinter[80], szPort[20];

	if ( zeittest ) schreibezeit("vor GetPrinterinfo \n" ) ;
    LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));
	if ( zeittest ) schreibezeit("nach GetPrinterinfo \n" ) ;


	nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);

	nRecno = 1;

    // GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.

//  spaeter evtl .aml : zu aufwendig 	nRecCount = GetRecCount () ;

//	sprintf (dmess, "nRecCount = %hd",nRecCount);  //050803
//	MessageBox(dmess, "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);

	if ( zeittest ) schreibezeit("vor erster Fetch \n" ) ;

	int abbruch = Temptab.opentemptab();

	if ( zeittest ) schreibezeit("nach erster Fetch \n" ) ;

	if ( abbruch == -3 ) 
    {
//		sprintf (dmess, "User-Abbruch ");
//		MessageBox (dmess, "", MB_ICONERROR);
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
			Temptab.drfree () ;
			dbClass.closedbase ("bws");
			if (Hauptmenue == 0)  //#LuD
			{
				PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			}
    		return;

	}

	int di = Temptab.lesetemptab (0);   //       1.DATENSATZ lesen f�r Variablen�bergabe
											// nErrorValue als Paramter ist hier nicht gut
	if (di != 0 || verboteneFelder ) 
    {
		if ( verboteneFelder )	// 160113
			sprintf (dmess, "%s",cverboteneFelder);
		else	// kann nur noch di als Ursache sein ........
			sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				Temptab.drfree () ;
				dbClass.closedbase ("bws");
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
	if ( ! Temptab.kun_leer_kz()) dkun_leer = 0  ;	// Ausgabe des LG als total normale Artikel
		// Filial-LS retourniert hier immer "1"


//	while ( (( nRecno < nRecCount) || ( nRecno == 1 && nRecCount == 1))
	while ( ( !di )
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA))
   	{
	    VariablenUebergabe ( hJob, szTemp2, nRecno );	// Variablen vor derListe .....
		if ( verboteneFelder )	// 160113
			MessageBox (cverboteneFelder, "drllls", MB_ICONERROR);
		nErrorValue = LlPrint(hJob);

		while ((!di) && (nErrorValue == 0) 
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage))

		{

			if ( party_mdn_aktiv == Temptab.get_lsk_mdn())	// 041206 : alles ein bischen anders
			{
				if ( Temptab.a_bas_a_typ()  == 12  )
				{
					lade_leerg( Temptab.a_bas_a() ,(int) Temptab.lsp_lief_me(),
				            Temptab.lsp_ls_vk_euro(), Temptab.a_bas_a_bz1() , Temptab.a_bas_a_gew());	// 230609

				FelderUebergabe ( hJob, szTemp2, nRecno );	// 20.12.09 : trotzdem z.B. texte laden

				}
			}
			else
			{
				if ( Temptab.a_bas_a_typ()  == 11 && dkun_leer )
				{
					lade_leerg( Temptab.a_bas_a() ,(int) Temptab.lsp_lief_me(),
				            Temptab.lsp_ls_vk_euro(), Temptab.a_bas_a_bz1(),Temptab.a_bas_a_gew());	// 230609

					FelderUebergabe ( hJob, szTemp2, nRecno );	// 20.12.09 : trotzdem z.B. texte laden

					di = Temptab.lesetemptab (0) ;	//       DATENSATZ lesen
					if ( di )
					{
						di = di ;	// Testbreakpoint
						// 210506 
						VariablenUebergabe ( hJob, szTemp2, nRecno );
						if ( verboteneFelder )	// 160113
							MessageBox (cverboteneFelder, "drllls", MB_ICONERROR);

						SchreibeLeergut (hJob,szTemp2) ;	

					}

					continue ;
				}
			}

		    FelderUebergabe ( hJob, szTemp2, nRecno );
    		// GR: Drucken der aktuellen Tabellenzeile
    		nErrorValue = LlPrintFields(hJob);
			nPrintFehler = nErrorValue;

			if (nErrorValue == LL_WRN_REPEAT_DATA)
			{

			    VariablenUebergabe ( hJob, szTemp2, nRecno );
				nErrorValue = LlPrint(hJob);
//			    FelderUebergabe ( hJob, szTemp2, nRecno );
//	    		nErrorValue = LlPrintFields(hJob);
			}


			if (nErrorValue == 0)	// US: everything fine, record could have been printed...
	    	{
				// US: ...but probably the user defined a filter condition!
				//     So before updating time dependent variables we have to check if the record really has been printed:
				if (LlPrintDidMatchFilter(hJob))
				{
					// GR: Aktualisieren der zeitabhaengigen Variable 'FixedVariable2'
					// US: update the time dependent variable 'FixedVariable2'
					//     coming after printig the list(s)
		    		sprintf(szTemp2, "FixedVariable2, record %d", nRecno);
		    		LlDefineVariable(hJob, "FixedVariable2", szTemp2);
		    	}

//	    		if (nPrintFehler == 0) nRecno++;	// GR: gehe zum naechsten Datensatz
			}

			di = Temptab.lesetemptab (nPrintFehler) ;	//       DATENSATZ lesen
			if ( di )
			{
				di = di ;	// Testbreakpoint
				// 210506 
				VariablenUebergabe ( hJob, szTemp2, nRecno );
				SchreibeLeergut (hJob,szTemp2) ;	

			}


		}
  	}

	// US: all records have been printed, now flush the table
	//     If footer doesn't fit to this page try again for the next page:

	VariablenUebergabe ( hJob, szTemp2, nRecno );

	SchreibeLeergut (hJob,szTemp2) ;	// 160305

	nErrorValue = LlPrintFieldsEnd(hJob);

	//	210506 if (nErrorValue == LL_WRN_REPEAT_DATA)

	while (nErrorValue == LL_WRN_REPEAT_DATA)
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	    VariablenUebergabe ( hJob, szTemp2, nRecno );
	    FelderUebergabe ( hJob, szTemp2, nRecno );
		SchreibeLeergut (hJob,szTemp2) ;	// 160305

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);

	}

	if (nErrorValue == LL_WRN_REPEAT_DATA)	// US: footer still doesn't fit!
		MessageBox("Error because table is too small for footer!", "List & Label App", MB_OK|MB_ICONEXCLAMATION);

	//GR: Druck beenden
	LlPrintEnd(hJob,0);

	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) loeschen
        LlPreviewDisplay(hJob, szFilename, penvchar, hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, penvchar);
    }
	// GR: Beenden des List & Label Jobs

	LlJobClose(hJob);

	Temptab.drfree();
	dbClass.closedbase ("bws");

	if ( zeittest ) schreibezeit("alles vorbei \n" ) ;

	if (Hauptmenue == 0)  
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}
}

//=============================================================================
LRESULT CMainFrame::OnLulMessage(WPARAM wParam, LPARAM lParam)
//=============================================================================
{
	// GR: Dies ist die List & Label Callback Funktion.
	//     Saemtliche Rueckmeldungen bzw. Events werden dieser Funktion
	//     gemeldet.
	// US: This is the List & Label Callback function.
	//     Is is called for requests an notifications.


	PSCLLCALLBACK	pscCallback = (PSCLLCALLBACK) lParam;
	LRESULT			lRes = TRUE;
	CString			sVariableDescr;

	static CHAR szHelpText[256];

	ASSERT(pscCallback->_nSize == sizeof(scLlCallback));	// GR: Die groesse der Callback Struktur muss stimmen!
															// US: sizes of callback structs must match!

	switch(wParam)
	{
		case LL_CMND_VARHELPTEXT:	// GR: Es wird ein Beschreibungstext f�r eine Variable erfragt.
									// US: Helptext needed for selected variable within designer

				// GR: ( pscCallback->_lParam enth�lt einen LPCSTR des Beschreibungstextes )
				// US: ( pscCallback->_lParam contains a LPCSTR to the name of the selected variable )

				sVariableDescr = (LPCSTR)pscCallback->_lParam;

				if (!sVariableDescr.IsEmpty())
					sprintf(szHelpText,
							"This is the sample field / variable '%s'.",
							(LPCSTR)sVariableDescr);
				else
					strcpy(szHelpText, "No variable or field selected.");

				pscCallback->_lReply = (LPARAM)szHelpText;
				break;

		default:
				pscCallback->_lReply = lRes = FALSE; // GR: Die Nachricht wurde nicht bearbeitet.
													 // US: indicate that message hasn't been processed
	}

	return(lRes);
}




static char DefWert [256] ;

char *get_default (char *env)
{         
        int anz;
        char buffer [512];
        FILE *fp;
        sprintf (buffer, "%s\\bws_defa",getenv ("BWSETC"));	// 090109 : ab heute aus bws_defa, vorher nur dummy-Ablauf
		fp = fopen (buffer, "r");
        if (fp == (FILE *)NULL) return NULL;

// redundant        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmpi (wort[1], env, (int)strlen (env),
									(int)strlen(clipped(wort[1]))) == 0)
    
					 {
                                 strcpy (DefWert, clipped (wort [2]));
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}
// 031109 
char *get_cfg (char *env)
{         
        int anz;
        char buffer [512];
        FILE *fp;
        sprintf (buffer, "%s\\drllls.cfg",getenv ("BWSETC"));
		fp = fopen (buffer, "r");
        if (fp == (FILE *)NULL) return NULL;

        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = splitdoll (buffer);
                     if (anz < 2) continue;
                     if (strupcmpi (wort[1], env, (int)strlen (env),
							(int)strlen(clipped(wort[1]))) == 0)
                     {
                                 strcpy (DefWert, clipped (wort [2]));
                                 fclose (fp);
                                 return (char *) DefWert;
                     }
         }
         fclose (fp);
         return NULL;
}

void CMainFrame::GetCfgValues (void)
{
//       char cfg_v [1024];
//       if (ProgCfg == NULL) return;

	char hilfstr[256] ;
	char *p ;

	cfgOK = TRUE;

	Hauptmenue =0 ;
	Listenauswahl = 0 ;
	DrMitMenue = 1 ;

//	130110 ;
	if ( ! zeittest )
	{
		p =  get_cfg("zeittest");
		if ( p != NULL )
		{
			sprintf ( hilfstr ,"%s", p );
			zeittest = atoi ( hilfstr ) ;
		} ;
		if ( zeittest ) schreibezeit("Start via drllls.cfg \n" ) ;
	}



	// 090109
	NutzerIgnorieren = 0 ;
	p =  get_default("NutzerIgnorieren");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		NutzerIgnorieren = atoi ( hilfstr ) ;
	} ;

// 031109

	danzahlerlaubt = 0 ;
	p =  get_cfg("anzahlerlaubt");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		danzahlerlaubt = atoi ( hilfstr ) ;
	} ;


	dzerllfd = 0 ;
	// 0 == zerldaten.lfd_i als Schluessel
	// 1 == zerldaten.lfd als Schluessel
	p =  get_cfg("zerlegelfd");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		dzerllfd = atoi ( hilfstr ) ;
	} ;



	return ;
/* ---> der rest ist evtl. fuer spaeter
	Hauptmenue = 0 ;
	p =  get_default("Hauptmenue");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		Hauptmenue = atoi ( hilfstr ) ;
	} ;
	

	Listenauswahl = 0 ;
	p =  get_default("Listenauswahl");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		Listenauswahl = atoi ( hilfstr ) ;
	} ;

	DrMitMenue = 0 ;
	p =  get_default("DrMitMenue");
	if ( p != NULL )
	{
	    sprintf ( hilfstr ,"%s", p );
		DrMitMenue = atoi ( hilfstr ) ;
	} ;

	if ( buffer !=  (char *) 0) 
			free ( buffer );
< ---- */

/* --->
		Listenauswahl =0 ;
		if (ProgCfg->GetCfgValue ("Listenauswahl", cfg_v) == TRUE)
       {
           Listenauswahl = atoi (cfg_v);
	   }
< -*/
/* -->
		Hauptmenue = 0;
		if (ProgCfg->GetCfgValue ("Hauptmenue", cfg_v) == TRUE)
       {
           Hauptmenue = atoi (cfg_v);
	   }

	   exit(0) ;
< ---- */

/* --->
		DrMitMenue = 1 ;
       if (ProgCfg->GetCfgValue ("DrMitMenue", cfg_v) == TRUE)
       {
           DrMitMenue = atoi (cfg_v);
	   }
< ---- */

}


    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt
	/************
	LlDefineFieldExt(hJob, "Barcode_EAN13", "44|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P2", "44|44444|44444|44", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P5", "44|44444|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN128", "EAN128ean128", LL_BARCODE_EAN128, NULL);
	LlDefineFieldExt(hJob, "Barcode_CODE128", "Code 128", LL_BARCODE_CODE128, NULL);
	LlDefineFieldExt(hJob, "Barcode_Codabar", "A123456A", LL_BARCODE_CODABAR, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCA", "44|44444", LL_BARCODE_EAN8, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCE", "4|44444|44444", LL_BARCODE_UPCA, NULL);
	LlDefineFieldExt(hJob, "Barcode_3OF9", "*TEST*", LL_BARCODE_3OF9, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IND", "44444", LL_BARCODE_25INDUSTRIAL, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IL", "444444", LL_BARCODE_25INTERLEAVED, NULL);
	LlDefineFieldExt(hJob, "Barcode_25MAT", "44444", LL_BARCODE_25MATRIX, NULL);
	LlDefineFieldExt(hJob, "Barcode_25DL", "44444", LL_BARCODE_25DATALOGIC, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET5", "44444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET10", "44444-4444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET12", "44444-444444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_FIM", "A", LL_BARCODE_FIM, NULL);
	**********/


void fitapldefines (HJOB hJob)
{

/* -->
	sprintf (form_feld.form_nr, "test1");

	dbClass.sqlopen (form_feld_curs);

	while (dbClass.sqlfetch (form_feld_curs) == 0)
	{
		ll_typ = holetyp(form_feld.tab_nam , form_feld.feld_nam );

		form_feld.tab_nam[0] = toupper(form_feld.tab_nam[0]) ;
		form_feld.feld_nam[0] = toupper(form_feld.feld_nam[0]) ;
		CString s = form_feld.tab_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.tab_nam, s ) ;
		s = form_feld.feld_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.feld_nam, s ) ;
		sprintf( tabdotfeld , "%s.%s", form_feld.tab_nam, form_feld.feld_nam);
		sprintf ( defabeleg, "%s", form_feld.feld_nam);
		if (ll_typ == LL_NUMERIC )
		{
			sprintf ( defabeleg, "12345678");
		};
	
			LlDefineFieldExt(hJob, tabdotfeld, defabeleg,ll_typ,NULL);

	}
*********/
}


