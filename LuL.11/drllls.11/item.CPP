#include "stdafx.h"
#include "DbClass.h"
#include "item.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif


struct ITEM item,  item_null;
struct CRONMHD cronmhd, cronmhd_null ;
struct ZERLDATEN zerldaten, zerldaten_null ;	// 091209

extern DB_CLASS dbClass;
extern int dzerllfd ;

int ITEM_CLASS::leseitem (	)
{

      int di = dbClass.sqlfetch (readcursor);

	  return di;
}


int ITEM_CLASS::openitem (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}



void ITEM_CLASS::prepare (void)
{
	dbClass.sqlin  ((char *) item.name, SQLCHAR, 19);

	dbClass.sqlout ((char *) item.bezkurz, SQLCHAR, 11);
    dbClass.sqlout ((char *) item.bezlang, SQLCHAR, 19);

 	test_upd_cursor = 1;

	readcursor =
		dbClass.sqlcursor ("select bezkurz, bezlang from item where name = ? " ) ;
	
  }

int ZERLDATEN_CLASS::lesezerldaten ( void )	// 091209
{

	if ( readcursor < 0 ) prepare ();
	int di = dbClass.sqlopen (readcursor);
	if ( ! di )
		di = dbClass.sqlfetch (readcursor);
	  return di;
}

void ZERLDATEN_CLASS::prepare (void)
{

	dbClass.sqlin ((long *)  &zerldaten.lfd_i, SQLLONG, 0);
    dbClass.sqlin ((double *) &zerldaten.a, SQLDOUBLE, 0);
// keine Mandanten-spezifik, da lfd bereits passt ..........

	dbClass.sqlout ((short *) &zerldaten.mdn, SQLSHORT, 0);
    dbClass.sqlout ((short *) &zerldaten.fil, SQLSHORT, 0);
	dbClass.sqlout ((long *)  &zerldaten.partie, SQLLONG, 0);
	dbClass.sqlout ((char *)   zerldaten.lief, SQLCHAR, 17);
	dbClass.sqlout ((char *)   zerldaten.lief_rech_nr, SQLCHAR, 17 );
	dbClass.sqlout ((char *)   zerldaten.ident_extern, SQLCHAR, 21 );
	dbClass.sqlout ((char *)   zerldaten.ident_intern, SQLCHAR, 21 );
	dbClass.sqlout ((double *)&zerldaten.a,            SQLDOUBLE, 0 );
	dbClass.sqlout ((double *)&zerldaten.a_gt, SQLDOUBLE, 0 );
	dbClass.sqlout ((char *)   zerldaten.kategorie, SQLCHAR, 5 );
	dbClass.sqlout ((short *) &zerldaten.schnitt, SQLSHORT, 0  );
	dbClass.sqlout ((short *) &zerldaten.gebland, SQLSHORT, 0  );
	dbClass.sqlout ((short *) &zerldaten.mastland, SQLSHORT, 0 );
	dbClass.sqlout ((short *) &zerldaten.schlaland, SQLSHORT, 0 );
	dbClass.sqlout ((short *) &zerldaten.zerland, SQLSHORT, 0 );
	dbClass.sqlout ((char *)   zerldaten.esnum, SQLCHAR,  11 );
	dbClass.sqlout ((char *)   zerldaten.eznum1, SQLCHAR, 11 );
	dbClass.sqlout ((char *)   zerldaten.eznum2, SQLCHAR, 11 );
	dbClass.sqlout ((char *)   zerldaten.eznum3, SQLCHAR, 11 );
//	dbClass.sqlout ((TIMESTAMP_STRUCT *) zerldaten.    dat date,
	dbClass.sqlout ((short *) &zerldaten.anz_gedruckt, SQLSHORT, 0 ) ;
	dbClass.sqlout ((short *) &zerldaten.anz_entnommen, SQLSHORT, 0 );
//    lfd serial not null ,
	dbClass.sqlout ((long *)  &zerldaten.lfd , SQLLONG, 0 );
	dbClass.sqlout ((long *)  &zerldaten.lfd_i, SQLLONG, 0 );
	dbClass.sqlout ((short *) &zerldaten.zerlegt, SQLSHORT, 0 );
	dbClass.sqlout ((short *) &zerldaten.aktiv, SQLSHORT, 0 );


	if ( dzerllfd == 1 )
	{

		readcursor =
		dbClass.sqlcursor ("select "
		" mdn ,fil ,partie ,lief ,lief_rech_nr "
		" ,ident_extern ,ident_intern , a ,a_gt ,kategorie "
		" ,schnitt ,gebland ,mastland , schlaland , zerland "
		" ,esnum ,eznum1 ,eznum2 ,eznum3 ,anz_gedruckt "
		" ,anz_entnommen ,lfd , lfd_i ,zerlegt , aktiv "
	
		" from zerldaten where lfd = ? and a = ? " ) ;
	}
	else
	{	// so war es bisher ..... 080113
		readcursor =
		dbClass.sqlcursor ("select "
		" mdn ,fil ,partie ,lief ,lief_rech_nr "
		" ,ident_extern ,ident_intern , a ,a_gt ,kategorie "
		" ,schnitt ,gebland ,mastland , schlaland , zerland "
		" ,esnum ,eznum1 ,eznum2 ,eznum3 ,anz_gedruckt "
		" ,anz_entnommen ,lfd , lfd_i ,zerlegt , aktiv "
	
		" from zerldaten where lfd_i = ? and a = ? " ) ;
	}
}

int CRONMHD_CLASS::lesemhd (	void)
{

	if ( readcursor < 0 ) prepare ();
	int di = dbClass.sqlopen (readcursor);
	if ( ! di )
		di = dbClass.sqlfetch (readcursor);
	  return di;
}


void CRONMHD_CLASS::prepare (void)
{

	dbClass.sqlin  ((long  *) &cronmhd.ls  , SQLLONG, 0  );
	dbClass.sqlin  ((long  *) &cronmhd.posi, SQLLONG, 0 );
	dbClass.sqlin  ((short *) &cronmhd.mdn , SQLSHORT, 0  );

	dbClass.sqlout ((char *) cronmhd.dmhd01, SQLCHAR, 12);
    dbClass.sqlout ((double *) &cronmhd.mmhd01, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) cronmhd.dmhd02, SQLCHAR, 12);
    dbClass.sqlout ((double *) &cronmhd.mmhd02, SQLDOUBLE, 0);
 
	dbClass.sqlout ((char *) cronmhd.dmhd03, SQLCHAR, 12);
    dbClass.sqlout ((double *) &cronmhd.mmhd03, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) cronmhd.dmhd04, SQLCHAR, 12);
    dbClass.sqlout ((double *) &cronmhd.mmhd04, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) cronmhd.dmhd05, SQLCHAR, 12);
    dbClass.sqlout ((double *) &cronmhd.mmhd05, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) cronmhd.dmhd06, SQLCHAR, 12);
    dbClass.sqlout ((double *) &cronmhd.mmhd06, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) cronmhd.dmhd07, SQLCHAR, 12);
    dbClass.sqlout ((double *) &cronmhd.mmhd07, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) cronmhd.dmhd08, SQLCHAR, 12);
    dbClass.sqlout ((double *) &cronmhd.mmhd08, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) cronmhd.dmhd09, SQLCHAR, 12);
    dbClass.sqlout ((double *) &cronmhd.mmhd09, SQLDOUBLE, 0);

	dbClass.sqlout ((char *) cronmhd.dmhd10, SQLCHAR, 12);
    dbClass.sqlout ((double *) &cronmhd.mmhd10, SQLDOUBLE, 0);

	readcursor =
		dbClass.sqlcursor ("select "
		"  dmhd01 , mmhd01 "
		" ,dmhd02 , mmhd02 "
		" ,dmhd03 , mmhd03 "
		" ,dmhd04 , mmhd04 "
		" ,dmhd05 , mmhd05 "
		" ,dmhd06 , mmhd06 "
		" ,dmhd07 , mmhd07 "
		" ,dmhd08 , mmhd08 "
		" ,dmhd09 , mmhd09 "
		" ,dmhd10 , mmhd10 "
		" from cronmhd where ls = ? and posi = ? and mdn = ? " ) ;
  }
