#include "stdafx.h"
#include "DbClass.h"
#include "txt.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif


struct ATEXTE atexte,  atexte_null;	// 101014
struct AUFPT_VERKFRAGE aufpt_verkfrage,  aufpt_verkfrage_null;	// 161014
struct PTXT ptxt,  ptxt_null;
struct NTXT ntxt,  ntxt_null;	// 110205
extern DB_CLASS dbClass;

// int itxt_nr ;
// static int anzzfelder ;

/**
--------------------------------------------------------------------------------
-
-       Procedure       :       clipped
-
-       In              :       string
-
-       Out             :
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Blanks am Ende eines Strings entfernen.
-
-    ich baue mal so um, das nur noch  rechts geclipped wird
--------------------------------------------------------------------------------
**/
#ifdef OLD_DCL
char *clipped (string)
char *string;
#else
char *clipped (char *string)
#endif
{
 char *clstring;
 short i,len;

 len = (short) strlen (string);

 if (len == 0) return (string);
 len --;
 clstring = string;
 // 201010 damit werden auch Leerstrings korrekt gekappt 
 for (i = len; i >= 0; i --)
 {
  if ((unsigned char) clstring[i] > 0x20)
  {
   break;
  }
 }
 clstring [i + 1] = 0;

 clstring = string;
 /* -----> links-clippen lasse ich mal weg 
 len = (short) strlen (clstring);

 for (i = 0; i < len; i ++, clstring +=1)
 {
  if ((unsigned char) *clstring > (unsigned char) 0X20)
  {
   break;
  }
 }
 < ----- */
 return (clstring);
}

// 161014

int AUFPT_VERKFRAGE_CLASS::leseaufpt_verkfrage (short emdn, long els, double ea )
{
	if ( readcursor < 0 )
	{	aufpt_verkfrage.mdn = emdn - 1 ;	// Start erzwingen
		// Speicher bereitstellen und urladen 

		ctxt_ls1	= (char*) malloc ( sizeof ( atexte.disp_txt));
		ctxt_rech1	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_ls2	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_rech2	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_ls3	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_rech3	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_ls4	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_rech4	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_ls5	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_rech5	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_ls6	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_rech6	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_ls7	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_rech7	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_ls8	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_rech8	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_ls9	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_rech9	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_ls10	= (char*) malloc ( sizeof( atexte.disp_txt));
		ctxt_rech10	= (char*) malloc ( sizeof( atexte.disp_txt));

		prepare();
	}
	if ( aufpt_verkfrage.mdn == emdn && aufpt_verkfrage.ls == els && aufpt_verkfrage.a == ea )
		return 0 ;	// es passt bereits alles ...
	memcpy ( &aufpt_verkfrage, &aufpt_verkfrage_null, sizeof ( struct AUFPT_VERKFRAGE ));
	aufpt_verkfrage.a = ea ;
	aufpt_verkfrage.mdn = emdn ;
	aufpt_verkfrage.ls = els ;

		sprintf( ctxt_ls1,"");
		sprintf( ctxt_rech1,"");
		sprintf( ctxt_ls2,"");
		sprintf( ctxt_rech2,"");
		sprintf( ctxt_ls3,"");
		sprintf( ctxt_rech3,"");
		sprintf( ctxt_ls4,"");
		sprintf( ctxt_rech4,"");
		sprintf( ctxt_ls5,"");
		sprintf( ctxt_rech5,"");
		sprintf( ctxt_ls6,"");
		sprintf( ctxt_rech6,"");
		sprintf( ctxt_ls7,"");
		sprintf( ctxt_rech7,"");
		sprintf( ctxt_ls8,"");
		sprintf( ctxt_rech8,"");
		sprintf( ctxt_ls9,"");
		sprintf( ctxt_rech9,"");
		sprintf( ctxt_ls10,"");
		sprintf( ctxt_rech10,"");

	int di = dbClass.sqlopen (readcursor);
	di = dbClass.sqlfetch (readcursor);

	return di;
}

// 101014
int ATEXTE_CLASS::leseatexte (long exttext)
{
	if ( readcursor < 0 ) prepare();

	memcpy ( &atexte, &atexte_null, sizeof ( struct ATEXTE ));
	atexte.txt_nr = exttext;
	int di = dbClass.sqlopen (readcursor);
	di = dbClass.sqlfetch (readcursor);
	  return di;
}

int PTXT_CLASS::leseptxt ()
{
	memcpy ( &ptxt, &ptxt_null, sizeof ( struct PTXT ));	// 290506 
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int NTXT_CLASS::lesentxt ()
{
	memcpy ( &ntxt, &ntxt_null, sizeof ( struct NTXT ));	// 290506 
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}



int PTXT_CLASS::openptxt (char *ctabnam)
{

//		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?
		prepare ( ctabnam );
        return dbClass.sqlopen (readcursor);
}

int NTXT_CLASS::openntxt (char *ctabnam)
{

//		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?
		prepare ( ctabnam );
        return dbClass.sqlopen (readcursor);
}


int PTXT_CLASS::suchereadcursor (char *ctabnam)
{
	for ( int i = 0 ; i < MYMAXRANGE ; i ++ )
	{
		if ( ctabimatr[i] < 0 )
		{
			// Eintragsende erreicht : readcursor zeigt auf naechsten freien Platz
			readcursor = i ;
			return -1 ;
		}
		if ( ! strncmp ( ctabnmatr[i],clipped (ctabnam) , strlen(clipped (ctabnam))))
		{
			// erfolgreich gefunden
			readcursor = ctabimatr[i] ;
			return 0 ;
		}
	}
	// Notbremse ?!
	readcursor = 0 ;
	return -1  ;
}
int NTXT_CLASS::suchereadcursor (char *ctabnam)
{
	for ( int i = 0 ; i < MYMAXRANGE ; i ++ )
	{
		if ( ctabimatr[i] < 0 )
		{
			// Eintragsende erreicht : readcursor zeigt auf naechsten freien Platz
			readcursor = i ;
			return -1 ;
		}
		if ( ! strncmp ( ctabnmatr[i],clipped (ctabnam) , strlen(clipped (ctabnam))))
		{
			// erfolgreich gefunden
			readcursor = ctabimatr[i] ;
			return 0 ;
		}
	}
	// Notbremse ?!
	readcursor = 0 ;
	return -1  ;
}


void PTXT_CLASS::prepare (char * ctabnam)
{

	// 111110
	int ihilfe = suchereadcursor( ctabnam ) ;
	if ( ! ihilfe )
	{
		// alles ist gut, dencursor gibbet schon ....
		// readcursor zeigt auf atuelle selektion
		return ;
	}
	ihilfe = readcursor ;

	test_upd_cursor = 1;
// ich gehe mal davon aus, das eine Zeile nicht groesser als 100 Zeichen ist ...

	dbClass.sqlin ((long *)   &ptxt.nr, SQLLONG, 0);

	dbClass.sqlout ((long *)  &ptxt.zei,SQLLONG, 0);
	dbClass.sqlout ((char *)  ptxt.txt,SQLCHAR, 101);

//		readcursor = dbClass.sqlcursor ("select "
//	" zei, txt "
//	" from " clipped (ctabnam) " where nr = ? order by zei" ) ;
	char buffer[256] ;
	sprintf ( buffer , "select zei,txt from %s where nr = ? order by zei ", 
		clipped (ctabnam) );
	readcursor = dbClass.sqlcursor (buffer);

	if ( readcursor > -1 )	// 111110 : neuer Eintrag
	{
		if ( ihilfe < MYMAXRANGE )
		{
			ctabimatr[ihilfe] = readcursor ;
			sprintf ( ctabnmatr[ihilfe] , "%s" , ctabnam ) ;
		}
	}

}

void NTXT_CLASS::prepare (char * ctabnam)
{

	// 111110
	int ihilfe = suchereadcursor( ctabnam ) ;
	if ( ! ihilfe )
	{
		// alles ist gut, dencursor gibbet schon ....
		// readcursor zeigt auf atuelle selektion
		return ;
	}
	ihilfe = readcursor ;

	test_upd_cursor = 1;
// ich gehe mal davon aus, das eine Zeile nicht groesser als 100 Zeichen ist ...

	dbClass.sqlin ((short *)   &ntxt.mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *)   &ntxt.fil, SQLSHORT, 0);
	dbClass.sqlin ((long *)   &ntxt.ls, SQLLONG, 0);
	dbClass.sqlin ((long *)   &ntxt.posi, SQLLONG, 0);

	dbClass.sqlout ((long *)  &ntxt.zei,SQLLONG, 0);
	dbClass.sqlout ((char *)  ntxt.txt,SQLCHAR, 101);


	char buffer[256] ;
	sprintf ( buffer , "select zei, txt from %s where mdn = ? and fil = ? and ls = ? and posi = ? order by zei ", 
		clipped (ctabnam) );
	readcursor = dbClass.sqlcursor (buffer);

	if ( readcursor > -1 )	// 111110 : neuer Eintrag
	{
		if ( ihilfe < MYMAXRANGE )
		{
			ctabimatr[ihilfe] = readcursor ;
			sprintf ( ctabnmatr[ihilfe] , "%s" , ctabnam ) ;
		}
	}
}

// 101014 A
void ATEXTE_CLASS::prepare (void)
{
	dbClass.sqlin ((long *) &atexte.txt_nr, SQLLONG, 0);

//	dbClass.sqlout ((long *) &atxte.sys, SQLLONG, 0);
//	dbClass.sqlout ((short *) &atxte.waa, SQLSHORT, 0);
// //	dbClass.sqlout ((long *) &atxte.txt_nr, SQLLONG, 0);
	dbClass.sqlout ((char *)  atexte.txt, SQLCHAR, 1025 );
	dbClass.sqlout ((char *)  atexte.disp_txt, SQLCHAR, 1025);
//	dbClass.sqlout ((short *) &atxte.alignment, SQLSHORT, 0);
//	dbClass.sqlout ((short *) &atxte.send_ctrl, SQLSHORT, 0);
//	dbClass.sqlout ((short *) &atxte.txt_typ, SQLSHORT, 0);
//	dbClass.sqlout ((short *) &atxte.txt_platz, SQLSHORT, 0);
//	dbClass.sqlout ((double *) &atxte.a, SQLDOUBLE, 0);


	readcursor = dbClass.sqlcursor (
		" select txt, disp_txt from atexte "
		" where txt_nr = ? "
	);

}


// 161014 A

AUFPT_VERKFRAGE_CLASS::~AUFPT_VERKFRAGE_CLASS()
{
	if (  ctxt_ls1 == NULL )
		return;
	free (ctxt_ls1);
	free (ctxt_rech1);
	free (ctxt_ls2);
	free (ctxt_rech2);
	free (ctxt_ls3);
	free (ctxt_rech3);
	free (ctxt_ls4);
	free (ctxt_rech4);
	free (ctxt_ls5);
	free (ctxt_rech5);
	free (ctxt_ls6);
	free (ctxt_rech6);
	free (ctxt_ls7);
	free (ctxt_rech7);
	free (ctxt_ls8);
	free (ctxt_rech8);
	free (ctxt_ls9);
	free (ctxt_rech9);
	free (ctxt_ls10);
	free (ctxt_rech10);

}
void AUFPT_VERKFRAGE_CLASS::prepare (void)
{
	dbClass.sqlin ((long *) &aufpt_verkfrage.ls, SQLLONG, 0);
	dbClass.sqlin ((short *) &aufpt_verkfrage.mdn, SQLSHORT, 0);
	dbClass.sqlin ((double *) &aufpt_verkfrage.a, SQLDOUBLE, 0);

// 031114 : def1 .. def10 dazu
	dbClass.sqlout ((short *) &aufpt_verkfrage.def1, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def2, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def3, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def4, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def5, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def6, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def7, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def8, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def9, SQLSHORT, 0);
	dbClass.sqlout ((short *) &aufpt_verkfrage.def10, SQLSHORT, 0);


	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls1, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls2, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls3, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls4, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls5, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls6, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls7, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls8, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls9, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_ls10, SQLLONG, 0);

	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech1, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech2, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech3, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech4, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech5, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech6, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech7, SQLLONG, 0);	
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech8, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech9, SQLLONG, 0);
	dbClass.sqlout ((long *) &aufpt_verkfrage.txt_rech10, SQLLONG, 0);

	dbClass.sqlout ((char *) ctxt_ls1, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls2, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls3, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls4, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls5, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls6, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls7, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls8, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls9, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_ls10, SQLCHAR, sizeof(atexte.disp_txt));

	dbClass.sqlout ((char *) ctxt_rech1, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech2, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech3, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech4, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech5, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech6, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech7, SQLCHAR, sizeof(atexte.disp_txt));	
	dbClass.sqlout ((char *) ctxt_rech8, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech9, SQLCHAR, sizeof(atexte.disp_txt));
	dbClass.sqlout ((char *) ctxt_rech10, SQLCHAR, sizeof(atexte.disp_txt));

	readcursor = dbClass.sqlcursor (
		" select "
		"  def1, def2, def3, def4, def5, def6, def7, def8, def9, def10 " 
		"  ,txt_ls1, txt_ls2, txt_ls3, txt_ls4, txt_ls5, txt_ls6, txt_ls7, txt_ls8, txt_ls9, txt_ls10 " 
		"  ,txt_rech1, txt_rech2, txt_rech3, txt_rech4, txt_rech5, txt_rech6, txt_rech7, txt_rech8, txt_rech9, txt_rech10 " 
		" ,( select disp_txt from atexte where txt_nr = txt_ls1)"
		" ,( select disp_txt from atexte where txt_nr = txt_ls2)"
		" ,( select disp_txt from atexte where txt_nr = txt_ls3)"
		" ,( select disp_txt from atexte where txt_nr = txt_ls4)"
		" ,( select disp_txt from atexte where txt_nr = txt_ls5)"
		" ,( select disp_txt from atexte where txt_nr = txt_ls6)"
		" ,( select disp_txt from atexte where txt_nr = txt_ls7)"
		" ,( select disp_txt from atexte where txt_nr = txt_ls8)"
		" ,( select disp_txt from atexte where txt_nr = txt_ls9)"
		" ,( select disp_txt from atexte where txt_nr = txt_ls10)"

		" ,( select disp_txt from atexte where txt_nr = txt_rech1)"
		" ,( select disp_txt from atexte where txt_nr = txt_rech2)"
		" ,( select disp_txt from atexte where txt_nr = txt_rech3)"
		" ,( select disp_txt from atexte where txt_nr = txt_rech4)"
		" ,( select disp_txt from atexte where txt_nr = txt_rech5)"
		" ,( select disp_txt from atexte where txt_nr = txt_rech6)"
		" ,( select disp_txt from atexte where txt_nr = txt_rech7)"
		" ,( select disp_txt from atexte where txt_nr = txt_rech8)"
		" ,( select disp_txt from atexte where txt_nr = txt_rech9)"
		" ,( select disp_txt from atexte where txt_nr = txt_rech10)"
		" from aufpt_verkfrage "
		" where ls = ? and mdn = ? and a = ? "
	);
}
 
