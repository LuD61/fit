//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
//=============================================================================

// llsmpvw.h : interface of the CLlSampleView class


class CLlSampleView : public CView
{
protected: // create from serialization only
	CLlSampleView();
	DECLARE_DYNCREATE(CLlSampleView)

// Attributes
public:
	CLlSampleDoc* GetDocument();

// Operations
public:

// Implementation
public:
	virtual ~CLlSampleView();
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


// Generated message map functions
protected:
	//{{AFX_MSG(CLlSampleView)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG	// debug version in llsmpvw.cpp
inline CLlSampleDoc* CLlSampleView::GetDocument()
   { return (CLlSampleDoc*) m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////
