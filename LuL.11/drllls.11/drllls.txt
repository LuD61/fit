170105 : Allgemeine Infos LL-Lieferscheindruck :

Programmstand 53100.rsr bzw. beldr.rsr  neuer als 16.11.04
sys_par llls1  aktivieren

bws_default-Eintrag "llls_druck" : 1 bedeutet : vor jedem Druck wird
        ein Druckerwahl-Dialog aktivert

LuL_Formate 53100.lst ( Kunden-LS) bzw. 53101.lst ( Filial-LS)


LuL-Basis-dll und .exes muessen vorhanden sein


drllls.dll (17.12.04) druckt die LS
                 enstsprechend  Kunden- bzw. Filialstamm-Infos

dr98000.exe ab 6.12.04  : Bearbeitung der Formate 53100 und 53101


Transfer von Formaten/Etiketten :
mittels der bat-dateien ( Alter mind. 02.12.04 )
"labelaus.bat" "labelein.bat"
"formaus.bat" "formein.bat"
dbio.exe mindestens Sept. 04



Verzeichnisse :
 %BWS%\format\ll\formunl
    hier stehenje FRMNAME Dateien rum :
        FRMNAMEF.nft
        FRMNAMEF.nff
        FRMNAMEF.nfh
        FRMNAMEF.foo
        FRMNAMEF.fow
        FRMNAME.lst

 %BWS%\format\ll\labelunl
     entsprechende Dateien fuer Etiketten




Interne Infos :

Folgende Vereinbarungen fuer drllls :
wird angestartet, falls llls1 = ON ist


Ablauf : Als Basiscursor dient :

 select * from  lsk, lsp ,a_bas
  where lsk.ls = lsp.ls
    and lsk.mdn = lsp.mdn
    and lsp.a = a_bas.a
    order by lsp.a, ......

  mit den Ranges  lsk.mdn und lsk.ls

  d.h. das sind die einzigen Relationen, alles andere wird im Basistatement
   weggefiltert
  und anschliessend gegbnenfalls  nachgepopelt

also : where und order-bedingung muessen genau darauf aufsetzen,

 weitere Tabellen koennen beliebig dazuselektiert werden,
  dienen jedoch nur der Layoutung und werden per Vorselektion gelesen
  ( d.h. nur dann unterstuetzt, wenn es hart im Programm drin steht)

// 16.03.05

ausgewertet werden z.Z. die Systemparameter

wa_pos_txt-> falls "ON" ,
    lsp.posi muss zwingend aktiv sein
    lsk.mdn und lsk.fil muss zwingend aktiv sein (sollte immer bereits so sein)
    und in lsp.lsp_txt muss Tabelle lsp_txt als texttabelle eingetragen  


wa_pos_txt-> falls "OFF" , muss Tabelle lspt als texttabelle eingetragen sein 


nachkpreis : steht jetzt als Wert in Tabelle rangt zur Verfuegung (aktuelle Programmstaende von dr98000 vorausgesetzt)

a_zustxt_wa -> falls aktiv, wird tabelle a_zus_txt gelesen und entsprechende pos-Zustaztext gefuellt



Kunden-Leergutverwaltung : in Abh�ngigkeit von syspar "kun_leer" wird Tabelle bestueckt,
	Ausgabe dann gegebnenfalls per "kun.kun_leer_kz" steuerbar
	- bei kun.kun_leer_kz = 0 Ausgabe wie bisher
	- bei kun.kun_leer_kz > 0 oder filiale wird Tabelle bestueckt, dann jedoch kein
		 Leergut-Artikel mehr als normale Position ausgegeben


	unbedingt folgende Felder aktivieren : a_bas.a_typ, kun.kun_leer_kz

	Tabelle "leer_lsh" ist Basis f�r die Layoutung
