#include "stdafx.h"
#include "DbClass.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif

#include "temptab.h"
#include "systables.h"
#include "syscolumns.h"
#include "form_ow.h"
#include "txt.h"
#include "range.h"
#include "item.h"
#include "adr.h"	// 171114
#include "ptabn.h"	// 171114
#include "kun.h"
#include "zustab.h"


extern DB_CLASS dbClass;
int verboteneFelder  ;	// 160113
char cverboteneFelder[120] ;	// 160113
static int badfeld = 0 ;	// 160113 : ungueltiges Feld-Marker


FIL_CLASS Fil ;
KUN_CLASS Kun ;
A_KUN_CLASS A_Kun ;
A_KUN_GX_CLASS A_Kun_Gx ;
A_KUN_TXT_CLASS A_Kun_txt ;	// 201213
AUFK_CLASS Aufk;			// 230914 


A_HNDW_CLASS a_hndw_class;			// 250808
A_EIG_CLASS a_eig_class;			// 250808
A_EIG_DIV_CLASS a_eig_div_class;	// 250808

extern AUFPT_VERKFRAGE_CLASS Aufpt_verkfrage; // 031114 : Achtung : es darf nur eine geben !!! 

extern ADR_CLASS Adr;		// 171114
extern TOU_CLASS Tou;		// 171114
extern PTABN_CLASS Ptabn;	// 171114
LEITWEG_CLASS Leitweg;		// 171114
TSMTGG_CLASS Tsmtgg;		// 171114
TSMTG_CLASS Tsmtg;			// 171114
DTA_CLASS Dta;				// 171114	

int redeckerpar = 0 ;		// 250808

int sonschill2 = 0 ;		// 230609

int conradi = 0 ;			// 221209 

int spmetro1_par = 0;		// 171114
int spboes_par = 0;			// 171114
int intboes = 0;			// 171114

char strboes_extramgr[257];	// 171114


static int opt_kmdn_pos ;	// 230108
static int opt_pmdn_pos ;	// 230108
static int opt_kls_pos ;	// 230108
static int opt_pls_pos ;	// 230108
static int opt_pposi_pos ;	// 230108

extern int datenausdatei ;	// wird in MainFrm.cpp gesetzt
extern short GlobalLila ;	// wird in MainFrm.cpp gesetzt
extern int macherange ( int , char * , char * ) ;	// 300904 aus range.cpp
int holerangesausdatei(char *, char * , char *) ;	// 300904 aus MainFrm.cpp

#define PRIV_LSK           "lsk"  
#define PRIV_LSP           "lsp"  
#define PRIV_A_BAS         "a_bas"  
#define PRIV_A_KUN_TXT    "a_kun_txt"  
#define NEBEN           1  
#define HAUPT           0  

// Mehr als 250 felder sollten nur sehr selten vorkommen 
char  tt_tabnam [MAXANZ][19];
int   tt_tabout [MAXANZ];
char  tt_feldnam[MAXANZ][19]; 	
int    tt_datetyp[MAXANZ];		// Datentyp in der Daba
int    tt_datelen[MAXANZ];		// Stringlaenge in der Daba
int    tt_feldtyp[MAXANZ];	    // Datentyp fuer Generator
int    tt_referenz  [MAXANZ];	// nur Index auf referenz-Liste
int    tt_range     [MAXANZ];	// nur Index auf range-Liste 
void * tt_sqlval    [MAXANZ];	// pointer auf (char-)Feldinhalte 
long	tt_sqllong    [MAXANZ];	// Matrix fuer longs und shorts 
double  tt_sqldouble [MAXANZ];	// Matrix fuer double 

void * savtt_sqlval    [MAXANZ];	// 310506 : pointer auf (char-)Feldinhalte 
long	savtt_sqllong    [MAXANZ];	// 310506 : Matrix fuer longs und shorts 
double  savtt_sqldouble [MAXANZ];	// 310506 : Matrix fuer double 



int     tt_hauneb [MAXANZ] ;	// Haupt- oder Nebencursor
								// um Entscheidung nur einmalig treffen zu muessen

 // mehr als 50 verschiedene Textbausteine sollten sehr selten sein
char  tt_creferenz[50][25];

// mehr als 20 verschiedene Bedingungen sind nicht erlaubt
char  tt_cvrange[MAXRANGE][50];	// maximale stringlaenge  49
char  tt_cbrange[MAXRANGE][50];
long  tt_lvrange[MAXRANGE];
long  tt_lbrange[MAXRANGE];
double  tt_dvrange[MAXRANGE];
double  tt_dbrange[MAXRANGE];
char  tt_qrange[MAXRANGE][100];	// Maximale Query-Laenge 99


static int aktupoint ;
static int v_abbruch ;
static int v_anzahl ;
static char tmyform_nr[99] ;

// 160305 -> zeiger auf diese felder
static int pa_bas_a ;
static int pa_bas_a_typ ;
static int pa_bas_a_gew ;	// 230609
static int pa_bas_a_bz1 ;

static int pa_bas_charg_hand ;	// 091209 

static int plsp_ls_ident ;	// 091209
static int plsp_ls_vk_euro ;
static int plsp_lief_me ;


// 201213 : Zeiger auf diese Felder oder eben "-1"
       int pa_kun_txt0;	// Aktiv oder eben nicht-Schalter
static int pa_kun_txt1;
static int pa_kun_txt2;
static int pa_kun_txt3;
static int pa_kun_txt4;
static int pa_kun_txt5;
static int pa_kun_sort;

int TEMPTAB_CLASS::structprep (char * myform_nr)
{

	int refdi = 0 ;	// Pointer zu Referenztexten
	int rangdi = 0 ;	// Pointer zu Ranges
	aktupoint = 0 ;
	sprintf ( tmyform_nr, "%s", myform_nr ) ;

	NFORM_TAB_CLASS Nform_tab_class ;
	SYSTABLES_CLASS Systables_class ;
	NFORM_FELD_CLASS Nform_feld_class ;
	SYSCOLUMNS_CLASS Syscolumns_class ;

	tt_tabnam [0][0] = '\0';

	sprintf ( nform_tab.form_nr , "%s", myform_nr );
	nform_tab.lila = GlobalLila ;

	pa_bas_charg_hand = pa_bas_a = pa_bas_a_typ = pa_bas_a_bz1 = pa_bas_a_gew = -1 ;
	plsp_ls_ident = plsp_ls_vk_euro = plsp_lief_me = -1 ;	// 160305 + 230609 + 091209 Initialisierung
	pa_kun_txt0 = pa_kun_txt1 = pa_kun_txt2 = pa_kun_txt3 = pa_kun_txt4 = pa_kun_txt5 = pa_kun_sort = -1; // 201213

	
//	NFORM_TAB_CLASS Nform_tab_class ;
	Nform_tab_class.opennformtab();
	while (!Nform_tab_class.lesenformtab () && aktupoint < 249 )	//       DATENSATZ lesen
	{


		sprintf ( systables.tabname, "%s", nform_tab.tab_nam );
//		SYSTABLES_CLASS Systables_class ;
		Systables_class.opensystables ();
		if ( ! Systables_class.lesesystables ())	// Test,ob es die Tabelle gibt 
		{
			sprintf ( nform_feld.tab_nam ,"%s", nform_tab.tab_nam) ;
			sprintf ( nform_feld.form_nr ,"%s", nform_tab.form_nr) ;
			nform_feld.lila = GlobalLila ;

//			NFORM_FELD_CLASS Nform_feld_class ;
			Nform_feld_class.opennform_feld() ;
			while ( ! Nform_feld_class.lesenform_feld ())	// Alle aktiven Felder dieser Tabelle
			{


				syscolumns.tabid = systables.tabid ;
				sprintf ( syscolumns.colname ,"%s", nform_feld.feld_nam) ;
//				SYSCOLUMNS_CLASS Syscolumns_class ;
				Syscolumns_class.opensyscolumns ();
				if (! Syscolumns_class.lesesyscolumns()) 
				{

//	Matritzen  laden ;
					sprintf ( tt_tabnam [aktupoint],"%s",clipped( nform_feld.tab_nam));

					if (  (!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_LSK))
					||(!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_LSP))
					||(!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_A_BAS))
					   )
					{
						tt_hauneb [aktupoint] = HAUPT ;

// 160305 A
						if (!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_LSP))
						{
							if ( ! strcmp ( clipped( nform_feld.feld_nam),"ls_vk_euro"))
								plsp_ls_vk_euro = aktupoint ;
							else
								if ( ! strcmp ( clipped( nform_feld.feld_nam),"ls_ident"))	// 091209 
									plsp_ls_ident = aktupoint ;
								else
									if( ! strcmp ( clipped( nform_feld.feld_nam),"lief_me")) 	
										plsp_lief_me = aktupoint ;
									else	// Artikeleintrag evtl. doppelt
										if( ! strcmp ( clipped( nform_feld.feld_nam),"a")) 	
										pa_bas_a = aktupoint ;
						}
						if (!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_A_BAS))
						{
							if ( ! strcmp ( clipped( nform_feld.feld_nam),"a_typ"))
								pa_bas_a_typ = aktupoint ;
							else
								if( ! strcmp ( clipped( nform_feld.feld_nam),"a_bz1")) 	
									pa_bas_a_bz1 = aktupoint ;
								else	// 091209 
									if( ! strcmp ( clipped( nform_feld.feld_nam),"charg_hand")) 	
										pa_bas_charg_hand = aktupoint ;
									else	// 230609 : a_gew dazu 
										if( ! strcmp ( clipped( nform_feld.feld_nam),"a_gew")) 	
											pa_bas_a_gew = aktupoint ;
										else	// Artikel-Eintrag evtl. doppelt
											if( ! strcmp ( clipped( nform_feld.feld_nam),"a" ))	// 230609 -> 091209 strncmp-> strcmp   	
												pa_bas_a = aktupoint ;


						}
						
// 160305 E

					}
					else
					{
						tt_hauneb [aktupoint] = NEBEN ;
					}

					tt_tabout [aktupoint] = (int) nform_tab.delstatus ;
// OUTER ist irrelevant
					tt_tabout [aktupoint] = 0 ;

					sprintf ( tt_feldnam[aktupoint],"%s",clipped( nform_feld.feld_nam)); 
					tt_datetyp[aktupoint] = syscolumns.coltype ;	// Datentyp in der Daba
					tt_datelen[aktupoint] = syscolumns.collength ;	// Datenlaenge in der Daba
					tt_feldtyp[aktupoint] = nform_feld.feld_typ ;	// Datentyp fuer Generator
					if ( strlen ( clipped( nform_feld.krz_txt)) > 0 && nform_feld.feld_typ)	// 091209 : clever : auf Vorrat gebaut
					{
						refdi ++ ;	// Achtung : index geht erst mit Feld 1 los ... 
						sprintf ( tt_creferenz[refdi], "%s", clipped(nform_feld.krz_txt));
						tt_referenz [aktupoint] = refdi ;
					}
					else
						tt_referenz [aktupoint] = 0 ;

					if ( nform_feld.range )
					{
						rangdi ++ ;	// Achtung : index geht erst mit Feld 1 los ... 
						tt_referenz [aktupoint] = rangdi ;
}
					else
					{
					}
					tt_range[aktupoint] = nform_feld.range ;	

// 201213 A
					if (!strcmp (clipped ( tt_tabnam[aktupoint] ), PRIV_A_KUN_TXT))
					{
						if ( ! strcmp ( clipped( nform_feld.feld_nam),"txt1"))
						{
							pa_kun_txt0 = 1;
							pa_kun_txt1 = aktupoint ;
						}
						else
							if ( ! strcmp ( clipped( nform_feld.feld_nam),"txt2"))
							{
								pa_kun_txt0 = 1;
								pa_kun_txt2 = aktupoint ;
							}
							else
								if( ! strcmp ( clipped( nform_feld.feld_nam),"txt3")) 	
								{
									pa_kun_txt0 = 1;
									pa_kun_txt3 = aktupoint ;
								}
								else
									if( ! strcmp ( clipped( nform_feld.feld_nam),"txt4")) 	
									{
										pa_kun_txt0 = 1;
										pa_kun_txt4 = aktupoint ;
									}
									else
										if ( ! strcmp ( clipped( nform_feld.feld_nam),"txt5"))
										{
											pa_kun_txt0 = 1;
											pa_kun_txt5 = aktupoint ;
										}
										else
											if( ! strcmp ( clipped( nform_feld.feld_nam),"sort")) 	
											{
												pa_kun_txt0 = 1;
												pa_kun_sort = aktupoint ;
											}
						
					}
// 201213 E

					aktupoint ++ ;
					tt_tabnam[aktupoint][0] = '\0' ;
				}
			}	// fetch nformfeld_curs
		}		// if tabid_curs
	}			// nformtab_curs 

	return aktupoint ;
}
int TEMPTAB_CLASS::getanzahl (void)
{
	return v_anzahl ;
}

// 121104 A
void TEMPTAB_CLASS::inittemptab ()
{
	int i ;
	i=0;
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;
		if ( tt_range[i] ) { i++ ; continue ; }	// Range-felder NICHT initialisieren
		switch ( tt_feldtyp[i] )
		{
		case 1 : // ptabn
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
			sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
// 310506 : so sah das bisher aus :			tt_sqlval[i], (char *)NULL ;
 				sprintf ( (char *)tt_sqlval[i] ," " ) ;	// 310506
				break ;
			case(iDBSMALLINT):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
				tt_sqllong[i] = 0 ;
				break ;
			case(iDBINTEGER):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
				tt_sqllong[i] = 0 ;
				break ;
			case(iDBDECIMAL):
				savtt_sqldouble[i] = tt_sqldouble[i] ;	// 310506
				tt_sqldouble[i] =0.0L ;
				break ;
			default :	// schlechte Notbremse
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
				break ;
			}
//			memcpy ( &ptabn,&ptabn_null, sizeof(struct PTABN));

			break ;

		case 2 : // Textbaustein 
				savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
				tt_sqllong[i] = 0 ;
			break ;
		case 3 : // adresse
			savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
			tt_sqllong[i] = 0 ;
//				memcpy ( &adr,&adr_null, sizeof(struct ADR));
			break ;
		default :	// Standard-Felder 
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
				break ;
			case(iDBSMALLINT):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
				tt_sqllong[i] = 0L ;
				break ;
			case(iDBINTEGER):
				savtt_sqllong[i] = tt_sqllong[i] ;	// 310506
				tt_sqllong[i] = 0L ;
				break ;
			case(iDBDECIMAL):
				savtt_sqldouble[i] = tt_sqldouble[i] ;	// 310506
				tt_sqldouble[i]= 0.0L ;
				break ;
			case(iDBDATUM):
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i]," ")  ;
				break ;		
			default :	// schlechte Notbremse
				sprintf ( (char *)savtt_sqlval[i] ,"%s", (char *)tt_sqlval[i]) ;	// 310506
				sprintf ( (char *)tt_sqlval[i], " ");
				break ;
			
			}
			break ;
		} ;
		i ++ ;
	}		// while	

}

// 310506 A
void TEMPTAB_CLASS::reloadtemptab ()
{
	int i ;
	i=0;
	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;
		if ( tt_range[i] ) { i++ ; continue ; }	// Range-felder NICHT initialisieren
		switch ( tt_feldtyp[i] )
		{
		case 1 : // ptabn
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
			sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			case(iDBSMALLINT):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			case(iDBINTEGER):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			case(iDBDECIMAL):
				tt_sqldouble[i] = savtt_sqldouble[i] ;
				break ;
			default :	// schlechte Notbremse
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			}
			break ;

		case 2 : // Textbaustein 
				tt_sqllong[i] = savtt_sqllong[i] ;
			break ;
		case 3 : // adresse
			tt_sqllong[i] = savtt_sqllong[i] ;
			break ;
		default :	// Standard-Felder 
			switch ( tt_datetyp[i] )
			{
			case(iDBCHAR):
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;	
				break ;
			case(iDBSMALLINT):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			case(iDBINTEGER):
				tt_sqllong[i] = savtt_sqllong[i] ;
				break ;
			case(iDBDECIMAL):
				tt_sqldouble[i] = savtt_sqldouble[i] ;
				break ;
			case(iDBDATUM):
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;		
			default :	// schlechte Notbremse
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)savtt_sqlval[i]) ;
				break ;
			}
			break ;
		} ;
		i ++ ;
	}		// while	

}

double TEMPTAB_CLASS::a_bas_a(void)
{
		if ( pa_bas_a == -1 ) return 0 ;
		return (tt_sqldouble[pa_bas_a] ) ;
}

// 091209 
long TEMPTAB_CLASS::a_bas_charg_hand(void)
{
		if ( pa_bas_charg_hand == -1 ) return 0 ;
		return (tt_sqllong[pa_bas_charg_hand] ) ;
}

// 091209 
long TEMPTAB_CLASS::lsp_ls_ident(void)
{
		if ( plsp_ls_ident == -1 ) return -1 ;
		char * p = (char *) (tt_sqlval[plsp_ls_ident] ) ;
		return atol ( p ) ;
}


short TEMPTAB_CLASS::a_bas_a_typ(void)
{

	if ( pa_bas_a_typ == -1 ) return 0 ;
	return ( (short) tt_sqllong[pa_bas_a_typ] ) ;
}

double TEMPTAB_CLASS::a_bas_a_gew(void)
{

	if ( pa_bas_a_gew == -1 ) return 0 ;
	return ( tt_sqldouble[pa_bas_a_gew] ) ;
}


char * TEMPTAB_CLASS::a_bas_a_bz1(void)
{
		if ( pa_bas_a_bz1 == -1 ) return NULL ;
		return ( (char *) tt_sqlval[pa_bas_a_bz1] ) ;
}
double TEMPTAB_CLASS::lsp_lief_me(void)
{
		if ( plsp_lief_me == -1 ) return 0 ;
		return (tt_sqldouble[plsp_lief_me] ) ;
}
double TEMPTAB_CLASS::lsp_ls_vk_euro(void)
{
	int di =  0 ;
	di = di ;
	if ( plsp_ls_vk_euro == -1 ) return 0 ;
		return (tt_sqldouble[plsp_ls_vk_euro] ) ;
}


/* ---->
double TEMPTAB_CLASS::get_lsp_a(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ), PRIV_LSP))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ) , "a" )) return ( tt_sqldouble[i] );
		}
	}
	return 0.0L ;
};
< ----- */

long get_lsk_kun(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ), "kun" )) return ( tt_sqllong[i] );
		}
	}
	return 0 ;
}


short get_lsk_kun_fil(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "kun_fil" )) return ( (short)tt_sqllong[i] );
		}
	}
	return 0 ;
}

long get_lsk_auf(void)	// 230914
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "auf" )) return ( (short)tt_sqllong[i] );
		}
	}
	return 0 ;
}

// 171114 A : Lieferadresse des LS, das Feld muss jedenfalls aktiviert sein f�r boesi !!! 
long get_lsk_adr(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "adr" )) return ( tt_sqllong[i] );
		}
	}
	return 0 ;
}

// 171114 A : Teilsortiment des LS, das Feld muss jedenfalls aktiviert sein f�r boesi !!! 
long get_lsk_teil_smt(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "teil_smt" )) return ( tt_sqllong[i] );
		}
	}
	return 0 ;
}


// 171114 A : Lieferadresse des LS, das Feld muss jedenfalls aktiviert sein f�r boesi !!! 
long get_lsk_tou(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "tou" )) return ( tt_sqllong[i] );
		}
	}
	return 0 ;
}


short TEMPTAB_CLASS::get_lsk_mdn(void)
{
// 230108 : Optimierung
	if ( opt_kmdn_pos > -1 && opt_kmdn_pos < MAXANZ )
	{
		if ( !strcmp (clipped ( tt_tabnam[opt_kmdn_pos] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[opt_kmdn_pos]), "mdn" )) return ( (short)tt_sqllong[opt_kmdn_pos] );
		}
		opt_kmdn_pos = -1 ;
	}
	if ( opt_pmdn_pos > -1 && opt_pmdn_pos < MAXANZ )
	{
		if ( !strcmp (clipped ( tt_tabnam[opt_pmdn_pos] ) , PRIV_LSP))
		{
			if ( !strcmp(clipped ( tt_feldnam[opt_pmdn_pos]), "mdn" )) return ( (short)tt_sqllong[opt_pmdn_pos] );
		}
		opt_pmdn_pos = -1 ;
	}

	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "mdn" ))
			{	opt_kmdn_pos = i ;	// 230108 
				return ( (short)tt_sqllong[i] );
			}
		}
	}

// Hilfsweise auch noch in lsp suchen 
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ), PRIV_LSP))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ) , "mdn" ))
			{
				opt_pmdn_pos = i ;	// 230108
				return ( (short)tt_sqllong[i] );
			}
		}
	}

	return 0 ;
}

// 110205
short TEMPTAB_CLASS::get_lsk_fil(void)
{
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "fil" )) return ( (short)tt_sqllong[i] );
		}
	}

// Hilfsweise auch noch in lsp suchen 
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ), PRIV_LSP))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ) , "fil" )) return ( (short)tt_sqllong[i] );
		}
	}

	return 0 ;
};

long TEMPTAB_CLASS::get_lsk_ls(void)
{

	if ( opt_kls_pos > -1 && opt_kls_pos < MAXANZ )
	{
		if ( !strcmp (clipped ( tt_tabnam[opt_kls_pos] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[opt_kls_pos]), "ls" )) return ( (long)tt_sqllong[opt_kls_pos] );
		}
		opt_kls_pos = -1 ;
	}
	if ( opt_pls_pos > -1 && opt_pls_pos < MAXANZ )
	{
		if ( !strcmp (clipped ( tt_tabnam[opt_pls_pos] ) , PRIV_LSP))
		{
			if ( !strcmp(clipped ( tt_feldnam[opt_pls_pos]), "ls" )) return ( (long)tt_sqllong[opt_pls_pos] );
		}
		opt_pls_pos = -1 ;
	}


	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_LSK))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "ls" ))
			{
				opt_kls_pos = i ;	// 230108
				return ( (long)tt_sqllong[i] );
			}
		}
	}

// Hilfsweise auch noch in lsp suchen 
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ), PRIV_LSP))
		{
			if ( !strcmp(clipped ( tt_feldnam[i] ) , "ls" )) 
			{
				opt_pls_pos = i ;	// 230108
				return ( (long)tt_sqllong[i] );
			}
		}
	}

	return 0L ;
}

long TEMPTAB_CLASS::get_lsp_posi(void)
{

	if ( opt_pposi_pos > -1 && opt_pposi_pos < MAXANZ )
	{
		if ( !strcmp (clipped ( tt_tabnam[opt_pposi_pos] ) , PRIV_LSP))
		{
			if ( !strcmp(clipped ( tt_feldnam[opt_pposi_pos]), "ls" )) return ( (long)tt_sqllong[opt_pposi_pos] );
		}
		opt_pposi_pos = -1 ;
	}

	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		
		if ( !strcmp (clipped ( tt_tabnam[i] ) , PRIV_LSP))
		{
			if ( !strcmp(clipped ( tt_feldnam[i]), "posi" ))
			{
				opt_pposi_pos = i ;	// 230108
				return ( (long)tt_sqllong[i] );
			}
		}
	}
	return 0L  ;
}

void ladetyp( int i, void * pointi )
{

	if ( i > 120 )
	{
		 i  = i;
	}
	
		                
	switch ( tt_feldtyp[i] )
	{
	case 1 : // ptabn
		switch ( tt_datetyp[i] )
		{
		case(iDBCHAR):
			if (  (char *) pointi == NULL )
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
			else
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *) pointi ) ;
				break ;
		case(iDBSMALLINT):

//			tt_sqllong[i] = *((long *) pointi) ;	 * 091209
			tt_sqllong[i] = (long)*((short *) pointi) ;


				break ;
		case(iDBINTEGER):
			tt_sqllong[i] = *((long *)pointi) ;
				break ;
		case(iDBDECIMAL):
			tt_sqldouble[i] = *((double*)pointi) ;
				break ;
		default :	// schlechte Notbremse
			// neu am 160113
			badfeld ++ ;	// unbekannter Datentyp 
 			sprintf ( (char *)tt_sqlval[i] ," " ) ;
				break ;
		}

		break ;

	case 2 : // Textbaustein 
				tt_sqllong[i] = *((long *)pointi) ;
		break ;
	case 3 : // adresse
		tt_sqllong[i] = *((long *)pointi) ;
		break ;
	default :	// Standard-Felder 
		switch ( tt_datetyp[i] )
		{
		case(iDBCHAR):
			if ( (char *)pointi == NULL )
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
			else
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)pointi ) ;
				break ;
		case(iDBSMALLINT):

//			tt_sqllong[i] = *((long *) pointi) ;	 * 190105
			tt_sqllong[i] = (long)*((short *) pointi) ;
				break ;
		case(iDBINTEGER):
			tt_sqllong[i] = *((long *)pointi) ;
				break ;
		case(iDBDECIMAL):
			tt_sqldouble[i]= *((double *) pointi) ;
				break ;
		case(iDBDATUM):
			if ( (char *) pointi == NULL )
				sprintf ( (char *)tt_sqlval[i] ," " ) ;
			else
				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)pointi ) ;
				break ;
		default :	// schlechte Notbremse
			// neu am 160113
			badfeld ++ ;	// unbekannter Datentyp 

// 160113			if ( (char *)pointi == NULL )
// 160113				sprintf ( (char *)tt_sqlval[i] ," " ) ;
// 160113			else
// 160113				sprintf ( (char *)tt_sqlval[i] ,"%s", (char *)pointi ) ;

			break ;
			
		}
		break ;
	} ;
}


bool komplettierefil ( void )
{

	badfeld = 0 ;	// 160113

	int oi = 0 ;
	int ei = 0 ;	// 160113
	char hilfech[26] ;
//	int colint ;	 230609 : wozu ?
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "fil"))
		{
			oi = 1 ;
			ei = 0 ;	// 160113

			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
// 230609 : wozu ?			colint = (int)strlen (hilfech);
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "abr_period" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.abr_period );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.adr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr_lief" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.adr_lief );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "afl" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.afl );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "auf_typ" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.auf_typ );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "best_kz" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.best_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bli_kz" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.bli_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "dat_ero" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.dat_ero );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "daten_mnp" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.daten_mnp );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "delstatus" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.delstatus );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fil" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.fil );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fil_kla" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.fil_kla );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fil_gr" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.fil_gr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fl_lad" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.fl_lad );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fl_nto" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.fl_nto );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fl_vk_ges" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.fl_vk_ges );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "frm" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.frm );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "iakv" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.iakv );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "inv_rht" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.inv_rht );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.kun );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lief" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.lief );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lief_rht" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.lief_rht );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lief_s" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.lief_s );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ls_abgr" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.ls_abgr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ls_kz" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.ls_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ls_sum" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.ls_sum );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "mdn" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.mdn );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pers" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.pers );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pers_anz" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.pers_anz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pos_kum" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.pos_kum );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_ausw" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.pr_ausw );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_bel_entl" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.pr_bel_entl );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_fil_kz" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.pr_fil_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_lst" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.pr_lst );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_vk_kz" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.pr_vk_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_bed_theke_lng" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.reg_bed_theke_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_kt_lng" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.reg_kt_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_kue_lng" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.reg_kue_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_lng" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.reg_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_tks_lng" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.reg_tks_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "reg_tkt_lng" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.reg_tkt_lng );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ret_entl" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.ret_entl );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "smt_kz" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.smt_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sonst_einh" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.sonst_einh );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sprache" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.sprache );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sw_kz" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.sw_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tou" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.tou );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "umlgr" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.umlgr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "verk_st_kz" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.verk_st_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vrs_typ" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.vrs_typ );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "inv_akv" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)fil.inv_akv );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "planumsatz" )) 
			{
				ei ++ ;	// 160113
				ladetyp( i, (void *)&fil.planumsatz );
				continue ;
			};
			if ( ! ei ) break ;	// 160113 : nicht unterstuetztes Feld angekreuzt

			continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// while

// 160113 : ret-code 
	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE ; 
}


bool komplettierezerldaten ( void )
{

	int oi = 0 ;

	int ei = 0 ;

	badfeld = 0 ;

	char hilfech[26] ;

	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "zerldaten"))
		{
			oi = 1 ;
			ei = 0 ;	// 160113

			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
// 230609 : wozu ?			colint = (int)strlen (hilfech);
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "mdn" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)&zerldaten.mdn );
				continue ;
			};

// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fil" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)&zerldaten.fil );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "partie" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)&zerldaten.partie );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lief" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) zerldaten.lief );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lief_rech_nr" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) zerldaten.lief_rech_nr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=AAAAAAAAAAAAAA
			if ( !strcmp ( hilfech, "ident_extern" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)&zerldaten.ident_extern );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ident_intern" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) zerldaten.ident_intern );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "a" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)&zerldaten.a );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "a_gt" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) &zerldaten.a_gt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kategorie" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) zerldaten.kategorie );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "schnitt" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) &zerldaten.schnitt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "gebland" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) &zerldaten.gebland );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "mastland" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) &zerldaten.mastland );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "schlaland" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)&zerldaten.schlaland );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "zerland" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)&zerldaten.zerland );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "esnum" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)zerldaten.esnum );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "eznum1" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)zerldaten.eznum1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "eznum2" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) zerldaten.eznum2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "eznum3" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)zerldaten.eznum3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#= geht nicht so recht
//			if ( !strcmp ( hilfech, "dat" )) 
//			{
//				ladetyp( i, (void *)zerldaten.dat );
//				continue ;
//			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "anz_gedruckt" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)&zerldaten.anz_gedruckt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "anz_entnommen" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) &zerldaten.anz_entnommen );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lfd_i" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *) &zerldaten.lfd_i );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "zerlegt" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)&zerldaten.zerlegt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "aktiv" )) 
			{
				ei ++ ;		// 160113
				ladetyp( i, (void *)&zerldaten.aktiv );
				continue ;
			};
			if ( ! ei )	// 160113 
				break ;

			continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// while 

// 160113 : ret-code 
	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE;

}


bool komplettiereabaserw ( void )
{

	badfeld = 0 ;

	int oi = 0 ;
	int ei = 0 ;	
	char hilfech[26] ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "a_bas_erw"))
		{
			oi = 1 ;
			ei = 0 ;

			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "a" )) 
			{

				ei ++ ;	
				ladetyp( i, (void *)&a_bas_erw.a );
				continue ;
			};

// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "userdef1" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.userdef1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "userdef2" )) 
 			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.userdef2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "userdef3" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.userdef3 );
				continue ;
			};
			
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pp_a_bz1" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)a_bas_erw.pp_a_bz1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pp_a_bz2" )) 
 			{
				ei ++ ;
				ladetyp( i, (void *)a_bas_erw.pp_a_bz2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lgr_tmpr" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)a_bas_erw.lgr_tmpr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lupine" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)a_bas_erw.lupine );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "schutzgas" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)a_bas_erw.schutzgas );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "huelle" )) 
			{
				ei ++ ;	
				ladetyp( i, (void *)&a_bas_erw.huelle );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "shop_wg1" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.shop_wg1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "shop_wg2" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.shop_wg2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "shop_wg3" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.shop_wg3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "shop_wg4" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.shop_wg4 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "shop_wg5" )) 
 			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.shop_wg5 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tara2" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.tara2);
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "a_tara2" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.a_tara2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tara3" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.tara3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "a_tara3" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.a_tara3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tara4" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.tara4 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "a_tara4" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.a_tara4 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "salz" )) 
  			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.salz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "davonfett" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.davonfett );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "davonzucker" )) 
    		{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.davonzucker );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ballaststoffe" )) 
    		{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.ballaststoffe );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "shop_aktion" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)a_bas_erw.shop_aktion );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "shop_neu" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)a_bas_erw.shop_neu);
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "shop_tv" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)a_bas_erw.shop_tv );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "shop_agew" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.shop_agew );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "a_bez" )) 
 			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.a_bez );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "zutat" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)a_bas_erw.zutat );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "minstaffgr" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.minstaffgr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "l_pack" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.l_pack );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "b_pack" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.b_pack );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "h_pack" )) 
  			{
				ei ++ ;
				ladetyp( i, (void *)&a_bas_erw.h_pack);
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=	271114 
			if ( !strcmp ( hilfech, "zolltarifnr" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)a_bas_erw.zolltarifnr );
				continue ;
			};

			if ( ! ei ) break ;

			continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// while

	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE ; 
}

// 031114 A
bool komplettiereaufpt_verkfrage ( short emdn, long  els, double ea )
{

	badfeld = 0 ;

	int oi = 0 ;
	int ei = 0 ;	
	char hilfech[26] ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "aufpt_verkfrage"))
		{
//			oi = 1 ; evtl. mehrfach durchsuchen 
			ei = 0 ;

			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "def1" )) 
			{

				Aufpt_verkfrage.leseaufpt_verkfrage(emdn,els,ea ) ;
				ei ++ ;	
				ladetyp( i, (void *)&aufpt_verkfrage.def1 );
				continue ;
			};

// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "def2" )) 
			{
				Aufpt_verkfrage.leseaufpt_verkfrage(emdn,els,ea ) ;
				ei ++ ;
				ladetyp( i, (void *)&aufpt_verkfrage.def2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "def3" )) 
 			{
				Aufpt_verkfrage.leseaufpt_verkfrage(emdn,els,ea ) ;
				ei ++ ;
				ladetyp( i, (void *)&aufpt_verkfrage.def3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "def4" )) 
			{
				Aufpt_verkfrage.leseaufpt_verkfrage(emdn,els,ea ) ;
				ei ++ ;
				ladetyp( i, (void *)&aufpt_verkfrage.def4 );
				continue ;
			};
			
			// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "def5" )) 
			{
				Aufpt_verkfrage.leseaufpt_verkfrage(emdn,els,ea ) ;
				ei ++ ;
				ladetyp( i, (void *)&aufpt_verkfrage.def5 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "def6" )) 
 			{
				Aufpt_verkfrage.leseaufpt_verkfrage(emdn,els,ea ) ;
				ei ++ ;
				ladetyp( i, (void *)&aufpt_verkfrage.def6 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "def7" )) 
			{
				Aufpt_verkfrage.leseaufpt_verkfrage(emdn,els,ea ) ;
				ei ++ ;
				ladetyp( i, (void *)&aufpt_verkfrage.def7 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "def8" )) 
			{
				Aufpt_verkfrage.leseaufpt_verkfrage(emdn,els,ea ) ;
				ei ++ ;
				ladetyp( i, (void *)&aufpt_verkfrage.def8 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "def9" )) 
			{
				Aufpt_verkfrage.leseaufpt_verkfrage(emdn,els,ea ) ;
				ei ++ ;
				ladetyp( i, (void *)&aufpt_verkfrage.def9 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "def10" )) 
			{
				Aufpt_verkfrage.leseaufpt_verkfrage(emdn,els,ea ) ;
				ei ++ ;	
				ladetyp( i, (void *)&aufpt_verkfrage.def10 );
				continue ;
			};

//			if ( ! ei ) break ;

			continue ;
		}
//		if ( oi ) break ;	// Tabelle zu ende 
	}	// while

//	if ( badfeld  || ( ! ei && oi ))
	if ( badfeld )	// wenn ix da ist, ist alles gut .....
		return FALSE ;

	return TRUE ; 
}

// 031114 E

// 230914 A
bool komplettiereaufk ( void )
{

	badfeld = 0 ;

	int oi = 0 ;
	int ei = 0 ;	
	char hilfech[26] ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "aufk"))
		{
			oi = 1 ;
			ei = 0 ;

			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fil" )) 
			{
				ei ++ ;	
				ladetyp( i, (void *)&aufk.fil );
				continue ;
			};

// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ang" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.ang );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr" )) 
 			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.adr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.kun );
				continue ;
			};
			
			// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "auf" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.auf );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lieferdat" )) 
 			{
				ei ++ ;
				ladetyp( i, (void *)aufk.lieferdat );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lieferzeit" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.lieferzeit );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "hinweis" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.hinweis );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "auf_stat" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.auf_stat );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "zeit_dec" )) 
			{
				ei ++ ;	
				ladetyp( i, (void *)&aufk.zeit_dec );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kopf_txt" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.kopf_txt );
				continue ;
			};



// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fuss_txt" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.fuss_txt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vertr" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.vertr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "auf_ext" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.auf_ext );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tou" )) 
 			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.tou );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pers_nam" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.pers_nam);
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "komm_dat" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.komm_dat );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "best_dat" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.best_dat );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "waehrung" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.waehrung );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "auf_art" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.auf_art );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "gruppe" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.gruppe );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ccmarkt" )) 
  			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.ccmarkt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fak_typ" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.fak_typ );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tou_nr" )) 
    		{
				ei ++ ;
				ladetyp( i, (void *)&aufk.tou_nr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ueb_kz" )) 
    		{
				ei ++ ;
				ladetyp( i, (void *)&aufk.ueb_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fix_dat" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.fix_dat );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "komm_name" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.komm_name);
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "akv" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.akv );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "akv_zeit" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.akv_zeit );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bearb" )) 
 			{
				ei ++ ;
				ladetyp( i, (void *)aufk.bearb );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bearb_zeit" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)aufk.bearb_zeit );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "psteuer_kz" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&aufk.psteuer_kz );
				continue ;
			};

			if ( ! ei ) break ;

			continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// while

	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE ; 
}


// 230914 E



bool komplettiereakuntxt ( void )
{

	int oi = 0 ;

	int ei = 0 ;

	badfeld = 0 ;

	char hilfech[26] ;

	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "a_kun_txt"))
		{
			oi = 1 ;
			ei = 0 ;

			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt1" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_kun_txt.txt1 );
				continue ;
			};

// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt2" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_kun_txt.txt2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt3" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_kun_txt.txt3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt4" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_kun_txt.txt4 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt5" )) 
			{
				ei ++ ;	
				ladetyp( i, (void *)&a_kun_txt.txt5 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sort" )) 
			{
				ei ++ ;
				ladetyp( i, (void *)&a_kun_txt.sort );
				continue ;
			};


			continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// while 

	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE;

}

bool komplettierekun ( void )
{

	int oi = 0 ;
	int ei = 0 ;	// 160113
	badfeld = 0 ;
	char hilfech[26] ;
// 230609 : wozu ?	int colint ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "kun"))
		{
			oi = 1 ;
			ei = 0 ;	// 160113
			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
// 230609 : wozu ?			colint = (int)strlen (hilfech);
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "mdn" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.mdn );
				continue ;
			};

// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fil" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.fil );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.adr1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.adr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "adr3" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.adr3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_seit" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_seit );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt_nr1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.txt_nr1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "frei_txt1" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.frei_txt1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_krz1" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_krz1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_bran" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_bran );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_krz2" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_krz2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_krz3" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_krz3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_typ" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_typ );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bbn" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.bbn );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_stu" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_stu );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_lst" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_lst );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vereinb" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.vereinb );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "inka_nr" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.inka_nr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vertr1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.vertr1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vertr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.vertr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "statk_period" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.statk_period );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "a_period" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.a_period );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sprache" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sprache );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt_nr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.txt_nr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "frei_txt2" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.frei_txt2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "freifeld1" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.freifeld1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "freifeld2" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.freifeld2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tou" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.tou );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "vers_art" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.vers_art );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "lief_art" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.lief_art );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "fra_ko_ber" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.fra_ko_ber );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "rue_schei" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.rue_schei );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "form_typ1" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.form_typ1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "auflage1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.auflage1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "freifeld3" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.freifeld3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "freifeld4" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.freifeld4 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "zahl_art" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.zahl_art );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "zahl_ziel" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.zahl_ziel );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "form_typ2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.form_typ2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "auflage2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.auflage2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "txt_nr3" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.txt_nr3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "frei_txt3" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.frei_txt3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "nr_bei_rech" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.nr_bei_rech );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "rech_st" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.rech_st );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sam_rech" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sam_rech );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "einz_ausw" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.einz_ausw );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "gut" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.gut );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "rab_schl" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.rab_schl );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bonus1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.bonus1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bonus2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.bonus2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tdm_grenz1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.tdm_grenz1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "tdm_grenz2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.tdm_grenz2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "jr_plan_ums" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.jr_plan_ums );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "deb_kto" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.deb_kto );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kred_lim" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kred_lim );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "inka_zaehl" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.inka_zaehl );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bank_kun" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.bank_kun );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "blz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.blz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kto_nr" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kto_nr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "hausbank" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.hausbank );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_of_po" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_of_po );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_of_lf" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_of_lf );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_of_best" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_of_best );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "delstatus" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.delstatus );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_bran2" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.kun_bran2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "rech_fuss_txt" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.rech_fuss_txt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ls_fuss_txt" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.ls_fuss_txt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ust_id" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.ust_id );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "rech_kopf_txt" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.rech_kopf_txt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ls_kopf_txt" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.ls_kopf_txt );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "gn_pkt_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.gn_pkt_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sw_rab" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sw_rab );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bbs" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.bbs );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "inka_nr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.inka_nr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sw_fil_gr" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sw_fil_gr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sw_fil" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sw_fil );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ueb_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.ueb_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "modif" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.modif );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_leer_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_leer_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ust_id16" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.ust_id16 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "iln" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.iln );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "waehrung" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.waehrung );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_ausw" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_ausw );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_hier" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_hier );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_ausw_ls" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_ausw_ls );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "pr_ausw_re" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.pr_ausw_re );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_gr1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_gr1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_gr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_gr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "eg_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.eg_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "bonitaet" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.bonitaet );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kred_vers" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kred_vers );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kst" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kst );
				continue ;
			};

// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "edi_typ" )) 
			{
				ei ++ ; ladetyp( i, (void *) kun.edi_typ );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_dta" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_dta );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_kz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_umf" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_umf );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_abr" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_abr );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_gesch" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_gesch );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_satz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_satz );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_med" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_med );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_nam" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sedas_nam );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_abk1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_abk1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_abk2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_abk2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_abk3" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_abk3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_nr1" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sedas_nr1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_nr2" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sedas_nr2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_nr3" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sedas_nr3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_vb1" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_vb1 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_vb2" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_vb2 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_vb3" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.sedas_vb3 );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "sedas_iln" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.sedas_iln );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kond_kun" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kond_kun );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "kun_schema" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.kun_schema );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "plattform" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.plattform );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "be_log" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.be_log );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "stat_kun" )) 
			{
				ei ++ ; ladetyp( i, (void *)&kun.stat_kun );
				continue ;
			};
// #=#=#=#=#=#=#=#=#=#=
			if ( !strcmp ( hilfech, "ust_nummer" )) 
			{
				ei ++ ; ladetyp( i, (void *)kun.ust_nummer );
				continue ;
			};
			if ( ! ei )		// 160113
				break ;	

		continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// while 

	// 160113 : ret-code 
	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE;
}

// 230609  

bool komplettierea_kun_gx(int istatus)
{
	if ( istatus ) 
		a_kun_gx.geb_anz = 0 ;


// hier kommt man nur bei aktiviertem sonschill2 vorbei

	int oi = 0 ;
	int ei = 0 ;	// 160113
	badfeld = 0 ;	// 160113
	char hilfech[26] ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "a_kun"))
		{
			oi = 1 ;
			ei = 0 ;	// 160113

			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
// ####################
			if ( !strcmp ( hilfech, "geb_anz" )) 
			{
				ei ++ ; ladetyp( i, (void *)&a_kun_gx.geb_anz );
				break ;	// wir wollen nur ein feld ueberladen
			};
			continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// for 
		// 160113 : ret-code 
	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE;

}


// 230506 : void komplettierea_kun ( void )
// 230609 : a_kun.geb_anz wird gegebnenfalls mit a_kun_gx.geb_anz uberladen !!!! 
bool komplettierea_kun ( int istatus ,double ext_a , short ext_typ )
{

// 250808 A
		double red_inh = 0.0 ;
		int  istat = 0 ;
		if ( ext_typ > 0  )
		{
			if ( ext_typ == 1 )
			{
				a_hndw.a = ext_a ;
				istat = a_hndw_class.opena_hndw () ;
				if ( !istat ) istat = a_hndw_class.lesea_hndw () ;
				if ( !istat ) red_inh = a_hndw.inh ;
			}
			else
			{
				if ( ext_typ == 2 )
				{
					a_eig.a = ext_a ;
					istat = a_eig_class.opena_eig () ;
					if ( !istat ) istat = a_eig_class.lesea_eig () ;
					if ( !istat ) red_inh = a_eig.inh ;
				}
				else
				{
					if ( ext_typ == 3 )
					{
						a_eig_div.a = ext_a ;
						istat = a_eig_div_class.opena_eig_div () ;
						if ( !istat ) istat = a_eig_div_class.lesea_eig_div () ;
						if ( !istat ) red_inh = a_eig_div.inh ;
					}
				}
			}
		}

// 250808 E 

	if ( istatus )
		memcpy ( &a_kun, &a_kun_null, sizeof ( struct A_KUN) ) ;


	int oi = 0 ;

	int ei = 0 ;	// 160113

	badfeld = 0 ;

	char hilfech[26] ;
	int colint ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( !strcmp (clipped ( tt_tabnam[i] ), "a_kun"))
		{
			oi = 1 ;
			ei = 0 ;	// 160113

			sprintf ( hilfech, "%s", clipped(tt_feldnam[i]));
			colint = (int)strlen (hilfech);
// ####################
			if ( !strcmp ( hilfech, "mdn" )) 
			{
				ei ++ ; ladetyp( i, (void *)&a_kun.mdn );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "fil" )) 
			{
				ei ++ ; ladetyp( i,  (void *) &a_kun.fil );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "kun" )) 
			{
				ei ++ ; ladetyp( i,  (void *) &a_kun.kun );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a" )) 
			{
				ei ++ ; ladetyp( i,  (void *) &a_kun.a );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a_kun" )) 
			{
				ei ++ ; ladetyp( i,  (void *) a_kun.a_kun );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a_bz1" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.a_bz1 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "me_einh_kun" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.me_einh_kun );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "inh" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.inh );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "kun_bran2" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.kun_bran2 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "tara" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.tara );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "ean" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.ean );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a_bz2" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.a_bz2 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "hbk_ztr" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.hbk_ztr );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "kopf_text" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.kopf_text );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pr_rech_kz" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.pr_rech_kz );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "modif" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.modif );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "text_nr" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.text_nr );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "devise" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.devise );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "geb_eti" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.geb_eti );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "geb_fill" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.geb_fill );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "geb_anz" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.geb_anz );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pal_eti" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.pal_eti );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pal_fill" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.pal_fill );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pal_anz" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.pal_anz );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pos_eti" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.pos_eti );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "sg1" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.sg1 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "sg2" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.sg2 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "pos_fill" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.pos_fill );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "ausz_art" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.ausz_art );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "text_nr2" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.text_nr2 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "cab" )) 
			{
				ei ++ ; ladetyp( i, (void *) &a_kun.cab );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a_bz3" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.a_bz3 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "a_bz4" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.a_bz4 );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "li_a" )) 
			{
				ei ++ ; ladetyp( i, (void *) a_kun.li_a );
				continue ;
			};
// ####################
			if ( ! strcmp ( hilfech, "geb_fakt" )) 
			{
				if ( ext_typ > 0 )	// 250808 
				{
					a_kun.geb_fakt = red_inh ;
				}
				ei ++ ; ladetyp( i, (void *) &a_kun.geb_fakt );
				continue ;
			};
			if ( !ei )	// 160113
				break ;
		continue ;
		}
		if ( oi ) break ;	// Tabelle zu ende 
	}	// while 

	// 160113 : ret-code 
	if ( badfeld  || ( ! ei && oi ))
		return FALSE ;

	return TRUE;

}

long plznum ( char * eplz )
{
char outputchar[11];
char vorspann [11];        // generiere den Vorspann einfach mal mit
int inpoint;
int outpoint;
int vorpoint;
int inlen;
char char1;

     inlen = (int)strlen(clipped (eplz));
     if ( inlen > 9 )  // eigentlich sinnlose Sicherheit
         inlen = 9;
     inpoint = 0;
     outpoint = 0;
     vorpoint = 0;
     vorspann[0] = '\0';
     sprintf ( outputchar, "0");

     while ( inpoint < inlen )
	 {
         char1 = eplz[inpoint];
		 inpoint ++;
		 if ( char1 > '9' ||  char1 <'0' )
		 {
            if ( vorpoint < 9 )
			{
                vorspann[vorpoint] = char1;
                vorpoint++;
				vorspann[vorpoint]= '\0';
				
			}
			continue;
		}
		outputchar[outpoint] = char1;
        outpoint ++;
		outputchar[outpoint] = '\0';
        vorpoint = 10;	// Vorspann vorbei .....
	 }
     return (atol( outputchar ));
}

int NR_BEI_RECH(void)
{
	if (strlen ( clipped(kun.nr_bei_rech )) > 1 )
	{
		if ( strlen(strboes_extramgr) > 1 )
			sprintf( strboes_extramgr + strlen(strboes_extramgr) , "%c", LL_CHAR_NEWLINE );
		if ( strlen(strboes_extramgr)  <  200  )	// soviel Platz muss sein .......
				sprintf(  strboes_extramgr + strlen(strboes_extramgr) , "Lieferanten-Nummer : %s", kun.nr_bei_rech );

		memcpy ( &kun, &kun_saveh, sizeof(struct KUN));
		return 1 ;
	}
	memcpy ( &kun, &kun_saveh, sizeof(struct KUN));

	if ( strlen(strboes_extramgr) > 1 )
		return 1 ;

	return 0;
}

int extramgrnr(void)
{
	sprintf ( strboes_extramgr,"");	// initialisieren
	if ( ! spboes_par )
		return 0 ;
	// Tabelle kun ist bereits komplettiert !!!!
	// Die Felder LSK.TOU,LSK.TEIL_SMT und LSK.ADR muessen selektiert sein !!!
	
	adr.adr = get_lsk_adr();
	Adr.openadr() ; 
	if ( Adr.leseadr(0))	// immer lesen ODER leeren, plz lesen
	{	// eigentlich nur im Fall von Dateninkonsistenzen m�glich
		return 0;
	}

	tou.leitw_typ = 0;

	tou.tou = get_lsk_tou();
	if ( tou.tou > 0 )
	{
		Tou.opentou(); 
		if ( Tou.lesetou(0))
		{
			tou.tou = 0;
			tou.leitw_typ = 0;
		}
	}

	if ( tou.leitw_typ > 0 )
	{

		leitweg.plz_v = plznum ( clipped( adr.plz)) ;
		leitweg.leitw_typ = tou.leitw_typ;
		Leitweg.openleitweg();

		if ( ! Leitweg.leseleitweg())	// da wurde was gefunden 
		{
			if ( tou.leitw_typ > 0  )
			{
				sprintf ( ptabn.ptitem, "leitw_typ" );
				sprintf ( ptabn.ptwert,"%d",tou.leitw_typ);
				Ptabn.openptabn();
				if (Ptabn.leseptabn())
					sprintf (ptabn.ptwer2,"");

				if ( strlen ( clipped(ptabn.ptwer2)) > 0 )
					sprintf ( strboes_extramgr, "LEITWEG : %s %02d" , ptabn.ptwer2 ,leitweg.leitweg );
				else
					sprintf ( strboes_extramgr, "LEITWEG : %04d", leitweg.leitweg );
			}
		}
	}

	long hilfteil_smt = get_lsk_teil_smt();

	memcpy ( &kun_saveh, &kun, sizeof(struct KUN));
	if ( hilfteil_smt > 0 )
	{
        if ( kun.inka_nr > 0 )
		{
            if ( kun.inka_nr != get_lsk_kun())
/*  -----> �berladen 
               execute from sql
                  select
                   #kun.kun_bran2,#kun.sedas_dta,#kun.edi_typ,
                   #kun.sedas_nr2 from kun
                  where kun.kun = $kun.inka_nr and kun.mdn = $lsk.mdn
               end
< ----*/
				Kun.ueberladeinka();
		}
        memcpy(  &dta,&dta_null, sizeof( struct DTA));
		if ( kun.edi_typ[0] == 'E' )
		{
			dta.dta = kun.sedas_dta ;
			Dta.opendta();
			Dta.lesedta();
		}
        if (dta.typ != 5)
	            return NR_BEI_RECH();
	}
	else         // teil_smt == 0
		return NR_BEI_RECH();

   // hier kommt nur noch dta.typ == 5 an ...
   if ( spmetro1_par )
   {
        if (strlen ( clipped ( kun.sedas_nr2 )) > 3 )
		{

			if ( strlen(strboes_extramgr) > 1 )
				sprintf( strboes_extramgr + strlen(strboes_extramgr) , "%c", LL_CHAR_NEWLINE );
			if ( strlen(strboes_extramgr)  <  200  )	// soviel Platz muss sein .......
				sprintf(  strboes_extramgr + strlen(strboes_extramgr) , "MGE-Lieferanten-Nummer : %s", kun.sedas_nr2 );

			memcpy ( &kun, &kun_saveh, sizeof(struct KUN));
			return 1 ;	// TRUE ......
		}
		else
			return NR_BEI_RECH();
   }

   else
   {
// das ganze ist nur sinnvoll, wenn die Abkommensnummern,Inkassos usw.
// je Zentralregulierer konsistent sind, sonst kommt halt Quatsch heraus
// oder man muss einfach einen anderen dta definieren, der dann wieder
// in sich konsistent ist, jedoch eine andere Datei darstellt
// d.h entweder stecken alle Kunden in der gleichen tsmtgg ( z.B. per kun_bran2)
// oder es existieren identische tsmtgg (tsmt_gr + abkommen)


	if ( Tsmtgg.holemgenummer (kun.inka_nr, clipped (kun.kun_bran2),kun.mdn, hilfteil_smt))
		return NR_BEI_RECH();
	else
	{
		if (tsmtg.abkommen > 0  )
		{
			if ( strlen(strboes_extramgr) > 1 )
				sprintf( strboes_extramgr + strlen(strboes_extramgr) , "%c", LL_CHAR_NEWLINE );
			if (( strlen(strboes_extramgr))  <  200  )	// soviel Platz muss sein .......
			{
				sprintf(  strboes_extramgr + strlen(strboes_extramgr) , "MGE-Lieferanten-Nummer : %ld", tsmtg.abkommen );
			}
			memcpy ( &kun, &kun_saveh, sizeof(struct KUN));
			return 1 ;	// TRUE ......
		}
		else
			return NR_BEI_RECH();
 
	   }
   }
}

void TEMPTAB_CLASS::komplettiereSatz(void)
{
	int di ;
	kun.mdn = get_lsk_mdn() ;
	kun.kun = get_lsk_kun() ;

	aufk.auf = get_lsk_auf();	// 230914 

	verboteneFelder = 0 ;	// 160113
	sprintf ( cverboteneFelder,"" ) ;

	if ( aufk.auf == 0 )
	{
		memcpy ( &aufk, &aufk_null, sizeof ( struct AUFK));
	}
	else
	{	// es wird ja eigentlich sogar nur ein Beleg behandelt .............
		if ( aufk.auf == aufk_save.auf )
		{

			memcpy ( &aufk, &aufk_save, sizeof(struct AUFK));
		}
		else
		{
			aufk.mdn = kun.mdn ;
			aufk_save.auf = aufk.auf;	// schon immer mal sichern
			di = Aufk.openaufk() ;
			di = Aufk.leseaufk() ;
			if ( di )	//	return ;	// eigentlich-Fehler
			{
				memcpy ( &aufk,&aufk_null, sizeof(struct AUFK)) ;
				aufk.auf = aufk_save.auf;
			}
			memcpy ( &aufk_save,&aufk, sizeof(struct AUFK)); ;
		}
		if ( ! komplettiereaufk () )
		{
				verboteneFelder ++ ;	
				sprintf ( cverboteneFelder,"Tabelle aufk mit verbotenen Feldern designet");
				MessageBox(NULL, "Tabelle aufk mit verbotenen Feldern designet", "drllls.exe", MB_OK|MB_ICONSTOP);
		}



	}

// 230914 E

	if ( ! komplettiereaufpt_verkfrage (get_lsk_mdn(), get_lsk_ls(),a_bas_a ()) )
	{
			verboteneFelder ++ ;	
			sprintf ( cverboteneFelder,"Tabelle aufpt_verkfrage mit verbotenen Feldern designet");
			MessageBox(NULL, "Tabelle aufpt_verkfrage mit verbotenen Feldern designet", "drllls.exe", MB_OK|MB_ICONSTOP);
	}


	if ( (! strcmp( tmyform_nr, "53101" ))|| get_lsk_kun_fil())
	{
		intboes = 0;	// nur fuer Kunden-LS 171114
		fil.mdn = kun.mdn ;
		fil.fil = (short) kun.kun ;
		if ( fil.fil == fil_save.fil )
		{
			memcpy ( &fil, &fil_save, sizeof(struct FIL));
		}
		else
		{
			di = Fil.openfil() ;
			di = Fil.lesefil (0) ;
			if ( di )	return ;	// Filialstamm-Fehler
			memcpy ( &fil_save,&fil, sizeof(struct FIL)); ;
		}
		if ( ! komplettierefil () )
		{
			//160113
				verboteneFelder ++ ;	
				sprintf ( cverboteneFelder,"Tabelle fil mit verbotenen Feldern designet");
				MessageBox(NULL, "Tabelle fil mit verbotenen Feldern designet", "drllls.exe", MB_OK|MB_ICONSTOP);
		}
	}
	else
	{
		if ( kun.kun == kun_save.kun )
		{
			memcpy ( &kun, &kun_save, sizeof(struct KUN)); 

		}
		else
		{
			di = Kun.openkun() ;
			di = Kun.lesekun (0) ;
			if ( di )	return ;	// Kundenstamm-Fehler
			memcpy ( &kun_save,&kun, sizeof(struct KUN)); ;

			if ( spboes_par )	// 171114 und nur einmalig am Start des Belegs
			{
					intboes = extramgrnr();
			}
			else
					intboes = 0;

		}
		if ( ! komplettierekun () )
		{
						//160113
				verboteneFelder ++ ;	
				sprintf ( cverboteneFelder,"Tabelle kun mit verbotenen Feldern designet");
				MessageBox(NULL,"Tabelle kun mit verbotenen Feldern designet", "drllls.exe", MB_OK|MB_ICONSTOP);
		}

		a_kun.mdn = kun.mdn ;
		a_kun.a = a_bas_a ();				// get_lsp_a() ;
		a_kun.kun = kun.kun ;
		sprintf ( a_kun.kun_bran2 , "0" );
		di = A_Kun.opena_kun() ;
		if ( A_Kun.lesea_kun(0))
		{
			sprintf ( a_kun.kun_bran2 , kun.kun_bran2 );
			a_kun.kun = 0L ;
			di = A_Kun.opena_kun() ;
			di = A_Kun.lesea_kun(0) ;
		}
//		if ( !di ) komplettierea_kun() ;	230506

		bool wasistlos ;	// 160113
		if (redeckerpar )
			wasistlos = komplettierea_kun( di , a_bas_a() , a_bas_a_typ() ) ;
		else	// 250808 : so war es bisher 
			wasistlos = komplettierea_kun( di , 0 , 0 ) ;


		if ( ! wasistlos )
		{
			verboteneFelder ++ ;
			sprintf ( cverboteneFelder, "Tabelle a_kun mit verbotenen Feldern designet" ) ;
			MessageBox(NULL, "Tabelle a_kun mit verbotenen Feldern designet", "drllls.exe", MB_OK|MB_ICONSTOP);
		}

		if ( sonschill2 )	// 230609 : hier wird eventuell a_kun.geb_anz mit a_kun_gx.geb_anz �berladen 
		{
			a_kun_gx.mdn = kun.mdn ;
			a_kun_gx.a = a_bas_a ();				// get_lsp_a() ;
			a_kun_gx.kun = kun.kun ;
			sprintf ( a_kun_gx.kun_bran2 , "0" );
			di = A_Kun_Gx.opena_kun_gx() ;
			if ( A_Kun_Gx.lesea_kun_gx())
			{
				sprintf ( a_kun_gx.kun_bran2 , kun.kun_bran2 );
				a_kun_gx.kun = 0L ;
				di = A_Kun_Gx.opena_kun_gx() ;
				di = A_Kun_Gx.lesea_kun_gx() ;
			}
			if ( ! komplettierea_kun_gx(di) )
			{
					verboteneFelder ++ ;
					sprintf ( cverboteneFelder, "Tabelle a_kun_gx mit verbotenen Feldern designet" ) ;
					MessageBox(NULL, "Tabelle a_kun_gx mit verbotenen Feldern designet", "drllls.exe", MB_OK|MB_ICONSTOP);
			}
		}

	}
}

int TEMPTAB_CLASS::lesetemptab (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;   
	  }

		inittemptab ();	//121104	- 310506 : reaktiviert
		int di = dbClass.sqlfetch (readcursor);
		if ( di )
			reloadtemptab () ;	// 310506
		if ( !di )
			komplettiereSatz();
		return di;
}

int TEMPTAB_CLASS::opentemptab (void)
{

	kun.kun = 0;
	a_kun.a = 0.0L ;
	a_zus_txt.a = 0.0L ;

	if ( readcursor < 0 )
	{
	}
	else
	{
		dbClass.sqlclose(readcursor) ;
		if ( del_cursor > -1 )	dbClass.sqlclose(del_cursor) ;
	}
	prepare () ;
	if ( v_abbruch ) return ( -3) ;

	if ( del_cursor > -1 )
			dbClass.sqlopen(del_cursor) ;

	// 310506 : wieder weg inittemptab() ;	// Nur einmalig 220506 

    return dbClass.sqlopen (readcursor);
}


// Schreiben der Werte like rangedialog
void schreiberangesausdatei(void)
{
int retco ;
int l = 0 ;
char token[125],wert1[125],wert2[125] ;
	for ( int i = 0 ; tt_tabnam[i][0] != '\0'; i++)
	{
		if ( ! tt_range[i]) continue ;

		sprintf ( token, "%s",tt_feldnam[i]);
		int j = holerangesausdatei( token, wert1,  wert2);

		l ++ ;
		tt_range[i] = l ;

		retco = macherange( i, wert1, wert2 ) ;

	}
}

// hier komplette Struktur UND Cursor neu aufbauen
void TEMPTAB_CLASS::prepare (void)
{

int i,j,k ;
int hauptrunde ;
char statementpuffer[4101];	// informix kann wohl im Standard 4096 
char tabellenpuffer[1025];	// sollte fuer > 50 Tabellennamen reichen
char grupptab[20] ;	
char wherefeld[15] ;
char whereklausel[1005] ;
char groupklausel[1005] ;
char orderklausel[1005] ;
char klausel[1005] ;

// Alles nochmals fuer Nebencursor fuer Definition der Variablen

char nstatementpuffer[4101];	// informix kann wohl im Standard 4096 
char ntabellenpuffer[1025];	// sollte fuer > 50 Tabellennamen reichen

extern ITEM_CLASS Item ;
Item.openitem();

// falls Feldueberlauf kann es schon mal knallen ..
for (i = 0 ; i < MAXANZ ; i++)	// 191006 
{
	tt_sqlval[i]	= ( char * )0 ;
	savtt_sqlval[i] = ( char * )0 ;
}
hauptrunde = HAUPT ;	// z.T. bissel redundant, einfach 2 Runden und dadurch 2 Cursoren 
 while ( TRUE )
 {

	i=j=k= 0 ;


	// memset ( statementpuffer, '\0', size_t(statementpuffer));

	sprintf ( wherefeld, " where ");	// Anfangsinit
	sprintf ( statementpuffer, " select ");
	sprintf ( nstatementpuffer, " select ");

	tabellenpuffer [0] = '\0' ;
	ntabellenpuffer [0] = '\0' ;
	if (  tt_hauneb[0] == HAUPT )
	{
		if ( ! tt_tabout[0] )
			sprintf ( tabellenpuffer, "%s", clipped ( tt_tabnam[0] ));
		else
			sprintf ( tabellenpuffer, " outer %s", clipped ( tt_tabnam[0] ));

	}
	else
	{
		if ( ! tt_tabout[0] )
			sprintf ( ntabellenpuffer, "%s", clipped ( tt_tabnam[0] ));
		else
//			sprintf ( ntabellenpuffer, " outer %s", clipped ( tt_tabnam[0] ));
// OUTER ist hier NICHT RELEVANT
			sprintf ( ntabellenpuffer, " %s", clipped ( tt_tabnam[0] ));

	}
	sprintf ( grupptab, "%s", clipped ( tt_tabnam[0] ));

	whereklausel[0] = '\0' ;
	groupklausel[0] = '\0' ;
	orderklausel[0] = '\0' ;


	sprintf ( form_w.form_nr,"%s" , nform_feld.form_nr );
	sprintf ( form_o.form_nr,"%s" , nform_feld.form_nr );

	form_w.lila = GlobalLila ;
	form_o.lila = GlobalLila ;

	FORM_O_CLASS Form_o_class ;
	Form_o_class.openform_o ();

	FORM_W_CLASS Form_w_class ;
	Form_w_class.openform_w ();


	while( TRUE)
	{
		if ( tt_tabnam[i][0] == '\0' ) break ;


		if (  tt_hauneb[i] == HAUPT )
		{

			j = (int) strlen( statementpuffer ) ;
			if ( j < 10 )	// erstes element
				sprintf ( statementpuffer + j, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);
			else
				sprintf ( statementpuffer + j, ",%s.%s", tt_tabnam[i], tt_feldnam[i]);
		}
		else
		{
			j = (int) strlen( nstatementpuffer ) ;
			if ( j < 10 )	// erstes element
				sprintf ( nstatementpuffer + j, "%s.%s" , tt_tabnam[i], tt_feldnam[i]);
			else
				sprintf ( nstatementpuffer + j, ",%s.%s", tt_tabnam[i], tt_feldnam[i]);
		}

// In der Hauptrunde Hauptcursor zuweisen, in der Nebenrunde Nebencursor zuweisen ...
		if (  (tt_hauneb[i] == HAUPT && hauptrunde == HAUPT)  
			|| (tt_hauneb[i] == NEBEN && hauptrunde == NEBEN ))
		{

			switch ( tt_datetyp[i] )
			{
				case(iDBCHAR):

					if (tt_sqlval[i] != (char *) 0)
					{
						free ( tt_sqlval[i]) ;
						tt_sqlval[i] = (char *) 0;
					}
					if (tt_sqlval[i] == (char *) 0) tt_sqlval[i] = (char *) malloc (tt_datelen[i]+2);
// 310506 
					if (savtt_sqlval[i] != (char *) 0)
					{
						free ( savtt_sqlval[i]) ;
						savtt_sqlval[i] = (char *) 0;
					}
					if (savtt_sqlval[i] == (char *) 0) savtt_sqlval[i] = (char *) malloc (tt_datelen[i]+2);
					sprintf ( (char*) tt_sqlval[i] ,"" ) ; sprintf ( (char*) savtt_sqlval[i],"") ;	// 191006
					dbClass.sqlout ((char *)  tt_sqlval[i], SQLCHAR , tt_datelen[i]+1 );
					break ;
				case(iDBSMALLINT):	// smallint
//					dbClass.sqlout ((short *)  &tt_sqllong[i], SQLSHORT, 0);
					dbClass.sqlout ((long *)  &tt_sqllong[i], SQLLONG, 0);
					break ;
				case(iDBINTEGER):	// integer
					dbClass.sqlout ((long *)  &tt_sqllong[i], SQLLONG, 0);
					break ;
				case(iDBDECIMAL):	// decimal
					dbClass.sqlout ((double *)  &tt_sqldouble[i], SQLDOUBLE, 0);
					break ;
				case(iDBDATUM):	// Datumsfeld
					if (tt_sqlval[i] != (char *) 0)
					{
						free ( tt_sqlval[i]) ;
						tt_sqlval[i] = (char *) 0;
					}
					if (tt_sqlval[i] == (char *) 0) tt_sqlval[i] = (char *) malloc (12);
// 310506 
					if (savtt_sqlval[i] != (char *) 0)
					{
						free ( savtt_sqlval[i]) ;
						savtt_sqlval[i] = (char *) 0;
					}
					if (savtt_sqlval[i] == (char *) 0) savtt_sqlval[i] = (char *) malloc (12);
					sprintf ( (char*) tt_sqlval[i] ,"" ) ; sprintf ( (char*) savtt_sqlval[i],"") ;	// 191006
					dbClass.sqlout ((char *)  tt_sqlval[i], SQLCHAR , 11 );
					break ;
				default :
					break ;

			} ;	// switch
		}
		if ( tt_range[i] )	k++ ;	// Darf nur im Hauptcursor sein ....

		if (( ! strncmp ( grupptab , tt_tabnam[i], strlen( grupptab)))
				&& strlen ( grupptab)== strlen(tt_tabnam[i] ))
		{
		}
		else
		{
			if ( tt_hauneb[i] == HAUPT )
			{
				j = (int) strlen( tabellenpuffer );
				if ( j > 1 )	// Folge oder erst-Eintrag
				{
					if ( ! tt_tabout[i] )
						sprintf ( tabellenpuffer + j,",%s" , tt_tabnam[i]);
					else
						sprintf ( tabellenpuffer + j,", outer %s" , tt_tabnam[i]);
				}
				else
				{
					if ( ! tt_tabout[i] )
						sprintf ( tabellenpuffer + j,"%s" , tt_tabnam[i]);
					else
						sprintf ( tabellenpuffer + j," outer %s" , tt_tabnam[i]);
				}
			}
			else
			{
				j = (int) strlen( ntabellenpuffer );
				if ( j > 1 )	// Folge oder erst-Eintrag
				{
					if ( ! tt_tabout[i] )
						sprintf ( ntabellenpuffer + j,",%s" , tt_tabnam[i]);
					else
	//					sprintf ( ntabellenpuffer + j,", outer %s" , tt_tabnam[i]);
	// OUTER HIER NICHT RELEVANT
						sprintf ( ntabellenpuffer + j,", %s" , tt_tabnam[i]);
				}
				else
				{
					if ( ! tt_tabout[i] )
						sprintf ( ntabellenpuffer + j,"%s" , tt_tabnam[i]);
					else
						sprintf ( ntabellenpuffer + j," outer %s" , tt_tabnam[i]);
				}
			}
			sprintf ( grupptab , "%s" , tt_tabnam[i]);

		}
		i ++ ;
	}		// while	

	if ( i )
	{

		if ( k  && ( hauptrunde == HAUPT ) )
		{
			Crange rangedial;

			if (! datenausdatei )
			{
				rangedial.DoModal();

				v_abbruch = rangedial.getabbruch();
				v_anzahl = rangedial.getanzahl();
			}
			else
			{
				v_abbruch = 0 ;
				schreiberangesausdatei() ;
			}

			for ( k= 0 ; tt_tabnam[k][0] != '\0' ;k ++ )
			{
				if ( ! tt_range[k] )
					continue ;
				if (  tt_hauneb[k] == NEBEN )
					continue ;	// Kann nur fehlerhaftes Design des Formulars sein

				j = (int) strlen( whereklausel ) ;

				switch ( tt_datetyp[k] )
				{
				case(iDBCHAR):
// nothing, nur 1 String, 2 Verschiedene Strings
					if (( tt_cvrange[tt_range[k]][0] == -1 ) && 
						( tt_cbrange[tt_range[k]][0] == -1 ) )
						break ;
					else
					{
						if ( tt_cbrange[tt_range[k]][0] == -1 )
						{
							dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,
							(int)strlen ( clipped(tt_cvrange[tt_range[k]]))+1 );
							sprintf ( whereklausel + j, " %s %s.%s  matches ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
							sprintf ( wherefeld, " and") ;
							break ;
						}
// default-Zweig
						dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,
							(int)strlen ( clipped(tt_cvrange[tt_range[k]]))+1 );
						dbClass.sqlin ((char *)  tt_cbrange[tt_range[k]], SQLCHAR ,
							(int)strlen ( clipped(tt_cbrange[tt_range[k]]))+1 );
						sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
				
						break ;
					}
					break ;				
				case(iDBSMALLINT):	// smallint
					if (( tt_lvrange[tt_range[k]] ==   11 ) && 
						( tt_lbrange[tt_range[k]] ==  -11 ) )	// keine Eingabe
						break ;
					else
					{
						if ( tt_lvrange[tt_range[k]] ==   tt_lbrange[tt_range[k]])
						{
							dbClass.sqlin ((short *) &tt_lvrange[tt_range[k]], SQLSHORT ,0);
							sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
							sprintf ( wherefeld, " and") ;
							break ;
						}
// default-Zweig
						dbClass.sqlin ((short *) &tt_lvrange[tt_range[k]], SQLSHORT, 0);
						dbClass.sqlin ((short *) &tt_lbrange[tt_range[k]], SQLSHORT, 0);
						sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
					break ;

				case(iDBINTEGER):	// integer
					if (( tt_lvrange[tt_range[k]] ==   11 ) && 
					( tt_lbrange[tt_range[k]] ==  -11 ) )	// keine Eingabe
						break ;
					else
					{
						if ( tt_lvrange[tt_range[k]] ==   tt_lbrange[tt_range[k]])
						{
							dbClass.sqlin ((long *) &tt_lvrange[tt_range[k]], SQLLONG ,0);
							sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
							sprintf ( wherefeld, " and") ;
							break ;
						}
// default-Zweig
						dbClass.sqlin ((long *) &tt_lvrange[tt_range[k]], SQLLONG, 0);
						dbClass.sqlin ((long *) &tt_lbrange[tt_range[k]], SQLLONG, 0);
						sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
					break ;

				case(iDBDECIMAL):	// decimal
					if (( tt_dvrange[tt_range[k]] ==   11 ) && 
						( tt_dbrange[tt_range[k]] ==  -11 ) )	// keine Eingabe
						break ;
					else
					{
						if ( tt_dvrange[tt_range[k]] ==  tt_dbrange[tt_range[k]])
						{
							dbClass.sqlin ((double *)  &tt_dvrange[tt_range[k]], SQLDOUBLE ,0);
							sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
							sprintf ( wherefeld, " and") ;
							break ;
						}
// default-Zweig
						dbClass.sqlin ((double *)  &tt_dvrange[tt_range[k]], SQLDOUBLE, 0);
						dbClass.sqlin ((double *)  &tt_dbrange[tt_range[k]], SQLDOUBLE, 0);
						sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
						break ;
					}
					break ;
				case(iDBDATUM):	// Datumsfeld
// nothing, nur 1 String, 2 Verschiedene Strings
					if (( tt_cvrange[tt_range[k]][0] == -1 ) && 
						( tt_cbrange[tt_range[k]][0] == -1 ) )
						break ;
					else
					{
						if ( tt_cbrange[tt_range[k]][0] == -1 )
						{
							dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,11);
							sprintf ( whereklausel + j, " %s %s.%s  = ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
							sprintf ( wherefeld, " and") ;
							break ;
						}
// default-Zweig
						dbClass.sqlin ((char *)  tt_cvrange[tt_range[k]], SQLCHAR ,11);
						dbClass.sqlin ((char *)  tt_cbrange[tt_range[k]], SQLCHAR ,11);
						sprintf ( whereklausel + j, " %s %s.%s  between ? and ? ",
										wherefeld , tt_tabnam[k], tt_feldnam[k]);
						sprintf ( wherefeld, " and") ;
				
						break ;
					}
					break ;				

				default :
					break ;

				}		// 	switch ( tt_datetyp[k] ) 
			}			// 	for ( k= 0 ; tt_tabnam[k][0] != '\0' ;k ++ )
		}				// if ( k  &&( hauptrunde == HAUPT )) -> das war ein range


// ##### WHERE - KLAUSEL

		sprintf ( klausel, " " );

		if (! Form_w_class.leseform_w())
		{
			sprintf ( klausel , "%s", clipped (form_w.krz_txt)) ;
			while (!Form_w_class.leseform_w())
			{
				j = (int)strlen ( clipped( klausel)) ;
				sprintf ( klausel + j , "%s" , clipped ( form_w.krz_txt));
			}
		}


		if ( wherefeld[1] == 'w' )	// noch nix reingeschrieben ausser " where "
		{
			if ( strlen ( clipped ( klausel)) > 2 )
				sprintf ( whereklausel , " where %s", clipped (klausel) );
			else
				sprintf ( whereklausel , " " );
		}
		else
		{
			if ( strlen ( clipped ( klausel)) > 2 )
			{
				j = (int)strlen ( clipped (whereklausel ));
				sprintf ( whereklausel + j , " and %s", clipped (klausel));
			}

		}


// ##### ORDER - KLAUSEL

		sprintf ( klausel, " " );

		if (! Form_o_class.leseform_o())
		{
			sprintf ( klausel , "%s", clipped (form_o.krz_txt)) ;
			while (!Form_o_class.leseform_o())
			{
				j = (int)strlen ( clipped( klausel)) ;
				sprintf ( klausel + j , "%s" , clipped ( form_o.krz_txt));
			}
		}

		if ( strlen ( clipped ( klausel)) > 1 )
			sprintf ( orderklausel , " order by %s", clipped (klausel) );
		else
			sprintf ( orderklausel , " " );
	

		j = (int) strlen ( statementpuffer ) ;
		sprintf ( statementpuffer + j, " from %s", tabellenpuffer );

		j = (int) strlen ( statementpuffer ) ;
		sprintf ( statementpuffer + j, " %s %s %s"
				,clipped ( whereklausel )
				,clipped ( groupklausel )
				,clipped ( orderklausel ) );
	}	// if i ( daher nur, wenn was da ist ....

	if ( hauptrunde == HAUPT )
	{

// 130910 : falls Testmode aktiv, dann das statement auf Platte ablegen 
	char * penvchar = getenv ("testmode");
	if ( penvchar == NULL )	penvchar = getenv ( "TESTMODE" ) ;
	if ( penvchar == NULL )
	{}
	else
	{ int testwert = atoi ( penvchar) ;
	  if ( testwert > 0 )
	  {
		  char dateiname[ 256 ] ;
		  penvchar = getenv ( "TMPPATH" ) ;
		  if ( penvchar == NULL )
			  sprintf ( dateiname , "lldrstatement.sql" ) ;
		  else
			  sprintf ( dateiname ,"%s\\lldrstatement.sql", penvchar) ;

		FILE * fp_struct ;
		fp_struct = fopen ( dateiname, "ab" ) ;
			if ( fp_struct == NULL )
			{
				// es hat halt NICHT funktioniert
			}

			int nwritten= (int) fwrite(statementpuffer, (int) strlen(statementpuffer), 1, fp_struct );
			fclose ( fp_struct ); 

	  }
	}
// 130910 : falls Testmode aktiv, dann das statement auf Platte ablegen 


	readcursor = dbClass.sqlcursor ( statementpuffer );
	}
	if ( i && ( hauptrunde == NEBEN ))
	{
		j = (int) strlen ( nstatementpuffer ) ;
		if ( j )
		{
			sprintf ( nstatementpuffer + j, " from %s", ntabellenpuffer );

/// 130910 : falls Testmode aktiv, dann das statement auf Platte ablegen 
			char * penvchar = getenv ("testmode");
			if ( penvchar == NULL )	penvchar = getenv ( "TESTMODE" ) ;
			if ( penvchar == NULL )
			{}
			else
			{	int testwert = atoi ( penvchar) ;
				if ( testwert > 0 )
				{
					char dateiname[ 256 ] ;
					penvchar = getenv ( "TMPPATH" ) ;
					if ( penvchar == NULL )
					sprintf ( dateiname , "lldrstatement.sql" ) ;
				else
					sprintf ( dateiname ,"%s\\lldrstatement.sql", penvchar) ;

				FILE * fp_struct ;
				fp_struct = fopen ( dateiname, "ab" ) ;
				if ( fp_struct == NULL )
				{
					// es hat halt NICHT funktioniert
				}

				int nwritten = (int) fwrite(nstatementpuffer, (int) strlen(nstatementpuffer), 1, fp_struct );
				fclose ( fp_struct ); 
			}
		}
// 130910 : falls Testmode aktiv, dann das statement auf Platte ablegen 



			del_cursor = dbClass.sqlcursor ( nstatementpuffer );
			//del_cursor zweckentfremdet nutzen ..... 
										
		}
	}
	if ( hauptrunde == NEBEN ) break ; // fertig
	if ( hauptrunde == HAUPT ) hauptrunde = NEBEN ;	// jetzt kommt die Nebenrunde
 }		// hauptrunden-while
}

short TEMPTAB_CLASS::kun_leer_kz(void)
{
	if ( ! strcmp( tmyform_nr , "53101")) return 1 ;	// Filiale immer kleine Tabelle 
	return kun.kun_leer_kz ;
}

void TEMPTAB_CLASS::drfree (void) 
{

	for (int i = 0 ; i < MAXANZ; i++ )
	{
		if (tt_sqlval[i] != (char *) 0)
		{
			free ( tt_sqlval[i]) ;
					tt_sqlval[i] = (char *) 0;
		} ;
// 310506 
		if (savtt_sqlval[i] != (char *) 0)
		{
			free ( savtt_sqlval[i]) ;
					savtt_sqlval[i] = (char *) 0;
		}
	}
}