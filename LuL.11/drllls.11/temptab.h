#ifndef _TEMPTAB_DEF
#define _TEMPTAB_DEF

#define MAXANZ 250
#define MAXRANGE 20

// Mehr als 250 felder sollten nur sehr selten vorkommen 
extern  char  tt_tabnam [MAXANZ][19];
extern  char  tt_feldnam[MAXANZ][19]; 	
extern  int    tt_datetyp[MAXANZ];	// Datentyp in der Daba
extern  int    tt_feldtyp[MAXANZ];	// Datentyp fuer Generator
extern  int    tt_referenz  [MAXANZ];	// nur Pointer auf referenz-Liste
extern  int    tt_range     [MAXANZ];	// ob Feld fuer range herangezogen wird
extern  void * tt_sqlval [MAXANZ];	// pointer auf Feldinhalte 
extern long  tt_sqllong    [MAXANZ];	// Matrix fuer longs und shorts 
extern double tt_sqldouble [MAXANZ];	// Matrix fuer double 

 // mehr als 50 verschiedene Textbausteine sollten sehr selten sein
 extern char  tt_creferenz[50][25];


extern char  tt_cvrange[MAXRANGE][50];	// maximale stringlaenge  49
extern char  tt_cbrange[MAXRANGE][50];
extern long  tt_lvrange[MAXRANGE];
extern long  tt_lbrange[MAXRANGE];
extern double  tt_dvrange[MAXRANGE];
extern double  tt_dbrange[MAXRANGE];
extern char  tt_qrange[MAXRANGE][100];	// Maximale Query-Laenge 99


extern struct TEMPTAB temptab, temptab_null;


class TEMPTAB_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
			   void komplettiereSatz(void);
       public :
               int structprep (char *);
               int lesetemptab (int);
               void inittemptab (void);
               void reloadtemptab (void);	// 310506
               int opentemptab (void);
//			   double get_lsp_a(void);

			   double a_bas_a(void) ;
			   short a_bas_a_typ(void) ;
			   char * a_bas_a_bz1(void) ;
			   double lsp_lief_me(void) ;
			   double a_bas_a_gew(void) ;	// 230609
			   double lsp_ls_vk_euro(void) ;


			   long lsp_ls_ident(void) ;		// 091209
			   long a_bas_charg_hand(void) ;	// 091209



			   short get_lsk_mdn(void) ;		// 110205
			   short get_lsk_fil(void) ;		// 110205
			   long get_lsk_ls(void) ;			// 110205
			   long get_lsp_posi(void) ;		// 110205

			   int getanzahl (void);
			   void drfree(void);
			   short kun_leer_kz(void);	// 160305
               TEMPTAB_CLASS () : DB_CLASS ()
               {
               }
};
#endif

