#include "stdafx.h"
#include "DbClass.h"
#include "systables.h"
#ifdef LL19
#include "cmbtll19.h"
#else
#ifdef LL12
#include "cmbtll12.h"
#else
#include "cmbtll11.h"
#endif
#endif


struct SYSTABLES systables, systables_null;
extern DB_CLASS dbClass;
static long anzzfelder = -1;
struct NFORM_TAB nform_tab, nform_tab_null;

int nformtab_curs , tabid_curs ;

int SYSTABLES_CLASS::lesesystables ()
{

      int di = dbClass.sqlfetch (tabid_curs);

	  return di;
}

int NFORM_TAB_CLASS::lesenformtab (void)
{
	/* 
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausegeben werden mu�
	  }
	  < ----- */

      int di = dbClass.sqlfetch (nformtab_curs);

	  return di;
}

int SYSTABLES_CLASS::opensystables (void)
{
        
 if ( tabid_curs < 0 ) prepare ();
//	prepare ();
	return dbClass.sqlopen (tabid_curs);
}

int NFORM_TAB_CLASS::opennformtab (void)
{
   
 if ( nformtab_curs < 0 ) prepare ();
//	prepare () ;
	return dbClass.sqlopen (nformtab_curs);
}

void SYSTABLES_CLASS::prepare (void)
{


		dbClass.sqlin ((char *) systables.tabname, SQLCHAR, 19);

		dbClass.sqlout ((long *)  &systables.tabid, SQLLONG, 0);

		tabid_curs = dbClass.sqlcursor ("select tabid from systables where tabname = ?");

		test_upd_cursor = 1;



}

void NFORM_TAB_CLASS::prepare (void)
{

// formtab_curs : Welche tabelle fuer diese Format existieren
		dbClass.sqlin ((char *)   nform_tab.form_nr,   SQLCHAR,  11);
		dbClass.sqlin ((short *) &nform_tab.lila, SQLSHORT,0);

		dbClass.sqlout ((short *) &nform_tab.delstatus, SQLSHORT,0);
		dbClass.sqlout ((char *) nform_tab.tab_nam, SQLCHAR,19);
 
		nformtab_curs = dbClass.sqlcursor ("select delstatus,tab_nam from nform_tab"
			   " where form_nr = ? and lila = ? order by tab_nam");




}