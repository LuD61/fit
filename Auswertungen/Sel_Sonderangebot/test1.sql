select
case when a_mo_wa.fil = 0 then a_mo_wa.kun
    when a_mo_wa.fil > 0 then a_mo_wa.fil end kunde,

case when a_mo_wa.fil = 0 then 0
    when a_mo_wa.fil > 0 then 1 end kun_fil,

a_mo_wa.jr, a_mo_wa.mo,
sum(case when a_bas.me_einh <> 2 then a_mo_wa.me_vk_mo * a_bas.a_gew
         when a_bas.me_einh = 2  then a_mo_wa.me_vk_mo  end ) menge,


sum(a_mo_wa.ums_vk_mo)
from a_mo_wa,a_bas, outer kun, outer fil where a_mo_wa.a = a_bas.a
and a_mo_wa.mdn = 1 and a_mo_wa.fil = 0
and a_mo_wa.jr between 2003 and 2003
and a_mo_wa.mo between 1 and 1
 and a_mo_wa.me_vk_mo <> 0.0
and a_mo_wa.mdn = kun.mdn
and a_mo_wa.kun = kun.kun
and a_mo_wa.mdn = fil.mdn
and a_mo_wa.fil = fil.fil

group by 1,2,3,4 order by kun_fil,kunde
