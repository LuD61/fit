/**** C/C++ constants and function definitions for L9.DLL ****/
/****  (c) 1991,..,1999,2000,01,02,03... combit GmbH, Konstanz, Germany  ****/
/****  [build of 2003-03-12 15:03:49] ****/

#ifndef _L9_H /* include header only once */
#define _L9_H

#if !defined(WIN32)
  #define WIN32 1
#endif


#ifndef EXPLICIT_TYPES
  #define EXPLICIT_TYPES
  #ifndef INT
    typedef int INT; /* you should comment this out if you have any problems with INT */
  #endif
  #ifndef CHAR
    typedef char CHAR; /* you should comment this out if you have any problems with CHAR */
  #endif
  typedef unsigned char UINT8;
  typedef unsigned short UINT16;
  typedef signed char INT8;
  typedef signed short INT16;
  #ifndef _BASETSD_H_ /* MSVC6 defines these itself in basetsd.h */
    typedef unsigned long UINT32;
    typedef signed long INT32;
  #endif
#endif

#if defined(WIN32) && (!defined(__BORLANDC__) || __BORLANDC__ >= 0x450) && !defined(RC_INVOKED)
 #include <tchar.h>    /* for wide char support - comment this out if you have no 'tchar.h' */
#endif

#ifndef DLLPROC
  #define DLLPROC WINAPI
#endif
#ifndef DLLCPROC
  #ifdef WIN32
    #define DLLCPROC WINAPIV
   #else
    #define DLLCPROC _far _cdecl
  #endif
#endif

#if !defined(_L9_) && defined(WIN32)
  #define WINL9API DECLSPEC_IMPORT
#else
  #define WINL9API
#endif

#ifndef CMBTLANG_DEFAULT
 #define CMBTLANG_DEFAULT    -1
 #define CMBTLANG_GERMAN      0
 #define CMBTLANG_ENGLISH     1
 #define CMBTLANG_ARABIC      2
 #define CMBTLANG_AFRIKAANS   3
 #define CMBTLANG_ALBANIAN    4
 #define CMBTLANG_BASQUE      5
 #define CMBTLANG_BULGARIAN   6
 #define CMBTLANG_BYELORUSSIAN 7
 #define CMBTLANG_CATALAN     8
 #define CMBTLANG_CHINESE     9
 #define CMBTLANG_CROATIAN    10
 #define CMBTLANG_CZECH       11
 #define CMBTLANG_DANISH      12
 #define CMBTLANG_DUTCH       13
 #define CMBTLANG_ESTONIAN    14
 #define CMBTLANG_FAEROESE    15
 #define CMBTLANG_FARSI       16
 #define CMBTLANG_FINNISH     17
 #define CMBTLANG_FRENCH      18
 #define CMBTLANG_GREEK       19
 #define CMBTLANG_HEBREW      20
 #define CMBTLANG_HUNGARIAN   21
 #define CMBTLANG_ICELANDIC   22
 #define CMBTLANG_INDONESIAN  23
 #define CMBTLANG_ITALIAN     24
 #define CMBTLANG_JAPANESE    25
 #define CMBTLANG_KOREAN      26
 #define CMBTLANG_LATVIAN     27
 #define CMBTLANG_LITHUANIAN  28
 #define CMBTLANG_NORWEGIAN   29
 #define CMBTLANG_POLISH      30
 #define CMBTLANG_PORTUGUESE  31
 #define CMBTLANG_ROMANIAN    32
 #define CMBTLANG_RUSSIAN     33
 #define CMBTLANG_SLOVAK      34
 #define CMBTLANG_SLOVENIAN   35
 #define CMBTLANG_SERBIAN     36
 #define CMBTLANG_SPANISH     37
 #define CMBTLANG_SWEDISH     38
 #define CMBTLANG_THAI        39
 #define CMBTLANG_TURKISH     40
 #define CMBTLANG_UKRAINIAN   41
 #define CMBTLANG_CHINESE_TRADITIONAL   48
#endif

/*--- type declarations ---*/

#ifndef HJOB
  #define HJOB                           INT
#endif

#ifndef HLLJOB
  #define HLLJOB                         INT
#endif

#ifndef HLLEXPR
  #define HLLEXPR                        LPVOID
#endif

#ifndef HLLINTERF
  #define HLLINTERF                      LPVOID
#endif

#ifndef HSTG
  #define HSTG                           UINT32
#endif

#ifndef HLLSTG
  #define HLLSTG                         UINT32
#endif

#ifndef HLLRTFOBJ
  #define HLLRTFOBJ                      UINT32
#endif

#ifndef _PRECT
  #define _PRECT                         RECT FAR *
#endif

#ifndef _PCRECT
  #define _PCRECT                        const RECT FAR *
#endif

#ifndef HLISTPOS
  #define HLISTPOS                       UINT32
#endif

#ifndef _LPHANDLE
  #define _LPHANDLE                      HANDLE FAR *
#endif

#ifndef _LPINT32
  #define _LPINT32                       INT32 FAR *
#endif

#ifndef _LCID
  #define _LCID                          UINT32
#endif

#ifndef PHGLOBAL
  #define PHGLOBAL                       HGLOBAL FAR *
#endif

#ifndef LLPUINT
  #define LLPUINT                        UINT FAR *
#endif

#ifndef _PDEVMODE
  #define _PDEVMODE                      DEVMODE FAR *
#endif

#ifndef _PDEVMODEA
  #define _PDEVMODEA                     DEVMODEA FAR *
#endif

#ifndef _PDEVMODEW
  #define _PDEVMODEW                     DEVMODEW FAR *
#endif

#ifndef _PCDEVMODE
  #define _PCDEVMODE                     const DEVMODE FAR *
#endif

#ifndef _PCDEVMODEA
  #define _PCDEVMODEA                    const DEVMODEA FAR *
#endif

#ifndef _PCDEVMODEW
  #define _PCDEVMODEW                    const DEVMODEW FAR *
#endif

#ifndef PSCLLCOLUMN
  #define PSCLLCOLUMN                    scLlColumn FAR *
#endif

#ifndef PVARIANT
  #define PVARIANT                       VARIANT FAR *
#endif


/*--- constant declarations ---*/

#define LL_JOBOPENFLAG_NOLLXPRELOAD    (0x00001000)        
#define LL_DEBUG_CMBTLL                (0x0001)             /* debug CMBTLL.DLL */
#define LL_DEBUG_CMBTDWG               (0x0002)             /* debug CMBTDWG.DLL */
#define LL_DEBUG_CMBTLL_NOCALLBACKS    (0x0004)            
#define LL_DEBUG_CMBTLL_NOSTORAGE      (0x0008)            
#define LL_DEBUG_CMBTLL_NOWAITDLG      (0x0010)            
#define LL_DEBUG_CMBTLL_NOSYSINFO      (0x0020)            
#define LL_DEBUG_CMBTLL_LOGTOFILE      (0x0040)            
#define LL_VERSION_MAJOR               (1)                  /* direct return of major version (f.ex. 1) */
#define LL_VERSION_MINOR               (2)                  /* direct return of minor version (f.ex. 13) */
#define LL_VERSION_SERNO_LO            (3)                  /* LOWORD(serial number) */
#define LL_VERSION_SERNO_HI            (4)                  /* HIWORD(serial number) */
#define LL_VERSION_OEMNO               (5)                  /* OEM number */
#define LL_VERSION_RESMAJOR            (11)                 /* internal, for LlRCGetVersion: resource version */
#define LL_VERSION_RESMINOR            (12)                 /* internal, for LlRCGetVersion: resource version */
#define LL_VERSION_RESLANGUAGE         (14)                 /* internal, for LlRCGetVersion: resource language */
#define LL_CMND_DRAW_USEROBJ           (0)                  /* callback for LL_DRAWING_USEROBJ */
#define LL_CMND_EDIT_USEROBJ           (1)                  /* callback for LL_DRAWING_USEROBJ_DLG */
#define LL_CMND_TABLELINE              (10)                 /* callback for LL_CB_TABLELINE */
#define LL_TABLE_LINE_HEADER           (0)                 
#define LL_TABLE_LINE_BODY             (1)                 
#define LL_TABLE_LINE_FOOTER           (2)                 
#define LL_TABLE_LINE_FILL             (3)                 
#define LL_TABLE_LINE_GROUP            (4)                 
#define LL_TABLE_LINE_GROUPFOOTER      (5)                 
#define LL_CMND_TABLEFIELD             (11)                 /* callback for LL_CB_TABLEFIELD */
#define LL_TABLE_FIELD_HEADER          (0)                 
#define LL_TABLE_FIELD_BODY            (1)                 
#define LL_TABLE_FIELD_FOOTER          (2)                 
#define LL_TABLE_FIELD_FILL            (3)                 
#define LL_TABLE_FIELD_GROUP           (4)                 
#define LL_TABLE_FIELD_GROUPFOOTER     (5)                 
#define LL_CMND_EVALUATE               (12)                 /* callback for "External$" function */
#define LL_CMND_OBJECT                 (20)                 /* callback of LL_CB_OBJECT */
#define LL_CMND_PAGE                   (21)                 /* callback of LL_CB_PAGE */
#define LL_CMND_PROJECT                (22)                 /* callback of LL_CB_PROJECT */
#define LL_CMND_DRAW_GROUP_BEGIN       (23)                 /* callback for LlPrintBeginGroup */
#define LL_CMND_DRAW_GROUP_END         (24)                 /* callback for LlPrintEndGroup */
#define LL_CMND_DRAW_GROUPLINE         (25)                 /* callback for LlPrintGroupLine */
#define LL_RSP_GROUP_IMT               (0)                 
#define LL_RSP_GROUP_NEXTPAGE          (1)                 
#define LL_RSP_GROUP_OK                (2)                 
#define LL_RSP_GROUP_DRAWFOOTER        (3)                 
#define LL_CMND_HELP                   (30)                 /* lParam: HIWORD=HELP_xxx, LOWORD=Context # */
#define LL_CMND_ENABLEMENU             (31)                 /* undoc: lParam/LOWORD(lParam) = HMENU */
#define LL_CMND_MODIFYMENU             (32)                 /* undoc: lParam/LOWORD(lParam) = HMENU */
#define LL_CMND_SELECTMENU             (33)                 /* undoc: lParam=ID (return TRUE if processed) */
#define LL_CMND_GETVIEWERBUTTONSTATE   (34)                 /* HIWORD(lParam)=ID, LOWORD(lParam)=state */
#define LL_CMND_VARHELPTEXT            (35)                 /* lParam=LPSTR(Name), returns LPSTR(Helptext) */
#define LL_INFO_METER                  (37)                 /* lParam = addr(scLlMeterInfo) */
#define LL_METERJOB_LOAD               (1)                 
#define LL_METERJOB_SAVE               (2)                 
#define LL_METERJOB_CONSISTENCYCHECK   (3)                 
#define LL_NTFY_FAILSFILTER            (1000)               /* data set fails filter expression */
#define LL_NTFY_VIEWERBTNCLICKED       (38)                 /* user presses a preview button (action will be done). lParam=ID. result: 0=allowed, 1=not allowed */
#define LL_CMND_DLGEXPR_VARBTN         (39)                 /* lParam: @scLlDlgExprVarExt, return: IDOK for ok */
#define LL_CMND_HOSTPRINTER            (40)                 /* lParam: scLlPrinter */
#define LL_PRN_CREATE_DC               (1)                  /* scLlPrinter._nCmd values */
#define LL_PRN_DELETE_DC               (2)                 
#define LL_PRN_SET_ORIENTATION         (3)                 
#define LL_PRN_GET_ORIENTATION         (4)                 
#define LL_PRN_EDIT                    (5)                  /* unused */
#define LL_PRN_GET_DEVICENAME          (6)                 
#define LL_PRN_GET_DRIVERNAME          (7)                 
#define LL_PRN_GET_PORTNAME            (8)                 
#define LL_PRN_RESET_DC                (9)                 
#define LL_PRN_COMPARE_PRINTER         (10)                
#define LL_PRN_GET_PHYSPAGE            (11)                
#define LL_PRN_SET_PHYSPAGE            (12)                
#define LL_PRN_GET_PAPERFORMAT         (13)                 /* fill _nPaperFormat */
#define LL_PRN_SET_PAPERFORMAT         (14)                 /* _nPaperFormat, _xPaperSize, _yPaperSize */
#define LL_OEM_TOOLBAR_START           (41)                
#define LL_OEM_TOOLBAR_END             (50)                
#define LL_NTFY_EXPRERROR              (51)                 /* lParam = LPCSTR(error text) */
#define LL_CMND_CHANGE_DCPROPERTIES_CREATE (52)                 /* lParam = addr(scLlPrinter), _hDC is valid */
#define LL_CMND_CHANGE_DCPROPERTIES_DOC (53)                 /* lParam = addr(scLlPrinter), _hDC is valid */
#define LL_CMND_CHANGE_DCPROPERTIES_PAGE (54)                 /* lParam = addr(scLlPrinter), _hDC is valid */
#define LL_CMND_CHANGE_DCPROPERTIES_PREPAGE (56)                 /* lParam = addr(scLlPrinter), _hDC and _pszBuffer( DEVMODE* ) are valid */
#define LL_CMND_MODIFY_METAFILE        (55)                 /* lParam = handle of metafile (32 bit: enh. metafile) */
#define LL_INTERNAL_MAXEVENTNUMBER     (56)                 /* internal: max. event number */
#define OBJECT_LABEL                   (1)                  /* old - please do not use any more */
#define OBJECT_LIST                    (2)                 
#define OBJECT_CARD                    (3)                 
#define LL_PROJECT_LABEL               (1)                  /* new names... */
#define LL_PROJECT_LIST                (2)                 
#define LL_PROJECT_CARD                (3)                 
#define LL_OBJ_MARKER                  (0)                  /* internal use only */
#define LL_OBJ_TEXT                    (1)                  /* the following are used in the object callback */
#define LL_OBJ_RECT                    (2)                 
#define LL_OBJ_LINE                    (3)                 
#define LL_OBJ_BARCODE                 (4)                 
#define LL_OBJ_DRAWING                 (5)                 
#define LL_OBJ_TABLE                   (6)                 
#define LL_OBJ_TEMPLATE                (7)                 
#define LL_OBJ_ELLIPSE                 (8)                 
#define LL_OBJ_GROUP                   (9)                  /* internal use only */
#define LL_OBJ_RTF                     (10)                
#define LL_OBJ_LLX                     (11)                
#define LL_OBJ_LAST                    (11)                 /* last object type (for loops as upper bound) */
#define LL_OBJ_PAGE                    (255)                /* for exporter */
#define LL_TYPEMASK                    (0x7ff00000)        
#define LL_TABLE_FOOTERFIELD           (0x00008000)         /* 'or'ed for footline-only fields // reserved also for Variables (see "$$xx$$")!!!! */
#define LL_TABLE_GROUPFIELD            (0x00004000)         /* 'or'ed for groupline-only fields */
#define LL_TABLE_HEADERFIELD           (0x00002000)         /* 'or'ed for headline-only fields */
#define LL_TABLE_BODYFIELD             (0x00001000)         /* 'or'ed for headline-only fields */
#define LL_TABLE_GROUPFOOTERFIELD      (0x00000800)         /* 'or'ed for group-footer-line-only fields */
#define LL_TABLE_FIELDTYPEMASK         (0x0000f800)         /* internal use */
#define LL_BARCODE                     (0x40000000)        
#define LL_BARCODE_EAN13               (0x40000000)        
#define LL_BARCODE_EAN8                (0x40000001)        
#define LL_BARCODE_UPCA                (0x40000002)        
#define LL_BARCODE_UPCE                (0x40000003)        
#define LL_BARCODE_3OF9                (0x40000004)        
#define LL_BARCODE_25INDUSTRIAL        (0x40000005)        
#define LL_BARCODE_25INTERLEAVED       (0x40000006)        
#define LL_BARCODE_25DATALOGIC         (0x40000007)        
#define LL_BARCODE_25MATRIX            (0x40000008)        
#define LL_BARCODE_POSTNET             (0x40000009)        
#define LL_BARCODE_FIM                 (0x4000000A)        
#define LL_BARCODE_CODABAR             (0x4000000B)        
#define LL_BARCODE_EAN128              (0x4000000C)        
#define LL_BARCODE_CODE128             (0x4000000D)        
#define LL_BARCODE_DP_LEITCODE         (0x4000000E)        
#define LL_BARCODE_DP_IDENTCODE        (0x4000000F)        
#define LL_BARCODE_GERMAN_PARCEL       (0x40000010)        
#define LL_BARCODE_CODE93              (0x40000011)        
#define LL_BARCODE_MSI                 (0x40000012)        
#define LL_BARCODE_CODE11              (0x40000013)        
#define LL_BARCODE_MSI_10_CD           (0x40000014)        
#define LL_BARCODE_MSI_10_10           (0x40000015)        
#define LL_BARCODE_MSI_11_10           (0x40000016)        
#define LL_BARCODE_MSI_PLAIN           (0x40000017)        
#define LL_BARCODE_EAN14               (0x40000018)        
#define LL_BARCODE_UCC14               (0x40000019)        
#define LL_BARCODE_LLXSTART            (0x40000040)        
#define LL_BARCODE_PDF417              (0x40000040)        
#define LL_BARCODE_MAXICODE            (0x40000041)        
#define LL_BARCODE_MAXICODE_UPS        (0x40000042)        
#define LL_BARCODE_JAPAN_POSTAL        (0x40000043)        
#define LL_BARCODE_METHODMASK          (0x000000ff)        
#define LL_BARCODE_WITHTEXT            (0x00000100)        
#define LL_BARCODE_WITHOUTTEXT         (0x00000200)        
#define LL_BARCODE_TEXTDONTCARE        (0x00000000)        
#define LL_DRAWING                     (0x20000000)        
#define LL_DRAWING_HMETA               (0x20000001)        
#define LL_DRAWING_USEROBJ             (0x20000002)        
#define LL_DRAWING_USEROBJ_DLG         (0x20000003)        
#define LL_DRAWING_HBITMAP             (0x20000004)        
#define LL_DRAWING_HICON               (0x20000005)        
#define LL_DRAWING_HEMETA              (0x20000006)        
#define LL_DRAWING_HDIB                (0x20000007)         /* global handle to BITMAPINFO and bits */
#define LL_DRAWING_METHODMASK          (0x000000ff)        
#define LL_META_MAXX                   (10000)             
#define LL_META_MAXY                   (10000)             
#define LL_TEXT                        (0x10000000)        
#define LL_TEXT_ALLOW_WORDWRAP         (0x10000000)        
#define LL_TEXT_DENY_WORDWRAP          (0x10000001)        
#define LL_TEXT_FORCE_WORDWRAP         (0x10000002)        
#define LL_NUMERIC                     (0x08000000)        
#define LL_NUMERIC_LOCALIZED           (0x08000001)        
#define LL_DATE                        (0x04000000)         /* LL's own julian */
#define LL_DATE_DELPHI_1               (0x04000001)        
#define LL_DATE_DELPHI                 (0x04000002)         /* DELPHI 2, 3, 4: OLE DATE */
#define LL_DATE_MS                     (0x04000002)         /* MS C/Basic: OLE DATE */
#define LL_DATE_OLE                    (0x04000002)         /* generic: OLE DATE */
#define LL_DATE_VFOXPRO                (0x04000003)         /* nearly LL's own julian, has an offset of 1! */
#define LL_DATE_DMY                    (0x04000004)         /* <d><sep><m><sep><yyyy>. Year MUST be 4 digits! */
#define LL_DATE_MDY                    (0x04000005)         /* <m><sep><d><sep><yyyy>. Year MUST be 4 digits! */
#define LL_DATE_YMD                    (0x04000006)         /* <yyyy><sep><m><sep><d>. Year MUST be 4 digits! */
#define LL_DATE_YYYYMMDD               (0x04000007)         /* <yyyymmdd> */
#define LL_DATE_LOCALIZED              (0x04000008)         /* localized (automatic VariantConversion) */
#define LL_DATE_METHODMASK             (0x000000ff)        
#define LL_BOOLEAN                     (0x02000000)        
#define LL_RTF                         (0x01000000)        
#define LL_HTML                        (0x00800000)        
#define LL_LLXOBJECT                   (0x00100000)         /* internal use only */
#define LL_FIXEDNAME                   (0x8000)            
#define LL_NOSAVEAS                    (0x4000)            
#define LL_EXPRCONVERTQUIET            (0x1000)             /* convert to new expressions without warning box */
#define LL_NONAMEINTITLE               (0x0800)             /* no file name appended to title */
#define LL_PRVOPT_PRN_USEDEFAULT       (0x00000000)        
#define LL_PRVOPT_PRN_ASKPRINTERIFNEEDED (0x00000001)        
#define LL_PRVOPT_PRN_ASKPRINTERALWAYS (0x00000002)        
#define LL_PRVOPT_PRN_ALWAYSUSEDEFAULT (0x00000003)        
#define LL_PRVOPT_PRN_ASSIGNMASK       (0x00000003)         /* used by L&L */
#define LL_OPTION_COPIES               (0)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_STARTPAGE            (1)                  /* compatibility only, please use LL_PRNOPT_PAGE */
#define LL_OPTION_PAGE                 (1)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_OFFSET               (2)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_COPIES_SUPPORTED     (3)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_FIRSTPAGE            (5)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_LASTPAGE             (6)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_JOBPAGES             (7)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_OPTION_PRINTORDER           (8)                  /* compatibility only, please use LL_PRNOPT_... */
#define LL_PRNOPT_COPIES               (0)                 
#define LL_COPIES_HIDE                 (-32768)             /* anything negative... */
#define LL_PRNOPT_STARTPAGE            (1)                 
#define LL_PRNOPT_PAGE                 (1)                  /* alias; please do not use STARTPAGE any more... */
#define LL_PAGE_HIDE                   (-32768)             /* must be exactly this value! */
#define LL_PRNOPT_OFFSET               (2)                 
#define LL_PRNOPT_COPIES_SUPPORTED     (3)                 
#define LL_PRNOPT_UNITS                (4)                  /* only GetOption() */
#define LL_UNITS_MM_DIV_10             (0)                  /* for LL_OPTION_UNITS and LL_OPTION_UNITS_DEFAULT */
#define LL_UNITS_INCH_DIV_100          (1)                 
#define LL_UNITS_INCH_DIV_1000         (2)                 
#define LL_UNITS_SYSDEFAULT_LORES      (3)                  /* for LL_OPTION_UNITS_DEFAULT only */
#define LL_UNITS_SYSDEFAULT            (4)                  /* for LL_OPTION_UNITS_DEFAULT only */
#define LL_PRNOPT_FIRSTPAGE            (5)                 
#define LL_PRNOPT_LASTPAGE             (6)                 
#define LL_PRNOPT_JOBPAGES             (7)                 
#define LL_PRNOPT_PRINTORDER           (8)                 
#define LL_PRINTORDER_HORZ_LTRB        (0)                 
#define LL_PRINTORDER_VERT_LTRB        (1)                 
#define LL_PRINTORDER_HORZ_RBLT        (2)                 
#define LL_PRINTORDER_VERT_RBLT        (3)                 
#define LL_PRNOPT_PRINTORDER_P1        (9)                  /* for future support */
#define LL_PRNOPT_PRINTORDER_P2        (10)                 /* for future support */
#define LL_PRNOPT_DEFPRINTERINSTALLED  (11)                 /* returns 0 for no default printer, 1 for default printer present */
#define LL_PRNOPT_PRINTDLG_DESTMASK    (12)                 /* any combination of the ones below... Default: all. Outdated, please use LL_OPTIONSTR_EXPORTS_ALLOWED */
#define LL_DESTINATION_PRN             (1)                 
#define LL_DESTINATION_PRV             (2)                 
#define LL_DESTINATION_FILE            (4)                 
#define LL_DESTINATION_EXTERN          (8)                 
#define LL_DESTINATION_MSFAX           (16)                 /* reserved */
#define LL_PRNOPT_PRINTDLG_DEST        (13)                 /* default destination; outdated, please use LL_PRNOPTSTR_EXPORT */
#define LL_PRNOPT_PRINTDLG_ONLYPRINTERCOPIES (14)                 /* show copies option in dialog only if they are supported by the printer. default: FALSE */
#define LL_PRNOPT_JOBID                (17)                
#define LL_PRNOPTSTR_PRINTDST_FILENAME (0)                  /* print to file: default filename (LlGet/SetPrintOptionString) */
#define LL_PRNOPTSTR_EXPORTDESCR       (1)                  /* r/o, returns the description of the export chosen */
#define LL_PRNOPTSTR_EXPORT            (2)                  /* sets default exporter to use / returns the name of the export chosen */
#define LL_PRNOPTSTR_PRINTJOBNAME      (3)                  /* set name to be given to StartDoc() (lpszMessage of LlPrintWithBoxStart() */
#define LL_PRINT_V1POINTX              (0x0000)            
#define LL_PRINT_NORMAL                (0x0100)            
#define LL_PRINT_PREVIEW               (0x0200)            
#define LL_PRINT_STORAGE               (0x0200)             /* same as LL_PRINT_PREVIEW */
#define LL_PRINT_FILE                  (0x0400)            
#define LL_PRINT_USERSELECT            (0x0800)            
#define LL_PRINT_EXPORT                (0x0800)             /* same as LL_PRINT_USERSELECT */
#define LL_PRINT_MODEMASK              (0x0f00)            
#define LL_PRINT_MULTIPLE_JOBS         (0x1000)            
#define LL_PRINT_KEEPJOB               (0x2000)            
#define LL_PRINT_FILENAME_IS_HGLOBAL   (0x4000)             /* reserved, not yet used */
#define LL_BOXTYPE_NORMALMETER         (0)                 
#define LL_BOXTYPE_BRIDGEMETER         (1)                 
#define LL_BOXTYPE_NORMALWAIT          (2)                 
#define LL_BOXTYPE_BRIDGEWAIT          (3)                 
#define LL_BOXTYPE_EMPTYWAIT           (4)                 
#define LL_BOXTYPE_EMPTYABORT          (5)                 
#define LL_BOXTYPE_STDWAIT             (6)                 
#define LL_BOXTYPE_STDABORT            (7)                 
#define LL_FILE_ALSONEW                (0x8000)            
#define LL_FCTPARATYPE_DOUBLE          (0x01)              
#define LL_FCTPARATYPE_DATE            (0x02)              
#define LL_FCTPARATYPE_STRING          (0x04)              
#define LL_FCTPARATYPE_BOOL            (0x08)              
#define LL_FCTPARATYPE_DRAWING         (0x10)              
#define LL_FCTPARATYPE_BARCODE         (0x20)              
#define LL_FCTPARATYPE_ALL             (0x3f)              
#define LL_FCTPARATYPE_PARA1           (0x8001)            
#define LL_FCTPARATYPE_PARA2           (0x8002)            
#define LL_FCTPARATYPE_PARA3           (0x8003)            
#define LL_FCTPARATYPE_PARA4           (0x8004)            
#define LL_FCTPARATYPE_SAME            (0x803f)            
#define LL_EXPRTYPE_DOUBLE             (1)                 
#define LL_EXPRTYPE_DATE               (2)                 
#define LL_EXPRTYPE_STRING             (3)                 
#define LL_EXPRTYPE_BOOL               (4)                 
#define LL_EXPRTYPE_DRAWING            (5)                 
#define LL_EXPRTYPE_BARCODE            (6)                 
#define LL_OPTION_NEWEXPRESSIONS       (0)                  /* default: TRUE */
#define LL_OPTION_ONLYONETABLE         (1)                  /* default: FALSE */
#define LL_OPTION_TABLE_COLORING       (2)                  /* default: LL_COLORING_LL */
#define LL_COLORING_LL                 (0)                 
#define LL_COLORING_PROGRAM            (1)                 
#define LL_COLORING_DONTCARE           (2)                 
#define LL_OPTION_SUPERVISOR           (3)                  /* default: FALSE */
#define LL_OPTION_UNITS                (4)                  /* default: see LL_OPTION_METRIC */
#define LL_OPTION_TABSTOPS             (5)                  /* default: LL_TABS_DELETE */
#define LL_TABS_DELETE                 (0)                 
#define LL_TABS_EXPAND                 (1)                 
#define LL_OPTION_CALLBACKMASK         (6)                  /* default: 0x00000000 */
#define LL_CB_PAGE                     (0x40000000)         /* callback for each page */
#define LL_CB_PROJECT                  (0x20000000)         /* callback for each label */
#define LL_CB_OBJECT                   (0x10000000)         /* callback for each object */
#define LL_CB_HELP                     (0x08000000)         /* callback for HELP (F1/Button) */
#define LL_CB_TABLELINE                (0x04000000)         /* callback for table line */
#define LL_CB_TABLEFIELD               (0x02000000)         /* callback for table field */
#define LL_OPTION_CALLBACKPARAMETER    (7)                  /* default: 0 */
#define LL_OPTION_HELPAVAILABLE        (8)                  /* default: TRUE */
#define LL_OPTION_SORTVARIABLES        (9)                  /* default: TRUE */
#define LL_OPTION_SUPPORTPAGEBREAK     (10)                 /* default: TRUE */
#define LL_OPTION_SHOWPREDEFVARS       (11)                 /* default: TRUE */
#define LL_OPTION_USEHOSTPRINTER       (13)                 /* default: FALSE // use host printer via callback */
#define LL_OPTION_EXTENDEDEVALUATION   (14)                 /* allows expressions in chevrons (amwin mode) */
#define LL_OPTION_TABREPRESENTATIONCODE (15)                 /* default: 247 (0xf7) */
#define LL_OPTION_METRIC               (18)                 /* default: depends on Windows defaults */
#define LL_OPTION_ADDVARSTOFIELDS      (19)                 /* default: FALSE */
#define LL_OPTION_MULTIPLETABLELINES   (20)                 /* default: TRUE */
#define LL_OPTION_CONVERTCRLF          (21)                 /* default: FALSE */
#define LL_OPTION_WIZ_FILENEW          (22)                 /* default: FALSE */
#define LL_OPTION_RETREPRESENTATIONCODE (23)                 /* default: LL_CHAR_NEWLINE (182) */
#define LL_OPTION_PRVZOOM_PERC         (25)                 /* initial preview zoom */
#define LL_OPTION_PRVRECT_LEFT         (26)                 /* initial preview position */
#define LL_OPTION_PRVRECT_TOP          (27)                
#define LL_OPTION_PRVRECT_WIDTH        (28)                
#define LL_OPTION_PRVRECT_HEIGHT       (29)                
#define LL_OPTION_STORAGESYSTEM        (30)                 /* 0=LX4-compatible, 1=STORAGE */
#define LL_STG_COMPAT4                 (0)                 
#define LL_STG_STORAGE                 (1)                 
#define LL_OPTION_COMPRESSSTORAGE      (31)                 /* 32 bit, STORAGE only [TRUE/FALSE] */
#define LL_OPTION_NOPARAMETERCHECK     (32)                 /* you need a bit more speed? */
#define LL_OPTION_NONOTABLECHECK       (33)                 /* don't check on "NO_TABLEOBJECT" error */
#define LL_OPTION_DRAWFOOTERLINEONPRINT (34)                 /* delay footerline printing to LlPrint(). Default FALSE */
#define LL_OPTION_PRVZOOM_LEFT         (35)                 /* initial preview position in percent of screen */
#define LL_OPTION_PRVZOOM_TOP          (36)                
#define LL_OPTION_PRVZOOM_WIDTH        (37)                
#define LL_OPTION_PRVZOOM_HEIGHT       (38)                
#define LL_OPTION_SPACEOPTIMIZATION    (40)                 /* default: TRUE */
#define LL_OPTION_REALTIME             (41)                 /* default: FALSE */
#define LL_OPTION_AUTOMULTIPAGE        (42)                 /* default: TRUE */
#define LL_OPTION_USEBARCODESIZES      (43)                 /* default: FALSE */
#define LL_OPTION_MAXRTFVERSION        (44)                 /* default: 0x100 (1.0) */
#define LL_OPTION_VARSCASESENSITIVE    (46)                 /* default: FALSE */
#define LL_OPTION_DELAYTABLEHEADER     (47)                 /* default: FALSE */
#define LL_OPTION_OFNDIALOG_EXPLORER   (48)                 /* default: Win16: FALSE, WIN32: NewShell present */
#define LL_OPTION_OFN_NOPLACESBAR      (0x40000000)        
#define LL_OPTION_EMFRESOLUTION        (49)                 /* default: 100 for 1/100 mm */
#define LL_OPTION_SETCREATIONINFO      (50)                 /* default: TRUE */
#define LL_OPTION_XLATVARNAMES         (51)                 /* default: TRUE */
#define LL_OPTION_LANGUAGE             (52)                 /* returns current language (r/o) */
#define LL_OPTION_PHANTOMSPACEREPRESENTATIONCODE (54)                 /* default: LL_CHAR_PHANTOMSPACE  */
#define LL_OPTION_LOCKNEXTCHARREPRESENTATIONCODE (55)                 /* default: LL_CHAR_LOCK  */
#define LL_OPTION_EXPRSEPREPRESENTATIONCODE (56)                 /* default: LL_CHAR_EXPRSEP */
#define LL_OPTION_DEFPRINTERINSTALLED  (57)                 /* r/o */
#define LL_OPTION_CALCSUMVARSONINVISIBLELINES (58)                 /* default: FALSE - only default value if no preferences in project */
#define LL_OPTION_NOFOOTERPAGEWRAP     (59)                 /* default: FALSE - only default value if no preferences in project */
#define LL_OPTION_IMMEDIATELASTPAGE    (64)                 /* default: FALSE */
#define LL_OPTION_LCID                 (65)                 /* default: LOCALE_USER_DEFAULT */
#define LL_OPTION_TEXTQUOTEREPRESENTATIONCODE (66)                 /* default: 1 */
#define LL_OPTION_SCALABLEFONTSONLY    (67)                 /* default: TRUE */
#define LL_OPTION_NOTIFICATIONMESSAGEHWND (68)                 /* default: NULL (parent window handle) */
#define LL_OPTION_DEFDEFFONT           (69)                 /* default: GetStockObject(ANSI_VAR_FONT) */
#define LL_OPTION_CODEPAGE             (70)                 /* default: CP_ACP; set codepage to use for conversions.  */
#define LL_OPTION_FORCEFONTCHARSET     (71)                 /* default: FALSE; set font's charset to the codepage according to LL_OPTION_LCID. Default: FALSE */
#define LL_OPTION_COMPRESSRTF          (72)                 /* default: TRUE; compress RTF text > 1024 bytes in project file */
#define LL_OPTION_ALLOW_LLX_EXPORTERS  (74)                 /* default: TRUE; allow ILlXExport extensions */
#define LL_OPTION_SUPPORTS_PRNOPTSTR_EXPORT (75)                 /* default: FALSE: hides "set to default" button in "export option" tab in designer */
#define LL_OPTION_DEBUGFLAG            (76)                
#define LL_OPTION_SKIPRETURNATENDOFRTF (77)                 /* default: FALSE */
#define LL_OPTION_INTERCHARSPACING     (78)                 /* default: FALSE: allows character interspacing in case of block justify  */
#define LL_OPTION_INCLUDEFONTDESCENT   (79)                 /* default: FALSE (compatibility) */
#define LL_OPTION_RESOLUTIONCOMPATIBLETO9X (80)                 /* default: FALSE (on NT/2K, else TRUE) */
#define LL_OPTION_USECHARTFIELDS       (81)                 /* default: FALSE */
#define LL_OPTION_OFNDIALOG_NOPLACESBAR (82)                 /* default: FALSE; do not use "Places" bar in NT2K? */
#define LL_OPTION_SKETCH_COLORDEPTH    (83)                 /* default: 1 */
#define LL_OPTION_FINAL_TRUE_ON_LASTPAGE (84)                 /* default: FALSE: internal use */
#define LL_OPTION_LLXAUTOSORTAXIS      (85)                 /* default: FALSE */
#define LL_OPTION_INTERCHARSPACING_FORCED (86)                 /* default: FALSE: forces character interspacing calculation in TEXT objects (possibly dangerous and slow) */
#define LL_OPTION_RTFAUTOINCREMENT     (87)                 /* default: FALSE, to increment RTF char pointer if nothing can be printed */
#define LL_OPTION_UNITS_DEFAULT        (88)                 /* default: LL_OPTION_UNITS_SYSDEFAULT. Use for contols that query the units, where we need to return "sysdefault" also */
#define LL_OPTION_NO_MAPI              (89)                 /* default: FALSE. Inhibit MAPI load for preview */
#define LL_OPTION_TOOLBARSTYLE         (90)                 /* default: LL_OPTION_TOOLBARSTYLE_OFFICE97|LL_OPTION_TOOLBARSTYLEFLAG_DOCKABLE */
#define LL_OPTION_TOOLBARSTYLE_STANDARD (0)                  /* OFFICE97 alike style */
#define LL_OPTION_TOOLBARSTYLE_OFFICEXP (1)                  /* DOTNET/OFFICE_XP alike style */
#define LL_OPTION_TOOLBARSTYLEMASK     (0x0f)              
#define LL_OPTION_TOOLBARSTYLEFLAG_GRADIENT (0x80)               /* starting with XP, use gradient style */
#define LL_OPTION_TOOLBARSTYLEFLAG_DOCKABLE (0x40)               /* dockable toolbars? */
#define LL_OPTION_TOOLBARSTYLEFLAG_CANCLOSE (0x20)               /* internal use only */
#define LL_OPTION_MENUSTYLE            (91)                 /* default: LL_OPTION_MENUSTYLE_STANDARD */
#define LL_OPTION_MENUSTYLE_STANDARD_WITHOUT_BITMAPS (0)                  /* values: see CTL */
#define LL_OPTION_MENUSTYLE_STANDARD   (1)                 
#define LL_OPTION_MENUSTYLE_OFFICEXP   (2)                 
#define LL_OPTION_RULERSTYLE           (92)                 /* default: LL_OPTION_RULERSTYLE_FLAT */
#define LL_OPTION_RULERSTYLE_FLAT      (0x10)              
#define LL_OPTION_RULERSTYLE_GRADIENT  (0x80)              
#define LL_OPTION_STATUSBARSTYLE       (93)                
#define LL_OPTION_STATUSBARSTYLE_STANDARD (0)                 
#define LL_OPTION_STATUSBARSTYLE_OFFICEXP (1)                 
#define LL_OPTION_TABBARSTYLE          (94)                
#define LL_OPTION_TABBARSTYLE_STANDARD (0)                 
#define LL_OPTION_TABBARSTYLE_OFFICEXP (1)                 
#define LL_OPTION_DROPWINDOWSTYLE      (95)                
#define LL_OPTION_DROPWINDOWSTYLE_STANDARD (0)                 
#define LL_OPTION_DROPWINDOWSTYLE_OFFICEXP (1)                 
#define LL_OPTION_INTERFACEWRAPPER     (96)                 /* returns IL<n>* */
#define LL_OPTION_FONTQUALITY          (97)                 /* LOGFONT.lfQuality, default: DEFAULT_QUALITY */
#define LL_OPTION_FONTPRECISION        (98)                 /* LOGFONT.lfOutPrecision, default: OUT_STRING_PRECIS */
#define LL_OPTION_UISTYLE              (99)                 /* UI collection, w/o */
#define LL_OPTION_UISTYLE_STANDARD     (0)                  /* 90=0x40, 91=1, 92=0x10, 93=0, 94=0, 95=0 */
#define LL_OPTION_UISTYLE_OFFICEXP     (1)                  /* 90=0x41, 91=2, 92=0x10, 93=1, 94=1, 95=1 */
#define LL_OPTION_NOFILEVERSIONUPGRADEWARNING (100)                /* default: FALSE */
#define LL_OPTION_UPDATE_FOOTER_ON_DATALINEBREAK_AT_FIRST_LINE (101)                /* default: FALSE */
#define LL_OPTIONSTR_LABEL_PRJEXT      (0)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LABEL_PRVEXT      (1)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LABEL_PRNEXT      (2)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_CARD_PRJEXT       (3)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_CARD_PRVEXT       (4)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_CARD_PRNEXT       (5)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LIST_PRJEXT       (6)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LIST_PRVEXT       (7)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LIST_PRNEXT       (8)                  /* internal... (compatibility to L6) */
#define LL_OPTIONSTR_LLXPATHLIST       (12)                
#define LL_OPTIONSTR_SHORTDATEFORMAT   (13)                
#define LL_OPTIONSTR_DECIMAL           (14)                 /* decimal point, default: system */
#define LL_OPTIONSTR_THOUSAND          (15)                 /* thousands separator, default: system */
#define LL_OPTIONSTR_CURRENCY          (16)                 /* currency symbol, default: system */
#define LL_OPTIONSTR_EXPORTS_AVAILABLE (17)                 /* r/o */
#define LL_OPTIONSTR_EXPORTS_ALLOWED   (18)                
#define LL_OPTIONSTR_DEFDEFFONT        (19)                 /* in "{(r,g,b),size,<logfont>}" */
#define LL_OPTIONSTR_EXPORTFILELIST    (20)                
#define LL_OPTIONSTR_MAILTO            (24)                 /* default TO: address for mailing from viewer */
#define LL_OPTIONSTR_MAILTO_CC         (25)                 /* default CC: address for mailing from viewer */
#define LL_OPTIONSTR_MAILTO_BCC        (26)                 /* default BCC: address for mailing from viewer */
#define LL_OPTIONSTR_MAILTO_SUBJECT    (27)                 /* default subject for mailing from viewer */
#define LL_OPTIONSTR_SAVEAS_PATH       (28)                 /* default filename for saving the LL file from viewer */
#define LL_OPTIONSTR_LABEL_PRJDESCR    (29)                 /* "Etikett" ... */
#define LL_OPTIONSTR_CARD_PRJDESCR     (30)                
#define LL_OPTIONSTR_LIST_PRJDESCR     (31)                
#define LL_OPTIONSTR_LLFILEDESCR       (32)                 /* "Vorschau-Datei" */
#define LL_OPTIONSTR_PROJECTPASSWORD   (33)                 /* w/o, of course :) */
#define LL_OPTIONSTR_FAX_RECIPNAME     (34)                
#define LL_OPTIONSTR_FAX_RECIPNUMBER   (35)                
#define LL_OPTIONSTR_FAX_QUEUENAME     (36)                
#define LL_OPTIONSTR_FAX_SENDERNAME    (37)                
#define LL_OPTIONSTR_FAX_SENDERCOMPANY (38)                
#define LL_OPTIONSTR_FAX_SENDERDEPT    (39)                
#define LL_OPTIONSTR_FAX_SENDERBILLINGCODE (40)                
#define LL_OPTIONSTR_FAX_AVAILABLEQUEUES (42)                 /* r/o (Tab-separated) [job can be -1 or a valid job] */
#define LL_OPTIONSTR_LOGFILEPATH       (43)                
#define LL_OPTIONSTR_LICENSINGINFO     (44)                 /* w/o, SERNO to define licensing state */
#define LL_OPTIONSTR_PRINTERALIASLIST  (45)                 /* multiple "PrnOld=PrnNew1[;PrnNew2[;...]]", erase with NULL or "" */
#define LL_SYSCOMMAND_MINIMIZE         (-1)                
#define LL_SYSCOMMAND_MAXIMIZE         (-2)                
#define LL_DLGBOXMODE_3DBUTTONS        (0x8000)             /* 'or'ed */
#define LL_DLGBOXMODE_3DFRAME2         (0x4000)             /* 'OR'ed */
#define LL_DLGBOXMODE_3DFRAME          (0x1000)             /* 'OR'ed */
#define LL_DLGBOXMODE_NOBITMAPS        (0x2000)             /* 'or'ed */
#define LL_DLGBOXMODE_DONTCARE         (0x0000)             /* load from INI */
#define LL_DLGBOXMODE_SAA              (0x0001)            
#define LL_DLGBOXMODE_ALT1             (0x0002)            
#define LL_DLGBOXMODE_ALT2             (0x0003)            
#define LL_DLGBOXMODE_ALT3             (0x0004)            
#define LL_DLGBOXMODE_ALT4             (0x0005)            
#define LL_DLGBOXMODE_ALT5             (0x0006)            
#define LL_DLGBOXMODE_ALT6             (0x0007)            
#define LL_DLGBOXMODE_ALT7             (0x0008)            
#define LL_DLGBOXMODE_ALT8             (0x0009)             /* Win95 */
#define LL_DLGBOXMODE_ALT9             (0x000A)             /* Win98 */
#define LL_DLGBOXMODE_ALT10            (0x000B)             /* Win98 with gray/color button bitmaps like IE4 */
#define LL_DLGBOXMODE_TOOLTIPS98       (0x0800)             /* 'OR'ed - sliding tooltips */
#define LL_CTL_ADDTOSYSMENU            (0x00000004)         /* from CTL */
#define LL_CTL_ALSOCHILDREN            (0x00000010)        
#define LL_CTL_CONVERTCONTROLS         (0x00010000)        
#define LL_GROUP_ALWAYSFOOTER          (0x40000000)        
#define LL_PRINTERCONFIG_SAVE          (1)                 
#define LL_PRINTERCONFIG_RESTORE       (2)                 
#define LL_ERR_STG_NOSTORAGE           (-1000)             
#define LL_ERR_STG_BADVERSION          (-1001)             
#define LL_ERR_STG_READ                (-1002)             
#define LL_ERR_STG_WRITE               (-1003)             
#define LL_ERR_STG_UNKNOWNSYSTEM       (-1004)             
#define LL_ERR_STG_BADHANDLE           (-1005)             
#define LL_ERR_STG_ENDOFLIST           (-1006)             
#define LL_ERR_STG_BADJOB              (-1007)             
#define LL_ERR_STG_ACCESSDENIED        (-1008)             
#define LL_ERR_STG_BADSTORAGE          (-1009)             
#define LL_ERR_STG_CANNOTGETMETAFILE   (-1010)             
#define LL_WRN_STG_UNFAXED_PAGES       (-1100)             
#define LL_STGSYS_OPTION_HAS16BITPAGES (200)                /* has job 16 bit pages? */
#define LL_STGSYS_OPTION_BOXTYPE       (201)                /* wait meter box type */
#define LL_STGSYS_OPTION_UNITS         (203)                /* LL_UNITS_INCH_DIV_100 or LL_UNITS_MM_DIV_10 */
#define LL_STGSYS_OPTION_PRINTERCOUNT  (204)                /* number of printers (1 or 2) */
#define LL_STGSYS_OPTION_ISSTORAGE     (205)                /* returns whether file is STORAGE or COMPAT4 */
#define LL_STGSYS_OPTION_EMFRESOLUTION (206)                /* EMFRESOLUTION used to print the file */
#define LL_STGSYS_OPTION_JOB           (207)                /* returns current job number */
#define LL_STGSYS_OPTION_TOTALPAGES    (208)                /* differs from GetPageCount() if print range in effect */
#define LL_STGSYS_OPTION_PAGESWITHFAXNUMBER (209)                /* NYI */
#define LL_STGSYS_OPTION_PAGENUMBER    (0)                  /* page number of current page */
#define LL_STGSYS_OPTION_COPIES        (1)                  /* number of copies (same for all pages at the moment) */
#define LL_STGSYS_OPTION_PRN_ORIENTATION (2)                  /* orientation (DMORIENT_LANDSCAPE, DMORIENT_PORTRAIT) */
#define LL_STGSYS_OPTION_PHYSPAGE      (3)                  /* is page "physical page" oriented? */
#define LL_STGSYS_OPTION_PRN_PIXELSOFFSET_X (4)                  /* this and the following values are */
#define LL_STGSYS_OPTION_PRN_PIXELSOFFSET_Y (5)                  /* values of the printer that the preview was */
#define LL_STGSYS_OPTION_PRN_PIXELS_X  (6)                  /* created on! */
#define LL_STGSYS_OPTION_PRN_PIXELS_Y  (7)                 
#define LL_STGSYS_OPTION_PRN_PIXELSPHYSICAL_X (8)                 
#define LL_STGSYS_OPTION_PRN_PIXELSPHYSICAL_Y (9)                 
#define LL_STGSYS_OPTION_PRN_PIXELSPERINCH_X (10)                
#define LL_STGSYS_OPTION_PRN_PIXELSPERINCH_Y (11)                
#define LL_STGSYS_OPTION_PRN_INDEX     (12)                 /* printer index of the page (0/1) */
#define LL_STGSYS_OPTION_PRN_PAPERTYPE (13)                
#define LL_STGSYS_OPTION_PRN_PAPERSIZE_X (14)                
#define LL_STGSYS_OPTION_PRN_PAPERSIZE_Y (15)                
#define LL_STGSYS_OPTION_PRN_FORCE_PAPERSIZE (16)                
#define LL_STGSYS_OPTION_PROJECTNAME   (100)                /* name of the original project (not page dependent) */
#define LL_STGSYS_OPTION_JOBNAME       (101)                /* name of the job (WindowTitle of LlPrintWithBoxStart()) (not page dependent) */
#define LL_STGSYS_OPTION_PRTNAME       (102)                /* printer name ("HP Laserjet 4L") */
#define LL_STGSYS_OPTION_PRTDEVICE     (103)                /* printer device ("PSCRIPT") */
#define LL_STGSYS_OPTION_PRTPORT       (104)                /* printer port ("LPT1:" or "\\server\printer") */
#define LL_STGSYS_OPTION_USER          (105)                /* user string (not page dependent) */
#define LL_STGSYS_OPTION_CREATION      (106)                /* creation date (not page dependent) */
#define LL_STGSYS_OPTION_CREATIONAPP   (107)                /* creation application (not page dependent) */
#define LL_STGSYS_OPTION_CREATIONDLL   (108)                /* creation DLL (not page dependent) */
#define LL_STGSYS_OPTION_CREATIONUSER  (109)                /* creation user and computer name (not page dependent) */
#define LL_STGSYS_OPTION_FAXPARA_QUEUE (110)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_RECIPNAME (111)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_RECIPNUMBER (112)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERNAME (113)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERCOMPANY (114)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERDEPT (115)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERBILLINGCODE (116)                /* NYI */
#define LL_STGSYS_OPTION_FAX_AVAILABLEQUEUES (118)                /* NYI, nPageIndex=1 */
#define LL_STGSYS_OPTION_PRINTERALIASLIST (119)                /* alternative printer list (taken from project) */
#define LL_STGSYSPRINTFLAG_FIT         (0x00000001)        
#define LL_STGSYSPRINTFLAG_STACKEDCOPIES (0x00000002)         /* n times page1, n times page2, ... (else n times (page 1...x)) */
#define LL_STGSYSPRINTFLAG_TRYPRINTERCOPIES (0x00000004)         /* first try printer copies, then simulated ones... */
#define LL_STGSYSPRINTFLAG_METER       (0x00000010)        
#define LL_STGSYSPRINTFLAG_ABORTABLEMETER (0x00000020)        
#define LL_STGSYSPRINTFLAG_METERMASK   (0x00000070)         /* allows 7 styles of abort boxes... */
#define LL_RTFTEXTMODE_RTF             (0x0000)            
#define LL_RTFTEXTMODE_PLAIN           (0x0001)            
#define LL_RTFTEXTMODE_EVALUATED       (0x0000)            
#define LL_RTFTEXTMODE_RAW             (0x0002)            
#define LL_ERR_BAD_JOBHANDLE           (-1)                 /* bad jobhandle */
#define LL_ERR_TASK_ACTIVE             (-2)                 /* LlDefineLayout() only once in a job */
#define LL_ERR_BAD_OBJECTTYPE          (-3)                 /* nObjType must be one of the allowed values (obsolete constant) */
#define LL_ERR_BAD_PROJECTTYPE         (-3)                 /* nObjType must be one of the allowed values */
#define LL_ERR_PRINTING_JOB            (-4)                 /* print job not opened, no print object */
#define LL_ERR_NO_BOX                  (-5)                 /* LlPrintSetBoxText(...) called when no abort box exists! */
#define LL_ERR_ALREADY_PRINTING        (-6)                 /* LlPrintWithBoxStart(...): another print job is being done, please wait or try LlPrintStart(...) */
#define LL_ERR_NOT_YET_PRINTING        (-7)                 /* LlPrintGetOptionString... */
#define LL_ERR_NO_PROJECT              (-10)                /* object with requested name does not exist (former ERR_NO_OBJECT) */
#define LL_ERR_NO_PRINTER              (-11)                /* printer couldn't be opened */
#define LL_ERR_PRINTING                (-12)                /* error while printing */
#define LL_ERR_NEEDS_VB                (-14)                /* '11...' needs VB.EXE */
#define LL_ERR_BAD_PRINTER             (-15)                /* PrintOptionsDialog(): no printer available */
#define LL_ERR_NO_PREVIEWMODE          (-16)                /* Preview functions: not in preview mode */
#define LL_ERR_NO_PREVIEWFILES         (-17)                /* PreviewDisplay(): no file found */
#define LL_ERR_PARAMETER               (-18)                /* bad parameter (usually NULL pointer) */
#define LL_ERR_BAD_EXPRESSION          (-19)                /* bad expression in LlExprEvaluate() and LlExprType() */
#define LL_ERR_BAD_EXPRMODE            (-20)                /* bad expression mode (LlSetExpressionMode()) */
#define LL_ERR_NO_TABLE                (-21)                /* not used */
#define LL_ERR_CFGNOTFOUND             (-22)                /* on LlPrintStart(), LlPrintWithBoxStart() [not found] */
#define LL_ERR_EXPRESSION              (-23)                /* on LlPrintStart(), LlPrintWithBoxStart() */
#define LL_ERR_CFGBADFILE              (-24)                /* on LlPrintStart(), LlPrintWithBoxStart() [read error, bad format] */
#define LL_ERR_BADOBJNAME              (-25)                /* on LlPrintEnableObject() - not a ':' at the beginning */
#define LL_ERR_NOOBJECT                (-26)                /* on LlPrintEnableObject() - "*" and no object in project */
#define LL_ERR_UNKNOWNOBJECT           (-27)                /* on LlPrintEnableObject() - object with that name not existing */
#define LL_ERR_NO_TABLEOBJECT          (-28)                /* LlPrint...Start() and no list in Project, or: */
#define LL_ERR_NO_OBJECT               (-29)                /* LlPrint...Start() and no object in project */
#define LL_ERR_NO_TEXTOBJECT           (-30)                /* LlPrintGetTextCharsPrinted() and no printable text in Project! */
#define LL_ERR_UNKNOWN                 (-31)                /* LlPrintIsVariableUsed(), LlPrintIsFieldUsed() */
#define LL_ERR_BAD_MODE                (-32)                /* LlPrintFields(), LlPrintIsFieldUsed() called on non-OBJECT_LIST */
#define LL_ERR_CFGBADMODE              (-33)                /* on LlDefineLayout(), LlPrint...Start(): file is in wrong expression mode */
#define LL_ERR_ONLYWITHONETABLE        (-34)                /* on LlDefinePageSeparation(), LlDefineGrouping() */
#define LL_ERR_UNKNOWNVARIABLE         (-35)                /* on LlGetVariableContents() */
#define LL_ERR_UNKNOWNFIELD            (-36)                /* on LlGetFieldContents() */
#define LL_ERR_UNKNOWNSORTORDER        (-37)                /* on LlGetFieldContents() */
#define LL_ERR_NOPRINTERCFG            (-38)                /* on LlPrintCopyPrinterConfiguration() - no or bad file */
#define LL_ERR_SAVEPRINTERCFG          (-39)                /* on LlPrintCopyPrinterConfiguration() - file could not be saved */
#define LL_ERR_RESERVED                (-40)                /* function not yet implemeted */
#define LL_ERR_NOVALIDPAGES            (-41)                /* could also be that 16 bit Viewer tries to open 32bit-only storage */
#define LL_ERR_NOTINHOSTPRINTERMODE    (-42)                /* cannot be done in Host Printer Mode (LlSetPrinterInPrinterFile()) */
#define LL_ERR_NOTFINISHED             (-43)                /* appears when a project reset() is done, but the table not finished */
#define LL_ERR_BUFFERTOOSMALL          (-44)                /* LlXXGetOptionStr() */
#define LL_ERR_BADCODEPAGE             (-45)                /* LL_OPTION_CODEPAGE */
#define LL_ERR_CANNOTCREATETEMPFILE    (-46)                /* cannot create temporary file */
#define LL_ERR_NODESTINATION           (-47)                /* no valid export destination */
#define LL_ERR_NOCHART                 (-48)                /* no chart control present */
#define LL_ERR_USER_ABORTED            (-99)                /* user aborted printing */
#define LL_ERR_BAD_DLLS                (-100)               /* DLLs not up to date (CTL, DWG, UTIL) */
#define LL_ERR_NO_LANG_DLL             (-101)               /* no or out-of-date language resource DLL */
#define LL_ERR_NO_MEMORY               (-102)               /* out of memory */
#define LL_ERR_EXCEPTION               (-104)               /* there was a GPF during the API execution. Any action that follows might cause problems! */
#define LL_ERR_LICENSEVIOLATION        (-105)               /* your license does not allow this call (see LL_OPTIONSTR_LICENSINGINFO) */
#define LL_WRN_PRINTFINISHED           (-997)               /* LlRTFDisplay() */
#define LL_WRN_REPEAT_DATA             (-998)               /* notification: page is full, prepare for next page */
#define LL_CHAR_TEXTQUOTE              (1)                 
#define LL_CHAR_PHANTOMSPACE           (2)                 
#define LL_CHAR_LOCK                   (3)                 
#define LL_CHAR_NEWLINE                (182)                /* "�" */
#define LL_CHAR_EXPRSEP                (164)                /* "�" */
#define LL_CHAR_TAB                    (247)                /* "�" */
#define LL_CHAR_EAN128NUL              (255)               
#define LL_CHAR_EAN128FNC1             (254)               
#define LL_CHAR_EAN128FNC2             (253)               
#define LL_CHAR_EAN128FNC3             (252)               
#define LL_CHAR_EAN128FNC4             (251)               
#define LL_CHAR_CODE93NUL              (255)               
#define LL_CHAR_CODE93EXDOLLAR         (254)               
#define LL_CHAR_CODE93EXPERC           (253)               
#define LL_CHAR_CODE93EXSLASH          (252)               
#define LL_CHAR_CODE93EXPLUS           (251)               
#define LL_DLGEXPR_VAREXTBTN_ENABLE    (0x00000001)         /* callback for simple Wizard extension */
#define LL_DLGEXPR_VAREXTBTN_DOMODAL   (0x00000002)        
#define LL_LLX_EXTENSIONTYPE_EXPORT    (1)                 
#define LL_LLX_EXTENSIONTYPE_BARCODE   (2)                 
#define LL_LLX_EXTENSIONTYPE_OBJECT    (3)                  /* nyi */
#define LL_LLX_EXTENSIONTYPE_WIZARD    (4)                  /* nyi */
#define LL_DECLARECHARTROW_FOR_OBJECTS (0x00000001)        
#define LL_DECLARECHARTROW_FOR_TABLECOLUMNS (0x00000002)         /* body only */
#define LL_DECLARECHARTROW_FOR_TABLECOLUMNS_FOOTERS (0x00000004)        
#define LL_GETCHARTOBJECTCOUNT_CHARTOBJECTS (1)                 
#define LL_GETCHARTOBJECTCOUNT_CHARTOBJECTS_BEFORE_TABLE (2)                 
#define LL_GETCHARTOBJECTCOUNT_CHARTCOLUMNS (3)                  /* body only */
#define LL_GETCHARTOBJECTCOUNT_CHARTCOLUMNS_FOOTERS (4)                 
#define LL_GRIPT_DIM_SCM               (1)                 
#define LL_GRIPT_DIM_PERC              (2)                 

/*--- function declaration ---*/

#ifndef RC_INVOKED

#ifdef __ZTC__ /* Zortech C++ */
#pragma ZTC align 1
#elif __WATCOMC__ > 1000 /* Watcom C++ >= 10.5 */
#pragma pack(push,1)
#elif __BORLANDC__ /* Borland C++ */
#pragma option -a1
#else
#pragma pack(1) /* MS, Watcom <= 10.0, ... */
#endif

#ifdef __cplusplus
extern "C" {
#endif

 #ifndef IMPLEMENTATION
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  LPARAM _lParam;    // parameter (most likely address of structure)
  LPARAM _lReply;    // reply (defaults to 0)
   UINT32   _lUserParameter;        // user parameter
  } scLlCallback, FAR *PSCLLCALLBACK;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  HWND _hWnd;     // L&L mainframe wnd
  UINT _nTotal;    // total count of objects
  UINT _nCurrent;    // current object number (0=start,...,total=end)
  UINT _nJob;     // LL_METERINFO_... constants
  } scLlMeterInfo, FAR *PSCLLMETERINFO;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  LPCSTR _pszContents;   // contents of the parameter string (do not overwrite!!!)
  BOOL _bEvaluate;    // TRUE on evaluation, FALSE on syntax check (fill _bError, _szError)!
   CHAR _szNewValue[0x4000+1]; // new value
   UINT _bError;    // FALSE
  CHAR _szError[128];   // error text
  } scLlExtFctA;
   typedef struct
    {
    UINT    _nSize;    
    LPCWSTR _pszContents;   
    BOOL   _bEvaluate;   
     WCHAR   _szNewValue[0x4000+1];
     UINT   _bError;     
    WCHAR   _szError[128];   
    } scLlExtFctW;
   #ifdef UNICODE
     typedef scLlExtFctW scLlExtFct, FAR* PSCLLEXTFCT;
    #else
     typedef scLlExtFctA scLlExtFct, FAR* PSCLLEXTFCT;
 #endif
 
 typedef struct     // internal use (same struct as BASIC struct)
  {
  UINT  _nSize;     // size of the structure
  LPVOID _pszContents;   // contents of the parameter string (do not overwrite!!!)
  BOOL _bEvaluate;    // TRUE on evaluation, FALSE on syntax check (fill _bError, _szError)!
   LPVOID _szNewValue;   // new value
   UINT _bError;    // FALSE
  LPVOID _szError;    // error text
  } scLlExtFctXXX, FAR *PSCLLEXTFCTXXX;
 
 #if defined(USE_OCX)
  typedef struct     // internal use (same struct as BASIC struct)
  {
  UINT  _nSize;     // size of the structure
  LPVOID _pszContents;   // contents of the parameter string (do not overwrite!!!)
  BOOL _bEvaluate;    // TRUE on evaluation, FALSE on syntax check (fill _bError, _szError)!
  BSTR _szNewValue;   // new value
  UINT _bError;    // FALSE
  BSTR _szError;    // error text
  } scLlExtFctXXX_B, FAR *PSCLLEXTFCTXXX_B;
 #endif
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  LPCSTR _pszName;    // name of the object
  INT  _nType;     // see LL_OBJ_... values
  BOOL _bPreDraw;    // TRUE on call before draw, FALSE on call afterwards
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
  } scLlObjectA;
   typedef struct
    {
    UINT  _nSize;     
    LPCWSTR _pszName;    
    INT _nType;     
    BOOL _bPreDraw;    
    HDC _hRefDC;      
    HDC _hPaintDC;     
    RECT _rcPaint;    
    } scLlObjectW;
   #ifdef UNICODE
     typedef scLlObjectW scLlObject, FAR* PSCLLOBJECT;
    #else
     typedef scLlObjectA scLlObject, FAR* PSCLLOBJECT;
   #endif
 
 #if defined(USE_OCX)
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  BSTR _pszName;    // name of the object
  INT  _nType;     // see LL_OBJ_... values
  BOOL _bPreDraw;    // TRUE on call before draw, FALSE on call afterwards
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
  } scLlObject_B, FAR *PSCLLOBJECT_B;
 #endif
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  BOOL _bDesignerPreview;  // flag if in Preview Mode or not
  BOOL _bPreDraw;    // TRUE on call before draw, FALSE on call afterwards
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
   } scLlPage, FAR *PSCLLPAGE;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  BOOL _bDesignerPreview;  // flag if in Preview Mode or not
  BOOL _bPreDraw;    // TRUE on call before draw, FALSE on call afterwards
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
   } scLlProject, FAR *PSCLLPROJECT;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  LPCSTR _pszName;    // name of the variable
  LPCSTR _pszContents;   // contents of the variable (valid if def�d by VariableExt())
  INT32 _lPara;     // lPara of the variable
  LPVOID _lpPtr;     // lpPtr of the variable
  HANDLE _hPara;     // hContents of the variable (valid if def�d by VariableExtHandle())
  BOOL _bIsotropic;   // "Isotropic" flag
  LPCSTR _pszParameters;   // for editable USERDWG: parameters
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
   INT  _nPaintMode;   // 0: Preview, 1: FastPreview, 2: Workspace (NYI)
  } scLlDrawUserObjA;
   typedef struct
    {
    UINT   _nSize;     
    LPCWSTR _pszName;   
    LPCWSTR _pszContents;  
    INT32  _lPara;     
    LPVOID _lpPtr;    
    HANDLE _hPara;    
    BOOL  _bIsotropic;   
    LPCWSTR _pszParameters;  
    HDC  _hRefDC;     
    HDC  _hPaintDC;    
    RECT  _rcPaint;    
     INT  _nPaintMode;  
    } scLlDrawUserObjW;
   #ifdef UNICODE
     typedef scLlDrawUserObjW scLlDrawUserObj, FAR* PSCLLDRAWUSEROBJ;
    #else
     typedef scLlDrawUserObjA scLlDrawUserObj, FAR* PSCLLDRAWUSEROBJ;
   #endif
 
 #if defined(USE_OCX)
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  BSTR _pszName;    // name of the variable
  BSTR _pszContents;   // contents of the variable (valid if def�d by VariableExt())
  INT32 _lPara;     // lPara of the variable
  LPVOID _lpPtr;     // lpPtr of the variable
  HANDLE _hPara;     // hContents of the variable (valid if def�d by VariableExtHandle())
  BOOL _bIsotropic;   // "Isotropic" flag
  BSTR _pszParameters;  // for editable USERDWG: parameters
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
   INT  _nPaintMode;   // 0: Preview, 1: FastPreview, 2: Workspace (NYI)
  } scLlDrawUserObj_B, FAR *PSCLLDRAWUSEROBJ_B;
 #endif
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  LPCSTR _pszName;    // name of the variable
  INT32 _lPara;     // lPara of the variable
  LPVOID _lpPtr;     // lpPtr of the variable
  HANDLE _hPara;     // hContents of the variable (valid if def�d by VariableExtHandle())
  BOOL _bIsotropic;   // "Isotropic" flag
  HWND _hWnd;     // parent window for dialog
  LPSTR _pszParameters;   // parameter buffer
  INT  _nParaBufSize;   // max size of buffer (1024+1)
  } scLlEditUserObjA;
   typedef struct
    {
    UINT     _nSize;    
    LPCWSTR _pszName;    
    INT32  _lPara;      
    LPVOID _lpPtr;     
    HANDLE _hPara;     
    BOOL  _bIsotropic;    
     HWND  _hWnd;      
     LPWSTR _pszParameters;    
     INT  _nParaBufSize;   
    } scLlEditUserObjW;
   #ifdef UNICODE
     typedef scLlEditUserObjW scLlEditUserObj, FAR* PSCLLEDITUSEROBJ;
    #else
     typedef scLlEditUserObjA scLlEditUserObj, FAR* PSCLLEDITUSEROBJ;
   #endif
 
 #if defined(USE_OCX)
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  BSTR _pszName;    // name of the variable
  INT32 _lPara;     // lPara of the variable
  LPVOID _lpPtr;     // lpPtr of the variable
  HANDLE _hPara;     // hContents of the variable (valid if def�d by VariableExtHandle())
  BOOL _bIsotropic;   // "Isotropic" flag
  HWND _hWnd;     // parent window for dialog
  BSTR _pszParameters;   // parameter buffer
  INT  _nParaBufSize;   // max size of buffer (1024+1)
  } scLlEditUserObj_B, FAR *PSCLLEDITUSEROBJ_B;
 #endif
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  INT  _nType;     // LL_TABLE_LINE_xxx constant
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
  INT  _nPageLine;       // number of line on page
  INT  _nLine;     // absolute number of line in table
  INT  _nLineDef;    // table line definition
  BOOL _bZebra;    // zebra mode selected by user (only body line!)
  RECT _rcSpacing;    // spaces around the frame
  } scLlTableLine, FAR *PSCLLTABLELINE;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  INT  _nType;     // LL_TABLE_FIELD_xxx constant
  HDC  _hRefDC;      // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // RECT in paint units (mm/10 or inch/100)
  INT  _nLineDef;    // table line definition
  INT  _nIndex;       // column index (0..)
  RECT _rcSpacing;    // spaces around the frame
  } scLlTableField, FAR *PSCLLTABLEFIELD;
 
 typedef struct
   {
  UINT  _nSize;     // size of the structure
  INT  _xPos;     // column position (paint units)
  INT  _nWidth;    // width (paint units)
  BOOL _bSepLeft;    // frame on left side selected?
  HFONT _fntColumnX;   // column font (NULL)
   } scLlColumn;
 
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  HDC  _hRefDC;    // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // suggested RECT in paint units (mm/10 or inch/100), change .bottom value
   HFONT _fntHeaderX;   // header default font
   HFONT _fntBodyX;    // body   default font
   HFONT _fntFooterX;   // footer default font
  INT  _nHeight;    // height of one body line (incl. spacing)
  BOOL _bPaint;    // do paint, or do just calculate?
  BOOL _bFrameLeft;   // frame on left side selected?
  BOOL _bFrameRight;   // frame on right side selected?
  INT32 _lParam;    // function argument: user parameter 1
  LPVOID _lpParam;    // function argument: user parameter 2
  INT  _nColumns;    // number of columns in this table
  PSCLLCOLUMN _lpColumns;      // pointer to an array of column info
  } scLlGroup, FAR *PSCLLGROUP;
 
 #if defined(USE_OCX)
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  HDC  _hRefDC;    // HDC to get information from
  HDC  _hPaintDC;     // HDC to paint on
  RECT _rcPaint;    // suggested RECT in paint units (mm/10 or inch/100), change .bottom value
   HFONT _fntHeader;    // header default font 
   HFONT _fntBody;    // body   default font 
   HFONT _fntFooter;    // footer default font 
  INT  _nHeight;    // height of one body line (incl. spacing)
  BOOL _bPaint;    // do paint, or do just calculate?
  BOOL _bFrameLeft;   // frame on left side selected?
  BOOL _bFrameRight;   // frame on right side selected?
  INT32 _lParam;    // function argument: user parameter 1
  BSTR _lpParam;    // function argument: user parameter 2
  INT  _nColumns;    // number of columns in this table
  PSCLLCOLUMN _lpColumns;      // pointer to an array of column info
  } scLlGroup_B, FAR *PSCLLGROUP_B;
 #endif
 
 typedef struct
  {
  UINT _nSize;     // size of the structure
  BOOL _bFirst;    // first or second printer?
   INT  _nCmd;     // i command
   HWND    _hWnd;     // i
   HDC  _hDC;     // i/o
   INT16 _nOrientation;   // i/o
  BOOL _bPhysPage;    // i/o, new L5
   UINT    _nBufSize;    // i
   LPSTR   _pszBuffer;    // i, fill
   INT     _nUniqueNumber;   // i
   INT  _nUniqueNumberCompare;  // i
   INT  _nPaperFormat;   // i/o (JobID for LL_CMND_CHANGE_DCPROPERTIES_DOC)
   INT    _xPaperSize;   // i/o, mm/10
   INT    _yPaperSize;   // i/o, mm/10
  } scLlPrinterA;
 #if WIN16
   typedef scLlPrinterA scLlPrinter, FAR* PSCLLPRINTER;
 #else
   typedef struct
    {
    UINT  _nSize;     
    BOOL  _bFirst;    
     INT  _nCmd;     
     HWND     _hWnd;     
     HDC  _hDC;     
     INT16  _nOrientation;   
    BOOL  _bPhysPage;    
     UINT     _nBufSize;    
     LPWSTR    _pszBuffer;    
     INT      _nUniqueNumber;   
     INT  _nUniqueNumberCompare;  
     INT  _nPaperFormat;   // i/o (JobID for LL_CMND_CHANGE_DCPROPERTIES_DOC)
     INT    _xPaperSize;   // i/o, mm/10
     INT    _yPaperSize;   // i/o, mm/10
    } scLlPrinterW;
   #ifdef UNICODE
     typedef scLlPrinterW scLlPrinter, FAR* PSCLLPRINTER;
    #else
     typedef scLlPrinterA scLlPrinter, FAR* PSCLLPRINTER;
   #endif
 #endif
 
 typedef LRESULT (FAR PASCAL *LLNOTIFICATION)(UINT nMessage, LPARAM lParam, UINT32 lUserParam); // use MakeProcInstance or smart callbacks
 
 typedef struct                    // for VC++
   {
   UINT32   FAR *ppStruct;
  LPVOID nIndex;
   } PTRPARAM;
 typedef struct                    // for VC++
   {
  INT  FAR *ppnID;
  INT  FAR *ppnType;
  LPVOID nIndex;
   } HELPPARAM;
 typedef struct                    // for VC++
   {
   INT     FAR *ppnFunction;
  LPVOID nIndex;
   } NTFYBTNPARAM;
 typedef struct
  {
  UINT  _nSize;     // size of the structure
  UINT _nFunction;    // function code (LL_DLGEXPR_VAREXTBTN_xxx)
   HWND _hWndDialog;   // dialog handle
  LPCSTR _pszPage;    // "CondDlgVar"
   CHAR _szValue[0x4000+1];  // new value
   UINT _bFields;    // FALSE
  UINT32 _nMask;     // LL_TEXT, ... OR LL_FOOTERFIELD...
  } scLlDlgExprVarExtA;
   typedef struct
    {
    UINT     _nSize;    
    UINT  _nFunction;    
     HWND  _hWndDialog;   
    LPCWSTR _pszPage;    
     WCHAR  _szValue[0x4000+1];  
     UINT  _bFields;    
    UINT32 _nMask;     
    } scLlDlgExprVarExtW;
   #ifdef UNICODE
     typedef scLlDlgExprVarExtW scLlDlgExprVarExt, FAR* PSCLLDLGEXPRVAREXT;
    #else
     typedef scLlDlgExprVarExtA scLlDlgExprVarExt, FAR* PSCLLDLGEXPRVAREXT;
   #endif
 
 typedef struct 
  {
  UINT _nSize;
  HWND _hWnd;
  UINT _nType;
  HMENU _hMenu;
  UINT _nParam;
  } scLlToolbar, FAR *PSCLLTOOLBAR;
 
 struct scLlDlgEditLineExParams
   {
   INT   _nSize;
   BOOL  _bEnableAskString;
  };
 struct scLlDlgEditLineExParams8
   {
   INT   _nSize;
   BOOL  _bEnableAskString;
   UINT  _bIncludeChartFields;
  };
 #endif // ifndef IMPLEMENTATION
 
 
 #ifdef _DEF_LLXINTERFACE // LL internal
   #include "lxoem.h"
 #endif // _DEF_LLXINTERFACE

#ifdef WIN16
 
 #ifdef ET_HLSTR
 typedef struct                 // for VC++
   {
  HLSTR FAR *pphlstrVar;
  HLSTR FAR *pphlstrHelpText;
  LPVOID nIndex;
   } HELPVARPARAM;
 #endif
#else  /* ifdef WIN16 */
#endif /* ifdef WIN16 */

WINL9API HLLJOB   DLLPROC  LlJobOpen
	(
	INT                  nLanguage
	);

WINL9API HLLJOB   DLLPROC  LlJobOpenLCID
	(
	_LCID                nLCID
	);

WINL9API void     DLLPROC  LlJobClose
	(
	HLLJOB               hLlJob
	);

WINL9API void     DLLPROC  LlSetDebug
	(
	INT                  nOnOff
	);

WINL9API UINT     DLLPROC  LlGetVersion
	(
	INT                  nCmd
	);

WINL9API UINT     DLLPROC  LlGetNotificationMessage
	(
	HLLJOB               hLlJob
	);

WINL9API INT      DLLPROC  LlSetNotificationMessage
	(
	HLLJOB               hLlJob,
	UINT                 nMessage
	);

WINL9API FARPROC  DLLPROC  LlSetNotificationCallback
	(
	HLLJOB               hLlJob,
	FARPROC              lpfnNotify
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineField LlDefineFieldW
       #define LlDefineFieldO LlDefineFieldA
     #else  // not UNICODE
       #define LlDefineField LlDefineFieldA
       #define LlDefineFieldO LlDefineFieldW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineFieldA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               lpbufContents
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineFieldW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              lpbufContents
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineFieldExt LlDefineFieldExtW
       #define LlDefineFieldExtO LlDefineFieldExtA
     #else  // not UNICODE
       #define LlDefineFieldExt LlDefineFieldExtA
       #define LlDefineFieldExtO LlDefineFieldExtW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineFieldExtA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               lpbufContents,
	INT32                lPara,
	LPVOID               lpPtr
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineFieldExtW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              lpbufContents,
	INT32                lPara,
	LPVOID               lpPtr
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineFieldExtHandle LlDefineFieldExtHandleW
       #define LlDefineFieldExtHandleO LlDefineFieldExtHandleA
     #else  // not UNICODE
       #define LlDefineFieldExtHandle LlDefineFieldExtHandleA
       #define LlDefineFieldExtHandleO LlDefineFieldExtHandleW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineFieldExtHandleA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	HANDLE               hContents,
	INT32                lPara,
	LPVOID               lpPtr
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineFieldExtHandleW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	HANDLE               hContents,
	INT32                lPara,
	LPVOID               lpPtr
	);
#endif // WIN32

WINL9API void     DLLPROC  LlDefineFieldStart
	(
	HLLJOB               hLlJob
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineVariable LlDefineVariableW
       #define LlDefineVariableO LlDefineVariableA
     #else  // not UNICODE
       #define LlDefineVariable LlDefineVariableA
       #define LlDefineVariableO LlDefineVariableW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineVariableA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               lpbufContents
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineVariableW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              lpbufContents
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineVariableExt LlDefineVariableExtW
       #define LlDefineVariableExtO LlDefineVariableExtA
     #else  // not UNICODE
       #define LlDefineVariableExt LlDefineVariableExtA
       #define LlDefineVariableExtO LlDefineVariableExtW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineVariableExtA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               lpbufContents,
	INT32                lPara,
	LPVOID               lpPtr
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineVariableExtW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              lpbufContents,
	INT32                lPara,
	LPVOID               lpPtr
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineVariableExtHandle LlDefineVariableExtHandleW
       #define LlDefineVariableExtHandleO LlDefineVariableExtHandleA
     #else  // not UNICODE
       #define LlDefineVariableExtHandle LlDefineVariableExtHandleA
       #define LlDefineVariableExtHandleO LlDefineVariableExtHandleW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineVariableExtHandleA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	HANDLE               hContents,
	INT32                lPara,
	LPVOID               lpPtr
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineVariableExtHandleW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	HANDLE               hContents,
	INT32                lPara,
	LPVOID               lpPtr
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineVariableName LlDefineVariableNameW
       #define LlDefineVariableNameO LlDefineVariableNameA
     #else  // not UNICODE
       #define LlDefineVariableName LlDefineVariableNameA
       #define LlDefineVariableNameO LlDefineVariableNameW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineVariableNameA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineVariableNameW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName
	);
#endif // WIN32

WINL9API void     DLLPROC  LlDefineVariableStart
	(
	HLLJOB               hLlJob
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineSumVariable LlDefineSumVariableW
       #define LlDefineSumVariableO LlDefineSumVariableA
     #else  // not UNICODE
       #define LlDefineSumVariable LlDefineSumVariableA
       #define LlDefineSumVariableO LlDefineSumVariableW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineSumVariableA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               lpbufContents
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineSumVariableW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              lpbufContents
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineLayout LlDefineLayoutW
       #define LlDefineLayoutO LlDefineLayoutA
     #else  // not UNICODE
       #define LlDefineLayout LlDefineLayoutA
       #define LlDefineLayoutO LlDefineLayoutW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineLayoutA
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCSTR               pszTitle,
	UINT                 nObjType,
	LPCSTR               pszObjName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineLayoutW
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCWSTR              pszTitle,
	UINT                 nObjType,
	LPCWSTR              pszObjName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDlgEditLine LlDlgEditLineW
       #define LlDlgEditLineO LlDlgEditLineA
     #else  // not UNICODE
       #define LlDlgEditLine LlDlgEditLineA
       #define LlDlgEditLineO LlDlgEditLineW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDlgEditLineA
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPSTR                lpBuf,
	INT                  nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDlgEditLineW
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPWSTR               lpBuf,
	INT                  nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDlgEditLineEx LlDlgEditLineExW
       #define LlDlgEditLineExO LlDlgEditLineExA
     #else  // not UNICODE
       #define LlDlgEditLineEx LlDlgEditLineExA
       #define LlDlgEditLineExO LlDlgEditLineExW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDlgEditLineExA
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPSTR                pszBuffer,
	UINT                 nBufSize,
	UINT32               nParaTypes,
	LPCSTR               pszTitle,
	BOOL                 bTable,
	LPVOID               pvReserved
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDlgEditLineExW
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPWSTR               pszBuffer,
	UINT                 nBufSize,
	UINT32               nParaTypes,
	LPCWSTR              pszTitle,
	BOOL                 bTable,
	LPVOID               pvReserved
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPreviewSetTempPath LlPreviewSetTempPathW
       #define LlPreviewSetTempPathO LlPreviewSetTempPathA
     #else  // not UNICODE
       #define LlPreviewSetTempPath LlPreviewSetTempPathA
       #define LlPreviewSetTempPathO LlPreviewSetTempPathW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPreviewSetTempPathA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszPath
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPreviewSetTempPathW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszPath
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPreviewDeleteFiles LlPreviewDeleteFilesW
       #define LlPreviewDeleteFilesO LlPreviewDeleteFilesA
     #else  // not UNICODE
       #define LlPreviewDeleteFiles LlPreviewDeleteFilesA
       #define LlPreviewDeleteFilesO LlPreviewDeleteFilesW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPreviewDeleteFilesA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszObjName,
	LPCSTR               pszPath
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPreviewDeleteFilesW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjName,
	LPCWSTR              pszPath
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPreviewDisplay LlPreviewDisplayW
       #define LlPreviewDisplayO LlPreviewDisplayA
     #else  // not UNICODE
       #define LlPreviewDisplay LlPreviewDisplayA
       #define LlPreviewDisplayO LlPreviewDisplayW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPreviewDisplayA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszObjName,
	LPCSTR               pszPath,
	HWND                 Wnd
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPreviewDisplayW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjName,
	LPCWSTR              pszPath,
	HWND                 Wnd
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPreviewDisplayEx LlPreviewDisplayExW
       #define LlPreviewDisplayExO LlPreviewDisplayExA
     #else  // not UNICODE
       #define LlPreviewDisplayEx LlPreviewDisplayExA
       #define LlPreviewDisplayExO LlPreviewDisplayExW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPreviewDisplayExA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszObjName,
	LPCSTR               pszPath,
	HWND                 Wnd,
	UINT32               nOptions,
	LPVOID               pOptions
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPreviewDisplayExW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjName,
	LPCWSTR              pszPath,
	HWND                 Wnd,
	UINT32               nOptions,
	LPVOID               pOptions
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlPrint
	(
	HLLJOB               hLlJob
	);

WINL9API INT      DLLPROC  LlPrintAbort
	(
	HLLJOB               hLlJob
	);

WINL9API BOOL     DLLPROC  LlPrintCheckLineFit
	(
	HLLJOB               hLlJob
	);

WINL9API INT      DLLPROC  LlPrintEnd
	(
	HLLJOB               hLlJob,
	INT                  nPages
	);

WINL9API INT      DLLPROC  LlPrintFields
	(
	HLLJOB               hLlJob
	);

WINL9API INT      DLLPROC  LlPrintFieldsEnd
	(
	HLLJOB               hLlJob
	);

WINL9API INT      DLLPROC  LlPrintGetCurrentPage
	(
	HLLJOB               hLlJob
	);

WINL9API INT      DLLPROC  LlPrintGetItemsPerPage
	(
	HLLJOB               hLlJob
	);

WINL9API INT      DLLPROC  LlPrintGetItemsPerTable
	(
	HLLJOB               hLlJob
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetRemainingItemsPerTable LlPrintGetRemainingItemsPerTableW
       #define LlPrintGetRemainingItemsPerTableO LlPrintGetRemainingItemsPerTableA
     #else  // not UNICODE
       #define LlPrintGetRemainingItemsPerTable LlPrintGetRemainingItemsPerTableA
       #define LlPrintGetRemainingItemsPerTableO LlPrintGetRemainingItemsPerTableW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetRemainingItemsPerTableA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszField
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetRemainingItemsPerTableW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszField
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetRemItemsPerTable LlPrintGetRemItemsPerTableW
       #define LlPrintGetRemItemsPerTableO LlPrintGetRemItemsPerTableA
     #else  // not UNICODE
       #define LlPrintGetRemItemsPerTable LlPrintGetRemItemsPerTableA
       #define LlPrintGetRemItemsPerTableO LlPrintGetRemItemsPerTableW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetRemItemsPerTableA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszField
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetRemItemsPerTableW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszField
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlPrintGetOption
	(
	HLLJOB               hLlJob,
	INT                  nIndex
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetPrinterInfo LlPrintGetPrinterInfoW
       #define LlPrintGetPrinterInfoO LlPrintGetPrinterInfoA
     #else  // not UNICODE
       #define LlPrintGetPrinterInfo LlPrintGetPrinterInfoA
       #define LlPrintGetPrinterInfoO LlPrintGetPrinterInfoW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetPrinterInfoA
	(
	HLLJOB               hLlJob,
	LPSTR                pszPrn,
	UINT                 nPrnLen,
	LPSTR                pszPort,
	UINT                 nPortLen
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetPrinterInfoW
	(
	HLLJOB               hLlJob,
	LPWSTR               pszPrn,
	UINT                 nPrnLen,
	LPWSTR               pszPort,
	UINT                 nPortLen
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintOptionsDialog LlPrintOptionsDialogW
       #define LlPrintOptionsDialogO LlPrintOptionsDialogA
     #else  // not UNICODE
       #define LlPrintOptionsDialog LlPrintOptionsDialogA
       #define LlPrintOptionsDialogO LlPrintOptionsDialogW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintOptionsDialogA
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCSTR               pszText
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintOptionsDialogW
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCWSTR              pszText
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlPrintSelectOffsetEx
	(
	HLLJOB               hLlJob,
	HWND                 hWnd
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintSetBoxText LlPrintSetBoxTextW
       #define LlPrintSetBoxTextO LlPrintSetBoxTextA
     #else  // not UNICODE
       #define LlPrintSetBoxText LlPrintSetBoxTextA
       #define LlPrintSetBoxTextO LlPrintSetBoxTextW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintSetBoxTextA
	(
	HLLJOB               hLlJob,
	LPCSTR               szText,
	INT                  nPercentage
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintSetBoxTextW
	(
	HLLJOB               hLlJob,
	LPCWSTR              szText,
	INT                  nPercentage
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlPrintSetOption
	(
	HLLJOB               hLlJob,
	INT                  nIndex,
	INT                  nValue
	);

WINL9API INT      DLLPROC  LlPrintUpdateBox
	(
	HLLJOB               hLlJob
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintStart LlPrintStartW
       #define LlPrintStartO LlPrintStartA
     #else  // not UNICODE
       #define LlPrintStart LlPrintStartA
       #define LlPrintStartO LlPrintStartW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintStartA
	(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCSTR               pszObjName,
	INT                  nPrintOptions,
	INT                  dummy
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintStartW
	(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCWSTR              pszObjName,
	INT                  nPrintOptions,
	INT                  dummy
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintWithBoxStart LlPrintWithBoxStartW
       #define LlPrintWithBoxStartO LlPrintWithBoxStartA
     #else  // not UNICODE
       #define LlPrintWithBoxStart LlPrintWithBoxStartA
       #define LlPrintWithBoxStartO LlPrintWithBoxStartW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintWithBoxStartA
	(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCSTR               pszObjName,
	INT                  nPrintOptions,
	INT                  nBoxType,
	HWND                 hWnd,
	LPCSTR               pszTitle
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintWithBoxStartW
	(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCWSTR              pszObjName,
	INT                  nPrintOptions,
	INT                  nBoxType,
	HWND                 hWnd,
	LPCWSTR              pszTitle
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrinterSetup LlPrinterSetupW
       #define LlPrinterSetupO LlPrinterSetupA
     #else  // not UNICODE
       #define LlPrinterSetup LlPrinterSetupA
       #define LlPrinterSetupO LlPrinterSetupW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrinterSetupA
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	UINT                 nObjType,
	LPCSTR               pszObjName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrinterSetupW
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	UINT                 nObjType,
	LPCWSTR              pszObjName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlSelectFileDlgTitleEx LlSelectFileDlgTitleExW
       #define LlSelectFileDlgTitleExO LlSelectFileDlgTitleExA
     #else  // not UNICODE
       #define LlSelectFileDlgTitleEx LlSelectFileDlgTitleExA
       #define LlSelectFileDlgTitleExO LlSelectFileDlgTitleExW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSelectFileDlgTitleExA
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCSTR               pszTitle,
	UINT                 nObjType,
	LPSTR                pszObjName,
	UINT                 nBufSize,
	LPVOID               pReserved
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSelectFileDlgTitleExW
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCWSTR              pszTitle,
	UINT                 nObjType,
	LPWSTR               pszObjName,
	UINT                 nBufSize,
	LPVOID               pReserved
	);
#endif // WIN32

WINL9API void     DLLPROC  LlSetDlgboxMode
	(
	UINT                 nMode
	);

WINL9API UINT     DLLPROC  LlGetDlgboxMode
	(
	void
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlExprParse LlExprParseW
       #define LlExprParseO LlExprParseA
     #else  // not UNICODE
       #define LlExprParse LlExprParseA
       #define LlExprParseO LlExprParseW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API HLLEXPR  DLLPROC  LlExprParseA
	(
	HLLJOB               hLlJob,
	LPCSTR               lpExprText,
	BOOL                 bIncludeFields
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API HLLEXPR  DLLPROC  LlExprParseW
	(
	HLLJOB               hLlJob,
	LPCWSTR              lpExprText,
	BOOL                 bIncludeFields
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlExprType
	(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlExprError LlExprErrorW
       #define LlExprErrorO LlExprErrorA
     #else  // not UNICODE
       #define LlExprError LlExprErrorA
       #define LlExprErrorO LlExprErrorW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API void     DLLPROC  LlExprErrorA
	(
	HLLJOB               hLlJob,
	LPSTR                pszBuf,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API void     DLLPROC  LlExprErrorW
	(
	HLLJOB               hLlJob,
	LPWSTR               pszBuf,
	UINT                 nBufSize
	);
#endif // WIN32

WINL9API void     DLLPROC  LlExprFree
	(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlExprEvaluate LlExprEvaluateW
       #define LlExprEvaluateO LlExprEvaluateA
     #else  // not UNICODE
       #define LlExprEvaluate LlExprEvaluateA
       #define LlExprEvaluateO LlExprEvaluateW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlExprEvaluateA
	(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr,
	LPSTR                pszBuf,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlExprEvaluateW
	(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr,
	LPWSTR               pszBuf,
	UINT                 nBufSize
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlSetOption
	(
	HLLJOB               hLlJob,
	INT                  nMode,
	UINT32               nValue
	);

WINL9API UINT32   DLLPROC  LlGetOption
	(
	HLLJOB               hLlJob,
	INT                  nMode
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlSetOptionString LlSetOptionStringW
       #define LlSetOptionStringO LlSetOptionStringA
     #else  // not UNICODE
       #define LlSetOptionString LlSetOptionStringA
       #define LlSetOptionStringO LlSetOptionStringW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSetOptionStringA
	(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPCSTR               pszBuffer
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSetOptionStringW
	(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPCWSTR              pszBuffer
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlGetOptionString LlGetOptionStringW
       #define LlGetOptionStringO LlGetOptionStringA
     #else  // not UNICODE
       #define LlGetOptionString LlGetOptionStringA
       #define LlGetOptionStringO LlGetOptionStringW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetOptionStringA
	(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetOptionStringW
	(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintSetOptionString LlPrintSetOptionStringW
       #define LlPrintSetOptionStringO LlPrintSetOptionStringA
     #else  // not UNICODE
       #define LlPrintSetOptionString LlPrintSetOptionStringA
       #define LlPrintSetOptionStringO LlPrintSetOptionStringW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintSetOptionStringA
	(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPCSTR               pszBuffer
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintSetOptionStringW
	(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPCWSTR              pszBuffer
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetOptionString LlPrintGetOptionStringW
       #define LlPrintGetOptionStringO LlPrintGetOptionStringA
     #else  // not UNICODE
       #define LlPrintGetOptionString LlPrintGetOptionStringA
       #define LlPrintGetOptionStringO LlPrintGetOptionStringW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetOptionStringA
	(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetOptionStringW
	(
	HLLJOB               hLlJob,
	INT                  nIndex,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlDesignerProhibitAction
	(
	HLLJOB               hLlJob,
	INT                  nMenuID
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDesignerProhibitFunction LlDesignerProhibitFunctionW
       #define LlDesignerProhibitFunctionO LlDesignerProhibitFunctionA
     #else  // not UNICODE
       #define LlDesignerProhibitFunction LlDesignerProhibitFunctionA
       #define LlDesignerProhibitFunctionO LlDesignerProhibitFunctionW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDesignerProhibitFunctionA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszFunction
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDesignerProhibitFunctionW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszFunction
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintEnableObject LlPrintEnableObjectW
       #define LlPrintEnableObjectO LlPrintEnableObjectA
     #else  // not UNICODE
       #define LlPrintEnableObject LlPrintEnableObjectA
       #define LlPrintEnableObjectO LlPrintEnableObjectW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintEnableObjectA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszObjectName,
	BOOL                 bEnable
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintEnableObjectW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjectName,
	BOOL                 bEnable
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlSetFileExtensions LlSetFileExtensionsW
       #define LlSetFileExtensionsO LlSetFileExtensionsA
     #else  // not UNICODE
       #define LlSetFileExtensions LlSetFileExtensionsA
       #define LlSetFileExtensionsO LlSetFileExtensionsW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSetFileExtensionsA
	(
	HLLJOB               hLlJob,
	INT                  nObjType,
	LPCSTR               pszObjectExt,
	LPCSTR               pszPrinterExt,
	LPCSTR               pszSketchExt
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSetFileExtensionsW
	(
	HLLJOB               hLlJob,
	INT                  nObjType,
	LPCWSTR              pszObjectExt,
	LPCWSTR              pszPrinterExt,
	LPCWSTR              pszSketchExt
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetTextCharsPrinted LlPrintGetTextCharsPrintedW
       #define LlPrintGetTextCharsPrintedO LlPrintGetTextCharsPrintedA
     #else  // not UNICODE
       #define LlPrintGetTextCharsPrinted LlPrintGetTextCharsPrintedA
       #define LlPrintGetTextCharsPrintedO LlPrintGetTextCharsPrintedW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetTextCharsPrintedA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszObjectName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetTextCharsPrintedW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjectName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetFieldCharsPrinted LlPrintGetFieldCharsPrintedW
       #define LlPrintGetFieldCharsPrintedO LlPrintGetFieldCharsPrintedA
     #else  // not UNICODE
       #define LlPrintGetFieldCharsPrinted LlPrintGetFieldCharsPrintedA
       #define LlPrintGetFieldCharsPrintedO LlPrintGetFieldCharsPrintedW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetFieldCharsPrintedA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszObjectName,
	LPCSTR               pszField
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetFieldCharsPrintedW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjectName,
	LPCWSTR              pszField
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintIsVariableUsed LlPrintIsVariableUsedW
       #define LlPrintIsVariableUsedO LlPrintIsVariableUsedA
     #else  // not UNICODE
       #define LlPrintIsVariableUsed LlPrintIsVariableUsedA
       #define LlPrintIsVariableUsedO LlPrintIsVariableUsedW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintIsVariableUsedA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintIsVariableUsedW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintIsFieldUsed LlPrintIsFieldUsedW
       #define LlPrintIsFieldUsedO LlPrintIsFieldUsedA
     #else  // not UNICODE
       #define LlPrintIsFieldUsed LlPrintIsFieldUsedA
       #define LlPrintIsFieldUsedO LlPrintIsFieldUsedW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintIsFieldUsedA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszFieldName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintIsFieldUsedW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszFieldName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintOptionsDialogTitle LlPrintOptionsDialogTitleW
       #define LlPrintOptionsDialogTitleO LlPrintOptionsDialogTitleA
     #else  // not UNICODE
       #define LlPrintOptionsDialogTitle LlPrintOptionsDialogTitleA
       #define LlPrintOptionsDialogTitleO LlPrintOptionsDialogTitleW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintOptionsDialogTitleA
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCSTR               pszTitle,
	LPCSTR               pszText
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintOptionsDialogTitleW
	(
	HLLJOB               hLlJob,
	HWND                 hWnd,
	LPCWSTR              pszTitle,
	LPCWSTR              pszText
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlSetPrinterToDefault LlSetPrinterToDefaultW
       #define LlSetPrinterToDefaultO LlSetPrinterToDefaultA
     #else  // not UNICODE
       #define LlSetPrinterToDefault LlSetPrinterToDefaultA
       #define LlSetPrinterToDefaultO LlSetPrinterToDefaultW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSetPrinterToDefaultA
	(
	HLLJOB               hLlJob,
	INT                  nObjType,
	LPCSTR               pszObjName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSetPrinterToDefaultW
	(
	HLLJOB               hLlJob,
	INT                  nObjType,
	LPCWSTR              pszObjName
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlDefineSortOrderStart
	(
	HLLJOB               hLlJob
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineSortOrder LlDefineSortOrderW
       #define LlDefineSortOrderO LlDefineSortOrderA
     #else  // not UNICODE
       #define LlDefineSortOrder LlDefineSortOrderA
       #define LlDefineSortOrderO LlDefineSortOrderW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineSortOrderA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszIdentifier,
	LPCSTR               pszText
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineSortOrderW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszIdentifier,
	LPCWSTR              pszText
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetSortOrder LlPrintGetSortOrderW
       #define LlPrintGetSortOrderO LlPrintGetSortOrderA
     #else  // not UNICODE
       #define LlPrintGetSortOrder LlPrintGetSortOrderA
       #define LlPrintGetSortOrderO LlPrintGetSortOrderW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetSortOrderA
	(
	HLLJOB               hLlJob,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetSortOrderW
	(
	HLLJOB               hLlJob,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineGrouping LlDefineGroupingW
       #define LlDefineGroupingO LlDefineGroupingA
     #else  // not UNICODE
       #define LlDefineGrouping LlDefineGroupingA
       #define LlDefineGroupingO LlDefineGroupingW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineGroupingA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszSortorder,
	LPCSTR               pszIdentifier,
	LPCSTR               pszText
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineGroupingW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszSortorder,
	LPCWSTR              pszIdentifier,
	LPCWSTR              pszText
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetGrouping LlPrintGetGroupingW
       #define LlPrintGetGroupingO LlPrintGetGroupingA
     #else  // not UNICODE
       #define LlPrintGetGrouping LlPrintGetGroupingA
       #define LlPrintGetGroupingO LlPrintGetGroupingW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetGroupingA
	(
	HLLJOB               hLlJob,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetGroupingW
	(
	HLLJOB               hLlJob,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlAddCtlSupport LlAddCtlSupportW
       #define LlAddCtlSupportO LlAddCtlSupportA
     #else  // not UNICODE
       #define LlAddCtlSupport LlAddCtlSupportA
       #define LlAddCtlSupportO LlAddCtlSupportW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlAddCtlSupportA
	(
	HWND                 hWnd,
	UINT32               nFlags,
	LPCSTR               pszInifile
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlAddCtlSupportW
	(
	HWND                 hWnd,
	UINT32               nFlags,
	LPCWSTR              pszInifile
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlPrintBeginGroup
	(
	HLLJOB               hLlJob,
	LPARAM               lParam,
	LPVOID               lpParam
	);

WINL9API INT      DLLPROC  LlPrintEndGroup
	(
	HLLJOB               hLlJob,
	LPARAM               lParam,
	LPVOID               lpParam
	);

WINL9API INT      DLLPROC  LlPrintGroupLine
	(
	HLLJOB               hLlJob,
	LPARAM               lParam,
	LPVOID               lpParam
	);

WINL9API INT      DLLPROC  LlPrintGroupHeader
	(
	HLLJOB               hLlJob,
	LPARAM               lParam
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetFilterExpression LlPrintGetFilterExpressionW
       #define LlPrintGetFilterExpressionO LlPrintGetFilterExpressionA
     #else  // not UNICODE
       #define LlPrintGetFilterExpression LlPrintGetFilterExpressionA
       #define LlPrintGetFilterExpressionO LlPrintGetFilterExpressionW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetFilterExpressionA
	(
	HLLJOB               hLlJob,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetFilterExpressionW
	(
	HLLJOB               hLlJob,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlPrintWillMatchFilter
	(
	HLLJOB               hLlJob
	);

WINL9API INT      DLLPROC  LlPrintDidMatchFilter
	(
	HLLJOB               hLlJob
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlGetFieldContents LlGetFieldContentsW
       #define LlGetFieldContentsO LlGetFieldContentsA
     #else  // not UNICODE
       #define LlGetFieldContents LlGetFieldContentsA
       #define LlGetFieldContentsO LlGetFieldContentsW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetFieldContentsA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetFieldContentsW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlGetVariableContents LlGetVariableContentsW
       #define LlGetVariableContentsO LlGetVariableContentsA
     #else  // not UNICODE
       #define LlGetVariableContents LlGetVariableContentsA
       #define LlGetVariableContentsO LlGetVariableContentsW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetVariableContentsA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetVariableContentsW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlGetSumVariableContents LlGetSumVariableContentsW
       #define LlGetSumVariableContentsO LlGetSumVariableContentsA
     #else  // not UNICODE
       #define LlGetSumVariableContents LlGetSumVariableContentsA
       #define LlGetSumVariableContentsO LlGetSumVariableContentsW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetSumVariableContentsA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetSumVariableContentsW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlGetUserVariableContents LlGetUserVariableContentsW
       #define LlGetUserVariableContentsO LlGetUserVariableContentsA
     #else  // not UNICODE
       #define LlGetUserVariableContents LlGetUserVariableContentsA
       #define LlGetUserVariableContentsO LlGetUserVariableContentsW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetUserVariableContentsA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetUserVariableContentsW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlGetVariableType LlGetVariableTypeW
       #define LlGetVariableTypeO LlGetVariableTypeA
     #else  // not UNICODE
       #define LlGetVariableType LlGetVariableTypeA
       #define LlGetVariableTypeO LlGetVariableTypeW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT32    DLLPROC  LlGetVariableTypeA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT32    DLLPROC  LlGetVariableTypeW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlGetFieldType LlGetFieldTypeW
       #define LlGetFieldTypeO LlGetFieldTypeA
     #else  // not UNICODE
       #define LlGetFieldType LlGetFieldTypeA
       #define LlGetFieldTypeO LlGetFieldTypeW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT32    DLLPROC  LlGetFieldTypeA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT32    DLLPROC  LlGetFieldTypeW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetColumnInfo LlPrintGetColumnInfoW
       #define LlPrintGetColumnInfoO LlPrintGetColumnInfoA
     #else  // not UNICODE
       #define LlPrintGetColumnInfo LlPrintGetColumnInfoA
       #define LlPrintGetColumnInfoO LlPrintGetColumnInfoW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetColumnInfoA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszObjectName,
	INT                  nCol,
	PSCLLCOLUMN          pCol
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetColumnInfoW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszObjectName,
	INT                  nCol,
	PSCLLCOLUMN          pCol
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlSetPrinterDefaultsDir LlSetPrinterDefaultsDirW
       #define LlSetPrinterDefaultsDirO LlSetPrinterDefaultsDirA
     #else  // not UNICODE
       #define LlSetPrinterDefaultsDir LlSetPrinterDefaultsDirA
       #define LlSetPrinterDefaultsDirO LlSetPrinterDefaultsDirW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSetPrinterDefaultsDirA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszDir
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSetPrinterDefaultsDirW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszDir
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlCreateSketch LlCreateSketchW
       #define LlCreateSketchO LlCreateSketchA
     #else  // not UNICODE
       #define LlCreateSketch LlCreateSketchA
       #define LlCreateSketchO LlCreateSketchW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlCreateSketchA
	(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCSTR               lpszObjName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlCreateSketchW
	(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCWSTR              lpszObjName
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlViewerProhibitAction
	(
	HLLJOB               hLlJob,
	INT                  nMenuID
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintCopyPrinterConfiguration LlPrintCopyPrinterConfigurationW
       #define LlPrintCopyPrinterConfigurationO LlPrintCopyPrinterConfigurationA
     #else  // not UNICODE
       #define LlPrintCopyPrinterConfiguration LlPrintCopyPrinterConfigurationA
       #define LlPrintCopyPrinterConfigurationO LlPrintCopyPrinterConfigurationW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintCopyPrinterConfigurationA
	(
	HLLJOB               hLlJob,
	LPCSTR               lpszFilename,
	INT                  nFunction
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintCopyPrinterConfigurationW
	(
	HLLJOB               hLlJob,
	LPCWSTR              lpszFilename,
	INT                  nFunction
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlSetPrinterInPrinterFile LlSetPrinterInPrinterFileW
       #define LlSetPrinterInPrinterFileO LlSetPrinterInPrinterFileA
     #else  // not UNICODE
       #define LlSetPrinterInPrinterFile LlSetPrinterInPrinterFileA
       #define LlSetPrinterInPrinterFileO LlSetPrinterInPrinterFileW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSetPrinterInPrinterFileA
	(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCSTR               pszObjName,
	INT                  nPrinter,
	LPCSTR               pszPrinter,
	_PCDEVMODEA          pDevMode
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlSetPrinterInPrinterFileW
	(
	HLLJOB               hLlJob,
	UINT                 nObjType,
	LPCWSTR              pszObjName,
	INT                  nPrinter,
	LPCWSTR              pszPrinter,
	_PCDEVMODEW          pDevMode
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlStgsysStorageOpen LlStgsysStorageOpenW
       #define LlStgsysStorageOpenO LlStgsysStorageOpenA
     #else  // not UNICODE
       #define LlStgsysStorageOpen LlStgsysStorageOpenA
       #define LlStgsysStorageOpenO LlStgsysStorageOpenW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API HLLSTG   DLLPROC  LlStgsysStorageOpenA
	(
	LPCSTR               lpszFilename,
	LPCSTR               pszTempPath,
	BOOL                 bReadOnly,
	BOOL                 bOneJobTranslation
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API HLLSTG   DLLPROC  LlStgsysStorageOpenW
	(
	LPCWSTR              lpszFilename,
	LPCWSTR              pszTempPath,
	BOOL                 bReadOnly,
	BOOL                 bOneJobTranslation
	);
#endif // WIN32

WINL9API void     DLLPROC  LlStgsysStorageClose
	(
	HLLSTG               hStg
	);

WINL9API INT      DLLPROC  LlStgsysGetAPIVersion
	(
	HLLSTG               hStg
	);

WINL9API INT      DLLPROC  LlStgsysGetFileVersion
	(
	HLLSTG               hStg
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlStgsysGetFilename LlStgsysGetFilenameW
       #define LlStgsysGetFilenameO LlStgsysGetFilenameA
     #else  // not UNICODE
       #define LlStgsysGetFilename LlStgsysGetFilenameA
       #define LlStgsysGetFilenameO LlStgsysGetFilenameW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysGetFilenameA
	(
	HLLSTG               hStg,
	INT                  nJob,
	INT                  nFile,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysGetFilenameW
	(
	HLLSTG               hStg,
	INT                  nJob,
	INT                  nFile,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlStgsysGetJobCount
	(
	HLLSTG               hStg
	);

WINL9API INT      DLLPROC  LlStgsysSetJob
	(
	HLLSTG               hStg,
	INT                  nJob
	);

WINL9API INT      DLLPROC  LlStgsysGetPageCount
	(
	HLLSTG               hStg
	);

WINL9API INT      DLLPROC  LlStgsysGetJobOptionValue
	(
	HLLSTG               hStg,
	INT                  nOption
	);

WINL9API INT      DLLPROC  LlStgsysGetPageOptionValue
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlStgsysGetPageOptionString LlStgsysGetPageOptionStringW
       #define LlStgsysGetPageOptionStringO LlStgsysGetPageOptionStringA
     #else  // not UNICODE
       #define LlStgsysGetPageOptionString LlStgsysGetPageOptionStringA
       #define LlStgsysGetPageOptionStringO LlStgsysGetPageOptionStringW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysGetPageOptionStringA
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysGetPageOptionStringW
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlStgsysSetPageOptionString LlStgsysSetPageOptionStringW
       #define LlStgsysSetPageOptionStringO LlStgsysSetPageOptionStringA
     #else  // not UNICODE
       #define LlStgsysSetPageOptionString LlStgsysSetPageOptionStringA
       #define LlStgsysSetPageOptionStringO LlStgsysSetPageOptionStringW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysSetPageOptionStringA
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPCSTR               pszBuffer
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysSetPageOptionStringW
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPCWSTR              pszBuffer
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlStgsysAppend
	(
	HLLSTG               hStg,
	HLLSTG               hStgToAppend
	);

WINL9API INT      DLLPROC  LlStgsysDeleteJob
	(
	HLLSTG               hStg,
	INT                  nPageIndex
	);

WINL9API INT      DLLPROC  LlStgsysDeletePage
	(
	HLLSTG               hStg,
	INT                  nPageIndex
	);

WINL9API HANDLE   DLLPROC  LlStgsysGetPageMetafile
	(
	HLLSTG               hStg,
	INT                  nPageIndex
	);

WINL9API HANDLE   DLLPROC  LlStgsysGetPageMetafile16
	(
	HLLSTG               hStg,
	INT                  nPageIndex
	);

WINL9API INT      DLLPROC  LlStgsysDestroyMetafile
	(
	HANDLE               hMF
	);

WINL9API INT      DLLPROC  LlStgsysDrawPage
	(
	HLLSTG               hStg,
	HDC                  hDC,
	HDC                  hPrnDC,
	BOOL                 bAskPrinter,
	_PCRECT              pRC,
	INT                  nPageIndex,
	BOOL                 bFit,
	LPVOID               pReserved
	);

WINL9API INT      DLLPROC  LlStgsysGetLastError
	(
	HLLSTG               hStg
	);

WINL9API INT      DLLPROC  LlStgsysDeleteFiles
	(
	HLLSTG               hStg
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlStgsysPrint LlStgsysPrintW
       #define LlStgsysPrintO LlStgsysPrintA
     #else  // not UNICODE
       #define LlStgsysPrint LlStgsysPrintA
       #define LlStgsysPrintO LlStgsysPrintW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysPrintA
	(
	HLLSTG               hStg,
	LPCSTR               pszPrinterName1,
	LPCSTR               pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCSTR               pszMessage,
	HWND                 hWndParent
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysPrintW
	(
	HLLSTG               hStg,
	LPCWSTR              pszPrinterName1,
	LPCWSTR              pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCWSTR              pszMessage,
	HWND                 hWndParent
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlStgsysStoragePrint LlStgsysStoragePrintW
       #define LlStgsysStoragePrintO LlStgsysStoragePrintA
     #else  // not UNICODE
       #define LlStgsysStoragePrint LlStgsysStoragePrintA
       #define LlStgsysStoragePrintO LlStgsysStoragePrintW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysStoragePrintA
	(
	LPCSTR               lpszFilename,
	LPCSTR               pszTempPath,
	LPCSTR               pszPrinterName1,
	LPCSTR               pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCSTR               pszMessage,
	HWND                 hWndParent
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysStoragePrintW
	(
	LPCWSTR              lpszFilename,
	LPCWSTR              pszTempPath,
	LPCWSTR              pszPrinterName1,
	LPCWSTR              pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCWSTR              pszMessage,
	HWND                 hWndParent
	);
#endif // WIN32

WINL9API HLLRTFOBJ DLLPROC  LlRTFCreateObject
	(
	HLLJOB               hLlJob
	);

WINL9API INT      DLLPROC  LlRTFDeleteObject
	(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlRTFSetText LlRTFSetTextW
       #define LlRTFSetTextO LlRTFSetTextA
     #else  // not UNICODE
       #define LlRTFSetText LlRTFSetTextA
       #define LlRTFSetTextO LlRTFSetTextW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlRTFSetTextA
	(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	LPCSTR               pszText
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlRTFSetTextW
	(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	LPCWSTR              pszText
	);
#endif // WIN32

WINL9API UINT     DLLPROC  LlRTFGetTextLength
	(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	INT                  nFlags
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlRTFGetText LlRTFGetTextW
       #define LlRTFGetTextO LlRTFGetTextA
     #else  // not UNICODE
       #define LlRTFGetText LlRTFGetTextA
       #define LlRTFGetTextO LlRTFGetTextW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlRTFGetTextA
	(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	INT                  nFlags,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlRTFGetTextW
	(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	INT                  nFlags,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlRTFEditObject
	(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	HWND                 hWnd,
	HDC                  hPrnDC,
	INT                  nProjectType
	);

WINL9API INT      DLLPROC  LlRTFCopyToClipboard
	(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF
	);

WINL9API INT      DLLPROC  LlRTFDisplay
	(
	HLLJOB               hLlJob,
	HLLRTFOBJ            hRTF,
	HDC                  hDC,
	_PRECT               pRC,
	BOOL                 bRestart,
	LLPUINT              pnState
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDebugOutput LlDebugOutputW
       #define LlDebugOutputO LlDebugOutputA
     #else  // not UNICODE
       #define LlDebugOutput LlDebugOutputA
       #define LlDebugOutputO LlDebugOutputW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API void     DLLPROC  LlDebugOutputA
	(
	INT                  nIndent,
	LPCSTR               pszText
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API void     DLLPROC  LlDebugOutputW
	(
	INT                  nIndent,
	LPCWSTR              pszText
	);
#endif // WIN32

WINL9API HLISTPOS DLLPROC  LlEnumGetFirstVar
	(
	HLLJOB               hLlJob,
	UINT32               nFlags
	);

WINL9API HLISTPOS DLLPROC  LlEnumGetFirstField
	(
	HLLJOB               hLlJob,
	UINT32               nFlags
	);

WINL9API HLISTPOS DLLPROC  LlEnumGetNextEntry
	(
	HLLJOB               hLlJob,
	HLISTPOS             nPos,
	UINT32               nFlags
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlEnumGetEntry LlEnumGetEntryW
       #define LlEnumGetEntryO LlEnumGetEntryA
     #else  // not UNICODE
       #define LlEnumGetEntry LlEnumGetEntryA
       #define LlEnumGetEntryO LlEnumGetEntryW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlEnumGetEntryA
	(
	HLLJOB               hLlJob,
	HLISTPOS             nPos,
	LPSTR                pszNameBuf,
	UINT                 nNameBufSize,
	LPSTR                pszContBuf,
	UINT                 nContBufSize,
	_LPHANDLE            pHandle,
	_LPINT32             pType
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlEnumGetEntryW
	(
	HLLJOB               hLlJob,
	HLISTPOS             nPos,
	LPWSTR               pszNameBuf,
	UINT                 nNameBufSize,
	LPWSTR               pszContBuf,
	UINT                 nContBufSize,
	_LPHANDLE            pHandle,
	_LPINT32             pType
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlPrintResetObjectStates
	(
	HLLJOB               hLlJob
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlXSetParameter LlXSetParameterW
       #define LlXSetParameterO LlXSetParameterA
     #else  // not UNICODE
       #define LlXSetParameter LlXSetParameterA
       #define LlXSetParameterO LlXSetParameterW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlXSetParameterA
	(
	HLLJOB               hLlJob,
	INT                  nExtensionType,
	LPCSTR               pszExtensionName,
	LPCSTR               pszKey,
	LPCSTR               pszValue
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlXSetParameterW
	(
	HLLJOB               hLlJob,
	INT                  nExtensionType,
	LPCWSTR              pszExtensionName,
	LPCWSTR              pszKey,
	LPCWSTR              pszValue
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlXGetParameter LlXGetParameterW
       #define LlXGetParameterO LlXGetParameterA
     #else  // not UNICODE
       #define LlXGetParameter LlXGetParameterA
       #define LlXGetParameterO LlXGetParameterW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlXGetParameterA
	(
	HLLJOB               hLlJob,
	INT                  nExtensionType,
	LPCSTR               pszExtensionName,
	LPCSTR               pszKey,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlXGetParameterW
	(
	HLLJOB               hLlJob,
	INT                  nExtensionType,
	LPCWSTR              pszExtensionName,
	LPCWSTR              pszKey,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlPrintResetProjectState
	(
	HLLJOB               hJob
	);

WINL9API void     DLLPROC  LlDefineChartFieldStart
	(
	HLLJOB               hLlJob
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlDefineChartFieldExt LlDefineChartFieldExtW
       #define LlDefineChartFieldExtO LlDefineChartFieldExtA
     #else  // not UNICODE
       #define LlDefineChartFieldExt LlDefineChartFieldExtA
       #define LlDefineChartFieldExtO LlDefineChartFieldExtW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineChartFieldExtA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszVarName,
	LPCSTR               pszContents,
	INT32                lPara,
	LPVOID               lpPtr
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlDefineChartFieldExtW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszVarName,
	LPCWSTR              pszContents,
	INT32                lPara,
	LPVOID               lpPtr
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlPrintDeclareChartRow
	(
	HLLJOB               hLlJob,
	UINT                 nFlags
	);

WINL9API INT      DLLPROC  LlPrintGetChartObjectCount
	(
	HLLJOB               hLlJob,
	UINT                 nType
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintIsChartFieldUsed LlPrintIsChartFieldUsedW
       #define LlPrintIsChartFieldUsedO LlPrintIsChartFieldUsedA
     #else  // not UNICODE
       #define LlPrintIsChartFieldUsed LlPrintIsChartFieldUsedA
       #define LlPrintIsChartFieldUsedO LlPrintIsChartFieldUsedW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintIsChartFieldUsedA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszFieldName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintIsChartFieldUsedW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszFieldName
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlGetChartFieldContents LlGetChartFieldContentsW
       #define LlGetChartFieldContentsO LlGetChartFieldContentsA
     #else  // not UNICODE
       #define LlGetChartFieldContents LlGetChartFieldContentsA
       #define LlGetChartFieldContentsO LlGetChartFieldContentsW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetChartFieldContentsA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszName,
	LPSTR                pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetChartFieldContentsW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszName,
	LPWSTR               pszBuffer,
	UINT                 nBufSize
	);
#endif // WIN32

WINL9API HLISTPOS DLLPROC  LlEnumGetFirstChartField
	(
	HLLJOB               hLlJob,
	UINT32               nFlags
	);

WINL9API FARPROC  DLLPROC  LlSetNotificationCallbackExt
	(
	HLLJOB               hLlJob,
	INT                  nEvent,
	FARPROC              lpfnNotify
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlStgsysGetPagePrinter LlStgsysGetPagePrinterW
       #define LlStgsysGetPagePrinterO LlStgsysGetPagePrinterA
     #else  // not UNICODE
       #define LlStgsysGetPagePrinter LlStgsysGetPagePrinterA
       #define LlStgsysGetPagePrinterO LlStgsysGetPagePrinterW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysGetPagePrinterA
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	LPSTR                pszDeviceName,
	UINT                 nDeviceNameSize,
	PHGLOBAL             phDevMode
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlStgsysGetPagePrinterW
	(
	HLLSTG               hStg,
	INT                  nPageIndex,
	LPWSTR               pszDeviceName,
	UINT                 nDeviceNameSize,
	PHGLOBAL             phDevMode
	);
#endif // WIN32

#ifdef WIN32
WINL9API INT      DLLPROC  LlExprEvaluateVar
	(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr,
	PVARIANT             pVar
	);
#endif // WIN32

WINL9API INT      DLLPROC  LlExprTypeVar
	(
	HLLJOB               hLlJob,
	HLLEXPR              lpExpr
	);

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlGetPrinterFromPrinterFile LlGetPrinterFromPrinterFileW
       #define LlGetPrinterFromPrinterFileO LlGetPrinterFromPrinterFileA
     #else  // not UNICODE
       #define LlGetPrinterFromPrinterFile LlGetPrinterFromPrinterFileA
       #define LlGetPrinterFromPrinterFileO LlGetPrinterFromPrinterFileW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetPrinterFromPrinterFileA
	(
	HLLJOB               hJob,
	UINT                 nObjType,
	LPCSTR               pszObjectName,
	INT                  nPrinter,
	LPSTR                pszPrinter,
	LLPUINT              pnPrinterBufSize,
	_PDEVMODEA           pDevMode,
	LLPUINT              pnDevModeBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlGetPrinterFromPrinterFileW
	(
	HLLJOB               hJob,
	UINT                 nObjType,
	LPCWSTR              pszObjectName,
	INT                  nPrinter,
	LPWSTR               pszPrinter,
	LLPUINT              pnPrinterBufSize,
	_PDEVMODEW           pDevMode,
	LLPUINT              pnDevModeBufSize
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
   #ifdef UNICODE
       #define LlPrintGetRemainingSpacePerTable LlPrintGetRemainingSpacePerTableW
       #define LlPrintGetRemainingSpacePerTableO LlPrintGetRemainingSpacePerTableA
     #else  // not UNICODE
       #define LlPrintGetRemainingSpacePerTable LlPrintGetRemainingSpacePerTableA
       #define LlPrintGetRemainingSpacePerTableO LlPrintGetRemainingSpacePerTableW
   #endif // UNICODE
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetRemainingSpacePerTableA
	(
	HLLJOB               hLlJob,
	LPCSTR               pszField,
	INT                  nDimension
	);
#endif // WIN32

#if defined(WIN32) && !defined(_NO_CMBTL9APIDEFINES)
#endif // WIN32, _NO_CMBTL9APIDEFINES
#ifdef WIN32
WINL9API INT      DLLPROC  LlPrintGetRemainingSpacePerTableW
	(
	HLLJOB               hLlJob,
	LPCWSTR              pszField,
	INT                  nDimension
	);
#endif // WIN32



#ifdef __cplusplus
}
#endif

#ifdef __ZTC__ /* Zortech C++ */
#pragma ZTC align
#elif __WATCOMC__ > 1000 /* Watcom C++ */
#pragma pack(pop)
#elif __BORLANDC__ /* Borland C++ */
#pragma option -a.
#else
#pragma pack() /* MS C++ */
#endif

#endif  /* #ifndef RC_INVOKED */

#endif  /* #ifndef _L9_H */

