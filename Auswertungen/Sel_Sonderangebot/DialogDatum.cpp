// DialogDatum.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "llmfc.h"
#include "DialogDatum.h"
#include "DbClass.h"
#include "DbRange.h"
#include "datum.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
extern DB_CLASS dbClass;
DB_RANGE dbRange;
extern short movon ;
extern short mdn ;
extern short jr ;
extern long kunvon ;
extern long kunbis ;
extern short wgvon ;
extern short wgbis ;
extern double avon ;
extern double abis ;
extern short jr ;
extern short kw ;
extern char R_renner[2] ;
extern char R_penner[2] ;
extern char order_by_zusatz[7] ;
extern char order_by[11] ;
extern char R_artikel[2] ;
extern char R_menge[2] ;
extern char R_umsatz[2] ;
extern char R_ertrag[2] ;
extern long max_anz;
extern char BezRennerPenner [] ;
extern char BezSortierung [] ;
extern char kunbereich[256];
extern char filbereich[256];
extern char wgbereich[256];
extern char abereich[256];


char cdatum[12];
extern char datvon[12] ;
extern char datbis[12] ;
extern DATE_STRUCT end_dat;
bool Monatok = FALSE;
extern short woche;
extern DATUM d_dat; 

CForm *CDialogDatum::_Dialog [] = 
{
   new CForm (IDC_MANDANT, (short *)      &mdn, FRMSHORT,0, "%hd"),
   new CForm (IDC_FILIALE, (char *)      kunbereich, FRMCHAR,1, 128),
   new CForm (IDC_JAHR, (short *)      &jr, FRMSHORT,0, "%hd"),
   new CForm (IDC_KW, (short *)      &kw, FRMSHORT,0, "%hd"),
   new CForm (IDC_DATVON, (CHAR *)      datvon, FRMDATUM,0, "dd.mm.yyyy"),
   new CForm (IDC_DATBIS, (CHAR *)      datbis, FRMDATUM,0, "dd.mm.yyyy"),
   new CForm (IDC_WGVON, (char *)      wgbereich, FRMCHAR,1, 128),
   new CForm (IDC_ART_VON, (char *)      abereich, FRMCHAR,1, 128),

   new CForm (IDC_RADIO_A, (CHAR *)      R_artikel, FRMCHARBOOL,0, ""),
   new CForm (IDC_RADIO_MENGE, (CHAR *)      R_menge, FRMCHARBOOL,0, ""),
   new CForm (IDC_RADIO_UMSATZ, (CHAR *)      R_umsatz, FRMCHARBOOL,0, ""),
   new CForm (IDC_RADIO_ERTRAG, (CHAR *)      R_ertrag, FRMCHARBOOL,0, ""),

   new CForm (IDC_RADIO_AB, (CHAR *)      R_renner, FRMCHARBOOL,0, ""),
   new CForm (IDC_RADIO_AUF, (CHAR *)      R_penner, FRMCHARBOOL,0, ""),
   NULL,
};

CVector CDialogDatum::Dialog ((void **) _Dialog);


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDialogDatum 


CDialogDatum::CDialogDatum(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogDatum::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}

BOOL CDialogDatum::OnInitDialog()
{
	CDialog::OnInitDialog();
    GetDlgItem (IDC_MANDANT)->SetFocus ();
    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);

	strcpy(kunbereich,"1 bis 999999");
	strcpy(wgbereich,"1 bis 9999");
	strcpy(abereich,"1 bis 9999999999999");

	Read ();

	return TRUE;
}

void CDialogDatum::EnableDialog (BOOL b)
{


    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->Enable (this, b);
    }
	if (b)
	{
	    GetDlgItem (IDC_MANDANT)->SetFocus ();
	    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
    }
}
void CDialogDatum::ToDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->ToScreen (this);
    }
}


void CDialogDatum::FromDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();

    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->FromScreen (this);
    }
}

BOOL CDialogDatum::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
CWnd *cWnd;
if (pMsg->message == WM_KEYDOWN)
{
	switch (pMsg->wParam)
	{
	case VK_F5 :      //so gehts nicht TODO
			cWnd = GetDlgItem (IDCANCEL);
			return CDialog::PreTranslateMessage(pMsg);
	case VK_RETURN : 
	case VK_TAB :
		    cWnd = GetFocus ();
			if (cWnd ==GetDlgItem (IDC_MANDANT))
			{
			
			    Write ();
 			    GetDlgItem (IDC_FILIALE)->SetFocus ();
               ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_FILIALE))
			{
			
			    Write ();
			    GetDlgItem (IDC_JAHR)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_JAHR))
			{
			    Write ();
				Read();
			    Write ();
			    GetDlgItem (IDC_KW)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_KW))
			{
				short wo = kw;
			    Write ();

				woche = kw;
				d_dat.SetzeDatum(1,1,jr);
				d_dat.format(cdatum);
				woche = get_woche(cdatum);
				int z=0;
				while (woche != kw && z < 365)
				{
					z ++;
					++d_dat;
					d_dat.format(cdatum);
					woche = get_woche(cdatum);
				}
				if (woche == kw)
				{
					d_dat.format(datvon);
					while (woche == kw)
					{
						--d_dat;
						d_dat.format(datvon);
						woche = get_woche(datvon);
					}
					++d_dat;
					d_dat.format(datvon);
					d_dat=d_dat+6;
					d_dat.format(datbis);
				}
				else
				{
					kw = wo;
				}


				Read();
			    Write ();
			    GetDlgItem (IDC_DATVON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_DATVON))
			{
			    Write ();
				Read();
			    Write ();
			    GetDlgItem (IDC_DATBIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_DATBIS))
			{
			    Write ();
				Read();
			    Write ();
			    GetDlgItem (IDC_WGVON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_WGVON))
			{
			    Write ();
			    GetDlgItem (IDC_ART_VON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_ART_VON))
			{
			    Write ();
			    GetDlgItem (IDOK)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDOK))
			{
			    Write ();
			    GetDlgItem (IDC_MANDANT)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}

			if (cWnd ==GetDlgItem (IDCANCEL))
			{
				return CDialog::PreTranslateMessage(pMsg);
			}
			if (cWnd ==GetDlgItem (IDOK))
			{
			    Write ();
				/**
				if (Monatok == FALSE)
                { 
					GetDlgItem (IDC_MANDANT)->SetFocus ();
					((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
					return TRUE;
                }
				**/
				return CDialog::PreTranslateMessage(pMsg);
			}
		    NextDlgCtrl();
		    return TRUE;

	}
}
	return CDialog::PreTranslateMessage(pMsg);
}




BOOL CDialogDatum::Read (void)
{
    ToDialog ();
    EnableDialog (TRUE);
	return TRUE;
	
}

BOOL CDialogDatum::Write (void)
{

    FromDialog ();

return TRUE;
}

void CDialogDatum::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	Write();

	CDialog::OnOK();
}


void CDialogDatum::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogDatum, CDialog)
	//{{AFX_MSG_MAP(CDialogDatum)
	ON_BN_CLICKED(IDC_RADIO_AB, OnRadioRenner)
	ON_BN_CLICKED(IDC_RADIO_AUF, OnRadioPenner)
	ON_BN_CLICKED(IDC_RADIO_A, OnRadioA)
	ON_BN_CLICKED(IDC_RADIO_MENGE, OnRadioMenge)
	ON_BN_CLICKED(IDC_RADIO_UMSATZ, OnRadioUmsatz)
	ON_BN_CLICKED(IDC_RADIO_ERTRAG, OnRadioErtrag)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDialogDatum 

void CDialogDatum::OnChangeWoBis() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnChangeWoVon() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnCloseupCombo1() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}



void CDialogDatum::OnRadioRenner() 
{
	Write();
		if (strcmp(R_renner,"J")== 0 ) 
		{
			strcpy(order_by_zusatz," desc ") ;
			strcpy (BezRennerPenner, "Renner");
extern char BezRennerPenner [] ;
extern char BezSortierung [] ;
		}
	
}

void CDialogDatum::OnRadioPenner() 
{
	Write();
		if (strcmp(R_penner,"J")== 0 ) 
		{
			strcpy(order_by_zusatz," asc ") ;
			strcpy (BezRennerPenner, "Penner");
		}


}

void CDialogDatum::OnRadioMenge() 
{
	Write();
		if (strcmp(R_menge,"J")== 0 ) 
		{
			strcpy(order_by," menge ") ;
			strcpy(BezSortierung,"nach Kilo") ;
		}
	
}

void CDialogDatum::OnRadioErtrag() 
{
	Write();
		if (strcmp(R_ertrag,"J")== 0 ) 
		{
			strcpy(order_by," ertrag ") ;
			strcpy(BezSortierung,"nach Ertrag") ;
		}
	
}

void CDialogDatum::OnRadioUmsatz() 
{
	Write();
		if (strcmp(R_umsatz,"J")== 0 ) 
		{
			strcpy(order_by," umsatz ") ;
			strcpy(BezSortierung,"nach Umsatz") ;
		}
	
}

void CDialogDatum::OnRadioA() 
{
	Write();
		if (strcmp(R_artikel,"J")== 0 ) 
		{
			strcpy(order_by," artikel ") ;
			strcpy(BezSortierung,"nach Artikelnummer") ;
		}
	
	
}
