#include "stdafx.h"
#include "DbClass.h"
#include "DbRange.h"
#include "datum.h"
#include "a_mo_wa.h"
#include "cmbtl9.h"

struct A_MO_WA a_mo_wa, a_mo_wa_null;
struct AUSGABE ausgabe, ausgabe_null;
struct A_BAS a_bas, a_bas_null;
extern DB_CLASS dbClass;
extern DB_RANGE dbRange;
static long anzfelder = -1;
short mdn = 1;
extern char datvon[12] ;
extern char datbis[12] ;
char kunbereich[256];
char filbereich[256];
char wgbereich[256];
char abereich[256];
long max_anz = 999999;
short jr = 0;
short kw = 0;
short vjr = 0;
short vvjr = 0;
short movon ;
short monat ;
extern char order_by_zusatz[7] ;
extern char order_by[11] ;
extern char BezRennerPenner [] ;
extern char BezSortierung [] ;
extern char R_menge[2] ;
extern char R_umsatz[2] ;
extern char R_ertrag[2] ;

DATUM d_dat, d_datfirst, d_datlast;
DATE_STRUCT end_dat;
char adr_krz [25];
double a_gew;
extern char Listenname[128];
char sys_par_wrt [2] = " ";




static char *clipped (char *str)
/**
Blank am Ende eines Strings abschneiden.
**/
{
          int i;

          i = strlen (str);
          if (i == 0) return (str);

          for (i = i - 1; i > 0; i --)
          {
                    if (str[i] > ' ')
                    {
                              str [i + 1] = (char) 0;
                              break;
                    }
          }
          return (str);
}



int A_MO_WA_CLASS::dbcount (void)
/**
Tabelle eti lesen.
**/
{


         if (test_upd_cursor == -1)
         {
			dbClass.sqlout ((char *) sys_par_wrt, SQLCHAR, 2);
			int dsqlstatus = dbClass.sqlcomm ("select sys_par_wrt from sys_par where sys_par_nam = \"abwper_par\""); 
			strcpy(filbereich,kunbereich);
			if (dsqlstatus == 0)
			{
				if (atoi(sys_par_wrt) == 1) 
				{
					prepare ();
				}
				else
				{
		             prepare ();
				}
			}
			else
			{
	             prepare ();
			}


         }

     dbClass.sqlopen (count_cursor);
     sqlstatus = dbClass.sqlfetch (count_cursor);
	 if (sqlstatus != 0) return -1 ;
	 anzfelder = 0;
     while (sqlstatus == 0)
     {
		 if (max_anz < ausgabe.fil_anzahl) ausgabe.fil_anzahl = max_anz;
		 if (ausgabe.fil_menge > 0.0)
		 {
			anzfelder += ausgabe.fil_anzahl;
		 }
         sqlstatus = dbClass.sqlfetch (count_cursor);
     }
     dbClass.sqlopen (count_cursor);
	 ausgabe.fil_anzahl = 0;
     return anzfelder; 
}


int A_MO_WA_CLASS::leseDS (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }


	  if (ausgabe.fil_anzahl == 0)
	  {

		fehlercode = dbClass.sqlfetch (count_cursor);
        while (fehlercode == 0)
		{
			if (ausgabe.fil_menge > 0.0) break;
			fehlercode = dbClass.sqlfetch (count_cursor);
		}
  	    if (max_anz < ausgabe.fil_anzahl) ausgabe.fil_anzahl = max_anz;
		ausgabe.sfil_ges_menge = ausgabe.fil_ges_menge ;  //nur zum Summieren im LL
		ausgabe.sfil_ges_umsatz = ausgabe.fil_ges_umsatz;   //nur zum Summieren im LL
		ausgabe.sfil_ges_ertrag = ausgabe.fil_ges_ertrag ;  //nur zum Summieren im LL

		  if (fehlercode == 0)
		  {
			 if (ausgabe.kun_fil == 1)
			 {
				 a_mo_wa.fil = (short) ausgabe.kun;
				 a_mo_wa.kun = 0;
				 ausgabe.kun = 0;
				 ausgabe.fil = a_mo_wa.fil;
			 }
			 else
			 {
				 ausgabe.fil = 0;
				 sprintf (ausgabe.fil_kla,"%ld",ausgabe.vertr);
				 a_mo_wa.kun = ausgabe.kun;
			 }

            if (ausgabe.kun_fil == 0)
			{
				dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
				dbClass.sqlout ((char *) adr_krz, SQLCHAR, 17);
				dbClass.sqlcomm ("select adr.adr_krz from kun,adr where kun = ?  and kun.adr1 = adr.adr");
			}
			else
			{
				dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
				dbClass.sqlout ((char *) adr_krz, SQLCHAR, 17);
				dbClass.sqlcomm ("select adr.adr_krz from fil,adr where fil.fil = ? and fil.adr = adr.adr ");
			}

			/****
			long anzahl = ausgabe.fil_anzahl;

			sqlstatus = 0;
			sqlstatus = dbClass.sqlopen (readcursor);
	        sqlstatus = dbClass.sqlfetch (readcursor);
			ausgabe.auswahl_menge = 0.0;
			ausgabe.auswahl_umsatz = 0.0;
			ausgabe.auswahl_ertrag = 0.0;
			while (anzahl > 0 && sqlstatus == 0)
			{
				anzahl --;
				ausgabe.auswahl_menge += ausgabe.menge;
				ausgabe.auswahl_umsatz += ausgabe.umsatz;
				ausgabe.auswahl_ertrag += ausgabe.ertrag;
		        sqlstatus = dbClass.sqlfetch (readcursor);
			}
			*****/


			sqlstatus = dbClass.sqlopen (readcursor);
	        sqlstatus = dbClass.sqlfetch (readcursor);
			ausgabe.ProzGesamt = ausgabe.fil_anteil;
			ausgabe.ProzAuswahl = ausgabe.umsatz / ausgabe.ges_umsatz * 100;
			/***
            if (ausgabe.fil_umsatz != 0.0 && ausgabe.ges_umsatz != 0.0)
			{
				ausgabe.ProzGesamt = ausgabe.umsatz / ausgabe.fil_umsatz * 100;
				ausgabe.ProzAuswahl = ausgabe.umsatz / ausgabe.ges_umsatz * 100;
			}
			if (strcmp(R_menge,"J")== 0 && ausgabe.fil_menge != 0.0 && ausgabe.ges_menge != 0.0)
			{
				ausgabe.ProzGesamt = ausgabe.fil_anteil;
				ausgabe.ProzAuswahl = ausgabe.menge / ausgabe.ges_menge * 100;
			} 
			else if (strcmp(R_umsatz,"J")== 0 && ausgabe.fil_umsatz != 0.0 && ausgabe.ges_menge != 0.0 ) 
			{
				ausgabe.ProzGesamt = ausgabe.fil_anteil;
				ausgabe.ProzAuswahl = ausgabe.umsatz / ausgabe.ges_umsatz * 100;
			}
			else if (strcmp(R_ertrag,"J")== 0 && ausgabe.fil_menge != 0.0 && ausgabe.ges_menge != 0.0 ) 
			{
				ausgabe.ProzGesamt = ausgabe.fil_anteil;
				ausgabe.ProzAuswahl = ausgabe.ertrag / ausgabe.ges_ertrag * 100;
			}
			**/


			if (sqlstatus == 100) 
			{
				ausgabe.fil_anzahl = 0;
			}
			if (sqlstatus == 0) ausgabe.fil_anzahl --;
			return sqlstatus;
		  }
		  else
		  {
			return fehlercode;
		  }
	  }
	  else //(ausgabe.fil_anzahl)
	  {
	        sqlstatus = dbClass.sqlfetch (readcursor);
			if (sqlstatus == 0) ausgabe.fil_anzahl --;
			if (sqlstatus == 100) 
			{
				fehlercode = dbClass.sqlfetch (count_cursor);
			    while (fehlercode == 0)
				{
					if (ausgabe.fil_menge > 0.0) break;
					fehlercode = dbClass.sqlfetch (count_cursor);
				}
				ausgabe.sfil_ges_menge = ausgabe.fil_ges_menge;   //nur zum Summieren im LL
				ausgabe.sfil_ges_umsatz = ausgabe.fil_ges_umsatz;   //nur zum Summieren im LL
				ausgabe.sfil_ges_ertrag = ausgabe.fil_ges_ertrag;   //nur zum Summieren im LL
  				if (max_anz < ausgabe.fil_anzahl) ausgabe.fil_anzahl = max_anz;

				if (fehlercode == 0)
				{
					 if (ausgabe.kun_fil == 1)
					{
						a_mo_wa.fil = (short) ausgabe.kun;
						a_mo_wa.kun = 0;
						ausgabe.kun = 0;
						ausgabe.fil = a_mo_wa.fil;
					}
					else
					{
						ausgabe.fil = 0;
						sprintf (ausgabe.fil_kla,"%ld",ausgabe.vertr);
						a_mo_wa.kun = ausgabe.kun;
					}	
		
					if (ausgabe.kun_fil == 0)
					{
						dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
						dbClass.sqlout ((char *) adr_krz, SQLCHAR, 17);
						dbClass.sqlcomm ("select adr.adr_krz from kun,adr where kun = ?  and kun.adr1 = adr.adr");
					}
					else
					{
						dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
						dbClass.sqlout ((char *) adr_krz, SQLCHAR, 17);
						dbClass.sqlcomm ("select adr.adr_krz from fil,adr where fil.fil = ? and fil.adr = adr.adr ");
					}



					sqlstatus = dbClass.sqlopen (readcursor);
					sqlstatus = dbClass.sqlfetch (readcursor);
	  			    if (sqlstatus == 0) ausgabe.fil_anzahl --;
				}
				else
				{
					return fehlercode;
				}
			}


            if (ausgabe.fil_umsatz != 0.0 && ausgabe.ges_umsatz != 0.0)
			{
				ausgabe.ProzGesamt = ausgabe.umsatz / ausgabe.fil_umsatz * 100;
				ausgabe.ProzAuswahl = ausgabe.umsatz / ausgabe.ges_umsatz * 100;
			}
			if (strcmp(R_menge,"J")== 0 && ausgabe.fil_menge != 0.0 && ausgabe.ges_menge != 0.0)
			{
				ausgabe.ProzGesamt = ausgabe.menge / ausgabe.fil_menge * 100;
				ausgabe.ProzAuswahl = ausgabe.menge / ausgabe.ges_menge * 100;
			} 
			else if (strcmp(R_umsatz,"J")== 0 && ausgabe.fil_umsatz != 0.0 && ausgabe.ges_menge != 0.0 ) 
			{
				ausgabe.ProzGesamt = ausgabe.umsatz / ausgabe.fil_umsatz * 100;
				ausgabe.ProzAuswahl = ausgabe.umsatz / ausgabe.ges_umsatz * 100;
			}
			else if (strcmp(R_ertrag,"J")== 0 && ausgabe.fil_menge != 0.0 && ausgabe.ges_menge != 0.0 ) 
			{
				ausgabe.ProzGesamt = ausgabe.ertrag / ausgabe.fil_ertrag * 100;
				ausgabe.ProzAuswahl = ausgabe.ertrag / ausgabe.ges_ertrag * 100;
			}

	  }



      return fehlercode;
}

int A_MO_WA_CLASS::openDS (void)
{
         return dbClass.sqlopen (readcursor);
}

void A_MO_WA_CLASS::prepare (void)
{
     char *sqltext1;
     char *sqltext2;
//	char *cmax_anz ;

    dbRange.RangeIn ("a_tag_wa.fil",filbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_tag_wa.kun",kunbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_bas.wg",wgbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_tag_wa.a",abereich, SQLLONG, 0); 
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 11);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 11);


	dbClass.sqlout ((char *) ausgabe.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((long *) &ausgabe.vertr, SQLLONG, 2);
	dbClass.sqlout ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &ausgabe.fil_anzahl, SQLSHORT, 0);   // Anzahl s�tze f�r eine Filiale
	dbClass.sqlout ((double *) &ausgabe.fil_menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_ges_menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_ges_umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_anteil, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_ertrag, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_ges_ertrag, SQLDOUBLE, 0);

    sqltext1 = dbRange.SetRange ("select  "
							"case when a_tag_wa.fil > 0 then fil.fil_kla "
							"	when a_tag_wa.fil = 0 then \"\" end klasse , "
							"case when a_tag_wa.kun > 0 then kun.vertr1 "
							"	when a_tag_wa.kun = 0 then 0 end vertr , "

							"case when a_tag_wa.fil = 0 then a_tag_wa.kun "
							"     when a_tag_wa.fil > 0 then a_tag_wa.fil end kunde, "
							"case when a_tag_wa.fil = 0 then 0 "
							"     when a_tag_wa.fil > 0 then 1 end kun_fil, "
							"count(unique a_tag_wa.a), "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_sa_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_sa_tag)  end ) menge, "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_sa_tag + a_tag_wa.me_vk_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_sa_tag + a_tag_wa.me_vk_tag)  end ) ges_menge, "
							"sum(a_tag_wa.ums_vk_sa_tag)  umsatz, "
							"sum(a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag)  ges_umsatz, "
							"case when sum(a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag) > 0 "
							"     then sum(a_tag_wa.ums_vk_sa_tag) / sum(a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag) * 100 "
							"	else 0 end Anteil, "
							"sum((a_tag_wa.ums_vk_sa_tag) - ((a_tag_wa.me_vk_sa_tag) * a_tag_wa.sk_vollk)) ertrag, "
							"sum((a_tag_wa.ums_vk_sa_tag + a_tag_wa.ums_vk_tag) - ((a_tag_wa.me_vk_sa_tag + a_tag_wa.me_vk_tag) * a_tag_wa.sk_vollk)) ges_ertrag "
							"from a_tag_wa,a_bas , outer fil, outer kun "
							"where a_tag_wa.a = a_bas.a "
							"and a_tag_wa.fil = fil.fil "
							"and a_tag_wa.kun = kun.kun "
							"and a_tag_wa.mdn = ? "
							"and (<a_tag_wa.fil> "
							"     or <a_tag_wa.kun> ) "
							"and a_tag_wa.dat between ? and ? "
							"and <a_bas.wg> "
							"and <a_tag_wa.a> "
							"and (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag) <> 0.0 "
							"group by 1,2,3,4 having sum(a_tag_wa.ums_vk_sa_tag) > 0 order by klasse,vertr,kunde ");


           count_cursor = dbClass.sqlcursor (sqltext1);




    dbRange.RangeIn ("a_bas.wg",wgbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_tag_wa.a",abereich, SQLLONG, 0); 
    dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 11);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 11);


	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) ausgabe.a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((double *) &ausgabe.menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ges_menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ges_umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ertrag, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ges_ertrag, SQLDOUBLE, 0);

    sqltext2 = dbRange.SetRange ("select  "
							"a_tag_wa.a artikel , a_bas.a_bz1, "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_sa_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_sa_tag)  end ) menge, "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag)  end ) ges_menge, "
							"sum(a_tag_wa.ums_vk_sa_tag)  umsatz, "
							"sum(a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag)  ges_umsatz, "
							"sum((a_tag_wa.ums_vk_sa_tag) - ((a_tag_wa.me_vk_sa_tag) * a_tag_wa.sk_vollk)) ertrag, "
							"sum((a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag) - ((a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag) * a_tag_wa.sk_vollk)) ges_ertrag "
							"from a_tag_wa,a_bas , outer fil, outer kun "
							"where a_tag_wa.a = a_bas.a "
							"and a_tag_wa.fil = fil.fil "
							"and a_tag_wa.kun = kun.kun "
							"and a_tag_wa.mdn = ? "
							"and a_tag_wa.fil = ? "
							"and a_tag_wa.kun = ? "
							"and a_tag_wa.dat between ? and ? "
							"and <a_bas.wg> "
							"and <a_tag_wa.a> "
							"group by 1,2 having sum(a_tag_wa.ums_vk_sa_tag) > 0 order by                                               ");

	clipped(sqltext2);
strcat (sqltext2,order_by);
strcat (sqltext2,order_by_zusatz);

           readcursor = dbClass.sqlcursor (sqltext2);



	test_upd_cursor = 1;


}
