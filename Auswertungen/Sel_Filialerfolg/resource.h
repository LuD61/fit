//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by llmfc.rc
//
#define IDR_MAINFRAME                   2
#define IDD_ABOUTBOX                    100
#define IDD_DIALOG1                     101
#define IDC_WOVON                       101
#define IDC_GR1VON                      101
#define IDC_GR1BEREICH                  101
#define IDC_WOBIS                       102
#define IDC_MOBIS                       102
#define IDC_EDIT1                       103
#define IDC_JAHR                        103
#define IDC_MANDANT                     104
#define IDC_FILIALE                     105
#define IDC_FILIALEBIS                  106
#define IDC_AVON                        107
#define IDC_GR1BIS                      107
#define IDC_ABIS                        108
#define IDC_FILBEREICH                  108
#define IDC_KUNDE                       109
#define IDC_KUNDEBIS                    110
#define IDC_WGVON                       111
#define IDC_GR2VON                      111
#define IDC_GR2BEREICH                  111
#define IDC_WGBIS                       112
#define IDC_GR3VON                      112
#define IDC_GR3BEREICH                  112
#define IDC_GR2BIS2                     113
#define IDC_GR2BIS                      113
#define IDC_GR3BIS                      114
#define IDC_GR4VON                      115
#define IDC_GR4BEREICH                  115
#define IDC_GR4BIS                      116
#define IDC_GR5VON                      117
#define IDC_GR5BEREICH                  117
#define IDC_GR5BIS                      118
#define IDC_GR6VON                      119
#define IDC_GR6BEREICH                  119
#define IDC_GR6BIS                      120
#define IDC_MOVON                       121
#define IDC_RADIOHWG                    122
#define IDC_RADIOWG                     123
#define IDC_RADIOSMT                    124
#define IDC_RADIOAG                     125
#define IDC_CHECKGR1N                   126
#define IDC_KUNBEREICH                  127
#define IDC_CHECKGR1S                   128
#define IDC_CHECKGR2N                   129
#define IDC_CHECKGR3N                   130
#define IDC_CHECKGR4N                   131
#define IDC_CHECKGR5N                   132
#define IDC_CHECKGR6N                   133
#define IDC_CHECKGR2S                   134
#define IDC_CHECKGR3S                   135
#define IDC_CHECKGR4S                   136
#define IDC_CHECKGR5S                   137
#define IDC_CHECKGR6S                   138
#define IDC_STATIC_BEREICH              139
#define IDC_MOBEREICH                   140
#define IDC_MONAT                       140
#define ID_PRINT_LIST_PRINTER           32770
#define ID_PRINT_LIST_PREVIEW           32771
#define ID_PRINT_LABEL_PRINTER          32772
#define ID_PRINT_LABEL_PREVIEW          32773
#define ID_EDIT_LIST                    32774
#define ID_EDIT_LABEL                   32775
#define ID_FILE_DEBUG                   32776
#define ID_FILE_STOP_DEBUG              32778
#define ID_FILE_START_DEBUG             32779
#define ID_PRINT_REPORT                 32780
#define ID_PRINT_LABEL                  32781

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         140
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
