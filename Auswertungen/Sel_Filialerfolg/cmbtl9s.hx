/**** C/C++ run-time linkage constants and function definitions for L9S.DLL ****/
/****  (c) 1991,92,93,94,95,96,97,98,99,2000,01,02... combit GmbH, Konstanz, Germany  ****/
/****  [build of 2002-11-04 15:11:11] ****/

#ifndef _L9S_H
#define _L9S_H

#if !defined(WIN16) && !defined(WIN32)
  #pragma message("you should define either WIN16 or WIN32 to ensure a correct compilation");
  #pragma message("(I assume WIN32, cross your fingers to proceed)");
  #define WIN32 1
#endif


#ifndef EXPLICIT_TYPES
  #define EXPLICIT_TYPES
  #ifndef INT
    typedef int INT; /* you should comment this out if you have any problems with INT */
  #endif
  #ifndef CHAR
    typedef char CHAR; /* you should comment this out if you have any problems with CHAR */
  #endif
  typedef unsigned char UINT8;
  typedef unsigned short UINT16;
  typedef signed char INT8;
  typedef signed short INT16;
  #ifndef _BASETSD_H_ /* MSVC6 defines these itself in basetsd.h */
    typedef unsigned long UINT32;
    typedef signed long INT32;
  #endif
#endif

#ifndef DLLPROC
  #define DLLPROC WINAPI
#endif
#ifndef DLLCPROC
  #ifdef WIN32
    #define DLLCPROC WINAPI
   #else
    #define DLLCPROC _far _cdecl
  #endif
#endif


#ifndef CMBTLANG_DEFAULT
 #define CMBTLANG_DEFAULT    -1
 #define CMBTLANG_GERMAN      0
 #define CMBTLANG_ENGLISH     1
 #define CMBTLANG_ARABIC      2
 #define CMBTLANG_AFRIKAANS   3
 #define CMBTLANG_ALBANIAN    4
 #define CMBTLANG_BASQUE      5
 #define CMBTLANG_BULGARIAN   6
 #define CMBTLANG_BYELORUSSIAN 7
 #define CMBTLANG_CATALAN     8
 #define CMBTLANG_CHINESE     9
 #define CMBTLANG_CROATIAN    10
 #define CMBTLANG_CZECH       11
 #define CMBTLANG_DANISH      12
 #define CMBTLANG_DUTCH       13
 #define CMBTLANG_ESTONIAN    14
 #define CMBTLANG_FAEROESE    15
 #define CMBTLANG_FARSI       16
 #define CMBTLANG_FINNISH     17
 #define CMBTLANG_FRENCH      18
 #define CMBTLANG_GREEK       19
 #define CMBTLANG_HEBREW      20
 #define CMBTLANG_HUNGARIAN   21
 #define CMBTLANG_ICELANDIC   22
 #define CMBTLANG_INDONESIAN  23
 #define CMBTLANG_ITALIAN     24
 #define CMBTLANG_JAPANESE    25
 #define CMBTLANG_KOREAN      26
 #define CMBTLANG_LATVIAN     27
 #define CMBTLANG_LITHUANIAN  28
 #define CMBTLANG_NORWEGIAN   29
 #define CMBTLANG_POLISH      30
 #define CMBTLANG_PORTUGUESE  31
 #define CMBTLANG_ROMANIAN    32
 #define CMBTLANG_RUSSIAN     33
 #define CMBTLANG_SLOVAK      34
 #define CMBTLANG_SLOVENIAN   35
 #define CMBTLANG_SERBIAN     36
 #define CMBTLANG_SPANISH     37
 #define CMBTLANG_SWEDISH     38
 #define CMBTLANG_THAI        39
 #define CMBTLANG_TURKISH     40
 #define CMBTLANG_UKRAINIAN   41
 #define CMBTLANG_CHINESE_TRADITIONAL   48
#endif


/*--- type declarations ---*/

#ifndef LPCTSTR
  #define LPCTSTR                        const TCHAR FAR *
#endif
#ifndef _PCRECT
  #define _PCRECT                        const RECT FAR *
#endif
#ifndef HLLSTG
  #define HLLSTG                         UINT32
#endif
#ifndef HSTG
  #define HSTG                           UINT32
#endif
#ifndef PHGLOBAL
  #define PHGLOBAL                       HGLOBAL FAR *
#endif

/*--- constant declarations ---*/

#define LL_ERR_BAD_JOBHANDLE           (-1)                 /* bad jobhandle */
#define LL_ERR_TASK_ACTIVE             (-2)                 /* LlDefineLayout() only once in a job */
#define LL_ERR_BAD_OBJECTTYPE          (-3)                 /* nObjType must be one of the allowed values (obsolete constant) */
#define LL_ERR_BAD_PROJECTTYPE         (-3)                 /* nObjType must be one of the allowed values */
#define LL_ERR_PRINTING_JOB            (-4)                 /* print job not opened, no print object */
#define LL_ERR_NO_BOX                  (-5)                 /* LlPrintSetBoxText(...) called when no abort box exists! */
#define LL_ERR_ALREADY_PRINTING        (-6)                 /* LlPrintWithBoxStart(...): another print job is being done, please wait or try LlPrintStart(...) */
#define LL_ERR_NO_PROJECT              (-10)                /* object with requested name does not exist (former ERR_NO_OBJECT) */
#define LL_ERR_NO_PRINTER              (-11)                /* printer couldn't be opened */
#define LL_ERR_PRINTING                (-12)                /* error while printing */
#define LL_ERR_ONLY_ONE_JOB            (-13)                /* only one job per application possible */
#define LL_ERR_NEEDS_VB                (-14)                /* '11...' needs VB.EXE */
#define LL_ERR_BAD_PRINTER             (-15)                /* PrintOptionsDialog(): no printer available */
#define LL_ERR_NO_PREVIEWMODE          (-16)                /* Preview functions: not in preview mode */
#define LL_ERR_NO_PREVIEWFILES         (-17)                /* PreviewDisplay(): no file found */
#define LL_ERR_PARAMETER               (-18)                /* bad parameter (usually NULL pointer) */
#define LL_ERR_BAD_EXPRESSION          (-19)                /* bad expression in LlExprEvaluate() and LlExprType() */
#define LL_ERR_BAD_EXPRMODE            (-20)                /* bad expression mode (LlSetExpressionMode()) */
#define LL_ERR_NO_TABLE                (-21)                /* no table object found to put data in */
#define LL_ERR_CFGNOTFOUND             (-22)                /* on LlPrintStart(), LlPrintWithBoxStart() [not found] */
#define LL_ERR_EXPRESSION              (-23)                /* on LlPrintStart(), LlPrintWithBoxStart() */
#define LL_ERR_CFGBADFILE              (-24)                /* on LlPrintStart(), LlPrintWithBoxStart() [read error, bad format] */
#define LL_ERR_BADOBJNAME              (-25)                /* on LlPrintEnableObject() - not a ':' at the beginning */
#define LL_ERR_NOOBJECT                (-26)                /* on LlPrintEnableObject() - "*" and no object in project */
#define LL_ERR_UNKNOWNOBJECT           (-27)                /* on LlPrintEnableObject() - object with that name not existing */
#define LL_ERR_NO_TABLEOBJECT          (-28)                /* LlPrint...Start() and no list in Project, or: */
#define LL_ERR_NO_OBJECT               (-29)                /* LlPrint...Start() and no object in project */
#define LL_ERR_NO_TEXTOBJECT           (-30)                /* LlPrintGetTextCharsPrinted() and no printable text in Project! */
#define LL_ERR_UNKNOWN                 (-31)                /* LlPrintIsVariableUsed(), LlPrintIsFieldUsed() */
#define LL_ERR_BAD_MODE                (-32)                /* LlPrintFields(), LlPrintIsFieldUsed() called on non-OBJECT_LIST */
#define LL_ERR_CFGBADMODE              (-33)                /* on LlDefineLayout(), LlPrint...Start(): file is in wrong expression mode */
#define LL_ERR_ONLYWITHONETABLE        (-34)                /* on LlDefinePageSeparation(), LlDefineGrouping() */
#define LL_ERR_UNKNOWNVARIABLE         (-35)                /* on LlGetVariableContents() */
#define LL_ERR_UNKNOWNFIELD            (-36)                /* on LlGetFieldContents() */
#define LL_ERR_UNKNOWNSORTORDER        (-37)                /* on LlGetFieldContents() */
#define LL_ERR_NOPRINTERCFG            (-38)                /* on LlPrintCopyPrinterConfiguration() - no or bad file */
#define LL_ERR_SAVEPRINTERCFG          (-39)                /* on LlPrintCopyPrinterConfiguration() - file could not be saved */
#define LL_ERR_RESERVED                (-40)                /* function not yet implemeted */
#define LL_ERR_BUFFERTOOSMALL          (-44)                /* LlXXGetOptionStr() */
#define LL_ERR_USER_ABORTED            (-99)                /* user aborted printing */
#define LL_ERR_BAD_DLLS                (-100)               /* DLLs not up to date (CTL, DWG, UTIL) */
#define LL_ERR_NO_LANG_DLL             (-101)               /* no or out-of-date language resource DLL */
#define LL_ERR_NO_MEMORY               (-102)               /* out of memory */
#define LL_BOXTYPE_NORMALMETER         (0)                 
#define LL_BOXTYPE_BRIDGEMETER         (1)                 
#define LL_BOXTYPE_NORMALWAIT          (2)                 
#define LL_BOXTYPE_BRIDGEWAIT          (3)                 
#define LL_UNITS_MM_DIV_10             (0)                 
#define LL_UNITS_INCH_DIV_100          (1)                 
#define LL_STG_COMPAT4                 (0)                 
#define LL_STG_STORAGE                 (1)                 
#define LL_ERR_STG_NOSTORAGE           (-1000)             
#define LL_ERR_STG_BADVERSION          (-1001)             
#define LL_ERR_STG_READ                (-1002)             
#define LL_ERR_STG_WRITE               (-1003)             
#define LL_ERR_STG_UNKNOWNSYSTEM       (-1004)             
#define LL_ERR_STG_BADHANDLE           (-1005)             
#define LL_ERR_STG_ENDOFLIST           (-1006)             
#define LL_ERR_STG_BADJOB              (-1007)             
#define LL_ERR_STG_ACCESSDENIED        (-1008)             
#define LL_ERR_STG_BADSTORAGE          (-1009)             
#define LL_ERR_STG_CANNOTGETMETAFILE   (-1010)             
#define LL_WRN_STG_UNFAXED_PAGES       (-1100)             
#define LL_STGSYS_OPTION_HAS16BITPAGES (200)                /* has job 16 bit pages? */
#define LL_STGSYS_OPTION_BOXTYPE       (201)                /* wait meter box type */
#define LL_STGSYS_OPTION_UNITS         (203)                /* LL_UNITS_INCH_DIV_100 or LL_UNITS_MM_DIV_10 */
#define LL_STGSYS_OPTION_PRINTERCOUNT  (204)                /* number of printers (1 or 2) */
#define LL_STGSYS_OPTION_ISSTORAGE     (205)                /* returns whether file is STORAGE or COMPAT4 */
#define LL_STGSYS_OPTION_EMFRESOLUTION (206)                /* EMFRESOLUTION used to print the file */
#define LL_STGSYS_OPTION_JOB           (207)                /* returns current job number */
#define LL_STGSYS_OPTION_TOTALPAGES    (208)                /* differs from GetPageCount() if print range in effect */
#define LL_STGSYS_OPTION_PAGESWITHFAXNUMBER (209)                /* NYI */
#define LL_STGSYS_OPTION_PAGENUMBER    (0)                  /* page number of current page */
#define LL_STGSYS_OPTION_COPIES        (1)                  /* number of copies (same for all pages at the moment) */
#define LL_STGSYS_OPTION_PRN_ORIENTATION (2)                  /* orientation (DMORIENT_LANDSCAPE, DMORIENT_PORTRAIT) */
#define LL_STGSYS_OPTION_PHYSPAGE      (3)                  /* is page "physical page" oriented? */
#define LL_STGSYS_OPTION_PRN_PIXELSOFFSET_X (4)                  /* this and the following values are */
#define LL_STGSYS_OPTION_PRN_PIXELSOFFSET_Y (5)                  /* values of the printer that the preview was */
#define LL_STGSYS_OPTION_PRN_PIXELS_X  (6)                  /* created on! */
#define LL_STGSYS_OPTION_PRN_PIXELS_Y  (7)                 
#define LL_STGSYS_OPTION_PRN_PIXELSPHYSICAL_X (8)                 
#define LL_STGSYS_OPTION_PRN_PIXELSPHYSICAL_Y (9)                 
#define LL_STGSYS_OPTION_PRN_PIXELSPERINCH_X (10)                
#define LL_STGSYS_OPTION_PRN_PIXELSPERINCH_Y (11)                
#define LL_STGSYS_OPTION_PRN_INDEX     (12)                 /* printer index of the page (0/1) */
#define LL_STGSYS_OPTION_PRN_PAPERTYPE (13)                
#define LL_STGSYS_OPTION_PRN_PAPERSIZE_X (14)                
#define LL_STGSYS_OPTION_PRN_PAPERSIZE_Y (15)                
#define LL_STGSYS_OPTION_PRN_FORCE_PAPERSIZE (16)                
#define LL_STGSYS_OPTION_PROJECTNAME   (100)                /* name of the original project (not page dependent) */
#define LL_STGSYS_OPTION_JOBNAME       (101)                /* name of the job (WindowTitle of LlPrintWithBoxStart()) (not page dependent) */
#define LL_STGSYS_OPTION_PRTNAME       (102)                /* printer name ("HP Laserjet 4L") */
#define LL_STGSYS_OPTION_PRTDEVICE     (103)                /* printer device ("PSCRIPT") */
#define LL_STGSYS_OPTION_PRTPORT       (104)                /* printer port ("LPT1:" or "\\server\printer") */
#define LL_STGSYS_OPTION_USER          (105)                /* user string (not page dependent) */
#define LL_STGSYS_OPTION_CREATION      (106)                /* creation date (not page dependent) */
#define LL_STGSYS_OPTION_CREATIONAPP   (107)                /* creation application (not page dependent) */
#define LL_STGSYS_OPTION_CREATIONDLL   (108)                /* creation DLL (not page dependent) */
#define LL_STGSYS_OPTION_CREATIONUSER  (109)                /* creation user and computer name (not page dependent) */
#define LL_STGSYS_OPTION_FAXPARA_QUEUE (110)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_RECIPNAME (111)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_RECIPNUMBER (112)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERNAME (113)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERCOMPANY (114)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERDEPT (115)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_SENDERBILLINGCODE (116)                /* NYI */
#define LL_STGSYS_OPTION_FAXPARA_DREMAILADDRESS (117)                /* NYI */
#define LL_STGSYS_OPTION_FAX_AVAILABLEQUEUES (118)                /* NYI, nPageIndex=1 */
#define LL_STGSYS_OPTION_PRINTERALIASLIST (119)                /* alternative printer list (taken from project) */
#define LL_STGSYSPRINTFLAG_FIT         (0x00000001)        
#define LL_STGSYSPRINTFLAG_STACKEDCOPIES (0x00000002)         /* n times page1, n times page2, ... (else n times (page 1...x)) */
#define LL_STGSYSPRINTFLAG_TRYPRINTERCOPIES (0x00000004)         /* first try printer copies, then simulated ones... */
#define LL_STGSYSPRINTFLAG_METER       (0x00000010)        
#define LL_STGSYSPRINTFLAG_ABORTABLEMETER (0x00000020)        
#define LL_STGSYSPRINTFLAG_METERMASK   (0x00000070)         /* allows 7 styles of abort boxes... */

/*--- function declaration ---*/

#ifndef RC_INVOKED

#ifdef __ZTC__ /* Zortech C++ */
#pragma ZTC align 1
#elif __WATCOMC__ > 1000 /* Watcom C++ >= 10.5 */
#pragma pack(push,1)
#elif __BORLANDC__ /* Borland C++ */
#pragma option -a1
#else
#pragma pack(1) /* MS, Watcom <= 10.0, ... */
#endif

#ifdef __cplusplus
extern "C" {
#endif

extern HINSTANCE hDLLL9S;
extern INT nDLLL9SCounter;

#ifdef IMPLEMENTATION
  #define extern /* */
  HINSTANCE hDLLL9S = NULL;
  INT       nDLLL9SCounter = 0;
#endif



/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysStorageOpenO LlStgsysStorageOpenA
     #else
       #define LlStgsysStorageOpen LlStgsysStorageOpenA
   #endif
#endif
  #ifdef WIN32
  typedef   HLLSTG ( DLLPROC *PFNLLSTGSYSSTORAGEOPENA)(
	LPCSTR               lpszFilename,
	LPCSTR               pszTempPath,
	BOOL                 bReadOnly,
	BOOL                 bOneJobTranslation);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSSTORAGEOPENA LlStgsysStorageOpenA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysStorageOpen LlStgsysStorageOpenW
     #else
       #define LlStgsysStorageOpenO LlStgsysStorageOpenW
   #endif
#endif
  #ifdef WIN32
  typedef   HLLSTG ( DLLPROC *PFNLLSTGSYSSTORAGEOPENW)(
	LPCWSTR              lpszFilename,
	LPCWSTR              pszTempPath,
	BOOL                 bReadOnly,
	BOOL                 bOneJobTranslation);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSSTORAGEOPENW LlStgsysStorageOpenW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef     void ( DLLPROC *PFNLLSTGSYSSTORAGECLOSE)(
	HLLSTG               hStg);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSSTORAGECLOSE LlStgsysStorageClose;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETAPIVERSION)(
	HLLSTG               hStg);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETAPIVERSION LlStgsysGetAPIVersion;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETFILEVERSION)(
	HLLSTG               hStg);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETFILEVERSION LlStgsysGetFileVersion;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysGetFilenameO LlStgsysGetFilenameA
     #else
       #define LlStgsysGetFilename LlStgsysGetFilenameA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETFILENAMEA)(
	HLLSTG               hStg,
	INT                  nJob,
	INT                  nFile,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETFILENAMEA LlStgsysGetFilenameA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysGetFilename LlStgsysGetFilenameW
     #else
       #define LlStgsysGetFilenameO LlStgsysGetFilenameW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETFILENAMEW)(
	HLLSTG               hStg,
	INT                  nJob,
	INT                  nFile,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETFILENAMEW LlStgsysGetFilenameW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETJOBCOUNT)(
	HLLSTG               hStg);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETJOBCOUNT LlStgsysGetJobCount;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSSETJOB)(
	HLLSTG               hStg,
	INT                  nJob);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSSETJOB LlStgsysSetJob;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETPAGECOUNT)(
	HLLSTG               hStg);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETPAGECOUNT LlStgsysGetPageCount;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETJOBOPTIONVALUE)(
	HLLSTG               hStg,
	INT                  nOption);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETJOBOPTIONVALUE LlStgsysGetJobOptionValue;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETPAGEOPTIONVALUE)(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETPAGEOPTIONVALUE LlStgsysGetPageOptionValue;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysGetPageOptionStringO LlStgsysGetPageOptionStringA
     #else
       #define LlStgsysGetPageOptionString LlStgsysGetPageOptionStringA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETPAGEOPTIONSTRINGA)(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPSTR                pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETPAGEOPTIONSTRINGA LlStgsysGetPageOptionStringA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysGetPageOptionString LlStgsysGetPageOptionStringW
     #else
       #define LlStgsysGetPageOptionStringO LlStgsysGetPageOptionStringW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETPAGEOPTIONSTRINGW)(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPWSTR               pszBuffer,
	UINT                 nBufSize);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETPAGEOPTIONSTRINGW LlStgsysGetPageOptionStringW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysSetPageOptionStringO LlStgsysSetPageOptionStringA
     #else
       #define LlStgsysSetPageOptionString LlStgsysSetPageOptionStringA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSSETPAGEOPTIONSTRINGA)(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPCSTR               pszBuffer);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSSETPAGEOPTIONSTRINGA LlStgsysSetPageOptionStringA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysSetPageOptionString LlStgsysSetPageOptionStringW
     #else
       #define LlStgsysSetPageOptionStringO LlStgsysSetPageOptionStringW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSSETPAGEOPTIONSTRINGW)(
	HLLSTG               hStg,
	INT                  nPageIndex,
	INT                  nOption,
	LPCWSTR              pszBuffer);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSSETPAGEOPTIONSTRINGW LlStgsysSetPageOptionStringW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSAPPEND)(
	HLLSTG               hStg,
	HLLSTG               hStgToAppend);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSAPPEND LlStgsysAppend;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSDELETEJOB)(
	HLLSTG               hStg,
	INT                  nPageIndex);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSDELETEJOB LlStgsysDeleteJob;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSDELETEPAGE)(
	HLLSTG               hStg,
	INT                  nPageIndex);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSDELETEPAGE LlStgsysDeletePage;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef   HANDLE ( DLLPROC *PFNLLSTGSYSGETPAGEMETAFILE)(
	HLLSTG               hStg,
	INT                  nPageIndex);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETPAGEMETAFILE LlStgsysGetPageMetafile;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef   HANDLE ( DLLPROC *PFNLLSTGSYSGETPAGEMETAFILE16)(
	HLLSTG               hStg,
	INT                  nPageIndex);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETPAGEMETAFILE16 LlStgsysGetPageMetafile16;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSDESTROYMETAFILE)(
	HANDLE               hMF);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSDESTROYMETAFILE LlStgsysDestroyMetafile;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSDRAWPAGE)(
	HLLSTG               hStg,
	HDC                  hDC,
	HDC                  hPrnDC,
	BOOL                 bAskPrinter,
	_PCRECT              pRC,
	INT                  nPageIndex,
	BOOL                 bFit,
	LPVOID               pReserved);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSDRAWPAGE LlStgsysDrawPage;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETLASTERROR)(
	HLLSTG               hStg);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETLASTERROR LlStgsysGetLastError;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
  typedef      INT ( DLLPROC *PFNLLSTGSYSDELETEFILES)(
	HLLSTG               hStg);
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSDELETEFILES LlStgsysDeleteFiles;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysPrintO LlStgsysPrintA
     #else
       #define LlStgsysPrint LlStgsysPrintA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSPRINTA)(
	HLLSTG               hStg,
	LPCSTR               pszPrinterName1,
	LPCSTR               pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCSTR               pszMessage,
	HWND                 hWndParent);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSPRINTA LlStgsysPrintA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysPrint LlStgsysPrintW
     #else
       #define LlStgsysPrintO LlStgsysPrintW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSPRINTW)(
	HLLSTG               hStg,
	LPCWSTR              pszPrinterName1,
	LPCWSTR              pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCWSTR              pszMessage,
	HWND                 hWndParent);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSPRINTW LlStgsysPrintW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysStoragePrintO LlStgsysStoragePrintA
     #else
       #define LlStgsysStoragePrint LlStgsysStoragePrintA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSSTORAGEPRINTA)(
	LPCSTR               lpszFilename,
	LPCSTR               pszTempPath,
	LPCSTR               pszPrinterName1,
	LPCSTR               pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCSTR               pszMessage,
	HWND                 hWndParent);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSSTORAGEPRINTA LlStgsysStoragePrintA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysStoragePrint LlStgsysStoragePrintW
     #else
       #define LlStgsysStoragePrintO LlStgsysStoragePrintW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSSTORAGEPRINTW)(
	LPCWSTR              lpszFilename,
	LPCWSTR              pszTempPath,
	LPCWSTR              pszPrinterName1,
	LPCWSTR              pszPrinterName2,
	INT                  nStartPageIndex,
	INT                  nEndPageIndex,
	INT                  nCopies,
	UINT                 nFlags,
	LPCWSTR              pszMessage,
	HWND                 hWndParent);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSSTORAGEPRINTW LlStgsysStoragePrintW;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysGetPagePrinterO LlStgsysGetPagePrinterA
     #else
       #define LlStgsysGetPagePrinter LlStgsysGetPagePrinterA
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETPAGEPRINTERA)(
	HLLSTG               hStg,
	INT                  nPageIndex,
	LPSTR                pszDeviceName,
	UINT                 nDeviceNameSize,
	PHGLOBAL             phDevMode);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETPAGEPRINTERA LlStgsysGetPagePrinterA;
/* ------------------------------------------------------------------ */
#ifndef IMPLEMENTATION
#ifdef WIN32
   #ifdef UNICODE
       #define LlStgsysGetPagePrinter LlStgsysGetPagePrinterW
     #else
       #define LlStgsysGetPagePrinterO LlStgsysGetPagePrinterW
   #endif
#endif
  #ifdef WIN32
  typedef      INT ( DLLPROC *PFNLLSTGSYSGETPAGEPRINTERW)(
	HLLSTG               hStg,
	INT                  nPageIndex,
	LPWSTR               pszDeviceName,
	UINT                 nDeviceNameSize,
	PHGLOBAL             phDevMode);
  #endif
#endif /* IMPLEMENTATION */

extern PFNLLSTGSYSGETPAGEPRINTERW LlStgsysGetPagePrinterW;

INT L9SxLoad(void);
void L9SxUnload(void);

#ifdef IMPLEMENTATION
#if defined(WIN32)
  #define PROCIMPL9S(p) TEXT(#p)
  #define LOADDLLPROCL9S(fn,pfn,n) \
    fn = (pfn)GetProcAddress(hDLLL9S,TEXT("_") PROCIMPL9S(fn) TEXT("@") PROCIMPL9S(n));
  #define LOADDLLCPROCL9S(fn,pfn,n) \
    fn = (pfn)GetProcAddress(hDLLL9S,TEXT("_") PROCIMPL9S(fn));
#else // WIN16
  #define LOADDLLPROCL9S(fn,pfn,n) \
    fn = (pfn)GetProcAddress(hDLLL9S,#fn);
  #define LOADDLLCPROCL9S(fn,pfn,n) \
    fn = (pfn)GetProcAddress(hDLLL9S,"_"#fn);
#endif // WIN32

INT L9SxLoad(void)
{
  if (nDLLL9SCounter++ > 0)
    return(0);

  INT nError = SetErrorMode(SEM_NOOPENFILEERRORBOX);

#ifdef WIN32
  #ifdef UNICODE
    hDLLL9S = LoadLibrary(L"CM3UL9S.DLL");
  #else
    hDLLL9S = LoadLibrary("CM32L9S.DLL");
  #endif
#else
  hDLLL9S = LoadLibrary("CMBTL9S.DLL");
#endif
  SetErrorMode(nError);


#ifdef WIN32
  if (hDLLL9S == NULL) return(-1);
#else
  if (hDLLL9S < HINSTANCE_ERROR) return(-1);
#endif

  LOADDLLPROCL9S(LlStgsysStorageOpenA,PFNLLSTGSYSSTORAGEOPENA,16);
  LOADDLLPROCL9S(LlStgsysStorageOpenW,PFNLLSTGSYSSTORAGEOPENW,16);
  LOADDLLPROCL9S(LlStgsysStorageClose,PFNLLSTGSYSSTORAGECLOSE,4);
  LOADDLLPROCL9S(LlStgsysGetAPIVersion,PFNLLSTGSYSGETAPIVERSION,4);
  LOADDLLPROCL9S(LlStgsysGetFileVersion,PFNLLSTGSYSGETFILEVERSION,4);
  LOADDLLPROCL9S(LlStgsysGetFilenameA,PFNLLSTGSYSGETFILENAMEA,20);
  LOADDLLPROCL9S(LlStgsysGetFilenameW,PFNLLSTGSYSGETFILENAMEW,20);
  LOADDLLPROCL9S(LlStgsysGetJobCount,PFNLLSTGSYSGETJOBCOUNT,4);
  LOADDLLPROCL9S(LlStgsysSetJob,PFNLLSTGSYSSETJOB,8);
  LOADDLLPROCL9S(LlStgsysGetPageCount,PFNLLSTGSYSGETPAGECOUNT,4);
  LOADDLLPROCL9S(LlStgsysGetJobOptionValue,PFNLLSTGSYSGETJOBOPTIONVALUE,8);
  LOADDLLPROCL9S(LlStgsysGetPageOptionValue,PFNLLSTGSYSGETPAGEOPTIONVALUE,12);
  LOADDLLPROCL9S(LlStgsysGetPageOptionStringA,PFNLLSTGSYSGETPAGEOPTIONSTRINGA,20);
  LOADDLLPROCL9S(LlStgsysGetPageOptionStringW,PFNLLSTGSYSGETPAGEOPTIONSTRINGW,20);
  LOADDLLPROCL9S(LlStgsysSetPageOptionStringA,PFNLLSTGSYSSETPAGEOPTIONSTRINGA,16);
  LOADDLLPROCL9S(LlStgsysSetPageOptionStringW,PFNLLSTGSYSSETPAGEOPTIONSTRINGW,16);
  LOADDLLPROCL9S(LlStgsysAppend,PFNLLSTGSYSAPPEND,8);
  LOADDLLPROCL9S(LlStgsysDeleteJob,PFNLLSTGSYSDELETEJOB,8);
  LOADDLLPROCL9S(LlStgsysDeletePage,PFNLLSTGSYSDELETEPAGE,8);
  LOADDLLPROCL9S(LlStgsysGetPageMetafile,PFNLLSTGSYSGETPAGEMETAFILE,8);
  LOADDLLPROCL9S(LlStgsysGetPageMetafile16,PFNLLSTGSYSGETPAGEMETAFILE16,8);
  LOADDLLPROCL9S(LlStgsysDestroyMetafile,PFNLLSTGSYSDESTROYMETAFILE,4);
  LOADDLLPROCL9S(LlStgsysDrawPage,PFNLLSTGSYSDRAWPAGE,32);
  LOADDLLPROCL9S(LlStgsysGetLastError,PFNLLSTGSYSGETLASTERROR,4);
  LOADDLLPROCL9S(LlStgsysDeleteFiles,PFNLLSTGSYSDELETEFILES,4);
  LOADDLLPROCL9S(LlStgsysPrintA,PFNLLSTGSYSPRINTA,36);
  LOADDLLPROCL9S(LlStgsysPrintW,PFNLLSTGSYSPRINTW,36);
  LOADDLLPROCL9S(LlStgsysStoragePrintA,PFNLLSTGSYSSTORAGEPRINTA,40);
  LOADDLLPROCL9S(LlStgsysStoragePrintW,PFNLLSTGSYSSTORAGEPRINTW,40);
  LOADDLLPROCL9S(LlStgsysGetPagePrinterA,PFNLLSTGSYSGETPAGEPRINTERA,20);
  LOADDLLPROCL9S(LlStgsysGetPagePrinterW,PFNLLSTGSYSGETPAGEPRINTERW,20);
  return(0);
}

void L9SxUnload(void)
{
  if (--nDLLL9SCounter > 0)
    return;

#ifdef WIN32
  if (hDLLL9S != NULL)
#else
  if (hDLLL9S >= HINSTANCE_ERROR)
#endif
    {
    FreeLibrary(hDLLL9S);
    hDLLL9S = NULL;
    LlStgsysStorageOpenA = NULL;
    LlStgsysStorageOpenW = NULL;
    LlStgsysStorageClose = NULL;
    LlStgsysGetAPIVersion = NULL;
    LlStgsysGetFileVersion = NULL;
    LlStgsysGetFilenameA = NULL;
    LlStgsysGetFilenameW = NULL;
    LlStgsysGetJobCount = NULL;
    LlStgsysSetJob = NULL;
    LlStgsysGetPageCount = NULL;
    LlStgsysGetJobOptionValue = NULL;
    LlStgsysGetPageOptionValue = NULL;
    LlStgsysGetPageOptionStringA = NULL;
    LlStgsysGetPageOptionStringW = NULL;
    LlStgsysSetPageOptionStringA = NULL;
    LlStgsysSetPageOptionStringW = NULL;
    LlStgsysAppend = NULL;
    LlStgsysDeleteJob = NULL;
    LlStgsysDeletePage = NULL;
    LlStgsysGetPageMetafile = NULL;
    LlStgsysGetPageMetafile16 = NULL;
    LlStgsysDestroyMetafile = NULL;
    LlStgsysDrawPage = NULL;
    LlStgsysGetLastError = NULL;
    LlStgsysDeleteFiles = NULL;
    LlStgsysPrintA = NULL;
    LlStgsysPrintW = NULL;
    LlStgsysStoragePrintA = NULL;
    LlStgsysStoragePrintW = NULL;
    LlStgsysGetPagePrinterA = NULL;
    LlStgsysGetPagePrinterW = NULL;
    }
}
#endif

#ifdef __cplusplus
}
#endif

#ifdef __ZTC__ /* Zortech C++ */
#pragma ZTC align
#elif __WATCOMC__ > 1000 /* Watcom C++ */
#pragma pack(pop)
#elif __BORLANDC__ /* Borland C++ */
#pragma option -a.
#else
#pragma pack() /* MS C++ */
#endif

#ifdef IMPLEMENTATION
#undef extern
#endif

#endif  /* #ifndef RC_INVOKED */

#endif  /* #ifndef _L9S_H */

