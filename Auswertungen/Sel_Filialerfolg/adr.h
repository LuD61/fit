#ifndef _ADR_DEF
#define _ADR_DEF

struct ADR {
   long      adr;
   char      adr_krz[17];
   char      adr_nam1[37];
   char      adr_nam2[37];
   short     adr_typ;
   char      adr_verkt[17];
   short     anr;
   short     delstatus;
   char      fax[21];
   short     fil;
   long      geb_dat;
   short     land;
   short     mdn;
   char      merkm_1[3];
   char      merkm_2[3];
   char      merkm_3[3];
   char      merkm_4[3];
   char      merkm_5[3];
   char      modem[21];
   char      ort1[37];
   char      ort2[37];
   char      partner[37];
   char      pf[17];
   char      plz[9];
   short     staat;
   char      str[37];
   char      tel[21];
   char      telex[21];
   long      txt_nr;
   char      plz_postf[9];
   char      plz_pf[9];
   char      iln[33];
   char      email[37];
   char      swift[25];
   char      adr_nam3[37];
};
extern struct ADR adr, adr_null;

#line 5 "adr.rh"

#endif