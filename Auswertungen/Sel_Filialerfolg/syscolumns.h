#ifndef _SYSCOLUMNS_DEF
#define _SYSCOLUMNS_DEF

struct SYSCOLUMNS {
   char     colname[19];
   long     tabid;
   short    coltype;
}
 ;

extern struct SYSCOLUMNS syscolumns;

#define iDBCHAR 0
#define iDBSMALLINT 1
#define iDBINTEGER  2
#define iDBDECIMAL  5
#define iDBDATUM    7

#endif