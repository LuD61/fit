// DialogDatum.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "llmfc.h"
#include "DialogDatum.h"
#include "Datum.h"
#include "DbClass.h"
#include "strfkt.h"
#include "dbRange.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
extern DB_CLASS dbClass;
extern DB_RANGE dbRange;
extern short movon ;
extern short mobis ;
extern short mdn ;
extern short jr ;
extern short filvon ;
extern short filbis ;
extern long kunvon ;
extern long kunbis ;
extern short flg_gruppierung;
extern double gr1von;
extern double gr1bis;
extern double gr2von;
extern double gr2bis;
extern double gr3von;
extern double gr3bis;
extern double gr4von;
extern double gr4bis;
extern double gr5von;
extern double gr5bis;
extern double gr6von;
extern double gr6bis;
extern short gr1von_short;
extern short gr1bis_short;
extern short gr2von_short;
extern short gr2bis_short;
extern short gr3von_short;
extern short gr3bis_short;
extern short gr4von_short;
extern short gr4bis_short;
extern short gr5von_short;
extern short gr5bis_short;
extern short gr6von_short;
extern short gr6bis_short;

extern char checkgr1n[2] ;
extern char checkgr1s[2] ;
extern char checkgr2n[2] ;
extern char checkgr2s[2] ;
extern char checkgr3n[2] ;
extern char checkgr3s[2] ;
extern char checkgr4n[2] ;
extern char checkgr4s[2] ;
extern char checkgr5n[2] ;
extern char checkgr5s[2] ;
extern char checkgr6n[2] ;
extern char checkgr6s[2] ;

extern char R_hwg[2] ;
extern char R_wg[2] ;
extern char R_smt[2] ;
extern char R_ag[2] ;

extern char filbereich[256];
extern short monat;
extern char gr1bereich[256];
extern char gr2bereich[256];
extern char gr3bereich[256];
extern char gr4bereich[256];
extern char gr5bereich[256];
extern char gr6bereich[256];

extern int MitVorjahr;
extern DATE_STRUCT end_dat;
bool Monatok = FALSE;
char Textausgabe[500] ;
char *Bereichtext  = "Bereichseingabe :  Eingabebeispiele : 1-10 ; 1 bis 10 , 1,2,3, <20, >100, kleiner 20, gr��er 100, <> 15, nicht 15 ";

CForm *CDialogDatum::_Dialog [] = 
{
   new CForm (IDC_STATIC_BEREICH, (char *)      Textausgabe, FRMCHAR, 1,""),
   new CForm (IDC_MANDANT, (short *)      &mdn, FRMSHORT,0, "%hd"),
   new CForm (IDC_FILBEREICH, (char *)      filbereich, FRMCHAR,1, 128),
   new CForm (IDC_MONAT, (short *)      &monat, FRMSHORT, 0,"%hd"),
   new CForm (IDC_JAHR, (short *)      &jr, FRMSHORT,0, "%hd"),
   new CForm (IDC_GR1BEREICH, (char *)      gr1bereich, FRMCHAR, 1,128),
   new CForm (IDC_GR2BEREICH, (char *)      gr2bereich, FRMCHAR, 1,128),
   new CForm (IDC_GR3BEREICH, (char *)      gr3bereich, FRMCHAR, 1,128),
   new CForm (IDC_GR4BEREICH, (char *)      gr4bereich, FRMCHAR, 1,128),
   new CForm (IDC_GR5BEREICH, (char *)      gr5bereich, FRMCHAR, 1,128),
   new CForm (IDC_GR6BEREICH, (char *)      gr6bereich, FRMCHAR, 1,128),

   new CForm (IDC_CHECKGR1N, (CHAR *)      &checkgr1n, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR1S, (CHAR *)      &checkgr1s, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR2N, (CHAR *)      &checkgr2n, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR2S, (CHAR *)      &checkgr2s, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR3N, (CHAR *)      &checkgr3n, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR3S, (CHAR *)      &checkgr3s, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR4N, (CHAR *)      &checkgr4n, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR4S, (CHAR *)      &checkgr4s, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR5N, (CHAR *)      &checkgr5n, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR5S, (CHAR *)      &checkgr5s, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR6N, (CHAR *)      &checkgr6n, FRMCHARBOOL,0, ""),
   new CForm (IDC_CHECKGR6S, (CHAR *)      &checkgr6s, FRMCHARBOOL,0, ""),

   new CForm (IDC_RADIOHWG, (CHAR *)      &R_hwg, FRMCHARBOOL,0, ""),
   new CForm (IDC_RADIOWG, (CHAR *)      &R_wg, FRMCHARBOOL,0, ""),
   new CForm (IDC_RADIOSMT, (CHAR *)      &R_smt, FRMCHARBOOL,0, ""),
   new CForm (IDC_RADIOAG, (CHAR *)      &R_ag, FRMCHARBOOL,0, ""),


   NULL,
};

CVector CDialogDatum::Dialog ((void **) _Dialog);


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDialogDatum 


CDialogDatum::CDialogDatum(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogDatum::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}

BOOL CDialogDatum::OnInitDialog()
{
	CDialog::OnInitDialog();
    GetDlgItem (IDC_MANDANT)->SetFocus ();
    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);

	strcpy(filbereich,"1 bis 9999");
	monat = 1;
//	strcpy(gr1bereich,"???");
//	strcpy(gr2bereich,"???");
//	strcpy(gr3bereich,"???");
//	strcpy(gr4bereich,"???");
//	strcpy(gr5bereich,"???");
//	strcpy(gr6bereich,"???");
	Read ();

	return TRUE;
}

void CDialogDatum::EnableDialog (BOOL b)
{


    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->Enable (this, b);
    }
	if (b)
	{
	    GetDlgItem (IDC_MANDANT)->SetFocus ();
	    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
    }
}
void CDialogDatum::ToDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->ToScreen (this);
    }
}
void CDialogDatum::GetFormBereich (void)
{
	CWnd *cWnd;
    CForm *Form;

    Dialog.FirstPosition ();

    cWnd = GetFocus ();
	strcpy(Textausgabe,"");
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
	    if (cWnd == GetDlgItem (Form->GetId()))
		{
			if (Form->Bereich == 1)	
			{
				strcpy(Textausgabe,Bereichtext);
			}
			break;
		}
    }
	Read();
}


void CDialogDatum::FromDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();

    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->FromScreen (this);
    }
}

BOOL CDialogDatum::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
CWnd *cWnd;
char dat[12];
if (pMsg->message == WM_KEYDOWN)
{
	switch (pMsg->wParam)
	{
	case VK_F5 :      //so gehts nicht TODO
			cWnd = GetDlgItem (IDCANCEL);
			return CDialog::PreTranslateMessage(pMsg);
	case VK_RETURN : 
	case VK_TAB :
		    cWnd = GetFocus ();
			if (cWnd ==GetDlgItem (IDC_MANDANT))
			{
			
			    Write ();
				if (jr == 0)
				{
					/**
					dbClass.opendbase ("bws");
					dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
					dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);
					dbClass.sqlout ((short *) &jr, SQLSHORT, 0);
					dbClass.sqlout ((short *) &movon, SQLSHORT, 0);

					dbClass.sqlcomm ("select end_dat,jr,period  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= today  " 
						"order by end_dat desc ");
						*/
					mobis = movon;
					sysdate(dat);
					jr = get_jahr(dat);
					if (jr < 1900) jr = 2000 + jr;

					Read();
				}

			    GetDlgItem (IDC_FILBEREICH)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);

				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_FILBEREICH))
			{
			    Write ();
			    GetDlgItem (IDC_JAHR)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_JAHR))
			{
			    Write ();
			    GetDlgItem (IDC_MONAT)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_MONAT))
			{
			    Write ();
			    GetDlgItem (IDC_GR1BEREICH)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}

			if (cWnd ==GetDlgItem (IDC_GR1BEREICH))
			{
			    Write ();
			    GetDlgItem (IDC_GR2BEREICH)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}


			if (cWnd ==GetDlgItem (IDC_GR2BEREICH))
			{
			    Write ();
			    GetDlgItem (IDC_GR3BEREICH)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}


			if (cWnd ==GetDlgItem (IDC_GR3BEREICH))
			{
			    Write ();
			    GetDlgItem (IDC_GR4BEREICH)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}


			if (cWnd ==GetDlgItem (IDC_GR4BEREICH))
			{
			    Write ();
			    GetDlgItem (IDC_GR5BEREICH)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}


			if (cWnd ==GetDlgItem (IDC_GR5BEREICH))
			{
			    Write ();
			    GetDlgItem (IDC_GR6BEREICH)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}


			if (cWnd ==GetDlgItem (IDC_GR6BEREICH))
			{
			    Write ();
			    GetDlgItem (IDOK)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}


			if (cWnd ==GetDlgItem (IDOK))
			{
			    Write ();
			    GetDlgItem (IDC_MANDANT)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}

			if (cWnd ==GetDlgItem (IDCANCEL))
			{
				return CDialog::PreTranslateMessage(pMsg);
			}
			if (cWnd ==GetDlgItem (IDOK))
			{
			    Write ();
				return CDialog::PreTranslateMessage(pMsg);
			}
		    NextDlgCtrl();
		    return TRUE;

	}
}
	return CDialog::PreTranslateMessage(pMsg);
}




BOOL CDialogDatum::Read (void)
{
    ToDialog ();
//    EnableDialog (TRUE);
	return TRUE;
	
}

BOOL CDialogDatum::Write (void)
{

    FromDialog ();

return TRUE;
}

void CDialogDatum::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	char ctext[256];
	Write();
	if (strlen(clipped(filbereich)) == 0) strcpy(filbereich,"0");
	if (strlen(clipped(gr1bereich)) == 0) strcpy(gr1bereich,"0");
	if (strlen(clipped(gr2bereich)) == 0) strcpy(gr2bereich,"0");
	if (strlen(clipped(gr3bereich)) == 0) strcpy(gr3bereich,"0");
	if (strlen(clipped(gr4bereich)) == 0) strcpy(gr4bereich,"0");
	if (strlen(clipped(gr5bereich)) == 0) strcpy(gr5bereich,"0");
	if (strlen(clipped(gr6bereich)) == 0) strcpy(gr6bereich,"0");

    if (dbRange.PruefeRange(filbereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Filiale", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_FILBEREICH)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}
    if (dbRange.PruefeRange(gr1bereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Gruppe 1", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_GR1BEREICH)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}
    if (dbRange.PruefeRange(gr2bereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Gruppe 2", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_GR2BEREICH)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}
    if (dbRange.PruefeRange(gr3bereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Gruppe 3", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_GR3BEREICH)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}
    if (dbRange.PruefeRange(gr4bereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Gruppe 4", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_GR4BEREICH)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}
    if (dbRange.PruefeRange(gr5bereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Gruppe 5", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_GR5BEREICH)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}
    if (dbRange.PruefeRange(gr6bereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Gruppe 6", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_GR6BEREICH)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}
	CDialog::OnOK();
}


void CDialogDatum::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogDatum, CDialog)
	//{{AFX_MSG_MAP(CDialogDatum)
	ON_BN_CLICKED(IDC_CHECKGR1N, OnCheckgr1n)
	ON_BN_CLICKED(IDC_CHECKGR1S, OnCheckgr1s)
	ON_BN_CLICKED(IDC_CHECKGR2N, OnCheckgr2n)
	ON_BN_CLICKED(IDC_CHECKGR2S, OnCheckgr2s)
	ON_BN_CLICKED(IDC_CHECKGR3N, OnCheckgr3n)
	ON_BN_CLICKED(IDC_CHECKGR3S, OnCheckgr3s)
	ON_BN_CLICKED(IDC_CHECKGR4N, OnCheckgr4n)
	ON_BN_CLICKED(IDC_CHECKGR4S, OnCheckgr4s)
	ON_BN_CLICKED(IDC_CHECKGR5N, OnCheckgr5n)
	ON_BN_CLICKED(IDC_CHECKGR5S, OnCheckgr5s)
	ON_BN_CLICKED(IDC_CHECKGR6N, OnCheckgr6n)
	ON_BN_CLICKED(IDC_CHECKGR6S, OnCheckgr6s)
	ON_BN_CLICKED(IDC_RADIOAG, OnRadioag)
	ON_BN_CLICKED(IDC_RADIOHWG, OnRadiohwg)
	ON_BN_CLICKED(IDC_RADIOSMT, OnRadiosmt)
	ON_BN_CLICKED(IDC_RADIOWG, OnRadiowg)
	ON_EN_CHANGE(IDC_FILBEREICH, OnChangeFilbereich)
	ON_EN_SETFOCUS(IDC_JAHR, OnSetfocusJahr)
	ON_EN_SETFOCUS(IDC_MANDANT, OnSetfocusMandant)
	ON_EN_SETFOCUS(IDC_FILBEREICH, OnSetfocusFilbereich)
	ON_EN_SETFOCUS(IDC_MONAT, OnSetfocusMonat)
	ON_EN_SETFOCUS(IDC_GR1BEREICH, OnSetfocusGr1bereich)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDialogDatum 


void CDialogDatum::OnCloseupCombo1() 
{
	
}


void CDialogDatum::OnCheckgr1n() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr1s() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr2n() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr2s() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr3n() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr3s() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr4n() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr4s() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr5n() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr5s() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr6n() 
{
	Write();
	
}

void CDialogDatum::OnCheckgr6s() 
{
	Write();
	
}

void CDialogDatum::OnRadioag() 
{
	Write();
	if (strcmp(R_ag,"J")== 0 ) 
	{
		flg_gruppierung = 3; 
	}
	
}

void CDialogDatum::OnRadiohwg() 
{
	Write();
	if (strcmp(R_hwg,"J")== 0 ) 
	{
		flg_gruppierung = 1; 
	}
	
}

void CDialogDatum::OnRadiosmt() 
{
	Write();
	if (strcmp(R_smt,"J")== 0 ) 
	{
		flg_gruppierung = 4; 
	}
	
}

void CDialogDatum::OnRadiowg() 
{
	Write();
	if (strcmp(R_wg,"J")== 0 ) 
	{
		flg_gruppierung = 2; 
	}
	
}

void CDialogDatum::OnChangeFilbereich() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnSetfocusJahr() 
{
	GetFormBereich ();
	
}

void CDialogDatum::OnSetfocusMandant() 
{
	GetFormBereich ();
	
}


void CDialogDatum::OnSetfocusFilbereich() 
{
	GetFormBereich ();
	
}

void CDialogDatum::OnSetfocusMonat() 
{
	GetFormBereich ();
	
}

void CDialogDatum::OnSetfocusGr1bereich() 
{
	GetFormBereich ();
	
}
