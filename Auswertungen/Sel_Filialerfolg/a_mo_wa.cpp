#include "stdafx.h"
#include "DbClass.h"
#include "DbRange.h"
#include "datum.h"
#include "a_mo_wa.h"
#include "cmbtl9.h"
#include "adr.h"

struct A_MO_WA a_mo_wa, a_mo_wa_null;
struct AUSGABE ausgabe, ausgabe_m,ausgabe_b,ausgabe_vj_m,ausgabe_vj_b,
				ausgabegr_m,ausgabegr_b,ausgabegr_vj_m,ausgabegr_vj_b,
				ausgabeges_m,ausgabeges_b,ausgabeges_vj_m,ausgabeges_vj_b,ausgabe_we, ausgabe_null;

struct A_BAS a_bas, a_bas_null;
struct ADR adr, adr_null;
extern DB_CLASS dbClass;
extern DB_RANGE dbRange;
static long anzfelder = -1;
short kun_fil = 1;
short mdn = 1;
short filvon = 1 ;
short filbis = 999;
long kunvon = 1 ;
long kunbis = 999999;
char filbereich [256] ;   
short monat = 1 ;   
short monat_von = 1 ;   
short monat_bis = 1 ;   
char gr1bereich [256] ;   
char gr2bereich [256] ;   
char gr3bereich [256] ;   
char gr4bereich [256] ;   
char gr5bereich [256] ;   
char gr6bereich [256] ;   
extern char datvon[12] ;
extern char datbis[12] ;
short wgvon = 1;
short wgbis = 9999;
double avon = 1;
double abis = 9999999999;
long max_anz = 999999;
short jr = 0;
short kw = 0;
short vjr = 0;
short vvjr = 0;
short movon ;
short mobis ;

short flg_gruppierung;
BOOL flg_kosten_holen = FALSE;
BOOL flg_kosten_gr_holen = FALSE;
BOOL flg_kosten_ges_holen = FALSE;
double gr1von;
double gr1bis;
double gr2von;
double gr2bis;
double gr3von;
double gr3bis;
double gr4von;
double gr4bis;
double gr5von;
double gr5bis;
double gr6von;
double gr6bis;
short gr1von_short;
short gr1bis_short;
short gr2von_short;
short gr2bis_short;
short gr3von_short;
short gr3bis_short;
short gr4von_short;
short gr4bis_short;
short gr5von_short;
short gr5bis_short;
short gr6von_short;
short gr6bis_short;

char checkgr1n[2] ;
char checkgr1s[2] ;
char checkgr2n[2] ;
char checkgr2s[2] ;
char checkgr3n[2] ;
char checkgr3s[2] ;
char checkgr4n[2] ;
char checkgr4s[2] ;
char checkgr5n[2] ;
char checkgr5s[2] ;
char checkgr6n[2] ;
char checkgr6s[2] ;
char R_hwg[2] ;
char R_wg[2] ;
char R_smt[2] ;
char R_ag[2] ;



BOOL flg_wo1_gekappt = FALSE;
BOOL flg_wo1vj_gekappt = FALSE;
short erste_wo ;
short letzte_wo ;
short wo1 = 0;
short wo2 = 0;
short wo3 = 0;
short wo4 = 0;
short wo5 = 0;
short wo1_vj = 0;
short wo2_vj = 0;
short wo3_vj = 0;
short wo4_vj = 0;
short wo5_vj = 0;
short woche ;
short woche_jr;
int _jr;
int akt_mo = 0;
int akt_jr = 0;
int _mo, _wo;
int _tag;

char datumvj_von[12];
char datumvj_bis[12];
char datumjr_von[12];
char datumjr_bis[12];
char datummo_von[12];
char datummo_bis[12];
char datumvjmo_von[12];
char datumvjmo_bis[12];
char erster_tag[12];
char letzter_tag[12];
char erster_tag_wo1[12];
char letzter_tag_wo1[12];
char erster_tag_wo2[12];
char letzter_tag_wo2[12];
char erster_tag_wo3[12];
char letzter_tag_wo3[12];
char erster_tag_wo4[12];
char letzter_tag_wo4[12];
char erster_tag_wo5[12];
char letzter_tag_wo5[12];
char erster_tag_wo1_vj[12];
char letzter_tag_wo1_vj[12];
char erster_tag_wo2_vj[12];
char letzter_tag_wo2_vj[12];
char erster_tag_wo3_vj[12];
char letzter_tag_wo3_vj[12];
char erster_tag_wo4_vj[12];
char letzter_tag_wo4_vj[12];
char erster_tag_wo5_vj[12];
char letzter_tag_wo5_vj[12];






extern char order_by_zusatz[7] ;
extern char order_by[11] ;
extern char BezRennerPenner [] ;
extern char BezSortierung [] ;

DATUM d_dat, d_datfirst, d_datlast;
DATE_STRUCT end_dat;
double a_gew;
extern char Listenname[128];
char sys_par_wrt [2] = " ";




static char *clipped (char *str)
/**
Blank am Ende eines Strings abschneiden.
**/
{
          int i;

          i = strlen (str);
          if (i == 0) return (str);

          for (i = i - 1; i > 0; i --)
          {
                    if (str[i] > ' ')
                    {
                              str [i + 1] = (char) 0;
                              break;
                    }
          }
          return (str);
}



int A_MO_WA_CLASS::dbcount (void)
/**
Tabelle eti lesen.
**/
{


         if (test_upd_cursor == -1)
         {
			dbClass.sqlout ((char *) sys_par_wrt, SQLCHAR, 2);
			int dsqlstatus = dbClass.sqlcomm ("select sys_par_wrt from sys_par where sys_par_nam = \"abwper_par\""); 
			if (dsqlstatus == 0)
			{
				if (atoi(sys_par_wrt) == 1) 
				{
					prepare_per ();
				}
				else
				{
		             prepare ();
				}
			}
			else
			{
	             prepare ();
			}


         }

	  ausgabe.ges_menge = 0.0;
	  ausgabe.ges_me_gr1 = 0.0;
	  ausgabe.ges_me_gr2 = 0.0;
	  ausgabe.ges_me_gr3 = 0.0;
	  ausgabe.ges_me_gr4 = 0.0;
	  ausgabe.ges_me_gr5 = 0.0;
	  ausgabe.ges_me_gr6 = 0.0;
	  ausgabe.ges_umsatz = 0.0;
	  ausgabe.ges_ums_gr1 = 0.0;
	  ausgabe.ges_ums_gr2 = 0.0;
	  ausgabe.ges_ums_gr3 = 0.0;
	  ausgabe.ges_ums_gr4 = 0.0;
	  ausgabe.ges_ums_gr5 = 0.0;
	  ausgabe.ges_ums_gr6 = 0.0;

//		 berechnePerioden ();

	    memcpy (&ausgabegr_m, &ausgabe_null, sizeof (struct AUSGABE));
	    memcpy (&ausgabegr_b, &ausgabe_null, sizeof (struct AUSGABE));
	    memcpy (&ausgabegr_vj_m, &ausgabe_null, sizeof (struct AUSGABE));
	    memcpy (&ausgabegr_vj_b, &ausgabe_null, sizeof (struct AUSGABE));
	    memcpy (&ausgabeges_m, &ausgabe_null, sizeof (struct AUSGABE));
	    memcpy (&ausgabeges_b, &ausgabe_null, sizeof (struct AUSGABE));
	    memcpy (&ausgabeges_vj_m, &ausgabe_null, sizeof (struct AUSGABE));
	    memcpy (&ausgabeges_vj_b, &ausgabe_null, sizeof (struct AUSGABE));

         dbClass.sqlopen (count_cursor);
         sqlstatus = dbClass.sqlfetch (count_cursor);
		 if (sqlstatus != 0) return -1 ;
		 anzfelder = 0;
		 strcpy(ausgabegr_m.fil_kla,a_mo_wa.fil_kla);
		 strcpy(ausgabegr_vj_m.fil_kla,a_mo_wa.fil_kla);
		 strcpy(ausgabegr_b.fil_kla,a_mo_wa.fil_kla);
		 strcpy(ausgabegr_vj_b.fil_kla,a_mo_wa.fil_kla);
         while (sqlstatus == 0)
         {
			 anzfelder ++;
	         dbClass.sqlopen (fil_kost_cursor);
	         while (sqlstatus == 0)
		     {
				 sqlstatus = dbClass.sqlfetch (fil_kost_cursor);
				 if (sqlstatus == 0) anzfelder ++;
			 }

	         sqlstatus = dbClass.sqlfetch (count_cursor);
         }
         dbClass.sqlopen (count_cursor);

         dbClass.sqlopen (fil_kla_cursor);
         sqlstatus = dbClass.sqlfetch (fil_kla_cursor);
		 if (sqlstatus != 0) return -1 ;
         while (sqlstatus == 0)
         {
			 anzfelder ++;

			 strcpy(ausgabe.fil_kla,a_mo_wa.fil_kla);
	         dbClass.sqlopen (fil_kost_cursor_gr);
			 sqlstatus = 0;
	         while (sqlstatus == 0)
		     {
				 sqlstatus = dbClass.sqlfetch (fil_kost_cursor_gr);
				 if (sqlstatus == 0) anzfelder ++;
			 }
	         sqlstatus = dbClass.sqlfetch (fil_kla_cursor);
         }
         dbClass.sqlopen (fil_kla_cursor);
	         dbClass.sqlopen (fil_kost_cursor_ges);
			 sqlstatus = 0;
	         while (sqlstatus == 0)
		     {
				 sqlstatus = dbClass.sqlfetch (fil_kost_cursor_ges);
				 if (sqlstatus == 0) anzfelder ++;
			 }

		 anzfelder ++;   // f�r die Gesamtsumme
         return anzfelder; 
}


int A_MO_WA_CLASS::leseDS (int fehlercode)
{
	int dsqlstatus;
	int dfil = 0;
	  kun_fil = 1; // nur Filialen
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }

		ausgabe.fil = 0;
		monat_bis = monat;
		if (flg_kosten_holen == TRUE)
		{
			ausgabe_b.fil = 0;
//		    memcpy (&ausgabe_m, &ausgabe_null, sizeof (struct AUSGABE));
//			memcpy (&ausgabe_b, &ausgabe_null, sizeof (struct AUSGABE));
//			memcpy (&ausgabe_vj_m, &ausgabe_null, sizeof (struct AUSGABE));
//			memcpy (&ausgabe_vj_b, &ausgabe_null, sizeof (struct AUSGABE));

	        ausgabe.kost_mo = 0.0;
	        ausgabe.kost_mo_vj = 0.0;
	        ausgabe.kost_b = 0.0;
	        ausgabe.kost_b_vj = 0.0;

			sqlstatus = dbClass.sqlfetch (fil_kost_cursor);
			if (sqlstatus == 0) return 0;
			flg_kosten_holen = FALSE;
		}
		if (flg_kosten_gr_holen == TRUE)
		{
			ausgabe_b.fil = 0;
//		    memcpy (&ausgabe_m, &ausgabe_null, sizeof (struct AUSGABE));
//			memcpy (&ausgabe_b, &ausgabe_null, sizeof (struct AUSGABE));
//			memcpy (&ausgabe_vj_m, &ausgabe_null, sizeof (struct AUSGABE));
//			memcpy (&ausgabe_vj_b, &ausgabe_null, sizeof (struct AUSGABE));
	        ausgabe.kost_mo = 0.0;
	        ausgabe.kost_mo_vj = 0.0;
	        ausgabe.kost_b = 0.0;
	        ausgabe.kost_b_vj = 0.0;

			sqlstatus = dbClass.sqlfetch (fil_kost_cursor_gr);
			if (sqlstatus == 0) return 0;
			flg_kosten_gr_holen = FALSE;
		}
		if (flg_kosten_ges_holen == TRUE)
		{
	        ausgabe.kost_mo = 0.0;
	        ausgabe.kost_mo_vj = 0.0;
	        ausgabe.kost_b = 0.0;
	        ausgabe.kost_b_vj = 0.0;
			sqlstatus = dbClass.sqlfetch (fil_kost_cursor_ges);
			if (sqlstatus == 0) return 0;
			flg_kosten_ges_holen = FALSE;
			ausgabe_b.fil = 0;
//		    memcpy (&ausgabe_m, &ausgabe_null, sizeof (struct AUSGABE));
//			memcpy (&ausgabe_b, &ausgabe_null, sizeof (struct AUSGABE));
//			memcpy (&ausgabe_vj_m, &ausgabe_null, sizeof (struct AUSGABE));
//			memcpy (&ausgabe_vj_b, &ausgabe_null, sizeof (struct AUSGABE));
		}

      if (strcmp (ausgabeges_m.fil_kla, "-")   == 0) 
	  {
		  return 100;
		  //jetzt raus, bin fertig
	  }

	  if (strlen(ausgabegr_m.fil_kla) == 0) //vorher war Gruppenwechsel, daher muss das sqlfetch jetzt �bersprungen werden
	  {
			strcpy(ausgabegr_m.fil_kla,a_mo_wa.fil_kla);
			strcpy(ausgabegr_vj_m.fil_kla,a_mo_wa.fil_kla);
			strcpy(ausgabegr_b.fil_kla,a_mo_wa.fil_kla);
			strcpy(ausgabegr_vj_b.fil_kla,a_mo_wa.fil_kla);
	  }
	  else
	  {
	    memcpy (&ausgabe_m, &ausgabe_null, sizeof (struct AUSGABE));
		memcpy (&ausgabe_b, &ausgabe_null, sizeof (struct AUSGABE));
		memcpy (&ausgabe_vj_m, &ausgabe_null, sizeof (struct AUSGABE));
		memcpy (&ausgabe_vj_b, &ausgabe_null, sizeof (struct AUSGABE));
		fehlercode = dbClass.sqlfetch (count_cursor);
	  }
	  if (fehlercode == 0) 
      {
	  
		vjr = jr -1;
		monat_von = monat;   //nur Monat x
	    memcpy (&ausgabe, &ausgabe_null, sizeof (struct AUSGABE));
         dbClass.sqlopen (readcursor);
		dsqlstatus = dbClass.sqlfetch (readcursor);
	    memcpy (&ausgabe_we, &ausgabe_null, sizeof (struct AUSGABE));
         dbClass.sqlopen (readcursor_we);
		 dbClass.sqlfetch (readcursor_we);
		if (dsqlstatus == 0) 
		{
	        if (strcmp (ausgabegr_m.fil_kla, a_mo_wa.fil_kla)   != 0) 
			{
				//Gruppenwechsel hat stattgefunden
			    memcpy (&ausgabe_m, &ausgabegr_m, sizeof (struct AUSGABE));
				memcpy (&ausgabe_b, &ausgabegr_b, sizeof (struct AUSGABE));
				memcpy (&ausgabe_vj_m, &ausgabegr_vj_m, sizeof (struct AUSGABE));
				memcpy (&ausgabe_vj_b, &ausgabegr_vj_b, sizeof (struct AUSGABE));
				ausgabe.mo_einn = ausgabegr_m.mo_einn;
				ausgabe.ges_einn = ausgabegr_m.ges_einn;
				ausgabe.vj_mo_einn = ausgabegr_m.vj_mo_einn;
				ausgabe.vj_ges_einn = ausgabegr_m.vj_ges_einn;

				//HAUS-6
				ausgabe.mo_einn_bto = ausgabegr_m.mo_einn_bto;
				ausgabe.ges_einn_bto = ausgabegr_m.ges_einn_bto;
				ausgabe.vj_mo_einn_bto = ausgabegr_m.vj_mo_einn_bto;
				ausgabe.vj_ges_einn_bto = ausgabegr_m.vj_ges_einn_bto;

				ausgabe.mo_kun_anz_int = ausgabegr_m.mo_kun_anz_int;
				ausgabe.ges_kun_anz_int = ausgabegr_m.ges_kun_anz_int;
				ausgabe.vj_mo_kun_anz_int = ausgabegr_m.vj_mo_kun_anz_int;
				ausgabe.vj_ges_kun_anz_int = ausgabegr_m.vj_ges_kun_anz_int;
				
				ausgabe.mo_stunden = ausgabegr_m.mo_stunden;
				ausgabe.ges_stunden = ausgabegr_m.ges_stunden;
				ausgabe.vj_mo_stunden = ausgabegr_m.vj_mo_stunden;
				ausgabe.vj_ges_stunden = ausgabegr_m.vj_ges_stunden;
				
				ausgabe.mo_z_me_vk = ausgabegr_m.mo_z_me_vk;
				ausgabe.ges_z_me_vk = ausgabegr_m.ges_z_me_vk;
				ausgabe.vj_mo_z_me_vk = ausgabegr_m.vj_mo_z_me_vk;
				ausgabe.vj_ges_z_me_vk = ausgabegr_m.vj_ges_z_me_vk;

				ausgabe.mo_z_ums_vk = ausgabegr_m.mo_z_ums_vk;
				ausgabe.ges_z_ums_vk = ausgabegr_m.ges_z_ums_vk;
				ausgabe.vj_mo_z_ums_vk = ausgabegr_m.vj_mo_z_ums_vk;
				ausgabe.vj_ges_z_ums_vk = ausgabegr_m.vj_ges_z_ums_vk;

				ausgabe.mo_z_ums_fil_ek = ausgabegr_m.mo_z_ums_fil_ek;
				ausgabe.ges_z_ums_fil_ek = ausgabegr_m.ges_z_ums_fil_ek;
				ausgabe.vj_mo_z_ums_fil_ek = ausgabegr_m.vj_mo_z_ums_fil_ek;
				ausgabe.vj_ges_z_ums_fil_ek = ausgabegr_m.vj_ges_z_ums_fil_ek;



				memcpy (&ausgabegr_m, &ausgabe_null, sizeof (struct AUSGABE));
				memcpy (&ausgabegr_b, &ausgabe_null, sizeof (struct AUSGABE));
				memcpy (&ausgabegr_vj_m, &ausgabe_null, sizeof (struct AUSGABE));
				memcpy (&ausgabegr_vj_b, &ausgabe_null, sizeof (struct AUSGABE));
				ausgabe.fil = 32000;
				strcpy(ausgabe.fil_kla,ausgabe_m.fil_kla); 


				sqlstatus = dbClass.sqlopen (fil_kost_cursor_gr);
				flg_kosten_gr_holen = TRUE;
				return 0;
			}
		}

		if (dsqlstatus == 0) 
		{
		    fuelle_ausgabe(); 
			fuelle_summen_m();
			if (a_mo_wa.jr == jr)	memcpy (&ausgabe_m, &ausgabe, sizeof (struct AUSGABE));
			if (a_mo_wa.jr == vjr)	memcpy (&ausgabe_vj_m, &ausgabe, sizeof (struct AUSGABE));
		}
//	    memcpy (&ausgabe, &ausgabe_null, sizeof (struct AUSGABE));
		dfil = ausgabe.fil;
		memcpy (&ausgabe, &ausgabe_null, sizeof (struct AUSGABE));
		ausgabe.fil = dfil;
		dsqlstatus = dbClass.sqlfetch (readcursor);  //2. fetch = n�chstes Jahr
	    memcpy (&ausgabe_we, &ausgabe_null, sizeof (struct AUSGABE));
         dbClass.sqlopen (readcursor_we);
		dbClass.sqlfetch (readcursor_we);
		if (dsqlstatus == 0) 
		{
		    fuelle_ausgabe();
			fuelle_summen_m();
			if (a_mo_wa.jr == jr)	memcpy (&ausgabe_m, &ausgabe, sizeof (struct AUSGABE));
			if (a_mo_wa.jr == vjr)	memcpy (&ausgabe_vj_m, &ausgabe, sizeof (struct AUSGABE));
		}

		monat_von = 1;  // Monat 1 bis x
//	    memcpy (&ausgabe, &ausgabe_null, sizeof (struct AUSGABE));
         dbClass.sqlopen (readcursor);
		memcpy (&ausgabe, &ausgabe_null, sizeof (struct AUSGABE));
		dsqlstatus = dbClass.sqlfetch (readcursor);
         dbClass.sqlopen (readcursor_we);
	    memcpy (&ausgabe_we, &ausgabe_null, sizeof (struct AUSGABE));
		dbClass.sqlfetch (readcursor_we);
		if (dsqlstatus == 0) 
		{
		    fuelle_ausgabe();
			fuelle_summen_b();
			if (a_mo_wa.jr == jr)	memcpy (&ausgabe_b, &ausgabe, sizeof (struct AUSGABE));
			if (a_mo_wa.jr == vjr)	memcpy (&ausgabe_vj_b, &ausgabe, sizeof (struct AUSGABE));
		}
//	    memcpy (&ausgabe, &ausgabe_null, sizeof (struct AUSGABE));
		memcpy (&ausgabe, &ausgabe_null, sizeof (struct AUSGABE));
		dsqlstatus = dbClass.sqlfetch (readcursor);  //2. fetch = n�chstes Jahr
	    memcpy (&ausgabe_we, &ausgabe_null, sizeof (struct AUSGABE));
		ausgabe.fil = dfil;
		strcpy(ausgabe.fil_kla,a_mo_wa.fil_kla); //281009
         dbClass.sqlopen (readcursor_we);
		dbClass.sqlfetch (readcursor_we);
		if (dsqlstatus == 0) 
		{
		    fuelle_ausgabe();
			fuelle_summen_b();
			if (a_mo_wa.jr == jr)	memcpy (&ausgabe_b, &ausgabe, sizeof (struct AUSGABE));
			if (a_mo_wa.jr == vjr)	memcpy (&ausgabe_vj_b, &ausgabe, sizeof (struct AUSGABE));
		}



            if (kun_fil == 0)
			{
				dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
				dbClass.sqlout ((char *) adr.adr_krz, SQLCHAR, 17);
				dbClass.sqlout ((char *) adr.adr_nam1, SQLCHAR, 37);
				dbClass.sqlout ((char *) adr.plz, SQLCHAR, 9);
				dbClass.sqlout ((char *) adr.ort1, SQLCHAR, 37);
				dbClass.sqlcomm ("select adr.adr_krz,adr.adr_nam1,adr.plz,adr.ort1 from kun,adr where kun = ?  and kun.adr1 = adr.adr");
			}
			else
			{
				dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
				dbClass.sqlout ((char *) adr.adr_krz, SQLCHAR, 17);
				dbClass.sqlout ((char *) adr.adr_nam1, SQLCHAR, 37);
				dbClass.sqlout ((char *) adr.plz, SQLCHAR, 9);
				dbClass.sqlout ((char *) adr.ort1, SQLCHAR, 37);
				dbClass.sqlcomm ("select adr.adr_krz,adr.adr_nam1,adr.plz,adr.ort1 from fil,adr where fil.fil = ? and fil.adr = adr.adr ");
			}






		monat_bis = monat;
		monat = 1;
			ausgabe.mo_einn = 0.0;
			ausgabe.ges_einn = 0.0;
			ausgabe.vj_mo_einn = 0.0;
			ausgabe.vj_ges_einn = 0.0;

			ausgabe.mo_einn_bto = 0.0;
			ausgabe.ges_einn_bto = 0.0;
			ausgabe.vj_mo_einn_bto = 0.0;
			ausgabe.vj_ges_einn_bto = 0.0;

			ausgabe.kun_anz_int = 0.0;
			ausgabe.ges_kun_anz_int = 0.0;
			ausgabe.vj_mo_kun_anz_int = 0.0;
			ausgabe.vj_ges_kun_anz_int = 0.0;

		while (monat <= monat_bis)
		{
			a_mo_wa.mo = monat;
			berechnePerioden ();
			ausgabe.einn = 0.0;
			sqlstatus = dbClass.sqlopen (read_fil_einn);
			sqlstatus = dbClass.sqlfetch (read_fil_einn);
			while (sqlstatus == 0)
			{
				if (ausgabe.jr == 2)
				{
					if (monat == monat_bis)  
					{
						ausgabe.mo_einn += ausgabe.einn; 
						ausgabegr_m.mo_einn += ausgabe.einn; 
						ausgabeges_m.mo_einn += ausgabe.einn; 

						ausgabe.mo_einn_bto += ausgabe.einn_bto; 
						ausgabegr_m.mo_einn_bto += ausgabe.einn_bto; 
						ausgabeges_m.mo_einn_bto += ausgabe.einn_bto; 

						ausgabe.mo_kun_anz_int += ausgabe.kun_anz_int; 
						ausgabegr_m.mo_kun_anz_int += ausgabe.kun_anz_int; 
						ausgabeges_m.mo_kun_anz_int += ausgabe.kun_anz_int; 
					}
					ausgabe.ges_einn += ausgabe.einn; //Summe 
					ausgabegr_m.ges_einn += ausgabe.einn; //Summe 
					ausgabeges_m.ges_einn += ausgabe.einn; //Summe 

					ausgabe.ges_einn_bto += ausgabe.einn_bto; //Summe 
					ausgabegr_m.ges_einn_bto += ausgabe.einn_bto; //Summe 
					ausgabeges_m.ges_einn_bto += ausgabe.einn_bto; //Summe 

					ausgabe.ges_kun_anz_int += ausgabe.kun_anz_int; //Summe 
					ausgabegr_m.ges_kun_anz_int += ausgabe.kun_anz_int; //Summe 
					ausgabeges_m.ges_kun_anz_int += ausgabe.kun_anz_int; //Summe 
				}
				else
				{
					if (monat == monat_bis) 
					{
						ausgabe.vj_mo_einn += ausgabe.einn; 
						ausgabegr_m.vj_mo_einn += ausgabe.einn; 
						ausgabeges_m.vj_mo_einn += ausgabe.einn; 

						ausgabe.vj_mo_einn_bto += ausgabe.einn_bto; 
						ausgabegr_m.vj_mo_einn_bto += ausgabe.einn_bto; 
						ausgabeges_m.vj_mo_einn_bto += ausgabe.einn_bto; 

						ausgabe.vj_mo_kun_anz_int += ausgabe.kun_anz_int; 
						ausgabegr_m.vj_mo_kun_anz_int += ausgabe.kun_anz_int; 
						ausgabeges_m.vj_mo_kun_anz_int += ausgabe.kun_anz_int; 

					}
					ausgabe.vj_ges_einn += ausgabe.einn; //Summe 
					ausgabegr_m.vj_ges_einn += ausgabe.einn; //Summe 
					ausgabeges_m.vj_ges_einn += ausgabe.einn; //Summe 

					ausgabe.vj_ges_einn_bto += ausgabe.einn_bto; //Summe 
					ausgabegr_m.vj_ges_einn_bto += ausgabe.einn_bto; //Summe 
					ausgabeges_m.vj_ges_einn_bto += ausgabe.einn_bto; //Summe 

					ausgabe.vj_ges_kun_anz_int += ausgabe.kun_anz_int; //Summe 
					ausgabegr_m.vj_ges_kun_anz_int += ausgabe.kun_anz_int; //Summe 
					ausgabeges_m.vj_ges_kun_anz_int += ausgabe.kun_anz_int; //Summe 

			}
			sqlstatus = dbClass.sqlfetch (read_fil_einn); 
		}
		monat ++;
	}
	monat--;


//HAUS-6  Lohnstunden holen
			ausgabe.mo_stunden = 0.0;
			ausgabe.ges_stunden = 0.0;
			ausgabe.vj_mo_stunden = 0.0;
			ausgabe.vj_ges_stunden = 0.0;
		sqlstatus = dbClass.sqlopen (read_lohnstunden);
		sqlstatus = dbClass.sqlfetch (read_lohnstunden);
		while (sqlstatus == 0)
		{
			if (ausgabe.jr == 2)
			{
				if (ausgabe.monat == monat_bis) 
				{
					ausgabe.mo_stunden += ausgabe.stunden; 
					ausgabegr_m.mo_stunden += ausgabe.stunden; 
					ausgabeges_m.mo_stunden += ausgabe.stunden; 
				}
				ausgabe.ges_stunden += ausgabe.stunden; //Summe 
				ausgabegr_m.ges_stunden += ausgabe.stunden; //Summe 
				ausgabeges_m.ges_stunden += ausgabe.stunden; //Summe 
			}
			else
			{
				if (ausgabe.monat == monat_bis) 
				{
					ausgabe.vj_mo_stunden += ausgabe.stunden; 
					ausgabegr_m.vj_mo_stunden += ausgabe.stunden; 
					ausgabeges_m.vj_mo_stunden += ausgabe.stunden; 
				}
				ausgabe.vj_ges_stunden += ausgabe.stunden; //Summe 
				ausgabegr_m.vj_ges_stunden += ausgabe.stunden; //Summe 
				ausgabeges_m.vj_ges_stunden += ausgabe.stunden; //Summe 
			}
			sqlstatus = dbClass.sqlfetch (read_lohnstunden); 
		}


//HAUS-6  Zusatz holen
		ausgabe.mo_z_me_vk = 0.0;
		ausgabe.ges_z_me_vk = 0.0;
		ausgabe.vj_mo_z_me_vk = 0.0;
		ausgabe.vj_ges_z_me_vk = 0.0;

		ausgabe.mo_z_ums_vk = 0.0;
		ausgabe.ges_z_ums_vk = 0.0;
		ausgabe.vj_mo_z_ums_vk = 0.0;
		ausgabe.vj_ges_z_ums_vk = 0.0;

		ausgabe.mo_z_ums_fil_ek = 0.0;
		ausgabe.ges_z_ums_fil_ek = 0.0;
		ausgabe.vj_mo_z_ums_fil_ek = 0.0;
		ausgabe.vj_ges_z_ums_fil_ek = 0.0;

		sqlstatus = dbClass.sqlopen (read_z_ums);
		sqlstatus = dbClass.sqlfetch (read_z_ums);
		while (sqlstatus == 0)
		{
			if (ausgabe.jr == 2)
			{
				if (ausgabe.monat == monat_bis) 
				{
					ausgabe.mo_z_me_vk += ausgabe.z_me_vk; 
					ausgabegr_m.mo_z_me_vk += ausgabe.z_me_vk; 
					ausgabeges_m.mo_z_me_vk += ausgabe.z_me_vk; 

					ausgabe.mo_z_ums_vk += ausgabe.z_ums_vk; 
					ausgabegr_m.mo_z_ums_vk += ausgabe.z_ums_vk; 
					ausgabeges_m.mo_z_ums_vk += ausgabe.z_ums_vk; 

					ausgabe.mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
					ausgabegr_m.mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
					ausgabeges_m.mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
				}
				ausgabe.ges_z_me_vk += ausgabe.z_me_vk; //Summe 
				ausgabegr_m.ges_z_me_vk += ausgabe.z_me_vk; //Summe 
				ausgabeges_m.ges_z_me_vk += ausgabe.z_me_vk; //Summe 

				ausgabe.ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 
				ausgabegr_m.ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 
				ausgabeges_m.ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 

				ausgabe.ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
				ausgabegr_m.ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
				ausgabeges_m.ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
			}
			else
			{
				if (ausgabe.monat == monat_bis) 
				{
					ausgabe.vj_mo_z_me_vk += ausgabe.z_me_vk; 
					ausgabegr_m.vj_mo_z_me_vk += ausgabe.z_me_vk; 
					ausgabeges_m.vj_mo_z_me_vk += ausgabe.z_me_vk; 

					ausgabe.vj_mo_z_ums_vk += ausgabe.z_ums_vk; 
					ausgabegr_m.vj_mo_z_ums_vk += ausgabe.z_ums_vk; 
					ausgabeges_m.vj_mo_z_ums_vk += ausgabe.z_ums_vk; 

					ausgabe.vj_mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
					ausgabegr_m.vj_mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
					ausgabeges_m.vj_mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
				}
				ausgabe.vj_ges_z_me_vk += ausgabe.z_me_vk; //Summe 
				ausgabegr_m.vj_ges_z_me_vk += ausgabe.z_me_vk; //Summe 
				ausgabeges_m.vj_ges_z_me_vk += ausgabe.z_me_vk; //Summe 

				ausgabe.vj_ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 
				ausgabegr_m.vj_ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 
				ausgabeges_m.vj_ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 

				ausgabe.vj_ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
				ausgabegr_m.vj_ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
				ausgabeges_m.vj_ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
		}
		sqlstatus = dbClass.sqlfetch (read_z_ums); 
	}



		sqlstatus = dbClass.sqlopen (read_z_ums_we);
		sqlstatus = dbClass.sqlfetch (read_z_ums_we);
		while (sqlstatus == 0)
		{
			if (ausgabe.jr == 2)
			{
				if (ausgabe.monat == monat_bis) 
				{
					ausgabe.mo_z_me_vk += ausgabe.z_me_vk; 
					ausgabegr_m.mo_z_me_vk += ausgabe.z_me_vk; 
					ausgabeges_m.mo_z_me_vk += ausgabe.z_me_vk; 

					ausgabe.mo_z_ums_vk += ausgabe.z_ums_vk; 
					ausgabegr_m.mo_z_ums_vk += ausgabe.z_ums_vk; 
					ausgabeges_m.mo_z_ums_vk += ausgabe.z_ums_vk; 

					ausgabe.mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
					ausgabegr_m.mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
					ausgabeges_m.mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
				}
				ausgabe.ges_z_me_vk += ausgabe.z_me_vk; //Summe 
				ausgabegr_m.ges_z_me_vk += ausgabe.z_me_vk; //Summe 
				ausgabeges_m.ges_z_me_vk += ausgabe.z_me_vk; //Summe 

				ausgabe.ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 
				ausgabegr_m.ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 
				ausgabeges_m.ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 

				ausgabe.ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
				ausgabegr_m.ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
				ausgabeges_m.ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
			}
			else
			{
				if (ausgabe.monat == monat_bis) 
				{
					ausgabe.vj_mo_z_me_vk += ausgabe.z_me_vk; 
					ausgabegr_m.vj_mo_z_me_vk += ausgabe.z_me_vk; 
					ausgabeges_m.vj_mo_z_me_vk += ausgabe.z_me_vk; 

					ausgabe.vj_mo_z_ums_vk += ausgabe.z_ums_vk; 
					ausgabegr_m.vj_mo_z_ums_vk += ausgabe.z_ums_vk; 
					ausgabeges_m.vj_mo_z_ums_vk += ausgabe.z_ums_vk; 

					ausgabe.vj_mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
					ausgabegr_m.vj_mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
					ausgabeges_m.vj_mo_z_ums_fil_ek += ausgabe.z_ums_fil_ek; 
				}
				ausgabe.vj_ges_z_me_vk += ausgabe.z_me_vk; //Summe 
				ausgabegr_m.vj_ges_z_me_vk += ausgabe.z_me_vk; //Summe 
				ausgabeges_m.vj_ges_z_me_vk += ausgabe.z_me_vk; //Summe 

				ausgabe.vj_ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 
				ausgabegr_m.vj_ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 
				ausgabeges_m.vj_ges_z_ums_vk += ausgabe.z_ums_vk; //Summe 

				ausgabe.vj_ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
				ausgabegr_m.vj_ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
				ausgabeges_m.vj_ges_z_ums_fil_ek += ausgabe.z_ums_fil_ek; //Summe 
		}
		sqlstatus = dbClass.sqlfetch (read_z_ums_we); 
	}


		sqlstatus = dbClass.sqlopen (fil_kost_cursor);
		flg_kosten_holen = TRUE;

  }

	if (fehlercode == 100)
	{
      if (strcmp (ausgabegr_m.fil_kla, "-")   != 0)//vorher war Gruppenwechsel, daher muss das sqlfetch jetzt �bersprungen werden 
	  {
				//letzter Gruppenwechsel
		    memcpy (&ausgabe_m, &ausgabegr_m, sizeof (struct AUSGABE));
			memcpy (&ausgabe_b, &ausgabegr_b, sizeof (struct AUSGABE));
			memcpy (&ausgabe_vj_m, &ausgabegr_vj_m, sizeof (struct AUSGABE));
			memcpy (&ausgabe_vj_b, &ausgabegr_vj_b, sizeof (struct AUSGABE));
			ausgabe.mo_einn = ausgabegr_m.mo_einn;
			ausgabe.ges_einn = ausgabegr_m.ges_einn;
			ausgabe.vj_mo_einn = ausgabegr_m.vj_mo_einn;
			ausgabe.vj_ges_einn = ausgabegr_m.vj_ges_einn;

			//HAUS-6
			ausgabe.mo_einn_bto = ausgabegr_m.mo_einn_bto;
			ausgabe.ges_einn_bto = ausgabegr_m.ges_einn_bto;
			ausgabe.vj_mo_einn_bto = ausgabegr_m.vj_mo_einn_bto;
			ausgabe.vj_ges_einn_bto = ausgabegr_m.vj_ges_einn_bto;

			ausgabe.mo_kun_anz_int = ausgabegr_m.mo_kun_anz_int;
			ausgabe.ges_kun_anz_int = ausgabegr_m.ges_kun_anz_int;
			ausgabe.vj_mo_kun_anz_int = ausgabegr_m.vj_mo_kun_anz_int;
			ausgabe.vj_ges_kun_anz_int = ausgabegr_m.vj_ges_kun_anz_int;
				
			ausgabe.mo_stunden = ausgabegr_m.mo_stunden;
			ausgabe.ges_stunden = ausgabegr_m.ges_stunden;
			ausgabe.vj_mo_stunden = ausgabegr_m.vj_mo_stunden;
			ausgabe.vj_ges_stunden = ausgabegr_m.vj_ges_stunden;

			ausgabe.mo_z_me_vk = ausgabegr_m.mo_z_me_vk;
			ausgabe.ges_z_me_vk = ausgabegr_m.ges_z_me_vk;
			ausgabe.vj_mo_z_me_vk = ausgabegr_m.vj_mo_z_me_vk;
			ausgabe.vj_ges_z_me_vk = ausgabegr_m.vj_ges_z_me_vk;

			ausgabe.mo_z_ums_vk = ausgabegr_m.mo_z_ums_vk;
			ausgabe.ges_z_ums_vk = ausgabegr_m.ges_z_ums_vk;
			ausgabe.vj_mo_z_ums_vk = ausgabegr_m.vj_mo_z_ums_vk;
			ausgabe.vj_ges_z_ums_vk = ausgabegr_m.vj_ges_z_ums_vk;

			ausgabe.mo_z_ums_fil_ek = ausgabegr_m.mo_z_ums_fil_ek;
			ausgabe.ges_z_ums_fil_ek = ausgabegr_m.ges_z_ums_fil_ek;
			ausgabe.vj_mo_z_ums_fil_ek = ausgabegr_m.vj_mo_z_ums_fil_ek;
			ausgabe.vj_ges_z_ums_fil_ek = ausgabegr_m.vj_ges_z_ums_fil_ek;


			memcpy (&ausgabegr_m, &ausgabe_null, sizeof (struct AUSGABE));
			memcpy (&ausgabegr_b, &ausgabe_null, sizeof (struct AUSGABE));
			memcpy (&ausgabegr_vj_m, &ausgabe_null, sizeof (struct AUSGABE));
			memcpy (&ausgabegr_vj_b, &ausgabe_null, sizeof (struct AUSGABE));
			ausgabe.fil = 32000;
			strcpy(ausgabe.fil_kla,ausgabe_m.fil_kla);
			strcpy(ausgabegr_m.fil_kla,"-"); 

			sqlstatus = dbClass.sqlopen (fil_kost_cursor_gr);
			flg_kosten_gr_holen = TRUE;
			return 0;
	  }
	  else if (strcmp (ausgabeges_m.fil_kla, "-")   != 0) //Gesamtmenge muss noch ausgegeben werden
	  {
		    memcpy (&ausgabe_m, &ausgabeges_m, sizeof (struct AUSGABE));
			memcpy (&ausgabe_b, &ausgabeges_b, sizeof (struct AUSGABE));
			memcpy (&ausgabe_vj_m, &ausgabeges_vj_m, sizeof (struct AUSGABE));
			memcpy (&ausgabe_vj_b, &ausgabeges_vj_b, sizeof (struct AUSGABE));
			ausgabe.mo_einn = ausgabeges_m.mo_einn;
			ausgabe.ges_einn = ausgabeges_m.ges_einn;
			ausgabe.vj_mo_einn = ausgabeges_m.vj_mo_einn;
			ausgabe.vj_ges_einn = ausgabeges_m.vj_ges_einn;


			//HAUS-6
			ausgabe.mo_einn_bto = ausgabeges_m.mo_einn_bto;
			ausgabe.ges_einn_bto = ausgabeges_m.ges_einn_bto;
			ausgabe.vj_mo_einn_bto = ausgabeges_m.vj_mo_einn_bto;
			ausgabe.vj_ges_einn_bto = ausgabeges_m.vj_ges_einn_bto;

			ausgabe.mo_kun_anz_int = ausgabeges_m.mo_kun_anz_int;
			ausgabe.ges_kun_anz_int = ausgabeges_m.ges_kun_anz_int;
			ausgabe.vj_mo_kun_anz_int = ausgabeges_m.vj_mo_kun_anz_int;
			ausgabe.vj_ges_kun_anz_int = ausgabeges_m.vj_ges_kun_anz_int;
				
			ausgabe.mo_stunden = ausgabeges_m.mo_stunden;
			ausgabe.ges_stunden = ausgabeges_m.ges_stunden;
			ausgabe.vj_mo_stunden = ausgabeges_m.vj_mo_stunden;
			ausgabe.vj_ges_stunden = ausgabeges_m.vj_ges_stunden;

			ausgabe.mo_z_me_vk = ausgabeges_m.mo_z_me_vk;
			ausgabe.ges_z_me_vk = ausgabeges_m.ges_z_me_vk;
			ausgabe.vj_mo_z_me_vk = ausgabeges_m.vj_mo_z_me_vk;
			ausgabe.vj_ges_z_me_vk = ausgabeges_m.vj_ges_z_me_vk;

			ausgabe.mo_z_ums_vk = ausgabeges_m.mo_z_ums_vk;
			ausgabe.ges_z_ums_vk = ausgabeges_m.ges_z_ums_vk;
			ausgabe.vj_mo_z_ums_vk = ausgabeges_m.vj_mo_z_ums_vk;
			ausgabe.vj_ges_z_ums_vk = ausgabeges_m.vj_ges_z_ums_vk;

			ausgabe.mo_z_ums_fil_ek = ausgabeges_m.mo_z_ums_fil_ek;
			ausgabe.ges_z_ums_fil_ek = ausgabeges_m.ges_z_ums_fil_ek;
			ausgabe.vj_mo_z_ums_fil_ek = ausgabeges_m.vj_mo_z_ums_fil_ek;
			ausgabe.vj_ges_z_ums_fil_ek = ausgabeges_m.vj_ges_z_ums_fil_ek;

			memcpy (&ausgabeges_m, &ausgabe_null, sizeof (struct AUSGABE));
			memcpy (&ausgabeges_b, &ausgabe_null, sizeof (struct AUSGABE));
			memcpy (&ausgabeges_vj_m, &ausgabe_null, sizeof (struct AUSGABE));
			memcpy (&ausgabeges_vj_b, &ausgabe_null, sizeof (struct AUSGABE));
			ausgabe.fil = 32001;
			strcpy(ausgabe.fil_kla,"Z");
			strcpy(ausgabeges_m.fil_kla,"-"); 
			sqlstatus = dbClass.sqlopen (fil_kost_cursor_ges);
			flg_kosten_ges_holen = TRUE;
			return 0;
	  }
	}


	if (ausgabe.fil == 0) 
	{
		ausgabe.fil = -1; //200110 
		flg_kosten_holen = FALSE;

	}
      return fehlercode;
}

void A_MO_WA_CLASS::fuelle_ausgabe (void)
{
			 ausgabe.kun = a_mo_wa.kun;
 			 if (kun_fil == 1)
			 {
				 ausgabe.kun = 0;
				 a_mo_wa.kun = 0;
				 ausgabe.fil = a_mo_wa.fil;
				 sprintf (ausgabe.fil_kla,"%s",a_mo_wa.fil_kla);
			 }
			 else
			 {
				 ausgabe.kun = a_mo_wa.kun;
				 sprintf (ausgabe.fil_kla,"%ld",ausgabe.vertr);
				 ausgabe.fil = 0;
			 }

// Wareneingangswerte mit dazunehmen (aus a_per_we) Direktlieferungen
			 ausgabe.menge += ausgabe_we.menge;
			 ausgabe.menge_sa += ausgabe_we.menge_sa;
			 ausgabe.umsatz_sk += ausgabe_we.umsatz_sk;
			 ausgabe.umsatz_sk_sa += ausgabe_we.umsatz_sk_sa;
			 ausgabe.umsatz += ausgabe_we.umsatz;
			 ausgabe.umsatz_sa += ausgabe_we.umsatz_sa;
			 ausgabe.retouren_me += ausgabe_we.retouren_me;
			 ausgabe.retouren += ausgabe_we.retouren;

			 ausgabe.me_gr1 += ausgabe_we.me_gr1;
			 ausgabe.me_gr2 += ausgabe_we.me_gr2;
			 ausgabe.me_gr3 += ausgabe_we.me_gr3;
			 ausgabe.me_gr4 += ausgabe_we.me_gr4;
			 ausgabe.me_gr5 += ausgabe_we.me_gr5;
			 ausgabe.me_gr6 += ausgabe_we.me_gr6;

			 ausgabe.ums_sk_gr1 += ausgabe_we.ums_sk_gr1;
			 ausgabe.ums_sk_gr2 += ausgabe_we.ums_sk_gr2;
			 ausgabe.ums_sk_gr3 += ausgabe_we.ums_sk_gr3;
			 ausgabe.ums_sk_gr4 += ausgabe_we.ums_sk_gr4;
			 ausgabe.ums_sk_gr5 += ausgabe_we.ums_sk_gr5;
			 ausgabe.ums_sk_gr6 += ausgabe_we.ums_sk_gr6;

			 ausgabe.ums_sk_sa_gr1 += ausgabe_we.ums_sk_sa_gr1;
			 ausgabe.ums_sk_sa_gr2 += ausgabe_we.ums_sk_sa_gr2;
			 ausgabe.ums_sk_sa_gr3 += ausgabe_we.ums_sk_sa_gr3;
			 ausgabe.ums_sk_sa_gr4 += ausgabe_we.ums_sk_sa_gr4;
			 ausgabe.ums_sk_sa_gr5 += ausgabe_we.ums_sk_sa_gr5;
			 ausgabe.ums_sk_sa_gr6 += ausgabe_we.ums_sk_sa_gr6;

			 ausgabe.ums_gr1 += ausgabe_we.ums_gr1;
			 ausgabe.ums_gr2 += ausgabe_we.ums_gr2;
			 ausgabe.ums_gr3 += ausgabe_we.ums_gr3;
			 ausgabe.ums_gr4 += ausgabe_we.ums_gr4;
			 ausgabe.ums_gr5 += ausgabe_we.ums_gr5;
			 ausgabe.ums_gr6 += ausgabe_we.ums_gr6;

			 ausgabe.ums_sa_gr1 += ausgabe_we.ums_sa_gr1;
			 ausgabe.ums_sa_gr2 += ausgabe_we.ums_sa_gr2;
			 ausgabe.ums_sa_gr3 += ausgabe_we.ums_sa_gr3;
			 ausgabe.ums_sa_gr4 += ausgabe_we.ums_sa_gr4;
			 ausgabe.ums_sa_gr5 += ausgabe_we.ums_sa_gr5;
			 ausgabe.ums_sa_gr6 += ausgabe_we.ums_sa_gr6;

			 ausgabe.ret_me_gr1 += ausgabe_we.ret_me_gr1;
			 ausgabe.ret_me_gr2 += ausgabe_we.ret_me_gr2;
			 ausgabe.ret_me_gr3 += ausgabe_we.ret_me_gr3;
			 ausgabe.ret_me_gr4 += ausgabe_we.ret_me_gr4;
			 ausgabe.ret_me_gr5 += ausgabe_we.ret_me_gr5;
			 ausgabe.ret_me_gr6 += ausgabe_we.ret_me_gr6;

			 ausgabe.ret_me_sa_gr1 += ausgabe_we.ret_me_sa_gr1;
			 ausgabe.ret_me_sa_gr2 += ausgabe_we.ret_me_sa_gr2;
			 ausgabe.ret_me_sa_gr3 += ausgabe_we.ret_me_sa_gr3;
			 ausgabe.ret_me_sa_gr4 += ausgabe_we.ret_me_sa_gr4;
			 ausgabe.ret_me_sa_gr5 += ausgabe_we.ret_me_sa_gr5;
			 ausgabe.ret_me_sa_gr6 += ausgabe_we.ret_me_sa_gr6;

			 ausgabe.ret_gr1 += ausgabe_we.ret_gr1;
			 ausgabe.ret_gr2 += ausgabe_we.ret_gr2;
			 ausgabe.ret_gr3 += ausgabe_we.ret_gr3;
			 ausgabe.ret_gr4 += ausgabe_we.ret_gr4;
			 ausgabe.ret_gr5 += ausgabe_we.ret_gr5;
			 ausgabe.ret_gr6 += ausgabe_we.ret_gr6;

			 ausgabe.ret_sa_gr1 += ausgabe_we.ret_sa_gr1;
			 ausgabe.ret_sa_gr2 += ausgabe_we.ret_sa_gr2;
			 ausgabe.ret_sa_gr3 += ausgabe_we.ret_sa_gr3;
			 ausgabe.ret_sa_gr4 += ausgabe_we.ret_sa_gr4;
			 ausgabe.ret_sa_gr5 += ausgabe_we.ret_sa_gr5;
			 ausgabe.ret_sa_gr6 += ausgabe_we.ret_sa_gr6;

			if (ausgabe.menge != 0.0) ausgabe.pr_sk = ausgabe.umsatz_sk / ausgabe.menge; else ausgabe.pr_sk = 0.0;
			if (ausgabe.me_gr1 != 0.0) ausgabe.pr_sk_gr1 = ausgabe.ums_sk_gr1 / ausgabe.me_gr1; else ausgabe.pr_sk_gr1 = 0.0;
			if (ausgabe.me_gr2 != 0.0) ausgabe.pr_sk_gr2 = ausgabe.ums_sk_gr2 / ausgabe.me_gr2; else ausgabe.pr_sk_gr2 = 0.0;
			if (ausgabe.me_gr3 != 0.0) ausgabe.pr_sk_gr3 = ausgabe.ums_sk_gr3 / ausgabe.me_gr3; else ausgabe.pr_sk_gr3 = 0.0;
			if (ausgabe.me_gr4 != 0.0) ausgabe.pr_sk_gr4 = ausgabe.ums_sk_gr4 / ausgabe.me_gr4; else ausgabe.pr_sk_gr4 = 0.0;
			if (ausgabe.me_gr5 != 0.0) ausgabe.pr_sk_gr5 = ausgabe.ums_sk_gr5 / ausgabe.me_gr5; else ausgabe.pr_sk_gr5 = 0.0;
			if (ausgabe.me_gr6 != 0.0) ausgabe.pr_sk_gr6 = ausgabe.ums_sk_gr6 / ausgabe.me_gr6; else ausgabe.pr_sk_gr6 = 0.0;
			 
 //=== Gruppe 1=========
			if (strcmp(checkgr1n,"J") == 0 && strcmp(checkgr1s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
			}
			else if (strcmp(checkgr1n,"J") == 0 && strcmp(checkgr1s,"N") == 0) 
			{
				ausgabe.me_gr1 -= ausgabe.me_sa_gr1;
				ausgabe.ums_gr1 -= ausgabe.ums_sa_gr1;
				ausgabe.ret_me_gr1 -= ausgabe.ret_me_sa_gr1;
				ausgabe.ret_gr1 -= ausgabe.ret_sa_gr1;
				ausgabe.ums_sk_gr1 -= ausgabe.ums_sk_sa_gr1;

			}
			else if (strcmp(checkgr1n,"N") == 0 && strcmp(checkgr1s,"J") == 0) 
			{
				ausgabe.me_gr1 = ausgabe.me_sa_gr1;
				ausgabe.ums_gr1 = ausgabe.ums_sa_gr1;
				ausgabe.ret_me_gr1 = ausgabe.ret_me_sa_gr1;
				ausgabe.ret_gr1 = ausgabe.ret_sa_gr1;
				ausgabe.ums_sk_gr1 = ausgabe.ums_sk_sa_gr1;

			}

//=== Gruppe 2=========
			if (strcmp(checkgr2n,"J") == 0 && strcmp(checkgr2s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
			}
			else if (strcmp(checkgr2n,"J") == 0 && strcmp(checkgr2s,"N") == 0) 
			{
				ausgabe.me_gr2 -= ausgabe.me_sa_gr2;
				ausgabe.ums_gr2 -= ausgabe.ums_sa_gr2;
				ausgabe.ret_me_gr2 -= ausgabe.ret_me_sa_gr2;
				ausgabe.ret_gr2 -= ausgabe.ret_sa_gr2;
				ausgabe.ums_sk_gr2 -= ausgabe.ums_sk_sa_gr2;

			}
			else if (strcmp(checkgr2n,"N") == 0 && strcmp(checkgr2s,"J") == 0) 
			{
				ausgabe.me_gr2 = ausgabe.me_sa_gr2;
				ausgabe.ums_gr2 = ausgabe.ums_sa_gr2;
				ausgabe.ret_me_gr2 = ausgabe.ret_me_sa_gr2;
				ausgabe.ret_gr2 = ausgabe.ret_sa_gr2;
				ausgabe.ums_sk_gr2 = ausgabe.ums_sk_sa_gr2;

			}
//=== Gruppe 3=========
			if (strcmp(checkgr3n,"J") == 0 && strcmp(checkgr3s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
			}
			else if (strcmp(checkgr3n,"J") == 0 && strcmp(checkgr3s,"N") == 0) 
			{
				ausgabe.me_gr3 -= ausgabe.me_sa_gr3;
				ausgabe.ums_gr3 -= ausgabe.ums_sa_gr3;
				ausgabe.ret_me_gr3 -= ausgabe.ret_me_sa_gr3;
				ausgabe.ret_gr3 -= ausgabe.ret_sa_gr3;
				ausgabe.ums_sk_gr3 -= ausgabe.ums_sk_sa_gr3;

			}
			else if (strcmp(checkgr3n,"N") == 0 && strcmp(checkgr3s,"J") == 0) 
			{
				ausgabe.me_gr3 = ausgabe.me_sa_gr3;
				ausgabe.ums_gr3 = ausgabe.ums_sa_gr3;
				ausgabe.ret_me_gr3 = ausgabe.ret_me_sa_gr3;
				ausgabe.ret_gr3 = ausgabe.ret_sa_gr3;
				ausgabe.ums_sk_gr3 = ausgabe.ums_sk_sa_gr3;

			}
//=== Gruppe 4=========
			if (strcmp(checkgr4n,"J") == 0 && strcmp(checkgr4s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
			}
			else if (strcmp(checkgr4n,"J") == 0 && strcmp(checkgr4s,"N") == 0) 
			{
				ausgabe.me_gr4 -= ausgabe.me_sa_gr4;
				ausgabe.ums_gr4 -= ausgabe.ums_sa_gr4;
				ausgabe.ret_me_gr4 -= ausgabe.ret_me_sa_gr4;
				ausgabe.ret_gr4 -= ausgabe.ret_sa_gr4;
				ausgabe.ums_sk_gr4 -= ausgabe.ums_sk_sa_gr4;

			}
			else if (strcmp(checkgr4n,"N") == 0 && strcmp(checkgr4s,"J") == 0) 
			{
				ausgabe.me_gr4 = ausgabe.me_sa_gr4;
				ausgabe.ums_gr4 = ausgabe.ums_sa_gr4;
				ausgabe.ret_me_gr4 = ausgabe.ret_me_sa_gr4;
				ausgabe.ret_gr4 = ausgabe.ret_sa_gr4;
				ausgabe.ums_sk_gr4 = ausgabe.ums_sk_sa_gr4;

			}
//=== Gruppe 5=========
			if (strcmp(checkgr5n,"J") == 0 && strcmp(checkgr5s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
			}
			else if (strcmp(checkgr5n,"J") == 0 && strcmp(checkgr5s,"N") == 0) 
			{
				ausgabe.me_gr5 -= ausgabe.me_sa_gr5;
				ausgabe.ums_gr5 -= ausgabe.ums_sa_gr5;
				ausgabe.ret_me_gr5 -= ausgabe.ret_me_sa_gr5;
				ausgabe.ret_gr5 -= ausgabe.ret_sa_gr5;
				ausgabe.ums_sk_gr5 -= ausgabe.ums_sk_sa_gr5;

			}
			else if (strcmp(checkgr5n,"N") == 0 && strcmp(checkgr5s,"J") == 0) 
			{
				ausgabe.me_gr5 = ausgabe.me_sa_gr5;
				ausgabe.ums_gr5 = ausgabe.ums_sa_gr5;
				ausgabe.ret_me_gr5 = ausgabe.ret_me_sa_gr5;
				ausgabe.ret_gr5 = ausgabe.ret_sa_gr5;
				ausgabe.ums_sk_gr5 = ausgabe.ums_sk_sa_gr5;

			}
//=== Gruppe 6=========
			if (strcmp(checkgr6n,"J") == 0 && strcmp(checkgr6s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
			}
			else if (strcmp(checkgr6n,"J") == 0 && strcmp(checkgr6s,"N") == 0) 
			{
				ausgabe.me_gr6 -= ausgabe.me_sa_gr6;
				ausgabe.ums_gr6 -= ausgabe.ums_sa_gr6;
				ausgabe.ret_me_gr6 -= ausgabe.ret_me_sa_gr6;
				ausgabe.ret_gr6 -= ausgabe.ret_sa_gr6;
				ausgabe.ums_sk_gr6 -= ausgabe.ums_sk_sa_gr6;

			}
			else if (strcmp(checkgr6n,"N") == 0 && strcmp(checkgr6s,"J") == 0) 
			{
				ausgabe.me_gr6 = ausgabe.me_sa_gr6;
				ausgabe.ums_gr6 = ausgabe.ums_sa_gr6;
				ausgabe.ret_me_gr6 = ausgabe.ret_me_sa_gr6;
				ausgabe.ret_gr6 = ausgabe.ret_sa_gr6;
				ausgabe.ums_sk_gr6 -= ausgabe.ums_sk_sa_gr6;

			}


//			ausgabe.ums_sk_gr1 = ausgabe.pr_sk_gr1 * ausgabe.me_gr1;
//			ausgabe.ums_sk_gr2 = ausgabe.pr_sk_gr2 * ausgabe.me_gr2;
//			ausgabe.ums_sk_gr3 = ausgabe.pr_sk_gr3 * ausgabe.me_gr3;
//			ausgabe.ums_sk_gr4 = ausgabe.pr_sk_gr4 * ausgabe.me_gr4;
//			ausgabe.ums_sk_gr5 = ausgabe.pr_sk_gr5 * ausgabe.me_gr5;
//			ausgabe.ums_sk_gr6 = ausgabe.pr_sk_gr6 * ausgabe.me_gr6;




			ausgabe.ertrag = ausgabe.umsatz - ausgabe.umsatz_sk;
			ausgabe.ertrag_gr1 = ausgabe.ums_gr1 - ausgabe.ums_sk_gr1;
			ausgabe.ertrag_gr2 = ausgabe.ums_gr2 - ausgabe.ums_sk_gr2;
			ausgabe.ertrag_gr3 = ausgabe.ums_gr3 - ausgabe.ums_sk_gr3;
			ausgabe.ertrag_gr4 = ausgabe.ums_gr4 - ausgabe.ums_sk_gr4;
			ausgabe.ertrag_gr5 = ausgabe.ums_gr5 - ausgabe.ums_sk_gr5;
			ausgabe.ertrag_gr6 = ausgabe.ums_gr6 - ausgabe.ums_sk_gr6;



}

void A_MO_WA_CLASS::fuelle_summen_m (void)
{
//Gruppensummen 
         if (a_mo_wa.jr == jr)
		 {
			ausgabegr_m.me_gr1 += ausgabe.me_gr1;
			ausgabegr_m.ums_gr1 += ausgabe.ums_gr1;
			ausgabegr_m.ret_me_gr1 += ausgabe.ret_me_gr1;
			ausgabegr_m.ret_gr1 += ausgabe.ret_gr1;
			ausgabegr_m.ums_sk_gr1 += ausgabe.ums_sk_gr1;

			ausgabegr_m.me_gr2 += ausgabe.me_gr2;
			ausgabegr_m.ums_gr2 += ausgabe.ums_gr2;
			ausgabegr_m.ret_me_gr2 += ausgabe.ret_me_gr2;
			ausgabegr_m.ret_gr2 += ausgabe.ret_gr2;
			ausgabegr_m.ums_sk_gr2 += ausgabe.ums_sk_gr2;

			ausgabegr_m.me_gr3 += ausgabe.me_gr3;
			ausgabegr_m.ums_gr3 += ausgabe.ums_gr3;
			ausgabegr_m.ret_me_gr3 += ausgabe.ret_me_gr3;
			ausgabegr_m.ret_gr3 += ausgabe.ret_gr3;
			ausgabegr_m.ums_sk_gr3 += ausgabe.ums_sk_gr3;

			ausgabegr_m.me_gr4 += ausgabe.me_gr4;
			ausgabegr_m.ums_gr4 += ausgabe.ums_gr4;
			ausgabegr_m.ret_me_gr4 += ausgabe.ret_me_gr4;
			ausgabegr_m.ret_gr4 += ausgabe.ret_gr4;
			ausgabegr_m.ums_sk_gr4 += ausgabe.ums_sk_gr4;

			ausgabegr_m.me_gr5 += ausgabe.me_gr5;
			ausgabegr_m.ums_gr5 += ausgabe.ums_gr5;
			ausgabegr_m.ret_me_gr5 += ausgabe.ret_me_gr5;
			ausgabegr_m.ret_gr5 += ausgabe.ret_gr5;
			ausgabegr_m.ums_sk_gr5 += ausgabe.ums_sk_gr5;

			ausgabegr_m.me_gr6 += ausgabe.me_gr6;
			ausgabegr_m.ums_gr6 += ausgabe.ums_gr6;
			ausgabegr_m.ret_me_gr6 += ausgabe.ret_me_gr6;
			ausgabegr_m.ret_gr6 += ausgabe.ret_gr6;
			ausgabegr_m.ums_sk_gr6 += ausgabe.ums_sk_gr6;


			ausgabegr_m.ertrag += ausgabe.ertrag;
			ausgabegr_m.ertrag_gr1 += ausgabe.ertrag_gr1;
			ausgabegr_m.ertrag_gr2 += ausgabe.ertrag_gr2;
			ausgabegr_m.ertrag_gr3 += ausgabe.ertrag_gr3;
			ausgabegr_m.ertrag_gr4 += ausgabe.ertrag_gr4;
			ausgabegr_m.ertrag_gr5 += ausgabe.ertrag_gr5;
			ausgabegr_m.ertrag_gr6 += ausgabe.ertrag_gr6;

//Gesamtsummen 
			ausgabeges_m.me_gr1 += ausgabe.me_gr1;
			ausgabeges_m.ums_gr1 += ausgabe.ums_gr1;
			ausgabeges_m.ret_me_gr1 += ausgabe.ret_me_gr1;
			ausgabeges_m.ret_gr1 += ausgabe.ret_gr1;

			ausgabeges_m.me_gr2 += ausgabe.me_gr2;
			ausgabeges_m.ums_gr2 += ausgabe.ums_gr2;
			ausgabeges_m.ret_me_gr2 += ausgabe.ret_me_gr2;
			ausgabeges_m.ret_gr2 += ausgabe.ret_gr2;

			ausgabeges_m.me_gr3 += ausgabe.me_gr3;
			ausgabeges_m.ums_gr3 += ausgabe.ums_gr3;
			ausgabeges_m.ret_me_gr3 += ausgabe.ret_me_gr3;
			ausgabeges_m.ret_gr3 += ausgabe.ret_gr3;

			ausgabeges_m.me_gr4 += ausgabe.me_gr4;
			ausgabeges_m.ums_gr4 += ausgabe.ums_gr4;
			ausgabeges_m.ret_me_gr4 += ausgabe.ret_me_gr4;
			ausgabeges_m.ret_gr4 += ausgabe.ret_gr4;

			ausgabeges_m.me_gr5 += ausgabe.me_gr5;
			ausgabeges_m.ums_gr5 += ausgabe.ums_gr5;
			ausgabeges_m.ret_me_gr5 += ausgabe.ret_me_gr5;
			ausgabeges_m.ret_gr5 += ausgabe.ret_gr5;

			ausgabeges_m.me_gr6 += ausgabe.me_gr6;
			ausgabeges_m.ums_gr6 += ausgabe.ums_gr6;
			ausgabeges_m.ret_me_gr6 += ausgabe.ret_me_gr6;
			ausgabeges_m.ret_gr6 += ausgabe.ret_gr6;

			ausgabeges_m.ums_sk_gr1 += ausgabe.ums_sk_gr1;
			ausgabeges_m.ums_sk_gr2 += ausgabe.ums_sk_gr2;
			ausgabeges_m.ums_sk_gr3 += ausgabe.ums_sk_gr3;
			ausgabeges_m.ums_sk_gr4 += ausgabe.ums_sk_gr4;
			ausgabeges_m.ums_sk_gr5 += ausgabe.ums_sk_gr5;
			ausgabeges_m.ums_sk_gr6 += ausgabe.ums_sk_gr6;

			ausgabeges_m.ertrag += ausgabe.ertrag;
			ausgabeges_m.ertrag_gr1 += ausgabe.ertrag_gr1;
			ausgabeges_m.ertrag_gr2 += ausgabe.ertrag_gr2;
			ausgabeges_m.ertrag_gr3 += ausgabe.ertrag_gr3;
			ausgabeges_m.ertrag_gr4 += ausgabe.ertrag_gr4;
			ausgabeges_m.ertrag_gr5 += ausgabe.ertrag_gr5;
			ausgabeges_m.ertrag_gr6 += ausgabe.ertrag_gr6;
		 }
//Gruppensummen Vorjahr
         if (a_mo_wa.jr == vjr)
		 {
			ausgabegr_vj_m.me_gr1 += ausgabe.me_gr1;
			ausgabegr_vj_m.ums_gr1 += ausgabe.ums_gr1;
			ausgabegr_vj_m.ret_me_gr1 += ausgabe.ret_me_gr1;
			ausgabegr_vj_m.ret_gr1 += ausgabe.ret_gr1;

			ausgabegr_vj_m.me_gr2 += ausgabe.me_gr2;
			ausgabegr_vj_m.ums_gr2 += ausgabe.ums_gr2;
			ausgabegr_vj_m.ret_me_gr2 += ausgabe.ret_me_gr2;
			ausgabegr_vj_m.ret_gr2 += ausgabe.ret_gr2;

			ausgabegr_vj_m.me_gr3 += ausgabe.me_gr3;
			ausgabegr_vj_m.ums_gr3 += ausgabe.ums_gr3;
			ausgabegr_vj_m.ret_me_gr3 += ausgabe.ret_me_gr3;
			ausgabegr_vj_m.ret_gr3 += ausgabe.ret_gr3;

			ausgabegr_vj_m.me_gr4 += ausgabe.me_gr4;
			ausgabegr_vj_m.ums_gr4 += ausgabe.ums_gr4;
			ausgabegr_vj_m.ret_me_gr4 += ausgabe.ret_me_gr4;
			ausgabegr_vj_m.ret_gr4 += ausgabe.ret_gr4;

			ausgabegr_vj_m.me_gr5 += ausgabe.me_gr5;
			ausgabegr_vj_m.ums_gr5 += ausgabe.ums_gr5;
			ausgabegr_vj_m.ret_me_gr5 += ausgabe.ret_me_gr5;
			ausgabegr_vj_m.ret_gr5 += ausgabe.ret_gr5;

			ausgabegr_vj_m.me_gr6 += ausgabe.me_gr6;
			ausgabegr_vj_m.ums_gr6 += ausgabe.ums_gr6;
			ausgabegr_vj_m.ret_me_gr6 += ausgabe.ret_me_gr6;
			ausgabegr_vj_m.ret_gr6 += ausgabe.ret_gr6;

			ausgabegr_vj_m.ums_sk_gr1 += ausgabe.ums_sk_gr1;
			ausgabegr_vj_m.ums_sk_gr2 += ausgabe.ums_sk_gr2;
			ausgabegr_vj_m.ums_sk_gr3 += ausgabe.ums_sk_gr3;
			ausgabegr_vj_m.ums_sk_gr4 += ausgabe.ums_sk_gr4;
			ausgabegr_vj_m.ums_sk_gr5 += ausgabe.ums_sk_gr5;
			ausgabegr_vj_m.ums_sk_gr6 += ausgabe.ums_sk_gr6;

			ausgabegr_vj_m.ertrag += ausgabe.ertrag;
			ausgabegr_vj_m.ertrag_gr1 += ausgabe.ertrag_gr1;
			ausgabegr_vj_m.ertrag_gr2 += ausgabe.ertrag_gr2;
			ausgabegr_vj_m.ertrag_gr3 += ausgabe.ertrag_gr3;
			ausgabegr_vj_m.ertrag_gr4 += ausgabe.ertrag_gr4;
			ausgabegr_vj_m.ertrag_gr5 += ausgabe.ertrag_gr5;
			ausgabegr_vj_m.ertrag_gr6 += ausgabe.ertrag_gr6;

//Gesamtsummen Vorjahr
			ausgabeges_vj_m.me_gr1 += ausgabe.me_gr1;
			ausgabeges_vj_m.ums_gr1 += ausgabe.ums_gr1;
			ausgabeges_vj_m.ret_me_gr1 += ausgabe.ret_me_gr1;
			ausgabeges_vj_m.ret_gr1 += ausgabe.ret_gr1;

			ausgabeges_vj_m.me_gr2 += ausgabe.me_gr2;
			ausgabeges_vj_m.ums_gr2 += ausgabe.ums_gr2;
			ausgabeges_vj_m.ret_me_gr2 += ausgabe.ret_me_gr2;
			ausgabeges_vj_m.ret_gr2 += ausgabe.ret_gr2;

			ausgabeges_vj_m.me_gr3 += ausgabe.me_gr3;
			ausgabeges_vj_m.ums_gr3 += ausgabe.ums_gr3;
			ausgabeges_vj_m.ret_me_gr3 += ausgabe.ret_me_gr3;
			ausgabeges_vj_m.ret_gr3 += ausgabe.ret_gr3;

			ausgabeges_vj_m.me_gr4 += ausgabe.me_gr4;
			ausgabeges_vj_m.ums_gr4 += ausgabe.ums_gr4;
			ausgabeges_vj_m.ret_me_gr4 += ausgabe.ret_me_gr4;
			ausgabeges_vj_m.ret_gr4 += ausgabe.ret_gr4;

			ausgabeges_vj_m.me_gr5 += ausgabe.me_gr5;
			ausgabeges_vj_m.ums_gr5 += ausgabe.ums_gr5;
			ausgabeges_vj_m.ret_me_gr5 += ausgabe.ret_me_gr5;
			ausgabeges_vj_m.ret_gr5 += ausgabe.ret_gr5;

			ausgabeges_vj_m.me_gr6 += ausgabe.me_gr6;
			ausgabeges_vj_m.ums_gr6 += ausgabe.ums_gr6;
			ausgabeges_vj_m.ret_me_gr6 += ausgabe.ret_me_gr6;
			ausgabeges_vj_m.ret_gr6 += ausgabe.ret_gr6;

			ausgabeges_vj_m.ums_sk_gr1 += ausgabe.ums_sk_gr1;
			ausgabeges_vj_m.ums_sk_gr2 += ausgabe.ums_sk_gr2;
			ausgabeges_vj_m.ums_sk_gr3 += ausgabe.ums_sk_gr3;
			ausgabeges_vj_m.ums_sk_gr4 += ausgabe.ums_sk_gr4;
			ausgabeges_vj_m.ums_sk_gr5 += ausgabe.ums_sk_gr5;
			ausgabeges_vj_m.ums_sk_gr6 += ausgabe.ums_sk_gr6;

			ausgabeges_vj_m.ertrag += ausgabe.ertrag;
			ausgabeges_vj_m.ertrag_gr1 += ausgabe.ertrag_gr1;
			ausgabeges_vj_m.ertrag_gr2 += ausgabe.ertrag_gr2;
			ausgabeges_vj_m.ertrag_gr3 += ausgabe.ertrag_gr3;
			ausgabeges_vj_m.ertrag_gr4 += ausgabe.ertrag_gr4;
			ausgabeges_vj_m.ertrag_gr5 += ausgabe.ertrag_gr5;
			ausgabeges_vj_m.ertrag_gr6 += ausgabe.ertrag_gr6;
		 }
}
void A_MO_WA_CLASS::fuelle_summen_b (void)
{
//Gruppensummen 
         if (a_mo_wa.jr == jr)
		 {
			ausgabegr_b.me_gr1 += ausgabe.me_gr1;
			ausgabegr_b.ums_gr1 += ausgabe.ums_gr1;
			ausgabegr_b.ret_me_gr1 += ausgabe.ret_me_gr1;
			ausgabegr_b.ret_gr1 += ausgabe.ret_gr1;

			ausgabegr_b.me_gr2 += ausgabe.me_gr2;
			ausgabegr_b.ums_gr2 += ausgabe.ums_gr2;
			ausgabegr_b.ret_me_gr2 += ausgabe.ret_me_gr2;
			ausgabegr_b.ret_gr2 += ausgabe.ret_gr2;

			ausgabegr_b.me_gr3 += ausgabe.me_gr3;
			ausgabegr_b.ums_gr3 += ausgabe.ums_gr3;
			ausgabegr_b.ret_me_gr3 += ausgabe.ret_me_gr3;
			ausgabegr_b.ret_gr3 += ausgabe.ret_gr3;

			ausgabegr_b.me_gr4 += ausgabe.me_gr4;
			ausgabegr_b.ums_gr4 += ausgabe.ums_gr4;
			ausgabegr_b.ret_me_gr4 += ausgabe.ret_me_gr4;
			ausgabegr_b.ret_gr4 += ausgabe.ret_gr4;

			ausgabegr_b.me_gr5 += ausgabe.me_gr5;
			ausgabegr_b.ums_gr5 += ausgabe.ums_gr5;
			ausgabegr_b.ret_me_gr5 += ausgabe.ret_me_gr5;
			ausgabegr_b.ret_gr5 += ausgabe.ret_gr5;

			ausgabegr_b.me_gr6 += ausgabe.me_gr6;
			ausgabegr_b.ums_gr6 += ausgabe.ums_gr6;
			ausgabegr_b.ret_me_gr6 += ausgabe.ret_me_gr6;
			ausgabegr_b.ret_gr6 += ausgabe.ret_gr6;

			ausgabegr_b.ums_sk_gr1 += ausgabe.ums_sk_gr1;
			ausgabegr_b.ums_sk_gr2 += ausgabe.ums_sk_gr2;
			ausgabegr_b.ums_sk_gr3 += ausgabe.ums_sk_gr3;
			ausgabegr_b.ums_sk_gr4 += ausgabe.ums_sk_gr4;
			ausgabegr_b.ums_sk_gr5 += ausgabe.ums_sk_gr5;
			ausgabegr_b.ums_sk_gr6 += ausgabe.ums_sk_gr6;

			ausgabegr_b.ertrag += ausgabe.ertrag;
			ausgabegr_b.ertrag_gr1 += ausgabe.ertrag_gr1;
			ausgabegr_b.ertrag_gr2 += ausgabe.ertrag_gr2;
			ausgabegr_b.ertrag_gr3 += ausgabe.ertrag_gr3;
			ausgabegr_b.ertrag_gr4 += ausgabe.ertrag_gr4;
			ausgabegr_b.ertrag_gr5 += ausgabe.ertrag_gr5;
			ausgabegr_b.ertrag_gr6 += ausgabe.ertrag_gr6;

//Gesamtsummen 
			ausgabeges_b.me_gr1 += ausgabe.me_gr1;
			ausgabeges_b.ums_gr1 += ausgabe.ums_gr1;
			ausgabeges_b.ret_me_gr1 += ausgabe.ret_me_gr1;
			ausgabeges_b.ret_gr1 += ausgabe.ret_gr1;

			ausgabeges_b.me_gr2 += ausgabe.me_gr2;
			ausgabeges_b.ums_gr2 += ausgabe.ums_gr2;
			ausgabeges_b.ret_me_gr2 += ausgabe.ret_me_gr2;
			ausgabeges_b.ret_gr2 += ausgabe.ret_gr2;

			ausgabeges_b.me_gr3 += ausgabe.me_gr3;
			ausgabeges_b.ums_gr3 += ausgabe.ums_gr3;
			ausgabeges_b.ret_me_gr3 += ausgabe.ret_me_gr3;
			ausgabeges_b.ret_gr3 += ausgabe.ret_gr3;

			ausgabeges_b.me_gr4 += ausgabe.me_gr4;
			ausgabeges_b.ums_gr4 += ausgabe.ums_gr4;
			ausgabeges_b.ret_me_gr4 += ausgabe.ret_me_gr4;
			ausgabeges_b.ret_gr4 += ausgabe.ret_gr4;

			ausgabeges_b.me_gr5 += ausgabe.me_gr5;
			ausgabeges_b.ums_gr5 += ausgabe.ums_gr5;
			ausgabeges_b.ret_me_gr5 += ausgabe.ret_me_gr5;
			ausgabeges_b.ret_gr5 += ausgabe.ret_gr5;

			ausgabeges_b.me_gr6 += ausgabe.me_gr6;
			ausgabeges_b.ums_gr6 += ausgabe.ums_gr6;
			ausgabeges_b.ret_me_gr6 += ausgabe.ret_me_gr6;
			ausgabeges_b.ret_gr6 += ausgabe.ret_gr6;

			ausgabeges_b.ums_sk_gr1 += ausgabe.ums_sk_gr1;
			ausgabeges_b.ums_sk_gr2 += ausgabe.ums_sk_gr2;
			ausgabeges_b.ums_sk_gr3 += ausgabe.ums_sk_gr3;
			ausgabeges_b.ums_sk_gr4 += ausgabe.ums_sk_gr4;
			ausgabeges_b.ums_sk_gr5 += ausgabe.ums_sk_gr5;
			ausgabeges_b.ums_sk_gr6 += ausgabe.ums_sk_gr6;

			ausgabeges_b.ertrag += ausgabe.ertrag;
			ausgabeges_b.ertrag_gr1 += ausgabe.ertrag_gr1;
			ausgabeges_b.ertrag_gr2 += ausgabe.ertrag_gr2;
			ausgabeges_b.ertrag_gr3 += ausgabe.ertrag_gr3;
			ausgabeges_b.ertrag_gr4 += ausgabe.ertrag_gr4;
			ausgabeges_b.ertrag_gr5 += ausgabe.ertrag_gr5;
			ausgabeges_b.ertrag_gr6 += ausgabe.ertrag_gr6;
		 }
//Gruppensummen Vorjahr
         if (a_mo_wa.jr == vjr)
		 {
			ausgabegr_vj_b.me_gr1 += ausgabe.me_gr1;
			ausgabegr_vj_b.ums_gr1 += ausgabe.ums_gr1;
			ausgabegr_vj_b.ret_me_gr1 += ausgabe.ret_me_gr1;
			ausgabegr_vj_b.ret_gr1 += ausgabe.ret_gr1;

			ausgabegr_vj_b.me_gr2 += ausgabe.me_gr2;
			ausgabegr_vj_b.ums_gr2 += ausgabe.ums_gr2;
			ausgabegr_vj_b.ret_me_gr2 += ausgabe.ret_me_gr2;
			ausgabegr_vj_b.ret_gr2 += ausgabe.ret_gr2;

			ausgabegr_vj_b.me_gr3 += ausgabe.me_gr3;
			ausgabegr_vj_b.ums_gr3 += ausgabe.ums_gr3;
			ausgabegr_vj_b.ret_me_gr3 += ausgabe.ret_me_gr3;
			ausgabegr_vj_b.ret_gr3 += ausgabe.ret_gr3;

			ausgabegr_vj_b.me_gr4 += ausgabe.me_gr4;
			ausgabegr_vj_b.ums_gr4 += ausgabe.ums_gr4;
			ausgabegr_vj_b.ret_me_gr4 += ausgabe.ret_me_gr4;
			ausgabegr_vj_b.ret_gr4 += ausgabe.ret_gr4;

			ausgabegr_vj_b.me_gr5 += ausgabe.me_gr5;
			ausgabegr_vj_b.ums_gr5 += ausgabe.ums_gr5;
			ausgabegr_vj_b.ret_me_gr5 += ausgabe.ret_me_gr5;
			ausgabegr_vj_b.ret_gr5 += ausgabe.ret_gr5;

			ausgabegr_vj_b.me_gr6 += ausgabe.me_gr6;
			ausgabegr_vj_b.ums_gr6 += ausgabe.ums_gr6;
			ausgabegr_vj_b.ret_me_gr6 += ausgabe.ret_me_gr6;
			ausgabegr_vj_b.ret_gr6 += ausgabe.ret_gr6;

			ausgabegr_vj_b.ums_sk_gr1 += ausgabe.ums_sk_gr1;
			ausgabegr_vj_b.ums_sk_gr2 += ausgabe.ums_sk_gr2;
			ausgabegr_vj_b.ums_sk_gr3 += ausgabe.ums_sk_gr3;
			ausgabegr_vj_b.ums_sk_gr4 += ausgabe.ums_sk_gr4;
			ausgabegr_vj_b.ums_sk_gr5 += ausgabe.ums_sk_gr5;
			ausgabegr_vj_b.ums_sk_gr6 += ausgabe.ums_sk_gr6;

			ausgabegr_vj_b.ertrag += ausgabe.ertrag;
			ausgabegr_vj_b.ertrag_gr1 += ausgabe.ertrag_gr1;
			ausgabegr_vj_b.ertrag_gr2 += ausgabe.ertrag_gr2;
			ausgabegr_vj_b.ertrag_gr3 += ausgabe.ertrag_gr3;
			ausgabegr_vj_b.ertrag_gr4 += ausgabe.ertrag_gr4;
			ausgabegr_vj_b.ertrag_gr5 += ausgabe.ertrag_gr5;
			ausgabegr_vj_b.ertrag_gr6 += ausgabe.ertrag_gr6;

//Gesamtsummen Vorjahr
			ausgabeges_vj_b.me_gr1 += ausgabe.me_gr1;
			ausgabeges_vj_b.ums_gr1 += ausgabe.ums_gr1;
			ausgabeges_vj_b.ret_me_gr1 += ausgabe.ret_me_gr1;
			ausgabeges_vj_b.ret_gr1 += ausgabe.ret_gr1;

			ausgabeges_vj_b.me_gr2 += ausgabe.me_gr2;
			ausgabeges_vj_b.ums_gr2 += ausgabe.ums_gr2;
			ausgabeges_vj_b.ret_me_gr2 += ausgabe.ret_me_gr2;
			ausgabeges_vj_b.ret_gr2 += ausgabe.ret_gr2;

			ausgabeges_vj_b.me_gr3 += ausgabe.me_gr3;
			ausgabeges_vj_b.ums_gr3 += ausgabe.ums_gr3;
			ausgabeges_vj_b.ret_me_gr3 += ausgabe.ret_me_gr3;
			ausgabeges_vj_b.ret_gr3 += ausgabe.ret_gr3;

			ausgabeges_vj_b.me_gr4 += ausgabe.me_gr4;
			ausgabeges_vj_b.ums_gr4 += ausgabe.ums_gr4;
			ausgabeges_vj_b.ret_me_gr4 += ausgabe.ret_me_gr4;
			ausgabeges_vj_b.ret_gr4 += ausgabe.ret_gr4;

			ausgabeges_vj_b.me_gr5 += ausgabe.me_gr5;
			ausgabeges_vj_b.ums_gr5 += ausgabe.ums_gr5;
			ausgabeges_vj_b.ret_me_gr5 += ausgabe.ret_me_gr5;
			ausgabeges_vj_b.ret_gr5 += ausgabe.ret_gr5;

			ausgabeges_vj_b.me_gr6 += ausgabe.me_gr6;
			ausgabeges_vj_b.ums_gr6 += ausgabe.ums_gr6;
			ausgabeges_vj_b.ret_me_gr6 += ausgabe.ret_me_gr6;
			ausgabeges_vj_b.ret_gr6 += ausgabe.ret_gr6;

			ausgabeges_vj_b.ums_sk_gr1 += ausgabe.ums_sk_gr1;
			ausgabeges_vj_b.ums_sk_gr2 += ausgabe.ums_sk_gr2;
			ausgabeges_vj_b.ums_sk_gr3 += ausgabe.ums_sk_gr3;
			ausgabeges_vj_b.ums_sk_gr4 += ausgabe.ums_sk_gr4;
			ausgabeges_vj_b.ums_sk_gr5 += ausgabe.ums_sk_gr5;
			ausgabeges_vj_b.ums_sk_gr6 += ausgabe.ums_sk_gr6;

			ausgabeges_vj_b.ertrag += ausgabe.ertrag;
			ausgabeges_vj_b.ertrag_gr1 += ausgabe.ertrag_gr1;
			ausgabeges_vj_b.ertrag_gr2 += ausgabe.ertrag_gr2;
			ausgabeges_vj_b.ertrag_gr3 += ausgabe.ertrag_gr3;
			ausgabeges_vj_b.ertrag_gr4 += ausgabe.ertrag_gr4;
			ausgabeges_vj_b.ertrag_gr5 += ausgabe.ertrag_gr5;
			ausgabeges_vj_b.ertrag_gr6 += ausgabe.ertrag_gr6;
		 }
}

int A_MO_WA_CLASS::openDS (void)
{
         dbClass.sqlopen (readcursor);
         return dbClass.sqlopen (readcursor);
}

void A_MO_WA_CLASS::prepare (void)
{
     char *sqltext1;
     char *sqltext2;
    sqltext1 = "select  ";
    sqltext2 = "select  ";

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &kunvon, SQLLONG, 0);
	dbClass.sqlin ((short *) &kunbis, SQLLONG, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);

	dbClass.sqlout ((char *) a_mo_wa.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((long *) &ausgabe.vertr, SQLLONG, 2);
	dbClass.sqlout ((short *) &a_mo_wa.fil, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun

           readcursor = dbClass.sqlcursor ("select "
							"case when a_mo_wa.fil > 0 then fil.fil_kla "
							"	when a_mo_wa.fil = 0 then \"\" end klasse , "
							"case when a_mo_wa.kun > 0 then kun.vertr1 "
							"	when a_mo_wa.kun = 0 then 0 end vertr , "

							"case when a_mo_wa.fil = 0 then a_mo_wa.kun "
							"     when a_mo_wa.fil > 0 then a_mo_wa.fil end kunde, "
							"case when a_mo_wa.fil = 0 then 0 "
							"     when a_mo_wa.fil > 0 then 1 end kun_fil "
							"from a_mo_wa,a_bas,wg, outer fil ,outer kun where "
							"    a_mo_wa.mdn = fil.mdn "
							"and a_mo_wa.fil = fil.fil "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_mo_wa.mdn = ? "
							"and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.kun between ? and ? "
							"and a_mo_wa.jr = ? "
							"and a_mo_wa.mo between ? and ? "
							"group by 1,2,3,4 order by klasse,vertr,kun_fil,kunde "
							);


	test_upd_cursor = 1;


}

void A_MO_WA_CLASS::prepare_per (void)
{
     char *sqltext1;
     char *sqltext2;
    sqltext1 = "select  ";
    sqltext2 = "select  ";

  vjr = jr - 1;

	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.kost_art, SQLSHORT, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_mo, SQLDOUBLE, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_mo_vj, SQLDOUBLE, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_b, SQLDOUBLE, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_b_vj, SQLDOUBLE, 0);   
	dbClass.sqlout ((char *) ausgabe.gr_kz, SQLCHAR, 2);   
	dbClass.sqlout ((char *) ausgabe.kost_art_bz, SQLCHAR, 25);   

	fil_kost_cursor = dbClass.sqlcursor ("select fil_pkost.kost_art, "
		"sum (case when fil_pkost.jr = ? and fil_pkost.period = ? then fil_pkost.kost_period end) , "
		"sum (case when fil_pkost.jr = ? -1 and fil_pkost.period = ? then fil_pkost.kost_period end) , "
		"sum (case when fil_pkost.jr = ? then fil_pkost.kost_period end) , "
		"sum (case when fil_pkost.jr = ? -1 then fil_pkost.kost_period end) , "
		"fil_kostgr.gr_kz, kost_art.kost_art_bz "
						"from fil_pkost,fil_kostgr,kost_art "
						"where fil_pkost.mdn = fil_kostgr.mdn and fil_pkost.kost_art = fil_kostgr.kost_art and kost_art.kost_art = fil_pkost.kost_art "
						" and fil_pkost.mdn = ? and fil_pkost.fil = ? and fil_kostgr.gr_kz in (\"1\",\"2\") and fil_pkost.period <= ? "
						"group by 1,6,7 order by fil_kostgr.gr_kz,fil_pkost.kost_art ");





	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *) ausgabe.fil_kla, SQLCHAR, 2);
    dbRange.RangeIn ("fil.fil",filbereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.kost_art, SQLSHORT, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_mo, SQLDOUBLE, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_mo_vj, SQLDOUBLE, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_b, SQLDOUBLE, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_b_vj, SQLDOUBLE, 0);   
	dbClass.sqlout ((char *) ausgabe.gr_kz, SQLCHAR, 2);   
	dbClass.sqlout ((char *) ausgabe.kost_art_bz, SQLCHAR, 25);   

	fil_kost_cursor_gr = dbClass.sqlcursor (dbRange.SetRange("select fil_pkost.kost_art, "
		"sum (case when fil_pkost.jr = ? and fil_pkost.period = ? then fil_pkost.kost_period end) , "
		"sum (case when fil_pkost.jr = ? -1 and fil_pkost.period = ? then fil_pkost.kost_period end) , "
		"sum (case when fil_pkost.jr = ? then fil_pkost.kost_period end) , "
		"sum (case when fil_pkost.jr = ? -1 then fil_pkost.kost_period end) , "
		"fil_kostgr.gr_kz, kost_art.kost_art_bz "
						"from fil_pkost,fil_kostgr,kost_art "
						"where fil_pkost.mdn = fil_kostgr.mdn and fil_pkost.kost_art = fil_kostgr.kost_art and kost_art.kost_art = fil_pkost.kost_art "
						" and fil_pkost.mdn = ? and fil_pkost.fil in (select fil from fil where fil_kla =  ? and <fil.fil>) "
						"and fil_kostgr.gr_kz in (\"1\",\"2\") and fil_pkost.period <= ? "
						"group by 1,6,7 order by fil_kostgr.gr_kz,fil_pkost.kost_art "));



	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
    dbRange.RangeIn ("fil.fil",filbereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.kost_art, SQLSHORT, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_mo, SQLDOUBLE, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_mo_vj, SQLDOUBLE, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_b, SQLDOUBLE, 0);   
	dbClass.sqlout ((double *) &ausgabe.kost_b_vj, SQLDOUBLE, 0);   
	dbClass.sqlout ((char *) ausgabe.gr_kz, SQLCHAR, 2);   
	dbClass.sqlout ((char *) ausgabe.kost_art_bz, SQLCHAR, 25);   

	fil_kost_cursor_ges = dbClass.sqlcursor (dbRange.SetRange("select fil_pkost.kost_art, "
		"sum (case when fil_pkost.jr = ? and fil_pkost.period = ? then fil_pkost.kost_period end) , "
		"sum (case when fil_pkost.jr = ? -1 and fil_pkost.period = ? then fil_pkost.kost_period end) , "
		"sum (case when fil_pkost.jr = ? then fil_pkost.kost_period end) , "
		"sum (case when fil_pkost.jr = ? -1 then fil_pkost.kost_period end) , "
		"fil_kostgr.gr_kz, kost_art.kost_art_bz "
						"from fil_pkost,fil_kostgr,kost_art "
						"where fil_pkost.mdn = fil_kostgr.mdn and fil_pkost.kost_art = fil_kostgr.kost_art and kost_art.kost_art = fil_pkost.kost_art "
						" and fil_pkost.mdn = ? and fil_pkost.fil in (select fil from fil where <fil.fil>) "
						"and fil_kostgr.gr_kz in (\"1\",\"2\") and fil_pkost.period <= ? "
						"group by 1,6,7 order by fil_kostgr.gr_kz,fil_pkost.kost_art "));




	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);

	fil_abrech_cursor = dbClass.sqlcursor ("select end_dat  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= " 
						"(select end_dat from fil_abrech where mdn = ? and fil = 0 "
						"and period_kz = \"M\" and jr = ? and period = ?) "
						"order by end_dat desc ");



	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);

	fil_abrech_cursor_vj = dbClass.sqlcursor ("select end_dat  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= " 
						"(select end_dat from fil_abrech where mdn = ? and fil = 0 "
						"and period_kz = \"M\" and jr = ? and period = ?) "
						"order by end_dat desc ");





	dbClass.sqlin ((char *) datumvj_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumvj_bis, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumjr_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumjr_bis, SQLCHAR, 11);

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((char *) datumvj_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumjr_bis, SQLCHAR, 11);
	dbClass.sqlin ((char *) datummo_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datummo_bis, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumvjmo_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumvjmo_bis, SQLCHAR, 11);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);   
	dbClass.sqlout ((double *) &ausgabe.einn, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.einn_bto, SQLDOUBLE, 0); //HAUS-6
	dbClass.sqlout ((long *) &ausgabe.kun_anz_int, SQLLONG, 0); //HAUS-6

	// cursor f�r Monat (z.b. bei mo = 6 : nur Monat 6)
	// 2 * fetch , f�r vjr und jr
           read_fil_einn = dbClass.sqlcursor ("select "
							"case when einn_dat between ? and ? then 1 "
							"when einn_dat between  ? and ? then 2 end jr, " 
							"sum(fil_einn.einn_nto), "
							"sum(fil_einn.einn), "
							"sum(fil_einn.kun_anz_int) "
							"from fil_einn where "
							"    fil_einn.mdn = ? "
							"and fil_einn.fil = ? "
							"and fil_einn.einn_dat between ? and ? "
							"and ( "
							 "   fil_einn.einn_dat between ? and ? or "
							 "   fil_einn.einn_dat between ? and ? "
							 " ) "
							"group by 1 order by jr "
							);





//========================================================================
//HAUS-6======= Cursor Zusatzwerte Ums,kg,ertrag =========================
//========================================================================
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat_von, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);   
	dbClass.sqlout ((short *) &ausgabe.monat, SQLSHORT, 0);   
	dbClass.sqlout ((double *) &ausgabe.z_me_vk, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.z_ums_vk, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.z_ums_fil_ek, SQLDOUBLE, 0);

           read_z_ums = dbClass.sqlcursor ("select "
							"case when jr = ? then 1 "
							"when jr =  ? then 2 end jr, "	
							"a_per_wa.period, " 
							"sum(case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end ) menge, "
							"sum (a_per_wa.ums_vk_per), "
							"sum (a_per_wa.ums_fil_ek_per) "
							"from a_per_wa, a_bas where "
							"    a_per_wa.mdn = ? "
							"and a_per_wa.fil = ? "
							"and a_per_wa.kun = 0 "
							"and a_per_wa.jr between ? and ? "
							"and a_per_wa.period between ? and ? "
							"and a_bas.a = a_per_wa.a "
							"group by 1,2 order by jr,period "
							);


	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat_von, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);   
	dbClass.sqlout ((short *) &ausgabe.monat, SQLSHORT, 0);   
	dbClass.sqlout ((double *) &ausgabe.z_me_vk, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.z_ums_vk, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.z_ums_fil_ek, SQLDOUBLE, 0);

           read_z_ums_we = dbClass.sqlcursor ("select "
							"case when jr = ? then 1 "
							"when jr =  ? then 2 end jr, "	
							"a_per_we.period, " 
							"sum(case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_per_we.me_ek_per  end ) menge, "
							"sum (a_per_we.ums_fil_vk_per / ptabn.ptwer2 ), "
							"sum(a_per_we.ums_fil_ek_per) "
							"from a_per_we,a_bas, ptabn where a_per_we.a = a_bas.a "
							"and a_per_we.mdn = ? "
							"and a_per_we.fil = ? "
							"and a_per_we.jr between ? and ? "
							"and a_per_we.period between ? and ? "
							"and a_per_we.me_ek_per <> 0.0 "
							"and ptabn.ptitem = \"mwst\" and ptabn.ptlfnr = a_bas.mwst "
							"group by 1,2 order by jr,period "
							);


//========================================================================
//HAUS-6======= Cursor Lohnstunden fil_lohn_std ==========================
//========================================================================
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat_von, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);   
	dbClass.sqlout ((short *) &ausgabe.monat, SQLSHORT, 0);   
	dbClass.sqlout ((double *) &ausgabe.stunden, SQLDOUBLE, 0);   

           read_lohnstunden = dbClass.sqlcursor ("select "
							"case when jr = ? then 1 "
							"when jr =  ? then 2 end jr, "	
							"fil_lohn_std.monat, "
							"sum (fil_lohn_std.stunden) "
							"from fil_lohn_std where "
							"    fil_lohn_std.mdn = ? "
							"and fil_lohn_std.fil = ? "
							"and fil_lohn_std.jr between ? and ? "
							"and fil_lohn_std.monat between ? and ? "
							"group by 1,2 order by jr,monat "
							);



	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
//	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
    dbRange.RangeIn ("fil.fil",filbereich, SQLSHORT, 0); 
	dbClass.sqlout ((short *) &a_mo_wa.fil, SQLLONG, 0);
	dbClass.sqlout ((char *) a_mo_wa.fil_kla, SQLCHAR, 2);

           count_cursor = dbClass.sqlcursor (dbRange.SetRange ("select fil, fil_kla from fil where mdn = ? and <fil.fil> "
						" order by fil_kla,fil"));


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
//	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
    dbRange.RangeIn ("fil.fil",filbereich, SQLSHORT, 0); 
	dbClass.sqlout ((char *) a_mo_wa.fil_kla, SQLCHAR, 2);

           fil_kla_cursor = dbClass.sqlcursor (dbRange.SetRange ("select  fil_kla from fil where mdn = ? and <fil.fil> "
						" order by fil_kla"));




//===========================================================================================
//===========================================================================================
//===========================================================================================
//===============     readcursor         ====================================================
//===========================================================================================
//===========================================================================================
//===========================================================================================




//=============================================
//========= Menge =============================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Menge SA ==========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 


//=============================================
//========= Umsatz zm SK ======================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 




//=============================================
//========= Umsatz zm SK SA======================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 





//=============================================
//========= Umsatz  ===========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Umsatz SA  ========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 



//=============================================
//========= Retouren Menge ===================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Retouren Umsatz ===================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 
	


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat_von, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);

	dbClass.sqlout ((char *) a_mo_wa.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((short *) &a_mo_wa.fil, SQLLONG, 0);
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);

//========= Menge =============================
	dbClass.sqlout ((double *) &ausgabe.menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr6, SQLDOUBLE, 0);
//========= Menge SA ===========================
	dbClass.sqlout ((double *) &ausgabe.menge_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr6, SQLDOUBLE, 0);
//========= Umsatz zum SK =====================
	dbClass.sqlout ((double *) &ausgabe.umsatz_sk, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr6, SQLDOUBLE, 0);
//========= Umsatz zum SK SA =====================
	dbClass.sqlout ((double *) &ausgabe.umsatz_sk_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr6, SQLDOUBLE, 0);
//========= Umsatz ============================
	dbClass.sqlout ((double *) &ausgabe.umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr6, SQLDOUBLE, 0);
//========= Umsatz SA ============================
	dbClass.sqlout ((double *) &ausgabe.umsatz_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr6, SQLDOUBLE, 0);
//========= Retouren  Menge =============================
	dbClass.sqlout ((double *) &ausgabe.retouren_me, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr6, SQLDOUBLE, 0);
//========= Retouren  Umsatz =============================
	dbClass.sqlout ((double *) &ausgabe.retouren, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr6, SQLDOUBLE, 0);


           readcursor = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_per_wa.fil > 0 then fil.fil_kla "
							"	when a_per_wa.fil = 0 then \"\" end klasse , "


							"a_per_wa.fil, a_per_wa.jr, "

//=============================================
//========= Menge =============================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

//=============================================
//========= Menge SA ==========================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "
//=============================================
//========= Umsatz zum SK =====================
//=============================================
							"sum (a_per_wa.ums_sk_vkost_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

//=============================================
//========= Umsatz zum SK SA===================
//=============================================
							"sum (a_per_wa.ums_sk_vkost_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

//=============================================
//========= Umsatz  ===========================
//=============================================
							"sum (a_per_wa.ums_vk_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "
//=============================================
//========= Umsatz SA =========================
//=============================================
							"sum (a_per_wa.ums_vk_sa_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "
//=============================================
//========= Retouren Menge ====================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "
//=============================================
//========= Retouren Umsatz  ===========================
//=============================================
							"sum (a_per_wa.ums_ret_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ) "

							"from a_per_wa,a_bas, outer fil where "
							"    a_per_wa.mdn = fil.mdn "
							"and a_per_wa.fil = fil.fil "
							"and a_per_wa.a = a_bas.a "
							"and a_per_wa.mdn = ? "
							"and a_per_wa.fil = ? "
							"and a_per_wa.kun = 0 "
							"and a_per_wa.jr between ? and ? "
							"and a_per_wa.period between ? and ? "
							"and (a_per_wa.me_vk_per <> 0 or a_per_wa.me_ret_per <> 0) "
							"group by 1,2,3 order by klasse,a_per_wa.fil "
							));



//===========================================================================================
//===========================================================================================
//===========================================================================================
//===============     readcursor_we       ====================================================
//===========================================================================================
//===========================================================================================
//===========================================================================================




//=============================================
//========= Menge =============================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Menge SA ==========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 


//=============================================
//========= Umsatz zm SK ======================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Umsatz zm SK SA====================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 


//=============================================
//========= Umsatz  ===========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Umsatz SA  ========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 



//=============================================
//========= Retouren Menge ===================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Retouren Umsatz ===================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 
	


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat_von, SQLSHORT, 0);
	dbClass.sqlin ((short *) &monat, SQLSHORT, 0);

	dbClass.sqlout ((char *) a_mo_wa.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((short *) &a_mo_wa.fil, SQLLONG, 0);
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);

//========= Menge =============================
	dbClass.sqlout ((double *) &ausgabe_we.menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr6, SQLDOUBLE, 0);
//========= Menge SA ===========================
	dbClass.sqlout ((double *) &ausgabe_we.menge_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr6, SQLDOUBLE, 0);
//========= Umsatz zum SK =====================
	dbClass.sqlout ((double *) &ausgabe_we.umsatz_sk, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr6, SQLDOUBLE, 0);
//========= Umsatz zum SK SA =====================
	dbClass.sqlout ((double *) &ausgabe_we.umsatz_sk_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr6, SQLDOUBLE, 0);
//========= Umsatz ============================
	dbClass.sqlout ((double *) &ausgabe_we.umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr6, SQLDOUBLE, 0);
//========= Umsatz SA ============================
	dbClass.sqlout ((double *) &ausgabe_we.umsatz_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr6, SQLDOUBLE, 0);
//========= Retouren  Menge =============================
	dbClass.sqlout ((double *) &ausgabe_we.retouren_me, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr6, SQLDOUBLE, 0);
//========= Retouren  Umsatz =============================
	dbClass.sqlout ((double *) &ausgabe_we.retouren, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr6, SQLDOUBLE, 0);


           readcursor_we = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_per_we.fil > 0 then fil.fil_kla "
							"	when a_per_we.fil = 0 then \"\" end klasse , "


							"a_per_we.fil, a_per_we.jr, " 

//=============================================
//========= Menge =============================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

//=============================================
//========= Menge SA ==========================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "
//=============================================
//========= Umsatz zum SK =====================
//=============================================
							"sum (a_per_we.ums_fil_ek_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

//=============================================
//========= Umsatz zum SK SA===================
//=============================================
							"sum (a_per_we.ums_fil_ek_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "
//=============================================
//========= Umsatz  ===========================
//=============================================
							"sum (a_per_we.ums_fil_vk_per  / ptabn.ptwer2 ), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2 end ), "
//=============================================
//========= Umsatz SA =========================
//=============================================
							"sum (a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2 ), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2" // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2"
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2 end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2 " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2   "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2 " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per   / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2 end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2 " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per   / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2 " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per   / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  end ), "
//=============================================
//========= Retouren Menge ====================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "
//=============================================
//========= Retouren Umsatz  ===========================
//=============================================
							"sum (a_per_we.ums_ret_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ) "

							"from a_per_we,a_bas,ptabn, outer fil where "
							"    a_per_we.mdn = fil.mdn "
							"and a_per_we.fil = fil.fil "
							"and a_per_we.a = a_bas.a "
							"and a_per_we.mdn = ? "
							"and a_per_we.fil = ? "
							"and a_per_we.jr = ? "
							"and a_per_we.period between ? and ? "
							"and ptabn.ptitem = \"mwst\" and ptabn.ptlfnr = a_bas.mwst "
							"and (a_per_we.me_ek_per <> 0 or a_per_we.me_ret_per <> 0) "
							"group by 1,2,3 order by klasse,a_per_we.fil "
							));







	test_upd_cursor = 1;
}


void A_MO_WA_CLASS::berechnePerioden (void)
{
	// n�tig wegen Umsatz aus fil_einn
//========================================================================
//============= Hole Perioden im lfd-Jahr ================================
//========================================================================
		strcpy(erster_tag_wo1,"");
		strcpy(erster_tag_wo2,"");
		strcpy(erster_tag_wo3,"");
		strcpy(erster_tag_wo4,"");
		strcpy(erster_tag_wo5,"");
		strcpy(letzter_tag_wo1,"");
		strcpy(letzter_tag_wo2,"");
		strcpy(letzter_tag_wo3,"");
		strcpy(letzter_tag_wo4,"");
		strcpy(letzter_tag_wo5,"");

         dbClass.sqlopen (fil_abrech_cursor);
	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;

		d_datlast.SetzeDatum(1,1,_jr);
		d_datlast.format(datumjr_von);

		d_datlast.SetzeDatum(_tag,_mo,_jr);
		d_datlast.format(datummo_bis);
		d_datlast.format(datumjr_bis);
		--d_datlast;
		d_datlast.format(letzter_tag);
		letzte_wo = get_woche(letzter_tag);
		_wo = letzte_wo;
		while (letzte_wo == _wo)
		{
			++d_datlast;
			d_datlast.format(letzter_tag);
			_wo = get_woche(letzter_tag);
		}
		--d_datlast;
		d_datlast.format(letzter_tag);//Dies ist jetzt der letzte Tag der KW

	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;
		d_datfirst.SetzeDatum(_tag,_mo,_jr);
		++d_datfirst;
		++d_datfirst;
		d_datfirst.format(erster_tag);
		_jr = get_jahr(erster_tag); 
		erste_wo = get_woche(erster_tag);
		_wo = erste_wo;
		flg_wo1_gekappt = FALSE;
		while (erste_wo == _wo)
		{
			--d_datfirst;
			d_datfirst.format(erster_tag);
			if (get_jahr(erster_tag) != _jr && flg_wo1_gekappt == FALSE) 
			{
				++d_datfirst;
				d_datfirst.format(erster_tag_wo1);
				d_datfirst.format(datummo_von);
				flg_wo1_gekappt = TRUE;
				--d_datfirst;
			}
			_wo = get_woche(erster_tag);
		}
		++d_datfirst;
		d_datfirst.format(erster_tag);//Dies ist jetzt der 1. Tag der KW
		if (flg_wo1_gekappt == FALSE) d_datfirst.format(datummo_von);
		wo1 = get_woche(erster_tag); 


		d_dat=d_datfirst;

		if (flg_wo1_gekappt == FALSE) strncpy(erster_tag_wo1,erster_tag,10);
		d_dat=d_dat+6;
		d_dat.format(letzter_tag_wo1);
		++d_dat;
		/****************************************** 260211 Wochen brauchen wir hier nicht, f�hrt zu Fehler
		while (d_datlast-d_dat > 0)
		{
			if (wo2 == 0) 
			{
				d_dat.format(erster_tag_wo2);
				wo2 = get_woche(erster_tag_wo2);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo2);
			}
			else if (wo3 == 0)
			{
				d_dat.format(erster_tag_wo3);
				wo3 = get_woche(erster_tag_wo3);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo3);
			}
			else if (wo4 == 0)
			{
				d_dat.format(erster_tag_wo4);
				wo4 = get_woche(erster_tag_wo4);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo4);
				d_dat.format(datummo_bis);
				d_dat.format(datumjr_bis);
			}
			else if (wo5 == 0)
			{
				d_dat.format(erster_tag_wo5);
				wo5 = get_woche(erster_tag_wo5);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo5);
				d_dat.format(datummo_bis);
				d_dat.format(datumjr_bis);
			}
			++d_dat;
		}
		*****************************************************/

//========================================================================
//============= Hole Perioden im Vorjahr ================================
//========================================================================
		strcpy(erster_tag_wo1_vj,"");
		strcpy(erster_tag_wo2_vj,"");
		strcpy(erster_tag_wo3_vj,"");
		strcpy(erster_tag_wo4_vj,"");
		strcpy(erster_tag_wo5_vj,"");
		strcpy(letzter_tag_wo1_vj,"");
		strcpy(letzter_tag_wo2_vj,"");
		strcpy(letzter_tag_wo3_vj,"");
		strcpy(letzter_tag_wo4_vj,"");
		strcpy(letzter_tag_wo5_vj,"");

         dbClass.sqlopen (fil_abrech_cursor_vj);
	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor_vj);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;

		d_datlast.SetzeDatum(1,1,_jr);
		d_datlast.format(datumvj_von);
		d_datlast.format(datumvj_bis);


		d_datlast.SetzeDatum(_tag,_mo,_jr);
		d_datlast.format(datumvjmo_bis);
		d_datlast.format(datumvj_bis);
		--d_datlast;
		d_datlast.format(letzter_tag);
		letzte_wo = get_woche(letzter_tag);
		_wo = letzte_wo;
		while (letzte_wo == _wo)
		{
			++d_datlast;
			d_datlast.format(letzter_tag);
			_wo = get_woche(letzter_tag);
		}
		--d_datlast;
		d_datlast.format(letzter_tag);//Dies ist jetzt der letzte Tag der KW

	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor_vj);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;
		d_datfirst.SetzeDatum(_tag,_mo,_jr);
		++d_datfirst;
		d_datfirst.format(erster_tag);
		_jr = get_jahr(erster_tag); 
		d_datfirst.format(datumvjmo_von);
		erste_wo = get_woche(erster_tag);
		_wo = erste_wo;
		flg_wo1vj_gekappt = FALSE;
		while (erste_wo == _wo)
		{
			--d_datfirst;
			d_datfirst.format(erster_tag);
			if (get_jahr(erster_tag) != _jr && flg_wo1vj_gekappt == FALSE) 
			{
				++d_datfirst;
				d_datfirst.format(erster_tag_wo1_vj);
				d_datfirst.format(datumvjmo_von);
				flg_wo1vj_gekappt = TRUE;
				--d_datfirst;
				break;
			}
			_wo = get_woche(erster_tag);
		}
		++d_datfirst;
		d_datfirst.format(erster_tag);//Dies ist jetzt der 1. Tag der KW
		wo1_vj = get_woche(erster_tag);


		d_dat=d_datfirst;

		if (flg_wo1vj_gekappt == FALSE) strncpy(erster_tag_wo1_vj,erster_tag,10);
		d_dat=d_dat+6;
		d_dat.format(letzter_tag_wo1_vj);
		++d_dat;
		/****************************************** 260211 Wochen brauchen wir hier nicht, f�hrt zu Fehler
		while (d_datlast-d_dat > 0)
		{
			if (wo2_vj == 0) 
			{
				d_dat.format(erster_tag_wo2_vj);
				wo2_vj = get_woche(erster_tag_wo2_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo2_vj);
			}
			else if (wo3_vj == 0)
			{
				d_dat.format(erster_tag_wo3_vj);
				wo3_vj = get_woche(erster_tag_wo3_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo3_vj);
			}
			else if (wo4_vj == 0)
			{
				d_dat.format(erster_tag_wo4_vj);
				wo4_vj = get_woche(erster_tag_wo4_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo4_vj);
				d_dat.format(datumvjmo_bis);
				d_dat.format(datumvj_bis);
			}
			else if (wo5_vj == 0)
			{
				d_dat.format(erster_tag_wo5_vj);
				wo5_vj = get_woche(erster_tag_wo5_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo5_vj);
				d_dat.format(datumvjmo_bis);
				d_dat.format(datumvj_bis);
			}
			++d_dat;
		}
***************************************/



		woche_jr = jr;
		if (erste_wo > letzte_wo) woche_jr --;

}
