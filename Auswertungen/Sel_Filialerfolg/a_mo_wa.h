#ifndef _A_MO_WA_DEF
#define _A_MO_WA_DEF

struct A_BAS { 
   short     me_einh;
   double	a_gew;
};
extern struct A_BAS a_bas, a_bas_null;

struct AUSGABE { 
   short     fil;
   char     fil_kla[2];
   long     vertr;
   long     kun;
   short     kun_fil;
   double menge;
   double me_gr1;
   double me_gr2;
   double me_gr3;
   double me_gr4;
   double me_gr5;
   double me_gr6;
   double menge_sa;
   double me_sa_gr1;
   double me_sa_gr2;
   double me_sa_gr3;
   double me_sa_gr4;
   double me_sa_gr5;
   double me_sa_gr6;
   double ges_menge;
   double ges_me_gr1;
   double ges_me_gr2;
   double ges_me_gr3;
   double ges_me_gr4;
   double ges_me_gr5;
   double ges_me_gr6;

   double umsatz;
   double ums_gr1;
   double ums_gr2;
   double ums_gr3;
   double ums_gr4;
   double ums_gr5;
   double ums_gr6;
   double ges_umsatz;
   double ges_ums_gr1;
   double ges_ums_gr2;
   double ges_ums_gr3;
   double ges_ums_gr4;
   double ges_ums_gr5;
   double ges_ums_gr6;

   double umsatz_sk;
   double ums_sk_gr1;
   double ums_sk_gr2;
   double ums_sk_gr3;
   double ums_sk_gr4;
   double ums_sk_gr5;
   double ums_sk_gr6;

   double umsatz_sk_sa;
   double ums_sk_sa_gr1;
   double ums_sk_sa_gr2;
   double ums_sk_sa_gr3;
   double ums_sk_sa_gr4;
   double ums_sk_sa_gr5;
   double ums_sk_sa_gr6;


   double pr_sk;
   double pr_sk_gr1;
   double pr_sk_gr2;
   double pr_sk_gr3;
   double pr_sk_gr4;
   double pr_sk_gr5;
   double pr_sk_gr6;


   double umsatz_sa;
   double ums_sa_gr1;
   double ums_sa_gr2;
   double ums_sa_gr3;
   double ums_sa_gr4;
   double ums_sa_gr5;
   double ums_sa_gr6;

   double ertrag;
   double ertrag_gr1;
   double ertrag_gr2;
   double ertrag_gr3;
   double ertrag_gr4;
   double ertrag_gr5;
   double ertrag_gr6;

   double retouren_sa;
   double ret_sa_gr1;
   double ret_sa_gr2;
   double ret_sa_gr3;
   double ret_sa_gr4;
   double ret_sa_gr5;
   double ret_sa_gr6;

   double retouren;
   double ret_gr1;
   double ret_gr2;
   double ret_gr3;
   double ret_gr4;
   double ret_gr5;
   double ret_gr6;

   double retouren_me_sa;
   double ret_me_sa_gr1;
   double ret_me_sa_gr2;
   double ret_me_sa_gr3;
   double ret_me_sa_gr4;
   double ret_me_sa_gr5;
   double ret_me_sa_gr6;

   double retouren_me;
   double ret_me_gr1;
   double ret_me_gr2;
   double ret_me_gr3;
   double ret_me_gr4;
   double ret_me_gr5;
   double ret_me_gr6;

   double fil_menge;
   double fil_umsatz;
   double fil_ges_umsatz;
   double fil_anteil;
   double fil_ertrag;
   double auswahl_menge;
   double auswahl_umsatz;
   double auswahl_ertrag;
   double ges_ertrag;
   long fil_anzahl;
   double a;
   char a_bz1[25];
   double ProzGesamt;
   double ProzAuswahl;
   double einn;

	double mo_einn;
	double ges_einn;
	double vj_mo_einn;
	double vj_ges_einn;

//HAUS-6 A
   double einn_bto; 
   long  kun_anz_int; 

	double mo_einn_bto;
	double ges_einn_bto;
	double vj_mo_einn_bto;
	double vj_ges_einn_bto;

	long mo_kun_anz_int;
	long ges_kun_anz_int;
	long vj_mo_kun_anz_int;
	long vj_ges_kun_anz_int;

   double stunden; 
	double mo_stunden;
	double ges_stunden;
	double vj_mo_stunden;
	double vj_ges_stunden;

	double z_me_vk;
	double mo_z_me_vk;
	double ges_z_me_vk;
	double vj_mo_z_me_vk;
	double vj_ges_z_me_vk;

	double z_ums_vk;
	double mo_z_ums_vk;
	double ges_z_ums_vk;
	double vj_mo_z_ums_vk;
	double vj_ges_z_ums_vk;

	double z_ums_fil_ek;
	double mo_z_ums_fil_ek;
	double ges_z_ums_fil_ek;
	double vj_mo_z_ums_fil_ek;
	double vj_ges_z_ums_fil_ek;

//HAUS-6 E

   short jr;
   short monat;
   short kost_art;
   double kost_mo;
   double kost_mo_vj;
   double kost_b;
   double kost_b_vj;
   char gr_kz [2];
   char kost_art_bz [25];
};

extern struct AUSGABE ausgabe, ausgabe_m,ausgabe_b,ausgabe_vj_m,ausgabe_vj_b,
				ausgabegr_m,ausgabegr_b,ausgabegr_vj_m,ausgabegr_vj_b,
				ausgabeges_m,ausgabeges_b,ausgabeges_vj_m,ausgabeges_vj_b,ausgabe_we, ausgabe_null;

struct A_MO_WA {
   short     delstatus;
   short     mdn;
   short     fil;
   long      kun;
   double    a;
   short     jr;
   short     mo;
   double    me_vk_mo;
   double    ums_vk_mo;
   double    ums_fil_ek_mo;
   double    me_vk_sa_mo;
   double    ums_vk_sa_mo;
   double    ums_fil_ek_sa_mo;
   double    me_ret_mo;
   double    ums_hk_vkost_mo;
   double    ums_hk_tkost_mo;
   double    ums_sk_vkost_mo;
   double    ums_sk_tkost_mo;
   double    ums_mat_o_b_mo;
   double    ums_ret_mo;
   double    ums_fil_ek_ret_mo;
   double    gn_pkt_gbr_ges;
   short     waehrung;
   double    ums_vk_e;
   double    ums_vk_f;
   double    ums_vk_sa_e;
   double    ums_vk_sa_f;
   double    ums_ret_e;
   double    ums_ret_f;
   double    ums_fil_ek_e;
   double    ums_fil_ek_ret_e;
   double    ums_fil_ek_sa_e;
   double    gn_pkt_gbr_e;
   double    m_kun_mo;
   double    u_kun_mo;
   double    u_kun_fek_mo;
   double    m_kun_ret_mo;
   double    u_kun_ret_mo;
   double    u_kun_fek_ret_mo;
   double    ue_kun_mo;
   double    ue_kun_fek_mo;
   double    ue_kun_ret_mo;
   double    ue_kun_fek_ret_mo;
   double    m_upls_mo;
   double    u_upls_mo;
   double    u_upls_fek_mo;
   double    ue_upls_mo;
   double    ue_upls_fek_mo;
   double    m_umin_mo;
   double    u_umin_mo;
   double    u_umin_fek_mo;
   double    ue_umin_mo;
   double    ue_umin_fek_mo;
   double    m_we_mo;
   double    u_we_ek_mo;
   double    u_we_fek_mo;
   double    u_we_fvk_mo;
   double    m_we_ret_mo;
   double    u_we_ret_ek_mo;
   double    u_we_ret_fek_mo;
   double    u_we_ret_fvk_mo;
   double    ue_we_ek_mo;
   double    ue_we_fek_mo;
   double    ue_we_fvk_mo;
   double    ue_we_ret_ek_mo;
   double    ue_we_ret_fek_mo;
   double    ue_we_ret_fvk_mo;
   double    me1_vk_mo;
   double    me2_vk_mo;
   double    me3_vk_mo;
   char     fil_kla[2];
};
extern struct A_MO_WA a_mo_wa, a_mo_wa_null;

class A_MO_WA_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
               void prepare_per (void);
				int fil_abrech_cursor;
				int fil_abrech_cursor_vj;
				int fil_kost_cursor;
				int fil_kost_cursor_gr;
				int fil_kost_cursor_ges;
				int read_fil_einn;
				int read_lohnstunden;
				int read_z_ums;
				int read_z_ums_we;
				int fil_kla_cursor;
				int readcursor_we;
       public :
               int dbcount (void);
               int leseDS (int);
               int openDS (void);
               void fuelle_ausgabe (void);
               void fuelle_summen_m (void);
               void fuelle_summen_b (void);
               void berechnePerioden (void);

               A_MO_WA_CLASS () : DB_CLASS ()
               {
				 fil_abrech_cursor = -1;
				 fil_abrech_cursor_vj = -1;
				 fil_kost_cursor = -1;
				 fil_kost_cursor_gr = -1;
				 fil_kost_cursor_ges = -1;
				 read_fil_einn = -1;
				 read_lohnstunden = -1;
				 read_z_ums = -1;
				 read_z_ums_we = -1;
				 fil_kla_cursor = -1;
				 readcursor_we = -1;
               }
               ~A_MO_WA_CLASS ()
               {
		         if (fil_abrech_cursor != -1) sqlclose (fil_abrech_cursor); 
		         if (fil_abrech_cursor_vj != -1) sqlclose (fil_abrech_cursor_vj); 
		         if (read_fil_einn != -1) sqlclose (read_fil_einn); 
		         if (read_lohnstunden != -1) sqlclose (read_lohnstunden); 
		         if (read_z_ums != -1) sqlclose (read_z_ums); 
		         if (read_z_ums_we != -1) sqlclose (read_z_ums_we); 
		         if (fil_kost_cursor != -1) sqlclose (fil_kost_cursor); 
		         if (fil_kost_cursor_gr != -1) sqlclose (fil_kost_cursor_gr); 
		         if (fil_kost_cursor_ges != -1) sqlclose (fil_kost_cursor_ges); 
		         if (fil_kla_cursor != -1) sqlclose (fil_kla_cursor); 
		         if (readcursor_we != -1) sqlclose (readcursor_we); 
               } 


};
#endif
