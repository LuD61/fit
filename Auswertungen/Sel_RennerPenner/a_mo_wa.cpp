#include "stdafx.h"
#include "DbClass.h"
#include "DbRange.h"
#include "datum.h"
#include "a_mo_wa.h"
#include "cmbtl9.h"
#include <string>
using namespace std;
//#include "cmbtll11.h"

struct A_MO_WA a_mo_wa, a_mo_wa_null;
struct AUSGABE ausgabe, ausgabegr, ausgabeges, ausgabe_null;
struct A_BAS a_bas, a_bas_null;
extern DB_CLASS dbClass;
extern DB_RANGE dbRange;
static long anzfelder = -1;
short mdn = 1;
char kunbereich[256];
char filbereich[256];
char wgbereich[256];
char abereich[256];
extern char datvon[12] ;
extern char datbis[12] ;
long max_anz = 10;
short jr = 0;
short vjr = 0;
short vvjr = 0;
short movon ;
short monat ;
BOOL flg_ges_holen = FALSE;
extern char order_by_zusatz[7] ;
extern char order_by[11] ;
extern char BezRennerPenner [] ;
extern char BezSortierung [] ;
extern char R_menge[2] ;
extern char R_umsatz[2] ;
extern char R_ertrag[2] ;

DATUM d_dat, d_datfirst, d_datlast;
DATE_STRUCT end_dat;
char adr_krz [25];
double a_gew;
extern char Listenname[128];
char sys_par_wrt [2] = " ";
	 double we_a;




static char *clipped (char *str)
/**
Blank am Ende eines Strings abschneiden.
**/
{
          int i;

          i = strlen (str);
          if (i == 0) return (str);

          for (i = i - 1; i > 0; i --)
          {
                    if (str[i] > ' ')
                    {
                              str [i + 1] = (char) 0;
                              break;
                    }
          }
          return (str);
}

/***********
const char* Replace(char* strBase, const std::string to_find, const std::string to_replace)
{

//	a, b: a <= "string gefunden" <= b
//	i, j: Z�hler f�r 'strBase' und 'to_find'
//	k: markiert den Anfang eines Suchdurchgangs
//	count: Anzahl der zutreffenden Zeichen
// end_string: replaced string

char* end_string;
unsigned long int a, b, i, j, k, count;
std::string final;
a = b = i = j = k = count = 0;

if (to_find.length() > strlen(strBase))
return (const char*)NULL;

while (*(strBase + i) != '\0')
{
	if (*(strBase + i) == to_find[j])
	{
		a = i;
		++i; ++j; ++count;
		while (*(strBase + i) == to_find[j] && *(strBase + i) != NULL)
		{
			b = i++; 
			++j; ++count;
		}
		if (count == to_find.length())
		{
		// kopiere alles von 'k' bis zum Anfang der gefundenen Zeichenfolge
			while (k < a) final += *(strBase + k++);
			final += to_replace.c_str();	 // kopiere den replace-string
		}
		else
		{
		// kopiere alles von 'k' bis zum letzen �berpr�ften Zeichen *verwirr*
			while (k <= b) final += *(strBase + k++);
		}
		k = b + 1;	 // k: Ende des gefundenen Strings -> Anfang f�r neue Suche
		b = i;	 // b muss mit aktuellem Suchindex mitlaufen
		count = j = 0;
	}
	else
	{
		final += *(strBase + i);
		k = b = ++i;	 // k und b m�ssen mit aktuellem Index mitlaufen
	}
}

end_string = new char[final.length() + 1];
strcpy(end_string, final.c_str());
return end_string;
}

*************/

int A_MO_WA_CLASS::dbcount (void)
/**
Tabelle eti lesen.
**/
{


         if (test_upd_cursor == -1)
         {
// 			prepare_ret();  k�nnte man aktivieren f�r Auswertung nur Retouren  ( evt. checkbox)
			strcpy(filbereich,kunbereich);
			 prepare();
         }

//	 filvon = kunvon;
//	 filbis = kunbis;
//	 if (kunbis > 32766) filbis = 32766;

	    memcpy (&ausgabegr, &ausgabe_null, sizeof (struct AUSGABE));
	    memcpy (&ausgabeges, &ausgabe_null, sizeof (struct AUSGABE));

         dbClass.sqlopen (count_cursor);
         sqlstatus = dbClass.sqlfetch (count_cursor);
		 if (sqlstatus != 0) return -1 ;
		 anzfelder = 0;
         while (sqlstatus == 0)
         {
			 if (max_anz < ausgabe.fil_anzahl) ausgabe.fil_anzahl = max_anz;
			 anzfelder += ausgabe.fil_anzahl;
	         sqlstatus = dbClass.sqlfetch (count_cursor);
         }
         dbClass.sqlopen (count_cursor);

         dbClass.sqlopen (readcursor_gesamt);
         sqlstatus = dbClass.sqlfetch (readcursor_gesamt);
		 if (sqlstatus != 0) return -1 ;
		 int dz = 0;
         while (sqlstatus == 0)
         {
			 dz ++;
			 if (max_anz < dz) break;
			 anzfelder ++;
	         sqlstatus = dbClass.sqlfetch (readcursor_gesamt);
         }
         dbClass.sqlopen (readcursor_gesamt);


		 ausgabe.fil_anzahl = 0;
         return anzfelder; 
}

int A_MO_WA_CLASS::leseGes (void)
{
			ausgabe.sfil_ges_menge = 0;
			ausgabe.sfil_ges_umsatz = 0;
			ausgabe.sfil_ges_ertrag = 0;
			ausgabeges.menge_we = 0.0;
			ausgabeges.umsatz_we = 0.0;
			ausgabeges.ertrag_we = 0.0;
			sqlstatus = dbClass.sqlfetch (readcursor_gesamt);
			if (strcmp(R_menge,"J")== 0 && ausgabeges.fil_menge != 0.0 && ausgabeges.auswahl_menge != 0.0)
			{
				ausgabeges.ProzGesamt = ausgabeges.menge / ausgabeges.fil_menge * 100;
				ausgabeges.ProzAuswahl = ausgabeges.menge / ausgabeges.auswahl_menge * 100;
			} 
			else if (strcmp(R_umsatz,"J")== 0 && ausgabeges.fil_umsatz != 0.0 && ausgabeges.auswahl_menge != 0.0 ) 
			{
				ausgabeges.ProzGesamt = ausgabeges.umsatz / ausgabeges.fil_umsatz * 100;
				ausgabeges.ProzAuswahl = ausgabeges.umsatz / ausgabeges.auswahl_umsatz * 100;
			}
			else if (strcmp(R_ertrag,"J")== 0 && ausgabeges.fil_menge != 0.0 && ausgabeges.auswahl_menge != 0.0 ) 
			{
				ausgabeges.ProzGesamt = ausgabeges.ertrag / ausgabeges.fil_ertrag * 100;
				ausgabeges.ProzAuswahl = ausgabeges.ertrag / ausgabeges.auswahl_ertrag * 100;
			}

			strcpy(adr_krz, "Gesamt");
			ausgabe.kun_fil = 1;
			ausgabe.fil = 0;
			strcpy(ausgabe.fil_kla,"G");
			if (sqlstatus == 0) return 0;
			flg_ges_holen = FALSE;
			return sqlstatus;
}

int A_MO_WA_CLASS::leseDS (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }
		ausgabe.sfil_ges_menge = 0 ; //220212
		ausgabe.sfil_ges_umsatz = 0;   //220212
		ausgabe.sfil_ges_ertrag = 0 ; //220212

		if (flg_ges_holen == TRUE) return leseGes();
		

	  if (ausgabe.fil_anzahl == 0)
	  {

		fehlercode = dbClass.sqlfetch (count_cursor);
        while (fehlercode == 0)
		{
			if (ausgabe.fil_menge > 0.0001 || ausgabe.fil_menge < 0.001) break;
			fehlercode = dbClass.sqlfetch (count_cursor);
		}
  	    if (max_anz < ausgabe.fil_anzahl) ausgabe.fil_anzahl = max_anz;
		ausgabe.fil_menge += ausgabe.fil_menge_we;
		ausgabe.fil_umsatz += ausgabe.fil_umsatz_we;
		ausgabe.fil_ertrag += ausgabe.fil_ertrag_we;

		ausgabeges.fil_menge += ausgabe.fil_menge_we;
		ausgabeges.fil_umsatz += ausgabe.fil_umsatz_we;
		ausgabeges.fil_ertrag += ausgabe.fil_ertrag_we;

//281009		ausgabe.sfil_ges_menge = ausgabe.fil_menge ;  //nur zum Summieren im LL
//281009		ausgabe.sfil_ges_umsatz = ausgabe.fil_umsatz;   //nur zum Summieren im LL
//281009		ausgabe.sfil_ges_ertrag = ausgabe.fil_ertrag ;  //nur zum Summieren im LL

		  if (fehlercode == 0)
		  {
			 if (ausgabe.kun_fil == 1)
			 {
				 a_mo_wa.fil = (short) ausgabe.kun;
				 a_mo_wa.kun = 0;
				 ausgabe.kun = 0;
				 ausgabe.fil = a_mo_wa.fil;
			 }
			 else
			 {
				 ausgabe.fil = 0;
				 sprintf (ausgabe.fil_kla,"%ld",ausgabe.vertr);
				 a_mo_wa.kun = ausgabe.kun;
			 }

            if (ausgabe.kun_fil == 0)
			{
				dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
				dbClass.sqlout ((char *) adr_krz, SQLCHAR, 17);
				dbClass.sqlcomm ("select adr.adr_krz from kun,adr where kun = ?  and kun.adr1 = adr.adr");
			}
			else
			{
				dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
				dbClass.sqlout ((char *) adr_krz, SQLCHAR, 17);
				dbClass.sqlcomm ("select adr.adr_krz from fil,adr where fil.fil = ? and fil.adr = adr.adr ");
			}

			long anzahl = ausgabe.fil_anzahl;
			sqlstatus = 0;
			ausgabe.menge_we = 0.0;
			ausgabe.umsatz_we = 0.0;
			ausgabe.ertrag_we = 0.0;
			sqlstatus = dbClass.sqlopen (readcursor);
	        sqlstatus = dbClass.sqlfetch (readcursor);
//			ausgabe.menge += ausgabe.menge_we;
//			ausgabe.umsatz += ausgabe.umsatz_we;
//			ausgabe.ertrag += ausgabe.ertrag_we;
			ausgabe.auswahl_menge = 0.0;
			ausgabe.auswahl_umsatz = 0.0;
			ausgabe.auswahl_ertrag = 0.0;
			while (anzahl > 0 && sqlstatus == 0)
			{
				anzahl --;
				ausgabe.auswahl_menge += ausgabe.menge;
				ausgabe.auswahl_umsatz += ausgabe.umsatz;
				ausgabe.auswahl_ertrag += ausgabe.ertrag;
				ausgabe.menge_we = 0.0;
				ausgabe.umsatz_we = 0.0;
				ausgabe.ertrag_we = 0.0;
		        sqlstatus = dbClass.sqlfetch (readcursor);
//				ausgabe.menge += ausgabe.menge_we;
//				ausgabe.umsatz += ausgabe.umsatz_we;
//				ausgabe.ertrag += ausgabe.ertrag_we;
			}

			ausgabeges.auswahl_menge = 0.0;
			ausgabeges.auswahl_umsatz = 0.0;
			ausgabeges.auswahl_ertrag = 0.0;
			sqlstatus = dbClass.sqlopen (readcursor_gesamt);
	        sqlstatus = dbClass.sqlfetch (readcursor_gesamt);
			while (max_anz > 0 && sqlstatus == 0)
			{
				ausgabeges.auswahl_menge += ausgabeges.menge;
				ausgabeges.auswahl_umsatz += ausgabeges.umsatz;
				ausgabeges.auswahl_ertrag += ausgabeges.ertrag;
				ausgabeges.menge_we = 0.0;
				ausgabeges.umsatz_we = 0.0;
				ausgabeges.ertrag_we = 0.0;
		        sqlstatus = dbClass.sqlfetch (readcursor_gesamt);
			}
			sqlstatus = dbClass.sqlopen (readcursor_gesamt);





//281009  readcursor_ges war n�tig, da sonst falsche werte in fil_menge .... (durch die we-tabelle)
			sqlstatus = dbClass.sqlopen (readcursor_ges);
	        sqlstatus = dbClass.sqlfetch (readcursor_ges);
			ausgabe.fil_menge = 0.0;
			ausgabe.fil_umsatz = 0.0;
			ausgabe.fil_ertrag = 0.0;
			ausgabeges.fil_menge = 0.0;
			ausgabeges.fil_umsatz = 0.0;
			ausgabeges.fil_ertrag = 0.0;
			while (sqlstatus == 0)
			{
				anzahl --;
				ausgabe.fil_menge += ausgabe.menge;
				ausgabe.fil_umsatz += ausgabe.umsatz;
				ausgabe.fil_ertrag += ausgabe.ertrag;
				ausgabeges.fil_menge += ausgabe.menge;
				ausgabeges.fil_umsatz += ausgabe.umsatz;
				ausgabeges.fil_ertrag += ausgabe.ertrag;
				ausgabe.menge_we = 0.0;
				ausgabe.umsatz_we = 0.0;
				ausgabe.ertrag_we = 0.0;
		        sqlstatus = dbClass.sqlfetch (readcursor_ges);
			}
			ausgabe.sfil_ges_menge = ausgabe.fil_menge ;  //nur zum Summieren im LL
			ausgabe.sfil_ges_umsatz = ausgabe.fil_umsatz;   //nur zum Summieren im LL
			ausgabe.sfil_ges_ertrag = ausgabe.fil_ertrag ;  //nur zum Summieren im LL
//281009 Ende






			ausgabe.menge_we = 0.0;
			ausgabe.umsatz_we = 0.0;
			ausgabe.ertrag_we = 0.0;
			sqlstatus = dbClass.sqlopen (readcursor);
	        sqlstatus = dbClass.sqlfetch (readcursor);
//			ausgabe.menge += ausgabe.menge_we;
//			ausgabe.umsatz += ausgabe.umsatz_we;
//			ausgabe.ertrag += ausgabe.ertrag_we;
			if (strcmp(R_menge,"J")== 0 && ausgabe.fil_menge != 0.0 && ausgabe.auswahl_menge != 0.0)
			{
				ausgabe.ProzGesamt = ausgabe.menge / ausgabe.fil_menge * 100;
				ausgabe.ProzAuswahl = ausgabe.menge / ausgabe.auswahl_menge * 100;
			} 
			else if (strcmp(R_umsatz,"J")== 0 && ausgabe.fil_umsatz != 0.0 && ausgabe.auswahl_menge != 0.0 ) 
			{
				ausgabe.ProzGesamt = ausgabe.umsatz / ausgabe.fil_umsatz * 100;
				ausgabe.ProzAuswahl = ausgabe.umsatz / ausgabe.auswahl_umsatz * 100;
			}
			else if (strcmp(R_ertrag,"J")== 0 && ausgabe.fil_menge != 0.0 && ausgabe.auswahl_menge != 0.0 ) 
			{
				ausgabe.ProzGesamt = ausgabe.ertrag / ausgabe.fil_ertrag * 100;
				ausgabe.ProzAuswahl = ausgabe.ertrag / ausgabe.auswahl_ertrag * 100;
			}


			if (sqlstatus == 100) ausgabe.fil_anzahl = 0;
			if (sqlstatus == 0) ausgabe.fil_anzahl --;
			return fehlercode;
		  }
		  else
		  {
			flg_ges_holen = TRUE;
			return leseGes();;
		  }
	  }
	  else //(ausgabe.fil_anzahl)
	  {
				ausgabe.menge_we = 0.0;
				ausgabe.umsatz_we = 0.0;
				ausgabe.ertrag_we = 0.0;
	        sqlstatus = dbClass.sqlfetch (readcursor);
//			ausgabe.menge += ausgabe.menge_we;
//			ausgabe.umsatz += ausgabe.umsatz_we;
//			ausgabe.ertrag += ausgabe.ertrag_we;
			if (sqlstatus == 0) ausgabe.fil_anzahl --;
			if (sqlstatus == 100) 
			{
				ausgabe.fil_anzahl = 0;
				return fehlercode;
			}
			if (strcmp(R_menge,"J")== 0 && ausgabe.fil_menge != 0.0 && ausgabe.auswahl_menge != 0.0)
			{
				ausgabe.ProzGesamt = ausgabe.menge / ausgabe.fil_menge * 100;
				ausgabe.ProzAuswahl = ausgabe.menge / ausgabe.auswahl_menge * 100;
			} 
			else if (strcmp(R_umsatz,"J")== 0 && ausgabe.fil_umsatz != 0.0 && ausgabe.auswahl_menge != 0.0 ) 
			{
				ausgabe.ProzGesamt = ausgabe.umsatz / ausgabe.fil_umsatz * 100;
				ausgabe.ProzAuswahl = ausgabe.umsatz / ausgabe.auswahl_umsatz * 100;
			}
			else if (strcmp(R_ertrag,"J")== 0 && ausgabe.fil_menge != 0.0 && ausgabe.auswahl_menge != 0.0 ) 
			{
				ausgabe.ProzGesamt = ausgabe.ertrag / ausgabe.fil_ertrag * 100;
				ausgabe.ProzAuswahl = ausgabe.ertrag / ausgabe.auswahl_ertrag * 100;
			}

	  }



      return fehlercode;
}

int A_MO_WA_CLASS::openDS (void)
{
         return dbClass.sqlopen (readcursor);
}

void A_MO_WA_CLASS::prepare (void)
{
     char *sqltext1;
     char *sqltext2;

    dbRange.RangeIn ("a_tag_wa.fil",filbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_tag_wa.kun",kunbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_bas.wg",wgbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_tag_wa.a",abereich, SQLLONG, 0); 
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 11);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 11);


	dbClass.sqlout ((char *) ausgabe.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((long *) &ausgabe.vertr, SQLLONG, 2);
	dbClass.sqlout ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &ausgabe.fil_anzahl, SQLSHORT, 0);   // Anzahl s�tze f�r eine Filiale
	dbClass.sqlout ((double *) &ausgabe.fil_menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_ertrag, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_menge_we, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_umsatz_we, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_ertrag_we, SQLDOUBLE, 0);

    sqltext1 = dbRange.SetRange ("select  "
							"case when a_tag_wa.fil > 0 then fil.fil_kla "
							"	when a_tag_wa.fil = 0 then \"\" end klasse , "
							"case when a_tag_wa.kun > 0 then kun.vertr1 "
							"	when a_tag_wa.kun = 0 then 0 end vertr , "

							"case when a_tag_wa.fil = 0 then a_tag_wa.kun "
							"     when a_tag_wa.fil > 0 then a_tag_wa.fil end kunde, "
							"case when a_tag_wa.fil = 0 then 0 "
							"     when a_tag_wa.fil > 0 then 1 end kun_fil, "
							"count(unique a_tag_wa.a), "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag)  end ) menge, "
							"sum(a_tag_wa.ums_vk_tag+ a_tag_wa.ums_vk_sa_tag- a_tag_wa.ums_ret_tag)  umsatz, "
							"sum( "
							"	case when a_tag_wa.fil > 0 then (a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag) - ((a_tag_wa.ums_fil_ek_e + a_tag_wa.ums_fil_ek_sa_e - a_tag_wa.ums_fil_ek_sa_e)) "
							"	     when a_tag_wa.fil = 0 then (a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag) - ((a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_tag_wa.sk_vollk) "
							"	end ) ertrag, "

							"sum(case when a_bas.me_einh <> 2 then (a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag)  end ) menge_we, "
							"sum(a_tag_we.ums_fil_vk_tag / ptabn.ptwer2 + a_tag_we.ums_fil_vk_sa_tag / ptabn.ptwer2 - a_tag_we.ums_ret_tag / ptabn.ptwer2)  umsatz_we, "
							"sum( "
							"	 (a_tag_we.ums_fil_vk_tag / ptabn.ptwer2 + a_tag_we.ums_fil_vk_sa_tag / ptabn.ptwer2 - a_tag_we.ums_ret_tag / ptabn.ptwer2) - ((a_tag_we.ums_fil_ek_tag + a_tag_we.ums_fil_ek_sa_tag - a_tag_we.ums_ret_fil_ek_tag)) "
							"	 ) ertrag_we "

							"from a_tag_wa,a_bas , outer a_tag_we, outer ptabn, outer fil, outer kun "
							"where a_tag_wa.a = a_bas.a "
							"and a_tag_wa.fil = fil.fil "
							"and a_tag_wa.kun = kun.kun "
							"and a_tag_wa.mdn = ? "
							"and (<a_tag_wa.fil> "
							"     or <a_tag_wa.kun> ) "
							"and a_tag_wa.dat between ? and ? "
							"and <a_bas.wg> "
							"and <a_tag_wa.a> "
//							"and (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag + a_tag_wa.me_ret_tag) <> 0.0 "

							"and a_tag_wa.mdn = a_tag_we.mdn "
							"and a_tag_wa.fil = a_tag_we.fil "
							"and a_tag_wa.a = a_tag_we.a "
							"and a_tag_wa.dat = a_tag_we.dat "
							"and ptabn.ptitem = \"mwst\" "
							"and ptabn.ptlfnr = a_bas.mwst "

							"group by 1,2,3,4 order by klasse,vertr,kunde ");






           count_cursor = dbClass.sqlcursor (sqltext1);

    dbRange.RangeIn ("a_bas.wg",wgbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_tag_wa.a",abereich, SQLLONG, 0); 
    dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 11);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 11);


	/*********
	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) ausgabe.a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((double *) &ausgabe.menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ertrag, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.menge_we, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.umsatz_we, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ertrag_we, SQLDOUBLE, 0);

    sqltext2 = dbRange.SetRange ("select  "
							"a_tag_wa.a, a_bas.a_bz1, "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag)  end ) menge, "
							"sum(a_tag_wa.ums_vk_tag+ a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag)  umsatz, "
							"sum( "
							"	case when a_tag_wa.fil > 0 then (a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag) - ((a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_tag_wa.pr_fil_ek) "
							"	     when a_tag_wa.fil = 0 then (a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag) - ((a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_tag_wa.sk_vollk) "
							"	end ) ertrag, "

							"sum(case when a_bas.me_einh <> 2 then (a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag)  end ) menge_we, "
							"sum(a_tag_we.ums_fil_vk_tag / ptabn.ptwer2 + a_tag_we.ums_fil_vk_sa_tag / ptabn.ptwer2 - a_tag_we.ums_ret_tag / ptabn.ptwer2)  umsatz_we, "
							"sum( "
							"	 (a_tag_we.ums_fil_vk_tag + a_tag_we.ums_fil_vk_sa_tag - a_tag_we.ums_ret_tag) - ((a_tag_we.ums_fil_ek_tag + a_tag_we.ums_fil_ek_sa_tag - a_tag_we.ums_ret_fil_ek_tag) ) "
							"	 ) ertrag_we "

							"from a_tag_wa,a_bas , outer a_tag_we, outer ptabn, outer fil, outer kun "
							"where a_tag_wa.a = a_bas.a "
							"and a_tag_wa.fil = fil.fil "
							"and a_tag_wa.kun = kun.kun "
							"and a_tag_wa.mdn = ? "
							"and a_tag_wa.fil = ? "
							"and a_tag_wa.kun = ? "
							"and a_tag_wa.dat between ? and ? "
							"and <a_bas.wg> "
							"and <a_tag_wa.a> "
//							"and (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag + a_tag_wa.me_ret_tag) <> 0.0 "
							"and a_tag_wa.mdn = a_tag_we.mdn "
							"and a_tag_wa.fil = a_tag_we.fil "
							"and a_tag_wa.a = a_tag_we.a "
							"and a_tag_wa.dat = a_tag_we.dat "
							"and ptabn.ptitem = \"mwst\" "
							"and ptabn.ptlfnr = a_bas.mwst "
							"group by 1,2 order by                                               ");
*********/
	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
//	dbClass.sqlout ((double *) &we_a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) ausgabe.a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((double *) &ausgabe.menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ertrag, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.menge_we, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.umsatz_we, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ertrag_we, SQLDOUBLE, 0);

    sqltext2 = dbRange.SetRange ("select  "
							"a_tag_wa.a, a_bas.a_bz1, "
							"sum (case when a_bas.me_einh <> 2 then a_tag_wa.me_vk_tag * a_bas.a_gew else a_tag_wa.me_vk_tag end) + "
							"sum (case when a_bas.me_einh <> 2 then a_tag_wa.me_vk_sa_tag * a_bas.a_gew else a_tag_wa.me_vk_sa_tag end) - " 
							"sum (case when a_bas.me_einh <> 2 then a_tag_wa.me_ret_tag * a_bas.a_gew else a_tag_wa.me_ret_tag end) + "
							"sum (case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then a_tag_we.me_ek_tag * a_bas.a_gew else a_tag_we.me_ek_tag end else 0 end) + "
							"sum (case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then a_tag_we.me_ek_sa_tag * a_bas.a_gew else a_tag_we.me_ek_sa_tag end else 0 end) -  "
							"sum (case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then a_tag_we.me_ret_tag * a_bas.a_gew else a_tag_we.me_ret_tag end else 0 end)  "
							"menge, "

							"sum (a_tag_wa.ums_vk_tag) + "
							"sum (a_tag_wa.ums_vk_sa_tag) - "
							"sum (a_tag_wa.ums_ret_tag) + "
							"sum (case when a_tag_we.a > 0 then a_tag_we.ums_fil_vk_tag / ptabn.ptwer2 else 0 end) + "
							"sum (case when a_tag_we.a > 0 then a_tag_we.ums_fil_vk_sa_tag / ptabn.ptwer2 else 0 end) - " 
							"sum (case when a_tag_we.a > 0 then a_tag_we.ums_ret_tag / ptabn.ptwer2 else 0 end) "
							"umsatz, "
					
							"sum( "
							"	case when a_tag_wa.fil > 0 then (a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag) - ((a_tag_wa.ums_fil_ek_e + a_tag_wa.ums_fil_ek_sa_e - a_tag_wa.ums_fil_ek_ret_e) ) "
							"	     when a_tag_wa.fil = 0 then (a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag) - ((a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_tag_wa.sk_vollk) "
							"	end ) "
							" + "
							"sum( case when a_tag_we.a > 0 then "
							"	 (a_tag_we.ums_fil_vk_tag + a_tag_we.ums_fil_vk_sa_tag - a_tag_we.ums_ret_tag) - ((a_tag_we.ums_fil_ek_tag + a_tag_we.ums_fil_ek_sa_tag - a_tag_we.ums_ret_fil_ek_tag) ) "
							"	else 0 end ) "
							"ertrag, "

							"sum(case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then (a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag)  end  else 0 end) menge_we, "

							"sum(case when a_tag_we.a > 0 then a_tag_we.ums_fil_vk_tag / ptabn.ptwer2 + a_tag_we.ums_fil_vk_sa_tag / ptabn.ptwer2 - a_tag_we.ums_ret_tag / ptabn.ptwer2 else 0 end)  umsatz_we, "

							"sum( case when a_tag_we.a > 0 then "
							"	 (a_tag_we.ums_fil_vk_tag + a_tag_we.ums_fil_vk_sa_tag - a_tag_we.ums_ret_tag) - ((a_tag_we.ums_fil_ek_tag + a_tag_we.ums_fil_ek_sa_tag - a_tag_we.ums_ret_fil_ek_tag) ) "
							"	else 0 end ) ertrag_we "

							"from a_tag_wa,a_bas , outer a_tag_we, outer ptabn, outer fil, outer kun "
							"where a_tag_wa.a = a_bas.a "
							"and a_tag_wa.fil = fil.fil "
							"and a_tag_wa.kun = kun.kun "
							"and a_tag_wa.mdn = ? "
							"and a_tag_wa.fil = ? "
							"and a_tag_wa.kun = ? "
							"and a_tag_wa.dat between ? and ? "
							"and <a_bas.wg> "
							"and <a_tag_wa.a> "
//							"and (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag + a_tag_wa.me_ret_tag) <> 0.0 "
							"and a_tag_wa.mdn = a_tag_we.mdn "
							"and a_tag_wa.fil = a_tag_we.fil "
							"and a_tag_wa.a = a_tag_we.a "
							"and a_tag_wa.dat = a_tag_we.dat "
							"and ptabn.ptitem = \"mwst\" "
							"and ptabn.ptlfnr = a_bas.mwst "
							"group by 1,2 order by                                                                                                         ");

	clipped(sqltext2);
strcat (sqltext2,order_by);
strcat (sqltext2,order_by_zusatz);

           readcursor = dbClass.sqlcursor (sqltext2);



    dbRange.RangeIn ("a_tag_wa.fil",filbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_tag_wa.kun",kunbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_bas.wg",wgbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_tag_wa.a",abereich, SQLLONG, 0); 
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 11);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 11);

	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
//	dbClass.sqlout ((double *) &we_a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) ausgabe.a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((double *) &ausgabeges.menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabeges.umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabeges.ertrag, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabeges.menge_we, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabeges.umsatz_we, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabeges.ertrag_we, SQLDOUBLE, 0);

	/***
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag + "
																"a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag + "
														"a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag)  end ) "
							"else "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag)  end ) "
							"end "
							"menge, "
*******/


    sqltext2 = dbRange.SetRange ("select  "
							"a_tag_wa.a, a_bas.a_bz1, "
							"sum (case when a_bas.me_einh <> 2 then a_tag_wa.me_vk_tag * a_bas.a_gew else a_tag_wa.me_vk_tag end) + "
							"sum (case when a_bas.me_einh <> 2 then a_tag_wa.me_vk_sa_tag * a_bas.a_gew else a_tag_wa.me_vk_sa_tag end) - " 
							"sum (case when a_bas.me_einh <> 2 then a_tag_wa.me_ret_tag * a_bas.a_gew else a_tag_wa.me_ret_tag end) + "
							"sum (case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then a_tag_we.me_ek_tag * a_bas.a_gew else a_tag_we.me_ek_tag end else 0 end) + "
							"sum (case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then a_tag_we.me_ek_sa_tag * a_bas.a_gew else a_tag_we.me_ek_sa_tag end else 0 end) -  "
							"sum (case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then a_tag_we.me_ret_tag * a_bas.a_gew else a_tag_we.me_ret_tag end else 0 end)  "
							"menge, "

							"sum (a_tag_wa.ums_vk_tag) + "
							"sum (a_tag_wa.ums_vk_sa_tag) - "
							"sum (a_tag_wa.ums_ret_tag) + "
							"sum (case when a_tag_we.a > 0 then a_tag_we.ums_fil_vk_tag / ptabn.ptwer2 else 0 end) + "
							"sum (case when a_tag_we.a > 0 then a_tag_we.ums_fil_vk_sa_tag / ptabn.ptwer2 else 0 end) - " 
							"sum (case when a_tag_we.a > 0 then a_tag_we.ums_ret_tag / ptabn.ptwer2 else 0 end) "
							"umsatz, "
					
							"sum( "
							"	case when a_tag_wa.fil > 0 then (a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag) - ((a_tag_wa.ums_fil_ek_e + a_tag_wa.ums_fil_ek_sa_e - a_tag_wa.ums_fil_ek_ret_e) ) "
							"	     when a_tag_wa.fil = 0 then (a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag) - ((a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_tag_wa.sk_vollk) "
							"	end ) "
							" + "
							"sum( case when a_tag_we.a > 0 then "
							"	 (a_tag_we.ums_fil_vk_tag + a_tag_we.ums_fil_vk_sa_tag - a_tag_we.ums_ret_tag) - ((a_tag_we.ums_fil_ek_tag + a_tag_we.ums_fil_ek_sa_tag - a_tag_we.ums_ret_fil_ek_tag) ) "
							"	else 0 end ) "
							"ertrag, "

							"sum(case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then (a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag)  end  else 0 end) menge_we, "

							"sum(case when a_tag_we.a > 0 then a_tag_we.ums_fil_vk_tag / ptabn.ptwer2 + a_tag_we.ums_fil_vk_sa_tag / ptabn.ptwer2 - a_tag_we.ums_ret_tag / ptabn.ptwer2 else 0 end)  umsatz_we, "

							"sum( case when a_tag_we.a > 0 then "
							"	 (a_tag_we.ums_fil_vk_tag + a_tag_we.ums_fil_vk_sa_tag - a_tag_we.ums_ret_tag) - ((a_tag_we.ums_fil_ek_tag + a_tag_we.ums_fil_ek_sa_tag - a_tag_we.ums_ret_fil_ek_tag) ) "
							"	else 0 end ) ertrag_we "

							"from a_tag_wa,a_bas , outer a_tag_we, outer ptabn, outer fil, outer kun "
							"where a_tag_wa.a = a_bas.a "
							"and a_tag_wa.fil = fil.fil "
							"and a_tag_wa.kun = kun.kun "
							"and a_tag_wa.mdn = ? "
							"and (<a_tag_wa.fil> "
							"     or <a_tag_wa.kun> ) "
							"and a_tag_wa.dat between ? and ? "
							"and <a_bas.wg> "
							"and <a_tag_wa.a> "

							"and a_tag_wa.mdn = a_tag_we.mdn "
							"and a_tag_wa.fil = a_tag_we.fil "
							"and a_tag_wa.a = a_tag_we.a "
							"and a_tag_wa.dat = a_tag_we.dat "
							"and ptabn.ptitem = \"mwst\" "
							"and ptabn.ptlfnr = a_bas.mwst "
							"group by 1,2 order by                                                                                                         ");

	clipped(sqltext2);
strcat (sqltext2,order_by);
strcat (sqltext2,order_by_zusatz);

           readcursor_gesamt = dbClass.sqlcursor (sqltext2);









    dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 11);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 11);


	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
//	dbClass.sqlout ((double *) &we_a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) ausgabe.a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((double *) &ausgabe.menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ertrag, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.menge_we, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.umsatz_we, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ertrag_we, SQLDOUBLE, 0);

    sqltext2 = dbRange.SetRange ("select  "
							"a_tag_wa.a, a_bas.a_bz1, "
							"sum (case when a_bas.me_einh <> 2 then a_tag_wa.me_vk_tag * a_bas.a_gew else a_tag_wa.me_vk_tag end) + "
							"sum (case when a_bas.me_einh <> 2 then a_tag_wa.me_vk_sa_tag * a_bas.a_gew else a_tag_wa.me_vk_sa_tag end) - " 
							"sum (case when a_bas.me_einh <> 2 then a_tag_wa.me_ret_tag * a_bas.a_gew else a_tag_wa.me_ret_tag end) + "
							"sum (case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then a_tag_we.me_ek_tag * a_bas.a_gew else a_tag_we.me_ek_tag end else 0 end) + "
							"sum (case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then a_tag_we.me_ek_sa_tag * a_bas.a_gew else a_tag_we.me_ek_sa_tag end else 0 end) -  "
							"sum (case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then a_tag_we.me_ret_tag * a_bas.a_gew else a_tag_we.me_ret_tag end else 0 end)  "
							"menge, "

							"sum (a_tag_wa.ums_vk_tag) + "
							"sum (a_tag_wa.ums_vk_sa_tag) - "
							"sum (a_tag_wa.ums_ret_tag) + "
							"sum (case when a_tag_we.a > 0 then a_tag_we.ums_fil_vk_tag / ptabn.ptwer2 else 0 end) + "
							"sum (case when a_tag_we.a > 0 then a_tag_we.ums_fil_vk_sa_tag / ptabn.ptwer2 else 0 end) - " 
							"sum (case when a_tag_we.a > 0 then a_tag_we.ums_ret_tag / ptabn.ptwer2 else 0 end) "
							"umsatz, "
					
							"sum( "
							"	case when a_tag_wa.fil > 0 then (a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag) - ((a_tag_wa.ums_fil_ek_e + a_tag_wa.ums_fil_ek_sa_e - a_tag_wa.ums_fil_ek_ret_e) ) "
							"	     when a_tag_wa.fil = 0 then (a_tag_wa.ums_vk_tag + a_tag_wa.ums_vk_sa_tag - a_tag_wa.ums_ret_tag) - ((a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_tag_wa.sk_vollk) "
							"	end ) "
							" + "
							"sum( case when a_tag_we.a > 0 then "
							"	 (a_tag_we.ums_fil_vk_tag + a_tag_we.ums_fil_vk_sa_tag - a_tag_we.ums_ret_tag) - ((a_tag_we.ums_fil_ek_tag + a_tag_we.ums_fil_ek_sa_tag - a_tag_we.ums_ret_fil_ek_tag) ) "
							"	else 0 end ) "
							"ertrag, "

							"sum(case when a_tag_we.a > 0 then case when a_bas.me_einh <> 2 then (a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_we.me_ek_tag + a_tag_we.me_ek_sa_tag - a_tag_we.me_ret_tag)  end  else 0 end) menge_we, "

							"sum(case when a_tag_we.a > 0 then a_tag_we.ums_fil_vk_tag / ptabn.ptwer2 + a_tag_we.ums_fil_vk_sa_tag / ptabn.ptwer2 - a_tag_we.ums_ret_tag / ptabn.ptwer2 else 0 end)  umsatz_we, "

							"sum( case when a_tag_we.a > 0 then "
							"	 (a_tag_we.ums_fil_vk_tag + a_tag_we.ums_fil_vk_sa_tag - a_tag_we.ums_ret_tag) - ((a_tag_we.ums_fil_ek_tag + a_tag_we.ums_fil_ek_sa_tag - a_tag_we.ums_ret_fil_ek_tag) ) "
							"	else 0 end ) ertrag_we "

							"from a_tag_wa,a_bas , outer a_tag_we, outer ptabn, outer fil, outer kun "
							"where a_tag_wa.a = a_bas.a "
							"and a_tag_wa.fil = fil.fil "
							"and a_tag_wa.kun = kun.kun "
							"and a_tag_wa.mdn = ? "
							"and a_tag_wa.fil = ? "
							"and a_tag_wa.kun = ? "
							"and a_tag_wa.dat between ? and ? "
							"and a_tag_wa.mdn = a_tag_we.mdn "
							"and a_tag_wa.fil = a_tag_we.fil "
							"and a_tag_wa.a = a_tag_we.a "
							"and a_tag_wa.dat = a_tag_we.dat "
							"and ptabn.ptitem = \"mwst\" "
							"and ptabn.ptlfnr = a_bas.mwst "
							"group by 1,2 order by                                                                                                         ");

	clipped(sqltext2);
strcat (sqltext2,order_by);
strcat (sqltext2,order_by_zusatz);

           readcursor_ges = dbClass.sqlcursor (sqltext2);



	test_upd_cursor = 1;


}


/******
void A_MO_WA_CLASS::prepare_ret (void)   //nur Retouren 
{
     char *sqltext1;
     char *sqltext2;

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &kunvon, SQLLONG, 0);
	dbClass.sqlin ((short *) &kunbis, SQLLONG, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 11);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 11);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &abis, SQLDOUBLE, 0);


	dbClass.sqlout ((char *) ausgabe.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((long *) &ausgabe.vertr, SQLLONG, 2);
	dbClass.sqlout ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &ausgabe.fil_anzahl, SQLSHORT, 0);   // Anzahl s�tze f�r eine Filiale
	dbClass.sqlout ((double *) &ausgabe.fil_menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.fil_ertrag, SQLDOUBLE, 0);

    sqltext1 = "select  "
							"case when a_tag_wa.fil > 0 then fil.fil_kla "
							"	when a_tag_wa.fil = 0 then \"\" end klasse , "
							"case when a_tag_wa.kun > 0 then kun.vertr1 "
							"	when a_tag_wa.kun = 0 then 0 end vertr , "

							"case when a_tag_wa.fil = 0 then a_tag_wa.kun "
							"     when a_tag_wa.fil > 0 then a_tag_wa.fil end kunde, "
							"case when a_tag_wa.fil = 0 then 0 "
							"     when a_tag_wa.fil > 0 then 1 end kun_fil, "
							"count(unique a_tag_wa.a), "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_ret_tag * -1) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_ret_tag * -1)  end ) menge, "
							"sum(a_tag_wa.ums_ret_tag * -1)  umsatz, "
							"sum((a_tag_wa.ums_ret_tag * -1) - ((a_tag_wa.me_ret_tag * -1) * a_tag_wa.sk_vollk)) ertrag "
							"from a_tag_wa,a_bas , outer fil, outer kun "
							"where a_tag_wa.a = a_bas.a "
							"and a_tag_wa.fil = fil.fil "
							"and a_tag_wa.kun = kun.kun "
							"and a_tag_wa.mdn = ? "
							"and (a_tag_wa.fil between ? and ? "
							"     or a_tag_wa.kun between ? and ?) "
							"and a_tag_wa.dat between ? and ? "
							"and a_bas.wg between ? and ? "
							"and a_tag_wa.a between ? and ? "
							"and (a_tag_wa.me_ret_tag) <> 0.0 "
							"group by 1,2,3,4 order by klasse,vertr,kunde ";






           count_cursor = dbClass.sqlcursor (sqltext1);

    dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((char *) datvon, SQLCHAR, 11);
	dbClass.sqlin ((char *) datbis, SQLCHAR, 11);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &abis, SQLDOUBLE, 0);


	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) ausgabe.a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((double *) &ausgabe.menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ertrag, SQLDOUBLE, 0);

    sqltext2 = "select  "
							"a_tag_wa.a, a_bas.a_bz1, "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_ret_tag * -1) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_ret_tag * -1)  end ) menge, "
							"sum(a_tag_wa.ums_ret_tag * -1)  umsatz, "
							"sum((a_tag_wa.ums_ret_tag * -1) - ((a_tag_wa.me_ret_tag * -1) * a_tag_wa.sk_vollk)) ertrag "
							"from a_tag_wa,a_bas , outer fil, outer kun "
							"where a_tag_wa.a = a_bas.a "
							"and a_tag_wa.fil = fil.fil "
							"and a_tag_wa.kun = kun.kun "
							"and a_tag_wa.mdn = ? "
							"and a_tag_wa.fil = ? "
							"and a_tag_wa.kun = ? "
							"and a_tag_wa.dat between ? and ? "
							"and a_bas.wg between ? and ? "
							"and a_tag_wa.a between ? and ? "
							"and (a_tag_wa.me_ret_tag) <> 0.0 "
							"group by 1,2 order by                                               ";

	clipped(sqltext2);
strcat (sqltext2,order_by);
strcat (sqltext2,order_by_zusatz);

           readcursor = dbClass.sqlcursor (sqltext2);



	test_upd_cursor = 1;

}
****/

