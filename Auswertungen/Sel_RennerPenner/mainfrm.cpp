//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
// Diese Applikation z.Z. nur f�r "Renner - Penner Liste Nach Menge, Umsatz und Ertrag Filiale und Kunden  SE.TEC   02.2009)
// ( f. Fa. Hausner   ausgelesen wird a_tag_wa 
// Erstellung : 13.12.2008  LuD

//=============================================================================

// mainfrm.cpp : implementation of the CMainFrame class

#include "stdafx.h"
#include "llmfc.h"
#include "mainfrm.h"
#include "wmask.h"
#include "DbClass.h"
#include "a_mo_wa.h"
#include "form_feld.h"
#include "systables.h"
#include "syscolumns.h"
#include "Token.h"
#include "strfkt.h"
#include "datum.h"
#include "DialogDatum.h"

#include "cmbtl9.h"
//#include "cmbtll11.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GR: Waehlen Sie hier die List & Label Sprache f�r den Designer und die
//     Dialoge aus :
//    (Die entsprechenden Sprachkonstanten entnehmen Sie der Datei cmbtl9.h)

// US: choose your LuL-Language for all designer, dialogs, etc... here:
//    (see cmbtl9.h for other language-constants)

const int LUL_LANGUAGE = CMBTLANG_GERMAN;

//LuD...
//const char *FILENAME_DEFAULT = "c:\\temp\\rezeptur.lst";  // Zeiger auf eine Konstante
const char *FILENAME_DEFAULT = "c:\\user\\fit\\format\\LL\\Produktion\\rezaufl.lst                               ";  // Zeiger auf eine Konstante
bool cfgOK;
char Listenname[128];
int Listenauswahl = 0;   // wenn 1 : Auswahl mehrerer Reports
int Hauptmenue = 0;   // wenn 1 : Auswahl edit oder print �ber Menue
int Divisor = 1;


char R_renner[2] = {"J"};
char R_penner[2] = {"N"};
char R_menge[2] = {"J"};
char R_umsatz[2] = {"N"};
char R_ertrag[2] = {"N"};
char order_by_zusatz[7] = {" desc "};
char order_by[11] = {"  menge   "};
char BezRennerPenner [] = {"Renner"};
char BezSortierung [] = {"nach Kilo     "};
char datvon[12] ;
char datbis[12] ;
char Format [30];

extern short mdn ;
extern char kunbereich[256];
extern char filbereich[256];
extern char wgbereich[256];
extern char abereich[256];
extern char adr_krz[25];
extern short a_bas_me_einh;
extern short a_bas_lief_einh;
extern double a_bas_a_gew;
extern char artikelbezeichnung[26];
extern char a_bz1[25];
extern char einh_bz[9];
long idat = 0;



  DB_CLASS dbClass;
  A_MO_WA_CLASS AMoWa;
  FORM_FELD form_feld;
  SYSCOLUMNS syscolumns;
  SYSTABLES systables;

HWND hMainWindow;

int form_feld_curs, tabid_curs, coltyp_curs,  dynacurs ; 
/////////////////////////////////////////////////////////////////////////////
// CMainFrame

// GR: Registrieren eines Nachrichtenkanals f�r List & Label Callbacks
// US: register LuL MessageBase:
UINT CMainFrame::m_uLuLMessageBase = RegisterWindowMessage("cmbtLLMessage");

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_START_DEBUG, OnStartDebug)
	ON_COMMAND(ID_FILE_STOP_DEBUG, OnStopDebug)
	ON_COMMAND(ID_EDIT_LABEL, OnEditLabel)
	ON_COMMAND(ID_EDIT_LIST, OnEditList)
	ON_UPDATE_COMMAND_UI(ID_FILE_START_DEBUG, OnUpdateStartDebug)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP_DEBUG, OnUpdateStopDebug)
	ON_COMMAND(ID_PRINT_LABEL, OnPrintLabel)
	ON_COMMAND(ID_PRINT_REPORT, OnPrintReport)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(m_uLuLMessageBase,OnLulMessage)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_bDebug = FALSE;
    ProgCfg = new PROG_CFG ("sel_RennerPenner");

}

CMainFrame::~CMainFrame()
{
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }

}


/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	
	LlAddCtlSupport(m_hWnd,
					LL_CTL_ADDTOSYSMENU | LL_CTL_CONVERTCONTROLS | LL_CTL_ALSOCHILDREN,
					"combit.ini");
	sprintf(Format,"%s","-------&");
	GetCfgValues();
	
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_PRINT_REPORT,0l);
	}
		
return 0;
}

void CMainFrame::OnStartDebug()
{
	MessageBox("Make sure that DEBWIN2 had been started before this demo application. "
				"If this doesn't happen you won't see any debug outputs now!",
				"List & Label Sample App", MB_OK | MB_ICONINFORMATION);
//	LlSetDebug(LL_DEBUG_CMBTLL);
	m_bDebug = TRUE;

}

void CMainFrame::OnStopDebug()
{
	LlSetDebug(FALSE);
	m_bDebug = FALSE;
}

void CMainFrame::OnUpdateStopDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bDebug);
}

void CMainFrame::OnUpdateStartDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bDebug);
}


//=============================================================================
void CMainFrame::OnEditLabel()
//=============================================================================
{
	CHAR szFilename[128+1] = "*.lbl";
	HWND hWnd = m_hWnd;
	HJOB hJob;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	// US: initialize Job
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

	// GR: Setzen der List & Label Optionen
	// US: Setting the List & Label options
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	//GR: Exporter aktivieren
	//US: Enable exporter
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");     

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
/* ---> GrJ GrJ
	if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
    {
        LlJobClose(hJob);
        return;
    }
< ---- */

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	// GR: Aufruf des Designers

   	sprintf ( szFilename, FILENAME_DEFAULT );

 	if (LlDefineLayout(hJob, hWnd, "Designer", LL_PROJECT_LABEL, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
}

long holetyp(char* tab_nam , char* feld_nam)
{
	static char alttabnam[20];
	static long alttabid ;
	if ( strncmp ( tab_nam, alttabnam, strlen(tab_nam)) == 0 )
	{
		// bereits gefunden
	}
	else
	{
		sprintf ( systables.tabname, "%s" , tab_nam );
		dbClass.sqlopen (tabid_curs);	// destroyed den Cursor, sqlopen refreshed
		if (!dbClass.sqlfetch(tabid_curs))	// gefunden
		{
			alttabid = systables.tabid ;
			sprintf ( alttabnam, "%s", tab_nam );
		}
		else

			return LL_TEXT ;

	}
 	sprintf ( syscolumns.colname, "%s" , feld_nam );
 	syscolumns.tabid = alttabid ;
	dbClass.sqlopen (coltyp_curs);	// destroyed den Cursor, sqlopen refreshed
	if (!dbClass.sqlfetch(coltyp_curs))	// gefunden
	{
		switch (syscolumns.coltype)
		{
		case(iDBCHAR):
			return LL_TEXT ;
		case(iDBSMALLINT):	// smallint
		case(iDBINTEGER):	// integer
		case(iDBDECIMAL):	// decimal
			return LL_NUMERIC ;
		case(iDBDATUM):	// Datumsfeld
			return LL_DATE ;
		}
		return LL_TEXT ;
	}
	else
		return LL_TEXT ;
		
}


int GetRecCount()
{

	return AMoWa.dbcount ();

}



void Variablendefinition (HJOB hJob)
{
		LlDefineVariableExt(hJob, "Mandant",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "FilBereich",        "1-9999", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "ABereich",        "1-9999999999999", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "WGBereich",        "1-9999999999999", LL_TEXT, NULL);
	    LlDefineVariableExt(hJob, "datum.von", "01.01.2008", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "datum.bis", "31.12.2009", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Divisor",        "1", LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "Format",        "-------&", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "BezRennerPenner",        "Renner", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "BezSortierung",        "nach Kilo", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "SortMenge",        "J", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "SortUmsatz",        "N", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "SortErtrag",        "N", LL_TEXT, NULL);
}

void VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{
		sprintf(szTemp2, "%d", Divisor );
		LlDefineVariableExt(hJob, "Divisor",        szTemp2, LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "Format",        Format, LL_TEXT, NULL);

		sprintf(szTemp2, "%d", mdn );
		LlDefineVariableExt(hJob, "Mandant",        szTemp2, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "datum.von",        datvon, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "datum.bis",        datbis, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "artikel.bis",        szTemp2, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "BezRennerPenner",        BezRennerPenner, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "BezSortierung",        BezSortierung, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "SortMenge",        R_menge, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "SortUmsatz",        R_umsatz, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "SortErtrag",        R_ertrag, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "FilBereich",        kunbereich, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "WGBereich",        wgbereich, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "ABereich",        abereich, LL_TEXT, NULL);

}

void Felderdefinition (HJOB hJob)
{
	LlDefineFieldExt(hJob, "ausgabe.kun",        "4711", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.fil",        "33", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.kun_fil",        "0", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.adr_krz",        "Testkunde", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "ausgabe.a",        "22", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "ausgabe.a_bz1", "Artikel-Bezeichnung", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.fil.menge",        "200.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.fil.umsatz",        "4000.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.fil.ertrag",        "123.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.menge",        "20.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.umsatz",        "400.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.ertrag",        "12.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.auswahl.menge",        "100.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.auswahl.umsatz",        "3000.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.auswahl.ertrag",        "113.2", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ausgabe.ProzGesamt",        "20", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ausgabe.ProzAuswahl",        "30", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.fil_kla",        "A", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.ges_menge",        "20.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.ges_umsatz",        "400.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.ges_ertrag",        "12.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.fil_ges_umsatz",        "400.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.fil_ges_menge",        "412.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.fil_ges_ertrag",        "123.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.sfil_ges_umsatz",        "400.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.sfil_ges_menge",        "412.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabe.sfil_ges_ertrag",        "123.2", LL_NUMERIC, NULL);


	LlDefineFieldExt(hJob, "ausgabeges.fil.menge",        "200.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabeges.fil.umsatz",        "4000.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabeges.fil.ertrag",        "123.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabeges.menge",        "20.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabeges.umsatz",        "400.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabeges.ertrag",        "12.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabeges.auswahl.menge",        "100.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabeges.auswahl.umsatz",        "3000.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabeges.auswahl.ertrag",        "113.2", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ausgabeges.ProzGesamt",        "20", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ausgabeges.ProzAuswahl",        "30", LL_NUMERIC, NULL);

	LlDefineFieldExt(hJob, "ausgabeges.fil_ges_umsatz",        "400.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabeges.fil_ges_menge",        "412.2", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "ausgabeges.fil_ges_ertrag",        "123.2", LL_NUMERIC, NULL);
}

void FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{
		sprintf(szTemp2, "%ld", ausgabe.kun );
    LlDefineFieldExt(hJob, "ausgabe.kun",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%ld", ausgabe.fil );
    LlDefineFieldExt(hJob, "ausgabe.fil",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%hd", ausgabe.kun_fil );
    LlDefineFieldExt(hJob, "ausgabe.kun_fil",        szTemp2, LL_NUMERIC, NULL);

    LlDefineFieldExt(hJob, "ausgabe.adr_krz", adr_krz, LL_TEXT, NULL);

		sprintf(szTemp2, "%.2lf", ausgabe.fil_menge );
    LlDefineFieldExt(hJob, "ausgabe.fil.menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.fil_umsatz );
    LlDefineFieldExt(hJob, "ausgabe.fil.umsatz",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.fil_ertrag );
    LlDefineFieldExt(hJob, "ausgabe.fil.ertrag",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.menge );
    LlDefineFieldExt(hJob, "ausgabe.menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.umsatz );
    LlDefineFieldExt(hJob, "ausgabe.umsatz",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ertrag );
    LlDefineFieldExt(hJob, "ausgabe.ertrag",        szTemp2, LL_NUMERIC, NULL);

		sprintf(szTemp2, "%.2lf", ausgabe.auswahl_menge );
    LlDefineFieldExt(hJob, "ausgabe.auswahl.menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.auswahl_umsatz );
    LlDefineFieldExt(hJob, "ausgabe.auswahl.umsatz",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.auswahl_ertrag );
    LlDefineFieldExt(hJob, "ausgabe.auswahl.ertrag",        szTemp2, LL_NUMERIC, NULL);

		sprintf(szTemp2, "%.2lf", ausgabe.ProzGesamt );
    LlDefineFieldExt(hJob, "ausgabe.ProzGesamt",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ProzAuswahl );
    LlDefineFieldExt(hJob, "ausgabe.ProzAuswahl",        szTemp2, LL_NUMERIC, NULL);

		sprintf(szTemp2, "%.0lf", ausgabe.a );
    LlDefineFieldExt(hJob, "ausgabe.a",        szTemp2, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "ausgabe.a_bz1",        ausgabe.a_bz1, LL_TEXT, NULL);



	LlDefineFieldExt(hJob, "ausgabe.fil_kla",        ausgabe.fil_kla, LL_TEXT, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.fil_umsatz );
    LlDefineFieldExt(hJob, "ausgabe.fil_ges_umsatz",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.fil_menge );
    LlDefineFieldExt(hJob, "ausgabe.fil_ges_menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.fil_ertrag );
    LlDefineFieldExt(hJob, "ausgabe.fil_ges_ertrag",        szTemp2, LL_NUMERIC, NULL);

		sprintf(szTemp2, "%.2lf", ausgabe.sfil_ges_umsatz );
// 220212		ausgabe.sfil_ges_umsatz = 0.0; //damit nur einmal pro Filiale aufsummiert wird 
    LlDefineFieldExt(hJob, "ausgabe.sfil_ges_umsatz",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.sfil_ges_menge );
// 220212		ausgabe.sfil_ges_menge = 0.0; //damit nur einmal pro Filiale aufsummiert wird 
    LlDefineFieldExt(hJob, "ausgabe.sfil_ges_menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.sfil_ges_ertrag );
// 220212		ausgabe.sfil_ges_ertrag = 0.0; //damit nur einmal pro Filiale aufsummiert wird 
    LlDefineFieldExt(hJob, "ausgabe.sfil_ges_ertrag",        szTemp2, LL_NUMERIC, NULL);




		sprintf(szTemp2, "%.2lf", ausgabeges.fil_menge );
    LlDefineFieldExt(hJob, "ausgabeges.fil.menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabeges.fil_umsatz );
    LlDefineFieldExt(hJob, "ausgabeges.fil.umsatz",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabeges.fil_ertrag );
    LlDefineFieldExt(hJob, "ausgabeges.fil.ertrag",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabeges.menge );
    LlDefineFieldExt(hJob, "ausgabeges.menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabeges.umsatz );
    LlDefineFieldExt(hJob, "ausgabeges.umsatz",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabeges.ertrag );
    LlDefineFieldExt(hJob, "ausgabeges.ertrag",        szTemp2, LL_NUMERIC, NULL);

		sprintf(szTemp2, "%.2lf", ausgabeges.auswahl_menge );
    LlDefineFieldExt(hJob, "ausgabeges.auswahl.menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabeges.auswahl_umsatz );
    LlDefineFieldExt(hJob, "ausgabeges.auswahl.umsatz",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabeges.auswahl_ertrag );
    LlDefineFieldExt(hJob, "ausgabeges.auswahl.ertrag",        szTemp2, LL_NUMERIC, NULL);

		sprintf(szTemp2, "%.2lf", ausgabeges.ProzGesamt );
    LlDefineFieldExt(hJob, "ausgabeges.ProzGesamt",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabeges.ProzAuswahl );
    LlDefineFieldExt(hJob, "ausgabeges.ProzAuswahl",        szTemp2, LL_NUMERIC, NULL);

		sprintf(szTemp2, "%.2lf", ausgabeges.fil_umsatz );
    LlDefineFieldExt(hJob, "ausgabeges.fil_ges_umsatz",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabeges.fil_menge );
    LlDefineFieldExt(hJob, "ausgabeges.fil_ges_menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabeges.fil_ertrag );
    LlDefineFieldExt(hJob, "ausgabeges.fil_ges_ertrag",        szTemp2, LL_NUMERIC, NULL);



	LlDefineFieldExt(hJob, "ausgabe.fil_kla",        ausgabe.fil_kla, LL_TEXT, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.fil_umsatz );
    LlDefineFieldExt(hJob, "ausgabe.fil_ges_umsatz",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.fil_menge );
    LlDefineFieldExt(hJob, "ausgabe.fil_ges_menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.fil_ertrag );
    LlDefineFieldExt(hJob, "ausgabe.fil_ges_ertrag",        szTemp2, LL_NUMERIC, NULL);



}


//=============================================================================
void CMainFrame::OnEditList()
//=============================================================================
{
	CHAR szFilename[128+1] = "*.lst";
	HWND hWnd = m_hWnd;
	HJOB hJob;
    char *etc;
	char filename[512];

	etc = getenv ("BWS");
    sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	FILENAME_DEFAULT = filename;
	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden (Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");   //LL11


	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");


	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
		{
	        LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    // GR: Zur�cksetzen der internen Feld-Puffer
    LlDefineFieldStart(hJob);

//	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

	if (Listenauswahl != 1)  
    {
	   	sprintf ( szFilename, FILENAME_DEFAULT );
	}
	// GR: Aufruf des Designers
    if (LlDefineLayout(hJob, hWnd, "Design Rezeptur", LL_PROJECT_LIST, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
}


//=============================================================================
void CMainFrame::OnPrintLabel()
//=============================================================================
{
	DoLabelPrint();
}


//=============================================================================
void CMainFrame::OnPrintReport()
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");
    char *p;
    Token.NextToken ();
    sysdate(datvon) ;
    sysdate(datbis) ;
	while ((p = Token.NextToken ()) != NULL)
	{
		if (strcmp (p, "-h") == 0)
		{
              MessageBox ("Aufruf : sel_fil_einn -mdn Mandant -datvon Datum-von -datbis Datum-bis ",
                                "",
                                MB_OK);
              return;
		}
		if (strcmp (p, "-mdn") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            mdn = (atoi (p));
		}

	}
    CDialogDatum dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber OK zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		return;
	}

	DoListPrint();
}

//=============================================================================
void CMainFrame::DoLabelPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lbl", szTemp[30], szTemp2[40], szBoxText[200];
	HJOB hJob;
	int  nRet = 0;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// US: Choose new expression mode with formula support
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);
	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog

   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
   		{
	    	if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    for (int i=1; i<10; i++)
	{
		sprintf(szTemp, "Field%d", i);
    	LlDefineVariable(hJob, szTemp, szTemp);
   	}

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

    // GR: Druck starten

	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_BRIDGEMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

	// GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	LlPrintSetOption(hJob, LL_PRNOPT_OFFSET, 0);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
        return;
    }


	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

	char szPrinter[80], szPort[20];
	int  nRecCount = 30, nErrorValue = 0, nRecno, nLastPage;

	LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

    nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

	// GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	while (nRecno < nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& nErrorValue == 0)
	{
		for (i=1; i<10; i++)
		{
			sprintf(szTemp, "Field%d", i);
			if (LlPrintIsVariableUsed(hJob, szTemp))
			{
				sprintf(szTemp2, "contents of Field%d, record %d", i, nRecno);
    			LlDefineVariable(hJob, szTemp, szTemp2);
			}
    	}

    	// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    	sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    	nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        if (nErrorValue == LL_ERR_USER_ABORTED)
    	{
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
    		return;
    	}

    	// GR: Variablen drucken
    	nErrorValue = LlPrint(hJob);

		// GR: gehe zum naechsten Datensatz
    	nRecno++;
	}

	//GR: Druck beenden
	LlPrintEnd(hJob,0);


	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) l�schen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }

	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
}

//=============================================================================
void CMainFrame::DoListPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lst", szTemp[50], szTemp2[60], szBoxText[200];
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;
	strcpy(szTemp, " ");
	char *etc;
	char filename[512];

	etc = getenv ("BWS");
    sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	FILENAME_DEFAULT = filename;


	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");   
//	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");   //LL11


	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
	   	{
    		if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);




    // GR: Druck starten
	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }
    // GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
		if (Hauptmenue == 0)  //#LuD
		{
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		}
		
        return;
    }


	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

    int  nRecCount = 1, nErrorValue = 0, nPrintFehler = 0, nLastPage, nRecno;
	char szPrinter[80], szPort[20];

    LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

	nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	nRecCount = GetRecCount () ;
	if (nRecCount < 1) 
    {
		sprintf (dmess, "Datensatz nicht gefunden ");
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
//	sprintf (dmess, "nRecCount = %hd",nRecCount);  //050803
//	MessageBox(dmess, "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
/******
	int di = AWoWa.leseDS (nErrorValue);   //       1.DATENSATZ lesen f�r Variablen�bergabe
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
******/
//	APreis.SetDatabase (dbClass.GetDatabase());
	AMoWa.openDS ();
	int dleseStatus = 0;
	while (nRecno <= nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA)
			&& (dleseStatus == 0))
   	{
	    VariablenUebergabe ( hJob, szTemp2, nRecno );

		nErrorValue = LlPrint(hJob);
		while (nRecno <= nRecCount 
			&& (nErrorValue == 0) 
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& ((dleseStatus = AMoWa.leseDS (nPrintFehler)) == 0))  //       DATENSATZ lesen

		{

		    FelderUebergabe ( hJob, szTemp2, nRecno );
    		// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    		sprintf(szBoxText, "printing on %s %s\n %d von %d ", szPrinter, szPort,nRecno,nRecCount);
    		nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        	if (nErrorValue == LL_ERR_USER_ABORTED)
   			{
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;
   			}

    		// GR: Drucken der aktuellen Tabellenzeile
    		nErrorValue = LlPrintFields(hJob);
			nPrintFehler = nErrorValue;
//050803
			if (nErrorValue == LL_WRN_REPEAT_DATA)
			{
					// GR: Aktualisieren der seitenabhaengigen Variablen
			    VariablenUebergabe ( hJob, szTemp2, nRecno );
				nErrorValue = LlPrint(hJob);
//			    FelderUebergabe ( hJob, szTemp2, nRecno );
//	    		nErrorValue = LlPrintFields(hJob);
			}


			if (nErrorValue == 0)	// US: everything fine, record could have been printed...
	    	{
				// US: ...but probably the user defined a filter condition!
				//     So before updating time dependent variables we have to check if the record really has been printed:
				if (LlPrintDidMatchFilter(hJob))
				{
					// GR: Aktualisieren der zeitabhaengigen Variable 'FixedVariable2'
					// US: update the time dependent variable 'FixedVariable2'
					//     coming after printig the list(s)
		    		sprintf(szTemp2, "FixedVariable2, record %d", nRecno);
		    		LlDefineVariable(hJob, "FixedVariable2", szTemp2);
		    	}

	    		if (nPrintFehler == 0) nRecno++;	// GR: gehe zum naechsten Datensatz
			}
		}
  	}

	// US: all records have been printed, now flush the table
	//     If footer doesn't fit to this page try again for the next page:

	nErrorValue = LlPrintFieldsEnd(hJob);
	if (nErrorValue == LL_WRN_REPEAT_DATA)
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	    VariablenUebergabe ( hJob, szTemp2, nRecno );
	    FelderUebergabe ( hJob, szTemp2, nRecno );

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);
	}

	if (nErrorValue == LL_WRN_REPEAT_DATA)	// US: footer still doesn't fit!
		MessageBox("Error because table is too small for footer!", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);

	//GR: Druck beenden
	LlPrintEnd(hJob,0);
	
	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) loeschen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }
	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}
}

//=============================================================================
LRESULT CMainFrame::OnLulMessage(WPARAM wParam, LPARAM lParam)
//=============================================================================
{
	// GR: Dies ist die List & Label Callback Funktion.
	//     Saemtliche Rueckmeldungen bzw. Events werden dieser Funktion
	//     gemeldet.
	// US: This is the List & Label Callback function.
	//     Is is called for requests an notifications.


	PSCLLCALLBACK	pscCallback = (PSCLLCALLBACK) lParam;
	LRESULT			lRes = TRUE;
	CString			sVariableDescr;

	static CHAR szHelpText[256];

	ASSERT(pscCallback->_nSize == sizeof(scLlCallback));	// GR: Die groesse der Callback Struktur muss stimmen!
															// US: sizes of callback structs must match!

	switch(wParam)
	{
		case LL_CMND_VARHELPTEXT:	// GR: Es wird ein Beschreibungstext f�r eine Variable erfragt.
									// US: Helptext needed for selected variable within designer

				// GR: ( pscCallback->_lParam enth�lt einen LPCSTR des Beschreibungstextes )
				// US: ( pscCallback->_lParam contains a LPCSTR to the name of the selected variable )

				sVariableDescr = (LPCSTR)pscCallback->_lParam;

				if (!sVariableDescr.IsEmpty())
					sprintf(szHelpText,
							"This is the sample field / variable '%s'.",
							(LPCSTR)sVariableDescr);
				else
					strcpy(szHelpText, "No variable or field selected.");

				pscCallback->_lReply = (LPARAM)szHelpText;
				break;

		default:
				pscCallback->_lReply = lRes = FALSE; // GR: Die Nachricht wurde nicht bearbeitet.
													 // US: indicate that message hasn't been processed
	}

	return(lRes);
}


void CMainFrame::GetCfgValues (void)
{
       char cfg_v [512];

       if (ProgCfg == NULL) return;

       cfgOK = TRUE;
	   sprintf(Listenname,"RennerPenner");
       if (ProgCfg->GetCfgValue ("Listenname", cfg_v) == TRUE)
       {
		   sprintf(Listenname,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Listenauswahl", cfg_v) == TRUE)
       {
           Listenauswahl = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Hauptmenue", cfg_v) == TRUE)
       {
           Hauptmenue = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Divisor", cfg_v) == TRUE)
       {
           Divisor = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Format", cfg_v) == TRUE)
       {
		   sprintf(Format,cfg_v); 
	   }
}


    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt
	/************
	LlDefineFieldExt(hJob, "Barcode_EAN13", "44|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P2", "44|44444|44444|44", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P5", "44|44444|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN128", "EAN128ean128", LL_BARCODE_EAN128, NULL);
	LlDefineFieldExt(hJob, "Barcode_CODE128", "Code 128", LL_BARCODE_CODE128, NULL);
	LlDefineFieldExt(hJob, "Barcode_Codabar", "A123456A", LL_BARCODE_CODABAR, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCA", "44|44444", LL_BARCODE_EAN8, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCE", "4|44444|44444", LL_BARCODE_UPCA, NULL);
	LlDefineFieldExt(hJob, "Barcode_3OF9", "*TEST*", LL_BARCODE_3OF9, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IND", "44444", LL_BARCODE_25INDUSTRIAL, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IL", "444444", LL_BARCODE_25INTERLEAVED, NULL);
	LlDefineFieldExt(hJob, "Barcode_25MAT", "44444", LL_BARCODE_25MATRIX, NULL);
	LlDefineFieldExt(hJob, "Barcode_25DL", "44444", LL_BARCODE_25DATALOGIC, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET5", "44444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET10", "44444-4444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET12", "44444-444444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_FIM", "A", LL_BARCODE_FIM, NULL);
	**********/
void fitapldefines (HJOB hJob)
{

}


