//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by llmfc.rc
//
#define IDR_MAINFRAME                   2
#define IDD_ABOUTBOX                    100
#define IDD_DIALOG1                     101
#define IDC_WOVON                       101
#define IDC_WOBIS                       102
#define IDC_EDIT1                       103
#define IDC_JAHR                        103
#define IDC_MANDANT                     104
#define IDC_FILIALE                     105
#define IDC_WGVON                       106
#define IDC_AVON                        107
#define IDC_FILIALEBIS                  107
#define IDC_ABIS                        108
#define IDC_WGBIS                       108
#define IDC_KUNDE                       109
#define IDC_ART_VON                     109
#define IDC_KUNDEBIS                    110
#define IDC_ART_BIS                     110
#define IDC_DATVON                      111
#define IDC_RADIO_ANZ                   112
#define IDC_RADIO_MENGE                 112
#define IDC_RADIO_KUN                   113
#define IDC_RADIO_UMSATZ                113
#define IDC_RADIO_ERTRAG                114
#define IDC_RADIO2                      115
#define IDC_RADIO_AB                    115
#define IDC_RADIO3                      116
#define IDC_RADIO_AUF                   116
#define IDC_WGBIS1                      117
#define IDC_DATBIS                      117
#define IDC_MAX_ART                     118
#define IDC_RETOUREN                    120
#define ID_PRINT_LIST_PRINTER           32770
#define ID_PRINT_LIST_PREVIEW           32771
#define ID_PRINT_LABEL_PRINTER          32772
#define ID_PRINT_LABEL_PREVIEW          32773
#define ID_EDIT_LIST                    32774
#define ID_EDIT_LABEL                   32775
#define ID_FILE_DEBUG                   32776
#define ID_FILE_STOP_DEBUG              32778
#define ID_FILE_START_DEBUG             32779
#define ID_PRINT_REPORT                 32780
#define ID_PRINT_LABEL                  32781

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         121
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
