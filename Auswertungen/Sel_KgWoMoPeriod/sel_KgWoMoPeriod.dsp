# Microsoft Developer Studio Project File - Name="sel_KgWoMoPeriod" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=sel_KgWoMoPeriod - Win32 Release
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "sel_KgWoMoPeriod.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "sel_KgWoMoPeriod.mak" CFG="sel_KgWoMoPeriod - Win32 Release"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "sel_KgWoMoPeriod - Win32 Release" (basierend auf\
  "Win32 (x86) Application")
!MESSAGE "sel_KgWoMoPeriod - Win32 Debug" (basierend auf\
  "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "sel_KgWoMoPeriod - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "d:\informix\incl\esql" /I "..\include" /I "..\sel_include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 ODBC32.lib /nologo /subsystem:windows /machine:I386 /out:"d:\user\fit\bin\sel_KgWoMoPeriod.exe" /libpath:"..\lib"
# SUBTRACT LINK32 /incremental:yes /map /debug

!ELSEIF  "$(CFG)" == "sel_KgWoMoPeriod - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /I "..\include" /I "..\sel_include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 ODBC32.lib /nologo /subsystem:windows /debug /machine:I386 /out:"d:\user\fit\bin\sel_KgWoMoPeriod.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "sel_KgWoMoPeriod - Win32 Release"
# Name "sel_KgWoMoPeriod - Win32 Debug"
# Begin Source File

SOURCE=.\a_mo_wa.cpp
# End Source File
# Begin Source File

SOURCE=.\a_mo_wa.h
# End Source File
# Begin Source File

SOURCE=..\lib\cm32l9.lib
# End Source File
# Begin Source File

SOURCE=.\cmbtll11.h
# End Source File
# Begin Source File

SOURCE=.\cmll11.lib
# End Source File
# Begin Source File

SOURCE=.\datum.cpp
# End Source File
# Begin Source File

SOURCE=.\datum.h
# End Source File
# Begin Source File

SOURCE=.\DbClass.cpp
# End Source File
# Begin Source File

SOURCE=.\DbClass.h
# End Source File
# Begin Source File

SOURCE=..\sel_include\DbRange.cpp
# End Source File
# Begin Source File

SOURCE=..\sel_include\DbRange.h
# End Source File
# Begin Source File

SOURCE=..\sel_include\Debug.cpp
# End Source File
# Begin Source File

SOURCE=..\sel_include\Debug.h
# End Source File
# Begin Source File

SOURCE=.\DialogDatum.cpp
# End Source File
# Begin Source File

SOURCE=.\DialogDatum.h
# End Source File
# Begin Source File

SOURCE=..\sel_include\Form.cpp
# End Source File
# Begin Source File

SOURCE=..\sel_include\Form.h
# End Source File
# Begin Source File

SOURCE=.\llmfc.cpp
# End Source File
# Begin Source File

SOURCE=.\res\llmfc.ico
# End Source File
# Begin Source File

SOURCE=.\llmfc.rc
# End Source File
# Begin Source File

SOURCE=.\llsmpdoc.cpp
# End Source File
# Begin Source File

SOURCE=.\llsmpvw.cpp
# End Source File
# Begin Source File

SOURCE=.\mainfrm.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_progcfg.cpp
# End Source File
# Begin Source File

SOURCE=..\sel_include\stdafx.cpp
# End Source File
# Begin Source File

SOURCE=..\sel_include\STRFKT.CPP
# End Source File
# Begin Source File

SOURCE=..\sel_include\STRFKT.H
# End Source File
# Begin Source File

SOURCE=..\sel_include\Text.cpp
# End Source File
# Begin Source File

SOURCE=..\sel_include\Text.h
# End Source File
# Begin Source File

SOURCE=.\token.cpp
# End Source File
# Begin Source File

SOURCE=..\sel_include\Vector.cpp
# End Source File
# Begin Source File

SOURCE=..\sel_include\Vector.h
# End Source File
# End Target
# End Project
