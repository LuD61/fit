#include <stdio.h>
	static short woche_korr(short,short);
	static short woche_pre(char *);
	short get_woche(char *);
	short get_jahr(char *);
	short get_lauf_tag(char *);
	short get_schalt_jahr(short);
	short get_monat(char *);
	short get_tag(char *);
	#define   NEU   (short)1
	#define   ALT   (short)2

class DATUM
{
  private:
    int AnzahlTage(int m, int j);
    int TageImJahr(int j);
    long int TMJ_Nummer();
    void Nummer_TMJ(long int num);
  public:
    int tag,monat,jahr;
    DATUM ();
    DATUM (int t, int m, int j);
    DATUM SetzeDatum(int t, int m, int j);
    void format(char*);
//    char* format(char* formatstring);
    DATUM operator+ (long int t);
    void operator= (char* cdat);
    DATUM operator- (long int t);
    long int operator- (DATUM d);
    long int operator- (char* cdat);
    DATUM operator++();
    DATUM operator--();
};

