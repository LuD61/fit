//010112 Kw 1 von 2012 musste mit KW1 von 2011 verglichen werden !! 
//130810 Wochen VJ
//281209 Problem mit 53.KW (ende des Jahres 2009 Vergleich mit 2008)

//220909  Problem mit der 53.KW
#include "stdafx.h"
#include "DbClass.h"
#include "DbRange.h"
#include "datum.h"
#include "a_mo_wa.h"
#include "cmbtl9.h"
//#include "Debug.h"

struct A_MO_WA a_mo_wa, a_mo_wa_null;
struct AUSGABE ausgabe, ausgabe_null;
struct A_BAS a_bas, a_bas_null;
extern DB_CLASS dbClass;
extern DB_RANGE dbRange;
static long anzfelder = -1;
double menge[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
double umsatz[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
double menge_vj[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
double umsatz_vj[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
double menge_woche[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 };
double menge_woche_vj[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 };
short wochen_index = 0;
short d2000 = 2000;
char erster_tag[11];
char letzter_tag[11];
char erster_tag_wo1[11];
char letzter_tag_wo1[11];
char erster_tag_wo2[11];
char letzter_tag_wo2[11];
char erster_tag_wo3[11];
char letzter_tag_wo3[11];
char erster_tag_wo4[11];
char letzter_tag_wo4[11];
char erster_tag_wo5[11];
char letzter_tag_wo5[11];
char erster_tag_wo1_vorjahr[11];
char erster_tag_wo1_vj[11];
char letzter_tag_wo1_vj[11];
char erster_tag_wo2_vj[11];
char letzter_tag_wo2_vj[11];
char erster_tag_wo3_vj[11];
char letzter_tag_wo3_vj[11];
char erster_tag_wo4_vj[11];
char letzter_tag_wo4_vj[11];
char erster_tag_wo5_vj[11];
char letzter_tag_wo5_vj[11];
char datumvj_von[11];
char datumvj_bis[11];
char datumjr_von[11];
char datumjr_bis[11];
char datummo_von[11];
char datummo_bis[11];
char datumvjmo_von[11];
char datumvjmo_bis[11];
BOOL flg_wo1_gekappt = FALSE;
BOOL flg_wo1vj_gekappt = FALSE;
char tag[12];
short woche_jr;
int _jr;
int _mo, _wo;
int _tag;
short mdn = 1;
char filbereich [256] ;   
char kunbereich [256] ;   
char wgbereich [256] ;   
char abereich [256] ;   
short jr = 0;
short vjr = 0;
short vvjr = 0;
short movon ;
short monat ;
DATUM d_dat, d_datfirst, d_datlast;
DATE_STRUCT end_dat;
short erste_wo ;
short erste_wo_vj ;
short letzte_wo ;
short wo1 = 0;
short wo2 = 0;
short wo3 = 0;
short wo4 = 0;
short wo5 = 0;
short wo1_vj = 0;
short wo2_vj = 0;
short wo3_vj = 0;
short wo4_vj = 0;
short wo5_vj = 0;
short woche ;
char adr_krz [25];
double a_gew;
short a_bas_ag ;
short a_bas_dr_folge;
short a_bas_me_einh;
short a_bas_lief_einh;
double a_bas_a_gew;
char rez_preis_bz[12];
char a_bz1[25];
char einh_bz[9];
double rez_preis;
extern char Listenname[128];
char sys_par_wrt [2] = " ";
short abwper_par = 0;


int A_MO_WA_CLASS::dbcount (void)
/**
Tabelle eti lesen.
**/
{
  vjr = jr -1;
  vvjr = vjr -1;


         if (test_upd_cursor == -1)
         {
			dbClass.sqlout ((char *) sys_par_wrt, SQLCHAR, 2);
			int dsqlstatus = dbClass.sqlcomm ("select sys_par_wrt from sys_par where sys_par_nam = \"abwper_par\""); 
			if (dsqlstatus == 0)
			{
				if (atoi(sys_par_wrt) == 1) 
				{
					abwper_par = 1;
					prepare_per ();
				}
				else
				{
					abwper_par = 0;
		             prepare ();
				}
			}
			else
			{
	             prepare ();
			}


         }
	
		 d2000 = 2000;
		 if (jr < 2000) d2000 = 1900;
		 berechnePerioden ();


         dbClass.sqlopen (count_cursor);
         sqlstatus = dbClass.sqlfetch (count_cursor);
		 if (sqlstatus != 0) return -1 ;
		 anzfelder = 0;
         while (sqlstatus == 0)
         {
			 anzfelder ++;
	         sqlstatus = dbClass.sqlfetch (count_cursor);
         }
         dbClass.sqlopen (count_cursor);
         return anzfelder; 
}

void A_MO_WA_CLASS::berechnePerioden (void)
{
//========================================================================
//============= Hole Perioden im lfd-Jahr ================================
//========================================================================
		strcpy(erster_tag_wo1,"");
		strcpy(erster_tag_wo2,"");
		strcpy(erster_tag_wo3,"");
		strcpy(erster_tag_wo4,"");
		strcpy(erster_tag_wo5,"");
		strcpy(letzter_tag_wo1,"");
		strcpy(letzter_tag_wo2,"");
		strcpy(letzter_tag_wo3,"");
		strcpy(letzter_tag_wo4,"");
		strcpy(letzter_tag_wo5,"");

	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;

		d_datlast.SetzeDatum(1,1,_jr);
		d_datlast.format(datumjr_von);

		d_datlast.SetzeDatum(_tag,_mo,_jr);
		d_datlast.format(datummo_bis);
		d_datlast.format(datumjr_bis);
		--d_datlast;
		d_datlast.format(letzter_tag);
		letzte_wo = get_woche(letzter_tag);
		_wo = letzte_wo;
		while (letzte_wo == _wo)
		{
			++d_datlast;
			d_datlast.format(letzter_tag);
			_wo = get_woche(letzter_tag);
		}
		--d_datlast;
		d_datlast.format(letzter_tag);//Dies ist jetzt der letzte Tag der KW
		//221012 A
		while (letzte_wo == 52 && _jr == end_dat.year)
		{
			++d_datlast;
			d_datlast.format(letzter_tag);
			_wo = get_woche(letzter_tag);
			_jr = get_jahr(letzter_tag) + d2000; 
		}
		--d_datlast;
		d_datlast.format(letzter_tag);//Dies ist jetzt der letzte Tag der KW
//		if (jr != end_dat.year) letzte_wo = 53;
		//221012


	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;
		d_datfirst.SetzeDatum(_tag,_mo,_jr);
		++d_datfirst;
		++d_datfirst;
		d_datfirst.format(erster_tag);
		_jr = get_jahr(erster_tag); 
		erste_wo = get_woche(erster_tag);
		_wo = erste_wo;
/****
		if (erste_wo == 53)  // 271009 erste Woche soll Woche1 sein !
		{
			while (erste_wo == _wo)
			{
				++d_datfirst;
				d_datfirst.format(erster_tag);
				_wo = get_woche(erster_tag);
			}
			erste_wo = _wo;
		}
****/
		flg_wo1_gekappt = FALSE;
		while (erste_wo == _wo)
		{
			--d_datfirst;
			d_datfirst.format(erster_tag);
			if (get_jahr(erster_tag) != _jr && flg_wo1_gekappt == FALSE) 
			{
				++d_datfirst;
				d_datfirst.format(erster_tag_wo1);
				d_datfirst.format(datummo_von);
				flg_wo1_gekappt = TRUE;
				--d_datfirst;
			}
			_wo = get_woche(erster_tag);
		}
		++d_datfirst;
		d_datfirst.format(erster_tag);//Dies ist jetzt der 1. Tag der KW
		if (flg_wo1_gekappt == FALSE) d_datfirst.format(datummo_von);
		wo1 = get_woche(erster_tag); 


		d_dat=d_datfirst;

		if (flg_wo1_gekappt == FALSE) strncpy(erster_tag_wo1,erster_tag,10);
		d_dat=d_dat+6;
		d_dat.format(letzter_tag_wo1);
		++d_dat;
		while (d_datlast-d_dat >= 0)    //221012 statt alt ">"  neu ">=" 
		{
			if (wo2 == 0) 
			{
				d_dat.format(erster_tag_wo2);
				wo2 = get_woche(erster_tag_wo2);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo2);
			}
			else if (wo3 == 0)
			{
				d_dat.format(erster_tag_wo3);
				wo3 = get_woche(erster_tag_wo3);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo3);
			}
			else if (wo4 == 0)
			{
				d_dat.format(erster_tag_wo4);
				wo4 = get_woche(erster_tag_wo4);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo4);
				d_dat.format(letzter_tag_wo5);
				d_dat.format(datummo_bis);
				d_dat.format(datumjr_bis);
			}
			else if (wo5 == 0)
			{
				d_dat.format(erster_tag_wo5);
				wo5 = get_woche(erster_tag_wo5);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo5);
				while (get_jahr(letzter_tag_wo5) > _jr)  //220909  
				{
					--d_dat ;
					d_dat.format(letzter_tag_wo5);
				}

				d_dat.format(datummo_bis);
				d_dat.format(datumjr_bis);
			}
			++d_dat;
		}

//========================================================================
//============= Hole Perioden im Vorjahr ================================
//========================================================================
		strcpy(erster_tag_wo1_vj,"");
		strcpy(erster_tag_wo2_vj,"");
		strcpy(erster_tag_wo3_vj,"");
		strcpy(erster_tag_wo4_vj,"");
		strcpy(erster_tag_wo5_vj,"");
		strcpy(letzter_tag_wo1_vj,"");
		strcpy(letzter_tag_wo2_vj,"");
		strcpy(letzter_tag_wo3_vj,"");
		strcpy(letzter_tag_wo4_vj,"");
		strcpy(letzter_tag_wo5_vj,"");

	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor_vj);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;

		d_datlast.SetzeDatum(1,1,_jr);
		d_datlast.format(datumvj_von);
		d_datlast.format(datumvj_bis);


		d_datlast.SetzeDatum(_tag,_mo,_jr);
		d_datlast.format(datumvjmo_bis);
		d_datlast.format(datumvj_bis);
		--d_datlast;
		d_datlast.format(letzter_tag);
		letzte_wo = get_woche(letzter_tag);
		_wo = letzte_wo;
		while (letzte_wo == _wo)
		{
			++d_datlast;
			d_datlast.format(letzter_tag);
			_wo = get_woche(letzter_tag);
		}
		--d_datlast;
		d_datlast.format(letzter_tag);//Dies ist jetzt der letzte Tag der KW

	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor_vj);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;
		d_datfirst.SetzeDatum(_tag,_mo,_jr);
		++d_datfirst;
		++d_datfirst;
		++d_datfirst;  //010112
		d_datfirst.format(erster_tag);
		_jr = get_jahr(erster_tag); 
		d_datfirst.format(datumvjmo_von);
		erste_wo_vj = get_woche(erster_tag);
		_wo = erste_wo_vj;
		flg_wo1vj_gekappt = FALSE;
		while (erste_wo_vj == _wo)
		{
			--d_datfirst;
			d_datfirst.format(erster_tag);
			if (get_jahr(erster_tag) != _jr && flg_wo1vj_gekappt == FALSE) 
			{
				++d_datfirst;
				d_datfirst.format(erster_tag_wo1_vj);
				d_datfirst.format(datumvjmo_von);
				flg_wo1vj_gekappt = TRUE;
				--d_datfirst;
				break;
			}
			_wo = get_woche(erster_tag);
		}
		++d_datfirst;
		if (erste_wo_vj < erste_wo)     //260612
		{
			d_datfirst = d_datfirst + 7;
		}
		d_datfirst.format(erster_tag);//Dies ist jetzt der 1. Tag der KW
		wo1_vj = get_woche(erster_tag);


		d_dat=d_datfirst;

		if (flg_wo1vj_gekappt == FALSE) strncpy(erster_tag_wo1_vj,erster_tag,10);
		d_dat=d_dat+6;
		d_dat.format(letzter_tag_wo1_vj);
		++d_dat;
		while (d_datlast-d_dat > 0)
		{
			if (wo2_vj == 0) 
			{
				d_dat.format(erster_tag_wo2_vj);
				wo2_vj = get_woche(erster_tag_wo2_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo2_vj);
			}
			else if (wo3_vj == 0)
			{
				d_dat.format(erster_tag_wo3_vj);
				wo3_vj = get_woche(erster_tag_wo3_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo3_vj);
			}
			else if (wo4_vj == 0)
			{
				d_dat.format(erster_tag_wo4_vj);
				wo4_vj = get_woche(erster_tag_wo4_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo4_vj);
				d_dat.format(datumvjmo_bis);
				d_dat.format(datumvj_bis);
			}
			else if (wo5_vj == 0)
			{
				d_dat.format(erster_tag_wo5_vj);
				wo5_vj = get_woche(erster_tag_wo5_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo5_vj);
				d_dat.format(datumvjmo_bis);
				d_dat.format(datumvj_bis);
			}
			++d_dat;
		}




		woche_jr = jr;
		if (erste_wo > letzte_wo) woche_jr --;

		//egal wie die Perioden im Vorjahr eingestellt sind, immer kw_x mit kw_vj_x vergleichen 
		strcpy (erster_tag_wo1_vorjahr, erster_tag_wo1_vj);
        int doffset = holeVJ_Tage(wo1_vj,erster_tag_wo1_vj,letzter_tag_wo1_vj);
		//290111 A
		d_dat.SetzeDatum(get_tag(letzter_tag_wo1_vj),get_monat(letzter_tag_wo1_vj),vjr);
		d_dat = d_dat + 1;
		d_dat.format(erster_tag_wo2_vj);
		d_dat = d_dat + 6;
		d_dat.format(letzter_tag_wo2_vj);
		d_dat = d_dat + 1;
		d_dat.format(erster_tag_wo3_vj);
		d_dat = d_dat + 6;
		d_dat.format(letzter_tag_wo3_vj);
		d_dat = d_dat + 1;
		d_dat.format(erster_tag_wo4_vj);
		d_dat = d_dat + 6;
		d_dat.format(letzter_tag_wo4_vj);
		//221012 A
		while (get_jahr(letzter_tag_wo4_vj) + d2000 != vjr)
		{
			--d_dat;
			d_dat.format(letzter_tag_wo4_vj);
		}
		//221012 A

		//290111 E




		strcpy (erster_tag_wo1_vj, erster_tag_wo1_vorjahr);
//290111      holeVJ_Tage(wo2_vj+doffset,erster_tag_wo2_vj,letzter_tag_wo2_vj);
//290111        holeVJ_Tage(wo3_vj+doffset,erster_tag_wo3_vj,letzter_tag_wo3_vj);
//290111        holeVJ_Tage(wo4_vj+doffset,erster_tag_wo4_vj,letzter_tag_wo4_vj);

//281209        holeVJ_Tage(wo5+doffset,erster_tag_wo5_vj,letzter_tag_wo5_vj);
		d_dat.SetzeDatum(get_tag(letzter_tag_wo4_vj),get_monat(letzter_tag_wo4_vj),vjr);
		d_dat = d_dat + 1; //290111
		d_dat.format(erster_tag_wo5_vj);
		d_dat = d_dat + 6;
		//221012 A
		while (get_jahr(erster_tag_wo5_vj) > (vjr - d2000))
		{
			--d_dat;
			d_dat.format(erster_tag_wo5_vj);
		}
		//221012 A

		d_dat.format(letzter_tag_wo5_vj);
		while (get_jahr(letzter_tag_wo5_vj) > (vjr - d2000))
		{
			--d_dat;
			d_dat.format(letzter_tag_wo5_vj);
		}
		//281209 E



		if (wo1_vj == 1 && wo1 == 53)
		{
			wo1_vj = 1;
			wo2_vj = wo1_vj +1;
			wo3_vj = wo2_vj +1;
			wo4_vj = wo3_vj +1;
		}
		wo1_vj = wo1;
		wo2_vj = wo2;
		wo3_vj = wo3;
		wo4_vj = wo4;
		// KW53 des akt. Jahre muss mit KW1 des Vorjahres verglichen werden , sonst fehlen hier die Daten  
		if (wo5_vj == 1 && wo5 == 53) 
		{
			wo5_vj = 1;
		}
		else
		{
			wo5_vj = wo5;
		}
		if (wo1 > wo1_vj)
		{
			strcpy(erster_tag_wo1_vj, erster_tag_wo2_vj); 
			strcpy(erster_tag_wo2_vj, erster_tag_wo3_vj); 
			strcpy(erster_tag_wo3_vj, erster_tag_wo4_vj); 
			strcpy(erster_tag_wo4_vj, erster_tag_wo5_vj); 
			strcpy(letzter_tag_wo1_vj, letzter_tag_wo2_vj); 
			strcpy(letzter_tag_wo2_vj, letzter_tag_wo3_vj); 
			strcpy(letzter_tag_wo3_vj, letzter_tag_wo4_vj); 
			strcpy(letzter_tag_wo4_vj, letzter_tag_wo5_vj); 
			if (wo5 == 0) wo5_vj = 0;
			if (wo5 != 0) //300512
			{
				d_dat.format(letzter_tag_wo4_vj);
				d_dat=d_dat+1;
				d_dat.format(erster_tag_wo5_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo5_vj);
			}
		}
		strcpy(datumvjmo_bis,letzter_tag_wo4_vj);
		strcpy(datumvj_bis,letzter_tag_wo4_vj);
//130810		if (wo5 > 0)
		if (wo5_vj > 0) //130810
		{
			strcpy(datumvjmo_bis,letzter_tag_wo5_vj);
			strcpy(datumvj_bis,letzter_tag_wo5_vj);
		}
		else
		{
			strcpy(letzter_tag_wo5_vj,letzter_tag_wo4_vj);
			strcpy(erster_tag_wo5_vj,"");
		}


}

int A_MO_WA_CLASS::holeVJ_Tage (int wo_jr, char* erster_tag, char* letzter_tag)
{
	int wo = 0;
	int di = 0;
	int tmp_wo = 0;
	int tmp_jr = 0;
	int doffset = 0;
		d_dat.SetzeDatum(1,1,vjr);
		d_dat.format(erster_tag);
		wo = get_woche(erster_tag);
		while (wo_jr == 1 && wo != 1)  //010112
		{
			++d_dat;
			d_dat.format(erster_tag);
			wo = get_woche(erster_tag);
		}
		if (wo_jr == 53 && wo == 1) doffset = 1;  //damit kw2 im jr mit der kw3 im vjr usw. verglichen werden 
		if (wo_jr > 1 && wo_jr < 53)
		{
			di = 0;
			while (wo != wo_jr)
			{
				++d_dat; di ++;
				d_dat.format(erster_tag);
				wo = get_woche(erster_tag);
				tmp_wo = wo;
				wo = get_woche(erster_tag);
				if (wo < tmp_wo) // wenn wo = 53 und im Vorjahr keine 53.kw vorhanden   220909
				{ 
					wo_jr = wo; //damit der letzte Tag �ber diese Woche ermittelt werden kann    220909
				}
				if (di > 365) break;
			}
		}
		di = 0;
		if (wo == 1 && wo_jr == 53)
		{
			d_dat.SetzeDatum(1,1,vjr);
			d_dat = d_dat - 7;
			d_dat.format(erster_tag);
			wo = get_woche(erster_tag);
			while (wo != 1)
			{
				++d_dat;
				d_dat.format(erster_tag);
				wo = get_woche(erster_tag);
			}
		}
//290111		while (wo == wo_jr || (wo == 1 && wo_jr == 53))
		while (wo == wo_jr ) //290111 
		{
			++d_dat; di ++;
			d_dat.format(letzter_tag);
			wo = get_woche(letzter_tag);
			if (di > 8) break;
		}
		--d_dat;
		d_dat.format(letzter_tag);
    	while (get_jahr(letzter_tag) > (vjr -d2000))  
		{
			--d_dat ;
			d_dat.format(letzter_tag);
		}

		d_dat.format(letzter_tag);
		return doffset;
}

int A_MO_WA_CLASS::leseDS (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }


      fehlercode = dbClass.sqlfetch (count_cursor);


	  if (fehlercode == 0)
      {
			 ausgabe.kun = a_mo_wa.kun;
			 if (ausgabe.kun_fil == 1)
			 {
				 ausgabe.kun = 0;
				 a_mo_wa.fil = (short) a_mo_wa.kun;
				 a_mo_wa.kun = 0;
				 ausgabe.fil = a_mo_wa.fil;
			 }
			 else
			 {
				 ausgabe.kun = a_mo_wa.kun;
				 sprintf (ausgabe.fil_kla,"%ld",ausgabe.vertr);
				 ausgabe.fil = 0;
			 }

            if (ausgabe.kun_fil == 0)
			{
				dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
				dbClass.sqlout ((char *) adr_krz, SQLCHAR, 17);
				dbClass.sqlcomm ("select adr.adr_krz from kun,adr where kun = ?  and kun.adr1 = adr.adr");
			}
			else
			{
				dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
				dbClass.sqlout ((char *) adr_krz, SQLCHAR, 17);
				dbClass.sqlcomm ("select adr.adr_krz from fil,adr where fil.fil = ? and fil.adr = adr.adr ");
			}



		sqlstatus = dbClass.sqlopen (readcursor);
//		sqlstatus = dbClass.sqlopen (readcursor_mo);
		sqlstatus = dbClass.sqlopen (readcursor_woche_jr);
		sqlstatus = dbClass.sqlopen (readcursor_woche_vjr);
//		if (wo1 > wo2 || wo1 == 1)  dbClass.sqlopen (readcursor_woche1_vjr);;
//		if (wo1_vj > wo2_vj || wo1_vj == 1) dbClass.sqlopen (readcursor_woche1_vvjr);;
		menge[0] = 0.0;menge[1] = 0.0;menge[2] = 0.0;menge[3] = 0.0;menge[4] = 0.0;
		menge[5] = 0.0;menge[6] = 0.0;menge[7] = 0.0;menge[8] = 0.0;menge[9] = 0.0;
		menge[10] = 0.0;menge[11] = 0.0;menge[12] = 0.0;menge[13] = 0.0;

		menge_vj[0] = 0.0;menge_vj[1] = 0.0;menge_vj[2] = 0.0;menge_vj[3] = 0.0;menge_vj[4] = 0.0;
		menge_vj[5] = 0.0;menge_vj[6] = 0.0;menge_vj[7] = 0.0;menge_vj[8] = 0.0;menge_vj[9] = 0.0;
		menge_vj[10] = 0.0;menge_vj[11] = 0.0;menge_vj[12] = 0.0;menge_vj[13] = 0.0;

		//menge[1] == wo1
		//menge[2] == wo2
		//menge[3] == wo3
		//menge[4] == wo4
		//menge[5] == wo5
		//menge[6] == Menge aus readcursor : Monat 1-n
		//menge[7] == enge aus readcursor_mo  Monat n
        sqlstatus = dbClass.sqlfetch (readcursor);
		while (sqlstatus == 0)
         {
             if (a_mo_wa.jr == jr)
			 {
				menge[6] += a_mo_wa.me_vk_mo ;
				 if (a_mo_wa.fil > 0)
				 {
					umsatz[6] += a_mo_wa.ums_fil_ek_mo; 
				 }
				 else
				 {
					umsatz[6] += a_mo_wa.ums_vk_mo; 
				 }

			 }
			 else
			 {
				menge_vj[6] += a_mo_wa.me_vk_mo ;
				 if (a_mo_wa.fil > 0)
				 {
					umsatz_vj[6] += a_mo_wa.ums_fil_ek_mo; 
				 }
				 else
				 {
					umsatz_vj[6] += a_mo_wa.ums_vk_mo; 
				 }
			 }

	         sqlstatus = dbClass.sqlfetch (readcursor);
         }

		if (abwper_par > 0)
		{
			sqlstatus = dbClass.sqlopen (readcursor_vj);
			sqlstatus = dbClass.sqlfetch (readcursor_vj);
			while (sqlstatus == 0)
			{
				menge_vj[6] += a_mo_wa.me_vk_mo ;
				 if (a_mo_wa.fil > 0)
				 {
					umsatz_vj[6] += a_mo_wa.ums_fil_ek_mo; 
				 }
				 else
				 {
					umsatz_vj[6] += a_mo_wa.ums_vk_mo; 
				 }
	         sqlstatus = dbClass.sqlfetch (readcursor_vj);
			 }
         }





		/*********** 
        sqlstatus = dbClass.sqlfetch (readcursor_mo);
		while (sqlstatus == 0)
         {
             monat = a_mo_wa.mo;
             if (a_mo_wa.jr == jr)
			 {
				menge[7] += a_mo_wa.me_vk_mo ;
				 if (a_mo_wa.fil > 0)
				 {
					umsatz[7] += a_mo_wa.ums_fil_ek_mo; 
				 }
				 else
				 {
					umsatz[7] += a_mo_wa.ums_vk_mo; 
				 }

			 }
			 else
			 {
				menge_vj[7] += a_mo_wa.me_vk_mo ;
				 if (a_mo_wa.fil > 0)
				 {
					umsatz_vj[7] += a_mo_wa.ums_fil_ek_mo; 
				 }
				 else
				 {
					umsatz_vj[7] += a_mo_wa.ums_vk_mo; 
				 }
			 }

	         sqlstatus = dbClass.sqlfetch (readcursor_mo);
         }
		 **********************/

		wochen_index = 0; 

        if (abwper_par == 0)
		{
			sqlstatus = dbClass.sqlfetch (readcursor_woche_jr);
			while (sqlstatus == 0)
			{
				menge[wochen_index] += a_mo_wa.me_vk_mo ;
				umsatz[wochen_index] += a_mo_wa.ums_vk_mo; 
				sqlstatus = dbClass.sqlfetch (readcursor_woche_jr);
			}
			/***
			if (wo1 > wo2)  // wenn bei periode 1 die woche mit kw 52 der 53 beginnt 
			{
				menge[1] = 0.0;
				umsatz[1] = 0.0;
				sqlstatus = dbClass.sqlfetch (readcursor_woche1_vjr); //1 Jahr davor suchen
				if (sqlstatus == 0)
				{
					menge[1] += a_mo_wa.me_vk_mo ;
					umsatz[1] += a_mo_wa.ums_vk_mo; 
				}
			}
			*****/
			wochen_index = 0;

			sqlstatus = dbClass.sqlfetch (readcursor_woche_vjr);
			while (sqlstatus == 0)
			{
				menge_vj[wochen_index] += a_mo_wa.me_vk_mo ;
				umsatz_vj[wochen_index] += a_mo_wa.ums_vk_mo; 
				sqlstatus = dbClass.sqlfetch (readcursor_woche_vjr);
			}
			/***
			if (wo1_vj > wo2_vj)  // wenn bei periode 1 die woche mit kw 52 der 53 beginnt 
			{
				menge_vj[1] = 0.0;
				umsatz_vj[1] = 0.0;
				sqlstatus = dbClass.sqlfetch (readcursor_woche1_vvjr); //1 Jahr davor suchen
				if (sqlstatus == 0)
				{
					menge_vj[1] += a_mo_wa.me_vk_mo ;
					umsatz_vj[1] += a_mo_wa.ums_vk_mo; 
				}
			}
			*****/
		}
		else //if abwper_par
		{
			sqlstatus = dbClass.sqlfetch (readcursor_woche_jr);
			while (sqlstatus == 0)
			{
				menge[wochen_index] += a_mo_wa.me_vk_mo ;
//				umsatz[wochen_index] += a_mo_wa.ums_vk_mo; 
				sqlstatus = dbClass.sqlfetch (readcursor_woche_jr);
			}
			/*****
			if (wo1 == 1 || wo1 > wo2)  // Woche 1 immer mit den Daten aus a_tag_wa �berschreiben, da evt hier nicht volle Woche
			{
				menge[1] = 0.0;
				umsatz[1] = 0.0;
				sqlstatus = dbClass.sqlfetch (readcursor_woche1_vjr); //1 Jahr davor suchen
				if (sqlstatus == 0)
				{
					menge[1] += a_mo_wa.me_vk_mo ;
					umsatz[1] += a_mo_wa.ums_vk_mo; 
				}
			}
			*******/
			wochen_index = 0;
			sqlstatus = dbClass.sqlfetch (readcursor_woche_vjr);
			while (sqlstatus == 0)
			{
				menge_vj[wochen_index] += a_mo_wa.me_vk_mo ;
//				umsatz_vj[wochen_index] += a_mo_wa.ums_vk_mo; 
				sqlstatus = dbClass.sqlfetch (readcursor_woche_vjr);
			}
			/*****
			if (wo1_vj == 1 || wo1_vj > wo2_vj)  // Woche 1 immer mit den Daten aus a_tag_wa �berschreiben, da evt hier nicht volle Woche
			{
				menge_vj[1] = 0.0;
				umsatz_vj[1] = 0.0;
				sqlstatus = dbClass.sqlfetch (readcursor_woche1_vvjr); //1 Jahr davor suchen
				if (sqlstatus == 0)
				{
					menge_vj[1] += a_mo_wa.me_vk_mo ;
					umsatz_vj[1] += a_mo_wa.ums_vk_mo; 
				}
			}
			*****/
		}
		menge[7] = menge[1] + menge[2] + menge[3] + menge[4] + menge[5];  
		menge_vj[7] = menge_vj[1] + menge_vj[2] + menge_vj[3] + menge_vj[4] + menge_vj[5] ; 

		umsatz[7] = umsatz[1] + umsatz[2] + umsatz[3] + umsatz[4] + umsatz[5];  
		umsatz_vj[7] = umsatz_vj[1] + umsatz_vj[2] + umsatz_vj[3] + umsatz_vj[4] + umsatz_vj[5];  

		if (movon == 1)
		{
			// in Periode 1 m�ssen diese gleich sein , wo werden jetzt Rundungsdifferenzen vermieden
			menge[6] = menge[7];
			menge_vj[6] = menge_vj[7];
		}
	
	}
      return fehlercode;
}

int A_MO_WA_CLASS::openDS (void)
{
         return dbClass.sqlopen (readcursor);
}

void A_MO_WA_CLASS::prepare (void)
{

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);

	fil_abrech_cursor = dbClass.sqlcursor ("select end_dat  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= " 
						"(select end_dat from fil_abrech where mdn = ? and fil = 0 "
						"and period_kz = \"M\" and jr = ? and period = ?) "
						"order by end_dat desc ");




    dbRange.RangeIn ("a_mo_wa.fil",filbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_mo_wa.kun",kunbereich, SQLLONG, 0); 
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_mo_wa.a",abereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);

	dbClass.sqlout ((char *) ausgabe.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((long *) &ausgabe.vertr, SQLLONG, 2);
	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun

           count_cursor = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_mo_wa.fil > 0 then fil.fil_kla "
							"	when a_mo_wa.fil = 0 then \"\" end klasse , "
							"case when a_mo_wa.kun > 0 then kun.vertr1 "
							"	when a_mo_wa.kun = 0 then 0 end vertr , "

							"case when a_mo_wa.fil = 0 then a_mo_wa.kun "
							"     when a_mo_wa.fil > 0 then a_mo_wa.fil end kunde, "
							"case when a_mo_wa.fil = 0 then 0 "
							"     when a_mo_wa.fil > 0 then 1 end kun_fil "
							"from a_mo_wa,a_bas,wg, outer fil ,outer kun where "
							"    a_mo_wa.mdn = fil.mdn "
							"and <a_mo_wa.fil> "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_mo_wa.mdn = ? "
							"and <a_mo_wa.kun> "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between 1 and ? "
							"and <wg.wg> "
							"and <a_mo_wa.a> "
							"and a_mo_wa.me_vk_mo <> 0.0 "
							"group by 1,2,3,4 order by klasse,vertr,kun_fil,kunde "
							));
	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_mo_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);

	// cursor f�r laufendes Jahr (z.b. bei movon = 6 : Monat 1 bis 6)
	// 2 * fetch , f�r vjr und jr
           readcursor = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_mo_wa.fil = 0 then a_mo_wa.kun "
							"     when a_mo_wa.fil > 0 then a_mo_wa.fil end kunde, "
							"case when a_mo_wa.fil = 0 then 0 "
							"     when a_mo_wa.fil > 0 then 1 end kun_fil, "
							"a_mo_wa.jr, "
							"sum(case when a_bas.me_einh <> 2 then a_mo_wa.me_vk_mo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_mo_wa.me_vk_mo  end ) menge, "
							"sum(a_mo_wa.ums_vk_mo), "
							"sum(a_mo_wa.ums_fil_ek_mo) "
							"from a_mo_wa,wg,a_bas where a_mo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_mo_wa.mdn = ? "
							"and a_mo_wa.fil = ? "
							"and a_mo_wa.kun = ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between 1 and ? "
							"and <wg.wg> "
							"and <a_mo_wa.a> "
							"and a_mo_wa.me_vk_mo <> 0.0 "
							"group by 1,2,3 order by kun_fil,kunde "
							));

		   /*****

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_mo_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);
 
	// cursor f�r Monat 
	// 2 * fetch , f�r vjr und jr
           readcursor_mo = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_mo_wa.fil = 0 then a_mo_wa.kun "
							"     when a_mo_wa.fil > 0 then a_mo_wa.fil end kunde, "
							"case when a_mo_wa.fil = 0 then 0 "
							"     when a_mo_wa.fil > 0 then 1 end kun_fil, "
							"a_mo_wa.jr, a_mo_wa.mo, "
							"sum(case when a_bas.me_einh <> 2 then a_mo_wa.me_vk_mo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_mo_wa.me_vk_mo  end ) menge, "
							"sum(a_mo_wa.ums_vk_mo), "
							"sum(a_mo_wa.ums_fil_ek_mo) "
							"from a_mo_wa,a_bas,wg where a_mo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_mo_wa.mdn = ? "
							"and a_mo_wa.fil = ? "
							"and a_mo_wa.kun = ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo = ? "
							"and <wg.wg> "
							"and <a_mo_wa.a> "
							"and a_mo_wa.me_vk_mo <> 0.0 "
							"group by 1,2,3,4 "));
							**************/

	dbClass.sqlin ((short *) &wo1, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo2, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo3, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo4, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo5, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo1, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo2, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo3, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo4, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo5, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_wo_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &wochen_index, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);
	// cursor f�r die 4 (oder 5) Wochen
	// x * fetch , f�r jr
           readcursor_woche_jr = dbClass.sqlcursor (dbRange.SetRange ("select " 
							"case when a_wo_wa.fil = 0 then a_wo_wa.kun "
							"     when a_wo_wa.fil > 0 then a_wo_wa.fil end kunde, "
							"case when a_wo_wa.fil = 0 then 0 "
							"     when a_wo_wa.fil > 0 then 1 end kun_fil, "
							"case when a_wo_wa.wo = ? then 1 "
							"     when a_wo_wa.wo = ? then 2 "
							"     when a_wo_wa.wo = ? then 3 "
							"     when a_wo_wa.wo = ? then 4 "
							"     when a_wo_wa.wo = ? then 5  end index, "
							"a_wo_wa.jr, a_wo_wa.wo, "
							"sum(case when a_bas.me_einh <> 2 then a_wo_wa.me_vk_wo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_wo_wa.me_vk_wo  end ) menge, "
							"sum(a_wo_wa.ums_vk_wo), "
							"sum(a_wo_wa.ums_fil_ek_wo) "
							"from a_wo_wa,a_bas,wg where a_wo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_wo_wa.mdn = ? "
							"and a_wo_wa.fil = ? "
							"and a_wo_wa.kun = ? "
							"and a_wo_wa.jr = ? "
							"and a_wo_wa.wo in (?,?,?,?,?) "
							"and <wg.wg> "
							"and <a_wo_wa.a> "
							"and a_wo_wa.me_vk_wo <> 0.0 "
							"group by 1,2,3,4,5 order by kun_fil,kunde "
							));

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo1, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_wo_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);
	// cursor f�r die 1. Woche, wenn wo1 > wo2
	// 1 * fetch , f�r Vorjahr
           readcursor_woche1_vjr = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_wo_wa.fil = 0 then a_wo_wa.kun "
							"     when a_wo_wa.fil > 0 then a_wo_wa.fil end kunde, "
							"case when a_wo_wa.fil = 0 then 0 "
							"     when a_wo_wa.fil > 0 then 1 end kun_fil, "
							"a_wo_wa.jr, a_wo_wa.wo, "
							"sum(case when a_bas.me_einh <> 2 then a_wo_wa.me_vk_wo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_wo_wa.me_vk_wo  end ) menge, "
							"sum(a_wo_wa.ums_vk_wo), "
							"sum(a_wo_wa.ums_fil_ek_wo) "
							"from a_wo_wa,a_bas,wg where a_wo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_wo_wa.mdn = ? "
							"and a_wo_wa.fil = ? "
							"and a_wo_wa.kun = ? "
							"and a_wo_wa.jr = ? "
							"and a_wo_wa.wo = ? "
							"and <wg.wg> "
							"and <a_wo_wa.a> "
							"and a_wo_wa.me_vk_wo <> 0.0 "
							"group by 1,2,3,4 order by kun_fil,kunde "
							));

 
//==========================================================================================
//=============================== ubers Vorjahr  ===========================================
//==========================================================================================
  vjr = jr -1;
  vvjr = vjr -1;

	dbClass.sqlin ((short *) &wo1_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo2_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo3_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo4_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo5_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo1_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo2_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo3_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo4_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo5_vj, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_wo_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &wochen_index, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);
	// cursor f�r die 4 (oder 5) Wochen
	// x * fetch , f�r vjr
           readcursor_woche_vjr = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_wo_wa.fil = 0 then a_wo_wa.kun "
							"     when a_wo_wa.fil > 0 then a_wo_wa.fil end kunde, "
							"case when a_wo_wa.fil = 0 then 0 "
							"     when a_wo_wa.fil > 0 then 1 end kun_fil, "
							"case when a_wo_wa.wo = ? then 1 "
							"     when a_wo_wa.wo = ? then 2 "
							"     when a_wo_wa.wo = ? then 3 "
							"     when a_wo_wa.wo = ? then 4 "
							"     when a_wo_wa.wo = ? then 5  end index, "
							"a_wo_wa.jr, a_wo_wa.wo, "
							"sum(case when a_bas.me_einh <> 2 then a_wo_wa.me_vk_wo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_wo_wa.me_vk_wo  end ) menge, "
							"sum(a_wo_wa.ums_vk_wo), "
							"sum(a_wo_wa.ums_fil_ek_wo) "
							"from a_wo_wa,a_bas,wg where a_wo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_wo_wa.mdn = ? "
							"and a_wo_wa.fil = ? "
							"and a_wo_wa.kun = ? "
							"and a_wo_wa.jr = ? "
							"and a_wo_wa.wo in (?,?,?,?,?) "
							"and <wg.wg> "
							"and <a_wo_wa.a> "
							"and a_wo_wa.me_vk_wo <> 0.0 "
							"group by 1,2,3,4,5 order by kun_fil,kunde "
							));

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &vvjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo1_vj, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_wo_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);
	// cursor f�r die 1. Woche, wenn wo1 > wo2
	// 1 * fetch , f�r VorVorjahr
           readcursor_woche1_vvjr = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_wo_wa.fil = 0 then a_wo_wa.kun "
							"     when a_wo_wa.fil > 0 then a_wo_wa.fil end kunde, "
							"case when a_wo_wa.fil = 0 then 0 "
							"     when a_wo_wa.fil > 0 then 1 end kun_fil, "
							"a_wo_wa.jr, a_wo_wa.wo, "
							"sum(case when a_bas.me_einh <> 2 then a_wo_wa.me_vk_wo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_wo_wa.me_vk_wo  end ) menge, "
							"sum(a_wo_wa.ums_vk_wo), "
							"sum(a_wo_wa.ums_fil_ek_wo) "
							"from a_wo_wa,a_bas,wg where a_wo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_wo_wa.mdn = ? "
							"and a_wo_wa.fil = ? "
							"and a_wo_wa.kun = ? "
							"and a_wo_wa.jr = ? "
							"and a_wo_wa.wo = ? "
							"and <wg.wg> "
							"and <a_wo_wa.a> "
							"and a_wo_wa.me_vk_wo <> 0.0 "
							"group by 1,2,3,4 order by kun_fil,kunde "
							));



	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);

	fil_abrech_cursor_vj = dbClass.sqlcursor ("select end_dat  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= " 
						"(select end_dat from fil_abrech where mdn = ? and fil = 0 "
						"and period_kz = \"M\" and jr = ? and period = ?) "
						"order by end_dat desc ");



 
}
void A_MO_WA_CLASS::prepare_per (void)
{


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);

	fil_abrech_cursor = dbClass.sqlcursor ("select end_dat  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= " 
						"(select end_dat from fil_abrech where mdn = ? and fil = 0 "
						"and period_kz = \"M\" and jr = ? and period = ?) "
						"order by end_dat desc ");




	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
    dbRange.RangeIn ("a_per_wa.fil",filbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_per_wa.kun",kunbereich, SQLLONG, 0); 
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_per_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((char *) ausgabe.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((long *) &ausgabe.vertr, SQLLONG, 2);
	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	
	count_cursor = dbClass.sqlcursor (dbRange.SetRange("select "
							"case when a_per_wa.fil > 0 then fil.fil_kla "
							"	when a_per_wa.fil = 0 then \"\" end klasse , "
							"case when a_per_wa.kun > 0 then kun.vertr1 "
							"	when a_per_wa.kun = 0 then 0 end vertr , "

							"case when a_per_wa.fil = 0 then a_per_wa.kun "
							"     when a_per_wa.fil > 0 then a_per_wa.fil end kunde, "
							"case when a_per_wa.fil = 0 then 0 "
							"     when a_per_wa.fil > 0 then 1 end kun_fil "
							"from a_per_wa,a_bas,wg, outer fil, outer kun where "
							"    a_per_wa.mdn = fil.mdn "
							"and a_per_wa.fil = fil.fil "
							"and a_per_wa.kun = kun.kun "
							"and a_per_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_per_wa.mdn = ? "
							"and <a_per_wa.fil>  "
							"and <a_per_wa.kun> "
							"and a_per_wa.jr between ? and ? "
							"and a_per_wa.period between 1 and ? "
							"and <wg.wg> "
							"and <a_per_wa.a> "
							"and a_per_wa.me_vk_per <> 0.0 "
							"group by 1,2,3,4 order by klasse,vertr,kun_fil,kunde "
							));
	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);
//	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_per_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);

	// cursor f�r laufendes Jahr (z.b. bei movon = 6 : Monat 1 bis 6)
           readcursor = dbClass.sqlcursor (dbRange.SetRange("select "
							"case when a_per_wa.fil = 0 then a_per_wa.kun "
							"     when a_per_wa.fil > 0 then a_per_wa.fil end kunde, "
							"case when a_per_wa.fil = 0 then 0 "
							"     when a_per_wa.fil > 0 then 1 end kun_fil, "
							"a_per_wa.jr, "
							"sum(case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end ) menge, "
							"sum(a_per_wa.ums_vk_per), "
							"sum(a_per_wa.ums_fil_ek_per) "
							"from a_per_wa,wg,a_bas where a_per_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_per_wa.mdn = ? "
							"and a_per_wa.fil = ? "
							"and a_per_wa.kun = ? "
							"and a_per_wa.jr = ? "
							"and a_per_wa.period between 1 and ? "
							"and <wg.wg> "
							"and <a_per_wa.a> "
							"and a_per_wa.me_vk_per <> 0.0 "
							"group by 1,2,3 order by kun_fil,kunde "
							));


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((char *) datumvj_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumvj_bis, SQLCHAR, 11);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_tag_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);

//							"sum(a_tag_wa.pr_fil_ek * (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag)) "
           readcursor_vj = dbClass.sqlcursor (dbRange.SetRange("select "
							"case when a_tag_wa.fil = 0 then a_tag_wa.kun "
							"     when a_tag_wa.fil > 0 then a_tag_wa.fil end kunde, "
							"case when a_tag_wa.fil = 0 then 0 "
							"     when a_tag_wa.fil > 0 then 1 end kun_fil, "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag )  end ) menge, "
							"sum(a_tag_wa.ums_vk_tag), "
							"sum((a_tag_wa.ums_fil_ek_e + a_tag_wa.ums_fil_ek_sa_e)) "
							"from a_tag_wa,wg,a_bas where a_tag_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_tag_wa.mdn = ? "
							"and a_tag_wa.fil = ? "
							"and a_tag_wa.kun = ? "
							"and a_tag_wa.dat between ? and ? "
							"and <wg.wg> "
							"and <a_tag_wa.a> "
							"and (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag  - a_tag_wa.me_ret_tag) <> 0.0 "
							"group by 1,2 order by kun_fil,kunde "
							));

	/***************************

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
//	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo1, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo2, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo3, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo4, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo5, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_wo_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
//	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);
 
	// cursor f�r Monat 
	// 2 * fetch , f�r vjr und jr
           readcursor_mo = dbClass.sqlcursor (dbRange.SetRange("select "
							"case when a_wo_wa.fil = 0 then a_wo_wa.kun "
							"     when a_wo_wa.fil > 0 then a_wo_wa.fil end kunde, "
							"case when a_wo_wa.fil = 0 then 0 "
							"     when a_wo_wa.fil > 0 then 1 end kun_fil, "
							"a_wo_wa.jr,  "
							"sum(case when a_bas.me_einh <> 2 then a_wo_wa.me_vk_wo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_wo_wa.me_vk_wo  end ) menge, "
							"sum(a_wo_wa.ums_vk_wo), "
							"sum(a_wo_wa.ums_fil_ek_wo) "
							"from a_wo_wa,a_bas,wg where a_wo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_wo_wa.mdn = ? "
							"and a_wo_wa.fil = ? "
							"and a_wo_wa.kun = ? "
							"and a_wo_wa.jr between ? and ? "
							"and a_wo_wa.wo in (?,?,?,?,?) "
							"and <wg.wg> "
							"and <a_wo_wa.a> "
							"and a_wo_wa.me_vk_wo <> 0.0 "
							"group by 1,2,3 "));

**************************/

/****
	dbClass.sqlin ((short *) &wo1, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo2, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo3, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo4, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo5, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo1, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo2, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo3, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo4, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo5, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_wo_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &wochen_index, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);
	// cursor f�r die 4 (oder 5) Wochen
	// x * fetch , f�r jr
           readcursor_woche_jr = dbClass.sqlcursor (dbRange.SetRange("select "
							"case when a_wo_wa.fil = 0 then a_wo_wa.kun "
							"     when a_wo_wa.fil > 0 then a_wo_wa.fil end kunde, "
							"case when a_wo_wa.fil = 0 then 0 "
							"     when a_wo_wa.fil > 0 then 1 end kun_fil, "
							"case when a_wo_wa.wo = ? then 1 "
							"     when a_wo_wa.wo = ? then 2 "
							"     when a_wo_wa.wo = ? then 3 "
							"     when a_wo_wa.wo = ? then 4 "
							"     when a_wo_wa.wo = ? then 5  end index, "
							"a_wo_wa.jr, a_wo_wa.wo, "
							"sum(case when a_bas.me_einh <> 2 then a_wo_wa.me_vk_wo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_wo_wa.me_vk_wo  end ) menge, "
							"sum(a_wo_wa.ums_vk_wo), "
							"sum(a_wo_wa.ums_fil_ek_wo) "
							"from a_wo_wa,a_bas,wg where "
							"a_wo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_wo_wa.mdn = ? "
							"and a_wo_wa.fil = ? "
							"and a_wo_wa.kun = ? "
							"and a_wo_wa.jr = ? "
							"and a_wo_wa.wo in (?,?,?,?,?) "
							"and <wg.wg> "
							"and <a_wo_wa.a> "
							"and a_wo_wa.me_vk_wo <> 0.0 "
							"group by 1,2,3,4,5 order by kun_fil,kunde "
							));
*************/

	dbClass.sqlin ((char *) erster_tag_wo1, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo1, SQLCHAR, 11);
	dbClass.sqlin ((char *) erster_tag_wo2, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo2, SQLCHAR, 11);
	dbClass.sqlin ((char *) erster_tag_wo3, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo3, SQLCHAR, 11);
	dbClass.sqlin ((char *) erster_tag_wo4, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo4, SQLCHAR, 11);
	dbClass.sqlin ((char *) erster_tag_wo5, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo5, SQLCHAR, 11);

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);

	dbClass.sqlin ((char *) erster_tag_wo1, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo5, SQLCHAR, 11);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_tag_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &wochen_index, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	// cursor f�r die 4 (oder 5) Wochen
	// x * fetch , f�r jr
//           readcursor_woche1_vjr = dbClass.sqlcursor (dbRange.SetRange("select "
           readcursor_woche_jr = dbClass.sqlcursor (dbRange.SetRange("select "
							"case when a_tag_wa.fil = 0 then a_tag_wa.kun "
							"     when a_tag_wa.fil > 0 then a_tag_wa.fil end kunde, "
							"case when a_tag_wa.fil = 0 then 0 "
							"     when a_tag_wa.fil > 0 then 1 end kun_fil, "
							"case when dat between ? and ? then 1 "
							"     when dat between ? and ?  then 2 "
							"     when dat between ? and ?  then 3 "
							"     when dat between ? and ?  then 4 "
							"     when dat between ? and ?  then 5  end index, "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag )  end ) menge "
							"from a_tag_wa,a_bas,wg where "
							"a_tag_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_tag_wa.mdn = ? "
							"and a_tag_wa.fil = ? "
							"and a_tag_wa.kun = ? "
							"and a_tag_wa.dat between  ? and ?  "
							"and <wg.wg> "
							"and <a_tag_wa.a> "
							"group by 1,2,3 order by index "
							));

 
//==========================================================================================
//=============================== ubers Vorjahr  ===========================================
//==========================================================================================



/***
	dbClass.sqlin ((short *) &wo1_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo2_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo3_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo4_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo5_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo1_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo2_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo3_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo4_vj, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wo5_vj, SQLSHORT, 0);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_wo_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &wochen_index, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);
	// cursor f�r die 4 (oder 5) Wochen
	// x * fetch , f�r vjr
           readcursor_woche_vjr = dbClass.sqlcursor (dbRange.SetRange("select "
							"case when a_wo_wa.fil = 0 then a_wo_wa.kun "
							"     when a_wo_wa.fil > 0 then a_wo_wa.fil end kunde, "
							"case when a_wo_wa.fil = 0 then 0 "
							"     when a_wo_wa.fil > 0 then 1 end kun_fil, "
							"case when a_wo_wa.wo = ? then 1 "
							"     when a_wo_wa.wo = ? then 2 "
							"     when a_wo_wa.wo = ? then 3 "
							"     when a_wo_wa.wo = ? then 4 "
							"     when a_wo_wa.wo = ? then 5  end index, "
							"a_wo_wa.jr, a_wo_wa.wo, "
							"sum(case when a_bas.me_einh <> 2 then a_wo_wa.me_vk_wo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_wo_wa.me_vk_wo  end ) menge, "
							"sum(a_wo_wa.ums_vk_wo), "
							"sum(a_wo_wa.ums_fil_ek_wo) "
							"from a_wo_wa,a_bas,wg where a_wo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_wo_wa.mdn = ? "
							"and a_wo_wa.fil = ? "
							"and a_wo_wa.kun = ? "
							"and a_wo_wa.jr = ? "
							"and a_wo_wa.wo in (?,?,?,?,?) "
							"and <wg.wg> "
							"and <a_wo_wa.a> "
							"and a_wo_wa.me_vk_wo <> 0.0 "
							"group by 1,2,3,4,5 order by kun_fil,kunde "
							));
***********/

	dbClass.sqlin ((char *) erster_tag_wo1_vj, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo1_vj, SQLCHAR, 11);
	dbClass.sqlin ((char *) erster_tag_wo2_vj, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo2_vj, SQLCHAR, 11);
	dbClass.sqlin ((char *) erster_tag_wo3_vj, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo3_vj, SQLCHAR, 11);
	dbClass.sqlin ((char *) erster_tag_wo4_vj, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo4_vj, SQLCHAR, 11);
	dbClass.sqlin ((char *) erster_tag_wo5_vj, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo5_vj, SQLCHAR, 11);

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.kun, SQLLONG, 0);

	dbClass.sqlin ((char *) erster_tag_wo1_vj, SQLCHAR, 11);
	dbClass.sqlin ((char *) letzter_tag_wo5_vj, SQLCHAR, 11);
    dbRange.RangeIn ("wg.wg",wgbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_tag_wa.a",abereich, SQLSHORT, 0); 

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &wochen_index, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	// cursor f�r die 4 (oder 5) Wochen
	// x * fetch , f�r vjr
//           readcursor_woche1_vvjr = dbClass.sqlcursor (dbRange.SetRange("select "
           readcursor_woche_vjr = dbClass.sqlcursor (dbRange.SetRange("select "
							"case when a_tag_wa.fil = 0 then a_tag_wa.kun "
							"     when a_tag_wa.fil > 0 then a_tag_wa.fil end kunde, "
							"case when a_tag_wa.fil = 0 then 0 "
							"     when a_tag_wa.fil > 0 then 1 end kun_fil, "
							"case when dat between ? and ? then 1 "
							"     when dat between ? and ?  then 2 "
							"     when dat between ? and ?  then 3 "
							"     when dat between ? and ?  then 4 "
							"     when dat between ? and ?  then 5  end index, "
							"sum(case when a_bas.me_einh <> 2 then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag  - a_tag_wa.me_ret_tag) * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then (a_tag_wa.me_vk_tag + a_tag_wa.me_vk_sa_tag - a_tag_wa.me_ret_tag)  end ) menge "
							"from a_tag_wa,a_bas,wg where "
							"a_tag_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_tag_wa.mdn = ? "
							"and a_tag_wa.fil = ? "
							"and a_tag_wa.kun = ? "
							"and a_tag_wa.dat between  ? and ?  "
							"and <wg.wg> "
							"and <a_tag_wa.a> "
							"group by 1,2,3 order by index "
							));





	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);

	fil_abrech_cursor_vj = dbClass.sqlcursor ("select end_dat  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= " 
						"(select end_dat from fil_abrech where mdn = ? and fil = 0 "
						"and period_kz = \"M\" and jr = ? and period = ?) "
						"order by end_dat desc ");




}
