// DialogDatum.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "llmfc.h"
#include "DialogDatum.h"
#include "DbClass.h"
#include "strfkt.h"
#include "datum.h"
#include "dbRange.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
extern DB_CLASS dbClass;
extern DB_RANGE dbRange;
extern short movon ;
extern double avon ;
extern double abis ;
extern short mdn ;
extern short jr ;
extern char filbereich[256];
extern char kunbereich[256];
extern char wgbereich[256];
extern char abereich[256];
extern int MitVorjahr;
extern DATE_STRUCT end_dat;
bool Monatok = FALSE;
int Bereich = 0;

char Textausgabe[500] ;
//char *Bereichtext  = "Bereichseingabe :  Bereich von/bis wird mit Trennzeichen \"-\" eingegeben,                             z.B. Artikel 1 bis 10 als 1-10. Mehrere Bereiche mit \",\" trennen z.B. 1-10,20-30,32,35,40";
char *Bereichtext  = "Bereichseingabe :  Eingabebeispiele : 1-10 ; 1 bis 10 , 1,2,3, <20, >100, kleiner 20, gr��er 100, <> 15, nicht 15 ";
CForm *CDialogDatum::_Dialog [] = 
{
   new CForm (IDC_STATIC_BEREICH, (char *)      Textausgabe, FRMCHAR, 1,""),
   new CForm (IDC_MANDANT, (short *)      &mdn, FRMSHORT, 0,"%hd"),
   new CForm (IDC_FILIALE, (char *)      filbereich, FRMCHAR,1, 128),
   new CForm (IDC_KUNDE, (char *)      &kunbereich, FRMCHAR, 1,128),
   new CForm (IDC_JAHR, (short *)      &jr, FRMSHORT,0, "%hd"),
   new CForm (IDC_WOVON, (short *)      &movon, FRMSHORT,0, "%hd"),
   new CForm (IDC_WGVON, (char *)      &wgbereich, FRMCHAR,1, 128),
   new CForm (IDC_AVON, (char *)      &abereich, FRMCHAR,1, 128),
   NULL,
};

CVector CDialogDatum::Dialog ((void **) _Dialog);


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDialogDatum 


CDialogDatum::CDialogDatum(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogDatum::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}

BOOL CDialogDatum::OnInitDialog()
{
	CDialog::OnInitDialog();
    GetDlgItem (IDC_MANDANT)->SetFocus ();
    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);

	strcpy(filbereich,"1 bis 9999");
	strcpy(kunbereich,"1 bis 99999999");
	strcpy(wgbereich,"1 bis 9999");
	strcpy(abereich,"1 bis 999999");
	Read ();

	return TRUE;
}

void CDialogDatum::EnableDialog (BOOL b)
{


    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->Enable (this, b);
    }

	if (b)
	{
	    GetDlgItem (IDC_MANDANT)->SetFocus ();
	    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
    }
}
void CDialogDatum::ToDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->ToScreen (this);
    }
}


void CDialogDatum::FromDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();

    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->FromScreen (this);
    }
}

void CDialogDatum::GetFormBereich (void)
{
	CWnd *cWnd;
    CForm *Form;

    Dialog.FirstPosition ();

    cWnd = GetFocus ();
	strcpy(Textausgabe,"");
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
	    if (cWnd == GetDlgItem (Form->GetId()))
		{
			if (Form->Bereich == 1)	
			{
				strcpy(Textausgabe,Bereichtext);
			}
			break;
		}
    }
	Read();
}

BOOL CDialogDatum::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
CWnd *cWnd;
char dat[12];
if (pMsg->message == WM_KEYDOWN)
{
	switch (pMsg->wParam)
	{
	case VK_F5 :      //so gehts nicht TODO
			cWnd = GetDlgItem (IDCANCEL);
			return CDialog::PreTranslateMessage(pMsg);
	case VK_RETURN : 
	case VK_TAB :
		    cWnd = GetFocus ();
			if (cWnd ==GetDlgItem (IDC_MANDANT))
			{
			
			    Write ();
				if (jr == 0)
				{
					/**
					dbClass.opendbase ("bws");
					dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
					dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);
					dbClass.sqlout ((short *) &jr, SQLSHORT, 0);

					dbClass.sqlcomm ("select end_dat,jr  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= today + 30 " 
						"order by end_dat desc ");
						*****/

					sysdate(dat);
					jr = get_jahr(dat);
					if (jr < 1900) jr = 2000 + jr;
					Read();
				}

			    Write ();
			    GetDlgItem (IDC_FILIALE)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);

				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_FILIALE))
			{
			    Write ();
				strcpy(Textausgabe,"");
				Read();

			    GetDlgItem (IDC_KUNDE)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_KUNDE))
			{
			    Write ();
			    GetDlgItem (IDC_JAHR)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_JAHR))
			{
			    Write ();
			    GetDlgItem (IDC_WOVON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_WOVON))
			{
			    Write ();
			    GetDlgItem (IDC_WGVON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_WGVON))
			{
			    Write ();
			    GetDlgItem (IDC_AVON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_AVON))
			{
			    Write ();
			    GetDlgItem (IDOK)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDOK))
			{
			    Write ();
			    GetDlgItem (IDC_MANDANT)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}

			if (cWnd ==GetDlgItem (IDCANCEL))
			{
				return CDialog::PreTranslateMessage(pMsg);
			}
			if (cWnd ==GetDlgItem (IDOK))
			{
			    Write ();
				/**
				if (Monatok == FALSE)
                { 
					GetDlgItem (IDC_MANDANT)->SetFocus ();
					((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
					return TRUE;
                }
				**/
				return CDialog::PreTranslateMessage(pMsg);
			}
		    NextDlgCtrl();
		    return TRUE;

	}
}
	return CDialog::PreTranslateMessage(pMsg);
}




BOOL CDialogDatum::Read (void)
{
    ToDialog ();
//    EnableDialog (TRUE);
	return TRUE;
	
}

BOOL CDialogDatum::Write (void)
{

    FromDialog ();

return TRUE;
}

void CDialogDatum::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	char ctext[256];
	Write();
	if (strlen(clipped(filbereich)) > 0 && strlen(clipped(kunbereich)) > 0)
	{
		if (strcmp(clipped(filbereich),"0") != 0 && strcmp(clipped(kunbereich),"0") != 0)
		{

			sprintf (ctext , "Bitte entweder nur Kunden oder nur Filialen ausw�hlen !");
			MessageBox(ctext, "Kilopreisstatistik", MB_OK|MB_ICONSTOP);
			GetDlgItem (IDC_FILIALE)->SetFocus ();
			((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
			return;
		}
    }
	if (strlen(clipped(kunbereich)) == 0)
	{
		strcpy(kunbereich,"0");
	}
	if (strlen(clipped(filbereich)) == 0)
	{
		strcpy(filbereich,"0");
	}

    if (dbRange.PruefeRange(filbereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Filiale", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_FILIALE)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}
    if (dbRange.PruefeRange(kunbereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Kunde", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_KUNDE)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}
    if (dbRange.PruefeRange(wgbereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Warengruppe", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_WGVON)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}
    if (dbRange.PruefeRange(abereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Artikelgruppe", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_AVON)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}


	CDialog::OnOK();
}


void CDialogDatum::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogDatum, CDialog)
	//{{AFX_MSG_MAP(CDialogDatum)
	ON_EN_CHANGE(IDC_WOVON, OnChangeWoVon)
	ON_EN_KILLFOCUS(IDC_WGVON, OnKillfocusWgvon)
	ON_EN_KILLFOCUS(IDC_FILIALE, OnKillfocusFiliale)
	ON_EN_KILLFOCUS(IDC_KUNDE, OnKillfocusKunde)
	ON_EN_SETFOCUS(IDC_FILIALE, OnSetfocusFiliale)
	ON_EN_SETFOCUS(IDC_KUNDE, OnSetfocusKunde)
	ON_EN_SETFOCUS(IDC_WGVON, OnSetfocusWgvon)
	ON_EN_SETFOCUS(IDC_AVON, OnSetfocusAvon)
	ON_EN_KILLFOCUS(IDC_AVON, OnKillfocusAvon)
	ON_EN_SETFOCUS(IDC_MANDANT, OnSetfocusMandant)
	ON_EN_SETFOCUS(IDC_JAHR, OnSetfocusJahr)
	ON_EN_SETFOCUS(IDC_WOVON, OnSetfocusWovon)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDialogDatum 

void CDialogDatum::OnChangeWoVon() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnCloseupCombo1() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnKillfocusWgvon() 
{
	
}
void CDialogDatum::OnKillfocusAvon() 
{
	
}
void CDialogDatum::OnKillfocusKunde() 
{
	
}


void CDialogDatum::OnKillfocusFiliale() 
{
	
}

void CDialogDatum::OnSetfocusFiliale() 
{
	GetFormBereich ();
}
void CDialogDatum::OnSetfocusKunde() 
{
	GetFormBereich ();
}
void CDialogDatum::OnSetfocusWgvon() 
{
	GetFormBereich ();
}
void CDialogDatum::OnSetfocusAvon() 
{
	GetFormBereich ();
}

void CDialogDatum::OnSetfocusMandant() 
{
	GetFormBereich ();
}

void CDialogDatum::OnSetfocusJahr() 
{
	GetFormBereich ();
}

void CDialogDatum::OnSetfocusWovon() 
{
	GetFormBereich ();
}
