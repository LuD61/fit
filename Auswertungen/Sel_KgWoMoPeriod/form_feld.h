#ifndef _FORM_FELD_DEF
#define _FORM_FELD_DEF

struct FORM_FELD {
   char		agg_funk[2];
   char     feld_id[2];
   char     feld_nam[19];
   char     feld_typ[2];
   short    delstatus;
   char     form_nr[6];
   long     lfd;
   char     tab_nam[19];
};

extern struct FORM_FELD form_feld;

#endif