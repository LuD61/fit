#include "stdafx.h"
#include "DbRange.h"
#include "DbClass.h"  
#include "strfkt.h"
#include "Debug.h"

   CDebug Debug;

void DB_RANGE::RangeIn (char *feldname, char *var, int typ, int len)
{
	// var == string f�r where - Bedingung, z.B. "10-15,17,19" soll umgesetzt werden in "(between 10 and 15 or kun in (17,19) )" 
	// typ == FeldTyp : SQLCHAR, SQLLONG ...
	// len == diese Anzahl Char. sollen f�r die zu generierende Bedingung bereitgestellt werden (default 100)


     if (InRangeAnz == MAXRANGEVARS)
     {
         return;
     }

     strcpy(InRange[InRangeAnz].feld, feldname);
     InRange[InRangeAnz].var = var;
     InRange[InRangeAnz].len = len;
     InRange[InRangeAnz].typ = typ;
     InRangeAnz ++;
}

char* DB_RANGE::SetRange (char* SqlBase)
{
	int i;
	int len = 0;

	Debug.Output ("\nSetRange Quelle :\n");
	Debug.Output (SqlBase);
    for (i = 0; i < InRangeAnz; i ++)
	{
		if (InRange[i].len == 0) len += 100; else len += InRange[i].len;
	}

    Sql = (char *) realloc (Sql,strlen(SqlBase) + len);
	if (Sql == NULL) 
	{
		Debug.Output (" Fehler realloc Sql  ");
		return "";
	}
	strcpy(Sql,SqlBase);
    for (i = 0; i < InRangeAnz; i ++)
	{
		sprintf(FindString,"<%s>",InRange[i].feld);
		Debug.Output ("\nFindString : ");
		Debug.Output (FindString);
		Debug.Output (" Eingabe : ");
		Debug.Output (InRange[i].var);
		ReplaceRange(i);
		Debug.Output ("\nReplace : ");
		Debug.Output (cReplace);
		Sql = Replace(Sql, FindString, cReplace,FALSE);
	}
	Debug.Output ("\nSetRange Ziel :\n");
	Debug.Output (Sql);

	InRangeAnz = 0;
	return (char*)Sql;
}
BOOL DB_RANGE::PruefeRange (char* input)
{
	char cvon[30] ;
	char cbis[30];
	int cha = ',';
	int chgreater = '>';
	int chsmaller = '<';
	int chequal = '=';
	BOOL zahl;

	strcpy(cvon,"");
	strcpy(cbis,"");
	cRange = (char *) realloc (cRange,256);
	if (cRange == NULL)
	{
		Debug.Output (" Fehler malloc cRange  ");
		return FALSE;
	}
	strcpy(cRange, input);
		cRange = Replace(cRange, "kleiner", "<",TRUE);
		cRange = Replace(cRange, "gr��er", ">",TRUE);
		cRange = Replace(cRange, "groe�er", ">",TRUE);
		cRange = Replace(cRange, "bis", "-",TRUE);
		cRange = Replace(cRange, "und", ",",TRUE);
		cRange = Replace(cRange, "nicht", "<>",TRUE);
	cRange = Replace(cRange, ";", ",",TRUE);
	cRange = Replace(cRange, "_", "-",TRUE);
	cRange = Replace(cRange, "--", "-",TRUE);
	cRange = Replace(cRange, ",,", ",",TRUE);
	cRange = Replace(cRange, "--", "-",TRUE);
	cRange = Replace(cRange, ",,", ",",TRUE);
	cRange = Replace(cRange, " ", "",TRUE);
	clipped(cRange);
    for (int i = 0; i < strlen(cRange); i ++)
	{
		zahl = FALSE;
		if (memcmp(&cRange[i],"0",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"1",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"2",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"3",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"4",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"5",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"6",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"7",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"8",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"9",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"-",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],",",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],">",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i],"<",1) == 0) zahl = TRUE ;
		if (memcmp(&cRange[i]," ",1) == 0) zahl = TRUE ;
		if (zahl == FALSE) return FALSE;
	}
	return TRUE;
}

BOOL DB_RANGE::ReplaceRange (int i)
{
	char *pdest;
	int result;
	char cvon[30] ;
	char cbis[30];
	char gleich[2];
	char cinhalt[100];
	int cha = ',';
	int chgreater = '>';
	int chsmaller = '<';
	int chequal = '=';
	int y;

	strcpy(cvon,"");
	strcpy(cbis,"");
//    if (cRange == (char *) 0) cRange = (char *) malloc (256);
	cRange = (char *) realloc (cRange,256);
	if (cRange == NULL)
	{
		Debug.Output (" Fehler malloc cRange  ");
		return FALSE;
	}
	strcpy(cRange, InRange[i].var);

	if (InRange[i].typ != SQLCHAR)
	{
		cRange = Replace(cRange, "kleiner", "<",TRUE);
		cRange = Replace(cRange, "gr��er", ">",TRUE);
		cRange = Replace(cRange, "groe�er", ">",TRUE);
		cRange = Replace(cRange, "bis", "-",TRUE);
		cRange = Replace(cRange, "und", ",",TRUE);
		cRange = Replace(cRange, "nicht", "<>",TRUE);
	}
	cRange = Replace(cRange, ";", ",",TRUE);
	cRange = Replace(cRange, "_", "-",TRUE);
	cRange = Replace(cRange, "--", "-",TRUE);
	cRange = Replace(cRange, ",,", ",",TRUE);
	cRange = Replace(cRange, "--", "-",TRUE);
	cRange = Replace(cRange, ",,", ",",TRUE);
	cRange = Replace(cRange, " ", "",TRUE);
	if (InRange[i].len == 0) InRange[i].len = 100;
    cdest = (char *) realloc (cdest,InRange[i].len);
	if (cdest == NULL)
	{
		Debug.Output (" Fehler malloc cdest  ");
		return FALSE;
	}
	strcpy(cdest,"");

    pdest = strchr( cRange, cha );
	strcpy(gleich,"");
    if( pdest == NULL )
    {
		if (InRange[i].typ == SQLCHAR)
		{
				sprintf(cdest," %s matches \"*%s*\" ",InRange[i].feld,cRange); 
		}
		else
		{
			cha = '-';
			pdest = strchr( cRange, cha );
			if( pdest == NULL )
			{
			    if (strchr( cRange, chgreater ) == NULL  && strchr( cRange, chsmaller ) == NULL && strchr( cRange, chequal ) == NULL) strcpy(gleich,"=") ;
				sprintf(cdest,"%s %s %s ",InRange[i].feld,gleich,cRange); 
			}
			else
			{
			    result = pdest - cRange + 1;
				strcpy(cvon,"                            ");
				strncpy(cvon,&cRange[0], result - 1);
				strcat(cbis,&cRange[result]);
				sprintf(cdest," %s between %s and %s ",InRange[i].feld,clipped(cvon),clipped(cbis));
			}
		}
	}
	else
	{
	    result = pdest - cRange + 1;
		strncat(cvon,&cRange[0], result - 1);
		if (strchr( cvon, chgreater ) != NULL  && strchr( cvon, chsmaller ) != NULL) // hier <> ,daher mit and verkn�pfen
		{
		    strcpy(InsRange_and[InsRange_andAnz].var, clipped(cvon));
			InsRange_andAnz ++;
		}
		else
		{
		    strcpy(InsRange[InsRangeAnz].var, clipped(cvon));
			InsRangeAnz ++;
		}

		int pos = result;
		while (TRUE)
		{
		    pdest = strchr( &cRange[pos], cha );
		    result = pdest - &cRange[pos] + 1;
			if (pdest == NULL)
			{
				strcpy(cbis,&cRange[pos]);
				if (strchr( cbis, chgreater ) != NULL  && strchr( cbis, chsmaller ) != NULL)   // hier <> ,daher mit and verkn�pfen
				{
					strcpy(InsRange_and[InsRange_andAnz].var, clipped(cbis));
					InsRange_andAnz ++;
				}
				else
				{
					strcpy(InsRange[InsRangeAnz].var, clipped(cbis));
					InsRangeAnz ++;
				}
				break;
			}

			strcpy(cvon,"                            ");
			strncpy(cvon,&cRange[pos], result - 1);
			if (strchr( cvon, chgreater ) != NULL  && strchr( cvon, chsmaller ) != NULL)  // hier <> ,daher mit and verkn�pfen
			{
			    strcpy(InsRange_and[InsRange_andAnz].var, clipped(cvon));
				InsRange_andAnz ++;
			}
			else
			{
			    strcpy(InsRange[InsRangeAnz].var, clipped(cvon));
				InsRangeAnz ++;
			}
			pos += result;
		}
		if (InsRangeAnz > 0) strcpy(cdest," (");
	    for (y = 0; y < InsRangeAnz ; y ++)
		{
			strcpy(cRange,InsRange[y].var);
			if (InRange[i].typ == SQLCHAR)
			{
					if (strlen(clipped(cdest)) < 3) sprintf(cinhalt," %s matches \"*%s*\" ",InRange[i].feld,cRange); 
					else  sprintf(cinhalt," or %s matches \"*%s*\" ",InRange[i].feld,cRange); 
			}
			else
			{
				cha = '-';
				pdest = strchr( cRange, cha );
				if( pdest == NULL )
				{
					strcpy(gleich,"");
					if (strchr( cRange, chgreater ) == NULL  && strchr( cRange, chsmaller ) == NULL && strchr( cRange, chequal ) == NULL) strcpy(gleich,"=") ;
					if (strlen(clipped(cdest)) < 3) sprintf(cinhalt," %s %s %s ",InRange[i].feld,gleich,cRange); 
					else  sprintf(cinhalt," or %s %s %s ",InRange[i].feld,gleich,cRange); 
				}   
				else
				{
					result = pdest - cRange + 1;
					strcpy(cvon,"                            ");
					strncpy(cvon,&cRange[0], result - 1);
					strcpy(cbis,&cRange[result]);
					if (strlen(clipped(cdest)) < 3) sprintf(cinhalt," %s between %s and %s ",InRange[i].feld,clipped(cvon),clipped(cbis));
					else sprintf(cinhalt," or %s between %s and %s ",InRange[i].feld,clipped(cvon),clipped(cbis));
				}
			}
			strcat (cdest,cinhalt);
		}
		if (InsRangeAnz > 0)  strcat(cdest,")");


		if (InsRangeAnz == 0 && InsRange_andAnz > 0) strcpy(cdest," (");
	    for (y = 0; y < InsRange_andAnz ; y ++)
		{
			strcpy(cRange,InsRange_and[y].var);
			if (InRange[i].typ == SQLCHAR)
			{
					if (strlen(clipped(cdest)) < 3) sprintf(cinhalt," %s matches \"*%s*\" ",InRange[i].feld,cRange); 
					else  sprintf(cinhalt," and %s matches \"*%s*\" ",InRange[i].feld,cRange); 
			}
			else
			{
				cha = '-';
				pdest = strchr( cRange, cha );
				if( pdest == NULL )
				{
					strcpy(gleich,"");
					if (strchr( cRange, chgreater ) == NULL  && strchr( cRange, chsmaller ) == NULL && strchr( cRange, chequal ) == NULL) strcpy(gleich,"=") ;
					if (strlen(clipped(cdest)) < 3) sprintf(cinhalt," %s %s %s ",InRange[i].feld,gleich,cRange); 
					else  sprintf(cinhalt," and %s %s %s ",InRange[i].feld,gleich,cRange); 
				}   
				else
				{
					result = pdest - cRange + 1;
					strcpy(cvon,"                            ");
					strncpy(cvon,&cRange[0], result - 1);
					strcpy(cbis,&cRange[result]);
					if (strlen(clipped(cdest)) < 3) sprintf(cinhalt," %s between %s and %s ",InRange[i].feld,clipped(cvon),clipped(cbis));
					else sprintf(cinhalt," and %s between %s and %s ",InRange[i].feld,clipped(cvon),clipped(cbis));
				}
			}
			strcat (cdest,cinhalt);
		}


	}


	strcpy(cReplace,cdest);
	InsRange_andAnz = 0;
	InsRangeAnz = 0;
	return (TRUE);
}








char* DB_RANGE::Replace(char* strBase, const std::string to_find, const std::string to_replace, BOOL all)
{
/**
*	a, b: a <= "string gefunden" <= b
*	i, j: Z�hler f�r 'strBase' und 'to_find'
*	k: markiert den Anfang eines Suchdurchgangs
*	count: Anzahl der zutreffenden Zeichen
* end_string: replaced string
**/

    BOOL gefunden = FALSE;	
	char* end_string;
	unsigned long int a, b, i, j, k, count;
	std::string final;
	a = b = i = j = k = count = 0;
	
	if (to_find.length() > strlen(strBase))
	return (char*) strBase;

	while (*(strBase + i) != '\0')
	{
		if (*(strBase + i) == to_find[j] && gefunden == FALSE)
		{
			a = i;
			++i; ++j; ++count;
			while (*(strBase + i) == to_find[j] && *(strBase + i) != NULL)
			{
				b = i++; 
				++j; ++count;
			}
			if (count == to_find.length())
			{
			// kopiere alles von 'k' bis zum Anfang der gefundenen Zeichenfolge
				while (k < a) final += *(strBase + k++);
				final += to_replace.c_str();	 // kopiere den replace-string
				if (all == FALSE) gefunden = TRUE;
			}
			else
			{
			// kopiere alles von 'k' bis zum letzen �berpr�ften Zeichen *verwirr*
				while (k <= b) final += *(strBase + k++);
			}
			k = b + 1;	 // k: Ende des gefundenen Strings -> Anfang f�r neue Suche
			b = i;	 // b muss mit aktuellem Suchindex mitlaufen
			count = j = 0;
		}
		else
		{
			final += *(strBase + i);
			k = b = ++i;	 // k und b m�ssen mit aktuellem Index mitlaufen
		}
	}

	end_string = new char[final.length() + 1];
	strcpy(end_string, final.c_str());
	return end_string;
}



