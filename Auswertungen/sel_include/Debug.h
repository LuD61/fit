#ifndef _DEBUG_DEF 
#define _DEBUG_DEF
#include <windows.h>
#include <stdio.h>
#include "Text.h"

class CDebug
{
public :
	Text LogName;
	Text ProgName;
	Text DebugFile;
	FILE *fdebug;
	BOOL dbMode;

	CDebug ();
	CDebug (Text&, Text&);

    void CreateLogfile ();
    void CreateDebugFile ();
	void Output (Text&);
	void Output (LPSTR);
};
#endif 