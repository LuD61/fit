#ifndef _FORM_DEF
#define _FORM_DEF
#include <odbcinst.h>

#define FRMCHAR 0
#define FRMSHORT 1
#define FRMLONG 2
#define FRMDOUBLE 3
#define FRMDATE 4
#define FRMBOOL 5
#define FRMCHARBOOL 6
#define FRMDATUM 7

#define LISTBOX 1
#define COMBOBOX 2
#define COMBOLIST 3


class CForm
{
   protected :
       int Id;
       void *VarDB;
       int  Type;
       char *Picture;
       int VarLen;
       int ListboxType;
       char *DbItem;
	   char texttext[256];
		int jrhstart ;
		int jrh1 ;
		int jrh2 ;
   public :
	   int Bereich;
       CForm ();
       CForm (int Id, void *VarDB, int Type,int Bereich, int VarLen);
       CForm (int Id, void *VarDB, int Type,int Bereich, char *Picture);
       ~CForm ();

       void SetItem (int Id, void *VarDB, int Type,int Bereich, char *Picture);
	
       int GetId (void);
       void *GetVarDB (void);
       int GetType (void);
       void DatFormat (void);
       void SetListboxType (int);
       int GetListboxType (void);
       void SetDbItem (char *);
       char * GetDbItem (void);

       void Enable (CWnd *, BOOL);
       CString& ToString (CString&);
       void FromString (CString&);
       CString& DateToString (DATE_STRUCT *);
       void StringToDate (char *, DATE_STRUCT *);
       void TestDate ();

       virtual void ToScreen (CWnd *);
       virtual void FromScreen (CWnd *);
//       virtual void FillListBox (CWnd *, char *) {}
//       virtual void FillListBox (CWnd *, char **){}
//       virtual void FillPtBox (void *, CWnd *, char *){}
//       virtual void FillPtBox (void *, CWnd *){}
//       virtual void FillPtBoxLong (void *, CWnd *, char *){}
//       virtual void FillPtBoxLong (void *, CWnd *){}
static BOOL IsMon31 (int mon)
/**
Monat im Datum pruefen.
**/
{
         static int mon31[] = {1,3,5,7,8,10,12};
         int i;

         for (i = 0; i < 7; i ++)
         {
                      if (mon == mon31[i]) return TRUE;
         }
         return FALSE;
}

static  IsMon30 (int mon)
/**
Monat im Datum pruefen.
**/
{
         static int mon30[] = {4,6,9,11};
         int i;

         for (i = 0; i < 4; i ++)
         {
                      if (mon == mon30[i]) return TRUE;
         }
         return FALSE;
}

static  IsMon29 (int mon, int jr)
/**
Monat im Datum pruefen.
**/
{
         static int sjr = 1954;
         int jrdiff; 

         if (mon != 2) return FALSE; 

         jrdiff = jr - sjr;
         if (jr % 4 == 0) return TRUE; 
         return FALSE;
}
 

static  IsMon28 (int mon, int jr)
/**
Monat im Datum pruefen.
**/
{
         static int sjr = 1954;
         int jrdiff; 

         if (mon != 2) return FALSE; 

         jrdiff = jr - sjr;
         if (jr % 4) return TRUE; 
         return FALSE;
}

};

class CListForm : virtual public CForm
{
   protected :
        CListBox *lb; 
        CComboBox *cb; 
       
   public :
       CListForm (int Id, void *VarDB, int Type, int VarLen, int ListboxType);
       CListForm (int Id, void *VarDB, int Type, char *Picture, int ListboxType);
       CListForm (int Id, void *VarDB, int Type, int VarLen, int ListboxType, char *DbItem);
       CListForm (int Id, void *VarDB, int Type, char *Picture, int ListboxType, char *DbItem);

       void FillListBox (CWnd *, char *);
       void FillListBox (CWnd *, char **);
       void FillPtBox (void *, CWnd *, char *);
       void FillPtBox (void *, CWnd *);
       void FillPtBoxLong (void *, CWnd *, char *);
       void FillPtBoxLong (void *, CWnd *);
       void GetText (CWnd *, int, CString&);
       void ToScreen (CWnd *);
       void FromScreen (CWnd *);
};



#endif