//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
// Diese Applikation z.Z. nur f�r Vorselektion Umsatz Wg/Wo (Tabelle a_mo_wa wird ausgelesen)
// Erstellung : 11.02.2008  LuD

//=============================================================================

// mainfrm.cpp : implementation of the CMainFrame class

#include "stdafx.h"
#include "llmfc.h"
#include "mainfrm.h"
#include "wmask.h"
#include "DbClass.h"
#include "a_wo_wa.h"
#include "form_feld.h"
#include "systables.h"
#include "syscolumns.h"
#include "Token.h"
#include "DialogDatum.h"

#include "cmbtl9.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GR: Waehlen Sie hier die List & Label Sprache f�r den Designer und die
//     Dialoge aus :
//    (Die entsprechenden Sprachkonstanten entnehmen Sie der Datei cmbtl9.h)

// US: choose your LuL-Language for all designer, dialogs, etc... here:
//    (see cmbtl9.h for other language-constants)

const int LUL_LANGUAGE = CMBTLANG_GERMAN;

//LuD...
//const char *FILENAME_DEFAULT = "c:\\temp\\rezeptur.lst";  // Zeiger auf eine Konstante
const char *FILENAME_DEFAULT = "c:\\user\\fit\\format\\LL\\Produktion\\rezaufl.lst                               ";  // Zeiger auf eine Konstante
bool cfgOK;
char ListennameLaengs[128];
char ListennameQuer[128];
char ListennameLang[128];
int Listenauswahl = 0;   // wenn 1 : Auswahl mehrerer Reports
int Hauptmenue = 0;   // wenn 1 : Auswahl edit oder print �ber Menue
int Divisor = 1;
char Format [30];
extern short mdn ;
extern short filvon ;
extern short filbis ;
int MitVorjahr = 0;
int MaxAnzahlMonate = 12;
extern short jr ;
extern short vjr ;
extern short movon ;
extern short mobis ;
extern char wg_bz1[25];
extern double menge[];
extern double menge_vj[];
extern short a_bas_me_einh;
extern short a_bas_lief_einh;
extern double a_bas_a_gew;
extern char artikelbezeichnung[26];
extern char rez_preis_bz[12];
extern double rez_preis;
extern char a_bz1[25];
extern char einh_bz[9];
char cdatumvon[12];
char cdatumbis[12];
long idat = 0;

        static char *Monate [] = {"",
                                  "Januar",
                                  "Februar",
                                  "M�rz",
                                  "April",
                                  "Mai",
                                  "Juni",
                                  "Juli",
                                  "August",
                                  "September",
                                  "Oktober",
                                  "November",
                                  "Dezember",
                                   NULL,
                                   NULL,
        };


  DB_CLASS dbClass;
  A_WO_WA_CLASS AWoWa;
  FORM_FELD form_feld;
  SYSCOLUMNS syscolumns;
  SYSTABLES systables;

HWND hMainWindow;

int form_feld_curs, tabid_curs, coltyp_curs,  dynacurs ; 
/////////////////////////////////////////////////////////////////////////////
// CMainFrame

// GR: Registrieren eines Nachrichtenkanals f�r List & Label Callbacks
// US: register LuL MessageBase:
UINT CMainFrame::m_uLuLMessageBase = RegisterWindowMessage("cmbtLLMessage");

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_START_DEBUG, OnStartDebug)
	ON_COMMAND(ID_FILE_STOP_DEBUG, OnStopDebug)
	ON_COMMAND(ID_EDIT_LABEL, OnEditLabel)
	ON_COMMAND(ID_EDIT_LIST, OnEditList)
	ON_UPDATE_COMMAND_UI(ID_FILE_START_DEBUG, OnUpdateStartDebug)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP_DEBUG, OnUpdateStopDebug)
	ON_COMMAND(ID_PRINT_LABEL, OnPrintLabel)
	ON_COMMAND(ID_PRINT_REPORT, OnPrintReport)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(m_uLuLMessageBase,OnLulMessage)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_bDebug = FALSE;
    ProgCfg = new PROG_CFG ("sel_umsatzwgwo");

}

CMainFrame::~CMainFrame()
{
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }

}


/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	
	LlAddCtlSupport(m_hWnd,
					LL_CTL_ADDTOSYSMENU | LL_CTL_CONVERTCONTROLS | LL_CTL_ALSOCHILDREN,
					"combit.ini");
	GetCfgValues();
	
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_PRINT_REPORT,0l);
	}
		
return 0;
}

void CMainFrame::OnStartDebug()
{
	MessageBox("Make sure that DEBWIN2 had been started before this demo application. "
				"If this doesn't happen you won't see any debug outputs now!",
				"List & Label Sample App", MB_OK | MB_ICONINFORMATION);
//	LlSetDebug(LL_DEBUG_CMBTLL);
	m_bDebug = TRUE;

}

void CMainFrame::OnStopDebug()
{
	LlSetDebug(FALSE);
	m_bDebug = FALSE;
}

void CMainFrame::OnUpdateStopDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bDebug);
}

void CMainFrame::OnUpdateStartDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bDebug);
}


//=============================================================================
void CMainFrame::OnEditLabel()
//=============================================================================
{
	CHAR szFilename[128+1] = "*.lbl";
	HWND hWnd = m_hWnd;
	HJOB hJob;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	// US: initialize Job
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

	// GR: Setzen der List & Label Optionen
	// US: Setting the List & Label options
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	//GR: Exporter aktivieren
	//US: Enable exporter
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
/* ---> GrJ GrJ
	if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
    {
        LlJobClose(hJob);
        return;
    }
< ---- */

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	// GR: Aufruf des Designers

   	sprintf ( szFilename, FILENAME_DEFAULT );

 	if (LlDefineLayout(hJob, hWnd, "Designer", LL_PROJECT_LABEL, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
}

long holetyp(char* tab_nam , char* feld_nam)
{
	static char alttabnam[20];
	static long alttabid ;
	if ( strncmp ( tab_nam, alttabnam, strlen(tab_nam)) == 0 )
	{
		// bereits gefunden
	}
	else
	{
		sprintf ( systables.tabname, "%s" , tab_nam );
		dbClass.sqlopen (tabid_curs);	// destroyed den Cursor, sqlopen refreshed
		if (!dbClass.sqlfetch(tabid_curs))	// gefunden
		{
			alttabid = systables.tabid ;
			sprintf ( alttabnam, "%s", tab_nam );
		}
		else

			return LL_TEXT ;

	}
 	sprintf ( syscolumns.colname, "%s" , feld_nam );
 	syscolumns.tabid = alttabid ;
	dbClass.sqlopen (coltyp_curs);	// destroyed den Cursor, sqlopen refreshed
	if (!dbClass.sqlfetch(coltyp_curs))	// gefunden
	{
		switch (syscolumns.coltype)
		{
		case(iDBCHAR):
			return LL_TEXT ;
		case(iDBSMALLINT):	// smallint
		case(iDBINTEGER):	// integer
		case(iDBDECIMAL):	// decimal
			return LL_NUMERIC ;
		case(iDBDATUM):	// Datumsfeld
			return LL_DATE ;
		}
		return LL_TEXT ;
	}
	else
		return LL_TEXT ;
		
}


int GetRecCount()
{
	short AnzMonate = mobis - movon + 1;
	sprintf(ausgabe.mo1,"%hd",movon);
	sprintf(ausgabe.mo2,"%hd",movon + 1);
	sprintf(ausgabe.mo3,"%hd",movon + 2);
	sprintf(ausgabe.mo4,"%hd",movon + 3);
	sprintf(ausgabe.mo5,"%hd",movon + 4);
	sprintf(ausgabe.mo6,"%hd",movon + 5);
	sprintf(ausgabe.mo7,"%hd",movon + 6);
	sprintf(ausgabe.mo8,"%hd",movon + 7);
	sprintf(ausgabe.mo9,"%hd",movon + 8);
	sprintf(ausgabe.mo10,"%hd",movon + 9);
	sprintf(ausgabe.mo11,"%hd",movon + 10);
	sprintf(ausgabe.mo12,"%hd",movon + 11);
	sprintf(ausgabe.mo13,"%hd",movon + 12);

	if (AnzMonate == 12) strcpy(ausgabe.mo13, "Summe");  
	if (AnzMonate == 11) strcpy(ausgabe.mo12, "Summe");  
	if (AnzMonate == 10) strcpy(ausgabe.mo11, "Summe");  
	if (AnzMonate == 9) strcpy(ausgabe.mo10, "Summe");  
	if (AnzMonate == 8) strcpy(ausgabe.mo9, "Summe");  
	if (AnzMonate == 7) strcpy(ausgabe.mo8, "Summe");  
	if (AnzMonate == 6) strcpy(ausgabe.mo7, "Summe");  
	if (AnzMonate == 5) strcpy(ausgabe.mo6, "Summe");  
	if (AnzMonate == 4) strcpy(ausgabe.mo5, "Summe"); 
	if (AnzMonate == 3) strcpy(ausgabe.mo4, "Summe");  
	if (AnzMonate == 2) strcpy(ausgabe.mo3, "Summe");  
	if (AnzMonate == 1) strcpy(ausgabe.mo2, "Summe"); 
	return AWoWa.dbcount ();

}


void Felderdefinition (HJOB hJob)
{
    LlDefineFieldExt(hJob, "wg.wg",        "22", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "wg.wg_bz1", "WG-Bezeichnung", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat1.mo", "Januar", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat2.mo", "Februar", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat3.mo", "M�rz", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat4.mo", "April", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat5.mo", "Mai", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat6.mo", "Juni", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat7.mo", "Juli", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat8.mo", "August", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat9.mo", "Sept.", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat10.mo", "Okt.", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat11.mo", "Nov.", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat12.mo", "Dez.", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat13.mo", "Summe", LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat1.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat2.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat3.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat4.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat5.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat6.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat7.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat8.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat9.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat10.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat11.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat12.me_vk_mo",        "12.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "monat13.me_vk_mo",        "12.12", LL_NUMERIC, NULL);


		LlDefineFieldExt(hJob, "monat1.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat2.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat3.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat4.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat5.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat6.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat7.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat8.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat9.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat10.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat11.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat12.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);
		LlDefineFieldExt(hJob, "monat13.me_vk_mo_vj",        "12.12", LL_NUMERIC, NULL);


	LlDefineFieldExt(hJob, "ausgabe.hirarchie1",        "1", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.hirarchie2",        "2", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.hirarchie3",        "3", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.hirarchie4",        "4", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.h1_bez",        "hierachie1", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.h2_bez",        "hierachie2", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.h3_bez",        "hierachie3", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.h4_bez",        "hierachie4", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.kun",        "4711", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.kun_krz1",        "Testkunde", LL_TEXT, NULL);
}

void Variablendefinition (HJOB hJob)
{
		LlDefineVariableExt(hJob, "Mandant",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Vorjahr",        "2007", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Vorjahr_u",        "2007", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Jahr",        "2008", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "FilVon",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "FilBis",        "999", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "MoVon",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "MoBis",        "12", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Wochenzahl",        "12", LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "Hierarchie1Von",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie1Bis",        "12", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie2Von",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie2Bis",        "12", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie3Von",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie3Bis",        "12", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie4Von",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie4Bis",        "12", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "KundeVon",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "KundeBis",        "999999", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Divisor",        "1", LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "Format",        "-------&", LL_TEXT, NULL);
}

void VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{
		sprintf(szTemp2, "%d", Divisor );
		LlDefineVariableExt(hJob, "Divisor",        szTemp2, LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "Format",        Format, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", mdn );
		LlDefineVariableExt(hJob, "Mandant",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", jr );
		LlDefineVariableExt(hJob, "Jahr",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", vjr );
		LlDefineVariableExt(hJob, "Vorjahr",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", vjr );
	    if (MitVorjahr == 2)
		{
			LlDefineVariableExt(hJob, "Vorjahr_u",        szTemp2, LL_TEXT, NULL);
		}
		else
		{
			LlDefineVariableExt(hJob, "Vorjahr_u",        "0", LL_TEXT, NULL);
		}
		sprintf(szTemp2, "%d", filvon );
		LlDefineVariableExt(hJob, "FilVon",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", filbis );
		LlDefineVariableExt(hJob, "FilBis",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", movon );
		LlDefineVariableExt(hJob, "MoVon",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", mobis );
		LlDefineVariableExt(hJob, "MoBis",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", mobis - movon + 1 );
		LlDefineVariableExt(hJob, "Wochenzahl",        szTemp2, LL_NUMERIC, NULL);

		LlDefineVariableExt(hJob, "Hierarchie1Von",        "0", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie1Bis",        "0", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie2Von",        "0", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie2Bis",        "0", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie3Von",        "0", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie3Bis",        "0", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie4Von",        "0", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Hierarchie4Bis",        "0", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "KundeVon",        "0", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "KundeBis",        "0", LL_TEXT, NULL);
}

void FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{
		sprintf(szTemp2, "%d", ausgabe.wg );
    LlDefineFieldExt(hJob, "wg.wg",        szTemp2, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "wg.wg_bz1", wg_bz1, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat1.mo", ausgabe.mo1, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat2.mo", ausgabe.mo2, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat3.mo", ausgabe.mo3, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat4.mo", ausgabe.mo4, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat5.mo", ausgabe.mo5, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat6.mo", ausgabe.mo6, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat7.mo", ausgabe.mo7, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat8.mo", ausgabe.mo8, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat9.mo", ausgabe.mo9, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat10.mo", ausgabe.mo10, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat11.mo", ausgabe.mo11, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat12.mo", ausgabe.mo12, LL_TEXT, NULL);
    LlDefineFieldExt(hJob, "monat13.mo", ausgabe.mo13, LL_TEXT, NULL);
		sprintf(szTemp2, "%.2f", menge[0] );
    LlDefineFieldExt(hJob, "monat1.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[1] );
    LlDefineFieldExt(hJob, "monat2.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[2] );
    LlDefineFieldExt(hJob, "monat3.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[3] );
    LlDefineFieldExt(hJob, "monat4.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[4] );
    LlDefineFieldExt(hJob, "monat5.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[5] );
    LlDefineFieldExt(hJob, "monat6.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[6] );
    LlDefineFieldExt(hJob, "monat7.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[7] );
    LlDefineFieldExt(hJob, "monat8.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[8] );
    LlDefineFieldExt(hJob, "monat9.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[9] );
    LlDefineFieldExt(hJob, "monat10.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[10] );
    LlDefineFieldExt(hJob, "monat11.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[11] );
    LlDefineFieldExt(hJob, "monat12.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2f", menge[12] );
    LlDefineFieldExt(hJob, "monat13.me_vk_mo",        szTemp2, LL_NUMERIC, NULL);
	

	if (MitVorjahr)
	{
			sprintf(szTemp2, "%.2f", menge_vj[0] );
	    LlDefineFieldExt(hJob, "monat1.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[1] );
		LlDefineFieldExt(hJob, "monat2.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[2] );
	    LlDefineFieldExt(hJob, "monat3.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[3] );
		LlDefineFieldExt(hJob, "monat4.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[4] );
	    LlDefineFieldExt(hJob, "monat5.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[5] );
		LlDefineFieldExt(hJob, "monat6.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[6] );
	    LlDefineFieldExt(hJob, "monat7.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[7] );
		LlDefineFieldExt(hJob, "monat8.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[8] );
	    LlDefineFieldExt(hJob, "monat9.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[9] );
		LlDefineFieldExt(hJob, "monat10.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[10] );
	    LlDefineFieldExt(hJob, "monat11.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[11] );
		LlDefineFieldExt(hJob, "monat12.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
			sprintf(szTemp2, "%.2f", menge_vj[12] );
	    LlDefineFieldExt(hJob, "monat13.me_vk_mo_vj",        szTemp2, LL_NUMERIC, NULL);
	}
	
	
	LlDefineFieldExt(hJob, "ausgabe.hirarchie1",        "0", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.hirarchie2",        "0", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.hirarchie3",        "0", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.hirarchie4",        "0", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.h1_bez",        "", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.h2_bez",        "", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.h3_bez",        "", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.h4_bez",        "", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.kun",        "0", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.kun_krz1",        "", LL_TEXT, NULL);
}


//=============================================================================
void CMainFrame::OnEditList()
//=============================================================================
{
	CHAR szFilename[128+1] = "*.lst";
	HWND hWnd = m_hWnd;
	HJOB hJob;
    char *etc;
	char filename[512];

	etc = getenv ("BWS");
    sprintf (filename, "%s\\format\\LL\\%s.lst", etc, ListennameLang);
	FILENAME_DEFAULT = filename;
	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden (Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");


	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");


	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
		{
	        LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    // GR: Zur�cksetzen der internen Feld-Puffer
    LlDefineFieldStart(hJob);

//	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

	if (Listenauswahl != 1)  
    {
	   	sprintf ( szFilename, FILENAME_DEFAULT );
	}
	// GR: Aufruf des Designers
    if (LlDefineLayout(hJob, hWnd, "Design Rezeptur", LL_PROJECT_LIST, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
}


//=============================================================================
void CMainFrame::OnPrintLabel()
//=============================================================================
{
	DoLabelPrint();
}


//=============================================================================
void CMainFrame::OnPrintReport()
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");
	movon = 1;
    mobis = 12;
    char *p;
    Token.NextToken ();
	while ((p = Token.NextToken ()) != NULL)
	{
		if (strcmp (p, "-h") == 0)
		{
              MessageBox ("Aufruf : sel_umsatzwgwo -mdn Mandant -datvon Datum-von -datbis Datum-bis ",
                                "",
                                MB_OK);
              return;
		}
		if (strcmp (p, "-mdn") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            mdn = (atoi (p));
		}

	}
    CDialogDatum dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber OK zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		return;
	}

	DoListPrint();
}

//=============================================================================
void CMainFrame::DoLabelPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lbl", szTemp[30], szTemp2[40], szBoxText[200];
	HJOB hJob;
	int  nRet = 0;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// US: Choose new expression mode with formula support
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);
	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog

   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
   		{
	    	if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    for (int i=1; i<10; i++)
	{
		sprintf(szTemp, "Field%d", i);
    	LlDefineVariable(hJob, szTemp, szTemp);
   	}

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

    // GR: Druck starten

	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_BRIDGEMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

	// GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	LlPrintSetOption(hJob, LL_PRNOPT_OFFSET, 0);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
        return;
    }


	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

	char szPrinter[80], szPort[20];
	int  nRecCount = 30, nErrorValue = 0, nRecno, nLastPage;

	LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

    nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

	// GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	while (nRecno < nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& nErrorValue == 0)
	{
		for (i=1; i<10; i++)
		{
			sprintf(szTemp, "Field%d", i);
			if (LlPrintIsVariableUsed(hJob, szTemp))
			{
				sprintf(szTemp2, "contents of Field%d, record %d", i, nRecno);
    			LlDefineVariable(hJob, szTemp, szTemp2);
			}
    	}

    	// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    	sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    	nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        if (nErrorValue == LL_ERR_USER_ABORTED)
    	{
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
    		return;
    	}

    	// GR: Variablen drucken
    	nErrorValue = LlPrint(hJob);

		// GR: gehe zum naechsten Datensatz
    	nRecno++;
	}

	//GR: Druck beenden
	LlPrintEnd(hJob,0);


	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) l�schen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }

	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
}

//=============================================================================
void CMainFrame::DoListPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lst", szTemp[50], szTemp2[60], szBoxText[200];
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;
	strcpy(szTemp, " ");
	char *etc;
	char filename[512];

	etc = getenv ("BWS");
	if (mobis - movon +1  >= 7)
	{
	    sprintf (filename, "%s\\format\\LL\\%s.lst", etc, ListennameLang);
	}
	else if (mobis - movon +1  >= 4)
	{
	    sprintf (filename, "%s\\format\\LL\\%s.lst", etc, ListennameQuer);
	}
	else
	{
	    sprintf (filename, "%s\\format\\LL\\%s.lst", etc, ListennameLaengs);
	}
	FILENAME_DEFAULT = filename;


	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");


	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
	   	{
    		if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);


    // GR: Druck starten
	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }
    // GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
		if (Hauptmenue == 0)  //#LuD
		{
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		}
		
        return;
    }


	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

    int  nRecCount = 1, nErrorValue = 0, nPrintFehler = 0, nLastPage, nRecno;
	char szPrinter[80], szPort[20];

    LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

	nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	nRecCount = GetRecCount () ;
	if (nRecCount < 1) 
    {
		sprintf (dmess, "Datensatz nicht gefunden ");
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
//	sprintf (dmess, "nRecCount = %hd",nRecCount);  //050803
//	MessageBox(dmess, "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
/******
	int di = AWoWa.leseDS (nErrorValue);   //       1.DATENSATZ lesen f�r Variablen�bergabe
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
******/
//	APreis.SetDatabase (dbClass.GetDatabase());
	AWoWa.openDS ();
	int dleseStatus = 0;
	while (nRecno < nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA)
			&& (dleseStatus == 0))
   	{
	    VariablenUebergabe ( hJob, szTemp2, nRecno );

		nErrorValue = LlPrint(hJob);
		while (nRecno <= nRecCount 
			&& (nErrorValue == 0) 
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& ((dleseStatus = AWoWa.leseDS (nPrintFehler)) == 0))  //       DATENSATZ lesen

		{

		    FelderUebergabe ( hJob, szTemp2, nRecno );
    		// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    		sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    		nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        	if (nErrorValue == LL_ERR_USER_ABORTED)
   			{
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;
   			}

    		// GR: Drucken der aktuellen Tabellenzeile
    		nErrorValue = LlPrintFields(hJob);
			nPrintFehler = nErrorValue;
//050803
			if (nErrorValue == LL_WRN_REPEAT_DATA)
			{
					// GR: Aktualisieren der seitenabhaengigen Variablen
			    VariablenUebergabe ( hJob, szTemp2, nRecno );
				nErrorValue = LlPrint(hJob);
//			    FelderUebergabe ( hJob, szTemp2, nRecno );
//	    		nErrorValue = LlPrintFields(hJob);
			}


			if (nErrorValue == 0)	// US: everything fine, record could have been printed...
	    	{
				// US: ...but probably the user defined a filter condition!
				//     So before updating time dependent variables we have to check if the record really has been printed:
				if (LlPrintDidMatchFilter(hJob))
				{
					// GR: Aktualisieren der zeitabhaengigen Variable 'FixedVariable2'
					// US: update the time dependent variable 'FixedVariable2'
					//     coming after printig the list(s)
		    		sprintf(szTemp2, "FixedVariable2, record %d", nRecno);
		    		LlDefineVariable(hJob, "FixedVariable2", szTemp2);
		    	}

	    		if (nPrintFehler == 0) nRecno++;	// GR: gehe zum naechsten Datensatz
			}
		}
  	}

	// US: all records have been printed, now flush the table
	//     If footer doesn't fit to this page try again for the next page:

	nErrorValue = LlPrintFieldsEnd(hJob);
	if (nErrorValue == LL_WRN_REPEAT_DATA)
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	    VariablenUebergabe ( hJob, szTemp2, nRecno );
	    FelderUebergabe ( hJob, szTemp2, nRecno );

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);
	}

	if (nErrorValue == LL_WRN_REPEAT_DATA)	// US: footer still doesn't fit!
		MessageBox("Error because table is too small for footer!", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);

	//GR: Druck beenden
	LlPrintEnd(hJob,0);
	
	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) loeschen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }
	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}
}

//=============================================================================
LRESULT CMainFrame::OnLulMessage(WPARAM wParam, LPARAM lParam)
//=============================================================================
{
	// GR: Dies ist die List & Label Callback Funktion.
	//     Saemtliche Rueckmeldungen bzw. Events werden dieser Funktion
	//     gemeldet.
	// US: This is the List & Label Callback function.
	//     Is is called for requests an notifications.


	PSCLLCALLBACK	pscCallback = (PSCLLCALLBACK) lParam;
	LRESULT			lRes = TRUE;
	CString			sVariableDescr;

	static CHAR szHelpText[256];

	ASSERT(pscCallback->_nSize == sizeof(scLlCallback));	// GR: Die groesse der Callback Struktur muss stimmen!
															// US: sizes of callback structs must match!

	switch(wParam)
	{
		case LL_CMND_VARHELPTEXT:	// GR: Es wird ein Beschreibungstext f�r eine Variable erfragt.
									// US: Helptext needed for selected variable within designer

				// GR: ( pscCallback->_lParam enth�lt einen LPCSTR des Beschreibungstextes )
				// US: ( pscCallback->_lParam contains a LPCSTR to the name of the selected variable )

				sVariableDescr = (LPCSTR)pscCallback->_lParam;

				if (!sVariableDescr.IsEmpty())
					sprintf(szHelpText,
							"This is the sample field / variable '%s'.",
							(LPCSTR)sVariableDescr);
				else
					strcpy(szHelpText, "No variable or field selected.");

				pscCallback->_lReply = (LPARAM)szHelpText;
				break;

		default:
				pscCallback->_lReply = lRes = FALSE; // GR: Die Nachricht wurde nicht bearbeitet.
													 // US: indicate that message hasn't been processed
	}

	return(lRes);
}


void CMainFrame::GetCfgValues (void)
{
       char cfg_v [512];

       if (ProgCfg == NULL) return;

       cfgOK = TRUE;
	   sprintf(ListennameLang,"umsatzwgmoLang");
	   sprintf(ListennameQuer,"umsatzwgmoQuer");
	   sprintf(ListennameQuer,"umsatzwgmoLaengs");
       if (ProgCfg->GetCfgValue ("ListennameLaengs", cfg_v) == TRUE)
       {
		   sprintf(ListennameLaengs,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("ListennameQuer", cfg_v) == TRUE)
       {
		   sprintf(ListennameQuer,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("ListennameLang", cfg_v) == TRUE)
       {
		   sprintf(ListennameLang,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Listenauswahl", cfg_v) == TRUE)
       {
           Listenauswahl = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Hauptmenue", cfg_v) == TRUE)
       {
           Hauptmenue = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Divisor", cfg_v) == TRUE)
       {
           Divisor = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Format", cfg_v) == TRUE)
       {
		   sprintf(Format,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("MitVorjahr", cfg_v) == TRUE)
       {
           MitVorjahr = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("MaxAnzahlWochen", cfg_v) == TRUE)
       {
           MaxAnzahlMonate = atoi (cfg_v);
	   }
}


    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt
	/************
	LlDefineFieldExt(hJob, "Barcode_EAN13", "44|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P2", "44|44444|44444|44", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P5", "44|44444|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN128", "EAN128ean128", LL_BARCODE_EAN128, NULL);
	LlDefineFieldExt(hJob, "Barcode_CODE128", "Code 128", LL_BARCODE_CODE128, NULL);
	LlDefineFieldExt(hJob, "Barcode_Codabar", "A123456A", LL_BARCODE_CODABAR, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCA", "44|44444", LL_BARCODE_EAN8, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCE", "4|44444|44444", LL_BARCODE_UPCA, NULL);
	LlDefineFieldExt(hJob, "Barcode_3OF9", "*TEST*", LL_BARCODE_3OF9, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IND", "44444", LL_BARCODE_25INDUSTRIAL, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IL", "444444", LL_BARCODE_25INTERLEAVED, NULL);
	LlDefineFieldExt(hJob, "Barcode_25MAT", "44444", LL_BARCODE_25MATRIX, NULL);
	LlDefineFieldExt(hJob, "Barcode_25DL", "44444", LL_BARCODE_25DATALOGIC, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET5", "44444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET10", "44444-4444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET12", "44444-444444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_FIM", "A", LL_BARCODE_FIM, NULL);
	**********/
void fitapldefines (HJOB hJob)
{

//char tabdotfeld[101];
//char defabeleg [101];
//long ll_typ ;

	// GrJ :spaeter soll hier ne gigantische Schleife alles automatisch generieren
	// Namenskonvention ( Vorschlag 1 : 1. Buchstabe Tabelle/feld grossbuchstabe
	//									2. fuer ptab-Felder ein kleines "c" vor Feldname
	//								d.h. Original-feld mit Schluessel wird immer angeboten,
	//									ptabn-Feld nur, falls angekreuzt(Performance)
	//									3. fuer adress-felder : ein kleines "a"
	//									, danach nummer-feld-name, danach feldname
	//										Bsp.: Kun.aAdr1.Str
	//									Aufloesung nur, wenn angekreuzt

// 		dbClass.opendbase ("bws");
//	}
/* ---> Mustereintrag von Wille ---> 
	dbClass.sqlin ((double *) &a_bas.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *)  a_bas.a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *)  a_bas.a_bz2, SQLCHAR, 25);
    cursor = dbClass.sqlcursor ("select a_bz1, a_bz2 from a_bas where a = ?");
	a = 2.0;
	while (dbClass.sqlfetch (cursor) == 0)
	{
	}
	dbClass.sqlclose (cursor);	// destroyed den Cursor, sqlopen refreshed 
< ---- */

/****************		
		dbClass.sqlin ((long *) &syscolumns.tabid, SQLLONG, 0);
		dbClass.sqlin ((char *)  syscolumns.colname, SQLCHAR, 19);

		dbClass.sqlout ((short *) &syscolumns.coltype, SQLSHORT,0);
    
		coltyp_curs = dbClass.sqlcursor ("select coltype from syscolumns where tabid = ? and colname = ?");
    
		dbClass.sqlin ((char *) systables.tabname, SQLCHAR, 19);

		dbClass.sqlout ((long *)  &systables.tabid, SQLLONG, 0);

		tabid_curs = dbClass.sqlcursor ("select tabid from systables where tabname = ?");

// Hier kommt der Hauptcursor

		dbClass.sqlin ((char *) form_feld.form_nr, SQLCHAR, 25);

		dbClass.sqlout ((char *)  form_feld.agg_funk, SQLCHAR, 2);
		dbClass.sqlout ((char *)  form_feld.feld_id, SQLCHAR, 2);
		dbClass.sqlout ((char *)  form_feld.feld_nam, SQLCHAR, 19);
		dbClass.sqlout ((char *)  form_feld.feld_typ, SQLCHAR, 2);
		dbClass.sqlout ((short *) &form_feld.delstatus, SQLSHORT, 0);
		dbClass.sqlout ((char *)  form_feld.tab_nam, SQLCHAR, 19);
		dbClass.sqlout ((long *)  &form_feld.lfd, SQLLONG, 0);
    
		form_feld_curs = dbClass.sqlcursor 
			("select agg_funk, feld_id, feld_nam, feld_typ, delstatus, tab_nam, lfd from form_feld where form_nr = ?");
    
	}

	sprintf (form_feld.form_nr, "test1");

	dbClass.sqlopen (form_feld_curs);

	while (dbClass.sqlfetch (form_feld_curs) == 0)
	{
		ll_typ = holetyp(form_feld.tab_nam , form_feld.feld_nam );

		form_feld.tab_nam[0] = toupper(form_feld.tab_nam[0]) ;
		form_feld.feld_nam[0] = toupper(form_feld.feld_nam[0]) ;
		CString s = form_feld.tab_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.tab_nam, s ) ;
		s = form_feld.feld_nam ;
		s.TrimRight() ;
		strcpy ( form_feld.feld_nam, s ) ;
		sprintf( tabdotfeld , "%s.%s", form_feld.tab_nam, form_feld.feld_nam);
		sprintf ( defabeleg, "%s", form_feld.feld_nam);
		if (ll_typ == LL_NUMERIC )
		{
			sprintf ( defabeleg, "12345678");
		};
	
			LlDefineFieldExt(hJob, tabdotfeld, defabeleg,ll_typ,NULL);

	}
********************************/
}


