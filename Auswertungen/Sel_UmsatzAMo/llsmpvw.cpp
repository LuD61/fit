//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
//=============================================================================

// llsmpvw.cpp : implementation of the CLlSampleView class

#include "stdafx.h"
#include "llmfc.h"

#include "llsmpdoc.h"
#include "llsmpvw.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLlSampleView

IMPLEMENT_DYNCREATE(CLlSampleView, CView)

BEGIN_MESSAGE_MAP(CLlSampleView, CView)
	//{{AFX_MSG_MAP(CLlSampleView)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLlSampleView construction/destruction

CLlSampleView::CLlSampleView()
{
	// TODO: add construction code here
}

CLlSampleView::~CLlSampleView()
{
}

/////////////////////////////////////////////////////////////////////////////
// CLlSampleView drawing

void CLlSampleView::OnDraw(CDC* /*pDC*/)
{
	// TODO: add draw code here
}



/////////////////////////////////////////////////////////////////////////////
// CLlSampleView diagnostics

#ifdef _DEBUG
void CLlSampleView::AssertValid() const
{
	CView::AssertValid();
}

void CLlSampleView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CLlSampleDoc* CLlSampleView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLlSampleDoc)));
	return (CLlSampleDoc*) m_pDocument;
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLlSampleView message handlers

