#ifndef _VECTOR_DEF
#define _VECTOR_DEF

class CVector
{
    private :

       void **Arr;
       int anz;
       int pos;
    public :
       int GetAnz ()
       {
           return anz;
       }

       CVector ();
       CVector (void **Object);
       ~CVector ();
       void SetObject (void **Object);
       void Add (void *);
       void SetPosition (int);
       void FirstPosition (void);
       void *Get (int);
       void *GetNext (int);
       void *GetNext (void);
       BOOL Drop (int);
};
#endif
