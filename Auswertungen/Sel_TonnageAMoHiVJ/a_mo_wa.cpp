#include "stdafx.h"
#include "DbClass.h"
#include "a_mo_wa.h"
#include "cmbtl9.h"

struct A_MO_WA a_mo_wa, a_mo_wa_null;
struct AUSGABE ausgabe, ausgabe_null;
struct A_BAS a_bas, a_bas_null;
extern DB_CLASS dbClass;
static long anzfelder = -1;
double menge[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
double menge_vj[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
short mdn = 1;
short filvon = 0 ;
short filbis = 9999;
short jr = 2008;
short vjr = 2007;
short movon ;
short mobis ;
short monatvon ;
short monatbis ;
short monat ;
long hi1von ;
long hi1bis ;
long hi2von ;
long hi2bis ;
long hi3von ;
long hi3bis ;
long hi4von ;
long hi4bis ;
long kunvon ;
long kunbis ;
double avon ;
double abis ;
long hwgvon ;
long hwgbis ;
long wgvon ;
long wgbis ;
char a_bz1 [25];
char hwg_bz1 [25];
char wg_bz1 [25];
double a_gew;
short a_bas_ag ;
short a_bas_dr_folge;
short a_bas_me_einh;
short a_bas_lief_einh;
double a_bas_a_gew;
char rez_preis_bz[12];
char einh_bz[9];
double rez_preis;
extern char ListennameLaengs[128];
extern char ListennameQuer[128];
char Listenname_vgl[128];
char Listenname_vgleinzel[128];


int A_MO_WA_CLASS::dbcount (void)
/**
Tabelle eti lesen.
**/
{

         if (test_upd_cursor == -1)
         {
			 if (hi1bis == 0)
			 {
				 if (abis > 0.0)
				 {
					prepare ();
				 }
				 else if (wgbis > 0)
				 {
					prepare_wg ();
				 }
				 else
				 {
					 prepare_hwg ();
				 }
			 }
			 else if (hi2bis == 0)
			 {
				 if (abis > 0.0)
				 {
					prepareHi_1 ();
				 }
				 else if (wgbis > 0)
				 {
					prepareHi_1_wg ();
				 }
				 else
				 {
					 prepareHi_1_hwg ();
				 }
			 }
			 else if (hi3bis == 0)
			 {
				 if (abis > 0.0)
				 {
					prepareHi_2 ();
				 }
				 else if (wgbis > 0)
				 {
					prepareHi_2_wg ();
				 }
				 else
				 {
					 prepareHi_2_hwg ();
				 }
			 }
			 else if (hi4bis == 0)
			 {
				 if (abis > 0.0)
				 {
					prepareHi_3 ();
				 }
				 else if (wgbis > 0)
				 {
					prepareHi_3_wg ();
				 }
				 else
				 {
					 prepareHi_3_hwg ();
				 }
			 }
			 else if (kunbis == 0)
			 {
				 if (abis > 0.0)
				 {
					prepareHi_4 ();
				 }
				 else if (wgbis > 0)
				 {
					prepareHi_4_wg ();
				 }
				 else
				 {
					 prepareHi_4_hwg ();
				 }
			 }
			 else 
			 {
				 if (abis > 0.0)
				 {
					prepareHi_5 ();
				 }
				 else if (wgbis > 0)
				 {
					prepareHi_5_wg ();
				 }
				 else
				 {
					 prepareHi_5_hwg ();
				 }
			 }
         }
         dbClass.sqlopen (count_cursor);
         sqlstatus = dbClass.sqlfetch (count_cursor);
		 if (sqlstatus != 0) return -1 ;
		 anzfelder = 0;
         while (sqlstatus == 0)
         {
			 anzfelder ++;
	         sqlstatus = dbClass.sqlfetch (count_cursor);
         }
         dbClass.sqlopen (count_cursor);
         return anzfelder; 
}

int A_MO_WA_CLASS::leseDS (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }
      fehlercode = dbClass.sqlfetch (count_cursor);
	  if (ausgabe.hirarchie1 > 0)
	  {
		dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
		dbClass.sqlout ((char *) ausgabe.h1_bez, SQLCHAR, 17);
		dbClass.sqlcomm ("select h1_bezkrz from h1 where hirarchie1 = ? ");
	  }
	  if (ausgabe.hirarchie2 > 0)
	  {
		dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
		dbClass.sqlout ((char *) ausgabe.h2_bez, SQLCHAR, 17);
		dbClass.sqlcomm ("select h2_bezkrz from h2 where hirarchie2 = ? ");
	  }
	  if (ausgabe.hirarchie3 > 0)
	  {
		dbClass.sqlin ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
		dbClass.sqlout ((char *) ausgabe.h3_bez, SQLCHAR, 17);
		dbClass.sqlcomm ("select h3_bezkrz from h3 where hirarchie3 = ? ");
	  }
	  if (ausgabe.hirarchie4 > 0)
	  {
		dbClass.sqlin ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
		dbClass.sqlout ((char *) ausgabe.h4_bez, SQLCHAR, 17);
		dbClass.sqlcomm ("select h4_bezkrz from h4 where hirarchie4 = ? ");
	  }

	  if (fehlercode == 0) 
      {
		sqlstatus = dbClass.sqlopen (readcursor);
        sqlstatus = dbClass.sqlfetch (readcursor);
		menge[0] = 0.0;menge[1] = 0.0;menge[2] = 0.0;menge[3] = 0.0;menge[4] = 0.0;
		menge[5] = 0.0;menge[6] = 0.0;menge[7] = 0.0;menge[8] = 0.0;menge[9] = 0.0;
		menge[10] = 0.0;menge[11] = 0.0;menge[12] = 0.0;menge[13] = 0.0;

		menge_vj[0] = 0.0;menge_vj[1] = 0.0;menge_vj[2] = 0.0;menge_vj[3] = 0.0;menge_vj[4] = 0.0;
		menge_vj[5] = 0.0;menge_vj[6] = 0.0;menge_vj[7] = 0.0;menge_vj[8] = 0.0;menge_vj[9] = 0.0;
		menge_vj[10] = 0.0;menge_vj[11] = 0.0;menge_vj[12] = 0.0;menge_vj[13] = 0.0;
		while (sqlstatus == 0)
         {
             monat = a_mo_wa.mo;  
             if (monat -monatvon >= 0)
			 {
				if (ausgabe.jr == jr)
				{
						menge[monat - monatvon] += a_mo_wa.me_vk_mo ;
						menge[monatbis - monatvon + 1] += a_mo_wa.me_vk_mo ; //Summe
				}
				else
				{
						menge_vj[monat - monatvon] += a_mo_wa.me_vk_mo ;
						menge_vj[monatbis - monatvon + 1] += a_mo_wa.me_vk_mo ; //Summe
				}
			 }
	         sqlstatus = dbClass.sqlfetch (readcursor);
         }

      }
      return fehlercode;
}

int A_MO_WA_CLASS::openDS (void)
{
         return dbClass.sqlopen (readcursor);
}

void A_MO_WA_CLASS::prepare (void)
{


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &abis, SQLDOUBLE, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);



    count_cursor = dbClass.sqlcursor ("select a_mo_wa.a, a_bas.a_bz1,a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1  " 
                            "from a_mo_wa,a_bas,hwg,wg where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ? "
							"and a_bas.a between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"and a_mo_wa.kun > 0 "
							"group by 1,2,3,4,5,6  order by a_bas.hwg,a_bas.wg,a_mo_wa.a  "
							);

	test_upd_cursor = 1;

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &ausgabe.a, SQLDOUBLE, 0);

	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);

	dbClass.sqlout ((short *) &a_bas.me_einh, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_bas.a_gew, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);


            readcursor = dbClass.sqlcursor ("select a_mo_wa.a, a_mo_wa.jr,a_mo_wa.mo,a_bas.me_einh,a_bas.a_gew, " 
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.a = ?  "
							"and a_mo_wa.kun > 0 "
							"group by 1,2,3,4,5 order by 1,2,3  "
						);

}

void A_MO_WA_CLASS::prepare_wg (void)
{


//	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
//	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
//	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
//	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
//	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
//	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
//	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);



	/*
    count_cursor = dbClass.sqlcursor ("select a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1  " 
                            "from a_mo_wa,a_bas,hwg,wg where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4  order by a_bas.hwg,a_bas.wg  "
							);
*/
    count_cursor = dbClass.sqlcursor ("select a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1  " 
                            "from a_bas,hwg,wg where a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4  order by a_bas.hwg,a_bas.wg  "
							);

	test_upd_cursor = 1;

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.wg, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);

	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);


            readcursor = dbClass.sqlcursor ("select a_bas.wg, a_mo_wa.jr,a_mo_wa.mo, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.wg = ?  "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and a_mo_wa.kun > 0 "
							"group by 1,2,3 order by 1,2,3  "
						);

}

void A_MO_WA_CLASS::prepare_hwg (void)
{


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);



    count_cursor = dbClass.sqlcursor ("select a_bas.hwg,hwg.hwg_bz1  " 
                            "from a_mo_wa,a_bas,hwg where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and a_bas.hwg = hwg.hwg "
							"and a_mo_wa.kun > 0 "
							"group by 1,2  order by a_bas.hwg  "
							);

	test_upd_cursor = 1;

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.hwg, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);

	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);


            readcursor = dbClass.sqlcursor ("select a_bas.hwg, a_mo_wa.jr,a_mo_wa.mo, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.hwg = ?  "
							"and a_mo_wa.kun > 0 "
							"group by 1,2,3 order by 1,2,3  "
						);

}

void A_MO_WA_CLASS::prepareHi_1 (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &abis, SQLDOUBLE, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1, kun.hirarchie1 " 
							",a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg,wg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.a between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4,5,6,7 "
							"order by kun.hirarchie1, a_bas.hwg,a_bas.wg,a_bas.a  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_bas.me_einh, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_bas.a_gew, SQLDOUBLE, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	

            readcursor = dbClass.sqlcursor ("select a_bas.a,a_mo_wa.jr, a_mo_wa.mo, a_bas.me_einh,a_bas.a_gew, "
							"kun.hirarchie1, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.a = ? "
							"and kun.hirarchie1 = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6 "
							"order by kun.hirarchie1,a_bas.a  "
							);

}
void A_MO_WA_CLASS::prepareHi_1_wg (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select kun.hirarchie1 " 
							",a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg,wg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4,5 "
							"order by kun.hirarchie1, a_bas.hwg,a_bas.wg  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	

            readcursor = dbClass.sqlcursor ("select a_mo_wa.jr, a_mo_wa.mo,  "
							"kun.hirarchie1, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.wg = ? "
							"and kun.hirarchie1 = ? "
							"and a_mo_wa.me_vk_mo <> 0.0 "
							"and kun.mdn = ? "
							"group by 1,2,3 "
							"order by kun.hirarchie1 "
							);

}

void A_MO_WA_CLASS::prepareHi_1_hwg (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select kun.hirarchie1 " 
							",a_bas.hwg,hwg.hwg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg "
							"group by 1,2,3 "
							"order by kun.hirarchie1, a_bas.hwg  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	

            readcursor = dbClass.sqlcursor ("select a_mo_wa.jr, a_mo_wa.mo,  "
							"kun.hirarchie1, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.hwg = ? "
							"and kun.hirarchie1 = ? "
							"and a_mo_wa.me_vk_mo <> 0.0 "
							"and kun.mdn = ? "
							"group by 1,2,3 "
							"order by kun.hirarchie1 "
							);
}

void A_MO_WA_CLASS::prepareHi_2 (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &abis, SQLDOUBLE, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1, kun.hirarchie1, kun.hirarchie2 " 
							",a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg,wg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.a between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4,5,6,7,8  "
							"order by kun.hirarchie1, kun.hirarchie2,a_bas.hwg,a_bas.wg,a_bas.a  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_bas.me_einh, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_bas.a_gew, SQLDOUBLE, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);

	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);


            readcursor = dbClass.sqlcursor ("select a_bas.a,a_mo_wa.jr, a_mo_wa.mo, a_bas.me_einh,a_bas.a_gew, "
							"kun.hirarchie1,kun.hirarchie2, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.a = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6,7 order by 1,2,3  "
							);

}



void A_MO_WA_CLASS::prepareHi_2_wg (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select kun.hirarchie1, kun.hirarchie2 " 
							",a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg,wg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4,5,6 "
							"order by kun.hirarchie1,kun.hirarchie2, a_bas.hwg,a_bas.wg  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	

            readcursor = dbClass.sqlcursor ("select a_mo_wa.jr, a_mo_wa.mo,  "
							"kun.hirarchie1,kun.hirarchie2, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.wg = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and a_mo_wa.me_vk_mo <> 0.0 "
							"and kun.mdn = ? "
							"group by 1,2,3,4 "
							"order by kun.hirarchie1,kun.hirarchie2 "
							);

}

void A_MO_WA_CLASS::prepareHi_2_hwg (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select kun.hirarchie1,kun.hirarchie2 " 
							",a_bas.hwg,hwg.hwg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg "
							"group by 1,2,3,4 "
							"order by kun.hirarchie1,kun.hirarchie2, a_bas.hwg  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	

            readcursor = dbClass.sqlcursor ("select a_mo_wa.jr, a_mo_wa.mo,  "
							"kun.hirarchie1,kun.hirarchie2, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.hwg = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and a_mo_wa.me_vk_mo <> 0.0 "
							"and kun.mdn = ? "
							"group by 1,2,3,4 "
							"order by kun.hirarchie1, kun.hirarchie2 "
							);
}



void A_MO_WA_CLASS::prepareHi_3 (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &abis, SQLDOUBLE, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1, kun.hirarchie1, kun.hirarchie2,kun.hirarchie3 " 
							",a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg,wg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.a between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and kun.hirarchie3 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4,5,6,7,8,9 "
							"order by kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,a_bas.hwg,a_bas.wg,a_bas.a  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_bas.me_einh, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_bas.a_gew, SQLDOUBLE, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);


            readcursor = dbClass.sqlcursor ("select a_bas.a, a_mo_wa.jr,a_mo_wa.mo,  a_bas.me_einh,a_bas.a_gew, "
							"kun.hirarchie1,kun.hirarchie2,kun.hirarchie3, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.a = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and kun.hirarchie3 = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6,7,8 order by 1,2,3  "
							);

}



void A_MO_WA_CLASS::prepareHi_3_wg (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select kun.hirarchie1, kun.hirarchie2,kun.hirarchie3 " 
							",a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg,wg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and kun.hirarchie3 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4,5,6,7 "
							"order by kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,a_bas.hwg,a_bas.wg  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);


            readcursor = dbClass.sqlcursor ("select a_bas.wg, a_mo_wa.jr,a_mo_wa.mo, "
							"kun.hirarchie1,kun.hirarchie2,kun.hirarchie3, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.wg = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and kun.hirarchie3 = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6 order by 1,2,3  "
							);

}

void A_MO_WA_CLASS::prepareHi_3_hwg (void)
{
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select kun.hirarchie1, kun.hirarchie2,kun.hirarchie3 " 
							",a_bas.hwg,hwg.hwg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and kun.hirarchie3 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg  "
							"group by 1,2,3,4,5 "
							"order by kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,a_bas.hwg  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);


            readcursor = dbClass.sqlcursor ("select a_bas.hwg, a_mo_wa.jr,a_mo_wa.mo, "
							"kun.hirarchie1,kun.hirarchie2,kun.hirarchie3, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.hwg = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and kun.hirarchie3 = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6 order by 1,2,3  "
							);
}



void A_MO_WA_CLASS::prepareHi_4 (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &abis, SQLDOUBLE, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1, kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4 " 
							",a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg,wg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.a between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and kun.hirarchie3 between ? and ? "
							"and kun.hirarchie4 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4,5,6,7,8,9,10  "
							"order by kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4,a_bas.hwg,a_bas.wg,a_bas.a  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_bas.me_einh, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_bas.a_gew, SQLDOUBLE, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);

            readcursor = dbClass.sqlcursor ("select a_bas.a, a_mo_wa.jr,a_mo_wa.mo,  a_bas.me_einh,a_bas.a_gew, "
							"kun.hirarchie1,kun.hirarchie2,kun.hirarchie3,kun.hirarchie4, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.a = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and kun.hirarchie3 = ? "
							"and kun.hirarchie4 = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6,7,8,9 order by 1,2,3  "
							);

}

void A_MO_WA_CLASS::prepareHi_4_wg (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4 " 
							",a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg,wg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and kun.hirarchie3 between ? and ? "
							"and kun.hirarchie4 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4,5,6,7,8  "
							"order by kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4,a_bas.hwg,a_bas.wg  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);

            readcursor = dbClass.sqlcursor ("select  a_mo_wa.jr,a_mo_wa.mo, "
							"kun.hirarchie1,kun.hirarchie2,kun.hirarchie3,kun.hirarchie4, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.wg = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and kun.hirarchie3 = ? "
							"and kun.hirarchie4 = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6 order by 1,2,3  "
							);

}

void A_MO_WA_CLASS::prepareHi_4_hwg (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4bis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4 " 
							",a_bas.hwg,hwg.hwg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ?  "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and kun.hirarchie3 between ? and ? "
							"and kun.hirarchie4 between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg "
							"group by 1,2,3,4,5,6 "
							"order by kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4,a_bas.hwg "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);

            readcursor = dbClass.sqlcursor ("select  a_mo_wa.jr,a_mo_wa.mo, "
							"kun.hirarchie1,kun.hirarchie2,kun.hirarchie3,kun.hirarchie4, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.hwg = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and kun.hirarchie3 = ? "
							"and kun.hirarchie4 = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6 order by 1,2,3  "
							);

}


void A_MO_WA_CLASS::prepareHi_5 (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &avon, SQLDOUBLE, 0);
	dbClass.sqlin ((double *) &abis, SQLDOUBLE, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &kunvon, SQLLONG, 0);
	dbClass.sqlin ((long *) &kunbis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((char *) a_bz1, SQLCHAR, 25);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlout ((char *) ausgabe.kun_krz1, SQLCHAR, 17);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select a_bas.a, a_bas.a_bz1, kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4, " 
							"kun.kun, kun.kun_krz1 "
							",a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg,wg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.a between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and kun.hirarchie3 between ? and ? "
							"and kun.hirarchie4 between ? and ? "
							"and kun.kun between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4,5,6,7,8,9,10,11,12  "
							"order by kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4,kun.kun,a_bas.hwg,a_bas.wg,a_bas.a  "
							);

	test_upd_cursor = 1;  


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((double *) &ausgabe.a, SQLDOUBLE, 0);
	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_bas.me_einh, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_bas.a_gew, SQLDOUBLE, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlout ((char *) ausgabe.kun_krz1, SQLCHAR, 17);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);


            readcursor = dbClass.sqlcursor ("select a_bas.a, a_mo_wa.jr,a_mo_wa.mo, a_bas.me_einh,a_bas.a_gew, "
							"kun.hirarchie1,kun.hirarchie2,kun.hirarchie3,kun.hirarchie4,kun.kun,kun.kun_krz1, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.a = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and kun.hirarchie3 = ? "
							"and kun.hirarchie4 = ? "
							"and kun.kun = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6,7,8,9,10,11 order by 1,2,3  "
							);

}

void A_MO_WA_CLASS::prepareHi_5_wg (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &wgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &kunvon, SQLLONG, 0);
	dbClass.sqlin ((long *) &kunbis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlout ((char *) ausgabe.kun_krz1, SQLCHAR, 17);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4, " 
							"kun.kun, kun.kun_krz1 "
							",a_bas.hwg,a_bas.wg,hwg.hwg_bz1,wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg,wg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? and a_bas.wg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and kun.hirarchie3 between ? and ? "
							"and kun.hirarchie4 between ? and ? "
							"and kun.kun between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg and a_bas.wg = wg.wg "
							"group by 1,2,3,4,5,6,7,8,9,10 "
							"order by kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4,kun.kun,a_bas.hwg,a_bas.wg  "
							);

	test_upd_cursor = 1;  


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlout ((char *) ausgabe.kun_krz1, SQLCHAR, 17);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);


            readcursor = dbClass.sqlcursor ("select  a_mo_wa.jr,a_mo_wa.mo, "
							"kun.hirarchie1,kun.hirarchie2,kun.hirarchie3,kun.hirarchie4,kun.kun,kun.kun_krz1, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.wg = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and kun.hirarchie3 = ? "
							"and kun.hirarchie4 = ? "
							"and kun.kun = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6,7,8 order by 1,2,3  "
							);

}

void A_MO_WA_CLASS::prepareHi_5_hwg (void)
{

  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &hwgbis, SQLSHORT, 0);
	dbClass.sqlin ((long *) &hi1von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi1bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi2bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi3bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4von, SQLLONG, 0);
	dbClass.sqlin ((long *) &hi4bis, SQLLONG, 0);
	dbClass.sqlin ((long *) &kunvon, SQLLONG, 0);
	dbClass.sqlin ((long *) &kunbis, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlout ((char *) ausgabe.kun_krz1, SQLCHAR, 17);
	dbClass.sqlout ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlout ((char *) hwg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4, " 
							"kun.kun, kun.kun_krz1 "
							",a_bas.hwg,hwg.hwg_bz1 " 
                            "from a_mo_wa,a_bas,kun,hwg  where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? "
							"and a_bas.hwg between ? and ? "
							"and kun.hirarchie1 between ? and ? "
							"and kun.hirarchie2 between ? and ? "
							"and kun.hirarchie3 between ? and ? "
							"and kun.hirarchie4 between ? and ? "
							"and kun.kun between ? and ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"and a_bas.hwg = hwg.hwg "
							"group by 1,2,3,4,5,6,7,8 "
							"order by kun.hirarchie1, kun.hirarchie2,kun.hirarchie3,kun.hirarchie4,kun.kun,a_bas.hwg "
							);

	test_upd_cursor = 1;  


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.hwg, SQLSHORT, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie1, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie2, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie3, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.hirarchie4, SQLLONG, 0);
	dbClass.sqlout ((long *) &ausgabe.kun, SQLLONG, 0);
	dbClass.sqlout ((char *) ausgabe.kun_krz1, SQLCHAR, 17);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);


            readcursor = dbClass.sqlcursor ("select  a_mo_wa.jr,a_mo_wa.mo, "
							"kun.hirarchie1,kun.hirarchie2,kun.hirarchie3,kun.hirarchie4,kun.kun,kun.kun_krz1, "
							"sum(case (a_bas.me_einh) when 2 then a_mo_wa.me_vk_mo else a_mo_wa.me_vk_mo * a_bas.a_gew end) "
                            "from a_mo_wa,a_bas,kun where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? and a_mo_wa.mo between ? and ?  "
							"and a_bas.hwg = ? "
							"and kun.hirarchie1 = ? "
							"and kun.hirarchie2 = ? "
							"and kun.hirarchie3 = ? "
							"and kun.hirarchie4 = ? "
							"and kun.kun = ? "
							"and (a_mo_wa.ums_vk_mo <> 0.0 or a_mo_wa.ums_fil_ek_mo <> 0.0) "
							"and kun.mdn = ? "
							"group by 1,2,3,4,5,6,7,8 order by 1,2,3  "
							);

}
