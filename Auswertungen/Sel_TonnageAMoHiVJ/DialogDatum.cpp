// DialogDatum.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "llmfc.h"
#include "DialogDatum.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
extern short movon ;
extern short mobis ;
extern short mdn ;
extern short jr ;
extern short filvon ;
extern short filbis ;
extern long hi1von ;
extern long hi1bis ;
extern long hi2von ;
extern long hi2bis ;
extern long hi3von ;
extern long hi3bis ;
extern long hi4von ;
extern long hi4bis ;
extern long kunvon ;
extern long kunbis ;
extern double avon ;
extern double abis ;
extern long hwgvon ;
extern long hwgbis ;
extern long wgvon ;
extern long wgbis ;
bool Monatok = FALSE;

CForm *CDialogDatum::_Dialog [] = 
{
   new CForm (IDC_MANDANT, (short *)      &mdn, FRMSHORT, "%hd"),
   new CForm (IDC_FILIALE, (short *)      &filvon, FRMSHORT, "%hd"),
   new CForm (IDC_FILIALEBIS, (short *)      &filbis, FRMSHORT, "%hd"),
   new CForm (IDC_JAHR, (short *)      &jr, FRMSHORT, "%hd"),
   new CForm (IDC_WOVON, (long *)      &movon, FRMLONG, "%ld"),
   new CForm (IDC_WOBIS, (long *) &mobis, FRMLONG, "%ld"),
   new CForm (IDC_AVON, (double *)      &avon, FRMDOUBLE, "%.0lf"),
   new CForm (IDC_ABIS, (double *) &abis, FRMDOUBLE, "%.0lf"),
   new CForm (IDC_HI1_VON, (long *)      &hi1von, FRMLONG, "%ld"),
   new CForm (IDC_HI1_BIS, (long *) &hi1bis, FRMLONG, "%ld"),
   new CForm (IDC_HI2_VON, (long *)      &hi2von, FRMLONG, "%ld"),
   new CForm (IDC_HI2_BIS, (long *) &hi2bis, FRMLONG, "%ld"),
   new CForm (IDC_HI3_VON, (long *)      &hi3von, FRMLONG, "%ld"),
   new CForm (IDC_HI3_BIS, (long *) &hi3bis, FRMLONG, "%ld"),
   new CForm (IDC_HI4_VON, (long *)      &hi4von, FRMLONG, "%ld"),
   new CForm (IDC_HI4_BIS, (long *) &hi4bis, FRMLONG, "%ld"),
   new CForm (IDC_KUN_VON, (long *) &kunvon, FRMLONG, "%ld"),
   new CForm (IDC_KUN_BIS, (long *) &kunbis, FRMLONG, "%ld"),
   new CForm (IDC_HWGVON, (long *) &hwgvon, FRMLONG, "%ld"),
   new CForm (IDC_HWGBIS, (long *) &hwgbis, FRMLONG, "%ld"),
   new CForm (IDC_WGVON, (long *) &wgvon, FRMLONG, "%ld"),
   new CForm (IDC_WGBIS, (long *) &wgbis, FRMLONG, "%ld"),
   NULL,
};

CVector CDialogDatum::Dialog ((void **) _Dialog);


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDialogDatum 


CDialogDatum::CDialogDatum(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogDatum::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}

BOOL CDialogDatum::OnInitDialog()
{
	CDialog::OnInitDialog();
    GetDlgItem (IDC_MANDANT)->SetFocus ();
    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
	Read ();

	return TRUE;
}

void CDialogDatum::EnableDialog (BOOL b)
{


    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->Enable (this, b);
    }
	if (b)
	{
	    GetDlgItem (IDC_MANDANT)->SetFocus ();
	    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
    }
}
void CDialogDatum::ToDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->ToScreen (this);
    }
}


void CDialogDatum::FromDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();

    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->FromScreen (this);
    }
}

BOOL CDialogDatum::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
CWnd *cWnd;
if (pMsg->message == WM_KEYDOWN)
{
	switch (pMsg->wParam)
	{
	case VK_RETURN : 
	case VK_TAB :
		    cWnd = GetFocus ();
			if (cWnd ==GetDlgItem (IDC_MANDANT))
			{
			    Write ();
			    GetDlgItem (IDC_FILIALE)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_FILIALE))
			{
			    Write ();
			    GetDlgItem (IDC_FILIALEBIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_FILIALEBIS))
			{
			    Write ();
			    GetDlgItem (IDC_JAHR)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_JAHR))
			{
			    Write ();
			    GetDlgItem (IDC_WOVON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_AVON))
			{
			    Write ();
			    GetDlgItem (IDC_ABIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_ABIS))
			{
			    Write ();
			    GetDlgItem (IDC_HWGVON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}


			if (cWnd ==GetDlgItem (IDC_HWGVON))
			{
			    Write ();
			    GetDlgItem (IDC_HWGBIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_HWGBIS))
			{
			    Write ();
			    GetDlgItem (IDC_WGVON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}

			if (cWnd ==GetDlgItem (IDC_WGVON))
			{
			    Write ();
			    GetDlgItem (IDC_WGBIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_WGBIS))
			{
			    Write ();
			    GetDlgItem (IDC_HI1_VON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}


			if (cWnd ==GetDlgItem (IDC_WOVON))
			{
			    Write ();
			    GetDlgItem (IDC_WOBIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_WOBIS))
			{
			    Write ();
			    GetDlgItem (IDC_AVON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_HI1_VON))
			{
			    Write ();
			    GetDlgItem (IDC_HI1_BIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_HI1_BIS))
			{
			    Write ();
			    GetDlgItem (IDC_HI2_VON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}

			if (cWnd ==GetDlgItem (IDC_HI2_VON))
			{
			    Write ();
			    GetDlgItem (IDC_HI2_BIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_HI2_BIS))
			{
			    Write ();
			    GetDlgItem (IDC_HI3_VON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}

			if (cWnd ==GetDlgItem (IDC_HI3_VON))
			{
			    Write ();
			    GetDlgItem (IDC_HI3_BIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_HI3_BIS))
			{
			    Write ();
			    GetDlgItem (IDC_HI4_VON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}

			if (cWnd ==GetDlgItem (IDC_HI4_VON))
			{
			    Write ();
			    GetDlgItem (IDC_HI4_BIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_HI4_BIS))
			{
			    Write ();
			    GetDlgItem (IDC_KUN_VON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_KUN_VON))
			{
			    Write ();
			    GetDlgItem (IDC_KUN_BIS)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_KUN_BIS))
			{
			    Write ();
			    GetDlgItem (IDC_MANDANT)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}




			if (cWnd ==GetDlgItem (IDCANCEL))
			{
				return CDialog::PreTranslateMessage(pMsg);
			}
			if (cWnd ==GetDlgItem (IDOK))
			{
			    Write ();
				if (Monatok == FALSE)
                { 
					GetDlgItem (IDC_MANDANT)->SetFocus ();
					((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
					return TRUE;
                }
				return CDialog::PreTranslateMessage(pMsg);
			}
		    NextDlgCtrl();
		    return TRUE;

	}
}
	return CDialog::PreTranslateMessage(pMsg);
}




BOOL CDialogDatum::Read (void)
{
    ToDialog ();
    EnableDialog (TRUE);
	return TRUE;
	
}

BOOL CDialogDatum::Write (void)
{

    FromDialog ();
	if (kunbis > 0) if (hi4bis == 0) hi4bis = 999999; 
	if (hi4bis > 0) if (hi3bis == 0) hi3bis = 999999; 
	if (hi3bis > 0) if (hi2bis == 0) hi2bis = 999999; 
	if (hi2bis > 0) if (hi1bis == 0) hi1bis = 999999; 
    ToDialog ();


return TRUE;
}

void CDialogDatum::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	Write();
	CDialog::OnOK();
}


void CDialogDatum::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogDatum, CDialog)
	//{{AFX_MSG_MAP(CDialogDatum)
	ON_EN_CHANGE(IDC_WOBIS, OnChangeWoBis)
	ON_EN_CHANGE(IDC_WOVON, OnChangeWoVon)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDialogDatum 

void CDialogDatum::OnChangeWoBis() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnChangeWoVon() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnCloseupCombo1() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}
