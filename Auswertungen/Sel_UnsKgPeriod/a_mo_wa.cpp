#include "stdafx.h"
#include "DbClass.h"
#include "DbRange.h"
#include "a_mo_wa.h"
#include "cmbtl9.h"
#include "datum.h"

struct A_MO_WA a_mo_wa, a_mo_wa_null;
struct AUSGABE ausgabe, ausgabe_null;
struct MONATE monate, monate_null;
struct A_BAS a_bas, a_bas_null;
extern DB_CLASS dbClass;
extern DB_RANGE dbRange;
extern short UmsatzFilEinn ;
extern short MitWeDirect ;
static long anzfelder = -1;
BOOL flg_wo1_gekappt = FALSE;
BOOL flg_wo1vj_gekappt = FALSE;
double menge[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
double umsatz[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
double menge_vj[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
double umsatz_vj[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
DATUM d_dat, d_datfirst, d_datlast;
DATE_STRUCT end_dat;
short erste_wo ;
short letzte_wo ;
short wo1 = 0;
short wo2 = 0;
short wo3 = 0;
short wo4 = 0;
short wo5 = 0;
short wo1_vj = 0;
short wo2_vj = 0;
short wo3_vj = 0;
short wo4_vj = 0;
short wo5_vj = 0;
short woche ;
short woche_jr;
extern short movon;

char datumvj_von[12];
char datumvj_bis[12];
char datumjr_von[12];
char datumjr_bis[12];
char datummo_von[12];
char datummo_bis[12];
char datumvjmo_von[12];
char datumvjmo_bis[12];
char erster_tag[12];
char letzter_tag[12];
char erster_tag_wo1[12];
char letzter_tag_wo1[12];
char erster_tag_wo2[12];
char letzter_tag_wo2[12];
char erster_tag_wo3[12];
char letzter_tag_wo3[12];
char erster_tag_wo4[12];
char letzter_tag_wo4[12];
char erster_tag_wo5[12];
char letzter_tag_wo5[12];
char erster_tag_wo1_vj[12];
char letzter_tag_wo1_vj[12];
char erster_tag_wo2_vj[12];
char letzter_tag_wo2_vj[12];
char erster_tag_wo3_vj[12];
char letzter_tag_wo3_vj[12];
char erster_tag_wo4_vj[12];
char letzter_tag_wo4_vj[12];
char erster_tag_wo5_vj[12];
char letzter_tag_wo5_vj[12];

short mdn = 1;
short monat1;
short monat2;
short monat3;
short monat4;
short AnzMonate;
char filbereich[256];
char kunbereich[256];
char mobereich[256];
char abereich[256];
int _jr;
int akt_mo = 0;
int akt_jr = 0;
int _mo, _wo;
int _tag;
short jr = 0;
short vjr = 0;
short vorjahr = 0;
short monat ;
char adr_krz [25];
double a_gew;
short a_bas_ag ;
short a_bas_dr_folge;
short a_bas_me_einh;
short a_bas_lief_einh;
double a_bas_a_gew;
char rez_preis_bz[12];
char a_bz1[25];
char einh_bz[9];
double rez_preis;
extern char ListennameLaengs[128];
extern char ListennameQuer[128];
char Listenname_vgl[128];
char Listenname_vgleinzel[128];
extern int MitVorjahr;
char sys_par_wrt [2] = " ";
short dcount = 0;

short A_MO_WA_CLASS::GetMonate (void)
{
	dbClass.opendbase ("bws");

	monat1 = 0; monat2 = 0; monat3 = 0; monat4 = 0; dcount = 0;
    dbRange.RangeIn ("monate.mo",mobereich, SQLSHORT, 0); 
	dbClass.sqlout ((short *) &monate.mo, SQLSHORT, 0);

	monate_cursor = dbClass.sqlcursor (dbRange.SetRange ("select mo  from monate where <monate.mo> "
						"order by mo "));

         dbClass.sqlopen (monate_cursor);
		 while (dbClass.sqlfetch (monate_cursor) == 0)
		 {
			 dcount++;
			 if (monat1 == 0)
			 {
				 monat1 = monate.mo;
				 continue;
			 }
			 if (monat2 == 0)
			 {
				 monat2 = monate.mo;
				 continue;
			 }
			 if (monat3 == 0)
			 {
				 monat3 = monate.mo;
				 continue;
			 }
			 if (monat4 == 0)
			 {
				 monat4 = monate.mo;
				 continue;
			 }
		 }
		 dbClass.sqlclose(monate_cursor);
		 return dcount;
}

int A_MO_WA_CLASS::dbcount (void)
{

         if (test_upd_cursor == -1)
         {
			dbClass.sqlout ((char *) sys_par_wrt, SQLCHAR, 2);
			int dsqlstatus = dbClass.sqlcomm ("select sys_par_wrt from sys_par where sys_par_nam = \"abwper_par\""); 
			if (dsqlstatus == 0)
			{
				if (atoi(sys_par_wrt) == 1) 
				{
					prepare_per ();
				}
				else
				{
		             prepare ();
				}
			}
			else
			{
	             prepare ();
			} 


         }
         dbClass.sqlopen (count_cursor);
         sqlstatus = dbClass.sqlfetch (count_cursor);
		 if (sqlstatus != 0) return -1 ;
		 anzfelder = 0;
         while (sqlstatus == 0)
         {
			 anzfelder ++;
	         sqlstatus = dbClass.sqlfetch (count_cursor);
         }
         dbClass.sqlopen (count_cursor);
         return anzfelder; 
}

int A_MO_WA_CLASS::leseDS (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }
      fehlercode = dbClass.sqlfetch (count_cursor);
			 ausgabe.kun = a_mo_wa.kun;
			 if (ausgabe.kun_fil == 1)
			 {
				 ausgabe.kun = 0;
				 a_mo_wa.fil = (short) a_mo_wa.kun;
				 a_mo_wa.kun = 0;
				 ausgabe.fil = a_mo_wa.fil;
			 }
			 else
			 {
				 ausgabe.kun = a_mo_wa.kun;
				 ausgabe.fil = 0;
				 sprintf (ausgabe.fil_kla,"%ld",ausgabe.vertr);
			 }

	  if (fehlercode == 0)
      {
		sqlstatus = dbClass.sqlopen (readcursor);
		if (MitWeDirect == 1) dbClass.sqlopen (readcursor_we);
		menge[0] = 0.0;menge[1] = 0.0;menge[2] = 0.0;menge[3] = 0.0;menge[4] = 0.0;
		menge[5] = 0.0;menge[6] = 0.0;menge[7] = 0.0;menge[8] = 0.0;menge[9] = 0.0;
		menge[10] = 0.0;menge[11] = 0.0;menge[12] = 0.0;menge[13] = 0.0;

		umsatz[0] = 0.0;umsatz[1] = 0.0;umsatz[2] = 0.0;umsatz[3] = 0.0;umsatz[4] = 0.0;
		umsatz[5] = 0.0;umsatz[6] = 0.0;umsatz[7] = 0.0;umsatz[8] = 0.0;umsatz[9] = 0.0;
		umsatz[10] = 0.0;umsatz[11] = 0.0;umsatz[12] = 0.0;umsatz[13] = 0.0;

		menge_vj[0] = 0.0;menge_vj[1] = 0.0;menge_vj[2] = 0.0;menge_vj[3] = 0.0;menge_vj[4] = 0.0;
		menge_vj[5] = 0.0;menge_vj[6] = 0.0;menge_vj[7] = 0.0;menge_vj[8] = 0.0;menge_vj[9] = 0.0;
		menge_vj[10] = 0.0;menge_vj[11] = 0.0;menge_vj[12] = 0.0;menge_vj[13] = 0.0;

		umsatz_vj[0] = 0.0;umsatz_vj[1] = 0.0;umsatz_vj[2] = 0.0;umsatz_vj[3] = 0.0;umsatz_vj[4] = 0.0;
		umsatz_vj[5] = 0.0;umsatz_vj[6] = 0.0;umsatz_vj[7] = 0.0;umsatz_vj[8] = 0.0;umsatz_vj[9] = 0.0;
		umsatz_vj[10] = 0.0;umsatz_vj[11] = 0.0;umsatz_vj[12] = 0.0;umsatz_vj[13] = 0.0;




        sqlstatus = dbClass.sqlfetch (readcursor);
		while (sqlstatus == 0)
         {
            if (ausgabe.kun_fil == 0)
			{
				dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
				dbClass.sqlout ((char *) adr_krz, SQLCHAR, 17);
				dbClass.sqlcomm ("select adr.adr_krz from kun,adr where kun = ?  and kun.adr1 = adr.adr");
			}
			else
			{
				dbClass.sqlin ((long *) &ausgabe.fil, SQLSHORT, 0);
				dbClass.sqlout ((char *) adr_krz, SQLCHAR, 17);
				dbClass.sqlcomm ("select adr.adr_krz from fil,adr where fil.fil = ? and fil.adr = adr.adr ");
			}




	            monat = a_mo_wa.mo;
				movon = 0;
				if (a_mo_wa.mo == monat1) monat = 0; 
				if (a_mo_wa.mo == monat2) monat = 1; 
				if (a_mo_wa.mo == monat3) monat = 2; 
				if (a_mo_wa.mo == monat4) monat = 3; 
 
			akt_jr = a_mo_wa.jr;
			akt_mo = a_mo_wa.mo;
             if (a_mo_wa.jr == jr)
			 {
				menge[monat - movon] += a_mo_wa.me_vk_mo ;
				menge[AnzMonate] += a_mo_wa.me_vk_mo ; //Summe
				 if (UmsatzFilEinn == 0)
				 {
					umsatz[monat - movon] += a_mo_wa.ums_vk_mo; 
					umsatz[AnzMonate] += a_mo_wa.ums_vk_mo; //Summe 
				 }

			 }
			 else
			 {
				menge_vj[monat - movon] += a_mo_wa.me_vk_mo ;
				menge_vj[AnzMonate] += a_mo_wa.me_vk_mo ; //Summe
				 if (UmsatzFilEinn == 0)
				 {
					umsatz_vj[monat - movon] += a_mo_wa.ums_vk_mo; 
					umsatz_vj[AnzMonate] += a_mo_wa.ums_vk_mo; //Summe 
				 }
			 }

	         sqlstatus = dbClass.sqlfetch (readcursor);
         }

// Wareneingangswerte mit dazunehmen (aus a_per_we) Direktlieferungen
      if (MitWeDirect == 1 && ausgabe.kun_fil == 1)
	  {
        sqlstatus = dbClass.sqlfetch (readcursor_we);
		while (sqlstatus == 0)
         {
	            monat = a_mo_wa.mo;
				movon = 0;
				if (a_mo_wa.mo == monat1) monat = 0; 
				if (a_mo_wa.mo == monat2) monat = 1; 
				if (a_mo_wa.mo == monat3) monat = 2; 
				if (a_mo_wa.mo == monat4) monat = 3; 
 
             if (a_mo_wa.jr == jr)
			 {
				menge[monat - movon] += a_mo_wa.me_vk_mo ;
				menge[AnzMonate] += a_mo_wa.me_vk_mo ; //Summe
				 if (UmsatzFilEinn == 0)
				 {
					umsatz[monat - movon] += a_mo_wa.ums_vk_mo; 
					umsatz[AnzMonate] += a_mo_wa.ums_vk_mo; //Summe 
				 }

			 }
			 else
			 {
				menge_vj[monat - movon] += a_mo_wa.me_vk_mo ;
				menge_vj[AnzMonate] += a_mo_wa.me_vk_mo ; //Summe
				 if (UmsatzFilEinn == 0)
				 {
					umsatz_vj[monat - movon] += a_mo_wa.ums_vk_mo; 
					umsatz_vj[AnzMonate] += a_mo_wa.ums_vk_mo; //Summe 
				 }
			 }

	         sqlstatus = dbClass.sqlfetch (readcursor_we);
         }
	  }





		monat = movon;
/*** 
		while (monat <= mobis)
		{
			a_mo_wa.mo = monat;
			movon = 0;
			if (a_mo_wa.mo == monat1) monat = 0; 
			if (a_mo_wa.mo == monat2) monat = 1; 
			if (a_mo_wa.mo == monat3) monat = 2; 
			if (a_mo_wa.mo == monat4) monat = 3; 
			if (UmsatzFilEinn == 1 && ausgabe.kun_fil == 1)
			{
				berechnePerioden ();
				ausgabe.einn = 0.0;
				sqlstatus = dbClass.sqlopen (read_fil_einn);
				sqlstatus = dbClass.sqlfetch (read_fil_einn);
				while (sqlstatus == 0)
				{
					if (ausgabe.jr == 2)
					{
						umsatz[monat - movon] += ausgabe.einn; 
						umsatz[AnzMonate] += ausgabe.einn; //Summe 
					}
					else
					{
						umsatz_vj[monat - movon] += ausgabe.einn; 
						umsatz_vj[AnzMonate] += ausgabe.einn; //Summe 
					}
					sqlstatus = dbClass.sqlfetch (read_fil_einn);
				}
			}
			monat ++;
		}
*****/
		// ===== Monat 1 =========
			a_mo_wa.mo = monat1;
			movon = 0;
			if (a_mo_wa.mo == monat1) monat = 0; 
			if (a_mo_wa.mo == monat2) monat = 1; 
			if (a_mo_wa.mo == monat3) monat = 2; 
			if (a_mo_wa.mo == monat4) monat = 3; 
			if (UmsatzFilEinn == 1 && ausgabe.kun_fil == 1 && a_mo_wa.mo > 0)
			{
				berechnePerioden ();
				ausgabe.einn = 0.0;
				sqlstatus = dbClass.sqlopen (read_fil_einn);
				sqlstatus = dbClass.sqlfetch (read_fil_einn);
				while (sqlstatus == 0)
				{
					if (ausgabe.jr == 2)
					{
						umsatz[monat - movon] += ausgabe.einn; 
						umsatz[AnzMonate] += ausgabe.einn; //Summe 
					}
					else
					{
						umsatz_vj[monat - movon] += ausgabe.einn; 
						umsatz_vj[AnzMonate] += ausgabe.einn; //Summe 
					}
					sqlstatus = dbClass.sqlfetch (read_fil_einn);
				}
			}
		// ===== Monat 2 ========= 
			a_mo_wa.mo = monat2;
			movon = 0;
			if (a_mo_wa.mo == monat1) monat = 0; 
			if (a_mo_wa.mo == monat2) monat = 1; 
			if (a_mo_wa.mo == monat3) monat = 2; 
			if (a_mo_wa.mo == monat4) monat = 3; 
			if (UmsatzFilEinn == 1 && ausgabe.kun_fil == 1 && a_mo_wa.mo > 0)
			{
				berechnePerioden ();
				ausgabe.einn = 0.0;
				sqlstatus = dbClass.sqlopen (read_fil_einn);
				sqlstatus = dbClass.sqlfetch (read_fil_einn);
				while (sqlstatus == 0)
				{
					if (ausgabe.jr == 2)
					{
						umsatz[monat - movon] += ausgabe.einn; 
						umsatz[AnzMonate] += ausgabe.einn; //Summe 
					}
					else
					{
						umsatz_vj[monat - movon] += ausgabe.einn; 
						umsatz_vj[AnzMonate] += ausgabe.einn; //Summe 
					}
					sqlstatus = dbClass.sqlfetch (read_fil_einn);
				}
			}
		// ===== Monat 3 =========
			a_mo_wa.mo = monat3;
			movon = 0;
			if (a_mo_wa.mo == monat1) monat = 0; 
			if (a_mo_wa.mo == monat2) monat = 1; 
			if (a_mo_wa.mo == monat3) monat = 2; 
			if (a_mo_wa.mo == monat4) monat = 3; 
			if (UmsatzFilEinn == 1 && ausgabe.kun_fil == 1 && a_mo_wa.mo > 0)
			{
				berechnePerioden ();
				ausgabe.einn = 0.0;
				sqlstatus = dbClass.sqlopen (read_fil_einn);
				sqlstatus = dbClass.sqlfetch (read_fil_einn);
				while (sqlstatus == 0)
				{
					if (ausgabe.jr == 2)
					{
						umsatz[monat - movon] += ausgabe.einn; 
						umsatz[AnzMonate] += ausgabe.einn; //Summe 
					}
					else
					{
						umsatz_vj[monat - movon] += ausgabe.einn; 
						umsatz_vj[AnzMonate] += ausgabe.einn; //Summe 
					}
					sqlstatus = dbClass.sqlfetch (read_fil_einn);
				}
			}
		// ===== Monat 4 =========
			a_mo_wa.mo = monat4;
			movon = 0;
			if (a_mo_wa.mo == monat1) monat = 0; 
			if (a_mo_wa.mo == monat2) monat = 1; 
			if (a_mo_wa.mo == monat3) monat = 2; 
			if (a_mo_wa.mo == monat4) monat = 3; 
			if (UmsatzFilEinn == 1 && ausgabe.kun_fil == 1 && a_mo_wa.mo > 0)
			{
				berechnePerioden ();
				ausgabe.einn = 0.0;
				sqlstatus = dbClass.sqlopen (read_fil_einn);
				sqlstatus = dbClass.sqlfetch (read_fil_einn);
				while (sqlstatus == 0)
				{
					if (ausgabe.jr == 2)
					{
						umsatz[monat - movon] += ausgabe.einn; 
						umsatz[AnzMonate] += ausgabe.einn; //Summe 
					}
					else
					{
						umsatz_vj[monat - movon] += ausgabe.einn; 
						umsatz_vj[AnzMonate] += ausgabe.einn; //Summe 
					}
					sqlstatus = dbClass.sqlfetch (read_fil_einn);
				}
			}

     }
      return fehlercode;
}

int A_MO_WA_CLASS::openDS (void)
{
         return dbClass.sqlopen (readcursor);
}

void A_MO_WA_CLASS::prepare (void)
{

 
  vjr = jr;
  vorjahr = jr - 1;
  if (MitVorjahr)
  {
	  vjr = jr -1;
	  //================ mit Vorjahr ======================
  }

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);

	fil_abrech_cursor = dbClass.sqlcursor ("select end_dat  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= " 
						"(select end_dat from fil_abrech where mdn = ? and fil = 0 "
						"and period_kz = \"M\" and jr = ? and period = ?) "
						"order by end_dat desc ");



	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vorjahr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);

	fil_abrech_cursor_vj = dbClass.sqlcursor ("select end_dat  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= " 
						"(select end_dat from fil_abrech where mdn = ? and fil = 0 "
						"and period_kz = \"M\" and jr = ? and period = ?) "
						"order by end_dat desc ");





    dbRange.RangeIn ("a_mo_wa.fil",filbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_mo_wa.kun",kunbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_mo_wa.mo",mobereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_mo_wa.a",abereich, SQLLONG, 0); 
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);

	dbClass.sqlout ((char *) ausgabe.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((long *) &ausgabe.vertr, SQLLONG, 2);

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
//	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
//	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);

           count_cursor = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_mo_wa.fil > 0 then fil.fil_kla "
							"	when a_mo_wa.fil = 0 then \"\" end klasse , "
							"case when a_mo_wa.kun > 0 then kun.vertr1 "
							"	when a_mo_wa.kun = 0 then 0 end vertr , "

							"case when a_mo_wa.fil = 0 then a_mo_wa.kun "
							"     when a_mo_wa.fil > 0 then a_mo_wa.fil end kunde, "
							"case when a_mo_wa.fil = 0 then 0 "
							"     when a_mo_wa.fil > 0 then 1 end kun_fil, "
//							"a_mo_wa.jr,a_mo_wa.mo, "
							"sum(case when a_bas.me_einh <> 2 then a_mo_wa.me_vk_mo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_mo_wa.me_vk_mo  end ) menge, "
							"sum(a_mo_wa.ums_vk_mo), "
							"sum(a_mo_wa.ums_fil_ek_mo) "
							"from a_mo_wa,a_bas , outer fil, outer kun "
							"where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.fil = fil.fil "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.mdn = ? "
							"and <a_mo_wa.fil> "
							"and <a_mo_wa.kun> "
							"and a_mo_wa.jr between ? and ? "
							"and <a_mo_wa.mo> "
							"and <a_mo_wa.a> "
							"and a_mo_wa.me_vk_mo <> 0.0 "
							"group by 1,2,3,4 order by klasse,vertr,kun_fil,kunde "
							));
	test_upd_cursor = 1;


    dbRange.RangeIn ("a_mo_wa.mo",mobereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_mo_wa.a",abereich, SQLLONG, 0); 
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((long *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);

           readcursor = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_mo_wa.fil = 0 then a_mo_wa.kun "
							"     when a_mo_wa.fil > 0 then a_mo_wa.fil end kunde, "
							"case when a_mo_wa.fil = 0 then 0 "
							"     when a_mo_wa.fil > 0 then 1 end kun_fil, "
							"a_mo_wa.jr, a_mo_wa.mo, "
							"sum(case when a_bas.me_einh <> 2 then a_mo_wa.me_vk_mo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_mo_wa.me_vk_mo  end ) menge, "
							"sum(a_mo_wa.ums_vk_mo), "
							"sum(a_mo_wa.ums_fil_ek_mo) "
							"from a_mo_wa,a_bas where a_mo_wa.a = a_bas.a "
							"and a_mo_wa.mdn = ? "
							"and a_mo_wa.fil = ? "
							"and a_mo_wa.kun = ? "
							"and a_mo_wa.jr between ? and ? "
							"and <a_mo_wa.mo> "
							"and <a_mo_wa.a> "
							"and a_mo_wa.me_vk_mo <> 0.0 "
							"group by 1,2,3,4 order by kun_fil,kunde "
							));
 
if (UmsatzFilEinn == 1)
{
	dbClass.sqlin ((char *) datumvj_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumvj_bis, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumjr_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumjr_bis, SQLCHAR, 11);

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
	dbClass.sqlin ((char *) datumvj_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumjr_bis, SQLCHAR, 11);
	dbClass.sqlin ((char *) datummo_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datummo_bis, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumvjmo_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumvjmo_bis, SQLCHAR, 11);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);   
	dbClass.sqlout ((double *) &ausgabe.einn, SQLDOUBLE, 0);

	// cursor f�r Monat (z.b. bei mo = 6 : nur Monat 6)
	// 2 * fetch , f�r vjr und jr
           read_fil_einn = dbClass.sqlcursor ("select "
							"case when einn_dat between ? and ? then 1 "
							"when einn_dat between  ? and ? then 2 end jr, " 
							"sum(fil_einn.einn) "
							"from fil_einn where "
							"    fil_einn.mdn = ? "
							"and fil_einn.fil = ? "
							"and fil_einn.einn_dat between ? and ? "
							"and ( "
							 "   fil_einn.einn_dat between ? and ? or "
							 "   fil_einn.einn_dat between ? and ? "
							 " ) "
							"group by 1 order by jr "
							);
}
if (MitWeDirect == 1)
{
    dbRange.RangeIn ("a_mo_wa.mo",mobereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_mo_wa.a",abereich, SQLLONG, 0); 
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);

           readcursor_we = dbClass.sqlcursor (dbRange.SetRange ("select "
							"a_mo_we.fil, "
							"a_mo_we.jr, a_mo_we.mo, "
							"sum(case when a_bas.me_einh <> 2 then a_mo_we.me_ek_mo * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_mo_we.me_ek_mo  end ) menge, "
							"sum (a_mo_we.ums_fil_vk_mo / ptabn.ptwer2 ), "
							"sum(a_mo_we.ums_fil_ek_mo) "
							"from a_mo_we,a_bas, ptabn where a_mo_we.a = a_bas.a "
							"and a_mo_we.mdn = ? "
							"and a_mo_we.fil = ? "
							"and a_mo_we.jr between ? and ? "
							"and <a_mo_we.mo> "
							"and <a_mo_we.a> "
							"and a_mo_we.me_vk_mo <> 0.0 "
							"and ptabn.ptitem = \"mwst\" and ptabn.ptlfnr = a_bas.mwst "
							"group by 1,2,3 order by jr,mo,fil "
							));
}


}
void A_MO_WA_CLASS::prepare_per (void)
{
  vjr = jr;
  vorjahr = jr - 1;
  if (MitVorjahr)
  {
	  vjr = jr -1;
	  //================ mit Vorjahr ======================
  }

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);

	fil_abrech_cursor = dbClass.sqlcursor ("select end_dat  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= " 
						"(select end_dat from fil_abrech where mdn = ? and fil = 0 "
						"and period_kz = \"M\" and jr = ? and period = ?) "
						"order by end_dat desc ");



	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vorjahr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);

	fil_abrech_cursor_vj = dbClass.sqlcursor ("select end_dat  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= " 
						"(select end_dat from fil_abrech where mdn = ? and fil = 0 "
						"and period_kz = \"M\" and jr = ? and period = ?) "
						"order by end_dat desc ");



    dbRange.RangeIn ("a_per_wa.fil",filbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_per_wa.kun",kunbereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_per_wa.period",mobereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_per_wa.a",abereich, SQLLONG, 0); 
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);

	dbClass.sqlout ((char *) ausgabe.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((long *) &ausgabe.vertr, SQLLONG, 2);

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
//	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
//	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);

           count_cursor = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_per_wa.fil > 0 then fil.fil_kla "
							"	when a_per_wa.fil = 0 then \"\" end klasse , "
							"case when a_per_wa.kun > 0 then kun.vertr1 "
							"	when a_per_wa.kun = 0 then 0 end vertr , "

							"case when a_per_wa.fil = 0 then a_per_wa.kun "
							"     when a_per_wa.fil > 0 then a_per_wa.fil end kunde, "
							"case when a_per_wa.fil = 0 then 0 "
							"     when a_per_wa.fil > 0 then 1 end kun_fil, "
//							"a_per_wa.jr,a_per_wa.period, "
							"sum(case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end ) menge, "
							"sum(a_per_wa.ums_vk_per), "
							"sum(a_per_wa.ums_fil_ek_per) "
							"from a_per_wa,a_bas , outer fil, outer kun "
							"where a_per_wa.a = a_bas.a "
							"and a_per_wa.fil = fil.fil "
							"and a_per_wa.kun = kun.kun "
							"and a_per_wa.mdn = ? "
							"and <a_per_wa.fil> "
							"and <a_per_wa.kun> "
							"and a_per_wa.jr between ? and ? "
							"and <a_per_wa.period> "
							"and <a_per_wa.a> "
							"and a_per_wa.me_vk_per <> 0.0 "
							"group by 1,2,3,4 order by klasse,vertr,kun_fil,kunde "
							));
	test_upd_cursor = 1;


    dbRange.RangeIn ("a_per_wa.period",mobereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_per_wa.a",abereich, SQLLONG, 0); 
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);

           readcursor = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_per_wa.fil = 0 then a_per_wa.kun "
							"     when a_per_wa.fil > 0 then a_per_wa.fil end kunde, "
							"case when a_per_wa.fil = 0 then 0 "
							"     when a_per_wa.fil > 0 then 1 end kun_fil, "
							"a_per_wa.jr, a_per_wa.period, "
							"sum(case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end ) menge, "
							"sum(a_per_wa.ums_vk_per), "
							"sum(a_per_wa.ums_fil_ek_per) "
							"from a_per_wa,a_bas where a_per_wa.a = a_bas.a "
							"and a_per_wa.mdn = ? "
							"and a_per_wa.fil = ? "
							"and a_per_wa.kun = ? "
							"and a_per_wa.jr between ? and ? "
							"and <a_per_wa.period> "
							"and <a_per_wa.a> "
							"and a_per_wa.me_vk_per <> 0.0 "
							"group by 1,2,3,4 order by jr,period,kun_fil,kunde "
							));

		   

if (UmsatzFilEinn == 1)
{
	dbClass.sqlin ((char *) datumvj_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumvj_bis, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumjr_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumjr_bis, SQLCHAR, 11);

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
	dbClass.sqlin ((char *) datumvj_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumjr_bis, SQLCHAR, 11);
	dbClass.sqlin ((char *) datummo_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datummo_bis, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumvjmo_von, SQLCHAR, 11);
	dbClass.sqlin ((char *) datumvjmo_bis, SQLCHAR, 11);

	dbClass.sqlout ((short *) &ausgabe.jr, SQLSHORT, 0);   
	dbClass.sqlout ((double *) &ausgabe.einn, SQLDOUBLE, 0);

	// cursor f�r Monat (z.b. bei mo = 6 : nur Monat 6)
	// 2 * fetch , f�r vjr und jr
           read_fil_einn = dbClass.sqlcursor ("select "
							"case when einn_dat between ? and ? then 1 "
							"when einn_dat between  ? and ? then 2 end jr, " 
							"sum(fil_einn.einn) "
							"from fil_einn where "
							"    fil_einn.mdn = ? "
							"and fil_einn.fil = ? "
							"and fil_einn.einn_dat between ? and ? "
							"and ( "
							 "   fil_einn.einn_dat between ? and ? or "
							 "   fil_einn.einn_dat between ? and ? "
							 " ) "
							"group by 1 order by jr "
							);
}
if (MitWeDirect == 1)
{
    dbRange.RangeIn ("a_per_we.period",mobereich, SQLLONG, 0); 
    dbRange.RangeIn ("a_per_we.a",abereich, SQLLONG, 0); 
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &a_mo_wa.fil, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);

	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_vk_mo, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.ums_fil_ek_mo, SQLDOUBLE, 0);

           readcursor_we = dbClass.sqlcursor (dbRange.SetRange ("select "
							"a_per_we.fil, "
							"a_per_we.jr, a_per_we.period, "
							"sum(case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
							"    when a_bas.me_einh = 2  then a_per_we.me_ek_per  end ) menge, "
							"sum (a_per_we.ums_fil_vk_per / ptabn.ptwer2 ), "
							"sum(a_per_we.ums_fil_ek_per) "
							"from a_per_we,a_bas, ptabn where a_per_we.a = a_bas.a "
							"and a_per_we.mdn = ? "
							"and a_per_we.fil = ? "
							"and a_per_we.jr between ? and ? "
							"and <a_per_we.period> "
							"and <a_per_we.a> "
							"and a_per_we.me_ek_per <> 0.0 "
							"and ptabn.ptitem = \"mwst\" and ptabn.ptlfnr = a_bas.mwst "
							"group by 1,2,3 order by jr,period,fil "
							));
}






}


void A_MO_WA_CLASS::berechnePerioden (void)
{
	// n�tig wegen Umsatz aus fil_einn
//========================================================================
//============= Hole Perioden im lfd-Jahr ================================
//========================================================================
	    wo1 = 0; wo2 = 0; wo3 = 0; wo4 = 0; wo5 = 0;
	    wo1_vj = 0; wo2_vj = 0; wo3_vj = 0; wo4_vj = 0; wo5_vj = 0;
		strcpy(erster_tag_wo1,"");
		strcpy(erster_tag_wo2,"");
		strcpy(erster_tag_wo3,"");
		strcpy(erster_tag_wo4,"");
		strcpy(erster_tag_wo5,"");
		strcpy(letzter_tag_wo1,""); 
		strcpy(letzter_tag_wo2,"");
		strcpy(letzter_tag_wo3,"");
		strcpy(letzter_tag_wo4,"");
		strcpy(letzter_tag_wo5,"");

         dbClass.sqlopen (fil_abrech_cursor);
	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day; 

		d_datlast.SetzeDatum(1,1,_jr);
		d_datlast.format(datumjr_von);

		d_datlast.SetzeDatum(_tag,_mo,_jr);
		d_datlast.format(datummo_bis);
		d_datlast.format(datumjr_bis);
		--d_datlast;
		d_datlast.format(letzter_tag);
		letzte_wo = get_woche(letzter_tag);
		_wo = letzte_wo;
		while (letzte_wo == _wo)
		{
			++d_datlast;
			d_datlast.format(letzter_tag);
			_wo = get_woche(letzter_tag);
		}
		--d_datlast;
		d_datlast.format(letzter_tag);//Dies ist jetzt der letzte Tag der KW

	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;
		d_datfirst.SetzeDatum(_tag,_mo,_jr);
		++d_datfirst;
		++d_datfirst;
		d_datfirst.format(erster_tag);
		_jr = get_jahr(erster_tag); 
		erste_wo = get_woche(erster_tag);
		_wo = erste_wo;
		flg_wo1_gekappt = FALSE;
		while (erste_wo == _wo)
		{
			--d_datfirst;
			d_datfirst.format(erster_tag);
			if (get_jahr(erster_tag) != _jr && flg_wo1_gekappt == FALSE) 
			{
				++d_datfirst;
				d_datfirst.format(erster_tag_wo1);
				d_datfirst.format(datummo_von);
				flg_wo1_gekappt = TRUE;
				--d_datfirst;
			}
			_wo = get_woche(erster_tag);
		}
		++d_datfirst;
		d_datfirst.format(erster_tag);//Dies ist jetzt der 1. Tag der KW
		if (flg_wo1_gekappt == FALSE) d_datfirst.format(datummo_von);
		wo1 = get_woche(erster_tag); 


		d_dat=d_datfirst;

		if (flg_wo1_gekappt == FALSE) strncpy(erster_tag_wo1,erster_tag,10);
		d_dat=d_dat+6;
		d_dat.format(letzter_tag_wo1);
		++d_dat;
		while (d_datlast-d_dat > 0)
		{
			if (wo2 == 0) 
			{
				d_dat.format(erster_tag_wo2);
				wo2 = get_woche(erster_tag_wo2);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo2);
			}
			else if (wo3 == 0)
			{
				d_dat.format(erster_tag_wo3);
				wo3 = get_woche(erster_tag_wo3);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo3);
			}
			else if (wo4 == 0)
			{
				d_dat.format(erster_tag_wo4);
				wo4 = get_woche(erster_tag_wo4);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo4);
				d_dat.format(datummo_bis);
				d_dat.format(datumjr_bis);
			}
			else if (wo5 == 0)
			{
				d_dat.format(erster_tag_wo5);
				wo5 = get_woche(erster_tag_wo5);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo5);
				d_dat.format(datummo_bis);
				d_dat.format(datumjr_bis);
			}
			++d_dat;
		}

//========================================================================
//============= Hole Perioden im Vorjahr ================================
//========================================================================
		strcpy(erster_tag_wo1_vj,"");
		strcpy(erster_tag_wo2_vj,"");
		strcpy(erster_tag_wo3_vj,"");
		strcpy(erster_tag_wo4_vj,"");
		strcpy(erster_tag_wo5_vj,"");
		strcpy(letzter_tag_wo1_vj,"");
		strcpy(letzter_tag_wo2_vj,"");
		strcpy(letzter_tag_wo3_vj,"");
		strcpy(letzter_tag_wo4_vj,"");
		strcpy(letzter_tag_wo5_vj,"");

         dbClass.sqlopen (fil_abrech_cursor_vj);
	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor_vj);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;

		d_datlast.SetzeDatum(1,1,_jr);
		d_datlast.format(datumvj_von);
		d_datlast.format(datumvj_bis);


		d_datlast.SetzeDatum(_tag,_mo,_jr);
		d_datlast.format(datumvjmo_bis);
		d_datlast.format(datumvj_bis);
		--d_datlast;
		d_datlast.format(letzter_tag);
		letzte_wo = get_woche(letzter_tag);
		_wo = letzte_wo;
		while (letzte_wo == _wo)
		{
			++d_datlast;
			d_datlast.format(letzter_tag);
			_wo = get_woche(letzter_tag);
		}
		--d_datlast;
		d_datlast.format(letzter_tag);//Dies ist jetzt der letzte Tag der KW

	    sqlstatus = dbClass.sqlfetch (fil_abrech_cursor_vj);
		_jr = end_dat.year;
		_mo = end_dat.month;
		_tag = end_dat.day;
		d_datfirst.SetzeDatum(_tag,_mo,_jr);
		++d_datfirst;
		d_datfirst.format(erster_tag);
		_jr = get_jahr(erster_tag); 
		d_datfirst.format(datumvjmo_von);
		erste_wo = get_woche(erster_tag);
		_wo = erste_wo;
		flg_wo1vj_gekappt = FALSE;
		while (erste_wo == _wo)
		{
			--d_datfirst;
			d_datfirst.format(erster_tag);
			if (get_jahr(erster_tag) != _jr && flg_wo1vj_gekappt == FALSE) 
			{
				++d_datfirst;
				d_datfirst.format(erster_tag_wo1_vj);
				d_datfirst.format(datumvjmo_von);
				flg_wo1vj_gekappt = TRUE;
				--d_datfirst;
				break;
			}
			_wo = get_woche(erster_tag);
		}
		++d_datfirst;
		d_datfirst.format(erster_tag);//Dies ist jetzt der 1. Tag der KW
		wo1_vj = get_woche(erster_tag);


		d_dat=d_datfirst;

		if (flg_wo1vj_gekappt == FALSE) strncpy(erster_tag_wo1_vj,erster_tag,10);
		d_dat=d_dat+6;
		d_dat.format(letzter_tag_wo1_vj);
		++d_dat;
		while (d_datlast-d_dat > 0)
		{
			if (wo2_vj == 0) 
			{
				d_dat.format(erster_tag_wo2_vj);
				wo2_vj = get_woche(erster_tag_wo2_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo2_vj);
			}
			else if (wo3_vj == 0)
			{
				d_dat.format(erster_tag_wo3_vj);
				wo3_vj = get_woche(erster_tag_wo3_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo3_vj);
			}
			else if (wo4_vj == 0)
			{
				d_dat.format(erster_tag_wo4_vj);
				wo4_vj = get_woche(erster_tag_wo4_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo4_vj);
				d_dat.format(datumvjmo_bis);
				d_dat.format(datumvj_bis);
			}
			else if (wo5_vj == 0)
			{
				d_dat.format(erster_tag_wo5_vj);
				wo5_vj = get_woche(erster_tag_wo5_vj);
				d_dat=d_dat+6;
				d_dat.format(letzter_tag_wo5_vj);
				d_dat.format(datumvjmo_bis);
				d_dat.format(datumvj_bis);
			}
			++d_dat;
		}




		woche_jr = jr;
		if (erste_wo > letzte_wo) woche_jr --;

}
