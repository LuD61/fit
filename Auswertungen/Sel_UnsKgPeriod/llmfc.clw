; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDialogDatum
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "llmfc.h"
LastPage=0

ClassCount=6
Class1=CLlSampleApp
Class2=CAboutDlg
Class3=CLlSampleDoc
Class4=CLlSampleView
Class5=CMainFrame

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Class6=CDialogDatum
Resource3=IDD_DIALOG1

[CLS:CLlSampleApp]
Type=0
BaseClass=CWinApp
HeaderFile=llmfc.h
ImplementationFile=llmfc.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=llmfc.cpp
ImplementationFile=llmfc.cpp
LastObject=CAboutDlg

[CLS:CLlSampleDoc]
Type=0
BaseClass=CDocument
HeaderFile=llsmpdoc.h
ImplementationFile=llsmpdoc.cpp

[CLS:CLlSampleView]
Type=0
BaseClass=CView
HeaderFile=llsmpvw.h
ImplementationFile=llsmpvw.cpp

[CLS:CMainFrame]
Type=0
BaseClass=CFrameWnd
HeaderFile=mainfrm.h
ImplementationFile=mainfrm.cpp

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=6
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDOK,button,1342373889
Control6=IDC_STATIC,static,1342308352

[DLG:IDD_DIALOG1]
Type=1
Class=CDialogDatum
ControlCount=15
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_WOVON,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_JAHR,edit,1350631552
Control9=IDC_MANDANT,edit,1350631552
Control10=IDC_FILIALE,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_AVON,edit,1350631552
Control13=IDC_STATIC,static,1342308352
Control14=IDC_KUNDE,edit,1350631552
Control15=IDC_VJ,button,1342242819

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_START_DEBUG
Command2=ID_FILE_STOP_DEBUG
Command3=ID_APP_EXIT
Command4=ID_EDIT_LIST
Command5=ID_EDIT_LABEL
Command6=ID_PRINT_REPORT
Command7=ID_PRINT_LABEL
Command8=ID_APP_ABOUT
CommandCount=8

[ACL:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_NEXT_PANE
Command5=ID_PREV_PANE
CommandCount=5

[CLS:CDialogDatum]
Type=0
HeaderFile=DialogDatum.h
ImplementationFile=DialogDatum.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CDialogDatum

