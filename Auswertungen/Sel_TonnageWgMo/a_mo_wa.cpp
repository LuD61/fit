#include "stdafx.h"
#include "DbClass.h"
#include "a_mo_wa.h"
#include "cmbtl9.h"

struct A_MO_WA a_mo_wa, a_mo_wa_null;
struct AUSGABE ausgabe, ausgabe_null;
struct A_BAS a_bas, a_bas_null;
extern DB_CLASS dbClass;
static long anzfelder = -1;
double menge[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
double menge_vj[] = {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0   ,0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

short mdn = 1;
short filvon = 0 ;
short filbis = 9999;
short jr = 2008;
short vjr = 0;
short movon ;
short mobis ;
short monat ;
char wg_bz1 [25];
double a_gew;
short a_bas_ag ;
short a_bas_dr_folge;
short a_bas_me_einh;
short a_bas_lief_einh;
double a_bas_a_gew;
char rez_preis_bz[12];
char a_bz1[25];
char einh_bz[9];
double rez_preis;
extern char ListennameLaengs[128];
extern char ListennameQuer[128];
char Listenname_vgl[128];
char Listenname_vgleinzel[128];
extern int MitVorjahr;


int A_MO_WA_CLASS::dbcount (void)
/**
Tabelle eti lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         sqlstatus = dbClass.sqlfetch (count_cursor);
		 if (sqlstatus != 0) return -1 ;
		 anzfelder = 0;
         while (sqlstatus == 0)
         {
			 anzfelder ++;
	         sqlstatus = dbClass.sqlfetch (count_cursor);
         }
         dbClass.sqlopen (count_cursor);
         return anzfelder; 
}

int A_MO_WA_CLASS::leseDS (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }
      fehlercode = dbClass.sqlfetch (count_cursor);
	  if (fehlercode == 0)
      {
		sqlstatus = dbClass.sqlopen (readcursor);
		menge[0] = 0.0;menge[1] = 0.0;menge[2] = 0.0;menge[3] = 0.0;menge[4] = 0.0;
		menge[5] = 0.0;menge[6] = 0.0;menge[7] = 0.0;menge[8] = 0.0;menge[9] = 0.0;
		menge[10] = 0.0;menge[11] = 0.0;menge[12] = 0.0;menge[13] = 0.0;

		menge_vj[0] = 0.0;menge_vj[1] = 0.0;menge_vj[2] = 0.0;menge_vj[3] = 0.0;menge_vj[4] = 0.0;
		menge_vj[5] = 0.0;menge_vj[6] = 0.0;menge_vj[7] = 0.0;menge_vj[8] = 0.0;menge_vj[9] = 0.0;
		menge_vj[10] = 0.0;menge_vj[11] = 0.0;menge_vj[12] = 0.0;menge_vj[13] = 0.0;

        sqlstatus = dbClass.sqlfetch (readcursor);
		while (sqlstatus == 0)
         {
             monat = a_mo_wa.mo;
             if (a_mo_wa.jr == jr)
			 {
				 if (a_bas.me_einh != 2)
				 {
					menge[monat - movon] += a_mo_wa.me_vk_mo * a_bas.a_gew;
					menge[mobis - movon + 1] += (a_mo_wa.me_vk_mo  * a_bas.a_gew) ; //Summe
				 }
				 else
				{
					menge[monat - movon] += a_mo_wa.me_vk_mo ;
					menge[mobis - movon + 1] += a_mo_wa.me_vk_mo ; //Summe
				}
			 }
			 else
			 {
				 if (a_bas.me_einh != 2)
				 {
					menge_vj[monat - movon] += a_mo_wa.me_vk_mo * a_bas.a_gew;
					menge_vj[mobis - movon + 1] += (a_mo_wa.me_vk_mo  * a_bas.a_gew) ; //Summe
				 }
				 else
				{
					menge_vj[monat - movon] += a_mo_wa.me_vk_mo ;
					menge_vj[mobis - movon + 1] += a_mo_wa.me_vk_mo ; //Summe
				}
			 }
	         sqlstatus = dbClass.sqlfetch (readcursor);
         }

      }
      return fehlercode;
}

int A_MO_WA_CLASS::openDS (void)
{
         return dbClass.sqlopen (readcursor);
}

void A_MO_WA_CLASS::prepare (void)
{

 if (MitVorjahr)
  {
	  vjr = jr -1;
	  //================ mit Vorjahr ======================

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select wg.wg, wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,wg  where a_mo_wa.a = a_bas.a and a_bas.wg = wg.wg "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ? and a_mo_wa.me_vk_mo <> 0.0 "
							"group by 1,2  order by 1  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &vjr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.wg, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_bas.me_einh, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_bas.a_gew, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);

            readcursor = dbClass.sqlcursor ("select wg.wg, a_mo_wa.jr,a_mo_wa.mo,a_bas.me_einh,a_bas.a_gew, sum(a_mo_wa.me_vk_mo) " 
                            "from a_mo_wa,a_bas,wg where a_mo_wa.a = a_bas.a and a_bas.wg = wg.wg "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr between ? and ? "
							"and a_mo_wa.mo between ? and ?  "
							"and wg.wg = ?  and a_mo_wa.me_vk_mo <> 0.0 "
							"group by 1,2,3,4,5 order by 1,2,3  "
							);

 }

  else
  {
	  //================ ohne Vorjahr ======================
  
	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((char *) wg_bz1, SQLCHAR, 25);


    count_cursor = dbClass.sqlcursor ("select wg.wg, wg.wg_bz1 " 
                            "from a_mo_wa,a_bas,wg  where a_mo_wa.a = a_bas.a and a_bas.wg = wg.wg "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr = ? and a_mo_wa.mo between ? and ? and a_mo_wa.me_vk_mo <> 0.0 "
							"group by 1,2  order by 1  "
							);

	test_upd_cursor = 1;


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &ausgabe.wg, SQLSHORT, 0);

	dbClass.sqlout ((short *) &ausgabe.wg, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_mo_wa.mo, SQLSHORT, 0);
	dbClass.sqlout ((short *) &a_bas.me_einh, SQLSHORT, 0);
	dbClass.sqlout ((double *) &a_bas.a_gew, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &a_mo_wa.me_vk_mo, SQLDOUBLE, 0);

            readcursor = dbClass.sqlcursor ("select wg.wg,a_mo_wa.jr, a_mo_wa.mo,a_bas.me_einh,a_bas.a_gew, sum(a_mo_wa.me_vk_mo) " 
                            "from a_mo_wa,a_bas,wg where a_mo_wa.a = a_bas.a and a_bas.wg = wg.wg "
							"and a_mo_wa.mdn = ? and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.jr = ? and a_mo_wa.mo between ? and ?  "
							"and wg.wg = ?  and a_mo_wa.me_vk_mo <> 0.0 "
							"group by 1,2,3,4,5 order by 1,2,3  "
							);

	}
}