#include <afxdisp.h>
#include "datum.h"
short mon_tab[12]={0,31,28,31,30,31,30,31,31,30,31,30}; /* in get_lauf_tag()*/

DATUM::DATUM()
{
  tag=1; monat=1; jahr=1900;
}

DATUM::DATUM(int t, int m, int j)
{
  jahr=j; monat=m; tag=t;
}

void DATUM::operator=(char* cdat) //LuD 060204
{
//erwartet wird hier dd.mm.yyyy sollte noch abgesichert serden! LuD 060204
  char ctag[3] ;
  char cmo[3];
  char cjr[5];
  char ch[12];

int  cha;
char *pdest;
int result;
	  strcpy (ch,cdat);
	  strcpy (ctag,"");
      strcpy (cmo,"");
      strcpy (cjr,"");
cha = '.';
   //vorw�rtz suchen
   pdest = strchr( cdat, cha );
   result = pdest - cdat + 1;
   if( pdest == NULL )
   {
		cha = '-';
	    pdest = strchr( cdat, cha );
		result = pdest - cdat + 1;
   }
   if( pdest == NULL )
   {
	  tag = 1;
	  monat = 1;
      jahr = 1900;
	  return;
   }
   if (result == 3)
   {
	//erwartet wird hier dd.mm.yyyy oder dd.mm.yy 

	  strncat(ctag,&ch[0], 2);
	  strncat(cmo,&ch[3], 2);
	//rueckw. suchen
	  pdest = strrchr( cdat, cha );
      result = pdest - cdat + 1;
	  if (result == 6)
      {
	      strncat(cjr,&ch[6],4);
      }
	  else
      {
	      strncat(cjr,&ch[6],2);
      }
   }
   else
   {
	//erwartet wird hier yyyy-mm-dd  

	  strncat(cjr,&ch[0], 4);
	  strncat(cmo,&ch[5], 2);
      strncat(ctag,&ch[8],2);
   }	   
      
  tag = atoi(ctag);
  monat = atoi(cmo);
  jahr = atoi(cjr);
  if (jahr < 100) jahr = jahr + 2000;

}

char* DATUM::format() //LuD 060204
{
  char *s;
  sprintf(s,"%02d.%02d.%4d\n",tag,monat,jahr);
  return s; 
}

char* DATUM::format(char* formatstring) //LuD 060204
{
	//m�gliche Format siehe : strftime
  const char *cs;
  char *s ;
  short std = 00, min = 00;
  CTime t( jahr, monat, tag, std, min, 0 );

//CString cs = t.Format( "%A, %d. %B %Y" );
  CString cstr = t.Format( formatstring );
  cs = LPCSTR (cstr);
  s = _strdup (cs);
//  strcpy (s1, s); f�llt hier auf die Nase !!!! 
//  free(s);
  return s; 
}

/***********************************************
Die folgende private-Methode (kann nur von
Klassenmethoden aufgerufen werden) ermittelt,
wieviele Tage das als Parameter �bergebene Monat
hat.
Parameter:
  m: Monat
  j: Jahr
************************************************/
int DATUM::AnzahlTage(int m, int j)
{
  int anzahl;
  switch (m)
  {
    case 4: case 6: case 9: case 11:
      anzahl = 30; break;
    case 2:
      if (j % 4 == 0)
	anzahl = 29;
      else
	anzahl = 28;
      break;
    default:
      anzahl = 31;
  }
  return anzahl;
}
/*************************************************
Die private-Methode TageImJahr ermittelt, wieviele
das als Parameter �bergebene Jahr hat. Schaltjahre
sind Jahre, die durch 4 teilbar sind.
Parameter:
  int j: Jahreszahl (ab 1900)
*************************************************/
int DATUM::TageImJahr(int j)
{
  if (j % 4 == 0)
    return 366;
  else
    return 365;
}
/*********************************************
Die private-Methode TMJ_Nummer ermittelt,
wieviele Tage seit dem 1. 1. 1900 vergangen
sind. Diese Methode wird f�r die sp�ter
definierten Datumsoperationen verwendet.
Die Berechnung erfolgt sehr intuitiv und
ohne tiefere mathematischen Optimierungen.
Parameter: keine
*********************************************/
long int DATUM::TMJ_Nummer()
{
  long int num;
  int i;
  num=0;
  for (i=1900; i<jahr; i++)
    num=num+TageImJahr(i);
  for (i=1; i<monat; i++)
    num=num+AnzahlTage(i,jahr);
  num=num+tag;
  return num;
}
/*****************************************
Die private-Methode Nummer_TMJ f�r die
zur Methode TMJ_Nummer inverse Operation
durch. Es wird das Datum ermittelt, f�r
das seit dem 1. 1. 1900 num Tage vergangen
sind. Auch diese Methode wurde ohne viel
Mathematik geradlinig programmiert.
Parameter:
  num: Tage seit dem 1. 1. 1900
*****************************************/
void DATUM::Nummer_TMJ(long int num)
{
  // Jahr ermitteln
  jahr=1900;
  while (num>TageImJahr(jahr))
  {
    num=num-TageImJahr(jahr);
    jahr++;
  }
  // Monat ermitteln
  monat=1;
  while (num>AnzahlTage(monat,jahr))
  {
    num=num-AnzahlTage(monat,jahr);
    monat++;
  }
  tag=num;
}

/************************************************
Die folgende Methode setzt das Datum entsprechend
den Parameterwerten.
Parameter:
  t: Tag
  m: Monat
  j: Jahr
************************************************/
DATUM DATUM::SetzeDatum(int t, int m, int j)
{
  tag=t; monat=m; jahr=j;
  return *this;
}

/*******************************************************
Der Prefix-Operator ++ erh�ht das im Objekt gespeicherte
Datum um 1 Tag. Die Methode liefert das neue Datum
als R�ckgabewert.
*******************************************************/
DATUM DATUM::operator++()
{
  long int num;
  num=TMJ_Nummer()+1;
  Nummer_TMJ(num);
  return *this;
}
/****************************************************
Der Prefix-Operator -- ermittelt den Vortag des im
Objekt gespeicherten Datums. Die Methode liefert das
neue Datum als R�ckgabewert.
****************************************************/
DATUM DATUM::operator--()
{
  long int num;
  num=TMJ_Nummer()-1;
  Nummer_TMJ(num);
  return *this;
}
/****************************************************
Der bin�re Operator + ermittelt das Datum, das t Tage
nach dem im Objekt gespeicherten Datum liegt.
Das neue Datum wird als R�ckgabewert geliefert.
Parameter:
  long int t: Anzahl der Tage
****************************************************/
DATUM DATUM::operator+(long int t)
{
  DATUM temp;
  long int num;
  num=TMJ_Nummer()+t;
  temp.Nummer_TMJ(num);
  return temp;
}
/****************************************************
Der bin�re Operator - ermittelt das Datum, das t Tage
vor dem im Objekt gespeicherten Datum liegt.
Das neue Datum wird als R�ckgabewert geliefert.
Parameter:
  long int t: Anzahl der Tage
****************************************************/
DATUM DATUM::operator-(long int t)
{
  DATUM temp;
  long int num;
  num=TMJ_Nummer()-t;
  temp.Nummer_TMJ(num);
  return temp;
}
/*********************************************************
Der bin�re Operator - ermittelt die Differenz in Tagen
zwischen dem im Objekt gespeicherten und dem als Parameter
�bergebenen Datum.
Parameter:
  DATUM d: der rechte Operand in der bin�ren Operation -
*********************************************************/
long int DATUM::operator- (DATUM d)
{
  long int num;
  num=TMJ_Nummer();
  num=num-d.TMJ_Nummer();
  return num;
}

long int DATUM::operator- (char* cdat)
{
  long int num;
  DATUM d ;
  d = cdat;
  num=TMJ_Nummer();
  num=num-d.TMJ_Nummer();
  return num;
}

// FUNKTIONEN au�erhalb der Klasse DATUM
// 






/**********************************************/
/*    Funkton liefert Kalenderwoche im Jahr   */
/*           52(53)     vom Vorjahr           */
/*           1 ..52(53) im Jahr               */
/*           1          fuer Neujahr(im Dez)  */
/**********************************************/
short get_woche(char *datum)
{
short woche;
short jahr;
char  buff[16];

woche = woche_pre(datum);
if(woche == 0) /* Korrektur bei Anfang des Jahres */
	{
	jahr = get_jahr(datum);
	if(jahr == 0)
		jahr = 99;
	else
		jahr--;
	sprintf(buff,"31.12.%02d",jahr);
        woche = woche_pre(buff);
	}
return(woche);
}/* end of get_woche() */

/****static Funktionen fuer get_woche********/
/****static get_woche_pre********************/
static short woche_pre(char *datum)
{
short woche,jahr,rest;
short korr_neu;
short l_tag;

l_tag = get_lauf_tag(datum);
jahr = get_jahr(datum);
korr_neu = woche_korr(jahr,NEU);
woche = (short)(l_tag / 7);
rest  = (short)(l_tag % 7);

if(rest == 0)
	{
	woche--;
	rest = 7;
	}
if(korr_neu <= 4)
	woche++;
if(korr_neu > 1)
	if(rest >= ((short)9 - korr_neu))
		woche++;
if(woche == 53)
	{
	if(woche_korr(jahr,ALT) <= 3)
		woche = 1;
	}
if(woche == 0)
	{
	}
return(woche);
} /* end of static woche_pre() */

/***********static woche_korr***************/
/*          Basis = 01.01.90               */

static short woche_korr(short jahr_ein,short par)
{
short jahr;
short wochentag;
short i;
int   rest=0;
int   tage=0;

jahr = jahr_ein;
if(par == ALT) /* Ende des Jahres */
	jahr++;
if(jahr < 90 )
	jahr += 100;

for(i=90;i<jahr;i++)
	{
	rest = (int)(i % 4);
	if(rest != 0)
		tage += 365;
	else
		tage += 366;
	}
if(par == NEU) /* Anfang des Jahres */
	tage++;

wochentag = (short)(tage % 7);
if(wochentag == 0)
	return(7);

return(wochentag);
}/* end of static woche_korr() */

/*    Funkton liefert jahr (short)     */
/***************************************/
short get_jahr(char *datum)
{
char  dat[16];
strcpy(dat,datum);
if(strlen(dat) == 8)
      return((short)(atoi(&dat[6])));
else
      return((short)(atoi(&dat[8])));
}

/********************************************/
/*    Funkton liefert laufenden Tag im Jahr */
/********************************************/
short get_lauf_tag(char *datum)
{
short i,l_tag=0,tag,mon;
mon=get_monat(datum);
tag=get_tag(datum);
for(i=1;i<mon;i++)
    {
    l_tag += mon_tab[i];
    }
if(mon > 2)
     l_tag += tag + get_schalt_jahr(get_jahr(datum));
else
     l_tag += tag;

return(l_tag);
}
/********************************************/
/*    Funkton liefert 1/0 wenn Schalt-Jahr  */
/********************************************/
short get_schalt_jahr(short jahr)   /* jahr 2-Stellig */
{
short i;
i=jahr%4;
if(i == 0) 
    return(1);
else
    return(0);
}
/***************************************/
/*    Funkton liefert monat(short)     */
/***************************************/
short get_monat(char *datum)
{
char  dat[16];
strcpy(dat,datum);
dat[5]='\0';
return((short)(atoi(&dat[3])));
}



/***************************************/
/*    Funkton liefert tag (short)      */
/***************************************/
short get_tag(char *datum)
{
char  dat[16];
strcpy(dat,datum);
dat[2]='\0';
return((short)(atoi(dat)));
}
