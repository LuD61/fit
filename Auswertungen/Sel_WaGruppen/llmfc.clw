; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDialogDatum
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "llmfc.h"
LastPage=0

ClassCount=6
Class1=CLlSampleApp
Class2=CAboutDlg
Class3=CLlSampleDoc
Class4=CLlSampleView
Class5=CMainFrame

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Class6=CDialogDatum
Resource3=IDD_DIALOG1

[CLS:CLlSampleApp]
Type=0
BaseClass=CWinApp
HeaderFile=llmfc.h
ImplementationFile=llmfc.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=llmfc.cpp
ImplementationFile=llmfc.cpp
LastObject=CAboutDlg

[CLS:CLlSampleDoc]
Type=0
BaseClass=CDocument
HeaderFile=llsmpdoc.h
ImplementationFile=llsmpdoc.cpp

[CLS:CLlSampleView]
Type=0
BaseClass=CView
HeaderFile=llsmpvw.h
ImplementationFile=llsmpvw.cpp

[CLS:CMainFrame]
Type=0
BaseClass=CFrameWnd
HeaderFile=mainfrm.h
ImplementationFile=mainfrm.cpp

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=6
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDOK,button,1342373889
Control6=IDC_STATIC,static,1342308352

[DLG:IDD_DIALOG1]
Type=1
Class=CDialogDatum
ControlCount=44
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_GR1BEREICH,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_JAHR,edit,1350631552
Control9=IDC_MANDANT,edit,1350631552
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_GR2BEREICH,edit,1350631552
Control18=IDC_GR3BEREICH,edit,1350631552
Control19=IDC_GR4BEREICH,edit,1350631552
Control20=IDC_GR5BEREICH,edit,1350631552
Control21=IDC_GR6BEREICH,edit,1350631552
Control22=IDC_RADIOHWG,button,1342177289
Control23=IDC_RADIOWG,button,1342177289
Control24=IDC_RADIOSMT,button,1342177289
Control25=IDC_RADIOAG,button,1342177289
Control26=IDC_STATIC,button,1342177287
Control27=IDC_STATIC,button,1342177287
Control28=IDC_STATIC,button,1342177287
Control29=IDC_CHECKGR1N,button,1342177283
Control30=IDC_CHECKGR1S,button,1342177283
Control31=IDC_CHECKGR2N,button,1342177283
Control32=IDC_CHECKGR3N,button,1342177283
Control33=IDC_CHECKGR4N,button,1342177283
Control34=IDC_CHECKGR5N,button,1342177283
Control35=IDC_CHECKGR6N,button,1342177283
Control36=IDC_CHECKGR2S,button,1342177283
Control37=IDC_CHECKGR3S,button,1342177283
Control38=IDC_CHECKGR4S,button,1342177283
Control39=IDC_CHECKGR5S,button,1342177283
Control40=IDC_CHECKGR6S,button,1342177283
Control41=IDC_FILBEREICH,edit,1350631552
Control42=IDC_KUNBEREICH,edit,1350631552
Control43=IDC_STATIC_BEREICH,static,1342312448
Control44=IDC_MOBEREICH,edit,1350631552

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_START_DEBUG
Command2=ID_FILE_STOP_DEBUG
Command3=ID_APP_EXIT
Command4=ID_EDIT_LIST
Command5=ID_EDIT_LABEL
Command6=ID_PRINT_REPORT
Command7=ID_PRINT_LABEL
Command8=ID_APP_ABOUT
CommandCount=8

[ACL:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_NEXT_PANE
Command5=ID_PREV_PANE
CommandCount=5

[CLS:CDialogDatum]
Type=0
HeaderFile=DialogDatum.h
ImplementationFile=DialogDatum.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CDialogDatum

