#include "stdafx.h"
#include "DbClass.h"
#include "DbRange.h"
#include "datum.h"
#include "a_mo_wa.h"
#include "cmbtl9.h"
#include "adr.h"

struct A_MO_WA a_mo_wa, a_mo_wa_null;
struct AUSGABE ausgabe,ausgabe_we, ausgabe_null;
struct A_BAS a_bas, a_bas_null;
struct ADR adr, adr_null;
extern DB_CLASS dbClass;
extern DB_RANGE dbRange;
static long anzfelder = -1;
short mdn = 1;
short filvon = 1 ;
short filbis = 999;
long kunvon = 1 ;
long kunbis = 999999;
char filbereich [256] ;   
char kunbereich [256] ;   
char mobereich [256] ;   
char gr1bereich [256] ;   
char gr2bereich [256] ;   
char gr3bereich [256] ;   
char gr4bereich [256] ;   
char gr5bereich [256] ;   
char gr6bereich [256] ;   
char dfil_kla[3];
extern char datvon[12] ;
extern char datbis[12] ;
short wgvon = 1;
short wgbis = 9999;
double avon = 1;
double abis = 9999999999;
long max_anz = 999999;
short jr = 0;
short kw = 0;
short vjr = 0;
short vvjr = 0;
short movon ;
short mobis ;
short monat ;

short flg_gruppierung;
double gr1von;
double gr1bis;
double gr2von;
double gr2bis;
double gr3von;
double gr3bis;
double gr4von;
double gr4bis;
double gr5von;
double gr5bis;
double gr6von;
double gr6bis;
short gr1von_short;
short gr1bis_short;
short gr2von_short;
short gr2bis_short;
short gr3von_short;
short gr3bis_short;
short gr4von_short;
short gr4bis_short;
short gr5von_short;
short gr5bis_short;
short gr6von_short;
short gr6bis_short;

char checkgr1n[2] ;
char checkgr1s[2] ;
char checkgr2n[2] ;
char checkgr2s[2] ;
char checkgr3n[2] ;
char checkgr3s[2] ;
char checkgr4n[2] ;
char checkgr4s[2] ;
char checkgr5n[2] ;
char checkgr5s[2] ;
char checkgr6n[2] ;
char checkgr6s[2] ;
char R_hwg[2] ;
char R_wg[2] ;
char R_smt[2] ;
char R_ag[2] ;


extern char order_by_zusatz[7] ;
extern char order_by[11] ;
extern char BezRennerPenner [] ;
extern char BezSortierung [] ;

DATUM d_dat, d_datfirst, d_datlast;
DATE_STRUCT end_dat;
double a_gew;
extern char Listenname[128];
char sys_par_wrt [2] = " ";




static char *clipped (char *str)
/**
Blank am Ende eines Strings abschneiden.
**/
{
          int i;

          i = strlen (str);
          if (i == 0) return (str);

          for (i = i - 1; i > 0; i --)
          {
                    if (str[i] > ' ')
                    {
                              str [i + 1] = (char) 0;
                              break;
                    }
          }
          return (str);
}



int A_MO_WA_CLASS::dbcount (void)
/**
Tabelle eti lesen.
**/
{


         if (test_upd_cursor == -1)
         {
			dbClass.sqlout ((char *) sys_par_wrt, SQLCHAR, 2);
			int dsqlstatus = dbClass.sqlcomm ("select sys_par_wrt from sys_par where sys_par_nam = \"abwper_par\""); 
			if (dsqlstatus == 0)
			{
				if (atoi(sys_par_wrt) == 1) 
				{
					prepare_per ();
				}
				else
				{
		             prepare ();
				}
			}
			else
			{
	             prepare ();
			}


         }

	  ausgabe.ges_menge = 0.0;
	  ausgabe.ges_me_gr1 = 0.0;
	  ausgabe.ges_me_gr2 = 0.0;
	  ausgabe.ges_me_gr3 = 0.0;
	  ausgabe.ges_me_gr4 = 0.0;
	  ausgabe.ges_me_gr5 = 0.0;
	  ausgabe.ges_me_gr6 = 0.0;
	  ausgabe.ges_umsatz = 0.0;
	  ausgabe.ges_ums_gr1 = 0.0;
	  ausgabe.ges_ums_gr2 = 0.0;
	  ausgabe.ges_ums_gr3 = 0.0;
	  ausgabe.ges_ums_gr4 = 0.0;
	  ausgabe.ges_ums_gr5 = 0.0;
	  ausgabe.ges_ums_gr6 = 0.0;

     dbClass.sqlopen (readcursor);
     sqlstatus = dbClass.sqlfetch (readcursor);
	 if (sqlstatus != 0) return -1 ;
	 anzfelder = 0;
     while (sqlstatus == 0)
     {
		anzfelder ++;

		ausgabe.ges_menge += ausgabe.menge;
		ausgabe.ges_me_gr1 += ausgabe.me_gr1;
		ausgabe.ges_me_gr2 += ausgabe.me_gr2;
		ausgabe.ges_me_gr3 += ausgabe.me_gr3;
		ausgabe.ges_me_gr4 += ausgabe.me_gr4;
		ausgabe.ges_me_gr5 += ausgabe.me_gr5;
		ausgabe.ges_me_gr6 += ausgabe.me_gr6;
		ausgabe.ges_umsatz += ausgabe.umsatz;
		ausgabe.ges_ums_gr1 += ausgabe.ums_gr1;
		ausgabe.ges_ums_gr2 += ausgabe.ums_gr2;
		ausgabe.ges_ums_gr3 += ausgabe.ums_gr3;
		ausgabe.ges_ums_gr4 += ausgabe.ums_gr4;
		ausgabe.ges_ums_gr5 += ausgabe.ums_gr5;
		ausgabe.ges_ums_gr6 += ausgabe.ums_gr6;

        sqlstatus = dbClass.sqlfetch (readcursor);
     }
     dbClass.sqlopen (readcursor);
     dbClass.sqlopen (readcursor_we);
     return anzfelder; 
}


int A_MO_WA_CLASS::leseDS (int fehlercode)
{
	  if (fehlercode == LL_WRN_REPEAT_DATA)
	  {
         return 0;    // Hier kein fetch , da DS nicht mehr auf Seite passt , und nochmal 
					  // ausgegeben werden mu�
	  }

      initialisiere();

      fehlercode = dbClass.sqlfetch (readcursor);


	  if (fehlercode == 0)
      {
			 ausgabe.kun = a_mo_wa.kun;
			 if (ausgabe.kun_fil == 1)
			 {
				 ausgabe.kun = 0;
				 a_mo_wa.fil = (short) a_mo_wa.kun;
				 a_mo_wa.kun = 0;
				 ausgabe.fil = a_mo_wa.fil;
			      memcpy (&ausgabe_we, &ausgabe_null, sizeof (struct AUSGABE));
				 dbClass.sqlfetch (readcursor_we);    // ziemlich gewagt ...  funktioniert nur, wenn auch �berall Directlieferungen passiert sind 
												      // da es aber bisher l�uft, lass ich es erst mal so ein neues open auf den Cursor , mit eimem sqlin auf nur einer Filiale w�re aus Performancegr�nden sehr schlecht
			 }
			 else
			 {
				 ausgabe.kun = a_mo_wa.kun;
				 sprintf (ausgabe.fil_kla,"%ld",ausgabe.vertr);
				 ausgabe.fil = 0;
			 }


// Wareneingangswerte mit dazunehmen (aus a_per_we) Direktlieferungen
			 if (ausgabe.kun_fil == 1)
			 {

			 ausgabe.menge += ausgabe_we.menge;
			 ausgabe.menge_sa += ausgabe_we.menge_sa;
			 ausgabe.umsatz_sk += ausgabe_we.umsatz_sk;
			 ausgabe.umsatz += ausgabe_we.umsatz;
			 ausgabe.umsatz_sa += ausgabe_we.umsatz_sa;
			 ausgabe.retouren_me += ausgabe_we.retouren_me;
			 ausgabe.retouren += ausgabe_we.retouren;


				 ausgabe.me_gr1 += ausgabe_we.me_gr1;
				 ausgabe.me_gr2 += ausgabe_we.me_gr2;
				ausgabe.me_gr3 += ausgabe_we.me_gr3;
				ausgabe.me_gr4 += ausgabe_we.me_gr4;
				ausgabe.me_gr5 += ausgabe_we.me_gr5;
				ausgabe.me_gr6 += ausgabe_we.me_gr6;
	
				ausgabe.ums_sk_gr1 += ausgabe_we.ums_sk_gr1;
				ausgabe.ums_sk_gr2 += ausgabe_we.ums_sk_gr2;
				ausgabe.ums_sk_gr3 += ausgabe_we.ums_sk_gr3;
				ausgabe.ums_sk_gr4 += ausgabe_we.ums_sk_gr4;
				ausgabe.ums_sk_gr5 += ausgabe_we.ums_sk_gr5;
				ausgabe.ums_sk_gr6 += ausgabe_we.ums_sk_gr6;

				ausgabe.ums_sk_sa_gr1 += ausgabe_we.ums_sk_sa_gr1;
				ausgabe.ums_sk_sa_gr2 += ausgabe_we.ums_sk_sa_gr2;
				ausgabe.ums_sk_sa_gr3 += ausgabe_we.ums_sk_sa_gr3;
				ausgabe.ums_sk_sa_gr4 += ausgabe_we.ums_sk_sa_gr4;
				ausgabe.ums_sk_sa_gr5 += ausgabe_we.ums_sk_sa_gr5;
				ausgabe.ums_sk_sa_gr6 += ausgabe_we.ums_sk_sa_gr6;

				ausgabe.ums_gr1 += ausgabe_we.ums_gr1;
				ausgabe.ums_gr2 += ausgabe_we.ums_gr2;
				ausgabe.ums_gr3 += ausgabe_we.ums_gr3;
				ausgabe.ums_gr4 += ausgabe_we.ums_gr4;
				ausgabe.ums_gr5 += ausgabe_we.ums_gr5;
				ausgabe.ums_gr6 += ausgabe_we.ums_gr6;
	
				ausgabe.ums_sa_gr1 += ausgabe_we.ums_sa_gr1;
				ausgabe.ums_sa_gr2 += ausgabe_we.ums_sa_gr2;
				ausgabe.ums_sa_gr3 += ausgabe_we.ums_sa_gr3;
				ausgabe.ums_sa_gr4 += ausgabe_we.ums_sa_gr4;
				ausgabe.ums_sa_gr5 += ausgabe_we.ums_sa_gr5;
				ausgabe.ums_sa_gr6 += ausgabe_we.ums_sa_gr6;
	
				ausgabe.ret_me_gr1 += ausgabe_we.ret_me_gr1;
				ausgabe.ret_me_gr2 += ausgabe_we.ret_me_gr2;
				ausgabe.ret_me_gr3 += ausgabe_we.ret_me_gr3;
				ausgabe.ret_me_gr4 += ausgabe_we.ret_me_gr4;
				ausgabe.ret_me_gr5 += ausgabe_we.ret_me_gr5;
				ausgabe.ret_me_gr6 += ausgabe_we.ret_me_gr6;
	
				ausgabe.ret_me_sa_gr1 += ausgabe_we.ret_me_sa_gr1;
				ausgabe.ret_me_sa_gr2 += ausgabe_we.ret_me_sa_gr2;
				ausgabe.ret_me_sa_gr3 += ausgabe_we.ret_me_sa_gr3;
				ausgabe.ret_me_sa_gr4 += ausgabe_we.ret_me_sa_gr4;
				ausgabe.ret_me_sa_gr5 += ausgabe_we.ret_me_sa_gr5;
				ausgabe.ret_me_sa_gr6 += ausgabe_we.ret_me_sa_gr6;
	
				ausgabe.ret_gr1 += ausgabe_we.ret_gr1;
				ausgabe.ret_gr2 += ausgabe_we.ret_gr2;
				ausgabe.ret_gr3 += ausgabe_we.ret_gr3;
				ausgabe.ret_gr4 += ausgabe_we.ret_gr4;
				ausgabe.ret_gr5 += ausgabe_we.ret_gr5;
				ausgabe.ret_gr6 += ausgabe_we.ret_gr6;
	
				ausgabe.ret_sa_gr1 += ausgabe_we.ret_sa_gr1;
				ausgabe.ret_sa_gr2 += ausgabe_we.ret_sa_gr2;
				ausgabe.ret_sa_gr3 += ausgabe_we.ret_sa_gr3;
				ausgabe.ret_sa_gr4 += ausgabe_we.ret_sa_gr4;
				ausgabe.ret_sa_gr5 += ausgabe_we.ret_sa_gr5;
				ausgabe.ret_sa_gr6 += ausgabe_we.ret_sa_gr6;
			 }
	


			if (ausgabe.menge != 0.0) ausgabe.pr_sk = ausgabe.umsatz_sk / ausgabe.menge; else ausgabe.pr_sk = 0.0;
			if (ausgabe.me_gr1 != 0.0) ausgabe.pr_sk_gr1 = ausgabe.ums_sk_gr1 / ausgabe.me_gr1; else ausgabe.pr_sk_gr1 = 0.0;
			if (ausgabe.me_gr2 != 0.0) ausgabe.pr_sk_gr2 = ausgabe.ums_sk_gr2 / ausgabe.me_gr2; else ausgabe.pr_sk_gr2 = 0.0;
			if (ausgabe.me_gr3 != 0.0) ausgabe.pr_sk_gr3 = ausgabe.ums_sk_gr3 / ausgabe.me_gr3; else ausgabe.pr_sk_gr3 = 0.0;
			if (ausgabe.me_gr4 != 0.0) ausgabe.pr_sk_gr4 = ausgabe.ums_sk_gr4 / ausgabe.me_gr4; else ausgabe.pr_sk_gr4 = 0.0;
			if (ausgabe.me_gr5 != 0.0) ausgabe.pr_sk_gr5 = ausgabe.ums_sk_gr5 / ausgabe.me_gr5; else ausgabe.pr_sk_gr5 = 0.0;
			if (ausgabe.me_gr6 != 0.0) ausgabe.pr_sk_gr6 = ausgabe.ums_sk_gr6 / ausgabe.me_gr6; else ausgabe.pr_sk_gr6 = 0.0;
			 
 //=== Gruppe 1=========
			if (strcmp(checkgr1n,"J") == 0 && strcmp(checkgr1s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
				ausgabe.ertrag_gr1 = ausgabe.ums_gr1 - ausgabe.ums_sk_gr1;
			}
			else if (strcmp(checkgr1n,"J") == 0 && strcmp(checkgr1s,"N") == 0) 
			{
				ausgabe.me_gr1 -= ausgabe.me_sa_gr1;
				ausgabe.ums_gr1 -= ausgabe.ums_sa_gr1;
				ausgabe.ret_me_gr1 -= ausgabe.ret_me_sa_gr1;
				ausgabe.ret_gr1 -= ausgabe.ret_sa_gr1;
				ausgabe.ums_sk_gr1 -= ausgabe.ums_sk_sa_gr1;
				if (ausgabe.fil > 0)
				{
					ausgabe.ertrag_gr1 = ausgabe.ums_gr1 - ausgabe.ums_sk_gr1;
				}
				else  // bei Kunden hier der volle ERtrag rein , mit SA , da SA Einzeln nicht ausgewiesen werden kann, weil
					  // es kein ums_sk_sa_vollk in a_per_wa gibt !!!
				{
					ausgabe.ertrag_gr1 = ausgabe.ums_gr1  + ausgabe.ums_sa_gr1 - ausgabe.ums_sk_gr1;
				}

			}
			else if (strcmp(checkgr1n,"N") == 0 && strcmp(checkgr1s,"J") == 0) 
			{
				ausgabe.me_gr1 = ausgabe.me_sa_gr1;
				ausgabe.ums_gr1 = ausgabe.ums_sa_gr1;
				ausgabe.ret_me_gr1 = ausgabe.ret_me_sa_gr1;
				ausgabe.ret_gr1 = ausgabe.ret_sa_gr1;
				ausgabe.ums_sk_gr1 = ausgabe.ums_sk_sa_gr1;
				if (ausgabe.fil > 0) ausgabe.ertrag_gr1 = ausgabe.ums_gr1 - ausgabe.ums_sk_gr1;

			}

//=== Gruppe 2=========
			if (strcmp(checkgr2n,"J") == 0 && strcmp(checkgr2s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
				ausgabe.ertrag_gr2 = ausgabe.ums_gr2 - ausgabe.ums_sk_gr2;
			}
			else if (strcmp(checkgr2n,"J") == 0 && strcmp(checkgr2s,"N") == 0) 
			{
				ausgabe.me_gr2 -= ausgabe.me_sa_gr2;
				ausgabe.ums_gr2 -= ausgabe.ums_sa_gr2;
				ausgabe.ret_me_gr2 -= ausgabe.ret_me_sa_gr2;
				ausgabe.ret_gr2 -= ausgabe.ret_sa_gr2;
				ausgabe.ums_sk_gr2 -= ausgabe.ums_sk_sa_gr2;
				if (ausgabe.fil > 0)
				{
					ausgabe.ertrag_gr2 = ausgabe.ums_gr2 - ausgabe.ums_sk_gr2;
				}
				else  // bei Kunden hier der volle ERtrag rein , mit SA , da SA Einzeln nicht ausgewiesen werden kann, weil
					  // es kein ums_sk_sa_vollk in a_per_wa gibt !!!
				{
					ausgabe.ertrag_gr2 = ausgabe.ums_gr2  + ausgabe.ums_sa_gr2 - ausgabe.ums_sk_gr2;
				}

			}
			else if (strcmp(checkgr2n,"N") == 0 && strcmp(checkgr2s,"J") == 0) 
			{
				ausgabe.me_gr2 = ausgabe.me_sa_gr2;
				ausgabe.ums_gr2 = ausgabe.ums_sa_gr2;
				ausgabe.ret_me_gr2 = ausgabe.ret_me_sa_gr2;
				ausgabe.ret_gr2 = ausgabe.ret_sa_gr2;
				ausgabe.ums_sk_gr2 = ausgabe.ums_sk_sa_gr2;
				if (ausgabe.fil > 0) ausgabe.ertrag_gr2 = ausgabe.ums_gr2 - ausgabe.ums_sk_gr2;

			}
//=== Gruppe 3=========
			if (strcmp(checkgr3n,"J") == 0 && strcmp(checkgr3s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
				ausgabe.ertrag_gr3 = ausgabe.ums_gr3 - ausgabe.ums_sk_gr3;
			}
			else if (strcmp(checkgr3n,"J") == 0 && strcmp(checkgr3s,"N") == 0) 
			{
				ausgabe.me_gr3 -= ausgabe.me_sa_gr3;
				ausgabe.ums_gr3 -= ausgabe.ums_sa_gr3;
				ausgabe.ret_me_gr3 -= ausgabe.ret_me_sa_gr3;
				ausgabe.ret_gr3 -= ausgabe.ret_sa_gr3;
				ausgabe.ums_sk_gr3 -= ausgabe.ums_sk_sa_gr3;
				if (ausgabe.fil > 0)
				{
					ausgabe.ertrag_gr3 = ausgabe.ums_gr3 - ausgabe.ums_sk_gr3;
				}
				else  // bei Kunden hier der volle ERtrag rein , mit SA , da SA Einzeln nicht ausgewiesen werden kann, weil
					  // es kein ums_sk_sa_vollk in a_per_wa gibt !!!
				{
					ausgabe.ertrag_gr3 = ausgabe.ums_gr3  + ausgabe.ums_sa_gr3 - ausgabe.ums_sk_gr3;
				}

			}
			else if (strcmp(checkgr3n,"N") == 0 && strcmp(checkgr3s,"J") == 0) 
			{
				ausgabe.me_gr3 = ausgabe.me_sa_gr3;
				ausgabe.ums_gr3 = ausgabe.ums_sa_gr3;
				ausgabe.ret_me_gr3 = ausgabe.ret_me_sa_gr3;
				ausgabe.ret_gr3 = ausgabe.ret_sa_gr3;
				ausgabe.ums_sk_gr3 = ausgabe.ums_sk_sa_gr3;
				if (ausgabe.fil > 0) ausgabe.ertrag_gr3 = ausgabe.ums_gr3 - ausgabe.ums_sk_gr3;

			}
//=== Gruppe 4=========
			if (strcmp(checkgr4n,"J") == 0 && strcmp(checkgr4s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
				ausgabe.ertrag_gr4 = ausgabe.ums_gr4 - ausgabe.ums_sk_gr4;
			}
			else if (strcmp(checkgr4n,"J") == 0 && strcmp(checkgr4s,"N") == 0) 
			{
				ausgabe.me_gr4 -= ausgabe.me_sa_gr4;
				ausgabe.ums_gr4 -= ausgabe.ums_sa_gr4;
				ausgabe.ret_me_gr4 -= ausgabe.ret_me_sa_gr4;
				ausgabe.ret_gr4 -= ausgabe.ret_sa_gr4;
				ausgabe.ums_sk_gr4 -= ausgabe.ums_sk_sa_gr4;
				if (ausgabe.fil > 0)
				{
					ausgabe.ertrag_gr4 = ausgabe.ums_gr4 - ausgabe.ums_sk_gr4;
				}
				else  // bei Kunden hier der volle ERtrag rein , mit SA , da SA Einzeln nicht ausgewiesen werden kann, weil
					  // es kein ums_sk_sa_vollk in a_per_wa gibt !!!
				{
					ausgabe.ertrag_gr4 = ausgabe.ums_gr4  + ausgabe.ums_sa_gr4 - ausgabe.ums_sk_gr4;
				}

			}
			else if (strcmp(checkgr4n,"N") == 0 && strcmp(checkgr4s,"J") == 0) 
			{
				ausgabe.me_gr4 = ausgabe.me_sa_gr4;
				ausgabe.ums_gr4 = ausgabe.ums_sa_gr4;
				ausgabe.ret_me_gr4 = ausgabe.ret_me_sa_gr4;
				ausgabe.ret_gr4 = ausgabe.ret_sa_gr4;
				ausgabe.ums_sk_gr4 = ausgabe.ums_sk_sa_gr4;
				if (ausgabe.fil > 0) ausgabe.ertrag_gr4 = ausgabe.ums_gr4 - ausgabe.ums_sk_gr4;

			}
//=== Gruppe 5=========
			if (strcmp(checkgr5n,"J") == 0 && strcmp(checkgr5s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
				ausgabe.ertrag_gr5 = ausgabe.ums_gr5 - ausgabe.ums_sk_gr5;
			}
			else if (strcmp(checkgr5n,"J") == 0 && strcmp(checkgr5s,"N") == 0) 
			{
				ausgabe.me_gr5 -= ausgabe.me_sa_gr5;
				ausgabe.ums_gr5 -= ausgabe.ums_sa_gr5;
				ausgabe.ret_me_gr5 -= ausgabe.ret_me_sa_gr5;
				ausgabe.ret_gr5 -= ausgabe.ret_sa_gr5;
				ausgabe.ums_sk_gr5 -= ausgabe.ums_sk_sa_gr5;
				if (ausgabe.fil > 0)
				{
					ausgabe.ertrag_gr5 = ausgabe.ums_gr5 - ausgabe.ums_sk_gr5;
				}
				else  // bei Kunden hier der volle ERtrag rein , mit SA , da SA Einzeln nicht ausgewiesen werden kann, weil
					  // es kein ums_sk_sa_vollk in a_per_wa gibt !!!
				{
					ausgabe.ertrag_gr5 = ausgabe.ums_gr5  + ausgabe.ums_sa_gr5 - ausgabe.ums_sk_gr5;
				}

			}
			else if (strcmp(checkgr5n,"N") == 0 && strcmp(checkgr5s,"J") == 0) 
			{
				ausgabe.me_gr5 = ausgabe.me_sa_gr5;
				ausgabe.ums_gr5 = ausgabe.ums_sa_gr5;
				ausgabe.ret_me_gr5 = ausgabe.ret_me_sa_gr5;
				ausgabe.ret_gr5 = ausgabe.ret_sa_gr5;
				ausgabe.ums_sk_gr5 = ausgabe.ums_sk_sa_gr5;
				if (ausgabe.fil > 0) ausgabe.ertrag_gr5 = ausgabe.ums_gr5 - ausgabe.ums_sk_gr5;

			}
//=== Gruppe 6=========
			if (strcmp(checkgr6n,"J") == 0 && strcmp(checkgr6s,"J") == 0) 
			{
				// hier ist nix zu tun, da in den Feldern me_vk_per .... Normal und SA-Umsatz zusammen laufen (im Gegensatz zu a_tag_wa!) 
				ausgabe.ertrag_gr6 = ausgabe.ums_gr6 - ausgabe.ums_sk_gr6;
			}
			else if (strcmp(checkgr6n,"J") == 0 && strcmp(checkgr6s,"N") == 0) 
			{
				ausgabe.me_gr6 -= ausgabe.me_sa_gr6;
				ausgabe.ums_gr6 -= ausgabe.ums_sa_gr6;
				ausgabe.ret_me_gr6 -= ausgabe.ret_me_sa_gr6;
				ausgabe.ret_gr6 -= ausgabe.ret_sa_gr6;
				ausgabe.ums_sk_gr6 -= ausgabe.ums_sk_sa_gr6;
				if (ausgabe.fil > 0)
				{
					ausgabe.ertrag_gr6 = ausgabe.ums_gr6 - ausgabe.ums_sk_gr6;
				}
				else  // bei Kunden hier der volle ERtrag rein , mit SA , da SA Einzeln nicht ausgewiesen werden kann, weil
					  // es kein ums_sk_sa_vollk in a_per_wa gibt !!!
				{
					ausgabe.ertrag_gr6 = ausgabe.ums_gr6  + ausgabe.ums_sa_gr6 - ausgabe.ums_sk_gr6;
				}

			}
			else if (strcmp(checkgr6n,"N") == 0 && strcmp(checkgr6s,"J") == 0) 
			{
				ausgabe.me_gr6 = ausgabe.me_sa_gr6;
				ausgabe.ums_gr6 = ausgabe.ums_sa_gr6;
				ausgabe.ret_me_gr6 = ausgabe.ret_me_sa_gr6;
				ausgabe.ret_gr6 = ausgabe.ret_sa_gr6;
				ausgabe.ums_sk_gr6 = ausgabe.ums_sk_sa_gr6;
				if (ausgabe.fil > 0) ausgabe.ertrag_gr6 = ausgabe.ums_gr6 - ausgabe.ums_sk_gr6;

			}
		
			ausgabe.ertrag = ausgabe.umsatz - ausgabe.umsatz_sk;


            if (ausgabe.kun_fil == 0)
			{
				dbClass.sqlin ((long *) &ausgabe.kun, SQLLONG, 0);
				dbClass.sqlout ((char *) adr.adr_krz, SQLCHAR, 17);
				dbClass.sqlout ((char *) adr.adr_nam1, SQLCHAR, 37);
				dbClass.sqlout ((char *) adr.plz, SQLCHAR, 9);
				dbClass.sqlout ((char *) adr.ort1, SQLCHAR, 37);
				dbClass.sqlcomm ("select adr.adr_krz,adr.adr_nam1,adr.plz,adr.ort1 from kun,adr where kun = ?  and kun.adr1 = adr.adr");
			}
			else
			{
				dbClass.sqlin ((short *) &ausgabe.fil, SQLSHORT, 0);
				dbClass.sqlout ((char *) adr.adr_krz, SQLCHAR, 17);
				dbClass.sqlout ((char *) adr.adr_nam1, SQLCHAR, 37);
				dbClass.sqlout ((char *) adr.plz, SQLCHAR, 9);
				dbClass.sqlout ((char *) adr.ort1, SQLCHAR, 37);
				dbClass.sqlcomm ("select adr.adr_krz,adr.adr_nam1,adr.plz,adr.ort1 from fil,adr where fil.fil = ? and fil.adr = adr.adr ");
			}




	
	}
      return fehlercode;
}

void A_MO_WA_CLASS::initialisiere (void)
{
      memcpy (&ausgabe, &ausgabe_null, sizeof (struct AUSGABE));
	  /*
	  ausgabe.menge = 0.0;
	  ausgabe.me_gr1 = 0.0;
	  ausgabe.me_gr2 = 0.0;
	  ausgabe.me_gr3 = 0.0;
	  ausgabe.me_gr4 = 0.0;
	  ausgabe.me_gr5 = 0.0;
	  ausgabe.me_gr6 = 0.0;
	  ausgabe.umsatz = 0.0;
	  ausgabe.ums_gr1 = 0.0;
	  ausgabe.ums_gr2 = 0.0;
	  ausgabe.ums_gr3 = 0.0;
	  ausgabe.ums_gr4 = 0.0;
	  ausgabe.ums_gr5 = 0.0;
	  ausgabe.ums_gr6 = 0.0;
	  ausgabe.umsatz_sk = 0.0;
	  ausgabe.ums_sk_gr1 = 0.0;
	  ausgabe.ums_sk_gr2 = 0.0;
	  ausgabe.ums_sk_gr3 = 0.0;
	  ausgabe.ums_sk_gr4 = 0.0;
	  ausgabe.ums_sk_gr5 = 0.0;
	  ausgabe.ums_sk_gr6 = 0.0;
	  */

}


int A_MO_WA_CLASS::openDS (void)
{
         return dbClass.sqlopen (readcursor);
}

void A_MO_WA_CLASS::prepare (void)
{
     char *sqltext1;
     char *sqltext2;
    sqltext1 = "select  ";
    sqltext2 = "select  ";

	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filvon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &filbis, SQLSHORT, 0);
	dbClass.sqlin ((short *) &kunvon, SQLLONG, 0);
	dbClass.sqlin ((short *) &kunbis, SQLLONG, 0);
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
	dbClass.sqlin ((short *) &movon, SQLSHORT, 0);
	dbClass.sqlin ((short *) &mobis, SQLSHORT, 0);

	dbClass.sqlout ((char *) ausgabe.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((long *) &ausgabe.vertr, SQLLONG, 2);
	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun

           readcursor = dbClass.sqlcursor ("select "
							"case when a_mo_wa.fil > 0 then fil.fil_kla "
							"	when a_mo_wa.fil = 0 then \"\" end klasse , "
							"case when a_mo_wa.kun > 0 then kun.vertr1 "
							"	when a_mo_wa.kun = 0 then 0 end vertr , "

							"case when a_mo_wa.fil = 0 then a_mo_wa.kun "
							"     when a_mo_wa.fil > 0 then a_mo_wa.fil end kunde, "
							"case when a_mo_wa.fil = 0 then 0 "
							"     when a_mo_wa.fil > 0 then 1 end kun_fil "
							"from a_mo_wa,a_bas,wg, outer fil ,outer kun where "
							"    a_mo_wa.mdn = fil.mdn "
							"and a_mo_wa.fil = fil.fil "
							"and a_mo_wa.kun = kun.kun "
							"and a_mo_wa.a = a_bas.a "
							"and a_bas.wg = wg.wg "
							"and a_mo_wa.mdn = ? "
							"and a_mo_wa.fil between ? and ? "
							"and a_mo_wa.kun between ? and ? "
							"and a_mo_wa.jr = ? "
							"and a_mo_wa.mo between ? and ? "
							"group by 1,2,3,4 order by klasse,vertr,kun_fil,kunde "
							);


	test_upd_cursor = 1;


}

void A_MO_WA_CLASS::prepare_per (void)
{
     char *sqltext1;
     char *sqltext2;
    sqltext1 = "select  ";
    sqltext2 = "select  ";


//=============================================
//========= Menge =============================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Menge SA ==========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 


//=============================================
//========= Umsatz zm SK ======================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Umsatz zm SK SA====================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 


//=============================================
//========= Umsatz  ===========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Umsatz SA  ========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 



//=============================================
//========= Retouren Menge ===================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Retouren Umsatz ===================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 
	


	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
    dbRange.RangeIn ("a_per_wa.fil",filbereich, SQLSHORT, 0); 
    dbRange.RangeIn ("a_per_wa.kun",kunbereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
    dbRange.RangeIn ("a_per_wa.period",mobereich, SQLSHORT, 0); 

	dbClass.sqlout ((char *) ausgabe.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((long *) &ausgabe.vertr, SQLLONG, 2);
	dbClass.sqlout ((short *) &a_mo_wa.kun, SQLLONG, 0);
	dbClass.sqlout ((short *) &ausgabe.kun_fil, SQLSHORT, 0);   // hier nur kennz. ob fil oder kun

//========= Menge =============================
	dbClass.sqlout ((double *) &ausgabe.menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_gr6, SQLDOUBLE, 0);
//========= Menge SA ===========================
	dbClass.sqlout ((double *) &ausgabe.menge_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.me_sa_gr6, SQLDOUBLE, 0);
//========= Umsatz zum SK =====================
	dbClass.sqlout ((double *) &ausgabe.umsatz_sk, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_gr6, SQLDOUBLE, 0);
//========= Umsatz zum SK SA=====================
	dbClass.sqlout ((double *) &ausgabe.umsatz_sk_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sk_sa_gr6, SQLDOUBLE, 0);
//========= Umsatz ============================
	dbClass.sqlout ((double *) &ausgabe.umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_gr6, SQLDOUBLE, 0);
//========= Umsatz SA ============================
	dbClass.sqlout ((double *) &ausgabe.umsatz_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ums_sa_gr6, SQLDOUBLE, 0);
//========= Retouren  Menge =============================
	dbClass.sqlout ((double *) &ausgabe.retouren_me, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_me_gr6, SQLDOUBLE, 0);
//========= Retouren  Umsatz =============================
	dbClass.sqlout ((double *) &ausgabe.retouren, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe.ret_gr6, SQLDOUBLE, 0);


           readcursor = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_per_wa.fil > 0 then fil.fil_kla "
							"	when a_per_wa.fil = 0 then \"\" end klasse , "
							"case when a_per_wa.kun > 0 then kun.vertr1 "
							"	when a_per_wa.kun = 0 then 0 end vertr , "


							"case when a_per_wa.fil = 0 then a_per_wa.kun "
							"     when a_per_wa.fil > 0 then a_per_wa.fil end kunde, "
							"case when a_per_wa.fil = 0 then 0 "
							"     when a_per_wa.fil > 0 then 1 end kun_fil, "

//=============================================
//========= Menge =============================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_per  end "
								"end ), "

//=============================================
//========= Menge SA ==========================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_vk_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_vk_sa_per  end "
								"end ), "
//=============================================
//========= Umsatz zum SK =====================
//=============================================
/***
							"sum (a_per_wa.ums_sk_vkost_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_per end ), "
// bei kun_fil = 1 : wie vorher �ber fil_ek, wenn kun_fil = 0 , dann �ber ums_sk
***/
							"sum (case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 1
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 2
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 3
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
								"end ), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 4
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
								"end ), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 5
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 6
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_per_wa.fil > 0 then a_per_wa.ums_fil_ek_per  "
													"when a_per_wa.fil = 0  then a_per_wa.ums_sk_vkost_per  end "
								"end ), "

//=============================================
//========= Umsatz zum SK SA====================
//=============================================
							"sum (a_per_wa.ums_sk_vkost_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_fil_ek_sa_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_fil_ek_sa_per end ), "
//=============================================
//========= Umsatz  ===========================
//=============================================
							"sum (a_per_wa.ums_vk_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_per end ), "
//=============================================
//========= Umsatz SA =========================
//=============================================
							"sum (a_per_wa.ums_vk_sa_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_vk_sa_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_vk_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_vk_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_vk_sa_per end ), "
//=============================================
//========= Retouren Menge ====================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_wa.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_wa.me_ret_per  end "
								"end ), "
//=============================================
//========= Retouren Umsatz  ===========================
//=============================================
							"sum (a_per_wa.ums_ret_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_wa.ums_ret_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_wa.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_wa.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_wa.ums_ret_per end ) "

							"from a_per_wa,a_bas, outer fil, outer kun where "
							"    a_per_wa.mdn = fil.mdn "
							"and a_per_wa.fil = fil.fil "
							"and a_per_wa.kun = kun.kun "
							"and a_per_wa.a = a_bas.a "
							"and a_per_wa.mdn = ? "
							"and <a_per_wa.fil> "
							"and <a_per_wa.kun> "
							"and a_per_wa.jr = ? "
							"and <a_per_wa.period> "
							"and (a_per_wa.me_vk_per <> 0 or a_per_wa.me_ret_per <> 0) "
							"group by 1,2,3,4 order by klasse,vertr,kun_fil,kunde " 
							));


//===========================================================================================
//===========================================================================================
//===========================================================================================
//===============     readcursor_we       ====================================================
//===========================================================================================
//===========================================================================================
//===========================================================================================




//=============================================
//========= Menge =============================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Menge SA ==========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 


//=============================================
//========= Umsatz zm SK ======================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Umsatz zm SK SA====================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Umsatz  ===========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Umsatz SA  ========================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 



//=============================================
//========= Retouren Menge ===================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 

//=============================================
//========= Retouren Umsatz ===================
//=============================================
	// Gruppe 1
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr1bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr1bereich, SQLSHORT, 0); 

	// Gruppe 2
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr2bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr2bereich, SQLSHORT, 0); 

	// Gruppe 3
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr3bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr3bereich, SQLSHORT, 0); 

	// Gruppe 4
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr4bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr4bereich, SQLSHORT, 0); 

	// Gruppe 5
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr5bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr5bereich, SQLSHORT, 0); 

	// Gruppe 6
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.hwg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.wg",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.ag",gr6bereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &flg_gruppierung, SQLSHORT, 0);
    dbRange.RangeIn ("a_bas.teil_smt",gr6bereich, SQLSHORT, 0); 
	



	dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
    dbRange.RangeIn ("a_per_we.fil",filbereich, SQLSHORT, 0); 
	dbClass.sqlin ((short *) &jr, SQLSHORT, 0);
    dbRange.RangeIn ("a_per_we.period",mobereich, SQLSHORT, 0); 

//	dbClass.sqlout ((char *) ausgabe.fil_kla, SQLCHAR, 2);
	dbClass.sqlout ((char *)  dfil_kla, SQLCHAR, 2);
	dbClass.sqlout ((short *) &a_mo_wa.fil, SQLLONG, 0);
	dbClass.sqlout ((short *) &a_mo_wa.jr, SQLSHORT, 0);

//========= Menge =============================
	dbClass.sqlout ((double *) &ausgabe_we.menge, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_gr6, SQLDOUBLE, 0);
//========= Menge SA ===========================
	dbClass.sqlout ((double *) &ausgabe_we.menge_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.me_sa_gr6, SQLDOUBLE, 0);
//========= Umsatz zum SK =====================
	dbClass.sqlout ((double *) &ausgabe_we.umsatz_sk, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_gr6, SQLDOUBLE, 0);
//========= Umsatz zum SK =====================
	dbClass.sqlout ((double *) &ausgabe_we.umsatz_sk_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sk_sa_gr6, SQLDOUBLE, 0);
//========= Umsatz ============================
	dbClass.sqlout ((double *) &ausgabe_we.umsatz, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_gr6, SQLDOUBLE, 0);
//========= Umsatz SA ============================
	dbClass.sqlout ((double *) &ausgabe_we.umsatz_sa, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ums_sa_gr6, SQLDOUBLE, 0);
//========= Retouren  Menge =============================
	dbClass.sqlout ((double *) &ausgabe_we.retouren_me, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_me_gr6, SQLDOUBLE, 0);
//========= Retouren  Umsatz =============================
	dbClass.sqlout ((double *) &ausgabe_we.retouren, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr1, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr2, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr3, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr4, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr5, SQLDOUBLE, 0);
	dbClass.sqlout ((double *) &ausgabe_we.ret_gr6, SQLDOUBLE, 0);



           readcursor_we = dbClass.sqlcursor (dbRange.SetRange ("select "
							"case when a_per_we.fil > 0 then fil.fil_kla "
							"	when a_per_we.fil = 0 then \"\" end klasse , "


							"a_per_we.fil, a_per_we.jr, "

//=============================================
//========= Menge =============================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_per  end "
								"end ), "

//=============================================
//========= Menge SA ==========================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Menge SA Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 2 = ? and <a_bas.wg>  then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ek_sa_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ek_sa_per  end "
								"end ), "
//=============================================
//========= Umsatz zum SK =====================
//=============================================
							"sum (a_per_we.ums_fil_ek_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_per end ), "

//=============================================
//========= Umsatz zum SK SA===================
//=============================================
							"sum (a_per_we.ums_fil_ek_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_ek_sa_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_ek_sa_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_ek_sa_per end ), "
//=============================================
//========= Umsatz  ===========================
//=============================================
							"sum (a_per_we.ums_fil_vk_per / ptabn.ptwer2 ), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2  " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per   / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per   / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per   / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_per   / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_per  / ptabn.ptwer2 end ), "
//=============================================
//========= Umsatz SA =========================
//=============================================
							"sum (a_per_we.ums_fil_vk_sa_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2 " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2 " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2 end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2 " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per   / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per   / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2 " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per   / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per   / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per  / ptabn.ptwer2 end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_fil_vk_sa_per   / ptabn.ptwer2 "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_fil_vk_sa_per   / ptabn.ptwer2 "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_fil_vk_sa_per / ptabn.ptwer2  end ), "
//=============================================
//========= Retouren Menge ====================
//=============================================
							"sum (case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"), "
							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 1
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 2
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 3
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 4
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 5
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  " // Retouren Menge Gruppe 6
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 2 = ? and <a_bas.wg> then " 
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 3 = ? and <a_bas.ag> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
							"          when 4 = ? and <a_bas.teil_smt> then  "
													"case when a_bas.me_einh <> 2 then a_per_we.me_ret_per * a_bas.a_gew "
													"when a_bas.me_einh = 2  then a_per_we.me_ret_per  end "
								"end ), "
//=============================================
//========= Retouren Umsatz  ===========================
//=============================================
							"sum (a_per_we.ums_ret_per), "
							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 1
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 2
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 3
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 4
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 5
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ), "

							"sum (case when 1 = ? and <a_bas.hwg> then  a_per_we.ums_ret_per " // Gruppe 6
							"          when 2 = ? and <a_bas.wg> then  a_per_we.ums_ret_per  "
							"          when 3 = ? and <a_bas.ag> then  a_per_we.ums_ret_per  "
							"          when 4 = ? and <a_bas.teil_smt> then  a_per_we.ums_ret_per end ) "

							"from a_per_we,a_bas,ptabn, outer fil where "
							"    a_per_we.mdn = fil.mdn "
							"and a_per_we.fil = fil.fil "
							"and a_per_we.a = a_bas.a "
							"and a_per_we.mdn = ? "
							"and <a_per_we.fil> "
							"and a_per_we.jr = ? "
							"and <a_per_we.period> "
							"and ptabn.ptitem = \"mwst\" and ptabn.ptlfnr = a_bas.mwst "
							"and (a_per_we.me_ek_per <> 0 or a_per_we.me_ret_per <> 0) "
							"group by 1,2,3 order by klasse,a_per_we.fil " 


							));


	test_upd_cursor = 1;


}
