//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
// Diese Applikation z.Z. nur f�r Warenausgang Gruppenstatistik  SE.TEC   03.2009
// ( f. Fa. Hausner   ausgelesen wird a_per_wa 
// Erstellung : 13.12.2008  LuD

//=============================================================================

// mainfrm.cpp : implementation of the CMainFrame class

#include "stdafx.h"
#include "llmfc.h"
#include "mainfrm.h"
#include "wmask.h"
#include "DbClass.h"
#include "DbRange.h"
#include "a_mo_wa.h"
#include "form_feld.h"
#include "systables.h"
#include "syscolumns.h"
#include "Token.h"
#include "strfkt.h"
#include "datum.h"
#include "DialogDatum.h"
#include "debug.h"
#include "adr.h"

#include "cmbtl9.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

extern CDebug Debug;
/////////////////////////////////////////////////////////////////////////////
// GR: Waehlen Sie hier die List & Label Sprache f�r den Designer und die
//     Dialoge aus :
//    (Die entsprechenden Sprachkonstanten entnehmen Sie der Datei cmbtl9.h)

// US: choose your LuL-Language for all designer, dialogs, etc... here:
//    (see cmbtl9.h for other language-constants)

const int LUL_LANGUAGE = CMBTLANG_GERMAN;

//LuD...
//const char *FILENAME_DEFAULT = "c:\\temp\\rezeptur.lst";  // Zeiger auf eine Konstante
const char *FILENAME_DEFAULT = "c:\\user\\fit\\format\\LL\\Produktion\\rezaufl.lst                               ";  // Zeiger auf eine Konstante
bool cfgOK;
char Listenname[128];
int Listenauswahl = 0;   // wenn 1 : Auswahl mehrerer Reports
int Hauptmenue = 0;   // wenn 1 : Auswahl edit oder print �ber Menue
int Divisor = 1;
int MitVorjahr = 0;
char FormatMe [30];
char FormatUms [30];
extern char erster_tag_wo1[12];
extern char letzter_tag_wo1[12];
extern char erster_tag_wo2[12];
extern char letzter_tag_wo2[12];
extern char erster_tag_wo3[12];
extern char letzter_tag_wo3[12];
extern char erster_tag_wo4[12];
extern char letzter_tag_wo4[12];
extern char erster_tag_wo5[12];
extern char letzter_tag_wo5[12];

extern short flg_gruppierung;
extern double gr1von;
extern double gr1bis;
extern double gr2von;
extern double gr2bis;
extern double gr3von;
extern double gr3bis;
extern double gr4von;
extern double gr4bis;
extern double gr5von;
extern double gr5bis;
extern double gr6von;
extern double gr6bis;
extern short gr1von_short;
extern short gr1bis_short;
extern short gr2von_short;
extern short gr2bis_short;
extern short gr3von_short;
extern short gr3bis_short;
extern short gr4von_short;
extern short gr4bis_short;
extern short gr5von_short;
extern short gr5bis_short;
extern short gr6von_short;
extern short gr6bis_short;

extern char checkgr1n[2] ;
extern char checkgr1s[2] ;
extern char checkgr2n[2] ;
extern char checkgr2s[2] ;
extern char checkgr3n[2] ;
extern char checkgr3s[2] ;
extern char checkgr4n[2] ;
extern char checkgr4s[2] ;
extern char checkgr5n[2] ;
extern char checkgr5s[2] ;
extern char checkgr6n[2] ;
extern char checkgr6s[2] ;

extern char R_hwg[2] ;
extern char R_wg[2] ;
extern char R_smt[2] ;
extern char R_ag[2] ;



extern short wo1;
extern short wo2;
extern short wo3;
extern short wo4;
extern short wo5;
extern short wo1_vj;
extern short wo2_vj;
extern short wo3_vj;
extern short wo4_vj;
extern short wo5_vj;
extern short mdn ;
extern short filvon ;
extern short filbis ;
extern long kunvon ;
extern long kunbis ;
extern short jr ;
extern short vjr ;
extern short movon ;
extern short mobis ;
extern short erste_wo ;
extern short letzte_wo ;
extern double menge[];
extern double menge_vj[];
extern double menge_woche[];
extern double menge_woche_vj[];
extern double umsatz[];
extern double umsatz_vj[];
extern short a_bas_me_einh;
extern short a_bas_lief_einh;
extern double a_bas_a_gew;
extern char artikelbezeichnung[26];
extern char rez_preis_bz[12];
extern double rez_preis;
extern char a_bz1[25];
extern char einh_bz[9];
char cdatumvon[12];
char cdatumbis[12];
long idat = 0;

extern char filbereich[256]; 
extern char kunbereich[256]; 
extern char mobereich[256]; 
extern char gr1bereich[256]; 
extern char gr2bereich[256]; 
extern char gr3bereich[256]; 
extern char gr4bereich[256]; 
extern char gr5bereich[256]; 
extern char gr6bereich[256]; 


        static char *Monate [] = {"",
                                  "Januar",
                                  "Februar",
                                  "M�rz",
                                  "April",
                                  "Mai",
                                  "Juni",
                                  "Juli",
                                  "August",
                                  "September",
                                  "Oktober",
                                  "November",
                                  "Dezember",
                                   NULL,
                                   NULL,
        };


  DB_CLASS dbClass;
  DB_RANGE dbRange;
  A_MO_WA_CLASS AMoWa;
  FORM_FELD form_feld;
  SYSCOLUMNS syscolumns;
  SYSTABLES systables;

HWND hMainWindow;

int form_feld_curs, tabid_curs, coltyp_curs,  dynacurs ; 
/////////////////////////////////////////////////////////////////////////////
// CMainFrame

// GR: Registrieren eines Nachrichtenkanals f�r List & Label Callbacks
// US: register LuL MessageBase:
UINT CMainFrame::m_uLuLMessageBase = RegisterWindowMessage("cmbtLLMessage");

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_START_DEBUG, OnStartDebug)
	ON_COMMAND(ID_FILE_STOP_DEBUG, OnStopDebug)
	ON_COMMAND(ID_EDIT_LABEL, OnEditLabel)
	ON_COMMAND(ID_EDIT_LIST, OnEditList)
	ON_UPDATE_COMMAND_UI(ID_FILE_START_DEBUG, OnUpdateStartDebug)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP_DEBUG, OnUpdateStopDebug)
	ON_COMMAND(ID_PRINT_LABEL, OnPrintLabel)
	ON_COMMAND(ID_PRINT_REPORT, OnPrintReport)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(m_uLuLMessageBase,OnLulMessage)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_bDebug = FALSE;
    ProgCfg = new PROG_CFG ("sel_WaGruppen");

}

CMainFrame::~CMainFrame()
{
    if (ProgCfg != NULL)
    {
        ProgCfg->CloseCfg ();
        delete ProgCfg;
        ProgCfg = NULL;
    }

}


/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	
	LlAddCtlSupport(m_hWnd,
					LL_CTL_ADDTOSYSMENU | LL_CTL_CONVERTCONTROLS | LL_CTL_ALSOCHILDREN,
					"combit.ini");
	sprintf(FormatMe,"%s","----,--&");
	sprintf(FormatUms,"%s","-----,--&");
	GetCfgValues();
	
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_PRINT_REPORT,0l);
	}
		
return 0;
}

void CMainFrame::OnStartDebug()
{
	MessageBox("Make sure that DEBWIN2 had been started before this demo application. "
				"If this doesn't happen you won't see any debug outputs now!",
				"List & Label Sample App", MB_OK | MB_ICONINFORMATION);
//	LlSetDebug(LL_DEBUG_CMBTLL);
	m_bDebug = TRUE;

}

void CMainFrame::OnStopDebug()
{
	LlSetDebug(FALSE);
	m_bDebug = FALSE;
}

void CMainFrame::OnUpdateStopDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bDebug);
}

void CMainFrame::OnUpdateStartDebug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bDebug);
}


//=============================================================================
void CMainFrame::OnEditLabel()
//=============================================================================
{
	CHAR szFilename[128+1] = "*.lbl";
	HWND hWnd = m_hWnd;
	HJOB hJob;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	// US: initialize Job
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

	// GR: Setzen der List & Label Optionen
	// US: Setting the List & Label options
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	//GR: Exporter aktivieren
	//US: Enable exporter
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
/* ---> GrJ GrJ
	if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
    {
        LlJobClose(hJob);
        return;
    }
< ---- */

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	// GR: Aufruf des Designers

   	sprintf ( szFilename, FILENAME_DEFAULT );

 	if (LlDefineLayout(hJob, hWnd, "Designer", LL_PROJECT_LABEL, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
}

long holetyp(char* tab_nam , char* feld_nam)
{
	static char alttabnam[20];
	static long alttabid ;
	if ( strncmp ( tab_nam, alttabnam, strlen(tab_nam)) == 0 )
	{
		// bereits gefunden
	}
	else
	{
		sprintf ( systables.tabname, "%s" , tab_nam );
		dbClass.sqlopen (tabid_curs);	// destroyed den Cursor, sqlopen refreshed
		if (!dbClass.sqlfetch(tabid_curs))	// gefunden
		{
			alttabid = systables.tabid ;
			sprintf ( alttabnam, "%s", tab_nam );
		}
		else

			return LL_TEXT ;

	}
 	sprintf ( syscolumns.colname, "%s" , feld_nam );
 	syscolumns.tabid = alttabid ;
	dbClass.sqlopen (coltyp_curs);	// destroyed den Cursor, sqlopen refreshed
	if (!dbClass.sqlfetch(coltyp_curs))	// gefunden
	{
		switch (syscolumns.coltype)
		{
		case(iDBCHAR):
			return LL_TEXT ;
		case(iDBSMALLINT):	// smallint
		case(iDBINTEGER):	// integer
		case(iDBDECIMAL):	// decimal
			return LL_NUMERIC ;
		case(iDBDATUM):	// Datumsfeld
			return LL_DATE ;
		}
		return LL_TEXT ;
	}
	else
		return LL_TEXT ;
		
}


int GetRecCount()
{
	gr1von_short = (short) gr1von;
	gr1bis_short = (short) gr1bis;
	gr2von_short = (short) gr2von;
	gr2bis_short = (short) gr2bis;
	gr3von_short = (short) gr3von;
	gr3bis_short = (short) gr3bis;
	gr4von_short = (short) gr4von;
	gr4bis_short = (short) gr4bis;
	gr5von_short = (short) gr5von;
	gr5bis_short = (short) gr5bis;
	gr6von_short = (short) gr6von;
	gr6bis_short = (short) gr6bis;


	return AMoWa.dbcount ();

}


void Variablendefinition (HJOB hJob)
{
		LlDefineVariableExt(hJob, "Mandant",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Jahr",        "2008", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "MitVorjahr",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Vorjahr",        "2007", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "FilVon",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "FilBis",        "999", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "FilBereich",        "1-999", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "KunBereich",        "1-99999999", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "MoBereich",        "1-12", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr1Bereich",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr2Bereich",        "2", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr3Bereich",        "3", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr4Bereich",        "4", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr5Bereich",        "5", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr6Bereich",        "6", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "KunVon",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "KunBis",        "99999", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "MoVon",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "MoBis",        "12", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "KundeVon",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "KundeBis",        "999999", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Divisor",        "1", LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "FormatMe",        "---,--&", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "FormatUms",        "---,--&", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "ausgabe.kun_fil",        "0", LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "Gruppierung",        "1", LL_NUMERIC, NULL);

		LlDefineVariableExt(hJob, "checkgr1n",        "J", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr1s",        "N", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr2n",        "N", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr2s",        "J", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr3n",        "J", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr3s",        "N", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr4n",        "J", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr4s",        "N", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr5n",        "J", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr5s",        "N", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr6n",        "J", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr6s",        "N", LL_TEXT, NULL);

		LlDefineVariableExt(hJob, "gr1von",        "1", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr1bis",        "12", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr2von",        "13", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr2bis",        "15", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr3von",        "16", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr3bis",        "17", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr4von",        "18", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr4bis",        "19", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr5von",        "20", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr5bis",        "21", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr6von",        "22", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "gr6bis",        "23", LL_TEXT, NULL);


}

void VariablenUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{
		sprintf(szTemp2, "%d", Divisor );
		LlDefineVariableExt(hJob, "Divisor",        szTemp2, LL_NUMERIC, NULL);
		LlDefineVariableExt(hJob, "FormatMe",        FormatMe, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "FormatUms",        FormatUms, LL_TEXT, NULL);

		sprintf(szTemp2, "%d", mdn );
		LlDefineVariableExt(hJob, "Mandant",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", jr );
		LlDefineVariableExt(hJob, "Jahr",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", vjr );
		LlDefineVariableExt(hJob, "Vorjahr",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", MitVorjahr );
		LlDefineVariableExt(hJob, "MitVorjahr",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", filvon );
		LlDefineVariableExt(hJob, "FilVon",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", filbis );
		LlDefineVariableExt(hJob, "FilBis",        szTemp2, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "FilBereich",        filbereich, LL_TEXT, NULL);

		LlDefineVariableExt(hJob, "KunBereich",        kunbereich, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "MoBereich",        mobereich, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr1Bereich",        gr1bereich, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr2Bereich",        gr2bereich, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr3Bereich",        gr3bereich, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr4Bereich",        gr4bereich, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr5Bereich",        gr5bereich, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "Gr6Bereich",        gr6bereich, LL_TEXT, NULL);

		sprintf(szTemp2, "%d", kunvon );
		LlDefineVariableExt(hJob, "KunVon",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", kunbis );
		LlDefineVariableExt(hJob, "KunBis",        szTemp2, LL_TEXT, NULL);

		sprintf(szTemp2, "%d", movon );
		LlDefineVariableExt(hJob, "MoVon",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", mobis );
		LlDefineVariableExt(hJob, "MoBis",        szTemp2, LL_TEXT, NULL);

		sprintf(szTemp2, "%hd", ausgabe.kun_fil );
        LlDefineVariableExt(hJob, "ausgabe.kun_fil",        szTemp2, LL_NUMERIC, NULL);

		sprintf(szTemp2, "%d", flg_gruppierung );
		LlDefineVariableExt(hJob, "Gruppierung",        szTemp2, LL_NUMERIC, NULL);

		LlDefineVariableExt(hJob, "KundeVon",        "0", LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "KundeBis",        "0", LL_TEXT, NULL);

		LlDefineVariableExt(hJob, "checkgr1n",        checkgr1n, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr1s",        checkgr1s, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr2n",        checkgr2n, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr2s",        checkgr2s, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr3n",        checkgr3n, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr3s",        checkgr3s, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr4n",        checkgr4n, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr4s",        checkgr4s, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr5n",        checkgr5n, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr5s",        checkgr5s, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr6n",        checkgr6n, LL_TEXT, NULL);
		LlDefineVariableExt(hJob, "checkgr6s",        checkgr6s, LL_TEXT, NULL);


		sprintf(szTemp2, "%d", gr1von_short );
		LlDefineVariableExt(hJob, "gr1von",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr1bis_short );
		LlDefineVariableExt(hJob, "gr1bis",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr2von_short );
		LlDefineVariableExt(hJob, "gr2von",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr2bis_short );
		LlDefineVariableExt(hJob, "gr2bis",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr3von_short );
		LlDefineVariableExt(hJob, "gr3von",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr3bis_short );
		LlDefineVariableExt(hJob, "gr3bis",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr4von_short );
		LlDefineVariableExt(hJob, "gr4von",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr4bis_short );
		LlDefineVariableExt(hJob, "gr4bis",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr5von_short );
		LlDefineVariableExt(hJob, "gr5von",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr5bis_short );
		LlDefineVariableExt(hJob, "gr5bis",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr6von_short );
		LlDefineVariableExt(hJob, "gr6von",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%d", gr6bis_short );
		LlDefineVariableExt(hJob, "gr6bis",        szTemp2, LL_TEXT, NULL);

}

void Felderdefinition (HJOB hJob)
{

    LlDefineFieldExt(hJob, "Jahr",        "2008", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.Jahr",        "1234", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.monat",        "123", LL_NUMERIC, NULL);

	LlDefineFieldExt(hJob, "ausgabe.kun",        "4711", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.fil",        "999", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.kun_fil",        "0", LL_NUMERIC, NULL);
	LlDefineFieldExt(hJob, "adr.adr_krz",        "Testkunde", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr.adr_nam1",        "Testkunde xyz", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr.plz",        "72189", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr.ort1",        "V�hringen", LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "ausgabe.fil_kla",        "A", LL_TEXT, NULL);

    LlDefineFieldExt(hJob, "menge.menge",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.gr1",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.gr2",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.gr3",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.gr4",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.gr5",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.gr6",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.umsatz",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.gr1",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.gr2",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.gr3",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.gr4",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.gr5",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.gr6",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesmenge.menge",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesmenge.gr1",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesmenge.gr2",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesmenge.gr3",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesmenge.gr4",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesmenge.gr5",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesmenge.gr6",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesumsatz.umsatz",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesumsatz.gr1",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesumsatz.gr2",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesumsatz.gr3",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesumsatz.gr4",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesumsatz.gr5",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "gesumsatz.gr6",       "1234.12", LL_NUMERIC, NULL);

    LlDefineFieldExt(hJob, "sk.preis",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "sk.gr1",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "sk.gr2",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "sk.gr3",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "sk.gr4",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "sk.gr5",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "sk.gr6",       "1234.12", LL_NUMERIC, NULL);

    LlDefineFieldExt(hJob, "umsatz.umsatz_sk",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.sk_gr1",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.sk_gr2",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.sk_gr3",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.sk_gr4",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.sk_gr5",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.sk_gr6",       "1234.12", LL_NUMERIC, NULL);

    LlDefineFieldExt(hJob, "ertrag.ertrag",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ertrag.gr1",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ertrag.gr2",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ertrag.gr3",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ertrag.gr4",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ertrag.gr5",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "ertrag.gr6",       "1234.12", LL_NUMERIC, NULL);

    LlDefineFieldExt(hJob, "menge.retouren",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.ret_gr1",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.ret_gr2",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.ret_gr3",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.ret_gr4",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.ret_gr5",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "menge.ret_gr6",        "1234.123", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.retouren",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.ret_gr1",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.ret_gr2",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.ret_gr3",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.ret_gr4",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.ret_gr5",       "1234.12", LL_NUMERIC, NULL);
    LlDefineFieldExt(hJob, "umsatz.ret_gr6",       "1234.12", LL_NUMERIC, NULL);


}

void FelderUebergabe ( HJOB hJob, char szTemp2[], int nRecno )
{
		sprintf(szTemp2, "%hd", jr );
    LlDefineFieldExt(hJob, "Jahr",        szTemp2, LL_NUMERIC, NULL);


		sprintf(szTemp2, "%ld", ausgabe.kun );
    LlDefineFieldExt(hJob, "ausgabe.kun",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%ld", ausgabe.fil );
    LlDefineFieldExt(hJob, "ausgabe.fil",        szTemp2, LL_TEXT, NULL);
		sprintf(szTemp2, "%hd", ausgabe.kun_fil );
    LlDefineFieldExt(hJob, "ausgabe.kun_fil",        szTemp2, LL_NUMERIC, NULL);

	LlDefineFieldExt(hJob, "ausgabe.fil_kla",        ausgabe.fil_kla, LL_TEXT, NULL);

    LlDefineFieldExt(hJob, "adr.adr_krz", adr.adr_krz, LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr.adr_nam1",        adr.adr_nam1, LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr.plz",        adr.plz, LL_TEXT, NULL);
	LlDefineFieldExt(hJob, "adr.ort1",        adr.ort1, LL_TEXT, NULL);


		sprintf(szTemp2, "%.3lf", ausgabe.menge );
    LlDefineFieldExt(hJob, "menge.menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.me_gr1 );
    LlDefineFieldExt(hJob, "menge.gr1",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.me_gr2 );
    LlDefineFieldExt(hJob, "menge.gr2",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.me_gr3 );
    LlDefineFieldExt(hJob, "menge.gr3",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.me_gr4 );
    LlDefineFieldExt(hJob, "menge.gr4",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.me_gr5 );
    LlDefineFieldExt(hJob, "menge.gr5",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.me_gr6 );
    LlDefineFieldExt(hJob, "menge.gr6",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.umsatz );
    LlDefineFieldExt(hJob, "umsatz.umsatz",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_gr1 );
    LlDefineFieldExt(hJob, "umsatz.gr1",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_gr2 );
    LlDefineFieldExt(hJob, "umsatz.gr2",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_gr3 );
    LlDefineFieldExt(hJob, "umsatz.gr3",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_gr4 );
    LlDefineFieldExt(hJob, "umsatz.gr4",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_gr5 );
    LlDefineFieldExt(hJob, "umsatz.gr5",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_gr6 );
    LlDefineFieldExt(hJob, "umsatz.gr6",       szTemp2, LL_NUMERIC, NULL);


		sprintf(szTemp2, "%.3lf", ausgabe.ges_menge );
    LlDefineFieldExt(hJob, "gesmenge.menge",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ges_me_gr1 );
    LlDefineFieldExt(hJob, "gesmenge.gr1",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ges_me_gr2 );
    LlDefineFieldExt(hJob, "gesmenge.gr2",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ges_me_gr3 );
    LlDefineFieldExt(hJob, "gesmenge.gr3",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ges_me_gr4 );
    LlDefineFieldExt(hJob, "gesmenge.gr4",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ges_me_gr5 );
    LlDefineFieldExt(hJob, "gesmenge.gr5",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ges_me_gr6 );
    LlDefineFieldExt(hJob, "gesmenge.gr6",        szTemp2, LL_NUMERIC, NULL);

		sprintf(szTemp2, "%.2lf", ausgabe.ges_umsatz );
    LlDefineFieldExt(hJob, "gesumsatz.umsatz",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ges_ums_gr1 );
    LlDefineFieldExt(hJob, "gesumsatz.gr1",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ges_ums_gr2 );
    LlDefineFieldExt(hJob, "gesumsatz.gr2",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ges_ums_gr3 );
    LlDefineFieldExt(hJob, "gesumsatz.gr3",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ges_ums_gr4 );
    LlDefineFieldExt(hJob, "gesumsatz.gr4",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ges_ums_gr5 );
    LlDefineFieldExt(hJob, "gesumsatz.gr5",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ges_ums_gr6 );
    LlDefineFieldExt(hJob, "gesumsatz.gr6",       szTemp2, LL_NUMERIC, NULL);

		sprintf(szTemp2, "%.2lf", ausgabe.pr_sk );
    LlDefineFieldExt(hJob, "sk.preis",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.pr_sk_gr1 );
    LlDefineFieldExt(hJob, "sk.gr1",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.pr_sk_gr2 );
    LlDefineFieldExt(hJob, "sk.gr2",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.pr_sk_gr3 );
    LlDefineFieldExt(hJob, "sk.gr3",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.pr_sk_gr4 );
    LlDefineFieldExt(hJob, "sk.gr4",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.pr_sk_gr5 );
    LlDefineFieldExt(hJob, "sk.gr5",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.pr_sk_gr6 );
    LlDefineFieldExt(hJob, "sk.gr6",       szTemp2, LL_NUMERIC, NULL);


		sprintf(szTemp2, "%.2lf", ausgabe.umsatz_sk );
    LlDefineFieldExt(hJob, "umsatz.umsatz_sk",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_sk_gr1 );
    LlDefineFieldExt(hJob, "umsatz.sk_gr1",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_sk_gr2 );
    LlDefineFieldExt(hJob, "umsatz.sk_gr2",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_sk_gr3 );
    LlDefineFieldExt(hJob, "umsatz.sk_gr3",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_sk_gr4 );
    LlDefineFieldExt(hJob, "umsatz.sk_gr4",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_sk_gr5 );
    LlDefineFieldExt(hJob, "umsatz.sk_gr5",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ums_sk_gr6 );
    LlDefineFieldExt(hJob, "umsatz.sk_gr6",       szTemp2, LL_NUMERIC, NULL);


		sprintf(szTemp2, "%.2lf", ausgabe.ertrag );
    LlDefineFieldExt(hJob, "ertrag.ertrag",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ertrag_gr1 );
    LlDefineFieldExt(hJob, "ertrag.gr1",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ertrag_gr2 );
    LlDefineFieldExt(hJob, "ertrag.gr2",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ertrag_gr3 );
    LlDefineFieldExt(hJob, "ertrag.gr3",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ertrag_gr4 );
    LlDefineFieldExt(hJob, "ertrag.gr4",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ertrag_gr5 );
    LlDefineFieldExt(hJob, "ertrag.gr5",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ertrag_gr6 );
    LlDefineFieldExt(hJob, "ertrag.gr6",       szTemp2, LL_NUMERIC, NULL);



		sprintf(szTemp2, "%.3lf", ausgabe.retouren_me );
    LlDefineFieldExt(hJob, "menge.retouren",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ret_me_gr1 );
    LlDefineFieldExt(hJob, "menge.ret_gr1",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ret_me_gr2 );
    LlDefineFieldExt(hJob, "menge.ret_gr2",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ret_me_gr3 );
    LlDefineFieldExt(hJob, "menge.ret_gr3",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ret_me_gr4 );
    LlDefineFieldExt(hJob, "menge.ret_gr4",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ret_me_gr5 );
    LlDefineFieldExt(hJob, "menge.ret_gr5",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.3lf", ausgabe.ret_me_gr6 );
    LlDefineFieldExt(hJob, "menge.ret_gr6",        szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.retouren );
    LlDefineFieldExt(hJob, "umsatz.retouren",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ret_gr1 );
    LlDefineFieldExt(hJob, "umsatz.ret_gr1",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ret_gr2 );
    LlDefineFieldExt(hJob, "umsatz.ret_gr2",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ret_gr3 );
    LlDefineFieldExt(hJob, "umsatz.ret_gr3",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ret_gr4 );
    LlDefineFieldExt(hJob, "umsatz.ret_gr4",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ret_gr5 );
    LlDefineFieldExt(hJob, "umsatz.ret_gr5",       szTemp2, LL_NUMERIC, NULL);
		sprintf(szTemp2, "%.2lf", ausgabe.ret_gr6 );
    LlDefineFieldExt(hJob, "umsatz.ret_gr6",       szTemp2, LL_NUMERIC, NULL);

}


//=============================================================================
void CMainFrame::OnEditList()
//=============================================================================
{
	CHAR szFilename[128+1] = "*.lst";
	HWND hWnd = m_hWnd;
	HJOB hJob;
    char *etc;
	char filename[512];

	etc = getenv ("BWS");
    sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	FILENAME_DEFAULT = filename;
	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
	}
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
	}

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden (Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");


	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");


	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		if (LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST|LL_FILE_ALSONEW
								, szFilename, sizeof(szFilename), NULL) < 0)
		{
	        LlJobClose(hJob);
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
			return;
		}
	}

	// GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    // GR: Zur�cksetzen der internen Feld-Puffer
    LlDefineFieldStart(hJob);

//	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);

	if (Listenauswahl != 1)  
    {
	   	sprintf ( szFilename, FILENAME_DEFAULT );
	}
	// GR: Aufruf des Designers
    if (LlDefineLayout(hJob, hWnd, "Design Rezeptur", LL_PROJECT_LIST, szFilename) < 0)
    {
        MessageBox("Error by calling LlDefineLayout", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
        return;
    }
    // GR: Beenden des List & Label Jobs
    LlJobClose(hJob);
	PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
}


//=============================================================================
void CMainFrame::OnPrintLabel()
//=============================================================================
{
	DoLabelPrint();
}


//=============================================================================
void CMainFrame::OnPrintReport()
//=============================================================================
{
    LPSTR CommandLine = GetCommandLine ();
    CToken Token (CommandLine, " ");
	movon = 1;
	mobis = 1;
    char *p;
    Token.NextToken ();
	while ((p = Token.NextToken ()) != NULL)
	{
		if (strcmp (p, "-h") == 0)
		{
              MessageBox ("Aufruf : sel_WaGruppen -mdn Mandant -datvon Datum-von -datbis Datum-bis ",
                                "",
                                MB_OK);
              return;
		}
		if (strcmp (p, "-mdn") == 0)
		{
			p = Token.NextToken ();
			if (p == NULL) break;
            mdn = (atoi (p));
		}

	}
    CDialogDatum dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber OK zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// ZU ERLEDIGEN: F�gen Sie hier Code ein, um ein Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		return;
	}

	DoListPrint();
}

//=============================================================================
void CMainFrame::DoLabelPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lbl", szTemp[30], szTemp2[40], szBoxText[200];
	HJOB hJob;
	int  nRet = 0;

	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// US: Choose new expression mode with formula support
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);
	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog

   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LABEL
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
   		{
	    	if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

    for (int i=1; i<10; i++)
	{
		sprintf(szTemp, "Field%d", i);
    	LlDefineVariable(hJob, szTemp, szTemp);
   	}

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

    // GR: Druck starten

	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LABEL, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_BRIDGEMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }

	// GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	LlPrintSetOption(hJob, LL_PRNOPT_OFFSET, 0);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
        return;
    }


	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

	char szPrinter[80], szPort[20];
	int  nRecCount = 30, nErrorValue = 0, nRecno, nLastPage;

	LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

    nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt

	// GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	while (nRecno < nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& nErrorValue == 0)
	{
		for (i=1; i<10; i++)
		{
			sprintf(szTemp, "Field%d", i);
			if (LlPrintIsVariableUsed(hJob, szTemp))
			{
				sprintf(szTemp2, "contents of Field%d, record %d", i, nRecno);
    			LlDefineVariable(hJob, szTemp, szTemp2);
			}
    	}

    	// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    	sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    	nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        if (nErrorValue == LL_ERR_USER_ABORTED)
    	{
    		LlPrintEnd(hJob,0);
    		LlJobClose(hJob);
    		return;
    	}

    	// GR: Variablen drucken
    	nErrorValue = LlPrint(hJob);

		// GR: gehe zum naechsten Datensatz
    	nRecno++;
	}

	//GR: Druck beenden
	LlPrintEnd(hJob,0);


	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) l�schen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }

	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
}

//=============================================================================
void CMainFrame::DoListPrint()
//=============================================================================
{
    HWND hWnd = m_hWnd;
    CHAR szFilename[128+1] = "*.lst", szTemp[50], szTemp2[60], szBoxText[200];
	CHAR dmess[128] = "";
	HJOB hJob;
	int  nRet = 0;
	strcpy(szTemp, " ");
	char *etc;
	char filename[512];

	etc = getenv ("BWS");
    sprintf (filename, "%s\\format\\LL\\%s.lst", etc, Listenname);
	FILENAME_DEFAULT = filename;


	// GR: Initialisieren von List & Label.
	//     Es wird ein Job geoeffnet
	hJob = LlJobOpen(LUL_LANGUAGE);
	if (hJob==LL_ERR_BAD_JOBHANDLE)
	{
		MessageBox("Job can't be initialized!", "List & Label Sample App", MB_OK|MB_ICONSTOP);
		return;
    }
	else if (hJob==LL_ERR_NO_LANG_DLL)
	{
		MessageBox("Language file not found!\nEnsure that *.lng files can be found in your LuL DLL directory.",
					"List & Label Sample App",
					MB_OK|MB_ICONSTOP);
		return;
    }

 // Ich muss wohl noch meine Lizenz setzen , damit Lauff�hig beim Kunden !?(Siehe redist.txt)
	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "q6GLOw");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LICENSINGINFO , "pn4SOw");


	//GR: Exporter aktivieren
	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cm32l9ex.llx");
//	LlSetOptionString(hJob, LL_OPTIONSTR_LLXPATHLIST, "cmll11ex.llx");

	// GR: Setzen der List & Label Optionen
	LlSetOption(hJob, LL_OPTION_NEWEXPRESSIONS, TRUE);

	// GR: Setze den Modus multiple datalines fuer Tabellen (wird nur fuer SAMPLE3.LST benoetigt)
	//     Der Benutzer hat nun mehrere Zeilendefinitionen in Tabellen zu verfuegung.
	LlSetOption(hJob, LL_OPTION_MULTIPLETABLELINES, TRUE);

	LlSetOption(hJob, LL_OPTION_WIZ_FILENEW, TRUE);

	// GR: Auswahl der Projekt-Datei �ber Datei-Auswahl-Dialog
   	sprintf ( szFilename, FILENAME_DEFAULT );
	if (Listenauswahl == 1)  //#LuD
    {
		nRet = LlSelectFileDlgTitleEx(hJob, hWnd, "", LL_PROJECT_LIST
								, szFilename,sizeof(szFilename), NULL);
		if (nRet < 0)
	   	{
    		if (nRet != LL_ERR_USER_ABORTED)
				MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
			LlJobClose(hJob);
			return;
		}
	}

    // GR: Zur�cksetzen der internen Variablen-Puffer
    LlDefineVariableStart(hJob);

	dbClass.opendbase ("bws");
    Variablendefinition (hJob);
    Felderdefinition (hJob);




    // GR: Druck starten
	if (LlPrintWithBoxStart(hJob, LL_PROJECT_LIST, szFilename,
							LL_PRINT_EXPORT,
							LL_BOXTYPE_NORMALMETER, hWnd, "Printing...") < 0)
    {
        MessageBox("Error While Printing", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
        LlJobClose(hJob);
        return;
    }
    // GR: Einstellungen bzw. optionen f�r den Drucker-Optionen-Dialog
	LlPrintSetOption(hJob, LL_PRNOPT_COPIES, 	LL_COPIES_HIDE);
    LlPrintSetOption(hJob, LL_PRNOPT_STARTPAGE, 1);
	if (LlPrintOptionsDialog(hJob, hWnd, "Select printing options") < 0)
    {
        LlPrintEnd(hJob,0);
        LlJobClose(hJob);
		if (Hauptmenue == 0)  //#LuD
		{
			PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
		}
		
        return;
    }


	//GR: Druckziel abfragen
	CString sMedia;
	LlPrintGetOptionString(hJob, LL_PRNOPTSTR_EXPORT, sMedia.GetBuffer(256), 256);
	sMedia.ReleaseBuffer();

    int  nRecCount = 1, nErrorValue = 0, nPrintFehler = 0, nLastPage, nRecno;
	char szPrinter[80], szPort[20];

    LlPrintGetPrinterInfo(hJob, szPrinter, sizeof(szPrinter), szPort, sizeof(szPort));

	nLastPage = LlPrintGetOption(hJob, LL_OPTION_LASTPAGE);
    nRecno = 1;

    // GR: Druckschleife
    //     Diese wird so lange wiederholt, bis saemtliche Daten abgearbeitet wurden, oder
    //     ein Fehler auftritt.
	nRecCount = GetRecCount () ;
	if (nRecCount < 1) 
    {
		sprintf (dmess, "Datensatz nicht gefunden ");
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
//	sprintf (dmess, "nRecCount = %hd",nRecCount);  //050803
//	MessageBox(dmess, "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);
/******
	int di = AWoWa.leseDS (nErrorValue);   //       1.DATENSATZ lesen f�r Variablen�bergabe
	if (di != 0) 
    {
		sprintf (dmess, "Datensatz nicht gefunden SQL:%hd",di);
		MessageBox (dmess, "", MB_ICONERROR);
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;

	}
******/
//	APreis.SetDatabase (dbClass.GetDatabase());
	AMoWa.openDS ();
	int dleseStatus = 0;
	while (nRecno <= nRecCount
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& (nErrorValue == 0 || nErrorValue == LL_WRN_REPEAT_DATA)
			&& (dleseStatus == 0))
   	{
	    VariablenUebergabe ( hJob, szTemp2, nRecno );

		nErrorValue = LlPrint(hJob);
		while (nRecno <= nRecCount 
			&& (nErrorValue == 0) 
			&& (LlPrintGetCurrentPage(hJob) <= nLastPage)
			&& ((dleseStatus = AMoWa.leseDS (nPrintFehler)) == 0))  //       DATENSATZ lesen

		{

		    FelderUebergabe ( hJob, szTemp2, nRecno );
    		// GR: Prozentbalken in der Fortschritts-Anzeige setzen
    		sprintf(szBoxText, "printing on %s %s", szPrinter, szPort);
    		nErrorValue = LlPrintSetBoxText(hJob, szBoxText, (100 * nRecno / nRecCount));
        	if (nErrorValue == LL_ERR_USER_ABORTED)
   			{
    			LlPrintEnd(hJob,0);
    			LlJobClose(hJob);
				if (Hauptmenue == 0)  //#LuD
				{
					PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
				}
    			return;
   			}

    		// GR: Drucken der aktuellen Tabellenzeile
    		nErrorValue = LlPrintFields(hJob);
			nPrintFehler = nErrorValue;
//050803
			if (nErrorValue == LL_WRN_REPEAT_DATA)
			{
					// GR: Aktualisieren der seitenabhaengigen Variablen
			    VariablenUebergabe ( hJob, szTemp2, nRecno );
				nErrorValue = LlPrint(hJob);
//			    FelderUebergabe ( hJob, szTemp2, nRecno );
//	    		nErrorValue = LlPrintFields(hJob);
			}


			if (nErrorValue == 0)	// US: everything fine, record could have been printed...
	    	{
				// US: ...but probably the user defined a filter condition!
				//     So before updating time dependent variables we have to check if the record really has been printed:
				if (LlPrintDidMatchFilter(hJob))
				{
					// GR: Aktualisieren der zeitabhaengigen Variable 'FixedVariable2'
					// US: update the time dependent variable 'FixedVariable2'
					//     coming after printig the list(s)
		    		sprintf(szTemp2, "FixedVariable2, record %d", nRecno);
		    		LlDefineVariable(hJob, "FixedVariable2", szTemp2);
		    	}

	    		if (nPrintFehler == 0) nRecno++;	// GR: gehe zum naechsten Datensatz
			}
		}
  	}

	// US: all records have been printed, now flush the table
	//     If footer doesn't fit to this page try again for the next page:

	nErrorValue = LlPrintFieldsEnd(hJob);
	if (nErrorValue == LL_WRN_REPEAT_DATA)
	{
		// GR: Aktualisieren der seitenabhaengigen Variablen
	    VariablenUebergabe ( hJob, szTemp2, nRecno );
	    FelderUebergabe ( hJob, szTemp2, nRecno );

		// US: ... and then try again:
		nErrorValue = LlPrintFieldsEnd(hJob);
	}

	if (nErrorValue == LL_WRN_REPEAT_DATA)	// US: footer still doesn't fit!
		MessageBox("Error because table is too small for footer!", "List & Label Sample App", MB_OK|MB_ICONEXCLAMATION);

	//GR: Druck beenden
	LlPrintEnd(hJob,0);
	
	//GR: Druckziel = Preview?
	if (sMedia=="PRV")
    {
        // GR: Beim Preview-Druck Preview anzeigen und dann Preview-Datei (.LL) loeschen
        LlPreviewDisplay(hJob, szFilename, "", hWnd);

        LlPreviewDeleteFiles(hJob, szFilename, "");
    }
	// GR: Beenden des List & Label Jobs
	LlJobClose(hJob);
	if (Hauptmenue == 0)  //#LuD
	{
		PostMessage(WM_COMMAND,ID_APP_EXIT,0l);
	}
}

//=============================================================================
LRESULT CMainFrame::OnLulMessage(WPARAM wParam, LPARAM lParam)
//=============================================================================
{
	// GR: Dies ist die List & Label Callback Funktion.
	//     Saemtliche Rueckmeldungen bzw. Events werden dieser Funktion
	//     gemeldet.
	// US: This is the List & Label Callback function.
	//     Is is called for requests an notifications.


	PSCLLCALLBACK	pscCallback = (PSCLLCALLBACK) lParam;
	LRESULT			lRes = TRUE;
	CString			sVariableDescr;

	static CHAR szHelpText[256];

	ASSERT(pscCallback->_nSize == sizeof(scLlCallback));	// GR: Die groesse der Callback Struktur muss stimmen!
															// US: sizes of callback structs must match!

	switch(wParam)
	{
		case LL_CMND_VARHELPTEXT:	// GR: Es wird ein Beschreibungstext f�r eine Variable erfragt.
									// US: Helptext needed for selected variable within designer

				// GR: ( pscCallback->_lParam enth�lt einen LPCSTR des Beschreibungstextes )
				// US: ( pscCallback->_lParam contains a LPCSTR to the name of the selected variable )

				sVariableDescr = (LPCSTR)pscCallback->_lParam;

				if (!sVariableDescr.IsEmpty())
					sprintf(szHelpText,
							"This is the sample field / variable '%s'.",
							(LPCSTR)sVariableDescr);
				else
					strcpy(szHelpText, "No variable or field selected.");

				pscCallback->_lReply = (LPARAM)szHelpText;
				break;

		default:
				pscCallback->_lReply = lRes = FALSE; // GR: Die Nachricht wurde nicht bearbeitet.
													 // US: indicate that message hasn't been processed
	}

	return(lRes);
}


void CMainFrame::GetCfgValues (void)
{
       char cfg_v [512];

       if (ProgCfg == NULL) return;

       cfgOK = TRUE;
	   strcpy(R_hwg,"N");
	   strcpy(R_wg,"N");
	   strcpy(R_ag,"N");
	   strcpy(R_smt,"N");
	   sprintf(Listenname,"WaGruppen");
       if (ProgCfg->GetCfgValue ("Listenname", cfg_v) == TRUE)
       {
		   sprintf(Listenname,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Listenauswahl", cfg_v) == TRUE)
       {
           Listenauswahl = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Hauptmenue", cfg_v) == TRUE)
       {
           Hauptmenue = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Divisor", cfg_v) == TRUE)
       {
           Divisor = atoi (cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("FormatMe", cfg_v) == TRUE)
       {
		   sprintf(FormatMe,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("FormatUms", cfg_v) == TRUE)
       {
		   sprintf(FormatUms,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("flg_gruppierung", cfg_v) == TRUE)
       {
           flg_gruppierung = atoi (cfg_v);
		   if (flg_gruppierung == 1)  strcpy (R_hwg,"J");
		   if (flg_gruppierung == 2)  strcpy (R_wg,"J");
		   if (flg_gruppierung == 3)  strcpy (R_ag,"J");
		   if (flg_gruppierung == 4)  strcpy (R_smt,"J");
	   }
       if (ProgCfg->GetCfgValue ("Gruppe1Bereich", cfg_v) == TRUE)
       {
           strcpy(gr1bereich,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe2Bereich", cfg_v) == TRUE)
       {
           strcpy(gr2bereich,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe3Bereich", cfg_v) == TRUE)
       {
           strcpy(gr3bereich,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe4Bereich", cfg_v) == TRUE)
       {
           strcpy(gr4bereich,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe5Bereich", cfg_v) == TRUE)
       {
           strcpy(gr5bereich,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe6Bereich", cfg_v) == TRUE)
       {
           strcpy(gr6bereich,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe1N", cfg_v) == TRUE)
       {
		   sprintf(checkgr1n,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe1SA", cfg_v) == TRUE)
       {
		   sprintf(checkgr1s,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe2N", cfg_v) == TRUE)
       {
		   sprintf(checkgr2n,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe2SA", cfg_v) == TRUE)
       {
		   sprintf(checkgr2s,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe3N", cfg_v) == TRUE)
       {
		   sprintf(checkgr3n,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe3SA", cfg_v) == TRUE)
       {
		   sprintf(checkgr3s,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe4N", cfg_v) == TRUE)
       {
		   sprintf(checkgr4n,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe4SA", cfg_v) == TRUE)
       {
		   sprintf(checkgr4s,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe5N", cfg_v) == TRUE)
       {
		   sprintf(checkgr5n,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe5SA", cfg_v) == TRUE)
       {
		   sprintf(checkgr5s,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe6N", cfg_v) == TRUE)
       {
		   sprintf(checkgr6n,cfg_v);
	   }
       if (ProgCfg->GetCfgValue ("Gruppe6SA", cfg_v) == TRUE)
       {
		   sprintf(checkgr6s,cfg_v);
	   }

       if (ProgCfg->GetCfgValue ("DEBUG", cfg_v) == TRUE)
       {
				 Debug.LogName = "sel_WaGruppen";
				 Debug.dbMode = atoi (cfg_v);
	   }




}


    // GR: Definition der Barcode-Variablen
    //     Normalerweise definiert man nur die Barcode-Typen die man benoetigt
	/************
	LlDefineFieldExt(hJob, "Barcode_EAN13", "44|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P2", "44|44444|44444|44", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN13P5", "44|44444|44444|44444", LL_BARCODE_EAN13, NULL);
	LlDefineFieldExt(hJob, "Barcode_EAN128", "EAN128ean128", LL_BARCODE_EAN128, NULL);
	LlDefineFieldExt(hJob, "Barcode_CODE128", "Code 128", LL_BARCODE_CODE128, NULL);
	LlDefineFieldExt(hJob, "Barcode_Codabar", "A123456A", LL_BARCODE_CODABAR, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCA", "44|44444", LL_BARCODE_EAN8, NULL);
	LlDefineFieldExt(hJob, "Barcode_UPCE", "4|44444|44444", LL_BARCODE_UPCA, NULL);
	LlDefineFieldExt(hJob, "Barcode_3OF9", "*TEST*", LL_BARCODE_3OF9, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IND", "44444", LL_BARCODE_25INDUSTRIAL, NULL);
	LlDefineFieldExt(hJob, "Barcode_25IL", "444444", LL_BARCODE_25INTERLEAVED, NULL);
	LlDefineFieldExt(hJob, "Barcode_25MAT", "44444", LL_BARCODE_25MATRIX, NULL);
	LlDefineFieldExt(hJob, "Barcode_25DL", "44444", LL_BARCODE_25DATALOGIC, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET5", "44444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET10", "44444-4444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_POSTNET12", "44444-444444", LL_BARCODE_POSTNET, NULL);
	LlDefineFieldExt(hJob, "Barcode_FIM", "A", LL_BARCODE_FIM, NULL);
	**********/
void fitapldefines (HJOB hJob)
{

}


