#ifndef _A_MO_WA_DEF
#define _A_MO_WA_DEF

struct A_BAS { 
   short     me_einh;
   double	a_gew;
};
extern struct A_BAS a_bas, a_bas_null;

struct AUSGABE { 
   short     fil;
   char     fil_kla[2];
   long     vertr;
   long     kun;
   short     kun_fil;
   double menge;
   double me_gr1;
   double me_gr2;
   double me_gr3;
   double me_gr4;
   double me_gr5;
   double me_gr6;
   double menge_sa;
   double me_sa_gr1;
   double me_sa_gr2;
   double me_sa_gr3;
   double me_sa_gr4;
   double me_sa_gr5;
   double me_sa_gr6;
   double ges_menge;
   double ges_me_gr1;
   double ges_me_gr2;
   double ges_me_gr3;
   double ges_me_gr4;
   double ges_me_gr5;
   double ges_me_gr6;

   double umsatz;
   double ums_gr1;
   double ums_gr2;
   double ums_gr3;
   double ums_gr4;
   double ums_gr5;
   double ums_gr6;
   double ges_umsatz;
   double ges_ums_gr1;
   double ges_ums_gr2;
   double ges_ums_gr3;
   double ges_ums_gr4;
   double ges_ums_gr5;
   double ges_ums_gr6;

   double umsatz_sk;
   double ums_sk_gr1;
   double ums_sk_gr2;
   double ums_sk_gr3;
   double ums_sk_gr4;
   double ums_sk_gr5;
   double ums_sk_gr6;

   double umsatz_sk_sa;
   double ums_sk_sa_gr1;
   double ums_sk_sa_gr2;
   double ums_sk_sa_gr3;
   double ums_sk_sa_gr4;
   double ums_sk_sa_gr5;
   double ums_sk_sa_gr6;

   double pr_sk;
   double pr_sk_gr1;
   double pr_sk_gr2;
   double pr_sk_gr3;
   double pr_sk_gr4;
   double pr_sk_gr5;
   double pr_sk_gr6;


   double umsatz_sa;
   double ums_sa_gr1;
   double ums_sa_gr2;
   double ums_sa_gr3;
   double ums_sa_gr4;
   double ums_sa_gr5;
   double ums_sa_gr6;

   double ertrag;
   double ertrag_gr1;
   double ertrag_gr2;
   double ertrag_gr3;
   double ertrag_gr4;
   double ertrag_gr5;
   double ertrag_gr6;

   double retouren_sa;
   double ret_sa_gr1;
   double ret_sa_gr2;
   double ret_sa_gr3;
   double ret_sa_gr4;
   double ret_sa_gr5;
   double ret_sa_gr6;

   double retouren;
   double ret_gr1;
   double ret_gr2;
   double ret_gr3;
   double ret_gr4;
   double ret_gr5;
   double ret_gr6;

   double retouren_me_sa;
   double ret_me_sa_gr1;
   double ret_me_sa_gr2;
   double ret_me_sa_gr3;
   double ret_me_sa_gr4;
   double ret_me_sa_gr5;
   double ret_me_sa_gr6;

   double retouren_me;
   double ret_me_gr1;
   double ret_me_gr2;
   double ret_me_gr3;
   double ret_me_gr4;
   double ret_me_gr5;
   double ret_me_gr6;

   double fil_menge;
   double fil_umsatz;
   double fil_ges_umsatz;
   double fil_anteil;
   double fil_ertrag;
   double auswahl_menge;
   double auswahl_umsatz;
   double auswahl_ertrag;
   double ges_ertrag;
   long fil_anzahl;
   double a;
   char a_bz1[25];
   double ProzGesamt;
   double ProzAuswahl;
};
extern struct AUSGABE ausgabe,ausgabe_we, ausgabe_null;

struct A_MO_WA {
   short     delstatus;
   short     mdn;
   short     fil;
   long      kun;
   double    a;
   short     jr;
   short     mo;
   double    me_vk_mo;
   double    ums_vk_mo;
   double    ums_fil_ek_mo;
   double    me_vk_sa_mo;
   double    ums_vk_sa_mo;
   double    ums_fil_ek_sa_mo;
   double    me_ret_mo;
   double    ums_hk_vkost_mo;
   double    ums_hk_tkost_mo;
   double    ums_sk_vkost_mo;
   double    ums_sk_tkost_mo;
   double    ums_mat_o_b_mo;
   double    ums_ret_mo;
   double    ums_fil_ek_ret_mo;
   double    gn_pkt_gbr_ges;
   short     waehrung;
   double    ums_vk_e;
   double    ums_vk_f;
   double    ums_vk_sa_e;
   double    ums_vk_sa_f;
   double    ums_ret_e;
   double    ums_ret_f;
   double    ums_fil_ek_e;
   double    ums_fil_ek_ret_e;
   double    ums_fil_ek_sa_e;
   double    gn_pkt_gbr_e;
   double    m_kun_mo;
   double    u_kun_mo;
   double    u_kun_fek_mo;
   double    m_kun_ret_mo;
   double    u_kun_ret_mo;
   double    u_kun_fek_ret_mo;
   double    ue_kun_mo;
   double    ue_kun_fek_mo;
   double    ue_kun_ret_mo;
   double    ue_kun_fek_ret_mo;
   double    m_upls_mo;
   double    u_upls_mo;
   double    u_upls_fek_mo;
   double    ue_upls_mo;
   double    ue_upls_fek_mo;
   double    m_umin_mo;
   double    u_umin_mo;
   double    u_umin_fek_mo;
   double    ue_umin_mo;
   double    ue_umin_fek_mo;
   double    m_we_mo;
   double    u_we_ek_mo;
   double    u_we_fek_mo;
   double    u_we_fvk_mo;
   double    m_we_ret_mo;
   double    u_we_ret_ek_mo;
   double    u_we_ret_fek_mo;
   double    u_we_ret_fvk_mo;
   double    ue_we_ek_mo;
   double    ue_we_fek_mo;
   double    ue_we_fvk_mo;
   double    ue_we_ret_ek_mo;
   double    ue_we_ret_fek_mo;
   double    ue_we_ret_fvk_mo;
   double    me1_vk_mo;
   double    me2_vk_mo;
   double    me3_vk_mo;
};
extern struct A_MO_WA a_mo_wa, a_mo_wa_null;

class A_MO_WA_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
               void prepare_per (void);
				int readcursor_we;
       public :
               int dbcount (void);
               int leseDS (int);
               int openDS (void);
               void initialisiere (void);
               A_MO_WA_CLASS () : DB_CLASS ()
               {
				 readcursor_we = -1;
               }
               ~A_MO_WA_CLASS ()
               {
		         if (readcursor_we != -1) sqlclose (readcursor_we); 
               } 


};
#endif
