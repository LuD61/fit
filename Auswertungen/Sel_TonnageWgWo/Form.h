#ifndef _FORM_DEF
#define _FORM_DEF
#include <odbcinst.h>

#define FRMCHAR 0
#define FRMSHORT 1
#define FRMLONG 2
#define FRMDOUBLE 3
#define FRMDATE 4
#define FRMBOOL 5
#define FRMCHARBOOL 6

#define LISTBOX 1
#define COMBOBOX 2
#define COMBOLIST 3


class CForm
{
   protected :
       int Id;
       void *VarDB;
       int  Type;
       char *Picture;
       int VarLen;
       int ListboxType;
       char *DbItem;
   public :
       CForm ();
       CForm (int Id, void *VarDB, int Type, int VarLen);
       CForm (int Id, void *VarDB, int Type, char *Picture);
       ~CForm ();

       void SetItem (int Id, void *VarDB, int Type, char *Picture);

       int GetId (void);
       void *GetVarDB (void);
       int GetType (void);
       void SetListboxType (int);
       int GetListboxType (void);
       void SetDbItem (char *);
       char * GetDbItem (void);

       void Enable (CWnd *, BOOL);
       CString& ToString (CString&);
       void FromString (CString&);
       CString& DateToString (DATE_STRUCT *);
       void StringToDate (char *, DATE_STRUCT *);
       void TestDate ();

       virtual void ToScreen (CWnd *);
       virtual void FromScreen (CWnd *);
       virtual void FillListBox (CWnd *, char *) {}
       virtual void FillListBox (CWnd *, char **){}
       virtual void FillPtBox (void *, CWnd *, char *){}
       virtual void FillPtBox (void *, CWnd *){}
       virtual void FillPtBoxLong (void *, CWnd *, char *){}
       virtual void FillPtBoxLong (void *, CWnd *){}
};

class CListForm : virtual public CForm
{
   protected :
        CListBox *lb; 
        CComboBox *cb; 
       
   public :
       CListForm (int Id, void *VarDB, int Type, int VarLen, int ListboxType);
       CListForm (int Id, void *VarDB, int Type, char *Picture, int ListboxType);
       CListForm (int Id, void *VarDB, int Type, int VarLen, int ListboxType, char *DbItem);
       CListForm (int Id, void *VarDB, int Type, char *Picture, int ListboxType, char *DbItem);

       void FillListBox (CWnd *, char *);
       void FillListBox (CWnd *, char **);
       void FillPtBox (void *, CWnd *, char *);
       void FillPtBox (void *, CWnd *);
       void FillPtBoxLong (void *, CWnd *, char *);
       void FillPtBoxLong (void *, CWnd *);
       void GetText (CWnd *, int, CString&);
       void ToScreen (CWnd *);
       void FromScreen (CWnd *);
};



#endif