#ifndef _A_WO_WA_DEF
#define _A_WO_WA_DEF

struct A_BAS { 
   short     me_einh;
   double	a_gew;
};
extern struct A_BAS a_bas, a_bas_null;

struct AUSGABE { 
   short     fil;
   short     wg;
   char     mo1[10];
   double    me_vk_mo1;
   char     mo2[10];
   double    me_vk_mo2;
   char     mo3[10];
   double    me_vk_mo3;
   char     mo4[10];
   double    me_vk_mo4;
   char     mo5[10];
   double    me_vk_mo5;
   char     mo6[10];
   double    me_vk_mo6;
   char     mo7[10];
   double    me_vk_mo7;
   char     mo8[10];
   double    me_vk_mo8;
   char     mo9[10];
   double    me_vk_mo9;
   char     mo10[10];
   double    me_vk_mo10;
   char     mo11[10];
   double    me_vk_mo11;
   char     mo12[10];
   double    me_vk_mo12;
   char     mo13[10];
   double    me_vk_mo13;
};
extern struct AUSGABE ausgabe, ausgabe_null;

struct A_WO_WA { 
   short     delstatus;
   short     mdn;
   short     fil;
   long      kun;
   double    a;
   short     jr;
   short     wo;
   double    me_vk_wo;
   double    ums_vk_wo;
   double    ums_fil_ek_wo;
   double    me_vk_sa_wo;
   double    ums_vk_sa_wo;
   double    ums_fil_ek_sa_wo;
   double    me_ret_wo;
   double    ums_ret_wo;
   double    ums_hk_vkost_wo;
   double    ums_hk_tkost_wo;
   double    ums_sk_vkost_wo;
   double    ums_sk_tkost_wo;
   double    ums_mat_o_b_wo;
   double    ums_fil_ek_ret_wo;
   double    gn_pkt_gbr_ges;
   short     waehrung;
   double    ums_vk_e;
   double    ums_vk_f;
   double    ums_vk_sa_e;
   double    ums_vk_sa_f;
   double    ums_ret_e;
   double    ums_ret_f;
   double    ums_fil_ek_e;
   double    ums_fil_ek_ret_e;
   double    ums_fil_ek_sa_e;
   double    gn_pkt_gbr_e;
   double    m_kun_wo;
   double    u_kun_wo;
   double    u_kun_fek_wo;
   double    m_kun_ret_wo;
   double    u_kun_ret_wo;
   double    u_kun_fek_ret_wo;
   double    ue_kun_wo;
   double    ue_kun_fek_wo;
   double    ue_kun_ret_wo;
   double    ue_kun_fek_ret_wo;
   double    m_upls_wo;
   double    u_upls_wo;
   double    u_upls_fek_wo;
   double    ue_upls_wo;
   double    ue_upls_fek_wo;
   double    m_umin_wo;
   double    u_umin_wo;
   double    u_umin_fek_wo;
   double    ue_umin_wo;
   double    ue_umin_fek_wo;
   double    m_we_wo;
   double    u_we_ek_wo;
   double    u_we_fek_wo;
   double    u_we_fvk_wo;
   double    m_we_ret_wo;
   double    u_we_ret_ek_wo;
   double    u_we_ret_fek_wo;
   double    u_we_ret_fvk_wo;
   double    ue_we_ek_wo;
   double    ue_we_fek_wo;
   double    ue_we_fvk_wo;
   double    ue_we_ret_ek_wo;
   double    ue_we_ret_fek_wo;
   double    ue_we_ret_fvk_wo;
   double    me1_vk_wo;
   double    me2_vk_wo;
   double    me3_vk_wo;
};
extern struct A_WO_WA a_wo_wa, a_wo_wa_null;

class A_WO_WA_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leseDS (int);
               int openDS (void);
               A_WO_WA_CLASS () : DB_CLASS ()
               {
               }
};
#endif
