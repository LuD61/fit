//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by llmfc.rc
//
#define IDR_MAINFRAME                   2
#define IDD_ABOUTBOX                    100
#define IDD_DIALOG1                     101
#define IDC_WOVON                       101
#define IDC_WOBIS                       102
#define IDC_EDIT1                       103
#define IDC_JAHR                        103
#define IDC_MANDANT                     104
#define IDC_FILIALE                     105
#define IDC_FILIALEBIS                  106
#define IDC_CHECK1                      107
#define ID_PRINT_LIST_PRINTER           32770
#define ID_PRINT_LIST_PREVIEW           32771
#define ID_PRINT_LABEL_PRINTER          32772
#define ID_PRINT_LABEL_PREVIEW          32773
#define ID_EDIT_LIST                    32774
#define ID_EDIT_LABEL                   32775
#define ID_FILE_DEBUG                   32776
#define ID_FILE_STOP_DEBUG              32778
#define ID_FILE_START_DEBUG             32779
#define ID_PRINT_REPORT                 32780
#define ID_PRINT_LABEL                  32781

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         108
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
