#include "stdafx.h"
#include "Form.h"
#include "dbclass.h"

CForm::CForm ()
{
    Id = 0;
    VarDB = NULL;
    Type = 0;
    VarLen = 0;
    ListboxType = 0;
    DbItem = NULL;
}
    
CForm::CForm (int Id, void *VarDB, int Type, int VarLen)
{
    this->Id     = Id;
    this->VarDB  = VarDB;
    this->Type   = Type;
    this->VarLen = VarLen;
    switch (Type)
    {
           case FRMSHORT :
               Picture = "%hd";
               break;
           case FRMLONG :
               Picture = "%ld";
               break;
           case FRMDOUBLE :
               Picture = "%.4lf";
               break;
           default :
               Picture = "";
    }
    ListboxType = 0;
    DbItem = NULL;
}

CForm::CForm (int Id, void *VarDB, int Type, char *Picture) 
{
    this->Id      = Id;
    this->VarDB   = VarDB;
    this->Type    = Type;
    this->Picture = Picture;
    this->VarLen  = 256;
    ListboxType = 0;
    DbItem = NULL;
}

CForm::~CForm () 
{
}

void CForm::SetItem (int Id, void *VarDB, int Type, char *Picture)
{
    this->Id      = Id;
    this->VarDB   = VarDB;
    this->Type    = Type;
    this->Picture = Picture;
}

int CForm::GetId (void)
{
    return Id;
}

void *CForm::GetVarDB (void)
{
    return VarDB;
}

int CForm::GetType (void)
{
    return Type;
}

void CForm::ToScreen (CWnd *cWnd)
{
    CString Text;
    BOOL State;

	CWnd *Item = cWnd->GetDlgItem (Id);
    switch (Type)
    {
         case FRMCHAR :
             Text.Format ("%s", VarDB);
             Text.TrimRight ();
             Item->SetWindowText (Text.GetBuffer (256));
             break;
         case FRMSHORT :
             Text.Format (Picture, *(short *) VarDB);
             Item->SetWindowText (Text.GetBuffer (256));
             break;
         case FRMLONG :
             Text.Format (Picture, *(long *) VarDB);
             Item->SetWindowText (Text.GetBuffer (256));
             break;
         case FRMDOUBLE :
             Text.Format (Picture, *(double *) VarDB);
             Item->SetWindowText (Text.GetBuffer (256));
             break;
         case FRMDATE :
             Text = DateToString ((DATE_STRUCT *) VarDB);
             Item->SetWindowText (Text.GetBuffer (256));
             break;
         case FRMBOOL :
             State = *(BOOL *) VarDB;
             Item->SetWindowText (Text.GetBuffer (256));
             ((CButton *) Item)->SetCheck (State);
             break;
         case FRMCHARBOOL :
             Text.Format ("%s", VarDB);
             if (Text.CompareNoCase ("J") == 0)
             {
                   State = TRUE;
             }
             else
             {
                   State = FALSE;
             }
             ((CButton *) Item)->SetCheck (State);
             break;
    }
}

void CForm::FromScreen (CWnd *cWnd)
{
    CString Text;
    char txt [256];
    BOOL State;

	CWnd *Item = cWnd->GetDlgItem (Id);

    if (Type == FRMBOOL)
    {
        State = ((CButton *) Item)->GetCheck ();
        *(BOOL *) VarDB = State;
        return;
    }
    else if (Type == FRMBOOL)
    {
        State = ((CButton *) Item)->GetCheck ();
        if (State)
        {
            Text = "J";
        }
        else
        {
            Text = "N";
        }
        strcpy ((char *) VarDB, Text.GetBuffer (256));
        return;
    }
    ((CEdit *) Item)->GetWindowText (txt, VarLen);
    switch (Type)
    {
         case FRMCHAR :
             strcpy ((char *) VarDB, txt);
             break;
         case FRMSHORT :
             *(short *) VarDB = atoi (txt);
             break;
         case FRMLONG :
             *(long *) VarDB = atol (txt);
             break;
         case FRMDOUBLE :
             *(double *) VarDB = (double) atof (txt);
             break;
         case FRMDATE :
             StringToDate (txt, (DATE_STRUCT *) VarDB);
             break;
    }
}

CString& CForm::ToString (CString& Str)
{
    CString Text;

    switch (Type)
    {
         case FRMCHAR :
             Text.Format ("%s", VarDB);
             break;
         case FRMSHORT :
             Text.Format (Picture, *(short *) VarDB);
             break;
         case FRMLONG :
             Text.Format (Picture, *(long *) VarDB);
             break;
         case FRMDOUBLE :
             Text.Format (Picture, *(double *) VarDB);
             break;
         case FRMDATE :
             Text = DateToString ((DATE_STRUCT *) VarDB);
             break;
         case FRMBOOL :
             Text.Format ("%hd", VarDB);
             break;
         case FRMCHARBOOL :
             Text.Format ("%s", VarDB);
             break;
    }
    Str = Text;
    return Str;
}

void CForm::FromString (CString& Text)
{
    char *txt;

    txt = Text.GetBuffer (256);
    if (Type == FRMBOOL)
    {
        *(BOOL *) VarDB = atoi (txt);
        return;
    }
    else if (Type == FRMBOOL)
    {
        strcpy ((char *) VarDB, txt);
        return;
    }
    switch (Type)
    {
         case FRMCHAR :
             strcpy ((char *) VarDB, txt);
             break;
         case FRMSHORT :
             *(short *) VarDB = atoi (txt);
             break;
         case FRMLONG :
             *(long *) VarDB = atol (txt);
             break;
         case FRMDOUBLE :
             *(double *) VarDB = (double) atof (txt);
             break;
         case FRMDATE :
             StringToDate (txt, (DATE_STRUCT *) VarDB);
             break;
    }
}


void CForm::Enable (CWnd *cWnd, BOOL b)
{

	CWnd *Item = cWnd->GetDlgItem (Id);
    if (Item != NULL)
    {
        Item->EnableWindow (b);
    }
}

CString& CForm::DateToString (DATE_STRUCT * Date)
{
    CString Datum;

    if (Date->year == 0)
    {
        Datum = "01.01.1900";
    }
    else
    {
        Datum.Format ("%02hd.%02hd.%04hd", Date->day,
                                           Date->month,
                                           Date->year);
    }
    return Datum;
}

void CForm::StringToDate (char *Datum, DATE_STRUCT * Date)
{
    char *day;
    char *month;
    char *year;

    Date->day = 1;
    Date->month = 1;
    Date->year = 1900;
    if ((day = strtok (Datum, ".")) == NULL)
    {
        return;
    }
    if ((month = strtok (NULL, ".")) == NULL)
    {
        return;
    }
    if ((year = strtok (NULL, ".")) == NULL)
    {
        return;
    }
    Date->day   = atoi (day);
    Date->month = atoi (month);
    Date->year  = atoi (year);
}


void CForm::TestDate (void)
{
    if (Type != FRMDATE)
    {
        return;
    }

    DATE_STRUCT *Date = (DATE_STRUCT *) VarDB;
    if (Date->year == 0)
    {
        Date->day = 1;
        Date->month = 1;
        Date->year = 1900;
    }
}


void CForm::SetListboxType (int ListboxType)
{
     this->ListboxType = ListboxType;
}

int CForm::GetListboxType (void)
{
     return ListboxType;
}

void CForm::SetDbItem (char *DbItem)
{
     this->DbItem = DbItem;
}

char * CForm::GetDbItem (void)
{
    return DbItem;
}


CListForm::CListForm (int Id, void *VarDB, int Type, int VarLen, int ListboxType) :
           CForm (Id, VarDB, Type, VarLen)
{
     this->ListboxType = ListboxType;
     this->DbItem = NULL;
     lb = NULL;
     cb = NULL;
}

CListForm::CListForm (int Id, void *VarDB, int Type, char *Picture, int ListboxType) :
           CForm (Id, VarDB, Type, Picture)
{
     this->ListboxType = ListboxType;
     this->DbItem = NULL;
     lb = NULL;
     cb = NULL;
}

CListForm::CListForm (int Id, void *VarDB, int Type, int VarLen, int ListboxType, char *DbItem) :
           CForm (Id, VarDB, Type, Picture)
{
     this->ListboxType = ListboxType;
     this->DbItem = DbItem;
     lb = NULL;
     cb = NULL;
}


CListForm::CListForm (int Id, void *VarDB, int Type, char *Picture, int ListboxType, char *DbItem) :
           CForm (Id, VarDB, Type, Picture)
{
     this->ListboxType = ListboxType;
     this->DbItem = DbItem;
     lb = NULL;
     cb = NULL;
}


void CListForm::FillListBox (CWnd *Dlg, char *Item)
{
      if (ListboxType == 0)
      {
          return;
      }

      if (ListboxType == LISTBOX)
      {
             CListBox *lb = (CListBox *) Dlg->GetDlgItem (Id);
             if (lb == NULL) return;
             lb->AddString (Item);
      }
      else if (ListboxType == COMBOBOX ||
               ListboxType == COMBOLIST)
      {
             CComboBox *cb = (CComboBox *) Dlg->GetDlgItem (Id);
             if (cb == NULL) return;
             int ret = cb->AddString (Item);
      }
}


void CListForm::FillListBox (CWnd *Dlg, char **Item)
{
      if (ListboxType == 0) return;

      if (ListboxType == LISTBOX)
      {
             lb = (CListBox *) Dlg->GetDlgItem (Id);
             if (lb == NULL) return;
      }
      else if (ListboxType == COMBOBOX ||
               ListboxType == COMBOLIST)
      {
             cb = (CComboBox *) Dlg->GetDlgItem (Id);
             if (cb == NULL) return;
      }

      for (int i; Item[i] != NULL; i ++)
      {
          if (lb != NULL)
          {
              lb->AddString (Item[i]);
          }
          else if (cb != NULL)
          {
              cb->AddString (Item[i]);
          }
      }
}


void CListForm::FillPtBox (void *DbObject, CWnd *Dlg, char *Item)
{
      CString Text;
      char ptwert [5];
      char ptbezk [10];

      if (ListboxType == 0) return;

      if (ListboxType == LISTBOX)
      {
             lb = (CListBox *) Dlg->GetDlgItem (Id);
             if (lb == NULL) return;
      }
      else if (ListboxType == COMBOBOX ||
               ListboxType == COMBOLIST)
      {
             cb = (CComboBox *) Dlg->GetDlgItem (Id);
             if (cb == NULL) return;
      }

      DB_CLASS *Ptab = (DB_CLASS *) DbObject;

      Ptab->sqlout ((char *) ptbezk, 0, 9);
      Ptab->sqlin ((char *) Item, 0, 19);
      int cursor = Ptab->sqlcursor ("select ptbezk, ptlfnr from ptabn "
                                   "where ptitem = ? "
                                   "order by ptlfnr");
      while (Ptab->sqlfetch (cursor) == 0)
      {
          Text.Format ("%s %s", ptwert, ptbezk); 
          if (lb != NULL)
          {
              lb->InsertString (-1, Text.GetBuffer (256));
          }
          else if (cb != NULL)
          {
              cb->InsertString (-1, Text.GetBuffer (256));
          }
      }
      Ptab->sqlclose (cursor);
}

void CListForm::FillPtBox (void *DbObject, CWnd *Dlg)
{
      CString Text;
      char ptwert [5];
      char ptbezk [10];
      if (ListboxType == 0) return;
      if (ListboxType == LISTBOX)
      {
             lb = (CListBox *) Dlg->GetDlgItem (Id);
             if (lb == NULL) return;
      }
      else if (ListboxType == COMBOBOX ||
               ListboxType == COMBOLIST)
      {
             cb = (CComboBox *) Dlg->GetDlgItem (Id);
             if (cb == NULL) return;
      }
      if (DbItem == NULL) return;

      DB_CLASS *Ptab = (DB_CLASS *) DbObject;

      Ptab->sqlout ((char *) ptwert, 0, 4);
      Ptab->sqlout ((char *) ptbezk, 0, 9);
      Ptab->sqlin ((char *) DbItem, 0, 19);
      int cursor = Ptab->sqlcursor ("select ptwert, ptbezk, ptlfnr from ptabn "
                                   "where ptitem = ? "
                                   "order by ptlfnr");
      while (Ptab->sqlfetch (cursor) == 0)
      {
          Text.Format ("%s %s", ptwert, ptbezk); 
          if (lb != NULL)
          {
              lb->InsertString (-1, Text.GetBuffer (256));
          }
          else if (cb != NULL)
          {
              cb->InsertString (-1, Text.GetBuffer (256));
          }
      }
      Ptab->sqlclose (cursor);
}


void CListForm::FillPtBoxLong (void *DbObject, CWnd *Dlg, char *Item)
{
      CString Text;
      char ptwert [5];
      char ptbez [37];

      if (ListboxType == 0) return;
      if (ListboxType == LISTBOX)
      {
             lb = (CListBox *) Dlg->GetDlgItem (Id);
             if (lb == NULL) return;
      }
      else if (ListboxType == COMBOBOX ||
               ListboxType == COMBOLIST)
      {
             cb = (CComboBox *) Dlg->GetDlgItem (Id);
             if (cb == NULL) return;
      }

      DB_CLASS *Ptab = (DB_CLASS *) DbObject;

      Ptab->sqlout ((char *) ptwert, 0, 4);
      Ptab->sqlout ((char *) ptbez, 0, 36);
      Ptab->sqlin ((char *) Item, 0, 19);
      int cursor = Ptab->sqlcursor ("select ptwert, ptbez, ptlfnr from ptabn "
                                   "where ptitem = ? "
                                   "order by ptlfnr");
      while (Ptab->sqlfetch (cursor) == 0)
      {
          Text.Format ("%s %s", ptwert, ptbez); 
          if (lb != NULL)
          {
              lb->InsertString (-1, Text.GetBuffer (256));
          }
          else if (cb != NULL)
          {
              cb->InsertString (-1, Text.GetBuffer (256));
          }
      }
      Ptab->sqlclose (cursor);
}


void CListForm::FillPtBoxLong (void *DbObject, CWnd *Dlg)
{
      CString Text;
      char ptwert [5];
      char ptbez [37];
      if (ListboxType == 0) return;
      if (ListboxType == LISTBOX)
      {
             lb = (CListBox *) Dlg->GetDlgItem (Id);
             if (lb == NULL) return;
      }
      else if (ListboxType == COMBOBOX ||
               ListboxType == COMBOLIST)
      {
             cb = (CComboBox *) Dlg->GetDlgItem (Id);
      }

      if (DbItem == NULL) return;

      DB_CLASS *Ptab = (DB_CLASS *) DbObject;


      Ptab->sqlout ((char *) ptwert, 0, 4);
      Ptab->sqlout ((char *) ptbez, 0, 36);
      Ptab->sqlin ((char *) DbItem, 0, 19);
      int cursor = Ptab->sqlcursor ("select ptwert,ptbez, ptlfnr from ptabn "
                                   "where ptitem = ? "
                                   "order by ptlfnr");
      while (Ptab->sqlfetch (cursor) == 0)
      {
          Text.Format ("%s %s", ptwert, ptbez); 
          if (lb != NULL)
          {
              lb->InsertString (-1, Text.GetBuffer (256));
          }
          else if (cb != NULL)
          {
              cb->InsertString (-1, Text.GetBuffer (256));
          }
      }
      Ptab->sqlclose (cursor);
}


void CListForm::GetText (CWnd *Dlg, int idx, CString& Text)
{
      if (ListboxType == 0)
      {
          return;
      }

      if (lb != NULL)
      {
             lb->GetText (idx, Text);
      }
      else if (cb != NULL)
      {
             cb->GetLBText (idx, Text);
      }
}


void CListForm::ToScreen (CWnd *Dlg)
{
      CString PtWert;
      CString Text;
      int count;

      if (ListboxType == 0)
      {
          return;
      }

      if (lb != NULL)
      {
             count = lb->GetCount ();
      }
      else if (cb != NULL)
      {
             count = cb->GetCount ();
      }
      else
      {
             return;
      }
      ToString (PtWert);
      PtWert.TrimRight ();
      char *ptwert = PtWert.GetBuffer (4);
      for (int i = 0; i < count; i ++)
      {
             GetText (Dlg, i, Text);
             char *txt = Text.GetBuffer (256);
             txt = strtok (txt, " ");
             Text = txt;
             Text.TrimRight ();
             if (PtWert.Compare (Text) == 0)
             {
                 break;
             }
      }
      if (i == count)
      {
          return;
      }
      if (lb != NULL)
      {
          lb->SetCurSel (i);
      }
      else if (cb != NULL)
      {
          cb->SetCurSel (i);
      }
}

void CListForm::FromScreen (CWnd *Dlg)
{
      char buffer [256];
             
      if (ListboxType == 0)
      {
          CForm::FromScreen (Dlg);
          return;
      }
      else if (ListboxType == COMBOBOX)
      {
          CForm::FromScreen (Dlg);
          return;
      }

      if (lb != NULL)
      {
             int idx = lb->GetCurSel ();
             lb->GetText (idx, buffer);
      }
      else if (cb != NULL)
      {
             int idx = cb->GetCurSel ();
             cb->GetLBText (idx, buffer);
      }
      else
      {
             return;
      }

      char *txt = strtok (buffer, " ");
      if (txt == NULL)
      {
            txt = "";
      }
      CString Text = txt;
      FromString (Text);
}



