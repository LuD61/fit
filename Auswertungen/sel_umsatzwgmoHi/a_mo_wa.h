#ifndef _A_MO_WA_DEF
#define _A_MO_WA_DEF

struct A_BAS { 
   short     me_einh;
   double	a_gew;
};
extern struct A_BAS a_bas, a_bas_null;

struct AUSGABE { 
   short     fil;
   short     wg;
   long     hirarchie1;
   long     hirarchie2;
   long     hirarchie3;
   long     hirarchie4;
   char		h1_bez [17];
   char		h2_bez [17];
   char		h3_bez [17];
   char		h4_bez [17];
   long		kun;
   char		kun_krz1[17];
   char     mo1[10];
   double    me_vk_mo1;
   char     mo2[10];
   double    me_vk_mo2;
   char     mo3[10];
   double    me_vk_mo3;
   char     mo4[10];
   double    me_vk_mo4;
   char     mo5[10];
   double    me_vk_mo5;
   char     mo6[10];
   double    me_vk_mo6;
   char     mo7[10];
   double    me_vk_mo7;
   char     mo8[10];
   double    me_vk_mo8;
   char     mo9[10];
   double    me_vk_mo9;
   char     mo10[10];
   double    me_vk_mo10;
   char     mo11[10];
   double    me_vk_mo11;
   char     mo12[10];
   double    me_vk_mo12;
   char     mo13[10];
   double    me_vk_mo13;
};
extern struct AUSGABE ausgabe, ausgabe_null;

struct A_MO_WA {
   short     delstatus;
   short     mdn;
   short     fil;
   long      kun;
   double    a;
   short     jr;
   short     mo;
   double    me_vk_mo;
   double    ums_vk_mo;
   double    ums_fil_ek_mo;
   /***
   double    me_vk_sa_mo;
   double    ums_vk_sa_mo;
   double    ums_fil_ek_sa_mo;
   double    me_ret_mo;
   double    ums_hk_vkost_mo;
   double    ums_hk_tkost_mo;
   double    ums_sk_vkost_mo;
   double    ums_sk_tkost_mo;
   double    ums_mat_o_b_mo;
   double    ums_ret_mo;
   double    ums_fil_ek_ret_mo;
   double    gn_pkt_gbr_ges;
   short     waehrung;
   double    ums_vk_e;
   double    ums_vk_f;
   double    ums_vk_sa_e;
   double    ums_vk_sa_f;
   double    ums_ret_e;
   double    ums_ret_f;
   double    ums_fil_ek_e;
   double    ums_fil_ek_ret_e;
   double    ums_fil_ek_sa_e;
   double    gn_pkt_gbr_e;
   double    m_kun_mo;
   double    u_kun_mo;
   double    u_kun_fek_mo;
   double    m_kun_ret_mo;
   double    u_kun_ret_mo;
   double    u_kun_fek_ret_mo;
   double    ue_kun_mo;
   double    ue_kun_fek_mo;
   double    ue_kun_ret_mo;
   double    ue_kun_fek_ret_mo;
   double    m_upls_mo;
   double    u_upls_mo;
   double    u_upls_fek_mo;
   double    ue_upls_mo;
   double    ue_upls_fek_mo;
   double    m_umin_mo;
   double    u_umin_mo;
   double    u_umin_fek_mo;
   double    ue_umin_mo;
   double    ue_umin_fek_mo;
   double    m_we_mo;
   double    u_we_ek_mo;
   double    u_we_fek_mo;
   double    u_we_fvk_mo;
   double    m_we_ret_mo;
   double    u_we_ret_ek_mo;
   double    u_we_ret_fek_mo;
   double    u_we_ret_fvk_mo;
   double    ue_we_ek_mo;
   double    ue_we_fek_mo;
   double    ue_we_fvk_mo;
   double    ue_we_ret_ek_mo;
   double    ue_we_ret_fek_mo;
   double    ue_we_ret_fvk_mo;
   double    me1_vk_mo;
   double    me2_vk_mo;
   double    me3_vk_mo;
   ********/
};
extern struct A_MO_WA a_mo_wa, a_mo_wa_null;

class A_MO_WA_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
               void prepareHi_1 (void);
               void prepareHi_2 (void);
               void prepareHi_3 (void);
               void prepareHi_4 (void);
               void prepareHi_5 (void);
       public :
               int dbcount (void);
               int leseDS (int);
               int openDS (void);
               A_MO_WA_CLASS () : DB_CLASS ()
               {
               }
};
#endif
