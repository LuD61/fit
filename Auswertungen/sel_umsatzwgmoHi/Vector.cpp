#include "stdafx.h"
#include "Vector.h"

CVector::CVector ()
{
     Arr = NULL;
     anz = 0;
     pos = 0;
}

CVector::CVector (void **Object)
{
      SetObject (Object);
}


CVector::~CVector ()
{
     if (Arr != NULL)
     {
         delete Arr;
     }
}

void CVector::SetObject (void **Object)
{
     void **ArrS; 
     if (Arr != NULL)
     {
         delete Arr;
     }
     Arr = Object;
     for (anz = 0; Arr[anz] != NULL; anz ++);
     ArrS = new void *[anz];
     for (int i = 0; i < anz; i ++)
     {
         ArrS[i] = Arr[i];
     }
     Arr = ArrS;
}

void CVector::Add (void *Object)
{
    void **ArrS;

    ArrS = new void * [anz + 1];
    for (int i = 0; i < anz; i ++)
    {
        ArrS[i] = Arr[i];
    }

    void *Ch = Object;
    ArrS[i] = Ch;
    anz ++;
    delete Arr;
    Arr = ArrS;
}


BOOL CVector::Drop (int idx)
{
    void **ArrS;

    if (idx >= anz)
    {
        return FALSE;
    }

    ArrS = new void * [anz - 1];
    for (int i = 0; i < anz; i ++)
    {
        if (i == idx) continue;
        ArrS[i] = Arr[i];
    }

    anz --;
    delete Arr;
    Arr = ArrS;
    return TRUE;
}

void CVector::SetPosition (int pos)
{
    this->pos = pos;
}

void CVector::FirstPosition (void)
{
    SetPosition (0);
}

void *CVector::Get (int idx)
{
    if (idx >= anz)
    {
        return NULL;
    }
    return Arr [idx];
}

void *CVector::GetNext (int pos)
{
    this->pos = pos;
    return GetNext ();
}

void *CVector::GetNext (void)
{
    if (pos >= anz)
    {
        return NULL;
    }
    pos ++;
    return Arr [pos - 1];
}
