//=============================================================================
//
//  Project: List & Label
//           Copyright (c) 1998-2002 combit GmbH, All Rights Reserved
//
//  Authors: combit Software Team
//
//-----------------------------------------------------------------------------
//
//  Module:  LLMFC - List & Label MFC Sample Application
//
//=============================================================================

// llsmpdoc.h : interface of the CLlSampleDoc class

class CLlSampleDoc : public CDocument
{
protected: // create from serialization only
	CLlSampleDoc();
	DECLARE_DYNCREATE(CLlSampleDoc)

// Attributes
public:

// Operations
public:

// Implementation
public:
	virtual ~CLlSampleDoc();
	virtual void Serialize(CArchive& ar);	// overridden for document i/o
#ifdef _DEBUG
	virtual	void AssertValid() const;
	virtual	void Dump(CDumpContext& dc) const;
#endif
protected:
	virtual	BOOL	OnNewDocument();

// Generated message map functions
protected:
	//{{AFX_MSG(CLlSampleDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
