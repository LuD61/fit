#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "dbclass.h"
#include "a_basc.h"

struct A_BAS _a_bas, _a_bas_null;

void A_BAS_CL::prepare (void)
{
            char *sqltext;

            sqlin ((double *)   &_a_bas.a,  SQLDOUBLE, 0);
    out_quest ((char *) &_a_bas.a,3,0);
    out_quest ((char *) &_a_bas.mdn,1,0);
    out_quest ((char *) &_a_bas.fil,1,0);
    out_quest ((char *) _a_bas.a_bz1,0,25);
    out_quest ((char *) _a_bas.a_bz2,0,25);
    out_quest ((char *) &_a_bas.a_gew,3,0);
    out_quest ((char *) &_a_bas.a_typ,1,0);
    out_quest ((char *) &_a_bas.a_typ2,1,0);
    out_quest ((char *) &_a_bas.abt,1,0);
    out_quest ((char *) &_a_bas.ag,2,0);
    out_quest ((char *) _a_bas.best_auto,0,2);
    out_quest ((char *) _a_bas.bsd_kz,0,2);
    out_quest ((char *) _a_bas.cp_aufschl,0,2);
    out_quest ((char *) &_a_bas.delstatus,1,0);
    out_quest ((char *) &_a_bas.dr_folge,1,0);
    out_quest ((char *) &_a_bas.erl_kto,2,0);
    out_quest ((char *) _a_bas.hbk_kz,0,2);
    out_quest ((char *) &_a_bas.hbk_ztr,1,0);
    out_quest ((char *) _a_bas.hnd_gew,0,2);
    out_quest ((char *) &_a_bas.hwg,1,0);
    out_quest ((char *) _a_bas.kost_kz,0,3);
    out_quest ((char *) &_a_bas.me_einh,1,0);
    out_quest ((char *) _a_bas.modif,0,2);
    out_quest ((char *) &_a_bas.mwst,1,0);
    out_quest ((char *) &_a_bas.plak_div,1,0);
    out_quest ((char *) _a_bas.stk_lst_kz,0,2);
    out_quest ((char *) &_a_bas.sw,3,0);
    out_quest ((char *) &_a_bas.teil_smt,1,0);
    out_quest ((char *) &_a_bas.we_kto,2,0);
    out_quest ((char *) &_a_bas.wg,1,0);
    out_quest ((char *) &_a_bas.zu_stoff,1,0);
    out_quest ((char *) &_a_bas.akv,2,0);
    out_quest ((char *) &_a_bas.bearb,2,0);
    out_quest ((char *) _a_bas.pers_nam,0,9);
    out_quest ((char *) &_a_bas.prod_zeit,3,0);
    out_quest ((char *) _a_bas.pers_rab_kz,0,2);
    out_quest ((char *) &_a_bas.gn_pkt_gbr,3,0);
    out_quest ((char *) &_a_bas.kost_st,2,0);
    out_quest ((char *) _a_bas.sw_pr_kz,0,2);
    out_quest ((char *) &_a_bas.kost_tr,2,0);
    out_quest ((char *) &_a_bas.a_grund,3,0);
    out_quest ((char *) &_a_bas.kost_st2,2,0);
    out_quest ((char *) &_a_bas.we_kto2,2,0);
    out_quest ((char *) &_a_bas.charg_hand,2,0);
    out_quest ((char *) &_a_bas.intra_stat,2,0);
    out_quest ((char *) _a_bas.qual_kng,0,5);
    out_quest ((char *) _a_bas.a_bz3,0,25);
    out_quest ((char *) &_a_bas.lief_einh,1,0);
    out_quest ((char *) &_a_bas.inh_lief,3,0);
    out_quest ((char *) &_a_bas.erl_kto_1,2,0);
    out_quest ((char *) &_a_bas.erl_kto_2,2,0);
    out_quest ((char *) &_a_bas.erl_kto_3,2,0);
    out_quest ((char *) &_a_bas.we_kto_1,2,0);
    out_quest ((char *) &_a_bas.we_kto_2,2,0);
    out_quest ((char *) &_a_bas.we_kto_3,2,0);
    out_quest ((char *) _a_bas.skto_f,0,2);
    out_quest ((char *) &_a_bas.sk_vollk,3,0);
    out_quest ((char *) &_a_bas.a_ersatz,3,0);
    out_quest ((char *) &_a_bas.a_ers_kz,1,0);
    out_quest ((char *) &_a_bas.me_einh_abverk,1,0);
    out_quest ((char *) &_a_bas.inh_abverk,3,0);
    out_quest ((char *) _a_bas.hnd_gew_abverk,0,2);
            cursor = sqlcursor ("select a_bas.a,  a_bas.mdn,  "
"a_bas.fil,  a_bas.a_bz1,  a_bas.a_bz2,  a_bas.a_gew,  a_bas.a_typ,  "
"a_bas.a_typ2,  a_bas.abt,  a_bas.ag,  a_bas.best_auto,  a_bas.bsd_kz,  "
"a_bas.cp_aufschl,  a_bas.delstatus,  a_bas.dr_folge,  a_bas.erl_kto,  "
"a_bas.hbk_kz,  a_bas.hbk_ztr,  a_bas.hnd_gew,  a_bas.hwg,  a_bas.kost_kz,  "
"a_bas.me_einh,  a_bas.modif,  a_bas.mwst,  a_bas.plak_div,  "
"a_bas.stk_lst_kz,  a_bas.sw,  a_bas.teil_smt,  a_bas.we_kto,  a_bas.wg,  "
"a_bas.zu_stoff,  a_bas.akv,  a_bas.bearb,  a_bas.pers_nam,  "
"a_bas.prod_zeit,  a_bas.pers_rab_kz,  a_bas.gn_pkt_gbr,  "
"a_bas.kost_st,  a_bas.sw_pr_kz,  a_bas.kost_tr,  a_bas.a_grund,  "
"a_bas.kost_st2,  a_bas.we_kto2,  a_bas.charg_hand,  a_bas.intra_stat,  "
"a_bas.qual_kng,  a_bas.a_bz3,  a_bas.lief_einh,  a_bas.inh_lief,  "
"a_bas.erl_kto_1,  a_bas.erl_kto_2,  a_bas.erl_kto_3,  a_bas.we_kto_1,  "
"a_bas.we_kto_2,  a_bas.we_kto_3,  a_bas.skto_f,  a_bas.sk_vollk,  "
"a_bas.a_ersatz,  a_bas.a_ers_kz,  a_bas.me_einh_abverk,  "
"a_bas.inh_abverk,  a_bas.hnd_gew_abverk from a_bas "

#line 19 "a_basc.rpp"
                                  "where a = ?");
    ins_quest ((char *) &_a_bas.a,3,0);
    ins_quest ((char *) &_a_bas.mdn,1,0);
    ins_quest ((char *) &_a_bas.fil,1,0);
    ins_quest ((char *) _a_bas.a_bz1,0,25);
    ins_quest ((char *) _a_bas.a_bz2,0,25);
    ins_quest ((char *) &_a_bas.a_gew,3,0);
    ins_quest ((char *) &_a_bas.a_typ,1,0);
    ins_quest ((char *) &_a_bas.a_typ2,1,0);
    ins_quest ((char *) &_a_bas.abt,1,0);
    ins_quest ((char *) &_a_bas.ag,2,0);
    ins_quest ((char *) _a_bas.best_auto,0,2);
    ins_quest ((char *) _a_bas.bsd_kz,0,2);
    ins_quest ((char *) _a_bas.cp_aufschl,0,2);
    ins_quest ((char *) &_a_bas.delstatus,1,0);
    ins_quest ((char *) &_a_bas.dr_folge,1,0);
    ins_quest ((char *) &_a_bas.erl_kto,2,0);
    ins_quest ((char *) _a_bas.hbk_kz,0,2);
    ins_quest ((char *) &_a_bas.hbk_ztr,1,0);
    ins_quest ((char *) _a_bas.hnd_gew,0,2);
    ins_quest ((char *) &_a_bas.hwg,1,0);
    ins_quest ((char *) _a_bas.kost_kz,0,3);
    ins_quest ((char *) &_a_bas.me_einh,1,0);
    ins_quest ((char *) _a_bas.modif,0,2);
    ins_quest ((char *) &_a_bas.mwst,1,0);
    ins_quest ((char *) &_a_bas.plak_div,1,0);
    ins_quest ((char *) _a_bas.stk_lst_kz,0,2);
    ins_quest ((char *) &_a_bas.sw,3,0);
    ins_quest ((char *) &_a_bas.teil_smt,1,0);
    ins_quest ((char *) &_a_bas.we_kto,2,0);
    ins_quest ((char *) &_a_bas.wg,1,0);
    ins_quest ((char *) &_a_bas.zu_stoff,1,0);
    ins_quest ((char *) &_a_bas.akv,2,0);
    ins_quest ((char *) &_a_bas.bearb,2,0);
    ins_quest ((char *) _a_bas.pers_nam,0,9);
    ins_quest ((char *) &_a_bas.prod_zeit,3,0);
    ins_quest ((char *) _a_bas.pers_rab_kz,0,2);
    ins_quest ((char *) &_a_bas.gn_pkt_gbr,3,0);
    ins_quest ((char *) &_a_bas.kost_st,2,0);
    ins_quest ((char *) _a_bas.sw_pr_kz,0,2);
    ins_quest ((char *) &_a_bas.kost_tr,2,0);
    ins_quest ((char *) &_a_bas.a_grund,3,0);
    ins_quest ((char *) &_a_bas.kost_st2,2,0);
    ins_quest ((char *) &_a_bas.we_kto2,2,0);
    ins_quest ((char *) &_a_bas.charg_hand,2,0);
    ins_quest ((char *) &_a_bas.intra_stat,2,0);
    ins_quest ((char *) _a_bas.qual_kng,0,5);
    ins_quest ((char *) _a_bas.a_bz3,0,25);
    ins_quest ((char *) &_a_bas.lief_einh,1,0);
    ins_quest ((char *) &_a_bas.inh_lief,3,0);
    ins_quest ((char *) &_a_bas.erl_kto_1,2,0);
    ins_quest ((char *) &_a_bas.erl_kto_2,2,0);
    ins_quest ((char *) &_a_bas.erl_kto_3,2,0);
    ins_quest ((char *) &_a_bas.we_kto_1,2,0);
    ins_quest ((char *) &_a_bas.we_kto_2,2,0);
    ins_quest ((char *) &_a_bas.we_kto_3,2,0);
    ins_quest ((char *) _a_bas.skto_f,0,2);
    ins_quest ((char *) &_a_bas.sk_vollk,3,0);
    ins_quest ((char *) &_a_bas.a_ersatz,3,0);
    ins_quest ((char *) &_a_bas.a_ers_kz,1,0);
    ins_quest ((char *) &_a_bas.me_einh_abverk,1,0);
    ins_quest ((char *) &_a_bas.inh_abverk,3,0);
    ins_quest ((char *) _a_bas.hnd_gew_abverk,0,2);
            sqltext = "update a_bas set a_bas.a = ?,  "
"a_bas.mdn = ?,  a_bas.fil = ?,  a_bas.a_bz1 = ?,  a_bas.a_bz2 = ?,  "
"a_bas.a_gew = ?,  a_bas.a_typ = ?,  a_bas.a_typ2 = ?,  a_bas.abt = ?,  "
"a_bas.ag = ?,  a_bas.best_auto = ?,  a_bas.bsd_kz = ?,  "
"a_bas.cp_aufschl = ?,  a_bas.delstatus = ?,  a_bas.dr_folge = ?,  "
"a_bas.erl_kto = ?,  a_bas.hbk_kz = ?,  a_bas.hbk_ztr = ?,  "
"a_bas.hnd_gew = ?,  a_bas.hwg = ?,  a_bas.kost_kz = ?,  "
"a_bas.me_einh = ?,  a_bas.modif = ?,  a_bas.mwst = ?,  "
"a_bas.plak_div = ?,  a_bas.stk_lst_kz = ?,  a_bas.sw = ?,  "
"a_bas.teil_smt = ?,  a_bas.we_kto = ?,  a_bas.wg = ?,  "
"a_bas.zu_stoff = ?,  a_bas.akv = ?,  a_bas.bearb = ?,  "
"a_bas.pers_nam = ?,  a_bas.prod_zeit = ?,  a_bas.pers_rab_kz = ?,  "
"a_bas.gn_pkt_gbr = ?,  a_bas.kost_st = ?,  a_bas.sw_pr_kz = ?,  "
"a_bas.kost_tr = ?,  a_bas.a_grund = ?,  a_bas.kost_st2 = ?,  "
"a_bas.we_kto2 = ?,  a_bas.charg_hand = ?,  a_bas.intra_stat = ?,  "
"a_bas.qual_kng = ?,  a_bas.a_bz3 = ?,  a_bas.lief_einh = ?,  "
"a_bas.inh_lief = ?,  a_bas.erl_kto_1 = ?,  a_bas.erl_kto_2 = ?,  "
"a_bas.erl_kto_3 = ?,  a_bas.we_kto_1 = ?,  a_bas.we_kto_2 = ?,  "
"a_bas.we_kto_3 = ?,  a_bas.skto_f = ?,  a_bas.sk_vollk = ?,  "
"a_bas.a_ersatz = ?,  a_bas.a_ers_kz = ?,  a_bas.me_einh_abverk = ?,  "
"a_bas.inh_abverk = ?,  a_bas.hnd_gew_abverk = ? "

#line 21 "a_basc.rpp"
                                  "where a = ?";
            sqlin ((double *)   &_a_bas.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &_a_bas.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor ("select a from a_bas "
                                  "where a = ?");
            sqlin ((double *)   &_a_bas.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor ("delete from a_bas "
                                  "where a = ?");
    ins_quest ((char *) &_a_bas.a,3,0);
    ins_quest ((char *) &_a_bas.mdn,1,0);
    ins_quest ((char *) &_a_bas.fil,1,0);
    ins_quest ((char *) _a_bas.a_bz1,0,25);
    ins_quest ((char *) _a_bas.a_bz2,0,25);
    ins_quest ((char *) &_a_bas.a_gew,3,0);
    ins_quest ((char *) &_a_bas.a_typ,1,0);
    ins_quest ((char *) &_a_bas.a_typ2,1,0);
    ins_quest ((char *) &_a_bas.abt,1,0);
    ins_quest ((char *) &_a_bas.ag,2,0);
    ins_quest ((char *) _a_bas.best_auto,0,2);
    ins_quest ((char *) _a_bas.bsd_kz,0,2);
    ins_quest ((char *) _a_bas.cp_aufschl,0,2);
    ins_quest ((char *) &_a_bas.delstatus,1,0);
    ins_quest ((char *) &_a_bas.dr_folge,1,0);
    ins_quest ((char *) &_a_bas.erl_kto,2,0);
    ins_quest ((char *) _a_bas.hbk_kz,0,2);
    ins_quest ((char *) &_a_bas.hbk_ztr,1,0);
    ins_quest ((char *) _a_bas.hnd_gew,0,2);
    ins_quest ((char *) &_a_bas.hwg,1,0);
    ins_quest ((char *) _a_bas.kost_kz,0,3);
    ins_quest ((char *) &_a_bas.me_einh,1,0);
    ins_quest ((char *) _a_bas.modif,0,2);
    ins_quest ((char *) &_a_bas.mwst,1,0);
    ins_quest ((char *) &_a_bas.plak_div,1,0);
    ins_quest ((char *) _a_bas.stk_lst_kz,0,2);
    ins_quest ((char *) &_a_bas.sw,3,0);
    ins_quest ((char *) &_a_bas.teil_smt,1,0);
    ins_quest ((char *) &_a_bas.we_kto,2,0);
    ins_quest ((char *) &_a_bas.wg,1,0);
    ins_quest ((char *) &_a_bas.zu_stoff,1,0);
    ins_quest ((char *) &_a_bas.akv,2,0);
    ins_quest ((char *) &_a_bas.bearb,2,0);
    ins_quest ((char *) _a_bas.pers_nam,0,9);
    ins_quest ((char *) &_a_bas.prod_zeit,3,0);
    ins_quest ((char *) _a_bas.pers_rab_kz,0,2);
    ins_quest ((char *) &_a_bas.gn_pkt_gbr,3,0);
    ins_quest ((char *) &_a_bas.kost_st,2,0);
    ins_quest ((char *) _a_bas.sw_pr_kz,0,2);
    ins_quest ((char *) &_a_bas.kost_tr,2,0);
    ins_quest ((char *) &_a_bas.a_grund,3,0);
    ins_quest ((char *) &_a_bas.kost_st2,2,0);
    ins_quest ((char *) &_a_bas.we_kto2,2,0);
    ins_quest ((char *) &_a_bas.charg_hand,2,0);
    ins_quest ((char *) &_a_bas.intra_stat,2,0);
    ins_quest ((char *) _a_bas.qual_kng,0,5);
    ins_quest ((char *) _a_bas.a_bz3,0,25);
    ins_quest ((char *) &_a_bas.lief_einh,1,0);
    ins_quest ((char *) &_a_bas.inh_lief,3,0);
    ins_quest ((char *) &_a_bas.erl_kto_1,2,0);
    ins_quest ((char *) &_a_bas.erl_kto_2,2,0);
    ins_quest ((char *) &_a_bas.erl_kto_3,2,0);
    ins_quest ((char *) &_a_bas.we_kto_1,2,0);
    ins_quest ((char *) &_a_bas.we_kto_2,2,0);
    ins_quest ((char *) &_a_bas.we_kto_3,2,0);
    ins_quest ((char *) _a_bas.skto_f,0,2);
    ins_quest ((char *) &_a_bas.sk_vollk,3,0);
    ins_quest ((char *) &_a_bas.a_ersatz,3,0);
    ins_quest ((char *) &_a_bas.a_ers_kz,1,0);
    ins_quest ((char *) &_a_bas.me_einh_abverk,1,0);
    ins_quest ((char *) &_a_bas.inh_abverk,3,0);
    ins_quest ((char *) _a_bas.hnd_gew_abverk,0,2);
            ins_cursor = sqlcursor ("insert into a_bas (a,  mdn,  "
"fil,  a_bz1,  a_bz2,  a_gew,  a_typ,  a_typ2,  abt,  ag,  best_auto,  bsd_kz,  cp_aufschl,  "
"delstatus,  dr_folge,  erl_kto,  hbk_kz,  hbk_ztr,  hnd_gew,  hwg,  kost_kz,  "
"me_einh,  modif,  mwst,  plak_div,  stk_lst_kz,  sw,  teil_smt,  we_kto,  wg,  zu_stoff,  "
"akv,  bearb,  pers_nam,  prod_zeit,  pers_rab_kz,  gn_pkt_gbr,  kost_st,  "
"sw_pr_kz,  kost_tr,  a_grund,  kost_st2,  we_kto2,  charg_hand,  intra_stat,  "
"qual_kng,  a_bz3,  lief_einh,  inh_lief,  erl_kto_1,  erl_kto_2,  erl_kto_3,  "
"we_kto_1,  we_kto_2,  we_kto_3,  skto_f,  sk_vollk,  a_ersatz,  a_ers_kz,  "
"me_einh_abverk,  inh_abverk,  hnd_gew_abverk) "

#line 32 "a_basc.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 34 "a_basc.rpp"
}
