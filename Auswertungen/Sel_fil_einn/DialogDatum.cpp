// DialogDatum.cpp: Implementierungsdatei
//

#include "strfkt.h"
#include "stdafx.h"
#include "llmfc.h"
#include "DialogDatum.h"
#include "DbClass.h"
#include "dbRange.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
extern DB_CLASS dbClass;
extern DB_RANGE dbRange;
extern short movon ;
extern short mdn ;
extern short jr ;
extern char filbereich[256];
extern int FlgUmsatz;
extern char kunanz[2] ;
extern char kunums[2] ;
extern DATE_STRUCT end_dat;
bool Monatok = FALSE;

CForm *CDialogDatum::_Dialog [] = 
{
   new CForm (IDC_MANDANT, (short *)      &mdn, FRMSHORT,0, "%hd"),
   new CForm (IDC_FILIALE, (char *)      filbereich, FRMCHAR,1, 128),
   new CForm (IDC_JAHR, (short *)      &jr, FRMSHORT,0, "%hd"),
   new CForm (IDC_WOVON, (short *)      &movon, FRMSHORT,0, "%ld"),
   new CForm (IDC_RADIO_ANZ, (CHAR *)      kunanz, FRMCHARBOOL,0, ""),
   new CForm (IDC_RADIO_KUN, (CHAR *)      kunums, FRMCHARBOOL,0, ""),
   NULL,
};

CVector CDialogDatum::Dialog ((void **) _Dialog);


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDialogDatum 


CDialogDatum::CDialogDatum(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogDatum::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}

BOOL CDialogDatum::OnInitDialog()
{
	CDialog::OnInitDialog();
    GetDlgItem (IDC_MANDANT)->SetFocus ();
    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
	strcpy(filbereich,"1 bis 9999");
	sprintf(kunums,"N");
	sprintf(kunanz,"J");

	Read ();

	return TRUE;
}

void CDialogDatum::EnableDialog (BOOL b)
{


    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->Enable (this, b);
    }
	if (b)
	{
	    GetDlgItem (IDC_MANDANT)->SetFocus ();
	    ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
    }
}
void CDialogDatum::ToDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();
    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->ToScreen (this);
    }
}


void CDialogDatum::FromDialog (void)
{
    CForm *Form;

    Dialog.FirstPosition ();

    while ((Form = (CForm *) Dialog.GetNext ()) != NULL)
    {
        Form->FromScreen (this);
    }
}

BOOL CDialogDatum::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
CWnd *cWnd;
if (pMsg->message == WM_KEYDOWN)
{
	switch (pMsg->wParam)
	{
	case VK_F5 :      //so gehts nicht TODO
			cWnd = GetDlgItem (IDCANCEL);
			return CDialog::PreTranslateMessage(pMsg);
	case VK_RETURN : 
	case VK_TAB :
		    cWnd = GetFocus ();
			if (cWnd ==GetDlgItem (IDC_MANDANT))
			{
			
			    Write ();
				if (jr == 0)
				{
					dbClass.opendbase ("bws");
					dbClass.sqlin ((short *) &mdn, SQLSHORT, 0);
					dbClass.sqlout ((DATE_STRUCT *) &end_dat, SQLDATE, 0);
					dbClass.sqlout ((short *) &jr, SQLSHORT, 0);

					dbClass.sqlcomm ("select end_dat,jr  from fil_abrech where mdn = ? and fil = 0 and period_kz = \"M\" "
						"and end_dat <= today + 30 " 
						"order by end_dat desc ");
					Read();
				}

			    GetDlgItem (IDC_FILIALE)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);

				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_FILIALE))
			{
			    Write ();
			    GetDlgItem (IDC_JAHR)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_JAHR))
			{
			    Write ();
			    GetDlgItem (IDC_WOVON)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDC_WOVON))
			{
			    Write ();
			    GetDlgItem (IDOK)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}
			if (cWnd ==GetDlgItem (IDOK))
			{
			    Write ();
			    GetDlgItem (IDC_MANDANT)->SetFocus ();
                ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
				return TRUE;
			}

			if (cWnd ==GetDlgItem (IDCANCEL))
			{
				return CDialog::PreTranslateMessage(pMsg);
			}
			if (cWnd ==GetDlgItem (IDOK))
			{
			    Write ();
				/**
				if (Monatok == FALSE)
                { 
					GetDlgItem (IDC_MANDANT)->SetFocus ();
					((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
					return TRUE;
                }
				**/
				return CDialog::PreTranslateMessage(pMsg);
			}
		    NextDlgCtrl();
		    return TRUE;

	}
}
	return CDialog::PreTranslateMessage(pMsg);
}




BOOL CDialogDatum::Read (void)
{
    ToDialog ();
    EnableDialog (TRUE);
	return TRUE;
	
}

BOOL CDialogDatum::Write (void)
{

    FromDialog ();

return TRUE;
}

void CDialogDatum::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	char ctext[256];
	Write();
	if (strlen(clipped(filbereich)) == 0)
	{
		strcpy(filbereich,"0");
	}
    if (dbRange.PruefeRange(filbereich) == FALSE)
	{
		sprintf (ctext , "Bitte Eingabe �berpr�fen !");
		MessageBox(ctext, "Filiale", MB_OK|MB_ICONSTOP);

	    GetDlgItem (IDC_FILIALE)->SetFocus ();
        ((CEdit *) GetFocus ())->SetSel (0, -1, FALSE);
		return;
	}

	CDialog::OnOK();
}


void CDialogDatum::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogDatum)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogDatum, CDialog)
	//{{AFX_MSG_MAP(CDialogDatum)
	ON_EN_CHANGE(IDC_WOVON, OnChangeWoVon)
	ON_EN_KILLFOCUS(IDC_WGVON, OnKillfocusWgvon)
	ON_EN_KILLFOCUS(IDC_WGBIS, OnKillfocusWgbis)
	ON_BN_CLICKED(IDC_RADIO_ANZ, OnRadioAnz)
	ON_BN_CLICKED(IDC_RADIO_KUN, OnRadioKun)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDialogDatum 

void CDialogDatum::OnChangeWoBis() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnChangeWoVon() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnCloseupCombo1() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnKillfocusWgvon() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnKillfocusWgbis() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void CDialogDatum::OnRadioAnz() 
{
	Write();
	FlgUmsatz = 1;
		if (strcmp(kunanz,"J")== 0 ) 
		{
			FlgUmsatz = 0;
		}
	
}

void CDialogDatum::OnRadioKun() 
{
	Write();
	FlgUmsatz = 0;
		if (strcmp(kunums,"J")== 0 ) 
		{
			FlgUmsatz = 1;
		}


}
