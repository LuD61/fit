#ifndef AFX_DIALOGDATUM_H__59B55868_20E1_4E0C_9961_D9367436DB18__INCLUDED_
#define AFX_DIALOGDATUM_H__59B55868_20E1_4E0C_9961_D9367436DB18__INCLUDED_

// DialogDatum.h : Header-Datei
//
#include "Vector.h"
#include "Form.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDialogDatum 

class CDialogDatum : public CDialog
{
// Konstruktion
public:
	CDialogDatum(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDialogDatum)
	enum { IDD = IDD_DIALOG1 };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDialogDatum)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
    static CForm *_Dialog []; 
    static CVector Dialog;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDialogDatum)
	afx_msg void OnChangeWoBis();
	afx_msg void OnChangeWoVon();
	afx_msg void OnCloseupCombo1();
	virtual void OnOK();
	afx_msg void OnKillfocusWgvon();
	afx_msg void OnKillfocusWgbis();
	afx_msg void OnRadioAnz();
	afx_msg void OnRadioKun();
	//}}AFX_MSG
    void ToDialog (void);
    void FromDialog (void);
	void EnableDialog (BOOL);
	BOOL Read (void);
	BOOL Write (void);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_DIALOGDATUM_H__59B55868_20E1_4E0C_9961_D9367436DB18__INCLUDED_
