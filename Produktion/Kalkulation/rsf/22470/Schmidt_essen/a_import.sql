{ TABLE "fit".a_import row size = 163 number of columns = 11 index size = 18 }
create table "fit".a_import 
  (
    lief_art char(40),


    a decimal(13,0),
    a_bez char(50),
    lief_bez char(40),
    mat_o_b decimal(12,2),
    ag integer,
    a_typ smallint,
    mat integer,
    datum date,
    mdn smallint,
    fil smallint

  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".a_import from "public";

create unique index "fit".i01a_import on "fit".a_import (a);






