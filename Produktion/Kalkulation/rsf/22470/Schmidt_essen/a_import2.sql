create table a_import2 
  (
    inaktiv char(10),
    ptbezk_a_typ char(16),
    stk_lst_kz char(3),
    a_neu decimal(13,0),
    mat_neu decimal (6,0),
    a_bz1 char(24),
    a_bz2 char(24),
    a_bz3 char(24),
    ptbezk_me_einh char(16),
    a_gew decimal(8,3),
    abt decimal (6,0),
    ag decimal (6,0),
    bsd_kz char(3),
    ptbezk_mwst char(16),
    a_varb_befe decimal(4,1),
    a_varb_befe_i_fett decimal(4,1),
    a_varb_eiw_ges decimal(4,1),
    a_varb_fett decimal(5,2),
    a_varb_fett_eiw decimal(5,2),
    a_varb_h2o decimal(4,1),
    a_varb_quid_nr decimal (6,0),
    a decimal(13,0),
    mat decimal (6,0),
    a_typ_alt decimal (6,0),
    ag_alt decimal (6,0),
    _a_typ smallint,
    _me_einh smallint,
    _mwst smallint,
    _datum date,
    _info char(60),
    _wg integer,
    _hwg integer,
    _status smallint
    
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on a_import2 from "public";

create unique index i01a_import2 on a_import2 (a);




