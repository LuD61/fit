#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "schlre_vorkost.h"

struct SCHLRE_VORKOST schlre_vorkost, schlre_vorksost_null;

void SCHLRE_VORKOST_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &schlre_vorkost.mdn, 1, 0);
            ins_quest ((char *) &schlre_vorkost.rech_nr, 2, 0);
            ins_quest ((char *) &schlre_vorkost.ein_nr, 2, 0);
            ins_quest ((char *) &schlre_vorkost.kost_art, 1, 0);
    out_quest ((char *) &schlre_vorkost.mdn,1,0);
    out_quest ((char *) &schlre_vorkost.rech_nr,2,0);
    out_quest ((char *) &schlre_vorkost.kost_art,1,0);
    out_quest ((char *) schlre_vorkost.kost_art_bz,0,25);
    out_quest ((char *) &schlre_vorkost.rech_basis,1,0);
    out_quest ((char *) &schlre_vorkost.rech_wert,3,0);
    out_quest ((char *) &schlre_vorkost.wert,3,0);
    out_quest ((char *) &schlre_vorkost.anzahl,2,0);
    out_quest ((char *) &schlre_vorkost.mwst_schl,1,0);
    out_quest ((char *) &schlre_vorkost.ein_nr,2,0);
            cursor = prepare_sql ("select schlre_vorkost.mdn,  "
"schlre_vorkost.rech_nr,  schlre_vorkost.kost_art,  "
"schlre_vorkost.kost_art_bz,  schlre_vorkost.rech_basis,  "
"schlre_vorkost.rech_wert,  schlre_vorkost.wert,  "
"schlre_vorkost.anzahl,  schlre_vorkost.mwst_schl,  "
"schlre_vorkost.ein_nr from schlre_vorkost "

#line 26 "schlre_vorkost.rpp"
                                  "where mdn = ? "
                                  "and   rech_nr = ? "
                                  "and   ein_nr = ? "
                                  "and   kost_art = ? ");

    ins_quest ((char *) &schlre_vorkost.mdn,1,0);
    ins_quest ((char *) &schlre_vorkost.rech_nr,2,0);
    ins_quest ((char *) &schlre_vorkost.kost_art,1,0);
    ins_quest ((char *) schlre_vorkost.kost_art_bz,0,25);
    ins_quest ((char *) &schlre_vorkost.rech_basis,1,0);
    ins_quest ((char *) &schlre_vorkost.rech_wert,3,0);
    ins_quest ((char *) &schlre_vorkost.wert,3,0);
    ins_quest ((char *) &schlre_vorkost.anzahl,2,0);
    ins_quest ((char *) &schlre_vorkost.mwst_schl,1,0);
    ins_quest ((char *) &schlre_vorkost.ein_nr,2,0);
            sqltext = "update we_kopf set "
"schlre_vorkost.mdn = ?,  schlre_vorkost.rech_nr = ?,  "
"schlre_vorkost.kost_art = ?,  schlre_vorkost.kost_art_bz = ?,  "
"schlre_vorkost.rech_basis = ?,  schlre_vorkost.rech_wert = ?,  "
"schlre_vorkost.wert = ?,  schlre_vorkost.anzahl = ?,  "
"schlre_vorkost.mwst_schl = ?,  schlre_vorkost.ein_nr = ? "

#line 32 "schlre_vorkost.rpp"
                                  "where mdn = ? "
                                  "and   rech_nr = ? "
                                  "and   ein_nr = ? "
                                  "and   kost_art = ? ";

            ins_quest ((char *) &schlre_vorkost.mdn, 1, 0);
            ins_quest ((char *) &schlre_vorkost.rech_nr, 2, 0);
            ins_quest ((char *) &schlre_vorkost.ein_nr, 2, 0);
            ins_quest ((char *) &schlre_vorkost.kost_art, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &schlre_vorkost.mdn, 1, 0);
            ins_quest ((char *) &schlre_vorkost.rech_nr, 2, 0);
            ins_quest ((char *) &schlre_vorkost.ein_nr, 2, 0);
            ins_quest ((char *) &schlre_vorkost.kost_art, 1, 0);
            test_upd_cursor = prepare_sql ("select mdn from schlre_vorkost "
                                  "where mdn = ? "
                                  "and   rech_nr = ? "
                                  "and   ein_nr = ? "
                                  "and   kost_art = ? ");
            ins_quest ((char *) &schlre_vorkost.mdn, 1, 0);
            ins_quest ((char *) &schlre_vorkost.rech_nr, 2, 0);
            ins_quest ((char *) &schlre_vorkost.ein_nr, 2, 0);
            ins_quest ((char *) &schlre_vorkost.kost_art, 1, 0);
            del_cursor = prepare_sql ("delete from schlre_vorkost "
                                  "where mdn = ? "
                                  "and   rech_nr = ? "
                                  "and   ein_nr = ? "
                                  "and   kost_art = ? ");
    ins_quest ((char *) &schlre_vorkost.mdn,1,0);
    ins_quest ((char *) &schlre_vorkost.rech_nr,2,0);
    ins_quest ((char *) &schlre_vorkost.kost_art,1,0);
    ins_quest ((char *) schlre_vorkost.kost_art_bz,0,25);
    ins_quest ((char *) &schlre_vorkost.rech_basis,1,0);
    ins_quest ((char *) &schlre_vorkost.rech_wert,3,0);
    ins_quest ((char *) &schlre_vorkost.wert,3,0);
    ins_quest ((char *) &schlre_vorkost.anzahl,2,0);
    ins_quest ((char *) &schlre_vorkost.mwst_schl,1,0);
    ins_quest ((char *) &schlre_vorkost.ein_nr,2,0);
            ins_cursor = prepare_sql ("insert into schlre_vorkost ("
"mdn,  rech_nr,  kost_art,  kost_art_bz,  rech_basis,  rech_wert,  wert,  anzahl,  "
"mwst_schl,  ein_nr) "

#line 62 "schlre_vorkost.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?)"); 

#line 64 "schlre_vorkost.rpp"
}
int SCHLRE_VORKOST_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

