#ifndef _SCHLRE_K_DEF
#define _SCHLRE_K_DEF

#include "dbclass.h"

struct SCHLRE_K {
   short     mdn;
   long      rech_nr;
   short     stat;
   long      ein_nr;
   long      part_nr;
   long      text_nr;
   long      rech_dat;
   long      lief_dat;
   long      lief_s;
   long      kreditor;
   long      regul;
   short     zahl_kond;
   long      eink;
   short     schl_art;
   double    basispr;
   double    netto_betr;
   double    gesamt_betr;
   short     mwst;
   double    mwst_wert;
   short     linie;
   short     schein_mwst;
   double    verrechnet;
   double    abzug_proz;
   long      kun;
};
extern struct SCHLRE_K schlre_k, schlre_k_null;

#line 7 "schlre_k.rh"

class SCHLRE_K_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               SCHLRE_K_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
