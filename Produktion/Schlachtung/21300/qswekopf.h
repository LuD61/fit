#ifndef _QSWEKOPF_DEF
#define _QSWEKOPF_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct QSWEKOPF {
   short     mdn;
   short     fil;
   long      lfd;
   char      lief_rech_nr[17];
   char      lief[17];
   char      txt[61];
   short     wrt;
   long      dat;
   char      pers_nam[9];
};
extern struct QSWEKOPF qswekopf, qswekopf_null;

#line 10 "qswekopf.rh"

class QSWEKOPF_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               QSWEKOPF_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
