#ifndef _SCHLRE_P_DEF
#define _SCHLRE_P_DEF

#include "dbclass.h"

struct SCHLRE_P {
   short     mdn;
   long      ein_nr;
   long      part_nr;
   double    a;
   char      a_krz_bz[13];
   double    netto_gew;
   short     anzahl;
   char      hdkl[7];
   double    mfa;
   double    bonus_pr;
   double    bonus_wert;
   double    rab_pr;
   double    rab_wert;
   double    art_sum;
   long      vorkost;
   double    grund_pr;
   double    vorkost_wert;
   char      tierkennung[21];
   char      tierkenn[17];
   char      fettstufe[3];
   long      schlacht_nr;
   short     linie;
   long      datum;
   char      stat[2];
   double    leb_pr;
   short     vork_stat;
   char      abrech_kz[3];
   double    gew_leb;
   char      freibank[4];
   long      erzeuger;
   char      bnp[2];
   long      ag;
   double    vorkost_wert1;
   double    vorkost_wert6;
   double    vorkost_wertl;
   double    ges_pr;
   short     anz_bu;
   short     anz_unt;
   short     anz_leber;
   short     anz_freibank;
};
extern struct SCHLRE_P schlre_p, schlre_p_null;

#line 7 "schlre_p.rh"

class SCHLRE_P_CLASS : public DB_CLASS
{
       private :
               int cursor_o;
               void prepare (void);
               void prepare_o (char *);
       public :
               SCHLRE_P_CLASS () : DB_CLASS (), cursor_o (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_o (char *);
               int dbread_o (void);
               int dbclose_o (void);
               int dbupdate (void);
};
#endif
