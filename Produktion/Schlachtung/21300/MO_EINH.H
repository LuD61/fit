struct KE
{
          short me_einh_kun;
          char  me_einh_kun_bez [20];
          double inh;
          short me_einh_bas;
          char  me_einh_bas_bez [20];
};

typedef struct KE KEINHEIT;


class EINH_CLASS
{
           private :
                int dsqlstatus;
                int AufEinh;
           public :
                EINH_CLASS () : AufEinh (1)
                {
                }
                int GetAufEinh (void)
                {
                    return AufEinh;
                }
                void SetAufEinh (int einh)
                {
                    AufEinh = einh;
                }
                void AktAufEinh (short, short, long, double, int);
                double GetInh (int, double);
                void FillKmb (KEINHEIT *);
                int  ReadKmb (void);
                void GetKunEinh (short, short, long, double, KEINHEIT *);
                void GetBasEinh (double, KEINHEIT *);
                void GetKunEinhBas (double, KEINHEIT *);
};
