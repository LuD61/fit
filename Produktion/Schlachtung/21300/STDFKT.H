int DCcopy (LPSTR, LPSTR);
int WaitConsoleExec (LPSTR, DWORD);
int ConsoleExec (LPSTR, DWORD);
int ProcExec (LPSTR, WORD, int, int, int, int);
DWORD ProcWaitExec (LPSTR prog, WORD, int, int, int, int);
DWORD ProcWaitExecEx (LPSTR prog, WORD, int, int, int, int);
int CreateBatch (char *format, ...);
int AppendBatch (char *format, ...);
int RunBatch (char *format, ...);
int RunBatchPause (char *format, ...);

