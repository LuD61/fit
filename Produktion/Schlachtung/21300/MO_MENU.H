struct MENUE {
   short     anz_menue;
   short     fil;
   char      komm0[51];
   char      komm1[51];
   char      komm2[51];
   char      komm3[51];
   char      komm4[51];
   char      komm5[51];
   char      komm6[51];
   char      komm7[51];
   char      komm8[51];
   char      komm9[51];
   short     mdn;
   char      menue0[6];
   char      menue1[6];
   char      menue2[6];
   char      menue3[6];
   char      menue4[6];
   char      menue5[6];
   char      menue6[6];
   char      menue7[6];
   char      menue8[6];
   char      menue9[6];
   char      pers[13];
   char      prog[6];
   char      zei0[28];
   char      zei1[28];
   char      zei2[28];
   char      zei3[28];
   char      zei4[28];
   char      zei5[28];
   char      zei6[28];
   char      zei7[28];
   char      zei8[28];
   char      zei9[28];
};

struct PROG_KOPF
{
   char      prog[6];
   char      prog_titel[25];
   char      prog_typ[2];
   char      prog_komm[51];
   char      prog_vers[7];
   char      p_name[33];
   char      mdnk[2];
   char      filk[2];
   char      benk[2];
};

struct SYS_BEN {
   short     berecht;
   char      dir_spr_vor[2];
   char      dir_spr_zur[2];
   short     fil;
   short     mdn;
   char      pers[13];
   char      pers_nam[9];
   char      pers_passwd[13];
   long      zahl;
};

struct SYS_INST {
   char      auf_nr[9];
   long      dat;
   short     fil;
   char      kun_nam[31];
   short     mdn;
   short     monitor;
   short     rechnertyp;
   char      unix[13];
   char      usv[2];
   char      projekt[13];
};


extern struct MENUE menue;
extern struct PROG_KOPF  prog_kopf;
extern struct SYS_BEN    sys_ben;
extern struct SYS_INST   sys_inst;

class MENUE_CLASS
{
        private :
             short menue_cursor;
             short prog_cursor;
             short sys_ben_cursor;
             short sys_ben_cursor_pers;
        public :
             MENUE_CLASS (void)
             {
                       menue_cursor   = -1;
                       prog_cursor    = -1;
                       sys_ben_cursor = -1;
                       sys_ben_cursor_pers = -1;
             }
             int LeseMenue (void);
             int Leseprog_kopf (char *);
             int Lesesys_ben (char *, char *);
             int Lesesys_ben_pers (char *);
             void SetSysBen (void);
             void SetSysBenPers (void);
             int Lesesys_inst (void);
};
