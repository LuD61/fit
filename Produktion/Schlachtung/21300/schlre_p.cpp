#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "schlre_p.h"

struct SCHLRE_P schlre_p, schlre_p_null;

void SCHLRE_P_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &schlre_p.mdn, 1, 0);
            ins_quest ((char *) &schlre_p.ein_nr, 2, 0);
            ins_quest ((char *) &schlre_p.schlacht_nr, 2, 0);
    out_quest ((char *) &schlre_p.mdn,1,0);
    out_quest ((char *) &schlre_p.ein_nr,2,0);
    out_quest ((char *) &schlre_p.part_nr,2,0);
    out_quest ((char *) &schlre_p.a,3,0);
    out_quest ((char *) schlre_p.a_krz_bz,0,13);
    out_quest ((char *) &schlre_p.netto_gew,3,0);
    out_quest ((char *) &schlre_p.anzahl,1,0);
    out_quest ((char *) schlre_p.hdkl,0,7);
    out_quest ((char *) &schlre_p.mfa,3,0);
    out_quest ((char *) &schlre_p.bonus_pr,3,0);
    out_quest ((char *) &schlre_p.bonus_wert,3,0);
    out_quest ((char *) &schlre_p.rab_pr,3,0);
    out_quest ((char *) &schlre_p.rab_wert,3,0);
    out_quest ((char *) &schlre_p.art_sum,3,0);
    out_quest ((char *) &schlre_p.vorkost,2,0);
    out_quest ((char *) &schlre_p.grund_pr,3,0);
    out_quest ((char *) &schlre_p.vorkost_wert,3,0);
    out_quest ((char *) schlre_p.tierkennung,0,21);
    out_quest ((char *) schlre_p.tierkenn,0,17);
    out_quest ((char *) schlre_p.fettstufe,0,3);
    out_quest ((char *) &schlre_p.schlacht_nr,2,0);
    out_quest ((char *) &schlre_p.linie,1,0);
    out_quest ((char *) &schlre_p.datum,2,0);
    out_quest ((char *) schlre_p.stat,0,2);
    out_quest ((char *) &schlre_p.leb_pr,3,0);
    out_quest ((char *) &schlre_p.vork_stat,1,0);
    out_quest ((char *) schlre_p.abrech_kz,0,3);
    out_quest ((char *) &schlre_p.gew_leb,3,0);
    out_quest ((char *) schlre_p.freibank,0,4);
    out_quest ((char *) &schlre_p.erzeuger,2,0);
    out_quest ((char *) schlre_p.bnp,0,2);
    out_quest ((char *) &schlre_p.ag,2,0);
    out_quest ((char *) &schlre_p.vorkost_wert1,3,0);
    out_quest ((char *) &schlre_p.vorkost_wert6,3,0);
    out_quest ((char *) &schlre_p.vorkost_wertl,3,0);
    out_quest ((char *) &schlre_p.ges_pr,3,0);
    out_quest ((char *) &schlre_p.anz_bu,1,0);
    out_quest ((char *) &schlre_p.anz_unt,1,0);
    out_quest ((char *) &schlre_p.anz_leber,1,0);
    out_quest ((char *) &schlre_p.anz_freibank,1,0);
            cursor = prepare_sql ("select schlre_p.mdn,  "
"schlre_p.ein_nr,  schlre_p.part_nr,  schlre_p.a,  schlre_p.a_krz_bz,  "
"schlre_p.netto_gew,  schlre_p.anzahl,  schlre_p.hdkl,  schlre_p.mfa,  "
"schlre_p.bonus_pr,  schlre_p.bonus_wert,  schlre_p.rab_pr,  "
"schlre_p.rab_wert,  schlre_p.art_sum,  schlre_p.vorkost,  "
"schlre_p.grund_pr,  schlre_p.vorkost_wert,  schlre_p.tierkennung,  "
"schlre_p.tierkenn,  schlre_p.fettstufe,  schlre_p.schlacht_nr,  "
"schlre_p.linie,  schlre_p.datum,  schlre_p.stat,  schlre_p.leb_pr,  "
"schlre_p.vork_stat,  schlre_p.abrech_kz,  schlre_p.gew_leb,  "
"schlre_p.freibank,  schlre_p.erzeuger,  schlre_p.bnp,  schlre_p.ag,  "
"schlre_p.vorkost_wert1,  schlre_p.vorkost_wert6,  "
"schlre_p.vorkost_wertl,  schlre_p.ges_pr,  schlre_p.anz_bu,  "
"schlre_p.anz_unt,  schlre_p.anz_leber,  schlre_p.anz_freibank from schlre_p "

#line 25 "schlre_p.rpp"
                                  "where mdn = ? "
                                  "and   ein_nr = ? "
                                  "and   schlacht_nr = ? ");

    ins_quest ((char *) &schlre_p.mdn,1,0);
    ins_quest ((char *) &schlre_p.ein_nr,2,0);
    ins_quest ((char *) &schlre_p.part_nr,2,0);
    ins_quest ((char *) &schlre_p.a,3,0);
    ins_quest ((char *) schlre_p.a_krz_bz,0,13);
    ins_quest ((char *) &schlre_p.netto_gew,3,0);
    ins_quest ((char *) &schlre_p.anzahl,1,0);
    ins_quest ((char *) schlre_p.hdkl,0,7);
    ins_quest ((char *) &schlre_p.mfa,3,0);
    ins_quest ((char *) &schlre_p.bonus_pr,3,0);
    ins_quest ((char *) &schlre_p.bonus_wert,3,0);
    ins_quest ((char *) &schlre_p.rab_pr,3,0);
    ins_quest ((char *) &schlre_p.rab_wert,3,0);
    ins_quest ((char *) &schlre_p.art_sum,3,0);
    ins_quest ((char *) &schlre_p.vorkost,2,0);
    ins_quest ((char *) &schlre_p.grund_pr,3,0);
    ins_quest ((char *) &schlre_p.vorkost_wert,3,0);
    ins_quest ((char *) schlre_p.tierkennung,0,21);
    ins_quest ((char *) schlre_p.tierkenn,0,17);
    ins_quest ((char *) schlre_p.fettstufe,0,3);
    ins_quest ((char *) &schlre_p.schlacht_nr,2,0);
    ins_quest ((char *) &schlre_p.linie,1,0);
    ins_quest ((char *) &schlre_p.datum,2,0);
    ins_quest ((char *) schlre_p.stat,0,2);
    ins_quest ((char *) &schlre_p.leb_pr,3,0);
    ins_quest ((char *) &schlre_p.vork_stat,1,0);
    ins_quest ((char *) schlre_p.abrech_kz,0,3);
    ins_quest ((char *) &schlre_p.gew_leb,3,0);
    ins_quest ((char *) schlre_p.freibank,0,4);
    ins_quest ((char *) &schlre_p.erzeuger,2,0);
    ins_quest ((char *) schlre_p.bnp,0,2);
    ins_quest ((char *) &schlre_p.ag,2,0);
    ins_quest ((char *) &schlre_p.vorkost_wert1,3,0);
    ins_quest ((char *) &schlre_p.vorkost_wert6,3,0);
    ins_quest ((char *) &schlre_p.vorkost_wertl,3,0);
    ins_quest ((char *) &schlre_p.ges_pr,3,0);
    ins_quest ((char *) &schlre_p.anz_bu,1,0);
    ins_quest ((char *) &schlre_p.anz_unt,1,0);
    ins_quest ((char *) &schlre_p.anz_leber,1,0);
    ins_quest ((char *) &schlre_p.anz_freibank,1,0);
            sqltext = "update schlre_p set schlre_p.mdn = ?,  "
"schlre_p.ein_nr = ?,  schlre_p.part_nr = ?,  schlre_p.a = ?,  "
"schlre_p.a_krz_bz = ?,  schlre_p.netto_gew = ?,  "
"schlre_p.anzahl = ?,  schlre_p.hdkl = ?,  schlre_p.mfa = ?,  "
"schlre_p.bonus_pr = ?,  schlre_p.bonus_wert = ?,  "
"schlre_p.rab_pr = ?,  schlre_p.rab_wert = ?,  schlre_p.art_sum = ?,  "
"schlre_p.vorkost = ?,  schlre_p.grund_pr = ?,  "
"schlre_p.vorkost_wert = ?,  schlre_p.tierkennung = ?,  "
"schlre_p.tierkenn = ?,  schlre_p.fettstufe = ?,  "
"schlre_p.schlacht_nr = ?,  schlre_p.linie = ?,  schlre_p.datum = ?,  "
"schlre_p.stat = ?,  schlre_p.leb_pr = ?,  schlre_p.vork_stat = ?,  "
"schlre_p.abrech_kz = ?,  schlre_p.gew_leb = ?,  "
"schlre_p.freibank = ?,  schlre_p.erzeuger = ?,  schlre_p.bnp = ?,  "
"schlre_p.ag = ?,  schlre_p.vorkost_wert1 = ?,  "
"schlre_p.vorkost_wert6 = ?,  schlre_p.vorkost_wertl = ?,  "
"schlre_p.ges_pr = ?,  schlre_p.anz_bu = ?,  schlre_p.anz_unt = ?,  "
"schlre_p.anz_leber = ?,  schlre_p.anz_freibank = ? "

#line 30 "schlre_p.rpp"
                                  "where mdn = ? "
                                  "and   ein_nr = ? "
                                  "and   schlacht_nr = ? ";

            ins_quest ((char *) &schlre_p.mdn, 1, 0);
            ins_quest ((char *) &schlre_p.ein_nr, 2, 0);
            ins_quest ((char *) &schlre_p.schlacht_nr, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &schlre_p.mdn, 1, 0);
            ins_quest ((char *) &schlre_p.ein_nr, 2, 0);
            ins_quest ((char *) &schlre_p.schlacht_nr, 2, 0);
            test_upd_cursor = prepare_sql ("select ein_nr from schlre_p "
                                  "where mdn = ? "
                                  "and   ein_nr = ? "
                                  "and   schlacht_nr = ? ");
								   
            ins_quest ((char *) &schlre_p.mdn, 1, 0);
            ins_quest ((char *) &schlre_p.ein_nr, 2, 0);
            ins_quest ((char *) &schlre_p.schlacht_nr, 2, 0);
            del_cursor = prepare_sql ("delete from schlre_p "
                                  "where mdn = ? "
                                  "and   ein_nr = ? "
                                  "and   schlacht_nr = ? ");
    ins_quest ((char *) &schlre_p.mdn,1,0);
    ins_quest ((char *) &schlre_p.ein_nr,2,0);
    ins_quest ((char *) &schlre_p.part_nr,2,0);
    ins_quest ((char *) &schlre_p.a,3,0);
    ins_quest ((char *) schlre_p.a_krz_bz,0,13);
    ins_quest ((char *) &schlre_p.netto_gew,3,0);
    ins_quest ((char *) &schlre_p.anzahl,1,0);
    ins_quest ((char *) schlre_p.hdkl,0,7);
    ins_quest ((char *) &schlre_p.mfa,3,0);
    ins_quest ((char *) &schlre_p.bonus_pr,3,0);
    ins_quest ((char *) &schlre_p.bonus_wert,3,0);
    ins_quest ((char *) &schlre_p.rab_pr,3,0);
    ins_quest ((char *) &schlre_p.rab_wert,3,0);
    ins_quest ((char *) &schlre_p.art_sum,3,0);
    ins_quest ((char *) &schlre_p.vorkost,2,0);
    ins_quest ((char *) &schlre_p.grund_pr,3,0);
    ins_quest ((char *) &schlre_p.vorkost_wert,3,0);
    ins_quest ((char *) schlre_p.tierkennung,0,21);
    ins_quest ((char *) schlre_p.tierkenn,0,17);
    ins_quest ((char *) schlre_p.fettstufe,0,3);
    ins_quest ((char *) &schlre_p.schlacht_nr,2,0);
    ins_quest ((char *) &schlre_p.linie,1,0);
    ins_quest ((char *) &schlre_p.datum,2,0);
    ins_quest ((char *) schlre_p.stat,0,2);
    ins_quest ((char *) &schlre_p.leb_pr,3,0);
    ins_quest ((char *) &schlre_p.vork_stat,1,0);
    ins_quest ((char *) schlre_p.abrech_kz,0,3);
    ins_quest ((char *) &schlre_p.gew_leb,3,0);
    ins_quest ((char *) schlre_p.freibank,0,4);
    ins_quest ((char *) &schlre_p.erzeuger,2,0);
    ins_quest ((char *) schlre_p.bnp,0,2);
    ins_quest ((char *) &schlre_p.ag,2,0);
    ins_quest ((char *) &schlre_p.vorkost_wert1,3,0);
    ins_quest ((char *) &schlre_p.vorkost_wert6,3,0);
    ins_quest ((char *) &schlre_p.vorkost_wertl,3,0);
    ins_quest ((char *) &schlre_p.ges_pr,3,0);
    ins_quest ((char *) &schlre_p.anz_bu,1,0);
    ins_quest ((char *) &schlre_p.anz_unt,1,0);
    ins_quest ((char *) &schlre_p.anz_leber,1,0);
    ins_quest ((char *) &schlre_p.anz_freibank,1,0);
            ins_cursor = prepare_sql ("insert into schlre_p ("
"mdn,  ein_nr,  part_nr,  a,  a_krz_bz,  netto_gew,  anzahl,  hdkl,  mfa,  bonus_pr,  "
"bonus_wert,  rab_pr,  rab_wert,  art_sum,  vorkost,  grund_pr,  vorkost_wert,  "
"tierkennung,  tierkenn,  fettstufe,  schlacht_nr,  linie,  datum,  stat,  leb_pr,  "
"vork_stat,  abrech_kz,  gew_leb,  freibank,  erzeuger,  bnp,  ag,  vorkost_wert1,  "
"vorkost_wert6,  vorkost_wertl,  ges_pr,  anz_bu,  anz_unt,  anz_leber,  "
"anz_freibank) "

#line 57 "schlre_p.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 59 "schlre_p.rpp"
}
void SCHLRE_P_CLASS::prepare_o (char *order)
{

            ins_quest ((char *) &schlre_p.mdn, 1, 0);
            ins_quest ((char *) &schlre_p.ein_nr, 2, 0);
            if (order)
            {
    out_quest ((char *) &schlre_p.mdn,1,0);
    out_quest ((char *) &schlre_p.ein_nr,2,0);
    out_quest ((char *) &schlre_p.part_nr,2,0);
    out_quest ((char *) &schlre_p.a,3,0);
    out_quest ((char *) schlre_p.a_krz_bz,0,13);
    out_quest ((char *) &schlre_p.netto_gew,3,0);
    out_quest ((char *) &schlre_p.anzahl,1,0);
    out_quest ((char *) schlre_p.hdkl,0,7);
    out_quest ((char *) &schlre_p.mfa,3,0);
    out_quest ((char *) &schlre_p.bonus_pr,3,0);
    out_quest ((char *) &schlre_p.bonus_wert,3,0);
    out_quest ((char *) &schlre_p.rab_pr,3,0);
    out_quest ((char *) &schlre_p.rab_wert,3,0);
    out_quest ((char *) &schlre_p.art_sum,3,0);
    out_quest ((char *) &schlre_p.vorkost,2,0);
    out_quest ((char *) &schlre_p.grund_pr,3,0);
    out_quest ((char *) &schlre_p.vorkost_wert,3,0);
    out_quest ((char *) schlre_p.tierkennung,0,21);
    out_quest ((char *) schlre_p.tierkenn,0,17);
    out_quest ((char *) schlre_p.fettstufe,0,3);
    out_quest ((char *) &schlre_p.schlacht_nr,2,0);
    out_quest ((char *) &schlre_p.linie,1,0);
    out_quest ((char *) &schlre_p.datum,2,0);
    out_quest ((char *) schlre_p.stat,0,2);
    out_quest ((char *) &schlre_p.leb_pr,3,0);
    out_quest ((char *) &schlre_p.vork_stat,1,0);
    out_quest ((char *) schlre_p.abrech_kz,0,3);
    out_quest ((char *) &schlre_p.gew_leb,3,0);
    out_quest ((char *) schlre_p.freibank,0,4);
    out_quest ((char *) &schlre_p.erzeuger,2,0);
    out_quest ((char *) schlre_p.bnp,0,2);
    out_quest ((char *) &schlre_p.ag,2,0);
    out_quest ((char *) &schlre_p.vorkost_wert1,3,0);
    out_quest ((char *) &schlre_p.vorkost_wert6,3,0);
    out_quest ((char *) &schlre_p.vorkost_wertl,3,0);
    out_quest ((char *) &schlre_p.ges_pr,3,0);
    out_quest ((char *) &schlre_p.anz_bu,1,0);
    out_quest ((char *) &schlre_p.anz_unt,1,0);
    out_quest ((char *) &schlre_p.anz_leber,1,0);
    out_quest ((char *) &schlre_p.anz_freibank,1,0);
                    cursor_o = prepare_sql ("select "
"schlre_p.mdn,  schlre_p.ein_nr,  schlre_p.part_nr,  schlre_p.a,  "
"schlre_p.a_krz_bz,  schlre_p.netto_gew,  schlre_p.anzahl,  "
"schlre_p.hdkl,  schlre_p.mfa,  schlre_p.bonus_pr,  "
"schlre_p.bonus_wert,  schlre_p.rab_pr,  schlre_p.rab_wert,  "
"schlre_p.art_sum,  schlre_p.vorkost,  schlre_p.grund_pr,  "
"schlre_p.vorkost_wert,  schlre_p.tierkennung,  schlre_p.tierkenn,  "
"schlre_p.fettstufe,  schlre_p.schlacht_nr,  schlre_p.linie,  "
"schlre_p.datum,  schlre_p.stat,  schlre_p.leb_pr,  schlre_p.vork_stat,  "
"schlre_p.abrech_kz,  schlre_p.gew_leb,  schlre_p.freibank,  "
"schlre_p.erzeuger,  schlre_p.bnp,  schlre_p.ag,  "
"schlre_p.vorkost_wert1,  schlre_p.vorkost_wert6,  "
"schlre_p.vorkost_wertl,  schlre_p.ges_pr,  schlre_p.anz_bu,  "
"schlre_p.anz_unt,  schlre_p.anz_leber,  schlre_p.anz_freibank from schlre_p "

#line 68 "schlre_p.rpp"
                                          "where mdn = ? "
                                          "and   ein_nr = ? %s", order);
            }
            else
            {
    out_quest ((char *) &schlre_p.mdn,1,0);
    out_quest ((char *) &schlre_p.ein_nr,2,0);
    out_quest ((char *) &schlre_p.part_nr,2,0);
    out_quest ((char *) &schlre_p.a,3,0);
    out_quest ((char *) schlre_p.a_krz_bz,0,13);
    out_quest ((char *) &schlre_p.netto_gew,3,0);
    out_quest ((char *) &schlre_p.anzahl,1,0);
    out_quest ((char *) schlre_p.hdkl,0,7);
    out_quest ((char *) &schlre_p.mfa,3,0);
    out_quest ((char *) &schlre_p.bonus_pr,3,0);
    out_quest ((char *) &schlre_p.bonus_wert,3,0);
    out_quest ((char *) &schlre_p.rab_pr,3,0);
    out_quest ((char *) &schlre_p.rab_wert,3,0);
    out_quest ((char *) &schlre_p.art_sum,3,0);
    out_quest ((char *) &schlre_p.vorkost,2,0);
    out_quest ((char *) &schlre_p.grund_pr,3,0);
    out_quest ((char *) &schlre_p.vorkost_wert,3,0);
    out_quest ((char *) schlre_p.tierkennung,0,21);
    out_quest ((char *) schlre_p.tierkenn,0,17);
    out_quest ((char *) schlre_p.fettstufe,0,3);
    out_quest ((char *) &schlre_p.schlacht_nr,2,0);
    out_quest ((char *) &schlre_p.linie,1,0);
    out_quest ((char *) &schlre_p.datum,2,0);
    out_quest ((char *) schlre_p.stat,0,2);
    out_quest ((char *) &schlre_p.leb_pr,3,0);
    out_quest ((char *) &schlre_p.vork_stat,1,0);
    out_quest ((char *) schlre_p.abrech_kz,0,3);
    out_quest ((char *) &schlre_p.gew_leb,3,0);
    out_quest ((char *) schlre_p.freibank,0,4);
    out_quest ((char *) &schlre_p.erzeuger,2,0);
    out_quest ((char *) schlre_p.bnp,0,2);
    out_quest ((char *) &schlre_p.ag,2,0);
    out_quest ((char *) &schlre_p.vorkost_wert1,3,0);
    out_quest ((char *) &schlre_p.vorkost_wert6,3,0);
    out_quest ((char *) &schlre_p.vorkost_wertl,3,0);
    out_quest ((char *) &schlre_p.ges_pr,3,0);
    out_quest ((char *) &schlre_p.anz_bu,1,0);
    out_quest ((char *) &schlre_p.anz_unt,1,0);
    out_quest ((char *) &schlre_p.anz_leber,1,0);
    out_quest ((char *) &schlre_p.anz_freibank,1,0);
                    cursor_o = prepare_sql ("select "
"schlre_p.mdn,  schlre_p.ein_nr,  schlre_p.part_nr,  schlre_p.a,  "
"schlre_p.a_krz_bz,  schlre_p.netto_gew,  schlre_p.anzahl,  "
"schlre_p.hdkl,  schlre_p.mfa,  schlre_p.bonus_pr,  "
"schlre_p.bonus_wert,  schlre_p.rab_pr,  schlre_p.rab_wert,  "
"schlre_p.art_sum,  schlre_p.vorkost,  schlre_p.grund_pr,  "
"schlre_p.vorkost_wert,  schlre_p.tierkennung,  schlre_p.tierkenn,  "
"schlre_p.fettstufe,  schlre_p.schlacht_nr,  schlre_p.linie,  "
"schlre_p.datum,  schlre_p.stat,  schlre_p.leb_pr,  schlre_p.vork_stat,  "
"schlre_p.abrech_kz,  schlre_p.gew_leb,  schlre_p.freibank,  "
"schlre_p.erzeuger,  schlre_p.bnp,  schlre_p.ag,  "
"schlre_p.vorkost_wert1,  schlre_p.vorkost_wert6,  "
"schlre_p.vorkost_wertl,  schlre_p.ges_pr,  schlre_p.anz_bu,  "
"schlre_p.anz_unt,  schlre_p.anz_leber,  schlre_p.anz_freibank from schlre_p "

#line 74 "schlre_p.rpp"
                                          "where mdn = ? "
                                          "and   ein_nr = ? ");
           }
}


int SCHLRE_P_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int SCHLRE_P_CLASS::dbreadfirst_o (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         if (cursor_o == -1)
         {
                this->prepare_o (order);
         }
         open_sql (cursor_o);
         fetch_sql (cursor_o);
         return (sqlstatus);
}

int SCHLRE_P_CLASS::dbread_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{

         fetch_sql (cursor_o);
         return (sqlstatus);
}

int SCHLRE_P_CLASS::dbclose_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1) return 0;
         close_sql (cursor_o);
         cursor_o = -1;
         return (0);
}

int SCHLRE_P_CLASS::dbupdate ()
/**
Tabelle schlre_p Updaten.
**/
{
    strcpy (schlre_p.freibank,"0");
    strcpy (schlre_p.stat,"0");
    if (schlre_p.anzahl == 0) schlre_p.anzahl = 0;
    if ( test_upd_cursor == -1)
    {
        this->prepare ();
    }
         open_sql (test_upd_cursor);
         fetch_sql (test_upd_cursor);
         if (sqlstatus == 100)
         {
                   execute_curs (ins_cursor);
         }
         else if (sqlstatus == 0)
         {
                   execute_curs (upd_cursor);
         }

         return 0;
}

