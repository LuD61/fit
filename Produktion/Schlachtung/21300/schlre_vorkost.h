#ifndef _SCHLRE_VORKOST_DEF
#define _SCHLRE_VORKOST_DEF

#include "dbclass.h"

struct SCHLRE_VORKOST {
   short     mdn;
   long      rech_nr;
   short     kost_art;
   char      kost_art_bz[25];
   short     rech_basis;
   double    rech_wert;
   double    wert;
   long      anzahl;
   short     mwst_schl;
   long      ein_nr;
};
extern struct SCHLRE_VORKOST schlre_vorkost, schlre_vorkost_null;

#line 7 "schlre_vorkost.rh"

class SCHLRE_VORKOST_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SCHLRE_VORKOST_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
