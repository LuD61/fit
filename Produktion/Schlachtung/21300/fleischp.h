#ifndef _FLEISCHP_DEF
#define _FLEISCHP_DEF

#include "dbclass.h"

struct FLEISCHPRUEFRING {
   long      lfnr;
   char      datum[11];
   char      gattung[3];
   char      taetowie[17];
   char      schlachthof[17];
   short     qualiprogramm;
   char      h_klasse[3];
   long      p_gruppe;
   double    netto_gew;
   double    speck_mass;
   double    fleis_mass;
   long      reflektion;
   double    mfa;
   double    ph_wert;
   long      beanst;
   char      bemerkung[7];
   char      bemerkungvl[11];
   char      bemerkungth[11];
   char      geschlecht[4];
   char      beanstandung1[7];
   char      beanstandung2[7];
   char      beanstandung3[7];
   char      beanstandung4[7];
   char      beanstandung5[7];
   double    salmonellennr;
   char      bemerkungv[11];
   char      kun_kurz[7];
   long      mastb_nr;
   char      mastb_name[17];
   char      mastb_str[37];
   char      mastb_plz[9];
   char      mastb_ort[37];
   char      mastb_balis[21];
   char      lieferant[17];
   char      lief_name[17];
   char      lief_str[37];
   char      lief_ort[37];
   char      lief_balis[21];
   long      schlb_nr;
   char      schlb_name[17];
   char      schlb_str[37];
   char      schlb_ort[37];
   char      schlb_balis[21];
   long      sped_nr;
   char      sped_name[17];
   char      sped_str[37];
   char      sped_ort[37];
   char      sped_balis[21];
   double    kg_preis;
   char      dateiname[23];
   long      dat;
   long      fit_ag;
   double    fit_a;
   long      fit_adr;
   double    fit_basis_pr;
   double    fit_preis;
   short     fit_bu;
   short     fit_unt;
   short     fit_sal;
   short     fit_leber;
   long      ein_nr;
   long      rech_nr;
   char      err_txt[61];
   short     status;
};
extern struct FLEISCHPRUEFRING fleischpruefring, fleischpruefring_null;

#line 7 "fleischp.rh"

struct FLEISCH {
   long      anzahl;
   double    anzahlQS;
   double    anzahlGQ;
   };
   extern struct FLEISCH fleisch;

class FLEISCHP_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
               void preparesum (void);
               short cu_fleischpsum;
               short cu_liefbez;
               short ins_cursorfl;
               short cursorfl;
       public :
               FLEISCHP_CLASS () : DB_CLASS ()
               {
               		cu_fleischpsum = -1;
               		cu_liefbez = -1;
               		ins_cursorfl = -1;
               		cursorfl = -1;
               }
               int dbreadfirst (void);
               int dbread (void);
               int dbupdate (void);
               int dbdelete (void);
               int dbinsert (void);
               int lese_sum (void);
               void close_sum (void);
	           void all_out_quest (void);
               
               
};
#endif
