#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "qsschlpos.h"

struct QSSCHLPOS qsschlpos, qsschlpos_null;

void QSSCHLPOS_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &qsschlpos.mdn, 1, 0);
            ins_quest ((char *) &qsschlpos.fil, 1, 0);
            ins_quest ((char *) qsschlpos.lief, 0, 17);
            ins_quest ((char *) &qsschlpos.ein_nr, 2, 0);
            ins_quest ((char *) &qsschlpos.lfd, 2, 0);
            ins_quest ((char *) &qsschlpos.a, 3, 0);
            ins_quest ((char *) &qsschlpos.schlacht_nr, 2, 0);
    out_quest ((char *) &qsschlpos.mdn,1,0);
    out_quest ((char *) &qsschlpos.fil,1,0);
    out_quest ((char *) &qsschlpos.lfd,2,0);
    out_quest ((char *) &qsschlpos.ein_nr,2,0);
    out_quest ((char *) qsschlpos.lief,0,17);
    out_quest ((char *) &qsschlpos.a,3,0);
    out_quest ((char *) &qsschlpos.schlacht_nr,2,0);
    out_quest ((char *) qsschlpos.txt,0,61);
    out_quest ((char *) &qsschlpos.wrt,1,0);
    out_quest ((char *) &qsschlpos.temp,3,0);
    out_quest ((char *) &qsschlpos.ph_wert,3,0);
    out_quest ((char *) &qsschlpos.dat,2,0);
    out_quest ((char *) qsschlpos.pers_nam,0,9);
            cursor = prepare_sql ("select qsschlpos.mdn,  "
"qsschlpos.fil,  qsschlpos.lfd,  qsschlpos.ein_nr,  qsschlpos.lief,  "
"qsschlpos.a,  qsschlpos.schlacht_nr,  qsschlpos.txt,  qsschlpos.wrt,  "
"qsschlpos.temp,  qsschlpos.ph_wert,  qsschlpos.dat,  "
"qsschlpos.pers_nam from qsschlpos "

#line 32 "qsschlpos.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   ein_nr = ? "
                                  "and lfd = ? "
                                  "and a = ? "
                                  "and schlacht_nr = ?");

    ins_quest ((char *) &qsschlpos.mdn,1,0);
    ins_quest ((char *) &qsschlpos.fil,1,0);
    ins_quest ((char *) &qsschlpos.lfd,2,0);
    ins_quest ((char *) &qsschlpos.ein_nr,2,0);
    ins_quest ((char *) qsschlpos.lief,0,17);
    ins_quest ((char *) &qsschlpos.a,3,0);
    ins_quest ((char *) &qsschlpos.schlacht_nr,2,0);
    ins_quest ((char *) qsschlpos.txt,0,61);
    ins_quest ((char *) &qsschlpos.wrt,1,0);
    ins_quest ((char *) &qsschlpos.temp,3,0);
    ins_quest ((char *) &qsschlpos.ph_wert,3,0);
    ins_quest ((char *) &qsschlpos.dat,2,0);
    ins_quest ((char *) qsschlpos.pers_nam,0,9);
            sqltext = "update qsschlpos set "
"qsschlpos.mdn = ?,  qsschlpos.fil = ?,  qsschlpos.lfd = ?,  "
"qsschlpos.ein_nr = ?,  qsschlpos.lief = ?,  qsschlpos.a = ?,  "
"qsschlpos.schlacht_nr = ?,  qsschlpos.txt = ?,  qsschlpos.wrt = ?,  "
"qsschlpos.temp = ?,  qsschlpos.ph_wert = ?,  qsschlpos.dat = ?,  "
"qsschlpos.pers_nam = ? "

#line 41 "qsschlpos.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   ein_nr = ? "
                                  "and lfd = ? "
                                  "and a = ? "
                                  "and schlacht_nr = ?";

            ins_quest ((char *) &qsschlpos.mdn, 1, 0);
            ins_quest ((char *) &qsschlpos.fil, 1, 0);
            ins_quest ((char *) qsschlpos.lief, 0, 17);
            ins_quest ((char *) &qsschlpos.ein_nr, 2, 0);
            ins_quest ((char *) &qsschlpos.lfd, 2, 0);
            ins_quest ((char *) &qsschlpos.a, 3, 0);
            ins_quest ((char *) &qsschlpos.schlacht_nr, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &qsschlpos.mdn, 1, 0);
            ins_quest ((char *) &qsschlpos.fil, 1, 0);
            ins_quest ((char *) qsschlpos.lief, 0, 17);
            ins_quest ((char *) &qsschlpos.ein_nr, 2, 0);
            ins_quest ((char *) &qsschlpos.lfd, 2, 0);
            ins_quest ((char *) &qsschlpos.a, 3, 0);
            ins_quest ((char *) &qsschlpos.schlacht_nr, 2, 0);
            test_upd_cursor = prepare_sql ("select lief from qsschlpos "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   ein_nr = ? "
                                  "and lfd = ? "
                                  "and a = ? "
                                  "and schlacht_nr = ?");
            ins_quest ((char *) &qsschlpos.mdn, 1, 0);
            ins_quest ((char *) &qsschlpos.fil, 1, 0);
            ins_quest ((char *) qsschlpos.lief, 0, 17);
            ins_quest ((char *) &qsschlpos.ein_nr, 2, 0);
            ins_quest ((char *) &qsschlpos.lfd, 2, 0);
            ins_quest ((char *) &qsschlpos.a, 3, 0);
            ins_quest ((char *) &qsschlpos.schlacht_nr, 2, 0);
            del_cursor = prepare_sql ("delete from qsschlpos "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   lief = ? "
                                  "and   ein_nr = ? "
                                  "and lfd = ? "
                                  "and a = ? "
                                  "and schlacht_nr = ?");
    ins_quest ((char *) &qsschlpos.mdn,1,0);
    ins_quest ((char *) &qsschlpos.fil,1,0);
    ins_quest ((char *) &qsschlpos.lfd,2,0);
    ins_quest ((char *) &qsschlpos.ein_nr,2,0);
    ins_quest ((char *) qsschlpos.lief,0,17);
    ins_quest ((char *) &qsschlpos.a,3,0);
    ins_quest ((char *) &qsschlpos.schlacht_nr,2,0);
    ins_quest ((char *) qsschlpos.txt,0,61);
    ins_quest ((char *) &qsschlpos.wrt,1,0);
    ins_quest ((char *) &qsschlpos.temp,3,0);
    ins_quest ((char *) &qsschlpos.ph_wert,3,0);
    ins_quest ((char *) &qsschlpos.dat,2,0);
    ins_quest ((char *) qsschlpos.pers_nam,0,9);
            ins_cursor = prepare_sql ("insert into qsschlpos ("
"mdn,  fil,  lfd,  ein_nr,  lief,  a,  schlacht_nr,  txt,  wrt,  temp,  ph_wert,  dat,  pers_nam) "

#line 89 "qsschlpos.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?)");

#line 91 "qsschlpos.rpp"
}

int QSSCHLPOS_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

