#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include<winbase.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_draw.h"
#include "wmask.h"
#include "mo_meld.h"
#include "mo_qm.h" 

static mfont ubfont = {"MS SANS SERIF", 100, 0, 1,
                             BLUECOL,
                             GRAYCOL,
                             0,
                             NULL};

static mfont textfont = {"", 100, 0, 1,
                             YELLOWCOL,
                             GRAYCOL,
                             0,
                             NULL};

static mfont buttonfont = {"", 100, 0, 1,
                           RGB (0, 255, 255),
                           BLUECOL,
                           0,
                           NULL};

static mfont scrollfont = { "", 100, 0, 1,
                            BLUECOL,
                            BLUECOL,
                            1,
                            NULL};

static char *Texte0[] = {"Laderaum hygienisch",
                 "Hacken und Beh�ltnisse i.O.",
			     "Holzpaletten/Kartonagen",
			     "Fleisch korrekt verpackt",
			     "Kleidung hygienisch",
			     "Kopfbedeckung",
};

static 	int textanz;
static 	int frmtanz = 6;

static char *Ub0[] = {"Qualit�tssicherung Hygiene",
                      NULL,
};

static int Yes0[] = {1, 1, 1, 1, 1, 1};
static int No0[]  = {0, 0, 0, 0, 0, 0};


static int MaxRowPos = 6;
static int MaxColPos = 1;

static focustab [][2] = {1, 2,
                          4, 5,
						  7, 8,
					     10,11,
						 13,14,
						 16,17,
                         19,20,  
                         22,23,  
                         25,26,  
                         28,29,  
                         31,32,  
                         34,35,  
                         37,38,  
                         40,41,  
};

static form UbForm;
static form TextForm;
static form QmForm;


ColButton Bu1  =    {NULL, -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     RGB (0, 255, 255),
                     BLUECOL,
                     17};

ColButton Bu2  =    {NULL, -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, -1, -1,
                     NULL, 0, 0,
                     RGB (0, 255, 255),
                     LTGRAYCOL,
                     2};

ColButton ScrollUp    =    {NULL, -1, -1,
                            NULL, 0, 0,
                            NULL, 0, 0,
                            NULL, -1, -1,
                            NULL, 0, 0,
                            RGB (0, 255, 255),
                            BLUECOL,
                            14};


ColButton ScrollDown   =   {"", -1, -1,
                            NULL, 0, 0,
                            NULL, 0, 0,
                            NULL, -1, -1,
                            NULL, 0, 0,
                            RGB (255, 255, 255),
                            BLUECOL,
                            14};

static int scx = 15;
static int scy = 3;

static field _fscroll[] = {
(char *) &ScrollUp,            scx, scy, 0, 0, 0, "", COLBUTTON, 0, 0, 950,
(char *) &ScrollDown,          scx, scy, 0, 0, 0, "", COLBUTTON, 0, 0, 951,
};

static form fscroll = {2, 0, 0, _fscroll, 0, 0, 0, 0, &scrollfont};
static BOOL  ScrollBar;

QMKOPF::QMKOPF (HINSTANCE hInstance, HWND hMainWindow)
{
	                   this->hInstance   = hInstance;
	                   this->hMainWindow = hMainWindow;
					   registered = 0;
					   lpszClassName = "QmW";
					   lpszScClassName = "QmWSc";
					   hWnd = NULL;

					   CoB = new COBTAB [30];
					   QmForm.mask = new field [30];
					   QmForm.fieldanz  = 0;
                       QmForm.frmstart  = 0;
                       QmForm.frmscroll = 0;
                       QmForm.caption   = NULL;
                       QmForm.before    = NULL;
                       QmForm.after     = NULL;
                       QmForm.cbfield   = NULL;
                       QmForm.font      = NULL;

					   TextForm.mask = new field [2];
					   TextForm.fieldanz  = 0;
                       TextForm.frmstart  = 0;
                       TextForm.frmscroll = 0;
                       TextForm.caption   = NULL;
                       TextForm.before    = NULL;
                       TextForm.after     = NULL;
                       TextForm.cbfield   = NULL;
                       TextForm.font      = NULL;

					   UbForm.mask = new field [1];
					   UbForm.fieldanz  = 0;
                       UbForm.frmstart  = 0;
                       UbForm.frmscroll = 0;
                       UbForm.caption   = NULL;
                       UbForm.before    = NULL;
                       UbForm.after     = NULL;
                       UbForm.cbfield   = NULL;
                       UbForm.font      = NULL;

                       kreuz            = LoadBitmap (hInstance, "kreuz");
                       ScrollUp.bmp     = LoadBitmap (hInstance, "pfeilo");
                       ScrollDown.bmp   = LoadBitmap (hInstance, "pfeilu");

					   FRow = FCol = 0;
					   Offset = 20;
			           scrfx = (double) 1.0;
			           scrfy = (double) 1.0;
                       MaxRowPos = 6;
                       MaxColPos = 1;
					   Texte = Texte0;
					   Yes   = Yes0;
					   No    = No0;
					   Ub    = Ub0;
					   textanz = 6;
					   frmtanz = 6;
					   scstart = 0;
					   scend = 6;
					   style = WS_POPUP;
					   Exstyle = 0;
					   ScrollBar = FALSE;
}

QMKOPF::~QMKOPF ()
{
					   delete QmForm.mask;
					   delete TextForm.mask;
					   delete UbForm.mask;

			           if (CoB == NULL) return;

			           delete CoB;
					   CoB = NULL;
			           delete QmForm.mask;
}

void QMKOPF::SetParams (char **ub, char **texte, int *yes, int *no, DWORD style)
/**
Texte und Werte setzen.
**/
{
	  Texte = texte;
	  Yes   = yes;
	  No    = no;
	  Ub    = ub;
	  this->style = style;
	  Exstyle = 0;
      ScrollBar = FALSE;
}

void QMKOPF::SetParamsEx (char **ub, char **texte, int *yes, int *no, 
						  DWORD style, DWORD Exstyle, BOOL Scrollbar)
/**
Texte und Werte setzen.
**/
{
	  Texte = texte;
	  Yes   = yes;
	  No    = no;
	  Ub    = ub;
	  this->style = style;
	  this->Exstyle = Exstyle;
	  ScrollBar = Scrollbar;
}

int QMKOPF::GetTextanz (void)
/**
Anzahl Textzeilen ermitteln.
**/
{
	  int i;

	  for (i = 0; Texte[i]; i ++);
	  return i;
}

void QMKOPF::KorrScroll (void)
/**
ScrollButton's anpassen.
**/
{
	  RECT rect, rect1;
	  TEXTMETRIC tm;
	  int cx, cy;
	  
      SetTextMetrics (hMainWindow, &tm, &scrollfont);
      cx =  scx * tm.tmAveCharWidth;
	  cy =  scy * tm.tmHeight;
	  fscroll.mask[0].length = cx;
	  fscroll.mask[0].rows   = cy;
	  fscroll.mask[1].length = cx;
	  fscroll.mask[1].rows   = cy;
	  GetClientRect (hMainWindow, &rect);
	  GetWindowRect (hMainWindow, &rect1); 
	  if ((style & WS_CHILD) == 0)
	  {
               rect.right  = rect1.right  - rect1.left;
			   rect.bottom = rect1.bottom - rect1.top;
	  }

	  fscroll.mask[0].pos[0] = 0;
	  fscroll.mask[0].pos[1] = rect.right  - cx;
	  fscroll.mask[1].pos[0] = rect.bottom - cy;
	  fscroll.mask[1].pos[1] = rect.right  - cx;
	  if (ScrollBar)
	  {
	                fscroll.mask[0].pos[1] = 0;
	                fscroll.mask[1].pos[1] = 0;
	  }
}

void QMKOPF::KorrScrollhWnd (void)
/**
ScrollButton's anpassen.
**/
{
	  RECT rect;
	  TEXTMETRIC tm;
	  int cx, cy;
	  
      SetTextMetrics (hWnd, &tm, &scrollfont);
      cx =  scx * tm.tmAveCharWidth;
	  cy =  scy * tm.tmHeight;
	  fscroll.mask[0].length = cx;
	  fscroll.mask[0].rows   = cy;
	  fscroll.mask[1].length = cx;
	  fscroll.mask[1].rows   = cy;
	  if (ScrollBar == FALSE)
	  {
	           GetClientRect (hWnd, &rect);
	           fscroll.mask[0].pos[0] = 0;
	           fscroll.mask[0].pos[1] = rect.right  - cx;
	           fscroll.mask[1].pos[0] = rect.bottom - cy;
	           fscroll.mask[1].pos[1] = rect.right  - cx;
	  }
	  else
	  {
	           GetClientRect (ScrollhWnd, &rect);
	           fscroll.mask[0].pos[0] = 0;
	           fscroll.mask[0].pos[1] = 0;
	           fscroll.mask[1].pos[0] = rect.bottom - cy;
	           fscroll.mask[1].pos[1] = 0;
	  }
}


void QMKOPF::FillQmForm0 (int i)
{
	  int row;
	  int pos;

	  pos = i;
	  row = i * 4 + 4;
	  i = i * 3;   

      QmForm.mask[i].feld = (char *) CoB[pos].GetText ();
	  QmForm.mask[i].length   = 40;
	  QmForm.mask[i].rows     = 3;
	  QmForm.mask[i].pos[0]   = row;
	  QmForm.mask[i].pos[1]   = 1 + Offset;
	  QmForm.mask[i].feldid   = NULL;
	  QmForm.mask[i].picture  = "";
	  QmForm.mask[i].attribut = COLBUTTON;
	  QmForm.mask[i].before   = NULL;
	  QmForm.mask[i].after    = NULL;
	  QmForm.mask[i].BuId     = 900 + i;

	  i ++;
      QmForm.mask[i].feld = (char *) CoB[pos].GetYes ();
	  QmForm.mask[i].length = 6;
	  QmForm.mask[i].rows   = 3;
	  QmForm.mask[i].pos[0] = row;
	  QmForm.mask[i].pos[1] = 47 + Offset;
	  QmForm.mask[i].feldid   = NULL;
	  QmForm.mask[i].picture  = "";
	  QmForm.mask[i].attribut = COLBUTTON;
	  QmForm.mask[i].before   = NULL;
	  QmForm.mask[i].after    = NULL;
	  QmForm.mask[i].BuId     = 900 + i;

	  i ++;
      QmForm.mask[i].feld = (char *) CoB[pos].GetNo ();
	  QmForm.mask[i].length = 6;
	  QmForm.mask[i].rows   = 3;
	  QmForm.mask[i].pos[0] = row;
	  QmForm.mask[i].pos[1] = 57 + Offset;
	  QmForm.mask[i].feldid   = NULL;
	  QmForm.mask[i].picture  = "";
	  QmForm.mask[i].attribut = COLBUTTON;
	  QmForm.mask[i].before   = NULL;
	  QmForm.mask[i].after    = NULL;
	  QmForm.mask[i].BuId     = 900 + i;
}

void QMKOPF::FillQmForm (int i)
{
	  int row;
	  int pos;

	  pos = i;
	  row = i * 4 + 4;
	  i = i * 3;   

      QmForm.mask[i].feld = (char *) CoB[pos].GetText ();

	  i ++;
      QmForm.mask[i].feld = (char *) CoB[pos].GetYes ();
	  i ++;
      QmForm.mask[i].feld = (char *) CoB[pos].GetNo ();
}


void QMKOPF::FillBu0 (int pos)
/**
ButtonTexte fuellen.
**/
{
	 int i; 

	 for (i = 0; i < textanz; i ++, pos ++)
	 {
		      if (Texte[pos] == NULL) 
			  {
				  i ++;
				  break;
			  }
              Bu1.text1 = Texte[pos];

			  CoB[i].SetText (&Bu1);
			  Bu2.bmp = NULL;
 		      if (Yes[pos] == 1)
			  {
			       Bu2.bmp = kreuz;
			  }
			  CoB[i].SetYes  (&Bu2);
			  Bu2.bmp = NULL;
 		      if (No[pos] == 1 && Yes[i] == 0)
			  {
			       Bu2.bmp = kreuz;
			  }
			  CoB[i].SetNo   (&Bu2);
			  FillQmForm0 (i);
	  }
	  scend = i - 1; 
	  QmForm.fieldanz = i * 3;
}


void QMKOPF::FillBu (int pos)
/**
ButtonTexte fuellen.
**/
{
	 int i; 

	 for (i = 0; i < frmtanz; i ++, pos ++)
	 {
		      if (Texte[pos] == NULL) 
			  {
				  i ++;
				  break;
			  }
              Bu1.text1 = Texte[pos];

			  CoB[i].SetText (&Bu1);
			  Bu2.bmp = NULL;
 		      if (Yes[pos] == 1)
			  {
			       Bu2.bmp = kreuz;
			  }
			  CoB[i].SetYes  (&Bu2);
			  Bu2.bmp = NULL;
 		      if (No[pos] == 1 && Yes[i] == 0)
			  {
			       Bu2.bmp = kreuz;
			  }
			  CoB[i].SetNo   (&Bu2);
			  FillQmForm (i);
	  }
	  scend = i - 1; 
	  QmForm.fieldanz = i * 3;
}

void QMKOPF::SetColBu (void)
/**
Button�s setzen.
**/
{


	 if (scrfx > (double) 1.0)
	 {
		 ubfont.FontHeight   = 120;
		 textfont.FontHeight   = 120;
		 buttonfont.FontHeight = 120;
	 }
     else if (scrfx < (double) 1.0)
     {
		 ubfont.FontHeight   = (int) (double) ((double) ubfont.FontHeight * scrfx);
		 textfont.FontHeight   = (int) (double) ((double) textfont.FontHeight * scrfx);
		 buttonfont.FontHeight = (int) (double) ((double) buttonfont.FontHeight * scrfx);
     }

	 textanz = GetTextanz ();
	 KorrScroll ();
     UbForm.mask[0].feld = Ub[0];     
	 UbForm.mask[0].length   = 0;
	 UbForm.mask[0].rows     = 0;
	 UbForm.mask[0].pos[0]   = 0;
	 UbForm.mask[0].pos[1]   = 1;
	 UbForm.mask[0].feldid   = NULL;
	 UbForm.mask[0].picture  = "";
	 UbForm.mask[0].attribut = DISPLAYONLY;
	 UbForm.mask[0].before   = NULL;
	 UbForm.mask[0].after    = NULL;
	 UbForm.mask[0].BuId     = 0;
	 UbForm.fieldanz = 1;
     UbForm.font = &ubfont;

     TextForm.mask[0].feld     = "Ja";
	 TextForm.mask[0].length   = 6;
	 TextForm.mask[0].rows     = 0;
	 TextForm.mask[0].pos[0]   = 1;
	 TextForm.mask[0].pos[1]   = 47 + Offset;
	 TextForm.mask[0].feldid   = NULL;
	 TextForm.mask[0].picture  = "";
	 TextForm.mask[0].attribut = DISPLAYONLY;
	 TextForm.mask[0].before   = NULL;
	 TextForm.mask[0].after    = NULL;
	 TextForm.mask[0].BuId     = 0;

     TextForm.mask[1].feld     = "Nein";
	 TextForm.mask[1].length   =  6;
	 TextForm.mask[1].rows     = 0;
	 TextForm.mask[1].pos[0]   = 1;
	 TextForm.mask[1].pos[1]   = 57 + Offset;
	 TextForm.mask[1].feldid   = NULL;
	 TextForm.mask[1].picture  = "";
	 TextForm.mask[1].attribut = DISPLAYONLY;
	 TextForm.mask[1].before   = NULL;
	 TextForm.mask[1].after    = NULL;
	 TextForm.mask[1].BuId     = 0;

	 TextForm.fieldanz = 2;
     TextForm.font = &textfont;

	 FillBu0 (0);
     QmForm.font = &buttonfont;
	 Setfeldanz ();
}

void QMKOPF::Display (void)
{
	  int i;

	  for (i = 0; i < QmForm.fieldanz; i++)
	  {
		  InvalidateRect (QmForm.mask[i].feldid, NULL, TRUE);
		  UpdateWindow   (QmForm.mask[i].feldid);
	  }
}

void QMKOPF::Scrollup (void)
{
	  
	  if (scstart == 0) return;
	  scstart -= frmtanz;
	  if (scstart < 0) scstart = 0;
	  FillBu (scstart);
      Display ();
      SetFocus (QmForm.mask[focustab[FRow][FCol]].feldid);
}

void QMKOPF::Scrolldown (void)
{
	  scstart += frmtanz;
	  while (scstart + frmtanz > textanz) 
	  {
		  scstart --;
		  if (scstart < 0)
		  {
			  scstart = 0;
			  break;
		  }
	  }
	  FillBu (scstart);
      Display ();
      SetFocus (QmForm.mask[focustab[FRow][FCol]].feldid);
}

void QMKOPF::Register (void)
{
    WNDCLASS wc;

	if (registered) return;

    wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
    wc.lpfnWndProc   =  (WNDPROC) WndProc;
    wc.cbClsExtra    =  0;
    wc.cbWndExtra    =  0;
    wc.hInstance     =  hInstance;
    wc.hIcon         =  LoadIcon (hInstance, "FITICON");
    wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground =  GetStockObject (GRAY_BRUSH);
    wc.lpszMenuName  =  "";
    wc.lpszClassName =  lpszClassName;

    RegisterClass(&wc);

    wc.lpfnWndProc   =  (WNDPROC) ScWndProc;
    wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
//    wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
    wc.lpszClassName =  lpszScClassName;
    RegisterClass(&wc);
}

void QMKOPF::Setfeldanz (void)
/**
Anzahl der Felder in Abhaengigkeit von der Fenstergroesse ermitteln.
**/
{
   TEXTMETRIC tm;
   int ly;
   RECT rect, rect1;
   int last;
	  
   SetTextMetrics (hMainWindow, &tm, &buttonfont);

   GetClientRect (hMainWindow, &rect); 
   GetWindowRect (hMainWindow, &rect1); 
   if ((style & WS_CHILD) == 0)
   {
           rect.bottom = rect1. bottom - rect1.top;
   }

   last = QmForm.fieldanz - 1;
   ly = (QmForm.mask[last].pos[0] + QmForm.mask[last].rows) * tm.tmHeight;
   while (ly >= rect.bottom)
   {
	   if (last == 0) break;
	   last --;
       ly = (QmForm.mask[last].pos[0] + QmForm.mask[last].rows) * tm.tmHeight;
   }
   QmForm.fieldanz = last + 1;
   frmtanz = QmForm.fieldanz / 3;
   MaxRowPos = frmtanz - 1;
}

void QMKOPF::OpenScrollBar (void)
/**
Scrollbar oeffnen.
**/
{
	  RECT rect;
	  TEXTMETRIC tm;
	  int x,y, cx, cy;
	  int border;
	  
	  border = GetSystemMetrics (SM_CXBORDER);
      SetTextMetrics (hMainWindow, &tm, &scrollfont);
	  GetClientRect (hWnd, &rect);
      cx =  scx * tm.tmAveCharWidth;
	  x = rect.right - cx - 2 * border;
	  y = 0;
	  cy = rect.bottom;
	  ScrollhWnd = CreateWindowEx (
			             WS_EX_CLIENTEDGE,
			             lpszScClassName,
                         "",
                         WS_CHILD | WS_VISIBLE,
                         x, y,
                         cx,
                         cy,
                         hWnd,
                         NULL,
                         hInstance,
                         NULL);
}


void QMKOPF::Open (void)
{
//	int x,y, cx, cy;
	RECT rect, rect1;

	Register ();

	GetClientRect (hMainWindow, &rect); 
	GetWindowRect (hMainWindow, &rect1); 


	if (style & WS_CHILD)
	{
	       hWnd = CreateWindowEx (
			             Exstyle,
			             lpszClassName,
                         "",
                         style,
                         0, 0,
                         rect.right,
                         rect.bottom,
                         hMainWindow,
                         NULL,
                         hInstance,
                         NULL);
	}
    else
	{
	       hWnd = CreateWindowEx (
			             Exstyle, 
			             lpszClassName,
                         "",
                         style,
                         rect1.left, rect1.top,
                         rect1.right - rect1.left,
                         rect1.bottom - rect1.top,
                         hMainWindow,
                         NULL,
                         hInstance,
                         NULL);
    }
    if (ScrollBar && textanz > frmtanz)
    { 
	       OpenScrollBar ();
	}
    KorrScrollhWnd ();
	ShowWindow (hWnd, SW_SHOWNORMAL);
    UpdateWindow (hWnd); 
}


void QMKOPF::ProcessMessages (void)
{
	      MSG msg;

		  syskey = 0;
          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
				  switch (msg.wParam)
				  {
				      case VK_F5 :
	             	        syskey = KEY5;
							break;
				      case VK_F12 :
	             	        syskey = KEY12;
							break;
				      case VK_DOWN :
	             	        syskey = KEYDOWN;
							FocusDown ();
							break;
				      case VK_UP :
	             	        syskey = KEYUP;
							FocusUp ();
							break;
				      case VK_RIGHT :
	             	        syskey = KEYRIGHT;
							FocusRight ();
							break;
				      case VK_LEFT :
	             	        syskey = KEYLEFT;
							FocusLeft ();
							break;
					  case VK_PRIOR :
						    Scrollup ();
							syskey = KEYPGU;
							break;
					  case VK_NEXT :
						    Scrolldown ();
							syskey = KEYPGD;
							break;
                      case VK_RETURN :  
	             	        syskey = KEYCR;
							Select ();
							break;
				  }
			  }
			  else if (msg.message == WM_COMMAND)
			  {
                  if (SelectBu ((int) LOWORD (msg.wParam))) continue;
				  if (SelectScroll ((int) LOWORD (msg.wParam))) continue;
			  }
			  else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
			  if (syskey == KEY5) break;
			  if (syskey == KEY12) break;
		  }
		  Close ();
}


void QMKOPF::Close (void)
/**
Fenster schliessen.
**/
{
	    if (hWnd)
		{
			     CloseControls (&TextForm);
			     CloseControls (&QmForm);
			     CloseControls (&UbForm);
			     CloseControls (&fscroll);
	             DestroyWindow (hWnd);
				 hWnd = NULL;
		}
}

void QMKOPF::Select (void)
/**
Ja oder Nein auf einer Spalte setzen.
**/
{
	    ColButton *CuBl;
		ColButton *CuBr;

		CuBl = (ColButton *) QmForm.mask[focustab[FRow][0]].feld; 
		CuBr = (ColButton *) QmForm.mask[focustab[FRow][1]].feld; 
		if (FCol == 0)
		{
			CuBl->bmp = kreuz;
			CuBr->bmp = NULL;
			Yes[FRow + scstart] = 1;
			No[FRow + scstart]  = 0;
		}
		else
		{
			CuBl->bmp = NULL;
			CuBr->bmp = kreuz;
			Yes[FRow + scstart] = 0;
			No[FRow + scstart]  = 1;
		}
		InvalidateRect (QmForm.mask[focustab[FRow][0]].feldid, NULL, TRUE);
		InvalidateRect (QmForm.mask[focustab[FRow][1]].feldid, NULL, TRUE);
		UpdateWindow   (QmForm.mask[focustab[FRow][0]].feldid);
		UpdateWindow   (QmForm.mask[focustab[FRow][1]].feldid);
        MessageBeep (0xFFFFFFFF);
}

BOOL QMKOPF::SelectBu (int Bu)
/**
Aktiviertes Fenster ermitteln.
**/
{
	     int i;
		 
		 for (i = 0; i < QmForm.fieldanz; i ++)
		 {
			 if (Bu == QmForm.mask[i].BuId) break;
		 }
		 if (i == QmForm.fieldanz) return FALSE;
   
         FRow = i / 3;
		 FCol = i % 3 - 1;
		 Select ();
		 return TRUE;
}

BOOL QMKOPF::SelectScroll (int Bu)
/**
Aktiviertes Fenster ermitteln.
**/
{
	     int i;
		 
		 for (i = 0; i < fscroll.fieldanz; i ++)
		 {
			 if (Bu == fscroll.mask[i].BuId) break;
		 }
		 if (i == fscroll.fieldanz) return FALSE;
		 
         switch (i)
		 {
		     case 0 :
			      PostMessage (hWnd, WM_KEYDOWN, (WPARAM) VK_PRIOR, 0l);
			      break;
		     case 1 :
			      PostMessage (hWnd, WM_KEYDOWN, (WPARAM) VK_NEXT, 0l);
			      break;
		 }
		 return TRUE;
}

void QMKOPF::FirstFocus (void)
/**
Focus auf 1. Button setzen.
**/
{
		FRow = 0;
        FCol = 0;
	    SetFocus (QmForm.mask[focustab[FRow][FCol]].feldid);
}

void QMKOPF::FocusDown (void)
/**
Focus auf 1. Button setzen.
**/
{
		if (FRow == MaxRowPos)
        {
               Scrolldown ();
               return;
        }
        FRow ++;
		if (FRow > MaxRowPos)
		{
		       FRow = 0;
		}
	    SetFocus (QmForm.mask[focustab[FRow][FCol]].feldid);
}

void QMKOPF::FocusUp (void)
/**
Focus auf 1. Button setzen.
**/
{
		if (FRow == 0)
        {
               Scrollup ();
               return;
        }
	    FRow --;
		if (FRow < 0)
		{
		       FRow = MaxColPos;
		}
	    SetFocus (QmForm.mask[focustab[FRow][FCol]].feldid);
}

void QMKOPF::FocusRight (void)
/**
Focus auf 1. Button setzen.
**/
{
	    FCol ++;
		if (FCol > MaxColPos)
		{
		       FCol = 0;
		}
	    SetFocus (QmForm.mask[focustab[FRow][FCol]].feldid);
}

void QMKOPF::FocusLeft (void)
/**
Focus auf 1. Button setzen.
**/
{
	    FCol --;
		if (FCol < 0)
		{
		       FCol = MaxColPos;
		}
	    SetFocus (QmForm.mask[focustab[FRow][FCol]].feldid);
}


void QMKOPF::Enter (void)
/**
Dialog starten.
**/
{
        scstart = 0;
	    SetColBu ();
	    Open ();
		FirstFocus ();
		ProcessMessages ();
}

CALLBACK QMKOPF::WndProc (HWND hWnd,UINT msg,
                             WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
				    display_form (hWnd, &UbForm,   0, 0);
					if (ScrollBar == FALSE && textanz > frmtanz)
					{
				              display_form (hWnd, &fscroll,  0, 0);
					}
				    display_form (hWnd, &TextForm, 0, 0);
				    display_form (hWnd, &QmForm,   0, 0);
                    hdc = BeginPaint (hWnd, &ps);
                    EndPaint (hWnd, &ps);
					break;
			  case WM_COMMAND :
				    PostMessage (hWnd, WM_COMMAND, wParam, lParam);
					return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

CALLBACK QMKOPF::ScWndProc (HWND hWnd,UINT msg,
                             WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
	                display_form (hWnd, &fscroll,  0, 0);
                    hdc = BeginPaint (hWnd, &ps);
                    EndPaint (hWnd, &ps);
					break;
			  case WM_COMMAND :
				    PostMessage (hWnd, WM_COMMAND, wParam, lParam);
					return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}
