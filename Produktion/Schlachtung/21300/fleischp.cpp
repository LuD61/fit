#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "fleischp.h"
#include "lief.h"
#include "schlre_k.h"

struct FLEISCHPRUEFRING fleischpruefring, fleischpruefring_null;
struct FLEISCH fleisch;
int lieferant_s;


void FLEISCHP_CLASS::prepare (void)
{

            ins_quest ((char *) fleischpruefring.lieferant, 0, 17);
            ins_quest ((char *) &fleischpruefring.dat, 2, 0);
    out_quest ((char *) &fleischpruefring.lfnr,2,0);
    out_quest ((char *) fleischpruefring.datum,0,11);
    out_quest ((char *) fleischpruefring.gattung,0,3);
    out_quest ((char *) fleischpruefring.taetowie,0,17);
    out_quest ((char *) fleischpruefring.schlachthof,0,17);
    out_quest ((char *) &fleischpruefring.qualiprogramm,1,0);
    out_quest ((char *) fleischpruefring.h_klasse,0,3);
    out_quest ((char *) &fleischpruefring.p_gruppe,2,0);
    out_quest ((char *) &fleischpruefring.netto_gew,3,0);
    out_quest ((char *) &fleischpruefring.speck_mass,3,0);
    out_quest ((char *) &fleischpruefring.fleis_mass,3,0);
    out_quest ((char *) &fleischpruefring.reflektion,2,0);
    out_quest ((char *) &fleischpruefring.mfa,3,0);
    out_quest ((char *) &fleischpruefring.ph_wert,3,0);
    out_quest ((char *) &fleischpruefring.beanst,2,0);
    out_quest ((char *) fleischpruefring.bemerkung,0,7);
    out_quest ((char *) fleischpruefring.bemerkungvl,0,11);
    out_quest ((char *) fleischpruefring.bemerkungth,0,11);
    out_quest ((char *) fleischpruefring.geschlecht,0,4);
    out_quest ((char *) fleischpruefring.beanstandung1,0,7);
    out_quest ((char *) fleischpruefring.beanstandung2,0,7);
    out_quest ((char *) fleischpruefring.beanstandung3,0,7);
    out_quest ((char *) fleischpruefring.beanstandung4,0,7);
    out_quest ((char *) fleischpruefring.beanstandung5,0,7);
    out_quest ((char *) &fleischpruefring.salmonellennr,3,0);
    out_quest ((char *) fleischpruefring.bemerkungv,0,11);
    out_quest ((char *) fleischpruefring.kun_kurz,0,7);
    out_quest ((char *) &fleischpruefring.mastb_nr,2,0);
    out_quest ((char *) fleischpruefring.mastb_name,0,17);
    out_quest ((char *) fleischpruefring.mastb_str,0,37);
    out_quest ((char *) fleischpruefring.mastb_plz,0,9);
    out_quest ((char *) fleischpruefring.mastb_ort,0,37);
    out_quest ((char *) fleischpruefring.mastb_balis,0,21);
    out_quest ((char *) fleischpruefring.lieferant,0,17);
    out_quest ((char *) fleischpruefring.lief_name,0,17);
    out_quest ((char *) fleischpruefring.lief_str,0,37);
    out_quest ((char *) fleischpruefring.lief_ort,0,37);
    out_quest ((char *) fleischpruefring.lief_balis,0,21);
    out_quest ((char *) &fleischpruefring.schlb_nr,2,0);
    out_quest ((char *) fleischpruefring.schlb_name,0,17);
    out_quest ((char *) fleischpruefring.schlb_str,0,37);
    out_quest ((char *) fleischpruefring.schlb_ort,0,37);
    out_quest ((char *) fleischpruefring.schlb_balis,0,21);
    out_quest ((char *) &fleischpruefring.sped_nr,2,0);
    out_quest ((char *) fleischpruefring.sped_name,0,17);
    out_quest ((char *) fleischpruefring.sped_str,0,37);
    out_quest ((char *) fleischpruefring.sped_ort,0,37);
    out_quest ((char *) fleischpruefring.sped_balis,0,21);
    out_quest ((char *) &fleischpruefring.kg_preis,3,0);
    out_quest ((char *) fleischpruefring.dateiname,0,33);
    out_quest ((char *) &fleischpruefring.dat,2,0);
    out_quest ((char *) &fleischpruefring.fit_ag,2,0);
    out_quest ((char *) &fleischpruefring.fit_a,3,0);
    out_quest ((char *) &fleischpruefring.fit_adr,2,0);
    out_quest ((char *) &fleischpruefring.fit_basis_pr,3,0);
    out_quest ((char *) &fleischpruefring.fit_preis,3,0);
    out_quest ((char *) &fleischpruefring.fit_bu,1,0);
    out_quest ((char *) &fleischpruefring.fit_unt,1,0);
    out_quest ((char *) &fleischpruefring.fit_sal,1,0);
    out_quest ((char *) &fleischpruefring.fit_leber,1,0);
    out_quest ((char *) &fleischpruefring.ein_nr,2,0);
    out_quest ((char *) &fleischpruefring.rech_nr,2,0);
    out_quest ((char *) fleischpruefring.err_txt,0,61);
    out_quest ((char *) &fleischpruefring.status,1,0);
            cursor = prepare_sql ("select "
"fleischpruefring.lfnr,  fleischpruefring.datum,  "
"fleischpruefring.gattung,  fleischpruefring.taetowie,  "
"fleischpruefring.schlachthof,  fleischpruefring.qualiprogramm,  "
"fleischpruefring.h_klasse,  fleischpruefring.p_gruppe,  "
"fleischpruefring.netto_gew,  fleischpruefring.speck_mass,  "
"fleischpruefring.fleis_mass,  fleischpruefring.reflektion,  "
"fleischpruefring.mfa,  fleischpruefring.ph_wert,  "
"fleischpruefring.beanst,  fleischpruefring.bemerkung,  "
"fleischpruefring.bemerkungvl,  fleischpruefring.bemerkungth,  "
"fleischpruefring.geschlecht,  fleischpruefring.beanstandung1,  "
"fleischpruefring.beanstandung2,  fleischpruefring.beanstandung3,  "
"fleischpruefring.beanstandung4,  fleischpruefring.beanstandung5,  "
"fleischpruefring.salmonellennr,  fleischpruefring.bemerkungv,  "
"fleischpruefring.kun_kurz,  fleischpruefring.mastb_nr,  "
"fleischpruefring.mastb_name,  fleischpruefring.mastb_str,  "
"fleischpruefring.mastb_plz,  fleischpruefring.mastb_ort,  "
"fleischpruefring.mastb_balis,  fleischpruefring.lieferant,  "
"fleischpruefring.lief_name,  fleischpruefring.lief_str,  "
"fleischpruefring.lief_ort,  fleischpruefring.lief_balis,  "
"fleischpruefring.schlb_nr,  fleischpruefring.schlb_name,  "
"fleischpruefring.schlb_str,  fleischpruefring.schlb_ort,  "
"fleischpruefring.schlb_balis,  fleischpruefring.sped_nr,  "
"fleischpruefring.sped_name,  fleischpruefring.sped_str,  "
"fleischpruefring.sped_ort,  fleischpruefring.sped_balis,  "
"fleischpruefring.kg_preis,  fleischpruefring.dateiname,  "
"fleischpruefring.dat,  fleischpruefring.fit_ag,  "
"fleischpruefring.fit_a,  fleischpruefring.fit_adr,  "
"fleischpruefring.fit_basis_pr,  fleischpruefring.fit_preis,  "
"fleischpruefring.fit_bu,  fleischpruefring.fit_unt,  "
"fleischpruefring.fit_sal,  fleischpruefring.fit_leber,  "
"fleischpruefring.ein_nr,  fleischpruefring.rech_nr,  "
"fleischpruefring.err_txt,  fleischpruefring.status from fleischpruefring "

#line 31 "fleischp.rpp"
                                  "where fleischpruefring.lieferant = ? "
                                  "and   fleischpruefring.dat = ? ");


 
    ins_quest ((char *) &fleischpruefring.lfnr,2,0);
    ins_quest ((char *) fleischpruefring.datum,0,11);
    ins_quest ((char *) fleischpruefring.gattung,0,3);
    ins_quest ((char *) fleischpruefring.taetowie,0,17);
    ins_quest ((char *) fleischpruefring.schlachthof,0,17);
    ins_quest ((char *) &fleischpruefring.qualiprogramm,1,0);
    ins_quest ((char *) fleischpruefring.h_klasse,0,3);
    ins_quest ((char *) &fleischpruefring.p_gruppe,2,0);
    ins_quest ((char *) &fleischpruefring.netto_gew,3,0);
    ins_quest ((char *) &fleischpruefring.speck_mass,3,0);
    ins_quest ((char *) &fleischpruefring.fleis_mass,3,0);
    ins_quest ((char *) &fleischpruefring.reflektion,2,0);
    ins_quest ((char *) &fleischpruefring.mfa,3,0);
    ins_quest ((char *) &fleischpruefring.ph_wert,3,0);
    ins_quest ((char *) &fleischpruefring.beanst,2,0);
    ins_quest ((char *) fleischpruefring.bemerkung,0,7);
    ins_quest ((char *) fleischpruefring.bemerkungvl,0,11);
    ins_quest ((char *) fleischpruefring.bemerkungth,0,11);
    ins_quest ((char *) fleischpruefring.geschlecht,0,4);
    ins_quest ((char *) fleischpruefring.beanstandung1,0,7);
    ins_quest ((char *) fleischpruefring.beanstandung2,0,7);
    ins_quest ((char *) fleischpruefring.beanstandung3,0,7);
    ins_quest ((char *) fleischpruefring.beanstandung4,0,7);
    ins_quest ((char *) fleischpruefring.beanstandung5,0,7);
    ins_quest ((char *) &fleischpruefring.salmonellennr,3,0);
    ins_quest ((char *) fleischpruefring.bemerkungv,0,11);
    ins_quest ((char *) fleischpruefring.kun_kurz,0,7);
    ins_quest ((char *) &fleischpruefring.mastb_nr,2,0);
    ins_quest ((char *) fleischpruefring.mastb_name,0,17);
    ins_quest ((char *) fleischpruefring.mastb_str,0,37);
    ins_quest ((char *) fleischpruefring.mastb_plz,0,9);
    ins_quest ((char *) fleischpruefring.mastb_ort,0,37);
    ins_quest ((char *) fleischpruefring.mastb_balis,0,21);
    ins_quest ((char *) fleischpruefring.lieferant,0,17);
    ins_quest ((char *) fleischpruefring.lief_name,0,17);
    ins_quest ((char *) fleischpruefring.lief_str,0,37);
    ins_quest ((char *) fleischpruefring.lief_ort,0,37);
    ins_quest ((char *) fleischpruefring.lief_balis,0,21);
    ins_quest ((char *) &fleischpruefring.schlb_nr,2,0);
    ins_quest ((char *) fleischpruefring.schlb_name,0,17);
    ins_quest ((char *) fleischpruefring.schlb_str,0,37);
    ins_quest ((char *) fleischpruefring.schlb_ort,0,37);
    ins_quest ((char *) fleischpruefring.schlb_balis,0,21);
    ins_quest ((char *) &fleischpruefring.sped_nr,2,0);
    ins_quest ((char *) fleischpruefring.sped_name,0,17);
    ins_quest ((char *) fleischpruefring.sped_str,0,37);
    ins_quest ((char *) fleischpruefring.sped_ort,0,37);
    ins_quest ((char *) fleischpruefring.sped_balis,0,21);
    ins_quest ((char *) &fleischpruefring.kg_preis,3,0);
    ins_quest ((char *) fleischpruefring.dateiname,0,33);
    ins_quest ((char *) &fleischpruefring.dat,2,0);
    ins_quest ((char *) &fleischpruefring.fit_ag,2,0);
    ins_quest ((char *) &fleischpruefring.fit_a,3,0);
    ins_quest ((char *) &fleischpruefring.fit_adr,2,0);
    ins_quest ((char *) &fleischpruefring.fit_basis_pr,3,0);
    ins_quest ((char *) &fleischpruefring.fit_preis,3,0);
    ins_quest ((char *) &fleischpruefring.fit_bu,1,0);
    ins_quest ((char *) &fleischpruefring.fit_unt,1,0);
    ins_quest ((char *) &fleischpruefring.fit_sal,1,0);
    ins_quest ((char *) &fleischpruefring.fit_leber,1,0);
    ins_quest ((char *) &fleischpruefring.ein_nr,2,0);
    ins_quest ((char *) &fleischpruefring.rech_nr,2,0);
    ins_quest ((char *) fleischpruefring.err_txt,0,61);
    ins_quest ((char *) &fleischpruefring.status,1,0);
             ins_cursor = prepare_sql ("insert into fleischpruefring ("
"lfnr,  datum,  gattung,  taetowie,  schlachthof,  qualiprogramm,  h_klasse,  "
"p_gruppe,  netto_gew,  speck_mass,  fleis_mass,  reflektion,  mfa,  ph_wert,  "
"beanst,  bemerkung,  bemerkungvl,  bemerkungth,  geschlecht,  beanstandung1,  "
"beanstandung2,  beanstandung3,  beanstandung4,  beanstandung5,  "
"salmonellennr,  bemerkungv,  kun_kurz,  mastb_nr,  mastb_name,  mastb_str,  "
"mastb_plz,  mastb_ort,  mastb_balis,  lieferant,  lief_name,  lief_str,  "
"lief_ort,  lief_balis,  schlb_nr,  schlb_name,  schlb_str,  schlb_ort,  "
"schlb_balis,  sped_nr,  sped_name,  sped_str,  sped_ort,  sped_balis,  "
"kg_preis,  dateiname,  dat,  fit_ag,  fit_a,  fit_adr,  fit_basis_pr,  fit_preis,  "
"fit_bu,  fit_unt,  fit_sal,  fit_leber,  ein_nr,  rech_nr,  err_txt,  status) "

#line 37 "fleischp.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 39 "fleischp.rpp"
           

}
void FLEISCHP_CLASS::preparesum (void)
{
// 310112 and (fl.fit_preis <> 0 or fit_unt = 1)    nur S�tze mit Preis sollen in die Abrechnung rein, es sei denn das Tier ist UNT
            char *sqltext;
            out_quest ((char *) fleischpruefring.lieferant, 0, 17);
            out_quest ((char *) lief.lief, 0, 17);
            out_quest ((char *) &fleischpruefring.dat, 2, 0);
            out_quest ((char *) &fleischpruefring.status, 1, 0);
            out_quest ((char *) fleischpruefring.gattung, 0, 3);
            out_quest ((char *) &fleisch.anzahl, 2, 0);
            out_quest ((char *) &fleisch.anzahlQS, 3, 0);
            out_quest ((char *) &fleisch.anzahlGQ, 3, 0);
            out_quest ((char *) &fleischpruefring.kg_preis, 3, 0);
            out_quest ((char *) &fleischpruefring.fit_bu, 1, 0);
            out_quest ((char *) &fleischpruefring.fit_unt, 1, 0);
            out_quest ((char *) &fleischpruefring.fit_sal, 1, 0);
            out_quest ((char *) &fleischpruefring.fit_leber, 1, 0);
            sqltext = "select fl.lieferant, lief.lief, fl.dat,sum(case when fl.status = 1 then 0 else status end) status, "
			"case when fl.gattung = 'SW' then 'SW' else 'GV' end gattung , "
			"  count(*) Anz, "
			"sum (case when qualiprogramm == 9 then qualiprogramm  /9 else 0 end) QS, "
	        "sum (case when qualiprogramm == 14 then qualiprogramm  /14 else 0 end) GQ, "
			"round(sum(kg_preis * netto_gew ) / sum(netto_gew),2), "
			"sum(fl.fit_bu), "
			"sum(fl.fit_unt), "
			"sum(fl.fit_sal), "
			"sum(fl.fit_leber) "
    		"from fleischpruefring fl, outer lief "
			"where fl.lieferant = lief.lief "
			"and status <= 4 "
			"and (fl.fit_preis <> 0 or fit_unt = 1) " 
			"group by 1,2,3,5 "
			"order by status,fl.dat,fl.lieferant" ;
            cu_fleischpsum = prepare_sql (sqltext);
            
            ins_quest ((char *) &_mdn.mdn, 1, 0);
            ins_quest ((char *) &lieferant_s, 2, 0);
            out_quest ((char *) _adr.adr_krz, 0, 17);
            out_quest ((char *) &lief.hdklpargr, 1, 0);
	        sqltext = "select  adr.adr_krz , lief.hdklpargr from lief,adr "
			"where lief.mdn = ? and lief.lief_s = ? and adr.adr = lief.adr ";
            cu_liefbez = prepare_sql (sqltext);
            
            sqltext = "update fleischpruefring set status = 4, ein_nr = ? where lieferant = ? and dat = ?" ;
            ins_quest ((char *) &schlre_k.ein_nr, 2, 0);
            ins_quest ((char *) fleischpruefring.lieferant, 0, 17);
            ins_quest ((char *) &fleischpruefring.dat, 2, 0);
            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &fleischpruefring.lfnr,2,0);
    ins_quest ((char *) fleischpruefring.datum,0,11);
    ins_quest ((char *) fleischpruefring.gattung,0,3);
    ins_quest ((char *) fleischpruefring.taetowie,0,17);
    ins_quest ((char *) fleischpruefring.schlachthof,0,17);
    ins_quest ((char *) &fleischpruefring.qualiprogramm,1,0);
    ins_quest ((char *) fleischpruefring.h_klasse,0,3);
    ins_quest ((char *) &fleischpruefring.p_gruppe,2,0);
    ins_quest ((char *) &fleischpruefring.netto_gew,3,0);
    ins_quest ((char *) &fleischpruefring.speck_mass,3,0);
    ins_quest ((char *) &fleischpruefring.fleis_mass,3,0);
    ins_quest ((char *) &fleischpruefring.reflektion,2,0);
    ins_quest ((char *) &fleischpruefring.mfa,3,0);
    ins_quest ((char *) &fleischpruefring.ph_wert,3,0);
    ins_quest ((char *) &fleischpruefring.beanst,2,0);
    ins_quest ((char *) fleischpruefring.bemerkung,0,7);
    ins_quest ((char *) fleischpruefring.bemerkungvl,0,11);
    ins_quest ((char *) fleischpruefring.bemerkungth,0,11);
    ins_quest ((char *) fleischpruefring.geschlecht,0,4);
    ins_quest ((char *) fleischpruefring.beanstandung1,0,7);
    ins_quest ((char *) fleischpruefring.beanstandung2,0,7);
    ins_quest ((char *) fleischpruefring.beanstandung3,0,7);
    ins_quest ((char *) fleischpruefring.beanstandung4,0,7);
    ins_quest ((char *) fleischpruefring.beanstandung5,0,7);
    ins_quest ((char *) &fleischpruefring.salmonellennr,3,0);
    ins_quest ((char *) fleischpruefring.bemerkungv,0,11);
    ins_quest ((char *) fleischpruefring.kun_kurz,0,7);
    ins_quest ((char *) &fleischpruefring.mastb_nr,2,0);
    ins_quest ((char *) fleischpruefring.mastb_name,0,17);
    ins_quest ((char *) fleischpruefring.mastb_str,0,37);
    ins_quest ((char *) fleischpruefring.mastb_plz,0,9);
    ins_quest ((char *) fleischpruefring.mastb_ort,0,37);
    ins_quest ((char *) fleischpruefring.mastb_balis,0,21);
    ins_quest ((char *) fleischpruefring.lieferant,0,17);
    ins_quest ((char *) fleischpruefring.lief_name,0,17);
    ins_quest ((char *) fleischpruefring.lief_str,0,37);
    ins_quest ((char *) fleischpruefring.lief_ort,0,37);
    ins_quest ((char *) fleischpruefring.lief_balis,0,21);
    ins_quest ((char *) &fleischpruefring.schlb_nr,2,0);
    ins_quest ((char *) fleischpruefring.schlb_name,0,17);
    ins_quest ((char *) fleischpruefring.schlb_str,0,37);
    ins_quest ((char *) fleischpruefring.schlb_ort,0,37);
    ins_quest ((char *) fleischpruefring.schlb_balis,0,21);
    ins_quest ((char *) &fleischpruefring.sped_nr,2,0);
    ins_quest ((char *) fleischpruefring.sped_name,0,17);
    ins_quest ((char *) fleischpruefring.sped_str,0,37);
    ins_quest ((char *) fleischpruefring.sped_ort,0,37);
    ins_quest ((char *) fleischpruefring.sped_balis,0,21);
    ins_quest ((char *) &fleischpruefring.kg_preis,3,0);
    ins_quest ((char *) fleischpruefring.dateiname,0,33);
    ins_quest ((char *) &fleischpruefring.dat,2,0);
    ins_quest ((char *) &fleischpruefring.fit_ag,2,0);
    ins_quest ((char *) &fleischpruefring.fit_a,3,0);
    ins_quest ((char *) &fleischpruefring.fit_adr,2,0);
    ins_quest ((char *) &fleischpruefring.fit_basis_pr,3,0);
    ins_quest ((char *) &fleischpruefring.fit_preis,3,0);
    ins_quest ((char *) &fleischpruefring.fit_bu,1,0);
    ins_quest ((char *) &fleischpruefring.fit_unt,1,0);
    ins_quest ((char *) &fleischpruefring.fit_sal,1,0);
    ins_quest ((char *) &fleischpruefring.fit_leber,1,0);
    ins_quest ((char *) &fleischpruefring.ein_nr,2,0);
    ins_quest ((char *) &fleischpruefring.rech_nr,2,0);
    ins_quest ((char *) fleischpruefring.err_txt,0,61);
    ins_quest ((char *) &fleischpruefring.status,1,0);
             ins_cursorfl = prepare_sql ("insert into fleischpruefring ("
"lfnr,  datum,  gattung,  taetowie,  schlachthof,  qualiprogramm,  h_klasse,  "
"p_gruppe,  netto_gew,  speck_mass,  fleis_mass,  reflektion,  mfa,  ph_wert,  "
"beanst,  bemerkung,  bemerkungvl,  bemerkungth,  geschlecht,  beanstandung1,  "
"beanstandung2,  beanstandung3,  beanstandung4,  beanstandung5,  "
"salmonellennr,  bemerkungv,  kun_kurz,  mastb_nr,  mastb_name,  mastb_str,  "
"mastb_plz,  mastb_ort,  mastb_balis,  lieferant,  lief_name,  lief_str,  "
"lief_ort,  lief_balis,  schlb_nr,  schlb_name,  schlb_str,  schlb_ort,  "
"schlb_balis,  sped_nr,  sped_name,  sped_str,  sped_ort,  sped_balis,  "
"kg_preis,  dateiname,  dat,  fit_ag,  fit_a,  fit_adr,  fit_basis_pr,  fit_preis,  "
"fit_bu,  fit_unt,  fit_sal,  fit_leber,  ein_nr,  rech_nr,  err_txt,  status) "

#line 92 "fleischp.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 94 "fleischp.rpp"



//310112 and (fleischpruefring.fit_preis <> 0 or fleischpruefring.fit_unt = 1)   Nur S�tze mit Preis oder UNT-Tiere in schlre_p rein 
            ins_quest ((char *) fleischpruefring.lieferant, 0, 17);
            ins_quest ((char *) &fleischpruefring.dat, 2, 0);
            out_quest ((char *) &fleischpruefring.fit_a, 3, 0);
            out_quest ((char *) fleischpruefring.err_txt, 0, 60);
            out_quest ((char *) &fleischpruefring.lfnr, 2, 0);
            out_quest ((char *) &fleischpruefring.netto_gew, 3, 0);
            out_quest ((char *) &fleischpruefring.mfa, 3, 0);
            out_quest ((char *) &fleischpruefring.fit_basis_pr, 3, 0);
            out_quest ((char *) &fleischpruefring.fit_preis, 3, 0);
            out_quest ((char *) &fleischpruefring.sped_nr, 2, 0);
            out_quest ((char *) &fleischpruefring.fit_bu, 1, 0);
            out_quest ((char *) &fleischpruefring.fit_unt, 1, 0);
            out_quest ((char *) &fleischpruefring.fit_sal, 1, 0);
            out_quest ((char *) &fleischpruefring.fit_leber, 1, 0);
            cursorfl = prepare_sql ("select fit_a,err_txt,max(lfnr),sum(case when fit_unt = 0 then netto_gew else 0 end),sum(mfa * netto_gew) / sum(netto_gew), "
									"sum(case when fit_unt = 0 then round(fit_basis_pr * netto_gew,2) else 0 end) , "
									"sum(case when fit_unt = 0 then round(fit_preis * netto_gew,2) else 0 end) , "
								    "count(*), sum(fit_bu), sum(fit_unt), sum(fit_sal), sum(fit_leber)  from fleischpruefring "
                                  "where fleischpruefring.lieferant = ? "
                                  "and   fleischpruefring.dat = ? "
								  "and (fleischpruefring.fit_preis <> 0 or fleischpruefring.fit_unt = 1) " 
								  " group by 1,2 order by 1");
			/***
            cursorfl = prepare_sql ("select fit_a,err_txt,max(lfnr),sum(netto_gew),sum(mfa * netto_gew) / sum(netto_gew), "
									"sum(fit_basis_pr * netto_gew) / sum(netto_gew), "
									"sum(fit_preis * netto_gew) / sum(netto_gew), "
								    "count(*), sum(fit_bu), sum(fit_unt), sum(fit_sal), sum(fit_leber)  from fleischpruefring "
                                  "where fleischpruefring.lieferant = ? "
                                  "and   fleischpruefring.dat = ? " 
								  " group by 1,2 order by 1");
								  *****/
}

int FLEISCHP_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
		 strcpy(fleischpruefring.lieferant,clipped(fleischpruefring.lieferant));
         if (cursorfl < 1)
         {
                this->preparesum ();
         }
         open_sql (cursorfl);
         fetch_sql (cursorfl);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}
int FLEISCHP_CLASS::dbread (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
		 strcpy(fleischpruefring.lieferant,clipped(fleischpruefring.lieferant));
         fetch_sql (cursorfl);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int FLEISCHP_CLASS::dbupdate (void)
/**
**/
{
         if (upd_cursor < 1)
         {
                this->preparesum ();
         }
         execute_curs (upd_cursor);
         return sqlstatus;
}
int FLEISCHP_CLASS::dbinsert (void)
/**
Insert in we_wiepro
**/
{
         if (ins_cursorfl <= 0)
         {
                     preparesum ();
         }
         execute_curs (ins_cursorfl);
         return sqlstatus;
}
int FLEISCHP_CLASS::lese_sum (void)
/**
summiertes Lesen zum Anzeigen
**/
{
         if (cu_fleischpsum == -1)
         {
                this->preparesum ();
         }
	     strcpy(_adr.adr_krz,""); 
		 lief.hdklpargr = 0;
         fetch_sql (cu_fleischpsum);
         if (sqlstatus == 0)
         {	
		        lieferant_s = atoi(fleischpruefring.lieferant);
		        open_sql (cu_liefbez);
		        fetch_sql (cu_liefbez);
                return 0;
         }
         return 100;
}
void FLEISCHP_CLASS::close_sum (void)
/**
summiertes Lesen schlie�en
**/
{
         if (cu_fleischpsum == -1) return;

         close_sql (cu_fleischpsum);
         close_sql (cu_liefbez);
         cu_fleischpsum = -1;
         cu_liefbez = -1;
}

void FLEISCHP_CLASS::all_out_quest (void)
{  
    out_quest ((char *) &fleischpruefring.lfnr,2,0);
    out_quest ((char *) fleischpruefring.datum,0,11);
    out_quest ((char *) fleischpruefring.gattung,0,3);
    out_quest ((char *) fleischpruefring.taetowie,0,17);
    out_quest ((char *) fleischpruefring.schlachthof,0,17);
    out_quest ((char *) &fleischpruefring.qualiprogramm,1,0);
    out_quest ((char *) fleischpruefring.h_klasse,0,3);
    out_quest ((char *) &fleischpruefring.p_gruppe,2,0);
    out_quest ((char *) &fleischpruefring.netto_gew,3,0);
    out_quest ((char *) &fleischpruefring.speck_mass,3,0);
    out_quest ((char *) &fleischpruefring.fleis_mass,3,0);
    out_quest ((char *) &fleischpruefring.reflektion,2,0);
    out_quest ((char *) &fleischpruefring.mfa,3,0);
    out_quest ((char *) &fleischpruefring.ph_wert,3,0);
    out_quest ((char *) &fleischpruefring.beanst,2,0);
    out_quest ((char *) fleischpruefring.bemerkung,0,7);
    out_quest ((char *) fleischpruefring.bemerkungvl,0,11);
    out_quest ((char *) fleischpruefring.bemerkungth,0,11);
    out_quest ((char *) fleischpruefring.geschlecht,0,4);
    out_quest ((char *) fleischpruefring.beanstandung1,0,7);
    out_quest ((char *) fleischpruefring.beanstandung2,0,7);
    out_quest ((char *) fleischpruefring.beanstandung3,0,7);
    out_quest ((char *) fleischpruefring.beanstandung4,0,7);
    out_quest ((char *) fleischpruefring.beanstandung5,0,7);
    out_quest ((char *) &fleischpruefring.salmonellennr,3,0);
    out_quest ((char *) fleischpruefring.bemerkungv,0,11);
    out_quest ((char *) fleischpruefring.kun_kurz,0,7);
    out_quest ((char *) &fleischpruefring.mastb_nr,2,0);
    out_quest ((char *) fleischpruefring.mastb_name,0,17);
    out_quest ((char *) fleischpruefring.mastb_str,0,37);
    out_quest ((char *) fleischpruefring.mastb_plz,0,9);
    out_quest ((char *) fleischpruefring.mastb_ort,0,37);
    out_quest ((char *) fleischpruefring.mastb_balis,0,21);
    out_quest ((char *) fleischpruefring.lieferant,0,17);
    out_quest ((char *) fleischpruefring.lief_name,0,17);
    out_quest ((char *) fleischpruefring.lief_str,0,37);
    out_quest ((char *) fleischpruefring.lief_ort,0,37);
    out_quest ((char *) fleischpruefring.lief_balis,0,21);
    out_quest ((char *) &fleischpruefring.schlb_nr,2,0);
    out_quest ((char *) fleischpruefring.schlb_name,0,17);
    out_quest ((char *) fleischpruefring.schlb_str,0,37);
    out_quest ((char *) fleischpruefring.schlb_ort,0,37);
    out_quest ((char *) fleischpruefring.schlb_balis,0,21);
    out_quest ((char *) &fleischpruefring.sped_nr,2,0);
    out_quest ((char *) fleischpruefring.sped_name,0,17);
    out_quest ((char *) fleischpruefring.sped_str,0,37);
    out_quest ((char *) fleischpruefring.sped_ort,0,37);
    out_quest ((char *) fleischpruefring.sped_balis,0,21);
    out_quest ((char *) &fleischpruefring.kg_preis,3,0);
    out_quest ((char *) fleischpruefring.dateiname,0,33);
    out_quest ((char *) &fleischpruefring.dat,2,0);
    out_quest ((char *) &fleischpruefring.fit_ag,2,0);
    out_quest ((char *) &fleischpruefring.fit_a,3,0);
    out_quest ((char *) &fleischpruefring.fit_adr,2,0);
    out_quest ((char *) &fleischpruefring.fit_basis_pr,3,0);
    out_quest ((char *) &fleischpruefring.fit_preis,3,0);
    out_quest ((char *) &fleischpruefring.fit_bu,1,0);
    out_quest ((char *) &fleischpruefring.fit_unt,1,0);
    out_quest ((char *) &fleischpruefring.fit_sal,1,0);
    out_quest ((char *) &fleischpruefring.fit_leber,1,0);
    out_quest ((char *) &fleischpruefring.ein_nr,2,0);
    out_quest ((char *) &fleischpruefring.rech_nr,2,0);
    out_quest ((char *) fleischpruefring.err_txt,0,61);
    out_quest ((char *) &fleischpruefring.status,1,0);
     char *felder = "  fleischpruefring.lfnr,  "
"fleischpruefring.datum,  fleischpruefring.gattung,  "
"fleischpruefring.taetowie,  fleischpruefring.schlachthof,  "
"fleischpruefring.qualiprogramm,  fleischpruefring.h_klasse,  "
"fleischpruefring.p_gruppe,  fleischpruefring.netto_gew,  "
"fleischpruefring.speck_mass,  fleischpruefring.fleis_mass,  "
"fleischpruefring.reflektion,  fleischpruefring.mfa,  "
"fleischpruefring.ph_wert,  fleischpruefring.beanst,  "
"fleischpruefring.bemerkung,  fleischpruefring.bemerkungvl,  "
"fleischpruefring.bemerkungth,  fleischpruefring.geschlecht,  "
"fleischpruefring.beanstandung1,  fleischpruefring.beanstandung2,  "
"fleischpruefring.beanstandung3,  fleischpruefring.beanstandung4,  "
"fleischpruefring.beanstandung5,  fleischpruefring.salmonellennr,  "
"fleischpruefring.bemerkungv,  fleischpruefring.kun_kurz,  "
"fleischpruefring.mastb_nr,  fleischpruefring.mastb_name,  "
"fleischpruefring.mastb_str,  fleischpruefring.mastb_plz,  "
"fleischpruefring.mastb_ort,  fleischpruefring.mastb_balis,  "
"fleischpruefring.lieferant,  fleischpruefring.lief_name,  "
"fleischpruefring.lief_str,  fleischpruefring.lief_ort,  "
"fleischpruefring.lief_balis,  fleischpruefring.schlb_nr,  "
"fleischpruefring.schlb_name,  fleischpruefring.schlb_str,  "
"fleischpruefring.schlb_ort,  fleischpruefring.schlb_balis,  "
"fleischpruefring.sped_nr,  fleischpruefring.sped_name,  "
"fleischpruefring.sped_str,  fleischpruefring.sped_ort,  "
"fleischpruefring.sped_balis,  fleischpruefring.kg_preis,  "
"fleischpruefring.dateiname,  fleischpruefring.dat,  "
"fleischpruefring.fit_ag,  fleischpruefring.fit_a,  "
"fleischpruefring.fit_adr,  fleischpruefring.fit_basis_pr,  "
"fleischpruefring.fit_preis,  fleischpruefring.fit_bu,  "
"fleischpruefring.fit_unt,  fleischpruefring.fit_sal,  "
"fleischpruefring.fit_leber,  fleischpruefring.ein_nr,  "
"fleischpruefring.rech_nr,  fleischpruefring.err_txt,  "
"fleischpruefring.status " ;

#line 223 "fleischp.rpp"
}

