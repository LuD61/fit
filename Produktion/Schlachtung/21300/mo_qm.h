#ifndef _MO_QM_DEF
#define _MO_QM_DEF
#include "wmask.h"

class COBTAB
{
       private :
		   ColButton BuText;
		   ColButton BuYes;
		   ColButton BuNo;
	   public :
		   COBTAB () {}
		   ColButton *GetText (void)
		   {
			   return &BuText;
		   }
		   ColButton *GetYes (void)
		   {
			   return &BuYes;
		   }
		   ColButton *GetNo (void)
		   {
			   return &BuNo;
		   }
		   void SetText (ColButton *Cub)
		   {
			   memcpy (&BuText, Cub, sizeof (ColButton));
		   }
		   void SetYes (ColButton *Cub)
		   {
			   memcpy (&BuYes, Cub, sizeof (ColButton));
		   }
		   void SetNo (ColButton *Cub)
		   {

               memcpy (&BuNo, Cub, sizeof (ColButton));
		   }
};

class QMKOPF 
{
       private :
		    HINSTANCE hInstance;
			HWND hMainWindow;
			HWND hWnd;
			BOOL registered;
			char *lpszClassName;
			char *lpszScClassName;
			COBTAB *CoB;
            HBITMAP kreuz;
			int FRow;
			int FCol;
			int Offset;
            double  scrfx;
            double  scrfy;
            int MaxRowPos;
            int MaxColPos;
            char **Texte;
            int *Yes;   
            int *No;
			char **Ub;
			int scstart;
			int scend;
			DWORD style;
			DWORD Exstyle;
			HWND  ScrollhWnd;

       public :
           QMKOPF (HINSTANCE, HWND);
		   ~QMKOPF ();

		   void SethMainWnd (HWND hMainWnd)
		   {
	                   this->hMainWindow = hMainWnd;
		   }

		   void SetDevice (double cx, double cy)
		   {
			   scrfx = cx;
			   scrfy = cy;
		   }

		   int GetTextanz (void);
		   void Setfeldanz (void);
           void SetParams (char **, char **, int *, int *, DWORD);
           void SetParamsEx (char **, char **, int *, int *, DWORD, DWORD, BOOL);
           void KorrScroll (void);
           void KorrScrollhWnd (void);
		   void FillBu0 (int);
           void FillQmForm0 (int);
		   void FillBu (int);
           void FillQmForm (int);
           void SetColBu (void);
		   void Register (void);
		   void ProcessMessages (void);
		   void OpenScrollBar (void);
		   void Open  (void);
		   void Close (void);
		   void Select (void);
		   BOOL SelectBu (int);
		   BOOL SelectScroll (int);
		   void Display (void);
		   void Scrollup (void);
		   void Scrolldown (void);
           void FocusRight (void);
           void FocusLeft (void);
           void FocusDown (void);
           void FocusUp (void);
           void FirstFocus (void);
		   void Enter (void);
           static CALLBACK WndProc(HWND,UINT, WPARAM,LPARAM);
           static CALLBACK ScWndProc(HWND,UINT, WPARAM,LPARAM);
};

#endif