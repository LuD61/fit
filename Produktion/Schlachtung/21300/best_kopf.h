#ifndef _BEST_KOPF_DEF
#define _BEST_KOPF_DEF

#include "dbclass.h"

struct BEST_KOPF {
   short     bearb_stat;
   long      best_blg;
   long      best_term;
   long      best_txt;
   short     fil;
   long      lfd;
   char      lief[17];
   long      lief_s;
   long      lief_term;
   short     mdn;
   long      we_dat;
   char      pers_nam[9];
   long      fil_adr;
   long      lief_adr;
   char      lieferzeit[6];
};
extern struct BEST_KOPF best_kopf, best_kopf_null;

#line 7 "best_kopf.rh"

class BEST_KOPF_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               BEST_KOPF_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
