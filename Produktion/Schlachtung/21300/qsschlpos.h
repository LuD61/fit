#ifndef _QSSCHLPOS_DEF
#define _QSSCHLPOS_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct QSSCHLPOS {
   short     mdn;
   short     fil;
   long      lfd;
   long      ein_nr;
   char      lief[17];
   double    a;
   long      schlacht_nr;
   char      txt[61];
   short     wrt;
   double    temp;
   double    ph_wert;
   long      dat;
   char      pers_nam[9];
};
extern struct QSSCHLPOS qsschlpos, qsschlpos_null;

#line 10 "qsschlpos.rh"

class QSSCHLPOS_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               QSSCHLPOS_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
