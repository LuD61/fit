#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "schlre_k.h"

struct SCHLRE_K schlre_k, schlre_k_null;

void SCHLRE_K_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &schlre_k.mdn, 1, 0);
            ins_quest ((char *) &schlre_k.linie, 1, 0);
            ins_quest ((char *) &schlre_k.lief_s, 2, 0);
            ins_quest ((char *) &schlre_k.kun, 2, 0);
            ins_quest ((char *) &schlre_k.lief_dat, 2, 0);
            ins_quest ((char *) &schlre_k.schl_art, 1, 0);
    out_quest ((char *) &schlre_k.mdn,1,0);
    out_quest ((char *) &schlre_k.rech_nr,2,0);
    out_quest ((char *) &schlre_k.stat,1,0);
    out_quest ((char *) &schlre_k.ein_nr,2,0);
    out_quest ((char *) &schlre_k.part_nr,2,0);
    out_quest ((char *) &schlre_k.text_nr,2,0);
    out_quest ((char *) &schlre_k.rech_dat,2,0);
    out_quest ((char *) &schlre_k.lief_dat,2,0);
    out_quest ((char *) &schlre_k.lief_s,2,0);
    out_quest ((char *) &schlre_k.kreditor,2,0);
    out_quest ((char *) &schlre_k.regul,2,0);
    out_quest ((char *) &schlre_k.zahl_kond,1,0);
    out_quest ((char *) &schlre_k.eink,2,0);
    out_quest ((char *) &schlre_k.schl_art,1,0);
    out_quest ((char *) &schlre_k.basispr,3,0);
    out_quest ((char *) &schlre_k.netto_betr,3,0);
    out_quest ((char *) &schlre_k.gesamt_betr,3,0);
    out_quest ((char *) &schlre_k.mwst,1,0);
    out_quest ((char *) &schlre_k.mwst_wert,3,0);
    out_quest ((char *) &schlre_k.linie,1,0);
    out_quest ((char *) &schlre_k.schein_mwst,1,0);
    out_quest ((char *) &schlre_k.verrechnet,3,0);
    out_quest ((char *) &schlre_k.abzug_proz,3,0);
    out_quest ((char *) &schlre_k.kun,2,0);
            cursor = prepare_sql ("select schlre_k.mdn,  "
"schlre_k.rech_nr,  schlre_k.stat,  schlre_k.ein_nr,  schlre_k.part_nr,  "
"schlre_k.text_nr,  schlre_k.rech_dat,  schlre_k.lief_dat,  "
"schlre_k.lief_s,  schlre_k.kreditor,  schlre_k.regul,  "
"schlre_k.zahl_kond,  schlre_k.eink,  schlre_k.schl_art,  "
"schlre_k.basispr,  schlre_k.netto_betr,  schlre_k.gesamt_betr,  "
"schlre_k.mwst,  schlre_k.mwst_wert,  schlre_k.linie,  "
"schlre_k.schein_mwst,  schlre_k.verrechnet,  schlre_k.abzug_proz,  "
"schlre_k.kun from schlre_k "

#line 28 "schlre_k.rpp"
                                  "where mdn = ? "
                                  "and   linie = ? "
                                  "and   lief_s = ? "
                                  "and   kun = ? "
                                  "and   lief_dat = ? "
                                  "and   schl_art = ? ");

    ins_quest ((char *) &schlre_k.mdn,1,0);
    ins_quest ((char *) &schlre_k.rech_nr,2,0);
    ins_quest ((char *) &schlre_k.stat,1,0);
    ins_quest ((char *) &schlre_k.ein_nr,2,0);
    ins_quest ((char *) &schlre_k.part_nr,2,0);
    ins_quest ((char *) &schlre_k.text_nr,2,0);
    ins_quest ((char *) &schlre_k.rech_dat,2,0);
    ins_quest ((char *) &schlre_k.lief_dat,2,0);
    ins_quest ((char *) &schlre_k.lief_s,2,0);
    ins_quest ((char *) &schlre_k.kreditor,2,0);
    ins_quest ((char *) &schlre_k.regul,2,0);
    ins_quest ((char *) &schlre_k.zahl_kond,1,0);
    ins_quest ((char *) &schlre_k.eink,2,0);
    ins_quest ((char *) &schlre_k.schl_art,1,0);
    ins_quest ((char *) &schlre_k.basispr,3,0);
    ins_quest ((char *) &schlre_k.netto_betr,3,0);
    ins_quest ((char *) &schlre_k.gesamt_betr,3,0);
    ins_quest ((char *) &schlre_k.mwst,1,0);
    ins_quest ((char *) &schlre_k.mwst_wert,3,0);
    ins_quest ((char *) &schlre_k.linie,1,0);
    ins_quest ((char *) &schlre_k.schein_mwst,1,0);
    ins_quest ((char *) &schlre_k.verrechnet,3,0);
    ins_quest ((char *) &schlre_k.abzug_proz,3,0);
    ins_quest ((char *) &schlre_k.kun,2,0);
            sqltext = "update schlre_k set schlre_k.mdn = ?,  "
"schlre_k.rech_nr = ?,  schlre_k.stat = ?,  schlre_k.ein_nr = ?,  "
"schlre_k.part_nr = ?,  schlre_k.text_nr = ?,  "
"schlre_k.rech_dat = ?,  schlre_k.lief_dat = ?,  "
"schlre_k.lief_s = ?,  schlre_k.kreditor = ?,  schlre_k.regul = ?,  "
"schlre_k.zahl_kond = ?,  schlre_k.eink = ?,  schlre_k.schl_art = ?,  "
"schlre_k.basispr = ?,  schlre_k.netto_betr = ?,  "
"schlre_k.gesamt_betr = ?,  schlre_k.mwst = ?,  "
"schlre_k.mwst_wert = ?,  schlre_k.linie = ?,  "
"schlre_k.schein_mwst = ?,  schlre_k.verrechnet = ?,  "
"schlre_k.abzug_proz = ?,  schlre_k.kun = ? "

#line 36 "schlre_k.rpp"
                                  "where mdn = ? "
                                  "and   linie = ? "
                                  "and   lief_s = ? "
                                  "and   kun = ? "
                                  "and   lief_dat = ? "
                                  "and   schl_art = ? ";

            ins_quest ((char *) &schlre_k.mdn, 1, 0);
            ins_quest ((char *) &schlre_k.linie, 1, 0);
            ins_quest ((char *) &schlre_k.lief_s, 2, 0);
            ins_quest ((char *) &schlre_k.kun, 2, 0);
            ins_quest ((char *) &schlre_k.lief_dat, 2, 0);
            ins_quest ((char *) &schlre_k.schl_art, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &schlre_k.mdn, 1, 0);
            ins_quest ((char *) &schlre_k.linie, 1, 0);
            ins_quest ((char *) &schlre_k.lief_s, 2, 0);
            ins_quest ((char *) &schlre_k.kun, 2, 0);
            ins_quest ((char *) &schlre_k.lief_dat, 2, 0);
            ins_quest ((char *) &schlre_k.schl_art, 1, 0);
            test_upd_cursor = prepare_sql ("select ein_nr from schlre_k "
                                  "where mdn = ? "
                                  "and   linie = ? "
                                  "and   lief_s = ? "
                                  "and   kun = ? "
                                  "and   lief_dat = ? "
                                  "and   schl_art = ? ");
            ins_quest ((char *) &schlre_k.mdn, 1, 0);
            ins_quest ((char *) &schlre_k.linie, 1, 0);
            ins_quest ((char *) &schlre_k.lief_s, 2, 0);
            ins_quest ((char *) &schlre_k.kun, 2, 0);
            ins_quest ((char *) &schlre_k.lief_dat, 2, 0);
            ins_quest ((char *) &schlre_k.schl_art, 1, 0);
            del_cursor = prepare_sql ("delete from schlre_k "
                                  "where mdn = ? "
                                  "and   linie = ? "
                                  "and   lief_s = ? "
                                  "and   kun = ? "
                                  "and   lief_dat = ? "
                                  "and   schl_art = ? ");
    ins_quest ((char *) &schlre_k.mdn,1,0);
    ins_quest ((char *) &schlre_k.rech_nr,2,0);
    ins_quest ((char *) &schlre_k.stat,1,0);
    ins_quest ((char *) &schlre_k.ein_nr,2,0);
    ins_quest ((char *) &schlre_k.part_nr,2,0);
    ins_quest ((char *) &schlre_k.text_nr,2,0);
    ins_quest ((char *) &schlre_k.rech_dat,2,0);
    ins_quest ((char *) &schlre_k.lief_dat,2,0);
    ins_quest ((char *) &schlre_k.lief_s,2,0);
    ins_quest ((char *) &schlre_k.kreditor,2,0);
    ins_quest ((char *) &schlre_k.regul,2,0);
    ins_quest ((char *) &schlre_k.zahl_kond,1,0);
    ins_quest ((char *) &schlre_k.eink,2,0);
    ins_quest ((char *) &schlre_k.schl_art,1,0);
    ins_quest ((char *) &schlre_k.basispr,3,0);
    ins_quest ((char *) &schlre_k.netto_betr,3,0);
    ins_quest ((char *) &schlre_k.gesamt_betr,3,0);
    ins_quest ((char *) &schlre_k.mwst,1,0);
    ins_quest ((char *) &schlre_k.mwst_wert,3,0);
    ins_quest ((char *) &schlre_k.linie,1,0);
    ins_quest ((char *) &schlre_k.schein_mwst,1,0);
    ins_quest ((char *) &schlre_k.verrechnet,3,0);
    ins_quest ((char *) &schlre_k.abzug_proz,3,0);
    ins_quest ((char *) &schlre_k.kun,2,0);
            ins_cursor = prepare_sql ("insert into schlre_k ("
"mdn,  rech_nr,  stat,  ein_nr,  part_nr,  text_nr,  rech_dat,  lief_dat,  lief_s,  "
"kreditor,  regul,  zahl_kond,  eink,  schl_art,  basispr,  netto_betr,  "
"gesamt_betr,  mwst,  mwst_wert,  linie,  schein_mwst,  verrechnet,  abzug_proz,  "
"kun) "

#line 78 "schlre_k.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 80 "schlre_k.rpp"
}
int SCHLRE_K_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

