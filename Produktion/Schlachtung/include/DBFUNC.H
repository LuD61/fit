
extern char qstring [];

int StatusQuery ();
void testquot (char *, char *);
void TestQueryField (char *, char *);
#ifndef _CONSOLE
void ReadQuery (form *, char *[]);
int DbAuswahlEx (short , void (*) (char *), void (*) (char *), form *);
int DbAuswahl (short , void (*) (char *), void (*) (char *));
void SetDbAwSpez (int (*) (short, void (*) (char *), void (*) (char *)));
void SetQueryTab (char *);
#endif
