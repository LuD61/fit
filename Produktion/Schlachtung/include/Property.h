#ifndef _PROPERTY_DEF
#define _PROPERTY_DEF
#include "Text.h"
#include "Vector.h"

class Prop
{
      public :
		  Text Item;
		  Text Value;

		  Prop (char *);
		  Prop (Text&, Text&);
		  Text* GetItem ();
		  void SetItem (Text&);
		  Text* GetValue ();
		  void SetValue (Text&);
};

class CProperty
{
      private :
		  CVector properties;
      public : 
		  CProperty ();
		  ~CProperty ();
		  BOOL Load (Text&);
		  Text* GetValue (Text&);
		  Text* GetItem (Text&);
		  Text* GetItem (int);
		  Text* GetItemAt (int);
		  Text* GetValueAt (int);
		  void Add (Text *);
};
#endif
