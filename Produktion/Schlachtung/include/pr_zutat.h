#ifndef _PRZUTAT_DEF
#define _PRZUTAT
#include "dbclass.h"

struct PR_ZUTAT {
   long      nr;
   long      zei;
   short     sg1;
   char      txt[61];
   short     mdn;
   short     fil;
};
extern struct PR_ZUTAT pr_zutat, pr_zutat_null;

#line 6 "pr_zutat.rh"

class PR_ZUTAT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
               int prep_awcursor (void);
               int prep_awcursor (char *);
               void fill_aw (int);
               int CallDbAuswahl (int);
               void TestQueryTxt (char *, char *);
               void TestQueryTxtn (char *);

       public :
               PR_ZUTAT_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
               int ShowAllBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND hWnd, int ws_flag, char *);
               void ReadQuery (form *, char *[]);
               int PrepareQuery (char *);
               int PrepareQuery (form *, char *[]);
               int QueryBu (HWND hWnd, int ws_flag);
               int ShowAllBu (HWND hWnd, int, int, 
                              int (*) (void), void (*) (int),
                              char *, int,  char *, int,
                              form *, form *, form *);  
};
#endif
