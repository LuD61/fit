create table schlre_drk 
  (
    druck smallint,
    mdn smallint,
    rech_nr integer,
    stat smallint,
    ein_nr integer,
    text_nr integer,
    rech_dat date,
    lief_dat date,
    lief_s integer,
    schl_art smallint,
    linie smallint,
    verrechnet decimal(6,2),
    abzug_proz decimal(5,2),
    kun integer,
    
    a decimal(13,0),
    a_krz_bz char(12),
    netto_gew decimal(10,2),
    anzahl smallint,
    hdkl char(6),
    mfa decimal(4,2),
    art_sum decimal(12,4),
    vorkost integer,
    grund_pr decimal(14,4),
    vorkost_wert decimal(8,2),
    tierkennung char(20),
    fettstufe char(2),
    schlacht_nr integer,
    leb_pr decimal(14,4),
    vork_stat smallint,
    abrech_kz char(2),
    gew_leb decimal(12,2),
    freibank char(3),
    bnp char(1),
    ag integer,
    vorkost_wert1 decimal(8,2),
    vorkost_wert6 decimal(8,2),
    vorkost_wertl decimal(8,2),
    ges_pr decimal(12,2),
    anz_bu smallint,
    anz_unt smallint,
    anz_leber smallint,
    anz_freibank smallint,
    
    kost_art smallint,  
    kost_art_bz char(24),
    rech_basis smallint,
    rech_wert decimal(12,2),
    wert decimal(12,2),
    anzahl_kost integer,
    mwst_schl smallint,
    
    ges_mwst_schl smallint,
    ges_mwst_proz decimal (5,2),
    ges_mwst_betr decimal (10,2),
    ges_netto decimal (10,2),
    ges_brutto decimal (10,2),
    ges_stk integer
  ) in fit_dat extent size 32 next size 256 lock mode row;

create unique index i01schlre_drk on schlre_drk (mdn,ein_nr,schlacht_nr,kost_art,druck);




