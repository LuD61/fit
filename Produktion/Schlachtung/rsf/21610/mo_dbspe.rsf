/*
-----------------------------------------------------------------------------
-
-      (C)OPYRIGHT 1991 by
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Geisental 12, 4630 Bochum 1, Tel: 0234/5065-250
-
-----------------------------------------------------------------------------
-
-  Rechtsklausel:
-  Fuer diese Dokumentation/Programmbeschreibung behalten wir uns alle Rechte
-  auch fuer den Fall der Patenterteilung und der Eintragung eines anderen
-  gewerblichen Schutzrechtes vor.
-  Missbraeuchliche Verwendung, wie insbesondere Vervielfaeltigung und
-  Weitergabe an Dritte, ist nicht gestattet;
-  sie kann zivil- und strafrechtlich geahndet werden.
-
@#+
- ------------------------------------------------------------------------------
-
-	Modulname		:	m_dbspz
-
-	Autor			:	GREINER
-	Erstellungsdatum	:	10.09.93
-
-	Projekt			:	BWS
-	Laendervariante		:	BRD
-
-	Rechner			:	Targon 31
-	Sprache			:       ROSI-SQL 3.1
-
-
------------------------------------------------------------------------------
-
-  Modulbeschreibung		:
-
- -----------------+------------------------------------------------------------
-  Programm - Stueckliste	:
-
-  Programm :           		Datum :
-  Version  : 2.1			Name  :
-
-  Pfad                 | Datei           | Beschreibung
-  --------------------- ----------------- -------------------------------------
-
-
-  Benoetigte Tabellenprogramme und eventuellen Update-Scripte :
-
-  -----------------+-----------------------------------------------------------
-  Systemparameter		:
-
-  Systemparameter  | Wert      | Beschreibung
-  -----------------  ----------  ----------------------------------------------
-  -----------------+-----------------------------------------------------------
-  Prueftabelle			:
-
-  Prueftabelle : 			Berechtigung   :
-                ----------------------                 ------------------------
-  Datentyp     :			Anz. Pruefwerte:
-                ----------------------                 ------------------------
-  Bereich      :
-                ----------------------                 ------------------------
-
-  lfd  | Wert  | Bezeichnung                              | Wert1   | Wert2
-  -----  ------  -----------------------------------------  --------  ---------
@#++
-  -----------------+-----------------------------------------------------------
-  Titel            : m_dbspz.rsf
-  -----------------+-----------------------------------------------------------
-  Version          : @(#)m_dbspz.rsf	2.1   6/23/92   15:47:58
-  -----------------+-----------------------------------------------------------
-  Aenderungsjournal		:
-
-  ---------------+------------------------------------------------------------
-  @(#) Datum     lfd.     Autor       Bemerkungen
-  @(#) --------  -------  ----------  -----------
-  @(#) 1.6.92   #1       GREINER      Ursprungsversion SCCS
------------------------------------------------------------------------------
*/

module mo_dbspe

/*----- Export Section ------------------------------------------------------*/

EXPORT

	database bws

	field danzahl
	field dzeiger

	proc prepupdate
	proc prepread
	proc prepscroll
	proc putmxkey
	proc recupdate
	proc recdelete
	proc recinsert
	proc recread
	proc getauswst
	proc getmxkey
	proc recreaddef
	proc inittable
	proc savekey
	proc backkey
	proc initkey
	proc cmpkey
	proc clrdelstatus
	proc getdelstatus
	proc initform
	proc dispform
	proc fetchcuu
	proc fetchread
	proc erasecursor
	proc closeread
	proc fetchfirst
	proc fetchnext
	proc position_einfuegen
	proc positionen_loeschen
	proc fetchscrollpos
	proc recreaddefpos
	proc inittablepos
	proc getmdnfil
END

/*----- Import Section ------------------------------------------------------*/


IMPORT module mo_cons
END

IMPORT module mo_data
END

IMPORT module mo_ptab
END

IMPORT module f216101 
END

IMPORT module mo_dbmdn
END

IMPORT module d_tbs 
	database bws
END

IMPORT module d_liefs 
	database bws
END

IMPORT module m_schlre 
	database bws
END
IMPORT module mo_meld
END

database bws end

#include "colaenge"

field dsql	 smallint
field danzahl	 smallint
field dzeiger	 smallint

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure prepariert die update cursor.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate
/* -------
	perform prepupdate_rechh returning (dsql)

	return (dsql)
------------------------ */
return (0)
END


/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure prepariert die read cursor.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread
/* ---
	perform prepread_rech returning (dsql)

	return (dsql)
---- */
return (0)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscroll 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor.
-							
---------------------------------------------------------------------------*/


PROCEDURE prepscroll
return (0)
END



/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey
/* ----
	perform putmxkey_rech (danzahl)
------- */
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey
/* --
	perform getmxkey_rech (dzeiger)
----- */
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate 
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate


	return(0)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete 
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete
/* ---
	perform recdelete_rech returning (dsql)
	return(dsql)
--- */
return (0)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert 
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert


	return (0)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread 
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread
/* ----	
	perform recread_rech returning (dsql)

	return (dsql)
------ */
return (0)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef
/* ---	
	perform recreaddef_rech returning (dsql)

	return (dsql)
---- */
	return (0)
END



/*-------------------------------------------------------------------------
-
-	Operatorname	: getauswst
-
-	In		: dstatus		smallint
-						0   Werte aus table
-						<>0 Initstring
-
-	Out		: Laenge des Schluesselfeldes (als konkreter Wert)
-						smallint
-			  mxstring		char	  zusammengesetzter 
-							  String aus im Matrix-
-							  menue auszugebenden
-							  Feldern 
-							  (max. 80 Zeichen)
-			  danzahl		smallint  Anzahl der Datensaetze
-						          einschl. geloeschte 
-							  Saetze
-			  
-	Beschreibung	: Die Procedure liest cursor absolut
-			  und gibt string fuer matrixmenue zurueck
-			  Zurueckgegeben werden muss Zahl fuer Laenge
-			  der integer bzw. smallint-Variablen,plus
-			  feld Bezeichnung . Das Ganze dient dem Aufblenden
-			  des Query-Auswahlfensters !!!
-
-------------------------------------------------------------------------*/

PROCEDURE getauswst

parameter field dstatus		smallint	end
return (8,"",danzahl)

END


/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable  
/* ---
	perform inittable_rech
------ */
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: savekey
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure sichert den alten Schluessel-
-			  wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey
/* ---
	perform savekey_rech
---- */
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: backkey
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt den gesicherten  
-			  Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey
/* --
	perform backkey_rech
---- */
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey
/* ---
	perform initkey_rech
---- */
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuellen
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey

field dret smallint
/* ---
	perform cmpkey_rech returning (dret)
------ */
	return (dret)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus

END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus


	return (0)
END



/*-------------------------------------------------------------------------
-
-	Operatorname	: initform
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Maske mamain1  
-
-------------------------------------------------------------------------*/

PROCEDURE initform

	init form mamain1

END

/*-------------------------------------------------------------------------
-
-	Operatorname	: dispform
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure gibt die Maske mamain1 aus  
-
-------------------------------------------------------------------------*/

PROCEDURE dispform

	display form mamain1

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread 
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber die read
-			  cursor.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread

field delstatus smallint
field dsql	smallint
/* ----
	perform fetchread_rech returning (dsql)
	if dsql return (dsql) end
---- */

	return (dsql)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht alle cursor
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor
/* ----
	perform erasecursor_rech
------- */
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu 
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber die 
-			  update cursor.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu

field delstatus smallint
field dsql	smallint
/* ----
	perform fetchcuu_rech returning (dsql)
	if dsql return (dsql) end
------ */
	return (dsql)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread 
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure schliesst die read cursor.  
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread
/* ---
	perform closeread_rech returning (dsql)

	return (dsql)
------- */
return (0)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchfirst 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest den ersten Satz von cursor
-			  cus_rech.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchfirst

/* ----
	fetch first cursor cus_rech
	return (sqlstatus)
---- */
return (0)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchnext 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest den naechsten Satz von cursor
-			  cus_rech.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchnext

/* ---
	fetch next cursor cus_rech
	return (sqlstatus)
------- */
return (0)
END
/*-------------------------------------------------------------------------
-
-	Operatorname		: positionen_loeschen
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht alle Positions- 
-				  saetze aus der Datenbank.             
-
-------------------------------------------------------------------------*/

PROCEDURE positionen_loeschen
/* ---
	execute from sql
		delete from rech_p
		where rech_p.mdn = $rech_k.mdn   and
		      rech_p.rech_nr = $rech_k.rech_nr   end
	return (sqlstatus)
----- */
return (0)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: position_einfuegen
-
-
-	In			: -
-
-	Out			: dsqlstatus		
-			  
-	Beschreibung		: Die Procedure fuegt einen Positionssatz
-				  in die Datenbanktabelle ein.          
-
-------------------------------------------------------------------------*/

PROCEDURE position_einfuegen

return (0)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure prepariert die scroll cursor cusmain2,
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos

return (0)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchscrollpos 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest alle Positionsaetze
-			  ueber den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchscrollpos
parameter
	field curmod smallint
end
/* ---
		if curmod = CRDFIRST
			fetch cursor cusmain2
		elseif curmod = CRDNEXT
			fetch cursor cusmain2
		end
	return (sqlstatus)
-- */
return (0)
END

/*-------------------------------------------------------------------------
-
-	Operatorplnr	: recreaddefpos
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddefpos

	return (0)
END

/*-------------------------------------------------------------------------
-
-	Operatorplnr	: inittablepos
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittablepos
/* ---
		init table rech_p
--- */
END

/*---------------------------------------------------------------------------
-	Procedure	: prepupdate_rechh
-	In		: -
-	Out		: sqlstatus	smallint
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_rech.
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_rechh
return (0)

END

/*------------------------------------------------------------------------*/

PROCEDURE getmdnfil
/* ---
	let rech_k.mdn = mdn.mdn
	let rech_p.mdn = rech_k.mdn
---- */
END

/*------------------------------------------------------------------------*/

